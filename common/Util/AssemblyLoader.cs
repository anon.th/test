using System;
using System.Data;
using System.Text;
using System.Reflection;
using com.common.AppExtension;
using com.common.DAL;
using com.common.classes;
namespace com.common.util
{
	/// <summary>
	/// Summary description for AssemblyLoader.
	/// </summary>
	public class AssemblyLoader 
	{
		public AssemblyLoader()
		{
			//
			// TODO: Add constructor logic here
			//
		}	
		public Object LoadAssembly(String strApplicationID,String strEnterpriseID,String strAssemblyid,String strInvokeMethod,Object inputParams)
		{
			DbConnection dbCon		=	null;
			DataSet dsService		=	null;
			IDbCommand dbcmd		=	null;		
			String strAssemblyPath	=	null;	
			String strNameSpace		=	null;
			String strClass			=	null;
			dbCon = DbConnectionManager.GetInstance().GetDbConnection(strApplicationID,strEnterpriseID);
			if (dbCon==null)
			{
				Logger.LogTraceError("QuotationMgrDAL.cs","GetAgentService","QBD001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			StringBuilder strQry =  new StringBuilder(); 
			strQry = strQry.Append("select applicationid,enterpriseid,assemblyid,assembly_description,assembly_path,assmebly_namespace,assembly_classname from assembly ");
			strQry.Append(" where applicationid = '"+strApplicationID+"' and enterpriseid = '"+strEnterpriseID);
			strQry.Append("' and assemblyid = '"+strAssemblyid+"'");						
			dbcmd = dbCon.CreateCommand(strQry.ToString(),CommandType.Text); 
			try
			{
				dsService = (DataSet)dbCon.ExecuteQuery(dbcmd,com.common.DAL.ReturnType.DataSetType);  
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("QuotationMgrDAL.cs","GetAgentService","QBD002","Error in Query String");
				throw appException;
			}
			if(dsService.Tables[0].Rows.Count > 0)
			{
				for(int i = 0;i<dsService.Tables[0].Rows.Count;i++)
				{
					DataRow drService = dsService.Tables[0].Rows[i];
					if(Utility.IsNotDBNull(drService["assembly_path"]))
						strAssemblyPath = drService["assembly_path"].ToString();

					if(Utility.IsNotDBNull(drService["assembly_classname"]))
						strClass = drService["assembly_classname"].ToString();

					if(Utility.IsNotDBNull(drService["assmebly_namespace"]))
						strNameSpace = drService["assmebly_namespace"].ToString();
				}
			}
			object oClass;
			Type tType;
			Assembly asm= Assembly.LoadFrom(strAssemblyPath);
			oClass=asm.CreateInstance(strClass);
			tType=asm.GetType(strNameSpace+"."+strClass);
			object[] oArguments = new object[1];
			oArguments[0]=inputParams;
			object oReturn = tType.InvokeMember(strInvokeMethod,BindingFlags.Default | BindingFlags.InvokeMethod,null, oClass, oArguments);
			return oReturn;	
		}
	}
}
