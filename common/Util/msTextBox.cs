using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.ComponentModel;
using System.Text;
using System.Text.RegularExpressions;
namespace com.common.util
{
	/// <summary>
	/// Summary description for msTextBox.
	/// </summary>
	/// 
	public enum MaskType : int
	{
		msUpperAlfaNumeric = 0,
		msUpperAlfaNumericWithUnderscore = 1,
		msLowerAlfaNumeric = 2,
		msLowerAlfaNumericWithUnderscore = 3,
		msAlfaNumericWithUnderscore = 4,
		msNumeric = 5,
		msPrice = 6,
		msDate = 7,
		msDateTime = 8,
		msTime = 9,
		msAlfaNumericWithSpace = 10,
		msTimeNoReturn = 11,//Phase2 - L02
		msAlfaNumericWithUnderscoreSetFormat = 12,
		msNumericCOD = 13, //BY X FEB 26 08
		msNumericCustVAS = 14, 
		msUpperAlfaNumericWithHyphen = 15,
		msUpperAlfaLetterOnly = 16,
		msDateOrTime = 17,
		msDatePOD= 18,
	}

	[DefaultProperty("Text"),
	ToolboxData("<{0}:msTextBox runat=server></{0}:msTextBox>")]
	public class msTextBox : TextBox
	{	
		#region Proporties
		private MaskType _mask; 
		[
		Bindable(true),
		Category("Appearance"),
		DefaultValue(MaskType.msUpperAlfaNumeric),
		Description("MaskType of Textbox to display.")
		]
		public virtual MaskType TextMaskType
		{
			get { return (MaskType) _mask; }
			set { _mask = value; }
		}

		private String _maskString; 
		[
		Bindable(true),
		Category("Appearance"),
		DefaultValue(""),
		Description("MaskString of Textbox.")
		]
		public virtual String TextMaskString
		{
			get { return (String) _maskString; }
			set { _maskString = value; }
		}
		private int _precision; 
		[
		Bindable(true),
		Category("Appearance"),
		DefaultValue(0),
		Description("NumberPrecision of Textbox.")
		]
		public virtual int NumberPrecision
		{
			get { return (int) _precision; }
			set { _precision = value; }
		}
		private int _scale; 
		[
		Bindable(true),
		Category("Appearance"),
		DefaultValue(0),
		Description("NumberPrecision of Textbox.")
		]
		public virtual int NumberScale
		{
			get { return (int) _scale; }
			set { _scale = value; }
		}
		private int _minValue; 
		[
		Bindable(true),
		Category("Appearance"),
		DefaultValue(0),
		Description("Minimum Value of Textbox.")
		]
		public virtual int NumberMinValue
		{
			get { return (int) _minValue; }
			set { _minValue = value; }
		}
		//For Customer VAS
		private double _minValueCustVAS; 
		[
		Bindable(true),
		Category("Appearance"),
		DefaultValue(0.01),
		Description("Minimum Value of Textbox.")
		]
		public virtual double NumberMinValueCustVAS
		{
			get { return (double) _minValueCustVAS; }
			set { _minValueCustVAS = value; }
		}
		//For Customer VAS
		private int _maxValue; 
		[
		Bindable(true),
		Category("Appearance"),
		DefaultValue(0),
		Description("Maximum Value of Textbox.")
		]
		public virtual int NumberMaxValue
		{
			get { return (int) _maxValue; }
			set { _maxValue = value; }
		}
//BY X FEB 26 08
		private double _maxValueCOD; 
		[
		Bindable(true),
		Category("Appearance"),
		DefaultValue(0.00),
		Description("Maximum Value of Textbox.")
		]
		public virtual double NumberMaxValueCOD
		{
			get { return (double) _maxValueCOD; }
			set { _maxValueCOD = value; }
		}
//END BY X FEB 26 08
		
		
		
		
		
		
		#endregion

		#region Methods
		/// <summary>
		///	Add a JavaScript to the Page and call it from the onKeyPress event
		/// </summary>
		/// <param name="e"></param>
		override protected void OnPreRender(EventArgs e) 
		{
			

			if (this.Page.Request.Browser.JavaScript == true) 
			{
				// Build JavaScript	
				if (_mask == MaskType.msDate) 
				{
					// Add KeyPress Event
					this.Columns=10;
					try 
					{
						this.Attributes.Remove("onKeyPress");
						//this.Attributes.Remove("onChange");
						this.Attributes.Remove("onPaste");
						this.Attributes.Remove("onblur");
					} 
					finally 
					{
						if(_maskString == "")
						{
							_maskString = "99/99/9999";
						}

						this.Attributes.Add("onKeyPress", "return InputMask(this,'"+_maskString+"')");
						this.Attributes.Add("onblur",  "CheckMask(this,'"+_maskString+"');return setDate(this);");
						this.Attributes.Add("onPaste", "CheckMask(this,'"+_maskString+"');return setDate(this);");
						//this.Attributes.Add("onChange", "CheckMask(this,'"+_maskString+"');return setDate(this);");
					}
				}
				if (_mask == MaskType.msDatePOD) 
				{
					// Add KeyPress Event
					this.Columns=10;
					try 
					{
						this.Attributes.Remove("onKeyPress");
						//this.Attributes.Remove("onChange");
						this.Attributes.Remove("onPaste");
						this.Attributes.Remove("onblur");
					} 
					finally 
					{
						if(_maskString == "")
						{
							_maskString = "99/99/9999";
						}

						this.Attributes.Add("onKeyPress", "return InputMask(this,'"+_maskString+"')");
						this.Attributes.Add("onblur",  "CheckMask(this,'"+_maskString+"');return setDateExportPOD(this);");
						this.Attributes.Add("onPaste", "CheckMask(this,'"+_maskString+"');return setDateExportPOD(this);");
						//this.Attributes.Add("onChange", "CheckMask(this,'"+_maskString+"');return setDate(this);");
					}
				}
				if (_mask == MaskType.msDateTime) 
				{
					// Add KeyPress Event
					this.Columns=16;
					try 
					{
						this.Attributes.Remove("onKeyPress");
						this.Attributes.Remove("onChange");
					} 
					finally 
					{
						if(_maskString == "")
						{
							_maskString = "99/99/9999 99:99";
						}

						this.Attributes.Add("onKeyPress", "return InputMask(this,'"+_maskString+"')");
						this.Attributes.Add("onChange", "CheckMask(this,'"+_maskString+"');return setDateTime(this);");
					}
				}
				if (_mask == MaskType.msDateOrTime) 
				{
					// Add KeyPress Event
					this.Columns=16;
					try 
					{
						this.Attributes.Remove("onKeyPress");
						this.Attributes.Remove("onChange");
					} 
					finally 
					{
						if(_maskString == "")
						{
							_maskString = "99/99/9999 99:99";
						}

						this.Attributes.Add("onKeyPress", "return InputMask(this,'"+_maskString+"')");
						this.Attributes.Add("onChange", "CheckMask(this,'"+_maskString+"');return setDateOrTime(this);");
					}
				}
				if (_mask == MaskType.msTime) 
				{
					// Add KeyPress Event
					this.Columns=5;
					try 
					{
						this.Attributes.Remove("onKeyPress");
						this.Attributes.Remove("onChange");
					} 
					finally 
					{
						if(_maskString == "")
						{
							_maskString = "99:99";
						}

						this.Attributes.Add("onKeyPress", "return InputMask(this,'"+_maskString+"')");
						this.Attributes.Add("onChange", "CheckMask(this,'"+_maskString+"');return setTime(this);");
					}
				}
				if (_mask == MaskType.msTimeNoReturn) 
				{//Phase2 - L02
					// Add KeyPress Event
					this.Columns=5;
					try 
					{
						this.Attributes.Remove("onKeyPress");
						this.Attributes.Remove("onChange");
					} 
					finally 
					{
						if(_maskString == "")
						{
							_maskString = "99:99";
						}

						this.Attributes.Add("onKeyPress", "return InputMask(this,'"+_maskString+"')");
						this.Attributes.Add("onChange", "CheckMask(this,'"+_maskString+"');setTime(this);");
					}
				}//Phase2 - L02
				else if (_mask == MaskType.msPrice) 
				{
					// Add KeyPress Event onBlur='price_onblur(this);' onFocus='price_onfocus(this);'

					try 
					{
						this.Attributes.Remove("onKeyup");
						this.Attributes.Remove("onKeypress");
					} 
					finally 
					{
						this.Attributes.Add("onKeyup", "formatInt(this);");
						this.Attributes.Add("onKeypress", "return PriceMask(this);");
					}
				}
				else if (_mask == MaskType.msNumeric) 
				{
					// Add KeyPress Event onBlur='price_onblur(this);' onFocus='price_onfocus(this);'
					this.Columns=_precision;
					
					//Console.WriteLine(_precision);

					try 
					{
						this.Attributes.Remove("onKeypress");
						//this.Attributes.Remove("onChange");
					} 
					finally 
					{
						//this.Attributes.Add("onKeypress", "return NumberMask(this,"+_precision+","+_scale+","+_minValue+","+_maxValue+");");
						//this.Attributes.Add("onChange","TrimNumber(this,"+_scale+");");
						this.Attributes.Add("onKeypress", "return NumberMask(this,"+_precision+","+_scale+","+_minValue+","+_maxValue+"); TrimNumber(this,"+_scale+");");
					}
				}
				else if (_mask == MaskType.msNumericCOD) //BY X FEB 26 08
				{
					this.Columns=_precision;
					try 
					{
						this.Attributes.Remove("onKeypress");
					} 
					finally 
					{
						this.Attributes.Add("onKeypress", "return NumberMask(this,"+_precision+","+_scale+","+_minValue+","+_maxValueCOD+"); TrimNumber(this,"+_scale+");");
					}
				}//END BY X FEB 26 08
				else if (_mask == MaskType.msNumericCustVAS) //Cust VAS
				{
					this.Columns=_precision;
					try 
					{
						this.Attributes.Remove("onKeypress");
					} 
					finally 
					{
						this.Attributes.Add("onKeypress", "return NumberMaskCust(this,"+_precision+","+_scale+","+_minValueCustVAS+","+_maxValue+"); TrimNumberCust(this,"+_scale+");");
					}
				}//Cust VAS
				else if (_mask == MaskType.msUpperAlfaNumeric) 
				{
					// Add KeyPress Event onBlur='price_onblur(this);' onFocus='price_onfocus(this);'

					try 
					{
						this.Attributes.Remove("onKeypress");
					} 
					finally 
					{
						this.Attributes.Add("onKeypress", "return UpperMask(this);");
					}
				}
				else if (_mask == MaskType.msUpperAlfaNumericWithUnderscore) 
				{
					// Add KeyPress Event onBlur='price_onblur(this);' onFocus='price_onfocus(this);'

					try 
					{
						this.Attributes.Remove("onKeypress");
					} 
					finally 
					{
						this.Attributes.Add("onKeypress", "return UpperMaskSpecial(this);");
					}
				}

				else if (_mask == MaskType.msLowerAlfaNumeric) 
				{
					// Add KeyPress Event onBlur='price_onblur(this);' onFocus='price_onfocus(this);'

					try 
					{
						this.Attributes.Remove("onKeypress");
					} 
					finally 
					{
						this.Attributes.Add("onKeypress", "return LowerMask(this);");
					}
				}
				else if (_mask == MaskType.msLowerAlfaNumericWithUnderscore) 
				{
					// Add KeyPress Event onBlur='price_onblur(this);' onFocus='price_onfocus(this);'

					try 
					{
						this.Attributes.Remove("onKeypress");
					} 
					finally 
					{
						this.Attributes.Add("onKeypress", "return LowerMaskSpecial(this);");
					}
				}
				else if (_mask == MaskType.msAlfaNumericWithUnderscore) 
				{
					// Add KeyPress Event onBlur='price_onblur(this);' onFocus='price_onfocus(this);'

					try 
					{
						this.Attributes.Remove("onKeypress");
					} 
					finally 
					{
						this.Attributes.Add("onKeypress", "return AlfaNumericMaskSpecial(this);");
					}
				}
				else if (_mask == MaskType.msAlfaNumericWithSpace) 
				{
					try 
					{
						this.Attributes.Remove("onKeypress");
					} 
					finally 
					{
						this.Attributes.Add("onKeypress", "return AlfaNumericSpaceMaskSpecial(this);");
					}
				}
				else if (_mask == MaskType.msAlfaNumericWithUnderscoreSetFormat) 
				{
					try 
					{
						this.Attributes.Remove("onKeypress");
						this.Attributes.Remove("onChange");
					} 
					finally 
					{
						this.Attributes.Add("onKeypress", "return InputMask(this,'"+_maskString+"')");
						this.Attributes.Add("onChange", "setElasticDateTime(this);");
					}
				}
				else if (_mask == MaskType.msUpperAlfaNumericWithHyphen) 
				{
					// Add KeyPress Event onBlur='price_onblur(this);' onFocus='price_onfocus(this);'

					try 
					{
						this.Attributes.Remove("onKeypress");
					} 
					finally 
					{
						this.Attributes.Add("onKeypress", "return UpperMaskSpecialWithHyphen(this);");
					}
				}
				else if (_mask == MaskType.msUpperAlfaLetterOnly) 
				{
					// Add KeyPress Event onBlur='price_onblur(this);' onFocus='price_onfocus(this);'

					try 
					{
						this.Attributes.Remove("onKeypress");
					} 
					finally 
					{
						this.Attributes.Add("onKeypress", "return UpperLetterOnlyMask(this);");
					}
				}
			}
		}
		
		#endregion
		
	}
}
