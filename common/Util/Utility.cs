using System;
using System.Configuration;
using System.Collections;
using System.Collections.Specialized;
using System.Web.SessionState;
using System.Globalization;
using System.Resources;
using System.Data;
using com.common.DAL;
using com.common.classes;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;
using System.IO;
using System.Text;
using System.Web;
using System.Web.Mail;

namespace com.common.util
{
	/// <summary>
	/// ResourceType enum used with Resource Files to identify between various resource files. At the moment only 
	/// three different resource files are used viz. for UserMessages, ModuleName and ScreenLabel
	/// </summary>
	public enum ResourceType
	{
		UserMessage,
		ModuleName,
		ScreenLabel
	}

	/// <summary>
	/// This enum is used while getting SystemCodeValues from the Core_SystemCode 
	/// tables using GetCodeValues() method in this class.
	/// </summary>
	public enum CodeValueType
	{
		StringValue,
		NumberValue
	}

	public enum JobType
	{
		MCAJob,
		InvoiceGenerationJob
	}

	/// <summary>
	/// This enum is used by the DateFormat() method of this class to implement different logic for Date, DateTime and Time for the time
	/// </summary>
	public enum DTFormat
	{
		Date,DateTime,Time,DateLongTime,DateVeryLongTime
	}

	/// <summary>
	/// This enum is used by EnableButton() method of this class to specify the type of the button.
	/// </summary>
	public enum ButtonType
	{
		Insert,Save,Delete
	}

	/// <summary>
	/// This enum is used by the FormatOuterJoinString() method of this class to implement different SQLQuery string format for different providers
	/// </summary>
	public enum OuterJoin
	{
		Left,Right
	}

	/// <summary>
	/// This class implements commonly required methods.
	/// </summary>
	public class Utility
	{
		string m_strAppID = null;
		string m_strUserCulture = null;
		string m_strEnterpriseID = null;
		string m_strUserID = null;
		string m_strDeploymentEnv = "false";
		HttpSessionState m_session = null;
		string m_strEmailNameSend = null;
		string m_strEmailSend = null;
		string m_strEmailSubject = null;
		string m_strSmtpServer = null;

		//String m_History = null;

		protected static ResourceManager m_rmMessages = null;
		protected static ResourceManager m_rmModules = null;
		protected static ResourceManager m_rmScreenLabels = null;

		/// <summary>
		/// Constructor. It stores the passed argument values in to corresponding instance member variables.
		/// </summary>
		/// <param name="appSettings">NameValueCollection to represent key-value collection of the web.config file</param>
		/// <param name="session">HttpSessionState object for the current user session</param>
		public Utility(NameValueCollection appSettings,HttpSessionState session)
		{
			m_strDeploymentEnv = appSettings["deploymentEnv"];
			m_session = session;
		}

		/// <summary>
		/// Gets the applicationid either from web.config file or session depending on value of deploymentEnv flag.
		/// If the flag is true the value is from session<br>
		/// If the flag is false the value is from the web.config file
		/// </summary>
		/// <returns>Application id for the current application</returns>
		public String GetAppID()
		{
			m_strAppID = System.Configuration.ConfigurationSettings.AppSettings["applicationID"];
			return m_strAppID;
		}


		/// <summary>
		/// Gets the enterpriseid either from web.config file or session depending on value of deploymentEnv flag.
		/// If the flag is true the value is from session<br>
		/// If the flag is false the value is from the web.config file
		/// </summary>
		/// <returns>Enerpriseid for the current enterprsie</returns>
		public String GetEnterpriseID()
		{
			if (m_strDeploymentEnv.Equals("false"))
			{
				m_strEnterpriseID = System.Configuration.ConfigurationSettings.AppSettings["enterpriseID"];
			}
			else
			{
				m_strEnterpriseID = (String)m_session["enterpriseID"];
			}
			return m_strEnterpriseID;
		}


		/// <summary>
		/// Gets the user culture either from web.config file or session depending on value of deploymentEnv flag.
		/// If the flag is true the value is from session<br>
		/// If the flag is false the value is from the web.config file
		/// </summary>
		/// <returns>User culture for the current user</returns>
		public String GetUserCulture()
		{
			if (m_strDeploymentEnv.Equals("false"))
			{
				m_strUserCulture = System.Configuration.ConfigurationSettings.AppSettings["userCulture"];
			}
			else
			{
				m_strUserCulture = (String)m_session["userCulture"];
			}

			if((m_strUserCulture == null) || (m_strUserCulture == ""))
			{
				m_strUserCulture = System.Configuration.ConfigurationSettings.AppSettings["userCulture"];
			}

			return m_strUserCulture;
		}


		/// <summary>
		/// Gets the userid for the current user either from web.config file or session depending on value of deploymentEnv flag.
		/// If the flag is true the value is from session<br>
		/// If the flag is false the value is from the web.config file
		/// </summary>
		/// <returns>User id of the current user</returns>
		public String GetUserID()
		{
			if (m_strDeploymentEnv.Equals("false"))
			{
				m_strUserID = System.Configuration.ConfigurationSettings.AppSettings["userID"];
			}
			else
			{
				m_strUserID = (String)m_session["userID"];
			}
			return m_strUserID;
		}

		/// <summary>
		/// Gets the userid for the current user either from web.config file or session depending on value of deploymentEnv flag.
		/// If the flag is true the value is from session<br>
		/// If the flag is false the value is from the web.config file
		/// </summary>
		/// <returns>Email Sernder</returns>
		public String GetEmailNameSend()
		{
			m_strEmailNameSend = System.Configuration.ConfigurationSettings.AppSettings["ForwardManifestSenderName"];
			return m_strEmailNameSend;
		}
		
		public String GetEmailSend()
		{

			m_strEmailSend = System.Configuration.ConfigurationSettings.AppSettings["ForwardManifestSenderEmail"];
			return m_strEmailSend;
		}
		public String GetEmailSubject()
		{

			m_strEmailSubject = System.Configuration.ConfigurationSettings.AppSettings["ForwardManifestSubjectEmail"];
			return m_strEmailSubject;
		}
		
		public String GetSmtpServer()
		{
			m_strSmtpServer = System.Configuration.ConfigurationSettings.AppSettings["smtpServer"];
			return m_strSmtpServer;
		}

		/// <summary>
		/// Resource Manager to read from UserMessages resource files.
		/// </summary>
		public static ResourceManager MessagesResourceManager
		{
			set
			{
				m_rmMessages = value;
			}

			get
			{
				return m_rmMessages;
			}
		}


		/// <summary>
		/// Resource Manager to read from Module resource files.
		/// </summary>
		public static ResourceManager ModulesResourceManager
		{
			set
			{
				m_rmModules = value;
			}

			get
			{
				return m_rmModules;
			}
		}



		/// <summary>
		/// Resource Manager to read from ScreenLabels resource files.
		/// </summary>
		public static ResourceManager ScreenLabelsResourceManager
		{
			set
			{
				m_rmScreenLabels = value;
			}

			get
			{
				return m_rmScreenLabels;
			}
		}


		/// <summary>
		/// This method is used to read culture specific text for the specified key and user culture 
		/// from the resource file identified by one of  ResourceType enum value
		/// </summary>
		/// <param name="resourceType">One of ResourceType enum value</param>
		/// <param name="strKey">one of valid key in the resource file</param>
		/// <param name="strUserCulture">Culture of the user</param>
		/// <returns>Returns the language specific string for the specified key</returns>
		public static String GetLanguageText(ResourceType resourceType, String strKey, String strUserCulture)
		{
			strKey = strKey.Trim();
			/*if (strKey.EndsWith(".")) 
			{
				strKey = strKey.Substring(0, strKey.Length-1); 
			}*/
			String strText = null;
			m_rmMessages.IgnoreCase=true;
			try
			{
				switch(resourceType)
				{
					case ResourceType.UserMessage:
						if(m_rmMessages != null)
						{
							strText = m_rmMessages.GetString(strKey.ToUpper(),new CultureInfo(strUserCulture));
						}
						break;

					case ResourceType.ModuleName:
						if(m_rmModules != null)
						{
							strText = m_rmModules.GetString(strKey.ToUpper(),new CultureInfo(strUserCulture));
						}
						if(strText.Trim()=="")
						{
							strText = strKey;
						}
						break;

					case ResourceType.ScreenLabel:
						if(m_rmScreenLabels != null)
						{
							strText = m_rmScreenLabels.GetString(strKey.ToUpper(),new CultureInfo(strUserCulture));
						}
						break;
				}
				/*if (strKey.ToUpper().Equals(strText)) 
				{
					strText = strKey;
				}*/
			}
			catch(Exception ex)
			{
				strText = strKey;
				Console.Write(ex.Message);
			}
			return strText;
		}


		public static String GetLanguageText(ResourceType resourceType, String strKey, String strUserCulture, ArrayList paramValues)
		{
			String strMessage = GetLanguageText(resourceType,strKey,strUserCulture);

			Regex re = new Regex("(<#>)");
			
			foreach(String strValue in paramValues)
			{
				strMessage = re.Replace(strMessage,strValue,1);
			}

			return strMessage;
		}


		/// <summary>
		/// Gets system code values from the Core_System_Code table for the specified applicaitonid, user Culture and the codeid
		/// </summary>
		/// <param name="strAppID">Applicaitonid for the current application</param>
		/// <param name="strUserCulture">User culture string for the current user</param>
		/// <param name="strCode">System code id</param>
		/// <param name="valueType">one of CodeValueType enum to specify whether numeric or string values are tobe returned.</param>
		/// <returns>ArrayList containing objects of type SystemCode</returns>
		public static ArrayList GetCodeValues(String strAppID, String strUserCulture, String strCode, CodeValueType valueType)
		{
			ArrayList codeValues = null;

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID);

			if(dbCon == null)
			{
				Logger.LogTraceError("Utility","GetCodeValues","EDB101","DbConnection object is null!!");
				System.Diagnostics.Trace.WriteLineIf(Logger.AppSwitch.TraceError,"[Utility::GetCodeValues()] DbConnection is null","ERROR");
				return codeValues;
			}

			String strSQLQuery = "select sequence, code_text, code_str_value, code_num_value from Core_System_Code where ";
			
			strSQLQuery += "applicationid = '"+strAppID+"' and culture = '"+strUserCulture+"' and codeid = '"+strCode+"' order by sequence"; 
			
			DataSet dsSystemCode = (DataSet)dbCon.ExecuteQuery(strSQLQuery,ReturnType.DataSetType);

			int cnt = dsSystemCode.Tables[0].Rows.Count;

			if(cnt > 0)
			{
				codeValues = new ArrayList();
				
				for(int i=0; i < cnt; i++)
				{
					DataRow drEach = dsSystemCode.Tables[0].Rows[i];
					SystemCode systemCode = new SystemCode();

					if(!drEach["code_text"].GetType().Equals(System.Type.GetType("System.DBNull")))
					{
						systemCode.Text = (String) drEach["code_text"];
					}

					switch(valueType)
					{
						case CodeValueType.StringValue:
						default:

							if(!drEach["code_str_value"].GetType().Equals(System.Type.GetType("System.DBNull")))
							{
								systemCode.StringValue = (String) drEach["code_str_value"];
							}

							break;

						case CodeValueType.NumberValue:
							if(!drEach["code_num_value"].GetType().Equals(System.Type.GetType("System.DBNull")))
							{
								systemCode.NumValue = (float) drEach["code_num_value"];
							}
							break;
					}

					codeValues.Add(systemCode);
				}


			}

			return  codeValues;
		}

		public static ArrayList GetCurrencyCode(String strAppID, String strUserCulture, String strCode, CodeValueType valueType)
		{
			ArrayList codeValues = null;

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID);

			if(dbCon == null)
			{
				Logger.LogTraceError("Utility","GetCodeValues","EDB101","DbConnection object is null!!");
				System.Diagnostics.Trace.WriteLineIf(Logger.AppSwitch.TraceError,"[Utility::GetCodeValues()] DbConnection is null","ERROR");
				return codeValues;
			}

			String strSQLQuery = "select DISTINCT CurrencyCode from dbo.CountriesCurrencies where ";
			
			strSQLQuery += "applicationid = '" + strAppID + "' ORDER BY CurrencyCode;"; 
			
			DataSet dsSystemCode = (DataSet)dbCon.ExecuteQuery(strSQLQuery,ReturnType.DataSetType);

			int cnt = dsSystemCode.Tables[0].Rows.Count;

			if(cnt > 0)
			{
				codeValues = new ArrayList();
				
				for(int i=0; i < cnt; i++)
				{
					DataRow drEach = dsSystemCode.Tables[0].Rows[i];
					SystemCode systemCode = new SystemCode();

					if(!drEach["CurrencyCode"].GetType().Equals(System.Type.GetType("System.DBNull")))
					{
						systemCode.Text = (String) drEach["CurrencyCode"];
					}

					switch(valueType)
					{
						case CodeValueType.StringValue:
						default:

							if(!drEach["CurrencyCode"].GetType().Equals(System.Type.GetType("System.DBNull")))
							{
								systemCode.StringValue = (String) drEach["CurrencyCode"];
							}

							break;

						case CodeValueType.NumberValue:
							if(!drEach["CurrencyCode"].GetType().Equals(System.Type.GetType("System.DBNull")))
							{
								systemCode.NumValue = (float) drEach["CurrencyCode"];
							}
							break;
					}
					codeValues.Add(systemCode);
				}
			}

			return  codeValues;
		}

		public static ArrayList GetServiceValues(String strAppID, String strEnterpriseID, String strPostCode)
		{
			ArrayList codeValues = null;

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID);

			if(dbCon == null)
			{
				Logger.LogTraceError("Utility","GetServiceValues","EDB101","DbConnection object is null!!");
				System.Diagnostics.Trace.WriteLineIf(Logger.AppSwitch.TraceError,"[Utility::GetCodeValues()] DbConnection is null","ERROR");
				return codeValues;
			}

			String strSQLQuery = "select distinct(service_code), ISNULL(transit_day * 24, 0) + ISNULL(transit_hour, 0) AS transit_time from Service ";
			
			strSQLQuery += "where applicationid = '"+strAppID+"' and enterpriseid = '"+strEnterpriseID+"' "; 
			strSQLQuery += "and service_code not in (select distinct(service_code) from Zipcode_Service_Excluded where zipcode = '"+strPostCode+"')";
			strSQLQuery += "order by transit_time";
			
			DataSet dsServiceCode = (DataSet)dbCon.ExecuteQuery(strSQLQuery,ReturnType.DataSetType);

			int cnt = dsServiceCode.Tables[0].Rows.Count;

			if(cnt > 0)
			{
				codeValues = new ArrayList();
				
				for(int i=0; i < cnt; i++)
				{
					DataRow drEach = dsServiceCode.Tables[0].Rows[i];
					SystemCode systemCode = new SystemCode();

					if(!drEach["service_code"].GetType().Equals(System.Type.GetType("System.DBNull")))
					{
						systemCode.Text = (String) drEach["service_code"];
						systemCode.StringValue = (String) drEach["service_code"];
					}
					codeValues.Add(systemCode);
				}
			}

			return  codeValues;
		}
		public string GetEnterpriseName(String strAppID, String enterpriseID)
		{
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID);

			if(dbCon == null)
			{
				Logger.LogTraceError("Utility","GetCodeValues","EDB101","DbConnection object is null!!");
				System.Diagnostics.Trace.WriteLineIf(Logger.AppSwitch.TraceError,"[Utility::GetCodeValues()] DbConnection is null","ERROR");
				return "";
			}

			String strSQLQuery = "select enterprise_name from enterprise where applicationid = '"+ strAppID + "' and enterpriseid = '" + enterpriseID + "'";
			 
			
			DataSet ds = (DataSet)dbCon.ExecuteQuery(strSQLQuery,ReturnType.DataSetType);

			if(ds.Tables[0].Rows.Count > 0)
				return ds.Tables[0].Rows[0][0].ToString();

			return "";
		}


		public static ArrayList GetCodeValuesForInvoiceStatus(String strAppID, String strUserCulture, String strCode, CodeValueType valueType)
		{
			ArrayList codeValues = null;

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID);

			if(dbCon == null)
			{
				Logger.LogTraceError("Utility","GetCodeValues","EDB101","DbConnection object is null!!");
				System.Diagnostics.Trace.WriteLineIf(Logger.AppSwitch.TraceError,"[Utility::GetCodeValues()] DbConnection is null","ERROR");
				return codeValues;
			}

			String strSQLQuery = "select sequence, code_text, code_str_value, code_num_value from Core_System_Code where ";
			
			strSQLQuery += "applicationid = '"+strAppID +
				"' and culture = '"+strUserCulture +
				"' and codeid = '"+strCode+
				"' and code_str_value <> 'P"+
				"' order by sequence"; 
			
			DataSet dsSystemCode = (DataSet)dbCon.ExecuteQuery(strSQLQuery,ReturnType.DataSetType);

			int cnt = dsSystemCode.Tables[0].Rows.Count;

			if(cnt > 0)
			{
				codeValues = new ArrayList();
				
				for(int i=0; i < cnt; i++)
				{
					DataRow drEach = dsSystemCode.Tables[0].Rows[i];
					SystemCode systemCode = new SystemCode();

					if(!drEach["code_text"].GetType().Equals(System.Type.GetType("System.DBNull")))
					{
						systemCode.Text = (String) drEach["code_text"];
					}

					switch(valueType)
					{
						case CodeValueType.StringValue:
						default:

							if(!drEach["code_str_value"].GetType().Equals(System.Type.GetType("System.DBNull")))
							{
								systemCode.StringValue = (String) drEach["code_str_value"];
							}

							break;

						case CodeValueType.NumberValue:
							if(!drEach["code_num_value"].GetType().Equals(System.Type.GetType("System.DBNull")))
							{
								systemCode.NumValue = (float) drEach["code_num_value"];
							}
							break;
					}

					codeValues.Add(systemCode);
				}


			}

			return  codeValues;
		}

		public static int IsServiceAvailable(string strAppID,string strEnterpriseID, string serviceType, string senderZipcode, string recipientZipcode)
		{
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			//			System.Collections.ArrayList storedParams = new System.Collections.ArrayList();
			//			storedParams.Add(new System.Data.SqlClient.SqlParameter("@enterpriseid",strEnterpriseID));
			//			storedParams.Add(new System.Data.SqlClient.SqlParameter("@serviceType",serviceType));
			//			storedParams.Add(new System.Data.SqlClient.SqlParameter("@senderZipcode",senderZipcode));
			//			storedParams.Add(new System.Data.SqlClient.SqlParameter("@recipientZipcode",recipientZipcode));

			string queryIsServiceAvailable = "Select IsAvailable FROM dbo.IsServiceAvailable('" + strEnterpriseID + "', '" + serviceType + "','" + senderZipcode + "','" + recipientZipcode + "')";

			DataSet ds = (DataSet)dbCon.ExecuteQuery(queryIsServiceAvailable,ReturnType.DataSetType);
			
			int result;
			if(ds == null)
				return 0;
			else
			{
				if(ds.Tables.Count > 0)
				{
					if(ds.Tables[0].Rows.Count > 0)
						result = (int)ds.Tables[0].Rows[0][0];
					else
						result = 0;
				}
				else
					result = 0;
			}
			return result;
		}

		public static DataSet GetStatusCode(String strAppID, String strEnterpriseid, String strCode)
		{
			DataSet ds = new DataSet();
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID);
			
			String strSQLQuery = "select status_code,status_description,status_type,auto_process from status_code where ";
			
			strSQLQuery += "applicationid = '"+strAppID+"' and enterpriseid = '"+strEnterpriseid+"'  "; 
			
			ds = (DataSet)dbCon.ExecuteQuery(strSQLQuery,ReturnType.DataSetType);

			return ds;

		}


		public static bool IsDeliveryManifestCountCorrected(String strAppID, String strEnterpriseid, String strPathCode,String strFlightVehicleNo,String strManifestDate)
		{
			DataSet ds = new DataSet();
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID);
			
			int iManifest_Count=0,iManifest_Details=0;
			
			String strManifestCount = "SELECT manifest_count FROM Delivery_Manifest ";
			strManifestCount+= "WHERE applicationid = '"+strAppID+"' AND enterpriseid = '"+strEnterpriseid+"' AND path_code = '"+ strPathCode+"' AND flight_vehicle_no = '"+ strFlightVehicleNo+"' ";

			DateTime dtDelManifestDateTime = System.DateTime.ParseExact(strManifestDate,"dd/MM/yyyy HH:mm",null);
			strManifestDate=Utility.DateFormat(strAppID, strEnterpriseid,dtDelManifestDateTime,DTFormat.DateTime);
			strManifestCount+=" and delivery_manifest_datetime ="+strManifestDate;

			ds = (DataSet)dbCon.ExecuteQuery(strManifestCount,ReturnType.DataSetType);

			if(!ds.Tables[0].Rows[0][0].GetType().Equals(System.Type.GetType("System.DBNull")))
			{
				iManifest_Count = (int) ds.Tables[0].Rows[0][0];
			}
			
			String strSQLQuery = "select count(*) from Delivery_Manifest_Detail where ";
			strSQLQuery += " applicationid = '"+strAppID+"' and enterpriseid = '"+strEnterpriseid+"' AND path_code = '"+ strPathCode+"' AND flight_vehicle_no = '"+ strFlightVehicleNo+"' "; 
			strSQLQuery+=" and delivery_manifest_datetime ="+strManifestDate;

			ds = (DataSet)dbCon.ExecuteQuery(strSQLQuery,ReturnType.DataSetType);


			if(!ds.Tables[0].Rows[0][0].GetType().Equals(System.Type.GetType("System.DBNull")))
			{
				iManifest_Details = (int) ds.Tables[0].Rows[0][0];
			}


			if(iManifest_Count!=iManifest_Details)
				return false;
			else
				return true;
			//return ds;
			//return false;

		}


		/// <summary>
		/// Formats the DateTime object value based on the current database (SQL/ORACLE/OLEDB)
		/// The required format is specified by the enum value DTFormat.
		/// </summary>
		/// <param name="strAppID">Applicaitonid for the current application</param>
		/// <param name="strEnterpriseID">Enterpriseid for the current enterprise</param>
		/// <param name="dtToConvert">DateTime object to format</param>
		/// <param name="format">one of DTFormat enum value</param>
		/// <returns>Returns the formated date</returns>
		public static String DateFormat(String strAppID, String strEnterpriseID, DateTime dtToConvert, DTFormat format)
		{
			String strToDate = null;
			DataProviderType dbProvider = DbConnectionManager.GetInstance().GetDbProviderType(strAppID,strEnterpriseID);

			switch(dbProvider)
			{
				case DataProviderType.Oracle:
				switch(format)
				{
					case DTFormat.Date:
						strToDate = "to_date('"+dtToConvert.ToString("MM/dd/yyyy")+"','mm/dd/yyyy')";		
						break;

					case DTFormat.DateTime:
						strToDate = "to_date('"+dtToConvert.ToString("MM/dd/yyyy HH:mm")+"','mm/dd/yyyy hh24:mi')";		
						break;

					case DTFormat.Time:
						strToDate = "to_date('01/01/2001 "+dtToConvert.ToString("HH:mm")+"','mm/dd/yyyy hh24:mi')";		
						break;
				}

					break;

				case DataProviderType.Sql:
				case DataProviderType.Oledb:
	
				switch(format)
				{
					case DTFormat.Date:
						strToDate = "'"+dtToConvert.ToString("MM/dd/yyyy")+"'";		
						break;

					case DTFormat.DateTime:
						strToDate = "'"+dtToConvert.ToString("MM/dd/yyyy HH:mm")+"'";		
						break;

					case DTFormat.Time:
						strToDate = "'01/01/2001 "+dtToConvert.ToString("HH:mm")+"'";		
						break;
					case DTFormat.DateLongTime: 
						strToDate = "'"+dtToConvert.ToString("MM/dd/yyyy HH:mm:ss")+"'";		
						break;
					case DTFormat.DateVeryLongTime:
						strToDate = "'"+dtToConvert.ToString("MM/dd/yyyy HH:mm:ss.fff")+"'";		
						break;
				}

					break;


	
			}

			return strToDate;
		}


		public static String ReplaceSingleQuote(String strToReplace)
		{
			String strReplaced="";
			if (strToReplace != null)
				strReplaced=strToReplace.Replace("'","''");

			return strReplaced;
		}


		public static string ReplaceSingleQuoteURL(string strToReplace)
		{
			string strReplaced = "";
			if (strToReplace != null)
			{
				strReplaced = System.Web.HttpUtility.UrlEncode(strToReplace).Replace("'","\\");
			}
			return strReplaced;
		}


		public static String ReplaceSemicolon(String strToReplace)
		{
			String strReplaced="";
			if (strToReplace != null)
				strReplaced=strToReplace.Replace(";","; ");

			return strReplaced;
		}


		/// <summary>
		/// This method is used to enable/disable Insert, Save and Delete button on various pages.
		/// </summary>
		/// <param name="button">Button object to be enabled or disabled passed by reference</param>
		/// <param name="buttonType">one of ButtonType enum to represent type of button (Insert/Update/Delete)</param>
		/// <param name="enable">true to enable, false to disable</param>
		/// <param name="accessRights">AccessRights object which has property for Insert, Update and Delete rights for the current user</param>
		public static void EnableButton(ref Button button, com.common.util.ButtonType buttonType,bool enable,AccessRights accessRights)
		{
			if(button == null || accessRights == null)
			{
				return;
			}

			switch(buttonType)
			{
				case ButtonType.Delete:
					button.Enabled = (enable && accessRights.Delete);
					break;

				case ButtonType.Insert:
					button.Enabled = (enable && accessRights.Insert);
					break;

				case ButtonType.Save:
					button.Enabled = (enable && (accessRights.Insert || accessRights.Update));
					break;

				default:
					break;
			}
		}


		/// <summary>
		/// Formats the SQLQuery string for OuterJoin based on the current database (SQL/ORACLE/OLEDB)
		/// The required format is specified by the enum value OuterJoin.
		/// </summary>
		/// <param name="strAppID">Applicaitonid for the current application</param>
		/// <param name="strEnterpriseID">Enterpriseid for the current enterprise</param>
		/// <param name="strLeftColumnName">string object to format</param>
		/// <param name="strRightColumnName">string object to format</param>
		/// <param name="format">one of OuterJoin enum value</param>
		/// <returns>Returns the formated Outer Join SQLQuery</returns>
		public static String FormatOuterJoinString(String strAppID, String strEnterpriseID, String strLeftColumnName, String strRightColumnName, OuterJoin format)
		{
			String strQuery = null;
			DataProviderType dbProvider = DbConnectionManager.GetInstance().GetDbProviderType(strAppID,strEnterpriseID);

			switch(dbProvider)
			{
				case DataProviderType.Oracle:
				switch(format)
				{
					case OuterJoin.Left:
						strQuery = strLeftColumnName+" = "+strRightColumnName+"(+) ";
						break;

					case OuterJoin.Right:
						strQuery = strLeftColumnName+"(+) = "+strRightColumnName+" ";
						break;
				}
					break;

				case DataProviderType.Oledb:
					break;

				case DataProviderType.Sql:
				switch(format)
				{
					case OuterJoin.Left:
						strQuery = strLeftColumnName+" *= "+strRightColumnName+" ";
						break;

					case OuterJoin.Right:
						strQuery = strLeftColumnName+" =* "+strRightColumnName+" ";
						break;
				}
					break;
			}

			return strQuery;
		}


		public static bool IsNotDBNull(Object objToCheck)
		{
			if(objToCheck != null && !objToCheck.GetType().Equals(System.Type.GetType("System.DBNull")))
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		public static String GetJobName(String strAppID, String strEnterpriseID, JobType jobType)
		{
			String strJobName = strAppID+"_"+strEnterpriseID+"_";

			switch(jobType)
			{
				case JobType.MCAJob:
					strJobName += "MCAJOB";
					break;

				case JobType.InvoiceGenerationJob:
					strJobName += "INVOICEGENJOB"; 
					break;

				default:
					break;
			}

			return strJobName;
		}


		public static void RegisterScriptFile(String strScriptFileName,String strRegisterByName, System.Web.UI.Page currentPage)
		{
			String strJSScriptDir = System.Configuration.ConfigurationSettings.AppSettings["jsDir"];
			String strJSScriptFullPath = strJSScriptDir +"\\"+strScriptFileName;
			
			if(!File.Exists(strJSScriptFullPath))
			{
				//No script file found by that name
				return;
			}

			StreamReader scriptReader = new StreamReader(strJSScriptFullPath);
			String strScript = scriptReader.ReadToEnd();
			
			if(!currentPage.IsStartupScriptRegistered(strRegisterByName))
			{
				currentPage.RegisterStartupScript(strRegisterByName,strScript);		
			}			
		}


		public static void RegisterScriptString(String strScriptString,String strRegisterByName, System.Web.UI.Page currentPage)
		{
			if(strScriptString != null && strScriptString.Length > 0)
			{
				if(!currentPage.IsStartupScriptRegistered(strRegisterByName))
				{
					currentPage.RegisterStartupScript(strRegisterByName,strScriptString);		
				}			
			}
		}


		public static String GetScript(String strScriptFileName, ArrayList paramValues)
		{
			String strScript = null;

			String strJSScriptDir = System.Configuration.ConfigurationSettings.AppSettings["jsDir"];
			String strJSScriptFullPath = strJSScriptDir +"\\"+strScriptFileName;
			
			if(!File.Exists(strJSScriptFullPath))
			{
				//No script file found by that name
				return strScript;
			}

			StreamReader scriptReader = new StreamReader(strJSScriptFullPath);
			strScript = scriptReader.ReadToEnd();
			
			Regex re = new Regex("(<#>)");
			
			foreach(String strValue in paramValues)
			{
				strScript = re.Replace(strScript,strValue);
			}
		
			return strScript;
		}

		public static String GetScript(String strScriptFileName, ArrayList paramValues,int height,int width)
		{
			String strScript = null;

			String strJSScriptDir = System.Configuration.ConfigurationSettings.AppSettings["jsDir"];
			String strJSScriptFullPath = strJSScriptDir +"\\"+strScriptFileName;
			
			if(!File.Exists(strJSScriptFullPath))
			{
				//No script file found by that name
				return strScript;
			}

			StreamReader scriptReader = new StreamReader(strJSScriptFullPath);
			strScript = scriptReader.ReadToEnd();
			
			Regex re = new Regex("(<#>)");
			
			foreach(String strValue in paramValues)
			{
				strScript = re.Replace(strScript,strValue);
			}
			strScript = strScript.Replace("#height",height.ToString());
			strScript = strScript.Replace("#width",width.ToString());
			return strScript;
		}


		public static String GetScriptPopupSetSize(String strScriptFileName, ArrayList paramValues,int height,int width)
		{
			String strScript = null;

			String strJSScriptDir = System.Configuration.ConfigurationSettings.AppSettings["jsDir"];
			String strJSScriptFullPath = strJSScriptDir +"\\"+strScriptFileName;
			
			if(!File.Exists(strJSScriptFullPath))
			{
				//No script file found by that name
				return strScript;
			}

			StreamReader scriptReader = new StreamReader(strJSScriptFullPath);
			strScript = scriptReader.ReadToEnd();
			
			Regex re = new Regex("(<#>)");
			
			foreach(String strValue in paramValues)
			{
				strScript = re.Replace(strScript,strValue,1);

			}
			strScript = strScript.Replace("#height",height.ToString());
			strScript = strScript.Replace("#width",width.ToString());
			return strScript;
		}


		#region "For Negative Nuber Format"

		public String encodeNegValue(object val)
		{
			String encodeVal = null;

			if (!val.GetType().Equals(System.Type.GetType("System.DBNull")))
			{
				decimal newval = Convert.ToDecimal(val);
				if(newval < 0 )
				{
					encodeVal = "(" +Math.Abs(newval)+ ")";
				}
				else
				{
					encodeVal = newval.ToString();
				}
			}
			return encodeVal;

		}


		public String encodeNegValue(object val,String m_format)
		{
			String encodeVal = null;

			if (!val.GetType().Equals(System.Type.GetType("System.DBNull")))
			{
				decimal newval = Convert.ToDecimal(val);
				if(newval < 0 )
				{
					encodeVal = "(" +String.Format(m_format,Math.Abs(newval))+ ")";
				}
				else
				{
					encodeVal = String.Format(m_format,newval);
				}
			}
			return encodeVal;

		}


		public decimal decodeNegValue(String val)

		{
			decimal decodeVal ;
			if(val.StartsWith("(") && val.EndsWith(")"))
			{
				int lastindex = val.Length-2 ;
				decodeVal = -Convert.ToDecimal(val.Substring(1,lastindex));
			}
			else
			{
				decodeVal = Convert.ToDecimal(val);
			}

			return decodeVal;
		}


		#endregion

		#region "For AutoCreate Processing"

		public static void CreateAutoConsignmentHistory(String strAppID, String strEnterpriseID, 
			int process_id, String remark, long booking_no, String consignment_no, 
			DateTime status_dt, String UserID,
			ref IDbCommand dbCmdCons, ref DbConnection dbConCons)
		{
			try
			{	
				StringBuilder strBuild = new StringBuilder();
				int iRowsAffected = 0;
			
				if (process_id > 0)
				{
					strBuild = new StringBuilder();
					strBuild.Append("SELECT * ");
					strBuild.Append("FROM status_code WITH(nolock) ");
					strBuild.Append("WHERE applicationid = '"+strAppID+"' ");
					strBuild.Append("AND enterpriseid = '"+strEnterpriseID+"' ");
					strBuild.Append("AND auto_process = "+process_id);

					dbCmdCons.CommandText = strBuild.ToString();
					DataSet dsStatusCode = (DataSet)dbConCons.ExecuteQueryInTransaction(dbCmdCons, ReturnType.DataSetType);  

					if ((dsStatusCode != null) && 
						(!dsStatusCode.GetType().Equals(System.Type.GetType("System.DBNull"))) && 
						(dsStatusCode.Tables[0].Rows.Count > 0))
					{
						DataRow dr = dsStatusCode.Tables[0].Rows[0];
						String strStatusCode = "";
						String strStatusType = "";
						
						if(dr["status_code"].ToString().Trim() != "" && dr["status_code"] != null)
							strStatusCode = dr["status_code"].ToString();
				
						if(dr["status_type"].ToString().Trim() != "" && dr["status_type"] != null)
							strStatusType = dr["status_type"].ToString();

						if(strStatusType == "P")
						{
							if (booking_no > 0)
							{
								bool isExistingBooking = false;

								strBuild = new StringBuilder();
								//								strBuild.Append("SELECT * ");
								//								strBuild.Append("FROM Pickup_Request WITH(nolock) ");
								//								strBuild.Append("WHERE applicationid = '"+strAppID+"' ");
								//								strBuild.Append("AND enterpriseid = '"+strEnterpriseID+"' ");
								//								strBuild.Append("AND booking_no = "+booking_no);

								strBuild.Append("SELECT top 1 * ");
								strBuild.Append("FROM Pickup_Request WITH(nolock) ");
								strBuild.Append("WHERE applicationid = '"+strAppID+"' ");
								strBuild.Append("AND enterpriseid = '"+strEnterpriseID+"' ");
								strBuild.Append("AND booking_no = "+booking_no);

								dbCmdCons.CommandText = strBuild.ToString();
								DataSet dsPickupRequest = (DataSet)dbConCons.ExecuteQueryInTransaction(dbCmdCons, ReturnType.DataSetType);  

								if (dsPickupRequest.Tables[0].Rows.Count > 0)
									isExistingBooking = true;

								if (isExistingBooking == true)
								{
									strBuild = new StringBuilder();
									strBuild.Append("insert into Dispatch_Tracking WITH(rowlock) (applicationid, enterpriseid, tracking_datetime, ");
									strBuild.Append("booking_no, status_code, exception_code, person_incharge, remark, ");
									strBuild.Append("deleted, delete_remark, last_userid, last_updated, location, consignee_name)values('");
									strBuild.Append(strAppID+"','"+strEnterpriseID+"'");
									strBuild.Append(",");
									
									strBuild.Append(Utility.DateFormat(strAppID, strEnterpriseID, status_dt, DTFormat.DateLongTime));
									strBuild.Append(",");
									
									strBuild.Append(booking_no);
									strBuild.Append(",");
									
									strBuild.Append("'");
									strBuild.Append(strStatusCode);
									strBuild.Append("'");
									strBuild.Append(",");

									strBuild.Append("null");
									strBuild.Append(",");

									strBuild.Append("null");
									strBuild.Append(",");

									strBuild.Append("'");
									strBuild.Append(remark);
									strBuild.Append("'");
									strBuild.Append(",");

									strBuild.Append("null");
									strBuild.Append(",");

									strBuild.Append("null");
									strBuild.Append(",");

									if(UserID.Length>0)
									{
										strBuild.Append("'");
										strBuild.Append(UserID);
										strBuild.Append("'");
									}
									else
										strBuild.Append("null");
									
									strBuild.Append(",");
									
									strBuild.Append(Utility.DateFormat(strAppID, strEnterpriseID, System.DateTime.Now, DTFormat.DateTime));
									strBuild.Append(",");

									if((dr["auto_location"]!= null) && 
										(!dr["auto_location"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
										(dr["auto_location"].ToString() != ""))
									{
										String strauto_location = (String)dr["auto_location"];
										if (strauto_location.Trim() == "Y")
										{
											StringBuilder strBuildUser = new StringBuilder();
											strBuildUser.Append("SELECT * ");
											strBuildUser.Append("FROM User_master WITH(nolock) ");
											strBuildUser.Append("WHERE applicationid = '"+strAppID+"' ");
											strBuildUser.Append("AND enterpriseid = '"+strEnterpriseID+"' ");
											strBuildUser.Append("AND userid = '"+UserID+"'");

											dbCmdCons.CommandText = strBuildUser.ToString();
											DataSet dsUser = (DataSet)dbConCons.ExecuteQueryInTransaction(dbCmdCons, ReturnType.DataSetType);  

											if ((dsUser != null) && 
												(!dsUser.GetType().Equals(System.Type.GetType("System.DBNull"))) && 
												(dsUser.Tables[0].Rows.Count > 0))
											{
												if (dsUser.Tables[0].Rows[0]["location"].ToString().Trim() != "")
												{
													//String strLocation = (String)dr["location"];
													//By Aoo
													String strLocation  = (String)dsUser.Tables[0].Rows[0]["location"].ToString();
													strBuild.Append("'");
													strBuild.Append(strLocation);
													strBuild.Append("'");
												}
												else
												{
													strBuild.Append("null");
												}
											}
											else
											{
												strBuild.Append("null");
											}
										}
										else
										{
											strBuild.Append("null");
										}
									}
									else
									{
										strBuild.Append("null");
									}
								
									strBuild.Append(",");

									strBuild.Append("null");
									strBuild.Append(")");


									dbCmdCons.CommandText = strBuild.ToString();
									iRowsAffected = dbConCons.ExecuteNonQueryInTransaction(dbCmdCons);
								}
							}
						}
						else
						{
							bool isExistingCon = false;

							strBuild = new StringBuilder();
							//strBuild.Append("SELECT * ");
							strBuild.Append("SELECT top 1 * ");
							strBuild.Append("FROM Shipment WITH(nolock) ");
							strBuild.Append("WHERE applicationid = '"+strAppID+"' ");
							strBuild.Append("AND enterpriseid = '"+strEnterpriseID+"' ");
							strBuild.Append("AND booking_no = "+booking_no+" ");
							strBuild.Append("AND consignment_no = '"+consignment_no+"'");

							dbCmdCons.CommandText = strBuild.ToString();
							DataSet dsShipment = (DataSet)dbConCons.ExecuteQueryInTransaction(dbCmdCons, ReturnType.DataSetType);  

							if (dsShipment.Tables[0].Rows.Count > 0)
								isExistingCon = true;

							if (isExistingCon == true)
							{
								strBuild = new StringBuilder();

								//ADD BY X APR 16 08

								//strBuild.Append("select * from shipment_tracking WITH(nolock) ");
								strBuild.Append("select top 1 * from shipment_tracking WITH(nolock) ");
								strBuild.Append("WHERE booking_no = "+booking_no+" ");
								strBuild.Append("and consignment_no = '"+consignment_no+"' ");
								strBuild.Append("and status_code = '"+ strStatusCode +"' ");
								strBuild.Append(" and tracking_datetime = "+Utility.DateFormat(strAppID, strEnterpriseID, status_dt, DTFormat.DateLongTime));


								dbCmdCons.CommandText = strBuild.ToString();
								iRowsAffected = dbConCons.ExecuteNonQueryInTransaction(dbCmdCons);

								if(iRowsAffected>0)
								{
									strBuild = new StringBuilder();
									strBuild.Append("delete FROM Shipment_tracking WITH(rowlock) ");
									strBuild.Append("WHERE booking_no = "+booking_no+" ");
									strBuild.Append("and consignment_no = '"+consignment_no+"' ");
									strBuild.Append("and status_code = '"+ strStatusCode +"' ");
									strBuild.Append(" and tracking_datetime = "+Utility.DateFormat(strAppID, strEnterpriseID, status_dt, DTFormat.DateLongTime));
									dbCmdCons.CommandText = strBuild.ToString();
									iRowsAffected = dbConCons.ExecuteNonQueryInTransaction(dbCmdCons);

								}

								strBuild = new StringBuilder();
								//END ADD BY X APR 16 08

								//strBuild.Append("insert into Shipment_Tracking(applicationid, enterpriseid, consignment_no, tracking_datetime, ");
								strBuild.Append("insert into MF_Shipment_Tracking_Staging WITH(rowlock) (applicationid, enterpriseid, consignment_no, tracking_datetime, ");
								strBuild.Append("booking_no, status_code, exception_code, person_incharge, remark, deleted, delete_remark, ");
								strBuild.Append("last_userid, last_updated, invoiceable, reason, surcharge, consignee_name, location, pkg_no)values('");
								strBuild.Append(strAppID+"','"+strEnterpriseID+"'");
								strBuild.Append(",");

								strBuild.Append("'");
								strBuild.Append(consignment_no);
								strBuild.Append("'");
								strBuild.Append(",");
									
								//strBuild.Append(Utility.DateFormat(strAppID, strEnterpriseID, status_dt, DTFormat.DateLongTime));
								//Stamp tracking datetime by using database time
								strBuild.Append("getdate()");
								strBuild.Append(",");
									
								strBuild.Append(booking_no);
								strBuild.Append(",");
									
								strBuild.Append("'");
								strBuild.Append(strStatusCode);
								strBuild.Append("'");
								strBuild.Append(",");

								strBuild.Append("null");
								strBuild.Append(",");

								strBuild.Append("null");
								strBuild.Append(",");

								strBuild.Append("'");
								strBuild.Append(remark);
								strBuild.Append("'");
								strBuild.Append(",");

								//edit by Tumz.
								strBuild.Append("'N'");
								strBuild.Append(",");

								strBuild.Append("null");
								strBuild.Append(",");

								strBuild.Append("'");
								strBuild.Append(UserID);
								strBuild.Append("'");
								strBuild.Append(",");

								strBuild.Append(Utility.DateFormat(strAppID, strEnterpriseID, System.DateTime.Now.AddSeconds(status_dt.Second), DTFormat.DateLongTime));
								strBuild.Append(",");

								if((dr["invoiceable"]!= null) && 
									(!dr["invoiceable"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
									(dr["invoiceable"].ToString() != ""))
								{
									String strinvoiceable = (String) dr["invoiceable"];
									strBuild.Append("'");
									strBuild.Append(strinvoiceable);
									strBuild.Append("'");}
								else
								{
									strBuild.Append("null");
								}
								strBuild.Append(",");
								strBuild.Append("null");
								strBuild.Append(",");

								strBuild.Append("null");
								strBuild.Append(",");

								strBuild.Append("null");
								strBuild.Append(",");

								if((dr["auto_location"]!= null) && 
									(!dr["auto_location"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
									(dr["auto_location"].ToString() != ""))
								{
									String strauto_location = (String)dr["auto_location"];
									if (strauto_location.Trim() == "Y")
									{
										StringBuilder strBuildUser = new StringBuilder();
										strBuildUser.Append("SELECT * ");
										strBuildUser.Append("FROM User_master WITH(nolock) ");
										strBuildUser.Append("WHERE applicationid = '"+strAppID+"' ");
										strBuildUser.Append("AND enterpriseid = '"+strEnterpriseID+"' ");
										strBuildUser.Append("AND userid = '"+UserID+"'");

										dbCmdCons.CommandText = strBuildUser.ToString();
										DataSet dsUser = (DataSet)dbConCons.ExecuteQueryInTransaction(dbCmdCons, ReturnType.DataSetType);  

										if ((dsUser != null) && 
											(!dsUser.GetType().Equals(System.Type.GetType("System.DBNull"))) && 
											(dsUser.Tables[0].Rows.Count > 0))
										{
											if (dsUser.Tables[0].Rows[0]["location"].ToString().Trim() != "")
											{
												//String strLocation = (String)dr["location"];
												//By Aoo
												String strLocation  = (String)dsUser.Tables[0].Rows[0]["location"].ToString();
												strBuild.Append("'");
												strBuild.Append(strLocation);
												strBuild.Append("'");
											}
											else
											{
												strBuild.Append("''");
											}
										}
										else
										{
											strBuild.Append("''");
										}
									}
									else
									{
										strBuild.Append("''");
									}
								}
								else
								{
									strBuild.Append("''");
								}
								// pkg_no column
								strBuild.Append(",0");
								
								strBuild.Append(")");

								dbCmdCons.CommandText = strBuild.ToString();
								iRowsAffected = dbConCons.ExecuteNonQueryInTransaction(dbCmdCons);
							}
						}
					}
				}
			}
			catch(Exception exception)
			{
				throw exception;
			}
		}


		//Create by Aoo 26/03/2010
		public static void CreateAutoConsignmentHistory(String strAppID, String strEnterpriseID, 
			int process_id, String remark, long booking_no, String consignment_no, 
			DateTime status_dt, String UserID,
			ref IDbCommand dbCmdCons, ref DbConnection dbConCons, ref IDbConnection conApp, ref IDbTransaction tranApp)
		{
			try
			{	
				//dbConCons.OpenConnection(ref conApp);
				//tranApp = dbConCons.BeginTransaction(conApp, IsolationLevel.ReadCommitted);

				StringBuilder strBuild = new StringBuilder();
				int iRowsAffected = 0;
			
				if (process_id > 0)
				{
					strBuild = new StringBuilder();
					strBuild.Append("SELECT * ");
					strBuild.Append("FROM status_code with(rowlock) ");
					strBuild.Append("WHERE applicationid = '"+strAppID+"' ");
					strBuild.Append("AND enterpriseid = '"+strEnterpriseID+"' ");
					strBuild.Append("AND auto_process = "+process_id);

					dbCmdCons = dbConCons.CreateCommand(strBuild.ToString(),CommandType.Text);
					dbCmdCons.Connection = conApp;
					dbCmdCons.Transaction = tranApp;

					DataSet dsStatusCode = (DataSet)dbConCons.ExecuteQueryInTransaction(dbCmdCons, ReturnType.DataSetType);  

					if ((dsStatusCode != null) && 
						(!dsStatusCode.GetType().Equals(System.Type.GetType("System.DBNull"))) && 
						(dsStatusCode.Tables[0].Rows.Count > 0))
					{
						DataRow dr = dsStatusCode.Tables[0].Rows[0];
						String strStatusCode = "";
						String strStatusType = "";
						
						if(dr["status_code"].ToString().Trim() != "" && dr["status_code"] != null)
							strStatusCode = dr["status_code"].ToString();
				
						if(dr["status_type"].ToString().Trim() != "" && dr["status_type"] != null)
							strStatusType = dr["status_type"].ToString();

						if(strStatusType == "P")
						{
							if (booking_no > 0)
							{
								bool isExistingBooking = false;

								strBuild = new StringBuilder();
								strBuild.Append("SELECT * ");
								strBuild.Append("FROM Pickup_Request with(rowlock) ");
								strBuild.Append("WHERE applicationid = '"+strAppID+"' ");
								strBuild.Append("AND enterpriseid = '"+strEnterpriseID+"' ");
								strBuild.Append("AND booking_no = "+booking_no);

								dbCmdCons = dbConCons.CreateCommand(strBuild.ToString(),CommandType.Text);
								dbCmdCons.Connection = conApp;
								dbCmdCons.Transaction = tranApp;

								DataSet dsPickupRequest = (DataSet)dbConCons.ExecuteQueryInTransaction(dbCmdCons, ReturnType.DataSetType);  

								if (dsPickupRequest.Tables[0].Rows.Count > 0)
									isExistingBooking = true;

								if (isExistingBooking == true)
								{
									strBuild = new StringBuilder();
									strBuild.Append("insert into Dispatch_Tracking with(rowlock)(applicationid, enterpriseid, tracking_datetime, ");
									strBuild.Append("booking_no, status_code, exception_code, person_incharge, remark, ");
									strBuild.Append("deleted, delete_remark, last_userid, last_updated, location, consignee_name)values('");
									strBuild.Append(strAppID+"','"+strEnterpriseID+"'");
									strBuild.Append(",");
									
									strBuild.Append(Utility.DateFormat(strAppID, strEnterpriseID, status_dt, DTFormat.DateLongTime));
									strBuild.Append(",");
									
									strBuild.Append(booking_no);
									strBuild.Append(",");
									
									strBuild.Append("'");
									strBuild.Append(strStatusCode);
									strBuild.Append("'");
									strBuild.Append(",");

									strBuild.Append("null");
									strBuild.Append(",");

									strBuild.Append("null");
									strBuild.Append(",");

									strBuild.Append("'");
									strBuild.Append(remark);
									strBuild.Append("'");
									strBuild.Append(",");

									strBuild.Append("null");
									strBuild.Append(",");

									strBuild.Append("null");
									strBuild.Append(",");

									if(UserID.Length>0)
									{
										strBuild.Append("'");
										strBuild.Append(UserID);
										strBuild.Append("'");
									}
									else
										strBuild.Append("null");
									
									strBuild.Append(",");
									
									strBuild.Append(Utility.DateFormat(strAppID, strEnterpriseID, System.DateTime.Now, DTFormat.DateTime));
									strBuild.Append(",");

									if((dr["auto_location"]!= null) && 
										(!dr["auto_location"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
										(dr["auto_location"].ToString() != ""))
									{
										String strauto_location = (String)dr["auto_location"];
										if (strauto_location.Trim() == "Y")
										{
											StringBuilder strBuildUser = new StringBuilder();
											strBuildUser.Append("SELECT * ");
											strBuildUser.Append("FROM User_master with(rowlock) ");
											strBuildUser.Append("WHERE applicationid = '"+strAppID+"' ");
											strBuildUser.Append("AND enterpriseid = '"+strEnterpriseID+"' ");
											strBuildUser.Append("AND userid = '"+UserID+"'");

											dbCmdCons = dbConCons.CreateCommand(strBuildUser.ToString(),CommandType.Text);
											dbCmdCons.Connection = conApp;
											dbCmdCons.Transaction = tranApp;

											DataSet dsUser = (DataSet)dbConCons.ExecuteQueryInTransaction(dbCmdCons, ReturnType.DataSetType);  

											if ((dsUser != null) && 
												(!dsUser.GetType().Equals(System.Type.GetType("System.DBNull"))) && 
												(dsUser.Tables[0].Rows.Count > 0))
											{
												if (dsUser.Tables[0].Rows[0]["location"].ToString().Trim() != "")
												{
													//String strLocation = (String)dr["location"];
													//By Aoo
													String strLocation  = (String)dsUser.Tables[0].Rows[0]["location"].ToString();
													strBuild.Append("'");
													strBuild.Append(strLocation);
													strBuild.Append("'");
												}
												else
												{
													strBuild.Append("null");
												}
											}
											else
											{
												strBuild.Append("null");
											}
										}
										else
										{
											strBuild.Append("null");
										}
									}
									else
									{
										strBuild.Append("null");
									}
								
									strBuild.Append(",");

									strBuild.Append("null");
									strBuild.Append(")");

									dbCmdCons = dbConCons.CreateCommand(strBuild.ToString(),CommandType.Text);
									dbCmdCons.Connection = conApp;
									dbCmdCons.Transaction = tranApp;

									iRowsAffected = dbConCons.ExecuteNonQueryInTransaction(dbCmdCons);
								}
							}
						}
						else
						{
							bool isExistingCon = false;

							strBuild = new StringBuilder();
							strBuild.Append("SELECT * ");
							strBuild.Append("FROM Shipment with(rowlock) ");
							strBuild.Append("WHERE applicationid = '"+strAppID+"' ");
							strBuild.Append("AND enterpriseid = '"+strEnterpriseID+"' ");
							strBuild.Append("AND booking_no = "+booking_no+" ");
							strBuild.Append("AND consignment_no = '"+consignment_no+"'");

							dbCmdCons = dbConCons.CreateCommand(strBuild.ToString(),CommandType.Text);
							dbCmdCons.Connection = conApp;
							dbCmdCons.Transaction = tranApp;

							DataSet dsShipment = (DataSet)dbConCons.ExecuteQueryInTransaction(dbCmdCons, ReturnType.DataSetType);  

							if (dsShipment.Tables[0].Rows.Count > 0)
								isExistingCon = true;

							if (isExistingCon == true)
							{
								strBuild = new StringBuilder();

								//ADD BY X APR 16 08

								strBuild.Append("select * from shipment_tracking with(rowlock) ");
								strBuild.Append("WHERE booking_no = "+booking_no+" ");
								strBuild.Append("and consignment_no = '"+consignment_no+"' ");
								strBuild.Append("and status_code = '"+ strStatusCode +"' ");
								strBuild.Append(" and tracking_datetime = "+Utility.DateFormat(strAppID, strEnterpriseID, status_dt, DTFormat.DateLongTime));


								dbCmdCons.CommandText = strBuild.ToString();
								iRowsAffected = dbConCons.ExecuteNonQueryInTransaction(dbCmdCons);

								if(iRowsAffected>0)
								{
									strBuild = new StringBuilder();
									strBuild.Append("delete FROM Shipment_tracking with(rowlock) ");
									strBuild.Append("WHERE booking_no = "+booking_no+" ");
									strBuild.Append("and consignment_no = '"+consignment_no+"' ");
									strBuild.Append("and status_code = '"+ strStatusCode +"' ");
									strBuild.Append(" and tracking_datetime = "+Utility.DateFormat(strAppID, strEnterpriseID, status_dt, DTFormat.DateLongTime));
									dbCmdCons.CommandText = strBuild.ToString();
									iRowsAffected = dbConCons.ExecuteNonQueryInTransaction(dbCmdCons);

								}

								strBuild = new StringBuilder();
								//END ADD BY X APR 16 08

								strBuild.Append("insert into Shipment_Tracking with(rowlock)(applicationid, enterpriseid, consignment_no, tracking_datetime, ");
								strBuild.Append("booking_no, status_code, exception_code, person_incharge, remark, deleted, delete_remark, ");
								strBuild.Append("last_userid, last_updated, invoiceable, reason, surcharge, consignee_name, location)values('");
								strBuild.Append(strAppID+"','"+strEnterpriseID+"'");
								strBuild.Append(",");

								strBuild.Append("'");
								strBuild.Append(consignment_no);
								strBuild.Append("'");
								strBuild.Append(",");
									
								strBuild.Append(Utility.DateFormat(strAppID, strEnterpriseID, status_dt, DTFormat.DateLongTime));
								strBuild.Append(",");
									
								strBuild.Append(booking_no);
								strBuild.Append(",");
									
								strBuild.Append("'");
								strBuild.Append(strStatusCode);
								strBuild.Append("'");
								strBuild.Append(",");

								strBuild.Append("null");
								strBuild.Append(",");

								strBuild.Append("null");
								strBuild.Append(",");

								strBuild.Append("'");
								strBuild.Append(remark);
								strBuild.Append("'");
								strBuild.Append(",");

								strBuild.Append("null");
								strBuild.Append(",");

								strBuild.Append("null");
								strBuild.Append(",");

								strBuild.Append("'");
								strBuild.Append(UserID);
								strBuild.Append("'");
								strBuild.Append(",");

								strBuild.Append(Utility.DateFormat(strAppID, strEnterpriseID, System.DateTime.Now, DTFormat.DateLongTime));
								strBuild.Append(",");

								if((dr["invoiceable"]!= null) && 
									(!dr["invoiceable"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
									(dr["invoiceable"].ToString() != ""))
								{
									String strinvoiceable = (String) dr["invoiceable"];
									strBuild.Append("'");
									strBuild.Append(strinvoiceable);
									strBuild.Append("'");}
								else
								{
									strBuild.Append("null");
								}
								strBuild.Append(",");
								strBuild.Append("null");
								strBuild.Append(",");

								strBuild.Append("null");
								strBuild.Append(",");

								strBuild.Append("null");
								strBuild.Append(",");

								if((dr["auto_location"]!= null) && 
									(!dr["auto_location"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
									(dr["auto_location"].ToString() != ""))
								{
									String strauto_location = (String)dr["auto_location"];
									if (strauto_location.Trim() == "Y")
									{
										StringBuilder strBuildUser = new StringBuilder();
										strBuildUser.Append("SELECT * ");
										strBuildUser.Append("FROM User_master with(rowlock) ");
										strBuildUser.Append("WHERE applicationid = '"+strAppID+"' ");
										strBuildUser.Append("AND enterpriseid = '"+strEnterpriseID+"' ");
										strBuildUser.Append("AND userid = '"+UserID+"'");

										dbCmdCons.CommandText = strBuildUser.ToString();
										DataSet dsUser = (DataSet)dbConCons.ExecuteQueryInTransaction(dbCmdCons, ReturnType.DataSetType);  

										if ((dsUser != null) && 
											(!dsUser.GetType().Equals(System.Type.GetType("System.DBNull"))) && 
											(dsUser.Tables[0].Rows.Count > 0))
										{
											if (dsUser.Tables[0].Rows[0]["location"].ToString().Trim() != "")
											{
												//String strLocation = (String)dr["location"];
												//By Aoo
												String strLocation  = (String)dsUser.Tables[0].Rows[0]["location"].ToString();
												strBuild.Append("'");
												strBuild.Append(strLocation);
												strBuild.Append("'");
											}
											else
											{
												strBuild.Append("null");
											}
										}
										else
										{
											strBuild.Append("null");
										}
									}
									else
									{
										strBuild.Append("null");
									}
								}
								else
								{
									strBuild.Append("null");
								}
								strBuild.Append(")");

								dbCmdCons = dbConCons.CreateCommand(strBuild.ToString(),CommandType.Text);
								dbCmdCons.Connection = conApp;
								dbCmdCons.Transaction = tranApp;

								iRowsAffected = dbConCons.ExecuteNonQueryInTransaction(dbCmdCons);
							}
						}
					}
				}
			}
			catch(Exception exception)
			{
				throw exception;
			}
		}


		public static void DeleteAutoConsignmentHistory(String strAppID, String strEnterpriseID, 
			int process_id, String remark, int booking_no, String consignment_no, 
			DateTime status_dt, String UserID,
			ref IDbCommand dbCmdCons, ref DbConnection dbConCons)
		{
			try
			{	
				StringBuilder strBuild = new StringBuilder();
				int iRowsAffected = 0;
			
				if (process_id > 0)
				{
					strBuild = new StringBuilder();
					strBuild.Append("SELECT * ");
					strBuild.Append("FROM status_code ");
					strBuild.Append("WHERE applicationid = '"+strAppID+"' ");
					strBuild.Append("AND enterpriseid = '"+strEnterpriseID+"' ");
					strBuild.Append("AND auto_process = "+process_id);

					dbCmdCons.CommandText = strBuild.ToString();
					DataSet dsStatusCode = (DataSet)dbConCons.ExecuteQueryInTransaction(dbCmdCons, ReturnType.DataSetType);  




					if ((dsStatusCode != null) && 
						(!dsStatusCode.GetType().Equals(System.Type.GetType("System.DBNull"))) && 
						(dsStatusCode.Tables[0].Rows.Count > 0))
					{
						DataRow dr = dsStatusCode.Tables[0].Rows[0];
						String strStatusCode = "";
						String strStatusType = "";
						
						if(dr["status_code"].ToString().Trim() != "" && dr["status_code"] != null)
							strStatusCode = dr["status_code"].ToString();
				
						if(dr["status_type"].ToString().Trim() != "" && dr["status_type"] != null)
							strStatusType = dr["status_type"].ToString();

						if(strStatusType == "P")
						{
							strBuild = new StringBuilder();
							strBuild.Append("UPDATE Dispatch_tracking ");
							strBuild.Append("set Dispatch_tracking.deleted = 'N', ");
							strBuild.Append("Dispatch_tracking.delete_remark = '"+remark+"', ");
							strBuild.Append("Dispatch_tracking.last_userid = '"+UserID+"', ");
							strBuild.Append("Dispatch_tracking.last_updated = " + Utility.DateFormat(strAppID, strEnterpriseID, System.DateTime.Now, DTFormat.DateTime));
							strBuild.Append("From Dispatch_tracking ");
							strBuild.Append("INNER JOIN (Select applicationid, enterpriseid, booking_no, status_code, rtrim(ltrim(isnull(exception_code, ''))) as exception_code, max(tracking_datetime) as tracking_datetime ");
							strBuild.Append("FROM Dispatch_tracking ");
							strBuild.Append("WHERE booking_no = "+booking_no+" ");
							strBuild.Append("and status_code = '"+ strStatusCode +"' ");
							strBuild.Append("group by applicationid, enterpriseid, booking_no, status_code, rtrim(ltrim(isnull(exception_code, '')))) TMP_Dispatch ");
							strBuild.Append("on (Dispatch_tracking.applicationid = TMP_Dispatch.applicationid ");
							strBuild.Append("and Dispatch_tracking.enterpriseid = TMP_Dispatch.enterpriseid ");
							strBuild.Append("and Dispatch_tracking.booking_no = TMP_Dispatch.booking_no ");
							strBuild.Append("and Dispatch_tracking.status_code = TMP_Dispatch.status_code ");
							strBuild.Append("and rtrim(ltrim(isnull(Dispatch_tracking.exception_code, ''))) = rtrim(ltrim(isnull(TMP_Dispatch.exception_code, ''))) ");
							strBuild.Append("and Dispatch_tracking.tracking_datetime = TMP_Dispatch.tracking_datetime) ");

							dbCmdCons.CommandText = strBuild.ToString();
							iRowsAffected = dbConCons.ExecuteNonQueryInTransaction(dbCmdCons);
						}
						else
						{
							strBuild = new StringBuilder();
							strBuild.Append("UPDATE Shipment_tracking ");
							strBuild.Append("set Shipment_tracking.deleted = 'Y', ");
							strBuild.Append("Shipment_tracking.delete_remark = '"+remark+"', ");
							strBuild.Append("Shipment_tracking.last_userid = '"+UserID+"', "); 
							strBuild.Append("Shipment_tracking.last_updated = "+Utility.DateFormat(strAppID, strEnterpriseID, System.DateTime.Now, DTFormat.DateLongTime));
							strBuild.Append("From Shipment_tracking ");
							strBuild.Append("INNER JOIN (Select applicationid, enterpriseid, booking_no, consignment_no, status_code, rtrim(ltrim(isnull(exception_code, ''))) as exception_code, max(tracking_datetime) as tracking_datetime ");
							strBuild.Append("FROM Shipment_tracking ");
							strBuild.Append("WHERE booking_no = "+booking_no+" ");
							strBuild.Append("and consignment_no = '"+consignment_no+"' ");
							strBuild.Append("and status_code = '"+ strStatusCode +"' ");
							strBuild.Append("group by applicationid, enterpriseid, booking_no, consignment_no, status_code, rtrim(ltrim(isnull(exception_code, '')))) TMP_Shipment ");
							strBuild.Append("on (Shipment_tracking.applicationid = TMP_Shipment.applicationid ");
							strBuild.Append("and Shipment_tracking.enterpriseid = TMP_Shipment.enterpriseid ");
							strBuild.Append("and Shipment_tracking.booking_no = TMP_Shipment.booking_no ");
							strBuild.Append("and Shipment_tracking.consignment_no = TMP_Shipment.consignment_no ");
							strBuild.Append("and Shipment_tracking.status_code = TMP_Shipment.status_code ");
							strBuild.Append("and rtrim(ltrim(isnull(Shipment_tracking.exception_code, ''))) = rtrim(ltrim(isnull(TMP_Shipment.exception_code, ''))) ");
							strBuild.Append("and Shipment_tracking.tracking_datetime = TMP_Shipment.tracking_datetime) ");

							dbCmdCons.CommandText = strBuild.ToString();
							iRowsAffected = dbConCons.ExecuteNonQueryInTransaction(dbCmdCons);
						}
					}
				}
			}
			catch(Exception exception)
			{
				throw exception;
			}
		}




		public static void DeleteAutoConsignmentHistory2(String strAppID, String strEnterpriseID, 
			int process_id, String remark, int booking_no, String consignment_no, 
			int iFlag,String strStatusCode, String UserID,
			ref IDbCommand dbCmdCons, ref DbConnection dbConCons)
		{
			try
			{	
				StringBuilder strBuild = new StringBuilder();
				int iRowsAffected = 0;
			
				if (process_id > 0)
				{
					strBuild = new StringBuilder();

					strBuild.Append("UPDATE Shipment_tracking ");
					strBuild.Append("set Shipment_tracking.deleted = 'Y', ");
					strBuild.Append("Shipment_tracking.delete_remark = '"+remark+"', ");
					strBuild.Append("Shipment_tracking.last_userid = '"+UserID+"', "); 
					strBuild.Append("Shipment_tracking.last_updated = "+Utility.DateFormat(strAppID, strEnterpriseID, System.DateTime.Now, DTFormat.DateLongTime));

					strBuild.Append("FROM Shipment_tracking ");
					strBuild.Append("WHERE booking_no = "+booking_no+" ");
					strBuild.Append("and consignment_no = '"+consignment_no+"' ");
					strBuild.Append("and status_code = '"+ strStatusCode +"' ");
					strBuild.Append(" and RIGHT(DATENAME(s,tracking_datetime),1) ="+iFlag);
					//strBuild.Append(" and tracking_datetime = "+Utility.DateFormat(strAppID, strEnterpriseID, dtTimeToDelete, DTFormat.DateLongTime));

					dbCmdCons.CommandText = strBuild.ToString();
					iRowsAffected = dbConCons.ExecuteNonQueryInTransaction(dbCmdCons);
				}

			}
			catch(Exception exception)
			{
				throw exception;
			}
		}




		#endregion

		#region "For check string value in number format"

		public static bool isNumeric(string val, System.Globalization.NumberStyles NumberStyle)
		{
			Double result;
			return Double.TryParse(val,NumberStyle,
				System.Globalization.CultureInfo.CurrentCulture,out result);
		}
		
		public static bool isNumeric(string val, System.Globalization.NumberStyles NumberStyle, string CultureName)
		{
			Double result;
			return Double.TryParse(val,NumberStyle,new System.Globalization.CultureInfo
				(CultureName),out result);
		}

		#endregion
		
		#region Utility for module ConsignmentNote Add By Lin 2/6/2010

		public static ArrayList GetListConsServiceType(String strAppID, String strEnterpriseID, String strPostCode)
		{
			ArrayList codeValues = null;

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID);

			if(dbCon == null)
			{
				Logger.LogTraceError("Utility","GetServiceValues","EDB101","DbConnection object is null!!");
				System.Diagnostics.Trace.WriteLineIf(Logger.AppSwitch.TraceError,"[Utility::GetCodeValues()] DbConnection is null","ERROR");
				return codeValues;
			}

			String strSQLQuery = "SELECT * FROM Service ";
		
			strSQLQuery += "WHERE APPLICATIONID = '"+strAppID+"' AND ENTERPRISEID = '"+strEnterpriseID+"' "; 
			strSQLQuery += "and service_code not in (select distinct(service_code) from Zipcode_Service_Excluded where zipcode = '"+strPostCode+"')";
			strSQLQuery += "ORDER BY convert(FLOAT,REPLACE(CONVERT(CHAR(5),commit_time,8),':','.')) + transit_day * 24 ";
		
			DataSet dsServiceCode = (DataSet)dbCon.ExecuteQuery(strSQLQuery,ReturnType.DataSetType);

			int cnt = dsServiceCode.Tables[0].Rows.Count;

			if(cnt > 0)
			{
				codeValues = new ArrayList();
			
				for(int i=0; i < cnt; i++)
				{
					DataRow drEach = dsServiceCode.Tables[0].Rows[i];
					SystemCode systemCode = new SystemCode();

					if(!drEach["service_code"].GetType().Equals(System.Type.GetType("System.DBNull")))
					{
						systemCode.Text = (String) drEach["service_code"];
						systemCode.StringValue = (String) drEach["service_code"];
					}
					codeValues.Add(systemCode);
				}
			}

			return  codeValues;
		} 
		

		//For Check input is Integer?
		//if input = integer then Integer = true
		//if input = double, float then Integer = false
		public static bool CheckNumeric(String sText, bool beInteger)
		{
			String ValidChars = "";
			bool beNumber = true;
			
			try
			{
				if (beInteger == true)
				{
					ValidChars = "0123456789";
				}
				else
				{
					ValidChars = "0123456789.";
				}

				for (int i = 0; i < sText.Length && beNumber == true; i++)
				{ 
					char tmpChar = sText[i]; 
					if (ValidChars.IndexOf(tmpChar) == -1)  
					{
						beNumber = false;
					}
				}
			}
			catch (Exception ex)
			{
				beNumber = false;
			}
			return beNumber;
		}


		public static String GetFileName(String FileName)
		{
			if (FileName != "")
			{
				return FileName.Substring(FileName.LastIndexOf("\\") +1);
			}
			else
			{
				return "";
			}
		}

		
		//For gen DescriptionBox of ConsignmentDetail
		public static string BoxDescFormat(String Size)
		{
			String SizeFormat = "";
			int MaxLenght = 0;

			try
			{
				MaxLenght = Size.Length;
				switch (MaxLenght)
				{
					case 0:
						SizeFormat = "   " + Size;
						break;
					case 1:
						SizeFormat = "  " + Size;
						break;
					case 2:
						SizeFormat = " " + Size;
						break;
					default:
						SizeFormat = Size;
						break;
				}
			}
			catch (Exception ex)
			{
				SizeFormat = "";
			}
			return SizeFormat;
		}

			
		public static bool ValidateConsignmentNo(string conno,ref string validconsigment)
		{
			bool result = true;
			validconsigment=conno;
			string m_strAppID = System.Configuration.ConfigurationSettings.AppSettings["applicationID"];
			string m_strEnterpriseID = System.Configuration.ConfigurationSettings.AppSettings["enterpriseID"];
			string[] tmpcon = conno.Split(' ');
			string strRegex=@"^[a-zA-Z0-9-/]{1,40}$";
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(m_strAppID);
			if(dbCon == null)
			{
				Logger.LogTraceError("Utility","GetServiceValues","EDB101","DbConnection object is null!!");
				System.Diagnostics.Trace.WriteLineIf(Logger.AppSwitch.TraceError,"[Utility::GetCodeValues()] DbConnection is null","ERROR");
				return false;
			}
			// ConsignmentAuditRule
			// ConsNoAuditRule
			string strSQLQuery = "SELECT code_num_value FROM dbo.Core_System_Code ";
			strSQLQuery += "WHERE codeid = 'ConsignmentAuditRule' AND culture = '"+ m_strEnterpriseID +"' AND sequence=1"; 		
			DataSet dsServiceCode = (DataSet)dbCon.ExecuteQuery(strSQLQuery,ReturnType.DataSetType);
			if(dsServiceCode.Tables[0].Rows.Count>0)
			{
				strRegex = dsServiceCode.Tables[0].Rows[0]["code_num_value"].ToString();
			}
			conno = tmpcon[0];
			//if (!Regex.IsMatch(conno, @"^[a-zA-Z0-9-/.\s]{1,40}$"))
			//if (!Regex.IsMatch(conno, @"^[0-9]{4}$"))
			if (!Regex.IsMatch(conno, strRegex))
			{
				strSQLQuery = "SELECT code_num_value FROM dbo.Core_System_Code ";
				strSQLQuery += "WHERE codeid = 'ConsNoAuditRule' AND culture = '"+ m_strEnterpriseID +"' AND sequence=1"; 		
				dsServiceCode = (DataSet)dbCon.ExecuteQuery(strSQLQuery,ReturnType.DataSetType);
				if(dsServiceCode.Tables[0].Rows.Count>0)
				{
					strRegex = dsServiceCode.Tables[0].Rows[0]["code_num_value"].ToString();
				}
				if (!Regex.IsMatch(conno, strRegex))
				{
					result = false;
				}				
			}
			else
			{
				validconsigment = validconsigment.Substring(2,validconsigment.Length-4);
			}

			return result;
		}


		public static bool GetConsNoRegex(string conno)
		{
			bool result = true;
//			if(conno != null && conno.Trim() !="")
//			{
//				string m_strAppID = System.Configuration.ConfigurationSettings.AppSettings["applicationID"];
//				string m_strEnterpriseID = System.Configuration.ConfigurationSettings.AppSettings["enterpriseID"];
//				string strRegex=@"^[a-zA-Z0-9-/]{1,40}$";
//				DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(m_strAppID);
//				if(dbCon == null)
//				{
//					Logger.LogTraceError("Utility","GetServiceValues","EDB101","DbConnection object is null!!");
//					System.Diagnostics.Trace.WriteLineIf(Logger.AppSwitch.TraceError,"[Utility::GetCodeValues()] DbConnection is null","ERROR");
//					return false;
//				}
//
//				string strQry = "select ConsNoRegex=code_num_value from dbo.Enterprise_Configurations WHERE applicationid = '"+m_strAppID+"'";
//				strQry += "And enterpriseid = '"+m_strEnterpriseID+"' AND codeid = 'ConsNoAuditRule'";
//
//				DataSet dsServiceCode = (DataSet)dbCon.ExecuteQuery(strQry,ReturnType.DataSetType);
//				if(dsServiceCode.Tables[0].Rows.Count>0)
//				{
//					strRegex = dsServiceCode.Tables[0].Rows[0]["ConsNoRegex"].ToString();
//				}
//
//				if (!Regex.IsMatch(conno, strRegex))
//				{
//					result = false;			
//				}
//			}
			return result;
		}

		public static bool ValidateEmail(string strEmail)
		{
			bool result = true;			
			string strRegex= @"[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}";
            string strRegex2 = @"[<>:;*|^]";//add regex to unexcept special char by pongsakorn.s
            if (!Regex.IsMatch(strEmail, strRegex))
            {
				result=false;				
			}
            else if(Regex.IsMatch(strEmail, strRegex2))//add regex to unexcept special char by pongsakorn.s
            {
                result = false;
            }
			return result;
		}


		public static bool ValidateFormat(string strValue, string strRegex)
		{
			bool result = true;			 		
			if (!Regex.IsMatch(strValue, strRegex))
			{
				result=false;				
			}
			return result;
		}
		#endregion

		public  String generateHTMLSpace(int cntSpace)
		{	
			string space ="";
			for(int i=0 ;i<cntSpace;i++)
			{
				space+=System.Web.HttpUtility.HtmlDecode("&nbsp;");
				//space+=" ";
			}
			return space;
		}

		public void SetInitialFocus(Control control) 
		{ 
			if (control.Page == null) 
			{ 
				throw new ArgumentException( 
					"The Control must be added to a Page before you can set the IntialFocus to it."); 
			} 
			if (control.Page.Request.Browser.JavaScript == true) 
			{ 
				// Create JavaScript 
				StringBuilder s = new StringBuilder(); 
				s.Append("\n<SCRIPT LANGUAGE='JavaScript'>\n"); 
				s.Append("<!--\n"); 
				s.Append("function SetInitialFocus()\n"); 
				s.Append("{\n"); 
				s.Append("   document."); 

				// Find the Form 
				Control p = control.Parent; 
				while (!(p is System.Web.UI.HtmlControls.HtmlForm)) 
					p = p.Parent; 
				s.Append(p.ClientID); 

				s.Append("['"); 
				s.Append(control.UniqueID); 
				s.Append("'].focus();\n"); 
				s.Append("}\n"); 
				s.Append("// -->\n"); 
				s.Append("window.onload = SetInitialFocus;\n"); 
				s.Append("</SCRIPT>"); 

				// Register Client Script 
				control.Page.RegisterClientScriptBlock("InitialFocus", s.ToString()); 
			} 
		}

		public bool CheckUserRoleLocation()
		{
			bool result = false;
			string strRoles = m_session["UserRole"].ToString();	
			string [] roles = strRoles.Split(';');
			foreach(string role in roles)
			{
				if(role.Trim() == "OPSSU")
				{
					result=true;
					break;
				}
				else if(role.Trim() =="CSSU")
				{
					result=true;
					break;
				}
			}
			return result;
		}

		public string UserLocation()
		{
			string UserLocation = m_session["UserLocation"].ToString();
			return UserLocation;
		}
		public string SendEmail(string body,string from,string mailTo,string ccTo,string subject,string smtpServer,string path)
		{
			string rtnErr="";
			try
			{
					MailMessage message = new MailMessage();

					message.To =  mailTo;
					message.From = from;
					message.Cc = ccTo;
					message.Body = body;
					message.Subject = subject;
					message.BodyFormat = MailFormat.Text;
				if (path != null)
				{
					MailAttachment attach = new MailAttachment(path);
					message.Attachments.Add(attach);
				}
					SmtpMail.SmtpServer = smtpServer;
					SmtpMail.Send(message);
			}
			catch(Exception ex)
			{
				// Thosapol Yennam 16/08/2013 Change Error Message 
				//rtnErr = "Can not send email.";
				rtnErr = ex.Message;
			}
				return rtnErr;

//        If Not args.AttachmentPaths Is Nothing Then
//            For Each path As String In args.AttachmentPaths
//                Dim attach As New Web.Mail.MailAttachment(path)
//                message.Attachments.Add(attach)
//            Next
//        End If

     
	
		}
		//add by Tanachon Mayoo
		//add date 2015-09-07
		//get key and value of  display datagrid on service page
		public static ArrayList GetKeyValues(string strAppID, string strEnterpriseID,string strConstant)
		{
			ArrayList codeValues = null;

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID);

			if(dbCon == null)
			{
				Logger.LogTraceError("Utility","GetKeyValues","EDB101","DbConnection object is null!!");
				System.Diagnostics.Trace.WriteLineIf(Logger.AppSwitch.TraceError,"[Utility::GetKeyValues()] DbConnection is null","ERROR");
				return codeValues;
			}
			
			String strSQLQuery = "select [key], [value] from  dbo.EnterpriseConfigurations('"+strEnterpriseID+ "','"+strConstant+"')";
			
			
			
			DataSet dsKeyCode = (DataSet)dbCon.ExecuteQuery(strSQLQuery,ReturnType.DataSetType);

			int cnt = dsKeyCode.Tables[0].Rows.Count;

			codeValues = new ArrayList();

			if(cnt > 0)
			{				
				for(int i=0; i < cnt; i++)
				{
					DataRow drEach = dsKeyCode.Tables[0].Rows[i];
		
					SystemCode systemCode = new SystemCode();

	
					if(!drEach["key"].GetType().Equals(System.Type.GetType("System.DBNull")))
					{
						systemCode.Text = (String) drEach["key"];
					}	
		
					if(!drEach["value"].GetType().Equals(System.Type.GetType("System.DBNull")))
					{
						systemCode.StringValue = (String) drEach["value"];
					}
					
					codeValues.Add(systemCode);
				}


			}

			return  codeValues;
		}

		//end 


		//add by Tanachon Mayoo
		//add date 2015-09-08
		//get key and value of  display datagrid on surcharges page
		public static ArrayList GetKeyValuesZipcodeVASExcluded(string strAppID, string strEnterpriseID,string strConstant)
		{
			ArrayList codeValues = null;

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID);

			if(dbCon == null)
			{
				Logger.LogTraceError("Utility","GetKeyValuesZipcodeVASExcluded","EDB101","DbConnection object is null!!");
				System.Diagnostics.Trace.WriteLineIf(Logger.AppSwitch.TraceError,"[Utility::GetKeyValuesZipcodeVASExcluded()] DbConnection is null","ERROR");
				return codeValues;
			}
			
			String strSQLQuery = "select [key], [value] from  dbo.EnterpriseConfigurations('"+strEnterpriseID+ "','"+strConstant+"')";
						
			
			DataSet dsKeyCode = (DataSet)dbCon.ExecuteQuery(strSQLQuery,ReturnType.DataSetType);

			int cnt = dsKeyCode.Tables[0].Rows.Count;

			codeValues = new ArrayList();

			if(cnt > 0)
			{				
				for(int i=0; i < cnt; i++)
				{
					DataRow drEach = dsKeyCode.Tables[0].Rows[i];
					SystemCode systemCode = new SystemCode();
	
					if(!drEach["key"].GetType().Equals(System.Type.GetType("System.DBNull")))
					{
						systemCode.Text = (String) drEach["key"];
					}	
		
					if(!drEach["value"].GetType().Equals(System.Type.GetType("System.DBNull")))
					{
						systemCode.StringValue = (String) drEach["value"];
					}					
					codeValues.Add(systemCode);
				}
			}
			return  codeValues;
		}

		//end 
	}


	
}
