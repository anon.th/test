using System;
using System.Data;
using System.Runtime.Serialization;


namespace com.common.classes
{
	/// <summary>
	/// Summary description for SessionDS.
	/// </summary>
	[Serializable()]
	public class SessionDS
	{
		protected DataSet m_ds = null;
		protected decimal     m_iQueryResultMax = 0;
		protected decimal     m_iDSRecSize = 0;


		public SessionDS()
		{
			//
			// TODO: Add constructor logic here
			//
		}
		
		/// <summary>
		/// 
		/// </summary>
		public DataSet ds
		{
			set
			{
				m_ds = value;
			}

			get
			{
				return m_ds;
			}
		}

		public decimal QueryResultMaxSize
		{
			set
			{
				m_iQueryResultMax = value;
			}

			get
			{
				return m_iQueryResultMax;
			}
		}

		public decimal DataSetRecSize
		{
			set
			{
				m_iDSRecSize = value;
			}

			get
			{
				return m_iDSRecSize;
			}
		}

	}
}
