using System;
using System.Diagnostics;


namespace com.common.classes
{
	/// <summary>
	/// This class has methods to Log Debug and Trace messages to various listners like EventLog, Flat files and 
	/// Console.
	/// </summary>
	public class Logger
	{
		protected static TraceSwitch m_applicationDebugSwitch = new TraceSwitch("AppSwitch","Displays messages for application");

		/// <summary>
		/// Single instance of TraceSwitch. The trace level of the switch is initialized from web.config file.
		/// </summary>
		public static TraceSwitch AppSwitch
		{
			get
			{
				return m_applicationDebugSwitch;
			}
		}

		private static String FormatClassMethodString(String strClassName, String strMethodName)
		{
			String strClassMethodString = "["+strClassName+"::"+strMethodName+"()] ";
			return strClassMethodString;
		}

		/// <summary>
		/// Log error messages to all registered Trace Listeners. Trace Messages will be logged 
		/// in both debug and release build. The message will be formatted as per format defined by the private
		/// FormatClassMethodString method.<br><br>
		/// 
		/// The message will be logged if the AppSwitch is at level 1 or above
		/// 
		/// </summary>
		/// <param name="strClassName">Class in which message is logged</param>
		/// <param name="strMethodName">Method of that class</param>
		/// <param name="strErrorNumber">error number</param>
		/// <param name="strMessage">Message to be logged</param>
		public static void LogTraceError(String strClassName, String strMethodName, String strErrorNumber , String strMessage )
		{
			String strFullMessage = FormatClassMethodString(strClassName,strMethodName);
			if(strErrorNumber != null)
			{
				strFullMessage += strErrorNumber + ": ";
			}
			strFullMessage += strMessage;
			System.Diagnostics.Trace.WriteLineIf(AppSwitch.TraceError,strFullMessage,"ERROR");
			System.Diagnostics.Trace.Flush();
		}


		/// <summary>
		/// Log warning messages to all registered Trace Listeners. Trace Messages will be logged 
		/// in both debug and release build. The message will be formatted as per format defined by the 
		/// private FormatClassMethodString method.<br><br>
		/// 
		/// The message will be logged if the AppSwitch is at level 2 or above
		/// 
		/// </summary>
		/// <param name="strClassName">Class in which message is logged</param>
		/// <param name="strMethodName">Method of that class</param>
		/// <param name="strErrorNumber">warning number</param>
		/// <param name="strMessage">Message to be logged</param>
		public static void LogTraceWarning(String strClassName, String strMethodName, String strErrorNumber, String strMessage)
		{
			String strFullMessage = FormatClassMethodString(strClassName,strMethodName);
			if(strErrorNumber != null)
			{
				strFullMessage += strErrorNumber + ": ";
			}
			strFullMessage += strMessage;
			System.Diagnostics.Trace.WriteLineIf(AppSwitch.TraceWarning,strFullMessage,"WARNING");
			System.Diagnostics.Trace.Flush();
		}


		/// <summary>
		/// Log informative messages to all registered Trace Listeners. Trace Messages will be logged 
		/// in both debug and release build. The message will be formatted as per format defined by the 
		/// private FormatClassMethodString method.<br><br>
		/// 
		/// The message will be logged if the AppSwitch is at level 3 or above
		/// </summary>
		/// <param name="strClassName">Class in which message is logged</param>
		/// <param name="strMethodName">Method of that class</param>
		/// <param name="strErrorNumber">warning number</param>
		/// <param name="strMessage">Message to be logged</param>
		public static void LogTraceInfo(String strClassName, String strMethodName, String strErrorNumber, String strMessage)
		{
			String strFullMessage = FormatClassMethodString(strClassName,strMethodName);
			if(strErrorNumber != null)
			{
				strFullMessage += strErrorNumber + ": ";
			}
			strFullMessage += strMessage;
			System.Diagnostics.Trace.WriteLineIf(AppSwitch.TraceInfo,strFullMessage,"INFO");
			System.Diagnostics.Trace.Flush();
		}


		/// <summary>
		/// Log verbose messages to all registered Trace Listeners. Trace Messages will be logged 
		/// in both debug and release build. The message will be formatted as per format defined by the 
		/// private FormatClassMethodString method.<br><br>
		/// 
		/// The message will be logged if the AppSwitch is at level 4

		/// </summary>
		/// <param name="strClassName">Class in which message is logged</param>
		/// <param name="strMethodName">Method of that class</param>
		/// <param name="strErrorNumber">verbose number</param>
		/// <param name="strMessage">Message to be logged</param>
		public static void LogTraceVerbose(String strClassName, String strMethodName, String strErrorNumber, String strMessage)
		{
			String strFullMessage = FormatClassMethodString(strClassName,strMethodName);
			if(strErrorNumber != null)
			{
				strFullMessage += strErrorNumber + ": ";
			}
			strFullMessage += strMessage;
			System.Diagnostics.Trace.WriteLineIf(AppSwitch.TraceVerbose,strFullMessage,"VERBOSE");
			System.Diagnostics.Trace.Flush();
		}


		/// <summary>
		/// Log error messages to all registered Debug Listeners. Debug Messages will be logged 
		/// only in debug build. The message will be formatted as per format defined by the private
		/// FormatClassMethodString method.<br><br>
		/// 
		/// The message will be logged if the AppSwitch is at level 1 or above
		/// 
		/// </summary>
		/// <param name="strClassName">Class in which message is logged</param>
		/// <param name="strMethodName">Method of that class</param>
		/// <param name="strErrorNumber">error number</param>
		/// <param name="strMessage">Message to be logged</param>
		public static void LogDebugError(String strClassName, String strMethodName, String strErrorNumber, String strMessage)
		{
			String strFullMessage = FormatClassMethodString(strClassName,strMethodName);
			if(strErrorNumber != null)
			{
				strFullMessage += strErrorNumber + ": ";
			}
			strFullMessage += strMessage;
			System.Diagnostics.Debug.WriteLineIf(AppSwitch.TraceError,strFullMessage,"ERROR");
			System.Diagnostics.Debug.Flush();
		}


		/// <summary>
		/// Log warning messages to all registered Debug Listeners. Debug Messages will be logged 
		/// only in debug build. The message will be formatted as per format defined by the private
		/// FormatClassMethodString method.<br><br>
		/// 
		/// The message will be logged if the AppSwitch is at level 2 or above
		/// 

		/// </summary>
		/// <param name="strClassName">Class in which message is logged</param>
		/// <param name="strMethodName">Method of that class</param>
		/// <param name="strErrorNumber">error number</param>
		/// <param name="strMessage">Message to be logged</param>
		public static void LogDebugWarning(String strClassName, String strMethodName, String strErrorNumber, String strMessage)
		{
			String strFullMessage = FormatClassMethodString(strClassName,strMethodName);
			if(strErrorNumber != null)
			{
				strFullMessage += strErrorNumber + ": ";
			}
			strFullMessage += strMessage;
			System.Diagnostics.Debug.WriteLineIf(AppSwitch.TraceWarning,strFullMessage,"WARNING");
			System.Diagnostics.Debug.Flush();
		}


		/// <summary>
		/// Log informative messages to all registered Debug Listeners. Debug Messages will be logged 
		/// only in debug build. The message will be formatted as per format defined by the private
		/// FormatClassMethodString method.<br><br>
		/// 
		/// The message will be logged if the AppSwitch is at level 3 or above
		/// 
		/// </summary>
		/// <param name="strClassName">Class in which message is logged</param>
		/// <param name="strMethodName">Method of that class</param>
		/// <param name="strErrorNumber">error number</param>
		/// <param name="strMessage">Message to be logged</param>
		public static void LogDebugInfo(String strClassName, String strMethodName, String strErrorNumber, String strMessage)
		{
			String strFullMessage = FormatClassMethodString(strClassName,strMethodName);
			if(strErrorNumber != null)
			{
				strFullMessage += strErrorNumber + ": ";
			}
			strFullMessage += strMessage;
			System.Diagnostics.Debug.WriteLineIf(AppSwitch.TraceInfo,strFullMessage,"INFO");
			System.Diagnostics.Debug.Flush();
		}


		/// <summary>
		/// Log verbose messages to all registered Debug Listeners. Debug Messages will be logged 
		/// only in debug build. The message will be formatted as per format defined by the private
		/// FormatClassMethodString method.<br><br>
		/// 
		/// The message will be logged if the AppSwitch is at level 4
		/// 
		/// </summary>
		/// <param name="strClassName">Class in which message is logged</param>
		/// <param name="strMethodName">Method of that class</param>
		/// <param name="strErrorNumber">error number</param>
		/// <param name="strMessage">Message to be logged</param>
		public static void LogDebugVerbose(String strClassName, String strMethodName, String strErrorNumber, String strMessage)
		{
			String strFullMessage = FormatClassMethodString(strClassName,strMethodName);
			if(strErrorNumber != null)
			{
				strFullMessage += strErrorNumber + ": ";
			}
			strFullMessage += strMessage;
			System.Diagnostics.Debug.WriteLineIf(AppSwitch.TraceVerbose,strFullMessage,"VERBOSE");
			System.Diagnostics.Debug.Flush();
		}



		/// <summary>
		/// Log error messages to the specified module log file. Trace Messages will be logged 
		/// in both debug and release build. The message will be formatted as per format defined by the private
		/// FormatClassMethodString method.<br><br>
		/// 
		/// The message will be logged if the AppSwitch is at level 1 or above
		/// 
		/// </summary>
		/// <param name="strModuleName">one of valid module name as defined in the web.config file</param>
		/// <param name="strClassName">Class in which message is logged</param>
		/// <param name="strMethodName">Method of that class</param>
		/// <param name="strErrorNumber">error number</param>
		/// <param name="strMessage">Message to be logged</param>
		public static void LogTraceError(String strModuleName, String strClassName, String strMethodName, String strErrorNumber , String strMessage)
		{
			String strFullMessage = FormatClassMethodString(strClassName,strMethodName);
			if(strErrorNumber != null)
			{
				strFullMessage += strErrorNumber + ": ";
			}
			strFullMessage += strMessage;
			
			if(AppSwitch.TraceError)
			{
				TraceListener listener = (TraceListener)System.Diagnostics.Trace.Listeners[strModuleName];
				if(listener != null)
				{
					listener.WriteLine(strFullMessage,"ERROR");
					listener.Flush();
				}

				LogTraceToEventLog(strFullMessage,"ERROR");

			}
		}


		/// <summary>
		/// Log warning messages to the specified module log file. Trace Messages will be logged 
		/// in both debug and release build. The message will be formatted as per format defined by the private
		/// FormatClassMethodString method.<br><br>
		/// 
		/// The message will be logged if the AppSwitch is at level 2 or above
		/// 
		/// </summary>
		/// <param name="strModuleName">one of valid module name as defined in the web.config file</param>
		/// <param name="strClassName">Class in which message is logged</param>
		/// <param name="strMethodName">Method of that class</param>
		/// <param name="strErrorNumber">error number</param>
		/// <param name="strMessage">Message to be logged</param>
		public static void LogTraceWarning(String strModuleName, String strClassName, String strMethodName, String strErrorNumber, String strMessage)
		{
			String strFullMessage = FormatClassMethodString(strClassName,strMethodName);
			if(strErrorNumber != null)
			{
				strFullMessage += strErrorNumber + ": ";
			}
			strFullMessage += strMessage;
			if(AppSwitch.TraceWarning)
			{
				TraceListener listener = (TraceListener)System.Diagnostics.Trace.Listeners[strModuleName];
				if(listener != null)
				{
					listener.WriteLine(strFullMessage,"WARNING");
					listener.Flush();
				}
			
				LogTraceToEventLog(strFullMessage,"WARNING");

			}
		}


		/// <summary>
		/// Log informative messages to the specified module log file. Trace Messages will be logged 
		/// in both debug and release build. The message will be formatted as per format defined by the private
		/// FormatClassMethodString method.<br><br>
		/// 
		/// The message will be logged if the AppSwitch is at level 3 or above
		/// 
		/// </summary>
		/// <param name="strModuleName">one of valid module name as defined in the web.config file</param>
		/// <param name="strClassName">Class in which message is logged</param>
		/// <param name="strMethodName">Method of that class</param>
		/// <param name="strErrorNumber">info number</param>
		/// <param name="strMessage">Message to be logged</param>
		public static void LogTraceInfo(String strModuleName, String strClassName, String strMethodName, String strErrorNumber, String strMessage)
		{
			String strFullMessage = FormatClassMethodString(strClassName,strMethodName);
			if(strErrorNumber != null)
			{
				strFullMessage += strErrorNumber + ": ";
			}
			strFullMessage += strMessage;
			if(AppSwitch.TraceInfo)
			{
				TraceListener listener = (TraceListener)System.Diagnostics.Trace.Listeners[strModuleName];
				if(listener != null)
				{
					listener.WriteLine(strFullMessage,"INFO");
					listener.Flush();
				}
		
				LogTraceToEventLog(strFullMessage,"INFO");

			}
		}


		
		/// <summary>
		/// Log verbose messages to the specified module log file. Trace Messages will be logged 
		/// in both debug and release build. The message will be formatted as per format defined by the private
		/// FormatClassMethodString method.<br><br>
		/// 
		/// The message will be logged if the AppSwitch is at level 4
		/// 
		/// </summary>
		/// <param name="strModuleName">one of valid module name as defined in the web.config file</param>
		/// <param name="strClassName">Class in which message is logged</param>
		/// <param name="strMethodName">Method of that class</param>
		/// <param name="strErrorNumber">error number</param>
		/// <param name="strMessage">Message to be logged</param>
		public static void LogTraceVerbose(String strModuleName, String strClassName, String strMethodName, String strErrorNumber, String strMessage)
		{
			String strFullMessage = FormatClassMethodString(strClassName,strMethodName);
			if(strErrorNumber != null)
			{
				strFullMessage += strErrorNumber + ": ";
			}
			strFullMessage += strMessage;
			if(AppSwitch.TraceVerbose)
			{
				TraceListener listener = (TraceListener)System.Diagnostics.Trace.Listeners[strModuleName];
				if(listener != null)
				{
					listener.WriteLine(strFullMessage,"VERBOSE");
					listener.Flush();
				}

				LogTraceToEventLog(strFullMessage,"VERBOSE");
			}
		}


		
		/// <summary>
		/// Log error messages to the specified module log file. Debug Messages will be logged 
		/// only in debug build. The message will be formatted as per format defined by the private
		/// FormatClassMethodString method.<br><br>
		/// 
		/// The message will be logged if the AppSwitch is at level 1 or above
		/// 
		/// </summary>
		/// <param name="strModuleName">one of valid module name as defined in the web.config file</param>
		/// <param name="strClassName">Class in which message is logged</param>
		/// <param name="strMethodName">Method of that class</param>
		/// <param name="strErrorNumber">error number</param>
		/// <param name="strMessage">Message to be logged</param>
		public static void LogDebugError(String strModuleName, String strClassName, String strMethodName, String strErrorNumber, String strMessage)
		{
			#if DEBUG
			String strFullMessage = FormatClassMethodString(strClassName,strMethodName);
			if(strErrorNumber != null)
			{
				strFullMessage += strErrorNumber + ": ";
			}
			strFullMessage += strMessage;
			if(AppSwitch.TraceError)
			{
				TraceListener listener = (TraceListener)System.Diagnostics.Debug.Listeners[strModuleName];
				if(listener != null)
				{
					listener.WriteLine(strFullMessage,"ERROR");
					listener.Flush();
				}

				LogDebugToEventLog(strFullMessage,"ERROR");

			}
			#endif

		}


		/// <summary>
		/// Log warning messages to the specified module log file. Debug Messages will be logged 
		/// only in debug build. The message will be formatted as per format defined by the private
		/// FormatClassMethodString method.<br><br>
		/// 
		/// The message will be logged if the AppSwitch is at level 2 or above
		/// 
		/// </summary>
		/// <param name="strModuleName">one of valid module name as defined in the web.config file</param>
		/// <param name="strClassName">Class in which message is logged</param>
		/// <param name="strMethodName">Method of that class</param>
		/// <param name="strErrorNumber">warning number</param>
		/// <param name="strMessage">Message to be logged</param>
		public static void LogDebugWarning(String strModuleName, String strClassName, String strMethodName, String strErrorNumber, String strMessage)
		{
			#if DEBUG
			String strFullMessage = FormatClassMethodString(strClassName,strMethodName);
			if(strErrorNumber != null)
			{
				strFullMessage += strErrorNumber + ": ";
			}
			strFullMessage += strMessage;
			if(AppSwitch.TraceWarning)
			{
				TraceListener listener = (TraceListener)System.Diagnostics.Debug.Listeners[strModuleName];
				if(listener != null)
				{
					listener.WriteLine(strFullMessage,"WARNING");
					listener.Flush();
				}

				LogDebugToEventLog(strFullMessage,"WARNING");
		
			}
			#endif

		}


		/// <summary>
		/// Log informative messages to the specified module log file. Debug Messages will be logged 
		/// only in debug build. The message will be formatted as per format defined by the private
		/// FormatClassMethodString method.<br><br>
		/// 
		/// The message will be logged if the AppSwitch is at level 3 or above
		/// 
		/// </summary>
		/// <param name="strModuleName">one of valid module name as defined in the web.config file</param>
		/// <param name="strClassName">Class in which message is logged</param>
		/// <param name="strMethodName">Method of that class</param>
		/// <param name="strErrorNumber">error number</param>
		/// <param name="strMessage">Message to be logged</param>
		public static void LogDebugInfo(String strModuleName, String strClassName, String strMethodName, String strErrorNumber, String strMessage)
		{
			#if DEBUG
			String strFullMessage = FormatClassMethodString(strClassName,strMethodName);
			if(strErrorNumber != null)
			{
				strFullMessage += strErrorNumber + ": ";
			}
			strFullMessage += strMessage;
			if(AppSwitch.TraceInfo)
			{
				TraceListener listener = (TraceListener)System.Diagnostics.Debug.Listeners[strModuleName];
				if(listener != null)
				{
					listener.WriteLine(strFullMessage,"INFO");
					listener.Flush();
				}
			
				LogDebugToEventLog(strFullMessage,"INFO");

			}
			#endif
		}

		
		/// <summary>
		/// Log verbose messages to the specified module log file. Debug Messages will be logged 
		/// only in debug build. The message will be formatted as per format defined by the private
		/// FormatClassMethodString method.<br><br>
		/// 
		/// The message will be logged if the AppSwitch is at level 4
		/// 
		/// </summary>
		/// <param name="strModuleName">one of valid module name as defined in the web.config file</param>
		/// <param name="strClassName">Class in which message is logged</param>
		/// <param name="strMethodName">Method of that class</param>
		/// <param name="strErrorNumber">verbose number</param>
		/// <param name="strMessage">Message to be logged</param>
		public static void LogDebugVerbose(String strModuleName, String strClassName, String strMethodName, String strErrorNumber, String strMessage)
		{
			#if DEBUG
			String strFullMessage = FormatClassMethodString(strClassName,strMethodName);
			if(strErrorNumber != null)
			{
				strFullMessage += strErrorNumber + ": ";
			}
			strFullMessage += strMessage;
			if(AppSwitch.TraceVerbose)
			{
				TraceListener listener = (TraceListener)System.Diagnostics.Debug.Listeners[strModuleName];
				if(listener != null)
				{
					listener.WriteLine(strFullMessage,"VERBOSE");
					listener.Flush();
				}
		
				LogDebugToEventLog(strFullMessage,"VERBOSE");

			}
			#endif
		}

		
		/// <summary>
		/// 
		/// </summary>
		/// <param name="strFullMessage"></param>
		/// <param name="strCategory"></param>
		private static void LogTraceToEventLog(String strFullMessage, String strCategory)
		{
			String strAppID = System.Configuration.ConfigurationSettings.AppSettings["applicationID"];
			String strIsLogToEventLog = System.Configuration.ConfigurationSettings.AppSettings["logToEventLog"];
			strIsLogToEventLog = strIsLogToEventLog.ToUpper();


			TraceListener listener = (TraceListener)System.Diagnostics.Trace.Listeners[strAppID];
			
			
			if(listener != null && strIsLogToEventLog == "TRUE")
			{
				listener.WriteLine(strFullMessage,strCategory);
				listener.Flush();
			}
		}
		private static void LogDebugToEventLog(String strFullMessage, String strCategory)
		{
			String strAppID = System.Configuration.ConfigurationSettings.AppSettings["applicationID"];
			String strIsLogToEventLog = System.Configuration.ConfigurationSettings.AppSettings["logToEventLog"];
			strIsLogToEventLog = strIsLogToEventLog.ToUpper();


			TraceListener listener = (TraceListener)System.Diagnostics.Debug.Listeners[strAppID];
			if(listener != null && strIsLogToEventLog == "TRUE")
			{
				listener.WriteLine(strFullMessage,strCategory);
				listener.Flush();
			}
		}

	}
}
