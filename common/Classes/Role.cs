using System;


namespace com.common.classes
{
	/// <summary>
	/// Summary description for Role.
	/// </summary>
	public class Role
	{
		private decimal m_id;
		private String m_strName;
		private String m_strDescription;
		public Role()
		{
		}
		
		public decimal RoleID
		{
			set
			{
				m_id = value;
			}
			get
			{
				return m_id;
			}
		}

		public String RoleName
		{
			set
			{
				m_strName = value;
			}
			get
			{
				return m_strName;
			}
		}

		public String RoleDescription
		{
			set
			{
				m_strDescription = value;
			}
			get
			{
				return m_strDescription;
			}
		}
	}
}
