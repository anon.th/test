using System;

namespace com.common.classes
{
	/// <summary>
	/// Summary description for Permission.
	/// </summary>
	public class Permission
	{
		private String m_strPermissionID = null;
		private String m_strPermissionDesc = null;
		private bool m_IsAllowDataRestriction;

		public Permission()
		{
		}

		public String PermissionID
		{
			set
			{
				m_strPermissionID = value;
			}
			get
			{
				return m_strPermissionID;
			}
		}

		public String PermissionDesc
		{
			set
			{
				m_strPermissionDesc = value;
			}
			get
			{
				return m_strPermissionDesc;
			}
		}

		public bool IsDataRestrictionAllowed()
		{
			return m_IsAllowDataRestriction;
		}

		public void setAllowDataRestriction(bool dataRestriction)
		{
			m_IsAllowDataRestriction = dataRestriction;
		}
	}
}
