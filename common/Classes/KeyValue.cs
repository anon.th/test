using System;

namespace com.common.classes
{
	/// <summary>
	/// Summary description for KeyValue.
	/// </summary>
	public class KeyValue
	{
		private String    m_strKey = null;		
		private String    m_strValue = null;


		public KeyValue()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		public String StringKey
		{
			get
			{
				return m_strKey;
			}

			set
			{
				m_strKey=value;
			}
		}

		
		public String StringValue
		{
			get
			{
				return m_strValue;
			}

			set
			{
				m_strValue=value;
			}
		}

	}
}
