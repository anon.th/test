<%@ Import Namespace="com.common.util" %>
<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<%@ Page language="c#" Codebehind="PickupRequestBooking.aspx.cs" AutoEventWireup="false" Inherits="com.ties.PickupRequestBooking" smartnavigation="false" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<TITLE>PickupRequestBooking</TITLE>
		<META name="vs_showGrid" content="False">
		<LINK rel="stylesheet" type="text/css" href="css/Styles.css" name="stylesheet">
		<META name="GENERATOR" content="Microsoft Visual Studio 7.0">
		<META name="CODE_LANGUAGE" content="C#">
		<META name="vs_defaultClientScript" content="JavaScript">
		<META name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<!--#INCLUDE FILE="msFormValidations.inc"-->
		<SCRIPT language="javascript" src="Scripts/settingScrollPosition.js"></SCRIPT>
		<script language="JavaScript" type="text/javascript">
			function callCodeBehind()
			{
				__doPostBack('<%= txtSendZip.ClientID  %>', '');
			}
		 <!--
			
			//function onKeyPress () {
			//	var keycode;
			//	if (window.event) keycode = window.event.keyCode;
			//	else if (e) keycode = e.which;
			//	else return true;
				
			//	if (keycode == 13) {
			//		document.forms['PickupRequestBooking'].Btn_Save.click();
			//		return false
			//	}
			//	return true 
			//}
			//document.onkeypress = onKeyPress;
			
		// -->
		</script>
	</HEAD>
	<BODY MS_POSITIONING="GridLayout">
		<FORM id="PickupRequestBooking" method="post" runat="server">
			<asp:textbox style="Z-INDEX: 105; POSITION: absolute; TOP: 2px; LEFT: 361px" id="TxtHidden" runat="server"
				AutoPostBack="True" Visible="False" Width="165px">PickupRequestBooking</asp:textbox>
			<TABLE style="Z-INDEX: 107; POSITION: absolute; TOP: 912px; LEFT: 0px" id="Table1" border="0"
				cellSpacing="0" cellPadding="0" width="300">
				<TR>
					<TD>&nbsp;</TD>
				</TR>
			</TABLE>
			<asp:validationsummary style="Z-INDEX: 106; POSITION: absolute; TOP: 17px; LEFT: 625px" id="PageValidSummary"
				runat="server" ShowMessageBox="True" ShowSummary="False" HeaderText="Please enter the missing  fields."></asp:validationsummary><asp:label style="Z-INDEX: 103; POSITION: absolute; TOP: 3px; LEFT: 15px" id="LblHeading" runat="server"
				Width="442px" CssClass="mainTitleSize" Height="30px">Pickup Request Booking</asp:label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<asp:regularexpressionvalidator style="Z-INDEX: 104; POSITION: absolute; TOP: 4px; LEFT: 534px" id="Regularpickupdate"
				runat="server" Width="467px" CssClass="errorMsgColor" Height="12px" ValidationExpression="^((1[0-2]|(0[1-9])|[1][\d]|[2][\d]|[3][0]|[3][1])\/((0[1-9])|(1[0-2]))\/([\d][\d][\d][\d])[\s]((0[1-9])|[1][\d]|(2[0-4]))\:(0[0-9]|(1[\d]|2[\d]|3[\d]|4[\d]|5[\d]|6[0])))"
				Display="None" ErrorMessage="PickupDate format should be DD/MM/YY HH:mm" ControlToValidate="Txt_Estpickupdatetime"
				Enabled="False">*</asp:regularexpressionvalidator><asp:label style="Z-INDEX: 108; POSITION: absolute; TOP: 40px; LEFT: 16px" id="ErrorMsg" runat="server"
				Width="643px" CssClass="errorMsgColor" Height="3px"></asp:label>
			<DIV style="Z-INDEX: 101; POSITION: relative; WIDTH: 765px; HEIGHT: 792px; TOP: 40px; LEFT: 0px"
				id="divMain" MS_POSITIONING="GridLayout" runat="server">
				<TABLE style="HEIGHT: 129px" border="0" width="100%">
					<TR height="27">
						<TD width="550" colSpan="12"><FONT face="Tahoma"><asp:button id="btn_Query" tabIndex="200" runat="server" Width="56px" CssClass="queryButton"
									Height="20" Text="Query" CausesValidation="False"></asp:button>&nbsp;
								<asp:button id="Btn_ExcuteQuery" tabIndex="1" runat="server" Width="96px" CssClass="queryButton"
									Height="20" Text="ExecuteQuery" CausesValidation="False"></asp:button>&nbsp;
								<asp:button id="Btn_Insert" tabIndex="2" runat="server" Width="81px" CssClass="queryButton"
									Height="20" Text="Insert" CausesValidation="False"></asp:button>&nbsp;
								<asp:button id="Btn_Save" tabIndex="3" runat="server" Width="70px" CssClass="queryButton" Height="20"
									Text="Save"></asp:button>&nbsp;
								<asp:button id="Btn_Delete" tabIndex="4" runat="server" Width="64px" CssClass="queryButton"
									Height="20" Text="Delete" CausesValidation="False"></asp:button>&nbsp;
								<asp:button id="btnService" runat="server" Width="126px" CssClass="queryButton" Text="Service Available"
									CausesValidation="False"></asp:button></FONT></TD>
						<TD colSpan="4" align="right"><FONT face="Tahoma"><asp:button id="btnMovefirst" runat="server" Width="30px" CssClass="queryButton" Height="20"
									Text="|<" CausesValidation="False"></asp:button><asp:button id="btnMovePrevious" runat="server" Width="30px" CssClass="queryButton" Height="20"
									Text="<" CausesValidation="False"></asp:button><asp:textbox id="Txt_RecCnt" runat="server" Width="29px" Height="18px" Enabled="False"></asp:textbox><asp:button id="btnMovenext" runat="server" Width="30px" CssClass="queryButton" Height="20"
									Text=">" CausesValidation="False"></asp:button><asp:button id="btnMoveLast" runat="server" Width="30px" CssClass="queryButton" Height="20"
									Text=">|" CausesValidation="False"></asp:button></FONT></TD>
					</TR>
					<TR>
						<TD colSpan="11"></TD>
						<TD colSpan="4" align="right"></TD>
					</TR>
					<TR height="27">
						<TD width="1%">&nbsp;&nbsp;</TD>
						<TD colSpan="3"><asp:label id="Lb_BookingNo" runat="server" CssClass="tableLabel">Booking No</asp:label></TD>
						<TD colSpan="3"><cc1:mstextbox id="Txt_BookingNo" Width="151px" CssClass="textField" Enabled="True" Text="" NumberMaxValue="2147483647"
								NumberPrecision="10" MaxLength="10" Runat="server" TextMaskType="msNumeric"></cc1:mstextbox></TD>
						<TD colSpan="2" align="right"><asp:label id="Lb_BookingDate" runat="server" Width="90px" CssClass="tableLabel">Booking Date</asp:label></TD>
						<TD colSpan="2"><cc1:mstextbox id="Txt_BookingDate" Width="106px" CssClass="textField" Enabled="True" Text="" MaxLength="16"
								Runat="server" TextMaskType="msAlfaNumericWithUnderscoreSetFormat" TextMaskString="99/99/9999 99:99"></cc1:mstextbox></TD>
						<TD colSpan="2"><asp:label id="Lb_StatusCode" runat="server" Width="97px" CssClass="tableLabel">Status Code</asp:label></TD>
						<TD colSpan="2"><asp:textbox id="Txt_StatusCode" runat="server" Width="57px" CssClass="textField" MaxLength="20"></asp:textbox><FONT face="Tahoma">&nbsp;</FONT>&nbsp;<asp:button id="BtnStatusCode" runat="server" CssClass="searchButton" Text="...." CausesValidation="False"></asp:button></TD>
					</TR>
					<TR>
						<TD width="1%"></TD>
						<TD colSpan="3"><asp:label id="Lb_BookingType" runat="server" Width="85px" CssClass="tableLabel">Booking Type</asp:label></TD>
						<TD colSpan="3"><asp:dropdownlist id="Drp_BookingType" runat="server" AutoPostBack="True" Width="150px" CssClass="textField"></asp:dropdownlist></TD>
						<TD colSpan="2" align="right"><asp:label id="Lb_RouteCode" runat="server" Width="99px" CssClass="tableLabel">Route Code</asp:label></TD>
						<TD colSpan="2"><asp:textbox id="txtRouteCode" tabIndex="6" runat="server" Width="115px" CssClass="textField"
								MaxLength="12"></asp:textbox>&nbsp;<FONT face="Tahoma"> </FONT>
							<asp:button id="btnRouteCode" tabIndex="7" runat="server" Width="21px" CssClass="searchButton"
								Height="19px" Text="..." CausesValidation="False"></asp:button></TD>
						<TD colSpan="2"><asp:label id="Lb_ExceptionCode" runat="server" Width="97px" CssClass="tableLabel">Exception Code</asp:label></TD>
						<TD colSpan="2"><asp:textbox id="Txt_ExceptionCode" runat="server" Width="55px" CssClass="textField" MaxLength="20"></asp:textbox><FONT face="Tahoma">&nbsp;</FONT>&nbsp;<asp:button id="BtnExceptionCode" runat="server" CssClass="searchButton" Text="...." CausesValidation="False"></asp:button></TD>
					</TR>
				</TABLE>
				<FIELDSET style="WIDTH: 768px; HEIGHT: 223px" id="fs1" runat="server"><LEGEND><asp:label id="lblCustomerhead" runat="server" CssClass="tableHeadingFieldset">Customer Information</asp:label></LEGEND>
					<TABLE style="WIDTH: 760px; HEIGHT: 193px" border="0">
						<TR>
							<TD width="2%"></TD>
							<TD width="5%"><FONT face="Tahoma"></FONT></TD>
							<TD width="8%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
						</TR>
						<TR height="27">
							<TD width="2%"></TD>
							<TD colSpan="3"><asp:label id="Lb_CustomerType" runat="server" Width="93px" CssClass="tableLabel">Customer Type</asp:label></TD>
							<TD colSpan="3"><asp:dropdownlist id="Drp_CustType" runat="server" Width="152px" CssClass="textField"></asp:dropdownlist></TD>
							<TD colSpan="3"><asp:checkbox id="Chk_NewCustomer" runat="server" Width="107px" CssClass="tableLabel" Text="New Customer"
									autopostback="True" Visible="False"></asp:checkbox></TD>
						</TR>
						<TR>
							<TD><asp:label id="lblCustID" runat="server" ForeColor="Red">*</asp:label></TD>
							<TD colSpan="3"><asp:label id="Lb_CustomerID" runat="server" CssClass="tableLabel">Customer ID</asp:label><asp:requiredfieldvalidator id="VaidCustID" runat="server" ErrorMessage="Cust ID is Required" ControlToValidate="txtCustID"
									ForeColor="#E9E9CB" Font-Size="1px">*</asp:requiredfieldvalidator></TD>
							<TD colSpan="2"><cc1:mstextbox id="txtCustID" tabIndex="8" runat="server" AutoPostBack="True" Width="153px" CssClass="textField"
									MaxLength="20" TextMaskType="msUpperAlfaNumericWithUnderscore"></cc1:mstextbox></TD>
							<TD>&nbsp;<asp:button id="BtnCustomerID" tabIndex="9" runat="server" CssClass="searchButton" Text="..."
									CausesValidation="False"></asp:button>&nbsp;</TD>
							<TD colSpan="2" align="right"><asp:label id="Label3" runat="server" ForeColor="Red"></asp:label></TD>
							<TD colSpan="9">&nbsp;&nbsp;<asp:label id="Lb_PaymentMode" runat="server" Width="90px" CssClass="tableLabel">&nbsp;Payment Mode</asp:label>
								<asp:radiobutton id="Rd_Cash" tabIndex="15" runat="server" AutoPostBack="True" CssClass="tableRadioButton"
									Text="Cash" BorderStyle="None" GroupName="PaymentMode"></asp:radiobutton>&nbsp;&nbsp;
								<asp:radiobutton id="rd_Credit" tabIndex="16" runat="server" AutoPostBack="True" Width="58px" CssClass="tableRadioButton"
									Text="Credit" BorderStyle="None" GroupName="PaymentMode"></asp:radiobutton></TD>
						</TR>
						<TR height="27">
							<TD><asp:label id="lblName" runat="server" ForeColor="Red">*</asp:label></TD>
							<TD colSpan="3"><asp:label id="Lb_Name" runat="server" CssClass="tableLabel">Name</asp:label><asp:requiredfieldvalidator id="ValidName" runat="server" ErrorMessage="Enter Customer Name" ControlToValidate="txtCustName"
									ForeColor="#E9E9CB" Font-Size="1px">*</asp:requiredfieldvalidator></TD>
							<TD colSpan="3"><asp:textbox id="txtCustName" tabIndex="10" runat="server" Width="153px" CssClass="textField"
									MaxLength="100"></asp:textbox></TD>
							<TD colSpan="2" align="right"><asp:label id="lblTele" runat="server" ForeColor="Red">*</asp:label><asp:requiredfieldvalidator id="ValidCustTelphone" runat="server" ErrorMessage="Enter Customer Telephone" ControlToValidate="Txt_Telephone"
									ForeColor="#E9E9CB" Font-Size="1px">*</asp:requiredfieldvalidator></TD>
							<TD colSpan="9">&nbsp;&nbsp;<asp:label id="lblTelephone" runat="server" CssClass="tableLabel">&nbsp;Telephone &nbsp;</asp:label>&nbsp;&nbsp;&nbsp;&nbsp;
								<asp:textbox id="Txt_Telephone" tabIndex="17" runat="server" Width="130px" CssClass="textField"
									MaxLength="20"></asp:textbox></TD>
						</TR>
						<TR height="27">
							<TD><asp:label id="lblAddress" runat="server" ForeColor="Red">*</asp:label></TD>
							<TD colSpan="3"><asp:label id="Lb_Address" runat="server" CssClass="tableLabel">Address</asp:label><asp:requiredfieldvalidator id="ValidCustAdd" runat="server" ErrorMessage="Enter Valid Address" ControlToValidate="txtCustAddr1"
									ForeColor="#E9E9CB" Font-Size="1px">*</asp:requiredfieldvalidator></TD>
							<TD colSpan="3"><asp:textbox id="txtCustAddr1" tabIndex="11" runat="server" Width="153px" CssClass="textField"
									MaxLength="100"></asp:textbox></TD>
							<TD colSpan="2" align="right"><asp:label id="Label5" runat="server" ForeColor="Red"></asp:label></TD>
							<TD colSpan="10">&nbsp;&nbsp;<asp:label id="lblFax" runat="server" CssClass="tableLabel">&nbsp;Fax </asp:label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<asp:textbox id="Txt_Fax" tabIndex="18" runat="server" Width="130px" CssClass="textField" MaxLength="20"></asp:textbox></TD>
						</TR>
						<TR>
							<TD></TD>
							<TD colSpan="3"></TD>
							<TD colSpan="3"><asp:textbox id="txtCustAdd2" tabIndex="12" runat="server" Width="153px" CssClass="textField"
									MaxLength="100"></asp:textbox></TD>
						</TR>
						<TR height="27">
							<TD><asp:label id="lblPostalCode" runat="server" ForeColor="Red">*</asp:label></TD>
							<TD colSpan="3"><asp:label id="Lb_ZipCode" runat="server" CssClass="tableLabel">Postal Code</asp:label><asp:requiredfieldvalidator id="ValidCustZipCode" runat="server" Width="4px" ErrorMessage="Customer zip Code is required"
									ControlToValidate="txtCustZipCode" ForeColor="#E9E9CB" Font-Size="1px">*</asp:requiredfieldvalidator></TD>
							<TD colSpan="2"><cc1:mstextbox id="txtCustZipCode" tabIndex="13" AutoPostBack="True" Width="153px" CssClass="textField"
									Enabled="True" Text="" MaxLength="10" Runat="server"></cc1:mstextbox></TD>
							<TD colSpan="8">&nbsp;<asp:button id="btnCustZipcode" tabIndex="14" runat="server" CssClass="searchButton" Text="..."
									CausesValidation="False"></asp:button>&nbsp;&nbsp;
								<asp:textbox id="Txt_Country" runat="server" Width="135px" CssClass="textField" ReadOnly="True"></asp:textbox>&nbsp;&nbsp;<asp:textbox id="Txt_StateCode" runat="server" Width="210px" CssClass="textField" ReadOnly="True"></asp:textbox>&nbsp;&nbsp;&nbsp;</TD>
						</TR>
					</TABLE>
				</FIELDSET>
				<FIELDSET style="WIDTH: 768px; HEIGHT: 240px" id="fs2" runat="server"><LEGEND><asp:label id="lblSenderhead" runat="server" CssClass="tableHeadingFieldset">Sender Information</asp:label></LEGEND>
					<TABLE style="WIDTH: 760px; HEIGHT: 210px" border="0">
						<TR>
							<TD width="2%"></TD>
							<TD width="5%"><FONT face="Tahoma"></FONT></TD>
							<TD width="8%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
						</TR>
						<TR>
							<TD></TD>
							<TD colSpan="6"><asp:checkbox id="Chk_SameCustomerInfo" tabIndex="19" runat="server" AutoPostBack="True" Width="237px"
									CssClass="tableLabel" Text="Same as Customer Information" ForeColor="Black"></asp:checkbox></TD>
						</TR>
						<TR>
							<TD><asp:label id="lblSenName" runat="server" ForeColor="Red">*</asp:label></TD>
							<TD colSpan="2"><asp:label id="Lb_Sname" runat="server" CssClass="tableLabel">Name</asp:label><asp:requiredfieldvalidator id="ValidSenderName" runat="server" ErrorMessage="Sender Name is required" ControlToValidate="txtSendName"
									ForeColor="#E9E9CB" Font-Size="1px">*</asp:requiredfieldvalidator></TD>
							<TD colSpan="3"><asp:textbox id="txtSendName" tabIndex="20" runat="server" AutoPostBack="True" Width="153px"
									CssClass="textField" MaxLength="100"></asp:textbox></TD>
							<TD><asp:button id="BtnSendID" tabIndex="21" runat="server" CssClass="searchButton" Text="..." CausesValidation="False"></asp:button></TD>
							<TD colSpan="5" align="right"><FONT face="Tahoma"><asp:label id="lblSenContact" runat="server" ForeColor="Red">*</asp:label></FONT></TD>
							<TD colSpan="4">&nbsp;&nbsp;&nbsp;<asp:label id="Label4" runat="server" Width="88px" CssClass="tableLabel">Contact Person</asp:label><asp:requiredfieldvalidator id="ValidContactPerson" runat="server" Width="3px" Height="7px" ErrorMessage="Enter Contact person"
									ControlToValidate="txtSendContPer" ForeColor="#E9E9CB" Font-Size="1px">*</asp:requiredfieldvalidator></TD>
							<TD><asp:textbox id="txtSendContPer" tabIndex="26" runat="server" Width="130" CssClass="textField"
									MaxLength="100"></asp:textbox></TD>
						</TR>
						<TR>
							<TD><asp:label id="lblSenAddress" runat="server" ForeColor="Red">*</asp:label></TD>
							<TD colSpan="2"><asp:label id="Lb_SAddress" runat="server" CssClass="tableLabel">Address</asp:label>&nbsp;&nbsp;&nbsp;
								<asp:requiredfieldvalidator id="VaildSenderAdd" runat="server" ErrorMessage="Enter Sender Address" ControlToValidate="txtSendAddr1"
									ForeColor="#E9E9CB" Font-Size="1px">*</asp:requiredfieldvalidator></TD>
							<TD colSpan="3"><asp:textbox id="txtSendAddr1" tabIndex="22" runat="server" Width="153px" CssClass="textField"
									MaxLength="100"></asp:textbox></TD>
							<TD colSpan="6" align="right"><FONT face="Tahoma"><asp:label id="lblSenTele" runat="server" ForeColor="Red">*</asp:label></FONT></TD>
							<TD colSpan="4">&nbsp;&nbsp;&nbsp;<asp:label id="LblSTelephone" runat="server" CssClass="tableLabel">Telephone</asp:label><asp:requiredfieldvalidator id="ValidSenderTelephone" runat="server" ErrorMessage="Enter Sender Telephone" ControlToValidate="txtSendTel"
									ForeColor="#E9E9CB" Font-Size="1px">*</asp:requiredfieldvalidator></TD>
							<TD colSpan="3"><asp:textbox id="txtSendTel" tabIndex="27" runat="server" Width="130px" CssClass="textField"
									MaxLength="20"></asp:textbox></TD>
						</TR>
						<TR>
							<TD></TD>
							<TD colSpan="2">
							<TD colSpan="3"><asp:textbox id="txtSendAddr2" tabIndex="23" runat="server" Width="153px" CssClass="textField"
									MaxLength="100"></asp:textbox></TD>
							<TD colSpan="6"></TD>
							<TD colSpan="4">&nbsp;&nbsp;&nbsp;<asp:label id="Lb_Sfax" runat="server" CssClass="tableLabel">Fax</asp:label></TD>
							<TD colSpan="2"><asp:textbox id="txtSendFax" tabIndex="28" runat="server" Width="130" CssClass="textField" MaxLength="20"></asp:textbox></TD>
						</TR>
						<TR>
							<TD style="HEIGHT: 22px"><asp:label id="lblSenPostalCode" runat="server" ForeColor="Red">*</asp:label></TD>
							<TD colSpan="2" style="HEIGHT: 22px"><asp:label id="Lb_SZipCode" runat="server" Width="68px" CssClass="tableLabel">Postal Code</asp:label>&nbsp;
								<asp:requiredfieldvalidator id="ValidSenderZipcode" runat="server" Width="3px" ErrorMessage="Sender Zip Code is required"
									ControlToValidate="txtSendZip" ForeColor="#E9E9CB" Font-Size="1px">*</asp:requiredfieldvalidator></TD>
							<TD colSpan="3" style="HEIGHT: 22px"><cc1:mstextbox id="txtSendZip" tabIndex="24" AutoPostBack="True" Width="153px" CssClass="textField"
									Enabled="True" Text="" MaxLength="10" Runat="server"></cc1:mstextbox></TD>
							<TD colSpan="15" style="HEIGHT: 22px"><asp:button id="btnSenZipcode" tabIndex="25" runat="server" CssClass="searchButton" Text="..."
									CausesValidation="False"></asp:button>&nbsp;&nbsp;&nbsp;<asp:textbox id="txtSendCity" runat="server" Width="135px" CssClass="textField" ReadOnly="True"></asp:textbox>
								&nbsp;<asp:textbox id="txtSendState" runat="server" Width="213px" CssClass="textField" ReadOnly="True"></asp:textbox></TD>
						</TR>
						<TR>
							<TD><asp:label id="lblSenCutOff" runat="server" ForeColor="Red">*</asp:label></TD>
							<TD colSpan="2"><asp:label id="lblCutoffTime" runat="server" Width="74px" CssClass="tableLabel">Cut-off Time</asp:label><asp:requiredfieldvalidator id="ValidCutofftime" runat="server" ErrorMessage="Cut off Time is required" ControlToValidate="txtSendCuttOffTime"
									ForeColor="#E9E9CB" Font-Size="1px">*</asp:requiredfieldvalidator></TD>
							<TD colSpan="3"><asp:textbox id="txtSendCuttOffTime" tabIndex="29" runat="server" Width="153px" CssClass="textField"
									ReadOnly="True"></asp:textbox>&nbsp;</TD>
							<TD colSpan="3"></TD>
							<TD colSpan="2"></TD>
						</TR>
						<TR>
							<TD></TD>
							<TD colSpan="2"><asp:label id="lblESA" runat="server" Width="74px" CssClass="tableLabel">ESA Applied</asp:label></TD>
							<TD colSpan="3"><asp:textbox id="txtESA" tabIndex="30" runat="server" Width="153px" CssClass="textField" ReadOnly="True"></asp:textbox></TD>
							<TD colSpan="6"></TD>
							<TD colSpan="4">&nbsp;&nbsp;&nbsp;<asp:label id="lblESASurcharge" runat="server" CssClass="tableLabel">ESA Surcharge</asp:label></TD>
							<TD colSpan="3"><asp:textbox id="txtESASurcharge" runat="server" Width="130px" CssClass="textField" ReadOnly="True"></asp:textbox></TD>
						</TR>
					</TABLE>
				</FIELDSET>
				<FIELDSET style="WIDTH: 768px; HEIGHT: 200px" id="fs3" runat="server"><LEGEND><asp:label id="lblPkghead" runat="server" CssClass="tableHeadingFieldset">Package Information</asp:label></LEGEND>
					<TABLE style="WIDTH: 760px; HEIGHT: 168px">
						<TR height="27">
							<TD style="WIDTH: 14px"></TD>
							<TD colSpan="4">
								<P><asp:label id="Lb_EstpickupDateTime" runat="server" CssClass="tableLabel">Est Pickup Date/Time</asp:label></P>
							</TD>
							<TD colSpan="3"><cc1:mstextbox id="Txt_Estpickupdatetime" tabIndex="31" AutoPostBack="True" Width="152px" CssClass="textField"
									Enabled="True" Text="" MaxLength="16" Runat="server" TextMaskType="msDateTime" TextMaskString="99/99/9999 99:99"></cc1:mstextbox></TD>
							<TD colSpan="2"><asp:label id="Lb_PackagePaymenttype" runat="server" CssClass="tableLabel">Payment Type</asp:label></TD>
							<TD colSpan="3"><asp:radiobutton id="Rd_FrieghtPrepaid" tabIndex="32" runat="server" CssClass="tableRadioButton"
									Text="Freight Prepaid" BorderStyle="None" GroupName="PayementType"></asp:radiobutton></TD>
							<TD colSpan="2"><asp:radiobutton id="Rd_FreightCollectd" tabIndex="33" runat="server" Width="105px" CssClass="tableRadioButton"
									Text="Freight Collect" BorderStyle="None" GroupName="PayementType" Enabled="False" EnableViewState="False"></asp:radiobutton></TD>
						</TR>
						<TR height="27">
							<TD style="WIDTH: 14px"></TD>
							<TD colSpan="4"><asp:label id="Lb_ActualPickupdatetime" runat="server" Width="154px" CssClass="tableLabel">Actual Pickup Date/Time</asp:label>&nbsp;
							</TD>
							</TD>
							<TD colSpan="3"><cc1:mstextbox id="Txt_ActualPickupDateTime" Width="151px" CssClass="textField" Enabled="True"
									Text="" MaxLength="16" Runat="server" TextMaskType="msDateTime" TextMaskString="99/99/9999 99:99" ReadOnly="True"></cc1:mstextbox></TD>
							<TD colSpan="2"><asp:label id="Lb_CashAmt" runat="server" CssClass="tableLabel">Cash to Collect on Pickup</asp:label></TD>
							<TD colSpan="3"><cc1:mstextbox id="Txt_CashAmount" tabIndex="34" Width="115px" CssClass="textField" Enabled="True"
									Text="" MaxLength="8" Runat="server" TextMaskType="msPrice"></cc1:mstextbox></TD>
							<TD colSpan="3"></TD>
							</TD></TD></TR>
						<TR>
							<TD style="WIDTH: 14px"><asp:label id="Label2" runat="server" ForeColor="Red">*</asp:label></TD>
							<TD colSpan="4"><asp:label id="Lb_TotalPackages" runat="server" CssClass="tableLabel">Total Packages</asp:label>&nbsp;
							</TD>
							<TD colSpan="3"><cc1:mstextbox id="Txt_Totpkg" tabIndex="36" runat="server" Width="67px" CssClass="textFieldRightAlign"
									NumberMaxValue="99999999" NumberPrecision="10" MaxLength="11" TextMaskType="msNumeric" NumberScale="0"
									NumberMinValue="0"></cc1:mstextbox></TD>
							<TD colSpan="2"></TD>
							<TD colSpan="3"></TD>
							<TD colSpan="3"></TD>
						</TR>
						<TR>
							<TD style="WIDTH: 14px; HEIGHT: 22px"></TD>
							<TD style="HEIGHT: 22px" colSpan="4"><asp:label id="Lb_ActualWeight" runat="server" CssClass="tableLabel">Actual Weight</asp:label>&nbsp;
							</TD>
							<TD style="HEIGHT: 22px" colSpan="3"><cc1:mstextbox id="Txt_ActualWeight" tabIndex="37" Visible="True" Width="67px" CssClass="textFieldRightAlign"
									Text="" NumberMaxValue="100000" NumberPrecision="6" Runat="server" TextMaskType="msNumeric" NumberScale="2" NumberMinValue="0"></cc1:mstextbox></TD>
							<TD style="HEIGHT: 22px" colSpan="3"><asp:button style="DISPLAY: none" id="BtnShipmentDetails" tabIndex="40" runat="server" CssClass="queryButton"
									Text="ShipmentDetails" CausesValidation="False" width="117px"></asp:button></TD>
							<TD style="HEIGHT: 22px" colSpan="3"></TD>
						</TR>
						<TR>
							<TD style="WIDTH: 14px"></TD>
							<TD colSpan="4"><asp:label id="Lb_DimWeight" runat="server" Width="79px" CssClass="tableLabel" Height="12px">Dim Weight</asp:label>&nbsp;
							</TD>
							<TD colSpan="3"><cc1:mstextbox id="Txt_DimWeight" tabIndex="38" Width="67px" CssClass="textFieldRightAlign" Enabled="True"
									Text="" MaxLength="8" Runat="server" TextMaskType="msPrice"></cc1:mstextbox></TD>
						</TR>
						<TR>
							<TD style="WIDTH: 14px"></TD>
							<TD colSpan="4"><asp:label id="Lb_RateCharge" runat="server" Width="79px" CssClass="tableLabel" Height="12px">Rated Charge</asp:label>&nbsp;
							</TD>
							<TD colSpan="3"><cc1:mstextbox id="txtRatedCharge" tabIndex="38" Width="67px" CssClass="textFieldRightAlign" Enabled="True"
									Text="" MaxLength="8" Runat="server" TextMaskType="msPrice" ReadOnly="True"></cc1:mstextbox></TD>
						</TR>
						<TR>
							<TD></TD>
							<TD colSpan="4"><asp:label id="Lb_Remarks" runat="server" Width="105px" CssClass="tableLabel" Height="22px">Remarks</asp:label>&nbsp;
							</TD>
							<TD colSpan="16"><asp:textbox id="Txt_Remarks" tabIndex="39" runat="server" Width="513px" CssClass="textField"
									Height="34px" TextMode="MultiLine"></asp:textbox></TD>
						</TR>
					</TABLE>
				</FIELDSET>
			</DIV>
			<%-- //Phase2 - K04 --%>
			<DIV style="Z-INDEX: 103; POSITION: relative; WIDTH: 439px; HEIGHT: 218px; TOP: 40px; LEFT: 46px"
				id="ExceedTimeState" MS_POSITIONING="GridLayout" runat="server"><BR>
				<P align="center"><asp:label style="Z-INDEX: 110; POSITION: absolute; TOP: 39px; LEFT: 41px" id="Label1" runat="server"
						Width="359px">The Estimated Pickup Date/Time exceeds the Cut-off time for Sender Postal code. To override the standard cut-off time and still schecule the pick up at the time you have entered press Cancel. To reschedule the pickup before the Cut-off time press OK.</asp:label></P>
				<P align="center"><asp:button style="Z-INDEX: 101; POSITION: absolute; TOP: 153px; LEFT: 220px" id="btnExceedCancel"
						runat="server" Width="65px" CssClass="queryButton" Text="Cancel" CausesValidation="False"></asp:button><asp:button style="Z-INDEX: 103; POSITION: absolute; TOP: 153px; LEFT: 169px" id="btnExceedOK"
						runat="server" Width="44px" CssClass="queryButton" Text="OK" CausesValidation="False"></asp:button></P>
			</DIV>
			<%-- //Phase2 - K04 --%>
			<DIV style="Z-INDEX: 102; POSITION: relative; WIDTH: 716px; HEIGHT: 83px; TOP: 40px; LEFT: -3px"
				id="ConfirmPanel" MS_POSITIONING="GridLayout" runat="server"><FONT face="Tahoma"></FONT><FONT face="Tahoma"></FONT><FONT face="Tahoma"></FONT><BR>
				<P align="center"></P>
				<asp:label style="Z-INDEX: 104; POSITION: absolute; TOP: 19px; LEFT: 248px" id="lblConfirmMsg"
					runat="server" CssClass="tableLabel"></asp:label><BR>
				<asp:button style="Z-INDEX: 101; POSITION: absolute; TOP: 41px; LEFT: 374px" id="btnToCancel"
					runat="server" Width="65px" CssClass="queryButton" Text="Cancel" CausesValidation="False"></asp:button><asp:button style="Z-INDEX: 102; POSITION: absolute; TOP: 41px; LEFT: 345px" id="btnNotToSave"
					runat="server" CssClass="queryButton" Text="No" CausesValidation="False"></asp:button><asp:button style="Z-INDEX: 103; POSITION: absolute; TOP: 41px; LEFT: 304px" id="btnToSaveChanges"
					runat="server" CssClass="queryButton" Text="Yes" CausesValidation="False"></asp:button>
				<P></P>
			</DIV>
			<input value="<%=strScrollPosition%>" 
type=hidden name=ScrollPosition>
		</FORM>
	</BODY>
</HTML>
