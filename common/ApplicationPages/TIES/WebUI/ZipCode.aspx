<%@ Page language="c#" Codebehind="ZipCode.aspx.cs" AutoEventWireup="false" Inherits="com.ties.ZipCode" %>
<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<%@ Import Namespace="com.common.util" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Postal Code</title>
		<LINK href="css/Styles.css" type="text/css" rel="stylesheet">
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="C #" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<!--#INCLUDE FILE="msFormValidations.inc"-->
		<script runat="server">
		</script>
	</HEAD>
	<body MS_POSITIONING="GridLayout" onunload="window.top.displayBanner.fnCloseAll(0);">
		<form id="ZipCode" method="post" runat="server">
			<asp:button id="btnQuery" style="Z-INDEX: 105; LEFT: 32px; POSITION: absolute; TOP: 65px" runat="server" CssClass="queryButton" Text="Query" CausesValidation="False" Width="60px"></asp:button><asp:button id="btnExecuteQuery" style="Z-INDEX: 102; LEFT: 83px; POSITION: absolute; TOP: 65px" runat="server" CssClass="queryButton" Text="Execute Query" CausesValidation="False" Width="130px"></asp:button><asp:button id="btnInsert" style="Z-INDEX: 103; LEFT: 212px; POSITION: absolute; TOP: 65px" runat="server" CssClass="queryButton" Text="Insert" CausesValidation="False" Width="68px"></asp:button><asp:button id="btnGenerate" style="Z-INDEX: 103; LEFT: 280px; POSITION: absolute; TOP: 65px" runat="server" CssClass="queryButton" Text="Generate" CausesValidation="False" Width="68px" Visible="False" Enabled="False"></asp:button>
			<asp:datagrid id="dgZipCode" style="Z-INDEX: 101; LEFT: 37px; POSITION: absolute; TOP: 150px" runat="server" Width="950px" Height="30px" AllowCustomPaging="True" AllowPaging="True" OnItemCommand="dgZipCode_Button" OnEditCommand="dgZipCode_Edit" OnCancelCommand="dgZipCode_Cancel" OnUpdateCommand="dgZipCode_Update" OnDeleteCommand="dgZipCode_Delete" OnItemDataBound="dgZipCode_Bound" OnPageIndexChanged="dgZipCode_PageChange" AutoGenerateColumns="False" ItemStyle-Height="10" PageSize="25">
				<ItemStyle Height="10px" CssClass="gridField"></ItemStyle>
				<Columns>
					<asp:EditCommandColumn ButtonType="LinkButton" UpdateText="&lt;IMG SRC='images/butt-update.gif' border=0 title='Update' &gt;" CancelText="&lt;IMG SRC='images/butt-cancel.gif' border=0 title='Cancel' &gt;" EditText="&lt;IMG SRC='images/butt-edit.gif' border=0 title='Edit' &gt;">
						<HeaderStyle CssClass="gridHeading"></HeaderStyle>
						<ItemStyle HorizontalAlign="Left"></ItemStyle>
					</asp:EditCommandColumn>
					<asp:ButtonColumn Text="&lt;IMG SRC='images/butt-delete.gif' border=0 title='Delete' &gt;" CommandName="Delete">
						<HeaderStyle CssClass="gridHeading"></HeaderStyle>
						<ItemStyle HorizontalAlign="Center"></ItemStyle>
					</asp:ButtonColumn>
					<asp:TemplateColumn HeaderText="Postal Code">
						<HeaderStyle Font-Bold="True" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle CssClass="gridField"></ItemStyle>
						<ItemTemplate>
							<asp:Label CssClass="gridLabel" ID="lblZipCode" Text='<%#DataBinder.Eval(Container.DataItem,"zipcode")%>' Runat="server">
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<cc1:mstextbox CssClass="gridTextBox" ID="txtZipCode" Text='<%#DataBinder.Eval(Container.DataItem,"zipcode")%>' Runat="server" Enabled="True" TextMaskType="msUpperAlfaNumericWithUnderscore" MaxLength="10">
							</cc1:mstextbox>
							<asp:RequiredFieldValidator ID="zipcValidator" Runat="server" BorderWidth="0" Display="None" ControlToValidate="txtZipCode" ErrorMessage="Postal Code"></asp:RequiredFieldValidator>
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="State">
						<HeaderStyle Font-Bold="True" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle CssClass="gridField"></ItemStyle>
						<ItemTemplate>
							<asp:Label CssClass="gridLabel" ID="lblStateCode" Text='<%#DataBinder.Eval(Container.DataItem,"state_code")%>' Runat="server">
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<cc1:mstextbox CssClass="gridTextBox" ID="txtStateCode" Text='<%#DataBinder.Eval(Container.DataItem,"state_code")%>' Runat="server" Enabled="True" TextMaskType="msUpperAlfaNumericWithUnderscore" MaxLength="10">
							</cc1:mstextbox>
							<asp:RequiredFieldValidator ID="scValidator" Runat="server" BorderWidth="0" Display="None" ControlToValidate="txtStateCode" ErrorMessage="State Code "></asp:RequiredFieldValidator>
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:ButtonColumn Text="&lt;IMG SRC='images/butt-search.gif' border=0 title='Search' &gt;" CommandName="StateSearch">
						<HeaderStyle CssClass="gridHeading"></HeaderStyle>
						<ItemStyle CssClass="gridField"></ItemStyle>
					</asp:ButtonColumn>
					<asp:TemplateColumn HeaderText="Country">
						<HeaderStyle Font-Bold="True" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle CssClass="gridField"></ItemStyle>
						<ItemTemplate>
							<asp:Label CssClass="gridLabel" ID="lblCountry" Text='<%#DataBinder.Eval(Container.DataItem,"country")%>' Runat="server">
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<cc1:mstextbox CssClass="gridTextBox" ID="txtCountry" Text='<%#DataBinder.Eval(Container.DataItem,"country")%>' Runat="server" Enabled="True" TextMaskType="msUpperAlfaNumericWithUnderscore" MaxLength="50">
							</cc1:mstextbox>
							<asp:RequiredFieldValidator ID="cValidator" Runat="server" BorderWidth="0" Display="None" ControlToValidate="txtCountry" ErrorMessage="Country"></asp:RequiredFieldValidator>
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Zone">
						<HeaderStyle Font-Bold="True" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle CssClass="gridField"></ItemStyle>
						<ItemTemplate>
							<asp:Label CssClass="gridLabel" ID="lblZoneCode" Text='<%#DataBinder.Eval(Container.DataItem,"zone_code")%>' Runat="server">
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<cc1:mstextbox CssClass="gridTextBox" ID="txtZoneCode" Text='<%#DataBinder.Eval(Container.DataItem,"zone_code")%>' Runat="server" Enabled="True" TextMaskType="msUpperAlfaNumericWithUnderscore" MaxLength="12">
							</cc1:mstextbox>
							<asp:RequiredFieldValidator ID="zcValidator" Runat="server" BorderWidth="0" Display="None" ControlToValidate="txtZoneCode" ErrorMessage="Zone Code"></asp:RequiredFieldValidator>
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:ButtonColumn Text="&lt;IMG SRC='images/butt-search.gif' border=0 title='Search' &gt;" CommandName="ZoneSearch">
						<HeaderStyle CssClass="gridHeading"></HeaderStyle>
						<ItemStyle CssClass="gridField"></ItemStyle>
					</asp:ButtonColumn>
					<asp:TemplateColumn HeaderText="Cut-off Time">
						<HeaderStyle Font-Bold="True" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle CssClass="gridField"></ItemStyle>
						<ItemTemplate>
							<asp:Label CssClass="gridLabelNumber" ID="lblCutoffTime" Text='<%#DataBinder.Eval(Container.DataItem,"cutoff_time","{0:HH:mm}")%>' Runat="server">
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<cc1:mstextbox CssClass="gridTextBoxNumber" ID="txtCutoffTime" Text='<%#DataBinder.Eval(Container.DataItem,"cutoff_time","{0:HH:mm}")%>' Runat="server" Enabled="True" TextMaskType="msTime" TextMaskString="99:99" MaxLength="5">
							</cc1:mstextbox>
							<asp:RequiredFieldValidator ID="cotValidator" Runat="server" BorderWidth="0" Display="None" ControlToValidate="txtCutoffTime" ErrorMessage="Cut-off Time"></asp:RequiredFieldValidator>
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Pickup&lt;br&gt;Route">
						<HeaderStyle Font-Bold="True" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle CssClass="gridField"></ItemStyle>
						<ItemTemplate>
							<asp:Label CssClass="gridLabel" ID="lblPickupRoute" Text='<%#DataBinder.Eval(Container.DataItem,"pickup_route")%>' Runat="server" Enabled="True">
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<cc1:mstextbox CssClass="gridTextBox" ID="txtPickupRoute" Text='<%#DataBinder.Eval(Container.DataItem,"pickup_route")%>' Runat="server" Enabled="True" TextMaskType="msUpperAlfaNumericWithUnderscore" MaxLength="12">
							</cc1:mstextbox>
							<asp:RequiredFieldValidator ID="prValidator" Runat="server" BorderWidth="0" Display="None" ControlToValidate="txtPickupRoute" ErrorMessage="Pickup Route "></asp:RequiredFieldValidator>
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:ButtonColumn Text="&lt;IMG SRC='images/butt-search.gif' border=0 title='Search' &gt;" CommandName="PickupRouteSearch">
						<HeaderStyle CssClass="gridHeading"></HeaderStyle>
						<ItemStyle CssClass="gridField"></ItemStyle>
					</asp:ButtonColumn>
					<asp:TemplateColumn HeaderText="Del.&lt;br&gt;Route&#160;">
						<HeaderStyle Font-Bold="True" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle CssClass="gridField"></ItemStyle>
						<ItemTemplate>
							<asp:Label CssClass="gridLabel" ID="lblDeliveryRoute" Text='<%#DataBinder.Eval(Container.DataItem,"delivery_route")%>' Runat="server" Enabled="True">
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<cc1:mstextbox CssClass="gridTextBox" ID="txtDeliveryRoute" Text='<%#DataBinder.Eval(Container.DataItem,"delivery_route")%>' Runat="server" Enabled="True" TextMaskType="msUpperAlfaNumericWithUnderscore" MaxLength="12">
							</cc1:mstextbox>
							<asp:RequiredFieldValidator ID="drValidator" Runat="server" BorderWidth="0" Display="None" ControlToValidate="txtDeliveryRoute" ErrorMessage="Delivery Route "></asp:RequiredFieldValidator>
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:ButtonColumn Text="&lt;IMG SRC='images/butt-search.gif' border=0 title='Search' &gt;" CommandName="DeliveryRouteSearch">
						<HeaderStyle CssClass="gridHeading"></HeaderStyle>
						<ItemStyle CssClass="gridField"></ItemStyle>
					</asp:ButtonColumn>
					<asp:TemplateColumn HeaderText="Area">
						<HeaderStyle CssClass="gridHeading"></HeaderStyle>
						<ItemStyle CssClass="gridField"></ItemStyle>
						<ItemTemplate>
							<asp:Label CssClass="gridLabel" ID="lblArea" Text='<%#DataBinder.Eval(Container.DataItem,"area")%>' Runat="server" Enabled="True">
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<asp:TextBox CssClass="gridTextBox" ID="txtArea" Text='<%#DataBinder.Eval(Container.DataItem,"area")%>' Runat="server" MaxLength="12">
							</asp:TextBox>
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Region">
						<HeaderStyle CssClass="gridHeading"></HeaderStyle>
						<ItemStyle CssClass="gridField"></ItemStyle>
						<ItemTemplate>
							<asp:Label CssClass="gridLabel" ID="lblRegion" Text='<%#DataBinder.Eval(Container.DataItem,"region")%>' Runat="server" Enabled="True">
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<asp:TextBox CssClass="gridTextBox" ID="txtRegion" Text='<%#DataBinder.Eval(Container.DataItem,"region")%>' Runat="server" MaxLength="12">
							</asp:TextBox>
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn Visible="False" HeaderText="ESA Surcharge">
						<HeaderStyle CssClass="gridHeading"></HeaderStyle>
						<ItemStyle CssClass="gridField"></ItemStyle>
						<ItemTemplate>
							<asp:Label CssClass="gridLabelNumber" ID="lblEsaSurcharge" Text='<%#DataBinder.Eval(Container.DataItem,"esa_surcharge","{0:n}")%>' Runat="server" Enabled="True">
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<cc1:mstextbox CssClass="gridTextBoxNumber" ID="txtEsaSurcharge" Text='<%#DataBinder.Eval(Container.DataItem,"esa_surcharge","{0:n}")%>' Runat="server" MaxLength="9" TextMaskType="msNumeric" NumberMaxValue="999999" NumberMinValue="0" NumberPrecision="8" NumberScale="2">
							</cc1:mstextbox>
						</EditItemTemplate>
					</asp:TemplateColumn>
				</Columns>
				<PagerStyle NextPageText="Next" PrevPageText="Previous" HorizontalAlign="Left" CssClass="normalText"></PagerStyle>
			</asp:datagrid><asp:label id="lblTitle" style="Z-INDEX: 111; LEFT: 30px; POSITION: absolute; TOP: 12px" runat="server" CssClass="mainTitleSize" Height="26px" Width="477px">Postal Code</asp:label><asp:label id="lblErrorMessage" style="Z-INDEX: 105; LEFT: 30px; POSITION: absolute; TOP: 96px" runat="server" CssClass="errorMsgColor" Height="42px" Width="808px">Place holder for err msg</asp:label><asp:validationsummary id="ValidationSummary1" style="Z-INDEX: 111; LEFT: 79px; POSITION: absolute; TOP: 106px" Height="38px" Width="346px" ShowSummary="False" DisplayMode="BulletList" ShowMessageBox="True" HeaderText="Please enter the missing fields." Runat="server"></asp:validationsummary></form>
	</body>
</HTML>
