<%@ Import Namespace="com.common.util" %>
<%@ Import Namespace="Cambro.Web.DbCombo" %>
<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<%@ Register TagPrefix="DbCombo" Namespace="Cambro.Web.DbCombo" Assembly="Cambro.Web.DbCombo" %>
<%@ Page language="c#" Codebehind="PrintBarcode.aspx.cs" AutoEventWireup="false" Inherits="TIES.PrintBarcode" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Print Barcode</title>
		<LINK href="css/Styles.css" type="text/css" rel="stylesheet" name="stylesheet">
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<!--#INCLUDE FILE="msFormValidations.inc"-->
		<script language="javascript" src="Scripts/settingScrollPosition.js"></script>
	</HEAD>
	<body onclick="GetScrollPosition();" onload="SetScrollPosition();" onunload="window.top.displayBanner.fnCloseAll(0);"
		MS_POSITIONING="GridLayout">
		<form id="CODAmountCollectedReport" method="post" runat="server">
			<asp:button id="btnQuery" style="Z-INDEX: 101; LEFT: 20px; POSITION: absolute; TOP: 40px" runat="server"
				Width="64px" CssClass="queryButton" Text="Query"></asp:button>&nbsp;
			<asp:button id="btnExecuteQuery" style="Z-INDEX: 102; LEFT: 90px; POSITION: absolute; TOP: 40px"
				runat="server" Width="123px" CssClass="queryButton" Text="Generate"></asp:button><asp:label id="lblErrorMessage" style="Z-INDEX: 105; LEFT: 24px; POSITION: absolute; TOP: 69px"
				runat="server" Width="700px" CssClass="errorMsgColor" Height="17px">Place holder for err msg</asp:label>&nbsp;
			<TABLE id="tblShipmentTracking" style="Z-INDEX: 104; LEFT: 21px; WIDTH: 792px; POSITION: absolute; TOP: 96px; HEIGHT: 208px"
				width="792" border="0" runat="server">
				<TR>
					<TD style="WIDTH: 432px; HEIGHT: 137px" vAlign="top">
						<fieldset style="WIDTH: 392px; HEIGHT: 88px"><legend><asp:label id="lblDates" runat="server" Width="128px" CssClass="tableHeadingFieldset" Font-Bold="True">Barcode Consignment</asp:label></legend>
							<TABLE id="tblDates" style="WIDTH: 384px; HEIGHT: 72px" cellSpacing="0" cellPadding="0"
								align="left" border="0" runat="server">
								<TR>
									<td style="WIDTH: 469px; HEIGHT: 1px"><FONT face="Tahoma">&nbsp;</FONT><asp:label id="Label2" runat="server" Width="104px" CssClass="tableLabel" Height="15px">Consignment No.</asp:label><FONT face="Tahoma">&nbsp;&nbsp;</FONT>
										<cc1:mstextbox id="txtConsignmentNo" runat="server" Width="83" CssClass="textField" TextMaskType="msUpperAlfaNumericWithUnderscore"
											MaxLength="20"></cc1:mstextbox>&nbsp;<FONT face="Tahoma">&nbsp;</FONT><asp:label id="lblYear" runat="server" Width="30px" CssClass="tableLabel" Height="15px">Amount</asp:label><FONT face="Tahoma">&nbsp;</FONT>
										<cc1:mstextbox id="txtAmount" runat="server" Width="83" CssClass="textField" NumberPrecision="4"
											NumberMinValue="1" NumberMaxValue="2999" TextMaskType="msNumeric" MaxLength="5"></cc1:mstextbox></td>
								</TR>
							</TABLE>
						</fieldset>
						<P>&nbsp;</P>
					</TD>
				</TR>
			</TABLE>
			<asp:label id="Label1" style="Z-INDEX: 103; LEFT: 20px; POSITION: absolute; TOP: 4px" runat="server"
				Width="484px" CssClass="maintitleSize" Height="27px"> Print Barcode</asp:label></TR></TABLE></TR></TABLE></form>
	</body>
</HTML>
