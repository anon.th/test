<%@ Page language="c#" Codebehind="CreditNote.aspx.cs" AutoEventWireup="false" Inherits="com.ties.CreditNote" smartNavigation="False" %>
<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<%@ Register TagPrefix="DbCombo" Namespace="Cambro.Web.DbCombo" Assembly="Cambro.Web.DbCombo" %>
<%@ Import Namespace="System.Configuration" %>
<%@ Import Namespace="com.common.util" %>
<%@ Import Namespace="Cambro.Web.DbCombo" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>CreditNote</title>
		<LINK rel="stylesheet" type="text/css" href="css/Styles.css" name="stylesheet">
		<meta name="GENERATOR" content="Microsoft Visual Studio 7.0">
		<meta name="CODE_LANGUAGE" content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<!--#INCLUDE FILE="msFormValidations.inc"-->
		<script language="javascript" src="Scripts/settingScrollPosition.js"></script>
		<script language="javascript">
		var CustomerID = "";
		var InvoiceID = "";
		function CustomerOnSelect(Value, Text, SelectionType)
		{	
			if(SelectionType!=3 &&SelectionType!=2){	
			CustomerID = Text;
			validateInput();
			}
		}
		function InvoiceOnSelect(Value, Text, SelectionType)
		{	
			if(SelectionType!=3 &&SelectionType!=2){	
			InvoiceID = Text;
			validateInput();
			}
		}
		function validateInput(){
		var frm = document.forms[0];		
			if(InvoiceID!="" && CustomerID!=""){				
				frm.btnInsertCons.disabled = false;
				frm.btnRetrieve.disabled = false;
			}else{
				frm.btnInsertCons.disabled = true;
				frm.btnRetrieve.disabled = true;
			}
		}
		</script>
	</HEAD>
	<body onclick="GetScrollPosition();" onload="SetScrollPosition();" MS_POSITIONING="GridLayout">
		<form id="DebitNote" method="post" runat="server">
			<DIV style="CLEAR: both; Z-INDEX: 104; LEFT: 8px; WIDTH: 472px; POSITION: relative; TOP: 40px; HEIGHT: 120px"
				id="msgPanel" runat="server" ms_positioning="GridLayout">
				<table style="WIDTH: 464px; HEIGHT: 63px" border="0" cellSpacing="1" cellPadding="1">
					<tr>
						<td align="left">&nbsp;&nbsp;&nbsp;&nbsp;
							<asp:label id="lblMessage" runat="server" Width="408px" Height="30px" CssClass="tableLabel"></asp:label></td>
					</tr>
					<tr>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							<asp:button id="btnToSaveChanges" tabIndex="9" runat="server" Width="40px" CssClass="queryButton"
								CausesValidation="False" Text="Yes"></asp:button><asp:button id="btnNotToSave" tabIndex="9" runat="server" Width="40px" CssClass="queryButton"
								CausesValidation="False" Text="No"></asp:button><asp:button id="btnNO" tabIndex="9" runat="server" Width="63" CssClass="queryButton" CausesValidation="False"
								Text="Cancel" Visible="False"></asp:button></td>
					</tr>
				</table>
			</DIV>
			<asp:validationsummary id="PageValidationSummary" runat="server" HeaderText="Please check the following Invalid/Mandatory field(s):<br><br>"
				DisplayMode="BulletList" ShowSummary="False" ShowMessageBox="True"></asp:validationsummary><asp:label style="Z-INDEX: 102; LEFT: 4px; POSITION: absolute; TOP: 4px" id="lblMainTitle"
				runat="server" Width="392px" Height="26px" CssClass="mainTitleSize">Credit Note</asp:label><asp:label style="Z-INDEX: 103; LEFT: 8px; POSITION: absolute; TOP: 32px" id="lblErrorMsg"
				runat="server" Width="100%" Height="22px" CssClass="errorMsgColor"></asp:label>
			<div style="CLEAR: both; Z-INDEX: 101; LEFT: 8px; WIDTH: 744px; POSITION: relative; TOP: 40px; HEIGHT: 651px"
				id="mainPanel" runat="server" ms_positioning="GridLayout"><asp:button style="Z-INDEX: 101; LEFT: 0px; POSITION: absolute; TOP: 8px" id="btnQry" tabIndex="20"
					runat="server" Width="56px" CssClass="queryButton" CausesValidation="False" Text="Query"></asp:button><asp:button style="Z-INDEX: 106; LEFT: 56px; POSITION: absolute; TOP: 8px" id="btnExecQry" tabIndex="19"
					runat="server" Width="104px" CssClass="queryButton" CausesValidation="False" Text="Execute Query" Enabled="False"></asp:button><asp:button style="Z-INDEX: 110; LEFT: 160px; POSITION: absolute; TOP: 8px" id="btnInsert" tabIndex="18"
					runat="server" Width="56px" CssClass="queryButton" CausesValidation="False" Text="Insert"></asp:button><asp:button style="Z-INDEX: 107; LEFT: 216px; POSITION: absolute; TOP: 8px" id="btnSave" tabIndex="17"
					runat="server" Width="56px" CssClass="queryButton" CausesValidation="False" Text="Save" Enabled="False"></asp:button><asp:button style="Z-INDEX: 105; LEFT: 616px; POSITION: absolute; TOP: 8px" id="btnGoToFirstPage"
					tabIndex="11" runat="server" Width="24px" CssClass="queryButton" CausesValidation="False" Text="|<"></asp:button><asp:button style="Z-INDEX: 100; LEFT: 640px; POSITION: absolute; TOP: 8px" id="btnPreviousPage"
					tabIndex="12" runat="server" Width="24px" CssClass="queryButton" CausesValidation="False" Text="<"></asp:button><asp:textbox style="Z-INDEX: 102; LEFT: 664px; POSITION: absolute; TOP: 8px" id="txtGoToRec"
					tabIndex="13" runat="server" Width="24px" CssClass="textField" Enabled="False"></asp:textbox><asp:button style="Z-INDEX: 108; LEFT: 688px; POSITION: absolute; TOP: 8px" id="btnNextPage"
					tabIndex="14" runat="server" Width="24px" CssClass="queryButton" CausesValidation="False" Text=">"></asp:button><asp:button style="Z-INDEX: 109; LEFT: 712px; POSITION: absolute; TOP: 8px" id="btnGoToLastPage"
					tabIndex="15" runat="server" Width="24px" CssClass="queryButton" CausesValidation="False" Text=">|"></asp:button>
				<table style="Z-INDEX: 111; LEFT: 0px; WIDTH: 744px; POSITION: absolute; TOP: 32px" id="mainTable"
					border="0" width="744" runat="server">
					<tr height="15">
						<td style="HEIGHT: 22px"></td>
						<td style="HEIGHT: 22px"></td>
						<td style="HEIGHT: 22px"></td>
						<td style="HEIGHT: 22px"></td>
						<td style="HEIGHT: 22px"></td>
						<td style="HEIGHT: 22px"></td>
						<td style="HEIGHT: 22px"></td>
						<td style="HEIGHT: 22px"></td>
						<td style="WIDTH: 34px; HEIGHT: 22px"></td>
						<td style="WIDTH: 1px; HEIGHT: 22px"></td>
						<td style="HEIGHT: 22px"></td>
						<td style="HEIGHT: 22px"></td>
						<td style="HEIGHT: 22px"></td>
						<td style="HEIGHT: 22px"></td>
						<td style="HEIGHT: 22px"></td>
						<td style="WIDTH: 122px; HEIGHT: 22px"></td>
						<td style="HEIGHT: 22px"></td>
						<td style="HEIGHT: 22px"></td>
						<td style="HEIGHT: 22px"></td>
						<td style="HEIGHT: 22px"></td>
					</tr>
					<tr>
						<td align="right"></td>
						<td style="WIDTH: 63px" colSpan="4"><asp:label id="lblCreditNoteNo" runat="server" Width="120" CssClass="tableLabel">Credit Note Number</asp:label></td>
						<td style="WIDTH: 154px" colSpan="4"><cc1:mstextbox id="txtCreditNoteNo" tabIndex="1" runat="server" Width="144px" CssClass="textField"
								ReadOnly="True" MaxLength="100"></cc1:mstextbox></td>
						<td colSpan="3"></td>
						<td style="WIDTH: 164px" colSpan="4"><asp:label id="lblTotalCreditAmt" runat="server" Width="130px" Height="12px" CssClass="tableLabel">Total Credit Amount</asp:label></td>
						<td colSpan="4"><asp:textbox id="txtTotalCreditAmt" tabIndex="2" runat="server" Width="144px" CssClass="textFieldRightAlign"
								ReadOnly="True" MaxLength="100"></asp:textbox></td>
					</tr>
					<%--onblur="round(this,2)"--%>
					<TR>
						<TD align="right"></TD>
						<TD style="WIDTH: 63px" colSpan="4"><asp:label id="lblCreditNoteDate" runat="server" Width="96px" Height="1px" CssClass="tableLabel">Credit Note Date</asp:label></TD>
						<TD style="WIDTH: 154px" colSpan="4"><cc1:mstextbox id="txtCreditNoteDate" tabIndex="3" runat="server" Width="144px" CssClass="textField"
								MaxLength="16" TextMaskString="99/99/9999" TextMaskType="msDate"></cc1:mstextbox></TD>
						<TD colSpan="3"></TD>
						<TD style="WIDTH: 164px" colSpan="4"><asp:label id="lblCreateBy" runat="server" Height="2px" CssClass="tableLabel">Created by / date</asp:label></TD>
						<TD colSpan="4"><FONT face="Tahoma"><cc1:mstextbox id="txtCreateUserID" tabIndex="4" runat="server" Width="96px" CssClass="textField"
									ReadOnly="True" MaxLength="40"></cc1:mstextbox>&nbsp;
								<cc1:mstextbox id="txtCreateDate" tabIndex="5" runat="server" Width="141px" CssClass="textField"
									ReadOnly="True" MaxLength="16" TextMaskString="99/99/9999 99:99" TextMaskType="msDateTime"></cc1:mstextbox></FONT></TD>
					</TR>
					<TR>
						<TD align="right"><asp:requiredfieldvalidator id="validCustomerID" ControlToValidate="DbCustomerID" ErrorMessage="Customer ID"
								Runat="server">*</asp:requiredfieldvalidator></TD>
						<TD style="WIDTH: 63px" colSpan="4"><asp:label id="lblCustomerID" runat="server" Width="80px" Height="2px" CssClass="tableLabel">Customer ID</asp:label></TD>
						<TD style="WIDTH: 154px" colSpan="4"><FONT face="Tahoma"><DBCOMBO:DBCOMBO id=DbCustomerID tabIndex=8 Runat="server" ValidationProperty="Text" AutoPostBack="True" TextBoxColumns="13" RegistrationKey='<%#ConfigurationSettings.AppSettings["DbComboRegKey"]%>' TextUpLevelSearchButton="v" ShowDbComboLink="False" ServerMethod="DbCustomerSelect"></DBCOMBO:DBCOMBO></FONT></TD>
						<TD colSpan="3"></TD>
						<TD style="WIDTH: 164px" colSpan="4"><asp:label id="lblPayerName" runat="server" Height="1px" CssClass="tablelabel">Customer Name</asp:label></TD>
						<TD colSpan="4"><cc1:mstextbox id="txtPayerName" tabIndex="7" runat="server" Width="160px" CssClass="textField"
								ReadOnly="True" MaxLength="100"></cc1:mstextbox></TD>
					</TR>
					<tr>
						<TD align="right"><asp:requiredfieldvalidator id="validInvoiceNo" ControlToValidate="DbInvoiceNo" ErrorMessage="Invoice No" Runat="server">*</asp:requiredfieldvalidator></TD>
						<td style="WIDTH: 63px" colSpan="4"><asp:label id="lblInvoiceNo" runat="server" Height="2px" CssClass="tableLabel">Invoice No</asp:label></td>
						<td style="WIDTH: 154px" colSpan="4"><DBCOMBO:DBCOMBO id=DbInvoiceNo tabIndex=8 Runat="server" ValidationProperty="Text" AutoPostBack="True" TextBoxColumns="13" RegistrationKey='<%#ConfigurationSettings.AppSettings["DbComboRegKey"]%>' TextUpLevelSearchButton="v" ShowDbComboLink="False" ServerMethod="DbInvoiceSelect"></DBCOMBO:DBCOMBO></td>
						<td colSpan="3"></td>
						<td style="WIDTH: 164px" colSpan="4"><asp:label id="lblInvoiceDate" runat="server" Height="3px" CssClass="tableLabel">Invoice Date</asp:label></td>
						<td colSpan="4"><cc1:mstextbox id="txtInvoiceDate" tabIndex="9" runat="server" Width="144px" CssClass="textField"
								ReadOnly="True" MaxLength="16" TextMaskString="99/99/9999" TextMaskType="msDate"></cc1:mstextbox></td>
					</tr>
					<tr>
						<td style="HEIGHT: 5px"></td>
						<td style="WIDTH: 63px; HEIGHT: 5px" colSpan="4"><asp:label id="lblStatus" runat="server" Height="1px" CssClass="tableLabel"> Status</asp:label></td>
						<td style="WIDTH: 154px; HEIGHT: 5px" colSpan="4"><asp:dropdownlist id="ddlStatus" tabIndex="13" runat="server" CssClass="textField" Enabled="True"
								AutoPostBack="True"></asp:dropdownlist></td>
						<td style="HEIGHT: 5px" colSpan="3"></td>
						<td style="WIDTH: 164px; HEIGHT: 5px" colSpan="4"><asp:label id="lblStatusUpdatedBy" runat="server" Height="3px" CssClass="tableLabel">Status updated by / date</asp:label></td>
						<td style="HEIGHT: 5px" colSpan="4"><FONT face="Tahoma"><cc1:mstextbox id="txtStatusUpdateUserID" tabIndex="11" runat="server" Width="96px" CssClass="textField"
									ReadOnly="True" MaxLength="40"></cc1:mstextbox>&nbsp;</FONT>
							<cc1:mstextbox id="txtStatusUpdatedDate" tabIndex="12" runat="server" Width="142px" CssClass="textField"
								ReadOnly="True" MaxLength="16" TextMaskString="99/99/9999 99:99" TextMaskType="msDateTime"></cc1:mstextbox></td>
					</tr>
					<tr>
						<td></td>
						<td style="WIDTH: 63px" vAlign="top" colSpan="4"><asp:label id="lblRemarks" runat="server" Height="2px" CssClass="tablelabel">Remarks</asp:label></td>
						<td style="WIDTH: 154px" colSpan="4"><asp:textbox id="txtRemarks" tabIndex="13" runat="server" Width="150px" Height="40px" CssClass="textField"
								MaxLength="200" Columns="20" Rows="2" TextMode="MultiLine"></asp:textbox></td>
						<td style="HEIGHT: 5px" colSpan="3"></td>
						<td style="WIDTH: 164px" vAlign="top" colSpan="4"><asp:label id="lblLastUpdateBy" runat="server" Height="3px" CssClass="tableLabel">Last updated by / date</asp:label></td>
						<td vAlign="top" colSpan="4"><FONT face="Tahoma"><cc1:mstextbox id="txtLastUpdatedUserID" tabIndex="14" runat="server" Width="96px" CssClass="textField"
									ReadOnly="True" MaxLength="40"></cc1:mstextbox>&nbsp;
								<cc1:mstextbox id="txtLastUpdatedDate" tabIndex="15" runat="server" Width="142px" CssClass="textField"
									ReadOnly="True" MaxLength="16" TextMaskString="99/99/9999 99:99" TextMaskType="msDateTime"></cc1:mstextbox></FONT></td>
					</tr>
					<tr>
						<td></td>
						<td style="WIDTH: 63px" colSpan="4"></td>
						<td style="WIDTH: 154px" colSpan="4"></td>
						<td colSpan="3"></td>
						<td style="WIDTH: 164px" colSpan="4"></td>
						<td colSpan="4"></td>
					</tr>
					<tr>
						<td></td>
						<td style="WIDTH: 63px" colSpan="4"><asp:button id="btnInsertCons" tabIndex="9" runat="server" Width="63" CssClass="queryButton"
								CausesValidation="False" Text="Insert" Enabled="False"></asp:button></td>
						<td style="WIDTH: 154px" colSpan="4"></td>
						<td colSpan="3"></td>
						<td style="WIDTH: 164px" colSpan="4"></td>
						<td colSpan="4"></td>
					</tr>
					<tr>
						<td></td>
						<td vAlign="top" colSpan="19"><asp:datagrid id="dgAssignedConsignment" tabIndex="10" runat="server" Width="800px" AllowPaging="True"
								OnItemDataBound="dgAssignedConsignment_Bound" OnEditCommand="dgAssignedConsignment_Edit" OnCancelCommand="dgAssignedConsignment_Cancel"
								OnUpdateCommand="dgAssignedConsignment_Update" OnDeleteCommand="dgAssignedConsignment_Delete" ItemStyle-Height="20"
								AutoGenerateColumns="False">
								<SelectedItemStyle CssClass="gridFieldSelected"></SelectedItemStyle>
								<ItemStyle Height="20px" CssClass="gridField"></ItemStyle>
								<Columns>
									<asp:EditCommandColumn ButtonType="LinkButton" UpdateText="&lt;IMG SRC='images/butt-update.gif' border=0 title='Save' &gt;"
										CancelText="&lt;IMG SRC='images/butt-cancel.gif' border=0 title='Cancel' &gt;" EditText="&lt;IMG SRC='images/butt-edit.gif' border=0 title='Update' &gt;">
										<HeaderStyle Width="3%" CssClass="gridHeading"></HeaderStyle>
										<ItemStyle HorizontalAlign="Left"></ItemStyle>
									</asp:EditCommandColumn>
									<asp:ButtonColumn Text="&lt;IMG SRC='images/butt-delete.gif' border=0 title='Delete' &gt;" CommandName="Delete">
										<HeaderStyle HorizontalAlign="Center" Width="3%" CssClass="gridHeading"></HeaderStyle>
										<ItemStyle HorizontalAlign="Center"></ItemStyle>
									</asp:ButtonColumn>
									<asp:TemplateColumn HeaderText="Consignment No.">
										<HeaderStyle Width="12%" CssClass="gridHeading"></HeaderStyle>
										<ItemStyle Wrap="False"></ItemStyle>
										<ItemTemplate>
											<asp:Label CssClass="gridLabel" ID="lblConsignmentNo" Text='<%#DataBinder.Eval(Container.DataItem,"Consignment_No")%>' Runat="server">
											</asp:Label>
										</ItemTemplate>
										<EditItemTemplate>
											<dbCombo:DBCOMBO id="DbConsignmentNo" ValidationProperty="Text" Width="72px" Runat="server" ServerMethod="DbConsignmentNoSelect" ShowDbComboLink="False" TextUpLevelSearchButton=" " Text='<%#DataBinder.Eval(Container.DataItem,"Consignment_No")%>' RegistrationKey='<%#ConfigurationSettings.AppSettings["DbComboRegKey"]%>' TextBoxColumns="22" AutoPostBack="True" >
											</dbCombo:DBCOMBO>
											<asp:requiredfieldvalidator id="reqDbConsignment" Runat="server" ErrorMessage="DbConsignmentNo" ControlToValidate="DbConsignmentNo"
												Display="None"></asp:requiredfieldvalidator>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Charge Code">
										<HeaderStyle Width="15%" CssClass="gridHeading"></HeaderStyle>
										<ItemTemplate>
											<asp:Label CssClass="gridLabel" ID="Label2" Text='<%#DataBinder.Eval(Container.DataItem,"charge_code")%>' Runat="server">
											</asp:Label>
										</ItemTemplate>
										<EditItemTemplate>
											<dbCombo:DBCOMBO id="DbChargeCode" ValidationProperty="Text" Width="36px" Runat="server" ServerMethod="DbChargeCodeSelect" ShowDbComboLink="False" TextUpLevelSearchButton=" " Text='<%#DataBinder.Eval(Container.DataItem,"charge_code")%>' RegistrationKey='<%#ConfigurationSettings.AppSettings["DbComboRegKey"]%>' TextBoxColumns="2" AutoPostBack="True" >
											</dbCombo:DBCOMBO>
											<asp:requiredfieldvalidator id="reqDbChargeCode" Runat="server" ErrorMessage="DbChargeCode" ControlToValidate="DbChargeCode"
												Display="None"></asp:requiredfieldvalidator>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Invoice Amount">
										<HeaderStyle Width="15%" CssClass="gridHeading"></HeaderStyle>
										<ItemTemplate>
											<asp:Label CssClass="gridLabelNumber" ID="lblInvoiceAmt" Runat="server" Enabled="True" Text='<%#DataBinder.Eval(Container.DataItem,"Invoice_Amt","{0:n}")%>'>
											</asp:Label>
										</ItemTemplate>
										<EditItemTemplate>
											<asp:TextBox CssClass="gridTextBoxNumber" Height="21px" ID="txtInvoiceAmt" Runat="server" ReadOnly="True" Text='<%#DataBinder.Eval(Container.DataItem,"Invoice_Amt","{0:n}")%>'>
											</asp:TextBox>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Reason Code">
										<HeaderStyle Width="11%" CssClass="gridHeading"></HeaderStyle>
										<ItemTemplate>
											<asp:Label CssClass="gridLabel" ID="Label3" Text='<%#DataBinder.Eval(Container.DataItem,"reason_code")%>' Runat="server">
											</asp:Label>
										</ItemTemplate>
										<EditItemTemplate>
											<dbCombo:DBCOMBO id="DbReasonCode" ValidationProperty="Text" Width="36px" Runat="server" ServerMethod="DbReasonCodeSelect" ShowDbComboLink="False" TextUpLevelSearchButton=" " Text='<%#DataBinder.Eval(Container.DataItem,"reason_code")%>' RegistrationKey='<%#ConfigurationSettings.AppSettings["DbComboRegKey"]%>' TextBoxColumns="2" AutoPostBack="True" >
											</dbCombo:DBCOMBO>
											<asp:requiredfieldvalidator id="reqReasonCode" Runat="server" ErrorMessage="DbReasonCode" ControlToValidate="DbReasonCode"
												Display="None"></asp:requiredfieldvalidator>
											<!--<asp:TextBox CssClass="gridTextBoxNumber" Height="21px" ID="txtReasonCode" Runat="server" ReadOnly="True" Text='<%#DataBinder.Eval(Container.DataItem,"reason_code")%>'>
											</asp:TextBox>-->
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Description">
										<HeaderStyle Width="30%" CssClass="gridHeading"></HeaderStyle>
										<ItemTemplate>
											<asp:Label CssClass="gridLabel" ID="lblCreditNoteDescription" Text='<%#DataBinder.Eval(Container.DataItem,"Description")%>' Runat="server" Enabled="True">
											</asp:Label>
										</ItemTemplate>
										<EditItemTemplate>
											<asp:TextBox CssClass="gridTextBox" ID="txtCreditNoteDescription" Height="21px" Text='<%#DataBinder.Eval(Container.DataItem,"Description")%>' ReadOnly='True' Runat="server" MaxLength="200">
											</asp:TextBox>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Remark">
										<HeaderStyle Width="15%" CssClass="gridHeading"></HeaderStyle>
										<ItemStyle Width="15%">
										</ItemStyle>
										<ItemTemplate>
											<asp:Label CssClass="gridLabel" ID="lblRemark" Text='<%#DataBinder.Eval(Container.DataItem,"Remark")%>' Runat="server" Enabled="True">
											</asp:Label>
										</ItemTemplate>
										<EditItemTemplate>
											<asp:TextBox CssClass="gridTextBox" ID="txtRemark" Height="21px" Text='<%#DataBinder.Eval(Container.DataItem,"Remark")%>' Runat="server" MaxLength="200">
											</asp:TextBox>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Credit Amount">
										<HeaderStyle Width="10%" CssClass="gridHeading"></HeaderStyle>
										<ItemStyle VerticalAlign="Middle"></ItemStyle>
										<ItemTemplate>
											<asp:Label CssClass="gridLabelNumber" ID="lblCreditAmt" Text='<%#DataBinder.Eval(Container.DataItem,"Amt","{0:n}")%>' Runat="server" Enabled="True">
											</asp:Label>
										</ItemTemplate>
										<EditItemTemplate>
											<cc1:mstextbox ID="txtCreditAmt" width="80px" Text='<%#DataBinder.Eval(Container.DataItem,"Amt","{0:n}")%>' Height="21px" Runat="server" CssClass="gridTextBoxNumber" AutoPostBack="false" TextMaskType="msNumeric" MaxLength="11" NumberMaxValue="99999999" NumberMinValue="0" NumberPrecision="10" NumberScale="2" TextMaskString="#.00" onblur="round(this,2)">
											</cc1:mstextbox>
										</EditItemTemplate>
									</asp:TemplateColumn>
								</Columns>
								<PagerStyle NextPageText="Next" PrevPageText="Previous" HorizontalAlign="Right" CssClass="normalText"></PagerStyle>
							</asp:datagrid></td>
					</tr>
				</table>
				<asp:textbox style="Z-INDEX: 104; LEFT: 432px; POSITION: absolute; TOP: 8px" id="txtDNtest" runat="server"
					Width="16px" CssClass="textFieldRightAlign" Visible="False"></asp:textbox><asp:button style="Z-INDEX: 116; LEFT: 320px; POSITION: absolute; TOP: 8px" id="btnRetrieve"
					tabIndex="16" runat="server" Width="62px" CssClass="queryButton" CausesValidation="False" Text="Retrieve"></asp:button><asp:button style="Z-INDEX: 115; LEFT: 272px; POSITION: absolute; TOP: 8px" id="btnPrint" tabIndex="16"
					runat="server" Width="48px" CssClass="queryButton" CausesValidation="False" Text="Print"></asp:button><asp:button style="Z-INDEX: 113; LEFT: 384px; POSITION: absolute; TOP: 8px" id="btnCancel" tabIndex="16"
					runat="server" Width="48px" CssClass="queryButton" CausesValidation="False" Text="Cancel" Visible="False"></asp:button><asp:label style="Z-INDEX: 117; LEFT: 448px; POSITION: absolute; TOP: 8px" id="lblNumRec" runat="server"
					Width="163px" Height="19px" CssClass="RecMsg"></asp:label></div>
			<input 
value="<%=strScrollPosition%>" type=hidden name=ScrollPosition>
		</form>
	</body>
</HTML>
