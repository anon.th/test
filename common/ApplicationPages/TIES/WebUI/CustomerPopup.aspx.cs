using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Text;
using com.common.DAL;
using com.common.classes;
using com.common.util;
using com.ties.classes;
using com.common.applicationpages;

namespace TIES.WebUI
{
	/// <summary>
	/// Summary description for CustomerDetails.
	/// </summary>
	public class CustomerDetails : BasePopupPage
	{
		protected System.Web.UI.WebControls.Label lblCustID;
		protected System.Web.UI.WebControls.Label lblCustName;
		protected System.Web.UI.WebControls.TextBox txtCustName;
		protected System.Web.UI.WebControls.TextBox txtCustID;
		protected System.Web.UI.WebControls.DataGrid dgCustomer;
		protected System.Web.UI.WebControls.Button btnRefresh;
		private String appID = null;
		private String enterpriseID = null;
		SessionDS m_sdsCustomer =null;
		protected System.Web.UI.WebControls.Button btnSearch;
		protected System.Web.UI.WebControls.Label CustErrMsg;
		//Utility utility = null;
		string strFORMID=null;
		protected System.Web.UI.WebControls.Button btnClose;
		string strCustomer=null;
		string strCustType=null;
		protected System.Web.UI.WebControls.Label lblZipcode;
		protected System.Web.UI.WebControls.TextBox txtZipCode;
		string strSalesID=null;
		private void Page_Load(object sender, System.EventArgs e)
		{
			//event javascript check upper case for postal code------PAK==21 June 2013
			this.txtZipCode.Attributes.Add("onKeyPress", "return UpperMaskSpecialWithHyphen(this)");
			// Put user code to initialize the page here
			//utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings,Page.Session);
			appID = utility.GetAppID();
			enterpriseID = utility.GetEnterpriseID();
			strCustomer = Request.Params["CustID"];
			strFORMID= Request.Params["FORMID"]; 
			strCustType = Request.Params["CustType"];
			strSalesID = Request.Params["SalesID"];
			if (!IsPostBack)
			{
				m_sdsCustomer = GetEmptyCustomer(0); 
				txtCustID.Text = Request.Params["CUSTID_TEXT"];
				BindGrid();

				btnSearch_Click(null,null);
			}
			else
			{
				m_sdsCustomer = (SessionDS)ViewState["CUST_DS"];
			}
		}

		public static SessionDS GetEmptyCustomer(int intRows)
		{
			DataTable dtCustomer = new DataTable();
			dtCustomer.Columns.Add(new DataColumn("custid", typeof(string)));
			dtCustomer.Columns.Add(new DataColumn("cust_name", typeof(string)));
			dtCustomer.Columns.Add(new DataColumn("address1", typeof(string)));
			dtCustomer.Columns.Add(new DataColumn("address2", typeof(string)));
			dtCustomer.Columns.Add(new DataColumn("zipcode", typeof(string)));
			dtCustomer.Columns.Add(new DataColumn("apply_esa_surcharge",typeof(string)));    
			for(int i=0; i < intRows; i++)
			{
				DataRow drEach = dtCustomer.NewRow();
				drEach[0] = "";
				drEach[1] = "";
				dtCustomer.Rows.Add(drEach);
			}
			DataSet dsCustIdFields = new DataSet();
			dsCustIdFields.Tables.Add(dtCustomer);
			SessionDS sessionDS = new SessionDS();
			sessionDS.ds = dsCustIdFields;
			sessionDS.DataSetRecSize = dsCustIdFields.Tables[0].Rows.Count;
			sessionDS.QueryResultMaxSize = sessionDS.DataSetRecSize;
			return sessionDS;
		}

		public void Paging(Object sender, DataGridPageChangedEventArgs e)
		{
			//runs when one of the page control is clicked
			//update the current page from the parameter values
			dgCustomer.CurrentPageIndex = e.NewPageIndex;
			ShowCurrentPage();
			//bind data to the grid
			//RefreshData();
		}
		
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			base.OnInit(e);
			InitializeComponent();
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
			this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
			this.dgCustomer.SelectedIndexChanged += new System.EventHandler(this.dgCustomer_SelectedIndexChanged);
			this.ID = "CustomerDetails";
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private void btnCancel_Click(object sender, System.EventArgs e)
		{
		
		}

		private void dgCustomer_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			string strCheckedValue = "true";
			CustErrMsg.Text = "";
			//Get the value and return it to the parent window
			int iSelIndex = dgCustomer.SelectedIndex;

			DataGridItem dgRow = dgCustomer.Items[iSelIndex];
			String strCustID = dgRow.Cells[0].Text;
			String strCustName = dgRow.Cells[1].Text;
			String strAddress1 = dgRow.Cells[2].Text;
			String strAddress2 = dgRow.Cells[3].Text;
			String strZipCode = dgRow.Cells[4].Text;
			String strCountry = dgRow.Cells[5].Text;
			String strPODSlipReqrd = dgRow.Cells[6].Text;
			String strEsa = dgRow.Cells[7].Text ; 
			String strPaymentMode = dgRow.Cells[8].Text ; 
			String strTelephone = dgRow.Cells[9].Text;
			String strFax = dgRow.Cells[10].Text;
			String strContactPer = dgRow.Cells[11].Text;
			String strSalesmanID = dgRow.Cells[12].Text;
			
			if(strCustName == "&nbsp;")
			{
				strCustName = "";
			}
			if(strAddress1 == "&nbsp;")
			{
				strAddress1 = "";
			}
			if(strAddress2 == "&nbsp;")
			{
				strAddress2 = "";
			}
			if(strZipCode == "&nbsp;")
			{
				strZipCode = "";
			}
			if(strCountry == "&nbsp;")
			{
				strCountry = "";
			}
			if(strTelephone == "&nbsp;")
			{
				strTelephone = "";
			}
			if(strFax == "&nbsp;")
			{
				strFax = "";
			}
			if(strSalesmanID == "&nbsp;")
			{
				strSalesmanID = "";
			}
			if(strContactPer == "&nbsp;")
			{
				strContactPer = "";
			}
					
			//Get the Country
			Enterprise enterprise = new Enterprise();
			enterprise.Populate(appID,enterpriseID);
			if(strCountry.Length == 0)
			{
				strCountry = enterprise.Country;
			}
			//Check domestic shipment only for same country
			/*if((strFORMID.Equals("DomesticShipment")) && (!strCountry.ToUpper().Equals(enterprise.Country.ToUpper())))
			{
				CustErrMsg.Text=Utility.GetLanguageText(ResourceType.UserMessage, "INVALID_COUNTRY", utility.GetUserCulture());
				return;
			}*/
			//Get the state & display...
			Zipcode zipCode = new Zipcode();
			zipCode.Populate(appID,enterpriseID,strCountry,strZipCode);
			String strState = zipCode.StateName;
			String strRouteCode = zipCode.DeliveryRoute;
			
			String strIsChecked = "false";

			if(strPODSlipReqrd == "Y")
			{
				strIsChecked = "true";
			}
					
			String sScript = "";
			sScript += "<script language=javascript>";				
			sScript +="\n";

//Removed by CRTS 954 Turn off Locking from over Credit Limit
			if(strFORMID.Equals("DomesticShipment"))
			{
				Customer customer = new Customer();
				customer.Populate(appID,enterpriseID,strCustID);
				sScript += "  window.opener.callCodeBehind('"+strCustID+"');";
				sScript += "  window.opener.__doPostBack();";
			}
			else if(strFORMID.Equals("BandQuotation"))
			{
				sScript += "  window.opener."+strFORMID+".txtCustID.value = \"" + strCustID + "\";";
				sScript += "  window.opener."+strFORMID+".txtCustName.value = \"" + strCustName + "\";";
				sScript += "  window.opener."+strFORMID+".txtAttentionTo.value = \"" + strContactPer + "\";";
				sScript += "  window.opener."+strFORMID+".txtSalesmanId.value = \"" + strSalesmanID + "\";";
			}
			else if (strFORMID.Equals("ShipmentTrackingQuery"))
			{
				sScript += "window.opener."+strFORMID+".txtPayerCode.value = \"" + strCustID + "\";";
			}
			else if (strFORMID.Equals("ShipmentTrackingQuery_New"))
			{
				sScript += "window.opener."+strFORMID+".txtPayerCode.value = \"" + strCustID + "\";";
			}
			else if (strFORMID.Equals("ShipmentTrackingQueryRpt"))
			{
				sScript += "window.opener."+strFORMID+".txtPayerCode.value = \"" + strCustID + "\";";
			}
			else if (strFORMID.Equals("ShipmentPendingPODReport"))
			{
				sScript += "window.opener."+strFORMID+".txtPayerCode.value = \"" + strCustID + "\";";
			}
			else if (strFORMID.Equals("ShipmentExceptionReport"))
			{
				sScript += "window.opener."+strFORMID+".txtPayerCode.value = \"" + strCustID + "\";";
			}
			else if (strFORMID.Equals("ServiceFailure"))
			{
				sScript += "window.opener."+strFORMID+".txtPayerCode.value = \"" + strCustID + "\";";
			}
			else if (strFORMID.Equals("DispatchTrackingReport"))
			{
				sScript += "window.opener."+strFORMID+".txtPayerCode.value = \"" + strCustID + "\";";
			}
			else if (strFORMID.Equals("DispatchTracking"))
			{
				sScript += "window.opener."+strFORMID+".txtPayerID.value = \"" + strCustID + "\";";
			}
			else if (strFORMID.Equals("RevenueCostReportQuery"))
			{
				sScript += "window.opener."+strFORMID+".txtPayerCode.value = \"" + strCustID + "\";";
			}
			else if (strFORMID.Equals("MarginAnalysisReport"))
			{
				sScript += "window.opener."+strFORMID+".txtPayerCode.value = \"" + strCustID + "\";";
			}
			else if (strFORMID.Equals("RevenueByStateReport"))
			{
				sScript += "window.opener."+strFORMID+".txtPayerCode.value = \"" + strCustID + "\";";
			}
			else if (strFORMID.Equals("PODReportQuery"))
			{
				sScript += "window.opener."+strFORMID+".txtPayerCode.value = \"" + strCustID + "\";";
			}
			else if (strFORMID.Equals("ServiceQualityIndicatorQuery"))
			{
				sScript += "window.opener."+strFORMID+".txtPayerCode.value = \"" + strCustID + "\";";
			}
			//Jeab 14 Jun 2011
			else if (strFORMID.Equals("PODEXRptQuery"))
			{
				sScript += "window.opener."+strFORMID+".txtPayerCode.value = \"" + strCustID + "\";";
			}
			//Jeab 14 Jun 2011  =========> End
			//Jeab 17 Jun 2011
			else if (strFORMID.Equals("RevenueBySalesIDRptQuery"))
			{
				sScript += "window.opener."+strFORMID+".txtPayerCode.value = \"" + strCustID + "\";";
			}
			//Jeab 17 Jun 2011  =========> End
			else if (strFORMID.Equals("SalesTerritoryReportQuery"))
			{
				sScript += "window.opener."+strFORMID+".txtPayerCode.value = \"" + strCustID + "\";";
			}
			else if (strFORMID.Equals("AverageDeliveryTimeReportQuery"))
			{
				sScript += "window.opener."+strFORMID+".txtPayerCode.value = \"" + strCustID + "\";";
			}
				//PSA# 2009-014
			else if (strFORMID.Equals("TotalAverageDeliveryTimeReportQuery"))
			{
				sScript += "window.opener."+strFORMID+".txtPayerCode.value = \"" + strCustID + "\";";
			}
				//PSA# 2009-014
				//HC Return Task
			else if (strFORMID.Equals("PendingHCReport"))
			{
				sScript += "window.opener."+strFORMID+".txtPayerCode.value = \"" + strCustID + "\";";
			}
				//HC Return Task
				//HC Return Task
			else if (strFORMID.Equals("ServiceExceptionHCReport"))
			{
				sScript += "window.opener."+strFORMID+".txtPayerCode.value = \"" + strCustID + "\";";
			}
				//HC Return Task
			else if (strFORMID.Equals("PostDeliveryCOD"))
			{
				sScript += "window.opener."+strFORMID+".txtCustID.value = \"" + strCustID + "\";";
			}
			else if (strFORMID.Equals("TopNReport"))
			{
				sScript += "window.opener."+strFORMID+".txtPayerCode.value = \"" + strCustID + "\";";
			}
			else if (strFORMID.Equals("AvgDeliveryTimeReport"))
			{
				sScript += "window.opener."+strFORMID+".txtPayerCode.value = \"" + strCustID + "\";";
			}
			else if (strFORMID.Equals("InvoiceGeneration"))
			{
				sScript += "window.opener."+strFORMID+".txtCustomerCode.value = \"" +strCustID + "\";";
				sScript += "window.opener."+strFORMID+".txtCustomerName.value = \"" +strCustName + "\";";
			}
				// by Ching Nov 30,2007
			else if (strFORMID.Equals("ShipmentPendingCOD"))
			{
				sScript += "window.opener."+strFORMID+".txtPayerCode.value = \"" + strCustID + "\";";
			}
			else if (strFORMID.Equals("CODAmountCollectedReport"))
			{
				sScript += "window.opener."+strFORMID+".txtPayerCode.value = \"" + strCustID + "\";";
			}
			else if (strFORMID.Equals("CODRemittanceReport"))
			{
				sScript += "window.opener."+strFORMID+".txtPayerCode.value = \"" + strCustID + "\";";
			}
				// End
			else if (strFORMID.Equals("InvoicePrintingQuery"))
			{
				sScript += "window.opener."+strFORMID+".txtCustomerCode.value = \"" +strCustID + "\";";
				sScript += "window.opener."+strFORMID+".txtCustomerName.value = \"" +strCustName + "\";";
			}
			else if (strFORMID.Equals("CustomerScheduledPickups"))
			{//Phase2 - L02
				String ReferAddr1 = Request.Params["ReferAddr1"];
				sScript += "window.opener."+strFORMID+".txtCustomerCode.value = \"" +strCustID + "\";";
				sScript += "window.opener."+strFORMID+".txtCustomerName.value = \"" +strCustName + "\";";
				if (ReferAddr1.Trim() == "")
					sScript += "window.opener."+strFORMID+".txtSendAddr1.value = \"" +strAddress1 + "\";";
			}//Phase2 - L02
			else if (strFORMID.Equals("CreditAgingReport"))
			{
				sScript += "window.opener."+strFORMID+".txtCustID.value = \"" + strCustID + "\";";
			}
			else if(strFORMID.Equals("ImportConsignments"))
			{
				String payerid= Request.Params["payerid"]; 
				String payer_name= Request.Params["payer_name"]; 
				String payer_address1= Request.Params["payer_address1"]; 
				String payer_address2= Request.Params["payer_address2"]; 
				String payer_zipcode= Request.Params["payer_zipcode"]; 
				String payer_telephone= Request.Params["payer_telephone"]; 
				String payer_fax= Request.Params["payer_fax"]; 
				String payer_payment_mode= Request.Params["payer_payment_mode"]; 

				sScript += "  window.opener."+strFORMID+"." + payerid + ".value = \"" + strCustID + "\";";
				sScript += "  window.opener."+strFORMID+"." + payer_name + ".value = \"" + strCustName + "\";";
				sScript += "  window.opener."+strFORMID+"." + payer_address1 + ".value = \"" + strAddress1 + "\";";
				sScript += "  window.opener."+strFORMID+"." + payer_address2 + ".value = \"" + strAddress2 + "\";";
				sScript += "  window.opener."+strFORMID+"." + payer_zipcode + ".value = \"" + strZipCode + "\";";
				sScript += "  window.opener."+strFORMID+"." + payer_telephone + ".value = \"" + strTelephone + "\";";
				sScript += "  window.opener."+strFORMID+"." + payer_fax + ".value = \"" + strFax +  "\";";

				//select payment_mode DropDownList
				sScript += "  window.opener."+strFORMID+"." + payer_payment_mode + ".value = \"" + strPaymentMode +  "\";";
				sScript += "  window.opener."+strFORMID+"." + payer_payment_mode + ".disabled = " + strPaymentMode.Equals("C").ToString().ToLower() +  ";";
			}
			else
			{
				sScript += "  window.opener."+strFORMID+".txtCustID.value = \"" + strCustID + "\";";
				sScript += "  window.opener."+strFORMID+".txtCustName.value = \"" + strCustName + "\";";
				sScript += "  window.opener."+strFORMID+".txtCustAddr1.value =\"" + strAddress1 + "\";";
				sScript += "  window.opener."+strFORMID+".txtCustAdd2.value = \"" + strAddress2 + "\";";
				sScript += "  window.opener."+strFORMID+".txtCustZipCode.value = \"" + strZipCode + "\";";
				sScript += "  window.opener."+strFORMID+".Txt_Country.value = \"" + strCountry + "\";";
				sScript += "  window.opener."+strFORMID+".Txt_StateCode.value = \"" + strState + "\";";
				sScript += "  window.opener."+strFORMID+".Txt_Telephone.value = \"" + strTelephone + "\";";
				sScript += "  window.opener."+strFORMID+".Txt_Fax.value = \"" + strFax + "\";";
				
				if (strPaymentMode=="R")
				{
					sScript += "  window.opener."+strFORMID+".rd_Credit.checked = " + strCheckedValue + ";";
					sScript += "  window.opener."+strFORMID+".rd_Credit.disabled = false;";
					sScript += "  window.opener."+strFORMID+".Rd_Cash.disabled = false;";
				}
				else if(strPaymentMode=="C")
				{
					sScript += "  window.opener."+strFORMID+".Rd_Cash.checked = " + strCheckedValue + ";";
					sScript += "  window.opener."+strFORMID+".rd_Credit.disabled = true;";
					sScript += "  window.opener."+strFORMID+".Rd_Cash.disabled = true;";
				}

//Removed by CRTS 954 Turn off Locking from over Credit Limit
				if(strFORMID.Equals("PickupRequestBooking"))
				{
					try
					{
						if(com.ties.DAL.CustomerProfileDAL.Check_IsCreditStatusNotAvail(utility.GetAppID(),utility.GetEnterpriseID(),txtCustID.Text.Trim()))
						{
										sScript += "  window.opener.document.getElementById('ErrorMsg').innerHTML='"+Utility.GetLanguageText(ResourceType.UserMessage, "MSG_PRB_CRSTATUS_NA", utility.GetUserCulture())+"';";
										sScript += "  window.opener."+strFORMID+".Rd_Cash.checked = " + strCheckedValue + ";";
										sScript += "  window.opener."+strFORMID+".rd_Credit.disabled = true;";
										sScript += "  window.opener."+strFORMID+".Rd_Cash.disabled = true;";
										sScript += "  window.opener."+strFORMID+".Btn_Save.disabled = true;";
								
						}

					}
					catch
					{
					}
				}
//End by CRTS 954 Turn off Locking from over Credit Limit

			}

			sScript += "  window.close();";
			sScript += "\n";
			sScript += "</script>";
			
			//Response.Write("<script>alert('"+sScript+"');</script>");
			Response.Write(sScript);
		}

		private void btnSearch_Click(object sender, System.EventArgs e)
		{
			StringBuilder strQry = new StringBuilder();
			strQry.Append("select * from customer where applicationid='");
			strQry.Append(appID);
			strQry.Append("' and enterpriseid = '");
			strQry.Append(enterpriseID);
			strQry.Append("'");
			if (txtCustID.Text.ToString().Trim() != "")
			{
				strQry.Append(" and custid like '%");
				strQry.Append( Utility.ReplaceSingleQuote(txtCustID.Text.ToString().Trim()));
				strQry.Append("%'");
			}
			if (txtCustName.Text.ToString().Trim() != "")
			{
				strQry.Append(" and cust_name like N'%");
				strQry.Append(Utility.ReplaceSingleQuote(txtCustName.Text.ToString().Trim()));
				strQry.Append("%'");
			}
			if (txtZipCode.Text.ToString().Trim() != "")
			{
				strQry.Append(" and zipcode like N'%");
				strQry.Append(Utility.ReplaceSingleQuote(txtZipCode.Text.ToString().Trim()));
				strQry.Append("%'");
			}
			if (strCustType != null && strCustType.ToString() != "")
			{
				String tmpStrCustType = Utility.ReplaceSingleQuote(strCustType.ToString().Trim());
				String newStrCustType = "";

				if(tmpStrCustType.Length == 1)
				{
					newStrCustType = "'" + tmpStrCustType + "'";
				}
				else
				{
					String[] arrOfStr = tmpStrCustType.Split(',');

					foreach(String str in arrOfStr)
					{
						newStrCustType += "'" + str + "',";
					}

					newStrCustType = newStrCustType.Substring(0, newStrCustType.Length - 1);
				}
				// add if by Tumz.
				if(strCustType != "0")
				{
					strQry.Append(" and payer_type in (");
					strQry.Append(newStrCustType);
					strQry.Append(")");
				}
			}

			strQry.Append(" and status_active = 'Y'");

			if (strSalesID != null && strSalesID.ToString() != "")
			{
				strQry.Append(" and salesmanid = '");
				strQry.Append(Utility.ReplaceSingleQuote(strSalesID));
				strQry.Append("'");
			}

			String strSQLQuery = strQry.ToString();
			ViewState["SQL_QUERY"] = strSQLQuery;
			dgCustomer.CurrentPageIndex = 0;
			ShowCurrentPage();
		}

		private void BindGrid()
		{
			dgCustomer.VirtualItemCount = System.Convert.ToInt32(m_sdsCustomer.QueryResultMaxSize);
			dgCustomer.DataSource = m_sdsCustomer.ds;
			dgCustomer.DataBind();
			ViewState["CUST_DS"] = m_sdsCustomer;
		}

		private void ShowCurrentPage()
		{
			int iStartIndex = dgCustomer.CurrentPageIndex * dgCustomer.PageSize;
			String sqlQuery = (String)ViewState["SQL_QUERY"];
			m_sdsCustomer = GetCUSTDDS(iStartIndex,dgCustomer.PageSize,sqlQuery);
			
			decimal pgCnt = (m_sdsCustomer.QueryResultMaxSize - 1)/dgCustomer.PageSize;
			if(pgCnt < dgCustomer.CurrentPageIndex)
			{
				dgCustomer.CurrentPageIndex = System.Convert.ToInt32(pgCnt);
			}
			dgCustomer.SelectedIndex = -1;
			dgCustomer.EditItemIndex = -1;

			BindGrid();
			getPageControls(Page);
		}

		private SessionDS GetCUSTDDS(int iCurrent, int iDSRecSize, String strQuery)
		{
			SessionDS sessionDS = null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(appID,enterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("CustomerPopup.aspx.cs","GetCUSTDDS","ERR001","DbConnection object is null!!");
				return sessionDS;
			}
			sessionDS = dbCon.ExecuteQuery(strQuery,iCurrent,iDSRecSize,"ZoneCode");
			return  sessionDS;
		}

		private void btnClose_Click(object sender, System.EventArgs e)
		{
			String sScript = "";
			sScript += "<script language=javascript>";
			sScript += "  window.close();";
			sScript += "</script>";
			Response.Write(sScript);
		}	
	}
}