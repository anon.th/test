using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.common.classes;
using com.common.RBAC;
using com.common.util;
using com.common.applicationpages;
using com.ties.DAL;
using com.ties.classes;
using System.Text.RegularExpressions;

 
namespace com.ties
{
	/// <summary>
	/// Summary description for DispatchTrackingReport.
	/// </summary>
	public class DispatchTrackingReport :com.common.applicationpages.BasePage
	{
		#region Web Form Designer generated code

		protected System.Web.UI.WebControls.Button btnGenerate;
		protected System.Web.UI.WebControls.Label lblTitle;
		protected System.Web.UI.WebControls.Label lblErrorMessage;
		protected System.Web.UI.WebControls.Label lblServiceCode;
		protected System.Web.UI.WebControls.Label lblConsigNo;
		protected System.Web.UI.WebControls.Label lblPayer;
		protected System.Web.UI.WebControls.RadioButton rbDate;
		protected System.Web.UI.WebControls.RadioButton rbPeriod;
		protected System.Web.UI.WebControls.DropDownList ddMonth;
		protected System.Web.UI.WebControls.RadioButton rbMonth;
		protected com.common.util.msTextBox txtYear;
		protected com.common.util.msTextBox txtPeriod;
		protected com.common.util.msTextBox txtTo;
		protected com.common.util.msTextBox txtDate;
		protected com.common.util.msTextBox txtConsigNo;
		protected System.Web.UI.WebControls.Label lblBookingNo;
		protected System.Web.UI.WebControls.Label lblDispatchType;
		protected System.Web.UI.WebControls.Label lblStatusCode;
		protected System.Web.UI.WebControls.Label lblExceptionCode;
		protected System.Web.UI.WebControls.DropDownList ddDelvType;
		protected System.Web.UI.WebControls.Label lblSenderName;
		protected System.Web.UI.WebControls.Button btnQuery;
		protected Cambro.Web.DbCombo.DbCombo Dbcombo1;
		protected Cambro.Web.DbCombo.DbCombo dbCmbSenderName;
		protected Cambro.Web.DbCombo.DbCombo dbCmbBookingNo;
		protected Cambro.Web.DbCombo.DbCombo dbCmbStatusCode;
		protected Cambro.Web.DbCombo.DbCombo dbCmbExceptionCode;
		protected System.Web.UI.WebControls.Label lblYear;
		protected System.Web.UI.HtmlControls.HtmlTable tblDates;
		protected System.Web.UI.WebControls.Label Label3;
		protected System.Web.UI.WebControls.Label Label1;
		protected System.Web.UI.WebControls.Label Label2;
		protected System.Web.UI.HtmlControls.HtmlTable tblDates1;
		protected System.Web.UI.HtmlControls.HtmlTable tblDates2;
		protected System.Web.UI.WebControls.Label lblDates;
		protected System.Web.UI.WebControls.RadioButton rbBookingDate;
		protected System.Web.UI.WebControls.Label lblPayerType;
		protected System.Web.UI.WebControls.Label Label4;
		protected System.Web.UI.WebControls.Label lblPayerCode;
		protected System.Web.UI.WebControls.TextBox txtPayerCode;
		protected System.Web.UI.WebControls.Button btnPayerCode;
		protected System.Web.UI.WebControls.Label lblRouteType;
		protected System.Web.UI.WebControls.Label lblRouteCode;
		protected Cambro.Web.DbCombo.DbCombo DbComboPathCode;
		protected Cambro.Web.DbCombo.DbCombo DbComboOriginDC;
		protected System.Web.UI.HtmlControls.HtmlTable tblShipmentTracking;
		protected System.Web.UI.HtmlControls.HtmlTable tblPayerType;
		protected System.Web.UI.HtmlControls.HtmlTable tblRouteType;
		protected System.Web.UI.WebControls.RadioButton rbActDate;
		protected System.Web.UI.WebControls.ListBox lsbCustType;
		protected System.Web.UI.WebControls.Label Label5;
		protected System.Web.UI.HtmlControls.HtmlTable Table1;
		protected System.Web.UI.WebControls.Label Label6;
		protected com.common.util.msTextBox txtCutoffTime;
		protected System.Web.UI.WebControls.RadioButton rbEstDate;

		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			base.OnInit(e);
			InitializeComponent();
			
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnQuery.Click += new System.EventHandler(this.btnQuery_Click);
			this.rbBookingDate.CheckedChanged += new System.EventHandler(this.rbBookingDate_CheckedChanged);
			this.rbEstDate.CheckedChanged += new System.EventHandler(this.rbEstDate_CheckedChanged);
			this.rbActDate.CheckedChanged += new System.EventHandler(this.rbActDate_CheckedChanged);
			this.rbMonth.CheckedChanged += new System.EventHandler(this.rbMonth_CheckedChanged);
			this.rbPeriod.CheckedChanged += new System.EventHandler(this.rbPeriod_CheckedChanged);
			this.rbDate.CheckedChanged += new System.EventHandler(this.rbDate_CheckedChanged);
			this.btnPayerCode.Click += new System.EventHandler(this.btnPayerCode_Click);
			this.btnGenerate.Click += new System.EventHandler(this.btnGenerate_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private String m_strAppID;
		private String m_strEnterpriseID;
		private String m_strCulture;
		DataSet m_dsQuery=null;
	
		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			m_strAppID = utility.GetAppID();
			m_strEnterpriseID = utility.GetEnterpriseID();
			m_strCulture = utility.GetUserCulture();

			DbComboOriginDC.ClientOnSelectFunction="makeUppercase('DbComboOriginDC:ulbTextBox');";
			//DbComboDestinationDC.ClientOnSelectFunction="makeUppercase('DbComboDestinationDC:ulbTextBox');";

			DbComboPathCode.RegistrationKey = System.Configuration.ConfigurationSettings.AppSettings["DbComboRegKey"];
			DbComboOriginDC.RegistrationKey = System.Configuration.ConfigurationSettings.AppSettings["DbComboRegKey"];
			dbCmbSenderName.RegistrationKey = System.Configuration.ConfigurationSettings.AppSettings["DbComboRegKey"];				
			dbCmbBookingNo.RegistrationKey = System.Configuration.ConfigurationSettings.AppSettings["DbComboRegKey"];
			dbCmbStatusCode .RegistrationKey = System.Configuration.ConfigurationSettings.AppSettings["DbComboRegKey"];
			dbCmbExceptionCode.RegistrationKey = System.Configuration.ConfigurationSettings.AppSettings["DbComboRegKey"];
			
			if(!IsPostBack)
			{
				DefaultScreen();
				ddlmonths();
				LoadCustomerTypeList();
				ddlDelvType();
			}

			SetDbComboServerStates();
			SetExceptionCodeServerStates();
		}


		private void btnGenerate_Click(object sender, System.EventArgs e)
		{
			lblErrorMessage.Text="";
			bool icheck = ValidateValues(); 
//			Session["FORMID"]="DISPATCH TRACKING"; 
		//Jeab 04 Jan 11
//			string strDispatchType = ddDelvType.SelectedValue.ToString();
//			if (strDispatchType == "W" )
//			{
				Session["FORMID"]="DISPATCH TRACKING_W"; 
//			}
		//Jeab 04 Jan 11 =========> End
			if(icheck==false)
			{
				m_dsQuery = GetShipmentQueryData();
				if(m_dsQuery == null)
					return;
				Session["SESSION_DS1"] = m_dsQuery;

				String sUrl="";
				sUrl  = "ReportViewer.aspx";
				ArrayList paramList = new ArrayList();
				paramList.Add(sUrl);
				String sScript = Utility.GetScript("openParentWindow.js",paramList);
				Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
			}
		}
	

		private void btnQuery_Click(object sender, System.EventArgs e)
		{
			DefaultScreen(); 
		}


		#region "Page Functions"

		private DataTable CreateEmptyDataTable()
		{
			DataTable dtShipment = new DataTable();

			#region "Dates"
			dtShipment.Columns.Add(new DataColumn("tran_date", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("start_date", typeof(DateTime)));
			dtShipment.Columns.Add(new DataColumn("end_date", typeof(DateTime)));
			#endregion
			
			#region "Payer Type"
			dtShipment.Columns.Add(new DataColumn("payer_type", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("payer_code", typeof(string)));
			#endregion
		
			#region "Route / DC Selection"
			dtShipment.Columns.Add(new DataColumn("route_code", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("origin_dc", typeof(string)));
			#endregion

			#region "Party Details"
			dtShipment.Columns.Add(new DataColumn("senderName", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("bookingNo", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("dispatchType", typeof(string)));
			#endregion

			#region "Status Description"
			dtShipment.Columns.Add(new DataColumn("statusCode", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("exceptionCode", typeof(string)));
			#endregion

			#region
			dtShipment.Columns.Add(new DataColumn("cutoff_time", typeof(string)));
			#endregion

			return dtShipment;
		}


		private DataSet GetShipmentQueryData()
		{
			DataSet dsShipment = new DataSet();
			DataTable dtShipment = CreateEmptyDataTable();
			DataRow dr = dtShipment.NewRow(); 
			
			#region "Dates"
			if (rbBookingDate.Checked) 
			{	// Booking Date Selected
				dr["tran_date"] = "B";
			}
			if (rbEstDate.Checked) 
			{	// Estimated Pickup Date Selected
				dr["tran_date"] = "E";
			}
			if (rbActDate.Checked) 
			{	// Actual Pickup Date Selected
				dr["tran_date"] = "A";
			}
			
			String strStartDate = null;
			DateTime dtStartDate = DateTime.Now;
			DateTime dtEndDate = DateTime.Now;

			if (rbMonth.Checked==true)
			{					
				if (!Regex.IsMatch(txtYear.Text, @"^[0-9]{4}$"))
				{
					lblErrorMessage.Text = "Please Enter year format 'YYYY'";
					return null;
				}

				if(ddMonth.SelectedItem.Value != null && Convert.ToInt32(ddMonth.SelectedItem.Value) < 10)
					strStartDate="01"+"/0"+ddMonth.SelectedItem.Value+"/"+txtYear.Text;
				else
					strStartDate="01"+"/"+ddMonth.SelectedItem.Value+"/"+txtYear.Text.Trim();
				dtStartDate=System.DateTime.ParseExact(strStartDate,"dd/MM/yyyy",null);				
				dtEndDate=dtStartDate.AddMonths(1).AddDays(-1);				
			}
			if (rbPeriod.Checked==true)
			{	
				dtStartDate=System.DateTime.ParseExact(txtPeriod.Text.Trim(),"dd/MM/yyyy",null);									
				dtEndDate=System.DateTime.ParseExact(txtTo.Text.Trim(),"dd/MM/yyyy",null);
			}

			if (rbDate.Checked==true)
			{				
				dtStartDate=System.DateTime.ParseExact(txtDate.Text.Trim(),"dd/MM/yyyy",null);
				dtEndDate=dtStartDate.AddDays(1).AddMinutes(-1);				
			}

			dr["start_date"] = dtStartDate;
			dr["end_date"] = dtEndDate;
			#endregion

			#region "Payer Type"

			string strCustPayerType = "";

			for (int i = 0; i <= lsbCustType.Items.Count - 1; i ++)
			{
				if(lsbCustType.Items[i].Selected == true)
				{
					strCustPayerType += "\"" + lsbCustType.Items[i].Value + "\",";
				}
			}

			if (strCustPayerType != "") 
				dr["payer_type"] = "(" + strCustPayerType.Substring(0,strCustPayerType.Length - 1) + ")";
			else
				dr["payer_type"] = System.DBNull.Value;
			
			dr["payer_code"] = txtPayerCode.Text.Trim();

			#endregion
			
			#region "Route / DC Selection"

			dr["route_code"] = DbComboPathCode.Value;
			dr["origin_dc"] = DbComboOriginDC.Value.ToUpper();
		
			#endregion

			#region "Party Details"

			dr["senderName"] = dbCmbSenderName.Value;

			int iBookingNo=0;
			if(dbCmbBookingNo.Value !="")
				iBookingNo = Convert.ToInt32(dbCmbBookingNo .Value);
			else
				iBookingNo = 0;

			if (Utility.IsNotDBNull(iBookingNo))
				dr["bookingNo"] = iBookingNo ;

			dr["dispatchType"] = ddDelvType.SelectedItem.Value; 

			#endregion

			#region "Status Description"

			dr["statusCode"] = dbCmbStatusCode.Value;
			dr["exceptionCode"] = dbCmbExceptionCode.Value;

			#endregion
			#region
			string strCutoffTime = null;
			strCutoffTime=txtCutoffTime.Text;				
			dr["cutoff_time"] = strCutoffTime;
			#endregion

			dtShipment.Rows.Add(dr);
			dsShipment.Tables.Add(dtShipment);
			return dsShipment;
		}


		private void DefaultScreen()
		{
			#region "Dates"

			rbBookingDate.Checked = true;
			rbEstDate.Checked = false;
			rbActDate.Checked = false;
			rbMonth.Checked = true;
			rbPeriod.Checked = false;
			rbDate.Checked = false;

			ddMonth.Enabled = true;
			ddMonth.SelectedIndex = -1;
			txtYear.Text = null;
			txtYear.Enabled = true;
			txtPeriod.Text = null;
			txtPeriod.Enabled = false;
			txtTo.Text = null;
			txtTo.Enabled = false;
			txtDate.Text = null;
			txtDate.Enabled = false;

			#endregion

			#region "Payer Type"

			lsbCustType.SelectedIndex = -1;
			txtPayerCode.Text = null;

			txtCutoffTime.Text = "";

			#endregion
			
			#region "Route / DC Selection"
			DbComboPathCode.Text="";
			DbComboPathCode.Value="";

			Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings,HttpContext.Current.Session);
			if(util.CheckUserRoleLocation())
			{
				DbComboOriginDC.Text="";
				DbComboOriginDC.Value="";
				DbComboOriginDC.Enabled=true;
			}
			else
			{
				string UserLocation = util.UserLocation();	
				DbComboOriginDC.Text=UserLocation.ToUpper();
				DbComboOriginDC.Value=UserLocation.ToUpper();
				DbComboOriginDC.Enabled=false;
			}

			#endregion

			#region "Party Details"

			dbCmbSenderName.Text="";
			dbCmbSenderName.Value="";

			dbCmbBookingNo.Text="";
			dbCmbBookingNo.Value="";

			ddDelvType.SelectedIndex = -1;

			#endregion

			#region "Status Description"

			dbCmbStatusCode.Text="";
			dbCmbStatusCode.Value="";

			dbCmbExceptionCode.Text="";
			dbCmbExceptionCode.Value="";

			#endregion

			lblErrorMessage.Text = "";
		}


		private bool ValidateValues()
		{
			bool iCheck=false;
			if((ddMonth.SelectedIndex==0)&&(txtYear.Text=="")&&(txtPeriod.Text=="")&&(txtTo.Text=="")&&(txtDate.Text==""))
			{
				lblErrorMessage.Text=Utility.GetLanguageText(ResourceType.UserMessage,"PLS_ENTER_EST_DVL",utility.GetUserCulture());
				return iCheck=true;			
			}
			if(rbMonth.Checked == true)
			{
				if((ddMonth.SelectedIndex>0) &&(txtYear.Text==""))
				{
					lblErrorMessage.Text=Utility.GetLanguageText(ResourceType.UserMessage,"MM_YEAR_REQ",utility.GetUserCulture());
					return iCheck=true;
				}
				else if((ddMonth.SelectedIndex==0) &&(txtYear.Text!=""))
				{
					lblErrorMessage.Text=Utility.GetLanguageText(ResourceType.UserMessage,"MM_YEAR_REQ",utility.GetUserCulture());
					return iCheck=true;
				}
				else
				{
					return iCheck=false;
				}
			}
			if(rbPeriod.Checked == true )
			{
				if((txtPeriod.Text!="")&&(txtTo.Text==""))
				{
					lblErrorMessage.Text=Utility.GetLanguageText(ResourceType.UserMessage,"PLS_ENTER_END_DT",utility.GetUserCulture());
					return iCheck=true;
				}
				else if((txtPeriod.Text=="")&&(txtTo.Text!=""))
				{
					lblErrorMessage.Text=Utility.GetLanguageText(ResourceType.UserMessage,"PLS_START_DT",utility.GetUserCulture());			
					return iCheck=true;
			
				}
				else
				{
					return iCheck=false;
				}
			}
			if(rbDate.Checked == true )
			{
				if(txtDate.Text=="")
				{
					lblErrorMessage.Text=Utility.GetLanguageText(ResourceType.UserMessage,"PLS_ENTER_DT",utility.GetUserCulture());	
					return iCheck=true;			
				}
			}
			
			return iCheck;
			
		}


		#endregion
	
		#region "Dates Part : Controls"

		private void rbBookingDate_CheckedChanged(object sender, System.EventArgs e)
		{
			lblErrorMessage.Text="";
		}


		private void rbEstDate_CheckedChanged(object sender, System.EventArgs e)
		{
			lblErrorMessage.Text="";
		}


		private void rbActDate_CheckedChanged(object sender, System.EventArgs e)
		{
			lblErrorMessage.Text="";
		}


		private void rbMonth_CheckedChanged(object sender, System.EventArgs e)
		{
			lblErrorMessage.Text="";
			ddMonth.Enabled = true;
			txtYear.Text = null;
			txtYear.Enabled = true;
			txtPeriod.Text = null;
			txtPeriod.Enabled = false;
			txtTo.Text = null;
			txtTo.Enabled = false;
			txtDate.Text = null;
			txtDate.Enabled = false;
		}


		private void rbPeriod_CheckedChanged(object sender, System.EventArgs e)
		{
			lblErrorMessage.Text="";
			ddMonth.Enabled = false;
			ddMonth.SelectedIndex = 0;
			txtYear.Text = null;
			txtYear.Enabled = false;
			txtPeriod.Text = null;
			txtPeriod.Enabled = true;
			txtTo.Text = null;
			txtTo.Enabled = true;
			txtDate.Text = null;
			txtDate.Enabled = false;
		}


		private void rbDate_CheckedChanged(object sender, System.EventArgs e)
		{
			lblErrorMessage.Text="";
			ddMonth.Enabled = false;
			txtYear.Text = null;
			txtYear.Enabled = false;
			txtPeriod.Text = null;
			txtPeriod.Enabled = false;
			txtTo.Text = null;
			txtTo.Enabled = false;
			txtDate.Text = null;
			txtDate.Enabled = true;
		}


		#endregion

		#region "Payer Type Part : Controls

		private void btnPayerCode_Click(object sender, System.EventArgs e)
		{
			string strCustPayerType = "";
			for (int i = 0; i <= lsbCustType.Items.Count - 1; i ++)
			{
				if(lsbCustType.Items[i].Selected == true)
				{
					strCustPayerType += lsbCustType.Items[i].Value + ",";
				}
			}
			if((strCustPayerType != null) && (strCustPayerType.Trim().Length > 0))
				strCustPayerType = strCustPayerType.Substring(0, strCustPayerType.Length - 1);

			String sUrl = "CustomerPopup.aspx?FORMID="+"DispatchTrackingReport"+
				"&CustType="+strCustPayerType.ToString();
			ArrayList paramList = new ArrayList();
			paramList.Add(sUrl);
			String sScript = Utility.GetScript("openWindowParam.js",paramList);
			Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
		}


		#endregion
		
		#region "Route / DC Selection : Controls"

		private void rbLongRoute_CheckedChanged(object sender, System.EventArgs e)
		{
			DbComboPathCode.Text="";
			DbComboPathCode.Value="";
		}


		private void rbShortRoute_CheckedChanged(object sender, System.EventArgs e)
		{
			DbComboPathCode.Text="";
			DbComboPathCode.Value="";	
		}


		private void rbAirRoute_CheckedChanged(object sender, System.EventArgs e)
		{
			DbComboPathCode.Text="";
			DbComboPathCode.Value="";
		}


		#endregion


		#region "Dates Part : DropDownList"

		private void ddlmonths()
		{
			DataTable dtMonths = new DataTable();
			dtMonths.Columns.Add(new DataColumn("Text", typeof(string)));
			dtMonths.Columns.Add(new DataColumn("StringValue", typeof(string)));
		
			DataRow drNilRow = dtMonths.NewRow();
			drNilRow[0] = "";
			drNilRow[1] = "";
			dtMonths.Rows.Add(drNilRow);

			ArrayList listMonthTxt = Utility.GetCodeValues(m_strAppID,m_strCulture, "months", CodeValueType.StringValue);
			foreach(SystemCode typeSysCode in listMonthTxt)
			{
				DataRow drEach = dtMonths.NewRow();
				drEach[0] = typeSysCode.Text;
				drEach[1] = typeSysCode.StringValue;
				dtMonths.Rows.Add(drEach);
			}
			DataView dvMonths = new DataView(dtMonths);

			ddMonth.DataSource = dvMonths;
			ddMonth.DataTextField = "Text";
			ddMonth.DataValueField = "StringValue";
			ddMonth.DataBind();	
		}


		#endregion

		#region "Payer Type Part : DropDownList"

		public void LoadCustomerTypeList()
		{
			ArrayList systemCodes = Utility.GetCodeValues(utility.GetAppID(),utility.GetUserCulture(),"customer_type",CodeValueType.StringValue);
			foreach(SystemCode sysCode in systemCodes)
			{	
				ListItem lstItem = new ListItem();
				lstItem.Text = sysCode.Text;
				lstItem.Value = sysCode.StringValue;
				lsbCustType.Items.Add(lstItem);
			}
		}


		#endregion

		#region "Route / DC Selection : DropDownList"

		private void SetDbComboServerStates()
		{
			String strDeliveryType="S";
			Hashtable hash = new Hashtable();
			hash.Add("strDeliveryType", strDeliveryType);
			DbComboPathCode.ServerState = hash;
			DbComboPathCode.ServerStateSecretString = "kdfsjgnskajh[apq2-4uhg465654435879mnbfdm cxs;q4";
		}


		[Cambro.Web.DbCombo.ResultsMethodAttribute(true)]
		public static object DbComboPathCodeSelect(Cambro.Web.DbCombo.ServerMethodArgs args)
		{
			Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings,HttpContext.Current.Session);
			String strAppID = util.GetAppID();
			String strEnterpriseID = util.GetEnterpriseID();
			
			String strDelType = "L";			
			String strWhereClause=null;
			if(args.ServerState != null && args.ServerState.Authenticate("kdfsjgnskajh[apq2-4uhg465654435879mnbfdm cxs;q4"))
			{
				if(args.ServerState["strDeliveryType"] != null && args.ServerState["strDeliveryType"].ToString().Length > 0)
				{
					strDelType = args.ServerState["strDeliveryType"].ToString();
					strWhereClause=" and Delivery_Type='"+Utility.ReplaceSingleQuote(strDelType)+"' ";
					if(strDelType == "S") 
					{
						strWhereClause=strWhereClause+"and path_code in ( ";
						strWhereClause=strWhereClause+"select distinct pickup_route ";
						strWhereClause=strWhereClause+"from Zipcode ";
						strWhereClause=strWhereClause+"where applicationid = '"+strAppID+"' ";
						strWhereClause=strWhereClause+"and enterpriseid = '"+ strEnterpriseID +"') ";
					}
				}				
			}
			
			DataSet dataset = com.ties.classes.DbComboDAL.PathCodeQuery(strAppID,strEnterpriseID,args, strWhereClause);	
			return dataset;
		}


		[Cambro.Web.DbCombo.ResultsMethodAttribute(true)]
		public static object DbComboDistributionCenterSelect(Cambro.Web.DbCombo.ServerMethodArgs args)
		{
			Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings,HttpContext.Current.Session);
			String strAppID = util.GetAppID();
			String strEnterpriseID = util.GetEnterpriseID();
			
			DataSet dataset = com.ties.classes.DbComboDAL.DistributionCenterQuery(strAppID,strEnterpriseID,args);	
			return dataset;
		}


		#endregion

		#region "Party Details : DropDownList"

		private void ddlDelvType()
		{
			DataTable dtDelvTypeOptions = new DataTable();
			dtDelvTypeOptions.Columns.Add(new DataColumn("Text", typeof(string)));
			dtDelvTypeOptions.Columns.Add(new DataColumn("StringValue", typeof(string)));

			DataRow drNilRow = dtDelvTypeOptions.NewRow();
			drNilRow[0] = "";
			drNilRow[1] = "*";
			dtDelvTypeOptions.Rows.Add(drNilRow);

			ArrayList systemCodes = Utility.GetCodeValues(utility .GetAppID (),utility.GetUserCulture(),"booking_type",CodeValueType.StringValue);
			foreach(SystemCode sysCode in systemCodes)
			{
				DataRow drEach = dtDelvTypeOptions.NewRow();
				drEach[0] = sysCode.Text;
				drEach[1] = sysCode.StringValue;
				dtDelvTypeOptions.Rows.Add(drEach);
			}

			DataView dvDelvTypeOptions = new DataView(dtDelvTypeOptions);

			ddDelvType.DataSource = (ICollection)dvDelvTypeOptions;
			ddDelvType.DataTextField = "Text";
			ddDelvType.DataValueField = "StringValue";
			ddDelvType.DataBind();
		}


		[Cambro.Web.DbCombo.ResultsMethodAttribute(true)]
		public static object SenderNameServerMethod(Cambro.Web.DbCombo.ServerMethodArgs args)
		{
			Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings,HttpContext.Current.Session);
			String strAppID = util.GetAppID();
			String strEnterpriseID = util.GetEnterpriseID();

			String strPayerid="";

			if(args.ServerState != null && args.ServerState.Authenticate("kdfsjgnskajh[apq2-4uhg465654435879mnbfdm qrs;q4"))
			{
				if(args.ServerState["strPayerId"] != null && args.ServerState["strPayerId"].ToString().Length > 0)
				{
					strPayerid = args.ServerState["strPayerId"].ToString();					
				}
			}	
			DataSet dataset = DbComboDAL.SenderNameQuery (strAppID,strEnterpriseID,args,strPayerid);	
			return dataset;                
		}


		[Cambro.Web.DbCombo.ResultsMethodAttribute(true)]
		public static object BookingNoServerMethod(Cambro.Web.DbCombo.ServerMethodArgs args)
		{
			Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings,HttpContext.Current.Session);
			String strAppID = util.GetAppID();
			String strEnterpriseID = util.GetEnterpriseID();

			string iBookingNo="";

			if(args.ServerState != null && args.ServerState.Authenticate("kdfsjgnskajh[apq2-4uhg465654435879mnbfdm qrs;q4"))
			{
				if(args.ServerState["iBookingNo"] != null && args.ServerState["iBookingNo"].ToString().Length > 0)
				{
					iBookingNo = args.ServerState["iBookingNo"].ToString();					
				}
			}	
			DataSet dataset = DbComboDAL.BookingNoCodeQuery (strAppID,strEnterpriseID,args);	
			return dataset;                
		}


		#endregion

		#region "Status Description : DropDownList"

		private void SetExceptionCodeServerStates()
		{
			String strStatusCode=dbCmbStatusCode .Value;
			Hashtable hash = new Hashtable();
			hash.Add("strStatusCode", strStatusCode);
			dbCmbExceptionCode  .ServerState = hash;
			dbCmbExceptionCode.ServerStateSecretString = "kdfsjgnskajh[apq2-4uhg465654435879mnbfdm qrs;q4";			
		}


		[Cambro.Web.DbCombo.ResultsMethodAttribute(true)]
		public static object StatusCodeServerMethod(Cambro.Web.DbCombo.ServerMethodArgs args)
		{
			Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings,HttpContext.Current.Session);
			String strAppID = util.GetAppID();
			String strEnterpriseID = util.GetEnterpriseID();

			String strStatusCode="";

			if(args.ServerState != null && args.ServerState.Authenticate("kdfsjgnskajh[apq2-4uhg465654435879mnbfdm qrs;q4"))
			{
				if(args.ServerState["strStatusCode"] != null && args.ServerState["strStatusCode"].ToString().Length > 0)
				{
					strStatusCode = args.ServerState["strStatusCode"].ToString();					
				}
			}	
			DataSet dataset = DbComboDAL.PickupStatusCodeQuery  (strAppID,strEnterpriseID,args);	
			return dataset;                
		}


		[Cambro.Web.DbCombo.ResultsMethodAttribute(true)]
		public static object ExceptionCodeServerMethod(Cambro.Web.DbCombo.ServerMethodArgs args)
		{
			Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings,HttpContext.Current.Session);
			String strAppID = util.GetAppID();
			String strEnterpriseID = util.GetEnterpriseID();

			String strStatusCode="";

			if(args.ServerState != null && args.ServerState.Authenticate("kdfsjgnskajh[apq2-4uhg465654435879mnbfdm qrs;q4"))
			{
				if(args.ServerState["strStatusCode"] != null && args.ServerState["strStatusCode"].ToString().Length > 0)
				{
					strStatusCode = args.ServerState["strStatusCode"].ToString();					
				}
			}	
			DataSet dataset = DbComboDAL.ExceptioCodeQuery  (strAppID,strEnterpriseID,args,strStatusCode);	
			return dataset;                
		}
		

		#endregion

	}
}