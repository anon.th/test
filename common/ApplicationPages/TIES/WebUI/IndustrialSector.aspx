<%@ Page language="c#" Codebehind="IndustrialSector.aspx.cs" AutoEventWireup="false" Inherits="com.ties.IndustrialSector" %>
<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>IndustrialSector</title>
		<LINK href="css/Styles.css" type="text/css" rel="stylesheet" name="stylesheet">
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<!--#INCLUDE FILE="msFormValidations.inc"-->
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="IndustrialSector" method="post" runat="server">
			<asp:button id="btnQuery" style="Z-INDEX: 101; LEFT: 23px; POSITION: absolute; TOP: 52px" runat="server" Width="127px" Text="Query" CssClass="queryButton" CausesValidation="False"></asp:button><asp:button id="btnExecuteQuery" style="Z-INDEX: 103; LEFT: 150px; POSITION: absolute; TOP: 52px" runat="server" Width="127px" Text="Execute Query" Enabled="False" CssClass="queryButton" CausesValidation="False"></asp:button><asp:button id="btnInsert" style="Z-INDEX: 102; LEFT: 276px; POSITION: absolute; TOP: 52px" runat="server" Width="127px" Text="Insert" CssClass="queryButton" CausesValidation="False"></asp:button>&nbsp;
			<asp:datagrid id="dgISector" AllowPaging="True" AllowCustomPaging="True" OnPageIndexChanged="OnISector_PageChange" OnEditCommand="OnEdit_ISector" OnCancelCommand="OnCancel_ISector" OnDeleteCommand="OnDelete_ISector" OnUpdateCommand="OnUpdate_ISector" style="Z-INDEX: 104; LEFT: 23px; POSITION: absolute; TOP: 126px" runat="server" AutoGenerateColumns="False" HorizontalAlign="Center" OnItemDataBound="OnItemBound_ISector" Width="528px">
				<Columns>
					<asp:EditCommandColumn ButtonType="LinkButton" UpdateText="&lt;IMG SRC='images/butt-update.gif' border=0 title='Update' &gt;" CancelText="&lt;IMG SRC='images/butt-cancel.gif' border=0 title='Cancel' &gt;" EditText="&lt;IMG SRC='images/butt-edit.gif' border=0 title='Edit' &gt;">
						<HeaderStyle Width="4%" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle Wrap="False" CssClass="gridField"></ItemStyle>
					</asp:EditCommandColumn>
					<asp:ButtonColumn Text="&lt;IMG SRC='images/butt-delete.gif' border=0 title='Delete' &gt;" ButtonType="LinkButton" CommandName="Delete">
						<HeaderStyle Width="2%" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle CssClass="gridField"></ItemStyle>
					</asp:ButtonColumn>
					<asp:TemplateColumn HeaderText="Code">
						<HeaderStyle Width="8%" CssClass="gridHeading" Font-Bold="True"></HeaderStyle>
						<ItemStyle Wrap="False" CssClass="gridField"></ItemStyle>
						<ItemTemplate>
							<asp:Label ID="lblCode" CssClass="gridLabel" Visible=True Text='<%#DataBinder.Eval(Container.DataItem,"industrial_sector_code")%>' Runat="server">
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<cc1:mstextbox ID="txtCode" Visible=True CssClass="gridTextBox" Text='<%#DataBinder.Eval(Container.DataItem,"industrial_sector_code")%>' Runat="server" TextMaskType="msUpperAlfaNumericWithUnderscore" MaxLength="12">
							</cc1:mstextbox>
							<asp:RequiredFieldValidator ID="cCostCode" Runat="server" BorderWidth="0" Display="None" ControlToValidate="txtCode" ErrorMessage="Industrial Sector Code "></asp:RequiredFieldValidator>
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Description">
						<HeaderStyle Width="30%" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle Wrap="False" CssClass="gridField"></ItemStyle>
						<ItemTemplate>
							<asp:Label ID="lblDescription" CssClass="gridLabel" Visible=True Text='<%#DataBinder.Eval(Container.DataItem,"industrial_sector_description")%>' Runat="server">
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<asp:TextBox ID="txtDescription" CssClass="gridTextBox" Visible=True Text='<%#DataBinder.Eval(Container.DataItem,"industrial_sector_description")%>' Runat="server" MaxLength="200">
							</asp:TextBox>
						</EditItemTemplate>
					</asp:TemplateColumn>
				</Columns>
				<PagerStyle NextPageText="Next" PrevPageText="Previous" HorizontalAlign="Right" CssClass="normalText"></PagerStyle>
			</asp:datagrid>
			<asp:Label id="lblValISector" CssClass="errorMsgColor" style="Z-INDEX: 105; LEFT: 26px; POSITION: absolute; TOP: 85px" runat="server" Width="364px"></asp:Label>
			<asp:Label id="Label1" style="Z-INDEX: 106; LEFT: 32px; POSITION: absolute; TOP: 14px" runat="server" Width="321px" Height="27px" CssClass="mainTitleSize">Industrial Sector</asp:Label>
			<asp:validationsummary id="PageValidationSummary" style="Z-INDEX: 105; LEFT: 728px; POSITION: absolute; TOP: 152px" runat="server" ShowMessageBox="True" ShowSummary="False" DisplayMode="BulletList" HeaderText="Please enter the missing fields."></asp:validationsummary>
		</form>
	</body>
</HTML>
