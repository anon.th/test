<%@ Page language="c#" Codebehind="ShipmentPackageReplaced.aspx.cs" AutoEventWireup="false" Inherits="com.ties.ShipmentPackageReplaced" %>
<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>ShipmentPackageReplaced</title>
		<LINK rel="stylesheet" type="text/css" href="css/Styles.css" name="stylesheet">
		<meta name="GENERATOR" content="Microsoft Visual Studio 7.0">
		<meta name="CODE_LANGUAGE" content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<!--#INCLUDE FILE="msFormValidations.inc"-->
		<script language="javascript" src="Scripts/settingScrollPosition.js"></script>
	</HEAD>
	<body onclick="GetScrollPosition();" onunload="window.top.displayBanner.fnCloseAll(0);"
		onload="SetScrollPosition();" MS_POSITIONING="GridLayout">
		<form id="ShipmentTrackingQuery" method="post" runat="server">
			<asp:button style="Z-INDEX: 100; POSITION: absolute; TOP: 40px; LEFT: 20px" id="btnQuery" runat="server"
				Text="Query" CssClass="queryButton" Width="64px"></asp:button><asp:label style="Z-INDEX: 105; POSITION: absolute; TOP: 71px; LEFT: 20px" id="lblErrorMessage"
				runat="server" CssClass="errorMsgColor" Width="700px" Height="17px">Place holder for err msg</asp:label>&nbsp;
			<asp:button style="Z-INDEX: 101; POSITION: absolute; TOP: 40px; LEFT: 84px" id="btnExecuteQuery"
				runat="server" Text="Execute Query" CssClass="queryButton" Width="123px"></asp:button><asp:button style="Z-INDEX: 102; POSITION: absolute; TOP: 40px; LEFT: 234px" id="btnCancelQry"
				runat="server" Text="Cancel Query" CssClass="queryButton" Visible="False" Enabled="False"></asp:button>
			<TABLE style="Z-INDEX: 104; POSITION: absolute; WIDTH: 851px; HEIGHT: 500px; TOP: 80px; LEFT: 14px"
				id="tblShipmentTracking" border="0" width="851" runat="server">
				<TR>
					<TD style="WIDTH: 464px">
						<fieldset style="WIDTH: 457px; HEIGHT: 129px"><legend><asp:label id="lblDates" runat="server" CssClass="tableHeadingFieldset" Width="20px" Font-Bold="True">Dates</asp:label></legend>
							<TABLE style="WIDTH: 445px; HEIGHT: 113px" id="tblDates" border="0" cellSpacing="0" cellPadding="0"
								align="left" runat="server">
								<TR>
									<td></td>
									<TD colSpan="3">&nbsp;
										<asp:radiobutton id="rbBookingDate" runat="server" Text="Booking Date" CssClass="tableRadioButton"
											Width="126px" Height="22px" AutoPostBack="True" GroupName="QueryByDate" Checked="True"></asp:radiobutton><asp:radiobutton id="rbActualPickUpDate" runat="server" Text="Actual PickUp Date" CssClass="tableRadioButton"
											Width="140px" Height="22px" AutoPostBack="True" GroupName="QueryByDate"></asp:radiobutton><asp:radiobutton id="rbActualPODDate" runat="server" Text="Actual POD Date" CssClass="tableRadioButton"
											Width="116px" Height="22px" AutoPostBack="True" GroupName="QueryByDate"></asp:radiobutton></TD>
									<td style="WIDTH: 1px"></td>
								</TR>
								<TR>
									<td></td>
									<TD colSpan="3">&nbsp;
										<asp:radiobutton id="rbMonth" runat="server" Text="Month" CssClass="tableRadioButton" Width="73px"
											Height="21px" AutoPostBack="True" GroupName="EnterDate" Checked="True"></asp:radiobutton>&nbsp;
										<asp:dropdownlist id="ddMonth" runat="server" CssClass="textField" Width="88px" Height="19px"></asp:dropdownlist>&nbsp;
										<asp:label id="lblYear" runat="server" CssClass="tableLabel" Width="30px">Year</asp:label>&nbsp;
										<cc1:mstextbox id="txtYear" runat="server" CssClass="textField" Width="83" MaxLength="5" TextMaskType="msNumeric"
											NumberMaxValue="2999" NumberMinValue="1" NumberPrecision="4"></cc1:mstextbox></TD>
									<td style="WIDTH: 1px"></td>
								</TR>
								<TR>
									<td style="HEIGHT: 20px"></td>
									<TD style="HEIGHT: 20px" colSpan="3">&nbsp;
										<asp:radiobutton id="rbPeriod" runat="server" Text="Period" CssClass="tableRadioButton" Width="62px"
											AutoPostBack="True" GroupName="EnterDate"></asp:radiobutton>&nbsp;&nbsp;&nbsp;
										<cc1:mstextbox id="txtPeriod" runat="server" CssClass="textField" Width="82" MaxLength="10" TextMaskType="msDate"
											TextMaskString="99/99/9999"></cc1:mstextbox>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
										&nbsp;
										<cc1:mstextbox id="txtTo" runat="server" CssClass="textField" Width="83" MaxLength="10" TextMaskType="msDate"
											TextMaskString="99/99/9999"></cc1:mstextbox></TD>
									<td style="WIDTH: 1px; HEIGHT: 20px"></td>
								</TR>
								<TR>
									<td></td>
									<TD colSpan="3">&nbsp;
										<asp:radiobutton id="rbDate" runat="server" Text="Date" CssClass="tableRadioButton" Width="74px"
											Height="22px" AutoPostBack="True" GroupName="EnterDate"></asp:radiobutton>&nbsp;&nbsp;<cc1:mstextbox id="txtDate" runat="server" CssClass="textField" Width="82" MaxLength="10" TextMaskType="msDate"
											TextMaskString="99/99/9999"></cc1:mstextbox></TD>
									<td style="WIDTH: 1px"></td>
								</TR>
							</TABLE>
						</fieldset>
					</TD>
					<TD>
						<fieldset style="WIDTH: 357px; HEIGHT: 50px"><legend><asp:label id="lblPayerType" runat="server" CssClass="tableHeadingFieldset" Width="79px" Font-Bold="True">Payer Type</asp:label></legend>
							<TABLE style="WIDTH: 344px; HEIGHT: 73px" id="tblPayerType" border="0" cellSpacing="0"
								cellPadding="0" align="left" runat="server">
								<TR height="33">
									<td bgColor="blue"></td>
									<TD class="tableLabel" colSpan="3">&nbsp;
										<asp:label id="lblPayerCode" runat="server" CssClass="tableLabel" Width="79px">Payer Code</asp:label><cc1:mstextbox id="txtPayerCode" runat="server" CssClass="textField" Width="148px" MaxLength="20"
											TextMaskType="msUpperAlfaNumericWithUnderscore"></cc1:mstextbox></TD>
									<td></td>
								</TR>
							</TABLE>
						</fieldset>
					</TD>
				</TR>
				<TR>
					<TD vAlign="top" colSpan="2">
						<fieldset style="WIDTH: 828px; HEIGHT: 150px"><legend><asp:label id="lblOther" runat="server" CssClass="tableHeadingFieldset" Width="79px" Font-Bold="True">Other</asp:label></legend>
							<TABLE style="WIDTH: 809px; HEIGHT: 121px" id="tblOther" border="0" cellSpacing="0" cellPadding="0"
								width="809" align="left" runat="server">
								<tr>
									<td><FONT face="Tahoma"></FONT></td>
									<TD class="tableLabel" colSpan="3">&nbsp;&nbsp;
										<asp:label id="lblBookingNo" runat="server" CssClass="tableLabel" Width="116px">Booking No</asp:label>&nbsp;<cc1:mstextbox id="txtBookingNo" runat="server" CssClass="textField" Width="134px" AutoPostBack="True"
											MaxLength="10" TextMaskType="msNumeric" NumberMaxValue="2147483647" NumberPrecision="10"></cc1:mstextbox>
									</TD>
								</tr>
								<tr>
									<td><FONT face="Tahoma"></FONT></td>
									<TD class="tableLabel" colSpan="3">&nbsp;&nbsp;
										<asp:label id="lblConsignmentNo" runat="server" CssClass="tableLabel" Width="120px">Consignment No</asp:label><asp:textbox id="txtConsignmentNo" runat="server" CssClass="textField" Width="161px" AutoPostBack="True"
											MaxLength="30"></asp:textbox>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
										<!--#Start:PSA 2009_029 By Gwang on 13Mar09--> 
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
										<!--#End:PSA 2009_029 By Gwang on 13Mar09--></TD>
								</tr>
								<tr>
									<td><FONT face="Tahoma"></FONT></td>
									<TD class="tableLabel" colSpan="3">&nbsp;&nbsp;
										<asp:label style="Z-INDEX: 0" id="lblRefNo" runat="server" CssClass="tableLabel" Width="120px">Customer Ref. #</asp:label><asp:textbox style="Z-INDEX: 0" id="txtRefNo" tabIndex="5" runat="server" CssClass="textField"
											Width="99px" AutoPostBack="True" MaxLength="20"></asp:textbox></TD>
								</tr>
								<tr>
									<td><FONT face="Tahoma"></FONT></td>
									<TD class="tableLabel" colSpan="3">&nbsp;&nbsp;
										<asp:label style="Z-INDEX: 0" id="Label2" runat="server" CssClass="tableLabel" Width="120px">Origin DC</asp:label><asp:textbox id="txtOriginDC" tabIndex="5" runat="server" CssClass="textField" Width="99px" AutoPostBack="True"
											MaxLength="20"></asp:textbox></TD>
								</tr>
								<tr>
									<td></td>
									<TD class="tableLabel" colSpan="3">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										<asp:checkbox id="chkMorethanWeight" runat="server" Text="More than "></asp:checkbox><asp:label id="lblMoreThanWeight" runat="server" CssClass="tableLabel"></asp:label>&nbsp;
										<asp:label style="Z-INDEX: 0" id="Label5" runat="server" CssClass="tableLabel"> kg.</asp:label></TD>
								</tr>
								<tr>
									<td></td>
									<TD class="tableLabel" colSpan="3">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									</TD>
								</tr>
							</TABLE>
						</fieldset>
					</TD>
				</TR>
			</TABLE>
			<asp:label style="Z-INDEX: 103; POSITION: absolute; TOP: 4px; LEFT: 20px" id="Label1" runat="server"
				CssClass="maintitleSize" Width="365px" Height="27px">Shipment Package Replace</asp:label></TR></TABLE></TR></TABLE><input 
value="<%=strScrollPosition%>" type=hidden name=ScrollPosition>
		</form>
	</body>
</HTML>
