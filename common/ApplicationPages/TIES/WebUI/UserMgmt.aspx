<%@ Page language="c#" Codebehind="UserMgmt.aspx.cs" AutoEventWireup="false" Inherits="TIES.WebUI.UserMgmt" %>
<%@ Register TagPrefix="iewc" Namespace="Microsoft.Web.UI.WebControls" Assembly="Microsoft.Web.UI.WebControls, Version=1.0.2.226, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<%@ Import Namespace="com.common.util" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>UserManagement</title>
		<LINK href="css/Styles.css" type="text/css" rel="stylesheet" name="stylesheet">
		<LINK href="css/Styles.css" type="text/css" rel="stylesheet" name="stylesheet">
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<!--#INCLUDE FILE="msFormValidations.inc"-->
	</HEAD>
	<body onunload="window.top.displayBanner.fnCloseAll(0);" MS_POSITIONING="GridLayout">
		<form id="UserMgmt" method="post" runat="server">
			<asp:label id="Label1" style="Z-INDEX: 103; LEFT: 27px; POSITION: absolute; TOP: 8px" runat="server" Height="5px" CssClass="mainTitleSize" Width="405px">User Management</asp:label>
			<DIV id="divMain" style="Z-INDEX: 102; LEFT: -1px; WIDTH: 713px; POSITION: relative; TOP: 32px; HEIGHT: 583px" MS_POSITIONING="GridLayout" runat="server"><asp:button id="btnDelete" style="Z-INDEX: 112; LEFT: 345px; POSITION: absolute; TOP: 8px" tabIndex="6" runat="server" Height="21px" CssClass="queryButton" Width="62px" Text="Delete" CausesValidation="False"></asp:button><asp:button id="btnInsert" style="Z-INDEX: 111; LEFT: 220px; POSITION: absolute; TOP: 8px" tabIndex="3" runat="server" Height="21px" CssClass="queryButton" Width="62px" Text="Insert" CausesValidation="False"></asp:button><asp:button id="btnGoToFirstPage" style="Z-INDEX: 110; LEFT: 585px; POSITION: absolute; TOP: 8px" tabIndex="29" runat="server" CssClass="queryButton" Width="24px" Text="|<" CausesValidation="False"></asp:button><asp:button id="btnPreviousPage" style="Z-INDEX: 106; LEFT: 608px; POSITION: absolute; TOP: 8px" tabIndex="30" runat="server" CssClass="queryButton" Width="24px" Text="<" CausesValidation="False"></asp:button><asp:textbox id="txtCountRec" style="Z-INDEX: 109; LEFT: 632px; POSITION: absolute; TOP: 8px" runat="server" Width="24px" Enabled="False"></asp:textbox><asp:button id="btnNextPage" style="Z-INDEX: 107; LEFT: 656px; POSITION: absolute; TOP: 8px" tabIndex="31" runat="server" CssClass="queryButton" Width="24px" Text=">" CausesValidation="False"></asp:button><asp:button id="btnGoToLastPage" style="Z-INDEX: 108; LEFT: 679px; POSITION: absolute; TOP: 8px" tabIndex="32" runat="server" CssClass="queryButton" Width="24px" Text=">|" CausesValidation="False"></asp:button><asp:button id="btnSave" style="Z-INDEX: 101; LEFT: 279px; POSITION: absolute; TOP: 8px" tabIndex="5" runat="server" Height="22px" CssClass="queryButton" Width="66px" Text="Save" CausesValidation="true"></asp:button><asp:button id="btnQry" style="Z-INDEX: 102; LEFT: 28px; POSITION: absolute; TOP: 8px" tabIndex="1" runat="server" Height="22px" CssClass="queryButton" Width="62px" Text="Query" CausesValidation="False"></asp:button><asp:button id="btnExecQry" style="Z-INDEX: 105; LEFT: 90px; POSITION: absolute; TOP: 8px" tabIndex="2" runat="server" Height="22px" CssClass="queryButton" Width="130px" Text="Execute Query" CausesValidation="False"></asp:button><asp:label id="lblMessages" style="Z-INDEX: 113; LEFT: 27px; POSITION: absolute; TOP: 40px" runat="server" Height="23px" Width="557px" ForeColor="Red"></asp:label>
				<FIELDSET style="Z-INDEX: 104; LEFT: 32px; WIDTH: 415px; POSITION: absolute; TOP: 64px; HEIGHT: 242px"><LEGEND><asp:label id="Label3" CssClass="tableHeadingFieldset" Runat="server">User Information</asp:label></LEGEND>
					<TABLE id="tblRoleInfo" style="WIDTH: 335px; HEIGHT: 140px" height="140" width="335" border="0" runat="server">
						<TR>
							<TD width="10%"><asp:label id="req" ForeColor="#cc0000" Runat="server">*</asp:label></TD>
							<TD class="tableLabel" width="45%"><asp:label id="lblUserID" tabIndex="7" Width="106px" Runat="server">User ID</asp:label></TD>
							<TD width="45%"><cc1:mstextbox id="txtUserID" tabIndex="8" CssClass="textfield" Width="154" Enabled="False" Runat="server" TextMaskType="msUpperAlfaNumericWithUnderscore" MaxLength="40"></cc1:mstextbox></TD>
						</TR>
						<TR>
							<TD width="10%"><asp:label id="reqPswd" ForeColor="#cc0000" Runat="server">*</asp:label></TD>
							<TD class="tableLabel" width="45%"><asp:label id="lblUserPswd" tabIndex="9" Width="108px" Runat="server">Password</asp:label></TD>
							<TD width="45%"><asp:button id="btnResetPswd" tabIndex="10" runat="server" Height="21px" CssClass="queryButton" Width="154" Text="Reset Password"></asp:button></TD>
						</TR>
						<TR>
							<TD width="10%"><asp:label id="reqName" ForeColor="#cc0000" Runat="server">*</asp:label></TD>
							<TD class="tableLabel" width="45%"><asp:label id="lblUsrName" tabIndex="11" Width="109px" Runat="server">User Name</asp:label></TD>
							<TD><cc1:mstextbox id="txtUserName" tabIndex="12" CssClass="textfield" Width="154" Runat="server" TextMaskType="msAlfaNumericWithSpace" MaxLength="100"></cc1:mstextbox></TD>
						</TR>
						<TR>
							<TD width="10%"><asp:label id="reqCul" ForeColor="#cc0000" Runat="server">*</asp:label></TD>
							<TD class="tableLabel" width="45%"><asp:label id="Label2" tabIndex="13" Width="109px" Runat="server">User Culture</asp:label></TD>
							<TD><asp:dropdownlist id="ddbCulture" tabIndex="14" runat="server" Height="69px" Width="154"></asp:dropdownlist></TD>
						</TR>
						<TR>
							<TD width="10%"><asp:regularexpressionvalidator id="regEmailValidate" Runat="server" ErrorMessage="Invalid Email" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" Display="None" ControlToValidate="txtEmail" EnableClientScript="False"></asp:regularexpressionvalidator></TD>
							<TD class="tableLabel" width="45%"><asp:label id="lblEmail" tabIndex="15" Width="116px" Runat="server">Email 1</asp:label></TD>
							<TD><asp:textbox id="txtEmail" tabIndex="16" CssClass="textfield" Width="154" Runat="server" MaxLength="50"></asp:textbox></TD>
						</TR>
						<TR>
							<TD width="10%"></TD>
							<TD class="tableLabel" width="45%"><asp:label id="lblDept" tabIndex="17" Width="109px" Runat="server">Department</asp:label></TD>
							<TD><cc1:mstextbox id="txtDepartment" tabIndex="18" CssClass="textfield" Width="154" Runat="server" TextMaskType="msAlfaNumericWithSpace" MaxLength="100"></cc1:mstextbox></TD>
						</TR>
						<TR>
							<TD style="HEIGHT: 13px" width="10%"></TD>
							<TD class="tableLabel" style="HEIGHT: 17px" width="45%"><asp:label id="lblUserType" tabIndex="19" Width="109px" Runat="server">User Type</asp:label></TD>
							<TD style="HEIGHT: 17px"><asp:dropdownlist id="ddbUserType" tabIndex="20" runat="server" Height="69px" Width="154" AutoPostBack="True"></asp:dropdownlist></TD>
						</TR>
						<TR>
							<TD style="HEIGHT: 25px" width="10%"></TD>
							<TD class="tableLabel" style="HEIGHT: 25px" width="45%"><FONT face="Tahoma"><asp:label id="lblUserLocation" tabIndex="19" runat="server" Width="113px">User Location</asp:label></FONT></TD>
							<TD style="HEIGHT: 21px"><FONT face="Tahoma"><asp:dropdownlist id="ddbUserLocation" tabIndex="20" runat="server" Height="69px" Width="154px" DataTextField="origin_code" DataValueField="origin_code"></asp:dropdownlist></FONT></TD>
						</TR>
						<TR>
							<TD width="10%"></TD>
							<TD class="tableLabel" width="45%"><asp:label id="lblCustomerAcc" tabIndex="19" runat="server" Width="112px">Customer Account</asp:label></TD>
							<TD><FONT face="Tahoma"><asp:dropdownlist id="ddbCustomerAcc" tabIndex="20" runat="server" Height="69px" Width="154px" DataTextField="custid" DataValueField="custid"></asp:dropdownlist></FONT></TD>
						</TR>
					</TABLE>
				</FIELDSET>
				<FIELDSET style="Z-INDEX: 103; LEFT: 454px; WIDTH: 251px; POSITION: absolute; TOP: 312px; HEIGHT: 209px"><LEGEND><asp:label id="lblPerGrn" CssClass="tableHeadingFieldset" Runat="server">Permission Grants</asp:label></LEGEND>
					<TABLE id="tblPermissions" style="WIDTH: 235px; HEIGHT: 101px" cellSpacing="1" cellPadding="1" width="235" border="0" runat="server">
						<TR>
							<TD><asp:checkbox id="chkGrantsIns" tabIndex="26" Width="20%" Runat="server" AutoPostBack="True"></asp:checkbox></TD>
							<TD class="tableLabel"><asp:label id="lblInsRec" Width="80%" Runat="server">Insert Record</asp:label></TD>
						</TR>
						<TR>
							<TD><asp:checkbox id="chkGrantsUpd" tabIndex="27" Width="20%" Runat="server" AutoPostBack="True"></asp:checkbox></TD>
							<TD class="tableLabel"><asp:label id="lblUpdRec" Width="80%" Runat="server">Update Record</asp:label></TD>
						</TR>
						<TR>
							<TD><asp:checkbox id="chkGrantsDel" tabIndex="28" Width="20%" Runat="server" AutoPostBack="True"></asp:checkbox></TD>
							<TD class="tableLabel"><asp:label id="lblDelRec" Width="80%" Runat="server">Delete Record</asp:label></TD>
						</TR>
					</TABLE>
				</FIELDSET>
				<FIELDSET style="Z-INDEX: 114; LEFT: 456px; WIDTH: 251px; POSITION: absolute; TOP: 64px; HEIGHT: 243px"><LEGEND><asp:label id="Label4" CssClass="tableHeadingFieldset" Runat="server">Additional Functional Roles</asp:label></LEGEND>
					<TABLE id="tblAdditionalRoles" cellSpacing="1" cellPadding="1" width="221" border="0" runat="server">
						<TR>
							<TD><iewc:treeview id="funcTree" tabIndex="21" runat="server" Height="217px" Width="220px" AutoPostBack="True"></iewc:treeview></TD>
						</TR>
					</TABLE>
				</FIELDSET>
				&nbsp;
				<fieldset style="Z-INDEX: 116; LEFT: 28px; WIDTH: 418px; POSITION: absolute; TOP: 312px; HEIGHT: 202px">
					<TABLE id="tblist" style="WIDTH: 322px; HEIGHT: 200px" runat="server">
						<TR>
							<TD style="WIDTH: 193px"><asp:listbox id="lbRoleMaster" runat="server" Height="194px" Width="190px" SelectionMode="Multiple"></asp:listbox></TD>
							<TD>
								<asp:button id="btnAdd" tabIndex="23" runat="server" CssClass="queryButton" Width="25" Text=">" CausesValidation="False"></asp:button>
								<asp:button id="btnRemove" tabIndex="22" runat="server" CssClass="queryButton" Width="25" Text="<" CausesValidation="False"></asp:button>
								<asp:button id="btnAddAll" tabIndex="25" runat="server" CssClass="queryButton" Width="25" Text=">>" CausesValidation="False"></asp:button>
								<asp:button id="btnRemoveAll" tabIndex="24" runat="server" CssClass="queryButton" Width="25" Text="<<" CausesValidation="False"></asp:button>
							</TD>
							<TD><asp:listbox id="lbRolesAdded" runat="server" Height="189px" Width="168px" SelectionMode="Multiple"></asp:listbox></TD>
						</TR>
					</TABLE>
				</fieldset>
				<table style="Z-INDEX: 116; LEFT: 25px; WIDTH: 554px; POSITION: absolute; TOP: 536px; HEIGHT: 26px">
					<tr>
						<td>&nbsp;</td>
					</tr>
				</table>
			</DIV>
			<DIV id="UserProfilePanel" style="Z-INDEX: 101; LEFT: 220px; WIDTH: 367px; POSITION: relative; TOP: 46px; HEIGHT: 137px" MS_POSITIONING="GridLayout" runat="server">
				<FIELDSET style="WIDTH: 374px; HEIGHT: 203px"><LEGEND><asp:label id="lblconf" CssClass="tableHeadingFieldset" Runat="server">Confirmation</asp:label></LEGEND>
					<p align="center"><br>
						<asp:label id="lblConfirmMsg" runat="server"></asp:label><br>
						<br>
						<asp:button id="btnToSaveChanges" runat="server" CssClass="queryButton" Text="Yes" CausesValidation="false"></asp:button><asp:button id="btnNotToSave" runat="server" CssClass="queryButton" Text="No" CausesValidation="False"></asp:button><asp:button id="btnToCancel" runat="server" CssClass="queryButton" Text="Cancel" CausesValidation="False"></asp:button></p>
				</FIELDSET>
			</DIV>
		</form>
	</body>
</HTML>
