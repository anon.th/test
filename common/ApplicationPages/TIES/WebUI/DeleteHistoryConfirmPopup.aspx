<%@ Page language="c#" Codebehind="DeleteHistoryConfirmPopup.aspx.cs" AutoEventWireup="false" Inherits="TIES.WebUI.DeleteHistoryConfirmPopup" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>DeleteHistoryConfirmPopup</title>
		<LINK href="css/Styles.css" type="text/css" rel="stylesheet" name="stylesheet">
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="DeleteHistoryConfirmPopup" method="post" runat="server">
			<asp:button id="btnYes" style="Z-INDEX: 101; POSITION: absolute; TOP: 184px; LEFT: 70px" runat="server"
				Width="58" Text="Yes" CssClass="queryButton"></asp:button><asp:button id="btnNo" style="Z-INDEX: 102; POSITION: absolute; TOP: 184px; LEFT: 129px" runat="server"
				Width="58px" Text="No" CssClass="queryButton"></asp:button><asp:textbox id="txtDeleteRemark" style="Z-INDEX: 103; POSITION: absolute; TOP: 96px; LEFT: 16px"
				runat="server" Height="73px" Width="230px" CssClass="TextField" TextMode="MultiLine"></asp:textbox><asp:label id="lblConfirmQ" style="Z-INDEX: 104; POSITION: absolute; TOP: 15px; LEFT: 16px"
				runat="server" Width="230px" CssClass="tableLabel"></asp:label><asp:label id="lblValRemark" style="Z-INDEX: 105; POSITION: absolute; TOP: 43px; LEFT: 16px"
				runat="server" Width="222px" CssClass="errorMsgColor"></asp:label><asp:label id="lblReason" style="Z-INDEX: 106; POSITION: absolute; TOP: 76px; LEFT: 16px" runat="server"
				Width="213px" CssClass="tableLabel"></asp:label></form>
	</body>
</HTML>
