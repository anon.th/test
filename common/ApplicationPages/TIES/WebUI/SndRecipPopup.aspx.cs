using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Text;
using com.common.DAL;
using com.common.classes;
using com.common.util;
using com.ties.classes;
using com.common.applicationpages;

//HC Return Task
using com.ties.DAL;
//HC Return Task

namespace TIES.WebUI
{
	/// <summary>
	/// Summary description for SenderPopup.
	/// </summary>
	public class SndRecipPopup : BasePopupPage
	{
		protected System.Web.UI.WebControls.Button btnSearch;
		protected System.Web.UI.WebControls.DataGrid dgSen;
		private String appID = null;
		private String enterpriseID = null;
		private string strDestZipCode = null;
		private string strCustType = null;
		private string  strCustID = null;
		//Utility utility = null;
		protected System.Web.UI.WebControls.DataGrid dgSndRecpMaster;
		private String strSndRcpType = null;
		SessionDS m_sdsSendRecip = null;
		String strFormID = "";
		String strPayMode=null;
		string strESASurcharge=null;
		string strESAApplied=null;
		String strtxtESA=null;
		String strtxtESASurcharge=null;
		String strZipCode = null;
		String strSendZip = null;
		protected System.Web.UI.WebControls.Label Label1;
		protected System.Web.UI.WebControls.TextBox txtZipCode;
		protected System.Web.UI.WebControls.Label lblZipCode;
		protected System.Web.UI.WebControls.TextBox txtName;
		protected System.Web.UI.WebControls.Button btnClose;
		String strESAValue = "0.00";
		String strESTDELDT = null; //HC Return Task
		String strCon = null; //HC Return Task
		String strBookingNo = null; //HC Return Task

		private void Page_Load(object sender, System.EventArgs e)
		{
			strFormID = Request.Params["FORMID"];
			strCustType = Request.Params["CUSTTYPE"];
			strCustID = Request.Params["CUSTID"]; 
			strDestZipCode = Request.Params["DestZipCode"];  
			strSndRcpType = Request.Params["SENDRCPTYPE"];
			strPayMode = Request.Params["PAYMODE"];
			strCustID = Request.Params["CUSTID"];
			strZipCode = Request.Params["ZIPCODE"];
			strSendZip = Request.Params["SENDZIP"];
			strESTDELDT = Request.Params["ESTDELDT"];		//HC Return Task
			strCon = Request.Params["STRCON"];				//HC Return Task
			strBookingNo = Request.Params["STRBOOKINNO"];	//HC Return Task

			//utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings,Page.Session);
			appID = utility.GetAppID();
			enterpriseID = utility.GetEnterpriseID();

			if(!Page.IsPostBack)
			{
				txtName.Text = Request.Params["SENDERNAME"];
				txtZipCode.Text = Request.Params["ZIPCODE"];				
				m_sdsSendRecip = GetEmptySndRcpDS(0);
				BindGrid();
			}
			else
			{
				m_sdsSendRecip = (SessionDS)ViewState["SNDRCP_DS"];

			}
		}
		public void Paging(Object sender, DataGridPageChangedEventArgs e)
		{
			//runs when one of the page control is clicked
			//update the current page from the parameter values
			
			dgSndRecpMaster.CurrentPageIndex = e.NewPageIndex;
			ShowCurrentPage();
		
		}

		public static SessionDS GetEmptySndRcpDS(int iNumRows)
		{
			DataTable dtSndRcp = new DataTable();
 
			dtSndRcp.Columns.Add(new DataColumn("zipcode", typeof(string)));
			dtSndRcp.Columns.Add(new DataColumn("state_name", typeof(string)));
			dtSndRcp.Columns.Add(new DataColumn("country", typeof(string)));

			for(int i=0; i < iNumRows; i++)
			{
				DataRow drEach = dtSndRcp.NewRow();
				drEach[0] = "";
				drEach[1] = "";
				drEach[2] = "";
				drEach[3] = "";
				drEach[4] = "";

				dtSndRcp.Rows.Add(drEach);
			}

			DataSet dsSndRcp = new DataSet();
			dsSndRcp.Tables.Add(dtSndRcp);

			SessionDS sessionDS = new SessionDS();
			sessionDS.ds = dsSndRcp;
			sessionDS.DataSetRecSize = dsSndRcp.Tables[0].Rows.Count;
			sessionDS.QueryResultMaxSize = sessionDS.DataSetRecSize;

			return  sessionDS;	
		}

		private SessionDS GetSndRcpDS(int iCurrent, int iDSRecSize, String strQuery)
		{

			SessionDS sessionDS = null;


			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(appID,enterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("SndRcipPopup.aspx.cs","GetSndRcpDS","ERR001","DbConnection object is null!!");
				return sessionDS;
			}

			
			sessionDS = dbCon.ExecuteQuery(strQuery,iCurrent,iDSRecSize,"SenderRecipentData");

			return  sessionDS;
			
		}

		private void BindGrid()
		{
			dgSndRecpMaster.VirtualItemCount = System.Convert.ToInt32(m_sdsSendRecip.QueryResultMaxSize); 
			dgSndRecpMaster.DataSource = m_sdsSendRecip.ds;
			dgSndRecpMaster.DataBind();
			ViewState["SNDRCP_DS"] = m_sdsSendRecip;
		}

		private void ShowCurrentPage()
		{
			int iStartIndex = dgSndRecpMaster.CurrentPageIndex * dgSndRecpMaster.PageSize;
			String sqlQuery = (String)ViewState["SQL_QUERY"];
			m_sdsSendRecip = GetSndRcpDS(iStartIndex,dgSndRecpMaster.PageSize,sqlQuery);
			decimal pgCnt = (m_sdsSendRecip.QueryResultMaxSize - 1)/dgSndRecpMaster.PageSize;
			if(pgCnt < dgSndRecpMaster.CurrentPageIndex)
			{
				dgSndRecpMaster.CurrentPageIndex = System.Convert.ToInt32(pgCnt);
			}
			dgSndRecpMaster.SelectedIndex = -1;
			dgSndRecpMaster.EditItemIndex = -1;

			BindGrid();
			getPageControls(Page);
		}			

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			base.OnInit(e);
			InitializeComponent();
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.dgSndRecpMaster.SelectedIndexChanged += new System.EventHandler(this.dgSndRecpMaster_SelectedIndexChanged);
			this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
			this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
		private void btnSearch_Click(object sender, System.EventArgs e)
		{
			String strSndRcpName = txtName.Text.Trim();
			strZipCode			 = txtZipCode.Text.Trim();
			StringBuilder strQry = new StringBuilder();
			strQry.Append("select * from Customer_snd_rec where applicationid='");
			strQry.Append(appID);
			strQry.Append("' and enterpriseid = '");
			strQry.Append(enterpriseID+"'");

			if(strSndRcpName != null && strSndRcpName != "")
			{
				strQry.Append(" and snd_rec_name like N'%");
				strQry.Append(Utility.ReplaceSingleQuote(strSndRcpName));
				strQry.Append("%' ");
			}

			if(strCustID != null && strCustID != "")
			{
				strQry.Append(" and custid like '%");
				strQry.Append(Utility.ReplaceSingleQuote(strCustID));
				strQry.Append("%' ");
			}

			strQry.Append(" and (snd_rec_type = 'B' or snd_rec_type = '");
			strQry.Append(strSndRcpType+"')");

			if(strZipCode != null && strZipCode != "")
			{
				strQry.Append(" and zipcode like '%");
				strQry.Append(Utility.ReplaceSingleQuote(strZipCode));
				strQry.Append("%' ");
			}

			String strSQLQuery = strQry.ToString();

			ViewState["SQL_QUERY"] = strSQLQuery;
			dgSndRecpMaster.CurrentPageIndex = 0;

			ShowCurrentPage();
		}

		
		public void dgSndRecpMaster_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			// Get the value and return it to the parent window
			int iSelIndex = dgSndRecpMaster.SelectedIndex;
			DataGridItem dgRow = dgSndRecpMaster.Items[iSelIndex];
			String strName = dgRow.Cells[0].Text;
			String strAddress1 = dgRow.Cells[1].Text;
			String strAddress2 = dgRow.Cells[2].Text;
			String strCountry = dgRow.Cells[3].Text;
			String strDestZipCode = dgRow.Cells[4].Text;
			String strTelephone = dgRow.Cells[5].Text;
			String strFax = dgRow.Cells[6].Text;
			String strContactPerson = dgRow.Cells[7].Text;

			//Used for calculation of ESA surcharge of Domestic Shipment
			strSendZip = strDestZipCode;
			

			if(strAddress1 == "&nbsp;")
			{
				strAddress1 = "";
			}
			if(strAddress2 == "&nbsp;")
			{
				strAddress2 = "";
			}
			if(strDestZipCode == "&nbsp;")
			{
				strDestZipCode = "";
			}
			if(strCountry == "&nbsp;")
			{
				strCountry = "";
			}
			if(strTelephone == "&nbsp;")
			{
				strTelephone = "";
			}
			if(strFax == "&nbsp;")
			{
				strFax = "";
			}
			if(strContactPerson == "&nbsp;")
			{
				strContactPerson = "";
			}

			//Get the Country
			if(strCountry.Length == 0)
			{
				Enterprise enterprise = new Enterprise();
				enterprise.Populate(appID,enterpriseID);
				strCountry = enterprise.Country;
			}

			//Get the state & display...
			Zipcode zipCode = new Zipcode();
			zipCode.Populate(appID,enterpriseID,strCountry,strDestZipCode);
			String strState = zipCode.StateName;
			String strStateCode = zipCode.StateCode; //HC Return Task

			DateTime dtCuttOffTime = zipCode.CuttOffTime;
	
			
			strESASurcharge = zipCode.EASSurcharge.ToString ();

			String sScript = "";
			sScript += "<script language=javascript>";
						
		
			if (strFormID=="PickupRequestBooking")
			{
				CalculateESA();
				sScript += "  window.opener."+strFormID+".txtSendZip.value = \"" +strDestZipCode+ "\";";
				sScript += "  window.opener."+strFormID+".txtESA.value = \"" +strtxtESA+ "\";";
				sScript += "  window.opener."+strFormID+".txtESASurcharge.value = \"" +strtxtESASurcharge+ "\";";
				if(strSndRcpType.Equals("S"))
				{
					String strCuttOffTime = dtCuttOffTime.ToString("HH:mm");
					sScript += "  window.opener."+strFormID+".txtSendName.value = \"" +strName+ "\";";
					sScript += "  window.opener."+strFormID+".txtSendAddr1.value = \"" +strAddress1+ "\";";
					sScript += "  window.opener."+strFormID+".txtSendAddr2.value = \"" +strAddress2+ "\";";
					sScript += "  window.opener."+strFormID+".txtSendState.value = \"" +strState+ "\";";
					sScript += "  window.opener."+strFormID+".txtSendZip.value = \"" +strDestZipCode+ "\";";
					sScript += "  window.opener."+strFormID+".txtSendCity.value = \"" +strCountry+ "\";";
					sScript += "  window.opener."+strFormID+".txtSendContPer.value = \"" +strContactPerson+ "\";";
					sScript += "  window.opener."+strFormID+".txtSendTel.value = \"" +strTelephone+ "\";";
					sScript += "  window.opener."+strFormID+".txtSendFax.value = \"" +strFax+ "\";";
					sScript += "  window.opener."+strFormID+".txtSendCuttOffTime.value = \"" +strCuttOffTime+ "\";";
				}
			}
			else if (strFormID=="CustomerScheduledPickups")
			{//Phase2 - L02
				sScript += "  window.opener."+strFormID+".txtSendName.value = \"" +strName+ "\";";
				if(strAddress1.Trim() != "")
					sScript += "  window.opener."+strFormID+".txtSendAddr1.value = \"" +strAddress1+ "\";";
			}//Phase2 - L02
			else if  (strFormID=="ImportConsignments")
			{
				if(strSndRcpType.Equals("S"))
				{
					String SENDNAMECLN = Request.Params["SENDNAMECLN"].ToString();
					String SENDADD1CLN = Request.Params["SENDADD1CLN"].ToString();
					String SENDADD2CLN = Request.Params["SENDADD2CLN"].ToString();
					String SENDZIPCLN = Request.Params["SENDZIPCLN"].ToString();
					String SENDTELECLN = Request.Params["SENDTELECLN"].ToString();
					String SENDFAXCLN = Request.Params["SENDFAXCLN"].ToString();
					String SENDCONTACTCLN = Request.Params["SENDCONTACTCLN"].ToString();

					sScript += "  window.opener."+strFormID+"." + SENDNAMECLN + ".value = \"" +strName+ "\";";
					sScript += "  window.opener."+strFormID+"." + SENDADD1CLN + ".value = \"" +strAddress1+ "\";";
					sScript += "  window.opener."+strFormID+"." + SENDADD2CLN + ".value = \"" +strAddress2+ "\";";
					sScript += "  window.opener."+strFormID+"." + SENDZIPCLN + ".value = \"" +strDestZipCode+ "\";";
					sScript += "  window.opener."+strFormID+"." + SENDTELECLN + ".value = \"" +strTelephone+ "\";";
					sScript += "  window.opener."+strFormID+"." + SENDFAXCLN + ".value = \"" +strFax+ "\";";
					sScript += "  window.opener."+strFormID+"." + SENDCONTACTCLN + ".value = \"" +strContactPerson+ "\";";
				}
				else if(strSndRcpType.Equals("R"))
				{
					String RECNAMECLN = Request.Params["RECNAMECLN"].ToString();
					String RECADD1CLN = Request.Params["RECADD1CLN"].ToString();
					String RECADD2CLN = Request.Params["RECADD2CLN"].ToString();
					String RECZIPCLN = Request.Params["RECZIPCLN"].ToString();
					String RECTELECLN = Request.Params["RECTELECLN"].ToString();
					String RECFAXCLN = Request.Params["RECFAXCLN"].ToString();
					String RECCONTACTCLN = Request.Params["RECCONTACTCLN"].ToString();

					sScript += "  window.opener."+strFormID+"." + RECNAMECLN + ".value = \"" +strName+ "\";";
					sScript += "  window.opener."+strFormID+"." + RECADD1CLN + ".value = \"" +strAddress1+ "\";";
					sScript += "  window.opener."+strFormID+"." + RECADD2CLN + ".value = \"" +strAddress2+ "\";";
					sScript += "  window.opener."+strFormID+"." + RECZIPCLN + ".value = \"" +strDestZipCode+ "\";";
					sScript += "  window.opener."+strFormID+"." + RECTELECLN + ".value = \"" +strTelephone+ "\";";
					sScript += "  window.opener."+strFormID+"." + RECFAXCLN + ".value = \"" +strFax+ "\";";
					sScript += "  window.opener."+strFormID+"." + RECCONTACTCLN + ".value = \"" +strContactPerson+ "\";";
				}
			}
			else
			{
				if(strSndRcpType.Equals("S"))
				{
					String strCuttOffTime = dtCuttOffTime.ToString("HH:mm");
					sScript += "  window.opener."+strFormID+".txtSendName.value = \"" +strName+ "\";";
					sScript += "  window.opener."+strFormID+".txtSendAddr1.value = \"" +strAddress1+ "\";";
					sScript += "  window.opener."+strFormID+".txtSendAddr2.value = \"" +strAddress2+ "\";";
					//sScript += "  window.opener."+strFormID+".txtSendState.value = \"" +strCountry+ "\";";
					sScript += "  window.opener."+strFormID+".txtSendZip.value = \"" +strDestZipCode+ "\";";
					sScript += "  window.opener."+strFormID+".txtSendCity.value = \"" +strState+ "\";";
					sScript += "  window.opener."+strFormID+".txtSendContPer.value = \"" +strContactPerson+ "\";";
					sScript += "  window.opener."+strFormID+".txtSendTel.value = \"" +strTelephone+ "\";";
					sScript += "  window.opener."+strFormID+".txtSendFax.value = \"" +strFax+ "\";";
					//sScript += "  window.opener."+strFormID+".txtSendCuttOffTime.value = \"'" +strCuttOffTime+ "'\";";
				}
				else if(strSndRcpType.Equals("R"))
				{
					sScript += "  window.opener."+strFormID+".txtRecName.value = \"" +strName+ "\";";
					sScript += "  window.opener."+strFormID+".txtRecipAddr1.value = \"" +strAddress1+ "\";";
					sScript += "  window.opener."+strFormID+".txtRecipAddr2.value = \"" +strAddress2+ "\";";
					sScript += "  window.opener."+strFormID+".txtRecipState.value = \"" +strCountry+ "\";";
					sScript += "  window.opener."+strFormID+".txtRecipZip.value = \"" +strDestZipCode+ "\";";
					sScript += "  window.opener."+strFormID+".txtRecipCity.value = \"" +strState+ "\";";
					sScript += "  window.opener."+strFormID+".txtRecpContPer.value = \"" +strContactPerson+ "\";";
					sScript += "  window.opener."+strFormID+".txtRecipTel.value = \"" +strTelephone+ "\";";
					sScript += "  window.opener."+strFormID+".txtRecipFax.value = \"" +strFax+ "\";";
				}
			}

			if(strFormID == "DomesticShipment")
			{
				CalDomesticShipmentESA();
				sScript += "  window.opener."+strFormID+".txtESASurchrg.value = \"" +strESAValue+ "\";";
				
				#region Edit by Sompote 2010-05-25
				/*
				//HC Return Task
				if ((strESTDELDT.ToString().Trim() != "") && (strStateCode.Trim() != ""))
				{
					object tmpReHCDateTime = DomesticShipmentMgrDAL.calEstHCDateTime(System.DateTime.ParseExact(strESTDELDT,"dd/MM/yyyy HH:mm",null), strStateCode,
						strCon, strBookingNo, appID, enterpriseID);

					if((tmpReHCDateTime != null) && 
						(!tmpReHCDateTime.GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						sScript += "  window.opener."+strFormID+".txtEstHCDt.value = \"" +((DateTime)tmpReHCDateTime).ToString("dd/MM/yyyy HH:mm")+ "\";";
					}
				}
				//HC Return Task
				*/
				if(strESTDELDT != null && strStateCode != null)				
				{
					//HC Return Task
					if ((strESTDELDT.ToString().Trim() != "") && (strStateCode.Trim() != ""))
					{
						object tmpReHCDateTime = DomesticShipmentMgrDAL.calEstHCDateTime(System.DateTime.ParseExact(strESTDELDT,"dd/MM/yyyy HH:mm",null), strStateCode,
							strCon, strBookingNo, appID, enterpriseID);

						if((tmpReHCDateTime != null) && 
							(!tmpReHCDateTime.GetType().Equals(System.Type.GetType("System.DBNull"))))
						{
							sScript += "  window.opener."+strFormID+".txtEstHCDt.value = \"" +((DateTime)tmpReHCDateTime).ToString("dd/MM/yyyy HH:mm")+ "\";";
						}
					}
					//HC Return Task
				}
				#endregion
			}
			if (strFormID=="ShipmentDetails")
			{
				sScript = "";
				sScript += "<script language=javascript>";
				CalculateESA();
				sScript += "  window.opener."+strFormID+".txtRecipZip.value = \"" + strDestZipCode + "\";";
				sScript += "  window.opener."+strFormID+".txtESA.value = \"" + strtxtESA + "\";";
				sScript += "  window.opener."+strFormID+".txtESASurcharge.value = \"" + strtxtESASurcharge + "\";";
				
			}
		
			sScript += "  window.opener."+strFormID+".submit();";
			sScript += "  window.close();";
			sScript += "</script>";
			Response.Write(sScript);
		
		}
		private void CalculateESA()
		{
			if (Request.Params["CUSTTYPE"]=="C")
			{
			
				Customer customer = new Customer() ;
				customer.Populate(appID ,enterpriseID,Request.Params["CUSTID"]);    
				strESAApplied = customer.ESASurcharge;
			}
			else if (Request.Params["CUSTTYPE"]== "A")
			{
				Agent agent = new Agent();
				agent.Populate(appID ,enterpriseID,Request.Params["CUSTID"]); 
				strESAApplied = agent.ESASurcharge; 
			}
			
 			if ((Request.Params["PAYMODE"]=="C") || (Request.Params["PAYMODE"]=="R"))
			{
				if(strESASurcharge!="0" && strESAApplied=="Y" )
				{
					strtxtESA="YES"; 
					strtxtESASurcharge = strESASurcharge; 
				}
				else 
				{
					strtxtESA="NO"; 
					strtxtESASurcharge = ""; 
				}
			}
			if((Request.Params["NEWCUST"]=="Y")&& (Request.Params["PAYMODE"]=="C"))
			{
				if(strESASurcharge!="0")
				{
					strtxtESA="YES"; 
					strtxtESASurcharge = strESASurcharge; 
				}
				else
				{
					strtxtESA="NO"; 
					strtxtESASurcharge = "";
				}
			}
		}
		private void CalDomesticShipmentESA()
		{
			if((strCustID != null))
			{
				String strESAApply = "Y";

				Customer customer = new Customer();
				customer.Populate(appID,enterpriseID,strCustID);
				strESAApply = customer.ESASurcharge;

				if(strESAApply == "Y")
				{
					decimal decSendSrchrg = 0;
					decimal decRecipSrchrg = 0;
					Zipcode zipCode = new Zipcode();
					if((strDestZipCode != null) && (strDestZipCode != ""))
					{
						zipCode.Populate(appID,enterpriseID,strDestZipCode);
						decSendSrchrg = zipCode.EASSurcharge;
					}
					if((strSendZip != null) && (strSendZip != ""))
					{
						zipCode.Populate(appID,enterpriseID,strSendZip);
						decRecipSrchrg = zipCode.EASSurcharge;
					}
					strESAValue = (decSendSrchrg+decRecipSrchrg).ToString("#0.00");
				}
			}
		}

		private void btnClose_Click(object sender, System.EventArgs e)
		{
			String sScript = "";
			sScript += "<script language=javascript>";
			sScript += "  window.close();";
			sScript += "</script>";
			Response.Write(sScript);
		}
	}
}
