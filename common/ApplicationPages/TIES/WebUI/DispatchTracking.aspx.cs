using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.common.classes;
using com.common.DAL;
using com.common.RBAC;
using com.ties.DAL;
using com.common.util;
using com.ties.classes;
using com.common.applicationpages;
namespace TIES.WebUI
{
	/// <summary>
	/// Summary description for DispatchTracking.
	/// </summary>
	public class DispatchTracking : BasePage
	{
		protected System.Web.UI.WebControls.Label lblDispatchType;
		protected System.Web.UI.WebControls.Label lblBookNo;
		protected System.Web.UI.WebControls.Label lblSenderName;
		protected System.Web.UI.WebControls.CheckBox chkboxOutStanding;
		protected System.Web.UI.WebControls.Label lblStatusCode;
		protected System.Web.UI.WebControls.Label lblExceptionCode;
		protected System.Web.UI.WebControls.Label lblBookToDate;
		protected System.Web.UI.WebControls.Label lblBookFromDate;
		protected System.Web.UI.WebControls.Label lblPickToDate;
		protected System.Web.UI.WebControls.Label lblPickFromDate;
		protected System.Web.UI.WebControls.Label lblPickDate;
		protected System.Web.UI.WebControls.Label lblBookDate;
		protected com.common.util.msTextBox txtPickFromDate;
		protected com.common.util.msTextBox txtPickToDate;
		protected System.Web.UI.WebControls.DropDownList ddDispatchType;
		protected System.Web.UI.WebControls.TextBox txtSender;
		protected com.common.util.msTextBox txtBookFromDate;
		protected com.common.util.msTextBox txtBookToDate;
		protected System.Web.UI.WebControls.Label lblStatus;
		protected System.Web.UI.WebControls.Button btnOutstanding;
		protected System.Web.UI.WebControls.Button btnExecuteQuery;
		protected System.Web.UI.WebControls.Button btnQuery;
		protected System.Web.UI.WebControls.TextBox txtStatus;
		protected System.Web.UI.WebControls.TextBox txtException;
		protected System.Web.UI.WebControls.Button btnStatusSrch;
		protected System.Web.UI.WebControls.Button btnExceptionSrch;
		private String m_strAppID;
		private String m_strEnterpriseID;
		private String m_strCulture;
		protected System.Web.UI.WebControls.Label Label1;
		protected System.Web.UI.HtmlControls.HtmlTable Table1;
		protected System.Web.UI.HtmlControls.HtmlTable Table3;
		protected System.Web.UI.HtmlControls.HtmlTable Table2;
		protected System.Web.UI.HtmlControls.HtmlTable Table4;
		protected com.common.util.msTextBox txtBookNo;
		protected System.Web.UI.WebControls.TextBox txtRouteCode;
		protected System.Web.UI.WebControls.Button btnRouteCode;
		protected System.Web.UI.WebControls.Label lblPickupRoute;
		protected System.Web.UI.WebControls.Label lblODC;
		protected System.Web.UI.WebControls.Label lblRouteType;
		protected System.Web.UI.HtmlControls.HtmlTable tblRouteType;
		protected Cambro.Web.DbCombo.DbCombo DbComboOriginDC;
		protected System.Web.UI.WebControls.Label Label2;
		protected System.Web.UI.WebControls.Button btnPayerID;
		protected System.Web.UI.WebControls.TextBox txtPayerID;
		private DataView m_dvDispatchOptions;
		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here

			//	Utility utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings,Page.Session);
			m_strAppID = utility.GetAppID();
			m_strEnterpriseID = utility.GetEnterpriseID();
			m_strCulture = utility.GetUserCulture();
			//Added by Tom 9/6/09
			DbComboOriginDC.RegistrationKey = System.Configuration.ConfigurationSettings.AppSettings["DbComboRegKey"];
			//End added by Tom 9/6/09
			if(!Page.IsPostBack)
			{
				Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings,HttpContext.Current.Session);
				if(util.CheckUserRoleLocation())
				{
					DbComboOriginDC.Text="";
					DbComboOriginDC.Value="";
					DbComboOriginDC.Enabled=true;
				}
				else
				{
					string UserLocation = util.UserLocation();	
					DbComboOriginDC.Text=UserLocation;
					DbComboOriginDC.Value=UserLocation;
					DbComboOriginDC.Enabled=false;
				}

				m_dvDispatchOptions = CreateDispatchOptions(true);
				BindDispatchOptions(m_dvDispatchOptions);
				Session["toRefresh"]=false;
			}
			else
			{
				
			}
			
		}
		/// <summary>
		/// To bind options to drop downlist
		/// </summary>
		/// <param name="dvDispatchOptions">Dataview of options</param>
		private void BindDispatchOptions(DataView dvDispatchOptions)
		{
			ddDispatchType.DataSource = dvDispatchOptions;
			ddDispatchType.DataTextField = "Text";
			ddDispatchType.DataValueField = "StringValue";
			ddDispatchType.DataBind();	
		}
		/// <summary>
		/// To retreive optoins from database dynamically 
		/// </summary>
		/// <param name="showNilOption">to show with empty options or not</param>
		/// <returns></returns>
		private DataView CreateDispatchOptions(bool showNilOption)
		{
			DataTable dtDispatchOptions = new DataTable();
			dtDispatchOptions.Columns.Add(new DataColumn("Text", typeof(string)));
			dtDispatchOptions.Columns.Add(new DataColumn("StringValue", typeof(string)));
			ArrayList listCodeTxt = Utility.GetCodeValues(m_strAppID,m_strCulture, "booking_type", CodeValueType.StringValue);
			if(showNilOption)
			{
				DataRow drNilRow = dtDispatchOptions.NewRow();
				drNilRow[0] = "";
				drNilRow[1] = "";
				dtDispatchOptions.Rows.Add(drNilRow);
			}
			foreach(SystemCode typeSysCode in listCodeTxt)
			{
				DataRow drEach = dtDispatchOptions.NewRow();
				drEach[0] = typeSysCode.Text;
				drEach[1] = typeSysCode.StringValue;
				dtDispatchOptions.Rows.Add(drEach);
			}
			DataView dvDispatchOptions = new DataView(dtDispatchOptions);
			return dvDispatchOptions;
		}
		/// <summary>
		/// Creates and empty datatable
		/// </summary>
		/// <returns></returns>
		private DataTable CreateEmptyDataTable()
		{
			DataTable dtDispatch = new DataTable();
			dtDispatch.Columns.Add(new DataColumn("Dispatch0", typeof(string)));
			dtDispatch.Columns.Add(new DataColumn("Dispatch1", typeof(string)));
			dtDispatch.Columns.Add(new DataColumn("Dispatch2", typeof(string)));
			dtDispatch.Columns.Add(new DataColumn("Dispatch3", typeof(string)));
			dtDispatch.Columns.Add(new DataColumn("Dispatch4", typeof(DateTime)));
			dtDispatch.Columns.Add(new DataColumn("Dispatch5", typeof(DateTime)));
			dtDispatch.Columns.Add(new DataColumn("Dispatch6", typeof(DateTime)));
			dtDispatch.Columns.Add(new DataColumn("Dispatch7", typeof(DateTime)));
			dtDispatch.Columns.Add(new DataColumn("Dispatch8", typeof(string)));
			//14Jan09;GwanG
			dtDispatch.Columns.Add(new DataColumn("Pickup_Route", typeof(string)));
			//Added by Tom 9/6/09 
			dtDispatch.Columns.Add(new DataColumn("Origin_Distribution_Center", typeof(string)));
			//End added by Tom 9/6/09
			dtDispatch.Columns.Add(new DataColumn("PayerID", typeof(string)));

			return dtDispatch;
		}
		/// <summary>
		/// Get's text field and store to dataset for querying
		/// </summary>
		/// <returns></returns>
		private DataSet GetDispatchQuery()
		{
			DataSet dsDispatch = new DataSet();
			DataTable dtDispatch = CreateEmptyDataTable();
			DataRow dr = dtDispatch.NewRow();
			//chkboxOutStanding
			dr[0] = txtBookNo.Text;
			dr[1] = txtSender.Text;
			if(!chkboxOutStanding.Checked)
			{
				dr[2] = txtStatus.Text;
				dr[3] = txtException.Text;
			}
			else
			{
				dr[2] = "";
				dr[3] = "";
			}
			if(txtBookFromDate.Text!="")
			{
				dr[4] = DateTime.ParseExact(txtBookFromDate.Text,"dd/MM/yyyy",null);
			}
			if(txtBookToDate.Text!="")
			{
				dr[5] = DateTime.ParseExact(txtBookToDate.Text+" 23:59","dd/MM/yyyy HH:mm",null);
			}
			if(txtPickFromDate.Text!="")
			{
				dr[6] = DateTime.ParseExact(txtPickFromDate.Text,"dd/MM/yyyy",null);
			}
			if(txtPickToDate.Text!="")
			{
				dr[7] = DateTime.ParseExact(txtPickToDate.Text+" 23:59","dd/MM/yyyy HH:mm",null);
			}
			dr[8] = ddDispatchType.SelectedItem.Value;

			//14Jan09;GwanG
			if(txtRouteCode.Text!="")
			{
				dr["Pickup_Route"] = txtRouteCode.Text ;
			}
			//Added by Tom 9/6/09
			dr["Origin_Distribution_Center"] = DbComboOriginDC.Value ;
			//End added by Tom 9/6/09
			if(txtPayerID.Text != "")
			{
				dr["PayerID"] = txtPayerID.Text;
			}

			dtDispatch.Rows.Add(dr);
			dsDispatch.Tables.Add(dtDispatch);
			return dsDispatch;
			
		}
		/// <summary>
		/// Open a popup window
		/// </summary>
		/// <param name="strUrl"></param>
		private void OpenWindowpage(String strUrl)
		{

			ArrayList paramList = new ArrayList();
			paramList.Add(strUrl);
			String sScript = Utility.GetScript("openParentWindow.js",paramList);
			Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
		}
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			base.OnInit(e);
			InitializeComponent();
			
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnOutstanding.Click += new System.EventHandler(this.btnOutstanding_Click);
			this.btnExecuteQuery.Click += new System.EventHandler(this.btnExecuteQuery_Click);
			this.btnQuery.Click += new System.EventHandler(this.btnQuery_Click);
			this.btnPayerID.Click += new System.EventHandler(this.btnPayerID_Click);
			this.chkboxOutStanding.CheckedChanged += new System.EventHandler(this.chkboxOutStanding_CheckedChanged);
			this.btnStatusSrch.Click += new System.EventHandler(this.btnStatusSrch_Click);
			this.btnExceptionSrch.Click += new System.EventHandler(this.btnExceptionSrch_Click);
			this.btnRouteCode.Click += new System.EventHandler(this.btnRouteCode_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		/// <summary>
		/// button Query event handler to reset all fields back to empty string
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void btnQuery_Click(object sender, System.EventArgs e)
		{
			btnExecuteQuery.Enabled = true;
			clearFields();
			
		}
		private void clearFields()
		{
			txtBookNo.Text = "";
			txtSender.Text = "";
			txtStatus.Text = "";
			txtException.Text = "";
			txtBookFromDate.Text = "";
			txtBookToDate.Text = "";
			txtPickFromDate.Text = "";
			txtPickToDate.Text = "";
			txtRouteCode.Text = "";
			txtPayerID.Text = "";
			chkboxOutStanding.Checked = false;
			txtStatus.Enabled = true;
			txtException.Enabled = true;
			btnStatusSrch.Enabled = true;
			btnExceptionSrch.Enabled = true;
			m_dvDispatchOptions = CreateDispatchOptions(true);
			Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings,HttpContext.Current.Session);
			if(util.CheckUserRoleLocation())
			{
				DbComboOriginDC.Text="";
				DbComboOriginDC.Value="";
				DbComboOriginDC.Enabled=true;
			}
			else
			{
				string UserLocation = util.UserLocation();	
				DbComboOriginDC.Text=UserLocation;
				DbComboOriginDC.Value=UserLocation;
				DbComboOriginDC.Enabled=false;
			}
			BindDispatchOptions(m_dvDispatchOptions);	
		}
		/// <summary>
		/// To execute the query
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void btnExecuteQuery_Click(object sender, System.EventArgs e)
		{
			//btnExecuteQuery.Enabled = false;
			DataSet dsDispatch = GetDispatchQuery();
			String strUrl = null;
			SessionDS sessionds = new SessionDS();
			if(!chkboxOutStanding.Checked)
			{
				sessionds = DispatchTrackingMgrDAL.QueryDispatch(null, dsDispatch,m_strAppID,m_strEnterpriseID, 0, 25,m_strCulture);
				strUrl = "DispatchTrackingQueryPage.aspx?QUERY_TYPE=FALSE_CHECK";
				Session["QUERY_TYPE"] = "FALSE_CHECK";
			}
			else if(chkboxOutStanding.Checked)
			{
				String[] strOutStandingStatusArray = TIESUtility.getOutstandingDispatchStatus(m_strAppID,m_strEnterpriseID);
				sessionds = DispatchTrackingMgrDAL.QueryDispatch(strOutStandingStatusArray, dsDispatch,m_strAppID,m_strEnterpriseID, 0, 25,m_strCulture);
				strUrl = "DispatchTrackingQueryPage.aspx?QUERY_TYPE=TRUE_CHECK";
				Session["QUERY_TYPE"] = "TRUE_CHECK";
			}
			Session["SESSION_DS1"] = sessionds;
			Session["QUERY_DS"] = dsDispatch;
			clearFields();
			OpenWindowpage(strUrl);
		}

		/// <summary>
		/// To enable and disable certain text field and buttons
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void chkboxOutStanding_CheckedChanged(object sender, System.EventArgs e)
		{
			if(chkboxOutStanding.Checked)
			{
				txtStatus.Enabled = false;
				txtException.Enabled = false;
				btnStatusSrch.Enabled = false;
				btnExceptionSrch.Enabled = false;
			}
			if(!chkboxOutStanding.Checked)
			{
				txtStatus.Enabled = true;
				txtException.Enabled = true;
				btnStatusSrch.Enabled = true;
				btnExceptionSrch.Enabled = true;
			}
			
		}

		/// <summary>
		/// button status event handler, to popup status code window
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void btnStatusSrch_Click(object sender, System.EventArgs e)
		{
			OpenWindowpage("StatusCodePopup.aspx?FORMID=DispatchTracking&STATUSCODE="+txtStatus.Text+"&EXCEPTCODE="+txtException.Text+"&STATUSTYPE=P  ");
		}

		/// <summary>
		/// button exception event handler to popup up exception window
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void btnExceptionSrch_Click(object sender, System.EventArgs e)
		{
			String sExceptionCID=txtException.ClientID;
			String sStatusCID=txtStatus.ClientID;
			OpenWindowpage("ExceptionCodePopup.aspx?FORMID=DispatchTracking&EXCEPTIONCODE="+txtException.Text+"&STATUSCODE="+txtStatus.Text+"&EXCEPTIONCODE_CID="+sExceptionCID+"&STATUSCODE_CID="+sStatusCID);
		}

		/// <summary>
		/// button outstanding button to get all oustanding dispatch
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void btnOutstanding_Click(object sender, System.EventArgs e)
		{
			string strPayerID = txtPayerID.Text;
			string[] strStatusArray = TIESUtility.getOutstandingDispatchStatus(m_strAppID, m_strEnterpriseID);
			SessionDS sessionds = DispatchTrackingMgrDAL.GetOutStandingDispatch(strStatusArray, m_strAppID, m_strEnterpriseID, txtPickFromDate.Text, txtPickToDate.Text, 0, 25,m_strCulture,strPayerID);
			Session["SESSION_DS1"] = sessionds;
			//Fixed by ...(26-Feb-2009)
			OpenWindowpage("DispatchTrackingQueryPage.aspx?QUERY_TYPE=OutStandingStatus&PickFrom=" + txtPickFromDate.Text.Trim() + "&PickTo=" + txtPickToDate.Text.Trim());
			//Fixed by ...(26-Feb-2009)
			Session["QUERY_TYPE"] =  "OutStandingStatus";
		}

		private void btnRouteCode_Click(object sender, System.EventArgs e)
		{
			TextBox txtRouteCode = (TextBox)Page.FindControl("txtRouteCode");
			String strPickupRoute = null;
			String strtxtPickupRoute = null;
			if(txtRouteCode != null)
			{
				strtxtPickupRoute = txtRouteCode.ClientID;
				strPickupRoute = txtRouteCode.Text;
			}
				
			String sUrl = "RouteCodePopup.aspx?FORMID=DispatchTracking&ROUTECODE_TEXT="+strtxtPickupRoute+"&ROUTECODE="+strPickupRoute;
			ArrayList paramList = new ArrayList();
			paramList.Add(sUrl);
			String sScript = Utility.GetScript("openWindowParam.js",paramList);
			Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
		}

		//Added by Tom 9/6/09

		[Cambro.Web.DbCombo.ResultsMethodAttribute(true)]
		public static object DbComboDistributionCenterSelect(Cambro.Web.DbCombo.ServerMethodArgs args)
		{
			Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings,HttpContext.Current.Session);
			String strAppID = util.GetAppID();
			String strEnterpriseID = util.GetEnterpriseID();
			
			DataSet dataset = com.ties.classes.DbComboDAL.DistributionCenterQuery(strAppID,strEnterpriseID,args);	
			return dataset;
		}

		private void btnPayerID_Click(object sender, System.EventArgs e)
		{
			String sUrl = "CustomerPopup.aspx?FORMID="+"DispatchTracking"+"&CUSTID_TEXT="+txtPayerID.Text.Trim().ToString();
			ArrayList paramList = new ArrayList();
			paramList.Add(sUrl);
			String sScript = Utility.GetScript("openParentWindow.js",paramList);
			Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
		}
		//End added by Tom 9/6/09
	}
}
