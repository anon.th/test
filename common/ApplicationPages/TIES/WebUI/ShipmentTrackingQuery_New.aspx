<%@ Page language="c#" Codebehind="ShipmentTrackingQuery_New.aspx.cs" AutoEventWireup="false" Inherits="com.ties.ShipmentTrackingQuery_New" debug="True"%>
<%@ Register TagPrefix="DbCombo" Namespace="Cambro.Web.DbCombo" Assembly="Cambro.Web.DbCombo" %>
<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>ShipmentTrackingQuery</title>
		<LINK rel="stylesheet" type="text/css" href="css/Styles.css" name="stylesheet">
		<meta name="GENERATOR" content="Microsoft Visual Studio 7.0">
		<meta name="CODE_LANGUAGE" content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<!--#INCLUDE FILE="msFormValidations.inc"-->
		<script language="javascript" src="Scripts/settingScrollPosition.js"></script>
	</HEAD>
	<body onclick="GetScrollPosition();" onunload="window.top.displayBanner.fnCloseAll(0);"
		onload="SetScrollPosition();" MS_POSITIONING="GridLayout">
		<form id="ShipmentTrackingQuery_New" method="post" runat="server">
			<asp:button style="Z-INDEX: 100; POSITION: absolute; TOP: 40px; LEFT: 20px" id="btnQuery" runat="server"
				Width="64px" CssClass="queryButton" Text="Query"></asp:button><asp:label style="Z-INDEX: 105; POSITION: absolute; TOP: 71px; LEFT: 20px" id="lblErrorMessage"
				runat="server" Width="700px" CssClass="errorMsgColor" Visible="False" Height="17px">Place holder for err msg</asp:label>&nbsp;
			<asp:button style="Z-INDEX: 101; POSITION: absolute; TOP: 40px; LEFT: 84px" id="btnExecuteQuery"
				runat="server" Width="123px" CssClass="queryButton" Text="Execute Query"></asp:button><asp:button style="Z-INDEX: 102; POSITION: absolute; TOP: 40px; LEFT: 234px" id="btnCancelQry"
				runat="server" CssClass="queryButton" Text="Cancel Query" Visible="False" Enabled="False"></asp:button>
			<TABLE style="Z-INDEX: 104; POSITION: absolute; WIDTH: 851px; HEIGHT: 500px; TOP: 80px; LEFT: 14px"
				id="tblShipmentTracking" border="0" width="851" runat="server">
				<TR>
					<TD style="WIDTH: 464px">
						<fieldset style="WIDTH: 457px; HEIGHT: 129px"><legend><asp:label id="lblDates" runat="server" Width="20px" CssClass="tableHeadingFieldset" Font-Bold="True">Dates</asp:label></legend>
							<TABLE style="WIDTH: 445px; HEIGHT: 113px" id="tblDates" border="0" cellSpacing="0" cellPadding="0"
								align="left" runat="server">
								<TR>
									<td></td>
									<TD colSpan="3">&nbsp;
										<asp:radiobutton id="rbBookingDate" runat="server" Width="126px" CssClass="tableRadioButton" Text="Booking Date"
											Height="22px" Checked="True" GroupName="QueryByDate" AutoPostBack="True"></asp:radiobutton><asp:radiobutton id="rbPickupDate" runat="server" Width="119px" CssClass="tableRadioButton" Text="Pickup Date"
											Height="22px" GroupName="QueryByDate" AutoPostBack="True"></asp:radiobutton><asp:radiobutton id="rbStatusDate" runat="server" Width="116px" CssClass="tableRadioButton" Text="Status Date"
											Height="22px" GroupName="QueryByDate" AutoPostBack="True"></asp:radiobutton></TD>
									<td style="WIDTH: 1px"></td>
								</TR>
								<TR>
									<td></td>
									<TD colSpan="3">&nbsp;
										<asp:radiobutton id="rbMonth" runat="server" Width="73px" CssClass="tableRadioButton" Text="Month"
											Height="21px" Checked="True" GroupName="EnterDate" AutoPostBack="True"></asp:radiobutton>&nbsp;
										<asp:dropdownlist id="ddMonth" runat="server" Width="88px" CssClass="textField" Height="19px"></asp:dropdownlist>&nbsp;
										<asp:label id="lblYear" runat="server" Width="30px" CssClass="tableLabel" Height="15px">Year</asp:label>&nbsp;
										<cc1:mstextbox id="txtYear" runat="server" Width="83" CssClass="textField" NumberPrecision="4"
											NumberMinValue="1" NumberMaxValue="2999" TextMaskType="msNumeric" MaxLength="5"></cc1:mstextbox></TD>
									<td style="WIDTH: 1px"></td>
								</TR>
								<TR>
									<td style="HEIGHT: 20px"></td>
									<TD style="HEIGHT: 20px" colSpan="3">&nbsp;
										<asp:radiobutton id="rbPeriod" runat="server" Width="62px" CssClass="tableRadioButton" Text="Period"
											GroupName="EnterDate" AutoPostBack="True"></asp:radiobutton>&nbsp;&nbsp;&nbsp;
										<cc1:mstextbox id="txtPeriod" runat="server" Width="82" CssClass="textField" TextMaskType="msDate"
											MaxLength="10" TextMaskString="99/99/9999"></cc1:mstextbox>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
										&nbsp;
										<cc1:mstextbox id="txtTo" runat="server" Width="83" CssClass="textField" TextMaskType="msDate"
											MaxLength="10" TextMaskString="99/99/9999"></cc1:mstextbox></TD>
									<td style="WIDTH: 1px; HEIGHT: 20px"></td>
								</TR>
								<TR>
									<td></td>
									<TD colSpan="3">&nbsp;
										<asp:radiobutton id="rbDate" runat="server" Width="74px" CssClass="tableRadioButton" Text="Date"
											Height="22px" GroupName="EnterDate" AutoPostBack="True"></asp:radiobutton>&nbsp;&nbsp;<cc1:mstextbox id="txtDate" runat="server" Width="82" CssClass="textField" TextMaskType="msDate"
											MaxLength="10" TextMaskString="99/99/9999"></cc1:mstextbox></TD>
									<td style="WIDTH: 1px"></td>
								</TR>
							</TABLE>
						</fieldset>
					</TD>
					<TD>
						<fieldset style="WIDTH: 357px; HEIGHT: 105px"><legend><asp:label id="lblPayerCustomer" runat="server" Width="110px" CssClass="tableHeadingFieldset"
									Font-Bold="True">Payer / Customer</asp:label></legend>
							<TABLE style="WIDTH: 344px; HEIGHT: 73px" id="tblPayerType" border="0" cellSpacing="0"
								cellPadding="0" align="left" runat="server">
								<TR>
									<td></td>
									<TD class="tableLabel" colSpan="3">&nbsp;
										<asp:label style="Z-INDEX: 0" id="lblPayerIdCus" runat="server" Width="90px" CssClass="tableLabel"
											Height="22px">Payer Code</asp:label></TD>
									<td><cc1:mstextbox style="Z-INDEX: 0" id="txtPayerCode" runat="server" Width="148px" CssClass="textField"
											TextMaskType="msUpperAlfaNumericWithUnderscore" MaxLength="20"></cc1:mstextbox><asp:button style="Z-INDEX: 0" id="btnPayerCode" runat="server" CssClass="searchButton" Text="..."
											CausesValidation="False"></asp:button></td>
								</TR>
								<TR>
									<TD bgColor="blue" height="33"></TD>
									<TD class="tableLabel" height="33" colSpan="3">&nbsp;
										<asp:label id="lblPayerTypeCus" runat="server" Width="90px" CssClass="tableLabel" Height="22px">Payer Type</asp:label></TD>
									<TD height="33">
										<!--<select style="WIDTH: 148px; HEIGHT: 60px" id="lsbCustType" multiple size="4" name="lsbCustType">
											<option value="C" >Customer</option>
											<option value="A">Agent</option>
											<option value="N">Non-Revenue</option>
											<option value="V">AA</option>
											<option value="E">AAA</option>
										</select>--><asp:listbox id="lsbCustType" runat="server" Width="148" Height="60" SelectionMode="Multiple">
											<asp:ListItem Value="C">Customer</asp:ListItem>
											<asp:ListItem Value="A">Agent</asp:ListItem>
											<asp:ListItem Value="N">Non-Revenue</asp:ListItem>
											<asp:ListItem Value="V">AA</asp:ListItem>
											<asp:ListItem Value="E">AAA</asp:ListItem>
										</asp:listbox></TD>
								</TR>
								<TR>
									<td height="33"></td>
									<TD class="tableLabel" height="33" colSpan="3">&nbsp;
										<asp:label style="Z-INDEX: 0" id="lblMasterAcc" runat="server" Width="90px" CssClass="tableLabel"
											Height="22px">Master Account</asp:label></TD>
									<td><cc1:mstextbox style="Z-INDEX: 0" id="txtMasterAcc" runat="server" Width="148px" CssClass="textField"
											TextMaskType="msUpperAlfaNumericWithUnderscore" MaxLength="20"></cc1:mstextbox></td>
								</TR>
							</TABLE>
						</fieldset>
					</TD>
				</TR>
				<TR>
					<TD style="WIDTH: 464px">
						<fieldset style="WIDTH: 458px; HEIGHT: 135px"><legend><asp:label id="lblRouteType" runat="server" Width="130px" CssClass="tableHeadingFieldset" Font-Bold="True">Route Type / Service</asp:label></legend>
							<TABLE style="WIDTH: 450px; HEIGHT: 69px" id="tblRouteType" border="0" cellSpacing="0"
								cellPadding="0" align="left" runat="server">
								<TR>
									<td></td>
									<TD style="WIDTH: 454px" class="tableLabel" colSpan="3">&nbsp;
										<asp:radiobutton id="rbPickUp" runat="server" Width="100px" CssClass="tableRadioButton" Text="Pickup"
											Height="22px" Checked="True" GroupName="QueryByRoute" AutoPostBack="True"></asp:radiobutton><asp:radiobutton id="rbDelivery" runat="server" Width="104px" CssClass="tableRadioButton" Text="Delivery"
											Height="22px" GroupName="QueryByRoute" AutoPostBack="True"></asp:radiobutton></TD>
									<td style="WIDTH: 2px"></td>
								</TR>
								<tr>
									<td></td>
									<TD class="tableLabel" colSpan="2">&nbsp;
										<asp:label id="lblRouteCode" runat="server" Width="111px" CssClass="tableLabel" Height="22px">Route Code</asp:label></TD>
									<TD style="WIDTH: 454px" class="tableLabel" colSpan="3">
										<!--<cc1:mstextbox id="txtRouteCode" runat="server" Width="151px" CssClass="textField" AutoPostBack="True"
											TextMaskType="msUpperAlfaNumericWithUnderscore" MaxLength="12"></cc1:mstextbox>&nbsp;
										<asp:button id="btnRouteCode" runat="server" CssClass="searchButton" Text="..." CausesValidation="False"></asp:button>-->
										<asp:DropDownList id="cboRouteCode" runat="server"></asp:DropDownList></TD>
									<td style="WIDTH: 2px"></td>
								</tr>
								<tr>
									<td></td>
									<TD class="tableLabel" colSpan="2">&nbsp;
										<asp:label id="lblServiceType" runat="server" Width="111px" CssClass="tableLabel" Height="22px">Service Type</asp:label></TD>
									<TD style="WIDTH: 340px" class="tableLabel"><asp:listbox id="lsbServiceType" runat="server" Width="145" Height="60" SelectionMode="Multiple"></asp:listbox></TD>
									<td style="WIDTH: 2px"></td>
								</tr>
							</TABLE>
						</fieldset>
					</TD>
					<TD>
						<fieldset style="WIDTH: 359px; HEIGHT: 135px"><legend><asp:label id="lblDestination" runat="server" Width="79px" CssClass="tableHeadingFieldset"
									Font-Bold="True">Destination</asp:label></legend>
							<TABLE id="tblDestination" border="0" cellSpacing="0" cellPadding="0" align="left" runat="server">
								<tr height="37">
									<td></td>
									<TD style="WIDTH: 360px" class="tableLabel" colSpan="3">&nbsp;
										<asp:label id="lblZipCode" runat="server" Width="74px" CssClass="tableLabel">Postal Code</asp:label>&nbsp;&nbsp;&nbsp;
										<cc1:mstextbox id="txtZipCode" runat="server" Width="139px" CssClass="textField" TextMaskType="msUpperAlfaNumericWithHyphen"
											MaxLength="10"></cc1:mstextbox><asp:button id="btnZipCode" runat="server" CssClass="searchButton" Text="..." CausesValidation="False"></asp:button></TD>
									<td></td>
								</tr>
								<tr>
									<td></td>
									<TD style="WIDTH: 360px" class="tableLabel" colSpan="3">&nbsp;
										<asp:label id="lblStateCode" runat="server" Width="90px" CssClass="tableLabel">State Code</asp:label><cc1:mstextbox id="txtStateCode" runat="server" Width="139" CssClass="textField" TextMaskType="msUpperAlfaNumericWithUnderscore"
											MaxLength="10"></cc1:mstextbox><asp:button id="btnStateCode" runat="server" CssClass="searchButton" Text="..." CausesValidation="False"></asp:button></TD>
									<td></td>
								</tr>
							</TABLE>
						</fieldset>
					</TD>
				</TR>
				<TR>
					<TD colSpan="2">
						<TABLE width="100%">
							<TR>
								<TD width="40%">
									<fieldset style="WIDTH: 351px; HEIGHT: 150px"><legend><asp:label id="lblShipment" runat="server" Width="65px" CssClass="tableHeadingFieldset" Font-Bold="True">Shipment</asp:label></legend>
										<TABLE style="WIDTH: 343px; HEIGHT: 121px" id="tblShipment" border="0" cellSpacing="0"
											cellPadding="0" align="left" runat="server">
											<tr>
												<TD width="4"></TD>
												<TD class="tableLabel" colSpan="3"><asp:label style="Z-INDEX: 0" id="lblConsignmentNo" runat="server" Width="110px" CssClass="tableLabel"
														Height="18px">Consignment No</asp:label></TD>
												<TD><cc1:mstextbox style="Z-INDEX: 0" id="txtConsignmentNo" runat="server" CssClass="tableTextbox" Width="161px" AutoPostBack="True" MaxLength="32"></cc1:mstextbox></TD>
											</tr>
											<tr>
												<TD></TD>
												<TD class="tableLabel" colSpan="3"><asp:label id="lblRefNo" runat="server" Width="110px" CssClass="tableLabel" Height="18px">Customer Ref. #</asp:label></TD>
												<TD><asp:textbox id="txtRefNo" tabIndex="5" runat="server" Width="161px" CssClass="textField" AutoPostBack="True"
														MaxLength="20"></asp:textbox></TD>
											</tr>
											<tr>
												<TD></TD>
												<TD class="tableLabel" colSpan="3"><asp:label style="Z-INDEX: 0" id="lblBookingNo" runat="server" Width="80px" CssClass="tableLabel"
														Height="18px">Booking No</asp:label></TD>
												<TD><cc1:mstextbox style="Z-INDEX: 0" id="txtBookingNo" runat="server" Width="130px" CssClass="textField"
														AutoPostBack="True" NumberPrecision="10" NumberMaxValue="2147483647" TextMaskType="msNumeric"
														MaxLength="10"></cc1:mstextbox></TD>
											</tr>
											<tr>
												<td></td>
												<TD class="tableLabel" colSpan="3"><asp:label style="Z-INDEX: 0" id="lblBookingType" runat="server" Width="115px" CssClass="tableLabel"
														Height="22px">Booking Type</asp:label></TD>
												<td><asp:dropdownlist style="Z-INDEX: 0" id="Drp_BookingType" runat="server" Width="130px" CssClass="textField"></asp:dropdownlist></td>
											</tr>
											<tr>
												<td></td>
												<TD class="tableLabel" colSpan="3"></TD>
											</tr>
										</TABLE>
									</fieldset>
								</TD>
								<TD width="60%">
									<fieldset style="WIDTH: 477px; HEIGHT: 150px"><legend><asp:label id="lblStatus" runat="server" Width="40px" CssClass="tableHeadingFieldset" Font-Bold="True">Status</asp:label></legend>
										<TABLE style="WIDTH: 470px; HEIGHT: 121px" id="Table1" border="0" cellSpacing="0" cellPadding="0"
											align="left" runat="server">
											<TR>
												<TD width="4"></TD>
												<TD colSpan="2"><asp:label style="Z-INDEX: 0" id="lblStatusCode" runat="server" Width="75px" CssClass="tableLabel">Status Code</asp:label><cc1:mstextbox style="Z-INDEX: 0" id="txtStatusCode" runat="server" Width="100px" CssClass="textField"
														AutoPostBack="True" TextMaskType="msUpperAlfaNumericWithUnderscore" MaxLength="10"></cc1:mstextbox><asp:button style="Z-INDEX: 0" id="btnStatusCode" runat="server" CssClass="searchButton" Text="..."
														CausesValidation="False"></asp:button>&nbsp;&nbsp;&nbsp;&nbsp;
													<asp:label style="Z-INDEX: 0" id="lblExceptionCode" runat="server" Width="89px" CssClass="tableLabel">Exception Code</asp:label><cc1:mstextbox style="Z-INDEX: 0" id="txtExceptionCode" runat="server" Width="100px" CssClass="textField"
														AutoPostBack="True" TextMaskType="msUpperAlfaNumericWithUnderscore" MaxLength="10"></cc1:mstextbox><asp:button style="Z-INDEX: 0" id="btnExceptionCode" runat="server" CssClass="searchButton"
														Text="..." CausesValidation="False"></asp:button></TD>
											</TR>
											<TR>
												<TD style="HEIGHT: 20px" width="4"></TD>
												<TD style="WIDTH: 150px; HEIGHT: 20px"><asp:label id="lblStatusIs" runat="server" CssClass="tableLabel">Status Is :</asp:label></TD>
												<TD style="HEIGHT: 20px"><asp:label style="Z-INDEX: 0" id="lblInvoiceIssued" runat="server" Width="110px" CssClass="tableLabel">Invoice Issued</asp:label><asp:radiobutton style="Z-INDEX: 0" id="rbInvoiceYes" runat="server" Width="64px" CssClass="tableRadioButton"
														Text="Yes" Height="22px" GroupName="QueryByInvoice" AutoPostBack="True"></asp:radiobutton><asp:radiobutton style="Z-INDEX: 0" id="rbInvoiceNo" runat="server" Width="50px" CssClass="tableRadioButton"
														Text="No" Height="22px" GroupName="QueryByInvoice" AutoPostBack="True"></asp:radiobutton><asp:radiobutton style="Z-INDEX: 0" id="rbInvoiceIgnore" runat="server" Width="50px" CssClass="tableRadioButton"
														Text="Ignore" Height="22px" Checked="True" GroupName="QueryByInvoice" AutoPostBack="True"></asp:radiobutton></TD>
											</TR>
											<TR>
												<TD style="HEIGHT: 20px" width="4"></TD>
												<TD style="WIDTH: 150px; HEIGHT: 20px">&nbsp;&nbsp;&nbsp;
													<asp:radiobutton style="Z-INDEX: 0" id="rbLastInHistory" runat="server" Width="130px" CssClass="tableRadioButton"
														Text="Last in history" Checked="True" GroupName="StatusIs" AutoPostBack="True"></asp:radiobutton></TD>
												<TD style="HEIGHT: 20px"><asp:label style="Z-INDEX: 0" id="lblShipmentClosed" runat="server" Width="110px" CssClass="tableLabel">Shipment Closed</asp:label><asp:radiobutton style="Z-INDEX: 0" id="rbCheckAllYes" runat="server" Width="64px" CssClass="tableRadioButton"
														Text="Yes" GroupName="SearchAll" AutoPostBack="True"></asp:radiobutton><asp:radiobutton style="Z-INDEX: 0" id="rbCheckAllNo" runat="server" Width="50px" CssClass="tableRadioButton"
														Text="No" GroupName="SearchAll" AutoPostBack="True"></asp:radiobutton><asp:radiobutton style="Z-INDEX: 0" id="rbCheckAllIgnore" runat="server" Width="45px" CssClass="tableRadioButton"
														Text="Ignore" Checked="True" GroupName="SearchAll" AutoPostBack="True"></asp:radiobutton></TD>
											</TR>
											<TR>
												<TD style="HEIGHT: 18px" width="4"></TD>
												<TD style="WIDTH: 150px; HEIGHT: 18px">&nbsp;&nbsp;&nbsp;
													<asp:radiobutton style="Z-INDEX: 0" id="rbInHistory" runat="server" Width="130px" CssClass="tableRadioButton"
														Text="In history" GroupName="StatusIs" AutoPostBack="True"></asp:radiobutton></TD>
												<TD style="HEIGHT: 18px"></TD>
											</TR>
											<TR>
												<TD width="4"></TD>
												<TD style="WIDTH: 150px; HEIGHT: 18px">&nbsp;&nbsp;&nbsp;
													<asp:radiobutton style="Z-INDEX: 0" id="rbMissing" runat="server" Width="130px" CssClass="tableRadioButton"
														Text="Missing" GroupName="StatusIs" AutoPostBack="True"></asp:radiobutton></TD>
												<TD></TD>
											</TR>
										</TABLE>
									</fieldset>
								</TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
			</TABLE>
			<asp:label style="Z-INDEX: 103; POSITION: absolute; TOP: 4px; LEFT: 20px" id="Label1" runat="server"
				Width="365px" CssClass="maintitleSize" Height="27px">Shipment Tracking</asp:label></TR></TABLE></TR></TABLE></form>
	</body>
</HTML>
