using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.common.classes;
using com.common.DAL;
using com.common.RBAC;
using com.ties.DAL;
using com.common.util;
using com.ties.classes;
using com.common.applicationpages;
namespace com.ties
{
	/// <summary>
	/// Summary description for DispatchTrackingQueryPage.
	/// </summary>
	public class DispatchTrackingQueryPage : BasePopupPage
	{
		protected System.Web.UI.WebControls.DataGrid m_dgDispatch;
		protected System.Web.UI.WebControls.Button btnPrevPage;
		protected System.Web.UI.WebControls.Button btnFirstPage;
		protected System.Web.UI.WebControls.Button btnLastPage;
		protected System.Web.UI.WebControls.Button btnNextPage;
		protected System.Web.UI.WebControls.TextBox txtPageFind;
	
		private SessionDS m_sdsDispatch = null;
		private String m_strAppID;
		private String m_strEnterpriseID;
		protected System.Web.UI.WebControls.Label lblErrorMsg;
		protected System.Web.UI.WebControls.Button btnClose;
		private String m_strCulture;
		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			Utility utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings,Page.Session);
			m_strAppID = utility.GetAppID();
			m_strEnterpriseID = utility.GetEnterpriseID();
			m_strCulture = utility.GetUserCulture();
			if(!Page.IsPostBack)
			{
				if((bool)Session["toRefresh"])
				{
					Session["toRefresh"]=false;
					m_dgDispatch.CurrentPageIndex = (int)Session["CURRENT_PAGE"];
					int iStartIndex = m_dgDispatch.CurrentPageIndex * m_dgDispatch.PageSize;
					String strType = (String)Session["QUERY_TYPE"];//Request.Params["QUERY_TYPE"];

					checkQueryType(iStartIndex, strType);
					Session["SESSION_DS1"] = m_sdsDispatch;
					BindDispatch();
				}
				else
				{
					ViewState["SCOperation"] = Operation.None;
					Session["toRefresh"]=false;
					Session["CURRENT_PAGE"]=0;
					m_sdsDispatch = (SessionDS)Session["SESSION_DS1"];
					BindDispatch();
				}
				
			}
			
			
		}
		/// <summary>
		/// Bind data to datagrid
		/// </summary>
		private void BindDispatch()
		{
			//Response.Write("<script>alert('"+System.Convert.ToInt32(m_sdsDispatch.QueryResultMaxSize)+"');</script>");

			m_dgDispatch.VirtualItemCount = System.Convert.ToInt32(m_sdsDispatch.QueryResultMaxSize);
			m_dgDispatch.DataSource =  m_sdsDispatch.ds;
			m_dgDispatch.DataBind();
			Session["SESSION_DS1"] = m_sdsDispatch;
		}
		/// <summary>
		/// On edit event handler, allows to popup another window
		/// that shows the deletion history
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		public void OnEdit_DispatchHistory(object sender, System.Web.UI.WebControls.DataGridCommandEventArgs e) 
		{	
			int rowNum = e.Item.ItemIndex;
			if(rowNum>=0)
			{
				LinkButton lnkbtnBookNo = (LinkButton)m_dgDispatch.Items[rowNum].FindControl("lnkbtnBookNo");
				String strBookNo = lnkbtnBookNo.Text;
				try
				{
					if(e.CommandName=="DispatchDeletePopUp")
					{	
						Session["SESSION_DS2"] = DispatchTrackingMgrDAL.QueryDispatchForDeletion(m_strAppID, m_strEnterpriseID, strBookNo, 0,25 );
						Label lblBookDateTime = (Label)e.Item.FindControl("lblBookDateTime");
						lblErrorMsg.Text = "";
						Session["FORMID"] = strBookNo;
						Session["BOOK_DATE"] = lblBookDateTime.Text;
						Session["INFO_INDEX"] = e.Item.ItemIndex;
						OpenWindowpage("DispatchRecordDeletion.aspx?FORMID="+strBookNo+"&BOOK_DATE="+lblBookDateTime.Text+"&INFO_INDEX="+e.Item.ItemIndex+"");
					}
					if(e.CommandName=="Pickup_Route_Search")
					{
						String strPickupRoute = null;
						String strtxtPickupRoute = null;

						msTextBox txtPickup_Route = (msTextBox)e.Item.FindControl("txtPickup_Route");
						if(txtPickup_Route != null)
						{
							strtxtPickupRoute = txtPickup_Route.ClientID;
							strPickupRoute = txtPickup_Route.Text;
							m_sdsDispatch = (SessionDS)Session["SESSION_DS1"];
							DataRow[] dr = m_sdsDispatch.ds.Tables[0].Select(string.Format("booking_no = {0}",strBookNo));
							String strSenderZip = dr[0]["sender_zipcode"].ToString();
							OpenWindowpage("RouteCodePopup.aspx?FORMID=DispatchTrackingQueryPage&ROUTECODE_TEXT="+strtxtPickupRoute+"&ROUTECODE="+strPickupRoute+"&BOOKNO="+strBookNo+"&SENDZIP="+strSenderZip);
						}
//						OpenWindowpage("RouteCodePopup.aspx?FORMID=DispatchTrackingQueryPage&ROUTECODE_TEXT="+strtxtPickupRoute+"&ROUTECODE="+strPickupRoute);
					}
					if(e.CommandName=="DispatchStatusUpdate")
					{
						lblErrorMsg.Text = "";
						OpenWindowpage("DispatchStatusUpdate.aspx?INFO_INDEX="+e.Item.ItemIndex+" ");
					}
					if(e.CommandName=="Cancel")
					{
						lblErrorMsg.Text = "";
						this.m_dgDispatch.EditItemIndex = -1;
						m_sdsDispatch = (SessionDS)Session["SESSION_DS1"];
						Session["SESSION_DS1"] = m_sdsDispatch;
						Session["CURRENT_PAGE"] = m_dgDispatch.CurrentPageIndex;
						BindDispatch();
						this.m_dgDispatch.Columns[15].Visible=true;
					}
					if(e.CommandName=="Update")
					{
						lblErrorMsg.Text = "";

						String strPickupRoute = null;

						msTextBox txtPickup_Route = (msTextBox)e.Item.FindControl("txtPickup_Route");
						if(txtPickup_Route != null)
						{
							strPickupRoute = txtPickup_Route.Text;
						}

						DispatchTrackingMgrDAL.UpdatePickUpRoute(m_strAppID,this.m_strEnterpriseID,strBookNo,strPickupRoute);

						this.m_dgDispatch.EditItemIndex = -1;

						Session["toRefresh"]=false;
						m_dgDispatch.CurrentPageIndex = (int)Session["CURRENT_PAGE"];
						int iStartIndex = m_dgDispatch.CurrentPageIndex * m_dgDispatch.PageSize;
						String strType = (String)Session["QUERY_TYPE"];//Request.Params["QUERY_TYPE"];

						checkQueryType(iStartIndex, strType);
				
						Session["SESSION_DS1"] = m_sdsDispatch;

						BindDispatch();
						//
						//						m_sdsDispatch = (SessionDS)Session["SESSION_DS1"];
						//						Session["SESSION_DS1"] = m_sdsDispatch;
						//						Session["CURRENT_PAGE"] = m_dgDispatch.CurrentPageIndex;
						//						BindDispatch();
						this.m_dgDispatch.Columns[15].Visible=true;
					}
				}
				catch(Exception err)
				{
					String msg = err.ToString();
					lblErrorMsg.Text = "No status codes found in Dispatch Tracking Table, check if Pickup Request record has inserted a new status code";
				}
				
			}
			
		}
		/// <summary>
		/// On updateing, popup another window for user to add new record to dispatch tracking
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		public void OnUpdate_Dispatch(object sender, System.Web.UI.WebControls.DataGridCommandEventArgs e) 
		{
	
			
		}

		public void dgDispatch_Edit(object sender, System.Web.UI.WebControls.DataGridCommandEventArgs e) 
		{
			lblErrorMsg.Text = "";	

			this.m_dgDispatch.EditItemIndex = e.Item.ItemIndex;

			m_sdsDispatch = (SessionDS)Session["SESSION_DS1"];

			Session["SESSION_DS1"] = m_sdsDispatch;

			//Session["SESSION_DS1"] = m_sdsDispatch;
			//m_dgDispatch.CurrentPageIndex = e.NewPageIndex;
			Session["CURRENT_PAGE"] = m_dgDispatch.CurrentPageIndex;
			BindDispatch();
			this.m_dgDispatch.Columns[15].Visible=true;

			//
			//			ViewState["Operation"] = Operation.Update;
			//			Session["dsBaseRates_Import"] = dsBaseRates_Import;
			//			BindGrid();
			//
			//			dgBZRates.Columns[0].Visible = false;
			//			return;

		}
		/// <summary>
		/// To open another window
		/// </summary>
		/// <param name="strUrl">url to specify path</param>
		private void OpenWindowpage(String strUrl)
		{
			ArrayList paramList = new ArrayList();
			paramList.Add(strUrl);
			String sScript = Utility.GetScript("PkgDetails.js",paramList);
			Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
		}
		
		/// <summary>
		/// To display acutal text on datagrid when retreiveing from Database
		/// e.g When retreive from database dispatch types is "C" buit display 
		/// on datagrid as "Call-In"
		/// 
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void OnDataBound_DispatchQueryPage(object sender, DataGridItemEventArgs e)
		{
			Label lblDispatchType = (Label)e.Item.FindControl("lblDispatchType");
			ArrayList listCodeTxt = Utility.GetCodeValues(m_strAppID,m_strCulture, "booking_type", CodeValueType.StringValue);
			if(lblDispatchType!=null)
			{
				foreach(SystemCode typeSysCode in listCodeTxt)
				{
					if(lblDispatchType.Text == typeSysCode.StringValue)
					{
						lblDispatchType.Text = typeSysCode.Text;
					}
				}
			}
		}
		/// <summary>
		/// To refresh data on datagrid
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void OnDispatchQuery_PageChange(object sender, DataGridPageChangedEventArgs e)
		{
			m_dgDispatch.CurrentPageIndex = e.NewPageIndex;
			int iStartIndex = m_dgDispatch.CurrentPageIndex * m_dgDispatch.PageSize;
			String strType = (String)Session["QUERY_TYPE"];//Request.Params["QUERY_TYPE"];

			checkQueryType(iStartIndex, strType);

			Session["SESSION_DS1"] = m_sdsDispatch;
			m_dgDispatch.CurrentPageIndex = e.NewPageIndex;
			Session["CURRENT_PAGE"] = m_dgDispatch.CurrentPageIndex;

			decimal iPageCnt = (m_sdsDispatch.QueryResultMaxSize - 1)/m_dgDispatch.PageSize;
			if(iPageCnt < m_dgDispatch.CurrentPageIndex)
			{
				m_dgDispatch.CurrentPageIndex = System.Convert.ToInt32(iPageCnt);
				//Fixed by ...(26-Feb-2009)
				Session["CURRENT_PAGE"] = m_dgDispatch.CurrentPageIndex;
				//Fixed by ...(26-Feb-2009)
			}

			BindDispatch();
		}
		private void checkQueryType(int iStartIndex, String strType)
		{
			if(strType == "OutStandingStatus")
			{
				m_sdsDispatch = (SessionDS)Session["SESSION_DS2"];
				string[] strStatusArray = TIESUtility.getOutstandingDispatchStatus(m_strAppID, m_strEnterpriseID);
				//Fixed by ...(26-Feb-2009)
				m_sdsDispatch = DispatchTrackingMgrDAL.GetOutStandingDispatch(strStatusArray,m_strAppID, m_strEnterpriseID, Request.Params["PickFrom"].ToString(), Request.Params["PickTo"].ToString(), iStartIndex, m_dgDispatch.PageSize,m_strCulture);
				//Fixed by ...(26-Feb-2009)
			}
			else if(strType == "FALSE_CHECK")
			{
				DataSet dsDispatch = (DataSet)Session["QUERY_DS"];
				m_sdsDispatch = DispatchTrackingMgrDAL.QueryDispatch(null, dsDispatch,m_strAppID,m_strEnterpriseID, iStartIndex, m_dgDispatch.PageSize,m_strCulture);
			
			}
			else if(strType == "TRUE_CHECK")
			{
				DataSet dsDispatch = (DataSet)Session["QUERY_DS"];
				string[] strStatusArray = TIESUtility.getOutstandingDispatchStatus(m_strAppID, m_strEnterpriseID);
				m_sdsDispatch = DispatchTrackingMgrDAL.QueryDispatch(strStatusArray, dsDispatch,m_strAppID,m_strEnterpriseID, iStartIndex, m_dgDispatch.PageSize,m_strCulture);
			
			}
		}
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			base.OnInit(e);
			InitializeComponent();
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.m_dgDispatch.SelectedIndexChanged += new System.EventHandler(this.m_dgDispatch_SelectedIndexChanged);
			this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		/// <summary>
		/// To close current window
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void btnClose_Click(object sender, System.EventArgs e)
		{
			String sScript = "";
			sScript += "<script language=javascript>";
			//	sScript += "  window.opener;" ;
			sScript += "  window.close();";
			sScript += "</script>";
			Response.Write(sScript);
		}

		private void m_dgDispatch_SelectedIndexChanged(object sender, System.EventArgs e)
		{
		
		}
	}
}
