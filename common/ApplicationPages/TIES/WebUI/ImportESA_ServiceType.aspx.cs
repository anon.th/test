using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.common.util;
using com.ties.DAL;
using com.common.classes;
using com.common.applicationpages;
using System.Text;
using System.Configuration;
using System.Data.OleDb;
using com.ties.classes;

namespace com.ties
{
	/// <summary>
	/// Summary description for ImportESA_ServiceType.
	/// </summary>
	public class ImportESA_ServiceType : BasePage
	{
		protected System.Web.UI.WebControls.TextBox txtFilePath;
		protected System.Web.UI.WebControls.Button btnImport_Save;
		protected System.Web.UI.WebControls.Button btnImport;
		protected System.Web.UI.WebControls.Button btnImport_Cancel;
		protected System.Web.UI.WebControls.Button btnImport_Show;
		protected System.Web.UI.WebControls.Button btnInsert;
		protected System.Web.UI.WebControls.Button btnExecuteQuery;
		protected System.Web.UI.WebControls.Button btnQuery;
		protected System.Web.UI.WebControls.Label lblTitle;
		protected System.Web.UI.HtmlControls.HtmlTable xxx;
		protected System.Web.UI.WebControls.DataGrid dgImportESA_Service;
		protected System.Web.UI.WebControls.Label lblErrorMessage;
		protected System.Web.UI.HtmlControls.HtmlInputFile inFile;

		//DataSet dsImportESA_ServiceType = null;

		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			if(!Page.IsPostBack)
			{
//				this.lblErrorMessage.Text="";
//				//				dsImportESA_ServiceType = SysDataManager2.GetEmptyESAServiceTypeDS().ds;
//				//				Session["dsImportESA_Service"] = dsImportESA_ServiceType;
//				this.dgImportESA_Service.EditItemIndex = 0;
//				this.dgImportESA_Service.DataSource = SysDataManager2.GetEmptyESAServiceTypeDS().ds;
//				this.dgImportESA_Service.DataBind();
//				dgImportESA_Service.Columns[0].Visible = false;  
//				dgImportESA_Service.Columns[1].Visible = false;  
//				dgImportESA_Service.Columns[2].Visible = false;
//				Session["SaveAble"]=false;

				QueryMode();
			}
		}

		public bool ckEffectiveDate(DateTime effDate)
		{
			DateTime effStart = System.DateTime.Now;
			ViewState["effStart"] = effStart;
			DateTime effEnd = System.DateTime.Now;
			bool ckDate = false;

			DataSet effDS = new DataSet();
			effDS = SysDataMgrDAL.GetEndRoundEffectiveDate(utility.GetAppID(),utility.GetEnterpriseID(),effStart.ToString("dd/MM/yyyy"));
			DataTable effDT = new DataTable();
			effDT = effDS.Tables[0];
			foreach(DataRow dr in effDT.Rows)
			{
				effEnd = Convert.ToDateTime(dr["endEffDate"].ToString());
				ViewState["effEnd"] = effEnd;
			}

			string getdate = "";
			if(effDate.Day.ToString().Length == 1)
				getdate = "0"+effDate.Day.ToString();
			else
				getdate = effDate.Day.ToString();

			if(effDate.Month.ToString().Length == 1)
				getdate += "/0"+effDate.Month.ToString()+"/"+effDate.Year.ToString();
			else
				getdate += "/"+effDate.Month.ToString()+"/"+effDate.Year.ToString();

			DateTime dt = DateTime.ParseExact(getdate,"dd/MM/yyyy",null);

			if(dt >= effStart && dt <= effEnd)
			{
				ckDate = true;
			}
			else
			{
				ckDate = false;
			}

			return ckDate;
		}


		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnQuery.Click += new System.EventHandler(this.btnQuery_Click);
			this.btnExecuteQuery.Click += new System.EventHandler(this.btnExecuteQuery_Click);
			this.btnImport.Click += new System.EventHandler(this.btnImport_Click);
			this.btnImport_Save.Click += new System.EventHandler(this.btnImport_Save_Click);
			this.txtFilePath.TextChanged += new System.EventHandler(this.txtFilePath_TextChanged);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private void btnImport_Click(object sender, System.EventArgs e)
		{
			ViewState["Mode"] = "Import";
			btnImport.Enabled = false;
			try
			{
				if(uploadFiletoServer())
				{
					//Cleare existing data
					//ImportConsignmentsDAL.ClearTempDataByUserId(appID, enterpriseID, userID);

					//Get data from Excel WorkSheet
					getExcelFiles();

					DataSet ds = (DataSet) Session["dsImportESA_Service"];

					//					ds = EliminateDuplicateRecords(ds);
					//
					//					ds = ValidateCustZones(ds);

					this.dgImportESA_Service.DataSource = ds;
					this.dgImportESA_Service.EditItemIndex=-1;
					this.dgImportESA_Service.DataBind();

					//					dgImportESA_Service.Columns[0].Visible = true;  
					//					dgImportESA_Service.Columns[1].Visible = true;
					if(ds.Tables[0].Rows.Count>0)
					{
						this.btnImport_Save.Enabled = true;
						dgImportESA_Service.Columns[0].Visible = true;
						dgImportESA_Service.Columns[1].Visible = true;
						dgImportESA_Service.Columns[2].Visible = true; 
						dgImportESA_Service_Bound();

					}
					else
					{
						dgImportESA_Service.Columns[0].Visible = false;
						dgImportESA_Service.Columns[0].Visible = false; 
					}

					Session["dsImportESA_Service"] = ds;

				}
			}
			catch(Exception ex)
			{
				this.lblErrorMessage.Text = "Invalid date format in Excel file.";
				txtFilePath.Text = "";
				Session["SaveAble"]=false;
			}

			foreach(DataGridItem dgItem in dgImportESA_Service.Items)
			{
				DataSet dsESAService = new DataSet();

				ImageButton imgSelect = (ImageButton)dgItem.Cells[0].Controls[1];
				Label lblCustID = (Label)dgItem.FindControl("lblCustID");
				Label lblESA_Sector = (Label)dgItem.FindControl("lblESA_Sector");
				Label lblServiceCode = (Label)dgItem.FindControl("lblServiceCode");
				Label lblesa_surcharge_amt = (Label)dgItem.FindControl("lblesa_surcharge_amt");
				Label lblesa_surcharge_percent = (Label)dgItem.FindControl("lblesa_surcharge_percent");
				Label lblmin_esa_amount = (Label)dgItem.FindControl("lblmin_esa_amount");
				Label lblmax_esa_amount = (Label)dgItem.FindControl("lblmax_esa_amount");
				Label lblEffectivedate = (Label)dgItem.FindControl("lblEffectivedate");

				StringBuilder stbErrors = new StringBuilder();
				string ckCustID = "1";
				string ckESA_Sector = "1";
				string ckServiceCode = "1";
				string ckEsaSurAmt = "1";
				string ckEsaSurPer = "1";
				string ckMinEsa = "1";
				string ckDupEsa = "1";
				string ckEffDate = "1";

				if(lblCustID != null)
				{
					if(lblCustID.Text.Equals(""))
					{
						ckCustID = "0";
						stbErrors.Append("- Customer ID must not null! \n");
					}
					if(!SysDataManager2.ckCustID(utility.GetEnterpriseID(),utility.GetAppID(),lblCustID.Text))
					{
						ckCustID = "0";
						stbErrors.Append("- Customer does not exist! \n");
					}
				}
				if(lblESA_Sector != null)
				{
					if(lblESA_Sector.Text.Equals(""))
					{
						ckESA_Sector = "0";
						stbErrors.Append("- ESA Sector must not null! \n");
					}
					if(!SysDataManager2.ckZoneOrServiceInSystem(utility.GetEnterpriseID(),utility.GetAppID(),lblESA_Sector.Text,"ZoneESA"))
					{
						ckESA_Sector = "0";
						stbErrors.Append("- ESA Sector does not exist! \n");
					}
				}
				if(lblServiceCode != null)
				{
					if(lblServiceCode.Text.Equals(""))
					{
						ckServiceCode = "0";
						stbErrors.Append("- Service type must not null! \n");
					}
					if(!SysDataManager2.ckZoneOrServiceInSystem(utility.GetEnterpriseID(),utility.GetAppID(),lblServiceCode.Text,"ServiceType"))
					{
						ckESA_Sector = "0";
						stbErrors.Append("- Service type does not exist! \n");
					}
				}

				if(lblesa_surcharge_amt != null && lblesa_surcharge_percent != null)
				{
					if(lblesa_surcharge_amt.Text.Trim().Equals(""))
						lblesa_surcharge_amt.Text = "0.0";
					if(lblesa_surcharge_percent.Text.Trim().Equals(""))
						lblesa_surcharge_percent.Text = "0.0";
					if(Convert.ToDouble(lblesa_surcharge_amt.Text.Trim()) > 0.0 && Convert.ToDouble(lblesa_surcharge_percent.Text.Trim()) > 0.0)
					{
						ckEsaSurAmt = "0";
						ckEsaSurPer = "0";
						stbErrors.Append("- Please enter either ESA surcharge amount or ESA surcharge percent. \n");
					}
					else if(Convert.ToDouble(lblesa_surcharge_amt.Text.Trim()) < 0.0 && Convert.ToDouble(lblesa_surcharge_percent.Text.Trim()) < 0.0)
					{
						ckEsaSurAmt = "0";
						ckEsaSurPer = "0";
						stbErrors.Append("- ESA surcharge amount and ESA surchyarge percent must not nagative value! \n");
					}
					else if(Convert.ToDouble(lblesa_surcharge_amt.Text.Trim()) == 0.0 && Convert.ToDouble(lblesa_surcharge_percent.Text.Trim()) > 0.0)
					{
						if(Convert.ToDouble(lblesa_surcharge_percent.Text.Trim()) > 100)
						{
							ckEsaSurPer = "0";
							stbErrors.Append("- ESA surcharge percent must less than 100! \n");
						}
					}
				}
				if(lblmin_esa_amount != null && lblmax_esa_amount != null)
				{
					if(lblmin_esa_amount.Text.Trim().Equals(""))
						lblmin_esa_amount.Text = "0.00";
					if(lblmax_esa_amount.Text.Trim().Equals(""))
						lblmax_esa_amount.Text = "0.00";
					if(Convert.ToDouble(lblmin_esa_amount.Text) >= 0 && Convert.ToDouble(lblmax_esa_amount.Text) >= 0 && Convert.ToDouble(lblmin_esa_amount.Text) > Convert.ToDouble(lblmax_esa_amount.Text))
					{
						ckMinEsa = "0";
						stbErrors.Append("- Min value must not more than max value! \n");
					}
				}

				object objEffectiveDate = System.DBNull.Value;

				if(lblEffectivedate != null)
				{
					if(lblEffectivedate.Text.Equals(""))
					{
						ckEffDate = "0";
						stbErrors.Append("- Effective date must not null! \n");
					}
					DateTime dtStart = System.DateTime.Now;
					DateTime dtEnd = System.DateTime.Now.AddYears(1);
					dtEnd = dtEnd.AddDays(-1);
					objEffectiveDate = DateTime.ParseExact(lblEffectivedate.Text.ToString().Trim(),"dd/MM/yyyy",null);
					if(Convert.ToDateTime(objEffectiveDate).Date < dtStart.Date || Convert.ToDateTime(objEffectiveDate).Date > dtEnd.Date)
					{
						ckEffDate = "0";
						stbErrors.Append("- Effective date must between "+dtStart.ToString("dd/MM/yyyy")+" and "+dtEnd.ToString("dd/MM/yyyy")+". \n");
					}

					dsESAService = SysDataManager2.GetESAServiceTypeDS(utility.GetAppID(),utility.GetEnterpriseID(),lblCustID.Text,lblESA_Sector.Text,lblServiceCode.Text,Convert.ToDateTime(objEffectiveDate).Date.ToString());
					if(dsESAService.Tables[0].Rows.Count > 0)
					{
						ckDupEsa = "0";
						stbErrors.Append("- ESA service type is duplicated! \n");
					}
				}

				
				
				if(ckCustID.Equals("0")||ckESA_Sector.Equals("0")||ckServiceCode.Equals("0")||ckEsaSurAmt.Equals("0")||ckEsaSurPer.Equals("0")||ckMinEsa.Equals("0")||ckDupEsa.Equals("0")||ckEffDate.Equals("0")||ckDupEsa.Equals("0"))
				{
					imgSelect.ImageUrl  = "images/butt-red.gif";
					imgSelect.ToolTip = stbErrors.ToString();
					this.btnImport_Save.Enabled=false;
					//return;
				}
				else
				{
					imgSelect.ImageUrl  = "images/butt-select.gif";
					this.btnImport_Save.Enabled=true;
				}
			}
			dgImportESA_Service.Columns[0].Visible = true;  
			dgImportESA_Service.Columns[1].Visible = true;
			dgImportESA_Service.Columns[2].Visible = true;

		}

		private bool uploadFiletoServer()
		{
			bool status = true;

			if((inFile.PostedFile != null ) && (inFile.PostedFile.ContentLength > 0))
			{
				string extension = System.IO.Path.GetExtension(inFile.PostedFile.FileName);

				if(extension.ToUpper() == ".XLS")
				{
					string fn = System.IO.Path.GetFileName(inFile.PostedFile.FileName);
					string path = Server.MapPath("Excel") + "\\";
					string pathFn = Server.MapPath("Excel") + "\\" + this.User.ToString() + "_" + fn;

					ViewState["FileName"] = pathFn;

					try
					{
						if(System.IO.Directory.Exists(path) == false)
							System.IO.Directory.CreateDirectory(path);

						if (System.IO.File.Exists(pathFn)) 
							System.IO.File.Delete(pathFn);

						inFile.PostedFile.SaveAs(pathFn);

						this.lblErrorMessage.Text = "";
					}
					catch(Exception ex)
					{
						throw new ApplicationException("Error during upload file to the server", null);
					}
				}
				else
				{
					this.lblErrorMessage.Text = "Not a valid Excel Workbook.";
					txtFilePath.Text = "";
					status = false;
				}
			}
			else
			{
				lblErrorMessage.Text = "Please select a file to upload.";
				status = false;
			}

			return status;
		}


		private void getExcelFiles()
		{
			int i = 0;

			try
			{	
				String connString = ConfigurationSettings.AppSettings["ExcelConn"];
				connString = connString.Replace("(DestinationFile)", (String)ViewState["FileName"]);

				int recCouter = 1;

				StringBuilder strBuilder = new StringBuilder();
				strBuilder.Append("SELECT custid,esa_sector,service_code,esa_surcharge_amt,esa_surcharge_percent,min_esa_amount,max_esa_amount,effective_date ");
				//HC Return Task
				strBuilder.Append("FROM [ESA_ServiceType$] ");
				OleDbDataAdapter daBaseRates = new OleDbDataAdapter(strBuilder.ToString(), connString);

				DataSet dsExcel = new DataSet();
				daBaseRates.Fill(dsExcel);

				DataSet dsImportESA_Service = new DataSet();
				dsImportESA_Service = SysDataManager2.GetEmptyESAServiceTypeDS().ds;
				if(dsImportESA_Service.Tables[0].Rows.Count == 1)
				{
					dsImportESA_Service.Tables[0].Rows[0].Delete();
					dsImportESA_Service.AcceptChanges();
				}

				foreach(DataRow dr in dsExcel.Tables[0].Rows)
				{

					if((dr["custid"].ToString().Trim() != "") ||
						(dr["esa_sector"].ToString().Trim() != "") ||
						(dr["service_code"].ToString().Trim() != "") || 
						(dr["esa_surcharge_amt"].ToString().Trim() != "") || 
						(dr["esa_surcharge_percent"].ToString().Trim() != "") || 
						(dr["min_esa_amount"].ToString().Trim() != "") || 
						(dr["max_esa_amount"].ToString().Trim() != "") || 
						(dr["effective_date"].ToString().Trim() != ""))
					{
						DataRow tmpDr = dsImportESA_Service.Tables[0].NewRow();

						recCouter++;

						tmpDr["custid"] = Convert.ToString(dr["custid"]);
						tmpDr["esa_sector"] = Convert.ToString(dr["esa_sector"]);
						tmpDr["service_code"] = Convert.ToString(dr["service_code"]);

						if((dr["esa_surcharge_amt"]!= null) && (!dr["esa_surcharge_amt"].GetType().Equals(System.Type.GetType("System.DBNull"))))
							tmpDr["esa_surcharge_amt"] = Convert.ToDecimal(dr["esa_surcharge_amt"]);
						else
							tmpDr["esa_surcharge_amt"] = 0;

						if((dr["esa_surcharge_percent"]!= null) && (!dr["esa_surcharge_percent"].GetType().Equals(System.Type.GetType("System.DBNull"))))
							tmpDr["esa_surcharge_percent"] = Convert.ToDecimal(dr["esa_surcharge_percent"]);
						else
							tmpDr["esa_surcharge_percent"] = 0;

						if((dr["min_esa_amount"]!= null) && (!dr["min_esa_amount"].GetType().Equals(System.Type.GetType("System.DBNull"))))
							tmpDr["min_esa_amount"] = Convert.ToDecimal(dr["min_esa_amount"]);
						else
							tmpDr["min_esa_amount"] = 0;

						if((dr["max_esa_amount"]!= null) && (!dr["max_esa_amount"].GetType().Equals(System.Type.GetType("System.DBNull"))))
							tmpDr["max_esa_amount"] = Convert.ToDecimal(dr["max_esa_amount"]);
						else
							tmpDr["max_esa_amount"] = 0;

						tmpDr["effective_date"] = Convert.ToString(dr["effective_date"]);

						dsImportESA_Service.Tables[0].Rows.Add(tmpDr);
						dsImportESA_Service.AcceptChanges();
					}					
				}

				dsExcel.Dispose();
				dsExcel = null;

				i++;
				recCouter = 1;
				Session["dsImportESA_Service"] = dsImportESA_Service;
			}
			catch(Exception ex)
			{
				String strMsg = ex.Message;

				if(strMsg.IndexOf("'Consignments$' is not a valid name") != -1)
				{
					throw new ApplicationException("Excel Workbook must contain two named sheets: Consignments and Packages.", null);
				}
				else if(strMsg.IndexOf("'Packages$' is not a valid name") != -1)
				{
					throw new ApplicationException("Excel Workbook must contain two named sheets: Consignments and Packages.", null);
				}
				else if(strMsg.IndexOf("No value given for one or more required parameters") != -1)
				{
					if(i == 0)
						throw new ApplicationException("Consignments worksheet has invalid columns", null);
					else
						throw new ApplicationException("Packages worksheet has invalid columns", null);
				}
				else
				{
					throw ex;
				}
			}
		}

		private void btnImport_Save_Click(object sender, System.EventArgs e)
		{
			string strLabel="";
			DataSet dsImportESA_Service = (DataSet) Session["dsImportESA_Service"];
			DataSet dsESA_Service = new DataSet();

			for(int i=0;i<dsImportESA_Service.Tables[0].Rows.Count;i++)
			{

				dsESA_Service = SysDataManager2.GetESAServiceTypeDS(utility.GetAppID(),utility.GetEnterpriseID(),
					dsImportESA_Service.Tables[0].Rows[i][0].ToString(), dsImportESA_Service.Tables[0].Rows[i][1].ToString(), dsImportESA_Service.Tables[0].Rows[i][2].ToString(),dsImportESA_Service.Tables[0].Rows[i][7].ToString());
				
				if(dsESA_Service.Tables[0].Rows.Count<=0)
				{

					//					DataSet zoneCount = new DataSet();
					//					zoneCount = SysDataMgrDAL.Get_CheckCustZoneCode(utility.GetAppID(), utility.GetEnterpriseID(),
					//						dsCustZone_Import.Tables[0].Rows[i][1].ToString());
					//					if(zoneCount.Tables[0].Rows.Count<=0)
					//					{
					//						this.lblErrorMessage.Text = "Incorrect ZoneCode.";
					//						Response.Write("<script>alert('Incorrect ZoneCode.');</script>");
					//						return;
					//					}
					//
					//
					//
					//					Zipcode zipcode = new Zipcode();
					//					zipcode.Populate(utility.GetAppID(),utility.GetEnterpriseID(),dsCustZone_Import.Tables[0].Rows[i][0].ToString());
					//
					//					TextBox txtCountry = (TextBox)customerProfileMultiPage.FindControl("txtCountry");				
					//
					//					if(zipcode.Country!=txtCountry.Text)
					//					{
					//						this.lblErrorMessage.Text = "Incorrect ZipCode.";
					//						Response.Write("<script>alert('Incorrect ZipCode.');</script>");
					//						return;
					//					}

					int rowInsert = 0;
					rowInsert = SysDataManager2.InsertESAServiceType(dsImportESA_Service.Tables[0].Rows[i],utility.GetAppID(),utility.GetEnterpriseID());

					if(rowInsert > 0)
					{
						QueryMode();
						strLabel = "Insert "+rowInsert+" row(s).";
					}
				}
				else
				{
					if(dsESA_Service.Tables[0].Rows.Count > 0)
					{
						this.lblErrorMessage.Text= "Error. Duplicate Data.";
						return;
					}
	
				}
			}

			this.dgImportESA_Service.EditItemIndex = -1;
			Session["SaveAble"]=false;
			this.btnQuery_Click(this,new System.EventArgs());
			this.lblErrorMessage.Text= strLabel;

		}

		protected void dgImportESA_Service_Bound()//(object sender, DataGridItemEventArgs e)
		{
			for(int i=0;i<dgImportESA_Service.Items.Count;i++)
			{
				DataGridItem Item = dgImportESA_Service.Items[i];
				ImageButton imgSelect = (ImageButton)Item.FindControl("imgSelect");
				Label lblCustID = (Label)Item.FindControl("lblCustID");
				Label lblESA_Sector = (Label)Item.FindControl("lblESA_Sector");
				Label lblServiceCode = (Label)Item.FindControl("lblServiceCode");
				Label lblesa_surcharge_amt = (Label)Item.FindControl("lblesa_surcharge_amt");
				Label lblesa_surcharge_percent = (Label)Item.FindControl("lblesa_surcharge_percent");
				Label lblmin_esa_amount = (Label)Item.FindControl("lblmin_esa_amount");
				Label lblmax_esa_amount = (Label)Item.FindControl("lblmax_esa_amount");
				Label lblEffectivedate = (Label)Item.FindControl("lblEffectivedate");

				Customer cs = new Customer();
				cs.Populate(utility.GetAppID(),utility.GetEnterpriseID(),lblCustID.Text.ToString());

				bool bESA_Surcharge_MaxMin = true;
				bool bESA_Surcharge = true;
				bool bZoneCode = true;
				bool bServiceCode = true;
				bool bEffective = true;
				bool bCust = true;
				bool ckDupEsa = true;

				StringBuilder stbErrors = new StringBuilder();
				DataSet dsZone = new DataSet();

				//by aoo
				bool bESAbyService = true;


				try //Any errors are going to be FALSE
				{
					string cn = cs.CustomerName;
					if(cn==null)
						bCust =false;
					//bZoneCode = SysDataManager2.VerifyZoneCode(utility.GetAppID(), utility.GetEnterpriseID(),lblESA_Sector.Text.ToString());

					dsZone = SysDataMgrDAL.Get_CheckESA_ZoneCode(utility.GetAppID(), utility.GetEnterpriseID(),lblESA_Sector.Text.ToString());

					if(dsZone.Tables[0].Rows.Count <= 0)
					{
						bZoneCode = false;
						stbErrors.Append(" - ESA sector must have in system. \n");
					}
					
					//bServiceCode = SysDataManager2.VerifyServiceCode(utility.GetAppID(), utility.GetEnterpriseID(),lblServiceCode.Text.ToString());
					 //by Aoo
					if(lblESA_Sector == null || lblESA_Sector.Text.ToString() == "")
					{
						bESAbyService = false;
						stbErrors.Append(" - ESA sector must not null. \n");
					}

					if(lblServiceCode != null)
					{
						if(lblServiceCode.Text.Equals(""))
						{
							bServiceCode = false;
							stbErrors.Append("- Service type must not null! \n");
						}
						if(!SysDataManager2.ckZoneOrServiceInSystem(utility.GetEnterpriseID(),utility.GetAppID(),lblServiceCode.Text,"ServiceType"))
						{
							bServiceCode = false;
							stbErrors.Append("- Service type does not exist! \n");
						}
					}

					if(lblesa_surcharge_amt.Text.Length<1 && lblesa_surcharge_percent.Text.Length<1)
					{
						bESA_Surcharge = false;
						stbErrors.Append(" - ESA surcharge amount and ESA surcharge percent must not null. \n");
					}
					else if(Decimal.Parse(lblesa_surcharge_amt.Text)>0 && Decimal.Parse(lblesa_surcharge_percent.Text)>0)
					{
						bESA_Surcharge = false;
						stbErrors.Append(" - ESA surcharge amount or ESA surcharge percent should have value. \n");
					}

					if(Decimal.Parse(lblesa_surcharge_percent.Text)>100 || Decimal.Parse(lblesa_surcharge_percent.Text)<0)
					{
						bESA_Surcharge = false;
						stbErrors.Append(" - ESA surcharge percent must less than 100 and must not less than 0. \n");
					}

					if(lblmin_esa_amount.Text.Length<1 && lblmax_esa_amount.Text.Length<1)
						bESA_Surcharge_MaxMin = false;
					else if(lblmin_esa_amount.Text.Length>0 && lblmax_esa_amount.Text.Length>0)
						if(Decimal.Parse(lblmin_esa_amount.Text.ToString()) > Decimal.Parse(lblmax_esa_amount.Text.ToString()))
							bESA_Surcharge_MaxMin = false;

					if(lblEffectivedate.Text.Length<1)
					{
						bEffective=false;
						stbErrors.Append(" - Effective date must not null. \n");
					}
					else
					{
						DateTime dt = DateTime.ParseExact(lblEffectivedate.Text.ToString(),"dd/MM/yyyy",null);
						DateTime dtCompare = DateTime.Now.AddYears(1);	
						dtCompare = dtCompare.AddDays(-1);
						if(dt.Date>dtCompare.Date||dt.Date<DateTime.Now.Date)
						{
							bEffective=false;
							stbErrors.Append(" - Effective date must between "+ DateTime.Now.ToString("dd/MM/yyyy",null) +" and "+ dtCompare.ToString("dd/MM/yyyy",null) +". \n");
						}
					}
					DataSet dsESAService = new DataSet();
					dsESAService = SysDataManager2.GetESAServiceTypeDS(utility.GetAppID(),utility.GetEnterpriseID(),lblCustID.Text,lblESA_Sector.Text,lblServiceCode.Text,lblEffectivedate.Text);
					if(dsESAService.Tables[0].Rows.Count > 0)
					{
						ckDupEsa = false;
						stbErrors.Append("- ESA service type is duplicated! \n");
					}
					if(bCust && bZoneCode && bServiceCode && bESA_Surcharge && bESA_Surcharge_MaxMin && bEffective && bESAbyService && ckDupEsa)
					{
						DataSet dsESA_Service = SysDataManager2.GetESAServiceTypeDS(utility.GetAppID(),utility.GetEnterpriseID(),lblCustID.Text.ToString(),
							lblESA_Sector.Text.ToString(),lblServiceCode.Text.ToString(),lblEffectivedate.Text.ToString());
				
						if(dsESA_Service.Tables[0].Rows.Count<=0)
							imgSelect.ImageUrl  = "images/butt-select.gif";
						else
						{
							imgSelect.ImageUrl  = "images/butt-red.gif";
							stbErrors.Append(" - ESA service type must have in system. \n");
							imgSelect.ToolTip = stbErrors.ToString();
				
							this.btnImport_Save.Enabled=false;
						}
					}
					else
					{
						imgSelect.ImageUrl  = "images/butt-red.gif";
						imgSelect.ToolTip = stbErrors.ToString();
						this.btnImport_Save.Enabled=false;
					}
				}
				catch(Exception ex)
				{
					bEffective=false;
				}
			
			}

		}
		public void dgImportESA_Service_Button(object sender, DataGridCommandEventArgs e)
		{
			String strCmdNm = e.CommandName;
			String strtxtZoneCode = null;
			String strZoneCode = null;
			if(strCmdNm.Equals("zoneSearch"))
			{
				msTextBox txtESA_Sector = (msTextBox)e.Item.FindControl("txtESA_Sector");

				if(txtESA_Sector != null)
				{
					txtESA_Sector.Attributes.Add("onpaste","return false;");
					txtESA_Sector.Attributes.Add("onkeypress","return false;");
					txtESA_Sector.Attributes.Add("onkeydown","return false;");

					strtxtZoneCode = txtESA_Sector.ClientID;
					strZoneCode = txtESA_Sector.Text;

					String sUrl = "ZonePopup.aspx?FORMID=ImportESA_ServiceType&ZONECODE_TEXT="+strZoneCode+"&ZONECODE="+strtxtZoneCode;
					ArrayList paramList = new ArrayList();
					paramList.Add(sUrl);
					String sScript = Utility.GetScript("openWindowParam.js",paramList);
					Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
				}
		
				
			}
			else if(strCmdNm.Equals("serviceSearch")) 
			{
				msTextBox txtServiceCode = (msTextBox)e.Item.FindControl("txtServiceCode");

				if(txtServiceCode != null)
				{
					txtServiceCode.Attributes.Add("onpaste","return false;");
					txtServiceCode.Attributes.Add("onkeypress","return false;");
					txtServiceCode.Attributes.Add("onkeydown","return false;");

					String sUrl = "ServiceCodePopup.aspx?FORMID=ImportESA_ServiceType&CODEID=" + txtServiceCode.ClientID;
					ArrayList paramList = new ArrayList();
					paramList.Add(sUrl);

					String sScript = Utility.GetScript("openWindowParam.js",paramList);
					Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
				}
			}
		}
		protected void dgImportESA_Service_Edit(object sender, DataGridCommandEventArgs e)
		{
			DataSet dsImportESA_Service = (DataSet)Session["dsImportESA_Service"];
			dgImportESA_Service.CurrentPageIndex = 0;
			dgImportESA_Service.EditItemIndex = e.Item.ItemIndex;
			dgImportESA_Service.DataSource = dsImportESA_Service;
			dgImportESA_Service.DataBind();
			dgImportESA_Service.Columns[0].Visible = false;
			return;		
		}
		protected void dgImportESA_Service_Cancel(object sender, DataGridCommandEventArgs e)
		{
			DataSet dsImportESA_Service = (DataSet)Session["dsImportESA_Service"];
			dgImportESA_Service.EditItemIndex = -1;
			lblErrorMessage.Text = "";
			dgImportESA_Service.CurrentPageIndex = 0;
			dgImportESA_Service.DataSource = dsImportESA_Service;
			dgImportESA_Service.DataBind();	

			foreach(DataGridItem dgItem in dgImportESA_Service.Items)
			{
				DataSet dsESAService = new DataSet();

				ImageButton imgSelect = (ImageButton)dgItem.Cells[0].Controls[1];
				Label lblCustID = (Label)dgItem.FindControl("lblCustID");
				Label lblESA_Sector = (Label)dgItem.FindControl("lblESA_Sector");
				Label lblServiceCode = (Label)dgItem.FindControl("lblServiceCode");
				Label lblesa_surcharge_amt = (Label)dgItem.FindControl("lblesa_surcharge_amt");
				Label lblesa_surcharge_percent = (Label)dgItem.FindControl("lblesa_surcharge_percent");
				Label lblmin_esa_amount = (Label)dgItem.FindControl("lblmin_esa_amount");
				Label lblmax_esa_amount = (Label)dgItem.FindControl("lblmax_esa_amount");
				Label lblEffectivedate = (Label)dgItem.FindControl("lblEffectivedate");

				StringBuilder stbErrors = new StringBuilder();
				string ckCustID = "1";
				string ckESA_Sector = "1";
				string ckServiceCode = "1";
				string ckEsaSurAmt = "1";
				string ckEsaSurPer = "1";
				string ckMinEsa = "1";
				string ckDupEsa = "1";
				string ckEffDate = "1";

				if(lblCustID != null)
				{
					if(lblCustID.Text.Equals(""))
					{
						ckCustID = "0";
						stbErrors.Append("- Customer ID must not null! \n");
					}
					if(!SysDataManager2.ckCustID(utility.GetEnterpriseID(),utility.GetAppID(),lblCustID.Text))
					{
						ckCustID = "0";
					}
				}
				if(lblESA_Sector != null)
				{
					if(lblESA_Sector.Text.Equals(""))
					{
						ckESA_Sector = "0";
						stbErrors.Append("- ESA Sector must not null! \n");
					}
					if(!SysDataManager2.ckZoneOrServiceInSystem(utility.GetEnterpriseID(),utility.GetAppID(),lblESA_Sector.Text,"ZoneESA"))
					{
						ckESA_Sector = "0";
						stbErrors.Append("- ESA Sector does not exist! \n");
					}
				}
				if(lblServiceCode != null)
				{
					if(lblServiceCode.Text.Equals(""))
					{
						ckServiceCode = "0";
						stbErrors.Append("- Service type must not null! \n");
					}
					if(!SysDataManager2.ckZoneOrServiceInSystem(utility.GetEnterpriseID(),utility.GetAppID(),lblESA_Sector.Text,"ServiceType"))
					{
						ckESA_Sector = "0";
						stbErrors.Append("- Service type does not exist! \n");
					}
				}

				if(lblesa_surcharge_amt != null && lblesa_surcharge_percent != null)
				{
					if(Convert.ToDouble(lblesa_surcharge_amt.Text.Trim()) > 0.0 && Convert.ToDouble(lblesa_surcharge_percent.Text.Trim()) > 0.0)
					{
						ckEsaSurAmt = "0";
						ckEsaSurPer = "0";
						stbErrors.Append("- Please enter either ESA surcharge amount or ESA surcharge percent. \n");
					}
					else if(Convert.ToDouble(lblesa_surcharge_amt.Text.Trim()) < 0.0 && Convert.ToDouble(lblesa_surcharge_percent.Text.Trim()) < 0.0)
					{
						ckEsaSurAmt = "0";
						ckEsaSurPer = "0";
						stbErrors.Append("- ESA surcharge amount and ESA surchyarge percent must not nagative value! \n");
					}
					else if(Convert.ToDouble(lblesa_surcharge_amt.Text.Trim()) == 0.0 && Convert.ToDouble(lblesa_surcharge_percent.Text.Trim()) > 0.0)
					{
						if(Convert.ToDouble(lblesa_surcharge_percent.Text.Trim()) > 100)
						{
							ckEsaSurPer = "0";
							stbErrors.Append("- ESA surcharge percent must less than 100! \n");
						}
					}
				}
				if(lblmin_esa_amount != null && lblmax_esa_amount != null)
				{
					if(Convert.ToDouble(lblmin_esa_amount.Text.Trim()) >= 0 && Convert.ToDouble(lblmax_esa_amount.Text.Trim()) >= 0 && Convert.ToDouble(lblmin_esa_amount.Text.Trim()) > Convert.ToDouble(lblmax_esa_amount.Text.Trim()))
					{
						ckMinEsa = "0";
						stbErrors.Append("- Min value must not more than max value! \n");
					}
				}

				object objEffectiveDate = System.DBNull.Value;

				if(lblEffectivedate != null)
				{
					if(lblEffectivedate.Text.Equals(""))
					{
						ckEffDate = "0";
						stbErrors.Append("- Effective date must not null! \n");
					}
					DateTime dtStart = System.DateTime.Now;
					DateTime dtEnd = System.DateTime.Now.AddYears(1);
					dtEnd = dtEnd.AddDays(-1);
					objEffectiveDate = DateTime.ParseExact(lblEffectivedate.Text.ToString().Trim(),"dd/MM/yyyy",null);
					if(Convert.ToDateTime(objEffectiveDate).Date < dtStart.Date || Convert.ToDateTime(objEffectiveDate).Date > dtEnd.Date)
					{
						ckEffDate = "0";
						stbErrors.Append("- Effective date must between "+dtStart.ToString("dd/MM/yyyy")+" and "+dtEnd.ToString("dd/MM/yyyy")+". \n");
					}
				}

				////				dsESAService = SysDataManager2.GetESAServiceTypeDS(utility.GetAppID(),utility.GetEnterpriseID(),lblCustID.Text,txtESA_Sector.Text,lblServiceCode.Text,lblEffectivedate.Text);
				////				if(dsESAService.Tables[0].Rows.Count > 0)
				////				{
				////					ckDupEsa = "0";
				////					stbErrors.Append("- ESA service type is duplicated! \n");
				////				}
				//
				if(ckCustID.Equals("0")||ckESA_Sector.Equals("0")||ckServiceCode.Equals("0")||ckEsaSurAmt.Equals("0")||ckEsaSurPer.Equals("0")||ckMinEsa.Equals("0")||ckDupEsa.Equals("0")||ckEffDate.Equals("0"))
				{
					imgSelect.ImageUrl  = "images/butt-red.gif";
					imgSelect.ToolTip = stbErrors.ToString();
					this.btnImport_Save.Enabled=false;
					//return;
				}
				else
				{
					imgSelect.ImageUrl  = "images/butt-select.gif";
					this.btnImport_Save.Enabled=true;
				}
			}
			dgImportESA_Service.Columns[0].Visible = true;  
			dgImportESA_Service.Columns[1].Visible = true;
			dgImportESA_Service.Columns[2].Visible = true;
		}

		protected void dgImportESA_Service_PageChange(object sender, DataGridPageChangedEventArgs e)
		{
			this.dgImportESA_Service.CurrentPageIndex = e.NewPageIndex;
			dgImportESA_Service.EditItemIndex = -1;
			if(ViewState["Mode"].ToString().Equals("Import"))
			{
				dgImportESA_Service.DataSource = (DataSet)Session["dsImportESA_Service"];
			}
			else if(ViewState["Mode"].ToString().Equals("Query"))
			{
				dgImportESA_Service.DataSource = (DataSet)Session["ESA_Query"];
			}
			dgImportESA_Service.DataBind();


			foreach(DataGridItem dgItem in dgImportESA_Service.Items)
			{
				DataSet dsESAService = new DataSet();

				ImageButton imgSelect = (ImageButton)dgItem.Cells[0].Controls[1];
				Label lblCustID = (Label)dgItem.FindControl("lblCustID");
				Label lblESA_Sector = (Label)dgItem.FindControl("lblESA_Sector");
				Label lblServiceCode = (Label)dgItem.FindControl("lblServiceCode");
				Label lblesa_surcharge_amt = (Label)dgItem.FindControl("lblesa_surcharge_amt");
				Label lblesa_surcharge_percent = (Label)dgItem.FindControl("lblesa_surcharge_percent");
				Label lblmin_esa_amount = (Label)dgItem.FindControl("lblmin_esa_amount");
				Label lblmax_esa_amount = (Label)dgItem.FindControl("lblmax_esa_amount");
				Label lblEffectivedate = (Label)dgItem.FindControl("lblEffectivedate");

				StringBuilder stbErrors = new StringBuilder();
				string ckCustID = "1";
				string ckESA_Sector = "1";
				string ckServiceCode = "1";
				string ckEsaSurAmt = "1";
				string ckEsaSurPer = "1";
				string ckMinEsa = "1";
				string ckDupEsa = "1";
				string ckEffDate = "1";

				if(lblCustID != null)
				{
					if(lblCustID.Text.Equals(""))
					{
						ckCustID = "0";
						stbErrors.Append("- Customer ID must not null! \n");
					}
					if(!SysDataManager2.ckCustID(utility.GetEnterpriseID(),utility.GetAppID(),lblCustID.Text))
					{
						ckCustID = "0";
						stbErrors.Append("- Customer does not exist! \n");
					}
				}
				if(lblESA_Sector != null)
				{
					if(lblESA_Sector.Text.Equals(""))
					{
						ckESA_Sector = "0";
						stbErrors.Append("- ESA Sector must not null! \n");
					}
					if(!SysDataManager2.ckZoneOrServiceInSystem(utility.GetEnterpriseID(),utility.GetAppID(),lblESA_Sector.Text,"ZoneESA"))
					{
						ckESA_Sector = "0";
						stbErrors.Append("- ESA Sector does not exist! \n");
					}
				}
				if(lblServiceCode != null)
				{
					if(lblServiceCode.Text.Equals(""))
					{
						ckServiceCode = "0";
						stbErrors.Append("- Service type must not null! \n");
					}
					if(!SysDataManager2.ckZoneOrServiceInSystem(utility.GetEnterpriseID(),utility.GetAppID(),lblServiceCode.Text,"ServiceType"))
					{
						ckESA_Sector = "0";
						stbErrors.Append("- Service type does not exist! \n");
					}
				}

				if(lblesa_surcharge_amt != null && lblesa_surcharge_percent != null)
				{
					if(lblesa_surcharge_amt.Text.Trim().Equals(""))
						lblesa_surcharge_amt.Text = "0.0";
					if(lblesa_surcharge_percent.Text.Trim().Equals(""))
						lblesa_surcharge_percent.Text = "0.0";
					if(Convert.ToDouble(lblesa_surcharge_amt.Text.Trim()) > 0.0 && Convert.ToDouble(lblesa_surcharge_percent.Text.Trim()) > 0.0)
					{
						ckEsaSurAmt = "0";
						ckEsaSurPer = "0";
						stbErrors.Append("- Please enter either ESA surcharge amount or ESA surcharge percent. \n");
					}
					else if(Convert.ToDouble(lblesa_surcharge_amt.Text.Trim()) < 0.0 && Convert.ToDouble(lblesa_surcharge_percent.Text.Trim()) < 0.0)
					{
						ckEsaSurAmt = "0";
						ckEsaSurPer = "0";
						stbErrors.Append("- ESA surcharge amount and ESA surchyarge percent must not nagative value! \n");
					}
					else if(Convert.ToDouble(lblesa_surcharge_amt.Text.Trim()) == 0.0 && Convert.ToDouble(lblesa_surcharge_percent.Text.Trim()) > 0.0)
					{
						if(Convert.ToDouble(lblesa_surcharge_percent.Text.Trim()) > 100)
						{
							ckEsaSurPer = "0";
							stbErrors.Append("- ESA surcharge percent must less than 100! \n");
						}
					}
				}
				if(lblmin_esa_amount != null && lblmax_esa_amount != null)
				{
					if(lblmin_esa_amount.Text.Trim().Equals(""))
						lblmin_esa_amount.Text = "0.00";
					if(lblmax_esa_amount.Text.Trim().Equals(""))
						lblmax_esa_amount.Text = "0.00";
					if(Convert.ToDouble(lblmin_esa_amount.Text) >= 0 && Convert.ToDouble(lblmax_esa_amount.Text) >= 0 && Convert.ToDouble(lblmin_esa_amount.Text) > Convert.ToDouble(lblmax_esa_amount.Text))
					{
						ckMinEsa = "0";
						stbErrors.Append("- Min value must not more than max value! \n");
					}
				}

				object objEffectiveDate = System.DBNull.Value;

				if(lblEffectivedate != null)
				{
					if(lblEffectivedate.Text.Equals(""))
					{
						ckEffDate = "0";
						stbErrors.Append("- Effective date must not null! \n");
					}
					DateTime dtStart = System.DateTime.Now;
					DateTime dtEnd = System.DateTime.Now.AddYears(1);
					dtEnd = dtEnd.AddDays(-1);
					objEffectiveDate = DateTime.ParseExact(lblEffectivedate.Text.ToString().Trim(),"dd/MM/yyyy",null);
					if(Convert.ToDateTime(objEffectiveDate).Date < dtStart.Date || Convert.ToDateTime(objEffectiveDate).Date > dtEnd.Date)
					{
						ckEffDate = "0";
						stbErrors.Append("- Effective date must between "+dtStart.ToString("dd/MM/yyyy")+" and "+dtEnd.ToString("dd/MM/yyyy")+". \n");
					}

					dsESAService = SysDataManager2.GetESAServiceTypeDS(utility.GetAppID(),utility.GetEnterpriseID(),lblCustID.Text,lblESA_Sector.Text,lblServiceCode.Text,Convert.ToDateTime(objEffectiveDate).Date.ToString());
					if(dsESAService.Tables[0].Rows.Count > 0)
					{
						ckDupEsa = "0";
						stbErrors.Append("- ESA service type is duplicated! \n");
					}
				}

				
				
				if(ckCustID.Equals("0")||ckESA_Sector.Equals("0")||ckServiceCode.Equals("0")||ckEsaSurAmt.Equals("0")||ckEsaSurPer.Equals("0")||ckMinEsa.Equals("0")||ckDupEsa.Equals("0")||ckEffDate.Equals("0")||ckDupEsa.Equals("0"))
				{
					imgSelect.ImageUrl  = "images/butt-red.gif";
					imgSelect.ToolTip = stbErrors.ToString();
					this.btnImport_Save.Enabled=false;
					//return;
				}
				else
				{
					imgSelect.ImageUrl  = "images/butt-select.gif";
					this.btnImport_Save.Enabled=true;
				}
			}
			if(ViewState["Mode"].ToString().Equals("Import"))
			{
				dgImportESA_Service.Columns[0].Visible = true;  
				dgImportESA_Service.Columns[1].Visible = true;
				dgImportESA_Service.Columns[2].Visible = true;
			}
			else if(ViewState["Mode"].ToString().Equals("Query"))
			{
				dgImportESA_Service.Columns[0].Visible = false;  
				dgImportESA_Service.Columns[1].Visible = false;
				dgImportESA_Service.Columns[2].Visible = false;
			}
		}
		protected void dgImportESA_Service_Update(object sender, DataGridCommandEventArgs e)
		{
			DataSet dsImportESA_Service = (DataSet)Session["dsImportESA_Service"];

			msTextBox txtCustID = (msTextBox)e.Item.FindControl("txtCustID");
			msTextBox txtESA_Sector = (msTextBox)e.Item.FindControl("txtESA_Sector");
			msTextBox txtServiceCode = (msTextBox)e.Item.FindControl("txtServiceCode");
			msTextBox txtesa_surcharge_amt = (msTextBox)e.Item.FindControl("txtesa_surcharge_amt");
			msTextBox txtesa_surcharge_percent = (msTextBox)e.Item.FindControl("txtesa_surcharge_percent");
			msTextBox txtmin_esa_amount = (msTextBox)e.Item.FindControl("txtmin_esa_amount");
			msTextBox txtmax_esa_amount = (msTextBox)e.Item.FindControl("txtmax_esa_amount");
			msTextBox txtEffectiveDate = (msTextBox)e.Item.FindControl("txtEffectiveDate");
		
			try
			{
				DataRow dr = dsImportESA_Service.Tables[0].Rows[e.Item.ItemIndex];
				dr[0] = txtCustID.Text;
				dr[1] = txtESA_Sector.Text;
				dr[2] = txtServiceCode.Text;
				dr[3] = Convert.ToDouble(txtesa_surcharge_amt.Text);
				dr[4] = Convert.ToDouble(txtesa_surcharge_percent.Text);
				dr[5] = Convert.ToDouble(txtmin_esa_amount.Text);
				dr[6] = Convert.ToDouble(txtmax_esa_amount.Text);
				dr[7] = DateTime.ParseExact(txtEffectiveDate.Text,"dd/MM/yyyy",null);

				this.dgImportESA_Service.EditItemIndex = -1;
				this.dgImportESA_Service.DataSource = dsImportESA_Service;
				this.dgImportESA_Service.DataBind();
			}
			catch (Exception ex)
			{
				lblErrorMessage.Text=ex.Message;
			}
//			this.dgImportESA_Service.EditItemIndex = -1;
//			this.dgImportESA_Service.DataSource = dsImportESA_Service;
//			this.dgImportESA_Service.DataBind();

			foreach(DataGridItem dgItem in dgImportESA_Service.Items)
			{
				DataSet dsESAService = new DataSet();

				ImageButton imgSelect = (ImageButton)dgItem.Cells[0].Controls[1];
				Label lblCustID = (Label)dgItem.FindControl("lblCustID");
				Label lblESA_Sector = (Label)dgItem.FindControl("lblESA_Sector");
				Label lblServiceCode = (Label)dgItem.FindControl("lblServiceCode");
				Label lblesa_surcharge_amt = (Label)dgItem.FindControl("lblesa_surcharge_amt");
				Label lblesa_surcharge_percent = (Label)dgItem.FindControl("lblesa_surcharge_percent");
				Label lblmin_esa_amount = (Label)dgItem.FindControl("lblmin_esa_amount");
				Label lblmax_esa_amount = (Label)dgItem.FindControl("lblmax_esa_amount");
				Label lblEffectivedate = (Label)dgItem.FindControl("lblEffectivedate");

				StringBuilder stbErrors = new StringBuilder();
				string ckCustID = "1";
				string ckESA_Sector = "1";
				string ckServiceCode = "1";
				string ckEsaSurAmt = "1";
				string ckEsaSurPer = "1";
				string ckMinEsa = "1";
				string ckDupEsa = "1";
				string ckEffDate = "1";

				if(lblCustID != null)
				{
					if(lblCustID.Text.Equals(""))
					{
						ckCustID = "0";
						stbErrors.Append("- Customer ID must not null! \n");
					}
					if(!SysDataManager2.ckCustID(utility.GetEnterpriseID(),utility.GetAppID(),lblCustID.Text))
					{
						ckCustID = "0";
						stbErrors.Append("- Customer does not exist! \n");
					}
				}
				if(lblESA_Sector != null)
				{
					if(lblESA_Sector.Text.Equals(""))
					{
						ckESA_Sector = "0";
						stbErrors.Append("- ESA Sector must not null! \n");
					}
					if(!SysDataManager2.ckZoneOrServiceInSystem(utility.GetEnterpriseID(),utility.GetAppID(),lblESA_Sector.Text,"ZoneESA"))
					{
						ckESA_Sector = "0";
						stbErrors.Append("- ESA Sector does not exist! \n");
					}
				}
				if(lblServiceCode != null)
				{
					if(lblServiceCode.Text.Equals(""))
					{
						ckServiceCode = "0";
						stbErrors.Append("- Service type must not null! \n");
					}
					if(!SysDataManager2.ckZoneOrServiceInSystem(utility.GetEnterpriseID(),utility.GetAppID(),lblServiceCode.Text,"ServiceType"))
					{
						ckESA_Sector = "0";
						stbErrors.Append("- Service type does not exist! \n");
					}
				}

				if(lblesa_surcharge_amt != null && lblesa_surcharge_percent != null)
				{
					if(lblesa_surcharge_amt.Text.Trim().Equals(""))
						lblesa_surcharge_amt.Text = "0.0";
					if(lblesa_surcharge_percent.Text.Trim().Equals(""))
						lblesa_surcharge_percent.Text = "0.0";
					if(Convert.ToDouble(lblesa_surcharge_amt.Text.Trim()) > 0.0 && Convert.ToDouble(lblesa_surcharge_percent.Text.Trim()) > 0.0)
					{
						ckEsaSurAmt = "0";
						ckEsaSurPer = "0";
						stbErrors.Append("- Please enter either ESA surcharge amount or ESA surcharge percent. \n");
					}
					else if(Convert.ToDouble(lblesa_surcharge_amt.Text.Trim()) < 0.0 && Convert.ToDouble(lblesa_surcharge_percent.Text.Trim()) < 0.0)
					{
						ckEsaSurAmt = "0";
						ckEsaSurPer = "0";
						stbErrors.Append("- ESA surcharge amount and ESA surchyarge percent must not nagative value! \n");
					}
					else if(Convert.ToDouble(lblesa_surcharge_amt.Text.Trim()) == 0.0 && Convert.ToDouble(lblesa_surcharge_percent.Text.Trim()) > 0.0)
					{
						if(Convert.ToDouble(lblesa_surcharge_percent.Text.Trim()) > 100)
						{
							ckEsaSurPer = "0";
							stbErrors.Append("- ESA surcharge percent must less than 100! \n");
						}
					}
				}
				if(lblmin_esa_amount != null && lblmax_esa_amount != null)
				{
					if(txtmin_esa_amount.Text.Trim().Equals(""))
						txtmin_esa_amount.Text = "0.00";
					if(txtmax_esa_amount.Text.Trim().Equals(""))
						txtmax_esa_amount.Text = "0.00";
					if(Convert.ToDouble(txtmin_esa_amount.Text) >= 0 && Convert.ToDouble(txtmax_esa_amount.Text) >= 0 && Convert.ToDouble(txtmin_esa_amount.Text) > Convert.ToDouble(txtmax_esa_amount.Text))
					{
						ckMinEsa = "0";
						stbErrors.Append("- Min value must not more than max value! \n");
					}
				}

				object objEffectiveDate = System.DBNull.Value;

				if(lblEffectivedate != null)
				{
					if(lblEffectivedate.Text.Equals(""))
					{
						ckEffDate = "0";
						stbErrors.Append("- Effective date must not null! \n");
					}
					DateTime dtStart = System.DateTime.Now;
					DateTime dtEnd = System.DateTime.Now.AddYears(1);
					dtEnd = dtEnd.AddDays(-1);
					objEffectiveDate = DateTime.ParseExact(lblEffectivedate.Text.ToString().Trim(),"dd/MM/yyyy",null);
					if(Convert.ToDateTime(objEffectiveDate).Date < dtStart.Date || Convert.ToDateTime(objEffectiveDate).Date > dtEnd.Date)
					{
						ckEffDate = "0";
						stbErrors.Append("- Effective date must between "+dtStart.ToString("dd/MM/yyyy")+" and "+dtEnd.ToString("dd/MM/yyyy")+". \n");
					}

					dsESAService = SysDataManager2.GetESAServiceTypeDS(utility.GetAppID(),utility.GetEnterpriseID(),lblCustID.Text,lblESA_Sector.Text,lblServiceCode.Text,Convert.ToDateTime(objEffectiveDate).Date.ToString());
					if(dsESAService.Tables[0].Rows.Count > 0)
					{
						ckDupEsa = "0";
						stbErrors.Append("- ESA service type is duplicated! \n");
					}
				}

				
				
				if(ckCustID.Equals("0")||ckESA_Sector.Equals("0")||ckServiceCode.Equals("0")||ckEsaSurAmt.Equals("0")||ckEsaSurPer.Equals("0")||ckMinEsa.Equals("0")||ckDupEsa.Equals("0")||ckEffDate.Equals("0")||ckDupEsa.Equals("0"))
				{
					imgSelect.ImageUrl  = "images/butt-red.gif";
					imgSelect.ToolTip = stbErrors.ToString();
					this.btnImport_Save.Enabled=false;
					//return;
				}
				else
				{
					imgSelect.ImageUrl  = "images/butt-select.gif";
					this.btnImport_Save.Enabled=true;
				}
			}
			dgImportESA_Service.Columns[0].Visible = true;  
			dgImportESA_Service.Columns[1].Visible = true;
			dgImportESA_Service.Columns[2].Visible = true;
			
		}

		protected void dgImportESA_Service_Delete(object sender, DataGridCommandEventArgs e)
		{
			try
			{

				DataSet dsImportESA = (DataSet)Session["dsImportESA_Service"];
				int rowIndex = e.Item.ItemIndex;
				int pageIndex = this.dgImportESA_Service.CurrentPageIndex;
				int dataAtRow = rowIndex*(pageIndex+1);

				dsImportESA.Tables[0].Rows.RemoveAt(dataAtRow);
				dsImportESA.AcceptChanges();

				this.dgImportESA_Service.DataSource = dsImportESA;
				this.dgImportESA_Service.EditItemIndex=-1;
				this.dgImportESA_Service.DataBind();

				if(dsImportESA.Tables[0].Rows.Count>0)
				{
					this.btnImport_Save.Enabled = true;
					dgImportESA_Service_Bound();
					dgImportESA_Service.Columns[0].Visible = true;
					dgImportESA_Service.Columns[2].Visible = true; 
				}
				else
				{   this.btnImport_Save.Enabled=false;
					dgImportESA_Service.Columns[0].Visible = false;
					dgImportESA_Service.Columns[0].Visible = false;
				}

				Session["dsImportESA_Service"] = dsImportESA;

			}
			catch(ApplicationException appException)
			{
				lblErrorMessage.Text = "Cannot Delete!";
				return;
			}
		}

		private void btnQuery_Click(object sender, System.EventArgs e)
		{
			QueryMode();
		}

		private void QueryMode()
		{
			ViewState["Mode"] = "Query";
			this.lblErrorMessage.Text="";
			this.dgImportESA_Service.EditItemIndex = 0;
			this.dgImportESA_Service.DataSource = SysDataManager2.GetEmptyESAServiceTypeDS().ds;
			this.dgImportESA_Service.DataBind();
			dgImportESA_Service.Columns[0].Visible = false;  
			dgImportESA_Service.Columns[1].Visible = false;  
			dgImportESA_Service.Columns[2].Visible = false;
			Session["SaveAble"]=false;

			this.btnQuery.Enabled = true;
			this.btnExecuteQuery.Enabled = true;
			this.btnImport_Show.Enabled = true;
			this.btnImport.Enabled = false;
			this.btnImport_Save.Enabled = false;
		}


		private void ExecuteQuery()
		{
			DataTable dtQuery = new DataTable();
			dtQuery.Columns.Add(new DataColumn("CustID", typeof(string)));
			dtQuery.Columns.Add(new DataColumn("ESA_Sector", typeof(string)));
			dtQuery.Columns.Add(new DataColumn("ServiceCode", typeof(string)));
			dtQuery.Columns.Add(new DataColumn("EffectiveDate", typeof(DateTime)));

			DataSet dsQueryESA = new DataSet();
			DataRow drQuery = dtQuery.NewRow();

			foreach(DataGridItem dgItem in dgImportESA_Service.Items)
			{
				msTextBox txtCustID = (msTextBox)dgItem.FindControl("txtCustID");
				msTextBox txtESA_Sector = (msTextBox)dgItem.FindControl("txtESA_Sector");
				msTextBox txtServiceCode = (msTextBox)dgItem.FindControl("txtServiceCode");
				msTextBox txtEffectiveDate = (msTextBox)dgItem.FindControl("txtEffectiveDate");

				
				if(!txtCustID.Text.Equals(""))
				{
					drQuery["CustID"] = txtCustID.Text;
				}
				if(!txtESA_Sector.Text.Equals(""))
				{
					drQuery["ESA_Sector"] = txtESA_Sector.Text;
				}
				if(!txtServiceCode.Text.Equals(""))
				{
					drQuery["ServiceCode"] = txtServiceCode.Text;;
				}
				if(!txtEffectiveDate.Text.Equals(""))
				{
					object objEffectivedate = DateTime.ParseExact(txtEffectiveDate.Text.Trim(),"dd/MM/yyyy",null);
					DateTime dt = Convert.ToDateTime(objEffectivedate);
					drQuery["EffectiveDate"] = dt;
				}

				dtQuery.Rows.Add(drQuery);
			}

			foreach(DataRow dr in dtQuery.Rows)
			{
				dsQueryESA = SysDataManager2.GetESAServiceTypeDS(utility.GetAppID(),utility.GetEnterpriseID(),dr["CustID"].ToString(),
				dr["ESA_Sector"].ToString(),dr["ServiceCode"].ToString(),dr["EffectiveDate"].ToString());
			}
			if(dsQueryESA.Tables[0].Rows.Count > 0)
			{
				this.dgImportESA_Service.EditItemIndex = -1;
				this.dgImportESA_Service.DataSource = dsQueryESA;
				Session["ESA_Query"] = dsQueryESA;
				this.dgImportESA_Service.DataBind();
				this.btnExecuteQuery.Enabled = false;
			}
			else
			{
				this.lblErrorMessage.Text = "No records found.";
			}

			
		}

		private void txtFilePath_TextChanged(object sender, System.EventArgs e)
		{
			this.btnImport.Enabled = true;
		}

		private void btnExecuteQuery_Click(object sender, System.EventArgs e)
		{
			ExecuteQuery();
		}

	}

}
