<%@ Page language="c#" Codebehind="TripReport.aspx.cs" AutoEventWireup="false" Inherits="com.ties.TripReport" %>
<%@ Register TagPrefix="DbCombo" Namespace="Cambro.Web.DbCombo" Assembly="Cambro.Web.DbCombo" %>
<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<%@ Import Namespace="Cambro.Web.DbCombo" %>
<%@ Import Namespace="com.common.util" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>TripReport</title>
		<LINK href="css/Styles.css" type="text/css" rel="stylesheet" name="stylesheet">
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<!--#INCLUDE FILE="msFormValidations.inc"-->
		<script language="javascript" src="Scripts/settingScrollPosition.js"></script>
	</HEAD>
	<body onclick="GetScrollPosition();" onload="SetScrollPosition();" onunload="window.top.displayBanner.fnCloseAll(0);"
		MS_POSITIONING="GridLayout">
		<form id="TripReport" method="post" runat="server">
			<asp:button id="btnQuery" style="Z-INDEX: 100; LEFT: 20px; POSITION: absolute; TOP: 40px" runat="server"
				Width="64px" CssClass="queryButton" Text="Query"></asp:button><asp:label id="lblErrorMessage" style="Z-INDEX: 104; LEFT: 24px; POSITION: absolute; TOP: 69px"
				runat="server" Width="700px" CssClass="errorMsgColor" Height="17px">Place holder for err msg</asp:label>&nbsp;
			<asp:button id="btnExecuteQuery" style="Z-INDEX: 101; LEFT: 90px; POSITION: absolute; TOP: 40px"
				runat="server" Width="123px" CssClass="queryButton" Text="Execute Query"></asp:button><asp:label id="Label1" style="Z-INDEX: 102; LEFT: 20px; POSITION: absolute; TOP: 4px" runat="server"
				Width="365px" CssClass="maintitleSize" Height="27px">Trip Report</asp:label>
			<!-- =================================================================================== -->
			<TABLE id="tblInternal" style="Z-INDEX: 103; LEFT: 21px; WIDTH: 500px; POSITION: absolute; TOP: 90px; HEIGHT: 200px"
				width="800" border="0" runat="server">
				<!-- ========== Report Type ========== -->
				<TR>
					<TD style="WIDTH: 430px; HEIGHT: 50px" vAlign="top">
						<fieldset style="WIDTH: 100%; HEIGHT: 50px"><legend><asp:label id="Label4" runat="server" CssClass="tableHeadingFieldset" Font-Bold="True">Report Type</asp:label></legend>
							<TABLE id="tbReportType" style="WIDTH: 100%; HEIGHT: 100%" cellSpacing="0" cellPadding="0"
								align="left" border="0" runat="server">
								<TR style="HEIGHT: 25px">
									<TD>
										<asp:radiobutton id="rbReport" Width="300px" CssClass="tableRadioButton" Text="Trip Report" GroupName="TypeReport"
											Checked="True" Runat="server"></asp:radiobutton>
									</TD>
								</TR>
								<TR style="HEIGHT: 25px">
									<TD>
										<asp:radiobutton id="rbSummaryReport" Width="300px" CssClass="tableRadioButton" Text="Summary Trip Report"
											GroupName="TypeReport" Runat="server"></asp:radiobutton>
									</TD>
								</TR>
							</TABLE>
						</fieldset>
						<P>&nbsp;</P>
					</TD>
				</TR>
				<!-- ========== Date ========== -->
				<TR>
					<TD style="WIDTH: 430px; HEIGHT: 75px" vAlign="top">
						<fieldset style="WIDTH: 100%; HEIGHT: 75px"><legend><asp:label id="lblDate" runat="server" CssClass="tableHeadingFieldset" Font-Bold="True">Date</asp:label></legend>
							<TABLE id="tbDates" style="WIDTH: 100%; HEIGHT: 100%" cellSpacing="0" cellPadding="0" align="left"
								border="0" runat="server">
								<TR style="HEIGHT: 25px">
									<TD>
										<asp:radiobutton id="rbMonth" runat="server" Width="73px" CssClass="tableRadioButton" Text="Month"
											Height="21px" AutoPostBack="True" GroupName="EnterDate" Checked="True"></asp:radiobutton>
									</TD>
									<TD>
										<asp:dropdownlist id="ddMonth" runat="server" Width="88px" CssClass="textField" Height="19px"></asp:dropdownlist>
									</TD>
									<TD>
										<asp:label id="lblYear" runat="server" CssClass="tableLabel">Year</asp:label>
									</TD>
									<TD>
										<cc1:mstextbox id="txtYear" runat="server" Width="83" CssClass="textField" MaxLength="5" TextMaskType="msNumeric"
											NumberMaxValue="2999" NumberMinValue="1" NumberPrecision="4"></cc1:mstextbox>
									</TD>
								</TR>
								<TR style="HEIGHT: 25px">
									<TD>
										<asp:radiobutton id="rbPeriod" runat="server" Width="62px" CssClass="tableRadioButton" Text="Period"
											AutoPostBack="True" GroupName="EnterDate"></asp:radiobutton>
									</TD>
									<TD>
										<cc1:mstextbox id="txtPeriod" runat="server" Width="82" CssClass="textField" MaxLength="10" TextMaskType="msDate"
											TextMaskString="99/99/9999"></cc1:mstextbox>
									</TD>
									<TD colspan="2">
										<cc1:mstextbox id="txtTo" runat="server" Width="83" CssClass="textField" MaxLength="10" TextMaskType="msDate"
											TextMaskString="99/99/9999"></cc1:mstextbox>
									</TD>
								</TR>
								<TR style="HEIGHT: 25px">
									<TD>
										<asp:radiobutton id="rbDate" runat="server" Width="74px" CssClass="tableRadioButton" Text="Date"
											Height="22px" AutoPostBack="True" GroupName="EnterDate"></asp:radiobutton>
									</TD>
									<TD colspan="3">
										<cc1:mstextbox id="txtDate" runat="server" Width="82" CssClass="textField" MaxLength="10" TextMaskType="msDate"
											TextMaskString="99/99/9999"></cc1:mstextbox>
									</TD>
								</TR>
							</TABLE>
						</fieldset>
						<P>&nbsp;</P>
					</TD>
				</TR>
				<TR>
					<TD style="WIDTH: 430px; HEIGHT: 75px" vAlign="top">
						<!-- Route / DC Selection -->
						<fieldset style="WIDTH: 100%; HEIGHT: 75px"><legend><asp:label id="lblRouteType" runat="server" CssClass="tableHeadingFieldset" Font-Bold="True">Route / DC Selection</asp:label></legend>
							<TABLE id="tblRouteType" style="WIDTH: 100%; HEIGHT: 100%" cellSpacing="0" cellPadding="0"
								align="left" border="0" runat="server">
								<TR style="HEIGHT: 25px">
									<TD class="tableLabel" width="20%">&nbsp;<asp:label id="lblRouteCode" runat="server" Width="111px" CssClass="tableLabel" Height="22px">Route Code</asp:label></TD>
									<TD><DBCOMBO:DBCOMBO id="DbComboPathCode" tabIndex="14" Width="25px" CssClass="tableLabel" AutoPostBack="True"
											ServerMethod="DbComboPathCodeSelect" ShowDbComboLink="False" TextUpLevelSearchButton="v" RegistrationKey=" "
											TextBoxColumns="18" Runat="server"></DBCOMBO:DBCOMBO></TD>
								</TR>
								<TR style="HEIGHT: 25px">
									<TD class="tableLabel">&nbsp;<asp:label id="Label2" runat="server" Width="154px" CssClass="tableLabel" Height="22px">Origin Distribution Center</asp:label></TD>
									<TD><DBCOMBO:DBCOMBO id="DbComboOriginDC" tabIndex="14" Width="25px" CssClass="tableLabel" AutoPostBack="True"
											ServerMethod="DbComboDistributionCenterSelect" ShowDbComboLink="False" TextUpLevelSearchButton="v"
											RegistrationKey=" " TextBoxColumns="18" Runat="server"></DBCOMBO:DBCOMBO></TD>
								</TR>
								<TR style="HEIGHT: 25px">
									<TD class="tableLabel">&nbsp;<asp:label id="Label3" runat="server" Width="181px" CssClass="tableLabel" Height="22px">Destination Distribution Center</asp:label></TD>
									<TD><DBCOMBO:DBCOMBO id="DbComboDestinationDC" tabIndex="14" Width="25px" CssClass="tableLabel" AutoPostBack="True"
											ServerMethod="DbComboDistributionCenterSelect" ShowDbComboLink="False" TextUpLevelSearchButton="v"
											RegistrationKey=" " TextBoxColumns="18" Runat="server"></DBCOMBO:DBCOMBO></TD>
								</TR>
							</TABLE>
						</fieldset>
						<P>&nbsp;</P>
					</TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>
