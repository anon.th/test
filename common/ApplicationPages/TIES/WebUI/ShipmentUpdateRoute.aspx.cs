using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.common.util;
using System.Data.SqlClient;
using com.ties.classes;
using com.ties.DAL;
using com.common.applicationpages;


namespace com.ties
{
	/// <summary>
	/// Summary description for ShipmentUpdateRoute.
	/// </summary>
	public class ShipmentUpdateRoute : BasePage
	{
		protected System.Web.UI.WebControls.Label TITLE;
		protected System.Web.UI.WebControls.Button btnSave;
		protected System.Web.UI.WebControls.Label lblErrorMsg;
		protected System.Web.UI.HtmlControls.HtmlTable ShipmentUpdRouteMainTable;
		protected System.Web.UI.WebControls.RadioButton rbtnLong;
		protected System.Web.UI.WebControls.RadioButton rbtnShort;
		protected System.Web.UI.WebControls.RadioButton rbtnAir;
		protected System.Web.UI.WebControls.Label lblRouteRadioButton;
		protected System.Web.UI.WebControls.Label lblRouteCode;
		protected com.common.util.msTextBox txtRouteCode;
		protected System.Web.UI.WebControls.Button btnRouteCodeSearch;
		protected System.Web.UI.WebControls.TextBox txtRouteDesc;
		protected System.Web.UI.WebControls.Label lblDeptrDtTime;
		protected com.common.util.msTextBox txtDeptrDtTime;
		protected System.Web.UI.WebControls.Label lblVehNo;
		protected System.Web.UI.WebControls.Label lblDriverName;
		protected System.Web.UI.WebControls.TextBox txtDriverName;
		protected System.Web.UI.WebControls.TextBox txtOrigin;
		protected System.Web.UI.WebControls.TextBox txtDestination;
		protected System.Web.UI.WebControls.Label lblDGOrigin;
		protected System.Web.UI.WebControls.Label lblDGDestination;
		protected System.Web.UI.WebControls.Label lblOrigin;
		protected System.Web.UI.WebControls.Label lblDestination;
		protected System.Web.UI.WebControls.Button btnRetrieveWayBill;
		protected System.Web.UI.WebControls.Button btnSelectAll;
		protected System.Web.UI.WebControls.Button btnClearAll;
		protected System.Web.UI.WebControls.RequiredFieldValidator validStatusCode;
		protected System.Web.UI.WebControls.Label lblStatusCode;
		protected com.common.util.msTextBox txtStatusCode;
		protected System.Web.UI.WebControls.Button btnStatusCodeSearch;
		protected System.Web.UI.WebControls.Label lblExceptionCode;
		protected com.common.util.msTextBox txtExceptioncode;
		protected System.Web.UI.WebControls.Button btnExcepDescSrch;
		protected System.Web.UI.WebControls.Label lblStatusDate;
		protected com.common.util.msTextBox txtStatusDateTime;
		protected System.Web.UI.WebControls.Label lblPersonInchrg;
		protected com.common.util.msTextBox txtPersonInchrg;
		protected System.Web.UI.WebControls.Label lblRemarks;
		protected com.common.util.msTextBox txtRemarks;
		
		String deliveryType="L";
		String appID = null;
		String enterpriseID = null;
		String userID = null;
		protected System.Web.UI.WebControls.ValidationSummary PageValidationSummary;
		DataSet m_dsShpmntUpdtRoute = null;
		protected System.Web.UI.WebControls.Label lblDlvryPath;
		protected System.Web.UI.WebControls.RequiredFieldValidator validDlvryPath;
		protected System.Web.UI.WebControls.RequiredFieldValidator validDepartDateTime;
		protected System.Web.UI.WebControls.RequiredFieldValidator validVehicleNo;
		protected System.Web.UI.WebControls.RequiredFieldValidator validStatusDateTime;
		protected System.Web.UI.WebControls.RequiredFieldValidator validPersonInChrg;
		protected Cambro.Web.DbCombo.DbCombo DbComboPathCode;
		protected System.Web.UI.WebControls.TextBox txtPathDesc;
		protected System.Web.UI.WebControls.Label lblDlvryDateTime;
		protected System.Web.UI.WebControls.RequiredFieldValidator validDlvryDateTime;
		protected Cambro.Web.DbCombo.DbCombo DbComboDlvryDt;
		protected System.Web.UI.HtmlControls.HtmlTable tblShipmentUpdateRoute;
		protected System.Web.UI.WebControls.TextBox txtStatusDesc;
		protected System.Web.UI.WebControls.TextBox txtExceptionDesc;
		protected Cambro.Web.DbCombo.DbCombo dbFlightVehicleNo;
		protected System.Web.UI.WebControls.DataGrid dgShipRoute;
		protected System.Web.UI.WebControls.Label Label2;
		protected System.Web.UI.WebControls.Label Label3;
		protected System.Web.UI.WebControls.Label lblLocation;
		protected com.common.util.msTextBox txtLocation;
		String strRouteType = null;
		string ValidST  = "Y";
	
		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			
			// Set the Third Party DbCombo component Registration Key.
			DbComboPathCode.RegistrationKey = System.Configuration.ConfigurationSettings.AppSettings["DbComboRegKey"];
			DbComboDlvryDt.RegistrationKey = System.Configuration.ConfigurationSettings.AppSettings["DbComboRegKey"];
			dbFlightVehicleNo.RegistrationKey = System.Configuration.ConfigurationSettings.AppSettings["DbComboRegKey"];

			utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings,Page.Session);
			appID = utility.GetAppID();
			enterpriseID = utility.GetEnterpriseID();
			userID = utility.GetUserID();
			if(!Page.IsPostBack)
			{				
				ResetScreen();
				validStatusCode.ErrorMessage = "Status Code ";
				validDepartDateTime.ErrorMessage ="Departure Date Time ";
				validDlvryPath.ErrorMessage = "Delivery Path ";
				validPersonInChrg.ErrorMessage = "Person In Charge ";
				validStatusCode.ErrorMessage = "Status Code ";
				validStatusDateTime.ErrorMessage = "Status date time ";
				validVehicleNo.ErrorMessage = "Vehicle/Flight No. ";
				validDlvryDateTime.ErrorMessage = "Delivery Date Time ";
				
				Utility.EnableButton(ref btnSave,ButtonType.Save,false,m_moduleAccessRights);

				String strDt = DateTime.Now.ToString("dd/MM/yyyy HH:mm");
				txtStatusDateTime.Text = strDt;

			}
			else
			{
				m_dsShpmntUpdtRoute = (DataSet)Session["SESSION_DS1"];
			}
			btnClearAll.Enabled=true;
			btnSelectAll.Enabled=true;

			SetDbComboServerStates();
			SetdbFlightVehicleStates();
			SetdbManifestDateStates();
			//addDbComboEventHandler();
			PageValidationSummary.HeaderText = Utility.GetLanguageText(ResourceType.UserMessage, "PLS_ENTER_MISS_FIELD", utility.GetUserCulture());
			getPageControls(Page);

			if(txtRouteCode.Text.Trim() == "") 
			{
				txtRouteDesc.Text = "";
				txtOrigin.Text = "";
				txtDestination.Text = "";
			}
		}

		private void SetDbComboServerStates()
		{
			String strDeliveryType=null;
			if (rbtnLong.Checked==true)
				strDeliveryType="L";
			else if (rbtnShort.Checked==true)
				strDeliveryType="S";
			else if (rbtnAir.Checked==true)
				strDeliveryType="A";

			Hashtable hash = new Hashtable();
			hash.Add("strDeliveryType", strDeliveryType);
			DbComboPathCode.ServerState = hash;
			DbComboPathCode.ServerStateSecretString = "kdfsjgnskajh[apq2-4uhg465654435879mnbfdm cxs;q4";
		}
		private void SetdbFlightVehicleStates()
		{			
			String strDeliveryPath="";				
			//query
			Hashtable hash = new Hashtable();			
			strDeliveryPath=DbComboPathCode.Value;
			if (strDeliveryPath !=null && strDeliveryPath !="")
			{
				hash.Add("strDeliveryPath", strDeliveryPath);
			}			
			dbFlightVehicleNo.ServerState = hash;
			dbFlightVehicleNo.ServerStateSecretString = "kdfsjgnskajh[apq2-4uhg465654435879mnbfdm cxs;q4";		
		}

		private void SetdbManifestDateStates()
		{			
			String strDeliveryPath="";			
			String strFlightVehicle="";			
			//query			
			strDeliveryPath=DbComboPathCode.Value;			
			strFlightVehicle=dbFlightVehicleNo.Value;
			Hashtable hash = new Hashtable();
			if (strDeliveryPath !=null && strDeliveryPath !="")
			{
				hash.Add("strDeliveryPath", strDeliveryPath);
			}
			if (strFlightVehicle != null && strFlightVehicle !="")
			{
				hash.Add("strFlightVehicle", strFlightVehicle);
			}
			DbComboDlvryDt.ServerState = hash;
			DbComboDlvryDt.ServerStateSecretString = "kdfsjgnskajh[apq2-4uhg465654435879mnbfdm cxs;q4";			
		}

		private void addDbComboEventHandler()
		{
			if(this.DbComboPathCode != null)
			{
				this.DbComboPathCode.SelectedItemChanged += new System.EventHandler(this.DbComboPathCode_OnSelectedIndexChanged);
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			base.OnInit(e);
			InitializeComponent();
			
			
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{   
			this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
			this.rbtnLong.CheckedChanged += new System.EventHandler(this.rbtnLong_CheckedChanged);
			this.rbtnShort.CheckedChanged += new System.EventHandler(this.rbtnShort_CheckedChanged);
			this.rbtnAir.CheckedChanged += new System.EventHandler(this.rbtnAir_CheckedChanged);
			this.DbComboPathCode.SelectedItemChanged += new System.EventHandler(this.DbComboPathCode_OnSelectedIndexChanged);
			this.dbFlightVehicleNo.SelectedItemChanged += new System.EventHandler(this.dbFlightVehicleNo_SelectedItemChanged);
			this.txtDeptrDtTime.TextChanged += new System.EventHandler(this.txtDeptrDtTime_TextChanged);
			this.DbComboDlvryDt.SelectedItemChanged += new System.EventHandler(this.DbComboDlvryDt_SelectedItemChanged);
			this.txtRouteCode.TextChanged += new System.EventHandler(this.txtRouteCode_TextChanged);
			this.btnRouteCodeSearch.Click += new System.EventHandler(this.btnRouteCodeSearch_Click);
			this.txtOrigin.TextChanged += new System.EventHandler(this.txtOrigin_TextChanged);
			this.txtStatusCode.TextChanged += new System.EventHandler(this.txtStatusCode_TextChanged);
			this.btnStatusCodeSearch.Click += new System.EventHandler(this.btnStatusCodeSearch_Click);
			this.txtExceptioncode.TextChanged += new System.EventHandler(this.txtExceptioncode_TextChanged);
			this.btnExcepDescSrch.Click += new System.EventHandler(this.btnExcepDescSrch_Click);
			this.txtStatusDateTime.TextChanged += new System.EventHandler(this.txtStatusDateTime_TextChanged);
			this.txtPersonInchrg.TextChanged += new System.EventHandler(this.txtPersonInchrg_TextChanged);
			this.txtRemarks.TextChanged += new System.EventHandler(this.txtRemarks_TextChanged);
			this.dgShipRoute.SelectedIndexChanged += new System.EventHandler(this.dgShipRoute_SelectedIndexChanged);
			this.btnRetrieveWayBill.Click += new System.EventHandler(this.btnRetrieveWayBill_Click);
			this.btnSelectAll.Click += new System.EventHandler(this.btnSelectAll_Click);
			this.btnClearAll.Click += new System.EventHandler(this.btnClearAll_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		public void BindGrid()
		{
			if(Session["SESSION_DS1"] != null)
			{
				m_dsShpmntUpdtRoute = (DataSet)Session["SESSION_DS1"];
			}
			dgShipRoute.DataSource = m_dsShpmntUpdtRoute;
			dgShipRoute.DataBind();
			//Call the method to check the value for the boolean column & check the chekbox accordingly.
			UpdateSelected();
		}

		private void btnRouteCodeSearch_Click(object sender, System.EventArgs e)
		{
//			if(rbtnAir.Checked)
//				strRouteType = "Air";
//			else if(rbtnLong.Checked)
//				strRouteType = "Long";
//			else if(rbtnShort.Checked)
//				strRouteType = "Short";

//			String sDbVehicleID="dbFlightVehicleNo_ulbTextBox";
//			String url = "RouteDetailPopup.aspx?ROUTETYPE="+strRouteType+"&FLIGHT_VEHICLE_ID="+sDbVehicleID;
			TextBox txtRouteCode = (TextBox)Page.FindControl("txtRouteCode");
			TextBox txtRouteDesc = (TextBox)Page.FindControl("txtRouteDesc");
			TextBox txtOrigin = (TextBox)Page.FindControl("txtOrigin");
			TextBox txtDestination = (TextBox)Page.FindControl("txtDestination");
			String strPickupRoute = null;
			String strtxtPickupRoute = null;
			if(txtRouteCode != null)
			{
				strtxtPickupRoute = txtRouteCode.ClientID;
				strPickupRoute = txtRouteCode.Text;
			}
				
			String sUrl = "RouteCodePopup.aspx?FORMID=ShipmentUpdateRoute" +
							"&ROUTECODE_TEXT="+strtxtPickupRoute+
							"&ROUTECODE="+strPickupRoute+
							"&ROUTEDESC="+txtRouteDesc.ClientID+
							"&ROUTEORIG="+txtOrigin.ClientID+
							"&ROUTEDEST="+txtDestination.ClientID+
							"&PATHCODE="+DbComboPathCode.Value+
							"&FGNO="+dbFlightVehicleNo.Value+
							"&DLDATE="+DbComboDlvryDt.Value;
			
			ArrayList paramList = new ArrayList();
			paramList.Add(sUrl);
			String sScript = Utility.GetScript("openWindowParam.js",paramList);
			Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
		}

		private void rbtnLong_CheckedChanged(object sender, System.EventArgs e)
		{
			strRouteType = "Long";
			if (rbtnLong.Checked==true)
			{
				deliveryType="L";			
			}

			lblVehNo.Text = Utility.GetLanguageText(ResourceType.ScreenLabel,"Vehicle No",utility.GetUserCulture());
			lblDriverName.Text =Utility.GetLanguageText(ResourceType.ScreenLabel,"Driver Name",utility.GetUserCulture());
			ClearDbCombos();
		}

		private void rbtnShort_CheckedChanged(object sender, System.EventArgs e)
		{
			strRouteType = "Short";
			if (rbtnShort.Checked==true)
			{
				deliveryType="S";			
			}
			lblVehNo.Text =Utility.GetLanguageText(ResourceType.ScreenLabel,"Vehicle No",utility.GetUserCulture());
			lblDriverName.Text = Utility.GetLanguageText(ResourceType.ScreenLabel,"Driver Name",utility.GetUserCulture());
			ClearDbCombos();
		}																		

		private void rbtnAir_CheckedChanged(object sender, System.EventArgs e)
		{
			strRouteType = "Air";
			if (rbtnAir.Checked==true)
			{
				deliveryType="A";						
			}			
			lblVehNo.Text = Utility.GetLanguageText(ResourceType.ScreenLabel,"Flight No",utility.GetUserCulture());
			lblDriverName.Text =Utility.GetLanguageText(ResourceType.ScreenLabel,"Air Way Bill No",utility.GetUserCulture());
			ClearDbCombos();
		}
		
		[Cambro.Web.DbCombo.ResultsMethodAttribute(true)]
		public static object DbComboPathCodeSelect(Cambro.Web.DbCombo.ServerMethodArgs args)
		{			
			Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings,HttpContext.Current.Session);
			String strAppID = util.GetAppID();
			String strEnterpriseID = util.GetEnterpriseID();
			
			String strDelType = "L";			
			String strWhereClause=null;
			if(args.ServerState != null && args.ServerState.Authenticate("kdfsjgnskajh[apq2-4uhg465654435879mnbfdm cxs;q4"))
			{
				if(args.ServerState["strDeliveryType"] != null && args.ServerState["strDeliveryType"].ToString().Length > 0)
				{
					strDelType = args.ServerState["strDeliveryType"].ToString();
					strWhereClause=" and Delivery_Type='"+Utility.ReplaceSingleQuote(strDelType)+"'";
				}				
			}
			
			DataSet dataset = DbComboDAL.PathCodeQuery(strAppID,strEnterpriseID,args, strWhereClause);	
			return dataset;
		}

		[Cambro.Web.DbCombo.ResultsMethodAttribute(true)]
		public static object ShowManifestedDate(Cambro.Web.DbCombo.ServerMethodArgs args)
		{
			Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings,HttpContext.Current.Session);
			String strAppID = util.GetAppID();
			String strEnterpriseID = util.GetEnterpriseID();			
			String strDelPath="";
			String strFltVeh="";
			String strWhereClause="";

			if(args.ServerState != null && args.ServerState.Authenticate("kdfsjgnskajh[apq2-4uhg465654435879mnbfdm cxs;q4"))
			{			
				if(args.ServerState["strDeliveryPath"] != null && args.ServerState["strDeliveryPath"].ToString().Length > 0)
				{
					strDelPath = args.ServerState["strDeliveryPath"].ToString();
					strWhereClause+=" and Path_Code='"+strDelPath+"'";
				}				
				if(args.ServerState["strFlightVehicle"] != null && args.ServerState["strFlightVehicle"].ToString().Length > 0)
				{
					strFltVeh = args.ServerState["strFlightVehicle"].ToString();
					strWhereClause+=" and Flight_Vehicle_No='"+strFltVeh+"'";
				}				
			}
			
			DataSet dataset = DbComboDAL.DeliveryDatetime(strAppID,strEnterpriseID,args, strWhereClause);	
			return dataset;
		}

		//		[Cambro.Web.DbCombo.ResultsMethodAttribute(true)]
		//		public static object DbComboDlvryDtSelect(Cambro.Web.DbCombo.ServerMethodArgs args)
		//		{
		//			Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings,HttpContext.Current.Session);
		//			String strAppID = util.GetAppID();
		//			String strEnterpriseID = util.GetEnterpriseID();
		//			
		//			DataSet dsData = null;
		//
		//			if (args.ClientState!=null)
		//			{
		//				if (args.ClientState["PathCode"] != null && args.ClientState["PathCode"].ToString()!="-1" )
		//				{
		//					string extraWhereClause = "";
		//					extraWhereClause += " AND path_code = '" + args.ClientState["PathCode"].ToString()+ "' ";
		//					dsData = DbComboDAL.DeliveryDatetime(strEnterpriseID,strEnterpriseID,args,extraWhereClause);
		//					return dsData;
		//				}
		//			}
		//
		//			dsData = DbComboDAL.DeliveryDatetime(strAppID,strEnterpriseID,args);
		//			return dsData;
		//		}

		[Cambro.Web.DbCombo.ResultsMethodAttribute(true)]
		public static object ShowFlightVehicleNo(Cambro.Web.DbCombo.ServerMethodArgs args)
		{
			Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings,HttpContext.Current.Session);
			String strAppID = util.GetAppID();
			String strEnterpriseID = util.GetEnterpriseID();
			String strDelPath="";			
			String strWhereClause="";

			if(args.ServerState != null && args.ServerState.Authenticate("kdfsjgnskajh[apq2-4uhg465654435879mnbfdm cxs;q4"))
			{					
				if(args.ServerState["strDeliveryPath"] != null && args.ServerState["strDeliveryPath"].ToString().Length > 0)
				{
					strDelPath = args.ServerState["strDeliveryPath"].ToString();
					strWhereClause+=" and Path_Code='"+strDelPath+"'";
				}									
			}
			
			DataSet dataset = DbComboDAL.FlightVehicleQuery(strAppID,strEnterpriseID,args, strWhereClause);	
			return dataset;
		}

		private void btnRetrieveWayBill_Click(object sender, System.EventArgs e)
		{
			if (dbFlightVehicleNo.Text !="")
			{
				if (dbFlightVehicleNo.Text!=dbFlightVehicleNo.Value)
				{
					dbFlightVehicleNo.Value=dbFlightVehicleNo.Text;
				}
			}
			if (DbComboPathCode.Value.Length <= 0 || DbComboDlvryDt.Value.Length <= 0 || 
				dbFlightVehicleNo.Value.Length <= 0 || txtDeptrDtTime.Text.Length <= 0)
			{
				//lblErrorMsg.Text =  Utility.GetLanguageText(ResourceType.ScreenLabel,"PLS_ENTER_DVL_DEP_DT_FLIGHT",utility.GetUserCulture());
				if (rbtnAir.Checked == true)
					lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"PLS_ENTER_MISS_FIELD",utility.GetUserCulture());
				else
					lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"PLS_ENTER_MISS_FIELD",utility.GetUserCulture());
				return;
			}

			getConsignmentParam CongnParam = new getConsignmentParam();			
			CongnParam.strDeliveryPathCode = DbComboPathCode.Value;
			CongnParam.dtDeliveryDatetime = DateTime.ParseExact(DbComboDlvryDt.Value,"dd/MM/yyyy HH:mm",null);
			CongnParam.strFltVehNo = dbFlightVehicleNo.Value;
			CongnParam.dtDepartDatetime = DateTime.ParseExact(txtDeptrDtTime.Text,"dd/MM/yyyy HH:mm",null);

			if (txtRouteCode.Text.Length > 0)
				CongnParam.strRouteCode = txtRouteCode.Text; 
			else
				CongnParam.strRouteCode = ""; 

			Session["SESSION_DS1"] = null;
			m_dsShpmntUpdtRoute = null;
			DataSet dsConsignment = ShipmentUpdateMgrDAL.GetAllDomesticCongn(appID,enterpriseID, CongnParam);
			m_dsShpmntUpdtRoute = ShipmentUpdateMgrDAL.GetEmptyShipUpdRouteDS();
			if((dsConsignment != null) && (dsConsignment.Tables[0].Rows.Count > 0))
			{
				
				int iTot = dsConsignment.Tables[0].Rows.Count;
				for(int i = 0; i < iTot; i++)
				{
					ShipmentUpdateMgrDAL.AddRowInShipUpdRouteDS(m_dsShpmntUpdtRoute);
					
					DataRow drDetail = dsConsignment.Tables[0].Rows[i];
					DataRow drShpmntRoute = m_dsShpmntUpdtRoute.Tables[0].Rows[i];
					
					drShpmntRoute["consignment_no"] = drDetail["consignment_no"];
					if(Utility.IsNotDBNull(drDetail["origin_state_code"]))
					{
						//String strOrg = TIESUtility.GetStateDesc(appID,enterpriseID,drDetail["origin_state_code"].ToString());
						String strOrg = drDetail["origin_state_code"].ToString();
						drShpmntRoute["origin_state_code"] = strOrg;
					}

					if(Utility.IsNotDBNull(drDetail["destination_state_code"]))
					{
						//String strDest = TIESUtility.GetStateDesc(appID,enterpriseID,drDetail["destination_state_code"].ToString());
						String strDest = drDetail["destination_state_code"].ToString();
						drShpmntRoute["destination_state_code"] = strDest;
					}
					
					if (Utility.IsNotDBNull(drDetail["route_code"]))
					{
						drShpmntRoute["route_code"] = drDetail["route_code"];
					}
					
				}
				btnClearAll.Enabled=false;
				btnSelectAll.Enabled=false;

				//Add a boolean column 
				m_dsShpmntUpdtRoute.Tables[0].Columns.Add(new DataColumn("isSelected", typeof(bool)));
				int cnt = m_dsShpmntUpdtRoute.Tables[0].Rows.Count;
				if (cnt==0)
				{
					btnClearAll.Enabled=false;
					btnSelectAll.Enabled=false;
				}
				else
				{
					btnClearAll.Enabled=true;
					btnSelectAll.Enabled=true;
				}
				for(int j=0;j<cnt;j++)
				{
					DataRow drCurrent = m_dsShpmntUpdtRoute.Tables[0].Rows[j];
					drCurrent["isSelected"] = false;
				}
				
				Session["SESSION_DS1"] = m_dsShpmntUpdtRoute;
				
				BindGrid();		

				Utility.EnableButton(ref btnSave,ButtonType.Save,true,m_moduleAccessRights);

				//if(Utility.IsDeliveryManifestCountCorrected(this.appID,this.enterpriseID,this.DbComboPathCode.Value,this.dbFlightVehicleNo.Value,this.DbComboDlvryDt.Value))
					//Utility.EnableButton(ref btnSave,ButtonType.Save,true,m_moduleAccessRights);
				//else
					//Utility.EnableButton(ref btnSave,ButtonType.Save,false,m_moduleAccessRights);
		
			}
			else
			{
				dgShipRoute.DataSource = "";
				dgShipRoute.DataBind();
				btnClearAll.Enabled=false;
				btnSelectAll.Enabled=false;
			}
			
		}

		private void btnSelectAll_Click(object sender, System.EventArgs e)
		{
			SelectAll(true);			
		}

		private void SelectAll(bool isSelect)
		{
			if (m_dsShpmntUpdtRoute==null)
			{
				btnClearAll.Enabled=false;
				btnSelectAll.Enabled=false;
				return;
			}
			else
			{
				btnClearAll.Enabled=true;
				btnSelectAll.Enabled=true;
			}
			
			if (m_dsShpmntUpdtRoute.Tables[0].Rows.Count <=0)
			{
				return;
			}
			for(int j=0;j<m_dsShpmntUpdtRoute.Tables[0].Rows.Count;j++)
			{
				DataRow drCurrent = m_dsShpmntUpdtRoute.Tables[0].Rows[j];
				drCurrent["isSelected"] = isSelect;
			}
			Session["SESSION_DS1"] = m_dsShpmntUpdtRoute;
			BindGrid();			
		}
		private void UpdateSelected()
		{
			if((m_dsShpmntUpdtRoute != null) && (m_dsShpmntUpdtRoute.Tables[0].Rows.Count > 0))
			{
				for(int i = 0; i < m_dsShpmntUpdtRoute.Tables[0].Rows.Count; i++)
				{
					DataRow drRow = m_dsShpmntUpdtRoute.Tables[0].Rows[i];
					
					if((drRow["isSelected"] != null) && (!drRow["isSelected"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						bool isSelected = (bool)drRow["isSelected"];
						CheckBox chkSelect = (CheckBox)dgShipRoute.Items[i].FindControl("chkSelect");
						if((chkSelect != null) && (isSelected))
						{
							chkSelect.Checked = true;
						}
						else
						{
							chkSelect.Checked = false;
						}
					}
				}
			}
		}

		private void btnClearAll_Click(object sender, System.EventArgs e)
		{
			SelectAll(false);
		}

		private DataSet GetSelectedData()
		{
			//Get only the selected data from the m_dsShpmntUpdtRoute.
			DataSet dsSelectedData = ShipmentUpdateMgrDAL.GetEmptyShipUpdRouteDS();
			dsSelectedData.Tables[0].Columns.Add(new DataColumn("tracking_datetime",typeof(DateTime)));
			dsSelectedData.Tables[0].Columns.Add(new DataColumn("CnsgBkgNo",typeof(String)));
			dsSelectedData.Tables[0].Columns.Add(new DataColumn("person_incharge",typeof(String)));
			dsSelectedData.Tables[0].Columns.Add(new DataColumn("location",typeof(String)));//BY X JAN 22 08
			dsSelectedData.Tables[0].Columns.Add(new DataColumn("remarks",typeof(String)));
			dsSelectedData.Tables[0].Columns.Add(new DataColumn("booking_no",typeof(int)));
			dsSelectedData.Tables[0].Columns.Add(new DataColumn("ZipCode",typeof(String)));
			dsSelectedData.Tables[0].Columns.Add(new DataColumn("consignee_Name",typeof(String)));
			dsSelectedData.Tables[0].Columns.Add(new DataColumn("pkg_no",typeof(string)));

			int j = 0;
			for(int i = 0;i < dgShipRoute.Items.Count; i++)
			{
				CheckBox chkSelect = (CheckBox)dgShipRoute.Items[i].FindControl("chkSelect");
				if((chkSelect != null) && (chkSelect.Checked))
				{
					ShipmentUpdateMgrDAL.AddRowInShipUpdRouteDS(dsSelectedData);
					DataRow drSelEach = dsSelectedData.Tables[0].Rows[j];
					Label lblConsgmntNo = (Label)dgShipRoute.Items[i].FindControl("lblConsgmntNo");
					if(lblConsgmntNo != null)
					{
						drSelEach["CnsgBkgNo"] = lblConsgmntNo.Text;
					
						drSelEach["tracking_datetime"] = DateTime.ParseExact(txtStatusDateTime.Text,"dd/MM/yyyy HH:mm",null);
						drSelEach["person_incharge"] = txtPersonInchrg.Text;

						if(txtLocation.Text!="")
							drSelEach["location"] = txtLocation.Text;
						else
							drSelEach["location"] = System.DBNull.Value;

						drSelEach["consignee_Name"] = System.DBNull.Value;

						drSelEach["remarks"] = txtRemarks.Text;

						int iBookingNo = ShipmentUpdateMgrDAL.GetShipmentBookingNo(appID,enterpriseID,lblConsgmntNo.Text);
						drSelEach["booking_no"] = iBookingNo;
						
						//ADD ZIPCODE BY X JAN 22 08
						DataSet tmpDSZipCode = ShipmentUpdateMgrDAL.getZipCodeByCon(appID,enterpriseID,lblConsgmntNo.Text, Convert.ToString(iBookingNo));
						drSelEach["ZipCode"] = tmpDSZipCode.Tables[0].Rows[0]["recipient_zipcode"].ToString();
						//END ADD ZIPCODE

						j++;
					}
				}	
			}
			return dsSelectedData;
		}
		private void btnSave_Click(object sender, System.EventArgs e)
		{
			lblErrorMsg.Text = "";

			if(txtExceptioncode.Text != "PODEX34")
			{                 

//CHECK LOCATION BY X JAN 22 08
				bool boolLocationOK=false;

				if(txtLocation.Text.Trim()!="") //�����प�������ҧ
				{
					DataSet dataset1 = com.ties.classes.DbComboDAL.UserLocationQuery(this.appID,this.enterpriseID);
					if(dataset1.Tables[0].Rows.Count > 0) //����մի���ʡ�������硡дիա�͹�Ш��
					{
						foreach(DataRow tmpDr in dataset1.Tables[0].Rows)
						{
							if(tmpDr["origin_code"].ToString().ToUpper()==txtLocation.Text.Trim().ToUpper())
								boolLocationOK = true; //��ҵç�ѹ �����
						}

						if(!boolLocationOK)//�����प�� �ç�дի� ���ҹ ������ç�ѹ �����ͧ��͹ �������͡ ���ѹ�֡��¨��
						{
							lblErrorMsg.Text =  "The Location must be a Distribution Center.";//Utility.GetLanguageText(ResourceType.UserMessage,"Consignee Name is a required entry for a POD status.",utility.GetUserCulture());  
							return;
						}
					}
				}

//END CHECK LOCATION
	//Jeab 22 Dec 10
			CheckStatusCode(txtStatusCode.Text);
			if(ValidST != "Y")
			{
			return;
			}
	//Jeab 22 Dec 10 =========> End
			//Check whether any row in the grid is selected
			if(AnyRowChecked() == false)
			{
				lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"SEL_CONSGMT",utility.GetUserCulture());
				return;
			}

			//Save the record.
				if(((int)ViewState["SUOperation"] == (int)Operation.Saved)) 
				{
					try
					{

						DataSet dsSelectedData = GetSelectedData();
					
						String	strDmstPickup = "Domestic";
					
						String strExcepCode = null;
						
						if(txtStatusCode.Text.Trim() == "POD")
						{
							bool isMDE = false;
							int cnt = dsSelectedData.Tables[0].Rows.Count;
							DataRow drEach = null;
							for(int i=0;i<cnt;i++)
							{
								drEach = dsSelectedData.Tables[0].Rows[i];	
								String strConsgtNo = (String)drEach["CnsgBkgNo"];
								isMDE = TIESUtility.IsStatusMDE(appID,enterpriseID,strConsgtNo);
								if(!isMDE)
								{
									lblErrorMsg.Text = "Consignment# "+strConsgtNo+" must have MDE status";
									return;
								}
							}							
						}


						if(txtStatusCode.Text.Trim() == "PODEX")
						{
							
							int cnt = dsSelectedData.Tables[0].Rows.Count;
							DataRow drEach = null;
							for(int i=0;i<cnt;i++)
							{
								drEach = dsSelectedData.Tables[0].Rows[i];	
								String strConsgtNo = drEach["CnsgBkgNo"].ToString().Trim().ToUpper();
								
								if(!ShipmentUpdateMgrDAL.CheckPODStatus(appID,enterpriseID, strConsgtNo))
								{
									lblErrorMsg.Text = "Consignment# "+strConsgtNo+" must have POD status";
									return;
								}

								if(!ShipmentUpdateMgrDAL.IsDupStatusPODEX(appID,enterpriseID, strConsgtNo, txtStatusCode.Text.ToUpper()))
								{
									lblErrorMsg.Text = txtStatusCode.Text.ToUpper() + " status has been assigned to Consignment# " +strConsgtNo+".";
									return;
								}// Consignment# "+strConsgtNo+" must have POD status

							}							
						}

						if(txtExceptioncode.Text.Length > 0)
							strExcepCode = txtExceptioncode.Text;

						ShipmentUpdateMgrDAL.AddRecordShipmntTracking(appID,enterpriseID,dsSelectedData,txtStatusCode.Text,strExcepCode,strDmstPickup,userID,"");
						ChangeSUState();
					}
					catch(ApplicationException appException)
					{
						String strMsg = appException.Message;
						if(strMsg.IndexOf("duplicate key") != -1 || strMsg.IndexOf("unique")!=-1)
						{
							//SQL Server returns "duplicate key" and Oracle returns "unique"
							lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"DUP_KEY_FOUND",utility.GetUserCulture());
						}
						else if(strMsg.IndexOf("FK") != -1 )
						{
							lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"FOREIGN_KEY_FOUND",utility.GetUserCulture());
						}
						else if(strMsg.IndexOf("PK") != -1 )
						{
							lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"FOREIGN_KEY_FOUND",utility.GetUserCulture());
						}
						else
						{
							lblErrorMsg.Text = strMsg;
						}
						return;
					}

					lblErrorMsg.Text =Utility.GetLanguageText(ResourceType.UserMessage,"INS_SUCCESSFULLY",utility.GetUserCulture());
					Utility.EnableButton(ref btnSave,ButtonType.Save,false,m_moduleAccessRights);
					//Utility.EnableButton(ref btnInsert,ButtonType.Insert,true,m_moduleAccessRights);
					ResetScreen();
				}
			}
			else
			{
				txtExceptioncode.Text = "";
				lblErrorMsg.Text = "Can't Update Exception Code PODEX34";
			}   
			
		}

		// Modified by	: Bernard Phua on 2-Jan-2003
		// Comments		: CheckWithLastestDate() method is commented. This is not required anymore.

		//		private bool CheckWithLastestDate()
		//		{
		//			DataSet dsLastestDt = null;
		//			for(int i = 0;i < dgShipRoute.Items.Count;i++)
		//			{
		//				Label lblConsgmntNo = (Label)dgShipRoute.Items[i].FindControl("lblConsgmntNo");
		//				CheckBox chkSelect = (CheckBox)dgShipRoute.Items[i].FindControl("chkSelect");
		//				if((lblConsgmntNo != null) && (chkSelect != null) && (chkSelect.Checked))
		//				{
		//					//get booking no for consignment.
		//					int iBkNo  = ShipmentUpdateMgrDAL.GetShipmentBookingNo(appID,enterpriseID,lblConsgmntNo.Text);
		//					dsLastestDt =  ShipmentTrackingMgrDAL.GetLatestDateFromShipment(appID,enterpriseID,iBkNo);
		//					
		//					if((dsLastestDt != null) && (dsLastestDt.Tables[0].Rows.Count > 0))
		//					{
		//						DataRow drDate = dsLastestDt.Tables[0].Rows[0];
		//						if((drDate["last_status_datetime"] != null) && (!drDate["last_status_datetime"].GetType().Equals(System.Type.GetType("System.DBNull"))))
		//						{
		//							DateTime dtLatestStatus = Convert.ToDateTime(drDate["last_status_datetime"]);
		//							int iCompareResult = dtLatestStatus.CompareTo(DateTime.ParseExact(txtStatusDateTime.Text,"dd/MM/yyyy HH:mm",null));
		//							if(iCompareResult > 0)
		//							{
		//								lblErrorMsg.Text = "The date is less than the Domestic latest Status date for the Consignment No "+lblConsgmntNo.Text;
		//								return false;
		//							}
		//							else if(iCompareResult == 0)
		//							{
		//								lblErrorMsg.Text = "The date is equal to the Domestic latest Status date for the Consignment No "+lblConsgmntNo.Text;
		//								return false;
		//							}
		//						}
		//					}
		//				}
		//			}
		//			return true;
		//		}
		private void ResetScreen()
		{
			DbComboPathCode.Text="";DbComboPathCode.Value="";
			dbFlightVehicleNo.Text = "";dbFlightVehicleNo.Value = "";
			DbComboDlvryDt.Text="";DbComboDlvryDt.Value="";
			txtPathDesc.Text="";
			txtDeptrDtTime.Text = ""; txtDestination.Text = "";
			txtDriverName.Text = ""; txtExceptioncode.Text = "";
			txtExceptionDesc.Text = ""; txtOrigin.Text = "";
			txtPersonInchrg.Text = ""; txtRemarks.Text = "";
			txtLocation.Text = "";
			txtRouteCode.Text = ""; txtRouteDesc.Text = "";
			txtStatusCode.Text = ""; 
			txtStatusDesc.Text = ""; 
			
			String strDt = DateTime.Now.ToString("dd/MM/yyyy HH:mm");
			txtStatusDateTime.Text = strDt;

			com.common.classes.User user = com.common.RBAC.RBACManager.GetUser(utility.GetAppID(), utility.GetEnterpriseID(), utility.GetUserID());
			this.txtLocation.Text = user.UserLocation;
			
			//Utility.EnableButton(ref btnInsert,ButtonType.Insert,false,m_moduleAccessRights);
			Utility.EnableButton(ref btnSave,ButtonType.Save,true,m_moduleAccessRights);

			ViewState["SUMode"] = ScreenMode.Insert;
			ViewState["SUOperation"] = Operation.Insert;
			
			rbtnLong.Checked = true;
			strRouteType = "Long";
			lblVehNo.Text =Utility.GetLanguageText(ResourceType.ScreenLabel,"Vehicle No",utility.GetUserCulture());
			lblDriverName.Text = Utility.GetLanguageText(ResourceType.ScreenLabel,"Driver Name",utility.GetUserCulture());

			ViewState["isTextChanged"] = false;
			
			m_dsShpmntUpdtRoute = ShipmentUpdateMgrDAL.GetEmptyShipUpdRouteDS();
		
			Session["SESSION_DS1"] = m_dsShpmntUpdtRoute;

			BindGrid();
		}
		private bool AnyRowChecked()
		{
			bool isChecked = false;
			for(int i = 0;i < dgShipRoute.Items.Count;i++)
			{
				CheckBox chkSelect = (CheckBox)dgShipRoute.Items[i].FindControl("chkSelect");
				if((chkSelect != null) && (chkSelect.Checked))
				{
					isChecked = true;
					break;
				}
			}
			return isChecked;
		}

		private void ChangeSUState()
		{
			if(((int)ViewState["SUMode"] == (int)ScreenMode.Insert) && ((int)ViewState["SUOperation"] == (int)Operation.None))
			{
				ViewState["SUOperation"] = Operation.Insert;
			}
			else if(((int)ViewState["SUMode"] == (int)ScreenMode.Insert) && ((int)ViewState["SUOperation"] == (int)Operation.Insert))
			{
				//During saving
				ViewState["SUOperation"] = Operation.Saved;
			}
			else if(((int)ViewState["SUMode"] == (int)ScreenMode.Insert) && ((int)ViewState["SUOperation"] == (int)Operation.Saved))
			{
				ViewState["SUOperation"] = Operation.Update;
			}
			else if(((int)ViewState["SUMode"] == (int)ScreenMode.Insert) && ((int)ViewState["SUOperation"] == (int)Operation.Update))
			{
				ViewState["SUOperation"] = Operation.Saved;
			}
			else if(((int)ViewState["SUMode"] == (int)ScreenMode.ExecuteQuery) && ((int)ViewState["SUOperation"] == (int)Operation.None))
			{
				ViewState["SUOperation"] = Operation.Update;
			}
			else if(((int)ViewState["SUMode"] == (int)ScreenMode.ExecuteQuery) && ((int)ViewState["SUOperation"] == (int)Operation.Update))
			{
				ViewState["SUOperation"] = Operation.None;
			}
		}

		private void txtRouteCode_TextChanged(object sender, System.EventArgs e)
		{
			if((bool)ViewState["isTextChanged"] == false)
			{
				ChangeSUState();
				ViewState["isTextChanged"] = true;
			}
		}

		private void txtDeptrDtTime_TextChanged(object sender, System.EventArgs e)
		{
			if((bool)ViewState["isTextChanged"] == false)
			{
				ChangeSUState();
				ViewState["isTextChanged"] = true;
			}
		}

		private void txtVehNo_TextChanged(object sender, System.EventArgs e)
		{
			if((bool)ViewState["isTextChanged"] == false)
			{
				ChangeSUState();
				ViewState["isTextChanged"] = true;
			}
		}

		private void txtOrigin_TextChanged(object sender, System.EventArgs e)
		{
		
		}

		private void txtStatusCode_TextChanged(object sender, System.EventArgs e)
		{
			//Jeab 22 Dec 10
			CheckStatusCode(txtStatusCode.Text);
			if(ValidST != "Y")
			{
				return;
			}
			//Jeab 22 Dec 10 =========> End



//			if((bool)ViewState["isTextChanged"] == false)
//			{
//				ChangeSUState();
//				ViewState["isTextChanged"] = true;
//			}
//
//			if(txtStatusCode.Text.Length > 0)
//			{
//				String strType = "D";
//				StatusCodeDesc statusCodeDesc =  TIESUtility.GetStatusCodeDesc(appID,enterpriseID,txtStatusCode.Text.Trim(),strType);
//				String strStatusCd = statusCodeDesc.strStatusCode;
//				String strStatusDes = statusCodeDesc.strStatusDesc;
//				String strAuto_Location = statusCodeDesc.strAuto_Location;
//				if(strStatusCd == null)
//				{
//					lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"STATUS_PRESENT",utility.GetUserCulture());  
//					//txtStatusCode.Text = "";
//					return;
//				}
//				else
//				{
//					txtStatusDesc.Text = strStatusDes;
//					//BY X FEB 06 08 ����ҡ�൵���鴹�� �ѹ����� AUTO PLC = N �������� User's Location �Ш��
////					StatusCodeDesc statusCodeDesc1 =  TIESUtility.GetStatusCodeInfo(appID,enterpriseID,txtStatusCode.Text.Trim());
////					String strAuto_Location = statusCodeDesc1.strAuto_Location;
//
//					if(strAuto_Location!=null)
//					{
//						if(strAuto_Location.Equals("Y"))
//						{
//							com.common.classes.User user = com.common.RBAC.RBACManager.GetUser(utility.GetAppID(), utility.GetEnterpriseID(), utility.GetUserID());
//							this.txtLocation.Text = user.UserLocation;
//						}
//						else
//							this.txtLocation.Text = "";
//					}
//					else
//						this.txtLocation.Text = "";
//
//				}
//			}
//			else
//			{
//				txtStatusDesc.Text="";
//			}
		}

		private void txtExceptioncode_TextChanged(object sender, System.EventArgs e)
		{
			if((bool)ViewState["isTextChanged"] == false)
			{
				ChangeSUState();
				ViewState["isTextChanged"] = true;
			}

			if(txtExceptioncode.Text.Length > 0)
			{
				if(txtStatusCode.Text.Length == 0)
				{
//					lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"PLS_ENTER_STATUS",utility.GetUserCulture());  
//					return;

				//Jeab 22 Mar 2012
					DataSet tmpStatusCode = TIESUtility.GetStatusCodeByExceptCode(appID,enterpriseID,txtExceptioncode.Text.Trim());
					if (tmpStatusCode.Tables[0].Rows.Count < 1 )
					{
						lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"NO_STATUS_CODE",utility.GetUserCulture());  
						txtExceptioncode.Text = "";
						txtExceptionDesc.Text = "";
						return;
					}
					DataRow tmpDr = tmpStatusCode.Tables[0].Rows[0];
					txtStatusCode.Text = (String)tmpDr["status_code"];
					//Jeab 22 Mar 2012  =========> End
					ExceptionCodeDesc excepCodeDesc =  TIESUtility.GetExceptionCodeDesc(appID,enterpriseID,txtExceptioncode.Text.Trim(),txtStatusCode.Text.Trim());
					String strExcepCd = excepCodeDesc.strExceptionCode;
					String strExcepDes = excepCodeDesc.strExceptionDesc;
					if(strExcepCd == null)
					{
						lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"NO_STATUS_CODE",utility.GetUserCulture());  
						txtExceptioncode.Text = "";
						return;
					}
					else
					{
						txtExceptionDesc.Text = strExcepDes;
					}
				}
			}
			else
			{
				txtExceptionDesc.Text="";
			}
		}

		private void txtStatusDateTime_TextChanged(object sender, System.EventArgs e)
		{
			if((bool)ViewState["isTextChanged"] == false)
			{
				ChangeSUState();
				ViewState["isTextChanged"] = true;
			}
		}

		private void txtPersonInchrg_TextChanged(object sender, System.EventArgs e)
		{
			if((bool)ViewState["isTextChanged"] == false)
			{
				ChangeSUState();
				ViewState["isTextChanged"] = true;
			}
		}

		private void txtRemarks_TextChanged(object sender, System.EventArgs e)
		{
			if((bool)ViewState["isTextChanged"] == false)
			{
				ChangeSUState();
				ViewState["isTextChanged"] = true;
			}
		}

		private void btnVehNoSrch_Click(object sender, System.EventArgs e)
		{
			if(rbtnAir.Checked)
				strRouteType = "Air";
			else if(rbtnLong.Checked)
				strRouteType = "Long";
			else if(rbtnShort.Checked)
				strRouteType = "Short";
			//String strOpenWin = "<script language=javascript>";
			String url = "RouteDetailPopup.aspx?ROUTETYPE="+strRouteType;
			//			//String features = "'dialogHeight:550px;dialogWidth:800px;dialogLeft:50;dialogTop:10;location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,titlebar=yes,toolbar=no'";
			//			String features = "'Height=550,Width=800,Left=50,Top=10,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,titlebar=yes,toolbar=no'";
			//			//strOpenWin += "window.showModalDialog('" + url+"', '',"+features +");";
			//			strOpenWin += "window.open('" + url+"', '',"+features +");";
			//			strOpenWin += "</script>";
			//			//Response.Write(strOpenWin);
			//			if(!this.IsStartupScriptRegistered("VehSrch"))
			//				this.RegisterStartupScript("VehSrch", strOpenWin);	
			ArrayList paramList = new ArrayList();
			paramList.Add(url);
			String sScript = Utility.GetScript("openWindowParam.js",paramList);
			Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);

		}

		private void btnStatusCodeSearch_Click(object sender, System.EventArgs e)
		{
			String status_type = "D";
			
			OpenWindowpage("StatusCodePopup.aspx?FORMID=ShipmentUpdateRoute&STATUSCODE="+txtStatusCode.Text+"&EXCEPTCODE="+txtExceptioncode.Text+"&STATUSTYPE="+status_type+" ");


		}

		private void btnExcepDescSrch_Click(object sender, System.EventArgs e)
		{
			OpenWindowpage("ExceptionCodePopup.aspx?FORMID=ShipmentUpdateRoute&EXCEPTIONCODE="+txtExceptioncode.Text+"&STATUSCODE="+txtStatusCode.Text);
		}
		private void OpenWindowpage(String strUrl)
		{
			ArrayList paramList = new ArrayList();
			paramList.Add(strUrl);
			String sScript = Utility.GetScript("openWindowParam.js",paramList);
			Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
		}

		protected void DbComboPathCode_OnSelectedIndexChanged(object sender, System.EventArgs e)
		{

			String strPathCode = DbComboPathCode.Value;
			String strPathDesc=null;
			strPathDesc = TIESUtility.GetPathCodeDesc(appID,enterpriseID,strPathCode);
			txtPathDesc.Text = 	strPathDesc;

			lblErrorMsg.Text="";
			dbFlightVehicleNo.Text="";dbFlightVehicleNo.Value="";
			txtDriverName.Text="";
			txtDeptrDtTime.Text="";
			ClearConsignment();
		}

		private void dgShipRoute_SelectedIndexChanged(object sender, System.EventArgs e)
		{
		
		}

		private void dbFlightVehicleNo_SelectedItemChanged(object sender, System.EventArgs e)
		{
			lblErrorMsg.Text="";
			DbComboDlvryDt.Text="";DbComboDlvryDt.Value="";			
			txtDriverName.Text="";
			txtDeptrDtTime.Text="";
			ClearConsignment();
		}

		private void DbComboDlvryDt_SelectedItemChanged(object sender, System.EventArgs e)
		{
			String strDelPathCode=DbComboPathCode.Value;;
			String strFlightVehicleNo=dbFlightVehicleNo.Value;;
			String strManifestedDate=DbComboDlvryDt.Value;		
			String strDriverFlightAWB="";			
			String strDepartureDate="";
	
			ClearConsignment();

			DataSet dsDMRecord=ReadDMRecord(strDelPathCode,strFlightVehicleNo,strManifestedDate);
				
			if (dsDMRecord.Tables[0].Rows.Count<=0)
			{
				lblErrorMsg.Text= Utility.GetLanguageText(ResourceType.UserMessage,"NO_RECORD_FOUND_PATH",utility.GetUserCulture());
				return;
			}			
			DataRow drEach=dsDMRecord.Tables[0].Rows[0];
			if ((drEach["awb_driver_name"] != null) && (drEach["awb_driver_name"].ToString() != ""))
			{
				strDriverFlightAWB = (String) drEach["awb_driver_name"];
			}
			txtDriverName.Text=strDriverFlightAWB;
			
			if ((drEach["departure_datetime"] != null) && (drEach["departure_datetime"].ToString() != ""))
			{
				DateTime dtDepartDate = (DateTime) drEach["departure_datetime"];
				strDepartureDate = dtDepartDate.ToString("dd/MM/yyyy HH:mm");
			}
			txtDeptrDtTime.Text=strDepartureDate;
		}

		private DataSet ReadDMRecord(String strDelPathCode, String strFlightVehicleNo, String strManifestedDate)
		{
			lblErrorMsg.Text="";
			DataSet dsQuery =GetAssignedDM();			
			DataRow drDMNew = dsQuery.Tables[0].NewRow();

			drDMNew["Path_Code"]=strDelPathCode;
			if (strFlightVehicleNo !="")
				drDMNew["Flight_Vehicle_No"]=strFlightVehicleNo;
			if (strManifestedDate !="")
			{
				drDMNew["Delivery_Manifest_DateTime"]=System.DateTime.ParseExact(strManifestedDate,"dd/MM/yyyy HH:mm",null);				
			}
			dsQuery.Tables[0].Rows.Add(drDMNew);
			DataSet dsDMRec=DeliveryManifestMgrDAL.DelManifestRecord(appID,enterpriseID,dsQuery);

			return dsDMRec;
		}

		private DataSet GetAssignedDM()
		{
			DataTable dtDM = new DataTable(); 
			dtDM.Columns.Add(new DataColumn("Path_Code", typeof(string)));
			dtDM.Columns.Add(new DataColumn("Flight_Vehicle_No", typeof(string)));
			dtDM.Columns.Add(new DataColumn("Delivery_Manifest_DateTime", typeof(DateTime)));
			dtDM.Columns.Add(new DataColumn("AWB_Driver_Name", typeof(string)));
			dtDM.Columns.Add(new DataColumn("Line_Haul_Cost", typeof(Decimal)));
			dtDM.Columns.Add(new DataColumn("Departure_DateTime", typeof(DateTime)));
			dtDM.Columns.Add(new DataColumn("Remark", typeof(string)));

			DataSet dsDM = new DataSet();
			dsDM.Tables.Add(dtDM);
			
			return  dsDM;				
		}

		private void ClearConsignment()
		{	
			if (m_dsShpmntUpdtRoute==null)
			{
				return;
			}
			m_dsShpmntUpdtRoute.Tables[0].Rows.Clear();
			BindGrid();
		}
	
		private void ClearDbCombos()
		{
			DbComboPathCode.Text="";DbComboPathCode.Value="";
			dbFlightVehicleNo.Text="";dbFlightVehicleNo.Value="";
			DbComboDlvryDt.Text="";DbComboDlvryDt.Value="";
			txtPathDesc.Text="";

		}

		private   string   CheckStatusCode(string STCode)
		{
			 ValidST  = "Y";
			if((bool)ViewState["isTextChanged"] == false)
			{
				ChangeSUState();
				ViewState["isTextChanged"] = true;
			}

			if(txtStatusCode.Text.Length > 0)
			{
				//Jeab 22 Mar 2012
				bool isExists = false;
				isExists = TIESUtility.IsStatusCodeExist(appID,enterpriseID,txtStatusCode.Text.Trim(),userID);
				if(isExists)
				{				
				//Jeab 22 Mar 2012  =========> End
				String strType = "D";
				StatusCodeDesc statusCodeDesc =  TIESUtility.GetStatusCodeDesc(appID,enterpriseID,txtStatusCode.Text.Trim(),strType);
				String strStatusCd = statusCodeDesc.strStatusCode;
				String strStatusDes = statusCodeDesc.strStatusDesc;
				String strAuto_Location = statusCodeDesc.strAuto_Location;
					if(strStatusCd == null)
					{
						lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"STATUS_PRESENT",utility.GetUserCulture());  
						//txtStatusCode.Text = "";
						ValidST = "N";
						return ValidST;
					}
					else
					{
						txtStatusDesc.Text = strStatusDes;
						//BY X FEB 06 08 ����ҡ�൵���鴹�� �ѹ����� AUTO PLC = N �������� User's Location �Ш��
						//					StatusCodeDesc statusCodeDesc1 =  TIESUtility.GetStatusCodeInfo(appID,enterpriseID,txtStatusCode.Text.Trim());
						//					String strAuto_Location = statusCodeDesc1.strAuto_Location;

						if(strAuto_Location!=null)
						{
							if(strAuto_Location.Equals("Y"))
							{
								com.common.classes.User user = com.common.RBAC.RBACManager.GetUser(utility.GetAppID(), utility.GetEnterpriseID(), utility.GetUserID());
								this.txtLocation.Text = user.UserLocation;
							}
							else
								this.txtLocation.Text = "";
						}
						else
							this.txtLocation.Text = "";

					}
				}
				//Jeab 22 Mar 2012
				else 
				{
					lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"STATUS_PRESENT",utility.GetUserCulture());
					txtStatusDesc.Text = "";
					ValidST = "N";
					return ValidST;
				}
				//Jeab 22 Mar 2012  =========> End
			}
			else
			{
				txtStatusDesc.Text="";
			}

			return ValidST ;
		}

	}
}
