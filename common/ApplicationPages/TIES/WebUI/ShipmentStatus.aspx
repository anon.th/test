<%@ Page language="c#" Codebehind="ShipmentStatus.aspx.cs" AutoEventWireup="false" Inherits="TIES.WebUI.ShipmentStatus" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>ShipmentStatus</title>
		<LINK rel="stylesheet" type="text/css" href="css/Styles.css" name="stylesheet">
		<meta name="GENERATOR" content="Microsoft Visual Studio 7.0">
		<meta name="CODE_LANGUAGE" content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
	</HEAD>
	<body onunload="window.opener.opener.top.displayBanner.fnCloseAll(2);" MS_POSITIONING="flowlayout">
		<form id="ShipmentStatus" method="post" runat="server">
			<table border="0" cellSpacing="1" cellPadding="1" width="794" runat="server">
				<tr>
					<td>
						<TABLE id="Table2" border="0" cellSpacing="1" cellPadding="1" width="100%" runat="server">
							<TR>
								<TD width="90" align="right"><asp:label style="WHITE-SPACE: nowrap" id="lblBookNo" runat="server" CssClass="tableLabel"
										Width="70px">Booking Number</asp:label></TD>
								<td class="tableLabel">:</td>
								<TD width="90"><asp:label id="lblDisBookNo" runat="server" CssClass="tableField" Width="70px"></asp:label></TD>
								<TD width="1">&nbsp;</TD>
								<TD width="90" align="right"><asp:label style="WHITE-SPACE: nowrap" id="lblConsgNo" runat="server" CssClass="tableLabel"
										Width="70px">Consignment No</asp:label></TD>
								<td class="tableLabel">:</td>
								<TD><asp:label style="WHITE-SPACE: nowrap" id="lblDispConsgNo" runat="server" CssClass="tableField"></asp:label></TD>
								<td width="1">&nbsp;</td>
								<TD width="90" align="right"><asp:label style="WHITE-SPACE: nowrap" id="Label14" runat="server" CssClass="tableLabel" Width="108px">Customer Ref. No</asp:label></TD>
								<td class="tableLabel">:</td>
								<TD width="100"><asp:label style="WHITE-SPACE: nowrap" id="lblDispRefNo" runat="server" CssClass="tableField"></asp:label></TD>
							</TR>
							<TR>
								<TD align="right"><asp:label style="WHITE-SPACE: nowrap" id="lblBookDateTime" runat="server" CssClass="tableLabel">Booking D/T</asp:label></TD>
								<td class="tableLabel">:</td>
								<TD><asp:label style="WHITE-SPACE: nowrap" id="lblDisBookDateTime" runat="server" CssClass="tableField"></asp:label></TD>
								<td>&nbsp;</td>
								<TD align="right"><asp:label style="WHITE-SPACE: nowrap" id="Label1" runat="server" CssClass="tableLabel" Width="70px">Est. Del. D/T</asp:label></TD>
								<td class="tableLabel">:</td>
								<TD><asp:label style="WHITE-SPACE: nowrap" id="lblDisEstDateTime" runat="server" CssClass="tableField"
										Width="96"></asp:label></TD>
								<td>&nbsp;</td>
								<TD align="right"><asp:label style="WHITE-SPACE: nowrap" id="lblShipmentType" runat="server" CssClass="tableLabel">Service Type</asp:label></TD>
								<td class="tableLabel">:</td>
								<TD><asp:label style="WHITE-SPACE: nowrap" id="lblDispShipmentType" runat="server" CssClass="tableField"></asp:label></TD>
							</TR>
							<TR>
								<TD align="right"><asp:label style="WHITE-SPACE: nowrap" id="lblOrigin" runat="server" CssClass="tableLabel">Origin Province</asp:label></TD>
								<td class="tableLabel">:</td>
								<TD><asp:label style="WHITE-SPACE: nowrap" id="lblDispOrigin" runat="server" CssClass="tableField"></asp:label></TD>
								<td>&nbsp;</td>
								<TD align="right"><asp:label style="WHITE-SPACE: nowrap" id="lblDestination" runat="server" CssClass="tableLabel"
										Width="70px">Dest. Province</asp:label></TD>
								<td class="tableLabel">:</td>
								<TD><asp:label id="lblDispDestination" runat="server" CssClass="tableField" Width="164px"></asp:label></TD>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
							</TR>
							<TR>
								<TD align="right"><asp:label id="lblPayerCode" runat="server" CssClass="tableLabel">Payer ID</asp:label></TD>
								<td class="tableLabel">:</td>
								<TD><asp:label style="WHITE-SPACE: nowrap" id="lblDispPayercode" runat="server" CssClass="tableField"></asp:label></TD>
								<td>&nbsp;</td>
								<TD align="right"><asp:label style="WHITE-SPACE: nowrap" id="lblPayerName" runat="server" CssClass="tableLabel"
										Width="70px">Payer Name</asp:label></TD>
								<td class="tableLabel">:</td>
								<TD class="tableLabel" colSpan="5"><asp:label style="WHITE-SPACE: nowrap" id="lblDispPayerName" runat="server" CssClass="tableField"
										Width="165px"></asp:label></TD>
							</TR>
							<TR>
								<TD colSpan="11">&nbsp;
									<asp:checkbox id="chkExpandPackageDetail" runat="server" CssClass="tableLabel" Text="Expand Package Details."
										AutoPostBack="True"></asp:checkbox></TD>
							</TR>
						</TABLE>
					</td>
				</tr>
				<tr>
					<td><asp:label id="lblErrorMsg" Width="750px" ForeColor="Red" Runat="server"></asp:label></td>
				</tr>
				<tr>
					<td><asp:datagrid id="dgShipment" runat="server" Width="100%" OnItemDataBound="OnDataBound_Shipment"
							AutoGenerateColumns="False">
							<Columns>
								<asp:TemplateColumn>
									<HeaderStyle CssClass="gridHeading" VerticalAlign="Top"></HeaderStyle>
									<ItemStyle HorizontalAlign="Left" VerticalAlign="Top" CssClass="gridField"></ItemStyle>
									<HeaderTemplate>
										<table cellpadding="2" cellspacing="0" border="0" width="100px">
											<tr>
												<td>
													<asp:Label style="white-space: nowrap;" CssClass="gridLabelHeading" Runat="server" Text='<%#strDT%>' ID="Label2">
													</asp:Label>
												</td>
											</tr>
										</table>
									</HeaderTemplate>
									<ItemTemplate>
										<table cellpadding="2" cellspacing="0" border="0" width="100px">
											<tr>
												<td>
													<asp:Label style="white-space: nowrap;" CssClass="gridLabelNonBorder" ID="lblStatusDateTime" Text='<%#DataBinder.Eval(Container.DataItem,"tracking_datetime","{0:dd/MM/yyyy HH:mm}")%>' Runat="server">
													</asp:Label>
												</td>
											</tr>
										</table>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn>
									<HeaderStyle VerticalAlign="Top" CssClass="gridHeading"></HeaderStyle>
									<ItemStyle HorizontalAlign="Left" VerticalAlign="Top" CssClass="gridField"></ItemStyle>
									<HeaderTemplate>
										<table cellpadding="2" cellspacing="0" border="0" width="600px">
											<tr>
												<td width="100px" style="border-right: white 1px solid; border-bottom: white 1px solid; border-color: #ffffff">
													<asp:Label CssClass="gridLabelHeading" Runat="server" Text='<%#strStatus%>' ID="Label3">
													</asp:Label>
												</td>
												<td width="470px" style="border-bottom: white 1px solid; border-color: #ffffff">
													<asp:Label CssClass="gridLabelHeading" Runat="server" Text='<%#strStatusDSC%>' ID="Label4">
													</asp:Label>
												</td>
											</tr>
											<tr>
												<td style="border-right: white 1px solid; border-bottom: white 1px solid; border-color: #ffffff">
													<asp:Label CssClass="gridLabelHeading" Runat="server" Text='<%#strExcep%>' ID="Label5">
													</asp:Label>
												</td>
												<td style="border-bottom: white 1px solid; border-color: #ffffff">
													<asp:Label CssClass="gridLabelHeading" Runat="server" Text='<%#strExcepDSC%>' ID="Label6">
													</asp:Label>
												</td>
											</tr>
											<tr>
												<td style="border-right: white 1px solid; border-color: #ffffff">&nbsp;</td>
												<td>
													<asp:Label CssClass="gridLabelHeading" Runat="server" Text='<%#strRemarks%>' ID="Label7">
													</asp:Label></td>
											</tr>
										</table>
									</HeaderTemplate>
									<ItemTemplate>
										<table cellpadding="2" cellspacing="0" border="0" width="600px">
											<tr>
												<td width="100px">
													<asp:Label CssClass="gridLabelNonBorder" ID="lblStatusCode" Text='<%#DataBinder.Eval(Container.DataItem,"status_code")%>' Runat="server">
													</asp:Label>
												</td>
												<td width="470px">
													<asp:Label CssClass="gridLabelNonBorder" ID="lblStatusDesc" Text='<%#DataBinder.Eval(Container.DataItem,"status_description")%>' Runat="server">
													</asp:Label>
												</td>
											</tr>
											<tr>
												<td>
													<asp:Label CssClass="gridLabelNonBorder" ID="lblExcepCode" Text='<%#DataBinder.Eval(Container.DataItem,"exception_code")%>' Runat="server">
													</asp:Label>
												</td>
												<td>
													<asp:Label CssClass="gridLabelNonBorder" ID="lblExcepDesc" Text='<%#DataBinder.Eval(Container.DataItem,"exception_description")%>' Runat="server">
													</asp:Label>
												</td>
											</tr>
											<tr>
												<td></td>
												<td>
													<asp:Label CssClass="gridLabelNonBorder" ID="lblConsgnee" Runat="server">Consignee Name: <%#DataBinder.Eval(Container.DataItem,"consignee_name")%>
														</asp:Label></td>
											</tr>
											<tr>
												<td></td>
												<td>
													<asp:Label CssClass="gridLabelNonBorder" ID="lblRemk" Runat="server">Remark: <%#DataBinder.Eval(Container.DataItem,"remarks")%>
														</asp:Label></td>
											</tr>
										</table>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn>
									<HeaderStyle CssClass="gridHeading" VerticalAlign="Top"></HeaderStyle>
									<ItemStyle HorizontalAlign="Center" VerticalAlign="Top" CssClass="gridField"></ItemStyle>
									<HeaderTemplate>
										<table cellpadding="2" cellspacing="0" border="0" width="70px">
											<tr>
												<td>
													<asp:Label CssClass="gridLabelHeading" Runat="server" Text='<%#strLocation%>' ID="Label8">
													</asp:Label></td>
											</tr>
											<tr>
											</tr>
											<tr>
											</tr>
										</table>
									</HeaderTemplate>
									<ItemTemplate>
										<table cellpadding="0" cellspacing="0" border="0">
											<tr>
												<td>
													<asp:Label CssClass="gridLabelNonBorder" style="white-space: nowrap;" ID="lblLocation" Text='<%#DataBinder.Eval(Container.DataItem,"location")%>' Runat="server">
													</asp:Label></td>
											</tr>
										</table>
									</ItemTemplate>
								</asp:TemplateColumn>
							</Columns>
						</asp:datagrid></td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
