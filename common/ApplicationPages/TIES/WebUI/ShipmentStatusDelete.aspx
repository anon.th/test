<%@ Page language="c#" Codebehind="ShipmentStatusDelete.aspx.cs" AutoEventWireup="false" Inherits="TIES.WebUI.ShipmentStatusDelete" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>ShipmentStatusDelete</title>
		<LINK rel="stylesheet" type="text/css" href="css/Styles.css" name="stylesheet">
		<meta name="GENERATOR" content="Microsoft Visual Studio 7.0">
		<meta name="CODE_LANGUAGE" content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
	</HEAD>
	<body onunload="window.opener.opener.top.displayBanner.fnCloseAll(2);" MS_POSITIONING="GridLayout">
		<form id="ShipmentStatusDelete" method="post" runat="server">
			<TABLE style="Z-INDEX: 101; POSITION: absolute; TOP: 175px; LEFT: 12px" id="Table1" border="0"
				cellSpacing="1" cellPadding="1" width="840" runat="server">
				<TR>
					<TD><FONT face="Tahoma"><asp:button id="btnMBG" runat="server" onclientclick="" Text="MBG" CssClass="queryButton" Width="70px"></asp:button></FONT></TD>
				</TR>
				<TR>
					<TD><asp:datagrid id="dgShipmentDelete" runat="server" Width="870px" OnItemDataBound="OnDataBound_ShipmentDeletion"
							AutoGenerateColumns="False" OnDeleteCommand="OnDelete_ShipmentHistory" OnPageIndexChanged="OnShipmentDelete_PageChange">
							<Columns>
								<asp:ButtonColumn Text="&lt;IMG SRC='images/butt-delete.gif' border=0 title='Delete' &gt;" CommandName="Delete">
									<HeaderStyle CssClass="gridHeading"></HeaderStyle>
									<ItemStyle CssClass="gridField"></ItemStyle>
								</asp:ButtonColumn>
								<asp:TemplateColumn>
									<HeaderStyle CssClass="gridHeading" VerticalAlign="Top"></HeaderStyle>
									<ItemStyle HorizontalAlign="Center" CssClass="gridField"></ItemStyle>
									<HeaderTemplate>
										<table border="0">
											<tr>
												<td style="width:15%">
													<asp:Label style="white-space: nowrap;" CssClass="gridLabelHeading" Runat="server" Text='<%#strDelete%>' ID="Label10">
													</asp:Label>
												</td>
											</tr>
											<tr>
											</tr>
											<tr>
											</tr>
										</table>
									</HeaderTemplate>
									<ItemTemplate>
										<asp:CheckBox ID="chkboxDeleted" Enabled="False" Runat="server"></asp:CheckBox>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn>
									<HeaderStyle Width="15%" CssClass="gridHeading" VerticalAlign="Top"></HeaderStyle>
									<ItemStyle HorizontalAlign="Left" CssClass="gridField" VerticalAlign="Top"></ItemStyle>
									<HeaderTemplate>
										<table border="0">
											<tr>
												<td style="width:15%">
													<asp:Label style="white-space: nowrap;" CssClass="gridLabelHeading" Runat="server" Text='<%#strDT%>' ID="Label2">
													</asp:Label>
												</td>
											</tr>
											<tr>
											</tr>
											<tr>
											</tr>
										</table>
									</HeaderTemplate>
									<ItemTemplate>
										<table>
											<tr>
												<td>
													<asp:Label style="white-space: nowrap;" CssClass="gridLabelNonBorder" ID="lblStatusDateTime" Text='<%#DataBinder.Eval(Container.DataItem,"tracking_datetime","{0:dd/MM/yyyy HH:mm}")%>' Runat="server">
													</asp:Label>
												</td>
											</tr>
										</table>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn>
									<HeaderStyle Width="358px" CssClass="gridHeading" VerticalAlign="Top"></HeaderStyle>
									<ItemStyle HorizontalAlign="Left" Width="358px" CssClass="gridField" VerticalAlign="Top"></ItemStyle>
									<HeaderTemplate>
										<table cellpadding="0" cellspacing="0" border="0">
											<tr style="white-space: nowrap;">
												<td width="145px" style="border-bottom: azure 1px solid;border-right: azure 1px solid;">
													<asp:Label style="WHITE-SPACE: nowrap" CssClass="gridLabelHeading" Runat="server" Text='<%#strStatus%>' ID="Label3">
													</asp:Label>
												</td>
												<td width="215px" style="border-bottom: azure 1px solid;">
													<asp:Label style="WHITE-SPACE: nowrap" CssClass="gridLabelHeading" Runat="server" Text='<%#strStatusDSC%>' ID="Label4">
													</asp:Label>
												</td>
											</tr>
											<tr style="white-space: nowrap;">
												<td width="145px" style="border-bottom: azure 1px solid;border-right: azure 1px solid;">
													<asp:Label style="WHITE-SPACE: nowrap" CssClass="gridLabelHeading" Runat="server" Text='<%#strExcep%>' ID="Label5">
													</asp:Label>
												</td>
												<td width="215px" style="border-bottom: azure 1px solid;">
													<asp:Label style="WHITE-SPACE: nowrap" CssClass="gridLabelHeading" Runat="server" Text='<%#strExcepDSC%>' ID="Label6">
													</asp:Label>
												</td>
											</tr>
											<tr style="white-space: nowrap;border-bottom: azure 1px solid" class="gridLabelHeading">
												<td colspan="2">
													<table>
														<tr>
															<td>
																<asp:Label CssClass="gridLabelHeading" Runat="server" Text='<%#strRemarks%>' ID="Label7">
																</asp:Label></td>
															<td><font color='white' size="2">/</font>
															</td>
															<td>
																<asp:Label style='text-align:left' CssClass="gridLabelHeading" Runat="server" Text='<%#strRemarksDel%>' ID="Label12">
																</asp:Label></td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</HeaderTemplate>
									<ItemTemplate>
										<table border="0">
											<tr>
												<td width="145px">
													<asp:Label style="white-space: nowrap;" CssClass="gridLabelNonBorder" ID="lblStatusCode" Text='<%#DataBinder.Eval(Container.DataItem,"status_code").ToString().Replace("NULL","") %>' Runat="server">
													</asp:Label>
												</td>
												<td width="215px">
													<asp:Label style="white-space: nowrap;" CssClass="gridLabelNonBorder" ID="lblStatusDesc" Text='<%#DataBinder.Eval(Container.DataItem,"status_description").ToString().Replace("NULL","") %>' Runat="server">
													</asp:Label>
												</td>
											</tr>
											<tr>
												<td width="145px">
													<asp:Label style="white-space: nowrap;" CssClass="gridLabelNonBorder" ID="lblExcepCode" Text='<%#DataBinder.Eval(Container.DataItem,"exception_code").ToString().Replace("NULL","") %>' Runat="server">
													</asp:Label>
												</td>
												<td width="215px">
													<asp:Label style="white-space: nowrap;" CssClass="gridLabelNonBorder" ID="lblExcepDesc" Text='<%#DataBinder.Eval(Container.DataItem,"exception_description").ToString().Replace("NULL","") %>' Runat="server">
													</asp:Label>
												</td>
											</tr>
											<tr>
												<td colspan="2">
													<asp:Label style="white-space: nowrap;" CssClass="gridLabelNonBorder" ID="lblConsgnee" Runat="server">Consignee Name: <%#DataBinder.Eval(Container.DataItem,"consignee_name").ToString().Replace("NULL","")%>
													</asp:Label></td>
											</tr>
											<tr>
												<td colspan="2">
													<asp:Label style="white-space: nowrap;" CssClass="gridLabelNonBorder" ID="lblRemk" Runat="server">Remark: <%#DataBinder.Eval(Container.DataItem,"remarks").ToString().Replace("NULL","")%>
													</asp:Label></td>
											</tr>
											<tr>
												<td colspan="2">
													<asp:Label style="white-space: nowrap;" CssClass="gridLabelNonBorder" ID="lblRemkDel" Runat="server">Remarks Delete: <%#DataBinder.Eval(Container.DataItem,"delete_remark").ToString().Replace("NULL","") %>
													</asp:Label></td>
											</tr>
										</table>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn>
									<HeaderStyle HorizontalAlign="Center" Width="5%" CssClass="gridHeading" VerticalAlign="Top"></HeaderStyle>
									<ItemStyle HorizontalAlign="Center" CssClass="gridField" VerticalAlign="Top"></ItemStyle>
									<HeaderTemplate>
										<table>
											<tr>
												<td>
													<asp:Label CssClass="gridLabelHeading" Runat="server" Text='<%#strUserUpdated%>' ID="Label11">
													</asp:Label></td>
											</tr>
											<tr>
											</tr>
											<tr>
											</tr>
										</table>
									</HeaderTemplate>
									<ItemTemplate>
										<table>
											<tr>
												<td>
													<asp:Label CssClass="gridLabelNonBorder" style="white-space: nowrap;" ID="lblUserUpdated" Text='<%#DataBinder.Eval(Container.DataItem,"last_userid")%>' Runat="server">
													</asp:Label></td>
											</tr>
										</table>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn>
									<HeaderStyle HorizontalAlign="Center" Width="5%" CssClass="gridHeading" VerticalAlign="Top"></HeaderStyle>
									<ItemStyle HorizontalAlign="Center" CssClass="gridField" VerticalAlign="Top"></ItemStyle>
									<HeaderTemplate>
										<table>
											<tr>
												<td>
													<asp:Label style="white-space: nowrap;" CssClass="gridLabelHeading" Runat="server" Text='<%#strUpdateDT%>' ID="Label13">
													</asp:Label></td>
											</tr>
											<tr>
											</tr>
											<tr>
											</tr>
										</table>
									</HeaderTemplate>
									<ItemTemplate>
										<table>
											<tr>
												<td>
													<asp:Label CssClass="gridLabelNonBorder" style="white-space: nowrap;" ID="lblDTUpdated" Text='<%#DataBinder.Eval(Container.DataItem,"last_updated","{0:dd/MM/yyyy HH:mm}")%>' Runat="server">
													</asp:Label></td>
											</tr>
										</table>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn>
									<HeaderStyle HorizontalAlign="Center" Width="5%" CssClass="gridHeading" VerticalAlign="Top"></HeaderStyle>
									<ItemStyle HorizontalAlign="Center" CssClass="gridField" VerticalAlign="Top"></ItemStyle>
									<HeaderTemplate>
										<table>
											<tr>
												<td>
													<asp:Label style="white-space: nowrap;" CssClass="gridLabelHeading" Runat="server" Text='<%#strLocation%>' ID="Label8" NAME="Label8">
													</asp:Label></td>
											</tr>
											<tr>
											</tr>
											<tr>
											</tr>
										</table>
									</HeaderTemplate>
									<ItemTemplate>
										<table>
											<tr>
												<td>
													<asp:Label CssClass="gridLabelNonBorder" style="white-space: nowrap;" ID="lblLocation" Text='<%#DataBinder.Eval(Container.DataItem,"location")%>' Runat="server">
													</asp:Label></td>
											</tr>
										</table>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn>
									<HeaderStyle HorizontalAlign="Center" Width="20%" CssClass="gridHeading" VerticalAlign="Top"></HeaderStyle>
									<ItemStyle HorizontalAlign="Center" CssClass="gridField" VerticalAlign="Top"></ItemStyle>
									<HeaderTemplate>
										<table>
											<tr>
												<td style="width:15%">
													<asp:Label style="white-space: nowrap;" CssClass="gridLabelHeading" Runat="server" Text='<%#strPersonInchrage%>' ID="Label9" NAME="Label9">
													</asp:Label></td>
											</tr>
											<tr>
											</tr>
											<tr>
											</tr>
										</table>
									</HeaderTemplate>
									<ItemTemplate>
										<table>
											<tr>
												<td>
													<asp:Label CssClass="gridLabelNonBorder" style="white-space: nowrap;" ID="lblPersonInc" Text='<%#DataBinder.Eval(Container.DataItem,"person_incharge")%>' Runat="server">
													</asp:Label></td>
											</tr>
										</table>
									</ItemTemplate>
								</asp:TemplateColumn>
							</Columns>
						</asp:datagrid></TD>
				</TR>
				<TR height="40">
					<TD vAlign="bottom" align="center"><asp:button id="btnClose" runat="server" Text="Close" CssClass="queryButton" Width="70px"></asp:button></TD>
				</TR>
			</TABLE>
			<TABLE style="Z-INDEX: 103; POSITION: absolute; WIDTH: 840px; HEIGHT: 110px; TOP: 23px; LEFT: 13px"
				id="Table2" border="0" cellSpacing="1" cellPadding="1" width="840" runat="server">
				<TR>
					<TD align="right"><asp:label id="lblBookNo" runat="server" CssClass="tableLabel" Width="97px">Booking Number</asp:label></TD>
					<td class="tableLabel">:</td>
					<TD><asp:label id="lblDisBookNo" runat="server" CssClass="tableField"></asp:label></TD>
					<TD>&nbsp;</TD>
					<TD align="right"><asp:label id="lblConsgNo" runat="server" CssClass="tableLabel" Width="94px">Consignment No</asp:label></TD>
					<td class="tableLabel">:</td>
					<TD><asp:label id="lblDispConsgNo" runat="server" CssClass="tableField"></asp:label></TD>
					<td>&nbsp;</td>
					<TD align="right"><asp:label id="Label14" runat="server" CssClass="tableLabel" Width="108px">Customer Ref. No</asp:label></TD>
					<td class="tableLabel">:</td>
					<TD><asp:label id="lblDispRefNo" runat="server" CssClass="tableField"></asp:label></TD>
				</TR>
				<TR>
					<TD align="right"><asp:label id="lblBookDateTime" runat="server" CssClass="tableLabel">Booking D/T</asp:label></TD>
					<td class="tableLabel">:</td>
					<TD><asp:label id="lblDisBookDateTime" runat="server" CssClass="tableField"></asp:label></TD>
					<td>&nbsp;</td>
					<TD align="right"><asp:label id="Label1" runat="server" CssClass="tableLabel">Est. Del. D/T</asp:label></TD>
					<td class="tableLabel">:</td>
					<TD><asp:label id="lblDisEstDateTime" runat="server" CssClass="tableField"></asp:label></TD>
					<td>&nbsp;</td>
					<TD align="right"><asp:label id="lblShipmentType" runat="server" CssClass="tableLabel">Service Type</asp:label></TD>
					<td class="tableLabel">:</td>
					<TD><asp:label id="lblDispShipmentType" runat="server" CssClass="tableField"></asp:label></TD>
				</TR>
				<TR>
					<TD align="right"><asp:label id="lblOrigin" runat="server" CssClass="tableLabel">Origin Province</asp:label></TD>
					<td class="tableLabel">:</td>
					<TD><asp:label id="lblDispOrigin" runat="server" CssClass="tableField"></asp:label></TD>
					<td>&nbsp;</td>
					<TD align="right"><asp:label id="lblDestination" runat="server" CssClass="tableLabel">Dest. Province</asp:label></TD>
					<td class="tableLabel">:</td>
					<TD><asp:label id="lblDispDestination" runat="server" CssClass="tableField" Width="144px"></asp:label></TD>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</TR>
				<TR>
					<TD align="right"><asp:label id="lblPayerCode" runat="server" CssClass="tableLabel">Payer ID</asp:label></TD>
					<td class="tableLabel">:</td>
					<TD><asp:label id="lblDispPayercode" runat="server" CssClass="tableField"></asp:label></TD>
					<td>&nbsp;</td>
					<TD align="right"><asp:label id="lblPayerName" runat="server" CssClass="tableLabel">Payer Name</asp:label></TD>
					<td class="tableLabel">:</td>
					<TD><asp:label style="WHITE-SPACE: nowrap" id="lblDispPayerName" runat="server" CssClass="tableField"
							Width="226px"></asp:label></TD>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</TR>
			</TABLE>
			<table style="Z-INDEX: 101; POSITION: absolute; TOP: 145px; LEFT: 12px" id="Table3" runat="server">
				<tr>
					<td><asp:checkbox id="chkExpend" runat="server" Text="Expand Package Details." CssClass="tableLabel" AutoPostBack="true"></asp:checkbox></td>
				</tr>
				<tr>
					<td height="20"></td>
				</tr>
				<tr>
					<td><asp:label id="lblErrorMsg" Width="692px" ForeColor="Red" Runat="server"></asp:label></td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
