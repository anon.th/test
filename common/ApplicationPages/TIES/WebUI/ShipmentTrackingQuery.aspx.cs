using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.common.classes;
using com.common.util;
using com.ties.DAL;
using com.common.applicationpages;

namespace com.ties
{
	/// <summary>
	/// Summary description for ShipmentTrackingQuery.
	/// </summary>
	public class ShipmentTrackingQuery : BasePage
	{
		protected System.Web.UI.WebControls.Button btnExecuteQuery;
		protected System.Web.UI.WebControls.Button btnQuery;
		protected System.Web.UI.WebControls.Button btnCancelQry;
		protected System.Web.UI.WebControls.Label Label1;
		protected System.Web.UI.WebControls.RadioButton rbBookingDate;
		protected System.Web.UI.WebControls.RadioButton rbPickupDate;
		protected System.Web.UI.WebControls.RadioButton rbStatusDate;
		protected System.Web.UI.WebControls.RadioButton rbMonth;
		protected System.Web.UI.WebControls.RadioButton rbPeriod;
		protected System.Web.UI.WebControls.RadioButton rbDate;
		protected System.Web.UI.WebControls.Label lblYear;
		protected com.common.util.msTextBox txtYear;
		protected com.common.util.msTextBox txtPeriod;
		protected com.common.util.msTextBox txtTo;
		protected System.Web.UI.WebControls.RadioButton rbLongRoute;
		protected System.Web.UI.WebControls.RadioButton rbShortRoute;
		protected System.Web.UI.WebControls.RadioButton rbAirRoute;
		protected System.Web.UI.WebControls.Label lblConsignmentNo;
		protected System.Web.UI.WebControls.TextBox txtConsignmentNo;
		protected System.Web.UI.WebControls.Label lblStatusCode;
		protected com.common.util.msTextBox txtStatusCode;
		protected com.common.util.msTextBox txtExceptionCode;
		protected System.Web.UI.WebControls.Label lblShipmentClosed;
		protected System.Web.UI.WebControls.RadioButton rbShipmentYes;
		protected System.Web.UI.WebControls.RadioButton rbShipmentNo;
		protected System.Web.UI.WebControls.RadioButton rbInvoiceYes;
		protected System.Web.UI.WebControls.Label lblRouteCode;
		//protected com.common.util.msTextBox txtRouteCode;
		protected System.Web.UI.HtmlControls.HtmlTable tblShipmentTracking;
		protected System.Web.UI.WebControls.RadioButton rbCustomer;
		protected System.Web.UI.WebControls.RadioButton rbAgent;
		protected System.Web.UI.WebControls.Label lblPayerCode;
		protected com.common.util.msTextBox txtPayerCode;
		protected System.Web.UI.WebControls.Button btnRouteCode;
		protected System.Web.UI.WebControls.Button btnPayerCode;
		protected System.Web.UI.WebControls.Button btnStatusCode;
		protected System.Web.UI.WebControls.Button btnExceptionCode;
		protected System.Web.UI.WebControls.Label lblZipCode;
		protected com.common.util.msTextBox txtZipCode;
		protected System.Web.UI.WebControls.Button btnZipCode;
		protected System.Web.UI.WebControls.Label lblStateCode;
		protected com.common.util.msTextBox txtStateCode;
		protected System.Web.UI.WebControls.Button btnStateCode;
		protected System.Web.UI.WebControls.RadioButton rbInvoiceNo;
		protected System.Web.UI.WebControls.Label lblInvoiceIssued;
		protected System.Web.UI.WebControls.Label lblBookingType;
		//Utility utility = null;
		String m_strAppID=null;
		protected System.Web.UI.WebControls.DropDownList ddMonth;
		String m_strEnterpriseID=null;
		String m_strCulture=null;
		protected System.Web.UI.WebControls.TextBox txtPeriod2;
		protected System.Web.UI.WebControls.ValidationSummary ValidationSummary1;
		protected System.Web.UI.WebControls.Label lblErrorMessage;
		protected System.Web.UI.HtmlControls.HtmlTable tblDates;
		protected System.Web.UI.HtmlControls.HtmlTable tblPayerType;
		protected System.Web.UI.HtmlControls.HtmlTable tblRouteType;
		protected System.Web.UI.HtmlControls.HtmlTable tblDestination;
		protected System.Web.UI.HtmlControls.HtmlTable tblShipment;
		protected com.common.util.msTextBox txtDate;
		protected System.Web.UI.WebControls.Label lblDates;
		protected System.Web.UI.WebControls.Label lblPayerType;
		protected System.Web.UI.WebControls.Label lblRouteType;
		protected System.Web.UI.WebControls.Label lblDestination;
		protected System.Web.UI.WebControls.Label lblShipment;
		protected com.common.util.msTextBox txtBookingNo;
		protected System.Web.UI.WebControls.Label lblExceptionCode;
		protected System.Web.UI.WebControls.Label lblBookingNo;
		protected System.Web.UI.WebControls.DropDownList Drp_BookingType;
		protected System.Web.UI.WebControls.RadioButton rbEstDate;
		protected System.Web.UI.WebControls.Label Label4;
		protected System.Web.UI.WebControls.DropDownList ddlPayerType;
		protected Cambro.Web.DbCombo.DbCombo DbComboPathCode;
		protected System.Web.UI.WebControls.Label Label2;
		protected Cambro.Web.DbCombo.DbCombo DbComboOriginDC;
		protected System.Web.UI.WebControls.Label Label3;
		protected Cambro.Web.DbCombo.DbCombo DbComboDestinationDC;
		protected System.Web.UI.WebControls.TextBox txtBookingType;
		protected System.Web.UI.WebControls.Label lblRefNo;
		protected System.Web.UI.WebControls.TextBox txtRefNo;
		protected System.Web.UI.WebControls.RadioButton rbCheckAllYes;
		protected System.Web.UI.WebControls.RadioButton rbCheckAllNo;
		private DataView m_dvMonths;

		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings,Page.Session);
			m_strAppID = utility.GetAppID();
			m_strEnterpriseID = utility.GetEnterpriseID();
			m_strCulture = utility.GetUserCulture();

			DbComboPathCode.RegistrationKey = System.Configuration.ConfigurationSettings.AppSettings["DbComboRegKey"];
			//DbComboOriginDC.RegistrationKey = System.Configuration.ConfigurationSettings.AppSettings["DbComboRegKey"];
			//DbComboDestinationDC.RegistrationKey = System.Configuration.ConfigurationSettings.AppSettings["DbComboRegKey"];

			if(!Page.IsPostBack)
			{
				DefaultScreen();
				Session["toRefresh"]=false;
				LoadBookingTypeList();
			}
			//btnExecuteQuery.Enabled = false;

			SetDbComboServerStates();
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnQuery.Click += new System.EventHandler(this.btnQuery_Click);
			this.btnExecuteQuery.Click += new System.EventHandler(this.btnExecuteQuery_Click);
			this.rbMonth.CheckedChanged += new System.EventHandler(this.rbMonth_CheckedChanged);
			this.rbPeriod.CheckedChanged += new System.EventHandler(this.rbPeriod_CheckedChanged);
			this.rbDate.CheckedChanged += new System.EventHandler(this.rbDate_CheckedChanged);
			this.btnPayerCode.Click += new System.EventHandler(this.btnPayerCode_Click);
			this.rbLongRoute.CheckedChanged += new System.EventHandler(this.rbLongRoute_CheckedChanged);
			this.rbShortRoute.CheckedChanged += new System.EventHandler(this.rbShortRoute_CheckedChanged);
			this.rbAirRoute.CheckedChanged += new System.EventHandler(this.rbAirRoute_CheckedChanged);
			this.btnZipCode.Click += new System.EventHandler(this.btnZipCode_Click);
			this.btnStateCode.Click += new System.EventHandler(this.btnStateCode_Click);
			this.btnStatusCode.Click += new System.EventHandler(this.btnStatusCode_Click);
			this.btnExceptionCode.Click += new System.EventHandler(this.btnExceptionCode_Click);
			this.rbInvoiceYes.CheckedChanged += new System.EventHandler(this.rbInvoiceYes_CheckedChanged);
			this.rbInvoiceNo.CheckedChanged += new System.EventHandler(this.rbInvoiceNo_CheckedChanged);
			this.ID = "ShipmentTrackingQueryRpt";
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private void btnExecuteQuery_Click(object sender, System.EventArgs e)
		{

			//btnExecuteQuery.Enabled = false;
			lblErrorMessage.Text = "";
			int intChk = 0;
			intChk = ValidateData();
			if(intChk < 1)
			{
				return;
			}
			String strModuleID = Request.Params["MODID"];
			DataSet dsShipment = GetShipmentQueryData();

			String strUrl = null;
			SessionDS sessionds = new SessionDS();
				sessionds = ShipmentTrackingMgrDAL.QueryShipmentTracking(dsShipment, m_strAppID, m_strEnterpriseID, 0, 50);

			strUrl = "ShipmentTracking.aspx?FORMID=TRUE_CHECK";
			//String strFeatures = "'height=500,width=900,left=30,top=30,location=no,menubar=no,scrollbars=yes,resizable=yes,status=yes,titlebar=no,toolbar=no'";
			Session["SESSION_DS1"] = sessionds;
			Session["QUERY_DS"] = dsShipment;
			ArrayList paramList = new ArrayList();
			paramList.Add(strUrl);
			String sScript = Utility.GetScript("openParentWindow.js",paramList);
			Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);

			//check for module id before opening another web form
//			if (strModuleID.Equals("03ShipmentTracking"))
//			{
//				String strUrl = null;
//				SessionDS sessionds = new SessionDS();
//				sessionds = ShipmentTrackingMgrDAL.QueryShipmentTracking(dsShipment, m_strAppID, m_strEnterpriseID, 0, 10);
//				strUrl = "ShipmentTracking.aspx?FORMID=TRUE_CHECK";
//				//String strFeatures = "'height=500,width=900,left=30,top=30,location=no,menubar=no,scrollbars=yes,resizable=yes,status=yes,titlebar=no,toolbar=no'";
//				Session["SESSION_DS1"] = sessionds;
//				Session["QUERY_DS"] = dsShipment;
//				ArrayList paramList = new ArrayList();
//				paramList.Add(strUrl);
//				String sScript = Utility.GetScript("openParentWindow.js",paramList);
//				Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
//			}
//			else
//			{
//				String strUrl = null;
//				//String sFeatures = "'height=700,width=900,left=20,top=5,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,titlebar=yes,toolbar=no'";
//				strUrl = "ReportViewer.aspx";
//				//String strFeatures = "'height=700,width=900,left=20,top=5,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,titlebar=yes,toolbar=no'";
//				Session["FORMID"] = "ShipmentTrackingQuery";
//				Session["SESSION_DS1"] = dsShipment;
//				//OpenWindowpage(strUrl, strFeatures);
//				ArrayList paramList = new ArrayList();
//				paramList.Add(strUrl);
//				String sScript = Utility.GetScript("openParentWindow.js",paramList);
//				Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
//			}
		}

		private void OpenWindowpage(String strUrl, String strFeatures)
		{
			
			ArrayList paramList = new ArrayList();
			paramList.Add(strUrl);
			String sScript = Utility.GetScript("openParentWindow.js",paramList);
			Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
		}

		private void btnQuery_Click(object sender, System.EventArgs e)
		{
			//btnExecuteQuery.Enabled = true;
			DefaultScreen();
		}

		private DataTable CreateEmptyDataTable()
		{
			DataTable dtShipment = new DataTable();
			dtShipment.Columns.Add(new DataColumn("tran_date", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("start_date", typeof(DateTime)));
			dtShipment.Columns.Add(new DataColumn("end_date", typeof(DateTime)));
			dtShipment.Columns.Add(new DataColumn("payer_type", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("payer_code", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("route_type", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("route_code", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("zip_code", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("state_code", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("booking_type", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("consignment_no", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("booking_no", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("status_code", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("exception_code", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("shipment_closed", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("shipment_invoiced", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("shipment_history", typeof(string)));
			//#Start:PSA 2009_029 By Gwang on 13Mar09#
			dtShipment.Columns.Add(new DataColumn("ref_no",typeof(string)));
			//#End:PSA 2009_029
			dtShipment.Columns.Add(new DataColumn("UserLocation",typeof(string)));
			return dtShipment;
		}

		private int ValidateData()
		{
			if (txtYear.Text != "" )
			{
				if (Convert.ToInt32(txtYear.Text) < 2000 || Convert.ToInt32(txtYear.Text) > 2099)
				{
					//lblErrorMessage.Text = "Enter year between 2000 to 2099";
					lblErrorMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"YEAR_00_99",utility.GetUserCulture());
					return -1;
				}
			}
			return 1;
		}
		
		private DataSet GetShipmentQueryData()
		{
			DataSet dsShipment = new DataSet();
			DataTable dtShipment = CreateEmptyDataTable();
			DataRow dr = dtShipment.NewRow(); //14 columns
			string strMonth =null;
			if (rbBookingDate.Checked) 
			{
				dr[0] = "B";
			}
			if (rbPickupDate.Checked) 
			{
				dr[0] = "P";
			}
			if (rbStatusDate.Checked) 
			{
				dr[0] = "S";
			}
			if (rbMonth.Checked) 
			{
				strMonth = ddMonth.SelectedItem.Value;
				if(strMonth.Length == 1)
					strMonth = "0" + strMonth;
				if (strMonth != "" && txtYear.Text != "")
				{
					dr[1] = DateTime.ParseExact ("01/"+strMonth.ToString()+"/"+txtYear.Text, "dd/MM/yyyy", null);
					int intLastDay = 0;
					intLastDay = LastDayOfMonth(strMonth, txtYear.Text);
					dr[2] = DateTime.ParseExact (intLastDay.ToString()+"/"+strMonth.ToString()+"/"+txtYear.Text+" 23:59", "dd/MM/yyyy HH:mm", null);
				}
			}
			if (rbPeriod.Checked) 
			{
				if (txtPeriod.Text != ""  && txtTo.Text != "")
				{
					dr[1] = DateTime.ParseExact(txtPeriod.Text ,"dd/MM/yyyy",null);
					dr[2] = DateTime.ParseExact(txtTo.Text+" 23:59" ,"dd/MM/yyyy HH:mm",null);
				}
			}
			if (rbDate.Checked) 
			{
				if (txtDate.Text != "")
				{
					dr[1] = DateTime.ParseExact(txtDate.Text, "dd/MM/yyyy",null);
					dr[2] = DateTime.ParseExact(txtDate.Text+" 23:59", "dd/MM/yyyy HH:mm",null);
				}
			}
			if (rbCustomer.Checked)
			{
				dr[3] = "C";
			}
			if (rbAgent.Checked)
			{
				dr[3] = "A";
			}
			dr[4] = txtPayerCode.Text;
			if (rbLongRoute.Checked)
			{
				dr[5] = "L";
			}
			if (rbShortRoute.Checked)
			{
				dr[5] = "S";
			}
			if (rbAirRoute.Checked)
			{
				dr[5] = "A";
			}
			//dr[6] = txtRouteCode.Text;
			dr[6] = DbComboPathCode.Value;
			dr[7] = txtZipCode.Text;
			dr[8] = txtStateCode.Text;
			dr[9] = Drp_BookingType.SelectedItem.Value; //txtBookingType.Text;
			//dr[9] = ddBookingType.SelectedItem.Value;
			dr[10] = txtConsignmentNo.Text;
			dr[11] = txtBookingNo.Text;
			dr[12] = txtStatusCode.Text;
			dr[13] = txtExceptionCode.Text;
			if (rbShipmentYes.Checked)
			{
				dr[14] = "Y";
			}
			else
			{
				dr[14] = "N";
			}
			if (rbInvoiceYes.Checked)
			{
				dr[15] = "Y";
			}
			else
			{
				dr[15] = "N";
			}
			if (rbCheckAllYes.Checked)
			{
				dr[16] = "Y";
			}
			else
			{
				dr[16] = "N";
			}

			//#Start:PSA 2009_029 By Gwang on 13Mar09#			
			if (this.txtRefNo.Text.Trim() != "")
			{
				dr["ref_no"] = txtRefNo.Text;
			}
			//#End:PSA 2009_029
			Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings,HttpContext.Current.Session);
			if(util.CheckUserRoleLocation())
			{
				dr["UserLocation"] = "";
			}
			else
			{
				dr["UserLocation"] = util.UserLocation();
			}


			dtShipment.Rows.Add(dr);
			dsShipment.Tables.Add(dtShipment);
			return dsShipment;
		}

		private DataView CreateMonths(bool showNilOption)
		{
			DataTable dtMonths = new DataTable();
			dtMonths.Columns.Add(new DataColumn("Text", typeof(string)));
			dtMonths.Columns.Add(new DataColumn("StringValue", typeof(string)));
			ArrayList listMonthTxt = Utility.GetCodeValues(m_strAppID,m_strCulture, "months", CodeValueType.StringValue);
			if(showNilOption)
			{
				DataRow drNilRow = dtMonths.NewRow();
				drNilRow[0] = "";
				drNilRow[1] = "";
				dtMonths.Rows.Add(drNilRow);
			}
			foreach(SystemCode typeSysCode in listMonthTxt)
			{
				DataRow drEach = dtMonths.NewRow();
				drEach[0] = typeSysCode.Text;
				drEach[1] = typeSysCode.StringValue;
				dtMonths.Rows.Add(drEach);
			}
			DataView dvMonths = new DataView(dtMonths);
			return dvMonths;
		}

		private void BindMonths(DataView dvMonths)
		{
			ddMonth.DataSource = dvMonths;
			ddMonth.DataTextField = "Text";
			ddMonth.DataValueField = "StringValue";
			ddMonth.DataBind();	
		}

		private int LastDayOfMonth(String strMonth, String strYear)
		{
			int intLastDay = 0;
			switch (strMonth)
			{
				case "01":
				case "03":
				case "05":
				case "07":
				case "08":
				case "10":
				case "12":
					intLastDay = 31;
					break;
				case "04":
				case "06":
				case "09":
				case "11":
					intLastDay = 30;
					break;
				case "02":
					intLastDay = 28;
					break;
			}
			return intLastDay;
		}

		private void DefaultScreen()
		{
			rbBookingDate.Checked = true;
			rbPickupDate.Checked = false;
			rbStatusDate.Checked = false;
			rbMonth.Checked = true;
			rbPeriod.Checked = false;
			rbDate.Checked = false;
			rbCustomer.Checked = true;
			rbAgent.Checked = false;
			rbInvoiceYes.Checked = false;
			rbInvoiceNo.Checked = true;
			ddMonth.Enabled = true;
			txtYear.Text = null;
			txtYear.Enabled = true;
			txtPeriod.Text = null;
			txtPeriod.Enabled = false;
			txtTo.Text = null;
			txtTo.Enabled = false;
			txtDate.Text = null;
			txtDate.Enabled = false;
			txtPayerCode.Text = null;
			//txtRouteCode.Text = null;
			DbComboPathCode.Text="";
			DbComboPathCode.Value="";


			txtZipCode.Text = null;
			txtStateCode.Text = null;
			txtConsignmentNo.Text = null;
			txtBookingNo.Text = null;
			txtStatusCode.Text = null;
			txtExceptionCode.Text = null;
			m_dvMonths = CreateMonths(true);
			BindMonths(m_dvMonths);
			lblErrorMessage.Text = "";
		}

		private void rbMonth_CheckedChanged(object sender, System.EventArgs e)
		{
			ddMonth.Enabled = true;
			txtYear.Text = null;
			txtYear.Enabled = true;
			txtPeriod.Text = null;
			txtPeriod.Enabled = false;
			txtTo.Text = null;
			txtTo.Enabled = false;
			txtDate.Text = null;
			txtDate.Enabled = false;
		}

		private void rbPeriod_CheckedChanged(object sender, System.EventArgs e)
		{
			ddMonth.Enabled = false;
			txtYear.Text = null;
			txtYear.Enabled = false;
			txtPeriod.Text = null;
			txtPeriod.Enabled = true;
			txtTo.Text = null;
			txtTo.Enabled = true;
			txtDate.Text = null;
			txtDate.Enabled = false;
		}

		private void rbDate_CheckedChanged(object sender, System.EventArgs e)
		{
			ddMonth.Enabled = false;
			txtYear.Text = null;
			txtYear.Enabled = false;
			txtPeriod.Text = null;
			txtPeriod.Enabled = false;
			txtTo.Text = null;
			txtTo.Enabled = false;
			txtDate.Text = null;
			txtDate.Enabled = true;
		}

		private void rbLongRoute_CheckedChanged(object sender, System.EventArgs e)
		{
			//txtRouteCode.Text = null;
			DbComboPathCode.Text="";
			DbComboPathCode.Value="";
		}

		private void rbShortRoute_CheckedChanged(object sender, System.EventArgs e)
		{
			//txtRouteCode.Text = null;		
			DbComboPathCode.Text="";
			DbComboPathCode.Value="";
		}

		private void rbAirRoute_CheckedChanged(object sender, System.EventArgs e)
		{
			//txtRouteCode.Text = null;
			DbComboPathCode.Text="";
			DbComboPathCode.Value="";
		}

		private void rbCustomer_CheckedChanged(object sender, System.EventArgs e)
		{
			txtPayerCode.Text = null;
		}

		private void rbAgent_CheckedChanged(object sender, System.EventArgs e)
		{
			txtPayerCode.Text = null;
		}

		private void rbInvoiceYes_CheckedChanged(object sender, System.EventArgs e)
		{
		
		}

		private void rbInvoiceNo_CheckedChanged(object sender, System.EventArgs e)
		{
		
		}

		private void btnPayerCode_Click(object sender, System.EventArgs e)
		{
			if (rbCustomer.Checked)
			{
				String sUrl = "CustomerPopup.aspx?FORMID="+"ShipmentTrackingQuery"+"&CUSTID_TEXT="+txtPayerCode.Text.Trim().ToString();
				ArrayList paramList = new ArrayList();
				paramList.Add(sUrl);
				String sScript = Utility.GetScript("openParentWindow.js",paramList);
				Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
			}
			else
			{
				String sUrl = "AgentPopup.aspx?FORMID=ShipmentTrackingQuery"+"&CUSTID_TEXT="+txtPayerCode.Text.Trim().ToString();
				ArrayList paramList = new ArrayList();
				paramList.Add(sUrl);
				String sScript = Utility.GetScript("openParentWindow.js",paramList);
				Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
			}
		}

		private void btnRouteCode_Click(object sender, System.EventArgs e)
		{
		}

		private void btnZipCode_Click(object sender, System.EventArgs e)
		{
			String sUrl = "ZipCodePopup.aspx?FORMID=ShipmentTrackingQuery"+"&ZIPCODE_CID="+txtZipCode.ClientID+"&ZIPCODE="+txtZipCode.Text.Trim().ToString();
			ArrayList paramList = new ArrayList();
			paramList.Add(sUrl);
			String sScript = Utility.GetScript("openPostalWindow.js",paramList); //Thosapol.y  Modify (26/06/2013)
			//String sScript = Utility.GetScript("openParentWindow.js",paramList); //Thosapol.y  Comment (26/06/2013)
			Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
		}

		private void btnStateCode_Click(object sender, System.EventArgs e)
		{
			String sUrl = "StatePopup.aspx?FORMID=ShipmentTrackingQuery"+"&STATECODE="+txtStateCode.ClientID+"&STATECODE_TEXT="+txtStateCode.Text.Trim().ToString();
			ArrayList paramList = new ArrayList();
			paramList.Add(sUrl);
			String sScript = Utility.GetScript("openParentWindow.js",paramList);
			Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
		}

		private void btnStatusCode_Click(object sender, System.EventArgs e)
		{
			String sUrl = "StatusCodePopup.aspx?FORMID=ShipmentTrackingQuery"+"&STATUSCODE="+txtStatusCode.Text+"&EXCEPTCODE="+txtExceptionCode.Text.Trim().ToString();
			ArrayList paramList = new ArrayList();
			paramList.Add(sUrl);
			String sScript = Utility.GetScript("openParentWindow.js",paramList);
			Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
		}

		private void btnExceptionCode_Click(object sender, System.EventArgs e)
		{
			String sUrl = "ExceptionCodePopup.aspx?FORMID=ShipmentTrackingQuery"+"&EXCEPTIONCODE="+txtExceptionCode.Text.Trim().ToString()+"&STATUSCODE="+txtStatusCode.Text.Trim().ToString();
			ArrayList paramList = new ArrayList();
			paramList.Add(sUrl);
			String sScript = Utility.GetScript("openParentWindow.js",paramList);
			Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
		}

		public void LoadBookingTypeList()
		{
			ListItem defItem = new ListItem();
			defItem.Text = "";
			defItem.Value = "0";
			Drp_BookingType.Items.Add(defItem);
			ArrayList systemCodes = Utility.GetCodeValues(utility.GetAppID(),utility.GetUserCulture(),"booking_type",CodeValueType.StringValue);
			foreach(SystemCode sysCode in systemCodes)
			{	
				ListItem lstItem = new ListItem();
				lstItem.Text = sysCode.Text;
				lstItem.Value = sysCode.StringValue;
				Drp_BookingType.Items.Add(lstItem);
			}

			if (Drp_BookingType.Items.Count > 0)
				Drp_BookingType.SelectedIndex = 0;
		}


		[Cambro.Web.DbCombo.ResultsMethodAttribute(true)]
		public static object DbComboPathCodeSelect(Cambro.Web.DbCombo.ServerMethodArgs args)
		{
			Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings,HttpContext.Current.Session);
			String strAppID = util.GetAppID();
			String strEnterpriseID = util.GetEnterpriseID();
			
			String strDelType = "L";			
			String strWhereClause=null;
			if(args.ServerState != null && args.ServerState.Authenticate("kdfsjgnskajh[apq2-4uhg465654435879mnbfdm cxs;q4"))
			{
				if(args.ServerState["strDeliveryType"] != null && args.ServerState["strDeliveryType"].ToString().Length > 0)
				{
					strDelType = args.ServerState["strDeliveryType"].ToString();
					if(strDelType == "S") 
					{
						strWhereClause=" and Delivery_Type in ('"+Utility.ReplaceSingleQuote(strDelType)+"','W') ";
						strWhereClause=strWhereClause+"and path_code in ( ";
						strWhereClause=strWhereClause+"select distinct delivery_route ";
						strWhereClause=strWhereClause+"from Zipcode ";
						strWhereClause=strWhereClause+"where applicationid = '"+strAppID+"' ";
						strWhereClause=strWhereClause+"and enterpriseid = '"+ strEnterpriseID +"') ";
					}
					else
						strWhereClause=" and Delivery_Type='"+Utility.ReplaceSingleQuote(strDelType)+"'";
				}				
			}
			
			DataSet dataset = com.ties.classes.DbComboDAL.PathCodeQuery(strAppID,strEnterpriseID,args, strWhereClause);	
			return dataset;
		}

		private void SetDbComboServerStates()
		{
			String strDeliveryType=null;
			if (rbLongRoute.Checked==true)
				strDeliveryType="L";
			else if (rbShortRoute.Checked==true)
				strDeliveryType="S";
			else if (rbAirRoute.Checked==true)
				strDeliveryType="A";

			Hashtable hash = new Hashtable();
			hash.Add("strDeliveryType", strDeliveryType);
			DbComboPathCode.ServerState = hash;
			DbComboPathCode.ServerStateSecretString = "kdfsjgnskajh[apq2-4uhg465654435879mnbfdm cxs;q4";
		}
	}
}
