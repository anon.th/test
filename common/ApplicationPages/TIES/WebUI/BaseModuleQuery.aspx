<%@ Page language="c#" Codebehind="BaseModuleQuery.aspx.cs" AutoEventWireup="false" Inherits="com.ties.BaseModuleQuery" %>
<%@ Register TagPrefix="dbCombo" Namespace="Cambro.Web.DbCombo" Assembly="Cambro.Web.DbCombo" %>
<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>BaseModuleQuery</title>
		<meta name="vs_showGrid" content="True">
		<LINK href="css/Styles.css" type="text/css" rel="stylesheet" name="stylesheet">
		<META content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<META content="C#" name="CODE_LANGUAGE">
		<META content="JavaScript" name="vs_defaultClientScript">
		<META content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<!--#INCLUDE FILE="msFormValidations.inc"-->
		<SCRIPT language="javascript">
		//************************************************ZipCode and State***************************************************
				var Country =-1;
				var State = -1;
				function dbCmbCountryOnSelect(Value, Text, SelectionType)
					{
						switch (SelectionType)
						{
							case 1: // The user is scrolling down the list 
									// ClinetOnSelectFunction fires each 
									// time a new item is selected.
							case 3: // The user has clicked on an item. The 
									// results disappear, and the item is 
									// selected.
							case 8: // The user has entered an exact match to 
									// one of the data items. This item is 
									// selected.
							case 9: // In a �re-query on postback� or an 
									// �auto-query on load�, both the text and 
									// value properties have matched with one 
									// of the items. This item is selected.
								if (Country != Value)
								{
									Country = Value;
									UpdateCountry();
								}
								break;
							case 2: // The user has pressed enter when an item 
									// is selected. The results disappear, and 
									// the item is selected.
								break;
							case 4: // The user has entered more text into the 
									// textbox. Another query will be run, and 
									// the selection is cleared. Nothing is 
									// selected.
							case 5: // The user has scrolled off the bottom or 
									// top of the drop-down, and cleared the 
									// current selection. Nothing is selected.
							case 6: // The user has pressed escape to clear 
									// the DbCombo, and hide the results. 
									// Nothing is selected.
							case 7: // DbCombo has evaluated the client state 
									// function, and found it to have changed. 
									// The current selection is cleared. 
									// Nothing is selected.
								if (Country != -1)
								{
									Country = -1;
									UpdateCountry();
								}
								break;
							default:
								break;
						}
					}
				    
				function dbCmbZipCodeState()
				{
					var state = new Object();
					state["Country"]=Country;
					return state;
				}

				function UpdateCountry()
				{
					if (typeof(DbComboServerExists)!='undefined')
					// This checks to make sure the DbCombo javascript 
					// file exists - if we are in down-level mode, the 
					// next line would fail.
					
						DbComboStateChanged('<%#dbCmbZipCode.UniqueID%>');
						DbComboStateChanged('<%#dbCmbState.UniqueID%>');
						// This informs a specific DbCombo that it's 
						// ClientState has changed. It will reevaluate 
						// it, and if it has changed, it will get new 
						// data from the server.
				}
				 function dbCmbStateOnSelect(Value, Text, SelectionType)
					{
						switch (SelectionType)
						{
							case 1: // The user is scrolling down the list 
									// ClinetOnSelectFunction fires each 
									// time a new item is selected.
							case 3: // The user has clicked on an item. The 
									// results disappear, and the item is 
									// selected.
							case 8: // The user has entered an exact match to 
									// one of the data items. This item is 
									// selected.
							case 9: // In a �re-query on postback� or an 
									// �auto-query on load�, both the text and 
									// value properties have matched with one 
									// of the items. This item is selected.
								if (State != Value)
								{
									State = Value;
									UpdateState();
								}
								break;
							case 2: // The user has pressed enter when an item 
									// is selected. The results disappear, and 
									// the item is selected.
								break;
							case 4: // The user has entered more text into the 
									// textbox. Another query will be run, and 
									// the selection is cleared. Nothing is 
									// selected.
							case 5: // The user has scrolled off the bottom or 
									// top of the drop-down, and cleared the 
									// current selection. Nothing is selected.
							case 6: // The user has pressed escape to clear 
									// the DbCombo, and hide the results. 
									// Nothing is selected.
							case 7: // DbCombo has evaluated the client state 
									// function, and found it to have changed. 
									// The current selection is cleared. 
									// Nothing is selected.
								if (State != -1)
								{
									State = -1;
									UpdateState();
								}
								break;
							default:
								break;
						}
					}
			
				function dbCmbStateCodeState()
				{
					var state = new Object();
					state["Country"]=Country;
					return state;
				}    
				function dbCmbStateNameState()
				{
					var state = new Object();
					state["State"]=State;
					return state;
				}

				function UpdateState()
				{
					if (typeof(DbComboServerExists)!='undefined')
					// This checks to make sure the DbCombo javascript 
					// file exists - if we are in down-level mode, the 
					// next line would fail.
					
						DbComboStateChanged('<%#dbCmbStateName.UniqueID%>');
						// This informs a specific DbCombo that it's 
						// ClientState has changed. It will reevaluate 
						// it, and if it has changed, it will get new 
						// data from the server.
				}
				    
			//*************************************Customer Profile*********************************
			var Payerid = -1;
			function dbCmbPayerIdOnSelect(Value, Text, SelectionType)
					{
						switch (SelectionType)
						{
							case 1: // The user is scrolling down the list 
									// ClinetOnSelectFunction fires each 
									// time a new item is selected.
							case 3: // The user has clicked on an item. The 
									// results disappear, and the item is 
									// selected.
							case 8: // The user has entered an exact match to 
									// one of the data items. This item is 
									// selected.
							case 9: // In a �re-query on postback� or an 
									// �auto-query on load�, both the text and 
									// value properties have matched with one 
									// of the items. This item is selected.
								if (Payerid != Value)
								{
									Payerid = Value;
									UpdatePayerName();
								}
								break;
							case 2: // The user has pressed enter when an item 
									// is selected. The results disappear, and 
									// the item is selected.
								break;
							case 4: // The user has entered more text into the 
									// textbox. Another query will be run, and 
									// the selection is cleared. Nothing is 
									// selected.
							case 5: // The user has scrolled off the bottom or 
									// top of the drop-down, and cleared the 
									// current selection. Nothing is selected.
							case 6: // The user has pressed escape to clear 
									// the DbCombo, and hide the results. 
									// Nothing is selected.
							case 7: // DbCombo has evaluated the client state 
									// function, and found it to have changed. 
									// The current selection is cleared. 
									// Nothing is selected.
								if (Payerid != -1)
								{
									Payerid = -1;
									UpdatePayerName();
								}
								break;
							default:
								break;
						}
					}
				    
				function dbCmbPayerNameState()
				{
					var state = new Object();
					state["Payerid"]=Payerid;
					return state;
				}

				function UpdatePayerName()
				{
					if (typeof(DbComboServerExists)!='undefined')
					// This checks to make sure the DbCombo javascript 
					// file exists - if we are in down-level mode, the 
					// next line would fail.
					
						DbComboStateChanged('<%#dbCmbPayerName.UniqueID%>');
								// This informs a specific DbCombo that it's 
						// ClientState has changed. It will reevaluate 
						// it, and if it has changed, it will get new 
						// data from the server.
				}
					    
			//*************************************Agent Profile*********************************
			var Agentid = -1;
			function dbCmbAgentIdOnSelect(Value, Text, SelectionType)
					{
						switch (SelectionType)
						{
							case 1: // The user is scrolling down the list 
									// ClinetOnSelectFunction fires each 
									// time a new item is selected.
							case 3: // The user has clicked on an item. The 
									// results disappear, and the item is 
									// selected.
							case 8: // The user has entered an exact match to 
									// one of the data items. This item is 
									// selected.
							case 9: // In a �re-query on postback� or an 
									// �auto-query on load�, both the text and 
									// value properties have matched with one 
									// of the items. This item is selected.
								if (Agentid != Value)
								{
									Agentid = Value;
									UpdateAgentName();
								}
								break;
							case 2: // The user has pressed enter when an item 
									// is selected. The results disappear, and 
									// the item is selected.
								break;
							case 4: // The user has entered more text into the 
									// textbox. Another query will be run, and 
									// the selection is cleared. Nothing is 
									// selected.
							case 5: // The user has scrolled off the bottom or 
									// top of the drop-down, and cleared the 
									// current selection. Nothing is selected.
							case 6: // The user has pressed escape to clear 
									// the DbCombo, and hide the results. 
									// Nothing is selected.
							case 7: // DbCombo has evaluated the client state 
									// function, and found it to have changed. 
									// The current selection is cleared. 
									// Nothing is selected.
								if (Agentid != -1)
								{
									Agentid = -1;
									UpdateAgentName();
								}
								break;
							default:
								break;
						}
					}
				    
				function dbCmbAgentNameState()
				{
					var state = new Object();
					state["Agentid"]=Agentid;
					return state;
				}

				function UpdateAgentName()
				{
					if (typeof(DbComboServerExists)!='undefined')
					// This checks to make sure the DbCombo javascript 
					// file exists - if we are in down-level mode, the 
					// next line would fail.
					
						DbComboStateChanged('<%#dbCmbAgentName.UniqueID%>');
								// This informs a specific DbCombo that it's 
						// ClientState has changed. It will reevaluate 
						// it, and if it has changed, it will get new 
						// data from the server.
				}
    
		</SCRIPT>
	</HEAD>
	<BODY onunload="window.top.displayBanner.fnCloseAll(0);" MS_POSITIONING="GridLayout">
		<FORM id="BaseModuleQuery" method="post" runat="server">
			<asp:label id="lblTitle" style="Z-INDEX: 101; LEFT: 14px; POSITION: absolute; TOP: 11px" runat="server"
				Height="28px" Width="674px" CssClass="maintitleSize">Title</asp:label>
			<DIV id="divDate" style="CLEAR: both; Z-INDEX: 112; LEFT: 6px; WIDTH: 411px; POSITION: relative; TOP: 107px; HEIGHT: 191px"
				runat="server">
				<FIELDSET style="WIDTH: 408px; HEIGHT: 177px"><LEGEND><asp:label id="Label3" CssClass="tableHeadingFieldset" Runat="server">Booking Date</asp:label></LEGEND>
					<TABLE id="tbDate" style="WIDTH: 98.23%; HEIGHT: 147px" runat="server">
						<TR height="5">
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
						</TR>
						<TR height="27">
							<TD colSpan="3"><asp:radiobutton id="rbMonth" runat="server" Height="22px" Width="100%" CssClass="tableRadioButton"
									AutoPostBack="True" GroupName="EnterDate" Checked="True" Text="Monthly"></asp:radiobutton></TD>
							<TD colSpan="4"><asp:dropdownlist id="ddMonth" tabIndex="1" runat="server" Width="100%" CssClass="textField"></asp:dropdownlist></TD>
							<TD colSpan="3"></TD>
							<TD colSpan="3"><asp:label id="lblYear" runat="server" Height="22px" Width="100%" CssClass="tableLabel">Year&nbsp;&nbsp;</asp:label></TD>
							<TD colSpan="4"><cc1:mstextbox id="txtYear" tabIndex="2" runat="server" Width="100%" CssClass="textField" MaxLength="5"
									TextMaskType="msNumeric" NumberMaxValue="2999" NumberMinValue="1" NumberPrecision="4"></cc1:mstextbox></TD>
							<TD colSpan="3"></TD>
						</TR>
						<TR height="27">
							<TD colSpan="3"><asp:radiobutton id="rbPeriod" tabIndex="3" runat="server" Height="22px" Width="100%" CssClass="tableRadioButton"
									AutoPostBack="True" GroupName="EnterDate" Text="Period"></asp:radiobutton></TD>
							<TD colSpan="4"><cc1:mstextbox id="txtPeriod" tabIndex="4" runat="server" Width="100%" CssClass="textField" MaxLength="10"
									TextMaskType="msDate" TextMaskString="99/99/9999"></cc1:mstextbox></TD>
							<TD colSpan="3"></TD>
							<TD colSpan="3"><asp:label id="lblTo" runat="server" Height="22px" Width="100%" CssClass="tableLabel">To&nbsp;&nbsp;</asp:label></TD>
							<TD colSpan="4"><cc1:mstextbox id="txtTo" tabIndex="5" runat="server" Width="100%" CssClass="textField" MaxLength="10"
									TextMaskType="msDate" TextMaskString="99/99/9999"></cc1:mstextbox></TD>
							<TD colSpan="3"></TD>
						</TR>
						<TR height="27">
							<TD colSpan="4"><asp:radiobutton id="rbDate" tabIndex="6" runat="server" Height="22px" Width="56px" CssClass="tableRadioButton"
									AutoPostBack="True" GroupName="EnterDate" Text="Daily"></asp:radiobutton></TD>
							<TD colSpan="4"><cc1:mstextbox id="txtDate" tabIndex="7" runat="server" Width="100%" CssClass="textField" MaxLength="10"
									TextMaskType="msDate" TextMaskString="99/99/9999"></cc1:mstextbox></TD>
							<TD colSpan="12"></TD>
						</TR>
					</TABLE>
				</FIELDSET>
			</DIV>
			<asp:button id="btnGenerate" style="Z-INDEX: 103; LEFT: 80px; POSITION: absolute; TOP: 50px"
				runat="server" CssClass="queryButton" Text="Generate" Width="82px"></asp:button><asp:button id="btnQuery" style="Z-INDEX: 102; LEFT: 15px; POSITION: absolute; TOP: 50px" runat="server"
				CssClass="queryButton" Text="Query" Width="64px"></asp:button><asp:label id="lblErrorMessage" style="Z-INDEX: 110; LEFT: 17px; POSITION: absolute; TOP: 77px"
				runat="server" Height="14px" Width="554px" CssClass="errorMsgColor">Place holder for err msg</asp:label>
			<DIV id="divStatusExcp" style="CLEAR: both; Z-INDEX: 111; LEFT: 6px; WIDTH: 470px; POSITION: relative; TOP: 100px; HEIGHT: 338px"
				runat="server">
				<fieldset style="WIDTH: 467px; HEIGHT: 338px"><legend><asp:label id="Label4" CssClass="tableHeadingFieldset" Runat="server">Status/Exception Codes</asp:label></legend>
					<TABLE id="tbStatusException" style="WIDTH: 99.67%; HEIGHT: 309px" runat="server">
						<TR height="5">
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
						</TR>
						<TR height="14">
							<TD align="left" colSpan="4"><asp:radiobutton id="rbStatus" tabIndex="8" runat="server" CssClass="tableRadioButton" GroupName="StatusExcpt"
									Checked="True" Text="Status"></asp:radiobutton></TD>
							<TD colSpan="2"></TD>
							<TD colSpan="4"><asp:radiobutton id="rbException" tabIndex="9" runat="server" CssClass="tableRadioButton" GroupName="StatusExcpt"
									Text="Exception"></asp:radiobutton></TD>
							<TD colSpan="10"></TD>
						</TR>
						<tr height="20">
							<td colSpan="20"></td>
						</tr>
						<TR height="14">
							<TD colSpan="4"><asp:label id="lblStatusClose" runat="server" Width="142px" CssClass="tableRadioButton" Font-Bold="True">Status Close</asp:label></TD>
							<TD colSpan="4"><asp:radiobutton id="rbSCYes" tabIndex="10" runat="server" CssClass="tableRadioButton" GroupName="StatusClose"
									Text="Yes"></asp:radiobutton></TD>
							<TD colSpan="6"><asp:radiobutton id="rbSCNo" tabIndex="11" runat="server" CssClass="tableRadioButton" GroupName="StatusClose"
									Text="No"></asp:radiobutton></TD>
							<TD colSpan="6"></TD>
						</TR>
						<tr height="15">
							<td colSpan="20"></td>
						</tr>
						<TR height="14">
							<TD colSpan="4"><asp:label id="lblInV" runat="server" Width="142px" CssClass="tableRadioButton" Font-Bold="True">Invoiceable</asp:label></TD>
							<TD colSpan="4"><asp:radiobutton id="rbInVYes" tabIndex="12" runat="server" CssClass="tableRadioButton" GroupName="Invoice"
									Text="Yes"></asp:radiobutton></TD>
							<TD colSpan="6"><asp:radiobutton id="rbInVNo" tabIndex="13" runat="server" CssClass="tableRadioButton" GroupName="Invoice"
									Text="No"></asp:radiobutton></TD>
							<TD colSpan="6"></TD>
						</TR>
						<tr height="15">
							<td colSpan="20"></td>
						</tr>
						<TR height="14">
							<TD colSpan="4"><asp:label id="lblStatusType" runat="server" Width="142px" CssClass="tableRadioButton" Font-Bold="True">Status Type</asp:label></TD>
							<TD colSpan="4"><asp:radiobutton id="rbPickup" tabIndex="14" runat="server" CssClass="tableRadioButton" GroupName="StatusType"
									Text="Pickup"></asp:radiobutton></TD>
							<TD colSpan="6"><asp:radiobutton id="rbDomestic" tabIndex="15" runat="server" CssClass="tableRadioButton" GroupName="StatusType"
									Text="Domestic"></asp:radiobutton></TD>
							<TD colSpan="4"><asp:radiobutton id="rbBoth" tabIndex="16" runat="server" CssClass="tableRadioButton" GroupName="StatusType"
									Text="Both"></asp:radiobutton></TD>
							<TD colSpan="2"></TD>
						</TR>
						<tr height="15">
							<td colSpan="20"></td>
						</tr>
						<TR height="14">
							<TD colSpan="4"><asp:label id="lblMbg" runat="server" Width="142px" CssClass="tableRadioButton" Font-Bold="True">MBG</asp:label></TD>
							<TD colSpan="4"><asp:radiobutton id="rbMBGYes" tabIndex="17" runat="server" CssClass="tableRadioButton" GroupName="MBG"
									Text="Yes"></asp:radiobutton></TD>
							<TD colSpan="6"><asp:radiobutton id="rbMBGNo" tabIndex="18" runat="server" CssClass="tableRadioButton" GroupName="MBG"
									Text="No"></asp:radiobutton></TD>
							<TD colSpan="6"></TD>
						</TR>
						<TR height="20">
							<TD colSpan="20"></TD>
						</TR>
						<TR height="14">
							<TD style="HEIGHT: 25px" colSpan="4"><asp:label id="lblStatusCode" runat="server" Width="142px" CssClass="tableRadioButton">Status Code</asp:label></TD>
							<TD style="HEIGHT: 25px" colSpan="5"><dbcombo:dbcombo id="dbCmbStatusCode" tabIndex="19" runat="server" Height="17px" Width="70px" AutoPostBack="True"
									ShowDbComboLink="False" TextUpLevelSearchButton="v" ServerMethod="StatusCodeServerMethod" TextBoxColumns="6"></dbcombo:dbcombo></TD>
							<TD style="HEIGHT: 25px" colSpan="11"></TD>
						</TR>
						<TR height="14">
							<TD colSpan="4"><asp:label id="Label1" runat="server" Width="142px" CssClass="tableRadioButton">Exception Code</asp:label></TD>
							<TD colSpan="5"><dbcombo:dbcombo id="dbCmbExceptionCode" tabIndex="20" runat="server" Height="17px" Width="69px"
									AutoPostBack="True" ShowDbComboLink="False" TextUpLevelSearchButton="v" ServerMethod="ExceptionCodeServerMethod"
									TextBoxColumns="6"></dbcombo:dbcombo></TD>
							<TD colSpan="11"></TD>
						</TR>
					</TABLE>
				</fieldset>
			</DIV>
			<DIV id="divCommodity" style="CLEAR: both; Z-INDEX: 107; LEFT: 6px; WIDTH: 400px; POSITION: relative; TOP: 100px; HEIGHT: 117px"
				runat="server">
				<fieldset style="WIDTH: 416px; HEIGHT: 127px"><legend><asp:label id="Label5" CssClass="tableHeadingFieldset" Runat="server">Commodity Codes</asp:label></legend>
					<TABLE id="tbCommodity" style="WIDTH: 90%; HEIGHT: 97px" runat="server">
						<TR height="5%">
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
						</TR>
						<TR height="5">
							<TD colSpan="4"><asp:label id="lblCmdCode" runat="server" Width="142px" CssClass="tableRadioButton">Commodity Code</asp:label></TD>
							<TD colSpan="5"><dbcombo:dbcombo id="dbCmbCmdCode" tabIndex="1" runat="server" Height="17px" Width="70px" AutoPostBack="True"
									ShowDbComboLink="False" TextUpLevelSearchButton="v" ServerMethod="CmdCodeServerMethod" TextBoxColumns="8"></dbcombo:dbcombo></TD>
							<TD colSpan="11"></TD>
						</TR>
						<TR height="10">
						</TR>
						<TR height="5">
							<TD colSpan="4"><asp:label id="lblCmdType" runat="server" Width="119px" CssClass="tableRadioButton">Commodity Type</asp:label></TD>
							<TD colSpan="5"><asp:dropdownlist id="ddCmdType" tabIndex="2" runat="server" Width="130px" CssClass="textField"></asp:dropdownlist></TD>
							<TD colSpan="11"></TD>
						</TR>
					</TABLE>
				</fieldset>
			</DIV>
			<DIV id="divIndusSct" style="CLEAR: both; Z-INDEX: 108; LEFT: 6px; WIDTH: 416px; POSITION: relative; TOP: 100px; HEIGHT: 99px"
				runat="server">
				<fieldset style="WIDTH: 416px; HEIGHT: 99px"><legend><asp:label id="Label6" CssClass="tableHeadingFieldset" Runat="server">Industrial Sector</asp:label></legend>
					<TABLE id="tbIndusSct" style="WIDTH: 400px; HEIGHT: 69px" runat="server">
						<TR height="5%">
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
						</TR>
						<TR height="5">
							<TD colSpan="4"><asp:label id="lblIndusCode" runat="server" Width="142px" CssClass="tableRadioButton">Industrial Sector Code</asp:label></TD>
							<TD colSpan="5"><dbcombo:dbcombo id="dbCmbIndusCode" tabIndex="1" runat="server" Height="17px" Width="70px" AutoPostBack="True"
									ShowDbComboLink="False" TextUpLevelSearchButton="v" ServerMethod="IndusCodeServerMethod" TextBoxColumns="8"></dbcombo:dbcombo></TD>
							<TD colSpan="11"></TD>
						</TR>
					</TABLE>
				</fieldset>
			</DIV>
			<DIV id="divVas" style="CLEAR: both; Z-INDEX: 109; LEFT: 6px; WIDTH: 416px; POSITION: relative; TOP: 100px; HEIGHT: 99px"
				runat="server">
				<fieldset style="WIDTH: 416px; HEIGHT: 99px"><legend><asp:label id="Label7" CssClass="tableHeadingFieldset" Runat="server">VAS</asp:label></legend>
					<TABLE id="tbVas" style="WIDTH: 400px; HEIGHT: 69px" runat="server">
						<TR height="5%">
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
						</TR>
						<TR height="5">
							<TD colSpan="4"><asp:label id="Label2" runat="server" Width="142px" CssClass="tableRadioButton">VAS Code</asp:label></TD>
							<TD colSpan="5"><dbcombo:dbcombo id="dbCmbVas" tabIndex="1" runat="server" Height="17px" Width="49px" AutoPostBack="True"
									ShowDbComboLink="False" TextUpLevelSearchButton="v" ServerMethod="VasServerMethod" TextBoxColumns="8"></dbcombo:dbcombo></TD>
							<TD colSpan="11"></TD>
						</TR>
					</TABLE>
				</fieldset>
			</DIV>
			<DIV id="divService" style="CLEAR: both; Z-INDEX: 105; LEFT: 6px; WIDTH: 416px; POSITION: relative; TOP: 100px; HEIGHT: 141px"
				runat="server">
				<fieldset style="WIDTH: 416px; HEIGHT: 141px"><legend><asp:label id="Label8" CssClass="tableHeadingFieldset" Runat="server">Service</asp:label></legend>
					<TABLE id="tbService" style="WIDTH: 400px; HEIGHT: 69px" runat="server">
						<TR height="5%">
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
						</TR>
						<TR height="27">
							<TD colSpan="4"><asp:label id="lblServiceCode" runat="server" Width="142px" CssClass="tableRadioButton">Service Code</asp:label></TD>
							<TD colSpan="5"><dbcombo:dbcombo id="dbCmbServiceCode" tabIndex="1" runat="server" AutoPostBack="True" ShowDbComboLink="False"
									TextUpLevelSearchButton="v" ServerMethod="ServiceCodeServerMethod" TextBoxColumns="4"></dbcombo:dbcombo></TD>
							<TD colSpan="11"></TD>
						</TR>
						<TR height="27">
							<TD colSpan="4"><asp:label id="lblTransitDays" runat="server" Width="142px" CssClass="tableRadioButton">Transit Day</asp:label></TD>
							<TD colSpan="5"><asp:dropdownlist id="ddTransitDays" tabIndex="2" runat="server" CssClass="textField"></asp:dropdownlist></TD>
							<TD colSpan="11"></TD>
						</TR>
						<TR height="27">
							<TD colSpan="4"><asp:label id="lblTransitHrs" runat="server" Width="142px" CssClass="tableRadioButton">Transit Hour</asp:label></TD>
							<TD colSpan="5"><asp:dropdownlist id="ddTransitHrs" tabIndex="3" runat="server" CssClass="textField"></asp:dropdownlist></TD>
							<TD colSpan="11"></TD>
						</TR>
					</TABLE>
				</fieldset>
			</DIV>
			<DIV id="divBaseZone" style="CLEAR: both; Z-INDEX: 106; LEFT: 6px; WIDTH: 416px; POSITION: relative; TOP: 100px; HEIGHT: 112px"
				runat="server">
				<fieldset style="WIDTH: 416px; HEIGHT: 112px"><legend><asp:label id="Label9" CssClass="tableHeadingFieldset" Runat="server">Origin / Destination Zone Code</asp:label></legend>
					<TABLE id="tbBaseZone" style="WIDTH: 400px; HEIGHT: 82px" runat="server">
						<TR height="5%">
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
						</TR>
						<TR height="27">
							<TD colSpan="4"><asp:label id="lblOriZone" runat="server" Width="115px" CssClass="tableRadioButton">Origin Zone Code</asp:label></TD>
							<TD colSpan="5"><dbcombo:dbcombo id="dbCmbOrgCode" tabIndex="1" runat="server" Height="17px" Width="49px" AutoPostBack="True"
									ShowDbComboLink="False" TextUpLevelSearchButton="v" ServerMethod="OriginCodeServerMethod" TextBoxColumns="15"></dbcombo:dbcombo></TD>
							<TD colSpan="11"></TD>
						</TR>
						<TR height="27">
							<TD colSpan="4"><asp:label id="lblDestZone" runat="server" Width="147px" CssClass="tableRadioButton">Destination Zone Code</asp:label></TD>
							<TD colSpan="5"><dbcombo:dbcombo id="dbCmbDestCode" tabIndex="2" runat="server" Height="17px" Width="49px" AutoPostBack="True"
									ShowDbComboLink="False" TextUpLevelSearchButton="v" ServerMethod="OriginCodeServerMethod" TextBoxColumns="15"></dbcombo:dbcombo></TD>
							<TD colSpan="11"></TD>
						</TR>
						<TR>
							<TD colSpan="4">
								<asp:label id="Label13" runat="server" CssClass="tableRadioButton" Width="142px">Service Code</asp:label></TD>
							<TD colSpan="5">
								<dbcombo:dbcombo id="dbCmbServiceCodeRate" tabIndex="1" runat="server" AutoPostBack="True" TextBoxColumns="4"
									ServerMethod="ServiceCodeServerMethod" TextUpLevelSearchButton="v" ShowDbComboLink="False"></dbcombo:dbcombo></TD>
							<TD colSpan="11"></TD>
						</TR>
					</TABLE>
				</fieldset>
			</DIV>
			<DIV id="divZipSate" style="CLEAR: both; Z-INDEX: 104; LEFT: 6px; WIDTH: 521px; POSITION: relative; TOP: 100px; HEIGHT: 89px"
				runat="server">
				<fieldset><legend><asp:label id="Label10" CssClass="tableHeadingFieldset" Runat="server">Postal Code / State</asp:label></legend>
					<TABLE id="tbZipSate" style="WIDTH: 700px; HEIGHT: 194px" runat="server">
						<TR height="5%">
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
						</TR>
						<TR height="27">
							<TD colSpan="4"><asp:label id="lblCountry" runat="server" Width="70px" CssClass="tableRadioButton">Country</asp:label></TD>
							<TD colSpan="5"><dbcombo:dbcombo id="dbCmbCountry" tabIndex="1" runat="server" Height="17px" Width="140px" AutoPostBack="True"
									ShowDbComboLink="False" TextUpLevelSearchButton="v" ServerMethod="CountryServerMethod" TextBoxColumns="10"
									ReQueryOnLoad="True" EnableViewState="False" ClientOnSelectFunction="dbCmbCountryOnSelect"></dbcombo:dbcombo></TD>
							<TD colSpan="11"></TD>
						</TR>
						<TR height="27">
							<TD colSpan="4"><asp:label id="lblZipcode" runat="server" Width="70px" CssClass="tableRadioButton">Postal Code</asp:label></TD>
							<TD colSpan="5"><dbcombo:dbcombo id="dbCmbZipCode" tabIndex="2" runat="server" Height="17px" Width="140px" AutoPostBack="True"
									ShowDbComboLink="False" TextUpLevelSearchButton="v" ServerMethod="ZipCodeServerMethod" TextBoxColumns="10"
									ClientStateFunction="dbCmbZipCodeState()"></dbcombo:dbcombo></TD>
							<TD colSpan="11"></TD>
						</TR>
						<TR height="27">
							<TD style="HEIGHT: 26px" colSpan="4"><asp:label id="lblState" runat="server" Width="70px" CssClass="tableRadioButton">State</asp:label></TD>
							<TD style="HEIGHT: 26px" colSpan="5"><dbcombo:dbcombo id="dbCmbState" tabIndex="3" runat="server" Height="17px" Width="140px" AutoPostBack="True"
									ShowDbComboLink="False" TextUpLevelSearchButton="v" ServerMethod="StateServerMethod" TextBoxColumns="10" ClientOnSelectFunction="dbCmbStateOnSelect"
									ClientStateFunction="dbCmbStateCodeState()"></dbcombo:dbcombo></TD>
							<TD colSpan="2"></TD>
							<TD style="HEIGHT: 26px" colSpan="4"><asp:label id="lblStateName" runat="server" Width="87px" CssClass="tableRadioButton">State Name</asp:label></TD>
							<TD style="HEIGHT: 26px" colSpan="5"><dbcombo:dbcombo id="dbCmbStateName" tabIndex="4" runat="server" Height="17px" Width="124px" AutoPostBack="True"
									ShowDbComboLink="False" TextUpLevelSearchButton="v" ServerMethod="StateNameServerMethod" TextBoxColumns="10" ClientStateFunction="dbCmbStateNameState()"></dbcombo:dbcombo></TD>
						</TR>
						<TR height="27">
							<TD colSpan="4"><asp:label id="lblZoneCode" runat="server" Width="70px" CssClass="tableRadioButton">Zone Code</asp:label></TD>
							<TD colSpan="5"><dbcombo:dbcombo id="dbCmbZoneCode" tabIndex="5" runat="server" Height="17px" Width="140px" AutoPostBack="True"
									ShowDbComboLink="False" TextUpLevelSearchButton="v" ServerMethod="OriginCodeServerMethod" TextBoxColumns="10"></dbcombo:dbcombo></TD>
							<TD colSpan="11"></TD>
						</TR>
						<TR height="27">
							<TD colSpan="4"><asp:label id="lblDelvRoute" runat="server" Width="128px" CssClass="tableRadioButton">Delivery Route Code</asp:label></TD>
							<TD colSpan="5"><dbcombo:dbcombo id="dbCmbDlvRouteCode" tabIndex="6" runat="server" Height="17px" Width="140px" AutoPostBack="True"
									ShowDbComboLink="False" TextUpLevelSearchButton="v" ServerMethod="RouteCodeServerMethod" TextBoxColumns="10"></dbcombo:dbcombo></TD>
							<TD colSpan="2"></TD>
							<TD colSpan="4"><asp:label id="lblPckRouteCode" runat="server" Width="122px" CssClass="tableRadioButton">Pickup Route Code</asp:label></TD>
							<TD colSpan="5"><dbcombo:dbcombo id="dbCmbPckRouteCode" tabIndex="7" runat="server" Height="17px" Width="122px" AutoPostBack="True"
									ShowDbComboLink="False" TextUpLevelSearchButton="v" ServerMethod="RouteCodeServerMethod" TextBoxColumns="10"></dbcombo:dbcombo></TD>
						</TR>
					</TABLE>
				</fieldset>
			</DIV>
			<DIV id="divCustProfile" style="CLEAR: both; Z-INDEX: 104; LEFT: 6px; WIDTH: 521px; POSITION: relative; TOP: 100px; HEIGHT: 89px"
				runat="server">
				<fieldset><legend><asp:label id="Label11" CssClass="tableHeadingFieldset" Runat="server">Payer Details</asp:label></legend>
					<TABLE id="tbCustProfile" style="WIDTH: 700px; HEIGHT: 64px" runat="server">
						<TR height="5%">
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
						</TR>
						<TR height="27">
							<TD colSpan="4"><asp:label id="lblPayerid" runat="server" Width="82px" CssClass="tableRadioButton">Payer ID</asp:label></TD>
							<TD colSpan="5"><dbcombo:dbcombo id="dbCmbPayerId" tabIndex="1" runat="server" Height="17px" Width="140px" AutoPostBack="True"
									ShowDbComboLink="False" TextUpLevelSearchButton="v" ServerMethod="PayerIdServerMethod" TextBoxColumns="21"
									ReQueryOnLoad="True" EnableViewState="False" ClientOnSelectFunction="dbCmbPayerIdOnSelect"></dbcombo:dbcombo></TD>
							<td colSpan="2"></td>
							<TD colSpan="4"><asp:label id="lblCustName" runat="server" Width="103px" CssClass="tableRadioButton">Payer Name</asp:label></TD>
							<TD colSpan="5"><dbcombo:dbcombo id="dbCmbPayerName" tabIndex="2" runat="server" Height="17px" Width="146px" AutoPostBack="True"
									ShowDbComboLink="False" TextUpLevelSearchButton="v" ServerMethod="PayerNameServerMethod" TextBoxColumns="25"
									ReQueryOnLoad="True" EnableViewState="False" ClientStateFunction="dbCmbPayerNameState()"></dbcombo:dbcombo></TD>
						</TR>
						<TR height="5">
							<TD colSpan="4"><asp:label id="lblCustIndustSect" runat="server" Width="142px" CssClass="tableRadioButton">Industrial Sector Code</asp:label></TD>
							<TD colSpan="5"><dbcombo:dbcombo id="CustDbCmbIndustSect" tabIndex="1" runat="server" Height="17px" Width="140px"
									AutoPostBack="True" ShowDbComboLink="False" TextUpLevelSearchButton="v" ServerMethod="IndusCodeServerMethod"
									TextBoxColumns="21"></dbcombo:dbcombo></TD>
							<TD colSpan="2"></TD>
						</TR>
						<TR height="15">
							<td colspan="20"></td>
						</TR>
						<TR height="27">
							<TD colSpan="4"><asp:label id="Lb_PaymentMode" runat="server" Width="122px" CssClass="tableLabel" Font-Bold="True">Payment Mode</asp:label></TD>
							<TD colSpan="3"><asp:radiobutton id="Rd_C_Cash" tabIndex="13" runat="server" CssClass="tableRadioButton" GroupName="PaymentMode"
									Text="Cash" BorderStyle="None" Width="89px"></asp:radiobutton></TD>
							<TD colSpan="3"><asp:radiobutton id="rd_C_Credit" tabIndex="16" runat="server" Width="75px" CssClass="tableRadioButton"
									AutoPostBack="True" GroupName="PaymentMode" Text="Credit" BorderStyle="None"></asp:radiobutton></TD>
						</TR>
						<TR height="15">
							<td colspan="20"></td>
						</TR>
						<TR height="14">
							<TD colSpan="4"><asp:label id="lblCustMbg" runat="server" Width="142px" CssClass="tableRadioButton" Font-Bold="True">MBG</asp:label></TD>
							<TD colSpan="3"><asp:radiobutton id="CustMbgYes" tabIndex="17" runat="server" CssClass="tableRadioButton" GroupName="MBG"
									Text="Yes"></asp:radiobutton></TD>
							<TD colSpan="3"><asp:radiobutton id="CustMbgNo" tabIndex="18" runat="server" CssClass="tableRadioButton" GroupName="MBG"
									Text="No"></asp:radiobutton></TD>
							<TD colSpan="10"></TD>
						</TR>
						<TR height="15">
							<td colspan="20"></td>
						</TR>
						<TR height="14">
							<TD colSpan="4"><asp:label id="lblStatusActive" runat="server" Width="142px" CssClass="tableRadioButton" Font-Bold="True">Status Active</asp:label></TD>
							<TD colSpan="3"><asp:radiobutton id="CustStatusYes" tabIndex="17" runat="server" CssClass="tableRadioButton" GroupName="StatusAct"
									Text="Yes"></asp:radiobutton></TD>
							<TD colSpan="3"><asp:radiobutton id="CustStatusNo" tabIndex="18" runat="server" CssClass="tableRadioButton" GroupName="StatusAct"
									Text="No"></asp:radiobutton></TD>
							<TD colSpan="10"></TD>
						</TR>
						<TR height="15">
							<td colspan="20"></td>
						</TR>
						<TR height="14">
							<TD colSpan="4"><asp:label id="lblCustApplyWt" runat="server" Width="142px" CssClass="tableRadioButton" Font-Bold="True">Apply Dim Wt </asp:label></TD>
							<TD colSpan="3"><asp:radiobutton id="CustApplyWtYes" tabIndex="17" runat="server" CssClass="tableRadioButton" GroupName="ApplydimWt"
									Text="Yes"></asp:radiobutton></TD>
							<TD colSpan="3"><asp:radiobutton id="CustApplyWtNo" tabIndex="18" runat="server" CssClass="tableRadioButton" GroupName="ApplydimWt"
									Text="No"></asp:radiobutton></TD>
							<TD colSpan="10"></TD>
						</TR>
						<TR height="15">
							<td colspan="20"></td>
						</TR>
						<TR height="14">
							<TD colSpan="4"><asp:label id="lblApplyEsaSurchg" runat="server" Width="142px" CssClass="tableRadioButton"
									Font-Bold="True">Apply Esa Surcharge </asp:label></TD>
							<TD colSpan="3"><asp:radiobutton id="CustApplyEsaYes" tabIndex="17" runat="server" CssClass="tableRadioButton" GroupName="ESA"
									Text="Yes"></asp:radiobutton></TD>
							<TD colSpan="3"><asp:radiobutton id="CustApplyEsaNo" tabIndex="18" runat="server" CssClass="tableRadioButton" GroupName="ESA"
									Text="No"></asp:radiobutton></TD>
							<TD colSpan="10"></TD>
						</TR>
						<TR height="15">
							<td colspan="20"></td>
						</TR>
						<TR height="14">
							<TD colSpan="4"><asp:label id="lblPODSlip" runat="server" Width="142px" CssClass="tableRadioButton" Font-Bold="True">POD Slip Required </asp:label></TD>
							<TD colSpan="3"><asp:radiobutton id="CustPODYes" tabIndex="17" runat="server" CssClass="tableRadioButton" GroupName="POD"
									Text="Yes"></asp:radiobutton></TD>
							<TD colSpan="3"><asp:radiobutton id="CustPODNo" tabIndex="18" runat="server" CssClass="tableRadioButton" GroupName="POD"
									Text="No"></asp:radiobutton></TD>
							<TD colSpan="10"></TD>
						</TR>
					</TABLE>
				</fieldset>
			</DIV>
			<DIV id="divAgent" style="CLEAR: both; Z-INDEX: 104; LEFT: 6px; WIDTH: 521px; POSITION: relative; TOP: 100px; HEIGHT: 89px"
				runat="server">
				<fieldset><legend><asp:label id="Label12" CssClass="tableHeadingFieldset" Runat="server">Agent Details</asp:label></legend>
					<TABLE id="tbAgent" style="WIDTH: 700px; HEIGHT: 64px" runat="server">
						<TR height="5%">
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
						</TR>
						<TR height="27">
							<TD colSpan="4"><asp:label id="lblAgentId" runat="server" Width="82px" CssClass="tableRadioButton">Agent ID</asp:label></TD>
							<TD colSpan="5"><dbcombo:dbcombo id="dbCmbAgentId" tabIndex="1" runat="server" Height="17px" Width="140px" AutoPostBack="True"
									ShowDbComboLink="False" TextUpLevelSearchButton="v" ServerMethod="AgentIdServerMethod" TextBoxColumns="20"
									ReQueryOnLoad="True" EnableViewState="False" ClientOnSelectFunction="dbCmbAgentIdOnSelect"></dbcombo:dbcombo></TD>
							<td colSpan="2"></td>
							<TD colSpan="4"><asp:label id="lblAgentName" runat="server" Width="103px" CssClass="tableRadioButton">Agent Name</asp:label></TD>
							<TD colSpan="5"><dbcombo:dbcombo id="dbCmbAgentName" tabIndex="2" runat="server" Height="17px" Width="146px" AutoPostBack="True"
									ShowDbComboLink="False" TextUpLevelSearchButton="v" ServerMethod="AgentNameServerMethod" TextBoxColumns="25"
									ReQueryOnLoad="True" EnableViewState="False" ClientStateFunction=" dbCmbAgentNameState()"></dbcombo:dbcombo></TD>
						</TR>
						<TR height="15">
							<td colspan="20"></td>
						</TR>
						<TR height="27">
							<TD colSpan="4"><asp:label id="A_Lb_PaymentMode" runat="server" Width="122px" CssClass="tableLabel" Font-Bold="True">Payment Mode</asp:label></TD>
							<TD colSpan="3"><asp:radiobutton id="Rd_A_Cash" tabIndex="13" runat="server" CssClass="tableRadioButton" GroupName="PaymentMode"
									Text="Cash" BorderStyle="None" Width="89px"></asp:radiobutton></TD>
							<TD colSpan="3"><asp:radiobutton id="Rd_A_Credit" tabIndex="16" runat="server" Width="75px" CssClass="tableRadioButton"
									AutoPostBack="True" GroupName="PaymentMode" Text="Credit" BorderStyle="None"></asp:radiobutton></TD>
						</TR>
						<TR height="15">
							<td colspan="20"></td>
						</TR>
						<TR height="14">
							<TD colSpan="4"><asp:label id="lblAgentMbg" runat="server" Width="142px" CssClass="tableRadioButton" Font-Bold="True">MBG</asp:label></TD>
							<TD colSpan="3"><asp:radiobutton id="AgentMbgYes" tabIndex="17" runat="server" CssClass="tableRadioButton" GroupName="MBG"
									Text="Yes"></asp:radiobutton></TD>
							<TD colSpan="3"><asp:radiobutton id="AgentMbgNo" tabIndex="18" runat="server" CssClass="tableRadioButton" GroupName="MBG"
									Text="No"></asp:radiobutton></TD>
							<TD colSpan="10"></TD>
						</TR>
						<TR height="15">
							<td colspan="20"></td>
						</TR>
						<TR height="14">
							<TD colSpan="4"><asp:label id="Label14" runat="server" Width="142px" CssClass="tableRadioButton" Font-Bold="True">Status Active</asp:label></TD>
							<TD colSpan="3"><asp:radiobutton id="AgentStatusYes" tabIndex="17" runat="server" CssClass="tableRadioButton" GroupName="StatusAct"
									Text="Yes"></asp:radiobutton></TD>
							<TD colSpan="3"><asp:radiobutton id="AgentStatusNo" tabIndex="18" runat="server" CssClass="tableRadioButton" GroupName="StatusAct"
									Text="No"></asp:radiobutton></TD>
							<TD colSpan="10"></TD>
						</TR>
						<TR height="15">
							<td colspan="20"></td>
						</TR>
						<TR height="14">
							<TD colSpan="4"><asp:label id="lblAgentApplyWt" runat="server" Width="142px" CssClass="tableRadioButton" Font-Bold="True">Apply Dim Wt </asp:label></TD>
							<TD colSpan="3"><asp:radiobutton id="AgentApplyWtYes" tabIndex="17" runat="server" CssClass="tableRadioButton" GroupName="ApplydimWt"
									Text="Yes"></asp:radiobutton></TD>
							<TD colSpan="3"><asp:radiobutton id="AgentApplyWtNo" tabIndex="18" runat="server" CssClass="tableRadioButton" GroupName="ApplydimWt"
									Text="No"></asp:radiobutton></TD>
							<TD colSpan="10"></TD>
						</TR>
						<TR height="15">
							<td colspan="20"></td>
						</TR>
						<TR height="14">
							<TD colSpan="4"><asp:label id="Label15" runat="server" Width="142px" CssClass="tableRadioButton" Font-Bold="True">Apply Esa Surcharge </asp:label></TD>
							<TD colSpan="3"><asp:radiobutton id="AgentApplyEsaYes" tabIndex="17" runat="server" CssClass="tableRadioButton" GroupName="ESA"
									Text="Yes"></asp:radiobutton></TD>
							<TD colSpan="3"><asp:radiobutton id="AgentApplyEsaNo" tabIndex="18" runat="server" CssClass="tableRadioButton" GroupName="ESA"
									Text="No"></asp:radiobutton></TD>
							<TD colSpan="10"></TD>
						</TR>
						<TR height="15">
							<td colspan="20"></td>
						</TR>
						<TR height="14">
							<TD colSpan="4"><asp:label id="Label16" runat="server" Width="142px" CssClass="tableRadioButton" Font-Bold="True">POD Slip Required </asp:label></TD>
							<TD colSpan="3"><asp:radiobutton id="AgentPODYes" tabIndex="17" runat="server" CssClass="tableRadioButton" GroupName="POD"
									Text="Yes"></asp:radiobutton></TD>
							<TD colSpan="3"><asp:radiobutton id="AgentPODNo" tabIndex="18" runat="server" CssClass="tableRadioButton" GroupName="POD"
									Text="No"></asp:radiobutton></TD>
							<TD colSpan="10"></TD>
						</TR>
					</TABLE>
				</fieldset>
			</DIV>
			<DIV id="divStaff" style="CLEAR: both; Z-INDEX: 105; LEFT: 6px; WIDTH: 416px; POSITION: relative; TOP: 100px; HEIGHT: 141px"
				runat="server">
				<fieldset style="WIDTH: 416px; HEIGHT: 141px"><legend><asp:label id="Label17" CssClass="tableHeadingFieldset" Runat="server">Staff</asp:label></legend>
					<TABLE id="Table1" style="WIDTH: 400px; HEIGHT: 69px" runat="server">
						<TR height="5%">
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
						</TR>
						<TR height="27">
							<TD colSpan="4"><asp:label id="lblStaffId" runat="server" Width="142px" CssClass="tableRadioButton">Staff ID</asp:label></TD>
							<TD colSpan="5"><dbcombo:dbcombo id="dbCmbStaffId" tabIndex="1" runat="server" AutoPostBack="True" ShowDbComboLink="False"
									TextUpLevelSearchButton="v" ServerMethod="StaffIdServerMethod" TextBoxColumns="4"></dbcombo:dbcombo></TD>
							<TD colSpan="11"></TD>
						</TR>
					</TABLE>
				</fieldset>
			</DIV>
			<DIV id="divVehicle" style="CLEAR: both; Z-INDEX: 105; LEFT: 6px; WIDTH: 416px; POSITION: relative; TOP: 100px; HEIGHT: 141px"
				runat="server">
				<fieldset style="WIDTH: 416px; HEIGHT: 141px"><legend><asp:label id="Label21" CssClass="tableHeadingFieldset" Runat="server">Vehicle</asp:label></legend>
					<TABLE id="Table2" style="WIDTH: 400px; HEIGHT: 69px" runat="server">
						<TR height="5%">
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
							<TD width="5%"></TD>
						</TR>
						<TR height="27">
							<TD colSpan="4"><asp:label id="lblTruckId" runat="server" Width="142px" CssClass="tableRadioButton">Truck ID</asp:label></TD>
							<TD colSpan="5"><dbcombo:dbcombo id="dbCmbTruckId" tabIndex="1" runat="server" AutoPostBack="True" ShowDbComboLink="False"
									TextUpLevelSearchButton="v" ServerMethod="VehicleIdServerMethod" TextBoxColumns="4"></dbcombo:dbcombo></TD>
							<TD colSpan="11"></TD>
						</TR>
					</TABLE>
				</fieldset>
			</DIV>
		</FORM>
	</BODY>
</HTML>
