using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.common.util;
using com.common.classes;
using com.common.DAL;
using System.Text;
using com.common.applicationpages;

namespace TIES.WebUI
{
	/// <summary>
	/// Summary description for VASPopup.
	/// </summary>
	public class VASPopup : BasePopupPage
	{
		protected System.Web.UI.WebControls.Button btnSearch;
		protected System.Web.UI.WebControls.Label lblVASDesc;
		protected System.Web.UI.WebControls.Label lblVASCode;
		protected System.Web.UI.WebControls.TextBox txtVASDesc;
		protected System.Web.UI.WebControls.DataGrid dgVASMaster;
		//Utility utility = null;
		private String appID = null;
		protected System.Web.UI.WebControls.TextBox txtVASCode;
		private String enterpriseID = null;
		private String strVASID = null;
		private String strVASDescID = null;
		private String strVASSurchargeID = null;
		private string strVASSurchargeIDH = null;
		private string strFormId=null;
		private String strDestZipCode = null;
		protected System.Web.UI.WebControls.Button btnClose;
		SessionDS m_sdsVas =null;

		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			//utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings,Page.Session);
			appID = utility.GetAppID();
			enterpriseID = utility.GetEnterpriseID();

			strDestZipCode = Request.Params["DestZipCode"];
			strVASID = Request.Params["VASID"];
			strVASDescID = Request.Params["VASDESC"];
			strFormId=Request.Params["FORMID"];  
			strVASSurchargeID = Request.Params["VASSURCHARGE"];
			if(Request.Params["HVASSURCHARGE"] != null)
			{
				strVASSurchargeIDH = Request.Params["HVASSURCHARGE"].ToString();
			}
			
			if (!IsPostBack)
			{
				txtVASCode.Text = Request.Params["VASCODE"];
				txtVASDesc.Text = Request.Params["VASDESCRIPTION"];

				m_sdsVas = GetEmptyVAS(0); 
				
				BindGrid();
			}
			else
			{
				m_sdsVas = (SessionDS)ViewState["VAS_DS"];
			}
		}

		public static SessionDS GetEmptyVAS(int intRows)
		{
			DataTable dtVAS = new DataTable();
			dtVAS.Columns.Add(new DataColumn("vas_code", typeof(string)));
			dtVAS.Columns.Add(new DataColumn("vas_description", typeof(string)));
			dtVAS.Columns.Add(new DataColumn("surcharge", typeof(string)));
			
			for(int i=0; i < intRows; i++)
			{
				DataRow drEach = dtVAS.NewRow();
				drEach[0] = "";
				drEach[1] = "";
				dtVAS.Rows.Add(drEach);
			}
			DataSet dsVASFields = new DataSet();
			dsVASFields.Tables.Add(dtVAS);
			SessionDS sessionDS = new SessionDS();
			sessionDS.ds = dsVASFields;
			sessionDS.DataSetRecSize = dsVASFields.Tables[0].Rows.Count;
			sessionDS.QueryResultMaxSize = sessionDS.DataSetRecSize;
			return sessionDS;
		}

		public void Paging(Object sender, DataGridPageChangedEventArgs e)
		{
			//runs when one of the page control is clicked
			//update the current page from the parameter values
			dgVASMaster.CurrentPageIndex = e.NewPageIndex;
			ShowCurrentPage();
		}
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			base.OnInit(e);
			InitializeComponent();
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.dgVASMaster.SelectedIndexChanged += new System.EventHandler(this.dgVASMaster_SelectedIndexChanged);
			this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
			this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
		
		private void btnSearch_Click(object sender, System.EventArgs e)
		{
			StringBuilder strQry = new StringBuilder();
			strQry.Append("select * from VAS a where a.applicationid='");
			strQry.Append(appID);
			strQry.Append("' and a.enterpriseid = '");
			strQry.Append(enterpriseID);
			strQry.Append("'");
			if (txtVASCode.Text.ToString() != null && txtVASCode.Text.ToString() != "")
			{
				strQry.Append(" and a.vas_code like '%");
				strQry.Append(Utility.ReplaceSingleQuote(txtVASCode.Text.ToString()));
				strQry.Append("%'");
			}
			if (txtVASDesc.Text.ToString() != null && txtVASDesc.Text.ToString() != "")
			{
				strQry.Append(" and a.vas_description like N'%");
				strQry.Append(Utility.ReplaceSingleQuote(txtVASDesc.Text.ToString()));
				strQry.Append("%'");
			}

			if((strFormId == "DomesticShipment")||(strFormId == "ShipmentDetails"))
			{
				strQry.Append(" and a.vas_code not in (select b.vas_code from zipcode_vas_excluded b where b.applicationid = '");
				strQry.Append(appID);
				strQry.Append("' and b.enterpriseid = '");
				strQry.Append(enterpriseID+"'");
				if(strDestZipCode != null && strDestZipCode != "")
				{
					strQry.Append(" and b.zipcode like '%");
					strQry.Append(strDestZipCode);
					strQry.Append("%' ");
				}
				strQry.Append(")");
			}
 
			String strSQLQuery = strQry.ToString();
			ViewState["SQL_QUERY"] = strSQLQuery;
			dgVASMaster.CurrentPageIndex = 0;
			ShowCurrentPage();
		}

		

		private void dgVASMaster_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			// Get the value and return it to the parent window
			int iSelIndex = dgVASMaster.SelectedIndex;
			DataGridItem dgRow = dgVASMaster.Items[iSelIndex];
			String strVASCode = dgRow.Cells[0].Text;
			String strVASDesc = dgRow.Cells[1].Text;
			String strVASSurchrg = dgRow.Cells[2].Text;

			if(strVASDesc == "&nbsp;")
			{
				strVASDesc = "";
			}
			if(strVASCode == "&nbsp;")
			{
				strVASCode = "";
			}
			if(strVASSurchrg == "&nbsp;")
			{
				strVASSurchrg = "";
			}
			
			 if(strFormId=="BandQuotation")
			{
				String sScript = "";
				sScript += "<script language=javascript>";
				sScript += "  window.opener."+strFormId+"."+strVASID+".value = \""+strVASCode+"\";";
				sScript += "  window.opener."+strFormId+"."+strVASDescID+".value = \""+strVASDesc+"\";";
				if( strVASSurchrg.IndexOf(".",0) >0 )
				{
					sScript += "  window.opener."+strFormId+"."+strVASSurchargeID+".value = \""+strVASSurchrg+"\";";					
				}		
				else
				{
					strVASSurchrg +=".00";
					sScript += "  window.opener."+strFormId+"."+strVASSurchargeID+".value = \""+strVASSurchrg+"\";";
				}
				sScript += "  window.close();";
				sScript += "</script>";
				Response.Write(sScript);
			}
			else if(strFormId=="AgentVAS")
			{
				String sScript = "";
				sScript += "<script language=javascript>";
				sScript += "  window.opener."+strFormId+"."+strVASID+".value = \""+strVASCode+"\";";
				sScript += "  window.opener."+strFormId+"."+strVASDescID+".value = \""+strVASDesc+"\";";
				if( strVASSurchrg.IndexOf(".",0) >0 )
				{
					sScript += "  window.opener."+strFormId+"."+strVASSurchargeID+".value = \""+strVASSurchrg+"\";";
					
				}		
				else
				{
					strVASSurchrg +=".00";
					sScript += "  window.opener."+strFormId+"."+strVASSurchargeID+".value = \""+strVASSurchrg+"\";";
					
				}
				sScript += "  window.close();";
				sScript += "</script>";
				Response.Write(sScript);
			}
			else if((strFormId == "DomesticShipment")||(strFormId.Trim() == "ShipmentDetails"))
			{			 
				String sScript = "";
				sScript += "<script language=javascript>";
				sScript += "  window.opener."+strFormId+"."+strVASID+".value = \""+strVASCode+"\";";
				sScript += "  window.opener."+strFormId+"."+strVASDescID+".value = \""+strVASDesc+"\";";
				sScript += "  window.opener."+strFormId+"."+strVASSurchargeID+".value = \""+strVASSurchrg+"\";";
				sScript += "  window.opener."+strFormId+"."+strVASSurchargeIDH+".value = \""+strVASSurchrg+"\";";
				sScript += "  window.close();";
				sScript += "</script>";
				Response.Write(sScript);
			}
			else if((strFormId=="EnterpriseProfile")||(strFormId.Trim() == "PackageDetails"))
			{
				String sScript = "";
				sScript += "<script language=javascript>";
				sScript += "  window.opener."+strFormId+"."+strVASID+".value = \""+strVASCode+"\";";

				if(strVASDescID!=null && strVASDescID!="") //case set to textbox in datagrid
					sScript += "  window.opener."+strFormId+"."+strVASDescID+".value = \""+strVASDesc+"\";";				

				sScript += "  window.close();";
				sScript += "</script>";
				Response.Write(sScript);
			}
		}

		private void BindGrid()
		{
			dgVASMaster.VirtualItemCount = System.Convert.ToInt32(m_sdsVas.QueryResultMaxSize);
			dgVASMaster.DataSource = m_sdsVas.ds;
			dgVASMaster.DataBind();
			ViewState["CUST_DS"] = m_sdsVas;
		}

		private void ShowCurrentPage()
		{
			int iStartIndex = dgVASMaster.CurrentPageIndex * dgVASMaster.PageSize;
			String sqlQuery = (String)ViewState["SQL_QUERY"];
			m_sdsVas = GetVASDDS(iStartIndex,dgVASMaster.PageSize,sqlQuery);
			decimal pgCnt = (m_sdsVas.QueryResultMaxSize - 1)/dgVASMaster.PageSize;
			if(pgCnt < dgVASMaster.CurrentPageIndex)
			{
				dgVASMaster.CurrentPageIndex = System.Convert.ToInt32(pgCnt);
			}
			dgVASMaster.SelectedIndex = -1;
			dgVASMaster.EditItemIndex = -1;

			BindGrid();
			getPageControls(Page);
		}

		private SessionDS GetVASDDS(int iCurrent, int iDSRecSize, String strQuery)
		{
			SessionDS sessionDS = null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(appID,enterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("VASPopup.aspx.cs","GetVASDDS","ERR001","DbConnection object is null!!");
				return sessionDS;
			}
			sessionDS = dbCon.ExecuteQuery(strQuery,iCurrent,iDSRecSize,"ZoneCode");
			return  sessionDS;
		}

		private void btnClose_Click(object sender, System.EventArgs e)
		{
			String sScript = "";
			sScript += "<script language=javascript>";
			sScript += "  window.close();";
			sScript += "</script>";
			Response.Write(sScript);
		}
	}
}
