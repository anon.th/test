using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.ties.DAL;
using com.common.applicationpages;
using com.common.util;

namespace TIES.WebUI
{
	/// <summary>
	/// Summary description for QuotationVASPopup.
	/// </summary>
	public class QuotationVASPopup : BasePopupPage
	{
		protected System.Web.UI.WebControls.DataGrid dgQtnVAS;
		protected System.Web.UI.WebControls.Button btnOk;
		DataSet dsQtnVAS = null;
		protected System.Web.UI.WebControls.Label lblErrorMsg;
		DataSet  m_dsQtnVAS = null;
		private String strErrorMsg = "";
		string strFormID =null;
		protected System.Web.UI.WebControls.Button btnClose;
		public string strCloseWindowStatus;
		
		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			strFormID=Request.Params["FORMID"];
			if(!Page.IsPostBack)
			{
				
				if ((strFormID != null) && (strFormID.Trim()=="ShipmentDetails"))
				{
					m_dsQtnVAS = (DataSet)Session["SESSION_DS3"];
					dsQtnVAS = (DataSet)Session["SESSION_DS5"];
					
				}
				else
				{
					m_dsQtnVAS = (DataSet)Session["SESSION_DS1"];
					dsQtnVAS = (DataSet)Session["SESSION_DS4"];					
					
				}

				
				if(dsQtnVAS != null)
				{
					BindDGQtnVAS();
				}
			}
			else
			{
				if ((strFormID != null) && (strFormID.Trim()=="ShipmentDetails"))
				{
					
					if (Session["SESSION_DS3"] != null)
					{
						m_dsQtnVAS = (DataSet)Session["SESSION_DS3"];
					}

					if(Session["SESSION_DS5"] != null)
					{
						dsQtnVAS = (DataSet)Session["SESSION_DS5"];
					}

				}
				else
				{

					if (Session["SESSION_DS1"] != null)
					{
						m_dsQtnVAS = (DataSet)Session["SESSION_DS1"];
					}

					if(Session["SESSION_DS4"] != null)
					{
						dsQtnVAS = (DataSet)Session["SESSION_DS4"];
					}
				}
			}
		}


		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			base.OnInit(e);
			InitializeComponent();
			
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
			this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		

		private void CheckSelectedItem()
		{
			int i = 0;
			if ((strFormID != null) && (strFormID.Trim()=="ShipmentDetails"))
			{
				dsQtnVAS = (DataSet)Session["SESSION_DS5"];
						
			}
			else
			{
				dsQtnVAS = (DataSet)Session["SESSION_DS4"];
			}

			for(i=0;i<dgQtnVAS.Items.Count;i++)
			{
				CheckBox chkSelect = (CheckBox)dgQtnVAS.Items[i].FindControl("chkSelect");

				DataRow drCurrent = dsQtnVAS.Tables[0].Rows[i];

				if(chkSelect != null)
				{
					if(chkSelect.Checked == true)
					{
						drCurrent["isSelected"] = true;
					}
					else if(chkSelect.Checked == false)
					{
						drCurrent["isSelected"] = false;
					}
				}
			}
		}

		private void BindDGQtnVAS()
		{
			strErrorMsg = "";

			if((dsQtnVAS != null) && (dsQtnVAS.Tables[0].Rows.Count == 0))
			{
				//strErrorMsg = "No records....";
				strErrorMsg = Utility.GetLanguageText(ResourceType.UserMessage, "NO_RECORDS_FOUND", utility.GetUserCulture());
				dsQtnVAS = DomesticShipmentMgrDAL.GetEmptyVASDS();
			}
			
			dgQtnVAS.DataSource = dsQtnVAS;
			dgQtnVAS.DataBind();
			
			lblErrorMsg.Text = strErrorMsg;
		}
		protected void dgQtnVAS_PageChange(object sender, DataGridPageChangedEventArgs e)
		{
			dgQtnVAS.CurrentPageIndex = e.NewPageIndex;
			BindDGQtnVAS();
		}

		private void btnOk_Click(object sender, System.EventArgs e)
		{
			int cnt = dgQtnVAS.Items.Count;
			int i = 0;
			int j = 0;
			
			
			//Updates the checked item in the dataset
			CheckSelectedItem();

			if(cnt > 0)
			{
				if ((strFormID != null) && (strFormID.Trim()=="ShipmentDetails"))
				{
					m_dsQtnVAS = (DataSet)Session["SESSION_DS3"];
						
				}
				else
				{
					m_dsQtnVAS = (DataSet)Session["SESSION_DS1"];
				}
				//Add row VAS dataset
				if(m_dsQtnVAS.Tables[0].Rows.Count == 0)
				{
					m_dsQtnVAS = DomesticShipmentMgrDAL.GetEmptyVASDS();
				}
			}
			if(m_dsQtnVAS != null)
				j = m_dsQtnVAS.Tables[0].Rows.Count;
			for(i=0;i<cnt;i++)
			{
				CheckBox chkSelect = (CheckBox)dgQtnVAS.Items[i].FindControl("chkSelect");
				if(chkSelect != null)
				{
					if (chkSelect.Checked == true)
					{
						
						//Add a row to the DataSet
						DomesticShipmentMgrDAL.AddNewRowInVAS(m_dsQtnVAS);
					
						//Update the data in the dataset 
						DataRow drCurrent = m_dsQtnVAS.Tables[0].Rows[j];
						Label lblVASCode = (Label)dgQtnVAS.Items[i].FindControl("lblVASCode");
						if(lblVASCode != null)
						{
							drCurrent["vas_code"] = lblVASCode.Text;
						}

						Label lblSurcharge = (Label)dgQtnVAS.Items[i].FindControl("lblSurcharge"); 
						if(lblSurcharge != null)
						{
							drCurrent["surcharge"] = Convert.ToDecimal(lblSurcharge.Text); 
						}

						Label lblVASDesc = (Label)dgQtnVAS.Items[i].FindControl("lblVASDesc"); 
						if(lblVASDesc != null)
						{
							drCurrent["vas_description"] = lblVASDesc.Text; 
						}

						j++;
					}
				}
			}
			//Add to the dataset & put in session
			if(m_dsQtnVAS == null)
			{
				m_dsQtnVAS = DomesticShipmentMgrDAL.GetEmptyVASDS();
			}
			if ((strFormID != null) && (strFormID.Trim()=="ShipmentDetails"))
			{
				Session["SESSION_DS3"]=m_dsQtnVAS;
			}
			else
			{
				Session["SESSION_DS1"] = m_dsQtnVAS;
			}
			
			//Close the popup Window
			String sScript = "";
			sScript += "<script language=javascript>";
			if ((strFormID != null) && (strFormID.Trim()=="ShipmentDetails"))
			{
				sScript +="window.opener."+strFormID.Trim()+".btnPopulateVAS.disabled = true;";
				sScript +="window.opener.ShipmentDetails.hdnRefresh.value='REFRESH';";
				sScript +="window.opener.ShipmentDetails.action='ShipmentDetails.aspx';";
				sScript +="window.opener.ShipmentDetails.submit();";
			}
			else
			{
				sScript +="window.opener.DomesticShipment.hdnRefresh.value='REFRESH';";
				sScript +="window.opener.DomesticShipment.action='DomesticShipment.aspx';";
				sScript +="window.opener.DomesticShipment.submit();";
			}
		
			
			sScript += "  window.close();";
			sScript += "</script>";
			Response.Write(sScript);
			strCloseWindowStatus="C";
		}

		private void btnClose_Click(object sender, System.EventArgs e)
		{
			String sScript = "";
			sScript += "<script language=javascript>";
			sScript += "  window.close();";
			sScript += "</script>";
			Response.Write(sScript);
		}		
	}
}
