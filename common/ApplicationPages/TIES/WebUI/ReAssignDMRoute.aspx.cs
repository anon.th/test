using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.ties.DAL;
using com.common.classes;
using com.common.util;
using com.ties.classes;
using com.ties.BAL;
using Cambro.Web.DbCombo;
using System.Configuration;
using com.common.applicationpages;

namespace com.ties
{
	/// <summary>
	/// Summary description for ReAssignDMRoute.
	/// </summary>
	public class ReAssignDMRoute : com.common.applicationpages.BasePage
	{
		protected System.Web.UI.WebControls.Button btnSave;
		protected System.Web.UI.WebControls.Label lblVehicle_Flight_No;
		protected com.common.util.msTextBox txtDriver_Flight_AWB;
		protected System.Web.UI.HtmlControls.HtmlTable DeliveryManifestTable;
		protected com.common.util.msTextBox txtLineHaulCost;
		protected Cambro.Web.DbCombo.DbCombo DbComboPathCodeNew;
		protected System.Web.UI.WebControls.Label lblDlvryPath1;
		protected System.Web.UI.WebControls.TextBox txtPathDesc1;
		protected System.Web.UI.WebControls.Label lblDlvryPath;
		protected Cambro.Web.DbCombo.DbCombo DbComboPathCode;
		protected System.Web.UI.WebControls.TextBox txtPathDesc;
		protected System.Web.UI.WebControls.Label lblLineHaulCost;
		protected System.Web.UI.WebControls.Label lblDriver_Flight_AWB;
		protected System.Web.UI.WebControls.Label lblManifestedDate;
		protected System.Web.UI.WebControls.Label lblDelType;
		protected System.Web.UI.WebControls.RadioButton rbtnLong;
		protected System.Web.UI.WebControls.RadioButton rbtnShort;
		protected System.Web.UI.WebControls.RadioButton rbtnAir;
		protected System.Web.UI.WebControls.TextBox txtDelvryType;
		protected System.Web.UI.WebControls.Label lblVehicle_Flight_No1;
		protected System.Web.UI.WebControls.Label lblLineHaulCost1;
		protected System.Web.UI.WebControls.Label lblDriver_Flight_AWB1;
		protected System.Web.UI.WebControls.Label lblManifestedDate1;
		protected System.Web.UI.WebControls.Label lblRemarks;
		protected System.Web.UI.WebControls.TextBox txtRemarks;
		protected System.Web.UI.WebControls.ValidationSummary PageValidationSummary;
		protected System.Web.UI.WebControls.Label lblErrorMsg;
		
		DataSet m_sdsAssignedConsignment=null;
		//Utility utility = null;
		String appID = null;
		String enterpriseID = null;
		String userID=null;
		protected System.Web.UI.WebControls.Label lblDepartDate;
		protected System.Web.UI.WebControls.Label lblDepartDate1;
		protected System.Web.UI.WebControls.RequiredFieldValidator validDlvryPath;
		protected System.Web.UI.WebControls.RequiredFieldValidator reqFlightVehicleNo;
		protected System.Web.UI.WebControls.RequiredFieldValidator reqManifestedDate;
		protected System.Web.UI.WebControls.RequiredFieldValidator reqLineHaulCost1;
		protected System.Web.UI.WebControls.RequiredFieldValidator reqDriver_Flight_AWB1;
		protected System.Web.UI.WebControls.RequiredFieldValidator reqDepartDate1;
		protected com.common.util.msTextBox txtLineHaulCost1;
		protected com.common.util.msTextBox txtDriver_Flight_AWB1;
		protected Cambro.Web.DbCombo.DbCombo dbFlightVehicleNoNew;
		protected Cambro.Web.DbCombo.DbCombo dbFlightVehicleNo;
		protected System.Web.UI.HtmlControls.HtmlTable DeliveryManifestTable2;
		protected System.Web.UI.WebControls.Button btnCheckAll;
		protected System.Web.UI.WebControls.Button btnClearAll;
		protected System.Web.UI.HtmlControls.HtmlTable DeliveryManifestTable1;
		protected System.Web.UI.WebControls.DataGrid dgAssignedConsignment;
		protected System.Web.UI.HtmlControls.HtmlTable tableMain;
		protected System.Web.UI.WebControls.Label lblMainTitle;
		protected System.Web.UI.WebControls.Label lblRoutPath;
		protected System.Web.UI.WebControls.Label Label1;
		protected System.Web.UI.WebControls.Label Label2;
		protected Cambro.Web.DbCombo.DbCombo dbDepartDate;
		protected com.common.util.msTextBox txtManifestDate;
		protected Cambro.Web.DbCombo.DbCombo dbDepartDateNew;
		protected com.common.util.msTextBox txtManifestDate1;
		protected System.Web.UI.WebControls.TextBox txtDelPath;
		String deliveryType=null;
	
		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
//			utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings,Page.Session);
			appID = utility.GetAppID();
			enterpriseID = utility.GetEnterpriseID();
			userID = utility.GetUserID();
	
			DbComboPathCode.RegistrationKey = System.Configuration.ConfigurationSettings.AppSettings["DbComboRegKey"];	
			dbDepartDate.RegistrationKey = System.Configuration.ConfigurationSettings.AppSettings["DbComboRegKey"];
			dbFlightVehicleNo.RegistrationKey = System.Configuration.ConfigurationSettings.AppSettings["DbComboRegKey"];
			DbComboPathCodeNew.RegistrationKey = System.Configuration.ConfigurationSettings.AppSettings["DbComboRegKey"];
			dbDepartDateNew.RegistrationKey = System.Configuration.ConfigurationSettings.AppSettings["DbComboRegKey"];
			dbFlightVehicleNoNew.RegistrationKey = System.Configuration.ConfigurationSettings.AppSettings["DbComboRegKey"];
			///added regKey here
			
					

			if(!Page.IsPostBack)
			{
				reqFlightVehicleNo.ErrorMessage=lblVehicle_Flight_No1.Text;
				reqDriver_Flight_AWB1.ErrorMessage=lblDriver_Flight_AWB1.Text;
				btnCheckAll.Visible=false;
				btnClearAll.Visible=false;
				m_sdsAssignedConsignment=DeliveryManifestMgrDAL.GetEmptyAssignedSessionDS();
				m_sdsAssignedConsignment.Tables[0].Rows.Clear();
				BindAssignedConsignment();
//				String strDt = DateTime.Now.ToString("dd/MM/yyyy HH:mm");
//				txtManifestedDate1.Text=strDt;
			}
			else
			{				

				if(Session["SESSION_DS2"] != null)
				{
					m_sdsAssignedConsignment = (DataSet)Session["SESSION_DS2"];
				}			
			}
			SetDbComboServerStates();	
			SetdbFlightVehicleStates();
			SetdbManifestDateStates();
			reqFlightVehicleNo.ErrorMessage=lblVehicle_Flight_No1.Text;
			reqDriver_Flight_AWB1.ErrorMessage=lblDriver_Flight_AWB1.Text;
		}

		private void SetDbComboServerStates()
		{
			String strDeliveryType="";
			if (rbtnShort.Checked==true)
				strDeliveryType="S";
			else if (rbtnAir.Checked==true)
				strDeliveryType="A";
			else
				strDeliveryType="L";
						
			Hashtable hash = new Hashtable();
			hash.Add("strDeliveryType", strDeliveryType);			
			DbComboPathCodeNew.ServerState = hash;
			DbComboPathCodeNew.ServerStateSecretString = "kdfsjgnskajh[apq2-4uhg465654435879mnbfdm cxs;q4";
		}

		private void SetdbFlightVehicleStates()
		{			
			String strDeliveryPath="";	
			String strDeliveryPath1="";	
	//query
			Hashtable hash = new Hashtable();			
			strDeliveryPath=DbComboPathCode.Value;
			if (strDeliveryPath !=null && strDeliveryPath !="")
			{
				hash.Add("strDeliveryPath", strDeliveryPath);
			}			
			dbFlightVehicleNo.ServerState = hash;
			dbFlightVehicleNo.ServerStateSecretString = "kdfsjgnskajh[apq2-4uhg465654435879mnbfdm cxs;q4";
	//Assign
			Hashtable hash1 = new Hashtable();			
			strDeliveryPath1=DbComboPathCodeNew.Value;
			if (strDeliveryPath1 !=null && strDeliveryPath1 !="")
			{
				hash1.Add("strDeliveryPath", strDeliveryPath1);
			}			
			dbFlightVehicleNoNew.ServerState = hash1;
			dbFlightVehicleNoNew.ServerStateSecretString = "kdfsjgnskajh[apq2-4uhg465654435879mnbfdm cxs;q4";
		}

		private void SetdbManifestDateStates()
		{			
			String strDeliveryPath="";			
			String strFlightVehicle="";
			String strDeliveryPath1="";			
			String strFlightVehicle1="";
//query			
			strDeliveryPath=DbComboPathCode.Value;			
			strFlightVehicle=dbFlightVehicleNo.Value;
			Hashtable hash = new Hashtable();
			if (strDeliveryPath !=null && strDeliveryPath !="")
			{
				hash.Add("strDeliveryPath", strDeliveryPath);
			}
			if (strFlightVehicle != null && strFlightVehicle !="")
			{
				hash.Add("strFlightVehicle", strFlightVehicle);
			}
			dbDepartDate.ServerState = hash;
			dbDepartDate.ServerStateSecretString = "kdfsjgnskajh[apq2-4uhg465654435879mnbfdm cxs;q4";
//assign
			strDeliveryPath1=DbComboPathCodeNew.Value;			
			strFlightVehicle1=dbFlightVehicleNoNew.Value;
			Hashtable hash1 = new Hashtable();
			if (strDeliveryPath1 !=null && strDeliveryPath1 !="")
			{
				hash1.Add("strDeliveryPath", strDeliveryPath1);
			}
			if (strFlightVehicle1 != null && strFlightVehicle1 !="")
			{
				hash1.Add("strFlightVehicle", strFlightVehicle1);
			}
			dbDepartDateNew.ServerState = hash1;
			dbDepartDateNew.ServerStateSecretString = "kdfsjgnskajh[apq2-4uhg465654435879mnbfdm cxs;q4";
		}

		private void ClearDBComboPathCode()
		{
			lblErrorMsg.Text =""; 
			DbComboPathCodeNew.Text="";
			DbComboPathCodeNew.Value="";
			txtPathDesc1.Text="";
			dbFlightVehicleNoNew.Text="";
			dbFlightVehicleNoNew.Value="";
			dbDepartDateNew.Text="";
			dbDepartDateNew.Value="";
			txtLineHaulCost1.Text="";
			txtManifestDate1.Text="";
			txtDriver_Flight_AWB1.Text="";
			
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			base.OnInit(e);
			InitializeComponent();			
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{   
			this.DbComboPathCode.SelectedItemChanged += new System.EventHandler(this.DbComboPathCode_OnSelectedIndexChanged);
			this.dbFlightVehicleNo.SelectedItemChanged += new System.EventHandler(this.ClearManifestDate);
			this.dbDepartDate.SelectedItemChanged += new System.EventHandler(this.DMRecord);
			this.btnCheckAll.Click += new System.EventHandler(this.btnCheckAll_Click);
			this.btnClearAll.Click += new System.EventHandler(this.btnClearAll_Click);
			this.rbtnLong.CheckedChanged += new System.EventHandler(this.rbtnLong_CheckedChanged);
			this.rbtnShort.CheckedChanged += new System.EventHandler(this.rbtnShort_CheckedChanged);
			this.rbtnAir.CheckedChanged += new System.EventHandler(this.rbtnAir_CheckedChanged);
			this.DbComboPathCodeNew.SelectedItemChanged += new System.EventHandler(this.DbComboPathCodeNew_OnSelectedIndexChanged);
			this.dbFlightVehicleNoNew.SelectedItemChanged += new System.EventHandler(this.ClearManifestDateNew);
			this.dbDepartDateNew.SelectedItemChanged += new System.EventHandler(this.DMRecordNew);
			this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private void btnCheckAll_Click(object sender, System.EventArgs e)
		{
			lblErrorMsg.Text="";
			for(int i=0;i<dgAssignedConsignment.Items.Count;i++)
			{
				CheckBox chkSelect = (CheckBox)dgAssignedConsignment.Items[i].FindControl("chkSelect");
				chkSelect.Checked=true;
			}
		}

		private void btnClearAll_Click(object sender, System.EventArgs e)
		{
			lblErrorMsg.Text="";
			for(int i=0;i<dgAssignedConsignment.Items.Count;i++)
			{
				CheckBox chkSelect = (CheckBox)dgAssignedConsignment.Items[i].FindControl("chkSelect");
				chkSelect.Checked=false;
			}
		}

		private void RetrieveConsignment()
		{
			lblErrorMsg.Text="";			
			GetAssignedConsignment();
			btnCheckAll.Visible=true;
			btnClearAll.Visible=true;
			BindAssignedConsignment();
		}

//		private void btnRetrieveConsignment_Click(object sender, System.EventArgs e)
//		{
//			lblErrorMsg.Text="";			
//			GetAssignedConsignment();
//			btnCheckAll.Visible=true;
//			btnClearAll.Visible=true;
//			BindAssignedConsignment();
//		}

		private void GetAssignedConsignment()
		{	
			DataTable dtQueryCons = new DataTable();
			dtQueryCons.Columns.Add(new DataColumn("path_code", typeof(string)));
			dtQueryCons.Columns.Add(new DataColumn("delivery_type", typeof(string)));
			dtQueryCons.Columns.Add(new DataColumn("Flight_Vehicle_No", typeof(string)));
			dtQueryCons.Columns.Add(new DataColumn("AWB_Driver_Name", typeof(string)));
			dtQueryCons.Columns.Add(new DataColumn("line_haul_cost", typeof(decimal)));
			dtQueryCons.Columns.Add(new DataColumn("delivery_manifest_Datetime", typeof(DateTime)));
			dtQueryCons.Columns.Add(new DataColumn("Departure_Datetime", typeof(DateTime)));
			
			DataRow drEach = dtQueryCons.NewRow();							
			dtQueryCons.Rows.Add(drEach);
			DataSet dsQuery = new DataSet();
			dsQuery.Tables.Add(dtQueryCons);			
			
			DataRow drQuery=dsQuery.Tables[0].Rows[0];

			String strDelPathCode=null;
			strDelPathCode=DbComboPathCode.Value;
			if (strDelPathCode == "" || strDelPathCode==null)
			{
				lblErrorMsg.Text= Utility.GetLanguageText(ResourceType.UserMessage,"INVALID_PATH_CODE",utility.GetUserCulture());
				txtPathDesc.Text="";
				return;
			}
			drQuery["path_code"] = strDelPathCode;			

			if(txtDelvryType.Text.Trim() !="")			
				drQuery["delivery_type"]=txtDelvryType.Text.Trim();			
			if (dbFlightVehicleNo.Value != "") 
				drQuery["flight_vehicle_no"]=dbFlightVehicleNo.Value;
			if (txtDriver_Flight_AWB.Text.Trim() != "" )
				drQuery["awb_driver_name"]=txtDriver_Flight_AWB.Text.Trim();
			if (txtLineHaulCost.Text.Trim() != "" )
				drQuery["line_haul_cost"]=Convert.ToDecimal(txtLineHaulCost.Text.Trim());
			//Comment by GwanG on 16June08
			//if (dbDepartDate.Value !="" ) 
			//	drQuery["delivery_manifest_datetime"]=System.DateTime.ParseExact(dbDepartDate.Value,"dd/MM/yyyy HH:mm",null);
			//if (txtManifestDate.Text.Trim().Length > 0 ) 
			//	drQuery["Departure_Datetime"]=System.DateTime.ParseExact(txtManifestDate.Text.Trim(),"dd/MM/yyyy HH:mm",null);
			if(dbDepartDate.Value != "")
				drQuery["departure_datetime"] = System.DateTime.ParseExact(dbDepartDate.Value,"dd/MM/yyyy HH:mm",null);
			if(txtManifestDate.Text.Trim().Length > 0)
				drQuery["delivery_manifest_datetime"]=System.DateTime.ParseExact(txtManifestDate.Text.Trim(),"dd/MM/yyyy HH:mm",null);
			//End Comment & Added
			
			m_sdsAssignedConsignment = (DataSet)DeliveryManifestMgrDAL.GetAssignedConsignmentList(appID,enterpriseID,0,0,dsQuery);			
			Session["SESSION_DS2"] = m_sdsAssignedConsignment;

			if (m_sdsAssignedConsignment.Tables[0].Rows.Count<=0)
			{
				lblErrorMsg.Text= Utility.GetLanguageText(ResourceType.UserMessage,"NO_CONSIGN_FOUND",utility.GetUserCulture());
			}

		}

		private void BindAssignedConsignment()
		{			
			dgAssignedConsignment.DataSource = m_sdsAssignedConsignment;
			dgAssignedConsignment.DataBind();
			Session["SESSION_DS2"] = m_sdsAssignedConsignment;
		}

		
		[Cambro.Web.DbCombo.ResultsMethodAttribute(true)]
		public static object DbComboPathCodeSelect(Cambro.Web.DbCombo.ServerMethodArgs args)
		{
			Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings,HttpContext.Current.Session);
			String strAppID = util.GetAppID();
			String strEnterpriseID = util.GetEnterpriseID();
			DataSet dataset = DbComboDAL.PathCodeWithOutWQuery(strAppID,strEnterpriseID,args);	
			return dataset;
		}

		[Cambro.Web.DbCombo.ResultsMethodAttribute(true)]
		public static object DbComboPathCodeSelectNew(Cambro.Web.DbCombo.ServerMethodArgs args)
		{
			Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings,HttpContext.Current.Session);
			String strAppID = util.GetAppID();
			String strEnterpriseID = util.GetEnterpriseID();
			String strDelType = "L";			
			String strWhereClause="";
			if(args.ServerState != null && args.ServerState.Authenticate("kdfsjgnskajh[apq2-4uhg465654435879mnbfdm cxs;q4"))
			{
				if(args.ServerState["strDeliveryType"] != null && args.ServerState["strDeliveryType"].ToString().Length > 0)
				{
					strDelType = args.ServerState["strDeliveryType"].ToString();
					strWhereClause=" and Delivery_Type='"+strDelType+"'";
				}				
			}
			strWhereClause += " and Delivery_Type <> 'W'";
			
			DataSet dataset = DbComboDAL.PathCodeQuery(strAppID,strEnterpriseID,args, strWhereClause);	
			return dataset;
		}

		[Cambro.Web.DbCombo.ResultsMethodAttribute(true)]
		public static object ShowFlightVehicleNo(Cambro.Web.DbCombo.ServerMethodArgs args)
		{
			Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings,HttpContext.Current.Session);
			String strAppID = util.GetAppID();
			String strEnterpriseID = util.GetEnterpriseID();
			String strDelPath="";			
			String strWhereClause="";

			if(args.ServerState != null && args.ServerState.Authenticate("kdfsjgnskajh[apq2-4uhg465654435879mnbfdm cxs;q4"))
			{					
				if(args.ServerState["strDeliveryPath"] != null && args.ServerState["strDeliveryPath"].ToString().Length > 0)
				{
					strDelPath = args.ServerState["strDeliveryPath"].ToString();
					strWhereClause+=" and Path_Code='"+strDelPath+"'";
				}									
			}
			
			DataSet dataset = DbComboDAL.FlightVehicleQuery(strAppID,strEnterpriseID,args, strWhereClause);	
			return dataset;
		}

		[Cambro.Web.DbCombo.ResultsMethodAttribute(true)]
		public static object ShowManifestedDate(Cambro.Web.DbCombo.ServerMethodArgs args)
		{
			Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings,HttpContext.Current.Session);
			String strAppID = util.GetAppID();
			String strEnterpriseID = util.GetEnterpriseID();			
			String strDelPath="";
			String strFltVeh="";
			String strWhereClause="";

			if(args.ServerState != null && args.ServerState.Authenticate("kdfsjgnskajh[apq2-4uhg465654435879mnbfdm cxs;q4"))
			{			
				if(args.ServerState["strDeliveryPath"] != null && args.ServerState["strDeliveryPath"].ToString().Length > 0)
				{
					strDelPath = args.ServerState["strDeliveryPath"].ToString();
					strWhereClause+=" and Path_Code='"+strDelPath+"'";
				}				
				if(args.ServerState["strFlightVehicle"] != null && args.ServerState["strFlightVehicle"].ToString().Length > 0)
				{
					strFltVeh = args.ServerState["strFlightVehicle"].ToString();
					strWhereClause+=" and Flight_Vehicle_No='"+strFltVeh+"'";
				}				
			}
			//Comment by Gwang on 16June08 
			//DataSet dataset = DbComboDAL.DeliveryDatetime(strAppID,strEnterpriseID,args, strWhereClause);	
			DataSet dataset = DbComboDAL.DepartDatetime(strAppID,strEnterpriseID,args, strWhereClause);
			//End Comment & Added
			return dataset;
		}

		protected void DbComboPathCode_OnSelectedIndexChanged(object sender, System.EventArgs e)
		{
			String strPathCode = DbComboPathCode.Value;			

			String strPathDesc_Type=null;
			strPathDesc_Type = TIESUtility.GetPathDesc_Type(appID,enterpriseID,strPathCode);
			char ch=',';
			if (strPathDesc_Type ==null)
			{
				lblErrorMsg.Text= Utility.GetLanguageText(ResourceType.UserMessage,"NO_RECORD_FOUND_PATH",utility.GetUserCulture());
				return;
			}		

			if ( strPathDesc_Type.Trim() !="")
			{
				String[] str1  = strPathDesc_Type.Split(ch);
				txtDelvryType.Text=str1[0].Trim();
				txtPathDesc.Text=str1[1].Trim();

				////boon  -- 2011/01/12 -start
				switch (str1[0].Trim())
				{
					case "A":
						txtDelPath.Text = "Air Route";
						rbtnAir.Checked = true;
						rbtnShort.Checked = false;
						rbtnLong.Checked = false;
						rbtnAir.Enabled = true;
						rbtnShort.Enabled = false;
						rbtnLong.Enabled = true;					
						this.rbtnAir_CheckedChanged(rbtnAir, EventArgs.Empty);						
						break;

					case "S":
						txtDelPath.Text = "Short Route";
						rbtnShort.Checked = true;
						rbtnAir.Checked = false;
						rbtnLong.Checked = false;
						rbtnAir.Enabled = false;
						rbtnShort.Enabled = true;
						rbtnLong.Enabled = false;
						this.rbtnShort_CheckedChanged(rbtnShort, EventArgs.Empty);
						break;

					case "L":
						txtDelPath.Text = "Linehaul";
						rbtnLong.Checked = true;
						rbtnAir.Checked = false;
						rbtnShort.Checked = false;
						rbtnAir.Enabled = true;
						rbtnShort.Enabled = false;						
						rbtnLong.Enabled = true;						
						this.rbtnLong_CheckedChanged(rbtnLong, EventArgs.Empty);
						break;
				}
				////boon  -- 2011/01/12 -end
			}

			if (txtDelvryType.Text.Trim()=="A")
			{
				if(!utility.GetUserCulture().ToUpper().Equals("EN-US"))
				{
					lblVehicle_Flight_No.Text = Utility.GetLanguageText(ResourceType.ScreenLabel, "Flight No", utility.GetUserCulture());
					lblDriver_Flight_AWB.Text = Utility.GetLanguageText(ResourceType.ScreenLabel, "Flight AWB", utility.GetUserCulture());
				} 
				else 
				{
					lblVehicle_Flight_No.Text = "Flight No";
					lblDriver_Flight_AWB.Text = "Flight AWB";		
				}
				//lblVehicle_Flight_No.Text="Flight No";
				//lblDriver_Flight_AWB.Text="Flight AWB";
			}
			else
			{
				if(!utility.GetUserCulture().ToUpper().Equals("EN-US"))
				{
					lblVehicle_Flight_No.Text = Utility.GetLanguageText(ResourceType.ScreenLabel, "Vehicle No", utility.GetUserCulture());
					lblDriver_Flight_AWB.Text = Utility.GetLanguageText(ResourceType.ScreenLabel, "Driver Name", utility.GetUserCulture());
				} 
				else 
				{
					lblVehicle_Flight_No.Text = "Vehicle No";
					lblDriver_Flight_AWB.Text = "Driver Name";		
				}
				//lblVehicle_Flight_No.Text="Vehicle No";
				//lblDriver_Flight_AWB.Text="Driver Name";
			}

			lblErrorMsg.Text="";
			dbFlightVehicleNo.Text="";
			dbDepartDate.Text="";
			txtLineHaulCost.Text="";
			txtDriver_Flight_AWB.Text="";
			txtManifestDate.Text="";
			ClearConsignment();
		}
		protected void DbComboPathCodeNew_OnSelectedIndexChanged(object sender, System.EventArgs e)
		{
			String strPathCode = DbComboPathCodeNew.Value;
			if (strPathCode=="")
			{
				lblErrorMsg.Text= Utility.GetLanguageText(ResourceType.UserMessage,"INVALID_PATH_CODE",utility.GetUserCulture());
				return;
			}
			String strPathDescription = null;
			strPathDescription = TIESUtility.GetPathCodeDesc(appID,enterpriseID,strPathCode);
			if (strPathDescription==null)
			{
				lblErrorMsg.Text= Utility.GetLanguageText(ResourceType.UserMessage,"NO_RECORD_FOUND_PATH",utility.GetUserCulture());
				return;
			}
			txtPathDesc1.Text =strPathDescription;

			lblErrorMsg.Text="";
			dbFlightVehicleNoNew.Text="";
			dbDepartDateNew.Text="";
			txtLineHaulCost1.Text="";
			txtDriver_Flight_AWB1.Text="";
			txtManifestDate1.Text="";
			txtRemarks.Text="";
		}

		private void rbtnShort_CheckedChanged(object sender, System.EventArgs e)
		{
			if (rbtnShort.Checked==true)
			{
				deliveryType="S";

				if(!utility.GetUserCulture().ToUpper().Equals("EN-US"))
				{
					lblVehicle_Flight_No1.Text = Utility.GetLanguageText(ResourceType.ScreenLabel, "Vehicle No", utility.GetUserCulture());
					lblDriver_Flight_AWB1.Text = Utility.GetLanguageText(ResourceType.ScreenLabel, "Driver Name", utility.GetUserCulture());
				} 
				else 
				{
					lblVehicle_Flight_No1.Text = "Vehicle No";
					lblDriver_Flight_AWB1.Text = "Driver Name";		
				}
			}
			reqFlightVehicleNo.ErrorMessage=lblVehicle_Flight_No1.Text;
			reqDriver_Flight_AWB1.ErrorMessage=lblDriver_Flight_AWB1.Text;
			ClearDBComboPathCode();
		}

		private void rbtnLong_CheckedChanged(object sender, System.EventArgs e)
		{
			if (rbtnLong.Checked==true)
			{
				deliveryType="L";
				if(!utility.GetUserCulture().ToUpper().Equals("EN-US"))
				{
					lblVehicle_Flight_No1.Text = Utility.GetLanguageText(ResourceType.ScreenLabel, "Vehicle No", utility.GetUserCulture());
					lblDriver_Flight_AWB1.Text = Utility.GetLanguageText(ResourceType.ScreenLabel, "Driver Name", utility.GetUserCulture());
				} 
				else 
				{
					lblVehicle_Flight_No1.Text = "Vehicle No";
					lblDriver_Flight_AWB1.Text = "Driver Name";		
				}				
			}
			reqFlightVehicleNo.ErrorMessage=lblVehicle_Flight_No1.Text;
			reqDriver_Flight_AWB1.ErrorMessage=lblDriver_Flight_AWB1.Text;
			ClearDBComboPathCode();
		}

		private void rbtnAir_CheckedChanged(object sender, System.EventArgs e)
		{
			if (rbtnAir.Checked==true)
			{
				deliveryType="A";
				if(!utility.GetUserCulture().ToUpper().Equals("EN-US"))
				{
					lblVehicle_Flight_No1.Text = Utility.GetLanguageText(ResourceType.ScreenLabel, "Flight No", utility.GetUserCulture());
					lblDriver_Flight_AWB1.Text = Utility.GetLanguageText(ResourceType.ScreenLabel, "Flight AWB", utility.GetUserCulture());
				} 
				else 
				{
					lblVehicle_Flight_No1.Text = "Flight No";
					lblDriver_Flight_AWB1.Text = "Flight AWB";		
				}			
			}
			else if (rbtnAir.Checked==false)
			{
				if(!utility.GetUserCulture().ToUpper().Equals("EN-US"))
				{
					lblVehicle_Flight_No1.Text = Utility.GetLanguageText(ResourceType.ScreenLabel, "Vehicle No", utility.GetUserCulture());
					lblDriver_Flight_AWB1.Text = Utility.GetLanguageText(ResourceType.ScreenLabel, "Driver Name", utility.GetUserCulture());
				} 
				else 
				{
					lblVehicle_Flight_No1.Text = "Vehicle No";
					lblDriver_Flight_AWB1.Text = "Driver Name";		
				}
				//lblVehicle_Flight_No1.Text="Vehicle No";
				//lblDriver_Flight_AWB1.Text="Driver Name";				
			}
			reqFlightVehicleNo.ErrorMessage=lblVehicle_Flight_No1.Text;
			reqDriver_Flight_AWB1.ErrorMessage=lblDriver_Flight_AWB1.Text;
			ClearDBComboPathCode();
		}

	/*	private void btnSave_Click(object sender, System.EventArgs e)
		{	
			bool haveConsignments=false;					
			DataSet dsConsignment=GetAssignedCons();

			for(int i=0;i<dgAssignedConsignment.Items.Count;i++)
			{
				CheckBox chkSelect = (CheckBox)dgAssignedConsignment.Items[i].FindControl("chkSelect");
				if(chkSelect!=null && chkSelect.Checked==true)
				{
					Label lblConsgmntNo=(Label) dgAssignedConsignment.Items[i].FindControl("lblConsgmntNo");
					Label lblDGOrigin=(Label) dgAssignedConsignment.Items[i].FindControl("lblDGOrigin");
					Label lblDGDestination=(Label) dgAssignedConsignment.Items[i].FindControl("lblDGDestination");
					Label lblDGRoute=(Label) dgAssignedConsignment.Items[i].FindControl("lblDGRoute");
			
					DataRow drNew = dsConsignment.Tables[0].NewRow();
					drNew["Consignment_No"]=lblConsgmntNo.Text.Trim();
					drNew["Origin_State_Code"]=lblDGOrigin.Text.Trim();
					drNew["Destination_State_Code"]=lblDGDestination.Text.Trim();
					drNew["Route_Code"]=lblDGRoute.Text.Trim();																							
					dsConsignment.Tables[0].Rows.Add(drNew);
					
					haveConsignments=true;
				}
			}

			if (haveConsignments==false)
			{
				lblErrorMsg.Text= Utility.GetLanguageText(ResourceType.UserMessage,"NO_CONSIGN_SELECTED",utility.GetUserCulture());
				return;
			}
			
			// OLD DM to Overwrite
			DataSet dsDMOld=GetAssignedDM();
			DataRow drOld = dsDMOld.Tables[0].NewRow();

			if (DbComboPathCode.Value !="")
				drOld["Path_Code"]=DbComboPathCode.Value;
			if (dbFlightVehicleNo.Value!="")
				drOld["Flight_Vehicle_No"]=dbFlightVehicleNo.Value;
			if (dbManifestedDate.Value!="")
				drOld["Delivery_Manifest_DateTime"]=System.DateTime.ParseExact(dbManifestedDate.Value,"dd/MM/yyyy HH:mm",null);			
			if (txtManifestDate.Text.Trim() !="")
				drOld["Departure_DateTime"]=System.DateTime.ParseExact(txtManifestDate.Text.Trim(),"dd/MM/yyyy HH:mm",null);			
			if(txtDriver_Flight_AWB.Text.Trim() !="")
				drOld["AWB_Driver_Name"]=txtDriver_Flight_AWB.Text.Trim();
			if(txtLineHaulCost.Text.Trim() !="")
				drOld["Line_Haul_Cost"]=Convert.ToDecimal(txtLineHaulCost.Text.Trim());
			
			dsDMOld.Tables[0].Rows.Add(drOld);

			// NEW DM to Re-Assign
			DataSet dsDMNew=GetAssignedDM();
			DataRow drN = dsDMNew.Tables[0].NewRow();
			

			if (DbComboPathCodeNew.Value !="")
				drN["Path_Code"]=DbComboPathCodeNew.Value;
			if (dbFlightVehicleNoNew.Value!="")
				drN["Flight_Vehicle_No"]=dbFlightVehicleNoNew.Value;
			if (dbManifestedDateNew.Value!="")
				drN["Delivery_Manifest_DateTime"]=System.DateTime.ParseExact(dbManifestedDateNew.Value,"dd/MM/yyyy HH:mm",null);
			if (txtManifestDate1.Text.Trim() !="")
				drN["Departure_DateTime"]=System.DateTime.ParseExact(txtManifestDate1.Text.Trim(),"dd/MM/yyyy HH:mm",null);
			if(txtDriver_Flight_AWB1.Text.Trim() !="")
				drN["AWB_Driver_Name"]=txtDriver_Flight_AWB1.Text.Trim();
			if(txtLineHaulCost1.Text.Trim() !="")
				drN["Line_Haul_Cost"]=Convert.ToDecimal(txtLineHaulCost1.Text.Trim());
			if(txtRemarks.Text.Trim() !="")
				drN["Remark"]=txtRemarks.Text.Trim();

			dsDMNew.Tables[0].Rows.Add(drN);

			try
			{
				//Re-Assign the Delivery Manifest
				DeliveryManifestMgrDAL.ReAssignRouteDM(appID, enterpriseID, dsConsignment, dsDMNew, dsDMOld);
				lblErrorMsg.Text= Utility.GetLanguageText(ResourceType.UserMessage,"INS_SUCCESSFULLY_RE",utility.GetUserCulture());
			}
			catch (Exception exp)
			{
				String strMsg=exp.Message; 
				if(strMsg.IndexOf("PRIMARY KEY") != -1 )
				{
					strMsg = Utility.GetLanguageText(ResourceType.UserMessage,"DUP_KEY_FOUND_CNSG_NO",utility.GetUserCulture());	
				}			
				if (strMsg.IndexOf("unique") != -1)
				{
					strMsg = Utility.GetLanguageText(ResourceType.UserMessage,"DUP_KEY_FOUND_CNSG_NO",utility.GetUserCulture());
				}
				//Foreign Key Violation ---> SQL Server
				if(strMsg.IndexOf("delivery_manifest_datetime") != -1)
				{
					strMsg = Utility.GetLanguageText(ResourceType.UserMessage,"INVALID_DEL_MANIFEST_DATE",utility.GetUserCulture());
				}
				else if(strMsg.IndexOf("flight_vehicle_no") != -1)
				{
					strMsg = Utility.GetLanguageText(ResourceType.UserMessage,"INVALID_FLIGHT_VEHICLE_NO",utility.GetUserCulture());
				}
				else if(strMsg.IndexOf("Delivery_Manifest") != -1)
				{
					strMsg = Utility.GetLanguageText(ResourceType.UserMessage,"INVALID_DEL_MANIFEST_DATE",utility.GetUserCulture());					
				}						
				else if(strMsg.IndexOf("Route_Code") != -1)
				{
					strMsg = Utility.GetLanguageText(ResourceType.UserMessage,"INVALID_ROUTE_CODE",utility.GetUserCulture());					
				}
				
			lblErrorMsg.Text=strMsg;
			}
		}*/

		private void btnSave_Click(object sender, System.EventArgs e)
		{	
			deliveryType = TIESUtility.GetDeliveryType(appID,enterpriseID,DbComboPathCode.Value);
			bool haveConsignments=false;					
			DataSet dsConsignment=GetAssignedCons();
			String newRouteCode=null;

			//Reassign Only the Delivery Path to Delivery Path
			//Make sure that the radio button is a Short Route
			#region Gwang
			if (rbtnShort.Checked==true)
			{
				//Make sure that the selected existing path/route is in Route_path Table
				if(TIESUtility.IsInRouteCode(appID,enterpriseID,DbComboPathCode.Value))
				{
					for(int i=0;i<dgAssignedConsignment.Items.Count;i++)
					{
						CheckBox chkSelect = (CheckBox)dgAssignedConsignment.Items[i].FindControl("chkSelect");
						if(chkSelect!=null && chkSelect.Checked==true)
						{
							Label lblConsgmntNo=(Label) dgAssignedConsignment.Items[i].FindControl("lblConsgmntNo");
							Label lblDGOrigin=(Label) dgAssignedConsignment.Items[i].FindControl("lblDGOrigin");
							Label lblDGDestination=(Label) dgAssignedConsignment.Items[i].FindControl("lblDGDestination");
							Label lblDGRoute=(Label) dgAssignedConsignment.Items[i].FindControl("lblDGRoute");
			
							DataRow drNew = dsConsignment.Tables[0].NewRow();
							drNew["Consignment_No"]=lblConsgmntNo.Text.Trim();
							drNew["Origin_State_Code"]=lblDGOrigin.Text.Trim();
							drNew["Destination_State_Code"]=lblDGDestination.Text.Trim();
							drNew["Route_Code"]=lblDGRoute.Text.Trim();																							
							dsConsignment.Tables[0].Rows.Add(drNew);
											
							haveConsignments=true;
						}
					}

					if (haveConsignments==false)
					{
						lblErrorMsg.Text= Utility.GetLanguageText(ResourceType.UserMessage,"NO_CONSIGN_SELECTED",utility.GetUserCulture());
						return;
					}
			
					// OLD DM to Overwrite
					DataSet dsDMOld=GetAssignedDM();
					DataRow drOld = dsDMOld.Tables[0].NewRow();

					if (DbComboPathCode.Value !="")
						drOld["Path_Code"]=DbComboPathCode.Value;
					if (dbFlightVehicleNo.Value!="")
						drOld["Flight_Vehicle_No"]=dbFlightVehicleNo.Value;
					/*Comment by Gwang on 17June08
					 * if (dbDepartDate.Value!="")
						drOld["Delivery_Manifest_DateTime"]=System.DateTime.ParseExact(dbDepartDate.Value,"dd/MM/yyyy HH:mm",null);			
					if (txtManifestDate.Text.Trim() !="")
						drOld["Departure_DateTime"]=System.DateTime.ParseExact(txtManifestDate.Text.Trim(),"dd/MM/yyyy HH:mm",null);	
					End Comment*/
					if (dbDepartDate.Value!="")
						drOld["Departure_DateTime"]=System.DateTime.ParseExact(dbDepartDate.Value,"dd/MM/yyyy HH:mm",null);			
					if (txtManifestDate.Text.Trim() !="")
						drOld["Delivery_Manifest_DateTime"]=System.DateTime.ParseExact(txtManifestDate.Text.Trim(),"dd/MM/yyyy HH:mm",null);			
					if(txtDriver_Flight_AWB.Text.Trim() !="")
						drOld["AWB_Driver_Name"]=txtDriver_Flight_AWB.Text.Trim();
					if(txtLineHaulCost.Text.Trim() !="")
						drOld["Line_Haul_Cost"]=Convert.ToDecimal(txtLineHaulCost.Text.Trim());
			
					dsDMOld.Tables[0].Rows.Add(drOld);

					// NEW DM to Re-Assign
					DataSet dsDMNew=GetAssignedDM();
					DataRow drN = dsDMNew.Tables[0].NewRow();
			

					if (DbComboPathCodeNew.Value !="")
					{
						drN["Path_Code"]=DbComboPathCodeNew.Value;
						newRouteCode = TIESUtility.GetRouteCodeByPathCode(appID,enterpriseID,DbComboPathCodeNew.Value);
					}
					if (dbFlightVehicleNoNew.Value!="")
						drN["Flight_Vehicle_No"]=dbFlightVehicleNoNew.Value;
					/*Comment by GwanG on 17June08
					 * if (dbDepartDateNew.Value!="")
						drN["Delivery_Manifest_DateTime"]=System.DateTime.ParseExact(dbDepartDateNew.Value,"dd/MM/yyyy HH:mm",null);
					if (txtManifestDate1.Text.Trim() !="")
						drN["Departure_DateTime"]=System.DateTime.ParseExact(txtManifestDate1.Text.Trim(),"dd/MM/yyyy HH:mm",null);
					End Comment*/
					if (dbDepartDateNew.Value!="")
						drN["Departure_DateTime"]=System.DateTime.ParseExact(dbDepartDateNew.Value,"dd/MM/yyyy HH:mm",null);
					if (txtManifestDate1.Text.Trim() !="")
						drN["Delivery_Manifest_DateTime"]=System.DateTime.ParseExact(txtManifestDate1.Text.Trim(),"dd/MM/yyyy HH:mm",null);
					if(txtDriver_Flight_AWB1.Text.Trim() !="")
						drN["AWB_Driver_Name"]=txtDriver_Flight_AWB1.Text.Trim();
					if(txtLineHaulCost1.Text.Trim() !="")
						drN["Line_Haul_Cost"]=Convert.ToDecimal(txtLineHaulCost1.Text.Trim());
					if(txtRemarks.Text.Trim() !="")
						drN["Remark"]=txtRemarks.Text.Trim();

					dsDMNew.Tables[0].Rows.Add(drN);

					try
					{
						//Re-Assign the Delivery Manifest
						DeliveryManifestMgrDAL.ReAssignRouteDM(appID, enterpriseID, dsConsignment, dsDMNew, dsDMOld,newRouteCode,6,userID);

						//boon -- Reset dgAssignedConsignment		(1)
						RetrieveConsignment(); //Retreive Consignments

						lblErrorMsg.Text= Utility.GetLanguageText(ResourceType.UserMessage,"INS_SUCCESSFULLY_RE",utility.GetUserCulture());						
					}
					catch (Exception exp)
					{
						String strMsg=exp.Message; 
						if(strMsg.IndexOf("PRIMARY KEY") != -1 )
						{
							strMsg = Utility.GetLanguageText(ResourceType.UserMessage,"DUP_KEY_FOUND_CNSG_NO",utility.GetUserCulture());	
						}			
						if (strMsg.IndexOf("unique") != -1)
						{
							strMsg = Utility.GetLanguageText(ResourceType.UserMessage,"DUP_KEY_FOUND_CNSG_NO",utility.GetUserCulture());
						}
						//Foreign Key Violation ---> SQL Server
						if(strMsg.IndexOf("delivery_manifest_datetime") != -1)
						{
							strMsg = Utility.GetLanguageText(ResourceType.UserMessage,"INVALID_DEL_MANIFEST_DATE",utility.GetUserCulture());
						}
						else if(strMsg.IndexOf("flight_vehicle_no") != -1)
						{
							strMsg = Utility.GetLanguageText(ResourceType.UserMessage,"INVALID_FLIGHT_VEHICLE_NO",utility.GetUserCulture());
						}
						else if(strMsg.IndexOf("Delivery_Manifest") != -1)
						{
							strMsg = Utility.GetLanguageText(ResourceType.UserMessage,"INVALID_DEL_MANIFEST_DATE",utility.GetUserCulture());					
						}						
						else if(strMsg.IndexOf("Route_Code") != -1)
						{
							strMsg = Utility.GetLanguageText(ResourceType.UserMessage,"INVALID_ROUTE_CODE",utility.GetUserCulture());					
						}
				
						lblErrorMsg.Text=strMsg;
					}

				}
			}
				#endregion

			#region Aoo
			else if(deliveryType.Equals("A") && rbtnLong.Checked == true)
			{
				for(int i=0;i<dgAssignedConsignment.Items.Count;i++)
				{
					CheckBox chkSelect = (CheckBox)dgAssignedConsignment.Items[i].FindControl("chkSelect");
					if(chkSelect!=null && chkSelect.Checked==true)
					{
						Label lblConsgmntNo=(Label) dgAssignedConsignment.Items[i].FindControl("lblConsgmntNo");
						Label lblDGOrigin=(Label) dgAssignedConsignment.Items[i].FindControl("lblDGOrigin");
						Label lblDGDestination=(Label) dgAssignedConsignment.Items[i].FindControl("lblDGDestination");
						Label lblDGRoute=(Label) dgAssignedConsignment.Items[i].FindControl("lblDGRoute");
			
						DataRow drNew = dsConsignment.Tables[0].NewRow();
						drNew["Consignment_No"]=lblConsgmntNo.Text.Trim();
						drNew["Origin_State_Code"]=lblDGOrigin.Text.Trim();
						drNew["Destination_State_Code"]=lblDGDestination.Text.Trim();
						drNew["Route_Code"]=lblDGRoute.Text.Trim();																							
						dsConsignment.Tables[0].Rows.Add(drNew);
											
						haveConsignments=true;
					}
				}

				if (haveConsignments==false)
				{
					lblErrorMsg.Text= Utility.GetLanguageText(ResourceType.UserMessage,"NO_CONSIGN_SELECTED",utility.GetUserCulture());
					return;
				}
			
				// OLD DM to Overwrite
				DataSet dsDMOld=GetAssignedDM();
				DataRow drOld = dsDMOld.Tables[0].NewRow();

				if (DbComboPathCode.Value !="")
					drOld["Path_Code"]=DbComboPathCode.Value;
				if (dbFlightVehicleNo.Value!="")
					drOld["Flight_Vehicle_No"]=dbFlightVehicleNo.Value;
				/*Comment by GwanG on 17June08
				 * if (dbDepartDate.Value!="")
					drOld["Delivery_Manifest_DateTime"]=System.DateTime.ParseExact(dbDepartDate.Value,"dd/MM/yyyy HH:mm",null);			
				if (txtManifestDate.Text.Trim() !="")
					drOld["Departure_DateTime"]=System.DateTime.ParseExact(txtManifestDate.Text.Trim(),"dd/MM/yyyy HH:mm",null);	*/
				if (dbDepartDate.Value!="")
					drOld["Departure_DateTime"]=System.DateTime.ParseExact(dbDepartDate.Value,"dd/MM/yyyy HH:mm",null);			
				if (txtManifestDate.Text.Trim() !="")
					drOld["Delivery_Manifest_DateTime"]=System.DateTime.ParseExact(txtManifestDate.Text.Trim(),"dd/MM/yyyy HH:mm",null);	
				if(txtDriver_Flight_AWB.Text.Trim() !="")
					drOld["AWB_Driver_Name"]=txtDriver_Flight_AWB.Text.Trim();
				if(txtLineHaulCost.Text.Trim() !="")
					drOld["Line_Haul_Cost"]=Convert.ToDecimal(txtLineHaulCost.Text.Trim());
			
				dsDMOld.Tables[0].Rows.Add(drOld);

				// NEW DM to Re-Assign
				DataSet dsDMNew=GetAssignedDM();
				DataRow drN = dsDMNew.Tables[0].NewRow();
			

				if (DbComboPathCodeNew.Value !="")
				{
					drN["Path_Code"]=DbComboPathCodeNew.Value;
					newRouteCode = null;
				}
				if (dbFlightVehicleNoNew.Value!="")
					drN["Flight_Vehicle_No"]=dbFlightVehicleNoNew.Value;
				/*Comment by GwanG on 17June08
				 * if (dbDepartDateNew.Value!="")
					drN["Delivery_Manifest_DateTime"]=System.DateTime.ParseExact(dbDepartDateNew.Value,"dd/MM/yyyy HH:mm",null);
				if (txtManifestDate1.Text.Trim() !="")
					drN["Departure_DateTime"]=System.DateTime.ParseExact(txtManifestDate1.Text.Trim(),"dd/MM/yyyy HH:mm",null);*/
				if (dbDepartDateNew.Value!="")
					drN["Departure_DateTime"]=System.DateTime.ParseExact(dbDepartDateNew.Value,"dd/MM/yyyy HH:mm",null);
				if (txtManifestDate1.Text.Trim() !="")
					drN["Delivery_Manifest_DateTime"]=System.DateTime.ParseExact(txtManifestDate1.Text.Trim(),"dd/MM/yyyy HH:mm",null);
				if(txtDriver_Flight_AWB1.Text.Trim() !="")
					drN["AWB_Driver_Name"]=txtDriver_Flight_AWB1.Text.Trim();
				if(txtLineHaulCost1.Text.Trim() !="")
					drN["Line_Haul_Cost"]=Convert.ToDecimal(txtLineHaulCost1.Text.Trim());
				if(txtRemarks.Text.Trim() !="")
					drN["Remark"]=txtRemarks.Text.Trim();

				dsDMNew.Tables[0].Rows.Add(drN);

				try
				{
					//Re-Assign the Delivery Manifest
					DeliveryManifestMgrDAL.ReAssignRouteDM(appID, enterpriseID, dsConsignment, dsDMNew, dsDMOld,newRouteCode,7,userID);

					//boon -- Reset dgAssignedConsignment		(2)
					RetrieveConsignment(); //Retreive Consignments

					lblErrorMsg.Text= Utility.GetLanguageText(ResourceType.UserMessage,"INS_SUCCESSFULLY_RE",utility.GetUserCulture());
				}
				catch (Exception exp)
				{
					String strMsg=exp.Message; 
					if(strMsg.IndexOf("PRIMARY KEY") != -1 )
					{
						strMsg = Utility.GetLanguageText(ResourceType.UserMessage,"DUP_KEY_FOUND_CNSG_NO",utility.GetUserCulture());	
					}			
					if (strMsg.IndexOf("unique") != -1)
					{
						strMsg = Utility.GetLanguageText(ResourceType.UserMessage,"DUP_KEY_FOUND_CNSG_NO",utility.GetUserCulture());
					}
					//Foreign Key Violation ---> SQL Server
					if(strMsg.IndexOf("delivery_manifest_datetime") != -1)
					{
						strMsg = Utility.GetLanguageText(ResourceType.UserMessage,"INVALID_DEL_MANIFEST_DATE",utility.GetUserCulture());
					}
					else if(strMsg.IndexOf("flight_vehicle_no") != -1)
					{
						strMsg = Utility.GetLanguageText(ResourceType.UserMessage,"INVALID_FLIGHT_VEHICLE_NO",utility.GetUserCulture());
					}
					else if(strMsg.IndexOf("Delivery_Manifest") != -1)
					{
						strMsg = Utility.GetLanguageText(ResourceType.UserMessage,"INVALID_DEL_MANIFEST_DATE",utility.GetUserCulture());					
					}						
					else if(strMsg.IndexOf("Route_Code") != -1)
					{
						strMsg = Utility.GetLanguageText(ResourceType.UserMessage,"INVALID_ROUTE_CODE",utility.GetUserCulture());					
					}
				
					lblErrorMsg.Text=strMsg;
				}
			}
			else if(deliveryType.Equals("L") && rbtnLong.Checked == true)
			{
				for(int i=0;i<dgAssignedConsignment.Items.Count;i++)
				{
					CheckBox chkSelect = (CheckBox)dgAssignedConsignment.Items[i].FindControl("chkSelect");
					if(chkSelect!=null && chkSelect.Checked==true)
					{
						Label lblConsgmntNo=(Label) dgAssignedConsignment.Items[i].FindControl("lblConsgmntNo");
						Label lblDGOrigin=(Label) dgAssignedConsignment.Items[i].FindControl("lblDGOrigin");
						Label lblDGDestination=(Label) dgAssignedConsignment.Items[i].FindControl("lblDGDestination");
						Label lblDGRoute=(Label) dgAssignedConsignment.Items[i].FindControl("lblDGRoute");
			
						DataRow drNew = dsConsignment.Tables[0].NewRow();
						drNew["Consignment_No"]=lblConsgmntNo.Text.Trim();
						drNew["Origin_State_Code"]=lblDGOrigin.Text.Trim();
						drNew["Destination_State_Code"]=lblDGDestination.Text.Trim();
						drNew["Route_Code"]=lblDGRoute.Text.Trim();																							
						dsConsignment.Tables[0].Rows.Add(drNew);
											
						haveConsignments=true;
					}
				}

				if (haveConsignments==false)
				{
					lblErrorMsg.Text= Utility.GetLanguageText(ResourceType.UserMessage,"NO_CONSIGN_SELECTED",utility.GetUserCulture());
					return;
				}
			
				// OLD DM to Overwrite
				DataSet dsDMOld=GetAssignedDM();
				DataRow drOld = dsDMOld.Tables[0].NewRow();

				if (DbComboPathCode.Value !="")
					drOld["Path_Code"]=DbComboPathCode.Value;
				if (dbFlightVehicleNo.Value!="")
					drOld["Flight_Vehicle_No"]=dbFlightVehicleNo.Value;
				/*Comment by GwanG on 17June08
				*if (dbDepartDate.Value!="")
					drOld["Delivery_Manifest_DateTime"]=System.DateTime.ParseExact(dbDepartDate.Value,"dd/MM/yyyy HH:mm",null);			
				if (txtManifestDate.Text.Trim() !="")
					drOld["Departure_DateTime"]=System.DateTime.ParseExact(txtManifestDate.Text.Trim(),"dd/MM/yyyy HH:mm",null);			*/
				if (dbDepartDate.Value!="")
					drOld["Departure_DateTime"]=System.DateTime.ParseExact(dbDepartDate.Value,"dd/MM/yyyy HH:mm",null);			
				if (txtManifestDate.Text.Trim() !="")
					drOld["Delivery_Manifest_DateTime"]=System.DateTime.ParseExact(txtManifestDate.Text.Trim(),"dd/MM/yyyy HH:mm",null);			
				if(txtDriver_Flight_AWB.Text.Trim() !="")
					drOld["AWB_Driver_Name"]=txtDriver_Flight_AWB.Text.Trim();
				if(txtLineHaulCost.Text.Trim() !="")
					drOld["Line_Haul_Cost"]=Convert.ToDecimal(txtLineHaulCost.Text.Trim());
			
				dsDMOld.Tables[0].Rows.Add(drOld);

				// NEW DM to Re-Assign
				DataSet dsDMNew=GetAssignedDM();
				DataRow drN = dsDMNew.Tables[0].NewRow();
			

				if (DbComboPathCodeNew.Value !="")
				{
					drN["Path_Code"]=DbComboPathCodeNew.Value;
					newRouteCode = null;//TIESUtility.GetRouteCodeByPathCode(appID,enterpriseID,DbComboPathCodeNew.Value);
				}
				if (dbFlightVehicleNoNew.Value!="")
					drN["Flight_Vehicle_No"]=dbFlightVehicleNoNew.Value;
				/*Comment by GwanG on 17June08
				if (dbDepartDateNew.Value!="")
					drN["Delivery_Manifest_DateTime"]=System.DateTime.ParseExact(dbDepartDateNew.Value,"dd/MM/yyyy HH:mm",null);
				if (txtManifestDate1.Text.Trim() !="")
					drN["Departure_DateTime"]=System.DateTime.ParseExact(txtManifestDate1.Text.Trim(),"dd/MM/yyyy HH:mm",null);*/
				if (dbDepartDateNew.Value!="")
					drN["Departure_DateTime"]=System.DateTime.ParseExact(dbDepartDateNew.Value,"dd/MM/yyyy HH:mm",null);
				if (txtManifestDate1.Text.Trim() !="")
					drN["Delivery_Manifest_DateTime"]=System.DateTime.ParseExact(txtManifestDate1.Text.Trim(),"dd/MM/yyyy HH:mm",null);
				if(txtDriver_Flight_AWB1.Text.Trim() !="")
					drN["AWB_Driver_Name"]=txtDriver_Flight_AWB1.Text.Trim();
				if(txtLineHaulCost1.Text.Trim() !="")
					drN["Line_Haul_Cost"]=Convert.ToDecimal(txtLineHaulCost1.Text.Trim());
				if(txtRemarks.Text.Trim() !="")
					drN["Remark"]=txtRemarks.Text.Trim();

				dsDMNew.Tables[0].Rows.Add(drN);

				try
				{
					//Re-Assign the Delivery Manifest
					DeliveryManifestMgrDAL.ReAssignRouteDM(appID, enterpriseID, dsConsignment, dsDMNew, dsDMOld,newRouteCode,7,userID);

					//boon -- Reset dgAssignedConsignment		(3)
					RetrieveConsignment(); //Retreive Consignments

					lblErrorMsg.Text= Utility.GetLanguageText(ResourceType.UserMessage,"INS_SUCCESSFULLY_RE",utility.GetUserCulture());
				}
				catch (Exception exp)
				{
					String strMsg=exp.Message; 
					if(strMsg.IndexOf("PRIMARY KEY") != -1 )
					{
						strMsg = Utility.GetLanguageText(ResourceType.UserMessage,"DUP_KEY_FOUND_CNSG_NO",utility.GetUserCulture());	
					}			
					if (strMsg.IndexOf("unique") != -1)
					{
						strMsg = Utility.GetLanguageText(ResourceType.UserMessage,"DUP_KEY_FOUND_CNSG_NO",utility.GetUserCulture());
					}
					//Foreign Key Violation ---> SQL Server
					if(strMsg.IndexOf("delivery_manifest_datetime") != -1)
					{
						strMsg = Utility.GetLanguageText(ResourceType.UserMessage,"INVALID_DEL_MANIFEST_DATE",utility.GetUserCulture());
					}
					else if(strMsg.IndexOf("flight_vehicle_no") != -1)
					{
						strMsg = Utility.GetLanguageText(ResourceType.UserMessage,"INVALID_FLIGHT_VEHICLE_NO",utility.GetUserCulture());
					}
					else if(strMsg.IndexOf("Delivery_Manifest") != -1)
					{
						strMsg = Utility.GetLanguageText(ResourceType.UserMessage,"INVALID_DEL_MANIFEST_DATE",utility.GetUserCulture());					
					}						
					else if(strMsg.IndexOf("Route_Code") != -1)
					{
						strMsg = Utility.GetLanguageText(ResourceType.UserMessage,"INVALID_ROUTE_CODE",utility.GetUserCulture());					
					}
				
					lblErrorMsg.Text=strMsg;
				}
			}
			else if(deliveryType.Equals("A") && rbtnAir.Checked == true)
			{
				for(int i=0;i<dgAssignedConsignment.Items.Count;i++)
				{
					CheckBox chkSelect = (CheckBox)dgAssignedConsignment.Items[i].FindControl("chkSelect");
					if(chkSelect!=null && chkSelect.Checked==true)
					{
						Label lblConsgmntNo=(Label) dgAssignedConsignment.Items[i].FindControl("lblConsgmntNo");
						Label lblDGOrigin=(Label) dgAssignedConsignment.Items[i].FindControl("lblDGOrigin");
						Label lblDGDestination=(Label) dgAssignedConsignment.Items[i].FindControl("lblDGDestination");
						Label lblDGRoute=(Label) dgAssignedConsignment.Items[i].FindControl("lblDGRoute");
			
						DataRow drNew = dsConsignment.Tables[0].NewRow();
						drNew["Consignment_No"]=lblConsgmntNo.Text.Trim();
						drNew["Origin_State_Code"]=lblDGOrigin.Text.Trim();
						drNew["Destination_State_Code"]=lblDGDestination.Text.Trim();
						drNew["Route_Code"]=lblDGRoute.Text.Trim();																							
						dsConsignment.Tables[0].Rows.Add(drNew);
											
						haveConsignments=true;
					}
				}

				if (haveConsignments==false)
				{
					lblErrorMsg.Text= Utility.GetLanguageText(ResourceType.UserMessage,"NO_CONSIGN_SELECTED",utility.GetUserCulture());
					return;
				}
			
				// OLD DM to Overwrite
				DataSet dsDMOld=GetAssignedDM();
				DataRow drOld = dsDMOld.Tables[0].NewRow();

				if (DbComboPathCode.Value !="")
					drOld["Path_Code"]=DbComboPathCode.Value;
				if (dbFlightVehicleNo.Value!="")
					drOld["Flight_Vehicle_No"]=dbFlightVehicleNo.Value;
				if (dbDepartDate.Value!="")
					drOld["Departure_DateTime"]=System.DateTime.ParseExact(dbDepartDate.Value,"dd/MM/yyyy HH:mm",null);	//Comment by GwanG on 17June08 drOld["Delivery_Manifest_DateTime"]=System.DateTime.ParseExact(dbDepartDate.Value,"dd/MM/yyyy HH:mm",null);			
				if (txtManifestDate.Text.Trim() !="")
					drOld["Delivery_Manifest_DateTime"]=System.DateTime.ParseExact(txtManifestDate.Text.Trim(),"dd/MM/yyyy HH:mm",null);//Comment by GwanG on 17June08 drOld["Departure_DateTime"]=System.DateTime.ParseExact(txtManifestDate.Text.Trim(),"dd/MM/yyyy HH:mm",null);			
				if(txtDriver_Flight_AWB.Text.Trim() !="")
					drOld["AWB_Driver_Name"]=txtDriver_Flight_AWB.Text.Trim();
				if(txtLineHaulCost.Text.Trim() !="")
					drOld["Line_Haul_Cost"]=Convert.ToDecimal(txtLineHaulCost.Text.Trim());
			
				dsDMOld.Tables[0].Rows.Add(drOld);


				// NEW DM to Re-Assign
				DataSet dsDMNew=GetAssignedDM();
				DataRow drN = dsDMNew.Tables[0].NewRow();
			

				if (DbComboPathCodeNew.Value !="")
				{
					drN["Path_Code"]=DbComboPathCodeNew.Value;
					newRouteCode = null;//TIESUtility.GetRouteCodeByPathCode(appID,enterpriseID,DbComboPathCodeNew.Value);
				}
				if (dbFlightVehicleNoNew.Value!="")
					drN["Flight_Vehicle_No"]=dbFlightVehicleNoNew.Value;
				/*Comment by GwanG on 17June08
				 * if (dbDepartDateNew.Value!="")
					drN["Delivery_Manifest_DateTime"]=System.DateTime.ParseExact(dbDepartDateNew.Value,"dd/MM/yyyy HH:mm",null);
				if (txtManifestDate1.Text.Trim() !="")
					drN["Departure_DateTime"]=System.DateTime.ParseExact(txtManifestDate1.Text.Trim(),"dd/MM/yyyy HH:mm",null);*/
				if (dbDepartDateNew.Value!="")
					drN["Departure_DateTime"]=System.DateTime.ParseExact(dbDepartDateNew.Value,"dd/MM/yyyy HH:mm",null);
				if (txtManifestDate1.Text.Trim() !="")
					drN["Delivery_Manifest_DateTime"]=System.DateTime.ParseExact(txtManifestDate1.Text.Trim(),"dd/MM/yyyy HH:mm",null);
				if(txtDriver_Flight_AWB1.Text.Trim() !="")
					drN["AWB_Driver_Name"]=txtDriver_Flight_AWB1.Text.Trim();
				if(txtLineHaulCost1.Text.Trim() !="")
					drN["Line_Haul_Cost"]=Convert.ToDecimal(txtLineHaulCost1.Text.Trim());
				if(txtRemarks.Text.Trim() !="")
					drN["Remark"]=txtRemarks.Text.Trim();

				dsDMNew.Tables[0].Rows.Add(drN);

				try
				{
					//Re-Assign the Delivery Manifest
					DeliveryManifestMgrDAL.ReAssignRouteDM(appID, enterpriseID, dsConsignment, dsDMNew, dsDMOld,newRouteCode,7,userID);

					//boon -- Reset dgAssignedConsignment		(4)
					RetrieveConsignment(); //Retreive Consignments

					lblErrorMsg.Text= Utility.GetLanguageText(ResourceType.UserMessage,"INS_SUCCESSFULLY_RE",utility.GetUserCulture());					
				}
				catch (Exception exp)
				{
					String strMsg=exp.Message; 
					if(strMsg.IndexOf("PRIMARY KEY") != -1 )
					{
						strMsg = Utility.GetLanguageText(ResourceType.UserMessage,"DUP_KEY_FOUND_CNSG_NO",utility.GetUserCulture());	
					}			
					if (strMsg.IndexOf("unique") != -1)
					{
						strMsg = Utility.GetLanguageText(ResourceType.UserMessage,"DUP_KEY_FOUND_CNSG_NO",utility.GetUserCulture());
					}
					//Foreign Key Violation ---> SQL Server
					if(strMsg.IndexOf("delivery_manifest_datetime") != -1)
					{
						strMsg = Utility.GetLanguageText(ResourceType.UserMessage,"INVALID_DEL_MANIFEST_DATE",utility.GetUserCulture());
					}
					else if(strMsg.IndexOf("flight_vehicle_no") != -1)
					{
						strMsg = Utility.GetLanguageText(ResourceType.UserMessage,"INVALID_FLIGHT_VEHICLE_NO",utility.GetUserCulture());
					}
					else if(strMsg.IndexOf("Delivery_Manifest") != -1)
					{
						strMsg = Utility.GetLanguageText(ResourceType.UserMessage,"INVALID_DEL_MANIFEST_DATE",utility.GetUserCulture());					
					}						
					else if(strMsg.IndexOf("Route_Code") != -1)
					{
						strMsg = Utility.GetLanguageText(ResourceType.UserMessage,"INVALID_ROUTE_CODE",utility.GetUserCulture());					
					}
				
					lblErrorMsg.Text=strMsg;
				}
			}
			else if(deliveryType.Equals("L") && rbtnAir.Checked == true)
			{
				for(int i=0;i<dgAssignedConsignment.Items.Count;i++)
				{
					CheckBox chkSelect = (CheckBox)dgAssignedConsignment.Items[i].FindControl("chkSelect");
					if(chkSelect!=null && chkSelect.Checked==true)
					{
						Label lblConsgmntNo=(Label) dgAssignedConsignment.Items[i].FindControl("lblConsgmntNo");
						Label lblDGOrigin=(Label) dgAssignedConsignment.Items[i].FindControl("lblDGOrigin");
						Label lblDGDestination=(Label) dgAssignedConsignment.Items[i].FindControl("lblDGDestination");
						Label lblDGRoute=(Label) dgAssignedConsignment.Items[i].FindControl("lblDGRoute");
			
						DataRow drNew = dsConsignment.Tables[0].NewRow();
						drNew["Consignment_No"]=lblConsgmntNo.Text.Trim();
						drNew["Origin_State_Code"]=lblDGOrigin.Text.Trim();
						drNew["Destination_State_Code"]=lblDGDestination.Text.Trim();
						drNew["Route_Code"]=lblDGRoute.Text.Trim();																							
						dsConsignment.Tables[0].Rows.Add(drNew);
											
						haveConsignments=true;
					}
				}

				if (haveConsignments==false)
				{
					lblErrorMsg.Text= Utility.GetLanguageText(ResourceType.UserMessage,"NO_CONSIGN_SELECTED",utility.GetUserCulture());
					return;
				}
			
				// OLD DM to Overwrite
				DataSet dsDMOld=GetAssignedDM();
				DataRow drOld = dsDMOld.Tables[0].NewRow();

				if (DbComboPathCode.Value !="")
					drOld["Path_Code"]=DbComboPathCode.Value;
				if (dbFlightVehicleNo.Value!="")
					drOld["Flight_Vehicle_No"]=dbFlightVehicleNo.Value;
				if (dbDepartDate.Value!="")
					drOld["Departure_DateTime"]=System.DateTime.ParseExact(dbDepartDate.Value,"dd/MM/yyyy HH:mm",null);			//Comment by GwanG on 17June08 drOld["Delivery_Manifest_DateTime"]=System.DateTime.ParseExact(dbDepartDate.Value,"dd/MM/yyyy HH:mm",null);			
				if (txtManifestDate.Text.Trim() !="")
					drOld["Delivery_Manifest_DateTime"]=System.DateTime.ParseExact(txtManifestDate.Text.Trim(),"dd/MM/yyyy HH:mm",null);			//Comment by GwanG on 17June08 drOld["Departure_DateTime"]=System.DateTime.ParseExact(txtManifestDate.Text.Trim(),"dd/MM/yyyy HH:mm",null);			
				if(txtDriver_Flight_AWB.Text.Trim() !="")
					drOld["AWB_Driver_Name"]=txtDriver_Flight_AWB.Text.Trim();
				if(txtLineHaulCost.Text.Trim() !="")
					drOld["Line_Haul_Cost"]=Convert.ToDecimal(txtLineHaulCost.Text.Trim());
			
				dsDMOld.Tables[0].Rows.Add(drOld);

				// NEW DM to Re-Assign
				DataSet dsDMNew=GetAssignedDM();
				DataRow drN = dsDMNew.Tables[0].NewRow();
			

				if (DbComboPathCodeNew.Value !="")
				{
					drN["Path_Code"]=DbComboPathCodeNew.Value;
					newRouteCode = null;//TIESUtility.GetRouteCodeByPathCode(appID,enterpriseID,DbComboPathCodeNew.Value);
				}
				if (dbFlightVehicleNoNew.Value!="")
					drN["Flight_Vehicle_No"]=dbFlightVehicleNoNew.Value;
				/*Comment by GwanG on 17June08
				 * if (dbDepartDateNew.Value!="")
					drN["Delivery_Manifest_DateTime"]=System.DateTime.ParseExact(dbDepartDateNew.Value,"dd/MM/yyyy HH:mm",null);
				if (txtManifestDate1.Text.Trim() !="")
					drN["Departure_DateTime"]=System.DateTime.ParseExact(txtManifestDate1.Text.Trim(),"dd/MM/yyyy HH:mm",null);*/
				if (dbDepartDateNew.Value!="")
					drN["Departure_DateTime"]=System.DateTime.ParseExact(dbDepartDateNew.Value,"dd/MM/yyyy HH:mm",null);
				if (txtManifestDate1.Text.Trim() !="")
					drN["Delivery_Manifest_DateTime"]=System.DateTime.ParseExact(txtManifestDate1.Text.Trim(),"dd/MM/yyyy HH:mm",null);
				if(txtDriver_Flight_AWB1.Text.Trim() !="")
					drN["AWB_Driver_Name"]=txtDriver_Flight_AWB1.Text.Trim();
				if(txtLineHaulCost1.Text.Trim() !="")
					drN["Line_Haul_Cost"]=Convert.ToDecimal(txtLineHaulCost1.Text.Trim());
				if(txtRemarks.Text.Trim() !="")
					drN["Remark"]=txtRemarks.Text.Trim();

				dsDMNew.Tables[0].Rows.Add(drN);

				try
				{
					//Re-Assign the Delivery Manifest
					DeliveryManifestMgrDAL.ReAssignRouteDM(appID, enterpriseID, dsConsignment, dsDMNew, dsDMOld,newRouteCode,7,userID);

					//boon -- Reset dgAssignedConsignment		(5)
					RetrieveConsignment(); //Retreive Consignments

					lblErrorMsg.Text= Utility.GetLanguageText(ResourceType.UserMessage,"INS_SUCCESSFULLY_RE",utility.GetUserCulture());
				}
				catch (Exception exp)
				{
					String strMsg=exp.Message; 
					if(strMsg.IndexOf("PRIMARY KEY") != -1 )
					{
						strMsg = Utility.GetLanguageText(ResourceType.UserMessage,"DUP_KEY_FOUND_CNSG_NO",utility.GetUserCulture());	
					}			
					if (strMsg.IndexOf("unique") != -1)
					{
						strMsg = Utility.GetLanguageText(ResourceType.UserMessage,"DUP_KEY_FOUND_CNSG_NO",utility.GetUserCulture());
					}
					//Foreign Key Violation ---> SQL Server
					if(strMsg.IndexOf("delivery_manifest_datetime") != -1)
					{
						strMsg = Utility.GetLanguageText(ResourceType.UserMessage,"INVALID_DEL_MANIFEST_DATE",utility.GetUserCulture());
					}
					else if(strMsg.IndexOf("flight_vehicle_no") != -1)
					{
						strMsg = Utility.GetLanguageText(ResourceType.UserMessage,"INVALID_FLIGHT_VEHICLE_NO",utility.GetUserCulture());
					}
					else if(strMsg.IndexOf("Delivery_Manifest") != -1)
					{
						strMsg = Utility.GetLanguageText(ResourceType.UserMessage,"INVALID_DEL_MANIFEST_DATE",utility.GetUserCulture());					
					}						
					else if(strMsg.IndexOf("Route_Code") != -1)
					{
						strMsg = Utility.GetLanguageText(ResourceType.UserMessage,"INVALID_ROUTE_CODE",utility.GetUserCulture());					
					}
				
					lblErrorMsg.Text=strMsg;
				}
			}
			#endregion
			else
			{
				for(int i=0;i<dgAssignedConsignment.Items.Count;i++)
				{
					CheckBox chkSelect = (CheckBox)dgAssignedConsignment.Items[i].FindControl("chkSelect");
					if(chkSelect!=null && chkSelect.Checked==true)
					{
						Label lblConsgmntNo=(Label) dgAssignedConsignment.Items[i].FindControl("lblConsgmntNo");
						Label lblDGOrigin=(Label) dgAssignedConsignment.Items[i].FindControl("lblDGOrigin");
						Label lblDGDestination=(Label) dgAssignedConsignment.Items[i].FindControl("lblDGDestination");
						Label lblDGRoute=(Label) dgAssignedConsignment.Items[i].FindControl("lblDGRoute");
			
						DataRow drNew = dsConsignment.Tables[0].NewRow();
						drNew["Consignment_No"]=lblConsgmntNo.Text.Trim();
						drNew["Origin_State_Code"]=lblDGOrigin.Text.Trim();
						drNew["Destination_State_Code"]=lblDGDestination.Text.Trim();
						drNew["Route_Code"]=lblDGRoute.Text.Trim();																							
						dsConsignment.Tables[0].Rows.Add(drNew);
					
						haveConsignments=true;
					}
				}

				if (haveConsignments==false)
				{
					lblErrorMsg.Text= Utility.GetLanguageText(ResourceType.UserMessage,"NO_CONSIGN_SELECTED",utility.GetUserCulture());
					return;
				}
			
				// OLD DM to Overwrite
				DataSet dsDMOld=GetAssignedDM();
				DataRow drOld = dsDMOld.Tables[0].NewRow();

				if (DbComboPathCode.Value !="")
					drOld["Path_Code"]=DbComboPathCode.Value;
				if (dbFlightVehicleNo.Value!="")
					drOld["Flight_Vehicle_No"]=dbFlightVehicleNo.Value;
				/*Comment by GwanG on 17June08
				 * if (dbDepartDate.Value!="")
					drOld["Delivery_Manifest_DateTime"]=System.DateTime.ParseExact(dbDepartDate.Value,"dd/MM/yyyy HH:mm",null);			
				if (txtManifestDate.Text.Trim() !="")
					drOld["Departure_DateTime"]=System.DateTime.ParseExact(txtManifestDate.Text.Trim(),"dd/MM/yyyy HH:mm",null);			*/
				if (dbDepartDate.Value!="")
					drOld["Departure_DateTime"]=System.DateTime.ParseExact(dbDepartDate.Value,"dd/MM/yyyy HH:mm",null);			
				if (txtManifestDate.Text.Trim() !="")
					drOld["Delivery_Manifest_DateTime"]=System.DateTime.ParseExact(txtManifestDate.Text.Trim(),"dd/MM/yyyy HH:mm",null);
				if(txtDriver_Flight_AWB.Text.Trim() !="")
					drOld["AWB_Driver_Name"]=txtDriver_Flight_AWB.Text.Trim();
				if(txtLineHaulCost.Text.Trim() !="")
					drOld["Line_Haul_Cost"]=Convert.ToDecimal(txtLineHaulCost.Text.Trim());
			
				dsDMOld.Tables[0].Rows.Add(drOld);

				// NEW DM to Re-Assign
				DataSet dsDMNew=GetAssignedDM();
				DataRow drN = dsDMNew.Tables[0].NewRow();
			

				if (DbComboPathCodeNew.Value !="")
					drN["Path_Code"]=DbComboPathCodeNew.Value;
				if (dbFlightVehicleNoNew.Value!="")
					drN["Flight_Vehicle_No"]=dbFlightVehicleNoNew.Value;
				/* Comment by GwanG on 17June08
				if (dbDepartDateNew.Value!="")
					drN["Delivery_Manifest_DateTime"]=System.DateTime.ParseExact(dbDepartDateNew.Value,"dd/MM/yyyy HH:mm",null);
				if (txtManifestDate1.Text.Trim() !="")
					drN["Departure_DateTime"]=System.DateTime.ParseExact(txtManifestDate1.Text.Trim(),"dd/MM/yyyy HH:mm",null);*/
				if (dbDepartDateNew.Value!="")
					drN["Departure_DateTime"]=System.DateTime.ParseExact(dbDepartDateNew.Value,"dd/MM/yyyy HH:mm",null);
				if (txtManifestDate1.Text.Trim() !="")
					drN["Delivery_Manifest_DateTime"]=System.DateTime.ParseExact(txtManifestDate1.Text.Trim(),"dd/MM/yyyy HH:mm",null);
				if(txtDriver_Flight_AWB1.Text.Trim() !="")
					drN["AWB_Driver_Name"]=txtDriver_Flight_AWB1.Text.Trim();
				if(txtLineHaulCost1.Text.Trim() !="")
					drN["Line_Haul_Cost"]=Convert.ToDecimal(txtLineHaulCost1.Text.Trim());
				if(txtRemarks.Text.Trim() !="")
					drN["Remark"]=txtRemarks.Text.Trim();

				dsDMNew.Tables[0].Rows.Add(drN);

				try
				{
					//Re-Assign the Delivery Manifest
					DeliveryManifestMgrDAL.ReAssignRouteDM(appID, enterpriseID, dsConsignment, dsDMNew, dsDMOld);

					//boon -- Reset dgAssignedConsignment		(6)
					RetrieveConsignment(); //Retreive Consignments

					lblErrorMsg.Text= Utility.GetLanguageText(ResourceType.UserMessage,"INS_SUCCESSFULLY_RE",utility.GetUserCulture());
				}
				catch (Exception exp)
				{
					String strMsg=exp.Message; 
					if(strMsg.IndexOf("PRIMARY KEY") != -1 )
					{
						strMsg = Utility.GetLanguageText(ResourceType.UserMessage,"DUP_KEY_FOUND_CNSG_NO",utility.GetUserCulture());	
					}			
					if (strMsg.IndexOf("unique") != -1)
					{
						strMsg = Utility.GetLanguageText(ResourceType.UserMessage,"DUP_KEY_FOUND_CNSG_NO",utility.GetUserCulture());
					}
					//Foreign Key Violation ---> SQL Server
					if(strMsg.IndexOf("delivery_manifest_datetime") != -1)
					{
						strMsg = Utility.GetLanguageText(ResourceType.UserMessage,"INVALID_DEL_MANIFEST_DATE",utility.GetUserCulture());
					}
					else if(strMsg.IndexOf("flight_vehicle_no") != -1)
					{
						strMsg = Utility.GetLanguageText(ResourceType.UserMessage,"INVALID_FLIGHT_VEHICLE_NO",utility.GetUserCulture());
					}
					else if(strMsg.IndexOf("Delivery_Manifest") != -1)
					{
						strMsg = Utility.GetLanguageText(ResourceType.UserMessage,"INVALID_DEL_MANIFEST_DATE",utility.GetUserCulture());					
					}						
					else if(strMsg.IndexOf("Route_Code") != -1)
					{
						strMsg = Utility.GetLanguageText(ResourceType.UserMessage,"INVALID_ROUTE_CODE",utility.GetUserCulture());					
					}
				
					lblErrorMsg.Text=strMsg;
				}

			}
		}



		private DataSet GetAssignedCons()
		{
			DataTable dtDMCons = new DataTable();
			dtDMCons.Columns.Add(new DataColumn("Consignment_no", typeof(string)));
			dtDMCons.Columns.Add(new DataColumn("Origin_State_Code", typeof(string)));
			dtDMCons.Columns.Add(new DataColumn("Destination_State_Code", typeof(string)));
			dtDMCons.Columns.Add(new DataColumn("Route_Code", typeof(string)));

			DataSet dsDMCons = new DataSet();
			dsDMCons.Tables.Add(dtDMCons);
			
			return  dsDMCons;	
			
		}

		private DataSet GetAssignedDM()
		{
			DataTable dtDM = new DataTable(); 
			dtDM.Columns.Add(new DataColumn("Path_Code", typeof(string)));
			dtDM.Columns.Add(new DataColumn("Flight_Vehicle_No", typeof(string)));
			dtDM.Columns.Add(new DataColumn("Delivery_Manifest_DateTime", typeof(DateTime)));
			dtDM.Columns.Add(new DataColumn("AWB_Driver_Name", typeof(string)));
			dtDM.Columns.Add(new DataColumn("Line_Haul_Cost", typeof(Decimal)));
			dtDM.Columns.Add(new DataColumn("Departure_DateTime", typeof(DateTime)));
			dtDM.Columns.Add(new DataColumn("Remark", typeof(string)));

			DataSet dsDM = new DataSet();
			dsDM.Tables.Add(dtDM);
			
			return  dsDM;				
		}

		
		private DataSet ReadDMRecord(String strDelPathCode, String strFlightVehicleNo, String strDepartDate)
		{
			lblErrorMsg.Text="";
			DataSet dsQuery =GetAssignedDM();			
			DataRow drDMNew = dsQuery.Tables[0].NewRow();

			drDMNew["Path_Code"]=strDelPathCode;
			if (strFlightVehicleNo !="")
				drDMNew["Flight_Vehicle_No"]=strFlightVehicleNo;
			if (strDepartDate !="")
			{
				drDMNew["Departure_datetime"]=System.DateTime.ParseExact(strDepartDate,"dd/MM/yyyy HH:mm",null);				
			}
			dsQuery.Tables[0].Rows.Add(drDMNew);
			//Comment by Gwang on 16June08 
			//DataSet dsDMRec=DeliveryManifestMgrDAL.DelManifestRecord(appID,enterpriseID,dsQuery); 
			DataSet dsDMRec = DeliveryManifestMgrDAL.DepartureRecord(appID,enterpriseID,dsQuery);
			//End Comment & Added
			return dsDMRec;
		}

		private void DMRecord(object sender, System.EventArgs e)
		{
			String strDelPathCode=DbComboPathCode.Value;;
			String strFlightVehicleNo=dbFlightVehicleNo.Value;;
			String strDepartDate=dbDepartDate.Value;		
			String strDriverFlightAWB="";
			String strLineHaulCost="";
			String strManifestDate="";
	
			ClearConsignment();

			DataSet dsDMRecord=ReadDMRecord(strDelPathCode,strFlightVehicleNo,strDepartDate);
				
			if (dsDMRecord.Tables[0].Rows.Count<=0)
			{
				lblErrorMsg.Text= Utility.GetLanguageText(ResourceType.UserMessage,"NO_RECORD_FOUND_PATH",utility.GetUserCulture());
				return;
			}			
			DataRow drEach=dsDMRecord.Tables[0].Rows[0];
			if ((drEach["awb_driver_name"] != null) && (drEach["awb_driver_name"].ToString() != ""))
			{
				strDriverFlightAWB = (String) drEach["awb_driver_name"];
			}
			txtDriver_Flight_AWB.Text=strDriverFlightAWB;
			
			if ((drEach["line_haul_cost"] != null) && (drEach["line_haul_cost"].ToString() != ""))
			{
				decimal dLineHaulCost = Convert.ToDecimal(drEach["line_haul_cost"]);
				strLineHaulCost = dLineHaulCost.ToString("#.00");				
			}
			txtLineHaulCost.Text=strLineHaulCost;
			
			if ((drEach["Delivery_Manifest_DateTime"] != null) && (drEach["Delivery_Manifest_DateTime"].ToString() != ""))
			{
				DateTime dtManiDate = (DateTime) drEach["Delivery_Manifest_DateTime"];//DateTime dtDepartDate = (DateTime) drEach["Delivery_Manifest_DateTime"];
				strManifestDate = dtManiDate.ToString("dd/MM/yyyy HH:mm");//strManifestDate = dtDepartDate.ToString("dd/MM/yyyy HH:mm");
			}
			txtManifestDate.Text=strManifestDate;

			RetrieveConsignment(); //Retreive Consignments
		}

		private void DMRecordNew(object sender, System.EventArgs e)
		{
			String strDelPathCode=DbComboPathCodeNew.Value;;
			String strFlightVehicleNo=dbFlightVehicleNoNew.Value;;
			String strDepartDate=dbDepartDateNew.Value;		
			String strDriverFlightAWB="";
			String strLineHaulCost="";
			String strManifestDate="";
			String strRemarks="";

			DataSet dsDMRecord=ReadDMRecord(strDelPathCode,strFlightVehicleNo,strDepartDate);
				
			if (dsDMRecord.Tables[0].Rows.Count<=0)
			{
				lblErrorMsg.Text= Utility.GetLanguageText(ResourceType.UserMessage,"NO_RECORD_FOUND_PATH",utility.GetUserCulture());
				return;
			}			
			DataRow drEach=dsDMRecord.Tables[0].Rows[0];
			if ((drEach["awb_driver_name"] != null) && (drEach["awb_driver_name"].ToString() != ""))
			{
				strDriverFlightAWB = (String) drEach["awb_driver_name"];
			}
			txtDriver_Flight_AWB1.Text=strDriverFlightAWB;
			
			if ((drEach["line_haul_cost"] != null) && (drEach["line_haul_cost"].ToString() != ""))
			{
				decimal dLineHaulCost1 = Convert.ToDecimal(drEach["line_haul_cost"]);
				strLineHaulCost=dLineHaulCost1.ToString("0.00");
			}
			txtLineHaulCost1.Text=strLineHaulCost;
			
			if ((drEach["Delivery_Manifest_DateTime"] != null) && (drEach["Delivery_Manifest_DateTime"].ToString() != ""))
			{
				DateTime dtManiDate = (DateTime) drEach["Delivery_Manifest_DateTime"];
				strManifestDate = dtManiDate.ToString("dd/MM/yyyy HH:mm");
			}
			txtManifestDate1.Text=strManifestDate;
			if ((drEach["remark"] != null) && (drEach["remark"].ToString() != ""))
			{
				strRemarks = (String) drEach["remark"];
			}
			txtRemarks.Text=strRemarks;

		}

		private void ClearManifestDate(object sender, System.EventArgs e)
		{
			lblErrorMsg.Text="";
			dbDepartDate.Text="";
			txtLineHaulCost.Text="";
			txtDriver_Flight_AWB.Text="";
			txtManifestDate.Text="";
			ClearConsignment();
		}
		private void ClearManifestDateNew(object sender, System.EventArgs e)
		{
			lblErrorMsg.Text="";
			dbDepartDateNew.Text="";
			txtLineHaulCost1.Text="";
			txtDriver_Flight_AWB1.Text="";
			txtManifestDate1.Text="";
		}
	
		private void ClearConsignment()
		{
			m_sdsAssignedConsignment.Tables[0].Rows.Clear();
			BindAssignedConsignment();
		}

		
	}
}
