<%@ Page language="c#" Codebehind="AgentSndRecPopup.aspx.cs" AutoEventWireup="false" Inherits="TIES.WebUI.AgentSndRec" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>AgentSndRec</title>
		<LINK href="css/Styles.css" type="text/css" rel="stylesheet" name="stylesheet">
		<meta name="GENERATOR" Content="Microsoft Visual Studio 7.0">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="AgentSndRec" method="post" runat="server">
			<asp:datagrid id="dgSndRecpMaster" style="Z-INDEX: 102; LEFT: 45px; POSITION: absolute; TOP: 96px" runat="server" OnPageIndexChanged="Paging" OnSelectedIndexChanged="dgSndRecpMaster_SelectedIndexChanged" AllowPaging="True" Width="522px" AutoGenerateColumns="False" BorderColor="#CCCCCC" BackColor="White" CellPadding="3" BorderWidth="1px" BorderStyle="None" CssClass="gridHeading" AllowCustomPaging="True">
				<SelectedItemStyle Font-Bold="True" ForeColor="White" CssClass="drilldownFieldSelected"></SelectedItemStyle>
				<ItemStyle CssClass="drilldownField"></ItemStyle>
				<HeaderStyle Width="10%" CssClass="drilldownHeading"></HeaderStyle>
				<FooterStyle ForeColor="#000066" BackColor="White"></FooterStyle>
				<Columns>
					<asp:BoundColumn DataField="snd_rec_name" HeaderText="Name" ItemStyle-Wrap="True"></asp:BoundColumn>
					<asp:BoundColumn DataField="address1" HeaderText="Address1" ItemStyle-Wrap="True"></asp:BoundColumn>
					<asp:BoundColumn DataField="address2" HeaderText="Address2" ItemStyle-Wrap="True"></asp:BoundColumn>
					<asp:BoundColumn DataField="country" HeaderText="Country"></asp:BoundColumn>
					<asp:BoundColumn DataField="zipcode" HeaderText="Postal Code"></asp:BoundColumn>
					<asp:BoundColumn DataField="telephone" HeaderText="telephone" Visible="False"></asp:BoundColumn>
					<asp:BoundColumn DataField="fax" HeaderText="fax" Visible="False"></asp:BoundColumn>
					<asp:BoundColumn DataField="contact_person" HeaderText="contact_person" Visible="False"></asp:BoundColumn>
					<asp:ButtonColumn Text="Select" CommandName="Select"></asp:ButtonColumn>
				</Columns>
				<PagerStyle NextPageText="Next" PrevPageText="Previous" HorizontalAlign="Right" ForeColor="#000066" BackColor="White" CssClass="normalText"></PagerStyle>
			</asp:datagrid>
			<asp:textbox id="txtName" style="Z-INDEX: 107; LEFT: 153px; POSITION: absolute; TOP: 61px" runat="server" CssClass="textField" Width="116px" Height="19px"></asp:textbox>
			<asp:label id="Label1" style="Z-INDEX: 106; LEFT: 56px; POSITION: absolute; TOP: 61px" runat="server" CssClass="tableLabel" Width="82px" Height="16px" Font-Bold="True"> Name</asp:label>
			<asp:button id="btnSearch" style="Z-INDEX: 103; LEFT: 293px; POSITION: absolute; TOP: 60px" runat="server" Width="78" CssClass="buttonProp" Height="21" Text="Search" CausesValidation="False" Font-Bold="True"></asp:button>
			<asp:button id="btnOk" style="Z-INDEX: 101; LEFT: 382px; POSITION: absolute; TOP: 61px" runat="server" Width="78px" CssClass="buttonProp" Height="21px" Text="Close" CausesValidation="False" Font-Bold="True"></asp:button>
		</form>
	</body>
</HTML>
