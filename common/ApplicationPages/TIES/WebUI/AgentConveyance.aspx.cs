using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.common.util;
using com.ties;
using com.ties.classes;
using com.ties.DAL;
using com.ties.BAL;
using com.common.classes;
using Cambro.Web.DbCombo;
using System.Configuration;
using com.common.applicationpages;


namespace com.ties
{
	/// <summary>
	/// Summary description for AgentConveyance.
	/// </summary>
	public class AgentConveyance : BasePage
	{
		string m_strAppID=null;
		string m_strEnterpriseID=null;
		SessionDS m_sdsConveyance = null;
		SessionDS m_sdsAgent = null;
		static private int m_iSetSize = 4;
		private string sDbComboRegKey="";
		//DataSet ds=null;

		protected System.Web.UI.WebControls.Label lblTitle;
		protected System.Web.UI.WebControls.Button btnGoToFirstPage;
		protected System.Web.UI.WebControls.Button btnPreviousPage;
		protected System.Web.UI.WebControls.TextBox txtCountRec;
		protected System.Web.UI.WebControls.Button btnNextPage;
		protected System.Web.UI.WebControls.Button btnGoToLastPage;
		protected System.Web.UI.WebControls.Label lblAgentName;
		protected System.Web.UI.WebControls.Label lblAgentCode;
		protected System.Web.UI.WebControls.Button btnQuery;
		protected System.Web.UI.WebControls.Button btnExecuteQuery;
		protected System.Web.UI.WebControls.DataGrid dgConveyance;
		protected System.Web.UI.WebControls.Button btnInsert;
		protected System.Web.UI.WebControls.Label lblErrorMessage;
		protected Cambro.Web.DbCombo.DbCombo dbCmbAgentName;
		protected Cambro.Web.DbCombo.DbCombo dbCmbAgentId;
		protected System.Web.UI.WebControls.TextBox txtConveyance;
		protected System.Web.UI.WebControls.RequiredFieldValidator reqAgentId;
		protected System.Web.UI.WebControls.ValidationSummary PageValidationSummary;
		protected System.Web.UI.HtmlControls.HtmlTable tblMain;
	
		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here			
			sDbComboRegKey=System.Configuration.ConfigurationSettings.AppSettings["DbComboRegKey"]; //"aeaaaaU99999baaaaaaaaaEbbaaaauxm6rdDVU1m6ndDVULm6fdDFn1yJ3gBFnx-FZGmfn0~RVxqXnwzSr1xd7w~RvL~NnwyYJ0~Sn1xqrxzDVeBIVLmUadm6fdDVU1m2yto6fdDVULm6fh~vZGmZq0yReHqyZcmdZsnfJwlUqdCIzZrVZWmcZsnczXc";//System.Configuration.ConfigurationSettings.AppSettings["DbComboRegKey"];
			lblErrorMessage.Text="";
			DbComboRegKey();
//			BindConveyanceGrid();
			m_strAppID = utility.GetAppID();
			m_strEnterpriseID = utility.GetEnterpriseID();
			if(!IsPostBack)
			{
				QueryMode();	
				ViewState["AgentOperation"] = Operation.None;
				ViewState["AgentMode"]= ScreenMode.None;
				ViewState["MovePrevious"]=false;
				ViewState["MoveNext"]=false;
				ViewState["MoveFirst"]=false;
				ViewState["MoveLast"]=false;  
			}
			else
			{
				if (Session["SESSION_DS1"] !=null)
				{
					m_sdsConveyance = (SessionDS) Session["SESSION_DS1"];
				}
				if (Session["SESSION_DS3"] !=null)
				{
					m_sdsAgent	= (SessionDS)Session["SESSION_DS3"];
				}
			}
			PageValidationSummary.HeaderText = Utility.GetLanguageText(ResourceType.UserMessage, "PLS_ENTER_MISS_FIELD", utility.GetUserCulture());
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			base.OnInit(e);
			InitializeComponent();
			
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnGoToFirstPage.Click += new System.EventHandler(this.btnGoToFirstPage_Click);
			this.btnPreviousPage.Click += new System.EventHandler(this.btnPreviousPage_Click);
			this.btnNextPage.Click += new System.EventHandler(this.btnNextPage_Click);
			this.btnGoToLastPage.Click += new System.EventHandler(this.btnGoToLastPage_Click);
			this.btnQuery.Click += new System.EventHandler(this.btnQuery_Click);
			this.btnExecuteQuery.Click += new System.EventHandler(this.btnExecuteQuery_Click);
			this.btnInsert.Click += new System.EventHandler(this.btnInsert_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private void DbComboRegKey()
		{
			dbCmbAgentId.RegistrationKey=System.Configuration.ConfigurationSettings.AppSettings["DbComboRegKey"];//sDbComboRegKey;//System.Configuration.ConfigurationSettings.AppSettings["DbComboRegKey"];
			dbCmbAgentName.RegistrationKey=System.Configuration.ConfigurationSettings.AppSettings["DbComboRegKey"];//sDbComboRegKey;//System.Configuration.ConfigurationSettings.AppSettings["DbComboRegKey"];
			
		}
		
		private void SetAgentNameServerStates()
		{						
			String strAgentID=dbCmbAgentId.Value;			
			Hashtable hash = new Hashtable();		
			if (strAgentID !="")
			{
				hash.Add("strAgentID", strAgentID);
			}			
			dbCmbAgentName.ServerState = hash;
			dbCmbAgentName.ServerStateSecretString = "kdfsjgnskajh[apq2-4uhg465654435879mnbfdm cxs;q4";
		}

		private void SetAgentIDServerStates()
		{			
			String strAgentName=dbCmbAgentName.Value;			
			Hashtable hash = new Hashtable();			
			if (strAgentName !="")
			{
				hash.Add("strAgentName", strAgentName);
			}						

			dbCmbAgentId.ServerState = hash;
			dbCmbAgentId.ServerStateSecretString = "kdfsjgnskajh[apq2-4uhg465654435879mnbfdm cxs;q4";			
		}

		[Cambro.Web.DbCombo.ResultsMethodAttribute(true)]		
		public static object AgentIdServerMethod(Cambro.Web.DbCombo.ServerMethodArgs args)
		{
			Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings,HttpContext.Current.Session);
			String strAppID = util.GetAppID();
			String strEnterpriseID = util.GetEnterpriseID();
						
			String strAgentName="";
			if(args.ServerState != null && args.ServerState.Authenticate("kdfsjgnskajh[apq2-4uhg465654435879mnbfdm cxs;q4"))
			{			
				if(args.ServerState["strAgentName"] != null && args.ServerState["strAgentName"].ToString().Length > 0)
				{
					strAgentName = args.ServerState["strAgentName"].ToString();					
				}	
			}
			
			DataSet dataset = DbComboDAL.PayerIDQuery(strAppID,strEnterpriseID,args, "A", strAgentName);	
			return dataset;
		}

		[Cambro.Web.DbCombo.ResultsMethod(true)]
		public static object AgentNameServerMethod(Cambro.Web.DbCombo.ServerMethodArgs args)
		{
			Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings,HttpContext.Current.Session);
			String strAppID = util.GetAppID();
			String strEnterpriseID = util.GetEnterpriseID();
	
			String strAgentID= "";

			if(args.ServerState != null && args.ServerState.Authenticate("kdfsjgnskajh[apq2-4uhg465654435879mnbfdm cxs;q4"))
			{				
				if (args.ServerState["strAgentID"] != null && args.ServerState["strAgentID"].ToString().Length > 0 )
				{
					strAgentID = (args.ServerState["strAgentID"].ToString());
				}
			}	

			DataSet dataset = DbComboDAL.PayerNameQuery(strAppID,strEnterpriseID,args, "A", strAgentID);	
			return dataset;  
		}

		
		public void QueryMode()
		{
			ViewState["AgentOperation"] = Operation.None;
			ViewState["AgentMode"]		= ScreenMode.Query;
			ViewState["ConvOperation"]=Operation.None;
			ViewState["ConvMode"]=ScreenMode.Query;
			 	
			m_sdsAgent = AgentProfileMgrDAL.GetEmptyAgentDS(1);
			EnableNavigationButtons(false,false,false,false);

			btnQuery.Enabled=true;
			btnExecuteQuery.Enabled=true;
			Utility.EnableButton(ref btnInsert,ButtonType.Insert,true,m_moduleAccessRights);
			
			dbCmbAgentId.Value="";dbCmbAgentName.Value="";
			dbCmbAgentId.Text="";dbCmbAgentName.Text="";

			m_sdsConveyance = SysDataManager1.GetEmptyConveyance() ;  

			m_sdsConveyance.DataSetRecSize = 1;
			AddRow(); 
			Session["SESSION_DS1"] = m_sdsConveyance;
			BindConveyanceGrid();
			lblErrorMessage.Text="";
			
			EditHRow(true);
			dgConveyance.Columns[0].Visible=false;
			dgConveyance.Columns[1].Visible=false;
			dbCmbAgentId.Enabled=true;
			dbCmbAgentName.Enabled=true;
		}

		private void AddRow()
		{
			SysDataManager1.AddNewRowInConveyanceDS(ref m_sdsConveyance);
			BindConveyanceGrid();
		}

		private void EditHRow(bool bItemIndex)
		{
			foreach (DataGridItem item in dgConveyance.Items)
			{ 
				if (bItemIndex) 
					dgConveyance.EditItemIndex = item.ItemIndex; 
				else 
					dgConveyance.EditItemIndex = -1; 
			}
			dgConveyance.DataBind();
		}

		private void GetChangedRow(DataGridItem item, int rowIndex)
		{
			msTextBox txtConveyance = (msTextBox)item.FindControl("txtConveyance");
			TextBox txtCCDescp = (TextBox)item.FindControl("txtCCDescp");
			msTextBox txtLength = (msTextBox)item.FindControl("txtLength");
			msTextBox txtBreadth = (msTextBox)item.FindControl("txtBreadth");
			msTextBox txtHeight = (msTextBox)item.FindControl("txtHeight");
			msTextBox txtWeight = (msTextBox)item.FindControl("txtWeight");
				
			string strConveyance = txtConveyance.Text.ToString();
			string strCCDescp = txtCCDescp.Text.ToString();
			string strLength = txtLength.Text.ToString();
			string strBreadth = txtBreadth.Text.ToString();
			string strHeight = txtHeight.Text.ToString();
			string strWeight = txtWeight.Text.ToString();
						
			DataRow dr = m_sdsConveyance.ds.Tables[0].Rows[rowIndex];
			dr[0] = strConveyance;
			dr[1] = strCCDescp;
			dr[2] = strLength;
			dr[3] = strBreadth;
			dr[4] = strHeight;			
			dr[5] = strWeight;

			Session["SESSION_DS1"] = m_sdsConveyance;
			
		}

		
		#region Conveyance Grid functions

		public void BindConveyanceGrid()
		{
			dgConveyance.VirtualItemCount = System.Convert.ToInt32(m_sdsConveyance.QueryResultMaxSize);
			dgConveyance.DataSource = m_sdsConveyance.ds;
			dgConveyance.DataBind(); 
			Session["SESSION_DS1"] = m_sdsConveyance;
		}

		public void dgConvey_Button(object sender, DataGridCommandEventArgs e)
		{
			int iOperation = (int)ViewState["ConvOperation"];
			int iMode =(int)ViewState["ConvMode"];
			String strCmdNm = e.CommandName;			
			String strtxtConveyanceCode=null; String strConveyanceCode=null;
			String strtxtCCDescp=null; 
			String strtxtLength=null; 
			String strtxtBreadth=null;
			String strtxtHeight=null; 
			String strtxtWeight=null; 
			String sUrl=null;
			String sScript=null;
			ArrayList paramList=null;
		
			if (strCmdNm.Equals("Update") || strCmdNm.Equals("Edit") || strCmdNm.Equals("Delete") || strCmdNm.Equals("Cancel"))
				return;
			if (iMode==(int)ScreenMode.ExecuteQuery)
				return;
			if (strCmdNm.Equals("ConveyanceSearch"))
			{
				msTextBox txtConveyance = (msTextBox)e.Item.FindControl("txtConveyance");
				if(txtConveyance != null)
				{
					strtxtConveyanceCode = txtConveyance.ClientID;
					strConveyanceCode = txtConveyance.Text;
				}
				TextBox txtCCDescp = (TextBox)e.Item.FindControl("txtCCDescp");
				if(txtCCDescp != null)
					strtxtCCDescp = txtCCDescp.ClientID;

				msTextBox txtLength = (msTextBox)e.Item.FindControl("txtLength");
				if(txtLength != null)
					strtxtLength = txtLength.ClientID;

				msTextBox txtBreadth = (msTextBox)e.Item.FindControl("txtBreadth");
				if(txtBreadth != null)
					strtxtBreadth = txtBreadth.ClientID;

				msTextBox txtHeight = (msTextBox)e.Item.FindControl("txtHeight");
				if(txtHeight != null)
					strtxtHeight = txtHeight.ClientID;

				msTextBox txtWeight = (msTextBox)e.Item.FindControl("txtWeight");
				if(txtWeight != null)
					strtxtWeight = txtWeight.ClientID;
                			
				sUrl = "ConveyancePopup.aspx?FORMID=AgentConveyance&CONVEYANCECODE_TEXT="+strtxtConveyanceCode+"&CONVEYANCECODE="+strConveyanceCode;
				sUrl+="&CCDESCP_TEXT="+strtxtCCDescp;
				sUrl+="&LENGTH_TEXT="+strtxtLength;
				sUrl+="&BREADTH_TEXT="+strtxtBreadth;
				sUrl+="&HEIGHT_TEXT="+strtxtHeight;
				sUrl+="&WEIGHT_TEXT="+strtxtWeight;

				paramList = new ArrayList();
				paramList.Add(sUrl);
				sScript = Utility.GetScript("openWindowParam.js",paramList);
				Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
			}			
		}

		protected void  dgConveyance_Edit(object sender, DataGridCommandEventArgs e)
		{		
			lblErrorMessage.Text = "";
			if((int)ViewState["ConvMode"] == (int)ScreenMode.Insert && (int) ViewState["ConvOperation"] == (int)Operation.Insert &&  dgConveyance.EditItemIndex > 0)
			{
				m_sdsConveyance.ds.Tables[0].Rows.RemoveAt(dgConveyance.EditItemIndex);		
				dgConveyance.CurrentPageIndex = 0;
			}
			dgConveyance.EditItemIndex = e.Item.ItemIndex;
			ViewState["ConvOperation"] = Operation.Update;
			BindConveyanceGrid();
		}
		protected void dgConveyance_ItemDataBound(object sender, DataGridItemEventArgs e)
		{
			if(e.Item.ItemIndex == -1)
				return;

			DataRow drSelected = m_sdsConveyance.ds.Tables[0].Rows[e.Item.ItemIndex];
			if(drSelected.RowState == DataRowState.Deleted)
				return;
			
			if ((int)ViewState["ConvOperation"] != (int)Operation.Insert && (!btnExecuteQuery.Enabled))
			{
				msTextBox txtConveyance = (msTextBox)e.Item.FindControl("txtConveyance");
				if(txtConveyance !=null)
				{
					txtConveyance.Text=drSelected[0].ToString();
					txtConveyance.Enabled=false;
				}
			}

			if ((int)ViewState["ConvMode"] == (int)ScreenMode.Insert && (int)ViewState["ConvOperation"] == (int)Operation.Insert)
				e.Item.Cells[1].Enabled=false;

			if((int)ViewState["ConvMode"]==(int)ScreenMode.Insert)
				e.Item.Cells[1].Enabled = false;	
			else
			{
				e.Item.Cells[1].Enabled = true;
			}
			SetAgentIDServerStates();
			SetAgentNameServerStates();

		}

		protected void dgConveyance_Cancel(object sender, DataGridCommandEventArgs e)
		{
			lblErrorMessage.Text = "";
			int rowIndex = e.Item.ItemIndex;
			if ((int)ViewState["ConvMode"] == (int)ScreenMode.Insert && (int)ViewState["ConvOperation"] == (int)Operation.Insert)
			{
				Utility.EnableButton(ref btnInsert,ButtonType.Insert,true,m_moduleAccessRights);
				m_sdsConveyance.ds.Tables[0].Rows.RemoveAt(rowIndex);				
			}
			ViewState["ConvOperation"] = Operation.None;
			dgConveyance.EditItemIndex = -1;
			BindConveyanceGrid();			
		}

		protected void dgConveyance_PageChange(object sender, DataGridPageChangedEventArgs e)
		{
			lblErrorMessage.Text = "";
			dgConveyance.CurrentPageIndex = e.NewPageIndex;
			dgConveyance.SelectedIndex = -1;
			dgConveyance.EditItemIndex = -1;
			RetreiveSelectedPage();
		}
		
		protected void dgConveyance_Delete(object sender, DataGridCommandEventArgs e)
		{
			if ((int)ViewState["ConvOperation"] == (int)Operation.Update)
			{
				dgConveyance.EditItemIndex = -1;
			}
			int rowIndex = e.Item.ItemIndex;
			m_sdsConveyance  = (SessionDS)Session["SESSION_DS1"];
			DataRow dr =m_sdsConveyance.ds.Tables[0].Rows[rowIndex];
			String strConveyance =(string) dr[0];
			String strAgentId=(String)ViewState["AgentID"];
			
			if (strConveyance==null || strConveyance =="")
				return;
			
			try
			{
				// delete from table
				int iRowsDeleted = SysDataManager1.DeleteConveyance(m_strAppID, m_strEnterpriseID, strAgentId, strConveyance);
				ArrayList paramValues = new ArrayList();
				paramValues.Add(iRowsDeleted.ToString());
				lblErrorMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"DLD_SUCCESSFULLY",utility.GetUserCulture(),paramValues);
			}
			catch(ApplicationException appException)
			{
				String strMsg = appException.Message;
				if(strMsg.IndexOf("FK") != -1)
					lblErrorMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"CHILD_FOUND_CANNOT_DEL",utility.GetUserCulture());	
				if(strMsg.ToLower().IndexOf("child record found") != -1 )
				{
					lblErrorMessage.Text =  Utility.GetLanguageText(ResourceType.UserMessage,"CHILD_FOUND_CANNOT_DEL",utility.GetUserCulture());				
				}
				Logger.LogTraceError("Conveyance.aspx.cs","dgConveyance_Delete","RBAC003",appException.Message.ToString());
				return;
			}

			if((int)ViewState["ConvMode"] == (int)ScreenMode.Insert)
				m_sdsConveyance.ds.Tables[0].Rows.RemoveAt(rowIndex);
			else
			{
				RetreiveSelectedPage();
			}
			ViewState["ConvOperation"] = Operation.None;
			//Make the row in non-edit Mode
			EditHRow(false);
			BindConveyanceGrid();
			Logger.LogDebugInfo("Conveyance","dgConveyance_Delete","INF004","updating data grid...");			
			
		}
	
		protected void dgConveyance_Update(object sender, DataGridCommandEventArgs e)
		{
			lblErrorMessage.Text = "";
			if (btnExecuteQuery.Enabled)
			{
				return;
			}
			int iRowsAffected = 0;
			int rowIndex = e.Item.ItemIndex;
			GetChangedRow(e.Item, e.Item.ItemIndex);
			String strAgentId=dbCmbAgentId.Value.Trim();
			if (strAgentId!="")
			{
				lblErrorMessage.Text="Agent Id cannot be null";
			}
		
			int iOperation = (int)ViewState["ConvOperation"];
			try
			{
				if (iOperation == (int)Operation.Insert)
				{
					iRowsAffected = SysDataManager1.InsertConveyance(m_sdsConveyance.ds, rowIndex, m_strAppID, m_strEnterpriseID,strAgentId);
					ArrayList paramValues = new ArrayList();
					paramValues.Add(iRowsAffected.ToString());
					lblErrorMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"ROWS_INS",utility.GetUserCulture(),paramValues);;
				}
				else
				{
					DataSet dsChangedRow = m_sdsConveyance.ds.GetChanges();
					iRowsAffected = SysDataManager1.UpdateConveyance(dsChangedRow,m_strAppID, m_strEnterpriseID, strAgentId);
					ArrayList paramValues = new ArrayList();
					paramValues.Add(iRowsAffected.ToString());
					lblErrorMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"ROWS_UPD",utility.GetUserCulture(),paramValues);;
					m_sdsConveyance.ds.Tables[0].Rows[e.Item.ItemIndex].AcceptChanges();
				}
			}
			catch(ApplicationException appException)
			{
				String strMsg = appException.Message;
				strMsg = appException.InnerException.Message;
				strMsg = appException.InnerException.InnerException.Message;
				if(strMsg.IndexOf("PRIMARY KEY") != -1 || strMsg.IndexOf("unique") != -1)
					lblErrorMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"DUP_KEY_FOUND",utility.GetUserCulture());
				else if(strMsg.IndexOf("duplicate key") != -1 )
					lblErrorMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"DUP_KEY_FOUND",utility.GetUserCulture());
				else if(strMsg.IndexOf("FOREIGN KEY") != -1 )
					lblErrorMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"MASTER_NOT_FOUND",utility.GetUserCulture());
				else
				{
					lblErrorMessage.Text =strMsg;
					return;
				}
				
				Logger.LogTraceError("DeliveryPathCode.aspx.cs","dgState_Update","RBAC003",appException.Message.ToString());
				return;
			}

			if (iRowsAffected == 0)
			{
				return;
			}
			Utility.EnableButton(ref btnInsert,ButtonType.Insert,true,m_moduleAccessRights);
			ViewState["ConvOperation"] = Operation.None;
			dgConveyance.EditItemIndex = -1;
			BindConveyanceGrid();
			Logger.LogDebugInfo("State","dgDelvPath_Update","INF004","updating data grid...");			
			
		}

		
		#endregion

		/*---*/
		private void btnQuery_Click(object sender, System.EventArgs e)
		{
			lblErrorMessage.Text = "";
			dgConveyance.CurrentPageIndex = 0;
			QueryMode();
		}

		private void btnInsert_Click(object sender, System.EventArgs e)
		{			
			lblErrorMessage.Text = "";
			if (btnExecuteQuery.Enabled == true)
				btnExecuteQuery.Enabled = false;

			if((int)ViewState["ConvMode"] != (int)ScreenMode.Insert || m_sdsConveyance.ds.Tables[0].Rows.Count >= dgConveyance.PageSize)
			{
				m_sdsConveyance = SysDataManager1.GetEmptyConveyance();
				dgConveyance.EditItemIndex = m_sdsConveyance.ds.Tables[0].Rows.Count - 1;
				dgConveyance.CurrentPageIndex = 0;
				AddRow();				
			}
			else
			{
				AddRow();
			}
			btnInsert.Enabled=false;
			EditHRow(false);
			ViewState["ConvMode"] = ScreenMode.Insert;
			ViewState["ConvOperation"] = Operation.Insert;
			dgConveyance.Columns[0].Visible=true;
			dgConveyance.Columns[1].Visible=true;
			dgConveyance.EditItemIndex = m_sdsConveyance.ds.Tables[0].Rows.Count - 1;
			dgConveyance.CurrentPageIndex = 0;
			BindConveyanceGrid();
			getPageControls(Page);
		}
		
		private void RetreiveSelectedPage()
		{
			String strAgentID=(String)ViewState["AgentID"];
			int iStartRow = dgConveyance.CurrentPageIndex * dgConveyance.PageSize;
			DataSet dsQuery = (DataSet)ViewState["QUERY_DSC"];			
			
			m_sdsConveyance = SysDataManager1.GetConveyanceDS( m_strAppID, m_strEnterpriseID, strAgentID, iStartRow, dgConveyance.PageSize, dsQuery);
			decimal iPageCnt = Convert.ToInt32((m_sdsConveyance.QueryResultMaxSize - 1))/dgConveyance.PageSize;
			if(iPageCnt < dgConveyance .CurrentPageIndex)
			{				
				dgConveyance.CurrentPageIndex = System.Convert.ToInt32(iPageCnt);
			}
			dgConveyance.SelectedIndex = -1;
			dgConveyance.EditItemIndex = -1;
			lblErrorMessage.Text = "";
			BindConveyanceGrid();
			Session["SESSION_DS1"] = m_sdsConveyance;
		}

		private void InsertAgent(int iRowIndex)
		{
			m_sdsAgent = AgentProfileMgrDAL.GetEmptyAgentDS(1);
			DataRow drEach =  m_sdsAgent.ds.Tables[0].Rows[0];	
			drEach["agentid"]	= dbCmbAgentId.Value; 
			drEach["agent_name"]= dbCmbAgentName.Value;
			Session["SESSION_DS3"]= m_sdsAgent;
		}
		
		private void GetAgentRecSet()
		{
			int iStartIndex = Convert.ToInt32(ViewState["currentSet"]) * m_iSetSize;
			DataSet dsQuery = (DataSet)ViewState["QUERY_DS"];
			m_sdsAgent = AgentProfileMgrDAL.GetAgentDS(m_strAppID,m_strEnterpriseID,iStartIndex,m_iSetSize,dsQuery);
			Session["SESSION_DS3"] = m_sdsAgent;
			ViewState["QryRes"] =  m_sdsAgent.QueryResultMaxSize; 			
			decimal pgCnt = (m_sdsAgent.QueryResultMaxSize - 1)/m_iSetSize;
			if(pgCnt < Convert.ToDecimal(ViewState["currentSet"]))
				ViewState["currentSet"] = System.Convert.ToInt32(pgCnt);
		}

		private void btnExecuteQuery_Click(object sender, System.EventArgs e)
		{
			lblErrorMessage.Text = "";
			btnExecuteQuery.Enabled = false;
			InsertAgent(0);
			ViewState["QUERY_DS"] = m_sdsAgent.ds;
			ViewState["currentPage"] = 0;
			ViewState["currentSet"] = 0;
			GetAgentRecSet();	
			if ((decimal)ViewState["QryRes"]!=0)
			{
				DisplayCurrentPage();
				ResetDetailsGridData();
				//Conveyance Rates			
				GetChangedRow(dgConveyance.Items[0],0);
				ViewState["QUERY_DSC"] = m_sdsConveyance.ds;
				dgConveyance.CurrentPageIndex = 0;
				RetreiveSelectedPage();	
				if(m_sdsConveyance.ds.Tables[0].Rows.Count==0)
				{
					lblErrorMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"NO_RECORD_FOUND",utility.GetUserCulture());
					dgConveyance.EditItemIndex = -1; 
					return;
				}
				//set all records to non-edit mode after execute query
				EditHRow(false);
				dgConveyance.Columns[0].Visible=true;
				dgConveyance.Columns[1].Visible=true;
				
				ViewState["ConvMode"] = ScreenMode.ExecuteQuery;
				ViewState["AgentMode"] = ScreenMode.ExecuteQuery;
				EnableNavigationButtons(true,true,true,true);
				dbCmbAgentId.Enabled=false;
				dbCmbAgentName.Enabled=false;
			}
			else
			{
				lblErrorMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"NO_RECORD_FOUND",utility.GetUserCulture());
			}
		}

		private void DisplayCurrentPage()
		{
			DataRow drCurrent = m_sdsAgent.ds.Tables[0].Rows[Convert.ToInt32(ViewState["currentPage"])];
						
			if(!drCurrent["agentid"].GetType().Equals(System.Type.GetType("System.DBNull")))
			{
				dbCmbAgentId.Text		= drCurrent["agentid"].ToString();
				dbCmbAgentId.Value		= drCurrent["agentid"].ToString();
				ViewState["AgentID"]	= drCurrent["agentid"].ToString();
			}
			if(!drCurrent["agent_name"].GetType().Equals(System.Type.GetType("System.DBNull")))
			{
				dbCmbAgentName.Text		= drCurrent["agent_name"].ToString();
				dbCmbAgentName.Value	= drCurrent["agent_name"].ToString();
			}
		}


		private void btnGoToLastPage_Click(object sender, System.EventArgs e)
		{
			ViewState["currentSet"] = Convert.ToInt32((m_sdsAgent.QueryResultMaxSize - 1))/m_iSetSize;	
			GetAgentRecSet();
			ViewState["currentPage"] = m_sdsAgent.DataSetRecSize - 1;
			DisplayCurrentPage();
			RetreiveSelectedPage();
			EnableNavigationButtons(true,true,false,false);
			Utility.EnableButton(ref btnInsert,ButtonType.Insert,true,m_moduleAccessRights);
			SetAgentIDServerStates();
			SetAgentNameServerStates();

		}

		private void btnPreviousPage_Click(object sender, System.EventArgs e)
		{
			int iCurrentPage = Convert.ToInt32(ViewState["currentPage"]);

			if(Convert.ToInt32(ViewState["currentPage"])  > 0 )
			{
				ViewState["currentPage"] = Convert.ToInt32(ViewState["currentPage"]) - 1;
				DisplayCurrentPage();	
				RetreiveSelectedPage();
			}
			else
			{
				if( (Convert.ToInt32(ViewState["currentSet"]) - 1) < 0)
				{
					EnableNavigationButtons(false,false,true,true);
					return;
				}
				
				ViewState["currentSet"] = Convert.ToInt32(ViewState["currentSet"]) - 1;	
				GetAgentRecSet();
				ViewState["currentPage"] = m_sdsAgent.DataSetRecSize - 1;
				DisplayCurrentPage();
				RetreiveSelectedPage();
			}
			EnableNavigationButtons(true,true,true,true);
			Utility.EnableButton(ref btnInsert,ButtonType.Insert,true,m_moduleAccessRights);
			SetAgentIDServerStates();
			SetAgentNameServerStates();
		}

		private void btnNextPage_Click(object sender, System.EventArgs e)
		{			
			int iCurrentPage = Convert.ToInt32(ViewState["currentPage"]);
			if(Convert.ToInt32(ViewState["currentPage"])  < (m_sdsAgent.DataSetRecSize - 1) )
			{
				ViewState["currentPage"] = Convert.ToInt32(ViewState["currentPage"]) + 1;
				DisplayCurrentPage();
				RetreiveSelectedPage();
			}
			else
			{
				decimal iTotalRec = (Convert.ToInt32(ViewState["currentSet"]) * m_iSetSize) + m_sdsAgent.DataSetRecSize;
				if( iTotalRec ==  m_sdsAgent.QueryResultMaxSize)
				{
					EnableNavigationButtons(true,true,false,false);
					return;
				}
				
				ViewState["currentSet"] = Convert.ToInt32(ViewState["currentSet"]) + 1;	
				ViewState["currentPage"] = 0;
				DisplayCurrentPage();
				RetreiveSelectedPage();
			}
			EnableNavigationButtons(true,true,true,true);
			Utility.EnableButton(ref btnInsert,ButtonType.Insert,true,m_moduleAccessRights);
			SetAgentIDServerStates();
			SetAgentNameServerStates();
		}

		private void btnGoToFirstPage_Click(object sender, System.EventArgs e)
		{
			ViewState["currentSet"] = 0;	
			GetAgentRecSet();
			ViewState["currentPage"] = 0;
			DisplayCurrentPage();
			RetreiveSelectedPage();
			EnableNavigationButtons(false,false,true,true);		
			Utility.EnableButton(ref btnInsert,ButtonType.Insert,true,m_moduleAccessRights);
			SetAgentIDServerStates();
			SetAgentNameServerStates();
		}


		private void EnableNavigationButtons(bool bMoveFirst, bool bMoveNext,bool bEnableMovePrevious, bool bMoveLast)
		{
			btnGoToFirstPage.Enabled	= bMoveFirst;
			btnPreviousPage.Enabled		= bMoveNext;
			btnNextPage.Enabled			= bEnableMovePrevious;
			btnGoToLastPage.Enabled		= bMoveLast;
		}

		
		private void dbCmbAgentId_SelectedItemChanged(object sender, System.EventArgs e)
		{
			DbComboRegKey();			
			SetAgentNameServerStates();
		}

		private void dbCmbAgentName_SelectedItemChanged(object sender, System.EventArgs e)
		{
			DbComboRegKey();
			SetAgentIDServerStates();			
		}
		
		private void ResetDetailsGrid()
		{
			m_sdsConveyance = SysDataManager1.GetEmptyConveyance(0);			// 11/11/2002 
			Session["SESSION_DS1"]	= m_sdsConveyance;		
			dgConveyance.EditItemIndex = 0;
			dgConveyance.CurrentPageIndex = 0;
			BindConveyanceGrid();
		}
		private void ResetDetailsGridData()
		{
			m_sdsConveyance = SysDataManager1.GetEmptyConveyance(1);			// 11/11/2002 
			Session["SESSION_DS1"]	= m_sdsConveyance;		
			dgConveyance.EditItemIndex = 0;
			dgConveyance.CurrentPageIndex = 0;
			BindConveyanceGrid();
		}

			
	}
}
