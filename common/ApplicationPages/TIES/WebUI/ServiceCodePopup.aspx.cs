using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Text;
using com.common.DAL;
using com.common.util;
using com.common.classes;
using com.ties.BAL;
using com.common.applicationpages;
//HC Return Task
using com.ties.classes;
using com.ties.DAL;
//HC Return Task


namespace TIES.WebUI
{
	/// <summary>
	/// Summary description for ServiceCodePopup.
	/// </summary>
	public class ServiceCodePopup : BasePopupPage
	{
		protected System.Web.UI.WebControls.DataGrid dgServiceCode;
		protected System.Web.UI.WebControls.Button btnSearch;
		protected System.Web.UI.WebControls.Label lblCustID;
		protected System.Web.UI.WebControls.TextBox txtServiceCode;
		protected System.Web.UI.WebControls.TextBox txtServiceDescription;
		protected System.Web.UI.WebControls.Button btnRefresh;
		//Utility utility = null;

		private String appID = null;
		private String enterpriseID = null;
		private String userID = null;	//HC Return Task
		private String strPickUpDtTime = "";
		private String strDestZipCode = null;
		private String strSendZipCode = null;
		private String strFormId=null;
		protected System.Web.UI.WebControls.Label lblErrorMsg;
		protected System.Web.UI.WebControls.Label lblServiceTitle;
		protected System.Web.UI.WebControls.Label lblCustName;
		protected System.Web.UI.WebControls.Button btnClose;
		SessionDS m_sdsService = null;
		String strConsignmentNo = null;
		String strBookingNo = null;
		protected System.Web.UI.WebControls.TextBox txtSendPostCode;
		protected System.Web.UI.WebControls.TextBox txtRecipPostcode;
		protected System.Web.UI.WebControls.Label lblSendPostCode;
		protected System.Web.UI.WebControls.Label lblRecipPostCode;
		protected System.Web.UI.WebControls.Label lblErrorMsg2;
		String strFormName = null;

		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			
			strPickUpDtTime = Request.Params["PickUpDtTime"];
			strDestZipCode = Request.Params["DestZipCode"];
			strSendZipCode = Request.Params["SendZipCode"];
			strFormId=Request.Params["FORMID"];  
			strConsignmentNo = Request.Params["CONNO"];
			strBookingNo = Request.Params["BOOKINGNO"];
			strFormName	 = Request.Params["FORMN"];
			
			//utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings,Page.Session);
			appID = utility.GetAppID();
			enterpriseID = utility.GetEnterpriseID();
			if(!Page.IsPostBack)
			{
				txtServiceCode.Text = Request.Params["SERVICECODE"];
				txtServiceDescription.Text = Request.Params["SERVICEDESC"];
				m_sdsService = GetEmptyServiceDS(0);
				BindGrid();
			}
			else
			{
				m_sdsService = (SessionDS)ViewState["SERVICE_DS"];

			}

			if(strFormName == "PickupRequestBooking")
			{
				//this.dgServiceCode.Columns[4].Visible=false;
				lblCustID.Visible = false;
				lblCustName.Visible = false;
				txtServiceCode.Visible = false;
				txtServiceDescription.Visible = false;
				lblSendPostCode.Visible = true;
				lblRecipPostCode.Visible = true;
				txtSendPostCode.Visible = true;
				txtRecipPostcode.Visible = true;
				if(strSendZipCode != "" && !Page.IsPostBack)
					txtSendPostCode.Text = strSendZipCode;	

				LoadService();
			}
			if(strFormName == "DomesticShipment" && !Page.IsPostBack)
			{
				txtServiceCode.Text = Request.Params["ServiceCode"];
				btnSearch_Click(this, new System.EventArgs());
			}
			if(strFormId == "ImportConsignments")
			{
				btnSearch_Click(this, new System.EventArgs());
			}
		}
		public void Paging(Object sender, DataGridPageChangedEventArgs e)
		{
			//runs when one of the page control is clicked
			//update the current page from the parameter values
			dgServiceCode.CurrentPageIndex = e.NewPageIndex;
			//bind data to the grid
			ShowCurrentPage();
		}
		
		public static SessionDS GetEmptyServiceDS(int iNumRows)
		{
			DataTable dtService = new DataTable();
 
			dtService.Columns.Add(new DataColumn("service_code", typeof(string)));
			dtService.Columns.Add(new DataColumn("service_description", typeof(string)));
			dtService.Columns.Add(new DataColumn("service_charge_percent", typeof(decimal)));
			dtService.Columns.Add(new DataColumn("service_charge_amt", typeof(Int32)));

			for(int i=0; i < iNumRows; i++)
			{
				DataRow drEach = dtService.NewRow();
				drEach[0] = "";
				drEach[1] = "";
				drEach[2] = 0;
				drEach[3] = 0;

				dtService.Rows.Add(drEach);
			}

			DataSet dsService = new DataSet();
			dsService.Tables.Add(dtService);

			SessionDS sessionDS = new SessionDS();
			sessionDS.ds = dsService;
			sessionDS.DataSetRecSize = dsService.Tables[0].Rows.Count;
			sessionDS.QueryResultMaxSize = sessionDS.DataSetRecSize;

			return  sessionDS;	
		}

		private void LoadService ()
		{
			if(strSendZipCode == "")
			{	
				StringBuilder strQry = new StringBuilder();
				strQry.Append(" Select distinct Service.service_code, ISNULL(Service.transit_day * 24, 0) + ISNULL(Service.transit_hour, 0) AS transit_time, ");
				strQry.Append(" Service.service_description, Service.service_charge_percent, Service.service_charge_amt, convert(varchar(5),Service.commit_time,108) as commit_time ");
				strQry.Append(" FROM   Zipcode INNER JOIN   Delivery_Path ON Zipcode.applicationid = Delivery_Path.applicationid "); 
				strQry.Append(" AND Zipcode.enterpriseid = Delivery_Path.enterpriseid AND Zipcode.pickup_route = Delivery_Path.path_code ");
				strQry.Append(" INNER JOIN  Service ON Delivery_Path.applicationid = Service.applicationid AND Delivery_Path.enterpriseid = Service.enterpriseid ");
				strQry.Append(" ORDER BY transit_time, commit_time");
				String strSQLQuery = strQry.ToString();
				ViewState["SQL_QUERY"] = strSQLQuery;
				dgServiceCode.CurrentPageIndex = 0;
				ShowCurrentPage();
			}
		}

		private SessionDS GetServiceDS(int iCurrent, int iDSRecSize, String strQuery)
		{
			SessionDS sessionDS = null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(appID,enterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("ServicePopup.aspx.cs","GetServiceDS","ERR001","DbConnection object is null!!");
				return sessionDS;
			}
			
			sessionDS = dbCon.ExecuteQuery(strQuery,iCurrent,iDSRecSize,"SenderRecipentData");
			return  sessionDS;			
		}

		private void BindGrid()
		{
			dgServiceCode.VirtualItemCount = System.Convert.ToInt32(m_sdsService.QueryResultMaxSize);
			dgServiceCode.DataSource = m_sdsService.ds;
			dgServiceCode.DataBind();
			ViewState["SERVICE_DS"] = m_sdsService;
		}

		private void ShowCurrentPage()
		{
			int iStartIndex = dgServiceCode.CurrentPageIndex * dgServiceCode.PageSize;
			String sqlQuery = (String)ViewState["SQL_QUERY"];
			m_sdsService = GetServiceDS(iStartIndex,dgServiceCode.PageSize,sqlQuery);
			int pgCnt = (Convert.ToInt32(m_sdsService.QueryResultMaxSize - 1))/dgServiceCode.PageSize;
			if(pgCnt < dgServiceCode.CurrentPageIndex)
			{
				dgServiceCode.CurrentPageIndex = System.Convert.ToInt32(pgCnt);
			}
			dgServiceCode.SelectedIndex = -1;
			dgServiceCode.EditItemIndex = -1;
			BindGrid();
			getPageControls(Page);
		}


		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			base.OnInit(e);
			InitializeComponent();
			
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.dgServiceCode.SelectedIndexChanged += new System.EventHandler(this.dgServiceCode_SelectedIndexChanged);
			this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
			this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private void btnSearch_Click(object sender, System.EventArgs e)
		{
			String strServiceCode = txtServiceCode.Text.Trim();
			String strServiceDesc = txtServiceDescription.Text.Trim();

			StringBuilder strQry = new StringBuilder();
			if(strFormName == "DomesticShipment")
			{	
				DataSet tmpDC = DomesticShipmentMgrDAL.getDCToOrigStation(appID, enterpriseID,
					0, 0, strSendZipCode.Trim()).ds;

				String strOriginDC = tmpDC.Tables[0].Rows[0]["origin_station"].ToString();

				tmpDC.Dispose();
				tmpDC = null;

//				strQry.Append("select service_code, transit_time,service_description,service_charge_percent,service_charge_amt from (");
//				strQry.Append(" Select distinct Service.service_code, ISNULL(Service.transit_day * 24, 0) + ISNULL(Service.transit_hour, 0) AS transit_time, ");
//				strQry.Append(" Service.service_description, Service.service_charge_percent, Service.service_charge_amt, convert(varchar(5),Service.commit_time,108) as commit_time ");
//				strQry.Append(" FROM   Zipcode INNER JOIN   Delivery_Path ON Zipcode.applicationid = Delivery_Path.applicationid "); 
//				strQry.Append(" AND Zipcode.enterpriseid = Delivery_Path.enterpriseid AND Zipcode.pickup_route = Delivery_Path.path_code ");
//				strQry.Append(" INNER JOIN  Service ON Delivery_Path.applicationid = Service.applicationid AND Delivery_Path.enterpriseid = Service.enterpriseid ");
//				strQry.Append(" WHERE (Zipcode.zipcode = '"+strSendZipCode+"') and Service.Service_code not in (select distinct service_code ");
//				strQry.Append(" from Zipcode_Service_Excluded where Zipcode_Service_Excluded.zipcode ='"+strDestZipCode+"') ");
//				strQry.Append(" AND Zipcode.applicationid='"+appID+"' AND Zipcode.enterpriseid='"+enterpriseID+"' ");
//				strQry.Append(" and Service.Service_code not in (Select Best_Service_Available.service_code");
//				strQry.Append(" from Best_Service_Available inner join Service on Best_Service_Available.service_code = Service.service_code");
//				strQry.Append(" Where Best_Service_Available.origin_dc ='"+strOriginDC+"'  and Best_Service_Available.recipientZipCode ='"+strDestZipCode+"'  )");	
//				strQry.Append(" union Select Best_Service_Available.service_code, -1, Service.service_description, Service.service_charge_percent, Service.service_charge_amt , convert(varchar(5),Service.commit_time,108) as commit_time ");
//				strQry.Append("   from Best_Service_Available inner join Service on Best_Service_Available.service_code = Service.service_code ");
//				strQry.Append("    Where Best_Service_Available.origin_dc ='"+strOriginDC+"' and Best_Service_Available.recipientZipCode ='"+strDestZipCode+"') Service Where service_code not in (select distinct service_code from Zipcode_Service_Excluded where Zipcode_Service_Excluded.zipcode ='"+strDestZipCode+"') ");
				
				strQry.Append(" Select 0 as BSA,applicationid,enterpriseid,service_code, ISNULL(Service.transit_day * 24, 0) + ISNULL(Service.transit_hour, 0) AS transit_time");
				strQry.Append(",service_description,service_charge_percent,service_charge_amt, convert(varchar(5),Service.commit_time,108) as commited_time from Service");
				strQry.Append(" Where service_code in (select service_code from Best_Service_Available");
				strQry.Append(" where origin_dc = '"+Utility.ReplaceSingleQuote(strOriginDC)+"'");
				strQry.Append(" and recipientZipCode = '"+Utility.ReplaceSingleQuote(strDestZipCode)+"')");
				if(strServiceCode != null && strServiceCode != "")
				{
					strQry.Append(" and Service.service_code like '%"+Utility.ReplaceSingleQuote(strServiceCode)+"%' ");
				}
				if ((strServiceDesc != null) && (!strServiceDesc.Equals("")))
				{
					strQry.Append(" and Service.service_description like N'%" + Utility.ReplaceSingleQuote(strServiceDesc) + "%' ");
				}
				strQry.Append(" Union");
				strQry.Append(" select 1 as BSA,* from (Select applicationid,enterpriseid,service_code, ISNULL(Service.transit_day * 24, 0) + ISNULL(Service.transit_hour, 0) AS transit_time");
				strQry.Append(",service_description,service_charge_percent,service_charge_amt, convert(varchar(5),Service.commit_time,108) as commited_time from Service) SR");
				strQry.Append(" Where (((SR.commited_time >= (select convert(varchar(5),Service.commit_time,108) as commited_time");
				strQry.Append(" from Service where service_code in (select service_code from Best_Service_Available");
				strQry.Append(" where origin_dc = '"+Utility.ReplaceSingleQuote(strOriginDC)+"'");
				strQry.Append(" and recipientZipCode = '"+Utility.ReplaceSingleQuote(strDestZipCode)+"')))");
				strQry.Append(" AND (SR.transit_time = (select ISNULL(Service.transit_day * 24, 0) + ISNULL(Service.transit_hour, 0) AS transit_time");
				strQry.Append(" from Service where service_code in (select service_code from Best_Service_Available");
				strQry.Append(" where origin_dc = '"+Utility.ReplaceSingleQuote(strOriginDC)+"'");
				strQry.Append(" and recipientZipCode = '"+Utility.ReplaceSingleQuote(strDestZipCode)+"'))))");
				strQry.Append(" or (SR.transit_time > (select ISNULL(Service.transit_day * 24, 0) + ISNULL(Service.transit_hour, 0) AS transit_time");
				strQry.Append(" from Service where service_code in (select service_code from Best_Service_Available");
				strQry.Append(" where origin_dc = '"+Utility.ReplaceSingleQuote(strOriginDC)+"'");
				strQry.Append(" and recipientZipCode = '"+Utility.ReplaceSingleQuote(strDestZipCode)+"')))) ");
				strQry.Append(" and SR.service_code not in (select service_code from Best_Service_Available");
				strQry.Append(" where origin_dc = '"+Utility.ReplaceSingleQuote(strOriginDC)+"'");
				strQry.Append(" and recipientZipCode = '"+Utility.ReplaceSingleQuote(strDestZipCode)+"')");
				strQry.Append(" AND SR.applicationid = '"+appID+"' and SR.enterpriseid = '"+enterpriseID+"'");
				// Comment By Athikom on 26 Feb 2010
////				strQry.Append(" select 1 as BSA,* from (Select applicationid,enterpriseid,service_code, ISNULL(Service.transit_day * 24, 0) + ISNULL(Service.transit_hour, 0) AS transit_time");
////				strQry.Append(",service_description,service_charge_percent,service_charge_amt, convert(varchar(5),Service.commit_time,108) as commited_time from Service) SR");
////				strQry.Append(" Where SR.commited_time >= (select convert(varchar(5),Service.commit_time,108) as commited_time");
////				strQry.Append(" from Service where service_code in (select service_code from Best_Service_Available");
////				strQry.Append(" where origin_dc = '"+Utility.ReplaceSingleQuote(strOriginDC)+"'");
////				strQry.Append(" and recipientZipCode = '"+Utility.ReplaceSingleQuote(strDestZipCode)+"'))");
////				strQry.Append(" AND SR.transit_time >= (select ISNULL(Service.transit_day * 24, 0) + ISNULL(Service.transit_hour, 0) AS transit_time");
////				strQry.Append(" from Service where service_code in (select service_code from Best_Service_Available");
////				strQry.Append(" where origin_dc = '"+Utility.ReplaceSingleQuote(strOriginDC)+"'");
////				strQry.Append(" and recipientZipCode = '"+Utility.ReplaceSingleQuote(strDestZipCode)+"'))");
////				strQry.Append(" and SR.service_code not in (select service_code from Best_Service_Available");
////				strQry.Append(" where origin_dc = '"+Utility.ReplaceSingleQuote(strOriginDC)+"'");
////				strQry.Append(" and recipientZipCode = '"+Utility.ReplaceSingleQuote(strDestZipCode)+"')");
////				strQry.Append(" AND SR.applicationid = '"+appID+"' and SR.enterpriseid = '"+enterpriseID+"'");
				// end Comment  
			}
			else if(strFormId == "ImportConsignments")
			{	
				DataSet tmpDC = DomesticShipmentMgrDAL.getDCToOrigStation(appID, enterpriseID,
					0, 0, strSendZipCode.Trim()).ds;

				String strOriginDC = tmpDC.Tables[0].Rows[0]["origin_station"].ToString();

				tmpDC.Dispose();
				tmpDC = null;

				strQry.Append(" Select 0 as BSA,applicationid,enterpriseid,service_code, ISNULL(Service.transit_day * 24, 0) + ISNULL(Service.transit_hour, 0) AS transit_time");
				strQry.Append(",service_description,service_charge_percent,service_charge_amt, convert(varchar(5),Service.commit_time,108) as commited_time from Service");
				strQry.Append(" Where service_code in (select service_code from Best_Service_Available");
				strQry.Append(" where origin_dc = '"+Utility.ReplaceSingleQuote(strOriginDC)+"'");
				strQry.Append(" and recipientZipCode = '"+Utility.ReplaceSingleQuote(strDestZipCode)+"')");
				if(strServiceCode != null && strServiceCode != "")
				{
					strQry.Append(" and Service.service_code like '%"+Utility.ReplaceSingleQuote(strServiceCode)+"%' ");
				}
				if ((strServiceDesc != null) && (!strServiceDesc.Equals("")))
				{
					strQry.Append(" and Service.service_description like N'%" + Utility.ReplaceSingleQuote(strServiceDesc) + "%' ");
				}
				strQry.Append(" Union");
				strQry.Append(" select 1 as BSA,* from (Select applicationid,enterpriseid,service_code, ISNULL(Service.transit_day * 24, 0) + ISNULL(Service.transit_hour, 0) AS transit_time");
				strQry.Append(",service_description,service_charge_percent,service_charge_amt, convert(varchar(5),Service.commit_time,108) as commited_time from Service) SR");
				strQry.Append(" Where (((SR.commited_time >= (select convert(varchar(5),Service.commit_time,108) as commited_time");
				strQry.Append(" from Service where service_code in (select service_code from Best_Service_Available");
				strQry.Append(" where origin_dc = '"+Utility.ReplaceSingleQuote(strOriginDC)+"'");
				strQry.Append(" and recipientZipCode = '"+Utility.ReplaceSingleQuote(strDestZipCode)+"')))");
				strQry.Append(" AND (SR.transit_time = (select ISNULL(Service.transit_day * 24, 0) + ISNULL(Service.transit_hour, 0) AS transit_time");
				strQry.Append(" from Service where service_code in (select service_code from Best_Service_Available");
				strQry.Append(" where origin_dc = '"+Utility.ReplaceSingleQuote(strOriginDC)+"'");
				strQry.Append(" and recipientZipCode = '"+Utility.ReplaceSingleQuote(strDestZipCode)+"'))))");
				strQry.Append(" or (SR.transit_time > (select ISNULL(Service.transit_day * 24, 0) + ISNULL(Service.transit_hour, 0) AS transit_time");
				strQry.Append(" from Service where service_code in (select service_code from Best_Service_Available");
				strQry.Append(" where origin_dc = '"+Utility.ReplaceSingleQuote(strOriginDC)+"'");
				strQry.Append(" and recipientZipCode = '"+Utility.ReplaceSingleQuote(strDestZipCode)+"')))) ");
				strQry.Append(" and SR.service_code not in (select service_code from Best_Service_Available");
				strQry.Append(" where origin_dc = '"+Utility.ReplaceSingleQuote(strOriginDC)+"'");
				strQry.Append(" and recipientZipCode = '"+Utility.ReplaceSingleQuote(strDestZipCode)+"')");
				strQry.Append(" AND SR.applicationid = '"+appID+"' and SR.enterpriseid = '"+enterpriseID+"'");
				// Comment By Athikom on 26 Feb 2010
////				strQry.Append(" select 1 as BSA,* from (Select applicationid,enterpriseid,service_code, ISNULL(Service.transit_day * 24, 0) + ISNULL(Service.transit_hour, 0) AS transit_time");
////				strQry.Append(",service_description,service_charge_percent,service_charge_amt, convert(varchar(5),Service.commit_time,108) as commited_time from Service) SR");
////				strQry.Append(" Where SR.commited_time >= (select convert(varchar(5),Service.commit_time,108) as commited_time");
////				strQry.Append(" from Service where service_code in (select service_code from Best_Service_Available");
////				strQry.Append(" where origin_dc = '"+Utility.ReplaceSingleQuote(strOriginDC)+"'");
////				strQry.Append(" and recipientZipCode = '"+Utility.ReplaceSingleQuote(strDestZipCode)+"'))");
////				strQry.Append(" AND SR.transit_time >= (select ISNULL(Service.transit_day * 24, 0) + ISNULL(Service.transit_hour, 0) AS transit_time");
////				strQry.Append(" from Service where service_code in (select service_code from Best_Service_Available");
////				strQry.Append(" where origin_dc = '"+Utility.ReplaceSingleQuote(strOriginDC)+"'");
////				strQry.Append(" and recipientZipCode = '"+Utility.ReplaceSingleQuote(strDestZipCode)+"'))");
////				strQry.Append(" and SR.service_code not in (select service_code from Best_Service_Available");
////				strQry.Append(" where origin_dc = '"+Utility.ReplaceSingleQuote(strOriginDC)+"'");
////				strQry.Append(" and recipientZipCode = '"+Utility.ReplaceSingleQuote(strDestZipCode)+"')");
////				strQry.Append(" AND SR.applicationid = '"+appID+"' and SR.enterpriseid = '"+enterpriseID+"'");
				// end Comment  
			}
			else if(strFormName == "PickupRequestBooking")
			{	
				lblErrorMsg2.Text = "";
				Zipcode Zip = new Zipcode();
				if(txtSendPostCode.Text.Trim() == "")
				{
					lblErrorMsg2.Text = "Please enter Sender Postal Code";
					return;
				}
				else
				{
					if(Zip.ckZipcodeInSystem(this.appID,this.enterpriseID,txtSendPostCode.Text.Trim()).Tables[0].Rows.Count<=0)
					{
						lblErrorMsg2.Text = "Please enter a valid Sender postal code";
						return;
					}
				}					
					
				if(txtRecipPostcode.Text.Trim() == "")
				{
					lblErrorMsg2.Text = "Please enter Recipient Postal Code";
					return;
				}
				else
				{
					if(Zip.ckZipcodeInSystem(this.appID,this.enterpriseID,txtRecipPostcode.Text.Trim()).Tables[0].Rows.Count<=0)
					{
						lblErrorMsg2.Text = "Please enter a valid Recipient postal code";
						return;
					}
				}

				if(txtSendPostCode.Text.Trim() != "" && txtRecipPostcode.Text.Trim() != "")
				{
					String strSendZipCode = txtSendPostCode.Text.Trim();
					String strDestZipCode = txtRecipPostcode.Text.Trim();
					DataSet tmpDC = DomesticShipmentMgrDAL.getDCToOrigStation(appID, enterpriseID,
						0, 0, strSendZipCode.Trim()).ds;

					String strOriginDC = tmpDC.Tables[0].Rows[0]["origin_station"].ToString();

					tmpDC.Dispose();
					tmpDC = null;
					strQry.Append(" Select 0 as BSA,applicationid,enterpriseid,service_code, ISNULL(Service.transit_day * 24, 0) + ISNULL(Service.transit_hour, 0) AS transit_time");
					strQry.Append(",service_description,service_charge_percent,service_charge_amt, convert(varchar(5),Service.commit_time,108) as commited_time from Service");
					strQry.Append(" Where service_code in (select service_code from Best_Service_Available");
					strQry.Append(" where origin_dc = '"+Utility.ReplaceSingleQuote(strOriginDC)+"'");
					strQry.Append(" and recipientZipCode = '"+Utility.ReplaceSingleQuote(strDestZipCode)+"')");
					if(strServiceCode != null && strServiceCode != "")
					{
						strQry.Append(" and Service.service_code like '%"+Utility.ReplaceSingleQuote(strServiceCode)+"%' ");
					}
					if ((strServiceDesc != null) && (!strServiceDesc.Equals("")))
					{
						strQry.Append(" and Service.service_description like N'%" + Utility.ReplaceSingleQuote(strServiceDesc) + "%' ");
					}
					strQry.Append(" Union");
					strQry.Append(" select 1 as BSA,* from (Select applicationid,enterpriseid,service_code, ISNULL(Service.transit_day * 24, 0) + ISNULL(Service.transit_hour, 0) AS transit_time");
					strQry.Append(",service_description,service_charge_percent,service_charge_amt, convert(varchar(5),Service.commit_time,108) as commited_time from Service) SR");
					strQry.Append(" Where (((SR.commited_time >= (select convert(varchar(5),Service.commit_time,108) as commited_time");
					strQry.Append(" from Service where service_code in (select service_code from Best_Service_Available");
					strQry.Append(" where origin_dc = '"+Utility.ReplaceSingleQuote(strOriginDC)+"'");
					strQry.Append(" and recipientZipCode = '"+Utility.ReplaceSingleQuote(strDestZipCode)+"')))");
					strQry.Append(" AND (SR.transit_time = (select ISNULL(Service.transit_day * 24, 0) + ISNULL(Service.transit_hour, 0) AS transit_time");
					strQry.Append(" from Service where service_code in (select service_code from Best_Service_Available");
					strQry.Append(" where origin_dc = '"+Utility.ReplaceSingleQuote(strOriginDC)+"'");
					strQry.Append(" and recipientZipCode = '"+Utility.ReplaceSingleQuote(strDestZipCode)+"'))))");
					strQry.Append(" or (SR.transit_time > (select ISNULL(Service.transit_day * 24, 0) + ISNULL(Service.transit_hour, 0) AS transit_time");
					strQry.Append(" from Service where service_code in (select service_code from Best_Service_Available");
					strQry.Append(" where origin_dc = '"+Utility.ReplaceSingleQuote(strOriginDC)+"'");
					strQry.Append(" and recipientZipCode = '"+Utility.ReplaceSingleQuote(strDestZipCode)+"')))) ");
					strQry.Append(" and SR.service_code not in (select service_code from Best_Service_Available");
					strQry.Append(" where origin_dc = '"+Utility.ReplaceSingleQuote(strOriginDC)+"'");
					strQry.Append(" and recipientZipCode = '"+Utility.ReplaceSingleQuote(strDestZipCode)+"')");
					strQry.Append(" AND SR.applicationid = '"+appID+"' and SR.enterpriseid = '"+enterpriseID+"'");
					// Comment By Athikom on 26 Feb 2010
////					strQry.Append("select service_code, transit_time,service_description,service_charge_percent,service_charge_amt from (");
////					strQry.Append(" Select distinct Service.service_code, ISNULL(Service.transit_day * 24, 0) + ISNULL(Service.transit_hour, 0) AS transit_time, ");
////					strQry.Append(" Service.service_description, Service.service_charge_percent, Service.service_charge_amt, convert(varchar(5),Service.commit_time,108) as commit_time ");
////					strQry.Append(" FROM   Zipcode INNER JOIN   Delivery_Path ON Zipcode.applicationid = Delivery_Path.applicationid "); 
////					strQry.Append(" AND Zipcode.enterpriseid = Delivery_Path.enterpriseid AND Zipcode.pickup_route = Delivery_Path.path_code ");
////					strQry.Append(" INNER JOIN  Service ON Delivery_Path.applicationid = Service.applicationid AND Delivery_Path.enterpriseid = Service.enterpriseid ");
////					strQry.Append(" WHERE (Zipcode.zipcode = '"+strSendZipCode+"') and Service.Service_code not in (select distinct service_code ");
////					strQry.Append(" from Zipcode_Service_Excluded where Zipcode_Service_Excluded.zipcode ='"+strDestZipCode+"') ");
////					strQry.Append(" AND Zipcode.applicationid='"+appID+"' AND Zipcode.enterpriseid='"+enterpriseID+"' ");
////					strQry.Append(" and Service.Service_code not in (Select Best_Service_Available.service_code");
////					strQry.Append(" from Best_Service_Available inner join Service on Best_Service_Available.service_code = Service.service_code");
////					strQry.Append(" Where Best_Service_Available.origin_dc ='"+strOriginDC+"'  and Best_Service_Available.recipientZipCode ='"+strDestZipCode+"'  )");	
////					strQry.Append(" union Select Best_Service_Available.service_code, -1, Service.service_description, Service.service_charge_percent, Service.service_charge_amt , convert(varchar(5),Service.commit_time,108) as commit_time ");
////					strQry.Append("   from Best_Service_Available inner join Service on Best_Service_Available.service_code = Service.service_code ");
////					strQry.Append("    Where Best_Service_Available.origin_dc ='"+strOriginDC+"' and Best_Service_Available.recipientZipCode ='"+strDestZipCode+"') Service Where service_code not in (select distinct service_code from Zipcode_Service_Excluded where Zipcode_Service_Excluded.zipcode ='"+strDestZipCode+"') ");
					// end Comment  
//					strQry.Append(" Select distinct Service.service_code, ISNULL(Service.transit_day * 24, 0) + ISNULL(Service.transit_hour, 0) AS transit_time, ");
//					strQry.Append(" Service.service_description, Service.service_charge_percent, Service.service_charge_amt , convert(varchar(5),Service.commit_time,108) as commit_time  ");
//					strQry.Append(" FROM   Zipcode INNER JOIN   Delivery_Path ON Zipcode.applicationid = Delivery_Path.applicationid "); 
//					strQry.Append(" AND Zipcode.enterpriseid = Delivery_Path.enterpriseid AND Zipcode.pickup_route = Delivery_Path.path_code ");
//					strQry.Append(" INNER JOIN  Service ON Delivery_Path.applicationid = Service.applicationid AND Delivery_Path.enterpriseid = Service.enterpriseid ");
//					strQry.Append(" WHERE (Zipcode.zipcode = '"+txtSendPostCode.Text.Trim()+"') and Service.Service_code not in (select distinct service_code ");
//					strQry.Append(" from Zipcode_Service_Excluded where Zipcode_Service_Excluded.zipcode ='"+txtRecipPostcode.Text.Trim()+"') ");
//					strQry.Append(" AND Zipcode.applicationid='"+appID+"' AND Zipcode.enterpriseid='"+enterpriseID+"' ");
				}
			}
			else
			{
				strQry.Append(" Select Service.service_code,Service.service_description, Service.service_charge_percent,Service.service_charge_amt , convert(varchar(5),Service.commit_time,108) as commit_time "); 
				strQry.Append(" from Service where applicationid='"+appID+"' and Service.enterpriseid = '"+enterpriseID+"'");
			}

			if(strServiceCode != null && strServiceCode != "")
			{
				if(strFormName == "PickupRequestBooking" || strFormName == "DomesticShipment" || strFormId == "ImportConsignments")
					strQry.Append(" and SR.service_code like '%"+Utility.ReplaceSingleQuote(strServiceCode)+"%' ");
				else
					strQry.Append(" and Service.service_code like '%"+Utility.ReplaceSingleQuote(strServiceCode)+"%' ");
			}
			
			if ((strServiceDesc != null) && (!strServiceDesc.Equals("")))
			{
				if(strFormName == "PickupRequestBooking" || strFormName == "DomesticShipment" || strFormId == "ImportConsignments")
					strQry.Append(" and SR.service_description like N'%" + Utility.ReplaceSingleQuote(strServiceDesc) + "%' ");
				else
					strQry.Append(" and Service.service_description like N'%" + Utility.ReplaceSingleQuote(strServiceDesc) + "%' ");
			}
			if(strFormName == "PickupRequestBooking" || strFormName == "DomesticShipment" || strFormId == "ImportConsignments")
			{
				//strQry.Append(" ORDER BY transit_time, commit_time");
				strQry.Append(" ORDER BY transit_time");
			}

			if(strQry.ToString() != "")
			{
				String strSQLQuery = strQry.ToString();
				ViewState["SQL_QUERY"] = strSQLQuery;
				dgServiceCode.CurrentPageIndex = 0;
				ShowCurrentPage();
			}
		}

		
		private void dgServiceCode_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			//Get the value and return it to the parent window
			int iSelIndex = dgServiceCode.SelectedIndex;
			DataGridItem dgRow = dgServiceCode.Items[iSelIndex];
			String strServiceCode = dgRow.Cells[0].Text;
			String strServiceDesc = dgRow.Cells[1].Text;
			String strServiceCharge = dgRow.Cells[2].Text;
			String strServiceAmt = dgRow.Cells[3].Text;

			if(strServiceDesc == "&nbsp;")
			{
				strServiceDesc = "";
			}
			if(strServiceCharge == "&nbsp;")
			{
				strServiceCharge = "";
			}
			if(strServiceAmt == "&nbsp;")
			{
				strServiceAmt = "";
			}
			String sScript = "";
			sScript += "<script language=javascript>";

			if(strFormId=="BandQuotation")
			{
				String strCodeClientid = Request.Params["CODEID"];
				String strDescClientid = Request.Params["DESCID"];
				String strSurchargeClientid = Request.Params["SURCHARGEID"];
				String strServiceChargeAmtId=Request.Params["SERVICE_CHARGE_AMT_ID"];

				sScript += "  window.opener.BandQuotation."+strCodeClientid+".value = \"" + strServiceCode + "\";";
				sScript += "  window.opener.BandQuotation."+strDescClientid+".value = \"" + strServiceDesc + "\";";
				sScript += "  window.opener.BandQuotation."+strSurchargeClientid+".value = \"" + strServiceCharge +"\";";
				sScript += "  window.opener.BandQuotation."+strServiceChargeAmtId+".value = \"" + strServiceAmt +"\";";
			}
			else if(strFormId=="ShipmentDetails")
			{
				String strDlvryDt = "";
				if(strPickUpDtTime.Length > 0)
				{
					DomesticShipmentMgrBAL domesticMgrBAL = new DomesticShipmentMgrBAL();
					strDlvryDt =  domesticMgrBAL.CalcDlvryDtTime(appID,enterpriseID,System.DateTime.ParseExact(strPickUpDtTime,"dd/MM/yyyy HH:mm",null),strServiceCode,strDestZipCode).ToString();
				}
				sScript += "  window.opener.ShipmentDetails.txtShpSvcCode.value = \"" + strServiceCode + "\";";
				sScript += "  window.opener.ShipmentDetails.txtShpSvcDesc.value = \"" + strServiceDesc + "\";";
				sScript += "  window.opener.ShipmentDetails.txtEstDelvdate.value = \"" + strDlvryDt + "\";";
			}
			else if(strFormId=="AgentService")
			{
				String strCodeClientid = Request.Params["CODEID"];
				String strDescClientid = Request.Params["DESCID"];
				String strFormid = Request.Params["FORMID"];
				sScript += "  window.opener."+strFormid+"."+strCodeClientid+".value = \"" + strServiceCode + "\";";
				sScript += "  window.opener."+strFormid+"."+strDescClientid+".value = \"" + strServiceDesc + "\";";;
			}
			else if(strFormId=="ImportConsignments")
			{
				String strCodeClientid = Request.Params["CODEID"];
				String strFormid = Request.Params["FORMID"];
				sScript += "  window.opener."+strFormid+"."+strCodeClientid+".value = \"" + strServiceCode + "\";";
			}
			else if(strFormId=="BaseZoneRates")
			{
				String strCodeClientid = Request.Params["CODEID"];
				String strFormid = Request.Params["FORMID"];
				sScript += "  window.opener."+strFormid+"."+strCodeClientid+".value = \"" + strServiceCode + "\";";
			}
			else if(strFormId=="ImportESA_ServiceType")
			{
				String strCodeClientid = Request.Params["CODEID"];
				String strFormid = Request.Params["FORMID"];
				sScript += "  window.opener."+strFormid+"."+strCodeClientid+".value = \"" + strServiceCode + "\";";
			}
			else if(strFormName=="PickupRequestBooking")
			{
				
			}
			else
			{
				//Call the Calculate Delivery Date Method.
				String strDlvryDt = "";
				if(strPickUpDtTime.Length > 0)
				{
					DomesticShipmentMgrBAL domesticMgrBAL = new DomesticShipmentMgrBAL();
					strDlvryDt =  domesticMgrBAL.CalcDlvryDtTime(appID,enterpriseID,System.DateTime.ParseExact(strPickUpDtTime,"dd/MM/yyyy HH:mm",null),strServiceCode,strDestZipCode).ToString();
				}
			
				sScript += "  window.opener.DomesticShipment.txtShpSvcCode.value = '" + strServiceCode +"';";
				//sScript += "  window.opener.DomesticShipment.txtShpSvcDesc.value = '" + strServiceDesc +"';";
				sScript += "  window.opener.DomesticShipment.txtShipEstDlvryDt.value = \"" + strDlvryDt +"\";";
				

				//HC Return Task
				Zipcode zipCode = new Zipcode();
				zipCode.Populate(appID, enterpriseID, strDestZipCode);
				String strStateCode = zipCode.StateCode;

				if ((strDlvryDt.ToString().Trim() != "") && (strStateCode.Trim() != ""))
				{
					object tmpReHCDateTime = DomesticShipmentMgrDAL.calEstHCDateTime(System.DateTime.ParseExact(strDlvryDt,"dd/MM/yyyy HH:mm",null), strStateCode,
						strConsignmentNo.ToString().Trim(), strBookingNo.ToString().Trim(),appID, enterpriseID);

					if((tmpReHCDateTime != null) && 
						(!tmpReHCDateTime.GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						sScript += "  window.opener.DomesticShipment.txtEstHCDt.value = \"" + ((DateTime)tmpReHCDateTime).ToString("dd/MM/yyyy HH:mm") +"\";";
					}
				}
				//HC Return Task
			}

			sScript += " window.opener.callCodeBehindSvcCode(); window.close();";
			sScript += "</script>";
			Response.Write(sScript);
		}

		private void btnClose_Click(object sender, System.EventArgs e)
		{
			String sScript = "";
			sScript += "<script language=javascript>";
			sScript += "  window.close();";
			sScript += "</script>";
			Response.Write(sScript);
		}

		//HC Return Task
	
		public static DataSet GetCoreSysetmCodeNumVal(String strAppID, String strEnterpriseID,
			String strCodeID, int iCurRow, int iDSRowSize)
		{
			SessionDS sessionDS = null;			
			String strSQL = "";

			strSQL = "SELECT code_num_value "; 	
			strSQL += "FROM core_system_code ";
			strSQL += "WHERE applicationid = '" + strAppID + "' ";
			strSQL += "AND codeid = '" + strCodeID + "' ";
			strSQL += "ORDER BY sequence asc ";
	
			
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("ImportConsignmentsDAL","GetCoreSysetmCodeNumVal","GetCoreSysetmCodeNumVal","DbConnection object is null!!");
				return sessionDS.ds;
			}
			
			
			sessionDS = dbCon.ExecuteQuery(strSQL, iCurRow, iDSRowSize, "core_system_code_num_val");
			return  sessionDS.ds;
		}
		//HC Return Task
	}
}
