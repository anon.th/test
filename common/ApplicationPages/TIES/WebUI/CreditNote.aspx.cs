using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.common.util;
using com.ties;
using com.ties.classes;
using com.ties.DAL;
using com.ties.BAL;
using com.common.classes;
using Cambro.Web.DbCombo;
using System.Configuration;
using com.common.applicationpages;
using System.Resources;


namespace com.ties
{
	/// <summary>
	/// Summary description for CreditNote.
	/// </summary>
	public class CreditNote : com.common.applicationpages.BasePage
	{
		protected System.Web.UI.WebControls.Button btnQry;
		protected System.Web.UI.WebControls.Button btnExecQry;
		protected System.Web.UI.WebControls.Button btnInsert;
		protected System.Web.UI.WebControls.Button btnSave;
		protected System.Web.UI.WebControls.Button btnGoToFirstPage;
		protected System.Web.UI.WebControls.Button btnPreviousPage;
		protected System.Web.UI.WebControls.Button btnNextPage;
		protected System.Web.UI.WebControls.Button btnGoToLastPage;
		protected System.Web.UI.WebControls.Label lblMainTitle;
		protected System.Web.UI.WebControls.Label lblErrorMsg;
		protected System.Web.UI.HtmlControls.HtmlGenericControl mainPanel;
		protected System.Web.UI.HtmlControls.HtmlTable mainTable;
		protected System.Web.UI.WebControls.Label lblCreditNoteNo;
		protected System.Web.UI.WebControls.Label lblCreditNoteDate;
		protected com.common.util.msTextBox txtCreditNoteDate;
		protected System.Web.UI.WebControls.Label lblInvoiceNo;
		protected System.Web.UI.WebControls.Label lblInvoiceDate;
		protected com.common.util.msTextBox txtInvoiceDate;
		protected System.Web.UI.WebControls.Label lblTotalCreditAmt;
		protected System.Web.UI.WebControls.TextBox txtTotalCreditAmt;
		protected com.common.util.msTextBox txtPayerName;
		protected System.Web.UI.WebControls.Label lblRemarks1;
		protected System.Web.UI.WebControls.TextBox txtRemarks1;
		protected System.Web.UI.WebControls.TextBox txtRemarks;
		protected System.Web.UI.WebControls.DataGrid dgAssignedConsignment;
		protected System.Web.UI.WebControls.Button btnInsertCons;
		protected System.Web.UI.WebControls.Button btnOk;
		protected Cambro.Web.DbCombo.DbCombo DbInvoiceNo;
		protected Cambro.Web.DbCombo.DbCombo DbConsignmentNo;
		protected Cambro.Web.DbCombo.DbCombo DbChargeCode;
		protected Cambro.Web.DbCombo.DbCombo DbReasonCode;
		protected TextBox txtReasonCode;
		protected System.Web.UI.WebControls.Label lblNumRec;
		protected System.Web.UI.WebControls.TextBox txtGoToRec;
			
		SessionDS m_sdsCreditNote = null;
		SessionDS m_sdsCreditDetail=null;
		static private decimal m_iSetSize = 4;
		String appID=null;
		String enterpriseID=null;
		protected System.Web.UI.WebControls.ValidationSummary PageValidationSummary;		
		protected System.Web.UI.WebControls.RequiredFieldValidator validInvoiceNo;
		protected System.Web.UI.WebControls.Button btnNO;
		protected System.Web.UI.WebControls.Label lblMessage;
		protected System.Web.UI.HtmlControls.HtmlGenericControl msgPanel;
		protected System.Web.UI.WebControls.Label lblPayerName;
		protected System.Web.UI.WebControls.Label lblRemarks;
		protected System.Web.UI.WebControls.Button btnToSaveChanges;
		protected System.Web.UI.WebControls.Button btnNotToSave;
		protected System.Web.UI.WebControls.Label lblCustomerID;
		protected com.common.util.msTextBox txtCreateDate;
		protected System.Web.UI.WebControls.Label lblStatusUpdatedBy;
		protected System.Web.UI.WebControls.Label lblLastUpdateBy;
		protected com.common.util.msTextBox txtStatusUpdatedDate;
		protected com.common.util.msTextBox txtCreateUserID;
		protected com.common.util.msTextBox txtStatusUpdateUserID;
		protected Cambro.Web.DbCombo.DbCombo DbCustomerID;
		protected System.Web.UI.WebControls.Label lblStatus;
		protected System.Web.UI.WebControls.DropDownList ddlStatus;
		protected System.Web.UI.WebControls.RequiredFieldValidator validCustomerID;
		protected System.Web.UI.WebControls.TextBox txtDNtest;
		String userID=null;
		protected com.common.util.msTextBox txtCreditNoteNo;
		protected System.Web.UI.WebControls.Label lblCreateBy;
		Decimal totalCreditAmnt=0;
		protected ArrayList userRoleArray;
		protected RequiredFieldValidator reqDbChargeCode,reqDbConsignment,reqDbReasonCode=null;	
		protected bool bACCTAPPROVE,bACCTCANCEL,bACCT,bACCTMGR;
		protected System.Web.UI.WebControls.Button btnRetrieve;
		protected System.Web.UI.WebControls.Button btnPrint;
		protected System.Web.UI.WebControls.Button btnCancel;
		protected com.common.util.msTextBox txtLastUpdatedUserID;
		protected com.common.util.msTextBox txtLastUpdatedDate;
		protected System.Web.UI.HtmlControls.HtmlGenericControl Div2;
		private String strFormatCurrency;
		protected System.Web.UI.WebControls.Button btnGenerate;
		private int currency_decimal;
	
		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			DbInvoiceNo.RegistrationKey = System.Configuration.ConfigurationSettings.AppSettings["DbComboRegKey"];
			DbCustomerID.RegistrationKey = System.Configuration.ConfigurationSettings.AppSettings["DbComboRegKey"];
			//			DbChargeCode.RegistrationKey = System.Configuration.ConfigurationSettings.AppSettings["DbComboRegKey"];			
			//			DbConsignmentNo.RegistrationKey = System.Configuration.ConfigurationSettings.AppSettings["DbComboRegKey"];
			//			DbReasonCode.RegistrationKey = System.Configuration.ConfigurationSettings.AppSettings["DbComboRegKey"];

			//utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings,Page.Session);		
	
			DbInvoiceNo.TextLoading = Utility.GetLanguageText(ResourceType.UserMessage,"MSG_DBCOMBO_LOADING",utility.GetUserCulture());
			DbInvoiceNo.TextNoResults = Utility.GetLanguageText(ResourceType.UserMessage,"MSG_DBCOMBO_NOT_FOUND",utility.GetUserCulture());
			DbCustomerID.TextLoading = Utility.GetLanguageText(ResourceType.UserMessage,"MSG_DBCOMBO_LOADING",utility.GetUserCulture());
			DbCustomerID.TextNoResults = Utility.GetLanguageText(ResourceType.UserMessage,"MSG_DBCOMBO_NOT_FOUND",utility.GetUserCulture());
			this.btnSave.Attributes.Add("onclick","javascript:document.getElementById('"+ this.btnSave.ClientID + "').disabled=true;" + GetPostBackEventReference(this.btnSave));
			appID = utility.GetAppID();
			enterpriseID = utility.GetEnterpriseID();
			userID = utility.GetUserID();
			setRole();
			

			msgPanel.Visible=false;

			DataSet profileDS = SysDataManager1.GetEnterpriseProfile(appID, enterpriseID);

			if(profileDS.Tables[0].Rows.Count > 0)
			{
				currency_decimal = (int) profileDS.Tables[0].Rows[0]["currency_decimal"];
				ViewState["m_format"] = "{0:n" + currency_decimal.ToString() + "}";
				strFormatCurrency=ViewState["m_format"].ToString();
			}

			if (!Page.IsPostBack)
			{
				string scriptDisable = "this.style.display = 'none';";
				btnInsertCons.Attributes.Add("onclick",scriptDisable);
				btnCancel.Enabled=false;
				dgAssignedConsignment.PageSize = Convert.ToInt16(System.Configuration.ConfigurationSettings.AppSettings["RecordPerPage"]);
				ViewState["DNOperation"] = Operation.None;	
				ViewState["CreditDetailMode"] = ScreenMode.None;
				ViewState["CreditDetailOperation"] = Operation.None;	
				ViewState["NextOperation"] = "None";								
				load_ddlStatus();	
				ddlStatus.Items.Insert(0, new ListItem(Utility.GetLanguageText(ResourceType.UserMessage, "MSG_DDL_ALL", utility.GetUserCulture()),""));
				ResetScreenForQuery();

				this.btnRetrieve.Enabled = false;

				if(utility.GetUserCulture()=="th-TH")
				{
					setScreenLabel();
				}
				setMessage();
				bool accept=false;
				if(bACCT||bACCTAPPROVE||bACCTCANCEL||bACCTMGR)
				{
					accept=true;
				}
				if(!accept)
				{
					btnCancel.Enabled=false;
					btnInsert.Enabled=false;
					btnQry.Enabled=false;
					btnExecQry.Enabled=false;
					btnPrint.Enabled=false;
					btnSave.Enabled=false;
					btnNextPage.Enabled=false;
					btnPreviousPage.Enabled=false;
					btnGoToFirstPage.Enabled=false;
					btnGoToLastPage.Enabled=false;
				}

				if(Session["dtRetrieveCons"]!= null)
				{
					this.txtCreditNoteNo.Text = Session["Credit_no"].ToString();
					if(this.txtCreditNoteNo.Text.Trim()!="")
					{
						ExecQry_Click();
					}
					else
					{						
						Insert_Click();
						m_sdsCreditNote = (SessionDS) Session["CreditHeader"];
						Session["SESSION_DS1"] = m_sdsCreditNote;
						DisplayCurrentPage();
						btnInsertCons.Enabled = true;
						DbCustomerID.ClientOnSelectFunction = "";
						DbInvoiceNo.ClientOnSelectFunction = "";
						DbCustomerID.Enabled=false;
						DbInvoiceNo.Enabled=false;
						ddlStatus.Enabled=false;
						SessionDS m_sdsCRDetailTMP = (SessionDS)Session["dtRetrieveCons"];
						if(m_sdsCRDetailTMP.ds.Tables[0].Rows.Count>0)
						{
							btnSave.Enabled=true;
						}
						else
						{
							btnSave.Enabled=false;
						}
					}
						ViewState["IsTextChanged"] = true;
						m_sdsCreditDetail = (SessionDS)Session["dtRetrieveCons"];
						Session["SESSION_DS2"] = m_sdsCreditDetail;
						this.BindConsGrid();
						Session.Remove("dtRetrieveCons");
						return;					
				}
			}
			else
			{
				if(Session["SESSION_DS1"] != null)
				{
					m_sdsCreditNote = (SessionDS)Session["SESSION_DS1"];
				}
				if(Session["SESSION_DS2"] != null)
				{
					m_sdsCreditDetail = (SessionDS)Session["SESSION_DS2"];
				}			
			}
			
			addDbComboEventHandler();
			PageValidationSummary.HeaderText = Utility.GetLanguageText(ResourceType.UserMessage, "PLS_ENTER_MISS_FIELD", utility.GetUserCulture());
			getPageControls(Page);
			setMessage();

			if((int)ViewState["CreditDetailOperation"]==(int)Operation.Insert)
			{
				this.btnRetrieve.Enabled = false;
			}
			if(ddlStatus.SelectedValue == "C" || ddlStatus.SelectedValue == "A")
			{
				this.btnRetrieve.Enabled = false;				
			}

		}

		private void setRole()
		{
			try
			{
				User user = new User();
				user.UserID = userID;
				userRoleArray = com.common.RBAC.RBACManager.GetAllRoles(appID,enterpriseID,user);
				Role role;
				bACCTAPPROVE =false;
				bACCTCANCEL =false;
				bACCT =false;
				bACCTMGR =false;
				for(int i=0;i<userRoleArray.Count;i++)
				{
					role = (Role)userRoleArray[i];
					switch(role.RoleName.ToUpper().Trim())
					{
						case "ACCTAPPROVE" :
							bACCTAPPROVE =true;
							break;
						case "ACCTMGR" :
							bACCTMGR =true;
							break;
						case "ACCTCANCEL" :
							bACCTCANCEL = true;
							break;
						case "ACCT" :
							bACCT = true;
							break;
					}					
				}
			}
			catch
			{
			}
		}
		private void setMessage()
		{
			try
			{
				
				validCustomerID.ErrorMessage = Utility.GetLanguageText(ResourceType.UserMessage,"msg_RequiredField",utility.GetUserCulture()).Replace("[Label Field Name]",lblCustomerID.Text);
				validInvoiceNo.ErrorMessage = Utility.GetLanguageText(ResourceType.UserMessage,"msg_RequiredField",utility.GetUserCulture()).Replace("[Label Field Name]",lblInvoiceNo.Text);
				
			}
			catch
			{
			}
		}

		private void setScreenLabel()
		{
			try
			{								
				/*lblCreditNoteNo.Text = Utility.GetLanguageText(ResourceType.ScreenLabel,"LBLDNNO",utility.GetUserCulture());
				lblCreditNoteDate.Text = Utility.GetLanguageText(ResourceType.ScreenLabel,"LBLDNDATE",utility.GetUserCulture());
				lblCustomerID.Text = Utility.GetLanguageText(ResourceType.ScreenLabel,"LBLCUSTOMERID",utility.GetUserCulture());
				lblInvoiceNo.Text = Utility.GetLanguageText(ResourceType.ScreenLabel,"LBLINVOICENO",utility.GetUserCulture());
				lblStatus.Text = Utility.GetLanguageText(ResourceType.ScreenLabel,"LBLSTATUS",utility.GetUserCulture());
				lblRemarks.Text = Utility.GetLanguageText(ResourceType.ScreenLabel,"LBLREMARKS",utility.GetUserCulture());
				lblTotalCreditAmt.Text = Utility.GetLanguageText(ResourceType.ScreenLabel,"LBLTOTALCreditAMNT",utility.GetUserCulture());
				lblCreateBy.Text = Utility.GetLanguageText(ResourceType.ScreenLabel,"LBLCREATEDBY",utility.GetUserCulture());
				lblPayerName.Text = Utility.GetLanguageText(ResourceType.ScreenLabel,"LBLCUSTOMERNAME",utility.GetUserCulture());
				lblInvoiceDate.Text = Utility.GetLanguageText(ResourceType.ScreenLabel,"LBLINVDATE",utility.GetUserCulture());
				lblStatusUpdatedBy.Text = Utility.GetLanguageText(ResourceType.ScreenLabel,"LBLSTATUPDATEBY",utility.GetUserCulture());
				lblLastUpdateBy.Text = Utility.GetLanguageText(ResourceType.ScreenLabel,"LBLUPDATEBY",utility.GetUserCulture());*/
			}
			catch
			{
			}
		}
		private void addDbComboEventHandler()
		{
			if(dgAssignedConsignment.EditItemIndex == -1)
				return;

			if(dgAssignedConsignment.Items.Count==0)
				return;

			try
			{
				int iOperation = (int) ViewState["CreditDetailOperation"];
				this.DbConsignmentNo = (Cambro.Web.DbCombo.DbCombo)dgAssignedConsignment.Items[dgAssignedConsignment.EditItemIndex].FindControl("DbConsignmentNo");
				if(this.DbConsignmentNo != null)
					this.DbConsignmentNo.SelectedItemChanged += new System.EventHandler(this.DbConsignmentNo_OnSelectedIndexChanged);

				this.DbChargeCode = (Cambro.Web.DbCombo.DbCombo)dgAssignedConsignment.Items[dgAssignedConsignment.EditItemIndex].FindControl("DbChargeCode");
				if(this.DbChargeCode != null)
					this.DbChargeCode.SelectedItemChanged += new System.EventHandler(this.DbChargeCode_OnSelectedIndexChanged);
				
				this.DbReasonCode = (Cambro.Web.DbCombo.DbCombo)dgAssignedConsignment.Items[dgAssignedConsignment.EditItemIndex].FindControl("DbReasonCode");
				if(this.DbReasonCode != null)
					this.DbReasonCode.SelectedItemChanged += new System.EventHandler(this.DbReasonCode_OnSelectedIndexChanged);
				
				SetDbComboServerStates();
			}
			catch (Exception ex)
			{
				lblErrorMsg.Text=ex.Message.ToString();
			}
		}

		private void SetDbComboServerStates()
		{			
			Hashtable hash = new Hashtable();
			String strInvoiceNo=DbInvoiceNo.Value;
			if (strInvoiceNo=="")
				strInvoiceNo=DbInvoiceNo.Text;

			hash.Add("strInvoiceNo", strInvoiceNo);
			if(this.DbConsignmentNo == null)
				return;

			this.DbConsignmentNo.ServerState = hash;
			this.DbConsignmentNo.ServerStateSecretString = "kdfsjgnskajh[apq2-4uhg465654435879mnbfdm cxs;q4";
			this.DbChargeCode.ServerState = hash;
			this.DbChargeCode.ServerStateSecretString = "kdfsjgnskajh[apq2-4uhg465654435879mnbfdm cxs;q4";
			this.DbCustomerID.ServerState = hash;
			this.DbCustomerID.ServerStateSecretString = "kdfsjgnskajh[apq2-4uhg465654435879mnbfdm cxs;q4";
			this.DbInvoiceNo.ServerState = hash;
			this.DbInvoiceNo.ServerStateSecretString = "kdfsjgnskajh[apq2-4uhg465654435879mnbfdm cxs;q4";
			this.DbReasonCode.ServerState = hash;
			this.DbReasonCode.ServerStateSecretString = "kdfsjgnskajh[apq2-4uhg465654435879mnbfdm cxs;q4";

		}

		private void SetDbCustomerServerStates()
		{			
			Hashtable hash = new Hashtable();
			try
			{
				String strCustomerId=DbCustomerID.Value;

				if(strCustomerId=="")
					strCustomerId=DbCustomerID.Text;

				hash.Add("custID", strCustomerId);
				this.DbInvoiceNo.ServerState = hash;
				this.DbInvoiceNo.ServerStateSecretString = "kdfsjgnskajh[apq2-4uhg465654435879mnbfdm cxs;q4";
			}
			catch
			{
			}
			
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			base.OnInit(e);
			InitializeComponent();
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnToSaveChanges.Click += new System.EventHandler(this.btnToSaveChanges_Click);
			this.btnNotToSave.Click += new System.EventHandler(this.btnNotToSave_Click);
			this.btnQry.Click += new System.EventHandler(this.btnQry_Click);
			this.btnExecQry.Click += new System.EventHandler(this.btnExecQry_Click);
			this.btnInsert.Click += new System.EventHandler(this.btnInsert_Click);
			this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
			this.btnGoToFirstPage.Click += new System.EventHandler(this.btnGoToFirstPage_Click);
			this.btnPreviousPage.Click += new System.EventHandler(this.btnPreviousPage_Click);
			this.btnNextPage.Click += new System.EventHandler(this.btnNextPage_Click);
			this.btnGoToLastPage.Click += new System.EventHandler(this.btnGoToLastPage_Click);
			this.txtCreditNoteNo.TextChanged += new System.EventHandler(this.txtCreditNoteNo_TextChanged);
			this.DbCustomerID.SelectedItemChanged += new System.EventHandler(this.GetCustomerDetails);
			this.DbInvoiceNo.SelectedItemChanged += new System.EventHandler(this.GetInvoiceDetails);
			this.ddlStatus.SelectedIndexChanged += new System.EventHandler(this.ddlStatus_SelectedIndexChanged);
			this.txtRemarks.TextChanged += new System.EventHandler(this.txtRemarks_TextChanged);
			this.btnInsertCons.Click += new System.EventHandler(this.btnInsertCons_Click);
			this.dgAssignedConsignment.PageIndexChanged += new System.Web.UI.WebControls.DataGridPageChangedEventHandler(this.dgAssignedConsignment_PageIndexChanged);
			this.dgAssignedConsignment.SelectedIndexChanged += new System.EventHandler(this.dgAssignedConsignment_SelectedIndexChanged);
			this.btnRetrieve.Click += new System.EventHandler(this.btnRetrieve_Click);
			this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private void EnableNavigationButtons(bool bMoveFirst, bool bMoveNext,bool bEnableMovePrevious, bool bMoveLast)
		{
			btnGoToFirstPage.Enabled	= bMoveFirst;
			btnPreviousPage.Enabled		= bMoveNext;
			btnNextPage.Enabled			= bEnableMovePrevious;
			btnGoToLastPage.Enabled		= bMoveLast;
		}

		private void ResetScreenForQuery()
		{
			txtCreditNoteNo.ReadOnly=false;
			m_sdsCreditNote = CreditDebitMgrDAL.GetEmptyCreditNoteDS(1);
			Session["SESSION_DS1"] = m_sdsCreditNote;
			m_sdsCreditDetail = CreditDebitMgrDAL.GetEmptyCreditDetailDS(1);
			Session["SESSION_DS2"] = m_sdsCreditDetail;
			ViewState["currCreditNote"]="";
			ViewState["currInvoiceNo"]="";
			ViewState["currCustomerID"]="";
			DbCustomerID.Text="";
			DbCustomerID.Value="";
			DbInvoiceNo.Text="";
			DbInvoiceNo.Value="";
			txtPayerName.Text="";
			txtInvoiceDate.Text="";
			dgAssignedConsignment.CurrentPageIndex=0;
			BindConsGrid();
			ShowCurrConsPage();

			ViewState["DNMode"] = ScreenMode.Query;
			ViewState["DNOperation"] = Operation.None;
			ViewState["IsTextChanged"] = false;
			ViewState["currentPage"] = 0;
			btnExecQry.Enabled = true;			
			this.btnRetrieve.Enabled = false;		
			lblNumRec.Text = "";			
			lblErrorMsg.Text = "";						
			DisplayCurrentPage();
			btnInsertCons.Enabled=false;
			btnSave.Enabled=false;
			btnCancel.Enabled=false;
			btnPrint.Enabled=false;

			EnableNavigationButtons(false,false,false,false);
		}
		
		private void QueryMode()
		{
			DbCustomerID.ClientOnSelectFunction = "";
			DbInvoiceNo.ClientOnSelectFunction = "";
			load_ddlStatus();	
			ddlStatus.Items.Insert(0, new ListItem(Utility.GetLanguageText(ResourceType.UserMessage, "MSG_DDL_ALL", utility.GetUserCulture()),""));
			ResetScreenForQuery();

			Utility.EnableButton(ref btnInsert,ButtonType.Insert,true,m_moduleAccessRights);
			//Utility.EnableButton(ref btnSave,ButtonType.Save,true,m_moduleAccessRights);
			txtTotalCreditAmt.Text="";
			txtCreditNoteNo.Enabled=true; txtCreditNoteNo.ReadOnly=false;
			txtCreditNoteDate.Enabled=true; txtCreditNoteDate.ReadOnly=false;
			txtInvoiceDate.Enabled=true; //txtInvoiceDate.ReadOnly=false;
			DbInvoiceNo.Enabled=true;
			DbCustomerID.Enabled = true;			
			ddlStatus.Enabled=true;
			btnExecQry.Enabled = true;
			this.btnRetrieve.Enabled =false;
			//txtPayerID.Enabled=false; txtPayerID.ReadOnly=true;
			txtCreditNoteDate.ReadOnly = false;
			txtPayerName.Enabled=false; txtPayerName.ReadOnly=true;
			txtRemarks.Enabled=true;txtRemarks.ReadOnly=false;
		}
		private void load_ddlStatus()
		{
			try
			{
				Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings,HttpContext.Current.Session);
				String strAppID = util.GetAppID();
				String strEnterpriseID = util.GetEnterpriseID();
				String strCulture = util.GetUserCulture();
				ddlStatus.DataSource = CreditDebitMgrDAL.StatusDNQuery(strAppID,strEnterpriseID,"credit_debit_status",strCulture);
				ddlStatus.DataTextField = "DbComboText";
				ddlStatus.DataValueField = "DbComboValue";
				ddlStatus.DataBind();
				
			}
			catch
			{
			}
		}
		

		private void Qry_Click()
		{
			lblErrorMsg.Text="";			
			if ((bool)ViewState["IsTextChanged"]==true)
			{
				ViewState["DNOperation"]=Operation.None;
				lblMessage.Text=Utility.GetLanguageText(ResourceType.UserMessage,"con_lostdata",utility.GetUserCulture());
				msgPanel.Visible=true;				
				mainPanel.Visible=false;
				ViewState["NextOperation"] = "Query";
				return;
			}			
			QueryMode();
		}

		private void btnQry_Click(object sender, System.EventArgs e)
		{	
			Qry_Click();
		}

		private void ExecQry_Click()
		{
			FillCreditNote(0);
			if(m_sdsCreditNote.ds.Tables[0].Rows.Count>0)
			{
				m_sdsCreditNote.ds.Tables[0].Rows[0]["PayerId"] = DbCustomerID.Text.ToUpper().Trim();
				m_sdsCreditNote.ds.Tables[0].Rows[0]["invoice_no"] = DbInvoiceNo.Text.ToUpper().Trim();
			}
			ViewState["QUERY_DS"] = m_sdsCreditNote.ds;
			ViewState["DNMode"] = ScreenMode.ExecuteQuery;
			ViewState["DNOperation"] = Operation.None;
			ViewState["IsTextChanged"] = false;		
			ViewState["currentPage"] = 0;
			ViewState["currentSet"] = 0;
			ViewState["NextOperation"] = "ExecuteQuery";						
			GetDNRecSet();			
			//ShowCurrentDGPage();
			Utility.EnableButton(ref btnInsert,ButtonType.Insert,false,m_moduleAccessRights);
			

			if(m_sdsCreditNote.QueryResultMaxSize == 0)
			{
				ResetScreenForQuery();

				lblErrorMsg.Text=Utility.GetLanguageText(ResourceType.UserMessage,"MSG_CNNOTFOUND",utility.GetUserCulture());				
				return;
			}
			else
			{
				btnExecQry.Enabled = false;
				txtCreditNoteNo.Enabled=true;
				txtCreditNoteNo.ReadOnly=true;
				txtCreditNoteDate.ReadOnly=true;
				DbInvoiceNo.Enabled=false;
				DbCustomerID.Enabled=false;
				btnPrint.Enabled=true;
			}
			
			DisplayCurrentPage();
			ddlStatus.Items.Remove(new ListItem(Utility.GetLanguageText(ResourceType.UserMessage, "MSG_DDL_ALL", utility.GetUserCulture()),""));
			/*if(ddlStatus.SelectedValue.ToUpper().Trim()=="N")
			{
				dgAssignedConsignment.Columns[1].Visible=true;
				dgAssignedConsignment.Columns[0].Visible=true;
			}
			else
			{				
				if(ddlStatus.SelectedValue.ToUpper().Trim()=="C")
				{
					dgAssignedConsignment.Columns[1].Visible=false;
					dgAssignedConsignment.Columns[0].Visible=false;
				}
				else
				{
					if(bACCTAPPROVE||bACCTMGR)
					{
						dgAssignedConsignment.Columns[1].Visible=false;
						dgAssignedConsignment.Columns[0].Visible=true;
					}
					else
					{
						dgAssignedConsignment.Columns[1].Visible=false;
						dgAssignedConsignment.Columns[0].Visible=false;
					}
				}
			}*/
			if(m_sdsCreditNote != null && m_sdsCreditNote.DataSetRecSize > 0)
			{
				EnableNavigationButtons(false,false,true,true);				
				btnCancel.Enabled=true;
				ShowCurrConsPage();
				//ShowCurrentDGPage();
			}
			lblErrorMsg.Text = "";
			DisplayRecNum();
			if(ddlStatus.SelectedValue == "C" || ddlStatus.SelectedValue == "A")
			{
				this.btnRetrieve.Enabled = false;
			}
		}

		private void btnExecQry_Click(object sender, System.EventArgs e)
		{
			ExecQry_Click();
		}

		private void DisplayCurrentPage()
		{
			//txtTotalCreditAmt.Text="";
			DataRow drCurrent = m_sdsCreditNote.ds.Tables[0].Rows[System.Convert.ToInt32(ViewState["currentPage"])];			

			if(Utility.IsNotDBNull(drCurrent["Credit_no"])==true && drCurrent["Credit_no"].ToString() !="")
			{
				txtCreditNoteNo.Text=drCurrent["Credit_no"].ToString().ToUpper();
				ViewState["currCreditNote"]=txtCreditNoteNo.Text.Trim();
			}
			else
				txtCreditNoteNo.Text="";

			if(Utility.IsNotDBNull(drCurrent["credit_date"])==true && drCurrent["credit_date"].ToString() !="")
			{
				DateTime dtCreditNoteDate=System.Convert.ToDateTime(drCurrent["credit_date"]);
				txtCreditNoteDate.Text= dtCreditNoteDate.ToString("dd/MM/yyyy",null);
			}
			else
				txtCreditNoteDate.Text="";

			if(Utility.IsNotDBNull(drCurrent["Invoice_No"])==true && drCurrent["Invoice_No"].ToString() !="")
			{
				DbInvoiceNo.Text=(String) drCurrent["Invoice_No"].ToString();
				DbInvoiceNo.Value=(String) drCurrent["Invoice_No"].ToString();
				ViewState["currInvoiceNo"]=DbInvoiceNo.Text.Trim();
			}				
			else
			{
				DbInvoiceNo.Text="";
				DbInvoiceNo.Value="";
			}

			if(Utility.IsNotDBNull(drCurrent["PayerId"])==true && drCurrent["PayerId"].ToString() !="")
			{
				DbCustomerID.Text=(String) drCurrent["PayerId"].ToString();
				DbCustomerID.Value=(String) drCurrent["PayerId"].ToString();
				ViewState["currentCustomerID"]=DbCustomerID.Text.Trim();
			}				
			else
			{
				DbCustomerID.Text="";
				DbCustomerID.Value="";
			}

			if (Utility.IsNotDBNull(drCurrent["Invoice_Date"])==true && drCurrent["Invoice_Date"].ToString() !="")
			{	
				DateTime dtInvoiceDate=System.Convert.ToDateTime(drCurrent["Invoice_Date"]);
				txtInvoiceDate.Text=dtInvoiceDate.ToString("dd/MM/yyyy",null);			
			}
			else
				txtInvoiceDate.Text="";

			/*if (Utility.IsNotDBNull(drCurrent["PayerId"])==true && drCurrent["PayerId"].ToString() !="")
				txtPayerID.Text=(String)drCurrent["PayerId"];			
			else
				txtPayerID.Text="";*/

			if (Utility.IsNotDBNull(drCurrent["Payer_Name"])==true && drCurrent["Payer_Name"].ToString() !="")
				txtPayerName.Text=drCurrent["Payer_Name"].ToString().ToUpper();
			else
				txtPayerName.Text="";

			if (Utility.IsNotDBNull(drCurrent["Remark"])==true && drCurrent["Remark"].ToString() !="")
				txtRemarks.Text=(String)drCurrent["Remark"];
			else
			{
				txtRemarks.Text="";
			}

			if (Utility.IsNotDBNull(drCurrent["status"])==true && drCurrent["status"].ToString() !="")
			{
				load_ddlStatus();
				ddlStatus.SelectedValue = (String)drCurrent["status"];
				try
				{
					if(!drCurrent["status"].ToString().ToUpper().Equals("N"))
					{
						ListItem lItem = ddlStatus.Items.FindByValue("N");
						if(lItem!=null)
						{
							ddlStatus.Items.Remove(lItem);
						}					
					}
				}
				catch
				{
					load_ddlStatus();
				}
			}
			else
			{
				ddlStatus.SelectedIndex=0;
			}

			if (Utility.IsNotDBNull(drCurrent["Create_By"])==true && drCurrent["Create_By"].ToString() !="")
				txtCreateUserID.Text= drCurrent["Create_By"].ToString().ToUpper();
			else
			{
				txtCreateUserID.Text="";
			}

			if (Utility.IsNotDBNull(drCurrent["Create_Date"])==true && drCurrent["Create_Date"].ToString() !="")
			{
				DateTime dtCreate=System.Convert.ToDateTime(drCurrent["Create_Date"]);
				txtCreateDate.Text=dtCreate.ToString("dd/MM/yyyy HH:mm",null);
			}
			else
			{
				txtCreateDate.Text="";
			}

			if (Utility.IsNotDBNull(drCurrent["Update_By"])==true && drCurrent["Update_By"].ToString() !="")
				txtLastUpdatedUserID.Text= drCurrent["Update_By"].ToString().ToUpper();
			else
			{
				txtLastUpdatedUserID.Text="";
			}

			if (Utility.IsNotDBNull(drCurrent["Update_Date"])==true && drCurrent["Update_Date"].ToString() !="")
			{
				DateTime dtUpate=System.Convert.ToDateTime(drCurrent["Update_Date"]);
				txtLastUpdatedDate.Text=dtUpate.ToString("dd/MM/yyyy HH:mm",null);
			}
			else
			{
				txtLastUpdatedDate.Text="";
			}

			if (Utility.IsNotDBNull(drCurrent["Status_Update_By"])==true && drCurrent["Status_Update_By"].ToString() !="")
				txtStatusUpdateUserID.Text= drCurrent["Status_Update_By"].ToString().ToUpper();
			else
			{
				txtStatusUpdateUserID.Text="";
			}

			if (Utility.IsNotDBNull(drCurrent["Status_Update_Date"])==true && drCurrent["Status_Update_Date"].ToString() !="")
			{
				DateTime dtUpate=System.Convert.ToDateTime(drCurrent["Status_Update_Date"]);
				txtStatusUpdatedDate.Text=dtUpate.ToString("dd/MM/yyyy HH:mm",null);
			}
			else
			{
				txtStatusUpdatedDate.Text="";
			}

			if(totalCreditAmnt>0)
			{
				txtTotalCreditAmt.Text = String.Format(strFormatCurrency,totalCreditAmnt);
			}
			
			if (Utility.IsNotDBNull(drCurrent["status"])==true && drCurrent["status"].ToString() !="")
			{
				if(drCurrent["status"].ToString().ToUpper()=="C")
				{					
					ddlStatus.Enabled=false;
					btnSave.Enabled=false;
					btnInsertCons.Enabled=false;
					txtRemarks.ReadOnly = true;
					dgAssignedConsignment.Columns[1].Visible=false;
					dgAssignedConsignment.Columns[0].Visible=false;
				}
				else if(drCurrent["status"].ToString().ToUpper()=="A")
				{
					if(bACCTAPPROVE||bACCTMGR)
					{
						ddlStatus.Enabled=true;
						btnSave.Enabled=true;
						btnInsertCons.Enabled=false;
						txtRemarks.ReadOnly = true;
						dgAssignedConsignment.Columns[1].Visible=false;
						dgAssignedConsignment.Columns[0].Visible=false;
						/*dgAssignedConsignment.Columns[1].Visible=true;
						dgAssignedConsignment.Columns[0].Visible=true;						*/
					}
					else
					{
						ddlStatus.Enabled=false;
						btnSave.Enabled=false;
						btnInsertCons.Enabled=false;
						txtRemarks.ReadOnly = true;
						dgAssignedConsignment.Columns[1].Visible=false;
						dgAssignedConsignment.Columns[0].Visible=false;
					}

					if(ddlStatus.SelectedValue.ToUpper().Trim()=="A")
						this.btnSave.Enabled=false;		
				}
				else
				{
					ddlStatus.Enabled=true;
					btnSave.Enabled=true;
					btnInsertCons.Enabled=true;
					txtRemarks.ReadOnly = false;
					dgAssignedConsignment.Columns[1].Visible=true;
					dgAssignedConsignment.Columns[0].Visible=true;					
				}
			}			
			SetDbComboServerStates();
			int iOperation = (int)ViewState["DNOperation"];
			//ShowCurrConsPage();
		}

		private void FillCreditNote(int iCurrentRow)
		{
			DataRow drCurrent = m_sdsCreditNote.ds.Tables[0].Rows[iCurrentRow];
			if(txtCreditNoteNo.Text != null && txtCreditNoteNo.Text.Trim() != "")			
				drCurrent["Credit_no"] = txtCreditNoteNo.Text.Trim();			
			if(txtCreditNoteDate.Text != null && txtCreditNoteDate.Text.Trim() != "")
			{
				if(CheckDateTime_Format(txtCreditNoteDate.Text.Trim()))
				{
					drCurrent["credit_date"] = System.DateTime.ParseExact(txtCreditNoteDate.Text.Trim(),"dd/MM/yyyy",null);
				}
				
			}
			if(DbInvoiceNo.Value != null && DbInvoiceNo.Value.Trim() != "")
			{
				drCurrent["invoice_no"] = DbInvoiceNo.Value.Trim();
			}

			if(txtInvoiceDate.Text != null && txtInvoiceDate.Text.Trim() != "")
			{
				drCurrent["Invoice_Date"] = System.DateTime.ParseExact(txtInvoiceDate.Text.Trim(),"dd/MM/yyyy",null);
			}

			if(DbCustomerID.Value != null && DbCustomerID.Value.Trim() != "")
			{
				drCurrent["PayerId"] = DbCustomerID.Value.Trim();
			}

			if(txtPayerName.Text != null && txtPayerName.Text.Trim() != "")
			{
				drCurrent["Payer_name"] = txtPayerName.Text.Trim();
			}
			
			if(txtRemarks.Text != null && txtRemarks.Text.Trim() != "")
			{			
				drCurrent["remark"] = txtRemarks.Text.Trim();			
			}
			else
			{
				drCurrent["remark"]="";
			}
			
			if(txtCreateUserID != null && txtCreateUserID.Text.Trim() != "")
			{
				drCurrent["Create_By"] = txtCreateUserID.Text.Trim();
			}

			if(txtCreateDate != null && txtCreateDate.Text.Trim() != "")
			{
				drCurrent["Create_Date"] = System.DateTime.ParseExact(txtCreateDate.Text.Trim() ,"dd/MM/yyyy HH:mm",null);
			}

			if(userID != null && userID.Trim() != "")
			{
				drCurrent["Update_By"] = userID.Trim();
				String updateDate = DateTime.Now.ToString("dd/MM/yyyy HH:mm");
				drCurrent["Update_Date"] = System.DateTime.ParseExact(updateDate,"dd/MM/yyyy HH:mm",null);
			}
		

			if(ddlStatus.SelectedIndex != -1)
			{
				if(drCurrent["status"]!=null && drCurrent["status"].ToString().Trim()!="")
				{
					if(!ddlStatus.SelectedValue.Equals(drCurrent["status"].ToString()))
					{
						drCurrent["Status_Update_By"] = userID.Trim();
						String updateDate = DateTime.Now.ToString("dd/MM/yyyy HH:mm");
						drCurrent["Status_Update_Date"] = System.DateTime.ParseExact(updateDate,"dd/MM/yyyy HH:mm",null);
						if(ddlStatus.SelectedValue.ToString().Trim().ToUpper()!="C")
						{
							txtCreditNoteDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
							drCurrent["credit_date"] = System.DateTime.ParseExact(txtCreditNoteDate.Text.Trim(),"dd/MM/yyyy",null);
						}
					}
				}
				drCurrent["status"] = ddlStatus.SelectedValue;
			}
			
		}

		public static SessionDS GetEmptyCreditNote()
		{			

			DataTable dtCreditNote= new DataTable();
 
			dtCreditNote.Columns.Add(new DataColumn("credit_no", typeof(string)));
			dtCreditNote.Columns.Add(new DataColumn("credit_date", typeof(DateTime)));
			dtCreditNote.Columns.Add(new DataColumn("invoice_no", typeof(string)));			
			dtCreditNote.Columns.Add(new DataColumn("remark", typeof(string)));
	
			DataSet dsDN= new DataSet();
			dsDN.Tables.Add(dtCreditNote);

			SessionDS sessionDS = new SessionDS();
			sessionDS.ds = dsDN;
			sessionDS.DataSetRecSize = dsDN.Tables[0].Rows.Count;
			sessionDS.QueryResultMaxSize = sessionDS.DataSetRecSize;

			return  sessionDS;
	
		}
		
		private void InsertMode()
		{
			txtCreditNoteNo.ReadOnly=true;
			m_sdsCreditNote = CreditDebitMgrDAL.GetEmptyCreditNoteDS(1);
			Session["SESSION_DS1"] = m_sdsCreditNote;
			m_sdsCreditDetail = CreditDebitMgrDAL.GetEmptyCreditDetailDS(1);
			m_sdsCreditDetail.ds.Tables[0].Rows.Clear();
			Session["SESSION_DS2"] = m_sdsCreditDetail;
			
			ViewState["currCreditNote"]="";
			ViewState["currInvoiceNo"]="";
			ViewState["DNMode"] = ScreenMode.Insert;
			ViewState["DNOperation"] = Operation.Insert;
			ViewState["IsTextChanged"] = false;
			ViewState["currentPage"] = 0;
			DbCustomerID.Text="";
			DbCustomerID.Value="";
			DbInvoiceNo.Text="";
			DbInvoiceNo.Value="";
			txtPayerName.Text="";
			txtInvoiceDate.Text="";
			DbCustomerID.ClientOnSelectFunction = "CustomerOnSelect";
			DbInvoiceNo.ClientOnSelectFunction = "InvoiceOnSelect";
			load_ddlStatus();
			BindConsGrid();
			DisplayCurrentPage();
			dgAssignedConsignment.Columns[1].Visible=true;
			dgAssignedConsignment.Columns[0].Visible=true;
			String strDt = DateTime.Now.ToString("dd/MM/yyyy HH:mm");
			txtCreditNoteDate.Text=DateTime.Now.ToString("dd/MM/yyyy");

			this.btnRetrieve.Enabled = false;
			btnInsert.Enabled=false;
			DbInvoiceNo.Enabled=true;
			DbCustomerID.Enabled = true;
			ddlStatus.Enabled=false;	
			#region Edit by Sompote 2010-05-13
			//ddlStatus.SelectedIndex = 0;			
			ddlStatus.SelectedValue = "N";
			#endregion
			btnExecQry.Enabled = false;
			btnInsertCons.Enabled=false;
			Utility.EnableButton(ref btnSave,ButtonType.Save,true,m_moduleAccessRights);
			lblNumRec.Text = "";
			lblErrorMsg.Text = "";
			txtCreditNoteDate.ReadOnly=true;
			txtCreateUserID.Text = userID.ToUpper();			
			txtCreateDate.Text = strDt;
			txtTotalCreditAmt.Text = "0";
			btnSave.Enabled=false;
			btnPrint.Enabled=false;
		}

		private void Insert_Click()
		{
			if ((bool)ViewState["IsTextChanged"]==true && (int)ViewState["DNOperation"]==(int)Operation.Insert)
			{
				ViewState["DNOperation"]=Operation.Insert;
				lblMessage.Text=Utility.GetLanguageText(ResourceType.UserMessage,"con_lostdata",utility.GetUserCulture());
				msgPanel.Visible=true;				
				mainPanel.Visible=false;
				ViewState["NextOperation"]="Insert";
				return;
			}
			lblErrorMsg.Text="";
			InsertMode();
		}

		private void btnInsert_Click(object sender, System.EventArgs e)
		{
			Insert_Click();			
		}

		private bool SaveUpdateRecord()
		{
			bool isError = true;
			
			//FillCreditNote(0);
			//Credit Note Date Validation
			DateTime dtCreditdate=DateTime.Now;
			DateTime dtInvoicedate=DateTime.Now;
			DataSet dsInv =null;
			SessionDS dsCustomer=null;
			if(DbCustomerID.Text!=null&&DbCustomerID.Text.Trim()!="")
			{
				dsCustomer = CreditDebitMgrDAL.GetCustomerDetails(appID,enterpriseID,DbCustomerID.Text.Trim());
				if(dsCustomer.ds.Tables[0].Rows.Count>0)
				{
					DbCustomerID.Value=DbCustomerID.Text.Trim().ToUpper();
					txtPayerName.Text = dsCustomer.ds.Tables[0].Rows[0]["cust_name"].ToString().ToUpper();
				}
				else
				{
					lblErrorMsg.Text=Utility.GetLanguageText(ResourceType.UserMessage,"msg_RequiredField",utility.GetUserCulture()).Replace("[Label Field Name]",lblCustomerID.Text);
					return isError;
				}
			}
			else
			{
				lblErrorMsg.Text=Utility.GetLanguageText(ResourceType.UserMessage,"msg_RequiredField",utility.GetUserCulture()).Replace("[Label Field Name]",lblCustomerID.Text);
				return isError;
			}
			
			if(DbInvoiceNo.Text!=null&&DbInvoiceNo.Text.Trim()!="")
			{
				dsInv = CreditDebitMgrDAL.InvoiceDateQuery(appID,enterpriseID,DbCustomerID.Text.Trim(),DbInvoiceNo.Text.Trim().ToUpper());
				if(dsInv.Tables[0].Rows.Count>0)
				{
					DbInvoiceNo.Value=DbInvoiceNo.Text.Trim().ToUpper();
					txtInvoiceDate.Text = Convert.ToDateTime(dsInv.Tables[0].Rows[0]["invoice_date"].ToString()).ToString("dd/MM/yyyy");
					//txtPayerName.Text = dsCustomer.ds.Tables[0].Rows[0]["cust_name"].ToString().ToUpper();
				}
				else
				{
					lblErrorMsg.Text=Utility.GetLanguageText(ResourceType.UserMessage,"msg_RequiredField",utility.GetUserCulture()).Replace("[Label Field Name]",lblInvoiceNo.Text);
					return isError;
				}
			}
			else
			{
				lblErrorMsg.Text=Utility.GetLanguageText(ResourceType.UserMessage,"msg_RequiredField",utility.GetUserCulture()).Replace("[Label Field Name]",lblInvoiceNo.Text);
				return isError;
			}

			if(txtCreditNoteDate.Text != null && txtCreditNoteDate.Text.Trim() != "")
			{
				dtCreditdate = System.DateTime.ParseExact(txtCreditNoteDate.Text.Trim(),"dd/MM/yyyy",null);
			}
			if(txtInvoiceDate.Text != null && txtInvoiceDate.Text.Trim() != "")
			{
				dtInvoicedate = System.DateTime.ParseExact(txtInvoiceDate.Text.Trim(),"dd/MM/yyyy",null);
			}
			if (txtRemarks.Text.Length > 200)
			{
				lblErrorMsg.Text=Utility.GetLanguageText(ResourceType.UserMessage,"REMARKS_EXCEED_200CHAR",utility.GetUserCulture());	
				return isError;
			}
			if (dtCreditdate < dtInvoicedate)
			{
				lblErrorMsg.Text=Utility.GetLanguageText(ResourceType.UserMessage,"Credit_DT_LESS_INVOICE_DT",utility.GetUserCulture());
				return isError;
			}
			if (m_sdsCreditNote.ds.Tables[0].Rows.Count==0)
			{
				lblErrorMsg.Text=Utility.GetLanguageText(ResourceType.UserMessage,"PLS_ENTER_Credit_NOTE",utility.GetUserCulture());	
				return isError;
			}

			int iOperation = (int)ViewState["DNOperation"];
			switch(iOperation)
			{
				case (int)Operation.None:
				case (int)Operation.Update:
					try
					{
						//FillCreditNote(System.Convert.ToInt32(ViewState["currentPage"]));
						int currentPage = System.Convert.ToInt32(ViewState["currentPage"]);
						if(ddlStatus.SelectedValue.ToUpper() == "C")
						{
							int total=0;
							bool accept=false;
							if(bACCTCANCEL||bACCTMGR)
							{
								accept=true;								
							}

							if(m_sdsCreditNote.ds.Tables[0].Rows[System.Convert.ToInt32(ViewState["currentPage"])]["status"].ToString().Trim().ToUpper()=="A")
							{
								if(!bACCTMGR)
								{
									accept=false;								
								}
																	
							}

							if(accept)
							{
								
								total = 0;//CreditDebitMgrDAL.getCNCount(appID,enterpriseID,DbInvoiceNo.Text.Trim(),"A");
								if(total>0)
								{
									lblErrorMsg.Text=Utility.GetLanguageText(ResourceType.UserMessage,"msg_CancelDN",utility.GetUserCulture());	
								}
								else
								{
									FillCreditNote(currentPage);
						
									CreditDebitMgrDAL.ModifyCreditNoteDS(appID,enterpriseID,m_sdsCreditNote.ds, m_sdsCreditDetail.ds,currentPage);	
									m_sdsCreditDetail.ds.AcceptChanges();
									m_sdsCreditNote.ds.Tables[0].Rows[System.Convert.ToInt32(ViewState["currentPage"])].AcceptChanges();	
									ViewState["IsTextChanged"] = false;
									ChangeDNState();						
						
									DisplayCurrentPage();
									BindConsGrid();
									btnInsert.Enabled=true;
									btnInsertCons.Enabled=false;
									lblErrorMsg.Text=Utility.GetLanguageText(ResourceType.UserMessage,"MSG_SAVESUCCESS",utility.GetUserCulture());	
									Logger.LogDebugInfo("DEBUG MODE : CreditNote","btnSave_Click","INF004","save in modify mode..");
									Logger.LogDebugInfo("module2","DEBUG MODE :(module2 only) CreditNote","btnSave_Click","INF004","save in modify mode...");
								}
							}
							else
							{
								DisplayCurrentPage();
								BindConsGrid();
								lblErrorMsg.Text=Utility.GetLanguageText(ResourceType.UserMessage,"MSG_CANCELCNROLEERROR",utility.GetUserCulture());
							}
						}						
						else
						{
							if(ddlStatus.SelectedValue.ToUpper() == "A")
							{
								bool accept=false;
								if(bACCTAPPROVE||bACCTMGR)
								{
									accept=true;
								}							
								if(accept)
								{
									if((ddlStatus.SelectedValue.ToUpper() == "A"&&m_sdsCreditDetail.ds.Tables[0].Rows.Count>0))
									{
										FillCreditNote(currentPage);
						
										CreditDebitMgrDAL.ModifyCreditNoteDS(appID,enterpriseID,m_sdsCreditNote.ds, m_sdsCreditDetail.ds,currentPage);	
										m_sdsCreditDetail.ds.AcceptChanges();
										m_sdsCreditNote.ds.Tables[0].Rows[System.Convert.ToInt32(ViewState["currentPage"])].AcceptChanges();	
										ViewState["IsTextChanged"] = false;
										ChangeDNState();						
						
										DisplayCurrentPage();
										BindConsGrid();
										btnInsert.Enabled=true;
										btnInsertCons.Enabled=false;
										lblErrorMsg.Text=Utility.GetLanguageText(ResourceType.UserMessage,"MSG_SAVESUCCESS",utility.GetUserCulture());	
										Logger.LogDebugInfo("DEBUG MODE : CreditNote","btnSave_Click","INF004","save in modify mode..");
										Logger.LogDebugInfo("module2","DEBUG MODE :(module2 only) CreditNote","btnSave_Click","INF004","save in modify mode...");
									}
									else
									{
										DisplayCurrentPage();
										BindConsGrid();
										lblErrorMsg.Text=Utility.GetLanguageText(ResourceType.UserMessage,"MSG_APPROVECNERROR",utility.GetUserCulture());	
									}
								}
								else
								{
									DisplayCurrentPage();
									BindConsGrid();
									lblErrorMsg.Text=Utility.GetLanguageText(ResourceType.UserMessage,"MSG_APPROVECNROLEERROR",utility.GetUserCulture());
								}
							}
							else
							{
								FillCreditNote(currentPage);
						
								CreditDebitMgrDAL.ModifyCreditNoteDS(appID,enterpriseID,m_sdsCreditNote.ds, m_sdsCreditDetail.ds,currentPage);	
								m_sdsCreditDetail.ds.AcceptChanges();
								m_sdsCreditNote.ds.Tables[0].Rows[System.Convert.ToInt32(ViewState["currentPage"])].AcceptChanges();	
								ViewState["IsTextChanged"] = false;
								ChangeDNState();														
								DisplayCurrentPage();
								BindConsGrid();
								btnInsert.Enabled=true;
								btnInsertCons.Enabled=true;
								lblErrorMsg.Text=Utility.GetLanguageText(ResourceType.UserMessage,"MSG_SAVESUCCESS",utility.GetUserCulture());	
								Logger.LogDebugInfo("DEBUG MODE : CreditNote","btnSave_Click","INF004","save in modify mode..");
								Logger.LogDebugInfo("module2","DEBUG MODE :(module2 only) CreditNote","btnSave_Click","INF004","save in modify mode...");
							}
						}

						
					}
					catch(ApplicationException appException)
					{
						String strMsg = appException.Message;
						strMsg = appException.InnerException.Message;												
											
						if(strMsg.IndexOf("duplicate key") != -1 )
							lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"CNSGMNT_DUPLICATE",utility.GetUserCulture());	
						else if (strMsg.IndexOf("conflict") != -1 ) 
							lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"CNSGMNT_DUPLICATE",utility.GetUserCulture());	
						else
						{
							//lblErrorMsg.Text = strMsg;
						}
						
						Logger.LogTraceError("TRACE MODE : Credit Note","btnSave_Click","ERR004","Error updating record :--- "+strMsg);
						Logger.LogTraceError("module2","TRACE MODE : (module2 only)Credit Note","btnSave_Click","ERR004","Error updating record : "+strMsg);
						return isError;						
					}					
					btnCancel.Enabled=true;
					DbInvoiceNo.Enabled=false;//after saving the Record, INVOICE must NOT be changed
					DbCustomerID.Enabled = false;					
					ViewState["DNOperation"]=Operation.None;
					#region Add by Sompote 2010-05-25
					ddlStatus.Enabled=false;
					#endregion					
					isError=false;					
					break;

				case (int)Operation.Insert:
					FillCreditNote(0);
					DataSet dsToInsert = m_sdsCreditNote.ds.GetChanges();
					m_sdsCreditNote.ds.AcceptChanges();
					try
					{
						
						String strCreditNote_No=null;
						CreditDebitMgrDAL.AddCreditNoteDS(appID,enterpriseID,dsToInsert, ref strCreditNote_No,m_sdsCreditDetail.ds);
						if (strCreditNote_No !=null && strCreditNote_No !="")
						{
							txtCreditNoteNo.Text=strCreditNote_No;
							DataRow dr=m_sdsCreditNote.ds.Tables[0].Rows[System.Convert.ToInt32(ViewState["currentPage"])];//dsToInsert.Tables[0].Rows[0];
							dr["Credit_no"]=strCreditNote_No;
						}

						ViewState["IsTextChanged"] = false;
						ChangeDNState();
						m_sdsCreditNote.ds.Tables[0].Rows[System.Convert.ToInt32(ViewState["currentPage"])].AcceptChanges();
						m_sdsCreditDetail.ds.AcceptChanges();
						DisplayCurrentPage();
						BindConsGrid();
						btnInsertCons.Enabled=true;
						btnInsert.Enabled=true;
						lblErrorMsg.Text=Utility.GetLanguageText(ResourceType.UserMessage,"MSG_SAVESUCCESS",utility.GetUserCulture());	
						Logger.LogDebugInfo("CreditNote","btnSave_Click","INF004","save in Insert mode");
					}
					catch(ApplicationException appException)
					{
						String strMsg = appException.Message;
						strMsg = appException.InnerException.Message;						
						
						if(strMsg.IndexOf("duplicate key") != -1 )
							lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"CNSGMNT_DUPLICATE",utility.GetUserCulture());	
						else if (strMsg.IndexOf("conflict") != -1 ) 
							lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"CNSGMNT_DUPLICATE",utility.GetUserCulture());	
						else
						{
							//lblErrorMsg.Text = strMsg;
						}
						
						Logger.LogTraceError("TRACE MODE : Credit Note","btnSave_Click","ERR004","Error inserting record :--- "+strMsg);
						Logger.LogTraceError("module2","TRACE MODE : (module2 only)Credit Note","btnSave_Click","ERR004","Error inserting record : "+strMsg);

						return isError;
					}					
					btnCancel.Enabled=true;
					DbInvoiceNo.Enabled=false;//after saving the Record, INVOICE must NOT be changed
					DbCustomerID.Enabled = false;
					ddlStatus.Enabled=false;
					ViewState["DNOperation"]=Operation.None;
					isError=false;					
					break;
			}

			return isError;

		}

		private void btnSave_Click(object sender, System.EventArgs e)
		{
			//Save the record and return the status
			lblErrorMsg.Text="";
			if(m_sdsCreditNote.ds.Tables[0].Rows[System.Convert.ToInt32(ViewState["currentPage"])]["status"].ToString().Trim().ToUpper()=="A"&&ddlStatus.SelectedValue.ToUpper().Trim()=="A")
				return;
			if(m_sdsCreditNote.ds.Tables[0].Rows[System.Convert.ToInt32(ViewState["currentPage"])]["status"].ToString().Trim().ToUpper()=="A"&&ddlStatus.SelectedValue.ToUpper().Trim()=="C")
			{
				bool alert=false;
				DateTime nowDate = DateTime.Now;
				DateTime statDate = (DateTime)m_sdsCreditNote.ds.Tables[0].Rows[System.Convert.ToInt32(ViewState["currentPage"])]["Status_Update_Date"];
//				int intDDiff = nowDate.Day-statDate.Day;
				if(nowDate.Month>statDate.Month)
				{
//					if(intDDiff>=0)
//					{
						alert=true;
//					}
				}
				else
				{
					if(nowDate.Year>statDate.Year)
					{
//						if(intDDiff>=0)
//						{
							alert=true;
//						}
					}
				}
				if(alert)
				{
					ViewState["NextOperation"]="Save";
					ShowCancelConfirm();
					BindConsGrid();
					return;
				}
			}
			bool isError = SaveUpdateRecord();
			if (isError==true)
			{
				return;
			}	
			btnSave.Enabled=false;
			ViewState["CreditDetailOperation"] = Operation.Saved;
			btnPrint.Enabled=true;
			
			#region Edit by Sompote 2010-05-25			
			/*			 
			if(this.ddlStatus.SelectedValue == "C" || this.ddlStatus.SelectedValue == "A")
			{
				this.btnInsertCons.Enabled = false;
			}
			*/
			this.btnInsertCons.Enabled = false;
			#endregion
		}	
	
		private void ShowCancelConfirm()
		{
			try
			{
				msgPanel.Visible=true;
				mainPanel.Visible=false;
				lblMessage.Text=Utility.GetLanguageText(ResourceType.UserMessage,"MSG_CNCANCEL_ACCOUNTING_PRIOR",utility.GetUserCulture());
			}
			catch
			{
			}
		}

		private void btnCancel_Click(object sender, System.EventArgs e)
		{
			String strInvoiceNo=(String)ViewState["currInvoiceNo"];
			String strCreditNo=(String)ViewState["currCreditNote"];
			if (strInvoiceNo=="" || strCreditNo == "")
			{
				lblErrorMsg.Text=Utility.GetLanguageText(ResourceType.UserMessage,"Credit_INVOICE_REQ",utility.GetUserCulture());
				return;
			}
			CreditDebitMgrDAL.DeleteCreditNoteDS(appID,enterpriseID,strInvoiceNo, strCreditNo);
			lblErrorMsg.Text=Utility.GetLanguageText(ResourceType.UserMessage,"DELETED_SUCCESSFULLY",utility.GetUserCulture());	
			
			//Disable All Controls, Clear the DataGrid immediately after deleting
			txtCreditNoteNo.Enabled=false;
			txtCreditNoteDate.Enabled=false;
			DbInvoiceNo.Enabled=false;
			DbCustomerID.Enabled = false;
			ddlStatus.Enabled=false;
			txtInvoiceDate.Enabled=false;
			txtRemarks.Enabled=false;
			txtTotalCreditAmt.Enabled=false;
			//txtPayerID.Enabled=false;
			txtPayerName.Enabled=false;
			btnInsertCons.Enabled=false;
			Utility.EnableButton(ref btnSave,ButtonType.Save,false,m_moduleAccessRights);
			//Clear the dgAssignedConsignment DataGrid
			m_sdsCreditDetail.ds.Tables[0].Rows.Clear();
			txtTotalCreditAmt.Text="";
			BindConsGrid();

		}

		private void ChangeDNState()
		{
			if((int)ViewState["DNMode"] == (int) ScreenMode.Insert && (int)ViewState["DNOperation"] == (int)Operation.None)
			{
				ViewState["DNOperation"] = Operation.Insert;
			}
			else if((int)ViewState["DNMode"] == (int) ScreenMode.Insert && (int)ViewState["DNOperation"] == (int)Operation.Insert)
			{
				ViewState["DNOperation"] = Operation.Saved;
			}
			else if((int)ViewState["DNMode"] == (int) ScreenMode.Insert && (int)ViewState["DNOperation"] == (int)Operation.Saved)
			{
				ViewState["DNOperation"] = Operation.Update;
			}
			else if((int)ViewState["DNMode"] == (int) ScreenMode.Insert && (int)ViewState["DNOperation"] == (int)Operation.Update)
			{
				ViewState["DNOperation"] = Operation.Saved;
			}
			else if((int)ViewState["DNMode"] == (int) ScreenMode.ExecuteQuery && (int)ViewState["DNOperation"] == (int)Operation.None)
			{
				ViewState["DNOperation"] = Operation.Update;
			}
			else if((int)ViewState["DNMode"] == (int) ScreenMode.ExecuteQuery && (int)ViewState["DNOperation"] == (int)Operation.Update)
			{
				ViewState["DNOperation"] = Operation.None;
			}
			
		}

		private void DisplayRecNum()
		{
			int iCurrentRec = (int)((System.Convert.ToDecimal(ViewState["currentPage"]) + 1) + (System.Convert.ToDecimal(ViewState["currentSet"]) * m_iSetSize)) ;
			if(iCurrentRec<=1)
			{
				EnableNavigationButtons(false,false,true,true);
			}
			else if(iCurrentRec<m_sdsCreditNote.QueryResultMaxSize)
			{
				EnableNavigationButtons(true,true,true,true);
			}
			else if(iCurrentRec>=m_sdsCreditNote.QueryResultMaxSize)
			{
				EnableNavigationButtons(true,true,false,false);
			}
			if(m_sdsCreditNote.QueryResultMaxSize==1)
			{
				EnableNavigationButtons(false,false,false,false);
			}
			String tmpTxt = Utility.GetLanguageText(ResourceType.UserMessage,"MSG_CNDN_RECORD_NUM",utility.GetUserCulture());
			tmpTxt = tmpTxt.Replace("[INDEX]",iCurrentRec.ToString());
			tmpTxt = tmpTxt.Replace("[MAX]",m_sdsCreditNote.QueryResultMaxSize.ToString());
			lblNumRec.Text = tmpTxt;//""+ iCurrentRec + " of " + m_sdsCreditNote.QueryResultMaxSize + " record(s)";

		}

		private void GetDNRecSet()
		{
			decimal iStartIndex = System.Convert.ToDecimal(ViewState["currentSet"]) * m_iSetSize;
			DataSet dsQuery = (DataSet)ViewState["QUERY_DS"];
			if(iStartIndex>=0)
			{
				m_sdsCreditNote = CreditDebitMgrDAL.GetCreditNoteDS(appID,enterpriseID,System.Convert.ToInt32(iStartIndex),System.Convert.ToInt32(m_iSetSize),dsQuery);
			}
			else
			{
				m_sdsCreditNote = CreditDebitMgrDAL.GetCreditNoteDS(appID,enterpriseID,System.Convert.ToInt32(0),System.Convert.ToInt32(ViewState["remain"]),dsQuery);
				ViewState["remain"]=0;
				ViewState["currentSet"]=0;
			}
			Session["SESSION_DS1"] = m_sdsCreditNote;
			decimal pgCnt = (m_sdsCreditNote.QueryResultMaxSize - 1)/m_iSetSize;
			
			if(pgCnt < System.Convert.ToDecimal(ViewState["currentSet"]))
			{
				ViewState["currentSet"] = pgCnt;
			}			
		}


		private void FirstPage()
		{
			ViewState["currentSet"] = 0;	
			GetDNRecSet();
			ViewState["currentPage"] = 0;
			DisplayCurrentPage();
			txtCreditNoteDate.ReadOnly=true;
			ShowCurrConsPage();
			DisplayRecNum();			
			EnableNavigationButtons(false,false,true,true);
			ViewState["DNOperation"]=Operation.None;
		}

		private void LastPage()
		{
			ViewState["currentSet"] = (m_sdsCreditNote.QueryResultMaxSize - 1)/m_iSetSize;	
			GetDNRecSet();
			ViewState["currentPage"] = m_sdsCreditNote.ds.Tables[0].Rows.Count-1;
			DisplayCurrentPage();
			txtCreditNoteDate.ReadOnly=true;
			ShowCurrConsPage();
			DisplayRecNum();
			EnableNavigationButtons(true,true,false,false);		
			ViewState["DNOperation"]=Operation.None;
		}

		private void PreviousPage()
		{
			decimal iCurrentPage = System.Convert.ToDecimal(ViewState["currentPage"]);
			
			if(System.Convert.ToDecimal(ViewState["currentPage"])  > 0 )
			{
				ViewState["currentPage"] = System.Convert.ToDecimal(ViewState["currentPage"]) - 1;
				DisplayCurrentPage();	
				txtCreditNoteDate.ReadOnly=true;		
				/*if( (System.Convert.ToDecimal(ViewState["currentSet"]) - 1) < 0)
				{
					EnableNavigationButtons(false,false,true,true);
				//	return;
				}*/
				//ddlStatus.Enabled=true;
			}
			else
			{
				/*if( (System.Convert.ToDecimal(ViewState["currentSet"]) - 1) < 0)
				{
					EnableNavigationButtons(false,false,true,true);
					return;
				}*/				
				if(m_sdsCreditNote.QueryResultMaxSize != m_iSetSize)
				{
					if((System.Convert.ToDecimal(ViewState["currentSet"])- 1)>0)
					{
						ViewState["currentSet"] = System.Convert.ToDecimal(ViewState["currentSet"])- 1;	
					}
					else
					{
						int iCurrentRec = (int)((System.Convert.ToDecimal(ViewState["currentPage"]) + 1) + (System.Convert.ToDecimal(ViewState["currentSet"]) * m_iSetSize)) ;
						iCurrentRec--;
						ViewState["remain"]=iCurrentRec;
						ViewState["currentSet"] = System.Convert.ToDecimal(ViewState["currentSet"])- 1;	
						
					}										
					GetDNRecSet();
					ViewState["currentPage"] = m_sdsCreditNote.DataSetRecSize - 1;
				}
				else
				{
					ViewState["currentSet"] = 0;	
					GetDNRecSet();
					ViewState["currentPage"] = m_sdsCreditNote.QueryResultMaxSize - 2;	
				}
				
				DisplayCurrentPage();				
			}			
			ShowCurrConsPage();
			DisplayRecNum();
			
			//EnableNavigationButtons(true,true,true,true);
			ViewState["DNOperation"]=Operation.None;
		}

		private void NextPage()
		{
			decimal iCurrentPage = System.Convert.ToDecimal(ViewState["currentPage"]);
			if(System.Convert.ToDecimal(ViewState["currentPage"])  < (m_sdsCreditNote.DataSetRecSize - 1) )
			{
				/*decimal iTotalRec = (System.Convert.ToDecimal(ViewState["currentSet"]) * m_iSetSize) + m_sdsCreditNote.DataSetRecSize;
				if( iTotalRec ==  m_sdsCreditNote.QueryResultMaxSize)
				{
					EnableNavigationButtons(true,true,false,false);
					//return;
				}
				else
				{
					EnableNavigationButtons(true,true,true,true);
				}*/
				ViewState["currentPage"] = System.Convert.ToDecimal(ViewState["currentPage"]) + 1;
				DisplayCurrentPage();				
				txtCreditNoteDate.ReadOnly=true;				
				//ddlStatus.Enabled=true;
			}
			else
			{
				decimal iTotalRec = (System.Convert.ToDecimal(ViewState["currentSet"]) * m_iSetSize) + m_sdsCreditNote.DataSetRecSize;
				if( iTotalRec ==  m_sdsCreditNote.QueryResultMaxSize)
				{
					EnableNavigationButtons(true,true,false,false);
					return;
				}
				
				ViewState["currentSet"] = System.Convert.ToDecimal(ViewState["currentSet"]) + 1;	
				GetDNRecSet();
				ViewState["currentPage"] = 0;
				DisplayCurrentPage();								
			}
			DisplayRecNum();
			ShowCurrConsPage();			
			/*if(System.Convert.ToDecimal(ViewState["currentPage"])  == (m_sdsCreditNote.QueryResultMaxSize - 1) )
			{
				EnableNavigationButtons(true,true,false,false);
			}
			else
			{
				EnableNavigationButtons(true,true,true,true);	
			}*/
			ViewState["DNOperation"]=Operation.None;
		}

		private void MoveFirstDS()
		{
			if ((bool)ViewState["IsTextChanged"]==true)
			{
				ViewState["DNOperation"]=Operation.None;
				lblMessage.Text=Utility.GetLanguageText(ResourceType.UserMessage,"con_lostdata",utility.GetUserCulture());
				msgPanel.Visible=true;				
				mainPanel.Visible=false;
				ViewState["NextOperation"] = "MoveFirst";
				return;
			}
			FirstPage();
		}

		private void btnGoToFirstPage_Click(object sender, System.EventArgs e)
		{
			lblErrorMsg.Text="";
			txtTotalCreditAmt.Text = "";
			dgAssignedConsignment.CurrentPageIndex=0;
			MoveFirstDS();
			if(ddlStatus.SelectedValue == "C" || ddlStatus.SelectedValue == "A")
			{
				this.btnRetrieve.Enabled = false;
			}
		}
				
		private void MovePreviousDS()
		{
			if ((bool)ViewState["IsTextChanged"]==true)
			{
				ViewState["DNOperation"]=Operation.None;
				lblMessage.Text=Utility.GetLanguageText(ResourceType.UserMessage,"con_lostdata",utility.GetUserCulture());
				msgPanel.Visible=true;				
				mainPanel.Visible=false;
				ViewState["NextOperation"] = "MovePrevious";
				return;
			}
			PreviousPage();	
		}

		private void btnPreviousPage_Click(object sender, System.EventArgs e)
		{
			lblErrorMsg.Text="";
			txtTotalCreditAmt.Text = "";
			dgAssignedConsignment.CurrentPageIndex=0;
			MovePreviousDS();	
			if(ddlStatus.SelectedValue == "C"||ddlStatus.SelectedValue == "A")
			{
				this.btnRetrieve.Enabled = false;
			}
		}

		private void MoveNextDS()
		{
			if ((bool)ViewState["IsTextChanged"]==true)
			{
				ViewState["DNOperation"]=Operation.None;
				lblMessage.Text=Utility.GetLanguageText(ResourceType.UserMessage,"con_lostdata",utility.GetUserCulture());
				msgPanel.Visible=true;				
				mainPanel.Visible=false;
				ViewState["NextOperation"] = "MoveNext";
				return;
			}
			NextPage();	
		}

		private void btnNextPage_Click(object sender, System.EventArgs e)
		{
			lblErrorMsg.Text="";
			txtTotalCreditAmt.Text = "";
			dgAssignedConsignment.CurrentPageIndex=0;
			MoveNextDS();	
			if(ddlStatus.SelectedValue == "C"||ddlStatus.SelectedValue == "A")
			{
				this.btnRetrieve.Enabled = false;
			}
		}

		private void MoveLastDS()
		{
			if ((bool)ViewState["IsTextChanged"]==true)
			{
				ViewState["DNOperation"]=Operation.None;
				lblMessage.Text=Utility.GetLanguageText(ResourceType.UserMessage,"con_lostdata",utility.GetUserCulture());
				msgPanel.Visible=true;				
				mainPanel.Visible=false;
				ViewState["NextOperation"] = "MoveLast";
				return;
			}
			LastPage();	

		}

		private void btnGoToLastPage_Click(object sender, System.EventArgs e)
		{
			lblErrorMsg.Text="";
			txtTotalCreditAmt.Text = "";
			dgAssignedConsignment.CurrentPageIndex=0;
			MoveLastDS();
			if(ddlStatus.SelectedValue == "C"||ddlStatus.SelectedValue == "A")
			{
				this.btnRetrieve.Enabled = false;
			}
		}
	
		[Cambro.Web.DbCombo.ResultsMethodAttribute(true)]
		public static object DbInvoiceSelect(Cambro.Web.DbCombo.ServerMethodArgs args)
		{
			Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings,HttpContext.Current.Session);
			String strAppID = util.GetAppID();
			String strEnterpriseID = util.GetEnterpriseID();
			String customerID ="";

			if(args.ServerState != null && args.ServerState.Authenticate("kdfsjgnskajh[apq2-4uhg465654435879mnbfdm cxs;q4"))
			{
				if(args.ServerState["custID"] != null && args.ServerState["custID"].ToString().Length > 0)
				{
					customerID = args.ServerState["custID"].ToString();
				}				
			}
			DataSet dataset = DbComboDAL.InvoiceQuery(strAppID,strEnterpriseID,customerID,args);	
			return dataset;
		}

		[Cambro.Web.DbCombo.ResultsMethodAttribute(true)]
		public static object DbCustomerSelect(Cambro.Web.DbCombo.ServerMethodArgs args)
		{
			Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings,HttpContext.Current.Session);
			String strAppID = util.GetAppID();
			String strEnterpriseID = util.GetEnterpriseID();
			DataSet dataset = DbComboDAL.CustomerIDQuery(strAppID,strEnterpriseID,args);
			return dataset;
		}


		[Cambro.Web.DbCombo.ResultsMethodAttribute(true)]
		public static object DbConsignmentNoSelect(Cambro.Web.DbCombo.ServerMethodArgs args)
		{			
			Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings,HttpContext.Current.Session);
			String strAppID = util.GetAppID();
			String strEnterpriseID = util.GetEnterpriseID();			
			String strInvoiceNo ="";

			if(args.ServerState != null && args.ServerState.Authenticate("kdfsjgnskajh[apq2-4uhg465654435879mnbfdm cxs;q4"))
			{
				if(args.ServerState["strInvoiceNo"] != null && args.ServerState["strInvoiceNo"].ToString().Length > 0)
				{
					strInvoiceNo = args.ServerState["strInvoiceNo"].ToString();
				}				
			}
			DataSet dataset = DbComboDAL.ConsignmentQuery(strAppID,strEnterpriseID,strInvoiceNo,args);	
			return dataset;
		}		

		[Cambro.Web.DbCombo.ResultsMethodAttribute(true)]
		public static object DbChargeCodeSelect(Cambro.Web.DbCombo.ServerMethodArgs args)
		{
			Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings,HttpContext.Current.Session);
			String strAppID = util.GetAppID();
			String strEnterpriseID = util.GetEnterpriseID();
			String strCulture = "en-US";//util.GetUserCulture();
			DataSet dataset = DbComboDAL.ChargeReasonCodeQuery(strAppID,strEnterpriseID,"cn_chargecode",strCulture,args);
			return dataset;
		}

		[Cambro.Web.DbCombo.ResultsMethodAttribute(true)]
		public static object DbReasonCodeSelect(Cambro.Web.DbCombo.ServerMethodArgs args)
		{
			Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings,HttpContext.Current.Session);
			String strAppID = util.GetAppID();
			String strEnterpriseID = util.GetEnterpriseID();
			String strCulture = "en-US";//util.GetUserCulture();
			DataSet dataset = DbComboDAL.ChargeReasonCodeQuery(strAppID,strEnterpriseID,"cn_reasoncode",strCulture,args);
			return dataset;
		}
		protected void DbConsignmentNo_OnSelectedIndexChanged(object sender, System.EventArgs e)
		{
			String strInvoiceNo = (String)ViewState["currInvoiceNo"];
			int iOperation = (int) ViewState["CreditDetailOperation"];
			int currentIndex = (dgAssignedConsignment.EditItemIndex+(dgAssignedConsignment.CurrentPageIndex*dgAssignedConsignment.PageSize));
			if(iOperation == (int)Operation.Update)
				return;

			String strConsignmentNo = this.DbConsignmentNo.Value;
			
			if(dgAssignedConsignment.EditItemIndex == -1)
			{
				return;
			}
			DataRow drCurrent = m_sdsCreditDetail.ds.Tables[0].Rows[currentIndex];//m_sdsCreditDetail.ds.Tables[0].Rows[dgAssignedConsignment.EditItemIndex];
			drCurrent["Consignment_No"] = strConsignmentNo;		
			ViewState["CurrConsignmnetNo"]=strConsignmentNo;	
			
			String strInvNo = DbInvoiceNo.Text.Trim().ToUpper();
			String strChargeCode = this.DbChargeCode.Text;
			if(strChargeCode!=null&&strChargeCode.Trim()!="")
			{
				Decimal dInvoiceAmt = CreditDebitMgrDAL.GetCalcuratedInvoiceAmt(appID,enterpriseID,strConsignmentNo,strInvNo,strChargeCode);
				/*if(strInvoiceAmt==null)
				{
					strInvoiceAmt="0";
				}
				else
				{
					if(strInvoiceAmt.Trim()=="")
					{
						strInvoiceAmt="0";
					}
				}*/
				Label lblInvoiceAmt = (Label)dgAssignedConsignment.Items[dgAssignedConsignment.EditItemIndex].FindControl("lblInvoiceAmt");			
				if (lblInvoiceAmt!= null)					
					lblInvoiceAmt.Text=dInvoiceAmt.ToString();
				TextBox txtInvoiceAmt = (TextBox)dgAssignedConsignment.Items[dgAssignedConsignment.EditItemIndex].FindControl("txtInvoiceAmt");			
				if (txtInvoiceAmt!= null)					
					txtInvoiceAmt.Text=dInvoiceAmt.ToString();

//				if(strInvoiceAmt!=null&&strInvoiceAmt.Trim()!="")
//				{
					drCurrent["Invoice_Amt"] = dInvoiceAmt.ToString();
//				}
			}
			BindConsGrid();
			setDgErrorMsg();
			
		}
		protected void DbChargeCode_OnSelectedIndexChanged(object sender, System.EventArgs e)
		{
			int iOperation = (int) ViewState["CreditDetailOperation"];
			int currentIndex = (dgAssignedConsignment.EditItemIndex+(dgAssignedConsignment.CurrentPageIndex*dgAssignedConsignment.PageSize));
			/*if(iOperation == (int)Operation.Update)
				return;*/

			Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings,HttpContext.Current.Session);
			String strChargeCode = this.DbChargeCode.Text;
			String strTmpReasonCode = this.DbChargeCode.Value;
			int intSequence = 0;
			try
			{
				intSequence = Convert.ToInt16(strTmpReasonCode);
			}
			catch
			{
				intSequence = 0;
			}
			TextBox txtCreditNoteDescription = (TextBox)dgAssignedConsignment.Items[dgAssignedConsignment.EditItemIndex].FindControl("txtCreditNoteDescription");
			TextBox txtCreditAmt = (TextBox)dgAssignedConsignment.Items[dgAssignedConsignment.EditItemIndex].FindControl("txtCreditAmt");
			TextBox txtRemark = (TextBox)dgAssignedConsignment.Items[dgAssignedConsignment.EditItemIndex].FindControl("txtRemark");			

			String strAppID = util.GetAppID();
			String strEnterpriseID = util.GetEnterpriseID();
			String strCulture = "en-US";//util.GetUserCulture();
			DataSet ds = CreditDebitMgrDAL.GetReasonCodeFromChargeCode(strAppID,strEnterpriseID,intSequence,strCulture);
			String strReasonCode = "";
			String strReasonDesc="";
			if(ds!=null && ds.Tables[0].Rows.Count!=0)
			{
				strReasonCode =  ds.Tables[0].Rows[0]["DbComboText"].ToString();
				strReasonDesc = ds.Tables[0].Rows[0]["DbComboValue"].ToString();
			}
			/*if(dgAssignedConsignment.EditItemIndex == -1)
			{
				return;
			}*/
			if(this.DbChargeCode.Enabled==false)
			{
				return;
			}
			this.DbReasonCode = (DbCombo)dgAssignedConsignment.Items[dgAssignedConsignment.EditItemIndex].FindControl("DbReasonCode");			
			this.DbReasonCode.Text = strReasonCode.Trim();			
			this.DbReasonCode.Value = strReasonCode.Trim();	
			txtCreditNoteDescription.Text = strReasonDesc;
			DataRow drCurrent = m_sdsCreditDetail.ds.Tables[0].Rows[currentIndex];//m_sdsCreditDetail.ds.Tables[0].Rows[dgAssignedConsignment.EditItemIndex];
			if(Utility.IsNotDBNull(drCurrent["charge_code"])==true && drCurrent["charge_code"].ToString().Trim() !="")
			{
				ViewState["OldChargeCode"] = drCurrent["charge_code"];
			}
			else
			{
				ViewState["OldChargeCode"]="";
			}
			if(Utility.IsNotDBNull(drCurrent["reason_code"])==true && drCurrent["reason_code"].ToString() !="")
			{
				ViewState["OldReasonCode"] = drCurrent["reason_code"];
			}
			else
			{
				ViewState["OldReasonCode"]="";
			}

			if(Utility.IsNotDBNull(drCurrent["Description"])==true && drCurrent["Description"].ToString() !="")
			{
				ViewState["OldDesc"] = drCurrent["Description"];
			}
			else
			{
				ViewState["OldDesc"]="";
			}
			drCurrent["charge_code"] = strChargeCode;					
			drCurrent["reason_code"] = strReasonCode;						
			drCurrent["Description"] = strReasonDesc;
			String strConsignmentNo = this.DbConsignmentNo.Text.Trim().ToUpper();
			this.DbConsignmentNo.Value = strConsignmentNo;
			if(strConsignmentNo!=null&&strConsignmentNo.Trim()!="")
			{
				String strinvNo = "";
				Decimal dInvoiceAmt = 0;
				if(!strChargeCode.Trim().Equals(""))
				{
					DataSet chargeDetail = CreditDebitMgrDAL.getChargeCodeDetail(appID,enterpriseID,this.DbChargeCode.Text.Trim(),"en_US");
					
					if(chargeDetail.Tables[0].Rows.Count > 0)
					{
						strinvNo = DbInvoiceNo.Text.Trim().ToUpper();
						dInvoiceAmt = CreditDebitMgrDAL.GetCalcuratedInvoiceAmt(appID,enterpriseID,strConsignmentNo,strinvNo,strChargeCode);
					}

					/*if(strInvoiceAmt==null)
					{
						strInvoiceAmt="0";
					}
					else
					{
						if(strInvoiceAmt.Trim()=="")
						{
							strInvoiceAmt="0";
						}
					}*/
				}
				else
				{
					dInvoiceAmt=0;
				}
				Label lblInvoiceAmt = (Label)dgAssignedConsignment.Items[dgAssignedConsignment.EditItemIndex].FindControl("lblInvoiceAmt");			
				if (lblInvoiceAmt!= null)					
					lblInvoiceAmt.Text=dInvoiceAmt.ToString();
				TextBox txtInvoiceAmt = (TextBox)dgAssignedConsignment.Items[dgAssignedConsignment.EditItemIndex].FindControl("txtInvoiceAmt");			
				if (txtInvoiceAmt!= null)					
					txtInvoiceAmt.Text=dInvoiceAmt.ToString();

//				if(strInvoiceAmt!=null&&strInvoiceAmt.Trim()!="")
//				{
					drCurrent["Invoice_Amt"] = dInvoiceAmt.ToString();
//				}
			}
			if(txtRemark!=null && txtRemark.Text.Trim()!="")
			{
				drCurrent["remark"] = txtRemark.Text;
			}
			if(txtCreditAmt!=null && txtCreditAmt.Text.Trim()!="")
			{
				drCurrent["Amt"] = txtCreditAmt.Text;
			}
			BindConsGrid();
			setDgErrorMsg();

			
		}

		protected void DbReasonCode_OnSelectedIndexChanged(object sender, System.EventArgs e)
		{
			int iOperation = (int) ViewState["CreditDetailOperation"];
			int currentIndex = (dgAssignedConsignment.EditItemIndex+(dgAssignedConsignment.CurrentPageIndex*dgAssignedConsignment.PageSize));
			/*if(iOperation == (int)Operation.Update)
				return;*/

			Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings,HttpContext.Current.Session);
			String strReasonCode = this.DbReasonCode.Text.Trim();
			TextBox txtCreditNoteDescription = (TextBox)dgAssignedConsignment.Items[dgAssignedConsignment.EditItemIndex].FindControl("txtCreditNoteDescription");
			TextBox txtCreditAmt = (TextBox)dgAssignedConsignment.Items[dgAssignedConsignment.EditItemIndex].FindControl("txtCreditAmt");
			TextBox txtRemark = (TextBox)dgAssignedConsignment.Items[dgAssignedConsignment.EditItemIndex].FindControl("txtRemark");
			String strAppID = util.GetAppID();
			String strEnterpriseID = util.GetEnterpriseID();
			String strCulture = "en-US";//util.GetUserCulture();
			DataSet ds = CreditDebitMgrDAL.GetReasonCodeDetail(strAppID,strEnterpriseID,strReasonCode,strCulture);
			String strReasonDesc="";			
			if(ds!=null && ds.Tables[0].Rows.Count!=0)
			{
				strReasonDesc = ds.Tables[0].Rows[0]["DbComboValue"].ToString();
			}
			/*if(dgAssignedConsignment.EditItemIndex == -1)
			{
				return;
			}*/
			DataRow drCurrent = m_sdsCreditDetail.ds.Tables[0].Rows[currentIndex];//m_sdsCreditDetail.ds.Tables[0].Rows[dgAssignedConsignment.EditItemIndex];
//			if(Utility.IsNotDBNull(drCurrent["reason_code"])==true && drCurrent["reason_code"].ToString() !="")
//			{
//				ViewState["OldReasonCode"] = drCurrent["reason_code"];
//			}
//			else
//			{
//				ViewState["OldReasonCode"]="";
//			}
//
//			if(Utility.IsNotDBNull(drCurrent["Description"])==true && drCurrent["Description"].ToString() !="")
//			{
//				ViewState["OldDesc"] = drCurrent["Description"];
//			}
//			else
//			{
//				ViewState["OldDesc"]="";
//			}

			if((Operation)ViewState["CreditDetailOperation"] == Operation.Update)
			{				
				try
				{
					int tmp = Convert.ToInt16(strReasonCode.Trim());
					if(tmp>=9)
					{
						DataSet dsReason = CreditDebitMgrDAL.GetReasonCodeDetail(appID,enterpriseID,strReasonCode.Trim(),"en-US");
						if(dsReason.Tables[0].Rows.Count>0)
						{
							txtCreditNoteDescription.Text = strReasonDesc;						
							drCurrent["reason_code"] = strReasonCode;		
							drCurrent["Description"] = strReasonDesc;						
							lblErrorMsg.Text="";
						}
						else
						{
							this.DbReasonCode.Text = ViewState["OldReasonCode"].ToString().Trim();
							lblErrorMsg.Text=Utility.GetLanguageText(ResourceType.UserMessage,"MSG_REQUIREDFIELD",utility.GetUserCulture()).Replace("[Label Field Name]",dgAssignedConsignment.Columns[5].HeaderText);
							BindConsGrid();
							ViewState["reasonErr"]=true;
							return;
						}
						
					}
					else
					{	
						DataSet chargeDetail = CreditDebitMgrDAL.getChargeCodeDetail(appID,enterpriseID,this.DbChargeCode.Text.Trim(),util.GetUserCulture());
						int sequence = -1; 
						if(chargeDetail.Tables[0].Rows.Count>0)
						{
							sequence = Convert.ToInt16(chargeDetail.Tables[0].Rows[0]["DbComboValue"]);
						}
						if(tmp == sequence)
						{
							txtCreditNoteDescription.Text = strReasonDesc;						
							drCurrent["reason_code"] = strReasonCode;		
							drCurrent["Description"] = strReasonDesc;
							lblErrorMsg.Text="";
						}
						else
						{
							this.DbReasonCode.Text = ViewState["OldReasonCode"].ToString().Trim();
							lblErrorMsg.Text=Utility.GetLanguageText(ResourceType.UserMessage,"MSG_REASONCODE_ERR",utility.GetUserCulture());
							BindConsGrid();
							ViewState["reasonErr"]=true;
							return;
						}

					}
				}
				catch
				{
					lblErrorMsg.Text=Utility.GetLanguageText(ResourceType.UserMessage,"MSG_REQUIREDFIELD",utility.GetUserCulture()).Replace("[Label Field Name]",dgAssignedConsignment.Columns[5].HeaderText);
					BindConsGrid();
					ViewState["reasonErr"]=true;
					return;
				}
			}
			else
			{
				txtCreditNoteDescription.Text = strReasonDesc;						
				drCurrent["reason_code"] = strReasonCode;		
				drCurrent["Description"] = strReasonDesc;
				lblErrorMsg.Text="";
			}
			if(txtRemark!=null && txtRemark.Text.Trim()!="")
			{
				drCurrent["remark"] = txtRemark.Text;
			}

			if(txtCreditAmt!=null && txtCreditAmt.Text.Trim()!="")
			{
				drCurrent["Amt"] = txtCreditAmt.Text;
			}
			ViewState["reasonErr"]=false;
			BindConsGrid();			

			
		}
		

		private String GetCustomerID()
		{
			String custID="";
			try
			{
				custID = DbCustomerID.Value;
				if (custID.Trim()=="")						
					custID =DbCustomerID.Text;	
			}
			catch
			{
			}
			return custID;
		}
		
		private void GetInvoiceDetails(object sender, System.EventArgs e)
		{		
			btnInsertCons.Enabled=false;
			SessionDS m_sdsInvoiceDetails=null;
			String strInvoiceNo= DbInvoiceNo.Value;
			
			if (strInvoiceNo.Trim()=="")						
				strInvoiceNo =DbInvoiceNo.Text;			
						
			m_sdsInvoiceDetails = CreditDebitMgrDAL.GetInvoiceDetails(appID,enterpriseID,strInvoiceNo);			
			if (m_sdsInvoiceDetails == null || m_sdsInvoiceDetails.ds.Tables[0].Rows.Count<=0)
			{
				lblErrorMsg.Text=Utility.GetLanguageText(ResourceType.UserMessage,"MSG_CNNOTFOUND",utility.GetUserCulture());	
				return;
			}
			DataRow drCurrent = m_sdsInvoiceDetails.ds.Tables[0].Rows[0];			
			if (Utility.IsNotDBNull(drCurrent["Invoice_Date"])==true && drCurrent["Invoice_Date"].ToString() !="")
			{
				DateTime dtInvoiceDate = (DateTime)drCurrent["Invoice_Date"];			
				txtInvoiceDate.Text=dtInvoiceDate.ToString("dd/MM/yyyy");
			}
			/*if (Utility.IsNotDBNull(drCurrent["PayerId"])==true && drCurrent["PayerId"].ToString() !="")
					txtPayerID.Text=(String)drCurrent["PayerId"];			*/
			/*if (Utility.IsNotDBNull(drCurrent["Payer_Name"])==true && drCurrent["Payer_Name"].ToString() !="")
					txtPayerName.Text=(String)drCurrent["Payer_Name"];*/
			
			int iOperation=(int)ViewState["DNOperation"];
			if ( iOperation== (int)Operation.Insert)
			{
				ViewState["IsTextChanged"]=true;
			}
			SetDbComboServerStates();
			SetDbCustomerServerStates();
			if(((int)ViewState["DNMode"] == (int)ScreenMode.Insert)||((int)ViewState["DNMode"] == (int)ScreenMode.ExecuteQuery))
			{
				validateInputSave();
			}

		}

		private void GetCustomerDetails(object sender, System.EventArgs e)
		{
			btnInsertCons.Enabled=false;
			SessionDS m_sdsCustomerDetails=null;
			String custID = DbCustomerID.Value;

			if (custID.Trim()=="")						
				custID =DbCustomerID.Text;			
					
			m_sdsCustomerDetails = CreditDebitMgrDAL.GetCustomerDetails(appID,enterpriseID,custID);			
			if (m_sdsCustomerDetails == null || m_sdsCustomerDetails.ds.Tables[0].Rows.Count<=0)
			{
				lblErrorMsg.Text=Utility.GetLanguageText(ResourceType.UserMessage,"MSG_CNNOTFOUND",utility.GetUserCulture());	
				return;
			}
			DataRow drCurrent = m_sdsCustomerDetails.ds.Tables[0].Rows[0];			
			/*if (Utility.IsNotDBNull(drCurrent["Invoice_Date"])==true && drCurrent["Invoice_Date"].ToString() !="")
			{
				DateTime dtInvoiceDate = (DateTime)drCurrent["Invoice_Date"];			
				txtInvoiceDate.Text=dtInvoiceDate.ToString("dd/MM/yyyy HH:mm");
			}*/
			/*if (Utility.IsNotDBNull(drCurrent["PayerId"])==true && drCurrent["PayerId"].ToString() !="")
				txtPayerID.Text=(String)drCurrent["PayerId"];			*/
			if (Utility.IsNotDBNull(drCurrent["cust_name"])==true && drCurrent["cust_name"].ToString() !="")
				txtPayerName.Text=(String)drCurrent["cust_name"];
		
			int iOperation=(int)ViewState["DNOperation"];
			if ( iOperation== (int)Operation.Insert)
			{
				ViewState["IsTextChanged"]=true;
			}			
			SetDbCustomerServerStates();
			if(((int)ViewState["DNMode"] == (int)ScreenMode.Insert)||((int)ViewState["DNMode"] == (int)ScreenMode.ExecuteQuery))
			{
				validateInputSave();
			}
		}

		private void btnInsertCons_Click(object sender, System.EventArgs e)
		{

			if(!validateCustomerInvoice())
			{
				DbCustomerID.ClientOnSelectFunction = "";
				DbInvoiceNo.ClientOnSelectFunction = "";
				DbCustomerID.Enabled=false;
				DbInvoiceNo.Enabled=false;
				ViewState["CreditDetailOperation"] = Operation.Insert;
				//int row =m_sdsCreditDetail.ds.Tables[0].Rows.Count;
				AddRowInConsignmentGrid();								
				BindConsGrid();
			
				int currentIndex=0;
				if(m_sdsCreditDetail.ds.Tables[0].Rows.Count>dgAssignedConsignment.PageSize)
				{
					dgAssignedConsignment.CurrentPageIndex = dgAssignedConsignment.PageCount-1;
					currentIndex = (dgAssignedConsignment.PageSize-((dgAssignedConsignment.PageCount*dgAssignedConsignment.PageSize)-(m_sdsCreditDetail.ds.Tables[0].Rows.Count)))-1;
				}
				else
				{
					dgAssignedConsignment.CurrentPageIndex = 0;
					currentIndex = m_sdsCreditDetail.ds.Tables[0].Rows.Count-1;
				}
				dgAssignedConsignment.EditItemIndex = currentIndex;//m_sdsCreditDetail.ds.Tables[0].Rows.Count - 1;									
				BindConsGrid();			
				setDgErrorMsg();
				lblErrorMsg.Text="";
				btnInsertCons.Enabled=false;			
				btnSave.Enabled=false;
				this.btnRetrieve.Enabled = false;
			}
			else
			{
				DbCustomerID.Text = "";
				DbInvoiceNo.Text = "";
				DbCustomerID.Value = "";
				DbInvoiceNo.Value = "";
			}
		}

		private void AddRowInConsignmentGrid()
		{			
			CreditDebitMgrDAL.AddNewRowInConsDS(ref m_sdsCreditDetail);	
			ViewState["IsTextChanged"]=true;
			/*BindConsGrid();
			Session["SESSION_DS2"] = m_sdsCreditDetail;	*/		
		}

		private void BindConsGrid()
		{
			ViewState["reasonErr"]=false;
			if(m_sdsCreditDetail!=null)
			{
				//dgAssignedConsignment.VirtualItemCount = Convert.ToInt16(m_sdsCreditDetail.QueryResultMaxSize);			
				dgAssignedConsignment.DataSource = m_sdsCreditDetail.ds;
				dgAssignedConsignment.DataBind();

				DisableRetrieveButton();

				Session["SESSION_DS2"] = m_sdsCreditDetail;
			}
		}

		//private bool checkvalid
		protected void dgAssignedConsignment_Bound(object sender, DataGridItemEventArgs e)
		{
			if(e.Item.ItemIndex == -1)			
				return;					

//			if (e.Item.ItemType == ListItemType.EditItem) 
//			{
//				Response.Write(e.Item.ClientID);
//			}
			//int iOperation = (int) ViewState["DNOperation"];
			int iOperation = (int) ViewState["CreditDetailOperation"];
			DataRow drSelected = m_sdsCreditDetail.ds.Tables[0].Rows[e.Item.ItemIndex];
			if(drSelected.RowState == DataRowState.Deleted)			
				return;			

			this.DbConsignmentNo = (Cambro.Web.DbCombo.DbCombo) e.Item.FindControl("DbConsignmentNo");				
			this.DbReasonCode = (Cambro.Web.DbCombo.DbCombo) e.Item.FindControl("DbReasonCode");				
			this.DbChargeCode = (Cambro.Web.DbCombo.DbCombo) e.Item.FindControl("DbChargeCode");				
			TextBox txtDesc = (TextBox)e.Item.FindControl("txtCreditNoteDescription");
			msTextBox txtCreditAmt = (msTextBox)e.Item.FindControl("txtCreditAmt");
			Label lblCreditAmt = (Label)e.Item.FindControl("lblCreditAmt");
			TextBox txtInvoiceAmt = (TextBox)e.Item.FindControl("txtInvoiceAmt");
			Label lblInvoiceAmt = (Label)e.Item.FindControl("lblInvoiceAmt");
			String Amnt = (String)DataBinder.Eval(e.Item.DataItem, "Amt", strFormatCurrency);
			String Invoice_Amt = (String)DataBinder.Eval(e.Item.DataItem, "Invoice_Amt", strFormatCurrency);
			if(txtCreditAmt!=null)
			{
				txtCreditAmt.Text = Amnt.Replace(",","");				
				txtCreditAmt.NumberScale = currency_decimal;
				txtCreditAmt.MaxLength = 8+currency_decimal+1;								
				txtCreditAmt.NumberPrecision = 8+currency_decimal;
//				txtCreditAmt.Attributes.Add("onkeyup", "this.value=this.value.replace(/,/g,'');return NumberMask(this,"+txtCreditAmt.NumberPrecision+","+txtCreditAmt.NumberScale+","+txtCreditAmt.NumberMinValue+","+txtCreditAmt.NumberMaxValue+"); TrimNumber(this,"+txtCreditAmt.NumberScale+");");				
//				txtCreditAmt.Attributes.Add("onchange", "this.value=this.value.replace(/,/g,'');return NumberMask(this,"+txtCreditAmt.NumberPrecision+","+txtCreditAmt.NumberScale+","+txtCreditAmt.NumberMinValue+","+txtCreditAmt.NumberMaxValue+"); TrimNumber(this,"+txtCreditAmt.NumberScale+");");				
				txtCreditAmt.Attributes.Add("onpaste","return false;");
				txtCreditAmt.Attributes.Add("onblur","round(this,"+currency_decimal+")");
			}
			if(lblCreditAmt != null)
			{
				lblCreditAmt.Text = Amnt;
			}
			if(txtInvoiceAmt != null)
			{
				txtInvoiceAmt.Text = Invoice_Amt;
			}
			if(lblInvoiceAmt != null)
			{
				lblInvoiceAmt.Text = Invoice_Amt;
			}
			if(this.DbReasonCode != null)
			{
				this.DbReasonCode.TextLoading = Utility.GetLanguageText(ResourceType.UserMessage,"MSG_DBCOMBO_LOADING",utility.GetUserCulture());
				this.DbReasonCode.TextNoResults = Utility.GetLanguageText(ResourceType.UserMessage,"MSG_DBCOMBO_NOT_FOUND",utility.GetUserCulture());
			}
			if(this.DbChargeCode != null)
			{
				this.DbChargeCode.TextLoading = Utility.GetLanguageText(ResourceType.UserMessage,"MSG_DBCOMBO_LOADING",utility.GetUserCulture());
				this.DbChargeCode.TextNoResults = Utility.GetLanguageText(ResourceType.UserMessage,"MSG_DBCOMBO_NOT_FOUND",utility.GetUserCulture());
			}
			if(this.DbChargeCode != null)
			{
				this.DbChargeCode.TextLoading = Utility.GetLanguageText(ResourceType.UserMessage,"MSG_DBCOMBO_LOADING",utility.GetUserCulture());
				this.DbChargeCode.TextNoResults = Utility.GetLanguageText(ResourceType.UserMessage,"MSG_DBCOMBO_NOT_FOUND",utility.GetUserCulture());
			}
			if(this.DbConsignmentNo != null && iOperation == (int)Operation.Update)
			{
				this.DbConsignmentNo.Enabled = false;	
				this.DbChargeCode.Enabled = false;
				this.DbReasonCode.Enabled =true;
				int tmp=0;
				if(this.DbReasonCode.Value!=null && this.DbReasonCode.Value.Trim()!="")
				{
					try
					{
						tmp =Convert.ToInt16(this.DbReasonCode.Value.Trim());
					}
					catch
					{
					}
				}
				else
				{
					try
					{
						DataSet ds = CreditDebitMgrDAL.GetReasonCodeDetail(appID,enterpriseID,this.DbReasonCode.Text.Trim(),"en-US");
//						DataSet ds = CreditDebitMgrDAL.GetReasonCodeDetail(appID,enterpriseID,this.DbReasonCode.Text.Trim(),utility.GetUserCulture());
						tmp = Convert.ToInt16(ds.Tables[0].Rows[0]["code_num_value"].ToString().Trim());
					}
					catch
					{
					}
				}				
				if(tmp==1)
				{
					txtDesc.ReadOnly=false;
				}
				else
				{
					txtDesc.ReadOnly=true;
				}
			}				
			else if(this.DbConsignmentNo != null && iOperation == (int)Operation.Insert)
			{
				this.DbConsignmentNo.Enabled = true;
				this.DbChargeCode.Enabled = true;
				this.DbReasonCode.Enabled =false;
			}
			setDgErrorMsg();
			/*if ((iOperation==(int)Operation.Saved)||(iOperation==(int)Operation.Update)||(iOperation==(int)Operation.None))
			{*/
				txtTotalCreditAmt.Text = "";
				//Decimal dCreditAmt=0;
				//Decimal dTotCreditAmt=0;
				Decimal dCurrCreditAmt=0;
				/*Label lblCreditAmt=(Label) e.Item.FindControl("lblCreditAmt");				
				msTextBox txtCreditAmt=(msTextBox) e.Item.FindControl("txtCreditAmt");
			
				if (lblCreditAmt !=null && lblCreditAmt.Text !=null && lblCreditAmt.Text.Trim() !="")
				{
					dCreditAmt=System.Convert.ToDecimal(lblCreditAmt.Text);
				}			
				//if((iOperation==(int)Operation.None))
				//{
				if (txtTotalCreditAmt.Text != "")
				{
					dTotCreditAmt=System.Convert.ToDecimal(txtTotalCreditAmt.Text);
				}
				//}				
				dCurrCreditAmt=dCreditAmt+dTotCreditAmt;
				totalCreditAmnt = dCurrCreditAmt;
				txtTotalCreditAmt.Text=dCurrCreditAmt.ToString("#,##0.00");*/
				try
				{
					DataTable dtTmp = (DataTable)m_sdsCreditDetail.ds.Tables[0];
					object tmp = dtTmp.Compute("Sum(Amt)",null);
					if(tmp!=null&&tmp!=DBNull.Value)
					{
						dCurrCreditAmt = Decimal.Parse(tmp.ToString());
						txtTotalCreditAmt.Text=String.Format(strFormatCurrency,dCurrCreditAmt);
					}					
					
				}
				catch
				{
					txtTotalCreditAmt.Text = "";
				}
			//}
			
			//btnSave.Enabled=true;
			
			SetDbComboServerStates();
		}

		protected void dgAssignedConsignment_Edit(object sender, DataGridCommandEventArgs e)
		{
			if(dgAssignedConsignment.EditItemIndex != -1 && (e.Item.ItemIndex!=dgAssignedConsignment.EditItemIndex))
			{
				BindConsGrid();
				setDgErrorMsg();
				return;
			}
			if( (int) ViewState["CreditDetailOperation"] == (int)Operation.Insert && dgAssignedConsignment.EditItemIndex > 0)
			{
				m_sdsCreditDetail.ds.Tables[0].Rows.RemoveAt(dgAssignedConsignment.EditItemIndex);						
			}
			dgAssignedConsignment.EditItemIndex = e.Item.ItemIndex;
			ViewState["CreditDetailOperation"] = Operation.Update;
			txtTotalCreditAmt.Text="";
			ViewState["IsTextChanged"] = true;
			//add new
			int currentIndex = (e.Item.ItemIndex+(dgAssignedConsignment.CurrentPageIndex*dgAssignedConsignment.PageSize));
			DataRow drCurrent = m_sdsCreditDetail.ds.Tables[0].Rows[currentIndex];
			ViewState["OldReasonCode"] = drCurrent["reason_code"].ToString().Trim();	
			ViewState["OldDesc"] = drCurrent["Description"].ToString().Trim();
			ViewState["OldChargeCode"] = drCurrent["charge_code"].ToString().Trim();			
			BindConsGrid();
			setDgErrorMsg();
			btnSave.Enabled=false;
			btnInsertCons.Enabled=false;
			lblErrorMsg.Text = "";
			setDgErrorMsg();
			Logger.LogDebugInfo("CreditNote","CreditNote","dgAssignedConsignment_Edit","INF004","updating data grid...");					
		}

		private bool checkDublicateofChargeCode(string con_no, string chargecode)
		{
			int countofchagecode = 0;
			
			foreach(DataGridItem dtg in dgAssignedConsignment.Items)
			{
				
				DbCombo dlt = (DbCombo)dtg.FindControl("DbChargeCode");
				DbCombo DbConNo = (DbCombo)dtg.FindControl("DbConsignmentNo");

				if(dlt != null && DbConNo != null)
				{
					if(dlt.Text.Equals(chargecode)&& DbConNo.Text.Equals(con_no))
					{
						countofchagecode += 1;
					}
				}
				else
				{
				
					Label lb = (Label)dtg.FindControl("Label2");
					Label lblConNo = (Label)dtg.FindControl("lblConsignmentNo");
					if(lb.Text.Equals(chargecode) && lblConNo.Text.Equals(con_no))
					{
						countofchagecode += 1;
					}
				}
				
			}
			return (countofchagecode > 1);

		}

		private int checkSelectChargeCode_TOT(string con_no, string chargecode)
		{
			int countofchagecode = 0;
			
			foreach(DataGridItem dtg in dgAssignedConsignment.Items)
			{
				Label lb = (Label)dtg.FindControl("Label2");
				Label lblConNo = (Label)dtg.FindControl("lblConsignmentNo");

				if(lb != null && lblConNo != null)
				{
					if(chargecode.Trim().Equals("TOT"))
					{
						if(!lb.Text.Equals("TOT")&& lblConNo.Text.Equals(con_no))
						{
							countofchagecode = 1;
						}
						else if(lb.Text.Equals("TOT")&& lblConNo.Text.Equals(con_no))
						{
							countofchagecode = 2;
						}
					}
					else
					{
						if(lb.Text.Equals("TOT")&& lblConNo.Text.Equals(con_no))
						{
							countofchagecode = 2;
						}
					}
				}
			}
			return countofchagecode;
		}

		public void dgAssignedConsignment_Update(object sender, DataGridCommandEventArgs e)
		{
			/*if(dgAssignedConsignment.EditItemIndex != -1)
			{
				return;
			}*/
			if((bool)ViewState["reasonErr"]==true)
			{
				BindConsGrid();
				return;
			}

			TextBox tx;
			tx = (TextBox) e.Item.FindControl("txtCreditAmt");

			
			if( (tx.Text.Trim().Length <= 0) || (double.Parse(tx.Text) <= 0))
			{
				Page.RegisterStartupScript("SetFocus", "<script>alert('"+Utility.GetLanguageText(ResourceType.UserMessage,"MSG_CDAMT_MORETHAN_ZERO",utility.GetUserCulture())+"')</script>");
//				Page.RegisterStartupScript("SetFocus", "<script>document.getElementById('" + tx.ClientID + "').focus();</script>");
				Page.RegisterStartupScript("SetFocus", "<script>document.getElementById('" + tx.ClientID + "').select();</script>");
				return;
			} 
			
		
			DbCombo dlt =  (DbCombo)e.Item.FindControl("DbChargeCode");
			DbCombo DbConNo = (DbCombo)e.Item.FindControl("DbConsignmentNo");
			BindConsGrid();

			if((dlt != null && dlt.Text.Trim().Length >= 0) && (DbConNo != null && DbConNo.Text.Trim().Length >= 0))
			{
				if(checkDublicateofChargeCode(DbConNo.Text,dlt.Text))
				{
					Page.RegisterStartupScript("SetFocus", "<script>alert('"+Utility.GetLanguageText(ResourceType.UserMessage,"MSG_CANT_SEL_DUP_CON_CHARGECODE",utility.GetUserCulture())+"')</script>");
					return;
				}
			}
			DataSet chargeDetail = CreditDebitMgrDAL.getChargeCodeDetail(appID,enterpriseID,dlt.Text.Trim(),"en_US");
					
			if(chargeDetail.Tables[0].Rows.Count > 0)
			{
				decimal decCanInputAmt = DAL.CreditDebitMgrDAL.GetAvaliableTotalCDNAmount(this.appID,this.enterpriseID,this.DbInvoiceNo.Text.Trim(),DbConNo.Text.Trim(),dlt.Text.Trim());
				if(decCanInputAmt <= 0 || Decimal.Parse(tx.Text) > decCanInputAmt )
				{
					Page.RegisterStartupScript("SetFocus", "<script>alert('"+Utility.GetLanguageText(ResourceType.UserMessage,"MSG_TOTALCNAMOUNTOVER",utility.GetUserCulture())+"')</script>");
					return;
				}
			}
			else
			{
				Page.RegisterStartupScript("SetFocus", "<script>alert('"+Utility.GetLanguageText(ResourceType.UserMessage,"msg_RequiredField",utility.GetUserCulture()).Replace("[Label Field Name]","Charge Code")+"')</script>");
				return;
			}

			int intGetReturn = this.checkSelectChargeCode_TOT(DbConNo.Text.Trim(),dlt.Text.Trim());
			if(intGetReturn == 1)
			{
				Page.RegisterStartupScript("SetFocus", "<script>alert('"+Utility.GetLanguageText(ResourceType.UserMessage,"MSG_CANT_SEL_TOT_HAVEANOTHER",utility.GetUserCulture())+"')</script>");
				return;
			}
			else if(intGetReturn == 2)
			{
				Page.RegisterStartupScript("SetFocus", "<script>alert('"+Utility.GetLanguageText(ResourceType.UserMessage,"MSG_CANT_SEL_ANOTHER_HAVETOT",utility.GetUserCulture())+"')</script>");
				return;
			}
						
			int currentIndex = (e.Item.ItemIndex+(dgAssignedConsignment.CurrentPageIndex*dgAssignedConsignment.PageSize));
			lblErrorMsg.Text="";
			/*if (FillConsDataRow(e.Item,e.Item.ItemIndex)==false) 
			{
				return;
			}*/				
			if (FillConsDataRow(e.Item,currentIndex)==false) 
			{	
				BindConsGrid();
				return;
			}
			//Data has changed in the GRID
			ViewState["IsTextChanged"]=true;
			ViewState["CreditDetailOperation"] = Operation.Saved;
			if(currentIndex==m_sdsCreditDetail.ds.Tables[0].Rows.Count-1)
			{
				sortDetail();
			}
			Session["SESSION_DS2"] = m_sdsCreditDetail;
			int iOperation = (int)ViewState["DNOperation"];
			String strCreditNote = (String)ViewState["currCreditNote"];
			String strInvoiceNo = (String)ViewState["currInvoiceNo"];		
			btnInsertCons.Enabled = true;	
			dgAssignedConsignment.EditItemIndex=-1;	
			txtTotalCreditAmt.Text="";
			btnSave.Enabled=true;
			enableDisableSave();
			BindConsGrid();	
			SetDbComboServerStates();
		}

		protected void dgAssignedConsignment_Cancel(object sender, DataGridCommandEventArgs e)
		{
			//dgAssignedConsignment.EditItemIndex = -1;
			/*if(dgAssignedConsignment.EditItemIndex != -1)
			{
				return;
			}*/
			int currentIndex = (e.Item.ItemIndex+(dgAssignedConsignment.CurrentPageIndex*dgAssignedConsignment.PageSize));
			DataRow drCurrent = m_sdsCreditDetail.ds.Tables[0].Rows[currentIndex];//m_sdsCreditDetail.ds.Tables[0].Rows[dgAssignedConsignment.EditItemIndex];
			if((int)ViewState["CreditDetailOperation"] == (int)Operation.Insert)
			{
				m_sdsCreditDetail.ds.Tables[0].Rows.RemoveAt(currentIndex);		
				m_sdsCreditDetail.QueryResultMaxSize--;
			}
			if(ViewState["OldReasonCode"]!=null)
			{
				drCurrent["reason_code"] = ViewState["OldReasonCode"];
				ViewState["OldReasonCode"]=null;
			}
			if(ViewState["OldDesc"]!=null)
			{
				drCurrent["Description"] = ViewState["OldDesc"];
				ViewState["OldDesc"]=null;
			}
			if(ViewState["OldChargeCode"]!=null)
			{
				drCurrent["charge_code"] = ViewState["OldChargeCode"];			
				ViewState["OldChargeCode"]=null;
			}			
			ViewState["CreditDetailOperation"] = Operation.None;
			txtTotalCreditAmt.Text="";
			dgAssignedConsignment.EditItemIndex = -1;			
			BindConsGrid();
			btnInsertCons.Enabled = true;
			btnSave.Enabled=true;
			lblErrorMsg.Text = "";
			enableDisableSave();
			Logger.LogDebugInfo("CreditNote","CreditNote","dgAssignedConsignment_Cancel","INF004","updating data grid...");			
		}

		protected void dgAssignedConsignment_Delete(object sender, DataGridCommandEventArgs e)
		{
			lblErrorMsg.Text="";
			if(dgAssignedConsignment.EditItemIndex != -1 && (e.Item.ItemIndex!=dgAssignedConsignment.EditItemIndex))
			{
				BindConsGrid();
				setDgErrorMsg();
				return;
			}

			if((bool)ViewState["reasonErr"]==true)
			{
				BindConsGrid();
				setDgErrorMsg();
				return;
			}
			//Consignment Number
			int currentIndex = (e.Item.ItemIndex+(dgAssignedConsignment.CurrentPageIndex*dgAssignedConsignment.PageSize));
			if(m_sdsCreditNote.ds.Tables[0].Rows[System.Convert.ToInt32(ViewState["currentPage"])]["status"].ToString().ToUpper()=="A")
			{
				if(Utility.IsNotDBNull(m_sdsCreditDetail.ds.Tables[0].Rows[currentIndex]["flag"]) && (int)m_sdsCreditDetail.ds.Tables[0].Rows[currentIndex]["flag"]==1)
				{
					lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"MSG_DELETE_DNDETAIL_ERR",utility.GetUserCulture());
					BindConsGrid();
					setDgErrorMsg();
					return;
				}			
			}
			String strConsignmentNo=null;
			this.DbConsignmentNo = (Cambro.Web.DbCombo.DbCombo)e.Item.FindControl("DbConsignmentNo");
			Label lblConsignmentNo = (Label)e.Item.FindControl("lblConsignmentNo");			
			if (this.DbConsignmentNo !=null)
				strConsignmentNo=this.DbConsignmentNo.Value;
			if (strConsignmentNo =="")
				strConsignmentNo=this.DbConsignmentNo.Text;
			if (lblConsignmentNo !=null)
				strConsignmentNo=lblConsignmentNo.Text.Trim();
			
			String strCreditNote = (String)ViewState["currCreditNote"];		
			String strInvoiceNo = (String)ViewState["currInvoiceNo"];
			try
			{
				if(currentIndex<=m_sdsCreditDetail.ds.Tables[0].Rows.Count-1 && m_sdsCreditDetail.ds.Tables[0].Rows.Count>0)
				{
					if((int)ViewState["CreditDetailMode"] == (int)ScreenMode.Insert)
					{
						//CreditDebitMgrDAL.DeleteCreditDetailDS(appID,enterpriseID,strInvoiceNo, strCreditNote,strConsignmentNo,dCreditAmount);
						m_sdsCreditDetail.ds.Tables[0].Rows.RemoveAt(currentIndex);//(e.Item.ItemIndex);						
					}
					else
					{
						if(dgAssignedConsignment.EditItemIndex == e.Item.ItemIndex)
							m_sdsCreditDetail.ds.Tables[0].Rows.RemoveAt(currentIndex);//(e.Item.ItemIndex);
						else
						{
							//CreditDebitMgrDAL.DeleteCreditDetailDS(appID,enterpriseID,strInvoiceNo, strCreditNote,strConsignmentNo,dCreditAmount);
							m_sdsCreditDetail.ds.Tables[0].Rows.RemoveAt(currentIndex);//(e.Item.ItemIndex);	
							if(dgAssignedConsignment.EditItemIndex > 0)
								m_sdsCreditDetail.ds.Tables[0].Rows.RemoveAt(currentIndex);//(dgAssignedConsignment.EditItemIndex-1);
						}
					}
				}
				dgAssignedConsignment.EditItemIndex = -1;
				//txtTotalCreditAmt.Text="";
				BindConsGrid();
				btnSave.Enabled=true;
				btnInsertCons.Enabled = true;
				if((int)ViewState["CreditDetailOperation"] != (int)Operation.Insert)
				{
					lblErrorMsg.Text=Utility.GetLanguageText(ResourceType.UserMessage,"DELETED_SUCCESSFULLY",utility.GetUserCulture());	
				}
				if(m_sdsCreditDetail.ds.Tables[0].Rows.Count==0)
				{
					txtTotalCreditAmt.Text=String.Format(strFormatCurrency,0);
				}
				enableDisableSave();
				setDgErrorMsg();
				ViewState["IsTextChanged"] = true;
			}
			catch(ApplicationException appException)
			{
				String strMsg = appException.Message;
				if(strMsg.IndexOf("FK") != -1)
				{					
					lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"ERR_DEL_Credit_TRANS",utility.GetUserCulture());
				}
				return;
			}				
			Logger.LogDebugInfo("CreditNote","CreditNote","dgAssignedConsignment_Delete","INF004","Deleted row in Credit_Detail table..");
			ViewState["CreditDetailOperation"] = Operation.None;
			btnInsertCons.Enabled = true;
			setDgErrorMsg();
			
		}

		private void ShowCurrConsPage()
		{			
			String strCreditNote = (String)ViewState["currCreditNote"];
			String strInvoiceNo = (String)ViewState["currInvoiceNo"];

			m_sdsCreditDetail.ds = CreditDebitMgrDAL.GetCreditDetailDS(appID,enterpriseID,strInvoiceNo, strCreditNote);
			dgAssignedConsignment.EditItemIndex = -1;
			BindConsGrid();
			//			lblErrorMsg.Text = "";
		}
		
		private Boolean FillConsDataRow(DataGridItem item, int drIndex)
		{
			Boolean validated=false;

			String strConsignmentNo=null;
			String strChargeCode=null;
			String strReasonCode=null;
			DataRow drCurrent = m_sdsCreditDetail.ds.Tables[0].Rows[drIndex];
			this.DbConsignmentNo = (Cambro.Web.DbCombo.DbCombo)item.FindControl("DbConsignmentNo");	
			this.DbChargeCode = (Cambro.Web.DbCombo.DbCombo)item.FindControl("DbChargeCode");	
			this.DbReasonCode = (Cambro.Web.DbCombo.DbCombo)item.FindControl("DbReasonCode");	
			TextBox txtCreditNoteDescription = (TextBox) item.FindControl("txtCreditNoteDescription");
			TextBox txtRemark = (TextBox) item.FindControl("txtRemark");

			msTextBox txtCreditAmt=(msTextBox)item.FindControl("txtCreditAmt");

			if (this.DbConsignmentNo == null)
			{//DbConsignment control is NOT CREATED/FOUND
				lblErrorMsg.Text=Utility.GetLanguageText(ResourceType.UserMessage,"CNSGMNT_NOT_SET",utility.GetUserCulture());
				return validated;
			}			
			if (this.DbConsignmentNo.Value.ToString() == "")
			{//DbConsignment has NO Value/Null Value
				if (this.DbConsignmentNo.Text.ToString() =="")
				{
					lblErrorMsg.Text=Utility.GetLanguageText(ResourceType.UserMessage,"SEL_CONSGMT_NO",utility.GetUserCulture());
					return validated;
				}
				else
				{//DbConsignment Control has Text property with SOME value
					this.DbConsignmentNo.Value=this.DbConsignmentNo.Text;
					strConsignmentNo=this.DbConsignmentNo.Text;
					validated=true;
				}				
			}
			else
			{//DbConsignmnet has SOME Value
				strConsignmentNo=this.DbConsignmentNo.Value;
				if(this.DbConsignmentNo.Text !="") 
				{
					strConsignmentNo=this.DbConsignmentNo.Text;
					if (this.DbConsignmentNo.Value.ToString()!=  this.DbConsignmentNo.Text.ToString())
					{	//Text and Value properties have different values, because USER has changed
						lblErrorMsg.Text=Utility.GetLanguageText(ResourceType.UserMessage,"CNSGMNT_INVALIDE",utility.GetUserCulture());
						return validated;
					}
				}	
			}
			//Now Assign the Values here........
			drCurrent["Consignment_No"]=strConsignmentNo;


			if (this.DbChargeCode == null)
			{//DbChargeCode control is NOT CREATED/FOUND
				lblErrorMsg.Text=Utility.GetLanguageText(ResourceType.UserMessage,"MSG_REQUIREDFIELD",utility.GetUserCulture()).Replace("[Label Field Name]",dgAssignedConsignment.Columns[3].HeaderText);
				return validated;
			}			
			if (this.DbChargeCode.Value.ToString() == "")
			{//DbChargeCode has NO Value/Null Value
				if (this.DbChargeCode.Text.ToString() =="")
				{
					lblErrorMsg.Text=Utility.GetLanguageText(ResourceType.UserMessage,"MSG_REQUIREDFIELD",utility.GetUserCulture()).Replace("[Label Field Name]",dgAssignedConsignment.Columns[3].HeaderText);
					return validated;
				}
				else
				{//DbChargeCode Control has Text property with SOME value
					this.DbChargeCode.Value=this.DbChargeCode.Text;
					strChargeCode=this.DbChargeCode.Text;
					validated=true;
				}				
			}
			else
			{//DbChargeCode has SOME Value
				strChargeCode=this.DbChargeCode.Text;
				if(this.DbChargeCode.Text !="") 
				{
					strChargeCode=this.DbChargeCode.Text;
					if (this.DbChargeCode.Value.ToString()!=  this.DbChargeCode.Text.ToString())
					{	//Text and Value properties have different values, because USER has changed
						lblErrorMsg.Text=Utility.GetLanguageText(ResourceType.UserMessage,"MSG_REQUIREDFIELD",utility.GetUserCulture()).Replace("[Label Field Name]",dgAssignedConsignment.Columns[3].HeaderText);
						return validated;
					}
				}	
			}
			//Now Assign the Values here........
			drCurrent["charge_code"]=strChargeCode;


			if (this.DbReasonCode == null)
			{//DbReasonCode control is NOT CREATED/FOUND
				lblErrorMsg.Text=Utility.GetLanguageText(ResourceType.UserMessage,"MSG_REQUIREDFIELD",utility.GetUserCulture()).Replace("[Label Field Name]",dgAssignedConsignment.Columns[5].HeaderText);
				return validated;
			}			
			if (this.DbReasonCode.Value.ToString().Trim() == "")
			{//DbReasonCode has NO Value/Null Value
				if (this.DbReasonCode.Text.ToString().Trim() =="")
				{
					lblErrorMsg.Text=Utility.GetLanguageText(ResourceType.UserMessage,"MSG_REQUIREDFIELD",utility.GetUserCulture()).Replace("[Label Field Name]",dgAssignedConsignment.Columns[5].HeaderText);
					return validated;
				}
				else
				{//DbReasonCode Control has Text property with SOME value
					this.DbReasonCode.Value=this.DbReasonCode.Text;
					strReasonCode=this.DbReasonCode.Text;
					validated=true;
				}				
			}
			else
			{//DbReasonCode has SOME Value
				strReasonCode=this.DbReasonCode.Text.Trim();
				if(this.DbReasonCode.Text.Trim() !="") 
				{
					strReasonCode=this.DbReasonCode.Text.Trim();
					if (this.DbReasonCode.Value.ToString().Trim()!=  this.DbReasonCode.Text.ToString().Trim())
					{	//Text and Value properties have different values, because USER has changed
						lblErrorMsg.Text=Utility.GetLanguageText(ResourceType.UserMessage,"MSG_REQUIREDFIELD",utility.GetUserCulture()).Replace("[Label Field Name]",dgAssignedConsignment.Columns[5].HeaderText);
						return validated;
					}
				}	
			}
			//Now Assign the Values here........
			if((Operation)ViewState["CreditDetailOperation"] == Operation.Update)
			{
				try
				{
					int tmp = Convert.ToInt16(strReasonCode.Trim());
					if(tmp>=9)
					{
						DataSet dsReason = CreditDebitMgrDAL.GetReasonCodeDetail(appID,enterpriseID,strReasonCode.Trim(),"en-US");
						if(dsReason.Tables[0].Rows.Count>0)
						{
							drCurrent["reason_code"]=strReasonCode;
						}
						else
						{
							lblErrorMsg.Text=Utility.GetLanguageText(ResourceType.UserMessage,"MSG_REQUIREDFIELD",utility.GetUserCulture()).Replace("[Label Field Name]",dgAssignedConsignment.Columns[5].HeaderText);
							return validated;
						}
					}
					else
					{
						DataSet chargeDetail = CreditDebitMgrDAL.getChargeCodeDetail(appID,enterpriseID,this.DbChargeCode.Text.Trim(),"en-US");
						int sequence = -1; 
						if(chargeDetail.Tables[0].Rows.Count>0)
						{
							sequence = Convert.ToInt16(chargeDetail.Tables[0].Rows[0]["DbComboValue"]);
						}
						if(tmp == sequence)
						{
							drCurrent["reason_code"]=strReasonCode;
						}
						else
						{
							this.DbReasonCode.Text = drCurrent["reason_code"].ToString().Trim();
							lblErrorMsg.Text=Utility.GetLanguageText(ResourceType.UserMessage,"MSG_REASONCODE_ERR",utility.GetUserCulture());
							return validated;
						}
					}
				}
				catch
				{
					lblErrorMsg.Text=Utility.GetLanguageText(ResourceType.UserMessage,"MSG_REQUIREDFIELD",utility.GetUserCulture()).Replace("[Label Field Name]",dgAssignedConsignment.Columns[5].HeaderText);
					return validated;
				}
			}
			else
			{
				drCurrent["reason_code"]=strReasonCode;
			}

			if(txtCreditNoteDescription != null && txtCreditNoteDescription.Text != "")
				drCurrent["Description"] = txtCreditNoteDescription.Text;			
			else
			{				
				lblErrorMsg.Text=Utility.GetLanguageText(ResourceType.UserMessage,"Credit_DESC_REQ",utility.GetUserCulture());
				validated=false;
				return validated;
			}

			if(txtCreditAmt != null && txtCreditAmt.Text !="")				
			{
				try
				{
					Decimal dAmt = System.Convert.ToDecimal(txtCreditAmt.Text);				
					drCurrent["Amt"]=dAmt;
					validated=true;
				}
				catch
				{
					lblErrorMsg.Text="Credit Note amount is mandatory.";
					validated=false;
					return validated;
				}
			}
			else
			{
				lblErrorMsg.Text="Credit Note amount is mandatory.";
				validated=false;
				return validated;
			}			
			
			if(txtRemark != null && txtRemark.Text !="")				
			{
				drCurrent["Remark"]=txtRemark.Text;
			}

			int iCounter=0;			
			for (int i=0;i<m_sdsCreditDetail.ds.Tables[0].Rows.Count;i++)
			{//Check for duplication of Consignment No
				DataRow dr = m_sdsCreditDetail.ds.Tables[0].Rows[i];
				if((strConsignmentNo == dr["Consignment_No"].ToString())&&(strChargeCode==dr["charge_code"].ToString()))
					iCounter++;
			}
			if (iCounter > 1)
			{
				lblErrorMsg.Text=Utility.GetLanguageText(ResourceType.UserMessage,"DUP_KEY_FOUND_CNSG_NO",utility.GetUserCulture());
				validated=false;
				return validated;
			}			
			else
			{
				validated=true;
			}
			validated=true;
			return validated;
		}


		private void dgAssignedConsignment_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if(dgAssignedConsignment.EditItemIndex != -1)
			{
				return;
			}
			int iOperation=(int)ViewState["DNOperation"];
			if ( iOperation== (int)Operation.Insert)
			{
				ViewState["IsTextChanged"]=true;
			}
		}

		private void txtRemarks_TextChanged(object sender, System.EventArgs e)
		{
//			int iOperation=(int)ViewState["DNOperation"];
//			if ( iOperation== (int)Operation.Insert)
//			{
//				ViewState["IsTextChanged"]=true;
//			}
			if ((bool)ViewState["IsTextChanged"]==false)
			{
				ViewState["IsTextChanged"]=true;
			}
		}

		private void btnToSaveChanges_Click(object sender, System.EventArgs e)
		{
			ViewState["IsTextChanged"] = false;
						
			bool isError = false;//SaveUpdateRecord();		
			if(isError == false)
			{
				if((String)ViewState["NextOperation"] == "Insert")
				{
					Insert_Click();
				}
				else if((String)ViewState["NextOperation"] == "ExecuteQuery")
				{
					ExecQry_Click();
				}
				else if((String)ViewState["NextOperation"] == "Query")
				{
					Qry_Click();
				}
				else if((String)ViewState["NextOperation"] == "MoveFirst")
				{
					MoveFirstDS();
				}
				else if((String)ViewState["NextOperation"] == "MoveNext")
				{
					MoveNextDS();
				}
				else if((String)ViewState["NextOperation"] == "MoveLast")
				{
					MoveLastDS();
				}
				else if((String)ViewState["NextOperation"] == "MovePrevious")
				{
					MovePreviousDS();
				}
				else if((String)ViewState["NextOperation"] == "Save")
				{
					bool bError = SaveUpdateRecord();
					if (bError==true)
					{
						return;
					}	
					ViewState["CreditDetailOperation"] = Operation.Saved;
				}
			}
			msgPanel.Visible=false;
			mainPanel.Visible=true;
			//			if ((bool)ViewState["IsTextChanged"]==true && (int)ViewState["DNOperation"]==(int)Operation.Insert)
			//				InsertMode();			
			//			else if((bool)ViewState["IsTextChanged"]==true && (int)ViewState["DNOperation"]==(int)Operation.None)
			//				QueryMode();
			//			else
			//				QueryMode();
		}

		private void btnNO_Click(object sender, System.EventArgs e)
		{
			msgPanel.Visible=false;
			mainPanel.Visible=true;
		}

		private void btnNotToSave_Click(object sender, System.EventArgs e)
		{
			//ViewState["IsTextChanged"]=false;
			if((String)ViewState["NextOperation"] == "Insert")
			{
				Insert_Click();
			}
			else if((String)ViewState["NextOperation"] == "ExecuteQuery")
			{
				ExecQry_Click();
			}
			else if((String)ViewState["NextOperation"] == "Query")
			{
				Qry_Click();
			}
			else if((String)ViewState["NextOperation"] == "MoveFirst")
			{
				MoveFirstDS();
			}
			else if((String)ViewState["NextOperation"] == "MoveNext")
			{
				MoveNextDS();
			}
			else if((String)ViewState["NextOperation"] == "MoveLast")
			{
				MoveLastDS();
			}
			else if((String)ViewState["NextOperation"] == "MovePrevious")
			{
				MovePreviousDS();
			}			

			BindConsGrid();
			SetDbComboServerStates();

			msgPanel.Visible=false;
			mainPanel.Visible=true;
		}

		private void txtCreditNoteDate_TextChanged(object sender, System.EventArgs e)
		{
			if((int)ViewState["DNMode"] != (int)ScreenMode.Query)
			{
				if ((bool)ViewState["IsTextChanged"]==false)
				{
					ViewState["IsTextChanged"]=true;
				}
			}
		}

		private void txtRemarks_TextChanged_1(object sender, System.EventArgs e)
		{
			if ((bool)ViewState["IsTextChanged"]==false)
			{
				ViewState["IsTextChanged"]=true;
			}
		}

		private void btnPrint_Click(object sender, System.EventArgs e)
		{
			if(dgAssignedConsignment.EditItemIndex!=-1)
			{
				BindConsGrid();
				return;
			}
			DbCustomerID.ClientOnSelectFunction = "";
			DbInvoiceNo.ClientOnSelectFunction = "";
			if(CheckDetailFound())
			{
				Session["FORMID"] = "CreditTracking";
				SetParamDataSet();

				string strURL = "ReportViewer.aspx";
				ArrayList paramList = new ArrayList();
				paramList.Add(strURL);
				String sScript = Utility.GetScript("openParentWindow.js",paramList);
				Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
				this.txtTotalCreditAmt.Text="";
				BindConsGrid();	
			}
			else
			{
				//txtTotalCreditAmt.Text="";
				BindConsGrid();	
				this.lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"MSG_CREDIT_NOTE_NOT_FOUND",utility.GetUserCulture());
			}
		}

		private void SetParamDataSet()
		{
			// add new dt for report coz , this form used 'Session["SESSION_DS1"]'
			DataTable dt = new DataTable();
			dt.TableName = "DT_CNNO_Selected";
			dt.Columns.Add("CN_NO",typeof(string));

			DataRow dr = dt.NewRow();
			dr["CN_NO"] = this.txtCreditNoteNo.Text.Trim();
			dt.Rows.Add(dr);
			dt.AcceptChanges();

			//Session["SESSION_DS1"] = this.txtCreditNoteNo.Text.Trim();//this.txtDNtest.Text.Trim();
			SessionDS sds = (SessionDS)Session["SESSION_DS1"];
			DataSet ds = sds.ds;

			if (ds.Tables["DT_CNNO_Selected"] != null)
			{
				ds.Tables.Remove("DT_CNNO_Selected");
				ds.AcceptChanges();
				ds.Tables.Add(dt);
			}
			else
				ds.Tables.Add(dt);

			ds.AcceptChanges();
			m_sdsCreditNote.ds = ds;
			Session["SESSION_DS1"] = m_sdsCreditNote;
		}

		private void validateInputQuery()
		{
			String custID = DbCustomerID.Value;		
			String strInvoiceNo= DbInvoiceNo.Value;		
			if(txtCreditNoteDate.Text.Trim()!=""||custID.Trim()!=""||strInvoiceNo.Trim()!="")
			{
				btnExecQry.Enabled=true;
			}
			else
			{
				btnExecQry.Enabled=false;
			}
		}

		private void dgAssignedConsignment_PageIndexChanged(object source, System.Web.UI.WebControls.DataGridPageChangedEventArgs e)
		{
			if(dgAssignedConsignment.EditItemIndex != -1)
			{
				BindConsGrid();
				return;
			}
			dgAssignedConsignment.CurrentPageIndex = e.NewPageIndex;
			BindConsGrid();
			DisplayCurrentPage();			
			//ShowCurrentDGPage();
			#region Add by Sompote 2010-05-14
			if((int)ViewState["DNMode"] == (int)ScreenMode.Insert)
			{
				ddlStatus.Enabled = false;
			}
			#endregion
		}

		private void setDgErrorMsg()
		{
			try
			{
				this.reqDbChargeCode = (RequiredFieldValidator)dgAssignedConsignment.Items[dgAssignedConsignment.EditItemIndex].FindControl("reqDbChargeCode");
				if(this.reqDbChargeCode != null)
				{
					reqDbChargeCode.ErrorMessage = Utility.GetLanguageText(ResourceType.UserMessage,"msg_RequiredField",utility.GetUserCulture()).Replace("[Label Field Name]",dgAssignedConsignment.Columns[3].HeaderText);
				}
				this.reqDbConsignment = (RequiredFieldValidator)dgAssignedConsignment.Items[dgAssignedConsignment.EditItemIndex].FindControl("reqDbConsignment");
				if(this.reqDbConsignment != null)
				{
					reqDbConsignment.ErrorMessage = Utility.GetLanguageText(ResourceType.UserMessage,"msg_RequiredField",utility.GetUserCulture()).Replace("[Label Field Name]",dgAssignedConsignment.Columns[2].HeaderText);
				}
				this.reqDbReasonCode = (RequiredFieldValidator)dgAssignedConsignment.Items[dgAssignedConsignment.EditItemIndex].FindControl("reqReasonCode");
				if(this.reqDbReasonCode != null)
				{
					reqDbReasonCode.ErrorMessage = Utility.GetLanguageText(ResourceType.UserMessage,"msg_RequiredField",utility.GetUserCulture()).Replace("[Label Field Name]",dgAssignedConsignment.Columns[5].HeaderText);
				}
			}
			catch
			{
			}
		}

		private bool CheckDetailFound()
		{
			int intCheckRow = 0;

			foreach(DataGridItem dgItem in dgAssignedConsignment.Items)
			{
				if(dgItem.ItemType == ListItemType.Item)
				{
					intCheckRow++;
				}
			}

			if(intCheckRow > 0)
				return true;
			else
				return false;
		}

		private void btnRetrieve_Click(object sender, System.EventArgs e)
		{
			ViewState["ClickState"] ="Retrieve";
			if(!validateCustomerInvoice())
			{
				DbCustomerID.ClientOnSelectFunction = "";
				DbInvoiceNo.ClientOnSelectFunction = "";
				DbCustomerID.Enabled=false;
				DbInvoiceNo.Enabled=false;
				Session["CreditNoteParentID"] = "MODID=05CreditNote&PARENTMODID=07Account";
				Session["Credit_no"] = this.txtCreditNoteNo.Text.Trim();
				Session["Invoice_no"] = DbInvoiceNo.Text.Trim();
				Session["CreditHeader"] = m_sdsCreditNote;
				Response.Redirect("CreditNoteRetrieveCons.aspx");
			}
			/*else
			{
				DbCustomerID.Text = "";
				DbInvoiceNo.Text = "";
				DbCustomerID.Value = "";
				DbInvoiceNo.Value = "";
			}*/
		}

		private void txtCreditNoteNo_TextChanged(object sender, System.EventArgs e)
		{
		
		}
		private void DisableRetrieveButton()
		{
			int intCk = 0;
			foreach(DataGridItem dgItem in this.dgAssignedConsignment.Items)
			{
				#region Edit by Sompote 2010-05-14
//				if(dgItem.ItemType == ListItemType.Item)
//				{
//					intCk++;
//				}
				if(dgItem.ItemType == ListItemType.Item || dgItem.ItemType == ListItemType.EditItem)
				{
					intCk++;
				}
				#endregion
			}

			if(intCk > 0)
				this.btnRetrieve.Enabled = false;
			else
				this.btnRetrieve.Enabled = true;
		}

		private void ddlStatus_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			BindConsGrid();
			SetDbComboServerStates();

			if(ddlStatus.SelectedValue == "C"||ddlStatus.SelectedValue == "A")
			{
				this.btnRetrieve.Enabled = false;
			}
			else
			{
				DisableRetrieveButton();
			}
			if(m_sdsCreditNote.ds.Tables[0].Rows[System.Convert.ToInt32(ViewState["currentPage"])]["status"].ToString().Trim().ToUpper()=="A"&&ddlStatus.SelectedValue.ToUpper().Trim()=="A")
				this.btnSave.Enabled=false;
			else
				this.btnSave.Enabled=true;

		}
		private bool CheckDateTime_Format(string strDT)
		{
			bool blReturn = true;
			try
			{
				DateTime dt  = System.DateTime.ParseExact(txtCreditNoteDate.Text.Trim(),"dd/MM/yyyy",null);
			}
			catch(Exception ex)
			{
				blReturn = false;
			}
			return blReturn;
		}
		private void validateInputSave()
		{
			if(ddlStatus.SelectedValue.ToUpper().Trim()!="C"&&ddlStatus.SelectedValue.ToUpper().Trim()!="A")
			{
				if(dgAssignedConsignment.EditItemIndex==-1)
				{
					if(DbCustomerID.Value!=null && DbCustomerID.Value!="" && DbCustomerID.Text.Trim()!="" && DbInvoiceNo.Value!=null && DbInvoiceNo.Value!="" && DbInvoiceNo.Text.Trim()!="")
					{
						btnInsertCons.Enabled=true;
						btnRetrieve.Enabled=true;
					}
					else
					{
						btnInsertCons.Enabled=false;
						btnRetrieve.Enabled=false;
					}
				}
			}
		}
		private bool validateCustomerInvoice()
		{
			bool isError = true;			
			DateTime dtDebitdate=DateTime.Now;
			DateTime dtInvoicedate=DateTime.Now;
			DataSet dsInv =null;
			SessionDS dsCustomer=null;
			if(DbCustomerID.Text!=null&&DbCustomerID.Text.Trim()!="")
			{
				dsCustomer = CreditDebitMgrDAL.GetCustomerDetails(appID,enterpriseID,DbCustomerID.Text.Trim());
				if(dsCustomer.ds.Tables[0].Rows.Count>0)
				{
					DbCustomerID.Value=DbCustomerID.Text.Trim().ToUpper();
					txtPayerName.Text = dsCustomer.ds.Tables[0].Rows[0]["cust_name"].ToString().ToUpper();
				}
				else
				{
					lblErrorMsg.Text=Utility.GetLanguageText(ResourceType.UserMessage,"MSG_INSERT_VALID_DATA",utility.GetUserCulture()).Replace("[Label Field Name]",lblCustomerID.Text);
					return isError;
				}
			}
			else
			{
				lblErrorMsg.Text=Utility.GetLanguageText(ResourceType.UserMessage,"msg_RequiredField",utility.GetUserCulture()).Replace("[Label Field Name]",lblCustomerID.Text);
				return isError;
			}
			
			if(DbInvoiceNo.Text!=null&&DbInvoiceNo.Text.Trim()!="")
			{
				dsInv = CreditDebitMgrDAL.InvoiceDateQuery(appID,enterpriseID,DbCustomerID.Text.Trim(),DbInvoiceNo.Text.Trim().ToUpper());
				if(dsInv.Tables[0].Rows.Count>0)
				{
					DbInvoiceNo.Value=DbInvoiceNo.Text.Trim().ToUpper();
					txtInvoiceDate.Text = Convert.ToDateTime(dsInv.Tables[0].Rows[0]["invoice_date"].ToString()).ToString("dd/MM/yyyy");
					//txtPayerName.Text = dsCustomer.ds.Tables[0].Rows[0]["cust_name"].ToString().ToUpper();
				}
				else
				{
					lblErrorMsg.Text=Utility.GetLanguageText(ResourceType.UserMessage,"MSG_INSERT_VALID_DATA",utility.GetUserCulture()).Replace("[Label Field Name]",lblInvoiceNo.Text);
					return isError;
				}
			}
			else
			{
				lblErrorMsg.Text=Utility.GetLanguageText(ResourceType.UserMessage,"msg_RequiredField",utility.GetUserCulture()).Replace("[Label Field Name]",lblInvoiceNo.Text);
				return isError;
			}

			if(txtCreditNoteDate.Text != null && txtCreditNoteDate.Text.Trim() != "")
			{
				dtDebitdate = System.DateTime.ParseExact(txtCreditNoteDate.Text.Trim(),"dd/MM/yyyy",null);
			}
			if(txtInvoiceDate.Text != null && txtInvoiceDate.Text.Trim() != "")
			{
				dtInvoicedate = System.DateTime.ParseExact(txtInvoiceDate.Text.Trim(),"dd/MM/yyyy",null);
			}
			if (txtRemarks.Text.Length > 200)
			{
				lblErrorMsg.Text=Utility.GetLanguageText(ResourceType.UserMessage,"REMARKS_EXCEED_200CHAR",utility.GetUserCulture());	
				return isError;
			}
			if (dtDebitdate < dtInvoicedate)
			{
				lblErrorMsg.Text=Utility.GetLanguageText(ResourceType.UserMessage,"DEBIT_DT_LESS_INVOICE_DT",utility.GetUserCulture());
				return isError;
			}
			if (m_sdsCreditNote.ds.Tables[0].Rows.Count==0)
			{
				lblErrorMsg.Text=Utility.GetLanguageText(ResourceType.UserMessage,"PLS_ENTER_DEBIT_NOTE",utility.GetUserCulture());	
				return isError;
			}
			if((ViewState["ClickState"]!= null) && (ViewState["ClickState"].ToString()== "Retrieve") && (CreditDebitMgrDAL.GetTotalCNDN(appID,enterpriseID ,DbInvoiceNo.Value.ToString())>0))
			{
				lblErrorMsg.Text=Utility.GetLanguageText(ResourceType.UserMessage, "MSG_CNDN_ALREADY", utility.GetUserCulture());
				ViewState["ClickState"] = null;
				return isError;
			}
			int iOperation = (int)ViewState["DNOperation"];
			switch(iOperation)
			{
				case (int)Operation.None:
				case (int)Operation.Update:
					/*try
					{						
						int currentPage = System.Convert.ToInt32(ViewState["currentPage"]);
						FillCreditNote(currentPage);						
					}
					catch(ApplicationException appException)
					{
						return isError;						
					}					
					btnCancel.Enabled=true;
					DbInvoiceNo.Enabled=false;//after saving the Record, INVOICE must NOT be changed
					DbCustomerID.Enabled = false;					
					ViewState["DNOperation"]=Operation.None;
					isError=false;	*/				
					break;

				case (int)Operation.Insert:
					FillCreditNote(0);					
					m_sdsCreditNote.ds.AcceptChanges();														
					isError=false;					
					break;
			}
			isError=false;
			return isError;
		}
		private void enableDisableSave()
		{
			if(m_sdsCreditDetail.ds.Tables[0].Rows.Count>0)
			{
				btnSave.Enabled = true;
			}
			else
			{
				btnSave.Enabled = false;
			}
		}
		private void sortDetail()
		{
			try
			{				
				DataRow[] dra;
				DataTable dtTmp = m_sdsCreditDetail.ds.Tables[0].Copy();
				//dtTmp.Rows.Clear();
				dra = dtTmp.Select(null,"Consignment_No,charge_code Asc",DataViewRowState.CurrentRows);
				
				
				for(int i=0;i<m_sdsCreditDetail.ds.Tables[0].Rows.Count;i++)
				{
					m_sdsCreditDetail.ds.Tables[0].Rows[i].ItemArray = dra[i].ItemArray;
				}
			}
			catch
			{
			}
		}
	}

}
