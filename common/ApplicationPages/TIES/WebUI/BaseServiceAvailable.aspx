<%@ Import Namespace="com.common.util" %>
<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<%@ Page language="c#" Codebehind="BaseServiceAvailable.aspx.cs" AutoEventWireup="false" Inherits="com.ties.BaseServiceAvailable" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Best Available Service</title>
		<LINK href="css/Styles.css" type="text/css" rel="stylesheet" name="stylesheet">
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<!--#INCLUDE FILE="msFormValidations.inc"-->
		<script runat="server">
		</script>
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Zone" method="post" runat="server">
			<asp:button id="btnQuery" style="Z-INDEX: 101; LEFT: 30px; POSITION: absolute; TOP: 62px" runat="server"
				Width="72px" CausesValidation="False" CssClass="queryButton" Text="Query"></asp:button><asp:button id="btnExecuteQuery" style="Z-INDEX: 102; LEFT: 102px; POSITION: absolute; TOP: 62px"
				runat="server" Width="130px" CausesValidation="False" CssClass="queryButton" Text="Execute Query"></asp:button><asp:button id="btnInsert" style="Z-INDEX: 103; LEFT: 232px; POSITION: absolute; TOP: 62px"
				runat="server" CausesValidation="False" CssClass="queryButton" Text="Insert"></asp:button><asp:datagrid id="dgBestService" style="Z-INDEX: 104; LEFT: 32px; POSITION: absolute; TOP: 128px"
				runat="server" Width="344px" AllowPaging="True" AllowCustomPaging="True" OnPageIndexChanged="dgBestService_PageChange" OnItemDataBound="dgBestService_Bound" OnUpdateCommand="dgBestService_Update"
				OnDeleteCommand="dgBestService_Delete" OnCancelCommand="dgBestService_Cancel" OnEditCommand="dgBestService_Edit" AutoGenerateColumns="False" ItemStyle-Height="20" PageSize="100">
				<ItemStyle Height="20px" CssClass="gridField"></ItemStyle>
				<Columns>
					<asp:EditCommandColumn ButtonType="LinkButton" UpdateText="&lt;IMG SRC='images/butt-update.gif' border=0 title='Update' &gt;"
						CancelText="&lt;IMG SRC='images/butt-cancel.gif' border=0 title='Cancel' &gt;" EditText="&lt;IMG SRC='images/butt-edit.gif' border=0 title='Edit' &gt;">
						<HeaderStyle Width="4%" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle HorizontalAlign="Left"></ItemStyle>
					</asp:EditCommandColumn>
					<asp:ButtonColumn Text="&lt;IMG SRC='images/butt-delete.gif' border=0 title='Delete' &gt;" CommandName="Delete">
						<HeaderStyle Width="2%" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle HorizontalAlign="Center"></ItemStyle>
					</asp:ButtonColumn>
					<asp:TemplateColumn HeaderText="Origin DC">
						<HeaderStyle Font-Bold="True" Width="30%" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle CssClass="gridField"></ItemStyle>
						<ItemTemplate>
							<asp:Label CssClass="gridLabel" ID="lblOriginDC" Text='<%#DataBinder.Eval(Container.DataItem,"origin_dc")%>' Runat="server">
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<cc1:mstextbox CssClass="gridTextBox" ID="txtOriginDC" Text='<%#DataBinder.Eval(Container.DataItem,"origin_dc")%>' Runat="server" Enabled="True" TextMaskType="msUpperAlfaNumericWithUnderscore" MaxLength="12">
							</cc1:mstextbox>
							<asp:RequiredFieldValidator ID="ogValidator" Runat="server" BorderWidth="0" Display="None" ControlToValidate="txtOriginDC"
								ErrorMessage="Origin DC"></asp:RequiredFieldValidator>
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Recipient&lt;br&gt; Postal Code">
						<HeaderStyle Font-Bold="True" Width="40%" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle CssClass="gridField"></ItemStyle>
						<ItemTemplate>
							<asp:Label CssClass="gridLabel" ID="lblRecipPostCode" Text='<%#DataBinder.Eval(Container.DataItem,"recipientZipCode")%>' Runat="server" Enabled="True">
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<asp:TextBox CssClass="gridTextBox" ID="txtZRecipPostCode" Text='<%#DataBinder.Eval(Container.DataItem,"recipientZipCode")%>' Runat="server" MaxLength="100">
							</asp:TextBox>
							<asp:RequiredFieldValidator ID="rpValidator" Runat="server" BorderWidth="0" Display="None" ControlToValidate="txtZRecipPostCode"
								ErrorMessage="Recipient Postal Code"></asp:RequiredFieldValidator>
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Best Service Available">
						<HeaderStyle Font-Bold="True" Width="30%" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle HorizontalAlign="Center" CssClass="gridField"></ItemStyle>
						<ItemTemplate>
							<asp:DropDownList ID="ddlBestService" CssClass="gridTextBox" Runat="server" AutoPostBack="True" style="width:75px"></asp:DropDownList>
						</ItemTemplate>
					</asp:TemplateColumn>
				</Columns>
				<PagerStyle NextPageText="Next" PrevPageText="Previous" HorizontalAlign="Right" CssClass="normalText"></PagerStyle>
			</asp:datagrid><asp:label id="lblTitle" style="Z-INDEX: 111; LEFT: 29px; POSITION: absolute; TOP: 12px" runat="server"
				Width="477px" CssClass="mainTitleSize" Height="26px">Best Service Available</asp:label><asp:label id="lblErrorMessage" style="Z-INDEX: 105; LEFT: 29px; POSITION: absolute; TOP: 92px"
				runat="server" Width="538px" CssClass="errorMsgColor" Height="31px" DESIGNTIMEDRAGDROP="63">Place holder for err msg</asp:label><asp:validationsummary id="ValidationSummary1" style="Z-INDEX: 111; LEFT: 87px; POSITION: absolute; TOP: 97px"
				Height="36px" Width="346px" Runat="server" HeaderText="The following field(s) are required:<br>" ShowMessageBox="True" DisplayMode="BulletList" ShowSummary="False"></asp:validationsummary></form>
	</body>
</HTML>
