<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<%@ Page language="c#" Codebehind="TopNReport.aspx.cs" AutoEventWireup="false" Inherits="com.ties.TopNReport" %>
<%@ Register TagPrefix="dbCombo" Namespace="Cambro.Web.DbCombo" Assembly="Cambro.Web.DbCombo" %>
<%@ Import Namespace="Cambro.Web.DbCombo" %>
<%@ Import Namespace="com.common.util" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>TopNReport</title>
		<LINK href="css/Styles.css" type="text/css" rel="stylesheet" name="stylesheet">
		<meta name="GENERATOR" Content="Microsoft Visual Studio 7.0">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<!--#INCLUDE FILE="msFormValidations.inc"-->
		<script language="javascript" src="Scripts/settingScrollPosition.js"></script>
				<script language="javascript">
			function makeUppercase(objId)
			{
				document.getElementById(objId).value = document.getElementById(objId).value.toUpperCase();
			}
		</script>
	</HEAD>
	<body MS_POSITIONING="GridLayout" onunload="window.top.displayBanner.fnCloseAll(0);">
		<form id="TopNReport" method="post" runat="server">
			<asp:button id="btnQuery" style="Z-INDEX: 100; POSITION: absolute; TOP: 54px; LEFT: 19px" runat="server"
				Text="Query" Width="64px" Height="20" CssClass="queryButton"></asp:button><asp:label id="lblErrorMessage" style="Z-INDEX: 105; POSITION: absolute; TOP: 80px; LEFT: 20px"
				runat="server" Width="528px" CssClass="errorMsgColor" Height="21px">Place holder for err msg</asp:label>&nbsp;
			<asp:button id="btnGenerate" style="Z-INDEX: 101; POSITION: absolute; TOP: 54px; LEFT: 84px"
				runat="server" Text="Generate" Width="82px" Height="20" CssClass="queryButton"></asp:button>
			<TABLE id="tblShipmentTracking" style="Z-INDEX: 104; POSITION: absolute; WIDTH: 799px; HEIGHT: 479px; TOP: 100px; LEFT: 16px"
				border="0" runat="server">
				<TR>
					<TD style="WIDTH: 458px; HEIGHT: 137px" vAlign="top">
						<fieldset style="WIDTH: 446px; HEIGHT: 129px"><legend><asp:label id="lblDates" runat="server" CssClass="tableHeadingFieldset" Width="77px" Font-Bold="True">Booking Date</asp:label></legend>
							<TABLE id="Table1" style="WIDTH: 360px; HEIGHT: 91px" cellSpacing="0" cellPadding="0" align="left"
								border="0" runat="server">
								<TR>
									<td></td>
									<TD colSpan="3">&nbsp;
										<asp:radiobutton id="rbMonth" runat="server" Text="Month" CssClass="tableRadioButton" Width="73px"
											Height="21px" AutoPostBack="True" GroupName="EnterDate" Checked="True"></asp:radiobutton>&nbsp;
										<asp:dropdownlist id="ddMonth" runat="server" CssClass="textField" Width="88px" Height="19px"></asp:dropdownlist>&nbsp;
										<asp:label id="Label5" runat="server" CssClass="tableLabel" Width="30px" Height="22px">Year</asp:label>&nbsp;
										<cc1:mstextbox id="txtYear" runat="server" CssClass="textField" Width="83" MaxLength="5" TextMaskType="msNumeric"
											NumberMaxValue="2999" NumberMinValue="1" NumberPrecision="4"></cc1:mstextbox></TD>
									<td></td>
								</TR>
								<TR>
									<td style="HEIGHT: 20px"></td>
									<TD style="HEIGHT: 20px" colSpan="3">&nbsp;
										<asp:radiobutton id="rbPeriod" runat="server" Text="Period" CssClass="tableRadioButton" Width="62px"
											AutoPostBack="True" GroupName="EnterDate"></asp:radiobutton>&nbsp;&nbsp;&nbsp;
										<cc1:mstextbox id="txtPeriod" runat="server" CssClass="textField" Width="82" MaxLength="10" TextMaskType="msDate"
											TextMaskString="99/99/9999"></cc1:mstextbox>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
										&nbsp;
										<cc1:mstextbox id="txtTo" runat="server" CssClass="textField" Width="83" MaxLength="10" TextMaskType="msDate"
											TextMaskString="99/99/9999"></cc1:mstextbox></TD>
									<td style="HEIGHT: 20px"></td>
								</TR>
								<TR>
									<td></td>
									<TD colSpan="3">&nbsp;
										<asp:radiobutton id="rbDate" runat="server" Text="Date" CssClass="tableRadioButton" Width="74px"
											Height="22px" AutoPostBack="True" GroupName="EnterDate"></asp:radiobutton>&nbsp;&nbsp;<cc1:mstextbox id="txtDate" runat="server" CssClass="textField" Width="82" MaxLength="10" TextMaskType="msDate"
											TextMaskString="99/99/9999"></cc1:mstextbox></TD>
									<td></td>
								</TR>
							</TABLE>
						</fieldset>
					</TD>
					<TD style="HEIGHT: 137px" vAlign="top">
						<fieldset style="WIDTH: 320px; HEIGHT: 105px"><legend><asp:label id="lblPayerType" runat="server" CssClass="tableHeadingFieldset" Width="79px" Font-Bold="True">Payer Type</asp:label></legend>
							<TABLE id="tblPayerType" style="WIDTH: 300px; HEIGHT: 73px" cellSpacing="0" cellPadding="0"
								align="left" border="0" runat="server">
								<tr>
									<td></td>
									<td colSpan="2" height="1">&nbsp;</td>
									<td></td>
									<td></td>
								</tr>
								<TR>
									<td></td>
									<TD class="tableLabel" vAlign="top" colSpan="2">&nbsp;<asp:label id="Label6" runat="server" CssClass="tableLabel" Width="88px" Height="22px">Payer Type</asp:label></TD>
									<td><asp:listbox id="lsbCustType" runat="server" Height="61px" Width="137px" SelectionMode="Multiple"></asp:listbox></td>
									<td></td>
								</TR>
								<TR height="33">
									<td bgColor="blue"></td>
									<TD class="tableLabel" colSpan="2">&nbsp;
										<asp:label id="Label10" runat="server" CssClass="tableLabel" Height="22px" Width="79px">Top</asp:label>
									</TD>
									<td><asp:dropdownlist id="ddTop" runat="server" CssClass="textField" Width="110px"></asp:dropdownlist></td>
									<td></td>
								</TR>
							</TABLE>
						</fieldset>
					</TD>
				</TR>
				<TR>
					<TD style="WIDTH: 458px; HEIGHT: 134px" vAlign="top">
						<fieldset style="WIDTH: 447px; HEIGHT: 127px"><legend><asp:label id="lblRouteType" runat="server" CssClass="tableHeadingFieldset" Width="145px" Font-Bold="True">Route / DC Selection</asp:label></legend>
							<TABLE id="tblRouteType" style="WIDTH: 438px; HEIGHT: 113px" cellSpacing="0" cellPadding="0"
								align="left" border="0" runat="server">
								<TR>
									<TD class="tableLabel" colSpan="2" style="WIDTH: 430px">&nbsp;
										<asp:radiobutton id="rbLongRoute" runat="server" Text="Linehaul" CssClass="tableRadioButton" Width="141px"
											Height="22px" AutoPostBack="True" GroupName="QueryByRoute" Checked="True"></asp:radiobutton><asp:radiobutton id="rbShortRoute" runat="server" Text="Delivery Route" CssClass="tableRadioButton"
											Width="122px" Height="22px" AutoPostBack="True" GroupName="QueryByRoute"></asp:radiobutton><asp:radiobutton id="rbAirRoute" runat="server" Text="Air Route" CssClass="tableRadioButton" Width="155px"
											Height="22px" AutoPostBack="True" GroupName="QueryByRoute"></asp:radiobutton></TD>
								</TR>
								<tr>
									<TD class="tableLabel" width="20%">&nbsp;<asp:label id="lblRouteCode" runat="server" CssClass="tableLabel" Width="111px" Height="22px">Route Code</asp:label></TD>
									<td style="WIDTH: 243px"><DBCOMBO:DBCOMBO id="DbComboPathCode" tabIndex="14" CssClass="tableLabel" Width="25px" AutoPostBack="True"
											Runat="server" TextBoxColumns="18" RegistrationKey=" " TextUpLevelSearchButton="v" ShowDbComboLink="False" ServerMethod="DbComboPathCodeSelect"></DBCOMBO:DBCOMBO></td>
								</tr>
								<TR>
									<TD class="tableLabel">&nbsp;<asp:label id="Label7" runat="server" CssClass="tableLabel" Width="154px" Height="22px">Origin Distribution Center</asp:label></TD>
									<TD style="WIDTH: 243px"><DBCOMBO:DBCOMBO id="DbComboOriginDC" tabIndex="14" CssClass="tableLabel" Width="25px" AutoPostBack="True"
											Runat="server" TextBoxColumns="18" RegistrationKey=" " TextUpLevelSearchButton="v" ShowDbComboLink="False" ServerMethod="DbComboDistributionCenterSelect"></DBCOMBO:DBCOMBO></TD>
								</TR>
								<TR>
									<TD class="tableLabel">&nbsp;<asp:label id="Label8" runat="server" CssClass="tableLabel" Width="181px" Height="22px">Destination Distribution Center</asp:label></TD>
									<TD style="WIDTH: 243px"><DBCOMBO:DBCOMBO id="DbComboDestinationDC" tabIndex="14" CssClass="tableLabel" Width="25px" AutoPostBack="True"
											Runat="server" TextBoxColumns="18" RegistrationKey=" " TextUpLevelSearchButton="v" ShowDbComboLink="False" ServerMethod="DbComboDistributionCenterSelect"></DBCOMBO:DBCOMBO></TD>
								</TR>
							</TABLE>
						</fieldset>
					</TD>
					<TD vAlign="top">
						<fieldset style="WIDTH: 320px; HEIGHT: 89px"><legend><asp:label id="lblDestination" runat="server" CssClass="tableHeadingFieldset" Width="79px"
									Font-Bold="True">Destination</asp:label></legend>
							<TABLE id="tblDestination" style="WIDTH: 300px; HEIGHT: 89px" cellSpacing="0" cellPadding="0"
								align="left" border="0" runat="server">
								<tr height="37">
									<td></td>
									<TD class="tableLabel" style="WIDTH: 360px" colSpan="3">&nbsp;
										<asp:label id="lblZipCode" runat="server" CssClass="tableLabel" Width="84px" Height="22px">Postal Code</asp:label>&nbsp;&nbsp;&nbsp;
										<cc1:mstextbox id="txtZipCode" runat="server" CssClass="textField" Width="139px" MaxLength="10"
											TextMaskType="msUpperAlfaNumericWithUnderscore"></cc1:mstextbox>&nbsp;&nbsp;
										<asp:button id="btnZipCode" runat="server" Text="..." CssClass="searchButton" CausesValidation="False"></asp:button></TD>
									<td></td>
								</tr>
								<tr>
									<td></td>
									<TD class="tableLabel" style="WIDTH: 360px" colSpan="3">&nbsp;
										<asp:label id="lblStateCode" runat="server" CssClass="tableLabel" Width="100px" Height="22px">State / Province</asp:label><cc1:mstextbox id="txtStateCode" runat="server" CssClass="textField" Width="139" Height="22" MaxLength="10"
											TextMaskType="msUpperAlfaNumericWithUnderscore"></cc1:mstextbox>&nbsp;&nbsp;
										<asp:button id="btnStateCode" runat="server" Text="..." CssClass="searchButton" CausesValidation="False"></asp:button></TD>
									<td></td>
								</tr>
							</TABLE>
						</fieldset>
					</TD>
				</TR>
				<TR>
					<TD vAlign="top" colSpan="2">
						<FIELDSET style="WIDTH: 725px; HEIGHT: 92px"><LEGEND>
								<asp:label id="Label2" Runat="server">Retrieval Basis on</asp:label></LEGEND>
							<TABLE id="tblDates" width="100%" align="left" border="0" runat="server">
								<TR>
									<TD colSpan="20">
										<asp:radiobutton id="rbBooked" runat="server" CssClass="tableRadioButton" Height="22px" Width="100%"
											Text="All Booked Shipments" Checked="True" GroupName="Basis" AutoPostBack="True"></asp:radiobutton></TD>
								</TR>
								<TR>
									<TD colSpan="20">
										<asp:radiobutton id="rbInvoiced" runat="server" CssClass="tableRadioButton" Height="22px" Width="100%"
											Text="Only Invoiced Shipments" GroupName="Basis" AutoPostBack="True"></asp:radiobutton></TD>
								</TR>
								<TR>
									<TD colSpan="20">
										<asp:radiobutton id="rbDM" runat="server" CssClass="tableRadioButton" Height="22px" Width="100%"
											Text="Only Delivery Manifested Shipments" GroupName="Basis" AutoPostBack="True"></asp:radiobutton></TD>
								</TR>
							</TABLE>
						</FIELDSET>
						<fieldset style="WIDTH: 726px; HEIGHT: 106px"><LEGEND>
								<asp:label id="Label4" Runat="server">Sort By</asp:label></LEGEND>
							<TABLE style="WIDTH: 480px; HEIGHT: 75px">
								<TR height="14">
									<TD colSpan="20"></TD>
								</TR>
								<TR height="27">
									<TD colSpan="5">
										<asp:radiobutton id="rbRevenue" runat="server" CssClass="tableRadioButton" Height="22px" Width="100%"
											Text="Revenue" Checked="True" GroupName="Sort" AutoPostBack="True"></asp:radiobutton></TD>
									<TD colSpan="5">
										<asp:radiobutton id="rbChrgWt" runat="server" CssClass="tableRadioButton" Height="22px" Width="100%"
											Text="Chargeable Weight" GroupName="Sort" AutoPostBack="True"></asp:radiobutton></TD>
									<TD colSpan="5">
										<asp:radiobutton id="rbVolume" runat="server" CssClass="tableRadioButton" Height="22px" Width="100%"
											Text="Volume" GroupName="Sort" AutoPostBack="True"></asp:radiobutton></TD>
									<TD colSpan="5">
										<asp:radiobutton id="rbConsignment" runat="server" CssClass="tableRadioButton" Height="22px" Width="100%"
											Text="Consignment" GroupName="Sort" AutoPostBack="True"></asp:radiobutton></TD>
								</TR>
								<TR height="8">
									<TD colSpan="20"></TD>
								</TR>
							</TABLE>
						</fieldset>
					</TD>
				</TR>
			</TABLE>
			<asp:label id="lblTitle" style="Z-INDEX: 103; POSITION: absolute; TOP: 10px; LEFT: 20px" runat="server"
				Width="365px" CssClass="maintitleSize" Height="27px">Top N Report</asp:label></form>
		</FORM>
	</body>
</HTML>
