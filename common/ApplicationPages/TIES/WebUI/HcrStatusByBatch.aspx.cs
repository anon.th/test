using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.ties.DAL;
using System.Text;
using com.common.util;
using com.common.classes;
using com.common.DAL;
using com.common.RBAC;


namespace TIES.WebUI
{
	/// <summary>
	/// Summary description for HcrStatusByBatch.
	/// </summary>
	public class HcrStatusByBatch : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.Label lblAppId;
		protected System.Web.UI.WebControls.Label lblEntId;
		protected System.Web.UI.WebControls.ListBox lbFrom;
		protected System.Web.UI.WebControls.ListBox lbTo;
		protected System.Web.UI.WebControls.Button btnSearch;
		protected System.Web.UI.WebControls.Label lblHcsNumber;
		protected System.Web.UI.WebControls.Label lblHcsDate;
		protected System.Web.UI.WebControls.Label lblLocation;
		protected System.Web.UI.WebControls.TextBox txtDate;
		protected System.Web.UI.WebControls.ListBox lbHcsData;
		protected System.Web.UI.HtmlControls.HtmlGenericControl divData;
		protected System.Web.UI.WebControls.TextBox txtHcsNo;
		protected System.Web.UI.WebControls.Button btnCallServer;
		protected System.Web.UI.WebControls.Label lblErrorMessage;
		protected System.Web.UI.WebControls.Label lblUserId;		
		protected System.Web.UI.HtmlControls.HtmlTable tblShipmentTracking;
		protected System.Web.UI.WebControls.TextBox txtConsVal;
		protected System.Web.UI.HtmlControls.HtmlGenericControl lblErrorMsg;
	
		private string strAppId = null;
		private string strEntId = null;
		private Utility utility = null;			
		DataSet ds = null;


		private void Page_Load(object sender, System.EventArgs e)
		{
			if(!Page.IsPostBack)
			{
				utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings,Page.Session);
				strAppId = utility.GetAppID();
				strEntId = utility.GetEnterpriseID();

				com.common.classes.User user = RBACManager.GetUser(strAppId, strEntId, (String)Session["userID"]);

				lblUserId.Text = user.UserID;
				lblLocation.Text = user.UserLocation;

				lblAppId.Text = utility.GetAppID();
				lblEntId.Text = utility.GetEnterpriseID();

				RefreshData();
				BindStatusCode();
			}
		}


		private void RefreshData()
		{
			DbConnection dbConApp = null;		

			dbConApp = DbConnectionManager.GetInstance().GetDbConnection(strAppId, strEntId);

			if(dbConApp == null)
			{
				Logger.LogTraceError("HcrStatusByBatch.aspx.cs","RefreshData","HcrStatusByBatch","dbConApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			StringBuilder strQry = new StringBuilder();
			strQry.Append("SELECT distinct status_code FROM Status_Code ");
			strQry.Append(" WHERE applicationid = '" + strAppId + "' ");
			strQry.Append(" AND enterpriseid = '" + strEntId + "'");
			strQry.Append(" AND is_hcr_scan = 'Y'");

			try
			{
				ds = (DataSet)dbConApp.ExecuteQuery(strQry.ToString(), ReturnType.DataSetType);
			}
			catch(ApplicationException appExpection)
			{
				Logger.LogTraceError("HcrStatusByBatch.aspx.cs","RefreshData","HcrStatusByBatch","Error in the query String");
				throw appExpection;
			}
			finally
			{
				ds.Dispose();
			}
		}

		
		private void BindStatusCode()
		{
			lbFrom.DataSource = ds;
			lbFrom.DataTextField = ds.Tables[0].Columns["status_code"].ColumnName.ToString();
			lbFrom.DataValueField = ds.Tables[0].Columns["status_code"].ColumnName.ToString();
			lbFrom.DataBind();
		}

		
		private void btnCallServer_Click(object sender, System.EventArgs e)
		{
			GetDataHcsListBox();

			lblHcsNumber.Text = txtHcsNo.Text;
			lblHcsDate.Text = txtDate.Text;
		}

		
		private void GetDataHcsListBox()
		{
			DataSet DS = null;
			string strDate = txtDate.Text;
			string strLocation = "";

			try
			{
				DS = HcrStatusByBatchDAL.GetByDateHcsHdDS(lblAppId.Text, lblEntId.Text, strDate, strLocation);

				if (DS.Tables[0].Rows.Count > 0)
				{
					ArrayList arrData = new ArrayList();

					foreach (DataRow dRow in DS.Tables[0].Rows)
					{
						arrData.Add((string)dRow["hcs_no"]);
					}

					lbHcsData.Items.Clear();
					lbHcsData.DataSource = arrData;
					lbHcsData.DataBind();

					if (lbHcsData.Items.Count == 1)
					{
						lbHcsData.SelectedIndex = 0;
						lbHcsDataSelected();
					}
					else
					{
						txtHcsNo.Text = " ";	
					}
				}
				else
				{
					lbHcsData.Items.Clear();
				}
			}
			catch(Exception ex)
			{
				lblErrorMsg.InnerText = ex.Message.ToString();
			} 
			finally
			{
				DS.Dispose();
			}
		}

		

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnCallServer.Click += new System.EventHandler(this.btnCallServer_Click);
			this.lbHcsData.SelectedIndexChanged += new System.EventHandler(this.lbHcsData_SelectedIndexChanged);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private void lbHcsData_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			lbHcsDataSelected();
		}

		
		private void lbHcsDataSelected()
		{
			txtHcsNo.Text = lbHcsData.SelectedItem.Text;
			lblHcsNumber.Text = lbHcsData.SelectedItem.Text;

			lblHcsDate.Text = txtDate.Text;
		}

	}
}
