<%@ Import Namespace="com.common.util" %>
<%@ Import Namespace="Cambro.Web.DbCombo" %>
<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<%@ Register TagPrefix="DbCombo" Namespace="Cambro.Web.DbCombo" Assembly="Cambro.Web.DbCombo" %>
<%@ Page language="c#" Codebehind="FirstDelReport.aspx.cs" AutoEventWireup="false" Inherits="TIES.WebUI.FirstDelReport" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Service Quality Indicator Report</title>
		<LINK href="css/Styles.css" type="text/css" rel="stylesheet" name="stylesheet">
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<!--#INCLUDE FILE="msFormValidations.inc"-->
		<script language="javascript" src="Scripts/settingScrollPosition.js"></script>
	</HEAD>
	<body onclick="GetScrollPosition();" onload="SetScrollPosition();" onunload="window.top.displayBanner.fnCloseAll(0);"
		MS_POSITIONING="GridLayout">
		<form id="SalesTerritoryReportQuery" method="post" runat="server">
			<asp:button id="btnQuery" style="Z-INDEX: 100; LEFT: 20px; POSITION: absolute; TOP: 40px" runat="server"
				Width="64px" CssClass="queryButton" Text="Query"></asp:button><asp:label id="lblErrorMessage" style="Z-INDEX: 105; LEFT: 24px; POSITION: absolute; TOP: 69px"
				runat="server" Width="700px" CssClass="errorMsgColor" Height="17px">Place holder for err msg</asp:label>&nbsp;
			<asp:button id="btnExecuteQuery" style="Z-INDEX: 101; LEFT: 90px; POSITION: absolute; TOP: 40px"
				runat="server" Width="123px" CssClass="queryButton" Text="Execute Query"></asp:button>
			<TABLE id="tblInternal" style="Z-INDEX: 104; LEFT: 21px; WIDTH: 800px; POSITION: absolute; TOP: 90px"
				width="800" border="0" runat="server">
				<TR>
					<TD style="WIDTH: 351px; HEIGHT: 137px" vAlign="top">
						<fieldset style="WIDTH: 390px; HEIGHT: 129px"><legend><asp:label id="lblDates" runat="server" Width="20px" CssClass="tableHeadingFieldset" Font-Bold="True">Dates</asp:label></legend>
							<TABLE id="tblDates" style="WIDTH: 360px; HEIGHT: 91px" cellSpacing="0" cellPadding="0"
								align="left" border="0" runat="server">
								<TR>
									<td style="HEIGHT: 22px"></td>
									<TD style="HEIGHT: 22px" colSpan="3">&nbsp;
										<asp:radiobutton id="rbBookingDate" runat="server" Width="168px" CssClass="tableRadioButton" Text="Actual Delivery Date"
											Height="22px" AutoPostBack="True" GroupName="QueryByDate" Checked="True"></asp:radiobutton></TD>
									<td style="HEIGHT: 22px"></td>
								</TR>
								<TR>
									<td></td>
									<TD colSpan="3">&nbsp;
										<asp:radiobutton id="rbMonth" runat="server" Width="73px" CssClass="tableRadioButton" Text="Month"
											Height="21px" AutoPostBack="True" GroupName="EnterDate" Checked="True"></asp:radiobutton>&nbsp;
										<asp:dropdownlist id="ddMonth" runat="server" Width="88px" CssClass="textField" Height="19px"></asp:dropdownlist>&nbsp;
										<asp:label id="lblYear" runat="server" Width="30px" CssClass="tableLabel" Height="22px">Year</asp:label>&nbsp;
										<cc1:mstextbox id="txtYear" runat="server" Width="83" CssClass="textField" MaxLength="5" TextMaskType="msNumeric"
											NumberMaxValue="2999" NumberMinValue="1" NumberPrecision="4"></cc1:mstextbox></TD>
									<td></td>
								</TR>
								<TR>
									<td style="HEIGHT: 20px"></td>
									<TD style="HEIGHT: 20px" colSpan="3">&nbsp;
										<asp:radiobutton id="rbPeriod" runat="server" Width="62px" CssClass="tableRadioButton" Text="Period"
											AutoPostBack="True" GroupName="EnterDate"></asp:radiobutton>&nbsp;&nbsp;&nbsp;
										<cc1:mstextbox id="txtPeriod" runat="server" Width="82" CssClass="textField" MaxLength="10" TextMaskType="msDate"
											TextMaskString="99/99/9999"></cc1:mstextbox>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
										&nbsp;
										<cc1:mstextbox id="txtTo" runat="server" Width="83" CssClass="textField" MaxLength="10" TextMaskType="msDate"
											TextMaskString="99/99/9999"></cc1:mstextbox></TD>
									<td style="HEIGHT: 20px"></td>
								</TR>
								<TR>
									<td></td>
									<TD colSpan="3">&nbsp;
										<asp:radiobutton id="rbDate" runat="server" Width="74px" CssClass="tableRadioButton" Text="Date"
											Height="22px" AutoPostBack="True" GroupName="EnterDate"></asp:radiobutton>&nbsp;&nbsp;<cc1:mstextbox id="txtDate" runat="server" Width="82" CssClass="textField" MaxLength="10" TextMaskType="msDate"
											TextMaskString="99/99/9999"></cc1:mstextbox></TD>
									<td></td>
								</TR>
							</TABLE>
						</fieldset>
					</TD>
					<TD vAlign="top">
						<fieldset style="WIDTH: 320px"><legend><asp:label id="lblPayerType" runat="server" Width="79px" CssClass="tableHeadingFieldset" Font-Bold="True">Payer Type</asp:label></legend>
							<TABLE id="tblPayerType" style="WIDTH: 300px; HEIGHT: 73px" cellSpacing="0" cellPadding="0"
								align="left" border="0" runat="server">
								<tr>
									<td></td>
									<td colSpan="2" height="1">&nbsp;</td>
									<td></td>
									<td></td>
								</tr>
								<TR>
									<td></td>
									<TD class="tableLabel" vAlign="top" colSpan="2">&nbsp;
										<asp:label id="Label4" runat="server" CssClass="tableLabel" Width="88px" Height="22px">Payer Type</asp:label></TD>
									<td><asp:listbox id="lsbCustType" runat="server" Width="148px" Height="61px" SelectionMode="Multiple"></asp:listbox></td>
									<td></td>
								</TR>
								<tr>
									<td></td>
									<td colSpan="2" height="1">&nbsp;
										<asp:label id="lblPayerCode" runat="server" Width="79px" CssClass="tableLabel" Height="22px">Payer Code</asp:label></td>
									<td><asp:textbox id="txtPayerCode" runat="server" Width="148px" CssClass="textField"></asp:textbox><asp:button id="btnPayerCode" runat="server" CssClass="searchButton" Text="..." CausesValidation="False"></asp:button></td>
									<td></td>
								</tr>
							</TABLE>
						</fieldset>
					</TD>
				</TR>
				<TR>
					<TD style="WIDTH: 351px" vAlign="top">
						<fieldset style="WIDTH: 390px; HEIGHT: 32px"><legend><asp:label id="lblRouteType" runat="server" Width="145px" CssClass="tableHeadingFieldset" Font-Bold="True">Route / DC Selection</asp:label></legend>
							<TABLE id="tblRouteType" style="WIDTH: 360px; HEIGHT: 47px" cellSpacing="0" cellPadding="0"
								align="left" border="0" runat="server">
								<TR>
									<TD class="tableLabel" width="20%" colSpan="2"><FONT face="Tahoma">
											<P><asp:radiobutton id="rbShortRoute" runat="server" Width="168px" CssClass="tableRadioButton" Text="Delivery Route"
													Height="22px" AutoPostBack="True" GroupName="QueryByRoute" Checked="True"></asp:radiobutton></P>
										</FONT>
									</TD>
								</TR>
								<tr>
									<TD class="tableLabel" width="20%">&nbsp;<asp:label id="lblRouteCode" runat="server" Width="111px" CssClass="tableLabel" Height="22px">Route Code</asp:label></TD>
									<td><DBCOMBO:DBCOMBO id="DbComboPathCode" tabIndex="14" Width="25px" CssClass="tableLabel" AutoPostBack="True"
											ServerMethod="DbComboPathCodeSelect" ShowDbComboLink="False" TextUpLevelSearchButton="v" RegistrationKey=" "
											TextBoxColumns="18" Runat="server"></DBCOMBO:DBCOMBO></td>
								</tr>
								<TR>
									<TD class="tableLabel">&nbsp;<asp:label id="Label3" runat="server" Width="181px" CssClass="tableLabel" Height="22px">Destination Distribution Center</asp:label></TD>
									<TD><DBCOMBO:DBCOMBO id="DbComboDestinationDC" tabIndex="14" Width="25px" CssClass="tableLabel" AutoPostBack="True"
											ServerMethod="DbComboDistributionCenterSelect" ShowDbComboLink="False" TextUpLevelSearchButton="v"
											RegistrationKey=" " TextBoxColumns="18" Runat="server"></DBCOMBO:DBCOMBO></TD>
								</TR>
							</TABLE>
						</fieldset>
					</TD>
					<TD vAlign="top">
						<fieldset style="WIDTH: 320px; HEIGHT: 80px"><legend><asp:label id="lblDestination" runat="server" Width="79px" CssClass="tableHeadingFieldset"
									Font-Bold="True">Destination</asp:label></legend>
							<TABLE id="tblDestination" style="WIDTH: 300px; HEIGHT: 57px" cellSpacing="0" cellPadding="0"
								align="left" border="0" runat="server">
								<tr height="37">
									<td style="HEIGHT: 33px"></td>
									<TD class="tableLabel" style="WIDTH: 360px; HEIGHT: 33px" colSpan="3">&nbsp;
										<asp:label id="lblZipCode" runat="server" Width="84px" CssClass="tableLabel" Height="22px">Postal Code</asp:label>&nbsp;&nbsp;&nbsp;
										<cc1:mstextbox id="txtZipCode" runat="server" Width="139px" CssClass="textField" MaxLength="10"
											TextMaskType="msUpperAlfaNumericWithUnderscore"></cc1:mstextbox>&nbsp;&nbsp;
										<asp:button id="btnZipCode" runat="server" CssClass="searchButton" Text="..." CausesValidation="False"></asp:button></TD>
									<td style="HEIGHT: 33px"></td>
								</tr>
								<tr>
									<td style="HEIGHT: 33px"></td>
									<TD class="tableLabel" style="WIDTH: 360px; HEIGHT: 33px" colSpan="3">&nbsp;
										<asp:label id="lblStateCode" runat="server" Width="100px" CssClass="tableLabel" Height="22px">State / Province</asp:label><cc1:mstextbox id="txtStateCode" runat="server" Width="139" CssClass="textField" Height="22" MaxLength="10"
											TextMaskType="msUpperAlfaNumericWithUnderscore"></cc1:mstextbox>&nbsp;&nbsp;
										<asp:button id="btnStateCode" runat="server" CssClass="searchButton" Text="..." CausesValidation="False"></asp:button></TD>
									<td style="HEIGHT: 33px"></td>
								</tr>
							</TABLE>
						</fieldset>
					</TD>
				</TR>
				<TR>
					<TD style="WIDTH: 351px" vAlign="top">
						<asp:checkbox id="chkShowDetail" tabIndex="25" runat="server" Text="Show Detail" CssClass="normalText"
							Width="376px" Height="14px" AutoPostBack="True"></asp:checkbox>
					</TD>
					<TD style="WIDTH: 400px" vAlign="top">&nbsp;
					</TD>
				</TR>
			</TABLE>
			<asp:label id="Label1" style="Z-INDEX: 102; LEFT: 20px; POSITION: absolute; TOP: 4px" runat="server"
				Width="365px" CssClass="maintitleSize" Height="27px">First Delivery Report</asp:label></TR></TABLE></TR></TABLE><input 
style="Z-INDEX: 106; LEFT: 222px; POSITION: absolute; TOP: 35px" type=hidden 
value="<%=strScrollPosition%>" name=ScrollPosition>
		</form>
	</body>
</HTML>
