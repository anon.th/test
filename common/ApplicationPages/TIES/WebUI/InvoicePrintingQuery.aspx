<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<%@ Page language="c#" Codebehind="InvoicePrintingQuery.aspx.cs" AutoEventWireup="false" Inherits="com.ties.InvoicePrintingQuery" SmartNavigation="false" %>
<%@ Register TagPrefix="cr" Namespace="CrystalDecisions.Web" Assembly="CrystalDecisions.Web, Version=9.2.3300.0, Culture=neutral, PublicKeyToken=692fbea5521e1304" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>InvoicePrintingQuery</title>
		<LINK href="css/Styles.css" type="text/css" rel="stylesheet" name="stylesheet">
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<!--#INCLUDE FILE="msFormValidations.inc"-->
		<script language="javascript" src="Scripts/settingScrollPosition.js"></script>
	</HEAD>
	<body MS_POSITIONING="GridLayout" onclick="GetScrollPosition();" onload="SetScrollPosition();" onunload="window.top.displayBanner.fnCloseAll(0);">
		<form id="InvoicePrintingQuery" method="post" runat="server">
			<asp:button id="btnPrint" style="Z-INDEX: 101; LEFT: 99px; POSITION: absolute; TOP: 48px" runat="server" Width="96px" Text="Print" CssClass="queryButton"></asp:button><asp:button id="btnClear" style="Z-INDEX: 102; LEFT: 24px; POSITION: absolute; TOP: 48px" runat="server" Width="74px" Text="Clear" CssClass="queryButton"></asp:button><asp:label id="lblErrorMessage" style="Z-INDEX: 105; LEFT: 20px; POSITION: absolute; TOP: 80px" runat="server" Width="556px" Height="30px" CssClass="errorMsgColor"></asp:label>
			<TABLE id="tblInvoicePrintingQry" style="Z-INDEX: 104; LEFT: 18px; WIDTH: 484px; POSITION: absolute; TOP: 115px" width="484" border="0" runat="server">
				<TBODY>
					<TR width="100%">
						<TD vAlign="top" width="100%">
							<fieldset><legend><asp:label id="Label2" Runat="server">Print Option</asp:label></legend>
								<TABLE id="tblInvoiceQuery" width="100%" align="left" border="0" runat="server">
									<TR height="14">
										<TD width="5%"></TD>
										<TD width="5%"></TD>
										<TD width="5%"></TD>
										<TD width="5%"></TD>
										<TD width="5%"></TD>
										<TD width="5%"></TD>
										<TD width="5%"></TD>
										<TD width="5%"></TD>
										<TD width="5%"></TD>
										<TD width="5%"></TD>
										<TD width="5%"></TD>
										<TD width="5%"></TD>
										<TD width="5%"></TD>
										<TD width="5%"></TD>
										<TD width="5%"></TD>
										<TD width="5%"></TD>
										<TD width="5%"></TD>
										<TD width="5%"></TD>
										<TD width="5%"></TD>
										<TD width="5%"></TD>
									</TR>
									<TR height="27">
										<TD colSpan="10"><asp:radiobutton id="rbAll" runat="server" Width="242px" Text="Print All un-printed Invoice" Height="22px" CssClass="tableRadioButton" Checked="True" Font-Bold="True" GroupName="PrintOption" AutoPostBack="True"></asp:radiobutton></TD>
										<TD colSpan="10"></TD>
									</TR>
									<TR height="27">
										<TD colSpan="10"><asp:radiobutton id="rbSelective" runat="server" Width="100%" Text="Selective" Height="22px" CssClass="tableRadioButton" Font-Bold="True" GroupName="PrintOption" AutoPostBack="True"></asp:radiobutton></TD>
										<TD colSpan="10"></TD>
									</TR>
									<TR height="8">
										<TD colSpan="20"></TD>
									</TR>
								</TABLE>
							</fieldset>
							<fieldset><legend><asp:label id="Label1" Runat="server">Selective Status</asp:label></legend>
								<table style="WIDTH: 472px; HEIGHT: 191px">
									<TR height="14">
										<TD style="HEIGHT: 20px" colSpan="20"></TD>
									</TR>
									<TR height="27">
										<TD colSpan="6" style="WIDTH: 83px"><asp:label id="lblCustomerType" runat="server" Width="104px" Height="22px" CssClass="tableLabel" Font-Bold="true">Customer Type</asp:label></TD>
										<TD colSpan="4"><asp:radiobutton id="rbAgent" runat="server" Width="100%" Text="Agent" Height="22px" CssClass="tableRadioButton" GroupName="CustomerType" AutoPostBack="True"></asp:radiobutton></TD>
										<TD colSpan="4"><asp:radiobutton id="rbCustomer" runat="server" Width="100%" Text="Customer" Height="22px" CssClass="tableRadioButton" GroupName="CustomerType" AutoPostBack="True"></asp:radiobutton></TD>
										<TD colSpan="4"><asp:radiobutton id="rbBoth" runat="server" Width="100%" Text="Both" Height="22px" CssClass="tableRadioButton" Checked="True" GroupName="CustomerType" AutoPostBack="True"></asp:radiobutton></TD>
										<TD colSpan="2" style="WIDTH: 1px"></TD>
									</TR>
									<TR height="27">
										<TD colSpan="6" style="WIDTH: 83px"><asp:label id="lblCustomerCode" runat="server" Width="112px" Height="22px" CssClass="tableLabel">Customer Code</asp:label></TD>
										<TD colSpan="8"><cc1:mstextbox id="txtCustomerCode" runat="server" Width="100%" CssClass="textField" TextMaskType="msUpperAlfaNumericWithUnderscore" MaxLength="20"></cc1:mstextbox></TD>
										<TD colSpan="1"><asp:button id="btnCustomerCode" runat="server" Text="..." CssClass="queryButton" CausesValidation="False"></asp:button></TD>
										<TD colSpan="5" style="WIDTH: 36px"></TD>
									</TR>
									<TR height="27">
										<TD colSpan="6" style="WIDTH: 83px"><asp:label id="lblCustomerName" runat="server" Width="112px" Height="22px" CssClass="tableLabel">Customer Name</asp:label></TD>
										<TD colSpan="14" style="WIDTH: 279px"><cc1:mstextbox id="txtCustomerName" runat="server" Width="100%" CssClass="textField" TextMaskType="msUpperAlfaNumericWithUnderscore" MaxLength="100"></cc1:mstextbox></TD>
									</TR>
									<TR height="27">
										<TD colSpan="6" style="WIDTH: 83px"><asp:label id="lblInvoiceNo" runat="server" Width="112px" Height="22px" CssClass="tableLabel" Font-Bold="true">Invoice No</asp:label></TD>
										<TD colSpan="2"><asp:label id="lblFromInvNo" runat="server" Width="100%" Height="22px" CssClass="tableLabel">From</asp:label></TD>
										<TD colSpan="5"><cc1:mstextbox id="txtFromInvNo" runat="server" Width="86px" CssClass="textField" TextMaskType="msUpperAlfaNumericWithUnderscore" MaxLength="20"></cc1:mstextbox></TD>
										<TD align="right" colSpan="2"><asp:label id="lblToInvNo" runat="server" Width="22px" Height="22px" CssClass="tableLabel">To</asp:label></TD>
										<TD colSpan="5" style="WIDTH: 36px"><cc1:mstextbox id="txtToInvNo" runat="server" Width="88px" CssClass="textField" TextMaskType="msUpperAlfaNumericWithUnderscore" MaxLength="20"></cc1:mstextbox></TD>
									</TR>
									<TR height="27">
										<TD colSpan="6" style="WIDTH: 83px"><asp:label id="lblInvoiceDate" runat="server" Width="112px" Height="22px" CssClass="tableLabel" Font-Bold="true">Invoice Date</asp:label></TD>
										<TD colSpan="2"><asp:label id="lblFromInvDate" runat="server" Width="100%" Height="22px" CssClass="tableLabel">From</asp:label></TD>
										<TD colSpan="5"><cc1:mstextbox id="txtFromInvDate" runat="server" Width="85px" CssClass="textField" TextMaskType="msDate" MaxLength="10" TextMaskString="99/99/9999"></cc1:mstextbox></TD>
										<TD align="right" colSpan="2"><asp:label id="lblToInvDate" runat="server" Width="19px" Height="22px" CssClass="tableLabel">To</asp:label></TD>
										<TD colSpan="5" style="WIDTH: 36px"><cc1:mstextbox id="txtToInvDate" runat="server" Width="88px" CssClass="textField" TextMaskType="msDate" MaxLength="10" TextMaskString="99/99/9999"></cc1:mstextbox></TD>
									</TR>
									<TR height="8">
										<TD colSpan="20"></TD>
									</TR>
								</table>
							</fieldset>
						</TD>
					</TR>
				</TBODY>
			</TABLE>
			<asp:label id="lblTitle" style="Z-INDEX: 103; LEFT: 20px; POSITION: absolute; TOP: 9px" runat="server" Width="558px" Height="32px" CssClass="maintitleSize">Invoice Printing</asp:label>
			<input name="ScrollPosition" type="hidden" value="<%=strScrollPosition%>">
		</form>
	</body>
</HTML>
