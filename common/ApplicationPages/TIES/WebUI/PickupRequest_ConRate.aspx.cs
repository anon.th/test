using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.ties.DAL;
using com.common.classes;
using com.common.util;
using com.ties.classes;
using com.ties.BAL;
using com.common.applicationpages;
using System.Text;
using System.Globalization;
using com.common.DAL;

namespace TIES.WebUI
{
	/// <summary>
	/// Summary description for PickupRequest_ConRate.
	/// </summary>
	public class PickupRequest_ConRate : BasePopupPage
	{
		protected System.Web.UI.HtmlControls.HtmlTable Table1;
		protected System.Web.UI.WebControls.Label Label1;
		protected System.Web.UI.WebControls.Label Label2;
		protected System.Web.UI.WebControls.Label Label3;
		protected System.Web.UI.WebControls.Label Label9;
		protected System.Web.UI.WebControls.Label Label11;
		protected com.common.util.msTextBox txtHeader_DimWeight;
		protected System.Web.UI.WebControls.Button btnOK;
		protected System.Web.UI.WebControls.Button btnCancel;
		protected System.Web.UI.WebControls.Button btnInsertConsignment;
		protected System.Web.UI.WebControls.Button btnInsertPackage;
		protected System.Web.UI.WebControls.DataGrid dgPackage;
		protected System.Web.UI.WebControls.DataGrid dgVAS;
		protected System.Web.UI.WebControls.Button btnInsertVAS;
		protected System.Web.UI.WebControls.DataGrid dgConsignment;
		protected System.Web.UI.WebControls.Label lblErrorMsg;
		protected System.Web.UI.WebControls.Label lblConf;
		protected System.Web.UI.WebControls.Label lblConfirmMsg;
		protected System.Web.UI.WebControls.Button btnToSaveChanges;
		protected System.Web.UI.WebControls.Button btnNotToSave;
		protected System.Web.UI.WebControls.Button btnToCancel;
		protected System.Web.UI.HtmlControls.HtmlGenericControl divMain;
		protected System.Web.UI.HtmlControls.HtmlGenericControl divConfirmHoliday;
		protected System.Web.UI.HtmlControls.HtmlTable Table2;
		public string strCloseWindowStatus;

		String appID = null;
		String enterpriseID = null;
		String strBookingNo = null;
		DataTable dtConsignment = null;		
		DataTable dtVAS = null;
		DataTable dtPackage = null;
		DataTable dtPkgTmp = null;
		DataTable dtVASTmp = null;
		String strSendZipCode = null;
		String strDestZipCode = null;
		decimal totDimWt = 0;
		decimal totActWt = 0;
		decimal totSurcharge = 0;
		protected com.common.util.msTextBox txtHeader_TotalPkgs;
		protected com.common.util.msTextBox txtHeader_ActualWeight;
		protected com.common.util.msTextBox txtHeader_ChgWeight;
		protected com.common.util.msTextBox txtHeader_TotalCharge;
		protected com.common.util.msTextBox txtHeader_FreightCharge;
		protected com.common.util.msTextBox txtHeader_InsSurch;
		protected com.common.util.msTextBox txtHeader_OtherSurch;
		protected com.common.util.msTextBox txtHeader_VASSurch;
		protected com.common.util.msTextBox txtHeader_ESASurch;
		protected com.common.util.msTextBox TextBox1;
		int totPkgs = 0;
	
		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings,Page.Session);
			appID = utility.GetAppID();
			enterpriseID = utility.GetEnterpriseID();
			strBookingNo = null;
//			if(!this.User.IsInRole("MANAGER"))
//			{
//				dgConsignment.Columns[1].Visible = false;
//				dgPackage.Columns[0].Visible = false;
//				dgVAS.Columns[0].Visible = false;
//			}
			
			if(Request.Params["BOOKINGID"].ToString() != "")
				strBookingNo = Request.Params["BOOKINGID"].ToString();

			if(!Page.IsPostBack)
			{				
				btnInsertPackage.Enabled = false;
				btnInsertVAS.Enabled = false;
				divConfirmHoliday.Visible = false;
				ViewState["InsertdgConsignment"] = false;
				ViewState["InsertdgPackage"] = false;
				ViewState["InsertdgVAS"] = false;
			
				if(Session["SESSION_DTCon"] == null)
				{
					dtConsignment = PRBMgrDAL.ReadPickupConsignment(appID,enterpriseID,strBookingNo,null);
					dtPackage = PRBMgrDAL.ReadPickupConsignmentPkg(appID,enterpriseID,strBookingNo,null);
					dtVAS = PRBMgrDAL.ReadPickupConsignmentVAS(appID,enterpriseID,strBookingNo,null);

					ViewState["VIEWSTATE_DT"] = dtConsignment;
					ViewState["VIEWSTATE_DT2"] = dtPackage;
					ViewState["VIEWSTATE_DT3"] = dtVAS;
					
					dtPkgTmp = PRBMgrDAL.ReadPickupConsignmentPkg(appID,enterpriseID,null,null);
					dtVASTmp = PRBMgrDAL.ReadPickupConsignmentVAS(appID,enterpriseID,null,null);
				}
				else
				{
					dtConsignment = (DataTable)Session["SESSION_DTCon"];
					dtPackage = (DataTable)Session["SESSION_DTPkg"];
					dtVAS = (DataTable)Session["SESSION_DTVAS"];
					ViewState["VIEWSTATE_DT"] = dtConsignment;
					ViewState["VIEWSTATE_DT2"] = dtPackage;
					ViewState["VIEWSTATE_DT3"] = dtVAS;

					dtPkgTmp = PRBMgrDAL.ReadPickupConsignmentPkg(appID,enterpriseID,null,null);
					dtVASTmp = PRBMgrDAL.ReadPickupConsignmentVAS(appID,enterpriseID,null,null);
				}

				BindConsignmentGrid();
				BindPkgGrid();
				BindVASGrid();
			}
			else
			{
				dtConsignment = (DataTable)ViewState["VIEWSTATE_DT"];
				dtPackage = (DataTable)ViewState["VIEWSTATE_DT2"];
				dtVAS  = (DataTable)ViewState["VIEWSTATE_DT3"];
				
				if(ViewState["VIEWSTATE_DT2Tmp"] != null)
					dtPkgTmp = (DataTable)ViewState["VIEWSTATE_DT2Tmp"];
				else
					dtPkgTmp = PRBMgrDAL.ReadPickupConsignmentPkg(appID,enterpriseID,null,null);

				if(ViewState["VIEWSTATE_DT3Tmp"] != null)
					dtVASTmp = (DataTable)ViewState["VIEWSTATE_DT3Tmp"];
				else
					dtVASTmp = PRBMgrDAL.ReadPickupConsignmentVAS(appID,enterpriseID,null,null);
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// 		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
			this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
			this.dgConsignment.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.dgConsignment_ItemDataBound);
			this.btnInsertConsignment.Click += new System.EventHandler(this.btnInsertConsignment_Click);
			this.btnInsertPackage.Click += new System.EventHandler(this.btnInsertPackage_Click);
			this.btnInsertVAS.Click += new System.EventHandler(this.btnInsertVAS_Click);
			this.btnToSaveChanges.Click += new System.EventHandler(this.btnToSaveChanges_Click);
			this.btnNotToSave.Click += new System.EventHandler(this.btnNotToSave_Click);
			this.btnToCancel.Click += new System.EventHandler(this.btnToCancel_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		#region Web Form Event
		private void btnCancel_Click(object sender, System.EventArgs e)
		{
			Session.Remove("SS_Customer");

			String sScript = "";
			sScript += "<script language=javascript>";
			sScript += "  window.close();";
			sScript += "</script>";
			Response.Write(sScript);
		}
		private void btnInsertConsignment_Click(object sender, System.EventArgs e)
		{
			if(dgConsignment.EditItemIndex != -1)
				return;
			if(dgPackage.EditItemIndex != -1)
				return;
			if(dgVAS.EditItemIndex != -1)
				return;

			if((bool)ViewState["InsertdgConsignment"] == false && (bool)ViewState["InsertdgPackage"]==false)
			{
				this.lblErrorMsg.Text = "";
				btnInsertPackage.Enabled = false;
				btnInsertVAS.Enabled = false;

				DataSet dsServiceCode = LoadService(-1,null);
				dtPackage = PRBMgrDAL.ReadPickupConsignmentPkg(appID,enterpriseID,null,null);
				dtVAS = PRBMgrDAL.ReadPickupConsignmentVAS(appID,enterpriseID,null,null);
				BindPkgGrid();
				BindVASGrid();

				ViewState["InsertdgConsignment"] = true;
				AddRowInConsignmentGrid();

				DataRow drNew = dtConsignment.Rows[dtConsignment.Rows.Count-1];

				if (dtConsignment.Rows.Count > 1)
				{
					object maxConSeqNo = dtConsignment.Compute("max(ConsignmentSeqNo)",null);
					if(maxConSeqNo!=null && maxConSeqNo!=DBNull.Value)
						drNew["ConsignmentSeqNo"] = 1+ int.Parse(maxConSeqNo.ToString());
				}
				else
				{
					drNew["ConsignmentSeqNo"] = dtConsignment.Rows.Count;
				}
				
				ButtonColumn bt = (ButtonColumn)dgConsignment.Columns[0];
				bt.Text="";
				
				if(dtConsignment.Rows.Count > (dgConsignment.PageSize*dgConsignment.PageCount))
				{
					BindConsignmentGrid();
					dgConsignment.CurrentPageIndex=dgConsignment.PageCount-1;
				}

				dgConsignment.EditItemIndex = (dtConsignment.Rows.Count - 1)-(dgConsignment.PageSize*(dgConsignment.PageCount-1));
				dgConsignment.SelectedIndex = (dtConsignment.Rows.Count - 1)-(dgConsignment.PageSize*(dgConsignment.PageCount-1));
				dgConsignment.CurrentPageIndex = dgConsignment.PageCount - 1; // 2009 -12-08 by poo, must be index of page.

				BindConsignmentGrid();
				dtPkgTmp = PRBMgrDAL.ReadPickupConsignmentPkg(appID,enterpriseID,null,null);
				dtVASTmp = PRBMgrDAL.ReadPickupConsignmentVAS(appID,enterpriseID,null,null);

				dgPackage.CurrentPageIndex = 0; // set default page = 0 when create new consignment , by poo 2009-12-08
				BindPkgGrid();
				dgVAS.CurrentPageIndex = 0; // set default page = 0 when create new consignment , by poo 2009-12-08
				BindVASGrid();

				msTextBox dgConsignmenttxtRecipientPostCode = (msTextBox)dgConsignment.Items[dgConsignment.EditItemIndex].FindControl("dgConsignmenttxtRecipientPostCode");
				Page.RegisterStartupScript("SetFocus", "<script>document.getElementById('" + dgConsignmenttxtRecipientPostCode.ClientID + "').focus();</script>");
			}
		}
		private void btnInsertPackage_Click(object sender, System.EventArgs e)
		{
			if(dgPackage.EditItemIndex != -1)
				return;
			if(dgVAS.EditItemIndex != -1)
				return;

			if((bool)ViewState["InsertdgPackage"] == false)
			{
				this.lblErrorMsg.Text = "";
				ViewState["InsertdgPackage"] = true;
				dtPackage = (DataTable)ViewState["VIEWSTATE_DT2"];
				dtPkgTmp = dtPackage.Clone();
				dtPkgTmp.Rows.Clear();

				DataRow[] drPRPkgTemp = dtPackage.Select("consignmentSeqNo='"+(String)Session["consignmentSeq"]+"'");

				if(drPRPkgTemp.Length>0)
				{
					for (int i=0;i<drPRPkgTemp.Length;i++)
					{
						dtPkgTmp.ImportRow(drPRPkgTemp[i]);
					}
				}

				ViewState["VIEWSTATE_DT2Tmp"] = dtPkgTmp;

				AddRowInPkgGrid();

				DataRow drNew = dtPkgTmp.Rows[dtPkgTmp.Rows.Count-1];
				// check new MPS_NO , by poo 2009-12-08
				if (dtPkgTmp.Rows.Count > 0)
				{
					object maxPkgMpsNo = dtPkgTmp.Compute("max(Pkg_MPSNo)",null);
					if(maxPkgMpsNo != null && maxPkgMpsNo != DBNull.Value)
						drNew["Pkg_MPSNo"] = 1+ int.Parse(maxPkgMpsNo.ToString());
				}
				else
				{
					drNew["Pkg_MPSNo"] = dtPkgTmp.Rows.Count;
				} // end check
				drNew["pkg_volume"] = 1;

				if(dtPkgTmp.Rows.Count > (dgPackage.PageSize*dgPackage.PageCount))
				{
					BindPkgGrid();
					dgPackage.CurrentPageIndex=dgPackage.PageCount-1;
				}

				dgPackage.EditItemIndex = (dtPkgTmp.Rows.Count - 1)-(dgPackage.PageSize*(dgPackage.PageCount-1));
				dgPackage.CurrentPageIndex = dgPackage.PageCount - 1; // 2009 -12-08 by poo, must be index of page.

				BindPkgGrid();

				msTextBox dgPackagetxtLength = (msTextBox)dgPackage.Items[dgPackage.EditItemIndex].FindControl("dgPackagetxtLength");
				Page.RegisterStartupScript("SetFocus", "<script>document.getElementById('" + dgPackagetxtLength.ClientID + "').focus();</script>");
				Page.RegisterStartupScript("SetSelect", "<script>document.getElementById('" + dgPackagetxtLength.ClientID + "').select();</script>");
			}
		}
		private void btnInsertVAS_Click(object sender, System.EventArgs e)
		{

			if(dgConsignment.EditItemIndex != -1)
				return;
			if(dgPackage.EditItemIndex != -1)
				return;
			if(dgVAS.EditItemIndex != -1)
				return;

			if(dtPackage.Rows.Count>0)
			{
				this.lblErrorMsg.Text = "";
				GetGridValuesTotal();

				if((bool)ViewState["InsertdgVAS"] == false)
				{
					this.lblErrorMsg.Text = "";
					ViewState["InsertdgVAS"] = true;
					dtVAS = (DataTable)ViewState["VIEWSTATE_DT3"];
					dtVASTmp = dtVAS.Clone();
					dtVASTmp.Rows.Clear();

					DataRow[] drPRVASTemp = dtVAS.Select("consignmentSeqNo='"+(String)Session["consignmentSeq"]+"'");

					if(drPRVASTemp.Length>0)
					{
						for (int i=0;i<drPRVASTemp.Length;i++)
						{
							dtVASTmp.ImportRow(drPRVASTemp[i]);
						}
					}

					ViewState["VIEWSTATE_DT3Tmp"] = dtVASTmp;
					AddRowInVASGrid();
					if(dtVASTmp.Rows.Count > (dgVAS.PageSize*dgVAS.PageCount))
					{
						BindVASGrid();
						dgVAS.CurrentPageIndex=dgVAS.PageCount-1;
					}
					dgVAS.EditItemIndex = (dtVASTmp.Rows.Count - 1)-(dgVAS.PageSize*(dgVAS.PageCount-1));
					dgVAS.CurrentPageIndex = dgVAS.PageCount - 1; // 2009 -12-08 by poo, must be index of page.
					BindVASGrid();
				}
			}
		}
	
		private void btnNotToSave_Click(object sender, System.EventArgs e)
		{
			if (!(bool)ViewState["IsCalculatedDelVAS"])
			{
				divConfirmHoliday.Visible = false;
				divMain.Visible=true;
			}
			else
			{
			this.lblErrorMsg.Text = "";
			System.TimeSpan duration = new System.TimeSpan(1, 0, 0, 0);
			DateTime tmpEstDelDate = DateTime.ParseExact((String)ViewState["estDelDate"],"dd/MM/yyyy HH:mm",null);
			tmpEstDelDate = tmpEstDelDate.Add(duration);

			bool isHoliday=false;
			isHoliday = TIESUtility.IsDayHoliday(appID,enterpriseID,tmpEstDelDate);

			if(tmpEstDelDate.DayOfWeek == System.DayOfWeek.Saturday)
			{
//				lblConfirmMsg.Text = tmpEstDelDate.ToString("dd/MM/yyyy HH:mm") + " Weekend / Public Holiday Delivery Service is available. Weekend / Public Holiday Surcharge will be imposed. Do you want to continue?";
				lblConfirmMsg.Text = "Would the customer accept a surcharge for delivery on Saturday?";
				divConfirmHoliday.Visible = true;
				divMain.Visible=false;
				ViewState["estDelDate"] = tmpEstDelDate.ToString("dd/MM/yyyy HH:mm");
				ViewState["vasSurCharge"] = "SATDEL";
			}
			else if(tmpEstDelDate.DayOfWeek == System.DayOfWeek.Sunday)
			{
				lblConfirmMsg.Text = "Would the customer accept a surcharge for delivery on Sunday?";
				divConfirmHoliday.Visible = true;
				divMain.Visible=false;
				ViewState["estDelDate"] = tmpEstDelDate.ToString("dd/MM/yyyy HH:mm");
				ViewState["vasSurCharge"] = "SUNDEL";
			}
			else if(isHoliday)
			{
				lblConfirmMsg.Text = "Would the customer accept a surcharge for delivery on Public Holiday?";
				divConfirmHoliday.Visible = true;
				divMain.Visible=false;
				ViewState["estDelDate"] = tmpEstDelDate.ToString("dd/MM/yyyy HH:mm");
				ViewState["vasSurCharge"] = "PUBDEL";
			}
			else
			{
				divConfirmHoliday.Visible = false;
				divMain.Visible=true;
				DataRow drCurrent = dtConsignment.Rows[(int)ViewState["rowEvent"]];
				drCurrent["EstDelDate"] = tmpEstDelDate;
				DataRow[] drPRVAS = dtVAS.Select("consignmentSeqNo='"+drCurrent["ConsignmentSeqNo"].ToString()+
					"'and (vas_code='SATDEL' or vas_code='SUNDEL' or vas_code='PUBDEL')");
				drCurrent["VasSurch"] = 0;
				if(drPRVAS.Length>0)
				{
					for (int n=0;n<drPRVAS.Length;n++)
					{
						dtVAS.Rows.Remove(drPRVAS[n]);
					}
				}
				DataRow[] drVasTmp = dtVAS.Select("consignmentSeqNo='"+drCurrent["ConsignmentSeqNo"].ToString()+"'");
				dtVASTmp = dtVAS.Clone();
				if(drVasTmp.Length>0)
				{
					for (int n=0;n<drVasTmp.Length;n++)
					{
						dtVASTmp.ImportRow(drVasTmp[n]);
					}
				}
				if(dtVASTmp!=null)
				{
					object sumSurCharge = dtVASTmp.Compute("sum(vas_surcharge)",null);
					if(sumSurCharge!=null && sumSurCharge!=DBNull.Value) drCurrent["VasSurch"] = (decimal)sumSurCharge;
				}

				Customer customer = new Customer();
				customer.Populate(appID,enterpriseID, (String)Session["PickupCustomerID"]);

				DomesticShipmentMgrBAL domesticBAL = new DomesticShipmentMgrBAL();
				decimal decInsSurchrg = domesticBAL.ComputeInsSurchargeByCust(appID,enterpriseID,(Decimal)drCurrent[6],(String)Session["PickupCustomerID"],customer);
				drCurrent[14] = decInsSurchrg;

				drCurrent["TotalCharge"] = (decimal)drCurrent["FreightCharge"]+(decimal)drCurrent["InsSurch"]+(decimal)drCurrent["OtherSurch"]+
					(decimal)drCurrent["VasSurch"]+(decimal)drCurrent["ESASurch"];

				if((decimal)drCurrent["FreightCharge"] == 0)
				{
					lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"NO_RECORD_FOUND_RATES",utility.GetUserCulture());
					return;
				}

				ViewState["VIEWSTATE_DT3Tmp"] = dtVASTmp;
				BindVASGrid();
				ViewState["InsertdgConsignment"] = false;
				dgConsignment.EditItemIndex = -1;
				ButtonColumn bt = (ButtonColumn)dgConsignment.Columns[0];
				bt.Text="<IMG SRC='images/butt-select.gif' border=0 >";
				BindConsignmentGrid();
			}
		}
		}
		private void btnToSaveChanges_Click(object sender, System.EventArgs e)
		{
			this.lblErrorMsg.Text = "";
			String strVasCode = null;
			decimal decQuotSurchrg = 0;
			String strQuotVASCode = null;
			String strVasDesc = null;
			DataRow drCurrent = dtConsignment.Rows[(int)ViewState["rowEvent"]];

			if(ViewState["vasSurCharge"] != null)
			{
				strVasCode = (String)ViewState["vasSurCharge"];
				VAS vas = new VAS();
				vas.PopulateCustVas(appID,enterpriseID,strVasCode,(String)Session["PickupCustomerID"]);
				if(vas.VASCode == null)
				{
					vas.Populate(appID,enterpriseID,strVasCode);
				}
				decQuotSurchrg = vas.Surcharge;
				strQuotVASCode = vas.VASCode;
				strVasDesc = vas.VasDescription;

				dtVAS = (DataTable)ViewState["VIEWSTATE_DT3"];
				DataRow[] drPRVAS = dtVAS.Select("consignmentSeqNo='"+drCurrent["ConsignmentSeqNo"].ToString()+
					"'and (vas_code='SATDEL' or vas_code='SUNDEL' or vas_code='PUBDEL')");
				drCurrent["VasSurch"] = 0;
				if(drPRVAS.Length>0)
				{
					for (int n=0;n<drPRVAS.Length;n++)
					{
						dtVAS.Rows.Remove(drPRVAS[n]);
					}
				}
				PRBMgrDAL.AddNewRowInVASDT(dtVAS);
				int cnt = dtVAS.Rows.Count-1;
				DataRow dr = dtVAS.Rows[cnt];
				dr["applicationid"] = appID;
				dr["enterpriseid"] = enterpriseID;
				if(strBookingNo != null)
					dr["booking_no"] = strBookingNo;
				else
					dr["booking_no"] = System.DBNull.Value;

				dr["consignmentSeqNo"] = drCurrent["ConsignmentSeqNo"].ToString();
				dr["vas_code"] = vas.VASCode;
				dr["vas_description"] = vas.VasDescription;
				dr["vas_surcharge"] = vas.Surcharge;
				DataRow[] drVasTmp = dtVAS.Select("consignmentSeqNo='"+drCurrent["ConsignmentSeqNo"].ToString()+"'");
				dtVASTmp = dtVAS.Clone();
				if(drVasTmp.Length>0)
				{
					for (int n=0;n<drVasTmp.Length;n++)
					{
						dtVASTmp.ImportRow(drVasTmp[n]);
					}
				}
				if(dtVASTmp!=null)
				{
					object sumSurCharge = dtVASTmp.Compute("sum(vas_surcharge)",null);
					if(sumSurCharge!=null && sumSurCharge!=DBNull.Value) drCurrent["VasSurch"] = (decimal)sumSurCharge;
				}
				ViewState["VIEWSTATE_DT3Tmp"] = dtVASTmp;
				BindPkgGrid();
				BindVASGrid();
				
				DateTime tmpEstDelDate = DateTime.ParseExact((String)ViewState["estDelDate"],"dd/MM/yyyy HH:mm",null);
				divConfirmHoliday.Visible = false;
				divMain.Visible=true;		
				ViewState["InsertdgConsignment"] = false;

				Customer customer = new Customer();
				customer.Populate(appID,enterpriseID, (String)Session["PickupCustomerID"]);

				DomesticShipmentMgrBAL domesticBAL = new DomesticShipmentMgrBAL();
				decimal decInsSurchrg = domesticBAL.ComputeInsSurchargeByCust(appID,enterpriseID,(Decimal)drCurrent[6],(String)Session["PickupCustomerID"],customer);
				drCurrent[14] = decInsSurchrg;

				drCurrent["TotalCharge"] = (decimal)drCurrent["FreightCharge"]+(decimal)drCurrent["InsSurch"]+(decimal)drCurrent["OtherSurch"]+
					(decimal)drCurrent["VasSurch"]+(decimal)drCurrent["ESASurch"];
				drCurrent["EstDelDate"] = tmpEstDelDate;

				if ((bool)ViewState["IsCalculatedDelVAS"])
				{
					dgConsignment.EditItemIndex = -1;
				}
				ButtonColumn bt = (ButtonColumn)dgConsignment.Columns[0];
				bt.Text="<IMG SRC='images/butt-select.gif' border=0>";
				BindConsignmentGrid();
			}

			if (!(bool)ViewState["IsCalculatedDelVAS"])
			{
				msTextBox dgConsignmenttxtRecipientPostCode = (msTextBox)dgConsignment.Items[dgConsignment.EditItemIndex].FindControl("dgConsignmenttxtRecipientPostCode");
				DropDownList dgConsignmentddlServiceType = (DropDownList)dgConsignment.Items[dgConsignment.EditItemIndex].FindControl("dgConsignmentddlServiceType");

				//	Calculate Estimate Delivery Date
				DomesticShipmentMgrBAL domesticMgrBAL = new DomesticShipmentMgrBAL();
				String estDelDate = domesticMgrBAL.CalcDlvryDtTime(appID,enterpriseID,System.DateTime.ParseExact((String)Session["Estpickupdatetime"],"dd/MM/yyyy HH:mm",null),dgConsignmentddlServiceType.SelectedItem.Value,dgConsignmenttxtRecipientPostCode.Text.Trim());

				// Validate Holiday Date
				bool isHoliday=false;
				isHoliday = TIESUtility.IsDayHoliday(appID,enterpriseID,DateTime.ParseExact(estDelDate,"dd/MM/yyyy HH:mm",null));
				DateTime tmpEstDate = DateTime.ParseExact(estDelDate,"dd/MM/yyyy HH:mm",null);
				
				if(tmpEstDate.DayOfWeek == System.DayOfWeek.Saturday)	// validate saturday.
				{
					lblConfirmMsg.Text = "Would the customer accept a surcharge for delivery on Saturday?";
					divConfirmHoliday.Visible = true;
					divMain.Visible=false;
					ViewState["estDelDate"] = estDelDate;
					ViewState["vasSurCharge"] = "SATDEL";
				}
				else if(tmpEstDate.DayOfWeek == System.DayOfWeek.Sunday) // validate sunday.
				{
					lblConfirmMsg.Text = "Would the customer accept a surcharge for delivery on Sunday?";
					divConfirmHoliday.Visible = true;
					divMain.Visible=false;
					ViewState["estDelDate"] = estDelDate;
					ViewState["vasSurCharge"] = "SUNDEL";
				}
				else if(isHoliday) // holiday
				{
					lblConfirmMsg.Text = "Would the customer accept a surcharge for delivery on Public Holiday?";
					divConfirmHoliday.Visible = true;
					divMain.Visible=false;
					ViewState["estDelDate"] = estDelDate;
					ViewState["vasSurCharge"] = "PUBDEL";
				}
				else
				{
					drCurrent["EstDelDate"] = DateTime.ParseExact(estDelDate,"dd/MM/yyyy HH:mm",null);

					DataRow[] drPRVAS = dtVAS.Select("consignmentSeqNo='"+drCurrent["ConsignmentSeqNo"].ToString()+
						"'and (vas_code='SATDEL' or vas_code='SUNDEL' or vas_code='PUBDEL')");
					if(drPRVAS.Length>0)
					{
						for (int n=0;n<drPRVAS.Length;n++)
						{
							dtVAS.Rows.Remove(drPRVAS[n]);
						}
					}
					DataRow[] drVasTmp = dtVAS.Select("consignmentSeqNo='"+drCurrent["ConsignmentSeqNo"].ToString()+"'");
					dtVASTmp = dtVAS.Clone();
					if(drVasTmp.Length>0)
					{
						for (int n=0;n<drVasTmp.Length;n++)
						{
							dtVASTmp.ImportRow(drVasTmp[n]);
						}
					}
					if(dtVASTmp!=null)
					{
						object sumSurCharge = dtVASTmp.Compute("sum(vas_surcharge)",null);
						if(sumSurCharge!=null && sumSurCharge!=DBNull.Value) drCurrent["VasSurch"] = (decimal)sumSurCharge;
					}
					
					ViewState["VIEWSTATE_DT3Tmp"] = dtVASTmp;
					BindVASGrid();

					dgConsignment.SelectedIndex = (int)ViewState["eItemIndex"];
					drCurrent = UpdateConsignment(drCurrent);

					drCurrent["TotalCharge"] = (decimal)drCurrent["FreightCharge"]+(decimal)drCurrent["InsSurch"]+(decimal)drCurrent["OtherSurch"]+
						(decimal)drCurrent["VasSurch"]+(decimal)drCurrent["ESASurch"];

				dgConsignment.EditItemIndex = -1;
				ButtonColumn bt = (ButtonColumn)dgConsignment.Columns[0];
				bt.Text="<IMG SRC='images/butt-select.gif' border=0>";
				BindConsignmentGrid();
			}

				ViewState["IsCalculatedDelVAS"] = true;
			}
		}
		private void btnToCancel_Click(object sender, System.EventArgs e)
		{
			divConfirmHoliday.Visible = false;
			divMain.Visible=true;
		}

		//----------------------------------- Consignment----------------------------------------------------------------
		public void dgConsignment_Update(object sender, DataGridCommandEventArgs e)
		{
			this.lblErrorMsg.Text = "";
//			ViewState["InsertdgConsignment"] = false;
			decimal dDeclaredValue = 0;
			bool IsServiceAvail = false;
			int cnt = 0;

			try
			{
				msTextBox dgConsignmenttxtSeqno = (msTextBox)e.Item.FindControl("dgConsignmenttxtSeqno");				
				int k = (e.Item.ItemIndex+((dgConsignment.CurrentPageIndex)*dgConsignment.PageSize));
				DataRow drCurrent = dtConsignment.Rows[k];
				ViewState["eItemIndex"] = e.Item.ItemIndex;

				if(dgConsignmenttxtSeqno != null)
				{
					Session["consignmentSeq"] = dgConsignmenttxtSeqno.Text;	
					cnt = dtConsignment.Rows.Count;

					for(int i=0;i<cnt;i++)
					{
						DataRow dr = dtConsignment.Rows[i];
						if(i!=k)
						{
							if(dr["ConsignmentSeqNo"].ToString().Trim() == dgConsignmenttxtSeqno.Text)
							{
								lblErrorMsg.Text = "Duplicated Consignment Seq No.";
								return;
							}
						}
					}
					drCurrent["ConsignmentSeqNo"] = dgConsignmenttxtSeqno.Text;

				}
				msTextBox dgConsignmenttxtRecipientPostCode = (msTextBox)e.Item.FindControl("dgConsignmenttxtRecipientPostCode");

				// get service code selected.
				DropDownList dgConsignmentddlServiceType = (DropDownList)e.Item.FindControl("dgConsignmentddlServiceType");
				if(dgConsignmentddlServiceType != null)
				{
//					DataRow drCurrent = dtConsignment.Rows[k];
					
					if(dgConsignmentddlServiceType.Items.Count > 0)
					{
						drCurrent["ServiceType"] = dgConsignmentddlServiceType.SelectedItem.Value;
					}
					else
					{
						lblErrorMsg.Text = "Invalid Recipient Postal Code";  // end poo
						return;
					}
				}

				Zipcode zipCode = new Zipcode();
				Enterprise enterprise = new Enterprise();
				enterprise.Populate(appID,enterpriseID);
				String strPayerCountry = enterprise.Country;
				zipCode.Populate(appID,enterpriseID,strPayerCountry,dgConsignmenttxtRecipientPostCode.Text.Trim());

				if(zipCode.StateName == null)
				{
					lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"NO_CUST_ZIP",utility.GetUserCulture());
					return;
				}
				else
				{
					if(dgConsignmenttxtRecipientPostCode != null)
					{
//						DataRow drCurrent = dtConsignment.Rows[k];
						drCurrent["RecPostalCode"] = dgConsignmenttxtRecipientPostCode.Text;
					}
				}
				// verify service Code.
				IsServiceAvail = Service.IsAvailable(appID,enterpriseID,dgConsignmentddlServiceType.SelectedItem.Value,(String)Session["SenderPostalCode"], dgConsignmenttxtRecipientPostCode.Text.Trim());
				if(!IsServiceAvail)
				{
					String strErrorMsg = "Service type is not available";
					lblErrorMsg.Text = strErrorMsg;
					return;
				}

				msTextBox dgConsignmenttxtDeclareValue = (msTextBox)e.Item.FindControl("dgConsignmenttxtDeclareValue");
				if(dgConsignmenttxtDeclareValue.Text != "")
				{
//					DataRow drCurrent = dtConsignment.Rows[k];
					drCurrent["DeclaredValue"] = dgConsignmenttxtDeclareValue.Text;
					dDeclaredValue =  Convert.ToDecimal((dgConsignmenttxtDeclareValue.Text.ToString()));
				}
				else
				{
					// poo 2009-12-04
					lblErrorMsg.Text = "Invalid declare value";
					return;
				}

				msTextBox dgConsignmenttxtCODAmount = (msTextBox)e.Item.FindControl("dgConsignmenttxtCODAmount");
				if(dgConsignmenttxtCODAmount.Text != "")
				{
//					DataRow drCurrent = dtConsignment.Rows[k];
					drCurrent["CODAmount"] = dgConsignmenttxtCODAmount.Text;

					if(Convert.ToDecimal(dgConsignmenttxtCODAmount.Text) != 0)
					{	
						String strVasCode = "COD";
						VAS vas = new VAS();
						vas.Populate(appID,enterpriseID,strVasCode);
						decimal decQuotSurchrg = vas.Surcharge;
						String strQuotVASCode = vas.VASCode;
						String strVasDesc = vas.VasDescription;
						decimal tmpCODSurchrg = 0;
						DataSet dscustInfo = SysDataMgrDAL.GetCustInforByCustID(appID,enterpriseID, 
							0, 0, (String)Session["PickupCustomerID"].ToString().Trim()).ds;

						//Customer Profile
						if (dscustInfo.Tables[0].Rows.Count > 0)
						{
							if(((dscustInfo.Tables[0].Rows[0]["cod_surcharge_amt"] != null) && 
								(!dscustInfo.Tables[0].Rows[0]["cod_surcharge_amt"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
								(dscustInfo.Tables[0].Rows[0]["cod_surcharge_amt"].ToString() != "")) ||
								((dscustInfo.Tables[0].Rows[0]["cod_surcharge_percent"] != null) && 
								(!dscustInfo.Tables[0].Rows[0]["cod_surcharge_percent"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
								(dscustInfo.Tables[0].Rows[0]["cod_surcharge_percent"].ToString() != "")))
							{
								//Amount
								if((dscustInfo.Tables[0].Rows[0]["cod_surcharge_amt"] != null) && 
									(!dscustInfo.Tables[0].Rows[0]["cod_surcharge_amt"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
									(dscustInfo.Tables[0].Rows[0]["cod_surcharge_amt"].ToString() != ""))
								{
									tmpCODSurchrg = (decimal)dscustInfo.Tables[0].Rows[0]["cod_surcharge_amt"];
								}
								//Percentage
								if((dscustInfo.Tables[0].Rows[0]["cod_surcharge_percent"] != null) && 
									(!dscustInfo.Tables[0].Rows[0]["cod_surcharge_percent"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
									(dscustInfo.Tables[0].Rows[0]["cod_surcharge_percent"].ToString() != ""))
								{
									tmpCODSurchrg = (Convert.ToDecimal(dgConsignmenttxtCODAmount.Text) * ((decimal)dscustInfo.Tables[0].Rows[0]["cod_surcharge_percent"])) / 100;

									//Boundary Min & Max
									if((dscustInfo.Tables[0].Rows[0]["max_cod_surcharge"] != null) && 
										(!dscustInfo.Tables[0].Rows[0]["max_cod_surcharge"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
										(dscustInfo.Tables[0].Rows[0]["max_cod_surcharge"].ToString() != ""))
									{ //��ҡ�˹������ꡫ� ��������Թ��ꡫ� �����仹Ш��
										if(tmpCODSurchrg > Convert.ToDecimal(dscustInfo.Tables[0].Rows[0]["max_cod_surcharge"]))
											tmpCODSurchrg = Convert.ToDecimal(dscustInfo.Tables[0].Rows[0]["max_cod_surcharge"]);
									}

									if((dscustInfo.Tables[0].Rows[0]["min_cod_surcharge"] != null) && 
										(!dscustInfo.Tables[0].Rows[0]["min_cod_surcharge"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
										(dscustInfo.Tables[0].Rows[0]["min_cod_surcharge"].ToString() != ""))
									{//����ѹ���¡����Թ ������Թ仹Ш��
										if(tmpCODSurchrg < Convert.ToDecimal(dscustInfo.Tables[0].Rows[0]["min_cod_surcharge"]))
											tmpCODSurchrg = Convert.ToDecimal(dscustInfo.Tables[0].Rows[0]["min_cod_surcharge"]);
									}
									//Rounding
									tmpCODSurchrg = this.EnterpriseRounding(tmpCODSurchrg);
								}
							}
								//Enterprise
							else
							{
								tmpCODSurchrg = vas.Surcharge;
							}
						}
						else
						{
							tmpCODSurchrg = vas.Surcharge;
						}
						//
						dtVAS = (DataTable)ViewState["VIEWSTATE_DT3"];
						DataRow[] drPRVAS = dtVAS.Select("consignmentSeqNo='"+drCurrent["ConsignmentSeqNo"].ToString()+
							"'and vas_code='COD'");
						if(drPRVAS.Length>0)
						{
							for (int n=0;n<drPRVAS.Length;n++)
							{
								dtVAS.Rows.Remove(drPRVAS[n]);
							}
						}
					
						PRBMgrDAL.AddNewRowInVASDT(dtVAS);
						int cntVAS = dtVAS.Rows.Count-1;
						DataRow dr = dtVAS.Rows[cntVAS];
						dr["applicationid"] = appID;
						dr["enterpriseid"] = enterpriseID;
						if(strBookingNo != null)
							dr["booking_no"] = strBookingNo;
						else
							dr["booking_no"] = System.DBNull.Value;

						dr["consignmentSeqNo"] = drCurrent["ConsignmentSeqNo"].ToString();
						dr["vas_code"] = vas.VASCode;
						dr["vas_description"] = vas.VasDescription;
						dr["vas_surcharge"] = tmpCODSurchrg;
						DataRow[] drVasTmp = dtVAS.Select("consignmentSeqNo='"+drCurrent["ConsignmentSeqNo"].ToString()+"'");
						dtVASTmp = dtVAS.Clone();
						if(drVasTmp.Length>0)
						{
							for (int n=0;n<drVasTmp.Length;n++)
							{
								dtVASTmp.ImportRow(drVasTmp[n]);
							}
						}
						
						DataRow drTmp = dtConsignment.Rows[e.Item.ItemIndex];
						drTmp["VasSurch"] = tmpCODSurchrg;
						if(dtVASTmp!=null)
						{
							object sumSurCharge = dtVASTmp.Compute("sum(vas_surcharge)",null);
							if(sumSurCharge!=null && sumSurCharge!=DBNull.Value) drTmp["VasSurch"] = (decimal)sumSurCharge;
						}

						drCurrent = UpdateConsignment(drCurrent);
						
						drCurrent["TotalCharge"] = (decimal)drCurrent["FreightCharge"]+(decimal)drCurrent["InsSurch"]+(decimal)drCurrent["OtherSurch"]+
							(decimal)drCurrent["VasSurch"]+(decimal)drCurrent["ESASurch"];

						ViewState["VIEWSTATE_DT3Tmp"] = dtVASTmp;
						BindVASGrid();
					}
					else
					{
						dtVAS = (DataTable)ViewState["VIEWSTATE_DT3"];
						DataRow[] drPRVAS = dtVAS.Select("consignmentSeqNo='"+drCurrent["ConsignmentSeqNo"].ToString()+
							"'and (vas_code='COD')");
						drCurrent["VasSurch"] = 0;
						if(drPRVAS.Length>0)
						{
							for (int n=0;n<drPRVAS.Length;n++)
							{
								dtVAS.Rows.Remove(drPRVAS[n]);
							}
						}
						if(dtVASTmp!=null)
						{
							object sumSurCharge = dtVASTmp.Compute("sum(vas_surcharge)",null);
							if(sumSurCharge!=null && sumSurCharge!=DBNull.Value) drCurrent["VasSurch"] = (decimal)sumSurCharge;
						}

						// update Consignment
						drCurrent = UpdateConsignment(drCurrent);
						

						drCurrent["TotalCharge"] = (decimal)drCurrent["FreightCharge"]+(decimal)drCurrent["InsSurch"]+(decimal)drCurrent["OtherSurch"]+
							(decimal)drCurrent["VasSurch"]+(decimal)drCurrent["ESASurch"];
					}
				}
				else
				{
					// show error message. By poo , 2009-12-03
					lblErrorMsg.Text = "Invalid COD amount";  // end poo
					return;
				}
//				DataRow drCurrent = dtConsignment.Rows[k];
				if((decimal)drCurrent["FreightCharge"] == 0)
				{
					lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"NO_RECORD_FOUND_RATES",utility.GetUserCulture());
					return;
				}
				if((bool)ViewState["InsertdgConsignment"] == true)
				{
					Session["consignmentSeq"] = dgConsignmenttxtSeqno.Text;
					ViewState["InsertdgPackage"] = false;
					btnInsertPackage_Click(sender,e);
					btnInsertPackage.Enabled = true;
					btnInsertVAS.Enabled = true;
				}

				//  ######################### Check Pickup Date ######################### 
				bool isPickHoliday = false;
				isPickHoliday = TIESUtility.IsDayHoliday(appID,enterpriseID,DateTime.ParseExact((String)Session["Estpickupdatetime"],"dd/MM/yyyy HH:mm",null));
				DateTime tmpEstPickDate = DateTime.ParseExact((String)Session["Estpickupdatetime"],"dd/MM/yyyy HH:mm",null);

				DomesticShipmentMgrBAL domesticMgrBAL = new DomesticShipmentMgrBAL();
				String estDelDate = domesticMgrBAL.CalcDlvryDtTime(appID,enterpriseID,System.DateTime.ParseExact((String)Session["Estpickupdatetime"],"dd/MM/yyyy HH:mm",null),dgConsignmentddlServiceType.SelectedItem.Value,dgConsignmenttxtRecipientPostCode.Text.Trim());

				drCurrent["VasSurch"] = 0;

				if(tmpEstPickDate.DayOfWeek == System.DayOfWeek.Saturday)	// validate pickup saturday.
				{
					lblConfirmMsg.Text = "Would the customer accept a surcharge for pickup on Saturday?";
					divConfirmHoliday.Visible = true;
					divMain.Visible=false;
					ViewState["rowEvent"] = k;
					ViewState["estDelDate"] = estDelDate;
					ViewState["vasSurCharge"] = "SATPUP";

					ViewState["IsCalculatedDelVAS"] = false;
				}
				else if(tmpEstPickDate.DayOfWeek == System.DayOfWeek.Sunday) // validate pickup sunday.
				{
					lblConfirmMsg.Text = "Would the customer accept a surcharge for pickup on Sunday?";
					divConfirmHoliday.Visible = true;
					divMain.Visible=false;
					ViewState["rowEvent"] = k;
					ViewState["estDelDate"] = estDelDate;
					ViewState["vasSurCharge"] = "SUNPUP";

					ViewState["IsCalculatedDelVAS"] = false;
				}
				else if(isPickHoliday) // pickup holiday
				{
					lblConfirmMsg.Text = "Would the customer accept a surcharge for pickup on Public Holiday?";
					divConfirmHoliday.Visible = true;
					divMain.Visible=false;
					ViewState["rowEvent"] = k;
					ViewState["estDelDate"] = estDelDate;
					ViewState["vasSurCharge"] = "PUBPUP";

					ViewState["IsCalculatedDelVAS"] = false;
				}
				else
				{
					DataRow[] drPRVAS = dtVAS.Select("consignmentSeqNo='"+drCurrent["ConsignmentSeqNo"].ToString()+
						"'and (vas_code='SATPUP' or vas_code='SUNPUP' or vas_code='PUBPUP')");
					if(drPRVAS.Length>0)
					{
						for (int n=0;n<drPRVAS.Length;n++)
						{
							dtVAS.Rows.Remove(drPRVAS[n]);
						}
					}

					DataRow[] drVasTmp = dtVAS.Select("consignmentSeqNo='"+drCurrent["ConsignmentSeqNo"].ToString()+"'");
					dtVASTmp = dtVAS.Clone();
					if(drVasTmp.Length>0)
					{
						for (int n=0;n<drVasTmp.Length;n++)
						{
							dtVASTmp.ImportRow(drVasTmp[n]);
						}
					}
					if(dtVASTmp!=null)
					{
						object sumSurCharge = dtVASTmp.Compute("sum(vas_surcharge)",null);
						if(sumSurCharge!=null && sumSurCharge!=DBNull.Value) drCurrent["VasSurch"] = (decimal)sumSurCharge;
					}
					
					ViewState["VIEWSTATE_DT3Tmp"] = dtVASTmp;
					BindVASGrid();
					dgConsignment.SelectedIndex = e.Item.ItemIndex;
					drCurrent = UpdateConsignment(drCurrent);


					drCurrent["TotalCharge"] = (decimal)drCurrent["FreightCharge"]+(decimal)drCurrent["InsSurch"]+(decimal)drCurrent["OtherSurch"]+
						(decimal)drCurrent["VasSurch"]+(decimal)drCurrent["ESASurch"];

					//  If pickup is not SAT, SUN or PUB --> Calculate Estimate Delivery Date
				// Validate Holiday Date
				bool isHoliday=false;
				isHoliday = TIESUtility.IsDayHoliday(appID,enterpriseID,DateTime.ParseExact(estDelDate,"dd/MM/yyyy HH:mm",null));
				DateTime tmpEstDate = DateTime.ParseExact(estDelDate,"dd/MM/yyyy HH:mm",null);
				
				if(tmpEstDate.DayOfWeek == System.DayOfWeek.Saturday)	// validate saturday.
				{
					lblConfirmMsg.Text = "Would the customer accept a surcharge for delivery on Saturday?";
					divConfirmHoliday.Visible = true;
					divMain.Visible=false;
					ViewState["rowEvent"] = k;
					ViewState["estDelDate"] = estDelDate;
					ViewState["vasSurCharge"] = "SATDEL";
					}
				else if(tmpEstDate.DayOfWeek == System.DayOfWeek.Sunday) // validate sunday.
				{
					lblConfirmMsg.Text = "Would the customer accept a surcharge for delivery on Sunday?";
					divConfirmHoliday.Visible = true;
					divMain.Visible=false;
					ViewState["rowEvent"] = k;
					ViewState["estDelDate"] = estDelDate;
					ViewState["vasSurCharge"] = "SUNDEL";
				}
				else if(isHoliday) // holiday
				{
					lblConfirmMsg.Text = "Would the customer accept a surcharge for delivery on Public Holiday?";
					divConfirmHoliday.Visible = true;
					divMain.Visible=false;
					ViewState["rowEvent"] = k;
					ViewState["estDelDate"] = estDelDate;
					ViewState["vasSurCharge"] = "PUBDEL";
				}
				else
				{
					drCurrent["EstDelDate"] = DateTime.ParseExact(estDelDate,"dd/MM/yyyy HH:mm",null);

						drPRVAS = dtVAS.Select("consignmentSeqNo='"+drCurrent["ConsignmentSeqNo"].ToString()+
										"'and (vas_code='SATDEL' or vas_code='SUNDEL' or vas_code='PUBDEL')");
					if(drPRVAS.Length>0)
					{
						for (int n=0;n<drPRVAS.Length;n++)
						{
							dtVAS.Rows.Remove(drPRVAS[n]);
						}
					}

					drVasTmp = dtVAS.Select("consignmentSeqNo='"+drCurrent["ConsignmentSeqNo"].ToString()+"'");
					dtVASTmp = dtVAS.Clone();
					if(drVasTmp.Length>0)
					{
						for (int n=0;n<drVasTmp.Length;n++)
						{
							dtVASTmp.ImportRow(drVasTmp[n]);
						}
					}
					if(dtVASTmp!=null)
					{
						object sumSurCharge = dtVASTmp.Compute("sum(vas_surcharge)",null);
						if(sumSurCharge!=null && sumSurCharge!=DBNull.Value) drCurrent["VasSurch"] = (decimal)sumSurCharge;
					}
					
					ViewState["VIEWSTATE_DT3Tmp"] = dtVASTmp;
					BindVASGrid();
					dgConsignment.EditItemIndex = -1;
					dgConsignment.SelectedIndex = e.Item.ItemIndex;
					drCurrent = UpdateConsignment(drCurrent);


					drCurrent["TotalCharge"] = (decimal)drCurrent["FreightCharge"]+(decimal)drCurrent["InsSurch"]+(decimal)drCurrent["OtherSurch"]+
						(decimal)drCurrent["VasSurch"]+(decimal)drCurrent["ESASurch"];

					ButtonColumn bt = (ButtonColumn)dgConsignment.Columns[0];
					bt.Text="<IMG SRC='images/butt-select.gif' border=0 >";
					
					//BindConsignmentGrid();

					
				}

					ViewState["IsCalculatedDelVAS"] = true;
				}
				//  ######################### Check Pickup Date ######################### 				
				
				ViewState["InsertdgConsignment"] = false;
			}
			catch(System.Data.ConstraintException constrntExp)
			{
				String msg = constrntExp.ToString();
				return;
			}
			Package_Update();
			
		}

		protected void dgConsignment_PageChange(object sender, DataGridPageChangedEventArgs e)
		{
			this.lblErrorMsg.Text="";
			if((bool)ViewState["InsertdgConsignment"] == true) // 2009-12-08 by poo, delete current row insert mode
			{
				DataRow drCurrent = dtConsignment.Rows[dtConsignment.Rows.Count - 1];
				drCurrent.Delete();
				dtConsignment.AcceptChanges();
				ViewState["InsertdgConsignment"] = false;
			}
			dgConsignment.EditItemIndex = -1; // end delete current row insert mode
			dgConsignment.CurrentPageIndex = e.NewPageIndex;
			BindConsignmentGrid();
		}
		protected void dgConsignment_Edit(object sender, DataGridCommandEventArgs e)
		{
			this.lblErrorMsg.Text="";

			if((bool)(ViewState["InsertdgConsignment"]) == false)
			{
				ViewState["InsertdgConsignment"] = false;
				dgConsignment.EditItemIndex = e.Item.ItemIndex;
				dgConsignment.SelectedIndex = e.Item.ItemIndex;
				ButtonColumn bt = (ButtonColumn)dgConsignment.Columns[0];
				bt.Text="";
				BindConsignmentGrid();
			}
		}
		protected void dgConsignment_Cancel(object sender, DataGridCommandEventArgs e)
		{
			this.lblErrorMsg.Text="";
			dgConsignment.EditItemIndex = -1;
			if((bool)ViewState["InsertdgConsignment"] == true)
			{
				DataRow drCurrent = dtConsignment.Rows[GetCurrentIndexConsignment(e.Item.ItemIndex)];
				drCurrent.Delete();
			}

			ViewState["InsertdgConsignment"] = false;
			ButtonColumn bt = (ButtonColumn)dgConsignment.Columns[0];
			bt.Text="<IMG SRC='images/butt-select.gif' border=0 >";
			BindConsignmentGrid();
		}
		protected void dgConsignment_Delete(object sender, DataGridCommandEventArgs e)
		{
			this.lblErrorMsg.Text="";
			if((bool)ViewState["InsertdgConsignment"] == false)
			{
				DataRow dr = dtConsignment.Rows[GetCurrentIndexConsignment(e.Item.ItemIndex)];
				Session["consignmentSeq"] = dr["ConsignmentSeqNo"].ToString().Trim();

				//dtConsignment.Rows.RemoveAt(e.Item.ItemIndex);
				dtConsignment.Rows.Remove(dr);
				dgConsignment.EditItemIndex = -1;

				ViewState["VIEWSTATE_DT2Tmp"] = dtPkgTmp;

				String strConsignmentSeq = (String)Session["consignmentSeq"];

				DataRow[] drSelected = dtPackage.Select(" ConsignmentSeqNo = '"+strConsignmentSeq+"'");
	
				if(drSelected.Length>0)
				{	
					for(int i=0;i<drSelected.Length;i++)
					{
						dtPackage.Rows.Remove(drSelected[i]);
					}
				}

				dtPackage.AcceptChanges();

				ButtonColumn bt = (ButtonColumn)dgConsignment.Columns[0];
				bt.Text="<IMG SRC='images/butt-select.gif' border=0 >";
				BindConsignmentGrid();
				dtPkgTmp = PRBMgrDAL.ReadPickupConsignmentPkg(appID,enterpriseID,null,null);
				dtVASTmp = PRBMgrDAL.ReadPickupConsignmentVAS(appID,enterpriseID,null,null);
				BindPkgGrid();
				BindVASGrid();
			}
		}
		public void dgConsignment_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			this.lblErrorMsg.Text="";
			Label dgConsignmentlblSeqno = (Label)dgConsignment.SelectedItem.FindControl("dgConsignmentlblSeqno");
			String strSeqno = dgConsignmentlblSeqno.Text;
		}
		public void dgConsignment_Button(object sender, DataGridCommandEventArgs e)
		{
			if(dgConsignment.EditItemIndex != -1)
				return;
			if(dgPackage.EditItemIndex != -1)
				return;
			if(dgVAS.EditItemIndex != -1)
				return;

			this.lblErrorMsg.Text="";
			if((bool)(ViewState["InsertdgConsignment"]) == false && (bool)ViewState["InsertdgPackage"] == false &&
				(bool)ViewState["InsertdgVAS"] == false)
			{
				String strBookingNo = null;
				if(Request.Params["BOOKINGID"].ToString() != "")
					strBookingNo = Request.Params["BOOKINGID"].ToString();

				String strCmdNm = e.CommandName;
				if(strCmdNm.Equals("Select"))
				{
					//dgPackage.CssClass = "gridFieldSelected";
//					dgPackage.ItemStyle.CssClass = "gridFieldSelected";
//					dgVAS.CssClass = "gridFieldSelected";
					btnInsertPackage.Enabled = true;
					btnInsertVAS.Enabled = true;
					Label dgConsignmentlblSeqno	= (Label)e.Item.FindControl("dgConsignmentlblSeqno");
					Session["consignmentSeq"] = dgConsignmentlblSeqno.Text;
					if(dgConsignmentlblSeqno != null)
					{				
						dtPackage = (DataTable)ViewState["VIEWSTATE_DT2"];
						dtVAS = (DataTable)ViewState["VIEWSTATE_DT3"];
						DataRow[] drPRPkg = dtPackage.Select("consignmentSeqNo='"+dgConsignmentlblSeqno.Text+"'");
						DataRow[] drPRVAS = dtVAS.Select("consignmentSeqNo='"+dgConsignmentlblSeqno.Text+"'");
						dtPkgTmp = dtPackage.Clone();
						dtVASTmp = dtVAS.Clone();

						if(drPRPkg.Length>0)
						{
							for (int i=0;i<drPRPkg.Length;i++)
							{
								dtPkgTmp.ImportRow(drPRPkg[i]);
							}
						}
						if(drPRVAS.Length>0)
						{
							for (int n=0;n<drPRVAS.Length;n++)
							{
								dtVASTmp.ImportRow(drPRVAS[n]);
							}
						}

						ViewState["VIEWSTATE_DT2Tmp"] = dtPkgTmp;
						ViewState["VIEWSTATE_DT3Tmp"] = dtVASTmp;

						dgPackage.CurrentPageIndex = 0; // set default page = 0 when create new consignment , by poo 2009-12-08
						BindPkgGrid();
						dgVAS.CurrentPageIndex = 0; // set default page = 0 when create new consignment , by poo 2009-12-08
						BindVASGrid();	
					}
				
				}
			}
		}
		public void txtRecipientPostCode_TextChange(object sender, System.EventArgs e)
		{
			msTextBox dgConsignmenttxtRecipientPostCode = (msTextBox)dgConsignment.Items[dgConsignment.EditItemIndex].FindControl("dgConsignmenttxtRecipientPostCode");
			DropDownList DrpDlvType = (DropDownList)dgConsignment.Items[dgConsignment.EditItemIndex].FindControl("dgConsignmentddlServiceType");

			String receipPostCode = null;
			if(dgConsignmenttxtRecipientPostCode != null)
				receipPostCode = dgConsignmenttxtRecipientPostCode.Text.Trim();

			DataSet dsDlvType = LoadService(dgConsignment.EditItemIndex,receipPostCode);
			if (DrpDlvType!=null)
			{
				DrpDlvType.DataSource = dsDlvType;
				DrpDlvType.DataTextField = "ServiceValue"; 
				DrpDlvType.DataValueField="ServiceText";
				DrpDlvType.DataBind();
			}
			if(dsDlvType == null)
			{
				dgConsignmenttxtRecipientPostCode.Text = "";		
			}
			if(dsDlvType.Tables.Count == 0)
			{
				dgConsignmenttxtRecipientPostCode.Text = "";		
			}
			if(dsDlvType.Tables[0].Rows.Count == 0)
			{
				dgConsignmenttxtRecipientPostCode.Text = "";		
			}
			//lblErrorMsg.Text = "Invalid Recipient Postal Code";  // end poo
			//return;
		}
		private void btnOK_Click(object sender, System.EventArgs e)
		{
			Session.Remove("SS_Customer");

			if(dtPackage.Rows.Count>0)
			{
				this.lblErrorMsg.Text = "";
				GetGridValuesTotal();
				if(dgConsignment.EditItemIndex != -1)
				{
//					dtConsignment.Rows.RemoveAt(dgConsignment.EditItemIndex);
//					dgConsignment.EditItemIndex = -1;
					return;
				}
				if(dgPackage.EditItemIndex != -1)
				{
//					dtPackage.Rows.RemoveAt(dgPackage.EditItemIndex);
//					dgPackage.EditItemIndex = -1;
					return;
				}
				if(dgVAS.EditItemIndex != -1)
				{
//					dtVAS.Rows.RemoveAt(dgVAS.EditItemIndex);
//					dgVAS.EditItemIndex = -1;
					return;
				}
				Session["SESSION_DTCon"] = dtConsignment;
				Session["SESSION_DTPkg"] = dtPackage;
				Session["SESSION_DTVAS"] = dtVAS;
			
				String sScript = "";
				sScript += "<script language=javascript>";
				sScript += "  window.opener.PickupRequestBooking.Txt_ActualWeight.value = "+totActWt+";" ;
				sScript += "  window.opener.PickupRequestBooking.Txt_DimWeight.value ="+ totDimWt+";" ;
				sScript += "  window.opener.PickupRequestBooking.Txt_Totpkg.value ="+ totPkgs+";" ;
				sScript += "  window.opener.PickupRequestBooking.txtRatedCharge.value ="+ totSurcharge+";" ;
				sScript += "  window.close();";
				sScript += "</script>";
				Response.Write(sScript);
				strCloseWindowStatus="C";
			}
		}
		//----------------------------------- package----------------------------------------------------------------
		public void dgPackage_Update(object sender, DataGridCommandEventArgs e)
		{
			this.lblErrorMsg.Text = "";
			decimal iLength = 0;
			decimal iBreadth = 0;
			decimal iHeight = 0;
			decimal iWeight = 0;
			int iQty = 0;
			decimal fDimWt = 0;
			decimal decTotalWt = 0;
			decimal decTotal_ac_Wt = 0;//TU on 17June08
			decimal decTotalDimWt = 0;
			int cnt = 0;

			try
			{
				msTextBox dgPackagetxtMPSNo = (msTextBox)e.Item.FindControl("dgPackagetxtMPSNo");
				int k = (e.Item.ItemIndex+((dgPackage.CurrentPageIndex)*dgPackage.PageSize));
				if(dgPackagetxtMPSNo != null)
				{

					cnt = dtPkgTmp.Rows.Count;
					for(int i=0;i<cnt;i++)
					{
						DataRow drTmp = dtPkgTmp.Rows[i];
						if(i!=k && cnt!=1)
						if(drTmp["Pkg_MPSNo"].ToString().Trim() == dgPackagetxtMPSNo.Text)
						{
							lblErrorMsg.Text = "Duplicated MPS No.";
							return;
						}
					}
					DataRow drCurrent = dtPkgTmp.Rows[k];
					if(dgPackagetxtMPSNo.Text != "")
					{
						drCurrent["Pkg_MPSNo"] = dgPackagetxtMPSNo.Text;
					}
					else
					{
						lblErrorMsg.Text = "Please enter the MPSNo.";
						return;
					}
				}
			
				msTextBox dgPackagetxtLength = (msTextBox)e.Item.FindControl("dgPackagetxtLength");
				if(dgPackagetxtLength != null)
				{
					DataRow drCurrent = dtPkgTmp.Rows[k];					
					if(dgPackagetxtLength.Text != "")
					{						
						iLength =  Convert.ToDecimal((dgPackagetxtLength.Text.ToString()));
						if(iLength == 0)
						{
							lblErrorMsg.Text = "Please enter the length";
							return;
						}
						else
						{
							drCurrent["pkg_length"] = dgPackagetxtLength.Text;
						}
					}
					else
					{
						lblErrorMsg.Text = "Please enter the length";
						return;
					}
				}

				msTextBox dgPackagetxtBreadth = (msTextBox)e.Item.FindControl("dgPackagetxtBreadth");
				if(dgPackagetxtBreadth != null)
				{
					DataRow drCurrent = dtPkgTmp.Rows[k];
					if(dgPackagetxtBreadth.Text != "")
					{
						iBreadth = Convert.ToDecimal(dgPackagetxtBreadth.Text.ToString());
						if(iBreadth == 0)
						{
							lblErrorMsg.Text = "Please enter the breadth";
							return;
						}
						else
						{
							drCurrent["pkg_breadth"] = dgPackagetxtBreadth.Text;
						}
					}
					else
					{
						lblErrorMsg.Text = "Please enter the breadth";
						return;
					}
				}

				msTextBox dgPackagetxtHeight = (msTextBox)e.Item.FindControl("dgPackagetxtHeight");
				if(dgPackagetxtHeight != null)
				{
					DataRow drCurrent = dtPkgTmp.Rows[k];	
					if(dgPackagetxtHeight.Text != "")
					{						
						iHeight = Convert.ToDecimal(dgPackagetxtHeight.Text.ToString());
						if(iHeight == 0)
						{
							lblErrorMsg.Text = "Please enter the height";
							return;
						}
						else
						{
							drCurrent["pkg_height"] = dgPackagetxtHeight.Text;
						}
					}
					else
					{
						lblErrorMsg.Text = "Please enter the height";
						return;
					}
				}
			
				Label dgPackagelblVolume = (Label)e.Item.FindControl("dgPackagelblVolume");
				if((iLength != 0 ) && (iBreadth != 0) && (iHeight != 0))
				{
					dgPackagelblVolume.Text = Convert.ToString(Math.Round((iLength * iBreadth * iHeight),2));
					DataRow drCurrent = dtPkgTmp.Rows[k];
					drCurrent["pkg_volume"] = dgPackagelblVolume.Text;	
				}

				msTextBox dgPackagetxtQty = (msTextBox)e.Item.FindControl("dgPackagetxtQty");
				if(dgPackagetxtQty != null)
				{
					DataRow drCurrent = dtPkgTmp.Rows[k];
					if(dgPackagetxtQty.Text != "")
					{						
						iQty = Convert.ToInt16(dgPackagetxtQty.Text.ToString());
						if(iQty == 0)
						{
							lblErrorMsg.Text = "Please enter the Quantity";
							return;
						}
						else
						{
							drCurrent["pkg_qty"] = dgPackagetxtQty.Text;
						}
					}
					else
					{
						lblErrorMsg.Text = "Please enter the Quantity";
						return;
					}
				}

				msTextBox dgPackagetxtWeight = (msTextBox)e.Item.FindControl("dgPackagetxtWeight");
				if(dgPackagetxtWeight != null)
				{
					DataRow drCurrent = dtPkgTmp.Rows[k];						
					if(dgPackagetxtWeight.Text != "")
					{						
						iWeight = Convert.ToDecimal(dgPackagetxtWeight.Text.ToString());
						if(iWeight == 0)
						{
							lblErrorMsg.Text = "Please enter the weight";
							return;
						}
						else
						{
							drCurrent["pkg_wt"] = dgPackagetxtWeight.Text;
						}
					}
					else
					{
						lblErrorMsg.Text = "Please enter the weight";
						return;
					}
				}

				Label dgPackagelblTotalRWt = (Label)e.Item.FindControl("dgPackagelblTotalRWt");
				Label dgPackagelblTotalActWt = (Label)e.Item.FindControl("dgPackagelblTotalActWt");//tu 7Jun08
				if((iQty > 0 ) && (iWeight > 0))
				{
					dgPackagelblTotalActWt.Text=Convert.ToString((iQty * iWeight));//tu 7Jun08
					//iWeight = this.EnterpriseRounding(iWeight); by x jul 03 08
					//Modify on 3Jul 08 Calculate Tot_Wt
					if(iWeight<1)
					{
						dgPackagelblTotalRWt.Text = this.EnterpriseRounding(iQty * 1).ToString();
					}
					else
					{
						dgPackagelblTotalRWt.Text = this.EnterpriseRounding(iQty * EnterpriseRounding(iWeight)).ToString();
					}
					//lblTotWt.Text = this.EnterpriseRounding(iQty * iWeight).ToString();
					//END
					DataRow drCurrent = dtPkgTmp.Rows[k];
					if (dgPackagelblTotalActWt.Text=="") //tu 7Jun08
					{
						dgPackagelblTotalActWt.Text="0";
					}
					drCurrent["tot_wt"] = Math.Round(Convert.ToDecimal(dgPackagelblTotalRWt.Text), 2);	
					decTotalWt = Convert.ToDecimal(dgPackagelblTotalRWt.Text);
					drCurrent["tot_act_wt"] = Convert.ToDecimal(dgPackagelblTotalActWt.Text);
					decTotal_ac_Wt=Convert.ToDecimal(dgPackagelblTotalActWt.Text);
					drCurrent["consignmentSeqNo"] = (String)Session["consignmentSeq"];					
				}

				Label dgPackagelblTotalRDimWt = (Label)e.Item.FindControl("dgPackagelblTotalRDimWt");
				if((iLength != 0 ) && (iBreadth != 0) && (iHeight != 0))
				{
					dgPackagelblTotalRDimWt.Text = Convert.ToString((iLength * iBreadth * iHeight));
					DataRow drCurrent = dtPkgTmp.Rows[k];
					Customer c = new Customer();
					c.Populate(utility.GetAppID(),utility.GetEnterpriseID(),(String)Session["PickupCustomerID"]);
		
					Enterprise enterprise = new Enterprise();
					enterprise.Populate(utility.GetAppID(),utility.GetEnterpriseID());
					decimal iVolume = iLength * iBreadth * iHeight;
					decimal fDensityFactor = 0;			
					if((c.density_factor.ToString()!="")&&((decimal)c.density_factor != 0))
					{
						fDensityFactor = (decimal)c.density_factor ;
					}
					else
					{												 
						fDensityFactor = (decimal)enterprise.DensityFactor;
					}
					if(fDensityFactor > 0)
					{
						fDimWt = iVolume / fDensityFactor;
					}
					else
					{
						lblErrorMsg.Text = "The density Factor is 0";
						return;
					}

					fDimWt = this.EnterpriseRounding(fDimWt);
					//on 3Jul 08
					if(fDimWt<1)
					{
						decTotalDimWt = Math.Round((decimal)iQty, 2); 
						drCurrent["tot_dim_wt"] = decTotalDimWt;
					}
					else
					{
						decTotalDimWt = Math.Round(fDimWt * iQty, 2);
						drCurrent["tot_dim_wt"] = decTotalDimWt;
					}				
				}
				
				//Calculate the Chargeable weight
//				DataRow drRow = dsPkgDetails.Tables[0].Rows[e.Item.ItemIndex];
//				decimal decChrgeable = 0;
//			
//				if(strApplyDimWt.ToString().Trim() == "Y" || strApplyDimWt.ToString().Trim() == "")
//				{
//					if(decTotalWt < (decimal)decTotalDimWt)
//					{
//						decChrgeable = (decimal)decTotalDimWt;
//					}
//					else
//					{
//						decChrgeable = (decimal)decTotalWt;
//					}
//				}
//				else
//				{
//					decChrgeable = (decimal)decTotalWt;
//				}
//
//				drRow["chargeable_wt"] = decChrgeable;
				ViewState["VIEWSTATE_DT2Tmp"] = dtPkgTmp;

				String strConsignmentSeq = (String)Session["consignmentSeq"];

				DataRow[] drSelected = dtPackage.Select(" ConsignmentSeqNo = '"+strConsignmentSeq+"'");
	
				if(drSelected.Length>0)
				{	
					for(int i=0;i<drSelected.Length;i++)
					{
						dtPackage.Rows.Remove(drSelected[i]);
					}
				}

				dtPackage.AcceptChanges();

				foreach(DataRow drPkg in dtPkgTmp.Rows)
				{
					dtPackage.ImportRow(drPkg);
				}
				
				ViewState["VIEWSTATE_DT2"] = dtPackage;
				dgPackage.EditItemIndex = -1;

				int sumPkgQty = 0;
				decimal sumPkgActWeight = 0;
				decimal sumPkgDimWeight = 0;
				decimal sumPkgRWeight = 0;
				decimal sumDecChrgWt = 0;
				DataRow[] dr = dtPackage.Select(" ConsignmentSeqNo = '"+strConsignmentSeq+"'");


				if(dr.Length>0)
				{
					for(int n=0;n<dr.Length;n++)
					{
						sumPkgQty += (int)dr[n][9];
						sumPkgActWeight += (decimal)dr[n][10];
						sumPkgRWeight += (decimal)dr[n][11];
						sumPkgDimWeight += (decimal)dr[n][12];
						if((decimal)dr[n][11]<(decimal)dr[n][12])
						{
							sumDecChrgWt += (decimal)dr[n][12];
						}
						else
						{
							sumDecChrgWt += (decimal)dr[n][11];
						}
					}

					DataRow[] drCon = dtConsignment.Select(" ConsignmentSeqNo = '"+strConsignmentSeq+"'");
					DataTable dtConTmp = dtConsignment.Clone();

					Customer customer = new Customer();
					customer.Populate(appID,enterpriseID, (String)Session["PickupCustomerID"]);

					if(drCon.Length > 0)
					{						
						//drCon[0][8] = sumPkgQty;
						drCon[0]["TotalPkg"] = sumPkgQty;
						//drCon[0][9] = sumPkgActWeight;
						drCon[0]["ActualWt"]= sumPkgActWeight;
						//drCon[0][10] = sumPkgDimWeight;
						drCon[0]["DimWt"]=sumPkgDimWeight;



//						if(customer.ApplyDimWt.ToString().Trim() == "Y" || customer.ApplyDimWt.ToString().Trim() == "")
//						{
//							if(sumPkgDimWeight>sumPkgRWeight)
//							{
//								//drCon[0][11] = sumPkgDimWeight;
//								drCon[0]["ChgWt"] = sumPkgDimWeight;
//							}
//							else
//							{
//								drCon[0]["ChgWt"] = sumPkgRWeight;
//							}
//						}
//						else
//						{
//							drCon[0]["ChgWt"] = sumPkgRWeight;
//						}

						if (customer.Dim_By_tot =="Y" || customer.Dim_By_tot.Trim() == "")
						{
							if(sumPkgDimWeight < sumPkgRWeight)
							{
								drCon[0]["ChgWt"] = sumPkgRWeight;
							}
							else
							{
								drCon[0]["ChgWt"] = sumPkgDimWeight;
							}
						}
						else
						{
							if(customer.ApplyDimWt=="N")
							{
								drCon[0]["ChgWt"] = sumPkgRWeight;
							}
							else
							{
								drCon[0]["ChgWt"] = sumDecChrgWt;
							}
						}

						
						Zipcode zipcode = new Zipcode();
						zipcode.Populate(appID,enterpriseID,(String)Session["SenderPostalCode"]);
						String strOrgZone = zipcode.ZoneCode;
						zipcode.Populate(appID , enterpriseID , drCon[0]["RecPostalCode"].ToString().Trim());
						String strDestnZone = zipcode.ZoneCode;

						DataSet tmpCustZone = null;
						string sendCustZone = null;
						string recipCustZone = null;
						tmpCustZone = SysDataMgrDAL.GetCustZoneData(appID, enterpriseID,
							0, 0, (String)Session["PickupCustomerID"], (String)Session["SenderPostalCode"]).ds;
						if(tmpCustZone.Tables[0].Rows.Count > 0)
						{
							foreach(DataRow drTmp in tmpCustZone.Tables[0].Rows)
							{
								sendCustZone = drTmp["zone_code"].ToString().Trim();
							}
						}
						tmpCustZone = SysDataMgrDAL.GetCustZoneData(appID, enterpriseID,
							0, 0, (String)Session["PickupCustomerID"], drCon[0]["RecPostalCode"].ToString().Trim()).ds;
						if(tmpCustZone.Tables[0].Rows.Count > 0)
						{
							foreach(DataRow drTmp in tmpCustZone.Tables[0].Rows)
							{
								recipCustZone = drTmp["zone_code"].ToString().Trim();
							}
						}
						
						float freightCharge = 0;
						float freightCharge0 = 0;
						float sumFreight = 0;
						float totFreight = 0;
						if(customer.IsCustomer_Box == "Y")
						{
							if(!customer.IsBoxRateAvailable(appID,enterpriseID,sendCustZone,recipCustZone,drCon[0]["ServiceType"].ToString().Trim()))
							{
								lblErrorMsg.Text = "Customer has no Box Rate defined.";
								return;
							}
							else
							{
								float weightToFreight = 0;
								for(int y=0;y<dr.Length;y++)
								{

//									if(customer.ApplyDimWt.ToString().Trim() == "Y" || customer.ApplyDimWt.ToString().Trim() == "")
//									{
//										if(float.Parse(dr[y][11].ToString()) > float.Parse(dr[y][12].ToString()))
//										{
//											weightToFreight = float.Parse(dr[y][11].ToString())/int.Parse(dr[y][9].ToString());
//										}
//										else
//										{
//											weightToFreight = float.Parse(dr[y][12].ToString())/int.Parse(dr[y][9].ToString());
//										}
//									}
//									else
//									{
//										weightToFreight = float.Parse(dr[y][11].ToString())/int.Parse(dr[y][9].ToString());
//									}
//									
									//--- Dim by total = Yes 
									//----------- �ҡ����������ҧ Total R Wt ��� Total R Dim Wt ����˹�ҡ���ҡѹ��ҽ�觹�� (�ء Rows ����ѹ)
									//----------- ���������ʢ�ҧ�� ����ҷ��� Row ��Ҥ�������ҧ Total R Wt ��� Total R Dim Wt ����˹�ҡ���ҡѹ ����˹�ҡ��ҽ�觹��
									//----------- ��Ҥ�Ңͧ Dim by total = No ��� Apply Dim Wt �� No �����Ҥ�Ҩҡ Total R Wt

									if(customer.Dim_By_tot == "Y" || customer.Dim_By_tot.Trim() == "")
									{
										weightToFreight = float.Parse(dr[y][12].ToString())/int.Parse(dr[y][9].ToString());		
									}
									else
									{
										if(customer.Dim_By_tot == "N" && customer.ApplyDimWt=="Y")
										{											
											if(float.Parse(dr[y][11].ToString()) > float.Parse(dr[y][12].ToString()))											
											{											
												weightToFreight = float.Parse(dr[y][11].ToString())/int.Parse(dr[y][9].ToString());																					
											}																					
											else											
											{												
												weightToFreight = float.Parse(dr[y][12].ToString())/int.Parse(dr[y][9].ToString());											
											}								
										}
										else 
										{
											weightToFreight = float.Parse(dr[y][11].ToString())/int.Parse(dr[y][9].ToString());	
										}
									}

									freightCharge = TIESUtility.ComputeFreightCharge(appID,enterpriseID,(String)Session["PickupCustomerID"],"C",
										strOrgZone,strDestnZone,weightToFreight , drCon[0]["ServiceType"].ToString(),
										(String)Session["SenderPostalCode"] , drCon[0]["RecPostalCode"].ToString());
									freightCharge = (freightCharge * int.Parse((dr[y][9]).ToString()));
									sumFreight = sumFreight+freightCharge;
								}
								
								if(sumPkgQty < int.Parse(customer.Minimum_Box))
								{
									freightCharge0 = (TIESUtility.ComputeFreightCharge(appID,enterpriseID,(String)Session["PickupCustomerID"],"C",
										strOrgZone,strDestnZone,0 , drCon[0]["ServiceType"].ToString(),
										(String)Session["SenderPostalCode"] , drCon[0]["RecPostalCode"].ToString()));
									totFreight = ((int.Parse(customer.Minimum_Box)-sumPkgQty)*freightCharge0)+sumFreight;
								}
								else
									totFreight = sumFreight;
							}
						}
						else
						{
							totFreight = TIESUtility.ComputeFreightCharge(appID,enterpriseID,(String)Session["PickupCustomerID"],"C",
								strOrgZone,strDestnZone,float.Parse(drCon[0]["ChgWt"].ToString()) , drCon[0]["ServiceType"].ToString(),
								(String)Session["SenderPostalCode"] , drCon[0]["RecPostalCode"].ToString());
						}
						drCon[0]["FreightCharge"] = EnterpriseRounding((decimal)(totFreight));

						decimal decESACharge = calCulateESA_ServiceType(drCon[0]["RecPostalCode"].ToString(),(String)Session["SenderPostalCode"],
							(String)Session["PickupCustomerID"] , Convert.ToDecimal(freightCharge),drCon[0]["ServiceType"].ToString());
						drCon[0]["ESASurch"] = EnterpriseRounding(decESACharge);

						decimal OtherSurcharge = 0;
						if((customer.OtherSurchargeAmount == null || customer.OtherSurchargeAmount.ToString() == "") && (customer.OtherSurchargeAmountPercentage == null || customer.OtherSurchargeAmountPercentage.ToString() == ""))
						{
							OtherSurcharge = 0;
						}
						else
						{
							if(customer.OtherSurchargeAmount != "")
							{ 
								OtherSurcharge = Convert.ToDecimal(customer.OtherSurchargeAmount);
							}
							else if(customer.OtherSurchargeAmountPercentage != null && customer.OtherSurchargeAmountPercentage != "")
							{
								OtherSurcharge = Convert.ToDecimal(customer.OtherSurchargeAmountPercentage) * Convert.ToDecimal(freightCharge)/100;

								if(customer.OtherSurchargeMin != null && customer.OtherSurchargeMin != "")
								{
									OtherSurcharge = Math.Max(OtherSurcharge, Convert.ToDecimal(customer.OtherSurchargeMin));							
								}
								if(customer.OtherSurchargeMax != null && customer.OtherSurchargeMax != "")
								{
									OtherSurcharge = Math.Min(OtherSurcharge, Convert.ToDecimal(customer.OtherSurchargeMax));
								}
							}
						}
						drCon[0]["OtherSurch"] = EnterpriseRounding(OtherSurcharge);						

						DomesticShipmentMgrBAL domesticBAL = new DomesticShipmentMgrBAL();
						decimal decInsSurchrg = domesticBAL.ComputeInsSurchargeByCust(appID,enterpriseID,(Decimal)drCon[0][6],(String)Session["PickupCustomerID"],customer);
						drCon[0]["InsSurch"] = decInsSurchrg;

						drCon[0]["TotalCharge"] = Convert.ToDecimal(drCon[0]["VasSurch"].ToString().Trim()) + Convert.ToDecimal(drCon[0][14].ToString().Trim())+Convert.ToDecimal(drCon[0][13].ToString().Trim())+
							Convert.ToDecimal(drCon[0][17].ToString().Trim())+Convert.ToDecimal(drCon[0][15]);

						dtConTmp.ImportRow(drCon[0]);

						dtConsignment.Rows.Remove(drCon[0]);
						
						foreach(DataRow drConTmp in dtConTmp.Rows)
						{
							dtConsignment.ImportRow(drConTmp);
						}
						drCon = null;
						dtConTmp = null;
						drCon = dtConsignment.Select("","ConsignmentSeqNo ASC");
						dtConTmp = dtConsignment.Clone();
						if(drCon.Length > 0)
						{
							for(int n=0;n<drCon.Length;n++)
							{
								dtConTmp.ImportRow(drCon[n]);
							}
							dtConsignment.Rows.Clear();
							foreach(DataRow drConTmp2 in dtConTmp.Rows)
							{
								dtConsignment.ImportRow(drConTmp2);
							}
						}
						BindConsignmentGrid();
					}
				}
				ViewState["InsertdgPackage"] = false;
				BindPkgGrid();
			}
			catch(System.Data.ConstraintException constrntExp)
			{
				String msg = constrntExp.ToString();
				return;
			}
		}

//		public void dgPackage_Update(object sender, DataGridCommandEventArgs e)
//		{
//			Package_Update(e.Item.ItemIndex);
//		}

		public void Package_Update()
		{
			this.lblErrorMsg.Text = "";

			int sumPkgQty = 0;
			decimal sumPkgActWeight = 0;
			decimal sumPkgDimWeight = 0;
			decimal sumPkgRWeight = 0;
			decimal sumDecChrgWt = 0;

			try
			{
				string strConsignmentSeq = (String)Session["consignmentSeq"];
				dtPackage = (DataTable)ViewState["VIEWSTATE_DT2"];
				dtPkgTmp = dtPackage.Clone();
				DataRow[] dr = dtPackage.Select(" ConsignmentSeqNo = '"+strConsignmentSeq+"'");

				for(int n=0;n<dr.Length;n++)
				{
					sumPkgQty += (int)dr[n][9];
					sumPkgActWeight += (decimal)dr[n][10];
					sumPkgRWeight += (decimal)dr[n][11];
					sumPkgDimWeight += (decimal)dr[n][12];
					if((decimal)dr[n][11]<(decimal)dr[n][12])
					{
						sumDecChrgWt += (decimal)dr[n][12];
					}
					else
					{
						sumDecChrgWt += (decimal)dr[n][11];
					}
				}

				DataRow[] drCon = dtConsignment.Select(" ConsignmentSeqNo = '"+strConsignmentSeq+"'");
				DataTable dtConTmp = dtConsignment.Clone();

				Customer customer = new Customer();
				customer.Populate(appID,enterpriseID, (String)Session["PickupCustomerID"]);

				if(drCon.Length > 0)
				{						
					drCon[0]["TotalPkg"] = sumPkgQty;
					drCon[0]["ActualWt"]= sumPkgActWeight;
					drCon[0]["DimWt"]=sumPkgDimWeight;

					if (customer.Dim_By_tot =="Y" || customer.Dim_By_tot.Trim() == "")
					{
						if(sumPkgDimWeight < sumPkgRWeight)
						{
							drCon[0]["ChgWt"] = sumPkgRWeight;
						}
						else
						{
							drCon[0]["ChgWt"] = sumPkgDimWeight;
						}
					}
					else
					{
						if(customer.ApplyDimWt=="N")
						{
							drCon[0]["ChgWt"] = sumPkgRWeight;
						}
						else
						{
							drCon[0]["ChgWt"] = sumDecChrgWt;
						}
					}

						
					Zipcode zipcode = new Zipcode();
					zipcode.Populate(appID,enterpriseID,(String)Session["SenderPostalCode"]);
					String strOrgZone = zipcode.ZoneCode;
					zipcode.Populate(appID , enterpriseID , drCon[0]["RecPostalCode"].ToString().Trim());
					String strDestnZone = zipcode.ZoneCode;

					DataSet tmpCustZone = null;
					string sendCustZone = null;
					string recipCustZone = null;
					tmpCustZone = SysDataMgrDAL.GetCustZoneData(appID, enterpriseID,
						0, 0, (String)Session["PickupCustomerID"], (String)Session["SenderPostalCode"]).ds;
					if(tmpCustZone.Tables[0].Rows.Count > 0)
					{
						foreach(DataRow drTmp in tmpCustZone.Tables[0].Rows)
						{
							sendCustZone = drTmp["zone_code"].ToString().Trim();
						}
					}
					tmpCustZone = SysDataMgrDAL.GetCustZoneData(appID, enterpriseID,
						0, 0, (String)Session["PickupCustomerID"], drCon[0]["RecPostalCode"].ToString().Trim()).ds;
					if(tmpCustZone.Tables[0].Rows.Count > 0)
					{
						foreach(DataRow drTmp in tmpCustZone.Tables[0].Rows)
						{
							recipCustZone = drTmp["zone_code"].ToString().Trim();
						}
					}
						
					float freightCharge = 0;
					float freightCharge0 = 0;
					float sumFreight = 0;
					float totFreight = 0;
					if(customer.IsCustomer_Box == "Y")
					{
						if(!customer.IsBoxRateAvailable(appID,enterpriseID,sendCustZone,recipCustZone,drCon[0]["ServiceType"].ToString().Trim()))
						{
							lblErrorMsg.Text = "Customer has no Box Rate defined.";
							return;
						}
						else
						{
							float weightToFreight = 0;
							for(int y=0;y<dr.Length;y++)
							{					
								//--- Dim by total = Yes 
								//----------- �ҡ����������ҧ Total R Wt ��� Total R Dim Wt ����˹�ҡ���ҡѹ��ҽ�觹�� (�ء Rows ����ѹ)
								//----------- ���������ʢ�ҧ�� ����ҷ��� Row ��Ҥ�������ҧ Total R Wt ��� Total R Dim Wt ����˹�ҡ���ҡѹ ����˹�ҡ��ҽ�觹��
								//----------- ��Ҥ�Ңͧ Dim by total = No ��� Apply Dim Wt �� No �����Ҥ�Ҩҡ Total R Wt

								if(customer.Dim_By_tot == "Y" || customer.Dim_By_tot.Trim() == "")
								{
									weightToFreight = float.Parse(dr[y][12].ToString())/int.Parse(dr[y][9].ToString());		
								}
								else
								{
									if(customer.Dim_By_tot == "N" && customer.ApplyDimWt=="Y")
									{											
										if(float.Parse(dr[y][11].ToString()) > float.Parse(dr[y][12].ToString()))											
										{											
											weightToFreight = float.Parse(dr[y][11].ToString())/int.Parse(dr[y][9].ToString());																					
										}																					
										else											
										{												
											weightToFreight = float.Parse(dr[y][12].ToString())/int.Parse(dr[y][9].ToString());											
										}								
									}
									else 
									{
										weightToFreight = float.Parse(dr[y][11].ToString())/int.Parse(dr[y][9].ToString());	
									}
								}

								freightCharge = TIESUtility.ComputeFreightCharge(appID,enterpriseID,(String)Session["PickupCustomerID"],"C",
									strOrgZone,strDestnZone,weightToFreight , drCon[0]["ServiceType"].ToString(),
									(String)Session["SenderPostalCode"] , drCon[0]["RecPostalCode"].ToString());
								freightCharge = (freightCharge * int.Parse((dr[y][9]).ToString()));
								sumFreight = sumFreight+freightCharge;
							}
								
							if(sumPkgQty < int.Parse(customer.Minimum_Box))
							{
								freightCharge0 = (TIESUtility.ComputeFreightCharge(appID,enterpriseID,(String)Session["PickupCustomerID"],"C",
									strOrgZone,strDestnZone,0 , drCon[0]["ServiceType"].ToString(),
									(String)Session["SenderPostalCode"] , drCon[0]["RecPostalCode"].ToString()));
								totFreight = ((int.Parse(customer.Minimum_Box)-sumPkgQty)*freightCharge0)+sumFreight;
							}
							else
								totFreight = sumFreight;
						}
					}
					else
					{
						totFreight = TIESUtility.ComputeFreightCharge(appID,enterpriseID,(String)Session["PickupCustomerID"],"C",
							strOrgZone,strDestnZone,float.Parse(drCon[0]["ChgWt"].ToString()) , drCon[0]["ServiceType"].ToString(),
							(String)Session["SenderPostalCode"] , drCon[0]["RecPostalCode"].ToString());
					}
					drCon[0]["FreightCharge"] = EnterpriseRounding((decimal)(totFreight));

					decimal decESACharge = calCulateESA_ServiceType(drCon[0]["RecPostalCode"].ToString(),(String)Session["SenderPostalCode"],
						(String)Session["PickupCustomerID"] , Convert.ToDecimal(freightCharge),drCon[0]["ServiceType"].ToString());
					drCon[0]["ESASurch"] = EnterpriseRounding(decESACharge);

					decimal OtherSurcharge = 0;
					if((customer.OtherSurchargeAmount == null || customer.OtherSurchargeAmount.ToString() == "") && (customer.OtherSurchargeAmountPercentage == null || customer.OtherSurchargeAmountPercentage.ToString() == ""))
					{
						OtherSurcharge = 0;
					}
					else
					{
						if(customer.OtherSurchargeAmount != "")
						{ 
							OtherSurcharge = Convert.ToDecimal(customer.OtherSurchargeAmount);
						}
						else if(customer.OtherSurchargeAmountPercentage != null && customer.OtherSurchargeAmountPercentage != "")
						{
							OtherSurcharge = Convert.ToDecimal(customer.OtherSurchargeAmountPercentage) * Convert.ToDecimal(freightCharge)/100;

							if(customer.OtherSurchargeMin != null && customer.OtherSurchargeMin != "")
							{
								OtherSurcharge = Math.Max(OtherSurcharge, Convert.ToDecimal(customer.OtherSurchargeMin));							
							}
							if(customer.OtherSurchargeMax != null && customer.OtherSurchargeMax != "")
							{
								OtherSurcharge = Math.Min(OtherSurcharge, Convert.ToDecimal(customer.OtherSurchargeMax));
							}
						}
					}
					drCon[0]["OtherSurch"] = EnterpriseRounding(OtherSurcharge);						

					DomesticShipmentMgrBAL domesticBAL = new DomesticShipmentMgrBAL();
					decimal decInsSurchrg = domesticBAL.ComputeInsSurchargeByCust(appID,enterpriseID,(Decimal)drCon[0][6],(String)Session["PickupCustomerID"],customer);
					drCon[0]["InsSurch"] = decInsSurchrg;

					drCon[0]["TotalCharge"] = Convert.ToDecimal(drCon[0]["VasSurch"].ToString().Trim()) + Convert.ToDecimal(drCon[0][14].ToString().Trim())+Convert.ToDecimal(drCon[0][13].ToString().Trim())+
						Convert.ToDecimal(drCon[0][17].ToString().Trim())+Convert.ToDecimal(drCon[0][15]);

					dtConTmp.ImportRow(drCon[0]);

					dtConsignment.Rows.Remove(drCon[0]);
						
					foreach(DataRow drConTmp in dtConTmp.Rows)
					{
						dtConsignment.ImportRow(drConTmp);
					}
					drCon = null;
					dtConTmp = null;
					drCon = dtConsignment.Select("","ConsignmentSeqNo ASC");
					dtConTmp = dtConsignment.Clone();
					if(drCon.Length > 0)
					{
						for(int n=0;n<drCon.Length;n++)
						{
							dtConTmp.ImportRow(drCon[n]);
						}
						dtConsignment.Rows.Clear();
						foreach(DataRow drConTmp2 in dtConTmp.Rows)
						{
							dtConsignment.ImportRow(drConTmp2);
						}
					}
					BindConsignmentGrid();
				}
			}
			catch(System.Data.ConstraintException constrntExp)
			{
				String msg = constrntExp.ToString();
				return;
			}
		}


		protected void dgPackage_PageChange(object sender, DataGridPageChangedEventArgs e)
		{
			this.lblErrorMsg.Text="";
			if((bool)ViewState["InsertdgPackage"] == true) // 2009-12-08 by poo, delete current row insert mode
			{
				DataRow drCurrent = dtPkgTmp.Rows[dtPkgTmp.Rows.Count - 1];
				drCurrent.Delete();
				dtPkgTmp.AcceptChanges();
				ViewState["InsertdgPackage"] = false;
			}
			dgPackage.EditItemIndex = -1; // end delete current row insert mode
			dgPackage.CurrentPageIndex = e.NewPageIndex;
			BindPkgGrid();
		}
		protected void dgPackage_Edit(object sender, DataGridCommandEventArgs e)
		{
			this.lblErrorMsg.Text="";
			if((bool)(ViewState["InsertdgPackage"]) == false)
			{
				ViewState["InsertdgPackage"] = false;
				dgPackage.EditItemIndex = e.Item.ItemIndex;
				BindPkgGrid();
			}
		}
		protected void dgPackage_Cancel(object sender, DataGridCommandEventArgs e)
		{
			this.lblErrorMsg.Text="";
			if(dtPkgTmp.Rows.Count > 1)
			{
				dgPackage.EditItemIndex = -1;
				if((bool)ViewState["InsertdgPackage"] == true)
				{
					DataRow drCurrent = dtPkgTmp.Rows[GetCurrentIndexPackage(e.Item.ItemIndex)];
					drCurrent.Delete();
				}

				ViewState["InsertdgPackage"] = false;
				BindPkgGrid();
			}
			else
			{
				if((bool)ViewState["InsertdgPackage"] == false)
				{
					dgPackage.EditItemIndex = -1;
					BindPkgGrid();
				}
			}
		}
		protected void dgPackage_Delete(object sender, DataGridCommandEventArgs e)
		{
			if((bool)(ViewState["InsertdgPackage"]) == false)
			{
				this.lblErrorMsg.Text="";
				if(dtPkgTmp.Rows.Count > 1)
				{
					dtPkgTmp.Rows.RemoveAt(GetCurrentIndexPackage(e.Item.ItemIndex));
					ViewState["VIEWSTATE_DT2Tmp"] = dtPkgTmp;

					String strConsignmentSeq = (String)Session["consignmentSeq"];

					DataRow[] drSelected = dtPackage.Select(" ConsignmentSeqNo = '"+strConsignmentSeq+"'");
	
					if(drSelected.Length>0)
					{	
						for(int i=0;i<drSelected.Length;i++)
						{
							dtPackage.Rows.Remove(drSelected[i]);
						}
					}

					dtPackage.AcceptChanges();

					foreach(DataRow drPkg in dtPkgTmp.Rows)
					{
						dtPackage.ImportRow(drPkg);
					}
				
					ViewState["VIEWSTATE_DT2"] = dtPackage;
					dgPackage.EditItemIndex = -1;
					BindPkgGrid();

					int sumPkgQty = 0;
					decimal sumPkgActWeight = 0;
					decimal sumPkgDimWeight = 0;
					decimal sumPkgRWeight = 0;
					DataRow[] dr = dtPackage.Select(" ConsignmentSeqNo = '"+strConsignmentSeq+"'");
					if(dr.Length>0)
					{
						for(int n=0;n<dr.Length;n++)
						{
							sumPkgQty += (int)dr[n][9];
							sumPkgActWeight += (decimal)dr[n][10];
							sumPkgRWeight += (decimal)dr[n][11];
							sumPkgDimWeight += (decimal)dr[n][12];
						}
						DataRow[] drCon = dtConsignment.Select(" ConsignmentSeqNo = '"+strConsignmentSeq+"'");
						DataTable dtConTmp = dtConsignment.Clone();
						if(drCon.Length > 0)
						{						
							drCon[0][8] = sumPkgQty;
							drCon[0][9] = sumPkgActWeight;
							drCon[0][10] = sumPkgDimWeight;
							if(sumPkgDimWeight>sumPkgRWeight)
							{
								drCon[0][11] = sumPkgDimWeight;
							}
							else
							{
								drCon[0][11] = sumPkgRWeight;
							}
							Zipcode zipcode = new Zipcode();

							zipcode.Populate(appID,enterpriseID,(String)Session["SenderPostalCode"]);
							String strOrgZone = zipcode.ZoneCode;

							zipcode.Populate(appID,enterpriseID,drCon[0][4].ToString().Trim());
							String strDestnZone = zipcode.ZoneCode;

							Customer customer = new Customer();
							customer.Populate(appID,enterpriseID, (String)Session["PickupCustomerID"]);

							DataSet tmpCustZone = null;
							string sendCustZone = null;
							string recipCustZone = null;
							tmpCustZone = SysDataMgrDAL.GetCustZoneData(appID, enterpriseID,
								0, 0, (String)Session["PickupCustomerID"], (String)Session["SenderPostalCode"]).ds;
							if(tmpCustZone.Tables[0].Rows.Count > 0)
							{
								foreach(DataRow drTmp in tmpCustZone.Tables[0].Rows)
								{
									sendCustZone = drTmp["zone_code"].ToString().Trim();
								}
							}
							tmpCustZone = SysDataMgrDAL.GetCustZoneData(appID, enterpriseID,
								0, 0, (String)Session["PickupCustomerID"], drCon[0]["RecPostalCode"].ToString().Trim()).ds;
							if(tmpCustZone.Tables[0].Rows.Count > 0)
							{
								foreach(DataRow drTmp in tmpCustZone.Tables[0].Rows)
								{
									recipCustZone = drTmp["zone_code"].ToString().Trim();
								}
							}

							float freightCharge = 0;
							float freightCharge0 = 0;
							float sumFreight = 0;
							float totFreight = 0;
							if(customer.IsCustomer_Box == "Y")
							{
								if(!customer.IsBoxRateAvailable(appID,enterpriseID,sendCustZone,recipCustZone,drCon[0]["ServiceType"].ToString().Trim()))
								{
									lblErrorMsg.Text = "Customer has no Box Rate defined.";
									return;
								}
								else
								{
									float weightToFreight = 0;
									for(int y=0;y<dr.Length;y++)
									{
										if(float.Parse(dr[y][11].ToString()) > float.Parse(dr[y][12].ToString()))
										{
											weightToFreight = float.Parse(dr[y][11].ToString())/int.Parse(dr[y][9].ToString());
										}
										else
										{
											weightToFreight = float.Parse(dr[y][12].ToString())/int.Parse(dr[y][9].ToString());
										}
										freightCharge = TIESUtility.ComputeFreightCharge(appID,enterpriseID,(String)Session["PickupCustomerID"],"C",
											strOrgZone,strDestnZone,weightToFreight , drCon[0]["ServiceType"].ToString(),
											(String)Session["SenderPostalCode"] , drCon[0]["RecPostalCode"].ToString());
										sumFreight = sumFreight+(freightCharge*int.Parse((dr[y][9]).ToString()));
									}
								
									if(sumPkgQty < int.Parse(customer.Minimum_Box))
									{
										freightCharge0 = (TIESUtility.ComputeFreightCharge(appID,enterpriseID,(String)Session["PickupCustomerID"],"C",
											strOrgZone,strDestnZone,0 , drCon[0]["ServiceType"].ToString(),
											(String)Session["SenderPostalCode"] , drCon[0]["RecPostalCode"].ToString()));
										totFreight = ((int.Parse(customer.Minimum_Box)-sumPkgQty)*freightCharge0)+sumFreight;
									}
									else
										totFreight = sumFreight;
								}
							}
							else
							{
								totFreight = TIESUtility.ComputeFreightCharge(appID,enterpriseID,(String)Session["PickupCustomerID"],"C",
									strOrgZone,strDestnZone,float.Parse(drCon[0]["ChgWt"].ToString()) , drCon[0]["ServiceType"].ToString(),
									(String)Session["SenderPostalCode"] , drCon[0]["RecPostalCode"].ToString());
							}

//							float freightCharge = TIESUtility.ComputeFreightCharge(appID,enterpriseID,(String)Session["PickupCustomerID"],"C",
//								strOrgZone,strDestnZone,float.Parse(drCon[0][11].ToString()),drCon[0][5].ToString(),
//								(String)Session["SenderPostalCode"],drCon[0][4].ToString());
							drCon[0][13] = EnterpriseRounding((decimal)(totFreight));

							decimal decESACharge = calCulateESA_ServiceType(drCon[0][4].ToString(),(String)Session["SenderPostalCode"],
								(String)Session["PickupCustomerID"],Convert.ToDecimal(freightCharge),drCon[0][5].ToString());
							drCon[0][17] = EnterpriseRounding(decESACharge);

							decimal OtherSurcharge = 0;
							if((customer.OtherSurchargeAmount == null || customer.OtherSurchargeAmount.ToString() == "") && (customer.OtherSurchargeAmountPercentage == null || customer.OtherSurchargeAmountPercentage.ToString() == ""))
							{
								OtherSurcharge = 0;
							}
							else
							{
								if(customer.OtherSurchargeAmount != "")
								{ 
									OtherSurcharge = Convert.ToDecimal(customer.OtherSurchargeAmount);
								}
								else if(customer.OtherSurchargeAmountPercentage != null && customer.OtherSurchargeAmountPercentage != "")
								{
									OtherSurcharge = Convert.ToDecimal(customer.OtherSurchargeAmountPercentage) * Convert.ToDecimal(freightCharge)/100;

									if(customer.OtherSurchargeMin != null && customer.OtherSurchargeMin != "")
									{
										OtherSurcharge = Math.Max(OtherSurcharge, Convert.ToDecimal(customer.OtherSurchargeMin));							
									}
									if(customer.OtherSurchargeMax != null && customer.OtherSurchargeMax != "")
									{
										OtherSurcharge = Math.Min(OtherSurcharge, Convert.ToDecimal(customer.OtherSurchargeMax));
									}
								}
							}
							drCon[0][15] = EnterpriseRounding(OtherSurcharge);						

							DomesticShipmentMgrBAL domesticBAL = new DomesticShipmentMgrBAL();
							decimal decInsSurchrg = domesticBAL.ComputeInsSurchargeByCust(appID,enterpriseID,(Decimal)drCon[0][6],(String)Session["PickupCustomerID"],customer);
							drCon[0][14] = decInsSurchrg;

							drCon[0][12] = Convert.ToDecimal(drCon[0][16].ToString().Trim()) + Convert.ToDecimal(drCon[0][14].ToString().Trim())+Convert.ToDecimal(drCon[0][13].ToString().Trim())+
								Convert.ToDecimal(drCon[0][17].ToString().Trim())+Convert.ToDecimal(drCon[0][15]);

							dtConTmp.ImportRow(drCon[0]);

							dtConsignment.Rows.Remove(drCon[0]);
						
							foreach(DataRow drConTmp in dtConTmp.Rows)
							{
								dtConsignment.ImportRow(drConTmp);
							}
							drCon = null;
							dtConTmp = null;
							drCon = dtConsignment.Select("","ConsignmentSeqNo ASC");
							dtConTmp = dtConsignment.Clone();
							if(drCon.Length > 0)
							{
								for(int n=0;n<drCon.Length;n++)
								{
									dtConTmp.ImportRow(drCon[n]);
								}
								dtConsignment.Rows.Clear();
								foreach(DataRow drConTmp2 in dtConTmp.Rows)
								{
									dtConsignment.ImportRow(drConTmp2);
								}
							}
							BindConsignmentGrid();
						}
					}
				}
			}
		}
		public void dgPackage_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			Label dgPackagetxtMPSNo = (Label)dgPackage.SelectedItem.FindControl("dgPackagetxtMPSNo");
			String strMPSno = dgPackagetxtMPSNo.Text;
		}
		//-------------------------------------Vas----------------------------------------------------------
		protected void dgVAS_Edit(object sender, DataGridCommandEventArgs e)
		{
			this.lblErrorMsg.Text="";
			if((bool)(ViewState["InsertdgVAS"]) == false)
			{
				ViewState["InsertdgVAS"] = false;
				e.Item.Cells[0].Enabled = false;
				dgVAS.EditItemIndex = e.Item.ItemIndex;
				BindVASGrid();
			}
		}

		protected void dgVAS_Cancel(object sender, DataGridCommandEventArgs e)
		{
			this.lblErrorMsg.Text="";
			dgVAS.EditItemIndex = -1;
			if((bool)ViewState["InsertdgVAS"] == true)
			{
				DataRow drCurrent = dtVASTmp.Rows[GetCurrentIndexVAS(e.Item.ItemIndex)];
				drCurrent.Delete();
			}

			ViewState["InsertdgVAS"] = false;
			BindVASGrid();
		}

		protected void dgVAS_Delete(object sender, DataGridCommandEventArgs e)
		{
			this.lblErrorMsg.Text="";
			if((bool)ViewState["InsertdgVAS"] == false)
			{
				String strConsignmentSeq = (String)Session["consignmentSeq"];

				dtVASTmp.Rows.RemoveAt(GetCurrentIndexVAS(e.Item.ItemIndex));

				ViewState["VIEWSTATE_DT3Tmp"] = dtVASTmp;

				DataRow[] drSelected = dtVAS.Select(" ConsignmentSeqNo = '"+strConsignmentSeq+"'");
	
				if(drSelected.Length>0)
				{	
					for(int i=0;i<drSelected.Length;i++)
					{
						dtVAS.Rows.Remove(drSelected[i]);
					}
				}

				dtVAS.AcceptChanges();

				foreach(DataRow drVAS in dtVASTmp.Rows)
				{
					dtVAS.ImportRow(drVAS);
				}
				
				ViewState["VIEWSTATE_DT3"] = dtVAS;
				ViewState["InsertdgVAS"] = false;

				int cnt = dtVASTmp.Rows.Count;
				decimal decTotSurcharge = 0.00M;
				for(int n=0;n<cnt;n++)
				{
					DataRow drSurcharge = dtVASTmp.Rows[n];
					decTotSurcharge += Convert.ToDecimal(drSurcharge["vas_surcharge"].ToString().Trim());
				}
				DataRow[] drCon = dtConsignment.Select(" ConsignmentSeqNo = '"+strConsignmentSeq+"'");
				DataTable dtConTmp = dtConsignment.Clone();
				if(drCon.Length > 0)
				{
					drCon[0][16] = decTotSurcharge;
					drCon[0][12] = Convert.ToDecimal(drCon[0][16].ToString().Trim()) + Convert.ToDecimal(drCon[0][14].ToString().Trim())+Convert.ToDecimal(drCon[0][13].ToString().Trim())+
						Convert.ToDecimal(drCon[0][17].ToString().Trim())+Convert.ToDecimal(drCon[0][15]);
				}
				dtConTmp.ImportRow(drCon[0]);

				dtConsignment.Rows.Remove(drCon[0]);
						
				foreach(DataRow drConTmp in dtConTmp.Rows)
				{
					dtConsignment.ImportRow(drConTmp);
				}
				drCon = null;
				dtConTmp = null;
				drCon = dtConsignment.Select("","ConsignmentSeqNo ASC");
				dtConTmp = dtConsignment.Clone();
				if(drCon.Length > 0)
				{
					for(int n=0;n<drCon.Length;n++)
					{
						dtConTmp.ImportRow(drCon[n]);
					}
					dtConsignment.Rows.Clear();
					foreach(DataRow drConTmp2 in dtConTmp.Rows)
					{
						dtConsignment.ImportRow(drConTmp2);
					}
				}
				ViewState["VIEWSTATE_DT"] = dtConsignment;
				BindConsignmentGrid();

				dgVAS.EditItemIndex = -1;
				BindVASGrid();
			}
		}

		protected void dgVAS_PageChange(object sender, DataGridPageChangedEventArgs e)
		{
			this.lblErrorMsg.Text="";
			if((bool)ViewState["InsertdgVAS"] == true)	 // 2009-12-08 by poo, delete current row insert mode
			{
				DataRow drCurrent = dtVASTmp.Rows[dtVASTmp.Rows.Count - 1];
				drCurrent.Delete();
				dtVASTmp.AcceptChanges();
				ViewState["InsertdgVAS"] = false; 
			}
			dgVAS.EditItemIndex = -1; // end delete current row insert mode
			dgVAS.CurrentPageIndex = e.NewPageIndex;
			BindVASGrid();
		}
	
		public void dgVAS_Update(object sender, DataGridCommandEventArgs e)
		{
			this.lblErrorMsg.Text = "";
			msTextBox dgVAStxtVAS = (msTextBox)e.Item.FindControl("dgVAStxtVAS");
			msTextBox dgVAStxtDescription = (msTextBox)e.Item.FindControl("dgVAStxtDescription");
			String strConsignmentSeq = (String)Session["consignmentSeq"];
			if(dgVAStxtVAS.Text.Trim() == "")
			{
				lblErrorMsg.Text =Utility.GetLanguageText(ResourceType.UserMessage,"INVALVAS_BLANK",utility.GetUserCulture());
				return;
			}
			else if(dgVAStxtDescription.Text.Trim() == "")
			{
				lblErrorMsg.Text =Utility.GetLanguageText(ResourceType.UserMessage,"INVALDESC_BLANK",utility.GetUserCulture());
				return;
			}
//				VAS vas = new VAS();
//				vas.Populate(appID,enterpriseID,dgVAStxtVAS.Text.Trim());
//				strChkVasCode = vas.VASCode;

//				if(strChkVasCode == null)
//				{
//					lblErrorMsg.Text =Utility.GetLanguageText(ResourceType.UserMessage,"INVALID_VAS",utility.GetUserCulture());
//					return;
//				}
//				else
//				{
//					//Check whether this VAS service is available.
//					bool isExcluded = TIESUtility.IsVASExcluded(appID,enterpriseID,txtdgVASCode.Text.Trim(),txtRecipZip.Text.Trim());
//					if(isExcluded)
//					{
//						lblErrorMsg.Text =Utility.GetLanguageText(ResourceType.UserMessage,"NO_RECORD_FOUND_VAS",utility.GetUserCulture());
//						return;
//					}
//
//				}
//			}
			
			try
			{
				msTextBox dgVAStxtSurcharge = (msTextBox)e.Item.FindControl("dgVAStxtSurcharge");
				int k = (e.Item.ItemIndex+((dgVAS.CurrentPageIndex)*dgVAS.PageSize));
				if(dgVAStxtSurcharge != null)
				{
					//					if(Convert.ToDecimal(txtSurcharge.Text) == 0)
					//					{
					//						lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"PLS_ENTER_SUR_AMT",utility.GetUserCulture());
					//						return;
					//					}
					DataRow drCurrent = dtVASTmp.Rows[k];
					//decimal TmpSurcharge = TIESUtility.EnterpriseRounding(Convert.ToDecimal(dgVAStxtSurcharge.Text),(int)ViewState["wt_rounding_method"],(decimal)ViewState["wt_increment_amt"]);
					//txtSurcharge = String.Format((String)ViewState["m_format"],TmpSurcharge);
					//decimal TmpSurcharge = Convert.ToDecimal(txtSurcharge.Text);
					drCurrent["vas_surcharge"] = dgVAStxtSurcharge.Text;
				}
			
				//msTextBox dgVAStxtVAS = (msTextBox)e.Item.FindControl("dgVAStxtVAS");
				String strVASCode = null;
				if(dgVAStxtVAS != null)
				{
					DataRow drCurrent = dtVASTmp.Rows[k];
					drCurrent["vas_code"] = dgVAStxtVAS.Text;
					strVASCode = dgVAStxtVAS.Text;
				}

				if(dgVAStxtDescription != null)
				{
					DataRow drCurrent = dtVASTmp.Rows[k];
					if((dgVAStxtDescription.Text.Length == 0) && (strVASCode != null))
					{
						VAS vas = new VAS();
						vas.Populate(appID,enterpriseID,strVASCode);
						drCurrent["vas_description"] = vas.VasDescription;
					}
					else
					{
						drCurrent["vas_description"] = dgVAStxtDescription.Text;
					}

					drCurrent["consignmentSeqNo"] = (String)Session["consignmentSeq"];
				}

				int cnt = dtVASTmp.Rows.Count;
				decimal decTotSurcharge = 0.00M;
				for(int n=0;n<cnt;n++)
				{
					DataRow drSurcharge = dtVASTmp.Rows[n];
					decTotSurcharge += Convert.ToDecimal(drSurcharge["vas_surcharge"].ToString().Trim());
				}
				DataRow[] drCon = dtConsignment.Select(" ConsignmentSeqNo = '"+strConsignmentSeq+"'");
				DataTable dtConTmp = dtConsignment.Clone();
				if(drCon.Length > 0)
				{
					drCon[0][16] = decTotSurcharge;
					drCon[0][12] = Convert.ToDecimal(drCon[0][16].ToString().Trim()) + Convert.ToDecimal(drCon[0][14].ToString().Trim())+Convert.ToDecimal(drCon[0][13].ToString().Trim())+
						Convert.ToDecimal(drCon[0][17].ToString().Trim())+Convert.ToDecimal(drCon[0][15]);
				}
				dtConTmp.ImportRow(drCon[0]);

				dtConsignment.Rows.Remove(drCon[0]);
						
				foreach(DataRow drConTmp in dtConTmp.Rows)
				{
					dtConsignment.ImportRow(drConTmp);
				}
				drCon = null;
				dtConTmp = null;
				drCon = dtConsignment.Select("","ConsignmentSeqNo ASC");
				dtConTmp = dtConsignment.Clone();
				if(drCon.Length > 0)
				{
					for(int n=0;n<drCon.Length;n++)
					{
						dtConTmp.ImportRow(drCon[n]);
					}
					dtConsignment.Rows.Clear();
					foreach(DataRow drConTmp2 in dtConTmp.Rows)
					{
						dtConsignment.ImportRow(drConTmp2);
					}
				}
				BindConsignmentGrid();
			}
			catch(System.Data.ConstraintException appConstraintExp)
			{
				String msg = appConstraintExp.ToString();
				//lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"DUP_KEY_FOUND_VAS",utility.GetUserCulture());
				return;
			}

//			decimal totSurcharge = GetTotalSurcharge();
//			txtTotVASSurChrg.Text = String.Format((String)ViewState["m_format"], totSurcharge);
//				
//			CalculateTotalAmt();

			//Session["SESSION_DS3"] = m_dsVAS;
			ViewState["VIEWSTATE_DT3Tmp"] = dtVASTmp;

			DataRow[] drSelected = dtVAS.Select(" ConsignmentSeqNo = '"+strConsignmentSeq+"'");
	
			if(drSelected.Length>0)
			{	
				for(int i=0;i<drSelected.Length;i++)
				{
					dtVAS.Rows.Remove(drSelected[i]);
				}
			}

			dtVAS.AcceptChanges();

			foreach(DataRow drVAS in dtVASTmp.Rows)
			{
				dtVAS.ImportRow(drVAS);
			}
				
			ViewState["VIEWSTATE_DT3"] = dtVAS;
			ViewState["InsertdgVAS"] = false;

			dgVAS.EditItemIndex = -1;
			BindVASGrid();
		}
		public void dgVAS_ItemDataBound(object sender, DataGridItemEventArgs e)
		{
			if(e.Item.ItemType == ListItemType.EditItem)
			{
				e.Item.Cells[3].Enabled = true;	
			}
			else
			{
				e.Item.Cells[3].Enabled = false;
			}
		}
		
		public void dgVAS_Button(object sender, DataGridCommandEventArgs e)
		{
			lblErrorMsg.Text = "";
			if(this.dgVAS.EditItemIndex==-1)
				return;

			String strCmdNm = e.CommandName;
			if(strCmdNm.Equals("Search"))
			{
				String strSelectedRecZipCode = "";
				if(this.dgConsignment.SelectedIndex!=-1)
				{
					Label lblSelectedRecZipCode = (Label)this.dgConsignment.SelectedItem.FindControl("dgConsignmentlblRecipientPostCode");
					if(lblSelectedRecZipCode!=null)
						strSelectedRecZipCode = lblSelectedRecZipCode.Text.Trim();
				}

				msTextBox dgVAStxtVAS = (msTextBox)e.Item.FindControl("dgVAStxtVAS");
				String strtxtVASCode = null;
				if(dgVAStxtVAS != null)
				{
					strtxtVASCode = dgVAStxtVAS.ClientID;
				}
				
				msTextBox dgVAStxtDescription = (msTextBox)e.Item.FindControl("dgVAStxtDescription");
				String strtxtVASDesc = null;
				if (dgVAStxtDescription != null)
				{
					strtxtVASDesc = dgVAStxtDescription.ClientID;
				}

				msTextBox dgVAStxtSurcharge = (msTextBox)e.Item.FindControl("dgVAStxtSurcharge");
				String strtxtSurcharge = null;
				if (dgVAStxtSurcharge != null)
				{
					strtxtSurcharge = dgVAStxtSurcharge.ClientID;
				}
				String sUrl = "VASPopup.aspx?VASID="+strtxtVASCode+"&VASDESC="+strtxtVASDesc+"&VASSURCHARGE="+strtxtSurcharge+"&FORMID="+"PackageDetails"+"&DestZipCode="+strSelectedRecZipCode;
				String sScript ="";
				sScript ="<script language=javascript>window.name='PickupRequest_Conrate';window.open('"+sUrl+"','','height=550,width=800,left=100,top=50,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,titlebar=yes,toolbar=no');";
				sScript += "</script>";
				Response.Write(sScript);
			}
			
		}


		public DataRow UpdateConsignment(DataRow drCurrent) // Add by poo, 2009-12-04
		{
			
			Customer customer = new Customer();
			if (Session["SS_Customer"] == null)
			{
				customer.Populate(appID,enterpriseID, (String)Session["PickupCustomerID"]);
				Session.Add("SS_Customer",customer);
			}
			else
			{
				customer= (Customer)Session["SS_Customer"];
			}

			Zipcode zipcode = new Zipcode();
			string strOrgZone , strDestnZone;
			if (ViewState["ORG_Zone"] == null || ViewState["DES_Zone"] == null || ViewState["ORG_Zone"].ToString() == string.Empty || ViewState["DES_Zone"].ToString() == string.Empty)
			{
				zipcode.Populate(appID , enterpriseID , (String)Session["SenderPostalCode"]);
				strOrgZone = zipcode.ZoneCode;
				ViewState["ORG_Zone"] = zipcode.ZoneCode;
				zipcode.Populate(appID , enterpriseID , drCurrent["RecPostalCode"].ToString().Trim());
				strDestnZone = zipcode.ZoneCode;
				ViewState["DES_Zone"] = zipcode.ZoneCode;
			} 
			else
			{
				strOrgZone = (string)ViewState["ORG_Zone"];
				strDestnZone =(string)ViewState["DES_Zone"];
			}

			DomesticShipmentMgrBAL domesticBAL = new DomesticShipmentMgrBAL();
			decimal decInsSurchrg , decESACharge , OtherSurcharge;
			float freightCharge;

			// calculate InsSurch. By poo , 2009-12-03
			decInsSurchrg = domesticBAL.ComputeInsSurchargeByCust(appID,enterpriseID
				, (Decimal)drCurrent["DeclaredValue"] , (String)Session["PickupCustomerID"] , customer);
			// calculate freightCharge. By poo , 2009-12-03
			freightCharge = TIESUtility.ComputeFreightCharge(appID,enterpriseID,(String)Session["PickupCustomerID"],"C",
				strOrgZone , strDestnZone , float.Parse(drCurrent["ChgWt"].ToString()) , drCurrent["ServiceType"].ToString() , 
				(String)Session["SenderPostalCode"] , drCurrent["RecPostalCode"].ToString());
			// calculate decESACharge. By poo , 2009-12-03
			decESACharge = calCulateESA_ServiceType(drCurrent["RecPostalCode"].ToString() , (String)Session["SenderPostalCode"] ,
				(String)Session["PickupCustomerID"] , Convert.ToDecimal(freightCharge) , drCurrent["ServiceType"].ToString());
			// calculate OtherSurcharge. By poo , 2009-12-03
			OtherSurcharge = 0;
			if((customer.OtherSurchargeAmount == null || customer.OtherSurchargeAmount.ToString() == "") && (customer.OtherSurchargeAmountPercentage == null || customer.OtherSurchargeAmountPercentage.ToString() == ""))
			{
				OtherSurcharge = 0;
			}
			else
			{
				if(customer.OtherSurchargeAmount != "")
				{ 
					OtherSurcharge = Convert.ToDecimal(customer.OtherSurchargeAmount);
				}
				else if(customer.OtherSurchargeAmountPercentage != null && customer.OtherSurchargeAmountPercentage != "")
				{
					OtherSurcharge = Convert.ToDecimal(customer.OtherSurchargeAmountPercentage) * Convert.ToDecimal(freightCharge)/100;

					if(customer.OtherSurchargeMin != null && customer.OtherSurchargeMin != "")
					{
						OtherSurcharge = Math.Max(OtherSurcharge, Convert.ToDecimal(customer.OtherSurchargeMin));							
					}
					if(customer.OtherSurchargeMax != null && customer.OtherSurchargeMax != "")
					{
						OtherSurcharge = Math.Min(OtherSurcharge, Convert.ToDecimal(customer.OtherSurchargeMax));
					}
				}
			}

			drCurrent["InsSurch"] = decInsSurchrg; 
			drCurrent["FreightCharge"] = EnterpriseRounding((decimal)(freightCharge));
			drCurrent["ESASurch"] = EnterpriseRounding(decESACharge);
			drCurrent["OtherSurch"] = EnterpriseRounding(OtherSurcharge);

			return drCurrent; // end poo
		}
		private int GetCurrentIndexConsignment(int intIndex)
		{
			int currentIndex = intIndex + (dgConsignment.CurrentPageIndex * dgConsignment.PageSize);
			int checkMOD = ((dgConsignment.Items.Count - 1) % dgConsignment.PageSize);
			if (checkMOD == 0 && currentIndex >= dgConsignment.PageSize) // set new current page Index. by poo 2009-12-08
				dgConsignment.CurrentPageIndex = dgConsignment.CurrentPageIndex - 1;

			return currentIndex;
		}
		private int GetCurrentIndexPackage(int intIndex)
		{
			int currentIndex = intIndex + (dgPackage.CurrentPageIndex * dgPackage.PageSize);
			int checkMOD = ((dgPackage.Items.Count - 1) % dgPackage.PageSize);
			if (checkMOD == 0) // set new current page Index. by poo 2009-12-08
				dgPackage.CurrentPageIndex = dgPackage.CurrentPageIndex - 1;

			return currentIndex;
		}
		private int GetCurrentIndexVAS(int intIndex)
		{
			int currentIndex = intIndex + (dgVAS.CurrentPageIndex * dgVAS.PageSize);
			int checkMOD = ((dgVAS.Items.Count - 1) % dgVAS.PageSize);
			if (checkMOD == 0 && currentIndex >= dgVAS.PageSize) // set new current page Index. by poo 2009-12-08
				dgVAS.CurrentPageIndex = dgVAS.CurrentPageIndex - 1;

			return currentIndex;
		}
		#endregion
		
		#region Methods
		private void BindConsignmentGrid()
		{
			dgConsignment.DataSource = dtConsignment;
			dgConsignment.DataBind();
			int totPkg = 0;
			decimal totActWeight = 0.00M;
			decimal totDimWeight = 0.00M;
			decimal totChgWeight = 0.00M;
			decimal totCharge = 0.00M;
			decimal totFreight = 0.00M;
			decimal totIns = 0.00M;
			decimal totOther = 0.00M;
			decimal totVAS = 0.00M;
			decimal totESA = 0.00M;

			foreach(DataRow dr in dtConsignment.Rows)
			{
				totPkg += Convert.ToInt32(dr["TotalPkg"].ToString().Trim());
				totActWeight += Convert.ToDecimal(dr["ActualWt"].ToString().Trim());
				totDimWeight += Convert.ToDecimal(dr["DimWt"].ToString().Trim());
				totCharge += Convert.ToDecimal(dr["ChgWt"].ToString().Trim());
				totChgWeight += Convert.ToDecimal(dr["TotalCharge"].ToString().Trim());
				totFreight += Convert.ToDecimal(dr["FreightCharge"].ToString().Trim());
				totIns += Convert.ToDecimal(dr["InsSurch"].ToString().Trim());
				totOther += Convert.ToDecimal(dr["OtherSurch"].ToString().Trim());
				totVAS += Convert.ToDecimal(dr["VASSurch"].ToString().Trim());
				totESA += Convert.ToDecimal(dr["ESASurch"].ToString().Trim());
			}
			txtHeader_TotalPkgs.Text = totPkg.ToString();
			txtHeader_ActualWeight.Text = String.Format("{0:0.00}",totActWeight);
			txtHeader_DimWeight.Text = String.Format("{0:0.00}",totDimWeight);
			txtHeader_ChgWeight.Text = String.Format("{0:0.00}",totCharge);
			txtHeader_TotalCharge.Text = String.Format("{0:0.00}",totChgWeight);
			txtHeader_FreightCharge.Text = String.Format("{0:0.00}",totFreight);
			txtHeader_InsSurch.Text = String.Format("{0:0.00}",totIns);
			txtHeader_OtherSurch.Text = String.Format("{0:0.00}",totOther);
			txtHeader_VASSurch.Text = String.Format("{0:0.00}",totVAS);
			txtHeader_ESASurch.Text = String.Format("{0:0.00}",totESA);
//			ViewState["VIEWSTATE_DT"] = dtConsignment;
		}
		private void BindPkgGrid()
		{
			dgPackage.DataSource = dtPkgTmp;
			dgPackage.DataBind();
//			ViewState["VIEWSTATE_DT2"] = dtPackage;
		}
		private void BindVASGrid()
		{
			dgVAS.DataSource = dtVASTmp;
			dgVAS.DataBind();
//			ViewState["VIEWSTATE_DT3"] = dtVAS;
		}

		private void AddRowInConsignmentGrid()
		{
			try
			{
				dtConsignment = (DataTable)ViewState["VIEWSTATE_DT"];
				PRBMgrDAL.AddNewRowInConsignmentDT(dtConsignment);
			}
			catch(System.Data.ConstraintException appConstraintExp)
			{
				String msg = appConstraintExp.ToString();
				return;
			}
			ViewState["VIEWSTATE_DT"]= dtConsignment;
			
		}
		private void AddRowInPkgGrid()
		{
			try
			{
				dtPkgTmp = (DataTable)ViewState["VIEWSTATE_DT2Tmp"];
				PRBMgrDAL.AddNewRowInPackageDT(dtPkgTmp);
			}
			catch(System.Data.ConstraintException appConstraintExp)
			{
				String msg = appConstraintExp.ToString();
				return;
			}
			ViewState["VIEWSTATE_DT2Tmp"]= dtPkgTmp;
		}
		private void AddRowInVASGrid()
		{
			try
			{
				dtVASTmp = (DataTable)ViewState["VIEWSTATE_DT3Tmp"];
				PRBMgrDAL.AddNewRowInVASDT(dtVASTmp);
			}
			catch(System.Data.ConstraintException appConstraintExp)
			{
				String msg = appConstraintExp.ToString();
				return;
			}
			ViewState["VIEWSTATE_DT3"]= dtVAS;
		}
		private decimal EnterpriseRounding(decimal beforeRound)
		{
			int wt_rounding_method = 0;
			decimal wt_increment_amt = 1;
			decimal reVal = beforeRound;

			if (ViewState["wt_rounding_method"] != null)
				wt_rounding_method = (int)ViewState["wt_rounding_method"];

			if (ViewState["wt_increment_amt"] != null)
				wt_increment_amt = (decimal)ViewState["wt_increment_amt"];
		
			decimal BeforDot = 0;
			decimal AfterDot = 0;
			decimal tmp05 = Convert.ToDecimal(0.5);
			string strBeforeRound = beforeRound.ToString();
			int dotIndex = Convert.ToInt32(strBeforeRound.IndexOf(".", 0));

			if(dotIndex < 0) 
			{
				BeforDot = Convert.ToDecimal(strBeforeRound);
				AfterDot = Convert.ToDecimal("0.00");
			}
			else 
			{ 
				BeforDot = Convert.ToDecimal(strBeforeRound.Substring(0, dotIndex));
				AfterDot = Convert.ToDecimal("0." + strBeforeRound.Substring(dotIndex + 1, strBeforeRound.Length - (dotIndex + 1)));
			}

			if(wt_increment_amt == tmp05)
			{
				if(AfterDot == tmp05) 
				{
					reVal = beforeRound;
				}
				else if (AfterDot > tmp05)
				{
					if(wt_rounding_method == 0)
						reVal = BeforDot + 1;
					else if (wt_rounding_method == 1)
						reVal = BeforDot + 1;
					else if (wt_rounding_method == 2)
						reVal = BeforDot + tmp05;
				}
				else if (AfterDot < tmp05)
				{
					if(wt_rounding_method == 0)
						reVal = BeforDot + tmp05;
					else if (wt_rounding_method == 1)
						reVal = BeforDot + tmp05;
					else if (wt_rounding_method == 2)
						reVal = BeforDot;
				}
			}
			else if(wt_increment_amt == 1)
			{
				if(AfterDot == tmp05) 
				{
					if(wt_rounding_method == 0)
						reVal = BeforDot + 1;
					else if (wt_rounding_method == 1)
						reVal = BeforDot + 1;
					else if (wt_rounding_method == 2)
						reVal = BeforDot;
				}
				else if (AfterDot > tmp05)
				{
					if(wt_rounding_method == 0)
						reVal = BeforDot + 1;
					else if (wt_rounding_method == 1)
						reVal = BeforDot + 1;
					else if (wt_rounding_method == 2)
						reVal = BeforDot;
				}
				else if (AfterDot < tmp05)
				{
					if(wt_rounding_method == 0)
						reVal = BeforDot;
					else if (wt_rounding_method == 1)
						reVal = BeforDot + 1;
					else if (wt_rounding_method == 2)
						reVal = BeforDot;
				}
			}

			return reVal;
		}
		private void GetGridValuesTotal()
		{
			int cnt = dtConsignment.Rows.Count;
			int i = 0;

			for(i = 0;i<cnt;i++)
			{
				DataRow drEach = dtConsignment.Rows[i];
				totDimWt += Convert.ToDecimal(drEach["DimWt"].ToString().Trim());
				if (drEach["ActualWt"] == System.DBNull.Value )
				{
					drEach["ActualWt"]= 0	;
				}
				totActWt += Convert.ToDecimal(drEach["ActualWt"].ToString().Trim());
				totPkgs += Convert.ToInt32(drEach["TotalPkg"].ToString().Trim());
				totSurcharge += Convert.ToDecimal(drEach["TotalCharge"].ToString().Trim());
			}
		}
		private DataSet  LoadService(int rowIndex, String receipPostCode)
		{			
			DataSet dsService = new DataSet();
			DataTable dtService = new DataTable();
			dtService.Columns.Add("ServiceText",typeof(string));
			dtService.Columns.Add("ServiceValue",typeof(string));
			string strPostCode = "";
			
			if(rowIndex != -1)
			{
				DataRow rows = dtConsignment.Rows[rowIndex];
				strPostCode = (string)rows[1];
			}
		
			ArrayList serviceCodes = GetServiceValues(appID, enterpriseID, receipPostCode);
			DataRow dr; 
			//			DataRow dr =  dtDlvType.NewRow();
			//			dr[1] = System.DBNull.Value;
			//			dr[0] = System.DBNull.Value;
			//			dtDlvType.Rows.Add(dr); 
//			if((bool)ViewState["InsertdgConsignment"] == false)
//			{
//				dr = dtService.NewRow();
//				dr[0] = "";
//				dr[1] = "";
//				dtService.Rows.Add(dr);
//			}
			if(serviceCodes != null)
			{
				foreach(SystemCode serviceCode in serviceCodes)
				{
					dr = dtService.NewRow();
					dr[1] = serviceCode.Text;
					dr[0] = serviceCode.StringValue;
					dtService.Rows.Add(dr); 			
				}
			}
			dsService.Tables.Add(dtService);
			int cnt = dsService.Tables [0].Rows.Count; 
			return dsService;
		}
		private ArrayList GetServiceValues(String strAppID, String strEnterpriseID, String strDestZipCode)
		{
			ArrayList codeValues = null;

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(appID);

			if(dbCon == null)
			{
				Logger.LogTraceError("Utility","GetServiceValues","EDB101","DbConnection object is null!!");
				System.Diagnostics.Trace.WriteLineIf(Logger.AppSwitch.TraceError,"[Utility::GetCodeValues()] DbConnection is null","ERROR");
				return codeValues;
			}
			

			//			String strSQLQuery = "select distinct(service_code) from Service ";
			//			strSQLQuery += "where applicationid = '"+appID+"' and enterpriseid = '"+enterpriseID+"'"; 
			StringBuilder strQry = new StringBuilder();
			if(strDestZipCode != "")
			{
				strSendZipCode = (String)Session["SenderPostalCode"];
				//				StringBuilder strQry = new StringBuilder();
				DataSet tmpDC = DomesticShipmentMgrDAL.getDCToOrigStation(appID, enterpriseID,
					0, 0, strSendZipCode.Trim()).ds;

				String strOriginDC = tmpDC.Tables[0].Rows[0]["origin_station"].ToString();

				tmpDC.Dispose();
				tmpDC = null;

				//				strQry.Append("select service_code, transit_time,service_description,service_charge_percent,service_charge_amt from (");
				//				strQry.Append(" Select distinct Service.service_code, ISNULL(Service.transit_day * 24, 0) + ISNULL(Service.transit_hour, 0) AS transit_time, ");
				//				strQry.Append(" Service.service_description, Service.service_charge_percent, Service.service_charge_amt, convert(varchar(5),Service.commit_time,108) as commit_time ");
				//				strQry.Append(" FROM   Zipcode INNER JOIN   Delivery_Path ON Zipcode.applicationid = Delivery_Path.applicationid "); 
				//				strQry.Append(" AND Zipcode.enterpriseid = Delivery_Path.enterpriseid AND Zipcode.pickup_route = Delivery_Path.path_code ");
				//				strQry.Append(" INNER JOIN  Service ON Delivery_Path.applicationid = Service.applicationid AND Delivery_Path.enterpriseid = Service.enterpriseid ");
				//				strQry.Append(" WHERE (Zipcode.zipcode = '"+strSendZipCode+"') and Service.Service_code not in (select distinct service_code ");
				//				strQry.Append(" from Zipcode_Service_Excluded where Zipcode_Service_Excluded.zipcode ='"+strDestZipCode+"') ");
				//				strQry.Append(" AND Zipcode.applicationid='"+appID+"' AND Zipcode.enterpriseid='"+enterpriseID+"' ");
				//				strQry.Append(" and Service.Service_code not in (Select Best_Service_Available.service_code");
				//				strQry.Append(" from Best_Service_Available inner join Service on Best_Service_Available.service_code = Service.service_code");
				//				strQry.Append(" Where Best_Service_Available.origin_dc ='"+strOriginDC+"'  and Best_Service_Available.recipientZipCode ='"+strDestZipCode+"'  )");	
				//				strQry.Append(" union Select Best_Service_Available.service_code, -1, Service.service_description, Service.service_charge_percent, Service.service_charge_amt , convert(varchar(5),Service.commit_time,108) as commit_time ");
				//				strQry.Append(" from Best_Service_Available inner join Service on Best_Service_Available.service_code = Service.service_code ");
				//				strQry.Append(" Where Best_Service_Available.origin_dc ='"+strOriginDC+"' and Best_Service_Available.recipientZipCode ='"+strDestZipCode+"') Service Where service_code not in (select distinct service_code from Zipcode_Service_Excluded where Zipcode_Service_Excluded.zipcode ='"+strDestZipCode+"') ");
				//				strQry.Append(" ORDER BY transit_time, commit_time");

				strQry.Append(" Select 0 as BSA,applicationid,enterpriseid,service_code, ISNULL(Service.transit_day * 24, 0) + ISNULL(Service.transit_hour, 0) AS transit_time");
				strQry.Append(",service_description,service_charge_percent,service_charge_amt, convert(varchar(5),Service.commit_time,108) as commited_time from Service");
				strQry.Append(" Where service_code in (select service_code from Best_Service_Available");
				strQry.Append(" where origin_dc = '"+Utility.ReplaceSingleQuote(strOriginDC)+"'");
				strQry.Append(" and recipientZipCode = '"+Utility.ReplaceSingleQuote(strDestZipCode)+"')");
				strQry.Append(" Union");
				strQry.Append(" select 1 as BSA,* from (Select applicationid,enterpriseid,service_code, ISNULL(Service.transit_day * 24, 0) + ISNULL(Service.transit_hour, 0) AS transit_time");
				strQry.Append(",service_description,service_charge_percent,service_charge_amt, convert(varchar(5),Service.commit_time,108) as commited_time from Service) SR");
				strQry.Append(" Where (((SR.commited_time >= (select convert(varchar(5),Service.commit_time,108) as commited_time");
				strQry.Append(" from Service where service_code in (select service_code from Best_Service_Available");
				strQry.Append(" where origin_dc = '"+Utility.ReplaceSingleQuote(strOriginDC)+"'");
				strQry.Append(" and recipientZipCode = '"+Utility.ReplaceSingleQuote(strDestZipCode)+"')))");
				strQry.Append(" AND (SR.transit_time = (select ISNULL(Service.transit_day * 24, 0) + ISNULL(Service.transit_hour, 0) AS transit_time");
				strQry.Append(" from Service where service_code in (select service_code from Best_Service_Available");
				strQry.Append(" where origin_dc = '"+Utility.ReplaceSingleQuote(strOriginDC)+"'");
				strQry.Append(" and recipientZipCode = '"+Utility.ReplaceSingleQuote(strDestZipCode)+"'))))");
				strQry.Append(" or (SR.transit_time > (select ISNULL(Service.transit_day * 24, 0) + ISNULL(Service.transit_hour, 0) AS transit_time");
				strQry.Append(" from Service where service_code in (select service_code from Best_Service_Available");
				strQry.Append(" where origin_dc = '"+Utility.ReplaceSingleQuote(strOriginDC)+"'");
				strQry.Append(" and recipientZipCode = '"+Utility.ReplaceSingleQuote(strDestZipCode)+"')))) ");
				strQry.Append(" and SR.service_code not in (select service_code from Best_Service_Available");
				strQry.Append(" where origin_dc = '"+Utility.ReplaceSingleQuote(strOriginDC)+"'");
				strQry.Append(" and recipientZipCode = '"+Utility.ReplaceSingleQuote(strDestZipCode)+"')");
				strQry.Append(" AND SR.applicationid = '"+appID+"' and SR.enterpriseid = '"+enterpriseID+"'");
				//edit by Tumz.
				strQry.Append(" ORDER BY transit_time");
			}
			else
			{
				strQry.Append("select distinct(service_code),ISNULL(Service.transit_day * 24, 0) + ISNULL(Service.transit_hour, 0) AS transit_time from Service");
				strQry.Append(" where applicationid = '"+appID+"' and enterpriseid = '"+enterpriseID+"' Order By transit_time");
			}
			
			DataSet dsServiceCode = (DataSet)dbCon.ExecuteQuery(strQry.ToString(),ReturnType.DataSetType);

			int cnt = dsServiceCode.Tables[0].Rows.Count;

			if(cnt > 0)
			{
				codeValues = new ArrayList();
				
				for(int i=0; i < cnt; i++)
				{
					DataRow drEach = dsServiceCode.Tables[0].Rows[i];
					SystemCode systemCode = new SystemCode();

					if(!drEach["service_code"].GetType().Equals(System.Type.GetType("System.DBNull")))
					{
						systemCode.Text = (String) drEach["service_code"];
						systemCode.StringValue = (String) drEach["service_code"];
					}
					codeValues.Add(systemCode);
				}
			}

			return  codeValues;
		}
		#endregion

		private void dgConsignment_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
		{
			DropDownList DrpDlvType = (DropDownList)e.Item.FindControl("dgConsignmentddlServiceType");
			msTextBox dgConsignmenttxtRecipientPostCode = (msTextBox)e.Item.FindControl("dgConsignmenttxtRecipientPostCode");

			String strType=null;
			String receipPostCode = null;
			if(dgConsignmenttxtRecipientPostCode != null)
				receipPostCode = dgConsignmenttxtRecipientPostCode.Text.Trim();

			DataSet dsDlvType = LoadService(e.Item.ItemIndex,receipPostCode);
			if (DrpDlvType!=null)
			{
				DrpDlvType.DataSource = dsDlvType;
				DrpDlvType.DataTextField = "ServiceValue"; 
				DrpDlvType.DataValueField="ServiceText";
				DrpDlvType.DataBind();
			}

			if(e.Item.ItemIndex != -1)
			{
				DataRow drSelected = dtConsignment.Rows[e.Item.ItemIndex];			
			   
				if (DrpDlvType!=null)
				{
					strType = (String)drSelected["ServiceType"];
					DrpDlvType.SelectedIndex = DrpDlvType.Items.IndexOf(DrpDlvType.Items.FindByValue(strType));	

				}
			}
		}
		private decimal calCulateESA_ServiceType(String RecipZip, String SendZip,String custID, decimal FreightChrg, string svcCode)
		{

			decimal decSrchrg = 0;
			//Calculate the ESA surcharge
			if(RecipZip.Length > 0 && RecipZip != null && SendZip.Length > 0 && SendZip != null)
			{
		
				String strApplyDimWt = null;
				String strApplyESA = null;
				String strApplyESADel = null;

				Customer customer = new Customer();
				customer.Populate(appID, enterpriseID, custID);
				strApplyDimWt = customer.ApplyDimWt;
				strApplyESA = customer.ESASurcharge;
				strApplyESADel = customer.ESASurchargeDel;
				decimal surchargeOfSender = 0;
				decimal surchargeOfRecip = 0;

				if(strApplyESA == "Y")
				{					
					//***************** Get surcharge from Customer Zone by Sender Zipcode *************************
					DataSet tmpCustZone = SysDataManager2.GetESASurchargeDS(appID, enterpriseID,
						custID.Trim(), SendZip.Trim(),svcCode);
					//return ESA Surcharge All just send CustID and ZipCode 

					//					 tmpCustZone = SysDataMgrDAL.GetCustZoneData(appID, enterpriseID,
					//						0, 0, txtCustID.Text.Trim(), txtSendZip.Text.Trim()).ds;
					if(tmpCustZone.Tables[0].Rows.Count > 0)
					{
						//Modify by Gwang on 4-Feb-08
						//A both of them have a value
						if(((tmpCustZone.Tables[0].Rows[0]["esa_surcharge_amt"] != null) && 
							(!tmpCustZone.Tables[0].Rows[0]["esa_surcharge_amt"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
							(tmpCustZone.Tables[0].Rows[0]["esa_surcharge_amt"].ToString() != "")) &&
							((tmpCustZone.Tables[0].Rows[0]["esa_surcharge_percent"] != null) && 
							(!tmpCustZone.Tables[0].Rows[0]["esa_surcharge_percent"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
							(tmpCustZone.Tables[0].Rows[0]["esa_surcharge_percent"].ToString() != ""))
							)
						{
							//At least is more than zero
							if(((decimal)tmpCustZone.Tables[0].Rows[0]["esa_surcharge_amt"] != 0) || ((decimal)tmpCustZone.Tables[0].Rows[0]["esa_surcharge_percent"] != 0))
							{
								if((decimal)tmpCustZone.Tables[0].Rows[0]["esa_surcharge_amt"] != 0)
								{
									surchargeOfSender = (decimal)tmpCustZone.Tables[0].Rows[0]["esa_surcharge_amt"];
								}
								else if((decimal)tmpCustZone.Tables[0].Rows[0]["esa_surcharge_percent"] != 0)
								{

									surchargeOfSender = (FreightChrg * ((decimal)tmpCustZone.Tables[0].Rows[0]["esa_surcharge_percent"])) / 100;
								}

							}
								//A both of them is Zero.the surcharge is Zero
							else
							{
								surchargeOfSender = 0;								
							}
						}
							//At least one has a value
						else if(((tmpCustZone.Tables[0].Rows[0]["esa_surcharge_amt"] != null) && 
							(!tmpCustZone.Tables[0].Rows[0]["esa_surcharge_amt"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
							(tmpCustZone.Tables[0].Rows[0]["esa_surcharge_amt"].ToString() != "")) ||
							((tmpCustZone.Tables[0].Rows[0]["esa_surcharge_percent"] != null) && 
							(!tmpCustZone.Tables[0].Rows[0]["esa_surcharge_percent"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
							(tmpCustZone.Tables[0].Rows[0]["esa_surcharge_percent"].ToString() != ""))
							)
						{
							if((tmpCustZone.Tables[0].Rows[0]["esa_surcharge_amt"] != null) && 
								(!tmpCustZone.Tables[0].Rows[0]["esa_surcharge_amt"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
								(tmpCustZone.Tables[0].Rows[0]["esa_surcharge_amt"].ToString() != ""))
							{
								surchargeOfSender = (decimal)tmpCustZone.Tables[0].Rows[0]["esa_surcharge_amt"];
							}

							if((tmpCustZone.Tables[0].Rows[0]["esa_surcharge_percent"] != null) && 
								(!tmpCustZone.Tables[0].Rows[0]["esa_surcharge_percent"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
								(tmpCustZone.Tables[0].Rows[0]["esa_surcharge_percent"].ToString() != ""))
							{
								surchargeOfSender = (FreightChrg * ((decimal)tmpCustZone.Tables[0].Rows[0]["esa_surcharge_percent"])) / 100;
							}
						}
							//A both of them has Null/""
						else
						{	
							//							Zipcode zipcode = new Zipcode();
							//							zipcode.Populate(appID,enterpriseID,txtSendZip.Text.Trim());
							surchargeOfSender = 0;//zipcode.EASSurcharge;
						}
						//Apply MIN MAX
						if((tmpCustZone.Tables[0].Rows[0]["esa_surcharge_percent"] != null) && 
							(!tmpCustZone.Tables[0].Rows[0]["esa_surcharge_percent"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
							(tmpCustZone.Tables[0].Rows[0]["esa_surcharge_percent"].ToString() != ""))
						{
							if((decimal)tmpCustZone.Tables[0].Rows[0]["esa_surcharge_percent"] != 0)
							{

								if((tmpCustZone.Tables[0].Rows[0]["min_esa_amount"] != null) && 
									(!tmpCustZone.Tables[0].Rows[0]["min_esa_amount"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
									(tmpCustZone.Tables[0].Rows[0]["min_esa_amount"].ToString() != ""))
									if(surchargeOfSender<(decimal)tmpCustZone.Tables[0].Rows[0]["min_esa_amount"])					
										surchargeOfSender=(decimal)tmpCustZone.Tables[0].Rows[0]["min_esa_amount"];
							
								if((tmpCustZone.Tables[0].Rows[0]["max_esa_amount"] != null) && 
									(!tmpCustZone.Tables[0].Rows[0]["max_esa_amount"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
									(tmpCustZone.Tables[0].Rows[0]["max_esa_amount"].ToString() != ""))
									if(surchargeOfSender>(decimal)tmpCustZone.Tables[0].Rows[0]["max_esa_amount"])					
										surchargeOfSender=(decimal)tmpCustZone.Tables[0].Rows[0]["max_esa_amount"];
							}
						}
						//END Apply MIN MAX
					}
				
					else //IN CASE THERE IS NO RESULT OF ESA FROM DB
					{	
						//						Zipcode zipcode = new Zipcode();
						//						zipcode.Populate(appID,enterpriseID,txtSendZip.Text.Trim());
						surchargeOfSender = 0;//zipcode.EASSurcharge;
					}
				}
				else //IN CASE OF NO APPLY ESA
				{
					surchargeOfSender = 0;
				}

				if(strApplyESADel == "Y")
				{

					//***************** Get surcharge from Customer Zone by Recept Zipcode *************************
					DataSet tmpCustZone = SysDataManager2.GetESASurchargeDS(appID, enterpriseID,
						custID.Trim(), RecipZip.Trim(),svcCode);

					//					DataSet tmpCustZone = SysDataManager2.GetESASurchargeDS(utility.GetAppID(), utility.GetEnterpriseID(),
					//						txtCustID.Text.Trim(), RecipZip.Trim(),this.txtShpSvcCode.Text.Trim());

					//					DataSet tmpCustZone = SysDataMgrDAL.GetCustZoneData(appID, enterpriseID,
					//						0, 0, txtCustID.Text.Trim(), txtRecipZip.Text.Trim()).ds;
					if(tmpCustZone.Tables[0].Rows.Count > 0)
					{
						//Modify by Gwang on 4-Feb-08
						//A both of them have a value
						if(((tmpCustZone.Tables[0].Rows[0]["esa_surcharge_amt"] != null) && 
							(!tmpCustZone.Tables[0].Rows[0]["esa_surcharge_amt"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
							(tmpCustZone.Tables[0].Rows[0]["esa_surcharge_amt"].ToString() != "")) &&
							((tmpCustZone.Tables[0].Rows[0]["esa_surcharge_percent"] != null) && 
							(!tmpCustZone.Tables[0].Rows[0]["esa_surcharge_percent"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
							(tmpCustZone.Tables[0].Rows[0]["esa_surcharge_percent"].ToString() != ""))
							)
						{
							//A is more than zero
							if(((decimal)tmpCustZone.Tables[0].Rows[0]["esa_surcharge_amt"] != 0) || ((decimal)tmpCustZone.Tables[0].Rows[0]["esa_surcharge_percent"] != 0))
							{
								if((decimal)tmpCustZone.Tables[0].Rows[0]["esa_surcharge_amt"] != 0)
								{
									surchargeOfRecip = (decimal)tmpCustZone.Tables[0].Rows[0]["esa_surcharge_amt"];
								}
								else if((decimal)tmpCustZone.Tables[0].Rows[0]["esa_surcharge_percent"] != 0)
								{
									surchargeOfRecip = (FreightChrg * ((decimal)tmpCustZone.Tables[0].Rows[0]["esa_surcharge_percent"])) / 100;
								}

							}
								//A both of them is Zero.the surcharge is Zero
							else
							{
								surchargeOfRecip = 0;								
							}
						}
							//At least one has a value
						else if(((tmpCustZone.Tables[0].Rows[0]["esa_surcharge_amt"] != null) && 
							(!tmpCustZone.Tables[0].Rows[0]["esa_surcharge_amt"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
							(tmpCustZone.Tables[0].Rows[0]["esa_surcharge_amt"].ToString() != "")) ||
							((tmpCustZone.Tables[0].Rows[0]["esa_surcharge_percent"] != null) && 
							(!tmpCustZone.Tables[0].Rows[0]["esa_surcharge_percent"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
							(tmpCustZone.Tables[0].Rows[0]["esa_surcharge_percent"].ToString() != ""))
							)
						{
							if((tmpCustZone.Tables[0].Rows[0]["esa_surcharge_amt"] != null) && 
								(!tmpCustZone.Tables[0].Rows[0]["esa_surcharge_amt"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
								(tmpCustZone.Tables[0].Rows[0]["esa_surcharge_amt"].ToString() != ""))
							{
								surchargeOfRecip = (decimal)tmpCustZone.Tables[0].Rows[0]["esa_surcharge_amt"];
							}

							if((tmpCustZone.Tables[0].Rows[0]["esa_surcharge_percent"] != null) && 
								(!tmpCustZone.Tables[0].Rows[0]["esa_surcharge_percent"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
								(tmpCustZone.Tables[0].Rows[0]["esa_surcharge_percent"].ToString() != ""))
							{
								surchargeOfRecip = (FreightChrg * ((decimal)tmpCustZone.Tables[0].Rows[0]["esa_surcharge_percent"])) / 100;
							}
						}	
						else
						{	
							//							Zipcode zipcode = new Zipcode();
							//							zipcode.Populate(appID,enterpriseID,txtRecipZip.Text.Trim());
							surchargeOfRecip = 0;//zipcode.EASSurcharge;
						}

						//Apply MIN MAX
						if((tmpCustZone.Tables[0].Rows[0]["esa_surcharge_percent"] != null) && 
							(!tmpCustZone.Tables[0].Rows[0]["esa_surcharge_percent"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
							(tmpCustZone.Tables[0].Rows[0]["esa_surcharge_percent"].ToString() != ""))
						{
							if((decimal)tmpCustZone.Tables[0].Rows[0]["esa_surcharge_percent"] != 0)
							{

								if((tmpCustZone.Tables[0].Rows[0]["min_esa_amount"] != null) && 
									(!tmpCustZone.Tables[0].Rows[0]["min_esa_amount"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
									(tmpCustZone.Tables[0].Rows[0]["min_esa_amount"].ToString() != ""))
									if(surchargeOfRecip<(decimal)tmpCustZone.Tables[0].Rows[0]["min_esa_amount"])					
										surchargeOfRecip=(decimal)tmpCustZone.Tables[0].Rows[0]["min_esa_amount"];
							
								if((tmpCustZone.Tables[0].Rows[0]["max_esa_amount"] != null) && 
									(!tmpCustZone.Tables[0].Rows[0]["max_esa_amount"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
									(tmpCustZone.Tables[0].Rows[0]["max_esa_amount"].ToString() != ""))
									if(surchargeOfRecip>(decimal)tmpCustZone.Tables[0].Rows[0]["max_esa_amount"])					
										surchargeOfRecip=(decimal)tmpCustZone.Tables[0].Rows[0]["max_esa_amount"];
							}
						}
						//END Apply MIN MAX

					}
					else //NO ESA FROM DB
					{	
						//						Zipcode zipcode = new Zipcode();
						//						zipcode.Populate(appID,enterpriseID,txtRecipZip.Text.Trim());
						surchargeOfRecip = 0;//zipcode.EASSurcharge;
					}
				}
				else //NO ESA FOR DEST
				{
					surchargeOfRecip = 0;
				}
				//***************** Summary Surcharge *************************

				decSrchrg = surchargeOfSender + surchargeOfRecip;
			}
			return EnterpriseRounding(decSrchrg);
		}
//		private decimal Rounding(decimal beforeRound)
//		{
//			return TIESUtility.EnterpriseRounding(beforeRound,(int)ViewState["wt_rounding_method"],(decimal)ViewState["wt_increment_amt"]);
//		}

	}
}
