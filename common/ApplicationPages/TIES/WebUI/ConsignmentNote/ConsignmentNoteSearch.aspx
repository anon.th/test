<%@ Page language="c#" Codebehind="ConsignmentNoteSearch.aspx.cs" AutoEventWireup="false" Inherits="com.ties.ConsignmentNoteSearch" smartNavigation="false" %>
<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<%@ Import Namespace="com.common.util" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Search ConsignmentNote</title>
		<META http-equiv="Content-Type" content="text/html; charset=windows-874">
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../css/Styles.css" type="text/css" rel="Stylesheet">
		<!--#INCLUDE FILE="../msFormValidations.inc"-->
		<script language="javascript" src="../Scripts/settingScrollPosition.js"></script>
		<script language="javascript" type="text/javascript">
			function DoAction(serveraction) {
				frmSearchConsignment.elements("ServerAction").value = serveraction;
				frmSearchConsignment.submit();
			}
				
			function DoInsert() {
				window.location.href = "ConsignmentNote.aspx?ServerAction=INSERT";
			}
				
			function SelectItem(ConsignmentNo){
				window.location.href = "../ConsignmentNote/ConsignmentNote.aspx?ConsignmentNo=" + ConsignmentNo; 
			}
			
			var old_bgcolor;
			var old_class;
			function ShowBar(src) {
				if (!src.contains(event.fromElement)) {
					src.style.cursor = 'hand';
					old_class = src.className;
					src.className = 'tabletotal';
				}
			}
			function HideBar(src) {
				if (!src.contains(event.toElement)) {
					src.style.cursor = 'default';
					src.className = old_class;
				}
			}
		</script>
	</HEAD>
	<body onkeypress="microsoftKeyPress()" onclick="GetScrollPosition();" onload="SetScrollPosition();"
		onunload="window.top.displayBanner.fnCloseAll(0);" MS_POSITIONING="ViewLayout">
		<form id="frmSearchConsignment" runat="server">
			<input id="ServerAction" type="hidden" name="ServerAction">
			<table cellSpacing="0" cellPadding="0" width="100%" border="0">
				<tr>
					<td>
						<table class="TextBody" cellSpacing="0" cellPadding="3" border="0">
							<tr>
								<td colSpan="6"><asp:label id="lblMainTitle" runat="server" CssClass="mainTitleSize" Width="477px">Search Consignment Note</asp:label></td>
							</tr>
							<tr class="tablepagingitem" style="HEIGHT: 5px">
								<td style="WIDTH: 120px" align="right">&nbsp;</td>
								<td style="WIDTH: 120px" align="left">&nbsp;</td>
								<td style="WIDTH: 50px" align="right">&nbsp;</td>
								<td style="WIDTH: 100px" align="left">&nbsp;</td>
								<td style="WIDTH: 120px" align="right">&nbsp;</td>
								<td style="WIDTH: 50px" align="right">&nbsp;</td>
							</tr>
							<tr>
								<td>&nbsp;</td>
								<td colSpan="5"><a onclick="javascript:DoAction('CLEAR');"><input class="queryButton" id="btnQuery" style="WIDTH: 60px; HEIGHT: 20px" type="button"
											value="Query" name="btnQuery"></a> <a onclick="javascript:DoAction('SEARCH');">
										<input class="queryButton" id="btnExcute" style="WIDTH: 104px; HEIGHT: 20px" type="button"
											value="Execute Query" name="btnExcute"></a> <a onclick="javascript:DoInsert();">
										<input class="queryButton" id="btnInsert" style="WIDTH: 60px; HEIGHT: 20px" type="button"
											value="Insert" name="btnInsert"></a>
								</td>
							</tr>
							<tr class="tablepagingitem" style="HEIGHT: 5px">
								<td colSpan="6">&nbsp;</td>
							</tr>
							<tr>
								<td colSpan="6"><asp:label id="ErrorMsg" runat="server" CssClass="errorMsgColor" Width="643px" Height="3px"></asp:label></td>
							</tr>
							<tr>
								<td colSpan="6">&nbsp;</td>
							</tr>
							<tr>
								<td align="right">Consignment No&nbsp;: &nbsp;</td>
								<td align="left"><asp:textbox id="txtConsignmentNo" runat="server" Width="180px"></asp:textbox></td>
								<td>&nbsp;</td>
								<td align="right">Customer Code&nbsp;: &nbsp;</td>
								<td align="left"><asp:textbox id="txtCustomerCode" runat="server" Width="180px"></asp:textbox></td>
								<td>&nbsp;</td>
							</tr>
							<tr>
								<td colSpan="6">&nbsp;</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td align="center"><asp:datagrid id="dgResult" runat="server" AllowPaging="True" AutoGenerateColumns="False" PageSize="50"
							BorderWidth="0px" CellSpacing="1" CellPadding="2" HorizontalAlign="Center" HeaderStyle-CssClass="gridHeading"
							ItemStyle-CssClass="gridField" AlternatingItemStyle-CssClass="gridField">
							<AlternatingItemStyle CssClass="gridField"></AlternatingItemStyle>
							<ItemStyle CssClass="gridField"></ItemStyle>
							<HeaderStyle Font-Bold="True" HorizontalAlign="Center" CssClass="gridHeading"></HeaderStyle>
							<PagerStyle NextPageText="Next" Font-Size="Smaller" PrevPageText="Previous" HorizontalAlign="Right"></PagerStyle>
							<Columns>
								<asp:TemplateColumn HeaderText="No.">
									<HeaderStyle Width="25px"></HeaderStyle>
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
									<ItemTemplate>
										<%# (dgResult.PageSize*dgResult.CurrentPageIndex)+Container.ItemIndex+1 %>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:BoundColumn DataField="CONSIGNMENT_NO" HeaderText="Consignment No">
									<HeaderStyle Width="60px"></HeaderStyle>
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="CUSTOMERCODE" HeaderText="Customer Code">
									<HeaderStyle Width="60px"></HeaderStyle>
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="REFERENCE_NAME" HeaderText="Recipient Name">
									<HeaderStyle Width="200px"></HeaderStyle>
									<ItemStyle HorizontalAlign="Left"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="ADDRESS" HeaderText="Recipient Address">
									<HeaderStyle Width="200px"></HeaderStyle>
									<ItemStyle HorizontalAlign="Left"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="STATE" HeaderText="State">
									<HeaderStyle Width="150px"></HeaderStyle>
									<ItemStyle HorizontalAlign="Left"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="ZIPCODE" HeaderText="Zipcode">
									<HeaderStyle Width="30px"></HeaderStyle>
									<ItemStyle HorizontalAlign="Left"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="TELEPHONE" HeaderText="Telephone">
									<HeaderStyle Width="50px"></HeaderStyle>
									<ItemStyle HorizontalAlign="Left"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="SENDER_NAME" HeaderText="Sender Name">
									<HeaderStyle Width="100px"></HeaderStyle>
									<ItemStyle HorizontalAlign="Left"></ItemStyle>
								</asp:BoundColumn>
							</Columns>
						</asp:datagrid><asp:label id="lblNotFound" runat="server" Font-Bold="True" ForeColor="Red" Visible="False">Data not found.</asp:label></td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
