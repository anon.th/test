using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;  
using System.Drawing;
using System.Text;
using System.Configuration;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Web.Security;  
using System.Globalization; 
using com.common.classes;
using com.common.RBAC;
using com.common.util;
using com.common.DAL;
using com.common.applicationpages;
using com.ties.DAL;
using com.ties.classes;
using com.ties.BAL;  
using Cambro.Web.DbCombo;
using TIES;
using TIESDAL;

namespace com.ties //TIES.WebUI.ConsignmentNote
{
	/// <summary>
	/// Summary description for CustomerRef.
	/// </summary>
	public class CustomerRef : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.Label ErrorMsg;
		protected System.Web.UI.WebControls.Label ErrorMsg1;
		protected System.Web.UI.WebControls.TextBox txtCustomerCode;
		protected System.Web.UI.WebControls.TextBox txtCustomerName;
		protected System.Web.UI.WebControls.TextBox txtAddress1;
		protected System.Web.UI.WebControls.TextBox txtAddress2;
		protected System.Web.UI.WebControls.TextBox txtTelephone;
		protected System.Web.UI.WebControls.TextBox txtContactPerson;
	
		Utility utility = null;
		String strAppID = null;
		String strEnterpriseID = null;
		String PayerID = null;
		String userID = null;
		public String CustomerCode = "";
		public String strMsg = "";
		protected System.Web.UI.WebControls.TextBox txtProvince;
		protected com.common.util.msTextBox txtZipcode;
		protected System.Web.UI.WebControls.Label lblMainTitle;
		public String strMsg1 = "";

		private void Page_Load(object sender, System.EventArgs e)
		{
			String ServerAction = "";
			try
			{
				utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings,Page.Session);
				strAppID = Session["applicationID"] + ""; 
				strEnterpriseID = Session["enterpriseID"] + ""; 
				userID = Session["userID"] + ""; 
				PayerID = Session["PayerID"] + "";
				if (!Page.IsPostBack)
				{
					ServerAction = Request.QueryString["ServerAction"] + "";
					CustomerCode = Request.QueryString["CustomerCode"] + "";
					if (CustomerCode != "")
					{
						ServerAction = "LOAD";
						txtCustomerCode.ReadOnly = true;
						txtCustomerCode.CssClass = "txtReadOnly";
					}
					else
					{
						ServerAction = "INSERT";
					}
				}
				else
				{
					ServerAction = Request.Form["ServerAction"] + "";
					CustomerCode = Request.Form["CustomerCode"] + "";
				}

				switch (ServerAction.ToUpper())
				{
					case "LOAD":
						LoadData();
						break;
					case "INSERT":
						ClearData();
						break;
					case "DELETE":
						DeleteData();
						break;
					case "SAVE":
						SaveData();
						break;
				}
			}
			catch(ApplicationException appException)
			{
				strMsg = appException.Message.ToString();
				ErrorMsg.Text = strMsg;
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private void LoadData()
		{
			DataSet DS = new DataSet();
			DataTable DT = new DataTable();
			DataRow DR = null;

			try
			{
				ClearData();
				if (CustomerCode != "")
				{
					//Load customer data from table REFERENCE2
//					DS = ConsignmentNoteDAL.SearchCompanny(utility.GetAppID(), utility.GetEnterpriseID(), PayerID, CustomerCode, "", "");

					DS = ConsignmentNoteDAL.SearchConsigneeByCustCode(utility.GetAppID(), utility.GetEnterpriseID(), PayerID, CustomerCode);

					if (DS != null && DS.Tables.Count > 0)
					{
						DT = DS.Tables[0];
						if (DT.Rows.Count > 0)
						{
							DR = DT.Rows[0];
							if (!DR["CUSTOMERCODE"].GetType().Equals(System.Type.GetType("System.DBNull")))
							{
								txtCustomerCode.Text = (String)DR["CUSTOMERCODE"];
							}
							if (!DR["REFERENCE_NAME"].GetType().Equals(System.Type.GetType("System.DBNull")))
							{
								txtCustomerName.Text = (String)DR["REFERENCE_NAME"];
							}
							if (!DR["ADDRESS1"].GetType().Equals(System.Type.GetType("System.DBNull")))
							{
								txtAddress1.Text = (String)DR["ADDRESS1"];
							}
							if (!DR["ADDRESS2"].GetType().Equals(System.Type.GetType("System.DBNull")))
							{
								txtAddress2.Text = (String)DR["ADDRESS2"];
							}
							if (!DR["ZIPCODE"].GetType().Equals(System.Type.GetType("System.DBNull")))
							{
								txtZipcode.Text = (String)DR["ZIPCODE"];
							}
							if (!DR["TELEPHONE"].GetType().Equals(System.Type.GetType("System.DBNull")))
							{
								txtTelephone.Text  = (String)DR["TELEPHONE"];
							}
							if (!DR["CONTACTPERSON"].GetType().Equals(System.Type.GetType("System.DBNull")))
							{
								txtContactPerson.Text = (String)DR["CONTACTPERSON"];
							}
							if (!DR["STATE_NAME"].GetType().Equals(System.Type.GetType("System.DBNull")))
							{
								txtProvince.Text = (String)DR["STATE_NAME"];
							}
						}
					}
				}
			}
			catch(ApplicationException appException)
			{
				strMsg = appException.Message.ToString();
			}
			finally
			{
				ErrorMsg.Text = strMsg;
			}
		}

		private void ClearData()
		{
			strMsg = "";
			ErrorMsg.Text = "";
			ErrorMsg1.Text = "";

			try
			{
				txtCustomerCode.Text = "";
				txtCustomerName.Text = "";
				txtAddress1.Text = "";
				txtAddress2.Text = "";
				txtZipcode.Text = "";
				txtTelephone.Text = "";
				txtContactPerson.Text = "";
				txtProvince.Text = "";
			}
			catch(ApplicationException appException)
			{
				strMsg = appException.Message.ToString();
			}
			finally
			{
				ErrorMsg.Text = strMsg;
			}
		}

		private void DeleteData()
		{
			int iRowsAffected = 0;
			strMsg = "";
			ErrorMsg.Text = "";
			ErrorMsg1.Text = "";

			try
			{
				if (txtCustomerCode.Text != "")
				{
					iRowsAffected = ConsignmentNoteDAL.DeleteCustomer(strAppID, strEnterpriseID, PayerID, txtCustomerCode.Text); 
					if (iRowsAffected > 0)
					{
						ClearData();
						strMsg = "Delete successfully.";
					}
					else if (iRowsAffected == -1)
					{
						strMsg = "Can't delete data that is referred to Consignment Note.";
					}
					else
					{
						strMsg = "Delete fail.";
					}
				}
			}
			catch(ApplicationException appException)
			{
				strMsg = appException.Message.ToString();
			}
			finally
			{
				ErrorMsg.Text = strMsg;
			}
		}

		private void SaveData()
		{
			DataSet DS = new DataSet();
			int iRowsAffected = 0;
			strMsg = "";
			strMsg1 = "";
			ErrorMsg.Text = "";
			ErrorMsg1.Text = "";

			try
			{
				DataSet dsZipcode = new DataSet();
				if (txtCustomerCode.Text.Trim() == "") {strMsg1 += "Consignee Code, ";}
				if (txtCustomerName.Text.Trim() == "") {strMsg1 += "Consignee Name, ";}
				if (txtAddress1.Text.Trim() == "") {strMsg1 += "Address1, ";}
				if (txtAddress2.Text.Trim() == "") {strMsg1 += "Address2, ";}
				if (txtZipcode.Text.Trim() == "") {strMsg1 += "Zipcode, ";}
				if (txtTelephone.Text.Trim() == "") {strMsg1 += "Telephone No., ";}
				if (txtContactPerson.Text.Trim() == "") {strMsg1 += "Contact Person  ";}

				if (txtZipcode.Text.Trim() != "")
				{
					Zipcode zipcode = new Zipcode();
					dsZipcode = zipcode.ckZipcodeInSystem(strAppID,strEnterpriseID,txtZipcode.Text.Trim());
					if (Utility.CheckNumeric(txtZipcode.Text.Trim(), false) != true){strMsg1 += " Zipcode, ";}
					if (dsZipcode.Tables[0].Rows.Count < 1)
					{
						strMsg1 += " Zipcode, ";
					}
				}

				if (strMsg1 == "")
				{
					if (CustomerCode == "")
					{
						//Check duplicate CustomerCode
						if (txtCustomerCode.Text.Trim() != "")
						{
							DS = ConsignmentNoteDAL.SearchCustomerCode(strAppID, strEnterpriseID, PayerID, txtCustomerCode.Text.Trim());
							if (DS != null && DS.Tables.Count > 0)
							{
								if (DS.Tables[0].Rows.Count > 0) 
								{
									strMsg = "Customer Code duplicate, please check again.";
								}
								else
								{
									CustomerCode = ConsignmentNoteDAL.InsertCustomer(strAppID, strEnterpriseID, PayerID, txtCustomerCode.Text.Trim(), txtCustomerName.Text.Trim(), txtAddress1.Text.Trim(), txtAddress2.Text.Trim(), txtZipcode.Text.Trim(), txtTelephone.Text.Trim(), txtContactPerson.Text.Trim());  
									if (CustomerCode != "")
									{
										LoadData();
										strMsg = "Save successful.";
									}
									else
									{
										strMsg = "Save fail.";
									}
								}
							}
						}

//						if (strMsg != "")
//						{
//							CustomerCode = ConsignmentNoteDAL.InsertCustomer(strAppID, strEnterpriseID, PayerID, txtCustomerCode.Text.Trim(), txtCustomerName.Text.Trim(), txtAddress1.Text.Trim(), txtAddress2.Text.Trim(), txtZipcode.Text.Trim(), txtTelephone.Text.Trim(), txtContactPerson.Text.Trim());  
//							if (CustomerCode != "")
//							{
//								LoadData();
//								strMsg = "Save successful.";
//							}
//							else
//							{
//								strMsg = "Save fail.";
//							}
//						}
					}
					else
					{
						iRowsAffected = ConsignmentNoteDAL.UpdateCustomer(strAppID, strEnterpriseID, PayerID,txtCustomerCode.Text.Trim(), txtCustomerName.Text.Trim(), txtAddress1.Text.Trim(), txtAddress2.Text.Trim(), txtZipcode.Text.Trim(), txtTelephone.Text.Trim(), txtContactPerson.Text.Trim()); 
						if (iRowsAffected > 0)
						{
							LoadData();
							strMsg = "Save successful.";
						}
//						else if (iRowsAffected == -1)
//						{
//							strMsg = "Can't modify data that is referred to Consignment Note.";
//						}
						else
						{
							strMsg = "Save fail.";
						}
					}
				}

				if (strMsg1 != "") {strMsg1=Utility.GetLanguageText(ResourceType.UserMessage,"PLS_CHECK_DATA",utility.GetUserCulture())+ strMsg1.Substring(0, strMsg1.Length - 2);};
//				{strMsg1 = "��سҵ�Ǩ�ͺ  " + strMsg1.Substring(0, strMsg1.Length - 2); }
			}
			catch(ApplicationException appException)
			{
				strMsg = appException.Message.ToString();
			}
			finally
			{
				ErrorMsg.Text = strMsg;
				ErrorMsg1.Text = strMsg1;
			}
		}

		private void txtZipcode_TextChanged(object sender, System.EventArgs e)
		{
			ErrorMsg1.Text = "";
			DataSet dsState = ConsignmentNoteDAL.SearchStatecode(strAppID,strEnterpriseID,txtZipcode.Text.Trim());
			if(dsState.Tables[0].Rows.Count > 0)
			{
				foreach(DataRow dr in dsState.Tables[0].Rows)
				{
					txtProvince.Text = dr["state_name"].ToString().Trim();
				}
			}
			else
			{
				ErrorMsg1.Text = Utility.GetLanguageText(ResourceType.UserMessage,"PLS_CHECK_DATA",utility.GetUserCulture())+"Zipcode";
				txtProvince.Text = "";
			}
		}

		#region CheckNumeric
		/*//Check input is Integer?
		//if input = integer then Integer = true
		//if input = double, float then Integer = false
		private bool CheckNumeric(String sText, bool beInteger)
		{
			String ValidChars = "";
			bool beNumber = true;
			//String tmpChar;
		
			try
			{
				if (beInteger == true)
				{
					ValidChars = "0123456789";
				}
				else
				{
					ValidChars = "0123456789.";
				}

				for (int i = 0; i < sText.Length && beNumber == true; i++)
				{ 
					char tmpChar = sText[i]; 
					if (ValidChars.IndexOf(tmpChar) == -1)  
					{
						beNumber = false;
					}
				}
			}
			catch (Exception ex)
			{
				beNumber = false;
				strMsg = ex.Message.ToString();
				ErrorMsg.Text = strMsg;
			}
			return beNumber;
		}*/
		#endregion


	}
}
