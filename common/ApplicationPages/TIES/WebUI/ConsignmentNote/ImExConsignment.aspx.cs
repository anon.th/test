using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;  
using System.Drawing;
using System.Text;
using System.Configuration;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Web.Security;  
using System.Globalization; 
using com.common.classes;
using com.common.RBAC;
using com.common.util;
using com.common.DAL;
using com.common.applicationpages;
using com.ties.DAL;
using com.ties.classes;
using com.ties.BAL;  
using Cambro.Web.DbCombo;
using TIES;
using TIESDAL;
using System.Data.OleDb;
using System.IO;

namespace com.ties
{
	/// <summary>
	/// Summary description for ImExConsignment.
	/// </summary>
	public class ImExConsignment : BasePage
	{
		protected System.Web.UI.WebControls.Label ErrorMsg;
		protected System.Web.UI.HtmlControls.HtmlInputButton btnImport;
		protected System.Web.UI.HtmlControls.HtmlInputFile file_upload;
		protected System.Web.UI.WebControls.Panel pnlCheckStep;
		protected System.Web.UI.WebControls.Panel pnlException;
		protected System.Web.UI.WebControls.Label lblCheck1;
		protected System.Web.UI.WebControls.Label lblCheck2;
		protected System.Web.UI.WebControls.Label lblCheck4;
		protected System.Web.UI.WebControls.Label lblCheck5;
		protected System.Web.UI.WebControls.Label lblCheck6;
		protected System.Web.UI.WebControls.Image imgCheck1;
		protected System.Web.UI.WebControls.Image imgCheck2;
		protected System.Web.UI.WebControls.Image imgCheck3;
		protected System.Web.UI.WebControls.Image imgCheck4;
		protected System.Web.UI.WebControls.Image imgCheck5;
		protected System.Web.UI.WebControls.Image imgCheck6;
		protected System.Web.UI.WebControls.Label lblCheck3;
		protected System.Web.UI.WebControls.Image imgCheck7;
		protected System.Web.UI.WebControls.Image imgCheck8;
		protected System.Web.UI.WebControls.Image imgCheck9;
		protected System.Web.UI.WebControls.Image imgCheck10;
		protected System.Web.UI.WebControls.Image imgCheck11;
		protected System.Web.UI.WebControls.Image imgCheck12;
		protected System.Web.UI.WebControls.TextBox txtException;
		protected System.Web.UI.WebControls.Label lblMainTitle;	
		protected System.Web.UI.WebControls.Panel pnlDescription;

		Utility utility = null;
		String strAppID = null;
		String strEnterpriseID = null;
		String PayerID = null;
		String userID = null;
		public String strMsg = "";
		public int ErrorNumber = 0;
		private string FileName = "";
		protected com.common.util.msTextBox txtPayerId;
		protected System.Web.UI.WebControls.Button btnCancelEx;
		protected System.Web.UI.WebControls.Button btnExbyStaff;
		protected System.Web.UI.WebControls.Label lblResult;
		protected System.Web.UI.HtmlControls.HtmlTable tabExportStaff;
		protected com.common.util.msTextBox txtConsDate;
		protected ArrayList userRoleArray;

	
		private void Page_Load(object sender, System.EventArgs e)
		{
			String ServerAction = "";
	

			try
			{
				utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings,Page.Session);
				strAppID = Session["applicationID"] + ""; 
				strEnterpriseID = Session["enterpriseID"] + ""; 
				userID = Session["userID"] + ""; 
				PayerID = Session["PayerID"] + "";


				if (!Page.IsPostBack)
				{
					ServerAction = Request.QueryString["ServerAction"] + "";


					//Get Role >> PREPRINTIMPORT
					string strRole = "";
					strRole = getRole();

					if (strRole == "PREPRINTIMPORT") 
					{
						btnImport.Disabled = false;
					}
					else 
					{
						btnImport.Disabled = true;
					}

					// 2012-01-24 -- start
					if (strRole == "PREPRINTEXPORTSTAFF") 
					{
						tabExportStaff.Visible = true;
					}
					else 
					{
						tabExportStaff.Visible = false;
					}
					// 2012-01-24 -- end
				}
				else
				{
					ServerAction = Request.Form["ServerAction"] + ""; 
				}

				switch (ServerAction.ToUpper())
				{
					case "IMPORTDATA":
						ImportData();
						break;
					case "EXPORTDATA":
						ExportData();
						break;
					case "CLEAR":
						ClearStep();
						break;
				}
			}
			catch(ApplicationException appException)
			{
				strMsg = appException.Message.ToString();
				ErrorMsg.Text = strMsg;
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnCancelEx.Click += new System.EventHandler(this.btnCancelEx_Click);
			this.btnExbyStaff.Click += new System.EventHandler(this.btnExbyStaff_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private void ImportData()
		{
			try
			{
				ClearStep();
				//Check Excel File Format
				Step1(); 
				
				//step2
				if (txtException.Text == "" && strMsg == "")  
				{ 
					//do step2 : Import Data from Excel File
					imgCheck1.Visible = true;
					Step2(); 
				}
				else
				{
					//show error step1
					imgCheck2.Visible = true;
					imgCheck4.Visible = true;
					imgCheck6.Visible = true;
					imgCheck8.Visible = true;
					imgCheck10.Visible = true;
					imgCheck12.Visible = true;
				}

				//step3 
				if (txtException.Text == "" && strMsg == "")  
				{ 
					//do step 3 : Check Required Data
					imgCheck1.Visible = true;
					imgCheck3.Visible = true;
					Step3(); 
				}
				else
				{
					//show error step2
					imgCheck4.Visible = true;
					imgCheck6.Visible = true;
					imgCheck8.Visible = true;
					imgCheck10.Visible = true;
					imgCheck12.Visible = true;
				}

				//step4
				if (txtException.Text == "" && strMsg == "")  
				{ 
					//do step 4 : Check Required Data
					imgCheck1.Visible = true;
					imgCheck3.Visible = true;
					imgCheck5.Visible = true;
					Step4(); 
				}
				else
				{
					//show error step3
					imgCheck6.Visible = true;
					imgCheck8.Visible = true;
					imgCheck10.Visible = true;
					imgCheck12.Visible = true;
				}

				//step5
				if (txtException.Text == "" && strMsg == "")  
				{ 
					//do step 5 : Save Data to Database
					imgCheck1.Visible = true;
					imgCheck3.Visible = true;
					imgCheck5.Visible = true;
					imgCheck7.Visible = true;
					Step5(); 
				}
				else
				{
					//show error step4
					imgCheck8.Visible = true;
					imgCheck10.Visible = true;
					imgCheck12.Visible = true;
				}

				//step6
				if (txtException.Text == "" && strMsg == "")  
				{ 
					//do step 6 : Import Data Complete 
					imgCheck1.Visible = true;
					imgCheck3.Visible = true;
					imgCheck5.Visible = true;
					imgCheck7.Visible = true;
					imgCheck9.Visible = true;
					imgCheck11.Visible = true;
				}
				else
				{
					//show error step5
					imgCheck10.Visible = true;
					imgCheck12.Visible = true;
				}
			}
			catch(Exception ex)
			{
				ErrorMsg.Text = ex.Message.ToString() ;
			}
		}

		private void ClearStep()
		{
			strMsg = "";
			ErrorMsg.Text  = "";
			//lblDescription.Text = "";
			txtException.Text = "";			
			imgCheck1.Visible = false;
			imgCheck2.Visible = false;
			imgCheck3.Visible = false;
			imgCheck4.Visible = false;
			imgCheck5.Visible = false;
			imgCheck6.Visible = false;
			imgCheck7.Visible = false;
			imgCheck8.Visible = false;
			imgCheck9.Visible = false;
			imgCheck10.Visible = false;
			imgCheck11.Visible = false;
			imgCheck12.Visible = false;
		}

		
		private bool uploadFiletoServer()
		{
			bool status = true;

			if((file_upload.PostedFile != null ) && (file_upload.PostedFile.ContentLength > 0))
			{
				string extension = System.IO.Path.GetExtension(file_upload.PostedFile.FileName);

				if(extension.ToUpper() == ".XLS")
				{
					string fn = System.IO.Path.GetFileName(file_upload.PostedFile.FileName);
					string path = Server.MapPath("..\\Excel") + "\\";
					string pathFn = Server.MapPath("..\\Excel") + "\\" + userID + "_" + fn;

					ViewState["FileName"] = pathFn;

					try
					{
						if(System.IO.Directory.Exists(path) == false)
							System.IO.Directory.CreateDirectory(path);

						if (System.IO.File.Exists(pathFn)) 
							System.IO.File.Delete(pathFn);

						file_upload.PostedFile.SaveAs(pathFn);

					}
					catch(Exception ex)
					{
						throw new ApplicationException("Error during upload file to the server", null);
					}
				}
				else
				{
					status = false;
				}
			}
			else
			{
				status = false;
			}

			return status;
		}
		//Verify step 1 : Check Excel File Format
		private void Step1() 
		{
			string extension = "";
			strMsg = "";

			try
			{
				if ((file_upload.PostedFile != null) && (file_upload.PostedFile.ContentLength > 0))
				{
					extension = System.IO.Path.GetExtension(file_upload.PostedFile.FileName);
					if(extension.ToUpper() != ".XLS")
					{
						strMsg += "Selected file was not XLS file. (Excel File Format) Please check!\r\n";
					}
					else
					{
						imgCheck1.Visible = true;
						if(!uploadFiletoServer())
						{
							strMsg += "Cannot upload file to server. \r\n";
						}
					}
				}

				if (strMsg != "")
				{
					ErrorNumber = ErrorNumber + 1;
					txtException.Text += ErrorNumber.ToString() + ". " + strMsg;
				}
			}
			catch(Exception ex)
			{
				ErrorNumber = ErrorNumber + 1;
				txtException.Text += ErrorNumber.ToString() + ". Invalid data format in Excel file. \r\n";
			}
		}

		
		//Verify step 2 : Import Data from Excel File
		private void Step2()
		{
			DataTable dtConsignment = new DataTable(); 
			DataTable dtPackage = new DataTable(); 
			int iRowsAffected = 0;
			int jRowsAffected = 0;
			string Msg = "";
			strMsg = "";

			try
			{
				//				FileName = file_upload.Value + "";
				//				FileName = FileName.Replace("\'", "\\\'");
				//				String connString = ConfigurationSettings.AppSettings["ExcelConn"];
				//				connString = connString.Replace("(DestinationFile)", FileName);
				String connString = ConfigurationSettings.AppSettings["ExcelConn"];
				connString = connString.Replace("(DestinationFile)", (String)ViewState["FileName"]);

				//insert consignment sheet into tmp_Consignment
				StringBuilder strBuilder = new StringBuilder();
				strBuilder.Append("SELECT * FROM [Consignments$]");
				OleDbDataAdapter daExcelConsignment = new OleDbDataAdapter(strBuilder.ToString(), connString);

				DataSet dsExcelConsignment = new DataSet();
				daExcelConsignment.Fill(dsExcelConsignment);
				if ((dsExcelConsignment != null) && (dsExcelConsignment.Tables.Count > 0))
				{
					dtConsignment = dsExcelConsignment.Tables[0];
					if ((dtConsignment != null) && (dtConsignment.Rows.Count > 0))
					{
						iRowsAffected = ConsignmentNoteDAL.InsertTmpConsignment(strAppID, strEnterpriseID, PayerID,  dtConsignment);
					}
				}
				if (iRowsAffected < 0)
				{
					strMsg += "Error import Consignment data! Please check data in excel file.\r\n";
				}



				//insert package sheet into tmp_package
				if (strMsg == "")
				{
					strBuilder = new StringBuilder();
					strBuilder.Append("SELECT * FROM [Packages$]");
					OleDbDataAdapter daExcelPackage = new OleDbDataAdapter(strBuilder.ToString(), connString);

					DataSet dsExcelPackage = new DataSet();
					daExcelPackage.Fill(dsExcelPackage);
					if ((dsExcelPackage != null) && (dsExcelPackage.Tables.Count > 0))
					{
						dtPackage = dsExcelPackage.Tables[0];
						if ((dtPackage != null) && (dtPackage.Rows.Count > 0))
						{
							Msg = ConsignmentNoteDAL.InsertTmpPackage(strAppID, strEnterpriseID, PayerID,  dtPackage);
							jRowsAffected = Convert.ToInt16(Msg); 
						}
						if (jRowsAffected < 0)
						{
							strMsg += "Error import Package data! Please check data in excel file.\r\n";
						}
					}
				}


				if (strMsg != "")
				{
					ErrorNumber = ErrorNumber + 1;
					txtException.Text += ErrorNumber.ToString() + ". " + strMsg;
				}
			}
			catch(Exception ex)
			{
				ErrorNumber = ErrorNumber + 1;
				txtException.Text += strMsg + ErrorNumber.ToString() + ". Can not connect to Excel File! Please contact Administrator!\r\n";
			}
			finally
			{
				dtConsignment.Clear();
				dtConsignment.Dispose();
				dtPackage.Clear();
				dtPackage.Dispose(); 
			}
		}


		//Verify step 3 : Check Required Data
		private void Step3()
		{
			DataSet DS = new DataSet(); 
			DataSet DS1 = new DataSet(); 
			DataTable DT = new DataTable();
			DataTable DT1 = new DataTable();
			string consignment_no = "";
			string Msg = "";
			string pMsg = "";
			strMsg  = "";
 
			try
			{
				//Check tmpConsignment
				DS = ConsignmentNoteDAL.GetTmpConsignment(strAppID, strEnterpriseID, PayerID);
				if ((DS != null) && (DS.Tables.Count > 0))
				{
					Session["TMP_CONSIGNMENT"] = DS.Tables[0];  //declare for use next step
					DT = DS.Tables[0];
					if ((DT != null) && (DT.Rows.Count > 0))
					{
						foreach (DataRow DR in DT.Rows)
						{
							consignment_no = "";
							Msg = "";

							consignment_no = DR["consignment_no"].ToString();
							if (DR["consignment_no"].ToString().Trim() == "") { Msg += "consignment_no, "; }
							if (DR["payerid"].ToString().Trim() == "") { Msg += "payerid, "; }
							if (DR["payment_mode"].ToString().Trim() == "") { Msg += "payment_mode, "; }
							if (DR["sender_name"].ToString().Trim() == "") { Msg += "sender_name, "; }
							if (DR["sender_address1"].ToString().Trim() == "") { Msg += "sender_address1, "; }
							if (DR["sender_address2"].ToString().Trim() == "") { Msg += "sender_address2, "; }
							if (DR["sender_zipcode"].ToString().Trim() == "") { Msg += "sender_zipcode, "; }
							if (DR["sender_telephone"].ToString().Trim() == "") { Msg += "sender_telephone, "; }
							if (DR["sender_contact_person"].ToString().Trim() == "") { Msg += "sender_contact_person, "; }
							if (DR["recipient_name"].ToString().Trim() == "") { Msg += "recipient_name, "; }
							if (DR["recipient_address1"].ToString().Trim() == "") { Msg += "recipient_address1, "; }
							//							if (DR["recipient_address2"].ToString().Trim() == "") { Msg += "recipient_address2, "; }
							if (DR["recipient_zipcode"].ToString().Trim() == "") { Msg += "recipient_zipcode, "; }
							if (DR["recipient_telephone"].ToString().Trim() == "") { Msg += "recipient_telephone, "; }
							if (DR["recipient_contact_person"].ToString().Trim() == "") { Msg += "recipient_contact_person, "; }
							if (DR["service_code"].ToString().Trim() == "") { Msg += "service_code, "; }
							if (DR["payment_type"].ToString().Trim() == "") { Msg += "payment_type, "; }
							if (Msg != "")
							{
								Msg = Msg.Substring(0, Msg.Length - 2);
								strMsg += "Consignment_No " + consignment_no + " Require consignment data in column " + Msg + "\r\n";
							}
						}

						//Check tmpPackage
						DS1 = ConsignmentNoteDAL.GetTmpPackage(strAppID, strEnterpriseID, PayerID);
						if ((DS1 != null) && (DS1.Tables.Count > 0))
						{
							Session["TMP_PACKAGE"] = DS1.Tables[0];  //declare for use next step
							DT1 = DS1.Tables[0];
							if ((DT1 != null) && (DT1.Rows.Count > 0))
							{
								foreach (DataRow DR1 in DT1.Rows)
								{
									consignment_no = "";
									pMsg = "";

									consignment_no = DR1["consignment_no"].ToString();
									if (DR1["consignment_no"].ToString() == "") { pMsg += "consignment_no, "; }
									if (DR1["Seq_no"].ToString().Trim() == "") { pMsg += "Seq_no, "; }
									if (DR1["Width"].ToString().Trim() == "") { pMsg += "Width, "; }
									if (DR1["Length"].ToString().Trim() == "") { pMsg += "Length, "; }
									if (DR1["Height"].ToString().Trim() == "") { pMsg += "Height, "; }
									if (DR1["kilo"].ToString().Trim() == "") { pMsg += "kilo, "; }
									if (DR1["Qty"].ToString().Trim() == "") { pMsg += "Qty, "; }
									if (pMsg != "")
									{
										pMsg = pMsg.Substring(0, pMsg.Length - 2);
										strMsg += "Consignment_No " + consignment_no + " Require package data in column " + pMsg + "\r\n";
									}
								}
							}
						}
						else
						{
							strMsg += "Error tmpPackage Table Technical.! Please contact Administrator!\r\n";
						}
					}
				}
				else
				{
					strMsg += "Error tmpConsignment Table Technical.! Please contact Administrator!\r\n";
				}

				if (strMsg != "")
				{
					ErrorNumber = ErrorNumber + 1;
					txtException.Text +=  ErrorNumber.ToString() + ". System Require Data.! Please Check data in excel file!\r\n" + strMsg;
				}
			}
			catch(Exception ex)
			{
				ErrorNumber = ErrorNumber + 1;
				txtException.Text  += ErrorNumber.ToString() + ". Error Technical.! Please contact Administrator!\r\n";
			}
		}


		//Verify step 4 : Check Format Data
		private void Step4()
		{
			DataTable dtCon = new DataTable(); 
			DataTable dtPackage = new DataTable();
			DataSet DS = new DataSet(); 
			DataSet DS1 = new DataSet(); 
			DataSet DS2 = new DataSet(); 
			DataSet DS3 = new DataSet(); 
			DataSet DS4 = new DataSet(); 
			DataSet DS5 = new DataSet();
			DataTable DT = new DataTable(); 
			DataTable DT1 = new DataTable(); 
			DataTable DT2 = new DataTable(); 
			DataTable DT3 = new DataTable();  
			DataTable DT4 = new DataTable(); 
			DataTable DT5 = new DataTable(); 
			string Msg = "";
			string Msg1 = "";
			string Msg2 = "";
			string Msg3 = "";
			string pMsg = "";
			string pstrMsg = "";
			strMsg  = "";
		
			try
			{
				dtCon = (DataTable)Session["TMP_CONSIGNMENT"];
				if ((dtCon != null) && (dtCon.Rows.Count > 0))
				{
					foreach (DataRow drCon in dtCon.Rows)
					{
						Msg2 = ""; Msg3 = ""; 
						DS = null; DS1 = null; DS2 = null; DS3 = null; DS4 = null; DS5 = null;
						DT = null; DT1 = null; DT2 = null; DT3 = null; DT4 = null; DT5 = null;

						//check dupicate consignment_no
						DS = ConsignmentNoteDAL.SearchConsignmentNote(strAppID, strEnterpriseID, PayerID, drCon["consignment_no"].ToString().Trim(), "");
						if ((DS != null) && (DS.Tables.Count > 0) && (DS.Tables[0] != null))
						{
							DT = DS.Tables[0];
							if ((DT != null) && (DT.Rows.Count > 0)) {Msg1 += drCon["consignment_no"].ToString().Trim() + ", "; }
						}
						else
						{
							Msg2 += "consignment_no, "; 
						}

						//check format payerid
						DS1 = ConsignmentNoteDAL.SearchPayerID(strAppID, strEnterpriseID, drCon["payerid"].ToString().ToUpper().Trim());
						if ((DS1 != null) && (DS1.Tables.Count > 0) && (DS1.Tables[0] != null))
						{
							DT1 = DS1.Tables[0];
							if ((DT1 != null) && (DT1.Rows.Count != 1)) {Msg3 += "payerid, "; }
						}
						else
						{
							Msg2 += "payerid, "; 
						}
						
						//check format sender_zipcode
						DS2 = ConsignmentNoteDAL.SearchZipcode(strAppID, strEnterpriseID, drCon["sender_zipcode"].ToString().Trim());
						if ((DS2 != null) && (DS2.Tables.Count > 0) && (DS2.Tables[0] != null))
						{
							DT2 = DS2.Tables[0];
							if ((DT2 != null) && (DT2.Rows.Count != 1)) {Msg3 += "sender_zipcode, "; }
						}
						else
						{
							Msg2 += "sender_zipcode, "; 
						}

						//check format recipient_name
						DS3 = ConsignmentNoteDAL.SearchReference2(strAppID, strEnterpriseID, PayerID, drCon["recipient_name"].ToString().Trim(), "");
						if ((DS3 != null) && (DS3.Tables.Count > 0) && (DS3.Tables[0] != null))
						{
							DT3 = DS3.Tables[0];
							if ((DT3 != null) && (DT3.Rows.Count != 1)) {Msg3 += "recipient_name, "; }
						}
						else
						{
							Msg2 += "recipient_name, "; 
						}

						//check format recipient_zipcode
						DS4 = ConsignmentNoteDAL.SearchZipcode(strAppID, strEnterpriseID, drCon["recipient_zipcode"].ToString().Trim());
						if ((DS4 != null) && (DS4.Tables.Count > 0) && (DS4.Tables[0] != null))
						{
							DT4 = DS4.Tables[0];
							if ((DT4 != null) && (DT4.Rows.Count != 1)) {Msg3 += "recipient_zipcode, "; }
						}
						else
						{
							Msg2 += "recipient_zipcode, "; 
						}

						//check format service_code
						DS5 = ConsignmentNoteDAL.SearchService(strAppID, strEnterpriseID, drCon["service_code"].ToString().Trim());
						if ((DS5 != null) && (DS5.Tables.Count > 0) && (DS5.Tables[0] != null))
						{
							DT5 = DS5.Tables[0];
							if ((DT5 != null) && (DT5.Rows.Count != 1)) {Msg3 += "service_code, "; }
						}
						else
						{
							Msg2 += "service_code, "; 
						}

						if (Msg3 != "")
						{
							Msg3 = Msg3.Substring(0, Msg3.Length - 2);
							Msg += "Consignment_No " + drCon["consignment_no"].ToString().Trim() + " Format not correct in column " + Msg3 + "\r\n";
						}
						if (Msg2 != "")
						{
							Msg2 = Msg2.Substring(0, Msg2.Length - 2);
							Msg += "Consignment_No " + drCon["consignment_no"].ToString().Trim() + " Format not correct in column " + Msg2 + "\r\n";
						}
					} //end loop for each con

					if (Msg1 != "")
					{
						Msg1 = Msg1.Substring(0, Msg1.Length - 2);
						Msg += "Find Consignment_No dupicate " + Msg1 + "\r\n";
					}


					if (Msg != "")
					{
						strMsg += "Data not complete! Please check data format in Consignments sheet!\r\n" + Msg;
					}


					//check format package data 
					dtPackage = (DataTable)Session["TMP_PACKAGE"];
					if ((dtPackage != null) && (dtPackage.Rows.Count > 0))
					{
						foreach (DataRow drPackage in dtPackage.Rows)	
						{
							pMsg = ""; 
							if (Utility.CheckNumeric(drPackage["Seq_No"].ToString().Trim(),  true) != true) {pMsg  += "  mps_code,";}
							if (Utility.CheckNumeric(drPackage["Width"].ToString().Trim(),  false) != true) {pMsg += "  pkg_breadth,";}
							if (Utility.CheckNumeric(drPackage["Length"].ToString().Trim(), false) != true) {pMsg += "  pkg_length,";}
							if (Utility.CheckNumeric(drPackage["Height"].ToString().Trim(), false) != true) {pMsg += "  pkg_height,";}
							if (Utility.CheckNumeric(drPackage["Kilo"].ToString().Trim(), false) != true)   {pMsg += "  pkg_wt,";}
							if (Utility.CheckNumeric(drPackage["Qty"].ToString().Trim(), false) != true)    {pMsg += "  pkg_qty,";}

							if (pMsg != "")
							{
								pMsg = pMsg.Substring(0, pMsg.Length - 2);
								pstrMsg += "Consignment_No " + drPackage["consignment_no"].ToString().Trim() + " Format not correct in column " + pMsg + "\r\n";
							}
						} //end loop for each package

						if (pstrMsg != "")
						{
							strMsg += "Data not complete! Please check data format in Package sheet!\r\n" + pstrMsg;
						}
					}


				}
				else
				{
					strMsg += "Error Technical no data in tmpConsignment Table.! Please contact Administrator!\r\n";
				}

				if (strMsg != "")
				{
					ErrorNumber = ErrorNumber + 1;
					txtException.Text  += ErrorNumber.ToString() + ". " +  strMsg;
				}
			}
			catch(Exception ex)
			{
				ErrorNumber = ErrorNumber + 1;
				txtException.Text  += ErrorNumber.ToString() + ". Error Technical.! Please contact Administrator!\r\n";
			}
		}

		
		//Verify step 5 : Save Data to Database
		private void Step5()
		{
			DataTable dtCon = new DataTable();
			DataTable dtPackage = new DataTable(); 
			string Msg = "";
			strMsg  = "";

			try
			{
				dtCon = (DataTable)Session["TMP_CONSIGNMENT"];
				dtPackage = (DataTable)Session["TMP_PACKAGE"];

				if ((dtCon != null) && (dtCon.Rows.Count > 0) && (dtPackage != null))
				{
					//Insert data from tmp_consignment to tb_consignmentNote 
					//and tmp_package to tb_consignmentNoteDetail 
					Msg = ConsignmentNoteDAL.InsertTmpConsignmentToTable(strAppID, strEnterpriseID, PayerID, Session["userID"] + "", dtCon, dtPackage);
					if (Msg != "")  { strMsg += "Data not complete! Please check Consignment, Package data!\r\n" + Msg; }
				}
				else
				{
					strMsg += "Error Technical no data in tmp_Consignment, tmp_Package Table.! Please contact Administrator!\r\n";
				}
			}
			catch(Exception ex)
			{
				ErrorNumber = ErrorNumber + 1;
				txtException.Text  += ErrorNumber.ToString() + ". Error Technical.! Please contact Administrator!\r\n";
			}
		}


		private void ExportData()
		{
			DataSet dsConsignment = new DataSet();
			DataSet dsPackage = new DataSet(); 
			int iRowsAffected = 0;
			string Res = "";
 
			try
			{
				dsConsignment = ConsignmentNoteDAL.GetExportConsignment(strAppID, strEnterpriseID, PayerID);
				dsPackage = ConsignmentNoteDAL.GetExportPackage(strAppID, strEnterpriseID, PayerID);

				if ((dsConsignment != null) && (dsConsignment.Tables.Count > 0) && (dsConsignment.Tables[0] != null) && (dsPackage != null) && (dsPackage.Tables.Count > 0) && (dsPackage.Tables[0] != null))
				{
					TIESClasses.CreateXls Xls = new TIESClasses.CreateXls();
					// Set type column
					string[] ColumnType = new string[dsConsignment.Tables[0].Columns.Count];
					ColumnType[1] = Xls.TypeText; 

					Res = Xls.createXlsConsignment(dsConsignment, dsPackage, GetPathExport(),ColumnType);
					if (Res == "0")
					{	
						GetDownloadFile(Res);
				
						// Kill Process after save
						Xls.KillProcesses("EXCEL");

						//update flag flag_export in table tb_consignmentNote, tb_consignmentNoteDetail
						iRowsAffected = ConsignmentNoteDAL.UpdateConsignmentFlagExport(strAppID, strEnterpriseID, PayerID);

					} 
					else
					{
						ErrorMsg.Text = Res;
					}
				}
			}
			catch(Exception ex)
			{
				ErrorMsg.Text = ex.Message.ToString() ;
			}
			finally
			{
				dsConsignment.Dispose();
				dsPackage.Dispose();
			}
		}

		
		private string  GetPathExport()
		{
			//			DateTime dateT = DateTime.Now;
			//			FileName = "Consignment_" + dateT.Year.ToString() + dateT.Month.ToString()  + dateT.Day.ToString();
			FileName = PayerID.ToUpper() + "_" + userID.ToUpper() + "_Consignment_" + String.Format("{0:yyyyMMdd}", DateTime.Now);
			string mapFile = Server.MapPath("..\\Export") + "\\" + FileName + ".xls";
			return mapFile;
		}

		
		private void GetDownloadFile(string Res)
		{
			if (Res == "0")
			{
				//				DateTime dateT = DateTime.Now;
				//				FileName = "Consignment_" + dateT.Year.ToString() + dateT.Month.ToString()  + dateT.Day.ToString();
				FileName = PayerID.ToUpper() + "_" + userID.ToUpper() + "_Consignment_" + String.Format("{0:yyyyMMdd}", DateTime.Now);
				Response.ContentType="application/ms-excel";
				Response.AddHeader( "content-disposition","attachment; filename=" + FileName + ".xls");
				FileStream fs = new FileStream(GetPathExport(),FileMode.Open);
				long FileSize;
				FileSize = fs.Length;
				byte[] getContent = new byte[(int)FileSize];
				fs.Read(getContent, 0, (int)fs.Length);
				fs.Close();

				Response.BinaryWrite(getContent);
			} 
		}

		
		private string getRole()
		{
			User user = new User();
			user.UserID = userID;
			userRoleArray = com.common.RBAC.RBACManager.GetAllRoles(strAppID, strEnterpriseID, user);
			Role role;

			for(int i=0;i<userRoleArray.Count;i++)
			{
				role = (Role)userRoleArray[i];				
				if (role.RoleName.ToUpper().Trim() == "PREPRINTIMPORT") return "PREPRINTIMPORT";

				if (role.RoleName.ToUpper().Trim() == "PREPRINTEXPORTSTAFF") return "PREPRINTEXPORTSTAFF";
			}
			return " ";
		}

		
		private void btnCancelEx_Click(object sender, System.EventArgs e)
		{
			int iRowsAffected = 0;
			string strPayerId = txtPayerId.Text.ToString();
			string strConsDate = txtConsDate.Text.ToString();

			if ((strPayerId.Trim() == "") && (strConsDate.Trim() == ""))
			{
				lblResult.Text = "Please enter Payer ID and Consignment Date.";
			}
			else if (strPayerId.Trim() == "")
			{
				lblResult.Text = "Please enter Payer ID.";
			}
			else if (strConsDate.Trim() == "")
			{
				lblResult.Text = "Please enter Consignment Date.";
			}
			else
			{
				iRowsAffected = ConsignmentNoteDAL.UpdateCancelExport(strAppID, strEnterpriseID, strPayerId, strConsDate);

				lblResult.Text = "Canceled Affectation = " + Convert.ToString(iRowsAffected) + " Row(s)";
			}
		}

		
		private void btnExbyStaff_Click(object sender, System.EventArgs e)
		{
			string strPayerId = txtPayerId.Text.ToString();	
			string strConsDate = txtConsDate.Text.ToString();
			string Res = "";

			if ((strPayerId.Trim() == "") && (strConsDate.Trim() == ""))
			{
				lblResult.Text = "Please enter Payer ID and Consignment Date.";
			}
			else if (strPayerId.Trim() == "")
			{
				lblResult.Text = "Please enter Payer ID.";
			}
			else if (strConsDate.Trim() == "")
			{
				lblResult.Text = "Please enter Consignment Date.";
			}
			else
			{ 
				DataSet dsConsignment = new DataSet();
				DataSet dsPackage = new DataSet(); 

				try
				{
					dsConsignment = ConsignmentNoteDAL.GetExportConsignmentByStaff(strAppID, strEnterpriseID, strPayerId, strConsDate);
					dsPackage = ConsignmentNoteDAL.GetExportPackageByStaff(strAppID, strEnterpriseID, strPayerId, strConsDate);

					if ((dsConsignment != null) && (dsConsignment.Tables.Count > 0) && (dsConsignment.Tables[0] != null) && (dsPackage != null) && (dsPackage.Tables.Count > 0) && (dsPackage.Tables[0] != null))
					{
						TIESClasses.CreateXls Xls = new TIESClasses.CreateXls();
						// Set type column
						string[] ColumnType = new string[dsConsignment.Tables[0].Columns.Count];
						ColumnType[1] = Xls.TypeText; 

						Res = Xls.createXlsConsignment(dsConsignment, dsPackage, GetPathExport(),ColumnType);
						if (Res == "0")
						{	
							GetDownloadFile(Res);
				
							// Kill Process after save
							Xls.KillProcesses("EXCEL");
						} 
						else
						{
							ErrorMsg.Text = Res;
						}
					}
				}
				catch(Exception ex)
				{
					ErrorMsg.Text = ex.Message.ToString() ;
				}
				finally
				{
					dsConsignment.Dispose();
					dsPackage.Dispose(); 
				}
			}
		}
	}
}
