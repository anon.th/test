using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;  
using System.Drawing;
using System.Text;
using System.Configuration;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Web.Security;  
using System.Globalization; 
using com.common.classes;
using com.common.RBAC;
using com.common.util;
using com.common.DAL;
using com.common.applicationpages;
using com.ties.DAL;
using com.ties.classes;
using com.ties.BAL;  
using Cambro.Web.DbCombo;
using TIES;
using TIESDAL;

namespace com.ties
{
	/// <summary>
	/// Summary description for BoxSize.
	/// </summary>
	public class BoxSize : BasePage
	{
		protected System.Web.UI.WebControls.Label lblMainTitle;
		protected System.Web.UI.WebControls.Panel PanelSearch;
		protected System.Web.UI.WebControls.Panel PanelData;
		protected System.Web.UI.WebControls.Label ErrorMsg;
		protected System.Web.UI.WebControls.Label ErrorMsg1;

		Utility utility = null;
		String strAppID = null;
		String strEnterpriseID = null;
		String PayerID = null;
		String userID = null;
		public String BoxSizeID = "";
		public String ItemIndex = "";
		public String strMsg = "";
		protected System.Web.UI.WebControls.TextBox txtBoxSizeID;
		protected System.Web.UI.WebControls.TextBox txtWidth;
		protected System.Web.UI.WebControls.TextBox txtLength;
		protected System.Web.UI.WebControls.TextBox txtHeight;
		protected System.Web.UI.WebControls.TextBox txtMaxWeight;
		protected System.Web.UI.WebControls.DataGrid dgChargeRate;
		protected System.Web.UI.WebControls.DropDownList lstService;
		protected System.Web.UI.WebControls.DropDownList lstBoxSize;
		protected System.Web.UI.WebControls.Panel PanelButton;
		protected System.Web.UI.WebControls.LinkButton btnEditItem;
		protected System.Web.UI.WebControls.LinkButton btnDeleteItem;
		protected System.Web.UI.WebControls.LinkButton btnAddItem;
		protected System.Web.UI.WebControls.LinkButton btnSaveItem;
		protected System.Web.UI.WebControls.LinkButton btnCancelItem;
		protected System.Web.UI.WebControls.DropDownList lstServiceAdd;
		protected System.Web.UI.WebControls.Label lblNotFound;
		public String strMsg1 = "";
	
		private void Page_Load(object sender, System.EventArgs e)
		{
			String ServerAction = "";	

			try
			{
				utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings,Page.Session);
				strAppID = Session["applicationID"] + ""; 
				strEnterpriseID = Session["enterpriseID"] + ""; 
				userID = Session["userID"] + ""; 
				PayerID = Session["PayerID"] + "";
				if (!Page.IsPostBack)
				{
					ServerAction = Request.QueryString["ServerAction"] + "";
					BoxSizeID = Request.QueryString["BoxSizeID"] + ""; 
					ItemIndex = Request.QueryString["ItemIndex"] + "";
					PanelSearch.Visible = true;
					PanelData.Visible = false;
					PanelButton.Visible = false;
					LoadBoxSize();
				}
				else
				{
					ServerAction = Request.Form["ServerAction"] + ""; 
					BoxSizeID = Request.Form["BoxSizeID"] + "";
//					BoxSizeID = txtBoxSizeID.Text;
					ItemIndex = Request.Form["ItemIndex"] + "";
				}

				switch (ServerAction.ToUpper())
				{
					case "CLEAR":
						strMsg = "";
						ErrorMsg.Text = "";
						ErrorMsg1.Text = "";
						PanelSearch.Visible = true;
						PanelData.Visible = false ;
						PanelButton.Visible = false;
						LoadBoxSize();
						break;
					case "SEARCH":
						BoxSizeID = lstBoxSize.SelectedValue;
						LoadData(BoxSizeID);

						if (BoxSizeID == "STD") 
						{
							txtWidth.ReadOnly = true;
							txtWidth.CssClass = "txtReadOnly";
							txtLength.ReadOnly = true;
							txtLength.CssClass = "txtReadOnly";
							txtHeight.ReadOnly = true;
							txtHeight.CssClass = "txtReadOnly";							
							txtMaxWeight.ReadOnly = true;
							txtMaxWeight.CssClass = "txtReadOnly";
						}
						else
						{
							txtWidth.ReadOnly = false;
							txtWidth.CssClass = "";
							txtLength.ReadOnly = false;
							txtLength.CssClass = "";
							txtHeight.ReadOnly = false;
							txtHeight.CssClass = "";							
							txtMaxWeight.ReadOnly = false;
							txtMaxWeight.CssClass = "";
						}
						break;
					case "INSERT":
						ClearData();
						break;
					case "DELETE":
						DeleteData();
						break;
					case "SAVE":
						SaveData();
						break;
				}
			}
			catch(ApplicationException appException)
			{
				strMsg = appException.Message.ToString();
			}
			finally
			{
				if (strMsg != "") {ErrorMsg.Text = strMsg;}
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private void LoadBoxSize()
		{
			DataSet DS = new DataSet();
			DataTable DT = new DataTable();
 
			try
			{
				DS = ConsignmentNoteDAL.SearchBoxSize(strAppID,  strEnterpriseID,  PayerID, "","","","");
				if (DS != null && DS.Tables.Count > 0)
				{
					DT = DS.Tables[0];
					lstBoxSize.DataSource = DT;
					lstBoxSize.DataTextField = "Box_SizeID";
					lstBoxSize.DataValueField = "Box_SizeID";
					lstBoxSize.DataBind(); 
				}			
			}
			catch(ApplicationException appException)
			{
				strMsg = appException.Message.ToString();
			}
			finally
			{
				if (strMsg != "") {ErrorMsg.Text = strMsg;}
			}
		}

		private void LoadServiceTypeCombo(DropDownList lst)
		{
			try
			{
				//Clear DropDownList
				lst.Items.Clear();  

				//load lstServiceType
				ListItem defItem = new ListItem();
				defItem.Text = "";
				defItem.Value = "0";
				lst.Items.Add(defItem);
				ArrayList ServiceType = Utility.GetListConsServiceType(strAppID, strEnterpriseID, "");
				foreach(SystemCode sysCode in ServiceType)
				{
					ListItem lstItem = new ListItem();
					lstItem.Text = sysCode.Text;
					lstItem.Value = sysCode.StringValue;
					lst.Items.Add(lstItem);  
				}
			}
			catch(ApplicationException appException)
			{
				strMsg = appException.Message.ToString();
			}
			finally
			{
				if (strMsg != "") {ErrorMsg.Text = strMsg;}
			}
		}



		private void LoadData(String BoxSizeID)
		{
			DataSet DS = new DataSet();
			DataTable DT = new DataTable();
			DataRow DR = null;
 
			try
			{
				ClearData();
				if (BoxSizeID != "")
				{
					DS = ConsignmentNoteDAL.SearchBoxSize(strAppID,  strEnterpriseID,  PayerID, BoxSizeID, "", "", "");
					if (DS != null && DS.Tables.Count > 0)
					{
						DT = DS.Tables[0];
						if (DT != null && DT.Rows.Count > 0)
						{
							DR = DT.Rows[0];
							if (!DR["BOX_SIZEID"].GetType().Equals(System.Type.GetType("System.DBNull")))
							{
								txtBoxSizeID.Text = (String)DR["BOX_SIZEID"];
								txtBoxSizeID.ReadOnly = true;
								txtBoxSizeID.CssClass = "txtReadOnly";
							}
							if (!DR["BOX_WIDE"].GetType().Equals(System.Type.GetType("System.DBNull")))
							{
								txtWidth.Text = String.Format("{0:0,0.00}", DR["BOX_WIDE"] + "");
							}
							if (!DR["BOX_LENGTH"].GetType().Equals(System.Type.GetType("System.DBNull")))
							{
								txtLength.Text = String.Format("{0:0,0.00}", DR["BOX_LENGTH"] + "");
							}
							if (!DR["BOX_HEIGHT"].GetType().Equals(System.Type.GetType("System.DBNull")))
							{
								txtHeight.Text = String.Format("{0:0,0.00}", DR["BOX_HEIGHT"] + "");
							}
							if (!DR["BOX_MAXWEIGHT"].GetType().Equals(System.Type.GetType("System.DBNull")))
							{
								txtMaxWeight.Text = String.Format("{0:0,0.00}", DR["BOX_MAXWEIGHT"] + "");
							}
							
							//Load ChargeRate
							LoadChargeRate(BoxSizeID);
							this.BoxSizeID = BoxSizeID;
						}
					}
				}
			}
			catch(ApplicationException appException)
			{
				strMsg = appException.Message.ToString();
			}
			finally
			{
				if (strMsg != "") {ErrorMsg.Text = strMsg;}
			}
		}

		private void LoadChargeRate(String BoxSizeID)
		{
			DataSet DS = new DataSet();
			DataTable DT = new DataTable();
 
			try
			{
				DS = ConsignmentNoteDAL.SearchChargeRate(strAppID,  strEnterpriseID,  PayerID,  BoxSizeID);
				if (DS != null && DS.Tables.Count > 0)
				{
					Session["ChargeRate"] = DS.Tables[0];
					BindDG();
				}
			}
			catch(ApplicationException appException)
			{
				strMsg = appException.Message.ToString();
			}
			finally
			{
				if (strMsg != "") {ErrorMsg.Text = strMsg;}
			}
		}

		private void BindDG()
		{
			DataTable DT = new DataTable();
 
			try
			{
				DT = (DataTable)Session["ChargeRate"];
				if (DT != null)
				{
					dgChargeRate.DataSource = DT;
					dgChargeRate.DataBind(); 
				}
			}
			catch(ApplicationException appException)
			{
				strMsg = appException.Message.ToString();  
			}
			finally
			{
				if (strMsg != "") {ErrorMsg.Text = strMsg;}
			}
		}

		private void ClearData()
		{
			strMsg = "";
			ErrorMsg.Text = "";
			ErrorMsg1.Text = "";
 
			try
			{
				PanelSearch.Visible = false;
				PanelData.Visible = true;
				PanelButton.Visible = true;
				txtBoxSizeID.ReadOnly = false;
				txtBoxSizeID.CssClass = "";

				LoadChargeRate("-1");
				txtBoxSizeID.Text = "";
				txtWidth.Text = "";
				txtLength.Text = "";
				txtHeight.Text = "";
				txtMaxWeight.Text = "";
				BoxSizeID = "";

				txtWidth.ReadOnly = false;
				txtWidth.CssClass = "";
				txtLength.ReadOnly = false;
				txtLength.CssClass = "";
				txtHeight.ReadOnly = false;
				txtHeight.CssClass = "";							
				txtMaxWeight.ReadOnly = false;
				txtMaxWeight.CssClass = "";
			}
			catch (ApplicationException appException)
			{
				strMsg = appException.Message.ToString();
			}
			finally
			{
				if (strMsg != "") {ErrorMsg.Text = strMsg;}
			}
		}


		private void DeleteData()
		{
			int iRowsAffected = 0;
			strMsg = "";
			ErrorMsg.Text = "";
			ErrorMsg1.Text = "";

			if (BoxSizeID == "STD")
			{
				strMsg = "Can't delete Standard Box Size .";
			}

			try
			{
				if (BoxSizeID != "" && BoxSizeID != "STD")
				{
					iRowsAffected = ConsignmentNoteDAL.DeleteBoxSize(strAppID, strEnterpriseID, PayerID, BoxSizeID);
					if (iRowsAffected > 0)
					{
						ClearData();
						strMsg = "Delete successful.";
					}
					else if (iRowsAffected == -1)
					{
						strMsg = "Can't delete data that is referred to Consignment Note.";
					}
					else
					{
						strMsg = "Delete fail.";
					}
				}
			}
			catch (ApplicationException appException)
			{
				strMsg = appException.Message.ToString();
			}
			finally
			{
				if (strMsg != "") {ErrorMsg.Text = strMsg;}
			}
		}

		private void SaveData()
		{
			DataSet DS = new DataSet();
			DataTable DT = new DataTable();
			int iRowsAffected = 0;
			strMsg1 = "";
			strMsg = "";
			ErrorMsg.Text = "";
			ErrorMsg1.Text = "";

			try
			{
				if (txtBoxSizeID.Text.Trim() == "") {strMsg1 += "BoxSize ID, ";}

				if (txtWidth.Text.Trim() != "")
				{
					if (Utility.CheckNumeric(txtWidth.Text.Trim(), false) != true) {strMsg += "Width, ";}
				}
				else
				{
					strMsg1 += "Width, ";
				}

				if (txtLength.Text.Trim() != "")
				{
					if (Utility.CheckNumeric(txtLength.Text.Trim(), false) != true) {strMsg += "Length, ";}
				}
				else
				{
					strMsg1 += "Length, ";
				}

				if (txtHeight.Text.Trim() != "")
				{
					if (Utility.CheckNumeric(txtHeight.Text.Trim(), false) != true) {strMsg += "Height, ";}
				}
				else
				{
					strMsg1 += "Height, ";
				}

				if (txtMaxWeight.Text.Trim() != "")
				{
					if (Utility.CheckNumeric(txtMaxWeight.Text.Trim(), false) != true) {strMsg += "Max Weight, ";}
				}
				else
				{
					txtMaxWeight.Text = "0";
				}

				if (strMsg == "" && strMsg1 == "")
				{
					if (BoxSizeID == "")
					{
						//Insert data
//						if (txtBoxSizeID.Text.Trim() != "") 
//						{
//							DS = ConsignmentNoteDAL.SearchBoxSize(strAppID, strEnterpriseID, PayerID, txtBoxSizeID.Text.Trim(), "", "", "");
//							if (DS != null && DS.Tables.Count > 0)
//							{
//								DT = DS.Tables[0];
//								if (DT != null && DT.Rows.Count > 0)//{strMsg += "Find BoxSize ID duplicate., ";}
//								{
//									iRowsAffected = ConsignmentNoteDAL.UpdateBoxSize(strAppID, strEnterpriseID, PayerID,  txtBoxSizeID.Text.Trim().ToUpper(), txtWidth.Text.Trim(), txtLength.Text.Trim(), txtHeight.Text.Trim(), txtMaxWeight.Text.Trim(), (DataTable)Session["ChargeRate"]); 
//									if (iRowsAffected > 0)
//									{
//										LoadData(txtBoxSizeID.Text.Trim());
//										strMsg = "Save successful.";
//										txtBoxSizeID.ReadOnly = true;
//										txtBoxSizeID.CssClass = "txtReadOnly";
//										strMsg = "Save successful.";
//									}
//									else
//									{
//										strMsg = "Save fail.";
//									}
//								}
//							}
//						}
						
						if (strMsg == "")
						{
							BoxSizeID = ConsignmentNoteDAL.InsertBoxSize(strAppID, strEnterpriseID, PayerID,  txtBoxSizeID.Text.Trim().ToUpper(), txtWidth.Text.Trim(), txtLength.Text.Trim(), txtHeight.Text.Trim(), txtMaxWeight.Text.Trim(), (DataTable)Session["ChargeRate"]);
							if (BoxSizeID != "")
							{
								LoadData(BoxSizeID);
								strMsg = "Save successful.";
								txtBoxSizeID.ReadOnly = true;
								txtBoxSizeID.CssClass = "txtReadOnly";
								strMsg = "Save successful.";
							}
							else
							{
								strMsg = "Save fail";
							}
						}
					}
					else
					{
						if (txtBoxSizeID.Text == "STD"){strMsg = "Can't update Standard Box Size data.";}
						else
						{
							//Update data
							iRowsAffected = ConsignmentNoteDAL.UpdateBoxSize(strAppID, strEnterpriseID, PayerID,  txtBoxSizeID.Text.Trim().ToUpper(), txtWidth.Text.Trim(), txtLength.Text.Trim(), txtHeight.Text.Trim(), txtMaxWeight.Text.Trim(), (DataTable)Session["ChargeRate"]); 
							if (iRowsAffected > 0)
							{
								LoadData(txtBoxSizeID.Text.Trim());
								strMsg = "Save successful.";
								txtBoxSizeID.ReadOnly = true;
								txtBoxSizeID.CssClass = "txtReadOnly";
								strMsg = "Save successful.";
							}
							else if (iRowsAffected == -1)
							{
								strMsg = "Can't modify data that is referred to Consignment Note.";
							}
							else
							{
								strMsg = "Save fail.";
							}
						}
					}
				}
				else
				{
					if (strMsg.Length > 0) {strMsg =  Utility.GetLanguageText(ResourceType.UserMessage,"PLS_CHECK_DATA",utility.GetUserCulture()) + strMsg.Substring(0, strMsg.Length - 2) ;}
					if (strMsg1.Length > 0) {strMsg1 = Utility.GetLanguageText(ResourceType.UserMessage,"PLS_CHECK_DATA",utility.GetUserCulture()) + strMsg1.Substring(0, strMsg1.Length - 2);}
				}
			}
			catch (ApplicationException appException)
			{
				strMsg = appException.Message.ToString();
			}
			finally
			{
				if (strMsg != "") {ErrorMsg.Text = strMsg;}
				if (strMsg1 != "") {ErrorMsg1.Text = strMsg1;}
			}
		}


		private void dgChargeRate_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
		{
			DataSet DS = new DataSet();
			DataTable DT = new DataTable();
			DataRow DR = null;
			DropDownList lstServiceAdd;
			DropDownList lstService;
			String Service = "";

			try
			{
				if (e.Item.ItemType == ListItemType.Footer)
				{
					lstServiceAdd = (DropDownList)(e.Item.FindControl("lstServiceAdd"));
					if (lstServiceAdd != null)
					{
						LoadServiceTypeCombo(lstServiceAdd);
					}				
				}

				if (e.Item.ItemType == ListItemType.EditItem)
				{
					// Set ServiceType in Datagrid Edited
					lstService = (DropDownList)(e.Item.FindControl("lstService"));
					if (lstService != null)
					{
						LoadServiceTypeCombo(lstService);
						Service = DataBinder.Eval(e.Item.DataItem, "SERVICE_CODE").ToString(); 
						lstService.SelectedValue = Service;

						// Set ServiceDescription in Datagrid Edited
						if (Service != "")
						{
							DS = ConsignmentNoteDAL.GetServiceType(strAppID, strEnterpriseID, PayerID, Service);
							if (DS != null && DS.Tables.Count > 0)
							{
								DT = DS.Tables[0];
								if (DT != null && DT.Rows.Count > 0) 
								{
									DR = DT.Rows[0]; 
									e.Item.Cells[4].Text = DR["SERVICE_DESCRIPTION"] + "";
								}
							}
						}
					}
				}
					
				if (e.Item.ItemType == ListItemType.Item  || e.Item.ItemType == ListItemType.AlternatingItem)
				{
					e.Item.Attributes.Add("onmouseover", "javascript:ShowBar(this);");
					e.Item.Attributes.Add("onmouseout", "javascript:HideBar(this);");   
				}
			}
			catch(ApplicationException appException)
			{
				strMsg = appException.Message.ToString();  
			}
			finally
			{
				if (strMsg != "") {ErrorMsg.Text = strMsg;}
			}
		}

		private void dgChargeRate_ItemCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
		{
			DoItemAction(e.CommandName, e.Item); 
		}

		private void DoItemAction(String action, System.Web.UI.WebControls.DataGridItem item)
		{
			DataTable DT = new DataTable();
			DataRow DR = null;
			DataSet dsService = new DataSet();
			DataTable dtService = new DataTable();
			DataRow drService = null;
			DataSet dsCharge = new DataSet();
			DataTable dtCharge = new DataTable();
			DropDownList lstServiceAdd;
			DropDownList lstService;
			String ServiceType = "";
			String ServiceDesc = "";
			String Rate = "";
			String Msg = "";
			String Msg1 = "";
			ErrorMsg.Text = "";
			ErrorMsg1.Text = "";

			try
			{
				action = action.ToUpper();
 
				DT = (DataTable)Session["ChargeRate"];
				dgChargeRate.EditItemIndex = -1;

				switch (action)
				{
					case "ADD_ITEM":
						lstServiceAdd = (DropDownList)item.FindControl("lstServiceAdd");
						if (lstServiceAdd != null){ServiceType = lstServiceAdd.SelectedValue;}
						if (ServiceType == "0") 
						{
							Msg += " Service Type,";
						}
						else
						{
							//check duplicate ServiceType
							if (DT.Select("SERVICE_CODE='" + ServiceType + "'").Length != 0) {Msg1 = "Find ServiceType duplicate.";}

							dsService = ConsignmentNoteDAL.GetServiceType(strAppID, strEnterpriseID, PayerID, ServiceType);
							if (dsService != null && dsService.Tables.Count > 0)
							{
								dtService = dsService.Tables[0];
								if (dtService != null && dtService.Rows.Count > 0) 
								{
									drService = dtService.Rows[0]; 
									ServiceDesc = drService["SERVICE_DESCRIPTION"] + "";
								}
							}

							Rate = Request.Form["txtRateAdd"].ToString().Trim();
							if (Rate == "") 
							{
								Msg += " Charge Rate (Baht),";
							}
							else
							{
								if (Utility.CheckNumeric(Rate, false) != true) {Msg1 += " Charge Rate (Baht)";}
							}
						}

						if (Msg == "" && Msg1 == "")
						{
							DR = DT.NewRow();
							DR["SERVICE_CODE"] = ServiceType;
							DR["SERVICE_DESCRIPTION"] = ServiceDesc;
							DR["CHARGE_RATE"] = Convert.ToDouble(Rate);
							DT.Rows.Add(DR); 
						}
						dgChargeRate.EditItemIndex = -1;
						break;

					case "SAVE_ITEM":
						DR = DT.Rows[item.ItemIndex];
						lstService = (DropDownList)item.FindControl("lstService");
						if (lstService != null){ServiceType = lstService.SelectedValue;}
						if (ServiceType == "0") 
						{
							Msg += " Service Type,";
						}
						else
						{
							//check duplicate ServiceType
							if (DT.Select("SERVICE_CODE='" + ServiceType + "' AND RATEID=" + item.ItemIndex).Length != 0) {Msg1 = "Find ServiceType duplicate.";}

							dsService = ConsignmentNoteDAL.GetServiceType(strAppID, strEnterpriseID, PayerID, ServiceType);
							if (dsService != null && dsService.Tables.Count > 0)
							{
								dtService = dsService.Tables[0];
								if (dtService != null && dtService.Rows.Count > 0) 
								{
									drService = dtService.Rows[0]; 
									ServiceDesc = drService["SERVICE_DESCRIPTION"] + "";
								}
							}

							Rate = Request.Form["txtRate"].ToString().Trim();
							if (Rate == "") 
							{
								Msg += " Charge Rate (Baht),";
							}
							else
							{
								if (Utility.CheckNumeric(Rate, false) != true) {Msg1 += " Charge Rate (Baht)";}
							}
						}

						if (Msg == "" && Msg1 == "")
						{
							DR["SERVICE_CODE"] = ServiceType;
							DR["SERVICE_DESCRIPTION"] = ServiceDesc;
							DR["CHARGE_RATE"] = Convert.ToDouble(Rate);
						}
						dgChargeRate.EditItemIndex = -1;
						break;

					case "DELETE_ITEM":
						if (item.ItemIndex < DT.Rows.Count)
						{
							DT.Rows.RemoveAt(item.ItemIndex); 
						}
						dgChargeRate.EditItemIndex = -1;
						break;

					case "EDIT_ITEM":
						dgChargeRate.EditItemIndex = item.ItemIndex; 
						break;

					case "CANCEL_ITEM":
						dgChargeRate.EditItemIndex = -1;
						break;
				}
				Session["ChargeRate"] = DT;
				BindDG();

				if (Msg != "") {Msg = Utility.GetLanguageText(ResourceType.UserMessage,"PLS_CHECK_DATA",utility.GetUserCulture()) + Msg.Substring(0, Msg.Length - 1);}
				if (Msg1 != "") {Msg1 = Utility.GetLanguageText(ResourceType.UserMessage,"PLS_CHECK_DATA",utility.GetUserCulture()) + Msg1;}
			}
			catch(ApplicationException appException)
			{
				strMsg = appException.Message.ToString();
			}
			finally
			{
				if (strMsg != "") {ErrorMsg.Text = strMsg;}
				if (strMsg1 != "") {ErrorMsg1.Text = strMsg1;}
			}
		}


		



	}
}
