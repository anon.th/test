using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;  
using System.Drawing;
using System.Text;
using System.Configuration;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Web.Security;  
using System.Globalization; 
using com.common.classes;
using com.common.RBAC;
using com.common.util;
using com.common.DAL;
using com.common.applicationpages;
using com.ties.DAL;
using com.ties.classes;
using com.ties.BAL;  
using Cambro.Web.DbCombo;
using TIES;
using TIESDAL;

namespace com.ties
{
	/// <summary>
	/// Summary description for SetPrint.
	/// </summary>
	public class SetPrint : BasePage
	{
		protected System.Web.UI.WebControls.Label lblMainTitle;
		protected System.Web.UI.WebControls.CheckBox chkP1;
		protected System.Web.UI.WebControls.CheckBox chkP2;
		protected System.Web.UI.WebControls.CheckBox chkP3;
		protected System.Web.UI.WebControls.CheckBox chkP4;
		protected System.Web.UI.WebControls.CheckBox chkP5;
		protected System.Web.UI.WebControls.CheckBox chkP6;
		protected System.Web.UI.WebControls.Label ErrorMsg;

		Utility utility = null;
		String appID = null;
		String enterpriseID = null;
		String userID = null;
		String PayerID = "";
		string strMsg=null;
	
		private void Page_Load(object sender, System.EventArgs e)
		{
			String ServerAction = "";

			try
			{
				utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings,Page.Session);
				appID = utility.GetAppID();
				enterpriseID = utility.GetEnterpriseID();
				userID = utility.GetUserID();
				PayerID = Session["PayerID"] + "";

				if (!Page.IsPostBack)
				{
					ServerAction = Request.QueryString["ServerAction"] + "";
				}
				else
				{
					ServerAction = Request.Form["ServerAction"] + ""; 
				}

				if (ServerAction == ""){ServerAction = "SEARCH";}

				switch (ServerAction.ToUpper())
				{
					case "SEARCH":
						LoadData();
						break;
					case "SAVE":
						SaveData();
						break;
				}
			}
			catch(ApplicationException appException)
			{
				strMsg = appException.Message.ToString();
				ErrorMsg.Text = strMsg;
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
	
		private void LoadData()
		{
			DataSet DS = new DataSet(); 
			DataTable DT = new DataTable();
			String ParamName = "";
			String ParamValue = "";

			try
			{
				DS = ConsignmentNoteDAL.GetParameterConsignment(appID, enterpriseID, PayerID);   
				if (DS != null && DS.Tables.Count != 0)
				{
					DT = DS.Tables[0]; 
					if (DT != null && DT.Rows.Count > 0)
					{
						foreach(DataRow DR in DT.Rows)
						{
							ParamName = DR["ParamName"] + "";
							ParamValue = DR["ParamValue"] + "";
							switch (ParamName)
							{
								case "P1_Shipper":
									if (ParamValue != "Y")
									{
										chkP1.Checked = false;
									}
									else
									{
										chkP1.Checked = true;
									}
									break;
								case "P2_DataEntry":
									if (ParamValue != "Y")
									{
										chkP2.Checked = false;
									}
									else
									{
										chkP2.Checked = true;
									}
									break;
								case "P3_HCR1":
									if (ParamValue != "Y")
									{
										chkP3.Checked = false;
									}
									else
									{
										chkP3.Checked = true;
									}
									break;
								case "P4_HCR2":
									if (ParamValue != "Y")
									{
										chkP4.Checked = false;
									}
									else
									{
										chkP4.Checked = true;
									}
									break;
								case "P5_Destination":
									if (ParamValue != "Y")
									{
										chkP5.Checked = false;
									}
									else
									{
										chkP5.Checked = true;
									}
									break;
								case "P6_Consignee":
									if (ParamValue != "Y")
									{
										chkP6.Checked = false;
									}
									else
									{
										chkP6.Checked = true;
									}
									break;
							}
						}
					}
				}
			}
			catch(ApplicationException appException)
			{
				strMsg = appException.Message.ToString();
				ErrorMsg.Text = strMsg;
			}
		}

		private void SaveData()
		{
			int iRowsAffected = 0;
			String P1 = "";
			String P2 = "";
			String P3 = "";
			String P4 = "";
			String P5 = "";
			String P6 = "";

			try
			{
				if (chkP1.Checked != true) {P1 = "N";} else {P1 = "Y";}
				if (chkP2.Checked != true) {P2 = "N";} else {P2 = "Y";}
				if (chkP3.Checked != true) {P3 = "N";} else {P3 = "Y";}
				if (chkP4.Checked != true) {P4 = "N";} else {P4 = "Y";}
				if (chkP5.Checked != true) {P5 = "N";} else {P5 = "Y";}
				if (chkP6.Checked != true) {P6 = "N";} else {P6 = "Y";}
				iRowsAffected = ConsignmentNoteDAL.UpdateParameterPrintPage(appID, enterpriseID, PayerID, P1, P2, P3, P4, P5, P6);
				if (iRowsAffected < 0)
				{
					strMsg = "Error Saving. ";
				}
				else
				{
					strMsg = "Save successful.";
				}
				ErrorMsg.Text = strMsg;
			}
			catch(ApplicationException appException)
			{
				strMsg = appException.Message.ToString();
				ErrorMsg.Text = strMsg;
			}
		}
	}
}
