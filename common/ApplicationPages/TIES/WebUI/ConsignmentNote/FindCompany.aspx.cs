using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;  
using System.Drawing;
using System.Text;
using System.Configuration;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Web.Security;  
using System.Globalization; 
using com.common.classes;
using com.common.RBAC;
using com.common.util;
using com.common.DAL;
using com.common.applicationpages;
using com.ties.DAL;
using com.ties.classes;
using com.ties.BAL;  
using Cambro.Web.DbCombo;
using TIES;
using TIESDAL;

namespace com.ties
{
	/// <summary>
	/// Summary description for FindCompany.
	/// </summary>
	public class FindCompany : BasePopupPage //BasePage
	{
		protected System.Web.UI.WebControls.TextBox txtCustomerCode;
		protected System.Web.UI.WebControls.TextBox txtCompanyName;
		protected System.Web.UI.WebControls.TextBox txtTelephone;
		protected System.Web.UI.WebControls.DataGrid dgResult;

		Utility utility = null;
		String appID = null;
		String enterpriseID = null;
		String userID = null;
		protected System.Web.UI.WebControls.Label ErrorMsg;
		protected System.Web.UI.WebControls.Label lblNotFound;
		string strMsg=null;

	
		private void Page_Load(object sender, System.EventArgs e)
		{
			String ServerAction = "";

			try
			{
				utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings,Page.Session);
				appID = utility.GetAppID();
				enterpriseID = utility.GetEnterpriseID();
				userID = utility.GetUserID();

				if (!Page.IsPostBack)
				{
					ServerAction = Request.QueryString["ServerAction"] + "";
				}
				else
				{
					ServerAction = Request.Form["ServerAction"] + ""; 
				}

				switch (ServerAction.ToUpper())
				{
					case "SEARCH":
						LoadData();
						break;
					case "CLEAR":
						ClearData();
						break;
				}
			}
			catch(ApplicationException appException)
			{
				strMsg = appException.Message.ToString();
				ErrorMsg.Text = strMsg;
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.dgResult.PageIndexChanged += new System.Web.UI.WebControls.DataGridPageChangedEventHandler(this.dgResult_PageIndexChanged);
			this.dgResult.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.dgResult_ItemDataBound);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private void LoadData()
		{
			DataSet DS = new DataSet();
 
			try
			{
				dgResult.CurrentPageIndex = 0;
				Session["SEARCH_COMPANY"] = null;
				DS = ConsignmentNoteDAL.SearchCompanny(utility.GetAppID(), utility.GetEnterpriseID(), Session["PayerID"] + "", txtCustomerCode.Text, txtCompanyName.Text, txtTelephone.Text);   

				if (DS != null && DS.Tables.Count != 0)
				{
					Session["SEARCH_COMPANY"] = DS.Tables[0]; 
					BindDG();
				}
			}
			catch(ApplicationException appException)
			{
				strMsg = appException.Message.ToString();
				ErrorMsg.Text = strMsg;
			}
		}

		private void BindDG()
		{
			DataTable DT = new DataTable();

			try
			{
				DT = (DataTable)Session["SEARCH_COMPANY"];
				if (DT != null && DT.Rows.Count > 0)
				{
					lblNotFound.Visible = false;
					dgResult.Visible = true;
  
					dgResult.DataSource = DT;
					dgResult.DataBind();  
				}
				else
				{
					lblNotFound.Visible  = true;
					dgResult.Visible =false;  
				}
			}
			catch(ApplicationException appException)
			{
				strMsg = appException.Message.ToString() ;
				ErrorMsg.Text = strMsg;
			}
		}

		private void ClearData()
		{
			txtCustomerCode.Text = "";
			txtCompanyName.Text = "";
			txtTelephone.Text = "";
			dgResult.Visible = false;
		}

		private void dgResult_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
		{
			String Address = "";

			if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
			{
				Address = DataBinder.Eval(e.Item.DataItem,"ADDRESS1") + " " + DataBinder.Eval(e.Item.DataItem,"ADDRESS2") + " " + DataBinder.Eval(e.Item.DataItem,"STATE_NAME") + " " + DataBinder.Eval(e.Item.DataItem,"ZIPCODE");
				Address = Address.Replace("\n", " ");
				Address = Address.Replace("\r", " ");
				e.Item.Attributes["onClick"] = "javascript:SelectItem('" + DataBinder.Eval(e.Item.DataItem,"CUSTOMERCODE") + "','" + DataBinder.Eval(e.Item.DataItem,"REFERENCE_NAME") + "','" + Address + "','" + DataBinder.Eval(e.Item.DataItem,"TELEPHONE") + "','" + DataBinder.Eval(e.Item.DataItem,"ZIPCODE") + "','" + DataBinder.Eval(e.Item.DataItem,"ContactPerson") + "');";
				e.Item.Attributes.Add("onmouseover", "javascript:ShowBar(this);"); 
				e.Item.Attributes.Add("onmouseout", "javascript:HideBar(this);");
			}
		}

		private void dgResult_PageIndexChanged(object source, System.Web.UI.WebControls.DataGridPageChangedEventArgs e)
		{
			dgResult.CurrentPageIndex = e.NewPageIndex;
			BindDG();
		}
	}
}
