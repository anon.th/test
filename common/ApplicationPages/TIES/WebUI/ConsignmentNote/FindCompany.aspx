<%@ Page language="c#" Codebehind="FindCompany.aspx.cs" AutoEventWireup="false" Inherits="com.ties.FindCompany" smartNavigation="False" %>
<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<%@ Import Namespace="com.common.util" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>FindCompany</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../css/Styles.css" type="text/css" rel="Stylesheet">
		<!--#INCLUDE FILE="../msFormValidations.inc"-->
		<script language="javascript" src="../Scripts/settingScrollPosition.js"></script>
		<script language="javascript" type="text/javascript">
		function DoAction(serveraction) {
			frmSearchCompany.elements("ServerAction").value = serveraction;
			frmSearchCompany.submit();
		}
		
		function SelectItem(cid,cname,caddress,ctel,czone,czipcode,ccontactperson){
			/*alert("id = " + cid);
			alert("name = " + cname);
			alert("name = " + caddress);
			alert("name = " + ctel);*/
            window.opener.FindCompany(cid,cname,caddress,ctel,czone,czipcode,ccontactperson);
            window.close();  
        }
        
        var old_bgcolor;
		var old_class;
        function ShowBar(src) {
			if (!src.contains(event.fromElement)) {
				src.style.cursor = 'hand';
				old_class = src.className;
				src.className = 'tabletotal';
			}
		}
		function HideBar(src) {
			if (!src.contains(event.toElement)) {
				src.style.cursor = 'default';
				src.className = old_class;
			}
		}
		</script>
	</HEAD>
	<body onkeypress="microsoftKeyPress()" onclick="GetScrollPosition();" onload="SetScrollPosition();"
		onunload="window.top.displayBanner.fnCloseAll(0);" MS_POSITIONING="ViewLayout">
		<form id="frmSearchCompany" runat="server">
			<input id="ServerAction" type="hidden" name="ServerAction">
			<table cellSpacing="0" cellPadding="0" width="100%" border="0">
				<!--tr>
			<td style="WIDTH:1024px; HEIGHT:120px" background="images/top.jpg" align="right" valign="top">
				<FONT face="Tahoma" color="#ffffff" size="2"><STRONG>User ID : UATT1&nbsp;&nbsp;<BR>
						Customer ID : UNMA&nbsp; &nbsp;<BR>
						Unicity Marketing (Thailand) Co.,Ltd. &nbsp; </STRONG></FONT>
			</td>
		</tr-->
				<tr>
					<td vAlign="bottom" height="50"><span class="style1"><STRONG><FONT color="#ff9933" size="5">Search 
									Company</FONT></STRONG></span></td>
				</tr>
				<tr>
					<td>
						<table class="TextBody" cellSpacing="0" cellPadding="3" border="0">
							<tr class="tablepagingitem" style="HEIGHT: 5px">
								<td style="WIDTH: 120px" align="right">&nbsp;</td>
								<td style="WIDTH: 120px" align="left">&nbsp;</td>
								<td style="WIDTH: 50px" align="right">&nbsp;</td>
								<td style="WIDTH: 100px" align="left">&nbsp;</td>
								<td style="WIDTH: 120px" align="right">&nbsp;</td>
								<td style="WIDTH: 50px" align="right">&nbsp;</td>
							</tr>
							<tr>
								<td colSpan="6"><asp:label id="ErrorMsg" runat="server" Width="643px" CssClass="errorMsgColor" Height="3px"></asp:label></td>
							</tr>
							<tr>
								<td colSpan="6">&nbsp;</td>
							</tr>
							<tr>
								<td>&nbsp;</td>
								<td colSpan="5"><a onclick="javascript:DoAction('CLEAR');"><input class="queryButton" id="btnQuery" style="WIDTH: 60px; HEIGHT: 20px" type="button"
											value="Query" name="btnQuery"></a> <a onclick="javascript:DoAction('SEARCH');">
										<input class="queryButton" id="btnExcute" style="WIDTH: 104px; HEIGHT: 20px" type="button"
											value="Excute Query" name="btnExcute"></a>
								</td>
							</tr>
							<tr class="tablepagingitem" style="HEIGHT: 5px">
								<td colSpan="6">&nbsp;</td>
							</tr>
							<tr>
								<td align="right">Customer&nbsp;Code&nbsp;: &nbsp;</td>
								<td align="left"><asp:textbox id="txtCustomerCode" runat="server" Width="180px"></asp:textbox></td>
								<td>&nbsp;</td>
								<td align="right">Company&nbsp;Name&nbsp;: &nbsp;</td>
								<td align="left"><asp:textbox id="txtCompanyName" runat="server" Width="180px"></asp:textbox></td>
								<td></td>
							</tr>
							<tr>
								<td align="right">Telephone&nbsp;: &nbsp;</td>
								<td align="left"><asp:textbox id="txtTelephone" runat="server" Width="180px"></asp:textbox></td>
								<td colSpan="4"></td>
							</tr>
							<tr>
								<td colSpan="6"></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td><asp:datagrid id="dgResult" runat="server" AllowPaging="True" AutoGenerateColumns="False" PageSize="50"
							BorderWidth="0px" CellSpacing="1" CellPadding="2" HorizontalAlign="Center" HeaderStyle-CssClass="gridHeading"
							ItemStyle-CssClass="gridField" AlternatingItemStyle-CssClass="gridField" BorderColor="#CCCCCC">
							<AlternatingItemStyle CssClass="gridField"></AlternatingItemStyle>
							<ItemStyle CssClass="gridField"></ItemStyle>
							<HeaderStyle Font-Bold="True" HorizontalAlign="Center" CssClass="gridHeading"></HeaderStyle>
							<Columns>
								<asp:TemplateColumn HeaderText="No.">
									<HeaderStyle Width="25px"></HeaderStyle>
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
									<ItemTemplate>
										<%# (dgResult.PageSize*dgResult.CurrentPageIndex)+Container.ItemIndex+1 %>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:BoundColumn DataField="CUSTOMERCODE" HeaderText="Customer Code">
									<HeaderStyle HorizontalAlign="Center" Width="60px"></HeaderStyle>
									<ItemStyle HorizontalAlign="Left"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="REFERENCE_NAME" HeaderText="Company Name">
									<HeaderStyle Width="200px"></HeaderStyle>
									<ItemStyle HorizontalAlign="Left"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="TELEPHONE" HeaderText="Telephone">
									<HeaderStyle Width="50px"></HeaderStyle>
									<ItemStyle HorizontalAlign="Left"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="ADDRESS1" HeaderText="Address1">
									<HeaderStyle Width="250px"></HeaderStyle>
									<ItemStyle HorizontalAlign="Left"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="ADDRESS2" HeaderText="Address2">
									<HeaderStyle Width="150px"></HeaderStyle>
									<ItemStyle HorizontalAlign="Left"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="STATE_NAME" HeaderText="State">
									<HeaderStyle Width="50px"></HeaderStyle>
									<ItemStyle HorizontalAlign="Left"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="ZIPCODE" HeaderText="Zipcode">
									<HeaderStyle Width="30px"></HeaderStyle>
									<ItemStyle HorizontalAlign="Left"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn Visible="False" DataField="ContactPerson"></asp:BoundColumn>
							</Columns>
							<PagerStyle NextPageText="Next" Font-Size="Smaller" PrevPageText="Previous" HorizontalAlign="Right"></PagerStyle>
						</asp:datagrid></td>
				</tr>
				<tr>
					<td align="center"><asp:label id="lblNotFound" runat="server" Visible="False" ForeColor="Red" Font-Bold="True">Data not found.</asp:label></td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
