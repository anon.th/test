using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;  
using System.Drawing;
using System.Text;
using System.Configuration;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Web.Security;  
using System.Globalization; 
using com.common.classes;
using com.common.RBAC;
using com.common.util;
using com.common.DAL;
using com.common.applicationpages;
using com.ties.DAL;
using com.ties.classes;
using com.ties.BAL;  
using Cambro.Web.DbCombo;
using TIES;
using TIESDAL;
using System.Data.OleDb;
using System.IO;

namespace TIES.WebUI
{
	/// <summary>
	/// Summary description for ImportCustomerFile.
	/// </summary>
	public class ImportCustomerFile : BasePage
	{
		protected System.Web.UI.WebControls.Label lblMainTitle;
		protected System.Web.UI.WebControls.Label ErrorMsg;
		protected System.Web.UI.WebControls.Panel pnlCheckStep;
		protected System.Web.UI.WebControls.Panel pnlDescription;
		protected System.Web.UI.WebControls.TextBox txtException;
		protected System.Web.UI.WebControls.Panel pnlException;
		protected System.Web.UI.WebControls.Button btnImport;

		protected String strAppID;
		protected String strEnterpriseID;
		protected String userID;
		protected String strMsg;
		protected String payerID;

		protected const string ImgPathTrue = "../images/OK.png";
		protected const string ImgPathFalse = "../images/NO.png";

		protected System.Web.UI.WebControls.Label lblCheck1;
		protected System.Web.UI.WebControls.Label lblCheck3;
		protected System.Web.UI.WebControls.Label lblCheck4;
		protected System.Web.UI.WebControls.Label lblCheck6;
		protected System.Web.UI.WebControls.Image imgChkFormateFile;
		protected System.Web.UI.WebControls.Image imgChkRequired;
		protected System.Web.UI.WebControls.Image imgChkDataType;
		protected System.Web.UI.WebControls.Button btnClear;
		protected System.Web.UI.HtmlControls.HtmlInputFile file_upload;
		protected System.Web.UI.WebControls.Image imgChkImportComplete;

		private bool uploadFiletoServer()
		{
			bool blReturn = false;

			string fn = System.IO.Path.GetFileName(file_upload.PostedFile.FileName);
			string path = Server.MapPath("..\\Excel") + "\\";
			string pathFn = Server.MapPath("..\\Excel") + "\\" + userID + "_" + fn;

			string strFileName = userID + "_" + fn;
			string strFileNameNew = userID + "_" + System.IO.Path.GetFileNameWithoutExtension(file_upload.PostedFile.FileName) + "_new" + System.IO.Path.GetExtension(file_upload.PostedFile.FileName);
			string strSearch = "Order#";

			ViewState["FileName"] = fn;

			OleDbDataAdapter dataAdapter;
			DataSet dataSet;
			DataSet dsInsert;
			DataTable dtInsert;

			try
			{
				if(System.IO.Directory.Exists(path) == false)
					System.IO.Directory.CreateDirectory(path);

				if (System.IO.File.Exists(pathFn)) 
					System.IO.File.Delete(pathFn);

				file_upload.PostedFile.SaveAs(pathFn);

				TIESClasses.CreateXls cXls =new TIESClasses.CreateXls();
				string err_msg = "";

				if(cXls.delEmptyRowAndColumn(path,strFileName,strFileNameNew,strSearch ,ref err_msg))
				{

					String connString = ConfigurationSettings.AppSettings["ExcelConn"];
					connString = connString.Replace("(DestinationFile)", path + strFileNameNew);


					String[] strSheetName = getExcelSheetName(connString);

					string strSql = "SELECT * FROM [" + strSheetName[0].Trim() + "]";

					dataAdapter = new OleDbDataAdapter(strSql, connString);

					dataSet = new DataSet();

					dataAdapter.Fill(dataSet);

					if(dataAdapter != null)
					{
						dataAdapter.Dispose();
					}
					// Process Delete row and column
					//
					//
					//				dataSet.Tables[0];
					dsInsert = new DataSet("TABLES");
					dtInsert = new DataTable("TABLE");
					dtInsert.Columns.Add("ORDER_NO");
					dtInsert.Columns.Add("TAX_INV_NO");
					dtInsert.Columns.Add("COMPANY_CODE");
					dtInsert.Columns.Add("COMPANY_NAME");
					dtInsert.Columns.Add("ADDRESS_1");
					dtInsert.Columns.Add("ADDRESS_2");
					dtInsert.Columns.Add("ZIPCODE");
					dtInsert.Columns.Add("PHONE_NO");
					dtInsert.Columns.Add("CONTACT_PERSON");
					dtInsert.Columns.Add("SHIPPING_INSTRUCTION");
					dtInsert.Columns.Add("SHIP_VIA");	

					foreach(DataRow dr in dataSet.Tables[0].Rows)
					{
						DataRow drInsert  = dtInsert.NewRow();
						drInsert["ORDER_NO"] = dr["Order#"].ToString();
						drInsert["TAX_INV_NO"] = Utility.ReplaceSingleQuote(dr["TaxInvoiceNumber"].ToString());
						drInsert["COMPANY_CODE"] = Utility.ReplaceSingleQuote(dr["Company#"].ToString());
						drInsert["COMPANY_NAME"] = Utility.ReplaceSingleQuote(dr["CompanyName"].ToString());
						drInsert["ADDRESS_1"] = Utility.ReplaceSingleQuote(dr["Address1"].ToString());
						drInsert["ADDRESS_2"] = Utility.ReplaceSingleQuote(dr["Address2"].ToString());
						drInsert["ZIPCODE"] = dr["Zip#"].ToString();
						drInsert["PHONE_NO"] = dr["PhoneNumber"].ToString().Trim().Equals("") ? "-" : Utility.ReplaceSingleQuote(dr["PhoneNumber"].ToString());
						drInsert["CONTACT_PERSON"] = dr["ShiptoContactName"].ToString().Trim().Equals("") ? "-" : Utility.ReplaceSingleQuote(dr["ShiptoContactName"].ToString());
						drInsert["SHIPPING_INSTRUCTION"] = Utility.ReplaceSingleQuote(dr["ShippingInstruction"].ToString());
						drInsert["SHIP_VIA"] = dr["Ship Via"].ToString();

						dtInsert.Rows.Add(drInsert);
					}
												
					dsInsert.Tables.Add(dtInsert);
					ViewState["DS_Insert"] = dsInsert;

					if(dataSet != null)
					{
						dataSet.Dispose();
					}

					if(dsInsert != null)
					{
						dsInsert.Dispose();
					}

					if(dtInsert != null)
					{
						dtInsert.Dispose();
					}

					blReturn = true;
			
				}
				else
				{
					ErrorMsg.Text = getErrorMsg(utility.GetUserCulture().Trim(),"uploadFiletoServer",err_msg);
				}

				if(System.IO.File.Exists(path + strFileNameNew))
					System.IO.File.Delete(path + strFileNameNew);

				if (System.IO.File.Exists(pathFn)) 
					System.IO.File.Delete(pathFn);
			}
			catch(Exception ex)
			{
				ErrorMsg.Text = getErrorMsg(utility.GetUserCulture().Trim(),"uploadFiletoServer",ex.Message);

				if(System.IO.File.Exists(path + strFileNameNew))
					System.IO.File.Delete(path + strFileNameNew);

				if (System.IO.File.Exists(pathFn)) 
					System.IO.File.Delete(pathFn);
			}

			return blReturn;
		}


		private String[] getExcelSheetName(string conString)
		{
			OleDbConnection objConn = null;
			System.Data.DataTable dt = null;

			try
			{
				// Create connection object by using the preceding connection string.
				objConn = new OleDbConnection(conString);
				// Open connection with the database.
				objConn.Open();
				// Get the data table containg the schema guid.
				dt = objConn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
 
				if(dt == null)
				{
					return null;
				}

				String[] excelSheets = new String[dt.Rows.Count];
				int i = 0;

				// Add the sheet name to the string array.
				foreach(DataRow row in dt.Rows)
				{
					excelSheets[i] = row["TABLE_NAME"].ToString();
					i++;
				}

				// Loop through all of the sheets if you want too...
				for(int j=0; j < excelSheets.Length; j++)
				{
					// Query each excel sheet.
				}

				return excelSheets;
			}
			catch(Exception ex)
			{
				return null;
			}
			finally
			{
				// Clean up.
				if(objConn != null)
				{
					objConn.Close();
					objConn.Dispose();
				}
				if(dt != null)
				{
					dt.Dispose();
				}
			}
		}


		private string getErrorMsg(string strCulture,string ErrorType, string ErrorMsg)
		{
			string strErr = "";
			if(ErrorType.Equals("CheckEmptyFile"))
			{
				if(strCulture.Equals("en-US")){ strErr = "Error : Please select Excel file data before import data!";}
				if(strCulture.Equals("th-TH")){ strErr = "��ͼԴ��Ҵ : ��س����͡ Excel ����͹�ӡ�ù���Ң�����";}
			}
			if(ErrorType.Equals("CheckExcelFormateFile"))
			{
				if(strCulture.Equals("en-US")){ strErr = "Error : Wrong file extension (file extension is Excel file only!)";}
				if(strCulture.Equals("th-TH")){ strErr = "��ͼԴ��Ҵ : ������������١��ͧ (����������ͧ�� Excel �����ҹ��!)";}
			}
			if(ErrorType.Equals("CheckRequriedData"))
			{
				if(strCulture.Equals("en-US")){ strErr = "Error : Please check required data (Order#, Company# and Zipcode#)";}
				if(strCulture.Equals("th-TH")){ strErr = "��ͼԴ��Ҵ : ��سҵ�Ǩ�ͺ������ (Order#, Company# and Zipcode#) ";}
			}
			if(ErrorType.Equals("CheckDataType"))
			{
				if(strCulture.Equals("en-US")){ strErr = "Error : Please check data type (Order#, Company# and Zipcode#)";}
				if(strCulture.Equals("th-TH")){ strErr = "��ͼԴ��Ҵ : ��سҵ�Ǩ�ͺ��Դ������ (Order#, Company# and Zipcode#)";}
			}
			if(ErrorType.Equals("CheckImportAndSaveSucces"))
			{
				if(strCulture.Equals("en-US")){ strErr = "Error : " + ErrorMsg;}
				if(strCulture.Equals("th-TH")){ strErr = "��ͼԴ��Ҵ : " + ErrorMsg;}
			}
			if(ErrorType.Equals("uploadFiletoServer"))
			{
				if(strCulture.Equals("en-US")){ strErr = "Error : " + ErrorMsg;}
				if(strCulture.Equals("th-TH")){ strErr = "��ͼԴ��Ҵ : " + ErrorMsg;}
			}
			
			return strErr;
		}


		private bool CheckEmptyFile()
		{
			bool blReturn = false;
			if(this.file_upload.PostedFile.FileName == "")
			{
				ErrorMsg.Text = getErrorMsg(utility.GetUserCulture().Trim(),"CheckEmptyFile","");
			}
			else
			{
				blReturn = true;
			}
			return blReturn;
		}

		private bool CheckExcelFormateFile()
		{
			bool blReturn = false;

			try
			{
				if((file_upload.PostedFile != null ) && (file_upload.PostedFile.ContentLength > 0))
				{
					string extension = System.IO.Path.GetExtension(file_upload.PostedFile.FileName);

					if(extension.ToUpper() == ".XLS")
					{
						blReturn = true;
						imgChkFormateFile.ImageUrl = ImgPathTrue;
						imgChkFormateFile.Visible = true;
					}
					else
					{

						txtException.Text = getErrorMsg(utility.GetUserCulture(),"CheckExcelFormateFile","");
						imgChkFormateFile.ImageUrl = ImgPathFalse;
						imgChkFormateFile.Visible = true;
					}

				}
			}
			catch(Exception ex)
			{
				txtException.Text = ex.Message;
				imgChkFormateFile.ImageUrl = ImgPathFalse;
				imgChkFormateFile.Visible = true;
			}

			return blReturn;
		}

		private bool CheckRequriedData()
		{
			bool blReturn = false;
			DataSet dsInsert;

			try
			{
				dsInsert = (DataSet)ViewState["DS_Insert"];

				foreach(DataRow dr in dsInsert.Tables[0].Rows)
				{
					if(dr["ORDER_NO"] != null && dr["ORDER_NO"].ToString() != "")
					{
						blReturn = true;
					}
					else
					{
						blReturn = false;
						txtException.Text = getErrorMsg(utility.GetUserCulture(),"CheckRequriedData","");
						imgChkRequired.ImageUrl = ImgPathFalse;
						imgChkRequired.Visible = true;
						break;
					}

					if(dr["COMPANY_CODE"] != null && dr["COMPANY_CODE"].ToString() != "")
					{
						blReturn = true;
					}
					else
					{
						blReturn = false;
						txtException.Text = getErrorMsg(utility.GetUserCulture(),"CheckRequriedData","");
						imgChkRequired.ImageUrl = ImgPathFalse;
						imgChkRequired.Visible = true;
						break;
					}

					if(dr["ZIPCODE"] != null && dr["ZIPCODE"].ToString() != "")
					{
						blReturn = true;
					}
					else
					{
						blReturn = false;
						txtException.Text = getErrorMsg(utility.GetUserCulture(),"CheckRequriedData","");
						imgChkRequired.ImageUrl = ImgPathFalse;
						imgChkRequired.Visible = true;
						break;
					}
				}

				if(dsInsert != null)
				{
					dsInsert.Dispose();
				}
			}
			catch(Exception ex)
			{
				txtException.Text = ex.Message;
				imgChkRequired.ImageUrl = ImgPathFalse;
				imgChkRequired.Visible = true;
			}
			
			
			return blReturn;
		}

		private bool CheckDataType()
		{
			bool blReturn = false;
			DataSet dsInsert;

			try
			{
				dsInsert = (DataSet)ViewState["DS_Insert"];

				foreach(DataRow dr in dsInsert.Tables[0].Rows)
				{
					if(dr["ORDER_NO"] != null && dr["ORDER_NO"].ToString() != "")
					{
						blReturn = true;
					}
					else
					{
						blReturn = false;
						txtException.Text = getErrorMsg(utility.GetUserCulture(),"CheckDataType","");
						imgChkDataType.ImageUrl = ImgPathFalse;
						imgChkDataType.Visible = true;
						break;
					}

					if(dr["COMPANY_CODE"] != null && dr["COMPANY_CODE"].ToString() != "")
					{
						blReturn = true;
					}
					else
					{
						blReturn = false;
						txtException.Text = getErrorMsg(utility.GetUserCulture(),"CheckDataType","");
						imgChkDataType.ImageUrl = ImgPathFalse;
						imgChkDataType.Visible = true;
						break;
					}

					if(dr["ZIPCODE"] != null && dr["ZIPCODE"].ToString() != "")
					{
						blReturn = true;
					}
					else
					{
						blReturn = false;
						txtException.Text = getErrorMsg(utility.GetUserCulture(),"CheckDataType","");
						imgChkDataType.ImageUrl = ImgPathFalse;
						imgChkDataType.Visible = true;
						break;
					}
				}

				if(dsInsert != null)
				{
					dsInsert.Dispose();
				}
			}
			catch(Exception ex)
			{
				txtException.Text = ex.Message;
				imgChkDataType.ImageUrl = ImgPathFalse;
				imgChkDataType.Visible = true;
			}
			
			
			return blReturn;
		}

		private bool CheckImportAndSaveSucces()
		{
			bool blReturn = false;

			try
			{
				DataSet dsInsert = (DataSet)ViewState["DS_Insert"];
				string xmlInsert = dsInsert.GetXml().Replace("\r\n","");
				string strReturn = ConsignmentNoteDAL.ImportCustomerFile(strAppID,strEnterpriseID,payerID,userID,"ND","",xmlInsert);

				if(dsInsert != null){dsInsert.Dispose();}

				if(strReturn.Trim().ToUpper().Equals("SUCCESS"))
				{
					blReturn = true;
				}
				else
				{
					txtException.Text = getErrorMsg(utility.GetUserCulture(),"CheckImportAndSaveSucces",strReturn);
					imgChkImportComplete.ImageUrl = ImgPathFalse;
					imgChkImportComplete.Visible = true;
				}
			}
			catch(Exception ex)
			{
				txtException.Text = ex.Message;
				imgChkImportComplete.ImageUrl = ImgPathFalse;
				imgChkImportComplete.Visible = true;
			}

			return blReturn;
		}


		private void Page_Load(object sender, System.EventArgs e)
		{
			try
			{
				utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings,Page.Session);
				strAppID = utility.GetAppID();
				strEnterpriseID = utility.GetEnterpriseID();
				userID = Session["userID"].ToString();	
				if(Session["PayerID"] != null && Session["PayerID"].ToString() != "")
				{
					payerID = Session["PayerID"].ToString();
				}
				else
				{
					payerID = "KYTDI";
				}
			}
			catch(ApplicationException appException)
			{
				strMsg = appException.Message.ToString();
				ErrorMsg.Text = strMsg;
			}
			
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnImport.Click += new System.EventHandler(this.btnImport_Click);
			this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

	

		private void btnImport_Click(object sender, System.EventArgs e)
		{
			if(CheckEmptyFile())
			{
				if (CheckExcelFormateFile()) 
				{
					if (uploadFiletoServer())
					{
						imgChkFormateFile.ImageUrl = ImgPathTrue;
						imgChkFormateFile.Visible = true;

						if(CheckRequriedData())
						{
							imgChkRequired.ImageUrl = ImgPathTrue;
							imgChkRequired.Visible = true;

							if(CheckDataType())
							{
								imgChkDataType.ImageUrl = ImgPathTrue;
								imgChkDataType.Visible = true;

								if(CheckImportAndSaveSucces())
								{
									imgChkImportComplete.ImageUrl = ImgPathTrue;
									imgChkImportComplete.Visible = true;

									if(utility.GetUserCulture().Equals("en-US")){ ErrorMsg.Text = "Data has been imported successfully.[" + ViewState["FileName"].ToString() +"]"; }
									if(utility.GetUserCulture().Equals("th-TH")){ ErrorMsg.Text = "����Ң���������� [" + ViewState["FileName"].ToString() +"]"; }
								}
							}
						}
					}
				}
			}
		}

		private void btnClear_Click(object sender, System.EventArgs e)
		{
			ErrorMsg.Text = "";
			txtException.Text = "";
			file_upload.Controls.Clear();   //PostedFile.FileName == ""
			imgChkFormateFile.ImageUrl = "";
			imgChkFormateFile.Visible = false;
			imgChkRequired.ImageUrl = "";
			imgChkRequired.Visible = false;
			imgChkDataType.ImageUrl = "";
			imgChkDataType.Visible = false;
			imgChkImportComplete.ImageUrl = "";
			imgChkImportComplete.Visible = false;

		}
	}
}
