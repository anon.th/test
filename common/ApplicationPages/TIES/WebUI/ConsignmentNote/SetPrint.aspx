<%@ Import Namespace="com.common.util" %>
<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<%@ Page language="c#" Codebehind="SetPrint.aspx.cs" AutoEventWireup="false" Inherits="com.ties.SetPrint" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Set Print</title>
		<meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK href="../css/Styles.css" type="text/css" rel="Stylesheet">
		<!--#INCLUDE FILE="../msFormValidations.inc"-->
		<script language="javascript" src="../Scripts/settingScrollPosition.js"></script>
		<script language="javascript" type="text/javascript">
			 function DoAction(serveraction) {
				frmSetPrint.elements("ServerAction").value = serveraction;
				frmSetPrint.submit();
			}
		</script>
	</HEAD>
	<body onkeypress="microsoftKeyPress()" onclick="GetScrollPosition();" onload="SetScrollPosition();"
		onunload="window.top.displayBanner.fnCloseAll(0);" MS_POSITIONING="ViewLayout">
		<form id="frmSetPrint" method="post" runat="server">
			<input id="ServerAction" type="hidden" name="ServerAction">
			<table cellSpacing="0" cellPadding="0" width="100%" border="0">
				<tr>
					<td>
						<table class="TextBody" cellSpacing="0" cellPadding="3" border="0">
							<tr>
								<td colSpan="6"><asp:label id="lblMainTitle" runat="server" CssClass="mainTitleSize" Width="600px">Set Print ConsignmentNote</asp:label></td>
							</tr>
							<tr class="tablepagingitem" style="HEIGHT: 5px">
								<td style="WIDTH: 200px" align="right">&nbsp;</td>
								<td style="WIDTH: 400px" align="left">&nbsp;</td>
							</tr>
							<tr>
								<td></td>
								<td align="left">
									<a onclick="javascript:DoAction('SAVE');"><input class="queryButton" id="btnSave" style="WIDTH: 60px; HEIGHT: 20px" type="button"
											value="Save" name="btnSave"></a>
								</td>
							</tr>
							<tr>
								<td colSpan="2"><asp:label id="ErrorMsg" runat="server" CssClass="errorMsgColor" Width="643px" Height="3px"></asp:label></td>
							</tr>
							<tr>
								<td colSpan="2">&nbsp;</td>
							</tr>
							<tr>
								<td align="right" vAlign="top">Page Select&nbsp;: &nbsp;</td>
								<td align="left">
									<asp:CheckBox id="chkP1" runat="server" Text="1. Shipper"></asp:CheckBox><BR>
									<asp:CheckBox id="chkP2" runat="server" Text="2. Data Entry"></asp:CheckBox><BR>
									<asp:CheckBox id="chkP3" runat="server" Text="3. HCR-1"></asp:CheckBox><BR>
									<asp:CheckBox id="chkP4" runat="server" Text="4. HCR-2"></asp:CheckBox><BR>
									<asp:CheckBox id="chkP5" runat="server" Text="5. Destination"></asp:CheckBox><BR>
									<asp:CheckBox id="chkP6" runat="server" Text="6. Consignee"></asp:CheckBox>
								</td>
							</tr>
							<tr>
								<td colSpan="2">&nbsp;</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
