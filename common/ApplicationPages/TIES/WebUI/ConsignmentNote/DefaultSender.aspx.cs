using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.common.util;
using com.common.applicationpages;
using com.ties.DAL;


namespace TIES.WebUI.ConsignmentNote
{
	/// <summary>
	/// Summary description for DefaultSender.
	/// </summary>
	public class DefaultSender : BasePage
	{
		protected System.Web.UI.WebControls.Button btnQuery;
		protected System.Web.UI.WebControls.Button btnExcute;
		protected System.Web.UI.WebControls.DataGrid dgResult;

		Utility utility = null;
		String appID = null;
		String enterpriseID = null;
		String userID = null;
		String payerID = null;

		protected System.Web.UI.WebControls.Button btnAdd;
		protected System.Web.UI.WebControls.Label lblMainTitle;
		protected System.Web.UI.WebControls.Label ErrorMsg;
		protected System.Web.UI.WebControls.Label lblNotFound;
		protected System.Web.UI.WebControls.TextBox txtTel;
		protected System.Web.UI.HtmlControls.HtmlInputText txtSndNa;
		protected System.Web.UI.WebControls.TextBox txtCustNa;
		protected System.Web.UI.WebControls.TextBox txtUserID;
		protected System.Web.UI.WebControls.TextBox txtUpdDate;
		protected System.Web.UI.WebControls.TextBox txtSenderName;
		string strMsg=null;
	
		private void Page_Load(object sender, System.EventArgs e)
		{
			String ServerAction = "";

			try
			{
				utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings,Page.Session);
				appID = utility.GetAppID();
				enterpriseID = utility.GetEnterpriseID();
				userID = utility.GetUserID();
				payerID = (string)Session["PayerID"];

				if (!Page.IsPostBack)
				{
					ServerAction = Request.QueryString["ServerAction"] + "";
				}
				else
				{
					ServerAction = Request.Form["ServerAction"] + ""; 
				}

				switch (ServerAction.ToUpper())
				{
					case "SEARCH":
						LoadData();
						break;
					case "CLEAR":
						ClearData();
						break;
					case "SAVE":
						SaveData();
						break;
				}
			}
			catch(ApplicationException appException)
			{
				strMsg = appException.Message.ToString();
				ErrorMsg.Text = strMsg;
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.dgResult.PageIndexChanged += new System.Web.UI.WebControls.DataGridPageChangedEventHandler(this.dgResult_PageIndexChanged);
			this.dgResult.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.dgResult_ItemDataBound);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
	
		private void LoadData()
		{
			DataSet DS = new DataSet(); 

			try
			{
				dgResult.CurrentPageIndex = 0;
				Session["SEARCH_CUSTOMER_SENDER"] = null;
				DS = ConsignmentNoteDAL.GetCustSender(appID, enterpriseID, payerID);   
				if (DS != null && DS.Tables.Count != 0)
				{
					txtUserID.Text = userID.ToUpper();
					txtCustNa.Text = payerID;

					DataSet dsCustSndUser = new DataSet();
					dsCustSndUser = ConsignmentNoteDAL.GetCustSenderByUser(appID, enterpriseID, payerID, userID);
					if (dsCustSndUser != null && dsCustSndUser.Tables.Count != 0 && dsCustSndUser.Tables[0].Rows.Count != 0)
					{
						DataRow dr = null;
						dr = dsCustSndUser.Tables[0].Rows[0];
						
						txtSenderName.Text = dr["snd_rec_name"].ToString();
						txtUpdDate.Text = String.Format("{0:dd/MM/yyyy HH:mm}", dr["upd_date"]); 
 
						ViewState["Snd_Rec_Name"] = dr["snd_rec_name"].ToString();
					}

					Session["SEARCH_CUSTOMER_SENDER"] = DS.Tables[0]; 
					BindDG("LoadData");
				}
			}
			catch(ApplicationException appException)
			{
				strMsg = appException.Message.ToString();
				ErrorMsg.Text = strMsg;
			}
		}

		private void BindDG(string strModeAction)
		{
			DataTable DT = new DataTable();

			try
			{
				DT = (DataTable)Session["SEARCH_CUSTOMER_SENDER"];
				if (DT != null && DT.Rows.Count > 0)
				{
					if (strModeAction == "LoadData") DT.Columns.Add("checked_value");

					int i=0;
					string strSndByUser = "";
					
					strSndByUser = (string)ViewState["Snd_Rec_Name"];

					while(i< DT.Rows.Count)
					{
						if (DT.Rows[i]["snd_rec_name"].ToString() == strSndByUser)
						{
							DT.Rows[i]["checked_value"] = "Checked";
						}
						else
						{
							DT.Rows[i]["checked_value"] = " ";
						}
						i++;
					}

					lblNotFound.Visible = false;
					dgResult.Visible = true;
  
					dgResult.DataSource = DT;
					dgResult.DataBind();  
				}
				else
				{
					lblNotFound.Visible  = true;
					dgResult.Visible =false;  
				}
			}
			catch(ApplicationException appException)
			{
				strMsg = appException.Message.ToString() ;
				ErrorMsg.Text = strMsg;
			}
		}

		private void ClearData()
		{
			try
			{
				txtUserID.Text = "";
				txtCustNa.Text = "";   
				txtSenderName.Text = "";
				txtUpdDate.Text = "";
				ErrorMsg.Text = "";
				dgResult.DataSource = null;
				dgResult.DataBind();
			}
			catch(ApplicationException appException)
			{
				strMsg = appException.Message.ToString() ;
				ErrorMsg.Text = strMsg;
			}
		}
		
		private void SaveData()
		{
			string strCustId = "";
			string strSndRecName = "";

			strCustId = (string)Session["PayerID"];
			strSndRecName = Request.Form["txtSndNa"];	
		
			if (strSndRecName == "")
			{
				ErrorMsg.Text = "Please choose Sender Name (New)";
			}
			else
			{			
				try
				{
					int iResult = 0;

					iResult = ConsignmentNoteDAL.SaveCustomerSndByUser(appID, enterpriseID, userID, strCustId, strSndRecName);

					if (iResult > 0)
					{
						LoadData();
						ErrorMsg.Text = "Save successfully";
					}
				}
				catch(ApplicationException appException)
				{
					strMsg = appException.Message.ToString();
					ErrorMsg.Text = strMsg;
				}
			}
		}

		private void dgResult_PageIndexChanged(object source, System.Web.UI.WebControls.DataGridPageChangedEventArgs e)
		{
			dgResult.CurrentPageIndex = e.NewPageIndex;
			BindDG(" ");
		}

		private void dgResult_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
		{
			if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
			{
				e.Item.Attributes["onClick"] = "javascript:SelectItem('" + DataBinder.Eval(e.Item.DataItem,"snd_rec_name") + "');";
				e.Item.Attributes.Add("onmouseover", "javascript:ShowBar(this);"); 
				e.Item.Attributes.Add("onmouseout", "javascript:HideBar(this);");
			}
		} 	
	
	}
}
