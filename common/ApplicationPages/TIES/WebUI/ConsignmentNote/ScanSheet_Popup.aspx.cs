using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using iTextSharp.text.xml;
using iTextSharp.text.pdf;
using iTextSharp.text;
using System.IO;
using System.Text; 
using System.Net;
using iTextSharp.text.html.simpleparser;
using TIES;
using TIESDAL;
using com.common.classes;
using com.common.RBAC;
using com.common.util;
using com.common.DAL;
using com.common.applicationpages;
using com.ties.DAL;

namespace TIES.WebUI.ConsignmentNote
{
	/// <summary>
	/// Summary description for ScanSheet_Popup.
	/// </summary>
	public class ScanSheet_Popup : System.Web.UI.Page
	{
		Utility utility = null;

		protected System.Web.UI.HtmlControls.HtmlTable tblData;
		protected System.Web.UI.HtmlControls.HtmlGenericControl div_export;
		protected System.Web.UI.HtmlControls.HtmlTable tblHeader;
		protected System.Web.UI.HtmlControls.HtmlTable tblImg;
		protected System.Web.UI.HtmlControls.HtmlGenericControl divHeader;
		protected System.Web.UI.WebControls.Image Image1;
	
		private void Page_Load(object sender, System.EventArgs e)
		{
			if(!IsPostBack)
			{
//				Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings,HttpContext.Current.Session);
//				
//				string enterpriseID = util.GetEnterpriseID();
//				ViewState["AppID"] = util.GetAppID();
				
				if(Request.QueryString["serviceType"] != null)
				{
					ViewState["ServiceType"] = Request.QueryString["serviceType"];
				}
				else
				{
					if(Session["pServiceType"] != null && Session["pServiceType"].ToString() != "")
					{	
						ViewState["ServiceType"] = Session["pServiceType"].ToString();
						Session["pServiceType"]="";
					}
					else
					{
						ViewState["ServiceType"] = "";
					}					
				}
					

				if(Request.QueryString["location"] != null)
				{
					ViewState["Location"] = Request.QueryString["location"];
				}
				else
				{
					if(Session["pLocation"] != null && Session["pLocation"].ToString() != "")
					{	
						ViewState["Location"] = Session["pLocation"].ToString();
						Session["pLocation"]="";
					}
					else
					{
						ViewState["Location"] = "";
					}		
				}

				if(Request.QueryString["batchDate"] != null)
				{
					ViewState["BatchDate"] = Request.QueryString["batchDate"];
				}
				else
				{
					if(Session["pBatchDate"] != null && Session["pBatchDate"].ToString() != "")
					{	
						ViewState["BatchDate"] = Session["pBatchDate"].ToString();
						 Session["pBatchDate"]="";
					}
					else
					{
						ViewState["BatchDate"] = "";
					}	
				}
//
//				ViewState["Location"] = "POM";
//				ViewState["BatchDate"] = "2013-02-18";
			

				GenHeaderTable();


				utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings,Page.Session);
				string appID = utility.GetAppID();
				string enterpriseID = utility.GetEnterpriseID();
				ViewState["AppID"] = appID;
				ViewState["EnterpriseName"] = utility.GetEnterpriseName(appID, enterpriseID);

				DataSet dsScanSheet = ConsignmentNoteDAL.GetScanSheet(appID, enterpriseID, ViewState["BatchDate"].ToString(), ViewState["Location"].ToString(), ViewState["ServiceType"].ToString());
				
				for(int i=dsScanSheet.Tables[0].Rows.Count-1;i>=0;i--)
				{
					if(dsScanSheet.Tables[0].Rows[i]["BatchNumber"] == System.DBNull.Value)
						dsScanSheet.Tables[0].Rows.RemoveAt(i);
					else if(dsScanSheet.Tables[0].Rows[i]["BatchNumber"].ToString() == "")
						dsScanSheet.Tables[0].Rows.RemoveAt(i);
				}

				GenDetailTable(dsScanSheet);
				
				CreatePDFDocument();
			}

		}

		private string GenHeaderTable()
		{
			string tblHeader =	"<table runat='server' cellpadding='0' cellspacing='0' border='0' height='200px'>" +
								@"<tr valign='top' ><td Align='right' valign='top'>" +
								"<table runat='server' cellpadding='0' cellspacing='0' border='0' width='100%'>" +
								"<tr><td Align='right'><span style='FONT-SIZE: 15px; FONT-FAMILY: Tahoma;'><IMG SRC='" + Request.PhysicalApplicationPath + "/WebUI/images/pngaf_logo.jpg' width='110px'></span></td></tr>" +
								"<tr><td Align='right'><span style='FONT-SIZE: 10px; FONT-FAMILY: Tahoma;'>&nbsp;</span></td></tr></table>" +
								"</td>" +
								"<td valign='middle'>" +
								"<table runat='server' cellpadding='0' cellspacing='0' border='0' width='100%'>" +
								"<tr><td Align='left'><span style='FONT-SIZE: 15px; FONT-FAMILY: Tahoma;'>" + ViewState["EnterpriseName"] + "</span></td></tr>" +
								"<tr><td Align='left'><span style='FONT-SIZE: 10px; FONT-FAMILY: Tahoma;'>Service Type: " + ViewState["ServiceType"] + "</span></td></tr>" +
								"<tr><td Align='left'><span style='FONT-SIZE: 10px; FONT-FAMILY: Tahoma;'>Location: " + ViewState["Location"] + "</span></td></tr>" +
								"<tr><td Align='left'><span style='FONT-SIZE: 10px; FONT-FAMILY: Tahoma;'>Batch Date: " +  ViewState["BatchDate"] + "</span></td></tr></table>" +
								"</td>" +
								"</tr></table>";

			return tblHeader;
			
//			HtmlTableRow tr1 = new HtmlTableRow();
//			tr1.ID = "tr" + "1"; 
//			///////////row1////////////////////
//			HtmlTableCell td11 = new HtmlTableCell();
//			td11.ID = "tdImage";
//			td11.InnerHtml = @"<IMG SRC='http://localhost/PRD/TIES/WebUI/images/pngaf_logo.jpg'>";
//			td11.Width = "100px";//HttpContext.Current.Server.MapPath("/PRD/TIES/WebUI/images/pngaf_logo.jpg")
//			td11.Align = "right";
//			td11.VAlign = "bottom";
//
//
//			//td11.RowSpan=4;
//
//			HtmlTableCell td12 = new HtmlTableCell();
//			td12.ID = "lblEnterpriseName";
//			td12.InnerHtml = EnterpriseName;
//			td12.Style.Add("FONT-WEIGHT","bold");
//			td12.Style.Add("FONT-SIZE","15px");
//
//			///////////row2////////////////////
//
//			HtmlTableCell td22 = new HtmlTableCell();
//			td22.ID = "lblServicceType";
//			td22.InnerHtml = "ServiceType :" + ServicceType;
//				
//			///////////row3////////////////////
//
//			HtmlTableCell td32 = new HtmlTableCell();
//			td32.ID = "lblLocation";
//			td32.InnerHtml = "Location :" + Location;
//
//			///////////row4////////////////////
//			HtmlTableCell td42 = new HtmlTableCell();
//			td42.ID = "lblBatchDate";
//			td42.InnerHtml = "Batch Date :" + BatchDate;
//			///////////////////////
//				
//				
//			HtmlTableRow trImg = new HtmlTableRow();
//			trImg.ID = "trImg"; 
//			trImg.Height = "50px";
//			trImg.VAlign = "middle";
//			trImg.Cells.Add(td11);
//				
//			tr1.ID = "tr" + "1"; 
//			tr1.Cells.Add(td12);
//
//				
//			HtmlTableRow tr2 = new HtmlTableRow();
//			tr2.ID = "tr" + "2"; 
//			tr2.Cells.Add(td22);
//
//			HtmlTableRow tr3 = new HtmlTableRow();
//			tr3.ID = "tr" + "3"; 
//			tr3.Cells.Add(td32);
//
//			HtmlTableRow tr4 = new HtmlTableRow();
//			tr4.ID = "tr" + "4";
//			tr4.Cells.Add(td42);
//
//
//			tblImg.Rows.Add(trImg);
//			tblHeader.Rows.Add(tr1);
//			tblHeader.Rows.Add(tr2);
//			tblHeader.Rows.Add(tr3);
//			tblHeader.Rows.Add(tr4);

		}
		
		private void GenDetailTable(DataSet dsScanSheet)
		{
			if(dsScanSheet != null)
			{
				try
				{
					for(int i=0;i<=((dsScanSheet.Tables[0].Rows.Count-1)/3);i++)
					{
						HtmlTableRow tr = new HtmlTableRow();
						HtmlTableCell td = new HtmlTableCell();
						
						if(i % 5 == 0 || i== 0)
						{
							td.ColSpan = 3;
							td.InnerHtml = GenHeaderTable();
							tr.Cells.Add(td);
							
							tblData.Rows.Add(tr);

							tr = new HtmlTableRow();
							td = new HtmlTableCell();
						}
							tr.ID = "tr" + i ;

							string BatchNumber = "";
							string ArrivalDC = "";
							string ServiceType = "";
							string AWB_Number = "";

							if(dsScanSheet.Tables[0].Rows.Count >= ((i*3)+1))
							{
								if(dsScanSheet.Tables[0].Rows[(i*3)]["BatchNumber"] != System.DBNull.Value)
									BatchNumber = "*$" + dsScanSheet.Tables[0].Rows[(i*3)]["BatchNumber"].ToString() + "*";
								else
									BatchNumber = " ";

								if(dsScanSheet.Tables[0].Rows[(i*3)]["ArrivalDC"] != System.DBNull.Value)
									ArrivalDC = dsScanSheet.Tables[0].Rows[(i*3)]["ArrivalDC"].ToString();
								else
									ArrivalDC = "";

								if(dsScanSheet.Tables[0].Rows[(i*3)]["ServiceType"] != System.DBNull.Value)
									ServiceType = dsScanSheet.Tables[0].Rows[i*3]["ServiceType"].ToString();
								else
									ServiceType = "";

								if(dsScanSheet.Tables[0].Rows[(i*3)]["AWB_Number"] != System.DBNull.Value)
									AWB_Number  = dsScanSheet.Tables[0].Rows[(i*3)]["AWB_Number"].ToString();
								else
									AWB_Number = "";

								td.InnerHtml = @"<table runat='server' cellpadding='0' cellspacing='0' border='0' height='200px'>" +
									"<tr><td ><span style='FONT-SIZE: 10px; FONT-FAMILY: Tahoma;'> &nbsp;</span></td></tr>" +
									"<tr><td ><span style='FONT-SIZE: 10px; FONT-FAMILY: Tahoma;'> &nbsp;</span></td></tr>" +
									"<tr><td VAlign='bottom' height='200px' Align='center'><span style='FONT-SIZE: 12px; FONT-FAMILY: IDAutomationHC39M;' class='IDAutomationHC39M'> " + BatchNumber + "</span></td></tr>" +
									"<tr><td ><span style='FONT-SIZE: 10px; FONT-FAMILY: Tahoma;'>DC: " + ArrivalDC + " Service: " + ServiceType + "</span></td></tr>" +
									"<tr><td ><span style='FONT-SIZE: 10px; FONT-FAMILY: Tahoma;'>AWB: " + AWB_Number + "</span></td></tr>" +
									"<tr><td ><span style='FONT-SIZE: 10px; FONT-FAMILY: Tahoma;'> &nbsp;</span></td></tr></table>" ;
								//td.Attributes.CssStyle;
							}
							else
							{
								td.InnerHtml = " ";
							}
							td.Width = "33.3%";
							td.Align = "Center";
							td.Height = "200px";


						
							HtmlTableCell td2 = new HtmlTableCell();
							if(dsScanSheet.Tables[0].Rows.Count >= ((i*3)+2))
							{
								if(dsScanSheet.Tables[0].Rows[(i*3)+1]["BatchNumber"] != System.DBNull.Value)
									BatchNumber = "*$" + dsScanSheet.Tables[0].Rows[(i*3)+1]["BatchNumber"].ToString() + "*";
								else
									BatchNumber = " ";

								if(dsScanSheet.Tables[0].Rows[(i*3)+1]["ArrivalDC"] != System.DBNull.Value)
									ArrivalDC = dsScanSheet.Tables[0].Rows[(i*3)+1]["ArrivalDC"].ToString();
								else
									ArrivalDC = "";

								if(dsScanSheet.Tables[0].Rows[(i*3)+1]["ServiceType"] != System.DBNull.Value)
									ServiceType = dsScanSheet.Tables[0].Rows[(i*3)+1]["ServiceType"].ToString();
								else
									ServiceType = "";

								if(dsScanSheet.Tables[0].Rows[(i*3)+1]["AWB_Number"] != System.DBNull.Value)
									AWB_Number  = dsScanSheet.Tables[0].Rows[(i*3)+1]["AWB_Number"].ToString();
								else
									AWB_Number = "";

								td2.InnerHtml = @"<table runat='server' cellpadding='0' cellspacing='0' border='0' height='200px'>" +
									"<tr><td ><span style='FONT-SIZE: 10px; FONT-FAMILY: Tahoma;'> &nbsp;</span></td></tr>" +
									"<tr><td ><span style='FONT-SIZE: 10px; FONT-FAMILY: Tahoma;'> &nbsp;</span></td></tr>" +
									"<tr><td VAlign='bottom' height='200px' Align='center'><span style='FONT-SIZE: 12px; FONT-FAMILY: IDAutomationHC39M;' class='IDAutomationHC39M'> " + BatchNumber + "</span></td></tr>" +
									"<tr><td ><span style='FONT-SIZE: 10px; FONT-FAMILY: Tahoma;'>DC: " + ArrivalDC + " Service: " + ServiceType + "</span></td></tr>" +
									"<tr><td ><span style='FONT-SIZE: 10px; FONT-FAMILY: Tahoma;'>AWB: " + AWB_Number + "</span></td></tr>" +
									"<tr><td ><span style='FONT-SIZE: 10px; FONT-FAMILY: Tahoma;'> &nbsp;</span></td></tr></table>" ;
							}
							else
							{
								td2.InnerHtml = " ";
							}
							td2.Width = "33.3%";
							td2.Align = "Center";
							td2.Height = "200px";
							//td2.Attributes.CssStyle = "Tahoma";


							HtmlTableCell td3 = new HtmlTableCell();
							if(dsScanSheet.Tables[0].Rows.Count >= ((i*3)+3))
							{
								if(dsScanSheet.Tables[0].Rows[(i*3)+2]["BatchNumber"] != System.DBNull.Value)
									BatchNumber = "*$" + dsScanSheet.Tables[0].Rows[(i*3)+2]["BatchNumber"].ToString() + "*";
								else
									BatchNumber = " ";

								if(dsScanSheet.Tables[0].Rows[(i*3)+2]["ArrivalDC"] != System.DBNull.Value)
									ArrivalDC = dsScanSheet.Tables[0].Rows[(i*3)+2]["ArrivalDC"].ToString();
								else
									ArrivalDC = "";

								if(dsScanSheet.Tables[0].Rows[(i*3)+2]["ServiceType"] != System.DBNull.Value)
									ServiceType = dsScanSheet.Tables[0].Rows[(i*3)+2]["ServiceType"].ToString();
								else
									ServiceType = "";

								if(dsScanSheet.Tables[0].Rows[(i*3)+2]["AWB_Number"] != System.DBNull.Value)
									AWB_Number  = dsScanSheet.Tables[0].Rows[(i*3)+2]["AWB_Number"].ToString();
								else
									AWB_Number = "";

								td3.InnerHtml = @"<table runat='server' cellpadding='0' cellspacing='0' border='0' height='200px'>" +
									"<tr><td ><span style='FONT-SIZE: 10px; FONT-FAMILY: Tahoma;'> &nbsp;</span></td></tr>" +
									"<tr><td ><span style='FONT-SIZE: 10px; FONT-FAMILY: Tahoma;'> &nbsp;</span></td></tr>" +
									"<tr><td VAlign='bottom' height='200px' Align='center'><span style='FONT-SIZE: 12px; FONT-FAMILY: IDAutomationHC39M;' class='IDAutomationHC39M'> " + BatchNumber + "</span></td></tr>" +
									"<tr><td ><span style='FONT-SIZE: 10px; FONT-FAMILY: Tahoma;'>DC: " + ArrivalDC + " Service: " + ServiceType + "</span></td></tr>" +
									"<tr><td ><span style='FONT-SIZE: 10px; FONT-FAMILY: Tahoma;'>AWB: " + AWB_Number + "</span></td></tr>" +
									"<tr><td ><span style='FONT-SIZE: 10px; FONT-FAMILY: Tahoma;'> &nbsp;</span></td></tr></table>" ;
							}
							else
							{
								td3.InnerHtml = " ";
							}
							td3.Width = "33.3%";
							td3.Align = "Center";
							td3.Height = "200px";

					
							tr.Cells.Add(td);
							tr.Cells.Add(td2);
							tr.Cells.Add(td3);

						tblData.Rows.Add(tr);
					}
				
				}
				catch(Exception ex)
				{
					string err = ex.Message;
				}
			}
		}


		public  void CreatePDFDocument()
		{
			
			// step 1: creation of a document-object
			Document document = new Document();
			try
			{
				iTextSharp.text.FontFactory.Register(@"c:\windows\fonts\IDAutomationHC39M.ttf", "IDAutomationHC39M");
				iTextSharp.text.FontFactory.Register(@"c:\windows\fonts\Tahoma.ttf", "Tahoma");

				StringBuilder sb = new StringBuilder();
				this.RenderControl(new HtmlTextWriter(new StringWriter(sb)));
				string strHtml = sb.ToString();
				//string s = sb.ToString();
				// step 2:
				// we create a writer that listens to the document
				
				string fileName = Request.PhysicalApplicationPath + @"WebUI\Export\" + "PDF_GEN_" + DateTime.UtcNow.Day.ToString() + DateTime.UtcNow.Month.ToString() + "_" + DateTime.UtcNow.Hour.ToString() + DateTime.UtcNow.Minute.ToString() + ".pdf"; //System.Configuration.ConfigurationSettings.AppSettings["InvoiceExportPath"]
//				PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(Request.PhysicalApplicationPath + "~1.pdf", FileMode.Create));
//							document.Open();
				PdfWriter pdfWriter = PdfWriter.GetInstance(document,new FileStream( fileName , FileMode.Create));

				//create an instance of your PDFpage class. This is the class we generated above.
				pdfPage page = new pdfPage();
				//set the PageEvent of the pdfWriter instance to the instance of our PDFPage class
				pdfWriter.PageEvent = page;

				StringReader se = new StringReader(strHtml);
				HTMLWorker obj = new HTMLWorker(document);
				document.Open();
				//document.HtmlStyleClass = HttpContext.Current.Server.MapPath("/PRD/TIES/WebUI/css/Styles.css");

				obj.Parse(se);
				document.Close();
				ShowPdf(fileName);
			}
			catch(Exception ex)
			{
				document.Close();
			}


		}

		public void ShowPdf(string strFileName)
		{
			
			Response.ClearContent();
			Response.ClearHeaders();
			Response.ContentType = "application/pdf";
			Response.AddHeader("Content-Disposition", String.Format("inline; filename={0}.pdf", Path.GetFileName(strFileName)));

			Response.WriteFile(strFileName);
			Response.Flush();
			Response.Close();
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
	}


	public class pdfPage : iTextSharp.text.pdf.PdfPageEventHelper 
	{
		PdfContentByte cb;
		// we will put the final number of pages in a template
		PdfTemplate template;
		// this is the BaseFont we are going to use for the header / footer
		BaseFont bf = null;

		//I create a font object to use within my footer
		protected iTextSharp.text.Font footer
		{
			get
			{
				// create a basecolor to use for the footer font, if needed.
				//BaseColor black = new BaseColor(0, 0, 0);
				iTextSharp.text.Font font = FontFactory.GetFont("Tahoma", 10, iTextSharp.text.Font.NORMAL, iTextSharp.text.Color.BLACK);
				return font;
			}
		}
		public override void OnOpenDocument(PdfWriter writer, Document document)
		{
			try
			{
				//PrintTime = DateTime.Now;
				bf = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
				cb = writer.DirectContent;
				template = cb.CreateTemplate(50, 50);
			}
			catch (DocumentException de)
			{
				string str = de.Message;
			}
			catch (System.IO.IOException ioe)
			{
				string str = ioe.Message;
			}
		}

		//override the OnStartPage event handler to add our header
//		public override void OnStartPage(PdfWriter writer, Document doc)
//		{
////			//I use a PdfPtable with 1 column to position my header where I want it
////			PdfPTable headerTbl = new PdfPTable(1);
//// 
////			//set the width of the table to be the same as the document
////			headerTbl.TotalWidth = doc.PageSize.Width;
//// 
////			//I use an image logo in the header so I need to get an instance of the image to be able to insert it. I believe this is something you couldn't do with older versions of iTextSharp
////			iTextSharp.text.Image logo = iTextSharp.text.Image.GetInstance(HttpContext.Current.Server.MapPath("/PRD/TIES/WebUI/images/pngaf_logo.jpg"));
//// 
////			//I used a large version of the logo to maintain the quality when the size was reduced. I guess you could reduce the size manually and use a smaller version, but I used iTextSharp to reduce the scale. As you can see, I reduced it down to 7% of original size.
////			logo.ScalePercent(100);
//// 
////			//create instance of a table cell to contain the logo
////			PdfPCell cell = new PdfPCell(logo);
//// 
////			//align the logo to the right of the cell
////			cell.HorizontalAlignment = Element.ALIGN_LEFT;
//// 
////			//add a bit of padding to bring it away from the right edge
////			cell.PaddingLeft = 100;
//// 
////			//remove the border
////			cell.Border = 0;
//// 
////			//Add the cell to the table
////			headerTbl.AddCell(cell);
//// 
////			//write the rows out to the PDF output stream. I use the height of the document to position the table. Positioning seems quite strange in iTextSharp and caused me the biggest headache.. It almost seems like it starts from the bottom of the page and works up to the top, so you may ned to play around with this.
////			headerTbl.WriteSelectedRows(0,-1, 0, (doc.PageSize.Height-10), writer.DirectContent);
////
////
////			//////////////////////////////////////////////////////////
////			
////			//I use a PdfPtable with 1 column to position my header where I want it
////			PdfPTable headerTbl2 = new PdfPTable(1);
////			
////			//set the width of the table to be the same as the document
////			headerTbl2.TotalWidth = doc.PageSize.Width;
//// 
////			//Create a paragraph that contains the footer text
////			Paragraph para = new Paragraph("PNG", footer);//Session["EnterpriseName"]
//// 
////			//add a carriage return
////			para.Add(Environment.NewLine);
////			para.Add("Servicce Type: " );//+ Session["ServicceType"]);
////			para.Add(Environment.NewLine);
////			para.Add("Location: " );//+ Session["Location"]);
////			para.Add(Environment.NewLine);
////			para.Add("Batch Date: " );//+ Session["BatchDate"]);
//// 
////			//create a cell instance to hold the text
////			PdfPCell cell2 = new PdfPCell(para);
//// 
////			//set cell border to 0
////			cell2.Border = 0;
//// 
////			cell2.HorizontalAlignment = Element.ALIGN_LEFT;
////			//set border to 0
////			cell2.Border = 0;
//// 
////			// add some padding to take away from the edge of the page
////			cell2.PaddingLeft = 200;
////
////			//Add the cell to the table
////			headerTbl2.AddCell(cell2);
//// 
////			//write the rows out to the PDF output stream. I use the height of the document to position the table. Positioning seems quite strange in iTextSharp and caused me the biggest headache.. It almost seems like it starts from the bottom of the page and works up to the top, so you may ned to play around with this.
////			headerTbl2.WriteSelectedRows(0,-1, 0, (doc.PageSize.Height-15), writer.DirectContent);
////			
////			
//
//			////////////////////////////////////////////////
//			
//
////			base.OnStartPage(writer, document);
////
////			Rectangle pageSize = document.PageSize;
////
////			if (Title != string.Empty)
////			{
////				cb.BeginText();
////				cb.SetFontAndSize(bf, 15);
////				cb.SetRGBColorFill(50, 50, 200);
////				cb.SetTextMatrix(pageSize.GetLeft(30), pageSize.GetTop(20));
////				cb.ShowText(Title);
////				cb.EndText();
////			}
////
////			if (HeaderLeft + HeaderRight != string.Empty)
////			{
////				PdfPTable HeaderTable = new PdfPTable(2);
////				HeaderTable.DefaultCell.VerticalAlignment = Element.ALIGN_MIDDLE;
////				HeaderTable.TotalWidth = pageSize.Width - 80;
////				HeaderTable.SetWidthPercentage(new float[] { 45, 45 }, pageSize);
////
////				PdfPCell HeaderLeftCell = new PdfPCell(new Phrase(8, HeaderLeft, HeaderFont));
////				HeaderLeftCell.Padding = 5;
////				HeaderLeftCell.PaddingBottom = 8;
////				HeaderLeftCell.BorderWidthRight = 0;
////				HeaderTable.AddCell(HeaderLeftCell);
////
////				PdfPCell HeaderRightCell = new PdfPCell(new Phrase(8, HeaderRight, HeaderFont));
////				HeaderRightCell.HorizontalAlignment = PdfPCell.ALIGN_RIGHT;
////				HeaderRightCell.Padding = 5;
////				HeaderRightCell.PaddingBottom = 8;
////				HeaderRightCell.BorderWidthLeft = 0;
////				HeaderTable.AddCell(HeaderRightCell);
////
////				cb.SetRGBColorFill(0, 0, 0);
////				HeaderTable.WriteSelectedRows(0, -1, pageSize.GetLeft(30), pageSize.GetTop(20), cb);
////			}
//		}
 
		//override the OnPageEnd event handler to add our footer
		public override void OnEndPage(PdfWriter writer, Document doc)
		{
			base.OnEndPage(writer, doc);

			int pageN = writer.PageNumber;
			String text = "Page " + pageN + " of  ";
			float text_width = bf.GetWidthPoint(text, 10);

			iTextSharp.text.Rectangle pageSize = doc.PageSize;

			cb.SetRGBColorFill(0, 0, 0);

			cb.BeginText();
			cb.SetFontAndSize(bf, 10);
			cb.ShowTextAligned(PdfContentByte.ALIGN_RIGHT,
				text,
				pageSize.GetRight(30),
				pageSize.GetBottom(20), 0);
			cb.EndText();

			cb.AddTemplate(template, pageSize.GetRight(30), pageSize.GetBottom(20));
		}

		public override void OnCloseDocument(PdfWriter writer, Document document)
		{
			base.OnCloseDocument(writer, document);

			template.BeginText();
			template.SetFontAndSize(bf, 10);
			template.SetTextMatrix(0, 0);
			template.ShowText("" + (writer.PageNumber - 1));
			template.EndText();
		}
	}
}
