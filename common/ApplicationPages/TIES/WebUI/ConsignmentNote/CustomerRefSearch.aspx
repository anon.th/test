<%@ Page language="c#" Codebehind="CustomerRefSearch.aspx.cs" AutoEventWireup="false" Inherits="com.ties.CustomerRefSearch" %>
<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<%@ Import Namespace="com.common.util" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>SearchCustomer</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../css/Styles.css" type="text/css" rel="Stylesheet">
		<!--#INCLUDE FILE="../msFormValidations.inc"-->
		<script language="javascript" src="../Scripts/settingScrollPosition.js"></script>
		<script language="javascript" type="text/javascript">
			function DoAction(serveraction) {
				document.forms[0].elements("ServerAction").value = serveraction;
                document.forms[0].submit(); 
			}
				
			function DoInsert() {
				window.location.href = "CustomerRef.aspx?ServerAction=INSERT";
			}
				
			function SelectItem(CustomerCode){
				window.location.href = "../ConsignmentNote/CustomerRef.aspx?CustomerCode=" + CustomerCode; 
			}
			
			var old_bgcolor;
			var old_class;
			function ShowBar(src) {
				if (!src.contains(event.fromElement)) {
					src.style.cursor = 'hand';
					old_class = src.className;
					src.className = 'tabletotal';
				}
			}
			function HideBar(src) {
				if (!src.contains(event.toElement)) {
					src.style.cursor = 'default';
					src.className = old_class;
				}
			}
		</script>
	</HEAD>
	<body onkeypress="microsoftKeyPress()" onclick="GetScrollPosition();" onload="SetScrollPosition();"
		onunload="window.top.displayBanner.fnCloseAll(0);" MS_POSITIONING="ViewLayout">
		<form id="frmCustomerRefSearch" method="post" runat="server">
			<input id="ServerAction" type="hidden" name="ServerAction">
			<table cellSpacing="0" cellPadding="0" width="100%" border="0">
				<tr>
					<td>
						<table class="TextBody" cellSpacing="0" cellPadding="3" border="0" width="100%">
							<tr>
								<td colSpan="4"><asp:label id="lblMainTitle" runat="server" Width="477px" CssClass="mainTitleSize">Search Consignee</asp:label></td>
							</tr>
							<tr class="tablepagingitem" style="HEIGHT: 5px">
								<td style="WIDTH: 250px" align="right">&nbsp;</td>
								<td style="WIDTH: 150px" align="left">&nbsp;</td>
								<td style="WIDTH: 100px" align="left">&nbsp;</td>
								<td style="WIDTH: 200px" align="right">&nbsp;</td>
							</tr>
							<tr>
								<td></td>
								<td colSpan="4" align="left">
									<a onclick="javascript:DoAction('CLEAR');"><input class="queryButton" id="btnQuery" style="WIDTH: 60px; HEIGHT: 20px" type="button"
											value="Query" name="btnQuery"></a> &nbsp; <a onclick="javascript:DoAction('SEARCH');">
										<input class="queryButton" id="btnExcute" style="WIDTH: 104px; HEIGHT: 20px" type="button"
											value="Execute Query" name="btnExcute"></a>&nbsp; <a onclick="javascript:DoInsert();">
										<input class="queryButton" id="btnInsert" style="WIDTH: 60px; HEIGHT: 20px" type="button"
											value="Insert" name="btnInsert"></a>&nbsp;
								</td>
							</tr>
							<tr class="tablepagingitem" style="HEIGHT: 5px">
								<td colSpan="4">&nbsp;</td>
							</tr>
							<tr>
								<td colSpan="4"><asp:label id="ErrorMsg" runat="server" Width="643px" CssClass="errorMsgColor" Height="3px"></asp:label></td>
							</tr>
							<tr>
								<td colSpan="4">&nbsp;</td>
							</tr>
							<tr>
								<td align="right">Consignee&nbsp;Code&nbsp;: &nbsp;</td>
								<td align="left"><asp:textbox id="txtCustomerCode" runat="server"></asp:textbox></td>
							</tr>
							<tr>
								<td align="right">Telephone&nbsp;: &nbsp;</td>
								<td align="left"><asp:textbox id="txtTelephone" runat="server" Width="160px"></asp:textbox></td>
								<td align="right">
									&nbsp;&nbsp;</td>
								<td align="left"></td>
							</tr>
							<tr>
								<td align="right">Company&nbsp;Name&nbsp;: &nbsp;</td>
								<td align="left"><asp:textbox id="txtCompanyName" runat="server" Width="200px"></asp:textbox></td>
								<td align="right">
									&nbsp;&nbsp;</td>
								<td align="left"></td>
							</tr>
							<tr>
								<td colSpan="4">&nbsp;</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td align="center"><asp:datagrid id="dgResult" runat="server" AlternatingItemStyle-CssClass="gridField" ItemStyle-CssClass="gridField"
							HeaderStyle-CssClass="gridHeading" HorizontalAlign="Center" CellPadding="2" CellSpacing="1" BorderWidth="0px"
							PageSize="50" AutoGenerateColumns="False" AllowPaging="True" Width="1000px">
							<AlternatingItemStyle CssClass="gridField"></AlternatingItemStyle>
							<ItemStyle CssClass="gridField"></ItemStyle>
							<HeaderStyle Font-Bold="True" HorizontalAlign="Center" CssClass="gridHeading"></HeaderStyle>
							<Columns>
								<asp:TemplateColumn HeaderText="No.">
									<HeaderStyle Width="25px"></HeaderStyle>
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
									<ItemTemplate>
										<%# (dgResult.PageSize*dgResult.CurrentPageIndex)+Container.ItemIndex+1 %>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:BoundColumn DataField="CUSTOMERCODE" HeaderText="Code">
									<HeaderStyle Width="60px"></HeaderStyle>
									<ItemStyle HorizontalAlign="Left"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="REFERENCE_NAME" HeaderText="Consignee Name">
									<HeaderStyle Width="200px"></HeaderStyle>
									<ItemStyle HorizontalAlign="Left"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="ADDRESS1" HeaderText="Address1">
									<HeaderStyle Width="250px"></HeaderStyle>
									<ItemStyle HorizontalAlign="Left"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="ADDRESS2" HeaderText="Address2">
									<HeaderStyle Width="200px"></HeaderStyle>
									<ItemStyle HorizontalAlign="Left"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="STATE_NAME" HeaderText="State">
									<HeaderStyle Width="50px"></HeaderStyle>
									<ItemStyle HorizontalAlign="Left"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="ZIPCODE" HeaderText="Zipcode">
									<HeaderStyle Width="30px"></HeaderStyle>
									<ItemStyle HorizontalAlign="Left"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="TELEPHONE" HeaderText="Telephone">
									<HeaderStyle Width="30px"></HeaderStyle>
									<ItemStyle HorizontalAlign="Left"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="CONTACTPERSON" HeaderText="Contact Person">
									<HeaderStyle Width="150px"></HeaderStyle>
									<ItemStyle HorizontalAlign="Left"></ItemStyle>
								</asp:BoundColumn>
							</Columns>
							<PagerStyle NextPageText="Next" Font-Size="Smaller" PrevPageText="Previous" HorizontalAlign="Right"></PagerStyle>
						</asp:datagrid><asp:label id="lblNotFound" runat="server" Visible="False" ForeColor="Red" Font-Bold="True">Data not found.</asp:label></td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
