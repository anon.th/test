using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;  
using System.Drawing;
using System.Text;
using System.Configuration;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Web.Security;  
using System.Globalization; 
using com.common.classes;
using com.common.RBAC;
using com.common.util;
using com.common.DAL;
using com.common.applicationpages;
using com.ties.DAL;
using com.ties.classes;
using com.ties.BAL;  
using Cambro.Web.DbCombo;
using TIES;
using TIESDAL;

namespace com.ties //TIES.WebUI.ConsignmentNote
{
	/// <summary>
	/// Summary description for ConsignmentNote.
	/// </summary>
	public class ConsignmentNote : System.Web.UI.Page // BasePage
	{
		protected System.Web.UI.WebControls.Label ErrorMsg;
		protected System.Web.UI.WebControls.Label lblConsignmentNo;
		protected System.Web.UI.WebControls.TextBox txtCompanyName;
		protected System.Web.UI.WebControls.TextBox txtCompanyTel;
		protected System.Web.UI.WebControls.TextBox txtCompanyAddress;
		protected System.Web.UI.WebControls.TextBox txtCustomerRef;
		protected System.Web.UI.WebControls.TextBox txtRecipientName;
		protected System.Web.UI.WebControls.DropDownList lstServiceType;
		protected System.Web.UI.WebControls.CheckBox chkHC;
		protected System.Web.UI.WebControls.CheckBox chkInvoice;
		protected com.common.util.msTextBox txtCOD;
		protected com.common.util.msTextBox txtDeclared;
		protected System.Web.UI.WebControls.TextBox txtInstruction;
		protected System.Web.UI.HtmlControls.HtmlImage imgSearch;

		Utility utility = null;
		String strAppID = null;
		String strEnterpriseID = null;
		String PayerID = null;
		String userID = null;
		public String ConsignmentNo = "";
		public String ItemIndex = "";
		public String CompanyCode = "";
		public String Zipcode = "";
		public String strMsg = "";
		public String strMsg1 = "";
		protected System.Web.UI.WebControls.Label ErrorMsg1;
		protected System.Web.UI.WebControls.Label lblCompanyCode;
		protected System.Web.UI.WebControls.TextBox txtCustomerZone;
		protected System.Web.UI.WebControls.Label lblMainTitle;
		protected System.Web.UI.WebControls.DropDownList ddlSender;
		protected System.Web.UI.WebControls.TextBox txtContactPerson;
		protected System.Web.UI.WebControls.TextBox txtSenderTel;
		protected System.Web.UI.WebControls.TextBox txtAddr;
		protected System.Web.UI.WebControls.TextBox txtPostalCode;
		protected System.Web.UI.WebControls.TextBox txtState;
		protected System.Web.UI.WebControls.DataGrid dgPackage;
		protected ArrayList userRoleArray;
		
			
		private void Page_Load(object sender, System.EventArgs e)
		{
			String ServerAction = "";
	
			try
			{
				utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings,Page.Session);
				strAppID = Session["applicationID"] + ""; 
				strEnterpriseID = Session["enterpriseID"] + ""; 
				userID = Session["userID"] + ""; 
				PayerID = Session["PayerID"] + "";
				string hcReturn = null;
				string invoiceReturn = null;

				if (!Page.IsPostBack)
				{
					ServerAction = Request.QueryString["ServerAction"] + "";
					ConsignmentNo = Request.QueryString["ConsignmentNo"] + ""; 
					ItemIndex = Request.QueryString["ItemIndex"] + "";
					DataSet dsCust = ConsignmentNoteDAL.GetInitConData(strAppID, strEnterpriseID, PayerID);
					DataSet dsCustSender = ConsignmentNoteDAL.GetCustSender(strAppID, strEnterpriseID, PayerID);
					DataSet dsCustSndByUser = ConsignmentNoteDAL.GetCustSenderByUser(strAppID, strEnterpriseID, PayerID, userID);
					
					//Set Initial data of Customer
					if (dsCust.Tables[0] != null && dsCust.Tables[0].Rows.Count > 0)
					{
						DataRow drCust = dsCust.Tables[0].Rows[0];
						double tmpMaxAmt = (drCust["insurance_maximum_amt"].ToString() == "")? 0 : double.Parse(drCust["insurance_maximum_amt"].ToString());
						if (tmpMaxAmt > 0){txtDeclared.NumberMaxValue = Convert.ToInt32(tmpMaxAmt.ToString());}
						invoiceReturn = drCust["hc_invoice_required"].ToString();
						hcReturn = drCust["pod_slip_required"].ToString();
						ViewState["MaxAmt"] = tmpMaxAmt;

						//boon 21/10/2010
						//add session for "SpecialInstruction" that is query from Customer Profile
						Session.Add("SpecialInstruction", drCust["remark2"].ToString());


						//Get Role >> PREPRINTSTAFF
						string strRole = "";
						strRole = getRole();

						if (strRole == "PREPRINTSTAFF") 
						{
							ddlSender.Enabled = true;
							ddlSender.CssClass = "";
						}
						else 
						{
							ddlSender.Enabled = false;
							ddlSender.CssClass = "txtReadOnly";
						}
					}

					//Set Initial value for DropDownList
					if (dsCustSender.Tables[0] != null && dsCustSender.Tables[0].Rows.Count > 0)
					{
						ddlSender.DataSource = dsCustSender.Tables[0];
						ddlSender.DataTextField =dsCustSender.Tables[0].Columns["snd_rec_name"].ColumnName.ToString();
						ddlSender.DataValueField =dsCustSender.Tables[0].Columns["snd_rec_name"].ColumnName.ToString();
						ddlSender.DataBind () ;

						if (dsCustSndByUser.Tables.Count > 0 && dsCustSndByUser.Tables[0].Rows.Count > 0)
						{
							DataRow drCustSndUser = dsCustSndByUser.Tables[0].Rows[0];
							string strSndRecName = drCustSndUser["snd_rec_name"].ToString().Trim();
							ddlSender.SelectedValue = strSndRecName;

							ViewState["Snd_Rec_Name"] = strSndRecName;
						}
						else
						{
							ViewState["Snd_Rec_Name"] = "";
							ddlSender.SelectedIndex = 0;
						}

						ddlSenderSelectedValue();
					}

					//Set Server Action Mode
					if (ConsignmentNo != "")
					{
						ServerAction = "LOAD";
					}
					else
					{
						ServerAction = "INSERT";
						if(hcReturn == "Y")
							chkHC.Checked = true;
						else
							chkHC.Checked = false;

						if(invoiceReturn == "Y")
							chkInvoice.Checked = true;
						else
							chkInvoice.Checked = false;
					}

					dsCust.Dispose();
					dsCustSender.Dispose();
					dsCustSndByUser.Dispose();
				}
				else
				{
					ServerAction = Request.Form["ServerAction"] + ""; 
					ConsignmentNo = Request.Form["ConsignmentNo"] + "";
					ItemIndex = Request.Form["ItemIndex"] + "";
					CompanyCode = Request.Form["CompanyCode"] + "";
					Zipcode = Request.Form["Zipcode"] + ""; 
					if (Convert.ToInt32(ViewState["MaxAmt"].ToString()) > 0)
					{
						txtDeclared.NumberMaxValue = Convert.ToInt32(ViewState["MaxAmt"].ToString());
					}
				}

				switch (ServerAction.ToUpper())
				{
					case "LOAD":
						LoadData();
						break;

					case "INSERT":
						ClearData();
						if (ConsignmentNo == "")
						{
							if(hcReturn == "Y")
								chkHC.Checked = true;
							else
								chkHC.Checked = false;

							if(invoiceReturn == "Y")
								chkInvoice.Checked = true;
							else
								chkInvoice.Checked = false;
						}					
						break;

					case "DELETE":
						DeleteData();
						break;

					case "SAVE":
						SaveData();
						break;

					case "FINDCOMPANY":
						InitDataControl();
						//						SetService();
						break;
				}

			}
			catch(ApplicationException appException)
			{
				strMsg = appException.Message.ToString();
				ErrorMsg.Text = strMsg;
			}
		}



		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.ddlSender.SelectedIndexChanged += new System.EventHandler(this.ddlSender_SelectedIndexChanged);
			this.dgPackage.ItemCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dgPackage_ItemCommand);
			this.dgPackage.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.dgPackage_ItemDataBound);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private void InitDataControl()
		{
			LoadServiceTypeCombo();
//			if(Session["DefaultServiceType"]!= null)
//				lstServiceType.SelectedValue = Session["DefaultServiceType"] + "";
			if(Session["SpecialInstruction"]!= null)
				txtInstruction.Text = Session["SpecialInstruction"] + "";
//			if(Session["SenderContactName"]!= null)
//				txtSenderName.Text = Session["SenderContactName"] + "";
//			if(Session["SenderTelephone"]!= null)
//				txtSenderTel.Text = Session["SenderTelephone"] + "";

		}

		private void LoadServiceTypeCombo()
		{			
			//lstServiceType
//			ListItem defItem = new ListItem();
//			defItem.Text = "";
//			defItem.Value = "0";
//			lstServiceType.Items.Add(defItem);
			String senderZipcode = null;
			lstServiceType.Items.Clear();

			/*  boon mark 2010/10/09 - start
			DataSet ds = ConsignmentNoteDAL.SearchCustomerCode(strAppID, strEnterpriseID, PayerID, PayerID);
			if(ds.Tables[0].Rows.Count > 0)
			{
				foreach(DataRow dr in ds.Tables[0].Rows)
				{
					senderZipcode = dr["zipcode"].ToString().Trim();
				}
			}			
			  boon mark 2010/10/09 - end */

			senderZipcode = txtPostalCode.Text;


//			ArrayList ServiceType = Utility.GetListConsServiceType(strAppID, strEnterpriseID, "");
			ArrayList ServiceType = GetServiceValues(strAppID, strEnterpriseID, Zipcode, senderZipcode);
			foreach(SystemCode sysCode in ServiceType)
			{
				ListItem lstItem = new ListItem();
				lstItem.Text = sysCode.Text;
				lstItem.Value = sysCode.StringValue;
				lstServiceType.Items.Add(lstItem);
			}
		}

		private ArrayList GetServiceValues(String appID, String enterpriseID, String strDestZipCode, String strSendZipCode)
		{
			ArrayList codeValues = null;

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(appID);

			if(dbCon == null)
			{
				Logger.LogTraceError("Utility","GetServiceValues","EDB101","DbConnection object is null!!");
				System.Diagnostics.Trace.WriteLineIf(Logger.AppSwitch.TraceError,"[Utility::GetCodeValues()] DbConnection is null","ERROR");
				return codeValues;
			}
			
			StringBuilder strQry = new StringBuilder();
			if(strDestZipCode != "")
			{
				//				StringBuilder strQry = new StringBuilder();
				DataSet tmpDC = DomesticShipmentMgrDAL.getDCToOrigStation(appID, enterpriseID,
					0, 0, strSendZipCode.Trim()).ds;

				String strOriginDC = tmpDC.Tables[0].Rows[0]["origin_station"].ToString();

				tmpDC.Dispose();
				tmpDC = null;

				strQry.Append(" Select 0 as BSA,applicationid,enterpriseid,service_code, ISNULL(Service.transit_day * 24, 0) + ISNULL(Service.transit_hour, 0) AS transit_time");
				strQry.Append(",service_description,service_charge_percent,service_charge_amt, convert(varchar(5),Service.commit_time,108) as commited_time, transit_day from Service");
				strQry.Append(" Where service_code in (select service_code from Best_Service_Available");
				strQry.Append(" where origin_dc = '"+Utility.ReplaceSingleQuote(strOriginDC)+"'");
				strQry.Append(" and recipientZipCode = '"+Utility.ReplaceSingleQuote(strDestZipCode)+"')");
				strQry.Append(" Union");
				strQry.Append(" select 1 as BSA,* from (Select applicationid,enterpriseid,service_code, ISNULL(Service.transit_day * 24, 0) + ISNULL(Service.transit_hour, 0) AS transit_time");
				strQry.Append(",service_description,service_charge_percent,service_charge_amt, convert(varchar(5),Service.commit_time,108) as commited_time, transit_day from Service) SR");
				strQry.Append(" Where (((SR.commited_time >= (select convert(varchar(5),Service.commit_time,108) as commited_time");
				strQry.Append(" from Service where service_code in (select service_code from Best_Service_Available");
				strQry.Append(" where origin_dc = '"+Utility.ReplaceSingleQuote(strOriginDC)+"'");
				strQry.Append(" and recipientZipCode = '"+Utility.ReplaceSingleQuote(strDestZipCode)+"')))");
				strQry.Append(" AND (SR.transit_time = (select ISNULL(Service.transit_day * 24, 0) + ISNULL(Service.transit_hour, 0) AS transit_time");
				strQry.Append(" from Service where service_code in (select service_code from Best_Service_Available");
				strQry.Append(" where origin_dc = '"+Utility.ReplaceSingleQuote(strOriginDC)+"'");
				strQry.Append(" and recipientZipCode = '"+Utility.ReplaceSingleQuote(strDestZipCode)+"'))))");
				strQry.Append(" or (SR.transit_time > (select ISNULL(Service.transit_day * 24, 0) + ISNULL(Service.transit_hour, 0) AS transit_time");
				strQry.Append(" from Service where service_code in (select service_code from Best_Service_Available");
				strQry.Append(" where origin_dc = '"+Utility.ReplaceSingleQuote(strOriginDC)+"'");
				strQry.Append(" and recipientZipCode = '"+Utility.ReplaceSingleQuote(strDestZipCode)+"')))) ");
				strQry.Append(" and SR.service_code not in (select service_code from Best_Service_Available");
				strQry.Append(" where origin_dc = '"+Utility.ReplaceSingleQuote(strOriginDC)+"'");
				strQry.Append(" and recipientZipCode = '"+Utility.ReplaceSingleQuote(strDestZipCode)+"')");
				strQry.Append(" AND SR.applicationid = '"+appID+"' and SR.enterpriseid = '"+enterpriseID+"'");
				strQry.Append(" AND SR.transit_day < 5 ");
				//edit by Tumz.
				strQry.Append(" ORDER BY transit_time");
			}
			else
			{
				strQry.Append("select distinct(service_code),ISNULL(Service.transit_day * 24, 0) + ISNULL(Service.transit_hour, 0) AS transit_time from Service");
//				strQry.Append(" where applicationid = '"+appID+"' and enterpriseid = '"+enterpriseID+"' Order By transit_time");
				strQry.Append(" where transit_day < 5");
				strQry.Append(" and applicationid = '"+appID+"' and enterpriseid = '"+enterpriseID+"' Order By transit_time");
			}
			
			DataSet dsServiceCode = (DataSet)dbCon.ExecuteQuery(strQry.ToString(),ReturnType.DataSetType);

			int cnt = dsServiceCode.Tables[0].Rows.Count;

			if(cnt > 0)
			{
				codeValues = new ArrayList();
				
				for(int i=0; i < cnt; i++)
				{
					DataRow drEach = dsServiceCode.Tables[0].Rows[i];
					SystemCode systemCode = new SystemCode();

					if(!drEach["service_code"].GetType().Equals(System.Type.GetType("System.DBNull")))
					{
						systemCode.Text = (String) drEach["service_code"];
						systemCode.StringValue = (String) drEach["service_code"];
					}
					codeValues.Add(systemCode);
				}
			}

			dsServiceCode.Dispose();

			return  codeValues;
		}

		private void SetService()
		{
			String BestService = "";
			String UserLocation = "";
			DataSet dsBest = new DataSet();
			DataTable dtBest = new DataTable();
			DataRow drBest = null;
			DataSet DS = new DataSet(); 
			DataTable DT = new DataTable(); 
 
			try
			{
				if (Zipcode != "")
				{
					//Get BestService 
					UserLocation = Session["UserLocation"] + "";
					dsBest = ConsignmentNoteDAL.SearchBestService(strAppID, strEnterpriseID, UserLocation, Zipcode);
					if ((dsBest != null) && (dsBest.Tables.Count > 0) && (dsBest.Tables[0] != null))
					{
						dtBest = dsBest.Tables[0];
						if (dtBest.Rows.Count > 0)
						{
							drBest = dtBest.Rows[0];
							BestService = drBest["service_code"] + "";
						}
						else
						{
							BestService = "";
						}
					}
				}

				//Get posible ServiceCode
				DS = ConsignmentNoteDAL.GetPossibleService(strAppID, strEnterpriseID, BestService);
				if ((DS != null) && (DS.Tables.Count > 0))
				{
					DT = DS.Tables[0];
					if ((DT != null) && (DT.Rows.Count > 0))
					{
						lstServiceType.DataSource = DT;
						lstServiceType.DataTextField = "service_code";
						lstServiceType.DataValueField = "service_code";
						lstServiceType.DataBind(); 
					}
				}

				//Add new item 
				ListItem BlankItem = new ListItem("","");
				lstServiceType.Items.Insert(0, BlankItem);  

				// Set BestServiceCode
				lstServiceType.SelectedValue = BestService;
			}
			catch(ApplicationException appException)
			{
				strMsg = appException.Message.ToString();
				ErrorMsg.Text = strMsg;
			}
			finally
			{
				dsBest.Dispose();
				dtBest.Dispose();
				DS.Dispose();
				DT.Dispose();
			}
		}

		/*private void SetAllServiceCode()
		{
			DataSet DS = new DataSet();
			DataTable DT = new DataTable();
 
			try
			{
				DS = ConsignmentNoteDAL.GetPossibleService(strAppID, strEnterpriseID, "");
				if ((DS != null) && (DS.Tables.Count > 0) && (DS.Tables[0] != null))
				{
					DT = DS.Tables[0]; 
				}
			}
			catch(ApplicationException appException)
			{
				strMsg = appException.Message.ToString();
				ErrorMsg.Text = strMsg;
			}
		}*/

		private void LoadData()
		{
			DataSet dsCon = new DataSet();
			DataTable dtCon = new DataTable(); 
			DataRow drCon = null;
			DataSet dsParam = new DataSet();
			DataTable dtParam = new DataTable(); 

			try
			{
				ClearData();
				if (ConsignmentNo != "")
				{
					//Load ConsignmentNote Data
					dsCon = ConsignmentNoteDAL.SearchConsignmentNoteDetail(strAppID, strEnterpriseID, PayerID, ConsignmentNo);
					if (dsCon != null && dsCon.Tables.Count > 0)
					{
						dtCon = dsCon.Tables[0];
						if (dtCon.Rows.Count > 0)
						{
							drCon = dtCon.Rows[0];
							if (!drCon["Consignment_No"].GetType().Equals(System.Type.GetType("System.DBNull")))
							{
								lblConsignmentNo.Text = (String)drCon["Consignment_No"];
							}
							if (!drCon["Sender_Name"].GetType().Equals(System.Type.GetType("System.DBNull")))
							{
								ddlSender.SelectedValue = (String)drCon["Sender_Name"];
								ddlSenderSelectedValue();
							}
							if (!drCon["Sender_Tel"].GetType().Equals(System.Type.GetType("System.DBNull")))
							{
								txtSenderTel.Text = (String)drCon["Sender_Tel"];
							}
							if (!drCon["CUSTOMERCODE"].GetType().Equals(System.Type.GetType("System.DBNull")))
							{
								CompanyCode = (String)drCon["CUSTOMERCODE"];
							}
							if (!drCon["reference_name"].GetType().Equals(System.Type.GetType("System.DBNull")))
							{
								txtCompanyName.Text = (String)drCon["reference_name"];
							}
							if (!drCon["Telephone"].GetType().Equals(System.Type.GetType("System.DBNull")))
							{
								txtCompanyTel.Text = (String)drCon["Telephone"];
							}
							if (!drCon["address1"].GetType().Equals(System.Type.GetType("System.DBNull")))
							{
								txtCompanyAddress.Text = (String)drCon["address1"] + " ";
							}
							if (!drCon["address2"].GetType().Equals(System.Type.GetType("System.DBNull")))
							{
								txtCompanyAddress.Text += (String)drCon["address2"] + " ";
							}
							if (!drCon["state_name"].GetType().Equals(System.Type.GetType("System.DBNull")))
							{
								txtCompanyAddress.Text += (String)drCon["state_name"] + " ";
							}
							if (!drCon["zipcode"].GetType().Equals(System.Type.GetType("System.DBNull")))
							{
								txtCompanyAddress.Text += (String)drCon["zipcode"];
							}
							if (!drCon["zone_code"].GetType().Equals(System.Type.GetType("System.DBNull")))
							{
								txtCustomerZone.Text = (String)drCon["zone_code"];
							}
							if (!drCon["cust_ref"].GetType().Equals(System.Type.GetType("System.DBNull")))
							{
								txtCustomerRef.Text = (String)drCon["cust_ref"];
							}
							if (!drCon["recipion_contact_person"].GetType().Equals(System.Type.GetType("System.DBNull")))
							{
								txtRecipientName.Text = (String)drCon["recipion_contact_person"];
							}
							if (!drCon["ServiceType"].GetType().Equals(System.Type.GetType("System.DBNull")))
							{
								Zipcode = (String)drCon["zipcode"];
								LoadServiceTypeCombo();
								lstServiceType.SelectedValue = (String)drCon["ServiceType"];
							}
							if ((bool)drCon["HCReturn"] == true)
							{
								chkHC.Checked = true;
							}
							if ((bool)drCon["InvReturn"] == true)
							{
								chkInvoice.Checked = true;
							}
							if (!drCon["codamount"].GetType().Equals(System.Type.GetType("System.DBNull")))
							{
								txtCOD.Text = String.Format("{0:0.00}", (decimal)drCon["codamount"]); 
								//txtCOD.Text = String.Format("{0:N5}", drCon["codamount"] + ""); 
								//txtCOD.Text = drCon["codamount"].ToString();

								//TempData = Convert.ToDouble(dt1.Rows[0]["codamount"].ToString());
								//txtDeclarevalue.Text = TempData.ToString("#,##0.00");
							}
							if (!drCon["declarevalue"].GetType().Equals(System.Type.GetType("System.DBNull")))
							{
								txtDeclared.Text = String.Format("{0:0.00}", (decimal)drCon["declarevalue"]); 
							}
							if (!drCon["special_instruction"].GetType().Equals(System.Type.GetType("System.DBNull")))
							{
								txtInstruction.Text = (String)drCon["special_instruction"];
							}

							//Load Package Data
							LoadPackage(ConsignmentNo);
						}
					}
				}
				//				else
				//				{
				//					//Load Initial Parameter ConsignmentNote Data
				//					dsParam = ConsignmentNoteDAL.GetParameterConsignment(strAppID, strEnterpriseID, PayerID);  
				//					if (dsParam != null && dsParam.Tables.Count > 0)
				//					{
				//						dtParam = dsParam.Tables[0];
				//						foreach(DataRow drParam in dtParam.Rows)
				//						{
				////							if (drParam["ParamName"] + "" == "Sender_Contact_Name")	{txtSenderName.Text = drParam["ParamValue"] + "";}
				////							if (drParam["ParamName"] + "" == "Sender_Contact_Name")	{ddlSender.SelectedValue = drParam["ParamValue"] + "";}	//Get from Customer Profile
				////							if (drParam["ParamName"] + "" == "Sender_Telephone")	{txtSenderTel.Text = drParam["ParamValue"] + "";}	//Get from Customer Profile
				//							if (drParam["ParamName"] + "" == "Default_Service_Type"){lstServiceType.SelectedValue = drParam["ParamValue"] + "";}
				////							if (drParam["ParamName"] + "" == "Special_Instruction")	{txtInstruction.Text = drParam["ParamValue"] + "";}
				//						}
				//					}
				//				}
			}
			catch(ApplicationException appException)
			{
				strMsg = appException.Message.ToString();
				ErrorMsg.Text = strMsg;
			}
			finally
			{
				dsCon.Dispose();
				dtCon.Dispose();
			}
		}

		private void LoadPackage(String ConsignmentNo)
		{
			DataSet dsPackage = new DataSet();
			DataTable dtPackage = new DataTable(); 

			try
			{
				dsPackage = ConsignmentNoteDAL.SearchPackage(strAppID, strEnterpriseID, PayerID, ConsignmentNo, "", "");
				if (dsPackage != null && dsPackage.Tables.Count > 0)
				{
					Session["PACKAGE"] = dsPackage.Tables[0]; 
					BindDG();
				}
			}
			catch(ApplicationException appException)
			{
				strMsg = appException.Message.ToString();
				ErrorMsg.Text = strMsg;
			}
			finally
			{
				dsPackage.Dispose();
				dtPackage.Dispose();
			}
		}


		private void BindDG()
		{
			DataTable DT = new DataTable();
 
			try
			{
				DT = (DataTable)Session["PACKAGE"];
				if (DT != null)
				{
					//					if (DT.Rows.Count == 0)
					//					{
					//						DT.Rows.Add(DT.NewRow());  //add new row
					//					}
					dgPackage.DataSource = DT;
					dgPackage.DataBind();
				}
			}
			catch(ApplicationException appException)
			{
				strMsg = appException.Message.ToString();
				ErrorMsg.Text = strMsg;
			}
			finally
			{
				DT.Dispose();
			}
		}

		/*private void SearchCompany()
		{
			DataSet DS = new DataSet();
			DataRow DR = null;

			try
			{
				DS = ConsignmentNoteDAL.SearchCompanny(strAppID, strEnterpriseID, Session["PayerID"] + "", "", txtCompanyName.Text, ""); 
				if (DS != null && DS.Tables.Count > 0 && DS.Tables[0].Rows.Count > 0)
				{
					DR = DS.Tables[0].Rows[0];
					lblCompanyCode.Text = DR["CUSTOMERCODE"] + "";
					txtCompanyName.Text = DR["REFERENCE_NAME"] + "";
					txtCompanyTel.Text = DR["TELEPHONE"] + "";
					txtCompanyAddress.Text = DR["ADDRESS1"] + " " + DR["ADDRESS2"] + " " + DR["STATE_NAME"] + " " + DR["ZIPCODE"];
				}
			}
			catch(ApplicationException appException)
			{
				strMsg = appException.Message.ToString();
				ErrorMsg.Text = strMsg;
			}
		}*/

		private void ClearData()
		{
			strMsg = "";
			ErrorMsg.Text = "";
			ErrorMsg1.Text = "";
 
			try
			{
				LoadPackage("-1");
				lblConsignmentNo.Text = "";
				lblCompanyCode.Text = "";
				if ((string)ViewState["Snd_Rec_Name"] != "")
				{
					ddlSender.SelectedValue = (string)ViewState["Snd_Rec_Name"];
				}
				else
				{
					ddlSender.SelectedIndex = 0;
				}
				ddlSenderSelectedValue();

				txtCompanyName.Text = "";
				txtCompanyTel.Text = "";
				txtCompanyAddress.Text = "";
				txtCustomerZone.Text = "";
				txtCustomerRef.Text = "";
				txtRecipientName.Text = "";
				lstServiceType.SelectedIndex = -1;
				chkHC.Checked = false;
				chkInvoice.Checked = false;
				txtCOD.Text = "";
				txtDeclared.Text = "";
				txtInstruction.Text = "";
				//ValidationSummary1.Visible = false;
				InitDataControl();
			}
			catch (ApplicationException appException)
			{
				strMsg = appException.Message.ToString();
				ErrorMsg.Text = strMsg;
			}
		}

		private void DeleteData()
		{
			int iRowsAffected = 0;
			strMsg = "";
			ErrorMsg.Text = "";
			ErrorMsg1.Text = "";

			try
			{
				if (ConsignmentNo != "")
				{
					iRowsAffected = ConsignmentNoteDAL.DeleteConsignmentNote(strAppID, strEnterpriseID, ConsignmentNo);
					if (iRowsAffected > 0)
					{
						ClearData();
						strMsg = "Delete successful.";
					}
					else
					{
						strMsg = "Delete fail.";
					}
					ErrorMsg.Text = strMsg;
				}
			}
			catch (ApplicationException appException)
			{
				strMsg = appException.Message.ToString();
				ErrorMsg.Text = strMsg;
			}
		}

		private void SaveData()
		{
			int iRowsAffected = 0;
			String HCReturn = "0";
			String InvReturn = "0";
			strMsg1 = "";
			strMsg = "";
			ErrorMsg.Text = "";
			ErrorMsg1.Text = "";

			try
			{
				if (ddlSender.SelectedValue  == "") {strMsg1 = "Sender Contact Name, ";}
				if (txtSenderTel.Text == "") {strMsg1 += "Sender Telephone, ";}
				if (txtCompanyTel.Text == "") {strMsg1 += "Recipient Telephone, ";}
				if (txtCompanyName.Text == "") {strMsg1 += "Company Name, ";}
				if (txtRecipientName.Text == "") {strMsg1 += "Recipient Name, ";}
				if (lstServiceType.SelectedValue == "") {strMsg1 += "Service Type, ";}

				if (txtCOD.Text != "")
				{
					if (Utility.CheckNumeric(txtCOD.Text, false) != true) {strMsg = "C.O.D. Amount, ";}
				}
				else
				{
					txtCOD.Text = "0";
				}
				
				if (txtDeclared.Text != "")
				{
					if (Utility.CheckNumeric(txtDeclared.Text, false) != true) {strMsg += "Declared Value in THB, ";}
				}
				else
				{
					txtDeclared.Text = "0";
				}
				
				//check Package Detail --- start
				DataTable dtPk = new DataTable();

				dtPk = (DataTable)Session["PACKAGE"];
				if (dtPk == null || dtPk.Rows.Count == 0 )
				{
					strMsg1 += "Package Detail, ";
				}
		
				dtPk.Dispose();

				if (chkHC.Checked == true) {HCReturn = "1";}
				if (chkInvoice.Checked == true) {InvReturn = "1";}

				if (strMsg == "" && strMsg1 == "")
				{
					if (ConsignmentNo == "")
					{
						//Insert data
						ConsignmentNo = ConsignmentNoteDAL.InsertConsignmentNote(strAppID, strEnterpriseID, PayerID, ddlSender.SelectedValue.Trim(), txtSenderTel.Text.Trim(), txtCompanyName.Text.Trim(), txtCompanyTel.Text.Trim(), txtCompanyAddress.Text.Trim(), CompanyCode, txtCustomerRef.Text.Trim(), txtRecipientName.Text.Trim(), lstServiceType.SelectedValue, HCReturn, InvReturn, txtCOD.Text.Trim(), txtDeclared.Text.Trim(), txtInstruction.Text.Trim(), (DataTable)Session["PACKAGE"]);
						if (ConsignmentNo != "")
						{
							LoadData();
							strMsg = "Save successful.";
						}
						else
						{
							strMsg = "Save fail.";
						}
					}
					else
					{
						//Updata data
						iRowsAffected = ConsignmentNoteDAL.UpdateConsignmentNote(strAppID, strEnterpriseID, ConsignmentNo, PayerID, ddlSender.SelectedValue.Trim(), txtSenderTel.Text.Trim(), txtCompanyName.Text.Trim(), txtCompanyTel.Text.Trim(), txtCompanyAddress.Text.Trim(), CompanyCode, txtCustomerRef.Text.Trim(), txtRecipientName.Text.Trim(), lstServiceType.SelectedValue, HCReturn, InvReturn, txtCOD.Text.Trim(), txtDeclared.Text.Trim(), txtInstruction.Text.Trim(), (DataTable)Session["PACKAGE"]);
						if (iRowsAffected > 0)
						{
							LoadData();
							strMsg = "Save successful.";
						}
						else
						{
							strMsg = "Save fail.";
						}
					}
				}
				else
				{
					if (strMsg.Length > 0) {strMsg =  Utility.GetLanguageText(ResourceType.UserMessage,"PLS_CHECK_DATA",utility.GetUserCulture()) + strMsg.Substring(0, strMsg.Length - 2) ;}
					if (strMsg1.Length > 0) {strMsg1 = Utility.GetLanguageText(ResourceType.UserMessage,"PLS_CHECK_DATA",utility.GetUserCulture()) + strMsg1.Substring(0, strMsg1.Length - 2);}
				}
			}
			catch (ApplicationException appException)
			{
				strMsg = appException.Message.ToString();
			}
			finally
			{
				ErrorMsg.Text = strMsg;
				ErrorMsg1.Text = strMsg1;
			}
		}

		private void dgPackage_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
		{
			DataSet DS = new DataSet(); 
			DataTable DT = new DataTable();
			DropDownList lstBoxSizeAdd;
			DropDownList lstBoxSize;
			String BoxSize = "";

			try
			{
				if (e.Item.ItemType == ListItemType.Footer)
				{
					lstBoxSizeAdd = (DropDownList)(e.Item.FindControl("lstBoxSizeAdd"));
					if (lstBoxSizeAdd != null)
					{
						lstBoxSizeAdd.Items.Clear(); 
						DS = ConsignmentNoteDAL.GetBoxSize(strAppID, strEnterpriseID, "", PayerID);
						if (DS != null && DS.Tables.Count > 0)
						{
							DT = DS.Tables[0];
							lstBoxSizeAdd.DataSource = DT;
							lstBoxSizeAdd.DataTextField = "Box_SizeID";
							lstBoxSizeAdd.DataValueField = "Box_SizeID";
							lstBoxSizeAdd.DataBind();

							ListItem InsertItem = new ListItem("CUSTOMIZE", "CUSTOMIZE");
							lstBoxSizeAdd.Items.Insert(0, InsertItem); 
						}
					}
				}

				if (e.Item.ItemType == ListItemType.EditItem)
				{
					// Set Box Size in Datagrid Edited
					lstBoxSize = (DropDownList)(e.Item.FindControl("lstBoxSize"));
					if (lstBoxSize != null)
					{
						lstBoxSize.Items.Clear();
						DS = ConsignmentNoteDAL.GetBoxSize(strAppID, strEnterpriseID, "", PayerID);
						if (DS != null && DS.Tables.Count > 0)
						{
							DT = DS.Tables[0];
							lstBoxSize.DataSource = DT;
							lstBoxSize.DataTextField = "Box_SizeID";
							lstBoxSize.DataValueField = "Box_SizeID";
							lstBoxSize.DataBind();

							ListItem InsertItem = new ListItem("CUSTOMIZE","CUSTOMIZE");
							lstBoxSize.Items.Insert(0, InsertItem); 
							BoxSize = DataBinder.Eval(e.Item.DataItem, "BOX_SIZE").ToString(); 
							lstBoxSize.SelectedValue =  BoxSize;

							// Set Weight, Length, Height in Datagrid Edited
							if (BoxSize != "")
							{
								if (BoxSize == "CUSTOMIZE")
								{
									e.Item.Cells[7].Text = "<input type=text id=txtWeight name=txtWeight size=5 maxlength=3 style='TEXT-ALIGN: right' value='" + DataBinder.Eval(e.Item.DataItem, "WIDTH").ToString() + "'>";
									e.Item.Cells[8].Text = "<input type=text id=txtLength name=txtLength size=5 maxlength=3 style='TEXT-ALIGN: right' value='" + DataBinder.Eval(e.Item.DataItem, "LENGTH").ToString() + "'>";
									e.Item.Cells[9].Text = "<input type=text id=txtHeight name=txtHeight size=5 maxlength=3 style='TEXT-ALIGN: right' value='" + DataBinder.Eval(e.Item.DataItem, "HEIGTH").ToString() + "'>";
								}
							}
						}
					}
				}

				if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
				{
					e.Item.Attributes.Add("onmouseover", "javascript:ShowBar(this);"); 
					e.Item.Attributes.Add("onmouseout", "javascript:HideBar(this);");
				}
			}
			catch (ApplicationException appException)
			{
				strMsg = appException.Message.ToString();
				ErrorMsg.Text = strMsg;
			}
			finally
			{
				DS.Dispose();
				DT.Dispose();
			}
		}

		private void dgPackage_ItemCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
		{
			DoItemAction(e.CommandName, e.Item);
		}

		private void DoItemAction(String action, System.Web.UI.WebControls.DataGridItem item)
		{
			DataTable DT = new DataTable();
			DataRow DR = null;
			DataSet dsBox = new DataSet(); 
			DataTable dtBox = new DataTable();
			DataRow drBox = null;
			DropDownList lstBoxSizeAdd;
			DropDownList lstBoxSize;
			String BoxType = "";
			String BoxSize = "";
			String BoxDesc = "";
			String ActualWeight = "";
			String Quantity = "";
			double Total = 0;
			String Width = "";
			String Length = "";
			String Height = "";
			String Msg = "";
			String Msg1 = "";
			ErrorMsg.Text = "";
			ErrorMsg1.Text = "";

			try
			{
				action = action.ToUpper();

				DT = (DataTable)Session["PACKAGE"];
				dgPackage.EditItemIndex = -1;	

				/*if (action != "ADD_ITEM")
				{
					ItemIndex = dgPackage.DataKeys[item.ItemIndex].ToString();  
				}
				DT = (DataTable)Session["PACKAGE"];*/

				switch (action)
				{
					case "ADD_ITEM":
						lstBoxSizeAdd = (DropDownList)item.FindControl("lstBoxSizeAdd");
						if (lstBoxSizeAdd != null) {BoxSize = lstBoxSizeAdd.SelectedValue; }

						ActualWeight = Request.Form["txtActualWeightAdd"].Trim();
						if (ActualWeight == ""){Msg += " Actual Weight (Kg.),";}
                        else if (Utility.CheckNumeric(ActualWeight, false) != true){Msg += "  Actual Weight (Kg.),";}
						else if (ActualWeight.Length > 6){Msg += " Actual Weight (Kg.),";}
						else if (Convert.ToDecimal(ActualWeight) <= 0){Msg += " Actual Weight (Kg.),";}

						Quantity = Request.Form["txtQuantityAdd"].Trim();
						if (Quantity == ""){Msg += " Quantity,";}
						else if (Utility.CheckNumeric(Quantity, true) != true){Msg += " Quantity,";}
						else if (Quantity.Length > 3){Msg += " Quantity,";}
						else if (Convert.ToInt32(Quantity) <= 0){Msg += " Quantity,";}

						if (Msg == "" && Msg1 == ""){Total = Convert.ToDouble(ActualWeight) * Convert.ToInt64(Quantity) ;}
						if (BoxSize != "")
						{
							if (BoxSize != "CUSTOMIZE")
							{
								BoxType = "S";
								dsBox = ConsignmentNoteDAL.GetBoxSize(strAppID, strEnterpriseID, BoxSize, PayerID);
								if (dsBox != null && dsBox.Tables.Count > 0)
								{
									dtBox = dsBox.Tables[0];
									if (dtBox != null && dtBox.Rows.Count > 0)
									{
										drBox = dtBox.Rows[0];
										if (!drBox["BOX_WIDE"].GetType().Equals(System.Type.GetType("System.DBNull"))){Width = drBox["BOX_WIDE"].ToString();}
										if (!drBox["BOX_LENGTH"].GetType().Equals(System.Type.GetType("System.DBNull"))){Length = drBox["BOX_LENGTH"].ToString();}
										if (!drBox["BOX_HEIGHT"].GetType().Equals(System.Type.GetType("System.DBNull"))){Height = drBox["BOX_HEIGHT"].ToString();}
									}
								}
							}
							else
							{
								BoxType = "C";
								Width = Request.Form["txtWidthAdd"].Trim();		//boon 2010/01/20
								if (Width == ""){Msg += " Width,";}
								else if (Utility.CheckNumeric(Width, true) != true){Msg += " Width,";}
								else if (Width.Length > 3){Msg += " Width,";}
								else if (Convert.ToInt32(Width) <= 0){Msg += " Width,";}

								Length = Request.Form["txtLengthAdd"].Trim();	
								if (Length == ""){Msg += " Length,";}
								else if (Utility.CheckNumeric(Length, true) != true){Msg += " Length,";}
								else if (Length.Length > 3){Msg += " Length,";}
								else if (Convert.ToInt32(Length) <= 0){Msg += " Length,";}	

								Height = Request.Form["txtHeightAdd"].Trim();	
								if (Height == ""){Msg += " Height,";}
								else if (Utility.CheckNumeric(Height, true) != true){Msg += " Height,";}
								else if (Height.Length > 3){Msg += " Height,";}
								else if (Convert.ToInt32(Height) <= 0){Msg += " Height,";}
							}
							/*
							if (Utility.CheckNumeric(Width, false) != true){Msg1 += " Width,";}
							if (Utility.CheckNumeric(Length, false) != true){Msg1 += " Length,";}
							if (Utility.CheckNumeric(Height, false) != true){Msg1 += " Height,";}

							//format BOX DESCRIPTION : 999.99_x_999.99_x_999.99
							BoxDesc = Utility.BoxDescFormat(Width) + " x " + Utility.BoxDescFormat(Length) + " x " + Utility.BoxDescFormat(Height);
							//BoxDesc = BoxDescFormat(Width) + " x " + BoxDescFormat(Length) + " x " + BoxDescFormat(Height);
							*/													

							//format BOX DESCRIPTION : 999_x_999_x_999
							BoxDesc = Utility.BoxDescFormat(Width) + " x " + Utility.BoxDescFormat(Length) + " x " + Utility.BoxDescFormat(Height);
						}

						if (Msg == "" && Msg1 == "")
						{
							/*If DT.Rows.Count > 0 AndAlso DT.Rows(0).Item("LOAN_ID") & "" = "" Then
								DT.Rows.RemoveAt(0)
							End If*/

							DR = DT.NewRow();
							//Set new Seq.No.
							for (int i=0; i < DT.Rows.Count; i++)
							{
								DT.Rows[i]["SEQ_NO"] = i + 1;
							}
							DR["SEQ_NO"] = DT.Rows.Count + 1;

							DR["BOXTYPE"] = BoxType;
							DR["BOX_DESC"] = BoxDesc;
							DR["BOX_SIZE"] = BoxSize;
							DR["KILO"] = ActualWeight;
							DR["QTY"] = Convert.ToInt64(Quantity);
							DR["TOTAL_KG"] = Total;
							DR["WIDTH"] = Width;
							DR["LENGTH"] = Length;
							DR["HEIGTH"] = Height;
							DT.Rows.Add(DR);
						}
						dgPackage.EditItemIndex = -1;
						break;

					case "SAVE_ITEM":
						// If item.ItemIndex >= 0 And item.ItemIndex < DT.Rows.Count Then
						//TempType = dgResult.DataKeys(item.ItemIndex)
						
						DR = DT.Rows[item.ItemIndex];
						lstBoxSize = (DropDownList)item.FindControl("lstBoxSize");
						if (lstBoxSize != null) {BoxSize = lstBoxSize.SelectedValue; }

						ActualWeight = Request.Form["txtActualWeight"].Trim();
						if (ActualWeight == ""){Msg += " Actual Weight (Kg.),";}
						else if (Utility.CheckNumeric(ActualWeight, false) != true){Msg += "  Actual Weight (Kg.),";}
						else if (ActualWeight.Length > 6){Msg += " Actual Weight (Kg.),";}
						else if (Convert.ToDecimal(ActualWeight) <= 0){Msg += " Actual Weight (Kg.),";}							

						Quantity = Request.Form["txtQuantity"].Trim();
						if (Quantity == ""){Msg += " Quantity,";}
						else if (Utility.CheckNumeric(Quantity, true) != true){Msg += " Quantity,";}
						else if (Quantity.Length > 3){Msg += " Quantity,";} 
						else if (Convert.ToInt32(Quantity) <= 0){Msg += " Quantity,";}

						if (Msg == "" && Msg1 == ""){Total = Convert.ToDouble(ActualWeight) * Convert.ToInt64(Quantity);}
						if (BoxSize != "")
						{
							if (BoxSize != "CUSTOMIZE")
							{
								BoxType = "S";
								dsBox = ConsignmentNoteDAL.GetBoxSize(strAppID, strEnterpriseID, BoxSize, PayerID);
								if (dsBox != null && dsBox.Tables.Count > 0)
								{
									dtBox = dsBox.Tables[0];
									if (dtBox != null && dtBox.Rows.Count > 0)
									{
										drBox = dtBox.Rows[0];
										if (!drBox["BOX_WIDE"].GetType().Equals(System.Type.GetType("System.DBNull"))){Width = drBox["BOX_WIDE"].ToString();}
										if (!drBox["BOX_LENGTH"].GetType().Equals(System.Type.GetType("System.DBNull"))){Length = drBox["BOX_LENGTH"].ToString();}
										if (!drBox["BOX_HEIGHT"].GetType().Equals(System.Type.GetType("System.DBNull"))){Height = drBox["BOX_HEIGHT"].ToString();}
									}
								}
							}
							else
							{
								BoxType = "C";
								Width = Request.Form["txtWeight"].Trim();	
								if (Width == ""){Msg += " Width,";}
								else if (Utility.CheckNumeric(Width, true) != true){Msg += " Width,";}
								else if (Width.Length > 3){Msg += " Width,";}
								else if (Convert.ToInt32(Width) <= 0){Msg += " Width,";}

								Length = Request.Form["txtLength"].Trim();
								if (Length == ""){Msg += " Length,";}
								else if (Utility.CheckNumeric(Length, true) != true){Msg += " Length,";}
								else if (Length.Length > 3){Msg += " Length,";}
								else if (Convert.ToInt32(Length) <= 0){Msg += " Length,";}

								Height = Request.Form["txtHeight"].Trim();
								if (Height == ""){Msg += " Height,";}
								else if (Utility.CheckNumeric(Height, true) != true){Msg += " Height,";}
								else if (Height.Length > 3){Msg += " Height,";}
								else if (Convert.ToInt32(Height) <= 0){Msg += " Height,";}
							}
							/*
							if (Utility.CheckNumeric(Width, false) != true){Msg1 += " Width,";}
							if (Utility.CheckNumeric(Length, false) != true){Msg1 += " Length,";}
							if (Utility.CheckNumeric(Height, false) != true){Msg1 += " Height,";}

							//format BOX DESCRIPTION : 999.99_x_999.99_x_999.99
							BoxDesc = Utility.BoxDescFormat(Width) + " x " + Utility.BoxDescFormat(Length) + " x " + Utility.BoxDescFormat(Height);
							//BoxDesc = BoxDescFormat(Width) + " x " + BoxDescFormat(Length) + " x " + BoxDescFormat(Height);
							*/												

							//format BOX DESCRIPTION : 999_x_999_x_999
							BoxDesc = Utility.BoxDescFormat(Width) + " x " + Utility.BoxDescFormat(Length) + " x " + Utility.BoxDescFormat(Height);
						}

						if (Msg == "" && Msg1 == "")
						{
							DR["BOXTYPE"] = BoxType;
							DR["BOX_DESC"] = BoxDesc;
							DR["BOX_SIZE"] = BoxSize;
							DR["KILO"] = ActualWeight;
							DR["QTY"] = Quantity;
							DR["TOTAL_KG"] = Total;
							DR["WIDTH"] = Width;
							DR["LENGTH"] = Length;
							DR["HEIGTH"] = Height;							
						}
						dgPackage.EditItemIndex = -1;
						break;

					case "DELETE_ITEM":
						if (item.ItemIndex < DT.Rows.Count)
						{
							DT.Rows.RemoveAt(item.ItemIndex); 
						}
						dgPackage.EditItemIndex = -1;

						//Set new Seq.No.
						for (int i = 0; i < DT.Rows.Count; i++)
						{
							DT.Rows[i]["SEQ_NO"] = i + 1;
						}

						break;

					case "EDIT_ITEM":
						dgPackage.EditItemIndex = item.ItemIndex; 
						break;

					case "CANCEL_ITEM":
						dgPackage.EditItemIndex = -1;
						break;
				}
				Session["PACKAGE"] = DT;
				BindDG();

				if (Msg != "") {ErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"PLS_CHECK_DATA",utility.GetUserCulture()) + Msg.Substring(0, Msg.Length - 1);}
				if (Msg1 != "") {ErrorMsg1.Text = Utility.GetLanguageText(ResourceType.UserMessage,"PLS_CHECK_DATA",utility.GetUserCulture()) + Msg1.Substring(0, Msg1.Length - 1) ;}
			}
			catch (ApplicationException appException)
			{
				strMsg = appException.Message.ToString();
				ErrorMsg.Text = strMsg;
			}
			finally
			{
				DT.Dispose();
				dsBox.Dispose();
				dtBox.Dispose();
			}
		}

		private void ddlSender_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			ddlSenderSelectedValue();
		}

		private void ddlSenderSelectedValue()
		{
			DataSet dsSendTel = ConsignmentNoteDAL.GetCustSenderBySndNa(strAppID, strEnterpriseID, PayerID, ddlSender.SelectedValue.ToString());

			DataRow dr = null;

			if (dsSendTel.Tables[0].Rows.Count > 0) 
			{
				dr = dsSendTel.Tables[0].Rows[0];
				
				txtContactPerson.Text = dr["contact_person"].ToString();
				txtAddr.Text = dr["address1"].ToString() + " " + dr["address2"].ToString();
				txtPostalCode.Text = dr["zipcode"].ToString();
				txtState.Text = dr["state_name"].ToString();
				txtSenderTel.Text = dr["telephone"].ToString();	
			}

			dsSendTel.Dispose();
		}

		private string getRole()
		{
            User user = new User();
			user.UserID = userID;
			userRoleArray = com.common.RBAC.RBACManager.GetAllRoles(strAppID, strEnterpriseID, user);
			Role role;

			for(int i=0;i<userRoleArray.Count;i++)
			{
				role = (Role)userRoleArray[i];				
				if (role.RoleName.ToUpper().Trim() == "PREPRINTSTAFF") return "PREPRINTSTAFF";
			}
			return " ";
		}

		#region BoxDescFormat for gen descriptionBox
			/*private String BoxDescFormat(String Size)
			{
				String SizeFormat = "";
				int MaxLenght = 0;
				int i = 0;
				int iPart = 0;

				try
				{
					//format BOX DESCRIPTION : 999.99_x_999.99_x_999.99			
					i = Size.IndexOf(".");
					MaxLenght = Size.Length;
					if (i == -1)
					{
						switch (MaxLenght)
						{
							case 0:
								SizeFormat = "   " + Size;
								break;
							case 1:
								SizeFormat = "  " + Size;
								break;
							case 2:
								SizeFormat = " " + Size;
								break;
							case 3:
								SizeFormat = Size;
								break;
						}
						SizeFormat = SizeFormat + ".00";
					}
					else
					{
						switch (i)
						{
							case 0:
								SizeFormat = "   " + Size;
								break;
							case 1:
								SizeFormat = "  " + Size;
								break;
							case 2:
								SizeFormat = " " + Size;
								break;
							case 3:
								SizeFormat = Size;
								break;
						}
						MaxLenght = Size.Length;
						iPart = MaxLenght - i - 1;
						if (iPart == 0)   
						{
							SizeFormat += ".00";
						}
						else if (iPart == 1)  //5.1
						{
							SizeFormat += "0"; 
						} 
						else if (iPart > 2)
						{
							SizeFormat += Size.Substring(i-1, 2); //��ҷȹ��� 2 ���˹�
						}
						else	//5.12
						{
							SizeFormat = Size; 
						}
					}
				}
				catch (Exception ex)
				{
					strMsg = ex.Message.ToString();
					ErrorMsg.Text = strMsg;
				}
				return SizeFormat;
			}


			
			//Check input is Integer?
			//if input = integer then Integer = true
			//if input = double, float then Integer = false
			private bool CheckNumeric(String sText, bool beInteger)
			{
				String ValidChars = "";
				bool beNumber = true;
				//String tmpChar;
			
				try
				{
					if (beInteger == true)
					{
						ValidChars = "0123456789";
					}
					else
					{
						ValidChars = "0123456789.";
					}

					for (int i = 0; i < sText.Length && beNumber == true; i++)
					{ 
						char tmpChar = sText[i]; 
						if (ValidChars.IndexOf(tmpChar) == -1)  
						{
							beNumber = false;
						}
					}
				}
				catch (Exception ex)
				{
					beNumber = false;
					strMsg = ex.Message.ToString();
					ErrorMsg.Text = strMsg;
				}
				return beNumber;
			}
		*/
		#endregion
		
	}
}
