<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<%@ Import Namespace="System.Configuration" %>
<%@ Import Namespace="com.common.util" %>
<%@ Page language="c#" Codebehind="BillPlacementRulesPopUp.aspx.cs" AutoEventWireup="false" Inherits="TIES.WebUI.BillPlacementRulesPopUp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>BillPlacementRulesPopUp</title>
		<meta content="True" name="vs_snapToGrid">
		<meta content="False" name="vs_showGrid">
		<LINK href="css/Styles.css" type="text/css" rel="stylesheet" name="stylesheet">
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<!--#INCLUDE FILE="msFormValidations.inc"-->
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			&nbsp;
			<TABLE id="Table4" cellSpacing="0" cellPadding="0" width="667" border="0">
				<TR>
					<TD width="80%"><asp:label id="Label5" runat="server" CssClass="mainTitleSize">Set Bill Placement Rules</asp:label></TD>
					<TD width="10%"></TD>
					<TD width="10%"></TD>
				</TR>
				<TR>
					<TD><asp:label id="lblErrorMsg" runat="server" CssClass="errorMsgColor" Width="312px" Height="32px"
							ForeColor="Red">ErrorMessage</asp:label><asp:label id="Label6" style="Z-INDEX: 114; LEFT: 427px; POSITION: absolute; TOP: 134px" runat="server"
							CssClass="tableLabel" Width="142px">Last Updated by:</asp:label></TD>
					<TD><asp:button id="btnQK" runat="server" CssClass="queryButton" Width="82px" Text="OK" CausesValidation="False"></asp:button></TD>
					<TD></TD>
				</TR>
				<TR>
					<TD><asp:label id="Label4" runat="server" Width="96px" CssClass="tablelabel">BPD: Remark</asp:label><asp:textbox id="txtRemark" runat="server" Width="432px" Height="21px"></asp:textbox></TD>
					<TD><asp:button id="btnCancel" runat="server" CssClass="queryButton" Width="83px" Text="Cancel"
							CausesValidation="False"></asp:button></TD>
					<TD></TD>
				</TR>
				<TR>
					<TD colSpan="3">
						<FIELDSET style="WIDTH: 377px; HEIGHT: 68px" align="absMiddle"><LEGEND><asp:label id="Label52" CssClass="tableHeadingFieldset" Runat="server">Accept Placement Frequency</asp:label></LEGEND>&nbsp;&nbsp;</FONT><FONT face="Verdana">&nbsp;&nbsp;&nbsp;</FONT>
							<TABLE id="Table2" cellSpacing="0" width="326" align="left" border="0" runat="server">
								<TR height="25">
									<TD vAlign="middle" align="center" width="10%"><FONT face="Tahoma"><asp:radiobutton id="r_Weekly" runat="server" CssClass="tableLabel" Text="Weekly" AutoPostBack="True"
												GroupName="APF"></asp:radiobutton>&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;
											<asp:radiobutton id="r_Semi_Monthly" runat="server" CssClass="tableLabel" Text="Semi-monthly" AutoPostBack="True"
												GroupName="APF"></asp:radiobutton>&nbsp;&nbsp;&nbsp; &nbsp;
											<asp:radiobutton id="r_Monthly" runat="server" CssClass="tableLabel" Text="Monthly" AutoPostBack="True"
												GroupName="APF"></asp:radiobutton></FONT></TD>
								</TR>
							</TABLE>
						</FIELDSET>
					</TD>
				</TR>
				<TR>
					<TD colSpan="3">
						<FIELDSET runat="server" id="Weekly" style="WIDTH: 667px; HEIGHT: 140px" align="top"><LEGEND><asp:label id="Label1" CssClass="tableHeadingFieldset" Runat="server">Weekly</asp:label></LEGEND>
							<TABLE id="Table1" cellSpacing="0" width="651" align="left" border="0" runat="server" CssClass="tableLabel">
								<TR>
									<TD style="WIDTH: 82px; HEIGHT: 19px" align="left" width="82"></TD>
									<TD style="WIDTH: 89px; HEIGHT: 19px" vAlign="top" width="89"></TD>
									<TD style="WIDTH: 93px; HEIGHT: 19px" vAlign="top" width="93"></TD>
									<TD style="WIDTH: 88px; HEIGHT: 19px" vAlign="top" width="88"></TD>
									<TD style="WIDTH: 59px; HEIGHT: 19px" vAlign="top" width="59"></TD>
									<TD style="HEIGHT: 19px" vAlign="top" width="150"></TD>
									<TD style="HEIGHT: 19px" vAlign="top" width="150">
										<asp:Label id="Label7" runat="server" CssClass="tableLabel">IF public Holiday </asp:Label>
									</TD>
								</TR>
								<TR height="25">
									<TD style="WIDTH: 82px" align="left" width="82"><asp:checkbox id="cb_Weekly_Monday" runat="server" CssClass="tableLabel" Text="Monday"></asp:checkbox></TD>
									<TD style="WIDTH: 89px" vAlign="top" width="89"><FONT face="Tahoma"><asp:checkbox id="cb_Weekly_TuesDay" runat="server" CssClass="tableLabel" Text="Tuesday"></asp:checkbox></FONT></TD>
									<TD style="WIDTH: 93px" vAlign="top" width="93"><FONT face="Tahoma"><asp:checkbox id="cb_Weekly_Wednesday" runat="server" CssClass="tableLabel" Text="Wednesday"></asp:checkbox></FONT></TD>
									<TD style="WIDTH: 88px" vAlign="top" width="88"><asp:checkbox id="cb_Weekly_Thursday" runat="server" CssClass="tableLabel" Text="Thursday"></asp:checkbox></TD>
									<TD style="WIDTH: 59px" vAlign="top" width="59"><FONT face="Tahoma"><asp:checkbox id="cb_Weekly_Friday" runat="server" CssClass="tableLabel" Text="Friday"></asp:checkbox></FONT></TD>
									<TD vAlign="top" width="150"></TD>
									<TD vAlign="top" width="150"><FONT face="Tahoma">&nbsp;
											<asp:radiobutton id="r_Weekly_AllowPlacement" runat="server" CssClass="tableLabel" Width="187px"
												Text="Allow placement on that date" AutoPostBack="True" GroupName="Weekly"></asp:radiobutton></FONT></TD>
								</TR>
								<TR height="25">
									<TD style="WIDTH: 82px; HEIGHT: 25px" width="82"><FONT face="Tahoma"><asp:checkbox id="cb_Weekly_Saturday" runat="server" CssClass="tableLabel" Text="Saturday"></asp:checkbox></FONT></TD>
									<TD style="WIDTH: 89px; HEIGHT: 25px" width="89"><asp:checkbox id="cb_Weekly_Sunday" runat="server" CssClass="tableLabel" Text="Sunday"></asp:checkbox></TD>
									<TD style="WIDTH: 93px; HEIGHT: 25px" width="93"></TD>
									<TD style="WIDTH: 88px; HEIGHT: 25px" width="88"></TD>
									<TD style="WIDTH: 59px; HEIGHT: 25px" width="59"></TD>
									<TD style="HEIGHT: 25px" align="left" width="150"></TD>
									<TD style="HEIGHT: 25px" align="left" width="150"><FONT face="Tahoma">&nbsp;</FONT>
										<asp:radiobutton id="r_Weekly_AdjustToNext" runat="server" CssClass="tableLabel" Width="192px" Text="Adjust to next business day"
											AutoPostBack="True" GroupName="Weekly"></asp:radiobutton></TD>
								</TR>
								<TR>
									<TD style="WIDTH: 82px" width="82"></TD>
									<TD style="WIDTH: 89px" width="89"></TD>
									<TD style="WIDTH: 93px" width="93"></TD>
									<TD style="WIDTH: 88px" width="88"></TD>
									<TD style="WIDTH: 59px" width="59"></TD>
									<TD align="left" width="150"></TD>
									<TD align="left" width="150"><FONT face="Tahoma">&nbsp;</FONT>
										<asp:radiobutton id="r_Weekly_AdjustToPrevious" runat="server" CssClass="tableLabel" Width="221px"
											Text="Adjust to previous business day" AutoPostBack="True" GroupName="Weekly"></asp:radiobutton></TD>
								</TR>
								<TR>
									<TD style="WIDTH: 82px" width="82"></TD>
									<TD style="WIDTH: 89px" width="89"></TD>
									<TD style="WIDTH: 93px" width="93"></TD>
									<TD style="WIDTH: 88px" width="88"></TD>
									<TD style="WIDTH: 59px" width="59"></TD>
									<TD align="left" width="150"></TD>
									<TD align="left" width="150"><FONT face="Tahoma">&nbsp;</FONT>
										<asp:radiobutton id="r_Weekly_CancelPlacement" runat="server" CssClass="tableLabel" Width="182px"
											Text="Cancel this placement" AutoPostBack="True" GroupName="Weekly"></asp:radiobutton></TD>
								</TR>
							</TABLE>
						</FIELDSET>
					</TD>
				</TR>
				<TR>
					<TD colSpan="3">
						<FIELDSET id="Semi_Monthly" style="WIDTH: 667px" align="top"><LEGEND><asp:label id="Label2" CssClass="tableHeadingFieldset" Runat="server">Semi-monthly (first payment of the month)</asp:label></LEGEND>
							<TABLE id="Table10" cellSpacing="0" width="647" align="left" border="0" runat="server"
								CssClass="tableLabel">
								<TR height="25">
									<TD style="WIDTH: 291px; HEIGHT: 30px" align="right" width="291"></TD>
									<TD style="HEIGHT: 30px" width="724">
										<asp:Label id="Label8" runat="server" CssClass="tableLabel">If bill placement Date occurs on Weekend or Public Holiday</asp:Label></TD>
								</TR>
								<TR height="25">
									<TD style="WIDTH: 291px; HEIGHT: 26px" noWrap align="left" width="291"><asp:radiobutton id="r_Semi_Monthly_FirstPlacement" runat="server" CssClass="tableLabel" Text="The"
											AutoPostBack="True" GroupName="SemiMonthCon"></asp:radiobutton><FONT face="Tahoma">&nbsp;
											<asp:dropdownlist id="ddl_Semi_Monthly_OnceAMonth" runat="server" Width="101px" Height="15px" AutoPostBack="True"></asp:dropdownlist>&nbsp;
											<asp:dropdownlist id="ddl_Semi_Monthly_OnceAMonthOf" runat="server" Width="104px" AutoPostBack="True"></asp:dropdownlist></FONT></TD>
									<TD align="left"><FONT face="Tahoma">&nbsp;&nbsp; </FONT>
										<asp:radiobutton id="r_Semi_Monthly_AllowPlacement" runat="server" CssClass="tableLabel" Width="256px"
											Text="Allow placement on that date" AutoPostBack="True" GroupName="SemiMonth"></asp:radiobutton></FONT></TD>
								</TR>
								<TR height="25">
									<TD style="WIDTH: 291px" noWrap align="left" width="291" colSpan="1"></TD>
									<TD width="724"><FONT face="Tahoma">&nbsp;&nbsp; </FONT>
										<asp:radiobutton id="r_Semi_Monthly_AdjustToNext" runat="server" CssClass="tableLabel" Width="239px"
											Text="Adjust to next business day" AutoPostBack="True" GroupName="SemiMonth"></asp:radiobutton></TD>
								</TR>
								<TR height="25">
									<TD style="WIDTH: 291px" width="291"><FONT face="Tahoma"><asp:radiobutton id="r_Semi_Monthly_DayRange" runat="server" CssClass="tableLabel" Text="Day" AutoPostBack="True"
												GroupName="SemiMonthCon"></asp:radiobutton><FONT face="Tahoma">&nbsp;
												<cc1:mstextbox onpaste="return false;" id="txt_Semi_Monthly_DayStart" style="TEXT-TRANSFORM: uppercase"
													tabIndex="4" runat="server" CssClass="textField" Width="34px" MaxLength="3" TextMaskType="msNumeric"
													onfocus="javascript:this.select();" NumberMaxValue="31" NumberMinValue="1" NumberPrecision="5"
													NumberScale="1"></cc1:mstextbox>&nbsp;<FONT size="2">through </FONT>
												<cc1:mstextbox onpaste="return false;" id="txt_Semi_Monthly_DayEnd" style="TEXT-TRANSFORM: uppercase"
													tabIndex="4" runat="server" CssClass="textField" Width="39px" MaxLength="3" TextMaskType="msNumeric"
													onfocus="javascript:this.select();" NumberMaxValue="31" NumberMinValue="1" NumberPrecision="5"
													NumberScale="1"></cc1:mstextbox><FONT size="2">&nbsp; of the month</FONT></FONT></FONT></TD>
									<TD align="left"><FONT face="Tahoma">&nbsp;&nbsp; </FONT>
										<asp:radiobutton id="r_Semi_Monthly_AdjustToPrevious" runat="server" CssClass="tableLabel" Width="257px"
											Text="Adjust to previous business day" AutoPostBack="True" GroupName="SemiMonth"></asp:radiobutton></TD>
								</TR>
								<TR height="25">
									<TD style="WIDTH: 291px" width="291"></TD>
									<TD align="left"><FONT face="Tahoma">&nbsp;&nbsp;
											<asp:radiobutton id="r_Semi_Monthly_CancelPlacement" runat="server" CssClass="tableLabel" Width="182px"
												Text="Cancel this placement" AutoPostBack="True" GroupName="SemiMonth"></asp:radiobutton></FONT></TD>
								</TR>
							</TABLE>
						</FIELDSET>
					</TD>
				</TR>
				<TR>
					<TD colSpan="3">
						<FIELDSET id="Monthly" style="WIDTH: 667px;HEIGHT: 135px" align="top"><LEGEND><asp:label id="Label3" CssClass="tableHeadingFieldset" Runat="server">Monthly</asp:label></LEGEND>
							<TABLE id="Table3" cellSpacing="0" width="647" align="left" border="0" runat="server" CssClass="tableLabel">
								<TR height="25">
									<TD style="WIDTH: 291px; HEIGHT: 30px" align="right" width="291"></TD>
									<TD style="HEIGHT: 30px" width="724">
										<asp:Label id="Label9" runat="server" CssClass="tableLabel">If bill placement Date occurs on Weekend or Public Holiday</asp:Label></TD>
								</TR>
								<TR height="25">
									<TD style="WIDTH: 291px; HEIGHT: 26px" noWrap align="left" width="291"><asp:radiobutton id="r_Monthly_OnceAMonth" runat="server" CssClass="tableLabel" Text="The" AutoPostBack="True"
											GroupName="MonthCon"></asp:radiobutton><FONT face="Tahoma">&nbsp;
											<asp:dropdownlist id="ddl_Monthly_OnceAMonth" runat="server" Width="101px" Height="15px" AutoPostBack="True"></asp:dropdownlist>&nbsp;
											<asp:dropdownlist id="ddl_Monthly_OnceAMonthOf" runat="server" Width="104px" AutoPostBack="True"></asp:dropdownlist></FONT></TD>
									<TD align="left"><FONT face="Tahoma">&nbsp;&nbsp; </FONT>
										<asp:radiobutton id="r_Monthly_AllowPlacement" runat="server" CssClass="tableLabel" Width="256px"
											Text="Allow placement on that date" AutoPostBack="True" GroupName="Month"></asp:radiobutton></TD>
								</TR>
								<TR height="25">
									<TD style="WIDTH: 291px" noWrap align="left" width="291" colSpan="1"></TD>
									<TD align="left"><FONT face="Tahoma">&nbsp;&nbsp; </FONT>
										<asp:radiobutton id="r_Monthly_AdjustToNext" runat="server" CssClass="tableLabel" Width="239px" Text="Adjust to next business day"
											AutoPostBack="True" GroupName="Month"></asp:radiobutton></TD>
								</TR>
								<TR height="25">
									<TD style="WIDTH: 291px" width="291"><FONT face="Tahoma"><asp:radiobutton id="r_Monthly_DayRange" runat="server" CssClass="tableLabel" Text="Day" AutoPostBack="True"
												GroupName="MonthCon"></asp:radiobutton><FONT face="Tahoma">&nbsp;
												<cc1:mstextbox onpaste="return false;" id="txt_Monthly_DayStart" style="TEXT-TRANSFORM: uppercase"
													tabIndex="4" runat="server" CssClass="textField" Width="34px" MaxLength="2" TextMaskType="msNumeric"
													onfocus="javascript:this.select();" NumberMaxValue="31" NumberMinValue="1" NumberPrecision="5"
													NumberScale="1"></cc1:mstextbox>&nbsp;<FONT size="2">through </FONT>
												<cc1:mstextbox onpaste="return false;" id="txt_Monthly_DayEnd" style="TEXT-TRANSFORM: uppercase"
													tabIndex="4" runat="server" CssClass="textField" Width="39px" MaxLength="2" TextMaskType="msNumeric"
													onfocus="javascript:this.select();" NumberMaxValue="31" NumberMinValue="1" NumberPrecision="5"
													NumberScale="1"></cc1:mstextbox><FONT size="2">&nbsp; of the month</FONT></FONT></FONT></TD>
									<TD align="left"><FONT face="Tahoma">&nbsp;&nbsp; </FONT>
										<asp:radiobutton id="r_Monthly_AdjustToPrevious" runat="server" CssClass="tableLabel" Width="257px"
											Text="Adjust to previous business day" AutoPostBack="True" GroupName="Month"></asp:radiobutton></TD>
								</TR>
							</TABLE>
						</FIELDSET>
					</TD>
				</TR>
			</TABLE>
			<asp:label id="lblUpdatedBy" style="Z-INDEX: 115; LEFT: 427px; POSITION: absolute; TOP: 155px"
				runat="server"></asp:label></form>
	</body>
</HTML>
