<%@ Page language="c#" Codebehind="CustomerScheduledPickups.aspx.cs" AutoEventWireup="false" Inherits="com.ties.CustomerScheduledPickups" %>
<%@ Register TagPrefix="dbCombo" Namespace="Cambro.Web.DbCombo" Assembly="Cambro.Web.DbCombo" %>
<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<%@ Import Namespace="com.common.util" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Customer Scheduled Pickups</title>
		<LINK href="css/Styles.css" type="text/css" rel="stylesheet" name="stylesheet">
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<!--#INCLUDE FILE="msFormValidations.inc"-->
		<script language="javascript" src="Scripts/settingScrollPosition.js"></script>
	</HEAD>
	<body onkeypress="microsoftKeyPress()" onclick="GetScrollPosition();" onload="SetScrollPosition();"
		onunload="window.top.displayBanner.fnCloseAll(0);" MS_POSITIONING="FlowLayout">
		<form id="CustomerScheduledPickups" name="CustomerScheduledPickups" method="post" runat="server">
			<table id="Table2" width="95%" align="center" border="0">
				<tr>
					<td><asp:label id="lblMainTitle" runat="server" CssClass="mainTitleSize" Width="477px" Height="26px">Customer Scheduled Pickups</asp:label></td>
				</tr>
				<tr>
					<td>
						<table id="Table3" width="100%" border="0">
							<tr>
								<td><asp:button id="btnQry" runat="server" CssClass="queryButton" Width="61px" CausesValidation="False"
										Text="Query"></asp:button><asp:button id="btnExecQry" runat="server" CssClass="queryButton" Width="130px" CausesValidation="False"
										Text="Execute Query"></asp:button><asp:button id="btnInsert" runat="server" CssClass="queryButton" Width="62px" CausesValidation="False"
										Text="Insert"></asp:button><asp:button id="btnSave" runat="server" CssClass="queryButton" Width="66px" Text="Save"></asp:button><asp:button id="btnDelete" runat="server" CssClass="queryButton" Width="62px" CausesValidation="False"
										Text="Delete"></asp:button></td>
								<td align="right"><asp:button id="btnGoToFirstPage" runat="server" CssClass="queryButton" Width="24px" CausesValidation="False"
										Text="|<"></asp:button><asp:button id="btnPreviousPage" runat="server" CssClass="queryButton" Width="24px" CausesValidation="False"
										Text="<"></asp:button><asp:textbox id="txtGoToRec" tabIndex="8" runat="server" Width="23px" Height="19px" Enabled="False"></asp:textbox><asp:button id="btnNextPage" runat="server" CssClass="queryButton" Width="24px" CausesValidation="False"
										Text=">"></asp:button><asp:button id="btnGoToLastPage" runat="server" CssClass="queryButton" Width="24px" CausesValidation="False"
										Text=">|"></asp:button></td>
							</tr>
						</table>
						<asp:label id="lblErrorMsg" runat="server" CssClass="errorMsgColor" Width="566px" Height="19px"></asp:label><asp:validationsummary id="PageValidationSummary" runat="server" ShowMessageBox="True" ShowSummary="False"
							Visible="True"></asp:validationsummary></td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				</tr>
				<tr id="trMain" runat="server">
					<td>
						<fieldset><legend><asp:label id="lblSendInfo" CssClass="tableHeadingFieldset" Runat="server">Sender Information</asp:label></legend>
							<TABLE id="Table4" cellSpacing="1" cellPadding="1" width="95%" align="center" border="0">
								<tr>
									<td>
										<TABLE id="Table5" cellSpacing="1" cellPadding="1" width="100%" border="0">
											<TR>
												<TD><asp:requiredfieldvalidator id="validCustID" Runat="server" ControlToValidate="txtCustomerCode">*</asp:requiredfieldvalidator></TD>
												<TD><asp:label id="lblCustomerAccount" runat="server" CssClass="tableLabel" Width="108px" Height="22px">Customer Account</asp:label></TD>
												<TD><asp:textbox id="txtCustomerCode" tabIndex="5" runat="server" CssClass="textField" Width="160px"
														AutoPostBack="True" MaxLength="12"></asp:textbox>&nbsp;<asp:button id="btnDisplayCustDtls" tabIndex="16" runat="server" CssClass="searchButton" Width="21px"
														Height="19px" CausesValidation="False" Text="..."></asp:button></TD>
												<TD width="10%">&nbsp;</TD>
												<TD><asp:label id="Label2" runat="server" CssClass="tableLabel" Width="108px" Height="22px"> Customer Name</asp:label></TD>
												<TD><asp:textbox id="txtCustomerName" tabIndex="5" runat="server" CssClass="textField" Width="200px"
														MaxLength="200" ReadOnly="True"></asp:textbox></TD>
											</TR>
											<TR>
												<TD>&nbsp;</TD>
												<TD><asp:label id="lblReference" runat="server" CssClass="tableLabel" Width="108px" Height="22px">Reference</asp:label></TD>
												<TD><asp:textbox id="txtSendName" tabIndex="5" runat="server" CssClass="textField" Width="160px"
														AutoPostBack="True" MaxLength="100"></asp:textbox>&nbsp;<asp:button id="btnReference" tabIndex="16" runat="server" CssClass="searchButton" Width="21px"
														Height="19px" CausesValidation="False" Text="..."></asp:button></TD>
												<TD>&nbsp;</TD>
												<TD><asp:label id="Label3" runat="server" CssClass="tableLabel" Width="108px" Height="22px">Address 1</asp:label></TD>
												<TD><asp:textbox id="txtSendAddr1" tabIndex="5" runat="server" CssClass="textField" Width="190px"
														MaxLength="200" ReadOnly="True"></asp:textbox></TD>
											</TR>
											<TR>
												<TD></TD>
												<TD colSpan="2"><asp:checkbox id="chkPickHolidays" tabIndex="14" runat="server" CssClass="tableLabel" Width="185px"
														Height="16px" Text="Pickups on Public Holidays" AutoPostBack="True" TextAlign="Left"></asp:checkbox></TD>
												<TD></TD>
												<TD></TD>
												<TD>&nbsp;</TD>
											</TR>
										</TABLE>
									</td>
								</tr>
								<tr>
									<td align="left" id="trDbCombo">
										<TABLE id="Table1" cellSpacing="1" cellPadding="1" width="80%" border="0">
											<TR>
												<TD></TD>
												<TD>&nbsp;</TD>
												<TD align="center" colSpan="2"><asp:label id="Label10" runat="server" CssClass="tableLabel" Width="61px" Height="22px">1st Pickup</asp:label></TD>
												<TD></TD>
												<TD align="center" colSpan="2"><asp:label id="Label11" runat="server" CssClass="tableLabel" Width="71px" Height="22px">2nd Pickup</asp:label></TD>
												<TD></TD>
												<TD align="center" colSpan="2"><asp:label id="Label12" runat="server" CssClass="tableLabel" Width="75px" Height="22px">3rd Pickup</asp:label></TD>
												<TD width="20%">&nbsp;</TD>
												<TD></TD>
												<TD></TD>
												<TD><asp:label id="Label20" runat="server" CssClass="tableLabel" Width="45px" Height="22px">All</asp:label></TD>
											</TR>
											<TR>
												<TD align="center"></TD>
												<TD align="center"></TD>
												<TD align="center">
													<asp:label id="Label24" runat="server" Height="22px" Width="16px" CssClass="tableLabel">Route</asp:label></TD>
												<TD align="center">
													<asp:label id="Label25" runat="server" Height="22px" Width="9px" CssClass="tableLabel">Time</asp:label></TD>
												<TD align="center"></TD>
												<TD align="center">
													<asp:label id="Label26" runat="server" Height="22px" Width="6px" CssClass="tableLabel">Route</asp:label></TD>
												<TD align="center">
													<asp:label id="Label27" runat="server" Height="22px" Width="17px" CssClass="tableLabel">Time</asp:label></TD>
												<TD align="center"></TD>
												<TD align="center">
													<asp:label id="Label28" runat="server" Height="22px" Width="12px" CssClass="tableLabel">Route</asp:label></TD>
												<TD align="center">
													<asp:label id="Label29" runat="server" Height="22px" Width="25px" CssClass="tableLabel">Time</asp:label></TD>
												<TD align="center" width="20%"></TD>
												<TD align="center"></TD>
												<TD align="center"></TD>
												<TD align="center"></TD>
											</TR>
											<TR>
												<TD></TD>
												<TD><asp:label id="Label1" runat="server" CssClass="tableLabel" Width="108px" Height="22px">Monday</asp:label></TD>
												<TD>
													<DBCOMBO:DBCOMBO id="DbComboPathCodeMon1" tabIndex="14" Width="15px" Runat="server" AutoPostBack="True"
														TextBoxColumns="8" RegistrationKey=" " TextUpLevelSearchButton="v" ShowDbComboLink="False" ServerMethod="DbComboPathCodeSelect"></DBCOMBO:DBCOMBO></TD>
												<TD><cc1:mstextbox id="txtMon1" style="TEXT-ALIGN: right" runat="server" CssClass="textField" Width="80px"
														AutoPostBack="True" MaxLength="5" ReadOnly="false" TextMaskType="msTimeNoReturn" TextMaskString="99:99"></cc1:mstextbox></TD>
												<TD></TD>
												<TD>
													<DBCOMBO:DBCOMBO id="DbComboPathCodeMon2" tabIndex="14" Width="15px" CssClass="tableLabel" Runat="server"
														AutoPostBack="True" TextBoxColumns="8" RegistrationKey=" " TextUpLevelSearchButton="v" ShowDbComboLink="False"
														ServerMethod="DbComboPathCodeSelect"></DBCOMBO:DBCOMBO></TD>
												<TD><cc1:mstextbox id="txtMon2" style="TEXT-ALIGN: right" runat="server" CssClass="textField" Width="80px"
														AutoPostBack="True" MaxLength="5" ReadOnly="false" TextMaskType="msTimeNoReturn" TextMaskString="99:99"></cc1:mstextbox></TD>
												<TD></TD>
												<TD>
													<DBCOMBO:DBCOMBO id="DbComboPathCodeMon3" tabIndex="14" Width="15px" CssClass="tableLabel" Runat="server"
														AutoPostBack="True" TextBoxColumns="8" RegistrationKey=" " TextUpLevelSearchButton="v" ShowDbComboLink="False"
														ServerMethod="DbComboPathCodeSelect"></DBCOMBO:DBCOMBO></TD>
												<TD><cc1:mstextbox id="txtMon3" style="TEXT-ALIGN: right" runat="server" CssClass="textField" Width="80px"
														AutoPostBack="True" MaxLength="5" ReadOnly="false" TextMaskType="msTimeNoReturn" TextMaskString="99:99"></cc1:mstextbox></TD>
												<TD></TD>
												<TD><asp:label id="Label13" runat="server" CssClass="tableLabel" Width="60px" Height="22px">Monday</asp:label></TD>
												<TD></TD>
												<TD><asp:checkbox id="chkMon" runat="server" Text=" " AutoPostBack="True"></asp:checkbox></TD>
											</TR>
											<TR>
												<TD></TD>
												<TD><asp:label id="Label4" runat="server" CssClass="tableLabel" Width="108px" Height="22px">Tuesday</asp:label></TD>
												<TD>
													<DBCOMBO:DBCOMBO id="DbComboPathCodeTue1" tabIndex="14" Width="15px" CssClass="tableLabel" Runat="server"
														AutoPostBack="True" TextBoxColumns="8" RegistrationKey=" " TextUpLevelSearchButton="v" ShowDbComboLink="False"
														ServerMethod="DbComboPathCodeSelect"></DBCOMBO:DBCOMBO></TD>
												<TD><cc1:mstextbox id="txtTue1" style="TEXT-ALIGN: right" runat="server" CssClass="textField" Width="80px"
														AutoPostBack="True" MaxLength="5" ReadOnly="false" TextMaskType="msTimeNoReturn" TextMaskString="99:99"></cc1:mstextbox></TD>
												<TD></TD>
												<TD>
													<DBCOMBO:DBCOMBO id="DbComboPathCodeTue2" tabIndex="14" Width="15px" CssClass="tableLabel" Runat="server"
														AutoPostBack="True" TextBoxColumns="8" RegistrationKey=" " TextUpLevelSearchButton="v" ShowDbComboLink="False"
														ServerMethod="DbComboPathCodeSelect"></DBCOMBO:DBCOMBO></TD>
												<TD><cc1:mstextbox id="txtTue2" style="TEXT-ALIGN: right" runat="server" CssClass="textField" Width="80px"
														AutoPostBack="True" MaxLength="5" ReadOnly="false" TextMaskType="msTimeNoReturn" TextMaskString="99:99"></cc1:mstextbox></TD>
												<TD></TD>
												<TD>
													<DBCOMBO:DBCOMBO id="DbComboPathCodeTue3" tabIndex="14" Width="15px" CssClass="tableLabel" Runat="server"
														AutoPostBack="True" TextBoxColumns="8" RegistrationKey=" " TextUpLevelSearchButton="v" ShowDbComboLink="False"
														ServerMethod="DbComboPathCodeSelect"></DBCOMBO:DBCOMBO></TD>
												<TD><cc1:mstextbox id="txtTue3" style="TEXT-ALIGN: right" runat="server" CssClass="textField" Width="80px"
														AutoPostBack="True" MaxLength="5" ReadOnly="false" TextMaskType="msTimeNoReturn" TextMaskString="99:99"></cc1:mstextbox></TD>
												<TD></TD>
												<TD><asp:label id="Label14" runat="server" CssClass="tableLabel" Width="61px" Height="22px">Tuesday</asp:label></TD>
												<TD></TD>
												<TD><asp:checkbox id="chkTue" runat="server" Text=" " AutoPostBack="True"></asp:checkbox></TD>
											</TR>
											<TR>
												<TD></TD>
												<TD><asp:label id="Label5" runat="server" CssClass="tableLabel" Width="108px" Height="22px">Wednesday</asp:label></TD>
												<TD>
													<DBCOMBO:DBCOMBO id="DbComboPathCodeWed1" tabIndex="14" Width="15px" CssClass="tableLabel" Runat="server"
														AutoPostBack="True" TextBoxColumns="8" RegistrationKey=" " TextUpLevelSearchButton="v" ShowDbComboLink="False"
														ServerMethod="DbComboPathCodeSelect"></DBCOMBO:DBCOMBO></TD>
												<TD><cc1:mstextbox id="txtWed1" style="TEXT-ALIGN: right" runat="server" CssClass="textField" Width="80px"
														AutoPostBack="True" MaxLength="5" ReadOnly="false" TextMaskType="msTimeNoReturn" TextMaskString="99:99"></cc1:mstextbox></TD>
												<TD></TD>
												<TD>
													<DBCOMBO:DBCOMBO id="DbComboPathCodeWed2" tabIndex="14" Width="15px" CssClass="tableLabel" Runat="server"
														AutoPostBack="True" TextBoxColumns="8" RegistrationKey=" " TextUpLevelSearchButton="v" ShowDbComboLink="False"
														ServerMethod="DbComboPathCodeSelect"></DBCOMBO:DBCOMBO></TD>
												<TD><cc1:mstextbox id="txtWed2" style="TEXT-ALIGN: right" runat="server" CssClass="textField" Width="80px"
														AutoPostBack="True" MaxLength="5" ReadOnly="false" TextMaskType="msTimeNoReturn" TextMaskString="99:99"></cc1:mstextbox></TD>
												<TD></TD>
												<TD>
													<DBCOMBO:DBCOMBO id="DbComboPathCodeWed3" tabIndex="14" Width="15px" CssClass="tableLabel" Runat="server"
														AutoPostBack="True" TextBoxColumns="8" RegistrationKey=" " TextUpLevelSearchButton="v" ShowDbComboLink="False"
														ServerMethod="DbComboPathCodeSelect"></DBCOMBO:DBCOMBO></TD>
												<TD><cc1:mstextbox id="txtWed3" style="TEXT-ALIGN: right" runat="server" CssClass="textField" Width="80px"
														AutoPostBack="True" MaxLength="5" ReadOnly="false" TextMaskType="msTimeNoReturn" TextMaskString="99:99"></cc1:mstextbox></TD>
												<TD></TD>
												<TD><asp:label id="Label15" runat="server" CssClass="tableLabel" Width="68px" Height="22px">Wednesday</asp:label></TD>
												<TD></TD>
												<TD><asp:checkbox id="chkWed" runat="server" Text=" " AutoPostBack="True"></asp:checkbox></TD>
											</TR>
											<TR>
												<TD></TD>
												<TD><asp:label id="Label6" runat="server" CssClass="tableLabel" Width="108px" Height="22px">Thursday</asp:label></TD>
												<TD>
													<DBCOMBO:DBCOMBO id="DbComboPathCodeThu1" tabIndex="14" Width="15px" CssClass="tableLabel" Runat="server"
														AutoPostBack="True" TextBoxColumns="8" RegistrationKey=" " TextUpLevelSearchButton="v" ShowDbComboLink="False"
														ServerMethod="DbComboPathCodeSelect"></DBCOMBO:DBCOMBO></TD>
												<TD><cc1:mstextbox id="txtThu1" style="TEXT-ALIGN: right" runat="server" CssClass="textField" Width="80px"
														AutoPostBack="True" MaxLength="5" ReadOnly="false" TextMaskType="msTimeNoReturn" TextMaskString="99:99"></cc1:mstextbox></TD>
												<TD></TD>
												<TD>
													<DBCOMBO:DBCOMBO id="DbComboPathCodeThu2" tabIndex="14" Width="15px" CssClass="tableLabel" Runat="server"
														AutoPostBack="True" TextBoxColumns="8" RegistrationKey=" " TextUpLevelSearchButton="v" ShowDbComboLink="False"
														ServerMethod="DbComboPathCodeSelect"></DBCOMBO:DBCOMBO></TD>
												<TD><cc1:mstextbox id="txtThu2" style="TEXT-ALIGN: right" runat="server" CssClass="textField" Width="80px"
														AutoPostBack="True" MaxLength="5" ReadOnly="false" TextMaskType="msTimeNoReturn" TextMaskString="99:99"></cc1:mstextbox></TD>
												<TD></TD>
												<TD>
													<DBCOMBO:DBCOMBO id="DbComboPathCodeThu3" tabIndex="14" Width="15px" CssClass="tableLabel" Runat="server"
														AutoPostBack="True" TextBoxColumns="8" RegistrationKey=" " TextUpLevelSearchButton="v" ShowDbComboLink="False"
														ServerMethod="DbComboPathCodeSelect"></DBCOMBO:DBCOMBO></TD>
												<TD><cc1:mstextbox id="txtThu3" style="TEXT-ALIGN: right" runat="server" CssClass="textField" Width="80px"
														AutoPostBack="True" MaxLength="5" ReadOnly="false" TextMaskType="msTimeNoReturn" TextMaskString="99:99"></cc1:mstextbox></TD>
												<TD></TD>
												<TD><asp:label id="Label16" runat="server" CssClass="tableLabel" Width="54px" Height="22px">Thursday</asp:label></TD>
												<TD></TD>
												<TD><asp:checkbox id="chkThu" runat="server" Text=" " AutoPostBack="True"></asp:checkbox></TD>
											</TR>
											<TR>
												<TD></TD>
												<TD><asp:label id="Label7" runat="server" CssClass="tableLabel" Width="108px" Height="22px">Friday</asp:label></TD>
												<TD>
													<DBCOMBO:DBCOMBO id="DbComboPathCodeFri1" tabIndex="14" Width="15px" CssClass="tableLabel" Runat="server"
														AutoPostBack="True" TextBoxColumns="8" RegistrationKey=" " TextUpLevelSearchButton="v" ShowDbComboLink="False"
														ServerMethod="DbComboPathCodeSelect"></DBCOMBO:DBCOMBO></TD>
												<TD><cc1:mstextbox id="txtFri1" style="TEXT-ALIGN: right" runat="server" CssClass="textField" Width="80px"
														AutoPostBack="True" MaxLength="5" ReadOnly="false" TextMaskType="msTimeNoReturn" TextMaskString="99:99"></cc1:mstextbox></TD>
												<TD></TD>
												<TD>
													<DBCOMBO:DBCOMBO id="DbComboPathCodeFri2" tabIndex="14" Width="15px" CssClass="tableLabel" Runat="server"
														AutoPostBack="True" TextBoxColumns="8" RegistrationKey=" " TextUpLevelSearchButton="v" ShowDbComboLink="False"
														ServerMethod="DbComboPathCodeSelect"></DBCOMBO:DBCOMBO></TD>
												<TD><cc1:mstextbox id="txtFri2" style="TEXT-ALIGN: right" runat="server" CssClass="textField" Width="80px"
														AutoPostBack="True" MaxLength="5" ReadOnly="false" TextMaskType="msTimeNoReturn" TextMaskString="99:99"></cc1:mstextbox></TD>
												<TD></TD>
												<TD>
													<DBCOMBO:DBCOMBO id="DbComboPathCodeFri3" tabIndex="14" Width="15px" CssClass="tableLabel" Runat="server"
														AutoPostBack="True" TextBoxColumns="8" RegistrationKey=" " TextUpLevelSearchButton="v" ShowDbComboLink="False"
														ServerMethod="DbComboPathCodeSelect"></DBCOMBO:DBCOMBO></TD>
												<TD><cc1:mstextbox id="txtFri3" style="TEXT-ALIGN: right" runat="server" CssClass="textField" Width="80px"
														AutoPostBack="True" MaxLength="5" ReadOnly="false" TextMaskType="msTimeNoReturn" TextMaskString="99:99"></cc1:mstextbox></TD>
												<TD></TD>
												<TD><asp:label id="Label17" runat="server" CssClass="tableLabel" Width="64px" Height="22px">Friday</asp:label></TD>
												<TD></TD>
												<TD><asp:checkbox id="chkFri" runat="server" Text=" " AutoPostBack="True"></asp:checkbox></TD>
											</TR>
											<TR>
												<TD style="HEIGHT: 24px"></TD>
												<TD style="HEIGHT: 24px"><asp:label id="Label8" runat="server" CssClass="tableLabel" Width="108px" Height="22px">Saturday</asp:label></TD>
												<TD style="HEIGHT: 24px">
													<DBCOMBO:DBCOMBO id="DbComboPathCodeSat1" tabIndex="14" Width="15px" CssClass="tableLabel" Runat="server"
														AutoPostBack="True" TextBoxColumns="8" RegistrationKey=" " TextUpLevelSearchButton="v" ShowDbComboLink="False"
														ServerMethod="DbComboPathCodeSelect"></DBCOMBO:DBCOMBO></TD>
												<TD style="HEIGHT: 24px"><cc1:mstextbox id="txtSat1" style="TEXT-ALIGN: right" runat="server" CssClass="textField" Width="80px"
														AutoPostBack="True" MaxLength="5" ReadOnly="false" TextMaskType="msTimeNoReturn" TextMaskString="99:99"></cc1:mstextbox></TD>
												<TD style="HEIGHT: 24px"></TD>
												<TD style="HEIGHT: 24px">
													<DBCOMBO:DBCOMBO id="DbComboPathCodeSat2" tabIndex="14" Width="15px" CssClass="tableLabel" Runat="server"
														AutoPostBack="True" TextBoxColumns="8" RegistrationKey=" " TextUpLevelSearchButton="v" ShowDbComboLink="False"
														ServerMethod="DbComboPathCodeSelect"></DBCOMBO:DBCOMBO></TD>
												<TD style="HEIGHT: 24px"><cc1:mstextbox id="txtSat2" style="TEXT-ALIGN: right" runat="server" CssClass="textField" Width="80px"
														AutoPostBack="True" MaxLength="5" ReadOnly="false" TextMaskType="msTimeNoReturn" TextMaskString="99:99"></cc1:mstextbox></TD>
												<TD style="HEIGHT: 24px"></TD>
												<TD style="HEIGHT: 24px">
													<DBCOMBO:DBCOMBO id="DbComboPathCodeSat3" tabIndex="14" Width="15px" CssClass="tableLabel" Runat="server"
														AutoPostBack="True" TextBoxColumns="8" RegistrationKey=" " TextUpLevelSearchButton="v" ShowDbComboLink="False"
														ServerMethod="DbComboPathCodeSelect"></DBCOMBO:DBCOMBO></TD>
												<TD style="HEIGHT: 24px"><cc1:mstextbox id="txtSat3" style="TEXT-ALIGN: right" runat="server" CssClass="textField" Width="80px"
														AutoPostBack="True" MaxLength="5" ReadOnly="false" TextMaskType="msTimeNoReturn" TextMaskString="99:99"></cc1:mstextbox></TD>
												<TD style="HEIGHT: 24px"></TD>
												<TD style="HEIGHT: 24px"><asp:label id="Label18" runat="server" CssClass="tableLabel" Width="49px" Height="22px">Saturday</asp:label></TD>
												<TD style="HEIGHT: 24px"></TD>
												<TD style="HEIGHT: 24px"><asp:checkbox id="chkSat" runat="server" Text=" " AutoPostBack="True"></asp:checkbox></TD>
											</TR>
											<TR>
												<TD></TD>
												<TD><asp:label id="Label9" runat="server" CssClass="tableLabel" Width="108px" Height="22px">Sunday</asp:label></TD>
												<TD>
													<DBCOMBO:DBCOMBO id="DbComboPathCodeSun1" tabIndex="14" Width="15px" CssClass="tableLabel" Runat="server"
														AutoPostBack="True" TextBoxColumns="8" RegistrationKey=" " TextUpLevelSearchButton="v" ShowDbComboLink="False"
														ServerMethod="DbComboPathCodeSelect"></DBCOMBO:DBCOMBO></TD>
												<TD><cc1:mstextbox id="txtSun1" style="TEXT-ALIGN: right" runat="server" CssClass="textField" Width="80px"
														AutoPostBack="True" MaxLength="5" ReadOnly="false" TextMaskType="msTimeNoReturn" TextMaskString="99:99"></cc1:mstextbox></TD>
												<TD></TD>
												<TD>
													<DBCOMBO:DBCOMBO id="DbComboPathCodeSun2" tabIndex="14" Width="15px" CssClass="tableLabel" Runat="server"
														AutoPostBack="True" TextBoxColumns="8" RegistrationKey=" " TextUpLevelSearchButton="v" ShowDbComboLink="False"
														ServerMethod="DbComboPathCodeSelect"></DBCOMBO:DBCOMBO></TD>
												<TD><cc1:mstextbox id="txtSun2" style="TEXT-ALIGN: right" runat="server" CssClass="textField" Width="80px"
														AutoPostBack="True" MaxLength="5" ReadOnly="false" TextMaskType="msTimeNoReturn" TextMaskString="99:99"></cc1:mstextbox></TD>
												<TD></TD>
												<TD>
													<DBCOMBO:DBCOMBO id="DbComboPathCodeSun3" tabIndex="14" Width="15px" CssClass="tableLabel" Runat="server"
														AutoPostBack="True" TextBoxColumns="8" RegistrationKey=" " TextUpLevelSearchButton="v" ShowDbComboLink="False"
														ServerMethod="DbComboPathCodeSelect"></DBCOMBO:DBCOMBO></TD>
												<TD><cc1:mstextbox id="txtSun3" style="TEXT-ALIGN: right" runat="server" CssClass="textField" Width="80px"
														AutoPostBack="True" MaxLength="5" ReadOnly="false" TextMaskType="msTimeNoReturn" TextMaskString="99:99"></cc1:mstextbox></TD>
												<TD></TD>
												<TD><asp:label id="Label19" runat="server" CssClass="tableLabel" Width="47px" Height="22px">Sunday</asp:label></TD>
												<TD></TD>
												<TD><asp:checkbox id="chkSun" runat="server" Text=" " AutoPostBack="True"></asp:checkbox></TD>
											</TR>
										</TABLE>
									</td>
								</tr>
							</TABLE>
						</fieldset>
					</td>
				</tr>
				<tr id="trConfirm" runat="server">
					<td align="center">
						<table id="Table7" cellSpacing="1" cellPadding="1" width="55%" align="center" border="0">
							<tr>
								<td>
									<fieldset><legend><asp:label id="Label21" CssClass="tableHeadingFieldset" Runat="server">Confirmation</asp:label></legend>
										<table id="Table6" cellSpacing="1" cellPadding="1" width="55%" align="center" border="0">
											<tr>
												<td align="left"><span><asp:label id="lblConfirmation" runat="server" Width="401px" Height="46px">Changes to data have been made. Would you like to save these changes?</asp:label></span></td>
											</tr>
											<tr>
												<td>&nbsp;</td>
											</tr>
											<tr>
												<td align="center"><asp:button id="btnToSaveChanges" runat="server" CssClass="queryButton" Text="Yes"></asp:button><asp:button id="btnNotToSave" runat="server" CssClass="queryButton" CausesValidation="False"
														Text="No"></asp:button><asp:button id="btnToCancel" runat="server" CssClass="queryButton" CausesValidation="False"
														Text="Cancel"></asp:button></td>
											</tr>
											<tr>
												<td>&nbsp;</td>
											</tr>
										</table>
									</fieldset>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr id="trDelete" runat="server">
					<td align="center">
						<table id="Table7" cellSpacing="1" cellPadding="1" width="55%" align="center" border="0">
							<tr>
								<td>
									<fieldset><legend><asp:label id="Label22" CssClass="tableHeadingFieldset" Runat="server">Confirmation</asp:label></legend>
										<table id="Table6" cellSpacing="1" cellPadding="1" width="55%" align="center" border="0">
											<tr>
												<td align="left"><span><asp:label id="Label23" runat="server" Width="401px" Height="46px">Record Deleted cannot be reverted back. Confirm Deletion ?</asp:label></span></td>
											</tr>
											<tr>
												<td>&nbsp;</td>
											</tr>
											<tr>
												<td align="center"><asp:button id="btnComfirmDel" runat="server" CssClass="queryButton" Text="Yes"></asp:button><asp:button id="btnNoConfirmDel" runat="server" CssClass="queryButton" CausesValidation="False"
														Text="No"></asp:button></td>
											</tr>
											<tr>
												<td>&nbsp;</td>
											</tr>
										</table>
									</fieldset>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
