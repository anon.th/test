using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.common.util;
using com.common.classes;
using com.common.DAL;
using com.ties.classes;   
using System.Text;
using com.common.applicationpages;

//HC Return Task
using com.ties.DAL;
//HC Return Task

namespace TIES.WebUI
{
	/// <summary>
	/// Summary description for RecipientReferencesPopup.
	/// </summary>
	public class RecipientReferencesPopup : BasePopupPage
		//public class RecipientReferencesPopup : System.Web.UI.Page
	{
		#region Web Form Designer generated code

		protected System.Web.UI.WebControls.Label lblVASCode;
		protected System.Web.UI.WebControls.Button btnSearch;

		protected System.Web.UI.WebControls.Label lblCountry;
		protected System.Web.UI.WebControls.DataGrid dgReferences;
		protected System.Web.UI.WebControls.Label Label1;
		protected System.Web.UI.WebControls.Label Label2;
		protected System.Web.UI.WebControls.Label Label3;
		protected System.Web.UI.WebControls.TextBox txtTelephone;
		protected System.Web.UI.WebControls.TextBox txtName;
		protected System.Web.UI.WebControls.TextBox txtPostalCode;
		protected System.Web.UI.WebControls.TextBox txtContactPerson;

		protected System.Web.UI.WebControls.Label lblReferences;
		protected System.Web.UI.WebControls.TextBox txtGridFax;
		protected System.Web.UI.WebControls.RequiredFieldValidator re	;
		protected System.Web.UI.WebControls.RequiredFieldValidator ReqTxtPhone;
		protected System.Web.UI.WebControls.ValidationSummary validateSum;
		protected System.Web.UI.WebControls.Button btnQuery;
		protected System.Web.UI.WebControls.Button btnOk;
		protected System.Web.UI.WebControls.Button btnInsert;
		protected System.Web.UI.WebControls.TextBox txtFax;

		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			base.OnInit(e);
			InitializeComponent();
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnQuery.Click += new System.EventHandler(this.btnQuery_Click);
			this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
			this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
			this.btnInsert.Click += new System.EventHandler(this.btnInsert_Click);
			this.dgReferences.ItemCreated += new System.Web.UI.WebControls.DataGridItemEventHandler(this.dgReferences_ItemCreated);
			this.dgReferences.CancelCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dgReferences_CancelCommand);
			this.dgReferences.EditCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dgReferences_EditCommand);
			this.dgReferences.UpdateCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dgReferences_UpdateCommand);
			this.dgReferences.DeleteCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dgReferences_DeleteCommand);
			this.dgReferences.SelectedIndexChanged += new System.EventHandler(this.dgReferences_SelectedIndexChanged);
			this.Load += new System.EventHandler(this.Page_Load);

		}

		#endregion
	
		String strAppID = null;
		String strEnterpriseID = null;
		String strFormID = "";
		String strTelephoneNumber = "";
		String strESTDELDT = "";	//HC Return Task
		String strCON = "";			//HC Return Task
		String strBOOKINGNO = "";	//HC Return Task

		SessionDS m_sdsReferences = null;

		String RecpTelephone = "";
		String RecpName = "";
		String RecpAddress1 = "";
		String RecpAddress2 = "";
		String RecpPostalCode = "";


		String RecpContactPerson = "";
		String RecpFax = "";

		//Client Control Name
		String CIDTelephone = "";
		String CIDName = "";
		String CIDAddress1 = "";
		String CIDAddress2 = "";
		String CIDPostalCode = "";
		String CIDCity = "";
		String CIDState = "";
		String CIDContactPerson = "";
		String CIDFax = "";
		String CIDRouteCode = "";
		String CIDDestination = "";
		String CIDESTHCDT = "";//HC Return Task

		protected AccessRights accRight = null;

		private void Page_Load(object sender, System.EventArgs e)
		{
			
			strAppID = utility.GetAppID();
			strEnterpriseID = utility.GetEnterpriseID();
			strFormID = Request.Params["FORMID"];
			strTelephoneNumber = Request.Params["TELEPHONENUMBER"]; 
			strTelephoneNumber = strTelephoneNumber.Replace("Flat", "#");
			strESTDELDT = Request.Params["ESTDELDT"];		//HC Return Task 
			strCON = Request.Params["STRCON"];				//HC Return Task 
			strBOOKINGNO = Request.Params["STRBOOKINGNO"];	//HC Return Task 

			CIDTelephone =  Request.Params["CIDTelephone"];
			CIDName = Request.Params["CIDName"];
			CIDAddress1 = Request.Params["CIDAddress1"];
			CIDAddress2 = Request.Params["CIDAddress2"];
			CIDPostalCode = Request.Params["CIDPostalCode"];
			CIDCity = Request.Params["CIDCity"];
			CIDState = Request.Params["CIDState"];
			CIDContactPerson = Request.Params["CIDContactPerson"];
			CIDFax = Request.Params["CIDFax"];
			CIDRouteCode = Request.Params["CIDRouteCode"];
			CIDDestination = Request.Params["CIDDestination"];
			CIDESTHCDT = Request.Params["CIDESTHCDT"];//HC Return Task

			

			//event javascript check number
			txtTelephone.Attributes.Add("onkeypress","if (window.event.keyCode >= 48 && window.event.keyCode <= 57) return ture; else return false;");

			//event javascript check upper case for postal code------PAK==21 June 2013
			this.txtPostalCode.Attributes.Add("onKeyPress", "return UpperMaskSpecialWithHyphen(this)");

			if(!Page.IsPostBack)
			{
				txtTelephone.Text = strTelephoneNumber;
				ViewState["SCModePop"] = com.ties.ScreenMode.None;
				ViewState["SCOperationPop"] = com.ties.ScreenMode.None;
				Utility.EnableButton(ref btnInsert,ButtonType.Insert,true,accRight);


				if (strTelephoneNumber.Trim() != "")
				{
					SearchData();
				}
				else
				{
					m_sdsReferences = GetEmptyReferencesDS(0);
				}
				
				BindGrid();
			}
			else
			{
				validateSum.HeaderText = Utility.GetLanguageText(ResourceType.UserMessage, "PLS_ENTER_MISS_FIELD", utility.GetUserCulture());
				m_sdsReferences = (SessionDS)ViewState["REFERENCES_DS"];
			}
		}

		public static SessionDS GetEmptyReferencesDS(int iNumRows)
		{
			DataTable dtReferences = new DataTable();
			dtReferences.Columns.Add(new DataColumn("telephone", typeof(string)));
			dtReferences.Columns.Add(new DataColumn("reference_name", typeof(string)));
			dtReferences.Columns.Add(new DataColumn("address1", typeof(string)));
			dtReferences.Columns.Add(new DataColumn("address2", typeof(string)));
			dtReferences.Columns.Add(new DataColumn("zipcode", typeof(string)));
			dtReferences.Columns.Add(new DataColumn("fax", typeof(string)));
			dtReferences.Columns.Add(new DataColumn("contactperson", typeof(string)));

			for(int i=0; i < iNumRows; i++)
			{
				DataRow drEach = dtReferences.NewRow();
				drEach[0] = "";
				drEach[1] = "";
				drEach[2] = "";
				drEach[3] = "";
				drEach[4] = "";
				drEach[5] = "";
				drEach[6] = "";
				dtReferences.Rows.Add(drEach);
			}

			DataSet dsReferences = new DataSet();
			dsReferences.Tables.Add(dtReferences);

			SessionDS sessionDS = new SessionDS();
			sessionDS.ds = dsReferences;
			sessionDS.DataSetRecSize = dsReferences.Tables[0].Rows.Count;
			sessionDS.QueryResultMaxSize = sessionDS.DataSetRecSize;

			return  sessionDS;
	
		}


		private void BindGrid()
		{
			dgReferences.VirtualItemCount = System.Convert.ToInt32(m_sdsReferences.QueryResultMaxSize);
			dgReferences.DataSource = m_sdsReferences.ds;
			dgReferences.DataBind();
			ViewState["REFERENCES_DS"] = m_sdsReferences;
		}


		private SessionDS GetReferencesDS(int iCurrent, int iDSRecSize, String strQuery)
		{
			SessionDS sessionDS = null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("RecipientReferencesPopup.cs","GetReferencesDS","EDB101","DbConnection object is null!!");
				return sessionDS;
			}

			sessionDS = dbCon.ExecuteQuery(strQuery, iCurrent, iDSRecSize,"References");

			return  sessionDS;	
		}


		private void ShowCurrentPage()
		{
			int iStartIndex = dgReferences.CurrentPageIndex * dgReferences.PageSize;
			String sqlQuery = (String)ViewState["SQL_QUERY"];
			m_sdsReferences = GetReferencesDS(iStartIndex, dgReferences.PageSize, sqlQuery);
			decimal pgCnt = (m_sdsReferences.QueryResultMaxSize - 1)/dgReferences.PageSize;
			if(pgCnt < dgReferences.CurrentPageIndex)
			{
				dgReferences.CurrentPageIndex = System.Convert.ToInt32(pgCnt);
			}
			dgReferences.SelectedIndex = -1;
			dgReferences.EditItemIndex = -1;

			BindGrid();
			getPageControls(Page);
		}


		#region "Control Event"

		private void btnOk_Click(object sender, System.EventArgs e)
		{
			String sScript = "";
			sScript += "<script language=javascript>";
			sScript += "  window.close();";
			sScript += "</script>";
			Response.Write(sScript);
		}


		private void btnSearch_Click(object sender, System.EventArgs e)
		{
			lblReferences.Text ="";
			SearchData();
		}


		private void SearchData()
		{
			
			String strTelephone = txtTelephone.Text.ToString().Trim();
			String strName = txtName.Text.ToString().Trim();
			String strPostalCode = txtPostalCode.Text.ToString().Trim();
			String strContactPerson = txtContactPerson.Text.ToString().Trim();
			String strFax = txtFax.Text.ToString().Trim();

			StringBuilder strQry = new StringBuilder();
			strQry.Append("SELECT telephone, reference_name, address1, address2, zipcode, fax, contactperson, telephone as current_telephone  ");
			strQry.Append("FROM [References] ");
			strQry.Append("WHERE applicationid='");
			strQry.Append(strAppID);
			strQry.Append("' and enterpriseid = '");
			strQry.Append(strEnterpriseID);
			strQry.Append("' ");

			if(strTelephone != null && strTelephone != "")
			{
				strQry.Append(" and telephone like N'%");
				strQry.Append(Utility.ReplaceSingleQuote(strTelephone));
				strQry.Append("%' ");
			}

			if(strName != null && strName != "")
			{
				strQry.Append(" and reference_name like N'%");
				strQry.Append(Utility.ReplaceSingleQuote(strName));
				strQry.Append("%' ");
			}

			if(strPostalCode != null && strPostalCode != "")
			{
				strQry.Append(" and zipcode like N'%");
				strQry.Append(Utility.ReplaceSingleQuote(strPostalCode));
				strQry.Append("%' ");
			}

			if(strContactPerson != null && strContactPerson != "")
			{
				strQry.Append(" and contactperson like N'%");
				strQry.Append(Utility.ReplaceSingleQuote(strContactPerson));
				strQry.Append("%' ");
			}

			if(strFax != null && strFax != "")
			{
				strQry.Append(" and fax like N'%");
				strQry.Append(Utility.ReplaceSingleQuote(strFax));
				strQry.Append("%' ");
			}
			
			String strSQLQuery = strQry.ToString();
			ViewState["SQL_QUERY"] = strSQLQuery;
			dgReferences.CurrentPageIndex = 0;
			ShowCurrentPage();
		}

		/*private void UpdateRecpReferences(String strAppID, String strEnterpriseID, String telephone, String name, 
			String address1, String address2, String postalCode, String fax, String contactPerson)
		{
			IDbCommand dbCom = null;

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(strTelephone!=null && strTelephone!="")
			{
				String strSQLQuery = "UPDATE [References] SET telephone = '"+Utility.ReplaceSingleQuote(telephone)+"', reference_name = N'"+Utility.ReplaceSingleQuote(name)+"' , address1= N'"+Utility.ReplaceSingleQuote(address1)+"' , address2= N'"+Utility.ReplaceSingleQuote(address2)+"', ";
				strSQLQuery += " zipcode = '"+postalCode+"', fax = '"+fax+"', contactperson = '"+contactPerson+"' ";
				strSQLQuery += "where applicationid = "+"'"+strAppID+"' and enterpriseid = '"+strEnterpriseID+"' and telephone = '"+Utility.ReplaceSingleQuote(telephone)+"' "; 
	
				dbCom = dbCon.CreateCommand(strSQLQuery,CommandType.Text);
				if(dbCon.ExecuteNonQuery(dbCom)>0)
					return true;
				else
				{
					//dbCon.ExecuteNonQuery(
				}
			}
		}
*/
		private void dgReferences_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			int iSelIndex = dgReferences.SelectedIndex;
			
			ViewState["SCModePop"] = com.ties.ScreenMode.ExecuteQuery;
			//DataGridItem dgRow = dgReferences.Items[iSelIndex];
			DataRow drEach = m_sdsReferences.ds.Tables[0].Rows[iSelIndex];

			RecpTelephone = drEach["telephone"].ToString().Trim().Replace("&nbsp;", "");
			RecpName = drEach["reference_name"].ToString().Trim().Replace("&nbsp;", "");
			RecpAddress1 = drEach["address1"].ToString().Trim().Replace("&nbsp;", "");
			RecpAddress2 = drEach["address2"].ToString().Trim().Replace("&nbsp;", "");
			RecpPostalCode = drEach["zipcode"].ToString().Trim().Replace("&nbsp;", "");
			RecpFax = drEach["fax"].ToString().Trim().Replace("&nbsp;", "");
			RecpContactPerson = drEach["contactperson"].ToString().Trim().Replace("&nbsp;", "");

			//Label lbltemp = ((Label)dgRow.FindControl("lblGridTelephone")).Text;


			//				if(lbltemp != null)
			//				{
			//					string xx = lbltemp.Text;
			//				}


			//			foreach (TableRow r in Table1.Rows)
			//				foreach (TableCell c in r.Cells)
			//					c.ApplyStyle(tableStyle);
			//			dgRow.Controls.Count();


			//string xx = dgReferences.Items[0].Cells[4].Controls[1].Text;
			//dgReferences.Items[0].Cells[4].Controls[1].Text

			//	RecpTelephone = dgReferences.Items[0].Cells[3].Controls[1].Text;
			//			RecpName = dgRow.Cells[4].Text.ToString().Trim().Replace("&nbsp;", "");
			//			RecpAddress1 = dgRow.Cells[5].Text.ToString().Trim().Replace("&nbsp;", "");
			//			RecpAddress2 = dgRow.Cells[6].Text.ToString().Trim().Replace("&nbsp;", "");
			//			RecpPostalCode = dgRow.Cells[7].Text.ToString().Trim().Replace("&nbsp;", "");
			//			RecpFax = dgRow.Cells[8].Text.ToString().Trim().Replace("&nbsp;", "");
			//			RecpContactPerson = dgRow.Cells[9].Text.ToString().Trim().Replace("&nbsp;", "");
			/*
						RecpTelephone = dgRow.Cells[3].Controls[1].Text.ToString().Trim().Replace("&nbsp;", "");
						RecpName = dgRow.Cells[4].Text.ToString().Trim().Replace("&nbsp;", "");
						RecpAddress1 = dgRow.Cells[5].Text.ToString().Trim().Replace("&nbsp;", "");
						RecpAddress2 = dgRow.Cells[6].Text.ToString().Trim().Replace("&nbsp;", "");
						RecpPostalCode = dgRow.Cells[7].Text.ToString().Trim().Replace("&nbsp;", "");
						RecpFax = dgRow.Cells[8].Text.ToString().Trim().Replace("&nbsp;", "");
						RecpContactPerson = dgRow.Cells[9].Text.ToString().Trim().Replace("&nbsp;", "");
			*/
			String strCountry = "";
			String strState = "";
			String routeCode = "";
			String strStateCode = "";//HC Return Task

			if (strFormID=="DomesticShipment")

			{
				Zipcode zipCode = new Zipcode();
				zipCode.Populate(strAppID,strEnterpriseID, RecpPostalCode);
				strCountry = zipCode.Country;
				zipCode.Populate(strAppID,strEnterpriseID, strCountry, RecpPostalCode); 
				strState=zipCode.StateName;
				routeCode=zipCode.DeliveryRoute;
	
				String sScript = "";
				sScript += "<script language=javascript>";
				sScript += "  window.opener."+strFormID+"."+CIDTelephone+".value = \""+RecpTelephone+"\";";
//				sScript += "  window.opener."+strFormID+"."+CIDName+".value = \""+RecpName+"\";";
//				sScript += "  window.opener."+strFormID+"."+CIDAddress1+".value = \""+RecpAddress1+"\";";
//				sScript += "  window.opener."+strFormID+"."+CIDAddress2+".value = \""+RecpAddress2+"\";";
//				//sScript += "  window.opener."+strFormID+"."+CIDPostalCode+".value = \""+RecpPostalCode+"\";";
//				//sScript += "  window.opener."+strFormID+"."+CIDCity+".value = \""+strState+"\";";
//				sScript += "  window.opener."+strFormID+"."+CIDState+".value = \""+strCountry+"\";";
//				sScript += "  window.opener."+strFormID+"."+CIDContactPerson+".value = \""+RecpContactPerson+"\";";
//				sScript += "  window.opener."+strFormID+"."+CIDFax+".value = \""+RecpFax+"\";";
//				sScript += "  window.opener."+strFormID+"."+CIDRouteCode+".value = \""+routeCode+"\";";
//				sScript += "  window.opener."+strFormID+"."+CIDDestination+".value = \""+strState+"\";";
				
				//HC Return Task
				strStateCode = zipCode.StateCode;

				if ((strESTDELDT.ToString().Trim() != "") && (strStateCode.Trim() != ""))
				{

					object tmpReHCDateTime = DomesticShipmentMgrDAL.calEstHCDateTime(System.DateTime.ParseExact(strESTDELDT,"dd/MM/yyyy HH:mm",null), strStateCode,
						strCON, strBOOKINGNO, strAppID, strEnterpriseID);

					if((tmpReHCDateTime != null) && 
						(!tmpReHCDateTime.GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						sScript += "  window.opener."+strFormID+"."+CIDESTHCDT+".value = \""+((DateTime)tmpReHCDateTime).ToString("dd/MM/yyyy HH:mm")+"\";";
					}
				}
				//HC Return Task
				sScript += "  window.opener."+strFormID+".submit();";
				sScript += "  window.close();";
				sScript += "</script>";
				Response.Write(sScript);
			}
			else if (strFormID=="ImportConsignments")
			{
				Zipcode zipCode = new Zipcode();
				zipCode.Populate(strAppID,strEnterpriseID, RecpPostalCode);
				strCountry = zipCode.Country;
				zipCode.Populate(strAppID,strEnterpriseID, strCountry, RecpPostalCode); 
				strState=zipCode.StateName;
				routeCode=zipCode.DeliveryRoute;
	
				String sScript = "";
				sScript += "<script language=javascript>";
				sScript += "  window.opener."+strFormID+"."+CIDTelephone+".value = \""+RecpTelephone+"\";";
				sScript += "  window.opener."+strFormID+"."+CIDName+".value = \""+RecpName+"\";";
				sScript += "  window.opener."+strFormID+"."+CIDAddress1+".value = \""+RecpAddress1+"\";";
				sScript += "  window.opener."+strFormID+"."+CIDAddress2+".value = \""+RecpAddress2+"\";";
				sScript += "  window.opener."+strFormID+"."+CIDPostalCode+".value = \""+RecpPostalCode+"\";";
				sScript += "  window.opener."+strFormID+"."+CIDContactPerson+".value = \""+RecpContactPerson+"\";";
				sScript += "  window.opener."+strFormID+"."+CIDFax+".value = \""+RecpFax+"\";";
				sScript += "  window.close();";
				sScript += "</script>";
				Response.Write(sScript);
			}
		}


		public void Paging(Object sender, DataGridPageChangedEventArgs e)
		{
			dgReferences.CurrentPageIndex = e.NewPageIndex;
			ShowCurrentPage();
		}

		private String replaceStr(String strOld)
		{
			return strOld.Replace("&nbsp;", "");
		}

		#endregion

		private void dgReferences_CancelCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
		{
			ViewState["SCModePop"] = "cancel";
			dgReferences.EditItemIndex = -1;
			BindGrid();
		}

		private void dgReferences_DeleteCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
		{
			DataRow drReference = m_sdsReferences.ds.Tables[0].Rows[e.Item.ItemIndex];
			String telephone = drReference["telephone"].ToString().Trim().Replace("&nbsp;", "");

			SysDataManager1.DeleteReferences(telephone,strAppID,strEnterpriseID);
			lblReferences.Text =Utility.GetLanguageText(ResourceType.UserMessage,"DELETED_SUCCESSFULLY",utility.GetUserCulture());
			m_sdsReferences.ds.Tables[0].Rows[e.Item.ItemIndex].AcceptChanges();

			ViewState["SCModePop"] = "delete";
			lblReferences.Text = "";
			m_sdsReferences.ds.Tables[0].Rows.RemoveAt(e.Item.ItemIndex);
			dgReferences.EditItemIndex = -1;
			
			BindGrid();
		}

		private void dgReferences_EditCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
		{
			dgReferences.EditItemIndex = e.Item.ItemIndex;
			BindGrid();
			//			ViewState["SCModePop"] = "update";
		}

		private void dgReferences_UpdateCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
		{
			GetLastUpdate(e.Item.ItemIndex);
			m_sdsReferences = (SessionDS)Session["SESSION_DS_REFERENCES"];
			DataSet dsReferences = m_sdsReferences.ds.GetChanges();
			SessionDS sdsReferences = new SessionDS();
			sdsReferences.ds = dsReferences;


			if(ViewState["SCModePop"].ToString() == "Insert")
			{
				try
				{
					SysDataManager1.InsertReferences(sdsReferences,strAppID,strEnterpriseID);
					lblReferences.Text=Utility.GetLanguageText(ResourceType.UserMessage,"INS_SUCCESSFULLY",utility.GetUserCulture());							
					m_sdsReferences.ds.Tables[0].Rows[e.Item.ItemIndex].AcceptChanges();
					ViewState["SCModePop"] = "query";
				}
				catch(ApplicationException appException)
				{
					string strMsg = appException.Message;
					strMsg = appException.InnerException.Message;
					if(strMsg.ToLower().IndexOf("cannot insert duplicate key in object") != -1)
					{
						lblReferences.Text = "Reference telephone already exists.";
					}
					else if(strMsg.IndexOf("duplicate key") != -1  || strMsg.IndexOf("unique") != -1)
					{
						lblReferences.Text = Utility.GetLanguageText(ResourceType.UserMessage,"DUP_KEY_FOUND",utility.GetUserCulture());
					}
					else
					{
						lblReferences.Text = strMsg;
					}
					
					return;				
				}
				catch(Exception err)
				{
					string strMsg = err.Message;
					if(strMsg.ToLower().IndexOf("cannot insert duplicate key in object") != -1)
					{
						lblReferences.Text = "Reference telephone already exists.";
					}
					else if(strMsg.IndexOf("duplicate key") != -1  || strMsg.IndexOf("unique") != -1)
					{
						lblReferences.Text = Utility.GetLanguageText(ResourceType.UserMessage,"DUP_KEY_FOUND",utility.GetUserCulture());
					}
					else
					{
						lblReferences.Text = Utility.GetLanguageText(ResourceType.UserMessage,"ERROR",utility.GetUserCulture());
					}					
				}				
			}
			else
			{
				try 
				{
					SysDataManager1.UpdateReferences(sdsReferences,strAppID,strEnterpriseID);
					lblReferences.Text =Utility.GetLanguageText(ResourceType.UserMessage,"UPD_SUCCESSFULLY",utility.GetUserCulture());
					m_sdsReferences.ds.Tables[0].Rows[e.Item.ItemIndex][7]= m_sdsReferences.ds.Tables[0].Rows[e.Item.ItemIndex][0];
					m_sdsReferences.ds.Tables[0].Rows[e.Item.ItemIndex].AcceptChanges();
					ViewState["SCModePop"] = "query";
				}
				catch (Exception ex)
				{
					string strMsg = ex.ToString();
					if(strMsg.ToLower().IndexOf("cannot insert duplicate key in object") != -1)
					{
						lblReferences.Text = "Reference telephone already exists.";
						m_sdsReferences.ds.Tables[0].Rows[e.Item.ItemIndex][0]= m_sdsReferences.ds.Tables[0].Rows[e.Item.ItemIndex][7];
						m_sdsReferences.ds.Tables[0].Rows[e.Item.ItemIndex].AcceptChanges();
					}
					else if(strMsg.IndexOf("duplicate key") != -1  || strMsg.IndexOf("unique") != -1)
					{
						lblReferences.Text = Utility.GetLanguageText(ResourceType.UserMessage,"DUP_KEY_FOUND",utility.GetUserCulture());
						m_sdsReferences.ds.Tables[0].Rows[e.Item.ItemIndex][0]= m_sdsReferences.ds.Tables[0].Rows[e.Item.ItemIndex][7];
						m_sdsReferences.ds.Tables[0].Rows[e.Item.ItemIndex].AcceptChanges();
					}
					else
					{
						lblReferences.Text = Utility.GetLanguageText(ResourceType.UserMessage,"ERROR",utility.GetUserCulture());
					}	

				}
			}

			Utility.EnableButton(ref btnInsert,ButtonType.Insert,true,accRight);
			//			btnSearch.Enabled = true;
			dgReferences.EditItemIndex = -1;
			BindGrid();

		}

		private void GetLastUpdate(int row)
		{
			TextBox txtTel =(TextBox)dgReferences.Items[row].FindControl("txtGridTelephone");
			TextBox txtName = (TextBox)dgReferences.Items[row].FindControl("txtGridReference_name");
			TextBox txtAdd1 = (TextBox)dgReferences.Items[row].FindControl("txtAddress1");
			TextBox txtAdd2 = (TextBox)dgReferences.Items[row].FindControl("txtAddress2");
			TextBox txtPostCode = (TextBox)dgReferences.Items[row].FindControl("txtPostal");
			TextBox txtFax	= (TextBox)dgReferences.Items[row].FindControl("txtGridFax");
			TextBox txtContactPerson = (TextBox)dgReferences.Items[row].FindControl("txtGridContactperson");

			DataRow dr = m_sdsReferences.ds.Tables[0].Rows[row];
			
			
			dr[0] = txtTel.Text;
			dr[1] = txtName.Text;
			dr[2] = txtAdd1.Text;
			dr[3] =txtAdd2.Text;
			dr[4] = txtPostCode.Text;
			dr[5] = txtFax.Text;
			dr[6] = txtContactPerson.Text;
			if(dr[7].ToString() == "")
			{
				dr[7] = txtTel.Text;
			}
			
			Session["SESSION_DS_REFERENCES"] = m_sdsReferences;
		}

		private void btnInsert_Click(object sender, System.EventArgs e)
		{
			lblReferences.Text = "";
			Utility.EnableButton(ref btnInsert,ButtonType.Insert,false,accRight);
			btnSearch.Enabled = false;
			dgReferences.CurrentPageIndex = 0;

			if((ViewState["SCModePop"].ToString() != "insert" || m_sdsReferences.ds.Tables[0].Rows.Count >= dgReferences.PageSize))
			{
				m_sdsReferences = SysDataManager1.GetEmptyReferences(1);
				dgReferences.EditItemIndex = m_sdsReferences.ds.Tables[0].Rows.Count-1;
			}
			else
			{
				AddRow();				
				dgReferences.EditItemIndex = m_sdsReferences.ds.Tables[0].Rows.Count-1;
			}

			dgReferences.VirtualItemCount = m_sdsReferences.ds.Tables[0].Rows.Count;
			enableEditColumn(true);
			//m_dvTypeOptions = CreateTypeOptions(false);
			BindGrid();
			ViewState["SCModePop"] =com.ties.ScreenMode.Insert;
			//getPageControls(Page);
			//			TextBox txt =  (TextBox)dgReferences.FindControl("txtGridTelephone");
			//			txt.Enabled= true;
		}

		private void enableEditColumn(bool toEnable)
		{
			for (int i = 0;i< dgReferences.Columns.Count;i++)
				dgReferences.Columns[i].Visible = toEnable;

		}

		private void AddRow()
		{
			SysDataManager1.AddNewRowReferencesDS(ref m_sdsReferences);
			BindGrid();
		}

		private void btnQuery_Click(object sender, System.EventArgs e)
		{
			lblReferences.Text = "";
			//m_sdsReferences = SysDataManager1.GetEmptyReferences(0);
			dgReferences.CurrentPageIndex = 0;
			btnSearch.Enabled = true;
			ViewState["SCModePop"] = "query";
			ViewState["SCOperationPop"] = "none";
			Utility.EnableButton(ref btnInsert,ButtonType.Insert,true,accRight);
			dgReferences.EditItemIndex = -1;
			//enableEditColumn(false);
			BindGrid();
		}

		private void dgReferences_ItemCreated(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
		{
			if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
			{
				if (ViewState["SCModePop"].ToString() == "insert" || ViewState["SCModePop"].ToString() == "update")
				{
					e.Item.Cells[0].Visible = false;
				}
				else
				{
					e.Item.Cells[0].Visible = true;
					//string xx = e.Item.Cells[3].Text;

				}
			}
		}

		private void SaveBtn_Click(object sender, System.EventArgs e)
		{
		
		}

	}
}
