<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<%@ Register TagPrefix="dbCombo" Namespace="Cambro.Web.DbCombo" Assembly="Cambro.Web.DbCombo" %>
<%@ Import Namespace="Cambro.Web.DbCombo" %>
<%@ Page language="c#" Codebehind="InvManageSelect.aspx.cs" AutoEventWireup="false" Inherits="com.ties.InvManageSelect" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Invoice Management Selection</title>
		<LINK rel="stylesheet" type="text/css" href="css/Styles.css" name="stylesheet">
		<META name="GENERATOR" content="Microsoft Visual Studio 7.0">
		<META name="CODE_LANGUAGE" content="C#">
		<META name="vs_defaultClientScript" content="JavaScript">
		<META name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5"> <!--#INCLUDE FILE="msFormValidations.inc"-->
		<SCRIPT language="javascript" src="Scripts/settingScrollPosition.js"></SCRIPT>
		<script language="javascript" src="Scripts/msFormValidation.js"></script>
		<script language="javascript">
		<!--
		
		var currentAmntPaid=0;
		var currentBalanceDue=0;
		var digit=0;
		function setValue()
		{
			var s = document.all['inFile'];
			document.all['txtFilePath'].innerText = s.value;
			if(s.value != '') 
				document.all['btnImport'].disabled = false;
			else
				document.all['btnImport'].disabled = true;
			
		}
		
		function upBrowse()
		{
			document.all['divBrowse'].style.backgroundImage='url(images/btn-browse-up.GIF)';
		}
		
		function downBrowse()
		{
			document.all['divBrowse'].style.backgroundImage='url(images/btn-browse-down.GIF)';
		}
		
		function clearValue()
		{
			if(document.all['txtFilePath'].innerText == '') {
				document.location.reload();
			}
		}
		function validateInput(){
			var frm = document.forms[0];		
				if(frm.txtAmntPaid.value!=""){
					var today = new Date();
					var day = today.getDate();
					var month = today.getMonth()+1;
					var year = today.getFullYear();
					var hours = today.getHours();
					if(day<10){
						day = "0"+day;
					}
					if(month<10){
						month = "0"+month;
					}
					if(hours<10){
						hours = "0"+hours;
					}
					var min = today.getMinutes();
					if(min<10){
						min = "0"+min;
					}
					var sec = today.getSeconds();
					if(sec<10){
						sec = "0"+sec;
					}
					var time = hours+":"+min+":"+sec;
					if((month<10)&&(month.length==1)){
						month = '0'+month;
					}
						if(frm.txtPaidDate.value==''){
							frm.txtPaidDate.value =  day+"/"+month+"/"+year;
							
						}
					var username = document.getElementById("txtUserName").value;//frm.txtUsername.value;					
					//document.getElementById("lblDispPaymentUpdate").innerHTML = username+" "+day+"/"+month+"/"+year+" "+time;
					//document.getElementById("lblDispPaymentUpdate").style.display="block";
					//document.getElementById("lblPaymentUpd").style.display="block";
					var amntPaid,totalAmnt=0;
					if(currentAmntPaid=="0"){
					//currentAmntPaid=frm.txtCurrAmntPaid.value;	
					currentAmntPaid=frm.hidtxtCurrAmt.value;
						if(currentAmntPaid.lastIndexOf('.')<=-1)	
						{
						digit=0;
						}else{
						digit = (currentAmntPaid.length-1)-currentAmntPaid.lastIndexOf('.');					
						}
					}					
					amntPaid=frm.txtAmntPaid.value;
					totalAmnt = parseFloat(currentAmntPaid.replace(/,/g,''))+parseFloat(amntPaid);
					frm.txtCurrAmntPaid.value = totalAmnt;
					round(frm.txtCurrAmntPaid,digit);
					frm.txtCurrAmntPaid.value=addCommas(frm.txtCurrAmntPaid.value);
					if(currentBalanceDue=="0"){
						//currentBalanceDue=frm.txtBalanceDue.value;	
						currentBalanceDue=frm.hidtxtBalanceDue.value;	
					}
					totalAmnt = parseFloat(currentBalanceDue.replace(/,/g,''))-parseFloat(amntPaid);
					frm.txtBalanceDue.value = totalAmnt;
					round(frm.txtBalanceDue,digit);
					frm.txtBalanceDue.value=addCommas(frm.txtBalanceDue.value);
					//document.getElementById("lblApplyAmt").style.display="block";
					//document.getElementById("txtApplyAmt").style.display="block";
					//document.getElementById("ddlLinkToCDN").disabled=false;
					
				}else{
					frm.txtPaidDate.value="";
					frm.txtPaidDate.innerHTML="";
					//document.getElementById("lblDispPaymentUpdate").innerHTML="";
					//document.getElementById("lblDispPaymentUpdate").style.display="none";
					//document.getElementById("lblPaymentUpd").style.display="none";
					//document.getElementById("lblApplyAmt").style.display="none";
					//document.getElementById("txtApplyAmt").style.display="none";
					//document.getElementById("ddlLinkToCDN").disabled=true;
				}			
			}
			function addCommas(nStr)
			{			
				nStr += '';
				x = nStr.split('.');				
				x1 = x[0];
				
				//x2 = x.length &gt; 1 ? '.' + x[1] : '';
				var x2
				if(x.length=2){
					x2="."+x[1];
				}
				var rgx = /(\d+)(\d{3})/;
				while (rgx.test(x1)) {
					x1 = x1.replace(rgx, '$1' + ',' + '$2');
				}
				return x1 + x2;
			}
		//-->
		</script>
	</HEAD>
	<BODY onclick="GetScrollPosition();" onunload="window.top.displayBanner.fnCloseAll(0);"
		onload="SetScrollPosition();" MS_POSITIONING="GridLayout">
		<FORM id="InvManageSelect" method="post" runat="server">
			<DIV style="Z-INDEX: 101; LEFT: 24px; WIDTH: 954px; POSITION: absolute; TOP: 23px; HEIGHT: 272px"
				id="divInvManageList" runat="server" ms_positioning="GridLayout">
				<TABLE style="Z-INDEX: 100; LEFT: 9px; WIDTH: 945px; POSITION: absolute; TOP: 12px; HEIGHT: 222px"
					id="Table2" border="0" cellSpacing="1" cellPadding="1" width="932">
					<TBODY>
						<TR>
							<TD><FONT face="Tahoma"><asp:label id="lblMainTitle" runat="server" Height="26px" CssClass="mainTitleSize" Width="477px">Invoice Management Listing</asp:label></FONT></TD>
						</TR>
						<TR>
							<TD><FONT face="Tahoma"><asp:label id="lblErrorForCancel" runat="server" Height="30px" CssClass="errorMsgColor"></asp:label></FONT></TD>
						</TR>
						<TR>
							<TD><asp:button id="btnCancel" runat="server" CssClass="queryButton" Width="96px" CausesValidation="False"
									Text="Cancel"></asp:button><asp:button id="btnSelectAll" runat="server" CssClass="queryButton" Width="96px" CausesValidation="False"
									Text="Select All"></asp:button><asp:button id="btnApproveSelected" runat="server" CssClass="queryButton" Width="154px" Text="Approve Selected"></asp:button><asp:button id="btnCancelSelected" runat="server" CssClass="queryButton" Width="154px" Text="Cancel Selected"></asp:button><asp:button id="btnPrintHeader" runat="server" CssClass="queryButton" Width="154px" Text="Print Header Selected"></asp:button><asp:button id="btnPrintDetail" runat="server" CssClass="queryButton" Width="153px" CausesValidation="False"
									Text="Print Detail Selected"></asp:button><asp:button id="btnPrintInvPkg" runat="server" Width="120px" CssClass="queryButton" Text="Print Detail(Pkg)"
									CausesValidation="False" Visible="True"></asp:button><asp:button id="btnExport" runat="server" CssClass="queryButton" Width="100px" CausesValidation="False"
									Text="Export" Visible="True"></asp:button><asp:button id="btnSaveImport" runat="server" CssClass="queryButton" Width="96px" CausesValidation="False"
									Text="Save"></asp:button></TD>
						</TR>
						<TR>
							<TD><asp:label id="lblErrorMsg" runat="server" Height="30px" CssClass="errorMsgColor" Width="556px"></asp:label></TD>
						</TR>
						<TR>
							<TD><asp:label id="Label1" runat="server" Height="26px" CssClass="mainTitleSize">Status Selected</asp:label><asp:label id="_Label1" runat="server" Height="26px" CssClass="mainTitleSize" Width="2px">:</asp:label><asp:label id="lblShowStatust" runat="server" Height="26px" CssClass="mainTitleSize" Width="275px">[lblShowStatust]</asp:label></TD>
						</TR>
						<TR>
							<TD><asp:datagrid id="dgPreview" runat="server" Height="54px" Width="890px" SelectedItemStyle-CssClass="gridFieldSelectedINV"
									AutoGenerateColumns="False" ItemStyle-Height="20" HeaderStyle-Height="10px">
									<SelectedItemStyle CssClass="gridFieldSelectedINV"></SelectedItemStyle>
									<ItemStyle Height="10px" CssClass="gridField"></ItemStyle>
									<HeaderStyle Height="10px"></HeaderStyle>
									<Columns>
										<asp:TemplateColumn>
											<HeaderStyle Width="20px" CssClass="gridBlueHeading"></HeaderStyle>
											<ItemTemplate>
												<asp:CheckBox ID="chkSelect" Runat="server" AutoPostBack="true" OnCheckedChanged="getCheck_onChecked"></asp:CheckBox>
											</ItemTemplate>
										</asp:TemplateColumn>
										<asp:EditCommandColumn ButtonType="LinkButton" UpdateText="" CancelText="" EditText="&lt;IMG SRC='images/butt-edit.gif' border=0 title='Edit' &gt;">
											<HeaderStyle HorizontalAlign="Center" CssClass="gridBlueHeading"></HeaderStyle>
											<ItemStyle HorizontalAlign="Center"></ItemStyle>
										</asp:EditCommandColumn>
										<asp:TemplateColumn>
											<HeaderStyle CssClass="gridBlueHeading"></HeaderStyle>
											<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
											<ItemTemplate>
												&nbsp;
												<asp:ImageButton CommandName="Select" id="imgSelect" runat="server" ImageUrl="images/butt-select.gif"></asp:ImageButton>&nbsp;
											</ItemTemplate>
										</asp:TemplateColumn>
										<asp:BoundColumn DataField="invoice_no" HeaderText="Invoice No.">
											<HeaderStyle HorizontalAlign="Center" CssClass="gridBlueHeading"></HeaderStyle>
											<ItemStyle HorizontalAlign="Left" CssClass="gridField"></ItemStyle>
										</asp:BoundColumn>
										<asp:BoundColumn DataField="invoice_date" HeaderText="Invoice Date" DataFormatString="{0:dd/MM/yyyy}">
											<HeaderStyle HorizontalAlign="Center" CssClass="gridBlueHeading"></HeaderStyle>
											<ItemStyle HorizontalAlign="Center" CssClass="gridField"></ItemStyle>
										</asp:BoundColumn>
										<asp:BoundColumn DataField="invoice_date" HeaderText="Created Date" DataFormatString="{0:dd/MM/yyyy}">
											<HeaderStyle HorizontalAlign="Center" CssClass="gridBlueHeading"></HeaderStyle>
											<ItemStyle HorizontalAlign="Center" CssClass="gridField"></ItemStyle>
										</asp:BoundColumn>
										<asp:BoundColumn DataField="Due_date" HeaderText="Due Date" DataFormatString="{0:dd/MM/yyyy}">
											<HeaderStyle HorizontalAlign="Center" CssClass="gridBlueHeading"></HeaderStyle>
											<ItemStyle HorizontalAlign="Center" CssClass="gridField"></ItemStyle>
										</asp:BoundColumn>
										<asp:BoundColumn DataField="payertype_name" HeaderText="Invoice Type">
											<HeaderStyle HorizontalAlign="Center" CssClass="gridBlueHeading"></HeaderStyle>
											<ItemStyle HorizontalAlign="Left" CssClass="gridField"></ItemStyle>
										</asp:BoundColumn>
										<asp:BoundColumn DataField="payerid" HeaderText="Customer ID">
											<HeaderStyle HorizontalAlign="Center" CssClass="gridBlueHeading"></HeaderStyle>
											<ItemStyle HorizontalAlign="Left" CssClass="gridField"></ItemStyle>
										</asp:BoundColumn>
										<asp:TemplateColumn HeaderText="Adjustment">
											<HeaderStyle HorizontalAlign="Center" CssClass="gridBlueHeading"></HeaderStyle>
											<ItemStyle HorizontalAlign="Right" CssClass="gridField"></ItemStyle>
											<ItemTemplate>
												<asp:Label CssClass="gridFieldNum" ID="lblAdj" Runat="server" Text='<%#utility.encodeNegValue(DataBinder.Eval(Container.DataItem,"invoice_adj_amount"),(String)ViewState["m_format"])%>'>
												</asp:Label>
											</ItemTemplate>
										</asp:TemplateColumn>
										<asp:TemplateColumn HeaderText="Total Invoice Amount">
											<HeaderStyle HorizontalAlign="Center" CssClass="gridBlueHeading"></HeaderStyle>
											<ItemStyle HorizontalAlign="Right" CssClass="gridField"></ItemStyle>
											<ItemTemplate>
												<asp:Label CssClass="gridFieldNum" ID="Label5" Runat="server" Text='<%#String.Format((String)ViewState["m_format"],DataBinder.Eval(Container.DataItem,"invoice_amt"))%>'>
												</asp:Label>
											</ItemTemplate>
										</asp:TemplateColumn>
										<asp:BoundColumn DataField="invoicestatus_name" HeaderText="Invoice Status">
											<HeaderStyle HorizontalAlign="Center" CssClass="gridBlueHeading"></HeaderStyle>
											<ItemStyle HorizontalAlign="Left" CssClass="gridField"></ItemStyle>
										</asp:BoundColumn>
										<asp:BoundColumn DataField="first_possible_bpd" HeaderText="1st Possible BPD" DataFormatString="{0:dd/MM/yyyy}">
											<HeaderStyle HorizontalAlign="Center" CssClass="gridBlueHeading"></HeaderStyle>
											<ItemStyle HorizontalAlign="Left" CssClass="gridField"></ItemStyle>
										</asp:BoundColumn>
										<asp:BoundColumn DataField="next_bill_placement_date" HeaderText="Next BPD" DataFormatString="{0:dd/MM/yyyy}">
											<HeaderStyle HorizontalAlign="Center" CssClass="gridBlueHeading"></HeaderStyle>
											<ItemStyle HorizontalAlign="Left" CssClass="gridField"></ItemStyle>
										</asp:BoundColumn>
										<asp:BoundColumn DataField="actual_bill_placement_date" HeaderText="Act BPD" DataFormatString="{0:dd/MM/yyyy}">
											<HeaderStyle HorizontalAlign="Center" CssClass="gridBlueHeading"></HeaderStyle>
											<ItemStyle HorizontalAlign="Left" CssClass="gridField"></ItemStyle>
										</asp:BoundColumn>
										<asp:TemplateColumn HeaderText="Amt Paid">
											<HeaderStyle HorizontalAlign="Center" CssClass="gridBlueHeading"></HeaderStyle>
											<ItemStyle HorizontalAlign="Right" CssClass="gridField" Wrap="False"></ItemStyle>
											<ItemTemplate>
												<asp:Label CssClass="gridFieldNum" ID="lblAmtPaid" Runat="server" Text='<%#String.Format((String)ViewState["m_format"],DataBinder.Eval(Container.DataItem,"amt_paid"))%>'>
												</asp:Label>
											</ItemTemplate>
										</asp:TemplateColumn>
										<asp:BoundColumn DataField="payment_date" HeaderText="Paid Date" DataFormatString="{0:dd/MM/yyyy}">
											<HeaderStyle HorizontalAlign="Center" CssClass="gridBlueHeading"></HeaderStyle>
											<ItemStyle HorizontalAlign="Left" CssClass="gridField"></ItemStyle>
										</asp:BoundColumn>
										<asp:BoundColumn DataField="promised_dt" HeaderText="Promise Dt" DataFormatString="{0:dd/MM/yyyy}">
											<HeaderStyle HorizontalAlign="Center" CssClass="gridBlueHeading"></HeaderStyle>
											<ItemStyle HorizontalAlign="Left" CssClass="gridField"></ItemStyle>
										</asp:BoundColumn>
										<asp:BoundColumn DataField="batch_no" HeaderText="Batch Number">
											<HeaderStyle HorizontalAlign="Center" CssClass="gridBlueHeading"></HeaderStyle>
											<ItemStyle HorizontalAlign="Left" CssClass="gridField"></ItemStyle>
										</asp:BoundColumn>
										<asp:BoundColumn DataField="link_to_invoice" HeaderText="Linked Credit Note">
											<HeaderStyle HorizontalAlign="Center" CssClass="gridBlueHeading"></HeaderStyle>
											<ItemStyle HorizontalAlign="Left" CssClass="gridField"></ItemStyle>
										</asp:BoundColumn>
									</Columns>
									<PagerStyle NextPageText="Next" PrevPageText="Previous" HorizontalAlign="Left" CssClass="normalText"></PagerStyle>
								</asp:datagrid></TD>
						</TR>
					</TBODY>
				</TABLE>
			</DIV>
			<DIV style="Z-INDEX: 103; LEFT: 20px; WIDTH: 702px; POSITION: absolute; TOP: 33px; HEIGHT: 461px"
				id="divInvoiceManagment" runat="server" ms_positioning="GridLayout">
				<TABLE style="Z-INDEX: 101; LEFT: 14px; WIDTH: 688px; POSITION: absolute; TOP: 11px; HEIGHT: 448px"
					id="Table1" border="0" cellSpacing="1" cellPadding="1" width="670">
					<TR>
						<TD style="HEIGHT: 9px"><asp:label id="lblTitle" runat="server" Height="32px" CssClass="maintitleSize" Width="558px">Invoice Management</asp:label></TD>
					</TR>
					<TR>
						<TD style="HEIGHT: 330px"><asp:label id="lblErrorMessage" runat="server" Height="30px" CssClass="errorMsgColor" Width="556px"></asp:label>
							<table border="0">
								<TR>
									<TD><asp:button id="btnClear" runat="server" CssClass="queryButton" Width="74px" Text="Clear"></asp:button><asp:button id="btnGenerate" runat="server" Height="20px" CssClass="queryButton" Width="155px"
											Text="Execute Query"></asp:button>&nbsp;
									</TD>
								</TR>
								<TR>
									<TD>
										<TABLE style="WIDTH: 600px; HEIGHT: 272px" id="tblShipmentExcepPODRepQry" border="0" width="500"
											runat="server">
											<TR width="100%">
												<TD vAlign="top" width="100%">
													<FIELDSET><LEGEND><asp:label id="Label2" CssClass="tableHeadingFieldset" Runat="server">Invoice Management</asp:label></LEGEND>
														<TABLE style="LEFT: 1px; WIDTH: 482px; TOP: 2px; HEIGHT: 233px" id="tblDates" border="0"
															width="482" align="left" runat="server">
															<TR height="14">
																<TD style="WIDTH: 118px">&nbsp;</TD>
																<TD style="WIDTH: 49px">&nbsp;</TD>
																<TD>&nbsp;</TD>
																<TD>&nbsp;</TD>
																<TD style="WIDTH: 46px">&nbsp;</TD>
																<TD>&nbsp;</TD>
															</TR>
															<TR height="27">
																<TD style="WIDTH: 118px"><asp:label id="lblInvoiceDate" runat="server" CssClass="tablelabel" Width="91px">Invoice Date</asp:label></TD>
																<TD style="WIDTH: 49px">&nbsp;
																	<asp:label id="Label3" runat="server" CssClass="tablelabel" Width="32px">From</asp:label></TD>
																<TD>&nbsp;
																	<cc1:mstextbox id="txtInvDateFrom" runat="server" CssClass="textField" Width="130px" TextMaskString="99/99/9999"
																		TextMaskType="msDate" MaxLength="10"></cc1:mstextbox></TD>
																<TD>&nbsp;</TD>
																<TD style="WIDTH: 46px" align="right"><asp:label id="Label4" runat="server" CssClass="tablelabel" Width="19px">To</asp:label></TD>
																<TD><cc1:mstextbox id="txtInvDateto" runat="server" CssClass="textField" Width="130px" TextMaskString="99/99/9999"
																		TextMaskType="msDate" MaxLength="10"></cc1:mstextbox></TD>
															</TR>
															<TR height="27">
																<TD style="WIDTH: 118px"><asp:label id="lblInvoiceNo" runat="server" CssClass="tablelabel" Width="91px">Invoice Number</asp:label></TD>
																<TD style="WIDTH: 49px" colSpan="5"><asp:textbox id="txtInvoiceNo" tabIndex="4" runat="server" CssClass="textField" Width="141px"
																		MaxLength="20"></asp:textbox></FONT></TD>
															</TR>
															<TR height="27">
																<TD style="WIDTH: 118px"><asp:label id="lblDueDate" runat="server" CssClass="tablelabel" Width="91px">Due Date</asp:label></TD>
																<TD style="WIDTH: 49px">&nbsp;
																	<asp:label id="Label10" runat="server" CssClass="tablelabel" Width="32px">From</asp:label></TD>
																<TD>&nbsp;
																	<cc1:mstextbox id="txtDueDateFrom" runat="server" CssClass="textField" Width="130px" TextMaskString="99/99/9999"
																		TextMaskType="msDate" MaxLength="10"></cc1:mstextbox></TD>
																<TD>&nbsp;</TD>
																<TD style="WIDTH: 46px" align="right"><asp:label id="Label11" runat="server" CssClass="tablelabel" Width="19px">To</asp:label></TD>
																<TD><cc1:mstextbox id="txtDueDateto" runat="server" CssClass="textField" Width="130px" TextMaskString="99/99/9999"
																		TextMaskType="msDate" MaxLength="10"></cc1:mstextbox></TD>
															</TR>
															<TR height="27">
																<TD style="WIDTH: 118px"><asp:label id="lblInvoiceType" runat="server" CssClass="tablelabel" Width="91px">Invoice Type</asp:label></TD>
																<TD style="WIDTH: 49px" colSpan="5"><asp:dropdownlist id="ddlInvType" tabIndex="20" runat="server" Height="69px" Width="154px" DataValueField="custid"
																		DataTextField="custid"></asp:dropdownlist></TD>
															</TR>
															<TR height="27">
																<TD style="WIDTH: 118px"><asp:label id="lblCustID" runat="server" CssClass="tablelabel" Width="91px">Customer ID</asp:label></TD>
																<TD style="WIDTH: 49px" colSpan="5"><asp:dropdownlist id="ddbCustomerAcc" tabIndex="20" runat="server" Height="69px" Width="154px" DataValueField="custid"
																		DataTextField="custid" AutoPostBack="True"></asp:dropdownlist><dbcombo:dbcombo id="dbCmbAgentId" tabIndex="1" runat="server" Height="17px" Width="140px" AutoPostBack="True"
																		ReQueryOnLoad="True" ShowDbComboLink="False" TextUpLevelSearchButton="v" ServerMethod="AgentIdServerMethod" TextBoxColumns="20"></dbcombo:dbcombo></TD>
															</TR>
															<TR height="27">
																<TD style="WIDTH: 118px; HEIGHT: 22px"><asp:label id="lblCustName" runat="server" CssClass="tablelabel" Width="104px">Customer Name</asp:label></TD>
																<TD style="WIDTH: 49px; HEIGHT: 22px" colSpan="5"><asp:textbox id="txtCustName" tabIndex="4" runat="server" CssClass="textField" Width="357px"
																		MaxLength="20" BackColor="Silver"></asp:textbox></TD>
															</TR>
															<TR height="27">
																<TD style="WIDTH: 118px"><asp:label id="lblStatust" runat="server" CssClass="tablelabel" Width="91px"> Status</asp:label></TD>
																<TD style="WIDTH: 49px" colSpan="5"><asp:dropdownlist id="ddlInvStatus" tabIndex="20" runat="server" Height="69px" Width="154px" DataValueField="custid"
																		DataTextField="custid"></asp:dropdownlist></TD>
															</TR>
															<TR>
																<TD style="WIDTH: 118px"><asp:label id="Label6" runat="server" CssClass="tablelabel" Width="118px"> Browse for Import</asp:label></TD>
																<TD style="WIDTH: 49px; HEIGHT: 20px" colSpan="4"><asp:textbox id="txtFilePath" tabIndex="4" runat="server" CssClass="textField" Width="250px"
																		MaxLength="200"></asp:textbox></TD>
																<td height="20">
																	<div style="DISPLAY: inline; BACKGROUND-IMAGE: url(images/btn-browse-up.GIF); WIDTH: 70px; HEIGHT: 20px"
																		id="divBrowse" onmouseup="upBrowse();" onmousedown="downBrowse();"><INPUT onblur="setValue();" style="BORDER-RIGHT: #88a0c8 1px outset; BORDER-TOP: #88a0c8 1px outset; FONT-SIZE: 11px; FILTER: alpha(opacity: 0); BORDER-LEFT: #88a0c8 1px outset; WIDTH: 0px; COLOR: #003068; BORDER-BOTTOM: #88a0c8 1px outset; FONT-FAMILY: Arial; BACKGROUND-COLOR: #e9edf0; TEXT-DECORATION: none"
																			id="inFile" onfocus="setValue();" type="file" name="inFile" runat="server"></div>
																	</INPUT><asp:button id="btnImport" runat="server" CssClass="queryButton" Width="60px" Text="Import"
																		Enabled="False"></asp:button></td>
															</TR>
															<tr>
																<td></td>
															</tr>
														</TABLE>
													</FIELDSET>
												</TD>
											</TR>
										</TABLE>
									</TD>
								</TR>
							</table>
						</TD>
					</TR>
					<TR>
						<TD><input value="<%=strScrollPosition%>" type=hidden 
      name=ScrollPosition></TD>
					</TR>
				</TABLE>
			</DIV>
			<asp:panel style="Z-INDEX: 104; LEFT: 170px; POSITION: absolute; TOP: 306px" id="PanelInvoiceCancel"
				runat="server" Height="180px" Width="640px" Visible="False">
				<FIELDSET style="WIDTH: 609px; HEIGHT: 177px; BACKGROUND-COLOR: #ffff99"><LEGEND>
						<asp:label id="lblSendInfo" CssClass="tableHeadingFieldset" Runat="server">Invoice Cancel</asp:label></LEGEND>
					<TABLE id="tblSenderInfo" style="WIDTH: 608px; HEIGHT: 127px; BACKGROUND-COLOR: #ffff99"
						cellSpacing="0" cellPadding="0" width="608" bgColor="#ffff99" border="0" runat="server">
						<TR>
							<TD style="WIDTH: 40px" width="40"></TD>
							<TD style="WIDTH: 141px" width="141"></TD>
							<TD style="WIDTH: 221px" width="221" colSpan="2"></TD>
							<TD style="WIDTH: 15px" width="15"></TD>
							<TD width="25%"></TD>
						</TR>
						<TR>
							<TD style="WIDTH: 40px" width="40">&nbsp;
							</TD>
							<TD style="WIDTH: 141px" width="141">
								<asp:label id="lblSendAddr1" runat="server" Width="155px" CssClass="tableLabel" Height="21px"
									BackColor="#FFFF80">Invoice Cancel Type</asp:label></TD>
							<TD style="WIDTH: 221px" width="221" colSpan="2">
								<asp:DropDownList id="ddlInvoiceCancelType" runat="server" Width="149px" AutoPostBack="True"></asp:DropDownList></TD>
							<TD style="WIDTH: 15px" width="15"></TD>
							<TD width="25%"></TD>
						</TR>
						<TR>
							<TD style="PADDING-LEFT: 190px" align="left" colSpan="6"><FONT style="TEXT-ALIGN: left" face="Tahoma">
									<asp:Label id="lblShowCancelInvoice" runat="server" CssClass="tableLabel"></asp:Label></FONT></TD>
						</TR>
						<TR height="25">
							<TD style="WIDTH: 40px" width="40"></TD>
							<TD style="WIDTH: 141px" width="141">
								<asp:label id="lblSendNm" runat="server" Width="158px" CssClass="tableLabel" Height="20px"
									BackColor="#FFFF80">Reason</asp:label></TD>
							<TD style="WIDTH: 221px" width="221" colSpan="2">
								<asp:DropDownList id="ddlInvoiceCancelReason" runat="server" Width="147px" AutoPostBack="True"></asp:DropDownList></TD>
							<TD style="WIDTH: 15px" width="15"></TD>
							<TD width="25%"></TD>
						</TR>
						<TR>
							<TD style="WIDTH: 40px" width="40"></TD>
							<TD style="WIDTH: 141px" width="141"></TD>
							<TD style="WIDTH: 221px" width="221" colSpan="2"></TD>
							<TD style="WIDTH: 15px" width="15"></TD>
							<TD width="25%"></TD>
						</TR>
						<TR>
							<TD style="WIDTH: 40px" width="40"></TD>
							<TD style="WIDTH: 141px" width="141"></TD>
							<TD style="WIDTH: 221px" width="221" colSpan="2"><FONT face="Tahoma">
									<asp:button id="btnOKCancelInvoice" runat="server" Width="96px" CssClass="queryButton" Text="OK"
										CausesValidation="False"></asp:button></FONT>
								<asp:button id="btnSaveCancel" runat="server" Width="96px" CssClass="queryButton" Text="Save"
									CausesValidation="False"></asp:button><FONT face="Tahoma">&nbsp;</FONT>
								<asp:button id="btnCancelINVRemark" runat="server" Width="96px" CssClass="queryButton" Text="Cancel"
									CausesValidation="False"></asp:button></TD>
							<TD style="WIDTH: 15px" width="15"></TD>
							<TD width="25%"></TD>
						</TR>
					</TABLE>
				</FIELDSET>
			</asp:panel>
			<div style="Z-INDEX: 102; LEFT: 24px; WIDTH: 1813px; POSITION: relative; TOP: 23px; HEIGHT: 750px"
				id="InvoiceDetails" runat="server" ms_positioning="GridLayout"><asp:button style="Z-INDEX: 102; LEFT: 11px; POSITION: absolute; TOP: 37px" id="btnCancelDtl"
					runat="server" CssClass="queryButton" Width="61px" CausesValidation="False" Text="Cancel"></asp:button><asp:label style="Z-INDEX: 103; LEFT: 18px; POSITION: absolute; TOP: 62px" id="lblErrorMsgDtl"
					runat="server" Height="19px" CssClass="errorMsgColor" Width="537px"></asp:label><asp:validationsummary style="Z-INDEX: 104; LEFT: 14px; POSITION: absolute; TOP: 62px" id="PageValidationSummaryDtl"
					runat="server" Height="39px" Width="346px" Visible="True" ShowSummary="False" ShowMessageBox="True"></asp:validationsummary><asp:label style="Z-INDEX: 101; LEFT: 15px; POSITION: absolute; TOP: 9px" id="lblMainTitleDtl"
					runat="server" Height="26px" CssClass="mainTitleSize" Width="477px">Invoice Details</asp:label>
				<TABLE style="Z-INDEX: 105; LEFT: 7px; WIDTH: 727px; POSITION: absolute; TOP: 95px; HEIGHT: 139px"
					id="Table3" border="0" cellSpacing="1" width="727" align="left" runat="server">
					<TR height="25">
						<TD style="WIDTH: 150px" bgColor="#008499" vAlign="middle" width="150" align="left">&nbsp;
							<asp:label id="Label_50" runat="server" Height="16px" CssClass="tableLabel" Width="150px" BackColor="#008499"
								Font-Size="11px" ForeColor="White">Customer ID</asp:label></TD>
						<TD style="WIDTH: 9px" vAlign="top" width="9"></TD>
						<TD style="WIDTH: 1001px" vAlign="middle" width="1001"><asp:label id="lblCustIDDtl" runat="server" CssClass="tableLabel" Width="143px"></asp:label></TD>
						<TD style="WIDTH: 150px" bgColor="#008499" width="150" align="left">&nbsp;
							<asp:label id="Label_6" runat="server" Height="15px" CssClass="tableLabel" Width="150px" BackColor="#008499"
								Font-Size="11px" ForeColor="White"> Province</asp:label></TD>
						<TD style="WIDTH: 8px" width="8"></TD>
						<TD style="WIDTH: 61px" width="61"><asp:label id="lblProvinceDtl" runat="server" CssClass="tableLabel" Width="150px"></asp:label></TD>
					</TR>
					<TR height="25">
						<TD style="WIDTH: 150px; HEIGHT: 25px" bgColor="#008499" width="96" align="left">&nbsp;
							<asp:label id="Label_48" runat="server" Height="15px" CssClass="tableLabel" Width="150px" BackColor="#008499"
								Font-Size="11px" ForeColor="White">Customer Name</asp:label></TD>
						<TD style="WIDTH: 9px; HEIGHT: 25px" width="9"></TD>
						<TD style="WIDTH: 1001px; HEIGHT: 25px" vAlign="middle" width="1001"><asp:label id="lblCustNameDtl" runat="server" CssClass="tableLabel" Width="260px"></asp:label></TD>
						<TD style="WIDTH: 150px; HEIGHT: 25px" bgColor="#008499" width="150" align="left">&nbsp;
							<asp:label id="Label_49" runat="server" Height="14px" CssClass="tableLabel" Width="93px" BackColor="#008499"
								Font-Size="11px" ForeColor="White"> Telephone</asp:label></TD>
						<TD style="WIDTH: 8px; HEIGHT: 25px" width="8"></TD>
						<TD style="WIDTH: 61px; HEIGHT: 25px" width="61"><asp:label id="lblTelephoneDtl" runat="server" CssClass="tableLabel" Width="150px"></asp:label></TD>
					</TR>
					<TR height="25">
						<TD style="WIDTH: 150px" bgColor="#008499" vAlign="middle" width="150" align="left">&nbsp;
							<asp:label id="Label_45" runat="server" Height="15px" CssClass="tableLabel" Width="150px" BackColor="#008499"
								Font-Size="11px" ForeColor="White"> Address 1</asp:label></TD>
						<TD style="WIDTH: 9px" width="9"></TD>
						<TD style="WIDTH: 1001px" width="1001"><asp:label id="lblAddress1Dtl" runat="server" CssClass="tableLabel" Width="303px"></asp:label></TD>
						<TD style="WIDTH: 150px; HEIGHT: 25px" bgColor="#008499" width="148" align="left">&nbsp;
							<asp:label id="Label_47" runat="server" Height="13px" CssClass="tableLabel" Width="83px" BackColor="#008499"
								Font-Size="11px" ForeColor="White">Fax</asp:label></TD>
						<TD style="WIDTH: 8px" width="8"></TD>
						<TD style="WIDTH: 61px" width="61"><asp:label id="lblFaxDtl" runat="server" CssClass="tableLabel" Width="150px"></asp:label></TD>
					</TR>
					<TR height="25">
						<TD style="WIDTH: 150px" bgColor="#008499" width="150">&nbsp;
							<asp:label id="Label_1" runat="server" Height="16px" CssClass="tableLabel" Width="150px" BackColor="#008499"
								Font-Size="11px" ForeColor="White">Address 2</asp:label></TD>
						<TD style="WIDTH: 9px" width="9"></TD>
						<TD style="WIDTH: 1001px" width="1001"><asp:label id="lblAddress2Dtl" runat="server" CssClass="tableLabel" Width="301px"></asp:label></TD>
						<TD style="WIDTH: 150px" bgColor="#008499" width="148" align="left">&nbsp;
							<asp:label style="WHITE-SPACE: nowrap" id="Label_19" runat="server" Height="13px" CssClass="tableLabel"
								Width="83px" BackColor="#008499" Font-Size="11px" ForeColor="White">Contact Person</asp:label></TD>
						<TD style="WIDTH: 8px" width="8" align="left"></TD>
						<TD style="WIDTH: 61px" width="61" align="left"><asp:label id="lblContactDtl" runat="server" CssClass="tableLabel" Width="300px"></asp:label></TD>
					</TR>
					<TR height="25">
						<TD style="WIDTH: 150px" bgColor="#008499" vAlign="middle" width="150">&nbsp;
							<asp:label id="Label_2" runat="server" Height="16px" CssClass="tableLabel" Width="150px" BackColor="#008499"
								Font-Size="11px" ForeColor="White">Postal Code</asp:label></TD>
						<TD style="WIDTH: 9px" width="9"></TD>
						<TD style="WIDTH: 1001px" width="1001"><asp:label id="lblPostalCodeDtl" runat="server" CssClass="tableLabel" Width="143px"></asp:label></TD>
						<TD style="WIDTH: 150px" bgColor="#008499" width="150">&nbsp;
							<asp:label id="Label_46" runat="server" Height="16px" CssClass="tableLabel" Width="150px" BackColor="#008499"
								Font-Size="11px" ForeColor="White">HC POD Required</asp:label></TD>
						<TD style="WIDTH: 8px" width="8" align="left"></TD>
						<TD style="WIDTH: 61px" width="61" align="left"><asp:label id="lblHC_POD_RequiredDtl" runat="server" CssClass="tableLabel" Width="150px" Font-Size="11px"></asp:label></TD>
					</TR>
				</TABLE>
				<TABLE style="Z-INDEX: 106; LEFT: 6px; WIDTH: 727px; POSITION: absolute; TOP: 244px; HEIGHT: 139px"
					id="Table4" border="0" cellSpacing="1" width="727" align="left" runat="server">
					<TBODY>
						<TR height="25">
							<TD style="WIDTH: 150px" bgColor="#008499" vAlign="middle" width="150" align="left">&nbsp;
								<asp:label style="WHITE-SPACE: nowrap" id="Label_3" runat="server" Height="15px" Width="150px"
									BackColor="#008499" Font-Size="11px" ForeColor="White">Invoice Number</asp:label></TD>
							<TD style="WIDTH: 9px" vAlign="top" width="9"></TD>
							<TD style="WIDTH: 1001px" vAlign="middle" width="1001"><asp:label id="lblInvNoDtl" runat="server" CssClass="tableLabel" Width="143px"></asp:label></TD>
							<TD style="WIDTH: 150px" bgColor="#008499" width="150" align="left">&nbsp;
								<asp:label id="Label_7" runat="server" Height="15px" CssClass="tableLabel" Width="81px" BackColor="#008499"
									Font-Size="11px" ForeColor="White"> Due Date</asp:label></TD>
							<TD style="WIDTH: 8px" width="8"></TD>
							<TD style="WIDTH: 61px" width="61"><asp:label id="lblDueDateDtl" runat="server" CssClass="tableLabel" Width="150px"></asp:label></TD>
						</TR>
						<TR height="25">
							<TD style="WIDTH: 150px" bgColor="#008499" vAlign="middle" width="96" align="left">&nbsp;
								<asp:label id="Label_9" runat="server" Height="16px" CssClass="tableLabel" Width="150px" BackColor="#008499"
									Font-Size="11px" ForeColor="White">Invoice Status</asp:label></TD>
							<TD style="WIDTH: 9px" vAlign="top" width="9"></TD>
							<TD style="WIDTH: 1001px" vAlign="middle" width="1001"><asp:label id="lblInvStatusDtl" runat="server" CssClass="tableLabel" Width="143px"></asp:label></TD>
							<TD style="WIDTH: 150px" bgColor="#008499" width="150" align="left">&nbsp;
								<asp:label id="Label_11" runat="server" Height="14px" CssClass="tableLabel" Width="93px" BackColor="#008499"
									Font-Size="11px" ForeColor="White"> Total Amount</asp:label></TD>
							<TD style="WIDTH: 8px" width="8"></TD>
							<TD style="WIDTH: 61px" width="61"><asp:label id="lblTotalAmtDtl" runat="server" CssClass="tableLabel" Width="150px"></asp:label></TD>
						</TR>
						<TR height="25">
							<TD style="WIDTH: 150px; HEIGHT: 25px" bgColor="#008499" width="96" align="left">&nbsp;
								<asp:label id="Label_13" runat="server" Height="15px" CssClass="tableLabel" Width="150px" BackColor="#008499"
									Font-Size="11px" ForeColor="White">Payment Term</asp:label></TD>
							<TD style="WIDTH: 9px; HEIGHT: 25px" width="9"></TD>
							<TD style="WIDTH: 1001px; HEIGHT: 25px" vAlign="middle" width="1001"><asp:label id="lblPaymentTermDtl" runat="server" CssClass="tableLabel" Width="260px"></asp:label></TD>
							<TD style="WIDTH: 150px; HEIGHT: 25px" bgColor="#008499" width="148" align="left">&nbsp;
								<asp:label id="Label_15" runat="server" Height="13px" CssClass="tableLabel" Width="150px" BackColor="#008499"
									Font-Size="11px" ForeColor="White">Total Adjustments</asp:label></TD>
							<TD style="WIDTH: 8px; HEIGHT: 25px" width="8"></TD>
							<TD style="WIDTH: 61px; HEIGHT: 25px" width="61"><asp:label id="lblTotalAdjDtl" runat="server" CssClass="tableLabel" Width="150px"></asp:label></TD>
						</TR>
						<TR height="25">
							<TD style="WIDTH: 150px" bgColor="#008499" vAlign="middle" width="96" align="left">&nbsp;
								<asp:label id="Label_17" runat="server" Height="15px" CssClass="tableLabel" Width="150px" BackColor="#008499"
									Font-Size="11px" ForeColor="White"> Invoice Date</asp:label></TD>
							<TD style="WIDTH: 9px" width="9"></TD>
							<TD style="WIDTH: 1001px" width="1001"><asp:label id="lblInvDateDtl" runat="server" CssClass="tableLabel" Width="303px"></asp:label></TD>
							<TD style="WIDTH: 150px" bgColor="#008499" width="148" align="left">&nbsp;
								<asp:label id="lblCancel_AppHeadDtl" runat="server" Height="13px" CssClass="tableLabel" Width="150px"
									BackColor="#008499" Font-Size="11px" ForeColor="White"></asp:label></TD>
							<TD style="WIDTH: 8px" width="8"></TD>
							<TD style="WIDTH: 61px" width="61"><asp:label id="lblCancel_AppTextDtl" runat="server" CssClass="tableLabel" Width="150px"></asp:label></TD>
						</TR>
					</TBODY>
				</TABLE>
				<table style="Z-INDEX: 105; LEFT: 5px; POSITION: absolute; TOP: 406px" width="890">
					<tr>
						<td><asp:datagrid id="dgEditDtl" runat="server" Height="95px" SelectedItemStyle-CssClass="gridFieldSelectedINV"
								AutoGenerateColumns="False" ItemStyle-Height="20" OnUpdateCommand="dgEditDtl_dgUpdate" OnItemDataBound="dgEditDtl_dgBound">
								<SelectedItemStyle CssClass="gridFieldSelectedINV"></SelectedItemStyle>
								<ItemStyle Height="10px" CssClass="gridField"></ItemStyle>
								<Columns>
									<asp:TemplateColumn>
										<HeaderStyle CssClass="gridBlueHeading"></HeaderStyle>
										<ItemStyle HorizontalAlign="Center" CssClass="gridField"></ItemStyle>
										<ItemTemplate>
											<asp:ImageButton CommandName="Update" id="imgUpdate" runat="server" ImageUrl="images/butt-update.gif"></asp:ImageButton>&nbsp;&nbsp;
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Consignment No.">
										<HeaderStyle HorizontalAlign="Center" Width="86px" CssClass="gridBlueHeading"></HeaderStyle>
										<ItemStyle Wrap="False" CssClass="gridField" VerticalAlign="Middle"></ItemStyle>
										<ItemTemplate>
											<asp:Label Width="86px" CssClass="gridLabel" runat="server" ID="lblConsgnNo"></asp:Label>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Type">
										<HeaderStyle HorizontalAlign="Center" Width="86px" CssClass="gridBlueHeading"></HeaderStyle>
										<ItemStyle Wrap="False" CssClass="gridField" VerticalAlign="Middle"></ItemStyle>
										<ItemTemplate>
											<asp:DropDownList Width="86px" CssClass="gridDropDown" ID="ddlAdjType" Runat="server"></asp:DropDownList>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Apply As*">
										<HeaderStyle HorizontalAlign="Center" Width="86px" CssClass="gridBlueHeading"></HeaderStyle>
										<ItemStyle Wrap="False" CssClass="gridField" VerticalAlign="Middle"></ItemStyle>
										<ItemTemplate>
											<asp:DropDownList Width="86px" CssClass="gridDropDown" ID="ddlApplyAs" Runat="server"></asp:DropDownList>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Amount">
										<HeaderStyle HorizontalAlign="Center" Width="100px" CssClass="gridBlueHeading"></HeaderStyle>
										<ItemStyle CssClass="gridField"></ItemStyle>
										<ItemTemplate>
											<cc1:mstextbox CssClass="gridTextBoxNumber" ID="txtAmount" Runat="server" MaxLength="9" TextMaskType="msNumeric"
												NumberMaxValue="999999" NumberMinValue="0" NumberPrecision="8" NumberScale="2"></cc1:mstextbox>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Remark">
										<HeaderStyle HorizontalAlign="Center" Width="250px" CssClass="gridBlueHeading"></HeaderStyle>
										<ItemStyle CssClass="gridField"></ItemStyle>
										<ItemTemplate>
											<asp:TextBox CssClass="gridTextBox" runat="server" ID="txtRemark"></asp:TextBox>
										</ItemTemplate>
									</asp:TemplateColumn>
								</Columns>
							</asp:datagrid></td>
					</tr>
					<tr>
						<td><asp:datagrid id="dgPreviewDtl" runat="server" Height="95px" Width="1800px" SelectedItemStyle-CssClass="gridFieldSelectedINV"
								AutoGenerateColumns="False" ItemStyle-Height="20" OnItemDataBound="dgPreviewDtl_ItemDataBound"
								OnItemCommand="dgPreviewDtl_Button" OnEditCommand="dgPreviewDtl_dgEditDtl">
								<SelectedItemStyle CssClass="gridFieldSelectedINV"></SelectedItemStyle>
								<ItemStyle Height="10px" CssClass="gridField"></ItemStyle>
								<Columns>
									<asp:ButtonColumn ItemStyle-CssClass="gridField" HeaderStyle-CssClass="gridBlueHeading" ButtonType="LinkButton"
										Text="&lt;IMG SRC='images/butt-edit.gif' border=0 title='Edit' &gt;" CommandName="Edit">
										<HeaderStyle Width="10px" CssClass="gridBlueHeading"></HeaderStyle>
										<ItemStyle Width="10px" HorizontalAlign="Center"></ItemStyle>
									</asp:ButtonColumn>
									<asp:BoundColumn DataField="consignment_no" HeaderText="Consignment No.">
										<HeaderStyle HorizontalAlign="Center" CssClass="gridBlueHeading" Wrap="False"></HeaderStyle>
										<ItemStyle CssClass="gridField" Wrap="False"></ItemStyle>
									</asp:BoundColumn>
									<asp:BoundColumn DataField="ref_no" HeaderText="Ref No.">
										<HeaderStyle HorizontalAlign="Center" CssClass="gridBlueHeading" Wrap="False"></HeaderStyle>
										<ItemStyle CssClass="gridField" Wrap="False"></ItemStyle>
									</asp:BoundColumn>
									<asp:BoundColumn DataField="sender_name" HeaderText="Sender">
										<HeaderStyle HorizontalAlign="Center" Width="180px" CssClass="gridBlueHeading" Wrap="False"></HeaderStyle>
										<ItemStyle HorizontalAlign="Left" CssClass="gridField" Wrap="False"></ItemStyle>
									</asp:BoundColumn>
									<asp:BoundColumn DataField="sender_zipcode" HeaderText="Origin">
										<HeaderStyle Width="42px" HorizontalAlign="Center" CssClass="gridBlueHeading" Wrap="False"></HeaderStyle>
										<ItemStyle Width="42px" HorizontalAlign="Left" CssClass="gridField" Wrap="False"></ItemStyle>
									</asp:BoundColumn>
									<asp:BoundColumn DataField="act_pickup_datetime" HeaderText="Pickup" DataFormatString="{0:dd/MM/yyyy HH:mm}">
										<HeaderStyle HorizontalAlign="Center" CssClass="gridBlueHeading" Wrap="False"></HeaderStyle>
										<ItemStyle HorizontalAlign="Left" CssClass="gridField" Wrap="False"></ItemStyle>
									</asp:BoundColumn>
									<asp:BoundColumn DataField="recipient_name" HeaderText="Recipient">
										<HeaderStyle HorizontalAlign="Center" CssClass="gridBlueHeading" Wrap="False"></HeaderStyle>
										<ItemStyle HorizontalAlign="Left" CssClass="gridField" Wrap="False"></ItemStyle>
									</asp:BoundColumn>
									<asp:TemplateColumn HeaderText="Dest.">
										<HeaderStyle Width="42px" HorizontalAlign="Center" CssClass="gridBlueHeading" Wrap="False"></HeaderStyle>
										<ItemStyle Width="42px" HorizontalAlign="Left" CssClass="gridField"></ItemStyle>
										<ItemTemplate>
											<asp:Label Width="42px" CssClass="gridFieldNum" ID="lblDest" Runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"recipient_zipcode")%>'>
											</asp:Label>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:BoundColumn DataField="act_delivery_date" HeaderText="Del.D/T" DataFormatString="{0:dd/MM/yyyy HH:mm}">
										<HeaderStyle Width="112px" HorizontalAlign="Center" CssClass="gridBlueHeading" Wrap="False"></HeaderStyle>
										<ItemStyle Width="112px" HorizontalAlign="Left" CssClass="gridField" Wrap="False"></ItemStyle>
									</asp:BoundColumn>
									<asp:BoundColumn DataField="service_code" HeaderText="Service">
										<HeaderStyle HorizontalAlign="Center" CssClass="gridBlueHeading" Wrap="False"></HeaderStyle>
										<ItemStyle HorizontalAlign="Center" CssClass="gridField" Wrap="False"></ItemStyle>
									</asp:BoundColumn>
									<asp:TemplateColumn HeaderText="Freight">
										<HeaderStyle Width="63px" HorizontalAlign="Center" CssClass="gridBlueHeading" Wrap="False"></HeaderStyle>
										<ItemStyle Width="63px" HorizontalAlign="Right" CssClass="gridField"></ItemStyle>
										<ItemTemplate>
											<asp:Label Width="63px" CssClass="gridFieldNum" ID="lblFreight" Runat="server" Text='<%#String.Format((String)ViewState["m_format"],DataBinder.Eval(Container.DataItem,"tot_freight_charge"))%>'>
											</asp:Label>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Ins.">
										<HeaderStyle Width="49px" HorizontalAlign="Center" VerticalAlign="Middle" CssClass="gridBlueHeading"
											Wrap="False"></HeaderStyle>
										<ItemStyle Width="49px" HorizontalAlign="Right" CssClass="gridField"></ItemStyle>
										<ItemTemplate>
											<asp:Label Width="49px" CssClass="gridFieldNum" ID="lblInsurance" Runat="server" Text='<%#String.Format((String)ViewState["m_format"],DataBinder.Eval(Container.DataItem,"insurance_amt"))%>'>
											</asp:Label>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="VAS">
										<HeaderStyle Width="49px" HorizontalAlign="Center" VerticalAlign="Middle" CssClass="gridBlueHeading"
											Wrap="False"></HeaderStyle>
										<ItemStyle Width="49px" HorizontalAlign="Right" CssClass="gridField"></ItemStyle>
										<ItemTemplate>
											<asp:Label Width="49px" CssClass="gridFieldNum" ID="lblVAS" Runat="server" Text='<%#String.Format((String)ViewState["m_format"],DataBinder.Eval(Container.DataItem,"tot_vas_surcharge"))%>'>
											</asp:Label>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="ESA">
										<HeaderStyle Width="49px" HorizontalAlign="Center" CssClass="gridBlueHeading" Wrap="False"></HeaderStyle>
										<ItemStyle Width="49px" HorizontalAlign="Right" CssClass="gridField"></ItemStyle>
										<ItemTemplate>
											<asp:Label Width="49px" CssClass="gridFieldNum" ID="lblESA" Runat="server" Text='<%#String.Format((String)ViewState["m_format"],DataBinder.Eval(Container.DataItem,"esa_surcharge"))%>'>
											</asp:Label>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Other">
										<HeaderStyle Width="49px" HorizontalAlign="Center" CssClass="gridBlueHeading" Wrap="False"></HeaderStyle>
										<ItemStyle Width="49px" HorizontalAlign="Right" CssClass="gridField"></ItemStyle>
										<ItemTemplate>
											<asp:Label Width="49px" CssClass="gridFieldNum" ID="lblOther" Runat="server" Text='<%#utility.encodeNegValue(DataBinder.Eval(Container.DataItem,"other_surcharge"),(String)ViewState["m_format"])%>' >
											</asp:Label>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Except.">
										<HeaderStyle Width="49px" HorizontalAlign="Center" CssClass="gridBlueHeading" Wrap="False"></HeaderStyle>
										<ItemStyle Width="49px" HorizontalAlign="Right" CssClass="gridField"></ItemStyle>
										<ItemTemplate>
											<asp:Label Width="49px" CssClass="gridFieldNum" ID="lblException" Runat="server" Text='<%#utility.encodeNegValue(DataBinder.Eval(Container.DataItem,"total_exception"),(String)ViewState["m_format"])%>'>
											</asp:Label>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="MBG">
										<HeaderStyle Width="49px" HorizontalAlign="Center" CssClass="gridBlueHeading" Wrap="False"></HeaderStyle>
										<ItemStyle Width="49px" HorizontalAlign="Right" CssClass="gridField"></ItemStyle>
										<ItemTemplate>
											<asp:Label Width="49px" CssClass="gridFieldNum" ID="lblMBG" Runat="server" Text='<%#utility.encodeNegValue(DataBinder.Eval(Container.DataItem,"mbg_amount"),(String)ViewState["m_format"])%>'>
											</asp:Label>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Adj.">
										<HeaderStyle Width="49px" HorizontalAlign="Center" CssClass="gridBlueHeading" Wrap="False"></HeaderStyle>
										<ItemStyle Width="49px" HorizontalAlign="Right" CssClass="gridField"></ItemStyle>
										<ItemTemplate>
											<asp:Label Width="49px" CssClass="gridFieldNum" ID="lblAdjustment" Runat="server" Text='<%#utility.encodeNegValue(DataBinder.Eval(Container.DataItem,"invoice_adj_amount"),(String)ViewState["m_format"])%>'>
											</asp:Label>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Total">
										<HeaderStyle HorizontalAlign="Center" CssClass="gridBlueHeading" Wrap="False"></HeaderStyle>
										<ItemStyle HorizontalAlign="Right" CssClass="gridField"></ItemStyle>
										<ItemTemplate>
											<asp:Label CssClass="gridFieldNum" ID="lblTOT" Runat="server" Text='<%#String.Format((String)ViewState["m_format"],DataBinder.Eval(Container.DataItem,"invoice_amt"))%>'>
											</asp:Label>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:BoundColumn DataField="adjusted_remark" HeaderText="Adjust Remark" Visible="False">
										<HeaderStyle HorizontalAlign="Center" CssClass="gridBlueHeading" Wrap="False"></HeaderStyle>
										<ItemStyle CssClass="gridField" Wrap="False"></ItemStyle>
									</asp:BoundColumn>
								</Columns>
								<PagerStyle NextPageText="Next" PrevPageText="Previous" HorizontalAlign="Left" CssClass="normalText"></PagerStyle>
							</asp:datagrid></td>
					</tr>
					<tr height="20">
					</tr>
				</table>
			</div>
			<asp:panel id="pnlinvupdate" Visible="False" Runat="server" Width="800">
				<TABLE id="table0" cellSpacing="2" cellPadding="0" width="100%" border="0">
					<TR>
						<TD>
							<asp:label id="LblHeading" runat="server" Width="442px" CssClass="mainTitleSize" Height="30px">Invoice Payments Update</asp:label></TD>
						<TD>
							<asp:button id="btnOK" runat="server" Width="85" CssClass="buttonProp" Height="21" Text="Save"></asp:button>&nbsp;
							<asp:button id="btnCancelinvoice" runat="server" CssClass="buttonProp" width="58" text="Cancel"
								height="21"></asp:button></TD>
					</TR>
				</TABLE>
				<TABLE id="table1" cellSpacing="2" cellPadding="0" width="100%" border="0">
					<TR>
						<TD>
							<asp:label id="ErrMsg" runat="server" Width="100%" CssClass="errorMsgColor" Height="16px"></asp:label></TD>
					</TR>
				</TABLE>
				<TABLE id="table2" cellSpacing="2" cellPadding="0" width="100%" border="0">
					<TR>
						<TD width="20%">
							<asp:label id="Label7" runat="server" Width="100%" CssClass="tableLabel" Height="16">Customer ID</asp:label></TD>
						<TD width="20%">
							<asp:textbox id="txtCustID" runat="server" Width="116px" CssClass="textField" ReadOnly="True"></asp:textbox></TD>
						<TD width="20%">
							<asp:label id="lblInvoiceStatus" runat="server" Width="100%" CssClass="tableLabel" Height="16">Invoice Status</asp:label></TD>
						<TD width="40%"><FONT face="Tahoma">
								<asp:label id="lblInvDispStat" runat="server" Width="100px" CssClass="tableLabel" Height="16">Status</asp:label></FONT></TD>
					</TR>
					<TR>
						<TD>
							<asp:label id="Label8" runat="server" Width="100%" CssClass="tableLabel" Height="16">Customer Name</asp:label></TD>
						<TD colSpan="3">
							<asp:label id="lblCustDispName" runat="server" Width="100%" CssClass="tableLabel" Height="16"
								Font-Bold="True">Cust Name</asp:label></TD>
					</TR>
					<TR>
						<TD><FONT face="Tahoma">
								<asp:label id="lblInvNo" runat="server" Width="100%" CssClass="tableLabel" Height="16">Invoice Number</asp:label></FONT></TD>
						<TD><FONT face="Tahoma">
								<asp:textbox id="txtInvNo" runat="server" Width="116px" CssClass="textField" ReadOnly="True"></asp:textbox></FONT></TD>
						<TD>
							<asp:label id="lblLastUpdate" runat="server" Width="100%" CssClass="tableLabel" Height="16"
								Font-Bold="True"> Last Updated By/Date</asp:label></TD>
						<TD>
							<asp:label id="lblDispUpdate" runat="server" Width="100%" CssClass="tableLabel" Height="16"
								Font-Bold="True">Updated By / Date</asp:label></TD>
					</TR>
					<TR>
						<TD>
							<asp:label id="lblPrintedDueDate" runat="server" Width="100%" CssClass="tableLabel" Height="16"
								Font-Bold="True">Printed Due Date</asp:label></TD>
						<TD>
							<cc1:mstextbox id="txtPrintedDueDate" tabIndex="3" runat="server" Width="116px" CssClass="textField"
								MaxLength="16" TextMaskType="msDate" TextMaskString="99/99/9999" AutoPostBack="True" ReadOnly="True"></cc1:mstextbox></TD>
						<TD>
							<asp:label id="lblApproveDate" runat="server" Width="100%" CssClass="tableLabel" Height="16"
								Font-Bold="True"> Approved By/Date</asp:label></TD>
						<TD>
							<asp:label id="lblDispApprove" runat="server" Width="100%" CssClass="tableLabel" Height="16"
								Font-Bold="True">Approved By / Date</asp:label></TD>
					</TR>
					<TR>
						<TD>
							<asp:label id="lblInternalDueDate" runat="server" Width="100%" CssClass="tableLabel" Height="16"
								Font-Bold="True">Internal Due Date</asp:label></TD>
						<TD>
							<cc1:mstextbox id="txtInternalDueDate" tabIndex="3" runat="server" Width="116px" CssClass="textField"
								MaxLength="16" TextMaskType="msDate" TextMaskString="99/99/9999" AutoPostBack="True" ReadOnly="True"></cc1:mstextbox></TD>
						<TD></TD>
						<TD></TD>
					</TR>
					<TR>
						<TD>
							<asp:label id="lblActualBPD" runat="server" Width="100%" CssClass="tableLabel" Height="16">Actual BPD</asp:label></TD>
						<TD>
							<cc1:mstextbox id="txtActualBPD" tabIndex="3" runat="server" Width="116px" CssClass="textField"
								MaxLength="16" TextMaskType="msDate" TextMaskString="99/99/9999" AutoPostBack="True" ReadOnly="True"></cc1:mstextbox></TD>
						<TD>
							<asp:label id="lblBatchNo" runat="server" Width="100%" CssClass="tableLabel" Height="16">Batch Number</asp:label></TD>
						<TD>
							<asp:textbox id="txtBatchNo" runat="server" Width="116px" CssClass="textField" ReadOnly="True"></asp:textbox></TD>
					</TR>
					<TR>
						<TD>
							<asp:label id="lblPPD" runat="server" Width="100%" CssClass="tableLabel" Height="16">Promised Payment Date</asp:label></TD>
						<TD>
							<cc1:mstextbox id="txtPromisedPaymentDate" tabIndex="3" runat="server" Width="116px" CssClass="textField"
								MaxLength="16" TextMaskType="msDate" TextMaskString="99/99/9999" AutoPostBack="True" ReadOnly="True"></cc1:mstextbox></TD>
						<TD>
							<asp:label id="lblTotalInvAmnt" runat="server" Width="100%" CssClass="tableLabel" Height="16">Total Invoice Amount</asp:label></TD>
						<TD>
							<cc1:mstextbox id="txtTotalInvAmnt" CssClass="textField" Height="21px" Text="" width="116px" Runat="server"
								MaxLength="11" TextMaskType="msNumeric" TextMaskString="#.00" AutoPostBack="false" ReadOnly="True"
								NumberScale="2" NumberPrecision="10" NumberMinValue="0" NumberMaxValue="99999999"></cc1:mstextbox></TD>
					</TR>
					<TR>
						<TD>
							<asp:label id="lblAmntDate" runat="server" Width="100%" CssClass="tableLabel" Height="16">Amount Paid</asp:label></TD>
						<TD>
							<cc1:mstextbox id="txtAmntPaid" CssClass="textField" Text="" width="116px" Runat="server" MaxLength="11"
								TextMaskType="msNumeric" TextMaskString="#.00" AutoPostBack="false" ReadOnly="True" NumberScale="2"
								NumberPrecision="10" NumberMinValue="-99999999" NumberMaxValue="99999999" NumberMaxValueCOD="99999999"
								NumberMinValueCustVAS="-99999999"></cc1:mstextbox></TD>
						<TD>
							<asp:label id="lblCurrentAmntPaid" runat="server" Width="100%" CssClass="tableLabel" Height="16">Current Amount Paid</asp:label></TD>
						<TD>
							<cc1:mstextbox id="txtCurrAmntPaid" CssClass="textField" Height="21px" Text="" width="116px" Runat="server"
								MaxLength="11" TextMaskType="msNumeric" TextMaskString="#.00" AutoPostBack="false" ReadOnly="True"
								NumberScale="2" NumberPrecision="10" NumberMinValue="0" NumberMaxValue="99999999"></cc1:mstextbox>
							<cc1:mstextbox id="hidtxtCurrAmt" style="DISPLAY: none" CssClass="textField" Height="21px" Text=""
								width="0px" Runat="server" MaxLength="11" TextMaskType="msNumeric" TextMaskString="#.00" AutoPostBack="false"
								ReadOnly="True" NumberScale="2" NumberPrecision="10" NumberMinValue="0" NumberMaxValue="99999999"></cc1:mstextbox></TD>
						</TD></TR>
					<TR>
						<TD><FONT face="Tahoma">
								<asp:label id="lblPaidDate" runat="server" Width="100%" CssClass="tableLabel" Height="16">Paid Date</asp:label></FONT></TD>
						<TD>
							<cc1:mstextbox id="txtPaidDate" tabIndex="3" runat="server" Width="116px" CssClass="textField"
								MaxLength="16" TextMaskType="msDate" TextMaskString="99/99/9999" AutoPostBack="True" ReadOnly="True"></cc1:mstextbox></TD>
						<TD>
							<asp:label id="lblBalanceDue" runat="server" Width="100%" CssClass="tableLabel" Height="16">Balance Due</asp:label></TD>
						<TD>
							<cc1:mstextbox id="txtBalanceDue" CssClass="textField" Text="" width="116px" Runat="server" MaxLength="11"
								TextMaskType="msNumeric" TextMaskString="#.00" AutoPostBack="false" ReadOnly="True" NumberScale="2"
								NumberPrecision="10" NumberMinValue="0" NumberMaxValue="99999999"></cc1:mstextbox>
							<cc1:mstextbox id="hidtxtBalanceDue" style="DISPLAY: none" CssClass="textField" Height="21px" Text=""
								width="0px" Runat="server" MaxLength="11" TextMaskType="msNumeric" TextMaskString="#.00" AutoPostBack="false"
								ReadOnly="True" NumberScale="2" NumberPrecision="10" NumberMinValue="0" NumberMaxValue="99999999"></cc1:mstextbox></TD>
						</TD></TR>
					<TR>
						<TD colspan="4">
							<asp:label id="lblPaymentUpd" runat="server" Width="100%" CssClass="tableLabel" Height="16"
								Font-Bold="True">Payment Updated By/Date</asp:label></TD>
					</TR>
					<TR>
						<TD>
							<asp:label id="Label9" runat="server" Width="100%" CssClass="tableLabel" Height="16" Visible="False">Link to Credit Note</asp:label></TD>
						<TD>
							<asp:dropdownlist id="ddlLinkToCDN" tabIndex="13" runat="server" Width="116px" CssClass="textField"
								Visible="False" Enabled="False"></asp:dropdownlist></TD>
						<TD>
							<asp:label id="lblApplyAmt" style="DISPLAY: none" runat="server" Width="100%" CssClass="tableLabel"
								Height="16">Applied Amount</asp:label></TD>
						<TD>
							<cc1:mstextbox id="txtApplyAmt" style="DISPLAY: none" CssClass="textField" Text="" width="116px"
								Runat="server" MaxLength="11" TextMaskType="msNumeric" TextMaskString="#.00" AutoPostBack="false"
								ReadOnly="False" NumberScale="2" NumberPrecision="10" NumberMaxValue="99999999" NumberMaxValueCOD="99999999"
								NumberMinValueCustVAS="0"></cc1:mstextbox></TD>
					</TR>
				</TABLE>
				<TABLE id="table3" cellSpacing="2" cellPadding="0" width="100%" border="0">
					<TR>
						<TD>
							<asp:label id="Label12" runat="server" Width="442px" CssClass="mainTitleSize" Height="30px">Associated Credit and Debit Notes</asp:label></TD>
					</TR>
					<TR>
						<TD>
							<asp:datagrid id="dgAssociatedCNDN" Width="100%" ItemStyle-Height="20" AutoGenerateColumns="False"
								Runat="server" AllowPaging="True">
								<ItemStyle Height="20px" CssClass="gridField"></ItemStyle>
								<Columns>
									<asp:BoundColumn DataField="CNDN" HeaderText="CR/DB">
										<HeaderStyle HorizontalAlign="Center" Width="20%" CssClass="gridHeading"></HeaderStyle>
										<ItemStyle HorizontalAlign="Center"></ItemStyle>
									</asp:BoundColumn>
									<asp:BoundColumn DataField="CNDN_NO" HeaderText="Note ID">
										<HeaderStyle HorizontalAlign="Center" Width="40%" CssClass="gridHeading"></HeaderStyle>
									</asp:BoundColumn>
									<asp:BoundColumn DataField="STAT_TEXT" HeaderText="Status">
										<HeaderStyle HorizontalAlign="Center" Width="20%" CssClass="gridHeading"></HeaderStyle>
									</asp:BoundColumn>
									<asp:BoundColumn DataField="total_amnt" HeaderText="Total Amount">
										<HeaderStyle HorizontalAlign="Center" Width="20%" CssClass="gridHeading"></HeaderStyle>
									</asp:BoundColumn>
								</Columns>
								<PagerStyle NextPageText="Next" PrevPageText="Previous" HorizontalAlign="Left" CssClass="normalText"></PagerStyle>
							</asp:datagrid>
							<asp:textbox id="txtUserName" style="DISPLAY: none" runat="server" ReadOnly="True"></asp:textbox></TD>
					</TR>
					</TR></TABLE>
			</asp:panel>
		</FORM>
		</FONT></TR></TBODY></TABLE></FORM>
	</BODY>
</HTML>
