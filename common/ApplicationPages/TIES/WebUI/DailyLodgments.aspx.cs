using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using TIESDAL;
using com.common.classes;
using com.common.DAL;
using com.common.RBAC;
using com.ties.DAL;
using com.common.util;

using com.common.applicationpages;
using com.ties;
using System.Text;
using System.Globalization;
using System.Data.OleDb;
using System.IO;


namespace TIES.WebUI
{
	/// <summary>
	/// Summary description for DailyLodgments.
	/// </summary>
	public class DailyLodgments : BasePage
	{
		protected System.Web.UI.WebControls.Label Label1;
		protected System.Web.UI.WebControls.Button btnQry;
		protected System.Web.UI.WebControls.Button btnExecQry;
		protected System.Web.UI.WebControls.Button btnInsert;
		protected System.Web.UI.WebControls.Button btnDelete;
		protected System.Web.UI.WebControls.Label lblErrorMsg;
		protected System.Web.UI.WebControls.ValidationSummary PageValidationSummary;
		protected System.Web.UI.HtmlControls.HtmlGenericControl divMain;
		protected System.Web.UI.WebControls.Label lbl1;
		protected com.common.util.msTextBox Txt_BatchDate;
		protected System.Web.UI.WebControls.Label Label2;
		protected System.Web.UI.WebControls.DropDownList Drp_ServiceType;
		protected System.Web.UI.WebControls.Label Label3;
		protected System.Web.UI.WebControls.DataGrid dgLodgments;
		protected System.Web.UI.HtmlControls.HtmlTable MainTable;
	
		private string m_strAppID;
		protected System.Web.UI.WebControls.ValidationSummary Validationsummary1;
		protected System.Web.UI.WebControls.Button btnPrintScanSheet;
		protected System.Web.UI.WebControls.Button btnPrintLodgementSummary;
		protected System.Web.UI.WebControls.Button btnManifestScannedPkgs;
		protected System.Web.UI.WebControls.Button btnPrintManifest;
		protected System.Web.UI.WebControls.Button btnExport;
		protected System.Web.UI.HtmlControls.HtmlGenericControl divExport;
		protected System.Web.UI.WebControls.Button btnExceedCancel;
		protected System.Web.UI.WebControls.Button btnExceedOK;
		protected System.Web.UI.HtmlControls.HtmlTable Table2;
		protected com.common.util.msTextBox Txt_Exp_DateFrom;
		protected com.common.util.msTextBox Txt_Exp_DateTo;
		protected System.Web.UI.WebControls.DropDownList Drp_Location;
		protected System.Web.UI.WebControls.DropDownList Drp_Exp_Location;
		protected System.Web.UI.WebControls.Button btnHidReport;
		private string m_strEnterpriseID;

		private System.Data.DataSet DbDataSource
		{
			get
			{
				if(ViewState["DbDataSource"] == null)
				{
					return null;
				}
				else
				{
					return (DataSet)ViewState["DbDataSource"];
				}
			}
			set
			{
				ViewState["DbDataSource"]=value;
			}
		}

		private int LifeOfBatch
		{
			get
			{
				if(ViewState["LifeOfBatch"] == null)
				{
					return 5;
				}
				else
				{
					return (int)ViewState["LifeOfBatch"];
				}
			}
			set
			{
				ViewState["LifeOfBatch"]=value;
			}
		}
		private bool BatchExpired
		{
			get
			{
				if(ViewState["BatchExpired"] == null)
				{
					return true;
				}
				else
				{
					return (bool)ViewState["BatchExpired"];
				}
			}
			set
			{
				ViewState["BatchExpired"]=value;
			}
		}
		private int DataSourceInsertIndex
		{
			get
			{
				if(ViewState["DataSourceInsertIndex"] == null)
				{
					return -1;
				}
				else
				{
					return (int)ViewState["DataSourceInsertIndex"];
				}
			}
			set
			{
				ViewState["DataSourceInsertIndex"]=value;
			}
		}

		private int GridPageSize
		{
			get
			{
				if(ViewState["GridPageSize"] == null)
				{
					return 50;
				}
				else
				{
					return (int)ViewState["GridPageSize"];
				}
			}
			set
			{
				ViewState["GridPageSize"]=value;
			}
		}

		private string BatchDate
		{
			get
			{
				if(ViewState["BatchDate"] == null)
				{
					return "";
				}
				else
				{
					return (string)ViewState["BatchDate"];
				}
			}
			set
			{
				ViewState["BatchDate"]=value;
			}
		}

		private System.Data.DataSet AirlinesFormat
		{
			get
			{
				if(ViewState["AirlinesFormat"] == null)
				{
					return null;
				}
				else
				{
					return (System.Data.DataSet)ViewState["AirlinesFormat"];
				}
			}
			set
			{
				ViewState["AirlinesFormat"]=value;
			}
		}

		private void Page_Load(object sender, System.EventArgs e)
		{
			m_strAppID = utility.GetAppID();
			m_strEnterpriseID = utility.GetEnterpriseID();

			if(!Page.IsPostBack)
			{
				GridPageSize= Int32.Parse(System.Configuration.ConfigurationSettings.AppSettings["DailyLodgmentsRecordPerPage"]);
				DataSet dsDayToRead = BatchManifest.GetDayToRead(m_strAppID,m_strEnterpriseID);
				if(dsDayToRead.Tables[0].Rows.Count>0)
				{
					this.LifeOfBatch= int.Parse(dsDayToRead.Tables[0].Rows[0]["code_str_value"].ToString());
				}				
				BindAirlines();
				ResetControl();

				SetInitialSelect(Txt_BatchDate);
				//BindDataSource();
				//BindGrid();
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			base.OnInit(e);
			InitializeComponent();
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnQry.Click += new System.EventHandler(this.btnQry_Click);
			this.btnExecQry.Click += new System.EventHandler(this.btnExecQry_Click);
			this.btnInsert.Click += new System.EventHandler(this.btnInsert_Click);
			this.btnPrintScanSheet.Click += new System.EventHandler(this.btnPrintScanSheet_Click);
			this.btnPrintLodgementSummary.Click += new System.EventHandler(this.btnPrintLodgementSummary_Click);
			this.btnManifestScannedPkgs.Click += new System.EventHandler(this.btnManifestScannedPkgs_Click);
			this.btnPrintManifest.Click += new System.EventHandler(this.btnPrintManifest_Click);
			this.btnExport.Click += new System.EventHandler(this.btnExport_Click);
			this.dgLodgments.ItemCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dgLodgments_ItemCommand);
			this.dgLodgments.PageIndexChanged += new System.Web.UI.WebControls.DataGridPageChangedEventHandler(this.dgLodgments_PageIndexChanged);
			this.dgLodgments.CancelCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dgLodgments_CancelCommand);
			this.dgLodgments.EditCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dgLodgments_EditCommand);
			this.dgLodgments.UpdateCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dgLodgments_UpdateCommand);
			this.dgLodgments.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.dgLodgments_ItemDataBound);
			this.dgLodgments.SelectedIndexChanged += new System.EventHandler(this.dgLodgments_SelectedIndexChanged);
			this.btnExceedOK.Click += new System.EventHandler(this.btnExceedOK_Click);
			this.btnExceedCancel.Click += new System.EventHandler(this.btnExceedCancel_Click);
			this.btnHidReport.Click += new System.EventHandler(this.btnHidReport_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
		public static void SetInitialSelect(Control control) 
		{ 
			if (control.Page == null) 
			{ 
				throw new ArgumentException( 
					"The Control must be added to a Page before you can set the IntialFocus to it."); 
			} 
			if (control.Page.Request.Browser.JavaScript == true) 
			{ 
				// Create JavaScript 
				StringBuilder s = new StringBuilder(); 
				s.Append("\n<SCRIPT LANGUAGE='JavaScript'>\n"); 
				s.Append("<!--\n"); 
				s.Append("function SetInitialFocus()\n"); 
				s.Append("{\n"); 
				s.Append("   document."); 

				// Find the Form 
				Control p = control.Parent; 
				while (!(p is System.Web.UI.HtmlControls.HtmlForm)) 
					p = p.Parent; 
				s.Append(p.ClientID); 

				s.Append("['"); 
				s.Append(control.UniqueID); 
				s.Append("'].select();\n"); 
				s.Append("}\n"); 
				s.Append("// -->\n"); 
				s.Append("window.onload = SetInitialFocus;\n"); 
				s.Append("</SCRIPT>"); 

				// Register Client Script 
				control.Page.RegisterClientScriptBlock("InitialFocus", s.ToString()); 
			} 
		} 

		private string DateDbFormat(string localDate)
		{
			string serverdate="";
			if(localDate.Trim() != "")
			{
				try
				{
					DateTime dBatchDate = DateTime.ParseExact(localDate.Trim(),"dd/MM/yyyy",null);
					serverdate = dBatchDate.ToString("yyyy-MM-dd");
				}
				catch
				{
					serverdate = null;
				}				
			}
			return serverdate;
		}

		private string DateTimeDbFormat(string localDate)
		{
			string serverdate=null;
			if(localDate.Trim() != "")
			{
				try
				{
					DateTime dBatchDate = DateTime.ParseExact(localDate.Trim(),"dd/MM/yyyy HH:mm",null);
					serverdate = dBatchDate.ToString("yyyy-MM-dd HH:mm");
				}
				catch
				{
					serverdate = null;
				}				
			}
			return serverdate;
		}

		private void ResetControl()
		{
			lblErrorMsg.Text ="";
			this.divMain.Visible=true;
			this.divExport.Visible=false;
			BatchDate="";
			DataSourceInsertIndex=-1;
			DbDataSource = null;
			ViewState["PRBMode"]=ScreenMode.None;
			ViewState["PRBOperation"]=Operation.None;

			btnInsert.Enabled=false;
			btnPrintManifest.Enabled=false;
			btnManifestScannedPkgs.Enabled=false;
//			btnMovenext.Enabled =false;
//			btnMovePrevious.Enabled = false;
//			btnMovefirst.Enabled=false;
//			btnMoveLast.Enabled =false; 
			lblErrorMsg.Text="";
			Txt_BatchDate.Text=DateTime.Now.ToString("dd/MM/yyyy");
			Txt_BatchDate.ReadOnly = false;
			Txt_BatchDate.TextMaskString="99/99/9999";
			Txt_BatchDate.TextMaskType =  com.common.util.MaskType.msDate;

			BindServiceType();
			BindLocation();
			this.dgLodgments.CurrentPageIndex = 0;
			this.dgLodgments.EditItemIndex=-1;
			this.dgLodgments.SelectedIndex=-1;
			this.dgLodgments.PageSize=this.GridPageSize;

			string userLocation = Session["UserLocation"].ToString();						
			Drp_Location.SelectedValue=userLocation;
			string userRole = Session["UserRole"].ToString();
			if(userRole.ToUpper().IndexOf("OPSSU") != -1)
			{
				//Drp_Location.Enabled=true;
				if(btnExecQry.Enabled)
				{
					Drp_Location.Enabled=true;				
				}
				else
				{
					Drp_Location.Enabled=false;
				}				
			}
			else
			{
				Drp_Location.Enabled=false;	
			}

			string sBatchDate = DateDbFormat(Txt_BatchDate.Text.Trim());
			BatchDate = sBatchDate;
			DataSet ds = TIESDAL.DailyLodgmentsMgrDAL.QueryDailyLodgments(m_strAppID,m_strEnterpriseID, sBatchDate,"0","0");
			DataRow dr = ds.Tables[0].NewRow();
			ds.Tables[0].Rows.Add(dr);
			ds.Tables[0].AcceptChanges();
			DbDataSource = ds;
			BatchExpired=false;
			BindGrid();
		}

		private void BindServiceType()
		{
			this.Drp_ServiceType.DataSource=com.ties.classes.DbComboDAL.GetAllServiceCode(m_strAppID,m_strEnterpriseID);
			this.Drp_ServiceType.DataTextField="DbComboText";
			this.Drp_ServiceType.DataValueField="DbComboValue";
			this.Drp_ServiceType.DataBind();
			
			//Drp_ServiceType.Items.Insert(0,new ListItem("",""));
		}

		private void BindLocation()
		{
			Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings,HttpContext.Current.Session);
			string strAppID = util.GetAppID();
			string strEnterpriseID = util.GetEnterpriseID();

			DataSet dataset = com.ties.classes.DbComboDAL.UserLocationQuery(strAppID,strEnterpriseID);	
			this.Drp_Location.DataSource = dataset;
			Drp_Location.DataTextField = dataset.Tables[0].Columns["origin_code"].ToString();
			Drp_Location.DataValueField = dataset.Tables[0].Columns["origin_code"].ToString();
			this.Drp_Location.DataBind();
			Drp_Location.Items.Insert(0,new ListItem("",""));


			this.Drp_Exp_Location.DataSource = dataset;
			Drp_Exp_Location.DataTextField = dataset.Tables[0].Columns["origin_code"].ToString();
			Drp_Exp_Location.DataValueField = dataset.Tables[0].Columns["origin_code"].ToString();
			this.Drp_Exp_Location.DataBind();
			Drp_Exp_Location.Items.Insert(0,new ListItem("ALL","0"));
		}

		private void BindAirlines()
		{
			Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings,HttpContext.Current.Session);
			string strAppID = util.GetAppID();
			string strEnterpriseID = util.GetEnterpriseID();

			DataSet dataset = ImportConsignmentsDAL.GetCoreSysetmCode(strAppID, strEnterpriseID, "Airlines", 0, 0, "PNGAF");
			this.AirlinesFormat = dataset;
		}

		private void BindDataSource()
		{
			string sBatchDate = DateDbFormat(Txt_BatchDate.Text.Trim());
			BatchDate = sBatchDate;
			DataSet ds = TIESDAL.DailyLodgmentsMgrDAL.QueryDailyLodgments(m_strAppID,m_strEnterpriseID, sBatchDate
				,this.Drp_Location.SelectedValue
				,this.Drp_ServiceType.SelectedValue);
			DbDataSource = ds;

			DateTime cNow = new DateTime(DateTime.Now.Year,DateTime.Now.Month,DateTime.Now.Day);
			DateTime cBatchDate = new DateTime(DateTime.Now.Year,DateTime.Now.Month,DateTime.Now.Day);
			if(Txt_BatchDate.Text.Trim() != "")
			{
				cBatchDate= DateTime.ParseExact(Txt_BatchDate.Text.Trim(),"dd/MM/yyyy",null);
			}
			System.TimeSpan Diff =cNow.Subtract(cBatchDate);
			if(Diff.Days > this.LifeOfBatch)
			{
				BatchExpired=false;
			}
			else
			{
				BatchExpired=true;
			}
			btnInsert.Enabled=BatchExpired;
			btnManifestScannedPkgs.Enabled=BatchExpired;
		}

		private void BindGrid()
		{			
//			this.dgLodgments.Columns[0].Visible=BatchExpired;
			this.dgLodgments.Columns[1].Visible=BatchExpired;
			this.dgLodgments.DataSource = DbDataSource;
			this.dgLodgments.DataBind();
		}

		private void btnExecQry_Click(object sender, System.EventArgs e)
		{			
			ViewState["CommandState"] = null; 

			btnExecQry.Enabled = false;
			Txt_BatchDate.Enabled = false; // Thosapol.y Add Code (28/06/2013)
			Drp_ServiceType.Enabled = false; // Thosapol.y Add Code (28/06/2013)
			Drp_Location.Enabled = false; // Thosapol.y Add Code (28/06/2013)
			lblErrorMsg.Text="";
			BindDataSource();
			BindGrid();
		}

		private void btnQry_Click(object sender, System.EventArgs e)
		{
			btnExecQry.Enabled=true;
			Txt_BatchDate.Enabled =true; // Thosapol.y Add Code (28/06/2013)
			Drp_ServiceType.Enabled =true; // Thosapol.y Add Code (28/06/2013)
			ResetControl();
			//BindDataSource();
			BindGrid();
			SetInitialSelect(Txt_BatchDate);
		}

		private void Txt_BatchDate_TextChanged(object sender, System.EventArgs e)
		{
		
		}

		private void dgLodgments_EditCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
		{
			int idx=e.Item.ItemIndex;
			lblErrorMsg.Text ="";
			btnManifestScannedPkgs.Enabled=false;
			btnPrintManifest.Enabled=false;
			dgLodgments.SelectedIndex=-1;
			dgLodgments.EditItemIndex = e.Item.ItemIndex;
			ViewState["ConvOperation"] = Operation.Update;
			BindGrid();
			if(dgLodgments.Items[idx].FindControl("txtAWB_Number") != null)
			{
				TextBox txtAWB_Number = (TextBox)dgLodgments.Items[idx].FindControl("txtAWB_Number");
				SetInitialFocus(txtAWB_Number);
			}

			ViewState["CommandState"] = "EDIT";
		}
		public static void SetInitialFocus(Control control) 
		{ 
			if (control.Page == null) 
			{ 
				throw new ArgumentException( 
					"The Control must be added to a Page before you can set the IntialFocus to it."); 
			} 
			if (control.Page.Request.Browser.JavaScript == true) 
			{ 
				// Create JavaScript 
				StringBuilder s = new StringBuilder(); 
				s.Append("\n<SCRIPT LANGUAGE='JavaScript'>\n"); 
				s.Append("<!--\n"); 
				s.Append("function SetInitialFocus()\n"); 
				s.Append("{\n"); 
				s.Append("   document."); 

				// Find the Form 
				Control p = control.Parent; 
				while (!(p is System.Web.UI.HtmlControls.HtmlForm)) 
					p = p.Parent; 
				s.Append(p.ClientID); 

				s.Append("['"); 
				s.Append(control.ClientID); 
				s.Append("'].focus();\n"); 
				s.Append("}\n"); 
				s.Append("// -->\n"); 
				s.Append("window.onload = SetInitialFocus;\n"); 
				s.Append("</SCRIPT>"); 

				// Create JavaScript 
//				StringBuilder s = new StringBuilder(); 
//				s.Append("<SCRIPT LANGUAGE='JavaScript'>"); 
//				s.Append("document.getElementById('"); 
//				s.Append(control.ClientID); 
//				s.Append("').focus();\n"); 		
//				s.Append("SetInitialFocus();"); 
//				s.Append("</SCRIPT>"); 

				// Register Client Script 
				control.Page.RegisterClientScriptBlock("InitialFocus", s.ToString()); 
			} 
		}


		private void dgLodgments_UpdateCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
		{
			
			HtmlInputHidden hBatchID = (HtmlInputHidden)e.Item.FindControl("hBatchID");
			HtmlInputHidden hBatchNumber = (HtmlInputHidden)e.Item.FindControl("hBatchNumber");
			string Batch_no = null;
			string BatchID = null;
			string BatchDate = null;
			string Created_By  = null;
			string ServiceType = null;
			string DepartureDT = null;
			string ArrivalDT = null;
			string FlightNo = null;
			string AWB = null;

			DataSet ds = this.DbDataSource;
			if(hBatchID != null)
			{
				BatchID = hBatchID.Value;
			}
			if(hBatchNumber != null && hBatchNumber.Value.Trim().Length > 0)
			{
				Batch_no = hBatchNumber.Value;
			}
			else{
				Batch_no = null;
			}

			BatchDate = this.BatchDate;
			TextBox txtAWB_Number = (TextBox)e.Item.FindControl("txtAWB_Number");
			TextBox txtFlight_Number = (TextBox)e.Item.FindControl("txtFlight_Number");
			msTextBox txtDepartureDT = (msTextBox)e.Item.FindControl("txtDepartureDT");
			msTextBox txtArrivalDT = (msTextBox)e.Item.FindControl("txtArrivalDT");
			AWB = txtAWB_Number.Text.Trim().ToUpper();
			FlightNo = txtFlight_Number.Text.Trim().ToUpper();
			DepartureDT = DateTimeDbFormat(txtDepartureDT.Text);
			ArrivalDT = DateTimeDbFormat(txtArrivalDT.Text);
			Created_By = Session["userID"].ToString();
			ServiceType = this.Drp_ServiceType.SelectedValue;
			if(AWB.Length>0 && this.AirlinesFormat != null)
			{
				DataSet dsAirlinesFormat = this.AirlinesFormat;
				bool isValidFormat = false;
				foreach(DataRow dr in dsAirlinesFormat.Tables[0].Rows)
				{
					int idx = AWB.IndexOf(dr["code_text"].ToString());
					if(idx == 0)
					{
						isValidFormat = true;
						break;
					}
				}
				if(isValidFormat==false)
				{
					lblErrorMsg.Text = "The starting two letters of AWB. must be one of the allowable airline codes.";					
					return;
				}
			}

			if(FlightNo.Length >0 && this.AirlinesFormat != null)
			{
				DataSet dsAirlinesFormat = this.AirlinesFormat;
				bool isValidFormat = false;
				foreach(DataRow dr in dsAirlinesFormat.Tables[0].Rows)
				{
					int idx = FlightNo.IndexOf(dr["code_text"].ToString());
					if(idx == 0)
					{
						isValidFormat = true;
						break;
					}
				}
				if(isValidFormat==false)
				{
					lblErrorMsg.Text = "The starting two letters of Flight No. must be one of the allowable airline codes.";					
					return;
				}
			}

			try
			{
				int roweffect= DailyLodgmentsMgrDAL.UpdateDailyLodgment(m_strAppID,m_strEnterpriseID,
					Batch_no,BatchID,BatchDate,Created_By,
					ServiceType,DepartureDT,ArrivalDT,FlightNo,AWB);
				lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"INS_SUCCESSFULLY",utility.GetUserCulture());
			}
			catch(Exception err)
			{
				string msg = err.ToString();
				lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"ERROR",utility.GetUserCulture());
			}


			this.btnInsert.Enabled=false;
			DataSourceInsertIndex=-1;
			ViewState["ConvOperation"] = Operation.None;
			dgLodgments.PageSize= this.GridPageSize;
			dgLodgments.EditItemIndex = -1;
			BindDataSource();
			BindGrid();		
		}

		private void dgLodgments_CancelCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
		{
			if(DataSourceInsertIndex != -1)
			{
				int current_page=dgLodgments.CurrentPageIndex * dgLodgments.PageSize;
				DataSet ds = this.DbDataSource;	

				if((ViewState["CommandState"].ToString()) == "INSERT")
				{
					ds.Tables[0].Rows.RemoveAt(DataSourceInsertIndex+current_page);	
				}
				
				this.DbDataSource= ds;
				DataSourceInsertIndex=-1;
			}
			DataSourceInsertIndex=-1;
			this.btnInsert.Enabled=false;
			btnManifestScannedPkgs.Enabled=false;
			btnPrintManifest.Enabled=false;

			ViewState["ConvOperation"] = Operation.None;
			dgLodgments.PageSize= this.GridPageSize;
			dgLodgments.EditItemIndex = -1;
			dgLodgments.SelectedIndex = -1;
			BindGrid();
		}

		private void dgLodgments_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			lblErrorMsg.Text ="";
			DataSourceInsertIndex=dgLodgments.SelectedIndex+1;		
			string userRole = Session["UserRole"].ToString();
			if(userRole.ToUpper().IndexOf("OPSSR") != -1)
			{
				btnPrintManifest.Enabled=true;				
				//Drp_Location.Enabled=true;	
				if(btnExecQry.Enabled)
				{
					Drp_Location.Enabled=true;				
				}
				else
				{
					Drp_Location.Enabled=false;
				}

				DataSet ds = this.DbDataSource;
				DataRow drSelected = ds.Tables[0].Rows[dgLodgments.SelectedIndex];
				if(drSelected["DepartureDT"] == DBNull.Value)
				{
					btnManifestScannedPkgs.Enabled=true;
				}
				else
				{
					btnManifestScannedPkgs.Enabled=false;
				}

				if(drSelected["BatchNumber"] != DBNull.Value && drSelected["BatchNumber"].ToString().Trim() != "")
				{
					btnPrintManifest.Enabled=true;
				}
				else
				{
					btnPrintManifest.Enabled=false;
				}

				int Scanned_Pkg_Count = int.Parse(string.Format("{0}", drSelected["Scanned_Pkg_Count"]));

				if( Scanned_Pkg_Count > 0)
				{
					btnPrintManifest.Enabled=true;
				}
				else
				{
					btnPrintManifest.Enabled=false;
				}
			}
			else
			{
				btnManifestScannedPkgs.Enabled=false;
				btnPrintManifest.Enabled=false;
				Drp_Location.Enabled=false;	
			}
			btnInsert.Enabled=BatchExpired;
			btnManifestScannedPkgs.Enabled=BatchExpired;

			string userLocation = Session["UserLocation"].ToString();						
			Drp_Location.SelectedValue=userLocation;
			if(userRole.ToUpper().IndexOf("OPSSU") != -1)
			{
				//Drp_Location.Enabled=true;	
				if(btnExecQry.Enabled)
				{
					Drp_Location.Enabled=true;				
				}
				else
				{
					Drp_Location.Enabled=false;
				}
			}
			else
			{
				Drp_Location.Enabled=false;	
			}
			SetInitialFocus(dgLodgments.SelectedItem.Cells[0]);			
		}

		private void dgLodgments_ItemCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
		{
			
		}

		private void btnInsert_Click(object sender, System.EventArgs e)
		{
			lblErrorMsg.Text ="";
			if(DataSourceInsertIndex != -1)
			{
				int current_page=dgLodgments.CurrentPageIndex * dgLodgments.PageSize;
				HtmlInputHidden hSelectedBatchID = (HtmlInputHidden)this.dgLodgments.Items[DataSourceInsertIndex-1].FindControl("hSelectedBatchID");
				HtmlInputHidden hSelectedBatchNumber = (HtmlInputHidden)this.dgLodgments.Items[DataSourceInsertIndex-1].FindControl("hSelectedBatchNumber");
				HtmlInputHidden hSelectedDest_Pkg_Count = (HtmlInputHidden)this.dgLodgments.Items[DataSourceInsertIndex-1].FindControl("hSelectedDest_Pkg_Count");
				HtmlInputHidden hSelectedScanned_Pkg_Count = (HtmlInputHidden)this.dgLodgments.Items[DataSourceInsertIndex-1].FindControl("hSelectedScanned_Pkg_Count");

				btnPrintManifest.Enabled=false;
				btnManifestScannedPkgs.Enabled=false;
				this.dgLodgments.SelectedIndex=-1;
				DataSet ds = this.DbDataSource;
				DataRow dr = ds.Tables[0].NewRow();				
				dr["BatchID"]=hSelectedBatchID.Value;
				if(hSelectedDest_Pkg_Count.Value.Trim().Length>0)
				{
					dr["Dest_Pkg_Count"]=hSelectedDest_Pkg_Count.Value;
				}
				if(hSelectedScanned_Pkg_Count.Value.Trim().Length>0)
				{
					dr["Scanned_Pkg_Count"]=hSelectedScanned_Pkg_Count.Value;
				}
				
				
				ds.Tables[0].Rows.InsertAt(dr,DataSourceInsertIndex + current_page);
				if(DataSourceInsertIndex >= this.dgLodgments.PageSize)
				{
					this.dgLodgments.PageSize = DataSourceInsertIndex + 1;
				}
				this.dgLodgments.EditItemIndex=DataSourceInsertIndex;
				this.DbDataSource= ds;
				this.BindGrid();
				if(dgLodgments.Items[DataSourceInsertIndex].FindControl("txtAWB_Number") != null)
				{
					TextBox txtAWB_Number = (TextBox)dgLodgments.Items[DataSourceInsertIndex].FindControl("txtAWB_Number");
					SetInitialFocus(txtAWB_Number);
				}
			}
			
			ViewState["CommandState"] = "INSERT";
		}

		private void dgLodgments_PageIndexChanged(object source, System.Web.UI.WebControls.DataGridPageChangedEventArgs e)
		{
			dgLodgments.CurrentPageIndex = e.NewPageIndex;
			//showCurrentPage();
			dgLodgments.SelectedIndex = -1;
			dgLodgments.EditItemIndex = -1;
			this.BindGrid();
		}

		private void btnExport_Click(object sender, System.EventArgs e)
		{
			string userLocation = Session["UserLocation"].ToString();
			string userRole = Session["UserRole"].ToString();	

			this.Txt_Exp_DateFrom.Text=this.Txt_BatchDate.Text;
			this.Txt_Exp_DateTo.Text=this.Txt_BatchDate.Text;
			Drp_Exp_Location.SelectedValue=userLocation;
			if(userRole.ToUpper().IndexOf("OPSSR") != -1)
			{
				this.Drp_Exp_Location.Enabled=true;			
			}
			else
			{
				Drp_Exp_Location.Enabled=false;	
			}

			this.divMain.Visible=false;
			this.divExport.Visible=true;
		}

		private void btnExceedOK_Click(object sender, System.EventArgs e)
		{
			string sBatchDateFrom = DateDbFormat(Txt_Exp_DateFrom.Text.Trim());
			string sBatchDateTo = DateDbFormat(Txt_Exp_DateTo.Text.Trim());
			string fileName= this.Drp_Exp_Location.SelectedValue + "-" + String.Format("{0:yyyyMMddhhmmss}", DateTime.Now) + ".xls";
			string path= Server.MapPath(@"..\WebUI\Export") + @"\" + fileName;
			string Res = "";
			DataSet ds = DailyLodgmentsMgrDAL.QueryDailyLodgmentsSummaryReport(m_strAppID,m_strEnterpriseID
													, sBatchDateFrom,sBatchDateTo,this.Drp_Exp_Location.SelectedValue,null);
			TIESClasses.CreateXls Xls = new TIESClasses.CreateXls();

			Res = Xls.createXlsDailyLodgments(ds.Tables[0],path);
			if (Res == "0")
			{	
				GetDownloadFile(Res,fileName,path);				
				// Kill Process after save
				Xls.KillProcesses("EXCEL");
			} 
			else
			{
				lblErrorMsg.Text = "ERROR : " + Res;
			}

			this.Txt_Exp_DateFrom.Text="";
			this.Txt_Exp_DateTo.Text="";
			if(this.Drp_Exp_Location.Items.Count>0)
			{
				this.Drp_Exp_Location.SelectedIndex=0;
			}
			this.divMain.Visible=true;
			this.divExport.Visible=false;
		}

		private void btnExceedCancel_Click(object sender, System.EventArgs e)
		{
			this.Txt_Exp_DateFrom.Text="";
			this.Txt_Exp_DateTo.Text="";
			if(this.Drp_Exp_Location.Items.Count>0)
			{
				this.Drp_Exp_Location.SelectedIndex=0;
			}
			this.divMain.Visible=true;
			this.divExport.Visible=false;
		}


		private void GetDownloadFile(string Res,string FileName, string path)
		{
			if (Res == "0")
			{				
				String sUrl = @"../WebUI/Export/"+FileName;
				ArrayList paramList = new ArrayList();
				paramList.Add(sUrl);
				String sScript = Utility.GetScript("openWindowParam.js",paramList);
				Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);

//				Response.ContentType="application/ms-excel";
//				Response.AddHeader( "content-disposition","attachment; filename=" + FileName);
//				FileStream fs = new FileStream(path,FileMode.Open);
//				long FileSize;
//				FileSize = fs.Length;
//				byte[] getContent = new byte[(int)FileSize];
//				fs.Read(getContent, 0, (int)fs.Length);
//				fs.Close();
//
//				Response.BinaryWrite(getContent);
			} 
		}

		private void btnPrintScanSheet_Click(object sender, System.EventArgs e)
		{
			string location = Drp_Location.SelectedItem.Text;
			string serviceType = Drp_ServiceType.SelectedItem.Text;
			string batchDate = Txt_BatchDate.Text;
			String[] arrBatchDate = batchDate.Split('/');
			if(arrBatchDate.Length == 3)
				batchDate = arrBatchDate[2] + "-" + arrBatchDate[1] + "-" + arrBatchDate[0];


			//string sUrlPdf = "ConsignmentNote/ScanSheet_Popup.aspx?serviceType=" + serviceType + "&location="+location+"&batchDate=" + batchDate;
			Session["pServiceType"] =serviceType;
			Session["pLocation"] =location;
			Session["pBatchDate"] =batchDate;

			string sUrl="PdfModalDialog.aspx?path=ConsignmentNote/ScanSheet_Popup.aspx";
			ArrayList paramList = new ArrayList();
			paramList.Add(sUrl);
			//String sScript = Utility.GetScriptPopupSetSize("OpenSetSize.js",paramList,600,550);
			String sScript = Utility.GetScriptPopupSetSize("openParentWindowDairyLodsment.js",paramList,600,550);
			Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
		}

		private void btnPrintLodgementSummary_Click(object sender, System.EventArgs e)
		{
			Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings,HttpContext.Current.Session);
			string strEnterpriseID = util.GetEnterpriseID();
			string strApplicationID = util.GetAppID();

			string location = Drp_Location.SelectedItem.Text;
			string serviceType = Drp_ServiceType.SelectedItem.Text;
			string batchDate = Txt_BatchDate.Text;
			batchDate = batchDate.Replace('/','-');

			Session["FORMID"]="DairyLodgments"; 
			String strUrl = null;
			strUrl = "ReportViewer.aspx?applicationid=" + strApplicationID + "&batchDate=" + batchDate.Trim()+ "&serviceType=" + serviceType + "&location=" + location.Trim() + "&enterpriseID=" + strEnterpriseID ;
			ArrayList paramList = new ArrayList();
			paramList.Add(strUrl);
			String sScript = Utility.GetScriptPopupSetSize("openParentWindowDairyLodsment.js",paramList,650,1000);
			Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
		}

		private void dgLodgments_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
		{
//			TextBox txtAWB_Number = (TextBox)e.Item.FindControl("txtAWB_Number");
//			if(txtAWB_Number != null)
//			{
//				SetInitialFocus(txtAWB_Number);
//			}			
		}

		private void btnPrintManifest_Click(object sender, System.EventArgs e)
		{
			if(DataSourceInsertIndex != -1)
			{
				DataSet ds = this.DbDataSource;
				DataRow drSelected = ds.Tables[0].Rows[DataSourceInsertIndex-1];				
				// Thosapol Modify (02/07/2013)
				if(drSelected["Status"].ToString() != "READY"){
					String sOpenMsg = "<script language=javascript> Confirm_Print(); </script>";
					Utility.RegisterScriptString(sOpenMsg,"Confirm_Print",this.Page);					
				}
				else{
					showreport();
				}					
			}

		}

		private void btnManifestScannedPkgs_Click(object sender, System.EventArgs e)
		{
			if(DataSourceInsertIndex != -1)
			{
				int current_page=dgLodgments.CurrentPageIndex * dgLodgments.PageSize;
				string batch_no="";
				DataSet ds = this.DbDataSource;
				DataRow drSelected = ds.Tables[0].Rows[DataSourceInsertIndex-1+ current_page];
				if(drSelected["BatchNumber"] != DBNull.Value)
				{
					batch_no = drSelected["BatchNumber"].ToString();
					int result = BatchManifest.UpdateScannedPkgs(m_strAppID,m_strEnterpriseID,Session["userID"].ToString().ToUpper(),this.Drp_Location.SelectedValue, batch_no); 
					DataSet dsBatch = SWB_Emulator.SWB_Batch_Manifest_Update(m_strAppID,m_strEnterpriseID,batch_no,Session["userID"].ToString().ToUpper(),"2");

					lblErrorMsg.Text = "Selected manifest will be updated in the background within about 5-10 minutes.";
				}				
				
			}
		}

		private void showreport()
		{
			if(DataSourceInsertIndex != -1)
			{
				//string strbatchType ="";
				Session["FORMID"]="BatchManifestControlPanel_Delivery"; 
				string batch_no="";
				DataSet ds = this.DbDataSource;
				DataRow drSelected = ds.Tables[0].Rows[DataSourceInsertIndex-1];
				if(drSelected["BatchNumber"] != DBNull.Value)
				{
					batch_no = drSelected["BatchNumber"].ToString();
				}						
				string strRpt="ReportViewer.aspx:Batch_no=" + batch_no + ";Batch_type=A;Location=" + Drp_Location.SelectedValue.ToString().Trim() + ";Doc_Type=pdf";
				
				string sUrl="PdfModalDialog.aspx?path="+strRpt;
				ArrayList paramList = new ArrayList();
				paramList.Add(sUrl);
				//String sScript = Utility.GetScript("openParentWindow.js",paramList);
				String sScript = Utility.GetScriptPopupSetSize("openParentWindowDairyLodsment.js",paramList,650,1000);
				Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
			}
		}

		private void btnHidReport_Click(object sender, System.EventArgs e)
		{
			string confirmValue = Request.Form["confirm_value"];
			if (confirmValue == "Yes") 
			{								
				showreport();
			} 
		}
	}
}
