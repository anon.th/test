<%@ Page language="c#" Codebehind="Zone.aspx.cs" AutoEventWireup="false" Inherits="com.ties.Zone" %>
<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<%@ Import Namespace="com.common.util" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Zone</title>
		<LINK href="css/Styles.css" type="text/css" rel="stylesheet" name="stylesheet">
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<!--#INCLUDE FILE="msFormValidations.inc"-->
		<script runat="server">
		</script>
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Zone" method="post" runat="server">
			<asp:button id="btnQuery" style="Z-INDEX: 101; LEFT: 30px; POSITION: absolute; TOP: 62px" runat="server"
				Text="Query" CssClass="queryButton" CausesValidation="False" Width="72px"></asp:button>
			<asp:button id="btnExecuteQuery" style="Z-INDEX: 102; LEFT: 102px; POSITION: absolute; TOP: 62px"
				runat="server" Text="Execute Query" CssClass="queryButton" CausesValidation="False" Width="130px"></asp:button>
			<asp:button id="btnInsert" style="Z-INDEX: 103; LEFT: 232px; POSITION: absolute; TOP: 62px"
				runat="server" Text="Insert" CssClass="queryButton" CausesValidation="False"></asp:button>
			<asp:datagrid id="dgZone" style="Z-INDEX: 104; LEFT: 29px; POSITION: absolute; TOP: 138px" runat="server"
				ItemStyle-Height="20" AutoGenerateColumns="False" OnEditCommand="dgZone_Edit" OnCancelCommand="dgZone_Cancel"
				OnDeleteCommand="dgZone_Delete" OnUpdateCommand="dgZone_Update" OnItemDataBound="dgZone_Bound"
				OnPageIndexChanged="dgZone_PageChange" AllowCustomPaging="True" AllowPaging="True" Width="549px">
				<ItemStyle Height="20px" CssClass="gridField"></ItemStyle>
				<Columns>
					<asp:EditCommandColumn ButtonType="LinkButton" UpdateText="&lt;IMG SRC='images/butt-update.gif' border=0 title='Update' &gt;"
						CancelText="&lt;IMG SRC='images/butt-cancel.gif' border=0 title='Cancel' &gt;" EditText="&lt;IMG SRC='images/butt-edit.gif' border=0 title='Edit' &gt;">
						<HeaderStyle Width="4%" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle HorizontalAlign="Left"></ItemStyle>
					</asp:EditCommandColumn>
					<asp:ButtonColumn Text="&lt;IMG SRC='images/butt-delete.gif' border=0 title='Delete' &gt;" ButtonType="LinkButton"
						CommandName="Delete">
						<HeaderStyle Width="2%" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle HorizontalAlign="Center"></ItemStyle>
					</asp:ButtonColumn>
					<asp:TemplateColumn HeaderText="Zone Code" HeaderStyle-Font-Bold="true">
						<HeaderStyle Width="15%" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle CssClass="gridField"></ItemStyle>
						<ItemTemplate>
							<asp:Label CssClass="gridLabel" ID="lblZoneCode" Text='<%#DataBinder.Eval(Container.DataItem,"zone_code")%>' Runat="server">
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<cc1:mstextbox CssClass="gridTextBox" ID="txtZoneCode" Text='<%#DataBinder.Eval(Container.DataItem,"zone_code")%>' Runat="server" Enabled="True" MaxLength="12">
							</cc1:mstextbox>
							<asp:RequiredFieldValidator ID="zcValidator" Runat="server" BorderWidth="0" Display="None" ControlToValidate="txtZoneCode"
								ErrorMessage="Zone Code is required field"></asp:RequiredFieldValidator>
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Zone Name">
						<HeaderStyle Width="50%" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle CssClass="gridField"></ItemStyle>
						<ItemTemplate>
							<asp:Label CssClass="gridLabel" ID="lblZoneName" Text='<%#DataBinder.Eval(Container.DataItem,"zone_name")%>' Runat="server" Enabled="True">
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<asp:TextBox CssClass="gridTextBox" ID="txtZoneName" Text='<%#DataBinder.Eval(Container.DataItem,"zone_name")%>' Runat="server" MaxLength="100">
							</asp:TextBox>
						</EditItemTemplate>
					</asp:TemplateColumn>
				</Columns>
				<PagerStyle NextPageText="Next" PrevPageText="Previous" HorizontalAlign="Right" CssClass="normalText"></PagerStyle>
			</asp:datagrid>
			<asp:label id="lblTitle" style="Z-INDEX: 111; LEFT: 29px; POSITION: absolute; TOP: 12px" runat="server"
				Width="477px" Height="26px" CssClass="mainTitleSize">Zone</asp:label>
			<asp:Label id="lblErrorMessage" style="Z-INDEX: 105; LEFT: 29px; POSITION: absolute; TOP: 92px"
				runat="server" Width="538px" Height="31px" CssClass="errorMsgColor" DESIGNTIMEDRAGDROP="63">Place holder for err msg</asp:Label>
			<asp:validationsummary id="ValidationSummary1" style="Z-INDEX: 111; LEFT: 90px; POSITION: absolute; TOP: 96px"
				Height="36px" Width="479px" Runat="server" ShowMessageBox="True" DisplayMode="SingleParagraph" ShowSummary="False"></asp:validationsummary>
		</form>
	</body>
</HTML>
