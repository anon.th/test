using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.ties.DAL;
using com.common.util;
using com.common.classes;
using com.common.applicationpages;


namespace com.ties
{
	/// <summary>
	/// Summary description for VASZipcodeExcluded.
	/// </summary>
	/// 
	

	public class VASZipcodeExcluded : BasePage
	{
		protected System.Web.UI.WebControls.Button btnExecQry;
		protected System.Web.UI.WebControls.Button btnQry;
		protected System.Web.UI.WebControls.Button btnSave;
		protected System.Web.UI.WebControls.Button btnInsert;
		protected System.Web.UI.WebControls.Button btnDelete;


		
		SessionDS m_sdsVAS = null;
		SessionDS m_sdsZipcodeVASExcluded = null;

		protected System.Web.UI.WebControls.Label lblVASMessage;
		protected System.Web.UI.WebControls.Label lblZVASEMessage;
		protected System.Web.UI.WebControls.ValidationSummary ValidationSummary1;
		protected System.Web.UI.WebControls.Button btnZVASEInsertMultiple;
		protected System.Web.UI.WebControls.DataGrid dgVAS;
		protected System.Web.UI.WebControls.DataGrid dgZipcodeVASExcluded;
		protected System.Web.UI.WebControls.Button btnZVASEInsert;
		protected System.Web.UI.WebControls.Label Label1;
		protected System.Web.UI.WebControls.Label lblTitle;
		private DataView m_dvVASCustomerOptions = null;

		//Utility utility = null;
	
		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here

			//utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings,Page.Session);
			ShowButtonColumns(true);
			

		
			if(!Page.IsPostBack)
			{
				ViewState["VASOperation"] = Operation.None;
				ViewState["ZVASEMode"] = ScreenMode.None;
				ViewState["ZVASEOperation"] = Operation.None;
				Session["SESSION_DS2"] = m_sdsZipcodeVASExcluded;

				m_dvVASCustomerOptions = CreateVASCustomerOptions(true);
				ResetScreenForQuery();

			}
			else
			{
				m_sdsVAS = (SessionDS)Session["SESSION_DS1"];
				m_sdsZipcodeVASExcluded = (SessionDS)Session["SESSION_DS2"];
				m_dvVASCustomerOptions = CreateVASCustomerOptions(true);
				lblVASMessage.Text = "";
				if(Session["VASZipCode"] != null)
				{
					Session["VASZipCode"]=null;
					ShowCurrentZVASEPage();
					ViewState["ZVASEMode"] = ScreenMode.ExecuteQuery;
				}
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			base.OnInit(e);
			InitializeComponent();
			
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnExecQry.Click += new System.EventHandler(this.btnExecQry_Click);
			this.btnInsert.Click += new System.EventHandler(this.btnInsert_Click);
			this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
			this.btnQry.Click += new System.EventHandler(this.btnQry_Click);
			this.btnZVASEInsert.Click += new System.EventHandler(this.btnZVASEInsert_Click);
			this.btnZVASEInsertMultiple.Click += new System.EventHandler(this.btnZVASEInsertMultiple_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		public string AllowCust(object allow_cust_code)
		{
			string allow_cust_str ="";

			if(allow_cust_code!=null && allow_cust_code!=DBNull.Value)
			{
				if(allow_cust_code.ToString().Equals("Y"))
				{
					allow_cust_str = "Yes";
				}
				else if(allow_cust_code.ToString().Equals("N"))
				{
					allow_cust_str = "No";
				}
			}

			return allow_cust_str;
		}

		private void btnQry_Click(object sender, System.EventArgs e)
		{
			ResetScreenForQuery();
		}

		private void ResetScreenForQuery()
		{
			
			m_sdsVAS = SysDataMgrDAL.GetEmptyVASCodeDS(1);
			
			ViewState["VASMode"] = ScreenMode.Query;

			Session["SESSION_DS1"] = m_sdsVAS;
			dgVAS.EditItemIndex = 0;
			dgVAS.CurrentPageIndex = 0;
			BindVASGrid();
			btnExecQry.Enabled = true;
			Utility.EnableButton(ref btnInsert,ButtonType.Insert,true,m_moduleAccessRights);
			lblVASMessage.Text = "";

			ShowButtonColumns(false);
			
			ResetDetailsGrid();
		}
		private void btnExecQry_Click(object sender, System.EventArgs e)
		{
			FillVASDataRow(dgVAS.Items[0],0);
			ViewState["QUERY_DS"] = m_sdsVAS.ds;
			dgVAS.CurrentPageIndex = 0;
			ShowCurrentVASPage();
			btnExecQry.Enabled = false;
			Utility.EnableButton(ref btnInsert,ButtonType.Insert,true,m_moduleAccessRights);
			lblVASMessage.Text = "";
			ViewState["VASMode"] = ScreenMode.ExecuteQuery;
		}

		private void btnInsert_Click(object sender, System.EventArgs e)
		{
			//If coming to any other mode to Insert Mode then create the empty data set.

			if((int)ViewState["VASMode"] != (int)ScreenMode.Insert || m_sdsVAS.ds.Tables[0].Rows.Count >= dgVAS.PageSize)
			{
				m_sdsVAS = SysDataMgrDAL.GetEmptyVASCodeDS(0);
			}

			AddRowInVASGrid();	
			dgVAS.EditItemIndex = m_sdsVAS.ds.Tables[0].Rows.Count - 1;
			dgVAS.CurrentPageIndex = 0;
			Logger.LogDebugInfo("VASZipcodeExcluded","btnInsert_Click","INF003","Data Grid Items count"+dgVAS.Items.Count);
			BindVASGrid();
			ViewState["VASMode"] = ScreenMode.Insert;
			ViewState["VASOperation"] = Operation.Insert;
			Utility.EnableButton(ref btnInsert,ButtonType.Insert,false,m_moduleAccessRights);
			btnExecQry.Enabled = false;
			lblVASMessage.Text = "";
			ResetDetailsGrid();
			getPageControls(Page);
		}

		public void dgVAS_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			Label lblVASCode = (Label)dgVAS.SelectedItem.FindControl("lblVASCode");
			msTextBox txtVASCode = (msTextBox)dgVAS.SelectedItem.FindControl("txtVASCode");
			String strVASCode = null;

			if(lblVASCode != null)
			{
				strVASCode = lblVASCode.Text;
			}

			if(txtVASCode != null)
			{
				strVASCode = txtVASCode.Text;
			}
			ViewState["CurrentVAS"] = strVASCode;
			dgZipcodeVASExcluded.CurrentPageIndex = 0;

			Logger.LogDebugInfo("VASZipcodeExcluded","dgVAS_SelectedIndexChanged","INF004","updating data grid..."+dgVAS.SelectedIndex+"  : "+strVASCode);
			ShowCurrentZVASEPage();

			ViewState["ZVASEMode"] = ScreenMode.ExecuteQuery;

			if((int)ViewState["VASMode"] != (int)ScreenMode.Insert && (int)ViewState["VASOperation"] != (int)Operation.Insert)
			{
				Utility.EnableButton(ref btnInsert,ButtonType.Insert,true,m_moduleAccessRights);
			}

			if((int)ViewState["VASMode"] == (int)ScreenMode.Insert && dgVAS.EditItemIndex == dgVAS.SelectedIndex && (int)ViewState["VASOperation"] == (int)Operation.Insert)
			{
				btnZVASEInsert.Enabled = false;
				btnZVASEInsertMultiple.Enabled = false;
			}
			else
			{
				btnZVASEInsert.Enabled = true;
				btnZVASEInsertMultiple.Enabled = true;
			}
			lblVASMessage.Text = "";
			lblZVASEMessage.Text="";
		}

		protected void dgVAS_Edit(object sender, DataGridCommandEventArgs e)
		{
			if((int)ViewState["VASMode"] == (int)ScreenMode.Insert && (int) ViewState["VASOperation"] == (int)Operation.Insert && dgVAS.EditItemIndex > 0)
			{
				m_sdsVAS.ds.Tables[0].Rows.RemoveAt(dgVAS.EditItemIndex);
				m_sdsVAS.QueryResultMaxSize--;
				dgVAS.CurrentPageIndex = 0;
			}
			dgVAS.EditItemIndex = e.Item.ItemIndex;
			ViewState["VASOperation"] = Operation.Update;
			BindVASGrid();
			lblVASMessage.Text = "";
			Logger.LogDebugInfo("VASZipcodeExcluded","dgVAS_Edit","INF004","updating data grid...");			
		}
		
		public void dgVAS_Update(object sender, DataGridCommandEventArgs e)
		{
			try
			{
				FillVASDataRow(e.Item,e.Item.ItemIndex);
			}
			catch(ApplicationException appException)
			{
				String strMsg = appException.Message;
				strMsg = appException.InnerException.Message;
				if(strMsg.IndexOf("duplicate key") != -1 )
				{
					lblVASMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"DUP_KEY_FOUND_VAS",utility.GetUserCulture());
					return;
				}
				if(strMsg.IndexOf("unique") != -1 )
				{
					lblVASMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"DUP_KEY_FOUND_VAS",utility.GetUserCulture());
					return;
				}
			}

			bool AllowCustValid = (bool)ViewState["AllowCustValid"];
			if(!AllowCustValid)
			{
				lblVASMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"Allow for Customer flag must be Yes or No.",utility.GetUserCulture());
				return;
			}

			int iOperation = (int)ViewState["VASOperation"];
			switch(iOperation)
			{
				case (int)Operation.Update:
					DataSet dsToUpdate = m_sdsVAS.ds.GetChanges();
					SysDataMgrDAL.ModifyVASCodeDS(utility.GetAppID(),utility.GetEnterpriseID(),dsToUpdate);
					lblVASMessage.Text=Utility.GetLanguageText(ResourceType.UserMessage,"UPD_SUCCESSFULLY",utility.GetUserCulture());
					Utility.EnableButton(ref btnInsert,ButtonType.Insert,true,m_moduleAccessRights);
					m_sdsVAS.ds.Tables[0].Rows[e.Item.ItemIndex].AcceptChanges();
					Logger.LogDebugInfo("VASZipcodeExcluded","dgVAS_OnUpdate","INF004","update in modify mode..");
					break;
				case (int)Operation.Insert:
					DataSet dsToInsert = m_sdsVAS.ds.GetChanges();
					try
					{
						SysDataMgrDAL.AddVASCodeDS(utility.GetAppID(),utility.GetEnterpriseID(),dsToInsert);
						lblVASMessage.Text=Utility.GetLanguageText(ResourceType.UserMessage,"INS_SUCCESSFULLY",utility.GetUserCulture());
						Utility.EnableButton(ref btnInsert,ButtonType.Insert,true,m_moduleAccessRights);		
					}
					catch(ApplicationException appException)
					{
						String strMsg = appException.Message;
						strMsg = appException.InnerException.Message;
						strMsg = appException.InnerException.InnerException.Message;
						if(strMsg.IndexOf("duplicate key") != -1 )
						{
							lblVASMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"DUP_KEY_FOUND_VAS",utility.GetUserCulture());
							return;
						}
						if(strMsg.IndexOf("unique constraint ") != -1 )
						{
							lblVASMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"DUP_KEY_FOUND_VAS",utility.GetUserCulture());
							return;
						}
					
					}
					m_sdsVAS.ds.Tables[0].Rows[e.Item.ItemIndex].AcceptChanges();
					Logger.LogDebugInfo("VASZipcodeExcluded","dgVAS_OnUpdate","INF004","update in Insert mode");
					break;
			}
			dgVAS.EditItemIndex = -1;
			ViewState["VASOperation"] = Operation.None;
			lblZVASEMessage.Text="";
			BindVASGrid();
		}

		protected void dgVAS_Cancel(object sender, DataGridCommandEventArgs e)
		{
			dgVAS.EditItemIndex = -1;

			if((int)ViewState["VASMode"] == (int)ScreenMode.Insert && (int)ViewState["VASOperation"] == (int)Operation.Insert)
			{
				m_sdsVAS.ds.Tables[0].Rows.RemoveAt(e.Item.ItemIndex);
				m_sdsVAS.QueryResultMaxSize--;
				dgVAS.CurrentPageIndex = 0;
			}
			ViewState["VASOperation"] = Operation.None;
			BindVASGrid();
			Utility.EnableButton(ref btnInsert,ButtonType.Insert,true,m_moduleAccessRights);
			lblVASMessage.Text = "";
			Logger.LogDebugInfo("VASZipcodeExcluded","dgVAS_Cancel","INF004","updating data grid...");			
		}

		public void dgVAS_Delete(object sender, DataGridCommandEventArgs e)
		{
			msTextBox txtVASCode = (msTextBox)e.Item.FindControl("txtVASCode");
			Label lblVASCode = (Label)e.Item.FindControl("lblVASCode");
			String strVASCode = null;
			if(txtVASCode != null)
			{
				strVASCode = txtVASCode.Text;
			}

			if(lblVASCode != null)
			{
				strVASCode = lblVASCode.Text;
			}

			try
			{
				SysDataMgrDAL.DeleteVASCodeDS(utility.GetAppID(),utility.GetEnterpriseID(),strVASCode);
			}
			catch(ApplicationException appException)
			{
				String strMsg = appException.Message;
				if(strMsg.IndexOf("FK") != -1)
				{
					lblVASMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"ERR_DEL_VAS_TRANS",utility.GetUserCulture());
				}
				if(strMsg.IndexOf("child record") != -1)
				{
					lblVASMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"ERR_DEL_VAS_TRANS",utility.GetUserCulture());
				}
				return;

			}

			if((int)ViewState["VASMode"] == (int)ScreenMode.Insert)
			{
				m_sdsVAS.ds.Tables[0].Rows.RemoveAt(e.Item.ItemIndex);
				m_sdsVAS.QueryResultMaxSize--;
				dgVAS.CurrentPageIndex = 0;

				if((int) ViewState["VASOperation"] == (int)Operation.Insert && dgVAS.EditItemIndex > 0)
				{
					m_sdsVAS.ds.Tables[0].Rows.RemoveAt(dgVAS.EditItemIndex-1);
					m_sdsVAS.QueryResultMaxSize--;
				}
				dgVAS.EditItemIndex = -1;
				dgVAS.SelectedIndex = -1;
				BindVASGrid();
				ResetDetailsGrid();
			}
			else
			{
				ShowCurrentVASPage();
			}
			Logger.LogDebugInfo("VASZipcodeExcluded","dgVAS_Delete","INF004","Deleted row in VAS_Code table..");
			ViewState["VASOperation"] = Operation.None;
			Utility.EnableButton(ref btnInsert,ButtonType.Insert,true,m_moduleAccessRights);
		}

		private void BindVASGrid()
		{
			dgVAS.VirtualItemCount = System.Convert.ToInt32(m_sdsVAS.QueryResultMaxSize);
			dgVAS.DataSource = m_sdsVAS.ds;
			dgVAS.DataBind();
			Session["SESSION_DS1"] = m_sdsVAS;
		}

		private void AddRowInVASGrid()
		{
			SysDataMgrDAL.AddNewRowInVASCodeDS(m_sdsVAS);
			Session["SESSION_DS1"] = m_sdsVAS;
		}

		protected void dgVAS_Bound(object sender, DataGridItemEventArgs e)
		{
			if(e.Item.ItemIndex == -1)
			{
				return;
			}

			DropDownList ddlAllowForCust = (DropDownList)e.Item.FindControl("ddlAllowForCust");

			DataRow drSelected = m_sdsVAS.ds.Tables[0].Rows[e.Item.ItemIndex];
			if(drSelected.RowState == DataRowState.Deleted)
			{
				return;
			}

			if(ddlAllowForCust != null)
			{
				ddlAllowForCust.DataSource = LoadVASCustomerOptions();
				ddlAllowForCust.DataTextField = "Text";
				ddlAllowForCust.DataValueField = "StringValue";
				ddlAllowForCust.DataBind();

	
				String strType = (String)(drSelected["allow_for_customer"] == DBNull.Value?"":drSelected["allow_for_customer"]);

				ddlAllowForCust.SelectedIndex = ddlAllowForCust.Items.IndexOf(ddlAllowForCust.Items.FindByValue(strType));
			}

			Operation iOperation = (Operation) ViewState["VASOperation"];

			msTextBox txtVASCode = (msTextBox)e.Item.FindControl("txtVASCode");			
			if(txtVASCode != null && iOperation.Equals(Operation.Update))
			{
				txtVASCode.Enabled = false;
			}

//			DropDownList ddlAllowForCust = (DropDownList)e.Item.FindControl("ddlAllowForCust");
//			if(ddlAllowForCust!=null && iOperation.Equals(Operation.Update))
//			{
//				ListItem lstAllowCust = ddlAllowForCust.Items.FindByValue(drSelected["allow_for_customer"].ToString().Trim());
//				if(lstAllowCust!=null) lstAllowCust.Selected = true;
//			}
		}

		protected void dgVAS_PageChange(object sender, DataGridPageChangedEventArgs e)
		{
			dgVAS.CurrentPageIndex = e.NewPageIndex;
			ShowCurrentVASPage();
			Logger.LogDebugInfo("VASZipcodeExcluded","dgVAS_PageChange","INF009","On Page Change "+e.NewPageIndex);
		}


		private void ShowCurrentVASPage()
		{
			int iStartIndex = dgVAS.CurrentPageIndex * dgVAS.PageSize;
			DataSet dsQuery = (DataSet)ViewState["QUERY_DS"];
			m_sdsVAS = SysDataMgrDAL.GetVASCodeDS(utility.GetAppID(),utility.GetEnterpriseID(),iStartIndex,dgVAS.PageSize,dsQuery);
			int pgCnt = (Convert.ToInt32(m_sdsVAS.QueryResultMaxSize) - 1)/dgVAS.PageSize;
			if(pgCnt < dgVAS.CurrentPageIndex)
			{
				dgVAS.CurrentPageIndex = System.Convert.ToInt32(pgCnt);
			}
			dgVAS.SelectedIndex = -1;
			dgVAS.EditItemIndex = -1;
			lblVASMessage.Text = "";

			BindVASGrid();
			ResetDetailsGrid();
		}


		/// ZVASE Grid Methos..
		/// 
	
		private void btnZVASEInsert_Click(object sender, System.EventArgs e)
		{
			//If coming to any other mode to Insert Mode then create the empty data set.

//			if((int)ViewState["ZVASEMode"] != (int)ScreenMode.Insert || m_sdsZipcodeVASExcluded.ds.Tables[0].Rows.Count >= dgZipcodeVASExcluded.PageSize)
//			{
//				m_sdsZipcodeVASExcluded = SysDataMgrDAL.GetEmptyZipcodeVASExcludedDS(0);
//			}
//
//			AddRowInZVASEGrid();	
//			dgZipcodeVASExcluded.EditItemIndex = m_sdsZipcodeVASExcluded.ds.Tables[0].Rows.Count - 1;
//			dgZipcodeVASExcluded.CurrentPageIndex = 0;
//			Logger.LogDebugInfo("VASZipcodeExcluded","btnZVASEInsert_Click","INF003","Data Grid Items count"+dgZipcodeVASExcluded.Items.Count);
//			BindZVASEGrid();
//			ViewState["ZVASEMode"] = ScreenMode.Insert;
//			ViewState["ZVASEOperation"] = Operation.Insert;
//			btnZVASEInsert.Enabled = false;
//			btnZVASEInsertMultiple.Enabled = false;
			String strVASCode	= (String)ViewState["CurrentVAS"];				
			String sUrl = "AgentZipCodePopup.aspx?FORMID=VASZipcodeExcluded&VASCODE="+strVASCode;
			ArrayList paramList = new ArrayList();
			paramList.Add(sUrl);
			String sScript = Utility.GetScript("openParentWindow.js",paramList);
			Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
		}

		protected void dgZipcodeVASExcluded_Edit(object sender, DataGridCommandEventArgs e)
		{
			dgZipcodeVASExcluded.EditItemIndex = e.Item.ItemIndex;
			ViewState["ZVASEOperation"] = Operation.Update;
			BindZVASEGrid();
			lblZVASEMessage.Text = "";
			Logger.LogDebugInfo("VASZipcodeExcluded","dgZipcodeVASExcluded_Edit","INF004","updating data grid...");			
		}

		public void dgZipcodeVASExcluded_Update(object sender, DataGridCommandEventArgs e)
		{
			FillZVASEDataRow(e.Item,e.Item.ItemIndex);

			int iOperation = (int)ViewState["ZVASEOperation"];

			String strVASCode = (String)ViewState["CurrentVAS"];
			switch(iOperation)
			{

				case (int)Operation.Insert:

					DataSet dsToInsert = m_sdsZipcodeVASExcluded.ds.GetChanges();
					try
					{
						SysDataMgrDAL.AddZipcodeVASExcludedDS(utility.GetAppID(),utility.GetEnterpriseID(),strVASCode,dsToInsert);
						lblZVASEMessage.Text=Utility.GetLanguageText(ResourceType.UserMessage,"INS_SUCCESSFULLY",utility.GetUserCulture());
						btnZVASEInsert.Enabled = true;
						btnZVASEInsertMultiple.Enabled = true;
					}
					catch(ApplicationException appException)
					{
						String strMsg = appException.Message;
						strMsg = appException.InnerException.Message;
						strMsg = appException.InnerException.InnerException.Message;
						if(strMsg.IndexOf("duplicate key") != -1 )
						{
							lblZVASEMessage.Text = "Duplicate Zipcode Code is not allowed.";
						} 
						else if(strMsg.IndexOf("FOREIGN KEY") != -1 || strMsg.IndexOf("parent key") != -1)
						{
							lblZVASEMessage.Text = "Zipcode not found. Please create Zipcode in Zipcode Master.";
						}
						else if(strMsg.IndexOf("unique") != -1 )
						{
							lblZVASEMessage.Text = "Duplicate Zipcode Code is not allowed.";
						} 
						else if(strMsg.IndexOf("APPDB.SYS_C002182") != -1 )
						{
							//violated - parent key not found //Zipcode not present in parent
							lblZVASEMessage.Text = "Zipcode not found. Please create Zipcode in Zipcode Master.";
						}
						else
						{
							lblZVASEMessage.Text = strMsg;
						}
						return;
						
					}

					m_sdsZipcodeVASExcluded.ds.Tables[0].Rows[e.Item.ItemIndex].AcceptChanges();
					Logger.LogDebugInfo("VASZipcodeExcluded","dgZipcodeVASExcluded_OnUpdate","INF004","update in Insert mode");
					break;

			}
			dgZipcodeVASExcluded.EditItemIndex = -1;
			ViewState["ZVASEOperation"] = Operation.None;
			BindZVASEGrid();		
			lblVASMessage.Text = "";
		}


		protected void dgZipcodeVASExcluded_Cancel(object sender, DataGridCommandEventArgs e)
		{
			dgZipcodeVASExcluded.EditItemIndex = -1;
			if((int)ViewState["ZVASEMode"] == (int)ScreenMode.Insert && (int)ViewState["ZVASEOperation"] == (int)Operation.Insert)
			{
				m_sdsZipcodeVASExcluded.ds.Tables[0].Rows.RemoveAt(e.Item.ItemIndex);
				m_sdsZipcodeVASExcluded.QueryResultMaxSize--;
				dgZipcodeVASExcluded.CurrentPageIndex = 0;
			}

			btnZVASEInsert.Enabled = true;
			btnZVASEInsertMultiple.Enabled = true;
			lblZVASEMessage.Text = "";

			ViewState["ZVASEOperation"] = Operation.None;
			BindZVASEGrid();
			Logger.LogDebugInfo("VASZipcodeExcluded","dgZipcodeVASExcluded_Cancel","INF004","updating data grid...");			
		}

		protected void dgZipcodeVASExcluded_Delete(object sender, DataGridCommandEventArgs e)
		{
			msTextBox txtZipcode = (msTextBox)e.Item.FindControl("txtZipcode");
			Label lblZipcode = (Label)e.Item.FindControl("lblZipcode");
			String strZipcode = null;
			if(txtZipcode != null)
			{
				strZipcode = txtZipcode.Text;
			}

			if(lblZipcode != null)
			{
				strZipcode = lblZipcode.Text;
			}

			String strVASCode = (String)ViewState["CurrentVAS"];
			try
			{
				SysDataMgrDAL.DeleteZipcodeVASExcludedDS(utility.GetAppID(),utility.GetEnterpriseID(),strVASCode,strZipcode);
				lblZVASEMessage.Text=Utility.GetLanguageText(ResourceType.UserMessage,"DLD_SUCCESSFULLY",utility.GetUserCulture());
			}
			catch(ApplicationException appException)
			{
				String strMsg = appException.Message;
				if(strMsg.IndexOf("FK") != -1)
				{
					lblZVASEMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"ERROR",utility.GetUserCulture());
				}
				if(strMsg.IndexOf("child record") != -1)
				{
					lblZVASEMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"ERROR",utility.GetUserCulture());
				}
				return;

			}
			if((int)ViewState["ZVASEMode"] == (int)ScreenMode.Insert)
			{
				m_sdsZipcodeVASExcluded.ds.Tables[0].Rows.RemoveAt(e.Item.ItemIndex);
				m_sdsZipcodeVASExcluded.QueryResultMaxSize--;
				dgZipcodeVASExcluded.CurrentPageIndex = 0;

				if((int) ViewState["ZVASEOperation"] == (int)Operation.Insert && dgZipcodeVASExcluded.EditItemIndex > 0)
				{
					m_sdsZipcodeVASExcluded.ds.Tables[0].Rows.RemoveAt(dgZipcodeVASExcluded.EditItemIndex-1);
					m_sdsZipcodeVASExcluded.QueryResultMaxSize--;
				}
				dgZipcodeVASExcluded.EditItemIndex = -1;
				BindZVASEGrid();
			}
			else
			{
				ShowCurrentZVASEPage();
			}

			Logger.LogDebugInfo("VASZipcodeExcluded","dgZipcodeVASExcluded_Delete","INF004","Deleted row in Zipcode_VAS_Excluded table..");

			ViewState["ZVASEOperation"] = Operation.None;
			btnZVASEInsert.Enabled = true;
			btnZVASEInsertMultiple.Enabled = true;
			lblVASMessage.Text = "";
		}

		private void BindZVASEGrid()
		{
			dgZipcodeVASExcluded.VirtualItemCount = System.Convert.ToInt32(m_sdsZipcodeVASExcluded.QueryResultMaxSize);
			dgZipcodeVASExcluded.DataSource = m_sdsZipcodeVASExcluded.ds;
			dgZipcodeVASExcluded.DataBind();
			Session["SESSION_DS2"] = m_sdsZipcodeVASExcluded;
		}

		private void AddRowInZVASEGrid()
		{
			SysDataMgrDAL.AddNewRowInZipcodeVASExcludedDS(m_sdsZipcodeVASExcluded);
			Session["SESSION_DS2"] = m_sdsZipcodeVASExcluded;
		}

		public void dgZipcodeVASExcluded_Button(object sender, DataGridCommandEventArgs e)
		{
			String strCmdNm = e.CommandName;
			if(strCmdNm.Equals("search"))
			{
				TextBox txtZipcode = (TextBox)e.Item.FindControl("txtZipcode");
				TextBox txtState = (TextBox)e.Item.FindControl("txtState");
				TextBox txtCountry = (TextBox)e.Item.FindControl("txtCountry");

				String strZipcodeClientID = null;
				String strStateClientID = null;
				String strCountryClientID = null;

				String strZipcode = null;
				
				if(txtZipcode != null)
				{
					strZipcodeClientID = txtZipcode.ClientID;
					strZipcode = txtZipcode.Text;
				}

				if(txtState != null)
				{
					strStateClientID = txtState.ClientID;
				}
				
				if(txtCountry != null)
				{
					strCountryClientID = txtCountry.ClientID;
				}



				String sUrl = "ZipcodePopup.aspx?FORMID=VASZipcodeExcluded&ZIPCODE="+strZipcode+"&ZIPCODE_CID="+strZipcodeClientID+"&STATE_CID="+strStateClientID+"&COUNTRY_CID="+strCountryClientID;
				ArrayList paramList = new ArrayList();
				paramList.Add(sUrl);
				String sScript = Utility.GetScript("openWindowParam.js",paramList);
				Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);	
			}
			
		}


		protected void dgZipcodeVASExcluded_Bound(object sender, DataGridItemEventArgs e)
		{
			if(e.Item.ItemIndex == -1)
			{
				return;
			}

			DataRow drSelected = m_sdsZipcodeVASExcluded.ds.Tables[0].Rows[e.Item.ItemIndex];
			if(drSelected.RowState == DataRowState.Deleted)
			{
				return;
			}

			msTextBox txtZipcode = (msTextBox)e.Item.FindControl("txtZipcode");
			
			int iOperation = (int) ViewState["ZVASEOperation"];
			if(txtZipcode != null && iOperation == (int)Operation.Update)
			{
				txtZipcode.Enabled = false;
			}

			if(e.Item.ItemType == ListItemType.EditItem)
			{
				e.Item.Cells[0].Enabled = true;
				e.Item.Cells[3].Enabled = true;
			}
			else
			{
				e.Item.Cells[0].Enabled = false;
				e.Item.Cells[3].Enabled = false;
			}
		}

		protected void dgZipcodeVASExcluded_PageChange(object sender, DataGridPageChangedEventArgs e)
		{
			dgZipcodeVASExcluded.CurrentPageIndex = e.NewPageIndex;
			ShowCurrentZVASEPage();
			Logger.LogDebugInfo("VASZipcodeExcluded","dgZipcodeVASExcluded_PageChange","INF009","On Page Change "+e.NewPageIndex);
		}

		private void ShowCurrentZVASEPage()
		{
			int iStartIndex = dgZipcodeVASExcluded.CurrentPageIndex * dgZipcodeVASExcluded.PageSize;
			String strVASCode = (String)ViewState["CurrentVAS"];
			m_sdsZipcodeVASExcluded = SysDataMgrDAL.GetZipcodeVASExcludedDS(utility.GetAppID(),utility.GetEnterpriseID(),strVASCode,iStartIndex,dgZipcodeVASExcluded.PageSize);

			int pgCnt = (Convert.ToInt32(m_sdsZipcodeVASExcluded.QueryResultMaxSize) - 1)/dgZipcodeVASExcluded.PageSize;
			if(pgCnt < dgZipcodeVASExcluded.CurrentPageIndex)
			{
				dgZipcodeVASExcluded.CurrentPageIndex = System.Convert.ToInt32(pgCnt);
			}
			dgZipcodeVASExcluded.EditItemIndex = -1;
			BindZVASEGrid();
//			lblZVASEMessage.Text = "";
		}


		private void ShowButtonColumns(bool bShow)
		{
			dgVAS.Columns[0].Visible = bShow;
			dgVAS.Columns[1].Visible = bShow;
			dgVAS.Columns[2].Visible = bShow;
		}

		private void FillVASDataRow(DataGridItem item, int drIndex)
		{
			//Added by GwanG on 21Apr08
			ViewState["AllowCustValid"] = true;

			try
			{
				DataRow drCurrent = m_sdsVAS.ds.Tables[0].Rows[drIndex];

				msTextBox txtVASCode = (msTextBox)item.FindControl("txtVASCode");
				if(txtVASCode != null)
				{
					drCurrent["vas_code"] = txtVASCode.Text;
				}

				TextBox txtVASDescription = (TextBox)item.FindControl("txtVASDescription");
				if(txtVASDescription != null)
				{					
					drCurrent["vas_description"] = txtVASDescription.Text;
				}

				msTextBox txtSurcharge = (msTextBox)item.FindControl("txtSurcharge");
				if(txtSurcharge != null )
				{
					if(txtSurcharge.Text == "")
					{
						drCurrent["surcharge"] = System.DBNull.Value;
					}
					else
					{
					
						drCurrent["surcharge"] = txtSurcharge.Text;
					}
				}

				DropDownList ddlAllowForCust = (DropDownList)item.FindControl("ddlAllowForCust");
				if(ddlAllowForCust != null)
				{
					if(ddlAllowForCust.SelectedItem.Value == "")
					{
						drCurrent["allow_for_customer"] = System.DBNull.Value;
						ViewState["AllowCustValid"] = false;
					}
					else
					{
						drCurrent["allow_for_customer"] = ddlAllowForCust.SelectedItem.Value;
					}
				
				}

//				DropDownList ddlAllowForCust = (DropDownList)item.FindControl("ddlAllowForCust");
//				if(ddlAllowForCust != null)
//				{
//					if(ddlAllowForCust.SelectedIndex!=0)
//						drCurrent["allow_for_customer"] = ddlAllowForCust.SelectedItem.Value;
//					else
//						drCurrent["allow_for_customer"] = DBNull.Value;
//				}
			}
			catch(Exception appException)
			{
				Logger.LogTraceError("RBACManager","AddVASCodeDS","SDM001","Error During Insert "+ appException.Message.ToString());
				throw new ApplicationException("Error inserting VAS Code ",appException);				
			}

		}

		private void FillZVASEDataRow(DataGridItem item, int drIndex)
		{
			msTextBox txtZipcode = (msTextBox)item.FindControl("txtZipcode");
			if(txtZipcode != null)
			{
				DataRow drCurrent = m_sdsZipcodeVASExcluded.ds.Tables[0].Rows[drIndex];
				drCurrent["zipcode"] = txtZipcode.Text;
			}

			TextBox txtState = (TextBox)item.FindControl("txtState");
			if(txtState != null)
			{
				DataRow drCurrent = m_sdsZipcodeVASExcluded.ds.Tables[0].Rows[drIndex];
				drCurrent["state_name"] = txtState.Text;
			}

			TextBox txtCountry = (TextBox)item.FindControl("txtCountry");
			if(txtCountry != null)
			{
				DataRow drCurrent = m_sdsZipcodeVASExcluded.ds.Tables[0].Rows[drIndex];
				drCurrent["country"] = txtCountry.Text;
			}
		}

		private void ResetDetailsGrid()
		{
			dgZipcodeVASExcluded.CurrentPageIndex = 0;
			btnZVASEInsert.Enabled = false;	
			btnZVASEInsertMultiple.Enabled = false;
			
			m_sdsZipcodeVASExcluded = SysDataMgrDAL.GetEmptyZipcodeVASExcludedDS(0);
			Session["SESSION_DS2"] = m_sdsZipcodeVASExcluded;
			lblZVASEMessage.Text = "";
			BindZVASEGrid();

		}

		private void btnZVASEInsertMultiple_Click(object sender, System.EventArgs e)
		{
			btnZVASEInsert.Enabled = false;	
			btnZVASEInsertMultiple.Enabled = false;
			dgZipcodeVASExcluded.CurrentPageIndex = 0;

		
		}

		private void btnSave_Click(object sender, System.EventArgs e)
		{
		
		}

		private DataView CreateVASCustomerOptions(bool showNilOption) 
		{
			DataTable dtVASCustomerOptions = new DataTable();
 
			dtVASCustomerOptions.Columns.Add(new DataColumn("Text", typeof(string)));
			dtVASCustomerOptions.Columns.Add(new DataColumn("StringValue", typeof(string)));

			ArrayList VASCustomerOptionArray = Utility.GetCodeValues(utility.GetAppID(),utility.GetUserCulture(),"vas_customer",CodeValueType.StringValue);

			if(showNilOption)
			{
				DataRow drNilRow = dtVASCustomerOptions.NewRow();
				drNilRow[0] = "";
				drNilRow[1] = "";
				dtVASCustomerOptions.Rows.Add(drNilRow);
			}
			foreach(SystemCode typeSysCode in VASCustomerOptionArray)
			{
				DataRow drEach = dtVASCustomerOptions.NewRow();
				drEach[0] = typeSysCode.Text;
				drEach[1] = typeSysCode.StringValue;
				dtVASCustomerOptions.Rows.Add(drEach);
			}

			DataView dvVASCustomerOptions = new DataView(dtVASCustomerOptions);
			return dvVASCustomerOptions;
		}

		private ICollection LoadVASCustomerOptions()
		{
			return m_dvVASCustomerOptions;
		}
		
	}
}
