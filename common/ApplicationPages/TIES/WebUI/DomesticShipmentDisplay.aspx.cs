using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.ties.DAL;
using com.common.classes;
using com.common.util;
using com.ties.classes;
using com.ties.BAL;

namespace com.ties
{
	/*public struct ServiceAvailable
	{
		public bool isServiceAvail;
		public String strVASCode;
	}*/

	/// <summary>
	/// Summary description for DomesticShipmentDisplay.
	/// </summary>
	public class DomesticShipmentDisplay : com.common.applicationpages.BasePopupPage
	{
		protected System.Web.UI.WebControls.Button btnQry;
		protected System.Web.UI.WebControls.Label lblConfirmMsg;
		protected System.Web.UI.WebControls.Button btnToSaveChanges;
		protected System.Web.UI.WebControls.Button btnNotToSave;
		protected System.Web.UI.WebControls.Button btnToCancel;
		protected System.Web.UI.WebControls.Panel ConfirmPanel;
		protected System.Web.UI.WebControls.Panel DomstcShipPanel;
		protected System.Web.UI.WebControls.Button btnExecQry;
		protected System.Web.UI.WebControls.Button btnInsert;
		protected System.Web.UI.WebControls.Button btnSave;
		protected System.Web.UI.WebControls.Button btnDelete;
		protected System.Web.UI.WebControls.Button btnGoToFirstPage;
		protected System.Web.UI.WebControls.Button btnPreviousPage;
		protected System.Web.UI.WebControls.Button btnNextPage;
		protected System.Web.UI.WebControls.Button btnGoToLastPage;
		protected System.Web.UI.WebControls.Label lblMainTitle;
		protected System.Web.UI.WebControls.Label lblErrorMsg;
		protected System.Web.UI.WebControls.TextBox txtGoToRec;
		protected System.Web.UI.WebControls.Label lblBookingNo;
		protected System.Web.UI.WebControls.TextBox txtBookingNo;
		protected System.Web.UI.WebControls.Label lblBookDate;
		protected System.Web.UI.WebControls.TextBox txtBookDate;
		protected System.Web.UI.WebControls.Label lblOrigin;
		protected System.Web.UI.WebControls.TextBox txtOrigin;
		protected System.Web.UI.WebControls.RequiredFieldValidator validConsgNo;
		protected System.Web.UI.WebControls.Label lblConsgmtNo;
		protected com.common.util.msTextBox txtConsigNo;
		protected System.Web.UI.WebControls.Label lblRefNo;
		protected System.Web.UI.WebControls.TextBox txtRefNo;
		protected System.Web.UI.WebControls.Label lblDestination;
		protected System.Web.UI.WebControls.TextBox txtDestination;
		protected System.Web.UI.WebControls.Label lblShipManifestDt;
		protected System.Web.UI.WebControls.TextBox txtShipManifestDt;
		protected System.Web.UI.WebControls.Label lblStatusCode;
		protected System.Web.UI.WebControls.TextBox txtLatestStatusCode;
		protected System.Web.UI.WebControls.Label lblRouteCode;
		protected System.Web.UI.WebControls.TextBox txtRouteCode;
		protected System.Web.UI.WebControls.Button btnRouteCode;
		protected System.Web.UI.WebControls.Label lblExcepCode;
		protected System.Web.UI.WebControls.TextBox txtLatestExcepCode;
		protected System.Web.UI.WebControls.Label lblDelManifest;
		protected System.Web.UI.WebControls.TextBox txtDelManifest;
		protected System.Web.UI.WebControls.RequiredFieldValidator validCustType;
		protected System.Web.UI.WebControls.Label lblCustType;
		protected System.Web.UI.WebControls.DropDownList ddbCustType;
		protected System.Web.UI.WebControls.CheckBox chkNewCust;
		protected System.Web.UI.WebControls.RequiredFieldValidator validCustID;
		protected System.Web.UI.WebControls.Label lblCustID;
		protected System.Web.UI.WebControls.TextBox txtCustID;
		protected System.Web.UI.WebControls.Button btnDisplayCustDtls;
		protected System.Web.UI.WebControls.RequiredFieldValidator validCustName;
		protected System.Web.UI.WebControls.Label lblName;
		protected System.Web.UI.WebControls.TextBox txtCustName;
		protected System.Web.UI.WebControls.Label lblPaymode;
		protected System.Web.UI.WebControls.RadioButton rbCash;
		protected System.Web.UI.WebControls.RadioButton rbCredit;
		protected System.Web.UI.WebControls.RequiredFieldValidator validCustAdd1;
		protected System.Web.UI.WebControls.Label lblAddress;
		protected System.Web.UI.WebControls.TextBox txtCustAdd1;
		protected System.Web.UI.WebControls.Label lblTelphone;
		protected System.Web.UI.WebControls.TextBox txtCustTelephone;
		protected System.Web.UI.WebControls.TextBox txtCustAdd2;
		protected System.Web.UI.WebControls.Label lblFax;
		protected System.Web.UI.WebControls.TextBox txtCustFax;
		protected System.Web.UI.WebControls.RequiredFieldValidator validCustZip;
		protected System.Web.UI.WebControls.Label lblZip;
		protected System.Web.UI.WebControls.TextBox txtCustZipCode;
		protected System.Web.UI.WebControls.TextBox txtCustCity;
		protected System.Web.UI.WebControls.TextBox txtCustStateCode;
		protected System.Web.UI.WebControls.CheckBox chkSendCustInfo;
		protected System.Web.UI.WebControls.RequiredFieldValidator validSendName;
		protected System.Web.UI.WebControls.Label lblSendNm;
		protected System.Web.UI.WebControls.TextBox txtSendName;
		protected System.Web.UI.WebControls.Button btnSendCust;
		protected System.Web.UI.WebControls.Label lblSendConPer;
		protected System.Web.UI.WebControls.TextBox txtSendContPer;
		protected System.Web.UI.WebControls.RequiredFieldValidator validSenderAdd1;
		protected System.Web.UI.WebControls.Label lblSendAddr1;
		protected System.Web.UI.WebControls.TextBox txtSendAddr1;
		protected System.Web.UI.WebControls.Label lblSendTel;
		protected System.Web.UI.WebControls.TextBox txtSendTel;
		protected System.Web.UI.WebControls.TextBox txtSendAddr2;
		protected System.Web.UI.WebControls.Label lblSendFax;
		protected System.Web.UI.WebControls.TextBox txtSendFax;
		protected System.Web.UI.WebControls.RequiredFieldValidator validSendZip;
		protected System.Web.UI.WebControls.Label lblSendZip;
		protected System.Web.UI.WebControls.TextBox txtSendZip;
		protected System.Web.UI.WebControls.TextBox txtSendCity;
		protected System.Web.UI.WebControls.TextBox txtSendState;
		protected System.Web.UI.WebControls.Label lblSendCuttOffTime;
		protected System.Web.UI.WebControls.TextBox txtSendCuttOffTime;
		protected System.Web.UI.WebControls.CheckBox chkRecip;
		protected System.Web.UI.WebControls.RequiredFieldValidator validRecipName;
		protected System.Web.UI.WebControls.Label lblRecipNm;
		protected System.Web.UI.WebControls.TextBox txtRecName;
		protected System.Web.UI.WebControls.Button btnRecipNm;
		protected System.Web.UI.WebControls.Label lblRecpContPer;
		protected System.Web.UI.WebControls.TextBox txtRecpContPer;
		protected System.Web.UI.WebControls.RequiredFieldValidator validRecipAdd1;
		protected System.Web.UI.WebControls.Label lblRecipAddr1;
		protected System.Web.UI.WebControls.TextBox txtRecipAddr1;
		protected System.Web.UI.WebControls.Label lblRecipTelephone;
		protected System.Web.UI.WebControls.TextBox txtRecipTel;
		protected System.Web.UI.WebControls.TextBox txtRecipAddr2;
		protected System.Web.UI.WebControls.Label lblRecipFax;
		protected System.Web.UI.WebControls.TextBox txtRecipFax;
		protected System.Web.UI.WebControls.RequiredFieldValidator validRecipZip;
		protected System.Web.UI.WebControls.Label lblRecipZip;
		protected System.Web.UI.WebControls.TextBox txtRecipZip;
		protected System.Web.UI.WebControls.TextBox txtRecipCity;
		protected System.Web.UI.WebControls.TextBox txtRecipState;
		protected System.Web.UI.WebControls.Label lblPkgActualWt;
		protected System.Web.UI.WebControls.TextBox txtPkgActualWt;
		protected System.Web.UI.WebControls.RequiredFieldValidator validCommCode;
		protected System.Web.UI.WebControls.Label lblPkgCommCode;
		protected System.Web.UI.WebControls.TextBox txtPkgCommCode;
		protected System.Web.UI.WebControls.Button btnPkgCommCode;
		protected System.Web.UI.WebControls.Label lblPkgDimWt;
		protected System.Web.UI.WebControls.TextBox txtPkgDimWt;
		protected System.Web.UI.WebControls.Button btnPkgDetails;
		protected System.Web.UI.WebControls.TextBox txtPkgCommDesc;
		protected System.Web.UI.WebControls.Label lblPkgTotPkgs;
		protected System.Web.UI.WebControls.TextBox txtPkgTotpkgs;
		protected System.Web.UI.WebControls.Label lblPkgChargeWt;
		protected System.Web.UI.WebControls.TextBox txtPkgChargeWt;
		protected System.Web.UI.WebControls.RequiredFieldValidator validSrvcCode;
		protected System.Web.UI.WebControls.Label lblShipSerCode;
		protected System.Web.UI.WebControls.TextBox txtShpSvcCode;
		protected System.Web.UI.WebControls.Button btnShpSvcCode;
		protected System.Web.UI.WebControls.TextBox txtShpSvcDesc;
		protected System.Web.UI.WebControls.Label lblShipPickUpTime;
		protected System.Web.UI.WebControls.TextBox txtShipPckUpTime;
		protected System.Web.UI.WebControls.RequiredFieldValidator validDclrValue;
		protected System.Web.UI.WebControls.Label lblShipDclValue;
		protected System.Web.UI.WebControls.TextBox txtShpDclrValue;
		protected System.Web.UI.WebControls.Label lblShipEstDlvryDt;
		protected System.Web.UI.WebControls.TextBox txtShipEstDlvryDt;
		protected System.Web.UI.WebControls.Label lblShipAdpercDV;
		protected System.Web.UI.WebControls.TextBox txtShpAddDV;
		protected System.Web.UI.WebControls.Label lblShipMaxCovg;
		protected System.Web.UI.WebControls.TextBox txtShpMaxCvrg;
		protected System.Web.UI.WebControls.Label lblShipInsSurchrg;
		protected System.Web.UI.WebControls.TextBox txtShpInsSurchrg;
		protected System.Web.UI.WebControls.CheckBox chkshpRtnHrdCpy;
		protected System.Web.UI.WebControls.RadioButton rbtnShipFrghtPre;
		protected System.Web.UI.WebControls.RadioButton rbtnShipFrghtColl;
		protected System.Web.UI.WebControls.Button btnPopulateVAS;
		protected System.Web.UI.WebControls.Button btnBind;
		protected System.Web.UI.WebControls.DataGrid dgVAS;
		protected System.Web.UI.WebControls.Button btnDGInsert;
		protected System.Web.UI.WebControls.Label lblFreightChrg;
		protected System.Web.UI.WebControls.TextBox txtFreightChrg;
		protected System.Web.UI.WebControls.Label lblInsChrg;
		protected System.Web.UI.WebControls.TextBox txtInsChrg;
		protected System.Web.UI.WebControls.Label lblTotVASSurChrg;
		protected System.Web.UI.WebControls.TextBox txtTotVASSurChrg;
		protected System.Web.UI.WebControls.Label lblESASurchrg;
		protected System.Web.UI.WebControls.TextBox txtESASurchrg;
		protected System.Web.UI.WebControls.Label lblShpTotAmt;
		protected System.Web.UI.WebControls.TextBox txtShpTotAmt;
		protected System.Web.UI.WebControls.ValidationSummary PageValidationSummary;
		protected System.Web.UI.HtmlControls.HtmlTable MainTable;
		protected System.Web.UI.HtmlControls.HtmlTable TblBookDtl;
		//Utility utility = null;
		String appID = null;
		String enterpriseID = null;
		String userID = null;
		DataSet m_dsVAS = null;
		private String strErrorMsg = null;
		DataSet m_dsPkgDetails = null;
		DomesticShipmentMgrBAL domesticMgrBAL = null;
		SessionDS m_sdsDomesticShip = null;
		protected System.Web.UI.WebControls.Button btnClose;
		protected System.Web.UI.WebControls.Label lblActualDeliveryDt;
		protected System.Web.UI.WebControls.TextBox txtActualDeliveryDt;
		protected System.Web.UI.HtmlControls.HtmlTable tblCustInfo;
		protected System.Web.UI.HtmlControls.HtmlTable tblSenderInfo;
		protected System.Web.UI.HtmlControls.HtmlTable tblRecepInfo;
		protected System.Web.UI.HtmlControls.HtmlTable tblPkgInfo;
		protected System.Web.UI.HtmlControls.HtmlTable tblShipService;
		protected System.Web.UI.HtmlControls.HtmlTable tblShipHandling;
		protected System.Web.UI.HtmlControls.HtmlTable tblCharges;
		protected System.Web.UI.WebControls.Label lblCustInfo;
		protected System.Web.UI.WebControls.Label lblSendInfo;
		protected System.Web.UI.WebControls.Label lblRepInfo;
		protected System.Web.UI.WebControls.Label lblPkgDetails;
		protected System.Web.UI.WebControls.Label lblshipHan;
		protected System.Web.UI.WebControls.Label lblService;
		protected System.Web.UI.WebControls.Label lblCODAmount;
		protected System.Web.UI.WebControls.TextBox txtCODAmount;
		static int m_iSetSize = 10;

		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings,Page.Session);
			appID = utility.GetAppID();
			enterpriseID = utility.GetEnterpriseID();
			userID = utility.GetUserID();
			if (!IsPostBack)
			{
				btnPkgCommCode.Attributes.Add("onclick","return CallCommodityWin()");
				validCustID.ErrorMessage = "Cust ID is Required.";
				validCustZip.ErrorMessage = "Customer zip Code is required";
				validSendZip.ErrorMessage = "Sender Zip Code is required";
				validSendName.ErrorMessage = "Sender Name is required";
				validRecipName.ErrorMessage = "Recipient Name is required";
				validRecipZip.ErrorMessage = "Recipient Zip Code is required";
				validConsgNo.ErrorMessage = "Consignment Number is required";
				validDclrValue.ErrorMessage = "Declare value is required";
				validSrvcCode.ErrorMessage = "Service code is required";
				validCommCode.ErrorMessage = "Commodity code is required";
				validCustName.ErrorMessage = "Customer Name is required";
				validCustAdd1.ErrorMessage = "Customer Address is required";

				m_dsVAS = DomesticShipmentMgrDAL.GetEmptyVASDS();
				Session["SESSION_DS1"] = m_dsVAS;
				BindVASGrid();

				rbtnShipFrghtPre.Enabled = false;
				rbtnShipFrghtColl.Enabled = false;
				LoadCustomerTypeList();
				btnGoToFirstPage.Enabled = false;
				btnGoToLastPage.Enabled = false;
				btnNextPage.Enabled = false;
				btnPreviousPage.Enabled = false;
				btnInsert.Enabled=true;
				btnQry.Enabled = true;				
				btnDelete.Enabled=false;
				btnExecQry.Enabled = true;
				chkNewCust.Checked = false;
				chkSendCustInfo.Checked = false;
				chkRecip.Checked = false;
				rbCredit.Checked = true;
				chkshpRtnHrdCpy.Checked = true;
				//Flag used for checking if it is holiday/weekend extra surcharge will be imposed.
				ViewState["AddSurcharge"] = "none";
				//Saving the VAS Code the holiday/weekend service
				ViewState["ServiceVASCode"] = "";
				ViewState["isHoliday"] = false;
				ViewState["isTextChanged"] = false;
				ViewState["DSMode"] = ScreenMode.None;
				ViewState["DSOperation"] = Operation.None;
				Session["SESSION_DS3"] = m_sdsDomesticShip;
				ViewState["CurrentSetSize"]=0;
				ViewState["Operations"] = Operation.None;
				ViewState["Day"] = "";
				ViewState["ServiceExists"] = false;
				//On load default screen to Query Mode
				ViewState["DSMode"] = ScreenMode.Query;
				btnPkgDetails.Enabled = false;
				ResetForQuery();
				//Flag for retrieval of packages from pickup.
				ViewState["PickupBooking"]="No";

				DataSet profileDS = SysDataManager1.GetEnterpriseProfile(appID, enterpriseID);

				if(profileDS.Tables[0].Rows.Count > 0)
				{
					int currency_decimal = (int) profileDS.Tables[0].Rows[0]["currency_decimal"];
					ViewState["m_format"] = "{0:F" + currency_decimal.ToString() + "}";
				}

				validConsgNo.Enabled = false;
				validCustType.Enabled = false;
				validCustID.Enabled = false;
				validCustName.Enabled = false;
				validCustAdd1.Enabled = false;
				validCustZip.Enabled = false;
				validSendName.Enabled = false;
				validSenderAdd1.Enabled = false;
				validSendZip.Enabled = false;
				validRecipName.Enabled = false;
				validRecipAdd1.Enabled = false;
				validRecipZip.Enabled = false;
				validCommCode.Enabled = false;
				validSrvcCode.Enabled = false;
				validDclrValue.Enabled = false;
			}
			else
			{
				m_dsVAS = (DataSet)Session["SESSION_DS1"];
				m_sdsDomesticShip = (SessionDS)Session["SESSION_DS3"];
			}
			if (!IsPostBack)
			{
				ExecuteQueryDS();
			}
			PageValidationSummary.HeaderText = Utility.GetLanguageText(ResourceType.UserMessage, "PLS_ENTER_MISS_FIELD", utility.GetUserCulture());
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			base.OnInit(e);
			InitializeComponent();
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnExecQry.Click += new System.EventHandler(this.btnExecQry_Click);
			this.btnDisplayCustDtls.Click += new System.EventHandler(this.btnDisplayCustDtls_Click);
			this.btnShpSvcCode.Click += new System.EventHandler(this.btnShpSvcCode_Click);
			this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion

		public void LoadCustomerTypeList()
		{
			ListItem defItem = new ListItem();
			defItem.Text = "";
			defItem.Value = "0";
			ddbCustType.Items.Add(defItem);
			ArrayList systemCodes = Utility.GetCodeValues(appID,utility.GetUserCulture(),"customer_type",CodeValueType.StringValue);
			foreach(SystemCode sysCode in systemCodes)
			{	
				ListItem lstItem = new ListItem();
				lstItem.Text = sysCode.Text;
				lstItem.Value = sysCode.StringValue;
				ddbCustType.Items.Add(lstItem);
			}
		}

		private void GetValuesIntoDS(int index)
		{
			//get the parameters
			String strFormId = Request.Params["FORMID"];
			String strBookNo = Request.Params["BOOK_NO"];
			String strConsign = Request.Params["CONSIGN"];

			strErrorMsg = "";

			DataRow drEach = m_sdsDomesticShip.ds.Tables[0].Rows[index];
			if (strFormId == "ShipmentTracking.aspx")
			{
				drEach["booking_no"] = Convert.ToInt64(strBookNo.ToString().Trim());
			}
			else
			{

				if(txtBookingNo.Text.Trim().Length > 0)
					drEach["booking_no"] = Convert.ToInt64(txtBookingNo.Text.Trim());
				else
					drEach["booking_no"] = 0;
			}

			if (strFormId == "ShipmentTracking.aspx")
			{
				drEach["consignment_no"] = strConsign.ToString().Trim();
			}
			else
			{
				drEach["consignment_no"] = txtConsigNo.Text.Trim() ;
			}

			if(txtRefNo.Text.Trim() != "")
			{
				drEach["ref_no"] = txtRefNo.Text.Trim();
			}

			if(txtBookDate.Text.Trim().Length > 0)
			{
				drEach["booking_datetime"] = System.DateTime.ParseExact(txtBookDate.Text.Trim(),"dd/MM/yyyy HH:mm",null);
			}

			if(txtCustID.Text.Trim() != "")
			{
				drEach["payerid"] = txtCustID.Text.Trim();
			}

			if(ddbCustType.SelectedItem.Text.ToString() != "")
			{
				drEach["payer_type"] = ddbCustType.SelectedItem.Value.ToString();
			}

			if(chkNewCust.Checked == true)
				drEach["new_account"] = "Y";
			else
				drEach["new_account"] = "N";

			if(txtCustName.Text.Trim() != "")
			{
				drEach["payer_name"] = txtCustName.Text.Trim();
			}

			if(txtCustAdd1.Text.Trim() != "")
			{
				drEach["payer_address1"] = txtCustAdd1.Text.Trim();
			}

			if(txtCustAdd2.Text.Trim() != "")
			{
				drEach["payer_address2"] = txtCustAdd2.Text.Trim();
			}

			if(txtCustZipCode.Text.Trim() != "")
			{
				drEach["payer_zipcode"] = txtCustZipCode.Text.Trim();
			}

			if(txtCustStateCode.Text.Trim() != "")
			{
				drEach["payer_country"] = txtCustStateCode.Text.Trim();
			}

			if(txtCustTelephone.Text.Trim() != "")
			{
				drEach["payer_telephone"] = txtCustTelephone.Text.Trim();
			}

			if(txtCustFax.Text.Trim() != "")
			{
				drEach["payer_fax"] = txtCustFax.Text.Trim();
			}

			if(rbCash.Checked == true)
				drEach["payment_mode"] = "C";
			else if(rbCredit.Checked == true)
				drEach["payment_mode"] = "R";
			
			if(txtSendName.Text.Trim() != "")
			{
				drEach["sender_name"] = txtSendName.Text.Trim();
			}

			if(txtSendAddr1.Text.Trim() != "")
			{
				drEach["sender_address1"] = txtSendAddr1.Text.Trim();
			}

			if(txtSendAddr2.Text.Trim() != "")
			{
				drEach["sender_address2"] = txtSendAddr2.Text.Trim();
			}

			if(txtSendZip.Text.Trim() != "")
			{
				drEach["sender_zipcode"] = txtSendZip.Text.Trim();
			}

			if(txtSendState.Text.Trim() != "")
			{
				drEach["sender_country"] = txtSendState.Text.Trim();
			}

			if(txtSendTel.Text.Trim() != "")
			{
				drEach["sender_telephone"] = txtSendTel.Text.Trim();
			}

			if(txtSendFax.Text.Trim() != "")
			{
				drEach["sender_fax"] = txtSendFax.Text.Trim();
			}

			if(txtSendContPer.Text.Trim() != "")
			{
				drEach["sender_contact_person"] = txtSendContPer.Text.Trim();
			}

			if(txtRecName.Text.Trim() != "")
			{
				drEach["recipient_name"] = txtRecName.Text.Trim();
			}

			if(txtRecipAddr1.Text.Trim() != "")
			{
				drEach["recipient_address1"] = txtRecipAddr1.Text.Trim();
			}

			if(txtRecipAddr2.Text.Trim() != "")
			{
				drEach["recipient_address2"] = txtRecipAddr2.Text.Trim();
			}

			if(txtRecipZip.Text.Trim() != "")
			{
				drEach["recipient_zipcode"] = txtRecipZip.Text.Trim();
			}

			if(txtRecipState.Text.Trim() != "")
			{
				drEach["recipient_country"] = txtRecipState.Text.Trim();
			}

			if(txtPkgActualWt.Text.Trim().Length > 0)
			{
				drEach["tot_wt"] = Convert.ToDecimal(txtPkgActualWt.Text.Trim());
			}
			else
			{
				drEach["tot_wt"] = 0;
			}
			if(txtRecipTel.Text.Trim() != "")
			{
				drEach["recipient_telephone"] = txtRecipTel.Text.Trim();
			}

			if(txtPkgDimWt.Text.Trim().Length > 0)
			{
				drEach["tot_dim_wt"] = Convert.ToDecimal(txtPkgDimWt.Text.Trim());
			}
			else
			{
				drEach["tot_dim_wt"] = 0;
			}
			if(txtRecipFax.Text.Trim() != "")
			{
				drEach["recipient_fax"] = txtRecipFax.Text.Trim();
			}
			if(txtPkgTotpkgs.Text.Trim().Length > 0)
			{
				drEach["tot_pkg"] = Convert.ToDecimal(txtPkgTotpkgs.Text.Trim());
			}
			else
			{
				drEach["tot_pkg"] = 0;
			}

			if(txtShpSvcCode.Text.Trim() != "")
			{
				drEach["service_code"] = txtShpSvcCode.Text.Trim();
			}

			if(txtRecpContPer.Text.Trim() != "")
			{
				drEach["recipient_contact_person"] = txtRecpContPer.Text.Trim();
			}

			if(txtShpDclrValue.Text.Trim().Length > 0)
			{
				drEach["declare_value"] = Convert.ToDecimal(txtShpDclrValue.Text.Trim());
			}
			else
			{
				drEach["declare_value"] = 0;
			}
			if(txtPkgChargeWt.Text.Trim().Length > 0)
			{
				drEach["chargeable_wt"] = Convert.ToDecimal(txtPkgChargeWt.Text.Trim());
			}
			else
			{
				drEach["chargeable_wt"] = 0;
			}
			if(txtShpInsSurchrg.Text.Trim().Length > 0)
			{
				drEach["insurance_surcharge"] = Convert.ToDecimal(txtShpInsSurchrg.Text.Trim());
			}
			else
			{
				drEach["insurance_surcharge"] = 0;
			}
			if(txtShpMaxCvrg.Text.Trim().Length > 0)
			{
				drEach["max_insurance_cover"] = Convert.ToDecimal(txtShpMaxCvrg.Text.Trim());
			}
			else
			{
				drEach["max_insurance_cover"] = 0;
			}
			if(txtShipPckUpTime.Text.Trim().Length > 0)
			{
				drEach["act_pickup_datetime"] = System.DateTime.ParseExact(txtShipPckUpTime.Text.Trim(),"dd/MM/yyyy HH:mm",null);
			}
			if(txtPkgCommCode.Text.Trim() != "")
			{
				drEach["commodity_code"] = txtPkgCommCode.Text.Trim();
			}

			if (txtShipEstDlvryDt.Text.Trim().Length > 0)
			{
				drEach["est_delivery_datetime"] = System.DateTime.ParseExact(txtShipEstDlvryDt.Text.Trim(),"dd/MM/yyyy HH:mm",null);
			}

			if(txtShpAddDV.Text.Trim().Length > 0)
			{
				drEach["percent_dv_additional"] = txtShpAddDV.Text.Trim();
			}
			
			//drEach["act_delivery_date"] = DateTime.Now;

			if(rbtnShipFrghtPre.Checked == true)
				drEach["payment_type"] = "FP";
			else if (rbtnShipFrghtColl.Checked == true)
				drEach["payment_type"] = "FC";
			

			if(chkshpRtnHrdCpy.Checked == true)
				drEach["return_pod_slip"] = "Y";
			else if(chkshpRtnHrdCpy.Checked == false)
				drEach["return_pod_slip"] = "N";
			
			drEach["shipment_type"]= "D";
			
			if(txtSendCity.Text.Trim() != "")
			{
				drEach["origin_state_code"] = txtSendCity.Text.Trim();
			}
			

			//drEach["invoice_no"] = "";
			

			if(txtRecipCity.Text.Trim() != "")
			{
				drEach["destination_state_code"] = txtRecipCity.Text.Trim();
			}
			
			//drEach["invoice_amt"] = 0;

			if(txtTotVASSurChrg.Text.Trim().Length > 0)
			{
				drEach["tot_vas_surcharge"] = Convert.ToInt64(txtTotVASSurChrg.Text.Trim());
			}
			else
			{
				drEach["tot_vas_surcharge"] = 0;
			}

			//drEach["debit_amt"] = 0;
			//drEach["credit_amt"] = 0;
			//drEach["cash_collected"] = 0;

			if(txtLatestStatusCode.Text.Trim() != "")
			{
				drEach["last_status_code"] = txtLatestStatusCode.Text.Trim();
			}

			//drEach["last_exception_code"] = "";
			
			//drEach["last_status_datetime"] = DateTime.Now;
			
			if(txtShipManifestDt.Text.Trim().Length > 0)
			{
				drEach["shpt_manifest_datetime"] = System.DateTime.ParseExact(txtShipManifestDt.Text.Trim(),"dd/MM/yyyy HH:mm",null);
			}
			if(txtDelManifest.Text.Trim().Length > 0)
			{
				drEach["delivery_manifested"] =  txtDelManifest.Text.Trim().Substring(0,1);
			}
		
			if(txtRouteCode.Text.Trim() != "")
			{
				drEach["route_code"] = txtRouteCode.Text.Trim();
			}
			
			//Get the quotation number & version 
			QuotationData custData;
			custData.iQuotationVersion = 0;
			custData.strQuotationNo= null;

			if(ddbCustType.SelectedItem.Value != "0")
			{
				if (ddbCustType.SelectedItem.Value == "C")
				{
					try
					{
						custData = TIESUtility.CustomerQtnActive(appID,enterpriseID,txtCustID.Text.Trim());
					}
					catch(ApplicationException appException)
					{
						String strMsg = appException.Message;
						strErrorMsg = strMsg;
						return;
					}
				}
				else if (ddbCustType.SelectedItem.Value == "A")
				{
					try
					{
						custData = TIESUtility.AgentQtnActive(appID,enterpriseID,txtCustID.Text.Trim());
					}
					catch(ApplicationException appException)
					{
						String strMsg = appException.Message;
						strErrorMsg = strMsg;
						return;
					}
				}
			
				drEach["quotation_no"] = custData.strQuotationNo;
			
				drEach["quotation_version"] = custData.iQuotationVersion.ToString();
			}

			if(txtPkgCommCode.Text.Trim() != "")
			{
				drEach["commodity_code"] = txtPkgCommCode.Text.Trim();
			}

			
			if(txtFreightChrg.Text.Trim().Length > 0)
			{
				drEach["tot_freight_charge"] = Convert.ToDecimal(txtFreightChrg.Text.Trim());
			}
			else
			{
				drEach["tot_freight_charge"] = 0;
			}

			if(txtESASurchrg.Text.Trim().Length > 0)
			{
				drEach["esa_surcharge"] = Convert.ToDecimal(txtESASurchrg.Text.Trim());
			}
			else
			{
				drEach["esa_surcharge"] = 0;
			}
			if(txtActualDeliveryDt.Text.Trim().Length > 0)
			{
				drEach["esa_surcharge"] = txtActualDeliveryDt.Text.Trim();
			}
			

			lblErrorMsg.Text = strErrorMsg;
		}
		private void ClearAllFields()
		{
			txtBookDate.Text = "";
			txtBookingNo.Text = "";
			txtOrigin.Text = "";
			txtDestination.Text = "";
			txtLatestStatusCode.Text = "";
			txtLatestExcepCode.Text = "";
			txtDelManifest.Text = "";
			ddbCustType.SelectedItem.Selected = false;
			ddbCustType.Items.FindByValue("0").Selected = true;
			txtConsigNo.Text = "";
			txtRefNo.Text = "";
			txtShipManifestDt.Text = "";
			txtRouteCode.Text = "";
			txtGoToRec.Text = "";
			txtCustID.Text = "";
			txtCustName.Text = "";
			txtCustAdd1.Text = "";
			txtCustAdd2.Text = "";
			txtCustZipCode.Text = "";
			txtCustCity.Text = "";
			txtCustStateCode.Text = "";
			txtCustTelephone.Text = "";
			txtCustFax.Text = "";
			txtActualDeliveryDt.Text="";
			chkNewCust.Checked = false;
			chkSendCustInfo.Checked = false;
			chkRecip.Checked = false;

			if((int)ViewState["DSMode"]!=(int)ScreenMode.Query)
			{
				rbCredit.Checked = true;
			}

			txtSendAddr1.Text = "";
			txtSendAddr2.Text = "";
			txtSendName.Text = "";
			txtSendZip.Text = "";
			txtSendCity.Text = "";
			txtSendState.Text = "";
			txtSendCuttOffTime.Text = "";
			txtSendContPer.Text = "";
			txtSendTel.Text = "";
			txtSendFax.Text = "";
			txtRecName.Text = "";
			txtRecipAddr1.Text = "";
			txtRecipAddr2.Text = "";
			txtRecipZip.Text = "";
			txtRecipCity.Text = "";
			txtRecipState.Text = "";
			txtRecpContPer.Text = "";
			txtRecipTel.Text = "";
			txtRecipFax.Text = "";
			txtPkgActualWt.Text = "";
			txtPkgChargeWt.Text = "";
			txtPkgCommCode.Text = "";
			txtPkgDimWt.Text = "";
			txtPkgCommDesc.Text = "";
			txtPkgDimWt.Text = "";
			txtPkgTotpkgs.Text = "";
			txtShipEstDlvryDt.Text = "";
			txtShipManifestDt.Text = "";
			txtShipPckUpTime.Text = "";
			txtShpAddDV.Text = "";
			txtShpDclrValue.Text = "";
			txtShpInsSurchrg.Text = "";
			txtShpMaxCvrg.Text = "";
			txtShpSvcCode.Text = "";
			txtShpSvcDesc.Text = "";
			txtShpTotAmt.Text = "";
			txtTotVASSurChrg.Text = "";
			txtFreightChrg.Text = "";
			txtInsChrg.Text = "";
			txtTotVASSurChrg.Text = "";
			txtShpTotAmt.Text = "";

		}
		private void btnInsert_Click(object sender, System.EventArgs e)
		{
			m_sdsDomesticShip = DomesticShipmentMgrDAL.GetDomShipSessionDS();
			Session["SESSION_DS3"] = m_sdsDomesticShip;

			btnPkgDetails.Enabled = true;

			if (((int)ViewState["DSOperation"] == (int)Operation.Insert) || ((int)ViewState["DSOperation"] == (int)Operation.Update))
			{
				lblConfirmMsg.Text = "Do you want to save changes? ";
				DomstcShipPanel.Visible = true;
				ddbCustType.Visible = false;
				ViewState["MoveFirst"]=true;			
				return;
			}

			Session["SESSION_DS2"] = null;
			lblErrorMsg.Text = "";
			btnSave.Enabled=true;
			btnExecQry.Enabled = false;
			btnDelete.Enabled=false;
			rbtnShipFrghtColl.Enabled = true;
			rbtnShipFrghtPre.Enabled = true;
			rbtnShipFrghtPre.Checked = true;
			rbCredit.Checked = true;
			rbCash.Checked = false;
			ClearAllFields();

			String strDt = DateTime.Now.ToString("dd/MM/yyyy HH:mm");
			txtBookDate.Text = strDt;
			txtShipManifestDt.Text = strDt;
			txtLatestStatusCode.Text = TIESUtility.getInitialShipmentStatusCode(appID,enterpriseID);
			txtDelManifest.Text = TIESUtility.getInitialDlvrymanifest(appID,enterpriseID);
			chkSendCustInfo.Checked = false;
			chkRecip.Checked = false;
			txtShipPckUpTime.Text = DateTime.Now.ToString("dd/MM/yyyy HH:mm");
			
			m_dsVAS = DomesticShipmentMgrDAL.GetEmptyVASDS();
			Session["SESSION_DS1"] = m_dsVAS;
			BindVASGrid();

			ViewState["AddSurcharge"] = "none";
			ViewState["DSMode"]=ScreenMode.Insert;
			ViewState["currentPage"] = 0;

			txtBookingNo.Enabled = true;
			txtBookDate.Enabled = false;
			txtDelManifest.Enabled = false;
			txtShipManifestDt.Enabled = false;
			//txtRouteCode.Enabled = false;
		}

		private ServiceAvailable CheckDayServiceAvail()
		{
			bool isWeekHolidaySrvAvail = false;
			ServiceData serviceData;
			serviceData.isServiceAvail = false;
			serviceData.strVASCode = null;
			DateTime dtDlvryDt;
			String strWeekDay = "";
			String strDayType = "";
			String strVASCode = null;
			ServiceAvailable serviceAvailable = new ServiceAvailable();
			ViewState["Day"] = "";

			strErrorMsg = "";
			//Get the day from the delivery date.
			dtDlvryDt = System.DateTime.ParseExact(txtShipEstDlvryDt.Text.ToString(),"dd/MM/yyyy HH:mm",null);
			strWeekDay = dtDlvryDt.DayOfWeek.ToString();

			if ((strWeekDay.Equals("Saturday")) || (strWeekDay.Equals("Sunday")))
			{
				//Check whether the service is there for saturday or sunday
				strDayType = "W";

				ViewState["Day"] = "WeekEnd";

				try
				{
					serviceData = TIESUtility.IsServiceAvailable(appID,enterpriseID,txtSendZip.Text.Trim(),strWeekDay,strDayType);
				}
				catch(ApplicationException appException)
				{
					String strMsg = appException.Message;
					strErrorMsg = strMsg;
					return serviceAvailable;
				}
				
				//Flag which shows service available or not
				isWeekHolidaySrvAvail = serviceData.isServiceAvail;

				ViewState["ServiceExists"] = isWeekHolidaySrvAvail;

				//VAS Code for the service
				strVASCode = serviceData.strVASCode;

			}

			//Check whether the delivery date is on holiday 
			bool isHoliday = false;
			try
			{
				isHoliday = TIESUtility.IsDayHoliday(appID,enterpriseID,System.DateTime.ParseExact(txtShipEstDlvryDt.Text.Trim().ToString(),"dd/MM/yyyy HH:mm",null));
			}
			catch(ApplicationException appException)
			{
				String strMsg = appException.Message;
				strErrorMsg = strMsg;
				return serviceAvailable;
			}

			if(isHoliday == true)
			{
				ViewState["isHoliday"] = true;
				//check whether service is provided for holiday
				strDayType = "H";
				
				ViewState["Day"] = "Holiday";

				try
				{
					serviceData	= TIESUtility.IsServiceAvailable(appID,enterpriseID,txtSendZip.Text.Trim(),strWeekDay,strDayType);
				}
				catch(ApplicationException appException)
				{
					String strMsg = appException.Message;
					strErrorMsg = strMsg;
					return serviceAvailable;
				}
				
				//Flag which shows service available or not
				isWeekHolidaySrvAvail = serviceData.isServiceAvail;
				
				ViewState["ServiceExists"] = isWeekHolidaySrvAvail;
				
				//VAS Code for the service
				strVASCode = serviceData.strVASCode; 
				
			}
			
			serviceAvailable.isServiceAvail = isWeekHolidaySrvAvail;
			serviceAvailable.strVASCode = strVASCode;
			
			return serviceAvailable;
		}

		private void CalcAdditionalSurchrg(String strVASCode)
		{
			//The service is available
			QuotationData quotationData;
			quotationData.iQuotationVersion = 0;
			quotationData.strQuotationNo = null;
			int iQuotnVersion = 0;
			String strQuotnNo = null;

			//Get the quotation number & version 
			if(ddbCustType.SelectedItem.Value == "C")
			{
				quotationData =  TIESUtility.CustomerQtnActive(appID,enterpriseID,txtCustID.Text.Trim());
				iQuotnVersion = quotationData.iQuotationVersion;
				strQuotnNo = quotationData.strQuotationNo;
			}
			else if(ddbCustType.SelectedItem.Value == "A")
			{
				quotationData = TIESUtility.AgentQtnActive(appID,enterpriseID,txtCustID.Text.Trim());
				iQuotnVersion = quotationData.iQuotationVersion;
				strQuotnNo = quotationData.strQuotationNo;
			}

			//Check whether the quotation is active.
			String strQuotVASCode = null;
			String strVasDesc = null;
			decimal decQuotSurchrg;
			VASSurcharge  vasSurcharge;
			

			if(strQuotnNo != null)
			{
				vasSurcharge = TIESUtility.GetSurchrgVASCode(appID,enterpriseID,ddbCustType.SelectedItem.Value,txtCustID.Text.Trim(),strQuotnNo,iQuotnVersion,strVASCode);
				decQuotSurchrg = vasSurcharge.decSurcharge;			
				strQuotVASCode = vasSurcharge.strVASCode;
				strVasDesc = vasSurcharge.strVASDesc;
			}
			else //There is no active quotation
			{
				//Get the Surcharge from the Vas base table
				VAS vas = new VAS();
				vas.Populate(appID,enterpriseID,strVASCode);
				decQuotSurchrg = vas.Surcharge;
				strQuotVASCode = vas.VASCode;
				strVasDesc = vas.VasDescription;
			}

			if(strQuotVASCode != null)
			{
				//Add the record to the VAS DataSet	& bind to the GRID
				AddRowInVASGrid();	
				dgVAS.EditItemIndex = m_dsVAS.Tables[0].Rows.Count - 1;

				DataRow drCurrent = m_dsVAS.Tables[0].Rows[dgVAS.EditItemIndex];
				drCurrent["surcharge"] = decQuotSurchrg;
				drCurrent["vas_code"] = strQuotVASCode;
				drCurrent["vas_description"] = strVasDesc;

				//Display the Total VAS Surcharge
				if(txtTotVASSurChrg.Text.Length > 0)
				{
					txtTotVASSurChrg.Text = String.Format((String)ViewState["m_format"], Convert.ToDecimal(txtTotVASSurChrg.Text)+decQuotSurchrg);
				}
				else
				{
					txtTotVASSurChrg.Text = String.Format((String)ViewState["m_format"], decQuotSurchrg);
				}

				//Display the Total of all charges
				CalculateTotalAmt();

				dgVAS.EditItemIndex = -1;
				BindVASGrid();
				
			}
		}

		private void SaveUpdateRecord()
		{

			if(dgVAS.EditItemIndex != -1)
			{
				DataRow drCurrent = m_dsVAS.Tables[0].Rows[dgVAS.EditItemIndex];
				drCurrent.Delete();
			}

			ServiceAvailable serviceAvailable =  CheckDayServiceAvail();


			m_dsPkgDetails = (DataSet)Session["SESSION_DS2"];
			
			strErrorMsg = "";
			
			if((DomesticShipmentMgrDAL.IsConsgmentExist(appID,enterpriseID,txtConsigNo.Text.Trim()) == true) && ((int)ViewState["DSMode"] != (int)ScreenMode.ExecuteQuery))
			{
				strErrorMsg = "The Consignment number already exists.";
			}
			else if((m_dsPkgDetails == null) && ((int)ViewState["DSMode"] != (int)ScreenMode.ExecuteQuery))
			{
				strErrorMsg = "Please enter the package details";
			}
			else if(m_dsVAS == null)
			{
				strErrorMsg = "Please enter the VAS details";
			}
			else if((serviceAvailable.isServiceAvail == true) && ((String)ViewState["AddSurcharge"] == "none"))
			{
				ViewState["ServiceVASCode"] = serviceAvailable.strVASCode;
				//display the popup msg
				strErrorMsg = "Weekend / Public Holiday Delivery Service is available. Weekend / Public Holiday Surcharge ";
				lblConfirmMsg.Text = "Weekend / Public Holiday Delivery Service is available. Weekend / Public Holiday Surcharge will be imposed. Do you want to continue?";
				DomstcShipPanel.Visible = true;
				ddbCustType.Visible = false;
				ViewState["AddSurcharge"] = "asking";
			}
			else
			{

				//if no service available then get the next working day...
				if((bool)ViewState["ServiceExists"] == false && (((String)ViewState["Day"] == "WeekEnd" ||((String)ViewState["Day"] == "Holiday"))))
				{
					ReCalculateDlvryDt();
				}
				
				GetValuesIntoDS((int)(ViewState["currentPage"]));

				DataSet dsDomesticShipment = m_sdsDomesticShip.ds.GetChanges();
				if(((txtBookingNo.Text.Length == 0) && ((int)ViewState["DSMode"] == (int)ScreenMode.Insert) && ((int)ViewState["DSOperation"] == (int)Operation.Insert))
					|| ((txtBookingNo.Text.Length > 0) && ((int)ViewState["DSMode"] == (int)ScreenMode.Insert) && ((int)ViewState["DSOperation"] == (int)Operation.Insert)))
				{
					try
					{
//						txtBookingNo.Text = DomesticShipmentMgrDAL.AddDomesticShipment(appID,enterpriseID,dsDomesticShipment,m_dsVAS,m_dsPkgDetails,userID).ToString();
						ViewState["isTextChanged"] = false;
						ChangeDSState();
						m_sdsDomesticShip.ds.Tables[0].Rows[(int)ViewState["currentPage"]].AcceptChanges();
					}
					catch(ApplicationException appException)
					{
						String strMsg = appException.Message;
						if(strMsg.IndexOf("duplicate key") != -1 )
						{
							strErrorMsg = "Duplicate Key cannot save the record";
						}
						else if(strMsg.IndexOf("FK") != -1 )
						{
							strErrorMsg = "Foreign Key Error cannot save the record";
						}
						else
						{
							strErrorMsg = strMsg;
						}
						lblErrorMsg.Text = strErrorMsg;
						return;
					}
					
					strErrorMsg = "Created Successfully.";
					//ViewState["DSOperation"] = Operation.None;
				}
				else if((txtBookingNo.Text.Length > 0) && ((int)ViewState["DSOperation"] == (int)Operation.Update))
				{
					try
					{

						DomesticShipmentMgrDAL.UpdateDomesticShp(appID,enterpriseID,dsDomesticShipment,m_dsVAS,m_dsPkgDetails);
						ViewState["isTextChanged"] = false;
						ChangeDSState();
						m_sdsDomesticShip.ds.Tables[0].Rows[(int)ViewState["currentPage"]].AcceptChanges();
					}
					catch(ApplicationException appException)
					{
						String strMsg = appException.Message;
						if(strMsg.IndexOf("duplicate key") != -1 )
						{
							strErrorMsg = "Duplicate Key cannot update the record";
						}
						else if(strMsg.IndexOf("FK") != -1 )
						{
							strErrorMsg = "Foreign Key Error cannot update the record";
						}
						else
						{
							strErrorMsg = appException.Message.ToString();
						}
						return;
					}
					strErrorMsg = "Updated Successfully";
				}

				txtBookingNo.Enabled = false;
			}
			
			lblErrorMsg.Text = strErrorMsg;
		}
		private void btnSave_Click(object sender, System.EventArgs e)
		{
			SaveUpdateRecord();
			ViewState["isTextChanged"] = false;
		}

		private void txtBookingNo_TextChanged(object sender, System.EventArgs e)
		{
			if ((txtBookingNo.Text.Trim().Length > 0) && ((int)ViewState["DSMode"] == (int)ScreenMode.Insert))
			{
				BookingNoDetails();
				ViewState["Operations"] = Operation.Insert;
			}
		}

		/// <BookingNoDetails>
		/// This method displays the details for the domestic shipment from the pickuprequest table.
		/// </BookingNoDetails>
		private void BookingNoDetails()
		{
			lblErrorMsg.Text = "";

			int iBookingNo = Convert.ToInt32(txtBookingNo.Text.Trim());

			//check whether the booking no. already exists in the domestic shipment.
//			bool IsBookingNoExist = DomesticShipmentMgrDAL.IsBookingNoExist(appID,enterpriseID,iBookingNo);
//	
//			if(IsBookingNoExist)
//			{
//				lblErrorMsg.Text = "Domestic shipment for this booking no is already created";
//				return;
//			}


			ViewState["DSMode"] = ScreenMode.Insert;
			strErrorMsg = "";
			Enterprise enterprise = null;
			Zipcode zipCode = null;
			
			DataSet dsPickUpData = DomesticShipmentMgrDAL.GetFromPickUp(appID,enterpriseID,iBookingNo);
			int cnt = dsPickUpData.Tables[0].Rows.Count;
			if (cnt>0)
			{
				ViewState["PickupBooking"] = "Yes";

				DataRow drEach = dsPickUpData.Tables[0].Rows[0];
				DateTime dtBookingDateTime = DateTime.Now;
				if(!drEach["booking_datetime"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					dtBookingDateTime = (DateTime)drEach["booking_datetime"];
					txtBookDate.Text = dtBookingDateTime.ToString("dd/MM/yyyy HH:mm");
				}
				

				String strPayerType = "";
				if(!drEach["payer_type"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					strPayerType = (String)drEach["payer_type"];
					ddbCustType.SelectedItem.Selected = false;
					ddbCustType.Items.FindByValue(strPayerType).Selected = true;
				}
				
				String strPayerZip = "";
				if(!drEach["payer_zipcode"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					strPayerZip = (String)drEach["payer_zipcode"];
				}
				txtCustZipCode.Text = strPayerZip;

				String strPayerCountry = "";
				if(!drEach["payer_country"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					strPayerCountry = (String)drEach["payer_country"];
				}
				txtCustStateCode.Text = strPayerCountry;

				
				String strPayerName = "";
				if(!drEach["payer_name"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					strPayerName = (String)drEach["payer_name"];
					txtCustName.Text = strPayerName;
				}
				else
				{
					txtCustName.Text = "";
				}
				


				String strPayerID = "";
				if(!drEach["payerid"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					strPayerID = (String)drEach["payerid"];
					txtCustID.Text = strPayerID;
				}
				else
				{
					txtCustID.Text = "";
				}
				

				String strPayerAdd1 = "";
				if(!drEach["payer_address1"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					strPayerAdd1 = (String)drEach["payer_address1"];
					txtCustAdd1.Text = strPayerAdd1;
				}
				else
				{
					txtCustAdd1.Text = "";
				}
				

				String strPayerAdd2 = "";
				if(!drEach["payer_address2"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					strPayerAdd2 = (String)drEach["payer_address2"];
					txtCustAdd2.Text = strPayerAdd2;
				}
				else
				{
					txtCustAdd2.Text = "";
				}
				

				String strPayerState = "";
				
				if(strPayerCountry.Equals(""))
				{
					enterprise = new Enterprise();
					enterprise.Populate(appID,enterpriseID);
					strPayerCountry = enterprise.Country;
				}
				zipCode = new Zipcode();
				zipCode.Populate(appID,enterpriseID,strPayerCountry,strPayerZip);
				strPayerState = zipCode.StateName;
				txtCustCity.Text = strPayerState;
				txtCustStateCode.Text = strPayerCountry;
				txtOrigin.Text = strPayerState;

				String strPayerTel = "";
				if(!drEach["payer_telephone"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					strPayerTel = (String)drEach["payer_telephone"];
					txtCustTelephone.Text = strPayerTel;
				}
				else
				{
					txtCustTelephone.Text = "";
				}
				

				String strPayerFax = "";
				if(!drEach["payer_Fax"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					strPayerFax = (String)drEach["payer_Fax"];
				}
				txtCustFax.Text = strPayerFax;

				String strSenderNm = "";
				if(!drEach["sender_name"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					strSenderNm = (String)drEach["sender_name"];
				}
				txtSendName.Text = strSenderNm;

				String strSenderAdd1 = "";
				if(!drEach["sender_address1"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					strSenderAdd1 = (String)drEach["sender_address1"];
				}
				txtSendAddr1.Text = strSenderAdd1;

				String strSenderAdd2 = "";
				if(!drEach["sender_address2"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					strSenderAdd2 = (String)drEach["sender_address2"];
				}
				txtSendAddr2.Text = strSenderAdd2;
				
				String strSenderZip = "";
				if(!drEach["sender_zipcode"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					strSenderZip = (String)drEach["sender_zipcode"];
				}
				txtSendZip.Text = strSenderZip;

				String strSendCountry = "";
				if(!drEach["sender_country"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					strSendCountry = (String)drEach["sender_country"];
				}
				txtSendState.Text = strSendCountry;
				
				String strSenderState = "";
				//call the method & get the state
				if(strSendCountry.Equals(""))
				{
					enterprise = new Enterprise();
					enterprise.Populate(appID,enterpriseID);
					strSendCountry = enterprise.Country;
				}
				zipCode = new Zipcode();
				zipCode.Populate(appID,enterpriseID,strSendCountry,strSenderZip);
				strSenderState = zipCode.StateName;
				txtSendCity.Text = strSenderState;
				txtSendState.Text = strSendCountry;
				txtDestination.Text = strSenderState;
				txtSendCuttOffTime.Text = zipCode.CuttOffTime.ToString();
			}
			else
			{
				ViewState["PickupBooking"] = "No";
				lblErrorMsg.Text = "The booking number doesn't exist.";
				return;
			}
		}
		

		private void chkCustInfo_CheckedChanged(object sender, System.EventArgs e)
		{
			if(chkNewCust.Checked == true)
			{
				rbCash.Checked = true;
				rbCredit.Checked = false;
				txtCustID.Enabled = false;
				btnDisplayCustDtls.Enabled = false;
				txtCustID.Text = "NEW";
				txtCustName.Text = "";
				txtCustAdd1.Text = "";
				txtCustAdd2.Text = "";
				txtCustStateCode.Text = "";
				txtCustZipCode.Text = "";
				txtCustTelephone.Text = "";
				txtCustFax.Text = "";
				ddbCustType.SelectedItem.Selected = false;
				ddbCustType.Items.FindByValue("C").Selected = true;

				//				DataSet dsCashCust = TIESUtility.GetCashCustomer(appID,enterpriseID);
				//				if(dsCashCust != null)
				//				{
				//					int i = 0;
				//					for(i = 0;i < dsCashCust.Tables[0].Rows.Count;i++)
				//					{
				//						DataRow drEach = dsCashCust.Tables[0].Rows[i];
				//
				//						if(!drEach["custid"].GetType().Equals(System.Type.GetType("System.DBNull")))
				//						{
				//							txtCustID.Text =  (String)drEach["custid"];
				//						}
				//						if(!drEach["cust_name"].GetType().Equals(System.Type.GetType("System.DBNull")))
				//						{
				//							txtCustName.Text =  (String)drEach["cust_name"];
				//						}
				//						if(!drEach["address1"].GetType().Equals(System.Type.GetType("System.DBNull")))
				//						{
				//							txtCustAdd1.Text =  (String)drEach["address1"];
				//						}
				//						if(!drEach["address2"].GetType().Equals(System.Type.GetType("System.DBNull")))
				//						{
				//							txtCustAdd2.Text =  (String)drEach["address2"];
				//						}
				//						if(!drEach["country"].GetType().Equals(System.Type.GetType("System.DBNull")))
				//						{
				//							txtCustStateCode.Text =  (String)drEach["country"];
				//						}
				//						if(!drEach["zipcode"].GetType().Equals(System.Type.GetType("System.DBNull")))
				//						{
				//							txtCustZipCode.Text =  (String)drEach["zipcode"];
				//						}
				//						if(!drEach["telephone"].GetType().Equals(System.Type.GetType("System.DBNull")))
				//						{
				//							txtCustTelephone.Text =  (String)drEach["telephone"];
				//						}
				//						if(!drEach["fax"].GetType().Equals(System.Type.GetType("System.DBNull")))
				//						{
				//							txtCustFax.Text =  (String)drEach["fax"];
				//						}
				//
				//						//Get the Country
				//						String strCustCountry = "";
				//						if(txtCustStateCode.Text.Trim().Length == 0)
				//						{
				//							Enterprise enterprise = new Enterprise();
				//							enterprise.Populate(appID,enterpriseID);
				//							strCustCountry = enterprise.Country;
				//							txtCustStateCode.Text = strCustCountry;
				//						}
				//						else
				//						{
				//							strCustCountry = txtCustStateCode.Text.Trim();
				//						}
				//
				//						//Get the state & display...
				//						Zipcode zipCode = new Zipcode();
				//						zipCode.Populate(appID,enterpriseID,strCustCountry,txtCustZipCode.Text.Trim());
				//						txtCustCity.Text = zipCode.StateName;
				//						
				//					}
				//					ddbCustType.SelectedItem.Selected = false;
				//					ddbCustType.Items.FindByValue("C").Selected = true;
				//				}
			}
			else
			{
				rbCash.Checked = false;
				rbCredit.Checked = true;
				txtCustID.Enabled = true;
				btnDisplayCustDtls.Enabled = true;
			}
		}

		private void chkSendCustInfo_CheckedChanged(object sender, System.EventArgs e)
		{
			//ViewState["isTextChanged"]=true;
			CopyCustInfoToSender();
		}

		private void chkRecip_CheckedChanged(object sender, System.EventArgs e)
		{
			CopyCustInfoToRecip();
		}

		/// <CopyCustInfoToSender>
		/// This method copies all the information from the Customer section to the Sender section
		/// </CopyCustInfoToSender>
		private void CopyCustInfoToSender()
		{
			if(chkSendCustInfo.Checked)
			{
				txtSendName.Text	= txtCustName.Text.Trim();
				txtSendAddr1.Text	= txtCustAdd1.Text.Trim();
				txtSendAddr2.Text	= txtCustAdd2.Text.Trim();
				txtSendZip.Text		= txtCustZipCode.Text.Trim();
				txtSendCity.Text	= txtCustCity.Text.Trim();
				txtSendState.Text	= txtCustStateCode.Text.Trim();

				Customer customer = new Customer();
				customer.Populate(appID,enterpriseID,txtCustID.Text.Trim());
				txtSendContPer.Text = customer.ContactPerson;
				//txtSendContPer.Text = txtCustID.Text.Trim();
				txtSendTel.Text		= txtCustTelephone.Text.Trim();
				txtSendFax.Text		= txtCustFax.Text.Trim();
				if((txtSendState.Text.Trim().Length>0) && (txtCustZipCode.Text.Trim().Length>0))
				{
					Zipcode zipCode = new Zipcode();
					zipCode.Populate(appID,enterpriseID,txtSendState.Text.Trim(),txtCustZipCode.Text.Trim());
					txtSendCity.Text = zipCode.StateName;
					txtSendCuttOffTime.Text = zipCode.CuttOffTime.ToString("HH:mm");
					txtOrigin.Text = txtSendCity.Text;
				}
			}
			else
			{
				txtSendName.Text	= "";
				txtSendAddr1.Text	= "";
				txtSendAddr2.Text	= "";
				txtSendZip.Text		= "";
				txtSendCity.Text	= "";
				txtSendState.Text	= "";
				txtSendContPer.Text = "";
				txtSendTel.Text		= "";
				txtSendFax.Text		= "";
				txtSendCuttOffTime.Text  = "";
				txtSendContPer.Text = "";
				txtOrigin.Text = "";
			}
		}

		/// <CopyCustInfoToRecip>
		/// This method copies all the information from the Customer section to the Recipient section
		/// </CopyCustInfoToRecip>
		private void CopyCustInfoToRecip()
		{
			if(chkRecip.Checked)
			{
				//txtRecName.Text		= txtCustName.Text.Trim();
				txtRecName.Text		= txtCustName.Text.Trim();
				txtRecipAddr1.Text	= txtCustAdd1.Text.Trim();
				txtRecipAddr2.Text	= txtCustAdd2.Text.Trim();
				txtRecipZip.Text	= txtCustZipCode.Text.Trim();
				txtRecipCity.Text	= txtCustCity.Text.Trim();
				txtRecipState.Text	= txtCustStateCode.Text.Trim();
				//txtRecpContPer.Text = txtCustID.Text.Trim();
				Customer customer = new Customer();
				customer.Populate(appID,enterpriseID,txtCustID.Text.Trim());
				txtSendContPer.Text = customer.ContactPerson;
				txtRecipTel.Text	= txtCustTelephone.Text.Trim();
				txtRecipFax.Text	= txtCustFax.Text.Trim();
				txtDestination.Text = txtRecipCity.Text;
			}
			else
			{
				txtRecName.Text		= "";
				txtRecipAddr1.Text	= "";
				txtRecipAddr2.Text	= "";
				txtRecipZip.Text	= "";
				txtRecipCity.Text	= "";
				txtRecipState.Text	= "";
				txtRecpContPer.Text = "";
				txtRecipTel.Text	= "";
				txtRecipFax.Text	= "";
				txtDestination.Text = "";
				txtRecpContPer.Text = "";
			}
		}

		private void txtCustZipCode_TextChanged(object sender, System.EventArgs e)
		{
			
			if((txtCustZipCode.Text.Length > 0) && ((int)ViewState["DSMode"] != (int)ScreenMode.ExecuteQuery) && ((int)ViewState["DSMode"]!=(int)ScreenMode.Query))
			{
				//Get the State & Country
				Enterprise enterprise = new Enterprise();
				enterprise.Populate(appID,enterpriseID);
				String strPayerCountry = enterprise.Country;
		
				Zipcode zipCode = new Zipcode();
				zipCode.Populate(appID,enterpriseID,strPayerCountry,txtCustZipCode.Text.Trim());
				txtCustCity.Text = zipCode.StateName;
				txtCustStateCode.Text = strPayerCountry;
			}
		}

		private void txtSendZip_TextChanged(object sender, System.EventArgs e)
		{
			if((txtSendZip.Text.Length > 0) && ((int)ViewState["DSMode"] != (int)ScreenMode.ExecuteQuery) && ((int)ViewState["DSMode"]!=(int)ScreenMode.Query))
			{
				//Get the State & Country
				Enterprise enterprise = new Enterprise();
				enterprise.Populate(appID,enterpriseID);
				String strPayerCountry = enterprise.Country;
		
				Zipcode zipCode = new Zipcode();
				zipCode.Populate(appID,enterpriseID,strPayerCountry,txtSendZip.Text.Trim());
				txtSendCity.Text = zipCode.StateName;
				txtSendState.Text = strPayerCountry;
				txtSendCuttOffTime.Text = zipCode.CuttOffTime.ToString();
				txtOrigin.Text = txtSendCity.Text;
			}
		}

		private void txtCustID_TextChanged(object sender, System.EventArgs e)
		{
			//ViewState["isTextChanged"]=true;

			if((txtCustID.Text.Trim().Length > 0) && (chkNewCust.Checked == false) && ((int)ViewState["DSMode"] != (int)ScreenMode.ExecuteQuery) && ((int)ViewState["DSMode"]!=(int)ScreenMode.Query))
			{
				Customer customer = new Customer();
				customer.Populate(appID,enterpriseID,txtCustID.Text.Trim());
				txtCustName.Text = customer.CustomerName;
				txtCustAdd1.Text = customer.CustomerAddress1;
				txtCustAdd2.Text = customer.CustomerAddress2;
				txtCustFax.Text = customer.CustomerFax;
				txtCustTelephone.Text = customer.CustomerTelephone;
				txtCustZipCode.Text = customer.ZipCode;
				
				//call the method to get the country & state code
				Enterprise enterprise = new Enterprise();
				enterprise.Populate(appID,enterpriseID);
				String strCountry = enterprise.Country;

				Zipcode zipCode = new Zipcode();
				zipCode.Populate(appID,enterpriseID,strCountry,txtCustZipCode.Text);
				String strState = zipCode.StateName;
				txtCustCity.Text = strState;
				txtCustStateCode.Text = strCountry;
			}
		}

		private void btnDisplayCustDtls_Click(object sender, System.EventArgs e)
		{
			strErrorMsg = "";
			if(ddbCustType.SelectedItem.Value == "0")
			{
				strErrorMsg = "Select the Customer type.";
			}
			else if(ddbCustType.SelectedItem.Value == "C")
			{
				String sUrl = "CustomerPopup.aspx?FORMID="+"DomesticShipment";
				String sFeatures = "'height=320;width=160;left=100;top=50;location=no;menubar=no;resizable=yes;scrollbars=no;status=no;titlebar=yes;toolbar=no'";
				String sScript ="";
				sScript += "<script language=javascript>";
				sScript += "window.open('" + sUrl + "',''," + sFeatures + ");";
				sScript += "</script>";
				Response.Write(sScript);
			}
			else if(ddbCustType.SelectedItem.Value == "A")
			{
				
				String sUrl = "AgentPopup.aspx?FORMID=DomesticShipment"+"&CUSTID_TEXT="+txtCustID.Text.Trim().ToString();
				String sFeatures = "'height=320;width=160;left=100;top=50;location=no;menubar=no;resizable=yes;scrollbars=no;status=no;titlebar=yes;toolbar=no'";
				String sScript ="";
				sScript += "<script language=javascript>";
				sScript += "window.open('" + sUrl + "',''," + sFeatures + ");";
				sScript += "</script>";
				Response.Write(sScript);

			}
			
			lblErrorMsg.Text = strErrorMsg;
		}

		private void btnPkgDetails_Click(object sender, System.EventArgs e)
		{

			if(((int)ViewState["DSMode"] == (int)ScreenMode.ExecuteQuery) && (txtBookingNo.Text.Trim().Length > 0))
			{
				DataSet dsQryPkg = null;

				try
				{
					//call the method for getting the package details during query
					dsQryPkg = DomesticShipmentMgrDAL.GetPakageDtls(appID,enterpriseID,Convert.ToInt32(txtBookingNo.Text.Trim()), txtConsigNo.Text.Trim());
				}
				catch(ApplicationException appException)
				{
					String strMsg = appException.Message;
					lblErrorMsg.Text = strMsg;
					return;
				}
				Session["SESSION_DS2"] = dsQryPkg;
			}

			String strApplyDimWt = null;
			String strApplyESA = null;

			if(ddbCustType.SelectedItem.Value == "C")
			{
				Customer customer = new Customer();
				customer.Populate(appID,enterpriseID,txtCustID.Text);
				strApplyDimWt = customer.ApplyDimWt;
				strApplyESA = customer.ESASurcharge;

			}
			else if(ddbCustType.SelectedItem.Value == "A")
			{
				Agent agent = new Agent();
				agent.Populate(appID,enterpriseID,txtCustID.Text);
				strApplyDimWt = agent.ApplyDimWt;
				strApplyESA = agent.ESASurcharge;
			}
			if(strApplyESA == "Y")
			{
				bool isHasCustZone = true;
				decimal surchargeOfSender = 0;
				decimal surchargeOfRecip = 0;
				decimal decSrchrg = 0;

				//***************** Get surcharge from Customer Zone by Sender Zipcode *************************
				DataSet tmpCustZone = SysDataMgrDAL.GetCustZoneData(appID, enterpriseID,
					0, 0, txtCustID.Text.Trim(), txtSendZip.Text.Trim()).ds;
				if(tmpCustZone.Tables[0].Rows.Count > 0)
				{
					if((tmpCustZone.Tables[0].Rows[0]["extended_area_surcharge_amt"] != null) && 
						(!tmpCustZone.Tables[0].Rows[0]["extended_area_surcharge_amt"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
						(tmpCustZone.Tables[0].Rows[0]["extended_area_surcharge_amt"].ToString() != "") &&
						(tmpCustZone.Tables[0].Rows[0]["extended_area_surcharge_percent"] != null) && 
						(!tmpCustZone.Tables[0].Rows[0]["extended_area_surcharge_percent"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
						(tmpCustZone.Tables[0].Rows[0]["extended_area_surcharge_percent"].ToString() != "")
						)
					{
						if((tmpCustZone.Tables[0].Rows[0]["extended_area_surcharge_amt"] != null) && 
							(!tmpCustZone.Tables[0].Rows[0]["extended_area_surcharge_amt"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
							(tmpCustZone.Tables[0].Rows[0]["extended_area_surcharge_amt"].ToString() != ""))
						{
							surchargeOfSender = (decimal)tmpCustZone.Tables[0].Rows[0]["extended_area_surcharge_amt"];
						}

						if((tmpCustZone.Tables[0].Rows[0]["extended_area_surcharge_percent"] != null) && 
							(!tmpCustZone.Tables[0].Rows[0]["extended_area_surcharge_percent"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
							(tmpCustZone.Tables[0].Rows[0]["extended_area_surcharge_percent"].ToString() != ""))
						{
							surchargeOfSender = (Convert.ToDecimal(txtFreightChrg.Text.Trim()) * ((decimal)tmpCustZone.Tables[0].Rows[0]["extended_area_surcharge_percent"])) / 100;
						}
					}
					else
					{	
						isHasCustZone = false;
					}
				}
				else
				{
					isHasCustZone = false;
				}
				//***************** Get surcharge from Customer Zone by Sender Zipcode *************************

				//***************** Get surcharge from Customer Zone by Recept Zipcode *************************
				tmpCustZone = SysDataMgrDAL.GetCustZoneData(appID, enterpriseID,
					0, 0, txtCustID.Text.Trim(), txtRecipZip.Text.Trim()).ds;
				if(tmpCustZone.Tables[0].Rows.Count > 0)
				{
					if((tmpCustZone.Tables[0].Rows[0]["extended_area_surcharge_amt"] != null) && 
						(!tmpCustZone.Tables[0].Rows[0]["extended_area_surcharge_amt"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
						(tmpCustZone.Tables[0].Rows[0]["extended_area_surcharge_amt"].ToString() != "") &&
						(tmpCustZone.Tables[0].Rows[0]["extended_area_surcharge_percent"] != null) && 
						(!tmpCustZone.Tables[0].Rows[0]["extended_area_surcharge_percent"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
						(tmpCustZone.Tables[0].Rows[0]["extended_area_surcharge_percent"].ToString() != "")
						)
					{
						if((tmpCustZone.Tables[0].Rows[0]["extended_area_surcharge_amt"] != null) && 
							(!tmpCustZone.Tables[0].Rows[0]["extended_area_surcharge_amt"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
							(tmpCustZone.Tables[0].Rows[0]["extended_area_surcharge_amt"].ToString() != ""))
						{
							surchargeOfRecip = (decimal)tmpCustZone.Tables[0].Rows[0]["extended_area_surcharge_amt"];
						}

						if((tmpCustZone.Tables[0].Rows[0]["extended_area_surcharge_percent"] != null) && 
							(!tmpCustZone.Tables[0].Rows[0]["extended_area_surcharge_percent"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
							(tmpCustZone.Tables[0].Rows[0]["extended_area_surcharge_percent"].ToString() != ""))
						{
							surchargeOfRecip = (Convert.ToDecimal(txtFreightChrg.Text.Trim()) * ((decimal)tmpCustZone.Tables[0].Rows[0]["extended_area_surcharge_percent"])) / 100;
						}
					}
					else
					{	
						isHasCustZone = false;
					}
				}
				else
				{
					isHasCustZone = false;
				}
				//***************** Get surcharge from Customer Zone by Recept Zipcode *************************

				if (isHasCustZone)
				{
					decSrchrg = surchargeOfSender + surchargeOfRecip;
				}
				else
				{
					Zipcode zipcode = new Zipcode();
					zipcode.Populate(appID, enterpriseID, txtSendZip.Text.Trim());
					decSrchrg = zipcode.EASSurcharge;

					zipcode.Populate(appID, enterpriseID, txtRecipZip.Text.Trim());
					decSrchrg = decSrchrg + zipcode.EASSurcharge;
				}

				txtESASurchrg.Text = String.Format((String)ViewState["m_format"], decSrchrg);
			}	
			else
			{
				txtESASurchrg.Text = String.Format((String)ViewState["m_format"], 0);
			}

			String sUrl = "PkgDetailsPopup.aspx?ApplyDimWt="+strApplyDimWt+"&BTNACTIVE="+(String)ViewState["PickupBooking"];
			String sFeatures = "'height=320;width=160;left=100;top=50;location=no;menubar=no;resizable=yes;scrollbars=no;status=no;titlebar=yes;toolbar=no'";
			String sScript ="";
			sScript += "<script language=javascript>";
			sScript += "window.open('" + sUrl + "',''," + sFeatures + ");";
			sScript += "</script>";
			Response.Write(sScript);
		}

		private void btnPkgCommCode_Click(object sender, System.EventArgs e)
		{
		
		}

		private void txtShpDclrValue_TextChanged(object sender, System.EventArgs e)
		{
			String strItem = ddbCustType.SelectedItem.Value;
			String strWeight = txtPkgChargeWt.Text;
			decimal decWeight = 0;
			lblErrorMsg.Text = "";

			if (strWeight.Length > 0)
			{
				decWeight = Convert.ToDecimal(strWeight);
			}

			if(strItem.Equals("0"))
			{
				strErrorMsg = "Please select the Customer Type";
				txtShpDclrValue.Text = "";
			}
			else if(decWeight == 0)
			{
				strErrorMsg = "The chargeable weight not found";
				txtShpDclrValue.Text = "";
			}
			else if(txtSendZip.Text.Trim().Length == 0)
			{
				strErrorMsg = "Please enter the sender zip code";
				txtShpDclrValue.Text = "";
			}
			else if(txtRecipZip.Text.Trim().Length == 0)
			{
				strErrorMsg = "Please enter the recipient zip code";
				txtShpDclrValue.Text = "";
			}
			else if(txtShpSvcCode.Text.Trim().Length == 0)
			{
				strErrorMsg = "Please enter the Service Code";
				txtShpDclrValue.Text = "";
			}
			else if(txtCustID.Text.Trim().Length == 0)
			{
				strErrorMsg = "Please enter the Customer ID";
				txtShpDclrValue.Text = "";
			}
			else if(txtShipPckUpTime.Text.Trim().Length == 0)
			{
				strErrorMsg = "Please enter the PickUp Date Time";
				txtShpDclrValue.Text = "";
			}
			else if((txtShpDclrValue.Text.Trim().Length > 0))
			{
				//calculate the estimated date time
				domesticMgrBAL = new DomesticShipmentMgrBAL();

				txtShipEstDlvryDt.Text =  domesticMgrBAL.CalcDlvryDtTime(appID,enterpriseID,System.DateTime.ParseExact(txtShipPckUpTime.Text,"dd/MM/yyyy HH:mm",null),txtShpSvcCode.Text,txtRecipZip.Text).ToString();
				
				Enterprise enterprise = new Enterprise();
				enterprise.Populate(appID,enterpriseID);

				txtShpMaxCvrg.Text = String.Format((String)ViewState["m_format"], enterprise.MaxInsuranceAmt);

				decimal decFreeInsAmt = 0;
				decFreeInsAmt = enterprise.FreeInsuranceAmount;

				if(Convert.ToDecimal(txtShpDclrValue.Text) > (enterprise.MaxInsuranceAmt))
				{
					txtShpAddDV.Text = enterprise.InsPercSurchrg.ToString("#0.00");
				}
				else
				{
					txtShpAddDV.Text = "0";
				}


				Customer customer = new Customer();
				customer.Populate(appID,enterpriseID, txtCustID.Text.Trim());

				DomesticShipmentMgrBAL domesticBAL = new DomesticShipmentMgrBAL();
				//Compute the insurance surcharge
				//decimal decInsSurchrg = domesticBAL.ComputeInsSurcharge(Convert.ToDecimal(txtShpDclrValue.Text.Trim()),Convert.ToDecimal(txtShpAddDV.Text.Trim()),Convert.ToDecimal(txtShpMaxCvrg.Text.Trim()),decFreeInsAmt);
				decimal decInsSurchrg = domesticBAL.ComputeInsSurchargeByCust(appID, enterpriseID,
					Convert.ToDecimal(txtShpDclrValue.Text.Trim()), txtCustID.Text.Trim(), customer);

				txtShpInsSurchrg.Text = String.Format((String)ViewState["m_format"], decInsSurchrg);

				//Display value in Insurance Surcharge Field also
				txtInsChrg.Text = String.Format((String)ViewState["m_format"], decInsSurchrg);

				CalculateTotalAmt();

				Zipcode zipcode = new Zipcode();
				//Get the Origin Zone Code
				zipcode.Populate(appID,enterpriseID,txtSendZip.Text);
				String strOrgZone = zipcode.ZoneCode;

				//Get the desctination Zone Code
				zipcode.Populate(appID,enterpriseID,txtRecipZip.Text);
				String strDestnZone = zipcode.ZoneCode;
				float fCalcFrgtChrg = 0;

				try
				{
					//Compute the Freight Charge	
					fCalcFrgtChrg = TIESUtility.ComputeFreightCharge(appID,enterpriseID,txtCustID.Text.Trim(),ddbCustType.SelectedItem.Value,strOrgZone,strDestnZone,
						(float)Convert.ToDecimal(txtPkgChargeWt.Text),txtShpSvcCode.Text,
						txtSendZip.Text.Trim(), txtRecipZip.Text.Trim());
					txtFreightChrg.Text = String.Format((String)ViewState["m_format"], fCalcFrgtChrg);
				}
				catch(ApplicationException appException)
				{
					String strMsg = appException.Message;
					lblErrorMsg.Text = strMsg;
					return;
				}

				CalculateTotalAmt();
			}
			
			lblErrorMsg.Text = strErrorMsg;
		}
	
		private void CalculateTotalAmt()
		{
			//Calculate the total Amount
			decimal decInsuranceChrg = 0;
			if(txtInsChrg.Text.Trim().Length > 0)
			{
				decInsuranceChrg = Convert.ToDecimal(txtInsChrg.Text.Trim());
			}

			decimal decFrghtChrg = 0;
			if (txtFreightChrg.Text.Trim().Length > 0)
			{
				decFrghtChrg = Convert.ToDecimal(txtFreightChrg.Text.Trim());
			}

			decimal decTotVASSurcharge = 0;
			if(txtTotVASSurChrg.Text.Trim().Length > 0)
			{
				decTotVASSurcharge = Convert.ToDecimal(txtTotVASSurChrg.Text.Trim());
			}

			decimal decESASSurcharge = 0;
			if(txtESASurchrg.Text.Length > 0)
			{
				decESASSurcharge = Convert.ToDecimal(txtESASurchrg.Text);
			}

			decimal decTotalAmt = decTotVASSurcharge + decFrghtChrg + decInsuranceChrg + decESASSurcharge;	
			DataSet profileDS = SysDataManager1.GetEnterpriseProfile(appID, enterpriseID);
			int currency_decimal = (int) profileDS.Tables[0].Rows[0]["currency_decimal"];
			String format = "{0:F" + currency_decimal.ToString() + "}";
			txtShpTotAmt.Text = String.Format(format, decTotalAmt);
		}

		private void rbCash_CheckedChanged(object sender, System.EventArgs e)
		{	
			if((bool)ViewState["isTextChanged"] == false)
			{
				ChangeDSState();
				ViewState["isTextChanged"] = true;
			}

			if (((int)ViewState["DSMode"] != (int)ScreenMode.ExecuteQuery ) && ((int)ViewState["DSMode"]!=(int)ScreenMode.Query))
			{
				if(rbCash.Checked == true)
				{
					rbtnShipFrghtPre.Enabled = true;
					rbtnShipFrghtColl.Enabled = true;
				}
				else
				{
					rbtnShipFrghtPre.Enabled = false;
					rbtnShipFrghtColl.Enabled = false;
				}
			}
		}

		private void rbCredit_CheckedChanged(object sender, System.EventArgs e)
		{
			if((bool)ViewState["isTextChanged"] == false)
			{
				ChangeDSState();
				ViewState["isTextChanged"] = true;
			}

			if((rbCredit.Checked == true) && ((int)ViewState["DSMode"] != (int)ScreenMode.ExecuteQuery) && ((int)ViewState["DSMode"]!=(int)ScreenMode.Query))
			{
				rbtnShipFrghtPre.Enabled = false;
				rbtnShipFrghtColl.Enabled = false;
			}
		}

		private void dgVAS_SelectedIndexChanged(object sender, System.EventArgs e)
		{
		}
		protected void dgVAS_Edit(object sender, DataGridCommandEventArgs e)
		{
			/*dgVAS.EditItemIndex = e.Item.ItemIndex;
			BindVASGrid();*/
		}

		protected void dgVAS_Cancel(object sender, DataGridCommandEventArgs e)
		{
			dgVAS.EditItemIndex = -1;
			BindVASGrid();
		}

		protected void dgVAS_Delete(object sender, DataGridCommandEventArgs e)
		{
			/*if((bool)ViewState["isTextChanged"] == false)
			{
				ChangeDSState();
				ViewState["isTextChanged"] = true;
			}

			DataRow drCurrent = m_dsVAS.Tables[0].Rows[e.Item.ItemIndex];
			drCurrent.Delete();
			BindVASGrid();
			GetTotalSurcharge();*/
		}

		protected void dgVAS_PageChange(object sender, DataGridPageChangedEventArgs e)
		{
			dgVAS.CurrentPageIndex = e.NewPageIndex;
			BindVASGrid();
		}
		public void dgVAS_Update(object sender, DataGridCommandEventArgs e)
		{
			if((bool)ViewState["isTextChanged"] == false)
			{
				ChangeDSState();
				ViewState["isTextChanged"] = true;
			}

			lblErrorMsg.Text = "";
			String strChkVasCode = null;
			TextBox txtdgVASCode = (TextBox)e.Item.FindControl("txtVASCode");
			if(txtdgVASCode != null)
			{
				VAS vas = new VAS();
				vas.Populate(appID,enterpriseID,txtdgVASCode.Text.Trim());
				strChkVasCode = vas.VASCode;
				if(strChkVasCode != null)
				{
					int cnt = m_dsVAS.Tables[0].Rows.Count;
					int i = 0;
					String strDSVASCode = "";

					for(i = 0;i<cnt;i++)
					{
						DataRow drEach = m_dsVAS.Tables[0].Rows[i];
						strDSVASCode = (String)drEach["vas_code"];
						if(strDSVASCode.Equals(strChkVasCode))
						{
							lblErrorMsg.Text = "VAS Code already exists";
							dgVAS.SelectedItemStyle.ForeColor = System.Drawing.Color.Red;
							//break;
							return;
						}
					}
				}
				else
				{
					lblErrorMsg.Text = "VAS Code doesn't exists";
					return;
				}
			}
			
			//			if(strErrorMsg.Equals(""))
			//			{
			TextBox txtSurcharge = (TextBox)e.Item.FindControl("txtSurcharge");
			if(txtSurcharge != null)
			{
				DataRow drCurrent = m_dsVAS.Tables[0].Rows[e.Item.ItemIndex];
				drCurrent["surcharge"] = Convert.ToDecimal(txtSurcharge.Text);
			}

			TextBox txtRemarks = (TextBox)e.Item.FindControl("txtRemarks");
			if(txtRemarks != null)
			{
				DataRow drCurrent = m_dsVAS.Tables[0].Rows[e.Item.ItemIndex];
				drCurrent["remarks"] = txtRemarks.Text;
			}
			
			TextBox txtVASCode = (TextBox)e.Item.FindControl("txtVASCode");
			if(txtVASCode != null)
			{
				DataRow drCurrent = m_dsVAS.Tables[0].Rows[e.Item.ItemIndex];
				drCurrent["vas_code"] = txtVASCode.Text;
			}

			TextBox txtVASDesc = (TextBox)e.Item.FindControl("txtVASDesc");
			if(txtVASDesc != null)
			{
				DataRow drCurrent = m_dsVAS.Tables[0].Rows[e.Item.ItemIndex];
				drCurrent["vas_description"] = txtVASDesc.Text;
			}
			
			decimal totSurcharge = GetTotalSurcharge();
			txtTotVASSurChrg.Text = String.Format((String)ViewState["m_format"], totSurcharge);
			CalculateTotalAmt();

			Session["SESSION_DS1"] = m_dsVAS;
			dgVAS.EditItemIndex = -1;
			BindVASGrid();
			//}
			//lblErrorMsg.Text = strErrorMsg;
		}

		public void BindVASGrid()
		{

			dgVAS.DataSource = m_dsVAS;
			dgVAS.DataBind();
			
			Session["SESSION_DS1"] = m_dsVAS;

		}

		private void btnDGInsert_Click(object sender, System.EventArgs e)
		{
			ViewState["Operations"] = Operation.Insert;
			AddRowInVASGrid();	

			dgVAS.EditItemIndex = m_dsVAS.Tables[0].Rows.Count - 1;
			BindVASGrid();
		}

		private void AddRowInVASGrid()
		{
			DomesticShipmentMgrDAL.AddNewRowInVAS(m_dsVAS);
			Session["SESSION_DS1"] = m_dsVAS;
		}

		public void dgVAS_Button(object sender, DataGridCommandEventArgs e)
		{
			String strCmdNm = e.CommandName;
			if(strCmdNm.Equals("Search"))
			{
				TextBox txtVASCode = (TextBox)e.Item.FindControl("txtVASCode");
				String strtxtVASCode = null;
				if(txtVASCode != null)
				{
					strtxtVASCode = txtVASCode.ClientID;
				}
				
				TextBox txtVASDesc = (TextBox)e.Item.FindControl("txtVASDesc");
				String strtxtVASDesc = null;
				if (txtVASDesc != null)
				{
					strtxtVASDesc = txtVASDesc.ClientID;
				}

				TextBox txtSurcharge = (TextBox)e.Item.FindControl("txtSurcharge");
				String strtxtSurcharge = null;
				if (txtSurcharge != null)
				{
					strtxtSurcharge = txtSurcharge.ClientID;
				}

				String sUrl = "VASPopup.aspx?VASID="+strtxtVASCode+"&VASDESC="+strtxtVASDesc+"&VASSURCHARGE="+strtxtSurcharge+"&FORMID="+"DomesticShipment";
				String sFeatures = "'height=320;width=160;left=100;top=50;location=no;menubar=no;resizable=yes;scrollbars=no;status=no;titlebar=yes;toolbar=no'";
				String sScript ="";
				sScript += "<script language=javascript>";
				sScript += "window.open('" + sUrl + "',''," + sFeatures + ");";
				sScript += "</script>";
				Response.Write(sScript);
			}
			
		}

		public void dgVAS_ItemDataBound(object sender, DataGridItemEventArgs e)
		{

			//			TextBox txtVASDesc = (TextBox)e.Item.FindControl("txtVASDesc");
			//			if(txtVASDesc != null)
			//			{
			//				txtVASDesc.Enabled = false;
			//			}
			//
			if(e.Item.ItemType == ListItemType.EditItem)
			{
				e.Item.Cells[3].Enabled = true;	
			}
			else
			{
				e.Item.Cells[3].Enabled = false;
			}
		}

		private void ddbCustType_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if((bool)ViewState["isTextChanged"] == false)
			{
				ChangeDSState();
				ViewState["isTextChanged"] = true;
			}
		}
		private decimal GetTotalSurcharge()
		{
			decimal decTotSurchrg = 0;
			if(m_dsVAS != null)
			{
			
				int cnt = m_dsVAS.Tables[0].Rows.Count;
				int i = 0;
				

				for(i = 0;i<cnt;i++)
				{
					DataRow drEach = m_dsVAS.Tables[0].Rows[i];

					String vas_code = "";
					if((drEach["vas_code"] != null) && 
						(!drEach["vas_code"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
						(drEach["vas_code"].ToString() != ""))
					{
						vas_code = drEach["vas_code"].ToString().Trim();
					}

					if (vas_code == "COD")
					{
						DataSet dscustInfo = SysDataMgrDAL.GetCustInforByCustID(appID,enterpriseID, 
							0, 0, txtCustID.Text.ToString().Trim()).ds;

						if(dscustInfo.Tables[0].Rows.Count > 0)
						{
							if(((dscustInfo.Tables[0].Rows[0]["cod_surcharge_amt"] != null) && 
								(!dscustInfo.Tables[0].Rows[0]["cod_surcharge_amt"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
								(dscustInfo.Tables[0].Rows[0]["cod_surcharge_amt"].ToString() != "")) ||
								((dscustInfo.Tables[0].Rows[0]["cod_surcharge_percent"] != null) && 
								(!dscustInfo.Tables[0].Rows[0]["cod_surcharge_percent"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
								(dscustInfo.Tables[0].Rows[0]["cod_surcharge_percent"].ToString() != "")))
							{
								if((dscustInfo.Tables[0].Rows[0]["cod_surcharge_amt"] != null) && 
									(!dscustInfo.Tables[0].Rows[0]["cod_surcharge_amt"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
									(dscustInfo.Tables[0].Rows[0]["cod_surcharge_amt"].ToString() != ""))
								{
									decTotSurchrg += (decimal)dscustInfo.Tables[0].Rows[0]["cod_surcharge_amt"];
								}

								if((dscustInfo.Tables[0].Rows[0]["cod_surcharge_percent"] != null) && 
									(!dscustInfo.Tables[0].Rows[0]["cod_surcharge_percent"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
									(dscustInfo.Tables[0].Rows[0]["cod_surcharge_percent"].ToString() != ""))
								{
									decTotSurchrg += (Convert.ToDecimal(txtCODAmount.Text) * ((decimal)dscustInfo.Tables[0].Rows[0]["cod_surcharge_percent"])) / 100;
								}
							}
							else
							{
								if(!drEach["surcharge"].GetType().Equals(System.Type.GetType("System.DBNull")))
									decTotSurchrg += Convert.ToDecimal(drEach["surcharge"]);
							}
						}
						else
						{
							if(!drEach["surcharge"].GetType().Equals(System.Type.GetType("System.DBNull")))
								decTotSurchrg += Convert.ToDecimal(drEach["surcharge"]);
						}
					}
					else
					{
						if(!drEach["surcharge"].GetType().Equals(System.Type.GetType("System.DBNull")))
							decTotSurchrg += Convert.ToDecimal(drEach["surcharge"]);
					}
				}
			}
			return decTotSurchrg;
		}

		private void btnRouteCode_Click(object sender, System.EventArgs e)
		{
			
		}

		private void btnShpSvcCode_Click(object sender, System.EventArgs e)
		{
			strErrorMsg = "";

			if(txtRecipZip.Text.Trim().Length > 0)
			{
				//Call the popup window.
				String sUrl = "ServiceCodePopup.aspx?PickUpDtTime="+txtShipPckUpTime.Text.Trim()+"&DestZipCode="+txtRecipZip.Text.Trim();
				String sFeatures = "'height=320;width=160;left=100;top=50;location=no;menubar=no;resizable=yes;scrollbars=no;status=no;titlebar=yes;toolbar=no'";
				String sScript ="";
				sScript += "<script language=javascript>";
				sScript += "window.open('" + sUrl + "',''," + sFeatures + ");";
				sScript += "</script>";
				Response.Write(sScript);
			}
			else
			{
				strErrorMsg = "Please enter the Recipient ZipCode";
			}
			lblErrorMsg.Text = strErrorMsg;
			
		}

		private void txtShpSvcCode_TextChanged(object sender, System.EventArgs e)
		{
			if((bool)ViewState["isTextChanged"] == false)
			{
				ChangeDSState();
				ViewState["isTextChanged"] = true;
			}

			strErrorMsg = "";
			//Call the calculation of Estimated Delivery date method
			if((txtShpSvcCode.Text.Trim().Length > 0) && ((int)ViewState["DSMode"] != (int)ScreenMode.ExecuteQuery) && ((int)ViewState["DSMode"]!=(int)ScreenMode.Query))
			{
				Service service = new Service();
				service.Populate(appID,enterpriseID,txtShpSvcCode.Text);
				String strCode = service.ServiceCode;
				String strDesc = service.ServiceDescription;
				txtShpSvcDesc.Text = strDesc;

				if(strCode == null)
				{
					lblErrorMsg.Text = "Service Code doesn't exists...";
					txtShpSvcCode.Text = "";
					return;
				}
				bool IsServiceAvail = false;
				try
				{
					//Check whether the service is provided for the destination zip code
					IsServiceAvail = TIESUtility.IsZipCodeServiceAvailable(appID,enterpriseID,txtShpSvcCode.Text.Trim(),txtRecipZip.Text.Trim());
				}
				catch(ApplicationException appException)
				{
					lblErrorMsg.Text = appException.Message;
					return;
				}


				if(IsServiceAvail == true)
				{
					if(txtShipPckUpTime.Text.Trim().Length > 0)
					{
						domesticMgrBAL = new DomesticShipmentMgrBAL();
						txtShipEstDlvryDt.Text = domesticMgrBAL.CalcDlvryDtTime(appID,enterpriseID,System.DateTime.ParseExact(txtShipPckUpTime.Text,"dd/MM/yyyy HH:mm",null),txtShpSvcCode.Text,txtRecipZip.Text).ToString();
					}
				}
				else if(IsServiceAvail == false)
				{
					strErrorMsg = "Service type is not available";
					txtShipEstDlvryDt.Text = "";
				}
			}
			lblErrorMsg.Text = strErrorMsg;
		}

		private void txtShipPckUpTime_TextChanged(object sender, System.EventArgs e)
		{
			if((bool)ViewState["isTextChanged"] == false)
			{
				ChangeDSState();
				ViewState["isTextChanged"] = true;
			}
			//Call the calculation of Estimated Delivery date method
			if((txtShipPckUpTime.Text.Trim().Length > 0) && ((int)ViewState["DSMode"] != (int)ScreenMode.ExecuteQuery) && ((int)ViewState["DSMode"]!=(int)ScreenMode.Query))
			{
				txtShipEstDlvryDt.Text = domesticMgrBAL.CalcDlvryDtTime(appID,enterpriseID,System.DateTime.ParseExact(txtShipPckUpTime.Text,"dd/MM/yyyy HH:mm",null),txtShpSvcCode.Text,txtRecipZip.Text).ToString();
			}
			DatePkUpDlvryTextChange();
		}

		private void DatePkUpDlvryTextChange()
		{
			ServiceAvailable serviceAvailable =  CheckDayServiceAvail();
			if((serviceAvailable.isServiceAvail == true) && ((String)ViewState["AddSurcharge"] == "none"))
			{
				ViewState["ServiceVASCode"] = serviceAvailable.strVASCode;
				//display the popup msg
				strErrorMsg = "Weekend / Public Holiday Delivery Service is available. Weekend / Public Holiday Surcharge";
				lblConfirmMsg.Text = "Weekend / Public Holiday Delivery Service is available. Weekend / Public Holiday Surcharge will be imposed. Do you want to continue?";
				DomstcShipPanel.Visible = true;
				ddbCustType.Visible = false;
				ViewState["AddSurcharge"] = "asking";
			}

			//if no service available then get the next working day...
			if((bool)ViewState["ServiceExists"] == false && (((String)ViewState["Day"] == "WeekEnd" ||((String)ViewState["Day"] == "Holiday"))))
			{
				ReCalculateDlvryDt();
			}
		}

		private String GetGridVasCode()
		{
			String strVasCode = "";
			int i = 0;

			for(i=0;i<dgVAS.Items.Count;i++)
			{
				Label lblVASCode = (Label)dgVAS.Items[i].FindControl("lblVASCode");
				if(lblVASCode != null)
				{
					if(i == dgVAS.Items.Count - 1)
					{
						strVasCode = strVasCode+lblVASCode.Text;
					}
					else
					{
						strVasCode = strVasCode+lblVASCode.Text+":";
					}
				}
			}
			return strVasCode;
		}
		private void btnPopulateVAS_Click(object sender, System.EventArgs e)
		{
			QuotationData quotationData;
			quotationData.iQuotationVersion = 0;
			quotationData.strQuotationNo = null;
			DataSet dsPopulateVAS = null;
			strErrorMsg = "";

			if((ddbCustType.SelectedItem.Value != "0") && (txtCustID.Text.Trim().Length > 0))
			{
				if(ddbCustType.SelectedItem.Value == "C")
				{
					try
					{
						quotationData = TIESUtility.CustomerQtnActive(appID,enterpriseID,txtCustID.Text.Trim());		
					}
					catch(ApplicationException appException)
					{
						lblErrorMsg.Text = appException.Message;
						return;
					}
				}
				else if(ddbCustType.SelectedItem.Value == "A")
				{
					try
					{
						quotationData = TIESUtility.AgentQtnActive(appID,enterpriseID,txtCustID.Text.Trim());
					}
					catch(ApplicationException appException)
					{
						lblErrorMsg.Text = appException.Message.ToString();
						return;
					}
				}
				
				//Get VAS for the active quotation
				if(quotationData.strQuotationNo != null)
				{
					String strVASList = GetGridVasCode();
					try
					{
						dsPopulateVAS = QuotationVAS.GetQuotationVAS(appID,enterpriseID,txtCustID.Text.Trim(),quotationData.strQuotationNo,quotationData.iQuotationVersion,strVASList,ddbCustType.SelectedItem.Value,txtRecipZip.Text);
					}
					catch(ApplicationException appException)
					{
						lblErrorMsg.Text = appException.Message;
					}
					dsPopulateVAS.Tables[0].Columns.Add(new DataColumn("isSelected", typeof(bool)));
					int cnt = dsPopulateVAS.Tables[0].Rows.Count;
					int i = 0;
					
					for(i=0;i<cnt;i++)
					{
						DataRow drCurrent = dsPopulateVAS.Tables[0].Rows[i];
						drCurrent["isSelected"] = false;
					}
					Session["SESSION_DS4"] = dsPopulateVAS;
					//Call the popup window.
					String sUrl = "QuotationVASPopup.aspx";
					String sFeatures = "'height=320;width=160;left=100;top=50;location=no;menubar=no;resizable=yes;scrollbars=no;status=no;titlebar=yes;toolbar=no'";
					String sScript ="";
					sScript += "<script language=javascript>";
					sScript += "window.open('" + sUrl + "',''," + sFeatures + ");";
					sScript += "</script>";
					Response.Write(sScript);
				}
				else
				{
					strErrorMsg = "There is no active Quotation.";
				}
			}
			else if((ddbCustType.SelectedItem.Value == "0") || (txtCustID.Text.Trim().Length == 0))
			{
				strErrorMsg = "Please select the customer Type/Customer ID";
			}
			lblErrorMsg.Text = strErrorMsg;
		}

		private void btnBind_Click(object sender, System.EventArgs e)
		{
			//Calculate the surcharge
			GetTotalSurcharge();

			BindVASGrid();		
		}

		private void txtRecipZip_TextChanged(object sender, System.EventArgs e)
		{
			if((bool)ViewState["isTextChanged"] == false)
			{
				ChangeDSState();
				ViewState["isTextChanged"] = true;
			}

			if (((int)ViewState["DSMode"] != (int)ScreenMode.ExecuteQuery) && ((int)ViewState["DSMode"]!=(int)ScreenMode.Query))
			{
				//Get the State & Country
				Enterprise enterprise = new Enterprise();
				enterprise.Populate(appID,enterpriseID);
				String strPayerCountry = enterprise.Country;
		
				Zipcode zipCode = new Zipcode();
				zipCode.Populate(appID,enterpriseID,strPayerCountry,txtRecipZip.Text.Trim());
				txtRecipCity.Text = zipCode.StateName;
				txtRecipState.Text = strPayerCountry;
				txtDestination.Text = txtRecipCity.Text;
				txtRouteCode.Text = zipCode.DeliveryRoute;
			}
		}

		private void btnToSaveChanges_Click(object sender, System.EventArgs e)
		{
			ViewState["isTextChanged"] = false;

			//Call the save/Update method.
			DomstcShipPanel.Visible = false;	
			ddbCustType.Visible = true;
			if ((String)ViewState["AddSurcharge"] == "asking")
			{
				//Call the surcharge calculation method
				if((String)ViewState["ServiceVASCode"] != "")
				{
					CalcAdditionalSurchrg((String)ViewState["ServiceVASCode"]);
					ViewState["AddSurcharge"] = "updated";
				}
			}
			//Call the save/Update method
			SaveUpdateRecord();
		}

		private void ReCalculateDlvryDt()
		{
			ViewState["AddSurcharge"] = "none";

			DateTime dtDlvryDt = System.DateTime.ParseExact(txtShipEstDlvryDt.Text.Trim(),"dd/MM/yyyy HH:mm",null);

			//Calculate the next date 
			dtDlvryDt = dtDlvryDt.AddDays(1);

			txtShipEstDlvryDt.Text = dtDlvryDt.ToString("dd/MM/yyyy HH:mm");
			CheckTheCalcDay();
			
		}

		private void CheckTheCalcDay()
		{
			//Check whether the calculated day has service
			ServiceAvailable serviceAvailable =  CheckDayServiceAvail();

			//Weekend / Holiday
			if(((String)ViewState["Day"] == "WeekEnd") || ((String)ViewState["Day"] == "Holiday"))
			{
				//Check service for weekend/holiday
				if((serviceAvailable.isServiceAvail == true) && ((String)ViewState["AddSurcharge"] == "none"))
				{
					ViewState["ServiceVASCode"] = serviceAvailable.strVASCode;
					//display the popup msg if service available for weekend/holiday
					strErrorMsg = "Weekend / Public Holiday Delivery Service is available. Weekend / Public Holiday Surcharge";
					lblConfirmMsg.Text = "Weekend / Public Holiday Delivery Service is available. Weekend / Public Holiday Surcharge will be imposed. Do you want to continue?";
					DomstcShipPanel.Visible = true;
					ddbCustType.Visible = false;
					ViewState["AddSurcharge"] = "asking";
				}
				else
				{
					//No service available weekend/Holiday so add 1 day & again check whether weekend/holiday.
					DateTime dtDlvryDt = System.DateTime.ParseExact(txtShipEstDlvryDt.Text.Trim(),"dd/MM/yyyy HH:mm",null);
					dtDlvryDt = dtDlvryDt.AddDays(1);
					txtShipEstDlvryDt.Text = dtDlvryDt.ToString("dd/MM/yyyy HH:mm");
					CheckTheCalcDay();
				}
			}
		}

		private void btnNotToSave_Click(object sender, System.EventArgs e)
		{
			ViewState["isTextChanged"] = false;
			ViewState["DSOperation"] = Operation.None;
			DomstcShipPanel.Visible = false;
			ddbCustType.Visible = true;
			if ((String)ViewState["AddSurcharge"] == "asking")
			{
				ReCalculateDlvryDt();

				//Check whether it is holiday/Sat/Sun
				ServiceAvailable serviceAvailable =  CheckDayServiceAvail();

				if((serviceAvailable.isServiceAvail == true) && ((String)ViewState["AddSurcharge"] == "none"))
				{
					ViewState["ServiceVASCode"] = serviceAvailable.strVASCode;
					//display the popup msg
					strErrorMsg = "Weekend / Public Holiday Delivery Service is available. Weekend / Public Holiday Surcharge";
					lblConfirmMsg.Text = "Weekend / Public Holiday Delivery Service is available. Weekend / Public Holiday Surcharge will be imposed. Do you want to continue?";
					DomstcShipPanel.Visible = true;
					ddbCustType.Visible = false;
					ViewState["AddSurcharge"] = "asking";
				}
				
			}
		}

		private void btnToCancel_Click(object sender, System.EventArgs e)
		{
			DomstcShipPanel.Visible = false;
			ddbCustType.Visible = true;
			ViewState["AddSurcharge"] = "none";
		}

		private void btnSendCust_Click(object sender, System.EventArgs e)
		{
			strErrorMsg = "";
			if(ddbCustType.SelectedItem.Value == "0")
			{
				strErrorMsg = "Select the Customer type.";
			}
			else if(ddbCustType.SelectedItem.Value == "C")
			{
				String sUrl = "SndRecipPopup.aspx?FORMID="+"DomesticShipment"+"&SENDRCPTYPE="+"S"+"&SENDERNAME="+txtSendName.Text.Trim();
				String sFeatures = "'height=320;width=160;left=100;top=50;location=no;menubar=no;resizable=yes;scrollbars=no;status=no;titlebar=yes;toolbar=no'";
				String sScript ="";
				sScript += "<script language=javascript>";
				sScript += "window.open('" + sUrl + "',''," + sFeatures + ");";
				sScript += "</script>";
				Response.Write(sScript);
			}
			else if(ddbCustType.SelectedItem.Value == "A")
			{
				String sUrl = "AgentSndRecPopup.aspx?FORMID="+"DomesticShipment"+"&SENDRCPTYPE="+"S"+"&SENDERNAME="+txtSendName.Text.Trim();
				String sFeatures = "'height=320;width=160;left=100;top=50;location=no;menubar=no;resizable=yes;scrollbars=no;status=no;titlebar=yes;toolbar=no'";
				String sScript ="";
				sScript += "<script language=javascript>";
				sScript += "window.open('" + sUrl + "',''," + sFeatures + ");";
				sScript += "</script>";
				Response.Write(sScript);
			}
			
			lblErrorMsg.Text = strErrorMsg;

		}
		private void btnRecipNm_Click(object sender, System.EventArgs e)
		{
			strErrorMsg = "";
			if(ddbCustType.SelectedItem.Value == "0")
			{
				strErrorMsg = "Select the Customer type.";
			}
			else if(ddbCustType.SelectedItem.Value == "C")
			{
				String sUrl = "SndRecipPopup.aspx?FORMID="+"DomesticShipment"+"&SENDRCPTYPE="+"R"+"&SENDERNAME="+txtRecName.Text.Trim();
				String sFeatures = "'height=320;width=160;left=100;top=50;location=no;menubar=no;resizable=yes;scrollbars=no;status=no;titlebar=yes;toolbar=no'";
				String sScript ="";
				sScript += "<script language=javascript>";
				sScript += "window.open('" + sUrl + "',''," + sFeatures + ");";
				sScript += "</script>";
				Response.Write(sScript);
			}
			else if(ddbCustType.SelectedItem.Value == "A")
			{
				String sUrl = "AgentSndRecPopup.aspx?FORMID="+"DomesticShipment"+"&SENDRCPTYPE="+"R"+"&SENDERNAME="+txtRecName.Text.Trim();
				String sFeatures = "'height=320;width=160;left=100;top=50;location=no;menubar=no;resizable=yes;scrollbars=no;status=no;titlebar=yes;toolbar=no'";
				String sScript ="";
				sScript += "<script language=javascript>";
				sScript += "window.open('" + sUrl + "',''," + sFeatures + ");";
				sScript += "</script>";
				Response.Write(sScript);
			}
			
			lblErrorMsg.Text = strErrorMsg;
		}

		private void txtShipEstDlvryDt_TextChanged(object sender, System.EventArgs e)
		{
			if((bool)ViewState["isTextChanged"] == false)
			{
				ChangeDSState();
				ViewState["isTextChanged"] = true;
			}

			//			if (((int)ViewState["DSMode"] != (int)ScreenMode.ExecuteQuery) && ((int)ViewState["DSMode"]!=(int)ScreenMode.Query))
			//			{
			//On Changing the Estimated Delivery date 
			DatePkUpDlvryTextChange();
			//			}
		}

		private void btnQry_Click(object sender, System.EventArgs e)
		{
			if (((int)ViewState["DSOperation"] == (int)Operation.Insert) || ((int)ViewState["DSOperation"] == (int)Operation.Update))
			{
				lblConfirmMsg.Text = "Do you want to save changes? ";
				DomstcShipPanel.Visible = true;
				ddbCustType.Visible = false;
				ViewState["MoveFirst"]=true;			
				return;
			}
			btnExecQry.Enabled = true;
			
			Query();
		}

		private void ResetForQuery()
		{
			rbCash.Checked = false;
			rbCredit.Checked = false;
			rbtnShipFrghtColl.Checked = false;
			rbtnShipFrghtPre.Checked = false;
			rbtnShipFrghtColl.Enabled = true;
			rbtnShipFrghtPre.Enabled = true;
			chkshpRtnHrdCpy.Checked = false;
			txtBookingNo.Enabled = true;
			txtBookDate.Enabled = true;
			txtDelManifest.Enabled = true;
			txtShipManifestDt.Enabled = true;
			txtRouteCode.Enabled = true;
			m_sdsDomesticShip = DomesticShipmentMgrDAL.GetDomShipSessionDS();
			Session["SESSION_DS3"] = m_sdsDomesticShip;
		}

		private void Query()
		{
			btnPkgDetails.Enabled = false;
			if (((bool)ViewState["isTextChanged"] == true)&& ((int)ViewState["DSMode"]==(int)ScreenMode.ExecuteQuery)) 
			{
				lblConfirmMsg.Text = "Do you want to save changes? ";
				DomstcShipPanel.Visible = true;
				ddbCustType.Visible = false;
				ViewState["DSMode"] = ScreenMode.Query;
				return;
			}
			ViewState["DSMode"] = ScreenMode.Query;
			ClearAllFields();
			lblErrorMsg.Text =""; 
			btnExecQry.Enabled = true;
			btnInsert.Enabled=true;
			btnGoToFirstPage.Enabled = false;
			btnNextPage.Enabled = false;
			btnPreviousPage.Enabled = false;
			btnGoToFirstPage.Enabled = false;
			btnGoToLastPage.Enabled = false;
			txtGoToRec.Text = "";

			
			ResetForQuery();
			
			ViewState["DSOperation"] = Operation.None;
			ViewState["isTextChanged"] = false;
			ViewState["currentPage"] = 0;
		}

		private void btnExecQry_Click(object sender, System.EventArgs e)
		{
			btnPkgDetails.Enabled = true;
			txtBookingNo.Enabled = false;
			if (((int)ViewState["DSOperation"] == (int)Operation.Insert) || ((int)ViewState["DSOperation"] == (int)Operation.Update))
			{
				lblConfirmMsg.Text = "Do you want to save changes? ";
				DomstcShipPanel.Visible = true;
				ddbCustType.Visible = false;
				ViewState["MoveFirst"]=true;			
				return;
			}
			ExecuteQueryDS();
		}
		private void ExecuteQueryDS()
		{
			GetValuesIntoDS(0);

			ViewState["QUERY_DS"] = m_sdsDomesticShip.ds;

			ViewState["DSMode"] = ScreenMode.ExecuteQuery;
			ViewState["DSOperation"] = Operation.None;
			ViewState["isTextChanged"] = false;
			ViewState["currentPage"] = 0;
			ViewState["currentSet"] = 0;
			
			FetchDSRecSet();
			if(m_sdsDomesticShip.QueryResultMaxSize == 0)
			{
				lblErrorMsg.Text = "Search resulted in 0 match";
				EnableNavigationButtons(false,false,false,false);
				return;
			}

			DisplayRecords();

			btnExecQry.Enabled = false;
			btnInsert.Enabled=true;
			txtDelManifest.Enabled = false;
			txtShipManifestDt.Enabled = false;
			btnDelete.Enabled=true;
			txtRouteCode.Enabled = false;

			if(m_sdsDomesticShip != null && m_sdsDomesticShip.DataSetRecSize > 0)
			{
				EnableNavigationButtons(false,false,true,true);
			}

			
		}
		
		private void FetchDSRecSet()
		{
			int iStartIndex = (int)ViewState["currentSet"] * m_iSetSize;
			DataSet dsQuery = (DataSet)ViewState["QUERY_DS"];
			m_sdsDomesticShip = DomesticShipmentMgrDAL.GetShipmentData(appID,enterpriseID,iStartIndex,m_iSetSize,dsQuery);
			Session["SESSION_DS3"] = m_sdsDomesticShip;
			decimal pgCnt = (m_sdsDomesticShip.QueryResultMaxSize - 1)/m_iSetSize;
			if(pgCnt < (int)ViewState["currentSet"])
			{
				ViewState["currentSet"] = System.Convert.ToInt32(pgCnt);
			}
		}

		private void EnableNavigationButtons(bool bMoveFirst, bool bMoveNext,bool bEnableMovePrevious, bool bMoveLast)
		{
			btnGoToFirstPage.Enabled	= bMoveFirst;
			btnPreviousPage.Enabled		= bMoveNext;
			btnNextPage.Enabled			= bEnableMovePrevious;
			btnGoToLastPage.Enabled		= bMoveLast;
		}
		private void DisplayRecords()
		{
			DataRow drEach = m_sdsDomesticShip.ds.Tables[0].Rows[(int)ViewState["currentPage"]];

			Zipcode zipCode = new Zipcode();

			txtBookingNo.Text = drEach["booking_no"].ToString();

			if ((drEach["payer_type"] != null) && (drEach["payer_type"].ToString() != ""))
			{
				ddbCustType.SelectedItem.Selected=false;   
				ddbCustType.Items.FindByValue(drEach["payer_type"].ToString()).Selected=true; 
			}

			if ((drEach["consignment_no"] != null) && (drEach["consignment_no"].ToString() != ""))
			{
				txtConsigNo.Text = drEach["consignment_no"].ToString();
			}
			else
			{
				txtConsigNo.Text = "";
			}

			if ((drEach["ref_no"] != null) && (drEach["ref_no"].ToString() != ""))
			{
				txtRefNo.Text = drEach["ref_no"].ToString();
			}
			else
			{
				txtRefNo.Text = "";
			}

			if ((drEach["origin_state_code"] != null) && (drEach["origin_state_code"].ToString() != ""))
			{
				txtOrigin.Text = drEach["origin_state_code"].ToString();
			}
			else
			{
				txtOrigin.Text = "";
			}

			if ((drEach["destination_state_code"] != null) && (drEach["destination_state_code"].ToString() != ""))
			{
				txtDestination.Text = drEach["destination_state_code"].ToString();
			}
			else
			{
				txtDestination.Text = "";
			}

			if ((drEach["origin_state_code"] != null) && (drEach["origin_state_code"].ToString() != ""))
			{
				txtSendZip.Text = drEach["origin_state_code"].ToString();
			}
			else
			{
				txtSendZip.Text = "";
			}

			if ((drEach["destination_state_code"] != null) && (drEach["destination_state_code"].ToString() != ""))
			{
				txtRecipZip.Text = drEach["destination_state_code"].ToString();
			}
			else
			{
				txtRecipZip.Text = "";
			}

			if ((drEach["booking_datetime"] != null) && (drEach["booking_datetime"].ToString() != ""))
			{
				DateTime dtBooking = (DateTime)drEach["booking_datetime"];
				txtBookDate.Text = dtBooking.ToString("dd/MM/yyyy HH:mm");
			}
			else
			{
				txtBookDate.Text = "";
			}

			if ((drEach["shpt_manifest_datetime"] != null) && (drEach["shpt_manifest_datetime"].ToString() != ""))
			{
				DateTime dtManifstDateTime = (DateTime)drEach["shpt_manifest_datetime"];
				txtShipManifestDt.Text = dtManifstDateTime.ToString("dd/MM/yyyy HH:mm");
			}
			else
			{
				txtShipManifestDt.Text = "";
			}
			
			if ((drEach["last_status_code"] != null) && (drEach["last_status_code"].ToString() != ""))
			{
				txtLatestStatusCode.Text = drEach["last_status_code"].ToString();
			}
			else
			{
				txtLatestStatusCode.Text = "";
			}

			if ((drEach["last_exception_code"] != null) && (drEach["last_exception_code"].ToString() != ""))
			{
				txtLatestExcepCode.Text = drEach["last_exception_code"].ToString();
			}
			else
			{
				txtLatestExcepCode.Text = "";
			}

			if(drEach["delivery_manifested"].ToString() == "N")
				txtDelManifest.Text = "No";
			else if(drEach["delivery_manifested"].ToString() == "Y")
				txtDelManifest.Text = "Yes";

			if ((drEach["route_code"] != null) && (drEach["route_code"].ToString() != ""))
			{
				txtRouteCode.Text = drEach["route_code"].ToString();
			}
			else
			{
				txtRouteCode.Text = "";
			}

			if ((drEach["payerid"] != null) && (drEach["payerid"].ToString() != ""))
			{
				txtCustID.Text = drEach["payerid"].ToString();
			}
			else
			{
				txtCustID.Text = "";
			}

			String strNewAcnt = drEach["new_account"].ToString();
			if(strNewAcnt.Equals("Y"))
				chkNewCust.Checked = true;
			else if(strNewAcnt.Equals("N"))
				chkNewCust.Checked = false;

			if ((drEach["payer_name"] != null) && (drEach["payer_name"].ToString() != ""))
			{
				txtCustName.Text = drEach["payer_name"].ToString();
			}
			else
			{
				txtCustName.Text = "";
			}

			if ((drEach["payer_address1"] != null) && (drEach["payer_address1"].ToString() != ""))
			{
				txtCustAdd1.Text = drEach["payer_address1"].ToString();
			}
			else
			{
				txtCustAdd1.Text = "";
			}

			if ((drEach["payer_address2"] != null) && (drEach["payer_address2"].ToString() != ""))
			{
				txtCustAdd2.Text = drEach["payer_address2"].ToString();
			}
			else
			{
				txtCustAdd2.Text = "";
			}

			if ((drEach["payer_fax"] != null) && (drEach["payer_fax"].ToString() != ""))
			{
				txtCustFax.Text = drEach["payer_fax"].ToString();
			}
			else
			{
				txtCustFax.Text = "";
			}

			if ((drEach["payer_country"] != null) && (drEach["payer_country"].ToString() != ""))
			{
				txtCustStateCode.Text = drEach["payer_country"].ToString();
			}
			else
			{
				txtCustStateCode.Text = "";
			}

			if ((drEach["payer_telephone"] != null) && (drEach["payer_telephone"].ToString() != ""))
			{
				txtCustTelephone.Text = drEach["payer_telephone"].ToString();
			}
			else
			{
				txtCustTelephone.Text = "";
			}

			if ((drEach["payer_zipcode"] != null) && (drEach["payer_zipcode"].ToString() != ""))
			{
				txtCustZipCode.Text = drEach["payer_zipcode"].ToString();
			}
			else
			{
				txtCustZipCode.Text = "";
			}

			zipCode.Populate(appID,enterpriseID,txtCustStateCode.Text,txtCustZipCode.Text);
			txtCustCity.Text = zipCode.StateName;

			String strPaymentMode = drEach["payment_mode"].ToString();
			if (strPaymentMode.Equals("C"))
				rbCash.Checked = true;
			else if (strPaymentMode.Equals("R"))
				rbCredit.Checked = true;

			if ((drEach["sender_name"] != null) && (drEach["sender_name"].ToString() != ""))
			{
				txtSendName.Text = drEach["sender_name"].ToString();
			}
			else
			{
				txtSendName.Text = "";
			}

			if ((drEach["sender_address1"] != null) && (drEach["sender_address1"].ToString() != ""))
			{
				txtSendAddr1.Text = drEach["sender_address1"].ToString();
			}
			else
			{
				txtSendAddr1.Text = "";
			}

			if ((drEach["sender_address2"] != null) && (drEach["sender_address2"].ToString() != ""))
			{
				txtSendAddr2.Text = drEach["sender_address2"].ToString();
			}
			else
			{
				txtSendAddr2.Text = "";
			}

			if ((drEach["sender_zipcode"] != null) && (drEach["sender_zipcode"].ToString() != ""))
			{
				txtSendZip.Text = drEach["sender_zipcode"].ToString();
			}
			else
			{
				txtSendZip.Text = "";
			}

			if ((drEach["sender_contact_person"] != null) && (drEach["sender_contact_person"].ToString() != ""))
			{
				txtSendContPer.Text = drEach["sender_contact_person"].ToString();
			}
			else
			{
				txtSendContPer.Text = "";
			}

			if ((drEach["sender_fax"] != null) && (drEach["sender_fax"].ToString() != ""))
			{
				txtSendFax.Text = drEach["sender_fax"].ToString();
			}
			else
			{
				txtSendFax.Text = "";
			}

			if ((drEach["sender_country"] != null) && (drEach["sender_country"].ToString() != ""))
			{
				txtSendState.Text = drEach["sender_country"].ToString();
			}
			else
			{
				txtSendState.Text = "";
			}

			if ((drEach["sender_telephone"] != null) && (drEach["sender_telephone"].ToString() != ""))
			{
				txtSendTel.Text = drEach["sender_telephone"].ToString();
			}
			else
			{
				txtSendTel.Text = "";
			}

			zipCode.Populate(appID,enterpriseID,txtSendState.Text,txtSendZip.Text);
			txtSendCity.Text = zipCode.StateName;
			txtSendCuttOffTime.Text = zipCode.CuttOffTime.ToString("HH:mm");

			if ((drEach["recipient_address1"] != null) && (drEach["recipient_address1"].ToString() != ""))
			{
				txtRecipAddr1.Text = drEach["recipient_address1"].ToString();
			}
			else
			{
				txtRecipAddr1.Text = "";
			}

			if ((drEach["recipient_address2"] != null) && (drEach["recipient_address2"].ToString() != ""))
			{
				txtRecipAddr2.Text = drEach["recipient_address2"].ToString();
			}
			else
			{
				txtRecipAddr2.Text = "";
			}

			if ((drEach["recipient_fax"] != null) && (drEach["recipient_fax"].ToString() != ""))
			{
				txtRecipFax.Text = drEach["recipient_fax"].ToString();
			}
			else
			{
				txtRecipFax.Text = "";
			}

			if ((drEach["recipient_country"] != null) && (drEach["recipient_country"].ToString() != ""))
			{
				txtRecipState.Text = drEach["recipient_country"].ToString();
			}
			else
			{
				txtRecipState.Text = "";
			}

			if ((drEach["recipient_telephone"] != null) && (drEach["recipient_telephone"].ToString() != ""))
			{
				txtRecipTel.Text = drEach["recipient_telephone"].ToString();
			}
			else
			{
				txtRecipTel.Text = "";
			}


			if ((drEach["recipient_zipcode"] != null) && (drEach["recipient_zipcode"].ToString() != ""))
			{
				txtRecipZip.Text = drEach["recipient_zipcode"].ToString();
			}
			else
			{
				txtRecipZip.Text = "";
			}

			if ((drEach["recipient_name"] != null) && (drEach["recipient_name"].ToString() != ""))
			{
				txtRecName.Text = drEach["recipient_name"].ToString();
			}
			else
			{
				txtRecName.Text = "";
			}

			if ((drEach["recipient_contact_person"] != null) && (drEach["recipient_contact_person"].ToString() != ""))
			{
				txtRecpContPer.Text = drEach["recipient_contact_person"].ToString();
			}
			else
			{
				txtRecpContPer.Text = "";

			}

			zipCode.Populate(appID,enterpriseID,txtRecipState.Text,txtRecipZip.Text);
			txtRecipCity.Text = zipCode.StateName;
			//txtRouteCode.Text = zipCode.DeliveryRoute;

			txtPkgActualWt.Text = drEach["tot_wt"].ToString();
			txtPkgDimWt.Text = drEach["tot_dim_wt"].ToString();
			txtPkgTotpkgs.Text = drEach["tot_pkg"].ToString();
			txtPkgChargeWt.Text = drEach["chargeable_wt"].ToString();

			if ((drEach["commodity_code"] != null) && (drEach["commodity_code"].ToString() != ""))
			{
				txtPkgCommCode.Text = drEach["commodity_code"].ToString();
			}
			else
			{
				txtPkgCommCode.Text = "";
			}

			
			CommodityDetails commdetails = new CommodityDetails();
			commdetails.Populate(appID,enterpriseID,txtPkgCommCode.Text);
			txtPkgCommDesc.Text = commdetails.CommodityDescription;

			if ((drEach["est_delivery_datetime"] != null) && (drEach["est_delivery_datetime"].ToString() != ""))
			{
				DateTime dtDlvryDt = (DateTime)drEach["est_delivery_datetime"];
				txtShipEstDlvryDt.Text = dtDlvryDt.ToString("dd/MM/yyyy HH:mm");
			}
			else
			{
				txtShipEstDlvryDt.Text = "";
			}


			if ((drEach["act_pickup_datetime"] != null) && (drEach["act_pickup_datetime"].ToString() != ""))
			{
				//DateTime dtActPickUp = (DateTime)drEach["act_pickup_datetime"];
				//txtShipPckUpTime.Text = dtActPickUp.ToString("dd/MM/yyyy HH:mm");
//				if ((drEach["last_status_code"] != null) && (drEach["last_status_code"].ToString() != "") && (drEach["last_status_code"].ToString() == "POD"))
//				{
//					DateTime dtBkg =  DomesticShipmentMgrDAL.PickupBkgDateTimeForPOD(appID,enterpriseID,Convert.ToInt32(txtBookingNo.Text));
//					if(!dtBkg.ToString("dd/MM/yyyy HH:mm").Equals("01/01/0001 00:00"))
//					txtShipPckUpTime.Text = dtBkg.ToString("dd/MM/yyyy HH:mm");
//					else
//						txtShipPckUpTime.Text="";
//				}
//				else
//				{
					DateTime dtActPickUp = (DateTime)drEach["act_pickup_datetime"];
					txtShipPckUpTime.Text = dtActPickUp.ToString("dd/MM/yyyy HH:mm");
//				}
			}
			else
			{
				txtShipPckUpTime.Text = "";
			}
			if ((drEach["act_delivery_date"] != null) && (drEach["act_delivery_date"].ToString() != ""))
			{
				DateTime dtActDlvry = (DateTime)drEach["act_delivery_date"];
				txtActualDeliveryDt.Text = dtActDlvry.ToString("dd/MM/yyyy HH:mm");
			}
			else
			{
				txtActualDeliveryDt.Text = "";
			}

			if ((drEach["percent_dv_additional"] != null) && (drEach["percent_dv_additional"].ToString() != ""))
			{
				txtShpAddDV.Text = drEach["percent_dv_additional"].ToString();
			}
			else
			{
				txtShpAddDV.Text = "";
			}

			if ((drEach["declare_value"] != null) && (drEach["declare_value"].ToString() != ""))
			{
				decimal delShpDclrValue	=	Convert.ToDecimal(drEach["declare_value"]);
				txtShpDclrValue.Text = String.Format((String)ViewState["m_format"], delShpDclrValue);
			}
			else
			{
				txtShpDclrValue.Text = "";
			}
			if ((drEach["insurance_surcharge"] != null) && (drEach["insurance_surcharge"].ToString() != ""))
			{
				decimal delShpInsSurchrg	=	Convert.ToDecimal(drEach["insurance_surcharge"]);
				txtShpInsSurchrg.Text = String.Format((String)ViewState["m_format"], delShpInsSurchrg);
			}
			else
			{
				txtShpInsSurchrg.Text = "";
			}

			if ((drEach["max_insurance_cover"] != null) && (drEach["max_insurance_cover"].ToString() != ""))
			{
				decimal delShpMaxCvrg	=	Convert.ToDecimal(drEach["max_insurance_cover"]);
				txtShpMaxCvrg.Text = String.Format((String)ViewState["m_format"], delShpMaxCvrg);
			}
			else
			{
				txtShpMaxCvrg.Text = "";
			}


			//txtShpAddDV.Text = drEach["percent_dv_additional"].ToString();
			//txtShpDclrValue.Text = drEach["declare_value"].ToString();
			//txtShpInsSurchrg.Text = drEach["insurance_surcharge"].ToString();
			//txtShpMaxCvrg.Text = drEach["max_insurance_cover"].ToString();

			if ((drEach["service_code"] != null) && (drEach["service_code"].ToString() != ""))
			{
				txtShpSvcCode.Text = drEach["service_code"].ToString();
			}
			else
			{
				txtShpSvcCode.Text = "";
			}

			//call the method to get the description of service code.
			Service service = new Service();
			service.Populate(appID,enterpriseID,txtShpSvcCode.Text.Trim());
			txtShpSvcDesc.Text = service.ServiceDescription;
			
			String strRutrnPOD = drEach["return_pod_slip"].ToString();
			if(strRutrnPOD.Equals("Y"))
				chkshpRtnHrdCpy.Checked = true;
			else if(strRutrnPOD.Equals("N"))
				chkshpRtnHrdCpy.Checked = false;

			String strFright = drEach["payment_type"].ToString();
			if(strFright.Equals("FP"))
				rbtnShipFrghtPre.Checked = true;
			else if(strFright.Equals("FC"))
				rbtnShipFrghtColl.Checked = true;

			//Get the Shipment VAS & bind to the VAS grid
			try
			{
				m_dsVAS = DomesticShipmentMgrDAL.GetVASData(appID,enterpriseID,Convert.ToInt32(drEach["booking_no"]),(String)drEach["consignment_no"]);
			}
			catch(ApplicationException appException)
			{
				lblErrorMsg.Text = appException.Message;
				return;
			}
			BindVASGrid();
			decimal decFreightChrg	= Convert.ToDecimal(drEach["tot_freight_charge"]);
			txtFreightChrg.Text = String.Format((String)ViewState["m_format"], decFreightChrg);

			decimal decinsChrg	= Convert.ToDecimal(drEach["insurance_surcharge"]);
			txtInsChrg.Text = String.Format((String)ViewState["m_format"], decinsChrg);
			
			decimal decTotVASSurChrg = 0;
			if (drEach["tot_vas_surcharge"] != System.DBNull.Value)
			{
				decTotVASSurChrg	= Convert.ToDecimal(drEach["tot_vas_surcharge"]);	
			}
			txtTotVASSurChrg.Text = String.Format((String)ViewState["m_format"], decTotVASSurChrg);
			
			if ((drEach["esa_surcharge"] != null) && (drEach["esa_surcharge"].ToString() != ""))
			{
				decimal decESASurchrg	= Convert.ToDecimal(drEach["esa_surcharge"]);
				txtESASurchrg.Text = String.Format((String)ViewState["m_format"], decESASurchrg);
			}
			else
			{
				txtESASurchrg.Text = String.Format((String)ViewState["m_format"], 0);
			}

			if ((drEach["cod_amount"] != null) && (drEach["cod_amount"].ToString() != ""))
			{
				decimal decTmpCODAmount = Convert.ToDecimal(drEach["cod_amount"]);
				//txtCODAmount.Text = TIESUtility.Round_Enterprise_Currency(appID,enterpriseID,decTmpCODAmount).ToString();			
				txtCODAmount.Text = String.Format((String)ViewState["m_format"], decTmpCODAmount);
			}
			else
			{
				decimal tmpCod = 0;
				txtCODAmount.Text = String.Format((String)ViewState["m_format"], tmpCod);
			}

			CalculateTotalAmt();
		}

		private void btnGoToLastPage_Click(object sender, System.EventArgs e)
		{
			MoveLastDS();
		}
		private void MoveLastDS()
		{
			if (((int)ViewState["DSOperation"] == (int)Operation.Insert) || ((int)ViewState["DSOperation"] == (int)Operation.Update))
			{
				lblConfirmMsg.Text = "Do you want to save changes? ";
				DomstcShipPanel.Visible = true;
				ddbCustType.Visible = false;
				ViewState["MoveLast"]=true;			
				return;
			}
			ViewState["currentSet"] = (m_sdsDomesticShip.QueryResultMaxSize - 1)/m_iSetSize;	
			FetchDSRecSet();
			ViewState["currentPage"] = m_sdsDomesticShip.DataSetRecSize - 1;
			DisplayRecords();	

			EnableNavigationButtons(true,true,false,false);
		}
		private void MoveLast()
		{
			//			if (((int)ViewState["DSOperation"] == (int)Operation.Insert) || ((int)ViewState["DSOperation"] == (int)Operation.Update))
			//			{
			//				lblConfirmMsg.Text = "Do you want to save changes? ";
			//				DomstcShipPanel.Visible = true;
			//				//ViewState["isTextChanged"] = false;
			//				ViewState["MoveLast"]=true;			
			//				return;
			//			}
			//			int Qrysize=0;
			//			//ClearAllFields();
			//			if (m_sdsDomesticShip.QueryResultMaxSize > 10)
			//			{
			//				Qrysize = m_sdsDomesticShip.QueryResultMaxSize-10;
			//			}
			//			else
			//			{
			//				Qrysize=0;
			//			}
			//			GetValuesIntoDS(0);
			//			Session["QUERY_DS"] = m_sdsDomesticShip.ds;
			//			m_sdsDomesticShip.DataSetRecSize = 10;
			//			try
			//			{
			//				m_sdsDomesticShip = DomesticShipmentMgrDAL.GetShipmentData(appID,enterpriseID,Qrysize,m_sdsDomesticShip.DataSetRecSize,m_sdsDomesticShip.ds );  
			//			}
			//			catch(ApplicationException appException)
			//			{
			//				lblErrorMsg.Text = appException.Message;
			//				return;
			//			}
			//			ViewState["CurrentRow"]=m_sdsDomesticShip.DataSetRecSize-1;
			//			DisplayRecords(m_sdsDomesticShip.DataSetRecSize-1);
			//			btnNextPage.Enabled = false;
			//			btnPreviousPage.Enabled = true;
		}

		private void btnGoToFirstPage_Click(object sender, System.EventArgs e)
		{
			MoveFirstDS();
		}
		private void MoveFirstDS()
		{
			if (((int)ViewState["DSOperation"] == (int)Operation.Insert) || ((int)ViewState["DSOperation"] == (int)Operation.Update))
			{
				lblConfirmMsg.Text = "Do you want to save changes? ";
				DomstcShipPanel.Visible = true;
				ddbCustType.Visible = false;
				ViewState["MoveFirst"]=true;			
				return;
			}
			ViewState["currentSet"] = 0;	
			FetchDSRecSet();
			ViewState["currentPage"] = 0;
			DisplayRecords();
			EnableNavigationButtons(false,false,true,true);
		}
		private void MoveFirst()
		{
			//			if (((int)ViewState["DSOperation"] == (int)Operation.Insert) || ((int)ViewState["DSOperation"] == (int)Operation.Update))
			//			{
			//				lblConfirmMsg.Text = "Do you want to save changes? ";
			//				DomstcShipPanel.Visible = true;
			//				//ViewState["isTextChanged"] = false;
			//				ViewState["MoveFirst"]=true;			
			//				return;
			//			}
			//			btnPreviousPage.Enabled = false;
			//			btnNextPage.Enabled = true;   
			//			ClearAllFields();
			//			GetValuesIntoDS(0);
			//			Session["QUERY_DS"] = m_sdsDomesticShip.ds;
			//			m_sdsDomesticShip.DataSetRecSize = 10;
			//			try
			//			{
			//				m_sdsDomesticShip = DomesticShipmentMgrDAL.GetShipmentData(appID,enterpriseID,0,m_sdsDomesticShip.DataSetRecSize,m_sdsDomesticShip.ds );  
			//			}
			//			catch(ApplicationException appException)
			//			{
			//				lblErrorMsg.Text = appException.Message;
			//				return;
			//			}
			//			ViewState["CurrentRow"]=0;
			//			DisplayRecords((int)ViewState["CurrentRow"]); 
			//			Session["SESSION_DS3"] = m_sdsDomesticShip;
		}

		private void btnNextPage_Click(object sender, System.EventArgs e)
		{
			MoveNextDS();
		}
		private void MoveNextDS()
		{
			lblErrorMsg.Text = "";
			if (((int)ViewState["DSOperation"] == (int)Operation.Insert) || ((int)ViewState["DSOperation"] == (int)Operation.Update))
			{
				lblConfirmMsg.Text = "Do you want to save changes? ";
				DomstcShipPanel.Visible = true;
				ddbCustType.Visible = false;
				//ViewState["isTextChanged"] = false;
				ViewState["MoveNext"]=true;
				return;
			}
			int iCurrentPage = (int)ViewState["currentPage"];

			if((int)ViewState["currentPage"]  < (m_sdsDomesticShip.DataSetRecSize - 1) )
			{
				ViewState["currentPage"] = (int)ViewState["currentPage"] + 1;
				DisplayRecords();
			}
			else
			{
				decimal iTotalRec = ((decimal)ViewState["currentSet"] * m_iSetSize) + m_sdsDomesticShip.DataSetRecSize;
				if( iTotalRec ==  m_sdsDomesticShip.QueryResultMaxSize)
				{
					EnableNavigationButtons(true,true,false,false);
					return;
				}
				
				ViewState["currentSet"] = (int)ViewState["currentSet"] + 1;	
				FetchDSRecSet();
				ViewState["currentPage"] = 0;
				DisplayRecords();
			}
			EnableNavigationButtons(true,true,true,true);
		}
		private void btnPreviousPage_Click(object sender, System.EventArgs e)
		{
			MovePreviousDS();
		}
		private void MovePreviousDS()
		{
			if (((int)ViewState["DSOperation"] == (int)Operation.Insert) || ((int)ViewState["DSOperation"] == (int)Operation.Update))
			{
				lblConfirmMsg.Text = "Do you want to save changes? ";
				DomstcShipPanel.Visible = true;
				ddbCustType.Visible = false;
				ViewState["MovePrevious"]=true;
				return;
			}

			int iCurrentPage = (int)ViewState["currentPage"];

			if((int)ViewState["currentPage"]  > 0 )
			{
				ViewState["currentPage"] = (int)ViewState["currentPage"] - 1;
				DisplayRecords();
			}
			else
			{
				if( ((int)ViewState["currentSet"] - 1) < 0)
				{
					EnableNavigationButtons(false,false,true,true);
					return;
				}
				
				ViewState["currentSet"] = (int)ViewState["currentSet"] - 1;	
				FetchDSRecSet();
				ViewState["currentPage"] = m_sdsDomesticShip.DataSetRecSize - 1;
				DisplayRecords();
			}
			EnableNavigationButtons(true,true,true,true);
		}
		
		private void txtGoToRec_TextChanged(object sender, System.EventArgs e)
		{
		
		}

		private void txtPkgCommCode_TextChanged(object sender, System.EventArgs e)
		{
			if((bool)ViewState["isTextChanged"] == false)
			{
				ChangeDSState();
				ViewState["isTextChanged"] = true;
			}

			strErrorMsg = "";

			if(txtPkgCommCode.Text.Length > 0)
			{
				CommodityDetails commdetails = new CommodityDetails();
				commdetails.Populate(appID,enterpriseID,txtPkgCommCode.Text);
				
				String strDesc = commdetails.CommodityDescription;
				String strCode = commdetails.CommodityCode;
				if(strCode == null)
				{
					strErrorMsg = "Commodity Code does not exist";
				}
				else
				{
					txtPkgCommDesc.Text = strDesc;
				}
			}
		}

		private void rbtnShipFrghtPre_CheckedChanged(object sender, System.EventArgs e)
		{
			if((bool)ViewState["isTextChanged"] == false)
			{
				ChangeDSState();
				ViewState["isTextChanged"] = true;
			}
		}

		private void rbtnShipFrghtColl_CheckedChanged(object sender, System.EventArgs e)
		{
			if((bool)ViewState["isTextChanged"] == false)
			{
				ChangeDSState();
				ViewState["isTextChanged"] = true;
			}
		}

		private void txtConsigNo_TextChanged(object sender, System.EventArgs e)
		{
			if((bool)ViewState["isTextChanged"] == false)
			{
				ChangeDSState();
				ViewState["isTextChanged"] = true;
			}
		}

		private void txtRefNo_TextChanged(object sender, System.EventArgs e)
		{
			if((bool)ViewState["isTextChanged"] == false)
			{
				ChangeDSState();
				ViewState["isTextChanged"] = true;
			}
		}

		private void txtRouteCode_TextChanged(object sender, System.EventArgs e)
		{
			if((bool)ViewState["isTextChanged"] == false)
			{
				ChangeDSState();
				ViewState["isTextChanged"] = true;
			}
		}

		private void txtCustName_TextChanged(object sender, System.EventArgs e)
		{
			if((bool)ViewState["isTextChanged"] == false)
			{
				ChangeDSState();
				ViewState["isTextChanged"] = true;
			}
		}

		private void txtCustAdd1_TextChanged(object sender, System.EventArgs e)
		{
			if((bool)ViewState["isTextChanged"] == false)
			{
				ChangeDSState();
				ViewState["isTextChanged"] = true;
			}
		}

		private void txtCustAdd2_TextChanged(object sender, System.EventArgs e)
		{
			if((bool)ViewState["isTextChanged"] == false)
			{
				ChangeDSState();
				ViewState["isTextChanged"] = true;
			}
		}

		private void txtCustTelephone_TextChanged(object sender, System.EventArgs e)
		{
			if((bool)ViewState["isTextChanged"] == false)
			{
				ChangeDSState();
				ViewState["isTextChanged"] = true;
			}
		}

		private void txtCustFax_TextChanged(object sender, System.EventArgs e)
		{
			if((bool)ViewState["isTextChanged"] == false)
			{
				ChangeDSState();
				ViewState["isTextChanged"] = true;
			}
		}

		private void txtSendName_TextChanged(object sender, System.EventArgs e)
		{
			if((bool)ViewState["isTextChanged"] == false)
			{
				ChangeDSState();
				ViewState["isTextChanged"] = true;
			}
		}

		private void txtSendAddr1_TextChanged(object sender, System.EventArgs e)
		{
			if((bool)ViewState["isTextChanged"] == false)
			{
				ChangeDSState();
				ViewState["isTextChanged"] = true;
			}
		}

		private void txtSendAddr2_TextChanged(object sender, System.EventArgs e)
		{
			if((bool)ViewState["isTextChanged"] == false)
			{
				ChangeDSState();
				ViewState["isTextChanged"] = true;
			}
		}

		private void txtSendContPer_TextChanged(object sender, System.EventArgs e)
		{
			if((bool)ViewState["isTextChanged"] == false)
			{
				ChangeDSState();
				ViewState["isTextChanged"] = true;
			}
		}

		private void txtSendTel_TextChanged(object sender, System.EventArgs e)
		{
			if((bool)ViewState["isTextChanged"] == false)
			{
				ChangeDSState();
				ViewState["isTextChanged"] = true;
			}
		}

		private void txtSendFax_TextChanged(object sender, System.EventArgs e)
		{
			if((bool)ViewState["isTextChanged"] == false)
			{
				ChangeDSState();
				ViewState["isTextChanged"] = true;
			}
		}

		private void txtRecName_TextChanged(object sender, System.EventArgs e)
		{
			if((bool)ViewState["isTextChanged"] == false)
			{
				ChangeDSState();
				ViewState["isTextChanged"] = true;
			}
		}

		private void txtRecipAddr1_TextChanged(object sender, System.EventArgs e)
		{
			if((bool)ViewState["isTextChanged"] == false)
			{
				ChangeDSState();
				ViewState["isTextChanged"] = true;
			}
		}

		private void txtRecipAddr2_TextChanged(object sender, System.EventArgs e)
		{
			if((bool)ViewState["isTextChanged"] == false)
			{
				ChangeDSState();
				ViewState["isTextChanged"] = true;
			}
		}

		private void txtRecpContPer_TextChanged(object sender, System.EventArgs e)
		{
			if((bool)ViewState["isTextChanged"] == false)
			{
				ChangeDSState();
				ViewState["isTextChanged"] = true;
			}
		}

		private void txtRecipTel_TextChanged(object sender, System.EventArgs e)
		{
			if((bool)ViewState["isTextChanged"] == false)
			{
				ChangeDSState();
				ViewState["isTextChanged"] = true;
			}
		}

		private void txtRecipFax_TextChanged(object sender, System.EventArgs e)
		{
			if((bool)ViewState["isTextChanged"] == false)
			{
				ChangeDSState();
				ViewState["isTextChanged"] = true;
			}
		}

		private void chkshpRtnHrdCpy_CheckedChanged(object sender, System.EventArgs e)
		{
			if((bool)ViewState["isTextChanged"] == false)
			{
				ChangeDSState();
				ViewState["isTextChanged"] = true;
			}
		}

		private void ChangeDSState()
		{
			if(((int)ViewState["DSMode"] == (int)ScreenMode.Insert) && ((int)ViewState["DSOperation"] == (int)Operation.None))
			{
				ViewState["DSOperation"] = Operation.Insert;
			}
			else if(((int)ViewState["DSMode"] == (int)ScreenMode.Insert) && ((int)ViewState["DSOperation"] == (int)Operation.Insert))
			{
				//During saving
				ViewState["DSOperation"] = Operation.Saved;
			}
			else if(((int)ViewState["DSMode"] == (int)ScreenMode.Insert) && ((int)ViewState["DSOperation"] == (int)Operation.Saved))
			{
				ViewState["DSOperation"] = Operation.Update;
			}
			else if(((int)ViewState["DSMode"] == (int)ScreenMode.Insert) && ((int)ViewState["DSOperation"] == (int)Operation.Update))
			{
				ViewState["DSOperation"] = Operation.Saved;
			}
			else if(((int)ViewState["DSMode"] == (int)ScreenMode.ExecuteQuery) && ((int)ViewState["DSOperation"] == (int)Operation.None))
			{
				ViewState["DSOperation"] = Operation.Update;
			}
			else if(((int)ViewState["DSMode"] == (int)ScreenMode.ExecuteQuery) && ((int)ViewState["DSOperation"] == (int)Operation.Update))
			{
				ViewState["DSOperation"] = Operation.None;
			}
		}

		private void btnDelete_Click(object sender, System.EventArgs e)
		{
			btnPkgDetails.Enabled = true;
			if (((int)ViewState["DSOperation"] == (int)Operation.Insert) || ((int)ViewState["DSOperation"] == (int)Operation.Update))
			{
				lblConfirmMsg.Text = "Do you want to save changes? ";
				DomstcShipPanel.Visible = true;	
				ddbCustType.Visible = false;
				return;
			}
			if(txtBookingNo.Text.Length > 0)
			{
				String[] strDeleteStatusList = TIESUtility.getDeleteAllowStatus(appID,enterpriseID);
				DomesticShipmentMgrDAL.DeleteDomesticShp(appID,enterpriseID,Convert.ToInt32(txtBookingNo.Text),txtConsigNo.Text.ToString().Trim(),strDeleteStatusList);
				lblErrorMsg.Text = "Deleted successfully";
			}
		}

		private void btnClose_Click(object sender, System.EventArgs e)
		{
			String strFrmId = Request.Params["FORMID"];
			String sScript = "";
			sScript += "<script language=javascript>";
			sScript += "  window.opener;" ;
			sScript += "  window.close();";
			sScript += "</script>";
			Response.Write(sScript);
		}

	}
}
