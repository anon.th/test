<%@ Page language="c#" Codebehind="ShipmentTracking.aspx.cs" AutoEventWireup="false" Inherits="com.ties.ShipmentTracking" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>ShipmentTracking</title>
		<LINK href="css/Styles.css" type="text/css" rel="stylesheet" name="stylesheet">
		<meta name="GENERATOR" Content="Microsoft Visual Studio 7.0">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
	</HEAD>
	<body MS_POSITIONING="GridLayout" onunload="window.opener.top.displayBanner.fnCloseAll(1);">
		<form id="ShipmentTracking" method="post" runat="server">
			<asp:datagrid id="dgShipTrack" style="Z-INDEX: 101; LEFT: 6px; POSITION: absolute; TOP: 97px" runat="server" OnItemCommand="dgShipTrack_OnItemCommand" OnDeleteCommand="dgShipTrack_OnDelete" AutoGenerateColumns="False" AllowCustomPaging="True" AllowPaging="True" OnItemDataBound="dgShipTrack_OnDataBound" OnPageIndexChanged="dgShipTrack_OnPageChange" PageSize="50">
				<ItemStyle Height="20px" CssClass="gridFieldSmall"></ItemStyle>
				<Columns>
					<asp:ButtonColumn Text="&lt;IMG SRC='images/butt-select.gif' border=0 title='Select' &gt;" ButtonType="LinkButton" CommandName="Delete">
						<HeaderStyle Width="2%" CssClass="gridHeadingSmall"></HeaderStyle>
						<ItemStyle HorizontalAlign="Center"></ItemStyle>
					</asp:ButtonColumn>
					<asp:TemplateColumn HeaderText="Booking No">
						<HeaderStyle Width="5%" CssClass="gridHeadingSmall"></HeaderStyle>
						<ItemStyle CssClass="gridFieldSmall"></ItemStyle>
						<ItemTemplate>
							<asp:Label CssClass="gridLabelSmall" ID="lnkbtnBookNo" Text='<%#DataBinder.Eval(Container.DataItem,"booking_no")%>' Runat="server">
							</asp:Label>
						</ItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Consignment No">
						<HeaderStyle Width="5%" CssClass="gridHeadingSmall"></HeaderStyle>
						<ItemStyle CssClass="gridFieldSmall"></ItemStyle>
						<ItemTemplate>
							<asp:LinkButton ID="lnkbtnConsignNo" Text='<%#DataBinder.Eval(Container.DataItem,"consignment_no")%>' Runat="server">
							</asp:LinkButton>
						</ItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Booking Date">
						<HeaderStyle Width="5%" CssClass="gridHeadingSmall"></HeaderStyle>
						<ItemStyle CssClass="gridFieldSmall"></ItemStyle>
						<ItemTemplate>
							<asp:Label CssClass="gridLabelSmall" ID="lblBookDate" Text='<%#DataBinder.Eval(Container.DataItem,"booking_datetime","{0:dd/MM/yyyy HH:mm}")%>' Runat="server">
							</asp:Label>
						</ItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Pickup Date">
						<HeaderStyle Width="5%" CssClass="gridHeadingSmall"></HeaderStyle>
						<ItemStyle CssClass="gridFieldSmall"></ItemStyle>
						<ItemTemplate>
							<asp:Label CssClass="gridLabelSmall" ID="lblPickupDate" Text='<%#DataBinder.Eval(Container.DataItem,"act_pickup_datetime","{0:dd/MM/yyyy HH:mm}")%>' Runat="server">
							</asp:Label>
						</ItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Payer Code">
						<HeaderStyle Width="5%" CssClass="gridHeadingSmall"></HeaderStyle>
						<ItemStyle CssClass="gridFieldSmall"></ItemStyle>
						<ItemTemplate>
							<asp:Label CssClass="gridLabelSmall" ID="lblPayerCode" Text='<%#DataBinder.Eval(Container.DataItem,"payerid")%>' Runat="server">
							</asp:Label>
						</ItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Payer Name">
						<HeaderStyle Width="15%" CssClass="gridHeadingSmall"></HeaderStyle>
						<ItemStyle CssClass="gridFieldSmall"></ItemStyle>
						<ItemTemplate>
							<asp:Label CssClass="gridLabelSmall" ID="lblPayerName" Text='<%#DataBinder.Eval(Container.DataItem,"payer_Name")%>' Runat="server">
							</asp:Label>
						</ItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Ref No">
						<HeaderStyle Width="8%" CssClass="gridHeadingSmall"></HeaderStyle>
						<ItemStyle CssClass="gridFieldSmall"></ItemStyle>
						<ItemTemplate>
							<asp:Label CssClass="gridLabelSmall" ID="lblReferenceNo" Text='<%#DataBinder.Eval(Container.DataItem,"ref_no")%>' Runat="server">
							</asp:Label>
						</ItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="ORI">
						<HeaderStyle Width="3%" CssClass="gridHeadingSmall"></HeaderStyle>
						<ItemStyle CssClass="gridFieldSmall"></ItemStyle>
						<ItemTemplate>
							<asp:Label CssClass="gridLabelSmall" ID="lblORI" Text='<%#DataBinder.Eval(Container.DataItem,"origin_state_code")%>' Runat="server">
							</asp:Label>
						</ItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="DST">
						<HeaderStyle Width="3%" CssClass="gridHeadingSmall"></HeaderStyle>
						<ItemStyle CssClass="gridFieldSmall"></ItemStyle>
						<ItemTemplate>
							<asp:Label CssClass="gridLabelSmall" ID="lblDST" Text='<%#DataBinder.Eval(Container.DataItem,"destination_state_code")%>' Runat="server">
							</asp:Label>
						</ItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Status Date/Time">
						<HeaderStyle Width="8%" CssClass="gridHeadingSmall"></HeaderStyle>
						<ItemStyle CssClass="gridFieldSmall"></ItemStyle>
						<ItemTemplate>
							<asp:Label CssClass="gridLabelSmall" ID="lblStatusDateTime" style="white-space: nowrap;"  Text='<%#DataBinder.Eval(Container.DataItem,"last_status_datetime","{0:dd/MM/yyyy HH:mm}")%>' Runat="server">
							</asp:Label>
						</ItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Status Code">
						<HeaderStyle Width="5%" CssClass="gridHeadingSmall"></HeaderStyle>
						<ItemStyle CssClass="gridFieldSmall"></ItemStyle>
						<ItemTemplate>
							<asp:Label CssClass="gridLabelSmall" ID="lblStatusCode" Text='<%#DataBinder.Eval(Container.DataItem,"last_status_code")%>' Runat="server">
							</asp:Label>
						</ItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Exception Code">
						<HeaderStyle Width="5%" CssClass="gridHeadingSmall"></HeaderStyle>
						<ItemStyle CssClass="gridFieldSmall"></ItemStyle>
						<ItemTemplate>
							<asp:Label CssClass="gridLabelSmall" ID="lblExceptionCode" Text='<%#DataBinder.Eval(Container.DataItem,"last_exception_code")%>' Runat="server">
							</asp:Label>
						</ItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Pickup<br>Delivery Route">
						<HeaderStyle Width="5%" CssClass="gridHeadingSmall"></HeaderStyle>
						<ItemStyle CssClass="gridFieldSmall"></ItemStyle>
						<ItemTemplate>
							<asp:Label CssClass="gridLabelSmall" ID="pickup_route" Text='<%#DataBinder.Eval(Container.DataItem,"pickup_route")%>' Runat="server">
							</asp:Label><br/>
							<asp:Label CssClass="gridLabelSmall" ID="lblRouteCode" Text='<%#DataBinder.Eval(Container.DataItem,"route_code")%>' Runat="server">
							</asp:Label>
						</ItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Invoice Date">
						<HeaderStyle Width="5%" CssClass="gridHeadingSmall"></HeaderStyle>
						<ItemStyle CssClass="gridFieldSmall"></ItemStyle>
						<ItemTemplate>
							<asp:Label CssClass="gridLabelSmall" ID="lblInvoiceDate" Text='<%#DataBinder.Eval(Container.DataItem,"invoice_date","{0:dd/MM/yyyy HH:mm}")%>' Runat="server">
							</asp:Label>
						</ItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Close Status">
						<HeaderStyle Width="5%" CssClass="gridHeadingSmall"></HeaderStyle>
						<ItemStyle CssClass="gridFieldSmall"></ItemStyle>
						<ItemTemplate>
							<asp:Label CssClass="gridLabelSmall" ID="lblCloseStatus" Text='<%#DataBinder.Eval(Container.DataItem,"status_close")%>' Runat="server">
							</asp:Label>
						</ItemTemplate>
					</asp:TemplateColumn>
				</Columns>
				<PagerStyle NextPageText="Next" PrevPageText="Previous" HorizontalAlign="Left" CssClass="normalText"></PagerStyle>
			</asp:datagrid>
			<asp:button id="btnQuery" style="Z-INDEX: 102; LEFT: 6px; POSITION: absolute; TOP: 20px" runat="server" Text="Close" CssClass="queryButton"></asp:button>
			<asp:Label id="lblErrorMsg" style="Z-INDEX: 103; LEFT: 6px; POSITION: absolute; TOP: 54px" runat="server" CssClass="errorMsgColor" Height="33px" Width="862px"></asp:Label></form>
		</FORM>
	</body>
</HTML>
