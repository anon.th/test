using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.common.classes;
using com.common.DAL;
using com.common.RBAC;
using com.ties.DAL;
using com.common.util;
using com.ties.classes;
using com.common.applicationpages;
namespace com.ties
{
	/// <summary>
	/// Summary description for DispatchStatusUpdate.
	/// </summary>
	public class DispatchStatusUpdate : BasePopupPage
	{
		protected System.Web.UI.WebControls.Label lblDisSenderName;
		protected System.Web.UI.WebControls.Label lblSenderName;
		protected System.Web.UI.WebControls.Label lblDisBookDateTime;
		protected System.Web.UI.WebControls.Label lblBookDateTime;
		protected System.Web.UI.WebControls.Label lblDisBookNo;
		protected System.Web.UI.WebControls.Label lblBookNo;
		protected System.Web.UI.WebControls.Label lblStatus;
		protected System.Web.UI.WebControls.Label lblException;
		protected System.Web.UI.WebControls.Label lblRemarks;
		protected System.Web.UI.WebControls.Label lblStatusDateTime;
		protected System.Web.UI.WebControls.Label lblPersonInCharge;
		protected System.Web.UI.WebControls.TextBox txtRemarks;
		protected com.common.util.msTextBox txtStatusDateTime;
		protected System.Web.UI.WebControls.TextBox txtPersonInCharge;
		protected System.Web.UI.WebControls.Button btnSave;
		protected System.Web.UI.WebControls.TextBox txtStatus;
		protected System.Web.UI.WebControls.TextBox txtException;
		protected System.Web.UI.WebControls.Button btnStatusSrch;
		protected System.Web.UI.WebControls.Button btnExceptionSrch;
		protected System.Web.UI.WebControls.Button btnCancel;
		protected System.Web.UI.WebControls.Label lblAstPerson;
		protected System.Web.UI.WebControls.Label lblAstStatusDT;
		private SessionDS m_sdsDispatchUpdate = null;
		private String m_strAppID;
		protected System.Web.UI.WebControls.Label lblAstStatusCode;
		protected System.Web.UI.WebControls.Label lblReqPickupDateTime;
		protected System.Web.UI.WebControls.Label lblDisReqPickupDateTime;
		protected System.Web.UI.WebControls.Label lblActPickupDateTime;
		protected System.Web.UI.WebControls.Label lblDisActPickupDateTime;
		protected System.Web.UI.WebControls.Label lblErrMsg;
		
		private String m_strEnterpriseID;
		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			Utility utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings,Page.Session);
			m_strAppID = utility.GetAppID();
			m_strEnterpriseID = utility.GetEnterpriseID();
			m_sdsDispatchUpdate = (SessionDS)Session["SESSION_DS1"];

			
			if(!Page.IsPostBack)
			{
				
				String striRow = Request.Params["INFO_INDEX"];
				displayInfo(int.Parse(striRow));
				txtStatusDateTime.Text = DateTime.Now.ToString("dd/MM/yyyy HH:mm");
			}
		}
		/// <summary>
		/// displays information at the top
		/// </summary>
		/// <param name="rowIndex"></param>
		private void displayInfo(int rowIndex)
		{
			DataRow dr = m_sdsDispatchUpdate.ds.Tables[0].Rows[rowIndex];

			lblDisBookNo.Text = dr["booking_no"].ToString();
			lblDisSenderName.Text = dr["sender_name"].ToString();
			if(dr["booking_datetime"]!=System.DBNull.Value && dr["booking_datetime"].ToString()!="")
			{
				DateTime dtBookDateTime = (DateTime)dr["booking_datetime"];
				lblDisBookDateTime.Text = dtBookDateTime.ToString("dd/MM/yyyy HH:mm");
			}
			if(dr["act_pickup_datetime"]!=System.DBNull.Value && dr["act_pickup_datetime"].ToString()!="")
			{
				DateTime dtActPickupDateTime = (DateTime)dr["act_pickup_datetime"];
				lblDisActPickupDateTime.Text = dtActPickupDateTime.ToString("dd/MM/yyyy HH:mm");
			}
			if(dr["req_pickup_datetime"]!=System.DBNull.Value && dr["req_pickup_datetime"].ToString()!="")
			{
				DateTime dtReqPickupDateTime = (DateTime)dr["req_pickup_datetime"];
				lblDisReqPickupDateTime.Text = dtReqPickupDateTime.ToString("dd/MM/yyyy HH:mm");
			}
		}
		/// <summary>
		/// to open a popup window
		/// </summary>
		/// <param name="strUrl"></param>
		private void OpenWindowpage(String strUrl)
		{
			ArrayList paramList = new ArrayList();
			paramList.Add(strUrl);
			String sScript = Utility.GetScript("openLastChildWindow.js",paramList);
			Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
		}
		/// <summary>
		/// to retreive text fields  and store to a dataset
		/// </summary>
		private void UpdateDispatchInfo()
		{
			DataSet dsDispatch = new DataSet();
			DataTable dtDispatch = new DataTable();
			dtDispatch.Columns.Add(new DataColumn("booking_no", typeof(string)));
			dtDispatch.Columns.Add(new DataColumn("status_code", typeof(string)));
			dtDispatch.Columns.Add(new DataColumn("exception_code", typeof(string)));
			dtDispatch.Columns.Add(new DataColumn("status_datetime", typeof(DateTime)));
			dtDispatch.Columns.Add(new DataColumn("person_incharge", typeof(string)));
			dtDispatch.Columns.Add(new DataColumn("remark", typeof(string)));
			dtDispatch.Columns.Add(new DataColumn("UserUpdate", typeof(string)));  //Jeab 01 Sep 2011
			dtDispatch.Columns.Add(new DataColumn("UserLoc", typeof(string)));  //Jeab 01 Sep 2011

			DataRow dr = dtDispatch.NewRow();

			dr["booking_no"] = lblDisBookNo.Text;
			dr["status_code"] = txtStatus.Text;
			dr["exception_code"] = txtException.Text;
			dr["status_datetime"] = DateTime.ParseExact(txtStatusDateTime.Text,"dd/MM/yyyy HH:mm",null);
			dr["person_incharge"] = txtPersonInCharge.Text;
			dr["remark"] = txtRemarks.Text;
			dr["UserUpdate"] =Session["userID"];  //Jeab 01 Sep 2011
			dr["UserLoc"] =Session["UserLocation"];  //Jeab 01 Sep 2011

			DateTime bkgDateTime =DateTime.ParseExact(lblDisBookDateTime.Text,"dd/MM/yyyy HH:mm",null);
			DateTime statusDateTime =DateTime.ParseExact(txtStatusDateTime.Text,"dd/MM/yyyy HH:mm",null);
			if(bkgDateTime.CompareTo(statusDateTime)>0)
			{
				lblErrMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"STATUS_DT_LESS_BKG_DT",utility.GetUserCulture());

				return;
			}

			dtDispatch.Rows.Add(dr);
			dsDispatch.Tables.Add(dtDispatch);
			try
			{
//				DispatchTrackingMgrDAL.UpdateDispatch(dsDispatch, m_strAppID, m_strEnterpriseID);
//					RefreshQueryWindow();
				//Jeab 5 Sep 2011
				int iUpdate = DispatchTrackingMgrDAL.UpdateDispatch(dsDispatch, m_strAppID, m_strEnterpriseID);
				if(iUpdate==0)
				{
					lblErrMsg.Text  = "Not  allow to udpate";
				}
				else
				{
					RefreshQueryWindow();
				}  //Jeab 5 Sep 2011  =========> End
				//closeWindow();
			}
			catch(ApplicationException err)
			{
				int errNum = err.Message.IndexOf("duplicate",0);
				if(errNum>0)
				{
					lblErrMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"DUP_KEY_FOUND",utility.GetUserCulture());

				}
			}
			catch(Exception err)
			{
				String strErr = err.Message;
				lblErrMsg.Text = strErr;
			}

		}
		//check if all mandatory fields entered
		private bool isAllRequiredFieldsEntered()
		{
			bool isInvalidStatus=false;
			bool isInvalidException=false;
			bool isAllowUpdate = false;

			if(txtStatus.Text == "")
			{
				lblAstStatusCode.Text = "*";
				
			}
			else
			{
				lblAstStatusCode.Text = "";

				StatusCodeDesc status = TIESUtility.GetStatusCodeDesc(m_strAppID,m_strEnterpriseID,txtStatus.Text,"P");
				if(status.strStatusCode==null)
				{
					lblAstStatusCode.Text = "*";
					isInvalidStatus=true;
					
				}
				if(txtException.Text!="")
				{
					ExceptionCodeDesc exception = TIESUtility.GetExceptionCodeDesc(m_strAppID,m_strEnterpriseID,txtException.Text,txtStatus.Text);
					if(exception.strExceptionCode==null)
					{
						isInvalidException=true;
					}
				}
			}

			if(txtPersonInCharge.Text == "")
			{
				lblAstPerson.Text = "*";
				
			}
			else
			{
				lblAstPerson.Text = "";
			}

			if(txtStatusDateTime.Text == "")
			{
				lblAstStatusDT.Text = "*";
				
			}
			else
			{
				lblAstStatusDT.Text = "";
			}

			if(lblAstStatusCode.Text == "")
			{
				if (txtStatus.Text.Trim() == "PUX") 
					isAllowUpdate = TIESUtility.IsStatusCodeExist(m_strAppID, m_strEnterpriseID, txtStatus.Text.Trim(), utility.GetUserID());
				else
					isAllowUpdate = true;
			}

			if(lblAstStatusCode.Text == "" && lblAstPerson.Text == "" && lblAstStatusDT.Text =="" && !isInvalidException && isAllowUpdate)
			{
				lblErrMsg.Text="";
				return true;
			}
			else if(isInvalidStatus)
			{
				lblErrMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"INVALID_STATUS_CODE",utility.GetUserCulture());

			}
			else if(isInvalidException)
			{
				lblErrMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"INVALID_EXCEPTION_CODE",utility.GetUserCulture());
			}
			else if (!isAllowUpdate)
			{
				lblErrMsg.Text = "This status (PUX) allow only be updated by CS user.";
			}
			else
			{
				lblErrMsg.Text= Utility.GetLanguageText(ResourceType.UserMessage,"PLS_ENTER_MARKED_FIELD",utility.GetUserCulture());
			}
				return false;
		}
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnStatusSrch.Click += new System.EventHandler(this.btnStatusSrch_Click);
			this.btnExceptionSrch.Click += new System.EventHandler(this.btnExceptionSrch_Click);
			this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
			this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		/// <summary>
		///  popup a status code window
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void btnStatusSrch_Click(object sender, System.EventArgs e)
		{
			OpenWindowpage("StatusCodePopup.aspx?FORMID=DispatchStatusUpdate&EXCEPTIONCODE="+txtException.Text+"&STATUSCODE="+txtStatus.Text+"&STATUSTYPE=P ");
	
		}
		/// <summary>
		///  popup a status code window
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void btnExceptionSrch_Click(object sender, System.EventArgs e)
		{
			OpenWindowpage("ExceptionCodePopup.aspx?FORMID=DispatchStatusUpdate&EXCEPTIONCODE="+txtException.Text+"&STATUSCODE="+txtStatus.Text);
	
		}
		/// <summary>
		///  do checking and validation before inserting to database 
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void btnSave_Click(object sender, System.EventArgs e)
		{	
			if(isAllRequiredFieldsEntered())
			{
				UpdateDispatchInfo();
			}
		}

		/// <summary>
		/// close current window
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void btnCancel_Click(object sender, System.EventArgs e)
		{
			closeWindow();
		}
		/// <summary>
		/// to close current window
		/// </summary>
		private void closeWindow()
		{
			String sScript = "";
			sScript += "<script language=javascript>";
			sScript += "  window.opener;" ;
			sScript += "  window.close();";
			sScript += "</script>";
			Response.Write(sScript);
		}
		private void RefreshQueryWindow()
		{
			Session["toRefresh"] = true;
			
			String sScript = "";
			sScript += "<script language=javascript>";
			sScript += "var sURL = unescape(window.opener.location.pathname); ";
			sScript += "  window.opener.location.replace(sURL);" ;
			sScript += "  window.close();";
			sScript += "</script>";
			Response.Write(sScript);
			
		}

		
	}
}
