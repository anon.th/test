using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.common.util;
using com.common.classes;
using com.common.DAL;
using com.ties.DAL;
using System.Text;
using com.common.applicationpages;

namespace com.ties
{
	/// <summary>
	/// Summary description for RouteCodePopup.
	/// </summary>
	public class RouteCodePopup : BasePopupPage
	{
		protected System.Web.UI.WebControls.DataGrid dgRouteMaster;
		protected System.Web.UI.WebControls.Button btnSearch;
		protected System.Web.UI.WebControls.Button btnOk;
		//protected Utility utility = null;
		//Utility utility = null;
		String strAppID = "";
		String strEnterpriseID = "";
		SessionDS m_sdsRouteCode = null;
		String strFormID = null;
		String strRouteClient=null;
		String strRouteCode=null;
		String strRouteDescClient=null;
		String strRouteOriginClient=null;
		String strRouteDestinationClient=null;
		String strPathCode=null;
		String strFGNo=null;
		String strBookNo=null;
		String strSendZip=null;
		String strUserID=null;

		protected System.Web.UI.WebControls.Label lblPathCode;
		protected System.Web.UI.WebControls.TextBox txtPathCode;
		String strDLDate=null;

		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			//utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings,Page.Session);
			strAppID = utility.GetAppID();
			strEnterpriseID = utility.GetEnterpriseID();
			strFormID = Request.Params["FORMID"];
			strRouteClient = Request.Params["ROUTECODE_TEXT"];
			strRouteDescClient = Request.Params["ROUTEDESC"];
			strRouteOriginClient = Request.Params["ROUTEORIG"];
			strRouteDestinationClient = Request.Params["ROUTEDEST"];
			strPathCode = Request.Params["PATHCODE"];
			strFGNo = Request.Params["FGNO"];
			strDLDate = Request.Params["DLDATE"];
			strBookNo = Request.Params["BOOKNO"];
			strSendZip = Request.Params["SENDZIP"];
			
			strUserID =Convert.ToString(Session["userID"]);

			if(!Page.IsPostBack)
			{
				strRouteCode = Request.Params["ROUTECODE"];
				
				txtPathCode.Text = strRouteCode;
				m_sdsRouteCode = GetEmptyZoneCodeDS(0);
				BindGrid();
			}
			else
			{
				m_sdsRouteCode = (SessionDS)ViewState["ROUTECODE_DS"];
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			base.OnInit(e);
			InitializeComponent();
			
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
			this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		public void Paging(Object sender, DataGridPageChangedEventArgs e)
		{
			//runs when one of the page control is clicked
			//update the current page from the parameter values
			//dgRouteMaster.CurrentPageIndex = e.NewPageIndex;
			//bind data to the grid
			//RefreshData();
			dgRouteMaster.CurrentPageIndex = e.NewPageIndex;
			ShowCurrentPage();
		}

		private void btnSearch_Click(object sender, System.EventArgs e)
		{
			StringBuilder strQry = new StringBuilder();
			strQry.Append(string.Format("SELECT b.DC, a.route_code, a.route_description FROM GetRoutes('{0}', '{1}') a ",strEnterpriseID,strUserID));
			if (strSendZip==null || strSendZip == "")
			{
				strQry.Append(string.Format("INNER JOIN GetZipcodeInfo(1, '{0}', '{1}') b ",strEnterpriseID, null));
			}
			else
			{
				strQry.Append(string.Format("INNER JOIN GetZipcodeInfo(1, '{0}', '{1}') b ",strEnterpriseID, strSendZip));
			}
			
			strQry.Append(" ON b.DC = a.origin_DC ");
			if (txtPathCode.Text != "")
			{
				strQry.Append(" WHERE a.route_code like '%");
				strQry.Append(txtPathCode.Text.Trim());
				strQry.Append("%'");
			}
			strQry.Append(" ORDER BY a.route_code");

			String strSQLQuery = strQry.ToString();
			//RefreshData();
			ViewState["SQL_QUERY"] = strSQLQuery;
			dgRouteMaster.CurrentPageIndex = 0;

			ShowCurrentPage();
		}

		private void btnOk_Click(object sender, System.EventArgs e)
		{
			// Get the value and return it to the parent window
			String sScript = "";
			sScript += "<script language=javascript>";
			sScript += "  window.close();";
			sScript += "</script>";
			Response.Write(sScript);
		}

		public void dgRouteMaster_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			// Get the value and return it to the parent window
			int iSelIndex = dgRouteMaster.SelectedIndex;
			DataGridItem dgRow = dgRouteMaster.Items[iSelIndex];
			String strPathCode = dgRow.Cells[1].Text;
			String strPathDesc = dgRow.Cells[2].Text;
			String strOrig = "";
			String strDest = "";

			if (strFormID != "ShipmentUpdateRoute")
			{
				
				String sScript = "";
				sScript += "<script language=javascript>";
				sScript += "  window.opener."+strFormID+"."+strRouteClient+".value = \""+strPathCode+"\";";
				sScript += "  window.close();";
				sScript += "</script>";
				Response.Write(sScript);
			}
			else
			{
				//		DataSet dManifestDetail = ShipmentUpdateMgrDAL.getRouteDetails(strAppID, strEnterpriseID,
				//				strPathCode, strFGNo, System.DateTime.ParseExact(strDLDate,"dd/MM/yyyy HH:mm",null));

				//		if (dManifestDetail.Tables[0].Rows.Count > 0)
				//		{
				//			strOrig = (String)dManifestDetail.Tables[0].Rows[0]["origin_state_code"];
				//			strDest = (String)dManifestDetail.Tables[0].Rows[0]["destination_state_code"];
				//		}
				DataSet dsDelPath = ShipmentUpdateMgrDAL.getRouteDetails(strAppID, strEnterpriseID, strPathCode);

				if (dsDelPath.Tables[0].Rows.Count > 0)
				{
					strOrig = dsDelPath.Tables[0].Rows[0]["origin_station"].ToString();
					strDest = dsDelPath.Tables[0].Rows[0]["destination_station"].ToString();
				}
				

				String sScript = "";
				sScript += "<script language=javascript>";
				sScript += "  window.opener."+strFormID+"."+strRouteClient+".value = \""+strPathCode+"\";";
				sScript += "  window.opener."+strFormID+"."+strRouteDescClient+".value = \""+strPathDesc+"\";";
				//sScript += "  window.opener."+strFormID+"."+strRouteDescClient+".value = \""+strRouteDesc+"\";";
				sScript += "  window.opener."+strFormID+"."+strRouteOriginClient+".value = \""+strOrig+"\";";
				sScript += "  window.opener."+strFormID+"."+strRouteDestinationClient+".value = \""+strDest+"\";";
				sScript += "  window.close();";
				sScript += "</script>";
				Response.Write(sScript);
			}

		}

		public static SessionDS GetEmptyZoneCodeDS(int intRows)
		{
			DataTable dtRoute = new DataTable();
			dtRoute.Columns.Add(new DataColumn("DC", typeof(string)));
			dtRoute.Columns.Add(new DataColumn("route_code", typeof(string)));
			dtRoute.Columns.Add(new DataColumn("route_description", typeof(string)));

			for(int i=0; i < intRows; i++)
			{
				DataRow drEach = dtRoute.NewRow();
				drEach[0] = "";
				drEach[1] = "";
				drEach[2] = "";
				dtRoute.Rows.Add(drEach);
			}

			DataSet dsRouteFields = new DataSet();
			dsRouteFields.Tables.Add(dtRoute);
			SessionDS sessionDS = new SessionDS();
			sessionDS.ds = dsRouteFields;
			sessionDS.DataSetRecSize = dsRouteFields.Tables[0].Rows.Count;
			sessionDS.QueryResultMaxSize = sessionDS.DataSetRecSize;
			return sessionDS;
		}

		private void BindGrid()
		{
			dgRouteMaster.VirtualItemCount = System.Convert.ToInt32(m_sdsRouteCode.QueryResultMaxSize);
			dgRouteMaster.DataSource = m_sdsRouteCode.ds;
			dgRouteMaster.DataBind();
			ViewState["ROUTECODE_DS"] = m_sdsRouteCode;
		}

		private void ShowCurrentPage()
		{
			int iStartIndex = dgRouteMaster.CurrentPageIndex * dgRouteMaster.PageSize;
			String sqlQuery = (String)ViewState["SQL_QUERY"];
			m_sdsRouteCode = GetRouteCodeDS(iStartIndex,dgRouteMaster.PageSize,sqlQuery);
			decimal pgCnt = (m_sdsRouteCode.QueryResultMaxSize - 1)/dgRouteMaster.PageSize;
			if(pgCnt < dgRouteMaster.CurrentPageIndex)
			{
				dgRouteMaster.CurrentPageIndex = System.Convert.ToInt32(pgCnt);
			}
			dgRouteMaster.SelectedIndex = -1;
			dgRouteMaster.EditItemIndex = -1;

			BindGrid();
			getPageControls(Page);
		}

		private SessionDS GetRouteCodeDS(int iCurrent, int iDSRecSize, String strQuery)
		{
			SessionDS sessionDS = null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("RouteCodePopup.aspx.cs","GetRouteCodeDS","EDB101","DbConnection object is null!!");
				return sessionDS;
			}
			sessionDS = dbCon.ExecuteQuery(strQuery,iCurrent,iDSRecSize,"RouteCode");
			return  sessionDS;
		}


	}
}
