using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.common.util;
using com.common.classes;
using com.common.DAL;
using System.Text;
using com.common.applicationpages;

namespace com.ties
{
	/// <summary>
	/// Summary description for ZonePopup.
	/// </summary>
	public class ZonePopup : BasePopupPage
	{
		protected System.Web.UI.WebControls.DataGrid dgZoneMaster;
		protected System.Web.UI.WebControls.TextBox txtZoneCode;
		protected System.Web.UI.WebControls.Label lblZoneCode;
		//Utility utility = null;
		String strAppID = "";
		String strEnterpriseID = "";
		protected String strZoneID   = null;
		protected String strZoneName = null;
		protected System.Web.UI.WebControls.Button btnSearch;
		protected System.Web.UI.WebControls.Button btnOk;
		SessionDS m_sdsZoneCode = null;
		String strFormID = null;

		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
		//	utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings,Page.Session);
			strAppID = utility.GetAppID();
			strEnterpriseID = utility.GetEnterpriseID();

			strZoneID = Request.Params["ZONECODE"];
			strZoneName = Request.Params["ZONEDESC"];
			strFormID = Request.Params["FORMID"];

			if(!strFormID.Equals("ESA_Sectors")&&!strFormID.Equals("ImportESA_ServiceType"))
			{
				lblZoneCode.Text = "Zone code";
				this.dgZoneMaster.Columns[0].HeaderText = "Zone Code";
				this.dgZoneMaster.Columns[0].HeaderText = "Zone Name";
			}
			else
			{
				lblZoneCode.Text = "ESA Sector";
				this.dgZoneMaster.Columns[0].HeaderText = "ESA Sector Code";
				this.dgZoneMaster.Columns[0].HeaderText = "ESA Sector Name";
			}
			if(!Page.IsPostBack)
			{
				txtZoneCode.Text = Request.Params["ZONECODE_TEXT"];
				m_sdsZoneCode = GetEmptyZoneCodeDS(0);
				BindGrid();
			}
			else
			{
				m_sdsZoneCode = (SessionDS)ViewState["ZONECODE_DS"];
			}

		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			base.OnInit(e);
			InitializeComponent();
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
			this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		public void Paging(Object sender, DataGridPageChangedEventArgs e)
		{
			//runs when one of the page control is clicked
			//update the current page from the parameter values
			//dgZoneMaster.CurrentPageIndex = e.NewPageIndex;
			//bind data to the grid
			//RefreshData();
			dgZoneMaster.CurrentPageIndex = e.NewPageIndex;
			ShowCurrentPage();
		}

		private void btnSearch_Click(object sender, System.EventArgs e)
		{
			StringBuilder strQry = new StringBuilder();
			if(!strFormID.Equals("ESA_Sectors")&&!strFormID.Equals("ImportESA_ServiceType"))
			{
				strQry.Append("select zone_code, zone_name from zone where applicationid='");
				strQry.Append(strAppID);
				strQry.Append("' and enterpriseid = '");
				strQry.Append(strEnterpriseID);
				strQry.Append("'");
				if (txtZoneCode.Text.ToString() != null && txtZoneCode.Text.ToString() != "")
				{
					strQry.Append(" and zone_code like '%");
					strQry.Append(Utility.ReplaceSingleQuote(txtZoneCode.Text.ToString()));
					strQry.Append("%'");
				}
			}
			else
			{
				strQry.Append("select code_str_value as zone_code,code_text as zone_name from core_system_code where codeid = 'ESA_Sector' order by code_str_value ");
			}

			String strSQLQuery = strQry.ToString();
			//RefreshData();
			ViewState["SQL_QUERY"] = strSQLQuery;
			dgZoneMaster.CurrentPageIndex = 0;

			ShowCurrentPage();
		}

		private void btnOk_Click(object sender, System.EventArgs e)
		{
			// Get the value and return it to the parent window
			String sScript = "";
			sScript += "<script language=javascript>";
			sScript += "  window.close();";
			sScript += "</script>";
			Response.Write(sScript);
		}

		public void dgZoneMaster_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			// Get the value and return it to the parent window
			int iSelIndex = dgZoneMaster.SelectedIndex;
			DataGridItem dgRow = dgZoneMaster.Items[iSelIndex];
			String strZoneCode = dgRow.Cells[0].Text;
			String strZoneDesc = dgRow.Cells[1].Text;
			
			String sScript = "";
			sScript += "<script language=javascript>";
			//sScript += "  window.opener.ZipCode."+strZoneID+".value = '"+strZoneCode+"';" ;
			sScript += "  window.opener."+strFormID+"."+strZoneID+".value = '"+strZoneCode+"';" ;
			if(strFormID == "AgentProfile")
			{
				sScript += "  window.opener."+strFormID+"."+strZoneName+".value = '"+strZoneDesc+"';" ;
			}
			//strFormID
			sScript += "  window.close();";
			sScript += "</script>";
			Response.Write(sScript);
		}

		public static SessionDS GetEmptyZoneCodeDS(int intRows)
		{
			DataTable dtZone = new DataTable();
			dtZone.Columns.Add(new DataColumn("zone_code", typeof(string)));
			dtZone.Columns.Add(new DataColumn("zone_name", typeof(string)));

			for(int i=0; i < intRows; i++)
			{
				DataRow drEach = dtZone.NewRow();
				drEach[0] = "";
				drEach[1] = "";
				dtZone.Rows.Add(drEach);
			}

			DataSet dsZoneFields = new DataSet();
			dsZoneFields.Tables.Add(dtZone);
			SessionDS sessionDS = new SessionDS();
			sessionDS.ds = dsZoneFields;
			sessionDS.DataSetRecSize = dsZoneFields.Tables[0].Rows.Count;
			sessionDS.QueryResultMaxSize = sessionDS.DataSetRecSize;
			return sessionDS;
		}

		private void BindGrid()
		{
			dgZoneMaster.VirtualItemCount = System.Convert.ToInt32(m_sdsZoneCode.QueryResultMaxSize);
			dgZoneMaster.DataSource = m_sdsZoneCode.ds;
			dgZoneMaster.DataBind();
			ViewState["ZONECODE_DS"] = m_sdsZoneCode;

			//dgZoneMaster.DataSource = dsZone;
			//dgZoneMaster.DataBind();
		}

		private void ShowCurrentPage()
		{
			int iStartIndex = dgZoneMaster.CurrentPageIndex * dgZoneMaster.PageSize;
			String sqlQuery = (String)ViewState["SQL_QUERY"];
			m_sdsZoneCode = GetZoneCodeDS(iStartIndex,dgZoneMaster.PageSize,sqlQuery);
			decimal pgCnt = (m_sdsZoneCode.QueryResultMaxSize - 1)/dgZoneMaster.PageSize;
			if(pgCnt < dgZoneMaster.CurrentPageIndex)
			{
				dgZoneMaster.CurrentPageIndex = System.Convert.ToInt32(pgCnt);
			}
			dgZoneMaster.SelectedIndex = -1;
			dgZoneMaster.EditItemIndex = -1;

			BindGrid();
			getPageControls(Page);
		}

		private SessionDS GetZoneCodeDS(int iCurrent, int iDSRecSize, String strQuery)
		{
			SessionDS sessionDS = null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("ZoneCodePopup.aspx.cs","GetZoneCodeDS","EDB101","DbConnection object is null!!");
				return sessionDS;
			}
			sessionDS = dbCon.ExecuteQuery(strQuery,iCurrent,iDSRecSize,"ZoneCode");
			return  sessionDS;
		}


	}
}
