using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.common.RBAC;
using com.common.DAL;
using com.common.util;
using com.common.classes;
using Microsoft.Web.UI.WebControls;
using com.common.applicationpages;

namespace TIES.WebUI
{
	/// <summary>
	/// Summary description for UserProfile11.
	/// </summary>
	public class UserProfile : BasePage
	{
		protected System.Web.UI.WebControls.Button btnRemove;
		protected System.Web.UI.WebControls.ListBox lbRolesAdded;
		protected System.Web.UI.WebControls.Label lblDelRec;
		protected System.Web.UI.WebControls.CheckBox chkGrantsDel;
		protected System.Web.UI.WebControls.Label lblUpdRec;
		protected System.Web.UI.WebControls.CheckBox chkGrantsUpd;
		protected System.Web.UI.WebControls.Label lblInsRec;
		protected System.Web.UI.WebControls.CheckBox chkGrantsIns;
		protected System.Web.UI.WebControls.ListBox lbRoleMaster;
		protected System.Web.UI.WebControls.Button btnAdd;
		protected System.Web.UI.WebControls.Button btnRemoveAll;
		protected System.Web.UI.WebControls.Label lblMessages;
		protected System.Web.UI.WebControls.Button btnSave;
		protected Microsoft.Web.UI.WebControls.TreeView funcTree;
		protected System.Web.UI.WebControls.Button btnAddAll;
		private   Microsoft.Web.UI.WebControls.TreeNode treeNode;
		private   Microsoft.Web.UI.WebControls.TreeNode tiesNode;
		ArrayList childList = new ArrayList();
		private   String m_strAppID = null;
		private   String m_strEnterpriseID = null;
		private String m_strUserID = null;
		//Utility   utility = null;
		ArrayList selItemArray = new ArrayList();
		private   String strCreatedUserID = null;
		bool      isChecked = false;
		private   long selRoleID = 0;
		private   String selUserID = null;
		protected com.common.util.msTextBox txtUserID;
		protected System.Web.UI.WebControls.Label lblUserID;
		protected System.Web.UI.WebControls.Button btnResetPswd;
		protected System.Web.UI.WebControls.Label lblUserPswd;
		protected com.common.util.msTextBox txtUserName;
		protected System.Web.UI.WebControls.Label lblUsrName;
		protected System.Web.UI.WebControls.DropDownList ddbCulture;
		protected System.Web.UI.WebControls.Label Label2;
		protected System.Web.UI.WebControls.TextBox txtEmail;
		protected System.Web.UI.WebControls.Label lblEmail;
		protected com.common.util.msTextBox txtDepartment;
		protected System.Web.UI.WebControls.Label lblDept;
		protected System.Web.UI.WebControls.DropDownList ddbUserType;
		protected System.Web.UI.WebControls.Label lblUserType;
		protected System.Web.UI.HtmlControls.HtmlTable tblRoleInfo;
		protected System.Web.UI.WebControls.Label lblTitle;
		protected System.Web.UI.WebControls.RegularExpressionValidator regEmailValidate;
		protected System.Web.UI.WebControls.ValidationSummary reqSummary;
		protected System.Web.UI.HtmlControls.HtmlTable tblPermissions;
		protected System.Web.UI.HtmlControls.HtmlTable tblAdditionalRoles;
		protected System.Web.UI.WebControls.Label lblUsrInfos;
		protected System.Web.UI.WebControls.Label lblUsrInfo;
		protected System.Web.UI.WebControls.Label lblAddFnRol;
		com.common.classes.User user = new com.common.classes.User();

		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			//utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings,Page.Session);
			m_strAppID = utility.GetAppID();
			m_strEnterpriseID = utility.GetEnterpriseID();
			m_strUserID = utility.GetUserID();
			lbRoleMaster.Enabled=false;
			lbRolesAdded.Enabled=false;
			funcTree.Enabled=true;			
			m_strUserID=m_strUserID.ToUpper();
			txtUserID.Text=m_strUserID;
			if(!Page.IsPostBack)
			{
				ViewState["isTextChange"] = false;
				ViewState["iTotalUsers"] = 0;
				ViewState["iPageIndex"] = 0;
				ViewState["isRolesDeleted"] = false;
				ViewState["isQueryMode"] = false;
				ViewState["isDirty"] = false;
				ViewState["UMOperation"] = "None";
				ViewState["RemoveModules"] = false;
				ViewState["NoModifyIDU"] = false;

				LoadUserTypeList();
				LoadUserCultureList();
				txtUserID.Enabled = false;
				btnResetPswd.Text =Utility.GetLanguageText(ResourceType.ScreenLabel,"Reset Password",utility.GetUserCulture());
				btnResetPswd.Enabled = false;
				btnRemove.Enabled = false;
				btnAdd.Enabled = false;
				btnRemoveAll.Enabled = false;
				btnAddAll.Enabled = false;

				//txtUsrPswd.Enabled = true;
				//funcTree.Enabled = false;
				chkGrantsDel.Checked = false;
				chkGrantsIns.Checked = false;
				chkGrantsUpd.Checked = false;
				chkGrantsDel.Enabled = false;
				chkGrantsIns.Enabled = false;
				chkGrantsUpd.Enabled = false;
				lblMessages.Text = "";
				
				Module module = new Module();
				module.ModuleId = "root";
				treeNode = BuildModulesTree(module);
				tiesNode =(TreeNode)treeNode.Nodes[0].Clone();				
				funcTree.Nodes.AddAt(0,tiesNode);
				GetDataIntoList();

				user = RBACManager.GetUser(m_strAppID, m_strEnterpriseID, m_strUserID);		
				if(user != null)
				{
					txtUserName.Text = user.UserName;
					// Mohan, 21/11/02
					m_strUserID=user.UserID.ToUpper();				
					if (m_strUserID=="ADMINISTRATOR")
					{					
						txtUserName.Text=utility.GetEnterpriseID();
					}				
					btnResetPswd.Enabled = true;				
					txtDepartment.Text = user.UserDepartment;
					txtEmail.Text = user.UserEmail;
					ddbUserType.SelectedItem.Selected = false;
					ddbUserType.Items.FindByValue(user.UserType).Selected = true;
					ddbCulture.SelectedItem.Selected = false;
					ddbCulture.Items.FindByValue(user.UserCulture).Selected = true;
				
					ArrayList userList = (ArrayList)RBACManager.GetUserProfile(m_strAppID, m_strEnterpriseID,user.UserID);				
					if ((userList != null)&& (userList.Count<=0))
					{
						lblMessages.Text = Utility.GetLanguageText(ResourceType.UserMessage,"NO_RECORDS_FOUND",utility.GetUserCulture());				
					}
					else if((userList != null)&& (userList.Count>0))
					{
						lblMessages.Text = "";
						ViewState["iTotalUsers"] = userList.Count;
						Session.Add("UserArray",userList);
						DisplayUsers(0);
						ViewState["iPageIndex"] = 0;					
					}	
					if (m_strUserID=="ADMINISTRATOR")
					{
						SelectAllNodes(tiesNode,true);					//Select all nodes if user is ADMINISTRATOR
					}
				}
			}
			else
			{
				tiesNode = funcTree.Nodes[0];
			}			

		}
		public void LoadUserTypeList()
		{
			ListItem defItem = new ListItem();
			defItem.Text = "User Type";
			defItem.Value = "0";
			ddbUserType.Items.Add(defItem);
			ArrayList systemCodes = Utility.GetCodeValues(m_strAppID,utility.GetUserCulture(),"user_type",CodeValueType.StringValue);
			foreach(SystemCode sysCode in systemCodes)
			{	
				ListItem lstItem = new ListItem();
				lstItem.Text = sysCode.Text;
				lstItem.Value = sysCode.StringValue;
				ddbUserType.Items.Add(lstItem);
			}
		}

		public void LoadUserCultureList()
		{
			ListItem defItem = new ListItem();
			defItem.Text = "User Culture";
			defItem.Value = "0";
			ddbCulture.Items.Add(defItem);
			ArrayList systemCodes = Utility.GetCodeValues(m_strAppID,utility.GetUserCulture(),"user_culture",CodeValueType.StringValue);
			foreach(SystemCode sysCode in systemCodes)
			{	
				ListItem lstItem = new ListItem();
				lstItem.Text = sysCode.Text;
				lstItem.Value = sysCode.StringValue;
				ddbCulture.Items.Add(lstItem);
			}
		}

		public TreeNode BuildModulesTree(Module parentModule)
		{
			TreeNode node = new TreeNode();
			if(parentModule.ModuleName !=null)
			{
				node.Text = Utility.GetLanguageText(ResourceType.ModuleName,parentModule.ModuleName,utility.GetUserCulture());
			}
			
			if(parentModule.ModuleIconImage != null)
			{
				node.ImageUrl = parentModule.ModuleIconImage;
			}

			node.CheckBox = true;
			node.NodeData = "IDU"+":"+parentModule.ModuleId;
			String moduleID = parentModule.ModuleId;
			childList = (ArrayList)RBACManager.GetCoreModules(m_strAppID,moduleID);
			int cnt = childList.Count;
				
			foreach(Module child in childList)
			{
				node.Nodes.Add(BuildModulesTree(child));
			}
			return node;
		}
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			base.OnInit(e);
			InitializeComponent();
			
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
			this.chkGrantsIns.CheckedChanged += new System.EventHandler(this.chkGrantsIns_CheckedChanged);
			this.chkGrantsUpd.CheckedChanged += new System.EventHandler(this.chkGrantsUpd_CheckedChanged);
			this.chkGrantsDel.CheckedChanged += new System.EventHandler(this.chkGrantsDel_CheckedChanged);
			this.btnResetPswd.Click += new System.EventHandler(this.btnResetPswd_Click);
			this.txtUserName.TextChanged += new System.EventHandler(this.txtUserName_TextChanged);
			this.funcTree.SelectedIndexChange += new Microsoft.Web.UI.WebControls.SelectEventHandler(this.funcTree_SelectedIndexChange);
			this.funcTree.Check += new Microsoft.Web.UI.WebControls.ClickEventHandler(this.funcTree_SelectedCheckChange);
			this.btnRemove.Click += new System.EventHandler(this.btnRemove_Click);
			this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
			this.btnRemoveAll.Click += new System.EventHandler(this.btnRemoveAll_Click);
			this.btnAddAll.Click += new System.EventHandler(this.btnAddAll_Click);
			this.ID = "UserProfile";
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private void funcTree_SelectedIndexChange(System.Object sender ,TreeViewSelectEventArgs e)
		{

			String strParentNodeValue = null;
			String strSelectedTreeNode = null;
			String strIndex = funcTree.SelectedNodeIndex;
			//int index = int.Parse(funcTree.SelectedNodeIndex);
			TreeNodeCollection nodeCollection =  funcTree.GetNodeFromIndex(strIndex).Nodes;
			//			if(funcTree.Nodes[index].Checked)
			//			{
			//				funcTree.Nodes[index].Checked = false;
			//			}
			//			else
			//			{
			//				funcTree.Nodes[index].Checked = true;
			//			}
			int nodeCnt = nodeCollection.Count;
			
			if (nodeCnt > 0)
			{
				chkGrantsDel.Checked = false;
				chkGrantsIns.Checked = false;
				chkGrantsUpd.Checked = false;
				chkGrantsDel.Enabled = false;
				chkGrantsIns.Enabled = false;
				chkGrantsUpd.Enabled = false;
			}
			else if (nodeCnt == 0)
			{
				chkGrantsDel.Enabled = true;
				chkGrantsIns.Enabled = true;
				chkGrantsUpd.Enabled = true;
				strSelectedTreeNode = funcTree.GetNodeFromIndex(strIndex).NodeData;
				if (strIndex.Equals("0"))
				{
					strParentNodeValue = "root";
				}
				else
				{
					TreeNode parentNode = (TreeNode)funcTree.GetNodeFromIndex(strIndex).Parent;
					strParentNodeValue = parentNode.NodeData;	
				}
					
				String strModuleRights = strSelectedTreeNode.Substring(0,strSelectedTreeNode.IndexOf(":"));
				strModuleRights = strModuleRights.Substring(0,(strModuleRights.Length));
					
				try
				{
					if((strModuleRights.IndexOf("I"))>=0)
					{
						chkGrantsIns.Checked = true;
					}
					else
					{
						chkGrantsIns.Checked = false;
					}

				}
				catch(Exception indexOutOfBoundExp)
				{
					String msg = indexOutOfBoundExp.ToString();
					chkGrantsIns.Checked = false;
				}

				try
				{
					if((strModuleRights.IndexOf("U"))>=0)
					{
						chkGrantsUpd.Checked = true;
					}
					else
					{
						chkGrantsUpd.Checked = false;
					}

				}
				catch(Exception indexOutOfBoundUExp)
				{
					String msg = indexOutOfBoundUExp.ToString();
					chkGrantsUpd.Checked = false;
				}

				try
				{
					if((strModuleRights.IndexOf("D"))>=0)
					{
						chkGrantsDel.Checked = true;
					}
					else
					{
						chkGrantsDel.Checked = false;
					}

				}
				catch(Exception indexOutOfBoundDExp)
				{
					String msg = indexOutOfBoundDExp.ToString();
					chkGrantsDel.Checked = false;
				}
			}

		}
		private void ButtonsEnabled(bool isEnable)
		{
			btnAdd.Enabled = isEnable;
			btnAddAll.Enabled = isEnable;			
			btnRemove.Enabled = isEnable;
			Utility.EnableButton(ref btnSave,ButtonType.Save,isEnable,m_moduleAccessRights);
			
		}
		private void funcTree_SelectedCheckChange(System.Object sender , TreeViewClickEventArgs e)
		{				
			String strIndex = e.Node;		
			if (funcTree.GetNodeFromIndex(strIndex).Checked ==true)
			{ 
				funcTree.GetNodeFromIndex(strIndex).Checked =false;
			}
			else
			{
				funcTree.GetNodeFromIndex(strIndex).Checked =true;
			}
			return;
				
		}

		private void GetDataIntoList()
		{
			Role role = new Role();
			role.RoleID = 0;
			ArrayList roleListArray =  RBACManager.GetAllRoles(m_strAppID,m_strEnterpriseID,role);
			if ((roleListArray != null) && (roleListArray.Count>0))
			{
				foreach(Role allRoles in roleListArray)
				{
					ListItem  listItemVar = new ListItem();

					listItemVar.Text = allRoles.RoleName;
					listItemVar.Value = allRoles.RoleID.ToString();
					lbRoleMaster.Items.Add(listItemVar);
				}
			}			 
		}


		private void SelectAllNodes(TreeNode rootNode,bool btnFlag)
		{
			
			rootNode.Checked = btnFlag;
			TreeNodeCollection treeNodeCollection = rootNode.Nodes;
			foreach (TreeNode node in treeNodeCollection)
			{				
				SelectAllNodes(node,btnFlag);
			}
		}

		private void btnAdd_Click(object sender, System.EventArgs e)
		{
			lblMessages.Text = "";
			ViewState["isTextChange"] = true;
			ListItemCollection tmpItemCollection = new ListItemCollection();
			int i = 0;
			foreach(ListItem listItem in lbRoleMaster.Items)
			{
				if(listItem.Selected)
				{
					ListItem lstItem = new ListItem();
					lstItem.Text = listItem.Text;
					lstItem.Value = listItem.Value;
					lbRolesAdded.Items.Add(lstItem);
					tmpItemCollection.Add(lstItem);
					int iSelRoleID = Convert.ToInt32(listItem.Value);
					if((i == 0) && (lbRolesAdded.Items.Count == 1) && (IsAnyNodeChecked(tiesNode) ==  true))
					{
						//remove the added role
						/*******************************************/

						ListItemCollection tmpItemCol = new ListItemCollection();

						foreach(ListItem addedItem in lbRolesAdded.Items)
						{
							
							ListItem addedLstItem = new ListItem();
							addedLstItem.Text = addedItem.Text;
							addedLstItem.Value = addedItem.Value;
							lbRoleMaster.Items.Add(addedLstItem);
							tmpItemCol.Add(addedLstItem);
							
						}

						foreach(ListItem tmpLstItem in tmpItemCol)
						{
							lbRolesAdded.Items.Remove(tmpLstItem);
						}
						/***************************************/
						//lblConfirmMsg.Text = "Adding roles will remove the selected modules? ";
						ViewState["RemoveModules"] = true;
						ViewState["isRolesDeleted"] = false;
						//UserProfilePanel.Visible = true;
						//lbRoleMaster.Visible = false;
						//lbRolesAdded.Visible = false;
						ddbUserType.Visible = false;
						ddbCulture.Visible = false;
						return;
					}
					SelectAllNodes(tiesNode,iSelRoleID);
					i++;
				}
			}

			foreach(ListItem lstItem in tmpItemCollection)
			{
				lbRoleMaster.Items.Remove(lstItem);
			}

			//(lbRolesAdded.Items.Count == 1)
		}

		private void lbRoleMaster_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			String strRoleID = lbRoleMaster.SelectedItem.Value;
			selItemArray.Add(strRoleID);

		}

		private void UpdateAllNodeData(TreeNode rootNode)
		{
			TreeNodeCollection treeNodeCollection = rootNode.Nodes;
			foreach (TreeNode node in treeNodeCollection)
			{
				String strNodeData = node.NodeData;
				String strID = strNodeData.Substring((strNodeData.IndexOf(":")+1));
				node.NodeData = "IDU:"+strID;
				UpdateAllNodeData(node);
			}
		}

		private void RemoveAddedRoles()
		{
			lblMessages.Text = "";
			ListItemCollection tmpItemCollection = new ListItemCollection();

			foreach(ListItem listItem in lbRolesAdded.Items)
			{
				ListItem lstItem = new ListItem();
				lstItem.Text = listItem.Text;
				lstItem.Value = listItem.Value;
				lbRoleMaster.Items.Add(lstItem);
				tmpItemCollection.Add(lstItem);
			}

			foreach(ListItem lstItem in tmpItemCollection)
			{
				lbRolesAdded.Items.Remove(lstItem);
			}
		
			SelectAllNodes(tiesNode,false);	
			//Make all the tree nodedata's module rights to IDU
			UpdateAllNodeData(tiesNode);
			if (lbRolesAdded.Items.Count>0)
			{
				foreach(ListItem listItem in lbRolesAdded.Items)
				{	
					int iSelRoleID = Convert.ToInt32(listItem.Value);
					SelectAllNodes(tiesNode,iSelRoleID);	
				}
			}
		}

		private void btnRemove_Click(object sender, System.EventArgs e)
		{
			lblMessages.Text = "";
			ViewState["isTextChange"] = true;
			ListItemCollection tmpItemCollection = new ListItemCollection();

			foreach(ListItem listItem in lbRolesAdded.Items)
			{
				if(listItem.Selected)
				{
					ListItem lstItem = new ListItem();
					lstItem.Text = listItem.Text;
					lstItem.Value = listItem.Value;
					lbRoleMaster.Items.Add(lstItem);
					tmpItemCollection.Add(lstItem);
				}
			}

			foreach(ListItem lstItem in tmpItemCollection)
			{
				lbRolesAdded.Items.Remove(lstItem);
			}
			
			//Unselect the whole tree & again check it depending on the roles which exists
			SelectAllNodes(tiesNode,false);	
			//Make all the tree nodedata to IDU
			UpdateAllNodeData(tiesNode);
			if (lbRolesAdded.Items.Count>0)
			{
				foreach(ListItem listItem in lbRolesAdded.Items)
				{	
					int iSelRoleID = Convert.ToInt32(listItem.Value);
					SelectAllNodes(tiesNode,iSelRoleID);	
				}
			}
		}

		private void btnRemoveAll_Click(object sender, System.EventArgs e)
		{
			ViewState["isTextChange"] = true;
			lblMessages.Text = "";
			SelectAllNodes(tiesNode,false);
			chkGrantsDel.Checked = false;
			chkGrantsIns.Checked = false;
			chkGrantsUpd.Checked = false;

			foreach(ListItem listItem in lbRolesAdded.Items)
			{
				lbRoleMaster.Items.Add(listItem.Text);
			}
			lbRolesAdded.Items.Clear();

		}

		private void btnAddAll_Click(object sender, System.EventArgs e)
		{
			ViewState["isTextChange"] = true;
			lblMessages.Text = "";
			lbRolesAdded.Items.Clear();
			foreach(ListItem listItem in lbRoleMaster.Items)
			{
				lbRolesAdded.Items.Add(listItem.Text);
			}
			lbRoleMaster.Items.Clear();
		}

		private void btnSave_Click(object sender, System.EventArgs e)
		{
			//SaveUpdateRecord();
			UpdateUserProfile();
		}

		private void UpdateUserProfile()
		{
			try
			{
				int iuserID = txtUserID.Text.Trim().Length;
				String strUserID = txtUserID.Text.Trim();
				String strUsername=txtUserName.Text.Trim();
				String strDept=txtDepartment.Text.Trim();
				String strEmail=txtEmail.Text.Trim();
				String strCulture=ddbCulture.SelectedItem.Value;
				String strUserType=ddbUserType.SelectedItem.Value;			
			
				if (strCulture == "0")
				{
					lblMessages.Text = Utility.GetLanguageText(ResourceType.UserMessage,"SEL_CULTURE",utility.GetUserCulture());				
					return;
				}			
				com.common.classes.User user = new User();
				user.UserID=strUserID;
				user.UserName=strUsername;
				if(Session["Password"] != null)
				{
					user.UserPswd = (String)Session["Password"];
				}
				user.UserDepartment=strDept;
				user.UserEmail=strEmail;						
				user.UserCulture=strCulture;			
				RBACManager.ModifyUser(m_strAppID, m_strEnterpriseID, user);
				lblMessages.Text=Utility.GetLanguageText(ResourceType.UserMessage,"UPD_SUCCESSFULLY",utility.GetUserCulture());				
			}
			catch (ApplicationException err)
			{
				lblMessages.Text =err.Message;
			}

		}

		private bool IsAnyNodeChecked(TreeNode rootNode)
		{	
			bool isCheck = false;
			
			TreeNodeCollection treeNodeCollection = rootNode.Nodes;

			foreach (TreeNode node in treeNodeCollection)
			{
				isCheck = node.Checked;
				if (isCheck)
				{
					break;	
				}	
				IsAnyNodeChecked(node);
			}

			return isCheck;
		}
		private void SelectAllNodes(TreeNode rootNode,String userID)
		{	
			strCreatedUserID = userID;
			String strIndex = null;
			String treeNode = null;
			TreeNode parentNode = null;
			String strParentNodeValue = null;
			String strParentID = null, strModRight = null;
			String strChildID = null;
			User user = new User();
			user.UserID = strCreatedUserID;
			
			TreeNodeCollection treeNodeCollection = rootNode.Nodes;

			foreach (TreeNode node in treeNodeCollection)
			{
				isChecked = node.Checked;
				if (isChecked)
				{					
					strIndex = node.GetNodeIndex();
					treeNode = funcTree.GetNodeFromIndex(strIndex).NodeData;
					parentNode = (TreeNode)funcTree.GetNodeFromIndex(strIndex).Parent;
					strParentNodeValue = parentNode.NodeData;
					strChildID =treeNode.Substring((treeNode.IndexOf(":")+1));
					strModRight =treeNode.Substring(0,(treeNode.IndexOf(":")));
					strParentID = strParentNodeValue.Substring((strParentNodeValue.IndexOf(":")+1));
					RBACManager.AddModules(m_strAppID,m_strEnterpriseID,user,strParentID,strChildID,strModRight);
				}	
				SelectAllNodes(node,strCreatedUserID);
			}
		}
		private String UnionStrings(String treeRights, String modRights)
		{
			int iLength = treeRights.Length;
			int i = 0;
			for(i = 0;i<=iLength-1;i++)
			{
				String strChar = treeRights.Substring(i,1);
				int iPos = modRights.IndexOf(strChar);
				if (iPos == -1)
				{
					modRights = modRights.Trim() + strChar;
				}
			}
			return modRights;
		}
		 
		private void SelectAllNodes(TreeNode rootNode,long roleID)
		{	
			selRoleID = roleID;
			String strIndex = null;
			String treeNode = null;
			TreeNode parentNode = null;
			String strParentNodeValue = null;
			String strParentID = null;
			String strModRight = null;
			String strChildID = null;
			
			TreeNodeCollection treeNodeCollection = rootNode.Nodes;

			foreach (TreeNode node in treeNodeCollection)
			{
				strIndex = node.GetNodeIndex();
				treeNode = funcTree.GetNodeFromIndex(strIndex).NodeData;
				parentNode = (TreeNode)funcTree.GetNodeFromIndex(strIndex).Parent;
				strParentNodeValue = parentNode.NodeData;
				strChildID =treeNode.Substring((treeNode.IndexOf(":")+1));
				strParentID = strParentNodeValue.Substring((strParentNodeValue.IndexOf(":")+1));
				ArrayList selRoleList = RBACManager.GetAllModules(m_strAppID,m_strEnterpriseID,strParentID,strChildID,selRoleID);
				if (selRoleList.Count>0)
				{
					String strNodeData = null;
					Module module = (Module)selRoleList[0];
					String strSelModRights = module.ModuleRights;
					String strNode = treeNode.Substring((treeNode.IndexOf(":")+1));
					if(node.Checked == true)
					{
						strModRight =treeNode.Substring(0,(treeNode.IndexOf(":")));
						String strRightsUnion = UnionStrings(strModRight,strSelModRights);
						strNodeData = strRightsUnion.Trim()+":"+strNode;
					}
					else
					{
						node.Checked = true;
						strNodeData = strSelModRights.Trim()+":"+strNode;
					}
					funcTree.GetNodeFromIndex(strIndex).NodeData = strNodeData;
				}
				SelectAllNodes(node,selRoleID);
			}
		}
		private void UpdateSelectedNodeData(String modRights)
		{
			String strIndex = funcTree.SelectedNodeIndex;
			String strSelectedNode = funcTree.GetNodeFromIndex(strIndex).NodeData;
			String strNode = strSelectedNode.Substring((strSelectedNode.IndexOf(":")+1));
			String strNodeData = modRights+":"+strNode;
			funcTree.GetNodeFromIndex(strIndex).NodeData = strNodeData;
		}
		private void OnCheckedChange()
		{
			bool isUpd = chkGrantsUpd.Checked;
			bool isDel = chkGrantsDel.Checked;
			bool isIns = chkGrantsIns.Checked;
			
			String strFlag = "";
			if (isUpd) 
				strFlag += "U";
			if (isDel)
				strFlag += "D";
			if (isIns)
				strFlag += "I";
			UpdateSelectedNodeData(strFlag);
		}
		private void chkGrantsIns_CheckedChanged(object sender, System.EventArgs e)
		{
			if(lbRolesAdded.Items.Count > 0)
			{				
				ViewState["NoModifyIDU"] = true;
				ddbUserType.Visible = false;
				ddbCulture.Visible = false;
			}
			if(((bool)ViewState["NoModifyIDU"] == true) && (chkGrantsIns.Checked == true))
			{
				chkGrantsIns.Checked = false;
			}
			else if(((bool)ViewState["NoModifyIDU"] == true) && (chkGrantsIns.Checked == false))
			{
				chkGrantsIns.Checked = true;
			}
			else
			{
				OnCheckedChange();
			}
			ViewState["isTextChange"] = true;
			ViewState["NoModifyIDU"] = false;
		}
		private void chkGrantsUpd_CheckedChanged(object sender, System.EventArgs e)
		{
			if(lbRolesAdded.Items.Count > 0)
			{				
				ViewState["NoModifyIDU"] = true;			
				ddbUserType.Visible = false;
				ddbCulture.Visible = false;
			}
			if(((bool)ViewState["NoModifyIDU"] == true) && (chkGrantsUpd.Checked == true))
			{
				chkGrantsUpd.Checked = false;
			}
			else if(((bool)ViewState["NoModifyIDU"] == true) && (chkGrantsUpd.Checked == false))
			{
				chkGrantsUpd.Checked = true;
			}
			else
			{
				OnCheckedChange();
			}
			ViewState["isTextChange"] = true;
			ViewState["NoModifyIDU"] = false;
		}
		private void chkGrantsDel_CheckedChanged(object sender, System.EventArgs e)
		{
			if(lbRolesAdded.Items.Count > 0)
			{
				ViewState["NoModifyIDU"] = true;
				ddbUserType.Visible = false;
				ddbCulture.Visible = false;
			}
			if(((bool)ViewState["NoModifyIDU"] == true) && (chkGrantsDel.Checked == true))
			{
				chkGrantsDel.Checked = false;
			}
			else if(((bool)ViewState["NoModifyIDU"] == true) && (chkGrantsDel.Checked == false))
			{
				chkGrantsDel.Checked = true;
			}
			else
			{
				OnCheckedChange();
			}
			ViewState["isTextChange"] = true;
			ViewState["NoModifyIDU"] = false;
		}

		private void DisplayUsers(int indexRole)
		{
			RemoveAddedRoles();
			ArrayList userArray = (ArrayList)Session["UserArray"];
			if ((userArray != null) && (indexRole <= userArray.Count))
			{
				SelectAllNodes(tiesNode,false);
				com.common.classes.User user = (com.common.classes.User)userArray[indexRole];
				ArrayList userRoleArray = RBACManager.GetAllRoles(m_strAppID,m_strEnterpriseID,user);
				if ((userRoleArray != null) && (userRoleArray.Count > 0))
				{
					lblMessages.Text = "";
					ListItemCollection tmpItemCollection = new ListItemCollection();
					
					foreach(Role roleObj in userRoleArray)
					{
						ListItem lstItem = new ListItem();
						lstItem.Value =Convert.ToInt32(roleObj.RoleID).ToString() ;
						lstItem.Text = roleObj.RoleName;
						lbRolesAdded.Items.Add(lstItem);
						tmpItemCollection.Add(lstItem);
						int iSelRoleID = Convert.ToInt32(lstItem.Value);
						SelectAllNodes(tiesNode,iSelRoleID);
					}
					
					foreach(ListItem lstItem in tmpItemCollection)
					{
						lbRoleMaster.Items.Remove(lstItem);
					}
				}
				//get data from module_tree_user_relation
				SelDisplayedUserRights(tiesNode,txtUserID.Text.Trim());
			}
		}

		private void SelDisplayedUserRights(TreeNode rootNode,String userID)
		{	
			selUserID = userID;
			String strIndex = null;
			String treeNode = null;
			TreeNode parentNode = null;
			String strParentNodeValue = null;
			String strParentID = null;
			String strModRight = null;
			String strChildID = null;
			com.common.classes.User user = new com.common.classes.User();
			user.UserID = selUserID;

			TreeNodeCollection treeNodeCollection = rootNode.Nodes;
			foreach (TreeNode node in treeNodeCollection)
			{
				strIndex = node.GetNodeIndex();
				treeNode = funcTree.GetNodeFromIndex(strIndex).NodeData;
				parentNode = (TreeNode)funcTree.GetNodeFromIndex(strIndex).Parent;
				strParentNodeValue = parentNode.NodeData;
				strChildID =treeNode.Substring((treeNode.IndexOf(":")+1));
				strParentID = strParentNodeValue.Substring((strParentNodeValue.IndexOf(":")+1));
				ArrayList selRoleList = RBACManager.GetAllModules(m_strAppID,m_strEnterpriseID,strParentID,strChildID,selUserID);
				
				if(selRoleList.Count>0)
				{
					String strNodeData = null;
					Module module = (Module)selRoleList[0];
					String strSelModRights = module.ModuleRights;
					String strNode = treeNode.Substring((treeNode.IndexOf(":")+1));
					if(node.Checked == true)
					{
						strModRight =treeNode.Substring(0,(treeNode.IndexOf(":")));
						String strRightsUnion = UnionStrings(strModRight,strSelModRights);
						strNodeData = strRightsUnion.Trim()+":"+strNode;
					}
					else
					{
						node.Checked = true;
						strNodeData = strSelModRights.Trim()+":"+strNode;
					}
					funcTree.GetNodeFromIndex(strIndex).NodeData = strNodeData;
				}
				SelDisplayedUserRights(node,selUserID);
			}
		}

		
		private void RemoveModulesAddRole()
		{
			//Uncheck the tree nodes
			SelectAllNodes(tiesNode,false);	
			//Make all the tree nodedata's module rights to IDU
			UpdateAllNodeData(tiesNode);

			//Add the selected role to the list.
			lblMessages.Text = "";
			ViewState["isTextChange"] = true;
			ListItemCollection tmpItemCollection = new ListItemCollection();
			foreach(ListItem listItem in lbRoleMaster.Items)
			{
				if(listItem.Selected)
				{
					ListItem lstItem = new ListItem();
					lstItem.Text = listItem.Text;
					lstItem.Value = listItem.Value;
					lbRolesAdded.Items.Add(lstItem);
					tmpItemCollection.Add(lstItem);
					int iSelRoleID = Convert.ToInt32(listItem.Value);
					//Update the nodes according to the roles
					SelectAllNodes(tiesNode,iSelRoleID);
					
				}
			}
			foreach(ListItem lstItem in tmpItemCollection)
			{
				lbRoleMaster.Items.Remove(lstItem);
			}
		}
	
		private void txtUserID_TextChanged(object sender, System.EventArgs e)
		{			
			ViewState["isTextChange"] = true;
		}

		private void txtUserName_TextChanged(object sender, System.EventArgs e)
		{
			ViewState["isTextChange"] = true;
		}

		private void ddbCulture_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			ViewState["isTextChange"] = true;
		}

		private void txtEmail_TextChanged(object sender, System.EventArgs e)
		{
			ViewState["isTextChange"] = true;
		}

		private void txtDepartment_TextChanged(object sender, System.EventArgs e)
		{
			ViewState["isTextChange"] = true;
		}

		private void ddbUserType_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			ViewState["isTextChange"] = true;
		}

		private void btnResetPswd_Click(object sender, System.EventArgs e)
		{	
			String sUrl = "PasswordPopup.aspx";
			ArrayList paramList = new ArrayList();
			paramList.Add(sUrl);
			String sScript = Utility.GetScript("openWindowParam.js",paramList);
			Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);	
		}

		private void btnExecQry_Click(object sender, System.EventArgs e)
		{
		
		}
	}
}
