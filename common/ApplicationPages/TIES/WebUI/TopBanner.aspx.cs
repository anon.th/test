using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.common.classes;
using com.common.DAL;
using com.common.RBAC;
using com.common.applicationpages;
using com.common.util;

namespace commom.webui.aspx.MainFrame
{
	/// <summary>
	/// This is the topmost static frame which is customized according to the enterprise
	/// </summary>
	public class TopBanner : System.Web.UI.Page
	//public class TopBanner : BasePage
	{
		private CoreEnterprise enterprise;
		protected String strBGImage;
		protected System.Web.UI.WebControls.ImageButton imgLogout;
		protected System.Web.UI.WebControls.ImageButton Logout1;
		protected System.Web.UI.WebControls.ImageButton logout;
		protected String strStyleSheet = null;
		protected Utility utility = null;
		private void Page_Load(object sender, System.EventArgs e)
		{
			string strlog="";
			utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings,Page.Session);
			if(Request.Params["str"] != null)
			{
				strlog=Request.Params["str"];
			}
			if(strlog.ToUpper().Equals("LOGOUT"))
			{
				FormsAuthentication.SignOut();
				Session.Abandon();
				//Response.Redirect("EnterpriseLogon.aspx?ReturnUrl=%2fTIES%2fWebUI%2fMainFrame.aspx");
				Response.Redirect("EnterpriseLogon.aspx?errMsg=LOGOUT&culture=" + utility.GetUserCulture());
				Response.End();
			}
			
			//Getting the images realated to the enterprise
			enterprise = (CoreEnterprise)Session["OneEnterprise"];

			if(enterprise == null)
			{
				return;
			}

			String tempStyle = enterprise.StyleSheetURL;
			if(tempStyle!="")
			{
				strStyleSheet = tempStyle;
			}
			else
			{
				//Default Style
				strStyleSheet = "css/Styles.css";
			}
			String tempLogo = enterprise.LogoURL;
			if(tempLogo!="")
			{
				//Logo.ImageUrl = tempLogo;
			}
			else
			{
				//Logo.ImageUrl = "images/logo.gif";
			}
			
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			base.OnInit(e);
			InitializeComponent();
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

	}
}
