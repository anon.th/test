<%@ Page language="c#" Codebehind="CreditAgingReport.aspx.cs" AutoEventWireup="false" Inherits="TIES.WebUI.CreditAgingReport" %>
<%@ Import Namespace="com.common.util" %>
<%@ Import Namespace="Cambro.Web.DbCombo" %>
<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<%@ Register TagPrefix="DbCombo" Namespace="Cambro.Web.DbCombo" Assembly="Cambro.Web.DbCombo" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Credit Aging Report</title>
		<LINK href="css/Styles.css" type="text/css" rel="stylesheet" name="stylesheet">
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<!--#INCLUDE FILE="msFormValidations.inc"-->
		<script language="javascript" src="Scripts/settingScrollPosition.js"></script>
	</HEAD>
	<body onclick="GetScrollPosition();" onload="SetScrollPosition();" onunload="window.top.displayBanner.fnCloseAll(0);"
		MS_POSITIONING="GridLayout">
		<form id="CreditAgingReport" name="CreditAgingReport" method="post" runat="server">
			<asp:button id="btnQuery" style="Z-INDEX: 101; LEFT: 20px; POSITION: absolute; TOP: 40px" runat="server"
				Width="64px" CssClass="queryButton" Text="Query"></asp:button><asp:label id="lblErrorMessage" style="Z-INDEX: 105; LEFT: 20px; POSITION: absolute; TOP: 71px"
				runat="server" Width="700px" CssClass="errorMsgColor" Height="17px"></asp:label>&nbsp;
			<asp:button id="btnExecuteQuery" style="Z-INDEX: 102; LEFT: 84px; POSITION: absolute; TOP: 40px"
				runat="server" Width="123px" CssClass="queryButton" Text="Generate"></asp:button>
			<TABLE id="tblInternal" style="Z-INDEX: 104; LEFT: 14px; WIDTH: 800px; POSITION: absolute; TOP: 84px; HEIGHT: 500px"
				width="800" border="0" runat="server">
				<TR>
					<TD style="WIDTH: 430px; HEIGHT: 117px" vAlign="top">
						<TABLE class="tableRadioButton" id="Table1" style="WIDTH: 440px; HEIGHT: 24px" cellSpacing="0"
							cellPadding="0" width="440" border="0">
							<TR>
								<TD style="WIDTH: 240px"><FONT face="Tahoma">&nbsp; Age Credit from (Date):</FONT></TD>
								<TD style="WIDTH: 180px"><FONT face="Tahoma"></FONT>&nbsp;Accounts</TD>
							</TR>
							<tr height="10">
								<td style="WIDTH: 240px"></td>
								<td style="WIDTH: 180px"><FONT face="Tahoma"></FONT></td>
							</tr>
							<TR>
								<TD style="WIDTH: 240px"><asp:radiobutton id="rbDateInvoice" runat="server" Width="192px" CssClass="tableRadioButton" Text="Date Invoice was generated"
										Height="20px" AutoPostBack="True" GroupName="EnterDate" Checked="True"></asp:radiobutton></TD>
								<TD style="WIDTH: 180px"><asp:radiobutton id="rbActive" runat="server" Width="80px" CssClass="tableRadioButton" Text="Active"
										Height="20px" AutoPostBack="True" GroupName="Account" Checked="True"></asp:radiobutton></TD>
							</TR>
							<TR>
								<TD style="WIDTH: 240px; HEIGHT: 20px"><asp:radiobutton id="rbBPDate" runat="server" Width="176px" CssClass="tableRadioButton" Text="Actual Bill Placement Date"
										AutoPostBack="True" GroupName="EnterDate"></asp:radiobutton></TD>
								<TD style="WIDTH: 180px"><asp:radiobutton id="rbInactive" runat="server" Width="80px" CssClass="tableRadioButton" Text="Inactive"
										Height="20px" AutoPostBack="True" GroupName="Account"></asp:radiobutton></TD>
							</TR>
							<TR>
								<TD style="WIDTH: 240px"><asp:radiobutton id="rbFirstBPDate" runat="server" Width="168px" CssClass="tableRadioButton" Text="First Bill Placement Date"
										Height="20px" AutoPostBack="True" GroupName="EnterDate"></asp:radiobutton></TD>
								<TD style="WIDTH: 180px"><asp:radiobutton id="rbBoth" runat="server" Width="72px" CssClass="tableRadioButton" Text="Both"
										Height="20px" AutoPostBack="True" GroupName="Account"></asp:radiobutton></TD>
							</TR>
							<TR>
								<TD style="WIDTH: 240px"><asp:radiobutton id="rbDueDate" runat="server" Width="96px" CssClass="tableRadioButton" Text="Due Date"
										Height="20px" AutoPostBack="True" GroupName="EnterDate"></asp:radiobutton></TD>
								<TD style="WIDTH: 180px"><FONT face="Tahoma"></FONT></TD>
							</TR>
						</TABLE>
						</FONT><FONT face="Tahoma"></FONT></TD>
					<TD style="HEIGHT: 117px" vAlign="top">
						<TABLE class="tableRadioButton" id="Table3" cellSpacing="0" cellPadding="0" width="300"
							border="0">
							<TR>
								<TD style="WIDTH: 106px; HEIGHT: 25px" vAlign="middle"><FONT face="Tahoma"><asp:label id="lblSalesMan" runat="server" Width="80px" CssClass="tableLabel" Height="22px">Salesman ID</asp:label></FONT></TD>
								<TD style="WIDTH: 164px"><cc1:mstextbox id="txtSalesmanID" style="TEXT-TRANSFORM: uppercase" runat="server" Width="135px"
										CssClass="textField" AutoPostBack="True" TextMaskType="msUpperAlfaNumericWithUnderscore" MaxLength="20"></cc1:mstextbox><asp:button id="btnDisplaySalesDtls" tabIndex="16" runat="server" Width="21px" CssClass="searchButton"
										Text="..." Height="19px" CausesValidation="False"></asp:button></TD>
								<TD></TD>
							</TR>
							<tr>
								<td style="WIDTH: 106px" height="10"></td>
							</tr>
							<TR>
								<TD style="WIDTH: 106px; HEIGHT: 25px" vAlign="middle"><FONT face="Tahoma"><asp:label id="lblCustID" runat="server" Width="80px" CssClass="tableLabel" Height="22px">Customer ID</asp:label></FONT></TD>
								<TD style="WIDTH: 164px"><cc1:mstextbox id="txtCustID" style="TEXT-TRANSFORM: uppercase" runat="server" Width="135px" CssClass="textField"
										AutoPostBack="True" TextMaskType="msUpperAlfaNumericWithUnderscore" MaxLength="20" ReadOnly="True"></cc1:mstextbox><asp:button id="btnDisplayCustDtls" tabIndex="16" runat="server" Width="21px" CssClass="searchButton"
										Text="..." Height="19px" CausesValidation="False"></asp:button></TD>
								<TD></TD>
							</TR>
							<tr>
								<td style="WIDTH: 106px" height="10"></td>
							</tr>
							<tr>
								<TD style="WIDTH: 106px"><asp:label id="lblMasterAcc" runat="server" Width="95px" CssClass="tableLabel" Height="22px">Master Account</asp:label></TD>
								<TD style="WIDTH: 164px"><dbcombo:dbcombo id="DbComboMasterAccount" tabIndex="1" runat="server" Width="80px" AutoPostBack="True"
										Debug="false" TextBoxColumns="4" TextUpLevelSearchButton="F" ShowDbComboLink="False" ServerMethod="MasterAccountServerMethod"></dbcombo:dbcombo></TD>
								<TD style="WIDTH: 46px"><FONT face="Tahoma"></FONT></TD>
							</tr>
						</TABLE>
					</TD>
				</TR>
				<TR>
					<TD style="WIDTH: 430px; HEIGHT: 134px" vAlign="top">
						<TABLE class="tableRadioButton" id="Table1" style="WIDTH: 440px; HEIGHT: 24px" cellSpacing="0"
							cellPadding="0" width="440" border="0">
							<TR>
								<TD style="WIDTH: 178px; HEIGHT: 13px"><FONT face="Tahoma">&nbsp; Summary or Detail</FONT></TD>
							</TR>
							<tr height="10">
								<td></td>
								<td></td>
								<td></td>
							</tr>
							<TR>
								<TD style="WIDTH: 178px"><asp:radiobutton id="rbSummary" runat="server" Width="88px" CssClass="tableRadioButton" Text="Summary"
										Height="20px" AutoPostBack="True" GroupName="SummaryDetail" Checked="True"></asp:radiobutton></TD>
							</TR>
							<TR>
								<TD style="WIDTH: 178px; HEIGHT: 20px"><asp:radiobutton id="rbDetail" runat="server" Width="80px" CssClass="tableRadioButton" Text="Detail"
										AutoPostBack="True" GroupName="SummaryDetail"></asp:radiobutton></TD>
							</TR>
						</TABLE>
					</TD>
					<TD style="HEIGHT: 134px" vAlign="top">&nbsp;
					</TD>
				</TR>
				<TR>
					<TD vAlign="top" colSpan="2">&nbsp;
					</TD>
				</TR>
			</TABLE>
			<asp:label id="Label1" style="Z-INDEX: 103; LEFT: 20px; POSITION: absolute; TOP: 4px" runat="server"
				Width="568px" CssClass="maintitleSize" Height="27px"> Credit Aging Report</asp:label></TR></TABLE></TR></TABLE></form>
	</body>
</HTML>
