<%@ Page language="c#" Codebehind="MarginAnalysisReport.aspx.cs" AutoEventWireup="false" Inherits="TIES.WebUI.MarginAnalysisReport" %>
<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<%@ Register TagPrefix="DbCombo" Namespace="Cambro.Web.DbCombo" Assembly="Cambro.Web.DbCombo" %>
<%@ Import Namespace="Cambro.Web.DbCombo" %>
<%@ Import Namespace="com.common.util" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>MarginAnalysisReport</title>
		<LINK href="css/Styles.css" type="text/css" rel="stylesheet" name="stylesheet">
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<!--#INCLUDE FILE="msFormValidations.inc"-->
	</HEAD>
	<body onunload="window.top.displayBanner.fnCloseAll(0);" MS_POSITIONING="GridLayout">
		<form id="MarginAnalysisReport" method="post" runat="server">
			<asp:button id="btnQuery" style="Z-INDEX: 100; LEFT: 19px; POSITION: absolute; TOP: 54px" runat="server" Text="Query" Width="64px" CssClass="queryButton"></asp:button><asp:label id="lblErrorMessage" style="Z-INDEX: 105; LEFT: 20px; POSITION: absolute; TOP: 80px" runat="server" Width="528px" CssClass="errorMsgColor" Height="21px">Place holder for err msg</asp:label>&nbsp;
			<asp:button id="btnGenerate" style="Z-INDEX: 101; LEFT: 83px; POSITION: absolute; TOP: 54px" runat="server" Text="Generate" Width="82px" CssClass="queryButton"></asp:button>
			<TABLE id="tblMarginAnalysisRepQry" style="Z-INDEX: 104; LEFT: 14px; WIDTH: 800px; POSITION: absolute; TOP: 106px; HEIGHT: 450px" width="800" border="0" runat="server">
				<TR>
					<TD style="WIDTH: 467px; HEIGHT: 137px" vAlign="top">
						<fieldset style="WIDTH: 458px; HEIGHT: 129px"><legend><asp:label id="lblDates" runat="server" Width="243px" CssClass="tableHeadingFieldset" Font-Bold="True">Retrieval Basis on Shipment Booking Date</asp:label></legend>
							<TABLE id="tblDates" style="WIDTH: 360px; HEIGHT: 91px" cellSpacing="0" cellPadding="0" align="left" border="0" runat="server">
								<TR>
									<td style="HEIGHT: 18px"></td>
									<TD style="HEIGHT: 18px" colSpan="3">&nbsp;
										<asp:radiobutton id="rbMonth" runat="server" Text="Month" Width="73px" CssClass="tableRadioButton" Height="21px" Checked="True" GroupName="EnterDate" AutoPostBack="True"></asp:radiobutton>&nbsp;
										<asp:dropdownlist id="ddMonth" runat="server" Width="88px" CssClass="textField" Height="19px"></asp:dropdownlist>&nbsp;
										<asp:label id="lblYear" runat="server" Width="30px" CssClass="tableLabel" Height="22px">Year</asp:label>&nbsp;
										<cc1:mstextbox id="txtYear" runat="server" Width="83" CssClass="textField" NumberPrecision="4" NumberMinValue="1" NumberMaxValue="2999" TextMaskType="msNumeric" MaxLength="5"></cc1:mstextbox></TD>
									<td style="HEIGHT: 18px"></td>
								</TR>
								<TR>
									<td style="HEIGHT: 14px"></td>
									<TD style="HEIGHT: 14px" colSpan="3">&nbsp;
										<asp:radiobutton id="rbPeriod" runat="server" Text="Period" Width="62px" CssClass="tableRadioButton" GroupName="EnterDate" AutoPostBack="True"></asp:radiobutton>&nbsp;&nbsp;&nbsp;
										<cc1:mstextbox id="txtPeriod" runat="server" Width="82" CssClass="textField" TextMaskType="msDate" MaxLength="10" TextMaskString="99/99/9999"></cc1:mstextbox>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
										&nbsp;
										<cc1:mstextbox id="txtTo" runat="server" Width="83" CssClass="textField" TextMaskType="msDate" MaxLength="10" TextMaskString="99/99/9999"></cc1:mstextbox></TD>
									<td style="HEIGHT: 14px"></td>
								</TR>
								<TR>
									<td style="HEIGHT: 23px"></td>
									<TD style="HEIGHT: 23px" colSpan="3">&nbsp;
										<asp:radiobutton id="rbDate" runat="server" Text="Date" Width="74px" CssClass="tableRadioButton" Height="22px" GroupName="EnterDate" AutoPostBack="True"></asp:radiobutton>&nbsp;&nbsp;<cc1:mstextbox id="txtDate" runat="server" Width="82" CssClass="textField" TextMaskType="msDate" MaxLength="10" TextMaskString="99/99/9999"></cc1:mstextbox></TD>
									<td style="HEIGHT: 23px"></td>
								</TR>
							</TABLE>
						</fieldset>
					</TD>
					<TD style="HEIGHT: 137px" vAlign="top">
						<fieldset style="WIDTH: 320px; HEIGHT: 105px"><legend><asp:label id="lblPayerType" runat="server" Width="79px" CssClass="tableHeadingFieldset" Font-Bold="True">Payer Type</asp:label></legend>
							<TABLE id="tblPayerType" style="WIDTH: 300px; HEIGHT: 73px" cellSpacing="0" cellPadding="0" align="left" border="0" runat="server">
								<TBODY>
									<tr>
										<td></td>
										<td colSpan="2" height="1">&nbsp;</td>
										<td></td>
										<td></td>
									</tr>
									<TR>
										<td></td>
										<TD class="tableLabel" vAlign="top" colSpan="2">&nbsp;
											<asp:label id="Label1" runat="server" Width="86px" CssClass="tableLabel" Height="22px">Payer Type</asp:label></TD>
										<td><asp:listbox id="lsbCustType" runat="server" Width="137px" Height="61px" SelectionMode="Multiple"></asp:listbox></td>
										<td></td>
									</TR>
									<TR height="33">
										<td bgColor="blue"></td>
										<TD class="tableLabel" vAlign="top" colSpan="2">&nbsp;
											<asp:label id="lblPayerCode" runat="server" Width="79px" CssClass="tableLabel" Height="22px">Payer Code</asp:label>&nbsp;&nbsp;</TD>
										<td><asp:TextBox id="txtPayerCode" runat="server" Width="148px" CssClass="textField"></asp:TextBox>&nbsp;&nbsp;
											<asp:button id="btnPayerCode" runat="server" Text="..." CssClass="searchButton" CausesValidation="False"></asp:button></td>
										<td></td>
									</TR>
								</TBODY>
							</TABLE>
						</fieldset>
					</TD>
				</TR>
				<TR>
					<TD style="WIDTH: 467px; HEIGHT: 134px" vAlign="top">
						<fieldset style="WIDTH: 459px; HEIGHT: 127px"><legend><asp:label id="lblRouteType" runat="server" Width="145px" CssClass="tableHeadingFieldset" Font-Bold="True">Route / DC Selection</asp:label></legend>
							<TABLE id="tblRouteType" style="WIDTH: 452px; HEIGHT: 113px" cellSpacing="0" cellPadding="0" align="left" border="0" runat="server">
								<TR>
									<TD class="tableLabel" colSpan="2">&nbsp;
										<asp:radiobutton id="rbLongRoute" runat="server" Text="Linehaul" Width="145px" CssClass="tableRadioButton" Height="22px" Checked="True" GroupName="QueryByRoute" AutoPostBack="True"></asp:radiobutton><asp:radiobutton id="rbShortRoute" runat="server" Text="Delivery Route" Width="131px" CssClass="tableRadioButton" Height="22px" GroupName="QueryByRoute" AutoPostBack="True"></asp:radiobutton><asp:radiobutton id="rbAirRoute" runat="server" Text="Air Route" Width="149px" CssClass="tableRadioButton" Height="22px" GroupName="QueryByRoute" AutoPostBack="True"></asp:radiobutton></TD>
								</TR>
								<tr>
									<TD class="tableLabel" width="20%">&nbsp;<asp:label id="lblRouteCode" runat="server" Width="111px" CssClass="tableLabel" Height="22px">Route Code</asp:label></TD>
									<td><DBCOMBO:DBCOMBO id="DbComboPathCode" tabIndex="14" Width="25px" CssClass="tableLabel" AutoPostBack="True" ServerMethod="DbComboPathCodeSelect" ShowDbComboLink="False" TextUpLevelSearchButton="v" RegistrationKey=" " TextBoxColumns="18" Runat="server"></DBCOMBO:DBCOMBO></td>
								</tr>
								<TR>
									<TD class="tableLabel">&nbsp;<asp:label id="Label2" runat="server" Width="154px" CssClass="tableLabel" Height="22px">Origin Distribution Center</asp:label></TD>
									<TD><DBCOMBO:DBCOMBO id="DbComboOriginDC" tabIndex="14" Width="25px" CssClass="tableLabel" AutoPostBack="True" ServerMethod="DbComboDistributionCenterSelect" ShowDbComboLink="False" TextUpLevelSearchButton="v" RegistrationKey=" " TextBoxColumns="18" Runat="server"></DBCOMBO:DBCOMBO></TD>
								</TR>
								<TR>
									<TD class="tableLabel">&nbsp;<asp:label id="Label3" runat="server" Width="181px" CssClass="tableLabel" Height="22px">Destination Distribution Center</asp:label></TD>
									<TD><DBCOMBO:DBCOMBO id="DbComboDestinationDC" tabIndex="14" Width="25px" CssClass="tableLabel" AutoPostBack="True" ServerMethod="DbComboDistributionCenterSelect" ShowDbComboLink="False" TextUpLevelSearchButton="v" RegistrationKey=" " TextBoxColumns="18" Runat="server"></DBCOMBO:DBCOMBO></TD>
								</TR>
							</TABLE>
						</fieldset>
					</TD>
					<TD style="HEIGHT: 134px" vAlign="top">
						<fieldset style="WIDTH: 320px; HEIGHT: 89px"><legend><asp:label id="lblDestination" runat="server" Width="79px" CssClass="tableHeadingFieldset" Font-Bold="True">Destination</asp:label></legend>
							<TABLE id="tblDestination" style="WIDTH: 300px; HEIGHT: 89px" cellSpacing="0" cellPadding="0" align="left" border="0" runat="server">
								<tr height="37">
									<td></td>
									<TD class="tableLabel" style="WIDTH: 360px" colSpan="3">&nbsp;
										<asp:label id="lblZipCode" runat="server" Width="84px" CssClass="tableLabel" Height="22px">Postal Code</asp:label>&nbsp;&nbsp;&nbsp;
										<cc1:mstextbox id="txtZipCode" runat="server" Width="139px" CssClass="textField" TextMaskType="msUpperAlfaNumericWithUnderscore" MaxLength="10"></cc1:mstextbox>&nbsp;&nbsp;
										<asp:button id="btnZipCode" runat="server" Text="..." CssClass="searchButton" CausesValidation="False"></asp:button></TD>
									<td></td>
								</tr>
								<tr>
									<td></td>
									<TD class="tableLabel" style="WIDTH: 360px" colSpan="3">&nbsp;
										<asp:label id="lblStateCode" runat="server" Width="100px" CssClass="tableLabel" Height="22px">State / Province</asp:label><cc1:mstextbox id="txtStateCode" runat="server" Width="139" CssClass="textField" Height="22" TextMaskType="msUpperAlfaNumericWithUnderscore" MaxLength="10"></cc1:mstextbox>&nbsp;&nbsp;
										<asp:button id="btnStateCode" runat="server" Text="..." CssClass="searchButton" CausesValidation="False"></asp:button></TD>
									<td></td>
								</tr>
							</TABLE>
						</fieldset>
					</TD>
				</TR>
				<TR>
					<TD vAlign="top" colSpan="2">
						<TABLE id="Table1" width="700" border="0" runat="server">
							<TBODY>
								<TR>
									<TD>
										<fieldset><legend><asp:label id="Label4" Runat="server">Shipment Allocation Cost Based On</asp:label></legend>
											<table width="100%">
												<TR>
													<TD width="25%" colSpan="4"><asp:radiobutton id="rbConsgCAM" runat="server" Text="Consignment" CssClass="tableRadioButton" Height="22" Checked="True" GroupName="AllocMethod" AutoPostBack="True"></asp:radiobutton></TD>
													<TD width="25%" colSpan="4"><asp:radiobutton id="rbWtCAM" runat="server" Text="Weight" CssClass="tableRadioButton" Height="22px" GroupName="AllocMethod" AutoPostBack="True"></asp:radiobutton></TD>
													<TD width="25%" colSpan="4"><asp:radiobutton id="rbVolCAM" runat="server" Text="Volume" CssClass="tableRadioButton" Height="22px" GroupName="AllocMethod" AutoPostBack="True"></asp:radiobutton></TD>
													<TD width="25%" colSpan="8">&nbsp;</TD>
												</TR>
											</table>
										</fieldset>
										<br>
										<fieldset><legend><asp:label id="Label5" Runat="server">Line Haul Cost Based On</asp:label></legend>
											<table width="100%">
												<TR height="27">
													<TD width="25%" colSpan="4"><asp:radiobutton id="rbConsgLH" runat="server" Text="Consignment" CssClass="tableRadioButton" Height="22px" Checked="True" GroupName="LHMethod" AutoPostBack="True"></asp:radiobutton></TD>
													<TD width="25%" colSpan="4"><asp:radiobutton id="rbWtLH" runat="server" Text="Weight" CssClass="tableRadioButton" Height="22px" GroupName="LHMethod" AutoPostBack="True"></asp:radiobutton></TD>
													<TD width="25%" colSpan="4">&nbsp;</TD>
													<TD width="25%" colSpan="8">&nbsp;</TD>
												</TR>
											</table>
										</fieldset>
									</TD>
								</TR>
							</TBODY>
						</TABLE>
					</TD>
				</TR>
			</TABLE>
			<asp:label id="lblTitle" style="Z-INDEX: 103; LEFT: 20px; POSITION: absolute; TOP: 10px" runat="server" Width="365px" CssClass="maintitleSize" Height="27px">Margin Analysis Report</asp:label></form>
	</body>
</HTML>
