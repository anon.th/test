using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.ties.DAL;
using com.common.classes;
using com.common.util;
using com.ties.classes;
using com.ties.BAL;
using com.common.applicationpages;
using System.Text;
using System.IO;
using System.Configuration;
using System.Data.OleDb;
using TIESBAL;
using TIESDAL;

namespace com.ties
	{
		/// <summary>
	/// Summary description for ImportConsignments.
	/// </summary>
	public class ImportConsignments : BasePage
	{

		
		#region Web Form Designer generated code

		protected System.Web.UI.WebControls.Label lblMainTitle;
		protected System.Web.UI.WebControls.Label lblErrorMsg;
		protected System.Web.UI.WebControls.Button btnSave;
		protected System.Web.UI.WebControls.Button btnImport;
		protected System.Web.UI.WebControls.DataGrid dgConsignment;
		protected System.Web.UI.WebControls.DataGrid dgPackages;
		protected System.Web.UI.WebControls.DataGrid dgError;
		protected System.Web.UI.WebControls.TextBox txtFilePath;
		protected System.Web.UI.WebControls.Button btnBrowse;
		protected System.Web.UI.HtmlControls.HtmlInputFile inFile;
		protected System.Web.UI.WebControls.ImageButton ImageButton1;
		protected System.Web.UI.WebControls.Label lblTotalSavedCon;
		protected System.Web.UI.HtmlControls.HtmlInputButton btnHiddenPostbackBrowse;
		protected System.Web.UI.WebControls.DataGrid DataGrid1;
		protected System.Web.UI.HtmlControls.HtmlTableRow trMain;

		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			base.OnInit(e);
			InitializeComponent();
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnImport.Click += new System.EventHandler(this.btnImport_Click);
			this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
			this.txtFilePath.TextChanged += new System.EventHandler(this.txtFilePath_TextChanged);
			this.dgConsignment.SelectedIndexChanged += new System.EventHandler(this.dgConsignment_SelectedIndexChanged);
			this.btnHiddenPostbackBrowse.ServerClick += new System.EventHandler(this.btnHiddenPostbackBrowse_ServerClick);
			this.Load += new System.EventHandler(this.Page_Load);

		}

		#endregion

		String appID = null;
		String enterpriseID = null;
		String userID = null;
		DataSet dsConsignment = null;
		DataSet dsPackage = null;
		DataSet dsError = null;

		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			appID = utility.GetAppID();
			enterpriseID = utility.GetEnterpriseID();
			userID = utility.GetUserID();

			if(!IsPostBack)
			{			
				ImportConsignmentsDAL.ClearTempDataByUserId(appID, enterpriseID, userID);
				QueryMode();	  
				ViewState["userCulture"] = ImportConsignmentsDAL.GetUserCulture(appID, enterpriseID, userID);
				ViewState["selectedConsignment"] = "";
				ViewState["payment_mode"] = ImportConsignmentsDAL.GetCoreSysetmCode(appID, enterpriseID,
					"payment_mode", 0, 0, (String)ViewState["userCulture"]);
				ViewState["payment_type"] = ImportConsignmentsDAL.GetCoreSysetmCode(appID, enterpriseID,
					"payment_type", 0, 0, (String)ViewState["userCulture"]);

				//HC Return Task
				ViewState["pod_slip_required"] = ImportConsignmentsDAL.GetCoreSysetmCode(appID, enterpriseID,
					"pod_slip_required", 0, 0, (String)ViewState["userCulture"]);
				ViewState["hc_invoice_required"] = ImportConsignmentsDAL.GetCoreSysetmCode(appID, enterpriseID,
					"hc_invoice_required", 0, 0, (String)ViewState["userCulture"]);
				//HC Return Task

				txtFilePath.Attributes.Add("onkeyup", "clearValue();");

				DataSet profileDS = SysDataManager1.GetEnterpriseProfile(appID, enterpriseID);
				if(profileDS.Tables[0].Rows.Count > 0)
				{
					int currency_decimal = (int) profileDS.Tables[0].Rows[0]["currency_decimal"];
					ViewState["m_format"] = "{0:F" + currency_decimal.ToString() + "}";
					
					if  ( profileDS.Tables[0].Rows[0]["wt_rounding_method"] != System.DBNull.Value)
					{
						
						ViewState["wt_rounding_method"] = Convert.ToInt32(profileDS.Tables[0].Rows[0]["wt_rounding_method"]);
					}

					if  ( profileDS.Tables[0].Rows[0]["wt_increment_amt"] != System.DBNull.Value)
					{
						ViewState["wt_increment_amt"] = Convert.ToDecimal(profileDS.Tables[0].Rows[0]["wt_increment_amt"]);
					}
				}

				btnSave.Attributes.Add("onclick", "StartShowWait(this);");
				btnImport.Attributes.Add("onclick", "StartShowWait(this);");
			}
			else
			{
				dsConsignment = (DataSet) Session["dsConsignment"];
				dsPackage = (DataSet) Session["dsPackage"];
				dsError = (DataSet) Session["dsError"];
			}
		}

		
		protected void dgConsignment_Bound(object sender, DataGridItemEventArgs e)
		{
			if (e.Item.DataItem != null)
			{
				if(DataBinder.Eval(e.Item.DataItem, "recordid") != System.DBNull.Value)
				{
					String recordid = DataBinder.Eval(e.Item.DataItem, "recordid").ToString();

					ImageButton imgSelect = (ImageButton)e.Item.FindControl("imgSelect");
					if(imgSelect != null)
					{
						DataSet dsAudit = ImportConsignmentsDAL.GetAuditData(appID, enterpriseID,
							recordid, userID, "Consignment");

						if(dsAudit.Tables[0].Rows.Count > 0 )
							imgSelect.ImageUrl  = "images/butt-red.gif";
						else
							imgSelect.ImageUrl  = "images/butt-select.gif";
					}

					DropDownList ddlpayment_mode = (DropDownList)e.Item.FindControl("ddlpayment_mode");
					if(ddlpayment_mode != null)
					{
						DataSet dsPaymentMode = (DataSet)ViewState["payment_mode"];

						ddlpayment_mode.DataSource = dsPaymentMode;
						ddlpayment_mode.DataTextField = "code_text";
						ddlpayment_mode.DataValueField = "code_str_value";
						ddlpayment_mode.DataBind();

						ListItem  tmpList = new ListItem();
						tmpList.Value = "N";
						tmpList.Text = " ";
						ddlpayment_mode.Items.Insert(0, tmpList);

						String payment_mode = "";

						if((DataBinder.Eval(e.Item.DataItem, "payment_mode") != null) && 
							(DataBinder.Eval(e.Item.DataItem, "payment_mode") != DBNull.Value))
						{
							payment_mode = (String)DataBinder.Eval(e.Item.DataItem, "payment_mode");
							
							//If payment_mode='C' Then Disable DropDownList
							ddlpayment_mode.Enabled = !payment_mode.Equals("C"); 
						}

						if(payment_mode.Trim() != "")
							ddlpayment_mode.SelectedIndex = ddlpayment_mode.Items.IndexOf(ddlpayment_mode.Items.FindByValue(payment_mode));
						else
							ddlpayment_mode.SelectedIndex = ddlpayment_mode.Items.IndexOf(ddlpayment_mode.Items.FindByValue("N"));

						dsPaymentMode.Dispose();
						dsPaymentMode = null;
					}

					Label lblpayment_mode = (Label)e.Item.FindControl("lblpayment_mode");
					if(lblpayment_mode != null)
					{
						DataSet dsPaymentMode = (DataSet)ViewState["payment_mode"];

						String payment_mode = "";

						if((DataBinder.Eval(e.Item.DataItem, "payment_mode") != null) && 
							(DataBinder.Eval(e.Item.DataItem, "payment_mode") != System.DBNull.Value))
							payment_mode = (String)DataBinder.Eval(e.Item.DataItem, "payment_mode");

						DataRow[] drPaymentMode = dsPaymentMode.Tables[0].Select("code_str_value = '" + payment_mode + "'");

						if(drPaymentMode.Length > 0)
							lblpayment_mode.Text = drPaymentMode[0]["code_text"].ToString();

						dsPaymentMode.Dispose();
						dsPaymentMode = null;
					}

					DropDownList ddlpayment_type = (DropDownList)e.Item.FindControl("ddlpayment_type");
					if(ddlpayment_type != null)
					{
						DataSet dsPaymentType = (DataSet)ViewState["payment_type"];

						ddlpayment_type.DataSource = dsPaymentType;
						ddlpayment_type.DataTextField = "code_text";
						ddlpayment_type.DataValueField = "code_str_value";
						ddlpayment_type.DataBind();

						ListItem  tmpList = new ListItem();
						tmpList.Value = "N";
						tmpList.Text = " ";
						ddlpayment_type.Items.Insert(0, tmpList);

						String payment_type = "";

						if((DataBinder.Eval(e.Item.DataItem, "payment_type") != null) && 
							(DataBinder.Eval(e.Item.DataItem, "payment_type") != System.DBNull.Value))
							payment_type = (String)DataBinder.Eval(e.Item.DataItem, "payment_type");

						if(payment_type.Trim() != "")
							ddlpayment_type.SelectedIndex = ddlpayment_type.Items.IndexOf(ddlpayment_type.Items.FindByValue(payment_type));
						else
							ddlpayment_type.SelectedIndex = ddlpayment_type.Items.IndexOf(ddlpayment_type.Items.FindByValue("N"));


						dsPaymentType.Dispose();
						dsPaymentType = null;
					}

					Label lblpayment_type = (Label)e.Item.FindControl("lblpayment_type");
					if(lblpayment_type != null)
					{
						DataSet dsPaymentType = (DataSet)ViewState["payment_type"];

						String payment_type = "";

						if((DataBinder.Eval(e.Item.DataItem, "payment_type") != null) && 
							(DataBinder.Eval(e.Item.DataItem, "payment_type") != System.DBNull.Value))
							payment_type = (String)DataBinder.Eval(e.Item.DataItem, "payment_type");

						DataRow[] drPaymentType = dsPaymentType.Tables[0].Select("code_str_value = '" + payment_type + "'");

						if(drPaymentType.Length > 0)
							lblpayment_type.Text = drPaymentType[0]["code_text"].ToString();

						dsPaymentType.Dispose();
						dsPaymentType = null;
					}

					//HC Return Task
					DropDownList ddlReturnPODHC = (DropDownList)e.Item.FindControl("ddlReturnPODHC");
					if(ddlReturnPODHC != null)
					{
						DataSet dsPODSlipRequired = (DataSet)ViewState["pod_slip_required"];

						ddlReturnPODHC.DataSource = dsPODSlipRequired;
						ddlReturnPODHC.DataTextField = "code_text";
						ddlReturnPODHC.DataValueField = "code_str_value";
						ddlReturnPODHC.DataBind();

						ListItem  tmpList = new ListItem();
						tmpList.Value = "D";
						tmpList.Text = "DEFAULT";
						ddlReturnPODHC.Items.Insert(0, tmpList);

						String return_pod_hc = "";

						if((DataBinder.Eval(e.Item.DataItem, "return_pod_hc") != null) && 
							(DataBinder.Eval(e.Item.DataItem, "return_pod_hc") != System.DBNull.Value))
							return_pod_hc = (String)DataBinder.Eval(e.Item.DataItem, "return_pod_hc");

						if(return_pod_hc.Trim() != "")
							ddlReturnPODHC.SelectedIndex = ddlReturnPODHC.Items.IndexOf(ddlReturnPODHC.Items.FindByValue(return_pod_hc));
						else
							ddlReturnPODHC.SelectedIndex = ddlReturnPODHC.Items.IndexOf(ddlReturnPODHC.Items.FindByValue("D"));


						dsPODSlipRequired.Dispose();
						dsPODSlipRequired = null;
					}

					Label lblReturnPODHC = (Label)e.Item.FindControl("lblReturnPODHC");
					if(lblReturnPODHC != null)
					{
						DataSet dsPODSlipRequired = (DataSet)ViewState["pod_slip_required"];

						String return_pod_hc = "";

						if((DataBinder.Eval(e.Item.DataItem, "return_pod_hc") != null) && 
							(DataBinder.Eval(e.Item.DataItem, "return_pod_hc") != System.DBNull.Value))
							return_pod_hc = (String)DataBinder.Eval(e.Item.DataItem, "return_pod_hc");

						DataRow[] drReturnPODHC = dsPODSlipRequired.Tables[0].Select("code_str_value = '" + return_pod_hc + "'");

						
						if(drReturnPODHC.Length > 0) 
						{
							lblReturnPODHC.Text = drReturnPODHC[0]["code_text"].ToString();
						}
						else
						{
							if (return_pod_hc.Trim() == "")
							{
								lblReturnPODHC.Text = "DEFAULT";
							}
						}

						dsPODSlipRequired.Dispose();
						dsPODSlipRequired = null;
					}

					DropDownList ddlReturnInvHC = (DropDownList)e.Item.FindControl("ddlReturnInvHC");
					if(ddlReturnInvHC != null)
					{
						DataSet dsReturnInvHC = (DataSet)ViewState["hc_invoice_required"];

						ddlReturnInvHC.DataSource = dsReturnInvHC;
						ddlReturnInvHC.DataTextField = "code_text";
						ddlReturnInvHC.DataValueField = "code_str_value";
						ddlReturnInvHC.DataBind();

						ListItem  tmpList = new ListItem();
						tmpList.Value = "D";
						tmpList.Text = "DEFAULT";
						ddlReturnInvHC.Items.Insert(0, tmpList);

						String return_invoice_hc = "";

						if((DataBinder.Eval(e.Item.DataItem, "return_invoice_hc") != null) && 
							(DataBinder.Eval(e.Item.DataItem, "return_invoice_hc") != System.DBNull.Value))
							return_invoice_hc = (String)DataBinder.Eval(e.Item.DataItem, "return_invoice_hc");

						if(return_invoice_hc.Trim() != "")
							ddlReturnInvHC.SelectedIndex = ddlReturnInvHC.Items.IndexOf(ddlReturnInvHC.Items.FindByValue(return_invoice_hc));
						else
							ddlReturnInvHC.SelectedIndex = ddlReturnInvHC.Items.IndexOf(ddlReturnInvHC.Items.FindByValue("D"));


						dsReturnInvHC.Dispose();
						dsReturnInvHC = null;
					}

					Label lblReturnInvHC = (Label)e.Item.FindControl("lblReturnInvHC");
					if(lblReturnInvHC != null)
					{
						DataSet dsReturnInvHC = (DataSet)ViewState["hc_invoice_required"];

						String return_invoice_hc = "";

						if((DataBinder.Eval(e.Item.DataItem, "return_invoice_hc") != null) && 
							(DataBinder.Eval(e.Item.DataItem, "return_invoice_hc") != System.DBNull.Value))
							return_invoice_hc = (String)DataBinder.Eval(e.Item.DataItem, "return_invoice_hc");

						DataRow[] drReturnInvHC = dsReturnInvHC.Tables[0].Select("code_str_value = '" + return_invoice_hc + "'");

						if(drReturnInvHC.Length > 0) 
						{
							lblReturnInvHC.Text = drReturnInvHC[0]["code_text"].ToString();
						}
						else
						{
							if (return_invoice_hc.Trim() == "")
							{
								lblReturnInvHC.Text = "DEFAULT";
							}
						}

						dsReturnInvHC.Dispose();
						dsReturnInvHC = null;
					}
					//HC Return Task
				}
			}
		}

		protected void dgConsignment_Cancel(object sender, DataGridCommandEventArgs e)
		{
			dgConsignment.EditItemIndex = -1;
			BindConsignmentGrid();
		}

		public void dgConsignment_Delete(object sender, DataGridCommandEventArgs e)
		{
			int iSelIndex = e.Item.ItemIndex;
			DataGridItem dgRow = dgConsignment.Items[iSelIndex];
			Label lblConRecordid = (Label)dgRow.Cells[0].FindControl("lblConRecordid");
			Label lblconsignment_no = (Label)dgRow.Cells[4].FindControl("lblconsignment_no");

			String strRecordID = lblConRecordid.Text;
			String strConsignmentNo = lblconsignment_no.Text;

			try
			{
				int iRowsDeleted = 0;

				iRowsDeleted = ImportConsignmentsDAL.DeleteConsignment(appID, enterpriseID, strConsignmentNo,
					strRecordID, userID);

				lblErrorMsg.Text = iRowsDeleted + " row(s) deleted.";
			}
			catch(ApplicationException appException)
			{
				String strMsg = appException.Message;
				if(strMsg.IndexOf("FK") != -1)
				{
					lblErrorMsg.Text =  Utility.GetLanguageText(ResourceType.UserMessage,"ERR_DEL_PATH_TRANS",utility.GetUserCulture());
				}
				if(strMsg.ToLower().IndexOf("child record found") != -1 )
				{
					lblErrorMsg.Text =  Utility.GetLanguageText(ResourceType.UserMessage,"ERR_DEL_PATH_TRANS",utility.GetUserCulture());
							
				}			
				else
				{
					lblErrorMsg.Text = strMsg;
				}

				return;
			}

			RetreiveConsignmentData();
		}

		protected void dgConsignment_Edit(object sender, DataGridCommandEventArgs e)
		{
			dgConsignment.EditItemIndex = e.Item.ItemIndex;
			BindConsignmentGrid();
		}

		protected void dgConsignment_PageChange(object sender, DataGridPageChangedEventArgs e)
		{
			dgConsignment.CurrentPageIndex = e.NewPageIndex;
			dgConsignment.EditItemIndex = -1;
			dgConsignment.SelectedIndex = -1;
			BindConsignmentGrid();
		}

		public void dgConsignment_SelectedIndexChanged(object sender, System.EventArgs e)
		{

				int iSelIndex = dgConsignment.SelectedIndex;
				DataGridItem dgRow = dgConsignment.Items[iSelIndex];

				String strRecordID = "";
				Label lblConRecordid = (Label)dgRow.Cells[0].FindControl("lblConRecordid");
				if(lblConRecordid != null)
					strRecordID = lblConRecordid.Text;

				Label lblconsignment_no = (Label)dgRow.Cells[4].FindControl("lblconsignment_no");
				if(lblconsignment_no != null) 
				{
					ViewState["selectedConsignment"] = lblconsignment_no.Text;
				}

				if(ViewState["selectedConsignment"].ToString() != "")
				{
					dsPackage = ImportConsignmentsDAL.GetPackageData(appID, enterpriseID,
						0, 0, ViewState["selectedConsignment"].ToString(), userID);

					Session["dsPackage"] = dsPackage;
					BindPackageGrid();

				}
				else
				{
					dsPackage = ImportConsignmentsDAL.GetEmptyPackages();
					Session["dsPackage"] = dsPackage;
					BindPackageGrid();
				}

				if(strRecordID != "")
				{
					DataSet dsError = ImportConsignmentsDAL.GetAuditData(appID, enterpriseID,
						strRecordID, userID, "Consignment");

					Session["dsError"] = dsError;
					BindErrorGrid();
				}

		}

		public void dgConsignment_Update(object sender, DataGridCommandEventArgs e)
		{
			int iSelIndex = e.Item.ItemIndex;
			DataGridItem dgRow = dgConsignment.Items[iSelIndex];

			String strRecordID = "";
			String strNewConsignment = "";
			String strOldConsignment = "";

			Label lblConRecordid = (Label)dgRow.Cells[0].FindControl("lblConRecordid");
			if(lblConRecordid != null)
				strRecordID =  lblConRecordid.Text;

			if(strRecordID.Trim() != "")
			{
				TextBox txtconsignment_no = (TextBox)dgRow.Cells[4].FindControl("txtconsignment_no");
				string ConNo="";
				if(Utility.ValidateConsignmentNo(txtconsignment_no.Text.Trim(),ref ConNo)==false)
				{
					lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"CREATE_SIP_MSG_CONSNO_INVALID",utility.GetUserCulture()); 
					return;
				}
				txtconsignment_no.Text=ConNo;
				if(txtconsignment_no != null)
				{
					strNewConsignment =  txtconsignment_no.Text;
				}
				
				if(strNewConsignment.Trim() == "")
				{
					Label lblconsignment_no = (Label)dgRow.Cells[4].FindControl("lblconsignment_no");
					if(lblconsignment_no != null)
					{
						strNewConsignment =  lblconsignment_no.Text;
					}
				}
			
				dsConsignment = (DataSet)Session["dsConsignment"];
				DataRow[] drConsignment = dsConsignment.Tables[0].Select("recordid = '" + strRecordID + "'");
				
				if(drConsignment.Length > 0)
				{
					if((drConsignment[0]["consignment_no"] != null) || 
						(!drConsignment[0]["consignment_no"].GetType().Equals(System.Type.GetType("System.DBNull"))) || 
						(drConsignment[0]["consignment_no"].ToString() != ""))
					{
						strOldConsignment = drConsignment[0]["consignment_no"].ToString();
					}
				}
				
				fetchDatatoDsConsignment(dgConsignment.Items[iSelIndex], strRecordID);

				dsConsignment = (DataSet)Session["dsConsignment"];
				

				ArrayList arrOfRecordid = new ArrayList();

				arrOfRecordid.Add(strRecordID);
				arrOfRecordid = ImportConsignmentsDAL.GetArrayOfRecordIDCons(appID, enterpriseID,
					0, 0, arrOfRecordid, strNewConsignment ,strOldConsignment, userID);

				ImportConsignmentsDAL.UpdateConsignment(appID, enterpriseID, dsConsignment,
					arrOfRecordid, userID, (String)ViewState["userCulture"]);

				dgConsignment.EditItemIndex = -1;
				BindConsignmentGrid();

				if(strRecordID != "")
				{
					DataSet dsError = ImportConsignmentsDAL.GetAuditData(appID, enterpriseID,
						strRecordID, userID, "Consignment");

					Session["dsError"] = dsError;
					BindErrorGrid();
				}
			}
		}
		public void dgConsignment_Button(object sender, DataGridCommandEventArgs e)
		{
			String strCmdNm = e.CommandName;

			if(strCmdNm.Equals("ServiceCodeSearch"))
			{
				TextBox txtservice_code	= (TextBox)e.Item.FindControl("txtservice_code");
				TextBox txtsender_zipcode	= (TextBox)e.Item.FindControl("txtsender_zipcode");
				TextBox txtrecipient_zipcode	= (TextBox)e.Item.FindControl("txtrecipient_zipcode");
				if(txtservice_code != null)
				{
					String sUrl = "ServiceCodePopup.aspx?FORMID=ImportConsignments&CODEID=" + txtservice_code.ClientID +
									"&DestZipCode="+txtrecipient_zipcode.Text.Trim()+
									"&SendZipCode="+txtsender_zipcode.Text.Trim();
					ArrayList paramList = new ArrayList();
					paramList.Add(sUrl);

					String sScript = Utility.GetScript("openWindowParam.js",paramList);
					Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
				}
				
			}
			else if(strCmdNm.Equals("PayerIDSearch"))
			{
				TextBox txtpayerid	= (TextBox)e.Item.FindControl("txtpayerid");
				TextBox txtpayer_name	= (TextBox)e.Item.FindControl("txtpayer_name");
				TextBox txtpayer_address1	= (TextBox)e.Item.FindControl("txtpayer_address1");
				TextBox txtpayer_address2	= (TextBox)e.Item.FindControl("txtpayer_address2");
				TextBox txtpayer_zipcode	= (TextBox)e.Item.FindControl("txtpayer_zipcode");
				TextBox txtpayer_telephone	= (TextBox)e.Item.FindControl("txtpayer_telephone");
				TextBox txtpayer_fax	= (TextBox)e.Item.FindControl("txtpayer_fax");
				DropDownList ddlpayment_mode = (DropDownList)e.Item.FindControl("ddlpayment_mode");

				if(txtpayerid != null)
				{
					String sUrl = "CustomerPopup.aspx?FORMID="+"ImportConsignments" +
						"&payerid=" + txtpayerid.ClientID +
						"&payer_name=" + txtpayer_name.ClientID +
						"&payer_address1=" + txtpayer_address1.ClientID +
						"&payer_address2=" + txtpayer_address2.ClientID +
						"&payer_zipcode=" + txtpayer_zipcode.ClientID +
						"&payer_telephone=" + txtpayer_telephone.ClientID +
						"&payer_fax=" + txtpayer_fax.ClientID +
						"&payer_payment_mode=" + ddlpayment_mode.ClientID;
					ArrayList paramList = new ArrayList();
					paramList.Add(sUrl);
					String sScript = Utility.GetScript("openWindowParam.js",paramList);
					Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
				}
			}
			else if(strCmdNm.Equals("PayerPostalCodeSearch"))
			{
				TextBox txtpayer_zipcode	= (TextBox)e.Item.FindControl("txtpayer_zipcode");

				if(txtpayer_zipcode != null)
				{
					String sUrl = "ZipcodePopup.aspx?FORMID="+"ImportConsignments" +
						"&ZIPCODE_CID=" + txtpayer_zipcode.ClientID;
 
					ArrayList paramList = new ArrayList();
					paramList.Add(sUrl);
					String sScript = Utility.GetScript("openWindowParam.js",paramList);
					Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
				}
			}
			else if(strCmdNm.Equals("SenderNameSearch"))
			{
				TextBox txtsender_name	= (TextBox)e.Item.FindControl("txtsender_name");
				TextBox txtsender_address1	= (TextBox)e.Item.FindControl("txtsender_address1");
				TextBox txtsender_address2	= (TextBox)e.Item.FindControl("txtsender_address2");
				TextBox txtsender_zipcode	= (TextBox)e.Item.FindControl("txtsender_zipcode");
				TextBox txtsender_telephone	= (TextBox)e.Item.FindControl("txtsender_telephone");
				TextBox txtsender_fax	= (TextBox)e.Item.FindControl("txtsender_fax");
				TextBox txtsender_contact_person	= (TextBox)e.Item.FindControl("txtsender_contact_person");
				TextBox txtrecipient_zipcode	= (TextBox)e.Item.FindControl("txtrecipient_zipcode");
				TextBox txtpayerid	= (TextBox)e.Item.FindControl("txtpayerid");
				

				if(txtsender_name != null)
				{
					String sUrl = "SndRecipPopup.aspx?FORMID="+"ImportConsignments" +
						"&SENDRCPTYPE="+"S"+
						"&SENDERNAME="+ txtsender_name.Text.Trim()+
						"&ZIPCODE="+txtrecipient_zipcode.Text+
						"&CUSTID="+txtpayerid.Text+
						"&DestZipCode="+txtrecipient_zipcode.Text.Trim()+
						"&SENDZIP="+txtsender_zipcode.Text.Trim() +
						"&SENDNAMECLN="+ txtsender_name.ClientID +
						"&SENDADD1CLN=" + txtsender_address1.ClientID +
						"&SENDADD2CLN=" + txtsender_address2.ClientID +
						"&SENDZIPCLN=" + txtsender_zipcode.ClientID +
						"&SENDTELECLN=" + txtsender_telephone.ClientID +
						"&SENDFAXCLN=" + txtsender_fax.ClientID +
						"&SENDCONTACTCLN=" + txtsender_contact_person.ClientID;

					ArrayList paramList = new ArrayList();
					paramList.Add(sUrl);

					String sScript = Utility.GetScript("openWindowParam.js",paramList);
					Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
				}
			}
			else if(strCmdNm.Equals("SenderPostalCodeSearch"))
			{
				TextBox txtsender_zipcode	= (TextBox)e.Item.FindControl("txtsender_zipcode");

				if(txtsender_zipcode != null)
				{
					String sUrl = "ZipcodePopup.aspx?FORMID="+"ImportConsignments" +
						"&ZIPCODE_CID=" + txtsender_zipcode.ClientID;
 
					ArrayList paramList = new ArrayList();
					paramList.Add(sUrl);
					String sScript = Utility.GetScript("openWindowParam.js",paramList);
					Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
				}
			}
			else if(strCmdNm.Equals("RecipientNameSearch"))
			{
				TextBox txtrecipient_name	= (TextBox)e.Item.FindControl("txtrecipient_name");
				TextBox txtrecipient_address1	= (TextBox)e.Item.FindControl("txtrecipient_address1");
				TextBox txtrecipient_address2	= (TextBox)e.Item.FindControl("txtrecipient_address2");
				TextBox txtrecipient_zipcode	= (TextBox)e.Item.FindControl("txtrecipient_zipcode");
				TextBox txtrecipient_telephone	= (TextBox)e.Item.FindControl("txtrecipient_telephone");
				TextBox txtrecipient_fax	= (TextBox)e.Item.FindControl("txtrecipient_fax");
				TextBox txtrecipient_contact_person	= (TextBox)e.Item.FindControl("txtrecipient_contact_person");
				TextBox txtpayerid	= (TextBox)e.Item.FindControl("txtpayerid");
				TextBox txtsender_zipcode	= (TextBox)e.Item.FindControl("txtsender_zipcode");

				if(txtrecipient_name != null)
				{
					String sUrl = "SndRecipPopup.aspx?FORMID="+"ImportConsignments"+
						"&SENDRCPTYPE="+"R"+
						"&SENDERNAME="+txtrecipient_name.Text.Trim()+
						"&ZIPCODE="+txtrecipient_zipcode.Text+
						"&CUSTID="+txtpayerid.Text+
						"&DestZipCode="+txtrecipient_zipcode.Text.Trim()+
						"&SENDZIP="+txtsender_zipcode.Text.Trim() +
						"&RECNAMECLN="+ txtrecipient_name.ClientID +
						"&RECADD1CLN=" + txtrecipient_address1.ClientID +
						"&RECADD2CLN=" + txtrecipient_address2.ClientID +
						"&RECZIPCLN=" + txtrecipient_zipcode.ClientID +
						"&RECTELECLN=" + txtrecipient_telephone.ClientID +
						"&RECFAXCLN=" + txtrecipient_fax.ClientID +
						"&RECCONTACTCLN=" + txtrecipient_contact_person.ClientID;
					ArrayList paramList = new ArrayList();
					paramList.Add(sUrl);

					String sScript = Utility.GetScript("openWindowParam.js",paramList);
					Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
				}
			}
			else if(strCmdNm.Equals("RecipientPostalCodeSearch"))
			{
				TextBox txtrecipient_zipcode	= (TextBox)e.Item.FindControl("txtrecipient_zipcode");

				if(txtrecipient_zipcode != null)
				{
					String sUrl = "ZipcodePopup.aspx?FORMID="+"ImportConsignments" +
						"&ZIPCODE_CID=" + txtrecipient_zipcode.ClientID;
 
					ArrayList paramList = new ArrayList();
					paramList.Add(sUrl);
					String sScript = Utility.GetScript("openWindowParam.js",paramList);
					Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
				}
			}
			else if(strCmdNm.Equals("RecipientTelephoneSearch"))
			{
				TextBox txtrecipient_telephone	= (TextBox)e.Item.FindControl("txtrecipient_telephone");
				TextBox txtrecipient_name	= (TextBox)e.Item.FindControl("txtrecipient_name");
				TextBox txtrecipient_address1	= (TextBox)e.Item.FindControl("txtrecipient_address1");
				TextBox txtrecipient_address2	= (TextBox)e.Item.FindControl("txtrecipient_address2");
				TextBox txtrecipient_zipcode	= (TextBox)e.Item.FindControl("txtrecipient_zipcode");
				TextBox txtrecipient_contact_person	= (TextBox)e.Item.FindControl("txtrecipient_contact_person");
				TextBox txtrecipient_fax	= (TextBox)e.Item.FindControl("txtrecipient_fax");

				if(txtrecipient_telephone != null)
				{
					String sUrl = "RecipientReferencesPopup.aspx?FORMID="+"ImportConsignments" +
						"&TELEPHONENUMBER="+ txtrecipient_telephone.Text.Trim() +
						"&CIDTelephone=" + txtrecipient_telephone.ClientID + 
						"&CIDName=" + txtrecipient_name.ClientID + 
						"&CIDAddress1=" + txtrecipient_address1.ClientID + 
						"&CIDAddress2=" + txtrecipient_address2.ClientID + 
						"&CIDPostalCode=" + txtrecipient_zipcode.ClientID +  
						"&CIDContactPerson=" + txtrecipient_contact_person.ClientID + 
						"&CIDFax=" + txtrecipient_fax.ClientID ;
					
					ArrayList paramList = new ArrayList();
					paramList.Add(sUrl.Replace("#", "Flat"));

					String sScript = Utility.GetScript("openLargeWindowParam.js",paramList);
					Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
				}
			}
			else if(strCmdNm.Equals("CommodityCodeSearch"))
			{
				TextBox txtpayerid	= (TextBox)e.Item.FindControl("txtpayerid");
				TextBox txtcommodity_code	= (TextBox)e.Item.FindControl("txtcommodity_code");

				if(txtpayerid != null)
				{
					String sUrl = "CommodityPopup.aspx?FORMID=ImportConsignments"+
						"&CUSTID_TEXT="+txtpayerid.Text.Trim().ToString() +
						"&COMCODECLIENT="+txtcommodity_code.ClientID;
					ArrayList paramList = new ArrayList();
					paramList.Add(sUrl);

					String sScript = Utility.GetScript("openWindowParam.js",paramList);
					Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
				}
			}
		}


		protected void dgPackages_Bound(object sender, DataGridItemEventArgs e)
		{
			if (e.Item.DataItem != null)
			{
				if(DataBinder.Eval(e.Item.DataItem, "recordid") != System.DBNull.Value)
				{
					String recordid = DataBinder.Eval(e.Item.DataItem, "recordid").ToString();

					DataSet dsAudit = ImportConsignmentsDAL.GetAuditData(appID, enterpriseID,
						recordid, userID, "Package");

					ImageButton imgSelect = (ImageButton)e.Item.FindControl("imgSelectPKG");
					if(dsAudit.Tables[0].Rows.Count > 0 )
						imgSelect.ImageUrl  = "images/butt-red.gif";
					else
						imgSelect.ImageUrl  = "images/butt-select.gif";
				}
			}
		}

		protected void dgPackages_Cancel(object sender, DataGridCommandEventArgs e)
		{
			dgPackages.EditItemIndex = -1;
			BindPackageGrid();
		}

		public void dgPackages_Delete(object sender, DataGridCommandEventArgs e)
		{
			int iSelIndex = e.Item.ItemIndex;
			DataGridItem dgRow = dgPackages.Items[iSelIndex];
			Label lblPKGRecordid = (Label)dgRow.Cells[0].FindControl("lblPkgRecordid");
			Label lblpkg_consignment_no = (Label)dgRow.Cells[5].FindControl("lblpkg_consignment_no");

			String strRecordID = lblPKGRecordid.Text;
			String strConsignmentNo = lblpkg_consignment_no.Text;
			String strRecordIDOfConsign = "None";

			DataRow[] drConsignment = null;

			try
			{
				int iRowsDeleted = 0;
				
				if (strConsignmentNo.Trim() != "")
				{
					dsConsignment = (DataSet)Session["dsConsignment"];
					drConsignment = dsConsignment.Tables[0].Select("consignment_no = '" + strConsignmentNo + "'");

					if((drConsignment != null) && (!drConsignment.GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						if((drConsignment[0]["recordid"] != null) && 
							(!drConsignment[0]["recordid"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
							(drConsignment[0]["recordid"].ToString() != ""))
						{
							strRecordIDOfConsign = drConsignment[0]["recordid"].ToString();
						}
					}
				}
				iRowsDeleted = ImportConsignmentsDAL.DeletePacakgeByRecordId(appID, enterpriseID, strConsignmentNo,
						strRecordID, strRecordIDOfConsign, userID, (String)ViewState["userCulture"]);

				lblErrorMsg.Text = iRowsDeleted + " row(s) deleted.";
			}
			catch(ApplicationException appException)
			{
				String strMsg = appException.Message;
				if(strMsg.IndexOf("FK") != -1)
				{
					lblErrorMsg.Text =  Utility.GetLanguageText(ResourceType.UserMessage,"ERR_DEL_PATH_TRANS",utility.GetUserCulture());
				}
				if(strMsg.ToLower().IndexOf("child record found") != -1 )
				{
					lblErrorMsg.Text =  Utility.GetLanguageText(ResourceType.UserMessage,"ERR_DEL_PATH_TRANS",utility.GetUserCulture());
							
				}			
				else
				{
					lblErrorMsg.Text = strMsg;
				}

				return;
			}

			RetreivePacakgeData();
		}

		protected void dgPackages_Edit(object sender, DataGridCommandEventArgs e)
		{
			dgPackages.EditItemIndex = e.Item.ItemIndex;
			BindPackageGrid();
		}

		protected void dgPackages_PageChange(object sender, DataGridPageChangedEventArgs e)
		{
			dgPackages.CurrentPageIndex = e.NewPageIndex;
			dgPackages.SelectedIndex = -1;
			dgPackages.EditItemIndex = -1;
			BindPackageGrid();
		}

		public void dgPackages_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			int iSelIndex = dgPackages.SelectedIndex;
			DataGridItem dgRow = dgPackages.Items[iSelIndex];

			String strRecordID = "";
			Label lblPkgRecordid = (Label)dgRow.Cells[0].FindControl("lblPkgRecordid");
			if(lblPkgRecordid != null)
				strRecordID = lblPkgRecordid.Text;

			if(strRecordID != "")
			{
				DataSet dsError = ImportConsignmentsDAL.GetAuditData(appID, enterpriseID,
					strRecordID, userID, "Package");

				Session["dsError"] = dsError;
				BindErrorGrid();
			}
		}
		public void dgPackages_Update(object sender, DataGridCommandEventArgs e)
		{
			int iSelIndex = e.Item.ItemIndex;
			DataGridItem dgRow = dgPackages.Items[iSelIndex];
			Label lblPKGRecordid = (Label)dgRow.Cells[0].FindControl("lblPkgRecordid");
			String strRecordID = lblPKGRecordid.Text;
			
			fetchDatatoDsPackage(dgPackages.Items[iSelIndex], strRecordID);
			
			dsConsignment = (DataSet)Session["dsConsignment"];
			dsPackage = (DataSet)Session["dsPackage"];

			ArrayList arrOfRecordid = new ArrayList();
			foreach(DataGridItem dgi in dgPackages.Items)
			{
				Label lblPKGRecordidArr = (Label)dgi.Cells[0].FindControl("lblPkgRecordid");
				String strRecordIDArr = "";

				if(lblPKGRecordidArr != null)
					strRecordIDArr = lblPKGRecordidArr.Text;

				if(strRecordIDArr.Trim() != "")
					arrOfRecordid.Add(strRecordIDArr);
			}

			ImportConsignmentsDAL.UpdatePackage(appID, enterpriseID, dsConsignment, dsPackage,
				arrOfRecordid, userID, (String)ViewState["userCulture"]);

			dgPackages.EditItemIndex = -1;
			BindPackageGrid();

			if(strRecordID != "")
			{
				DataSet dsError = ImportConsignmentsDAL.GetAuditData(appID, enterpriseID,
					strRecordID, userID, "Package");

				Session["dsError"] = dsError;
				BindErrorGrid();
			}
		}

		public void dgPackages_Button(object sender, DataGridCommandEventArgs e)
		{

		}

	
		protected void dgError_PageChange(object sender, DataGridPageChangedEventArgs e)
		{
			dgError.CurrentPageIndex = e.NewPageIndex;
			BindErrorGrid();
		}


		public void QueryMode()
		{

			dsConsignment = ImportConsignmentsDAL.GetEmptyConsignments();
			Session["dsConsignment"] = dsConsignment;
			BindConsignmentGrid();

			dsPackage = ImportConsignmentsDAL.GetEmptyPackages();  
			Session["dsPackage"] = dsPackage;
			BindPackageGrid();

			dsError = ImportConsignmentsDAL.GetEmptyAudits();  
			Session["dsError"] = dsError;
			BindErrorGrid();
		}


		public void BindConsignmentGrid()
		{
			dsConsignment = (DataSet)Session["dsConsignment"];
			if(dsConsignment.Tables[0].Rows.Count == 0) 
			{
				dsConsignment = ImportConsignmentsDAL.GetEmptyConsignments();
				Session["dsConsignment"] = dsConsignment;
			}

			try{
				dsConsignment = (DataSet)Session["dsConsignment"];
				dgConsignment.DataSource = dsConsignment;
				dgConsignment.DataBind(); 
			
			}
			catch{
				dgConsignment.CurrentPageIndex = 0; 
				dgConsignment.DataSource = dsConsignment;
				dgConsignment.DataBind(); 
			}
			

			if(dsConsignment.Tables[0].Rows.Count == 1)
			{
				dgConsignment.Columns[46].Visible = false; //Jeab 24 Dec 10
				if(dsConsignment.Tables[0].Rows[0]["recordid"].ToString().Trim() == "")
				{
					dgConsignment.Columns[1].Visible = false; 
					dgConsignment.Columns[2].Visible = false; 
					dgConsignment.Columns[3].Visible = false;
					dgConsignment.Columns[8].Visible = false; 
					dgConsignment.Columns[10].Visible = false; 
					dgConsignment.Columns[15].Visible = false;
					dgConsignment.Columns[20].Visible = false; 
					dgConsignment.Columns[24].Visible = false;
					dgConsignment.Columns[29].Visible = false; 
					dgConsignment.Columns[33].Visible = false;
					dgConsignment.Columns[35].Visible = false;
					dgConsignment.Columns[42].Visible = false;
//					
				}
				else
				{
					dgConsignment.Columns[1].Visible = true; 
					dgConsignment.Columns[2].Visible = true; 
					dgConsignment.Columns[3].Visible = true;
					dgConsignment.Columns[8].Visible = true; 
					dgConsignment.Columns[10].Visible = true; 
					dgConsignment.Columns[15].Visible = true;
					dgConsignment.Columns[20].Visible = true; 
					dgConsignment.Columns[24].Visible = true;
					dgConsignment.Columns[29].Visible = true; 
					dgConsignment.Columns[33].Visible = true;
					dgConsignment.Columns[35].Visible = true;
					dgConsignment.Columns[42].Visible = true;
				}
			}
			else
			{
				dgConsignment.Columns[1].Visible = true; 
				dgConsignment.Columns[2].Visible = true; 
				dgConsignment.Columns[3].Visible = true;
				dgConsignment.Columns[8].Visible = true; 
				dgConsignment.Columns[10].Visible = true; 
				dgConsignment.Columns[15].Visible = true;
				dgConsignment.Columns[20].Visible = true; 
				dgConsignment.Columns[24].Visible = true;
				dgConsignment.Columns[29].Visible = true; 
				dgConsignment.Columns[33].Visible = true;
				dgConsignment.Columns[35].Visible = true;
				dgConsignment.Columns[42].Visible = true;
			}
		}


		public void BindPackageGrid()
		{
			dsPackage = (DataSet)Session["dsPackage"];

			if(dsPackage.Tables.Count > 0)
			{
				if(dsPackage.Tables[0].Rows.Count == 0) 
				{
					dsPackage = ImportConsignmentsDAL.GetEmptyPackages();
					Session["dsPackage"] = dsPackage;
				}

				
				try
				{
					dgPackages.DataSource = dsPackage.Tables[0];
					dgPackages.DataBind(); 
				}
				catch
				{
					dgPackages.CurrentPageIndex = 0; 
					dgPackages.DataSource = dsPackage;
					dgPackages.DataBind(); 
				}

				if(dsPackage.Tables[0].Rows.Count == 1)
				{
					if(dsPackage.Tables[0].Rows[0]["recordid"].ToString().Trim() == "")
					{
						dgPackages.Columns[1].Visible = false; 
						dgPackages.Columns[2].Visible = false; 
						dgPackages.Columns[3].Visible = false;
					}
					else
					{
						dgPackages.Columns[1].Visible = true; 
						dgPackages.Columns[2].Visible = true; 
						dgPackages.Columns[3].Visible = true;
					}
				}
				else
				{
					dgPackages.Columns[1].Visible = true; 
					dgPackages.Columns[2].Visible = true; 
					dgPackages.Columns[3].Visible = true;
				}

			}
			else
			{
				dsPackage = ImportConsignmentsDAL.GetEmptyPackages();
				Session["dsPackage"] = dsPackage;
			}

		}


		public void BindErrorGrid()
		{
			dsError = (DataSet)Session["dsError"];
			if(dsError.Tables[0].Rows.Count == 0) 
			{
				dsError = ImportConsignmentsDAL.GetEmptyAudits();
				Session["dsError"] = dsError;
			}

			try
			{
				dgError.DataSource = dsError;
				dgError.DataBind(); 
			}
			catch
			{
				dgError.CurrentPageIndex = 0; 
				dgError.DataSource = dsError;
				dgError.DataBind(); 
			}
	
		}


		private void RetreiveConsignmentData()
		{
			try
			{
				dsConsignment = ImportConsignmentsDAL.GetConsignmentData(appID, enterpriseID, 0, 0, userID);
				Session["dsConsignment"] = dsConsignment;
				BindConsignmentGrid();

				dsPackage = ImportConsignmentsDAL.GetEmptyPackages(); 
				Session["dsPackage"] = dsPackage;
				BindPackageGrid();

				dsError = ImportConsignmentsDAL.GetEmptyAudits(); 
				Session["dsError"] = dsError;
				BindErrorGrid();
			}

			catch( Exception ex)
			{
				lblErrorMsg.Text = ex.Message;
			}
		}


		private void RetreivePacakgeData()
		{
			if (ViewState["selectedConsignment"].ToString() != "") 
			{
				dsPackage = ImportConsignmentsDAL.GetPackageData(appID, enterpriseID, 0,0, 
					ViewState["selectedConsignment"].ToString(), userID);
			}
			else
			{
				dsPackage = ImportConsignmentsDAL.GetEmptyPackages(); 
			}

			Session["dsPackage"] = dsPackage;
			BindPackageGrid();

			dsError = ImportConsignmentsDAL.GetEmptyAudits(); 
			Session["dsError"] = dsError;
			BindErrorGrid();
		}


		private void fetchDatatoDsConsignment(DataGridItem dgItem, String recordid)
		{
			dsConsignment = (DataSet)Session["dsConsignment"];
			DataRow[] drConsignment = dsConsignment.Tables[0].Select("recordid = '" + recordid + "'");

			TextBox txtconsignment_no = (TextBox)dgItem.Cells[4].FindControl("txtconsignment_no");
			if(txtconsignment_no != null)
			{
				drConsignment[0]["consignment_no"] =  txtconsignment_no.Text;
			}
/////*****************
			msTextBox txtbooking_no = (msTextBox)dgItem.Cells[5].FindControl("txtbooking_no");
			if(txtbooking_no != null)
			{
				if(txtbooking_no.Text == "")
					drConsignment[0]["booking_no"] = DBNull.Value;
				else
					drConsignment[0]["booking_no"] =  txtbooking_no.Text;
			}

			TextBox txtref_no = (TextBox)dgItem.Cells[6].FindControl("txtref_no");
			if(txtref_no != null)
			{
				drConsignment[0]["ref_no"] =  txtref_no.Text;
			}

			TextBox txtservice_code = (TextBox)dgItem.Cells[7].FindControl("txtservice_code");
			if(txtservice_code != null)
			{
				drConsignment[0]["service_code"] =  txtservice_code.Text;

			}
			TextBox txtpayerid = (TextBox)dgItem.Cells[9].FindControl("txtpayerid");
			if(txtpayerid != null)
			{
				drConsignment[0]["payerid"] =  txtpayerid.Text;
			}

			TextBox txtpayer_name = (TextBox)dgItem.Cells[11].FindControl("txtpayer_name");
			if(txtpayer_name != null)
			{
				drConsignment[0]["payer_name"] =  txtpayer_name.Text;
			}

			TextBox txtpayer_address1 = (TextBox)dgItem.Cells[12].FindControl("txtpayer_address1");
			if(txtpayer_address1 != null)
			{
				drConsignment[0]["payer_address1"] =  txtpayer_address1.Text;
			}

			TextBox txtpayer_address2 = (TextBox)dgItem.Cells[13].FindControl("txtpayer_address2");
			if(txtpayer_address2 != null)
			{
				drConsignment[0]["payer_address2"] =  txtpayer_address2.Text;
			}

			TextBox txtpayer_zipcode = (TextBox)dgItem.Cells[14].FindControl("txtpayer_zipcode");
			if(txtpayer_zipcode != null)
			{
				drConsignment[0]["payer_zipcode"] =  txtpayer_zipcode.Text;
			}

			TextBox txtpayer_telephone = (TextBox)dgItem.Cells[16].FindControl("txtpayer_telephone");
			if(txtpayer_telephone != null)
			{
				drConsignment[0]["payer_telephone"] =  txtpayer_telephone.Text;
			}

			TextBox txtpayer_fax = (TextBox)dgItem.Cells[17].FindControl("txtpayer_fax");
			if(txtpayer_fax != null)
			{
				drConsignment[0]["payer_fax"] =  txtpayer_fax.Text;
			}

			DropDownList ddlpayment_mode = (DropDownList)dgItem.Cells[18].FindControl("ddlpayment_mode");
			if(ddlpayment_mode != null)
			{
				if(ddlpayment_mode.SelectedItem.Value.ToString().Trim() == "N")
					drConsignment[0]["payment_mode"] = System.DBNull.Value;
				else
					drConsignment[0]["payment_mode"] =  ddlpayment_mode.SelectedItem.Value;	
			}

			TextBox txtsender_name = (TextBox)dgItem.Cells[19].FindControl("txtsender_name");
			if(txtsender_name != null)
			{
				drConsignment[0]["sender_name"] =  txtsender_name.Text;
			}

			TextBox txtsender_address1 = (TextBox)dgItem.Cells[21].FindControl("txtsender_address1");
			if(txtsender_address1 != null)
			{
				drConsignment[0]["sender_address1"] =  txtsender_address1.Text;
			}

			TextBox txtsender_address2 = (TextBox)dgItem.Cells[22].FindControl("txtsender_address2");
			if(txtsender_address2 != null)
			{
				drConsignment[0]["sender_address2"] =  txtsender_address2.Text;
			}

			TextBox txtsender_zipcode = (TextBox)dgItem.Cells[23].FindControl("txtsender_zipcode");
			if(txtsender_zipcode != null)
			{
				drConsignment[0]["sender_zipcode"] =  txtsender_zipcode.Text;
			}
			TextBox txtsender_telephone = (TextBox)dgItem.Cells[25].FindControl("txtsender_telephone");
			if(txtsender_telephone != null)
			{
				drConsignment[0]["sender_telephone"] =  txtsender_telephone.Text;
			}

			TextBox txtsender_fax = (TextBox)dgItem.Cells[26].FindControl("txtsender_fax");
			if(txtsender_fax != null)
			{
				drConsignment[0]["sender_fax"] =  txtsender_fax.Text;
			}

			TextBox txtsender_contact_person = (TextBox)dgItem.Cells[27].FindControl("txtsender_contact_person");
			if(txtsender_contact_person != null)
			{
				drConsignment[0]["sender_contact_person"] =  txtsender_contact_person.Text;
			}

			TextBox txtrecipient_name = (TextBox)dgItem.Cells[28].FindControl("txtrecipient_name");
			if(txtrecipient_name != null)
			{
				drConsignment[0]["recipient_name"] =  txtrecipient_name.Text;
			}

			TextBox txtrecipient_address1 = (TextBox)dgItem.Cells[30].FindControl("txtrecipient_address1");
			if(txtrecipient_address1 != null)
			{
				drConsignment[0]["recipient_address1"] =  txtrecipient_address1.Text;
			}

			TextBox txtrecipient_address2 = (TextBox)dgItem.Cells[31].FindControl("txtrecipient_address2");
			if(txtrecipient_address2 != null)
			{
				drConsignment[0]["recipient_address2"] =  txtrecipient_address2.Text;
			}

			TextBox txtrecipient_zipcode = (TextBox)dgItem.Cells[32].FindControl("txtrecipient_zipcode");
			if(txtrecipient_zipcode != null)
			{
				drConsignment[0]["recipient_zipcode"] =  txtrecipient_zipcode.Text;
			}

			TextBox txtrecipient_telephone = (TextBox)dgItem.Cells[34].FindControl("txtrecipient_telephone");
			if(txtrecipient_telephone != null)
			{
				drConsignment[0]["recipient_telephone"] =  txtrecipient_telephone.Text;
			}

			TextBox txtrecipient_fax = (TextBox)dgItem.Cells[36].FindControl("txtrecipient_fax");
			if(txtrecipient_fax != null)
			{
				drConsignment[0]["recipient_fax"] =  txtrecipient_fax.Text;
			}

			TextBox txtrecipient_contact_person = (TextBox)dgItem.Cells[37].FindControl("txtrecipient_contact_person");
			if(txtrecipient_contact_person != null)
			{
				drConsignment[0]["recipient_contact_person"] =  txtrecipient_contact_person.Text;
			}

			TextBox txtdeclare_value = (TextBox)dgItem.Cells[38].FindControl("txtdeclare_value");
			if(txtdeclare_value != null)
			{
				drConsignment[0]["declare_value"] =  txtdeclare_value.Text;
			}

			TextBox txtcod_amount = (TextBox)dgItem.Cells[39].FindControl("txtcod_amount");
			if(txtcod_amount != null)
			{
				drConsignment[0]["cod_amount"] =  txtcod_amount.Text;
			}

			DropDownList ddlpayment_type = (DropDownList)dgItem.Cells[40].FindControl("ddlpayment_type");
			if(ddlpayment_type != null)
			{
				if(ddlpayment_type.SelectedItem.Value.ToString().Trim() == "N")
					drConsignment[0]["payment_type"] = System.DBNull.Value;
				else
					drConsignment[0]["payment_type"] =  ddlpayment_type.SelectedItem.Value;	
			}

			TextBox txtcommodity_code = (TextBox)dgItem.Cells[41].FindControl("txtcommodity_code");
			if(txtcommodity_code != null)
			{
				drConsignment[0]["commodity_code"] =  txtcommodity_code.Text;
			}

			//HC Return Task
			DropDownList ddlReturnPODHC = (DropDownList)dgItem.Cells[43].FindControl("ddlReturnPODHC");
			if(ddlReturnPODHC != null)
			{
				if(ddlReturnPODHC.SelectedItem.Value.ToString().Trim() == "D")
					drConsignment[0]["return_pod_hc"] = System.DBNull.Value;
				else
					drConsignment[0]["return_pod_hc"] =  ddlReturnPODHC.SelectedItem.Value;	
			}

			DropDownList ddlReturnInvHC = (DropDownList)dgItem.Cells[44].FindControl("ddlReturnInvHC");
			if(ddlReturnInvHC != null)
			{
				if(ddlReturnInvHC.SelectedItem.Value.ToString().Trim() == "D")
					drConsignment[0]["return_invoice_hc"] = System.DBNull.Value;
				else
					drConsignment[0]["return_invoice_hc"] =  ddlReturnInvHC.SelectedItem.Value;	
			}
			//HC Return Task

			TextBox txtremark = (TextBox)dgItem.Cells[45].FindControl("txtremark");
			if(txtremark != null)
			{
				drConsignment[0]["remark"] =  txtremark.Text;
			}

			dsConsignment.AcceptChanges();
			Session["dsConsignment"] = dsConsignment;
		}


		private void fetchDatatoDsPackage(DataGridItem dgItem, String recordid)
		{
			dsPackage = (DataSet)Session["dsPackage"];
			DataRow[] drPacakge = dsPackage.Tables[0].Select("recordid = '" + recordid + "'");

			TextBox txt_pkg_consignment_no = (TextBox)dgItem.Cells[4].FindControl("txt_pkg_consignment_no");
			if(txt_pkg_consignment_no != null)
			{
				drPacakge[0]["consignment_no"] =  txt_pkg_consignment_no.Text;
			}

			TextBox txt_mps_code = (TextBox)dgItem.Cells[5].FindControl("txt_mps_code");
			if(txt_mps_code != null)
			{
				drPacakge[0]["mps_code"] =  txt_mps_code.Text;
			}

			TextBox txt_pkg_length = (TextBox)dgItem.Cells[6].FindControl("txt_pkg_length");
			if(txt_pkg_length != null)
			{
				drPacakge[0]["pkg_length"] =  txt_pkg_length.Text;
			}

			TextBox txt_pkg_breadth = (TextBox)dgItem.Cells[7].FindControl("txt_pkg_breadth");
			if(txt_pkg_breadth != null)
			{
				drPacakge[0]["pkg_breadth"] =  txt_pkg_breadth.Text;

			}
			TextBox txt_pkg_height = (TextBox)dgItem.Cells[8].FindControl("txt_pkg_height");
			if(txt_pkg_height != null)
			{
				drPacakge[0]["pkg_height"] =  txt_pkg_height.Text;
			}

			TextBox txt_pkg_weight = (TextBox)dgItem.Cells[9].FindControl("txt_pkg_weight");
			if(txt_pkg_weight != null)
			{
				drPacakge[0]["pkg_wt"] =  txt_pkg_weight.Text;
			}

			TextBox txt_pkg_qty = (TextBox)dgItem.Cells[10].FindControl("txt_pkg_qty");
			if(txt_pkg_qty != null)
			{
				drPacakge[0]["pkg_qty"] =  txt_pkg_qty.Text;
			}


			dsPackage.AcceptChanges();
			Session["dsPackage"] = dsPackage;
		}


		private void btnImport_Click(object sender, System.EventArgs e)
		{

			try
			{
				
				int result = ImportConsignmentsDAL.ImportConsignments_Clear(appID, enterpriseID, userID);

				if(result != 0)
				{
					if(result == -1)
						lblErrorMsg.Text = "@userid parameter may not be NULL or blank.";
					else if(result == -2)
						lblErrorMsg.Text = "@userid is not a valid user ID for enterprise: PNGAF.";
					else if(result == -3)
						lblErrorMsg.Text = "@enterpriseid parameter is missing or invalid.";
					else if(result > 0)
						lblErrorMsg.Text = "SQL error message.";
				}
				else
				{
					lblTotalSavedCon.Text = "";

					if(uploadFiletoServer())
					{
						int res_Import = ImportConsignmentsDAL.ImportConsignments_Import(appID, enterpriseID, userID, ViewState["FileName"].ToString());
						if(res_Import == 0)
						{	
//							try
//							{
//							//Cleare existing data
//							ImportConsignmentsDAL.ClearTempDataByUserId(appID, enterpriseID, userID);
//
//							//Get data from Excel WorkSheet
//							getExcelFiles();
//
//							dsConsignment = (DataSet) Session["dsConsignment"];
//							dsPackage = (DataSet) Session["dsPackage"];
//
//							//Audit and Insert all data into temp table
//							ImportConsignmentsDAL.ImportToTempTables(appID, enterpriseID,
//								dsConsignment, dsPackage, (String)ViewState["userCulture"], userID);

							RetreiveConsignmentData();

//							}
//							catch
//							{
//								throw new ApplicationException("Error during import data", null);
//							}
							btnSave.Enabled = true;
						}
						else
						{
							
							if(res_Import != 0)
							{
								if(res_Import == -1)
									lblErrorMsg.Text = "@userid parameter may not be NULL or blank.";
								else if(res_Import == -2)
									lblErrorMsg.Text = "@userid is not a valid user ID for enterprise: PNGAF.";
								else if(res_Import == -3)
									lblErrorMsg.Text = "@enterpriseid parameter is missing or invalid.";
								else if(res_Import == -7)
									lblErrorMsg.Text = "@fn(file name) parameter may not be NULL or an empty string.";
								else if(res_Import == -8)
									lblErrorMsg.Text = "@fn(file name) extention may be .xls or .xlsx only.";
								else if(res_Import == -9)
									lblErrorMsg.Text = "@fn(file name) specified does not exist.";
								else if(res_Import == -10)
									lblErrorMsg.Text = "Failure on attempt to read: <fn> Sheet: Consignment check that the Excel worksheet is not open.";
								else if(res_Import == -11)
									lblErrorMsg.Text = "Failure on attempt to read: <fn> Sheet: Package check that the Excel worksheet is not open.";
								else if(res_Import > 0)
									lblErrorMsg.Text = "SQL error message.";
							}

						}
					}
				}
			}
			catch(Exception ex)
			{
				
				lblErrorMsg.Text = ex.Message.ToString();
				txtFilePath.Text = "";
			}
		}


		private bool uploadFiletoServer()
		{
			bool status = true;
			//inFile = (HtmlInputFile)Session["inFile"];
			if((inFile.PostedFile != null ) && inFile.PostedFile.ContentLength > 0)
			{
				string extension = System.IO.Path.GetExtension(inFile.PostedFile.FileName);

				if(extension.ToUpper() == ".XLS" || extension.ToUpper() == ".XLSX")
				{
					string fn = System.IO.Path.GetFileName(inFile.PostedFile.FileName);
//					string pathShare = Server.MapPath("Excel") + "\\";
//					string mappedPath = Server.MapPath("Excel") + "\\" + userID + "_" + fn;
					
					string pathShare=(string)System.Configuration.ConfigurationSettings.AppSettings["ImportStatusSharedPath"];
					string mappedPath = pathShare + "\\" + userID + "_" + fn;

					ViewState["FileName"] = mappedPath;

					try
					{
						if(System.IO.Directory.Exists(pathShare) == false)
							System.IO.Directory.CreateDirectory(pathShare);

						if (System.IO.File.Exists(mappedPath)) 
							System.IO.File.Delete(mappedPath);

						inFile.PostedFile.SaveAs(mappedPath);

						lblErrorMsg.Text = "";
					}
					catch(Exception ex)
					{
						throw new ApplicationException("Error during upload file to the server.", null);
					}
				}
				else
				{
					lblErrorMsg.Text = "Not a valid Excel Workbook.";
					txtFilePath.Text = "";
					status = false;
				}
			}
			else
			{
				lblErrorMsg.Text = "Please select a file to upload.";
				status = false;
			}

			return status;
		}


		private void getExcelFiles()
		{
			int i = 0;

			try
			{	
				String connString = ConfigurationSettings.AppSettings["ExcelConn"];
				connString = connString.Replace("(DestinationFile)", (String)ViewState["FileName"]);

				int recCouter = 1;

				StringBuilder strBuilder = new StringBuilder();
				strBuilder.Append("SELECT rtrim(ltrim(booking_no)) as booking_no, ");
				strBuilder.Append("rtrim(ltrim(consignment_no)) as consignment_no, ");
				strBuilder.Append("rtrim(ltrim(ref_no)) as ref_no, ");
				strBuilder.Append("rtrim(ltrim(payerid)) as payerid, ");
				strBuilder.Append("rtrim(ltrim(payer_name)) as payer_name, ");
				strBuilder.Append("rtrim(ltrim(payer_address1)) as payer_address1, ");
				strBuilder.Append("rtrim(ltrim(payer_address2)) as payer_address2, ");
				strBuilder.Append("rtrim(ltrim(payer_zipcode)) as payer_zipcode, ");
				strBuilder.Append("payer_telephone, payer_fax, payment_mode, ");
				strBuilder.Append("sender_name, sender_address1, sender_address2, sender_zipcode, ");
				strBuilder.Append("sender_telephone, sender_fax, sender_contact_person, ");
				strBuilder.Append("recipient_name, recipient_address1, recipient_address2, recipient_zipcode, ");
				strBuilder.Append("recipient_telephone, recipient_fax, recipient_contact_person, ");
				strBuilder.Append("rtrim(ltrim(service_code)) as service_code, ");
				strBuilder.Append("declare_value, payment_type, commodity_code, ");
				//HC Return Task
				strBuilder.Append("remark, cod_amount, return_pod_hc, return_invoice_hc ");
				//HC Return Task
				strBuilder.Append("FROM [Consignments$] ");
				OleDbDataAdapter daConsignment = new OleDbDataAdapter(strBuilder.ToString(), connString);

				DataSet dsExcelConsignment = new DataSet();
				daConsignment.Fill(dsExcelConsignment);

				dsConsignment = ImportConsignmentsDAL.GetEmptyConsignments();
				if(dsConsignment.Tables[0].Rows.Count == 1)
				{
					dsConsignment.Tables[0].Rows[0].Delete();
					dsConsignment.AcceptChanges();
				}

				foreach(DataRow dr in dsExcelConsignment.Tables[0].Rows)
				{
					if((dr["booking_no"].ToString().Trim() != "") ||
						(dr["consignment_no"].ToString().Trim() != "") ||
						(dr["ref_no"].ToString().Trim() != "") || 
						(dr["payerid"].ToString().Trim() != "") ||
						(dr["payer_name"].ToString().Trim() != "") ||
						(dr["payer_address1"].ToString().Trim() != "") ||
						(dr["payer_address2"].ToString().Trim() != "") ||
						(dr["payer_zipcode"].ToString().Trim() != "") ||
						(dr["payer_telephone"].ToString().Trim() != "") ||
						(dr["payer_fax"].ToString().Trim() != "") || 
						(dr["payment_mode"].ToString().Trim() != "") ||
						(dr["sender_name"].ToString().Trim() != "") ||
						(dr["sender_address1"].ToString().Trim() != "") ||
						(dr["sender_address2"].ToString().Trim() != "") || 
						(dr["sender_zipcode"].ToString().Trim() != "") ||
						(dr["sender_telephone"].ToString().Trim() != "") ||
						(dr["sender_fax"].ToString().Trim() != "") ||
						(dr["sender_contact_person"].ToString().Trim() != "") ||
						(dr["recipient_name"].ToString().Trim() != "") ||
						(dr["recipient_address1"].ToString().Trim() != "") ||
						(dr["recipient_address2"].ToString().Trim() != "") ||
						(dr["recipient_zipcode"].ToString().Trim() != "") ||
						(dr["recipient_telephone"].ToString().Trim() != "") ||
						(dr["recipient_fax"].ToString().Trim() != "") ||
						(dr["recipient_contact_person"].ToString().Trim() != "") ||
						(dr["service_code"].ToString().Trim() != "") ||
						(dr["declare_value"].ToString().Trim() != "") ||
						(dr["payment_type"].ToString().Trim() != "") ||
						(dr["commodity_code"].ToString().Trim() != "") ||
						(dr["remark"].ToString().Trim() != "") ||
						(dr["cod_amount"].ToString().Trim() != "") ||//HC Return Task
						(dr["return_pod_hc"].ToString().Trim() != "") ||
						(dr["return_invoice_hc"].ToString().Trim() != ""))//HC Return Task
					{
						DataRow tmpDr = dsConsignment.Tables[0].NewRow();

						tmpDr["recordid"] = recCouter.ToString();
						recCouter++;
						string ConNo="";
						if(Utility.ValidateConsignmentNo(dr["consignment_no"].ToString().Trim(), ref ConNo)==false)
						{							
							throw new Exception(Utility.GetLanguageText(ResourceType.UserMessage,"CREATE_SIP_MSG_CONSNO_INVALID",utility.GetUserCulture()));
						}

						tmpDr["booking_no"] = Convert.ToString(dr["booking_no"]).Trim();
						tmpDr["consignment_no"] = Convert.ToString(dr["consignment_no"]).Trim();
						tmpDr["ref_no"] = Convert.ToString(dr["ref_no"]).TrimStart().TrimEnd();
						tmpDr["payerid"] = Convert.ToString(dr["payerid"]).TrimStart().TrimEnd();
						tmpDr["payer_name"] = Convert.ToString(dr["payer_name"]).TrimStart().TrimEnd();
						tmpDr["payer_address1"] = Convert.ToString(dr["payer_address1"]).TrimStart().TrimEnd();
						tmpDr["payer_address2"] = Convert.ToString(dr["payer_address2"]).TrimStart().TrimEnd();
						tmpDr["payer_zipcode"] = Convert.ToString(dr["payer_zipcode"]).Trim();
						tmpDr["payer_telephone"] = Convert.ToString(dr["payer_telephone"]).TrimStart().TrimEnd();
						tmpDr["payer_fax"] = Convert.ToString(dr["payer_fax"]).TrimStart().TrimEnd();
						tmpDr["payment_mode"] = Convert.ToString(dr["payment_mode"]).TrimStart().TrimEnd();
						tmpDr["sender_name"] = Convert.ToString(dr["sender_name"]).TrimStart().TrimEnd();
						tmpDr["sender_address1"] = Convert.ToString(dr["sender_address1"]).TrimStart().TrimEnd();
						tmpDr["sender_address2"] = Convert.ToString(dr["sender_address2"]).TrimStart().TrimEnd();
						tmpDr["sender_zipcode"] = Convert.ToString(dr["sender_zipcode"]).Trim();
						tmpDr["sender_telephone"] = Convert.ToString(dr["sender_telephone"]).TrimStart().TrimEnd();
						tmpDr["sender_fax"] = Convert.ToString(dr["sender_fax"]).TrimStart().TrimEnd();
						tmpDr["sender_contact_person"] = Convert.ToString(dr["sender_contact_person"]).TrimStart().TrimEnd();
						tmpDr["recipient_name"] = Convert.ToString(dr["recipient_name"]).TrimStart().TrimEnd();
						tmpDr["recipient_address1"] = Convert.ToString(dr["recipient_address1"]).TrimStart().TrimEnd();
						tmpDr["recipient_address2"] = Convert.ToString(dr["recipient_address2"]).TrimStart().TrimEnd();
						tmpDr["recipient_zipcode"] = Convert.ToString(dr["recipient_zipcode"]).Trim();
						tmpDr["recipient_telephone"] = Convert.ToString(dr["recipient_telephone"]).TrimStart().TrimEnd();
						tmpDr["recipient_fax"] = Convert.ToString(dr["recipient_fax"]).TrimStart().TrimEnd();
						tmpDr["recipient_contact_person"] = Convert.ToString(dr["recipient_contact_person"]).TrimStart().TrimEnd();
						tmpDr["service_code"] = Convert.ToString(dr["service_code"]).TrimStart().TrimEnd();
						tmpDr["declare_value"] = Convert.ToString(dr["declare_value"]).TrimStart().TrimEnd();
						tmpDr["payment_type"] = Convert.ToString(dr["payment_type"]).TrimStart().TrimEnd();
						tmpDr["commodity_code"] = Convert.ToString(dr["commodity_code"]).TrimStart().TrimEnd();
						tmpDr["remark"] = Convert.ToString(dr["remark"]).TrimStart().TrimEnd();
						tmpDr["cod_amount"] = Convert.ToString(dr["cod_amount"]).TrimStart().TrimEnd();
						tmpDr["new_account"] = "N";//HC Return Task
						tmpDr["return_pod_hc"] = Convert.ToString(dr["return_pod_hc"]).TrimStart().TrimEnd();
						tmpDr["return_invoice_hc"] = Convert.ToString(dr["return_invoice_hc"]).TrimStart().TrimEnd();//HC Return Task

						dsConsignment.Tables[0].Rows.Add(tmpDr);
						dsConsignment.AcceptChanges();
					}					
				}

				dsExcelConsignment.Dispose();
				dsExcelConsignment = null;

				i++;
				recCouter = 1;

				strBuilder = new StringBuilder();
				strBuilder.Append("SELECT rtrim(ltrim(consignment_no)) as consignment_no, ");
				strBuilder.Append("rtrim(ltrim(mps_code)) as mps_code, ");
				strBuilder.Append("pkg_length, pkg_breadth, pkg_height, pkg_wt, pkg_qty ");
				strBuilder.Append("FROM [Packages$] ");
				OleDbDataAdapter daPackage  = new OleDbDataAdapter(strBuilder.ToString(), connString);

				DataSet dsExcelPackage = new DataSet();
				daPackage.Fill(dsExcelPackage);

				dsPackage = ImportConsignmentsDAL.GetEmptyPackages(); 
				if(dsPackage.Tables[0].Rows.Count == 1)
				{
					dsPackage.Tables[0].Rows[0].Delete();
					dsPackage.AcceptChanges();
				}

				foreach(DataRow dr in dsExcelPackage.Tables[0].Rows)
				{
					if((dr["consignment_no"].ToString().Trim() != "") ||
						(dr["mps_code"].ToString().Trim() != "") || 
						(dr["pkg_length"].ToString().Trim() != "") ||
						(dr["pkg_breadth"].ToString().Trim() != "") ||
						(dr["pkg_height"].ToString().Trim() != "") ||
						(dr["pkg_wt"].ToString().Trim() != "") ||
						(dr["pkg_qty"].ToString().Trim() != ""))
					{
						DataRow tmpDr = dsPackage.Tables[0].NewRow();

						tmpDr["recordid"] = recCouter.ToString();
						recCouter++;

						tmpDr["consignment_no"] = Convert.ToString(dr["consignment_no"]);
						tmpDr["mps_code"] = Convert.ToString(dr["mps_code"]);
						tmpDr["pkg_length"] = Convert.ToString(dr["pkg_length"]);
						tmpDr["pkg_breadth"] = Convert.ToString(dr["pkg_breadth"]);
						tmpDr["pkg_height"] = Convert.ToString(dr["pkg_height"]);
						tmpDr["pkg_wt"] = Convert.ToString(dr["pkg_wt"]);
						tmpDr["pkg_qty"] = Convert.ToString(dr["pkg_qty"]);

						dsPackage.Tables[0].Rows.Add(tmpDr);
						dsPackage.AcceptChanges();
					}
				}

				dsExcelPackage.Dispose();
				dsExcelPackage = null;

				Session["dsConsignment"] = dsConsignment;
				Session["dsPackage"] = dsPackage;
			}
			catch(Exception ex)
			{
				String strMsg = ex.Message;

				if(strMsg.IndexOf("'Consignments$' is not a valid name") != -1)
				{
					throw new ApplicationException("Excel Workbook must contain two named sheets: Consignments and Packages.", null);
				}
				else if(strMsg.IndexOf("'Packages$' is not a valid name") != -1)
				{
					throw new ApplicationException("Excel Workbook must contain two named sheets: Consignments and Packages.", null);
				}
				else if(strMsg.IndexOf("No value given for one or more required parameters") != -1)
				{
					if(i == 0)
						throw new ApplicationException("Consignments worksheet has invalid columns", null);
					else
						throw new ApplicationException("Packages worksheet has invalid columns", null);
				}
				else
				{
					throw ex;
				}
			}
		}


//		private void btnSave_Click(object sender, System.EventArgs e)
//		{//int countRec = 0;
//			int result = ImportConsignmentsDAL.ImportConsignments_Save(appID, enterpriseID, userID);
//
//			if(result != 0)
//			{
//				if(result != 0)
//				{
//					if(result == -1)
//						lblErrorMsg.Text = "@userid parameter may not be NULL or blank.";
//					else if(result == -2)
//						lblErrorMsg.Text = "@userid is not a valid user ID for enterprise: PNGAF.";
//					else if(result == -3)
//						lblErrorMsg.Text = "@enterpriseid parameter is missing or invalid.";
//					else if(result > 0)
//						lblErrorMsg.Text = "SQL error message.";
//				}
//			}
//			else
//			{
//				int importedCons = 0;
//				lblTotalSavedCon.Text = "";
//				ArrayList arCon_No =  new ArrayList(); 
//			
//				dsConsignment = ImportConsignmentsDAL.GetAuditedConsignment(appID, enterpriseID,
//					(String)ViewState["userCulture"], userID);
//
//				for(int i=0;i< dsConsignment.Tables[0].Rows.Count;i++)
//				{
//					arCon_No.Add(dsConsignment.Tables[0].Rows[0]["consignment_no"]);
//				}
//
//				dsPackage = ImportConsignmentsDAL.GetAuditedPackage(appID, enterpriseID,
//					(String)ViewState["userCulture"], userID);
//
//
//				////// by Tumz 31.03.54 ///////
//				if(dsPackage != null && dsPackage.Tables[0].Rows.Count > 0)
//				{
//					string[] conDelImportShipment_PKG = new string[dsPackage.Tables[0].Rows.Count];
//
//					for(int i=0;i<dsPackage.Tables[0].Rows.Count-1;i++)
//					{
//						DataSet dsShipment_PKG = ImportConsignmentsDAL.GetShipment_PKG(appID, enterpriseID, Convert.ToString(dsPackage.Tables[0].Rows[i]["consignment_no"]));
//						if(dsShipment_PKG != null && dsShipment_PKG.Tables[0].Rows.Count > 0)
//						{
//							for(int j=0;j<dsShipment_PKG.Tables[0].Rows.Count;j++)
//							{
//								if(((decimal)dsPackage.Tables[0].Rows[i]["mps_code"] !=  (decimal)dsShipment_PKG.Tables[0].Rows[i]["mps_code"]) || 
//									((decimal)dsPackage.Tables[0].Rows[i]["pkg_wt"] !=  (decimal)dsShipment_PKG.Tables[0].Rows[i]["pkg_wt"]) ||
//									((int)dsPackage.Tables[0].Rows[i]["pkg_qty"] !=  (int)dsShipment_PKG.Tables[0].Rows[i]["pkg_qty"])||
//									!((((decimal)dsPackage.Tables[0].Rows[i]["pkg_length"] == (decimal)dsShipment_PKG.Tables[0].Rows[i]["pkg_length"] && (decimal)dsPackage.Tables[0].Rows[i]["pkg_length"] ==  (decimal)dsShipment_PKG.Tables[0].Rows[i]["pkg_breadth"]) && (decimal)dsPackage.Tables[0].Rows[i]["pkg_length"] ==  (decimal)dsShipment_PKG.Tables[0].Rows[i]["pkg_height"] )&&
//									(((decimal)dsPackage.Tables[0].Rows[i]["pkg_breadth"] == (decimal)dsShipment_PKG.Tables[0].Rows[i]["pkg_length"] && (decimal)dsPackage.Tables[0].Rows[i]["pkg_breadth"] ==  (decimal)dsShipment_PKG.Tables[0].Rows[i]["pkg_breadth"]) && (decimal)dsPackage.Tables[0].Rows[i]["pkg_breadth"] ==  (decimal)dsShipment_PKG.Tables[0].Rows[i]["pkg_height"] )&&
//									(((decimal)dsPackage.Tables[0].Rows[i]["pkg_height"] == (decimal)dsShipment_PKG.Tables[0].Rows[i]["pkg_length"] && (decimal)dsPackage.Tables[0].Rows[i]["pkg_height"] ==  (decimal)dsShipment_PKG.Tables[0].Rows[i]["pkg_breadth"]) && (decimal)dsPackage.Tables[0].Rows[i]["pkg_height"] ==  (decimal)dsShipment_PKG.Tables[0].Rows[i]["pkg_height"] ))
//									)
//									conDelImportShipment_PKG[i] = dsPackage.Tables[0].Rows[i]["consignment_no"].ToString();
//							}
//						}
//					}
//
//					for(int j=conDelImportShipment_PKG.Length-1;j>=0;j--)
//					{
//						if(conDelImportShipment_PKG[j] != null)
//						{
//							for(int i=dsPackage.Tables[0].Rows.Count-1;i>=0;i--)
//							{
//								if(conDelImportShipment_PKG[j] == dsPackage.Tables[0].Rows[i]["consignment_no"].ToString())
//									dsPackage.Tables[0].Rows[i].Delete();
//							}	
//							dsPackage.AcceptChanges();
//						}
//					}
//
//				}
//			
//
//
//
//				Enterprise enterprise = new Enterprise();
//				enterprise.Populate(utility.GetAppID(),utility.GetEnterpriseID());
//
//				Zipcode zipCode = new Zipcode();
//				Customer customer = new Customer();
//
//				decimal fDensityFactor = (decimal)enterprise.DensityFactor;
//				int wt_rounding_method = 0;
//				decimal wt_increment_amt = 1;
//
//				wt_rounding_method = (int)enterprise.RoundingMethod;
//				wt_increment_amt = (decimal)enterprise.IncrementWt;
//			
//				//Filter Con by PicupDate
//				bool pickupErrorFlag = false;
//				string dayOfPickUp = "";
//				string PUPErrorMsg = "";
//				int cntDS = 0; 
//				bool isEmpty = false;
//				DataSet preShipRec = null;
//				if(!(cntDS <= dsConsignment.Tables[0].Rows.Count - 1))
//				{
//					isEmpty = true;
//				}
////countRec = dsConsignment.Tables[0].Rows.Count;
//				while(cntDS <= dsConsignment.Tables[0].Rows.Count - 1)
//				{
//					String tmpStrDate = System.DateTime.Now.ToString("dd/MM/yyyy HH:mm");
//				
//					if((dsConsignment.Tables[0].Rows[cntDS]["booking_no"] != null) && 
//						(!dsConsignment.Tables[0].Rows[cntDS]["booking_no"].GetType().Equals(System.Type.GetType("System.DBNull"))) && 
//						(dsConsignment.Tables[0].Rows[cntDS]["booking_no"].ToString() != ""))
//					{
//						DataSet dsBooking = ImportConsignmentsDAL.GetBookingDetailsByBookingNo(appID, enterpriseID, dsConsignment.Tables[0].Rows[cntDS]["booking_no"].ToString());
//
//						if(dsBooking.Tables[0].Rows.Count > 0)
//						{
//							if((dsBooking.Tables[0].Rows[0]["act_pickup_datetime"] != null) && 
//								(!dsBooking.Tables[0].Rows[0]["act_pickup_datetime"].GetType().Equals(System.Type.GetType("System.DBNull"))) && 
//								(dsBooking.Tables[0].Rows[0]["act_pickup_datetime"].ToString() != ""))
//							{
//								DateTime dtTmp = (DateTime)dsBooking.Tables[0].Rows[0]["act_pickup_datetime"];
//								tmpStrDate = dtTmp.ToString("dd/MM/yyyy HH:mm");
//							}
//						}	
//
//						dsBooking.Dispose();
//						dsBooking = null;
//					}	
//
//					DataSet dsEprise = SysDataManager1.GetEnterpriseProfile(appID,enterpriseID);
//					DateTime dtTMPDate = System.DateTime.ParseExact(tmpStrDate,"dd/MM/yyyy HH:mm",null); 
//	
//					// Saturday
//					if(dtTMPDate.DayOfWeek == System.DayOfWeek.Saturday)
//					{
//						if((dsEprise.Tables[0].Rows[0]["sat_pickup_avail"] != null) && 
//							(!dsEprise.Tables[0].Rows[0]["sat_pickup_avail"].GetType().Equals(System.Type.GetType("System.DBNull"))) && 
//							(dsEprise.Tables[0].Rows[0]["sat_pickup_avail"].ToString() != "") &&	
//							(dsEprise.Tables[0].Rows[0]["sat_pickup_avail"].ToString() != "N")
//							)					
//						{
//							cntDS++;
//						}
//						else 
//						{
//							dsConsignment.Tables[0].Rows[cntDS].Delete();
//							dsConsignment.AcceptChanges();
//							pickupErrorFlag = true;
//							cntDS = 0;
//							PUPErrorMsg = "Pickup on Saturday is not available.";
//						}
//					}
//
//					// Sunday
//					if(dtTMPDate.DayOfWeek == System.DayOfWeek.Sunday)
//					{
//						if((dsEprise.Tables[0].Rows[0]["sun_pickup_avail"] != null) && 
//							(!dsEprise.Tables[0].Rows[0]["sun_pickup_avail"].GetType().Equals(System.Type.GetType("System.DBNull"))) && 
//							(dsEprise.Tables[0].Rows[0]["sun_pickup_avail"].ToString() != "") &&	
//							(dsEprise.Tables[0].Rows[0]["sun_pickup_avail"].ToString() != "N")
//							)					
//						{
//							cntDS++;
//						}
//						else 
//						{
//							dsConsignment.Tables[0].Rows[cntDS].Delete();
//							dsConsignment.AcceptChanges();
//							pickupErrorFlag = true;
//							cntDS = 0;
//							PUPErrorMsg = "Pickup on Sunday is not available.";
//						}
//					}
//
//					// Monday - Friday
//					if((dtTMPDate.DayOfWeek != System.DayOfWeek.Saturday) && (dtTMPDate.DayOfWeek != System.DayOfWeek.Sunday))
//					{
//						Zipcode zipCodeCheck = new Zipcode();
//						zipCodeCheck.Populate(appID,enterpriseID, dsConsignment.Tables[0].Rows[cntDS]["payer_zipcode"].ToString());
//
//						bool isPubDay = DomesticShipmentMgrDAL.IsEnterpriseHolidayOfAutoManifest(appID, enterpriseID, dtTMPDate, zipCode.StateCode);
//
//						if(isPubDay)
//						{
//							dsConsignment.Tables[0].Rows[cntDS].Delete();
//							dsConsignment.AcceptChanges();
//							pickupErrorFlag = true;
//							cntDS = 0;
//							PUPErrorMsg = "Pickup on Public Holidays is not available.";
//						}
//						else 
//						{
//							cntDS++;
//						}
//					}
//				}
//				//Filter Con by PicupDate
//
//				foreach(DataRow dr in dsConsignment.Tables[0].Rows)
//				{
//					try 
//					{
//						DataSet FinalDsConsignment = DomesticShipmentMgrDAL.GetEmptyDomesticShipDS();
//						DataSet FinalDsPackage = DomesticShipmentMgrDAL.GetEmptyPkgDtlDS();
//						DataSet FinalDsVAS = DomesticShipmentMgrDAL.GetEmptyVASDS();
//						DataSet TmpDsVAS = DomesticShipmentMgrDAL.GetEmptyVASDS();
//
//						if(FinalDsConsignment.Tables[0].Rows.Count == 1) 
//						{
//							FinalDsConsignment.Tables[0].Rows[0].Delete();
//							FinalDsConsignment.AcceptChanges();
//						}
//
//						if(FinalDsPackage.Tables[0].Rows.Count == 1) 
//						{
//							FinalDsPackage.Tables[0].Rows[0].Delete();
//							FinalDsPackage.AcceptChanges();
//						}
//
//						if(FinalDsVAS.Tables[0].Rows.Count == 1) 
//						{
//							FinalDsVAS.Tables[0].Rows[0].Delete();
//							FinalDsVAS.AcceptChanges();
//						}
//
//						if(TmpDsVAS.Tables[0].Rows.Count == 1) 
//						{
//							TmpDsVAS.Tables[0].Rows[0].Delete();
//							TmpDsVAS.AcceptChanges();
//						}
//						long booking_no = 0;
//						String strApplyDimWt = "";
//						//String strApplyESA = "";
//						String strDimBytot = ""; //Jeab 12 Jan 11
//
//						if((dr["payerid"] != null) && 
//							(!dr["payerid"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
//							(dr["payerid"].ToString() != ""))
//						{
//							if (dr["payerid"].ToString().Trim().ToUpper() != "NEW")
//							{
//								customer.Populate(appID,enterpriseID, dr["payerid"].ToString());
//								strApplyDimWt = customer.ApplyDimWt;
//								//strApplyESA = customer.ESASurcharge;
//								strDimBytot = customer.Dim_By_tot;  //Jeab 12 Jan 11
//							}
//							else
//							{
//								strApplyDimWt = "N";
//								//strApplyESA = "N";
//								strDimBytot = "N";  //Jeab 12 Jan 11
//							}
//						}
//						else
//						{
//							strApplyDimWt = "N";
//							//strApplyESA = "N";
//							strDimBytot = "N";  //Jeab 12 Jan 11
//						}
//
//					
//						DataRow[] drPackage = dsPackage.Tables[0].Select("consignment_no = '" + dr["consignment_no"].ToString() + "'");
//					
//						//Jeab 13 Jan 11
//						decimal decTotDimWt = 0;
//						decimal decTotWt = 0;
//						foreach(DataRow drLocal in drPackage)
//						{
//							decimal tot_wt = 0;
//							decimal tot_dim_wt = 0;
//							decimal iWeight =0;
//							iWeight=Convert.ToDecimal(drLocal["pkg_wt"]);
//							if(iWeight<1)
//							{
//								iWeight = 1;
//							}
//							else
//							{
//								iWeight = EnterpriseRounding(iWeight);
//							}
//							
//							tot_wt = TIESUtility.EnterpriseRounding((iWeight * Convert.ToInt32(drLocal["pkg_qty"])), wt_rounding_method, wt_increment_amt);
//							//tot_wt = Convert.ToDecimal(drLocal["pkg_wt"])*Convert.ToInt32(drLocal["pkg_qty"]);  //Jeab 22 Feb 2011
//							decTotWt += tot_wt;
//
//							if((dr["payerid"] != null) && 
//								(!dr["payerid"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
//								(dr["payerid"].ToString() != ""))
//							{
//								if (dr["payerid"].ToString().Trim().ToUpper() != "NEW")
//								{
//									DataSet dscustInfo = SysDataMgrDAL.GetCustInforByCustID(utility.GetAppID(),
//										utility.GetEnterpriseID(), 0, 0, dr["payerid"].ToString()).ds;
//									if (dscustInfo.Tables[0].Rows.Count > 0)
//									{
//										if((dscustInfo.Tables[0].Rows[0]["density_factor"] != null) && 
//											(!dscustInfo.Tables[0].Rows[0]["density_factor"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
//											(dscustInfo.Tables[0].Rows[0]["density_factor"].ToString() != ""))
//										{
//											fDensityFactor = (decimal)dscustInfo.Tables[0].Rows[0]["density_factor"];
//										}
//										else
//										{
//											fDensityFactor = (decimal)enterprise.DensityFactor;
//										}
//									}
//									else
//									{
//										fDensityFactor = (decimal)enterprise.DensityFactor;
//									}
//								}
//								else
//								{
//									fDensityFactor = (decimal)enterprise.DensityFactor;
//								}
//							}
//							else
//							{
//								fDensityFactor = (decimal)enterprise.DensityFactor;
//							}
//							//************* density_factor for Flexible Rate ********************//
//							
//							//TU 14/07/08
//							decimal fDimWt = 0;
//							if(fDensityFactor > 0)
//							{
//								fDimWt = Convert.ToDecimal(drLocal["pkg_length"]) * Convert.ToDecimal(drLocal["pkg_breadth"]) * Convert.ToDecimal(drLocal["pkg_height"]) / fDensityFactor;
//							}
//							fDimWt = EnterpriseRounding(fDimWt);
//							if(fDimWt<1)
//							{
//								fDimWt=1;
//							}
//							tot_dim_wt = TIESUtility.EnterpriseRounding(fDimWt, wt_rounding_method, wt_increment_amt) * Convert.ToInt32(drLocal["pkg_qty"]);
//
//							decTotDimWt += tot_dim_wt;
//						}
//						//Jeab 13 Jan 11  =========> End
//
//						foreach(DataRow drLocal in drPackage)
//						{
//							DataRow tmpDrPKG = FinalDsPackage.Tables[0].NewRow();
//							decimal tot_wt = 0;
//							decimal tot_dim_wt = 0;
//				
//							tmpDrPKG["mps_no"] = Convert.ToString(drLocal["mps_code"]);
//							tmpDrPKG["pkg_length"] = Convert.ToDecimal(drLocal["pkg_length"]);
//							tmpDrPKG["pkg_breadth"] = Convert.ToDecimal(drLocal["pkg_breadth"]);
//							tmpDrPKG["pkg_height"] = Convert.ToDecimal(drLocal["pkg_height"]);
//							tmpDrPKG["pkg_wt"] = Convert.ToDecimal(drLocal["pkg_wt"]);
//							tmpDrPKG["pkg_qty"] = Convert.ToInt32(drLocal["pkg_qty"]);
//							tmpDrPKG["pkg_volume"] = Math.Round((Convert.ToDecimal(drLocal["pkg_length"]) * Convert.ToDecimal(drLocal["pkg_breadth"]) * Convert.ToDecimal(drLocal["pkg_height"])), 2);
//
//							//TU 14/07/08
//							decimal iWeight =0;
//							iWeight=Convert.ToDecimal(drLocal["pkg_wt"]);
//							if(iWeight<1)
//							{
//								iWeight = 1;
//							}
//							else
//							{
//								iWeight = EnterpriseRounding(iWeight);
//							}
//						
//							tot_wt = TIESUtility.EnterpriseRounding((iWeight * Convert.ToInt32(drLocal["pkg_qty"])), wt_rounding_method, wt_increment_amt);
//							//
//						
//							tmpDrPKG["tot_wt"] = tot_wt;
//
//							tmpDrPKG["tot_act_wt"] = Convert.ToDecimal(drLocal["pkg_wt"])*Convert.ToInt32(drLocal["pkg_qty"]);//TU 08/07/08
//
//							//************* density_factor for Flexible Rate ********************//
//							if((dr["payerid"] != null) && 
//								(!dr["payerid"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
//								(dr["payerid"].ToString() != ""))
//							{
//								if (dr["payerid"].ToString().Trim().ToUpper() != "NEW")
//								{
//									DataSet dscustInfo = SysDataMgrDAL.GetCustInforByCustID(utility.GetAppID(),
//										utility.GetEnterpriseID(), 0, 0, dr["payerid"].ToString()).ds;
//									if (dscustInfo.Tables[0].Rows.Count > 0)
//									{
//										if((dscustInfo.Tables[0].Rows[0]["density_factor"] != null) && 
//											(!dscustInfo.Tables[0].Rows[0]["density_factor"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
//											(dscustInfo.Tables[0].Rows[0]["density_factor"].ToString() != ""))
//										{
//											fDensityFactor = (decimal)dscustInfo.Tables[0].Rows[0]["density_factor"];
//										}
//										else
//										{
//											fDensityFactor = (decimal)enterprise.DensityFactor;
//										}
//									}
//									else
//									{
//										fDensityFactor = (decimal)enterprise.DensityFactor;
//									}
//								}
//								else
//								{
//									fDensityFactor = (decimal)enterprise.DensityFactor;
//								}
//							}
//							else
//							{
//								fDensityFactor = (decimal)enterprise.DensityFactor;
//							}
//							//************* density_factor for Flexible Rate ********************//
//						
//							//TU 14/07/08
//							decimal fDimWt = 0;
//							if(fDensityFactor > 0)
//							{
//								fDimWt = Convert.ToDecimal(drLocal["pkg_length"]) * Convert.ToDecimal(drLocal["pkg_breadth"]) * Convert.ToDecimal(drLocal["pkg_height"]) / fDensityFactor;
//							}
//							fDimWt = EnterpriseRounding(fDimWt);
//							if(fDimWt<1)
//							{
//								fDimWt=1;
//							}
//							tot_dim_wt = TIESUtility.EnterpriseRounding(fDimWt, wt_rounding_method, wt_increment_amt) * Convert.ToInt32(drLocal["pkg_qty"]);
//							//	
//						
//							tmpDrPKG["tot_dim_wt"] = tot_dim_wt;
//						
//							//Jeab 13 Jan 11
//							if(strDimBytot=="Y")
//							{
//								if(decTotWt > decTotDimWt)
//								{
//									tmpDrPKG["chargeable_wt"] = tot_wt;
//								}
//								else
//								{
//									tmpDrPKG["chargeable_wt"] = tot_dim_wt;
//								}
//							}
//							else
//							{
//								if(strApplyDimWt == "Y")
//								{
//									if(tot_wt < tot_dim_wt)
//									{
//										tmpDrPKG["chargeable_wt"] = tot_dim_wt;
//									}
//									else
//									{
//										tmpDrPKG["chargeable_wt"] = tot_wt;
//									}
//								}
//								else
//								{
//									tmpDrPKG["chargeable_wt"] = tot_wt;
//								}
//							}
//							//Jeab 13 Jan 11 =========> End
//
//							//						if(strApplyDimWt == "Y")
//							//						{
//							//							if(tot_wt < tot_dim_wt)
//							//							{
//							//								tmpDrPKG["chargeable_wt"] = tot_dim_wt;
//							//							}
//							//							else
//							//							{
//							//								tmpDrPKG["chargeable_wt"] = tot_wt;
//							//							}
//							//						}
//							//						else
//							//						{
//							//							tmpDrPKG["chargeable_wt"] = tot_wt;
//							//						}
//			
//							FinalDsPackage.Tables[0].Rows.Add(tmpDrPKG);
//							FinalDsPackage.AcceptChanges();
//						}
//
//						decimal shipment_tot_wt = 0;
//						decimal shipment_tot_act_wt = 0;//TU 08/07/08
//						decimal shipment_tot_dim_wt = 0;
//						int shipment_tot_pkg = 0;
//						decimal shipment_chargeable_wt = 0;
//
//
//						foreach(DataRow drLocal in FinalDsPackage.Tables[0].Rows)
//						{
//							shipment_tot_wt += Convert.ToDecimal(drLocal["tot_wt"]);
//							shipment_tot_act_wt+=Convert.ToDecimal(drLocal["tot_act_wt"]);//TU 08/07/08
//							shipment_tot_dim_wt += Convert.ToDecimal(drLocal["tot_dim_wt"]);
//							shipment_tot_pkg += Convert.ToInt32(drLocal["pkg_qty"]);
//							shipment_chargeable_wt += Convert.ToDecimal(drLocal["chargeable_wt"]);
//						}
//
//						DataRow tmpDrCons = FinalDsConsignment.Tables[0].NewRow();
//
//						if((dr["booking_no"] == null) || 
//							(dr["booking_no"].GetType().Equals(System.Type.GetType("System.DBNull"))) || 
//							dr["booking_no"].ToString().Trim() == "")
//							booking_no = 0;
//						else
//							booking_no = Convert.ToInt64(dr["booking_no"]);
//
//						tmpDrCons["booking_no"] = booking_no;
//						tmpDrCons["consignment_no"] = Convert.ToString(dr["consignment_no"]);
//						tmpDrCons["ref_no"] = Convert.ToString(dr["ref_no"]);
//						//					tmpDrCons["booking_datetime"] = System.DateTime.Now;
//						//Jeab 24 Dec 10
//						if((dr["booking_datetime"] == null) || 
//							(dr["booking_datetime"].GetType().Equals(System.Type.GetType("System.DBNull"))) || 
//							dr["booking_datetime"].ToString().Trim() == "")
//							tmpDrCons["booking_datetime"] = System.DateTime.Now;
//						else
//							tmpDrCons["booking_datetime"] = Convert.ToString(dr["booking_datetime"]); 
//						//Jeab 24 Dec 10 =========> End
//						tmpDrCons["payerid"] = Convert.ToString(dr["payerid"]);
//						tmpDrCons["payer_type"] =  (string)customer.payer_type;
//						tmpDrCons["new_account"] = Convert.ToString(dr["new_account"]);
//						tmpDrCons["payer_name"] =Convert.ToString(dr["payer_name"]);
//						tmpDrCons["payer_address1"] =Convert.ToString(dr["payer_address1"]);
//						tmpDrCons["payer_address2"] =Convert.ToString(dr["payer_address2"]);
//						tmpDrCons["payer_zipcode"] =Convert.ToString(dr["payer_zipcode"]);
//					
//						zipCode.Populate(appID,enterpriseID, Convert.ToString(dr["payer_zipcode"]));
//						tmpDrCons["payer_country"] = zipCode.Country;
//						tmpDrCons["payer_telephone"] =Convert.ToString(dr["payer_telephone"]);
//						tmpDrCons["payer_fax"] =Convert.ToString(dr["payer_fax"]);
//						tmpDrCons["payment_mode"] =Convert.ToString(dr["payment_mode"]);
//
//						tmpDrCons["sender_name"] =Convert.ToString(dr["sender_name"]);
//						tmpDrCons["sender_address1"] =Convert.ToString(dr["sender_address1"]);
//						tmpDrCons["sender_address2"] =Convert.ToString(dr["sender_address2"]);
//						tmpDrCons["sender_zipcode"] =Convert.ToString(dr["sender_zipcode"]);
//						zipCode.Populate(appID,enterpriseID, Convert.ToString(dr["sender_zipcode"]));
//						tmpDrCons["sender_country"] = zipCode.Country;
//						tmpDrCons["sender_telephone"] =Convert.ToString(dr["sender_telephone"]);
//						tmpDrCons["sender_fax"] =Convert.ToString(dr["sender_fax"]);
//						tmpDrCons["sender_contact_person"] =Convert.ToString(dr["sender_contact_person"]);
//
//						tmpDrCons["recipient_name"] =Convert.ToString(dr["recipient_name"]);
//						tmpDrCons["recipient_address1"] =Convert.ToString(dr["recipient_address1"]);
//						tmpDrCons["recipient_address2"] =Convert.ToString(dr["recipient_address2"]);
//						tmpDrCons["recipient_zipcode"] =Convert.ToString(dr["recipient_zipcode"]);
//						zipCode.Populate(appID,enterpriseID, Convert.ToString(dr["recipient_zipcode"]));
//						tmpDrCons["recipient_country"] = zipCode.Country;
//						tmpDrCons["recipient_telephone"] =Convert.ToString(dr["recipient_telephone"]);
//						tmpDrCons["recipient_fax"] =Convert.ToString(dr["recipient_fax"]);
//						tmpDrCons["recipient_contact_person"] =Convert.ToString(dr["recipient_contact_person"]);
//				
//						tmpDrCons["tot_pkg"] = shipment_tot_pkg;
//						tmpDrCons["tot_wt"] = TIESUtility.EnterpriseRounding(shipment_tot_wt, wt_rounding_method, wt_increment_amt);
//						tmpDrCons["tot_act_wt"] =shipment_tot_act_wt; //TU 08/07/08
//						tmpDrCons["tot_dim_wt"] = TIESUtility.EnterpriseRounding(shipment_tot_dim_wt, wt_rounding_method, wt_increment_amt);
//						tmpDrCons["chargeable_wt"] = TIESUtility.EnterpriseRounding(shipment_chargeable_wt, wt_rounding_method, wt_increment_amt);
//				
//						tmpDrCons["service_code"] = Convert.ToString(dr["service_code"]);
//						tmpDrCons["declare_value"] = Convert.ToDecimal(dr["declare_value"]);
//
//						decimal percent_dv_additional = 0;
//						decimal max_insurance_cover = 0;
//
//						if((customer.insurance_percent_surcharge == null) || 
//							(customer.insurance_percent_surcharge.GetType().Equals(System.Type.GetType("System.DBNull"))))
//							percent_dv_additional = (decimal)enterprise.InsPercSurchrg;
//						else
//							percent_dv_additional = (decimal)customer.insurance_percent_surcharge;
//						tmpDrCons["percent_dv_additional"] = percent_dv_additional;
//
//						if((customer.insurance_maximum_amt == null) || 
//							(customer.insurance_maximum_amt.GetType().Equals(System.Type.GetType("System.DBNull"))))
//							max_insurance_cover = (decimal)enterprise.MaxInsuranceAmt;
//						else
//							max_insurance_cover = (decimal)customer.insurance_maximum_amt;
//						tmpDrCons["max_insurance_cover"] = max_insurance_cover;
//
//					
//
//						//########## Invoicing Phase ##########
//
//						DomesticShipmentMgrBAL domesticBAL = new DomesticShipmentMgrBAL();
//						decimal decInsSurchrg = domesticBAL.ComputeInsSurchargeByCust(appID, enterpriseID, Convert.ToDecimal(dr["declare_value"]), dr["payerid"].ToString(), customer);
//						tmpDrCons["insurance_surcharge"] = Rounding(decInsSurchrg);
//
//						//########## Invoicing Phase ##########
//					
//						String tmpStrDate = System.DateTime.Now.ToString("dd/MM/yyyy HH:mm");
//						if((dr["booking_no"] != null) && 
//							(!dr["booking_no"].GetType().Equals(System.Type.GetType("System.DBNull"))) && 
//							(dr["booking_no"].ToString() != ""))
//						{
//							DataSet dsBooking = ImportConsignmentsDAL.GetBookingDetailsByBookingNo(appID, enterpriseID, dr["booking_no"].ToString());
//
//							if(dsBooking.Tables[0].Rows.Count > 0)
//							{
//								if((dsBooking.Tables[0].Rows[0]["act_pickup_datetime"] != null) && 
//									(!dsBooking.Tables[0].Rows[0]["act_pickup_datetime"].GetType().Equals(System.Type.GetType("System.DBNull"))) && 
//									(dsBooking.Tables[0].Rows[0]["act_pickup_datetime"].ToString() != ""))
//								{
//									DateTime dtTmp = (DateTime)dsBooking.Tables[0].Rows[0]["act_pickup_datetime"];
//									tmpStrDate = dtTmp.ToString("dd/MM/yyyy HH:mm");
//									tmpDrCons["act_pickup_datetime"] = System.DateTime.ParseExact(tmpStrDate,"dd/MM/yyyy HH:mm",null); 
//								}
//								else
//								{
//									tmpDrCons["act_pickup_datetime"] = System.DateTime.ParseExact(tmpStrDate,"dd/MM/yyyy HH:mm",null); 
//								}
//							}	
//							else
//							{
//								tmpDrCons["act_pickup_datetime"] = System.DateTime.ParseExact(tmpStrDate,"dd/MM/yyyy HH:mm",null);  
//							}	
//						}	
//						else
//						{
//							tmpDrCons["act_pickup_datetime"] = System.DateTime.ParseExact(tmpStrDate,"dd/MM/yyyy HH:mm",null);  
//						}
//
//					
//						tmpDrCons["act_delivery_date"] = System.DBNull.Value;
//						tmpDrCons["payment_type"] = Convert.ToString(dr["payment_type"]);
//						tmpDrCons["shipment_type"] = "D";
//						tmpDrCons["invoice_no"] = System.DBNull.Value;
//
//						//HC Return Task
//						if((dr["return_pod_hc"] == null) || 
//							(dr["return_pod_hc"].GetType().Equals(System.Type.GetType("System.DBNull"))) || 
//							(dr["return_pod_hc"].ToString() == ""))
//						{
//							if(customer.PODSlipRequired == "Y")
//								tmpDrCons["return_pod_slip"] = "Y";
//							else
//								tmpDrCons["return_pod_slip"] = "N";
//						}
//						else
//						{
//							tmpDrCons["return_pod_slip"] = Convert.ToString(dr["return_pod_hc"]);
//						}
//
//						if((dr["return_invoice_hc"] == null) || 
//							(dr["return_invoice_hc"].GetType().Equals(System.Type.GetType("System.DBNull"))) || 
//							(dr["return_invoice_hc"].ToString() == ""))
//						{
//							if(Convert.ToString(customer.hc_invoice_required) == "Y")
//								tmpDrCons["return_invoice_hc"] = "Y";
//							else
//								tmpDrCons["return_invoice_hc"] = "N";
//						}
//						else
//						{
//							tmpDrCons["return_invoice_hc"] = Convert.ToString(dr["return_invoice_hc"]);
//						}
//						//HC Return Task
//
//						zipCode.Populate(appID,enterpriseID, Convert.ToString(dr["sender_zipcode"]));
//						tmpDrCons["origin_state_code"] = zipCode.StateCode;
//
//						DataSet tmpDC = ImportConsignmentsDAL.getDCToOrigStation(appID, enterpriseID,
//							0, 0, Convert.ToString(dr["sender_zipcode"])).ds;
//						tmpDrCons["origin_station"] = tmpDC.Tables[0].Rows[0]["origin_station"].ToString();
//
//						zipCode.Populate(appID,enterpriseID, Convert.ToString(dr["recipient_zipcode"]));
//						tmpDrCons["destination_state_code"] = zipCode.StateCode;
//
//						zipCode.Populate(appID,enterpriseID, tmpDrCons["payer_country"].ToString(), Convert.ToString(dr["recipient_zipcode"]));
//						tmpDrCons["route_code"] = zipCode.DeliveryRoute;
//
//						tmpDC = ImportConsignmentsDAL.getDCToDestStation(appID, enterpriseID,
//							0, 0, zipCode.DeliveryRoute).ds;
//						tmpDrCons["destination_station"] = tmpDC.Tables[0].Rows[0]["origin_station"].ToString();
//
//						tmpDC.Dispose();
//						tmpDC = null;
//
//						tmpDrCons["invoice_amt"] = System.DBNull.Value;
//						tmpDrCons["debit_amt"] = System.DBNull.Value;
//						tmpDrCons["credit_amt"] = System.DBNull.Value;
//						tmpDrCons["cash_collected"] = System.DBNull.Value;
//						tmpDrCons["last_status_code"] = "MDE";
//						tmpDrCons["last_exception_code"] = System.DBNull.Value;
//						tmpDrCons["last_status_datetime"] = System.DateTime.Now; 
//						//tmpDrCons["shpt_manifest_datetime"] = System.DBNull.Value; 
//						tmpDrCons["shpt_manifest_datetime"] = System.DateTime.Now;
//						tmpDrCons["delivery_manifested"] = "N"; 
//						tmpDrCons["quotation_no"] = System.DBNull.Value; 
//						tmpDrCons["quotation_version"] = 0; 
//
//						if((dr["commodity_code"] != null) && 
//							(!dr["commodity_code"].GetType().Equals(System.Type.GetType("System.DBNull"))) && 
//							(dr["commodity_code"].ToString() != ""))
//						{
//							tmpDrCons["commodity_code"] = Convert.ToString(dr["commodity_code"]);
//						}
//						else
//						{
//							tmpDrCons["commodity_code"] = System.DBNull.Value;
//						}
//					
//						tmpDrCons["remark"] = Convert.ToString(dr["remark"]);
//						tmpDrCons["cod_amount"] = Convert.ToDecimal(dr["cod_amount"]);
//
//						zipCode.Populate(appID,enterpriseID, Convert.ToString(dr["sender_zipcode"]));
//						String strOrgZone = zipCode.ZoneCode;
//
//						zipCode.Populate(appID,enterpriseID, Convert.ToString(dr["recipient_zipcode"]));
//						String strDestnZone = zipCode.ZoneCode;
//					
//						customer.Populate(appID,enterpriseID, tmpDrCons["payerid"].ToString());
//
//						float freightCharge = 0;
//						float freightCharge0 = 0;
//						float sumFreight = 0;
//						float fCalcFrgtChrg = 0;
//						if(customer.IsCustomer_Box == "Y")
//						{
//							foreach(DataRow drLocal in FinalDsPackage.Tables[0].Rows)
//							{
//								freightCharge = TIESUtility.ComputeFreightCharge(appID, enterpriseID,
//									tmpDrCons["payerid"].ToString(), customer.payer_type.ToString(), 
//									strOrgZone, strDestnZone, float.Parse(drLocal["chargeable_wt"].ToString())/int.Parse(drLocal["pkg_qty"].ToString()),
//									tmpDrCons["service_code"].ToString(),  
//									Convert.ToString(dr["sender_zipcode"]), Convert.ToString(dr["recipient_zipcode"]));
//								freightCharge = (freightCharge*int.Parse(drLocal["pkg_qty"].ToString()));
//								sumFreight = sumFreight+freightCharge;
//							}
//							if(shipment_tot_pkg < int.Parse(customer.Minimum_Box))
//							{
//								freightCharge0 = TIESUtility.ComputeFreightCharge(appID, enterpriseID,
//									tmpDrCons["payerid"].ToString(), customer.payer_type.ToString(), 
//									strOrgZone, strDestnZone, 0,
//									tmpDrCons["service_code"].ToString(),  
//									Convert.ToString(dr["sender_zipcode"]), Convert.ToString(dr["recipient_zipcode"]));
//								fCalcFrgtChrg = ((int.Parse(customer.Minimum_Box)-shipment_tot_pkg)*freightCharge0)+sumFreight;
//							}
//							else
//								fCalcFrgtChrg = sumFreight;
//						}
//						else
//						{
//							fCalcFrgtChrg = TIESUtility.ComputeFreightCharge(appID, enterpriseID,
//								tmpDrCons["payerid"].ToString(), customer.payer_type.ToString(), 
//								strOrgZone, strDestnZone, (float)shipment_chargeable_wt,
//								tmpDrCons["service_code"].ToString(),  
//								Convert.ToString(dr["sender_zipcode"]), Convert.ToString(dr["recipient_zipcode"]));
//						}
//					
//						ViewState["FreightCharge"] = fCalcFrgtChrg;
//						tmpDrCons["tot_freight_charge"] = Rounding(Convert.ToDecimal(fCalcFrgtChrg));
//						//tmpDrCons["tot_freight_charge"] = Convert.ToDecimal(fCalcFrgtChrg);
//
//					
//						//########## Other Surcharge ##########
//						decimal OtherSurcharge = 0;
//						if((customer.OtherSurchargeAmount == null || customer.OtherSurchargeAmount.ToString() == "") && (customer.OtherSurchargeAmountPercentage == null || customer.OtherSurchargeAmountPercentage.ToString() == ""))
//						{
//							OtherSurcharge = 0;
//						}
//						else
//						{
//							if(customer.OtherSurchargeAmount != "")
//							{ 
//								OtherSurcharge = Convert.ToDecimal(customer.OtherSurchargeAmount);
//							}
//							else if(customer.OtherSurchargeAmountPercentage != null && customer.OtherSurchargeAmountPercentage != "")
//							{
//								OtherSurcharge = Convert.ToDecimal(customer.OtherSurchargeAmountPercentage) * Convert.ToDecimal(fCalcFrgtChrg);
//
//								if(customer.OtherSurchargeMin != null && customer.OtherSurchargeMin != "")
//								{
//									OtherSurcharge = Math.Max(OtherSurcharge, Convert.ToDecimal(customer.OtherSurchargeMin));							
//								}
//								if(customer.OtherSurchargeMax != null && customer.OtherSurchargeMax != "")
//								{
//									OtherSurcharge = Math.Min(OtherSurcharge, Convert.ToDecimal(customer.OtherSurchargeMax));
//								}
//							}
//						}tmpDrCons["other_surch_amount"] = Rounding(OtherSurcharge);
//										
//						//########## End Other Surcharge ##########
//
//						//					if(strApplyESA == "N")
//						//					{
//						//						tmpDrCons["esa_surcharge"] = 0;
//						//					}
//						//					else
//						//					{
//						//					tmpDrCons["esa_surcharge"] = this.calCulateESA(Convert.ToString(dr["recipient_zipcode"]),
//						//					Convert.ToString(dr["sender_zipcode"]), Convert.ToString(dr["payerid"]),Convert.ToDecimal(fCalcFrgtChrg));
//						tmpDrCons["esa_surcharge"] = this.calCulateESA_ServiceType(Convert.ToString(dr["recipient_zipcode"]),
//							Convert.ToString(dr["sender_zipcode"]), Convert.ToString(dr["payerid"]),Convert.ToDecimal(fCalcFrgtChrg),Convert.ToString(dr["service_code"]));
//
//						//					}
//
//						if((customer.mbg != null) && 
//							(!customer.mbg.GetType().Equals(System.Type.GetType("System.DBNull"))) && 
//							(customer.mbg.ToString() != ""))
//						{
//							if(customer.mbg.ToString().Trim() == "Y")
//								tmpDrCons["mbg"]= "Y";
//							else
//								tmpDrCons["mbg"]= "N";
//						}
//						else
//						{
//							tmpDrCons["mbg"]= "N";
//						}
//
//
//						DomesticShipmentMgrBAL domesticMgrBAL = new DomesticShipmentMgrBAL();
//						String strDlvryDt =  CalcDlvryDtTimeImpCon(appID, enterpriseID,
//							System.DateTime.ParseExact(tmpStrDate,"dd/MM/yyyy HH:mm", null),
//							tmpDrCons["service_code"].ToString(), Convert.ToString(dr["recipient_zipcode"]), zipCode.StateCode).ToString();
// 
//
//						tmpDrCons["est_delivery_datetime"] = System.DateTime.ParseExact(strDlvryDt,"dd/MM/yyyy HH:mm", null);
//
//
//						//HC Return Task
//						if (tmpDrCons["return_pod_slip"].ToString().Trim() == "Y" ||
//							tmpDrCons["return_invoice_hc"].ToString().Trim() == "Y")
//						{
//							tmpDrCons["est_hc_return_datetime"] = DomesticShipmentMgrDAL.calEstHCDateTime(System.DateTime.ParseExact(strDlvryDt,"dd/MM/yyyy HH:mm", null), zipCode.StateCode,
//								"","", appID, enterpriseID);
//						}
//						else
//						{
//							tmpDrCons["est_hc_return_datetime"] =  System.DBNull.Value;
//						}
//						tmpDrCons["act_hc_return_datetime"] = System.DBNull.Value;
//						//HC Return Task
//
//						if(Convert.ToDecimal(dr["cod_amount"]) > 0)
//						{
//							DataSet dsVas = ImportConsignmentsDAL.GetSurchargeFromVAS(appID, enterpriseID);
//							if(dsVas.Tables[0].Rows.Count > 0)
//							{
//								foreach(DataRow drVAS in dsVas.Tables[0].Rows)
//								{
//									DataRow tmpDrVAS = TmpDsVAS.Tables[0].NewRow();
//
//									if((drVAS["vas_code"] != null) && 
//										(!drVAS["vas_code"].GetType().Equals(System.Type.GetType("System.DBNull"))) && 
//										(drVAS["vas_code"].ToString() != ""))
//									{
//										tmpDrVAS["vas_code"] = Convert.ToString(drVAS["vas_code"]);
//									}
//
//									if((drVAS["vas_description"] != null) && 
//										(!drVAS["vas_description"].GetType().Equals(System.Type.GetType("System.DBNull"))) && 
//										(drVAS["vas_description"].ToString() != ""))
//									{
//										tmpDrVAS["vas_description"] = Convert.ToString(drVAS["vas_description"]);
//									}
//
//									if((drVAS["surcharge"] != null) && 
//										(!drVAS["surcharge"].GetType().Equals(System.Type.GetType("System.DBNull"))) && 
//										(drVAS["surcharge"].ToString() != ""))
//									{
//										tmpDrVAS["surcharge"] = Convert.ToDecimal(drVAS["surcharge"]);
//									}
//
//									tmpDrVAS["remarks"] = System.DBNull.Value;
//
//									TmpDsVAS.Tables[0].Rows.Add(tmpDrVAS);
//									TmpDsVAS.AcceptChanges();
//								}
//							}
//						}
//
//						//					DataSet dsEnt = ImportConsignmentsDAL.GetSurchargeFromEnterpriseSurcharge(appID, enterpriseID);
//						//					if(dsEnt.Tables[0].Rows.Count > 0)
//						//					{
//						//						foreach(DataRow drEnt in dsEnt.Tables[0].Rows)
//						//						{
//						//							DataRow tmpDrEnt = FinalDsVAS.Tables[0].NewRow();
//						//
//						//							if((drEnt["vas_code"] != null) && 
//						//								(!drEnt["vas_code"].GetType().Equals(System.Type.GetType("System.DBNull"))) && 
//						//								(drEnt["vas_code"].ToString() != ""))
//						//							{
//						//								tmpDrEnt["vas_code"] = Convert.ToString(drEnt["vas_code"]);
//						//							}
//						//
//						//							if((drEnt["vas_description"] != null) && 
//						//								(!drEnt["vas_description"].GetType().Equals(System.Type.GetType("System.DBNull"))) && 
//						//								(drEnt["vas_description"].ToString() != ""))
//						//							{
//						//								tmpDrEnt["vas_description"] = Convert.ToString(drEnt["vas_description"]);
//						//							}
//						//
//						//							if((drEnt["surcharge"] != null) && 
//						//								(!drEnt["surcharge"].GetType().Equals(System.Type.GetType("System.DBNull"))) && 
//						//								(drEnt["surcharge"].ToString() != ""))
//						//							{
//						//								tmpDrEnt["surcharge"] = Convert.ToDecimal(drEnt["surcharge"]);
//						//							}
//						//
//						//							tmpDrEnt["remarks"] = System.DBNull.Value;
//						//
//						//							FinalDsVAS.Tables[0].Rows.Add(tmpDrEnt);
//						//							FinalDsVAS.AcceptChanges();
//						//						}
//						//					}
//
//						decimal tot_vas_surcharge = 0;
//						decimal Max_surcharge ,Min_surcharge,tmp_codsurcharge = 0;
//					
//						foreach(DataRow drLocal in TmpDsVAS.Tables[0].Rows)
//						{
//							String vas_code = "";
//
//							if((drLocal["vas_code"] != null) && 
//								(!drLocal["vas_code"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
//								(drLocal["vas_code"].ToString() != ""))
//							{
//								vas_code = drLocal["vas_code"].ToString().Trim();
//							}
//
//							//VAS = COD
//							if (vas_code == "COD")
//							{
//								DataSet dscustInfo = SysDataMgrDAL.GetCustInforByCustID(appID,enterpriseID, 
//									0, 0, tmpDrCons["payerid"].ToString().Trim()).ds;							
//						
//								if(dscustInfo.Tables[0].Rows.Count > 0)
//								{
//									//Retreive min_surcharge 
//								
//									if((dscustInfo.Tables[0].Rows[0]["min_cod_surcharge"] != null) && 
//										(!dscustInfo.Tables[0].Rows[0]["min_cod_surcharge"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
//										(dscustInfo.Tables[0].Rows[0]["min_cod_surcharge"].ToString() != "")) 
//
//									{
//										Min_surcharge =  (decimal) dscustInfo.Tables[0].Rows[0]["min_cod_surcharge"];
//									}
//									else
//									{
//										Min_surcharge =  0;
//									}
//
//									//Retreive max_surcharge 
//
//									if((dscustInfo.Tables[0].Rows[0]["max_cod_surcharge"] != null) && 
//										(!dscustInfo.Tables[0].Rows[0]["max_cod_surcharge"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
//										(dscustInfo.Tables[0].Rows[0]["max_cod_surcharge"].ToString() != ""))
//									{
//									
//										Max_surcharge =  (decimal) dscustInfo.Tables[0].Rows[0]["max_cod_surcharge"];
//									
//									}
//									else 
//									{
//										Max_surcharge =  0;									
//									}
//
//
//									//Have a Fix OR Percentage Of COD_Surcharge 
//									if(((dscustInfo.Tables[0].Rows[0]["cod_surcharge_amt"] != null) && 
//										(!dscustInfo.Tables[0].Rows[0]["cod_surcharge_amt"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
//										(dscustInfo.Tables[0].Rows[0]["cod_surcharge_amt"].ToString() != "")) ||
//										((dscustInfo.Tables[0].Rows[0]["cod_surcharge_percent"] != null) && 
//										(!dscustInfo.Tables[0].Rows[0]["cod_surcharge_percent"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
//										(dscustInfo.Tables[0].Rows[0]["cod_surcharge_percent"].ToString() != "")))
//									{
//										if((dscustInfo.Tables[0].Rows[0]["cod_surcharge_amt"] != null) && 
//											(!dscustInfo.Tables[0].Rows[0]["cod_surcharge_amt"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
//											(dscustInfo.Tables[0].Rows[0]["cod_surcharge_amt"].ToString() != ""))
//										{
//											// Fix COD_Surcharge
//											tmp_codsurcharge = (decimal)dscustInfo.Tables[0].Rows[0]["cod_surcharge_amt"];
//											//if (Max_surcharge != 0 && Min_surcharge != 0)								
//											//{							
//
//											//Check Boundary Of Customer Between min_cod and max_cod
//											//											if (tmp_codsurcharge > Max_surcharge)
//											//											{
//											//												tmp_codsurcharge = Max_surcharge;
//											//											}
//											//											else if(tmp_codsurcharge < Min_surcharge)
//											//											{
//											//												tmp_codsurcharge = Min_surcharge;
//											//											}
//
//											tot_vas_surcharge += tmp_codsurcharge;	
//											//}
//											//										else
//											//										{
//											//											tot_vas_surcharge += (decimal)drLocal["surcharge"];
//											//										}
//
//										}
//
//										if((dscustInfo.Tables[0].Rows[0]["cod_surcharge_percent"] != null) && 
//											(!dscustInfo.Tables[0].Rows[0]["cod_surcharge_percent"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
//											(dscustInfo.Tables[0].Rows[0]["cod_surcharge_percent"].ToString() != ""))
//										{
//											//Percentage Of COD_Surcharge
//											tmp_codsurcharge = (Convert.ToDecimal(dr["cod_amount"]) * ((decimal)dscustInfo.Tables[0].Rows[0]["cod_surcharge_percent"])) / 100;
//
//											if (Max_surcharge != 0 && Min_surcharge != 0)								
//											{							
//
//												//Check Boundary Of Customer Between min_cod and max_cod
//												if (tmp_codsurcharge > Max_surcharge)
//												{
//													tmp_codsurcharge = Max_surcharge;
//												}
//												else if(tmp_codsurcharge < Min_surcharge)
//												{
//													tmp_codsurcharge = Min_surcharge;
//												}
//
//												tot_vas_surcharge += TIESUtility.EnterpriseRounding(tmp_codsurcharge,wt_rounding_method,wt_increment_amt);	
//											}
//											else
//											{
//												tot_vas_surcharge += (decimal)drLocal["surcharge"];
//											}
//										}
//									}
//
//										//Have Nothing,Retrieve From VAS_Surcharge
//									else
//									{
//										//tmp_codsurcharge =  TIESUtility.EnterpriseRounding((decimal)drLocal["surcharge"],wt_rounding_method,wt_increment_amt);	
//										tmp_codsurcharge = (decimal)drLocal["surcharge"];
//										//									if (Max_surcharge != 0 && Min_surcharge != 0)								
//										//									{						
//										//										//Check Boundary Of Customer Between min_cod and max_cod
//										//										if (tmp_codsurcharge > Max_surcharge)
//										//										{
//										//											tmp_codsurcharge = Max_surcharge;
//										//										}
//										//										else if(tmp_codsurcharge < Min_surcharge)
//										//										{
//										//											tmp_codsurcharge = Min_surcharge;
//										//										}
//										//									}
//
//										tot_vas_surcharge += tmp_codsurcharge;	
//									}
//
//								}
//								
//							}
//
//							else
//							{
//								//tot_vas_surcharge += TIESUtility.EnterpriseRounding((decimal)drLocal["surcharge"],wt_rounding_method,wt_increment_amt);	
//								tot_vas_surcharge += (decimal)drLocal["surcharge"];
//							}
//						}
//
//
//						//New FinalDsVAS 
//						DataSet newdsVas = ImportConsignmentsDAL.GetSurchargeFromVAS(appID, enterpriseID);
//						if(newdsVas.Tables[0].Rows.Count > 0)
//						{
//
//							foreach(DataRow newdrVas in newdsVas.Tables[0].Rows)
//							{
//								DataRow tmpnewdrVas = FinalDsVAS.Tables[0].NewRow();
//
//								if((newdrVas["vas_code"] != null) && 
//									(!newdrVas["vas_code"].GetType().Equals(System.Type.GetType("System.DBNull"))) && 
//									(newdrVas["vas_code"].ToString() != ""))
//								{
//									tmpnewdrVas["vas_code"] = Convert.ToString(newdrVas["vas_code"]);
//								}
//
//								if((newdrVas["vas_description"] != null) && 
//									(!newdrVas["vas_description"].GetType().Equals(System.Type.GetType("System.DBNull"))) && 
//									(newdrVas["vas_description"].ToString() != ""))
//								{
//									tmpnewdrVas["vas_description"] = Convert.ToString(newdrVas["vas_description"]);
//								}
//
//					
//								tmpnewdrVas["surcharge"] = Convert.ToDecimal(tot_vas_surcharge);
//								tmpnewdrVas["remarks"] = System.DBNull.Value;
//
//								FinalDsVAS.Tables[0].Rows.Add(tmpnewdrVas);
//								FinalDsVAS.AcceptChanges();
//							}
//						}
//						tmpDrCons["tot_vas_surcharge"] = Convert.ToDecimal(tot_vas_surcharge);
//
//
//						/* ##############################################################################
//						 * Add New by   : Oak 
//						 * Date			: 14:54 2/4/2551
//						 * Description  : Ref. from 3.4 Domesticshipment (INVB), part of VAS Inserting 
//						 */					
//						decimal tmp_vas_surcharge  = 0;
//						decimal sumVas = 0;
//						string str_vas_code = "";
//				
//						// Enterprise Flag (Active / Inactive)
//						DataSet dsEprise = SysDataManager1.GetEnterpriseProfile(appID,enterpriseID);
//						// Customer VAS records
//						DataSet dsVasCust = SysDataMgrDAL.QueryVAS(appID, enterpriseID, dr["payerid"].ToString());
//						// Enterprise VAS records
//						DataSet dsEntVAS = ImportConsignmentsDAL.GetSurchargeAllEntVAS(appID, enterpriseID);
//					
//						//For Pickup DateTime
//						if(dsEprise.Tables[0].Rows.Count > 0)
//						{
//							// Saturday
//							if(Convert.ToDateTime(tmpDrCons["act_pickup_datetime"]).DayOfWeek == System.DayOfWeek.Saturday)
//								str_vas_code = dsEprise.Tables[0].Rows[0]["sat_pickup_vas_code"].ToString();
//
//							// Sunday
//							if(Convert.ToDateTime(tmpDrCons["act_pickup_datetime"]).DayOfWeek == System.DayOfWeek.Sunday)
//								str_vas_code = dsEprise.Tables[0].Rows[0]["sun_pickup_vas_code"].ToString();
//
//
//							// Monday - Friday
//							if((Convert.ToDateTime(tmpDrCons["act_pickup_datetime"]).DayOfWeek != System.DayOfWeek.Saturday) && 
//								(Convert.ToDateTime(tmpDrCons["act_pickup_datetime"]).DayOfWeek != System.DayOfWeek.Sunday))
//							{
//								Zipcode zipCodeCheck = new Zipcode();
//								zipCodeCheck.Populate(appID,enterpriseID, dr["payer_zipcode"].ToString());
//								bool isCheckPubDay = DomesticShipmentMgrDAL.IsEnterpriseHolidayOfAutoManifest(appID, enterpriseID, Convert.ToDateTime(tmpDrCons["act_pickup_datetime"]), zipCode.StateCode);
//
//								if(isCheckPubDay)
//									str_vas_code = dsEprise.Tables[0].Rows[0]["pub_pickup_vas_code"].ToString();
//							}
//							
//						
//							if(str_vas_code.ToString().Trim() != "")
//							{	
//								if(dsVasCust.Tables[0].Select("vas_code='"+str_vas_code+"'").Length > 0)
//								{	// Calculated form Customer VAS Surcharge
//
//									DataRow tmpnewdrVas = FinalDsVAS.Tables[0].NewRow();
//									DataRow[] drCustVas = dsVasCust.Tables[0].Select("vas_code='"+str_vas_code+"'");
//
//									if((drCustVas[0]["vas_code"] != null) && 
//										(!drCustVas[0]["vas_code"].GetType().Equals(System.Type.GetType("System.DBNull"))) && 
//										(drCustVas[0]["vas_code"].ToString() != ""))
//									{
//										tmpnewdrVas["vas_code"] = Convert.ToString(drCustVas[0]["vas_code"]);
//									}
//
//									if((drCustVas[0]["description"] != null) && 
//										(!drCustVas[0]["description"].GetType().Equals(System.Type.GetType("System.DBNull"))) && 
//										(drCustVas[0]["description"].ToString() != ""))
//									{
//										tmpnewdrVas["vas_description"] = Convert.ToString(drCustVas[0]["description"]);
//									}
//
//									if((drCustVas[0]["surcharge"] != null) && 
//										(!drCustVas[0]["surcharge"].GetType().Equals(System.Type.GetType("System.DBNull"))) && 
//										(drCustVas[0]["surcharge"].ToString() != ""))
//									{
//										tmp_vas_surcharge  =  (decimal)drCustVas[0]["surcharge"];
//									}
//									else
//									{
//										tmp_vas_surcharge = (decimal)(Convert.ToDecimal(drCustVas[0]["percent"]) *  Convert.ToDecimal(ViewState["FreightCharge"].ToString()) / 100);
//									
//										if((drCustVas[0]["min_surcharge"] != null) && 
//											(!drCustVas[0]["min_surcharge"].GetType().Equals(System.Type.GetType("System.DBNull"))) && 
//											(drCustVas[0]["min_surcharge"].ToString() != ""))
//										{
//											if ((decimal)drCustVas[0]["min_surcharge"] > tmp_vas_surcharge )
//											{
//												tmp_vas_surcharge = (decimal)drCustVas[0]["min_surcharge"];
//											}
//										}
//
//										if((drCustVas[0]["max_surcharge"] != null) && 
//											(!drCustVas[0]["max_surcharge"].GetType().Equals(System.Type.GetType("System.DBNull"))) && 
//											(drCustVas[0]["max_surcharge"].ToString() != ""))
//										{
//											if ((decimal)drCustVas[0]["max_surcharge"] < tmp_vas_surcharge )
//											{
//												tmp_vas_surcharge = (decimal)drCustVas[0]["max_surcharge"];
//											}
//										}
//									}
//
//									tmpnewdrVas["surcharge"] = tmp_vas_surcharge;
//									tmpnewdrVas["remarks"] = System.DBNull.Value;
//
//									FinalDsVAS.Tables[0].Rows.Add(tmpnewdrVas);
//									FinalDsVAS.AcceptChanges();
//
//									sumVas += tmp_vas_surcharge;
//								}
//								else if((dsVasCust.Tables[0].Select("vas_code='"+str_vas_code+"'").Length == 0) &&
//									(dsEntVAS.Tables[0].Select("vas_code='"+str_vas_code+"'").Length > 0))
//								{	// Calculated form Enterprise VAS Surcharge
//									DataRow tmpnewdrVas = FinalDsVAS.Tables[0].NewRow();
//									DataRow[] drEntVas = dsEntVAS.Tables[0].Select("vas_code='"+str_vas_code+"'");
//
//									if((drEntVas[0]["vas_code"] != null) && 
//										(!drEntVas[0]["vas_code"].GetType().Equals(System.Type.GetType("System.DBNull"))) && 
//										(drEntVas[0]["vas_code"].ToString() != ""))
//									{
//										tmpnewdrVas["vas_code"] = Convert.ToString(drEntVas[0]["vas_code"]);
//									}
//
//									if((drEntVas[0]["vas_description"] != null) && 
//										(!drEntVas[0]["vas_description"].GetType().Equals(System.Type.GetType("System.DBNull"))) && 
//										(drEntVas[0]["vas_description"].ToString() != ""))
//									{
//										tmpnewdrVas["vas_description"] = Convert.ToString(drEntVas[0]["vas_description"]);
//									}
//
//									if((drEntVas[0]["surcharge"] != null) && 
//										(!drEntVas[0]["surcharge"].GetType().Equals(System.Type.GetType("System.DBNull"))) && 
//										(drEntVas[0]["surcharge"].ToString() != ""))
//									{
//										tmp_vas_surcharge  =  (decimal)drEntVas[0]["surcharge"];
//									}
//
//									tmpnewdrVas["surcharge"] = tmp_vas_surcharge;
//									tmpnewdrVas["remarks"] = System.DBNull.Value;
//
//									FinalDsVAS.Tables[0].Rows.Add(tmpnewdrVas);
//									FinalDsVAS.AcceptChanges();
//
//									sumVas += tmp_vas_surcharge;
//								}
//							}
//
//							tmpDrCons["tot_vas_surcharge"] = (decimal)tmpDrCons["tot_vas_surcharge"]+ sumVas;
//						}
//						//					return;
//						//###################### End Add New by : Oak ####################################
//
//						FinalDsConsignment.Tables[0].Rows.Add(tmpDrCons);
//						FinalDsConsignment.AcceptChanges();
//
//						int iSerialNo = 0;
//						if(Session["SerialNo"] != null)
//						{
//							String strSerialNo = (String)Session["SerialNo"];
//							iSerialNo = Convert.ToInt32(strSerialNo);
//						}
//
//						preShipRec = ImportConsignmentsDAL.GetPreshipRec(appID, enterpriseID, Convert.ToString(dr["consignment_no"]));
//						if(FinalDsVAS != null)
//						{
//							if (preShipRec.Tables[0].Rows.Count > 0)
//							{
//								long preBookingNo  = 0;
//
//								if((preShipRec.Tables[0].Rows[0]["booking_no"] != null) && 
//									(!preShipRec.Tables[0].Rows[0]["booking_no"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
//									(preShipRec.Tables[0].Rows[0]["booking_no"].ToString() != ""))
//									preBookingNo = Convert.ToInt64(preShipRec.Tables[0].Rows[0]["booking_no"].ToString().Trim());
//								else
//									preBookingNo = 0;
//							
//								DomesticShipmentMgrDAL.UpdateDomesticShp_Partial(appID, enterpriseID, FinalDsConsignment, FinalDsVAS, 
//									FinalDsPackage, userID, preBookingNo);
//
//								preShipRec = ImportConsignmentsDAL.GetPreshipRec(appID, enterpriseID, Convert.ToString(dr["consignment_no"]));
//								if((preShipRec.Tables[0].Rows[0]["booking_no"] != null) && 
//									(!preShipRec.Tables[0].Rows[0]["booking_no"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
//									(preShipRec.Tables[0].Rows[0]["booking_no"].ToString() != ""))
//									preBookingNo = Convert.ToInt64(preShipRec.Tables[0].Rows[0]["booking_no"].ToString().Trim());
//
//								booking_no = preBookingNo;
//							}
//							else
//							{
//								booking_no = ImportConsignmentsDAL.AddDomesticShipment(appID,enterpriseID,
//									FinalDsConsignment, FinalDsVAS, FinalDsPackage, userID, iSerialNo,
//									Convert.ToString(dr["recipient_zipcode"]));
//							}
//						}
//						else
//						{
//							booking_no = 0;
//						}
//					
//						
//						String path_code = "";
//						String dest_station = "";
//						String orig_station = "";
//
//						//############### get origin information of manifest ###############
//						DataSet tmpAM_DC = ImportConsignmentsDAL.getDCToOrigStation(appID, enterpriseID,
//							0, 0, Convert.ToString(dr["sender_zipcode"])).ds;
//						orig_station  = tmpAM_DC.Tables[0].Rows[0]["origin_station"].ToString();
//
//
//						//############### get destination information of manifest ###############
//						tmpAM_DC = ImportConsignmentsDAL.getDCToDestStation(appID, enterpriseID,
//							0, 0, tmpDrCons["route_code"].ToString()).ds;
//						path_code = tmpAM_DC.Tables[0].Rows[0]["path_code"].ToString();
//						dest_station = tmpAM_DC.Tables[0].Rows[0]["origin_station"].ToString();
//
//						tmpAM_DC.Dispose();
//						tmpAM_DC = null;
//						// Comment by Tu 16/02/2011 Remove DeliveryManifest
//						if (DeliveryManifestMgrDAL.IsAutomanifest(appID, enterpriseID, Convert.ToString(dr["service_code"])) == "Y")
//						{
//							DomesticShipmentMgrDAL.setAutomaticManifest(appID, enterpriseID,
//								FinalDsConsignment, path_code, Convert.ToInt32(booking_no),
//								orig_station, dest_station, tmpDrCons["route_code"].ToString(), Convert.ToString(dr["service_code"]),0);
//						}
//						else
//						{
//							//9. If the above query does not return a value then:
//							//Call exit_w-o_manifesting (logic for this exit program is shown below)
//							DomesticShipmentMgrDAL.UpdateShipmentOfAutoManifest(appID, enterpriseID,
//								Convert.ToInt32(booking_no), Convert.ToString(dr["consignment_no"]));
//						}
//						//
//						importedCons +=  FinalDsConsignment.Tables[0].Rows.Count;
//					}
//					catch(Exception ex)
//					{
//						String tmpErrMessage = "";
//						tmpErrMessage = ex.Message;
//					}
//				}
//			
//				// Added on 18-Oct-11
//				// Insert shipment_pkg_box
//				//ImportConsignmentsDAL.ImportConsignment_SavingPkgDetail(appID,enterpriseID,userID);
//				//*************************
//
//				RetreiveConsignmentData();
//
//				if (pickupErrorFlag)
//					lblErrorMsg.Text = PUPErrorMsg;
//				else
//				{
//					if(!isEmpty)
//					{
//						lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"MSG_SAVESUCCESS",utility.GetUserCulture());	
//						lblTotalSavedCon.Text = "Total Imported: " + importedCons.ToString() + " Consignments.";
//					}
//					else
//					{
//						lblErrorMsg.Text = "";//countRec.ToString();
//						lblTotalSavedCon.Text = "";
//					}
//				}
//
//				for(int i=0;i< arCon_No.Count;i++)
//				{
//					string con_no = Convert.ToString(arCon_No[i]);
//
//					if(con_no != "")
//						ReCalculateWhenSipBefore(appID, enterpriseID, con_no);
//				}
//			}
//		}

		private void btnSave_Click(object sender, System.EventArgs e)
		{//int countRec = 0;

			try
			{
				int recUpt = 0;

//				for(int i=0;i<=dgConsignment.Items.Count-1;i++)
//				{
//					DataGridItem dgRow = dgConsignment.Items[i];
//					ImageButton imgSelect = (ImageButton)dgRow.Cells[1].FindControl("imgSelect");
//
//					if(imgSelect != null)
//						if(imgSelect.ImageUrl == "images/butt-select.gif")
//						{
//							recUpt = recUpt + 1;
//						}
//				}
				int result = 0;
				DataSet dsResultSave = ImportConsignmentsDAL.ImportConsignments_Save(appID, enterpriseID, userID, 0,"");

				if(dsResultSave.Tables.Count == 1)
					if(dsResultSave.Tables[0].Rows.Count > 0)
					{
						result = (int)dsResultSave.Tables[0].Rows[0][0];
						recUpt = (int)dsResultSave.Tables[0].Rows[0][1];
					}


				if(result != 0)
				{
					if(result != 0)
					{
						if(result == -1)
							lblErrorMsg.Text = "@userid parameter may not be NULL or blank.";
						else if(result == -2)
							lblErrorMsg.Text = "@userid is not a valid user ID for enterprise: PNGAF.";
						else if(result == -3)
							lblErrorMsg.Text = "@enterpriseid parameter is missing or invalid.";
						else if(result > 0)
							lblErrorMsg.Text = "SQL error message.";
					}
				}
				else
				{

					RetreiveConsignmentData();


					if(recUpt > 0)
					{
						lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"MSG_SAVESUCCESS",utility.GetUserCulture());	
					}

				
					lblTotalSavedCon.Text = "Total Imported: " + recUpt.ToString() + " Consignments.";

				}
			}
			catch(Exception ex)
			{
				lblErrorMsg.Text = ex.Message;
			}
		}

	#region save event original version
//		private void btnSave_Click(object sender, System.EventArgs e)
//		{
//			int importedCons = 0;
//			lblTotalSavedCon.Text = "";
//			ArrayList arCon_No =  new ArrayList(); 
//			
//			dsConsignment = ImportConsignmentsDAL.GetAuditedConsignment(appID, enterpriseID,
//				(String)ViewState["userCulture"], userID);
//
//			for(int i=0;i< dsConsignment.Tables[0].Rows.Count;i++)
//			{
//				arCon_No.Add(dsConsignment.Tables[0].Rows[0]["consignment_no"]);
//			}
//
//			dsPackage = ImportConsignmentsDAL.GetAuditedPackage(appID, enterpriseID,
//				(String)ViewState["userCulture"], userID);
//
//
//			////// by Tumz 31.03.54 ///////
//			if(dsPackage != null && dsPackage.Tables[0].Rows.Count > 0)
//			{
//				string[] conDelImportShipment_PKG = new string[dsPackage.Tables[0].Rows.Count];
//
//				for(int i=0;i<dsPackage.Tables[0].Rows.Count-1;i++)
//				{
//					DataSet dsShipment_PKG = ImportConsignmentsDAL.GetShipment_PKG(appID, enterpriseID, Convert.ToString(dsPackage.Tables[0].Rows[i]["consignment_no"]));
//					if(dsShipment_PKG != null && dsShipment_PKG.Tables[0].Rows.Count > 0)
//					{
//						for(int j=0;j<dsShipment_PKG.Tables[0].Rows.Count;j++)
//						{
//							if(((decimal)dsPackage.Tables[0].Rows[i]["mps_code"] !=  (decimal)dsShipment_PKG.Tables[0].Rows[i]["mps_code"]) || 
//								((decimal)dsPackage.Tables[0].Rows[i]["pkg_wt"] !=  (decimal)dsShipment_PKG.Tables[0].Rows[i]["pkg_wt"]) ||
//								((int)dsPackage.Tables[0].Rows[i]["pkg_qty"] !=  (int)dsShipment_PKG.Tables[0].Rows[i]["pkg_qty"])||
//								!((((decimal)dsPackage.Tables[0].Rows[i]["pkg_length"] == (decimal)dsShipment_PKG.Tables[0].Rows[i]["pkg_length"] && (decimal)dsPackage.Tables[0].Rows[i]["pkg_length"] ==  (decimal)dsShipment_PKG.Tables[0].Rows[i]["pkg_breadth"]) && (decimal)dsPackage.Tables[0].Rows[i]["pkg_length"] ==  (decimal)dsShipment_PKG.Tables[0].Rows[i]["pkg_height"] )&&
//								(((decimal)dsPackage.Tables[0].Rows[i]["pkg_breadth"] == (decimal)dsShipment_PKG.Tables[0].Rows[i]["pkg_length"] && (decimal)dsPackage.Tables[0].Rows[i]["pkg_breadth"] ==  (decimal)dsShipment_PKG.Tables[0].Rows[i]["pkg_breadth"]) && (decimal)dsPackage.Tables[0].Rows[i]["pkg_breadth"] ==  (decimal)dsShipment_PKG.Tables[0].Rows[i]["pkg_height"] )&&
//								(((decimal)dsPackage.Tables[0].Rows[i]["pkg_height"] == (decimal)dsShipment_PKG.Tables[0].Rows[i]["pkg_length"] && (decimal)dsPackage.Tables[0].Rows[i]["pkg_height"] ==  (decimal)dsShipment_PKG.Tables[0].Rows[i]["pkg_breadth"]) && (decimal)dsPackage.Tables[0].Rows[i]["pkg_height"] ==  (decimal)dsShipment_PKG.Tables[0].Rows[i]["pkg_height"] ))
//								)
//								conDelImportShipment_PKG[i] = dsPackage.Tables[0].Rows[i]["consignment_no"].ToString();
//						}
//					}
//				}
//
//				for(int j=conDelImportShipment_PKG.Length-1;j>=0;j--)
//				{
//					if(conDelImportShipment_PKG[j] != null)
//					{
//						for(int i=dsPackage.Tables[0].Rows.Count-1;i>=0;i--)
//						{
//							if(conDelImportShipment_PKG[j] == dsPackage.Tables[0].Rows[i]["consignment_no"].ToString())
//								dsPackage.Tables[0].Rows[i].Delete();
//						}	
//						dsPackage.AcceptChanges();
//					}
//				}
//
//			}
//			
//
//
//
//			Enterprise enterprise = new Enterprise();
//			enterprise.Populate(utility.GetAppID(),utility.GetEnterpriseID());
//
//			Zipcode zipCode = new Zipcode();
//			Customer customer = new Customer();
//
//			decimal fDensityFactor = (decimal)enterprise.DensityFactor;
//			int wt_rounding_method = 0;
//			decimal wt_increment_amt = 1;
//
//			wt_rounding_method = (int)enterprise.RoundingMethod;
//			wt_increment_amt = (decimal)enterprise.IncrementWt;
//			
//			//Filter Con by PicupDate
//			bool pickupErrorFlag = false;
//			string dayOfPickUp = "";
//			string PUPErrorMsg = "";
//			int cntDS = 0; 
//			bool isEmpty = false;
//			DataSet preShipRec = null;
//			if(!(cntDS <= dsConsignment.Tables[0].Rows.Count - 1))
//			{
//				isEmpty = true;
//			}
//			while(cntDS <= dsConsignment.Tables[0].Rows.Count - 1)
//			{
//				String tmpStrDate = System.DateTime.Now.ToString("dd/MM/yyyy HH:mm");
//				
//				if((dsConsignment.Tables[0].Rows[cntDS]["booking_no"] != null) && 
//					(!dsConsignment.Tables[0].Rows[cntDS]["booking_no"].GetType().Equals(System.Type.GetType("System.DBNull"))) && 
//					(dsConsignment.Tables[0].Rows[cntDS]["booking_no"].ToString() != ""))
//				{
//					DataSet dsBooking = ImportConsignmentsDAL.GetBookingDetailsByBookingNo(appID, enterpriseID, dsConsignment.Tables[0].Rows[cntDS]["booking_no"].ToString());
//
//					if(dsBooking.Tables[0].Rows.Count > 0)
//					{
//						if((dsBooking.Tables[0].Rows[0]["act_pickup_datetime"] != null) && 
//							(!dsBooking.Tables[0].Rows[0]["act_pickup_datetime"].GetType().Equals(System.Type.GetType("System.DBNull"))) && 
//							(dsBooking.Tables[0].Rows[0]["act_pickup_datetime"].ToString() != ""))
//						{
//							DateTime dtTmp = (DateTime)dsBooking.Tables[0].Rows[0]["act_pickup_datetime"];
//							tmpStrDate = dtTmp.ToString("dd/MM/yyyy HH:mm");
//						}
//					}	
//
//					dsBooking.Dispose();
//					dsBooking = null;
//				}	
//
//				DataSet dsEprise = SysDataManager1.GetEnterpriseProfile(appID,enterpriseID);
//				DateTime dtTMPDate = System.DateTime.ParseExact(tmpStrDate,"dd/MM/yyyy HH:mm",null); 
//	
//				// Saturday
//				if(dtTMPDate.DayOfWeek == System.DayOfWeek.Saturday)
//				{
//					if((dsEprise.Tables[0].Rows[0]["sat_pickup_avail"] != null) && 
//						(!dsEprise.Tables[0].Rows[0]["sat_pickup_avail"].GetType().Equals(System.Type.GetType("System.DBNull"))) && 
//						(dsEprise.Tables[0].Rows[0]["sat_pickup_avail"].ToString() != "") &&	
//						(dsEprise.Tables[0].Rows[0]["sat_pickup_avail"].ToString() != "N")
//						)					
//					{
//						cntDS++;
//					}
//					else 
//					{
//						dsConsignment.Tables[0].Rows[cntDS].Delete();
//						dsConsignment.AcceptChanges();
//						pickupErrorFlag = true;
//						cntDS = 0;
//						PUPErrorMsg = "Pickup on Saturday is not available.";
//					}
//				}
//
//				// Sunday
//				if(dtTMPDate.DayOfWeek == System.DayOfWeek.Sunday)
//				{
//					if((dsEprise.Tables[0].Rows[0]["sun_pickup_avail"] != null) && 
//						(!dsEprise.Tables[0].Rows[0]["sun_pickup_avail"].GetType().Equals(System.Type.GetType("System.DBNull"))) && 
//						(dsEprise.Tables[0].Rows[0]["sun_pickup_avail"].ToString() != "") &&	
//						(dsEprise.Tables[0].Rows[0]["sun_pickup_avail"].ToString() != "N")
//						)					
//					{
//						cntDS++;
//					}
//					else 
//					{
//						dsConsignment.Tables[0].Rows[cntDS].Delete();
//						dsConsignment.AcceptChanges();
//						pickupErrorFlag = true;
//						cntDS = 0;
//						PUPErrorMsg = "Pickup on Sunday is not available.";
//					}
//				}
//
//				// Monday - Friday
//				if((dtTMPDate.DayOfWeek != System.DayOfWeek.Saturday) && (dtTMPDate.DayOfWeek != System.DayOfWeek.Sunday))
//				{
//					Zipcode zipCodeCheck = new Zipcode();
//					zipCodeCheck.Populate(appID,enterpriseID, dsConsignment.Tables[0].Rows[cntDS]["payer_zipcode"].ToString());
//
//					bool isPubDay = DomesticShipmentMgrDAL.IsEnterpriseHolidayOfAutoManifest(appID, enterpriseID, dtTMPDate, zipCode.StateCode);
//
//					if(isPubDay)
//					{
//						dsConsignment.Tables[0].Rows[cntDS].Delete();
//						dsConsignment.AcceptChanges();
//						pickupErrorFlag = true;
//						cntDS = 0;
//						PUPErrorMsg = "Pickup on Public Holidays is not available.";
//					}
//					else 
//					{
//						cntDS++;
//					}
//				}
//			}
//			//Filter Con by PicupDate
//
//			foreach(DataRow dr in dsConsignment.Tables[0].Rows)
//			{
//				try 
//				{
//					DataSet FinalDsConsignment = DomesticShipmentMgrDAL.GetEmptyDomesticShipDS();
//					DataSet FinalDsPackage = DomesticShipmentMgrDAL.GetEmptyPkgDtlDS();
//					DataSet FinalDsVAS = DomesticShipmentMgrDAL.GetEmptyVASDS();
//					DataSet TmpDsVAS = DomesticShipmentMgrDAL.GetEmptyVASDS();
//
//					if(FinalDsConsignment.Tables[0].Rows.Count == 1) 
//					{
//						FinalDsConsignment.Tables[0].Rows[0].Delete();
//						FinalDsConsignment.AcceptChanges();
//					}
//
//					if(FinalDsPackage.Tables[0].Rows.Count == 1) 
//					{
//						FinalDsPackage.Tables[0].Rows[0].Delete();
//						FinalDsPackage.AcceptChanges();
//					}
//
//					if(FinalDsVAS.Tables[0].Rows.Count == 1) 
//					{
//						FinalDsVAS.Tables[0].Rows[0].Delete();
//						FinalDsVAS.AcceptChanges();
//					}
//
//					if(TmpDsVAS.Tables[0].Rows.Count == 1) 
//					{
//						TmpDsVAS.Tables[0].Rows[0].Delete();
//						TmpDsVAS.AcceptChanges();
//					}
//					long booking_no = 0;
//					String strApplyDimWt = "";
//					//String strApplyESA = "";
//					String strDimBytot = ""; //Jeab 12 Jan 11
//
//					if((dr["payerid"] != null) && 
//						(!dr["payerid"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
//						(dr["payerid"].ToString() != ""))
//					{
//						if (dr["payerid"].ToString().Trim().ToUpper() != "NEW")
//						{
//							customer.Populate(appID,enterpriseID, dr["payerid"].ToString());
//							strApplyDimWt = customer.ApplyDimWt;
//							//strApplyESA = customer.ESASurcharge;
//							strDimBytot = customer.Dim_By_tot;  //Jeab 12 Jan 11
//						}
//						else
//						{
//							strApplyDimWt = "N";
//							//strApplyESA = "N";
//							strDimBytot = "N";  //Jeab 12 Jan 11
//						}
//					}
//					else
//					{
//						strApplyDimWt = "N";
//						//strApplyESA = "N";
//						strDimBytot = "N";  //Jeab 12 Jan 11
//					}
//
//					
//					DataRow[] drPackage = dsPackage.Tables[0].Select("consignment_no = '" + dr["consignment_no"].ToString() + "'");
//					
//					//Jeab 13 Jan 11
//					decimal decTotDimWt = 0;
//					decimal decTotWt = 0;
//					foreach(DataRow drLocal in drPackage)
//					{
//						decimal tot_wt = 0;
//						decimal tot_dim_wt = 0;
//						decimal iWeight =0;
//						iWeight=Convert.ToDecimal(drLocal["pkg_wt"]);
//						if(iWeight<1)
//						{
//							iWeight = 1;
//						}
//						else
//						{
//							iWeight = EnterpriseRounding(iWeight);
//						}
//							
//						tot_wt = TIESUtility.EnterpriseRounding((iWeight * Convert.ToInt32(drLocal["pkg_qty"])), wt_rounding_method, wt_increment_amt);
//						//tot_wt = Convert.ToDecimal(drLocal["pkg_wt"])*Convert.ToInt32(drLocal["pkg_qty"]);  //Jeab 22 Feb 2011
//						decTotWt += tot_wt;
//
//						if((dr["payerid"] != null) && 
//							(!dr["payerid"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
//							(dr["payerid"].ToString() != ""))
//						{
//							if (dr["payerid"].ToString().Trim().ToUpper() != "NEW")
//							{
//								DataSet dscustInfo = SysDataMgrDAL.GetCustInforByCustID(utility.GetAppID(),
//									utility.GetEnterpriseID(), 0, 0, dr["payerid"].ToString()).ds;
//								if (dscustInfo.Tables[0].Rows.Count > 0)
//								{
//									if((dscustInfo.Tables[0].Rows[0]["density_factor"] != null) && 
//										(!dscustInfo.Tables[0].Rows[0]["density_factor"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
//										(dscustInfo.Tables[0].Rows[0]["density_factor"].ToString() != ""))
//									{
//										fDensityFactor = (decimal)dscustInfo.Tables[0].Rows[0]["density_factor"];
//									}
//									else
//									{
//										fDensityFactor = (decimal)enterprise.DensityFactor;
//									}
//								}
//								else
//								{
//									fDensityFactor = (decimal)enterprise.DensityFactor;
//								}
//							}
//							else
//							{
//								fDensityFactor = (decimal)enterprise.DensityFactor;
//							}
//						}
//						else
//						{
//							fDensityFactor = (decimal)enterprise.DensityFactor;
//						}
//						//************* density_factor for Flexible Rate ********************//
//							
//						//TU 14/07/08
//						decimal fDimWt = 0;
//						if(fDensityFactor > 0)
//						{
//							fDimWt = Convert.ToDecimal(drLocal["pkg_length"]) * Convert.ToDecimal(drLocal["pkg_breadth"]) * Convert.ToDecimal(drLocal["pkg_height"]) / fDensityFactor;
//						}
//						fDimWt = EnterpriseRounding(fDimWt);
//						if(fDimWt<1)
//						{
//							fDimWt=1;
//						}
//						tot_dim_wt = TIESUtility.EnterpriseRounding(fDimWt, wt_rounding_method, wt_increment_amt) * Convert.ToInt32(drLocal["pkg_qty"]);
//
//						decTotDimWt += tot_dim_wt;
//					}
//					//Jeab 13 Jan 11  =========> End
//
//					foreach(DataRow drLocal in drPackage)
//					{
//						DataRow tmpDrPKG = FinalDsPackage.Tables[0].NewRow();
//						decimal tot_wt = 0;
//						decimal tot_dim_wt = 0;
//				
//						tmpDrPKG["mps_no"] = Convert.ToString(drLocal["mps_code"]);
//						tmpDrPKG["pkg_length"] = Convert.ToDecimal(drLocal["pkg_length"]);
//						tmpDrPKG["pkg_breadth"] = Convert.ToDecimal(drLocal["pkg_breadth"]);
//						tmpDrPKG["pkg_height"] = Convert.ToDecimal(drLocal["pkg_height"]);
//						tmpDrPKG["pkg_wt"] = Convert.ToDecimal(drLocal["pkg_wt"]);
//						tmpDrPKG["pkg_qty"] = Convert.ToInt32(drLocal["pkg_qty"]);
//						tmpDrPKG["pkg_volume"] = Math.Round((Convert.ToDecimal(drLocal["pkg_length"]) * Convert.ToDecimal(drLocal["pkg_breadth"]) * Convert.ToDecimal(drLocal["pkg_height"])), 2);
//
//						//TU 14/07/08
//						decimal iWeight =0;
//						iWeight=Convert.ToDecimal(drLocal["pkg_wt"]);
//						if(iWeight<1)
//						{
//							iWeight = 1;
//						}
//						else
//						{
//							iWeight = EnterpriseRounding(iWeight);
//						}
//						
//						tot_wt = TIESUtility.EnterpriseRounding((iWeight * Convert.ToInt32(drLocal["pkg_qty"])), wt_rounding_method, wt_increment_amt);
//						//
//						
//						tmpDrPKG["tot_wt"] = tot_wt;
//
//						tmpDrPKG["tot_act_wt"] = Convert.ToDecimal(drLocal["pkg_wt"])*Convert.ToInt32(drLocal["pkg_qty"]);//TU 08/07/08
//
//						//************* density_factor for Flexible Rate ********************//
//						if((dr["payerid"] != null) && 
//							(!dr["payerid"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
//							(dr["payerid"].ToString() != ""))
//						{
//							if (dr["payerid"].ToString().Trim().ToUpper() != "NEW")
//							{
//								DataSet dscustInfo = SysDataMgrDAL.GetCustInforByCustID(utility.GetAppID(),
//									utility.GetEnterpriseID(), 0, 0, dr["payerid"].ToString()).ds;
//								if (dscustInfo.Tables[0].Rows.Count > 0)
//								{
//									if((dscustInfo.Tables[0].Rows[0]["density_factor"] != null) && 
//										(!dscustInfo.Tables[0].Rows[0]["density_factor"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
//										(dscustInfo.Tables[0].Rows[0]["density_factor"].ToString() != ""))
//									{
//										fDensityFactor = (decimal)dscustInfo.Tables[0].Rows[0]["density_factor"];
//									}
//									else
//									{
//										fDensityFactor = (decimal)enterprise.DensityFactor;
//									}
//								}
//								else
//								{
//									fDensityFactor = (decimal)enterprise.DensityFactor;
//								}
//							}
//							else
//							{
//								fDensityFactor = (decimal)enterprise.DensityFactor;
//							}
//						}
//						else
//						{
//							fDensityFactor = (decimal)enterprise.DensityFactor;
//						}
//						//************* density_factor for Flexible Rate ********************//
//						
//						//TU 14/07/08
//						decimal fDimWt = 0;
//						if(fDensityFactor > 0)
//						{
//							fDimWt = Convert.ToDecimal(drLocal["pkg_length"]) * Convert.ToDecimal(drLocal["pkg_breadth"]) * Convert.ToDecimal(drLocal["pkg_height"]) / fDensityFactor;
//						}
//						fDimWt = EnterpriseRounding(fDimWt);
//						if(fDimWt<1)
//						{
//							fDimWt=1;
//						}
//						tot_dim_wt = TIESUtility.EnterpriseRounding(fDimWt, wt_rounding_method, wt_increment_amt) * Convert.ToInt32(drLocal["pkg_qty"]);
//						//	
//						
//						tmpDrPKG["tot_dim_wt"] = tot_dim_wt;
//						
//						//Jeab 13 Jan 11
//						if(strDimBytot=="Y")
//						{
//							if(decTotWt > decTotDimWt)
//							{
//								tmpDrPKG["chargeable_wt"] = tot_wt;
//							}
//							else
//							{
//								tmpDrPKG["chargeable_wt"] = tot_dim_wt;
//							}
//						}
//						else
//						{
//							if(strApplyDimWt == "Y")
//							{
//								if(tot_wt < tot_dim_wt)
//								{
//									tmpDrPKG["chargeable_wt"] = tot_dim_wt;
//								}
//								else
//								{
//									tmpDrPKG["chargeable_wt"] = tot_wt;
//								}
//							}
//							else
//							{
//								tmpDrPKG["chargeable_wt"] = tot_wt;
//							}
//						}
//						//Jeab 13 Jan 11 =========> End
//
//						//						if(strApplyDimWt == "Y")
//						//						{
//						//							if(tot_wt < tot_dim_wt)
//						//							{
//						//								tmpDrPKG["chargeable_wt"] = tot_dim_wt;
//						//							}
//						//							else
//						//							{
//						//								tmpDrPKG["chargeable_wt"] = tot_wt;
//						//							}
//						//						}
//						//						else
//						//						{
//						//							tmpDrPKG["chargeable_wt"] = tot_wt;
//						//						}
//			
//						FinalDsPackage.Tables[0].Rows.Add(tmpDrPKG);
//						FinalDsPackage.AcceptChanges();
//					}
//
//					decimal shipment_tot_wt = 0;
//					decimal shipment_tot_act_wt = 0;//TU 08/07/08
//					decimal shipment_tot_dim_wt = 0;
//					int shipment_tot_pkg = 0;
//					decimal shipment_chargeable_wt = 0;
//
//
//					foreach(DataRow drLocal in FinalDsPackage.Tables[0].Rows)
//					{
//						shipment_tot_wt += Convert.ToDecimal(drLocal["tot_wt"]);
//						shipment_tot_act_wt+=Convert.ToDecimal(drLocal["tot_act_wt"]);//TU 08/07/08
//						shipment_tot_dim_wt += Convert.ToDecimal(drLocal["tot_dim_wt"]);
//						shipment_tot_pkg += Convert.ToInt32(drLocal["pkg_qty"]);
//						shipment_chargeable_wt += Convert.ToDecimal(drLocal["chargeable_wt"]);
//					}
//
//					DataRow tmpDrCons = FinalDsConsignment.Tables[0].NewRow();
//
//					if((dr["booking_no"] == null) || 
//						(dr["booking_no"].GetType().Equals(System.Type.GetType("System.DBNull"))) || 
//						dr["booking_no"].ToString().Trim() == "")
//						booking_no = 0;
//					else
//						booking_no = Convert.ToInt64(dr["booking_no"]);
//
//					tmpDrCons["booking_no"] = booking_no;
//					tmpDrCons["consignment_no"] = Convert.ToString(dr["consignment_no"]);
//					tmpDrCons["ref_no"] = Convert.ToString(dr["ref_no"]);
//					//					tmpDrCons["booking_datetime"] = System.DateTime.Now;
//					//Jeab 24 Dec 10
//					if((dr["booking_datetime"] == null) || 
//						(dr["booking_datetime"].GetType().Equals(System.Type.GetType("System.DBNull"))) || 
//						dr["booking_datetime"].ToString().Trim() == "")
//						tmpDrCons["booking_datetime"] = System.DateTime.Now;
//					else
//						tmpDrCons["booking_datetime"] = Convert.ToString(dr["booking_datetime"]); 
//					//Jeab 24 Dec 10 =========> End
//					tmpDrCons["payerid"] = Convert.ToString(dr["payerid"]);
//					tmpDrCons["payer_type"] =  (string)customer.payer_type;
//					tmpDrCons["new_account"] = Convert.ToString(dr["new_account"]);
//					tmpDrCons["payer_name"] =Convert.ToString(dr["payer_name"]);
//					tmpDrCons["payer_address1"] =Convert.ToString(dr["payer_address1"]);
//					tmpDrCons["payer_address2"] =Convert.ToString(dr["payer_address2"]);
//					tmpDrCons["payer_zipcode"] =Convert.ToString(dr["payer_zipcode"]);
//					
//					zipCode.Populate(appID,enterpriseID, Convert.ToString(dr["payer_zipcode"]));
//					tmpDrCons["payer_country"] = zipCode.Country;
//					tmpDrCons["payer_telephone"] =Convert.ToString(dr["payer_telephone"]);
//					tmpDrCons["payer_fax"] =Convert.ToString(dr["payer_fax"]);
//					tmpDrCons["payment_mode"] =Convert.ToString(dr["payment_mode"]);
//
//					tmpDrCons["sender_name"] =Convert.ToString(dr["sender_name"]);
//					tmpDrCons["sender_address1"] =Convert.ToString(dr["sender_address1"]);
//					tmpDrCons["sender_address2"] =Convert.ToString(dr["sender_address2"]);
//					tmpDrCons["sender_zipcode"] =Convert.ToString(dr["sender_zipcode"]);
//					zipCode.Populate(appID,enterpriseID, Convert.ToString(dr["sender_zipcode"]));
//					tmpDrCons["sender_country"] = zipCode.Country;
//					tmpDrCons["sender_telephone"] =Convert.ToString(dr["sender_telephone"]);
//					tmpDrCons["sender_fax"] =Convert.ToString(dr["sender_fax"]);
//					tmpDrCons["sender_contact_person"] =Convert.ToString(dr["sender_contact_person"]);
//
//					tmpDrCons["recipient_name"] =Convert.ToString(dr["recipient_name"]);
//					tmpDrCons["recipient_address1"] =Convert.ToString(dr["recipient_address1"]);
//					tmpDrCons["recipient_address2"] =Convert.ToString(dr["recipient_address2"]);
//					tmpDrCons["recipient_zipcode"] =Convert.ToString(dr["recipient_zipcode"]);
//					zipCode.Populate(appID,enterpriseID, Convert.ToString(dr["recipient_zipcode"]));
//					tmpDrCons["recipient_country"] = zipCode.Country;
//					tmpDrCons["recipient_telephone"] =Convert.ToString(dr["recipient_telephone"]);
//					tmpDrCons["recipient_fax"] =Convert.ToString(dr["recipient_fax"]);
//					tmpDrCons["recipient_contact_person"] =Convert.ToString(dr["recipient_contact_person"]);
//				
//					tmpDrCons["tot_pkg"] = shipment_tot_pkg;
//					tmpDrCons["tot_wt"] = TIESUtility.EnterpriseRounding(shipment_tot_wt, wt_rounding_method, wt_increment_amt);
//					tmpDrCons["tot_act_wt"] =shipment_tot_act_wt; //TU 08/07/08
//					tmpDrCons["tot_dim_wt"] = TIESUtility.EnterpriseRounding(shipment_tot_dim_wt, wt_rounding_method, wt_increment_amt);
//					tmpDrCons["chargeable_wt"] = TIESUtility.EnterpriseRounding(shipment_chargeable_wt, wt_rounding_method, wt_increment_amt);
//				
//					tmpDrCons["service_code"] = Convert.ToString(dr["service_code"]);
//					tmpDrCons["declare_value"] = Convert.ToDecimal(dr["declare_value"]);
//
//					decimal percent_dv_additional = 0;
//					decimal max_insurance_cover = 0;
//
//					if((customer.insurance_percent_surcharge == null) || 
//						(customer.insurance_percent_surcharge.GetType().Equals(System.Type.GetType("System.DBNull"))))
//						percent_dv_additional = (decimal)enterprise.InsPercSurchrg;
//					else
//						percent_dv_additional = (decimal)customer.insurance_percent_surcharge;
//					tmpDrCons["percent_dv_additional"] = percent_dv_additional;
//
//					if((customer.insurance_maximum_amt == null) || 
//						(customer.insurance_maximum_amt.GetType().Equals(System.Type.GetType("System.DBNull"))))
//						max_insurance_cover = (decimal)enterprise.MaxInsuranceAmt;
//					else
//						max_insurance_cover = (decimal)customer.insurance_maximum_amt;
//					tmpDrCons["max_insurance_cover"] = max_insurance_cover;
//
//					
//
//					//########## Invoicing Phase ##########
//
//					DomesticShipmentMgrBAL domesticBAL = new DomesticShipmentMgrBAL();
//					decimal decInsSurchrg = domesticBAL.ComputeInsSurchargeByCust(appID, enterpriseID, Convert.ToDecimal(dr["declare_value"]), dr["payerid"].ToString(), customer);
//					tmpDrCons["insurance_surcharge"] = Rounding(decInsSurchrg);
//
//					//########## Invoicing Phase ##########
//					
//					String tmpStrDate = System.DateTime.Now.ToString("dd/MM/yyyy HH:mm");
//					if((dr["booking_no"] != null) && 
//						(!dr["booking_no"].GetType().Equals(System.Type.GetType("System.DBNull"))) && 
//						(dr["booking_no"].ToString() != ""))
//					{
//						DataSet dsBooking = ImportConsignmentsDAL.GetBookingDetailsByBookingNo(appID, enterpriseID, dr["booking_no"].ToString());
//
//						if(dsBooking.Tables[0].Rows.Count > 0)
//						{
//							if((dsBooking.Tables[0].Rows[0]["act_pickup_datetime"] != null) && 
//								(!dsBooking.Tables[0].Rows[0]["act_pickup_datetime"].GetType().Equals(System.Type.GetType("System.DBNull"))) && 
//								(dsBooking.Tables[0].Rows[0]["act_pickup_datetime"].ToString() != ""))
//							{
//								DateTime dtTmp = (DateTime)dsBooking.Tables[0].Rows[0]["act_pickup_datetime"];
//								tmpStrDate = dtTmp.ToString("dd/MM/yyyy HH:mm");
//								tmpDrCons["act_pickup_datetime"] = System.DateTime.ParseExact(tmpStrDate,"dd/MM/yyyy HH:mm",null); 
//							}
//							else
//							{
//								tmpDrCons["act_pickup_datetime"] = System.DateTime.ParseExact(tmpStrDate,"dd/MM/yyyy HH:mm",null); 
//							}
//						}	
//						else
//						{
//							tmpDrCons["act_pickup_datetime"] = System.DateTime.ParseExact(tmpStrDate,"dd/MM/yyyy HH:mm",null);  
//						}	
//					}	
//					else
//					{
//						tmpDrCons["act_pickup_datetime"] = System.DateTime.ParseExact(tmpStrDate,"dd/MM/yyyy HH:mm",null);  
//					}
//
//					
//					tmpDrCons["act_delivery_date"] = System.DBNull.Value;
//					tmpDrCons["payment_type"] = Convert.ToString(dr["payment_type"]);
//					tmpDrCons["shipment_type"] = "D";
//					tmpDrCons["invoice_no"] = System.DBNull.Value;
//
//					//HC Return Task
//					if((dr["return_pod_hc"] == null) || 
//						(dr["return_pod_hc"].GetType().Equals(System.Type.GetType("System.DBNull"))) || 
//						(dr["return_pod_hc"].ToString() == ""))
//					{
//						if(customer.PODSlipRequired == "Y")
//							tmpDrCons["return_pod_slip"] = "Y";
//						else
//							tmpDrCons["return_pod_slip"] = "N";
//					}
//					else
//					{
//						tmpDrCons["return_pod_slip"] = Convert.ToString(dr["return_pod_hc"]);
//					}
//
//					if((dr["return_invoice_hc"] == null) || 
//						(dr["return_invoice_hc"].GetType().Equals(System.Type.GetType("System.DBNull"))) || 
//						(dr["return_invoice_hc"].ToString() == ""))
//					{
//						if(Convert.ToString(customer.hc_invoice_required) == "Y")
//							tmpDrCons["return_invoice_hc"] = "Y";
//						else
//							tmpDrCons["return_invoice_hc"] = "N";
//					}
//					else
//					{
//						tmpDrCons["return_invoice_hc"] = Convert.ToString(dr["return_invoice_hc"]);
//					}
//					//HC Return Task
//
//					zipCode.Populate(appID,enterpriseID, Convert.ToString(dr["sender_zipcode"]));
//					tmpDrCons["origin_state_code"] = zipCode.StateCode;
//
//					DataSet tmpDC = ImportConsignmentsDAL.getDCToOrigStation(appID, enterpriseID,
//						0, 0, Convert.ToString(dr["sender_zipcode"])).ds;
//					tmpDrCons["origin_station"] = tmpDC.Tables[0].Rows[0]["origin_station"].ToString();
//
//					zipCode.Populate(appID,enterpriseID, Convert.ToString(dr["recipient_zipcode"]));
//					tmpDrCons["destination_state_code"] = zipCode.StateCode;
//
//					zipCode.Populate(appID,enterpriseID, tmpDrCons["payer_country"].ToString(), Convert.ToString(dr["recipient_zipcode"]));
//					tmpDrCons["route_code"] = zipCode.DeliveryRoute;
//
//					tmpDC = ImportConsignmentsDAL.getDCToDestStation(appID, enterpriseID,
//						0, 0, zipCode.DeliveryRoute).ds;
//					tmpDrCons["destination_station"] = tmpDC.Tables[0].Rows[0]["origin_station"].ToString();
//
//					tmpDC.Dispose();
//					tmpDC = null;
//
//					tmpDrCons["invoice_amt"] = System.DBNull.Value;
//					tmpDrCons["debit_amt"] = System.DBNull.Value;
//					tmpDrCons["credit_amt"] = System.DBNull.Value;
//					tmpDrCons["cash_collected"] = System.DBNull.Value;
//					tmpDrCons["last_status_code"] = "MDE";
//					tmpDrCons["last_exception_code"] = System.DBNull.Value;
//					tmpDrCons["last_status_datetime"] = System.DateTime.Now; 
//					//tmpDrCons["shpt_manifest_datetime"] = System.DBNull.Value; 
//					tmpDrCons["shpt_manifest_datetime"] = System.DateTime.Now;
//					tmpDrCons["delivery_manifested"] = "N"; 
//					tmpDrCons["quotation_no"] = System.DBNull.Value; 
//					tmpDrCons["quotation_version"] = 0; 
//
//					if((dr["commodity_code"] != null) && 
//						(!dr["commodity_code"].GetType().Equals(System.Type.GetType("System.DBNull"))) && 
//						(dr["commodity_code"].ToString() != ""))
//					{
//						tmpDrCons["commodity_code"] = Convert.ToString(dr["commodity_code"]);
//					}
//					else
//					{
//						tmpDrCons["commodity_code"] = System.DBNull.Value;
//					}
//					
//					tmpDrCons["remark"] = Convert.ToString(dr["remark"]);
//					tmpDrCons["cod_amount"] = Convert.ToDecimal(dr["cod_amount"]);
//
//					zipCode.Populate(appID,enterpriseID, Convert.ToString(dr["sender_zipcode"]));
//					String strOrgZone = zipCode.ZoneCode;
//
//					zipCode.Populate(appID,enterpriseID, Convert.ToString(dr["recipient_zipcode"]));
//					String strDestnZone = zipCode.ZoneCode;
//					
//					customer.Populate(appID,enterpriseID, tmpDrCons["payerid"].ToString());
//
//					float freightCharge = 0;
//					float freightCharge0 = 0;
//					float sumFreight = 0;
//					float fCalcFrgtChrg = 0;
//					if(customer.IsCustomer_Box == "Y")
//					{
//						foreach(DataRow drLocal in FinalDsPackage.Tables[0].Rows)
//						{
//							freightCharge = TIESUtility.ComputeFreightCharge(appID, enterpriseID,
//								tmpDrCons["payerid"].ToString(), customer.payer_type.ToString(), 
//								strOrgZone, strDestnZone, float.Parse(drLocal["chargeable_wt"].ToString())/int.Parse(drLocal["pkg_qty"].ToString()),
//								tmpDrCons["service_code"].ToString(),  
//								Convert.ToString(dr["sender_zipcode"]), Convert.ToString(dr["recipient_zipcode"]));
//							freightCharge = (freightCharge*int.Parse(drLocal["pkg_qty"].ToString()));
//							sumFreight = sumFreight+freightCharge;
//						}
//						if(shipment_tot_pkg < int.Parse(customer.Minimum_Box))
//						{
//							freightCharge0 = TIESUtility.ComputeFreightCharge(appID, enterpriseID,
//								tmpDrCons["payerid"].ToString(), customer.payer_type.ToString(), 
//								strOrgZone, strDestnZone, 0,
//								tmpDrCons["service_code"].ToString(),  
//								Convert.ToString(dr["sender_zipcode"]), Convert.ToString(dr["recipient_zipcode"]));
//							fCalcFrgtChrg = ((int.Parse(customer.Minimum_Box)-shipment_tot_pkg)*freightCharge0)+sumFreight;
//						}
//						else
//							fCalcFrgtChrg = sumFreight;
//					}
//					else
//					{
//						fCalcFrgtChrg = TIESUtility.ComputeFreightCharge(appID, enterpriseID,
//							tmpDrCons["payerid"].ToString(), customer.payer_type.ToString(), 
//							strOrgZone, strDestnZone, (float)shipment_chargeable_wt,
//							tmpDrCons["service_code"].ToString(),  
//							Convert.ToString(dr["sender_zipcode"]), Convert.ToString(dr["recipient_zipcode"]));
//					}
//					
//					ViewState["FreightCharge"] = fCalcFrgtChrg;
//					tmpDrCons["tot_freight_charge"] = Rounding(Convert.ToDecimal(fCalcFrgtChrg));
//					//tmpDrCons["tot_freight_charge"] = Convert.ToDecimal(fCalcFrgtChrg);
//
//					
//					//########## Other Surcharge ##########
//					decimal OtherSurcharge = 0;
//					if((customer.OtherSurchargeAmount == null || customer.OtherSurchargeAmount.ToString() == "") && (customer.OtherSurchargeAmountPercentage == null || customer.OtherSurchargeAmountPercentage.ToString() == ""))
//					{
//						OtherSurcharge = 0;
//					}
//					else
//					{
//						if(customer.OtherSurchargeAmount != "")
//						{ 
//							OtherSurcharge = Convert.ToDecimal(customer.OtherSurchargeAmount);
//						}
//						else if(customer.OtherSurchargeAmountPercentage != null && customer.OtherSurchargeAmountPercentage != "")
//						{
//							OtherSurcharge = Convert.ToDecimal(customer.OtherSurchargeAmountPercentage) * Convert.ToDecimal(fCalcFrgtChrg);
//
//							if(customer.OtherSurchargeMin != null && customer.OtherSurchargeMin != "")
//							{
//								OtherSurcharge = Math.Max(OtherSurcharge, Convert.ToDecimal(customer.OtherSurchargeMin));							
//							}
//							if(customer.OtherSurchargeMax != null && customer.OtherSurchargeMax != "")
//							{
//								OtherSurcharge = Math.Min(OtherSurcharge, Convert.ToDecimal(customer.OtherSurchargeMax));
//							}
//						}
//					}tmpDrCons["other_surch_amount"] = Rounding(OtherSurcharge);
//										
//					//########## End Other Surcharge ##########
//
//					//					if(strApplyESA == "N")
//					//					{
//					//						tmpDrCons["esa_surcharge"] = 0;
//					//					}
//					//					else
//					//					{
//					//					tmpDrCons["esa_surcharge"] = this.calCulateESA(Convert.ToString(dr["recipient_zipcode"]),
//					//					Convert.ToString(dr["sender_zipcode"]), Convert.ToString(dr["payerid"]),Convert.ToDecimal(fCalcFrgtChrg));
//					tmpDrCons["esa_surcharge"] = this.calCulateESA_ServiceType(Convert.ToString(dr["recipient_zipcode"]),
//						Convert.ToString(dr["sender_zipcode"]), Convert.ToString(dr["payerid"]),Convert.ToDecimal(fCalcFrgtChrg),Convert.ToString(dr["service_code"]));
//
//					//					}
//
//					if((customer.mbg != null) && 
//						(!customer.mbg.GetType().Equals(System.Type.GetType("System.DBNull"))) && 
//						(customer.mbg.ToString() != ""))
//					{
//						if(customer.mbg.ToString().Trim() == "Y")
//							tmpDrCons["mbg"]= "Y";
//						else
//							tmpDrCons["mbg"]= "N";
//					}
//					else
//					{
//						tmpDrCons["mbg"]= "N";
//					}
//
//
//					DomesticShipmentMgrBAL domesticMgrBAL = new DomesticShipmentMgrBAL();
//					String strDlvryDt =  CalcDlvryDtTimeImpCon(appID, enterpriseID,
//						System.DateTime.ParseExact(tmpStrDate,"dd/MM/yyyy HH:mm", null),
//						tmpDrCons["service_code"].ToString(), Convert.ToString(dr["recipient_zipcode"]), zipCode.StateCode).ToString();
// 
//
//					tmpDrCons["est_delivery_datetime"] = System.DateTime.ParseExact(strDlvryDt,"dd/MM/yyyy HH:mm", null);
//
//
//					//HC Return Task
//					if (tmpDrCons["return_pod_slip"].ToString().Trim() == "Y" ||
//						tmpDrCons["return_invoice_hc"].ToString().Trim() == "Y")
//					{
//						tmpDrCons["est_hc_return_datetime"] = DomesticShipmentMgrDAL.calEstHCDateTime(System.DateTime.ParseExact(strDlvryDt,"dd/MM/yyyy HH:mm", null), zipCode.StateCode,
//							"","", appID, enterpriseID);
//					}
//					else
//					{
//						tmpDrCons["est_hc_return_datetime"] =  System.DBNull.Value;
//					}
//					tmpDrCons["act_hc_return_datetime"] = System.DBNull.Value;
//					//HC Return Task
//
//					if(Convert.ToDecimal(dr["cod_amount"]) > 0)
//					{
//						DataSet dsVas = ImportConsignmentsDAL.GetSurchargeFromVAS(appID, enterpriseID);
//						if(dsVas.Tables[0].Rows.Count > 0)
//						{
//							foreach(DataRow drVAS in dsVas.Tables[0].Rows)
//							{
//								DataRow tmpDrVAS = TmpDsVAS.Tables[0].NewRow();
//
//								if((drVAS["vas_code"] != null) && 
//									(!drVAS["vas_code"].GetType().Equals(System.Type.GetType("System.DBNull"))) && 
//									(drVAS["vas_code"].ToString() != ""))
//								{
//									tmpDrVAS["vas_code"] = Convert.ToString(drVAS["vas_code"]);
//								}
//
//								if((drVAS["vas_description"] != null) && 
//									(!drVAS["vas_description"].GetType().Equals(System.Type.GetType("System.DBNull"))) && 
//									(drVAS["vas_description"].ToString() != ""))
//								{
//									tmpDrVAS["vas_description"] = Convert.ToString(drVAS["vas_description"]);
//								}
//
//								if((drVAS["surcharge"] != null) && 
//									(!drVAS["surcharge"].GetType().Equals(System.Type.GetType("System.DBNull"))) && 
//									(drVAS["surcharge"].ToString() != ""))
//								{
//									tmpDrVAS["surcharge"] = Convert.ToDecimal(drVAS["surcharge"]);
//								}
//
//								tmpDrVAS["remarks"] = System.DBNull.Value;
//
//								TmpDsVAS.Tables[0].Rows.Add(tmpDrVAS);
//								TmpDsVAS.AcceptChanges();
//							}
//						}
//					}
//
//					//					DataSet dsEnt = ImportConsignmentsDAL.GetSurchargeFromEnterpriseSurcharge(appID, enterpriseID);
//					//					if(dsEnt.Tables[0].Rows.Count > 0)
//					//					{
//					//						foreach(DataRow drEnt in dsEnt.Tables[0].Rows)
//					//						{
//					//							DataRow tmpDrEnt = FinalDsVAS.Tables[0].NewRow();
//					//
//					//							if((drEnt["vas_code"] != null) && 
//					//								(!drEnt["vas_code"].GetType().Equals(System.Type.GetType("System.DBNull"))) && 
//					//								(drEnt["vas_code"].ToString() != ""))
//					//							{
//					//								tmpDrEnt["vas_code"] = Convert.ToString(drEnt["vas_code"]);
//					//							}
//					//
//					//							if((drEnt["vas_description"] != null) && 
//					//								(!drEnt["vas_description"].GetType().Equals(System.Type.GetType("System.DBNull"))) && 
//					//								(drEnt["vas_description"].ToString() != ""))
//					//							{
//					//								tmpDrEnt["vas_description"] = Convert.ToString(drEnt["vas_description"]);
//					//							}
//					//
//					//							if((drEnt["surcharge"] != null) && 
//					//								(!drEnt["surcharge"].GetType().Equals(System.Type.GetType("System.DBNull"))) && 
//					//								(drEnt["surcharge"].ToString() != ""))
//					//							{
//					//								tmpDrEnt["surcharge"] = Convert.ToDecimal(drEnt["surcharge"]);
//					//							}
//					//
//					//							tmpDrEnt["remarks"] = System.DBNull.Value;
//					//
//					//							FinalDsVAS.Tables[0].Rows.Add(tmpDrEnt);
//					//							FinalDsVAS.AcceptChanges();
//					//						}
//					//					}
//
//					decimal tot_vas_surcharge = 0;
//					decimal Max_surcharge ,Min_surcharge,tmp_codsurcharge = 0;
//					
//					foreach(DataRow drLocal in TmpDsVAS.Tables[0].Rows)
//					{
//						String vas_code = "";
//
//						if((drLocal["vas_code"] != null) && 
//							(!drLocal["vas_code"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
//							(drLocal["vas_code"].ToString() != ""))
//						{
//							vas_code = drLocal["vas_code"].ToString().Trim();
//						}
//
//						//VAS = COD
//						if (vas_code == "COD")
//						{
//							DataSet dscustInfo = SysDataMgrDAL.GetCustInforByCustID(appID,enterpriseID, 
//								0, 0, tmpDrCons["payerid"].ToString().Trim()).ds;							
//						
//							if(dscustInfo.Tables[0].Rows.Count > 0)
//							{
//								//Retreive min_surcharge 
//								
//								if((dscustInfo.Tables[0].Rows[0]["min_cod_surcharge"] != null) && 
//									(!dscustInfo.Tables[0].Rows[0]["min_cod_surcharge"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
//									(dscustInfo.Tables[0].Rows[0]["min_cod_surcharge"].ToString() != "")) 
//
//								{
//									Min_surcharge =  (decimal) dscustInfo.Tables[0].Rows[0]["min_cod_surcharge"];
//								}
//								else
//								{
//									Min_surcharge =  0;
//								}
//
//								//Retreive max_surcharge 
//
//								if((dscustInfo.Tables[0].Rows[0]["max_cod_surcharge"] != null) && 
//									(!dscustInfo.Tables[0].Rows[0]["max_cod_surcharge"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
//									(dscustInfo.Tables[0].Rows[0]["max_cod_surcharge"].ToString() != ""))
//								{
//									
//									Max_surcharge =  (decimal) dscustInfo.Tables[0].Rows[0]["max_cod_surcharge"];
//									
//								}
//								else 
//								{
//									Max_surcharge =  0;									
//								}
//
//
//								//Have a Fix OR Percentage Of COD_Surcharge 
//								if(((dscustInfo.Tables[0].Rows[0]["cod_surcharge_amt"] != null) && 
//									(!dscustInfo.Tables[0].Rows[0]["cod_surcharge_amt"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
//									(dscustInfo.Tables[0].Rows[0]["cod_surcharge_amt"].ToString() != "")) ||
//									((dscustInfo.Tables[0].Rows[0]["cod_surcharge_percent"] != null) && 
//									(!dscustInfo.Tables[0].Rows[0]["cod_surcharge_percent"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
//									(dscustInfo.Tables[0].Rows[0]["cod_surcharge_percent"].ToString() != "")))
//								{
//									if((dscustInfo.Tables[0].Rows[0]["cod_surcharge_amt"] != null) && 
//										(!dscustInfo.Tables[0].Rows[0]["cod_surcharge_amt"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
//										(dscustInfo.Tables[0].Rows[0]["cod_surcharge_amt"].ToString() != ""))
//									{
//										// Fix COD_Surcharge
//										tmp_codsurcharge = (decimal)dscustInfo.Tables[0].Rows[0]["cod_surcharge_amt"];
//										//if (Max_surcharge != 0 && Min_surcharge != 0)								
//										//{							
//
//										//Check Boundary Of Customer Between min_cod and max_cod
//										//											if (tmp_codsurcharge > Max_surcharge)
//										//											{
//										//												tmp_codsurcharge = Max_surcharge;
//										//											}
//										//											else if(tmp_codsurcharge < Min_surcharge)
//										//											{
//										//												tmp_codsurcharge = Min_surcharge;
//										//											}
//
//										tot_vas_surcharge += tmp_codsurcharge;	
//										//}
//										//										else
//										//										{
//										//											tot_vas_surcharge += (decimal)drLocal["surcharge"];
//										//										}
//
//									}
//
//									if((dscustInfo.Tables[0].Rows[0]["cod_surcharge_percent"] != null) && 
//										(!dscustInfo.Tables[0].Rows[0]["cod_surcharge_percent"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
//										(dscustInfo.Tables[0].Rows[0]["cod_surcharge_percent"].ToString() != ""))
//									{
//										//Percentage Of COD_Surcharge
//										tmp_codsurcharge = (Convert.ToDecimal(dr["cod_amount"]) * ((decimal)dscustInfo.Tables[0].Rows[0]["cod_surcharge_percent"])) / 100;
//
//										if (Max_surcharge != 0 && Min_surcharge != 0)								
//										{							
//
//											//Check Boundary Of Customer Between min_cod and max_cod
//											if (tmp_codsurcharge > Max_surcharge)
//											{
//												tmp_codsurcharge = Max_surcharge;
//											}
//											else if(tmp_codsurcharge < Min_surcharge)
//											{
//												tmp_codsurcharge = Min_surcharge;
//											}
//
//											tot_vas_surcharge += TIESUtility.EnterpriseRounding(tmp_codsurcharge,wt_rounding_method,wt_increment_amt);	
//										}
//										else
//										{
//											tot_vas_surcharge += (decimal)drLocal["surcharge"];
//										}
//									}
//								}
//
//									//Have Nothing,Retrieve From VAS_Surcharge
//								else
//								{
//									//tmp_codsurcharge =  TIESUtility.EnterpriseRounding((decimal)drLocal["surcharge"],wt_rounding_method,wt_increment_amt);	
//									tmp_codsurcharge = (decimal)drLocal["surcharge"];
//									//									if (Max_surcharge != 0 && Min_surcharge != 0)								
//									//									{						
//									//										//Check Boundary Of Customer Between min_cod and max_cod
//									//										if (tmp_codsurcharge > Max_surcharge)
//									//										{
//									//											tmp_codsurcharge = Max_surcharge;
//									//										}
//									//										else if(tmp_codsurcharge < Min_surcharge)
//									//										{
//									//											tmp_codsurcharge = Min_surcharge;
//									//										}
//									//									}
//
//									tot_vas_surcharge += tmp_codsurcharge;	
//								}
//
//							}
//								
//						}
//
//						else
//						{
//							//tot_vas_surcharge += TIESUtility.EnterpriseRounding((decimal)drLocal["surcharge"],wt_rounding_method,wt_increment_amt);	
//							tot_vas_surcharge += (decimal)drLocal["surcharge"];
//						}
//					}
//
//
//					//New FinalDsVAS 
//					DataSet newdsVas = ImportConsignmentsDAL.GetSurchargeFromVAS(appID, enterpriseID);
//					if(newdsVas.Tables[0].Rows.Count > 0)
//					{
//
//						foreach(DataRow newdrVas in newdsVas.Tables[0].Rows)
//						{
//							DataRow tmpnewdrVas = FinalDsVAS.Tables[0].NewRow();
//
//							if((newdrVas["vas_code"] != null) && 
//								(!newdrVas["vas_code"].GetType().Equals(System.Type.GetType("System.DBNull"))) && 
//								(newdrVas["vas_code"].ToString() != ""))
//							{
//								tmpnewdrVas["vas_code"] = Convert.ToString(newdrVas["vas_code"]);
//							}
//
//							if((newdrVas["vas_description"] != null) && 
//								(!newdrVas["vas_description"].GetType().Equals(System.Type.GetType("System.DBNull"))) && 
//								(newdrVas["vas_description"].ToString() != ""))
//							{
//								tmpnewdrVas["vas_description"] = Convert.ToString(newdrVas["vas_description"]);
//							}
//
//					
//							tmpnewdrVas["surcharge"] = Convert.ToDecimal(tot_vas_surcharge);
//							tmpnewdrVas["remarks"] = System.DBNull.Value;
//
//							FinalDsVAS.Tables[0].Rows.Add(tmpnewdrVas);
//							FinalDsVAS.AcceptChanges();
//						}
//					}
//					tmpDrCons["tot_vas_surcharge"] = Convert.ToDecimal(tot_vas_surcharge);
//
//
//					/* ##############################################################################
//					 * Add New by   : Oak 
//					 * Date			: 14:54 2/4/2551
//					 * Description  : Ref. from 3.4 Domesticshipment (INVB), part of VAS Inserting 
//					 */					
//					decimal tmp_vas_surcharge  = 0;
//					decimal sumVas = 0;
//					string str_vas_code = "";
//				
//					// Enterprise Flag (Active / Inactive)
//					DataSet dsEprise = SysDataManager1.GetEnterpriseProfile(appID,enterpriseID);
//					// Customer VAS records
//					DataSet dsVasCust = SysDataMgrDAL.QueryVAS(appID, enterpriseID, dr["payerid"].ToString());
//					// Enterprise VAS records
//					DataSet dsEntVAS = ImportConsignmentsDAL.GetSurchargeAllEntVAS(appID, enterpriseID);
//					
//					//For Pickup DateTime
//					if(dsEprise.Tables[0].Rows.Count > 0)
//					{
//						// Saturday
//						if(Convert.ToDateTime(tmpDrCons["act_pickup_datetime"]).DayOfWeek == System.DayOfWeek.Saturday)
//							str_vas_code = dsEprise.Tables[0].Rows[0]["sat_pickup_vas_code"].ToString();
//
//						// Sunday
//						if(Convert.ToDateTime(tmpDrCons["act_pickup_datetime"]).DayOfWeek == System.DayOfWeek.Sunday)
//							str_vas_code = dsEprise.Tables[0].Rows[0]["sun_pickup_vas_code"].ToString();
//
//
//						// Monday - Friday
//						if((Convert.ToDateTime(tmpDrCons["act_pickup_datetime"]).DayOfWeek != System.DayOfWeek.Saturday) && 
//							(Convert.ToDateTime(tmpDrCons["act_pickup_datetime"]).DayOfWeek != System.DayOfWeek.Sunday))
//						{
//							Zipcode zipCodeCheck = new Zipcode();
//							zipCodeCheck.Populate(appID,enterpriseID, dr["payer_zipcode"].ToString());
//							bool isCheckPubDay = DomesticShipmentMgrDAL.IsEnterpriseHolidayOfAutoManifest(appID, enterpriseID, Convert.ToDateTime(tmpDrCons["act_pickup_datetime"]), zipCode.StateCode);
//
//							if(isCheckPubDay)
//								str_vas_code = dsEprise.Tables[0].Rows[0]["pub_pickup_vas_code"].ToString();
//						}
//							
//						
//						if(str_vas_code.ToString().Trim() != "")
//						{	
//							if(dsVasCust.Tables[0].Select("vas_code='"+str_vas_code+"'").Length > 0)
//							{	// Calculated form Customer VAS Surcharge
//
//								DataRow tmpnewdrVas = FinalDsVAS.Tables[0].NewRow();
//								DataRow[] drCustVas = dsVasCust.Tables[0].Select("vas_code='"+str_vas_code+"'");
//
//								if((drCustVas[0]["vas_code"] != null) && 
//									(!drCustVas[0]["vas_code"].GetType().Equals(System.Type.GetType("System.DBNull"))) && 
//									(drCustVas[0]["vas_code"].ToString() != ""))
//								{
//									tmpnewdrVas["vas_code"] = Convert.ToString(drCustVas[0]["vas_code"]);
//								}
//
//								if((drCustVas[0]["description"] != null) && 
//									(!drCustVas[0]["description"].GetType().Equals(System.Type.GetType("System.DBNull"))) && 
//									(drCustVas[0]["description"].ToString() != ""))
//								{
//									tmpnewdrVas["vas_description"] = Convert.ToString(drCustVas[0]["description"]);
//								}
//
//								if((drCustVas[0]["surcharge"] != null) && 
//									(!drCustVas[0]["surcharge"].GetType().Equals(System.Type.GetType("System.DBNull"))) && 
//									(drCustVas[0]["surcharge"].ToString() != ""))
//								{
//									tmp_vas_surcharge  =  (decimal)drCustVas[0]["surcharge"];
//								}
//								else
//								{
//									tmp_vas_surcharge = (decimal)(Convert.ToDecimal(drCustVas[0]["percent"]) *  Convert.ToDecimal(ViewState["FreightCharge"].ToString()) / 100);
//									
//									if((drCustVas[0]["min_surcharge"] != null) && 
//										(!drCustVas[0]["min_surcharge"].GetType().Equals(System.Type.GetType("System.DBNull"))) && 
//										(drCustVas[0]["min_surcharge"].ToString() != ""))
//									{
//										if ((decimal)drCustVas[0]["min_surcharge"] > tmp_vas_surcharge )
//										{
//											tmp_vas_surcharge = (decimal)drCustVas[0]["min_surcharge"];
//										}
//									}
//
//									if((drCustVas[0]["max_surcharge"] != null) && 
//										(!drCustVas[0]["max_surcharge"].GetType().Equals(System.Type.GetType("System.DBNull"))) && 
//										(drCustVas[0]["max_surcharge"].ToString() != ""))
//									{
//										if ((decimal)drCustVas[0]["max_surcharge"] < tmp_vas_surcharge )
//										{
//											tmp_vas_surcharge = (decimal)drCustVas[0]["max_surcharge"];
//										}
//									}
//								}
//
//								tmpnewdrVas["surcharge"] = tmp_vas_surcharge;
//								tmpnewdrVas["remarks"] = System.DBNull.Value;
//
//								FinalDsVAS.Tables[0].Rows.Add(tmpnewdrVas);
//								FinalDsVAS.AcceptChanges();
//
//								sumVas += tmp_vas_surcharge;
//							}
//							else if((dsVasCust.Tables[0].Select("vas_code='"+str_vas_code+"'").Length == 0) &&
//								(dsEntVAS.Tables[0].Select("vas_code='"+str_vas_code+"'").Length > 0))
//							{	// Calculated form Enterprise VAS Surcharge
//								DataRow tmpnewdrVas = FinalDsVAS.Tables[0].NewRow();
//								DataRow[] drEntVas = dsEntVAS.Tables[0].Select("vas_code='"+str_vas_code+"'");
//
//								if((drEntVas[0]["vas_code"] != null) && 
//									(!drEntVas[0]["vas_code"].GetType().Equals(System.Type.GetType("System.DBNull"))) && 
//									(drEntVas[0]["vas_code"].ToString() != ""))
//								{
//									tmpnewdrVas["vas_code"] = Convert.ToString(drEntVas[0]["vas_code"]);
//								}
//
//								if((drEntVas[0]["vas_description"] != null) && 
//									(!drEntVas[0]["vas_description"].GetType().Equals(System.Type.GetType("System.DBNull"))) && 
//									(drEntVas[0]["vas_description"].ToString() != ""))
//								{
//									tmpnewdrVas["vas_description"] = Convert.ToString(drEntVas[0]["vas_description"]);
//								}
//
//								if((drEntVas[0]["surcharge"] != null) && 
//									(!drEntVas[0]["surcharge"].GetType().Equals(System.Type.GetType("System.DBNull"))) && 
//									(drEntVas[0]["surcharge"].ToString() != ""))
//								{
//									tmp_vas_surcharge  =  (decimal)drEntVas[0]["surcharge"];
//								}
//
//								tmpnewdrVas["surcharge"] = tmp_vas_surcharge;
//								tmpnewdrVas["remarks"] = System.DBNull.Value;
//
//								FinalDsVAS.Tables[0].Rows.Add(tmpnewdrVas);
//								FinalDsVAS.AcceptChanges();
//
//								sumVas += tmp_vas_surcharge;
//							}
//						}
//
//						tmpDrCons["tot_vas_surcharge"] = (decimal)tmpDrCons["tot_vas_surcharge"]+ sumVas;
//					}
//					//					return;
//					//###################### End Add New by : Oak ####################################
//
//					FinalDsConsignment.Tables[0].Rows.Add(tmpDrCons);
//					FinalDsConsignment.AcceptChanges();
//
//					int iSerialNo = 0;
//					if(Session["SerialNo"] != null)
//					{
//						String strSerialNo = (String)Session["SerialNo"];
//						iSerialNo = Convert.ToInt32(strSerialNo);
//					}
//
//					preShipRec = ImportConsignmentsDAL.GetPreshipRec(appID, enterpriseID, Convert.ToString(dr["consignment_no"]));
//					if(FinalDsVAS != null)
//					{
//						if (preShipRec.Tables[0].Rows.Count > 0)
//						{
//							long preBookingNo  = 0;
//
//							if((preShipRec.Tables[0].Rows[0]["booking_no"] != null) && 
//								(!preShipRec.Tables[0].Rows[0]["booking_no"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
//								(preShipRec.Tables[0].Rows[0]["booking_no"].ToString() != ""))
//								preBookingNo = Convert.ToInt64(preShipRec.Tables[0].Rows[0]["booking_no"].ToString().Trim());
//							else
//								preBookingNo = 0;
//							
//							DomesticShipmentMgrDAL.UpdateDomesticShp_Partial(appID, enterpriseID, FinalDsConsignment, FinalDsVAS, 
//								FinalDsPackage, userID, preBookingNo);
//
//							preShipRec = ImportConsignmentsDAL.GetPreshipRec(appID, enterpriseID, Convert.ToString(dr["consignment_no"]));
//							if((preShipRec.Tables[0].Rows[0]["booking_no"] != null) && 
//								(!preShipRec.Tables[0].Rows[0]["booking_no"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
//								(preShipRec.Tables[0].Rows[0]["booking_no"].ToString() != ""))
//								preBookingNo = Convert.ToInt64(preShipRec.Tables[0].Rows[0]["booking_no"].ToString().Trim());
//
//							booking_no = preBookingNo;
//						}
//						else
//						{
//							booking_no = ImportConsignmentsDAL.AddDomesticShipment(appID,enterpriseID,
//								FinalDsConsignment, FinalDsVAS, FinalDsPackage, userID, iSerialNo,
//								Convert.ToString(dr["recipient_zipcode"]));
//						}
//					}
//					else
//					{
//						booking_no = 0;
//					}
//					
//						
//					String path_code = "";
//					String dest_station = "";
//					String orig_station = "";
//
//					//############### get origin information of manifest ###############
//					DataSet tmpAM_DC = ImportConsignmentsDAL.getDCToOrigStation(appID, enterpriseID,
//						0, 0, Convert.ToString(dr["sender_zipcode"])).ds;
//					orig_station  = tmpAM_DC.Tables[0].Rows[0]["origin_station"].ToString();
//
//
//					//############### get destination information of manifest ###############
//					tmpAM_DC = ImportConsignmentsDAL.getDCToDestStation(appID, enterpriseID,
//						0, 0, tmpDrCons["route_code"].ToString()).ds;
//					path_code = tmpAM_DC.Tables[0].Rows[0]["path_code"].ToString();
//					dest_station = tmpAM_DC.Tables[0].Rows[0]["origin_station"].ToString();
//
//					tmpAM_DC.Dispose();
//					tmpAM_DC = null;
//					// Comment by Tu 16/02/2011 Remove DeliveryManifest
//					if (DeliveryManifestMgrDAL.IsAutomanifest(appID, enterpriseID, Convert.ToString(dr["service_code"])) == "Y")
//					{
//						DomesticShipmentMgrDAL.setAutomaticManifest(appID, enterpriseID,
//							FinalDsConsignment, path_code, Convert.ToInt32(booking_no),
//							orig_station, dest_station, tmpDrCons["route_code"].ToString(), Convert.ToString(dr["service_code"]),0);
//					}
//					else
//					{
//						//9. If the above query does not return a value then:
//						//Call exit_w-o_manifesting (logic for this exit program is shown below)
//						DomesticShipmentMgrDAL.UpdateShipmentOfAutoManifest(appID, enterpriseID,
//							Convert.ToInt32(booking_no), Convert.ToString(dr["consignment_no"]));
//					}
//					//
//					importedCons +=  FinalDsConsignment.Tables[0].Rows.Count;
//				}
//				catch(Exception ex)
//				{
//					String tmpErrMessage = "";
//					tmpErrMessage = ex.Message;
//				}
//			}
//			
//			// Added on 18-Oct-11
//			// Insert shipment_pkg_box
//			//ImportConsignmentsDAL.ImportConsignment_SavingPkgDetail(appID,enterpriseID,userID);
//			//*************************
//
//			RetreiveConsignmentData();
//
//			if (pickupErrorFlag)
//				lblErrorMsg.Text = PUPErrorMsg;
//			else
//			{
//				if(!isEmpty)
//				{
//					lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"MSG_SAVESUCCESS",utility.GetUserCulture());	
//					lblTotalSavedCon.Text = "Total Imported: " + importedCons.ToString() + " Consignments.";
//				}
//				else
//				{
//					lblErrorMsg.Text = "";
//					lblTotalSavedCon.Text = "";
//				}
//			}
//
//			for(int i=0;i< arCon_No.Count;i++)
//			{
//				string con_no = Convert.ToString(arCon_No[i]);
//
//				if(con_no != "")
//					ReCalculateWhenSipBefore(appID, enterpriseID, con_no);
//			}
//			
//		}
		#endregion

		
		private void ReCalculateWhenSipBefore(String strAppId, String strEnterpriseId, string consignment_no)
		{
			try
			{
				DataSet ds = new DataSet();
				ds=RetSWBPcksBatchDAL.getPackageDetailByConsignment_No(strAppId,strEnterpriseId, consignment_no);
				if(ds.Tables[0].Rows.Count>0)
				{
					foreach(DataRow dr in ds.Tables[0].Rows)
					{
						if(!RetSWBPcksBatchBAL.isIdenticalPackage(strAppId,strEnterpriseId,dr["booking_no"].ToString(),dr["consignment_no"].ToString()))
						{
							DataSet dsQryPkg =null;
							DataSet dsPkgDetails =null;
							dsQryPkg = DomesticShipmentMgrDAL.GetPakageDtlsSWB(strAppId,strEnterpriseId,dr["consignment_no"].ToString(),dr["payerid"].ToString());
							if(dsQryPkg != null)
							{
								dsPkgDetails = dsQryPkg;

							}
							else
							{
								dsPkgDetails = DomesticShipmentMgrDAL.GetEmptyPkgDtlDS();
							}

							CalculateChargeable_wt(dsPkgDetails, dr["payerid"].ToString(), strAppId, strEnterpriseId);
											
							decimal fCalcFrgtChrg=0;
											
							decimal chargeable_wt = 0;
							if (dsPkgDetails.Tables[0].Rows.Count > 0)
							{
								for(int i=0;i < dsPkgDetails.Tables[0].Rows.Count;i++)
								{
									chargeable_wt += decimal.Parse(dsPkgDetails.Tables[0].Rows[i]["chargeable_wt"].ToString());
								}
								dr["char_wt"] = chargeable_wt;
							}
											

							fCalcFrgtChrg= CalculateAllFreightCharge(strAppId,strEnterpriseId,dr["payerid"].ToString() ,dr["service_code"].ToString() ,
								dr["sender_zipcode"].ToString(),dr["recipient_zipcode"].ToString(),dr["char_wt"].ToString(),dr["declare_value"].ToString() ,dsPkgDetails);
							if(fCalcFrgtChrg.ToString("#0.00").Equals("0.00") && dr["char_wt"].ToString().Length >0 && dr["declare_value"].ToString().Trim().Length >0)
							{
								continue;
							}
							else
							{
								if( dr["sender_zipcode"].ToString()!="" && dr["recipient_zipcode"].ToString()!="")
								{
									RetSWBPcksBatchDAL.MoveDataToOriginal_Insert(strAppId,strEnterpriseId,dr["booking_no"].ToString(),dr["consignment_no"].ToString());
									//RetSWBPcksBatchDAL.UpdateFreightCharge(strAppId,strEnterpriseId,dr["booking_no"].ToString(),dr["consignment_no"].ToString(),RoundingFreightCharge(fCalcFrgtChrg));
									// ESA Service Type
									decimal decESASSurcharge = DomesticShipmentMgrDAL.calCulateESA_ServiceType(strAppId,strEnterpriseId,dr["payerid"].ToString(),dr["sender_zipcode"].ToString(),dr["recipient_zipcode"].ToString(),dr["service_code"].ToString(),fCalcFrgtChrg.ToString());
									//GetOtherSurcharge
									decimal decTotOtherChrg= Rounding(DomesticShipmentMgrDAL.GetOtherSurcharge(strAppId,strEnterpriseId,dr["payerid"].ToString(),fCalcFrgtChrg.ToString()));
									//Insurance Surcharge
									decimal decInsuranceChrg =  RetSWBPcksBatchBAL.calInsuranceSurcharge(strAppId, strEnterpriseId, dr["payerid"].ToString().Trim(), Convert.ToDecimal( dr["declare_value"].ToString()));
									// VAS Surcharge
									DataSet m_dsVAS = new DataSet();
									m_dsVAS = DomesticShipmentMgrDAL.GetVASData(strAppId,strEnterpriseId,Convert.ToDecimal(dr["booking_no"]),dr["consignment_no"].ToString());
									if(m_dsVAS == null)
									{
										m_dsVAS = DomesticShipmentMgrDAL.GetEmptyVASDS();
									}
									decimal decTotVASSurcharge = DomesticShipmentMgrDAL.GetTotalSurcharge(m_dsVAS);
										
									decimal decTotalAmt = decTotVASSurcharge + fCalcFrgtChrg + decInsuranceChrg + decTotOtherChrg + decESASSurcharge;	
												
									RetSWBPcksBatchDAL.UpdateShipmentSurcharge(strAppId,strEnterpriseId,dr["booking_no"].ToString(),dr["consignment_no"].ToString(),fCalcFrgtChrg,decInsuranceChrg,decTotVASSurcharge,decESASSurcharge,decTotOtherChrg,decTotalAmt);
									RetSWBPcksBatchDAL.UpdateShipment_PKG_BOX(strAppId,strEnterpriseId,(int)dr["booking_no"],dr["consignment_no"].ToString(),dr["payerid"].ToString(),dr["sender_zipcode"].ToString(),dr["recipient_zipcode"].ToString(),dr["service_code"].ToString(),dsPkgDetails);
								}
							}
										
						}
					}
				}
			}
			catch{}

			}
		
		// edit by Tumz.
		//		fCalcFrgtChrg= DomesticShipmentMgrDAL.CalculateAllFreightCharge(strAppId,strEnterpriseId,dr["payerid"].ToString() ,dr["service_code"].ToString() ,
		//		dr["sender_zipcode"].ToString(),dr["recipient_zipcode"].ToString(),dr["char_wt"].ToString(),dr["declare_value"].ToString() ,dsPkgDetails,fCalcFrgtChrg);
			
		private decimal CalculateAllFreightCharge(string appID,string enterpriseID,
			string payerID,string ServiceCode,string senderZipcode,string RecipZipCode,string PkgChargeWt,
			string ShpDclrValue,DataSet dsPkgDetails)
		{			
			decimal fCalcFrgtChrg = 0;
			String strLocalErrorMsg = "";

			if( payerID != "" && senderZipcode.Trim() != "" &&
				RecipZipCode.Trim() != "" && PkgChargeWt.Trim() != "" &&
				ShpDclrValue.Trim() != "" /*&& IsCalculate("original_rated_freight")*/)
			{
				String strWeight = PkgChargeWt;

				if (strLocalErrorMsg.Trim() == "")
				{
					
					try
					{
						DataSet tmpCustZone = null;
						string sendCustZone = null;
						string recipCustZone = null;
						tmpCustZone = SysDataMgrDAL.GetCustZoneData(appID, enterpriseID,
							0, 0, payerID.Trim(), senderZipcode.Trim()).ds;
						if(tmpCustZone.Tables[0].Rows.Count > 0)
						{
							foreach(DataRow dr in tmpCustZone.Tables[0].Rows)
							{
								sendCustZone = dr["zone_code"].ToString().Trim();
							}
						}
						tmpCustZone = SysDataMgrDAL.GetCustZoneData(appID, enterpriseID,
							0, 0, payerID.Trim(), RecipZipCode).ds;
						if(tmpCustZone.Tables[0].Rows.Count > 0)
						{
							foreach(DataRow dr in tmpCustZone.Tables[0].Rows)
							{
								recipCustZone = dr["zone_code"].ToString().Trim();
							}
						}
						Customer customer = new Customer();
						customer.Populate(appID,enterpriseID, payerID.Trim());
						if(!customer.IsBoxRateAvailable(appID,enterpriseID,sendCustZone,recipCustZone,ServiceCode))
						{
							//lblErrorMsg.Text = "Customer has no Box Rate defined.";
							return 0;
						}
	
						fCalcFrgtChrg= DomesticShipmentMgrDAL.CalculateAllFreightCharge(appID,enterpriseID,payerID ,ServiceCode ,
							senderZipcode,RecipZipCode ,PkgChargeWt ,ShpDclrValue , dsPkgDetails,fCalcFrgtChrg);
						
						//						fCalcFrgtChrg = String.Format(((String)ViewState["m_format"], RoundingFreightCharge(fCalcFrgtChrg));
						return fCalcFrgtChrg;
					}
					catch(ApplicationException appException)
					{
						String strMsg = appException.Message;
						strLocalErrorMsg = strMsg;
						lblErrorMsg.Text = strMsg;

					}
				}
			}

			return 0;
			//return strLocalErrorMsg;
		}
	


		private void CalculateChargeable_wt(DataSet dsPkg, string payer_id, string strAppId, string strEnterpriseId)
		{
			Customer customer = new Customer();
			customer.Populate(strAppId,strEnterpriseId,payer_id);
			int cnt = dsPkg.Tables[0].Rows.Count;
			int i = 0;

			decimal decTotDimWt = 0;
			decimal decTotWt = 0;
			decimal  TOTVol = 0; 
			decimal decTot_act_Wt = 0;
			int iTotPkgs = 0;
			decimal decChrgWt = 0;


			//Jeab 28 Dec 10
			if (customer.Dim_By_tot =="Y")
			{
				decTotDimWt = 0;
				decTotWt = 0;
				for(i = 0;i<cnt;i++)
				{
					DataRow drEach = dsPkg.Tables[0].Rows[i];
					decTotDimWt += (decimal)drEach["tot_dim_wt"];
					decTotWt += (decimal)drEach["tot_wt"];
					if (drEach["tot_act_wt"]== System.DBNull.Value )
					{
						drEach["tot_act_wt"]= 0	;
					}
					decTot_act_Wt += (decimal)drEach["tot_act_wt"];
					iTotPkgs += (int)drEach["pkg_qty"];
					
					//					if (drEach["pkg_TOTvolume"] != null)
					//					{
					//						TOTVol += (decimal)drEach["pkg_TOTvolume"];
					//					}
					//					else
					//					{
					TOTVol += Convert.ToDecimal(drEach["pkg_length"])*Convert.ToDecimal(drEach["pkg_breadth"])*Convert.ToDecimal(drEach["pkg_height"])*Convert.ToDecimal(drEach["pkg_qty"]); //Jeab 22 Feb 2011
					//					}
					//edit by Tumz.
					////drEach["pkg_volume"] = Convert.ToDecimal(drEach["pkg_length"])*Convert.ToDecimal(drEach["pkg_breadth"])*Convert.ToDecimal(drEach["pkg_height"])*Convert.ToDecimal(drEach["pkg_qty"]);;
				}

				if(decTotDimWt < decTotWt)  // �� decTotWt  ᷹ decTot_act_Wt (�������Ѻ������͹���)  Jeab 28 Feb 2011
				{
					decChrgWt = decTotWt;  // �� decTotWt  ᷹ decTot_act_Wt  (�������Ѻ������͹���)    Jeab 28 Feb 2011
					for(i = 0;i<cnt;i++)
					{
						DataRow drEach = dsPkg.Tables[0].Rows[i];
						drEach["chargeable_wt"] = drEach["tot_wt"];
					}
				}
				else
				{
					decChrgWt = decTotDimWt;
					for(i = 0;i<cnt;i++)
					{
						DataRow drEach = dsPkg.Tables[0].Rows[i];
						drEach["chargeable_wt"] = drEach["tot_dim_wt"];
					}
				}
			}
			else
			{
				for(i = 0;i<cnt;i++)
				{
					DataRow drEach = dsPkg.Tables[0].Rows[i];
					decTotDimWt += (decimal)drEach["tot_dim_wt"];
					decTotWt += (decimal)drEach["tot_wt"];
					//TU on 17June08
					if (drEach["tot_act_wt"]== System.DBNull.Value )
					{
						drEach["tot_act_wt"]= 0	;
					}
					decTot_act_Wt += (decimal)drEach["tot_act_wt"];
					TOTVol += Convert.ToDecimal(drEach["pkg_length"])*Convert.ToDecimal(drEach["pkg_breadth"])*Convert.ToDecimal(drEach["pkg_height"])*Convert.ToDecimal(drEach["pkg_qty"]); //Jeab 22 Feb 2011

					//edit by Tumz.
					////drEach["pkg_volume"] = Convert.ToDecimal(drEach["pkg_length"])*Convert.ToDecimal(drEach["pkg_breadth"])*Convert.ToDecimal(drEach["pkg_height"])*Convert.ToDecimal(drEach["pkg_qty"]);

					iTotPkgs += (int)drEach["pkg_qty"];
					//decChrgWt += (decimal)drEach["chargeable_wt"];
					if(customer.ApplyDimWt == "Y" || customer.ApplyDimWt == "")
					{
						if((decimal)drEach["tot_wt"] < (decimal)drEach["tot_dim_wt"])
						{
							decChrgWt +=  (decimal)drEach["tot_dim_wt"];
							drEach["chargeable_wt"] = drEach["tot_dim_wt"];
						}
						else
						{
							decChrgWt += (decimal)drEach["tot_wt"];
							drEach["chargeable_wt"] = drEach["tot_wt"];
						}
					}
					else if(customer.ApplyDimWt == "N")
					{
						decChrgWt += (decimal)drEach["tot_wt"];
						drEach["chargeable_wt"] = drEach["tot_wt"];
					}
				}
			}
		}

		private void txtFilePath_TextChanged(object sender, System.EventArgs e)
		{
			//Session["inFile"] = inFile;
			//btnImport.Enabled = true;
//			try
//			{Session["inFile"] = inFile;
//				int result = ImportConsignmentsDAL.ImportConsignments_Browse(appID, enterpriseID, userID);
//
//				if(result != 0)
//				{
//					if(result == -1)
//						lblErrorMsg.Text = "@userid parameter may not be NULL or blank.";
//					else if(result == -2)
//						lblErrorMsg.Text = "@userid is not a valid user ID for enterprise: PNGAF.";
//					else if(result == -3)
//						lblErrorMsg.Text = "@enterpriseid parameter is missing or invalid.";
//					else if(result > 0)
//						lblErrorMsg.Text = "SQL error message.";
//
//					resultB = result;
//				}
//
//			}
//			catch(Exception ex)
//			{}
		}

		private decimal calCulateESA(String RecipZip, String SendZip,String custID, decimal FreightChrg)
		{
			decimal decSrchrg = 0;
			//Calculate the ESA surcharge
			if(RecipZip.Length > 0 && RecipZip != null && SendZip.Length > 0 && SendZip != null)
			{
		
				String strApplyDimWt = null;
				String strApplyESA = null;
				String strApplyESADel = null;

				Customer customer = new Customer();
				customer.Populate(appID, enterpriseID, custID);
				strApplyDimWt = customer.ApplyDimWt;
				strApplyESA = customer.ESASurcharge;
				strApplyESADel = customer.ESASurchargeDel;
				decimal surchargeOfSender = 0;
				decimal surchargeOfRecip = 0;
				

				if(strApplyESA == "Y")
				{			

					//***************** Get surcharge from Customer Zone by Sender Zipcode *************************
					DataSet tmpCustZone = SysDataMgrDAL.GetCustZoneData(appID, enterpriseID,
						0, 0, custID.Trim(), SendZip.Trim()).ds;
					if(tmpCustZone.Tables[0].Rows.Count > 0)
					{
						//Modify by Gwang on 4-Feb-08
						//A both of them have a value
						if(((tmpCustZone.Tables[0].Rows[0]["extended_area_surcharge_amt"] != null) && 
							(!tmpCustZone.Tables[0].Rows[0]["extended_area_surcharge_amt"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
							(tmpCustZone.Tables[0].Rows[0]["extended_area_surcharge_amt"].ToString() != "")) &&
							((tmpCustZone.Tables[0].Rows[0]["extended_area_surcharge_percent"] != null) && 
							(!tmpCustZone.Tables[0].Rows[0]["extended_area_surcharge_percent"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
							(tmpCustZone.Tables[0].Rows[0]["extended_area_surcharge_percent"].ToString() != ""))
							)
						{
							//At least is more than zero
							if(((decimal)tmpCustZone.Tables[0].Rows[0]["extended_area_surcharge_amt"] != 0) || ((decimal)tmpCustZone.Tables[0].Rows[0]["extended_area_surcharge_percent"] != 0))
							{
								if((decimal)tmpCustZone.Tables[0].Rows[0]["extended_area_surcharge_amt"] != 0)
								{
									surchargeOfSender = (decimal)tmpCustZone.Tables[0].Rows[0]["extended_area_surcharge_amt"];
								}
								else if((decimal)tmpCustZone.Tables[0].Rows[0]["extended_area_surcharge_percent"] != 0)
								{

									surchargeOfSender = (FreightChrg * ((decimal)tmpCustZone.Tables[0].Rows[0]["extended_area_surcharge_percent"])) / 100;
								}

							}
								//A both of them is Zero.the surcharge is Zero
							else
							{
								surchargeOfSender = 0;								
							}
						}
							//At least one has a value
						else if(((tmpCustZone.Tables[0].Rows[0]["extended_area_surcharge_amt"] != null) && 
							(!tmpCustZone.Tables[0].Rows[0]["extended_area_surcharge_amt"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
							(tmpCustZone.Tables[0].Rows[0]["extended_area_surcharge_amt"].ToString() != "")) ||
							((tmpCustZone.Tables[0].Rows[0]["extended_area_surcharge_percent"] != null) && 
							(!tmpCustZone.Tables[0].Rows[0]["extended_area_surcharge_percent"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
							(tmpCustZone.Tables[0].Rows[0]["extended_area_surcharge_percent"].ToString() != ""))
							)
						{
							if((tmpCustZone.Tables[0].Rows[0]["extended_area_surcharge_amt"] != null) && 
								(!tmpCustZone.Tables[0].Rows[0]["extended_area_surcharge_amt"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
								(tmpCustZone.Tables[0].Rows[0]["extended_area_surcharge_amt"].ToString() != ""))
							{
								surchargeOfSender = (decimal)tmpCustZone.Tables[0].Rows[0]["extended_area_surcharge_amt"];
							}

							if((tmpCustZone.Tables[0].Rows[0]["extended_area_surcharge_percent"] != null) && 
								(!tmpCustZone.Tables[0].Rows[0]["extended_area_surcharge_percent"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
								(tmpCustZone.Tables[0].Rows[0]["extended_area_surcharge_percent"].ToString() != ""))
							{
								surchargeOfSender = (FreightChrg * ((decimal)tmpCustZone.Tables[0].Rows[0]["extended_area_surcharge_percent"])) / 100;
							}
						}
							//A both of them has Null/""
						else
						{	
							Zipcode zipcode = new Zipcode();
							zipcode.Populate(appID,enterpriseID,SendZip.Trim());
							surchargeOfSender = zipcode.EASSurcharge;
						}
					}
					
					else
					{	
						Zipcode zipcode = new Zipcode();
						zipcode.Populate(appID,enterpriseID,SendZip.Trim());
						surchargeOfSender = zipcode.EASSurcharge;
					}
				}
				else
				{
					surchargeOfSender = 0;
				}

				//***************** Get surcharge from Customer Zone by Recept Zipcode *************************
				if(strApplyESADel == "Y")
				{

					//***************** Get surcharge from Customer Zone by Recept Zipcode *************************
					DataSet tmpCustZone = SysDataMgrDAL.GetCustZoneData(appID, enterpriseID,
						0, 0, custID.Trim(), RecipZip.Trim()).ds;
					if(tmpCustZone.Tables[0].Rows.Count > 0)
					{
						//Modify by Gwang on 4-Feb-08
						//A both of them have a value
						if(((tmpCustZone.Tables[0].Rows[0]["extended_area_surcharge_amt"] != null) && 
							(!tmpCustZone.Tables[0].Rows[0]["extended_area_surcharge_amt"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
							(tmpCustZone.Tables[0].Rows[0]["extended_area_surcharge_amt"].ToString() != "")) &&
							((tmpCustZone.Tables[0].Rows[0]["extended_area_surcharge_percent"] != null) && 
							(!tmpCustZone.Tables[0].Rows[0]["extended_area_surcharge_percent"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
							(tmpCustZone.Tables[0].Rows[0]["extended_area_surcharge_percent"].ToString() != ""))
							)
						{
							//A is more than zero
							if(((decimal)tmpCustZone.Tables[0].Rows[0]["extended_area_surcharge_amt"] != 0) || ((decimal)tmpCustZone.Tables[0].Rows[0]["extended_area_surcharge_percent"] != 0))
							{
								if((decimal)tmpCustZone.Tables[0].Rows[0]["extended_area_surcharge_amt"] != 0)
								{
									surchargeOfRecip = (decimal)tmpCustZone.Tables[0].Rows[0]["extended_area_surcharge_amt"];
								}
								else if((decimal)tmpCustZone.Tables[0].Rows[0]["extended_area_surcharge_percent"] != 0)
								{
									surchargeOfRecip = (FreightChrg * ((decimal)tmpCustZone.Tables[0].Rows[0]["extended_area_surcharge_percent"])) / 100;
								}

							}
								//A both of them is Zero.the surcharge is Zero
							else
							{
								surchargeOfRecip = 0;								
							}
						}
							//At least one has a value
						else if(((tmpCustZone.Tables[0].Rows[0]["extended_area_surcharge_amt"] != null) && 
							(!tmpCustZone.Tables[0].Rows[0]["extended_area_surcharge_amt"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
							(tmpCustZone.Tables[0].Rows[0]["extended_area_surcharge_amt"].ToString() != "")) ||
							((tmpCustZone.Tables[0].Rows[0]["extended_area_surcharge_percent"] != null) && 
							(!tmpCustZone.Tables[0].Rows[0]["extended_area_surcharge_percent"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
							(tmpCustZone.Tables[0].Rows[0]["extended_area_surcharge_percent"].ToString() != ""))
							)
						{
							if((tmpCustZone.Tables[0].Rows[0]["extended_area_surcharge_amt"] != null) && 
								(!tmpCustZone.Tables[0].Rows[0]["extended_area_surcharge_amt"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
								(tmpCustZone.Tables[0].Rows[0]["extended_area_surcharge_amt"].ToString() != ""))
							{
								surchargeOfRecip = (decimal)tmpCustZone.Tables[0].Rows[0]["extended_area_surcharge_amt"];
							}

							if((tmpCustZone.Tables[0].Rows[0]["extended_area_surcharge_percent"] != null) && 
								(!tmpCustZone.Tables[0].Rows[0]["extended_area_surcharge_percent"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
								(tmpCustZone.Tables[0].Rows[0]["extended_area_surcharge_percent"].ToString() != ""))
							{
								surchargeOfRecip = (FreightChrg * ((decimal)tmpCustZone.Tables[0].Rows[0]["extended_area_surcharge_percent"])) / 100;
							}
						}	
						else
						{	
							Zipcode zipcode = new Zipcode();
							zipcode.Populate(appID,enterpriseID,RecipZip.Trim());
							surchargeOfRecip = zipcode.EASSurcharge;
						}
					}
					else
					{	
						Zipcode zipcode = new Zipcode();
						zipcode.Populate(appID,enterpriseID,RecipZip.Trim());
						surchargeOfRecip = zipcode.EASSurcharge;
					}
				}
				else
				{
					surchargeOfRecip = 0;
				}
				decSrchrg = surchargeOfSender + surchargeOfRecip;
			}				
			//***************** Summary Surcharge *************************
				
				return Rounding(decSrchrg);
		}


		private decimal calCulateESA_ServiceType(String RecipZip, String SendZip,String custID, decimal FreightChrg, string svcCode)
		{

			decimal decSrchrg = 0;
			//Calculate the ESA surcharge
			if(RecipZip.Length > 0 && RecipZip != null && SendZip.Length > 0 && SendZip != null)
			{
		
				String strApplyDimWt = null;
				String strApplyESA = null;
				String strApplyESADel = null;

				Customer customer = new Customer();
				customer.Populate(appID, enterpriseID, custID);
				strApplyDimWt = customer.ApplyDimWt;
				strApplyESA = customer.ESASurcharge;
				strApplyESADel = customer.ESASurchargeDel;
				decimal surchargeOfSender = 0;
				decimal surchargeOfRecip = 0;

				if(strApplyESA == "Y")
				{					
					//***************** Get surcharge from Customer Zone by Sender Zipcode *************************
					DataSet tmpCustZone = SysDataManager2.GetESASurchargeDS(appID, enterpriseID,
						custID.Trim(), SendZip.Trim(),svcCode);
					//return ESA Surcharge All just send CustID and ZipCode 

					//					 tmpCustZone = SysDataMgrDAL.GetCustZoneData(appID, enterpriseID,
					//						0, 0, txtCustID.Text.Trim(), txtSendZip.Text.Trim()).ds;
					if(tmpCustZone.Tables[0].Rows.Count > 0)
					{
						//Modify by Gwang on 4-Feb-08
						//A both of them have a value
						if(((tmpCustZone.Tables[0].Rows[0]["esa_surcharge_amt"] != null) && 
							(!tmpCustZone.Tables[0].Rows[0]["esa_surcharge_amt"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
							(tmpCustZone.Tables[0].Rows[0]["esa_surcharge_amt"].ToString() != "")) &&
							((tmpCustZone.Tables[0].Rows[0]["esa_surcharge_percent"] != null) && 
							(!tmpCustZone.Tables[0].Rows[0]["esa_surcharge_percent"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
							(tmpCustZone.Tables[0].Rows[0]["esa_surcharge_percent"].ToString() != ""))
							)
						{
							//At least is more than zero
							if(((decimal)tmpCustZone.Tables[0].Rows[0]["esa_surcharge_amt"] != 0) || ((decimal)tmpCustZone.Tables[0].Rows[0]["esa_surcharge_percent"] != 0))
							{
								if((decimal)tmpCustZone.Tables[0].Rows[0]["esa_surcharge_amt"] != 0)
								{
									surchargeOfSender = (decimal)tmpCustZone.Tables[0].Rows[0]["esa_surcharge_amt"];
								}
								else if((decimal)tmpCustZone.Tables[0].Rows[0]["esa_surcharge_percent"] != 0)
								{

									surchargeOfSender = (FreightChrg * ((decimal)tmpCustZone.Tables[0].Rows[0]["esa_surcharge_percent"])) / 100;
								}

							}
								//A both of them is Zero.the surcharge is Zero
							else
							{
								surchargeOfSender = 0;								
							}
						}
							//At least one has a value
						else if(((tmpCustZone.Tables[0].Rows[0]["esa_surcharge_amt"] != null) && 
							(!tmpCustZone.Tables[0].Rows[0]["esa_surcharge_amt"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
							(tmpCustZone.Tables[0].Rows[0]["esa_surcharge_amt"].ToString() != "")) ||
							((tmpCustZone.Tables[0].Rows[0]["esa_surcharge_percent"] != null) && 
							(!tmpCustZone.Tables[0].Rows[0]["esa_surcharge_percent"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
							(tmpCustZone.Tables[0].Rows[0]["esa_surcharge_percent"].ToString() != ""))
							)
						{
							if((tmpCustZone.Tables[0].Rows[0]["esa_surcharge_amt"] != null) && 
								(!tmpCustZone.Tables[0].Rows[0]["esa_surcharge_amt"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
								(tmpCustZone.Tables[0].Rows[0]["esa_surcharge_amt"].ToString() != ""))
							{
								surchargeOfSender = (decimal)tmpCustZone.Tables[0].Rows[0]["esa_surcharge_amt"];
							}

							if((tmpCustZone.Tables[0].Rows[0]["esa_surcharge_percent"] != null) && 
								(!tmpCustZone.Tables[0].Rows[0]["esa_surcharge_percent"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
								(tmpCustZone.Tables[0].Rows[0]["esa_surcharge_percent"].ToString() != ""))
							{
								surchargeOfSender = (FreightChrg * ((decimal)tmpCustZone.Tables[0].Rows[0]["esa_surcharge_percent"])) / 100;
							}
						}
							//A both of them has Null/""
						else
						{	
							//							Zipcode zipcode = new Zipcode();
							//							zipcode.Populate(appID,enterpriseID,txtSendZip.Text.Trim());
							surchargeOfSender = 0;//zipcode.EASSurcharge;
						}
					//Apply MIN MAX
						if((tmpCustZone.Tables[0].Rows[0]["esa_surcharge_percent"] != null) && 
							(!tmpCustZone.Tables[0].Rows[0]["esa_surcharge_percent"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
							(tmpCustZone.Tables[0].Rows[0]["esa_surcharge_percent"].ToString() != ""))
						{
							if((decimal)tmpCustZone.Tables[0].Rows[0]["esa_surcharge_percent"] != 0)
							{

								if((tmpCustZone.Tables[0].Rows[0]["min_esa_amount"] != null) && 
									(!tmpCustZone.Tables[0].Rows[0]["min_esa_amount"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
									(tmpCustZone.Tables[0].Rows[0]["min_esa_amount"].ToString() != ""))
									if(surchargeOfSender<(decimal)tmpCustZone.Tables[0].Rows[0]["min_esa_amount"])					
										surchargeOfSender=(decimal)tmpCustZone.Tables[0].Rows[0]["min_esa_amount"];
							
								if((tmpCustZone.Tables[0].Rows[0]["max_esa_amount"] != null) && 
									(!tmpCustZone.Tables[0].Rows[0]["max_esa_amount"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
									(tmpCustZone.Tables[0].Rows[0]["max_esa_amount"].ToString() != ""))
									if(surchargeOfSender>(decimal)tmpCustZone.Tables[0].Rows[0]["max_esa_amount"])					
										surchargeOfSender=(decimal)tmpCustZone.Tables[0].Rows[0]["max_esa_amount"];
							}
						}
					//END Apply MIN MAX
					}
				
					else //IN CASE THERE IS NO RESULT OF ESA FROM DB
					{	
						//						Zipcode zipcode = new Zipcode();
						//						zipcode.Populate(appID,enterpriseID,txtSendZip.Text.Trim());
						surchargeOfSender = 0;//zipcode.EASSurcharge;
					}
				}
				else //IN CASE OF NO APPLY ESA
				{
					surchargeOfSender = 0;
				}

				if(strApplyESADel == "Y")
				{

					//***************** Get surcharge from Customer Zone by Recept Zipcode *************************
					DataSet tmpCustZone = SysDataManager2.GetESASurchargeDS(appID, enterpriseID,
						custID.Trim(), RecipZip.Trim(),svcCode);

					//					DataSet tmpCustZone = SysDataManager2.GetESASurchargeDS(utility.GetAppID(), utility.GetEnterpriseID(),
//						txtCustID.Text.Trim(), RecipZip.Trim(),this.txtShpSvcCode.Text.Trim());

					//					DataSet tmpCustZone = SysDataMgrDAL.GetCustZoneData(appID, enterpriseID,
					//						0, 0, txtCustID.Text.Trim(), txtRecipZip.Text.Trim()).ds;
					if(tmpCustZone.Tables[0].Rows.Count > 0)
					{
						//Modify by Gwang on 4-Feb-08
						//A both of them have a value
						if(((tmpCustZone.Tables[0].Rows[0]["esa_surcharge_amt"] != null) && 
							(!tmpCustZone.Tables[0].Rows[0]["esa_surcharge_amt"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
							(tmpCustZone.Tables[0].Rows[0]["esa_surcharge_amt"].ToString() != "")) &&
							((tmpCustZone.Tables[0].Rows[0]["esa_surcharge_percent"] != null) && 
							(!tmpCustZone.Tables[0].Rows[0]["esa_surcharge_percent"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
							(tmpCustZone.Tables[0].Rows[0]["esa_surcharge_percent"].ToString() != ""))
							)
						{
							//A is more than zero
							if(((decimal)tmpCustZone.Tables[0].Rows[0]["esa_surcharge_amt"] != 0) || ((decimal)tmpCustZone.Tables[0].Rows[0]["esa_surcharge_percent"] != 0))
							{
								if((decimal)tmpCustZone.Tables[0].Rows[0]["esa_surcharge_amt"] != 0)
								{
									surchargeOfRecip = (decimal)tmpCustZone.Tables[0].Rows[0]["esa_surcharge_amt"];
								}
								else if((decimal)tmpCustZone.Tables[0].Rows[0]["esa_surcharge_percent"] != 0)
								{
									surchargeOfRecip = (FreightChrg * ((decimal)tmpCustZone.Tables[0].Rows[0]["esa_surcharge_percent"])) / 100;
								}

							}
								//A both of them is Zero.the surcharge is Zero
							else
							{
								surchargeOfRecip = 0;								
							}
						}
							//At least one has a value
						else if(((tmpCustZone.Tables[0].Rows[0]["esa_surcharge_amt"] != null) && 
							(!tmpCustZone.Tables[0].Rows[0]["esa_surcharge_amt"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
							(tmpCustZone.Tables[0].Rows[0]["esa_surcharge_amt"].ToString() != "")) ||
							((tmpCustZone.Tables[0].Rows[0]["esa_surcharge_percent"] != null) && 
							(!tmpCustZone.Tables[0].Rows[0]["esa_surcharge_percent"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
							(tmpCustZone.Tables[0].Rows[0]["esa_surcharge_percent"].ToString() != ""))
							)
						{
							if((tmpCustZone.Tables[0].Rows[0]["esa_surcharge_amt"] != null) && 
								(!tmpCustZone.Tables[0].Rows[0]["esa_surcharge_amt"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
								(tmpCustZone.Tables[0].Rows[0]["esa_surcharge_amt"].ToString() != ""))
							{
								surchargeOfRecip = (decimal)tmpCustZone.Tables[0].Rows[0]["esa_surcharge_amt"];
							}

							if((tmpCustZone.Tables[0].Rows[0]["esa_surcharge_percent"] != null) && 
								(!tmpCustZone.Tables[0].Rows[0]["esa_surcharge_percent"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
								(tmpCustZone.Tables[0].Rows[0]["esa_surcharge_percent"].ToString() != ""))
							{
								surchargeOfRecip = (FreightChrg * ((decimal)tmpCustZone.Tables[0].Rows[0]["esa_surcharge_percent"])) / 100;
							}
						}	
						else
						{	
							//							Zipcode zipcode = new Zipcode();
							//							zipcode.Populate(appID,enterpriseID,txtRecipZip.Text.Trim());
							surchargeOfRecip = 0;//zipcode.EASSurcharge;
						}

						//Apply MIN MAX
						if((tmpCustZone.Tables[0].Rows[0]["esa_surcharge_percent"] != null) && 
							(!tmpCustZone.Tables[0].Rows[0]["esa_surcharge_percent"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
							(tmpCustZone.Tables[0].Rows[0]["esa_surcharge_percent"].ToString() != ""))
						{
							if((decimal)tmpCustZone.Tables[0].Rows[0]["esa_surcharge_percent"] != 0)
							{

								if((tmpCustZone.Tables[0].Rows[0]["min_esa_amount"] != null) && 
									(!tmpCustZone.Tables[0].Rows[0]["min_esa_amount"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
									(tmpCustZone.Tables[0].Rows[0]["min_esa_amount"].ToString() != ""))
									if(surchargeOfRecip<(decimal)tmpCustZone.Tables[0].Rows[0]["min_esa_amount"])					
										surchargeOfRecip=(decimal)tmpCustZone.Tables[0].Rows[0]["min_esa_amount"];
							
								if((tmpCustZone.Tables[0].Rows[0]["max_esa_amount"] != null) && 
									(!tmpCustZone.Tables[0].Rows[0]["max_esa_amount"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
									(tmpCustZone.Tables[0].Rows[0]["max_esa_amount"].ToString() != ""))
									if(surchargeOfRecip>(decimal)tmpCustZone.Tables[0].Rows[0]["max_esa_amount"])					
										surchargeOfRecip=(decimal)tmpCustZone.Tables[0].Rows[0]["max_esa_amount"];
							}
						}
						//END Apply MIN MAX

					}
					else //NO ESA FROM DB
					{	
						//						Zipcode zipcode = new Zipcode();
						//						zipcode.Populate(appID,enterpriseID,txtRecipZip.Text.Trim());
						surchargeOfRecip = 0;//zipcode.EASSurcharge;
					}
				}
				else //NO ESA FOR DEST
				{
					surchargeOfRecip = 0;
				}
				//***************** Summary Surcharge *************************

				decSrchrg = surchargeOfSender + surchargeOfRecip;
			}
				return Rounding(decSrchrg);
		}

		private String CalcDlvryDtTimeImpCon(String appID,String enterpriseID,DateTime pickUpDtTime,String srvCode,String recipZip, String stateCode)
		{
			Service service = new Service();
			service.Populate(appID,enterpriseID,srvCode);
			DateTime dtCommitTime = service.CommitTime;
			String strCommitTime = dtCommitTime.ToString("HH:mm");
			decimal iTransitDay = service.TransitDay;
			int iTransitHour = service.TransitHour;
			DateTime dtCalc = pickUpDtTime;
			String strDt = "";
			String strDlvryDtTime = "";

			String sat_delivery_avail = (String)System.Configuration.ConfigurationSettings.AppSettings["SatDel"]; 
			String sun_delivery_avail = (String)System.Configuration.ConfigurationSettings.AppSettings["SunDel"];
			String pub_delivery_avail = (String)System.Configuration.ConfigurationSettings.AppSettings["PubDel"];

			if(iTransitHour > 0)
			{
				//Add the hour to the pickup date
				dtCalc = pickUpDtTime.AddHours(iTransitHour);

				bool flag = true;
				int incDay = 0;
				DateTime newDate = dtCalc;

				while(flag)
				{
					// Saturday
					if(newDate.DayOfWeek == System.DayOfWeek.Saturday)
					{
						if(sat_delivery_avail.Trim() == "Y")
						{
							flag = false;
						}
						else
						{
							incDay = incDay + 1;
							newDate = newDate.AddDays(1);
						}
					}

					// Sunday
					if(newDate.DayOfWeek == System.DayOfWeek.Sunday)
					{
						if(sun_delivery_avail.Trim() == "Y")
						{
							flag = false;
						}
						else
						{
							incDay = incDay + 1;
							newDate = newDate.AddDays(1);
						}
					}

					// Monday - Friday
					if((newDate.DayOfWeek != System.DayOfWeek.Saturday) && (newDate.DayOfWeek != System.DayOfWeek.Sunday))
					{
						if(DomesticShipmentMgrDAL.IsEnterpriseHolidayOfAutoManifest(appID, enterpriseID, newDate, stateCode))
						{
							if(pub_delivery_avail.Trim() == "Y")
							{
								flag = false;
							}
							else
							{
								incDay = incDay + 1;
								newDate = newDate.AddDays(1);
							}
						}
						else
						{
							flag = false;
						}
					}
				}

				if (incDay > 0)
				{
					dtCalc = dtCalc.AddDays(incDay);
				}
				
				//strDlvryDtTime = dtCalc.ToString("dd/MM/yyyy HH:mm");
				strDlvryDtTime = dtCalc.ToString("dd/MM/yyyy") +" "+strCommitTime;
			}
			else if(iTransitDay >0)
			{
				//Add the day to the pickup date
				//dtCalc = pickUpDtTime.AddDays(Convert.ToDouble(iTransitDay));

				dtCalc = pickUpDtTime;

				int tmpTransDay = (int)iTransitDay;
				bool flag = true;
				while(flag)
				{
					if (tmpTransDay > 0)
					{
						dtCalc = dtCalc.AddDays(1);
							
						// Saturday
						if(dtCalc.DayOfWeek == System.DayOfWeek.Saturday)
						{
							if(sat_delivery_avail.Trim() == "Y") 
								tmpTransDay = tmpTransDay - 1;
						}

						// Sunday
						if(dtCalc.DayOfWeek == System.DayOfWeek.Sunday)
						{
							if(sun_delivery_avail.Trim() == "Y")
								tmpTransDay = tmpTransDay - 1;
						}

						// Monday - Friday
						if((dtCalc.DayOfWeek != System.DayOfWeek.Saturday) && (dtCalc.DayOfWeek != System.DayOfWeek.Sunday))
						{
							bool isPubDay = DomesticShipmentMgrDAL.IsEnterpriseHolidayOfAutoManifest(appID, enterpriseID, dtCalc, stateCode);

							if(isPubDay)
							{
								if(pub_delivery_avail.Trim() == "Y")
									tmpTransDay = tmpTransDay - 1;
							}
							else
							{
								tmpTransDay = tmpTransDay - 1;
							}
						}
					}
					else
					{
						flag = false;
					}
				}
				
				strDt = dtCalc.ToString("dd/MM/yyyy");
				strDlvryDtTime = strDt+" "+strCommitTime;
			}
			
			//Both Transit Day and  Transit Hour is zero
			if((iTransitDay == 0) && (iTransitHour == 0))
			{
				//Get the Pickup date & Recipient's cut off time from the zipcode
				strDt = pickUpDtTime.ToString("dd/MM/yyyy");
				Zipcode zipcode = new Zipcode();
				zipcode.Populate(appID,enterpriseID,recipZip);
				DateTime dtCuttOff =  zipcode.CuttOffTime;
				strCommitTime = dtCuttOff.ToString("HH:mm");
				strDlvryDtTime = strDt+" "+strCommitTime;
			}
			else if((iTransitDay > 0) && (iTransitHour == 0) && (dtCommitTime.Hour == 0) && (dtCommitTime.Minute == 0))
			{
				//Add the day to the pickup request date & Get the pickup Date's time...
				strDt = dtCalc.ToString("dd/MM/yyyy");

				//Get the commit time from pickUp request Date time
				strCommitTime = pickUpDtTime.ToString("HH:mm");
				strDlvryDtTime = strDt+" "+strCommitTime;
			}
			
			return strDlvryDtTime;
		}

		private decimal Rounding(decimal beforeRound)
		{
			return TIESUtility.EnterpriseRounding(beforeRound,(int)ViewState["wt_rounding_method"],(decimal)ViewState["wt_increment_amt"]);
		}
	
		//TU 04/07/08
		private decimal EnterpriseRounding(decimal beforeRound)
		{
			int wt_rounding_method = 0;
			decimal wt_increment_amt = 1;
			decimal reVal = beforeRound;

			if (ViewState["wt_rounding_method"] != null)
				wt_rounding_method = (int)ViewState["wt_rounding_method"];

			if (ViewState["wt_increment_amt"] != null)
				wt_increment_amt = (decimal)ViewState["wt_increment_amt"];
		
			decimal BeforDot = 0;
			decimal AfterDot = 0;
			decimal tmp05 = Convert.ToDecimal(0.5);
			string strBeforeRound = beforeRound.ToString();
			int dotIndex = Convert.ToInt32(strBeforeRound.IndexOf(".", 0));

			if(dotIndex < 0) 
			{
				BeforDot = Convert.ToDecimal(strBeforeRound);
				AfterDot = Convert.ToDecimal("0.00");
			}
			else 
			{ 
				BeforDot = Convert.ToDecimal(strBeforeRound.Substring(0, dotIndex));
				AfterDot = Convert.ToDecimal("0." + strBeforeRound.Substring(dotIndex + 1, strBeforeRound.Length - (dotIndex + 1)));
			}

			if(wt_increment_amt == tmp05)
			{
				if(AfterDot == tmp05) 
				{
					reVal = beforeRound;
				}
				else if (AfterDot > tmp05)
				{
					if(wt_rounding_method == 0)
						reVal = BeforDot + 1;
					else if (wt_rounding_method == 1)
						reVal = BeforDot + 1;
					else if (wt_rounding_method == 2)
						reVal = BeforDot + tmp05;
				}
				else if (AfterDot < tmp05)
				{
					if(wt_rounding_method == 0)
						reVal = BeforDot + tmp05;
					else if (wt_rounding_method == 1)
						reVal = BeforDot + tmp05;
					else if (wt_rounding_method == 2)
						reVal = BeforDot;
				}
			}
			else if(wt_increment_amt == 1)
			{
				if(AfterDot == tmp05) 
				{
					if(wt_rounding_method == 0)
						reVal = BeforDot + 1;
					else if (wt_rounding_method == 1)
						reVal = BeforDot + 1;
					else if (wt_rounding_method == 2)
						reVal = BeforDot;
				}
				else if (AfterDot > tmp05)
				{
					if(wt_rounding_method == 0)
						reVal = BeforDot + 1;
					else if (wt_rounding_method == 1)
						reVal = BeforDot + 1;
					else if (wt_rounding_method == 2)
						reVal = BeforDot;
				}
				else if (AfterDot < tmp05)
				{
					if(wt_rounding_method == 0)
						reVal = BeforDot;
					else if (wt_rounding_method == 1)
						reVal = BeforDot + 1;
					else if (wt_rounding_method == 2)
						reVal = BeforDot;
				}
			}

			return reVal;
		}

		private void btnHiddenPostbackBrowse_ServerClick(object sender, System.EventArgs e)
		{
			//Session["inFile"] = inFile;
			btnImport.Enabled = true;
		}
	}
}