using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.common.util;
using com.ties.DAL;
using com.common.classes;
using com.common.applicationpages;

namespace com.ties
{
	/// <summary>
	/// Summary description for State.
	/// </summary>
	public class State : BasePage
	{
		protected System.Web.UI.WebControls.Button btnQuery;
		protected System.Web.UI.WebControls.Button btnExecuteQuery;
		protected System.Web.UI.WebControls.Button btnInsert;
		protected System.Web.UI.WebControls.DataGrid dgState;

		//Utility utility = null;
		private String m_strAppID;
		private String m_strEnterpriseID;
		SessionDS m_sdsState = null;
		protected System.Web.UI.WebControls.Label lblTitle;
		protected System.Web.UI.WebControls.ValidationSummary ValidationSummary1;
		protected System.Web.UI.WebControls.Label lblErrorMessage;

		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			//utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings,Page.Session);
			m_strAppID = utility.GetAppID();
			m_strEnterpriseID = utility.GetEnterpriseID();

			if (!Page.IsPostBack)
			{
				QueryMode();
			}
			else
			{
				m_sdsState = (SessionDS) Session["SESSION_DS1"];
			}
			lblErrorMessage.Text = "";
			ValidationSummary1.HeaderText = Utility.GetLanguageText(ResourceType.UserMessage, "PLS_ENTER_MISS_FIELD", utility.GetUserCulture());
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			base.OnInit(e);
			InitializeComponent();
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnQuery.Click += new System.EventHandler(this.btnQuery_Click);
			this.btnExecuteQuery.Click += new System.EventHandler(this.btnExecuteQuery_Click);
			this.btnInsert.Click += new System.EventHandler(this.btnInsert_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		protected void dgState_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			Logger.LogDebugInfo("State","dgState_SelectedIndexChanged","INF004","updating data grid...");
		}

		protected void dgState_Update(object sender, DataGridCommandEventArgs e)
		{
			lblErrorMessage.Text = "";
			if (!btnExecuteQuery.Enabled)
			{
				int iRowsAffected = 0;
				int rowIndex = e.Item.ItemIndex;
				GetChangedRow(e.Item, e.Item.ItemIndex);

				//int iMode = (int)ViewState["Mode"];
				int iOperation = (int)ViewState["Operation"];
				try
				{
					if (iOperation == (int)Operation.Insert)
					{				
						iRowsAffected = SysDataManager2.InsertState(m_sdsState.ds, rowIndex, m_strAppID, m_strEnterpriseID);
						ArrayList paramValues = new ArrayList();
						paramValues.Add(""+iRowsAffected);						
						lblErrorMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"ROWS_INS",utility.GetUserCulture(),paramValues);
					}
					else
					{
						DataSet dsChangedRow = m_sdsState.ds.GetChanges();
						iRowsAffected = SysDataManager2.UpdateState(dsChangedRow, m_strAppID, m_strEnterpriseID);
						ArrayList paramValues = new ArrayList();
						paramValues.Add(""+iRowsAffected);						
						lblErrorMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"ROWS_UPD",utility.GetUserCulture(),paramValues);
						m_sdsState.ds.Tables[0].Rows[e.Item.ItemIndex].AcceptChanges();
					}
				}
				catch(ApplicationException appException)
				{
					String strMsg = appException.Message;
					strMsg = appException.InnerException.Message;
					strMsg = appException.InnerException.InnerException.Message;
					if(strMsg.IndexOf("duplicate") != -1 || strMsg.IndexOf("PRIMARY KEY") != -1)
					{
						lblErrorMessage.Text =  Utility.GetLanguageText(ResourceType.UserMessage,"NO_DUPLICATE_COUNTRY_STATE_NO",utility.GetUserCulture());
					}
					if(strMsg.IndexOf("unique") != -1 )
					{
						lblErrorMessage.Text =  Utility.GetLanguageText(ResourceType.UserMessage,"NO_DUPLICATE_COUNTRY_STATE_NO",utility.GetUserCulture());							
					}		
					if(strMsg.IndexOf("REFERENCE") != -1 )
					{
						lblErrorMessage.Text =  Utility.GetLanguageText(ResourceType.UserMessage,"CHILD_FOUND_CANNOT_DEL",utility.GetUserCulture());							
					}		
					return;
				}

				if (iRowsAffected == 0)
				{
					return;
				}
				Utility.EnableButton(ref btnInsert,ButtonType.Insert,true,m_moduleAccessRights);
				ViewState["Operation"] = Operation.None;
				dgState.EditItemIndex = -1;
				BindSGrid();
				Logger.LogDebugInfo("State","dgState_Update","INF004","updating data grid...");			
			}
		}

		protected void dgState_Cancel(object sender, DataGridCommandEventArgs e)
		{
			lblErrorMessage.Text = "";
			//int iMode = (int)ViewState["Mode"];
			int rowIndex = e.Item.ItemIndex;
			if ((int)ViewState["Mode"] == (int)ScreenMode.Insert && (int)ViewState["Operation"] == (int)Operation.Insert)
			{
				Utility.EnableButton(ref btnInsert,ButtonType.Insert,true,m_moduleAccessRights);
				m_sdsState.ds.Tables[0].Rows.RemoveAt(rowIndex);
				//ViewState["Mode"] = ScreenMode.None;
			}
			ViewState["Operation"] = Operation.None;
			dgState.EditItemIndex = -1;
			BindSGrid();
			Logger.LogDebugInfo("State","dgState_Edit","INF004","updating data grid...");
		}

		protected void dgState_Edit(object sender, DataGridCommandEventArgs e)
		{
			if((int)ViewState["Mode"] == (int)ScreenMode.Insert && (int) ViewState["Operation"] == (int)Operation.Insert && dgState.EditItemIndex > 0)
			{
				m_sdsState.ds.Tables[0].Rows.RemoveAt(dgState.EditItemIndex);
				m_sdsState.QueryResultMaxSize--;
				dgState.CurrentPageIndex = 0;
			}
			dgState.EditItemIndex = e.Item.ItemIndex;
			ViewState["Operation"] = Operation.Update;
			BindSGrid();
			Logger.LogDebugInfo("State","dgState_Edit","INF004","updating data grid...");
		}

		protected void dgState_Delete(object sender, DataGridCommandEventArgs e)
		{
			if (!btnExecuteQuery.Enabled)
			{
				if ((int)ViewState["Operation"] == (int)Operation.Update)
				{
					dgState.EditItemIndex = -1;
				}
				//dgState.EditItemIndex = e.Item.ItemIndex;
				BindSGrid();

				int rowIndex = e.Item.ItemIndex;
				m_sdsState = (SessionDS)Session["SESSION_DS1"];

				try
				{
					// delete from table
					int iRowsDeleted = SysDataManager2.DeleteState(m_sdsState.ds, rowIndex, m_strAppID, m_strEnterpriseID);
					ArrayList paramValues = new ArrayList();
					paramValues.Add(""+iRowsDeleted);						
					lblErrorMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"ROWS_DLD",utility.GetUserCulture(),paramValues);
					
				}
				catch(ApplicationException appException)
				{
					String strMsg = appException.Message;
					if(strMsg.IndexOf("FK") != -1)
					{
						lblErrorMessage.Text =  Utility.GetLanguageText(ResourceType.UserMessage,"ERR_DEL_COUNTRY_STATE_TRANS",utility.GetUserCulture());
					}
					if(strMsg.IndexOf("child record found") != -1 )
					{
						lblErrorMessage.Text =  Utility.GetLanguageText(ResourceType.UserMessage,"ERR_DEL_COUNTRY_STATE_TRANS",utility.GetUserCulture());
							
					}			
					else
					{
						lblErrorMessage.Text = strMsg;
					}

					Logger.LogTraceError("State.aspx.cs","dgState_Delete","RBAC003",appException.Message.ToString());
					//lblErrorMessage.Text = appException.Message.ToString();
					return;
				}

				// remove the data from grid
				//m_sdsState.ds.Tables[0].Rows.RemoveAt(rowIndex);

				if((int)ViewState["Mode"] == (int)ScreenMode.Insert)
				{
					m_sdsState.ds.Tables[0].Rows.RemoveAt(rowIndex);
					//m_sdsState.ds.Tables[0].Rows.RemoveAt(dgState.EditItemIndex-1);
					//m_sdsState.QueryResultMaxSize--;
					//dgState.CurrentPageIndex = 0;
				}
				else
				{
					RetreiveSelectedPage();
				}

				//if((int)ViewState["Mode"] == (int)ScreenMode.Insert && (int) ViewState["Operation"] == (int)Operation.Insert && dgState.EditItemIndex > 0)

				//Set ViewStates to None
				//ViewState["Mode"] = ScreenMode.None;
				ViewState["Operation"] = Operation.None;

				//Make the row in non-edit Mode
				EditHRow(false);

				BindSGrid();
				Logger.LogDebugInfo("State","dgState_Delete","INF004","updating data grid...");			
			}
		}

		protected void dgState_Bound(object sender, DataGridItemEventArgs e)
		{
			if(e.Item.ItemIndex == -1)
			{
				return;
			}

			DataRow drSelected = m_sdsState.ds.Tables[0].Rows[e.Item.ItemIndex];
			if(drSelected.RowState == DataRowState.Deleted)
			{
				return;
			}

			if ((int)ViewState["Operation"] != (int)Operation.Insert && (!btnExecuteQuery.Enabled))
			{
				msTextBox txtCountry = (msTextBox)e.Item.FindControl("txtCountry");
				msTextBox txtStateCode = (msTextBox)e.Item.FindControl("txtStateCode");
				if(txtCountry != null && txtStateCode != null) 
				{
					txtCountry.Enabled = false;
					txtStateCode.Enabled = false;
				}
			}
			if ((int)ViewState["Mode"] == (int)ScreenMode.Insert && (int)ViewState["Operation"] == (int)Operation.Insert)
			{
				e.Item.Cells[1].Enabled=false;
			}
		}

		protected void dgState_PageChange(object sender, DataGridPageChangedEventArgs e)
		{
			dgState.CurrentPageIndex = e.NewPageIndex;
			RetreiveSelectedPage();
			Logger.LogDebugInfo("State","dgState_PageChange","INF009","On Page Change "+e.NewPageIndex);
		}

		private void btnQuery_Click(object sender, System.EventArgs e)
		{
			lblErrorMessage.Text = "";
			dgState.CurrentPageIndex = 0;
			QueryMode();
		}

		private void btnExecuteQuery_Click(object sender, System.EventArgs e)
		{
			lblErrorMessage.Text = "";
			btnExecuteQuery.Enabled = false;

			//Since Query mode is always one row in edit mode, the rowindex is set to zero
			int rowIndex = 0; 

			GetChangedRow(dgState.Items[0], rowIndex);
			Session["QUERY_DS"] = m_sdsState.ds;

			RetreiveSelectedPage();

			//set all records to non-edit mode after execute query
			EditHRow(false);
			dgState.Columns[0].Visible=true;
			dgState.Columns[1].Visible=true;
		}

		private void btnInsert_Click(object sender, System.EventArgs e)
		{
			//clear the rows from the datagrid
			//m_sdsState.ds.Clear();
			if (btnExecuteQuery.Enabled == true)
			{
				btnExecuteQuery.Enabled = false;
			}
			if((int)ViewState["Mode"] != (int)ScreenMode.Insert || m_sdsState.ds.Tables[0].Rows.Count >= dgState.PageSize)
			{
				m_sdsState = SysDataManager2.GetEmptyState();
				//m_sdsState.ds.Clear();
			}
			else
			{
				AddRow();
			}

			Utility.EnableButton(ref btnInsert,ButtonType.Insert,false,m_moduleAccessRights);
			EditHRow(false);
			ViewState["Mode"] = ScreenMode.Insert;
			ViewState["Operation"] = Operation.Insert;
			dgState.Columns[0].Visible=true;
			dgState.Columns[1].Visible=true;
			//AddRow();
			dgState.EditItemIndex = m_sdsState.ds.Tables[0].Rows.Count - 1;
			dgState.CurrentPageIndex = 0;
			BindSGrid();
			getPageControls(Page);
			Logger.LogDebugInfo("State","btnInsert_Click","INF003","Data Grid Items count"+dgState.Items.Count);
		}

		private void BindSGrid()
		{
			dgState.VirtualItemCount = System.Convert.ToInt32(m_sdsState.QueryResultMaxSize);
			dgState.DataSource = m_sdsState.ds;
			dgState.DataBind();
			Session["SESSION_DS1"] = m_sdsState;
		}

		private void EditHRow(bool bItemIndex)
		{
			foreach (DataGridItem item in dgState.Items)
			{ 
				if (bItemIndex) 
				{
					dgState.EditItemIndex = item.ItemIndex; }
				else 
				{
					dgState.EditItemIndex = -1; }
			}
			dgState.DataBind();
		}

		private void QueryMode()
		{
			//Set ViewStates to None
			ViewState["Mode"] = ScreenMode.None;
			ViewState["Operation"] = Operation.None;

			//Set Button Enable Properties
			btnQuery.Enabled=true;
			btnExecuteQuery.Enabled=true;
			Utility.EnableButton(ref btnInsert,ButtonType.Insert,true,m_moduleAccessRights);

			//inset an empty row for query 
			m_sdsState = SysDataManager2.GetEmptyState();
			m_sdsState.DataSetRecSize = 1;
			Session["SESSION_DS1"] = m_sdsState;
			BindSGrid();

			//Make the row in Edit Mode
			EditHRow(true);
			dgState.Columns[0].Visible=false;
			dgState.Columns[1].Visible=false;
		}

		private void AddRow()
		{
			SysDataManager2.AddNewRowInStateCodeDS(ref m_sdsState);
			BindSGrid();
		}

		private void GetChangedRow(DataGridItem item, int rowIndex)
		{
			msTextBox txtCountry = (msTextBox)item.FindControl("txtCountry");
			msTextBox txtStateCode = (msTextBox)item.FindControl("txtStateCode");
			TextBox txtStateName = (TextBox)item.FindControl("txtStateName");
			//HC Return Task
			TextBox txtReturnDays = (TextBox)item.FindControl("txtReturnDays");
			//HC Return Task
				
			string strCountry = txtCountry.Text.ToString();
			string strCode = txtStateCode.Text.ToString();
			string strName = txtStateName.Text.ToString();
			//HC Return Task
			string strReturnDays = txtReturnDays.Text.ToString();
			//HC Return Task

			DataRow dr = m_sdsState.ds.Tables[0].Rows[rowIndex];
			dr[0] = strCountry;
			dr[1] = strCode;
			dr[2] = strName;
			//HC Return Task
			dr[3] = strReturnDays;
			//HC Return Task
		}

		private void RetreiveSelectedPage()
		{
			int iStartRow = dgState.CurrentPageIndex * dgState.PageSize;
			DataSet dsQuery = (DataSet)Session["QUERY_DS"];
			m_sdsState = SysDataManager2.GetStateCodeDS(utility.GetAppID(), utility.GetEnterpriseID(), iStartRow, dgState.PageSize, dsQuery);
			int iPageCnt = Convert.ToInt32((m_sdsState.QueryResultMaxSize - 1))/dgState.PageSize;
			if(iPageCnt < dgState.CurrentPageIndex)
			{
				dgState.CurrentPageIndex = System.Convert.ToInt32(iPageCnt);
			}

			dgState.SelectedIndex = -1;
			dgState.EditItemIndex = -1;
			
			BindSGrid();
			Session["SESSION_DS1"] = m_sdsState;
		}

	}
}
