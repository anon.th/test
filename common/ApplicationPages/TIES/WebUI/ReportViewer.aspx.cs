using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using CrystalDecisions.Web; 
using CrystalDecisions.Shared;
using CrystalDecisions.ReportSource;
using CrystalDecisions.CrystalReports.Engine;
using com.common.DAL;
using com.common.util;
using com.common.classes;
using com.common.applicationpages;
using com.ties.DAL;
using TIES.WebUI ;
using System.IO;

using ICSharpCode.SharpZipLib.Zip;


namespace com.ties
{
	/// <summary>
	/// Summary description for ReportViewer.
	/// </summary>
	public class ReportViewer : BasePopupPage
	{
		protected CrystalDecisions.Web.CrystalReportViewer rptViewer;
		//Utility utility =null;
		string strAppID=null;
		string strEnterpriseID=null;
		string strFORMID=null;
		string strUserId=null;			
		bool bShowGroupTree=false;		
		ReportDocument rptSource = null;
		protected System.Web.UI.WebControls.Button btnMoveFirst;
		protected System.Web.UI.WebControls.Button btnMovePrevious;
		protected System.Web.UI.WebControls.Button btnMoveNext;
		protected System.Web.UI.WebControls.Button btnMoveLast;
		protected com.common.util.msTextBox txtGoTo;
		protected System.Web.UI.WebControls.Button btnExport;
		protected System.Web.UI.WebControls.DropDownList ddbExport;
		protected System.Web.UI.WebControls.Button btnShowGrpTree;
		protected System.Web.UI.WebControls.DropDownList ddbZoom;
		protected System.Web.UI.WebControls.Button btnSearch;
		protected System.Web.UI.WebControls.TextBox txtTextToSearch;
		protected System.Web.UI.WebControls.Label lblZoom;
		protected System.Web.UI.WebControls.Label lblErrorMesssage;
		
		DataSet m_dsQuery = null;

		private void Page_Load(object sender, System.EventArgs e)
		{
			
			// Put user code to initialize the page here
			//utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings,Page.Session);
			strAppID = utility.GetAppID();
			strEnterpriseID = utility.GetEnterpriseID();
			strUserId = utility.GetUserID();  
			strFORMID = (String)Session["FORMID"]; 
			
			if (!IsPostBack)
			{
				LoadExportFormatList();				
				ViewState["ShowGroupTree"]=bShowGroupTree;
			}
			else
			{	
				bShowGroupTree=(bool)ViewState["ShowGroupTree"];
			}
			SetReportInstance();
			BindReport();
			
			if(ViewState["maxPage"] == null)
			{
				rptViewer.ShowLastPage();
				rptViewer.ShowFirstPage();
			}
			////// comment by Tumz. //////

			//Used to retrieve report objects and save them to a text file
			//saveReportObjects(strFORMID);

			/*if(!utility.GetUserCulture().ToUpper().Equals("EN-US"))
			{
				localizeReportObjects();
			}*/


			//			PageMargins pgMargin;
			//			pgMargin.leftMargin=1;
			//			pgMargin.rightMargin=1;
			//			pgMargin.bottomMargin=1;
			//			pgMargin.topMargin=1;
			//			rptSource.PrintOptions.ApplyPageMargins(pgMargin);
			

			//DisplayGroupTree switches back to the previous mode, if written before :: GetLastPageNumber(..)
			rptViewer.DisplayGroupTree = bShowGroupTree;

		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			base.OnInit(e);
			InitializeComponent();
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.rptViewer.Navigate += new CrystalDecisions.Web.NavigateEventHandler(this.rptViewer_Navigate);
			this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
			this.ddbZoom.SelectedIndexChanged += new System.EventHandler(this.btnZoom_Click);
			this.btnShowGrpTree.Click += new System.EventHandler(this.btnShowGrpTree_Click);
			this.btnMoveLast.Click += new System.EventHandler(this.btnMoveLast_Click);
			this.btnMoveNext.Click += new System.EventHandler(this.btnMoveNext_Click);
			this.btnExport.Click += new System.EventHandler(this.btnExport_Click);
			this.btnMoveFirst.Click += new System.EventHandler(this.btnMoveFirst_Click);
			this.btnMovePrevious.Click += new System.EventHandler(this.btnMovePrevious_Click);
			this.ddbExport.SelectedIndexChanged += new System.EventHandler(this.ddbExport_SelectedIndexChanged);
			this.Load += new System.EventHandler(this.Page_Load);
			this.Unload += new System.EventHandler(this.Page_Unload);
		}
		#endregion

		private void Page_Unload(object sender, System.EventArgs e)
		{
			CloseReports(rptSource);
			rptViewer.Dispose();
			rptViewer = null;
		}

		private void CloseReports(ReportDocument reportDocument)  
		{    
			Sections sections = reportDocument.ReportDefinition.Sections;
			foreach (Section section in sections)
			{
				ReportObjects reportObjects = section.ReportObjects;
				foreach (ReportObject reportObject in reportObjects)
				{
					if (reportObject.Kind == ReportObjectKind.SubreportObject)
					{
						SubreportObject subreportObject = (SubreportObject)reportObject;
						ReportDocument subReportDocument = subreportObject.OpenSubreport(subreportObject.SubreportName);
						subReportDocument.Close();
					}
				}
			}
			reportDocument.Close();
		}

		private void SetReportInstance()
		{
			ViewState["ReportName"]=null;
			if(strFORMID==null)
			{
				//SetLogonInfo();
				return;
			}
			//create your own report objects as a class
			if (strFORMID.IndexOf("InvoicePrintingQuery")>=0)
			{
				// declare invoice printing report object to rptSource class
				rptSource = new InvoicePrinting();

				//get parameter dataset from session_ds1
				m_dsQuery = (DataSet)Session["SESSION_DS1"];

				//pass the parameters to report
				SetInvoiceParams();
			}
			else if (strFORMID.IndexOf("BatchManifestControlPanel_Delivery")>=0)
			{			
				string strDeliveryType= Request.QueryString["Batch_type"].ToString();
				if(strDeliveryType=="S" || strDeliveryType=="W")
				{
					rptSource = new DeliveryManifestReport_MF();
					ViewState["ReportName"]="DeliveryManifestReport_MF";
				}
				else if(strDeliveryType=="L" || strDeliveryType=="A")
				{
					rptSource = new DeliveryManifestReportLH_Draft();
					ViewState["ReportName"]="DeliveryManifestReportLH_Draft";
				}
				SetDeliveryParams();
			}
			else if (strFORMID.IndexOf("ShipmentExceptionReport")>=0)
			{				
				rptSource = new ShipmentException();
				m_dsQuery = (DataSet)Session["SESSION_DS1"];								
				SetShipExcepParams();
			}
			else if (strFORMID.IndexOf("ShipmentException3PA")>=0)
			{
				rptSource = new ShipmentExceptionRpt3PA();
				m_dsQuery = (DataSet)Session["SESSION_DS1"];								
				SetShipExcepParams();
			}
				//Added by Tom 11/6/09
			else if (strFORMID == "DeliveryManifestList")
			{					
				rptSource = new DeliveryManifestReport();
				rptSource.PrintOptions.PaperSize = PaperSize.PaperA4;
				m_dsQuery = (DataSet)Session["SESSION_DS1"];									
				SetDeliveryManifestParams();
				//Added By Tom 17/12/2009
				rptViewer.Width = 1200;
				//End Added By Tom 17/12/2009
			}
			else if (strFORMID == "DeliveryManifestListLH")
			{					
				rptSource = new DeliveryManifestReportLH();
				rptSource.PrintOptions.PaperSize = PaperSize.PaperA4;
				m_dsQuery = (DataSet)Session["SESSION_DS1"];									
				SetDeliveryManifestParams();
			}
				//End added by Tom 11/6/09
			else if(strFORMID.IndexOf("CreditTracking")>=0 )
			{
				rptSource= new CreditNoteReport();
				SetCreditNoteParams();
			}
			else if(strFORMID.IndexOf("DebitTracking")>=0 )
			{
				rptSource= new  DebitNoteReport();
				SetDebitNoteParams();
			}
			else if (strFORMID == "SERVICEFAILURE")
			{
				m_dsQuery = (DataSet)Session["SESSION_DS1"];
				rptSource = new ServiceFailureRpt();
				//rptSource.PrintOptions.PaperSize = PaperSize.PaperA4;
				rptViewer.Width = 1200;
				SetServiceFailureParams();
			}
				//			else if (strFORMID == "ShipmentTrackingQueryRpt")
				//			{
				//				rptSource = new ShipmentTrackingReport();
				//				m_dsQuery = (DataSet)Session["SESSION_DS1"];
				//				SetShipTrackParams();
				//				rptViewer.DisplayGroupTree = false;
				//			}
			else if (strFORMID == "SalesTerritoryReportQuery")
				//			{
				//				rptSource = new SaleTerritoryReport();
				//				m_dsQuery = (DataSet)Session["SESSION_DS1"];
				//				SetRevenueCostParams();
				//			}
				// by sittichai  23/01/2008
			{
				m_dsQuery = (DataSet)Session["SESSION_DS1"];

				if((m_dsQuery != null) && 
					(!m_dsQuery.GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					if((m_dsQuery.Tables[0].Rows[0]["RptType"] != null) && 
						(!m_dsQuery.Tables[0].Rows[0]["RptType"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
						(m_dsQuery.Tables[0].Rows[0]["RptType"].ToString() != ""))
					{
						if(m_dsQuery.Tables[0].Rows[0]["RptType"].ToString() == "C")
						{
							rptSource =  new SaleTerritoryReport3PA();
						}
						else
						{
							rptSource =  new SaleTerritoryReport();
						}

						SetRevenueCostParams();
					}
				}
			}
			else if (strFORMID == "SalesTerritoryReportQueryRev")
				//			{
				//				rptSource = new SaleTerritoryReportRev(); 
				//				m_dsQuery = (DataSet)Session["SESSION_DS1"];
				//				SetRevenueCostParams();
				//			}
				//by sittichai  23/01/2008
			{
				m_dsQuery = (DataSet)Session["SESSION_DS1"];

				if((m_dsQuery != null) && 
					(!m_dsQuery.GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					if((m_dsQuery.Tables[0].Rows[0]["RptType"] != null) && 
						(!m_dsQuery.Tables[0].Rows[0]["RptType"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
						(m_dsQuery.Tables[0].Rows[0]["RptType"].ToString() != ""))
					{
						if(m_dsQuery.Tables[0].Rows[0]["RptType"].ToString() == "C")
						{
							rptSource =  new SaleTerritoryReportRev3PA();
						}
						else
						{
							rptSource =  new SaleTerritoryReportRev();
						}

						SetRevenueCostParams();
					}
				}
			}
			else if (strFORMID == "ServiceQualityIndicatorQuery")
			{
				rptSource = new ServiceQualityIndicatorReport();
				m_dsQuery = (DataSet)Session["SESSION_DS1"];
				SetSQIParams();
			}

		//Jeab 13 Jun 2011			
			else if (strFORMID == "SQI_PODEX_All" )
			{
				rptSource = new SQI_PODEX_All();
				m_dsQuery = (DataSet)Session["SESSION_DS1"];
				SetSQI_PODEXParams();
			}
		//Jeab 13 Jun 2011  =========> End

		//Jeab 17 Jun 2011			
			else if (strFORMID == "RevenueBySalesID" )
			{
				rptSource = new RevnBySalesID();
				m_dsQuery = (DataSet)Session["SESSION_DS1"];
				SetRevnBySalesIDParams();
			}
		//Jeab 17 Jun 2011  =========> End

		//Jeab 23 Jun 2011			
			else if (strFORMID == "RevenueBySalesIDSummary" )
			{
				rptSource = new RevnBySalesID_Summary();
				m_dsQuery = (DataSet)Session["SESSION_DS1"];
				SetRevnBySalesIDParams();
			}
		//Jeab 23 Jun 2011  =========> End

		//Jeab 06 Jul 2011			
			else if (strFORMID == "DailyKFACTIONLIST" )
			{
				rptSource = new DailyKF_PODEX();
				m_dsQuery = (DataSet)Session["SESSION_DS1"];
				SetDailyKFPODEXParam();
			}
		//Jeab 06 Jul 2011  =========> End

				// by sittichai  23/01/2008
			else if (strFORMID == "AverageDeliveryTimeReportQuery")
				//			{
				//				rptSource = new AverageDeliveryTimeReport();
				//				m_dsQuery = (DataSet)Session["SESSION_DS1"];
				//				SetAverageDeliveryTimeParams();
			{
				m_dsQuery = (DataSet)Session["SESSION_DS1"];

				if((m_dsQuery != null) && 
					(!m_dsQuery.GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					if((m_dsQuery.Tables[0].Rows[0]["RptType"] != null) && 
						(!m_dsQuery.Tables[0].Rows[0]["RptType"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
						(m_dsQuery.Tables[0].Rows[0]["RptType"].ToString() != ""))
					{
						if(m_dsQuery.Tables[0].Rows[0]["RptType"].ToString() == "C")
						{
							rptSource =  new AverageDeliveryTimeReport3PA(); 
						}
						else
						{
							rptSource =  new AverageDeliveryTimeReport();
						}

						SetAverageDeliveryTimeParams();
					}
				}
			}
				// by Ching Dec 3,2007
			else if (strFORMID == "ShipmentPendingCOD")
			{
				rptSource = new ShipmentPendingCODRpt();
				m_dsQuery = (DataSet)Session["SESSION_DS1"];
				SetShipmentPendingCODParams();
				rptViewer.Width = 1200; 
			}
			else if (strFORMID == "CODAmountCollectedReport")
			{
				rptSource = new  CODAmountCollected(); 
				m_dsQuery = (DataSet)Session["SESSION_DS1"];
				SetCODAmountCollectedReportParams();
				rptViewer.Width = 1200; 
			}
			else if (strFORMID == "CODRemittanceReport")
			{
				rptSource = new  CODRemittance();
				m_dsQuery = (DataSet)Session["SESSION_DS1"];
				SetCODRemittanceReportParams();
				rptViewer.Width = 1200; 
			}

			////// Tumz. ////////////
			else if (strFORMID == "ShipmentPackageReplace")
			{
				rptSource = new ShipmentPackageReplacedRpt();
				m_dsQuery = (DataSet)Session["SESSION_DS1"];
				SetShipmentPackageReplaceReportParams();
				rptViewer.Width = 1200;
			}

				//by sittichai 27/03/2008
			else if (strFORMID == "InvoiceHeaderReportQuery")
			{
				rptSource = new InvoicePrintingHeader();
				m_dsQuery = (DataSet)Session["SESSION_DS1"];
				SetInvoiceHeaderReportParams();
				rptViewer.Width = 1200;
				rptSource.PrintOptions.PaperSize = PaperSize.PaperA4;
			}

			else if (strFORMID == "InvoiceDetailReportQuery")
			{
				rptSource = new InvoicePrintingDetail();
				rptSource.PrintOptions.PaperSize = PaperSize.PaperLegal;
				rptSource.PrintOptions.PaperOrientation = PaperOrientation.Landscape;
				m_dsQuery = (DataSet)Session["SESSION_DS1"];
				SetInvoiceDetailReportParams();
				rptViewer.Width = 1800;
				rptViewer.ToolTip="InvoicePrintingDetail";

			}
			else if (strFORMID == "InvoiceDetailPkg")
			{
				rptSource = new InvoicePrintingDetailPKG();
				rptSource.PrintOptions.PaperSize = PaperSize.PaperLegal;
				rptSource.PrintOptions.PaperOrientation = PaperOrientation.Landscape;
				m_dsQuery = (DataSet)Session["SESSION_DS1"];
				SetInvoiceDetailReportParams();
				rptViewer.Width = 1800;
				rptViewer.ToolTip="InvoiceDetailPkg";

			}

				//End
				//HC Return Task
			else if (strFORMID == "PENDING HC REPORT")
			{
				m_dsQuery = (DataSet)Session["SESSION_DS1"];

				if((m_dsQuery != null) && 
					(!m_dsQuery.GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					if((m_dsQuery.Tables[0].Rows[0]["RptType"] != null) && 
						(!m_dsQuery.Tables[0].Rows[0]["RptType"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
						(m_dsQuery.Tables[0].Rows[0]["RptType"].ToString() != ""))
					{
						if(m_dsQuery.Tables[0].Rows[0]["RptType"].ToString() == "H")
						{
							rptSource = new PendingHCReturnReportHub();
						}
						else
						{
							rptSource = new PendingHCReturnReportCustomer();
						}
						SetPendingHCReportParams();
					}
				}
			}
				//HC Return Task
				//HC Return Task
			else if (strFORMID == "SERVICE EXCEPTION HC REPORT")
			{
				m_dsQuery = (DataSet)Session["SESSION_DS1"];

				if((m_dsQuery != null) && 
					(!m_dsQuery.GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					if((m_dsQuery.Tables[0].Rows[0]["RptType"] != null) && 
						(!m_dsQuery.Tables[0].Rows[0]["RptType"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
						(m_dsQuery.Tables[0].Rows[0]["RptType"].ToString() != ""))
					{
						if(m_dsQuery.Tables[0].Rows[0]["RptType"].ToString() == "H")
						{
							rptSource = new HCReturnServiceExceptionReportHub();
						}
						else
						{
							rptSource = new HCReturnServiceExceptionReportCustomer();
						}
						SetHCReturnServiceExceptionParams();
					}
				}
			}
				//HC Return Task
			else if(strFORMID=="DISPATCH TRACKING")
			{
				rptSource =  new DispatchTrackingRpt();
				m_dsQuery = (DataSet)Session["SESSION_DS1"];
				SetDispatchTracking();
				rptViewer.DisplayGroupTree = false;
				rptViewer.Width = 1600;
			}
		//Jeab 04 Jan 11
			else if(strFORMID=="DISPATCH TRACKING_W")
			{
				rptSource =  new  DispatchTrackingRpt_W();
				m_dsQuery = (DataSet)Session["SESSION_DS1"];
				SetDispatchTracking();
				rptViewer.DisplayGroupTree = false;
				rptViewer.Width = 1600;
			}
		//Jeab 04 Jan 11 =========> End
			else  if(strFORMID=="SHIPMENT PENDING REPORT")
			{
				m_dsQuery = (DataSet)Session["SESSION_DS1"];

				if((m_dsQuery != null) && 
					(!m_dsQuery.GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					if((m_dsQuery.Tables[0].Rows[0]["RptType"] != null) && 
						(!m_dsQuery.Tables[0].Rows[0]["RptType"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
						(m_dsQuery.Tables[0].Rows[0]["RptType"].ToString() != ""))
					{
						if(m_dsQuery.Tables[0].Rows[0]["RptType"].ToString() == "C")
						{
							rptSource =  new ShipmentPendingPODRpt3PA(); 
						}
						else
						{
							rptSource =  new ShipmentPendingPODRpt();
						}
					}
				}
				SetShipPendingPOD();
				rptViewer.Width = 1200; 
			}

			else  if(strFORMID=="SHIPMENT PENDING FX REPORT")
			{
				m_dsQuery = (DataSet)Session["SESSION_DS1"];

				if((m_dsQuery != null) && 
					(!m_dsQuery.GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					if((m_dsQuery.Tables[0].Rows[0]["RptType"] != null) && 
						(!m_dsQuery.Tables[0].Rows[0]["RptType"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
						(m_dsQuery.Tables[0].Rows[0]["RptType"].ToString() != ""))
					{
						rptSource =  new PendingFXReport(); 
					}
				}
				SetShipPendingPOD();
				rptViewer.Width = 1200; 
			}

			else if(strFORMID == "ShipmentTrackingQueryRpt")
			{
				m_dsQuery = (DataSet)Session["SESSION_DS1"];

				if((m_dsQuery != null) && (!m_dsQuery.GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					if((m_dsQuery.Tables[0].Rows[0]["RptType"] != null) && 
						(!m_dsQuery.Tables[0].Rows[0]["RptType"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
						(m_dsQuery.Tables[0].Rows[0]["RptType"].ToString() != ""))
					{
						if(m_dsQuery.Tables[0].Rows[0]["RptType"].ToString() == "C")
						{
							rptSource =  new ShipmentTrackingReport3PA(); 
						}
						else
						{
							rptSource =  new ShipmentTrackingReport();
						}
					}
				}
				SetShipTrackParams();
				rptViewer.Width = 1500; 
			}
				//Added By Tom 30/10/09
			else if(strFORMID == "ShipmentTrackingQueryRptAll")
			{
				m_dsQuery = (DataSet)Session["SESSION_DS1"];

				if((m_dsQuery != null) && (!m_dsQuery.GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					if((m_dsQuery.Tables[0].Rows[0]["RptType"] != null) && 
						(!m_dsQuery.Tables[0].Rows[0]["RptType"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
						(m_dsQuery.Tables[0].Rows[0]["RptType"].ToString() != ""))
					{
						if(m_dsQuery.Tables[0].Rows[0]["RptType"].ToString() == "C")
						{
							rptSource =  new ShipmentTrackingReport3PA(); 
						}
						else
						{
//							rptSource =  new ShipmentTrackingReportAll();
							rptSource= new ShipmentTrackingReportAll_DT();  //Jeab 14 Dec 10
						}
					}
				}
				SetShipTrackParams();
				rptViewer.Width = 1500; 
			}
				//End Added By Tom 30/10/09

		//Jeab 13 Dec 10
			else if(strFORMID == "ShipmentTrackingQueryRpt_NoDT")
			{
				m_dsQuery = (DataSet)Session["SESSION_DS1"];

				if((m_dsQuery != null) && (!m_dsQuery.GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					if((m_dsQuery.Tables[0].Rows[0]["RptType"] != null) && 
						(!m_dsQuery.Tables[0].Rows[0]["RptType"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
						(m_dsQuery.Tables[0].Rows[0]["RptType"].ToString() != ""))
					{
						if(m_dsQuery.Tables[0].Rows[0]["RptType"].ToString() == "C")
						{
							rptSource =  new ShipmentTrackingReport3PA(); 
						}
						else
						{
							rptSource =  new ShipmentTrackingReport_NoDT();
						}
					}
				}
				SetShipTrackParams();
				rptViewer.Width = 1500; 
			}

			else if(strFORMID == "ShipmentTrackingQueryRptAll_NoDT")
			{
				m_dsQuery = (DataSet)Session["SESSION_DS1"];

				if((m_dsQuery != null) && (!m_dsQuery.GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					if((m_dsQuery.Tables[0].Rows[0]["RptType"] != null) && 
						(!m_dsQuery.Tables[0].Rows[0]["RptType"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
						(m_dsQuery.Tables[0].Rows[0]["RptType"].ToString() != ""))
					{
						if(m_dsQuery.Tables[0].Rows[0]["RptType"].ToString() == "C")
						{
							rptSource =  new ShipmentTrackingReport3PA(); 
						}
						else
						{
							rptSource =  new ShipmentTrackingReportAll_NoDT();
						}
					}
				}
				SetShipTrackParams();
				rptViewer.Width = 1500; 
			}
	//Jeab 13 Dec 10  ========> End

			else if(strFORMID == "ShipmentNotInvoiceRpt")
			{
				m_dsQuery = (DataSet)Session["SESSION_DS1"];

				if((m_dsQuery != null) && (!m_dsQuery.GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					if((m_dsQuery.Tables[0].Rows[0]["RptType"] != null) && 
						(!m_dsQuery.Tables[0].Rows[0]["RptType"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
						(m_dsQuery.Tables[0].Rows[0]["RptType"].ToString() != ""))
					{
						rptSource =  new ShipmentNotInvoiced(); 
					}
				}
				SetShipmentNotInvoiceParams();
				rptViewer.Width = 1500; 
			}
			else if(strFORMID == "CustomerLastShipmentDate")
			{
				m_dsQuery = (DataSet)Session["SESSION_DS1"];

				if((m_dsQuery != null) && (!m_dsQuery.GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					if((m_dsQuery.Tables[0].Rows[0]["RptType"] != null) && 
						(!m_dsQuery.Tables[0].Rows[0]["RptType"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
						(m_dsQuery.Tables[0].Rows[0]["RptType"].ToString() != ""))
					{
						rptSource =  new CustomerLastShipmentDateRpt(); 
					}
				}
				CustomerLastShipmentDateParams();
				rptViewer.Width = 1500; 
			}
			else if (strFORMID == "PODReportQuery")
			{
				m_dsQuery = (DataSet)Session["SESSION_DS1"];

				if((m_dsQuery != null) && (!m_dsQuery.GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					if((m_dsQuery.Tables[0].Rows[0]["RptType"] != null) && 
						(!m_dsQuery.Tables[0].Rows[0]["RptType"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
						(m_dsQuery.Tables[0].Rows[0]["RptType"].ToString() != ""))
					{
						if(m_dsQuery.Tables[0].Rows[0]["RptType"].ToString() == "C")
						{
							rptSource =  new PODReport3PA(); 
						}
						else
						{
							rptSource =  new PODReport();
						}
					}
				}
				SetPODParams();
				rptViewer.Width = 1200;
			}
			else if (strFORMID == "TripReport")
			{
				m_dsQuery = (DataSet)Session["SESSION_DS1"];

				if((m_dsQuery != null) && (!m_dsQuery.GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					if(m_dsQuery.Tables[0].Rows[0]["report_type"].ToString() == "TripReport")
					{
						rptSource =  new TIES.WebUI.TripReport();
					}
					else
					{
						rptSource =  new SummaryTripReport();					}
				}
				SetTripReportParams();
				rptViewer.Width = 1200;
			}
			else if (strFORMID == "FXReportQuery")
			{
				m_dsQuery = (DataSet)Session["SESSION_DS1"];

				if((m_dsQuery != null) && (!m_dsQuery.GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					if((m_dsQuery.Tables[0].Rows[0]["RptType"] != null) && 
						(!m_dsQuery.Tables[0].Rows[0]["RptType"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
						(m_dsQuery.Tables[0].Rows[0]["RptType"].ToString() != ""))
					{
						//						if(m_dsQuery.Tables[0].Rows[0]["RptOpt"].ToString().Trim()=="POD")
						//						{
						rptSource =  new PODFXReport(); 
						//						}
						//						else if(m_dsQuery.Tables[0].Rows[0]["RptOpt"].ToString().Trim()=="Pending")
						//						{
						//							//rptSource =  new PendingFXReport();
						//						}
					}
				}
				SetFXParams();
				rptViewer.Width = 1200;
			}
			else if(strFORMID=="REVENUE BY STATE")
			{
				rptSource = new RevenueByStateRpt();
				m_dsQuery = (DataSet)Session["SESSION_DS1"];
				SetRevenueByState();
			}
			else if(strFORMID=="MarginAnalysisReport")
			{
				rptSource = new MarginAnalysisRpt();
				m_dsQuery = (DataSet)Session["SESSION_DS1"];
				SetMarginAnalysis();
				rptViewer.DisplayGroupTree = false;
			}
			else if(strFORMID == "REVENUE BY CUST/AGENT")
			{
				rptSource = new  RevenueByCustAgentRpt(); 
				m_dsQuery = (DataSet)Session["SESSION_DS1"];
				SetRevenueByCustAgent();
				rptViewer.DisplayGroupTree = false;
			}
			else if(strFORMID == "STATUS EXCEP REPORT")
			{
				rptSource = new  StatusExceptionRpt(); 
				m_dsQuery = (DataSet)Session["SESSION_DS1"];
				SetStatusException();
				rptViewer.DisplayGroupTree = false;
			}
			else if(strFORMID == "CREDIT AGING REPORT")
			{
				int reportType = (int)Session["REPORTTYPE"]; 
				if(reportType == 1)
					rptSource = new CreditAgingSummaryRPT()	;
				else
					rptSource = new CreditAgingDetailRPT();
				m_dsQuery = (DataSet)Session["SESSION_DS1"];
				SetReportTypeAndDueDate();
			}
			else if(strFORMID == "COMMODITY REPORT")
			{
				rptSource = new CommodityRpt(); 
				m_dsQuery = (DataSet)Session["SESSION_DS1"];
				SetCommodity();				
			}
			else if(strFORMID == "INDUSTRIAL SECTOR REPORT")
			{
				rptSource = new IndustrialSectorRpt(); 
				m_dsQuery = (DataSet)Session["SESSION_DS1"];
				SetIndusSector();				
			}
			else if(strFORMID == "VAS REPORT")
			{
				rptSource = new VasRpt(); 
				m_dsQuery = (DataSet)Session["SESSION_DS1"];
				SetVas();
			}
			else if(strFORMID == "SERVICE REPORT")
			{
				rptSource = new ServiceRpt(); 
				m_dsQuery = (DataSet)Session["SESSION_DS1"];
				SetService();
			}
		//// boon 2011-09-14 -- start
			else if(strFORMID == "STAFF REPORT")
			{
				rptSource = new StaffRpt(); 
				m_dsQuery = (DataSet)Session["SESSION_DS1"];
				SetStaff();
			}
			else if(strFORMID == "VEHICLE REPORT")
			{
				rptSource = new VehicleRpt(); 
				m_dsQuery = (DataSet)Session["SESSION_DS1"];
				SetVehicle();
			}
		//// boon 2011-09-14 -- end
			else if(strFORMID == "BASE ZONE RATES REPORT")
			{
				rptSource = new BaseZoneRatesRpt(); 
				m_dsQuery = (DataSet)Session["SESSION_DS1"];
				SetBaseZoneRates();
			}
			else if(strFORMID == "ZIPCODE STATE REPORT")
			{
				rptSource = new ZipCodeStateRpt(); 
				m_dsQuery = (DataSet)Session["SESSION_DS1"];
				SetZipCodeState();
				rptViewer.DisplayGroupTree = false;
			}
			else if(strFORMID == "CUSTOMER PROFILE REPORT")
			{
				rptSource = new CustomerProfileRpt(); 
				m_dsQuery = (DataSet)Session["SESSION_DS1"];
				SetCustomerProfile();
				rptViewer.DisplayGroupTree = false;
			}
			else if(strFORMID == "AGENT PROFILE REPORT")
			{
				rptSource = new AgentProfileRpt (); 
				m_dsQuery = (DataSet)Session["SESSION_DS1"];
				SetAgentProfile();
				rptViewer.DisplayGroupTree = false;
			}
			else if(strFORMID == "AvgDeliveryTimeReport")
			{
				rptSource = new AvgDeliveryTimeRpt(); 
				m_dsQuery = (DataSet)Session["SESSION_DS1"];
				SetAvgDlvyTimeParams();
				rptViewer.DisplayGroupTree = false;
			}
				//PSA# 2009-014
			else if (strFORMID == "TotalAverageDeliveryTimeReportQuery")
			{
				m_dsQuery = (DataSet)Session["SESSION_DS1"];

				if((m_dsQuery != null) && 
					(!m_dsQuery.GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					if((m_dsQuery.Tables[0].Rows[0]["RptType"] != null) && 
						(!m_dsQuery.Tables[0].Rows[0]["RptType"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
						(m_dsQuery.Tables[0].Rows[0]["RptType"].ToString() != ""))
					{
						if(m_dsQuery.Tables[0].Rows[0]["RptType"].ToString() == "C")
						{
							rptSource =  new TotalAverageDeliveryTimeReport(); 
						}
						else
						{
							rptSource =  new TotalAverageDeliveryTimeReport();
						}

						SetTotalAverageDeliveryTimeParams();
					}
				}
			}
				//PSA# 2009-014
			else if(strFORMID == "VERSUS REPORT")
			{
				rptSource = new VersusReportRpt();
				m_dsQuery = (DataSet)Session["SESSION_DS1"];
				SetVersusReportParams();
				rptViewer.DisplayGroupTree = false;
			}
			else if(strFORMID == "INVOICE RETURN PENDING")
			{
				rptSource = new InvoiceReturnPendingRpt();
				m_dsQuery = (DataSet)Session["SESSION_DS1"];
				SetInvoiceReturnServiceReportParams();
				rptViewer.DisplayGroupTree = false;
			}
			else if(strFORMID == "INVOICE RETURN EXCEPTION")
			{
				rptSource = new InvoiceReturnExceptionRpt();
				m_dsQuery = (DataSet)Session["SESSION_DS1"];
				SetInvoiceReturnServiceReportParams();
				rptViewer.DisplayGroupTree = false;
			}
			else if(strFORMID == "ShipmentPendingCODVerifyReport")
			{
				rptSource = new ShipmentPendingCODVerifyReport();
				m_dsQuery = (DataSet)Session["SESSION_DS1"];
				SetShipmentPendingCODVerifyReportParams();
				rptViewer.Width = 1200; 
			}
			else if(strFORMID == "ShipmentExportRpt")
			{
				rptSource = new ShipmentRpt();
				m_dsQuery = (DataSet)Session["SESSION_DS1"];
				SetShipmentExportRptReportParams();
				rptViewer.Width = 1800; 
			}
			else if(strFORMID == "UtilMonitorConsignment")
			{
				rptSource = new UtilMonitorConsignmentReport();
				m_dsQuery = (DataSet)Session["SESSION_DS1"];
				SetUtilMonitorConsignmentReportParams();
				rptViewer.DisplayGroupTree = false;
				rptViewer.Width =1200;
			}//ADD BY X JUL 31 08
			else if (strFORMID == "SalesTerritoryReportQueryTotal")
			{
				m_dsQuery = (DataSet)Session["SESSION_DS1"];
 
				if((m_dsQuery != null) && 
					(!m_dsQuery.GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					if((m_dsQuery.Tables[0].Rows[0]["RptType"] != null) && 
						(!m_dsQuery.Tables[0].Rows[0]["RptType"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
						(m_dsQuery.Tables[0].Rows[0]["RptType"].ToString() != ""))
					{
						rptSource =  new SaleTerritoryReport_Total();
						SetRevenueCostParams();
					}
				}
			}
			else if (strFORMID == "SalesTerritoryReportQueryRevTotal")
			{
				m_dsQuery = (DataSet)Session["SESSION_DS1"];
 
				if((m_dsQuery != null) && 
					(!m_dsQuery.GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					if((m_dsQuery.Tables[0].Rows[0]["RptType"] != null) && 
						(!m_dsQuery.Tables[0].Rows[0]["RptType"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
						(m_dsQuery.Tables[0].Rows[0]["RptType"].ToString() != ""))
					{
						rptSource =  new SaleTerritoryReportRev_Total();
						SetRevenueCostParams();
					}
				}
				
			}//END ADD BY X JUL 31 08
				//Added By Tom May 14,2010
			else if (strFORMID == "ShipmentNotRetrieve")
			{
				m_dsQuery = (DataSet)Session["SESSION_DS1"];
 
				if((m_dsQuery != null) && 
					(!m_dsQuery.GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					rptSource =  new ShptNotRetrieve();
					SetShipmentNotRetrieveParams();
				}
			}
				//End Added By Tom May 14,2010
				//Added By Tom May 26,2010
			else if (strFORMID == "ShipmentNotRetrieveDIM")
			{
				m_dsQuery = (DataSet)Session["SESSION_DS1"];
 
				if((m_dsQuery != null) && 
					(!m_dsQuery.GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					rptSource =  new ShptNotRetrieveDIM();
					SetShipmentNotRetrieveParams();
				}
			}
				//End Added By Tom May 26,2010
				//Add By TOM
				//Added By Tom Jun 7,2010
			else if (strFORMID == "VersusReportQuery")
			{
				m_dsQuery = (DataSet)Session["SESSION_DS1"];
 
				if((m_dsQuery != null) && 
					(!m_dsQuery.GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					rptSource =  new NoMDErpt();
					SetShipmentNotRetrieveParamsNoMDE();
				}
			}
				//End Added By Tom Jun 7,2010
				//Add By TOM
			else if (strFORMID == "RevenueReportSale")
			{
				m_dsQuery = (DataSet)Session["SESSION_DS1"];
 
				if((m_dsQuery != null) && 
					(!m_dsQuery.GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					if((m_dsQuery.Tables[0].Rows[0]["RptType"] != null) && 
						(!m_dsQuery.Tables[0].Rows[0]["RptType"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
						(m_dsQuery.Tables[0].Rows[0]["RptType"].ToString() != ""))
					{
						rptSource =  new RevenueReportSale();
						SetRevenueCostParams();
					}
				}
			}
			else if (strFORMID == "RevenueReport")
			{
				m_dsQuery = (DataSet)Session["SESSION_DS1"];
 
				if((m_dsQuery != null) && 
					(!m_dsQuery.GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					if((m_dsQuery.Tables[0].Rows[0]["RptType"] != null) && 
						(!m_dsQuery.Tables[0].Rows[0]["RptType"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
						(m_dsQuery.Tables[0].Rows[0]["RptType"].ToString() != ""))
					{
						rptSource =  new RevenueReportAll();
						SetRevenueCostParams();
					}
				}
				
			}
				//END ADD BY TOM
				//Add By TOM
			else if (strFORMID == "WeightVolumeSale")
			{
				m_dsQuery = (DataSet)Session["SESSION_DS1"];
 
				if((m_dsQuery != null) && 
					(!m_dsQuery.GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					if((m_dsQuery.Tables[0].Rows[0]["RptType"] != null) && 
						(!m_dsQuery.Tables[0].Rows[0]["RptType"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
						(m_dsQuery.Tables[0].Rows[0]["RptType"].ToString() != ""))
					{
						rptSource =  new WeightVolumeReport();
						SetRevenueCostParams();
					}
				}
			}
			else if (strFORMID == "WeightVolume")
			{
				m_dsQuery = (DataSet)Session["SESSION_DS1"];
 
				if((m_dsQuery != null) && 
					(!m_dsQuery.GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					if((m_dsQuery.Tables[0].Rows[0]["RptType"] != null) && 
						(!m_dsQuery.Tables[0].Rows[0]["RptType"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
						(m_dsQuery.Tables[0].Rows[0]["RptType"].ToString() != ""))
					{
						rptSource =  new WeightVolumeReportSale();
						SetRevenueCostParams();
					}
				}
				
			}
				//END ADD BY TOM
				//Added By Tom jun 11, 2010
			else if (strFORMID == "LHWeightVolume")
			{
				m_dsQuery = (DataSet)Session["SESSION_DS1"];
 
				if((m_dsQuery != null) && 
					(!m_dsQuery.GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					if((m_dsQuery.Tables[0].Rows[0]["RptType"] != null) && 
						(!m_dsQuery.Tables[0].Rows[0]["RptType"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
						(m_dsQuery.Tables[0].Rows[0]["RptType"].ToString() != ""))
					{
						rptSource =  new LHWeightVolume();
						SetLHWeightVolumeParam();
					}
				}
				
			}
			else if (strFORMID == "LHWeightVolumeBook")
			{
				m_dsQuery = (DataSet)Session["SESSION_DS1"];
 
				if((m_dsQuery != null) && 
					(!m_dsQuery.GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					if((m_dsQuery.Tables[0].Rows[0]["RptType"] != null) && 
						(!m_dsQuery.Tables[0].Rows[0]["RptType"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
						(m_dsQuery.Tables[0].Rows[0]["RptType"].ToString() != ""))
					{
						rptSource =  new LHweightVolumeBook();
						SetLHWeightVolumeParam();
					}
				}
				
			}
				//end Added By tom Jun 11, 2010
			//Added By Tom 27/7/09
			else if (strFORMID == "ConsigneeReport")
			{
				m_dsQuery = (DataSet)Session["SESSION_DS1"];
 
				if((m_dsQuery != null) && 
					(!m_dsQuery.GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					rptSource =  new ConsigneeRpt();
					SetConsigneeParams();
				}
				
			}
				//End Added By Tom 27/7/09
			else if(strFORMID == "CustChargeablewt") //ADD BY X DEC 22 08
			{

				m_dsQuery = (DataSet)Session["SESSION_DS1"];

				if((m_dsQuery != null) && (!m_dsQuery.GetType().Equals(System.Type.GetType("System.DBNull"))))

				{

					if((m_dsQuery.Tables[0].Rows[0]["RptType"] != null) 

						&&(!m_dsQuery.Tables[0].Rows[0]["RptType"].GetType().Equals(System.Type.GetType("System.DBNull"))) 

						&& (m_dsQuery.Tables[0].Rows[0]["RptType"].ToString() != ""))

					{

						rptSource =  new  CustomerChargeableWTRpt();

					}

				}

				CustChargeablewtParams();

				rptViewer.Width = 1800; 

			}//end add by X DEC 22 08

				//Start: PSA 2009-022 by Gwang; 05Mar09
			else if (strFORMID == "PrintBarcode")
			{
				rptSource = new PrintConsignmentNote();
				rptViewer.Width = 1200;
			}
				//End: PSA 2009-022 by Gwang; 05Mar09

				//Start: PSA 2009-020 by Gwang; 05Mar09
			else if (strFORMID == "PrePrintConsignmentPage1")
			{
				rptSource = new PrintConsignmentNote();
				SetPreprintConsignmentPage1Param();
				rptViewer.Width = 1200;
			}

			else if (strFORMID == "PrePrintConsignmentPage2")
			{
				rptSource = new PrintConsignmentNotePart2();
				SetPreprintConsignmentPage1Param2();
				rptViewer.Width = 1200;
			}
 
				//End: PSA 2009-020 by Gwang; 05Mar09
				//Added By Tom 12/10/2009
			else if (strFORMID == "FirstDelReport")
			{
				rptSource = new FirstDelRpt();
				m_dsQuery = (DataSet)Session["SESSION_DS1"];
				SetFirstDelParam();
				rptViewer.Width = 1200;
			}
				//End Added By Tom 12/10/2009
				//Added By Tom 12/10/2009
			else if (strFORMID == "FirstDelReportProv")
			{
				rptSource = new FirstDelRptProv();
				m_dsQuery = (DataSet)Session["SESSION_DS1"];
				SetFirstDelParam();
				rptViewer.Width = 1200;
			}
				//End Added By Tom 12/10/2009
				//Added By Tom 11/12/2009
			else if (strFORMID == "FirstDelDetailReport")
			{
				rptSource = new FirstDelDetailRpt();
				m_dsQuery = (DataSet)Session["SESSION_DS1"];
				SetFirstDelParam();
				rptViewer.Width = 1200;
			}
				//End Added By Tom 11/12/2009
			else if(strFORMID == "RtnToCustRpt")
			{
				rptSource = new RtntoCustRpt();
				m_dsQuery = (DataSet)Session["SESSION_DS1"];
				SetRtnCustParam();
				rptViewer.Width = 1200;
			}
			//add at 10/6/53
			else if (strFORMID == "ConsignmentNotePrint")
			{
				rptSource = new TIES.WebUI.ConsignmentNote.PreprintConsignment(); 
				//rptSource = new TIES.WebUI.ConsignmentNote.CrystalReport1();   
				SetPreprintConsParams();
				rptViewer.Width = 1200;
				rptSource.PrintOptions.PaperSize = PaperSize.PaperA4;
			}
			//add by Tumz.
			else if (strFORMID.IndexOf("DairyLodgments")>=0)
			{			
				m_dsQuery = (DataSet)Session["SESSION_DS1"];
				//string strDeliveryType= Request.QueryString["Batch_type"].ToString();
				
				rptSource = new TIES.WebUI.ConsignmentNote.DairyLodgments();
				ViewState["ReportName"]="DairyLodgments";
				SetDairyLodgmentsParams();
				rptViewer.Width = 1200;
				rptSource.PrintOptions.PaperSize = PaperSize.PaperA4;
			}
			//End added at 10/6/53
			else
				return;
					
			//dynamically set the connection details
			if (strFORMID == "InvoiceDetailReportQuery")
			{
				SetReportPublisherLogonInfo();
			}
			else
			{
				SetLogonInfo();
			}
		
		}
		private void SetDairyLodgmentsParams()
		{
			string strApplicationid= Request.QueryString["applicationid"].ToString();
			string strEnterpriseID= Request.QueryString["enterpriseID"].ToString();
			string strBatchDate= Request.QueryString["batchDate"].ToString();
			string strServiceType= Request.QueryString["serviceType"].ToString();
			string strLocation= Request.QueryString["location"].ToString();
			DateTime dt = new DateTime(); 
			string[] arrDate = strBatchDate.Split('-');
			if(arrDate.Length == 3)
			{
				dt = new DateTime(Convert.ToInt32(arrDate[2]),Convert.ToInt32(arrDate[1]), Convert.ToInt32(arrDate[0])); 
			}


			ParameterFieldDefinitions paramFldDefs ;
			ParameterValues paramVals = new ParameterValues() ;
			ParameterDiscreteValue paramDVal = new ParameterDiscreteValue();
			paramFldDefs = rptSource.DataDefinition.ParameterFields;

			foreach(ParameterFieldDefinition paramFldDef in paramFldDefs)
			{
				switch(paramFldDef.ParameterFieldName)
				{
					case "@applicationid":
						paramDVal.Value = strApplicationid;
						break;
					case "@enterpriseid":
						paramDVal.Value = strEnterpriseID;
						break;
					case "@BatchDateFrom":
						paramDVal.Value =  dt; 
						break;
					case "@BatchDateTO":
						paramDVal.Value =  dt; 
						break;
					case "@Location":
						paramDVal.Value = strLocation ; 
						break;
					case "@ServiceType":
						paramDVal.Value = null ; 
						break;
					default:
						continue;
				}
				paramVals = paramFldDef.CurrentValues;
				paramVals.Add(paramDVal);
				paramFldDef.ApplyCurrentValues(paramVals);
			}
		}
		//Added By Tom 12/10/2009
		private void SetFirstDelParam()
		{
			//declare instance for parameterfields class
			ParameterValues paramvals = new ParameterValues();
			ParameterFieldDefinitions paramFlds; 
			ParameterDiscreteValue paramDiscVal = new ParameterDiscreteValue();

			//getting parameter field values
			String strDateType="";
			DateTime dtStartDate, dtEndDate;
			String strPayerId="*"; 
			String strPayerType="";		//boon
			String strZipCode="*";
			String strStateCode="*";
			String strRouteCode = "*";
			String strDestinationDC = "*";
			
			DataRow dr = m_dsQuery.Tables[0].Rows[0];

			strDateType = dr["tran_date"].ToString();
			if (Utility.IsNotDBNull(dr["start_date"])) 
				dtStartDate = (DateTime) dr["start_date"];
			else
				dtStartDate = System.DateTime.ParseExact("01/01/1900", "dd/MM/yyyy", null);
			if (Utility.IsNotDBNull(dr["end_date"])) 
				dtEndDate = (DateTime) dr["end_date"];
			else
				dtEndDate = System.DateTime.ParseExact("01/01/1900", "dd/MM/yyyy", null);

			//strPayerType="";
			if (Utility.IsNotDBNull(dr["payer_type"]))		//boon
				strPayerType = dr["payer_type"].ToString();	//boon

			if (Utility.IsNotDBNull(dr["payer_code"]))
				strPayerId = dr["payer_code"].ToString();

			if (Utility.IsNotDBNull(dr["zip_code"]))
				strZipCode = dr["zip_code"].ToString();
			if (Utility.IsNotDBNull(dr["state_code"]))
				strStateCode = dr["state_code"].ToString();

			if (Utility.IsNotDBNull(dr["route_code"]))
				strRouteCode = dr["route_code"].ToString();
			if (Utility.IsNotDBNull(dr["destination_dc"]))
				strDestinationDC = dr["destination_dc"].ToString();

			//get all parameter fields
			paramFlds = rptSource.DataDefinition.ParameterFields;

			//passing parameter for parameter fields
			foreach(ParameterFieldDefinition paramFld in paramFlds)
			{
				switch(paramFld.ParameterFieldName)
				{
					case "ApplicationID":
						paramDiscVal.Value = strAppID;
						break;
					case "EnterpriseID":
						paramDiscVal.Value = strEnterpriseID;
						break;
					case "DateType":
						paramDiscVal.Value = strDateType;
						break;
					case "FromDate":
						paramDiscVal.Value = dtStartDate;
						break;
					case "ToDate":
						paramDiscVal.Value = dtEndDate;
						break;
					case "PayerType":						//boon
						paramDiscVal.Value = strPayerType;	//boon
						break;								//boon
					case "PayerCode":
						paramDiscVal.Value = strPayerId;
						break;
					case "PostalCode":
						paramDiscVal.Value = strZipCode;
						break;
					case "DestinationState":
						paramDiscVal.Value = strStateCode;
						break;
					case "RouteCode":
						paramDiscVal.Value = strRouteCode;
						break;
					case "DestinationDC":
						paramDiscVal.Value = strDestinationDC;
						break;
					case "prmUserID":
						paramDiscVal.Value = strUserId; 
						break;

						//case "Flag":
						//paramDiscVal.Value = strFlag;
						//break;
					default:
						continue;
				}
				paramvals = paramFld.CurrentValues;
				paramvals.Add(paramDiscVal);
				paramFld.ApplyCurrentValues(paramvals);
			}
		}
		//End Added By Tom 12/10/2009
		//Added By Tom 5/11/2009
		private void SetRtnCustParam()
		{
			//declare instance for parameterfields class
			ParameterValues paramvals = new ParameterValues();
			ParameterFieldDefinitions paramFlds; 
			ParameterDiscreteValue paramDiscVal = new ParameterDiscreteValue();
			ParameterRangeValue paramRangeVal = new ParameterRangeValue();

			//getting parameter field values
			String strDateType="";
			DateTime dtStartDate, dtEndDate;
			String strPayerId="*"; 
			String strStatusCode="*"; 
			String strCon=""; 
			
			DataRow dr = m_dsQuery.Tables[0].Rows[0];

			strDateType = dr["tran_date"].ToString();
			if (Utility.IsNotDBNull(dr["start_date"])) 
				dtStartDate = (DateTime) dr["start_date"];
			else
				dtStartDate = System.DateTime.ParseExact("01/01/1900", "dd/MM/yyyy", null);
			if (Utility.IsNotDBNull(dr["end_date"])) 
				dtEndDate = (DateTime) dr["end_date"];
			else
				dtEndDate = System.DateTime.ParseExact("01/01/1900", "dd/MM/yyyy", null);

			if (Utility.IsNotDBNull(dr["payer_code"]))
				strPayerId = dr["payer_code"].ToString();
			if (Utility.IsNotDBNull(dr["status_code"]))
				strStatusCode = dr["status_code"].ToString();
			if (Utility.IsNotDBNull(dr["consignment_no"]))
				strCon = dr["consignment_no"].ToString();

			//get all parameter fields
			paramFlds = rptSource.DataDefinition.ParameterFields;

			//passing parameter for parameter fields
			foreach(ParameterFieldDefinition paramFld in paramFlds)
			{
				switch(paramFld.ParameterFieldName)
				{
					case "p_appid":
						paramDiscVal.Value = strAppID;
						break;
					case "p_enterprise":
						paramDiscVal.Value = strEnterpriseID;
						break;
					case "Con":
						paramDiscVal.Value = strCon;
						break;
					case "p_payerid":
						paramDiscVal.Value = strPayerId;
						break;
					case "p_date_type":
						paramDiscVal.Value = strDateType;
						break;
					case "p_date_range":
						paramRangeVal.StartValue = dtStartDate;
						paramRangeVal.EndValue = dtEndDate;
						break;
					case "p_last_status_code":
						paramDiscVal.Value = strStatusCode;
						break;
					case "p_userid":
						paramDiscVal.Value = utility.GetUserID();
						break;

						//case "Flag":
						//paramDiscVal.Value = strFlag;
						//break;
					default:
						continue;
				}
				paramvals = paramFld.CurrentValues;
				if (paramFld.DiscreteOrRangeKind == DiscreteOrRangeKind.RangeValue)
					paramvals.Add(paramRangeVal);
				else 
					paramvals.Add(paramDiscVal);
				paramFld.ApplyCurrentValues(paramvals);
			}
		}
		//End Added By Tom 5/11/2009
		//Start: PSA 2009-020 by Gwang; 05Mar09
		private void SetPreprintConsignmentPage1Param2()
		{
			//declare instance for parameterfields class
			ParameterValues paramvals = new ParameterValues();
			ParameterFieldDefinitions paramFlds; 
			ParameterDiscreteValue paramDiscVal = new ParameterDiscreteValue();
			//get all parameter fields
			paramFlds = rptSource.DataDefinition.ParameterFields;
			//passing parameter for parameter fields
			foreach(ParameterFieldDefinition paramFld in paramFlds)
			{
				switch(paramFld.ParameterFieldName)
				{ 
					case "pUser":
						paramDiscVal.Value = strUserId; 
						break;
					default:
						continue;
				}
				paramvals = paramFld.CurrentValues;
				paramvals.Add(paramDiscVal);
				paramFld.ApplyCurrentValues(paramvals);
			}
		}

 

			private void SetPreprintConsignmentPage1Param()
			{
				//declare instance for parameterfields class
				ParameterValues paramvals = new ParameterValues();
				ParameterFieldDefinitions paramFlds; 
				ParameterDiscreteValue paramDiscVal = new ParameterDiscreteValue();
				paramFlds = rptSource.DataDefinition.ParameterFields;
				foreach(ParameterFieldDefinition paramFld in paramFlds)
				{
					switch(paramFld.ParameterFieldName)
					{ 
						case "ConsignmentParam":
							paramDiscVal.Value = Session["ConsignmentShow"].ToString().Trim(); 
							break;
						case "PayerID":
							paramDiscVal.Value = Session["PayerID"].ToString().Trim(); 
							break;
						case "pUser":
							paramDiscVal.Value = strUserId; 
							break;
						default:
							continue;
					}
					paramvals = paramFld.CurrentValues;
					paramvals.Add(paramDiscVal);
					paramFld.ApplyCurrentValues(paramvals);
				}
			}
		//End: PSA 2009-020 by Gwang; 05Mar09

		//Start Add at 10/06/53
		private void SetPreprintConsParams()
		{
			//declare instance for parameterfields class
			ParameterValues paramvals = new ParameterValues();
			ParameterFieldDefinitions paramFlds; 
			ParameterDiscreteValue paramDiscVal = new ParameterDiscreteValue();

			//get all parameter fields
			paramFlds = rptSource.DataDefinition.ParameterFields;
  
			//passing parameter for parameter fields
			foreach(ParameterFieldDefinition paramFld in paramFlds)
				{
					switch(paramFld.ParameterFieldName)
					{ 
						case "pUser":
//							paramDiscVal.Value = Session["PayerID"].ToString().Trim(); 
							paramDiscVal.Value = utility.GetUserID().ToUpper();
							break;
						default:
							continue;
					}
					paramvals = paramFld.CurrentValues;
					paramvals.Add(paramDiscVal);
					paramFld.ApplyCurrentValues(paramvals);
				}
		}
		//End Add at 10/6/53


		private void CustChargeablewtParams()

		{

			ParameterValues paramvals = new ParameterValues();

			ParameterFieldDefinitions paramFlds; 

			ParameterDiscreteValue paramDiscVal = new ParameterDiscreteValue();

			ParameterRangeValue paramRangeVal = new ParameterRangeValue();

			String strPayerId="*",strPayerType="";

			String strDateType="";              

			String strZipCode="*", strStateCode="*";

 

			long lBookingNo=0;

			DateTime dtStartDate, dtEndDate;

      

			DataRow dr = m_dsQuery.Tables[0].Rows[0];

			strDateType = dr["tran_date"].ToString();

			if (Utility.IsNotDBNull(dr["start_date"])) 

				dtStartDate = (DateTime) dr["start_date"];

			else

				dtStartDate = System.DateTime.ParseExact("01/01/1900", "dd/MM/yyyy", null);

			if (Utility.IsNotDBNull(dr["end_date"])) 

				dtEndDate = (DateTime) dr["end_date"];

			else

				dtEndDate = System.DateTime.ParseExact("01/01/1900", "dd/MM/yyyy", null);

			if (Utility.IsNotDBNull(dr["payer_type"]))

				strPayerType = dr["payer_type"].ToString();

			if (Utility.IsNotDBNull(dr["payer_code"]))

				strPayerId = dr["payer_code"].ToString();

			if (Utility.IsNotDBNull(dr["zip_code"]))

				strZipCode = dr["zip_code"].ToString();

			if (Utility.IsNotDBNull(dr["state_code"]))

				strStateCode = dr["state_code"].ToString();

			if (Utility.IsNotDBNull(dr["booking_no"]) && dr["booking_no"].ToString().Trim()!="")

				lBookingNo = Convert.ToInt64(dr["booking_no"]);

			paramFlds = rptSource.DataDefinition.ParameterFields;

			foreach(ParameterFieldDefinition paramFld in paramFlds)

			{

				switch(paramFld.ParameterFieldName)

				{                       

					case "ApplicationID":

						paramDiscVal.Value = strAppID;

						break;

					case "EnterpriseID":

						paramDiscVal.Value = strEnterpriseID;

						break;

					case "p_payer_type":

						paramDiscVal.Value = strPayerType;

						break;

					case "p_payerid":

						paramDiscVal.Value = strPayerId;

						break;

					case "p_userid":

						paramDiscVal.Value = utility.GetUserID();

						break; 

					case "FromDate":

						paramDiscVal.Value = dtStartDate;

						break;

					case "ToDate":

						paramDiscVal.Value = dtEndDate;

						break;

					default:

						continue;

				}

				paramvals = paramFld.CurrentValues;

				paramvals.Add(paramDiscVal);

				paramFld.ApplyCurrentValues(paramvals);

			}

		}

		private void SetLogonInfo()
		{
			//create a structure object to store connection details
			ConnectionDetails conDet = new ConnectionDetails();

			//get connection details from dbcon manager
			conDet = DbConnectionManager.GetConnectionDetails(strAppID, strEnterpriseID);

			TableLogOnInfo MyLogin; 
			//get the no of tables in the main report and 
			//assign connection details to all
			int intTabCnt = rptSource.Database.Tables.Count, i=0;
			for (i=0; i<intTabCnt; i++)
			{
				MyLogin = rptSource.Database.Tables[i].LogOnInfo;
				if (Utility.IsNotDBNull(conDet.DatabaseName))
					MyLogin.ConnectionInfo.DatabaseName = conDet.DatabaseName;
				MyLogin.ConnectionInfo.ServerName = conDet.ServerName;
				MyLogin.ConnectionInfo.UserID = conDet.UserID;
				MyLogin.ConnectionInfo.Password = conDet.Password;
				rptSource.Database.Tables[i].ApplyLogOnInfo(MyLogin);
				
				if(ViewState["ReportName"]!=null)
				{
					if(ViewState["ReportName"].ToString()=="DeliveryManifestReport_MF" && rptSource.Database.Tables[i].Name.IndexOf("v_DeliveryManifestByRoute")>=0)
					{
						rptSource.Database.Tables[i].Location = rptSource.Database.Tables[i].Location.Substring(rptSource.Database.Tables[i].Location.LastIndexOf(".")+1);
					}
					else if(ViewState["ReportName"].ToString()=="DeliveryManifestReportLH_Draft" && rptSource.Database.Tables[i].Name.IndexOf("v_DeliveryManifestByRoute")>=0)
					{
						rptSource.Database.Tables[i].Location = rptSource.Database.Tables[i].Location.Substring(rptSource.Database.Tables[i].Location.LastIndexOf(".")+1);
					}
					else if(ViewState["ReportName"].ToString()=="DeliveryManifestReportLH_Draft_Web" && rptSource.Database.Tables[i].Name.IndexOf("v_DeliveryManifestByRoute")>=0)
					{
						rptSource.Database.Tables[i].Location = rptSource.Database.Tables[i].Location.Substring(rptSource.Database.Tables[i].Location.LastIndexOf(".")+1);
					}
					else if(ViewState["ReportName"].ToString()=="DairyLodgments")
					{
						
					}
					else
					{
						rptSource.Database.Tables[i].Location = rptSource.Database.Tables[i].Name.ToUpper();
					}
				}
				else
				{
					rptSource.Database.Tables[i].Location = rptSource.Database.Tables[i].Name.ToUpper();
				}
				

				
				
				  
			}

			//get subreports from the main report
			Sections crSections; //new Sections();
			ReportObjects crRepObjects; //new ReportObjects();
			SubreportObject crSubRepObj; //new SubreportObject();
			ReportDocument crSubRep; //new ReportDocument();

			crSections = rptSource.ReportDefinition.Sections;
			int intSecCnt = rptSource.ReportDefinition.Sections.Count;
			for (i=0; i<intSecCnt; i++)
			{
				crRepObjects = crSections[i].ReportObjects;
				int intSubCnt = crSections[i].ReportObjects.Count;
				for (int j=0; j<intSubCnt; j++)
				{
					if (crRepObjects[j].Kind == ReportObjectKind.SubreportObject)
					{
						crSubRepObj = (SubreportObject)crRepObjects[j];
						crSubRep = crSubRepObj.OpenSubreport(crSubRepObj.SubreportName);
						//get the no of tables in the sub report and 
						//assign connection details to all
						int intSubTabCnt = crSubRep.Database.Tables.Count;
						for (int k=0; k<intSubTabCnt; k++)
						{
							MyLogin = crSubRep.Database.Tables[k].LogOnInfo;
							if (Utility.IsNotDBNull(conDet.DatabaseName))
								MyLogin.ConnectionInfo.DatabaseName = conDet.DatabaseName;
							MyLogin.ConnectionInfo.ServerName = conDet.ServerName;
							MyLogin.ConnectionInfo.UserID = conDet.UserID;
							MyLogin.ConnectionInfo.Password = conDet.Password;
							crSubRep.Database.Tables[k].ApplyLogOnInfo(MyLogin);
							crSubRep.Database.Tables[k].Location = crSubRep.Database.Tables[k].Name.ToUpper();
							if(ViewState["ReportName"]!=null)
							{
								if(ViewState["ReportName"].ToString()=="DeliveryManifestReportLH_Draft" && crSubRep.Database.Tables[k].Name.IndexOf("v_DeliveryManifestByRoute")>=0)
								{
									crSubRep.Database.Tables[k].Location = crSubRep.Database.Tables[k].Location.Substring(crSubRep.Database.Tables[k].Location.LastIndexOf(".")+1);
								}
								else if(ViewState["ReportName"].ToString()=="DeliveryManifestReportLH_Draft_Web" && crSubRep.Database.Tables[k].Name.IndexOf("v_DeliveryManifestByRoute")>=0)
								{
									crSubRep.Database.Tables[k].Location = crSubRep.Database.Tables[k].Location.Substring(crSubRep.Database.Tables[k].Location.LastIndexOf(".")+1);
								}
								else
								{
									crSubRep.Database.Tables[k].Location = crSubRep.Database.Tables[k].Name.ToUpper();
								}
							}
							else
							{
								crSubRep.Database.Tables[k].Location = crSubRep.Database.Tables[k].Name.ToUpper();
							}
						}
					}
				}
			}
		}
		private void SetReportPublisherLogonInfo()
		{
			//create a structure object to store connection details
			ConnectionDetails conDet = new ConnectionDetails();

			//get connection details from dbcon manager
			conDet = DbConnectionManager.GetConnectionReportPublisher(strAppID, strEnterpriseID);

			TableLogOnInfo MyLogin; 
			//get the no of tables in the main report and 
			//assign connection details to all
			int intTabCnt = rptSource.Database.Tables.Count, i=0;
			for (i=0; i<intTabCnt; i++)
			{
				MyLogin = rptSource.Database.Tables[i].LogOnInfo;
				if (Utility.IsNotDBNull(conDet.DatabaseName))
					MyLogin.ConnectionInfo.DatabaseName = conDet.DatabaseName;
				MyLogin.ConnectionInfo.ServerName = conDet.ServerName;
				MyLogin.ConnectionInfo.UserID = conDet.UserID;
				MyLogin.ConnectionInfo.Password = conDet.Password;
				rptSource.Database.Tables[i].ApplyLogOnInfo(MyLogin);
				
				if(ViewState["ReportName"]!=null)
				{
					if(ViewState["ReportName"].ToString()=="DeliveryManifestReport_MF" && rptSource.Database.Tables[i].Name.IndexOf("v_DeliveryManifestByRoute")>=0)
					{
						rptSource.Database.Tables[i].Location = rptSource.Database.Tables[i].Location.Substring(rptSource.Database.Tables[i].Location.LastIndexOf(".")+1);
					}
					else if(ViewState["ReportName"].ToString()=="DeliveryManifestReportLH_Draft" && rptSource.Database.Tables[i].Name.IndexOf("v_DeliveryManifestByRoute")>=0)
					{
						rptSource.Database.Tables[i].Location = rptSource.Database.Tables[i].Location.Substring(rptSource.Database.Tables[i].Location.LastIndexOf(".")+1);
					}
					else if(ViewState["ReportName"].ToString()=="DeliveryManifestReportLH_Draft_Web" && rptSource.Database.Tables[i].Name.IndexOf("v_DeliveryManifestByRoute")>=0)
					{
						rptSource.Database.Tables[i].Location = rptSource.Database.Tables[i].Location.Substring(rptSource.Database.Tables[i].Location.LastIndexOf(".")+1);
					}
					else
					{
						rptSource.Database.Tables[i].Location = rptSource.Database.Tables[i].Name.ToUpper();
					}
				}
				else
				{
					rptSource.Database.Tables[i].Location = rptSource.Database.Tables[i].Name.ToUpper();
				}					  
			}

			//get subreports from the main report
			Sections crSections; //new Sections();
			ReportObjects crRepObjects; //new ReportObjects();
			SubreportObject crSubRepObj; //new SubreportObject();
			ReportDocument crSubRep; //new ReportDocument();

			crSections = rptSource.ReportDefinition.Sections;
			int intSecCnt = rptSource.ReportDefinition.Sections.Count;
			for (i=0; i<intSecCnt; i++)
			{
				crRepObjects = crSections[i].ReportObjects;
				int intSubCnt = crSections[i].ReportObjects.Count;
				for (int j=0; j<intSubCnt; j++)
				{
					if (crRepObjects[j].Kind == ReportObjectKind.SubreportObject)
					{
						crSubRepObj = (SubreportObject)crRepObjects[j];
						crSubRep = crSubRepObj.OpenSubreport(crSubRepObj.SubreportName);
						//get the no of tables in the sub report and 
						//assign connection details to all
						int intSubTabCnt = crSubRep.Database.Tables.Count;
						for (int k=0; k<intSubTabCnt; k++)
						{
							MyLogin = crSubRep.Database.Tables[k].LogOnInfo;
							if (Utility.IsNotDBNull(conDet.DatabaseName))
								MyLogin.ConnectionInfo.DatabaseName = conDet.DatabaseName;
							MyLogin.ConnectionInfo.ServerName = conDet.ServerName;
							MyLogin.ConnectionInfo.UserID = conDet.UserID;
							MyLogin.ConnectionInfo.Password = conDet.Password;
							crSubRep.Database.Tables[k].ApplyLogOnInfo(MyLogin);
							crSubRep.Database.Tables[k].Location = crSubRep.Database.Tables[k].Name.ToUpper();
							if(ViewState["ReportName"]!=null)
							{
								if(ViewState["ReportName"].ToString()=="DeliveryManifestReportLH_Draft" && crSubRep.Database.Tables[k].Name.IndexOf("v_DeliveryManifestByRoute")>=0)
								{
									crSubRep.Database.Tables[k].Location = crSubRep.Database.Tables[k].Location.Substring(crSubRep.Database.Tables[k].Location.LastIndexOf(".")+1);
								}
								else if(ViewState["ReportName"].ToString()=="DeliveryManifestReportLH_Draft_Web" && crSubRep.Database.Tables[k].Name.IndexOf("v_DeliveryManifestByRoute")>=0)
								{
									crSubRep.Database.Tables[k].Location = crSubRep.Database.Tables[k].Location.Substring(crSubRep.Database.Tables[k].Location.LastIndexOf(".")+1);
								}
								else
								{
									crSubRep.Database.Tables[k].Location = crSubRep.Database.Tables[k].Name.ToUpper();
								}
							}
							else
							{
								crSubRep.Database.Tables[k].Location = crSubRep.Database.Tables[k].Name.ToUpper();
							}
						}
					}
				}
			}
		}
		/*
		private void BindReport()
		{
			rptViewer.ReportSource = rptSource;

			if(Session["FORMID"].ToString()=="BatchManifestControlPanel_Delivery")
			{
				//boon 2012/01/31 - start
//				ddbExport.SelectedValue=".pdf";

				string strFormat= Request.QueryString["Format"].ToString();

				if (strFormat == "xls")
					ddbExport.SelectedValue=".xls";
				else
					ddbExport.SelectedValue=".pdf";

				//boon 2012/01/31 - end

				btnExport_Click(null,null);
			}
		}
		*/
		private void BindReport()
		{
			rptViewer.ReportSource = rptSource;

			if(Session["FORMID"].ToString()=="BatchManifestControlPanel_Delivery")
			{
				if(Request.QueryString["Doc_Type"].ToString() == "xls")
					ddbExport.SelectedValue=".xls";
				else
					ddbExport.SelectedValue=".pdf";
			
				btnExport_Click(null,null);
			}
		}

		private void SetInvoiceParams()
		{
			//declare instance for parameterfields class
			ParameterValues paramvals = new ParameterValues();
			ParameterFieldDefinitions paramFlds; //= new ParameterFieldDefinitions();
			ParameterFieldDefinition paramFld; //= new ParameterFieldDefinition();
			ParameterDiscreteValue paramDiscVal = new ParameterDiscreteValue();
			ParameterRangeValue paramRangeVal = new ParameterRangeValue();

			//get all parameter fields
			paramFlds = rptSource.DataDefinition.ParameterFields;

			//getting parameter field values
			String strUnPrinted=null, strPayerType=null, strPayerID=null, strPayerName=null;
			String strNoStart=null, strNoEnd=null, strDateStart=null, strDateEnd=null;
			DateTime dtStart, dtEnd;

			DataRow dr = m_dsQuery.Tables[0].Rows[0];
			strUnPrinted = dr["unprinted"].ToString();
			strPayerType = dr["payertype"].ToString();
			strPayerID = dr["payerid"].ToString();
			strPayerName = dr["payername"].ToString();
			strNoStart = dr["startno"].ToString();
			strNoEnd = dr["endno"].ToString();
			strDateStart = dr["startdate"].ToString();
			strDateEnd = dr["enddate"].ToString();

			//passing parameter for parameter fields
			//applicationid
			paramDiscVal = new ParameterDiscreteValue();
			paramFld = paramFlds["p_appid"];
			paramDiscVal.Value = strAppID;
			paramvals = paramFld.CurrentValues;
			paramvals.Add(paramDiscVal);
			paramFld.ApplyCurrentValues(paramvals);

			//enterpriseid
			paramDiscVal = new ParameterDiscreteValue();
			paramFld = paramFlds["p_enterprise"];
			paramDiscVal.Value = strEnterpriseID;
			paramvals = paramFld.CurrentValues;
			paramvals.Add(paramDiscVal);
			paramFld.ApplyCurrentValues(paramvals);

			//payer type
			paramDiscVal = new ParameterDiscreteValue();
			paramFld = paramFlds["p_payer_type"];
			if (strPayerType=="A" || strPayerType=="C")
				paramDiscVal.Value = strPayerType;
			else
				paramDiscVal.Value = "*";
			paramvals = paramFld.CurrentValues;
			paramvals.Add(paramDiscVal);
			paramFld.ApplyCurrentValues(paramvals);

			//payerid
			paramDiscVal = new ParameterDiscreteValue();
			paramFld = paramFlds["p_payerid"];
			if (Utility.IsNotDBNull(strPayerID) && strPayerID!="")
				paramDiscVal.Value = strPayerID;
			else
				paramDiscVal.Value = "*";
			paramvals = paramFld.CurrentValues;
			paramvals.Add(paramDiscVal);
			paramFld.ApplyCurrentValues(paramvals);

			//payer name
			paramDiscVal = new ParameterDiscreteValue();
			paramFld = paramFlds["p_payer_name"];
			if (Utility.IsNotDBNull(strPayerName) && strPayerName!="")
				paramDiscVal.Value = strPayerName;
			else
				paramDiscVal.Value = "*";
			paramvals = paramFld.CurrentValues;
			paramvals.Add(paramDiscVal);
			paramFld.ApplyCurrentValues(paramvals);

			//unprinted invoice option
			paramDiscVal = new ParameterDiscreteValue();
			paramFld = paramFlds["p_unprinted"];
			paramDiscVal.Value = strUnPrinted;
			paramvals = paramFld.CurrentValues;
			paramvals.Add(paramDiscVal);
			paramFld.ApplyCurrentValues(paramvals);

			//invoice no range
			paramRangeVal = new ParameterRangeValue();
			paramFld = paramFlds["p_invoice_no"];
			if (Utility.IsNotDBNull(strNoStart) && strNoStart!="")
				paramRangeVal.StartValue = strNoStart;
			else
				paramRangeVal.StartValue = "*"; 

			if (Utility.IsNotDBNull(strNoEnd) && strNoEnd!="")
				paramRangeVal.EndValue = strNoEnd;
			else
				paramRangeVal.EndValue = "*";
			paramvals = paramFld.CurrentValues;
			paramvals.Add(paramRangeVal);
			paramFld.ApplyCurrentValues(paramvals);

			//invoice date range
			paramRangeVal = new ParameterRangeValue();
			paramFld = paramFlds["p_invoice_datetime"];
			if (Utility.IsNotDBNull(strDateStart) && strDateStart!="")
			{
				dtStart = System.DateTime.ParseExact(strDateStart, "dd/MM/yyyy", null);
				if (Utility.IsNotDBNull(strDateEnd) && strDateEnd!="")
				{
					dtEnd = System.DateTime.ParseExact(strDateEnd+" 23:59", "dd/MM/yyyy HH:mm", null);
					paramRangeVal.StartValue = dtStart;
					paramRangeVal.EndValue = dtEnd;
				}
				else
				{
					dtStart = System.DateTime.ParseExact("01/01/1900", "dd/MM/yyyy", null);
					paramRangeVal.StartValue = dtStart;
					paramRangeVal.EndValue = dtStart;
				}
			}
			else
			{
				dtStart = System.DateTime.ParseExact("01/01/1900", "dd/MM/yyyy", null);
				paramRangeVal.StartValue = dtStart;
				paramRangeVal.EndValue = dtStart;
			}
			paramvals = paramFld.CurrentValues;
			paramvals.Add(paramRangeVal);
			paramFld.ApplyCurrentValues(paramvals);
		}

		private void SetShipExcepParams()
		{
			//getting parameter field values
			DateTime dtDefaultFrom = System.DateTime.ParseExact("01/01/1900", "dd/MM/yyyy", null);
			DateTime dtDefaultTo = System.DateTime.ParseExact("01/01/2099", "dd/MM/yyyy", null);

			DataRow dr = m_dsQuery.Tables[0].Rows[0];

			//declare instance for parameterfields class
			ParameterValues paramvals = new ParameterValues();
			ParameterFieldDefinitions paramFlds; //= new ParameterFieldDefinitions();
			ParameterFieldDefinition paramFld; //= new ParameterFieldDefinition();
			ParameterDiscreteValue paramDiscVal = new ParameterDiscreteValue();
			ParameterRangeValue paramRangeVal = new ParameterRangeValue();

			//get all parameter fields
			paramFlds = rptSource.DataDefinition.ParameterFields;		
			
			//passing parameter for parameter fields
			//ApplicationID
			paramDiscVal = new ParameterDiscreteValue();
			paramFld = paramFlds["prmApplicationID"];
			paramDiscVal.Value = strAppID;
			paramvals = paramFld.CurrentValues;
			paramvals.Add(paramDiscVal);
			paramFld.ApplyCurrentValues(paramvals);

			//EnterpriseID
			paramDiscVal = new ParameterDiscreteValue();
			paramFld = paramFlds["prmEnterpriseID"];
			paramDiscVal.Value = strEnterpriseID;
			paramvals = paramFld.CurrentValues;
			paramvals.Add(paramDiscVal);
			paramFld.ApplyCurrentValues(paramvals);
			
			//User ID
			paramDiscVal = new ParameterDiscreteValue();
			paramFld = paramFlds["prmUserID"];
			paramDiscVal.Value = strUserId;
			paramvals = paramFld.CurrentValues;
			paramvals.Add(paramDiscVal);
			paramFld.ApplyCurrentValues(paramvals);

			//Payer Type
			paramDiscVal = new ParameterDiscreteValue();
			paramFld = paramFlds["prmPayerType"];
			if (Utility.IsNotDBNull(dr["payer_type"]) && dr["payer_type"].ToString() != "")
			{	
				String strPayerType =dr["payer_type"].ToString();
				paramDiscVal.Value = strPayerType;	
			}
			else
				paramDiscVal.Value = "*";
			paramvals = paramFld.CurrentValues;
			paramvals.Add(paramDiscVal);
			paramFld.ApplyCurrentValues(paramvals);
			
			//Payer ID
			paramDiscVal = new ParameterDiscreteValue();
			paramFld = paramFlds["prmPayerID"];
			if (Utility.IsNotDBNull(dr["payer_code"]) && dr["payer_code"].ToString()!="")
			{
				String strPayerID=dr["payer_code"].ToString();
				paramDiscVal.Value = strPayerID;
			}
			else
				paramDiscVal.Value = "*";
			paramvals = paramFld.CurrentValues;
			paramvals.Add(paramDiscVal);
			paramFld.ApplyCurrentValues(paramvals);

			//Entitled MBG
			paramDiscVal = new ParameterDiscreteValue();
			paramFld = paramFlds["prmEntitledMBG"];
			if (Utility.IsNotDBNull(dr["EntitledMBG"]) && dr["EntitledMBG"].ToString() !="")
			{
				String strEntitledMBG=dr["EntitledMBG"].ToString();
				paramDiscVal.Value = strEntitledMBG;
			}
			else
				paramDiscVal.Value = "*";
			paramvals = paramFld.CurrentValues;
			paramvals.Add(paramDiscVal);
			paramFld.ApplyCurrentValues(paramvals);

			//Consignment Number
			paramDiscVal = new ParameterDiscreteValue();
			paramFld = paramFlds["prmConsignmentNo"];
			if (Utility.IsNotDBNull(dr["ConsignmentNo"]) && dr["ConsignmentNo"].ToString() !="")
			{
				String strConsignmentNo=dr["ConsignmentNo"].ToString();
				paramDiscVal.Value = strConsignmentNo;
			}
			else
				paramDiscVal.Value = "*";
			paramvals = paramFld.CurrentValues;
			paramvals.Add(paramDiscVal);
			paramFld.ApplyCurrentValues(paramvals);

			//Status Code
			paramDiscVal = new ParameterDiscreteValue();
			paramFld = paramFlds["prmStatusCode"];
			if (Utility.IsNotDBNull(dr["Status"]) && dr["Status"].ToString() !="")
			{
				String strStatus=dr["Status"].ToString();
				paramDiscVal.Value = strStatus;
			}
			else
				paramDiscVal.Value = "*";
			paramvals = paramFld.CurrentValues;
			paramvals.Add(paramDiscVal);
			paramFld.ApplyCurrentValues(paramvals);

			//Exception Code
			paramDiscVal = new ParameterDiscreteValue();
			paramFld = paramFlds["prmExceptionCode"];
			if (Utility.IsNotDBNull(dr["Exception"]) && dr["Exception"].ToString() !="")
			{
				String strException=dr["Exception"].ToString();
				paramDiscVal.Value = strException;
			}
			else
				paramDiscVal.Value = "*";
			paramvals = paramFld.CurrentValues;
			paramvals.Add(paramDiscVal);
			paramFld.ApplyCurrentValues(paramvals);

		//Jeab  23 May 2011
			//Service Code
			paramDiscVal = new ParameterDiscreteValue();
			paramFld = paramFlds["prmServiceCode"];
			if (Utility.IsNotDBNull(dr["service_code"]) && dr["service_code"].ToString() !="")
			{
				String strServiceCode=dr["service_code"].ToString();
				paramDiscVal.Value = strServiceCode;
			}
			else
				paramDiscVal.Value = "*";
			paramvals = paramFld.CurrentValues;
			paramvals.Add(paramDiscVal);
			paramFld.ApplyCurrentValues(paramvals);
		//Jeab 23 May 2011  =========> End
			
			//Searching Date Type
			paramDiscVal = new ParameterDiscreteValue();
			paramFld = paramFlds["prmDateType"];
			if (Utility.IsNotDBNull(dr["tran_date"]) && dr["tran_date"].ToString() !="")
			{
				String strDateType=dr["tran_date"].ToString();
				paramDiscVal.Value = strDateType;
			}
			else
				paramDiscVal.Value = "*";
			paramvals = paramFld.CurrentValues;
			paramvals.Add(paramDiscVal);
			paramFld.ApplyCurrentValues(paramvals);

			//From Date
			paramDiscVal = new ParameterDiscreteValue();
			paramFld = paramFlds["prmDateFrom"];
			if (Utility.IsNotDBNull(dr["start_date"]) && dr["start_date"].ToString() !="")
			{
				DateTime dtDateFrom=System.Convert.ToDateTime(dr["start_date"]);
				paramDiscVal.Value = dtDateFrom;
			}						
			paramvals = paramFld.CurrentValues;
			paramvals.Add(paramDiscVal);
			paramFld.ApplyCurrentValues(paramvals);

			//To Date
			paramDiscVal = new ParameterDiscreteValue();
			paramFld = paramFlds["prmDateTo"];
			if (Utility.IsNotDBNull(dr["end_date"]) && dr["end_date"].ToString() !="")
			{
				DateTime dtDateTo=System.Convert.ToDateTime(dr["end_date"]);
				paramDiscVal.Value = dtDateTo;
			}						
			paramvals = paramFld.CurrentValues;
			paramvals.Add(paramDiscVal);
			paramFld.ApplyCurrentValues(paramvals);		
	
			//route_type
			paramDiscVal = new ParameterDiscreteValue();
			paramFld = paramFlds["p_route_type"];
			if (Utility.IsNotDBNull(dr["route_type"]) && dr["route_type"].ToString() !="")
			{
				String strroute_type=dr["route_type"].ToString();
				paramDiscVal.Value = strroute_type;
			}
			else
				paramDiscVal.Value = "*";
			paramvals = paramFld.CurrentValues;
			paramvals.Add(paramDiscVal);
			paramFld.ApplyCurrentValues(paramvals);

			//route_code
			paramDiscVal = new ParameterDiscreteValue();
			paramFld = paramFlds["p_route_code"];
			if (Utility.IsNotDBNull(dr["route_code"]) && dr["route_code"].ToString() !="")
			{
				String strroute_code=dr["route_code"].ToString();
				paramDiscVal.Value = strroute_code;
			}
			else
				paramDiscVal.Value = "*";
			paramvals = paramFld.CurrentValues;
			paramvals.Add(paramDiscVal);
			paramFld.ApplyCurrentValues(paramvals);

			//origin_dc
			paramDiscVal = new ParameterDiscreteValue();
			paramFld = paramFlds["p_origin_dc"];
			if (Utility.IsNotDBNull(dr["origin_dc"]) && dr["origin_dc"].ToString() !="")
			{
				String strorigin_dc=dr["origin_dc"].ToString();
				paramDiscVal.Value = strorigin_dc;
			}
			else
				paramDiscVal.Value = "*";
			paramvals = paramFld.CurrentValues;
			paramvals.Add(paramDiscVal);
			paramFld.ApplyCurrentValues(paramvals);

			//destination_dc
			paramDiscVal = new ParameterDiscreteValue();
			paramFld = paramFlds["p_destination_dc"];
			if (Utility.IsNotDBNull(dr["destination_dc"]) && dr["destination_dc"].ToString() !="")
			{
				String strdestination_dc=dr["destination_dc"].ToString();
				paramDiscVal.Value = strdestination_dc;
			}
			else
				paramDiscVal.Value = "*";
			paramvals = paramFld.CurrentValues;
			paramvals.Add(paramDiscVal);
			paramFld.ApplyCurrentValues(paramvals);

			//delPath_origin_dc
			paramDiscVal = new ParameterDiscreteValue();
			paramFld = paramFlds["p_delPath_origin_dc"];
			if (Utility.IsNotDBNull(dr["delPath_origin_dc"]) && dr["delPath_origin_dc"].ToString() !="")
			{
				String strdelPath_origin_dc=dr["delPath_origin_dc"].ToString();
				paramDiscVal.Value = strdelPath_origin_dc;
			}
			else
				paramDiscVal.Value = "*";
			paramvals = paramFld.CurrentValues;
			paramvals.Add(paramDiscVal);
			paramFld.ApplyCurrentValues(paramvals);

			//delPath_destination_dc
			paramDiscVal = new ParameterDiscreteValue();
			paramFld = paramFlds["p_delPath_destination_dc"];
			if (Utility.IsNotDBNull(dr["delPath_destination_dc"]) && dr["delPath_destination_dc"].ToString() !="")
			{
				String strdelPath_destination_dc=dr["delPath_destination_dc"].ToString();
				paramDiscVal.Value = strdelPath_destination_dc;
			}
			else
				paramDiscVal.Value = "*";
			paramvals = paramFld.CurrentValues;
			paramvals.Add(paramDiscVal);
			paramFld.ApplyCurrentValues(paramvals);

			//zip_code
			paramDiscVal = new ParameterDiscreteValue();
			paramFld = paramFlds["p_zipcode"];
			if (Utility.IsNotDBNull(dr["zip_code"]) && dr["zip_code"].ToString() !="")
			{
				String strzip_code=dr["zip_code"].ToString();
				paramDiscVal.Value = strzip_code;
			}
			else
				paramDiscVal.Value = "*";
			paramvals = paramFld.CurrentValues;
			paramvals.Add(paramDiscVal);
			paramFld.ApplyCurrentValues(paramvals);

			//state_code
			paramDiscVal = new ParameterDiscreteValue();
			paramFld = paramFlds["p_state_code"];
			if (Utility.IsNotDBNull(dr["state_code"]) && dr["state_code"].ToString() !="")
			{
				String strstate_code=dr["state_code"].ToString();
				paramDiscVal.Value = strstate_code;
			}
			else
				paramDiscVal.Value = "*";
			paramvals = paramFld.CurrentValues;
			paramvals.Add(paramDiscVal);
			paramFld.ApplyCurrentValues(paramvals);
		}

		private void SetDeliveryManifestParams()
		{		
			ParameterValues paramvals = new ParameterValues();
			ParameterFieldDefinitions paramFlds; 
			ParameterFieldDefinition paramFld; 
			ParameterDiscreteValue paramDiscVal = new ParameterDiscreteValue();
			
			//get all parameter fields
			paramFlds = rptSource.DataDefinition.ParameterFields;		
			
			//passing parameter for parameter fields
			//ApplicationID
			paramDiscVal = new ParameterDiscreteValue();
			paramFld = paramFlds["prmApplicationID"];
			paramDiscVal.Value = strAppID;
			paramvals = paramFld.CurrentValues;
			paramvals.Add(paramDiscVal);
			paramFld.ApplyCurrentValues(paramvals);

			//EnterpriseID
			paramDiscVal = new ParameterDiscreteValue();
			paramFld = paramFlds["prmEnterpriseID"];
			paramDiscVal.Value = strEnterpriseID;
			paramvals = paramFld.CurrentValues;
			paramvals.Add(paramDiscVal);
			paramFld.ApplyCurrentValues(paramvals);
			
			//User ID
			paramDiscVal = new ParameterDiscreteValue();
			paramFld = paramFlds["prmUserID"];
			paramDiscVal.Value = strUserId;
			paramvals = paramFld.CurrentValues;
			paramvals.Add(paramDiscVal);
			paramFld.ApplyCurrentValues(paramvals);

			
			for (int i=0;i<m_dsQuery.Tables[0].Rows.Count;i++)
			{
				DataRow dr = m_dsQuery.Tables[0].Rows[i];
				
				//Delivery Path Type, Assign ONLY ONCE, since this allows ONE value
				if (i==0)
				{
					paramDiscVal = new ParameterDiscreteValue();
					paramFld = paramFlds["prmDeliveryType"];
					if (Utility.IsNotDBNull(dr["delivery_type"]) && dr["delivery_type"].ToString() != "")
					{	
						String strDeliveryType =dr["delivery_type"].ToString();						
						paramDiscVal.Value = strDeliveryType;	
					}
					else
						paramDiscVal.Value = "*";
					paramvals = paramFld.CurrentValues;
					paramvals.Add(paramDiscVal);
					paramFld.ApplyCurrentValues(paramvals);
				}
			
				//Delivery Path Code
				paramDiscVal = new ParameterDiscreteValue();
				paramFld = paramFlds["prmPathCode"];
				if (Utility.IsNotDBNull(dr["path_code"]) && dr["path_code"].ToString()!="")
				{
					String strPathCode=dr["path_code"].ToString();
					paramDiscVal.Value = strPathCode;
				}
				else
					paramDiscVal.Value = "*";
				paramvals = paramFld.CurrentValues;
				paramvals.Add(paramDiscVal);
				paramFld.ApplyCurrentValues(paramvals);

				//prmFlight_VehicleNo
				paramDiscVal = new ParameterDiscreteValue();
				paramFld = paramFlds["prmFlight_VehicleNo"];
				if (Utility.IsNotDBNull(dr["flight_vehicle_no"]) && dr["flight_vehicle_no"].ToString() !="")
				{
					String strFlight_VehicleNo=dr["flight_vehicle_no"].ToString();
					paramDiscVal.Value = strFlight_VehicleNo;
				}
				else
					paramDiscVal.Value = "*";
				paramvals = paramFld.CurrentValues;
				paramvals.Add(paramDiscVal);
				paramFld.ApplyCurrentValues(paramvals);

				//prmDelManifestDate
				paramDiscVal = new ParameterDiscreteValue();
				paramFld = paramFlds["prmDelManifestDate"];
				if (Utility.IsNotDBNull(dr["delivery_manifest_datetime"]) && dr["delivery_manifest_datetime"].ToString() !="")
				{
					DateTime dtDelManifestDate=System.Convert.ToDateTime(dr["delivery_manifest_datetime"]);
					paramDiscVal.Value = dtDelManifestDate;
				}
				else
					paramDiscVal.Value = "*";
				paramvals = paramFld.CurrentValues;
				paramvals.Add(paramDiscVal);
				paramFld.ApplyCurrentValues(paramvals);
				
			}

		}
	
		private void SetCreditNoteParams()
		{
			//String strDebitNote = (String)Session["SESSION_DS1"]; 

			DataTable dt = new DataTable();
			SessionDS m_sdsCreditNote = (SessionDS)Session["SESSION_DS1"];
			dt = m_sdsCreditNote.ds.Tables["DT_CNNO_Selected"];

			String strCreditNote = (string)dt.Rows[0]["CN_NO"]; 

			ParameterValues paramvals = new ParameterValues();
			ParameterFieldDefinitions paramFlds; 
			ParameterFieldDefinition paramFld; 
			ParameterDiscreteValue paramDiscVal = new ParameterDiscreteValue();
			
			//get all parameter fields
			paramFlds = rptSource.DataDefinition.ParameterFields;		
			
			//passing parameter for parameter fields
			//ApplicationID
			paramDiscVal = new ParameterDiscreteValue();
			paramFld = paramFlds["@p_application_id"];
			paramDiscVal.Value = strAppID;
			paramvals = paramFld.CurrentValues;
			paramvals.Add(paramDiscVal);
			paramFld.ApplyCurrentValues(paramvals);

			//EnterpriseID
			paramDiscVal = new ParameterDiscreteValue();
			paramFld = paramFlds["@p_enterprise_id"];
			paramDiscVal.Value = strEnterpriseID;
			paramvals = paramFld.CurrentValues;
			paramvals.Add(paramDiscVal);
			paramFld.ApplyCurrentValues(paramvals);
			
			//User ID
//			paramDiscVal = new ParameterDiscreteValue();
//			paramFld = paramFlds["prmUserID"];
//			paramDiscVal.Value = strUserId;
//			paramvals = paramFld.CurrentValues;
//			paramvals.Add(paramDiscVal);
//			paramFld.ApplyCurrentValues(paramvals);

			paramDiscVal = new ParameterDiscreteValue();
			paramFld = paramFlds["@p_credit_no"];
			paramDiscVal.Value = strCreditNote;
			paramvals = paramFld.CurrentValues;
			paramvals.Add(paramDiscVal);
			paramFld.ApplyCurrentValues(paramvals);
		}

		private void SetDeliveryParams()
		{
			string strDeliveryType= Request.QueryString["Batch_type"].ToString();
			string strBatch_no= Request.QueryString["Batch_no"].ToString();
			string strLocation= Request.QueryString["Location"].ToString();


			ParameterFieldDefinitions paramFldDefs ;
			ParameterValues paramVals = new ParameterValues() ;
			ParameterDiscreteValue paramDVal = new ParameterDiscreteValue();
			paramFldDefs = rptSource.DataDefinition.ParameterFields;

			if(strDeliveryType=="S" || strDeliveryType=="W")
			{
				foreach(ParameterFieldDefinition paramFldDef in paramFldDefs)
				{
					switch(paramFldDef.ParameterFieldName)
					{
						case "prmDeliveryType":
							paramDVal.Value = strDeliveryType;
							break;
						case "prmBatchNo":
							paramDVal.Value =  strBatch_no; 
							break;
						case "prmApplicationID":
							paramDVal.Value = strAppID ; 
							break;
						case "prmEnterpriseID":
							paramDVal.Value = strEnterpriseID ; 
							break;
						case "prmUserID":
							paramDVal.Value = strUserId; 
							break;
						default:
							continue;
					}
					paramVals = paramFldDef.CurrentValues;
					paramVals.Add(paramDVal);
					paramFldDef.ApplyCurrentValues(paramVals);
				}
			}

			else
			{
				DataSet dsCheckReport = BatchManifest.GetBatchDetail_Shipping_BMCP(strAppID,strEnterpriseID, strBatch_no);
				if(dsCheckReport.Tables[0].Rows.Count>0)
				{
					bool hasFlag=false;
					for(int i=0;i<=dsCheckReport.Tables[0].Rows.Count-1;i++)
					{
						if(dsCheckReport.Tables[0].Rows[i]["Dep_DC"].ToString()==strLocation)
						{
							hasFlag=true;
							break;
						}
					}
					if(hasFlag==true)
					{
						foreach(ParameterFieldDefinition paramFldDef in paramFldDefs)
						{
							switch(paramFldDef.ParameterFieldName)
							{
								case "prmDeliveryType":
									paramDVal.Value = strDeliveryType;
									break;
								case "prmBatchNo":
									paramDVal.Value =  strBatch_no; 
									break;
								case "prmApplicationID":
									paramDVal.Value = strAppID ; 
									break;
								case "prmEnterpriseID":
									paramDVal.Value = strEnterpriseID ; 
									break;
								case "prmUserID":
									paramDVal.Value = strUserId; 
									break;
								case "prmLocation":
									paramDVal.Value = strLocation; 
									break;
								default:
									continue;
							}
							paramVals = paramFldDef.CurrentValues;
							paramVals.Add(paramDVal);
							paramFldDef.ApplyCurrentValues(paramVals);
						}
					}
					else
					{
						rptSource = new DeliveryManifestReportLH_Draft_Web();
						ViewState["ReportName"]="DeliveryManifestReportLH_Draft_Web";
						paramFldDefs = rptSource.DataDefinition.ParameterFields;
						string strMainLocation="";
						//getDC ����ش
						DataSet dsCheckDC= BatchManifest.GetBatchDetail_Shipping(strAppID,strEnterpriseID, strBatch_no);
						if(dsCheckDC.Tables[0].Rows.Count>0)
						{
							strMainLocation=dsCheckDC.Tables[0].Rows[0]["Dep_DC"].ToString().Trim();
						}
						
						foreach(ParameterFieldDefinition paramFldDef in paramFldDefs)
						{
							switch(paramFldDef.ParameterFieldName)
							{
								case "prmDeliveryType":
									paramDVal.Value = strDeliveryType;
									break;
								case "prmBatchNo":
									paramDVal.Value =  strBatch_no; 
									break;
								case "prmApplicationID":
									paramDVal.Value = strAppID ; 
									break;
								case "prmEnterpriseID":
									paramDVal.Value = strEnterpriseID ; 
									break;
								case "prmUserID":
									paramDVal.Value = strUserId; 
									break;
								case "prmLocation":
									paramDVal.Value = strLocation; 
									break;
								case "prmMainLocation":
									paramDVal.Value = strMainLocation; 
									break;
								default:
									continue;
							}
							paramVals = paramFldDef.CurrentValues;
							paramVals.Add(paramDVal);
							paramFldDef.ApplyCurrentValues(paramVals);
						}
					}
				}
			}
		}

		private void SetDebitNoteParams()
		{
			DataTable dt = new DataTable();
			SessionDS m_sdsDebitNote = (SessionDS)Session["SESSION_DS1"];
			dt = m_sdsDebitNote.ds.Tables["DT_DNNO_Selected"];

			String strDebitNote = (string)dt.Rows[0]["DN_NO"]; 

			ParameterValues paramvals = new ParameterValues();
			ParameterFieldDefinitions paramFlds; 
			ParameterFieldDefinition paramFld; 
			ParameterDiscreteValue paramDiscVal = new ParameterDiscreteValue();
			
			//get all parameter fields
			paramFlds = rptSource.DataDefinition.ParameterFields;		
			
			//passing parameter for parameter fields
			//ApplicationID
			paramDiscVal = new ParameterDiscreteValue();
			paramFld = paramFlds["@p_application_id"];
			paramDiscVal.Value = strAppID;
			paramvals = paramFld.CurrentValues;
			paramvals.Add(paramDiscVal);
			paramFld.ApplyCurrentValues(paramvals);

			//EnterpriseID
			paramDiscVal = new ParameterDiscreteValue();
			paramFld = paramFlds["@p_enterprise_id"];
			paramDiscVal.Value = strEnterpriseID;
			paramvals = paramFld.CurrentValues;
			paramvals.Add(paramDiscVal);
			paramFld.ApplyCurrentValues(paramvals);
			
			//User ID
//			paramDiscVal = new ParameterDiscreteValue();
//			paramFld = paramFlds["prmUserID"];
//			paramDiscVal.Value = strUserId;
//			paramvals = paramFld.CurrentValues;
//			paramvals.Add(paramDiscVal);
//			paramFld.ApplyCurrentValues(paramvals);

			paramDiscVal = new ParameterDiscreteValue();
			paramFld = paramFlds["@p_debit_no"];
			paramDiscVal.Value = strDebitNote;
			paramvals = paramFld.CurrentValues;
			paramvals.Add(paramDiscVal);
			paramFld.ApplyCurrentValues(paramvals);
		}

		private void SetServiceFailureParams()
		{
			String strConsignmentNo="*", strServiceCode="*";
			String strRouteCode="*", strRouteType="*";
			String strPayerID="*", strPayerType="";
			String strorigin_dc="*", strdestination_dc="*";
			String strdelPath_origin_dc="*", strdelPath_destination_dc="*";
			String strzip_code="*", strstate_code="*";
			DateTime strFrom = System.DateTime.ParseExact("01/01/1900", "dd/MM/yyyy", null);
			DateTime strTo = System.DateTime.ParseExact("01/01/1900", "dd/MM/yyyy", null);

			ParameterFields  paramFields = new ParameterFields(); 
			ParameterField paramField = new ParameterField();
			ParameterFieldDefinitions paramFldDefs ;
			ParameterFieldDefinition paramFldDef ;
			ParameterValues paramVals ;
			ParameterDiscreteValue paramDVal;
			paramFldDefs = rptSource.DataDefinition.ParameterFields;
			
			DataRow dr  = m_dsQuery.Tables[0].Rows[0];

			if (Utility.IsNotDBNull(dr["start_date"]))
				strFrom = Convert.ToDateTime(dr["start_date"]);
			if (Utility.IsNotDBNull(dr["end_date"]))
				strTo = Convert.ToDateTime(dr["end_date"]);

			if (Utility.IsNotDBNull(dr["route_code"])) 
				strRouteCode = dr["route_code"].ToString();
			if (Utility.IsNotDBNull(dr["route_type"])) 
				strRouteType = dr["route_type"].ToString();

			if (Utility.IsNotDBNull(dr["payer_code"])) 
				strPayerID = dr["payer_code"].ToString();
			if (Utility.IsNotDBNull(dr["payer_type"])) 
				strPayerType = dr["payer_type"].ToString();

			if (Utility.IsNotDBNull(dr["origin_dc"])) 
				strorigin_dc = dr["origin_dc"].ToString();
			if (Utility.IsNotDBNull(dr["destination_dc"])) 
				strdestination_dc = dr["destination_dc"].ToString();

			if (Utility.IsNotDBNull(dr["delPath_origin_dc"])) 
				strdelPath_origin_dc = dr["delPath_origin_dc"].ToString();
			if (Utility.IsNotDBNull(dr["delPath_destination_dc"])) 
				strdelPath_destination_dc = dr["delPath_destination_dc"].ToString();

			if (Utility.IsNotDBNull(dr["zip_code"])) 
				strzip_code = dr["zip_code"].ToString();
			if (Utility.IsNotDBNull(dr["state_code"])) 
				strstate_code = dr["state_code"].ToString();

			if (Utility.IsNotDBNull(dr["consignmentNo"])) 
				strConsignmentNo = dr["consignmentNo"].ToString();
			if (Utility.IsNotDBNull(dr["serviceCode"])) 
				strServiceCode = dr["serviceCode"].ToString();

			//ApplicationID
			paramDVal = new ParameterDiscreteValue();
			paramDVal.Value = strAppID;
			paramFldDef = paramFldDefs["prmApplicationId"];
			paramVals = paramFldDef.CurrentValues;
			paramVals.Add(paramDVal);
			paramFldDef.ApplyCurrentValues(paramVals);

			//EnterpriseID
			paramDVal = new ParameterDiscreteValue();
			paramDVal.Value = strEnterpriseID;
			paramFldDef = paramFldDefs["prmEnterpriseId"];
			paramVals = paramFldDef.CurrentValues;
			paramVals.Add(paramDVal);
			paramFldDef.ApplyCurrentValues(paramVals);

			//UserId
			paramDVal = new ParameterDiscreteValue();
			paramDVal.Value = strUserId;
			paramFldDef = paramFldDefs["prmUserId"];
			paramVals = paramFldDef.CurrentValues;
			paramVals.Add(paramDVal);
			paramFldDef.ApplyCurrentValues(paramVals);

			//Payer Type
			paramDVal = new ParameterDiscreteValue();
			if (Utility.IsNotDBNull(strPayerType) && strPayerType!="")
				paramDVal.Value = strPayerType;
			else
				paramDVal.Value = "*";
			paramFldDef = paramFldDefs["prmPayerType"];
			paramVals = paramFldDef.CurrentValues;
			paramVals.Add(paramDVal);
			paramFldDef.ApplyCurrentValues(paramVals);

			//Payer ID
			paramDVal = new ParameterDiscreteValue();
			if (Utility.IsNotDBNull(strPayerID) && strPayerID!="")
				paramDVal.Value = strPayerID;
			else
				paramDVal.Value = "*";
			paramFldDef = paramFldDefs["prmPayerId"];
			paramVals = paramFldDef.CurrentValues;
			paramVals.Add(paramDVal);
			paramFldDef.ApplyCurrentValues(paramVals);

			//ConsignmentNo
			paramDVal = new ParameterDiscreteValue();
			if (Utility.IsNotDBNull(strConsignmentNo) && strConsignmentNo!="")
				paramDVal.Value = strConsignmentNo;
			else
				paramDVal.Value = "*";
			paramFldDef = paramFldDefs["prmConsignNo"];
			paramVals = paramFldDef.CurrentValues;
			paramVals.Add(paramDVal);
			paramFldDef.ApplyCurrentValues(paramVals);

			//Service Code
			paramDVal = new ParameterDiscreteValue();
			if (Utility.IsNotDBNull(strServiceCode) && strServiceCode!="")
				paramDVal.Value = strServiceCode;
			else
				paramDVal.Value = "*";
			paramFldDef = paramFldDefs["prmServiceCode"];
			paramVals = paramFldDef.CurrentValues;
			paramVals.Add(paramDVal);
			paramFldDef.ApplyCurrentValues(paramVals);

			//route_type
			paramDVal = new ParameterDiscreteValue();
			if (Utility.IsNotDBNull(strRouteType) && strRouteType!="")
				paramDVal.Value = strRouteType;
			else
				paramDVal.Value = "*";
			paramFldDef = paramFldDefs["prmRouteType"];
			paramVals = paramFldDef.CurrentValues;
			paramVals.Add(paramDVal);
			paramFldDef.ApplyCurrentValues(paramVals);

			//route_code
			paramDVal = new ParameterDiscreteValue();
			if (Utility.IsNotDBNull(strRouteCode) && strRouteCode!="")
				paramDVal.Value = strRouteCode;
			else
				paramDVal.Value = "*";
			paramFldDef = paramFldDefs["prmRouteCode"];
			paramVals = paramFldDef.CurrentValues;
			paramVals.Add(paramDVal);
			paramFldDef.ApplyCurrentValues(paramVals);

			//origin_dc
			paramDVal = new ParameterDiscreteValue();
			if (Utility.IsNotDBNull(strorigin_dc) && strorigin_dc!="")
				paramDVal.Value = strorigin_dc;
			else
				paramDVal.Value = "*";
			paramFldDef = paramFldDefs["prmOriginDC"];
			paramVals = paramFldDef.CurrentValues;
			paramVals.Add(paramDVal);
			paramFldDef.ApplyCurrentValues(paramVals);

			//destination_dc
			paramDVal = new ParameterDiscreteValue();
			if (Utility.IsNotDBNull(strdestination_dc) && strdestination_dc!="")
				paramDVal.Value = strdestination_dc;
			else
				paramDVal.Value = "*";
			paramFldDef = paramFldDefs["prmDestinationDC"];
			paramVals = paramFldDef.CurrentValues;
			paramVals.Add(paramDVal);
			paramFldDef.ApplyCurrentValues(paramVals);

			//delPath_origin_dc
			paramDVal = new ParameterDiscreteValue();
			if (Utility.IsNotDBNull(strdelPath_origin_dc) && strdelPath_origin_dc!="")
				paramDVal.Value = strdelPath_origin_dc;
			else
				paramDVal.Value = "*";
			paramFldDef = paramFldDefs["prmDELOriginDC"];
			paramVals = paramFldDef.CurrentValues;
			paramVals.Add(paramDVal);
			paramFldDef.ApplyCurrentValues(paramVals);

			//delPath_destination_dc
			paramDVal = new ParameterDiscreteValue();
			if (Utility.IsNotDBNull(strdelPath_destination_dc) && strdelPath_destination_dc!="")
				paramDVal.Value = strdelPath_destination_dc;
			else
				paramDVal.Value = "*";
			paramFldDef = paramFldDefs["prmDELDestinationDC"];
			paramVals = paramFldDef.CurrentValues;
			paramVals.Add(paramDVal);
			paramFldDef.ApplyCurrentValues(paramVals);

			//zip_code
			paramDVal = new ParameterDiscreteValue();
			if (Utility.IsNotDBNull(strzip_code) && strzip_code!="")
				paramDVal.Value = strzip_code;
			else
				paramDVal.Value = "*";
			paramFldDef = paramFldDefs["prmZipCode"];
			paramVals = paramFldDef.CurrentValues;
			paramVals.Add(paramDVal);
			paramFldDef.ApplyCurrentValues(paramVals);

			//state_code
			paramDVal = new ParameterDiscreteValue();
			if (Utility.IsNotDBNull(strstate_code) && strstate_code!="")
				paramDVal.Value = strstate_code;
			else
				paramDVal.Value = "*";
			paramFldDef = paramFldDefs["prmStateCode"];
			paramVals = paramFldDef.CurrentValues;
			paramVals.Add(paramDVal);
			paramFldDef.ApplyCurrentValues(paramVals);

			//To
			paramDVal = new ParameterDiscreteValue();
			if (Utility.IsNotDBNull(strTo))
				paramDVal.Value = strTo;
			paramFldDef = paramFldDefs["prmEstTo"];
			paramVals = paramFldDef.CurrentValues;
			paramVals.Add(paramDVal);
			paramFldDef.ApplyCurrentValues(paramVals);

			//From
			paramDVal = new ParameterDiscreteValue();
			if (Utility.IsNotDBNull(strFrom))
				paramDVal.Value = strFrom;
			paramFldDef = paramFldDefs["prmEstFrom"];
			paramVals = paramFldDef.CurrentValues;
			paramVals.Add(paramDVal);
			paramFldDef.ApplyCurrentValues(paramVals);
		}
		
		private void SetVersusReportParams()
		{
			ParameterFields  paramFields = new ParameterFields(); 
			ParameterField paramField = new ParameterField();
			ParameterFieldDefinitions paramFldDefs ;
			ParameterFieldDefinition paramFldDef ;
			ParameterValues paramVals ;
			ParameterDiscreteValue paramDVal;
			paramFldDefs = rptSource.DataDefinition.ParameterFields;
			
			//getting parameter field values
			string strApplicationId = null;
			string strEnterpriseId = null;
			String strType= null;
			DateTime strFrom = System.DateTime.ParseExact("01/01/1900", "dd/MM/yyyy", null);
			DateTime strTo = System.DateTime.ParseExact("01/01/1900", "dd/MM/yyyy", null);
			String strStatusCode1= null;
			String strStatusCode2= null;
			String strStatusCode3= null;
			String strStatusCode4= null;
			String strStatusCode5= null;
			String strStatusCode6= null;
			String strStatusCode7= null;
			String strRouteType= null;
			String strRouteCode= null;
			String strPathOriginDC= null;
			String strPathDestinationDC= null;
			String strFlag= null;

			//Read your criteria values
			DataRow drEach  = m_dsQuery.Tables[0].Rows[0];

			if (Utility.IsNotDBNull(drEach["ApplicationId"]))
				strApplicationId = (String)drEach["ApplicationId"];
			strEnterpriseId = (String)drEach["EnterpriseId"];

			if (Utility.IsNotDBNull(drEach["Type"]))
				strType = (String)drEach["Type"];

			if (Utility.IsNotDBNull(drEach["FromDate"]))
				strFrom = Convert.ToDateTime(drEach["FromDate"]);
			if (Utility.IsNotDBNull(drEach["ToDate"]))
				strTo = Convert.ToDateTime(drEach["ToDate"]);

			if (Utility.IsNotDBNull(drEach["Status1"]))
				strStatusCode1 = (String)drEach["Status1"];
			if (Utility.IsNotDBNull(drEach["Status2"]))
				strStatusCode2 = (String)drEach["Status2"];
			if (Utility.IsNotDBNull(drEach["Status3"]))
				strStatusCode3 = (String)drEach["Status3"];
			if (Utility.IsNotDBNull(drEach["Status4"]))
				strStatusCode4 = (String)drEach["Status4"];
			if (Utility.IsNotDBNull(drEach["Status5"]))
				strStatusCode5 = (String)drEach["Status5"];
			if (Utility.IsNotDBNull(drEach["Status6"]))
				strStatusCode6 = (String)drEach["Status6"];
			if (Utility.IsNotDBNull(drEach["Status7"]))
				strStatusCode7 = (String)drEach["Status7"];

			if (Utility.IsNotDBNull(drEach["RouteType"]))
				strRouteType = (String)drEach["RouteType"];
			if (Utility.IsNotDBNull(drEach["RouteCode"]))
				strRouteCode = (String)drEach["RouteCode"];

			if (Utility.IsNotDBNull(drEach["PathOriginDC"]))
				strPathOriginDC = (String)drEach["PathOriginDC"];
			if (Utility.IsNotDBNull(drEach["PathDestinationDC"]))
				strPathDestinationDC = (String)drEach["PathDestinationDC"];

			if (Utility.IsNotDBNull(drEach["Flag"]))
				strFlag = (String)drEach["Flag"];

			//ApplicationID
			paramDVal = new ParameterDiscreteValue();
			paramDVal.Value = strApplicationId;
			paramFldDef = paramFldDefs["ApplicationID"];
			paramVals = paramFldDef.CurrentValues;
			paramVals.Add(paramDVal);
			paramFldDef.ApplyCurrentValues(paramVals);

			//EnterpriseID
			paramDVal = new ParameterDiscreteValue();
			paramDVal.Value = strEnterpriseID;
			paramFldDef = paramFldDefs["EnterpriseID"];
			paramVals = paramFldDef.CurrentValues;
			paramVals.Add(paramDVal);
			paramFldDef.ApplyCurrentValues(paramVals);

			//Type
			paramDVal = new ParameterDiscreteValue();
			if (strType=="D" || strType=="P")
				paramDVal.Value = strType;
			else
				paramDVal.Value = "*";
			paramFldDef = paramFldDefs["Type"];
			paramVals = paramFldDef.CurrentValues;
			paramVals.Add(paramDVal);
			paramFldDef.ApplyCurrentValues(paramVals);

			//From
			paramDVal = new ParameterDiscreteValue();
			if (Utility.IsNotDBNull(strFrom))
				paramDVal.Value = strFrom;
			paramFldDef = paramFldDefs["FromDate"];
			paramVals = paramFldDef.CurrentValues;
			paramVals.Add(paramDVal);
			paramFldDef.ApplyCurrentValues(paramVals);

			//To
			paramDVal = new ParameterDiscreteValue();
			if (Utility.IsNotDBNull(strTo))
				paramDVal.Value = strTo;
			paramFldDef = paramFldDefs["ToDate"];
			paramVals = paramFldDef.CurrentValues;
			paramVals.Add(paramDVal);
			paramFldDef.ApplyCurrentValues(paramVals);

			// Status Code1
			paramDVal = new ParameterDiscreteValue();
			if (Utility.IsNotDBNull(strStatusCode1) && strStatusCode1!="")
				paramDVal.Value = strStatusCode1;
			else
				paramDVal.Value = "*";
			paramFldDef = paramFldDefs["Status1"];
			paramVals = paramFldDef.CurrentValues;
			paramVals.Add(paramDVal);
			paramFldDef.ApplyCurrentValues(paramVals);
			
			// Status Code2
			paramDVal = new ParameterDiscreteValue();
			if (Utility.IsNotDBNull(strStatusCode2) && strStatusCode2!="")
				paramDVal.Value = strStatusCode2;
			else
				paramDVal.Value = "*";
			paramFldDef = paramFldDefs["Status2"];
			paramVals = paramFldDef.CurrentValues;
			paramVals.Add(paramDVal);
			paramFldDef.ApplyCurrentValues(paramVals);
			
			// Status Code3
			paramDVal = new ParameterDiscreteValue();
			if (Utility.IsNotDBNull(strStatusCode3) && strStatusCode3!="")
				paramDVal.Value = strStatusCode3;
			else
				paramDVal.Value = "*";
			paramFldDef = paramFldDefs["Status3"];
			paramVals = paramFldDef.CurrentValues;
			paramVals.Add(paramDVal);
			paramFldDef.ApplyCurrentValues(paramVals);
			
			// Status Code4
			paramDVal = new ParameterDiscreteValue();
			if (Utility.IsNotDBNull(strStatusCode4) && strStatusCode4!="")
				paramDVal.Value = strStatusCode4;
			else
				paramDVal.Value = "*";
			paramFldDef = paramFldDefs["Status4"];
			paramVals = paramFldDef.CurrentValues;
			paramVals.Add(paramDVal);
			paramFldDef.ApplyCurrentValues(paramVals);
			
			// Status Code5
			paramDVal = new ParameterDiscreteValue();
			if (Utility.IsNotDBNull(strStatusCode5) && strStatusCode5!="")
				paramDVal.Value = strStatusCode5;
			else
				paramDVal.Value = "*";
			paramFldDef = paramFldDefs["Status5"];
			paramVals = paramFldDef.CurrentValues;
			paramVals.Add(paramDVal);
			paramFldDef.ApplyCurrentValues(paramVals);
			
			// Status Code6
			paramDVal = new ParameterDiscreteValue();
			if (Utility.IsNotDBNull(strStatusCode6) && strStatusCode6!="")
				paramDVal.Value = strStatusCode6;
			else
				paramDVal.Value = "*";
			paramFldDef = paramFldDefs["Status6"];
			paramVals = paramFldDef.CurrentValues;
			paramVals.Add(paramDVal);
			paramFldDef.ApplyCurrentValues(paramVals);
			
			// Status Code7
			paramDVal = new ParameterDiscreteValue();
			if (Utility.IsNotDBNull(strStatusCode7) && strStatusCode7!="")
				paramDVal.Value = strStatusCode7;
			else
				paramDVal.Value = "*";
			paramFldDef = paramFldDefs["Status7"];
			paramVals = paramFldDef.CurrentValues;
			paramVals.Add(paramDVal);
			paramFldDef.ApplyCurrentValues(paramVals);

			//RouteType
			paramDVal = new ParameterDiscreteValue();
			if (strRouteType=="A" || strRouteType=="S" || strRouteType == "L")
				paramDVal.Value = strRouteType;
			else
				paramDVal.Value = "*";
			paramFldDef = paramFldDefs["RouteType"];
			paramVals = paramFldDef.CurrentValues;
			paramVals.Add(paramDVal);
			paramFldDef.ApplyCurrentValues(paramVals);

			//Route Code
			paramDVal = new ParameterDiscreteValue();
			if (Utility.IsNotDBNull(strRouteCode) && strRouteCode!="")
				paramDVal.Value = strRouteCode;
			else
				paramDVal.Value = "*";
			paramFldDef = paramFldDefs["RouteCode"];
			paramVals = paramFldDef.CurrentValues;
			paramVals.Add(paramDVal);
			paramFldDef.ApplyCurrentValues(paramVals);

			//PathOriginDC
			paramDVal = new ParameterDiscreteValue();
			if (Utility.IsNotDBNull(strPathOriginDC) && strPathOriginDC!="")
				paramDVal.Value = strPathOriginDC;
			else
				paramDVal.Value = "*";
			paramFldDef = paramFldDefs["PathOriginDC"];
			paramVals = paramFldDef.CurrentValues;
			paramVals.Add(paramDVal);
			paramFldDef.ApplyCurrentValues(paramVals);

			//PathDestinationDC
			paramDVal = new ParameterDiscreteValue();
			if (Utility.IsNotDBNull(strPathDestinationDC) && strPathDestinationDC!="")
				paramDVal.Value = strPathDestinationDC;
			else
				paramDVal.Value = "*";
			paramFldDef = paramFldDefs["PathDestinationDC"];
			paramVals = paramFldDef.CurrentValues;
			paramVals.Add(paramDVal);
			paramFldDef.ApplyCurrentValues(paramVals);

			//Flag
			paramDVal = new ParameterDiscreteValue();
			if (strFlag=="Y")
				paramDVal.Value = strFlag;
			else
				paramDVal.Value = "*";
			paramFldDef = paramFldDefs["Flag"];
			paramVals = paramFldDef.CurrentValues;
			paramVals.Add(paramDVal);
			paramFldDef.ApplyCurrentValues(paramVals);

			//rptViewer.DisplayGroupTree = false;
		}

		
		private void SetInvoiceReturnServiceReportParams()
		{
			ParameterFields  paramFields = new ParameterFields(); 
			ParameterField paramField = new ParameterField();
			ParameterFieldDefinitions paramFldDefs ;
			ParameterFieldDefinition paramFldDef ;
			ParameterValues paramVals ;
			ParameterDiscreteValue paramDVal;
			//	paramFlds = rptSource.DataDefinition.ParameterFields;
			paramFldDefs = rptSource.DataDefinition.ParameterFields;
			
			//getting parameter field values
			DateTime strFrom = System.DateTime.ParseExact("01/01/1900", "dd/MM/yyyy", null);
			DateTime  strTo = System.DateTime.ParseExact("01/01/1900", "dd/MM/yyyy", null);
			//			String strMissFirstStatus= null;
			//			String strStatusCode1= null;
			//			String strStatusCode2= null;
			//			String strStatusCode3= null;
			//			String strStatusCode4= null;
			//			String strStatusCode5= null;
			//			String strStatusCode6= null;
			//			String strStatusCode7= null;
			//			String strHistoryType= null;
			//			String strDeliveryType= null;
			//			String strRouteCode= null;

			string strApplicationId = null;
			string strEnterpriseId = null;
			//Read your criteria values
			DataRow drEach  = m_dsQuery.Tables[0].Rows[0];
			if (Utility.IsNotDBNull(drEach["ApplicationId"]))
				strApplicationId = (String)drEach["ApplicationId"];
			strEnterpriseId = (String)drEach["EnterpriseId"];
			if (Utility.IsNotDBNull(drEach["From_Date"]))
				strFrom = Convert.ToDateTime(drEach["From_Date"]);
			if (Utility.IsNotDBNull(drEach["To_Date"]))
				strTo = Convert.ToDateTime(drEach["To_Date"]);

			//			if (Utility.IsNotDBNull(drEach["Miss_first_Status"]))
			//				strMissFirstStatus = (String)drEach["Miss_first_Status"];
			//
			//			if (Utility.IsNotDBNull(drEach["Status_Code1"]))
			//				strStatusCode1 = (String)drEach["Status_Code1"];
			//			if (Utility.IsNotDBNull(drEach["Status_Code2"]))
			//				strStatusCode2 = (String)drEach["Status_Code2"];
			//			if (Utility.IsNotDBNull(drEach["Status_Code3"]))
			//				strStatusCode3 = (String)drEach["Status_Code3"];
			//			if (Utility.IsNotDBNull(drEach["Status_Code4"]))
			//				strStatusCode4 = (String)drEach["Status_Code4"];
			//			if (Utility.IsNotDBNull(drEach["Status_Code5"]))
			//				strStatusCode5 = (String)drEach["Status_Code5"];
			//			if (Utility.IsNotDBNull(drEach["Status_Code6"]))
			//				strStatusCode6 = (String)drEach["Status_Code6"];
			//			if (Utility.IsNotDBNull(drEach["Status_Code7"]))
			//				strStatusCode7 = (String)drEach["Status_Code7"];
			//
			//			if (Utility.IsNotDBNull(drEach["History_Type"]))
			//				strHistoryType = (String)drEach["History_Type"];
			//			if (Utility.IsNotDBNull(drEach["Delivery_Type"]))
			//				strDeliveryType = (String)drEach["Delivery_Type"];
			//			if (Utility.IsNotDBNull(drEach["Route_Code"]))
			//				strRouteCode = (String)drEach["Route_Code"];

			//To
			paramDVal = new ParameterDiscreteValue();
			if (Utility.IsNotDBNull(strTo))
				paramDVal.Value = strTo;
			paramFldDef = paramFldDefs["prmEstTo"];
			paramVals = paramFldDef.CurrentValues;
			paramVals.Add(paramDVal);
			paramFldDef.ApplyCurrentValues(paramVals);
			//From
			paramDVal = new ParameterDiscreteValue();
			if (Utility.IsNotDBNull(strFrom))
				paramDVal.Value = strFrom;
			paramFldDef = paramFldDefs["prmEstFrom"];
			paramVals = paramFldDef.CurrentValues;
			paramVals.Add(paramDVal);
			paramFldDef.ApplyCurrentValues(paramVals);

			//passing parameter for parameter fields
			//ApplicationID

			paramDVal = new ParameterDiscreteValue();
			paramDVal.Value = strApplicationId;
			paramFldDef = paramFldDefs["prmAppicationId"];
			paramVals = paramFldDef.CurrentValues;
			paramVals.Add(paramDVal);
			paramFldDef.ApplyCurrentValues(paramVals);

			//EnterpriseID
			paramDVal = new ParameterDiscreteValue();
			paramDVal.Value = strEnterpriseID;
			paramFldDef = paramFldDefs["prmEnterpriseId"];
			paramVals = paramFldDef.CurrentValues;
			paramVals.Add(paramDVal);
			paramFldDef.ApplyCurrentValues(paramVals);

			//UserId
			paramDVal = new ParameterDiscreteValue();
			paramDVal.Value = strUserId;
			paramFldDef = paramFldDefs["prmUserId"];
			paramVals = paramFldDef.CurrentValues;
			paramVals.Add(paramDVal);
			paramFldDef.ApplyCurrentValues(paramVals);
		
			//			//HistoryType
			//			paramDVal = new ParameterDiscreteValue();
			//			if (strHistoryType=="D" || strHistoryType=="P")
			//				paramDVal.Value = strHistoryType;
			//			else
			//				paramDVal.Value = "*";
			//			paramFldDef = paramFldDefs["prmHistoryType"];
			//			paramVals = paramFldDef.CurrentValues;
			//			paramVals.Add(paramDVal);
			//			paramFldDef.ApplyCurrentValues(paramVals);
			//
			//			//Delivery Path Type
			//			paramDVal = new ParameterDiscreteValue();
			//			if (strDeliveryType=="A" || strDeliveryType=="S" || strDeliveryType == "L")
			//				paramDVal.Value = strDeliveryType;
			//			else
			//				paramDVal.Value = "*";
			//			paramFldDef = paramFldDefs["prmDeliveryType"];
			//			paramVals = paramFldDef.CurrentValues;
			//			paramVals.Add(paramDVal);
			//			paramFldDef.ApplyCurrentValues(paramVals);
			//
			//			//Route Code
			//			paramDVal = new ParameterDiscreteValue();
			//			if (Utility.IsNotDBNull(strRouteCode) && strRouteCode!="")
			//				paramDVal.Value = strRouteCode;
			//			else
			//				paramDVal.Value = "*";
			//			paramFldDef = paramFldDefs["prmRouteCode"];
			//			paramVals = paramFldDef.CurrentValues;
			//			paramVals.Add(paramDVal);
			//			paramFldDef.ApplyCurrentValues(paramVals);
			//
			//			//Miss the First Status
			//			paramDVal = new ParameterDiscreteValue();
			//			if (strMissFirstStatus=="Y")
			//				paramDVal.Value = strMissFirstStatus;
			//			else
			//				paramDVal.Value = "*";
			//			paramFldDef = paramFldDefs["prmMissFirstStatus"];
			//			paramVals = paramFldDef.CurrentValues;
			//			paramVals.Add(paramDVal);
			//			paramFldDef.ApplyCurrentValues(paramVals);
			//
			//			// Status Code1
			//			paramDVal = new ParameterDiscreteValue();
			//			if (Utility.IsNotDBNull(strStatusCode1) && strStatusCode1!="")
			//				paramDVal.Value = strStatusCode1;
			//			else
			//				paramDVal.Value = "*";
			//			paramFldDef = paramFldDefs["prmStatusCode1"];
			//			paramVals = paramFldDef.CurrentValues;
			//			paramVals.Add(paramDVal);
			//			paramFldDef.ApplyCurrentValues(paramVals);
			//			
			//			// Status Code2
			//			paramDVal = new ParameterDiscreteValue();
			//			if (Utility.IsNotDBNull(strStatusCode1) && strStatusCode2!="")
			//				paramDVal.Value = strStatusCode2;
			//			else
			//				paramDVal.Value = "*";
			//			paramFldDef = paramFldDefs["prmStatusCode2"];
			//			paramVals = paramFldDef.CurrentValues;
			//			paramVals.Add(paramDVal);
			//			paramFldDef.ApplyCurrentValues(paramVals);
			//			
			//			// Status Code3
			//			paramDVal = new ParameterDiscreteValue();
			//			if (Utility.IsNotDBNull(strStatusCode1) && strStatusCode3!="")
			//				paramDVal.Value = strStatusCode3;
			//			else
			//				paramDVal.Value = "*";
			//			paramFldDef = paramFldDefs["prmStatusCode3"];
			//			paramVals = paramFldDef.CurrentValues;
			//			paramVals.Add(paramDVal);
			//			paramFldDef.ApplyCurrentValues(paramVals);
			//			
			//			// Status Code4
			//			paramDVal = new ParameterDiscreteValue();
			//			if (Utility.IsNotDBNull(strStatusCode1) && strStatusCode4!="")
			//				paramDVal.Value = strStatusCode4;
			//			else
			//				paramDVal.Value = "*";
			//			paramFldDef = paramFldDefs["prmStatusCode4"];
			//			paramVals = paramFldDef.CurrentValues;
			//			paramVals.Add(paramDVal);
			//			paramFldDef.ApplyCurrentValues(paramVals);
			//			
			//			// Status Code5
			//			paramDVal = new ParameterDiscreteValue();
			//			if (Utility.IsNotDBNull(strStatusCode5) && strStatusCode5!="")
			//				paramDVal.Value = strStatusCode5;
			//			else
			//				paramDVal.Value = "*";
			//			paramFldDef = paramFldDefs["prmStatusCode5"];
			//			paramVals = paramFldDef.CurrentValues;
			//			paramVals.Add(paramDVal);
			//			paramFldDef.ApplyCurrentValues(paramVals);
			//			
			//			// Status Code6
			//			paramDVal = new ParameterDiscreteValue();
			//			if (Utility.IsNotDBNull(strStatusCode6) && strStatusCode6!="")
			//				paramDVal.Value = strStatusCode6;
			//			else
			//				paramDVal.Value = "*";
			//			paramFldDef = paramFldDefs["prmStatusCode6"];
			//			paramVals = paramFldDef.CurrentValues;
			//			paramVals.Add(paramDVal);
			//			paramFldDef.ApplyCurrentValues(paramVals);
			//			
			//			// Status Code7
			//			paramDVal = new ParameterDiscreteValue();
			//			if (Utility.IsNotDBNull(strStatusCode7) && strStatusCode7!="")
			//				paramDVal.Value = strStatusCode7;
			//			else
			//				paramDVal.Value = "*";
			//			paramFldDef = paramFldDefs["prmStatusCode7"];
			//			paramVals = paramFldDef.CurrentValues;
			//			paramVals.Add(paramDVal);
			//			paramFldDef.ApplyCurrentValues(paramVals);

	

			
		
			//rptViewer.DisplayGroupTree = false;
		}


		private void SetShipTrackParams()
		{
			//declare instance for parameterfields class
			ParameterValues paramvals = new ParameterValues();
			ParameterFieldDefinitions paramFlds; 
			//ParameterFieldDefinition paramFld=null; 
			ParameterDiscreteValue paramDiscVal = new ParameterDiscreteValue();
			ParameterRangeValue paramRangeVal = new ParameterRangeValue();

			//getting parameter field values
			String strPayerId="*", strPayerType="";
			String strDateType="";
			String strRouteCode="*", strRouteType="";
			String strZipCode="*", strStateCode="*";
			String strorigin_dc="*",strdestination_dc="*";
			String strdelPathorigin_dc="*",strdelPathdestination_dc="*";
			String strBookingType="*";
			String strConsgNo="*", strStatusCode="*", strExcepCode="*", strShipClosed="*", strShipInvoiced="*";
			//Added By Tom 24/7/09
			String strTrackAll = "N";
			//End Added By Tom 24/7/09
			long lBookingNo=0;
			DateTime dtStartDate, dtEndDate;
			
			DataRow dr = m_dsQuery.Tables[0].Rows[0];
			strDateType = dr["tran_date"].ToString();
			if (Utility.IsNotDBNull(dr["start_date"])) 
				dtStartDate = (DateTime) dr["start_date"];
			else
				dtStartDate = System.DateTime.ParseExact("01/01/1900", "dd/MM/yyyy", null);
			if (Utility.IsNotDBNull(dr["end_date"])) 
				dtEndDate = (DateTime) dr["end_date"];
			else
				dtEndDate = System.DateTime.ParseExact("01/01/1900", "dd/MM/yyyy", null);

			if (Utility.IsNotDBNull(dr["payer_type"]))
				strPayerType = dr["payer_type"].ToString();
			if (Utility.IsNotDBNull(dr["payer_code"]))
				strPayerId = dr["payer_code"].ToString();

			if (Utility.IsNotDBNull(dr["route_type"]))
				strRouteType = dr["route_type"].ToString();
			if (Utility.IsNotDBNull(dr["route_code"]))
				strRouteCode = dr["route_code"].ToString();

			if (Utility.IsNotDBNull(dr["zip_code"]))
				strZipCode = dr["zip_code"].ToString();
			if (Utility.IsNotDBNull(dr["state_code"]))
				strStateCode = dr["state_code"].ToString();

			if (Utility.IsNotDBNull(dr["delPath_origin_dc"]))
				strdelPathorigin_dc = dr["delPath_origin_dc"].ToString();
			if (Utility.IsNotDBNull(dr["delPath_destination_dc"]))
				strdelPathdestination_dc = dr["delPath_destination_dc"].ToString();

			if (Utility.IsNotDBNull(dr["origin_dc"]))
				strorigin_dc = dr["origin_dc"].ToString();
			if (Utility.IsNotDBNull(dr["destination_dc"]))
				strdestination_dc = dr["destination_dc"].ToString();

			if (Utility.IsNotDBNull(dr["booking_type"]))
			{
				if(dr["booking_type"].ToString().Trim() != "0")
					strBookingType = dr["booking_type"].ToString();
			}
			if (Utility.IsNotDBNull(dr["consignment_no"]))
				strConsgNo = dr["consignment_no"].ToString();
			if (Utility.IsNotDBNull(dr["booking_no"]) && dr["booking_no"].ToString().Trim()!="")
				lBookingNo = Convert.ToInt64(dr["booking_no"]);
			if (Utility.IsNotDBNull(dr["status_code"]))
				strStatusCode = dr["status_code"].ToString();
			if (Utility.IsNotDBNull(dr["exception_code"]))
				strExcepCode = dr["exception_code"].ToString();
			if (Utility.IsNotDBNull(dr["shipment_closed"]))
				strShipClosed = dr["shipment_closed"].ToString();
			if (Utility.IsNotDBNull(dr["shipment_invoiced"]))
				strShipInvoiced = dr["shipment_invoiced"].ToString();
			
			//get all parameter fields
			paramFlds = rptSource.DataDefinition.ParameterFields;

			//passing parameter for parameter fields
			foreach(ParameterFieldDefinition paramFld in paramFlds)
			{
				switch(paramFld.ParameterFieldName)
				{
					case "p_appid":
						paramDiscVal.Value = strAppID;
						break;
					case "p_enterprise":
						paramDiscVal.Value = strEnterpriseID;
						break;
					case "p_consignment_no":
						paramDiscVal.Value = strConsgNo;
						break;
					case "p_booking_no":
						paramDiscVal.Value = lBookingNo;
						break;
					case "p_payer_type":
						paramDiscVal.Value = strPayerType;
						break;
					case "p_payerid":
						paramDiscVal.Value = strPayerId;
						break;
					case "p_date_type":
						paramDiscVal.Value = strDateType;
						break;
					case "p_date_range":
						paramRangeVal.StartValue = dtStartDate;
						paramRangeVal.EndValue = dtEndDate;
						break;
					case "p_route_code":
						paramDiscVal.Value = strRouteCode;
						break;
					case "p_zipcode":
						paramDiscVal.Value = strZipCode;
						break;
					case "p_state_code":
						paramDiscVal.Value = strStateCode;
						break;
					case "p_last_status_code":
						paramDiscVal.Value = strStatusCode;
						break;
					case "p_last_exception_code":
						paramDiscVal.Value = strExcepCode;
						break;
					case "p_invoice_flag":
						paramDiscVal.Value = strShipInvoiced;
						break;
					case "p_userid":
						paramDiscVal.Value = utility.GetUserID();
						break;
					case "p_origin_dc":
						paramDiscVal.Value = strorigin_dc;
						break;
					case "p_destination_dc":
						paramDiscVal.Value = strdestination_dc;
						break;
					case "p_route_type":
						paramDiscVal.Value = strRouteType;
						break;
					case "p_delPath_origin_dc":
						paramDiscVal.Value = strdelPathorigin_dc;
						break;
					case "p_delPath_destination_dc":
						paramDiscVal.Value = strdelPathdestination_dc;
						break;
					case "p_bookingtype":
						paramDiscVal.Value = strBookingType;
						break;
//					case "p_trackall":
//						paramDiscVal.Value = strTrackAll;
//						break;
					default:
						continue;
				}
				paramvals = paramFld.CurrentValues;
				if (paramFld.DiscreteOrRangeKind == DiscreteOrRangeKind.RangeValue)
					paramvals.Add(paramRangeVal);
				else 
					paramvals.Add(paramDiscVal);
				paramFld.ApplyCurrentValues(paramvals);
			}
		}

		private void CustomerLastShipmentDateParams()
		{
			ParameterValues paramvals = new ParameterValues();
			ParameterFieldDefinitions paramFlds; 
			ParameterDiscreteValue paramDiscVal = new ParameterDiscreteValue();
			ParameterRangeValue paramRangeVal = new ParameterRangeValue();
			
			
			DataRow dr = m_dsQuery.Tables[0].Rows[0];

			string strYears="";
			string strMonths="";
			DateTime Dates;

			strYears = dr["SMonths"].ToString();
			strMonths =dr["EMonths"].ToString();

			//Add By Tom 18/5/09
			string strYear="";
			strYear =dr["SeYear"].ToString();
			//End Add By Tom

			//Dates = System.DateTime.ParseExact("01/01/1900", "dd/MM/yyyy", null);dr["Dates"].ToString(); 
			

			if (Utility.IsNotDBNull(dr["Dates"])) 
				Dates = System.DateTime.ParseExact(dr["Dates"].ToString(), "dd/MM/yyyy", null);
			else
				Dates = System.DateTime.ParseExact("01/01/1900", "dd/MM/yyyy", null);
			

			paramFlds = rptSource.DataDefinition.ParameterFields;

			foreach(ParameterFieldDefinition paramFld in paramFlds)
			{
				switch(paramFld.ParameterFieldName)
				{				
						//					case "dates":
						//						paramDiscVal.Value = Dates;
						//						break;
					case "Smonth":
						paramDiscVal.Value = strYears;
						break;
					case "Emonth":
						paramDiscVal.Value = strMonths;
						break;
					case "prmUserId":
						paramDiscVal.Value = utility.GetUserID();
						break; 
					//Add By Tom 18/5/09
					case "SeYear":
						paramDiscVal.Value = strYear;
						break; 
					//End Add By Tom
					default:
						continue;
				}

				paramvals = paramFld.CurrentValues;
				paramvals.Add(paramDiscVal);
				paramFld.ApplyCurrentValues(paramvals);

			}
		}

		private void SetShipmentNotInvoiceParams()
		{
			ParameterValues paramvals = new ParameterValues();
			ParameterFieldDefinitions paramFlds; 
			ParameterDiscreteValue paramDiscVal = new ParameterDiscreteValue();
			ParameterRangeValue paramRangeVal = new ParameterRangeValue();

			String strPayerId="*",strPayerType="";
			String strDateType="";			
			String strZipCode="*", strStateCode="*";
			
			long lBookingNo=0;
			DateTime dtStartDate, dtEndDate;
			
			DataRow dr = m_dsQuery.Tables[0].Rows[0];
			strDateType = dr["tran_date"].ToString();
			if (Utility.IsNotDBNull(dr["start_date"])) 
				dtStartDate = (DateTime) dr["start_date"];
			else
				dtStartDate = System.DateTime.ParseExact("01/01/1900", "dd/MM/yyyy", null);
			if (Utility.IsNotDBNull(dr["end_date"])) 
				dtEndDate = (DateTime) dr["end_date"];
			else
				dtEndDate = System.DateTime.ParseExact("01/01/1900", "dd/MM/yyyy", null);
			if (Utility.IsNotDBNull(dr["payer_type"]))
				strPayerType = dr["payer_type"].ToString();
			if (Utility.IsNotDBNull(dr["payer_code"]))
				strPayerId = dr["payer_code"].ToString();
			if (Utility.IsNotDBNull(dr["zip_code"]))
				strZipCode = dr["zip_code"].ToString();
			if (Utility.IsNotDBNull(dr["state_code"]))
				strStateCode = dr["state_code"].ToString();
			if (Utility.IsNotDBNull(dr["booking_no"]) && dr["booking_no"].ToString().Trim()!="")
				lBookingNo = Convert.ToInt64(dr["booking_no"]);
			paramFlds = rptSource.DataDefinition.ParameterFields;

			foreach(ParameterFieldDefinition paramFld in paramFlds)
			{
				switch(paramFld.ParameterFieldName)
				{				
					case "ApplicationID":
						paramDiscVal.Value = strAppID;
						break;
					case "EnterpriseID":
						paramDiscVal.Value = strEnterpriseID;
						break;
					case "p_payer_type":
						paramDiscVal.Value = strPayerType;
						break;
					case "p_payerid":
						paramDiscVal.Value = strPayerId;
						break;
					case "DateType":
						paramDiscVal.Value = strDateType;
						break;
					case "p_userid":
						paramDiscVal.Value = utility.GetUserID();
						break; 
					case "FromDate":
						paramDiscVal.Value = dtStartDate;
						break;
					case "ToDate":
						paramDiscVal.Value = dtEndDate;
						break;
					default:
						continue;
				}

				paramvals = paramFld.CurrentValues;
				paramvals.Add(paramDiscVal);
				paramFld.ApplyCurrentValues(paramvals);

			}

		}
		//Added By Tom May 14, 2010 
		private void SetShipmentNotRetrieveParams()
		{
			//declare instance for parameterfields class
			ParameterValues paramvals = new ParameterValues();
			ParameterFieldDefinitions paramFlds; 
			ParameterDiscreteValue paramDiscVal = new ParameterDiscreteValue();

			//getting parameter field values
			String strDateType="";
			DateTime dtStartDate, dtEndDate;
			String strorigin_dc="*";
			String strdestination_dc="*";
			
			DataRow dr = m_dsQuery.Tables[0].Rows[0];

			strDateType = dr["tran_date"].ToString();
			if (Utility.IsNotDBNull(dr["start_date"])) 
				dtStartDate = (DateTime) dr["start_date"];
			else
				dtStartDate = System.DateTime.ParseExact("01/01/1900", "dd/MM/yyyy", null);
			if (Utility.IsNotDBNull(dr["end_date"])) 
				dtEndDate = (DateTime) dr["end_date"];
			else
				dtEndDate = System.DateTime.ParseExact("01/01/1900", "dd/MM/yyyy", null);

			if (Utility.IsNotDBNull(dr["origin_dc"]))
				strorigin_dc = dr["origin_dc"].ToString();
			if (Utility.IsNotDBNull(dr["destination_dc"]))
				strdestination_dc = dr["destination_dc"].ToString();

			//get all parameter fields
			paramFlds = rptSource.DataDefinition.ParameterFields;

			//passing parameter for parameter fields
			foreach(ParameterFieldDefinition paramFld in paramFlds)
			{
				switch(paramFld.ParameterFieldName)
				{
					case "ApplicationID":
						paramDiscVal.Value = strAppID;
						break;
					case "EnterpriseID":
						paramDiscVal.Value = strEnterpriseID;
						break;
					case "DateType":
						paramDiscVal.Value = strDateType;
						break;
					case "FromDate":
						paramDiscVal.Value = dtStartDate;
						break;
					case "ToDate":
						paramDiscVal.Value = dtEndDate;
						break;
					case "OriginDC":
						paramDiscVal.Value = strorigin_dc;
						break;
					case "DestinationDC":
						paramDiscVal.Value = strdestination_dc;
						break;
					case "prmUserId":
						paramDiscVal.Value = strUserId; 
						break;

						//case "Flag":
						//paramDiscVal.Value = strFlag;
						//break;
					default:
						continue;
				}
				paramvals = paramFld.CurrentValues;
				paramvals.Add(paramDiscVal);
				paramFld.ApplyCurrentValues(paramvals);
			}
		}
		//End Added By Tom May 14, 2010
		//Added By Tom jun 7, 2010 
		private void SetShipmentNotRetrieveParamsNoMDE()
		{
			//declare instance for parameterfields class
			ParameterValues paramvals = new ParameterValues();
			ParameterFieldDefinitions paramFlds; 
			ParameterDiscreteValue paramDiscVal = new ParameterDiscreteValue();

			//getting parameter field values
			String strDateType="";
			DateTime dtStartDate, dtEndDate;
			//String strorigin_dc="*";
			//String strdestination_dc="*";
			String strorigin_dc="";
			String strdestination_dc="";
			
			DataRow dr = m_dsQuery.Tables[0].Rows[0];

			strDateType = dr["tran_date"].ToString();
			if (Utility.IsNotDBNull(dr["start_date"])) 
				dtStartDate = (DateTime) dr["start_date"];
			else
				dtStartDate = System.DateTime.ParseExact("01/01/1900", "dd/MM/yyyy", null);
			if (Utility.IsNotDBNull(dr["end_date"])) 
				dtEndDate = (DateTime) dr["end_date"];
			else
				dtEndDate = System.DateTime.ParseExact("01/01/1900", "dd/MM/yyyy", null);

			if (Utility.IsNotDBNull(dr["origin_dc"]))
				strorigin_dc = dr["origin_dc"].ToString();
			if (Utility.IsNotDBNull(dr["destination_dc"]))
				strdestination_dc = dr["destination_dc"].ToString();

			//get all parameter fields
			paramFlds = rptSource.DataDefinition.ParameterFields;

			//passing parameter for parameter fields
			foreach(ParameterFieldDefinition paramFld in paramFlds)
			{
				switch(paramFld.ParameterFieldName)
				{
						
					//case "ApplicationID":
					//	paramDiscVal.Value = strAppID;
					//	break;
					//case "EnterpriseID":
					//	paramDiscVal.Value = strEnterpriseID;
					//	break;
					#region Add by Sompote 2010-07-02: Filter NoMDErpt Report					
					case "DateType":
						paramDiscVal.Value = strDateType;
						break;
					case "FromDate":
						paramDiscVal.Value = dtStartDate;
						break;
					case "ToDate":
						paramDiscVal.Value = dtEndDate;
						break;
					case "OriginDC":
						paramDiscVal.Value = strorigin_dc;
						break;
					case "DestinationDC":
						paramDiscVal.Value = strdestination_dc;
						break;
					#endregion
					case "prmUserID":
						paramDiscVal.Value = strUserId; 
						break;

						//case "Flag":
						//paramDiscVal.Value = strFlag;
						//break;
					default:
						continue;
				}
				paramvals = paramFld.CurrentValues;
				paramvals.Add(paramDiscVal);
				paramFld.ApplyCurrentValues(paramvals);
			}
		}
		//End Added By Tom jun 7, 2010
		//Added By Tom Jun 11, 2010
		private void SetLHWeightVolumeParam()
		{
			//declare instance for parameterfields class
			ParameterValues paramvals = new ParameterValues();
			ParameterFieldDefinitions paramFlds; 
			ParameterDiscreteValue paramDiscVal = new ParameterDiscreteValue();

			//getting parameter field values
			String strDateType="";
			DateTime dtStartDate, dtEndDate;
			String strPayerType="";
			String strPayerId="*"; 
			String strRouteType=""; 
			String strRouteCode="*";
			String strorigin_dc="*";
			String strdestination_dc="*";
			String strdelPathorigin_dc="*";
			String strdelPathdestination_dc="*";
			String strZipCode="*";
			String strStateCode="*";
			String strSaleManID="*";
			String strServiceType="*";
			String strFlag="";
			String strDiscount_Band="";
			String strMaster_account="";
			
			DataRow dr = m_dsQuery.Tables[0].Rows[0];

			strDateType = dr["tran_date"].ToString();
			if (Utility.IsNotDBNull(dr["start_date"])) 
				dtStartDate = (DateTime) dr["start_date"];
			else
				dtStartDate = System.DateTime.ParseExact("01/01/1900", "dd/MM/yyyy", null);
			if (Utility.IsNotDBNull(dr["end_date"])) 
				dtEndDate = (DateTime) dr["end_date"];
			else
				dtEndDate = System.DateTime.ParseExact("01/01/1900", "dd/MM/yyyy", null);

			if (Utility.IsNotDBNull(dr["payer_type"]))
				strPayerType = dr["payer_type"].ToString();
			if (Utility.IsNotDBNull(dr["payer_code"]))
				strPayerId = dr["payer_code"].ToString();
			if (strFORMID == "SalesTerritoryReportQueryRev")
			{
				if (Utility.IsNotDBNull(dr["discount_band"]))
					strDiscount_Band = dr["discount_band"].ToString();
				if (Utility.IsNotDBNull(dr["master_account"]))
					strMaster_account = dr["master_account"].ToString();
			}

			if (Utility.IsNotDBNull(dr["route_type"]))
				strRouteType = dr["route_type"].ToString();
			if (Utility.IsNotDBNull(dr["route_code"]))
				strRouteCode = dr["route_code"].ToString();

			if (Utility.IsNotDBNull(dr["zip_code"]))
				strZipCode = dr["zip_code"].ToString();
			if (Utility.IsNotDBNull(dr["state_code"]))
				strStateCode = dr["state_code"].ToString();

			if (Utility.IsNotDBNull(dr["origin_dc"]))
				strorigin_dc = dr["origin_dc"].ToString();
			if (Utility.IsNotDBNull(dr["destination_dc"]))
				strdestination_dc = dr["destination_dc"].ToString();

			if (Utility.IsNotDBNull(dr["delPath_origin_dc"]))
				strdelPathorigin_dc = dr["delPath_origin_dc"].ToString();
			if (Utility.IsNotDBNull(dr["delPath_destination_dc"]))
				strdelPathdestination_dc = dr["delPath_destination_dc"].ToString();

			if (Utility.IsNotDBNull(dr["sale_id"]))
				strSaleManID = dr["sale_id"].ToString();

			if (Utility.IsNotDBNull(dr["service_type"]))
				strServiceType = dr["service_type"].ToString();

			if (Utility.IsNotDBNull(dr["flag"]))
				strFlag = dr["flag"].ToString();

			//get all parameter fields
			paramFlds = rptSource.DataDefinition.ParameterFields;

			//passing parameter for parameter fields
			foreach(ParameterFieldDefinition paramFld in paramFlds)
			{
				switch(paramFld.ParameterFieldName)
				{
					case "DateType":
						paramDiscVal.Value = strDateType;
						break;
					case "FromDate":
						paramDiscVal.Value = dtStartDate;
						break;
					case "ToDate":
						paramDiscVal.Value = dtEndDate;
						break;
					case "RouteCode":
						paramDiscVal.Value = strRouteCode;
						break;
					case "OriginDC":
						paramDiscVal.Value = strorigin_dc;
						break;
					case "DestinationDC":
						paramDiscVal.Value= strdestination_dc ;
						break;
					case "PathOriginDC":
						paramDiscVal.Value = strdelPathorigin_dc;
						break;
					case "PathDestinationDC":
						paramDiscVal.Value = strdelPathdestination_dc;
						break;

					case "prmUserId":
						paramDiscVal.Value = strUserId; 
						break;

						//case "Flag":
						//paramDiscVal.Value = strFlag;
						//break;
					default:
						continue;
				}
				paramvals = paramFld.CurrentValues;
				paramvals.Add(paramDiscVal);
				paramFld.ApplyCurrentValues(paramvals);
			}
		}
		//End Added By Tom Jun 11, 2010
		private void SetRevenueCostParams()
		{
			//declare instance for parameterfields class
			ParameterValues paramvals = new ParameterValues();
			ParameterFieldDefinitions paramFlds; 
			ParameterDiscreteValue paramDiscVal = new ParameterDiscreteValue();

			//getting parameter field values
			String strDateType="";
			DateTime dtStartDate, dtEndDate;
			String strPayerType="";
			String strPayerId="*"; 
			String strRouteType=""; 
			String strRouteCode="*";
			String strorigin_dc="*";
			String strdestination_dc="*";
			String strdelPathorigin_dc="*";
			String strdelPathdestination_dc="*";
			String strZipCode="*";
			String strStateCode="*";
			String strSaleManID="*";
			String strServiceType="*";
			String strFlag="";
			String strDiscount_Band="";
			String strMaster_account="";
			String strPickup_RoutePath ="*";
			String strPickup_ZipCode ="*";
			String strPickup_State="*";
			
			DataRow dr = m_dsQuery.Tables[0].Rows[0];

			strDateType = dr["tran_date"].ToString();
			if (Utility.IsNotDBNull(dr["start_date"])) 
				dtStartDate = (DateTime) dr["start_date"];
			else
				dtStartDate = System.DateTime.ParseExact("01/01/1900", "dd/MM/yyyy", null);
			if (Utility.IsNotDBNull(dr["end_date"])) 
				dtEndDate = (DateTime) dr["end_date"];
			else
				dtEndDate = System.DateTime.ParseExact("01/01/1900", "dd/MM/yyyy", null);

			if (Utility.IsNotDBNull(dr["payer_type"]))
				strPayerType = dr["payer_type"].ToString();
			if (Utility.IsNotDBNull(dr["payer_code"]))
				strPayerId = dr["payer_code"].ToString();
			if (strFORMID == "SalesTerritoryReportQueryRev")
			{
				if (Utility.IsNotDBNull(dr["discount_band"]))
					strDiscount_Band = dr["discount_band"].ToString();
				if (Utility.IsNotDBNull(dr["master_account"]))
					strMaster_account = dr["master_account"].ToString();
			}
			if (strFORMID == "SalesTerritoryReportQueryRevTotal")
			{
				if (Utility.IsNotDBNull(dr["discount_band"]))
					strDiscount_Band = dr["discount_band"].ToString();
				if (Utility.IsNotDBNull(dr["master_account"]))
					strMaster_account = dr["master_account"].ToString();
			//Jeab 09 Mar 11
				if (Utility.IsNotDBNull(dr["picup_route_code"]))
					strPickup_RoutePath = dr["picup_route_code"].ToString();

				if (Utility.IsNotDBNull(dr["pickup_zip_code"]))
					strPickup_ZipCode = dr["pickup_zip_code"].ToString();

				if (Utility.IsNotDBNull(dr["pickup_state_code"] ))
					strPickup_State = dr["pickup_state_code"].ToString();
			//Jeab 09 Mar 11  =========> End
			}

			if (Utility.IsNotDBNull(dr["route_type"]))
				strRouteType = dr["route_type"].ToString();
			if (Utility.IsNotDBNull(dr["route_code"]))
				strRouteCode = dr["route_code"].ToString();

			if (Utility.IsNotDBNull(dr["zip_code"]))
				strZipCode = dr["zip_code"].ToString();
			if (Utility.IsNotDBNull(dr["state_code"]))
				strStateCode = dr["state_code"].ToString();

			if (Utility.IsNotDBNull(dr["origin_dc"]))
				strorigin_dc = dr["origin_dc"].ToString();
			if (Utility.IsNotDBNull(dr["destination_dc"]))
				strdestination_dc = dr["destination_dc"].ToString();

			if (Utility.IsNotDBNull(dr["delPath_origin_dc"]))
				strdelPathorigin_dc = dr["delPath_origin_dc"].ToString();
			if (Utility.IsNotDBNull(dr["delPath_destination_dc"]))
				strdelPathdestination_dc = dr["delPath_destination_dc"].ToString();

			if (Utility.IsNotDBNull(dr["sale_id"]))
				strSaleManID = dr["sale_id"].ToString();

			if (Utility.IsNotDBNull(dr["service_type"]))
				strServiceType = dr["service_type"].ToString();

			if (Utility.IsNotDBNull(dr["flag"]))
				strFlag = dr["flag"].ToString();
			if (strFORMID == "SalesTerritoryReportQueryRev")
			{
			if (Utility.IsNotDBNull(dr["picup_route_code"]))
				strPickup_RoutePath = dr["picup_route_code"].ToString();

			if (Utility.IsNotDBNull(dr["pickup_zip_code"]))
				strPickup_ZipCode = dr["pickup_zip_code"].ToString();

			if (Utility.IsNotDBNull(dr["pickup_state_code"] ))
				strPickup_State = dr["pickup_state_code"].ToString();
			}
			//get all parameter fields
			paramFlds = rptSource.DataDefinition.ParameterFields;

			//passing parameter for parameter fields
			foreach(ParameterFieldDefinition paramFld in paramFlds)
			{
				switch(paramFld.ParameterFieldName)
				{
					case "ApplicationID":
						paramDiscVal.Value = strAppID;
						break;
					case "EnterpriseID":
						paramDiscVal.Value = strEnterpriseID;
						break;
					case "DateType":
						paramDiscVal.Value = strDateType;
						break;
					case "FromDate":
						paramDiscVal.Value = dtStartDate;
						break;
					case "ToDate":
						paramDiscVal.Value = dtEndDate;
						break;
					case "PayerType":
						paramDiscVal.Value = strPayerType;
						break;
					case "PayerCode":
						paramDiscVal.Value = strPayerId;
						break;
					case "DisCountBand":
						if (strFORMID == "SalesTerritoryReportQueryRev" || strFORMID == "SalesTerritoryReportQueryRevTotal")
							paramDiscVal.Value = strDiscount_Band;
						break;
					case "MasterAccount":
						if (strFORMID == "SalesTerritoryReportQueryRev" || strFORMID == "SalesTerritoryReportQueryRevTotal")
							paramDiscVal.Value = strMaster_account;
						break;
					case "RouteType":
						paramDiscVal.Value = strRouteType;
						break;
					case "RouteCode":
						paramDiscVal.Value = strRouteCode;
						break;
					case "OriginDC":
						paramDiscVal.Value = strorigin_dc;
						break;
					case "DestinationDC":
						paramDiscVal.Value = strdestination_dc;
						break;
					case "PathOriginDC":
						paramDiscVal.Value = strdelPathorigin_dc;
						break;
					case "PathDestinationDC":
						paramDiscVal.Value = strdelPathdestination_dc;
						break;
					case "PostalCode":
						paramDiscVal.Value = strZipCode;
						break;
					case "DestinationState":
						paramDiscVal.Value = strStateCode;
						break;
					case "SalesID":
						paramDiscVal.Value = strSaleManID;
						break;
					case "ServiceType":
						paramDiscVal.Value = strServiceType;
						break;

					case "prmUserId":
						paramDiscVal.Value = strUserId; 
						break;
					case "PickupRouteCode":
						paramDiscVal.Value= strPickup_RoutePath;
						break;
					case "PickupZipCode":
						paramDiscVal.Value= strPickup_ZipCode;
						break;
					case "PickupState":
						paramDiscVal.Value= strPickup_State;
						break;
						//case "Flag":
						//paramDiscVal.Value = strFlag;
						//break;
					default:
						continue;
				}
				paramvals = paramFld.CurrentValues;
				paramvals.Add(paramDiscVal);
				paramFld.ApplyCurrentValues(paramvals);
			}
		}

		//Added By Tom 27/7/09
		private void SetConsigneeParams()
		{
			//declare instance for parameterfields class
			ParameterValues paramvals = new ParameterValues();
			ParameterFieldDefinitions paramFlds; 
			ParameterDiscreteValue paramDiscVal = new ParameterDiscreteValue();

			//getting parameter field values
			String strDateType="";
			DateTime dtStartDate, dtEndDate;
			String strPayerType="";
			String strPayerId="*"; 
			String strZipCode="*";
			String strStateCode="*";
			
			DataRow dr = m_dsQuery.Tables[0].Rows[0];

			strDateType = dr["tran_date"].ToString();
			if (Utility.IsNotDBNull(dr["start_date"])) 
				dtStartDate = (DateTime) dr["start_date"];
			else
				dtStartDate = System.DateTime.ParseExact("01/01/1900", "dd/MM/yyyy", null);
			if (Utility.IsNotDBNull(dr["end_date"])) 
				dtEndDate = (DateTime) dr["end_date"];
			else
				dtEndDate = System.DateTime.ParseExact("01/01/1900", "dd/MM/yyyy", null);

			if (Utility.IsNotDBNull(dr["payer_type"]))
				strPayerType = dr["payer_type"].ToString();
			if (Utility.IsNotDBNull(dr["payer_code"]))
				strPayerId = dr["payer_code"].ToString();

			if (Utility.IsNotDBNull(dr["zip_code"]))
				strZipCode = dr["zip_code"].ToString();
			if (Utility.IsNotDBNull(dr["state_code"]))
				strStateCode = dr["state_code"].ToString();

			//get all parameter fields
			paramFlds = rptSource.DataDefinition.ParameterFields;

			//passing parameter for parameter fields
			foreach(ParameterFieldDefinition paramFld in paramFlds)
			{
				switch(paramFld.ParameterFieldName)
				{
					case "ApplicationID":
						paramDiscVal.Value = strAppID;
						break;
					case "EnterpriseID":
						paramDiscVal.Value = strEnterpriseID;
						break;
					case "DateType":
						paramDiscVal.Value = strDateType;
						break;
					case "FromDate":
						paramDiscVal.Value = dtStartDate;
						break;
					case "ToDate":
						paramDiscVal.Value = dtEndDate;
						break;
					case "PayerType":
						paramDiscVal.Value = strPayerType;
						break;
					case "PayerCode":
						paramDiscVal.Value = strPayerId;
						break;
					case "PostalCode":
						paramDiscVal.Value = strZipCode;
						break;
					case "DestinationState":
						paramDiscVal.Value = strStateCode;
						break;
					case "prmUserId":
						paramDiscVal.Value = strUserId; 
						break;

						//case "Flag":
						//paramDiscVal.Value = strFlag;
						//break;
					default:
						continue;
				}
				paramvals = paramFld.CurrentValues;
				paramvals.Add(paramDiscVal);
				paramFld.ApplyCurrentValues(paramvals);
			}
		}
		//End Added By Tom 27/7/09

		//		private void SetRevenueCostParams()
		//		{
		//			//declare instance for parameterfields class
		//			ParameterValues paramvals = new ParameterValues();
		//			ParameterFieldDefinitions paramFlds; 
		//			ParameterDiscreteValue paramDiscVal = new ParameterDiscreteValue();
		//
		//			//getting parameter field values
		//			String strDateType="";
		//			DateTime dtStartDate, dtEndDate;
		//			String strPayerType="";
		//			String strPayerId="*"; 
		//			String strRouteType=""; 
		//			String strRouteCode="*";
		//			String strorigin_dc="*";
		//			String strdestination_dc="*";
		//			String strdelPathorigin_dc="*";
		//			String strdelPathdestination_dc="*";
		//			String strZipCode="*";
		//			String strStateCode="*";
		//			String strSaleManID="*";
		//			String strServiceType="*";
		//			String strFlag="";
		//			
		//			DataRow dr = m_dsQuery.Tables[0].Rows[0];
		//
		//			strDateType = dr["tran_date"].ToString();
		//			if (Utility.IsNotDBNull(dr["start_date"])) 
		//				dtStartDate = (DateTime) dr["start_date"];
		//			else
		//				dtStartDate = System.DateTime.ParseExact("01/01/1900", "dd/MM/yyyy", null);
		//			if (Utility.IsNotDBNull(dr["end_date"])) 
		//				dtEndDate = (DateTime) dr["end_date"];
		//			else
		//				dtEndDate = System.DateTime.ParseExact("01/01/1900", "dd/MM/yyyy", null);
		//
		//			if (Utility.IsNotDBNull(dr["payer_type"]))
		//				strPayerType = dr["payer_type"].ToString();
		//			if (Utility.IsNotDBNull(dr["payer_code"]))
		//				strPayerId = dr["payer_code"].ToString();
		//
		//			if (Utility.IsNotDBNull(dr["route_type"]))
		//				strRouteType = dr["route_type"].ToString();
		//			if (Utility.IsNotDBNull(dr["route_code"]))
		//				strRouteCode = dr["route_code"].ToString();
		//
		//			if (Utility.IsNotDBNull(dr["zip_code"]))
		//				strZipCode = dr["zip_code"].ToString();
		//			if (Utility.IsNotDBNull(dr["state_code"]))
		//				strStateCode = dr["state_code"].ToString();
		//
		//			if (Utility.IsNotDBNull(dr["origin_dc"]))
		//				strorigin_dc = dr["origin_dc"].ToString();
		//			if (Utility.IsNotDBNull(dr["destination_dc"]))
		//				strdestination_dc = dr["destination_dc"].ToString();
		//
		//			if (Utility.IsNotDBNull(dr["delPath_origin_dc"]))
		//				strdelPathorigin_dc = dr["delPath_origin_dc"].ToString();
		//			if (Utility.IsNotDBNull(dr["delPath_destination_dc"]))
		//				strdelPathdestination_dc = dr["delPath_destination_dc"].ToString();
		//
		//			if (Utility.IsNotDBNull(dr["sale_id"]))
		//				strSaleManID = dr["sale_id"].ToString();
		//
		//			if (Utility.IsNotDBNull(dr["service_type"]))
		//				strServiceType = dr["service_type"].ToString();
		//
		//			if (Utility.IsNotDBNull(dr["flag"]))
		//				strFlag = dr["flag"].ToString();
		//
		//			//get all parameter fields
		//			paramFlds = rptSource.DataDefinition.ParameterFields;
		//
		//			//passing parameter for parameter fields
		//			foreach(ParameterFieldDefinition paramFld in paramFlds)
		//			{
		//				switch(paramFld.ParameterFieldName)
		//				{
		//					case "ApplicationID":
		//						paramDiscVal.Value = strAppID;
		//						break;
		//					case "EnterpriseID":
		//						paramDiscVal.Value = strEnterpriseID;
		//						break;
		//					case "DateType":
		//						paramDiscVal.Value = strDateType;
		//						break;
		//					case "FromDate":
		//						paramDiscVal.Value = dtStartDate;
		//						break;
		//					case "ToDate":
		//						paramDiscVal.Value = dtEndDate;
		//						break;
		//					case "PayerType":
		//						paramDiscVal.Value = strPayerType;
		//						break;
		//					case "PayerCode":
		//						paramDiscVal.Value = strPayerId;
		//						break;
		//					case "RouteType":
		//						paramDiscVal.Value = strRouteType;
		//						break;
		//					case "RouteCode":
		//						paramDiscVal.Value = strRouteCode;
		//						break;
		//					case "OriginDC":
		//						paramDiscVal.Value = strorigin_dc;
		//						break;
		//					case "DestinationDC":
		//						paramDiscVal.Value = strdestination_dc;
		//						break;
		//					case "PathOriginDC":
		//						paramDiscVal.Value = strdelPathorigin_dc;
		//						break;
		//					case "PathDestinationDC":
		//						paramDiscVal.Value = strdelPathdestination_dc;
		//						break;
		//					case "PostalCode":
		//						paramDiscVal.Value = strZipCode;
		//						break;
		//					case "DestinationState":
		//						paramDiscVal.Value = strStateCode;
		//						break;
		//					case "SalesID":
		//						paramDiscVal.Value = strSaleManID;
		//						break;
		//					case "ServiceType":
		//						paramDiscVal.Value = strServiceType;
		//						break;
		//
		//					case "prmUserId":
		//						paramDiscVal.Value = strUserId; 
		//						break;
		//
		//						//case "Flag":
		//						//paramDiscVal.Value = strFlag;
		//						//break;
		//					default:
		//						continue;
		//				}
		//				paramvals = paramFld.CurrentValues;
		//				paramvals.Add(paramDiscVal);
		//				paramFld.ApplyCurrentValues(paramvals);
		//			}
		//		}
		//
		// by Ching Dec 12,2007 
		private void SetShipmentPendingCODParams()
		{
			//declare instance for parameterfields class
			ParameterValues paramvals = new ParameterValues();
			ParameterFieldDefinitions paramFlds; 
			ParameterDiscreteValue paramDiscVal = new ParameterDiscreteValue();

			//getting parameter field values
			String strDateType="";
			DateTime dtStartDate, dtEndDate;
			String strPayerType="";
			String strPayerId="*"; 
			String strRouteType=""; 
			String strRouteCode="*";
			String strorigin_dc="*";
			String strdestination_dc="*";
			String strdelPathorigin_dc="*";
			String strdelPathdestination_dc="*";
			String strZipCode="*";
			String strStateCode="*";
			
			DataRow dr = m_dsQuery.Tables[0].Rows[0];

			strDateType = dr["tran_date"].ToString();
			if (Utility.IsNotDBNull(dr["start_date"])) 
				dtStartDate = (DateTime) dr["start_date"];
			else
				dtStartDate = System.DateTime.ParseExact("01/01/1900", "dd/MM/yyyy", null);
			if (Utility.IsNotDBNull(dr["end_date"])) 
				dtEndDate = (DateTime) dr["end_date"];
			else
				dtEndDate = System.DateTime.ParseExact("01/01/1900", "dd/MM/yyyy", null);

			if (Utility.IsNotDBNull(dr["payer_type"]))
				strPayerType = dr["payer_type"].ToString();
			if (Utility.IsNotDBNull(dr["payer_code"]))
				strPayerId = dr["payer_code"].ToString();

			if (Utility.IsNotDBNull(dr["route_type"]))
				strRouteType = dr["route_type"].ToString();
			if (Utility.IsNotDBNull(dr["route_code"]))
				strRouteCode = dr["route_code"].ToString();

			if (Utility.IsNotDBNull(dr["zip_code"]))
				strZipCode = dr["zip_code"].ToString();
			if (Utility.IsNotDBNull(dr["state_code"]))
				strStateCode = dr["state_code"].ToString();

			if (Utility.IsNotDBNull(dr["origin_dc"]))
				strorigin_dc = dr["origin_dc"].ToString();
			if (Utility.IsNotDBNull(dr["destination_dc"]))
				strdestination_dc = dr["destination_dc"].ToString();

			if (Utility.IsNotDBNull(dr["delPath_origin_dc"]))
				strdelPathorigin_dc = dr["delPath_origin_dc"].ToString();
			if (Utility.IsNotDBNull(dr["delPath_destination_dc"]))
				strdelPathdestination_dc = dr["delPath_destination_dc"].ToString();

			//get all parameter fields
			paramFlds = rptSource.DataDefinition.ParameterFields;

			//passing parameter for parameter fields
			foreach(ParameterFieldDefinition paramFld in paramFlds)
			{
				switch(paramFld.ParameterFieldName)
				{
					case "ApplicationID":
						paramDiscVal.Value = strAppID;
						break;
					case "EnterpriseID":
						paramDiscVal.Value = strEnterpriseID;
						break;
					case "DateType":
						paramDiscVal.Value = strDateType;
						break;
					case "FromDate":
						paramDiscVal.Value = dtStartDate;
						break;
					case "ToDate":
						paramDiscVal.Value = dtEndDate;
						break;
					case "PayerType":
						paramDiscVal.Value = strPayerType;
						break;
					case "PayerCode":
						paramDiscVal.Value = strPayerId;
						break;
					case "RouteType":
						paramDiscVal.Value = strRouteType;
						break;
					case "RouteCode":
						paramDiscVal.Value = strRouteCode;
						break;
					case "OriginDC":
						paramDiscVal.Value = strorigin_dc;
						break;
					case "DestinationDC":
						paramDiscVal.Value = strdestination_dc;
						break;
					case "PathOriginDC":
						paramDiscVal.Value = strdelPathorigin_dc;
						break;
					case "PathDestinationDC":
						paramDiscVal.Value = strdelPathdestination_dc;
						break;
					case "PostalCode":
						paramDiscVal.Value = strZipCode;
						break;
					case "DestinationState":
						paramDiscVal.Value = strStateCode;
						break;
					case "prmUserId":
						paramDiscVal.Value = strUserId; 
						break;
					default:
						continue;
				}
				paramvals = paramFld.CurrentValues;
				paramvals.Add(paramDiscVal);
				paramFld.ApplyCurrentValues(paramvals);
			}
		}


		private void SetCODAmountCollectedReportParams()
		{
			//declare instance for parameterfields class
			ParameterValues paramvals = new ParameterValues();
			ParameterFieldDefinitions paramFlds; 
			ParameterDiscreteValue paramDiscVal = new ParameterDiscreteValue();

			//getting parameter field values
			String strDateType="";
			DateTime dtStartDate, dtEndDate;
			String strPayerType="";
			String strPayerId="*"; 
			String strRouteType=""; 
			String strRouteCode="*";
			String strorigin_dc="*";
			String strdestination_dc="*";
			String strdelPathorigin_dc="*";
			String strdelPathdestination_dc="*";
			String strZipCode="*";
			String strStateCode="*";
			
			DataRow dr = m_dsQuery.Tables[0].Rows[0];

			strDateType = dr["tran_date"].ToString();
			if (Utility.IsNotDBNull(dr["start_date"])) 
				dtStartDate = (DateTime) dr["start_date"];
			else
				dtStartDate = System.DateTime.ParseExact("01/01/1900", "dd/MM/yyyy", null);
			if (Utility.IsNotDBNull(dr["end_date"])) 
				dtEndDate = (DateTime) dr["end_date"];
			else
				dtEndDate = System.DateTime.ParseExact("01/01/1900", "dd/MM/yyyy", null);

			if (Utility.IsNotDBNull(dr["payer_type"]))
				strPayerType = dr["payer_type"].ToString();
			if (Utility.IsNotDBNull(dr["payer_code"]))
				strPayerId = dr["payer_code"].ToString();

			if (Utility.IsNotDBNull(dr["route_type"]))
				strRouteType = dr["route_type"].ToString();
			if (Utility.IsNotDBNull(dr["route_code"]))
				strRouteCode = dr["route_code"].ToString();

			if (Utility.IsNotDBNull(dr["zip_code"]))
				strZipCode = dr["zip_code"].ToString();
			if (Utility.IsNotDBNull(dr["state_code"]))
				strStateCode = dr["state_code"].ToString();

			if (Utility.IsNotDBNull(dr["origin_dc"]))
				strorigin_dc = dr["origin_dc"].ToString();
			if (Utility.IsNotDBNull(dr["destination_dc"]))
				strdestination_dc = dr["destination_dc"].ToString();

			if (Utility.IsNotDBNull(dr["delPath_origin_dc"]))
				strdelPathorigin_dc = dr["delPath_origin_dc"].ToString();
			if (Utility.IsNotDBNull(dr["delPath_destination_dc"]))
				strdelPathdestination_dc = dr["delPath_destination_dc"].ToString();

			//get all parameter fields
			paramFlds = rptSource.DataDefinition.ParameterFields;

			//passing parameter for parameter fields
			foreach(ParameterFieldDefinition paramFld in paramFlds)
			{
				switch(paramFld.ParameterFieldName)
				{
					case "ApplicationID":
						paramDiscVal.Value = strAppID;
						break;
					case "EnterpriseID":
						paramDiscVal.Value = strEnterpriseID;
						break;
					case "DateType":
						paramDiscVal.Value = strDateType;
						break;
					case "FromDate":
						paramDiscVal.Value = dtStartDate;
						break;
					case "ToDate":
						paramDiscVal.Value = dtEndDate;
						break;
					case "PayerType":
						paramDiscVal.Value = strPayerType;
						break;
					case "PayerCode":
						paramDiscVal.Value = strPayerId;
						break;
					case "RouteType":
						paramDiscVal.Value = strRouteType;
						break;
					case "RouteCode":
						paramDiscVal.Value = strRouteCode;
						break;
					case "OriginDC":
						paramDiscVal.Value = strorigin_dc;
						break;
					case "DestinationDC":
						paramDiscVal.Value = strdestination_dc;
						break;
					case "PathOriginDC":
						paramDiscVal.Value = strdelPathorigin_dc;
						break;
					case "PathDestinationDC":
						paramDiscVal.Value = strdelPathdestination_dc;
						break;
					case "PostalCode":
						paramDiscVal.Value = strZipCode;
						break;
					case "DestinationState":
						paramDiscVal.Value = strStateCode;
						break;
					default:
						continue;
				}
				paramvals = paramFld.CurrentValues;
				paramvals.Add(paramDiscVal);
				paramFld.ApplyCurrentValues(paramvals);
			}
		}

		private void SetShipmentPackageReplaceReportParams()
	{
			ParameterValues paramvals = new ParameterValues();
			ParameterFieldDefinitions paramFlds; 
			ParameterDiscreteValue paramDiscVal = new ParameterDiscreteValue();
			ParameterRangeValue paramRangeVal = new ParameterRangeValue();

			//getting parameter field values
			String strConsignment_no="";
			int intBooking_no=0;
			String strDateType="";
			DateTime dtStartDate, dtEndDate;
			String strUserid=""; 
			String strPayerId="*"; 
			String strorigin_dc="*";
			String strRef_no="*";
			int strmorethan_show=0;
			
			DataRow dr = m_dsQuery.Tables[0].Rows[0];

			strUserid = utility.GetUserID();
			strDateType = dr["tran_date"].ToString();
			if (Utility.IsNotDBNull(dr["start_date"])) 
				dtStartDate = (DateTime) dr["start_date"];
			else
				dtStartDate = System.DateTime.ParseExact("01/01/1900", "dd/MM/yyyy", null);
			
			if (Utility.IsNotDBNull(dr["end_date"])) 
				dtEndDate = (DateTime) dr["end_date"];
			else
				dtEndDate = System.DateTime.ParseExact("01/01/1900", "dd/MM/yyyy", null);

			if (Utility.IsNotDBNull(dr["consignment_no"]))
				strConsignment_no = dr["consignment_no"].ToString();

			if (Utility.IsNotDBNull(dr["booking_no"]) && dr["booking_no"].ToString() != "")
				intBooking_no = Convert.ToInt32(dr["booking_no"].ToString());
			
			if (Utility.IsNotDBNull(dr["payer_code"]))
				strPayerId = dr["payer_code"].ToString();
			
			if (Utility.IsNotDBNull(dr["ref_no"]))
				strRef_no = dr["ref_no"].ToString();

			if (Utility.IsNotDBNull(dr["origin_dc"]))
				strorigin_dc = dr["origin_dc"].ToString();

			if (Utility.IsNotDBNull(dr["morethan_show"]))
				strmorethan_show = Convert.ToInt32(dr["morethan_show"].ToString());

			//get all parameter fields
			paramFlds = rptSource.DataDefinition.ParameterFields;

			//passing parameter for parameter fields
			foreach(ParameterFieldDefinition paramFld in paramFlds)
			{
				switch(paramFld.ParameterFieldName)
				{
					case "p_consignment_no":
						paramDiscVal.Value = strConsignment_no;
						break;
					case "p_booking_no":
						paramDiscVal.Value = intBooking_no;
						break;
					case "p_appid":
						paramDiscVal.Value = strAppID;
						break;
					case "p_enterprise":
						paramDiscVal.Value = strEnterpriseID;
						break;
					case "p_date_type":
						paramDiscVal.Value = strDateType;
						break;
					case "p_date_range":
						paramRangeVal.StartValue = dtStartDate;
						paramRangeVal.EndValue = dtEndDate;
						break;
					case "p_payerid":
						paramDiscVal.Value = strPayerId;
						break;
					case "ref_no":
						paramDiscVal.Value = strRef_no;
						break;
					case "origin_dc":
						paramDiscVal.Value = strorigin_dc;
						break;
					case "p_morethan":
						paramDiscVal.Value = strmorethan_show;
						break;
					case "p_userid":
						paramDiscVal.Value = strUserid;
						break;
					default:
						continue;
				}

				paramvals = paramFld.CurrentValues;
				if (paramFld.DiscreteOrRangeKind == DiscreteOrRangeKind.RangeValue)
					paramvals.Add(paramRangeVal);
				else 
					paramvals.Add(paramDiscVal);

				paramFld.ApplyCurrentValues(paramvals);
			}
		}

		private void SetCODRemittanceReportParams()
		{
			//declare instance for parameterfields class
			ParameterValues paramvals = new ParameterValues();
			ParameterFieldDefinitions paramFlds; 
			ParameterDiscreteValue paramDiscVal = new ParameterDiscreteValue();

			//getting parameter field values
			String strDateType="";
			DateTime dtStartDate, dtEndDate;
			String strPayerType="";
			String strPayerId="*"; 
			String strRouteType=""; 
			String strRouteCode="*";
			String strorigin_dc="*";
			String strdestination_dc="*";
			String strdelPathorigin_dc="*";
			String strdelPathdestination_dc="*";
			String strZipCode="*";
			String strStateCode="*";
			String strCODAdvance="";
			String strDCList="";
			
			DataRow dr = m_dsQuery.Tables[0].Rows[0];

			strDateType = dr["tran_date"].ToString();
			if (Utility.IsNotDBNull(dr["start_date"])) 
				dtStartDate = (DateTime) dr["start_date"];
			else
				dtStartDate = System.DateTime.ParseExact("01/01/1900", "dd/MM/yyyy", null);
			if (Utility.IsNotDBNull(dr["end_date"])) 
				dtEndDate = (DateTime) dr["end_date"];
			else
				dtEndDate = System.DateTime.ParseExact("01/01/1900", "dd/MM/yyyy", null);

			if (Utility.IsNotDBNull(dr["payer_type"]))
				strPayerType = dr["payer_type"].ToString();
			if (Utility.IsNotDBNull(dr["payer_code"]))
				strPayerId = dr["payer_code"].ToString();

			if (Utility.IsNotDBNull(dr["route_type"]))
				strRouteType = dr["route_type"].ToString();
			if (Utility.IsNotDBNull(dr["route_code"]))
				strRouteCode = dr["route_code"].ToString();

			if (Utility.IsNotDBNull(dr["zip_code"]))
				strZipCode = dr["zip_code"].ToString();
			if (Utility.IsNotDBNull(dr["state_code"]))
				strStateCode = dr["state_code"].ToString();

			if (Utility.IsNotDBNull(dr["origin_dc"]))
				strorigin_dc = dr["origin_dc"].ToString();
			if (Utility.IsNotDBNull(dr["destination_dc"]))
				strdestination_dc = dr["destination_dc"].ToString();

			if (Utility.IsNotDBNull(dr["delPath_origin_dc"]))
				strdelPathorigin_dc = dr["delPath_origin_dc"].ToString();
			if (Utility.IsNotDBNull(dr["delPath_destination_dc"]))
				strdelPathdestination_dc = dr["delPath_destination_dc"].ToString();

			if(Utility.IsNotDBNull(dr["CODAdvance"]))
				strCODAdvance = dr["CODAdvance"].ToString();

			if (Utility.IsNotDBNull(dr["dc_list"]))
				strDCList = dr["dc_list"].ToString();

			//get all parameter fields
			paramFlds = rptSource.DataDefinition.ParameterFields;

			//passing parameter for parameter fields
			foreach(ParameterFieldDefinition paramFld in paramFlds)
			{
				switch(paramFld.ParameterFieldName)
				{
					case "ApplicationID":
						paramDiscVal.Value = strAppID;
						break;
					case "EnterpriseID":
						paramDiscVal.Value = strEnterpriseID;
						break;
					case "DateType":
						paramDiscVal.Value = strDateType;
						break;
					case "FromDate":
						paramDiscVal.Value = dtStartDate;
						break;
					case "ToDate":
						paramDiscVal.Value = dtEndDate;
						break;
					case "PayerType":
						paramDiscVal.Value = strPayerType;
						break;
					case "PayerCode":
						paramDiscVal.Value = strPayerId;
						break;
					case "RouteType":
						paramDiscVal.Value = strRouteType;
						break;
					case "RouteCode":
						paramDiscVal.Value = strRouteCode;
						break;
					case "OriginDC":
						paramDiscVal.Value = strorigin_dc;
						break;
					case "DestinationDC":
						paramDiscVal.Value = strdestination_dc;
						break;
					case "PathOriginDC":
						paramDiscVal.Value = strdelPathorigin_dc;
						break;
					case "PathDestinationDC":
						paramDiscVal.Value = strdelPathdestination_dc;
						break;
					case "PostalCode":
						paramDiscVal.Value = strZipCode;
						break;
					case "DestinationState":
						paramDiscVal.Value = strStateCode;
						break;
					case "CODAdvance":
						paramDiscVal.Value = strCODAdvance;
						break;
					case "prmUserId":
						paramDiscVal.Value = utility.GetUserID();
						break;
					case "dcList":
						paramDiscVal.Value = strDCList;
						break;

					default:
						continue;
				}
				paramvals = paramFld.CurrentValues;
				paramvals.Add(paramDiscVal);
				paramFld.ApplyCurrentValues(paramvals);
			}
		}

		private void SetShipmentPendingPODRpt3PAParams()
		{
			//declare instance for parameterfields class
			ParameterValues paramvals = new ParameterValues();
			ParameterFieldDefinitions paramFlds; 
			ParameterDiscreteValue paramDiscVal = new ParameterDiscreteValue();

			//getting parameter field values
			String strDateType="";
			DateTime dtStartDate, dtEndDate;
			String strPayerType="";
			String strPayerId="*"; 
			String strRouteType=""; 
			String strRouteCode="*";
			String strorigin_dc="*";
			String strdestination_dc="*";
			String strdelPathorigin_dc="*";
			String strdelPathdestination_dc="*";
			String strZipCode="*";
			String strStateCode="*";
			String strCODAdvance="";
			
			DataRow dr = m_dsQuery.Tables[0].Rows[0];

			strDateType = dr["tran_date"].ToString();
			if (Utility.IsNotDBNull(dr["start_date"])) 
				dtStartDate = (DateTime) dr["start_date"];
			else
				dtStartDate = System.DateTime.ParseExact("01/01/1900", "dd/MM/yyyy", null);
			if (Utility.IsNotDBNull(dr["end_date"])) 
				dtEndDate = (DateTime) dr["end_date"];
			else
				dtEndDate = System.DateTime.ParseExact("01/01/1900", "dd/MM/yyyy", null);

			if (Utility.IsNotDBNull(dr["payer_type"]))
				strPayerType = dr["payer_type"].ToString();
			if (Utility.IsNotDBNull(dr["payer_code"]))
				strPayerId = dr["payer_code"].ToString();

			if (Utility.IsNotDBNull(dr["route_type"]))
				strRouteType = dr["route_type"].ToString();
			if (Utility.IsNotDBNull(dr["route_code"]))
				strRouteCode = dr["route_code"].ToString();

			if (Utility.IsNotDBNull(dr["zip_code"]))
				strZipCode = dr["zip_code"].ToString();
			if (Utility.IsNotDBNull(dr["state_code"]))
				strStateCode = dr["state_code"].ToString();

			if (Utility.IsNotDBNull(dr["origin_dc"]))
				strorigin_dc = dr["origin_dc"].ToString();
			if (Utility.IsNotDBNull(dr["destination_dc"]))
				strdestination_dc = dr["destination_dc"].ToString();

			if (Utility.IsNotDBNull(dr["delPath_origin_dc"]))
				strdelPathorigin_dc = dr["delPath_origin_dc"].ToString();
			if (Utility.IsNotDBNull(dr["delPath_destination_dc"]))
				strdelPathdestination_dc = dr["delPath_destination_dc"].ToString();

			if(Utility.IsNotDBNull(dr["CODAdvance"]))
				strCODAdvance = dr["CODAdvance"].ToString();

			//get all parameter fields
			paramFlds = rptSource.DataDefinition.ParameterFields;

			//passing parameter for parameter fields
			foreach(ParameterFieldDefinition paramFld in paramFlds)
			{
				switch(paramFld.ParameterFieldName)
				{
					case "ApplicationID":
						paramDiscVal.Value = strAppID;
						break;
					case "EnterpriseID":
						paramDiscVal.Value = strEnterpriseID;
						break;
					case "DateType":
						paramDiscVal.Value = strDateType;
						break;
					case "FromDate":
						paramDiscVal.Value = dtStartDate;
						break;
					case "ToDate":
						paramDiscVal.Value = dtEndDate;
						break;
					case "PayerType":
						paramDiscVal.Value = strPayerType;
						break;
					case "PayerCode":
						paramDiscVal.Value = strPayerId;
						break;
					case "RouteType":
						paramDiscVal.Value = strRouteType;
						break;
					case "RouteCode":
						paramDiscVal.Value = strRouteCode;
						break;
					case "OriginDC":
						paramDiscVal.Value = strorigin_dc;
						break;
					case "DestinationDC":
						paramDiscVal.Value = strdestination_dc;
						break;
					case "PathOriginDC":
						paramDiscVal.Value = strdelPathorigin_dc;
						break;
					case "PathDestinationDC":
						paramDiscVal.Value = strdelPathdestination_dc;
						break;
					case "PostalCode":
						paramDiscVal.Value = strZipCode;
						break;
					case "DestinationState":
						paramDiscVal.Value = strStateCode;
						break;
					case "CODAdvance":
						paramDiscVal.Value = strCODAdvance;
						break;
					default:
						continue;
				}
				paramvals = paramFld.CurrentValues;
				paramvals.Add(paramDiscVal);
				paramFld.ApplyCurrentValues(paramvals);
			}
		}

		private void SetShipmentTrackingRpt3PAParams()
		{
			//declare instance for parameterfields class
			ParameterValues paramvals = new ParameterValues();
			ParameterFieldDefinitions paramFlds; 
			ParameterDiscreteValue paramDiscVal = new ParameterDiscreteValue();

			//getting parameter field values
			String strDateType="";
			DateTime dtStartDate, dtEndDate;
			String strPayerType="";
			String strPayerId="*"; 
			String strRouteType=""; 
			String strRouteCode="*";
			String strorigin_dc="*";
			String strdestination_dc="*";
			String strdelPathorigin_dc="*";
			String strdelPathdestination_dc="*";
			String strZipCode="*";
			String strStateCode="*";
			String strCODAdvance="";
			
			DataRow dr = m_dsQuery.Tables[0].Rows[0];

			strDateType = dr["tran_date"].ToString();
			if (Utility.IsNotDBNull(dr["start_date"])) 
				dtStartDate = (DateTime) dr["start_date"];
			else
				dtStartDate = System.DateTime.ParseExact("01/01/1900", "dd/MM/yyyy", null);
			if (Utility.IsNotDBNull(dr["end_date"])) 
				dtEndDate = (DateTime) dr["end_date"];
			else
				dtEndDate = System.DateTime.ParseExact("01/01/1900", "dd/MM/yyyy", null);

			if (Utility.IsNotDBNull(dr["payer_type"]))
				strPayerType = dr["payer_type"].ToString();
			if (Utility.IsNotDBNull(dr["payer_code"]))
				strPayerId = dr["payer_code"].ToString();

			if (Utility.IsNotDBNull(dr["route_type"]))
				strRouteType = dr["route_type"].ToString();
			if (Utility.IsNotDBNull(dr["route_code"]))
				strRouteCode = dr["route_code"].ToString();

			if (Utility.IsNotDBNull(dr["zip_code"]))
				strZipCode = dr["zip_code"].ToString();
			if (Utility.IsNotDBNull(dr["state_code"]))
				strStateCode = dr["state_code"].ToString();

			if (Utility.IsNotDBNull(dr["origin_dc"]))
				strorigin_dc = dr["origin_dc"].ToString();
			if (Utility.IsNotDBNull(dr["destination_dc"]))
				strdestination_dc = dr["destination_dc"].ToString();

			if (Utility.IsNotDBNull(dr["delPath_origin_dc"]))
				strdelPathorigin_dc = dr["delPath_origin_dc"].ToString();
			if (Utility.IsNotDBNull(dr["delPath_destination_dc"]))
				strdelPathdestination_dc = dr["delPath_destination_dc"].ToString();

			if(Utility.IsNotDBNull(dr["CODAdvance"]))
				strCODAdvance = dr["CODAdvance"].ToString();

			//get all parameter fields
			paramFlds = rptSource.DataDefinition.ParameterFields;

			//passing parameter for parameter fields
			foreach(ParameterFieldDefinition paramFld in paramFlds)
			{
				switch(paramFld.ParameterFieldName)
				{
					case "ApplicationID":
						paramDiscVal.Value = strAppID;
						break;
					case "EnterpriseID":
						paramDiscVal.Value = strEnterpriseID;
						break;
					case "DateType":
						paramDiscVal.Value = strDateType;
						break;
					case "FromDate":
						paramDiscVal.Value = dtStartDate;
						break;
					case "ToDate":
						paramDiscVal.Value = dtEndDate;
						break;
					case "PayerType":
						paramDiscVal.Value = strPayerType;
						break;
					case "PayerCode":
						paramDiscVal.Value = strPayerId;
						break;
					case "RouteType":
						paramDiscVal.Value = strRouteType;
						break;
					case "RouteCode":
						paramDiscVal.Value = strRouteCode;
						break;
					case "OriginDC":
						paramDiscVal.Value = strorigin_dc;
						break;
					case "DestinationDC":
						paramDiscVal.Value = strdestination_dc;
						break;
					case "PathOriginDC":
						paramDiscVal.Value = strdelPathorigin_dc;
						break;
					case "PathDestinationDC":
						paramDiscVal.Value = strdelPathdestination_dc;
						break;
					case "PostalCode":
						paramDiscVal.Value = strZipCode;
						break;
					case "DestinationState":
						paramDiscVal.Value = strStateCode;
						break;
					case "CODAdvance":
						paramDiscVal.Value = strCODAdvance;
						break;
					default:
						continue;
				}
				paramvals = paramFld.CurrentValues;
				paramvals.Add(paramDiscVal);
				paramFld.ApplyCurrentValues(paramvals);
			}
		}

		private void SetPODRpt3PAParams()
		{
			//declare instance for parameterfields class
			ParameterValues paramvals = new ParameterValues();
			ParameterFieldDefinitions paramFlds; 
			ParameterDiscreteValue paramDiscVal = new ParameterDiscreteValue();

			//getting parameter field values
			String strDateType="";
			DateTime dtStartDate, dtEndDate;
			String strPayerType="";
			String strPayerId="*"; 
			String strRouteType=""; 
			String strRouteCode="*";
			String strorigin_dc="*";
			String strdestination_dc="*";
			String strdelPathorigin_dc="*";
			String strdelPathdestination_dc="*";
			String strZipCode="*";
			String strStateCode="*";
			String strCODAdvance="";
			
			DataRow dr = m_dsQuery.Tables[0].Rows[0];

			strDateType = dr["tran_date"].ToString();
			if (Utility.IsNotDBNull(dr["start_date"])) 
				dtStartDate = (DateTime) dr["start_date"];
			else
				dtStartDate = System.DateTime.ParseExact("01/01/1900", "dd/MM/yyyy", null);
			if (Utility.IsNotDBNull(dr["end_date"])) 
				dtEndDate = (DateTime) dr["end_date"];
			else
				dtEndDate = System.DateTime.ParseExact("01/01/1900", "dd/MM/yyyy", null);

			if (Utility.IsNotDBNull(dr["payer_type"]))
				strPayerType = dr["payer_type"].ToString();
			if (Utility.IsNotDBNull(dr["payer_code"]))
				strPayerId = dr["payer_code"].ToString();

			if (Utility.IsNotDBNull(dr["route_type"]))
				strRouteType = dr["route_type"].ToString();
			if (Utility.IsNotDBNull(dr["route_code"]))
				strRouteCode = dr["route_code"].ToString();

			if (Utility.IsNotDBNull(dr["zip_code"]))
				strZipCode = dr["zip_code"].ToString();
			if (Utility.IsNotDBNull(dr["state_code"]))
				strStateCode = dr["state_code"].ToString();

			if (Utility.IsNotDBNull(dr["origin_dc"]))
				strorigin_dc = dr["origin_dc"].ToString();
			if (Utility.IsNotDBNull(dr["destination_dc"]))
				strdestination_dc = dr["destination_dc"].ToString();

			if (Utility.IsNotDBNull(dr["delPath_origin_dc"]))
				strdelPathorigin_dc = dr["delPath_origin_dc"].ToString();
			if (Utility.IsNotDBNull(dr["delPath_destination_dc"]))
				strdelPathdestination_dc = dr["delPath_destination_dc"].ToString();

			if(Utility.IsNotDBNull(dr["CODAdvance"]))
				strCODAdvance = dr["CODAdvance"].ToString();

			//get all parameter fields
			paramFlds = rptSource.DataDefinition.ParameterFields;

			//passing parameter for parameter fields
			foreach(ParameterFieldDefinition paramFld in paramFlds)
			{
				switch(paramFld.ParameterFieldName)
				{
					case "ApplicationID":
						paramDiscVal.Value = strAppID;
						break;
					case "EnterpriseID":
						paramDiscVal.Value = strEnterpriseID;
						break;
					case "DateType":
						paramDiscVal.Value = strDateType;
						break;
					case "FromDate":
						paramDiscVal.Value = dtStartDate;
						break;
					case "ToDate":
						paramDiscVal.Value = dtEndDate;
						break;
					case "PayerType":
						paramDiscVal.Value = strPayerType;
						break;
					case "PayerCode":
						paramDiscVal.Value = strPayerId;
						break;
					case "RouteType":
						paramDiscVal.Value = strRouteType;
						break;
					case "RouteCode":
						paramDiscVal.Value = strRouteCode;
						break;
					case "OriginDC":
						paramDiscVal.Value = strorigin_dc;
						break;
					case "DestinationDC":
						paramDiscVal.Value = strdestination_dc;
						break;
					case "PathOriginDC":
						paramDiscVal.Value = strdelPathorigin_dc;
						break;
					case "PathDestinationDC":
						paramDiscVal.Value = strdelPathdestination_dc;
						break;
					case "PostalCode":
						paramDiscVal.Value = strZipCode;
						break;
					case "DestinationState":
						paramDiscVal.Value = strStateCode;
						break;
					case "CODAdvance":
						paramDiscVal.Value = strCODAdvance;
						break;
					default:
						continue;
				}
				paramvals = paramFld.CurrentValues;
				paramvals.Add(paramDiscVal);
				paramFld.ApplyCurrentValues(paramvals);
			}
		}

		// End 
		private void SetPODParams()
		{
			//declare instance for parameterfields class
			ParameterValues paramvals = new ParameterValues();
			ParameterFieldDefinitions paramFlds; 
			ParameterDiscreteValue paramDiscVal = new ParameterDiscreteValue();

			//getting parameter field values
			String strDateType="";
			DateTime dtStartDate, dtEndDate;
			String strPayerType="";
			String strPayerId="*"; 
			String strRouteType=""; 
			String strRouteCode="*";
			String strorigin_dc="*";
			String strdestination_dc="*";
			String strdelPathorigin_dc="*";
			String strdelPathdestination_dc="*";
			String strZipCode="*";
			String strStateCode="*";
			String strService_Code="*";
			
			DataRow dr = m_dsQuery.Tables[0].Rows[0];

			strDateType = dr["tran_date"].ToString();
			if (Utility.IsNotDBNull(dr["start_date"])) 
				dtStartDate = (DateTime) dr["start_date"];
			else
				dtStartDate = System.DateTime.ParseExact("01/01/1900", "dd/MM/yyyy", null);
			if (Utility.IsNotDBNull(dr["end_date"])) 
				dtEndDate = (DateTime) dr["end_date"];
			else
				dtEndDate = System.DateTime.ParseExact("01/01/1900", "dd/MM/yyyy", null);

			if (Utility.IsNotDBNull(dr["payer_type"]))
				strPayerType = dr["payer_type"].ToString();
			if (Utility.IsNotDBNull(dr["payer_code"]))
				strPayerId = dr["payer_code"].ToString();

			if (Utility.IsNotDBNull(dr["route_type"]))
				strRouteType = dr["route_type"].ToString();
			if (Utility.IsNotDBNull(dr["route_code"]))
				strRouteCode = dr["route_code"].ToString();

			if (Utility.IsNotDBNull(dr["zip_code"]))
				strZipCode = dr["zip_code"].ToString();
			if (Utility.IsNotDBNull(dr["state_code"]))
				strStateCode = dr["state_code"].ToString();

			if (Utility.IsNotDBNull(dr["origin_dc"]))
				strorigin_dc = dr["origin_dc"].ToString();
			if (Utility.IsNotDBNull(dr["destination_dc"]))
				strdestination_dc = dr["destination_dc"].ToString();

			if (Utility.IsNotDBNull(dr["delPath_origin_dc"]))
				strdelPathorigin_dc = dr["delPath_origin_dc"].ToString();
			if (Utility.IsNotDBNull(dr["delPath_destination_dc"]))
				strdelPathdestination_dc = dr["delPath_destination_dc"].ToString();

			if (Utility.IsNotDBNull(dr["service_type"]))
				strService_Code = dr["service_type"].ToString();

			//get all parameter fields
			paramFlds = rptSource.DataDefinition.ParameterFields;

			//passing parameter for parameter fields
			foreach(ParameterFieldDefinition paramFld in paramFlds)
			{
				switch(paramFld.ParameterFieldName)
				{
					case "ApplicationID":
						paramDiscVal.Value = strAppID;
						break;
					case "EnterpriseID":
						paramDiscVal.Value = strEnterpriseID;
						break;
					case "DateType":
						paramDiscVal.Value = strDateType;
						break;
					case "FromDate":
						paramDiscVal.Value = dtStartDate;
						break;
					case "ToDate":
						paramDiscVal.Value = dtEndDate;
						break;
					case "PayerType":
						paramDiscVal.Value = strPayerType;
						break;
					case "PayerCode":
						paramDiscVal.Value = strPayerId;
						break;
					case "RouteType":
						paramDiscVal.Value = strRouteType;
						break;
					case "RouteCode":
						paramDiscVal.Value = strRouteCode;
						break;
					case "OriginDC":
						paramDiscVal.Value = strorigin_dc;
						break;
					case "DestinationDC":
						paramDiscVal.Value = strdestination_dc;
						break;
					case "PathOriginDC":
						paramDiscVal.Value = strdelPathorigin_dc;
						break;
					case "PathDestinationDC":
						paramDiscVal.Value = strdelPathdestination_dc;
						break;
					case "PostalCode":
						paramDiscVal.Value = strZipCode;
						break;
					case "DestinationState":
						paramDiscVal.Value = strStateCode;
						break;
					case "prmUserId":
						paramDiscVal.Value = strUserId; 
						break;
					case "Service_Code":
						paramDiscVal.Value = strService_Code;
						break;
					default:
						continue;
				}
				paramvals = paramFld.CurrentValues;
				paramvals.Add(paramDiscVal);
				paramFld.ApplyCurrentValues(paramvals);
			}
		}

		
		private void SetTripReportParams()
		{
			//declare instance for parameterfields class
			ParameterValues paramvals = new ParameterValues();
			ParameterFieldDefinitions paramFlds; 
			ParameterDiscreteValue paramDiscVal = new ParameterDiscreteValue();

			//getting parameter field values
			DateTime dtStartDate, dtEndDate;
			String strRouteCode="*";
			String strorigin_dc="*";
			String strdestination_dc="*";
			
			DataRow dr = m_dsQuery.Tables[0].Rows[0];

			if (Utility.IsNotDBNull(dr["start_date"])) 
				dtStartDate = (DateTime) dr["start_date"];
			else
				dtStartDate = System.DateTime.ParseExact("01/01/1900", "dd/MM/yyyy", null);

			if (Utility.IsNotDBNull(dr["end_date"])) 
				dtEndDate = (DateTime) dr["end_date"];
			else
				dtEndDate = System.DateTime.ParseExact("01/01/1900", "dd/MM/yyyy", null);

			if (Utility.IsNotDBNull(dr["route_code"]))
				strRouteCode = dr["route_code"].ToString();

			if (Utility.IsNotDBNull(dr["origin_dc"]))
				strorigin_dc = dr["origin_dc"].ToString();

			if (Utility.IsNotDBNull(dr["destination_dc"]))
				strdestination_dc = dr["destination_dc"].ToString();

			//get all parameter fields
			paramFlds = rptSource.DataDefinition.ParameterFields;

			//passing parameter for parameter fields
			foreach(ParameterFieldDefinition paramFld in paramFlds)
			{
				switch(paramFld.ParameterFieldName)
				{
					case "ApplicationID":
						paramDiscVal.Value = strAppID;
						break;
					case "EnterpriseID":
						paramDiscVal.Value = strEnterpriseID;
						break;
					case "FromDate":
						paramDiscVal.Value = dtStartDate;
						break;
					case "ToDate":
						paramDiscVal.Value = dtEndDate;
						break;
					case "RouteCode":
						paramDiscVal.Value = strRouteCode;
						break;
					case "OriginDC":
						paramDiscVal.Value = strorigin_dc;
						break;
					case "DestinationDC":
						paramDiscVal.Value = strdestination_dc;
						break;
					case "prmUserId":
						paramDiscVal.Value = strUserId; 
						break;
					default:
						continue;
				}
				paramvals = paramFld.CurrentValues;
				paramvals.Add(paramDiscVal);
				paramFld.ApplyCurrentValues(paramvals);
			}
		}


		private void SetFXParams()
		{
			//declare instance for parameterfields class
			ParameterValues paramvals = new ParameterValues();
			ParameterFieldDefinitions paramFlds; 
			ParameterDiscreteValue paramDiscVal = new ParameterDiscreteValue();

			//getting parameter field values
			String strDateType="";
			DateTime dtStartDate, dtEndDate;
			String strPayerType="";
			String strPayerId="*"; 
			String strRouteType=""; 
			String strRouteCode="*";
			String strorigin_dc="*";
			String strdestination_dc="*";
			String strdelPathorigin_dc="*";
			String strdelPathdestination_dc="*";
			String strZipCode="*";
			String strStateCode="*";
			
			DataRow dr = m_dsQuery.Tables[0].Rows[0];

			strDateType = dr["tran_date"].ToString();
			if (Utility.IsNotDBNull(dr["start_date"])) 
				dtStartDate = (DateTime) dr["start_date"];
			else
				dtStartDate = System.DateTime.ParseExact("01/01/1900", "dd/MM/yyyy", null);
			if (Utility.IsNotDBNull(dr["end_date"])) 
				dtEndDate = (DateTime) dr["end_date"];
			else
				dtEndDate = System.DateTime.ParseExact("01/01/1900", "dd/MM/yyyy", null);

			if (Utility.IsNotDBNull(dr["payer_type"]))
				strPayerType = dr["payer_type"].ToString();
			if (Utility.IsNotDBNull(dr["payer_code"]))
				strPayerId = dr["payer_code"].ToString();

			if (Utility.IsNotDBNull(dr["route_type"]))
				strRouteType = dr["route_type"].ToString();
			if (Utility.IsNotDBNull(dr["route_code"]))
				strRouteCode = dr["route_code"].ToString();

			if (Utility.IsNotDBNull(dr["zip_code"]))
				strZipCode = dr["zip_code"].ToString();
			if (Utility.IsNotDBNull(dr["state_code"]))
				strStateCode = dr["state_code"].ToString();

			if (Utility.IsNotDBNull(dr["origin_dc"]))
				strorigin_dc = dr["origin_dc"].ToString();
			if (Utility.IsNotDBNull(dr["destination_dc"]))
				strdestination_dc = dr["destination_dc"].ToString();

			if (Utility.IsNotDBNull(dr["delPath_origin_dc"]))
				strdelPathorigin_dc = dr["delPath_origin_dc"].ToString();
			if (Utility.IsNotDBNull(dr["delPath_destination_dc"]))
				strdelPathdestination_dc = dr["delPath_destination_dc"].ToString();

			//get all parameter fields
			paramFlds = rptSource.DataDefinition.ParameterFields;

			foreach(ParameterFieldDefinition paramFld in paramFlds)
			{
				switch(paramFld.ParameterFieldName)
				{
					case "ApplicationID":
						paramDiscVal.Value = strAppID;
						break;
					case "EnterpriseID":
						paramDiscVal.Value = strEnterpriseID;
						break;
					case "DateType":
						paramDiscVal.Value = strDateType;
						break;
					case "FromDate":
						paramDiscVal.Value = dtStartDate;
						break;
					case "ToDate":
						paramDiscVal.Value = dtEndDate;
						break;
					case "PayerType":
						paramDiscVal.Value = strPayerType;
						break;
					case "PayerCode":
						paramDiscVal.Value = strPayerId;
						break;
					case "RouteType":
						paramDiscVal.Value = strRouteType;
						break;
					case "RouteCode":
						paramDiscVal.Value = strRouteCode;
						break;
					case "OriginDC":
						paramDiscVal.Value = strorigin_dc;
						break;
					case "DestinationDC":
						paramDiscVal.Value = strdestination_dc;
						break;
					case "PathOriginDC":
						paramDiscVal.Value = strdelPathorigin_dc;
						break;
					case "PathDestinationDC":
						paramDiscVal.Value = strdelPathdestination_dc;
						break;
					case "PostalCode":
						paramDiscVal.Value = strZipCode;
						break;
					case "DestinationState":
						paramDiscVal.Value = strStateCode;
						break;
						//					case "prmUserId":
						//						paramDiscVal.Value = strUserId; 
						//						break;
					default:
						continue;
				}
				paramvals = paramFld.CurrentValues;
				paramvals.Add(paramDiscVal);
				paramFld.ApplyCurrentValues(paramvals);
			}
		}

		private void SetSQIParams()
		{
			//declare instance for parameterfields class
			ParameterValues paramvals = new ParameterValues();
			ParameterFieldDefinitions paramFlds; 
			ParameterDiscreteValue paramDiscVal = new ParameterDiscreteValue();

			//getting parameter field values
			String strDateType="";
			DateTime dtStartDate, dtEndDate;
			String strPayerType="";
			String strPayerId="*"; 
			String strRouteType=""; 
			String strRouteCode="*";
			String strorigin_dc="*";
			String strdestination_dc="*";
			String strdelPathorigin_dc="*";
			String strdelPathdestination_dc="*";
			String strZipCode="*";
			String strStateCode="*";
			
			DataRow dr = m_dsQuery.Tables[0].Rows[0];

			strDateType = dr["tran_date"].ToString();
			if (Utility.IsNotDBNull(dr["start_date"])) 
				dtStartDate = (DateTime) dr["start_date"];
			else
				dtStartDate = System.DateTime.ParseExact("01/01/1900", "dd/MM/yyyy", null);
			if (Utility.IsNotDBNull(dr["end_date"])) 
				dtEndDate = (DateTime) dr["end_date"];
			else
				dtEndDate = System.DateTime.ParseExact("01/01/1900", "dd/MM/yyyy", null);

			if (Utility.IsNotDBNull(dr["payer_type"]))
				strPayerType = dr["payer_type"].ToString();
			if (Utility.IsNotDBNull(dr["payer_code"]))
				strPayerId = dr["payer_code"].ToString();

			if (Utility.IsNotDBNull(dr["route_type"]))
				strRouteType = dr["route_type"].ToString();
			if (Utility.IsNotDBNull(dr["route_code"]))
				strRouteCode = dr["route_code"].ToString();

			if (Utility.IsNotDBNull(dr["zip_code"]))
				strZipCode = dr["zip_code"].ToString();
			if (Utility.IsNotDBNull(dr["state_code"]))
				strStateCode = dr["state_code"].ToString();

			if (Utility.IsNotDBNull(dr["origin_dc"]))
				strorigin_dc = dr["origin_dc"].ToString();
			if (Utility.IsNotDBNull(dr["destination_dc"]))
				strdestination_dc = dr["destination_dc"].ToString();

			if (Utility.IsNotDBNull(dr["delPath_origin_dc"]))
				strdelPathorigin_dc = dr["delPath_origin_dc"].ToString();
			if (Utility.IsNotDBNull(dr["delPath_destination_dc"]))
				strdelPathdestination_dc = dr["delPath_destination_dc"].ToString();

			//get all parameter fields
			paramFlds = rptSource.DataDefinition.ParameterFields;

			//passing parameter for parameter fields
			foreach(ParameterFieldDefinition paramFld in paramFlds)
			{
				switch(paramFld.ParameterFieldName)
				{
					case "ApplicationID":
						paramDiscVal.Value = strAppID;
						break;
					case "EnterpriseID":
						paramDiscVal.Value = strEnterpriseID;
						break;
					case "DateType":
						paramDiscVal.Value = strDateType;
						break;
					case "FromDate":
						paramDiscVal.Value = dtStartDate;
						break;
					case "ToDate":
						paramDiscVal.Value = dtEndDate;
						break;
					case "PayerType":
						paramDiscVal.Value = strPayerType;
						break;
					case "PayerCode":
						paramDiscVal.Value = strPayerId;
						break;
					case "RouteType":
						paramDiscVal.Value = strRouteType;
						break;
					case "RouteCode":
						paramDiscVal.Value = strRouteCode;
						break;
					case "OriginDC":
						paramDiscVal.Value = strorigin_dc;
						break;
					case "DestinationDC":
						paramDiscVal.Value = strdestination_dc;
						break;
					case "PathOriginDC":
						paramDiscVal.Value = strdelPathorigin_dc;
						break;
					case "PathDestinationDC":
						paramDiscVal.Value = strdelPathdestination_dc;
						break;
					case "PostalCode":
						paramDiscVal.Value = strZipCode;
						break;
					case "DestinationState":
						paramDiscVal.Value = strStateCode;
						break;
					default:
						continue;
				}
				paramvals = paramFld.CurrentValues;
				paramvals.Add(paramDiscVal);
				paramFld.ApplyCurrentValues(paramvals);
			}
		}


		//Jeab 17 Jun 2011
		private void SetRevnBySalesIDParams()
		{
			//declare instance for parameterfields class
			ParameterValues paramvals = new ParameterValues();
			ParameterFieldDefinitions paramFlds; 
			ParameterDiscreteValue paramDiscVal = new ParameterDiscreteValue();

			//getting parameter field values
			String strDateType="";
			DateTime dtStartDate, dtEndDate;
			String strPayerType="";
			String strPayerId="*"; 
			String strRouteType=""; 
			String strRouteCode="*";
			String strorigin_dc="*";
			String strdestination_dc="*";
			String strdelPathorigin_dc="*";
			String strdelPathdestination_dc="*";
			String strZipCode="*";
			String strStateCode="*";
			String strSalesID="*";
			String strServiceType="*";
			String strPickupRouteCode="*";
			String strPickupZipCode="*";
			String strPickupState="*";
			String strDiscountBand="*";
			String strMasterAccount="*";
			String strNewCust ="";
			String strNewCust2 =""; //Jeab 18 Jul 2011
			String strNewCust3 =""; //Jeab 18 Jul 2011
			String strNewCust4 =""; //Jeab 21 Jul 2011
			String strNewCust5 =""; //Jeab 21 Jul 2011
			String strNewCust6 =""; //Jeab 21 Jul 2011
			String strNewCust7 =""; //Jeab 21 Jul 2011
			String strNewCust8 =""; //Jeab 21 Jul 2011
			
			DataRow dr = m_dsQuery.Tables[0].Rows[0];

			strDateType = dr["tran_date"].ToString();
			if (Utility.IsNotDBNull(dr["start_date"])) 
				dtStartDate = (DateTime) dr["start_date"];
			else
				dtStartDate = System.DateTime.ParseExact("01/01/1900", "dd/MM/yyyy", null);
			if (Utility.IsNotDBNull(dr["end_date"])) 
				dtEndDate = (DateTime) dr["end_date"];
			else
				dtEndDate = System.DateTime.ParseExact("01/01/1900", "dd/MM/yyyy", null);

			if (Utility.IsNotDBNull(dr["payer_type"]))
				strPayerType = dr["payer_type"].ToString();
			if (Utility.IsNotDBNull(dr["payer_code"]))
				strPayerId = dr["payer_code"].ToString();

			if (Utility.IsNotDBNull(dr["route_type"]))
				strRouteType = dr["route_type"].ToString();
			if (Utility.IsNotDBNull(dr["route_code"]))
				strRouteCode = dr["route_code"].ToString();

			if (Utility.IsNotDBNull(dr["zip_code"]))
				strZipCode = dr["zip_code"].ToString();
			if (Utility.IsNotDBNull(dr["state_code"]))
				strStateCode = dr["state_code"].ToString();

			if (Utility.IsNotDBNull(dr["origin_dc"]))
				strorigin_dc = dr["origin_dc"].ToString();
			if (Utility.IsNotDBNull(dr["destination_dc"]))
				strdestination_dc = dr["destination_dc"].ToString();

			if (Utility.IsNotDBNull(dr["delPath_origin_dc"]))
				strdelPathorigin_dc = dr["delPath_origin_dc"].ToString();
			if (Utility.IsNotDBNull(dr["delPath_destination_dc"]))
				strdelPathdestination_dc = dr["delPath_destination_dc"].ToString();

			if (Utility.IsNotDBNull(dr["sale_id"]))
				strSalesID = dr["sale_id"].ToString();

			if (Utility.IsNotDBNull(dr["service_type"]))
				strServiceType = dr["service_type"].ToString();

			if (Utility.IsNotDBNull(dr["picup_route_code"]))
				strPickupRouteCode = dr["picup_route_code"].ToString();

			if (Utility.IsNotDBNull(dr["pickup_zip_code"]))
				strPickupZipCode = dr["pickup_zip_code"].ToString();
			
			if (Utility.IsNotDBNull(dr["pickup_state_code"]))
				strPickupState = dr["pickup_state_code"].ToString();

			if (Utility.IsNotDBNull(dr["discount_band"]))
				strDiscountBand = dr["discount_band"].ToString();

			if (Utility.IsNotDBNull(dr["Master_account"]))
				strMasterAccount = dr["Master_account"].ToString();

			//Jeab 13 Jul 2011
			if (Utility.IsNotDBNull(dr["cust_new"]))
				strNewCust = dr["cust_new"].ToString();

			//Jeab 18 Jul 2011
			if (Utility.IsNotDBNull(dr["cust_new2"]))
				strNewCust2 = dr["cust_new2"].ToString();
			if (Utility.IsNotDBNull(dr["cust_new3"]))
				strNewCust3 = dr["cust_new3"].ToString();

			//Jeab 21 Jul 2011
			if (Utility.IsNotDBNull(dr["cust_new4"]))
				strNewCust4 = dr["cust_new4"].ToString();
			if (Utility.IsNotDBNull(dr["cust_new5"]))
				strNewCust5 = dr["cust_new5"].ToString();
			if (Utility.IsNotDBNull(dr["cust_new6"]))
				strNewCust6 = dr["cust_new6"].ToString();
			if (Utility.IsNotDBNull(dr["cust_new7"]))
				strNewCust7 = dr["cust_new7"].ToString();
			if (Utility.IsNotDBNull(dr["cust_new8"]))
				strNewCust8 = dr["cust_new8"].ToString();
		

			//get all parameter fields
			paramFlds = rptSource.DataDefinition.ParameterFields;

			//passing parameter for parameter fields
			foreach(ParameterFieldDefinition paramFld in paramFlds)
			{
				switch(paramFld.ParameterFieldName)
				{
					case "ApplicationID":
						paramDiscVal.Value = strAppID;
						break;
					case "EnterpriseID":
						paramDiscVal.Value = strEnterpriseID;
						break;
					case "DateType":
						paramDiscVal.Value = strDateType;
						break;
					case "FromDate":
						paramDiscVal.Value = dtStartDate;
						break;
					case "ToDate":
						paramDiscVal.Value = dtEndDate;
						break;
					case "PayerType":
						paramDiscVal.Value = strPayerType;
						break;
					case "PayerCode":
						paramDiscVal.Value = strPayerId;
						break;
					case "RouteType":
						paramDiscVal.Value = strRouteType;
						break;
					case "RouteCode":
						paramDiscVal.Value = strRouteCode;
						break;
					case "OriginDC":
						paramDiscVal.Value = strorigin_dc;
						break;
					case "DestinationDC":
						paramDiscVal.Value = strdestination_dc;
						break;
					case "PathOriginDC":
						paramDiscVal.Value = strdelPathorigin_dc;
						break;
					case "PathDestinationDC":
						paramDiscVal.Value = strdelPathdestination_dc;
						break;
					case "PostalCode":
						paramDiscVal.Value = strZipCode;
						break;
					case "DestinationState":
						paramDiscVal.Value = strStateCode;
						break;
					case "SalesID":
						paramDiscVal.Value = strSalesID;
						break;
					case "ServiceType":
						paramDiscVal.Value = strServiceType;
						break;
					case "PickupRouteCode":
						paramDiscVal.Value = strPickupRouteCode;
						break;
					case "PickupZipCode":
						paramDiscVal.Value = strPickupZipCode;
						break;
					case "PickupState":
						paramDiscVal.Value = strPickupState;
						break;
					case "DisCountBand":
						paramDiscVal.Value = strDiscountBand;
						break;
					case "MasterAccount":
						paramDiscVal.Value = strMasterAccount;
						break;
					//Jeab 13 Jul 2011
					case "CustList":
						paramDiscVal.Value = strNewCust;
						break;
						//Jeab 18 Jul 2011
					case "CustList2":
						paramDiscVal.Value = strNewCust2;
						break;
					case "CustList3":
						paramDiscVal.Value = strNewCust3;
						break;
					//Jeab 21 Jul 2011
					case "CustList4":
						paramDiscVal.Value = strNewCust4;
						break;
					case "CustList5":
						paramDiscVal.Value = strNewCust5;
						break;
					case "CustList6":
						paramDiscVal.Value = strNewCust6;
						break;
					case "CustList7":
						paramDiscVal.Value = strNewCust7;
						break;
					case "CustList8":
						paramDiscVal.Value = strNewCust8;
						break;
					default:
						continue;
				}
				paramvals = paramFld.CurrentValues;
				paramvals.Add(paramDiscVal);
				paramFld.ApplyCurrentValues(paramvals);
			}
		}
		//Jeab 17 Jun 2011  =========> End

		//Jeab 13 Jun 2011
		private void SetSQI_PODEXParams()
		{
			//declare instance for parameterfields class
			ParameterValues paramvals = new ParameterValues();
			ParameterFieldDefinitions paramFlds; 
			ParameterDiscreteValue paramDiscVal = new ParameterDiscreteValue();

			//getting parameter field values
			String strDateType="";
			DateTime dtStartDate, dtEndDate;
			String strPayerType="";
			String strPayerId="*"; 
			String strRouteType=""; 
			String strRouteCode="*";
			String strorigin_dc="*";
			String strdestination_dc="*";
			String strdelPathorigin_dc="*";
			String strdelPathdestination_dc="*";
			String strZipCode="*";
			String strStateCode="*";
			String strServiceCode="*";
			String strExceptionType="*";
			String strtotCons="";
			
			DataRow dr = m_dsQuery.Tables[0].Rows[0];

			strDateType = dr["tran_date"].ToString();
			if (Utility.IsNotDBNull(dr["start_date"])) 
				dtStartDate = (DateTime) dr["start_date"];
			else
				dtStartDate = System.DateTime.ParseExact("01/01/1900", "dd/MM/yyyy", null);
			if (Utility.IsNotDBNull(dr["end_date"])) 
				dtEndDate = (DateTime) dr["end_date"];
			else
				dtEndDate = System.DateTime.ParseExact("01/01/1900", "dd/MM/yyyy", null);

			if (Utility.IsNotDBNull(dr["payer_type"]))
				strPayerType = dr["payer_type"].ToString();
			if (Utility.IsNotDBNull(dr["payerid"]))
				strPayerId = dr["payerid"].ToString();

			if (Utility.IsNotDBNull(dr["service_code"]))
				strServiceCode = dr["service_code"].ToString();

			if (Utility.IsNotDBNull(dr["route_type"]))
				strRouteType = dr["route_type"].ToString();
			if (Utility.IsNotDBNull(dr["route_code"]))
				strRouteCode = dr["route_code"].ToString();

			if (Utility.IsNotDBNull(dr["zip_code"]))
				strZipCode = dr["zip_code"].ToString();
			if (Utility.IsNotDBNull(dr["state_code"]))
				strStateCode = dr["state_code"].ToString();

			if (Utility.IsNotDBNull(dr["origin_dc"]))
				strorigin_dc = dr["origin_dc"].ToString();
			if (Utility.IsNotDBNull(dr["destination_dc"]))
				strdestination_dc = dr["destination_dc"].ToString();

			if (Utility.IsNotDBNull(dr["delPath_origin_dc"]))
				strdelPathorigin_dc = dr["delPath_origin_dc"].ToString();
			if (Utility.IsNotDBNull(dr["delPath_destination_dc"]))
				strdelPathdestination_dc = dr["delPath_destination_dc"].ToString();

			if (Utility.IsNotDBNull(dr["exception_type"]))
				strExceptionType = dr["exception_type"].ToString();

			strtotCons = Convert.ToString( PODEXRptQuery.GetTotalConsignment(strAppID,strEnterpriseID,dr["tran_date"].ToString(),dr["start_date"].ToString(),dr["end_date"].ToString()
				,dr["payerid"].ToString(),dr["destination_dc"].ToString(),dr["zip_code"].ToString(),dr["state_code"].ToString()
				,dr["origin_dc"].ToString(),dr["route_type"].ToString(),dr["route_code"].ToString(),dr["payer_type"].ToString()
				,dr["service_code"].ToString(),dr["delPath_origin_dc"].ToString(),dr["delPath_destination_dc"].ToString()));

			//get all parameter fields
			paramFlds = rptSource.DataDefinition.ParameterFields;

			//passing parameter for parameter fields
			foreach(ParameterFieldDefinition paramFld in paramFlds)
			{
				switch(paramFld.ParameterFieldName)
				{					
					case "DateType":
						paramDiscVal.Value = strDateType;
						break;
					case "FromDate":
						paramDiscVal.Value = dtStartDate;
						break;
					case "ToDate":
						paramDiscVal.Value = dtEndDate;
						break;
					case "PayerType":
						paramDiscVal.Value = strPayerType;
						break;
					case "PayerCode":
						paramDiscVal.Value = strPayerId;
						break;
					case "RouteType":
						paramDiscVal.Value = strRouteType;
						break;
					case "RouteCode":
						paramDiscVal.Value = strRouteCode;
						break;
					case "OriginDC":
						paramDiscVal.Value = strorigin_dc;
						break;
					case "DestinationDC":
						paramDiscVal.Value = strdestination_dc;
						break;
					case "PathOriginDC":
						paramDiscVal.Value = strdelPathorigin_dc;
						break;
					case "PathDestinationDC":
						paramDiscVal.Value = strdelPathdestination_dc;
						break;
					case "PostalCode":
						paramDiscVal.Value = strZipCode;
						break;
					case "DestinationState":
						paramDiscVal.Value = strStateCode;
						break;
					case "Service_Code":
						paramDiscVal.Value = strServiceCode;
						break;
					case "ExceptionType":
						paramDiscVal.Value = strExceptionType;
						break;
					case "totCons":
						paramDiscVal.Value = strtotCons;
						break;
					default:
						continue;
				}
				paramvals = paramFld.CurrentValues;
				paramvals.Add(paramDiscVal);
				paramFld.ApplyCurrentValues(paramvals);
			}
		}


		//Jeab 06 Jul 2011
		private void SetDailyKFPODEXParam()
		{
			//declare instance for parameterfields class
			ParameterValues paramvals = new ParameterValues();
			ParameterFieldDefinitions paramFlds; 
			ParameterDiscreteValue paramDiscVal = new ParameterDiscreteValue();

			//getting parameter field values
			String strDate ="";
			String strUser ="";

			DataRow dr = m_dsQuery.Tables[0].Rows[0];

			strDate = dr["rptDate"].ToString();
			strUser = dr["userid"].ToString();

			//get all parameter fields
			paramFlds = rptSource.DataDefinition.ParameterFields;

			//passing parameter for parameter fields
			foreach(ParameterFieldDefinition paramFld in paramFlds)
			{
				switch(paramFld.ParameterFieldName)
				{					
					case "rptDate":
						paramDiscVal.Value = strDate;
						break;
					case "userid":
						paramDiscVal.Value = strUser;
						break;			
					case "ApplicationID":
						paramDiscVal.Value = strAppID;
						break;
					case "EnterpriseID":
						paramDiscVal.Value = strEnterpriseID;
						break;
					default:
						continue;
				}
				paramvals = paramFld.CurrentValues;
				paramvals.Add(paramDiscVal);
				paramFld.ApplyCurrentValues(paramvals);
			}
		}
		//Jeab 06 Jul 2011  =========> End

		private void SetAverageDeliveryTimeParams()
		{
			//declare instance for parameterfields class
			ParameterValues paramvals = new ParameterValues();
			ParameterFieldDefinitions paramFlds; 
			ParameterDiscreteValue paramDiscVal = new ParameterDiscreteValue();

			//getting parameter field values
			String strDateType="";
			DateTime dtStartDate, dtEndDate;
			String strPayerType="";
			String strPayerId="*"; 
			String strRouteType=""; 
			String strRouteCode="*";
			String strorigin_dc="*";
			String strdestination_dc="*";
			String strdelPathorigin_dc="*";
			String strdelPathdestination_dc="*";
			String strZipCode="*";
			String strStateCode="*";
			String strServiceType="*";
			
			DataRow dr = m_dsQuery.Tables[0].Rows[0];

			strDateType = dr["tran_date"].ToString();
			if (Utility.IsNotDBNull(dr["start_date"])) 
				dtStartDate = (DateTime) dr["start_date"];
			else
				dtStartDate = System.DateTime.ParseExact("01/01/1900", "dd/MM/yyyy", null);
			if (Utility.IsNotDBNull(dr["end_date"])) 
				dtEndDate = (DateTime) dr["end_date"];
			else
				dtEndDate = System.DateTime.ParseExact("01/01/1900", "dd/MM/yyyy", null);

			if (Utility.IsNotDBNull(dr["payer_type"]))
				strPayerType = dr["payer_type"].ToString();
			if (Utility.IsNotDBNull(dr["payer_code"]))
				strPayerId = dr["payer_code"].ToString();

			if (Utility.IsNotDBNull(dr["route_type"]))
				strRouteType = dr["route_type"].ToString();
			if (Utility.IsNotDBNull(dr["route_code"]))
				strRouteCode = dr["route_code"].ToString();

			if (Utility.IsNotDBNull(dr["zip_code"]))
				strZipCode = dr["zip_code"].ToString();
			if (Utility.IsNotDBNull(dr["state_code"]))
				strStateCode = dr["state_code"].ToString();

			if (Utility.IsNotDBNull(dr["origin_dc"]))
				strorigin_dc = dr["origin_dc"].ToString();
			if (Utility.IsNotDBNull(dr["destination_dc"]))
				strdestination_dc = dr["destination_dc"].ToString();

			if (Utility.IsNotDBNull(dr["delPath_origin_dc"]))
				strdelPathorigin_dc = dr["delPath_origin_dc"].ToString();
			if (Utility.IsNotDBNull(dr["delPath_destination_dc"]))
				strdelPathdestination_dc = dr["delPath_destination_dc"].ToString();

			if (Utility.IsNotDBNull(dr["service_type"]))
				strServiceType = dr["service_type"].ToString();

			//get all parameter fields
			paramFlds = rptSource.DataDefinition.ParameterFields;

			//passing parameter for parameter fields
			foreach(ParameterFieldDefinition paramFld in paramFlds)
			{
				switch(paramFld.ParameterFieldName)
				{
					case "ApplicationID":
						paramDiscVal.Value = strAppID;
						break;
					case "EnterpriseID":
						paramDiscVal.Value = strEnterpriseID;
						break;
					case "DateType":
						paramDiscVal.Value = strDateType;
						break;
					case "FromDate":
						paramDiscVal.Value = dtStartDate;
						break;
					case "ToDate":
						paramDiscVal.Value = dtEndDate;
						break;
					case "PayerType":
						paramDiscVal.Value = strPayerType;
						break;
					case "PayerCode":
						paramDiscVal.Value = strPayerId;
						break;
					case "RouteType":
						paramDiscVal.Value = strRouteType;
						break;
					case "RouteCode":
						paramDiscVal.Value = strRouteCode;
						break;
					case "OriginDC":
						paramDiscVal.Value = strorigin_dc;
						break;
					case "DestinationDC":
						paramDiscVal.Value = strdestination_dc;
						break;
					case "PathOriginDC":
						paramDiscVal.Value = strdelPathorigin_dc;
						break;
					case "PathDestinationDC":
						paramDiscVal.Value = strdelPathdestination_dc;
						break;
					case "PostalCode":
						paramDiscVal.Value = strZipCode;
						break;
					case "DestinationState":
						paramDiscVal.Value = strStateCode;
						break;
					case "ServiceType":
						paramDiscVal.Value = strServiceType;
						break;
					default:
						continue;
				}
				paramvals = paramFld.CurrentValues;
				paramvals.Add(paramDiscVal);
				paramFld.ApplyCurrentValues(paramvals);
			}
		}


		//PSA# 2009-014
		private void SetTotalAverageDeliveryTimeParams()
		{
			//declare instance for parameterfields class
			ParameterValues paramvals = new ParameterValues();
			ParameterFieldDefinitions paramFlds; 
			ParameterDiscreteValue paramDiscVal = new ParameterDiscreteValue();

			//getting parameter field values
			String strDateType="";
			DateTime dtStartDate, dtEndDate;
			String strPayerType="";
			String strPayerId="*"; 
			String strRouteType=""; 
			String strRouteCode="*";
			String strorigin_dc="*";
			String strdestination_dc="*";
			String strdelPathorigin_dc="*";
			String strdelPathdestination_dc="*";
			String strZipCode="*";
			String strStateCode="*";
			String strServiceType="*";
			
			DataRow dr = m_dsQuery.Tables[0].Rows[0];

			strDateType = dr["tran_date"].ToString();
			if (Utility.IsNotDBNull(dr["start_date"])) 
				dtStartDate = (DateTime) dr["start_date"];
			else
				dtStartDate = System.DateTime.ParseExact("01/01/1900", "dd/MM/yyyy", null);
			if (Utility.IsNotDBNull(dr["end_date"])) 
				dtEndDate = (DateTime) dr["end_date"];
			else
				dtEndDate = System.DateTime.ParseExact("01/01/1900", "dd/MM/yyyy", null);

			if (Utility.IsNotDBNull(dr["payer_type"]))
				strPayerType = dr["payer_type"].ToString();
			if (Utility.IsNotDBNull(dr["payer_code"]))
				strPayerId = dr["payer_code"].ToString();

			if (Utility.IsNotDBNull(dr["route_type"]))
				strRouteType = dr["route_type"].ToString();
			if (Utility.IsNotDBNull(dr["route_code"]))
				strRouteCode = dr["route_code"].ToString();

			if (Utility.IsNotDBNull(dr["zip_code"]))
				strZipCode = dr["zip_code"].ToString();
			if (Utility.IsNotDBNull(dr["state_code"]))
				strStateCode = dr["state_code"].ToString();

			if (Utility.IsNotDBNull(dr["origin_dc"]))
				strorigin_dc = dr["origin_dc"].ToString();
			if (Utility.IsNotDBNull(dr["destination_dc"]))
				strdestination_dc = dr["destination_dc"].ToString();

			if (Utility.IsNotDBNull(dr["delPath_origin_dc"]))
				strdelPathorigin_dc = dr["delPath_origin_dc"].ToString();
			if (Utility.IsNotDBNull(dr["delPath_destination_dc"]))
				strdelPathdestination_dc = dr["delPath_destination_dc"].ToString();

			if (Utility.IsNotDBNull(dr["service_type"]))
				strServiceType = dr["service_type"].ToString();

			//get all parameter fields
			paramFlds = rptSource.DataDefinition.ParameterFields;

			//passing parameter for parameter fields
			foreach(ParameterFieldDefinition paramFld in paramFlds)
			{
				switch(paramFld.ParameterFieldName)
				{
					case "ApplicationID":
						paramDiscVal.Value = strAppID;
						break;
					case "EnterpriseID":
						paramDiscVal.Value = strEnterpriseID;
						break;
					case "DateType":
						paramDiscVal.Value = strDateType;
						break;
					case "FromDate":
						paramDiscVal.Value = dtStartDate;
						break;
					case "ToDate":
						paramDiscVal.Value = dtEndDate;
						break;
					case "PayerType":
						paramDiscVal.Value = strPayerType;
						break;
					case "PayerCode":
						paramDiscVal.Value = strPayerId;
						break;
					case "RouteType":
						paramDiscVal.Value = strRouteType;
						break;
					case "RouteCode":
						paramDiscVal.Value = strRouteCode;
						break;
					case "OriginDC":
						paramDiscVal.Value = strorigin_dc;
						break;
					case "DestinationDC":
						paramDiscVal.Value = strdestination_dc;
						break;
					case "PathOriginDC":
						paramDiscVal.Value = strdelPathorigin_dc;
						break;
					case "PathDestinationDC":
						paramDiscVal.Value = strdelPathdestination_dc;
						break;
					case "PostalCode":
						paramDiscVal.Value = strZipCode;
						break;
					case "DestinationState":
						paramDiscVal.Value = strStateCode;
						break;
					case "ServiceType":
						paramDiscVal.Value = strServiceType;
						break;
					default:
						continue;
				}
				paramvals = paramFld.CurrentValues;
				paramvals.Add(paramDiscVal);
				paramFld.ApplyCurrentValues(paramvals);
			}
		}

		//PSA# 2009-014

		//HC Return Task
		private void SetPendingHCReportParams()
		{
			//declare instance for parameterfields class
			ParameterValues paramvals = new ParameterValues();
			ParameterFieldDefinitions paramFlds; 
			ParameterDiscreteValue paramDiscVal = new ParameterDiscreteValue();

			//getting parameter field values
			DateTime dtStartDate, dtEndDate;
			String strPayerType="";
			String strPayerId="*"; 
			String strRouteType=""; 
			String strRouteCode="*";
			String strorigin_dc="*";
			String strdestination_dc="*";
			String strdelPathorigin_dc="*";
			String strdelPathdestination_dc="*";
			String strZipCode="*";
			String strStateCode="*";
			
			DataRow dr = m_dsQuery.Tables[0].Rows[0];

			if (Utility.IsNotDBNull(dr["start_date"])) 
				dtStartDate = (DateTime) dr["start_date"];
			else
				dtStartDate = System.DateTime.ParseExact("01/01/1900", "dd/MM/yyyy", null);
			if (Utility.IsNotDBNull(dr["end_date"])) 
				dtEndDate = (DateTime) dr["end_date"];
			else
				dtEndDate = System.DateTime.ParseExact("01/01/1900", "dd/MM/yyyy", null);

			if (Utility.IsNotDBNull(dr["payer_type"]))
				strPayerType = dr["payer_type"].ToString();
			if (Utility.IsNotDBNull(dr["payer_code"]))
				strPayerId = dr["payer_code"].ToString();

			if (Utility.IsNotDBNull(dr["route_type"]))
				strRouteType = dr["route_type"].ToString();
			if (Utility.IsNotDBNull(dr["route_code"]))
				strRouteCode = dr["route_code"].ToString();

			if (Utility.IsNotDBNull(dr["zip_code"]))
				strZipCode = dr["zip_code"].ToString();
			if (Utility.IsNotDBNull(dr["state_code"]))
				strStateCode = dr["state_code"].ToString();

			if (Utility.IsNotDBNull(dr["origin_dc"]))
				strorigin_dc = dr["origin_dc"].ToString();
			if (Utility.IsNotDBNull(dr["destination_dc"]))
				strdestination_dc = dr["destination_dc"].ToString();

			if (Utility.IsNotDBNull(dr["delPath_origin_dc"]))
				strdelPathorigin_dc = dr["delPath_origin_dc"].ToString();
			if (Utility.IsNotDBNull(dr["delPath_destination_dc"]))
				strdelPathdestination_dc = dr["delPath_destination_dc"].ToString();



			//get all parameter fields
			paramFlds = rptSource.DataDefinition.ParameterFields;

			//passing parameter for parameter fields
			foreach(ParameterFieldDefinition paramFld in paramFlds)
			{
				switch(paramFld.ParameterFieldName)
				{
					case "ApplicationID":
						paramDiscVal.Value = strAppID;
						break;
					case "EnterpriseID":
						paramDiscVal.Value = strEnterpriseID;
						break;
					case "UserId":
						paramDiscVal.Value = utility.GetUserID();
						break;
					case "FromDate":
						paramDiscVal.Value = dtStartDate;
						break;
					case "ToDate":
						paramDiscVal.Value = dtEndDate;
						break;
					case "PayerType":
						paramDiscVal.Value = strPayerType;
						break;
					case "PayerCode":
						paramDiscVal.Value = strPayerId;
						break;
					case "RouteType":
						paramDiscVal.Value = strRouteType;
						break;
					case "RouteCode":
						paramDiscVal.Value = strRouteCode;
						break;
					case "OriginDC":
						paramDiscVal.Value = strorigin_dc;
						break;
					case "DestinationDC":
						paramDiscVal.Value = strdestination_dc;
						break;
					case "PathOriginDC":
						paramDiscVal.Value = strdelPathorigin_dc;
						break;
					case "PathDestinationDC":
						paramDiscVal.Value = strdelPathdestination_dc;
						break;
					case "PostalCode":
						paramDiscVal.Value = strZipCode;
						break;
					case "DestinationState":
						paramDiscVal.Value = strStateCode;
						break;
					default:
						continue;
				}
				paramvals = paramFld.CurrentValues;
				paramvals.Add(paramDiscVal);
				paramFld.ApplyCurrentValues(paramvals);
			}
		}
		
		private void SetInvoiceHeaderReportParams()
		{
			//by sittichai 27/03/2008
			//declare instance for parameterfields class
			ParameterValues paramvals = new ParameterValues();
			ParameterFieldDefinitions paramFlds; 
			ParameterDiscreteValue paramDiscVal = new ParameterDiscreteValue();
			
			//getting parameter field values
			String strInvoiceNo=""; 
					
			DataRow dr;
			for(int i = 0; i<=m_dsQuery.Tables[0].Rows.Count-1;i++)
			{
				dr = m_dsQuery.Tables[0].Rows[i];
				if (Utility.IsNotDBNull(dr["invoice_no"]))
				{
					if(strInvoiceNo.Length>0)
						strInvoiceNo=strInvoiceNo+",";

					strInvoiceNo = strInvoiceNo+ dr["invoice_no"].ToString();
				}
			}
			//get all parameter fields
			paramFlds = rptSource.DataDefinition.ParameterFields;

			foreach(ParameterFieldDefinition paramFld in paramFlds)
			{
				switch(paramFld.ParameterFieldName)
				{
					case "Pm_AppID":
						paramDiscVal.Value = strAppID;
						break;
					case "Pm_EnterpriseID":
						paramDiscVal.Value = strEnterpriseID;
						break;
					case "Pm_InvoiceNo":
						paramDiscVal.Value = strInvoiceNo;
						break;
					default:
						continue;
				}
				paramvals = paramFld.CurrentValues;
				paramvals.Add(paramDiscVal);
				paramFld.ApplyCurrentValues(paramvals);
			}
		}

		private void SetInvoiceDetailReportParams()
		{
			//by sittichai 28/03/2008
			//declare instance for parameterfields class
			ParameterValues paramvals = new ParameterValues();
			ParameterFieldDefinitions paramFlds; 
			ParameterDiscreteValue paramDiscVal = new ParameterDiscreteValue();
			
			//getting parameter field values
			String strInvoiceNo=""; 
					
			DataRow dr;
			for(int i = 0; i<=m_dsQuery.Tables[0].Rows.Count-1;i++)
			{
				dr = m_dsQuery.Tables[0].Rows[i];
				if (Utility.IsNotDBNull(dr["invoice_no"]))
				{
					if(strInvoiceNo.Length>0)
						strInvoiceNo=strInvoiceNo+",";

					strInvoiceNo = strInvoiceNo+ dr["invoice_no"].ToString();
					ViewState["invoice_no"] = dr["invoice_no"].ToString();
					//by X jul 01 08
				}
			}
			//get all parameter fields
			paramFlds = rptSource.DataDefinition.ParameterFields;

			foreach(ParameterFieldDefinition paramFld in paramFlds)
			{
				switch(paramFld.ParameterFieldName)
				{
					case "Pm_AppID":
						paramDiscVal.Value = strAppID;
						break;
					case "Pm_EnterpriseID":
						paramDiscVal.Value = strEnterpriseID;
						break;
					case "prmUserID":
						paramDiscVal.Value = strUserId;
						break;
					case "Pm_InvoiceNo":
						paramDiscVal.Value = strInvoiceNo;
						break;
					default:
						continue;
				}
				paramvals = paramFld.CurrentValues;
				paramvals.Add(paramDiscVal);
				paramFld.ApplyCurrentValues(paramvals);
			}
		}

		private void SetHCReturnServiceExceptionParams()
		{
			//declare instance for parameterfields class
			ParameterValues paramvals = new ParameterValues();
			ParameterFieldDefinitions paramFlds; 
			ParameterDiscreteValue paramDiscVal = new ParameterDiscreteValue();

			//getting parameter field values
			String strDateType="";
			DateTime dtStartDate, dtEndDate;
			String strPayerType="";
			String strPayerId="*"; 
			String strRouteType=""; 
			String strRouteCode="*";
			String strorigin_dc="*";
			String strdestination_dc="*";
			String strdelPathorigin_dc="*";
			String strdelPathdestination_dc="*";
			String strZipCode="*";
			String strStateCode="*";
			
			DataRow dr = m_dsQuery.Tables[0].Rows[0];

			strDateType = dr["date_type"].ToString();
			if (Utility.IsNotDBNull(dr["start_date"])) 
				dtStartDate = (DateTime) dr["start_date"];
			else
				dtStartDate = System.DateTime.ParseExact("01/01/1900", "dd/MM/yyyy", null);
			if (Utility.IsNotDBNull(dr["end_date"])) 
				dtEndDate = (DateTime) dr["end_date"];
			else
				dtEndDate = System.DateTime.ParseExact("01/01/1900", "dd/MM/yyyy", null);

			if (Utility.IsNotDBNull(dr["payer_type"]))
				strPayerType = dr["payer_type"].ToString();
			if (Utility.IsNotDBNull(dr["payer_code"]))
				strPayerId = dr["payer_code"].ToString();

			if (Utility.IsNotDBNull(dr["route_type"]))
				strRouteType = dr["route_type"].ToString();
			if (Utility.IsNotDBNull(dr["route_code"]))
				strRouteCode = dr["route_code"].ToString();

			if (Utility.IsNotDBNull(dr["zip_code"]))
				strZipCode = dr["zip_code"].ToString();
			if (Utility.IsNotDBNull(dr["state_code"]))
				strStateCode = dr["state_code"].ToString();

			if (Utility.IsNotDBNull(dr["origin_dc"]))
				strorigin_dc = dr["origin_dc"].ToString();
			if (Utility.IsNotDBNull(dr["destination_dc"]))
				strdestination_dc = dr["destination_dc"].ToString();

			if (Utility.IsNotDBNull(dr["delPath_origin_dc"]))
				strdelPathorigin_dc = dr["delPath_origin_dc"].ToString();
			if (Utility.IsNotDBNull(dr["delPath_destination_dc"]))
				strdelPathdestination_dc = dr["delPath_destination_dc"].ToString();

			//get all parameter fields
			paramFlds = rptSource.DataDefinition.ParameterFields;

			//passing parameter for parameter fields
			foreach(ParameterFieldDefinition paramFld in paramFlds)
			{
				switch(paramFld.ParameterFieldName)
				{
					case "ApplicationID":
						paramDiscVal.Value = strAppID;
						break;
					case "EnterpriseID":
						paramDiscVal.Value = strEnterpriseID;
						break;
					case "UserId":
						paramDiscVal.Value = utility.GetUserID();
						break;
					case "DateType":
						paramDiscVal.Value = strDateType;
						break;
					case "FromDate":
						paramDiscVal.Value = dtStartDate;
						break;
					case "ToDate":
						paramDiscVal.Value = dtEndDate;
						break;
					case "PayerType":
						paramDiscVal.Value = strPayerType;
						break;
					case "PayerCode":
						paramDiscVal.Value = strPayerId;
						break;
					case "RouteType":
						paramDiscVal.Value = strRouteType;
						break;
					case "RouteCode":
						paramDiscVal.Value = strRouteCode;
						break;
					case "OriginDC":
						paramDiscVal.Value = strorigin_dc;
						break;
					case "DestinationDC":
						paramDiscVal.Value = strdestination_dc;
						break;
					case "PathOriginDC":
						paramDiscVal.Value = strdelPathorigin_dc;
						break;
					case "PathDestinationDC":
						paramDiscVal.Value = strdelPathdestination_dc;
						break;
					case "PostalCode":
						paramDiscVal.Value = strZipCode;
						break;
					case "DestinationState":
						paramDiscVal.Value = strStateCode;
						break;
					default:
						continue;
				}
				paramvals = paramFld.CurrentValues;
				paramvals.Add(paramDiscVal);
				paramFld.ApplyCurrentValues(paramvals);
			}
		}

		//HC Return Task

		private void SetDispatchTracking()
		{
			//declare instance for parameterfields class
			ParameterFields  paramFields = new ParameterFields(); 
			ParameterField paramField = new ParameterField();
			ParameterFieldDefinitions paramFldDefs ;
			ParameterFieldDefinition paramFldDef ;
			ParameterValues paramVals ;
			ParameterDiscreteValue paramDVal;
			paramFldDefs = rptSource.DataDefinition.ParameterFields;
			
			//variable declaration
			String strDateType="";
			String strCutoff="";
			DateTime dtStartDate, dtEndDate;
			String strPayerID="*", strPayerType="";
			String strRouteCode="*";
			String strorigin_dc="*";
			String strSenderName="*";
			int iBookingNo=0;
			String strDispatchType="*";
			String strStatusCode="*", strExceptionCode="*";

			DataRow dr = m_dsQuery.Tables[0].Rows[0];

			//passing parameter for parameter fields
			if (Utility.IsNotDBNull(dr["tran_date"]))
				strDateType = dr["tran_date"].ToString();

			if (Utility.IsNotDBNull(dr["start_date"])) 
				dtStartDate = (DateTime)dr["start_date"];
			else
				dtStartDate = System.DateTime.ParseExact("01/01/1900", "dd/MM/yyyy", null);

			if (Utility.IsNotDBNull(dr["end_date"])) 
				dtEndDate = (DateTime)dr["end_date"];
			else
				dtEndDate = System.DateTime.ParseExact("01/01/1900", "dd/MM/yyyy", null);

			if (Utility.IsNotDBNull(dr["payer_code"]))
				strPayerID = dr["payer_code"].ToString();
			if (Utility.IsNotDBNull(dr["payer_type"]))
				strPayerType = dr["payer_type"].ToString();
			
			if (Utility.IsNotDBNull(dr["route_code"]))
				strRouteCode = dr["route_code"].ToString();
			if (Utility.IsNotDBNull(dr["origin_dc"]))
				strorigin_dc = dr["origin_dc"].ToString();

			if (Utility.IsNotDBNull(dr["senderName"]))
				strSenderName = dr["senderName"].ToString();
			if (Utility.IsNotDBNull(dr["BookingNo"]))
				iBookingNo = Convert.ToInt32(dr["BookingNo"].ToString());
			if (Utility.IsNotDBNull(dr["dispatchType"]))
				strDispatchType = dr["dispatchType"].ToString();

			if (Utility.IsNotDBNull(dr["statusCode"]))
				strStatusCode = dr["statusCode"].ToString();
			if (Utility.IsNotDBNull(dr["exceptionCode"]))
				strExceptionCode = dr["exceptionCode"].ToString();
			if (Utility.IsNotDBNull(dr["cutoff_time"]))
				strCutoff = dr["cutoff_time"].ToString();
						
			//ApplicationID
			paramDVal = new ParameterDiscreteValue();
			paramDVal.Value = strAppID;
			paramFldDef = paramFldDefs["prmAppid"];
			paramVals = paramFldDef.CurrentValues;
			paramVals.Add(paramDVal);
			paramFldDef.ApplyCurrentValues(paramVals);

			//EnterpriseID
			paramDVal = new ParameterDiscreteValue();
			paramDVal.Value = strEnterpriseID;
			paramFldDef = paramFldDefs["prmEnterpriseid"];
			paramVals = paramFldDef.CurrentValues;
			paramVals.Add(paramDVal);
			paramFldDef.ApplyCurrentValues(paramVals);

			//UserId
			paramDVal = new ParameterDiscreteValue();
			paramDVal.Value = strUserId;
			paramFldDef = paramFldDefs["prmUserId"];
			paramVals = paramFldDef.CurrentValues;
			paramVals.Add(paramDVal);
			paramFldDef.ApplyCurrentValues(paramVals);

			//Payer Type
			paramDVal = new ParameterDiscreteValue();
			if (Utility.IsNotDBNull(strPayerType) && strPayerType!="")
				paramDVal.Value = strPayerType;
			else
				paramDVal.Value = "*";
			paramFldDef = paramFldDefs["prmPayerType"];
			paramVals = paramFldDef.CurrentValues;
			paramVals.Add(paramDVal);
			paramFldDef.ApplyCurrentValues(paramVals);

			//Payer ID
			paramDVal = new ParameterDiscreteValue();
			if (Utility.IsNotDBNull(strPayerID) && strPayerID!="")
				paramDVal.Value = strPayerID;
			else
				paramDVal.Value = "*";
			paramFldDef = paramFldDefs["prmPayerid"];
			paramVals = paramFldDef.CurrentValues;
			paramVals.Add(paramDVal);
			paramFldDef.ApplyCurrentValues(paramVals);


			//Route Code
			paramDVal = new ParameterDiscreteValue();
			if (Utility.IsNotDBNull(strRouteCode) && strRouteCode!="")
				paramDVal.Value = strRouteCode;
			else
				paramDVal.Value = "*";
			paramFldDef = paramFldDefs["prmRouteCode"];
			paramVals = paramFldDef.CurrentValues;
			paramVals.Add(paramDVal);
			paramFldDef.ApplyCurrentValues(paramVals);
			
			//Origin DC
			paramDVal = new ParameterDiscreteValue();
			if (Utility.IsNotDBNull(strorigin_dc) && strorigin_dc!="")
				paramDVal.Value = strorigin_dc;
			else
				paramDVal.Value = "*";
			paramFldDef = paramFldDefs["prmOriginDC"];
			paramVals = paramFldDef.CurrentValues;
			paramVals.Add(paramDVal);
			paramFldDef.ApplyCurrentValues(paramVals);

			//Sender Name
			paramDVal = new ParameterDiscreteValue();
			if (Utility.IsNotDBNull(strSenderName) && strSenderName!="")
				paramDVal.Value = strSenderName;
			else
				paramDVal.Value = "*";
			paramFldDef = paramFldDefs["prmSenderName"];
			paramVals = paramFldDef.CurrentValues;
			paramVals.Add(paramDVal);
			paramFldDef.ApplyCurrentValues(paramVals);
		
			//Booking No
			paramDVal = new ParameterDiscreteValue();
			if (Utility.IsNotDBNull(iBookingNo) && iBookingNo!=0)
				paramDVal.Value = iBookingNo;
			else
				paramDVal.Value = 0;
			paramFldDef = paramFldDefs["prmBookingNo"];
			paramVals = paramFldDef.CurrentValues;
			paramVals.Add(paramDVal);
			paramFldDef.ApplyCurrentValues(paramVals);

			//DispactchType
			paramDVal = new ParameterDiscreteValue();
			if (Utility.IsNotDBNull(strDispatchType) && strDispatchType!="")
				paramDVal.Value = strDispatchType;
			else
				paramDVal.Value = "*";
			paramFldDef = paramFldDefs["prmDispatchType"];
			paramVals = paramFldDef.CurrentValues;
			paramVals.Add(paramDVal);
			paramFldDef.ApplyCurrentValues(paramVals);

			//StatusCode
			paramDVal = new ParameterDiscreteValue();
			if (Utility.IsNotDBNull(strStatusCode) && strStatusCode!="")
				paramDVal.Value = strStatusCode;
			else
				paramDVal.Value = "*";
			paramFldDef = paramFldDefs["prmStatusCode"];
			paramVals = paramFldDef.CurrentValues;
			paramVals.Add(paramDVal);
			paramFldDef.ApplyCurrentValues(paramVals);

			//ExceptionCode
			paramDVal = new ParameterDiscreteValue();
			if (Utility.IsNotDBNull(strExceptionCode) && strExceptionCode!="")
				paramDVal.Value = strExceptionCode;
			else
				paramDVal.Value = "*";
			paramFldDef = paramFldDefs["prmExceptionCode"];
			paramVals = paramFldDef.CurrentValues;
			paramVals.Add(paramDVal);
			paramFldDef.ApplyCurrentValues(paramVals);
			
			//DateType
			paramDVal = new ParameterDiscreteValue();
			paramDVal.Value = strDateType;
			paramFldDef = paramFldDefs["prmDateType"];
			paramVals = paramFldDef.CurrentValues;
			paramVals.Add(paramDVal);
			paramFldDef.ApplyCurrentValues(paramVals);
			
			//CutOff
			paramDVal = new ParameterDiscreteValue();
			paramDVal.Value = strCutoff;
			paramFldDef = paramFldDefs["prmCutOff"];
			paramVals = paramFldDef.CurrentValues;
			paramVals.Add(paramDVal);
			paramFldDef.ApplyCurrentValues(paramVals);

			//FromDate
			paramDVal = new ParameterDiscreteValue();
			paramDVal.Value = dtStartDate;
			paramFldDef = paramFldDefs["prmStartDate"];
			paramVals = paramFldDef.CurrentValues;
			paramVals.Add(paramDVal);
			paramFldDef.ApplyCurrentValues(paramVals);

			//EndDate
			paramDVal = new ParameterDiscreteValue();
			paramDVal.Value = dtEndDate;
			paramFldDef = paramFldDefs["prmEndDate"];
			paramVals = paramFldDef.CurrentValues;
			paramVals.Add(paramDVal);
			paramFldDef.ApplyCurrentValues(paramVals);
		}
	
		
		private void SetShipPendingPOD()
		{
			DateTime dtStartDate, dtEndDate;
			String strShpPending = "";
			String strRouteCode="*", strRouteType="*";
			String strPayerID="*", strPayerType="";
			String strorigin_dc="*", strdestination_dc="*";
			String strdelPath_origin_dc="*", strdelPath_destination_dc="*";
			String strzip_code="*", strstate_code="*";
			
			//declare instance for parameterfields class
			ParameterFieldDefinitions paramFldDefs ;
			ParameterValues paramVals = new ParameterValues() ;
			ParameterDiscreteValue paramDVal = new ParameterDiscreteValue();
			paramFldDefs = rptSource.DataDefinition.ParameterFields;
			
			DataRow dr = m_dsQuery.Tables[0].Rows[0];

			if (Utility.IsNotDBNull(dr["ShipType"])) 
				strShpPending = dr["ShipType"].ToString() ;

			if (Utility.IsNotDBNull(dr["start_date"])) 
				dtStartDate = Convert.ToDateTime(dr["start_date"]);
			else
				dtStartDate = System.DateTime.ParseExact("01/01/1900", "dd/MM/yyyy", null);
			if (Utility.IsNotDBNull(dr["end_date"])) 
				dtEndDate = Convert.ToDateTime(dr["end_date"]);
			else
				dtEndDate = System.DateTime.ParseExact("01/01/1900", "dd/MM/yyyy", null);

			if (Utility.IsNotDBNull(dr["route_code"])) 
				strRouteCode = dr["route_code"].ToString();
			if (Utility.IsNotDBNull(dr["route_type"])) 
				strRouteType = dr["route_type"].ToString();

			if (Utility.IsNotDBNull(dr["payer_code"])) 
				strPayerID = dr["payer_code"].ToString();
			if (Utility.IsNotDBNull(dr["payer_type"])) 
				strPayerType = dr["payer_type"].ToString();

			if (Utility.IsNotDBNull(dr["origin_dc"])) 
				strorigin_dc = dr["origin_dc"].ToString();
			if (Utility.IsNotDBNull(dr["destination_dc"])) 
				strdestination_dc = dr["destination_dc"].ToString();

			if (Utility.IsNotDBNull(dr["delPath_origin_dc"])) 
				strdelPath_origin_dc = dr["delPath_origin_dc"].ToString();
			if (Utility.IsNotDBNull(dr["delPath_destination_dc"])) 
				strdelPath_destination_dc = dr["delPath_destination_dc"].ToString();

			if (Utility.IsNotDBNull(dr["zip_code"])) 
				strzip_code = dr["zip_code"].ToString();
			if (Utility.IsNotDBNull(dr["state_code"])) 
				strstate_code = dr["state_code"].ToString();

			foreach(ParameterFieldDefinition paramFldDef in paramFldDefs)
			{
				//paramVals = new ParameterValues();
				switch(paramFldDef.ParameterFieldName)
				{
					case "prmAppid":
						paramDVal.Value = strAppID;
						break;
					case "prmEnterpriseId":
						paramDVal.Value = strEnterpriseID; 
						break;
					case "prmUserId":
						paramDVal.Value = strUserId; 
						break;
					case "prmShipType":
						paramDVal.Value = strShpPending; 
						break;
					case "prmStartDate":
						paramDVal.Value = dtStartDate; 
						break;
					case "prmEndDate":
						paramDVal.Value = dtEndDate; 
						break;
					case "prmRouteCode":
						paramDVal.Value = strRouteCode; 
						break;
					case "prmRouteType":
						paramDVal.Value = strRouteType; 
						break;
					case "prmPayerID":
						paramDVal.Value = strPayerID; 
						break;
					case "prmPayerType":
						paramDVal.Value = strPayerType; 
						break;
					case "prmOriginDC":
						paramDVal.Value = strorigin_dc; 
						break;
					case "prmDestinationDC":
						paramDVal.Value = strdestination_dc; 
						break;
					case "prmDELOriginDC":
						paramDVal.Value = strdelPath_origin_dc; 
						break;
					case "prmDELDestinationDC":
						paramDVal.Value = strdelPath_destination_dc; 
						break;
					case "prmZipCode":
						paramDVal.Value = strzip_code; 
						break;
					case "prmStateCode":
						paramDVal.Value = strstate_code; 
						break;
					default:
						continue;
				}
			
				paramVals = paramFldDef.CurrentValues;
				paramVals.Add(paramDVal);
				paramFldDef.ApplyCurrentValues(paramVals);
				
			}
		}

		private void SetRevenueByState()
		{
			//declare instance for parameterfields class
			ParameterFieldDefinitions paramFldDefs ;
			ParameterValues paramVals = new ParameterValues() ;
			ParameterDiscreteValue paramDVal = new ParameterDiscreteValue();
			paramFldDefs = rptSource.DataDefinition.ParameterFields;

			DateTime dtStartDate, dtEndDate;
			String strPayerType="";
			String strPayerId="*"; 
			String strRouteType=""; 
			String strRouteCode="*";
			String strorigin_dc="*";
			String strdestination_dc="*";
			String strdelPathorigin_dc="*";
			String strdelPathdestination_dc="*";
			String strZipCode="*";
			String strStateCode="*";
			String strSendName="*";
			String strRecpName="*";
			String strSendrecpType="";

			DataRow dr = m_dsQuery.Tables[0].Rows[0];
		
			if (Utility.IsNotDBNull(dr["start_date"])) 
				dtStartDate = Convert.ToDateTime(dr["start_date"]);
			else
				dtStartDate = System.DateTime.ParseExact("01/01/1900", "dd/MM/yyyy", null);
			if (Utility.IsNotDBNull(dr["end_date"])) 
				dtEndDate = Convert.ToDateTime(dr["end_date"]);
			else
				dtEndDate = System.DateTime.ParseExact("01/01/1900", "dd/MM/yyyy", null);
		
			if (Utility.IsNotDBNull(dr["payer_type"]))
				strPayerType = dr["payer_type"].ToString();
			if (Utility.IsNotDBNull(dr["payer_code"]))
				strPayerId = dr["payer_code"].ToString();

			if (Utility.IsNotDBNull(dr["route_type"]))
				strRouteType = dr["route_type"].ToString();
			if (Utility.IsNotDBNull(dr["route_code"]))
				strRouteCode = dr["route_code"].ToString();

			if (Utility.IsNotDBNull(dr["zip_code"]))
				strZipCode = dr["zip_code"].ToString();
			if (Utility.IsNotDBNull(dr["state_code"]))
				strStateCode = dr["state_code"].ToString();

			if (Utility.IsNotDBNull(dr["origin_dc"]))
				strorigin_dc = dr["origin_dc"].ToString();
			if (Utility.IsNotDBNull(dr["destination_dc"]))
				strdestination_dc = dr["destination_dc"].ToString();

			if (Utility.IsNotDBNull(dr["delPath_origin_dc"]))
				strdelPathorigin_dc = dr["delPath_origin_dc"].ToString();
			if (Utility.IsNotDBNull(dr["delPath_destination_dc"]))
				strdelPathdestination_dc = dr["delPath_destination_dc"].ToString();

			if (Utility.IsNotDBNull(dr["sendrecpType"]))
				strSendrecpType = dr["sendrecpType"].ToString();
			if (Utility.IsNotDBNull(dr["send_name"]))
				strSendName = dr["send_name"].ToString();
			if (Utility.IsNotDBNull(dr["recp_name"]))
				strRecpName = dr["recp_name"].ToString();
		
			foreach(ParameterFieldDefinition paramFldDef in paramFldDefs)
			{
				switch(paramFldDef.ParameterFieldName)
				{
					case "prmAppid":
						paramDVal.Value = strAppID;
						break;
					case "prmEnterpriseId":
						paramDVal.Value = strEnterpriseID; 
						break;
					case "prmPayerId":
						paramDVal.Value = strPayerId ; 
						break;
					case "prmCustType":
						paramDVal.Value = strPayerType; 
						break;
					case "prmStartDate":
						paramDVal.Value = dtStartDate; 
						break;
					case "prmEndDate":
						paramDVal.Value = dtEndDate; 
						break;
					case "prmRouteType":
						paramDVal.Value = strRouteType; 
						break;
					case "prmRouteCode":
						paramDVal.Value = strRouteCode; 
						break;
					case "prmZipCode":
						paramDVal.Value = strZipCode; 
						break;
					case "prmState":
						paramDVal.Value = strStateCode; 
						break;
					case "prmUserId":
						paramDVal.Value = strUserId; 
						break;
					case "prmOriginDC":
						paramDVal.Value = strorigin_dc; 
						break;
					case "prmDestDC":
						paramDVal.Value = strdestination_dc; 
						break;
					case "prmDelPathOriginDC":
						paramDVal.Value = strdelPathorigin_dc; 
						break;
					case "prmDelPathDestDC":
						paramDVal.Value = strdelPathdestination_dc; 
						break;
					case "prmSndPayRecp":
						paramDVal.Value = strSendrecpType; 
						break;
					case "prmSenderName":
						paramDVal.Value = strSendName; 
						break;
					case "prmRecpName":
						paramDVal.Value = strRecpName; 
						break;
					default:
						continue;
				}
			
				paramVals = paramFldDef.CurrentValues;
				paramVals.Add(paramDVal);
				paramFldDef.ApplyCurrentValues(paramVals);
				
			}
		}

		private void SetMarginAnalysis()
		{
			//declare instance for parameterfields class
			ParameterValues paramvals = new ParameterValues();
			ParameterFieldDefinitions paramFlds; 
			ParameterDiscreteValue paramDiscVal = new ParameterDiscreteValue();
			ParameterRangeValue paramRangeVal = new ParameterRangeValue();

			DateTime dtStartDate, dtEndDate;
			String strPayerType="";
			String strPayerId="*"; 
			String strRouteType=""; 
			String strRouteCode="*";
			String strorigin_dc="*";
			String strdestination_dc="*";
			String strdelPathorigin_dc="*";
			String strdelPathdestination_dc="*";
			String strZipCode="*";
			String strStateCode="*";
			String strSACostMethod=null, strLHCostMethod=null;
			
			DataRow dr = m_dsQuery.Tables[0].Rows[0];
			if (Utility.IsNotDBNull(dr["start_date"])) 
				dtStartDate = (DateTime) dr["start_date"];
			else
				dtStartDate = System.DateTime.ParseExact("01/01/1900", "dd/MM/yyyy", null);
			if (Utility.IsNotDBNull(dr["end_date"])) 
				dtEndDate = (DateTime) dr["end_date"];
			else
				dtEndDate = System.DateTime.ParseExact("01/01/1900", "dd/MM/yyyy", null);

			if (Utility.IsNotDBNull(dr["payer_type"]))
				strPayerType = dr["payer_type"].ToString();
			if (Utility.IsNotDBNull(dr["payer_code"]))
				strPayerId = dr["payer_code"].ToString();

			if (Utility.IsNotDBNull(dr["route_type"]))
				strRouteType = dr["route_type"].ToString();
			if (Utility.IsNotDBNull(dr["route_code"]))
				strRouteCode = dr["route_code"].ToString();

			if (Utility.IsNotDBNull(dr["zip_code"]))
				strZipCode = dr["zip_code"].ToString();
			if (Utility.IsNotDBNull(dr["state_code"]))
				strStateCode = dr["state_code"].ToString();

			if (Utility.IsNotDBNull(dr["origin_dc"]))
				strorigin_dc = dr["origin_dc"].ToString();
			if (Utility.IsNotDBNull(dr["destination_dc"]))
				strdestination_dc = dr["destination_dc"].ToString();

			if (Utility.IsNotDBNull(dr["delPath_origin_dc"]))
				strdelPathorigin_dc = dr["delPath_origin_dc"].ToString();
			if (Utility.IsNotDBNull(dr["delPath_destination_dc"]))
				strdelPathdestination_dc = dr["delPath_destination_dc"].ToString();

			if (Utility.IsNotDBNull(dr["sa_cost_method"]))
				strSACostMethod = dr["sa_cost_method"].ToString();
			if (Utility.IsNotDBNull(dr["lh_cost_method"]))
				strLHCostMethod = dr["lh_cost_method"].ToString();

			//get all parameter fields
			paramFlds = rptSource.DataDefinition.ParameterFields;

			//passing parameter for parameter fields
			foreach(ParameterFieldDefinition paramFld in paramFlds)
			{
				switch(paramFld.ParameterFieldName)
				{
					case "p_appid":
						paramDiscVal.Value = strAppID;
						break;
					case "p_enterpriseid":
						paramDiscVal.Value = strEnterpriseID;
						break;
					case "p_payer_type":
						paramDiscVal.Value = strPayerType;
						break;
					case "p_payerid":
						paramDiscVal.Value = strPayerId;
						break;
					case "p_booking_date":
						paramRangeVal.StartValue = dtStartDate;
						paramRangeVal.EndValue = dtEndDate;
						break;
					case "p_route_type":
						paramDiscVal.Value = strRouteType;
						break;
					case "p_route_code":
						paramDiscVal.Value = strRouteCode;
						break;
					case "p_zipcode":
						paramDiscVal.Value = strZipCode;
						break;
					case "p_state_code":
						paramDiscVal.Value = strStateCode;
						break;
					case "p_userid":
						paramDiscVal.Value = utility.GetUserID();
						break;
					case "p_origin_dc":
						paramDiscVal.Value = strorigin_dc;
						break;
					case "p_destination_dc":
						paramDiscVal.Value = strdestination_dc;
						break;
					case "p_delPath_origin_dc":
						paramDiscVal.Value = strdelPathorigin_dc;
						break;
					case "p_delPath_destination_dc":
						paramDiscVal.Value = strdelPathdestination_dc;
						break;
					case "p_sa_cost_method":
						paramDiscVal.Value = strSACostMethod;
						break;
					case "p_lh_cost_method":
						paramDiscVal.Value = strLHCostMethod;
						break;
					default:
						continue;
				}
				paramvals = paramFld.CurrentValues;
				if (paramFld.DiscreteOrRangeKind == DiscreteOrRangeKind.RangeValue)
					paramvals.Add(paramRangeVal);
				else 
					paramvals.Add(paramDiscVal);
				paramFld.ApplyCurrentValues(paramvals);
			}
		}

		private void SetRevenueByCustAgent()
		{
			//declare instance for parameterfields class
			ParameterFieldDefinitions paramFldDefs ;
			ParameterValues paramVals = new ParameterValues() ;
			ParameterDiscreteValue paramDVal = new ParameterDiscreteValue();
			paramFldDefs = rptSource.DataDefinition.ParameterFields;

			DateTime dtStartDate, dtEndDate;
			String strPayerType="";
			String strPayerId="*"; 
			String strRouteType=""; 
			String strRouteCode="*";
			String strorigin_dc="*";
			String strdestination_dc="*";
			String strdelPathorigin_dc="*";
			String strdelPathdestination_dc="*";
			String strZipCode="*";
			String strStateCode="*";

			DataRow dr = m_dsQuery.Tables[0].Rows[0];
		
			if (Utility.IsNotDBNull(dr["start_date"])) 
				dtStartDate = Convert.ToDateTime(dr["start_date"]);
			else
				dtStartDate = System.DateTime.ParseExact("01/01/1900", "dd/MM/yyyy", null);
			if (Utility.IsNotDBNull(dr["end_date"])) 
				dtEndDate = Convert.ToDateTime(dr["end_date"]);
			else
				dtEndDate = System.DateTime.ParseExact("01/01/1900", "dd/MM/yyyy", null);
		
			if (Utility.IsNotDBNull(dr["payer_type"]))
				strPayerType = dr["payer_type"].ToString();
			if (Utility.IsNotDBNull(dr["payer_code"]))
				strPayerId = dr["payer_code"].ToString();

			if (Utility.IsNotDBNull(dr["route_type"]))
				strRouteType = dr["route_type"].ToString();
			if (Utility.IsNotDBNull(dr["route_code"]))
				strRouteCode = dr["route_code"].ToString();

			if (Utility.IsNotDBNull(dr["zip_code"]))
				strZipCode = dr["zip_code"].ToString();
			if (Utility.IsNotDBNull(dr["state_code"]))
				strStateCode = dr["state_code"].ToString();

			if (Utility.IsNotDBNull(dr["origin_dc"]))
				strorigin_dc = dr["origin_dc"].ToString();
			if (Utility.IsNotDBNull(dr["destination_dc"]))
				strdestination_dc = dr["destination_dc"].ToString();

			if (Utility.IsNotDBNull(dr["delPath_origin_dc"]))
				strdelPathorigin_dc = dr["delPath_origin_dc"].ToString();
			if (Utility.IsNotDBNull(dr["delPath_destination_dc"]))
				strdelPathdestination_dc = dr["delPath_destination_dc"].ToString();

			foreach(ParameterFieldDefinition paramFldDef in paramFldDefs)
			{
				switch(paramFldDef.ParameterFieldName)
				{
					case "prmAppid":
						paramDVal.Value = strAppID;
						break;
					case "prmEnterpriseId":
						paramDVal.Value = strEnterpriseID; 
						break;
					case "prmPayerId":
						paramDVal.Value = strPayerId ; 
						break;
					case "prmCustType":
						paramDVal.Value = strPayerType; 
						break;
					case "prmStartDate":
						paramDVal.Value = dtStartDate; 
						break;
					case "prmEndDate":
						paramDVal.Value = dtEndDate; 
						break;
					case "prmRouteType":
						paramDVal.Value = strRouteType; 
						break;
					case "prmRouteCode":
						paramDVal.Value = strRouteCode; 
						break;
					case "prmZipCode":
						paramDVal.Value = strZipCode; 
						break;
					case "prmState":
						paramDVal.Value = strStateCode; 
						break;
					case "prmUserId":
						paramDVal.Value = strUserId; 
						break;
					case "prmOriginDC":
						paramDVal.Value = strorigin_dc; 
						break;
					case "prmDestDC":
						paramDVal.Value = strdestination_dc; 
						break;
					case "prmDelPathOriginDC":
						paramDVal.Value = strdelPathorigin_dc; 
						break;
					case "prmDelPathDestDC":
						paramDVal.Value = strdelPathdestination_dc; 
						break;
					default:
						continue;
				}
			
				paramVals = paramFldDef.CurrentValues;
				paramVals.Add(paramDVal);
				paramFldDef.ApplyCurrentValues(paramVals);
				
			}
		}

		private void SetStatusException()
		{
			String strStatusType = null;
			String strStatusClose = null;
			String strInvoice = null;
			String strMBG = null;
			String strStatusExceptType = null;
		
			DateTime dtStartDate = DateTime.Now;
			DateTime dtEndDate = DateTime.Now;
		
			String strStatusCode = null,strExceptionCode = null;		

			//declare instance for parameterfields class
			ParameterFieldDefinitions paramFldDefs ;
			ParameterValues paramVals = new ParameterValues() ;
			ParameterDiscreteValue paramDVal = new ParameterDiscreteValue();
			paramFldDefs = rptSource.DataDefinition.ParameterFields;

			DataRow dr = m_dsQuery.Tables[0].Rows[0];
		
			if (Utility.IsNotDBNull(dr["From_Date"])) 
				dtStartDate = Convert.ToDateTime(dr["From_Date"]);
			else
				dtStartDate = System.DateTime.ParseExact("01/01/1900", "dd/MM/yyyy", null);
			if (Utility.IsNotDBNull(dr["To_Date"])) 
				dtEndDate = Convert.ToDateTime(dr["To_Date"]);
			else
				dtEndDate = System.DateTime.ParseExact("01/01/1900", "dd/MM/yyyy", null);
		
			if (Utility.IsNotDBNull(dr["StatusExcpType"])) 
				strStatusExceptType = dr["StatusExcpType"].ToString();
			if (Utility.IsNotDBNull(dr["StatusType"])) 
				strStatusType = dr["StatusType"].ToString();
			if (Utility.IsNotDBNull(dr["StatusClose"])) 
				strStatusClose = dr["StatusClose"].ToString();
			if (Utility.IsNotDBNull(dr["Invoice"])) 
				strInvoice = dr["Invoice"].ToString();
			if (Utility.IsNotDBNull(dr["MBG"])) 
				strMBG = dr["MBG"].ToString();
			if (Utility.IsNotDBNull(dr["StatusCode"])) 
				strStatusCode  = dr["StatusCode"].ToString();
			if (Utility.IsNotDBNull(dr["ExceptionCode"])) 
				strExceptionCode = dr["ExceptionCode"].ToString();
			
			foreach(ParameterFieldDefinition paramFldDef in paramFldDefs)
			{
				switch(paramFldDef.ParameterFieldName)
				{
					case "prmAppid":
						paramDVal.Value = strAppID;
						break;
					case "prmEnterpriseId":
						paramDVal.Value = strEnterpriseID; 
						break;
					case "prmStatusExcpType":
						paramDVal.Value = strStatusExceptType ; 
						break;
					case "prmStatusType":
						paramDVal.Value = strStatusType ; 
						break;
					case "prmStatusClose":
						paramDVal.Value = strStatusClose ; 
						break;
					case "prmInvoice":
						paramDVal.Value = strInvoice ; 
						break;
					case "prmMBG":
						paramDVal.Value = strMBG ; 
						break;
					case "prmStatusCode":
						paramDVal.Value = strStatusCode ; 
						break;
					case "prmExceptionCode":
						paramDVal.Value = strExceptionCode ; 
						break;
					case "prmUserId":
						paramDVal.Value = strUserId; 
						break;
					default:
						continue;
				}
				paramVals = paramFldDef.CurrentValues;
				paramVals.Add(paramDVal);
				paramFldDef.ApplyCurrentValues(paramVals);
				/*rptSource.ReportDefinition.se
				ReportObjects repObjs = rptSource.ReportDefinition.Sections[0].ReportObjects;
				String strName;
				foreach(ReportObject repObj in repObjs)
				{	
					strName = repObj.Name;
					if (strName.Equals("Text1"))
					{
						TextObject txtObj = (TextObject) repObj;
						txtObj.Text = "Success";
					}
				}*/	

				//Section section = rpt.ReportDefinition.Sections[0];//this.ReportDefinition.Sections[1];
				//TextObject txtObj = (TextObject) section.ReportObjects["Text1"];
				//txtObj.Text = "How are you?";
			}
		}

		private void SetReportTypeAndDueDate()
		{
			DataSet dsQuery = m_dsQuery;
			//DataSet dsCreditAging = ReportDAL.GetCreditAging(dsQuery);
			//rptSource.SetDataSource(dsCreditAging);
		
			DataRow dr = dsQuery.Tables[0].Rows[0];
			string applicationid = ""; 
			string enterpriseid = ""; 
			string custid = ""; 
			string salesmanid = ""; 
			string status_active = ""; 
			string master_account = ""; 
			int ref_date_no = 0;
			bool isSummary = true; 
			bool isReport = true ;	

			if (Utility.IsNotDBNull(dr["applicationid"])) 
				applicationid = dr["applicationid"].ToString();
			if (Utility.IsNotDBNull(dr["enterpriseid"])) 
				enterpriseid = dr["enterpriseid"].ToString();
			if (Utility.IsNotDBNull(dr["custid"])) 
				custid = dr["custid"].ToString();
			if (Utility.IsNotDBNull(dr["salesmanid"])) 
				salesmanid = dr["salesmanid"].ToString();
			if (Utility.IsNotDBNull(dr["status_active"])) 
				status_active = dr["status_active"].ToString();
			if (Utility.IsNotDBNull(dr["master_account"])) 
				master_account = dr["master_account"].ToString();
			if (Utility.IsNotDBNull(dr["ref_date_no"])) 
				ref_date_no = Convert.ToInt16(dr["ref_date_no"].ToString());
			if (Utility.IsNotDBNull(dr["isSummary"])) 
				isSummary = Convert.ToBoolean(dr["isSummary"].ToString()); 
			if (Utility.IsNotDBNull(dr["isReport"])) 
				isReport = Convert.ToBoolean(dr["isReport"].ToString()); 

			//declare instance for parameterfields class
			ParameterFieldDefinitions paramFldDefs ;
			ParameterValues paramVals = new ParameterValues() ;
			ParameterDiscreteValue paramDVal = new ParameterDiscreteValue();
			paramFldDefs = rptSource.DataDefinition.ParameterFields;			
			
			foreach(ParameterFieldDefinition paramFldDef in paramFldDefs)
			{
				switch(paramFldDef.ParameterFieldName)
				{
					case "prmApplicationid":
						paramDVal.Value = applicationid;						
						break;
					case "prmEnterpriseid":
						paramDVal.Value = enterpriseid; 
						break;
					case "prmCustid":						
						paramDVal.Value = custid ; 										
						break;
					case "prmSalesmanid":	
						paramDVal.Value = salesmanid ; 					
						break;
					case "prmStatus_active":	
						paramDVal.Value = status_active ; 					
						break;
					case "prmMaster_account":	
						paramDVal.Value = master_account ;
						break;	
					case "prmRef_date_no":	
						paramDVal.Value = ref_date_no ;
						break;		
					case "prmUserId":	
						paramDVal.Value = strUserId ;
						break;
					default:
						continue;
				}
				paramVals = paramFldDef.CurrentValues;
				paramVals.Add(paramDVal);
				paramFldDef.ApplyCurrentValues(paramVals);
			}
		}

		private void SetCommodity()
		{
			String strCmdCode = null;
			String strCmdType = null;
			
			//declare instance for parameterfields class
			ParameterFieldDefinitions paramFldDefs ;
			ParameterValues paramVals = new ParameterValues() ;
			ParameterDiscreteValue paramDVal = new ParameterDiscreteValue();
			paramFldDefs = rptSource.DataDefinition.ParameterFields;

			DataRow dr = m_dsQuery.Tables[0].Rows[0];
		
	
		
			if (Utility.IsNotDBNull(dr["CommodityCode"])) 
				strCmdCode = dr["CommodityCode"].ToString();
			if (Utility.IsNotDBNull(dr["CommodityType"])) 
				strCmdType = dr["CommodityType"].ToString();
	
			foreach(ParameterFieldDefinition paramFldDef in paramFldDefs)
			{
				switch(paramFldDef.ParameterFieldName)
				{
					case "prmAppid":
						paramDVal.Value = strAppID;
						break;
					case "prmEnterpriseId":
						paramDVal.Value = strEnterpriseID; 
						break;
					case "prmCommodityCode":
						if (strCmdCode !="")
						{
							paramDVal.Value = strCmdCode ; 
						}
						else
						{
							paramDVal.Value = "*"; 
						}
						break;

					case "prmCommodityType":

						if (strCmdType == "HIGH VALUE") 
							strCmdType ="H";
						else if (strCmdType== "NORMAL")
							strCmdType ="N";
						else if (strCmdType== "PROHIBITED")
							strCmdType ="P";
						else
						{
							strCmdType ="*";
						}

						paramDVal.Value = strCmdType ; 
						break;

					case "prmUserId":
						paramDVal.Value = strUserId; 
						break;
					default:
						continue;
				}
				paramVals = paramFldDef.CurrentValues;
				paramVals.Add(paramDVal);
				paramFldDef.ApplyCurrentValues(paramVals);
			}

		}
		private void SetIndusSector()
		{
			String strIndusCode = null;
			//declare instance for parameterfields class
			ParameterFieldDefinitions paramFldDefs ;
			ParameterValues paramVals = new ParameterValues() ;
			ParameterDiscreteValue paramDVal = new ParameterDiscreteValue();
			paramFldDefs = rptSource.DataDefinition.ParameterFields;

			DataRow dr = m_dsQuery.Tables[0].Rows[0];
		
			if (Utility.IsNotDBNull(dr["IndustrialSectorCode"])) 
				strIndusCode = dr["IndustrialSectorCode"].ToString();
		
	
			foreach(ParameterFieldDefinition paramFldDef in paramFldDefs)
			{
				switch(paramFldDef.ParameterFieldName)
				{
					case "prmAppid":
						paramDVal.Value = strAppID;
						break;
					case "prmEnterpriseId":
						paramDVal.Value = strEnterpriseID; 
						break;
					case "prmIndusCode":
						paramDVal.Value = strIndusCode ; 
						break;
					case "prmUserId":
						paramDVal.Value = strUserId; 
						break;
					default:
						continue;
				}
				paramVals = paramFldDef.CurrentValues;
				paramVals.Add(paramDVal);
				paramFldDef.ApplyCurrentValues(paramVals);
			}
		}
		private void SetVas()
		{
			String strVasCode = null;
			//declare instance for parameterfields class
			ParameterFieldDefinitions paramFldDefs ;
			ParameterValues paramVals = new ParameterValues() ;
			ParameterDiscreteValue paramDVal = new ParameterDiscreteValue();
			paramFldDefs = rptSource.DataDefinition.ParameterFields;

			DataRow dr = m_dsQuery.Tables[0].Rows[0];
		
			if (Utility.IsNotDBNull(dr["VasCode"])) 
				strVasCode = dr["VasCode"].ToString();
		
	
			foreach(ParameterFieldDefinition paramFldDef in paramFldDefs)
			{
				switch(paramFldDef.ParameterFieldName)
				{
					case "prmAppid":
						paramDVal.Value = strAppID;
						break;
					case "prmEnterpriseId":
						paramDVal.Value = strEnterpriseID; 
						break;
					case "prmVasCode":
						paramDVal.Value = strVasCode ; 
						break;
					case "prmUserId":
						paramDVal.Value = strUserId; 
						break;
					default:
						continue;
				}
				paramVals = paramFldDef.CurrentValues;
				paramVals.Add(paramDVal);
				paramFldDef.ApplyCurrentValues(paramVals);
			}
		}
		private void SetService()
		{	
			String strServiceCode = null;
			int iTransitDays = 0;
			int iTransitHrs = 0;
			//declare instance for parameterfields class
			ParameterFieldDefinitions paramFldDefs ;
			ParameterValues paramVals = new ParameterValues() ;
			ParameterDiscreteValue paramDVal = new ParameterDiscreteValue();
			paramFldDefs = rptSource.DataDefinition.ParameterFields;

			DataRow dr = m_dsQuery.Tables[0].Rows[0];
		
			if (Utility.IsNotDBNull(dr["ServiceCode"])) 
				strServiceCode  = dr["ServiceCode"].ToString();
			if (Utility.IsNotDBNull(dr["TransitDays"])) 
				iTransitDays = Convert.ToInt32(dr["TransitDays"]);
			if (Utility.IsNotDBNull(dr["TransitHrs"])) 
				iTransitHrs = Convert.ToInt32(dr["TransitHrs"]);
		
	
			foreach(ParameterFieldDefinition paramFldDef in paramFldDefs)
			{
				switch(paramFldDef.ParameterFieldName)
				{
					case "prmAppid":
						paramDVal.Value = strAppID;
						break;
					case "prmEnterpriseId":
						paramDVal.Value = strEnterpriseID; 
						break;
					case "prmServiceCode":
						paramDVal.Value = strServiceCode ; 
						break;
					case "prmTransitDays":
						paramDVal.Value = iTransitDays ; 
						break;
					case "prmTransitHrs":
						paramDVal.Value = iTransitHrs ; 
						break;
					case "prmUserId":
						paramDVal.Value = strUserId; 
						break;
					default:
						continue;
				}
				paramVals = paramFldDef.CurrentValues;
				paramVals.Add(paramDVal);
				paramFldDef.ApplyCurrentValues(paramVals);

			}	
		}
		
		
				
		
		//boon -- 2011-09-14 -- start
		private void SetStaff()
		{	
			String strEmpId = null;
			//declare instance for parameterfields class
			ParameterFieldDefinitions paramFldDefs ;
			ParameterValues paramVals = new ParameterValues() ;
			ParameterDiscreteValue paramDVal = new ParameterDiscreteValue();
			paramFldDefs = rptSource.DataDefinition.ParameterFields;

			DataRow dr = m_dsQuery.Tables[0].Rows[0];
		
			if (Utility.IsNotDBNull(dr["EmpId"])) 
				strEmpId  = dr["EmpId"].ToString();		
	
			foreach(ParameterFieldDefinition paramFldDef in paramFldDefs)
			{
				switch(paramFldDef.ParameterFieldName)
				{
					case "prmAppid":
						paramDVal.Value = strAppID;
						break;
					case "prmEnterpriseId":
						paramDVal.Value = strEnterpriseID; 
						break;
					case "prmEmpId":
						paramDVal.Value = strEmpId ; 
						break;
					case "prmUserId":
						paramDVal.Value = strUserId; 
						break;
					default:
						continue;
				}
				paramVals = paramFldDef.CurrentValues;
				paramVals.Add(paramDVal);
				paramFldDef.ApplyCurrentValues(paramVals);
			}	
		}
		
		
		private void SetVehicle()
		{	
			String strTruckId = null;
			//declare instance for parameterfields class
			ParameterFieldDefinitions paramFldDefs ;
			ParameterValues paramVals = new ParameterValues() ;
			ParameterDiscreteValue paramDVal = new ParameterDiscreteValue();
			paramFldDefs = rptSource.DataDefinition.ParameterFields;

			DataRow dr = m_dsQuery.Tables[0].Rows[0];
		
			if (Utility.IsNotDBNull(dr["TruckId"])) 
				strTruckId  = dr["TruckId"].ToString();		
	
			foreach(ParameterFieldDefinition paramFldDef in paramFldDefs)
			{
				switch(paramFldDef.ParameterFieldName)
				{
					case "prmAppid":
						paramDVal.Value = strAppID;
						break;
					case "prmEnterpriseId":
						paramDVal.Value = strEnterpriseID; 
						break;
					case "prmTruckId":
						paramDVal.Value = strTruckId ; 
						break;
					case "prmUserId":
						paramDVal.Value = strUserId; 
						break;
					default:
						continue;
				}
				paramVals = paramFldDef.CurrentValues;
				paramVals.Add(paramDVal);
				paramFldDef.ApplyCurrentValues(paramVals);
			}	
		}		
		//boon -- 2011-09-14 -- end

		private void SetBaseZoneRates()
		{
			String strOrigCode = null,strDestCode = null;
			String strServiceCode=null;
		
			//declare instance for parameterfields class
			ParameterFieldDefinitions paramFldDefs ;
			ParameterValues paramVals = new ParameterValues() ;
			ParameterDiscreteValue paramDVal = new ParameterDiscreteValue();
			paramFldDefs = rptSource.DataDefinition.ParameterFields;

			DataRow dr = m_dsQuery.Tables[0].Rows[0];
		
			if (Utility.IsNotDBNull(dr["OriginZoneCode"])) 
				strOrigCode  = dr["OriginZoneCode"].ToString();
			if (Utility.IsNotDBNull(dr["DestZoneCode"])) 
				strDestCode = dr["DestZoneCode"].ToString();
			if (Utility.IsNotDBNull(dr["ServiceCode"])) 
				strServiceCode = dr["ServiceCode"].ToString();
			
			foreach(ParameterFieldDefinition paramFldDef in paramFldDefs)
			{
				switch(paramFldDef.ParameterFieldName)
				{
					case "prmAppid":
						paramDVal.Value = strAppID;
						break;
					case "prmEnterpriseId":
						paramDVal.Value = strEnterpriseID; 
						break;
					case "prmOrigCode":
						paramDVal.Value = strOrigCode ; 
						break;
					case "prmDestCode":
						paramDVal.Value = strDestCode ; 
						break;
					case "prmUserId":
						paramDVal.Value = strUserId; 
						break;
					case "prmServiceCode":
						paramDVal.Value = strServiceCode; 
						break;
					default:
						continue;
				}
				paramVals = paramFldDef.CurrentValues;
				paramVals.Add(paramDVal);
				paramFldDef.ApplyCurrentValues(paramVals);

			}	
		}

		private void SetZipCodeState()
		{
			String strCountry = null;
			String strState = null;
			String strZipCode = null;
			strState = null;
			String strStateNam = null;
			String strZoneCode = null,strDelvRouteCode = null,strPckRouteCode = null;
			DateTime dtStartDate = DateTime.Now;
			DateTime dtEndDate = DateTime.Now;
			
		

			//declare instance for parameterfields class
			ParameterFieldDefinitions paramFldDefs ;
			ParameterValues paramVals = new ParameterValues() ;
			ParameterDiscreteValue paramDVal = new ParameterDiscreteValue();
			paramFldDefs = rptSource.DataDefinition.ParameterFields;

			DataRow dr = m_dsQuery.Tables[0].Rows[0];
		
			if (Utility.IsNotDBNull(dr["From_Date"])) 
				dtStartDate = Convert.ToDateTime(dr["From_Date"]);
			else
				dtStartDate = System.DateTime.ParseExact("01/01/1900", "dd/MM/yyyy", null);
			if (Utility.IsNotDBNull(dr["To_Date"])) 
				dtEndDate = Convert.ToDateTime(dr["To_Date"]);
			else
				dtEndDate = System.DateTime.ParseExact("01/01/1900", "dd/MM/yyyy", null);
		
			if (Utility.IsNotDBNull(dr["Country"])) 
				strCountry = dr["Country"].ToString();
			if (Utility.IsNotDBNull(dr["ZipCode"])) 
				strZipCode =dr["ZipCode"].ToString();
			if (Utility.IsNotDBNull(dr["State"])) 
				strState = dr["State"].ToString();
			if (Utility.IsNotDBNull(dr["StateName"])) 
				strStateNam = dr["StateName"].ToString();
			if (Utility.IsNotDBNull(dr["ZoneCode"])) 
				strZoneCode = dr["ZoneCode"].ToString();
			if (Utility.IsNotDBNull(dr["DlvRouteCode"])) 
				strDelvRouteCode = dr["DlvRouteCode"].ToString();
			if (Utility.IsNotDBNull(dr["PckRouteCode"])) 
				strPckRouteCode = dr["PckRouteCode"].ToString();
			

			foreach(ParameterFieldDefinition paramFldDef in paramFldDefs)
			{
				switch(paramFldDef.ParameterFieldName)
				{
					case "prmAppid":
						paramDVal.Value = strAppID;
						break;
					case "prmEnterpriseId":
						paramDVal.Value = strEnterpriseID; 
						break;
					case "prmCountry":
						paramDVal.Value = strCountry; 
						break;
					case "prmZipCode":
						paramDVal.Value = strZipCode; 
						break;
					case "prmStateCode":
						paramDVal.Value = strState ; 
						break;
					case "prmStateName":
						paramDVal.Value = strStateNam ; 
						break;
					case "prmZoneCode":
						paramDVal.Value = strZoneCode ; 
						break;
					case "prmDelvRouteCode":
						paramDVal.Value = strDelvRouteCode ; 
						break;
					case "prmPckRouteCode":
						paramDVal.Value = strPckRouteCode ; 
						break;
					case "prmUserId":
						paramDVal.Value = strUserId ; 
						break;
					default:
						continue;
				}
			
				paramVals = paramFldDef.CurrentValues;
				paramVals.Add(paramDVal);
				paramFldDef.ApplyCurrentValues(paramVals);
				
			}

		}

		private void SetCustomerProfile()
		{
			String strPayerId = null,strPayerName = null;
			//			String strQtype="Q";
			String strC_PaymentMode = null,strC_Mbg = null,strC_StatusAct = null;
			String strC_IndustSect = null,strC_ApplyDimWt = null,strC_PODSlip = null,strC_EsaSurchg = null;
		
			//declare instance for parameterfields class
			ParameterFieldDefinitions paramFldDefs ;
			ParameterValues paramVals = new ParameterValues() ;
			ParameterDiscreteValue paramDVal = new ParameterDiscreteValue();
			paramFldDefs = rptSource.DataDefinition.ParameterFields;

			DataRow dr = m_dsQuery.Tables[0].Rows[0];
		
			if (Utility.IsNotDBNull(dr["PayerId"]) && dr["PayerId"].ToString() !="") 
				strPayerId  = dr["PayerId"].ToString();
			else
				strPayerId  ="*";

			if (Utility.IsNotDBNull(dr["PayerName"]) && dr["PayerName"].ToString()!="") 
				strPayerName = dr["PayerName"].ToString();
			else
				strPayerName = "*";

			if (Utility.IsNotDBNull(dr["PaymentMode"]) && dr["PaymentMode"].ToString() !="") 
				strC_PaymentMode = dr["PaymentMode"].ToString();
			else
				strC_PaymentMode = "*";

			if (Utility.IsNotDBNull(dr["Mbg"]) && dr["Mbg"].ToString() !="") 
				strC_Mbg = dr["Mbg"].ToString();
			else
				strC_Mbg ="*";

			if (Utility.IsNotDBNull(dr["StatusActive"]) && dr["StatusActive"].ToString()!="") 
				strC_StatusAct = dr["StatusActive"].ToString();
			else
				strC_StatusAct = "*";

			if (Utility.IsNotDBNull(dr["IndustSect"]) && dr["IndustSect"].ToString()!="") 
				strC_IndustSect = dr["IndustSect"].ToString();
			else
				strC_IndustSect = "*";

			if (Utility.IsNotDBNull(dr["ApplyDimWt"]) && dr["ApplyDimWt"].ToString()!="") 
				strC_ApplyDimWt = dr["ApplyDimWt"].ToString();
			else
				strC_ApplyDimWt = "*";

			if (Utility.IsNotDBNull(dr["PODSlip"]) && dr["PODSlip"].ToString()!="") 
				strC_PODSlip = dr["PODSlip"].ToString();
			else
				strC_PODSlip = "*";

			if (Utility.IsNotDBNull(dr["ESASurchg"]) && dr["ESASurchg"].ToString()!="") 
				strC_EsaSurchg = dr["ESASurchg"].ToString();
			else
				strC_EsaSurchg ="*";

			foreach(ParameterFieldDefinition paramFldDef in paramFldDefs)
			{
				switch(paramFldDef.ParameterFieldName)
				{
					case "prmAppid":
						paramDVal.Value = strAppID;
						break;
					case "prmEnterpriseId":
						paramDVal.Value = strEnterpriseID; 
						break;
						//					case "prmQType":
						//						paramDVal .Value =strQtype;
						//						break;
					case "prmPayerId":
						paramDVal .Value =strPayerId;
						break;
					case "prmUser-Id":
						paramDVal .Value =strUserId;
						break;
					case "prmPayerName":
						paramDVal .Value =strPayerName;
						break;
					case "prmPaymentMode":
						paramDVal .Value =strC_PaymentMode;
						break;
					case "prmMbg":
						paramDVal .Value =strC_Mbg;
						break;
					case "prmStatusActive":
						paramDVal .Value =strC_StatusAct;
						break;
					case "prmIndustSect":
						paramDVal .Value =strC_IndustSect;
						break;
					case "prmApplyDimWt":
						paramDVal .Value =strC_ApplyDimWt;
						break;
					case "prmPODSlip":
						paramDVal .Value =strC_PODSlip;
						break;
					case "prmEsaSurchg":
						paramDVal .Value =strC_EsaSurchg;
						break;
					default:
						continue;
				}
				paramVals = paramFldDef.CurrentValues;
				paramVals.Add(paramDVal);
				paramFldDef.ApplyCurrentValues(paramVals);

			}	
		}

		private void SetAgentProfile()
		{
			String strAgentId = null,strAgentName = null;
			String strA_PaymentMode = null,strA_Mbg = null,strA_StatusAct = null;
			String strA_IndustSect = null,strA_ApplyDimWt = null,strA_PODSlip = null,strA_EsaSurchg = null;
			//declare instance for parameterfields class
			ParameterFieldDefinitions paramFldDefs ;
			ParameterValues paramVals = new ParameterValues() ;
			ParameterDiscreteValue paramDVal = new ParameterDiscreteValue();
			paramFldDefs = rptSource.DataDefinition.ParameterFields;

			DataRow dr = m_dsQuery.Tables[0].Rows[0];
		
			if (Utility.IsNotDBNull(dr["AgentId"]) && dr["AgentId"].ToString() !="") 
				strAgentId   = dr["AgentId"].ToString();
			else
				strAgentId   ="*";

			if (Utility.IsNotDBNull(dr["AgentName"]) && dr["AgentName"].ToString() !="") 
				strAgentName = dr["AgentName"].ToString();
			else
				strAgentName="*";

			if (Utility.IsNotDBNull(dr["PaymentMode"]) && dr["PaymentMode"].ToString() !="") 
				strA_PaymentMode = dr["PaymentMode"].ToString();
			else
				strA_PaymentMode = "*";

			if (Utility.IsNotDBNull(dr["Mbg"]) && dr["Mbg"].ToString() !="") 
				strA_Mbg = dr["Mbg"].ToString();
			else
				strA_Mbg ="*";

			if (Utility.IsNotDBNull(dr["StatusActive"]) && dr["StatusActive"].ToString() !="") 
				strA_StatusAct = dr["StatusActive"].ToString();
			else
				strA_StatusAct ="*";

			if (Utility.IsNotDBNull(dr["IndustSect"]) && dr["IndustSect"].ToString() !="") 
				strA_IndustSect = dr["IndustSect"].ToString();
			else
				strA_IndustSect ="*";

			if (Utility.IsNotDBNull(dr["ApplyDimWt"]) && dr["ApplyDimWt"].ToString() !="") 
				strA_ApplyDimWt = dr["ApplyDimWt"].ToString();
			else
				strA_ApplyDimWt = "*";

			if (Utility.IsNotDBNull(dr["PODSlip"]) && dr["PODSlip"].ToString() !="") 
				strA_PODSlip = dr["PODSlip"].ToString();
			else
				strA_PODSlip = "*";

			if (Utility.IsNotDBNull(dr["ESASurchg"]) && dr["ESASurchg"].ToString()!="") 
				strA_EsaSurchg = dr["ESASurchg"].ToString();
			else
				strA_EsaSurchg = "*";
	
			foreach(ParameterFieldDefinition paramFldDef in paramFldDefs)
			{
				switch(paramFldDef.ParameterFieldName)
				{
					case "prmAppid":
						paramDVal.Value = strAppID;
						break;
					case "prmEnterpriseId":
						paramDVal.Value = strEnterpriseID; 
						break;
					case "prmUserId":
						paramDVal.Value =strUserId;
						break;
					case "prmAgentId":
						paramDVal.Value =strAgentId ;
						break;
					case "prmAgentName":
						paramDVal.Value =strAgentName;
						break;
					case "prmPaymentMode":
						paramDVal.Value =strA_PaymentMode;
						break;
					case "prmMbg":
						paramDVal.Value =strA_Mbg;
						break;
					case "prmStatusActive":
						paramDVal.Value =strA_StatusAct;
						break;
						//	case "prmIndustSect":
						//		paramDVal .Value =strA_IndustSect;
						//	break;
					case "prmApplyDimWt":
						paramDVal.Value =strA_ApplyDimWt;
						break;
					case "prmPODSlip":
						paramDVal.Value =strA_PODSlip;
						break;
					case "prmEsaSurchg":
						paramDVal.Value =strA_EsaSurchg;
						break;
					default:
						continue;
				}
				paramVals = paramFldDef.CurrentValues;
				paramVals.Add(paramDVal);
				paramFldDef.ApplyCurrentValues(paramVals);

			}	
		}

		private void SetAvgDlvyTimeParams()
		{
			//declare instance for parameterfields class
			ParameterValues paramvals = new ParameterValues();
			ParameterFieldDefinitions paramFlds; 
			ParameterDiscreteValue paramDiscVal = new ParameterDiscreteValue();
			ParameterRangeValue paramRangeVal = new ParameterRangeValue();

			//getting parameter field values
			DateTime dtStartDate, dtEndDate;
			String strPayerType="";
			String strPayerId="*"; 
			String strRouteType=""; 
			String strRouteCode="*";
			String strorigin_dc="*";
			String strdestination_dc="*";
			String strdelPathorigin_dc="*";
			String strdelPathdestination_dc="*";
			String strZipCode="*";
			String strStateCode="*";
			String strZoneCode="*";
			String strServiceCode="*";

			DataRow dr = m_dsQuery.Tables[0].Rows[0];

			if (Utility.IsNotDBNull(dr["start_date"])) 
				dtStartDate = Convert.ToDateTime(dr["start_date"]);
			else
				dtStartDate = System.DateTime.ParseExact("01/01/1900", "dd/MM/yyyy", null);
			if (Utility.IsNotDBNull(dr["end_date"])) 
				dtEndDate = Convert.ToDateTime(dr["end_date"]);
			else
				dtEndDate = System.DateTime.ParseExact("01/01/1900", "dd/MM/yyyy", null);
		
			if (Utility.IsNotDBNull(dr["payer_type"]))
				strPayerType = dr["payer_type"].ToString();
			if (Utility.IsNotDBNull(dr["payer_code"]))
				strPayerId = dr["payer_code"].ToString();

			if (Utility.IsNotDBNull(dr["route_type"]))
				strRouteType = dr["route_type"].ToString();
			if (Utility.IsNotDBNull(dr["route_code"]))
				strRouteCode = dr["route_code"].ToString();

			if (Utility.IsNotDBNull(dr["zip_code"]))
				strZipCode = dr["zip_code"].ToString();
			if (Utility.IsNotDBNull(dr["state_code"]))
				strStateCode = dr["state_code"].ToString();

			if (Utility.IsNotDBNull(dr["origin_dc"]))
				strorigin_dc = dr["origin_dc"].ToString();
			if (Utility.IsNotDBNull(dr["destination_dc"]))
				strdestination_dc = dr["destination_dc"].ToString();

			if (Utility.IsNotDBNull(dr["delPath_origin_dc"]))
				strdelPathorigin_dc = dr["delPath_origin_dc"].ToString();
			if (Utility.IsNotDBNull(dr["delPath_destination_dc"]))
				strdelPathdestination_dc = dr["delPath_destination_dc"].ToString();

			if (Utility.IsNotDBNull(dr["zone_code"]))
				strZoneCode = dr["zone_code"].ToString();
			if (Utility.IsNotDBNull(dr["service_code"]))
				strServiceCode = dr["service_code"].ToString();

			//get all parameter fields
			paramFlds = rptSource.DataDefinition.ParameterFields;

			//passing parameter for parameter fields
			foreach(ParameterFieldDefinition paramFld in paramFlds)
			{
				switch(paramFld.ParameterFieldName)
				{
					case "p_appid":
						paramDiscVal.Value = strAppID;
						break;
					case "p_enterpriseid":
						paramDiscVal.Value = strEnterpriseID;
						break;
					case "p_booking_date":
						paramRangeVal.StartValue = dtStartDate;
						paramRangeVal.EndValue = dtEndDate;
						break;
					case "p_state_code":
						paramDiscVal.Value = strStateCode;
						break;
					case "p_zone_code":
						paramDiscVal.Value = strZoneCode;
						break;
					case "p_service_code":
						paramDiscVal.Value = strServiceCode;
						break;
					case "p_userid":
						paramDiscVal.Value = utility.GetUserID();
						break;
					case "p_payer_type":
						paramDiscVal.Value = strPayerType;
						break;
					case "p_payer_Id":
						paramDiscVal.Value = strPayerId;
						break;
					case "p_route_type":
						paramDiscVal.Value = strRouteType;
						break;
					case "p_route_code":
						paramDiscVal.Value = strRouteCode;
						break;
					case "p_zip_code":
						paramDiscVal.Value = strZipCode;
						break;
					case "p_origin_dc":
						paramDiscVal.Value = strorigin_dc;
						break;
					case "p_destination_dc":
						paramDiscVal.Value = strdestination_dc;
						break;
					case "p_del_origin_dc":
						paramDiscVal.Value = strdelPathorigin_dc;
						break;
					case "p_del_destination_dc":
						paramDiscVal.Value = strdelPathdestination_dc;
						break;
					default:
						continue;
				}
				paramvals = paramFld.CurrentValues;
				if (paramFld.DiscreteOrRangeKind == DiscreteOrRangeKind.RangeValue)
					paramvals.Add(paramRangeVal);
				else 
					paramvals.Add(paramDiscVal);
				paramFld.ApplyCurrentValues(paramvals);
			}
		}

		private void SetShipmentPendingCODVerifyReportParams()
		{
			//declare instance for parameterfields class
			ParameterValues paramvals = new ParameterValues();
			ParameterFieldDefinitions paramFlds; 
			ParameterDiscreteValue paramDiscVal = new ParameterDiscreteValue();

			//getting parameter field values
			String strDateType="";
			DateTime dtStartDate, dtEndDate;
			String strPayerType="";
			String strPayerId="*"; 
			String strRouteType=""; 
			String strRouteCode="*";
			String strorigin_dc="*";
			String strdestination_dc="*";
			String strdelPathorigin_dc="*";
			String strdelPathdestination_dc="*";
			String strZipCode="*";
			String strStateCode="*";
			String strCODRefused="*";
		
			DataRow dr = m_dsQuery.Tables[0].Rows[0];
			strDateType = dr["tran_date"].ToString();
			if (Utility.IsNotDBNull(dr["start_date"])) 
				dtStartDate = (DateTime) dr["start_date"];
			else
				dtStartDate = System.DateTime.ParseExact("01/01/1900", "dd/MM/yyyy", null);
			if (Utility.IsNotDBNull(dr["end_date"])) 
				dtEndDate = (DateTime) dr["end_date"];
			else
				dtEndDate = System.DateTime.ParseExact("01/01/1900", "dd/MM/yyyy", null);
			if (Utility.IsNotDBNull(dr["payer_type"]))
				strPayerType = dr["payer_type"].ToString();
			if (Utility.IsNotDBNull(dr["payer_code"]))
				strPayerId = dr["payer_code"].ToString();

			if (Utility.IsNotDBNull(dr["route_type"]))
				strRouteType = dr["route_type"].ToString();
			if (Utility.IsNotDBNull(dr["route_code"]))
				strRouteCode = dr["route_code"].ToString();

			if (Utility.IsNotDBNull(dr["zip_code"]))
				strZipCode = dr["zip_code"].ToString();
			if (Utility.IsNotDBNull(dr["state_code"]))
				strStateCode = dr["state_code"].ToString();

			if (Utility.IsNotDBNull(dr["origin_dc"]))
				strorigin_dc = dr["origin_dc"].ToString();
			if (Utility.IsNotDBNull(dr["destination_dc"]))
				strdestination_dc = dr["destination_dc"].ToString();

			if (Utility.IsNotDBNull(dr["delPath_origin_dc"]))
				strdelPathorigin_dc = dr["delPath_origin_dc"].ToString();
			if (Utility.IsNotDBNull(dr["delPath_destination_dc"]))
				strdelPathdestination_dc = dr["delPath_destination_dc"].ToString();

			if (Utility.IsNotDBNull(dr["cod_refused"]) && dr["cod_refused"].ToString().Equals("A")==false)
				strCODRefused = dr["cod_refused"].ToString();

			//get all parameter fields
			paramFlds = rptSource.DataDefinition.ParameterFields;

			//passing parameter for parameter fields 
			foreach(ParameterFieldDefinition paramFld in paramFlds)
			{
				switch(paramFld.ParameterFieldName)
				{
					case "ApplicationID":
						paramDiscVal.Value = strAppID;
						break;
					case "EnterpriseID":
						paramDiscVal.Value = strEnterpriseID;
						break;
					case "DateType":
						paramDiscVal.Value = strDateType;
						break;
					case "FromDate":
						paramDiscVal.Value = dtStartDate;
						break;
					case "ToDate":
						paramDiscVal.Value = dtEndDate;
						break;
					case "PayerType":
						paramDiscVal.Value = strPayerType;
						break;
					case "PayerCode":
						paramDiscVal.Value = strPayerId;
						break;
					case "RouteType":
						paramDiscVal.Value = strRouteType;
						break;
					case "RouteCode":
						paramDiscVal.Value = strRouteCode;
						break;
					case "OriginDC":
						paramDiscVal.Value = strorigin_dc;
						break;
					case "DestinationDC":
						paramDiscVal.Value = strdestination_dc;
						break;
					case "PathOriginDC":
						paramDiscVal.Value = strdelPathorigin_dc;
						break;
					case "PathDestinationDC":
						paramDiscVal.Value = strdelPathdestination_dc;
						break;
					case "PostalCode":
						paramDiscVal.Value = strZipCode;
						break;
					case "DestinationState":
						paramDiscVal.Value = strStateCode;
						break;
					case "CODRefused":
						paramDiscVal.Value = strCODRefused;
						break;
					default:
						continue;
				}
				paramvals = paramFld.CurrentValues;
				paramvals.Add(paramDiscVal);
				paramFld.ApplyCurrentValues(paramvals);
			}
		}

		private void SetShipmentExportRptReportParams()
		{
			//declare instance for parameterfields class
			ParameterValues paramvals = new ParameterValues();
			ParameterFieldDefinitions paramFlds; 
			ParameterDiscreteValue paramDiscVal = new ParameterDiscreteValue();

			//getting parameter field values
			DateTime dtStartDate, dtEndDate;

			String strPayerType="";
			String strPayerId="*"; 
			
		
			DataRow dr = m_dsQuery.Tables[0].Rows[0];

			if (Utility.IsNotDBNull(dr["start_date"])) 
				dtStartDate = (DateTime) dr["start_date"];
			else
				dtStartDate = System.DateTime.ParseExact("01/01/1900", "dd/MM/yyyy", null);
			if (Utility.IsNotDBNull(dr["end_date"])) 
				dtEndDate = (DateTime) dr["end_date"];
			else
				dtEndDate = System.DateTime.ParseExact("01/01/1900", "dd/MM/yyyy", null);
			if (Utility.IsNotDBNull(dr["payer_type"]))
				strPayerType = dr["payer_type"].ToString();
			if (Utility.IsNotDBNull(dr["payer_code"]))
				strPayerId = dr["payer_code"].ToString();


			//get all parameter fields
			paramFlds = rptSource.DataDefinition.ParameterFields;

			//passing parameter for parameter fields 
			foreach(ParameterFieldDefinition paramFld in paramFlds)
			{
				switch(paramFld.ParameterFieldName)
				{
					case "Fromdate":
						paramDiscVal.Value = dtStartDate;
						break;
					case "Todate":
						paramDiscVal.Value = dtEndDate;
						break;
					case "p_payer_type":
						paramDiscVal.Value = strPayerType;
						break;
					case "p_payerid":
						paramDiscVal.Value = strPayerId;
						break;
					default:
						continue;
				}
				paramvals = paramFld.CurrentValues;
				paramvals.Add(paramDiscVal);
				paramFld.ApplyCurrentValues(paramvals);
			}
		}

		private void SetUtilMonitorConsignmentReportParams()
		{
			//declare instance for parameterfields class
			ParameterValues paramvals = new ParameterValues();
			ParameterFieldDefinitions paramFlds; 
			//ParameterFieldDefinition paramFld=null; 
			ParameterDiscreteValue paramDiscVal = new ParameterDiscreteValue();
			ParameterRangeValue paramRangeVal = new ParameterRangeValue();

			//getting parameter field values
			String strPayerId="*", strPayerType="";
			String strDateType="";
			String strRouteCode="*", strRouteType="";
			String strZipCode="*", strStateCode="*";
			String strorigin_dc="*",strdestination_dc="*";
			String strdelPathorigin_dc="*",strdelPathdestination_dc="*";
			DateTime dtStartDate, dtEndDate;
			
			DataRow dr = m_dsQuery.Tables[0].Rows[0];
			strDateType = dr["tran_date"].ToString();
			if (Utility.IsNotDBNull(dr["start_date"])) 
				dtStartDate = (DateTime) dr["start_date"];
			else
				dtStartDate = System.DateTime.ParseExact("01/01/1900", "dd/MM/yyyy", null);
			if (Utility.IsNotDBNull(dr["end_date"])) 
				dtEndDate = (DateTime) dr["end_date"];
			else
				dtEndDate = System.DateTime.ParseExact("01/01/1900", "dd/MM/yyyy", null);

			if (Utility.IsNotDBNull(dr["payer_type"]))
				strPayerType = dr["payer_type"].ToString();
			if (Utility.IsNotDBNull(dr["payer_code"]))
				strPayerId = dr["payer_code"].ToString();

			if (Utility.IsNotDBNull(dr["route_type"]))
				strRouteType = dr["route_type"].ToString();
			if (Utility.IsNotDBNull(dr["route_code"]))
				strRouteCode = dr["route_code"].ToString();

			if (Utility.IsNotDBNull(dr["zip_code"]))
				strZipCode = dr["zip_code"].ToString();
			if (Utility.IsNotDBNull(dr["state_code"]))
				strStateCode = dr["state_code"].ToString();

			if (Utility.IsNotDBNull(dr["delPath_origin_dc"]))
				strdelPathorigin_dc = dr["delPath_origin_dc"].ToString();
			if (Utility.IsNotDBNull(dr["delPath_destination_dc"]))
				strdelPathdestination_dc = dr["delPath_destination_dc"].ToString();

			if (Utility.IsNotDBNull(dr["origin_dc"]))
				strorigin_dc = dr["origin_dc"].ToString();
			if (Utility.IsNotDBNull(dr["destination_dc"]))
				strdestination_dc = dr["destination_dc"].ToString();

			//get all parameter fields
			paramFlds = rptSource.DataDefinition.ParameterFields;

			//passing parameter for parameter fields
			foreach(ParameterFieldDefinition paramFld in paramFlds)
			{
				switch(paramFld.ParameterFieldName)
				{
					case "ApplicationID":
						paramDiscVal.Value = strAppID;
						break;
					case "EnterpriseID":
						paramDiscVal.Value = strEnterpriseID;
						break;
					case "FromDate":
						paramDiscVal.Value = dtStartDate;
						break;
					case "ToDate":
						paramDiscVal.Value = dtEndDate;
						break;
					case "PayerType":
						paramDiscVal.Value = strPayerType;
						break;
					case "PayerCode":
						paramDiscVal.Value = strPayerId;
						break;
					case "RouteType": 
						paramDiscVal.Value = strRouteType;
						break;
					case "RouteCode":
						paramDiscVal.Value = strRouteCode;
						break;
					case "OriginDC":
						paramDiscVal.Value = strorigin_dc;
						break;
					case "DestinationDC":
						paramDiscVal.Value = strdestination_dc;
						break;
					case "PathOriginDC":
						paramDiscVal.Value = strdelPathorigin_dc;
						break;
					case "PathDestinationDC":
						paramDiscVal.Value = strdelPathdestination_dc;
						break;
					case "PostalCode":
						paramDiscVal.Value = strZipCode;
						break;
					case "DestinationState":
						paramDiscVal.Value = strStateCode;
						break;
					case "prmUserId":
						paramDiscVal.Value = strUserId; 
						break;
					default:
						continue;
				}
				paramvals = paramFld.CurrentValues;
				paramvals.Add(paramDiscVal);
				paramFld.ApplyCurrentValues(paramvals);
			}
		}
	

		private void btnMoveFirst_Click(object sender, System.EventArgs e)
		{	
			rptViewer.ShowFirstPage();
		}

		private void btnMovePrevious_Click(object sender, System.EventArgs e)
		{
			if (txtGoTo.Text.Trim() !="")
			{
				rptViewer.ShowNthPage(System.Convert.ToInt32(txtGoTo.Text)+1);
				txtGoTo.Text="";
			} 
			rptViewer.ShowPreviousPage();			
		}

		private void btnMoveNext_Click(object sender, System.EventArgs e)
		{
			if (txtGoTo.Text.Trim() !="")
			{
				rptViewer.ShowNthPage(System.Convert.ToInt32(txtGoTo.Text)-1);
				txtGoTo.Text="";
			} 			
			rptViewer.ShowNextPage();
		}

		private void btnMoveLast_Click(object sender, System.EventArgs e)
		{			
			rptViewer.ShowLastPage();	
		}

		public void LoadExportFormatList()
		{
			ddbExport.Items.Clear();			
			ArrayList systemCodes = Utility.GetCodeValues(strAppID,utility.GetUserCulture(),"export",CodeValueType.StringValue);
			foreach(SystemCode sysCode in systemCodes)
			{	
				ListItem lstItem = new ListItem();
				lstItem.Text = sysCode.Text;
				lstItem.Value = sysCode.StringValue;
				ddbExport.Items.Add(lstItem);
			}
		}

		private void btnExport_Click(object sender, System.EventArgs e)
		{
			ExportOptions ExportOptions = new ExportOptions();
			DiskFileDestinationOptions DiskFileDstOptions = new DiskFileDestinationOptions();			
			String strExportFile =null;
			
			
			if (ddbExport.SelectedItem.Value.Trim()=="Formats:")
			{
				return;
			}

			string strExportFolder=System.Configuration.ConfigurationSettings.AppSettings["ReportExportFolder"];
			strExportFolder=Server.MapPath(strExportFolder);
			if (! System.IO.Directory.Exists(strExportFolder))
			{
				lblErrorMesssage.Text="Export folder not found. Please create the folder :'"+strExportFolder+"' and export";
				return;
			}

			///************IMPORTANT:Get the Export File directory from Web.config, Permissions must be given for
			///************"aspnet" user on the Folder read from "ReportExportFolder"
			strExportFile = strExportFolder+"\\"+Session.SessionID.ToString()+DateTime.Now.Day.ToString()+DateTime.Now.Hour.ToString()+DateTime.Now.Minute.ToString()+DateTime.Now.Second.ToString();
			
			//			strExportFile = "C:\\" + Session.SessionID.ToString();
			
			DiskFileDstOptions.DiskFileName = strExportFile;
			ExportOptions = rptSource.ExportOptions;			
			ExportOptions.DestinationOptions = DiskFileDstOptions;
			ExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;    

			switch (ddbExport.SelectedItem.Value.Trim().ToString())
			{
				case ".pdf":				
					ExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;					
					ExportOptions.FormatOptions=new PdfRtfWordFormatOptions();
					break;
				case ".doc":
					ExportOptions.ExportFormatType = ExportFormatType.WordForWindows;					
					ExportOptions.FormatOptions=new PdfRtfWordFormatOptions();
					break;
				case ".rtf":
					ExportOptions.ExportFormatType = ExportFormatType.RichText;					
					ExportOptions.FormatOptions=new PdfRtfWordFormatOptions();
					break;
				case ".xls":
					if (ddbExport.SelectedItem.Text.IndexOf("Data",1)>0)
					{
						ExportOptions.ExportFormatType = ExportFormatType.ExcelRecord;
					}
					else
					{
						ExportOptions.ExportFormatType = ExportFormatType.Excel;
					}	
					//BY X MAY 30 08
					ExcelFormatOptions objExcelOptions = new ExcelFormatOptions();
					
					//if(rptSource.GetType().Equals(new TIES.WebUI.InvoicePrintingDetail()))
					ExportOptions.FormatOptions = objExcelOptions;
					//END BY X MAY 30 08
					//					ExportOptions.FormatOptions=new ExcelFormatOptions(); COMMENTED BY X MAY 30 08
					break;
					//				case ".rpt":
					//					ExportOptions.ExportFormatType = ExportFormatType.CrystalReport;	
					//					ExportOptions.FormatOptions=new PdfRtfWordFormatOptions();
					//					break;
					//				case ".htm":
					//					ExportOptions.ExportFormatType = ExportFormatType.HTML40;										
					//					HTMLFormatOptions htmlOpt=new HTMLFormatOptions();
					//					htmlOpt.HTMLBaseFolderName="C:\\Crystal\\";
					//                    htmlOpt.HTMLFileName=strExportFile="C:\\Crystal\\"+ Session.SessionID.ToString() + ddbExport.SelectedItem.Value.Trim();
					//					htmlOpt.HTMLHasPageNavigator = true;
					//					ExportOptions.FormatOptions=htmlOpt;
					//					break;
	
				default:
					ExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;					
					ExportOptions.FormatOptions=new PdfRtfWordFormatOptions();
					break;
			}		
			

			int numPages=0,numFiles=0,pagePerFile=40;

			if(ViewState["maxPage"]!=null)
				numPages = Int32.Parse(ViewState["maxPage"].ToString());

			numFiles = (int)(Math.Ceiling((double)numPages/(double)pagePerFile));
			if(rptViewer.ToolTip=="InvoicePrintingDetail"&&numFiles>1&&ddbExport.SelectedItem.Value.Trim().ToString()==".xls")
			{

				for(int i =1;i<=numFiles;i++)
				{
					DiskFileDstOptions.DiskFileName = strExportFile+i;

					ExcelFormatOptions objExcelOptions = new ExcelFormatOptions();

					//objExcelOptions.ExcelUseConstantColumnWidth =true;
					//objExcelOptions.ExcelConstantColumnWidth = 1400;
					objExcelOptions.UsePageRange = true;

					objExcelOptions.FirstPageNumber = (pagePerFile*(i-1)) +1;
					if(i*pagePerFile>numPages)
						objExcelOptions.LastPageNumber = numPages;
					else
						objExcelOptions.LastPageNumber = i*pagePerFile; 

					ExportOptions.FormatOptions = objExcelOptions;

					rptSource.Export();
				}

			}
			else
				rptSource.Export(); 
			
			Response.ClearContent();
			Response.ClearHeaders();	

			switch (ddbExport.SelectedItem.Value.Trim().ToString())
			{
				case ".pdf":
					Response.ContentType = "application/pdf";
					break;
				case ".doc":
				case ".rtf":
					Response.ContentType = "application/msword";
					break;
				case ".xls":
					Response.ContentType = "application/x-msexcel";
					break;
				case ".htm":
					Response.ContentType = "text/html";
					break;
			}

			//by X jul 01 08
			string inv_no = "";
			if(ViewState["invoice_no"]!=null)
				inv_no = ViewState["invoice_no"].ToString();
			if(rptViewer.ToolTip=="InvoicePrintingDetail"&&numFiles>1&&ddbExport.SelectedItem.Value.Trim().ToString()==".xls")
			{
				string[] aFilenames = new string[numFiles];// = Directory.GetFiles(strExportFolder);

				// the name of the Zip File is the second Parameter passed in calling

				for(int i =1;i<=numFiles;i++)
				{
					Directory.Move(strExportFile+i,strExportFile+i+".xls");
					aFilenames[i-1]= strExportFile+i+".xls";
				}

				ZipOutputStream s = new ZipOutputStream(File.Create(strExportFile+".zip"));

				// Set compression level: 0 [none] - 9 [highest]
				s.SetLevel(5); 

				for (int i=0; i < aFilenames.Length; i++)
				{
					FileStream fs = File.OpenRead(aFilenames[i]);
      
					// normally, the Buffer is allocated once,
					// here we do it once per File for clarity's sake
					byte[] buffer = new byte[fs.Length];
					fs.Read(buffer, 0, buffer.Length);

					// and now we write a ZipEntry & the Data
					ZipEntry entry = new ZipEntry(inv_no.ToString()+"." + (i+1)+".xls");
					s.PutNextEntry(entry);
					s.Write(buffer, 0, buffer.Length);
				}
    
				s.Finish();
				s.Close();

				Response.ContentType="application/x-zip-compressed";
				Response.AddHeader("Content-Disposition", "attachment;filename="+strExportFile+".zip");
				Response.WriteFile(strExportFile+".zip");
				Response.Flush();
				Response.End();

				for(int i =1;i<=numFiles;i++)
				{
					System.IO.File.Delete(strExportFile+i);
					System.IO.File.Delete(strExportFile+i+".xls");
				}

				System.IO.File.Delete(strExportFile+".zip");   

			}
			else
			{
				Response.WriteFile(strExportFile);
				Response.Flush();
				Response.End();
				System.IO.File.Delete(strExportFile);
			}
			
			//To Print 3 Copies
			//rptSource.PrintToPrinter(3, true, 1, 1);		
			Response.Close();
			rptSource.Close();
			rptSource.Dispose();		
		}


		private void btnShowGrpTree_Click(object sender, System.EventArgs e)
		{
			if (!bShowGroupTree)
			{
				if(!utility.GetUserCulture().ToUpper().Equals("EN-US"))
				{
					btnShowGrpTree.Text = Utility.GetLanguageText(ResourceType.ScreenLabel, "Hide Group Tree", utility.GetUserCulture());
				}
				else
				{
					btnShowGrpTree.Text = "Hide Group Tree";
				}
				bShowGroupTree = true;
				rptViewer.DisplayGroupTree = bShowGroupTree;				
				ViewState["ShowGroupTree"] = bShowGroupTree;
			}
			else
			{
				if(!utility.GetUserCulture().ToUpper().Equals("EN-US"))
				{
					btnShowGrpTree.Text = Utility.GetLanguageText(ResourceType.ScreenLabel, "Show Group Tree", utility.GetUserCulture());
				}
				else
				{
					btnShowGrpTree.Text = "Show Group Tree";
				}
				bShowGroupTree = false;
				rptViewer.DisplayGroupTree = bShowGroupTree;				
				ViewState["ShowGroupTree"] = bShowGroupTree;
			}
		}
		

		private void btnZoom_Click(object sender, System.EventArgs e)
		{	
			if(System.Convert.ToInt32(ddbZoom.SelectedItem.Value)<125)
			{ // by Ching Dec 14,2007
				switch (strFORMID) 
				{   
					case "ShipmentPendingCOD":  
						rptViewer.Width=1200;
						break;
					case "CODAmountCollectedReport": 
						rptViewer.Width=1200;
						break;
					case "CODRemittanceReport": 
						rptViewer.Width=1200;
						break;
					case "SHIPMENT PENDING REPORT":  
						rptViewer.Width=1200;
						break;
						//					case "ShipmentTrackingQueryRpt":  
						//						rptViewer.Width=1200;
						//						break;
					case "PODReportQuery":  
						rptViewer.Width=1200;
						break;
					default : 
						rptViewer.Width=800;
						break;

				} 

				rptViewer.Height=1200; // End
			}
			else if(System.Convert.ToInt32(ddbZoom.SelectedItem.Value)==125)
			{ // by Ching Dec 14,2007
				switch (strFORMID) 
				{   
					case "ShipmentPendingCOD":  
						rptViewer.Width=1400;
						break;
					case "CODAmountCollectedReport": 
						rptViewer.Width=1400;
						break;
					case "CODRemittanceReport": 
						rptViewer.Width=1400;
						break;
					case "SHIPMENT PENDING REPORT":  
						rptViewer.Width=1400;
						break;
						//					case "ShipmentTrackingQueryRpt":  
						//						rptViewer.Width=1400;
						//						break;
					case "PODReportQuery":  
						rptViewer.Width=1400;
						break;
					default : 
						rptViewer.Width=900;
						break;
				} 
				
				//				rptViewer.Width=1000;
				rptViewer.Height=1500; // End
			}
			else if(System.Convert.ToInt32(ddbZoom.SelectedItem.Value)==150)
			{ // by Ching Dec 14,2007
				switch (strFORMID) 
				{   
					case "ShipmentPendingCOD":  
						rptViewer.Width=1700;
						break;
					case "CODAmountCollectedReport": 
						rptViewer.Width=1700;
						break;
					case "CODRemittanceReport": 
						rptViewer.Width=1700;
						break;
					case "SHIPMENT PENDING REPORT":  
						rptViewer.Width=1700;
						break;
						//					case "ShipmentTrackingQueryRpt":  
						//						rptViewer.Width=1700;
						//						break;
					case "PODReportQuery":  
						rptViewer.Width=1700;
						break;
					default : 
						rptViewer.Width=1000;
						break;
				} 

				//				rptViewer.Width=1200;
				rptViewer.Height=1850;
			}
			else if(System.Convert.ToInt32(ddbZoom.SelectedItem.Value)==175)
			{ // by Ching Dec 14,2007
				switch (strFORMID) 
				{   
					case "ShipmentPendingCOD":  
						rptViewer.Width=2000;
						break;
					case "CODAmountCollectedReport": 
						rptViewer.Width=2000;
						break;
					case "CODRemittanceReport": 
						rptViewer.Width=2000;
						break;
					case "SHIPMENT PENDING REPORT":  
						rptViewer.Width=2000;
						break;
						//					case "ShipmentTrackingQueryRpt":  
						//						rptViewer.Width=2000;
						//						break;
					case "PODReportQuery":  
						rptViewer.Width=2000;
						break;
					default : 
						rptViewer.Width=1100;
						break;
				} 

				//				rptViewer.Width=1400;
				rptViewer.Height=2000;
			}
			else if(System.Convert.ToInt32(ddbZoom.SelectedItem.Value)==200)
			{ // by Ching Dec 14,2007
				switch (strFORMID) 
				{   
					case "ShipmentPendingCOD":  
						rptViewer.Width=2300;
						break;
					case "CODAmountCollectedReport": 
						rptViewer.Width=2300;
						break;
					case "CODRemittanceReport": 
						rptViewer.Width=2300;
						break;
					case "SHIPMENT PENDING REPORT":  
						rptViewer.Width=2300;
						break;
						//					case "ShipmentTrackingQueryRpt":  
						//						rptViewer.Width=2300;
						//						break;
					case "PODReportQuery":  
						rptViewer.Width=2300;
						break;
					default : 
						rptViewer.Width=1200;
						break;
				} 

				//				rptViewer.Width=1600;
				rptViewer.Height=2200;
			}
			int iZoomValue=System.Convert.ToInt32(ddbZoom.SelectedItem.Value);
			rptViewer.Zoom (iZoomValue);			
			
		}

		private void btnSearch_Click(object sender, System.EventArgs e)
		{
			String strSearchText=txtTextToSearch.Text.Trim();
			if (strSearchText=="")
			{
				return;
			}			
			rptViewer.SearchForText(strSearchText,SearchDirection.Forward);	
		}

		private void rptViewer_Navigate(object source, CrystalDecisions.Web.NavigateEventArgs e)
		{
			//			lblMessage.Text= "Page : " + e.NewPageNumber.ToString() + " Of " + iLastPageNumber.ToString();
			if (ViewState["maxPage"] == null)
			{
				ViewState.Add("maxPage", e.NewPageNumber);
			}
		}

		private void ddbExport_SelectedIndexChanged(object sender, System.EventArgs e)
		{
		
		}
			
		private void localizeReportObjects()
		{
			if (rptSource != null)
			{
				Sections sections = rptSource.ReportDefinition.Sections;
				foreach (Section section in sections)
				{
					ReportObjects repObjs = section.ReportObjects;
					foreach (ReportObject repObj in repObjs)
					{
						if (repObj.Kind == ReportObjectKind.TextObject)
						{
							TextObject txtObj = (TextObject) repObj;
							txtObj.Text = Utility.GetLanguageText(ResourceType.ScreenLabel, txtObj.Text.Trim(), utility.GetUserCulture());
						}
					}
				}
			}
		}

		private void saveReportObjects(string repName)
		{
			if (repName.IndexOf("/") >= 0)
			{
				repName = repName.Replace("/", "_");
			}
			try
			{
				string saveDir = @"C:\Inetpub\Report Objects";
				FileStream fs = new FileStream(saveDir + Path.DirectorySeparatorChar + repName + ".txt", FileMode.Create, FileAccess.Write);
				StreamWriter sw = new StreamWriter(fs);
				if (rptSource != null)
				{
					/*Sections sections = rptSource.ReportDefinition.Sections;
					foreach (Section section in sections)
					{
						ReportObjects repObjs = section.ReportObjects;
						foreach (ReportObject repObj in repObjs)
						{
							if (repObj.Kind == ReportObjectKind.TextObject)
							{
								TextObject txtObj = (TextObject) repObj;
								sw.WriteLine(txtObj.Text);
							}
						}
					}*/
					Areas areas = rptSource.ReportDefinition.Areas;
					foreach (Area area in areas)
					{
						Sections sections = area.Sections;
						foreach (Section section in sections)
						{
							ReportObjects repObjs = section.ReportObjects;
							foreach (ReportObject repObj in repObjs)
							{
								if (repObj.Kind == ReportObjectKind.TextObject)
								{
									TextObject txtObj = (TextObject) repObj;
									sw.WriteLine(txtObj.Text);
								}
							}
						}
					}
				}
				sw.Close();
			}
			catch(Exception ex)
			{
				ex.ToString();
			}
		}
	}
}
