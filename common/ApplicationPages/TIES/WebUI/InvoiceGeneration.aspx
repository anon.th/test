<%@ Page language="c#" Codebehind="InvoiceGeneration.aspx.cs" AutoEventWireup="false" Inherits="com.ties.InvoiceGeneration" SmartNavigation="false" %>
<%@ Import Namespace="Cambro.Web.DbCombo" %>
<%@ Register TagPrefix="dbCombo" Namespace="Cambro.Web.DbCombo" Assembly="Cambro.Web.DbCombo" %>
<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>InvoiceGeneration</title>
		<LINK href="css/Styles.css" type="text/css" rel="stylesheet" name="stylesheet">
		<META content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<META content="C#" name="CODE_LANGUAGE">
		<META content="JavaScript" name="vs_defaultClientScript">
		<META content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema"> <!--#INCLUDE FILE="msFormValidations.inc"-->
		<SCRIPT language="javascript" src="Scripts/settingScrollPosition.js"></SCRIPT>
	</HEAD>
	<BODY onclick="GetScrollPosition();" onload="SetScrollPosition();" onunload="window.top.displayBanner.fnCloseAll(0);"
		MS_POSITIONING="GridLayout">
		<DIV id="divInvoiceGen" style="Z-INDEX: 101; LEFT: 7px; WIDTH: 619px; POSITION: absolute; TOP: 7px; HEIGHT: 421px"
			MS_POSITIONING="GridLayout" runat="server">
			<FORM id="InvoiceGeneration" method="post" runat="server">
				<asp:button id="btnGenerate" style="Z-INDEX: 100; LEFT: 99px; POSITION: absolute; TOP: 48px"
					runat="server" Width="104px" Text="Generate" CssClass="queryButton"></asp:button><asp:button id="btnClear" style="Z-INDEX: 101; LEFT: 24px; POSITION: absolute; TOP: 48px" runat="server"
					Width="74px" Text="Clear" CssClass="queryButton"></asp:button><asp:label id="lblErrorMessage" style="Z-INDEX: 105; LEFT: 19px; POSITION: absolute; TOP: 80px"
					runat="server" Width="556px" CssClass="errorMsgColor" Height="30px"></asp:label>&nbsp;
				<TABLE id="tblShipmentExcepPODRepQry" style="Z-INDEX: 103; LEFT: 20px; WIDTH: 484px; POSITION: absolute; TOP: 115px"
					width="484" border="0" runat="server">
					<TR width="100%">
						<TD vAlign="top" width="100%">
							<FIELDSET><LEGEND><asp:label id="Label2" CssClass="tableHeadingFieldset" Runat="server">Invoice Generation</asp:label></LEGEND>
								<TABLE id="tblDates" style="LEFT: 1px; TOP: 2px" width="100%" align="left" border="0" runat="server">
									<TR height="14">
										<TD width="5%"></TD>
										<TD width="5%"></TD>
										<TD width="5%"></TD>
										<TD width="5%"></TD>
										<TD width="5%"></TD>
										<TD style="WIDTH: 42px" width="42"></TD>
										<TD width="5%"></TD>
										<TD width="5%"></TD>
										<TD width="5%"></TD>
										<TD width="5%"></TD>
										<TD width="5%"></TD>
										<TD width="5%"></TD>
										<TD width="5%"></TD>
										<TD width="5%"></TD>
										<TD width="5%"></TD>
										<TD width="5%"></TD>
										<TD width="5%"></TD>
										<TD width="5%"></TD>
										<TD width="5%"></TD>
										<TD width="5%"></TD>
									</TR>
									<TR height="27">
										<TD style="WIDTH: 124px" colSpan="6"><asp:label id="lblInvoiceFor" runat="server" Width="91px" CssClass="tablelabel">Payment Mode</asp:label></TD>
										<TD colSpan="8"><asp:radiobutton id="rbCashConsig" runat="server" Width="100%" Text="Cash Consignments" CssClass="tableRadioButton"
												Height="22px" Checked="True" GroupName="InvoiceFor" AutoPostBack="True"></asp:radiobutton></TD>
										<TD colSpan="4"></TD>
										<TD colSpan="12"></TD>
									</TR>
									<TR>
										<TD style="WIDTH: 124px" colSpan="6"></TD>
										<TD colSpan="16"><FONT face="Tahoma">
												<P dir="ltr" style="MARGIN-RIGHT: 0px">&nbsp;&nbsp;&nbsp;
													<asp:checkbox id="chkShipByCash" runat="server" Width="254px" Text="Shipped by Cash Customers"
														CssClass="tableRadioButton" AutoPostBack="True"></asp:checkbox></P>
											</FONT>
										</TD>
									</TR>
									<TR>
										<TD style="WIDTH: 124px" colSpan="6"></TD>
										<TD colSpan="16"><FONT face="Tahoma">&nbsp;&nbsp;&nbsp;
												<asp:checkbox id="chkShipByCredit" runat="server" Width="250px" Text="Shipped by Credit Customers"
													CssClass="tableRadioButton"></asp:checkbox></FONT></TD>
									</TR>
									<TR>
										<TD style="WIDTH: 124px" colSpan="6"></TD>
										<TD colSpan="16"><asp:radiobutton id="rbCreditConsig" runat="server" Width="316px" Text="Credit Consignments" CssClass="tableRadioButton"
												Height="22px" GroupName="InvoiceFor" AutoPostBack="True"></asp:radiobutton></TD>
									</TR>
									<TR height="27">
										<TD style="WIDTH: 124px" colSpan="6"><asp:label id="lblCustomerCode" runat="server" Width="100%" CssClass="tableLabel" Height="22px">Customer Code</asp:label></TD>
										<TD colSpan="8" Width="210"><dbcombo:dbcombo id="dbCmbAgentId" tabIndex="1" runat="server" Width="210px" Height="17px" AutoPostBack="True"
												ReQueryOnLoad="True" ShowDbComboLink="False" TextUpLevelSearchButton="v" ServerMethod="AgentIdServerMethod" TextBoxColumns="20"></dbcombo:dbcombo>
											<dbcombo:dbcombo id="dbCmbAgentId_Cash" tabIndex="1" runat="server" Width="210px" Height="17px" AutoPostBack="True"
												TextBoxColumns="20" ServerMethod="AgentIdCashServerMethod" TextUpLevelSearchButton="v" ShowDbComboLink="False"
												ReQueryOnLoad="True"></dbcombo:dbcombo></TD>
										<TD colSpan="1"></TD>
										<TD colSpan="5"></TD>
									</TR>
									<TR height="27">
										<TD style="WIDTH: 124px" colSpan="6"><asp:label id="lblCustomerName" runat="server" Width="100%" CssClass="tableLabel" Height="22px">Customer Name</asp:label></TD>
										<TD colSpan="14"><cc1:mstextbox id="txtCustomerName" runat="server" Width="100%" CssClass="textField" TextMaskType="msUpperAlfaNumericWithUnderscore"
												MaxLength="100"></cc1:mstextbox></TD>
									</TR>
									<TR height="27">
										<TD style="WIDTH: 124px" colSpan="6"><asp:label id="lblInvoiceToDate" runat="server" Width="100%" CssClass="tableLabel" Height="22px">Last Pickup Date</asp:label></TD>
										<TD colSpan="6"><cc1:mstextbox id="txtLastPickupDate" runat="server" Width="100%" CssClass="textField" TextMaskType="msDate"
												MaxLength="10" TextMaskString="99/99/9999"></cc1:mstextbox></TD>
										<TD colSpan="13"></TD>
									</TR>
									<TR height="27">
										<TD style="WIDTH: 124px" colSpan="6"></TD>
										<TD colSpan="2"></TD>
										<TD colSpan="5"></TD>
										<TD align="right" colSpan="2"></TD>
										<TD colSpan="5"></TD>
									</TR>
									<TR height="14">
										<TD colSpan="20"></TD>
									</TR>
								</TABLE>
							</FIELDSET>
						</TD>
					</TR>
				</TABLE>
				<asp:label id="lblTitle" style="Z-INDEX: 102; LEFT: 20px; POSITION: absolute; TOP: 9px" runat="server"
					Width="558px" CssClass="maintitleSize" Height="32px">Invoice Generation</asp:label><input 
type=hidden value="<%=strScrollPosition%>" name=ScrollPosition>
			</FORM>
		</DIV>
		<DIV id="divInvoiceGenPreview" style="Z-INDEX: 102; LEFT: 24px; WIDTH: 1139px; POSITION: absolute; TOP: 7px; HEIGHT: 600px"
			MS_POSITIONING="GridLayout" runat="server">
			<form id="Form1" method="post" runat="server">
				<FONT face="Tahoma">
					<asp:label id="lblMainTitle" style="Z-INDEX: 100; LEFT: 8px; POSITION: absolute; TOP: 8px"
						runat="server" Width="477px" CssClass="mainTitleSize" Height="26px">Invoice Generation Preview</asp:label>
					<asp:datagrid id="dgPreview" OnUpdateCommand="dgPreview_SaveCust" style="Z-INDEX: 115; LEFT: 5px; POSITION: absolute; TOP: 291px"
						runat="server" Width="890px" Height="95px" ItemStyle-Height="20" AutoGenerateColumns="False"
						SelectedItemStyle-CssClass="gridFieldSelected">
						<SelectedItemStyle CssClass="gridFieldSelected"></SelectedItemStyle>
						<ItemStyle Height="10px" CssClass="gridField"></ItemStyle>
						<Columns>
							<asp:TemplateColumn>
								<HeaderStyle Width="20px" CssClass="gridBlueHeading"></HeaderStyle>
								<ItemTemplate>
									<asp:CheckBox Checked='<%#DataBinder.Eval(Container.DataItem,"CHECKED").ToString().Equals("Y")? true : false %>' OnCheckedChanged="dgPreview_CheckChanged" ID="chkSelect" Runat="server" AutoPostBack="true">
									</asp:CheckBox>
								</ItemTemplate>
							</asp:TemplateColumn>
							<asp:BoundColumn DataField="consignment_no" HeaderText="Consignment No.">
								<HeaderStyle HorizontalAlign="Center" CssClass="gridBlueHeading"></HeaderStyle>
								<ItemStyle CssClass="gridField"></ItemStyle>
							</asp:BoundColumn>
							<asp:BoundColumn DataField="ref_no" HeaderText="Ref No.">
								<HeaderStyle HorizontalAlign="Center" CssClass="gridBlueHeading"></HeaderStyle>
								<ItemStyle CssClass="gridField"></ItemStyle>
							</asp:BoundColumn>
							<asp:TemplateColumn HeaderText="Customer/ Sender">
								<HeaderStyle HorizontalAlign="Center" Width="230px" CssClass="gridBlueHeading"></HeaderStyle>
								<ItemStyle Wrap="False" CssClass="gridField" VerticalAlign="Middle"></ItemStyle>
								<ItemTemplate>
									<asp:DropDownList Width="200px" CssClass="gridDropDown" ID="ddlSCShipmentUpdate" Runat="server"></asp:DropDownList>
									<asp:ImageButton CommandName="Update" id="imgSave" runat="server" ImageUrl="images/butt-update.gif"></asp:ImageButton>
									<asp:Label ID="dgLabelSenderName" visible=False runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Sender_Name")%>' />
								</ItemTemplate>
							</asp:TemplateColumn>
							<asp:BoundColumn DataField="sender_zipcode" HeaderText="Origin">
								<HeaderStyle HorizontalAlign="Center" CssClass="gridBlueHeading"></HeaderStyle>
								<ItemStyle HorizontalAlign="Left" CssClass="gridField"></ItemStyle>
							</asp:BoundColumn>
							<asp:BoundColumn DataField="act_pickup_datetime" HeaderText="Pickup" DataFormatString="{0:dd/MM/yyyy HH:mm}">
								<HeaderStyle HorizontalAlign="Center" CssClass="gridBlueHeading"></HeaderStyle>
								<ItemStyle HorizontalAlign="Left" CssClass="gridField"></ItemStyle>
							</asp:BoundColumn>
							<asp:BoundColumn DataField="recipient_name" HeaderText="Recipient">
								<HeaderStyle HorizontalAlign="Center" CssClass="gridBlueHeading"></HeaderStyle>
								<ItemStyle HorizontalAlign="Left" CssClass="gridField"></ItemStyle>
							</asp:BoundColumn>
							<asp:BoundColumn DataField="recipient_zipcode" HeaderText="Dest.">
								<HeaderStyle HorizontalAlign="Center" CssClass="gridBlueHeading"></HeaderStyle>
								<ItemStyle HorizontalAlign="Left" CssClass="gridField"></ItemStyle>
							</asp:BoundColumn>
							<asp:BoundColumn DataField="act_delivery_date" HeaderText="Del. D/T" DataFormatString="{0:dd/MM/yyyy HH:mm}">
								<HeaderStyle HorizontalAlign="Center" CssClass="gridBlueHeading"></HeaderStyle>
								<ItemStyle HorizontalAlign="Left" CssClass="gridField"></ItemStyle>
							</asp:BoundColumn>
							<asp:BoundColumn DataField="service_code" HeaderText="Service">
								<HeaderStyle HorizontalAlign="Center" CssClass="gridBlueHeading"></HeaderStyle>
								<ItemStyle HorizontalAlign="Center" CssClass="gridField"></ItemStyle>
							</asp:BoundColumn>
							<asp:BoundColumn DataField="tot_freight_charge" HeaderText="Freight" DataFormatString="{0:#,##0.00}">
								<HeaderStyle HorizontalAlign="Center" CssClass="gridBlueHeading"></HeaderStyle>
								<ItemStyle HorizontalAlign="Right" CssClass="gridField"></ItemStyle>
							</asp:BoundColumn>
							<asp:BoundColumn DataField="insurance_surcharge" HeaderText="Insurance" DataFormatString="{0:#,##0.00}">
								<HeaderStyle HorizontalAlign="Center" CssClass="gridBlueHeading"></HeaderStyle>
								<ItemStyle HorizontalAlign="Right" CssClass="gridField"></ItemStyle>
							</asp:BoundColumn>
							<asp:BoundColumn DataField="other_surch_amount" HeaderText="Other" DataFormatString="{0:#,##0.00}">
								<HeaderStyle HorizontalAlign="Center" CssClass="gridBlueHeading"></HeaderStyle>
								<ItemStyle HorizontalAlign="Right" CssClass="gridField"></ItemStyle>
							</asp:BoundColumn>
							<asp:BoundColumn DataField="tot_vas_surcharge" HeaderText="VAS" DataFormatString="{0:#,##0.00}">
								<HeaderStyle HorizontalAlign="Center" CssClass="gridBlueHeading"></HeaderStyle>
								<ItemStyle HorizontalAlign="Right" CssClass="gridField"></ItemStyle>
							</asp:BoundColumn>
							<asp:BoundColumn DataField="esa_surcharge" HeaderText="ESA" DataFormatString="{0:#,##0.00}">
								<HeaderStyle HorizontalAlign="Center" CssClass="gridBlueHeading"></HeaderStyle>
								<ItemStyle HorizontalAlign="Right" CssClass="gridField"></ItemStyle>
							</asp:BoundColumn>
							<asp:BoundColumn DataField="last_exception_code" HeaderText="PODEX Code">
								<HeaderStyle HorizontalAlign="Center" CssClass="gridBlueHeading"></HeaderStyle>
								<ItemStyle HorizontalAlign="Center" CssClass="gridField"></ItemStyle>
							</asp:BoundColumn>
						</Columns>
						<PagerStyle NextPageText="Next" PrevPageText="Previous" HorizontalAlign="Left" CssClass="normalText"></PagerStyle>
					</asp:datagrid>
					<TABLE id="Table10" style="Z-INDEX: 114; LEFT: 7px; WIDTH: 727px; POSITION: absolute; TOP: 95px; HEIGHT: 139px"
						cellSpacing="1" width="727" align="left" border="0" runat="server">
						<TR height="25">
							<TD style="WIDTH: 96px" vAlign="middle" align="left" width="96" bgColor="#008499">&nbsp;
								<asp:label id="Label5" runat="server" Width="88px" Height="15px" Font-Size="11px" BackColor="#008499"
									ForeColor="White">Invoice Type</asp:label></TD>
							<TD style="WIDTH: 9px" vAlign="top" width="9"></TD>
							<TD style="WIDTH: 1001px" vAlign="middle" width="1001"><asp:label id="lblInvoiceType" runat="server" Width="143px" CssClass="tableLabel"></asp:label></TD>
							<TD style="WIDTH: 148px" align="left" width="148">&nbsp;</TD>
							<TD style="WIDTH: 8px" width="8"></TD>
							<TD style="WIDTH: 61px" width="61"></TD>
						</TR>
						<TR height="25">
							<TD style="WIDTH: 96px" vAlign="middle" align="left" width="96" bgColor="#008499">&nbsp;
								<asp:label id="Label50" runat="server" Width="88px" CssClass="tableLabel" Height="16px" Font-Size="11px"
									BackColor="#008499" ForeColor="White">Customer ID</asp:label></TD>
							<TD style="WIDTH: 9px" vAlign="top" width="9"></TD>
							<TD style="WIDTH: 1001px" vAlign="middle" width="1001"><asp:label id="lblCustID" runat="server" Width="143px" CssClass="tableLabel"></asp:label></TD>
							<TD style="WIDTH: 148px" align="left" width="148" bgColor="#008499">&nbsp;
								<asp:label id="Label6" runat="server" Width="81px" CssClass="tableLabel" Height="15px" Font-Size="11px"
									BackColor="#008499" ForeColor="White"> Province</asp:label></TD>
							<TD style="WIDTH: 8px" width="8"></TD>
							<TD style="WIDTH: 61px" width="61"><asp:label id="lblProvince" runat="server" Width="133px" CssClass="tableLabel"></asp:label></TD>
						</TR>
						<TR height="25">
							<TD style="WIDTH: 96px; HEIGHT: 25px" align="left" width="96" bgColor="#008499">&nbsp;
								<asp:label id="Label48" runat="server" Width="96px" CssClass="tableLabel" Height="15px" Font-Size="11px"
									BackColor="#008499" ForeColor="White">Customer Name</asp:label></TD>
							<TD style="WIDTH: 9px; HEIGHT: 25px" width="9"></TD>
							<TD style="WIDTH: 1001px; HEIGHT: 25px" vAlign="middle" width="1001"><asp:label id="lblCustName" runat="server" Width="260px" CssClass="tableLabel"></asp:label></TD>
							<TD style="WIDTH: 148px; HEIGHT: 25px" align="left" width="148" bgColor="#008499">&nbsp;
								<asp:label id="Label49" runat="server" Width="93px" CssClass="tableLabel" Height="14px" Font-Size="11px"
									BackColor="#008499" ForeColor="White"> Telephone</asp:label></TD>
							<TD style="WIDTH: 8px; HEIGHT: 25px" width="8"></TD>
							<TD style="WIDTH: 61px; HEIGHT: 25px" width="61"><asp:label id="lblTelephone" runat="server" Width="133px" CssClass="tableLabel"></asp:label></TD>
						</TR>
						<TR height="25">
							<TD style="WIDTH: 96px" vAlign="middle" align="left" width="96" bgColor="#008499">&nbsp;
								<asp:label id="Label45" runat="server" Width="90px" CssClass="tableLabel" Height="15px" Font-Size="11px"
									BackColor="#008499" ForeColor="White"> Address 1</asp:label></TD>
							<TD style="WIDTH: 9px" width="9"></TD>
							<TD style="WIDTH: 1001px" width="1001"><asp:label id="lblAddress1" runat="server" Width="303px" CssClass="tableLabel"></asp:label></TD>
							<TD style="WIDTH: 148px" align="left" width="148" bgColor="#008499">&nbsp;
								<asp:label id="Label47" tabIndex="100" runat="server" Width="83px" CssClass="tableLabel" Height="13px"
									Font-Size="11px" BackColor="#008499" ForeColor="White">Fax</asp:label></TD>
							<TD style="WIDTH: 8px" width="8"></TD>
							<TD style="WIDTH: 61px" width="61"><asp:label id="lblFax" runat="server" Width="133px" CssClass="tableLabel"></asp:label></TD>
						</TR>
						<TR height="25">
							<TD style="WIDTH: 96px" width="96" bgColor="#008499">&nbsp;
								<asp:label id="Label1" tabIndex="100" runat="server" Width="88px" CssClass="tableLabel" Height="16px"
									Font-Size="11px" BackColor="#008499" ForeColor="White">Address 2</asp:label></TD>
							<TD style="WIDTH: 9px" width="9"></TD>
							<TD style="WIDTH: 1001px" width="1001"><asp:label id="lblAddress2" runat="server" Width="301px" CssClass="tableLabel"></asp:label></TD>
							<TD style="WIDTH: 148px" width="148" bgColor="#008499">&nbsp;</TD>
							<TD style="WIDTH: 8px" align="left" width="8"></TD>
							<TD style="WIDTH: 61px" align="left" width="61"><asp:label id="lblHC_POD" runat="server" Width="127px" CssClass="tableLabel" Font-Size="11px"></asp:label></TD>
						</TR>
						<TR height="25">
							<TD style="WIDTH: 96px" vAlign="middle" width="96" bgColor="#008499">&nbsp;
								<asp:label id="Label3" tabIndex="100" runat="server" Width="99px" CssClass="tableLabel" Height="16px"
									Font-Size="11px" BackColor="#008499" ForeColor="White">Postal Code</asp:label></TD>
							<TD style="WIDTH: 9px" width="9"></TD>
							<TD style="WIDTH: 1001px" width="1001"><asp:label id="lblPostalCode" runat="server" Width="143px" CssClass="tableLabel"></asp:label></TD>
							<TD style="WIDTH: 148px" width="148" bgColor="#008499">&nbsp;
								<asp:label id="Label46" tabIndex="100" runat="server" Width="108px" CssClass="tableLabel" Height="16px"
									Font-Size="11px" BackColor="#008499" ForeColor="White">HC POD Required</asp:label></TD>
							<TD style="WIDTH: 8px" align="left" width="8"></TD>
							<TD style="WIDTH: 61px" align="left" width="61"><asp:label id="lblHC_POD_Required" runat="server" Width="127px" CssClass="tableLabel" Font-Size="11px"></asp:label></TD>
						</TR>
					</TABLE>
					<asp:button id="btnSelectAll" style="Z-INDEX: 113; LEFT: 7px; POSITION: absolute; TOP: 267px"
						runat="server" Width="102px" Text="Select All" CssClass="queryButton" CausesValidation="False"></asp:button><asp:button id="btnExport" style="Z-INDEX: 116; LEFT: 109px; POSITION: absolute; TOP: 267px"
						runat="server" CssClass="queryButton" Text="Export" Width="102px" Visible="False"></asp:button><asp:button id="btnCancel" style="Z-INDEX: 101; LEFT: 11px; POSITION: absolute; TOP: 37px" runat="server"
						Width="61px" Text="Cancel" CssClass="queryButton" CausesValidation="False"></asp:button><asp:button id="btnGenerateInv" style="Z-INDEX: 102; LEFT: 74px; POSITION: absolute; TOP: 37px"
						runat="server" Width="130px" Text="Generate" CssClass="queryButton" CausesValidation="False"></asp:button><asp:button id="btnSelectAllHC" style="Z-INDEX: 103; LEFT: 205px; POSITION: absolute; TOP: 37px"
						runat="server" Width="163px" Text="Select all HC Returned" CssClass="queryButton"></asp:button><asp:button id="btnGoToFirstPage" style="Z-INDEX: 104; LEFT: 597px; POSITION: absolute; TOP: 35px"
						runat="server" Width="24px" Text="|<" CssClass="queryButton" CausesValidation="False"></asp:button><asp:button id="btnPreviousPage" style="Z-INDEX: 105; LEFT: 622px; POSITION: absolute; TOP: 35px"
						runat="server" Width="24px" Text="<" CssClass="queryButton" CausesValidation="False"></asp:button><cc1:mstextbox id="txtGoToRec" style="Z-INDEX: 106; LEFT: 648px; POSITION: absolute; TOP: 37px"
						tabIndex="8" runat="server" Width="28px" CssClass="textFieldRightAlign" AutoPostBack="True" TextMaskType="msNumeric" MaxLength="5" Enabled="True" NumberScale="0" NumberPrecision="8" NumberMinValue="1"
						NumberMaxValue="999999" Wrap="False"></cc1:mstextbox><asp:button id="btnNextPage" style="Z-INDEX: 107; LEFT: 677px; POSITION: absolute; TOP: 36px"
						runat="server" Width="24px" Text=">" CssClass="queryButton" CausesValidation="False"></asp:button><asp:button id="btnGoToLastPage" style="Z-INDEX: 108; LEFT: 702px; POSITION: absolute; TOP: 36px"
						runat="server" Width="24px" Text=">|" CssClass="queryButton" CausesValidation="False"></asp:button><asp:label id="lblErrorMsg" style="Z-INDEX: 109; LEFT: 18px; POSITION: absolute; TOP: 62px"
						runat="server" Width="537px" CssClass="errorMsgColor" Height="19px"></asp:label><asp:label id="lblNumRec" style="Z-INDEX: 110; LEFT: 538px; POSITION: absolute; TOP: 60px"
						runat="server" Width="187px" CssClass="RecMsg" Height="19px"></asp:label><asp:validationsummary id="PageValidationSummary" style="Z-INDEX: 111; LEFT: 14px; POSITION: absolute; TOP: 62px"
						runat="server" Width="346px" Height="39px" Visible="True" ShowSummary="False" ShowMessageBox="True"></asp:validationsummary></FONT></form>
		</DIV>
	</BODY>
</HTML>
