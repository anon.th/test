<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<%@ Page language="c#" Codebehind="SalesPersonProfile.aspx.cs" AutoEventWireup="false" Inherits="com.ties.SalesPersonProfile" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>SalesPersonProfile</title>
		<LINK href="css/Styles.css" type="text/css" rel="stylesheet" name="stylesheet">
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<!--#INCLUDE FILE="msFormValidations.inc"-->
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="SalesPersonProfile" method="post" runat="server">
			<asp:ValidationSummary id="Pagevalid" style="Z-INDEX: 106; LEFT: 359px; POSITION: absolute; TOP: 4px" runat="server" ShowMessageBox="True" ShowSummary="False" HeaderText="Please enter the missing fields."></asp:ValidationSummary>
			<asp:datagrid id="dgSalesman" style="Z-INDEX: 101; LEFT: 14px; POSITION: absolute; TOP: 125px" runat="server" OnPageIndexChanged="OnSalesman_PageChange" OnItemDataBound="OnItemBound_Salesman" AllowCustomPaging="True" AllowPaging="True" OnDeleteCommand="OnDelete_Salesman" AutoGenerateColumns="False" OnCancelCommand="OnCancel_Salesman" OnEditCommand="OnEdit_Salesman" OnUpdateCommand="OnUpdate_Salesman" Width="766px">
				<SelectedItemStyle CssClass="gridFieldSelected"></SelectedItemStyle>
				<ItemStyle Height="20px" CssClass="gridField"></ItemStyle>
				<Columns>
					<asp:EditCommandColumn ButtonType="LinkButton" UpdateText="&lt;IMG SRC='images/butt-update.gif' border=0 title='Update' &gt;" CancelText="&lt;IMG SRC='images/butt-cancel.gif' border=0 title='Cancel' &gt;" EditText="&lt;IMG SRC='images/butt-edit.gif' border=0 title='Edit' &gt;">
						<HeaderStyle Width="2%" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle CssClass="gridField"></ItemStyle>
					</asp:EditCommandColumn>
					<asp:ButtonColumn Text="&lt;IMG SRC='images/butt-delete.gif' border=0 title='Delete' &gt;" ButtonType="LinkButton" CommandName="Delete">
						<HeaderStyle HorizontalAlign="Center" Width="3%" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle HorizontalAlign="Center" CssClass="gridField"></ItemStyle>
					</asp:ButtonColumn>
					<asp:TemplateColumn HeaderText="Salesman ID">
						<HeaderStyle Width="20%" CssClass="gridHeading" Font-Bold="True"></HeaderStyle>
						<ItemStyle CssClass="gridField"></ItemStyle>
						<ItemTemplate>
							<asp:Label CssClass="gridLabel" ID="lblSalesID" Visible=True Text='<%#DataBinder.Eval(Container.DataItem,"salesmanid")%>' Runat="server">
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<cc1:mstextbox CssClass="gridTextBox" ID="txtSalesID" Visible=True Text='<%#DataBinder.Eval(Container.DataItem,"salesmanid")%>' Runat="server" TextMaskType="msUpperAlfaNumericWithUnderscore" MaxLength="12">
							</cc1:mstextbox>
							<asp:RequiredFieldValidator ID="reqSalesID" Runat="server" BorderWidth="0" Display="None" ControlToValidate="txtSalesID" ErrorMessage="Salesman Id "></asp:RequiredFieldValidator>
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Sales Reference">
						<HeaderStyle Width="20%" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle CssClass="gridField"></ItemStyle>
						<ItemTemplate>
							<asp:Label CssClass="gridLabel" ID="lblRef" Visible=True Text='<%#DataBinder.Eval(Container.DataItem,"salesman_ref")%>' Runat="server">
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<asp:TextBox CssClass="gridTextBox" ID="txtRef" Visible=True Text='<%#DataBinder.Eval(Container.DataItem,"salesman_ref")%>' Runat="server" MaxLength="12">
							</asp:TextBox>
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Salesman Name">
						<HeaderStyle Width="20%" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle CssClass="gridField"></ItemStyle>
						<ItemTemplate>
							<asp:Label CssClass="gridLabel" ID="lblName" Visible=True Text='<%#DataBinder.Eval(Container.DataItem,"salesman_name")%>' Runat="server">
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<asp:TextBox CssClass="gridTextBox" ID="txtName" Visible=True Text='<%#DataBinder.Eval(Container.DataItem,"salesman_name")%>' Runat="server" TextMaskType="msUpperAlfaNumericWithUnderscore" MaxLength="100">
							</asp:TextBox>
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Commission">
						<HeaderStyle Width="15%" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle CssClass="gridField"></ItemStyle>
						<ItemTemplate>
							<asp:Label CssClass="gridLabel" ID="lblCommission" Visible=True Text='<%#DataBinder.Eval(Container.DataItem,"commission","{0:n}")%>' Runat="server">
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<cc1:mstextbox CssClass="gridTextBox" ID="txtCommission" Visible=True Text='<%#DataBinder.Eval(Container.DataItem,"commission","{0:n}")%>' Runat="server" TextMaskType="msNumeric" NumberMaxValue="100" NumberMinValue="0" NumberPrecision="5" NumberScale="2">
							</cc1:mstextbox>
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Telephone">
						<HeaderStyle Width="10%" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle CssClass="gridField"></ItemStyle>
						<ItemTemplate>
							<asp:Label CssClass="gridLabel" ID="lblTelephone" Visible=True Text='<%#DataBinder.Eval(Container.DataItem,"telephone")%>' Runat="server">
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<cc1:mstextbox CssClass="gridTextBox" ID="txtTelephone" Visible=True Text='<%#DataBinder.Eval(Container.DataItem,"telephone")%>' Runat="server" TextMaskType="msUpperAlfaNumericWithUnderscore" MaxLength="20">
							</cc1:mstextbox>
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Mobile">
						<HeaderStyle Width="10%" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle CssClass="gridField"></ItemStyle>
						<ItemTemplate>
							<asp:Label CssClass="gridLabel" ID="lblMobile" Visible=True Text='<%#DataBinder.Eval(Container.DataItem,"mobile_phone")%>' Runat="server">
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<cc1:mstextbox CssClass="gridTextBox" ID="txtMobile" Visible=True Text='<%#DataBinder.Eval(Container.DataItem,"mobile_phone")%>' Runat="server" TextMaskType="msUpperAlfaNumericWithUnderscore" MaxLength="20">
							</cc1:mstextbox>
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Email">
						<HeaderStyle Width="15%" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle CssClass="gridField"></ItemStyle>
						<ItemTemplate>
							<asp:Label CssClass="gridLabel" ID="lblEmail" Visible=True Text='<%#DataBinder.Eval(Container.DataItem,"email")%>' Runat="server">
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<asp:TextBox CssClass="gridTextBox" ID="txtEmail" Visible=True Text='<%#DataBinder.Eval(Container.DataItem,"email")%>' Runat="server" MaxLength="50">
							</asp:TextBox>
							<asp:RegularExpressionValidator ID="regEmailValidate" ControlToValidate="txtEmail" Runat="server" Display="None" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ErrorMessage="Invalid Email"></asp:RegularExpressionValidator>
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Designation">
						<HeaderStyle Width="25%" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle CssClass="gridField"></ItemStyle>
						<ItemTemplate>
							<asp:Label CssClass="gridLabel" ID="lblDesignation" Visible=True Text='<%#DataBinder.Eval(Container.DataItem,"designation")%>' Runat="server">
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<asp:TextBox CssClass="gridTextBox" ID="txtDesignation" Visible=True Text='<%#DataBinder.Eval(Container.DataItem,"designation")%>' Runat="server" MaxLength="100">
							</asp:TextBox>
						</EditItemTemplate>
					</asp:TemplateColumn>
				</Columns>
				<PagerStyle NextPageText="Next" PrevPageText="Previous" HorizontalAlign="Right" CssClass="normalText"></PagerStyle>
			</asp:datagrid><asp:button id="btnExecuteQuery" style="Z-INDEX: 102; LEFT: 72px; POSITION: absolute; TOP: 63px" runat="server" Text="Execute Query" CausesValidation="False" Width="133px" CssClass="queryButton"></asp:button><asp:button id="btnQuery" style="Z-INDEX: 103; LEFT: 18px; POSITION: absolute; TOP: 63px" runat="server" Text="Query" CausesValidation="False" Width="60px" CssClass="queryButton"></asp:button><asp:button id="btnInsert" style="Z-INDEX: 104; LEFT: 204px; POSITION: absolute; TOP: 63px" runat="server" Text="Insert" CausesValidation="False" CssClass="queryButton"></asp:button><asp:label id="lblSalesPerson" style="Z-INDEX: 105; LEFT: 17px; POSITION: absolute; TOP: 11px" runat="server" Width="637px" Height="43px" CssClass="mainTitleSize">Sales Person Profile</asp:label>
			<asp:Label id="lblErrMsg" style="Z-INDEX: 106; LEFT: 22px; POSITION: absolute; TOP: 96px" runat="server" Width="722px" Height="21px" CssClass="errorMsgColor"></asp:Label>
		</form>
	</body>
</HTML>
