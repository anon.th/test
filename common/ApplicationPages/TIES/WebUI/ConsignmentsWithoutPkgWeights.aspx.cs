using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using TIESDAL;
using com.common.classes;
using com.common.DAL;
using com.common.RBAC;
using com.ties.DAL;
using com.common.util;

using com.common.applicationpages;
using com.ties;
using System.Text;
using System.Globalization;
using System.IO;

namespace TIES.WebUI
{
	/// <summary>
	/// Summary description for ConsignmentsWithoutPkgWeights.
	/// </summary>
	public class ConsignmentsWithoutPkgWeights : BasePage
	{
		protected System.Web.UI.WebControls.ValidationSummary Validationsummary1;
		protected System.Web.UI.WebControls.Label Label1;
		protected System.Web.UI.WebControls.Button btnQry;
		protected System.Web.UI.WebControls.Button btnExecQry;
		protected System.Web.UI.WebControls.Button btnDelete;
		protected System.Web.UI.WebControls.Label lblErrorMsg;
		protected System.Web.UI.WebControls.Label lbl1;
		protected System.Web.UI.WebControls.Label Label3;
		protected System.Web.UI.WebControls.DropDownList Drp_Location;
		protected com.common.util.msTextBox Txt_BatchDate;
		protected System.Web.UI.WebControls.DataGrid dgLodgments;
		protected System.Web.UI.HtmlControls.HtmlGenericControl divMain;
		protected System.Web.UI.HtmlControls.HtmlTable MainTable;
		protected System.Web.UI.WebControls.Button btnHidReport;
	
		private string m_strAppID;
		private string m_strEnterpriseID;
		private int GridPageSize
		{
			get
			{
				if(ViewState["GridPageSize"] == null)
				{
					return 50;
				}
				else
				{
					return (int)ViewState["GridPageSize"];
				}
			}
			set
			{
				ViewState["GridPageSize"]=value;
			}
		}

		private string PageRefresh
		{
			get
			{
				if(ViewState["PageRefresh"] == null)
				{
					return "60";
				}
				else
				{
					return (string)ViewState["PageRefresh"];
				}
			}
			set
			{
				ViewState["PageRefresh"]=value;
			}
		}

		private string BatchDate
		{
			get
			{
				if(ViewState["BatchDate"] == null)
				{
					return "";
				}
				else
				{
					return (string)ViewState["BatchDate"];
				}
			}
			set
			{
				ViewState["BatchDate"]=value;
			}
		}
		private System.Data.DataSet DbDataSource
		{
			get
			{
				if(ViewState["DbDataSource"] == null)
				{
					return null;
				}
				else
				{
					return (DataSet)ViewState["DbDataSource"];
				}
			}
			set
			{
				ViewState["DbDataSource"]=value;
			}
		}

		
		//Consistant variable
		private const string formRefresh = "FormRefreshRateSec";
		private const string PageSize = "GridRowsPerPage";

		private void Page_Load(object sender, System.EventArgs e)
		{			
			try
			{
				m_strAppID = utility.GetAppID();
				m_strEnterpriseID = utility.GetEnterpriseID();
				if(!Page.IsPostBack)
				{			
					PageRefresh = ConsignmentsWithoutPkgWeightsDAL.QueryEnterpriseConfigurations(m_strAppID,m_strEnterpriseID,formRefresh);
					Response.AppendHeader("Refresh", PageRefresh);
					GridPageSize= Int32.Parse(ConsignmentsWithoutPkgWeightsDAL.QueryEnterpriseConfigurations(m_strAppID,m_strEnterpriseID,PageSize));
					ResetControl();
					SetInitialSelect(Txt_BatchDate);				
					SetDataPagerefresh();
				}
			
			}
			catch(Exception ex)
			{
				lblErrorMsg.Enabled = true;
				lblErrorMsg.Text = ex.Message;
			}
			
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			base.OnInit(e);
			InitializeComponent();
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnQry.Click += new System.EventHandler(this.btnQry_Click);
			this.btnExecQry.Click += new System.EventHandler(this.btnExecQry_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private string DateDbFormat(string localDate)
		{
			string serverdate="";
			if(localDate.Trim() != "")
			{
				try
				{
					DateTime dBatchDate = DateTime.ParseExact(localDate.Trim(),"dd/MM/yyyy",null);
					serverdate = dBatchDate.ToString("yyyy-MM-dd");
				}
				catch
				{
					serverdate = null;
				}				
			}
			return serverdate;
		}

		public static void SetInitialSelect(Control control) 
		{ 
			if (control.Page == null) 
			{ 
				throw new ArgumentException( 
					"The Control must be added to a Page before you can set the IntialFocus to it."); 
			} 
			if (control.Page.Request.Browser.JavaScript == true) 
			{ 
				// Create JavaScript 
				StringBuilder s = new StringBuilder(); 
				s.Append("\n<SCRIPT LANGUAGE='JavaScript'>\n"); 
				s.Append("<!--\n"); 
				s.Append("function SetInitialFocus()\n"); 
				s.Append("{\n"); 
				s.Append("   document."); 

				// Find the Form 
				Control p = control.Parent; 
				while (!(p is System.Web.UI.HtmlControls.HtmlForm)) 
					p = p.Parent; 
				s.Append(p.ClientID); 

				s.Append("['"); 
				s.Append(control.UniqueID); 
				s.Append("'].select();\n"); 
				s.Append("}\n"); 
				s.Append("// -->\n"); 
				s.Append("window.onload = SetInitialFocus;\n"); 
				s.Append("</SCRIPT>"); 

				// Register Client Script 
				control.Page.RegisterClientScriptBlock("InitialFocus", s.ToString()); 
			} 
		} 

		private void BindLocation()
		{
			Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings,HttpContext.Current.Session);
			string strAppID = util.GetAppID();
			string strEnterpriseID = util.GetEnterpriseID();

			DataSet dataset = com.ties.classes.DbComboDAL.UserLocationQuery(strAppID,strEnterpriseID);	
			this.Drp_Location.DataSource = dataset;
			Drp_Location.DataTextField = dataset.Tables[0].Columns["origin_code"].ToString();
			Drp_Location.DataValueField = dataset.Tables[0].Columns["origin_code"].ToString();
			this.Drp_Location.DataBind();
			Drp_Location.Items.Insert(0,new ListItem("",""));

		}

		private void ResetControl()
		{
			lblErrorMsg.Text ="";
			this.divMain.Visible=true;
			BatchDate="";
			DbDataSource=null;
			ViewState["PRBMode"]=ScreenMode.None;
			ViewState["PRBOperation"]=Operation.None;
			lblErrorMsg.Text="";
			Txt_BatchDate.Text=DateTime.Now.ToString("dd/MM/yyyy");
			Txt_BatchDate.ReadOnly = false;
			Txt_BatchDate.TextMaskString="99/99/9999";
			Txt_BatchDate.TextMaskType =  com.common.util.MaskType.msDate;
			BindLocation();
			this.dgLodgments.CurrentPageIndex = 0;
			this.dgLodgments.EditItemIndex=-1;
			this.dgLodgments.SelectedIndex=-1;
			this.dgLodgments.PageSize=this.GridPageSize;

			string userLocation = Session["UserLocation"].ToString();						
			Drp_Location.SelectedValue=userLocation;
			string userRole = Session["UserRole"].ToString();
			if(userRole.ToUpper().IndexOf("OPSSU") != -1)
			{
				//Drp_Location.Enabled=true;
				if(btnExecQry.Enabled)
				{
					Drp_Location.Enabled=true;				
				}
				else
				{
					Drp_Location.Enabled=false;
				}				
			}
			else
			{
				Drp_Location.Enabled=false;	
			}
			
			string sBatchDate = DateDbFormat(Txt_BatchDate.Text.Trim());
			BatchDate = sBatchDate;

			// Thosapol Yennam (Default Data empty)
			DataSet ds = TIESDAL.ConsignmentsWithoutPkgWeightsDAL.QueryConsignmentsWithoutPkgWeights(m_strAppID,m_strEnterpriseID, sBatchDate ,"0");
			DataRow dr = ds.Tables[0].NewRow();
			ds.Tables[0].Rows.Add(dr);
			ds.Tables[0].AcceptChanges();
			DbDataSource = ds;
			BindGrid();
		}

		private void BindGrid()
		{			
			this.dgLodgments.DataSource = DbDataSource;
			this.dgLodgments.DataBind();
		}

		private void btnQry_Click(object sender, System.EventArgs e)
		{
			btnExecQry.Enabled=true;
			Txt_BatchDate.Enabled =true; // Thosapol.y Add Code (28/06/2013)
			ResetControl();
			//BindDataSource();
			BindGrid();
			SetInitialSelect(Txt_BatchDate);

			ClearSession();
		}

		private void btnExecQry_Click(object sender, System.EventArgs e)
		{
			ViewState["CommandState"] = null; 

			btnExecQry.Enabled = false;
			Txt_BatchDate.Enabled = false; // Thosapol.y Add Code (28/06/2013)
			Drp_Location.Enabled = false; // Thosapol.y Add Code (28/06/2013)
			lblErrorMsg.Text="";
//			BindDataSource();
//			BindGrid();

			// Thosapol Yennam 20/08/2013 Comment > Don't used Session
//			Session["btnExecQry_Con"] = "T";
//			Session["BatchDate_Con"] = Txt_BatchDate.Text.Trim();
//			Session["Location_Con"] = this.Drp_Location.SelectedValue;

			// Thosapol Yennam 20/08/2013 Add Code > Used Url parameter
			string paramDate = "&BatchDate_Con=" + Txt_BatchDate.Text.Trim();
			string paramLocation = "&Location_Con=" + this.Drp_Location.SelectedValue;
			string paramButton = "&btnExecQry_Con=T";
//			string url = DefaultUrl(HttpContext.Current.Request.Url.AbsoluteUri) + paramDate + paramLocation + paramButton ;
			System.Uri uri = HttpContext.Current.Request.Url;
			string url = uri.Scheme + "://" + uri.Host + ":" + uri.Port + DefaultUrl(uri.PathAndQuery) + paramDate + paramLocation + paramButton ;
			string sUrl = uri.Scheme + "://" + uri.Host + ":";
			string sParam = DefaultUrl(uri.PathAndQuery) + paramDate + paramLocation + paramButton ;

			// Thosapol Yennam 23/08/2013 Add new code > javascript refresh page
			StringBuilder s = new StringBuilder();
			s.Append("\n<SCRIPT LANGUAGE='JavaScript'>\n"); 
			//s.Append("function reloadPage()\n");
			//s.Append("{\n");
			s.Append(" var sUrl = '" + sUrl + "'; ");
			s.Append(" var sParam = '" + sParam + "'; ");
			s.Append(" var sPort = window.location.port; ");
			s.Append(" if(sPort == ''){sPort = '80';} ");
//			s.Append(" alert( sUrl + sPort + sParam ); ");
			s.Append(" window.location.href = sUrl + sPort + sParam ; ");
			//s.Append("}\n");
			s.Append("</SCRIPT>"); 
			Page.RegisterClientScriptBlock( "reloadpage", s.ToString());
			
//			Response.Write("<script language='javascript'>alert(window.location.protocol + '//' + window.location.hostname + ':' + window.location.port);</script>");
//			Response.Write("<script language='javascript'>window.location.href ='" + url + "';</script>");
		}

		private void BindDataSource()
		{
			string sBatchDate = DateDbFormat(Txt_BatchDate.Text.Trim());
			BatchDate = sBatchDate;
			DataSet ds = TIESDAL.ConsignmentsWithoutPkgWeightsDAL.QueryConsignmentsWithoutPkgWeights(m_strAppID,m_strEnterpriseID, sBatchDate ,this.Drp_Location.SelectedValue);
			DbDataSource = ds;
			if (ds.Tables[0].Rows.Count <= 0 )
			{
				DataRow dr = ds.Tables[0].NewRow();
				ds.Tables[0].Rows.Add(dr);
				ds.Tables[0].AcceptChanges();
			}
		}

		private void ClearSession()
		{
			Session["btnExecQry_Con"] = string.Empty;
			Session["BatchDate_Con"] = string.Empty;
			Session["Location_Con"] = string.Empty;
		}

		private void SetDataPagerefresh()
		{
			// Thosapol Yennam 20/08/2013 Comment > Don't used Session
//			if(Session["BatchDate_Con"] != null)
//			{
//				if(Session["BatchDate_Con"].ToString() != string.Empty)
//				{
//					Txt_BatchDate.Text = Session["BatchDate_Con"].ToString();
//					Txt_BatchDate.Enabled = false;				
//				}
//			}
//
//			if(Session["Location_Con"] != null)
//			{
//				if(Session["Location_Con"].ToString() != string.Empty)
//				{
//					Drp_Location.SelectedValue = Session["Location_Con"].ToString();
//					Drp_Location.Enabled = false;				
//				}
//			}
//
//			if(Session["btnExecQry_Con"] != null)
//			{
//				if(Session["btnExecQry_Con"].ToString() != string.Empty )
//				{ 
//					btnExecQry.Enabled = false; 
//
//					lblErrorMsg.Text="";
//					BindDataSource();
//					BindGrid();
//
//				}				
//			}

			// Thosapol Yennam 20/08/2013 Add Code > Used Url parameter
			if (Request.Params["BatchDate_Con"] != null)
			{
				if (Request.Params["BatchDate_Con"] != string.Empty)
				{
					Txt_BatchDate.Text = Request.Params["BatchDate_Con"];
					Txt_BatchDate.Enabled = false;
				}
			}	
		
			if (Request.Params["Location_Con"] != null)
			{
				if (Request.Params["Location_Con"] != string.Empty)
				{
					Drp_Location.SelectedValue = Request.Params["Location_Con"];
					Drp_Location.Enabled = false;
				}
			}

			
			if (Request.Params["btnExecQry_Con"] != null)
			{
				if (Request.Params["btnExecQry_Con"] != string.Empty)
				{
					btnExecQry.Enabled = false; 

					lblErrorMsg.Text="";
					BindDataSource();
					BindGrid();
				}
			}
			

		}

		private static string DefaultUrl(string sUrl)
		{
			string[] arrurl = sUrl.Split('&');
			string rUrl = string.Empty;
			for(int i=0; i<arrurl.Length; i++)
			{
				string[] ParameterKey = arrurl[i].Split('=');
				if(ParameterKey[0] != "BatchDate_Con" && ParameterKey[0] != "Location_Con" && ParameterKey[0] != "btnExecQry_Con")    
				{
					if(rUrl == string.Empty)
					{
						rUrl = arrurl[i];
					}
					else
					{
						rUrl = rUrl + "&" + arrurl[i];
					}
					
				}
			}

			return rUrl;
		}

	}
}

