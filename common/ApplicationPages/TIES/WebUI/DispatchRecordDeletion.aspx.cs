using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.common.classes;
using com.common.DAL;
using com.common.RBAC;
using com.ties.DAL;
using com.common.util;
using com.ties.classes;
using com.common.applicationpages;
namespace TIES.WebUI
{
	/// <summary>
	/// Summary description for DispatchRecordDeletion.
	/// </summary>
	public class DispatchRecordDeletion : BasePopupPage
	{
		protected System.Web.UI.WebControls.Label lblSenderName;
		protected System.Web.UI.WebControls.Label lblBookNo;
		protected System.Web.UI.WebControls.Label lblBookDateTime;
		protected System.Web.UI.WebControls.Label lblDisBookNo;
		protected System.Web.UI.WebControls.Label lblPickupDateTime;
		protected System.Web.UI.WebControls.Label lblDisSenderName;
		protected System.Web.UI.WebControls.Label lblDisBookDateTime;
		protected System.Web.UI.WebControls.DataGrid m_dgDispatchDelete;
	
		private SessionDS m_sdsDispatchDelete = null;
		private String m_strAppID;
		protected System.Web.UI.WebControls.Button btnClose;
		protected System.Web.UI.WebControls.Label lblDisReqPickupDateTime;
		protected System.Web.UI.WebControls.Label lblDisActPickupDateTime;
		protected System.Web.UI.WebControls.Label lblActPickupDateTime;
		protected System.Web.UI.WebControls.Label lblErrorMsg;
		protected System.Web.UI.WebControls.Label lblOriginDC;
		protected System.Web.UI.WebControls.Label lblDisOriginDC;
		private String m_strEnterpriseID;

		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			Utility utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings,Page.Session);
			m_strAppID = utility.GetAppID();
			m_strEnterpriseID = utility.GetEnterpriseID();
			if(!Page.IsPostBack)
			{
				if((bool)Session["toRefresh"])
				{
					String strBookNo = (String)Session["FORMID"];//lRequest.Params["FORMID"];
					m_dgDispatchDelete.CurrentPageIndex = (int)Session["DELETE_PAGE_NO"];
					int iStartIndex = m_dgDispatchDelete.CurrentPageIndex * m_dgDispatchDelete.PageSize;
					m_sdsDispatchDelete = DispatchTrackingMgrDAL.QueryDispatchForDeletion(m_strAppID, m_strEnterpriseID, strBookNo, iStartIndex,m_dgDispatchDelete.PageSize );
					clearField();
					displayInfo();
					BindDispatch();
					RefreshQueryPage();
				}
				else
				{
					Session["DELETE_PAGE_NO"]=0;					
//					m_sdsDispatchDelete = (SessionDS)Session["SESSION_DS2"];
					String strBookNo = (String)Session["FORMID"];
					m_dgDispatchDelete.CurrentPageIndex = (int)Session["DELETE_PAGE_NO"];
					int iStartIndex = m_dgDispatchDelete.CurrentPageIndex * m_dgDispatchDelete.PageSize;
					m_sdsDispatchDelete = DispatchTrackingMgrDAL.QueryDispatchForDeletion(m_strAppID, m_strEnterpriseID, strBookNo, iStartIndex,m_dgDispatchDelete.PageSize );
					clearField();
					displayInfo();
					BindDispatch();
				}
			}
			else
			{
				clearField();
				m_sdsDispatchDelete = (SessionDS)Session["SESSION_DS2"];
				
			}
		
		}
		/// <summary>
		/// to display record dltion history
		/// </summary>
		private void displayInfo()
		{
			//SessionDS sds = (SessionDS)Session["SESSION_DS1"];
			int rowIndex = (int)Session["INFO_INDEX"];//int.Parse(Request.Params["INFO_INDEX"]);
			try
			{
				DataRow dr = m_sdsDispatchDelete.ds.Tables[0].Rows[0];
				lblDisBookNo.Text = dr["booking_no"].ToString();
				lblDisSenderName.Text = dr["sender_name"].ToString();
				lblDisOriginDC.Text = dr["location"].ToString();  //by sittichai  01/02/2008

				if(dr["booking_datetime"]!=System.DBNull.Value && dr["booking_datetime"].ToString()!="")
				{
					DateTime dtBookDateTime = (DateTime)dr["booking_datetime"];
					lblDisBookDateTime.Text = dtBookDateTime.ToString("dd/MM/yyyy HH:mm");
				}
				if(dr["act_pickup_datetime"]!=System.DBNull.Value && dr["act_pickup_datetime"].ToString()!="")
				{
					DateTime dtActPickupDateTime = (DateTime)dr["act_pickup_datetime"];
					lblDisActPickupDateTime.Text = dtActPickupDateTime.ToString("dd/MM/yyyy HH:mm");
				}
				if(dr["req_pickup_datetime"]!=System.DBNull.Value && dr["req_pickup_datetime"].ToString()!="")
				{
					DateTime dtReqPickupDateTime = (DateTime)dr["req_pickup_datetime"];
					lblDisReqPickupDateTime.Text = dtReqPickupDateTime.ToString("dd/MM/yyyy HH:mm");
				}


			}
			catch(Exception ex)
			{
				Logger.LogTraceVerbose("DispatchRecordDeletion","displayInfo","Error1",ex.Message);
				//lblErrorMsg.Text=Utility.GetLanguageText(ResourceType.UserMessage,"NO_RECORD_FOUND",utility.GetUserCulture());
				lblErrorMsg.Text="No matches found";
				lblActPickupDateTime.Visible	=	false;
				lblBookDateTime.Visible			=	false;
				lblBookNo.Visible				=	false;
				lblDisActPickupDateTime.Visible	=	false;
				lblDisBookDateTime.Visible		=	false;
				lblDisBookNo.Visible			=	false;
				lblDisReqPickupDateTime.Visible	=	false;
				lblDisSenderName.Visible		=	false;
				m_dgDispatchDelete.Visible		=	false;
				lblActPickupDateTime.Visible	=	false;
				lblSenderName.Visible			=	false;
				lblPickupDateTime.Visible		=	false;
				lblDisOriginDC.Visible			=	false;		//by sittichai  01/02/2008
				lblOriginDC.Visible				=	false;
			}
		}
		/// <summary>
		/// to bind data to datagrid
		/// </summary>
		private void clearField()
		{
			lblActPickupDateTime.Visible	=	true;
			lblBookDateTime.Visible			=	true;
			lblBookNo.Visible				=	true;
			lblDisActPickupDateTime.Visible	=	true;
			lblDisBookDateTime.Visible		=	true;
			lblDisBookNo.Visible			=	true;
			lblDisReqPickupDateTime.Visible	=	true;
			lblDisSenderName.Visible		=	true;
			m_dgDispatchDelete.Visible		=	true;
			lblActPickupDateTime.Visible	=	true;
			lblSenderName.Visible			=	true;
			lblPickupDateTime.Visible		=	true;
			lblOriginDC.Visible				=	true;		//by sittichai  01/02/2008
			lblDisOriginDC.Visible			=	true;
			lblErrorMsg.Text="";
		}
		private void BindDispatch()
		{
			m_dgDispatchDelete.VirtualItemCount = System.Convert.ToInt32(m_sdsDispatchDelete.QueryResultMaxSize);
			m_dgDispatchDelete.DataSource = m_sdsDispatchDelete.ds;
			m_dgDispatchDelete.DataBind();
			Session["SESSION_DS2"] = m_sdsDispatchDelete;

		}
		/// <summary>
		/// during databound, check if check box is check then
		/// Delete button to be disabled
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void OnDataBound_DispatchDeletion(object sender, DataGridItemEventArgs e)
		{
			if(e.Item.ItemIndex == -1)
			{
				return;
			}
			DataRow dr = m_sdsDispatchDelete.ds.Tables[0].Rows[e.Item.ItemIndex];
			String strDeleted = dr["deleted"].ToString();
			CheckBox chkboxDeleted = (CheckBox)e.Item.FindControl("chkboxDeleted");
			if(strDeleted=="y" || strDeleted=="Y" && chkboxDeleted!=null)
			{
				chkboxDeleted.Checked = true;
				e.Item.Cells[0].Enabled = false;
			}
			else
			{
				chkboxDeleted.Checked = false;
			}
			Label lblStatus = (Label)e.Item.FindControl("lblStatus");
			if(lblStatus.Text==TIESUtility.getInitialDispatchStatus(m_strAppID,m_strEnterpriseID))	
			{
				e.Item.Cells[0].Enabled = false;
			}
		}
		/// <summary>
		/// to update a record when it's bing deleted
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void OnDelete_DispatchHistory(object sender, DataGridCommandEventArgs e)
		{
			
			if(e.Item.Cells[0].Enabled ==false)
			{
				lblErrorMsg.Text=Utility.GetLanguageText(ResourceType.UserMessage,"NO_DEL_POSSIBLE",utility.GetUserCulture());
				return;
			}
			//m_sdsDispatchDelete = (SessionDS)Session["SESSION_DS2"];
			String strBookDateTime = (String)Session["BOOK_DATE"];//Request.Params["BOOK_DATE"];
			Label lblStatusDateTime = (Label)e.Item.FindControl("lblStatusDateTime");
			String strStatusDateTime = lblStatusDateTime.Text;
			int num = e.Item.ItemIndex - 1;
			
			OpenWindowpage("DeleteHistoryConfirmPopup.aspx?BOOKDATE="+strBookDateTime+"&STATUSDATE="+strStatusDateTime+"&ROWNUM="+num+"&FORM_ID=DispatchRecordDeletion.aspx ");
		}
		/// <summary>
		/// to refresh data during page change
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void OnDispatchDelete_PageChange(object sender, DataGridPageChangedEventArgs e)
		{
			m_dgDispatchDelete.CurrentPageIndex = e.NewPageIndex;
			String strBookNo = (String)Session["FORMID"];//Request.Params["FORMID"];
			int iStartIndex = m_dgDispatchDelete.CurrentPageIndex * m_dgDispatchDelete.PageSize;
			Session["DELETE_PAGE_NO"] = m_dgDispatchDelete.CurrentPageIndex;
			m_sdsDispatchDelete = DispatchTrackingMgrDAL.QueryDispatchForDeletion(m_strAppID, m_strEnterpriseID, strBookNo, iStartIndex,m_dgDispatchDelete.PageSize );
			BindDispatch();
		}
		/// <summary>
		/// to spcify what window to open
		/// </summary>
		/// <param name="strUrl">url to specify path</param>
		private void OpenWindowpage(String strUrl)
		{
//			String strOpenWin = "<script language=javascript>";
//			String url = strUrl;
//			String features = "'height=300,width=300,left=40,top=330,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,titlebar=yes,toolbar=no'";
//		//	strOpenWin += "window.open('" + url+"', '',"+features +");";
//			strOpenWin += "</script>";
//			//Response.Write(strOpenWin);
//			Response.Write("<script language=javascript>alert(window.opener.opener.top.displayBanner.win_op_child_child)</script>");
			ArrayList paramList = new ArrayList();
			paramList.Add(strUrl);
			String sScript = Utility.GetScript("openLastChildWindow.js",paramList);
			Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
		}
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			base.OnInit(e);
			InitializeComponent();
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		/// <summary>
		/// to close current window
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void btnClose_Click(object sender, System.EventArgs e)
		{
			String sScript = "";
			sScript += "<script language=javascript>";
			sScript += "  window.opener;" ;
			sScript += "  window.close();";
			sScript += "</script>";
			Response.Write(sScript);
		}
		private void RefreshQueryPage()
		{
			String sScript = "";
			sScript += "<script language=javascript>";
			sScript += "var sURL = unescape(window.opener.location.pathname); ";
			sScript += "  window.opener.location.replace(sURL);" ;
			//sScript += "  window.close();";
			sScript += "</script>";
			Response.Write(sScript);
		}
	}
}
