var serviceURL_Load = "http://172.25.46.8/MyService/Service.asmx/HCRScanSave";
var serviceURL_LoadCons = "http://172.25.46.8/MyService/Service.asmx/HCRScanLoad";
var serviceURL_LoadError = "http://172.25.46.8/MyService/Service.asmx/HCRScanError";

//var serviceURL_Load = "http://localhost/MyService/Service.asmx/HCRScanSave";
//var serviceURL_LoadCons = "http://localhost/MyService/Service.asmx/HCRScanLoad";
//var serviceURL_LoadError = "http://localhost/MyService/Service.asmx/HCRScanError";


function reDraw() 
{
	var row, del, index;
	var rows = document.getElementsByTagName("tr");
	for (var i = rows.length - 1; i >= 0; i--) {
		if (rows[i].className == "rida") {
			rows[i].parentNode.removeChild(rows[i]);
        }
    }
    index = $.jStorage.index();
        
    for (var i = 0; i < index.length; i++)
    {
		if (index[i].substring(0,8) == "valCons_")
		{								
			row = document.createElement("tr");
			row.className = "rida";
			var t = document.createElement("td");	        
			t.innerHTML = $.jStorage.get(index[i]);
			row.appendChild(t);
			del = document.createElement("a");
			del.setAttribute('href','javascript:void(0)'); 
			del.setAttribute('class','tableLabel'); 

			del.innerHTML = "";
			//delete
			(function (i) {
				del.onclick = function () {
					$.jStorage.deleteKey(i);
					reDraw();
				};
			})(index[i])        
			t = document.createElement("td");
			t.appendChild(del)
			row.appendChild(t);
			document.getElementById("disp_cons_local").appendChild(row);				
		}
    }
}


function InsertValue() 
{                
	var row;					
	val = document.getElementById('val').value;
					
	if (val != '')
	{
		row = document.createElement("tr");									
		document.getElementById("lberrmsg").innerHTML = "";
		$.jStorage.set("valCons_" + val, val);
		document.getElementById('val').value = "";			
		reDraw();
	}

}
			
			
function viewdataMissing() 
{
	var fromdate = document.getElementById('txtScanDate').value;
	var d = fromdate.substring(0,2);
	var m = fromdate.substring(3,5);
	var y = fromdate.substring(6,10);
				
	fromdate = y + "-" + m + "-" + d;
	var todate = document.getElementById('txtScanTo').value;
	d = todate.substring(0,2);
	m = todate.substring(3,5);
	y = todate.substring(6,10);
	todate = y + "-" + m + "-" + d;
				 					           
    $.ajax({
		url: serviceURL_LoadError,
        type: "POST",
        dataType: "json",
        data: "{fromdate:'" + fromdate + "',todate:'" + todate + "'}",
        contentType: "application/json; charset=utf-8",
        success: function (msg) {					
			$(".viewdataMissing").replaceWith(msg["d"]["ResponseData"]);
        },
        //error: function (e) {}
        error: function(msg){
			//alert("Data Missing Process error.");
		}
	});
}


function viewdataUpdate() 
{
	var fromdate, todate, dd, mm, yyyy;
	var currentTime = new Date();
				
	mm = '0' + (currentTime.getMonth() + 1);
    mm = mm.substring(mm.length - 2);
                
	dd = '0' + currentTime.getDate();
	dd = dd.substring(dd.length - 2);
				
	yyyy = currentTime.getFullYear();
				
	fromdate = yyyy + "-" + mm + "-" + dd;
	todate = yyyy + "-" + mm + "-" + dd;						
								 					           
    $.ajax({
        url: serviceURL_LoadCons,
        type: "POST",
        dataType: "json",
        data: "{fromdate:'"+fromdate+"',todate:'"+todate+"'}",
        contentType: "application/json; charset=utf-8",
        success: function (msg) {					
            $(".viewdataUpdate").replaceWith(msg["d"]["ResponseData"]);
        },
        //error: function (e) {}
        error: function(msg){
			//alert("Scan Process error.");
		}
    });                
}

       
function initialonLoad()
{                
    //Set current date
    var vNow = null;
    var strDay = "";
    var strMonth = "";
    var strYear = "";
    vNow = new Date();
    strDay = '0' + vNow.getDate();
    strDay = strDay.substring(strDay.length - 2);
    
    strMonth = '0' + (vNow.getMonth() + 1);
    strMonth = strMonth.substring(strMonth.length - 2);
    
    strYear  = vNow.getFullYear();
    document.getElementById('txtScanDate').value = strDay + '/' + strMonth + '/' + strYear;
    document.getElementById('txtScanTo').value = strDay + '/' + strMonth + '/' + strYear;                          
}
            
            
function clearView()
{
	if (browserType == "gecko" )
		document.poppedLayer = eval('document.getElementById("divBarcode")');
	else if (browserType == "ie")
		document.poppedLayer = eval('document.getElementById("divBarcode")');
	else
		document.poppedLayer = eval('document.layers["divBarcode"]');
	document.poppedLayer.style.visibility = "hidden";

	if (browserType == "gecko" )
		document.poppedLayer = eval('document.getElementById("divDataMissing")');
	else if (browserType == "ie")
		document.poppedLayer = eval('document.getElementById("divDataMissing")');
	else
		document.poppedLayer = eval('document.layers["divDataMissing"]');
	document.poppedLayer.style.visibility = "hidden";
		
	//Enable Button
	disableBtn(false);
		
	//Set Flag for Data Missing process	
	actionProcess = 'CLEAR';
	
	//Clear Status Update List Box
	$("#lbTo option").appendTo("#lbFrom");	
	
	//Sort Order in List Box
	sortListBox();	
}


function showScanData() 
{
	if (browserType == "gecko" )
		document.poppedLayer = eval('document.getElementById("divBarcode")');
	else if (browserType == "ie")
		document.poppedLayer = eval('document.getElementById("divBarcode")');
	else
		document.poppedLayer = eval('document.layers["divBarcode"]');
	document.poppedLayer.style.visibility = "visible";

	if (browserType == "gecko" )
		document.poppedLayer = eval('document.getElementById("divDataMissing")');
	else if (browserType == "ie")
		document.poppedLayer = eval('document.getElementById("divDataMissing")');
	else
		document.poppedLayer = eval('document.layers["divDataMissing"]');
	document.poppedLayer.style.visibility = "hidden";

	//Get Status Value from ListBox  
	getUpdStatus();	

	//Check Status Code for update
	var statusCode = document.getElementById('txtUpdStatus').value;
	if (statusCode == "")
	{
		document.forms[0].val.disabled = true;				
		alert("Choose status code to update");
	}
	else
	{
		document.forms[0].val.disabled = false;
		
		//Set focus to barcode textbox
		OnFocus();
	}

	//Disable Button
	disableBtn(true);			
				
	//Set Flag for SCAN process
	actionProcess = "SCAN";
}


function showDataMissing() 
{
	if (browserType == "gecko" )
		document.poppedLayer = eval('document.getElementById("divBarcode")');
	else if (browserType == "ie")
		document.poppedLayer = eval('document.getElementById("divBarcode")');
	else
		document.poppedLayer = eval('document.layers["divBarcode"]');
	document.poppedLayer.style.visibility = "hidden";

	if (browserType == "gecko" )
		document.poppedLayer = eval('document.getElementById("divDataMissing")');
	else if (browserType == "ie")
		document.poppedLayer = eval('document.getElementById("divDataMissing")');
	else
		document.poppedLayer = eval('document.layers["divDataMissing"]');
	document.poppedLayer.style.visibility = "visible";
	
	//Disable Button				
	disableBtn(true);
		
	//Set Flag for Data Missing process
	actionProcess = "DATAMISSING";			
}


function getUpdStatus()
{	
	var ref = document.getElementById('lbTo');
	for(i=0; i<ref.options.length; i++){
		ref.options[i].selected = true;
	}

	var updStatus = document.getElementById('txtUpdStatus');
	if (updStatus)
	{
		updStatus.value = "";		

		//get value from listbox (lbTo)
		$('#lbTo :selected').each(function(i, selected) {
			if (updStatus.value == "")
				updStatus.value += $(selected).val();
			else
				updStatus.value += ";" + $(selected).val();
		});		
	}
}

	
function disableBtn(boolVal)
{
    document.forms[0].btnAdd_.disabled = boolVal;
    document.forms[0].btnAddAll_.disabled = boolVal; 
    document.forms[0].btnRemove_.disabled = boolVal;
    document.forms[0].btnRemoveAll_.disabled = boolVal;
}


function sortListBox() 
{
	var lb = document.getElementById('lbFrom');
	arrTexts = new Array();

	for(i=0; i<lb.length; i++) {
		arrTexts[i] = lb.options[i].text;
	}

	arrTexts.sort();

	for(i=0; i<lb.length; i++) {
		lb.options[i].text = arrTexts[i];
		lb.options[i].value = arrTexts[i];
	}
	
	//clear select data in list box
	for(i=0; i<document.getElementById('lbFrom').options.length; i++) {
		document.getElementById('lbFrom').options[i].selected = false;
	}
}