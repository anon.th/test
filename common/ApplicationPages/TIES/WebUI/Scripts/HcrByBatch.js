
//var serviceURL_SaveHCR = "http://172.25.46.8/MyService/Service.asmx/HCRScanSave";
//var serviceURL_LoadCons = "http://172.25.46.8/MyService/Service.asmx/HCRScanLoad";
//var serviceURL_LoadError = "http://172.25.46.8/MyService/Service.asmx/HCRScanError";

var serviceURL_SaveHCR = "http://localhost/dmsWebService/Service.asmx/HCRScanSave";
var serviceURL_LoadCons = "http://localhost/dmsWebService/Service.asmx/HCRScanLoad";
var serviceURL_LoadError = "http://localhost/dmsWebService/Service.asmx/HCRScanError";

var serviceURL_ConsNoLoad = "http://localhost/dmsWebService/Service.asmx/ConsNoLoad";
var serviceURL_HCRByBatchSelectedSave = "http://localhost/dmsWebService/Service.asmx/HCRByBatchSelectedSave";
var serviceURL_ConsNoSelectedLoad = "http://localhost/dmsWebService/Service.asmx/ConsNoSelectedLoad";


///// -------------------------------------------------------------------------------------- /////

/////====================================== CancelCons ======================================/////
function CancelCons()
{
	if (browserType == "gecko" )
		document.poppedLayer = eval('document.getElementById("divSelectCons")');
	else if (browserType == "ie")
		document.poppedLayer = eval('document.getElementById("divSelectCons")');
	else
		document.poppedLayer = eval('document.layers["divSelectCons"]');
	document.poppedLayer.style.visibility = "hidden";						
}


/////====================================== ClickCheckAll ======================================/////		
function ClickCheckAll(vol)
{
	var i=1;
	for(i=1; i<=document.getElementById("hdnChkCount").value; i++)
	{
		if(vol.checked == true)
		{
			document.getElementById("chk_"+i).checked = true;
		}
		else
		{
			document.getElementById("chk_"+i).checked = false;
		}
	}
}


/////====================================== SelectAll ======================================/////
function SelectAll(CheckBoxControl)
{
	if (CheckBoxControl.checked == true)
	{
		var i;
		for (i=0; i < document.forms[0].elements.length; i++)
		{
			if ((document.forms[0].elements[i].type == 'checkbox') && (document.forms[0].elements[i].name.indexOf('dgCons') > -1))
			{
				document.forms[0].elements[i].checked = true;
			}
		}
	}
	else
	{
		var i;
		for (i=0; i < document.forms[0].elements.length; i++)
		{
			if ((document.forms[0].elements[i].type == 'checkbox') && (document.forms[0].elements[i].name.indexOf('dgCons') > -1))
			{
				document.forms[0].elements[i].checked = false;
			}
		}
	}
		}
		
/////====================================== SetMode ======================================/////
function SetMode(strmode)
{			
	document.getElementById("txtMode").value = strmode;
}


function reDraw() 
{
	var row, del, index;
	var rows = document.getElementsByTagName("tr");
	for (var i = rows.length - 1; i >= 0; i--) {
		if (rows[i].className == "rida") {
			rows[i].parentNode.removeChild(rows[i]);
        }
    }
    index = $.jStorage.index();
        
    for (var i = 0; i < index.length; i++)
    {
		if (index[i].substring(0,8) == "valCons_")
		{								
			row = document.createElement("tr");
			row.className = "rida";
			var t = document.createElement("td");	        
			t.innerHTML = $.jStorage.get(index[i]);
			row.appendChild(t);
			del = document.createElement("a");
			del.setAttribute('href','javascript:void(0)'); 
			del.setAttribute('class','tableLabel'); 

			del.innerHTML = "";
			//delete
			(function (i) {
				del.onclick = function () {
					$.jStorage.deleteKey(i);
					reDraw();
				};
			})(index[i])        
			t = document.createElement("td");
			t.appendChild(del)
			row.appendChild(t);
			document.getElementById("disp_cons_local").appendChild(row);				
		}
    }
}
			
			
function viewdataMissing() 
{
	var fromdate = '01/01/2012';	//document.getElementById('txtScanDate').value;
	var d = fromdate.substring(0,2);
	var m = fromdate.substring(3,5);
	var y = fromdate.substring(6,10);
				
	fromdate = y + "-" + m + "-" + d;
	
	var todate =  '31/12/2012';		//document.getElementById('txtScanTo').value;
	d = todate.substring(0,2);
	m = todate.substring(3,5);
	y = todate.substring(6,10);
	todate = y + "-" + m + "-" + d;
				 					           
    $.ajax({
		url: serviceURL_LoadError,
        type: "POST",
        dataType: "json",
        data: "{fromdate:'" + fromdate + "',todate:'" + todate + "'}",
        contentType: "application/json; charset=utf-8",
        success: function (msg) {					
			$(".viewdataMissing").replaceWith(msg["d"]["ResponseData"]);
        },
        //error: function (e) {},
        error: function(msg){
			//alert("Data Missing Process error.");
		}
	});
}


function viewdataUpdate() 
{
	var fromdate, todate, dd, mm, yyyy;
	var currentTime = new Date();
				
	mm = '0' + (currentTime.getMonth() + 1);
    mm = mm.substring(mm.length - 2);
                
	dd = '0' + currentTime.getDate();
	dd = dd.substring(dd.length - 2);
				
	yyyy = currentTime.getFullYear();
				
	fromdate = yyyy + "-" + mm + "-" + dd;
	todate = yyyy + "-" + mm + "-" + dd;						
								 					           
    $.ajax({
        url: serviceURL_LoadCons,
        type: "POST",
        dataType: "json",
        data: "{fromdate:'"+fromdate+"',todate:'"+todate+"'}",
        contentType: "application/json; charset=utf-8",
        success: function (msg) {					
            $(".viewdataUpdate").replaceWith(msg["d"]["ResponseData"]);
        },
        //error: function (e) {},
        error: function(msg){
			//alert("Scan Process error.");
		}
    });                
}

       
function clearView()
{		
	if (browserType == "gecko" )
		document.poppedLayer = eval('document.getElementById("divBarcode")');
	else if (browserType == "ie")
		document.poppedLayer = eval('document.getElementById("divBarcode")');
	else
		document.poppedLayer = eval('document.layers["divBarcode"]');
	document.poppedLayer.style.visibility = "hidden";
		
	//Enable Button
	disableBtn(false);
			
	//Clear Status Update List Box
	$("#lbTo option").appendTo("#lbFrom");	
	
	//Sort Order in List Box
	sortListBox();	
}


function showScanData() 
{
	if (browserType == "gecko" )
		document.poppedLayer = eval('document.getElementById("divBarcode")');
	else if (browserType == "ie")
		document.poppedLayer = eval('document.getElementById("divBarcode")');
	else
		document.poppedLayer = eval('document.layers["divBarcode"]');
	document.poppedLayer.style.visibility = "visible";

	//Get Status Value from ListBox  
	getUpdStatus();	

	//Check Status Code for update
	var statusCode = document.getElementById('txtUpdStatus').value;
	if (statusCode == "")
	{
		//document.forms[0].val.disabled = true;				
		alert("Choose status code to update");
		
		document.getElementById("txtMode").value = '';
	}
	else
	{
		//do nothing
	}

	//Disable Button
	disableBtn(true);			
}


function getUpdStatus()
{	
	var ref = document.getElementById('lbTo');
	for(i=0; i<ref.options.length; i++){
		ref.options[i].selected = true;
	}

	var updStatus = document.getElementById('txtUpdStatus');
	if (updStatus)
	{
		updStatus.value = "";		

		//get value from listbox (lbTo)
		$('#lbTo :selected').each(function(i, selected) {
			if (updStatus.value == "")
				updStatus.value += $(selected).val();
			else
				updStatus.value += ";" + $(selected).val();
		});		
	}
}

	
function disableBtn(boolVal)
{
    document.forms[0].btnAdd_.disabled = boolVal;
    document.forms[0].btnAddAll_.disabled = boolVal; 
    document.forms[0].btnRemove_.disabled = boolVal;
    document.forms[0].btnRemoveAll_.disabled = boolVal;
}


function sortListBox() 
{
	var lb = document.getElementById('lbFrom');
	arrTexts = new Array();

	for(i=0; i<lb.length; i++) {
		arrTexts[i] = lb.options[i].text;
	}

	arrTexts.sort();

	for(i=0; i<lb.length; i++) {
		lb.options[i].text = arrTexts[i];
		lb.options[i].value = arrTexts[i];
	}
	
	//clear select data in list box
	for(i=0; i<document.getElementById('lbFrom').options.length; i++) {
		document.getElementById('lbFrom').options[i].selected = false;
	}
}