var dots = 0;
var dotmax = 10;
var obj = null;
var Interval1 = 0;
var Interval2 = 0;
		
function ShowWait()
{
	var output; output = 'Please wait while your data are getting processed';
	dots++;
	
	if(dots>=dotmax)
		dots=1;
		
	for(var x = 0;x < dots;x++)
	{
		output += '.';
	} 
	CSSDiv.innerText =  output;
}

function disButton()
{
	if (obj.disabled == false) {
		obj.disabled = true; 
	}
}

function StartShowWait(val)
{
	CSSDiv.style.display = 'block'; 
	
	obj = val;
	
	Interval1 = window.setInterval('disButton()', 10);
	Interval2 = window.setInterval('ShowWait()', 1000);
}

function HideWait()
{
	CSSDiv.style.display = 'none';

	if(Interval1 != 0) {
		window.clearInterval(Interval1);
		//alert(Interval1);
	}
	
	if(Interval2 != 0) {
		window.clearInterval(Interval2);
		//alert(Interval2);
	}

	document.getElementById('btnSave').disabled = false; 
}