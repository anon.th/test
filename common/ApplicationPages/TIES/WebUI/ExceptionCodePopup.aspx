<%@ Page language="c#" Codebehind="ExceptionCodePopup.aspx.cs" AutoEventWireup="false" Inherits="TIES.WebUI.ExceptionCodePopup" SmartNavigation="False" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>ExceptionCodePopup</title>
		<LINK href="css/Styles.css" type="text/css" rel="stylesheet" name="stylesheet">
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="ExceptionCodePopup" method="post" runat="server">
			<TABLE id="Table1" style="Z-INDEX: 114; LEFT: 8px; WIDTH: 501px; POSITION: absolute; HEIGHT: 130px" cellSpacing="1" cellPadding="1" width="501" border="0">
				<tr>
					<td colspan="3"><asp:label id="Label5" style="Z-INDEX: 111; LEFT: 15px" runat="server" CssClass="mainTitleSize" Height="36px" Width="382px">Exception Code</asp:label>
					</td>
				</tr>
				<TR>
					<TD><asp:label id="Label1" runat="server" CssClass="tableLabel">Status Code</asp:label></TD>
					<TD><asp:textbox id="txtStatusSrch" runat="server" Width="151px" CssClass="textfield"></asp:textbox></TD>
					<TD>
						<asp:button id="btnSearch" runat="server" Text="Search" CssClass="queryButton" Width="68"></asp:button></TD>
				</TR>
				<TR>
					<TD>
						<asp:Label id="Label6" runat="server" CssClass="tableLabel">Exception Code</asp:Label></TD>
					<TD>
						<asp:TextBox id="txtExceptionSrch" runat="server" Width="151px" CssClass="textfield"></asp:TextBox></TD>
					<TD>
						<asp:button id="btnClose" runat="server" Width="68px" CssClass="queryButton" Text="Close"></asp:button></TD>
				</TR>
				<TR>
					<TD><asp:label id="Label7" runat="server" CssClass="tableLabel">MBG</asp:label></TD>
					<TD>
						<asp:DropDownList id="ddlMbgSrch" runat="server" Width="151px" CssClass="textField"></asp:DropDownList></TD>
					<TD></TD>
				</TR>
				<TR>
					<TD><asp:label id="Label3" runat="server" CssClass="tableLabel">Status Close</asp:label></TD>
					<TD>
						<asp:DropDownList id="ddlCloseSrch" runat="server" Width="151px" CssClass="textField"></asp:DropDownList></TD>
					<TD></TD>
				</TR>
				<TR>
					<TD><asp:label id="Label4" runat="server" CssClass="tableLabel">Invoiceable</asp:label></TD>
					<TD>
						<asp:DropDownList id="ddlInvoiceSrch" runat="server" Width="151px" CssClass="textField"></asp:DropDownList></TD>
					<TD></TD>
				</TR>
				<tr>
					<td colspan="3">&nbsp;</td>
				</tr>
				<tr>
					<td colspan="3">
						<asp:datagrid id="m_dgExceptionCode" style="Z-INDEX: 102; LEFT: 9px; POSITION: absolute" runat="server" CssClass="gridHeading" BorderStyle="None" BorderWidth="1px" CellPadding="3" BackColor="White" BorderColor="#CCCCCC" AllowPaging="True" AllowCustomPaging="True" OnPageIndexChanged="OnPaging_Dispatch" AutoGenerateColumns="False" Width="744px">
							<SelectedItemStyle Font-Bold="True" ForeColor="White" BackColor="#669999"></SelectedItemStyle>
							<ItemStyle CssClass="popupGridField"></ItemStyle>
							<HeaderStyle CssClass="gridHeading"></HeaderStyle>
							<FooterStyle ForeColor="#000066" BackColor="White"></FooterStyle>
							<Columns>
								<asp:BoundColumn DataField="status_code" HeaderText="Status Code">
									<ItemStyle HorizontalAlign="Left"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="status_description" HeaderText="Status Description">
									<ItemStyle HorizontalAlign="Left"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="exception_code" HeaderText="Exception Code">
									<ItemStyle HorizontalAlign="Left"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="exception_description" HeaderText="Exception Description">
									<ItemStyle HorizontalAlign="Left"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="mbg" HeaderText="MBG">
									<ItemStyle HorizontalAlign="Left"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="status_close" HeaderText="Status Close">
									<ItemStyle HorizontalAlign="Left"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="invoiceable" HeaderText="Invoiceable">
									<ItemStyle HorizontalAlign="Left"></ItemStyle>
								</asp:BoundColumn>
								<asp:ButtonColumn Text="Select" CommandName="Select">
									<ItemStyle HorizontalAlign="Left"></ItemStyle>
								</asp:ButtonColumn>
							</Columns>
							<PagerStyle HorizontalAlign="Left" ForeColor="#000066" BackColor="White" Mode="NumericPages"></PagerStyle>
						</asp:datagrid>
					</td>
				</tr>
			</TABLE>
		</form>
	</body>
</HTML>
