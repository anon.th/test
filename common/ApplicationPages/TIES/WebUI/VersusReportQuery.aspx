<%@ Import Namespace="com.common.util" %>
<%@ Import Namespace="Cambro.Web.DbCombo" %>
<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<%@ Register TagPrefix="DbCombo" Namespace="Cambro.Web.DbCombo" Assembly="Cambro.Web.DbCombo" %>
<%@ Page language="c#" Codebehind="VersusReportQuery.aspx.cs" AutoEventWireup="false" Inherits="com.ties.VersusReportQuery" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Versus Report Query</title>
		<meta content="True" name="vs_showGrid">
		<LINK href="css/Styles.css" type="text/css" rel="stylesheet" name="stylesheet">
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<!--#INCLUDE FILE="msFormValidations.inc"-->
		<script language="javascript" src="Scripts/settingScrollPosition.js"></script>
	</HEAD>
	<body onclick="GetScrollPosition();" onload="SetScrollPosition();" onunload="window.top.displayBanner.fnCloseAll(0);"
		MS_POSITIONING="GridLayout">
		<form id="CODAmountCollectedReport" method="post" runat="server">
			<asp:button id="btnQuery" style="Z-INDEX: 101; LEFT: 20px; POSITION: absolute; TOP: 40px" runat="server"
				Text="Query" CssClass="queryButton" Width="64px"></asp:button><asp:label id="lblErrorMessage" style="Z-INDEX: 105; LEFT: 24px; POSITION: absolute; TOP: 69px"
				runat="server" CssClass="errorMsgColor" Width="700px" Height="17px">Place holder for err msg</asp:label>&nbsp;
			<asp:button id="btnExecuteQuery" style="Z-INDEX: 102; LEFT: 90px; POSITION: absolute; TOP: 40px"
				runat="server" Text="Execute Query" CssClass="queryButton" Width="123px"></asp:button>
			<TABLE id="tblShipmentTracking" style="Z-INDEX: 104; LEFT: 22px; WIDTH: 568px; POSITION: absolute; TOP: 90px; HEIGHT: 168px"
				width="568" border="0" runat="server">
				<TR>
					<TD style="WIDTH: 589px; HEIGHT: 65px" vAlign="top">
						<fieldset style="WIDTH: 560px; HEIGHT: 114px"><legend><asp:label id="lblDates" runat="server" CssClass="tableHeadingFieldset" Width="20px" Font-Bold="True">Dates</asp:label></legend>
							<TABLE id="tblDates" style="WIDTH: 465px; HEIGHT: 40px" cellSpacing="0" cellPadding="0"
								align="left" border="0" runat="server">
								<TR>
									<td height="25"><FONT face="Tahoma"></FONT></td>
									<TD colSpan="3" height="25" rowSpan="1">&nbsp;<asp:radiobutton id="rbActualPickupDate" runat="server" Text="Last Status Date" CssClass="tableRadioButton"
											Width="168px" Height="23px" Checked="True" AutoPostBack="True" GroupName="QueryByDate"></asp:radiobutton></TD>
								</TR>
								<TR>
									<td colSpan="1" height="25" rowSpan="1"><FONT face="Tahoma"></FONT></td>
									<TD colSpan="3" height="25">&nbsp;
										<asp:radiobutton id="rbMonth" runat="server" Text="Month" CssClass="tableRadioButton" Width="73px"
											Height="21px" Checked="True" AutoPostBack="True" GroupName="EnterDate"></asp:radiobutton>&nbsp;
										<asp:dropdownlist id="ddMonth" runat="server" CssClass="textField" Width="88px" Height="19px"></asp:dropdownlist>&nbsp;
										<asp:label id="lblYear" runat="server" CssClass="tableLabel" Width="30px" Height="15px">Year</asp:label>&nbsp;
										<cc1:mstextbox id="txtYear" runat="server" CssClass="textField" Width="83" MaxLength="5" TextMaskType="msNumeric"
											NumberMaxValue="2999" NumberMinValue="1" NumberPrecision="4"></cc1:mstextbox></TD>
								</TR>
								<TR>
									<td colSpan="1" height="25" rowSpan="1"><FONT face="Tahoma"></FONT></td>
									<TD colSpan="3" height="25">&nbsp;
										<asp:radiobutton id="rbPeriod" runat="server" Text="Period" CssClass="tableRadioButton" Width="62px"
											AutoPostBack="True" GroupName="EnterDate"></asp:radiobutton>&nbsp;&nbsp;&nbsp;
										<cc1:mstextbox id="txtPeriod" runat="server" CssClass="textField" Width="82" MaxLength="10" TextMaskType="msDate"
											TextMaskString="99/99/9999"></cc1:mstextbox>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
										&nbsp;
										<cc1:mstextbox id="txtTo" runat="server" CssClass="textField" Width="83" MaxLength="10" TextMaskType="msDate"
											TextMaskString="99/99/9999"></cc1:mstextbox></TD>
								</TR>
								<TR>
									<td colSpan="1" height="25" rowSpan="1"><FONT face="Tahoma"></FONT></td>
									<TD colSpan="3" height="25"><FONT face="Tahoma">&nbsp;</FONT>
										<asp:radiobutton id="rbDate" runat="server" Text="Date" CssClass="tableRadioButton" Width="74px"
											Height="21px" AutoPostBack="True" GroupName="EnterDate"></asp:radiobutton>&nbsp;&nbsp;<cc1:mstextbox id="txtDate" runat="server" CssClass="textField" Width="82" MaxLength="10" TextMaskType="msDate"
											TextMaskString="99/99/9999"></cc1:mstextbox></TD>
								</TR>
								<tr>
									<td colSpan="1" height="25" rowSpan="1"><FONT face="Tahoma"></FONT></td>
									<td>
										<TABLE id="tblStatusCode" style="WIDTH: 552px; HEIGHT: 32px" cellSpacing="0" cellPadding="0"
											align="left" border="0" runat="server">
											<TR>
												<TD class="tableLabel" style="WIDTH: 68px" colSpan="1" height="25" rowSpan="1"><FONT face="Tahoma">&nbsp;&nbsp;&nbsp;
													</FONT>
													<asp:label id="lblStatusCode1" runat="server" CssClass="tableLabel" Width="104px" Height="22px">First Status Code</asp:label></TD>
												<TD style="WIDTH: 100px" height="25"><cc1:mstextbox id="txtFSC" runat="server" CssClass="textField" Width="60px" MaxLength="10" TextMaskString="99/99/9999"></cc1:mstextbox></TD>
												<TD style="WIDTH: 13px" height="25"><FONT face="Tahoma"></FONT></TD>
												<TD class="tableLabel" height="25"><FONT face="Tahoma">&nbsp; </FONT>
													<asp:label id="lblStatusCode2" runat="server" CssClass="tableLabel" Width="123px" Height="22px">Second Status Code</asp:label></TD>
												<TD style="WIDTH: 128px" height="25"><cc1:mstextbox id="txtSSC" runat="server" CssClass="textField" Width="60px" MaxLength="10" TextMaskString="99/99/9999"></cc1:mstextbox></TD>
												<TD style="WIDTH: 1px" height="25"><FONT face="Tahoma"></FONT></TD>
												<TD style="WIDTH: 52px" height="25"><FONT face="Tahoma">&nbsp; </FONT>
													<asp:label id="Label5" runat="server" CssClass="tableLabel" Width="109px" Height="22px">Third Status Code</asp:label></TD>
												<TD style="WIDTH: 190px" height="25"><FONT face="Tahoma"><cc1:mstextbox id="txtTSC" runat="server" CssClass="textField" Width="60px" MaxLength="10" TextMaskString="99/99/9999"></cc1:mstextbox></FONT></TD>
											</TR>
										</TABLE>
									</td>
								</tr>
							</TABLE>
						</fieldset>
					</TD>
					<TD style="HEIGHT: 65px" vAlign="top">&nbsp;&nbsp;&nbsp;
					</TD>
				</TR>
				<TR>
					<TD style="WIDTH: 589px; HEIGHT: 71px" vAlign="top">
						<fieldset style="WIDTH: 560px; HEIGHT: 64px"><legend><asp:label id="lblRouteType" runat="server" CssClass="tableHeadingFieldset" Width="80px" Font-Bold="True"> DC Selection</asp:label></legend>
							<TABLE id="tblRouteType" style="WIDTH: 465px; HEIGHT: 8px" cellSpacing="0" cellPadding="0"
								align="left" border="0" runat="server">
								<TR>
									<TD class="tableLabel" style="WIDTH: 293px" colSpan="1" height="25" rowSpan="1">&nbsp;<asp:label id="lblOriginDisCenter" runat="server" CssClass="tableLabel" Width="154px" Height="22px">Origin Distribution Center</asp:label></TD>
									<TD style="WIDTH: 190px" height="25"><DBCOMBO:DBCOMBO id="DbComboOriginDC" tabIndex="14" CssClass="tableLabel" Width="161px" AutoPostBack="True"
											Runat="server" TextBoxColumns="18" RegistrationKey=" " TextUpLevelSearchButton="v" ShowDbComboLink="False" ServerMethod="DbComboDistributionCenterSelect"></DBCOMBO:DBCOMBO></TD>
								</TR>
								<TR>
									<TD class="tableLabel" style="WIDTH: 293px" height="25">&nbsp;<asp:label id="lblDesDisCenter" runat="server" CssClass="tableLabel" Width="181px" Height="22px">Destination Distribution Center</asp:label></TD>
									<TD style="WIDTH: 190px" height="25"><DBCOMBO:DBCOMBO id="DbComboDestinationDC" tabIndex="14" CssClass="tableLabel" Width="160px" AutoPostBack="True"
											Runat="server" TextBoxColumns="18" RegistrationKey=" " TextUpLevelSearchButton="v" ShowDbComboLink="False" ServerMethod="DbComboDistributionCenterSelect"></DBCOMBO:DBCOMBO></TD>
								</TR>
							</TABLE>
						</fieldset>
					</TD>
					<TD style="HEIGHT: 71px" vAlign="top">&nbsp;&nbsp;&nbsp;
					</TD>
				</TR>
				<TR>
					<TD style="HEIGHT: 50px" colSpan="2">
						<FIELDSET style="WIDTH: 560px; HEIGHT: 108px"><LEGEND><asp:label id="lblVersusStatus" runat="server" CssClass="tableHeadingFieldset" Width="119px"
									Font-Bold="True">Versus Status Code</asp:label></LEGEND>
							<TABLE id="tblVersusStatus" style="WIDTH: 520px; HEIGHT: 94px" cellSpacing="0" cellPadding="0"
								align="left" border="0" runat="server">
								<TR>
									<TD style="HEIGHT: 3px" colSpan="8" height="3"><FONT face="Tahoma"><asp:radiobutton id="rbVFirstCond" runat="server" Text="First condition: Record has first status without second status"
												CssClass="tableRadioButton" Width="480px" Height="23px" Checked="True" AutoPostBack="True" GroupName="StatusQuery"></asp:radiobutton></FONT></TD>
								</TR>
								<TR>
									<TD style="HEIGHT: 3px" colSpan="8" height="3"><asp:radiobutton id="rbVSecondCond" runat="server" Text="Second condition: First status is missing from history"
											CssClass="tableRadioButton" Width="480px" Height="23px" AutoPostBack="True" GroupName="StatusQuery"></asp:radiobutton></TD>
								</TR>
								<TR>
									<TD style="HEIGHT: 3px" colSpan="8" height="3"><asp:radiobutton id="rbVThirdCond" runat="server" Text="Third condition: Record has first status without second status or third status"
											CssClass="tableRadioButton" Width="480px" Height="23px" AutoPostBack="True" GroupName="StatusQuery"></asp:radiobutton></TD>
								</TR>
								<TR>
									<TD class="tableLabel" style="WIDTH: 68px" colSpan="8" height="25"><FONT face="Tahoma"><asp:radiobutton id="rbVFourthCond" runat="server" Text="Fourth condition: MDE status is missing"
												CssClass="tableRadioButton" Width="480px" Height="23px" AutoPostBack="True" GroupName="StatusQuery"></asp:radiobutton></FONT></TD>
								</TR>
							</TABLE>
						</FIELDSET>
					</TD>
				</TR>
			</TABLE>
			<asp:label id="Label1" style="Z-INDEX: 103; LEFT: 20px; POSITION: absolute; TOP: 4px" runat="server"
				CssClass="maintitleSize" Width="365px" Height="27px"> Versus Report Query</asp:label></TR></TABLE></TR></TABLE><INPUT 
style="Z-INDEX: 106; LEFT: 222px; POSITION: absolute; TOP: 35px" type=hidden 
value="<%=strScrollPosition%>" name=ScrollPosition>&nbsp;&nbsp;</form>
	</body>
</HTML>
