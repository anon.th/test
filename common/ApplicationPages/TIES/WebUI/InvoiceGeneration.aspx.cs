using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Threading;
using com.common.util;
using com.ties.DAL;
using com.ties.BAL;
using com.ties.classes;
using com.common.classes;
using com.common.applicationpages;

using System.IO;


namespace com.ties
{
	/// <summary>
	/// Summary description for InvoiceGeneration.
	/// </summary>
	public class InvoiceGeneration : BasePage
	{

		//Utility utility = null;
		private String m_strAppID;
		private String m_strEnterpriseID;
		DataSet dsInvoice = new DataSet();
		protected System.Web.UI.WebControls.Button btnGenerate;
		protected System.Web.UI.WebControls.Button btnClear;
		protected System.Web.UI.WebControls.Label lblErrorMessage;
		protected System.Web.UI.WebControls.Label Label2;
		protected System.Web.UI.WebControls.Label lblInvoiceFor;
		protected System.Web.UI.WebControls.RadioButton rbCashConsig;
		protected System.Web.UI.WebControls.CheckBox chkShipByCash;
		protected System.Web.UI.WebControls.CheckBox chkShipByCredit;
		protected System.Web.UI.WebControls.RadioButton rbCreditConsig;
		protected System.Web.UI.WebControls.Label lblCustomerCode;
		protected Cambro.Web.DbCombo.DbCombo dbCmbAgentId;
		protected System.Web.UI.WebControls.Label lblCustomerName;
		protected com.common.util.msTextBox txtCustomerName;
		protected System.Web.UI.WebControls.Label lblInvoiceToDate;
		protected com.common.util.msTextBox txtLastPickupDate;
		protected System.Web.UI.WebControls.Label lblTitle;
		protected System.Web.UI.HtmlControls.HtmlGenericControl divInvoiceGen;
		protected System.Web.UI.HtmlControls.HtmlTable tblShipmentExcepPODRepQry;
		protected System.Web.UI.HtmlControls.HtmlTable tblDates;
		protected System.Web.UI.WebControls.Label lblMainTitle;
		protected System.Web.UI.WebControls.DataGrid dgPreview;
		protected System.Web.UI.WebControls.Label Label5;
		protected System.Web.UI.WebControls.Label lblInvoiceType;
		protected System.Web.UI.WebControls.Label Label50;
		protected System.Web.UI.WebControls.Label lblCustID;
		protected System.Web.UI.WebControls.Label Label6;
		protected System.Web.UI.WebControls.Label lblProvince;
		protected System.Web.UI.WebControls.Label Label48;
		protected System.Web.UI.WebControls.Label lblCustName;
		protected System.Web.UI.WebControls.Label Label49;
		protected System.Web.UI.WebControls.Label lblTelephone;
		protected System.Web.UI.WebControls.Label Label45;
		protected System.Web.UI.WebControls.Label lblAddress1;
		protected System.Web.UI.WebControls.Label Label47;
		protected System.Web.UI.WebControls.Label lblFax;
		protected System.Web.UI.WebControls.Label Label1;
		protected System.Web.UI.WebControls.Label lblAddress2;
		protected System.Web.UI.WebControls.Label lblHC_POD;
		protected System.Web.UI.WebControls.Label Label3;
		protected System.Web.UI.WebControls.Label lblPostalCode;
		protected System.Web.UI.WebControls.Label Label46;
		protected System.Web.UI.WebControls.Label lblHC_POD_Required;
		protected System.Web.UI.WebControls.Button btnSelectAll;
		protected System.Web.UI.WebControls.Button btnCancel;
		protected System.Web.UI.WebControls.Button btnSelectAllHC;
		protected System.Web.UI.WebControls.Button btnGoToFirstPage;
		protected System.Web.UI.WebControls.Button btnPreviousPage;
		protected com.common.util.msTextBox txtGoToRec;
		protected System.Web.UI.WebControls.Button btnNextPage;
		protected System.Web.UI.WebControls.Button btnGoToLastPage;
		protected System.Web.UI.WebControls.Label lblErrorMsg;
		protected System.Web.UI.WebControls.Label lblNumRec;
		protected System.Web.UI.WebControls.ValidationSummary PageValidationSummary;
		protected System.Web.UI.HtmlControls.HtmlGenericControl divInvoiceGenPreview;
		protected System.Web.UI.HtmlControls.HtmlForm Form1;
		protected System.Web.UI.HtmlControls.HtmlTable Table10;
		//InvoiceCalculationMgrBAL InvoiceMgrBAL = null;
		InvoiceJobMgrBAL m_objInvoiceJobMgrBAL = null;

		private DataSet dsInvoiceHeader,dsInvoiceResult,dsExport;
		private DataSet dsInvoiceQuery;
		private Utility ut;
		static private int m_iSetSize = 4;
		private SessionDS m_sdsInvoiceHeader = null;
		protected System.Web.UI.WebControls.Button btnGenerateInv;
		string strInvType;
		protected System.Web.UI.WebControls.Button btnExport;
		protected Cambro.Web.DbCombo.DbCombo dbCmbAgentId_Cash;
		string strInvJobID="";

	
		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			dbCmbAgentId.RegistrationKey=System.Configuration.ConfigurationSettings.AppSettings["DbComboRegKey"];
			dbCmbAgentId_Cash.RegistrationKey=System.Configuration.ConfigurationSettings.AppSettings["DbComboRegKey"];  //Jeab 29 Nov 2011

			ut = new Utility(System.Configuration.ConfigurationSettings.AppSettings,Page.Session);

			this.btnGenerate.Attributes.Add("onclick","javascript:document.getElementById('"+ this.btnGenerate.ClientID + "').disabled=true;" + this.GetPostBackEventReference(this.btnGenerate));
			this.btnGenerateInv.Attributes.Add("onclick","javascript:document.getElementById('"+ this.btnGenerateInv.ClientID + "').disabled=true;" + this.GetPostBackEventReference(this.btnGenerateInv));

			m_strAppID = utility.GetAppID();
			m_strEnterpriseID = utility.GetEnterpriseID();
			if (!Page.IsPostBack)
			{
				if(this.divInvoiceGen.Visible)
					QueryMode();

				if(this.divInvoiceGenPreview.Visible)
				{
					ViewState["CPMode"] = ScreenMode.ExecuteQuery;
					ViewState["CPOperation"] = Operation.None;
					ViewState["IsTextChanged"] = false;
					ViewState["currentPage"] = 0;
					ViewState["currentSet"] = 0;
					ViewState["btnSelectAll"]=true;
					this.GetRecSet();
					DisplayCurrentPage();
				}
			}
			else
			{

				if(this.divInvoiceGen.Visible)
				{
					//Jeab 29 Nov 2011
					if(rbCashConsig.Checked  != true)
					{
						if(this.dbCmbAgentId.Text.Trim()!=""&&this.dbCmbAgentId.Text.Trim()!="CASH")
						{
							DataSet dsCustData = CustomerScheduledPickupsDAL.GetCustData(utility.GetAppID(), utility.GetEnterpriseID(),
								this.dbCmbAgentId.Text.Trim());
							if(dsCustData.Tables[0].Rows.Count > 0) 
							{
								this.txtCustomerName.Text = dsCustData.Tables[0].Rows[0]["cust_name"].ToString();
							}
							else
								this.txtCustomerName.Text = "";
						}
						else
							this.txtCustomerName.Text = "";
					}
					else
					{
						if(this.dbCmbAgentId_Cash.Text.Trim()!="")
						{
							DataSet dsCustData = CustomerScheduledPickupsDAL.GetCustData(utility.GetAppID(), utility.GetEnterpriseID(),
								this.dbCmbAgentId_Cash.Text.Trim());
							if(dsCustData.Tables[0].Rows.Count > 0) 
							{
								this.txtCustomerName.Text = dsCustData.Tables[0].Rows[0]["cust_name"].ToString();
							}
							else
								this.txtCustomerName.Text = "";
						}
						else
							this.txtCustomerName.Text = "";
					}
					//Jeab 29 Nov 2011  =========> End
				}

				if(this.divInvoiceGenPreview.Visible)
				{
					m_sdsInvoiceHeader = (SessionDS)Session["SESSION_DS1"];
					dsInvoiceQuery = (DataSet)Session["dsPreviewQUERY"];
				}

			}

			lblErrorMessage.Text = "";
		}

		[Cambro.Web.DbCombo.ResultsMethodAttribute(true)]
		public static object AgentIdServerMethod(Cambro.Web.DbCombo.ServerMethodArgs args)
		{
			Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings,HttpContext.Current.Session);
			String strAppID = util.GetAppID();
			String strEnterpriseID = util.GetEnterpriseID();
			
			String strAgentName="";
			if(args.ServerState != null && args.ServerState.Authenticate("kdfsjgnskajh[apq2-4uhg465654435879mnbfdm cxs;q4"))
			{			
				if(args.ServerState["strAgentName"] != null && args.ServerState["strAgentName"].ToString().Length > 0)
				{
					strAgentName = args.ServerState["strAgentName"].ToString();					
				}	
			}
			
			DataSet dataset = DbComboDAL.PayerIDQueryInvoiceGeneration(strAppID,strEnterpriseID,args, "R", strAgentName);
			return dataset;
		}

		//Jeab 29 Nov 2011
		[Cambro.Web.DbCombo.ResultsMethodAttribute(true)]
		public static object AgentIdCashServerMethod(Cambro.Web.DbCombo.ServerMethodArgs args)
		{
			Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings,HttpContext.Current.Session);
			String strAppID = util.GetAppID();
			String strEnterpriseID = util.GetEnterpriseID();
			
			String strAgentName="";
			if(args.ServerState != null && args.ServerState.Authenticate("kdfsjgnskajh[apq2-4uhg465654435879mnbfdm cxs;q4"))
			{			
				if(args.ServerState["strAgentName"] != null && args.ServerState["strAgentName"].ToString().Length > 0)
				{
					strAgentName = args.ServerState["strAgentName"].ToString();					
				}	
			}
			
			DataSet dataset = DbComboDAL.PayerIDQueryInvoiceGeneration(strAppID,strEnterpriseID,args, "CASH", strAgentName);
			return dataset;
		}
		//Jeab 29 Nov 2011  =========> End


		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			base.OnInit(e);
			InitializeComponent();
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnGenerate.Click += new System.EventHandler(this.btnGenerate_Click);
			this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
			this.rbCashConsig.CheckedChanged += new System.EventHandler(this.rbCashConsig_CheckedChanged);
			this.chkShipByCash.CheckedChanged += new System.EventHandler(this.chkShipByCash_CheckedChanged);
			this.rbCreditConsig.CheckedChanged += new System.EventHandler(this.rbCreditConsig_CheckedChanged);
			this.btnSelectAll.Click += new System.EventHandler(this.btnSelectAll_Click);
			this.btnExport.Click += new System.EventHandler(this.btnExport_Click);
			this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
			this.btnGenerateInv.Click += new System.EventHandler(this.btnGenerateInv_Click);
			this.btnSelectAllHC.Click += new System.EventHandler(this.btnSelectAllHC_Click);
			this.btnGoToFirstPage.Click += new System.EventHandler(this.btnGoToFirstPage_Click);
			this.btnPreviousPage.Click += new System.EventHandler(this.btnPreviousPage_Click);
			this.txtGoToRec.TextChanged += new System.EventHandler(this.txtGoToRec_TextChanged);
			this.btnNextPage.Click += new System.EventHandler(this.btnNextPage_Click);
			this.btnGoToLastPage.Click += new System.EventHandler(this.btnGoToLastPage_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

#region "InvoiceGEN"
		private void btnGenerate_Click(object sender, System.EventArgs e)
		{
			if(this.rbCashConsig.Checked)
				if(!(this.chkShipByCash.Checked) && !(this.chkShipByCredit.Checked))
				{
					this.lblErrorMessage.Text="Please Select Ship By Cash and/ or Ship By Credit.";
					this.btnGenerate.Enabled=true;
					return;
				}

			if(this.rbCreditConsig.Checked)
				if(!(this.dbCmbAgentId.Text.Length>0))
				{
					this.lblErrorMessage.Text="Please Select Customer ID.";
					this.btnGenerate.Enabled=true;
					return;
				}
			//Jeab 29 Nov 2011
			if(this.rbCashConsig.Checked)
				if(!(this.dbCmbAgentId_Cash.Text.Length>0))
				{
					this.lblErrorMessage.Text="Please Select Customer ID.";
					this.btnGenerate.Enabled=true;
					return;
				}
			//Jeab 29 Nov 2011  =========> End

			dsInvoice = GetFilterData();
			
			if(InvoiceGenerationMgrDAL.CheckExistingInvoiceProcess(m_strAppID, m_strEnterpriseID, ut.GetUserID(),dsInvoice , Session.SessionID))
			{
				EnableCustomer();
				this.lblErrorMessage.Text="Invoice Generation is in progress. Please try again later.";
				this.btnGenerate.Enabled=true;
				return;
			}

			if(!InvoiceGenerationMgrDAL.GenTempInvoicePreview(m_strAppID,m_strEnterpriseID,ut.GetUserID(),dsInvoice, Session.SessionID))
			{
				this.lblErrorMessage.Text="No consignments for generating invoice.";
				this.btnGenerate.Enabled=true;
				return;
			}

			this.btnGenerate.Enabled=false;
			Session["dsPreviewQUERY"] = dsInvoice;
			ViewState["btnSelectAll"]=true;
			this.divInvoiceGen.Visible=false;
			this.divInvoiceGenPreview.Visible=true;
	
			m_strAppID = ut.GetAppID();
			m_strEnterpriseID = ut.GetEnterpriseID();
			EnableNavigationButtons(true,true,true,true);
			ViewState["CPMode"] = ScreenMode.ExecuteQuery;
			ViewState["CPOperation"] = Operation.None;
			ViewState["IsTextChanged"] = false;
			ViewState["currentPage"] = 0;
			ViewState["currentSet"] = 0;
			//CREATE DATA INTO TEMP TABLE
			//END CREATE DATA INTO TEMP TABLE
			this.GetRecSet();
			DisplayCurrentPage();
		}

		private void btnClear_Click(object sender, System.EventArgs e)
		{
			QueryMode();
		}

		private void rbCashConsig_CheckedChanged(object sender, System.EventArgs e)
		{
			dbCmbAgentId.Text="";
			dbCmbAgentId.Value="";
			this.lblErrorMessage.Text="";
			this.lblErrorMsg.Text="";
			this.chkShipByCash.Checked=true;
			this.chkShipByCredit.Checked=true;
			//Jeab 23 Nov 2011
//			dbCmbAgentId.EnableViewState =false;
//			this.dbCmbAgentId.Enabled=false;
//			DisableCustomer();
			txtCustomerName.Text = "";
			this.dbCmbAgentId_Cash.Enabled=true;
			dbCmbAgentId_Cash.EnableViewState =true;
			this.txtCustomerName.BackColor = System.Drawing.Color.White;
			this.txtCustomerName.ForeColor = System.Drawing.Color.Black;
			EnableCustomer();
			this.chkShipByCash.Enabled=true;
			this.chkShipByCredit.Enabled=true;
			dbCmbAgentId_Cash.Visible=true;
			dbCmbAgentId_Cash.Width=210;
			dbCmbAgentId.Visible=false;
			dbCmbAgentId.Width =0;
			//Jeab 23 Nov 2011  =========> End
		}

		private void rbCreditConsig_CheckedChanged(object sender, System.EventArgs e)
		{
			this.chkShipByCash.Checked=false;
			this.chkShipByCredit.Checked=false;
			this.lblErrorMessage.Text="";
			this.lblErrorMsg.Text="";
			txtCustomerName.Text = "";
			this.dbCmbAgentId.Enabled=true;
			dbCmbAgentId.EnableViewState =true;
			this.txtCustomerName.BackColor = System.Drawing.Color.White;
			this.txtCustomerName.ForeColor = System.Drawing.Color.Black;
			EnableCustomer();
			//Jeab 21 Dec 2011
			dbCmbAgentId_Cash.Visible=false;
			dbCmbAgentId.Visible=true;
			//Jeab 21 Dec 2011  =========>  End
		}

		private void EnableCustomer()
		{
			this.dbCmbAgentId.Value="";
			this.dbCmbAgentId.Text="";
			//Jeab 21 Dec 2011
			this.dbCmbAgentId_Cash.Value="";
			this.dbCmbAgentId_Cash.Text="";
			//Jeab 21 Dec 2011  =========>  End
			txtCustomerName.Text = null;
			txtCustomerName.Enabled = false;
			this.txtCustomerName.BackColor = System.Drawing.Color.White;
			this.txtCustomerName.ForeColor = System.Drawing.Color.Black;
			this.chkShipByCash.Enabled=false;
			this.chkShipByCredit.Enabled=false;

			lblCustomerCode.Text = Utility.GetLanguageText(ResourceType.ScreenLabel, lblCustomerCode.Text, utility.GetUserCulture());
			lblCustomerName.Text = Utility.GetLanguageText(ResourceType.ScreenLabel, lblCustomerName.Text, utility.GetUserCulture());
		}

		private void DisableCustomer()
		{
			this.dbCmbAgentId.Value="";
			this.dbCmbAgentId.Text="CASH";
			txtCustomerName.Text = "";
			txtCustomerName.BackColor = System.Drawing.Color.White;//System.Drawing.Color.LightGray;
			txtCustomerName.Enabled = false;
			this.chkShipByCash.Enabled=true;
			this.chkShipByCredit.Enabled=true;
		}

		private void QueryMode()
		{
			this.divInvoiceGenPreview.Visible=false;
			//			rbCashConsig.Checked = true;
			//			this.chkShipByCash.Checked=false;
			//			this.chkShipByCredit.Checked=false;
			//			rbCreditConsig.Checked = false;
			//			txtCustomerName.Text = null;
			//			txtCustomerName.Enabled = false;
			rbCashConsig.Checked = false;
			rbCreditConsig.Checked = true;
			this.chkShipByCash.Checked=false;
			this.chkShipByCredit.Checked=false;
			this.chkShipByCash.Enabled=false;
			this.chkShipByCredit.Enabled=false;
			//
			//			txtCustomerName.Text = null;
			//			txtCustomerName.Enabled = false;


			//			this.dbCmbAgentId.Text="";
			//			this.dbCmbAgentId.Enabled=true;
			txtLastPickupDate.Text = null;
			txtLastPickupDate.Enabled = true;
			//DisableCustomer();
			EnableCustomer();
			//Jeab 19 Dec 2011
			dbCmbAgentId.Visible=true;
			dbCmbAgentId.Width=210;
			dbCmbAgentId_Cash.Visible=false;
			dbCmbAgentId_Cash.Width =0;
			//Jeab 19 Dec 2011  =========> End
		}

		private DataSet GetFilterData()
		{
			DataTable dtInvoice = new DataTable();
			dtInvoice.Columns.Add(new DataColumn("Payment_mode", typeof(string)));
			dtInvoice.Columns.Add(new DataColumn("Cash_Cust", typeof(bool)));
			dtInvoice.Columns.Add(new DataColumn("Credit_Cust", typeof(bool)));
			dtInvoice.Columns.Add(new DataColumn("Customer_Code", typeof(string)));
			dtInvoice.Columns.Add(new DataColumn("Last_Pickup_Date", typeof(DateTime)));

			DataRow drInvoice = dtInvoice.NewRow();
			if (rbCashConsig.Checked)
			{
				if(this.chkShipByCash.Checked)
					drInvoice["Cash_Cust"] = true;
				else
					drInvoice["Cash_Cust"] = false;
				if(this.chkShipByCredit.Checked)
					drInvoice["Credit_Cust"] = true;
				else
					drInvoice["Credit_Cust"] = false;

				drInvoice["Payment_mode"] = "C";
				Session["Invoice_Type"] = "CASH";
			}
			else if (rbCreditConsig.Checked)
			{
				drInvoice["Payment_mode"] = "R";
				Session["Invoice_Type"] = "CREDIT";
			}
			
			//Jeab 29 Nov 2011
			if (rbCreditConsig.Checked)
			{
				drInvoice["Customer_Code"] = this.dbCmbAgentId.Text.ToString().Trim();
			}
			else
			{
				drInvoice["Customer_Code"] = this.dbCmbAgentId_Cash.Text.ToString().Trim();
			}
			//Jeab 29 Nov 2011  =========> End

			if (this.txtLastPickupDate.Text.Trim() != "")
				drInvoice["Last_Pickup_Date"] = DateTime.ParseExact(this.txtLastPickupDate.Text+" 23:59:59","dd/MM/yyyy HH:mm:ss",null);

			dtInvoice.Rows.Add(drInvoice);
			DataSet dsInv = new DataSet();
			dsInv.Tables.Add(dtInvoice);
			return  dsInv;
		}

		private void chkShipByCash_CheckedChanged(object sender, System.EventArgs e)
		{
			if(chkShipByCash.Checked)
			{
				this.dbCmbAgentId.Text="CASH";
				this.dbCmbAgentId.Enabled=false;
			}
		}

#endregion

#region "PREVIEW"
		private void GetRecSet()
		{
			String tmpStr = ViewState["currentSet"].ToString().Trim();
			int iStartIndex = 0;
			int intIndexOf = tmpStr.IndexOf(".");
	
			if (intIndexOf > 0)
			{
				iStartIndex = Convert.ToInt32(tmpStr.Substring(0, intIndexOf)) * m_iSetSize;
			}
			else
			{
				iStartIndex = Convert.ToInt32(tmpStr) * m_iSetSize;
			}

			dsInvoiceQuery = (DataSet)Session["dsPreviewQUERY"];
			//m_sdsInvoiceHeader  = InvoiceGenerationMgrDAL.GetInvoiceableShipmentsHeader(m_strAppID, m_strEnterpriseID,iStartIndex,m_iSetSize, dsInvoiceQuery);
			m_sdsInvoiceHeader  = InvoiceGenerationMgrDAL.GetTempInvoiceableShipmentsHeader(m_strAppID, m_strEnterpriseID,iStartIndex,m_iSetSize,ut.GetUserID(),Session.SessionID.ToString());
			
			Session["SESSION_DS1"] = m_sdsInvoiceHeader;
			decimal pgCnt = (m_sdsInvoiceHeader.QueryResultMaxSize - 1)/m_iSetSize;
			if(pgCnt < Convert.ToInt32(ViewState["currentSet"]))
			{
				ViewState["currentSet"] = System.Convert.ToInt32(pgCnt);
			}

			if(m_sdsInvoiceHeader.QueryResultMaxSize == 0 || m_sdsInvoiceHeader.QueryResultMaxSize == 1)
				EnableNavigationButtons(false,false,false,false);

		}

		public void DisplayCurrentPage()
		{
			
			this.lblErrorMessage.Text="";
			this.lblErrorMsg.Text="";

			ViewState["btnSelectAll"]=true;
			this.btnSelectAll.Text="Select All";

			strInvType = (string)Session["Invoice_Type"];

			BindHeader(strInvType);
			DisplayRecNum();

		}

		public void BindHeader(string strInvType)
		{

			if(m_sdsInvoiceHeader.ds.Tables[0].Rows.Count <= 0) 
			{
				return;
			}

			DataRow drCurrent = m_sdsInvoiceHeader.ds.Tables[0].Rows[Convert.ToInt32(ViewState["currentPage"])];
			Session["currentHeader"] = drCurrent;

			//CLEAR ALL HEADER
			this.lblInvoiceType.Text = "";
			this.lblCustID.Text = "";
			this.lblCustName.Text = "";
			this.lblAddress1.Text = "";
			this.lblAddress2.Text = "";
			this.lblPostalCode.Text = "";
			this.lblProvince.Text = "";
			this.lblTelephone.Text = "";
			this.lblFax.Text = "";
			this.lblHC_POD_Required.Text = "";
			string strCPayment_mode = "";
			//END OF CLEAR ALL HEADER
			
			
			if(strInvType=="CASH")
				this.lblInvoiceType.Text = "CASH";
			else
				this.lblInvoiceType.Text = "CREDIT";	
				
			//IN CASE OF INVOICE TYPE = CREDIT

			if(strInvType=="CREDIT")
			{
				if(drCurrent["payerid"]!=null&&(!drCurrent["payerid"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					this.lblCustID.Text = drCurrent["payerid"].ToString();
					ViewState["payerID"] = drCurrent["payerid"].ToString();
				}

				if(drCurrent["sender_name"]!=null&&(!drCurrent["sender_name"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					this.lblCustName.Text = drCurrent["sender_name"].ToString();

				if(drCurrent["sender_address1"]!=null&&(!drCurrent["sender_address1"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					this.lblAddress1.Text = drCurrent["sender_address1"].ToString();

				if(drCurrent["sender_address2"]!=null&&(!drCurrent["sender_address2"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					this.lblAddress2.Text = drCurrent["sender_address2"].ToString();

				if(drCurrent["sender_zipcode"]!=null&&(!drCurrent["sender_zipcode"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					string strZipCode = drCurrent["sender_zipcode"].ToString();

					this.lblPostalCode.Text = strZipCode;

					Zipcode zipCode = new Zipcode();
					zipCode.Populate(this.m_strAppID,this.m_strEnterpriseID,strZipCode);
					String strStateCode = zipCode.StateCode;

					this.lblProvince.Text = strStateCode;
				}

				if(drCurrent["sender_telephone"]!=null&&(!drCurrent["sender_telephone"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					this.lblTelephone.Text = drCurrent["sender_telephone"].ToString();

				if(drCurrent["sender_fax"]!=null&&(!drCurrent["sender_fax"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					this.lblFax.Text = drCurrent["sender_fax"].ToString();

				if(drCurrent["pod_slip_required"]!=null&&(!drCurrent["pod_slip_required"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					if(drCurrent["pod_slip_required"].ToString()=="N")
					{
						this.lblHC_POD_Required.Text = "NO";
						this.btnSelectAllHC.Enabled=false;
					}
					else
					{
						this.lblHC_POD_Required.Text = "YES";
						this.btnSelectAllHC.Enabled=true;
					}
				}

			}
			//END OF INVOICE TYPE = CREDIT

			if(drCurrent["CPayment_mode"]!=null&&(!drCurrent["CPayment_mode"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				strCPayment_mode = drCurrent["CPayment_mode"].ToString().Trim().ToString();

			//IN CASE OF INVOICE TYPE == "CASH"
			if(strInvType=="CASH") //&&drCurrent["CPayment_mode"].ToString()=="R")
			{
				if(drCurrent["payerid"]!=null&&(!drCurrent["payerid"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					if(strCPayment_mode.Equals("C"))
						this.lblCustID.Text = "N/A";
					else
    					this.lblCustID.Text = drCurrent["payerid"].ToString();

					ViewState["payerID"] = drCurrent["payerid"].ToString();
				}

				if(drCurrent["sender_name"]!=null&&(!drCurrent["sender_name"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					this.lblCustName.Text = drCurrent["sender_name"].ToString();

				if(drCurrent["sender_address1"]!=null&&(!drCurrent["sender_address1"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					this.lblAddress1.Text = drCurrent["sender_address1"].ToString();

				if(drCurrent["sender_address2"]!=null&&(!drCurrent["sender_address2"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					this.lblAddress2.Text = drCurrent["sender_address2"].ToString();

				if(drCurrent["sender_zipcode"]!=null&&(!drCurrent["sender_zipcode"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					string strZipCode = drCurrent["sender_zipcode"].ToString();

					this.lblPostalCode.Text = strZipCode;

					Zipcode zipCode = new Zipcode();
					zipCode.Populate(this.m_strAppID,this.m_strEnterpriseID,strZipCode);
					String strStateCode = zipCode.StateCode;

					this.lblProvince.Text = strStateCode;
				}

				if(drCurrent["sender_telephone"]!=null&&(!drCurrent["sender_telephone"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					this.lblTelephone.Text = drCurrent["sender_telephone"].ToString();

				if(drCurrent["sender_fax"]!=null&&(!drCurrent["sender_fax"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					this.lblFax.Text = drCurrent["sender_fax"].ToString();

				this.lblHC_POD_Required.Text = "NO";

				this.btnSelectAllHC.Enabled=false;

			}
			//END OF INVOICE TYPE = CASH
				
			//dsInvoiceResult = InvoiceGenerationMgrDAL.GetInvoiceableShipmentsList(m_strAppID,m_strEnterpriseID,dsInvoiceQuery,this.lblCustID.Text,this.lblCustName.Text);
			dsInvoiceResult = InvoiceGenerationMgrDAL.GetTempInvoiceableShipmentsList(m_strAppID,m_strEnterpriseID,ut.GetUserID(),drCurrent["CPayment_mode"].ToString(),this.lblCustID.Text,this.lblCustName.Text, Session.SessionID);
			
			BindGrid(dsInvoiceResult,drCurrent["CPayment_mode"].ToString());
		
		
		}

		

		public void BindGrid(DataSet dsResult,String strCustType)
		{
			ViewState["dsExport"] = dsResult;
			this.dgPreview.DataSource = dsResult;
			this.dgPreview.DataBind(); 

			DataSet dsInvoiceQuery = (DataSet)Session["dsPreviewQUERY"];
			string strPayerID = ViewState["payerID"].ToString();
			//���������� DDLSC ���
			//DataSet dsDDLSC = InvoiceGenerationMgrDAL.GetInvoiceableShipmentsCustomer(m_strAppID, m_strEnterpriseID,dsInvoiceQuery,this.lblCustID.Text,this.lblCustName.Text);
			DataSet dsDDLSC = InvoiceGenerationMgrDAL.GetTempInvoiceableShipmentsCustomer(m_strAppID, m_strEnterpriseID,ut.GetUserID(),strCustType,this.lblCustID.Text,this.lblCustName.Text,Session.SessionID.ToString());

			//BIND DDL & SAVE BUTTON
			if(strInvType=="CREDIT")
			{//IF EQUALS TO CREDIT , DISABLED DDL & SAVE BUTTON - ELSE ENABLED ALL
				for(int i = 0; i <= dgPreview.Items.Count - 1; i++)
				{
					DropDownList ddlSCShipmentUpdate = (DropDownList)this.dgPreview.Items[i].FindControl("ddlSCShipmentUpdate");
					ddlSCShipmentUpdate.DataSource = dsDDLSC;
					ddlSCShipmentUpdate.DataTextField = "sender_name";
					ddlSCShipmentUpdate.DataValueField = "sender_name";
					ddlSCShipmentUpdate.DataBind();
					ddlSCShipmentUpdate.Enabled=false;

					ImageButton imgSave = (ImageButton)this.dgPreview.Items[i].FindControl("imgSave");
					imgSave.Visible = false;

					//					if(this.lblHC_POD_Required.Text=="NO")
					//					{
					CheckBox chkSelect = (CheckBox)this.dgPreview.Items[i].FindControl("chkSelect");
					//						if(!chkSelect.Checked && (bool)Session["initdgCheck"])
					//							chkSelect.Checked = true;
					//					}
					chkSelect.Attributes.Add("RowNumber", i.ToString());
				}
			}
			if(strInvType=="CASH")
			{
				for(int i = 0; i <= dgPreview.Items.Count - 1; i++)
				{
					DropDownList ddlSCShipmentUpdate = (DropDownList)this.dgPreview.Items[i].FindControl("ddlSCShipmentUpdate");
					ddlSCShipmentUpdate.DataSource = dsDDLSC;
					ddlSCShipmentUpdate.DataTextField = "sender_name";
					ddlSCShipmentUpdate.DataValueField = "sender_name";
					ddlSCShipmentUpdate.DataBind();
					ddlSCShipmentUpdate.Enabled=true;

					ImageButton imgSave = (ImageButton)this.dgPreview.Items[i].FindControl("imgSave");
					imgSave.Visible = true;

					//IF HC POD REQUIRED = NO, CHECK ALL
					//					if(this.lblHC_POD_Required.Text=="NO")
					//					{
					CheckBox chkSelect = (CheckBox)this.dgPreview.Items[i].FindControl("chkSelect");
					//
					//						if(!chkSelect.Checked && (bool)Session["initdgCheck"])
					//							chkSelect.Checked = true;
					//					}
					chkSelect.Attributes.Add("RowNumber", i.ToString());

				}
			}

			//SELECT DROP DOWN LIST SAME AS SENDER_NAME
			for(int i = 0; i <= dgPreview.Items.Count - 1; i++)
			{
				DropDownList ddlSCShipmentUpdate = (DropDownList)this.dgPreview.Items[i].FindControl("ddlSCShipmentUpdate");
				Label dgLabelSenderName = (Label)this.dgPreview.Items[i].FindControl("dgLabelSenderName");
				ImageButton imgSave = (ImageButton)this.dgPreview.Items[i].FindControl("imgSave");

				ddlSCShipmentUpdate.SelectedIndex = ddlSCShipmentUpdate.Items.IndexOf(ddlSCShipmentUpdate.Items.FindByValue(dgLabelSenderName.Text.ToUpper().ToString()));
				if(strCustType=="R")
				{
					ddlSCShipmentUpdate.Enabled=false;
					imgSave.Visible = false;
				}

			}
			//END OF SELECT DDL

			if((TIESDAL.PostDeliveryCOD.GetUserRole(utility.GetAppID(),utility.GetEnterpriseID(),utility.GetUserID(),"ACCT")==0)&&(TIESDAL.PostDeliveryCOD.GetUserRole(utility.GetAppID(),utility.GetEnterpriseID(),utility.GetUserID(),"ACCTMGR")==0))
			{
				this.btnGenerateInv.Enabled=false;
				//����������� ACCT ����Ѿഷ DDL �С����� SAVE ����� ���� GENINV ��������
				for(int i = 0; i <= dgPreview.Items.Count - 1; i++)
				{
					DropDownList ddlSCShipmentUpdate = (DropDownList)this.dgPreview.Items[i].FindControl("ddlSCShipmentUpdate");
					ddlSCShipmentUpdate.Enabled=false;

					ImageButton imgSave = (ImageButton)this.dgPreview.Items[i].FindControl("imgSave");
					imgSave.Visible = false;
				}
			}
			else
			{
				this.btnGenerateInv.Enabled=true;
			}
		}

		private void btnCancel_Click(object sender, System.EventArgs e)
		{
			//Response.Redirect("InvoiceGeneration.aspx");
			this.divInvoiceGen.Visible=true;
			this.btnGenerate.Enabled=true;
			this.divInvoiceGenPreview.Visible=false;
			
			InvoiceGenerationMgrDAL.DeleteTempInvoicePreview(ut.GetAppID(), ut.GetEnterpriseID(), ut.GetUserID(), null, Session.SessionID); 

			QueryMode();
			//CLEAR ALL TEMP DATA

			//END CLEAR ALL TEMP DATA
		}

		private void txtGoToRec_TextChanged(object sender, System.EventArgs e)
		{
			int iPageEnter = Convert.ToInt32(this.txtGoToRec.Text);
			if(iPageEnter<=0)
				iPageEnter=0;

			int iSetSize = Convert.ToInt32(m_sdsInvoiceHeader.DataSetRecSize);

			if( iPageEnter> m_sdsInvoiceHeader.QueryResultMaxSize)
			{
				this.txtGoToRec.Text="";
				return;
			}

			int iCurrentSet = (int)Math.Floor((iPageEnter-1)/iSetSize);
			ViewState["currentSet"] = iCurrentSet;

			String tmpStr = ViewState["currentSet"].ToString().Trim();
			int iStartIndex = 0;
			int intIndexOf = tmpStr.IndexOf(".");

			if (intIndexOf > 0)
			{
				iStartIndex = Convert.ToInt32(tmpStr.Substring(0, intIndexOf)) * m_iSetSize;
			}
			else
			{
				iStartIndex = Convert.ToInt32(tmpStr) * m_iSetSize;
			}
			DataSet dsQuery = (DataSet)ViewState["QUERY_DS"];
			m_sdsInvoiceHeader = InvoiceGenerationMgrDAL.GetTempInvoiceableShipmentsHeader(m_strAppID, m_strEnterpriseID,iStartIndex,m_iSetSize,ut.GetUserID(),Session.SessionID.ToString());
			Session["SESSION_DS1"] = m_sdsInvoiceHeader;

			System.Threading.Thread.Sleep(3500);

			int iCurrentPage = (int)(Math.IEEERemainder((iPageEnter-1),iSetSize)); //.DivRem(iPageEnter-1/iSetSize);

			if(iCurrentPage>=0)
				ViewState["currentPage"] = (int)Math.Abs(iCurrentPage);
			else
				ViewState["currentPage"] = iSetSize - (int)Math.Abs(iCurrentPage);

			
			DisplayCurrentPage();
			DisplayRecNum();

			EnableNavigationButtons(true,true,true,true);

			if( iPageEnter >=  m_sdsInvoiceHeader.QueryResultMaxSize)
			{
				EnableNavigationButtons(true,true,false,false);
				this.txtGoToRec.Text="";
				return;
			}

			if(((Convert.ToInt32(ViewState["currentSet"]) - 1) < 0)&&((Convert.ToInt32(ViewState["currentPage"]) - 1) < 0))
			{
				EnableNavigationButtons(false,false,true,true);
				this.txtGoToRec.Text="";
				return;
			}

			this.txtGoToRec.Text="";

		}

		private void btnNextPage_Click(object sender, System.EventArgs e)
		{
			EnableNavigationButtons(true,true,true,true);
			int iCurrentPage = Convert.ToInt32(ViewState["currentPage"]);

			if(Convert.ToInt32(ViewState["currentPage"])  < (Convert.ToInt32(m_sdsInvoiceHeader.DataSetRecSize) - 1) )
			{
				ViewState["currentPage"] = Convert.ToInt32(ViewState["currentPage"]) + 1;
				DisplayCurrentPage();
			}
			else
			{
				decimal iTotalRec = (Convert.ToInt32(ViewState["currentRefSet"]) * m_iSetSize) + Convert.ToInt32(m_sdsInvoiceHeader.DataSetRecSize);
				if( iTotalRec ==  m_sdsInvoiceHeader.QueryResultMaxSize)
				{
					EnableNavigationButtons(true,true,false,false);
					return;
				}
				
				ViewState["currentSet"] = Convert.ToInt32(ViewState["currentSet"]) + 1;	
				GetRecSet();
				ViewState["currentPage"] = 0;
				DisplayCurrentPage();
			} 
			DisplayRecNum();
		}

		private void btnGoToLastPage_Click(object sender, System.EventArgs e)
		{
			ViewState["currentSet"] = (int)Math.Floor((double)(m_sdsInvoiceHeader.QueryResultMaxSize - 1)/m_iSetSize);//(m_sdsInvoiceHeader.QueryResultMaxSize - 1)/m_iSetSize;	
			GetRecSet();

			//ViewState["currentPage"] = (int)(Math.IEEERemainder((double)(m_sdsInvoiceHeader.QueryResultMaxSize - 1),(double)m_iSetSize)); //m_sdsInvoiceHeader.DataSetRecSize - 1;

			int iCurrentPage = (int)(Math.IEEERemainder((double)(m_sdsInvoiceHeader.QueryResultMaxSize - 1),(double)m_iSetSize)); //.DivRem(iPageEnter-1/iSetSize);

			if(iCurrentPage>=0)
				ViewState["currentPage"] = (int)Math.Abs(iCurrentPage);
			else
				ViewState["currentPage"] = m_iSetSize - (int)Math.Abs(iCurrentPage);


			DisplayCurrentPage();	
			DisplayRecNum();
			EnableNavigationButtons(true,true,false,false);
		}

		private void btnPreviousPage_Click(object sender, System.EventArgs e)
		{
			int iCurrentPage = Convert.ToInt32(ViewState["currentPage"]);

			if(Convert.ToInt32(ViewState["currentPage"])  > 0 )
			{
				ViewState["currentPage"] = Convert.ToInt32(ViewState["currentPage"]) - 1;
				DisplayCurrentPage();
			}
			else
			{
				if( (Convert.ToInt32(ViewState["currentSet"]) - 1) < 0)
				{
					EnableNavigationButtons(false,false,true,true);
					return;
				}
				ViewState["currentSet"] = Convert.ToInt32(ViewState["currentSet"]) - 1;	
				GetRecSet();
				System.Threading.Thread.Sleep(3500);

				ViewState["currentPage"] = m_sdsInvoiceHeader.DataSetRecSize - 1;
				DisplayCurrentPage();
			}
			DisplayRecNum();
			EnableNavigationButtons(true,true,true,true);

		}

		private void btnGoToFirstPage_Click(object sender, System.EventArgs e)
		{
			ViewState["currentSet"] = 0;	
			ViewState["currentPage"] = 0;
			GetRecSet();
			DisplayCurrentPage();
			DisplayRecNum();
			EnableNavigationButtons(false,false,true,true);
		}

		private void DisplayRecNum()
		{
			int iCurrentRec = (Convert.ToInt32(ViewState["currentPage"]) + 1) + (Convert.ToInt32(ViewState["currentSet"]) * m_iSetSize) ;

			if(!ut.GetUserCulture().ToUpper().Equals("EN-US"))
			{
				lblNumRec.Text = ""+ iCurrentRec + " " + Utility.GetLanguageText(ResourceType.ScreenLabel, "of", ut.GetUserCulture()) + " " + m_sdsInvoiceHeader.QueryResultMaxSize + " " + Utility.GetLanguageText(ResourceType.ScreenLabel, "record(s)", ut.GetUserCulture());
			}
			else
			{
				lblNumRec.Text = ""+ iCurrentRec + " of " + m_sdsInvoiceHeader.QueryResultMaxSize + " record(s)";
			}

			//ADDED BY X FEB 22 08
			if(iCurrentRec>=m_sdsInvoiceHeader.QueryResultMaxSize)
			{
				EnableNavigationButtons(true,true,false,false);
				return;
			}
			//END ADDED BY X FEB 22 08
		}

		private void EnableNavigationButtons(bool bMoveFirst, bool bMoveNext,bool bEnableMovePrevious, bool bMoveLast)
		{
			btnGoToFirstPage.Enabled	= bMoveFirst;
			btnPreviousPage.Enabled		= bMoveNext;
			btnNextPage.Enabled			= bEnableMovePrevious;
			btnGoToLastPage.Enabled		= bMoveLast;
		}

		private void btnSelectAll_Click(object sender, System.EventArgs e)
		{
			string strConsigNo=null;

			Button btnSelectAll = (Button)this.FindControl("btnSelectAll");
			if((bool)ViewState["btnSelectAll"])
			{
				ViewState["btnSelectAll"]=false;
				btnSelectAll.Text="Deselect All";
				for(int i = 0; i <= dgPreview.Items.Count - 1; i++)
				{
					CheckBox chkSelect = (CheckBox)this.dgPreview.Items[i].FindControl("chkSelect");
					chkSelect.Checked = true;
					strConsigNo = this.dgPreview.Items[i].Cells[1].Text;
					InvoiceGenerationMgrDAL.UpdateTempCheckedConsignment(utility.GetAppID(),utility.GetEnterpriseID(),ut.GetUserID(),strConsigNo,chkSelect.Checked,Session.SessionID);
				}
			}
			else
			{
				ViewState["btnSelectAll"]=true;
				btnSelectAll.Text="Select All";
				for(int i = 0; i <= dgPreview.Items.Count - 1; i++)
				{
					CheckBox chkSelect = (CheckBox)this.dgPreview.Items[i].FindControl("chkSelect");
					chkSelect.Checked = false;
					strConsigNo = this.dgPreview.Items[i].Cells[1].Text;
					InvoiceGenerationMgrDAL.UpdateTempCheckedConsignment(utility.GetAppID(),utility.GetEnterpriseID(),ut.GetUserID(),strConsigNo,chkSelect.Checked, Session.SessionID);
				}
			}
		}

		private void btnSelectAllHC_Click(object sender, System.EventArgs e)
		{
			string strConsigNo=null;
			//the shipment_tracking table is searching for the HCRHQ status code, if present, that consignment is selected.
			//GET THE CONSIG_no and search for HCRHQ status in Shipment_tracking
			for(int i = 0; i <= dgPreview.Items.Count - 1; i++)
			{
				CheckBox chkSelect = (CheckBox)this.dgPreview.Items[i].FindControl("chkSelect");

				strConsigNo = this.dgPreview.Items[i].Cells[1].Text;

				if(InvoiceGenerationMgrDAL.checkHCReturned(m_strAppID, m_strEnterpriseID,strConsigNo))
				{
					chkSelect.Checked=true;
					InvoiceGenerationMgrDAL.UpdateTempCheckedConsignment(utility.GetAppID(),utility.GetEnterpriseID(),ut.GetUserID(),strConsigNo,chkSelect.Checked, Session.SessionID);
				}
				else
				{
					chkSelect.Checked=false;
					InvoiceGenerationMgrDAL.UpdateTempCheckedConsignment(utility.GetAppID(),utility.GetEnterpriseID(),ut.GetUserID(),strConsigNo,chkSelect.Checked, Session.SessionID);
				}
			}
		}

		protected void dgPreview_SaveCust(object sender, DataGridCommandEventArgs e)
		{
			int iSelIndex = e.Item.ItemIndex;

			string strConsigNo = this.dgPreview.Items[iSelIndex].Cells[1].Text;
			DropDownList ddlSCShipmentUpdate = (DropDownList)this.dgPreview.Items[iSelIndex].FindControl("ddlSCShipmentUpdate");
			//੾�� CASH ��ҹ�鹷�����૿���͡ૹ��������Ш��
			string strSenderName = ddlSCShipmentUpdate.SelectedItem.Value.ToString();

			bool bUpdateOK=false;

			bUpdateOK = InvoiceGenerationMgrDAL.UpdateTempSenderName(utility.GetAppID(),utility.GetEnterpriseID(),ut.GetUserID(),strConsigNo,strSenderName,Session.SessionID.ToString());

			if(bUpdateOK)
				lblErrorMsg.Text =  Utility.GetLanguageText(ResourceType.UserMessage,"MODIFIED_SUCCESSFULLY",utility.GetUserCulture());
			else
				lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"MDY_FAIL",utility.GetUserCulture());


			strInvType = (string)Session["Invoice_Type"];

			BindHeader(strInvType);
			DisplayRecNum();

		}


		protected void dgPreview_CheckChanged(object sender, System.EventArgs e)
		{ 
			int i = 0;
			CheckBox chkSelect =  (CheckBox)sender;
			DataGridItem dgi;
			dgi =(DataGridItem)chkSelect.NamingContainer; 

			i = Convert.ToInt32(chkSelect.Attributes["RowNumber"]);

			string strConsigNo = this.dgPreview.Items[i].Cells[1].Text;
			InvoiceGenerationMgrDAL.UpdateTempCheckedConsignment(utility.GetAppID(),utility.GetEnterpriseID(),ut.GetUserID(),strConsigNo,chkSelect.Checked, Session.SessionID);
		}

		private void dgPreview_CheckBoxBinding(object sender, System.EventArgs e)
		{
			
		}

		private void btnGenerateInv_Click(object sender, System.EventArgs e)
		{
			DataTable dtConsignment = new DataTable();
			DataSet dsConsignment =new DataSet();
			dtConsignment.Columns.Add(new DataColumn("consignment_no", typeof(string)));
			dtConsignment.Columns.Add(new DataColumn("Sender_Name", typeof(string)));	

			//Generate Invoice group by HEADER that consignment has been CHECKED!

			//FIND CHECKED CONSIG BY LOOPING ALL DETAILS

			
			//GET ALL HEADER FOR LOOPING
			DataSet dsHeader  = InvoiceGenerationMgrDAL.GetTempListOfCHECKEDInvoiceHeader(m_strAppID, m_strEnterpriseID,ut.GetUserID(), Session.SessionID);
			if((dsHeader.Tables[0].Rows.Count<=0))
			{
				this.lblErrorMsg.Text="No consignments selected.";
				this.btnGenerateInv.Enabled=true;
				return;
			}

			//GEN INVOICE
			DataSet dsConsignments=new DataSet();
			string selectedCustID = "";
			if (rbCashConsig.Checked == true)
			{
				selectedCustID = this.dbCmbAgentId_Cash.Value;
			}
			else if (rbCreditConsig.Checked == true)
			{
				selectedCustID = this.dbCmbAgentId.Value;
			}

			string strInvJobID = Counter.GetNext(ut.GetAppID(),ut.GetEnterpriseID(),"InvoiceGenerationJobID").ToString();                                           
			InvoiceJobMgrDAL.InsertInvoice_Generation_Task(ut.GetAppID(), ut.GetEnterpriseID(), strInvJobID, DateTime.Now, "P", "Job Awaiting Execution for " + selectedCustID);
			
			this.lblErrorMessage.Text = "Invoice job id: " + strInvJobID + " has been generated." ;
			this.divInvoiceGen.Visible=true;
			this.btnGenerate.Enabled=true;
			this.divInvoiceGenPreview.Visible=false;
			EnableCustomer();

			//InvoiceJobMgrDAL.GenerateInvoices(ut.GetAppID(), ut.GetEnterpriseID(), ut.GetUserID(), Session.SessionID, strInvJobID);
			InvoiceJobMgrDAL objInvJobMgrDAL = new InvoiceJobMgrDAL();
			objInvJobMgrDAL.SyncGenerateInvoice(ut.GetAppID(), ut.GetEnterpriseID(), ut.GetUserID(), Session.SessionID, strInvJobID);

		}

#endregion

		private void ExportToXls(DataSet ds,String FileName)
		{
			DataTable dt = ds.Tables[0];
			String headerXls = "attachment;filename="+FileName+".xls";
			if (dt  != null)
			{
				Response.Clear();
				Response.AddHeader("Content-Disposition", headerXls);
				Response.Charset = "";
				Response.ContentType = "application/vnd.ms-excel";

				//write columns
				string tab = "";
				foreach (DataColumn dc in dt.Columns)
				{
					Response.Write(tab + dc.ColumnName);
					tab = "\t";
				}
				Response.Write("\n");

				//write rows
				int i;
				foreach (DataRow dr in dt.Rows)
				{
					tab = " ";
					for (i = 0; i < dt.Columns.Count; i++)
					{
						Response.Write(tab + dr[i].ToString());
						tab = "\t";
					}
					Response.Write("\n");
				}
				Response.End();
			}
		}


		private void btnExport_Click(object sender, System.EventArgs e)
		{
			string Res = "";
			lblErrorMsg.Text = "";

			dsExport =  (DataSet)ViewState["dsExport"];
			// Remove column
			string[] columnRemove = {"applicationid","enterpriseid","userid","pod_slip_required","checked","payerid"};
			RemoveColumn(columnRemove);
			// Set Header Column Name
			for (int i = 0;i<dsExport.Tables[0].Columns.Count;i++)
			{
				// Column checked not use
				dsExport.Tables[0].Columns[i].ColumnName = dgPreview.Columns[i+1].HeaderText;
			}
			// Set type column
			string[] ColumnType = new string[dsExport.Tables[0].Columns.Count];
			TIESClasses.CreateXls Xls = new TIESClasses.CreateXls();
			ColumnType[4] = Xls.TypeDate;
			ColumnType[7] = Xls.TypeDate;
			ColumnType[9] = Xls.TypeDecimal;
			ColumnType[10] = Xls.TypeDecimal;
			ColumnType[11] = Xls.TypeDecimal;
			ColumnType[12] = Xls.TypeDecimal;
			ColumnType[13] = Xls.TypeDecimal;
			
			Res = Xls.createXls(dsExport,GetPathExport(),ColumnType);
			
			if (Res == "0")
			{	
				GetDownloadFile(Res);
				
				// Kill Process after save
				Xls.KillProcesses("EXCEL");
			} 
			else
			{
				lblErrorMsg.Text = Res;
			}
		}

		private void RemoveColumn(string[] columnRemoveName)
		{
				foreach (string stringColumn in columnRemoveName)
				{
					if (CheckColumnNameIsExist(stringColumn) == true)
							dsExport.Tables[0].Columns.Remove(stringColumn);
				}
		}

		private bool CheckColumnNameIsExist(string stringColumn)
		{
			bool flag = false;

			foreach (DataColumn dc in dsExport.Tables[0].Columns)
			{
				if (stringColumn.ToLower() == dc.ColumnName.ToLower())
				{
					flag = true;
				} 
			}
			return flag;
		}

		private string  GetPathExport()
		{
			return Server.MapPath("") + "\\Export\\InvoiceGenerationDetails.xls";
		}

		private void GetDownloadFile(string Res)
		{
			if (Res == "0")
			{
				Response.ContentType="application/ms-excel";
				Response.AddHeader( "content-disposition","attachment; filename=InvoiceGenerationDetails.xls");
				FileStream fs = new FileStream(GetPathExport(),FileMode.Open);
				long FileSize;
				FileSize = fs.Length;
				byte[] getContent = new byte[(int)FileSize];
				fs.Read(getContent, 0, (int)fs.Length);
				fs.Close();

				Response.BinaryWrite(getContent);
			} 
		}
	}//class

}//namespace
