<%@ Register TagPrefix="DbCombo" Namespace="Cambro.Web.DbCombo" Assembly="Cambro.Web.DbCombo" %>
<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<%@ Import Namespace="Cambro.Web.DbCombo" %>
<%@ Import Namespace="com.common.util" %>
<%@ Page language="c#" Codebehind="CustomerChargeableWT.aspx.cs" AutoEventWireup="false" Inherits="com.ties.CustomerChargeableWT" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>ShipmentTrackingQuery</title>
		<LINK href="css/Styles.css" type="text/css" rel="stylesheet" name="stylesheet">
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<!--#INCLUDE FILE="msFormValidations.inc"-->
		<script language="javascript" src="Scripts/settingScrollPosition.js"></script>
	</HEAD>
	<body onclick="GetScrollPosition();" onload="SetScrollPosition();" onunload="window.top.displayBanner.fnCloseAll(0);"
		MS_POSITIONING="GridLayout">
		<form id="ShipmentTrackingQueryRpt" method="post" runat="server">
			<asp:button id="btnQuery" style="Z-INDEX: 101; LEFT: 20px; POSITION: absolute; TOP: 40px" runat="server"
				Text="Query" CssClass="queryButton" Width="64px"></asp:button><asp:label id="lblErrorMessage" style="Z-INDEX: 105; LEFT: 20px; POSITION: absolute; TOP: 71px"
				runat="server" CssClass="errorMsgColor" Width="700px" Height="17px">Place holder for err msg</asp:label>&nbsp;
			<asp:button id="btnExecuteQuery" style="Z-INDEX: 102; LEFT: 84px; POSITION: absolute; TOP: 40px"
				runat="server" Text="Execute Query" CssClass="queryButton" Width="123px"></asp:button>
			<TABLE id="tblInternal" style="Z-INDEX: 104; LEFT: 14px; WIDTH: 800px; POSITION: absolute; TOP: 84px; HEIGHT: 500px"
				width="800" border="0" runat="server">
				<TR>
					<TD style="WIDTH: 468px; HEIGHT: 164px" vAlign="top">
						<fieldset style="WIDTH: 456px; HEIGHT: 160px"><legend><asp:label id="lblDates" runat="server" CssClass="tableHeadingFieldset" Width="20px" Font-Bold="True">Dates</asp:label></legend>
							<P><FONT face="Tahoma">
									<TABLE class="tableRadioButton" id="Table1" style="WIDTH: 440px; HEIGHT: 24px" cellSpacing="0"
										cellPadding="0" width="440" border="0">
										<TR>
											<TD style="WIDTH: 133px"><asp:radiobutton id="rdactpickupdate" runat="server" Text="Actual Pickup Date" Checked="True" GroupName="SelectDate"></asp:radiobutton></TD>
											<TD></TD>
											<TD></TD>
										</TR>
									</TABLE>
								</FONT><FONT face="Tahoma"></FONT>
							</P>
							<P><FONT face="Tahoma">
									<TABLE class="tableRadioButton" id="Table2" cellSpacing="0" cellPadding="0" width="300"
										border="0">
										<TR>
											<TD><asp:radiobutton id="rbMonth" runat="server" Text="Month" CssClass="tableRadioButton" Width="73px"
													Height="21px" Checked="True" GroupName="EnterDate" AutoPostBack="True"></asp:radiobutton></TD>
											<TD><asp:dropdownlist id="ddMonth" runat="server" CssClass="textField" Width="88px" Height="19px"></asp:dropdownlist></TD>
											<TD><asp:label id="lblYear" runat="server" CssClass="tableLabel" Width="30px" Height="22px">Year</asp:label></TD>
											<TD><cc1:mstextbox id="txtYear" runat="server" CssClass="textField" Width="83" NumberPrecision="4"
													NumberMinValue="1" NumberMaxValue="2999" TextMaskType="msNumeric" MaxLength="5"></cc1:mstextbox></TD>
										</TR>
										<TR>
											<TD style="HEIGHT: 20px"><asp:radiobutton id="rbPeriod" runat="server" Text="Period" CssClass="tableRadioButton" Width="62px"
													GroupName="EnterDate" AutoPostBack="True"></asp:radiobutton></TD>
											<TD style="HEIGHT: 20px"><cc1:mstextbox id="txtPeriod" runat="server" CssClass="textField" Width="88px" TextMaskType="msDate"
													MaxLength="10" TextMaskString="99/99/9999"></cc1:mstextbox></TD>
											<TD style="HEIGHT: 20px"></TD>
											<TD style="HEIGHT: 20px"><cc1:mstextbox id="txtTo" runat="server" CssClass="textField" Width="83" TextMaskType="msDate"
													MaxLength="10" TextMaskString="99/99/9999"></cc1:mstextbox></TD>
										</TR>
										<TR>
											<TD><asp:radiobutton id="rbDate" runat="server" Text="Date" CssClass="tableRadioButton" Width="74px"
													Height="22px" GroupName="EnterDate" AutoPostBack="True"></asp:radiobutton></TD>
											<TD><cc1:mstextbox id="txtDate" runat="server" CssClass="textField" Width="88px" TextMaskType="msDate"
													MaxLength="10" TextMaskString="99/99/9999"></cc1:mstextbox></TD>
											<TD></TD>
											<TD></TD>
										</TR>
									</TABLE>
									<asp:radiobutton id="rdactdelDate" runat="server" Text="Actual Delivery Date" GroupName="SelectDate"
										Visible="False"></asp:radiobutton></FONT></P>
						</fieldset>
					</TD>
					<TD style="HEIGHT: 164px" vAlign="top">
						<fieldset style="WIDTH: 320px; HEIGHT: 160px">
							<P><legend><asp:label id="lblPayerType" runat="server" CssClass="tableHeadingFieldset" Width="79px" Font-Bold="True">Payer Type</asp:label></legend></P>
							<TABLE class="tableRadioButton" id="Table3" cellSpacing="0" cellPadding="0" width="300"
								border="0">
								<TR>
									<TD style="WIDTH: 64px" vAlign="top"><FONT face="Tahoma"><asp:label id="Label4" runat="server" CssClass="tableLabel" Width="88px" Height="22px">Payer Type</asp:label></FONT></TD>
									<TD style="WIDTH: 146px"><FONT face="Tahoma"><asp:listbox id="lsbCustType" runat="server" Width="150px" Height="61px" SelectionMode="Multiple"></asp:listbox></FONT></TD>
									<TD style="WIDTH: 25px"><FONT face="Tahoma"></FONT></TD>
								</TR>
								<TR>
									<TD style="WIDTH: 64px" vAlign="top"><FONT face="Tahoma"><asp:label id="lblPayerCode" runat="server" CssClass="tableLabel" Width="79px" Height="22px">Payer Code</asp:label></FONT></TD>
									<TD style="WIDTH: 146px"><FONT face="Tahoma"><asp:textbox id="txtPayerCode" runat="server" CssClass="textField" Width="148px"></asp:textbox></FONT></TD>
									<TD style="WIDTH: 25px"><FONT face="Tahoma"><asp:button id="btnPayerCode" runat="server" Text="..." CssClass="searchButton" CausesValidation="False"></asp:button></FONT></TD>
								</TR>
							</TABLE>
							<P><FONT face="Tahoma"></FONT>&nbsp;</P>
						</fieldset>
					</TD>
				</TR>
				<TR>
					<TD style="WIDTH: 468px; HEIGHT: 134px" vAlign="top">&nbsp;
					</TD>
					<TD style="HEIGHT: 134px" vAlign="top">&nbsp;
					</TD>
				</TR>
				<TR>
					<TD vAlign="top" colSpan="2">&nbsp;
					</TD>
				</TR>
			</TABLE>
			<asp:label id="Label1" style="Z-INDEX: 103; LEFT: 20px; POSITION: absolute; TOP: 4px" runat="server"
				CssClass="maintitleSize" Width="568px" Height="27px"> Customer Chargeable Weight Details Report</asp:label></TR></TABLE></TR></TABLE><INPUT 
type=hidden value="<%=strScrollPosition%>" name=ScrollPosition>
		</form>
	</body>
</HTML>
