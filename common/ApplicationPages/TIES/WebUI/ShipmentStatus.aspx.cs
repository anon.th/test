using System;
using System.Web.UI.HtmlControls;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.common.classes;
using com.common.DAL;
using com.common.RBAC;
using com.ties.DAL;
using com.common.util;
using com.ties.classes;
using com.common.applicationpages;

namespace TIES.WebUI
{
	/// <summary>
	/// Summary description for ShipmentStatusDelete.
	/// </summary>
	public class ShipmentStatus : BasePopupPage
	{
		private DataSet dsShipment = null;
        private SessionDS m_sdsShipment = null;
		private String strConsgnmentNo;
		private String strRefNo;
		private String m_strAppID;
		private String strPlayerId;
		protected System.Web.UI.WebControls.DataGrid dgShipment;
		private String m_strEnterpriseID;
		protected String strDT;
		protected String strStatus;
		protected String strStatusDSC;
		protected String strExcep;
		protected String strExcepDSC;
		protected String strRemarks;
		protected String strLocation;
		protected System.Web.UI.WebControls.Label lblErrorMsg;
		protected System.Web.UI.WebControls.Label lblBookNo;
		protected System.Web.UI.WebControls.Label lblDisBookNo;
		protected System.Web.UI.WebControls.Label lblConsgNo;
		protected System.Web.UI.WebControls.Label lblDispConsgNo;
		protected System.Web.UI.WebControls.Label Label14;
		protected System.Web.UI.WebControls.Label lblDispRefNo;
		protected System.Web.UI.WebControls.Label lblBookDateTime;
		protected System.Web.UI.WebControls.Label lblDisBookDateTime;
		protected System.Web.UI.WebControls.Label Label1;
		protected System.Web.UI.WebControls.Label lblDisEstDateTime;
		protected System.Web.UI.WebControls.Label lblShipmentType;
		protected System.Web.UI.WebControls.Label lblDispShipmentType;
		protected System.Web.UI.WebControls.Label lblOrigin;
		protected System.Web.UI.WebControls.Label lblDispOrigin;
		protected System.Web.UI.WebControls.Label lblDestination;
		protected System.Web.UI.WebControls.Label lblDispDestination;
		protected System.Web.UI.WebControls.Label lblPayerCode;
		protected System.Web.UI.WebControls.Label lblDispPayercode;
		protected System.Web.UI.WebControls.Label lblPayerName;
		protected System.Web.UI.WebControls.Label lblDispPayerName;
		protected System.Web.UI.HtmlControls.HtmlTable Table2;
		protected System.Web.UI.WebControls.CheckBox chkExpandPackageDetail;
		protected String strPerson;
		

		private void Page_Load(object sender, System.EventArgs e)
		{
//			// Put user code to initialize the page here
//			//Utility utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings,Page.Session);
			m_strAppID = utility.GetAppID();
			m_strEnterpriseID = utility.GetEnterpriseID();
			lblErrorMsg.Text="";

			
			strDT = Utility.GetLanguageText(ResourceType.ScreenLabel,"Status Date/Time",utility.GetUserCulture());
			strStatus = Utility.GetLanguageText(ResourceType.ScreenLabel,"Status Code",utility.GetUserCulture());
			strStatusDSC = Utility.GetLanguageText(ResourceType.ScreenLabel,"Status Description",utility.GetUserCulture());
			strExcep = Utility.GetLanguageText(ResourceType.ScreenLabel,"Exception Code",utility.GetUserCulture());
			strExcepDSC = Utility.GetLanguageText(ResourceType.ScreenLabel,"Exception Description",utility.GetUserCulture());
			strRemarks = Utility.GetLanguageText(ResourceType.ScreenLabel,"Remarks",utility.GetUserCulture());
			strLocation = Utility.GetLanguageText(ResourceType.ScreenLabel,"Location",utility.GetUserCulture());
			strPerson = Utility.GetLanguageText(ResourceType.ScreenLabel,"Person Responsible",utility.GetUserCulture());

			if(!Page.IsPostBack)
			{
				if (Request.Params["Type"] == "CONSGNO")
				{
					if (Request.Params["Number"] != "")
					{
						strConsgnmentNo = TrackAndTraceDAL.CovertAsciiToSharp(Request.Params["Number"].ToString());
					}
				}
				else if(Request.Params["Type"] == "CUSTREF")
				{
					if (Request.Params["Number"] != "")
					{
						//strConsgnmentNo = TrackAndTraceDAL.GetTopConsgnByRefNo(m_strAppID,m_strEnterpriseID,Request.Params["Number"].ToString());
						strRefNo = TrackAndTraceDAL.CovertAsciiToSharp(Request.Params["Number"].ToString());
					}
				}			

				// Thosapol Yennam 19/08/2013 Don't show message.
//				if (Request.Params["ERR"] == "T")
//				{
//					lblErrorMsg.Text = "Reference number is not unique for customer - latest pickup information displayed.";
//
//				}
//				else
//				{
//					lblErrorMsg.Text="";
//
//				}

//				if (Request.Params["PlayerId"] != "")
//				{
//					strPlayerId =  Request.Params["PlayerId"].ToString();
//				}
				strPlayerId =  Request.Params["PlayerId"].ToString();

				string flgExpandPackageDetail = TrackAndTraceDAL.QueryEnterpriseConfigurationsTrackandTrace(m_strAppID,m_strEnterpriseID);
				// Thosapol Yennam 15/08/2013  [Comment]
				//dsShipment = TrackAndTraceDAL.QueryShipmentTracking(m_strAppID,m_strEnterpriseID,strConsgnmentNo);
				//displayInfo();
				//BindShipment();
				// Thosapol Yennam 15/08/2013 [New Code]
				dsShipment = TrackAndTraceDAL.QueryTrackandTrace(m_strAppID,m_strEnterpriseID,strConsgnmentNo,strRefNo,strPlayerId,flgExpandPackageDetail);
				DisplayHeader();
				BindShipmentDetail();
				DisplayHeaderCheckbox();
			}
			
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			base.OnInit(e);
			InitializeComponent();
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.chkExpandPackageDetail.CheckedChanged += new System.EventHandler(this.chkExpandPackageDetail_CheckedChanged);
			this.dgShipment.SelectedIndexChanged += new System.EventHandler(this.dgShipment_SelectedIndexChanged);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private void displayInfo()
		{
				try
				{
					DataRow dr = dsShipment.Tables[0].Rows[0];		
					
					lblDisBookNo.Text = dr["booking_no"].ToString();
					lblDispConsgNo.Text = dr["consignment_no"].ToString();
					if(dr["booking_datetime"]!=System.DBNull.Value && dr["booking_datetime"].ToString()!="")
					{
						DateTime dtBookDateTime = (DateTime)dr["booking_datetime"];
						//lblDisBookDateTime.Text = dr["booking_datetime"].ToString();
						lblDisBookDateTime.Text = dtBookDateTime.ToString("dd/MM/yyyy HH:mm");

					}
					if(dr["est_delivery_datetime"]!=System.DBNull.Value && dr["est_delivery_datetime"].ToString()!="")
					{
						DateTime dtEstDateTime = (DateTime)dr["est_delivery_datetime"];
						//lblDisBookDateTime.Text = dr["booking_datetime"].ToString();
						lblDisEstDateTime.Text = dtEstDateTime.ToString("dd/MM/yyyy HH:mm");

					}
					lblDispRefNo.Text = dr["ref_no"].ToString();
					lblDispPayercode.Text = dr["payerid"].ToString();
					lblDispShipmentType.Text = dr["service_code"].ToString();
					lblDispOrigin.Text = dr["sen_state_code"].ToString();
					lblDispDestination.Text = dr["rec_state_code"].ToString();
					lblDispPayerName.Text = dr["payer_name"].ToString();
				}
				catch(Exception ex)
				{
					Logger.LogTraceError("ShipmentStatus","Error01","displayInfo",ex.Message);
				}
		}
	
		private void BindShipment()
		{
			dgShipment.DataSource = dsShipment;
			dgShipment.DataBind();
		}


		private void DisplayHeader()
		{
			try
			{
				DataRow dr = dsShipment.Tables[0].Rows[0];

				lblDisBookNo.Text = dr["booking_no"].ToString();
				lblDispConsgNo.Text = dr["consignment_no"].ToString();
				if(dr["booking_datetime"]!=System.DBNull.Value && dr["booking_datetime"].ToString()!="")
				{
					DateTime dtBookDateTime = (DateTime)dr["booking_datetime"];
					//lblDisBookDateTime.Text = dr["booking_datetime"].ToString();
					lblDisBookDateTime.Text = dtBookDateTime.ToString("dd/MM/yyyy HH:mm");
				}
				if(dr["est_delivery_datetime"]!=System.DBNull.Value && dr["est_delivery_datetime"].ToString()!="")
				{
					DateTime dtEstDateTime = (DateTime)dr["est_delivery_datetime"];
					//lblDisBookDateTime.Text = dr["booking_datetime"].ToString();
					lblDisEstDateTime.Text = dtEstDateTime.ToString("dd/MM/yyyy HH:mm");

				}
				lblDispRefNo.Text = dr["ref_no"].ToString();
				lblDispPayercode.Text = dr["payerid"].ToString();
				lblDispShipmentType.Text = dr["service_code"].ToString();
				lblDispOrigin.Text = dr["origin_state_code"].ToString();
				lblDispDestination.Text = dr["destination_state_code"].ToString();
				lblDispPayerName.Text = dr["payer_name"].ToString();

			}
			catch(Exception ex)
			{
				Logger.LogTraceError("ShipmentStatus","Error01","displayInfo",ex.Message);
			}
		}

		private void BindShipmentDetail()
		{
			dgShipment.DataSource = dsShipment.Tables[1];
			dgShipment.DataBind();
		}
 
		private void DisplayHeaderCheckbox()
		{
			try
			{
				string flg = TrackAndTraceDAL.QueryEnterpriseConfigurationsTrackandTrace(m_strAppID,m_strEnterpriseID);
				if (flg == "1")
				{
					chkExpandPackageDetail.Checked = true;
				}
				else if(flg == "0")
				{
					chkExpandPackageDetail.Checked = false;
				}
				
			}
			catch(Exception ex)
			{
				Logger.LogTraceError("ShipmentStatus","Error01","displayInfo",ex.Message);
			}
		}


		protected void OnDataBound_Shipment(object sender, DataGridItemEventArgs e)
		{

			if(e.Item.ItemIndex == -1)
			{
				return;
			}
			
			DataRow dr = dsShipment.Tables[1].Rows[e.Item.ItemIndex];

			Label lblStatusCode = (Label)e.Item.FindControl("lblStatusCode");
			String strStatusCode = dr["status_code"].ToString();
			Label lblStatusDesc = (Label)e.Item.FindControl("lblStatusDesc");			
			Label lblConsgnee = (Label)e.Item.FindControl("lblConsgnee");
			// Thosapol [Temp : 15/08/2013]
			//String strConsgnee = dr["consignee_name"].ToString();
			String strConsgnee = "AAAA";
			if(strStatusCode.Trim() == "")
			{
				lblStatusCode.Visible = false;
				lblStatusDesc.Visible = false;
			}
			else
			{
				if(strStatusCode == "POD" && (strConsgnee.Trim() != "" && strConsgnee != null) )
				{
					lblConsgnee.Visible = true;
				}
				else
				{
					lblConsgnee.Visible = false;
				}
				lblStatusCode.Visible = true;
				lblStatusDesc.Visible = true;
			}

			Label lblExcepCode = (Label)e.Item.FindControl("lblExcepCode");
			Label lblExcepDesc = (Label)e.Item.FindControl("lblExcepDesc");
			String strExcepCode = dr["exception_code"].ToString();
			if(strExcepCode.Trim() == "")
			{
				lblExcepCode.Visible = false;
				lblExcepDesc.Visible = false;
			}
			else
			{
				lblExcepCode.Visible = true;
				lblExcepDesc.Visible = true;
			}

			Label lblRemk = (Label)e.Item.FindControl("lblRemk");
			String strRemk = dr["remarks"].ToString();
			if(strRemk.Trim() == "")
			{
				lblRemk.Visible = false;
			}
			else
			{
				lblRemk.Visible = true;
			}

            Label lblLocation = (Label)e.Item.FindControl("lblLocation");
			String strLoc = dr["location"].ToString();
			if(strLoc.Trim() == "")
			{
				lblLocation.Visible = false;
			}
			else
			{
				lblLocation.Visible = true;
			}

		}

		private void OpenWindowpage(String strUrl)
		{
			ArrayList paramList = new ArrayList();
			paramList.Add(strUrl);
			String sScript = Utility.GetScript("openLastChildWindow.js",paramList);
			Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
		}

		private void btnClose_Click(object sender, System.EventArgs e)
		{
			String sScript = "";
			sScript += "<script language=javascript>";
			sScript += "  window.opener;" ;
			sScript += "  window.close();";
			sScript += "</script>";
			Response.Write(sScript);
		}

		private void dgShipment_SelectedIndexChanged(object sender, System.EventArgs e)
		{
		
		}
		private void RefreshQueryPage()
		{
			String sScript = "";
			sScript += "<script language=javascript>";
			sScript += "var sURL = unescape(window.opener.location.pathname); ";
			sScript += "  window.opener.location.replace(sURL);" ;
			//sScript += "  window.close();";
			sScript += "</script>";
			Response.Write(sScript);
		}
	
		private void chkExpandPackageDetail_CheckedChanged(object sender, System.EventArgs e)
		{
			string flg = TrackAndTraceDAL.QueryEnterpriseConfigurationsTrackandTrace(m_strAppID,m_strEnterpriseID);
			if(chkExpandPackageDetail.Checked == true)
			{
				flg =	"1"	;
			} 
			else
			{
				flg =	"0"	;
			}

			if (Request.Params["Type"] == "CONSGNO")
			{
				if (Request.Params["Number"] != "")
				{
					strConsgnmentNo = Request.Params["Number"].ToString();
				}
			}
			else if(Request.Params["Type"] == "CUSTREF")
			{
				if (Request.Params["Number"] != "")
				{
					//strConsgnmentNo = TrackAndTraceDAL.GetTopConsgnByRefNo(m_strAppID,m_strEnterpriseID,Request.Params["Number"].ToString());
					strRefNo = Request.Params["Number"].ToString();
				}
			}			


			if (Request.Params["ERR"] == "T")
			{
				lblErrorMsg.Text = "Reference number is not unique for customer - latest pickup information displayed.";

			}
			else
			{
				lblErrorMsg.Text="";

			}

//			if (Request.Params["PlayerId"] != "")
//			{
//				strPlayerId =  Request.Params["PlayerId"].ToString();
//			}
			strPlayerId =  Request.Params["PlayerId"].ToString();

			strPlayerId = TrackAndTraceDAL.QueryPlayerIdTrackandTrace(m_strAppID,m_strEnterpriseID,utility.GetUserID());
			dsShipment = TrackAndTraceDAL.QueryTrackandTrace(m_strAppID,m_strEnterpriseID,strConsgnmentNo,strRefNo,strPlayerId,flg);
			DisplayHeader();
			BindShipmentDetail();
		}

		

	}
}
