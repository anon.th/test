using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Text;
using com.common.DAL;  
using com.ties.DAL;
using com.common.util;
using com.common.classes;
using com.ties.classes; 
using com.common.applicationpages;
using TIES;


namespace com.ties
{
	/// <summary>
	/// Summary description for AgentZipCodePopup.
	/// </summary>
	public class AgentZipCodePopup : BasePopupPage
	{
		protected System.Web.UI.WebControls.Label lblTitle;
		protected System.Web.UI.WebControls.Label lblCountry;
		protected System.Web.UI.WebControls.Label lblStateCode;
		protected System.Web.UI.WebControls.Label lblZipCode;
		protected System.Web.UI.WebControls.DataGrid dgZipcode;
		protected System.Web.UI.WebControls.Label lblESA;
		protected System.Web.UI.WebControls.Button btnOK;
		protected com.common.util.msTextBox txtEAS;
		protected com.common.util.msTextBox txtCountry;
		protected System.Web.UI.WebControls.Button BtnCountry;
		protected System.Web.UI.WebControls.Button btnStateCode;
		protected System.Web.UI.WebControls.Button btnZipCode;
		protected com.common.util.msTextBox txtZipCode;
		protected System.Web.UI.WebControls.Button btnSearch;
		protected System.Web.UI.WebControls.Label lblErrorMsg;
		protected System.Web.UI.WebControls.Button Button1;
		protected com.common.util.msTextBox txtStateName;

		private String strAppID = "";
		private String strEnterpriseID = "";
		private DataSet m_sdsZipcode;
		private void Page_Load(object sender, System.EventArgs e)
		{
			strAppID		=utility.GetAppID();
			strEnterpriseID	=utility.GetEnterpriseID();
			if(!IsPostBack)
			{
				m_sdsZipcode = GetEmptyZipcodeDS(0);
				BindGrid();
				ViewState["tmpChkAll"] = false;
			}
			else
			{
				m_sdsZipcode = (DataSet)Session["ZIPCODE_DS"];
			}
			String strFormID	= Request.Params["FORMID"];	
			if(strFormID !="AgentProfile")
			{
				lblESA.Visible=false;
				txtEAS.Visible=false;
			}	
			else
			{
				lblESA.Visible=true;
				txtEAS.Visible=true;
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			base.OnInit(e);
			InitializeComponent();
			
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Button1.Click += new System.EventHandler(this.Button1_Click);
			this.BtnCountry.Click += new System.EventHandler(this.BtnCountry_Click);
			this.btnStateCode.Click += new System.EventHandler(this.btnStateCode_Click);
			this.btnZipCode.Click += new System.EventHandler(this.btnZipCode_Click);
			this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
			this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
			this.dgZipcode.ItemCreated += new System.Web.UI.WebControls.DataGridItemEventHandler(this.dgZipcode_ItemCreated);
			this.dgZipcode.PageIndexChanged += new System.Web.UI.WebControls.DataGridPageChangedEventHandler(this.dgZipcode_PageIndexChanged);
			this.dgZipcode.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.dgZipcode_ItemDataBound);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
	
		public static DataSet GetEmptyZipcodeDS(int iNumRows)
		{
			

			DataTable dtZipcode = new DataTable();
 
			dtZipcode.Columns.Add(new DataColumn("zipcode", typeof(string)));
			dtZipcode.Columns.Add(new DataColumn("state_code", typeof(string)));
			dtZipcode.Columns.Add(new DataColumn("state_name", typeof(string)));
			dtZipcode.Columns.Add(new DataColumn("country", typeof(string)));
			dtZipcode.Columns.Add(new DataColumn("IsChecked", typeof(bool)));
			for(int i=0; i < iNumRows; i++)
			{
				DataRow drEach = dtZipcode.NewRow();
				drEach[0] = "";
				drEach[1] = "";
				drEach[2] = "";
				drEach[3] = "";
				drEach[4] = false;

				dtZipcode.Rows.Add(drEach);
			}

			DataSet dsZipcode = new DataSet();
			dsZipcode.Tables.Add(dtZipcode);

			return  dsZipcode;
	
		}


		private DataSet GetZipcodeDS(String strQuery)
		{

			IDbCommand dbCmd = null;
			DataSet tmpDs;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("AgentZipCodePopup","GetZipcodeDS","Error 01","DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null",null);

			}

			dbCmd = dbCon.CreateCommand(strQuery,CommandType.Text);
			try
			{
				tmpDs = (DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
			}
			catch(System.ApplicationException appException)
			{
				Logger.LogTraceError("AgentZipCodePopup","GetZipcodeDS","","Error 02");
				throw appException;
			}
	
			return  tmpDs;
			
		}


		private void BindGrid()
		{
			try
			{
				dgZipcode.DataSource = m_sdsZipcode;
				dgZipcode.DataBind();
				Session["ZIPCODE_DS"] = m_sdsZipcode;
			}
			catch(Exception ex){
				ex = ex;
			}
		}


		private void ShowRecords()
		{
			String strZipCode	= txtZipCode.Text;
			String strStateName = txtStateName.Text;
			String strCountry	= txtCountry.Text;
			StringBuilder strQry = new StringBuilder();
			String strFormID	= Request.Params["FORMID"];	
			String strAgentID		= Request.Params["AGENTID"];
			String strZoneCode		= Request.Params["ZONECODE"];
			String strVASCode		= Request.Params["VASCODE"];
			String strServiceCode	= Request.Params["SERVICECODE"];
			String strTableName		= "Agent_Zone_zipcode";
			String strColumnName	= "zone_code";
			String strColumnVal		= strZoneCode;
			if(strFormID =="AgentVAS")
			{
				strTableName	= "agent_vas_excluded";
				strColumnName	= "vas_code";
				strColumnVal	= strVASCode;
			}
			if(strFormID =="AgentService")
			{
				strTableName="agent_service_excluded";
				strColumnName="service_code";
				strColumnVal	= strServiceCode;
			}
			if(strFormID =="VASZipcodeExcluded")
			{
				strTableName="zipcode_vas_excluded";
				strColumnName="vas_code";
				strColumnVal	= strVASCode;
			}
			if(strFormID =="ServiceZipcodeExcluded")
			{
				strTableName="zipcode_service_excluded";
				strColumnName="service_code";
				strColumnVal	= strServiceCode;
			}
			
			strQry.Append("SELECT  z.zipcode AS zipcode, s.state_code AS state_code, s.state_name AS state_name, z.country AS country, z.esa_surcharge AS esa_surcharge FROM ");
			strQry.Append(" Zipcode z INNER JOIN  State s  ON  z.applicationid = s.applicationid AND   z.enterpriseid = s.enterpriseid AND   z.country = s.country AND z.state_code = s.state_code ");
			strQry.Append(" AND z.applicationid = '");
			strQry.Append(strAppID);
			strQry.Append("' AND z.enterpriseid = '");
			strQry.Append(strEnterpriseID);
			strQry.Append("' ");
			strQry.Append("where not exists ( Select zipcode from "+strTableName+" AZZ where z.zipcode= AZZ.zipcode   and AZZ."+strColumnName+"='"+strColumnVal+"'  AND AZZ.applicationid = z.applicationid AND AZZ.enterpriseid = z.enterpriseid)");
			
			if(strZipCode != null && strZipCode != "")
			{
				strQry.Append(" and z.zipcode like '%");
				strQry.Append(Utility.ReplaceSingleQuote(strZipCode));
				strQry.Append("%' ");
			}
			if(strCountry != null && strCountry != "")
			{
				strQry.Append(" and z.country like '%");
				strQry.Append(Utility.ReplaceSingleQuote(strCountry));
				strQry.Append("%' ");
			}
			if(strStateName != null && strStateName != "")
			{
				strQry.Append(" and s.state_name like N'%");
				strQry.Append(Utility.ReplaceSingleQuote(strStateName));
				strQry.Append("%' ");
			}
			String strSQLQuery = strQry.ToString();
			m_sdsZipcode = GetZipcodeDS(strSQLQuery);
			m_sdsZipcode.Tables[0].Columns.Add(new DataColumn("IsChecked", typeof(bool)));
			foreach(DataRow dr in m_sdsZipcode.Tables[0].Rows)
			{
				dr["IsChecked"] = false;
				m_sdsZipcode.Tables[0].AcceptChanges();
			}

			BindGrid();
		}


		private void BtnCountry_Click(object sender, System.EventArgs e)
		{
			string strtxtCustId=null;
			strtxtCustId =txtCountry.Text;
			String sUrl = "ZipcodePopup.aspx?FORMID=AgentZipCodePopup&COUNTRY_TEXT="+strtxtCustId+"&ZIPCODE_CID="+txtZipCode.ClientID+"&STATE_CID="+txtStateName.ClientID+"&COUNTRY_CID="+txtCountry.ClientID;
			ArrayList paramList = new ArrayList();
			paramList.Add(sUrl);
			String sScript = Utility.GetScript("PkgDetails.js",paramList);
			Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
		}


		private void btnStateCode_Click(object sender, System.EventArgs e)
		{
			string strtxtCustId=null;
			strtxtCustId =txtStateName.Text;
			String sUrl = "ZipcodePopup.aspx?FORMID=AgentZipCodePopup&STATECODE="+strtxtCustId+"&ZIPCODE_CID="+txtZipCode.ClientID+"&STATE_CID="+txtStateName.ClientID+"&COUNTRY_CID="+txtCountry.ClientID;
			ArrayList paramList = new ArrayList();
			paramList.Add(sUrl);
			String sScript = Utility.GetScript("PkgDetails.js",paramList);
			Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);		
		}


		private void btnZipCode_Click(object sender, System.EventArgs e)
		{
			string strtxtCustId=null;
			strtxtCustId =txtZipCode.Text;
			String sUrl = "ZipcodePopup.aspx?FORMID=AgentZipCodePopup&ZIPCODE="+strtxtCustId+"&ZIPCODE_CID="+txtZipCode.ClientID+"&STATE_CID="+txtStateName.ClientID+"&COUNTRY_CID="+txtCountry.ClientID;
			ArrayList paramList = new ArrayList();
			paramList.Add(sUrl);
			String sScript = Utility.GetScript("js.PkgDetails",paramList);
			Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
		}


		private void btnSearch_Click(object sender, System.EventArgs e)
		{
			ShowRecords();
		}


		private void btnOK_Click(object sender, System.EventArgs e)
		{
			String strFormID		= Request.Params["FORMID"];		
			String strServiceCode	= Request.Params["SERVICECODE"];
			String strAgentID		= Request.Params["AGENTID"];
			String strZoneCode		= Request.Params["ZONECODE"];
			String strVASCode		= Request.Params["VASCODE"];
			ArrayList queryList = new ArrayList();
			int iRowsAffected = 0;
			decimal strVAS=0;
			
			if(strFormID =="AgentProfile")
			{
				//updating database table name-Agent_Zone_ZipCode
				
				if(m_sdsZipcode.Tables[0].Rows.Count>0)	
				{
					for(int i=0; i<m_sdsZipcode.Tables[0].Rows.Count;i++)
					{	
						if(m_sdsZipcode.Tables[0].Rows[i]["IsChecked"] != System.DBNull.Value)
						{
							bool state = (bool)m_sdsZipcode.Tables[0].Rows[i]["IsChecked"];

							if (state)
							{
								StringBuilder strBuilder = new StringBuilder();
								strBuilder.Append("insert into agent_zone_zipcode(applicationid,enterpriseid,agentid,zone_code,zipcode,country,state_code,esa_surcharge) ");
								strBuilder.Append(" values('"+strAppID);
								strBuilder.Append("','");
								strBuilder.Append(strEnterpriseID);
								strBuilder.Append("','");
								strBuilder.Append(strAgentID);
								strBuilder.Append("','");
								strBuilder.Append(strZoneCode);
								strBuilder.Append("','");
								strBuilder.Append(m_sdsZipcode.Tables[0].Rows[i]["zipcode"]);
								strBuilder.Append("','");
								strBuilder.Append(m_sdsZipcode.Tables[0].Rows[i]["country"]);
								strBuilder.Append("','");
								strBuilder.Append(m_sdsZipcode.Tables[0].Rows[i]["state_code"]);
								strBuilder.Append("',");
								strBuilder.Append(strVAS);
								strBuilder.Append(")");	
								queryList.Add(strBuilder.ToString());
							}
						}
					}
				}
			}
			else if(strFormID =="AgentVAS")
			{
				//updating database table name-Agent_VAS_Excluded
				if(m_sdsZipcode.Tables[0].Rows.Count>0)	
				{
					for(int i=0; i<m_sdsZipcode.Tables[0].Rows.Count;i++)
					{
						if(m_sdsZipcode.Tables[0].Rows[i]["IsChecked"] != System.DBNull.Value)
						{
							bool state = (bool)m_sdsZipcode.Tables[0].Rows[i]["IsChecked"];

							if (state)
							{
								StringBuilder strBuilder = new StringBuilder();
								strBuilder.Append("insert into agent_vas_excluded(applicationid,enterpriseid,agentid,vas_code,zipcode,country,state_code) ");
								strBuilder.Append(" values('"+strAppID);
								strBuilder.Append("','");
								strBuilder.Append(strEnterpriseID);
								strBuilder.Append("','");
								strBuilder.Append(strAgentID);
								strBuilder.Append("','");
								strBuilder.Append(strVASCode);
								strBuilder.Append("','");
								strBuilder.Append(m_sdsZipcode.Tables[0].Rows[i]["zipcode"]);
								strBuilder.Append("','");
								strBuilder.Append(m_sdsZipcode.Tables[0].Rows[i]["country"]);
								strBuilder.Append("','");
								strBuilder.Append(m_sdsZipcode.Tables[0].Rows[i]["state_code"]);
								strBuilder.Append("')");								
								queryList.Add(strBuilder.ToString());
							}
						}
					}
				}			
			}
			else if(strFormID =="AgentService")
			{
				//updating database table name-Agent_VAS_Excluded
				if(m_sdsZipcode.Tables[0].Rows.Count>0)	
				{
					for(int i=0; i<m_sdsZipcode.Tables[0].Rows.Count;i++)
					{
						if(m_sdsZipcode.Tables[0].Rows[i]["IsChecked"] != System.DBNull.Value)
						{
							bool state = (bool)m_sdsZipcode.Tables[0].Rows[i]["IsChecked"];

							if (state)
							{
								StringBuilder strBuilder = new StringBuilder();
								strBuilder.Append("insert into agent_service_excluded(applicationid,enterpriseid,agentid,service_code,zipcode,country,state_code) ");
								strBuilder.Append(" values('"+strAppID);
								strBuilder.Append("','");
								strBuilder.Append(strEnterpriseID);
								strBuilder.Append("','");
								strBuilder.Append(strAgentID);
								strBuilder.Append("','");
								strBuilder.Append(strServiceCode);
								strBuilder.Append("','");
								strBuilder.Append(m_sdsZipcode.Tables[0].Rows[i]["zipcode"]);
								strBuilder.Append("','");
								strBuilder.Append(m_sdsZipcode.Tables[0].Rows[i]["country"]);
								strBuilder.Append("','");
								strBuilder.Append(m_sdsZipcode.Tables[0].Rows[i]["state_code"]);
								strBuilder.Append("')");								
								queryList.Add(strBuilder.ToString());
							}	
						}
					}
				}			
			}
			else if(strFormID =="VASZipcodeExcluded")
			{
				//updating database table name-zipcode_VAS_Excluded
				if(m_sdsZipcode.Tables[0].Rows.Count>0)	
				{
					for(int i=0; i<m_sdsZipcode.Tables[0].Rows.Count;i++)
					{
						if(m_sdsZipcode.Tables[0].Rows[i]["IsChecked"] != System.DBNull.Value)
						{
							bool state = (bool)m_sdsZipcode.Tables[0].Rows[i]["IsChecked"];

							if (state)
							{
								StringBuilder strBuilder = new StringBuilder();
								strBuilder.Append("insert into zipcode_vas_excluded(applicationid,enterpriseid,vas_code,zipcode,country,state_code) ");
								strBuilder.Append(" values('"+strAppID);
								strBuilder.Append("','");
								strBuilder.Append(strEnterpriseID);
								strBuilder.Append("','");
								strBuilder.Append(strVASCode);
								strBuilder.Append("','");
								strBuilder.Append(m_sdsZipcode.Tables[0].Rows[i]["zipcode"]);
								strBuilder.Append("','");
								strBuilder.Append(m_sdsZipcode.Tables[0].Rows[i]["country"]);
								strBuilder.Append("','");
								strBuilder.Append(m_sdsZipcode.Tables[0].Rows[i]["state_code"]);
								strBuilder.Append("')");								
								queryList.Add(strBuilder.ToString());
							}
						}
					}
				}			
			}
			else if(strFormID =="ServiceZipcodeExcluded")
			{
				//updating database table name-zipcode_service_Excluded
				if(m_sdsZipcode.Tables[0].Rows.Count>0)	
				{
					for(int i=0; i<m_sdsZipcode.Tables[0].Rows.Count;i++)
					{
						if(m_sdsZipcode.Tables[0].Rows[i]["IsChecked"] != System.DBNull.Value)
						{
							bool state = (bool)m_sdsZipcode.Tables[0].Rows[i]["IsChecked"];

							if (state)
							{
								StringBuilder strBuilder = new StringBuilder();
								strBuilder.Append("insert into zipcode_service_excluded(applicationid,enterpriseid,service_code,zipcode,country,state_code) ");
								strBuilder.Append(" values('"+strAppID);
								strBuilder.Append("','");
								strBuilder.Append(strEnterpriseID);
								strBuilder.Append("','");
								strBuilder.Append(strServiceCode);
								strBuilder.Append("','");
								strBuilder.Append(m_sdsZipcode.Tables[0].Rows[i]["zipcode"]);
								strBuilder.Append("','");
								strBuilder.Append(m_sdsZipcode.Tables[0].Rows[i]["country"]);
								strBuilder.Append("','");
								strBuilder.Append(m_sdsZipcode.Tables[0].Rows[i]["state_code"]);
								strBuilder.Append("')");								
								queryList.Add(strBuilder.ToString());
							}
						}
					}
				}			
			}				
			
			if (queryList.Count > 0)
			{
				DbConnection dbConInv = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);
				if(dbConInv == null)
				{
					Logger.LogTraceError("InvoiceJobMgrBAL","CalculateCharges","EDB101","DbConnection object is null!!");
				}
				try
				{
					iRowsAffected = dbConInv.ExecuteBatchSQLinTranscation(queryList);
					lblErrorMsg.Text=iRowsAffected.ToString();
					if(strFormID =="AgentProfile")
					{
						Session["ZoneZipCode"]="submit";
					}
					else if(strFormID =="AgentVAS")
					{
						Session["VASZipCode"]="submit";
					}
					else if(strFormID =="AgentService")
					{
						Session["ServiceZipCode"]="submit";
					}
					else if(strFormID =="VASZipcodeExcluded")
					{
						Session["VASZipCode"]="submit";
					}
					else if(strFormID =="ServiceZipcodeExcluded")
					{
						Session["ServiceZipCode"]="submit";
					}						
					String sScript = "";
					sScript += "<script language=javascript>";
					sScript += "  window.opener."+strFormID+".submit();";
					sScript += "  window.close();";
					sScript += "</script>";
					Response.Write(sScript);
				
				}
				catch(ApplicationException appException)
				{
					throw new ApplicationException(appException.Message,appException);						
				}
			}
		}


		private void Button1_Click(object sender, System.EventArgs e)
		{
			String sScript = "";
			sScript += "<script language=javascript>";
			sScript += "  window.close();";
			sScript += "</script>";
			Response.Write(sScript);		
		}


		public void chkAll(object sender, System.EventArgs e)
		{
			DataSet ServiceStatus = (DataSet)m_sdsZipcode;
			CheckBox pchkAll = (CheckBox)sender;


			for(int i = 0; i<= ServiceStatus.Tables[0].Rows.Count - 1; i++)
			{
				if (pchkAll.Checked) 
				{
					ServiceStatus.Tables[0].Rows[i]["IsChecked"] = true;
					ViewState["tmpChkAll"] = true;
				}
				else 
				{
					ServiceStatus.Tables[0].Rows[i]["IsChecked"] = false;
					ViewState["tmpChkAll"] = false;
				}
			}

			for(int i = 0; i <= dgZipcode.Items.Count - 1; i++)
			{ 
				CheckBox chkFormId = (CheckBox)dgZipcode.Items[i].FindControl("chkFormId");

				if (pchkAll.Checked) 
					chkFormId.Checked = true;
				else 
					chkFormId.Checked = false;
			}

			m_sdsZipcode = ServiceStatus;
			Session["ZIPCODE_DS"] = m_sdsZipcode;
		}


		public void chkRow(object sender, System.EventArgs e)
		{
			DataSet ServiceStatus = (DataSet)m_sdsZipcode;
			CheckBox chkFormId = (CheckBox)sender;

			if (chkFormId.Checked) 
				ServiceStatus.Tables[0].Select("zipcode = " + chkFormId.ToolTip)[0]["IsChecked"] = true;
			else
				ServiceStatus.Tables[0].Select("zipcode = " + chkFormId.ToolTip)[0]["IsChecked"] = false;

			m_sdsZipcode = ServiceStatus;
			Session["ZIPCODE_DS"] = m_sdsZipcode;
		}


		private void dgZipcode_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
		{
			CheckBox chkFormId = (CheckBox)e.Item.FindControl("chkFormId");

			if(chkFormId != null)
			{
				if(DataBinder.Eval(e.Item.DataItem, "zipcode") != System.DBNull.Value)
				{
					string str = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "zipcode"));
					chkFormId.ToolTip = str; 
				}

				if(DataBinder.Eval(e.Item.DataItem, "IsChecked") != System.DBNull.Value)
				{
					bool state = (bool)DataBinder.Eval(e.Item.DataItem, "IsChecked");
					if (state)
						chkFormId.Checked = true;
					else
						chkFormId.Checked = false;
				}
			}
		}


		private void dgZipcode_PageIndexChanged(object source, System.Web.UI.WebControls.DataGridPageChangedEventArgs e)
		{
			dgZipcode.CurrentPageIndex = e.NewPageIndex;
			BindGrid();
		}


		private void dgZipcode_ItemCreated(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
		{
			if(e.Item.ItemType == ListItemType.Header)
			{
				if (ViewState["tmpChkAll"] == null)
					ViewState["tmpChkAll"] = false;

				bool state = (bool)ViewState["tmpChkAll"];
				
				CheckBox tmpChkAll = (CheckBox)e.Item.Cells[0].FindControl("chkFormHeader");

				if(state)
					tmpChkAll.Checked = true;
				else
					tmpChkAll.Checked = false;
			}
		}
		
	}
}
