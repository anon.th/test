using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.common.classes;
using com.common.util;
using com.ties.DAL;
using com.common.applicationpages;
using Cambro.Web.DbCombo;
using com.common.RBAC;
using System.Text ;
using com.common.DAL;
using GenCode128;
using System.Data.SqlClient ;

namespace com.ties
{
	/// <summary>
	/// Summary description for PrePrintConsignmentNote
	/// </summary>
	public class PrePrintConsignmentNote : BasePage 
	{
		protected System.Web.UI.WebControls.Button Add;
		protected System.Web.UI.WebControls.Button Edit;
		protected System.Web.UI.WebControls.Button Save;
		protected System.Web.UI.WebControls.Button Delete;
		protected System.Web.UI.WebControls.Button Print;
		protected System.Web.UI.WebControls.Label Label3;
		protected System.Web.UI.WebControls.Label Label4;
		protected System.Web.UI.WebControls.Label Label10;
		protected System.Web.UI.WebControls.Label Label11;
		protected System.Web.UI.WebControls.Label Label6;
		protected System.Web.UI.WebControls.Label lbCustZone;
		protected System.Web.UI.WebControls.Label Label14;
		protected System.Web.UI.WebControls.Label Label16;
		protected System.Web.UI.WebControls.Label Label20;
		protected System.Web.UI.WebControls.Label Label21;
		protected System.Web.UI.WebControls.Button btsearch;
		protected System.Web.UI.WebControls.Label Label5;
		protected System.Web.UI.WebControls.Label Label15;
		protected System.Web.UI.WebControls.Label Label22;
		protected System.Web.UI.WebControls.Label Label24;
		protected System.Web.UI.WebControls.Label Label26;
		protected System.Web.UI.WebControls.Label Label28;
		protected System.Web.UI.WebControls.Button btadd;
		protected System.Web.UI.WebControls.DropDownList dlsizebox;
		protected System.Web.UI.WebControls.Label Label17;
		protected System.Web.UI.WebControls.Label lbDate;
		protected System.Web.UI.WebControls.Label lbUserName;
		protected System.Web.UI.WebControls.Label lbUserID;
		protected System.Web.UI.WebControls.Label lbCustomerName;
		protected System.Web.UI.WebControls.Label lbCustomerID;
		protected System.Web.UI.WebControls.TextBox txtMaxKg;
		protected System.Web.UI.WebControls.TextBox txtKG;
		protected System.Web.UI.WebControls.TextBox txtQty;
		protected System.Web.UI.WebControls.DataGrid dgAdd;
		protected System.Web.UI.WebControls.TextBox txtCompanyName;
		protected System.Web.UI.WebControls.ListBox lsCompany;
		protected System.Web.UI.WebControls.CheckBox chkHC;
		protected System.Web.UI.WebControls.CheckBox chkInv;
		protected System.Web.UI.WebControls.ListBox lsService;
		protected System.Web.UI.WebControls.TextBox txtContactPerson;
		protected System.Web.UI.WebControls.Label Label2;
		protected System.Web.UI.WebControls.TextBox txtAddress;
		protected System.Web.UI.WebControls.Label Label1;
		protected System.Web.UI.WebControls.TextBox txtTelephone;
		protected System.Web.UI.WebControls.TextBox txtCharge;
		protected System.Web.UI.WebControls.TextBox txtBoxDesc;
		protected System.Web.UI.WebControls.Label Label8;
		protected System.Web.UI.WebControls.TextBox txtCODAmount;
		protected System.Web.UI.WebControls.Label Label7;
		protected System.Web.UI.WebControls.TextBox txtDeclareVal;
		protected System.Web.UI.WebControls.Button Search;
		protected System.Web.UI.WebControls.Label Label12;
		protected System.Web.UI.WebControls.TextBox txtspecial;
		protected System.Web.UI.WebControls.TextBox txtCustomerref;
		protected System.Web.UI.WebControls.Label lblCustInfo;
		protected System.Web.UI.WebControls.Label Label13;
		protected System.Web.UI.WebControls.Label Label18;
		protected System.Web.UI.WebControls.Label Label31;
		protected System.Web.UI.WebControls.TextBox txtConsignmentNo;
		protected System.Web.UI.WebControls.Label Label9;
		protected System.Web.UI.WebControls.Label lbMessage;
		protected System.Web.UI.WebControls.Panel Panel1;
		protected System.Web.UI.WebControls.Label Label32;
		protected System.Web.UI.WebControls.Label Label33;
		protected System.Web.UI.WebControls.Button btclose;
		protected System.Web.UI.WebControls.Button btsearchcon;
		protected System.Web.UI.WebControls.DataGrid dgSearch;
		protected System.Web.UI.WebControls.TextBox txtConsearch;
		protected System.Web.UI.WebControls.TextBox txtrefsearch;
		protected System.Web.UI.WebControls.Label Label34;
		string m_strAppID=null;
		string m_strEnterpriseID=null;
		string m_strCulture=null;	
		DataTable dt = new DataTable();
		bool blEdit=false;
		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings,Page.Session);
			m_strAppID = utility.GetAppID();
			m_strEnterpriseID = utility.GetEnterpriseID();
			m_strCulture = utility.GetUserCulture();

			if(!IsPostBack)
			{
				LoadCustomer();
				GetService();
				CreateTeble();
				GetBoxSize();
				GetBoxSize2();
				Save.Enabled = false;
//				getRunning();

				ClearData();
				btsearch.Enabled = false;
				btadd.Enabled = false;
				
				blEdit = false;
				Session["bool"] = blEdit;
				getBar();
			}
			Delete.Attributes.Add("Onclick","javascript:return confirm('You are delete?')");
		}
		 

		private void getBar()
		{
//			System.Drawing.Image myimg = Code128Rendering.MakeBarcodeImage(txtConsignmentNo.Text.Trim(), int.Parse("2"), true);
//			Image1.ImageUrl= myimg;
		}

		private void getRunning()
		{
			string strsql = "select max(Last_Consignment_Bar)+1 as Last_Consignment_Bar from dbo.tb_Running";
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(m_strAppID ,m_strEnterpriseID);
			IDbConnection conApp = dbCon.GetConnection();
	
			IDbCommand dbCmdCh=null;
			dbCmdCh = dbCon.CreateCommand(strsql,CommandType.Text);
			DataSet dsRunning = null;
			dsRunning =(DataSet)dbCon.ExecuteQuery(dbCmdCh,com.common.DAL.ReturnType.DataSetType);

			txtConsignmentNo.Text = dsRunning.Tables[0].Rows[0]["Last_Consignment_Bar"].ToString();
		}

		public void GetRate()
		{
			string strsql = "select * from tb_ChargeRate where 1=1";
			strsql += " and box_sizeid='" + dlsizebox.SelectedValue.ToString().Trim() + "'";
			strsql += " and Service_code='" + lsService.SelectedValue.Trim() + "'";

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(m_strAppID ,m_strEnterpriseID );
			IDbConnection conApp = dbCon.GetConnection();
	
			IDbCommand dbCmdCh=null;
			dbCmdCh = dbCon.CreateCommand(strsql,CommandType.Text);
			DataSet dsCharge = null;
			dsCharge =(DataSet)dbCon.ExecuteQuery(dbCmdCh,com.common.DAL.ReturnType.DataSetType);

			txtCharge.Text = dsCharge.Tables[0].Rows[0]["charge_rate"].ToString();
		}

		public void GetBoxSize()
		{
			string strsql = "select * from tb_BoxSize";
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(m_strAppID ,m_strEnterpriseID );
			IDbConnection conApp = dbCon.GetConnection();
	
			IDbCommand dbCmdCh=null;
			dbCmdCh = dbCon.CreateCommand(strsql,CommandType.Text);
			DataSet dsBox = null;
			dsBox =(DataSet)dbCon.ExecuteQuery(dbCmdCh,com.common.DAL.ReturnType.DataSetType);

			dlsizebox.DataSource = dsBox.Tables[0];
			dlsizebox.DataTextField ="Box_SizeID";
			dlsizebox.DataValueField ="Box_SizeID";
			dlsizebox.DataBind();
		}

		public void GetBoxSize2()
		{
			string strsql = "select * from tb_BoxSize where 1=1 ";
			strsql += " and box_sizeid='" + dlsizebox.SelectedItem.ToString().Trim() + "'";
			
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(m_strAppID ,m_strEnterpriseID );
			IDbConnection conApp = dbCon.GetConnection();
	
			IDbCommand dbCmdCh=null;
			dbCmdCh = dbCon.CreateCommand(strsql,CommandType.Text);
			DataSet dsBoxSize = null;
			dsBoxSize =(DataSet)dbCon.ExecuteQuery(dbCmdCh,com.common.DAL.ReturnType.DataSetType);

			txtMaxKg.Text =  dsBoxSize.Tables[0].Rows[0]["box_maxweight"].ToString();
			txtBoxDesc.Text = dsBoxSize.Tables[0].Rows[0]["box_wide"].ToString() + "*" + dsBoxSize.Tables[0].Rows[0]["box_length"].ToString() + "*" + dsBoxSize.Tables[0].Rows[0]["box_height"].ToString();
		}

		private void CreateTeble()
		{
			dt.Columns.Add("ServiceCode",typeof(string));
			dt.Columns.Add("BoxSize",typeof(string));
			dt.Columns.Add("DescriptionBox",typeof(string));
			dt.Columns.Add("Charge",typeof(string));
			dt.Columns.Add("KG",typeof(string));
			dt.Columns.Add("QTY",typeof(string));
			dt.Columns.Add("Total",typeof(string));

			dgAdd.DataSource = dt;
			dgAdd.DataBind();

			Session["Table"]= dt;
		}

		private void LoadCustomer()
		{
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(m_strAppID ,m_strEnterpriseID );
			IDbConnection conApp = dbCon.GetConnection();

			string strsql = "Select * from User_Master where userid='" + Session["userID"].ToString().Trim() + "'";
				
			IDbCommand dbCmdCh=null;
			dbCmdCh = dbCon.CreateCommand(strsql,CommandType.Text);
			DataSet dsUser = null;
			dsUser =(DataSet)dbCon.ExecuteQuery(dbCmdCh,com.common.DAL.ReturnType.DataSetType);

			strsql = "Select * from customer where custid='" + dsUser.Tables[0].Rows[0]["Payer"].ToString().Trim() + "'";
					
			IDbCommand dbCmdcust=null;
			dbCmdcust = dbCon.CreateCommand(strsql,CommandType.Text);
			DataSet dscust = null;
			dscust =(DataSet)dbCon.ExecuteQuery(dbCmdcust,com.common.DAL.ReturnType.DataSetType);

			lbDate.Text = DateTime.Now.Date.ToString("d");
			lbUserID.Text = dsUser.Tables[0].Rows[0]["UserId"].ToString().Trim();
			lbUserName.Text = dsUser.Tables[0].Rows[0]["User_Name"].ToString().Trim();
			lbCustomerID.Text = dsUser.Tables[0].Rows[0]["Payer"].ToString().Trim();

			if (dscust.Tables[0].Rows.Count > 0)
			{
				if((dscust.Tables[0].Rows[0]["cust_name"] != null) && 
					(!dscust.Tables[0].Rows[0]["cust_name"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
					(dscust.Tables[0].Rows[0]["cust_name"].ToString() != ""))
				{
					lbCustomerName.Text = dscust.Tables[0].Rows[0]["cust_name"].ToString().Trim();
				}
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Add.Click += new System.EventHandler(this.Add_Click);
			this.Edit.Click += new System.EventHandler(this.Edit_Click);
			this.Save.Click += new System.EventHandler(this.Save_Click);
			this.Search.Click += new System.EventHandler(this.Search_Click);
			this.btsearchcon.Click += new System.EventHandler(this.btsearchcon_Click);
			this.btclose.Click += new System.EventHandler(this.btclose_Click);
			this.dgSearch.ItemCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dgSearch_ItemCommand);
			this.Delete.Click += new System.EventHandler(this.Delete_Click);
			this.Print.Click += new System.EventHandler(this.Print_Click);
			this.btsearch.Click += new System.EventHandler(this.btsearch_Click);
			this.lsCompany.SelectedIndexChanged += new System.EventHandler(this.lsCompany_SelectedIndexChanged);
			this.lsService.SelectedIndexChanged += new System.EventHandler(this.lsService_SelectedIndexChanged);
			this.dlsizebox.SelectedIndexChanged += new System.EventHandler(this.dlsizebox_SelectedIndexChanged);
			this.btadd.Click += new System.EventHandler(this.btadd_Click);
			this.dgAdd.DeleteCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dgAdd_DeleteCommand);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private void btsearch_Click(object sender, System.EventArgs e)
		{   
			Session["Dsref"] = GetReference();			
			lsCompany.DataSource = GetReference().Tables[0];
			lsCompany.DataTextField = "reference_name";
			lsCompany.DataValueField = "reference_name";
			lsCompany.DataBind();
			
		}

		public DataSet GetReference()
		{
			string order = "";
			string strsql="";
			if(txtCompanyName.Text.Trim()=="")
			{
				strsql = @"select top 100";
			}
			else
			{
				strsql = @"select ";
			}
			strsql += @" telephone as customercode,reference_name,address1,address2,contactperson,t2.state_code,t2.zone_code,t3.state_name ,t1.zipcode,telephone
                                from [References] t1
                                inner join (select state_code,zone_code,zipcode from dbo.Zipcode 
											where enterpriseid='kdth'
											group by state_code,zone_code,zipcode) t2 on t1.zipcode= t2.zipcode
								inner join state t3
                                on t2.state_code = t3.state_code where 1=1";
			if(txtCompanyName.Text.Trim()!="")
			{
				strsql += " and ltrim(rtrim(reference_name)) like '%" + txtCompanyName.Text.Trim() + "%'";
			}
			if(txtTelephone.Text.Trim()!="")
			{
				strsql += " and Telephone like '%" + txtTelephone.Text.Trim() + "%'";
			}
								
			order += " order by reference_name";

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(m_strAppID ,m_strEnterpriseID );
			IDbConnection conApp = dbCon.GetConnection();

			IDbCommand dbCmdref=null;
			dbCmdref = dbCon.CreateCommand(strsql + order.Trim() ,CommandType.Text);
			DataSet dsref = null;
			dsref =(DataSet)dbCon.ExecuteQuery(dbCmdref,com.common.DAL.ReturnType.DataSetType);

			return dsref;
		}

		public void GetService()
		{
			string strsql="";
			strsql += @" select * from Service";

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(m_strAppID ,m_strEnterpriseID );
			IDbConnection conApp = dbCon.GetConnection();

			IDbCommand dbCmdServ=null;
			dbCmdServ = dbCon.CreateCommand(strsql ,CommandType.Text);
			DataSet dsServ = null;
			dsServ =(DataSet)dbCon.ExecuteQuery(dbCmdServ,com.common.DAL.ReturnType.DataSetType);

			lsService.DataSource = dsServ;
			lsService.DataTextField ="Service_code";
			lsService.DataValueField = "Service_code";
			lsService.DataBind();

			lsService.SelectedIndex =0;
		}

		private void lsCompany_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			DataSet dsref = new DataSet();
			dsref = (DataSet)Session["Dsref"];
			string strAddress = dsref.Tables[0].Rows[lsCompany.SelectedIndex]["address1"].ToString().Trim() + " " + dsref.Tables[0].Rows[lsCompany.SelectedIndex]["address2"].ToString().Trim() + " " + dsref.Tables[0].Rows[lsCompany.SelectedIndex]["state_code"].ToString().Trim() + " " + dsref.Tables[0].Rows[lsCompany.SelectedIndex]["Zipcode"].ToString().Trim();
			txtAddress.Text = strAddress;
			lbCustZone.Text = dsref.Tables[0].Rows[lsCompany.SelectedIndex]["Zone_code"].ToString().Trim();
			txtCompanyName.Text = dsref.Tables[0].Rows[lsCompany.SelectedIndex]["Reference_name"].ToString().Trim();
			txtTelephone.Text = dsref.Tables[0].Rows[lsCompany.SelectedIndex]["telephone"].ToString().Trim();
		}

		private void lsService_SelectedIndexChanged(object sender, System.EventArgs e)
		{
		
		}

		private void dlsizebox_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			GetBoxSize2();
			GetRate();
		}

		private void btadd_Click(object sender, System.EventArgs e)
		{
			DataRow dr;
			dt =(DataTable)Session["Table"];
			dr = dt.NewRow();
			//dr["seq_no"] = dt.Rows.Count + 1;
			dr["ServiceCode"] = lsService.SelectedValue;
			dr["BoxSize"] = dlsizebox.SelectedValue.ToString();
			dr["DescriptionBox"] = txtBoxDesc.Text;
			dr["Charge"] = txtCharge.Text;
			dr["KG"] = txtKG.Text;
			dr["QTY"] = txtQty.Text;
			dr["Total"] = (double.Parse(txtKG.Text) * double.Parse(txtQty.Text)).ToString();
			dt.Rows.Add(dr);
			dt.AcceptChanges();
			dgAdd.DataSource = dt; 
			dgAdd.DataBind();

			Session["Table"]=dt;

		}

		private void Add_Click(object sender, System.EventArgs e)
		{
			ClearData();
			btsearch.Enabled = false;
			btadd.Enabled = false;

			Delete.Enabled = false;
			Search.Enabled = true;
			Delete.Enabled = false;

			blEdit=false;
			Session["bool"] = blEdit;
			lbMessage.Text ="";
			Edit.Enabled = true;
		}

		private void ClearData()
		{
			txtConsignmentNo.Text="";
			Save.Enabled = false;
			txtContactPerson.Text ="";
			txtCustomerref.Text ="";
			txtTelephone.Text ="";
			lsCompany.Items.Clear();
			txtAddress.Text ="";
			txtspecial.Text ="";
			chkHC.Checked = false;
			chkInv.Checked =false;
			dlsizebox.SelectedIndex = 0;
			txtKG.Text ="";
			txtQty.Text ="";
			lsService.SelectedIndex = 0;
			dt.Rows.Clear();
			txtCharge.Text ="";
			txtMaxKg.Text ="";
			txtBoxDesc.Text ="";
			dgAdd.DataSource = dt;
			dgAdd.DataBind();
			txtCompanyName.Text ="";

		}

		private void Edit_Click(object sender, System.EventArgs e)
		{
			btsearch.Enabled = true;
			btadd.Enabled = true;

			Save.Enabled = true;
			Search.Enabled = false;
			Delete.Enabled = false;

			getRunning();

			blEdit=false;
			Session["bool"] = blEdit;
			lbMessage.Text ="";
			
			CreateTeble();
			Session["Table"]=dt;

			
			GetBoxSize2();
			GetRate();
			
		}

		private void Save_Click(object sender, System.EventArgs e)
		{
			if(txtCompanyName.Text.Trim() =="")
			{
				lbMessage.Text ="Company not found........";
				return;
			}
			if(lbCustZone.Text.Trim()=="")
			{
				lbMessage.Text ="Zone not found........";
				return;
			}
			if(txtDeclareVal.Text =="")
			{
				lbMessage.Text ="Declare Value not found........";
				return;
			}
			if(txtCODAmount.Text =="")
			{
				lbMessage.Text ="COD Amount not found........";
				return;
			}

			DataTable dtchk =(DataTable)Session["Table"];
			if(dtchk.Rows.Count ==0)
			{
				lbMessage.Text ="BOX not found........";
				return;
			}

			if(!bool.Parse(Session["bool"].ToString()))
			{
				InsertConsignmentNote();
				ClearData();
				Save.Enabled = false;
				Search.Enabled = true;
				lbMessage.Text ="Consignment No has been Saved........";
			}
			else
			{
				UpdateConsignmentNote();
				Save.Enabled = true;
				Search.Enabled = true;
				lbMessage.Text ="Consignment No has been Updated........";
			}
		
		}

		public void UpdateConsignmentNote()
		{
			DateTime dateT = DateTime.Now;
			string ConDate = dateT.Year.ToString();
			ConDate += dateT.Month.ToString().Length == 1 ? "0" + dateT.Month.ToString() : dateT.Month.ToString();
			ConDate += dateT.Day.ToString().Length == 1 ? "0" + dateT.Day.ToString() : dateT.Day.ToString();

			int inhc = chkHC.Checked==true?1:0;
			int iniv = chkInv.Checked== true?1:0;
			string dateUpdateDate = DateTime.Now.Date.ToString("G");
			string strsql = @"update tb_consignmentNote set PayerID='"+ lbCustomerID.Text.Trim() +"',LastUserID='"+ Session["userID"].ToString().Trim() +"'," +
				"Telephone ='"+ txtTelephone.Text.Trim() +"',HCReturn="+ inhc +",InvReturn="+ iniv +",codamount="+ txtCODAmount.Text.Trim() +","+
				"LastUpdateDate='"+ dateUpdateDate +"',ConsignmentDate="+ Int64.Parse(ConDate) + ","+
				"ServiceType='"+ lsService.SelectedValue.Trim() +"',DeclareValue="+ txtDeclareVal.Text.Trim() +",recipion_contact_person='"+ txtContactPerson.Text.Trim() +"'," +
				"cust_ref='"+ txtCustomerref.Text.Trim() +"',Special_Instruction='"+ txtspecial.Text.Trim() +"',address='"+ txtAddress.Text.Trim() +"',"+
				"company_name='"+ txtCompanyName.Text.Trim() +"',customercode='"+ txtTelephone.Text.Trim() +"' where consignment_no='"+ txtConsignmentNo.Text.Trim() +"'"; 
			
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(m_strAppID,m_strEnterpriseID );
			IDbConnection conApp = dbCon.GetConnection();
			IDbCommand dbExcute=null;
			dbExcute = dbCon.CreateCommand(strsql,CommandType.Text);
			dbCon.ExecuteNonQuery(dbExcute);

		}		

		public void InsertConsignmentNote()
		{
			
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(m_strAppID ,m_strEnterpriseID);
			IDbConnection conApp = dbCon.GetConnection();

			DateTime dateT = DateTime.Now;
			string ConDate = dateT.Year.ToString();
			ConDate += dateT.Month.ToString().Length == 1 ? "0" + dateT.Month.ToString() : dateT.Month.ToString();
			ConDate += dateT.Day.ToString().Length == 1 ? "0" + dateT.Day.ToString() : dateT.Day.ToString();

			int HC = chkHC.Checked==true?1:0;
			int Inv =chkInv.Checked==true?1:0;
			string dateUpdateDate = DateTime.Now.Date.ToString("G");
			string strHInsert = @"Insert Into tb_consignmentNote(enterpriseid,Consignment_No,PayerID,LastUserID,Telephone,HCReturn,InvReturn,codamount,LastUpdateDate,ConsignmentDate,ServiceType,DeclareValue,recipion_contact_person,cust_ref,Special_Instruction,address,company_name,customercode) 
            values('"+m_strEnterpriseID +"','" + txtConsignmentNo.Text.Trim() + "','" + lbCustomerID.Text.Trim() +"','"+ Session["userID"].ToString().Trim() +"','"+ txtTelephone.Text.Trim() +"',"+
				"" + HC + "," + Inv + ","+ txtCODAmount.Text.Trim() +",'" + DateTime.Parse(dateUpdateDate) + "'," + Int64.Parse(ConDate) + ",'" + lsService.SelectedValue.ToString().Trim() + "'," + txtDeclareVal.Text.Trim() + "," + 
				"'" + txtContactPerson.Text.Trim() + "','" +  txtCustomerref.Text.Trim() + "','" + txtspecial.Text.Trim() + "','" + txtAddress.Text.Trim() + "','" + txtCompanyName.Text.Trim() + "','"+ txtTelephone.Text.Trim() +"')";

			IDbCommand dbExcute=null;
			dbExcute = dbCon.CreateCommand(strHInsert,CommandType.Text);
			dbCon.ExecuteNonQuery(dbExcute);

			string dateUpdateDate1 = DateTime.Now.Date.ToString("G");
			string strDInsert="";
			dt = (DataTable)Session["Table"];
			for(int i=0;i< dt.Rows.Count ;i++ )
			{
				strDInsert = @"Insert Into tb_consignmentNoteDetail(consignment_no,seq_no,PayerID,Telephone,Boxsize,kilo,Qty,LastUpdateDate,ConsignmentDate,customercode) 
				values('" + txtConsignmentNo.Text.Trim() + "'," + i +",'" + lbCustomerID.Text.Trim() + "','" + txtTelephone.Text.Trim() + "'," +
					"'" + dt.Rows[i]["BoxSize"].ToString().Trim() + "'," + dt.Rows[i]["KG"].ToString().Trim() + "," + dt.Rows[i]["QTY"].ToString().Trim() + "," + 
					"'" + dateUpdateDate1 + "'," + Int64.Parse(ConDate) + ",'" + txtTelephone.Text.Trim() + "')";
			
				IDbCommand dbExcute1=null;
				dbExcute1 = dbCon.CreateCommand(strDInsert,CommandType.Text);
				dbCon.ExecuteNonQuery(dbExcute1);
			}	

			System.Drawing.Image myimg = Code128Rendering.MakeBarcodeImage(txtConsignmentNo.Text.Trim(), int.Parse("2"), true);
			byte[] content = ReadBitmap2ByteArray(myimg);
			StoreBlob2DataBase(content,txtConsignmentNo.Text.Trim());

			UpdateRunning();
		}

		protected static byte[] ReadBitmap2ByteArray(System.Drawing.Image fileName)
		{
			using(Bitmap image = new Bitmap(fileName))
			{
				System.IO.MemoryStream stream = new System.IO.MemoryStream();
				image.Save(stream, System.Drawing.Imaging.ImageFormat.Bmp);
				return stream.ToArray();
			}
		
		}

		protected static void StoreBlob2DataBase(byte[] content,string txt)
		{
			Utility objUtility = new Utility(System.Configuration.ConfigurationSettings.AppSettings,null);
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(objUtility.GetAppID() ,objUtility.GetEnterpriseID());
			IDbConnection conApp = dbCon.GetConnection();
			SqlConnection strcon = new SqlConnection(conApp.ConnectionString);
			strcon.Open();
			
			try
			{
				// insert new entry into table
				SqlCommand ssql =new SqlCommand("Update tb_ConsignmentNote set Images= @image where consignment_no='"+ txt +"'",strcon);
				SqlParameter imageParameter =  ssql.Parameters.Add("@image", SqlDbType.Binary);
				imageParameter.Value = content;
				imageParameter.Size  = content.Length;
				ssql.ExecuteNonQuery();
//				dbExcute1 = dbCon.CreateCommand(ssql,CommandType.Text);
//				dbCon.ExecuteNonQuery(dbExcute1);
			}
			finally
			{
				strcon.Close();
			}
		}

		public void UpdateRunning()
		{
			string NewConsignmentNo = (Int64.Parse(txtConsignmentNo.Text)).ToString();
			string strsql = "Update tb_Running set Last_Consignment_Bar = '" + NewConsignmentNo + "'";

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(m_strAppID ,m_strEnterpriseID);
			IDbConnection conApp = dbCon.GetConnection();

			IDbCommand dbCmdRun=null;
			dbCmdRun = dbCon.CreateCommand(strsql ,CommandType.Text);
			dbCon.ExecuteNonQuery(dbCmdRun);

			getRunning();
		}

		private void dgAdd_DeleteCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
		{
			dt = (DataTable)Session["Table"];
			dt.Rows.RemoveAt(e.Item.ItemIndex); 

			dgAdd.DataSource = dt;
			dgAdd.DataBind();
		}

		private void Search_Click(object sender, System.EventArgs e)
		{
			btsearch.Enabled = true;
			btadd.Enabled = true;

			Delete.Enabled = true;
			Search.Enabled = true;			

			this.Panel1.Visible = true;
			blEdit = true;
			Session["bool"]=blEdit;
			lbMessage.Text ="";

			GetBoxSize2();
			GetRate();
		}

		private void btclose_Click(object sender, System.EventArgs e)
		{
			Panel1.Visible = false; 
            dgSearch.DataSource = null; 
			dgSearch.DataBind();
			//Session["Consignment_no"]="";
		}

		private void btsearchcon_Click(object sender, System.EventArgs e)
		{
			LoadData();
		}

		private void LoadData()
		{
			string strsql = @"
                            select distinct t1.customercode,Payerid,t1.consignment_no,t2.cust_name,t2.address1,t2.address2,
                            (select state_name from state t3 where t2.state_code = t3.state_code) as state_name,t2.zipcode,t2.telephone,
                            t4.reference_name,t4.address1,t4.address2,
                            (select state_name from state t5 where t2.state_code = t5.state_code) as state_name_ref,t4.zipcode,t4.telephone
                            from tb_consignmentnote t1
                            left outer join Customer t2
                            on t1.payerid = t2.custid
                            left outer join [references] t4
                            on t1.telephone = t4.telephone
                            where t1.flag_export is null and payerid='"+ lbCustomerID.Text.Trim() +"'";

			if(txtConsearch.Text.Trim()!="")
			{
				strsql+=" and Cosignment_no ='"+ txtConsearch.Text.Trim() +"'";
			}
			if(txtrefsearch.Text.Trim()!="")
			{
				strsql+=" and reference_Name ='"+ txtrefsearch.Text.Trim() +"'";
			}

			strsql +=" order by t1.consignment_no";

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(m_strAppID ,m_strEnterpriseID );
			IDbConnection conApp = dbCon.GetConnection();

			IDbCommand dbCmdCh=null;
			dbCmdCh = dbCon.CreateCommand(strsql,CommandType.Text);
			DataSet dsretrive = null;
			dsretrive =(DataSet)dbCon.ExecuteQuery(dbCmdCh,com.common.DAL.ReturnType.DataSetType);

			dgSearch.DataSource = dsretrive.Tables[0];
			dgSearch.DataBind();

			Session["AllCon"]= dsretrive;
		}

		private void dgSearch_ItemCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
		{

			Panel1.Visible = false;
			DataSet ds = (DataSet)Session["AllCon"];
			Session["Consignment_no"] = ds.Tables[0].Rows[e.Item.ItemIndex]["Consignment_no"].ToString();
			Session["PayerID"] = ds.Tables[0].Rows[e.Item.ItemIndex]["PayerID"].ToString();
			ds.Clear();
			dgSearch.DataSource = ds.Tables[0];
			dgSearch.DataBind();

			DataSet dsMain = GetCustReferenceMain();
			txtConsignmentNo.Text = dsMain.Tables[0].Rows[0]["Consignment_no"].ToString();
			lbCustZone.Text = dsMain.Tables[0].Rows[0]["zone_code"].ToString();
			txtContactPerson.Text =dsMain.Tables[0].Rows[0]["recipion_contact_person"].ToString();
			txtCustomerref.Text =dsMain.Tables[0].Rows[0]["cust_ref"].ToString();
			txtCompanyName.Text =dsMain.Tables[0].Rows[0]["company_name"].ToString();
			txtTelephone.Text =dsMain.Tables[0].Rows[0]["Telephone"].ToString();
			txtAddress.Text =dsMain.Tables[0].Rows[0]["address"].ToString();
			txtspecial.Text =dsMain.Tables[0].Rows[0]["Special_Instruction"].ToString();
			chkHC.Checked =bool.Parse(dsMain.Tables[0].Rows[0]["HCReturn"].ToString());
			chkInv.Checked = bool.Parse(dsMain.Tables[0].Rows[0]["InvReturn"].ToString());
			lsService.SelectedValue = dsMain.Tables[0].Rows[0]["ServiceType"].ToString();
			
			DataSet dsSub = GetCustReferenceDetail();
			CreateTeble();			
			for (int i = 0; i < dsSub.Tables[0].Rows.Count; i++)
			{
				DataRow dr = dt.NewRow();
				dr["ServiceCode"] = dsSub.Tables[0].Rows[i]["servicetype"].ToString();
				dr["BoxSize"] = dsSub.Tables[0].Rows[i]["boxsize"].ToString();
				dr["DescriptionBox"] = dsSub.Tables[0].Rows[i]["box_Desc"].ToString();
				dr["Charge"] = dsSub.Tables[0].Rows[i]["charge_rate"].ToString();
				dr["KG"] = dsSub.Tables[0].Rows[i]["Kilo"].ToString();
				dr["QTY"] = dsSub.Tables[0].Rows[i]["Qty"].ToString();
				double s = double.Parse(dsSub.Tables[0].Rows[i]["Qty"].ToString());
				double s1 = double.Parse(dsSub.Tables[0].Rows[i]["Kilo"].ToString());
				dr["Total"] = s * s1;

				dt.Rows.Add(dr);
				dt.AcceptChanges();
			}

			dgAdd.DataSource = dt;
			dgAdd.DataBind();

			Save.Enabled = true;
			Delete.Enabled = true;
			Add.Enabled = true;
			Edit.Enabled = false;

			Session["bool"] = true;
		}

		public DataSet GetCustReferenceMain()   
		{
			string strsql = @"select t1.*,t2.zipcode,t3.zone_code,t2.reference_name,
							t2.address1 ,t2.address2,t4.state_name,t5.service_description
                            from tb_consignmentNote t1 
                            left outer join dbo.[References] t2 
                            on t1.telephone = t2.telephone 
                            left outer join dbo.Zipcode t3
                            on t2.zipcode = t3.zipcode 
                            left outer join state t4
                            on t4.state_code = t3.state_code  
			                left outer join Service t5
			                on t1.servicetype = t5.service_code
                            where 1=1 and consignment_no =" + Session["Consignment_no"].ToString() + " and payerid='" + Session["PayerID"].ToString().Trim() + "'";
			
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(m_strAppID ,m_strEnterpriseID);
			IDbConnection conApp = dbCon.GetConnection();

			IDbCommand dbCmdCh=null;
			dbCmdCh = dbCon.CreateCommand(strsql,CommandType.Text);
			DataSet dsMain = null;
			dsMain =(DataSet)dbCon.ExecuteQuery(dbCmdCh,com.common.DAL.ReturnType.DataSetType);
			return dsMain;

		}

		public DataSet GetCustReferenceDetail()
		{
			string strsql = @"select distinct t1.PayerID,t1.Telephone,LastUserID,HCReturn,InvReturn,t1.servicetype,
                            CODAmount,t1.LastUpdateDate,t1.ConsignmentDate,t1.ServiceType,
                            Boxsize,kilo,Qty,t1.LastUpdateDate,(cast(box_wide as varchar(10)) +'*'+ cast(box_length as varchar(10)) +'*'+ cast(box_height as varchar(10))) as box_desc,charge_rate,t2.seq_no
                            from tb_consignmentNote t1 
                            inner join tb_ConsignmentNoteDetail t2 
                            on t1.payerid = t2.payerid 
                            and t1.consignment_no = t2.consignment_no
                            left outer join tb_BoxSize t3 
                            on t2.Boxsize = t3.box_sizeid
                            left outer join tb_ChargeRate t5 
                            on t2.Boxsize=t5.box_sizeid 
                            and t1.ServiceType=t5.service_code where 1=1  
                            and t1.payerid='" + Session["PayerID"] + "' and t1.consignment_no='" + Session["Consignment_no"].ToString() + "' order by seq_no";
			
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(m_strAppID ,m_strEnterpriseID );
			IDbConnection conApp = dbCon.GetConnection();

			IDbCommand dbCmdCh=null;
			dbCmdCh = dbCon.CreateCommand(strsql,CommandType.Text);
			DataSet dsDetail = null;
			dsDetail =(DataSet)dbCon.ExecuteQuery(dbCmdCh,com.common.DAL.ReturnType.DataSetType);
			return dsDetail;
		}

		private void Delete_Click(object sender, System.EventArgs e)
		{

			string strsql =" delete from tb_ConsignmentNote where payerid='"+ lbCustomerID.Text.Trim() +"' and consignment_no='"+ txtConsignmentNo.Text.Trim() +"'";
			string strsql1 =" delete from tb_ConsignmentNoteDetail where payerid='"+ lbCustomerID.Text.Trim() +"' and consignment_no='"+ txtConsignmentNo.Text.Trim() +"'";
			
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(m_strAppID ,m_strEnterpriseID);
			IDbConnection conApp = dbCon.GetConnection();

			IDbCommand dbCmdCh=null;
			dbCmdCh = dbCon.CreateCommand(strsql +" "+ strsql1,CommandType.Text);
			dbCon.ExecuteNonQuery(dbCmdCh);
			blEdit=false;
			Session["bool"] = blEdit;
			ClearData();
			lbMessage.Text ="Consignment No. has been deleted........";

		}

		private void Print_Click(object sender, System.EventArgs e)
		{
			Session["PayerID"]=lbCustomerID.Text.Trim();

			Response.Redirect("popupConsignmentNote.aspx");

		}
	}
}

