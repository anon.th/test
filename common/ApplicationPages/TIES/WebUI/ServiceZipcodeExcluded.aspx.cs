using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.ties.DAL;
using com.common.util;
using com.common.classes;
using com.common.applicationpages;


namespace com.ties
{
	/// <summary>
	/// Summary description for ServiceZipcodeExcluded.
	/// </summary>
	/// 
	

	public class ServiceZipcodeExcluded : BasePage
	{
		protected System.Web.UI.WebControls.Button btnExecQry;
		protected System.Web.UI.WebControls.Button btnQry;
		protected System.Web.UI.WebControls.Button btnSave;
		protected System.Web.UI.WebControls.Button btnInsert;
		protected System.Web.UI.WebControls.Button btnDelete;
		
		SessionDS m_sdsServiceCode = null;
		SessionDS m_sdsZipcodeServiceExcluded = null;
		private String m_strCulture;
		private DataView m_dvAutoManifestOptions = null;
		private String m_strAppID;
		private String m_strEnterpriseID;		
		protected System.Web.UI.WebControls.DataGrid dgServiceCodes;
		protected System.Web.UI.WebControls.DataGrid dgZipcodeServiceExcluded;
		protected System.Web.UI.WebControls.Label lblSCMessage;
		protected System.Web.UI.WebControls.Label lblZSCEMessage;
		protected System.Web.UI.WebControls.ValidationSummary ValidationSummary1;
		protected System.Web.UI.WebControls.Button btnZSEInsert;
		protected System.Web.UI.WebControls.Button btnZSEInsertMultiple;
		protected System.Web.UI.WebControls.Label Label2;
		protected System.Web.UI.WebControls.Label lblTitle;
		//SessionDS dsServiceTmp = null;

		//Utility utility = null;
	
		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			m_strAppID = utility.GetAppID();
			m_strEnterpriseID = utility.GetEnterpriseID();
			m_strCulture = utility.GetUserCulture();
			//utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings,Page.Session);
			ShowButtonColumns(true);
					
			if(!Page.IsPostBack)
			{
				ViewState["SCOperation"] = Operation.None;
				ViewState["ZSEMode"] = ScreenMode.None;
				ViewState["ZSEOperation"] = Operation.None;
				Session["SESSION_DS2"] = m_sdsZipcodeServiceExcluded;

				m_dvAutoManifestOptions = CreateAutoManifestOptions(true);
				ResetScreenForQuery();

			}
			else
			{
				m_sdsServiceCode = (SessionDS)Session["SESSION_DS1"];
				m_sdsZipcodeServiceExcluded = (SessionDS)Session["SESSION_DS2"];
				m_dvAutoManifestOptions = CreateAutoManifestOptions(true);
				lblSCMessage.Text = "";
				if(Session["ServiceZipCode"] != null)
				{
					Session["ServiceZipCode"]=null;
					ShowCurrentZSEPage();
					ViewState["ZSEMode"] = ScreenMode.ExecuteQuery;
				}
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			base.OnInit(e);
			InitializeComponent();
			
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnQry.Click += new System.EventHandler(this.btnQry_Click);
			this.btnExecQry.Click += new System.EventHandler(this.btnExecQry_Click);
			this.btnInsert.Click += new System.EventHandler(this.btnInsert_Click);
			this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
			this.btnZSEInsert.Click += new System.EventHandler(this.btnZSEInsert_Click);
			this.btnZSEInsertMultiple.Click += new System.EventHandler(this.btnZSEInsertMultiple_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion


		private void btnQry_Click(object sender, System.EventArgs e)
		{
			ResetScreenForQuery();
		}

		private void ResetScreenForQuery()
		{			
			m_sdsServiceCode = SysDataMgrDAL.GetEmptyServiceCodeDS(1);
			
			ViewState["SCMode"] = ScreenMode.Query;

			Session["SESSION_DS1"] = m_sdsServiceCode;
			dgServiceCodes.EditItemIndex = 0;
			dgServiceCodes.CurrentPageIndex = 0;
			BindSCGrid();
			btnExecQry.Enabled = true;
			Utility.EnableButton(ref btnInsert,ButtonType.Insert,true,m_moduleAccessRights);
			lblSCMessage.Text = "";

			ShowButtonColumns(false);
			
			ResetDetailsGrid();
		}
		private void btnExecQry_Click(object sender, System.EventArgs e)
		{

			FillSCDataRow(dgServiceCodes.Items[0],0);
			ViewState["QUERY_DS"] = m_sdsServiceCode.ds;
			dgServiceCodes.CurrentPageIndex = 0;
			ShowCurrentSCPage();
			btnExecQry.Enabled = false;
			Utility.EnableButton(ref btnInsert,ButtonType.Insert,true,m_moduleAccessRights);
			lblSCMessage.Text = "";
			ViewState["SCMode"] = ScreenMode.ExecuteQuery;
		}

		private void btnInsert_Click(object sender, System.EventArgs e)
		{
			//If coming to any other mode to Insert Mode then create the empty data set.
			lblSCMessage.Text = "";
			lblZSCEMessage.Text = "";
			if((int)ViewState["SCMode"] != (int)ScreenMode.Insert || m_sdsServiceCode.ds.Tables[0].Rows.Count >= dgServiceCodes.PageSize)
			{
				m_sdsServiceCode = SysDataMgrDAL.GetEmptyServiceCodeDS(0);
			}

			AddRowInSCGrid();	
			dgServiceCodes.EditItemIndex = m_sdsServiceCode.ds.Tables[0].Rows.Count - 1;
			dgServiceCodes.CurrentPageIndex = 0;
			Logger.LogDebugInfo("ServiceZipcodeExcluded","btnInsert_Click","INF003","Data Grid Items count"+dgServiceCodes.Items.Count);
			BindSCGrid();
			ViewState["SCMode"] = ScreenMode.Insert;
			ViewState["SCOperation"] = Operation.Insert;
			Utility.EnableButton(ref btnInsert,ButtonType.Insert,false,m_moduleAccessRights);
			btnExecQry.Enabled = false;			
			ResetDetailsGrid();
			getPageControls(Page);
		}

		public void dgServiceCodes_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			Label lblServiceCode = (Label)dgServiceCodes.SelectedItem.FindControl("lblServiceCode");
			msTextBox txtServiceCode = (msTextBox)dgServiceCodes.SelectedItem.FindControl("txtServiceCode");
			String strServiceCode = null;

			if(lblServiceCode != null)
			{
				strServiceCode = lblServiceCode.Text;
			}

			if(txtServiceCode != null)
			{
				strServiceCode = txtServiceCode.Text;
			}
			ViewState["CurrentSC"] = strServiceCode;
			dgZipcodeServiceExcluded.CurrentPageIndex = 0;

			Logger.LogDebugInfo("ServiceZipcodeExcluded","dgServiceCodes_SelectedIndexChanged","INF004","updating data grid..."+dgServiceCodes.SelectedIndex+"  : "+strServiceCode);
			ShowCurrentZSEPage();

			ViewState["ZSEMode"] = ScreenMode.ExecuteQuery;

			if((int)ViewState["SCMode"] != (int)ScreenMode.Insert && (int)ViewState["SCOperation"] != (int)Operation.Insert)
			{
				Utility.EnableButton(ref btnInsert,ButtonType.Insert,true,m_moduleAccessRights);
			}

			if((int)ViewState["SCMode"] == (int)ScreenMode.Insert && dgServiceCodes.EditItemIndex == dgServiceCodes.SelectedIndex && (int)ViewState["SCOperation"] == (int)Operation.Insert)
			{
				btnZSEInsert.Enabled = false;
				btnZSEInsertMultiple.Enabled = false;
			}
			else
			{
				btnZSEInsert.Enabled = true;
				btnZSEInsertMultiple.Enabled = true;

			}
			lblSCMessage.Text = "";
			lblZSCEMessage.Text ="";
		}

		protected void dgServiceCodes_Edit(object sender, DataGridCommandEventArgs e)
		{
			lblSCMessage.Text = "";
			lblZSCEMessage.Text = "";
			if((int)ViewState["SCMode"] == (int)ScreenMode.Insert && (int) ViewState["SCOperation"] == (int)Operation.Insert && dgServiceCodes.EditItemIndex > 0)
			{
				m_sdsServiceCode.ds.Tables[0].Rows.RemoveAt(dgServiceCodes.EditItemIndex);
				m_sdsServiceCode.QueryResultMaxSize--;
				dgServiceCodes.CurrentPageIndex = 0;
			}

			dgServiceCodes.EditItemIndex = e.Item.ItemIndex;
			ViewState["SCOperation"] = Operation.Update;
			BindSCGrid();			
			Logger.LogDebugInfo("ServiceZipcodeExcluded","dgServiceCodes_Edit","INF004","updating data grid...");			
		}
		
		public void dgServiceCodes_Update(object sender, DataGridCommandEventArgs e)
		{			
			try
			{
				FillSCDataRow(e.Item,e.Item.ItemIndex);
			}
			catch(ApplicationException appException)
			{
				String strMsg = appException.Message;
				strMsg = appException.InnerException.Message;
				if(strMsg.IndexOf("duplicate key") != -1 )
				{
					lblSCMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"DUP_KEY_FOUND_SRV_CDE",utility.GetUserCulture());
					return;
				}
				if(strMsg.IndexOf("unique") != -1 )
				{
					lblSCMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"DUP_KEY_FOUND_SRV_CDE",utility.GetUserCulture());
					return;
				}
			}

			int intChkRet = CheckTransit(e.Item.ItemIndex);
			if (intChkRet < 0)
			{
				return;

			}
			int iOperation = (int)ViewState["SCOperation"];
			switch(iOperation)
			{

				case (int)Operation.Update:

					DataSet dsToUpdate = m_sdsServiceCode.ds.GetChanges();
					SysDataMgrDAL.ModifyServiceCodeDS(utility.GetAppID(),utility.GetEnterpriseID(),dsToUpdate);
					Utility.EnableButton(ref btnInsert,ButtonType.Insert,true,m_moduleAccessRights);
					lblSCMessage.Text=Utility.GetLanguageText(ResourceType.UserMessage,"UPD_SUCCESSFULLY",utility.GetUserCulture());
					m_sdsServiceCode.ds.Tables[0].Rows[e.Item.ItemIndex].AcceptChanges();
					Logger.LogDebugInfo("ServiceZipcodeExcluded","dgServiceCodes_OnUpdate","INF004","update in modify mode..");

					break;

				case (int)Operation.Insert:

					DataSet dsToInsert = m_sdsServiceCode.ds.GetChanges();
					try
					{
						SysDataMgrDAL.AddServiceCodeDS(utility.GetAppID(),utility.GetEnterpriseID(),dsToInsert);
						lblSCMessage.Text=Utility.GetLanguageText(ResourceType.UserMessage,"INS_SUCCESSFULLY",utility.GetUserCulture());
						Utility.EnableButton(ref btnInsert,ButtonType.Insert,true,m_moduleAccessRights);
						m_sdsServiceCode.ds.Tables[0].Rows[e.Item.ItemIndex].AcceptChanges();
						Logger.LogDebugInfo("ServiceZipcodeExcluded","dgServiceCodes_OnUpdate","INF004","update in Insert mode");
						
					}
					catch(ApplicationException appException)
					{
						String strMsg = appException.Message;
						strMsg = appException.InnerException.Message;
						strMsg = appException.InnerException.InnerException.Message;
						if(strMsg.IndexOf("duplicate key") != -1 || strMsg.IndexOf("PRIMARY KEY") != -1)
						{
							lblSCMessage.Text =  Utility.GetLanguageText(ResourceType.UserMessage,"DUP_KEY_FOUND_SRV_CDE",utility.GetUserCulture());
							return;
						}
						if(strMsg.IndexOf("unique") != -1 )
						{
							lblSCMessage.Text =  Utility.GetLanguageText(ResourceType.UserMessage,"DUP_KEY_FOUND_SRV_CDE",utility.GetUserCulture());
							return;
						}
						
						
					}
					break;

			}
			dgServiceCodes.EditItemIndex = -1;
			ViewState["SCOperation"] = Operation.None;
			lblZSCEMessage.Text="";
			BindSCGrid();
		}

		protected void dgServiceCodes_Cancel(object sender, DataGridCommandEventArgs e)
		{
			lblSCMessage.Text = "";
			lblZSCEMessage.Text = "";
			dgServiceCodes.EditItemIndex = -1;
			if((int)ViewState["SCMode"] == (int)ScreenMode.Insert && (int)ViewState["SCOperation"] == (int)Operation.Insert)
			{
				m_sdsServiceCode.ds.Tables[0].Rows.RemoveAt(e.Item.ItemIndex);
				m_sdsServiceCode.QueryResultMaxSize--;
				dgServiceCodes.CurrentPageIndex = 0;
			}

			ViewState["SCOperation"] = Operation.None;
			BindSCGrid();
			Utility.EnableButton(ref btnInsert,ButtonType.Insert,true,m_moduleAccessRights);
			Logger.LogDebugInfo("ServiceZipcodeExcluded","dgServiceCodes_Cancel","INF004","updating data grid...");			
		}

		public void dgServiceCodes_Delete(object sender, DataGridCommandEventArgs e)
		{
			msTextBox txtServiceCode = (msTextBox)e.Item.FindControl("txtServiceCode");
			Label lblServiceCode = (Label)e.Item.FindControl("lblServiceCode");
			String strServiceCode = null;
			if(txtServiceCode != null)
			{
				strServiceCode = txtServiceCode.Text;
			}

			if(lblServiceCode != null)
			{
				strServiceCode = lblServiceCode.Text;
			}

			try
			{
				SysDataMgrDAL.DeleteServiceCodeDS(utility.GetAppID(),utility.GetEnterpriseID(),strServiceCode);
				lblSCMessage.Text=Utility.GetLanguageText(ResourceType.UserMessage,"DLD_SUCCESSFULLY",utility.GetUserCulture());
			}
			catch(ApplicationException appException)
			{
				String strMsg = appException.Message;
				if(strMsg.IndexOf("FK") != -1)
				{
					lblSCMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"ERR_DEL_SERVICE_TRANS",utility.GetUserCulture());
					
				}
				if(strMsg.IndexOf("child record") != -1)
				{
					lblSCMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"ERR_DEL_SERVICE_TRANS",utility.GetUserCulture());
					
				}
				return;
			}

			if((int)ViewState["SCMode"] == (int)ScreenMode.Insert)
			{
				m_sdsServiceCode.ds.Tables[0].Rows.RemoveAt(e.Item.ItemIndex);
				m_sdsServiceCode.QueryResultMaxSize--;
				dgServiceCodes.CurrentPageIndex = 0;

				if((int) ViewState["SCOperation"] == (int)Operation.Insert && dgServiceCodes.EditItemIndex > 0)
				{
					m_sdsServiceCode.ds.Tables[0].Rows.RemoveAt(dgServiceCodes.EditItemIndex-1);
					m_sdsServiceCode.QueryResultMaxSize--;

				}
				dgServiceCodes.EditItemIndex = -1;
				dgServiceCodes.SelectedIndex = -1;
				BindSCGrid();
				ResetDetailsGrid();
			}
			else
			{
				ShowCurrentSCPage();
			}

			Logger.LogDebugInfo("ServiceZipcodeExcluded","dgServiceCodes_Delete","INF004","Deleted row in Service_Code table..");
			ViewState["SCOperation"] = Operation.None;
			lblZSCEMessage.Text="";
			Utility.EnableButton(ref btnInsert,ButtonType.Insert,true,m_moduleAccessRights);
		}

		private void BindSCGrid()
		{
			dgServiceCodes.VirtualItemCount = System.Convert.ToInt32(m_sdsServiceCode.QueryResultMaxSize);

//			SessionDS tmpSession = (SessionDS)Session["SESSION_DS1"];
//			if(tmpSession.ds.Tables[0].Rows[0]["service_code"] != "")
//			{
//				dsServiceTmp = tmpSession;
//			}
//			else
//			{
//				dsServiceTmp = (SessionDS)m_sdsServiceCode;
//			}					
			
			dgServiceCodes.DataSource = m_sdsServiceCode.ds;
			dgServiceCodes.DataBind();
			Session["SESSION_DS1"] = m_sdsServiceCode;
		}

		private void AddRowInSCGrid()
		{
			SysDataMgrDAL.AddNewRowInServiceCodeDS(m_sdsServiceCode);
			Session["SESSION_DS1"] = m_sdsServiceCode;
		}

		protected void dgServiceCodes_Bound(object sender, DataGridItemEventArgs e)
		{
			if(e.Item.ItemIndex == -1)
			{
				return;
			}
			DropDownList ddlAutoManifest = (DropDownList)e.Item.FindControl("ddlAutoManifest");

			DataRow drSelected = m_sdsServiceCode.ds.Tables[0].Rows[e.Item.ItemIndex];
			if(drSelected.RowState == DataRowState.Deleted)
			{
				return;
			}
//			if(drSelected["automanifest"]!= System.DBNull.Value)
//			{
				if(ddlAutoManifest != null)
				{
					ddlAutoManifest.DataSource = LoadAutoManifestOptions();
					ddlAutoManifest.DataTextField = "Text";
					ddlAutoManifest.DataValueField = "StringValue";
					ddlAutoManifest.DataBind();
					//	string a = (drSelected["automanifest"] == null?"":drSelected["automanifest"]);
	
					String strType = (String)(drSelected["automanifest"] == DBNull.Value?"":drSelected["automanifest"]);//drSelected["automanifest"];


					ddlAutoManifest.SelectedIndex = ddlAutoManifest.Items.IndexOf(ddlAutoManifest.Items.FindByValue(strType));
				}
//			}

			msTextBox txtServiceCode = (msTextBox)e.Item.FindControl("txtServiceCode");

			int iOperation = (int) ViewState["SCOperation"];
			if(txtServiceCode != null && iOperation == (int)Operation.Update)
			{
				txtServiceCode.Enabled = false;
			}
		}

		protected void dgServiceCodes_PageChange(object sender, DataGridPageChangedEventArgs e)
		{
			dgServiceCodes.CurrentPageIndex = e.NewPageIndex;
			ShowCurrentSCPage();
			Logger.LogDebugInfo("ServiceZipcodeExcluded","dgServiceCodes_PageChange","INF009","On Page Change "+e.NewPageIndex);
		}

		private void ShowCurrentSCPage()
		{
			int iStartIndex = dgServiceCodes.CurrentPageIndex * dgServiceCodes.PageSize;
			DataSet dsQuery = (DataSet)ViewState["QUERY_DS"];
			m_sdsServiceCode = SysDataMgrDAL.GetServiceCodeDS(utility.GetAppID(),utility.GetEnterpriseID(),iStartIndex,dgServiceCodes.PageSize,dsQuery);
			int pgCnt = Convert.ToInt32((m_sdsServiceCode.QueryResultMaxSize - 1))/dgServiceCodes.PageSize;
			if(pgCnt < dgServiceCodes.CurrentPageIndex)
			{
				dgServiceCodes.CurrentPageIndex = System.Convert.ToInt32(pgCnt);
			}
			dgServiceCodes.SelectedIndex = -1;
			dgServiceCodes.EditItemIndex = -1;
			lblSCMessage.Text = "";

			BindSCGrid();
			ResetDetailsGrid();
		}


		/// ZSE Grid Methos..
		/// 
	
		private void btnZSEInsert_Click(object sender, System.EventArgs e)
		{
//			//If coming to any other mode to Insert Mode then create the empty data set.
//
//			if((int)ViewState["ZSEMode"] != (int)ScreenMode.Insert || m_sdsZipcodeServiceExcluded.ds.Tables[0].Rows.Count >= dgZipcodeServiceExcluded.PageSize)
//			{
//				m_sdsZipcodeServiceExcluded = SysDataMgrDAL.GetEmptyZipcodeServiceExcludedDS(0);
//			}
//
//			AddRowInZSEGrid();	
//			dgZipcodeServiceExcluded.EditItemIndex = m_sdsZipcodeServiceExcluded.ds.Tables[0].Rows.Count - 1;
//			dgZipcodeServiceExcluded.CurrentPageIndex = 0;
//			Logger.LogDebugInfo("ServiceZipcodeExcluded","btnZSEInsert_Click","INF003","Data Grid Items count"+dgZipcodeServiceExcluded.Items.Count);
//			BindZSEGrid();
//			ViewState["ZSEMode"] = ScreenMode.Insert;
//			ViewState["ZSEOperation"] = Operation.Insert;
//			btnZSEInsert.Enabled = false;
//			btnZSEInsertMultiple.Enabled = false;
			String strServiceCode	= (String)ViewState["CurrentSC"];				
			String sUrl = "AgentZipCodePopup.aspx?FORMID=ServiceZipcodeExcluded&SERVICECODE="+strServiceCode;
			ArrayList paramList = new ArrayList();
			paramList.Add(sUrl);
			String sScript = Utility.GetScript("openParentWindow.js",paramList);
			Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
		}

		protected void dgZipcodeServiceExcluded_Edit(object sender, DataGridCommandEventArgs e)
		{
			dgZipcodeServiceExcluded.EditItemIndex = e.Item.ItemIndex;
			ViewState["ZSEOperation"] = Operation.Update;
			BindZSEGrid();
			lblZSCEMessage.Text = "";
			Logger.LogDebugInfo("ServiceZipcodeExcluded","dgZipcodeServiceExcluded_Edit","INF004","updating data grid...");			
		}

		public void dgZipcodeServiceExcluded_Update(object sender, DataGridCommandEventArgs e)
		{
			FillZSEDataRow(e.Item,e.Item.ItemIndex);

			int iOperation = (int)ViewState["ZSEOperation"];

			String strServiceCode = (String)ViewState["CurrentSC"];
			switch(iOperation)
			{
				case (int)Operation.Insert:

					DataSet dsToInsert = m_sdsZipcodeServiceExcluded.ds.GetChanges();
					try
					{
						SysDataMgrDAL.AddZipcodeServiceExcludedDS(utility.GetAppID(),utility.GetEnterpriseID(),strServiceCode,dsToInsert);
						lblZSCEMessage.Text=Utility.GetLanguageText(ResourceType.UserMessage,"INS_SUCCESSFULLY",utility.GetUserCulture());
						btnZSEInsert.Enabled = true;
						btnZSEInsertMultiple.Enabled = true;
					}
					catch(ApplicationException appException)
					{
						String strMsg = appException.Message;
						strMsg = appException.InnerException.Message;
						strMsg = appException.InnerException.InnerException.Message;
						if(strMsg.IndexOf("duplicate key") != -1 || strMsg.IndexOf("PRIMARY KEY") != -1)
						{
							lblZSCEMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"DUP_KEY_FOUND",utility.GetUserCulture());//"Duplicate Zipcode Code is not allowed";
						}
						else if(strMsg.IndexOf("FOREIGN KEY") != -1 || strMsg.IndexOf("parent key") != -1)
						{
							lblZSCEMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"NO_MASTER_RECORD_FOUND",utility.GetUserCulture()); //NO_MASTER_RECORD_FOUND "Zipcode not found. Please create Zipcode in Zipcode Master.";
						}
						else if(strMsg.IndexOf("unique") != -1 )
						{
							//Unique Key violed 
							lblZSCEMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"DUP_KEY_FOUND",utility.GetUserCulture());
						}						
						return;
						
					}

					m_sdsZipcodeServiceExcluded.ds.Tables[0].Rows[e.Item.ItemIndex].AcceptChanges();
					Logger.LogDebugInfo("ServiceZipcodeExcluded","dgZipcodeServiceExcluded_OnUpdate","INF004","update in Insert mode");
					break;

			}
			dgZipcodeServiceExcluded.EditItemIndex = -1;
			ViewState["ZSEOperation"] = Operation.None;
			BindZSEGrid();		
			lblSCMessage.Text = "";
		}

		protected void dgZipcodeServiceExcluded_Cancel(object sender, DataGridCommandEventArgs e)
		{
			dgZipcodeServiceExcluded.EditItemIndex = -1;
			if((int)ViewState["ZSEMode"] == (int)ScreenMode.Insert && (int)ViewState["ZSEOperation"] == (int)Operation.Insert)
			{
				m_sdsZipcodeServiceExcluded.ds.Tables[0].Rows.RemoveAt(e.Item.ItemIndex);
				m_sdsZipcodeServiceExcluded.QueryResultMaxSize--;
				dgZipcodeServiceExcluded.CurrentPageIndex = 0;
			}
			btnZSEInsert.Enabled = true;
			btnZSEInsertMultiple.Enabled = true;
			lblZSCEMessage.Text = "";

			ViewState["ZSEOperation"] = Operation.None;
			BindZSEGrid();
			Logger.LogDebugInfo("ServiceZipcodeExcluded","dgZipcodeServiceExcluded_Cancel","INF004","updating data grid...");			
		}

		protected void dgZipcodeServiceExcluded_Delete(object sender, DataGridCommandEventArgs e)
		{
			msTextBox txtZipcode = (msTextBox)e.Item.FindControl("txtZipcode");
			Label lblZipcode = (Label)e.Item.FindControl("lblZipcode");
			String strZipcode = null;
			if(txtZipcode != null)
				strZipcode = txtZipcode.Text;
			if(lblZipcode != null)
				strZipcode = lblZipcode.Text;

			String strServiceCode = (String)ViewState["CurrentSC"];
			try
			{
				SysDataMgrDAL.DeleteZipcodeServiceExcludedDS(utility.GetAppID(),utility.GetEnterpriseID(),strServiceCode,strZipcode);
				lblZSCEMessage.Text=Utility.GetLanguageText(ResourceType.UserMessage,"DLD_SUCCESSFULLY",utility.GetUserCulture());
			}
			catch(ApplicationException appException)
			{
				String strMsg = appException.Message;
				if(strMsg.IndexOf("FK") != -1)
				{
					lblZSCEMessage.Text =Utility.GetLanguageText(ResourceType.UserMessage,"ERROR",utility.GetUserCulture());
				}
				return;
			}

			if((int)ViewState["ZSEMode"] == (int)ScreenMode.Insert)
			{
				m_sdsZipcodeServiceExcluded.ds.Tables[0].Rows.RemoveAt(e.Item.ItemIndex);
				m_sdsZipcodeServiceExcluded.QueryResultMaxSize--;
				dgZipcodeServiceExcluded.CurrentPageIndex = 0;

				if((int) ViewState["ZSEOperation"] == (int)Operation.Insert && dgZipcodeServiceExcluded.EditItemIndex > 0)
				{
					m_sdsZipcodeServiceExcluded.ds.Tables[0].Rows.RemoveAt(dgZipcodeServiceExcluded.EditItemIndex-1);
					m_sdsZipcodeServiceExcluded.QueryResultMaxSize--;
				}
				dgZipcodeServiceExcluded.EditItemIndex = -1;
				BindZSEGrid();
			}
			else
			{
				ShowCurrentZSEPage();
			}

			Logger.LogDebugInfo("ServiceZipcodeExcluded","dgZipcodeServiceExcluded_Delete","INF004","Deleted row in Zipcode_service_Excluded table..");

			ViewState["ZSEOperation"] = Operation.None;
			btnZSEInsert.Enabled = true;
			btnZSEInsertMultiple.Enabled = true;
			lblSCMessage.Text = "";
		}

		private void BindZSEGrid()
		{
			dgZipcodeServiceExcluded.VirtualItemCount = System.Convert.ToInt32(m_sdsZipcodeServiceExcluded.QueryResultMaxSize);
			dgZipcodeServiceExcluded.DataSource = m_sdsZipcodeServiceExcluded.ds;
			dgZipcodeServiceExcluded.DataBind();
			Session["SESSION_DS2"] = m_sdsZipcodeServiceExcluded;
		}

		private void AddRowInZSEGrid()
		{
			SysDataMgrDAL.AddNewRowInZipcodeServiceExcludedDS(m_sdsZipcodeServiceExcluded);
			Session["SESSION_DS2"] = m_sdsZipcodeServiceExcluded;
		}

		public void dgZipcodeServiceExcluded_Button(object sender, DataGridCommandEventArgs e)
		{
			String strCmdNm = e.CommandName;
			if(strCmdNm.Equals("search"))
			{
				TextBox txtZipcode = (TextBox)e.Item.FindControl("txtZipcode");
				TextBox txtState = (TextBox)e.Item.FindControl("txtState");
				TextBox txtCountry = (TextBox)e.Item.FindControl("txtCountry");

				String strZipcodeClientID = null;
				String strStateClientID = null;
				String strCountryClientID = null;

				String strZipcode = null;
				
				if(txtZipcode != null)
				{
					strZipcodeClientID = txtZipcode.ClientID;
					strZipcode = txtZipcode.Text;
				}

				if(txtState != null)
				{
					strStateClientID = txtState.ClientID;
				}
				
				if(txtCountry != null)
				{
					strCountryClientID = txtCountry.ClientID;
				}


				String sUrl = "ZipcodePopup.aspx?FORMID=ServiceZipcodeExcluded&ZIPCODE="+strZipcode+"&ZIPCODE_CID="+strZipcodeClientID+"&STATE_CID="+strStateClientID+"&COUNTRY_CID="+strCountryClientID;
				ArrayList paramList = new ArrayList();
				paramList.Add(sUrl);
				String sScript = Utility.GetScript("openWindowParam.js",paramList);
				Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
			}
			
		}

		protected void dgZipcodeServiceExcluded_Bound(object sender, DataGridItemEventArgs e)
		{
			if(e.Item.ItemIndex == -1)
			{
				return;
			}

			DataRow drSelected = m_sdsZipcodeServiceExcluded.ds.Tables[0].Rows[e.Item.ItemIndex];
			if(drSelected.RowState == DataRowState.Deleted)
			{
				return;
			}

			msTextBox txtZipcode = (msTextBox)e.Item.FindControl("txtZipcode");
			
			int iOperation = (int) ViewState["ZSEOperation"];
			if(txtZipcode != null && iOperation == (int)Operation.Update)
			{
				txtZipcode.Enabled = false;
			}

			if(e.Item.ItemType == ListItemType.EditItem)
			{
				e.Item.Cells[0].Enabled = true;
				e.Item.Cells[3].Enabled = true;

			}
			else
			{
				e.Item.Cells[0].Enabled = false;
				e.Item.Cells[3].Enabled = false;
			}
		}

		protected void dgZipcodeServiceExcluded_PageChange(object sender, DataGridPageChangedEventArgs e)
		{
			dgZipcodeServiceExcluded.CurrentPageIndex = e.NewPageIndex;
			ShowCurrentZSEPage();
			Logger.LogDebugInfo("ServiceZipcodeExcluded","dgZipcodeServiceExcluded_PageChange","INF009","On Page Change "+e.NewPageIndex);
		}

		private void ShowCurrentZSEPage()
		{
			int iStartIndex = dgZipcodeServiceExcluded.CurrentPageIndex * dgZipcodeServiceExcluded.PageSize;
			String strServiceCode = (String)ViewState["CurrentSC"];
			m_sdsZipcodeServiceExcluded = SysDataMgrDAL.GetZipcodeServiceExcludedDS(utility.GetAppID(),utility.GetEnterpriseID(),strServiceCode,iStartIndex,dgZipcodeServiceExcluded.PageSize);

			int pgCnt = Convert.ToInt32((m_sdsZipcodeServiceExcluded.QueryResultMaxSize - 1))/dgZipcodeServiceExcluded.PageSize;
			if(pgCnt < dgZipcodeServiceExcluded.CurrentPageIndex)
			{
				dgZipcodeServiceExcluded.CurrentPageIndex = System.Convert.ToInt32(pgCnt);
			}
			dgZipcodeServiceExcluded.EditItemIndex = -1;
			BindZSEGrid();			
		}

		private void ShowButtonColumns(bool bShow)
		{
			dgServiceCodes.Columns[0].Visible = bShow;
			dgServiceCodes.Columns[1].Visible = bShow;
			dgServiceCodes.Columns[2].Visible = bShow;
		}

		private void FillSCDataRow(DataGridItem item, int drIndex)
		{
			try
			{
				msTextBox txtServiceCode = (msTextBox)item.FindControl("txtServiceCode");
				if(txtServiceCode != null)
				{
					DataRow drCurrent = m_sdsServiceCode.ds.Tables[0].Rows[drIndex];
					drCurrent["service_code"] = txtServiceCode.Text;
				}

				TextBox txtServiceDescription = (TextBox)item.FindControl("txtServiceDescription");
				if(txtServiceDescription != null)
				{
					DataRow drCurrent = m_sdsServiceCode.ds.Tables[0].Rows[drIndex];
					drCurrent["service_description"] = txtServiceDescription.Text;
				}

				TextBox txtPercentDiscount = (TextBox)item.FindControl("txtPercentDiscount");
				if(txtPercentDiscount != null )
				{
					DataRow drCurrent = m_sdsServiceCode.ds.Tables[0].Rows[drIndex];
					if(txtPercentDiscount.Text.Trim() == "")
					{
						drCurrent["service_charge_percent"] = System.DBNull.Value;
					}
					else 
					{						
						try
						{					
							drCurrent["service_charge_percent"] = txtPercentDiscount.Text;
						}
						catch(Exception e)
						{
							String msg = e.ToString();
							drCurrent["service_charge_percent"] = "-101";						
						}						
					}
				}
				TextBox txtServiceAmt = (TextBox)item.FindControl("txtServiceAmt");
				if(txtServiceAmt != null )
				{							
					Session["old_service_charge_amt"] = m_sdsServiceCode.ds.Tables[0].Rows[drIndex]["service_charge_amt"];
					DataRow drCurrent = m_sdsServiceCode.ds.Tables[0].Rows[drIndex];
					if(txtServiceAmt.Text == "")
					{
						drCurrent["service_charge_amt"] = System.DBNull.Value;;
					}
					else 
					{						
						try
						{
							drCurrent["service_charge_amt"] = txtServiceAmt.Text; 
						}
						catch(Exception e)
						{
							String msg = e.ToString();
							drCurrent["service_charge_amt"] = "-101";
						}

					}
				}

				msTextBox txtCommitTime = (msTextBox)item.FindControl("txtCommitTime");
				if(txtCommitTime != null )
				{
					DataRow drCurrent = m_sdsServiceCode.ds.Tables[0].Rows[drIndex];
					if(txtCommitTime.Text == "")
					{
						drCurrent["commit_time"] = System.DBNull.Value;
					}
					else
					{
						drCurrent["commit_time"] = txtCommitTime.Text;
					}
				
				}

				msTextBox txtTransitDay = (msTextBox)item.FindControl("txtTransitDay");
				if(txtTransitDay != null)
				{
					DataRow drCurrent = m_sdsServiceCode.ds.Tables[0].Rows[drIndex];
					if(txtTransitDay.Text == "")
					{
						drCurrent["transit_day"]= System.DBNull.Value;
					}
					else
					{
						drCurrent["transit_day"] = txtTransitDay.Text;
					}
				
				}

				msTextBox txtTransitHour = (msTextBox)item.FindControl("txtTransitHour");
				if(txtTransitHour != null)
				{
					DataRow drCurrent = m_sdsServiceCode.ds.Tables[0].Rows[drIndex];
					if(txtTransitHour.Text == "")
					{
						drCurrent["transit_hour"] = System.DBNull.Value;
					}
					else
					{
						drCurrent["transit_hour"] = txtTransitHour.Text;
					}
				
				}

				DropDownList ddlAutoManifest = (DropDownList)item.FindControl("ddlAutoManifest");
				if(ddlAutoManifest != null)
				{
					DataRow drCurrent = m_sdsServiceCode.ds.Tables[0].Rows[drIndex];
					if(ddlAutoManifest.SelectedItem.Value == "")
					{
						drCurrent["automanifest"] = System.DBNull.Value;
					}
					else
					{
						drCurrent["automanifest"] = ddlAutoManifest.SelectedItem.Value;
					}
				
				}
			}
			catch(Exception appException)
			{
				Logger.LogTraceError("RBACManager","AddserviceCodeDS","SDM001","Error During Insert "+ appException.Message.ToString());
				throw new ApplicationException("Error inserting service Code ",appException);				
			}


		}

		private void FillZSEDataRow(DataGridItem item, int drIndex)
		{
			msTextBox txtZipcode = (msTextBox)item.FindControl("txtZipcode");
			if(txtZipcode != null)
			{
				DataRow drCurrent = m_sdsZipcodeServiceExcluded.ds.Tables[0].Rows[drIndex];
				drCurrent["zipcode"] = txtZipcode.Text;
			}

			TextBox txtState = (TextBox)item.FindControl("txtState");
			if(txtState != null)
			{
				DataRow drCurrent = m_sdsZipcodeServiceExcluded.ds.Tables[0].Rows[drIndex];
				drCurrent["state_name"] = txtState.Text;
			}

			TextBox txtCountry = (TextBox)item.FindControl("txtCountry");
			if(txtCountry != null)
			{
				DataRow drCurrent = m_sdsZipcodeServiceExcluded.ds.Tables[0].Rows[drIndex];
				drCurrent["country"] = txtCountry.Text;
			}

		}

		private void ResetDetailsGrid()
		{
			dgZipcodeServiceExcluded.CurrentPageIndex = 0;
			btnZSEInsert.Enabled = false;	
			btnZSEInsertMultiple.Enabled = false;
			
			m_sdsZipcodeServiceExcluded = SysDataMgrDAL.GetEmptyZipcodeServiceExcludedDS(0);
			Session["SESSION_DS2"] = m_sdsZipcodeServiceExcluded;
			lblZSCEMessage.Text = "";
			BindZSEGrid();

		}

		private void btnZSEInsertMultiple_Click(object sender, System.EventArgs e)
		{
			btnZSEInsert.Enabled = false;	
			btnZSEInsertMultiple.Enabled = false;
			dgZipcodeServiceExcluded.CurrentPageIndex = 0;
		}

		private int CheckTransit(int drIndex)
		{
			int intRet = 1;
			int intTHour = 0;
			int intTDay = 0;
			int intChargeP=0;
			int intChargeD=0;
			String strTime = null;
			DataRow drCurrent = m_sdsServiceCode.ds.Tables[0].Rows[drIndex];

			if(Utility.IsNotDBNull(drCurrent["transit_day"]) && drCurrent["transit_day"].ToString() !="")
			{
				intTDay = System.Convert.ToInt32(drCurrent["transit_day"]);
			}
			if(Utility.IsNotDBNull(drCurrent["transit_hour"]) && drCurrent["transit_hour"].ToString() !="")
			{
				intTHour = System.Convert.ToInt32(drCurrent["transit_hour"]);
			}
			if(Utility.IsNotDBNull(drCurrent["commit_time"]) && drCurrent["commit_time"].ToString() !="")
			{
				DateTime dtCommitTime = (DateTime) drCurrent["commit_time"];
				strTime = dtCommitTime.Hour.ToString() + dtCommitTime.Minute.ToString();
			}

			if ((intTDay == 0) && (intTHour == 0))
			{
				lblSCMessage.Text =Utility.GetLanguageText(ResourceType.UserMessage,"PLS_ENTER_TRANSIT_DT",utility.GetUserCulture());
				intRet = -1;
				return -1;
			}
			else if ((intTDay > 0) && (intTHour > 0))
			{
				lblSCMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"PLS_ENTER_TRANSIT_DT",utility.GetUserCulture());
				intRet = -1;
				return -1;
			}
				else if ((intTDay > 0) && ((strTime == null) || strTime == "00"))
			{
				lblSCMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"PLS_ENTER_COMMIT_TIME",utility.GetUserCulture());
				intRet = -1;
				return -1;
			}
			if(Utility.IsNotDBNull(drCurrent["service_charge_percent"]) && drCurrent["service_charge_percent"].ToString() !="")
			{
				intChargeP = System.Convert.ToInt32(drCurrent["service_charge_percent"]);
			}
			if(Utility.IsNotDBNull(drCurrent["service_charge_amt"]) && drCurrent["service_charge_amt"].ToString() !="")
			{
				intChargeD = System.Convert.ToInt32(drCurrent["service_charge_amt"]);
			}
			if ((intChargeD == 0) && (intChargeP == 0))
			{
				drCurrent["service_charge_amt"] = Session["old_service_charge_amt"]; 
				Session["old_service_charge_amt"] = null;

				lblSCMessage.Text= Utility.GetLanguageText(ResourceType.UserMessage,"PLS_ENTER_SERVICE_CHR",utility.GetUserCulture());
				intRet = -1;
				return -1;
			}
			if((intChargeD != 0) && (intChargeP != 0))
			{
				lblSCMessage.Text= Utility.GetLanguageText(ResourceType.UserMessage,"PLS_ENTER_SERVICE_CHR_EITHER",utility.GetUserCulture());
				intRet = -1;
			}
			return intRet;
		}

		private void btnSave_Click(object sender, System.EventArgs e)
		{
		
		}

		/// <summary>
		/// To populate options type in the drop down list
		/// </summary>
		/// <param name="showNilOption">to show an empty option or not</param>
		/// <returns></returns>
		private DataView CreateAutoManifestOptions(bool showNilOption) 
		{
			DataTable dtAutoManifestOptions = new DataTable();
 
			dtAutoManifestOptions.Columns.Add(new DataColumn("Text", typeof(string)));
			dtAutoManifestOptions.Columns.Add(new DataColumn("StringValue", typeof(string)));

			ArrayList AutoManifestOptionArray = Utility.GetCodeValues(m_strAppID,m_strCulture,"automanifest",CodeValueType.StringValue);

			if(showNilOption)
			{
				DataRow drNilRow = dtAutoManifestOptions.NewRow();
				drNilRow[0] = "";
				drNilRow[1] = "";
				dtAutoManifestOptions.Rows.Add(drNilRow);
			}
			foreach(SystemCode typeSysCode in AutoManifestOptionArray)
			{
				DataRow drEach = dtAutoManifestOptions.NewRow();
				drEach[0] = typeSysCode.Text;
				drEach[1] = typeSysCode.StringValue;
				dtAutoManifestOptions.Rows.Add(drEach);
			}

			DataView dvAutoManifestOptions = new DataView(dtAutoManifestOptions);
			return dvAutoManifestOptions;
		}
		/// <summary>
		/// To load the options
		/// </summary>
		/// <returns></returns>
		private ICollection LoadAutoManifestOptions()
		{
			return m_dvAutoManifestOptions;
		}
	}
}
