<%@ Import Namespace="com.common.util" %>
<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<%@ Page language="c#" Codebehind="PickupRequest_ConRate.aspx.cs" AutoEventWireup="false" Inherits="TIES.WebUI.PickupRequest_ConRate" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>PickupRequest_ConRate</title>
		<LINK href="css/Styles.css" type="text/css" rel="stylesheet" name="stylesheet">
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<script>
		<!-- Begin
// isDigit
//    c 01....9 ise true, yoksa false doner.
function isDigit(myInput){
	var test = '' + myInput;
	if (isNaN(parseInt(test)))
		{
		return false;
		}
	return true;
}

function isFloat(myInput){
	var test = '' + myInput;
	if (isNaN(parseFloat(test)))
		{
		return false;
		}
	return true;
}


function isValidNumber(myInput)
{
	if (isNaN(myInput))
		{
			return false;
		}
	return true;

}
// trim fonksiyonlari
// ltrim: left trim
// rtrim: right trim
// ntrim: her iki taraftan trim
function lTrim(myInput2){
 while(myInput2.indexOf(' ',0)==0){
  myInput2 = myInput2.substr(1);
 }
 return myInput2;
}
function rTrim(myInput1){
 if (myInput1.length==0)
  return '';
 while(myInput1.lastIndexOf(' ')==(myInput1.length-1)){
  myInput1 = myInput1.substr(0,myInput1.length-1);
 }
 return myInput1;
}
function nTrim(myInput3){
 myInput3 = rTrim(lTrim(myInput3));
 return myInput3;
}

function leapYear(Year){
	if(((Year % 4)==0) && ((Year % 100)!=0) || ((Year % 400)==0))
		return (1);
	else
		return (0);
}

function getDaysInMonth(month, year) {
	var days = 100;
	if(month==1 || month==3 || month==5 || month==7 || month==8 || month==10 || month==12)
		days=31;
	else if (month==4 || month==6 || month==9 || month==11)
		days=30;
	else if (month==2){
		if (leapYear (year)==1)
			days=29;
		else
			days=28;
	}
	//alert(days);
	return (days);
}

function setDate(myInput){
	if (myInput.value.length>0){
		myInput.value = nTrim(myInput.value);
		//alert(myInput.value);
		var error=0;
		var myDate='';
		var myMonth='';
		var myYear='';
		
		for(i=0; i<2 ; i++)
		{
			if(isDigit(myInput.value.substr(i,1)))
			{
				myDate=myDate + myInput.value.substr(i,1)
			}
			else
			{
				error=1;
			}
		}

		for(i=3; i<5 ; i++)
		{
			if(isDigit(myInput.value.substr(i,1)))
			{
				myMonth=myMonth + myInput.value.substr(i,1)
			}
			else
			{
				error=1;
			}
		}

		for(i=6; i<10; i++)
		{
			if(isDigit(myInput.value.substr(i,1)))
			{
				myYear=myYear + myInput.value.substr(i,1);
			}
			else
			{
				error=1;
			}
		}

		if(error==0)
		{
			if(parseInt(myDate) <= getDaysInMonth(myMonth, myYear))
			{
				if(parseInt(myMonth) <= 12)
				{
					if((parseInt(myYear) < 2100) && (parseInt(myYear) > 1999))
					{
					}
					else
					{
						error=4;
					}
				}
				else
				{
					error=3;
				}
			}
			else
			{
				error=2;
			}
		
			switch(error)
			{
				case 2:
					alert('Incorrect date in dd/mm/yyyy : '+myDate);
					myInput.value='';
					myInput.focus();
					return false;
				case 3:
					alert('Enter month between 1 to 12');
					myInput.value='';
					myInput.focus();
					return false;
				case 4:
					alert('Enter year between 2000 to 2099');
					myInput.value='';
					myInput.focus();
					return false;
				case 0:
					return true;
				default:
					return false;
			}
		}	
		else
		{
			alert('Incorrect date. Please enter correct date.');
			myInput.value='';
			myInput.focus();
			return false;
		}
	}
}

function setDateTime(myInput)
{
	if (myInput.value.length>0)
	{
		myInput.value = nTrim(myInput.value);
		//alert(myInput.value);
		var error=0;
		var myDate='';
		var myMonth='';
		var myYear='';
		var myHour='';
		var myMin='';
		
		for(i=0; i<2 ; i++)
		{
			if(isDigit(myInput.value.substr(i,1)))
			{
				myDate=myDate + myInput.value.substr(i,1)
			}
			else
			{
				error=1;
			}
		}

		for(i=3; i<5 ; i++)
		{
			if(isDigit(myInput.value.substr(i,1)))
			{
				myMonth=myMonth + myInput.value.substr(i,1)
			}
			else
			{
				error=1;
			}
		}

		for(i=6; i<10; i++)
		{
			if(isDigit(myInput.value.substr(i,1)))
			{
				myYear=myYear + myInput.value.substr(i,1);
			}
			else
			{
				error=1;
			}
		}
		for(i=11; i<13; i++)
		{
			if(isDigit(myInput.value.substr(i,1)))
			{
				myHour=myHour + myInput.value.substr(i,1);
			}
			else
			{
				error=1;
			}
		}
		for(i=14; i<16; i++)
		{
			if(isDigit(myInput.value.substr(i,1)))
			{
				myMin=myMin + myInput.value.substr(i,1);
			}
			else
			{
				error=1;
			}
		}
		

		if(error==0)
		{
			if(parseInt(myDate) <= getDaysInMonth(myMonth, myYear))
			{
				if(parseInt(myMonth) <= 12)
				{
					if((parseInt(myYear) < 2100) && (parseInt(myYear) > 1999))
					{
						if(parseInt(myHour) <= 23)
						{
							if(parseInt(myMin) <= 59)
							{
							
							}
							else
							{
								error=6;
							}
						}
						else
						{
							error=5;
						}
					}
					else
					{
						error=4;
					}
				}
				else
				{
					error=3;
				}
			}
			else
			{
				error=2;
			}
		
			switch(error)
			{
				case 2:
					alert('Incorrect date in dd/mm/yyyy : '+myDate);
					myInput.value='';
					myInput.focus();
					return false;
					
				case 3:
					alert('Enter month between 1 to 12');
					myInput.value='';
					myInput.focus();
					return false;
					
				case 4:
					alert('Enter year between 2000 to 2099');
					myInput.value='';
					myInput.focus();
					return false;
					
				case 5:
					alert('Enter hour between 0 to 23');
					myInput.value='';
					myInput.focus();
					return false;
					
				case 6:
					alert('Enter minute between 0 to 59');
					myInput.value='';
					myInput.focus();
					return false;
					

				case 0:
					return true;
				default:
					return false;
			}
		}	
		else
		{
			alert('Incorrect date. Please enter correct date.');
			myInput.value='';
			myInput.focus();
			return false;
		}
	}

}

function setElasticDateTime(myInput)
{
	if (myInput.value.length>0)
	{
		myInput.value = nTrim(myInput.value);
		//alert(myInput.value);
		var error=0;
		var myDate='';
		var myMonth='';
		var myYear='';
		var myHour='';
		var myMin='';
		
		for(i=0; i<2 ; i++)
		{
			if(isDigit(myInput.value.substr(i,1)))
			{
				myDate=myDate + myInput.value.substr(i,1)
			}
			else
			{
				error=1;
			}
		}

		for(i=3; i<5 ; i++)
		{
			if(isDigit(myInput.value.substr(i,1)))
			{
				myMonth=myMonth + myInput.value.substr(i,1)
			}
			else
			{
				error=1;
			}
		}

		for(i=6; i<10; i++)
		{
			if(isDigit(myInput.value.substr(i,1)))
			{
				myYear=myYear + myInput.value.substr(i,1);
			}
			else
			{
				error=1;
			}
		}
		
		if(myInput.value.length > 10)
		{
			for(i=11; i<13; i++)
			{
				if(isDigit(myInput.value.substr(i,1)))
				{
					myHour=myHour + myInput.value.substr(i,1);
				}
				else
				{
					error=1;
				}
			}
			for(i=14; i<16; i++)
			{
				if(isDigit(myInput.value.substr(i,1)))
				{
					myMin=myMin + myInput.value.substr(i,1);
				}
				else
				{
					error=1;
				}
			}
		}
		
		if(error==0)
		{
			if(parseInt(myDate) <= getDaysInMonth(myMonth, myYear))
			{
				if(parseInt(myMonth) <= 12)
				{
					if((parseInt(myYear) < 2100) && (parseInt(myYear) > 1999))
					{
						if(myInput.value.length > 10)
						{
							if(parseInt(myHour) <= 23)
							{
								if(parseInt(myMin) <= 59)
								{
								
								}
								else
								{
									error=6;
								}
							}
							else
							{
								error=5;
							}
						}
					}
					else
					{
						error=4;
					}
				}
				else
				{
					error=3;
				}
			}
			else
			{
				error=2;
			}
		
			switch(error)
			{
				case 2:
					alert('Incorrect date in dd/mm/yyyy : '+myDate);
					myInput.value='';
					myInput.focus();
					return false;
					
				case 3:
					alert('Enter month between 1 to 12');
					myInput.value='';
					myInput.focus();
					return false;
					
				case 4:
					alert('Enter year between 2000 to 2099');
					myInput.value='';
					myInput.focus();
					return false;
					
				case 5:
					alert('Enter hour between 0 to 23');
					myInput.value='';
					myInput.focus();
					return false;
					
				case 6:
					alert('Enter minute between 0 to 59');
					myInput.value='';
					myInput.focus();
					return false;
					

				case 0:
					return true;
				default:
					return false;
			}
		}	
		else
		{
			alert('Incorrect date. Please enter correct date.');
			myInput.value='';
			myInput.focus();
			return false;
		}
	}

}

function setTime(myInput)
{
	if (myInput.value.length>0)
	{
		myInput.value = nTrim(myInput.value);
		//alert(myInput.value);
		var error=0;
		var myHour='';
		var myMin='';
		
		for(i=0; i<2; i++)
		{
			if(isDigit(myInput.value.substr(i,1)))
			{
				myHour=myHour + myInput.value.substr(i,1);
			}
			else
			{
				error=1;
			}
		}
		for(i=3; i<5; i++)
		{
			if(isDigit(myInput.value.substr(i,1)))
			{
				myMin=myMin + myInput.value.substr(i,1);
			}
			else
			{
				error=1;
			}
		}
		

		if(error==0)
		{
			if(parseInt(myHour) <= 23)
			{
				if(parseInt(myMin) <= 59)
				{
				
				}
				else
				{
					error=6;
				}
			}
			else
			{
				error=5;
			}

			switch(error)
			{
				case 5:
					alert('Enter hour between 0 to 23');
					myInput.value='';
					myInput.focus();
					return false;
				case 6:
					alert('Enter minute between 0 to 59');
					myInput.value='';
					myInput.focus();
					return false;

				case 0:
					return true;
				default:
					return false;
			}
		}	
		else
		{
			alert('Incorrect time. Please enter correct time.');
			myInput.value='';
			myInput.focus();
			return false;			
		}
	}

}

// Gordion mod�lleri
function CheckMask( toField, tcMask )
{
	var i;
	var lcVal = toField.value;
	var llValOK = true;
	
	if (tcMask.length == 0)
		return true;
		
	for (i=0; i<lcVal.length && llValOK; i++)
	{
		lcMaskChar = tcMask.charAt(i);
		lcValChar  = lcVal.charAt(i);
		switch (lcMaskChar)
		{
			case '9':  // Say�sal
				llValOK = (lcValChar >= '0' && lcValChar <= '9') ;
				break;			
			case 'X':  // Herhangi
				llValOK = true;
				break;			
			case '!':  // B�y�k harf
				llValOK = (lcValChar >= 'A' && lcValChar <= 'Z') || (lcValChar >= '0' && lcValChar <= '9') || (lcValChar = '%' ) || (lcValChar = '*' );
				break;
			default:   // masktaki harf
				llValOK = lcValChar == lcMaskChar;
		}
	}
	
	if (!llValOK)
		toField.value = "";
	
	return llValOK;
}

function InputMask( toField, tcMask )
{
	var lcMaskChar, lcNewChar = String.fromCharCode(window.event.keyCode);
	var llRetVal = true;
	
	if (tcMask.length == 0)
		return true;
		
	if (toField.value.length >= tcMask.length)  // InputMask tamam. Art�k kabul etme.
	{
		llRetVal = false;
	}
	else                                        // Mask� yeni gelen harfe g�re parse et
	{
		lcMaskChar = tcMask.charAt(toField.value.length);
		switch (lcMaskChar)
		{
			case '9':  // Say�sal
				llRetVal = (lcNewChar >= '0' && lcNewChar <= '9') ;
				break;
			case 'X':  // Herhangi
				break;
			case '!':  // B�y�k harf
				window.event.keyCode = lcNewChar.toUpperCase().charCodeAt(0);
				break;			         
			default:   // masktaki harf
				toField.value += lcMaskChar;
				llRetVal = InputMask(toField, tcMask);
		}
	}
	
	return llRetVal;
}

function formatInt ( ctrl )
{
	var separator = ",";
	var intm = ctrl.value.replace ( new RegExp ( separator, "g" ), "" );
	var regexp = new RegExp ( "\\B(\\d{3})(" + separator + "|$)" );
	do
	{
		intm = intm.replace ( regexp, separator + "$1" );
	}
	while ( intm.search ( regexp ) >= 0 )
	ctrl.value = intm;
}

function PriceMask( toField )
{
	var lcNewChar = String.fromCharCode(window.event.keyCode);
	var llRetVal = false;
	
	llRetVal = ((lcNewChar >= '0' && lcNewChar <= '9') || (lcNewChar <='.') || (lcNewChar <=',') ) ;
	
	return llRetVal;
}

function NumberMask(toField,precision,scale,minValue,maxValue)
{
	var lcNewChar = String.fromCharCode(window.event.keyCode);
	var llRetVal = false;
	var lmRetVal = false;
	//alert("value is : "+toField.value+" precision " + precision +" scale is : "+scale+" minValue : "+minValue+" maxValue : "+maxValue);
	
	var isDotPresent = false;
	
	var currentNum = toField.value;
	
	var currentScale = 0;
	
	for(i = 0; i < currentNum.length; i++)
	{
		if(currentNum.charAt(i) == '.')
		{
			isDotPresent = true;
		}
		if(isDotPresent)
		{
			if(currentNum.charAt(i) != '.')
			{
				currentScale++;
			}
		}
	}
	
	//alert("currentScale : "+currentScale+" scale is : "+scale);
	
	if((minValue < 0 && toField.value.length ==0 && lcNewChar =='-')||(minValue < 0 && toField.value.length > 0 && lcNewChar =='-'))
	{
		if(toField.value.length > 0)
		{
			llRetVal=true;
			if(llRetVal)
			{
				var newNum =lcNewChar + toField.value;
				//alert(newNum);
				var newFloatNum = parseFloat(newNum);
				//alert("New Num : "+ newNum+"New Float Num : "+newFloatNum);
				if(newFloatNum <= maxValue && newFloatNum >= minValue)
				{
					llRetVal = true;
				}
				else
				{
					llRetVal = false;
				}
				
			}
		}
		else
			llRetVal=true;
	}
	else
	{
			if(isDotPresent)
			{
				llRetVal = ((lcNewChar >= '0' && lcNewChar <= '9') && (toField.value.length <= precision) && (currentScale <= scale)) ;
			}
			else
			{
				llRetVal = (((lcNewChar >= '0' && lcNewChar <= '9') || ((lcNewChar == '.') && (scale > 0))) && (toField.value.length < precision)) ;	
			}
			
			
			if(llRetVal)
			{
				var newNum = ''+ toField.value + lcNewChar;
				//alert(newNum);
				var newFloatNum = parseFloat(newNum);
				//alert("New Num : "+ newNum+"New Float Num : "+newFloatNum);
				if(newFloatNum <= maxValue && newFloatNum >= minValue)
				{
					llRetVal = true;
				}
				else
				{
					llRetVal = false;
				}
				
			}
	}
	
	return llRetVal;
}


function NumberMaskCust(toField,precision,scale,minValue,maxValue)
{
	var lcNewChar = String.fromCharCode(window.event.keyCode);
	var llRetVal = false;
	var lmRetVal = false;
	//alert("value is : "+toField.value+" enterd:"+lcNewChar+" precision " + precision +" scale is : "+scale+" minValue : "+minValue+" maxValue : "+maxValue);
	
	if(toField.value=='0'&&toField.value+lcNewChar!='0.')
		return false;
		
	var isDotPresent = false;
	
	var currentNum = toField.value;
	
	var currentScale = 0;
	
	for(i = 0; i < currentNum.length; i++)
	{
		if(currentNum.charAt(i) == '.')
		{
			isDotPresent = true;
		}
		if(isDotPresent)
		{
			currentScale++;	
			if(isDotPresent&&(parseFloat(toField.value+lcNewChar)< parseFloat(minValue))&&currentScale>=scale)
				return false;		
		}
	}
	
	//alert("currentScale : "+currentScale+" scale is : "+scale);
	
	if((parseFloat(minValue) < parseFloat(0.00) && toField.value.length ==0 && lcNewChar =='-')||(parseFloat(minValue) < parseFloat(0.00) && toField.value.length > 0 && lcNewChar =='-'))
	{
		if(toField.value.length > 0)
		{
			llRetVal=true;
			if(llRetVal)
			{
				var newNum =lcNewChar + toField.value;
				//alert(newNum);
				var newFloatNum = parseFloat(newNum);
				//alert("New Num : "+ newNum+"New Float Num : "+newFloatNum);
				if(newFloatNum <= maxValue && newFloatNum >= minValue)
				{
					llRetVal = true;
				}
				else
				{
					llRetVal = false;
				}
				
			}
		}
		else
			llRetVal=true;
	}
	else
	{
			if(isDotPresent)
			{
				llRetVal = ((lcNewChar >= '0' && lcNewChar <= '9') && (toField.value.length <= precision) && (currentScale <= scale)) ;
			}
			else//IF it has no dot but 0 leads
			{
				llRetVal = (((lcNewChar >= '0' && lcNewChar <= '9') || ((lcNewChar == '.') && (scale >0 ))) && (toField.value.length < precision)) ;	
			}
			
			
			if(llRetVal)
			{
				var newNum = ''+ toField.value + lcNewChar;
				//alert(newNum);
				var newFloatNum = parseFloat(newNum);
				//alert("New Num : "+ newNum+"New Float Num : "+newFloatNum);
				if(newFloatNum <= maxValue && newFloatNum >= 0) // minValue)
				{
					llRetVal = true;
				}
				else
				{
					llRetVal = false;
				}
				
			}
	}
	
	return llRetVal;
}

function TrimNumberCust(toField,scale)
{
	var newNum = ''+ toField.value;
	var newFloatNum = parseFloat(newNum);
	//alert("New Number : "+newNum+" New Float Number : "+newFloatNum);
	if(!isNaN(newFloatNum))
	{
		toField.value = newFloatNum;
		
		if(scale > 0)
		{
			var currentNum = toField.value;
			var isDotPresent = false;

			for(i = 0; i < currentNum.length; i++)
			{
				if(currentNum.charAt(i) == '.')
				{
					isDotPresent = true;
					break;
				}
			}
			
			if(isDotPresent == false)
			{
				toField.value = toField.value + ".";
				
				for(i = 0; i < scale; i++)
				{
					toField.value = toField.value + '0';
				}
			}
		
		}
		
	}
	
}

function TrimNumber(toField,scale)
{
	var newNum = ''+ toField.value;
	var newFloatNum = parseFloat(newNum);
	//alert("New Number : "+newNum+" New Float Number : "+newFloatNum);

	if(!isNaN(newFloatNum))
	{
		toField.value = newFloatNum;
		
		if(scale > 0)
		{
			var currentNum = toField.value;
			var isDotPresent = false;

			for(i = 0; i < currentNum.length; i++)
			{
				if(currentNum.charAt(i) == '.')
				{
					isDotPresent = true;
					break;
				}
			}
			
			if(isDotPresent == false)
			{
				toField.value = toField.value + ".";
				
				for(i = 0; i < scale; i++)
				{
					toField.value = toField.value + '0';
				}
			}
		
		}
		
	}
	
}



function UpperMask( toField )
{
	var lcNewChar = String.fromCharCode(window.event.keyCode);
	var llRetVal = true;
	window.event.keyCode = lcNewChar.toUpperCase().charCodeAt(0);
	return llRetVal;
}

function UpperMaskSpecial( toField )
{
	var lcNewChar = String.fromCharCode(window.event.keyCode);
	var llRetVal = ((lcNewChar >= '0' && lcNewChar <= '9')||(lcNewChar >= 'A' && lcNewChar <= 'Z') || (lcNewChar >= 'a' && lcNewChar <= 'z') || (lcNewChar == '_') || (lcNewChar == '%'));
	window.event.keyCode = lcNewChar.toUpperCase().charCodeAt(0);
	return llRetVal;
}




function LowerMask( toField )
{
	var lcNewChar = String.fromCharCode(window.event.keyCode);
	var llRetVal = true;
	window.event.keyCode = lcNewChar.toLowerCase().charCodeAt(0);
	return llRetVal;
}

function LowerMaskSpecial( toField )
{
	var lcNewChar = String.fromCharCode(window.event.keyCode);
	var llRetVal = ((lcNewChar >= '0' && lcNewChar <= '9')||(lcNewChar >= 'A' && lcNewChar <= 'Z') || (lcNewChar >= 'a' && lcNewChar <= 'z') || (lcNewChar == '_') || (lcNewChar == '%'));
	window.event.keyCode = lcNewChar.toLowerCase().charCodeAt(0);
	return llRetVal;
}

function AlfaNumericMaskSpecial(toField)
{
	var lcNewChar = String.fromCharCode(window.event.keyCode);
	var llRetVal = ((lcNewChar >= '0' && lcNewChar <= '9')||(lcNewChar >= 'A' && lcNewChar <= 'Z') || (lcNewChar >= 'a' && lcNewChar <= 'z') || (lcNewChar == '_') || (lcNewChar == '%'));
	return llRetVal;
}

function AlfaNumericSpaceMaskSpecial(toField)
{
	var lcNewChar = String.fromCharCode(window.event.keyCode);
	var llRetVal = ((lcNewChar >= '0' && lcNewChar <= '9')||(lcNewChar >= 'A' && lcNewChar <= 'Z') || (lcNewChar >= 'a' && lcNewChar <= 'z') || (lcNewChar == ' ') || (lcNewChar == '%'));
	return llRetVal;
}

function round (theControl, howManyDigits) 
	{
		var n=theControl.value;
		n = n - 0;
		howManyDigits = howManyDigits || 2;
		var f = Math.pow(10, howManyDigits);
		n = Math.round(n * f) / f;
		n += Math.pow(10, - (howManyDigits + 1));
		n += '';
		if (howManyDigits == 0)
		{
			theControl.value=n.substring(0, n.indexOf('.')) 
		}
		else
		{
			theControl.value=n.substring(0, n.indexOf('.') + howManyDigits + 1);
		}
}

//  End -->
		</script>
	</HEAD>
	<body leftMargin="5" MS_POSITIONING="GridLayout">
		<form id="PackageDetails" method="post" runat="server">
			<DIV id="divMain" style="Z-INDEX: 102; LEFT: -1px; WIDTH: 1319px; POSITION: relative; TOP: 46px; HEIGHT: 900px"
				MS_POSITIONING="GridLayout" runat="server">
				<table id="Table1" style="Z-INDEX: 101; LEFT: 5px; WIDTH: 1000px; POSITION: absolute; TOP: 25px; HEIGHT: 264px"
					height="264" width="1000" border="0" runat="server">
					<tr height="20%">
						<td style="HEIGHT: 2.72%" width="25%">
							<DIV style="WIDTH: 958px; POSITION: relative; HEIGHT: 48px" ms_positioning="GridLayout"><FONT face="Tahoma"><asp:label id="Label1" style="Z-INDEX: 101; LEFT: 0px; POSITION: absolute; TOP: 0px" runat="server"
										CssClass="mainTitleSize">Pickup Requests / Consignment Rating</asp:label><asp:label id="lblErrorMsg" style="Z-INDEX: 102; LEFT: 8px; POSITION: absolute; TOP: 24px"
										runat="server" ForeColor="Red"></asp:label></FONT></DIV>
							<asp:label id="Label2" runat="server" CssClass="tableHeadingFieldset" Font-Size="13px">Consignments</asp:label><FONT face="Tahoma">&nbsp;
							</FONT>
							<asp:button id="btnOK" runat="server" CssClass="queryButton" Width="72px" Text="OK"></asp:button><FONT face="Tahoma">&nbsp;
								<asp:button id="btnCancel" runat="server" CssClass="queryButton" Width="72px" Text="Cancel"></asp:button>&nbsp;
								<asp:button id="btnInsertConsignment" runat="server" CssClass="queryButton" Width="66px" Text="Insert"
									CausesValidation="False"></asp:button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
								&nbsp;
								<asp:label id="Label3" runat="server" Font-Size="Smaller">Total</asp:label>&nbsp;&nbsp;
								<cc1:mstextbox id="txtHeader_TotalPkgs" style="TEXT-ALIGN: right" CssClass="textField" Width="55px"
									TextMaskType="msNumeric" Runat="server" Enabled="True" MaxLength="12" ReadOnly="True"></cc1:mstextbox>&nbsp;&nbsp;<cc1:mstextbox id="txtHeader_ActualWeight" style="TEXT-ALIGN: right" CssClass="textField" Width="55px"
									TextMaskType="msPrice" Runat="server" Enabled="True" MaxLength="12" ReadOnly="True" NumberPrecision="2" align="right"></cc1:mstextbox>&nbsp;&nbsp;<cc1:mstextbox id="txtHeader_DimWeight" style="TEXT-ALIGN: right" CssClass="textField" Width="55px"
									Runat="server" Enabled="True" MaxLength="12" ReadOnly="True" NumberPrecision="2" align="right"></cc1:mstextbox>&nbsp;&nbsp;<cc1:mstextbox id="txtHeader_ChgWeight" style="TEXT-ALIGN: right" CssClass="textField" Width="55px"
									Runat="server" Enabled="True" MaxLength="12" ReadOnly="True" NumberPrecision="2" align="right"></cc1:mstextbox>&nbsp;&nbsp;<cc1:mstextbox id="txtHeader_TotalCharge" style="TEXT-ALIGN: right" CssClass="textField" Width="55px"
									Runat="server" Enabled="True" MaxLength="12" ReadOnly="True" NumberPrecision="2" align="right"></cc1:mstextbox>&nbsp;&nbsp;<cc1:mstextbox id="txtHeader_FreightCharge" style="TEXT-ALIGN: right" CssClass="textField" Width="55px"
									Runat="server" Enabled="True" MaxLength="12" ReadOnly="True" NumberPrecision="2" align="right"></cc1:mstextbox>&nbsp;&nbsp;<cc1:mstextbox id="txtHeader_InsSurch" style="TEXT-ALIGN: right" CssClass="textField" Width="55px"
									Runat="server" Enabled="True" MaxLength="12" ReadOnly="True" NumberPrecision="2" align="right"></cc1:mstextbox>&nbsp;&nbsp;<cc1:mstextbox id="txtHeader_OtherSurch" style="TEXT-ALIGN: right" CssClass="textField" Width="55px"
									Runat="server" Enabled="True" MaxLength="12" ReadOnly="True" NumberPrecision="2" align="right"></cc1:mstextbox>&nbsp;&nbsp;<cc1:mstextbox id="txtHeader_VASSurch" style="TEXT-ALIGN: right" CssClass="textField" Width="55px"
									Runat="server" Enabled="True" MaxLength="12" ReadOnly="True" NumberPrecision="2" align="right"></cc1:mstextbox>&nbsp;&nbsp;<cc1:mstextbox id="txtHeader_ESASurch" style="TEXT-ALIGN: right" CssClass="textField" Width="55px"
									Text="{0:n}" Runat="server" Enabled="True" MaxLength="12" ReadOnly="True" NumberPrecision="2" align="right"></cc1:mstextbox>&nbsp;</FONT>
						</td>
					</tr>
					<tr>
						<td vAlign="top" height="100"><asp:datagrid id="dgConsignment" runat="server" Width="1220px" AllowPaging="True" PageSize="5"
								ItemStyle-Height="20" AutoGenerateColumns="False" OnSelectedIndexChanged="dgConsignment_SelectedIndexChanged" OnDeleteCommand="dgConsignment_Delete"
								OnUpdateCommand="dgConsignment_Update" OnCancelCommand="dgConsignment_Cancel" OnEditCommand="dgConsignment_Edit" OnPageIndexChanged="dgConsignment_PageChange"
								OnItemCommand="dgConsignment_Button">
								<SelectedItemStyle CssClass="gridFieldSelected"></SelectedItemStyle>
								<ItemStyle Height="20px" CssClass="gridField"></ItemStyle>
								<FooterStyle Wrap="False"></FooterStyle>
								<SelectedItemStyle Wrap="False"></SelectedItemStyle>
								<EditItemStyle Wrap="False"></EditItemStyle>
								<AlternatingItemStyle Wrap="False"></AlternatingItemStyle>
								<ItemStyle Wrap="False" Height="20px"></ItemStyle>
								<HeaderStyle Wrap="False"></HeaderStyle>
								<Columns>
									<asp:ButtonColumn Text="&lt;IMG SRC='images/butt-select.gif' border=0 &gt;" CommandName="Select">
										<HeaderStyle Width="1%" CssClass="gridHeading"></HeaderStyle>
										<ItemStyle Width="1%"></ItemStyle>
									</asp:ButtonColumn>
									<asp:ButtonColumn Text="&lt;IMG SRC='images/butt-delete.gif' border=0 title='Delete' &gt;" CommandName="Delete">
										<HeaderStyle Wrap="False" Width="4%" CssClass="gridHeading"></HeaderStyle>
										<ItemStyle Wrap="False"></ItemStyle>
										<FooterStyle Wrap="False"></FooterStyle>
									</asp:ButtonColumn>
									<asp:EditCommandColumn ButtonType="LinkButton" UpdateText="&lt;IMG SRC='images/butt-update.gif' border=0  title='Update' &gt;"
										CancelText="&lt;IMG SRC='images/butt-cancel.gif' border=0 title='Cancel'&gt;" EditText="&lt;IMG SRC='images/butt-edit.gif' border=0  title='Edit' &gt;">
										<HeaderStyle Wrap="False" Width="4%" CssClass="gridHeading"></HeaderStyle>
										<ItemStyle Wrap="False"></ItemStyle>
										<FooterStyle Wrap="False"></FooterStyle>
									</asp:EditCommandColumn>
									<asp:TemplateColumn HeaderText="Seq No">
										<HeaderStyle Wrap="False" Width="2%" CssClass="gridHeading"></HeaderStyle>
										<ItemStyle Wrap="False" HorizontalAlign="Left"></ItemStyle>
										<ItemTemplate>
											<asp:Label CssClass="gridLabel" ID="dgConsignmentlblSeqno" Text='<%#DataBinder.Eval(Container.DataItem,"ConsignmentSeqNo")%>' Runat="server">
											</asp:Label>
										</ItemTemplate>
										<FooterStyle Wrap="False"></FooterStyle>
										<EditItemTemplate>
											<cc1:mstextbox CssClass="gridTextBox" ID="dgConsignmenttxtSeqno" Text='<%#DataBinder.Eval(Container.DataItem,"ConsignmentSeqNo")%>' Runat="server" Enabled="False" MaxLength="12" TextMaskType="msUpperAlfaNumericWithUnderscore" >
											</cc1:mstextbox>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Recipient &lt;br&gt;Postal Code">
										<HeaderStyle Wrap="False" Width="6%" CssClass="gridHeading"></HeaderStyle>
										<ItemStyle Wrap="False"></ItemStyle>
										<ItemTemplate>
											<asp:Label CssClass="gridLabel" ID="dgConsignmentlblRecipientPostCode" Text='<%#DataBinder.Eval(Container.DataItem,"RecPostalCode")%>' Runat="server">
											</asp:Label>
										</ItemTemplate>
										<FooterStyle Wrap="False"></FooterStyle>
										<EditItemTemplate>
											<cc1:mstextbox CssClass="gridTextBox" ID="dgConsignmenttxtRecipientPostCode" AutoPostBack="true" OnTextChanged="txtRecipientPostCode_TextChange" Text='<%#DataBinder.Eval(Container.DataItem,"RecPostalCode")%>' Runat="server" NumberPrecision="5" NumberMinValue="1" NumberMaxValue="99999" TextMaskType="msNumeric" MaxLength="5">
											</cc1:mstextbox>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Service &lt;br&gt;Type">
										<HeaderStyle Wrap="False" Width="2%" CssClass="gridHeading"></HeaderStyle>
										<ItemStyle Wrap="False"></ItemStyle>
										<ItemTemplate>
											<asp:Label CssClass="gridLabel" ID="dgConsignmentlblServiceType" Text='<%#DataBinder.Eval(Container.DataItem,"ServiceType")%>' Runat="server" Enabled="True">
											</asp:Label>
										</ItemTemplate>
										<FooterStyle Wrap="False"></FooterStyle>
										<EditItemTemplate>
											<asp:DropDownList ID="dgConsignmentddlServiceType" CssClass="gridTextBox" Runat="server" AutoPostBack="True"
												style="width:50px"></asp:DropDownList>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Declared &lt;br&gt;Value">
										<HeaderStyle Wrap="False" Width="5%" CssClass="gridHeading"></HeaderStyle>
										<ItemStyle Wrap="False" VerticalAlign="Middle"></ItemStyle>
										<ItemTemplate>
											<asp:Label CssClass="gridLabelNumber" ID="dgConsignmentlblDeclareValue" Text='<%#DataBinder.Eval(Container.DataItem,"DeclaredValue","{0:n}")%>' Runat="server" Enabled="True" >
											</asp:Label>
										</ItemTemplate>
										<FooterStyle Wrap="False"></FooterStyle>
										<EditItemTemplate>
											<cc1:mstextbox CssClass="gridTextBoxNumber" ID="dgConsignmenttxtDeclareValue" Text='<%#DataBinder.Eval(Container.DataItem,"DeclaredValue","{0:n}")%>' Runat="server" TextMaskType="msNumeric" MaxLength="7" NumberMaxValue="9999999" NumberMinValue="0" NumberPrecision="7" NumberScale="2">
											</cc1:mstextbox>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="COD  &lt;br&gt;Amount">
										<HeaderStyle Wrap="False" Width="5%" CssClass="gridHeading"></HeaderStyle>
										<ItemStyle Wrap="False" VerticalAlign="Middle"></ItemStyle>
										<ItemTemplate>
											<asp:Label CssClass="gridLabelNumber" ID="dgConsignmentlblCODAmount" Text='<%#DataBinder.Eval(Container.DataItem,"CODAmount","{0:n}")%>' Runat="server" Enabled="True">
											</asp:Label>
										</ItemTemplate>
										<FooterStyle Wrap="False"></FooterStyle>
										<EditItemTemplate>
											<cc1:mstextbox CssClass="gridTextBoxNumber" ID="dgConsignmenttxtCODAmount" Text='<%#DataBinder.Eval(Container.DataItem,"CODAmount","{0:n}")%>' Runat="server" TextMaskType="msNumeric" MaxLength="7" NumberMaxValue="9999999" NumberMinValue="0" NumberPrecision="7" NumberScale="2">
											</cc1:mstextbox>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Total &lt;br&gt;Pkgs">
										<HeaderStyle Wrap="False" Width="5%" CssClass="gridHeading"></HeaderStyle>
										<ItemStyle Wrap="False"></ItemStyle>
										<ItemTemplate>
											<asp:Label CssClass="gridLabelNumber" ID="dgConsignmentlblTotalPkgs" Text='<%#DataBinder.Eval(Container.DataItem,"TotalPkg")%>' Runat="server">
											</asp:Label>
										</ItemTemplate>
										<FooterStyle Wrap="False"></FooterStyle>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Actual &lt;br&gt;Weight">
										<HeaderStyle Wrap="False" Width="5%" CssClass="gridHeading"></HeaderStyle>
										<ItemStyle Wrap="False"></ItemStyle>
										<ItemTemplate>
											<asp:Label CssClass="gridLabelNumber" ID="dgConsignmentlblActualWeight" Text='<%#DataBinder.Eval(Container.DataItem,"ActualWt","{0:n}")%>' Runat="server">
											</asp:Label>
										</ItemTemplate>
										<FooterStyle Wrap="False"></FooterStyle>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Dim &lt;br&gt;Weight">
										<HeaderStyle Wrap="False" Width="5%" CssClass="gridHeading"></HeaderStyle>
										<ItemStyle Wrap="False"></ItemStyle>
										<ItemTemplate>
											<asp:Label CssClass="gridLabelNumber" ID="dgConsignmentlblDimWeight" Text='<%#DataBinder.Eval(Container.DataItem,"DimWt","{0:n}")%>' Runat="server">
											</asp:Label>
										</ItemTemplate>
										<FooterStyle Wrap="False"></FooterStyle>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Chg &lt;br&gt;Weight">
										<HeaderStyle Wrap="False" Width="5%" CssClass="gridHeading"></HeaderStyle>
										<ItemStyle Wrap="False"></ItemStyle>
										<ItemTemplate>
											<asp:Label CssClass="gridLabelNumber" ID="dgConsignmentlblChgWeight" Text='<%#DataBinder.Eval(Container.DataItem,"ChgWt","{0:n}")%>' Runat="server">
											</asp:Label>
										</ItemTemplate>
										<FooterStyle Wrap="False"></FooterStyle>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Total &lt;br&gt;Charge">
										<HeaderStyle Wrap="False" Width="5%" CssClass="gridHeading"></HeaderStyle>
										<ItemStyle Wrap="False"></ItemStyle>
										<ItemTemplate>
											<asp:Label CssClass="gridLabelNumber" ID="dgConsignmentlblTotalCharge" Text='<%#DataBinder.Eval(Container.DataItem,"TotalCharge","{0:n}")%>' Runat="server">
											</asp:Label>
										</ItemTemplate>
										<FooterStyle Wrap="False"></FooterStyle>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Freight &lt;br&gt;Charge">
										<HeaderStyle Wrap="False" Width="5%" CssClass="gridHeading"></HeaderStyle>
										<ItemStyle Wrap="False"></ItemStyle>
										<ItemTemplate>
											<asp:Label CssClass="gridLabelNumber" ID="dgConsignmentlblFreightCharge" Text='<%#DataBinder.Eval(Container.DataItem,"FreightCharge","{0:n}")%>' Runat="server">
											</asp:Label>
										</ItemTemplate>
										<FooterStyle Wrap="False"></FooterStyle>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Ins. &lt;br&gt;Surch.">
										<HeaderStyle Wrap="False" Width="5%" CssClass="gridHeading"></HeaderStyle>
										<ItemStyle Wrap="False"></ItemStyle>
										<ItemTemplate>
											<asp:Label CssClass="gridLabelNumber" ID="dgConsignmentlblInsSurch" Text='<%#DataBinder.Eval(Container.DataItem,"InsSurch","{0:n}")%>' Runat="server">
											</asp:Label>
										</ItemTemplate>
										<FooterStyle Wrap="False"></FooterStyle>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Other &lt;br&gt;Surch.">
										<HeaderStyle Wrap="False" Width="5%" CssClass="gridHeading"></HeaderStyle>
										<ItemStyle Wrap="False"></ItemStyle>
										<ItemTemplate>
											<asp:Label CssClass="gridLabelNumber" ID="dgConsignmentlblOtherSurch" Text='<%#DataBinder.Eval(Container.DataItem,"OtherSurch","{0:n}")%>' Runat="server">
											</asp:Label>
										</ItemTemplate>
										<FooterStyle Wrap="False"></FooterStyle>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="VAS &lt;br&gt;Surch.">
										<HeaderStyle Wrap="False" Width="5%" CssClass="gridHeading"></HeaderStyle>
										<ItemStyle Wrap="False"></ItemStyle>
										<ItemTemplate>
											<asp:Label CssClass="gridLabelNumber" ID="dgConsignmentlblVASSurch" Text='<%#DataBinder.Eval(Container.DataItem,"VASSurch","{0:n}")%>' Runat="server">
											</asp:Label>
										</ItemTemplate>
										<FooterStyle Wrap="False"></FooterStyle>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="ESA &lt;br&gt;Surch.">
										<HeaderStyle Wrap="False" Width="5%" CssClass="gridHeading"></HeaderStyle>
										<ItemStyle Wrap="False"></ItemStyle>
										<ItemTemplate>
											<asp:Label CssClass="gridLabelNumber" ID="dgConsignmentlblESASurch" Text='<%#DataBinder.Eval(Container.DataItem,"ESASurch","{0:n}")%>' Runat="server">
											</asp:Label>
										</ItemTemplate>
										<FooterStyle Wrap="False"></FooterStyle>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Est Del. &lt;br&gt;Date">
										<HeaderStyle Wrap="False" Width="8%" CssClass="gridHeading"></HeaderStyle>
										<ItemStyle Wrap="False" HorizontalAlign="Left"></ItemStyle>
										<ItemTemplate>
											<asp:Label CssClass="gridLabelNumber" ID="dgConsignmentlblESTDelDate" Text='<%#DataBinder.Eval(Container.DataItem,"EstDelDate","{0:dd/MM/yyyy HH:mm}")%>' Runat="server">
											</asp:Label>
										</ItemTemplate>
										<FooterStyle Wrap="False"></FooterStyle>
									</asp:TemplateColumn>
								</Columns>
								<PagerStyle NextPageText="Next" Height="20px" Font-Size="Smaller" PrevPageText="Previous" HorizontalAlign="Right"
									Wrap="False"></PagerStyle>
							</asp:datagrid></td>
					</tr>
					<tr>
						<td></td>
					</tr>
					<tr>
						<td align="left" width="25%"></td>
						<TD width="25%"></TD>
						<TD width="25%"></TD>
						<TD width="25%"></TD>
					</tr>
					<TR>
						<TD vAlign="bottom" align="left" width="25%" height="50"><FONT face="Tahoma"><asp:label id="Label9" runat="server" CssClass="tableHeadingFieldset" Font-Size="13px">Packages</asp:label>&nbsp;
								<asp:button id="btnInsertPackage" runat="server" CssClass="queryButton" Width="66px" Text="Insert"
									CausesValidation="False"></asp:button></FONT></TD>
					</TR>
					<tr>
						<td vAlign="top" height="100"><asp:datagrid id="dgPackage" runat="server" Width="700px" AllowPaging="True" PageSize="5" ItemStyle-Height="20"
								AutoGenerateColumns="False" OnSelectedIndexChanged="dgPackage_SelectedIndexChanged" OnDeleteCommand="dgPackage_Delete"
								OnUpdateCommand="dgPackage_Update" OnCancelCommand="dgPackage_Cancel" OnEditCommand="dgPackage_Edit" OnPageIndexChanged="dgPackage_PageChange">
								<ItemStyle Height="20px"></ItemStyle>
								<Columns>
									<asp:ButtonColumn Text="&lt;IMG SRC='images/butt-delete.gif' border=0 title='Delete' &gt;" CommandName="Delete">
										<HeaderStyle Width="5%" CssClass="gridHeading"></HeaderStyle>
										<ItemStyle CssClass="gridFieldSelected"></ItemStyle>
									</asp:ButtonColumn>
									<asp:EditCommandColumn ButtonType="LinkButton" UpdateText="&lt;IMG SRC='images/butt-update.gif' border=0  title='Update' &gt;"
										CancelText="&lt;IMG SRC='images/butt-cancel.gif' border=0 title='Cancel'&gt;" EditText="&lt;IMG SRC='images/butt-edit.gif' border=0  title='Edit' &gt;">
										<HeaderStyle Width="5%" CssClass="gridHeading"></HeaderStyle>
										<ItemStyle CssClass="gridFieldSelected"></ItemStyle>
									</asp:EditCommandColumn>
									<asp:TemplateColumn HeaderText="MPS No">
										<HeaderStyle Wrap="False" Width="10%" CssClass="gridHeading"></HeaderStyle>
										<ItemStyle CssClass="gridFieldSelected"></ItemStyle>
										<ItemTemplate>
											<asp:Label CssClass="gridLabelNumber" ID="dgPackagelblMPSNo" Text='<%#DataBinder.Eval(Container.DataItem,"Pkg_MPSNo")%>' Runat="server">
											</asp:Label>
										</ItemTemplate>
										<EditItemTemplate>
											<cc1:mstextbox CssClass="gridTextBox" ID="dgPackagetxtMPSNo" Text='<%#DataBinder.Eval(Container.DataItem,"Pkg_MPSNo")%>' Runat="server" Enabled="True" NumberPrecision="5" NumberMinValue="1" NumberMaxValue="99999" TextMaskType="msNumeric" MaxLength="5" >
											</cc1:mstextbox>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Length">
										<HeaderStyle Width="10%" CssClass="gridHeading"></HeaderStyle>
										<ItemStyle CssClass="gridFieldSelected"></ItemStyle>
										<ItemTemplate>
											<asp:Label CssClass="gridLabelNumber" ID="dgPackagelblLength" Text='<%#DataBinder.Eval(Container.DataItem,"pkg_length","{0:n0}")%>' Runat="server" Enabled="True">
											</asp:Label>
										</ItemTemplate>
										<EditItemTemplate>
											<cc1:mstextbox CssClass="gridTextBoxNumber" ID="dgPackagetxtLength" Text='<%#DataBinder.Eval(Container.DataItem,"pkg_length","{0:n0}")%>' Runat="server" TextMaskType="msNumeric" MaxLength="7" NumberMaxValue="10000" NumberMinValue="0" NumberPrecision="6" NumberScale="0">
											</cc1:mstextbox>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Breadth">
										<HeaderStyle Width="10%" CssClass="gridHeading"></HeaderStyle>
										<ItemStyle CssClass="gridFieldSelected"></ItemStyle>
										<ItemTemplate>
											<asp:Label CssClass="gridLabelNumber" ID="dgPackagelblBreadth" Text='<%#DataBinder.Eval(Container.DataItem,"pkg_breadth","{0:n0}")%>' Runat="server" Enabled="True">
											</asp:Label>
										</ItemTemplate>
										<EditItemTemplate>
											<cc1:mstextbox CssClass="gridTextBoxNumber" ID="dgPackagetxtBreadth" Text='<%#DataBinder.Eval(Container.DataItem,"pkg_breadth","{0:n0}")%>' Runat="server" TextMaskType="msNumeric" MaxLength="7" NumberMaxValue="10000" NumberMinValue="0" NumberPrecision="6" NumberScale="2">
											</cc1:mstextbox>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Height">
										<HeaderStyle Width="10%" CssClass="gridHeading"></HeaderStyle>
										<ItemStyle CssClass="gridFieldSelected" VerticalAlign="Middle"></ItemStyle>
										<ItemTemplate>
											<asp:Label CssClass="gridLabelNumber" ID="dgPackagelblHeight" Text='<%#DataBinder.Eval(Container.DataItem,"pkg_height","{0:n0}")%>' Runat="server" Enabled="True" >
											</asp:Label>
										</ItemTemplate>
										<EditItemTemplate>
											<cc1:mstextbox CssClass="gridTextBoxNumber" ID="dgPackagetxtHeight" Text='<%#DataBinder.Eval(Container.DataItem,"pkg_height","{0:n0}")%>' Runat="server" TextMaskType="msNumeric" MaxLength="7" NumberMaxValue="10000" NumberMinValue="0" NumberPrecision="6" NumberScale="2">
											</cc1:mstextbox>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Volume">
										<HeaderStyle Width="10%" CssClass="gridHeading"></HeaderStyle>
										<ItemStyle CssClass="gridFieldSelected" VerticalAlign="Middle"></ItemStyle>
										<ItemTemplate>
											<asp:Label CssClass="gridLabelNumber" ID="dgPackagelblVolume" Text='<%#DataBinder.Eval(Container.DataItem,"pkg_volume","{0:n0}")%>' Runat="server" Enabled="True">
											</asp:Label>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Weight">
										<HeaderStyle Width="10%" CssClass="gridHeading"></HeaderStyle>
										<ItemStyle CssClass="gridFieldSelected"></ItemStyle>
										<ItemTemplate>
											<asp:Label CssClass="gridLabelNumber" ID="dgPackagelblWeight" Text='<%#DataBinder.Eval(Container.DataItem,"pkg_wt","{0:n}")%>' Runat="server">
											</asp:Label>
										</ItemTemplate>
										<EditItemTemplate>
											<cc1:mstextbox CssClass="gridTextBoxNumber" ID="dgPackagetxtWeight" Text='<%#DataBinder.Eval(Container.DataItem,"pkg_wt","{0:n}")%>' Runat="server" Enabled="True" TextMaskType="msNumeric" MaxLength="7" NumberMaxValue="10000" NumberMinValue="0" NumberPrecision="6" NumberScale="2">
											</cc1:mstextbox>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Qty">
										<HeaderStyle Width="10%" CssClass="gridHeading"></HeaderStyle>
										<ItemStyle CssClass="gridFieldSelected"></ItemStyle>
										<ItemTemplate>
											<asp:Label CssClass="gridLabelNumber" ID="dgPackagelblQty" Text='<%#DataBinder.Eval(Container.DataItem,"pkg_qty")%>' Runat="server">
											</asp:Label>
										</ItemTemplate>
										<EditItemTemplate>
											<cc1:mstextbox CssClass="gridTextBoxNumber" ID="dgPackagetxtQty" Text='<%#DataBinder.Eval(Container.DataItem,"pkg_qty")%>' Runat="server" Enabled="True" TextMaskType="msNumeric" MaxLength="4" NumberMaxValue="9999" NumberMinValue="0" NumberPrecision="4" NumberScale="0">
											</cc1:mstextbox>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Total Act Wt">
										<HeaderStyle Wrap="False" Width="6%" CssClass="gridHeading"></HeaderStyle>
										<ItemStyle CssClass="gridFieldSelected"></ItemStyle>
										<ItemTemplate>
											<asp:Label CssClass="gridLabelNumber" ID="dgPackagelblTotalActWt" Text='<%#DataBinder.Eval(Container.DataItem,"tot_act_wt","{0:n}")%>' Runat="server">
											</asp:Label>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Total R Wt">
										<HeaderStyle Wrap="False" Width="10%" CssClass="gridHeading"></HeaderStyle>
										<ItemStyle CssClass="gridFieldSelected"></ItemStyle>
										<ItemTemplate>
											<asp:Label CssClass="gridLabelNumber" ID="dgPackagelblTotalRWt" Text='<%#DataBinder.Eval(Container.DataItem,"tot_wt","{0:n}")%>' Runat="server">
											</asp:Label>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Total R Dim Wt">
										<HeaderStyle Wrap="False" Width="10%" CssClass="gridHeading"></HeaderStyle>
										<ItemStyle CssClass="gridFieldSelected"></ItemStyle>
										<ItemTemplate>
											<asp:Label CssClass="gridLabelNumber" ID="dgPackagelblTotalRDimWt" Text='<%#DataBinder.Eval(Container.DataItem,"tot_dim_wt","{0:n}")%>' Runat="server">
											</asp:Label>
										</ItemTemplate>
									</asp:TemplateColumn>
								</Columns>
								<PagerStyle NextPageText="Next" Font-Size="Smaller" PrevPageText="Previous" HorizontalAlign="Right"
									Height="20"></PagerStyle>
							</asp:datagrid></td>
					</tr>
					<tr>
						<td></td>
					</tr>
					<tr>
						<td></td>
					</tr>
					<tr>
						<td vAlign="bottom" height="50"><asp:label id="Label11" runat="server" CssClass="tableHeadingFieldset" Font-Size="13px">VAS</asp:label><FONT face="Tahoma">&nbsp;<asp:button id="btnInsertVAS" runat="server" CssClass="queryButton" Width="66px" Text="Insert"
									CausesValidation="False"></asp:button></FONT></td>
					</tr>
					<tr>
						<td vAlign="top" height="100"><FONT face="Tahoma"><asp:datagrid id="dgVAS" tabIndex="115" runat="server" Width="588px" AllowPaging="True" PageSize="5"
									ItemStyle-Height="20px" AutoGenerateColumns="False" OnDeleteCommand="dgVAS_Delete" OnUpdateCommand="dgVAS_Update" OnCancelCommand="dgVAS_Cancel"
									OnEditCommand="dgVAS_Edit" OnPageIndexChanged="dgVAS_PageChange" OnItemCommand="dgVAS_Button" OnItemDataBound="dgVAS_ItemDataBound"
									HeaderStyle-Height="20px">
									<ItemStyle Height="20px"></ItemStyle>
									<HeaderStyle Height="20px"></HeaderStyle>
									<Columns>
										<asp:ButtonColumn Text="&lt;IMG SRC='images/butt-delete.gif' border=0 title='Delete' &gt;" CommandName="Delete">
											<HeaderStyle Width="3%" CssClass="gridHeading"></HeaderStyle>
											<ItemStyle CssClass="gridFieldSelected"></ItemStyle>
										</asp:ButtonColumn>
										<asp:EditCommandColumn ButtonType="LinkButton" UpdateText="&lt;IMG SRC='images/butt-update.gif' border=0  title='Update' &gt;"
											CancelText="&lt;IMG SRC='images/butt-cancel.gif' border=0 title='Cancel'&gt;" EditText="&lt;IMG SRC='images/butt-edit.gif' border=0  title='Edit' &gt;">
											<HeaderStyle Width="3%" CssClass="gridHeading"></HeaderStyle>
											<ItemStyle CssClass="gridFieldSelected"></ItemStyle>
										</asp:EditCommandColumn>
										<asp:TemplateColumn HeaderText="VAS">
											<HeaderStyle Width="7%" CssClass="gridHeading"></HeaderStyle>
											<ItemStyle CssClass="gridFieldSelected"></ItemStyle>
											<ItemTemplate>
												<asp:Label ID="dgVASlblVAS" CssClass="gridLabel" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"vas_code")%>'>
												</asp:Label>
											</ItemTemplate>
											<EditItemTemplate>
												<cc1:mstextbox CssClass="gridTextBox" ReadOnly="True" runat="server" ID="dgVAStxtVAS" MaxLength="12" TextMaskType="msUpperAlfaNumericWithUnderscore" Text='<%#DataBinder.Eval(Container.DataItem,"vas_code")%>' Enabled="True">
												</cc1:mstextbox>
											</EditItemTemplate>
										</asp:TemplateColumn>
										<asp:ButtonColumn Text="&lt;IMG SRC='images/butt-search.gif' border=0 title='Search' &gt;" CommandName="Search">
											<HeaderStyle Width="3%" CssClass="gridHeading"></HeaderStyle>
											<ItemStyle CssClass="gridFieldSelected"></ItemStyle>
										</asp:ButtonColumn>
										<asp:TemplateColumn HeaderText="Description">
											<HeaderStyle Width="20%" CssClass="gridHeading"></HeaderStyle>
											<ItemStyle CssClass="gridFieldSelected"></ItemStyle>
											<ItemTemplate>
												<asp:Label runat="server" CssClass="gridLabel" ID="dgVASlblDescription" Text='<%#DataBinder.Eval(Container.DataItem,"vas_description")%>'>
												</asp:Label>
											</ItemTemplate>
											<EditItemTemplate>
												<cc1:mstextbox CssClass="gridTextBox" runat="server" ID="dgVAStxtDescription" Enabled=True EnableViewState=True Text='<%#DataBinder.Eval(Container.DataItem,"vas_description")%>'>
												</cc1:mstextbox>
											</EditItemTemplate>
										</asp:TemplateColumn>
										<asp:TemplateColumn HeaderText="Surcharge">
											<HeaderStyle Width="8%" CssClass="gridHeading"></HeaderStyle>
											<ItemStyle CssClass="gridFieldSelected"></ItemStyle>
											<ItemTemplate>
												<asp:Label runat="server" CssClass="gridLabelNumber" ID="dgVASlblSurcharge" Text='<%#DataBinder.Eval(Container.DataItem,"vas_surcharge","{0:n}")%>'>
												</asp:Label>
											</ItemTemplate>
											<EditItemTemplate>
												<cc1:mstextbox CssClass="gridTextBoxNumber" runat="server" ID="dgVAStxtSurcharge" TextMaskType="msNumeric" MaxLength="9" NumberMaxValue="999999" NumberMinValue="0" NumberPrecision="8" NumberScale="8" Text='<%#DataBinder.Eval(Container.DataItem,"vas_surcharge","{0:n}")%>'>
												</cc1:mstextbox>
											</EditItemTemplate>
										</asp:TemplateColumn>
									</Columns>
									<PagerStyle NextPageText="Next" Height="20px" Font-Size="Smaller" PrevPageText="Previous" HorizontalAlign="Right"></PagerStyle>
								</asp:datagrid></FONT></td>
					</tr>
					<tr>
						<td></td>
					</tr>
				</table>
			</DIV>
			<DIV id="divConfirmHoliday" style="Z-INDEX: 101; LEFT: 70px; WIDTH: 374px; POSITION: relative; TOP: 46px; HEIGHT: 178px; relative: absolute"
				MS_POSITIONING="GridLayout" runat="server">
				<fieldset style="Z-INDEX: 80; LEFT: 70px; WIDTH: 374px; POSITION: relative; TOP: 46px; HEIGHT: 178px; relative: absolute"
					MS_POSITIONING="GridLayout"><legend><asp:label id="lblConf" CssClass="tableHeadingFieldset" Runat="server">Confirmation</asp:label></legend>
					<TABLE id="Table2" style="Z-INDEX: 75; LEFT: 7px; WIDTH: 346px; POSITION: absolute; TOP: 23px; HEIGHT: 153px"
						runat="server">
						<TR>
							<TD>
								<P align="center"><asp:label id="lblConfirmMsg" runat="server" Width="321px"></asp:label></P>
								<P>
								<P align="center"><asp:button id="btnToSaveChanges" runat="server" CssClass="queryButton" Text="Yes"></asp:button><asp:button id="btnNotToSave" runat="server" CssClass="queryButton" Text="No" CausesValidation="False"></asp:button><asp:button id="btnToCancel" runat="server" CssClass="queryButton" Text="Cancel" CausesValidation="False"
										Visible="False"></asp:button></P>
								<P></P>
							</TD>
						</TR>
					</TABLE>
				</fieldset>
			</DIV>
		</form>
	</body>
</HTML>
