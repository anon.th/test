<%@ Page language="c#" Codebehind="CommodityPopup.aspx.cs" AutoEventWireup="false" Inherits="TIES.WebUI.CommodityPopup" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>CommodityPopup</title>
		<LINK href="css/Styles.css" type="text/css" rel="stylesheet" name="stylesheet">
		<meta name="GENERATOR" Content="Microsoft Visual Studio 7.0">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<script language="javascript" src="Scripts/settingScrollPosition.js"></script>
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="CommodityPopup" method="post" runat="server">
			<asp:datagrid id="dgCommodity" style="Z-INDEX: 101; LEFT: 49px; POSITION: absolute; TOP: 95px" runat="server" CssClass="gridHeading" BorderStyle="None" BorderWidth="1px" CellPadding="3" BackColor="White" BorderColor="#CCCCCC" AutoGenerateColumns="False" Width="522px" AllowPaging="True" PageSize="10" OnPageIndexChanged="Paging">
				<SelectedItemStyle Font-Bold="True" ForeColor="White" BackColor="#669999"></SelectedItemStyle>
				<HeaderStyle Width="10%" CssClass="gridHeading"></HeaderStyle>
				<ItemStyle CssClass="popupGridField"></ItemStyle>
				<FooterStyle ForeColor="#000066" BackColor="White"></FooterStyle>
				<Columns>
					<asp:BoundColumn DataField="commodity_code" HeaderText="Commodity Code"></asp:BoundColumn>
					<asp:BoundColumn DataField="commodity_description" HeaderText="Description"></asp:BoundColumn>
					<asp:ButtonColumn Text="Select" CommandName="Select"></asp:ButtonColumn>
				</Columns>
				<PagerStyle HorizontalAlign="Left" ForeColor="#000066" BackColor="White" Mode="NumericPages"></PagerStyle>
			</asp:datagrid>
			<asp:button id="btnClose" style="Z-INDEX: 108; LEFT: 454px; POSITION: absolute; TOP: 54px" runat="server" Width="84px" CssClass="buttonProp" Height="21px" Text="Close" CausesValidation="False"></asp:button>
			<asp:Label id="lblCustName" style="Z-INDEX: 107; LEFT: 214px; POSITION: absolute; TOP: 28px" runat="server" CssClass="tablelabel">Description</asp:Label>
			<asp:button id="btnSearch" style="Z-INDEX: 105; LEFT: 368px; POSITION: absolute; TOP: 54px" runat="server" CssClass="buttonProp" Width="84px" CausesValidation="False" Text="Search" Height="21px"></asp:button><asp:textbox id="txtCommodityCode" style="Z-INDEX: 103; LEFT: 49px; POSITION: absolute; TOP: 56px" runat="server" CssClass="textField" Width="116px" Height="19px"></asp:textbox><asp:label id="lblCustID" style="Z-INDEX: 100; LEFT: 49px; POSITION: absolute; TOP: 26px" runat="server" CssClass="tableLabel" Width="115px" Height="16px">Commodity Code</asp:label><asp:button id="btnRefresh" style="DISPLAY: none; Z-INDEX: 106; LEFT: 569px; POSITION: absolute; TOP: 69px" runat="server" Width="67px" Text="HIDDEN"></asp:button><asp:textbox id="txtCommodityDescription" style="Z-INDEX: 102; LEFT: 209px; POSITION: absolute; TOP: 55px" runat="server" CssClass="textField" Width="116px" Height="19px"></asp:textbox>
		</form>
	</body>
</HTML>
