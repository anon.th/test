using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.common.classes;
using com.common.DAL;
using com.common.RBAC;
using com.ties.DAL;
using com.common.util;
using com.common.applicationpages;
namespace com.ties
{
	/// <summary>
	/// Summary description for BandDiscount.
	/// </summary>
	public class BandDiscount : BasePage
	{
		protected System.Web.UI.WebControls.Button btnQuery;
		protected System.Web.UI.WebControls.Button btnInsert;
		protected System.Web.UI.WebControls.DataGrid dgBandDiscount;
		protected System.Web.UI.WebControls.Button btnExecuteQuery;
		private SessionDS m_sdsBandDiscount;
		private String m_strAppID;
		protected System.Web.UI.WebControls.Label lblValBandDiscount;
		protected System.Web.UI.WebControls.RegularExpressionValidator RegularExpressionValidator1;
		protected System.Web.UI.WebControls.Label Label1;
		protected System.Web.UI.WebControls.ValidationSummary PageValidationSummary;
		private String m_strEnterpriseID;
	
		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			//Utility utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings,Page.Session);
			m_strAppID = utility.GetAppID();
			m_strEnterpriseID = utility.GetEnterpriseID();
			if(!Page.IsPostBack)
			{
				ViewState["m_strCode"] = "";
				ViewState["index"] = 0;
				ViewState["prevRow"] = 0;
				ViewState["SCMode"] = ScreenMode.None;
				ViewState["SCOperation"] = Operation.None;

				dgBandDiscount.EditItemIndex = -1;
				m_sdsBandDiscount = SysDataManager1.GetEmptyBandDiscount(1);
				m_sdsBandDiscount.DataSetRecSize = 5;
				enableEditColumn(false);
				BindBandDiscount();
				Session["SESSION_DS1"] = m_sdsBandDiscount;
				Session["QUERY_DS"] = m_sdsBandDiscount;

				//-----START QUERYING------
				lblValBandDiscount.Text = "";
				Utility.EnableButton(ref btnInsert,ButtonType.Insert,true,m_moduleAccessRights);
				m_sdsBandDiscount = SysDataManager1.GetEmptyBandDiscount(1);
				dgBandDiscount.CurrentPageIndex = 0;
				ViewState["SCMode"] = ScreenMode.Query;
				ViewState["SCOperation"] = Operation.None;
				btnExecuteQuery.Enabled = true;
				ViewState["m_strCode"] = "";
				ViewState["prevRow"]=0;
				ViewState["index"]=0;
				dgBandDiscount.EditItemIndex = 0;
				enableEditColumn(false);
				BindBandDiscount();
			}
			else
			{
				m_sdsBandDiscount = (SessionDS)Session["SESSION_DS1"];
			}
		}
		/// <summary>
		/// To Bind Data to Datagrid
		/// </summary>
		private void BindBandDiscount()
		{
			dgBandDiscount.DataSource = m_sdsBandDiscount.ds;
			dgBandDiscount.DataBind();
			dgBandDiscount.VirtualItemCount = System.Convert.ToInt32(m_sdsBandDiscount.QueryResultMaxSize);
			Session["SESSION_DS1"] = m_sdsBandDiscount;
		}
		/// <summary>
		/// On editing datagrid event handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void OnEdit_BandDiscount(object sender, DataGridCommandEventArgs e)
		{		
			lblValBandDiscount.Text="";
			ViewState["SCOperation"] = Operation.Update;
			Utility.EnableButton(ref btnInsert,ButtonType.Insert,true,m_moduleAccessRights);
			int rowIndex = e.Item.ItemIndex;
			ViewState["index"] = rowIndex;
			//Label lblCode = null;
			if(dgBandDiscount.EditItemIndex >0)
			{
				msTextBox txtCode = (msTextBox)dgBandDiscount.Items[dgBandDiscount.EditItemIndex].FindControl("txtCode");
				
				if(txtCode.Text!=null && txtCode.Text=="")	
				{
					ViewState["m_strCode"] = txtCode.Text;
					m_sdsBandDiscount.ds.Tables[0].Rows.RemoveAt(dgBandDiscount.EditItemIndex);
				}
			}
			dgBandDiscount.EditItemIndex = rowIndex;
			BindBandDiscount();
	
		}
		/// <summary>
		/// On canceling edit mode
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void OnCancel_BandDiscount(object sender, DataGridCommandEventArgs e)
		{		
			lblValBandDiscount.Text="";
			dgBandDiscount.EditItemIndex = -1;
			m_sdsBandDiscount = (SessionDS)Session["Session_DS1"];
			int rowIndex = e.Item.ItemIndex;

			if((int)ViewState["SCMode"] == (int)ScreenMode.Insert && !checkCodeTextEmpty(rowIndex))
			{
				m_sdsBandDiscount.ds.Tables[0].Rows.RemoveAt(rowIndex);
			}
			enableDeleteColumn(true);
			Utility.EnableButton(ref btnInsert,ButtonType.Insert,true,m_moduleAccessRights);
			BindBandDiscount();
	
		}
		/// <summary>
		/// On clicking update button, allows saveing of data to database
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void OnUpdate_BandDiscount(object sender, DataGridCommandEventArgs e)
		{		
			int rowIndex = e.Item.ItemIndex;			
			updateLatestBandDiscount(rowIndex);
			DataSet dsBandDiscount = m_sdsBandDiscount.ds.GetChanges();
			SessionDS sdsBandDiscount = new SessionDS();
			sdsBandDiscount.ds = dsBandDiscount;

			int iOperation = (int)ViewState["SCOperation"];			
			try
			{
				if (iOperation == (int)Operation.Insert)
				{
					SysDataManager1.InsertBandDiscount(sdsBandDiscount,m_strAppID,m_strEnterpriseID);
					m_sdsBandDiscount.ds.Tables[0].Rows[rowIndex].AcceptChanges();
					lblValBandDiscount.Text =Utility.GetLanguageText(ResourceType.UserMessage,"INS_SUCCESSFULLY",utility.GetUserCulture());
				}
				else
				{
					SysDataManager1.UpdateBandDiscount(sdsBandDiscount,m_strAppID,m_strEnterpriseID);
					m_sdsBandDiscount.ds.Tables[0].Rows[rowIndex].AcceptChanges();
					lblValBandDiscount.Text = Utility.GetLanguageText(ResourceType.UserMessage,"UPD_SUCCESSFULLY",utility.GetUserCulture());
				}				
				
			}
			catch(ApplicationException appException)
			{
				String strMsg = appException.Message;
				strMsg = appException.InnerException.Message;												
										
				if(strMsg.IndexOf("duplicate key") != -1 || strMsg.IndexOf("unique") != -1)
					lblValBandDiscount.Text = Utility.GetLanguageText(ResourceType.UserMessage,"NO_DUPLICATE",utility.GetUserCulture());	
				else if (strMsg.IndexOf("conflict") != -1 ) 
					lblValBandDiscount.Text = Utility.GetLanguageText(ResourceType.UserMessage,"NO_DUPLICATE",utility.GetUserCulture());	
				else
				{
					lblValBandDiscount.Text = strMsg;
				}
										
				Logger.LogTraceError("TRACE MODE : band discount ","Update","ERR004","Error updating record :--- "+strMsg);
				Logger.LogTraceError("module2","TRACE MODE : (module2 only)band discount","btnSave_Click","ERR004","Error updating record : "+strMsg);										
				return;
			}							
			Utility.EnableButton(ref btnInsert,ButtonType.Insert,true,m_moduleAccessRights);			
			ViewState["SCOperation"] = Operation.None;
			dgBandDiscount.EditItemIndex = -1;			
			BindBandDiscount();	
		}

		/// <summary>
		/// On cliking on delete button will delete that particular data in the database
		/// base on the code
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void OnDelete_BandDiscount(object sender, DataGridCommandEventArgs e)
		{	
			if(checkEmptyFields(e.Item.ItemIndex))
			{
				ViewState["SCOperation"] = Operation.Delete;
				int rowNum = e.Item.ItemIndex;
				if((int)ViewState["SCMode"] == (int)ScreenMode.Insert && !checkCodeTextEmpty(rowNum))
				{
					DeleteBandDiscount(rowNum);
					if(dgBandDiscount.EditItemIndex>-1)
					{
						m_sdsBandDiscount.ds.Tables[0].Rows.RemoveAt(dgBandDiscount.EditItemIndex);
					}
				
				}
				else
				{
					DeleteBandDiscount(rowNum);
					if((int)ViewState["SCMode"] == (int)ScreenMode.Query)
					{
						showCurrentPage();
					}
				}
				
				Utility.EnableButton(ref btnInsert,ButtonType.Insert,true,m_moduleAccessRights);
				BindBandDiscount();
			}
		}
		/// <summary>
		/// to delete data
		/// </summary>
		/// <param name="rowIndex">Index of the row to delete</param>
		private void DeleteBandDiscount(int rowIndex)
		{
			m_sdsBandDiscount = (SessionDS)Session["SESSION_DS1"];
			DataRow dr = m_sdsBandDiscount.ds.Tables[0].Rows[rowIndex];
			String strCode = (String)dr[0];
			try
			{
				SysDataManager1.DeleteBandDiscount(m_strAppID,m_strEnterpriseID,strCode);
				m_sdsBandDiscount.ds.Tables[0].Rows.RemoveAt(rowIndex);
				lblValBandDiscount.Text = Utility.GetLanguageText(ResourceType.UserMessage,"DLD_SUCCESSFULLY",utility.GetUserCulture());
			}
			catch(ApplicationException appException)
			{
				String strMsg = appException.Message;
				if(strMsg.IndexOf("FK") != -1)
				{
					lblValBandDiscount.Text =  Utility.GetLanguageText(ResourceType.UserMessage,"CHILD_FOUND_CANNOT_DEL",utility.GetUserCulture());
				}
				if(strMsg.IndexOf("child record") != -1 || strMsg.IndexOf("REFERENCE") != -1)
				{//Oralce Child Record
					lblValBandDiscount.Text =  Utility.GetLanguageText(ResourceType.UserMessage,"CHILD_FOUND_CANNOT_DEL",utility.GetUserCulture());
				}
				else
				{
					lblValBandDiscount.Text = strMsg;
				}
			}
			
		}
		/// <summary>
		/// to set weather the code column be shown or not
		/// </summary>
		/// <param name="toEnable">to show or not</param>
		private void enableCodeColumn(bool toEnable)
		{		
			dgBandDiscount.Columns[2].Visible=toEnable;//Code column
		}
		/// <summary>
		/// to set weather the delete column be shown or not
		/// </summary>
		/// <param name="toEnable">to show or not</param>
		private void enableDeleteColumn(bool toEnable)
		{
			dgBandDiscount.Columns[1].Visible=toEnable;
		}
		/// <summary>
		/// to set weather the edit column be shown or not
		/// </summary>
		/// <param name="toEnable">to show or not</param>
		private void enableEditColumn(bool toEnable)
		{
			dgBandDiscount.Columns[0].Visible=toEnable;
			dgBandDiscount.Columns[1].Visible=toEnable;
		}

		private void AddRow()
		{
			SysDataManager1.AddNewRowInBandDiscountDS(ref m_sdsBandDiscount);
			BindBandDiscount();
		}
		
		/// <summary>
		/// to read the text from the text box and store it into a dataset
		/// </summary>
		/// <param name="rowIndex">to read from a this rowIndex</param>
		private void updateLatestBandDiscount(int rowIndex)
		{
			msTextBox txtCode = (msTextBox)dgBandDiscount.Items[rowIndex].FindControl("txtCode");
			TextBox txtDesc = (TextBox)dgBandDiscount.Items[rowIndex].FindControl("txtDesc");
			msTextBox txtPercent = (msTextBox)dgBandDiscount.Items[rowIndex].FindControl("txtPercent");
			DataRow dr = m_sdsBandDiscount.ds.Tables[0].Rows[rowIndex];

			if((int)ViewState["SCMode"]==(int)ScreenMode.Insert || (int)ViewState["SCMode"]==(int)ScreenMode.Query)
			{
				dr[0] = txtCode.Text.ToString();
			}
			else
			{
				dr[0] = (String)ViewState["m_strCode"];
			}
			String strDesc = txtDesc.Text.ToString();
			dr[1] = strDesc;
			String strPercent = txtPercent.Text.ToString();
			if(strPercent!=null && strPercent!="")
			{
				//decimal tt = decimal.Parse(strPercent);
				dr[2] = txtPercent.Text;
			}
			else
			{
				dr[2] = System.DBNull.Value;
			}
			Session["SESSION_DS1"] = m_sdsBandDiscount;
		}
		
		/// <summary>
		/// To check if there are empty fields
		/// </summary>
		/// <param name="rowIndex">to check in this row index</param>
		/// <returns> to retur true or false</returns>
		private bool checkEmptyFields(int rowIndex)
		{			
			msTextBox txtCode = (msTextBox)dgBandDiscount.Items[rowIndex].FindControl("txtCode");
			TextBox txtDesc = (TextBox)dgBandDiscount.Items[rowIndex].FindControl("txtDesc");
			msTextBox txtPercent = (msTextBox)dgBandDiscount.Items[rowIndex].FindControl("txtPercent");
			if(txtCode!=null && txtDesc!=null && txtPercent!=null)
			{
				DataRow dr = m_sdsBandDiscount.ds.Tables[0].Rows[rowIndex];
				if((int)ViewState["SCMode"]==(int)ScreenMode.Query)
					dr[0] = txtCode.Text;
				else
					dr[0] = (String)ViewState["m_strCode"];

				dr[1] = txtDesc.Text;
				
				//dr[2] = Convert.ToDecimal(txtPercent.Text);
				if(txtPercent.Text!="")
					dr[2] = txtPercent.Text;

				Session["SESSION_DS1"] = m_sdsBandDiscount;
				Session["QUERY_DS"] = m_sdsBandDiscount;
				return false;
			}
			return true;
		}
		/// <summary>
		/// To display the queried results
		/// </summary>
		private void QueryBandDiscount()
		{
			checkEmptyFields((int)ViewState["index"]);
			m_sdsBandDiscount = SysDataManager1.QueryBandDiscount(m_sdsBandDiscount, m_strAppID,m_strEnterpriseID, 0, dgBandDiscount.PageSize);		
			if(m_sdsBandDiscount.QueryResultMaxSize <1)
			{
				lblValBandDiscount.Text =  Utility.GetLanguageText(ResourceType.UserMessage,"NO_RECORD_FOUND",utility.GetUserCulture());
			}
			dgBandDiscount.VirtualItemCount = System.Convert.ToInt32(m_sdsBandDiscount.QueryResultMaxSize);
			Session["SESSION_DS1"] = m_sdsBandDiscount;
		}

		/// <summary>
		/// To check if the code text is empty
		/// </summary>
		/// <param name="rowIndex">to check at this row index</param>
		/// <returns>to return true or false</returns>
		private bool checkCodeTextEmpty(int rowIndex)
		{
			msTextBox txtCode = (msTextBox)dgBandDiscount.Items[rowIndex].FindControl("txtCode");
			if(txtCode!=null && txtCode.Text!="")
			{
				return true;
			}
			return false;
		}

		/// <summary>
		/// to check is code is empty with Validation shown to user
		/// </summary>
		/// <param name="rowIndex">to check at this row index</param>
		/// <returns>to return true or fale</returns>

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			base.OnInit(e);
			InitializeComponent();
			
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnQuery.Click += new System.EventHandler(this.btnQuery_Click);
			this.btnExecuteQuery.Click += new System.EventHandler(this.btnExecuteQuery_Click);
			this.btnInsert.Click += new System.EventHandler(this.btnInsert_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

/// <summary>
/// insert clikc buton event handler
/// </summary>
/// <param name="sender"></param>
/// <param name="e"></param>
		private void btnInsert_Click(object sender, System.EventArgs e)
		{
			lblValBandDiscount.Text = "";
			Utility.EnableButton(ref btnInsert,ButtonType.Insert,false,m_moduleAccessRights);
			btnExecuteQuery.Enabled = false;
			dgBandDiscount.CurrentPageIndex = 0;
			if((int)ViewState["SCMode"] == (int)ScreenMode.Query)
			{
				m_sdsBandDiscount = SysDataManager1.GetEmptyBandDiscount(1);
				dgBandDiscount.EditItemIndex = m_sdsBandDiscount.ds.Tables[0].Rows.Count-1;
			}
			else if(m_sdsBandDiscount.ds.Tables[0].Rows.Count >= dgBandDiscount.PageSize)
			{
				m_sdsBandDiscount = SysDataManager1.GetEmptyBandDiscount(1);
				dgBandDiscount.EditItemIndex = m_sdsBandDiscount.ds.Tables[0].Rows.Count-1;
			}
			else
			{
				AddRow();
				dgBandDiscount.EditItemIndex = m_sdsBandDiscount.ds.Tables[0].Rows.Count-1;
			}

			dgBandDiscount.VirtualItemCount = m_sdsBandDiscount.ds.Tables[0].Rows.Count;

			ViewState["SCMode"] = ScreenMode.Insert;
			ViewState["SCOperation"] = Operation.Insert;
			enableCodeColumn(true);
			enableEditColumn(true);
			BindBandDiscount();
			getPageControls(Page);
		}

		/// <summary>
		/// Query click event handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void btnQuery_Click(object sender, System.EventArgs e)
		{
			lblValBandDiscount.Text = "";
			Utility.EnableButton(ref btnInsert,ButtonType.Insert,true,m_moduleAccessRights);
			m_sdsBandDiscount = SysDataManager1.GetEmptyBandDiscount(1);
			dgBandDiscount.CurrentPageIndex = 0;
			ViewState["SCMode"] = ScreenMode.Query;
			ViewState["SCOperation"] = Operation.None;
			btnExecuteQuery.Enabled = true;
			ViewState["m_strCode"] = "";
			ViewState["prevRow"]=0;
			ViewState["index"]=0;
			dgBandDiscount.EditItemIndex = 0;
			enableEditColumn(false);
			BindBandDiscount();
		}

		/// <summary>
		/// Execute query event handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void btnExecuteQuery_Click(object sender, System.EventArgs e)
		{
			dgBandDiscount.EditItemIndex = -1;
			QueryBandDiscount();
			btnExecuteQuery.Enabled = false;
			enableEditColumn(true);
			enableDeleteColumn(true);
			Utility.EnableButton(ref btnInsert,ButtonType.Insert,true,m_moduleAccessRights);
			BindBandDiscount();
		}
		/// <summary>
		/// After page changes even t handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void OnBandDiscount_PageChange(object sender, DataGridPageChangedEventArgs e)
		{
			lblValBandDiscount.Text = "";
			btnExecuteQuery.Enabled = false;
			dgBandDiscount.CurrentPageIndex = e.NewPageIndex;
			showCurrentPage();

			dgBandDiscount.SelectedIndex = -1;
			dgBandDiscount.EditItemIndex = -1;

			BindBandDiscount();
		}
		/// <summary>
		/// to refresh the page
		/// </summary>
		private void showCurrentPage()
		{
			
			int iStartIndex = dgBandDiscount.CurrentPageIndex * dgBandDiscount.PageSize;
			SessionDS m_sdsQueryBandDiscount = (SessionDS)Session["QUERY_DS"];
			m_sdsBandDiscount = SysDataManager1.QueryBandDiscount(m_sdsQueryBandDiscount,m_strAppID,m_strEnterpriseID, iStartIndex,dgBandDiscount.PageSize);
			int pgCnt = Convert.ToInt32((m_sdsBandDiscount.QueryResultMaxSize - 1))/dgBandDiscount.PageSize;
			if(pgCnt < dgBandDiscount.CurrentPageIndex)
			{
				dgBandDiscount.CurrentPageIndex = System.Convert.ToInt32(pgCnt);
			}
			Session["SESSION_DS1"] = m_sdsBandDiscount;
			
		}

		/// <summary>
		/// Item bound event handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void OnItemBound_BandDiscount(object sender, DataGridItemEventArgs e)
		{
			msTextBox txtCode = (msTextBox)e.Item.FindControl("txtCode");
			if(txtCode!=null )
			{
				if( (int)ViewState["SCMode"] == (int)ScreenMode.Query && txtCode.Text!="" )
				{
					txtCode.Enabled = false;
				}

			}
		}
	}
}

