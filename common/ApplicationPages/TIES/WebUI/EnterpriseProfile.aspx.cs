using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.ties;
using com.common.classes;
using com.common.DAL;
using com.common.RBAC;
using com.ties.DAL;
using com.common.util;
using com.ties.classes;
using com.common.applicationpages;
namespace com.ties
{
	/// <summary>
	/// Summary description for EnterpriseProfile.
	/// </summary>
	public class EnterpriseProfile : BasePage
	{
		protected System.Web.UI.WebControls.Label lblDenseFact;
		protected System.Web.UI.WebControls.Label lblEnterpriseID;
		protected System.Web.UI.WebControls.Label lblEnterpriseName;
		protected System.Web.UI.WebControls.Label lblAddress;
		protected System.Web.UI.WebControls.Label lblCountry;
		protected System.Web.UI.WebControls.Label lblTelephone;
		protected System.Web.UI.WebControls.Label lblFax;
		protected System.Web.UI.WebControls.Label lblContact;
		protected System.Web.UI.WebControls.Label lblEmail;
		protected System.Web.UI.WebControls.Label lblWtInc;
		protected System.Web.UI.WebControls.Label lblState;
		protected System.Web.UI.WebControls.Label lblZipCode;
		protected System.Web.UI.WebControls.Label lblWtRoundMethod;
		protected System.Web.UI.WebControls.Label lblSpecialService;
		protected System.Web.UI.WebControls.Label lblTaxation;
		protected System.Web.UI.WebControls.Label lblTaxid;
		protected System.Web.UI.WebControls.Label lblEnterpriseProfile;
		protected System.Web.UI.WebControls.TextBox txtenterpriseName;
		protected System.Web.UI.WebControls.TextBox txtFax;
		protected System.Web.UI.WebControls.TextBox txtContact;
		protected System.Web.UI.WebControls.TextBox txtEmail;
		protected System.Web.UI.WebControls.TextBox txtAdd1;
		protected System.Web.UI.WebControls.TextBox txtEnterpriseID;
		protected System.Web.UI.WebControls.TextBox txtTelephone;
		protected System.Web.UI.WebControls.TextBox txtCountry;
		protected System.Web.UI.WebControls.TextBox txtAdd2;
		protected System.Web.UI.WebControls.TextBox txtZipCode;
		protected System.Web.UI.WebControls.TextBox txtTaxID;
		protected com.common.util.msTextBox txtTaxPercent;//protected System.Web.UI.WebControls.TextBox txtSundayDel;
		protected System.Web.UI.WebControls.Button btnZipSrch;
		protected System.Web.UI.WebControls.Button btnSave;
		protected System.Web.UI.WebControls.RadioButtonList rblRoundMethod;
		protected System.Web.UI.WebControls.CheckBox chkboxApplyTax;
		
		protected System.Web.UI.WebControls.DropDownList ddWtInc;
		private String m_strAppID;
		private String m_strEnterpriseID;
		protected System.Web.UI.WebControls.Label lblValMsg;
		protected System.Web.UI.WebControls.TextBox txtState;
		private String m_strCultureID;
		protected System.Web.UI.WebControls.Label Label1;
		protected System.Web.UI.WebControls.Label Label2;
		protected System.Web.UI.WebControls.DropDownList ddCurrType;
		protected System.Web.UI.WebControls.DropDownList ddCurrRoundDec;
		protected System.Web.UI.WebControls.Label Label3;
		protected System.Web.UI.WebControls.Button btnCountrySrch;
		protected System.Web.UI.HtmlControls.HtmlTable Table1;
		protected System.Web.UI.HtmlControls.HtmlTable Table4;
		protected System.Web.UI.HtmlControls.HtmlTable Table5;
		protected System.Web.UI.HtmlControls.HtmlTable Table6;
		protected System.Web.UI.HtmlControls.HtmlTable Table7;
		protected System.Web.UI.HtmlControls.HtmlTable Table8;
		protected System.Web.UI.WebControls.RegularExpressionValidator regEmailValidate;
		protected System.Web.UI.WebControls.ValidationSummary reqSummary;
		protected com.common.util.msTextBox txtPercentSur;
		protected System.Web.UI.WebControls.Label lblPercentSur;
		protected com.common.util.msTextBox txtMaxAmt;
		protected System.Web.UI.WebControls.Label lblMaxAmt;
		protected System.Web.UI.WebControls.Label lblFreeAmt;
		protected System.Web.UI.WebControls.Label lblInsurance;
		protected System.Web.UI.HtmlControls.HtmlTable Table2;
		protected System.Web.UI.WebControls.Label Label6;
		protected System.Web.UI.WebControls.DataGrid dgVAS;
		protected System.Web.UI.WebControls.Label lblTaxAmt;
		protected System.Web.UI.WebControls.RadioButtonList rblEntType;
		private String strState;
		private SessionDS m_sdsEntVAS;
		protected System.Web.UI.WebControls.Button btnInsert;
		protected System.Web.UI.WebControls.Label lblVASMsg;
		protected System.Web.UI.WebControls.DropDownList ddlDenseFact;
		protected com.common.util.msTextBox txtFreeAmt;
		protected System.Web.UI.WebControls.Label Label4;
		protected System.Web.UI.WebControls.Label Label5;
		protected System.Web.UI.WebControls.Label Label8;
		protected System.Web.UI.WebControls.Label Label9;
		protected System.Web.UI.HtmlControls.HtmlInputText txtTimeNorm;
		protected System.Web.UI.HtmlControls.HtmlInputText txtTimeSat;
		protected System.Web.UI.HtmlControls.HtmlInputText txtTimeSun;
		protected System.Web.UI.HtmlControls.HtmlInputText txtTimePub;
		protected System.Web.UI.WebControls.Label Label10;
		protected com.common.util.msTextBox txtSaturdayDel;
		protected System.Web.UI.WebControls.Button btnSaturdayDel;
		protected com.common.util.msTextBox txtSaturdayPickup;
		protected System.Web.UI.WebControls.Button btnSaturdayPickup;
		protected com.common.util.msTextBox txtSundayDel;
		protected System.Web.UI.WebControls.Button btnSundayDel;
		protected com.common.util.msTextBox txtSundayPickup;
		protected System.Web.UI.WebControls.Button btnSundayPickup;
		protected com.common.util.msTextBox txtPublicDel;
		protected System.Web.UI.WebControls.Button btnPublicDel;
		protected com.common.util.msTextBox txtPublicPickup;
		protected System.Web.UI.WebControls.Button btnPublicPickup;
		protected System.Web.UI.WebControls.CheckBox chkSat;
		protected System.Web.UI.WebControls.CheckBox chkSatPickup;
		protected System.Web.UI.WebControls.CheckBox chkSun;
		protected System.Web.UI.WebControls.CheckBox chkSunPickup;
		protected System.Web.UI.WebControls.CheckBox chkPub;
		protected System.Web.UI.WebControls.CheckBox chkPubPickup;
		DataView m_dvStatusOptions = null;

		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			//Utility utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings,Page.Session);
			m_strAppID = utility.GetAppID();
			m_strEnterpriseID = utility.GetEnterpriseID();
			m_strCultureID = utility.GetUserCulture();
			if(!Page.IsPostBack)
			{
				LoadTypeOptions();//ROUNDING METHOD IS HARD CODED
				DisplayEnterpriseProfile();	
				QueryMode();
				ExecuteQuery();

				chkboxlSpecialService_SelectedIndexChanged(null,null);

				btnSave.Attributes.Add("onclick", "javascript:return ValidateForm(" + txtTimeNorm.ClientID + "," + txtTimeSat.ClientID + "," + txtTimeSun.ClientID + "," + txtTimePub.ClientID + ");");
			}
			else
			{
				m_sdsEntVAS = (SessionDS) Session["SESSION_DS1"];
				
				int iMode = (int) ViewState["VASMode"];
				switch(iMode)
				{
					case (int) ScreenMode.Query:
						m_dvStatusOptions = GetStatusOptions(false);
						break;
					case (int) ScreenMode.Insert:
						m_dvStatusOptions = GetStatusOptions(false);
						break;
					default:
						m_dvStatusOptions = GetStatusOptions(true);
						break;
				}
				if((int)ViewState["VASOperation"]==(int)Operation.Update)
				{
					m_dvStatusOptions = GetStatusOptions(false);
				}
			}
			reqSummary.HeaderText = Utility.GetLanguageText(ResourceType.UserMessage, "PLS_ENTER_MISS_FIELD", utility.GetUserCulture());
		}
		/// <summary>
		/// To display the current enterprise profile to user for editing
		/// </summary>
		private void DisplayEnterpriseProfile()
		{
			DataSet dsEprise = SysDataManager1.GetEnterpriseProfile(m_strAppID,m_strEnterpriseID);
			DataRow dr = dsEprise.Tables[0].Rows[0];

			//add by Tumz.
			if(dr["WD_Ends"]!=null && dr["WD_Ends"].ToString()!="")
			{
				txtTimeNorm.Value = dr["WD_Ends"].ToString().Substring(0,5);
			}
			if(dr["SatWD_Ends"] !=null && dr["SatWD_Ends"].ToString() !="")
			{
				txtTimeSat.Value = dr["SatWD_Ends"].ToString().Substring(0,5);
			}
			if(dr["SunWD_Ends"] !=null && dr["SunWD_Ends"].ToString()!="")
			{
				txtTimeSun.Value = dr["SunWD_Ends"].ToString().Substring(0,5);
			}
			if(dr["PubWD_Ends"]!=null && dr["PubWD_Ends"].ToString()!="")
			{
				txtTimePub.Value = dr["PubWD_Ends"].ToString().Substring(0,5);
			}

			txtEnterpriseID.Text = dr["enterpriseid"].ToString();
			txtenterpriseName.Text = dr["enterprise_name"].ToString();
			String sEnterpriseType=dr["enterprise_type"].ToString();
			if (sEnterpriseType !="" )
			{
				rblEntType.Items.FindByValue(sEnterpriseType).Selected=true;
			}

			txtContact.Text = dr["contact_person"].ToString();
			txtTelephone.Text = dr["contact_telephone"].ToString();
			txtAdd1.Text = dr["address1"].ToString();
			txtAdd2.Text = dr["address2"].ToString();
			txtCountry.Text = dr["country"].ToString();
			if(dr["wt_rounding_method"].ToString()!="")
			{
				rblRoundMethod.Items.FindByValue(dr["wt_rounding_method"].ToString()).Selected = true;
			}
			if(dr["currency"].ToString()!="")
			{
				ddCurrType.Items.FindByValue(dr["currency"].ToString()).Selected = true;				
			}
			if(dr["currency_decimal"].ToString()!="")
			{
				ddCurrRoundDec.Items.FindByValue(dr["currency_decimal"].ToString()).Selected=true;
			}
			txtZipCode.Text = dr["zipcode"].ToString();
			if((dr["wt_increment_amt"]!= null) && (dr["wt_increment_amt"].ToString()!="") && (!dr["wt_increment_amt"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				String tmpStr  = Convert.ToString(dr["wt_increment_amt"]).Trim();
				String[] ArrOfStr = null;
				String FStr = "";
				String SStr = "";
				int testIndex = tmpStr.IndexOf(".", 0);

				if (testIndex > 0)
				{
					ArrOfStr = tmpStr.Split('.');
					
					if (ArrOfStr[0].ToString().Trim() == "")
						FStr = "0";
					else
						FStr = ArrOfStr[0].ToString();

					if(ArrOfStr[1].ToString().Trim() == "")
						SStr = "0";
					else
						SStr = ArrOfStr[1].ToString();

					tmpStr = FStr + "." + SStr;
				}
				else
				{
					tmpStr = tmpStr + ".0";
				}
				
				ddWtInc.Items.FindByText(tmpStr).Selected = true;
				//ddWtInc.SelectedItem.Value = dr["wt_increment_amt"].ToString();
			}
			decimal strFree_Amt=Convert.ToDecimal(dr["free_insurance_amt"]);			
			txtFreeAmt.Text = strFree_Amt.ToString("#0.00");
			/*if(dr["free_insurance_amt"].ToString().IndexOf(".",0)<=0)
			{
				txtFreeAmt.Text += ".00";
			}Comment by SUTHAT 12/12/2003*/
			decimal strmax_ins=Convert.ToDecimal(dr["max_insurance_amt"]);		
			txtMaxAmt.Text = strmax_ins.ToString("#0.00");
			/*if(dr["max_insurance_amt"].ToString().IndexOf(".",0)<=0)
			{
				txtMaxAmt.Text+=".00";
			}Comment by SUTHAT 12/12/2003*/
			txtPercentSur.Text = dr["insurance_percent_surcharge"].ToString();
			//			txtDenseFact.Text = dr["density_factor"].ToString();

			ddlDenseFact.DataSource = (ICollection)GetEnterpriseTypeOptions(true);
			ddlDenseFact.DataTextField = "Text";
			ddlDenseFact.DataValueField = "StringValue";
			ddlDenseFact.DataBind();

			if((dr["density_factor"]!= null) && (!dr["density_factor"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				int iDensityFactor=Convert.ToInt32(dr["density_factor"]);
				String strdensity_factor = Convert.ToString(iDensityFactor);
				ddlDenseFact.SelectedIndex = ddlDenseFact.Items.IndexOf(ddlDenseFact.Items.FindByValue(strdensity_factor));
			}

			if(dr["sat_delivery_avail"].ToString()=="Y")
			{
				chkSat.Checked = true;
				txtSaturdayDel.Enabled = true;
				btnSaturdayDel.Enabled = true;
			}
			txtSaturdayDel.Text = dr["sat_delivery_vas_code"].ToString();

			if(dr["sun_delivery_avail"].ToString()=="Y")
			{
				chkSun.Checked = true;
				txtSundayDel.Enabled = true;
				btnSundayDel.Enabled = true;			
			}
			txtSundayDel.Text = dr["sun_delivery_vas_code"].ToString();

			if(dr["pub_delivery_avail"].ToString()=="Y")
			{
				chkPub.Checked = true;
				txtPublicDel.Enabled = true;
				btnPublicDel.Enabled = true;
			}
			txtPublicDel.Text = dr["pub_delivery_vas_code"].ToString();

			//################## Add New 10:39 20/3/2551 ######################			
			if(dr["sat_pickup_avail"].ToString()=="Y")
			{
				chkSatPickup.Checked = true;
				txtSaturdayPickup.Enabled = true;
				btnSaturdayPickup.Enabled = true;
			}
			txtSaturdayPickup.Text = dr["sat_pickup_vas_code"].ToString();

			if(dr["sun_pickup_avail"].ToString()=="Y")
			{
				chkSunPickup.Checked = true;
				txtSundayPickup.Enabled = true;
				btnSundayPickup.Enabled = true;			
			}
			txtSundayPickup.Text = dr["sun_pickup_vas_code"].ToString();

			if(dr["pub_pickup_avail"].ToString()=="Y")
			{
				chkPubPickup.Checked = true;
				txtPublicPickup.Enabled = true;
				btnPublicPickup.Enabled = true;
			}
			txtPublicPickup.Text = dr["pub_pickup_vas_code"].ToString();

			//################## End Add New ######################

			txtFax.Text = dr["fax"].ToString();
			txtEmail.Text = dr["email"].ToString();
			if(dr["apply_tax"].ToString()=="Y" || dr["apply_tax"].ToString()=="y")
			{
				chkboxApplyTax.Checked = true;
				txtTaxPercent.Enabled = true;
				txtTaxID.Enabled = true;
			}
			else
			{
				chkboxApplyTax.Checked = false;
				txtTaxPercent.Enabled = false;
				txtTaxID.Enabled = false;
			}
			txtTaxPercent.Text = dr["tax_percent"].ToString();
			txtTaxID.Text = dr["tax_id"].ToString();

			Zipcode zip = new Zipcode();
			zip.Populate(m_strAppID, m_strEnterpriseID,txtZipCode.Text);
			txtState.Text = zip.StateCode;	
			
			//BindComboLists(); // for Status display in Enterprise_Service_Surcharge
		}
		/// <summary>
		/// To get the currenct options to be populated in the dropdownlist
		/// </summary>
		/// <returns></returns>
		private DataView GetCurrencyOptions()
		{
			DataTable dtTypeOptions = new DataTable();
 
			dtTypeOptions.Columns.Add(new DataColumn("Text", typeof(string)));
			dtTypeOptions.Columns.Add(new DataColumn("StringValue", typeof(string)));

			ArrayList arrCode = Utility.GetCodeValues(m_strAppID,m_strCultureID,"currency",CodeValueType.StringValue);
			foreach(SystemCode sysCode in arrCode)
			{
				DataRow drEach = dtTypeOptions.NewRow();
				
				drEach["Text"] = sysCode.Text;
				drEach["StringValue"] = sysCode.StringValue;
				dtTypeOptions.Rows.Add(drEach);
			}
			DataView dvTypeOptions = new DataView(dtTypeOptions);
			return dvTypeOptions;
		}
		/// <summary>
		/// To populate the round method options on the Radio button list
		/// </summary>
		/// <returns></returns>
		private DataView GetWtRoundMethodOptions()
		{
			DataTable dtTypeOptions = new DataTable();
 
			dtTypeOptions.Columns.Add(new DataColumn("Text", typeof(string)));
			dtTypeOptions.Columns.Add(new DataColumn("NumberValue", typeof(int)));

			ArrayList arrCode = Utility.GetCodeValues(m_strAppID,m_strCultureID,"wt_rounding_method",CodeValueType.NumberValue);
			foreach(SystemCode sysCode in arrCode)
			{
				DataRow drEach = dtTypeOptions.NewRow();
				
				drEach["Text"] = sysCode.Text;
				drEach["NumberValue"] = sysCode.NumValue;
				dtTypeOptions.Rows.Add(drEach);
			}
			DataView dvTypeOptions = new DataView(dtTypeOptions);
			return dvTypeOptions;
		
		}
		/// <summary>
		/// to load dropdownlist
		/// </summary>
		private void LoadTypeOptions()
		{
			ddCurrType.DataSource = GetCurrencyOptions();
			ddCurrType.DataTextField = "Text";
			ddCurrType.DataValueField = "StringValue";
			ddCurrType.DataBind();
			
		}
		/// <summary>
		/// To open another window
		/// </summary>
		/// <param name="strUrl">an url to specify where the address is</param>
		private void OpenWindowpage(String strUrl)
		{
			ArrayList paramList = new ArrayList();
			paramList.Add(strUrl);
			String sScript = Utility.GetScript("openWindowParam.js",paramList);
			Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);	
		}
		/// <summary>
		/// To retreived data entered by the user
		/// and store it to database
		/// </summary>
		/// <returns>t returns the database</returns>
		/// 
		//		private TimeSpan GetTimeFromString(string strTime)
		//		{
		//			try
		//			{
		//				string[] strArr = strTime.Trim().Split(':');
		//				TimeSpan ts = new TimeSpan();;
		//				if(strArr.Length > 1)
		//					ts = new TimeSpan(0,Convert.ToInt32(strArr[0]),Convert.ToInt32(strArr[1]));
		//				return ts;
		//			}
		//			catch(Exception err)
		//			{
		//				TimeSpan ts = new TimeSpan();
		//				lblValMsg.Text = "Invalid Workday Time;";
		//				return ts;
		//			}
		//		}

		private DataSet GetUpdatedProfile()
		{
			DataTable dtEnterpriseProfile = new DataTable();
 
			dtEnterpriseProfile.Columns.Add(new DataColumn("enterprise_name", typeof(string)));
			dtEnterpriseProfile.Columns.Add(new DataColumn("contact_person", typeof(string)));
			dtEnterpriseProfile.Columns.Add(new DataColumn("contact_telephone", typeof(string)));
			dtEnterpriseProfile.Columns.Add(new DataColumn("address1", typeof(string)));
			dtEnterpriseProfile.Columns.Add(new DataColumn("address2", typeof(string)));
			dtEnterpriseProfile.Columns.Add(new DataColumn("country", typeof(string)));
			dtEnterpriseProfile.Columns.Add(new DataColumn("wt_rounding_method", typeof(int)));
			dtEnterpriseProfile.Columns.Add(new DataColumn("zipcode", typeof(string)));
			dtEnterpriseProfile.Columns.Add(new DataColumn("wt_increment_amt", typeof(decimal)));
			dtEnterpriseProfile.Columns.Add(new DataColumn("free_insurance_amt", typeof(decimal)));
			dtEnterpriseProfile.Columns.Add(new DataColumn("max_insurance_amt", typeof(decimal)));
			dtEnterpriseProfile.Columns.Add(new DataColumn("insurance_percent_surcharge", typeof(decimal)));
			dtEnterpriseProfile.Columns.Add(new DataColumn("density_factor", typeof(string)));
			dtEnterpriseProfile.Columns.Add(new DataColumn("sat_delivery_avail", typeof(string)));
			dtEnterpriseProfile.Columns.Add(new DataColumn("sat_delivery_vas_code", typeof(string)));
			dtEnterpriseProfile.Columns.Add(new DataColumn("sun_delivery_avail", typeof(string)));
			dtEnterpriseProfile.Columns.Add(new DataColumn("sun_delivery_vas_code", typeof(string)));
			dtEnterpriseProfile.Columns.Add(new DataColumn("pub_delivery_avail", typeof(string)));
			dtEnterpriseProfile.Columns.Add(new DataColumn("pub_delivery_vas_code", typeof(string)));

			dtEnterpriseProfile.Columns.Add(new DataColumn("sat_pickup_avail", typeof(string)));
			dtEnterpriseProfile.Columns.Add(new DataColumn("sat_pickup_vas_code", typeof(string)));
			dtEnterpriseProfile.Columns.Add(new DataColumn("sun_pickup_avail", typeof(string)));
			dtEnterpriseProfile.Columns.Add(new DataColumn("sun_pickup_vas_code", typeof(string)));
			dtEnterpriseProfile.Columns.Add(new DataColumn("pub_pickup_avail", typeof(string)));
			dtEnterpriseProfile.Columns.Add(new DataColumn("pub_pickup_vas_code", typeof(string)));

			dtEnterpriseProfile.Columns.Add(new DataColumn("fax", typeof(string)));
			dtEnterpriseProfile.Columns.Add(new DataColumn("email", typeof(string)));
			dtEnterpriseProfile.Columns.Add(new DataColumn("apply_tax", typeof(string)));
			dtEnterpriseProfile.Columns.Add(new DataColumn("tax_percent", typeof(string)));
			dtEnterpriseProfile.Columns.Add(new DataColumn("tax_id", typeof(string)));
			dtEnterpriseProfile.Columns.Add(new DataColumn("currency", typeof(string)));
			dtEnterpriseProfile.Columns.Add(new DataColumn("currency_decimal", typeof(string)));
			//add by Tumz.
			dtEnterpriseProfile.Columns.Add(new DataColumn("WD_Ends", typeof(string)));
			dtEnterpriseProfile.Columns.Add(new DataColumn("SatWD_Ends", typeof(string)));
			dtEnterpriseProfile.Columns.Add(new DataColumn("SunWD_Ends", typeof(string)));
			dtEnterpriseProfile.Columns.Add(new DataColumn("PubWD_Ends", typeof(string)));
			
			DataRow drEPrise = dtEnterpriseProfile.NewRow();


			strState = txtState.Text;
			drEPrise["enterprise_name"] = txtenterpriseName.Text;
			drEPrise["contact_person"] = txtContact.Text;
			drEPrise["contact_telephone"] = txtTelephone.Text;
			drEPrise["address1"] = txtAdd1.Text;
			drEPrise["address2"] = txtAdd2.Text;
			drEPrise["country"] = txtCountry.Text;
			drEPrise["currency"] = ddCurrType.SelectedItem.Value;
			drEPrise["currency_decimal"] = ddCurrRoundDec.SelectedItem.Value;

			//add by Tumz.
			drEPrise["WD_Ends"] = txtTimeNorm.Value;
			drEPrise["SatWD_Ends"] = txtTimeSat.Value;
			drEPrise["SunWD_Ends"] = txtTimeSun.Value;
			drEPrise["PubWD_Ends"] = txtTimePub.Value;

			if(rblRoundMethod.SelectedIndex >-1)
			{
				drEPrise["wt_rounding_method"] = rblRoundMethod.Items[rblRoundMethod.SelectedIndex].Value;
			}
			else
			{
				drEPrise["wt_rounding_method"] = System.DBNull.Value;
			}
			drEPrise["zipcode"] = txtZipCode.Text;

			drEPrise["wt_increment_amt"] = ddWtInc.SelectedItem.Value;
			if(txtFreeAmt.Text!="")
			{
				drEPrise["free_insurance_amt"] = txtFreeAmt.Text;
			}
			else
			{
				drEPrise["free_insurance_amt"] = System.DBNull.Value;
			}
			if(txtMaxAmt.Text!="")
			{
				drEPrise["max_insurance_amt"] = txtMaxAmt.Text;
			}
			else
			{
				drEPrise["max_insurance_amt"] = System.DBNull.Value;
			}
			if(txtPercentSur.Text!="")
			{
				drEPrise["insurance_percent_surcharge"] = txtPercentSur.Text;
			}
			else
			{
				drEPrise["insurance_percent_surcharge"] = System.DBNull.Value;
			}
			//drEPrise["density_factor"] = txtDenseFact.Text;
			drEPrise["density_factor"] = ddlDenseFact.SelectedItem.Value;
			getSpecialService(drEPrise);

			drEPrise["fax"] = txtFax.Text;
			drEPrise["email"] = txtEmail.Text;
			if(chkboxApplyTax.Checked)
			{
				drEPrise["apply_tax"] = "y";
				drEPrise["tax_id"] = txtTaxID.Text;
				drEPrise["tax_percent"] = txtTaxPercent.Text;
			}
			else
			{
				drEPrise["apply_tax"] = "n";
				drEPrise["tax_id"] = "";
				drEPrise["tax_percent"] = System.DBNull.Value;
			}
			dtEnterpriseProfile.Rows.Add(drEPrise);
			DataSet dsEnterpriseProfile = new DataSet();
			dsEnterpriseProfile.Tables.Add(dtEnterpriseProfile);

			return dsEnterpriseProfile;
		}
		/// <summary>
		/// To retreive the special service new input form user
		/// and store to the Datarow
		/// </summary>
		/// <param name="drEPrise">Info store to this datarow</param>
		/// <returns>returns datarow</returns>
		private DataRow getSpecialService(DataRow drEPrise)
		{
			//			for(int i=0; i<chkboxlSpecialService.Items.Count; i++)
			//			{
			//				String strValue = chkboxlSpecialService.Items[i].Value;
			//				if(strValue == "Sat")
			//				{
			//					if(chkboxlSpecialService.Items[i].Selected)
			//					{
			//						drEPrise["sat_delivery_avail"] = "Y";
			//						drEPrise["sat_delivery_vas_code"] = txtSaturdayDel.Text;
			//					}
			//					else
			//					{
			//						drEPrise["sat_delivery_avail"] = "N";
			//						drEPrise["sat_delivery_vas_code"] = "";
			//					}
			//				}
			//				if(strValue == "Sun")
			//				{
			//					if(chkboxlSpecialService.Items[i].Selected)
			//					{
			//						drEPrise["sun_delivery_avail"] = "Y";
			//						drEPrise["sun_delivery_vas_code"] = txtSundayDel.Text;
			//					
			//					}
			//					else
			//					{
			//						drEPrise["sun_delivery_avail"] = "N";
			//						drEPrise["sun_delivery_vas_code"] = "";
			//					}
			//
			//				}
			//				if(strValue == "Pub")
			//				{
			//					if(chkboxlSpecialService.Items[i].Selected)
			//					{
			//						drEPrise["pub_delivery_avail"] = "Y";
			//						drEPrise["pub_delivery_vas_code"] = txtPublicDel.Text;
			//					}
			//					else
			//					{
			//						drEPrise["pub_delivery_avail"] = "N";
			//						drEPrise["pub_delivery_vas_code"] = "";
			//					}
			//				}
			//			}

			
			drEPrise["sat_delivery_avail"] = chkSat.Checked?"Y":"N";
			drEPrise["sat_delivery_vas_code"] = chkSat.Checked?txtSaturdayDel.Text:null;
			drEPrise["sun_delivery_avail"] = chkSun.Checked?"Y":"N";
			drEPrise["sun_delivery_vas_code"] = chkSun.Checked?txtSundayDel.Text:null;
			drEPrise["pub_delivery_avail"] = chkPub.Checked?"Y":"N";
			drEPrise["pub_delivery_vas_code"] = chkPub.Checked?txtPublicDel.Text:null;
			drEPrise["sat_pickup_avail"] = chkSatPickup.Checked?"Y":"N";
			drEPrise["sat_pickup_vas_code"] = chkSatPickup.Checked?txtSaturdayPickup.Text:null;
			drEPrise["sun_pickup_avail"] = chkSunPickup.Checked?"Y":"N";
			drEPrise["sun_pickup_vas_code"] = chkSunPickup.Checked?txtSundayPickup.Text:null;
			drEPrise["pub_pickup_avail"] = chkPubPickup.Checked?"Y":"N";
			drEPrise["pub_pickup_vas_code"] = chkPubPickup.Checked?txtPublicPickup.Text:null;

			return drEPrise;
		}
		/// <summary>
		/// to check if all fields are entered
		/// </summary>
		/// <returns>return true or false</returns>
		private bool areAllFieldsEntered()
		{
			
			int needToVal = 0;
			if(txtenterpriseName.Text=="")
			{
				lblValMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"PLS_ENTER_ALL",utility.GetUserCulture());
				needToVal +=1;
			}
			if(txtFax.Text=="")
			{
				lblValMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"PLS_ENTER_ALL",utility.GetUserCulture());
				needToVal +=1;
			}
			if(txtContact.Text=="")
			{
				lblValMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"PLS_ENTER_ALL",utility.GetUserCulture());
				needToVal +=1;
			}
			if(txtEmail.Text=="")
			{
				lblValMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"PLS_ENTER_ALL",utility.GetUserCulture());
				needToVal +=1;
			}
			if(txtAdd1.Text=="" && txtAdd2.Text=="")
			{
				lblValMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"PLS_ENTER_ALL",utility.GetUserCulture());
				needToVal +=1;
			}
			if(txtTelephone.Text=="")
			{
				lblValMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"PLS_ENTER_ALL",utility.GetUserCulture());
				needToVal +=1;
			}
			if(txtCountry.Text=="")
			{
				lblValMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"PLS_ENTER_ALL",utility.GetUserCulture());
				needToVal +=1;
			}
			if(ddlDenseFact.SelectedItem.Value.ToString() =="") //(txtDenseFact.Text=="")
			{
				lblValMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"PLS_ENTER_ALL",utility.GetUserCulture());
				needToVal +=1;
			}
			if(txtZipCode.Text=="")
			{
				lblValMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"PLS_ENTER_ALL",utility.GetUserCulture());
				needToVal +=1;
			}
			if(txtPercentSur.Text=="")
			{
				lblValMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"PLS_ENTER_ALL",utility.GetUserCulture());
				needToVal +=1;
			}
			if(txtFreeAmt.Text=="")
			{
				lblValMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"PLS_ENTER_ALL",utility.GetUserCulture());
				needToVal +=1;
			}
			if(txtMaxAmt.Text=="")
			{
				lblValMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"PLS_ENTER_ALL",utility.GetUserCulture());
				needToVal +=1;
			}
			if(txtTaxID.Text=="" && txtTaxID.Enabled)
			{
				lblValMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"PLS_ENTER_ALL",utility.GetUserCulture());
				needToVal +=1;
			}
			if(txtTaxPercent.Text=="" && txtTaxPercent.Enabled)
			{
				lblValMsg.Text =Utility.GetLanguageText(ResourceType.UserMessage,"PLS_ENTER_ALL",utility.GetUserCulture());
				needToVal +=1;
			}
			
			if(rblRoundMethod.SelectedIndex<0)
			{
				lblValMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"PLS_ENTER_ALL",utility.GetUserCulture());
				needToVal +=1;
			}
			Zipcode zip = new Zipcode();
			//zip.Populate(m_strAppID, m_strEnterpriseID,txtZipCode.Text);
			zip.Populate(m_strAppID, m_strEnterpriseID,txtZipCode.Text);
			
			//String strCountry = SysDataManager1.ValidateZipCode(m_strAppID,m_strEnterpriseID,zip.Country, txtState.Text);

			String strCountry = zip.Country;
			if(strCountry=="" || strCountry==null)
			{
				lblValMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"ZIP_REQ",utility.GetUserCulture());
				needToVal +=1;
			}
			else if(zip.Country!=txtCountry.Text)
			{
				lblValMsg.Text =Utility.GetLanguageText(ResourceType.UserMessage,"INVALID_COUNTRY_NAME",utility.GetUserCulture());
				needToVal +=1;
				
			}
			//zip.Populate(m_strAppID, m_strEnterpriseID,txtZipCode.Text);
			//zip.Populate(m_strAppID, m_strEnterpriseID,txtCountry.Text,txtZipCode.Text);
			//
			//			if( strZip!=txtZipCode.Text)
			//			{
			//				lblValMsg.Text += "ZipCode entered wrongly or not entered, please enter a correct code";
			//				needToVal +=1;
			//			}
			//Comment By Aoo  11/04/2008
			//			VAS vas = new VAS();
			//			if(txtSaturdayDel.Enabled)
			//			{
			//				vas.Populate(m_strAppID, m_strEnterpriseID, txtSaturdayDel.Text);
			//				if(vas.VASCode!=txtSaturdayDel.Text)
			//				{
			//					lblValMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"INVALID_VAS_CODE",utility.GetUserCulture());
			//					needToVal +=1;
			//				}
			//				
			//			}
			//			if(txtSundayDel.Enabled)
			//			{
			//				vas.Populate(m_strAppID, m_strEnterpriseID, txtSundayDel.Text);
			//				if(vas.VASCode!=txtSundayDel.Text)
			//				{
			//					lblValMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"INVALID_VAS_CODE",utility.GetUserCulture());
			//					needToVal +=1;
			//				}
			//				
			//			}
			//			if(txtPublicDel.Enabled)
			//			{
			//				vas.Populate(m_strAppID, m_strEnterpriseID, txtPublicDel.Text);
			//				if(vas.VASCode!=txtPublicDel.Text)
			//				{
			//					lblValMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"INVALID_VAS_CODE",utility.GetUserCulture());
			//					needToVal +=1;
			//				}
			//				
			//			}
			//
			//			//Add New 9:34 21/3/2551
			//			if(txtSaturdayPickup.Enabled)
			//			{
			//				vas.Populate(m_strAppID, m_strEnterpriseID, txtSaturdayPickup.Text);
			//				if(vas.VASCode!=txtSaturdayPickup.Text)
			//				{
			//					lblValMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"INVALID_VAS_CODE",utility.GetUserCulture());
			//					needToVal +=1;
			//				}				
			//			}
			//			if(txtSundayPickup.Enabled)
			//			{
			//				vas.Populate(m_strAppID, m_strEnterpriseID, txtSundayPickup.Text);
			//				if(vas.VASCode!=txtSundayPickup.Text)
			//				{
			//					lblValMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"INVALID_VAS_CODE",utility.GetUserCulture());
			//					needToVal +=1;
			//				}
			//				
			//			}
			//			if(txtPublicPickup.Enabled)
			//			{
			//				vas.Populate(m_strAppID, m_strEnterpriseID, txtPublicPickup.Text);
			//				if(vas.VASCode!=txtPublicPickup.Text)
			//				{
			//					lblValMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"INVALID_VAS_CODE",utility.GetUserCulture());
			//					needToVal +=1;
			//				}
			//				
			//			}
			//End Comment By Aoo
			//End Add New 

			if(needToVal>0)
			{
				return false;
			}
			return true;
		}
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			base.OnInit(e);
			InitializeComponent();
		
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
			this.btnZipSrch.Click += new System.EventHandler(this.btnZipSrch_Click);
			this.btnCountrySrch.Click += new System.EventHandler(this.btnCountrySrch_Click);
			this.ddWtInc.SelectedIndexChanged += new System.EventHandler(this.ddWtInc_SelectedIndexChanged);
			this.chkboxApplyTax.CheckedChanged += new System.EventHandler(this.chkboxApplyTax_CheckedChanged);
			this.btnInsert.Click += new System.EventHandler(this.btnInsert_Click);
			this.btnSaturdayDel.Click += new System.EventHandler(this.btnSaturdayDel_Click);
			this.btnPublicPickup.Click += new System.EventHandler(this.btnPublicPickup_Click);
			this.btnPublicDel.Click += new System.EventHandler(this.btnPublicDel_Click);
			this.btnSundayDel.Click += new System.EventHandler(this.btnSundayDel_Click);
			this.chkPub.CheckedChanged += new System.EventHandler(this.chkPub_CheckedChanged);
			this.chkPubPickup.CheckedChanged += new System.EventHandler(this.chkPubPickup_CheckedChanged);
			this.chkSat.CheckedChanged += new System.EventHandler(this.chkSat_CheckedChanged);
			this.chkSatPickup.CheckedChanged += new System.EventHandler(this.chkSatPickup_CheckedChanged);
			this.chkSun.CheckedChanged += new System.EventHandler(this.chkSun_CheckedChanged);
			this.chkSunPickup.CheckedChanged += new System.EventHandler(this.chkSunPickup_CheckedChanged);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		/// <summary>
		/// On country search opens statePopup window
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void btnCountrySrch_Click(object sender, System.EventArgs e)
		{
			OpenWindowpage("ZipCodePopup.aspx?FORMID=EnterpriseProfile&COUNTRY_CID=txtCountry&STATE_CID=txtState&ZIPCODE_CID=txtZipCode&COUNTRY_TEXT="+txtCountry.Text+" ");
		
		}

		/// <summary>
		/// Opens zip code popup window to select zip code
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void btnZipSrch_Click(object sender, System.EventArgs e)
		{
			OpenWindowpage("ZipCodePopup.aspx?FORMID=EnterpriseProfile&COUNTRY_CID=txtCountry&STATE_CID=txtState&ZIPCODE_CID=txtZipCode");
		}

		/// <summary>
		/// to check if there is a check on special service
		/// then disables or enables the specified text boxes and buttons
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void chkboxlSpecialService_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if(chkSat.Checked == true)
			{
				txtSaturdayDel.Enabled = true;
				btnSaturdayDel.Enabled = true;
			}
			else
			{
				txtSaturdayDel.Enabled = false;
				txtSaturdayDel.Text = "";
				btnSaturdayDel.Enabled = false;
			}

			//////////////////////////////////////////////////////////
			if(chkSatPickup.Checked == true)
			{
				txtSaturdayPickup.Enabled = true;
				btnSaturdayPickup.Enabled = true;
			}
			else
			{
				txtSaturdayPickup.Enabled = false;
				txtSaturdayPickup.Text = "";
				btnSaturdayPickup.Enabled = false;
			}

			if(chkSat.Checked == false && chkSatPickup.Checked == false)
			{
				txtTimeSat.Disabled = true;
				txtTimeSat.Value = "";
			}
			else
				txtTimeSat.Disabled = false;

			//////////////////////////////////////////////////////////
			if(chkSun.Checked == true)
			{
				txtSundayDel.Enabled = true;
				btnSundayDel.Enabled = true;
			}
			else
			{
				txtSundayDel.Enabled = false;
				txtSundayDel.Text = "";
				btnSundayDel.Enabled = false;
			}

			//////////////////////////////////////////////////////////
			if(chkSunPickup.Checked == true)
			{
				txtSundayPickup.Enabled = true;
				btnSundayPickup.Enabled = true;
			}
			else
			{
				txtSundayPickup.Enabled = false;
				txtSundayPickup.Text = "";
				btnSundayPickup.Enabled = false;
			}

			if(chkSun.Checked == false && chkSunPickup.Checked == false)
			{
				txtTimeSun.Disabled = true;
				txtTimeSun.Value = "";
			}
			else
				txtTimeSun.Disabled = false;

			//////////////////////////////////////////////////////////
			if(chkPub.Checked == true)
			{
				txtPublicDel.Enabled = true;
				btnPublicDel.Enabled = true;
			}
			else
			{
				txtPublicDel.Enabled = false;
				txtPublicDel.Text = "";
				btnPublicDel.Enabled = false;
			}

			//////////////////////////////////////////////////////////
			if(chkPubPickup.Checked == true)
			{
				txtPublicPickup.Enabled = true;
				btnPublicPickup.Enabled = true;
			}
			else
			{
				txtPublicPickup.Enabled = false;
				txtPublicPickup.Text = "";
				btnPublicPickup.Enabled = false;
			}
			
			if(chkPub.Checked == false && chkPubPickup.Checked == false)
			{
				txtTimePub.Disabled = true;
				txtTimePub.Value = "";
			}
			else
				txtTimePub.Disabled = false;

			//////////////////////////////////////////////////////////

			//			for(int i=0; i<chkboxlSpecialService.Items.Count; i++)
			//			{
			//				String strValue = chkboxlSpecialService.Items[i].Value;
			//				if(strValue == "Sat")
			//				{
			//					if(chkboxlSpecialService.Items[i].Selected)
			//					{
			//						txtSaturdayDel.Enabled = true;
			//						btnSaturdayDel.Enabled = true;
			//					}
			//					else
			//					{
			//						txtSaturdayDel.Enabled = false;
			//						btnSaturdayDel.Enabled = false;
			//						txtSaturdayDel.Text = "";
			//					}
			//				}
			//				if(strValue == "Sun")
			//				{
			//					if(chkboxlSpecialService.Items[i].Selected)
			//					{
			//						txtSundayDel.Enabled = true;
			//						btnSundayDel.Enabled = true;
			//					}
			//					else
			//					{
			//						txtSundayDel.Enabled = false;
			//						btnSundayDel.Enabled = false;
			//						txtSaturdayDel.Text = "";
			//	
			//					}
			//				}
			//				if(strValue == "Pub")
			//				{
			//					if(chkboxlSpecialService.Items[i].Selected)
			//					{
			//						txtPublicDel.Enabled = true;
			//						btnPublicDel.Enabled = true;
			//					}
			//					else
			//					{
			//						txtPublicDel.Enabled = false;
			//						btnPublicDel.Enabled = false;
			//						txtPublicDel.Text = "";
			//					}
			//				}
			//			}
			
//			foreach(ListItem itm in chkboxlSpecialService.Items)
//			{
//				switch(itm.Value)
//				{
//					case "Sat": 
//						txtSaturdayDel.Enabled = chkSat.Checked;
//						btnSaturdayDel.Enabled = chkSat.Checked;
//						if(!chkSat.Checked) txtSaturdayDel.Text = "";
//						break;
//					case "Sun": 
//						txtSundayDel.Enabled = chkSun.Checked;
//						btnSundayDel.Enabled = chkSun.Checked;
//						if(!chkSun.Checked) txtSundayDel.Text = "";
//						break;
//					case "Pub": 
//						txtPublicDel.Enabled = chkPub.Checked;
//						btnPublicDel.Enabled = chkPub.Checked;
//						if(!chkPub.Checked) txtPublicDel.Text = "";
//						break;
//						//################## Add New 3 Case (10:39 20/3/2551) ######################
//					case "SatPickup": 
//						txtSaturdayPickup.Enabled = chkSatPickup.Checked;
//						btnSaturdayPickup.Enabled = itm.Selected;
//						if(!itm.Selected) txtSaturdayPickup.Text = "";
//						break;
//					case "SunPickup":  
//						txtSundayPickup.Enabled = itm.Selected;
//						btnSundayPickup.Enabled = itm.Selected;
//						if(!itm.Selected) txtSundayPickup.Text = "";
//						break;
//					case "PubPickup":  
//						txtPublicPickup.Enabled = itm.Selected;
//						btnPublicPickup.Enabled = itm.Selected;
//						if(!itm.Selected) txtPublicPickup.Text = "";
//						break;
//				}
//			}
			
		}
		/// <summary>
		/// To popup VASCODE window for selection
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void btnSaturdayDel_Click(object sender, System.EventArgs e)
		{
			OpenWindowpage("VASPopup.aspx?FORMID=EnterpriseProfile&VASID=txtSaturdayDel&VASCODE="+txtSaturdayDel.Text+" ");
		}

		/// <summary>
		///  To popup VASCODE window for selection
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void btnSundayDel_Click(object sender, System.EventArgs e)
		{
			OpenWindowpage("VASPopup.aspx?FORMID=EnterpriseProfile&VASID=txtSundayDel&VASCODE="+txtSundayDel.Text+" ");
		}

		/// <summary>
		///  To popup VASCODE window for selection
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void btnPublicDel_Click(object sender, System.EventArgs e)
		{
			OpenWindowpage("VASPopup.aspx?FORMID=EnterpriseProfile&VASID=txtPublicDel&VASCODE="+txtPublicDel.Text+" ");
		}

		//##########################  Add New  ########################################################
		/// <summary>
		/// To popup VASCODE window for Saturday Pickup
		/// </summary>
		protected void btnSaturdayPickup_Click(object sender, System.EventArgs e)
		{
			OpenWindowpage("VASPopup.aspx?FORMID=EnterpriseProfile&VASID=txtSaturdayPickup&VASCODE="+txtSaturdayPickup.Text+" ");
		}

		/// <summary>
		/// To popup VASCODE window for Sunday Pickup
		/// </summary>
		protected void btnSundayPickup_Click(object sender, System.EventArgs e)
		{
			OpenWindowpage("VASPopup.aspx?FORMID=EnterpriseProfile&VASID=txtSundayPickup&VASCODE="+txtSundayPickup.Text+" ");
		}

		/// <summary>
		/// To popup VASCODE window for Public Holiday Pickup
		/// </summary>
		protected void btnPublicPickup_Click(object sender, System.EventArgs e)
		{
			OpenWindowpage("VASPopup.aspx?FORMID=EnterpriseProfile&VASID=txtPublicPickup&VASCODE="+txtPublicPickup.Text+" ");
		}
		//##########################  End Add New  ########################################################

		/// <summary>
		/// To save updated fields to database
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void btnSave_Click(object sender, System.EventArgs e)
		{
//			//add by Tumz.
//			if(lblValMsg.Text != "Invalid entry for ending time of workday.")
			lblValMsg.Text = "";

			//add by Tumz.
			ChkVasCodeAvailable();
			if(lblValMsg.Text != "")
				return;

			ChkWorkdayTime();
			if(lblValMsg.Text == "")
			{
				

				DataSet dsEprise = GetUpdatedProfile();
				lblValMsg.Text= "";
				if(areAllFieldsEntered())
				{
					try
					{
						SysDataManager1.UpdateEnterpriseProfile(dsEprise,m_strAppID, m_strEnterpriseID);
						lblValMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"UPD_SUCCESSFULLY",utility.GetUserCulture());
					}
					catch(Exception err)
					{
						lblValMsg.Text = err.Message;
					}
				}
			}
			else
			{
				return;
			}
		}
		private void ChkVasCodeAvailable()
		{
			if(txtSaturdayDel.Text != "" && !SysDataManager1.GetVASD_byCode(m_strAppID,m_strEnterpriseID, txtSaturdayDel.Text))
			{lblValMsg.Text = "Incorrect VAS code. ";return;}
			if(txtSaturdayPickup.Text != "" && !SysDataManager1.GetVASD_byCode(m_strAppID,m_strEnterpriseID, txtSaturdayPickup.Text))
			{lblValMsg.Text = "Incorrect VAS code. ";return;}

			if(txtSundayDel.Text != "" && !SysDataManager1.GetVASD_byCode(m_strAppID,m_strEnterpriseID, txtSundayDel.Text))
			{lblValMsg.Text = "Incorrect VAS code. ";return;}
			if(txtSundayPickup.Text != "" && !SysDataManager1.GetVASD_byCode(m_strAppID,m_strEnterpriseID, txtSundayPickup.Text))
			{lblValMsg.Text = "Incorrect VAS code. ";return;}

			if(txtPublicDel.Text != "" && !SysDataManager1.GetVASD_byCode(m_strAppID,m_strEnterpriseID, txtPublicDel.Text))
			{lblValMsg.Text = "Incorrect VAS code. ";return;}
			if(txtPublicPickup.Text != "" && !SysDataManager1.GetVASD_byCode(m_strAppID,m_strEnterpriseID, txtPublicPickup.Text))
			{lblValMsg.Text = "Incorrect VAS code. ";return;}


			
		}
		private void ChkWorkdayTime()
		{
			if(txtTimeNorm.Value == "")
			{
				lblValMsg.Text = "Please enter Normal Workday Ends time. ";
				return;
			}

			
			string[] arrStrNor = txtTimeNorm.Value.Split(':');
			if(arrStrNor.Length == 2)
			{
				if(Convert.ToInt32(arrStrNor[0]) >= 10 && Convert.ToInt32(arrStrNor[0]) <= 20)
				{
					
					if(Convert.ToInt32(arrStrNor[1]) > 59)
					{lblValMsg.Text = "Invalid entry for ending time of workday.";return;}

					if((Convert.ToInt32(arrStrNor[0]) == 20 && Convert.ToInt32(arrStrNor[1]) > 30))
					{
						lblValMsg.Text = "Work day ending time is out of range.";return;
					}

					//return;
						
				}
				else
				{
					lblValMsg.Text = "Work day ending time is out of range.";
					return;
				}
			}
			else if (txtTimeNorm.Value != "")
			{
				lblValMsg.Text = "Invalid entry for ending time of workday.";return;
			}


			if(chkSat.Checked || chkSatPickup.Checked)
			{
				//				if(txtSaturdayDel.Text == "")
				//				{
				//					lblValMsg.Text = "Please enter Saturday Delivery. ";
				//					return;
				//				}
				//				if(txtSaturdayPickup.Text == "")
				//				{
				//					lblValMsg.Text = "Please enter Saturday Pickup.";
				//					return;
				//				}

				string[] arrStrSat = txtTimeSat.Value.Split(':');
				if(txtTimeSat.Value == "")
				{
					lblValMsg.Text = "Please specify WD ending time of day when pickup/delivery allowed on special work days. ";
					return;
				}
				else if(arrStrSat.Length != 2 && txtTimeSat.Value != "")
				{
					lblValMsg.Text = "Invalid entry for ending time of workday.";return;
				}
				else if(Convert.ToInt32(arrStrSat[0]) >= 10 && Convert.ToInt32(arrStrSat[0]) <= 20)
				{	
					if(Convert.ToInt32(arrStrSat[1]) > 59)
					{lblValMsg.Text = "Invalid entry for ending time of workday.";return;}

					if((Convert.ToInt32(arrStrSat[0]) == 20 && Convert.ToInt32(arrStrSat[1]) > 30))
					{
						lblValMsg.Text = "Work day ending time is out of range.";return;
					}

					//return;
						
				}
				else
				{
					lblValMsg.Text = "Work day ending time is out of range.";return;
				}
				
			}
			else
			{
				if(txtTimeSat.Value != "")
				{
					lblValMsg.Text = "Can not enter Saturday Workday Ends time";
					return;}
			}

			if(chkSun.Checked || chkSunPickup.Checked)
			{
				//				if(txtSundayDel.Text == "")
				//				{
				//					lblValMsg.Text = "Please enter Sunday Delivery. ";return;
				//				}
				//				if(txtSundayPickup.Text == "")
				//				{
				//					lblValMsg.Text = "Please enter Sunday Pickup. ";return;
				//				}

				string[] arrStrSun = txtTimeSun.Value.Split(':');
				if(txtTimeSun.Value == "")
				{
					lblValMsg.Text = "Please specify WD ending time of day when pickup/delivery allowed on special work days. ";return;
				}
				else if(arrStrSun.Length != 2 && txtTimeSun.Value != "")
				{
					lblValMsg.Text = "Invalid entry for ending time of workday.";return;
				}
				else if(Convert.ToInt32(arrStrSun[0]) >= 10 && Convert.ToInt32(arrStrSun[0]) <= 20)
				{
							
					if(Convert.ToInt32(arrStrSun[1]) > 59)
					{lblValMsg.Text = "Invalid entry for ending time of workday.";return;}

					if((Convert.ToInt32(arrStrSun[0]) == 20 && Convert.ToInt32(arrStrSun[1]) > 30))
					{
						lblValMsg.Text = "Work day ending time is out of range.";return;
					}
					
					//return;
				}
				else
				{
					lblValMsg.Text = "Work day ending time is out of range.";return;
				}
			}
			else
			{
				if(txtTimeSun.Value != "")
				{
					lblValMsg.Text = "Can not enter Sunday Workday Ends time";
					return;
				}
			}

			if(chkPub.Checked || chkPubPickup.Checked)
			{
				//				if(txtPublicDel.Text == ""){
				//					lblValMsg.Text = "Please enter Public Holiday Delivery. ";return;
				//				}
				//				if(txtPublicPickup.Text == ""){
				//					lblValMsg.Text = "Please enter Public Holiday Pickup. ";return;
				//				}

				string[] arrStrPub = txtTimePub.Value.Split(':');
				if(txtTimePub.Value == "")
				{
					lblValMsg.Text = "Please specify WD ending time of day when pickup/delivery allowed on special work days. ";return;
				}
				else if(arrStrPub.Length != 2 && txtTimePub.Value != "")
				{
					lblValMsg.Text = "Invalid entry for ending time of workday.";return;
				}
				else if(Convert.ToInt32(arrStrPub[0]) >= 10 && Convert.ToInt32(arrStrPub[0]) <= 20)
				{
					
					if(Convert.ToInt32(arrStrPub[1]) > 59)
					{lblValMsg.Text = "Invalid entry for ending time of workday.";return;}

					if((Convert.ToInt32(arrStrPub[0]) == 20 && Convert.ToInt32(arrStrPub[1]) > 30))
					{
						lblValMsg.Text = "Work day ending time is out of range.";return;
					}

					//return;
						
				}
				else
				{
					lblValMsg.Text = "Work day ending time is out of range.";return;
				}
			}
			else
			{
				if(txtTimePub.Value != "")
				{
					lblValMsg.Text = "Can not enter Public Holiday Workday Ends time";
					return;
				}
			}


		}

		

		/// <summary>
		/// To check if Apply Tax check box has checked or not checked will
		/// disables or enables certain textboxes
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void chkboxApplyTax_CheckedChanged(object sender, System.EventArgs e)
		{
			if(chkboxApplyTax.Checked)
			{
				txtTaxPercent.Enabled = true;
				txtTaxID.Enabled = true;
			}
			else
			{
				txtTaxPercent.Enabled = false;
				txtTaxID.Enabled = false;
			}
		}

		/* Enterprise VAS GRID
		 * methods
		 * 
		*/

		public void BindEntVASGrid()
		{
			dgVAS.VirtualItemCount = System.Convert.ToInt32(m_sdsEntVAS.QueryResultMaxSize);
			dgVAS.DataSource = m_sdsEntVAS.ds;
			dgVAS.DataBind(); 
			Session["SESSION_DS1"] = m_sdsEntVAS;
		}

		public void QueryMode()
		{
			ViewState["VASOperation"]=Operation.None;
			ViewState["VASMode"]=ScreenMode.Query;
						 	
			m_sdsEntVAS = SysDataManager2.GetEmptyEntVAS();  

			m_sdsEntVAS.DataSetRecSize = 1;
			AddRow(); 
			Session["SESSION_DS1"] = m_sdsEntVAS;
			BindEntVASGrid();
			lblVASMsg.Text="";
			
			EditHRow(true);
			dgVAS.Columns[0].Visible=false;
			dgVAS.Columns[1].Visible=false;
			LoadComboLists(true);
		}

		private void AddRow()
		{
			SysDataManager2.AddNewRowInEntVASDS(ref m_sdsEntVAS);
			BindEntVASGrid();
		}

		private void EditHRow(bool bItemIndex)
		{
			foreach (DataGridItem item in dgVAS.Items)
			{ 
				if (bItemIndex) 
					dgVAS.EditItemIndex = item.ItemIndex; 
				else 
					dgVAS.EditItemIndex = -1; 
			}
			dgVAS.DataBind();
		}

		private bool GetChangedRow(DataGridItem item, int rowIndex)
		{
			bool isValid=true;
			msTextBox txtVASCode = (msTextBox)item.FindControl("txtVASCode");
			TextBox txtVASDescp=(TextBox) item.FindControl("txtVASDescp");
			msTextBox txtEffectiveFrom=(msTextBox)item.FindControl("txtEffectiveFrom");
			DropDownList ddlStatus=(DropDownList) item.FindControl("ddlStatus");			
			
			String strVASCode="";
			String strVASDescp="";
			DateTime dtEffectiveFrom;
			String strStatusActive="";

			DataRow dr = m_sdsEntVAS.ds.Tables[0].Rows[rowIndex];
			if(txtVASCode!=null)
			{
				strVASCode= txtVASCode.Text.ToString();
				dr["vas_code"] = strVASCode;
			}
			else
			{
				isValid=false;				
			}
			if(txtVASDescp!=null)
			{
				strVASDescp= txtVASDescp.Text.ToString();
				dr["vas_description"] = strVASDescp;
			}
			else
			{
				dr["vas_description"] ="";				
			}

			if(txtEffectiveFrom!=null && txtEffectiveFrom.Text !="")
			{
				dtEffectiveFrom=DateTime.ParseExact(txtEffectiveFrom.Text,"dd/MM/yyyy",null);				
				dr["Effective_from"]=dtEffectiveFrom;
			}
			else
			{
				isValid=false;
			}

			if(ddlStatus !=null && ddlStatus.SelectedItem !=null && ddlStatus.SelectedItem.Value !="")
			{
				strStatusActive=ddlStatus.SelectedItem.Value;							
				dr["Status_Active"]=strStatusActive;
			}
			else
			{
				isValid=false;
			}

			if (isValid==true)
			{
				Session["SESSION_DS1"] = m_sdsEntVAS;
			}
			return isValid;
			
		}

		/*---*/
		private void btnQuery_Click(object sender, System.EventArgs e)
		{
			lblVASMsg.Text = "";
			dgVAS.CurrentPageIndex = 0;
			QueryMode();
		}

		private void btnInsert_Click(object sender, System.EventArgs e)
		{			
			lblVASMsg.Text = "";
			
			if((int)ViewState["VASMode"] != (int)ScreenMode.Insert || m_sdsEntVAS.ds.Tables[0].Rows.Count >= dgVAS.PageSize)
			{
				m_sdsEntVAS = SysDataManager2.GetEmptyEntVAS();
				dgVAS.EditItemIndex = m_sdsEntVAS.ds.Tables[0].Rows.Count - 1;
				dgVAS.CurrentPageIndex = 0;
				AddRow();				
			}
			else
			{
				AddRow();
			}
						
			EditHRow(false);
			btnInsert.Enabled=false;
			ViewState["VASMode"] = ScreenMode.Insert;
			ViewState["VASOperation"] = Operation.Insert;
			dgVAS.Columns[0].Visible=true;
			dgVAS.Columns[1].Visible=true;
			dgVAS.EditItemIndex = m_sdsEntVAS.ds.Tables[0].Rows.Count - 1;
			dgVAS.CurrentPageIndex = 0;
			BindEntVASGrid();	
			LoadComboLists(false);
			getPageControls(Page);
		}
		
		private void RetreiveSelectedPage()
		{
			int iStartRow = dgVAS.CurrentPageIndex * dgVAS.PageSize;
			DataSet dsQuery = (DataSet)Session["QUERY_DS"];
			
			m_sdsEntVAS = SysDataManager2.GetEntVASDS(m_strAppID, m_strEnterpriseID, iStartRow, dgVAS.PageSize, dsQuery);
			int iPageCnt = Convert.ToInt32((m_sdsEntVAS.QueryResultMaxSize - 1))/dgVAS.PageSize;
			if(iPageCnt < dgVAS .CurrentPageIndex)
			{
				dgVAS.CurrentPageIndex = System.Convert.ToInt32(iPageCnt);
			}
			BindEntVASGrid();
			Session["SESSION_DS1"] = m_sdsEntVAS;
		}

		private void ExecuteQuery()
		{
			//			QueryMode();
			lblVASMsg.Text = "";			
			ViewState["VASMode"]=ScreenMode.ExecuteQuery; 
			//Since Query mode is always one row in edit mode, the rowindex is set to zero
			int rowIndex = 0; 
			GetChangedRow(dgVAS.Items[0], rowIndex);
			Session["QUERY_DS"] = m_sdsEntVAS.ds;
			RetreiveSelectedPage();
			if(m_sdsEntVAS.ds.Tables[0].Rows.Count==0)
			{
				lblVASMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"NO_RECORD_FOUND",utility.GetUserCulture());
				dgVAS.EditItemIndex = -1; 
				return;
			}
			//set all records to non-edit mode after execute query
			EditHRow(false);
			dgVAS.Columns[0].Visible=true;
			dgVAS.Columns[1].Visible=true;
		}

		private void LoadComboLists(bool bNilValue)
		{			
			m_dvStatusOptions = GetStatusOptions(bNilValue);		
		}

		protected ICollection LoadCloseStatusOptions()
		{			
			return m_dvStatusOptions;
		}

		private DataView GetStatusOptions(bool showNilOption) 
		{
			DataTable dtStatusOptions = new DataTable();
 
			dtStatusOptions.Columns.Add(new DataColumn("Text", typeof(string)));
			dtStatusOptions.Columns.Add(new DataColumn("StringValue", typeof(string)));

			ArrayList StatusOptionArray = Utility.GetCodeValues(utility.GetAppID(),utility.GetUserCulture(),"active_status",CodeValueType.StringValue);

			//DON'T delete this NilOption, Temporarily NOT showing
			//			if(showNilOption)
			//			{
			//				DataRow drNilRow = dtStatusOptions.NewRow();
			//				drNilRow[0] = "";
			//				drNilRow[1] = "";
			//				dtStatusOptions.Rows.Add(drNilRow);
			//			}
			foreach(SystemCode StatusSysCode in StatusOptionArray)
			{
				DataRow drEach = dtStatusOptions.NewRow();
				drEach[0] = StatusSysCode.Text;
				drEach[1] = StatusSysCode.StringValue;
				dtStatusOptions.Rows.Add(drEach);
			}

			DataView dvCloseStatusOptions = new DataView(dtStatusOptions);
			return dvCloseStatusOptions;
		}


		public void dgVAS_Button(object sender, DataGridCommandEventArgs e)
		{
			int iOperation = (int)ViewState["VASOperation"];
			int iMode =(int)ViewState["VASMode"];
			String strCmdNm = e.CommandName;			
			String strtxtVASCode=null; String strVASCode=null;
			String strtxtVASDescp=null; 			
			String sUrl=null;
			String sScript=null;
			ArrayList paramList=null;
		
			if (strCmdNm.Equals("Update") || strCmdNm.Equals("Edit") || strCmdNm.Equals("Delete") || strCmdNm.Equals("Cancel"))
				return;
			if (iMode==(int)ScreenMode.ExecuteQuery)
				return;
			if (!strCmdNm.Equals("VASSearch"))
			{
				return;
			}

			msTextBox txtVASCode = (msTextBox)e.Item.FindControl("txtVASCode");
			if(txtVASCode != null)
			{
				strtxtVASCode = txtVASCode.ClientID;
				strVASCode = txtVASCode.Text;
			}
			TextBox txtVASDescp = (TextBox)e.Item.FindControl("txtVASDescp");
			if(txtVASDescp != null)
				strtxtVASDescp = txtVASDescp.ClientID;
				                		
			sUrl = "VASPopup.aspx?FORMID=EnterpriseProfile&VASID="+strtxtVASCode+"&VASCODE="+strVASCode;
			sUrl+="&VASDESC="+strtxtVASDescp;
			
			paramList = new ArrayList();
			paramList.Add(sUrl);
			sScript = Utility.GetScript("openWindowParam.js",paramList);
			Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
					
		}

		protected void dgVAS_ItemDataBound(object sender, DataGridItemEventArgs e)
		{
			if(e.Item.ItemIndex == -1)
				return;

			DataRow drSelected = m_sdsEntVAS.ds.Tables[0].Rows[e.Item.ItemIndex];

			Label lblVASDescp = (Label)e.Item.FindControl("lblVASDescp");
			if(lblVASDescp != null ) 
				lblVASDescp.Text=drSelected["vas_description"].ToString();

			if(drSelected.RowState == DataRowState.Deleted)
				return;
			
			if ((int)ViewState["VASOperation"] != (int)Operation.Insert)
			{
				msTextBox txtVASCode = (msTextBox)e.Item.FindControl("txtVASCode");
				if(txtVASCode != null ) 
					txtVASCode.ReadOnly = true;
				
				TextBox txtVASDescp = (TextBox)e.Item.FindControl("txtVASDescp");
				if(txtVASDescp != null ) 
					txtVASDescp.ReadOnly = true;

				msTextBox txtEffectiveFrom = (msTextBox)e.Item.FindControl("txtEffectiveFrom");
				if(txtEffectiveFrom != null ) 
					txtEffectiveFrom.ReadOnly = true;
			}

			if ((int)ViewState["VASMode"] == (int)ScreenMode.Insert && (int)ViewState["VASOperation"] == (int)Operation.Insert)
				e.Item.Cells[1].Enabled=false;

			if((int)ViewState["VASMode"]==(int)ScreenMode.Insert)
				e.Item.Cells[1].Enabled = false;	
			else
			{
				e.Item.Cells[1].Enabled = true;
			}

			DropDownList ddlStatus = (DropDownList)e.Item.FindControl("ddlStatus");			
			if(ddlStatus != null)
			{
				ddlStatus.DataSource = LoadCloseStatusOptions();
				ddlStatus.DataTextField = "Text";
				ddlStatus.DataValueField = "StringValue";
				ddlStatus.DataBind();
				if((drSelected["status_active"].ToString() != null) && (Utility.IsNotDBNull(drSelected["status_active"])))
				{							
					String strStatusActive = (String)drSelected["status_active"];
					ddlStatus.SelectedIndex = ddlStatus.Items.IndexOf(ddlStatus.Items.FindByValue(strStatusActive));//.FindByValue("N"));
					
				}
			}

		}

		protected void dgVAS_Cancel(object sender, DataGridCommandEventArgs e)
		{
			lblVASMsg.Text = "";
			int rowIndex = e.Item.ItemIndex;
			if ((int)ViewState["VASMode"] == (int)ScreenMode.Insert && (int)ViewState["VASOperation"] == (int)Operation.Insert)
			{				
				m_sdsEntVAS.ds.Tables[0].Rows.RemoveAt(rowIndex);				
			}
			ViewState["VASOperation"] = Operation.None;
			dgVAS.EditItemIndex = -1;
			BindEntVASGrid();		
			btnInsert.Enabled=true;
		}

		protected void dgVAS_PageChange(object sender, DataGridPageChangedEventArgs e)
		{
			lblVASMsg.Text = "";
			dgVAS.CurrentPageIndex = e.NewPageIndex;
			dgVAS.SelectedIndex = -1;
			dgVAS.EditItemIndex = -1;
			RetreiveSelectedPage();
		}
		
		protected void dgVAS_Delete(object sender, DataGridCommandEventArgs e)
		{
			if ((int)ViewState["VASOperation"] == (int)Operation.Update)
			{
				dgVAS.EditItemIndex = -1;
			}
			BindEntVASGrid();

			int rowIndex = e.Item.ItemIndex;
			m_sdsEntVAS  = (SessionDS)Session["SESSION_DS1"];
			DataRow dr =m_sdsEntVAS.ds.Tables[0].Rows[rowIndex];
			String strVASCode =(string) dr[0];
			DateTime dtEffectiveFrom =Convert.ToDateTime(dr[2]);			
			
			if (strVASCode==null || strVASCode =="")			
				return;
			if (!Utility.IsNotDBNull(dtEffectiveFrom))
				return;

			try
			{
				// delete from table
				int iRowsDeleted = SysDataManager2.DeleteEntVAS(m_strAppID, m_strEnterpriseID, strVASCode,dtEffectiveFrom);
				ArrayList paramValues = new ArrayList();
				paramValues.Add(iRowsDeleted.ToString());
				lblVASMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"DLD_SUCCESSFULLY",utility.GetUserCulture(),paramValues);
			}
			catch(ApplicationException appException)
			{
				String strMsg = appException.Message;
				if(strMsg.IndexOf("FK") != -1)
				{
					lblVASMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"CHILD_FOUND_CANNOT_DEL",utility.GetUserCulture());					
				}
				if(strMsg.ToLower().IndexOf("child record found") != -1 )
				{
					lblVASMsg.Text =  Utility.GetLanguageText(ResourceType.UserMessage,"CHILD_FOUND_CANNOT_DEL",utility.GetUserCulture());				
				}				

				Logger.LogTraceError("EntVAS.aspx.cs","dgVAS_Delete","RBAC003",appException.Message.ToString());
				return;
			}

			if((int)ViewState["VASMode"] == (int)ScreenMode.Insert)
				m_sdsEntVAS.ds.Tables[0].Rows.RemoveAt(rowIndex);
			else
			{
				RetreiveSelectedPage();
			}
			ViewState["VASOperation"] = Operation.None;
			btnInsert.Enabled=true;
			//Make the row in non-edit Mode
			EditHRow(false);
			BindEntVASGrid();
			Logger.LogDebugInfo("EntVAS","dgVAS_Delete","INF004","updating data grid...");			
			
		}

		protected void dgVAS_Update(object sender, DataGridCommandEventArgs e)
		{
			lblVASMsg.Text = "";
			bool isValid=true;

			int iRowsAffected = 0;
			int rowIndex = e.Item.ItemIndex;
			isValid=GetChangedRow(e.Item, e.Item.ItemIndex);
			if (isValid==false)
			{
				lblVASMsg.Text="Please fill the required fields.";
				return;
			}
		
			int iOperation = (int)ViewState["VASOperation"];
			try
			{
				if (iOperation == (int)Operation.Insert)
				{
					iRowsAffected = SysDataManager2.InsertEntVAS(m_strAppID, m_strEnterpriseID,m_sdsEntVAS.ds,rowIndex);
					if (iRowsAffected > 0)
					{
						ArrayList paramValues = new ArrayList();
						paramValues.Add(iRowsAffected.ToString());
						lblVASMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"ROWS_INS",utility.GetUserCulture(),paramValues);;
					}
					else
					{
						lblVASMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage, "VAS_PRESENT", utility.GetUserCulture());
						return;
					}
				}
				else
				{
					DataSet dsChangedRow = m_sdsEntVAS.ds.GetChanges();
					iRowsAffected = SysDataManager2.UpdateEntVAS(m_strAppID, m_strEnterpriseID,dsChangedRow);
					ArrayList paramValues = new ArrayList();
					paramValues.Add(iRowsAffected.ToString());
					lblVASMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"ROWS_UPD",utility.GetUserCulture(),paramValues);;					
				}
			}
			catch(ApplicationException appException)
			{
				String strMsg = appException.Message;
				strMsg = appException.InnerException.Message;
				strMsg = appException.InnerException.InnerException.Message;
				if((strMsg.IndexOf("PRIMARY KEY") != -1) || (strMsg.IndexOf("unique") != -1))
					lblVASMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"NO_DUPLICATE",utility.GetUserCulture());
				else if(strMsg.IndexOf("duplicate key") != -1 )
					lblVASMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"NO_DUPLICATE",utility.GetUserCulture());	
				else if(strMsg.IndexOf("FOREIGN KEY") != -1 )
					lblVASMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"FOREIGN_KEY_FOUND",utility.GetUserCulture());
				else
				{
					lblVASMsg.Text =strMsg;
				}
				
				Logger.LogTraceError("DeliveryPathCode.aspx.cs","dgState_Update","RBAC003",appException.Message.ToString());
				return;
			}

			if (iRowsAffected == 0)
			{
				return;
			}

			m_sdsEntVAS.ds.Tables[0].Rows[e.Item.ItemIndex].AcceptChanges();
			ViewState["VASOperation"] = Operation.None;
			btnInsert.Enabled=true;
			dgVAS.EditItemIndex = -1;
			BindEntVASGrid();
			Logger.LogDebugInfo("State","dgDelvPath_Update","INF004","updating data grid...");			
		}

		protected void  dgVAS_Edit(object sender, DataGridCommandEventArgs e)
		{		
			lblVASMsg.Text = "";
			btnInsert.Enabled=false;
			if((int)ViewState["VASMode"] == (int)ScreenMode.Insert && (int) ViewState["VASOperation"] == (int)Operation.Insert &&  dgVAS.EditItemIndex > 0)
			{
				m_sdsEntVAS.ds.Tables[0].Rows.RemoveAt(dgVAS.EditItemIndex);		
				dgVAS.CurrentPageIndex = 0;
			}
			dgVAS.EditItemIndex = e.Item.ItemIndex;
			ViewState["VASOperation"] = Operation.Update;
			BindEntVASGrid();
		}
		
		private DataView GetEnterpriseTypeOptions(bool showNilOption) 
		{
			
			DataTable dtDensityFactorOptions = new DataTable();
			
			dtDensityFactorOptions.Columns.Add(new DataColumn("Text", typeof(string)));
			dtDensityFactorOptions.Columns.Add(new DataColumn("StringValue", typeof(string)));

			ArrayList mbgOptionArray = Utility.GetCodeValues(utility.GetAppID(),utility.GetUserCulture(),"density_factor",CodeValueType.StringValue);


			if(showNilOption)
			{
				DataRow drNilRow = dtDensityFactorOptions.NewRow();
				drNilRow[0] = "";
				drNilRow[1] = "";
				dtDensityFactorOptions.Rows.Add(drNilRow);
			}
	


			foreach(SystemCode mbgSysCode in mbgOptionArray)
			{
				DataRow drEach = dtDensityFactorOptions.NewRow();
				drEach[0] = mbgSysCode.Text;
				drEach[1] = mbgSysCode.StringValue;
				dtDensityFactorOptions.Rows.Add(drEach);
			}

			DataView dvDensityFactorOptions = new DataView(dtDensityFactorOptions);
			return dvDensityFactorOptions;
		}

		private void ddWtInc_SelectedIndexChanged(object sender, System.EventArgs e)
		{
		
		}

		private void chkSat_CheckedChanged(object sender, System.EventArgs e)
		{
			if(chkSat.Checked)
			{
				txtSaturdayDel.Enabled = true;
				btnSaturdayDel.Enabled = true;
				txtTimeSat.Disabled = false;
			}
			else
			{
				txtSaturdayDel.Enabled = false;
				txtSaturdayDel.Text = "";
				btnSaturdayDel.Enabled = false;
			}
			
			if(chkSat.Checked == false && chkSatPickup.Checked == false)
			{
				txtTimeSat.Disabled = true;
				txtTimeSat.Value = "";
			}
			else
				txtTimeSat.Disabled = false;
		}

		private void chkSatPickup_CheckedChanged(object sender, System.EventArgs e)
		{
			if(chkSatPickup.Checked)
			{
				txtSaturdayPickup.Enabled = true;
				btnSaturdayPickup.Enabled = true;
				txtTimeSat.Disabled = false;
			}
			else
			{
				txtSaturdayPickup.Enabled = false;
				txtSaturdayPickup.Text = "";
				btnSaturdayPickup.Enabled = false;
			}
			
			if(chkSat.Checked == false && chkSatPickup.Checked == false)
			{
				txtTimeSat.Disabled = true;
				txtTimeSat.Value = "";
			}
			else
				txtTimeSat.Disabled = false;
		}

		private void chkSun_CheckedChanged(object sender, System.EventArgs e)
		{
			if(chkSun.Checked)
			{
				txtSundayDel.Enabled = true;
				btnSundayDel.Enabled = true;
				txtTimeSun.Disabled = false;
			}
			else
			{
				txtSundayDel.Enabled = false;
				txtSundayDel.Text = "";
				btnSundayDel.Enabled = false;
			}
			
			if(chkSun.Checked == false && chkSunPickup.Checked == false)
			{
				txtTimeSun.Disabled = true;
				txtTimeSun.Value = "";
			}
			else
				txtTimeSun.Disabled = false;
		}

		private void chkSunPickup_CheckedChanged(object sender, System.EventArgs e)
		{
			if(chkSunPickup.Checked)
			{
				txtSundayPickup.Enabled = true;
				btnSundayPickup.Enabled = true;
				txtTimeSun.Disabled = false;
			}
			else
			{
				txtSundayPickup.Enabled = false;
				txtSundayPickup.Text = "";
				btnSundayPickup.Enabled = false;
			}
			
			if(chkSun.Checked == false && chkSunPickup.Checked == false)
			{
				txtTimeSun.Disabled = true;
				txtTimeSun.Value = "";
			}
			else
				txtTimeSun.Disabled = false;
		}

		private void chkPub_CheckedChanged(object sender, System.EventArgs e)
		{
			if(chkPub.Checked)
			{
				txtPublicDel.Enabled = true;
				btnPublicDel.Enabled = true;
				txtTimePub.Disabled = false;
			}
			else
			{
				txtPublicDel.Enabled = false;
				txtPublicDel.Text = "";
				btnPublicDel.Enabled = false;
			}

			if(chkPub.Checked == false && chkPubPickup.Checked == false)
			{
				txtTimePub.Disabled = true;
				txtTimePub.Value = "";
			}
			else
				txtTimePub.Disabled = false;
		}

		private void chkPubPickup_CheckedChanged(object sender, System.EventArgs e)
		{
			if(chkPubPickup.Checked)
			{
				txtPublicPickup.Enabled = true;
				btnPublicPickup.Enabled = true;
				txtTimePub.Disabled = false;
			}
			else
			{
				txtPublicPickup.Enabled = false;
				txtPublicPickup.Text = "";
				btnPublicPickup.Enabled = false;
			}

			if(chkPub.Checked == false && chkPubPickup.Checked == false)
			{
				txtTimePub.Disabled = true;
				txtTimePub.Value = "";
			}
			else
				txtTimePub.Disabled = false;
		}

	}
}
