<%@ Register TagPrefix="dbCombo" Namespace="Cambro.Web.DbCombo" Assembly="Cambro.Web.DbCombo" %>
<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<%@ Page language="c#" Codebehind="ManualRatingOverride.aspx.cs" AutoEventWireup="false" Inherits="com.ties.ManualRatingOverride" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>PostDeliveryCOD</title>
		<LINK href="css/Styles.css" type="text/css" rel="stylesheet" name="stylesheet">
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<!--#INCLUDE FILE="msFormValidations.inc"-->
	</HEAD>
	<BODY onunload="window.top.displayBanner.fnCloseAll(0);" MS_POSITIONING="GridLayout">
		<FORM id="PostDeliveryCOD" method="post" runat="server">
			<P><asp:label id="lblMainTitle" style="Z-INDEX: 102; LEFT: 7px; POSITION: absolute; TOP: 8px" runat="server" CssClass="mainTitleSize" Height="26px" Width="477px"> Manual Rating Override</asp:label></P>
			<DIV id="divMain" style="Z-INDEX: 101; LEFT: 0px; WIDTH: 838px; POSITION: relative; TOP: 46px; HEIGHT: 500px" MS_POSITIONING="GridLayout" runat="server">
				<TABLE id="MainTable" style="Z-INDEX: 101; LEFT: 0px; WIDTH: 726px; POSITION: absolute; TOP: 3px" width="726" border="0" runat="server">
					<TR vAlign="top">
						<TD vAlign="top" width="738"><asp:button id="btnQry" tabIndex="1" runat="server" CssClass="queryButton" Width="61px" CausesValidation="False" Text="Query"></asp:button><asp:button id="btnExecQry" tabIndex="2" runat="server" CssClass="queryButton" Width="130px" CausesValidation="False" Text="Execute Query"></asp:button><asp:button id="btnSave" tabIndex="3" runat="server" CssClass="queryButton" Width="66px" Text="Save"></asp:button><asp:button id="btnClearManual" tabIndex="4" runat="server" CssClass="queryButton" Width="155px" Text="Clear Manual Override"></asp:button>
							<DIV style="DISPLAY: inline; WIDTH: 166px; HEIGHT: 19px" ms_positioning="FlowLayout"></DIV>
							<asp:button id="btnGoToFirstPage" tabIndex="5" runat="server" CssClass="queryButton" Width="24px" CausesValidation="False" Text="|<"></asp:button><asp:button id="btnPreviousPage" tabIndex="6" runat="server" CssClass="queryButton" Width="24px" CausesValidation="False" Text="<"></asp:button><cc1:mstextbox id="txtGoToRec" tabIndex="7" runat="server" CssClass="textFieldRightAlign" Width="28px" Enabled="True" MaxLength="5" TextMaskType="msNumeric" NumberScale="0" NumberPrecision="8" NumberMinValue="1" NumberMaxValue="999999" AutoPostBack="True" Wrap="False"></cc1:mstextbox><asp:button id="btnNextPage" tabIndex="8" runat="server" CssClass="queryButton" Width="24px" CausesValidation="False" Text=">"></asp:button><asp:button id="btnGoToLastPage" tabIndex="9" runat="server" CssClass="queryButton" Width="24px" CausesValidation="False" Text=">|"></asp:button><asp:label id="lblErrorMsg" runat="server" CssClass="errorMsgColor" Height="19px" Width="537px"></asp:label><asp:label id="lblNumRec" runat="server" CssClass="RecMsg" Height="19px" Width="187px"></asp:label><asp:validationsummary id="PageValidationSummary" runat="server" Height="39px" Width="346px" Visible="True" ShowSummary="False" ShowMessageBox="True"></asp:validationsummary></TD>
					</TR>
					<TR>
						<TD width="726">
							<TABLE id="TblBookDtl" width="737" border="0" runat="server">
								<TR height="25">
									<TD width="3%"></TD>
									<TD style="WIDTH: 120px" width="120"><asp:label id="lblConsgmtNo" runat="server" CssClass="tableLabel" Height="15px" Width="113px">Consignment No</asp:label></TD>
									<TD style="WIDTH: 156px" width="156" colSpan="2"><asp:textbox id="txtConsigNo" tabIndex="10" runat="server" CssClass="textField" Width="141px" MaxLength="20"></asp:textbox></TD>
									<TD style="WIDTH: 191px" width="191"><asp:label id="lblRefNo" runat="server" CssClass="tableLabel" Height="15px" Width="147px">Ref No</asp:label></TD>
									<TD style="WIDTH: 146px" width="146"><asp:textbox id="txtRefNo" tabIndex="11" runat="server" CssClass="textField" Width="173px" MaxLength="20"></asp:textbox></TD>
								</TR>
								<TR height="25">
									<TD style="HEIGHT: 25px" width="3%"></TD>
									<TD style="WIDTH: 120px; HEIGHT: 25px" width="120"><asp:label id="lblRouteCode" runat="server" CssClass="tableLabel" Height="15px">Destination DC</asp:label></TD>
									<TD style="WIDTH: 156px; HEIGHT: 25px" width="156" colSpan="2"><DBCOMBO:DBCOMBO id="DbComboDestinationDC" tabIndex="12" CssClass="tableLabel" Width="25px" AutoPostBack="True" Runat="server" TextBoxColumns="18" TextUpLevelSearchButton="v" ShowDbComboLink="False" ServerMethod="DbComboDistributionCenterSelect"></DBCOMBO:DBCOMBO></TD>
									<TD style="WIDTH: 191px; HEIGHT: 25px" width="191"><asp:label id="lblDelManifest" runat="server" CssClass="tableLabel" Height="15px" Width="130px">Delivery Route Code</asp:label></TD>
									<TD style="HEIGHT: 25px" width="55%" colSpan="2"><DBCOMBO:DBCOMBO id="DbComboPathCode" tabIndex="13" CssClass="tableLabel" Width="25px" AutoPostBack="True" Runat="server" TextBoxColumns="18" TextUpLevelSearchButton="v" ShowDbComboLink="False" ServerMethod="DbComboPathCodeSelect"></DBCOMBO:DBCOMBO></TD>
								</TR>
								<TR>
									<TD style="HEIGHT: 25px" width="3%"></TD>
									<TD style="WIDTH: 120px; HEIGHT: 25px" width="120"><asp:label id="lblCustID" runat="server" CssClass="tableLabel" Height="15px" Width="95px">Customer ID</asp:label></TD>
									<TD style="WIDTH: 156px; HEIGHT: 25px" width="156" colSpan="2"><asp:dropdownlist id="ddbCustomer" tabIndex="14" runat="server" Height="69px" Width="154px" AutoPostBack="True" DataTextField="custid" DataValueField="custid"></asp:dropdownlist></TD>
									<TD style="WIDTH: 191px; HEIGHT: 25px" width="191" colSpan="3"><asp:textbox id="txtCustomer" style="TEXT-TRANSFORM: uppercase" tabIndex="15" runat="server" CssClass="textField" Width="229px" Enabled="False" MaxLength="20"></asp:textbox></TD>
								</TR>
								<TR height="25">
									<TD width="3%"></TD>
									<TD style="WIDTH: 120px" width="120"><asp:label id="lblStatusCode" runat="server" CssClass="tableLabel" Height="15px" Width="136px">Actual POD Date From</asp:label></TD>
									<TD style="WIDTH: 540px" width="540"><cc1:mstextbox id="txtAct_pod_From" tabIndex="16" runat="server" CssClass="textField" Width="100%" MaxLength="16" TextMaskType="msDate" TextMaskString="99/99/9999"></cc1:mstextbox></TD>
									<TD style="WIDTH: 102px" align="right" width="102"><asp:label id="lblExcepCode" runat="server" CssClass="tableLabel" Height="15px" Width="14px">To</asp:label></TD>
									<TD style="WIDTH: 191px" width="191"><cc1:mstextbox id="txtAct_Pod_to" tabIndex="17" runat="server" CssClass="textField" Width="100%" MaxLength="16" TextMaskType="msDate" TextMaskString="99/99/9999"></cc1:mstextbox></TD>
									<TD width="55%" colSpan="2"><asp:label id="lblActualPodDate" runat="server" CssClass="tableLabel" Height="15px"></asp:label></TD>
								</TR>
							</TABLE>
							<TABLE id="Table7" style="WIDTH: 744px; HEIGHT: 206px" width="744" align="left" border="0" runat="server">
								<TR height="25">
									<TD style="WIDTH: 45px" noWrap align="right"></TD>
									<TD style="WIDTH: 127px" noWrap width="127" colSpan="3"></TD>
									<TD style="WIDTH: 168px" noWrap align="middle" width="168"><asp:label id="Label59" runat="server" CssClass="tableLabelRight" Height="15px" Font-Size="11px">Rated</asp:label></TD>
									<TD style="WIDTH: 36px" noWrap align="middle" width="36"></TD>
									<TD style="WIDTH: 135px" noWrap align="middle" width="135"><asp:label id="Label15" runat="server" CssClass="tableLabelRight" Height="15px" Font-Size="11px">Override</asp:label></TD>
									<TD noWrap align="middle" width="50"></TD>
								</TR>
								<TR height="25">
									<TD style="WIDTH: 45px" noWrap></TD>
									<TD style="WIDTH: 127px" noWrap width="127" colSpan="3"><asp:label id="FreightChargeLabel" runat="server" CssClass="tableLabel" Height="15px" Font-Size="11px">Freight Charge</asp:label></TD>
									<TD style="WIDTH: 168px" noWrap align="right" width="168"><asp:label id="lblFreightCharge" tabIndex="100" runat="server" CssClass="tableLabelRight" Height="15px" Font-Size="11px"></asp:label></TD>
									<TD style="WIDTH: 36px" noWrap width="36"></TD>
									<TD style="WIDTH: 135px" noWrap align="right" width="135"><cc1:mstextbox id="txtFreightCharge" tabIndex="18" runat="server" CssClass="textFieldRightAlign" Width="100%" Enabled="True" MaxLength="9" TextMaskType="msNumeric" NumberScale="2" NumberPrecision="8" NumberMinValue="0" NumberMaxValue="99999999" AutoPostBack="True"></cc1:mstextbox></TD>
									<TD noWrap align="right" width="50"></TD>
								</TR>
								<TR height="25">
									<TD style="WIDTH: 45px" noWrap></TD>
									<TD style="WIDTH: 127px" noWrap width="127" colSpan="3"><asp:label id="InsruranceSurcLabel" runat="server" CssClass="tableLabel" Height="15px" Font-Size="11px">Insurance Surcharge</asp:label></TD>
									<TD style="WIDTH: 168px" noWrap align="right" width="168"><asp:label id="lblInsrSurc" tabIndex="100" runat="server" CssClass="tableLabelRight" Height="15px" Font-Size="11px"></asp:label></TD>
									<TD style="WIDTH: 36px" noWrap width="36"></TD>
									<TD style="WIDTH: 135px" noWrap align="right" width="135"><cc1:mstextbox id="txtInsrSurc" tabIndex="19" runat="server" CssClass="textFieldRightAlign" Width="100%" Enabled="True" MaxLength="9" TextMaskType="msNumeric" NumberScale="2" NumberPrecision="8" NumberMinValue="0" NumberMaxValue="99999999" AutoPostBack="True"></cc1:mstextbox></TD>
									<TD noWrap align="right" width="50"></TD>
								</TR>
								<TR height="25">
									<TD style="WIDTH: 45px" noWrap></TD>
									<TD style="WIDTH: 127px" noWrap width="127" colSpan="3"><asp:label id="OtherSurchargeLabel" runat="server" CssClass="tableLabel" Height="15px" Font-Size="11px">Other Surcharge</asp:label></TD>
									<TD style="WIDTH: 168px" noWrap align="right" width="168"><asp:label id="lblOtherSurc" tabIndex="100" runat="server" CssClass="tableLabelRight" Height="15px" Font-Size="11px"></asp:label></TD>
									<TD style="WIDTH: 36px" noWrap align="right" width="36"></TD>
									<TD style="WIDTH: 135px" noWrap align="right" width="135"><cc1:mstextbox id="txtOtherSurc" tabIndex="20" runat="server" CssClass="textFieldRightAlign" Width="100%" Enabled="True" MaxLength="9" TextMaskType="msNumeric" NumberScale="2" NumberPrecision="8" NumberMinValue="0" NumberMaxValue="99999999" AutoPostBack="True"></cc1:mstextbox></TD>
									<TD noWrap align="right" width="50"></TD>
								</TR>
								<TR height="25">
									<TD style="WIDTH: 45px" noWrap></TD>
									<TD style="WIDTH: 127px" noWrap width="127" colSpan="3"><asp:label id="TotalVASSurcLabel" runat="server" CssClass="tableLabel" Height="15px" Font-Size="11px">Total VAS Surcharge</asp:label></TD>
									<TD style="WIDTH: 168px" noWrap align="right" width="168"><asp:label id="lblTotVASSurc" tabIndex="100" runat="server" CssClass="tableLabelRight" Height="15px" Font-Size="11px"></asp:label></TD>
									<TD style="WIDTH: 36px" noWrap align="right" width="36"></TD>
									<TD style="WIDTH: 135px" noWrap align="right" width="135"><asp:label id="lblTotVASSurcOverride" tabIndex="100" runat="server" CssClass="tableLabelRight" Height="15px" Font-Size="11px"></asp:label></TD>
									<TD noWrap align="right" width="50"></TD>
								</TR>
								<TR>
									<TD style="WIDTH: 45px; HEIGHT: 20px" noWrap></TD>
									<TD style="WIDTH: 127px; HEIGHT: 20px" noWrap width="127" colSpan="3"><asp:label id="ESASurcLabel" runat="server" CssClass="tableLabel" Height="15px" Font-Size="11px">ESA Surcharge</asp:label></TD>
									<TD style="WIDTH: 168px; HEIGHT: 20px" noWrap align="right" width="168"><asp:label id="lblESASurc" tabIndex="100" runat="server" CssClass="tableLabelRight" Height="15px" Font-Size="11px"></asp:label></TD>
									<TD style="WIDTH: 36px; HEIGHT: 20px" noWrap align="right" width="36"></TD>
									<TD style="WIDTH: 135px; HEIGHT: 20px" noWrap align="right" width="135"><cc1:mstextbox id="txtESASurc" tabIndex="21" runat="server" CssClass="textFieldRightAlign" Width="100%" Enabled="True" MaxLength="9" TextMaskType="msNumeric" NumberScale="2" NumberPrecision="8" NumberMinValue="0" NumberMaxValue="99999999" AutoPostBack="True"></cc1:mstextbox></TD>
									<TD noWrap align="right" width="50"></TD>
								</TR>
								<TR>
									<TD style="WIDTH: 45px" noWrap></TD>
									<TD style="WIDTH: 127px" noWrap width="127" colSpan="3"><asp:label id="TotalAmountLabel" runat="server" CssClass="tableLabel" Height="15px" Font-Size="11px">Total Amount</asp:label></TD>
									<TD style="WIDTH: 168px" noWrap align="right" width="168"><asp:label id="lblTotAmt" runat="server" CssClass="tableLabel" Font-Size="11px" Font-Bold="True"></asp:label></TD>
									<TD style="WIDTH: 36px" noWrap align="right" width="36"></TD>
									<TD style="WIDTH: 135px" noWrap align="right" width="135"><asp:label id="lblTotAmtOverride" runat="server" CssClass="tableLabel" Font-Size="11px" Font-Bold="True"></asp:label></TD>
									<TD noWrap align="right" width="50"></TD>
								</TR>
								<TR>
									<TD style="WIDTH: 45px" noWrap></TD>
									<TD style="WIDTH: 132px" noWrap width="132" colSpan="2"><asp:label id="LastUserUpdateLabel" runat="server" CssClass="tableLabel" Height="15px" Font-Size="11px">Last User Updated</asp:label></TD>
									<TD style="WIDTH: 11px" noWrap align="left" width="11"><asp:label id="lblLastUserUpd" runat="server" CssClass="tableLabel" Font-Size="11px" Font-Bold="True"></asp:label></TD>
									<TD style="WIDTH: 168px" noWrap align="right" width="168"><asp:label id="Label53" runat="server" CssClass="tableLabelRight" Font-Size="11px">Last Updated D/T</asp:label></TD>
									<TD style="WIDTH: 36px" noWrap align="right" width="36"></TD>
									<TD style="WIDTH: 135px" noWrap align="left" width="135" colSpan="1" rowSpan="1"><asp:label id="lblLastUpdateDT" runat="server" CssClass="tableLabelRight" Font-Size="11px" Font-Bold="True"></asp:label></TD>
									<TD noWrap align="left" width="50"></TD>
								</TR>
							</TABLE>
						</TD>
					</TR>
				</TABLE>
			</DIV>
		</FORM>
	</BODY>
</HTML>
