<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<%@ Register TagPrefix="DbCombo" Namespace="Cambro.Web.DbCombo" Assembly="Cambro.Web.DbCombo" %>
<%@ Import Namespace="Cambro.Web.DbCombo" %>
<%@ Import Namespace="com.common.util" %>
<%@ Register TagPrefix="iewc" Namespace="Microsoft.Web.UI.WebControls" Assembly="Microsoft.Web.UI.WebControls, Version=1.0.2.226, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Page language="c#" Codebehind="CustomerProfile.aspx.cs" AutoEventWireup="false" Inherits="com.ties.CustomerProfile" ValidateRequest="false" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>CustomerProfile</title>
		<meta name="vs_snapToGrid" content="True">
		<meta name="vs_showGrid" content="True">
		<LINK rel="stylesheet" type="text/css" href="css/Styles.css" name="stylesheet">
		<META name="GENERATOR" content="Microsoft Visual Studio 7.0">
		<META name="CODE_LANGUAGE" content="C#">
		<META name="vs_defaultClientScript" content="JavaScript">
		<META name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<!--#INCLUDE FILE="msFormValidations.inc"-->
		<script language="javascript" src="Scripts/settingScrollPosition.js"></script>
		<script language="javascript">
//by X oct 03 08
		function setValue()
		{
			var s = document.all['inFile'];
			document.all['txtFilePath'].innerText = s.value;
			if(s.value != '') 
				document.all['btnImport'].disabled = false;
			else
				document.all['btnImport'].disabled = true;
			
		}
		
		function upBrowse()
		{
			document.all['divBrowse'].style.backgroundImage='url(images/btn-browse-up.GIF)';
		}
		
		function downBrowse()
		{
			document.all['divBrowse'].style.backgroundImage='url(images/btn-browse-down.GIF)';
		}		
		
		function clearValue()
		{
			if(document.all['txtFilePath'].innerText == '') {
				document.location.reload();
			}
		}
		
		function setOnOff()
		{
			//if(document.all['btnImport'].visible==false)
			var btnImp = document.getElementById("<%= btnImport_Show.ClientID %>");
			if(btnImp.visible == true)
			document.all['divBrowse'].style.visibility = 'visible';
			else
			document.all['divBrowse'].style.visibility = 'hidden';
		}
	
	//by X oct 03 08
	
		function clientvalidate(source, args){
			var rdoCash = document.getElementById("<%= radioBtnCash.ClientID %>");
			var rdoCredit = document.getElementById("<%= radioBtnCredit.ClientID %>");
			
				if (!rdoCash.checked && !rdoCredit.checked){
					args.IsValid = false;
				}   else {
					args.IsValid = true;
				}
			}
			
		
		
		</script>
	</HEAD>
	<BODY onload="setOnOff();" onunload="window.top.displayBanner.fnCloseAll(0);" MS_POSITIONING="GridLayout">
		<FORM id="CustomerProfile" method="post" encType="multipart/form-data" runat="server">
			<iewc:tabstrip id="TabStrip1" style="Z-INDEX: 103; LEFT: 29px; POSITION: absolute; TOP: 73px" tabIndex="10"
				runat="server" Height="35px" Width="699px" TabDefaultStyle="font-family:verdana;font-weight:bold;font-size:8pt;color:#0000ff;width:79;height:21;text-align:center;"
				TabHoverStyle="background-color:#777777" TabSelectedStyle="background-color:#0000ff;color:#000000;"
				TargetID="customerProfileMultiPage">
				<iewc:Tab Text="Customer" TargetID="CustomerPage"></iewc:Tab>
				<iewc:Tab Text="Special Information" TargetID="SpecialInfoPage" ToolTip="Customer Special Information"
					TabIndex="1"></iewc:Tab>
				<iewc:Tab Text="Reference" TargetID="ReferencePage" TabIndex="2"></iewc:Tab>
				<iewc:Tab Text="Status &amp; Exception" TargetID="StatusPage" TabIndex="3"></iewc:Tab>
				<iewc:Tab Text="Quotation Status" TargetID="QuatationPage" TabIndex="4"></iewc:Tab>
				<iewc:Tab Text="Zones" TargetID="Zones" TabIndex="5"></iewc:Tab>
				<iewc:Tab Text="VAS" TargetID="VAS" TabIndex="6"></iewc:Tab>
			</iewc:tabstrip><asp:label id="lblMainTitle" style="Z-INDEX: 116; LEFT: 29px; POSITION: absolute; TOP: 8px"
				runat="server" Height="26px" Width="477px" CssClass="mainTitleSize">Customer Profile</asp:label><asp:label id="lblNumRec" style="Z-INDEX: 115; LEFT: 460px; POSITION: absolute; TOP: 104px"
				runat="server" Height="19px" Width="274px" CssClass="RecMsg"></asp:label><asp:button id="btnQry" style="Z-INDEX: 112; LEFT: 27px; POSITION: absolute; TOP: 44px" runat="server"
				Height="22px" Width="62px" CssClass="queryButton" Text="Query" CausesValidation="False"></asp:button><asp:button id="btnExecQry" style="Z-INDEX: 102; LEFT: 89px; POSITION: absolute; TOP: 44px"
				tabIndex="1" runat="server" Height="22px" Width="130px" CssClass="queryButton" Text="Execute Query" CausesValidation="False"></asp:button><asp:button id="btnInsert" style="Z-INDEX: 110; LEFT: 219px; POSITION: absolute; TOP: 44px"
				tabIndex="2" runat="server" Height="22px" Width="62px" CssClass="queryButton" Text="Insert" CausesValidation="False"></asp:button><asp:button id="btnSave" style="Z-INDEX: 101; LEFT: 280px; POSITION: absolute; TOP: 44px" tabIndex="3"
				runat="server" Height="22px" Width="66px" CssClass="queryButton" Text="Save" CausesValidation="True"></asp:button><asp:button id="btnDelete" style="Z-INDEX: 111; LEFT: 346px; POSITION: absolute; TOP: 45px"
				tabIndex="4" runat="server" Height="21px" Width="62px" CssClass="queryButton" Text="Delete" CausesValidation="False" Visible="False"></asp:button><asp:button id="btnGoToFirstPage" style="Z-INDEX: 109; LEFT: 609px; POSITION: absolute; TOP: 43px"
				tabIndex="5" runat="server" Width="24px" CssClass="queryButton" Text="|<" CausesValidation="False"></asp:button><asp:button id="btnPreviousPage" style="Z-INDEX: 105; LEFT: 633px; POSITION: absolute; TOP: 43px"
				tabIndex="6" runat="server" Width="24px" CssClass="queryButton" Text="<" CausesValidation="False"></asp:button><asp:button id="btnNextPage" style="Z-INDEX: 106; LEFT: 679px; POSITION: absolute; TOP: 43px"
				tabIndex="8" runat="server" Width="24px" CssClass="queryButton" Text=">" CausesValidation="False"></asp:button><asp:button id="btnGoToLastPage" style="Z-INDEX: 107; LEFT: 703px; POSITION: absolute; TOP: 43px"
				tabIndex="9" runat="server" Width="24px" CssClass="queryButton" Text=">|" CausesValidation="False"></asp:button><asp:textbox id="txtCountRec" style="Z-INDEX: 108; LEFT: 657px; POSITION: absolute; TOP: 44px"
				tabIndex="7" runat="server" Width="24px"></asp:textbox><iewc:multipage id="customerProfileMultiPage" style="Z-INDEX: 104; LEFT: 32px; POSITION: absolute; TOP: 128px"
				runat="server" Height="564px" Width="734px" SelectedIndex="1">
				<iewc:PageView id="CustomerPage">
					<TABLE id="CustomerInfoTable" style="Z-INDEX: 112;WIDTH: 730px" width="730" border="0"
						runat="server">
						<TR width="100%">
							<TD width="100%">
								<fieldset>
									<legend>
										<asp:Label ID="lbl" Runat="server" CssClass="tableHeadingFieldset">Customer Information</asp:Label></legend>
									<TABLE id="tblCustInfo" width="100%" align="left" border="0" runat="server">
										<TR height="5">
											<TD width="2%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="8%"></TD>
										</TR>
										<TR height="25">
											<TD align="right" colSpan="1">
												<asp:requiredfieldvalidator id="validateAccNo" Width="100%" Runat="server" ControlToValidate="txtAccNo" Display="None"
													ErrorMessage="Account No."></asp:requiredfieldvalidator>
												<asp:label id="Label1" runat="server" Height="22px" Width="100%" CssClass="errorMsgColor">&nbsp;*&nbsp;</asp:label></TD>
											<TD colSpan="3">
												<asp:label id="lblAccNo" runat="server" Height="22px" Width="100%" CssClass="tableLabel">Account No.</asp:label></TD>
											<TD colSpan="3">
												<cc1:mstextbox id="txtAccNo" runat="server" Width="100%" CssClass="textField" AutoPostBack="False"
													MaxLength="12" TextMaskType="msUpperAlfaNumericWithUnderscore" tabIndex="11" style="text-transform:uppercase;"></cc1:mstextbox></TD>
											<TD colSpan="1"></TD>
											<TD colSpan="4">
												<asp:label id="lblRefNo" runat="server" Height="22px" Width="100%" CssClass="tableLabel">Customer Ref No</asp:label></TD>
											<TD colSpan="3">
												<cc1:mstextbox id="txtRefNo" tabIndex="12" runat="server" Width="100%" CssClass="textField" Enabled="True"
													MaxLength="12" TextMaskType="msUpperAlfaNumericWithUnderscore" style="text-transform:uppercase;"></cc1:mstextbox></TD>
											<TD colSpan="2" align="right">
												<asp:label id="lblStatus" runat="server" Height="22px" Width="100%" CssClass="tableLabel">Status</asp:label></TD>
											<TD colSpan="3">
												<asp:DropDownList id="ddlStatus" tabIndex="13" runat="server" Width="100%" CssClass="textField" Enabled="True"></asp:DropDownList></TD>
										</TR>
										<TR height="10">
											<TD colSpan="20"></TD>
										</TR>
										<tr height="25">
											<TD colSpan="1">
												<asp:requiredfieldvalidator id="reqfieldvalCustomerName" Width="100%" Runat="server" ControlToValidate="txtCustomerName"
													Display="None" ErrorMessage="Customer Name"></asp:requiredfieldvalidator>
												<asp:label id="lblreqCustomerName" runat="server" Height="22px" Width="100%" CssClass="errorMsgColor">&nbsp;*&nbsp;</asp:label></TD>
											<TD colSpan="3">
												<asp:label id="lblCustomerName" runat="server" Height="22px" Width="100%" CssClass="tableLabel">Customer Name</asp:label></TD>
											<TD colSpan="8">
												<asp:textbox id="txtCustomerName" runat="server" Width="285px" CssClass="textField" tabIndex="14"
													style="text-transform:uppercase;" MaxLength="100"></asp:textbox></TD>
											<TD align="right" colSpan="1"></TD>
											<TD colSpan="3">
												<asp:label id="lblContactPerson" runat="server" Height="22px" Width="100%" CssClass="tableLabel">Contact Person</asp:label></TD>
											<TD colSpan="4">
												<asp:textbox id="txtContactPerson" runat="server" Width="200px" CssClass="textField" tabIndex="15"
													style="text-transform:uppercase;" MaxLength="100"></asp:textbox></TD>
										</tr>
										<tr height="25">
											<TD colSpan="1">
												<asp:requiredfieldvalidator id="reqfieldvalAddress1" Width="100%" Runat="server" ControlToValidate="txtAddress1"
													Display="None" ErrorMessage="Address1"></asp:requiredfieldvalidator>
												<asp:label id="lblreqAddress1" runat="server" Height="22px" Width="100%" CssClass="errorMsgColor">&nbsp;*&nbsp;</asp:label></TD>
											<TD colSpan="3">
												<asp:label id="lblAddress1" runat="server" Height="22px" Width="100%" CssClass="tableLabel">Address 1</asp:label></TD>
											<TD colSpan="8">
												<asp:textbox id="txtAddress1" runat="server" Width="285px" CssClass="textField" tabIndex="16"
													style="text-transform:uppercase;" MaxLength="100"></asp:textbox></TD>
											<TD colSpan="1">
												<asp:requiredfieldvalidator id="reqfieldvalTelephone" Width="100%" Runat="server" ControlToValidate="txtTelephone"
													Display="None" ErrorMessage="Telephone"></asp:requiredfieldvalidator>
												<asp:label id="lblreqTelephone" runat="server" Height="22px" Width="100%" CssClass="errorMsgColor">&nbsp;*&nbsp;</asp:label></TD>
											<TD colSpan="3">
												<asp:label id="lblTelphone" runat="server" Height="22px" Width="100%" CssClass="tableLabel">Telephone</asp:label></TD>
											<TD colSpan="4">
												<asp:textbox id="txtTelephone" runat="server" Width="200px" CssClass="textField" tabIndex="21" MaxLength ="20"></asp:textbox></TD>
										</tr>
										<tr height="25">
											<TD colSpan="1">&nbsp;</TD>
											<TD colSpan="3">
												<asp:label id="lblAddress2" runat="server" Height="22px" Width="100%" CssClass="tableLabel">Address 2</asp:label></TD>
											<TD colSpan="8">
												<asp:textbox id="txtAddress2" runat="server" Width="285px" CssClass="textField" tabIndex="17"
													style="text-transform:uppercase;" MaxLength="100"></asp:textbox></TD>
											<TD colSpan="1">&nbsp;</TD>
											<TD colSpan="3">
												<asp:label id="lblFax" runat="server" Height="22px" Width="100%" CssClass="tableLabel">Fax</asp:label></TD>
											<TD colSpan="4">
												<asp:textbox id="txtFax" runat="server" Width="200px" CssClass="textField" tabIndex="22" MaxLength="20"></asp:textbox></TD>
										</tr>
										<tr height="25">
											<TD align="right" colSpan="1">
												<asp:requiredfieldvalidator id="reqfieldvalZipCode" Width="100%" Runat="server" ControlToValidate="txtZipCode"
													Display="None" ErrorMessage="Postal Code"></asp:requiredfieldvalidator>
												<asp:label id="lblreqZipCode" runat="server" Height="22px" Width="100%" CssClass="errorMsgColor">&nbsp;*&nbsp;</asp:label></TD>
											<TD colSpan="3">
												<asp:label id="lblZip" runat="server" Height="22px" Width="100%" CssClass="tableLabel">Postal Code</asp:label></TD>
											<TD colSpan="2">
												<cc1:mstextbox id="txtZipCode" runat="server" Width="100%" tabIndex="18" CssClass="textField" AutoPostBack="False"
													MaxLength="12" TextMaskType="msUpperAlfaNumericWithUnderscore"></cc1:mstextbox></TD>
											<TD colSpan="1">
												<asp:button id="btnZipcodePopup" runat="server" Width="100%" Text="..." CausesValidation="False"
													CssClass="queryButton" tabIndex="19"></asp:button></TD>
											<TD colSpan="1">
												<asp:label id="lblState" runat="server" Height="22px" Width="100%" CssClass="tableLabel">State</asp:label></TD>
											<TD colSpan="3">
												<asp:textbox id="txtState" runat="server" Width="100%" CssClass="textField" ReadOnly="True" style="text-transform:uppercase;"></asp:textbox></TD>
											<TD colSpan="2">&nbsp;</TD>
											<TD colSpan="3">
												<asp:label id="lblEmail" runat="server" Height="22px" Width="100%" CssClass="tableLabel">Email</asp:label></TD>
											<TD colSpan="4">
												<asp:textbox id="txtEmail" runat="server" Width="160px" tabIndex="23" CssClass="textField" MaxLength="50"></asp:textbox></TD>
										</tr>
										<tr height="25">
											<TD align="right" colSpan="1"></TD>
											<TD colSpan="3">
												<asp:label id="lblCountry" runat="server" Height="22px" Width="100%" CssClass="tableLabel">Country</asp:label></TD>
											<TD colSpan="4">
												<asp:textbox id="txtCountry" runat="server" Width="100%" tabIndex="20" CssClass="textField" AutoPostBack="False"
													ReadOnly="True" style="text-transform:uppercase;"></asp:textbox></TD>
											<TD colSpan="5">&nbsp;</TD>
											<TD colSpan="3">
												<asp:label id="lblCreatedBy" runat="server" Height="22px" Width="100%" CssClass="tableLabel">Created By</asp:label></TD>
											<TD colSpan="4">
												<asp:textbox id="txtCreatedBy" runat="server" Width="200px" CssClass="textField" AutoPostBack="False"
													ReadOnly="True" style="text-transform:uppercase;"></asp:textbox></TD>
										</tr>
									</TABLE>
								</fieldset>
							</TD>
						</TR>
						<TR width="100%" id="tbTrPaymentInfo" runat="server" >
							<TD width="100%">
								<fieldset>
									<legend>
										<asp:Label id="lblPyDetails" runat="server" CssClass="tableHeadingFieldset">Payment Details</asp:Label></legend>
									<TABLE id="tblPaymentInfo" width="100%" align="left" border="0" runat="server">
										<TR height="5">
											<TD width="2%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="8%"></TD>
										</TR>
										<tr>
											<TD colSpan="1">&nbsp;</TD>
											<TD colSpan="4">
												<asp:label id="lblPaymentMode" runat="server" Height="22px" Width="100%" CssClass="tableLabel">Payment Mode</asp:label>
											</TD>
											<TD colSpan="3" align="left">
												<asp:radiobutton id="radioBtnCash" runat="server" tabIndex="27" Height="15px" Text="Cash" Font-Size="Smaller"
													CssClass="tableRadioButton" onclick="event.returnValue=false;"></asp:radiobutton>
												<asp:radiobutton id="radioBtnCredit" runat="server" tabIndex="28" Height="15px" Text="Credit" Font-Size="Smaller"
													CssClass="tableRadioButton" onclick="event.returnValue=false;"></asp:radiobutton>
											</TD>
											<td colspan="4" align="left">
											</td>
											<TD colSpan="1">
												<%--<asp:requiredfieldvalidator id="RequirePaymentMode" Width="100%" Runat="server" ControlToValidate="radioBtnCash" Display="None" ErrorMessage="Payment Mode"></asp:requiredfieldvalidator>--%>
												<asp:CustomValidator ID="rdo" ClientValidationFunction="clientvalidate" EnableClientScript="True" Display="None"
													ErrorMessage="Payment Mode" Runat="server" Enabled="False"></asp:CustomValidator>
												<!--<asp:label id="lblReqPaymentMode" runat="server" Height="22px" Width="100%" CssClass="errorMsgColor">&nbsp;*&nbsp;</asp:label>-->
											</TD>
											<TD colSpan="3">
												<asp:label id="lblCreditOutstanding" runat="server" Height="22px" Width="100%" CssClass="tableLabel">Credit Available</asp:label>
											</TD>
											<TD colSpan="4">
												<cc1:mstextbox id="txtCreditOutstanding" runat="server" tabIndex="26" CssClass="textFieldRightAlign"
													MaxLength="9" TextMaskType="msNumeric" NumberMaxValue="999999" NumberMinValue="0" NumberPrecision="8"
													NumberScale="2" Visible="false"></cc1:mstextbox>
												<cc1:mstextbox id="txtCreditAvailable" runat="server" CssClass="textFieldRightAlign" Readonly="true"></cc1:mstextbox>
											</TD>
										</tr>
										<TR>
											<TD align="right" colSpan="1"></TD>
											<TD colSpan="4">
												<asp:label id="lblCreditLimit" runat="server" Height="22px" Width="100%" CssClass="tableLabel">Credit Limit</asp:label>
											</TD>
											<TD colSpan="3">
												<cc1:mstextbox id="txtCreditLimit" tabIndex="25" runat="server" CssClass="textFieldRightAlign"
													Readonly="true"></cc1:mstextbox>
											</TD>
											<TD colSpan="5">&nbsp;</TD>
											<TD colSpan="3">
												<asp:label id="lblCreditUsed" runat="server" Height="22px" Width="100%" CssClass="tableLabel">Credit Used</asp:label>
											</TD>
											<TD colSpan="4">
												<cc1:mstextbox id="txtCreditUsed" tabIndex="25" runat="server" CssClass="textFieldRightAlign" Readonly="true"></cc1:mstextbox>
											</TD>
										</TR>
										<TR>
											<TD align="right" colSpan="1"></TD>
											<TD colSpan="4">
												<asp:label id="lblCreditTerm" runat="server" Height="22px" Width="100%" CssClass="tableLabel">Credit Term</asp:label>
											</TD>
											<TD colSpan="3">
												<cc1:mstextbox id="txtCreditTerm" tabIndex="25" runat="server" CssClass="textFieldRightAlign" Readonly="true"></cc1:mstextbox>
												<!--<asp:DropDownList id="ddlCreditTerm" tabIndex="13" runat="server" Width="100%" CssClass="textField"
													Enabled="True" Visible="False"></asp:DropDownList>-->
											</TD>
											<TD colSpan="5">&nbsp;</TD>
											<TD colSpan="3">
												<asp:label id="Label23" runat="server" Height="22px" Width="100%" CssClass="tableLabel">Next Bill Placement Date</asp:label>
											</TD>
											<TD colSpan="4">
												<cc1:mstextbox id="txtNextBillPlacementDate" runat="server" tabIndex="32" Width="100%" CssClass="textField"
													AutoPostBack="true" MaxLength="12" TextMaskString="99/99/9999" TextMaskType="msDate"></cc1:mstextbox>
											</TD>
										</TR>
										<TR>
											<TD align="right" colSpan="1"></TD>
											<TD colSpan="4">
												<asp:label id="Label22" runat="server" Height="22px" Width="100%" CssClass="tableLabel">Credit Status</asp:label>
											</TD>
											<TD colSpan="3">
												<cc1:mstextbox id="txtCreditStatus" tabIndex="25" runat="server" CssClass="textFieldRightAlign"
													Readonly="true"></cc1:mstextbox>
											</TD>
											<TD colSpan="5">
												<asp:button id="btnChangeStatus" tabIndex="34" runat="server" Height="22px" Width="150px" CssClass="queryButton"
													Text="Change Status" CausesValidation="False" Enabled="False"></asp:button></TD>
											<TD colSpan="3">
												<asp:button id="btnSetBillPlacementRules" tabIndex="34" runat="server" Height="22px" Width="170px"
													CssClass="queryButton" Text="Set Bill Placement Rules" CausesValidation="False"></asp:button>
											</TD>
											<TD colSpan="4">
											</TD>
										</TR>
										<tr>
											<TD colSpan="1">&nbsp;</TD>
											<TD colSpan="4">
												<asp:label id="Label24" runat="server" Height="22px" Width="100%" CssClass="tableLabel">Credit Threshold(%)</asp:label>
											</TD>
											<TD colSpan="3">
												<cc1:mstextbox id="txtCreditThredholds" tabIndex="25" runat="server" CssClass="textFieldRightAlign"
													Readonly="true"></cc1:mstextbox>
											</TD>
											<TD colSpan="5">&nbsp;</TD>
											<TD colSpan="3">
												<asp:label id="lblActiveQuatationNo" runat="server" Height="22px" Width="100%" CssClass="tableLabel">Active Quotation</asp:label></TD>
											<TD colSpan="4">
												<cc1:mstextbox id="txtActiveQuatationNo" runat="server" tabIndex="30" Width="100%" CssClass="textField"
													MaxLength="12" TextMaskType="msUpperAlfaNumericWithUnderscore" ReadOnly="True"></cc1:mstextbox>
											</TD>
										</tr>
										<tr height="25">
											<TD colSpan="1"></TD>
											<TD colSpan="4">
												<asp:label id="lblMBG" runat="server" Height="22px" Width="100%" CssClass="tableLabel">MBG</asp:label>
											</TD>
											<TD colSpan="3">
												<asp:DropDownList id="ddlMBG" runat="server" tabIndex="29" Width="100%" CssClass="textField"></asp:DropDownList>
											</TD>
											<TD colSpan="5">&nbsp;</TD>
											<TD colSpan="7">
												<asp:CheckBox id="chkBox" runat="server" AutoPostBack="True"></asp:CheckBox>
												<asp:label id="Label20x" runat="server" Height="10px" Width="140px" CssClass="tableLabel">Customer BOX Minimum</asp:label>
												<cc1:mstextbox id="txtMinimumBox" tabIndex="25" runat="server" Width="30px" CssClass="textFieldRightAlign"
													Enabled="True" MaxLength="9" TextMaskType="msNumeric" NumberMaxValue="99" NumberMinValue="1"
													NumberPrecision="8" NumberScale="0" ReadOnly="true"></cc1:mstextbox>
											</TD>
										</tr>
										<tr runat="server" id="Remark" visible="true">
											<TD colSpan="1"></TD>
											<TD colSpan="4">
											</TD>
											<TD colSpan="2">
											</TD>
											<TD colSpan="4">&nbsp;</TD>
											<TD colSpan="3" valign="bottom">
												<asp:label id="lblRemarkBp" runat="server" Height="22px" Width="100%" CssClass="tableLabelRight">BPD Remark</asp:label>
											</TD>
											<TD colSpan="6">
												<cc1:mstextbox id="txtRemarkBp" runat="server" tabIndex="33" Width="100%" CssClass="textField"
													AutoPostBack="true" MaxLength="200" TextMaskType="msUpperAlfaNumericWithUnderscore" ReadOnly="True"></cc1:mstextbox>
											</TD>
										</tr>
										<tr>
											<TD colSpan="1"></TD>
											<TD colSpan="4">
											</TD>
											<TD colSpan="2">
											</TD>
											<TD colSpan="4">&nbsp;</TD>
											<TD colSpan="5">
											</TD>
											<TD colSpan="4"></TD>
										</tr>
										<tr height="25">
										</tr>
									</TABLE>
								</fieldset>
							</TD>
						</TR>
					</TABLE>
				</iewc:PageView>
				<iewc:PageView id="SpecialInfoPage">
					<TABLE id="Table1" style="Z-INDEX: 112; WIDTH: 730px" width="730" border="0" runat="server">
						<TR width="100%">
							<TD width="100%">
								<fieldset>
									<legend>
										<asp:Label id="Label3" runat="server" CssClass="tableHeadingFieldset">Special Information</asp:Label></legend>
									<TABLE id="tblSpecialInfo" width="100%" align="left" border="0" runat="server">
										<TR height="5">
											<TD width="2%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="8%"></TD>
										</TR>
										<tr height="25">
											<TD colSpan="1">&nbsp;</TD>
											<TD colSpan="5">
												<asp:label id="lblCustAccDispTab2" runat="server" Height="22px" Width="110px" CssClass="tableLabel">Customer Account</asp:label></TD>
											<TD colSpan="4">
												<asp:textbox id="txtCustAccDispTab2" runat="server" Width="100%" CssClass="textField" Enabled="False"></asp:textbox></TD>
											<TD align="right" colSpan="1"></TD>
											<TD colSpan="3">
												<asp:label id="lblCustNameDispTab2" runat="server" Height="22px" Width="100%" CssClass="tableLabel">Customer Name</asp:label></TD>
											<TD colSpan="6">
												<asp:textbox id="txtCustNameDispTab2" runat="server" Width="100%" CssClass="textField" Enabled="False"></asp:textbox></TD>
										</tr>
										<TR height="10">
											<TD colSpan="20"></TD>
										</TR>
										<tr height="25">
											<TD colSpan="1">&nbsp;</TD>
											<TD colSpan="5">
												<asp:label id="lblISectorCode" runat="server" Width="100%" CssClass="tableLabel" Height="22px">Industrial Sector Code</asp:label></TD>
											<TD colSpan="3">
												<cc1:mstextbox id="txtISectorCode" runat="server" tabIndex="31" Width="100%" CssClass="textField"
													MaxLength="12" TextMaskType="msUpperAlfaNumericWithUnderscore"></cc1:mstextbox></TD>
											<TD colSpan="1">
												<asp:button id="btnISectorSearch" runat="server" Width="100%" Text="..." CausesValidation="False"
													CssClass="queryButton" tabIndex="19"></asp:button></TD>
											<TD align="right" colSpan="1"></TD>
											<TD colSpan="5">
												<asp:label id="lblSalesmanID" runat="server" Width="100%" CssClass="tableLabel" Height="22px">Salesman ID</asp:label></TD>
											<TD colSpan="3">
												<asp:textbox id="txtSalesmanID" runat="server" tabIndex="33" Width="100%" CssClass="textField"
													ReadOnly="True"></asp:textbox></TD>
											<TD colSpan="1">
												<asp:button id="btnSearchSalesman" runat="server" tabIndex="51" Width="30px" CssClass="queryButton"
													Height="22px" CausesValidation="False" Text="..." OnClick="btnSearchSalesman_Click"></asp:button></TD>
										</tr>
										<tr height="25">
											<TD colSpan="1">&nbsp;</TD>
											<TD colSpan="5">
												<asp:label id="lblPromisedTotWt" runat="server" Width="100%" CssClass="tableLabel" Height="22px">Promised Total Wt</asp:label></TD>
											<TD colSpan="2">
												<cc1:mstextbox id="txtPromisedTotWt" runat="server" tabIndex="34" Width="100%" CssClass="textFieldRightAlign"
													MaxLength="6" TextMaskType="msNumeric" NumberMaxValue="999999" NumberMinValue="0" NumberPrecision="8"
													NumberScale="2"></cc1:mstextbox></TD>
											<TD colSpan="3">&nbsp;</TD>
											<TD colSpan="5">
												<asp:label id="lblInsurancePercentSurcharge" runat="server" Width="100%" CssClass="tableLabel"
													Height="22px">Insurance Surcharge Percentage</asp:label></TD>
											<TD colSpan="2">
												<cc1:mstextbox id="txtInsurancePercentSurcharge" runat="server" tabIndex="37" Width="100%" CssClass="textFieldRightAlign"
													AutoPostBack="False" MaxLength="6" TextMaskType="msNumeric" NumberMaxValue="100" NumberMinValue="0"
													NumberPrecision="6" NumberScale="2"></cc1:mstextbox></TD>
											<TD colSpan="2">&nbsp;</TD>
										</tr>
										<tr height="25">
											<TD align="right" colSpan="1"></TD>
											<TD colSpan="5">
												<asp:label id="lblPromisedPeriod" runat="server" Width="100%" CssClass="tableLabel" Height="22px">Promised Period</asp:label></TD>
											<TD colSpan="2">
												<cc1:mstextbox id="txtPromisedPeriod" runat="server" tabIndex="35" Width="100%" CssClass="textFieldRightAlign"
													AutoPostBack="False" MaxLength="4" TextMaskType="msNumeric" NumberMaxValue="9999" NumberMinValue="0"
													NumberPrecision="4" NumberScale="0"></cc1:mstextbox></TD>
											<TD colSpan="3">&nbsp;</TD>
											<TD colSpan="5">
												<asp:label id="lblFreeInsuranceAmt" runat="server" Width="100%" CssClass="tableLabel" Height="22px">Free Coverage</asp:label></TD>
											<TD colSpan="2">
												<cc1:mstextbox id="txtFreeInsuranceAmt" AutoPostBack="True" runat="server" tabIndex="38" Width="100%"
													CssClass="textFieldRightAlign" MaxLength="9" TextMaskType="msNumeric" NumberMaxValue="999999"
													NumberMinValue="0" NumberPrecision="8" NumberScale="2"></cc1:mstextbox></TD>
											<TD colSpan="2">&nbsp;</TD>
										</tr>
										<%--  HC Return Task --%>
										<tr height="25">
											<TD align="right" colSpan="1"></TD>
											<TD colSpan="5">
												<asp:label id="lblPromisedTotPkg" runat="server" Width="100%" CssClass="tableLabel" Height="22px">Promised Total Pkgs</asp:label></TD>
											<TD colSpan="2">
												<cc1:mstextbox id="txtPromisedTotPkg" runat="server" tabIndex="36" Width="100%" CssClass="textFieldRightAlign"
													MaxLength="4" TextMaskType="msNumeric" NumberMaxValue="9999" NumberMinValue="0" NumberPrecision="4"
													NumberScale="0"></cc1:mstextbox></TD>
											<TD colSpan="2">&nbsp;</TD>
											<%---Aoo---%>
											<td colspan="1">&nbsp;</td>
											<TD colSpan="5">
												<asp:label id="Label14" runat="server" Width="100%" CssClass="tableLabel" Height="22px">Maximum Coverage</asp:label></TD>
											<TD colSpan="2">
												<cc1:mstextbox id="txtMaxAmt" runat="server" AutoPostBack="True" CssClass="textFieldRightAlign"
													Width="104px" TextMaskType="msNumeric" NumberMaxValue="99999999" NumberMinValue="0" NumberPrecision="19"
													NumberScale="2"></cc1:mstextbox></TD>
											<TD colSpan="2">&nbsp;</TD>
											<%---Aoo---%>
											<%--<td colspan="1">
												<asp:requiredfieldvalidator id="validatePodSlipRequired" Width="100%" Runat="server" ControlToValidate="ddlPodSlipRequired" Display="None" ErrorMessage="HC Pod Required"></asp:requiredfieldvalidator>
												<asp:label id="lblreqPodSlipRequired" runat="server" Height="22px" Width="100%" CssClass="errorMsgColor">&nbsp;*&nbsp;</asp:label></td>
											<TD colSpan="5">
												<asp:label id="lblPodSlipRequired" runat="server" Width="100%" CssClass="tableLabel" Height="22px">HC Pod Required</asp:label>
												<asp:label id="Label7" runat="server" Width="100%" CssClass="tableLabel" Height="22px" Visible="False">Invoice Return Within(Days)</asp:label></TD>
											<TD colSpan="2">
												<asp:DropDownList id="ddlPodSlipRequired" runat="server" tabIndex="40" Width="100%" CssClass="textField"></asp:DropDownList>
												<cc1:mstextbox id="txtInvoiceReturn" runat="server" tabIndex="38" Width="0%" CssClass="textFieldRightAlign" MaxLength="2" TextMaskType="msNumeric" NumberMaxValue="20" NumberMinValue="0" NumberPrecision="8" NumberScale="2"></cc1:mstextbox></TD>
											<TD colSpan="12"></TD>--%>
										</tr>
										<tr height="25">
											<TD align="right" colSpan="1">
												<asp:requiredfieldvalidator id="validateApplyDimWt" Width="100%" Runat="server" ControlToValidate="ddlApplyDimWt"
													Display="None" ErrorMessage="Apply Dim Wt."></asp:requiredfieldvalidator>
												<asp:label id="lblreqApplyDimWt" runat="server" Height="22px" Width="100%" CssClass="errorMsgColor">&nbsp;*&nbsp;</asp:label></TD>
											<TD colSpan="5">
												<asp:label id="lblApplyDimWt" runat="server" Width="100%" CssClass="tableLabel" Height="22px">Apply Dim Wt</asp:label></TD>
											<TD colSpan="2">
												<asp:DropDownList id="ddlApplyDimWt" runat="server" tabIndex="39" Width="100%" CssClass="textField"
													AutoPostBack="False"></asp:DropDownList></TD>
											<TD colSpan="3">&nbsp;</TD>
											<%---Aoo---%>
											<TD colSpan="5">
												<asp:label id="lblreqMinInsuranceSurcharge" runat="server" Width="100%" CssClass="tableLabel"
													Height="22px">Minimum Insurance Surcharge</asp:label></TD>
											<TD colSpan="2">
												<cc1:mstextbox id="txtMinInsuranceSurcharge" runat="server" tabIndex="35" Width="100%" CssClass="textFieldRightAlign"
													AutoPostBack="True" MaxLength="9" TextMaskType="msNumeric" NumberMaxValue="100000" NumberMinValue="0"
													NumberPrecision="9" NumberScale="9"></cc1:mstextbox></TD>
											<TD colSpan="2">&nbsp;</TD>
											<%---Aoo---%>
											<%--<td colspan="1">
												<asp:requiredfieldvalidator id="validateInvoiceRequired" Width="100%" Runat="server" ControlToValidate="ddlInvoiceRequried" Display="None" ErrorMessage="HC Invoice Required"></asp:requiredfieldvalidator>
												<asp:label id="lblreqInvoiceRequired" runat="server" Height="22px" Width="100%" CssClass="errorMsgColor">&nbsp;*&nbsp;</asp:label></td>
											<TD colSpan="5">
												<asp:label id="lblInvoiceRequired" runat="server" Width="100%" CssClass="tableLabel" Height="22px">HC Invoice Required</asp:label></TD>
											<TD colSpan="2">
												<asp:DropDownList id="ddlInvoiceRequried" runat="server" tabIndex="40" Width="100%" CssClass="textField"></asp:DropDownList></TD>
											<TD colSpan="2">&nbsp;</TD>--%>
										</tr>
										<%-- HC Return Task --%>
										<tr height="25">
											<TD align="right" colSpan="1">
												<asp:requiredfieldvalidator id="validateApplyEsaSurcharge" Width="100%" Runat="server" ControlToValidate="ddlApplyEsaSurcharge"
													Display="None" ErrorMessage="Apply ESA Surcharge"></asp:requiredfieldvalidator>
												<asp:label id="lblreqApplyEsaSurcharge" runat="server" Height="22px" Width="100%" CssClass="errorMsgColor">&nbsp;*&nbsp;</asp:label></TD>
											<TD colSpan="5">
												<asp:label id="lblApplyEsaSurcharge" runat="server" Width="100%" CssClass="tableLabel" Height="22px">Apply ESA Surcharge For Pickup</asp:label></TD>
											<TD colSpan="2">
												<asp:DropDownList id="ddlApplyEsaSurcharge" runat="server" tabIndex="41" Width="100%" CssClass="textField"
													AutoPostBack="False"></asp:DropDownList></TD>
											<TD colSpan="3">&nbsp;</TD>
											<%---Aoo---%>
											<TD colSpan="5">
												<asp:label id="lblregMaxInsuranceSurcharge" runat="server" Width="100%" CssClass="tableLabel"
													Height="22px">Maximum Insurance Surcharge</asp:label></TD>
											<TD colSpan="2">
												<cc1:mstextbox id="txtMaxInsuranceSurcharge" runat="server" tabIndex="35" Width="100%" CssClass="textFieldRightAlign"
													AutoPostBack="True" MaxLength="9" TextMaskType="msNumeric" NumberMaxValue="100000" NumberMinValue="0"
													NumberPrecision="9" NumberScale="9"></cc1:mstextbox></TD>
											<TD colSpan="2">&nbsp;</TD>
											<%---Aoo---%>
											<%--<td colspan="1">&nbsp;</td>
											<TD colSpan="5">
												<asp:label id="Label14" runat="server" Width="100%" CssClass="tableLabel" Height="22px">Maximum Coverage</asp:label></TD>
											<TD colSpan="2">
												<cc1:mstextbox id="txtMaxAmt" runat="server" CssClass="textFieldRightAlign" Width="104px" TextMaskType="msNumeric" NumberMaxValue="99999999" NumberMinValue="0" NumberPrecision="19" NumberScale="2"></cc1:mstextbox></TD>
											<TD colSpan="2">&nbsp;</TD>--%>
										</tr>
										<tr height="25">
											<TD align="right" colSpan="1">
												<asp:requiredfieldvalidator id="validateApplyEsaSurchargeDel" Width="100%" Runat="server" ControlToValidate="ddlApplyEsaSurchargeDel"
													Display="None" ErrorMessage="Apply ESA Surcharge For Delivery"></asp:requiredfieldvalidator>
												<asp:label id="lblreqApplyEsaSurchargeDel" runat="server" Height="22px" Width="100%" CssClass="errorMsgColor">&nbsp;*&nbsp;</asp:label></TD>
											<TD colSpan="5">
												<asp:label id="lblApplyEsaSurchargeDel" runat="server" Width="100%" CssClass="tableLabel" Height="22px">Apply ESA Surcharge For Delivery</asp:label></TD>
											<TD colSpan="2">
												<asp:DropDownList id="ddlApplyEsaSurchargeDel" runat="server" tabIndex="41" Width="100%" CssClass="textField"
													AutoPostBack="False"></asp:DropDownList></TD>
											<TD colSpan="2">&nbsp;</TD>
											<%--Aoo--%>
											<td colspan="1">
												<asp:requiredfieldvalidator id="validatePodSlipRequired" Width="100%" Runat="server" ControlToValidate="ddlPodSlipRequired"
													Display="None" ErrorMessage="HC Pod Required"></asp:requiredfieldvalidator>
												<asp:label id="lblreqPodSlipRequired" runat="server" Height="22px" Width="100%" CssClass="errorMsgColor">&nbsp;*&nbsp;</asp:label></td>
											<TD colSpan="5">
												<asp:label id="lblPodSlipRequired" runat="server" Width="100%" CssClass="tableLabel" Height="22px">HC Pod Required</asp:label>
												<asp:label id="Label7" runat="server" Width="100%" CssClass="tableLabel" Height="22px" Visible="False">Invoice Return Within(Days)</asp:label></TD>
											<TD colSpan="2">
												<asp:DropDownList id="ddlPodSlipRequired" runat="server" tabIndex="40" Width="100%" CssClass="textField"></asp:DropDownList>
												<cc1:mstextbox id="txtInvoiceReturn" runat="server" tabIndex="38" Width="0%" CssClass="textFieldRightAlign"
													MaxLength="2" TextMaskType="msNumeric" NumberMaxValue="20" NumberMinValue="0" NumberPrecision="8"
													NumberScale="2"></cc1:mstextbox></TD>
											<TD colSpan="12"></TD>
											<%--<td colspan="1">&nbsp;</td>
											<TD colSpan="5">
												<asp:label id="Label15" runat="server" Width="100%" CssClass="tableLabel" Height="22px">Density Factor</asp:label></TD>
											<TD colSpan="2">
												<asp:DropDownList id="ddlDensityFactor" runat="server" tabIndex="41" Width="100%" CssClass="textField" AutoPostBack="False"></asp:DropDownList></TD>
											<TD colSpan="2">&nbsp;</TD>--%>
										</tr>
										<tr height="25">
											<TD align="right" colSpan="1"></TD>
											<TD colSpan="5">
												<asp:label id="Label13" runat="server" Width="100%" CssClass="tableLabel" Height="22px">COD Surcharge Amount</asp:label></TD>
											<TD colSpan="2">
												<cc1:mstextbox id="txtCODSurchargeAmt" runat="server" tabIndex="35" Width="100%" CssClass="textFieldRightAlign"
													AutoPostBack="True" MaxLength="9" TextMaskType="msNumeric" NumberMaxValue="100000" NumberMinValue="0"
													NumberPrecision="9" NumberScale="9"></cc1:mstextbox></TD>
											<TD colSpan="2">&nbsp;</TD>
											<%--Aoo--%>
											<td colspan="1">
												<asp:requiredfieldvalidator id="validateInvoiceRequired" Width="100%" Runat="server" ControlToValidate="ddlInvoiceRequried"
													Display="None" ErrorMessage="HC Invoice Required"></asp:requiredfieldvalidator>
												<asp:label id="lblreqInvoiceRequired" runat="server" Height="22px" Width="100%" CssClass="errorMsgColor">&nbsp;*&nbsp;</asp:label></td>
											<TD colSpan="5">
												<asp:label id="lblInvoiceRequired" runat="server" Width="100%" CssClass="tableLabel" Height="22px">HC Invoice Required</asp:label></TD>
											<TD colSpan="2">
												<asp:DropDownList id="ddlInvoiceRequried" runat="server" tabIndex="40" Width="100%" CssClass="textField"></asp:DropDownList></TD>
											<TD colSpan="2">&nbsp;</TD>
											<%--<td colspan="1">
												<asp:label id="Label11" runat="server" Height="22px" Width="100%" CssClass="errorMsgColor">&nbsp;*&nbsp;</asp:label>
												<asp:requiredfieldvalidator id="validateApplyCustomerType" Width="100%" Runat="server" ControlToValidate="ddlCustomerType" Display="None" ErrorMessage="Customer Type"></asp:requiredfieldvalidator></td>
											<TD colSpan="5">
												<asp:label id="Label17" runat="server" Width="100%" CssClass="tableLabel" Height="22px">Customer Type</asp:label></TD>
											<TD colSpan="2">
												<asp:DropDownList id="ddlCustomerType" runat="server" tabIndex="41" Width="100%" CssClass="textField" AutoPostBack="False"></asp:DropDownList></TD>
											<TD colSpan="2">&nbsp;</TD>--%>
										</tr>
										<tr height="25">
											<TD align="right" colSpan="1"></TD>
											<TD colSpan="5">
												<asp:label id="Label16" runat="server" Width="100%" CssClass="tableLabel" Height="22px">COD Surcharge Percentage</asp:label></TD>
											<TD colSpan="2">
												<cc1:mstextbox id="txtCODSurchargePercent" runat="server" tabIndex="35" Width="100%" CssClass="textFieldRightAlign"
													AutoPostBack="False" MaxLength="6" TextMaskType="msNumeric" NumberMaxValue="100" NumberMinValue="0"
													NumberPrecision="6" NumberScale="2"></cc1:mstextbox></TD>
											<TD colSpan="2">&nbsp;</TD>
											<%--Aoo--%>
											<td colspan="1">&nbsp;</td>
											<TD colSpan="5">
												<asp:label id="Label15" runat="server" Width="100%" CssClass="tableLabel" Height="22px">Density Factor</asp:label></TD>
											<TD colSpan="2">
												<%--<asp:DropDownList id="ddlDensityFactor" runat="server" tabIndex="41" Width="100%" CssClass="textField" AutoPostBack="False"></asp:DropDownList>--%>
												<cc1:mstextbox id="msDensityFactor" runat="server" tabIndex="35" Width="100%" CssClass="textFieldRightAlign"
													AutoPostBack="False" MaxLength="9" TextMaskType="msNumeric" NumberMaxValue="19999" NumberMinValue="1"
													NumberPrecision="10" NumberScale="0"></cc1:mstextbox></TD>
											<TD colSpan="2">&nbsp;</TD>
											<%--<TD colSpan="5">
											</TD>
											<TD colSpan="2">
											</TD>
											<TD colSpan="2">&nbsp;</TD>--%>
										</tr>
										<!--���� interface ��ǹ Maximum COD Surcharge �Ѻ Minimum COD Surcharge-->
										<tr height="25">
											<TD align="right" colSpan="1">
											</TD>
											<TD colSpan="5">
												<asp:label id="lblMaxCODSurcharge" runat="server" Width="100%" CssClass="tableLabel" Height="22px">Maximum COD Surcharge</asp:label></TD>
											<TD colSpan="2">
												<cc1:mstextbox id="txtMaxCODSurcharge" runat="server" tabIndex="35" Width="100%" CssClass="textFieldRightAlign"
													AutoPostBack="True" MaxLength="9" TextMaskType="msNumeric" NumberMaxValue="100000" NumberMinValue="1"
													NumberPrecision="9" NumberScale="9"></cc1:mstextbox></TD>
											<TD colSpan="2">&nbsp;</TD>
											<%--Aoo--%>
											<td colspan="1">
												<asp:label id="Label11" runat="server" Height="22px" Width="100%" CssClass="errorMsgColor">&nbsp;*&nbsp;</asp:label>
												<asp:requiredfieldvalidator id="validateApplyCustomerType" Width="100%" Runat="server" ControlToValidate="ddlCustomerType"
													Display="None" ErrorMessage="Customer Type"></asp:requiredfieldvalidator></td>
											<TD colSpan="5">
												<asp:label id="Label17" runat="server" Width="100%" CssClass="tableLabel" Height="22px">Customer Type</asp:label></TD>
											<TD colSpan="2">
												<asp:DropDownList id="ddlCustomerType" runat="server" tabIndex="41" Width="100%" CssClass="textField"
													AutoPostBack="False"></asp:DropDownList></TD>
											<TD colSpan="2">&nbsp;</TD>
											<%--<TD colSpan="5">
											</TD>
											<TD colSpan="2">
											</TD>
											<TD colSpan="2">&nbsp;</TD>--%>
										</tr>
										<tr height="25">
											<TD align="right" colSpan="1">
											</TD>
											<TD colSpan="5">
												<asp:label id="lblMinCODSurcharge" runat="server" Width="100%" CssClass="tableLabel" Height="22px">Minimum COD Surcharge</asp:label></TD>
											<TD colSpan="2">
												<cc1:mstextbox id="txtMinCODSurcharge" runat="server" tabIndex="35" Width="100%" CssClass="textFieldRightAlign"
													AutoPostBack="True" MaxLength="9" TextMaskType="msNumeric" NumberMaxValue="100000" NumberMinValue="1"
													NumberPrecision="9" NumberScale="9"></cc1:mstextbox></TD>
											<TD colSpan="2">&nbsp;</TD>
											<TD colSpan="1">&nbsp;</TD>
											<%--Aoo--%>
											<TD colSpan="5">
												<asp:label id="lblreqDiscountBand" runat="server" Width="100%" CssClass="tableLabel" Height="22px">Discount Band</asp:label></TD>
											<TD colSpan="2">
												<cc1:mstextbox id="msDiscountBand" runat="server" tabIndex="35" Width="100%" CssClass="textField"
													MaxLength="6"></cc1:mstextbox></TD>
											<TD colSpan="2">&nbsp;</TD>
										</tr>
										<%--Other Surcharge--%>
										<tr height="25">
											<TD align="right" colSpan="1"></TD>
											<TD colSpan="5">
												<asp:label id="lblreqOtherSurchargeAmount" runat="server" Width="100%" CssClass="tableLabel"
													Height="22px">Other Surcharge Amount</asp:label></TD>
											<TD colSpan="2">
												<cc1:mstextbox id="msOtherSurchargeAmount" runat="server" tabIndex="35" Width="100%" CssClass="textFieldRightAlign"
													AutoPostBack="True" MaxLength="9" TextMaskType="msNumeric" NumberMaxValue="100000" NumberMinValue="0"
													NumberPrecision="9" NumberScale="9"></cc1:mstextbox></TD>
											<TD colSpan="2">&nbsp;</TD>
											<td colspan="1">&nbsp;</td>
											<TD colSpan="5">
												<asp:label id="Label21" runat="server" Width="95px" CssClass="tableLabel" Height="22px">Master Account</asp:label></TD>
											<TD colSpan="2">
												<dbcombo:dbcombo id="DbComboMasterAccount" tabIndex="1" runat="server" Width="80px" AutoPostBack="True"
													ServerMethod="MasterAccountServerMethod" ShowDbComboLink="False" TextUpLevelSearchButton="F"
													TextBoxColumns="4" Debug="false"></dbcombo:dbcombo></TD>
											<TD colSpan="2">&nbsp;</TD>
										</tr>
										<tr height="25">
											<TD align="right" colSpan="1"></TD>
											<TD colSpan="5">
												<asp:label id="lblreqOtherSurchargePercentage" runat="server" Width="100%" CssClass="tableLabel"
													Height="22px">Other Surcharge Percentage</asp:label></TD>
											<TD colSpan="2">
												<cc1:mstextbox id="msOtherSurchargePercentage" runat="server" tabIndex="35" Width="100%" CssClass="textFieldRightAlign"
													AutoPostBack="True" MaxLength="9" TextMaskType="msNumeric" NumberMaxValue="100" NumberMinValue="0"
													NumberPrecision="6" NumberScale="2"></cc1:mstextbox></TD>
											<TD colSpan="2">&nbsp;</TD>
											<td colspan="1">&nbsp;</td>
											<TD colSpan="5">
												<asp:label id="lblDimByTOT" runat="server" Width="100%" CssClass="tableLabel" Height="22px">Dim by total</asp:label></TD>
											<TD colSpan="2">
												<asp:DropDownList id="ddlDimByTOT" runat="server" tabIndex="29" Width="100%" CssClass="textField"></asp:DropDownList></TD>
											<TD colSpan="2">&nbsp;</TD>
										</tr>
										<tr height="25">
											<TD align="right" colSpan="1"></TD>
											<TD colSpan="5">
												<asp:label id="lblreqOtherSurchargeMinimum" runat="server" Width="100%" CssClass="tableLabel"
													Height="22px">Other Surcharge Minimum</asp:label></TD>
											<TD colSpan="2">
												<cc1:mstextbox id="msOtherSurchargeMinimum" runat="server" tabIndex="35" Width="100%" CssClass="textFieldRightAlign"
													AutoPostBack="True" MaxLength="9" TextMaskType="msNumeric" NumberMaxValue="100000" NumberMinValue="0"
													NumberPrecision="9" NumberScale="9"></cc1:mstextbox></TD>
											<TD colSpan="2">&nbsp;</TD>
											<td colspan="1">
												<asp:requiredfieldvalidator id="Requiredfieldvalidator1" Width="100%" Runat="server" ControlToValidate="txtFirstShipDate"
													Display="None" ErrorMessage="First ship date"></asp:requiredfieldvalidator>
												<asp:label id="Label26" runat="server" Height="22px" Width="100%" CssClass="errorMsgColor">&nbsp;*&nbsp;</asp:label></td>
											<TD colSpan="5">
												<asp:label id="lblFirstShpDate" runat="server" Width="100%" CssClass="tableLabel" Height="22px">First ship date</asp:label></TD>
											<TD colSpan="2">
												<cc1:mstextbox id="txtFirstShipDate" runat="server" tabIndex="32" Width="100%" CssClass="textField"
													AutoPostBack="true" MaxLength="12" TextMaskString="99/99/9999" TextMaskType="msDate"></cc1:mstextbox></TD>
											<TD colSpan="2">&nbsp;</TD>
										</tr>
										<tr height="25">
											<TD align="right" colSpan="1"></TD>
											<TD colSpan="5">
												<asp:label id="lblreqOtherSurchargeMaximum" runat="server" Width="100%" CssClass="tableLabel"
													Height="22px">Other Surcharge Maximum</asp:label></TD>
											<TD colSpan="2">
												<cc1:mstextbox id="msOtherSurchargeMaximum" runat="server" tabIndex="35" Width="100%" CssClass="textFieldRightAlign"
													AutoPostBack="True" MaxLength="9" TextMaskType="msNumeric" NumberMaxValue="100000" NumberMinValue="0"
													NumberPrecision="9" NumberScale="9"></cc1:mstextbox></TD>
											<TD colSpan="2">&nbsp;</TD>
											<td colspan="1">&nbsp;</td>
											<TD colSpan="5">
												<asp:label id="lblCategory" runat="server" Width="100%" CssClass="tableLabel" Height="22px">Category</asp:label>
											</TD>
											<TD colSpan="2">
												<asp:TextBox id="txtCategory" Runat="server" Width="100%" Height="22px" Enabled="False"></asp:TextBox>
											</TD>
											<TD colSpan="2">&nbsp;</TD>
										</tr>
										<tr height="25">
											<TD align="right" colSpan="1"></TD>
											<TD colSpan="5">
												<asp:label id="lblreqOtherSurchargeDescription" runat="server" Width="100%" CssClass="tableLabel"
													Height="22px">Other Surcharge Description(for Invoice)</asp:label></TD>
											<TD colSpan="10">
												<cc1:mstextbox id="msOtherSurchargeDescription" runat="server" tabIndex="35" Width="100%" CssClass="textField"
													MaxLength="15"></cc1:mstextbox></TD>
											<%--<TD colSpan="2">&nbsp;</TD>
											<td colspan="1">&nbsp;</td>
											<TD colSpan="5">&nbsp;</TD>--%>
											<TD colSpan="2">&nbsp;</TD>
											<TD colSpan="2">&nbsp;</TD>
										</tr>
										<%--Other Surcharge--%>
										<tr height="25">
											<TD colSpan="1">&nbsp;</TD>
											<TD colSpan="5">
												<asp:label id="lblRemark" runat="server" Width="100%" CssClass="tableLabel" Height="22px">Remarks</asp:label></TD>
											<TD colSpan="14">
												<asp:textbox id="txtRemark" runat="server" Width="100%" tabIndex="42" CssClass="textField"></asp:textbox></TD>
										</tr>
										<%--Added By Tom 22/7/09--%>
										<tr height="25">
											<TD colSpan="1">&nbsp;</TD>
											<TD colSpan="5">
												<asp:label id="lblSpecialInstruction" runat="server" Width="100%" CssClass="tableLabel" Height="22px">Special Instruction</asp:label></TD>
											<TD colSpan="14">
												<asp:textbox id="txtSpecialInstruction" runat="server" Width="100%" tabIndex="42" CssClass="textField"></asp:textbox></TD>
										</tr>
										<%--End Added By Tom 22/7/09--%>
									</TABLE>
								</fieldset>
							</TD>
						</TR>
					</TABLE>
				</iewc:PageView>
				<iewc:PageView id="ReferencePage">
					<TABLE id="Table2" style="Z-INDEX: 112; WIDTH: 730px" width="730" border="0" runat="server">
						<TR width="100%">
							<TD width="100%">
								<fieldset>
									<legend>
										<asp:Label id="Label4" runat="server" CssClass="tableHeadingFieldset">Reference</asp:Label></legend>
									<TABLE id="tblReferences" width="100%" align="left" border="0" runat="server">
										<TR height="5">
											<TD width="2%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="8%"></TD>
										</TR>
										<tr height="25">
											<TD colSpan="1">&nbsp;</TD>
											<TD colSpan="5">
												<asp:label id="lblCustAccDispTab3" runat="server" Height="22px" Width="100%" CssClass="tableLabel">Customer Account</asp:label></TD>
											<TD colSpan="4">
												<asp:textbox id="txtCustAccDispTab3" runat="server" Width="100%" CssClass="textField" Enabled="False"></asp:textbox></TD>
											<TD align="right" colSpan="1"></TD>
											<TD colSpan="3">
												<asp:label id="lblCustNameDispTab3" runat="server" Height="22px" Width="100%" CssClass="tableLabel">Customer Name</asp:label></TD>
											<TD colSpan="6">
												<asp:textbox id="txtCustNameDispTab3" runat="server" Width="100%" CssClass="textField" Enabled="False"></asp:textbox></TD>
										</tr>
										<TR height="10">
											<TD colSpan="20"></TD>
										</TR>
										<TR height="25">
											<TD style="HEIGHT: 26px" colSpan="1">&nbsp;</TD>
											<TD colSpan="13" align="left">
												<asp:button id="btnRefViewAll" runat="server" tabIndex="50" Width="80px" CssClass="queryButton"
													CausesValidation="False" Text="View All"></asp:button>
												<asp:button id="btnRefInsert" runat="server" tabIndex="51" Width="50px" CssClass="queryButton"
													CausesValidation="False" Text="Insert"></asp:button>
												<asp:button id="btnRefSave" runat="server" tabIndex="52" Width="50px" CssClass="queryButton"
													Text="Save"></asp:button>
												<asp:button id="btnRefDelete" runat="server" tabIndex="53" Width="57px" CssClass="queryButton"
													CausesValidation="False" Text="Delete" Visible="True"></asp:button>
												<asp:button id="btnFirstRef" runat="server" tabIndex="54" Width="19px" CssClass="queryButton"
													CausesValidation="False" Text="|<"></asp:button>
												<asp:button id="btnPreviousRef" runat="server" tabIndex="55" Width="19px" CssClass="queryButton"
													CausesValidation="False" Text="<"></asp:button>
												<asp:textbox id="txtRefJumpCount" runat="server" tabIndex="56" Width="16px"></asp:textbox>
												<asp:button id="btnNextRef" runat="server" tabIndex="57" Width="19px" CssClass="queryButton"
													CausesValidation="False" Text=">"></asp:button>
												<asp:button id="btnLastRef" runat="server" tabIndex="58" Width="19px" CssClass="queryButton"
													CausesValidation="False" Text=">|"></asp:button></TD>
											<TD colSpan="6">
												<asp:label id="lblNumRefRec" runat="server" Width="100%" CssClass="RecMsg" Height="22px"></asp:label></TD>
										</TR>
										<TR height="10">
											<TD colSpan="20"></TD>
										</TR>
										<tr height="25">
											<TD align="right" colSpan="1">
												<asp:requiredfieldvalidator id="validateRefSndRecName" Width="100%" Runat="server" ControlToValidate="supposrtValidator"
													Display="None" ErrorMessage="Name "></asp:requiredfieldvalidator>
												<asp:requiredfieldvalidator id="valContactPerson" Width="100%" Runat="server" ControlToValidate="suppContactPerson"
													Display="None" ErrorMessage="Contact Person"></asp:requiredfieldvalidator>
												<asp:requiredfieldvalidator id="valAddress1" Width="100%" Runat="server" ControlToValidate="suppAddress1" Display="None"
													ErrorMessage="Address1"></asp:requiredfieldvalidator>
												<asp:requiredfieldvalidator id="valTelephone" Width="100%" Runat="server" ControlToValidate="suppTelephone"
													Display="None" ErrorMessage="Telephone"></asp:requiredfieldvalidator>
												<asp:requiredfieldvalidator id="valZipcode" Width="100%" Runat="server" ControlToValidate="suppZipcode" Display="None"
													ErrorMessage="ZipCode"></asp:requiredfieldvalidator>
												<asp:label id="Label2" runat="server" Height="22px" Width="100%" CssClass="errorMsgColor">&nbsp;*&nbsp;</asp:label></TD>
											<TD colSpan="3">
												<asp:label id="lblRefSndRecName" runat="server" Width="100%" CssClass="tableLabel" Height="22px">Name</asp:label></TD>
											<TD colSpan="6">
												<asp:textbox id="txtRefSndRecName" runat="server" tabIndex="59" Width="100%" CssClass="textField"
													AutoPostBack="False"></asp:textbox>
												<asp:textbox id="supposrtValidator" runat="server" Width="0" Visible="False">Text</asp:textbox>
												<asp:textbox id="suppContactPerson" runat="server" Width="0" Visible="False">Text</asp:textbox>
												<asp:textbox id="suppAddress1" runat="server" Width="0" Visible="False">Text</asp:textbox>
												<asp:textbox id="suppTelephone" runat="server" Width="0" Visible="False">Text</asp:textbox>
												<asp:textbox id="suppZipcode" runat="server" Width="0" Visible="False">Text</asp:textbox>
											</TD>
											<TD colSpan="1">
												<asp:label id="lblreqrefContactPerson" runat="server" Height="22px" Width="100%" CssClass="errorMsgColor">&nbsp;*&nbsp;</asp:label></TD>
											<TD colSpan="3">
												<asp:label id="lblRefContactPerson" runat="server" Width="100%" CssClass="tableLabel" Height="22px">Contact Person</asp:label></TD>
											<TD colSpan="6">
												<asp:textbox id="txtRefContactPerson" runat="server" tabIndex="60" Width="210px" CssClass="textField"
													AutoPostBack="False"></asp:textbox></TD>
										</tr>
										<tr height="25">
											<TD colSpan="1">
												<asp:label id="lblreqRefAddress1" runat="server" Height="22px" Width="100%" CssClass="errorMsgColor">&nbsp;*&nbsp;</asp:label></TD>
											<TD colSpan="3">
												<asp:label id="lblRefAddress1" runat="server" Width="100%" CssClass="tableLabel" Height="22px">Address 1</asp:label></TD>
											<TD colSpan="6">
												<asp:textbox id="txtRefAddress1" runat="server" tabIndex="61" Width="100%" CssClass="textField"></asp:textbox></TD>
											<TD colSpan="1">
												<asp:label id="lblreqReference" runat="server" Height="22px" Width="100%" CssClass="errorMsgColor">&nbsp;*&nbsp;</asp:label></TD>
											<TD colSpan="3">
												<asp:label id="lblRefType" runat="server" Width="100%" CssClass="tableLabel" Height="22px">Reference Type</asp:label></TD>
											<TD colSpan="2">
												<asp:checkbox id="chkSender" runat="server" tabIndex="66" Width="100%" Height="15px" AutoPostBack="False"
													Text="Sender" Font-Size="Smaller" CssClass="tableRadioButton"></asp:checkbox></TD>
											<TD colSpan="4">
												<asp:checkbox id="chkRecipient" runat="server" tabIndex="67" Width="100%" Height="15px" AutoPostBack="False"
													Text="Recipient" Font-Size="Smaller" CssClass="tableRadioButton"></asp:checkbox></TD>
										</tr>
										<tr height="25">
											<TD colSpan="1">&nbsp;</TD>
											<TD colSpan="3">
												<asp:label id="lblRefAddress2" runat="server" Width="100%" CssClass="tableLabel" Height="22px">Address 2</asp:label></TD>
											<TD colSpan="6">
												<asp:textbox id="txtRefAddress2" runat="server" tabIndex="62" Width="100%" CssClass="textField"></asp:textbox></TD>
											<TD colSpan="1">
												<asp:label id="lblreqRefTelephone" runat="server" Height="22px" Width="100%" CssClass="errorMsgColor">&nbsp;*&nbsp;</asp:label></TD>
											<TD colSpan="3">
												<asp:label id="lblRefTelephone" runat="server" Width="100%" CssClass="tableLabel" Height="22px">Telephone</asp:label></TD>
											<TD colSpan="3">
												<asp:textbox id="txtRefTelephone" runat="server" tabIndex="68" Width="100%" CssClass="textField"></asp:textbox></TD>
											<TD colSpan="3">&nbsp;</TD>
										</tr>
										<tr height="25">
											<TD align="right" colSpan="1">
												<asp:label id="lblreqRefZipcode" runat="server" Height="22px" Width="100%" CssClass="errorMsgColor">&nbsp;*&nbsp;</asp:label></TD>
											<TD colSpan="3">
												<asp:label id="lblRefZipcode" runat="server" Width="100%" CssClass="tableLabel" Height="22px">Postal Code</asp:label></TD>
											<TD colSpan="2">
												<cc1:mstextbox id="txtRefZipcode" runat="server" tabIndex="63" Width="100%" CssClass="textField"
													AutoPostBack="False" MaxLength="12" TextMaskType="msUpperAlfaNumericWithUnderscore"></cc1:mstextbox></TD>
											<TD colSpan="1">
												<asp:button id="btnRefZipcodeSearch" runat="server" tabIndex="64" CssClass="queryButton" CausesValidation="False"
													Text="..."></asp:button></TD>
											<TD colSpan="1">
												<asp:label id="lblRefState" runat="server" Width="100%" CssClass="tableLabel" Height="22px">State</asp:label></TD>
											<TD colSpan="3">
												<asp:textbox id="txtRefState" runat="server" Width="100%" CssClass="textField" ReadOnly="True"></asp:textbox></TD>
											<TD colSpan="3">
												<asp:label id="lblRefFax" runat="server" Width="100%" CssClass="tableLabel" Height="22px">Fax</asp:label></TD>
											<TD colSpan="3">
												<asp:textbox id="txtRefFax" runat="server" Width="100%" tabIndex="69" CssClass="textField"></asp:textbox></TD>
											<TD colSpan="3">&nbsp;</TD>
										</tr>
										<tr height="25">
											<TD align="right" colSpan="1"></TD>
											<TD colSpan="3">
												<asp:label id="lblRefCountry" runat="server" Width="100%" CssClass="tableLabel" Height="22px">Country</asp:label></TD>
											<TD colSpan="2">
												<asp:textbox id="txtRefCountry" runat="server" tabIndex="65" Width="100%" CssClass="textField"
													AutoPostBack="False" ReadOnly="True"></asp:textbox></TD>
											<TD colSpan="5">&nbsp;</TD>
											<TD colSpan="3">
												<asp:label id="lblRefEmail" runat="server" Width="120px" CssClass="tableLabel" Height="22px">Email</asp:label></TD>
											<TD colSpan="6">
												<asp:textbox id="txtRefEmail" runat="server" tabIndex="120" Width="210px" CssClass="textField"></asp:textbox></TD>
										</tr>
									</TABLE>
								</fieldset>
							</TD>
						</TR>
					</TABLE>
				</iewc:PageView>
				<iewc:PageView id="StatusPage">
					<TABLE id="Table3" style="Z-INDEX: 112; WIDTH: 730px" width="730" border="0" runat="server">
						<TR width="100%">
							<TD width="100%">
								<fieldset>
									<legend>
										<asp:Label id="Label5" runat="server" CssClass="tableHeadingFieldset">Status & Exception Surcharges</asp:Label></legend>
									<TABLE id="tblStatus" width="100%" align="left" border="0" runat="server">
										<TR height="5">
											<TD width="2%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="8%"></TD>
										</TR>
										<tr height="25">
											<TD colSpan="1">&nbsp;</TD>
											<TD colSpan="5">
												<asp:label id="lblCustAccDispTab4" runat="server" Height="22px" Width="100%" CssClass="tableLabel">Customer Account</asp:label></TD>
											<TD colSpan="4">
												<asp:textbox id="txtCustAccDispTab4" runat="server" Width="100%" CssClass="textField" Enabled="False"></asp:textbox></TD>
											<TD align="right" colSpan="1"></TD>
											<TD colSpan="3">
												<asp:label id="lblCustNameDispTab4" runat="server" Height="22px" Width="100%" CssClass="tableLabel">Customer Name</asp:label></TD>
											<TD colSpan="6">
												<asp:textbox id="txtCustNameDispTab4" runat="server" Width="100%" CssClass="textField" Enabled="False"></asp:textbox></TD>
										</tr>
										<TR height="5">
											<TD colSpan="20"></TD>
										</TR>
										<TR height="25">
											<TD style="HEIGHT: 26px" colSpan="1">&nbsp;</TD>
											<TD colSpan="12" align="left">
												<asp:button id="btnSCViewAll" runat="server" tabIndex="50" Width="80px" CssClass="queryButton"
													Height="22px" CausesValidation="False" Text="View All" OnClick="btnSCViewAll_Click"></asp:button>
												<asp:button id="btnSCInsert" runat="server" tabIndex="51" Width="62px" CssClass="queryButton"
													Height="22px" CausesValidation="False" Text="Insert" OnClick="btnSCInsert_Click"></asp:button>
											</TD>
											<TD colSpan="7">
												<asp:label id="lblSCRecCnt" runat="server" Width="100%" CssClass="RecMsg" Height="22px"></asp:label>
											</TD>
										</TR>
										<tr height="25">
											<TD colSpan="1">&nbsp;</TD>
											<TD colSpan="19">
												<asp:label id="lblSCMessage" runat="server" Height="22px" Width="100%" CssClass="errorMsgColor">Status Error.....</asp:label></TD>
										</tr>
										<TR>
											<TD colSpan="1">&nbsp;</TD>
											<TD colSpan="17">
												<asp:datagrid id="dgStatusCodes" style="Z-INDEX: 100" runat="server" Width="100%" SelectedItemStyle-CssClass="gridFieldSelected"
													OnDeleteCommand="dgStatusCodes_Delete" AllowCustomPaging="True" PageSize="3" AllowPaging="True"
													OnSelectedIndexChanged="dgStatusCodes_SelectedIndexChanged" OnUpdateCommand="dgStatusCodes_Update"
													OnCancelCommand="dgStatusCodes_Cancel" OnEditCommand="dgStatusCodes_Edit" OnItemDataBound="dgStatusCodes_Bound"
													OnItemCommand="dgStatusCodes_Button" OnPageIndexChanged="dgStatusCodes_PageChange" AutoGenerateColumns="False"
													ItemStyle-Height="20">
													<SelectedItemStyle CssClass="gridFieldSelected"></SelectedItemStyle>
													<ItemStyle Height="20px" CssClass="gridField"></ItemStyle>
													<Columns>
														<asp:ButtonColumn Text="&lt;IMG SRC='images/butt-select.gif' border=0 title='Select' &gt;" ButtonType="LinkButton"
															CommandName="Select">
															<HeaderStyle HorizontalAlign="Center" Width="3%" CssClass="gridHeading"></HeaderStyle>
															<ItemStyle HorizontalAlign="Center"></ItemStyle>
														</asp:ButtonColumn>
														<asp:EditCommandColumn ButtonType="LinkButton" UpdateText="&lt;IMG SRC='images/butt-update.gif' border=0 title='Update' &gt;"
															CancelText="&lt;IMG SRC='images/butt-cancel.gif' border=0 title='Cancel' &gt;" EditText="&lt;IMG SRC='images/butt-edit.gif' border=0 title='Edit' &gt;">
															<HeaderStyle Width="5%" CssClass="gridHeading"></HeaderStyle>
															<ItemStyle HorizontalAlign="Left"></ItemStyle>
														</asp:EditCommandColumn>
														<asp:ButtonColumn Text="&lt;IMG SRC='images/butt-delete.gif' border=0 title='Delete' &gt;" ButtonType="LinkButton"
															CommandName="Delete">
															<HeaderStyle HorizontalAlign="Center" Width="3%" CssClass="gridHeading"></HeaderStyle>
															<ItemStyle HorizontalAlign="Center"></ItemStyle>
														</asp:ButtonColumn>
														<asp:TemplateColumn HeaderText="Status Code">
															<HeaderStyle Font-Bold="True" Width="25%" CssClass="gridHeading"></HeaderStyle>
															<ItemStyle Wrap="False"></ItemStyle>
															<ItemTemplate>
																<asp:Label CssClass="gridLabel" ID="lblStatusCode" Text='<%#DataBinder.Eval(Container.DataItem,"status_code")%>' Runat="server">
																</asp:Label>
															</ItemTemplate>
															<EditItemTemplate>
																<cc1:mstextbox CssClass="gridTextBox" ID="txtStatusCode" Text='<%#DataBinder.Eval(Container.DataItem,"status_code")%>' Runat="server" Enabled="True" TextMaskType="msUpperAlfaNumericWithUnderscore" MaxLength="12">
																</cc1:mstextbox>
																<asp:RequiredFieldValidator ID="scValidator" Runat="server" BorderWidth="0" Display="None" ControlToValidate="txtStatusCode"
																	ErrorMessage="Status Code"></asp:RequiredFieldValidator>
															</EditItemTemplate>
														</asp:TemplateColumn>
														<asp:ButtonColumn Text="&lt;IMG SRC='images/butt-search.gif' border=0 title='Search' &gt;" ButtonType="LinkButton"
															CommandName="search">
															<HeaderStyle HorizontalAlign="Center" Width="3%" CssClass="gridHeading"></HeaderStyle>
															<ItemStyle HorizontalAlign="Center" CssClass="gridField"></ItemStyle>
														</asp:ButtonColumn>
														<asp:TemplateColumn HeaderText="Status Description">
															<HeaderStyle Width="35%" CssClass="gridHeading"></HeaderStyle>
															<ItemTemplate>
																<asp:Label CssClass="gridLabel" ID="lblStatusDescription" Text='<%#DataBinder.Eval(Container.DataItem,"status_description")%>' Runat="server" Enabled="True">
																</asp:Label>
															</ItemTemplate>
															<EditItemTemplate>
																<asp:TextBox CssClass="gridTextBox" ID="txtStatusDescription" Text='<%#DataBinder.Eval(Container.DataItem,"status_description")%>' Runat="server" ReadOnly="True" Enabled="True">
																</asp:TextBox>
															</EditItemTemplate>
														</asp:TemplateColumn>
														<asp:TemplateColumn HeaderText="Charge Amount">
															<HeaderStyle Width="25%" CssClass="gridHeading"></HeaderStyle>
															<ItemTemplate>
																<asp:Label CssClass="gridLabel" ID="lblSCChargeAmount" Text='<%#DataBinder.Eval(Container.DataItem,"surcharge","{0:n}")%>' Runat="server" Enabled="True" >
																</asp:Label>
															</ItemTemplate>
															<EditItemTemplate>
																<cc1:msTextBox CssClass="gridTextBox" ID="txtSCChargeAmount" Text='<%#DataBinder.Eval(Container.DataItem,"surcharge", "{0:n}")%>' Runat="server" Enabled="True" MaxLength="9" TextMaskType="msNumeric" NumberMaxValue="999999" NumberMinValue="0" NumberPrecision="8" NumberScale="2">
																</cc1:msTextBox>
															</EditItemTemplate>
														</asp:TemplateColumn>
													</Columns>
													<PagerStyle NextPageText="Next" PrevPageText="Previous" HorizontalAlign="Right" CssClass="normalText"></PagerStyle>
												</asp:datagrid>
											</TD>
											<TD colSpan="2">&nbsp;</TD>
										</TR>
										<TR height="5">
											<TD colSpan="20"></TD>
										</TR>
										<TR height="25">
											<TD style="HEIGHT: 26px" colSpan="1">&nbsp;</TD>
											<TD colSpan="12" align="left">
												<asp:button id="btnExInsert" runat="server" tabIndex="51" Width="62px" CssClass="queryButton"
													Height="22px" CausesValidation="False" Text="Insert" OnClick="btnExInsert_Click"></asp:button>
											</TD>
											<TD colSpan="5">
												<asp:label id="lblExRecCnt" runat="server" Width="100%" CssClass="RecMsg" Height="22px"></asp:label>
											</TD>
										</TR>
										<tr height="25">
											<TD colSpan="1">&nbsp;</TD>
											<TD colSpan="19">
												<asp:label id="lblEXMessage" runat="server" Height="22px" Width="100%" CssClass="errorMsgColor"></asp:label></TD>
										</tr>
										<tr height="25">
											<TD colSpan="1">&nbsp;</TD>
											<TD colSpan="17">
												<asp:datagrid id="dgExceptionCodes" style="Z-INDEX: 101" runat="server" Width="100%" OnDeleteCommand="dgExceptionCodes_Delete"
													AllowCustomPaging="True" PageSize="25" AllowPaging="True" OnUpdateCommand="dgExceptionCodes_Update"
													OnCancelCommand="dgExceptionCodes_Cancel" OnEditCommand="dgExceptionCodes_Edit" OnItemDataBound="dgExceptionCodes_Bound"
													OnItemCommand="dgExceptionCodes_Button" OnPageIndexChanged="dgExceptionCodes_PageChange" AutoGenerateColumns="False"
													ItemStyle-Height="20">
													<ItemStyle Height="20px" CssClass="gridField"></ItemStyle>
													<Columns>
														<asp:EditCommandColumn ButtonType="LinkButton" UpdateText="&lt;IMG SRC='images/butt-update.gif' border=0 title='Update' &gt;"
															CancelText="&lt;IMG SRC='images/butt-cancel.gif' border=0 title='Cancel' &gt;" EditText="&lt;IMG SRC='images/butt-edit.gif' border=0 title='Edit' &gt;">
															<HeaderStyle Width="5%" CssClass="gridHeading"></HeaderStyle>
															<ItemStyle CssClass="gridField"></ItemStyle>
														</asp:EditCommandColumn>
														<asp:ButtonColumn Text="&lt;IMG SRC='images/butt-delete.gif' border=0 title='Delete' &gt;" ButtonType="LinkButton"
															CommandName="Delete">
															<HeaderStyle HorizontalAlign="Center" Width="3%" CssClass="gridHeading"></HeaderStyle>
															<ItemStyle HorizontalAlign="Center" CssClass="gridField"></ItemStyle>
														</asp:ButtonColumn>
														<asp:TemplateColumn HeaderText="Exception Code">
															<HeaderStyle Font-Bold="True" Width="25%" CssClass="gridHeading"></HeaderStyle>
															<ItemTemplate>
																<asp:Label CssClass="gridLabel" ID="lblExceptionCode" Text='<%#DataBinder.Eval(Container.DataItem,"exception_code")%>' Runat="server">
																</asp:Label>
															</ItemTemplate>
															<EditItemTemplate>
																<cc1:mstextbox CssClass="gridTextBox" ID="txtExceptionCode" Text='<%#DataBinder.Eval(Container.DataItem,"exception_code")%>' Runat="server" Enabled="True" TextMaskType="msUpperAlfaNumericWithUnderscore" MaxLength="12">
																</cc1:mstextbox>
																<asp:RequiredFieldValidator ID="exValidator" Runat="server" BorderWidth="0" Display="None" ControlToValidate="txtExceptionCode"
																	ErrorMessage="Exception Code"></asp:RequiredFieldValidator>
															</EditItemTemplate>
														</asp:TemplateColumn>
														<asp:ButtonColumn Text="&lt;IMG SRC='images/butt-search.gif' border=0 title='Search' &gt;" ButtonType="LinkButton"
															CommandName="search">
															<HeaderStyle HorizontalAlign="Center" Width="3%" CssClass="gridHeading"></HeaderStyle>
															<ItemStyle HorizontalAlign="Center" CssClass="gridField"></ItemStyle>
														</asp:ButtonColumn>
														<asp:TemplateColumn HeaderText="Exception Description">
															<HeaderStyle Width="49%" CssClass="gridHeading"></HeaderStyle>
															<ItemTemplate>
																<asp:Label CssClass="gridLabel" ID="lblExceptionDescription" Text='<%#DataBinder.Eval(Container.DataItem,"exception_description")%>' Runat="server" Enabled="True">
																</asp:Label>
															</ItemTemplate>
															<EditItemTemplate>
																<asp:TextBox CssClass="gridTextBox" ID="txtExceptionDescription" Text='<%#DataBinder.Eval(Container.DataItem,"exception_description")%>' Runat="server" Enabled="True" ReadOnly="True">
																</asp:TextBox>
															</EditItemTemplate>
														</asp:TemplateColumn>
														<asp:TemplateColumn HeaderText="Charge Amount">
															<HeaderStyle Width="25%" CssClass="gridHeading"></HeaderStyle>
															<ItemTemplate>
																<asp:Label CssClass="gridLabel" ID="lblEXChargeAmount" Text='<%#DataBinder.Eval(Container.DataItem,"surcharge","{0:n}")%>' Runat="server" Enabled="True">
																</asp:Label>
															</ItemTemplate>
															<EditItemTemplate>
																<cc1:msTextBox CssClass="gridTextBox" ID="txtEXChargeAmount" Text='<%#DataBinder.Eval(Container.DataItem,"surcharge","{0:n}")%>' Runat="server" Enable="True" MaxLength="9" TextMaskType="msNumeric" NumberMaxValue="999999" NumberMinValue="0" NumberPrecision="8" NumberScale="2">
																</cc1:msTextBox>
															</EditItemTemplate>
														</asp:TemplateColumn>
													</Columns>
													<PagerStyle NextPageText="Next" PrevPageText="Previous" HorizontalAlign="Right" CssClass="normalText"></PagerStyle>
												</asp:datagrid>
											</TD>
											<TD colSpan="2">&nbsp;</TD>
										</tr>
									</TABLE>
								</fieldset>
							</TD>
						</TR>
					</TABLE>
				</iewc:PageView>
				<iewc:PageView id="QuatationPage">
					<TABLE id="Table5" style="Z-INDEX: 112; WIDTH: 730px" width="600" border="0" runat="server">
						<TR width="100%">
							<TD width="100%">
								<fieldset>
									<legend>
										<asp:Label id="Label6" runat="server" CssClass="tableHeadingFieldset">Quotation Status</asp:Label></legend>
									<TABLE id="tblQuatationStatus" align="left" border="0" runat="server">
										<TR height="5">
											<TD width="2%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="8%"></TD>
										</TR>
										<tr height="25">
											<TD colSpan="1">&nbsp;</TD>
											<TD colSpan="5">
												<asp:label id="lblCustAccDispTab5" runat="server" Height="22px" Width="100%" CssClass="tableLabel">Customer Account</asp:label></TD>
											<TD colSpan="4">
												<asp:textbox id="txtCustAccDispTab5" runat="server" Width="100%" CssClass="textField" Enabled="False"></asp:textbox></TD>
											<TD align="right" colSpan="1"></TD>
											<TD colSpan="3">
												<asp:label id="lblCustNameDispTab5" runat="server" Height="22px" Width="100%" CssClass="tableLabel">Customer Name</asp:label></TD>
											<TD colSpan="6">
												<asp:textbox id="txtCustNameDispTab5" runat="server" Width="100%" CssClass="textField" Enabled="False"></asp:textbox></TD>
										</tr>
										<TR height="5">
											<TD colSpan="20"></TD>
										</TR>
										<TR height="25">
											<TD style="HEIGHT: 26px" colSpan="1">&nbsp;</TD>
											<TD colSpan="13" align="left">
												<asp:button id="btnQuoteViewAll" runat="server" tabIndex="50" Width="80px" CssClass="queryButton"
													Height="22px" CausesValidation="False" Text="View All"></asp:button></TD>
											<TD colSpan="6">
												<asp:label id="lblNumQuoteRec" runat="server" Width="100%" CssClass="RecMsg" Height="22px"></asp:label></TD>
										</TR>
										<tr>
											<td colspan="20">
												<asp:datagrid id="dgQuotation" style="Z-INDEX: 104; LEFT: 18px; POSITION: absolute; TOP: 125px"
													runat="server" OnItemDataBound="OnItemBound_Quotation" AllowCustomPaging="True" PageSize="7"
													PagerStyle-PageButtonCount="7" AllowPaging="True" OnPageIndexChanged="OnQuotation_PageChange"
													AutoGenerateColumns="False" ItemStyle-Height="20" Width="580px">
													<Columns>
														<asp:TemplateColumn HeaderText="Quotation No.">
															<HeaderStyle Width="40%" CssClass="gridHeading"></HeaderStyle>
															<ItemStyle CssClass="gridField"></ItemStyle>
															<ItemTemplate>
																<asp:Label CssClass="gridLabel" ID="lblQuoteNo" Visible="True" Text='<%#DataBinder.Eval(Container.DataItem,"Quotation_No")%>' Runat="server">
																</asp:Label>
															</ItemTemplate>
														</asp:TemplateColumn>
														<asp:TemplateColumn HeaderText="Version">
															<HeaderStyle Width="10%" CssClass="gridHeading"></HeaderStyle>
															<ItemStyle CssClass="gridField"></ItemStyle>
															<ItemTemplate>
																<asp:Label CssClass="gridLabel" ID="lblQuoteVersion" Visible="True" Text='<%#DataBinder.Eval(Container.DataItem,"Quotation_version")%>' Runat="server">
																</asp:Label>
															</ItemTemplate>
														</asp:TemplateColumn>
														<asp:TemplateColumn HeaderText="Date">
															<HeaderStyle Width="15%" CssClass="gridHeading"></HeaderStyle>
															<ItemStyle CssClass="gridField"></ItemStyle>
															<ItemTemplate>
																<asp:Label CssClass="gridLabel" ID="lblQuoteDate" Visible="True" Text='<%#DataBinder.Eval(Container.DataItem,"Quotation_Date","{0:dd/MM/yyyy}")%>' Runat="server">
																</asp:Label>
															</ItemTemplate>
														</asp:TemplateColumn>
														<asp:TemplateColumn HeaderText="Status">
															<HeaderStyle Width="25%" CssClass="gridHeading"></HeaderStyle>
															<ItemStyle CssClass="gridField"></ItemStyle>
															<ItemTemplate>
																<asp:Label CssClass="gridLabel" ID="lblQuoteStatus" Visible="True" Text='<%#DataBinder.Eval(Container.DataItem,"Quotation_Status")%>' Runat="server">
																</asp:Label>
															</ItemTemplate>
														</asp:TemplateColumn>
													</Columns>
													<PagerStyle NextPageText="Next" PrevPageText="Previous" HorizontalAlign="Right" CssClass="normalText"></PagerStyle>
												</asp:datagrid>
											</td>
										</tr>
									</TABLE>
								</fieldset>
							</TD>
						</TR>
					</TABLE>
				</iewc:PageView>
				<iewc:PageView id="Zones">
					<TABLE id="Table4" style="Z-INDEX: 112; WIDTH: 730px; POSITION: absolute; TOP: 10px" width="600"
						border="0" runat="server">
						<TR width="100%">
							<TD width="100%">
								<fieldset><legend>
										<asp:label id="Label8" runat="server" CssClass="tableHeadingFieldset">Zones</asp:label></legend>
									<TABLE id="Table6" align="left" border="0" runat="server">
										<TR height="5">
											<TD width="2%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="8%"></TD>
										</TR>
										<tr height="25">
											<TD colSpan="1">&nbsp;</TD>
											<TD colSpan="5">
												<asp:label id="Label9" runat="server" Height="22px" Width="100%" CssClass="tableLabel">Customer Account</asp:label></TD>
											<TD colSpan="4">
												<asp:textbox id="txtCustAccDispTab6" runat="server" Width="100%" CssClass="textField" Enabled="False"></asp:textbox></TD>
											<TD align="right" colSpan="1"></TD>
											<TD colSpan="3">
												<asp:label id="Label10" runat="server" Height="22px" Width="100%" CssClass="tableLabel">Customer Name</asp:label></TD>
											<TD colSpan="6">
												<asp:textbox id="txtCustNameDispTab6" runat="server" Width="100%" CssClass="textField" Enabled="False"></asp:textbox></TD>
										</tr>
										<tr>
											<td colSpan="20">&nbsp;</td>
										</tr>
										<tr>
											<td>&nbsp;</td>
											<td colSpan="17">
												<asp:button id="btnQueryFR" runat="server" Width="70px" CssClass="queryButton" Text="Query"
													CausesValidation="False"></asp:button>
												<asp:button id="btnExecuteQueryFR" runat="server" Width="120px" CssClass="queryButton" Text="Execute Query"
													CausesValidation="False"></asp:button>
												<asp:button id="btnInsertFR" runat="server" Width="70px" CssClass="queryButton" Text="Insert"
													CausesValidation="False"></asp:button>&nbsp;
												<asp:button id="btnImport_Show" runat="server" Width="62px" CssClass="queryButton" Text="Import"></asp:button></td>
										</tr>
										<TR>
											<TD></TD>
											<TD colSpan="19"><FONT face="Tahoma">
													<asp:button id="btnImport_Cancel" runat="server" Width="62px" CssClass="queryButton" Text="Cancel"></asp:button>
													<div onmouseup="upBrowse();" onmousedown="downBrowse();" id="divBrowse" style="DISPLAY: inline; BACKGROUND-IMAGE: url(images/btn-browse-up.GIF); WIDTH: 70px; HEIGHT: 21px">
														<INPUT id="inFile" onblur="setValue();" style="BORDER-RIGHT: #88a0c8 1px outset; BORDER-TOP: #88a0c8 1px outset; FONT-SIZE: 11px; FILTER: alpha(opacity: 0); BORDER-LEFT: #88a0c8 1px outset; WIDTH: 0px; COLOR: #003068; BORDER-BOTTOM: #88a0c8 1px outset; FONT-FAMILY: Arial; BACKGROUND-COLOR: #e9edf0; TEXT-DECORATION: none"
															onfocus="setValue();" type="file" name="inFile" runat="server"></div>
													<asp:button id="btnImport" runat="server" Width="62px" CssClass="queryButton" Text="Import"
														Enabled="False"></asp:button>
													<asp:button id="btnImport_Save" runat="server" Width="66px" CssClass="queryButton" Text="Save"
														Enabled="False"></asp:button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
													<asp:textbox id="txtFilePath" runat="server" Width="240px" CssClass="textField"></asp:textbox></FONT></TD>
										</TR>
										<tr>
											<td colSpan="20">&nbsp;</td>
										</tr>
										<tr>
											<td>&nbsp;</td>
											<td colSpan="19">
												<asp:datagrid id="dgCustZones" runat="server" Width="528px" Visible="True" PageSize="50" OnPageIndexChanged="dgCustZones_PageChange"
													OnEditCommand="dgCustZones_Edit" OnCancelCommand="dgCustZones_Cancel" OnDeleteCommand="dgCustZones_Delete"
													OnUpdateCommand="dgCustZones_Update" OnItemCommand="dgCustZones_Button" AllowPaging="True"
													AutoGenerateColumns="False" HorizontalAlign="left">
													<Columns>
														<asp:TemplateColumn>
															<HeaderStyle CssClass="gridHeading"></HeaderStyle>
															<ItemStyle Wrap="False" CssClass="gridField"></ItemStyle>
															<ItemTemplate>
																&nbsp;&nbsp;
																<asp:ImageButton CommandName="Select" id="imgSelect" runat="server" ImageUrl="images/butt-select.gif"
																	Enabled="False"></asp:ImageButton>&nbsp;&nbsp;
															</ItemTemplate>
														</asp:TemplateColumn>
														<asp:EditCommandColumn ButtonType="LinkButton" UpdateText="&lt;IMG SRC='images/butt-update.gif' border=0 title='Update' &gt;"
															CancelText="&lt;IMG SRC='images/butt-cancel.gif' border=0 title='Cancel' &gt;" EditText="&lt;IMG SRC='images/butt-edit.gif' border=0 title='Edit' &gt;">
															<HeaderStyle Width="4%" CssClass="gridHeading"></HeaderStyle>
															<ItemStyle Wrap="False" CssClass="gridField"></ItemStyle>
														</asp:EditCommandColumn>
														<asp:ButtonColumn Text="&lt;IMG SRC='images/butt-delete.gif' border=0 title='Delete' &gt;" CommandName="Delete">
															<HeaderStyle Width="2%" CssClass="gridHeading"></HeaderStyle>
															<ItemStyle CssClass="gridField"></ItemStyle>
														</asp:ButtonColumn>
														<asp:TemplateColumn HeaderText="Postal Code">
															<HeaderStyle Font-Bold="True" Width="30%" CssClass="gridHeading"></HeaderStyle>
															<ItemStyle Wrap="False" CssClass="gridField"></ItemStyle>
															<ItemTemplate>
																<asp:Label id=lblzipcode Text='<%#DataBinder.Eval(Container.DataItem,"zipcode")%>' CssClass="gridLabel" Visible="True" Runat="server">
																</asp:Label>
															</ItemTemplate>
															<EditItemTemplate>
																<asp:TextBox id="Textbox1" Text='<%#DataBinder.Eval(Container.DataItem,"zipcode")%>' CssClass="gridTextBox" Visible="True" Runat="server" MaxLength="12">
																</asp:TextBox>
															</EditItemTemplate>
														</asp:TemplateColumn>
														<asp:ButtonColumn Text="&lt;IMG SRC='images/butt-search.gif' border=0 title='Search' &gt;" ButtonType="LinkButton"
															CommandName="zipcodeSearch">
															<HeaderStyle Width="5%" CssClass="gridHeading"></HeaderStyle>
															<ItemStyle Wrap="False" CssClass="gridField"></ItemStyle>
														</asp:ButtonColumn>
														<asp:TemplateColumn HeaderText="Zone">
															<HeaderStyle Font-Bold="True" Width="30%" CssClass="gridHeading"></HeaderStyle>
															<ItemStyle Wrap="False" CssClass="gridField"></ItemStyle>
															<ItemTemplate>
																<asp:Label id="lblzone" Text='<%#DataBinder.Eval(Container.DataItem,"zone_code")%>' CssClass="gridLabel" Runat="server" Visible="True">
																</asp:Label>
															</ItemTemplate>
															<EditItemTemplate>
																<asp:TextBox id=txtzone Text='<%#DataBinder.Eval(Container.DataItem,"zone_code")%>' CssClass="gridTextBox" Runat="server" Visible="True" MaxLength="200">
																</asp:TextBox>
															</EditItemTemplate>
														</asp:TemplateColumn>
														<asp:ButtonColumn Text="&lt;IMG SRC='images/butt-search.gif' border=0 title='Search' &gt;" ButtonType="LinkButton"
															CommandName="zoneSearch">
															<HeaderStyle Width="5%" CssClass="gridHeading"></HeaderStyle>
															<ItemStyle Wrap="False" CssClass="gridField"></ItemStyle>
														</asp:ButtonColumn>
														<asp:TemplateColumn HeaderText="Effective Date">
															<HeaderStyle Font-Bold="True" Width="30%" CssClass="gridHeading"></HeaderStyle>
															<ItemStyle Wrap="False" CssClass="gridField"></ItemStyle>
															<ItemTemplate>
																<asp:Label ID="lblEffectiveDate" CssClass="gridLabel" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"effective_date","{0:dd/MM/yyyy}")%>'>
																</asp:Label>
															</ItemTemplate>
															<EditItemTemplate>
																<cc1:mstextbox ID="txtEffectiveDate" MaxLength="10" TextMaskString="99/99/9999" TextMaskType="msDate" Text='<%#DataBinder.Eval(Container.DataItem,"effective_date","{0:dd/MM/yyyy}")%>' Runat="server">
																</cc1:mstextbox>
															</EditItemTemplate>
														</asp:TemplateColumn>
													</Columns>
													<PagerStyle NextPageText="Next" PrevPageText="Previous" HorizontalAlign="Right" CssClass="normalText"></PagerStyle>
												</asp:datagrid></td>
										</tr>
									</TABLE>
								</fieldset>
							</TD>
						</TR>
					</TABLE>
				</iewc:PageView>
				<iewc:PageView id="VAS">
					<TABLE id="Table7" style="Z-INDEX: 112; WIDTH: 730px" width="600" border="0" runat="server">
						<TR width="100%">
							<TD width="100%">
								<fieldset>
									<legend>
										<asp:Label id="Label12" runat="server" CssClass="tableHeadingFieldset">Value Added Services and Surcharges</asp:Label></legend>
									<TABLE id="Table8" align="left" border="0" runat="server">
										<TR height="5">
											<TD width="2%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="8%"></TD>
										</TR>
										<tr height="25">
											<TD colSpan="1">&nbsp;</TD>
											<TD colSpan="5">
												<asp:label id="Label18" runat="server" Height="22px" Width="100%" CssClass="tableLabel">Customer Account</asp:label></TD>
											<TD colSpan="4">
												<asp:textbox id="txtCustAccDispTab7" runat="server" Width="100%" CssClass="textField" Enabled="False"></asp:textbox></TD>
											<TD align="right" colSpan="1"></TD>
											<TD colSpan="3">
												<asp:label id="Label19" runat="server" Height="22px" Width="100%" CssClass="tableLabel">Customer Name</asp:label></TD>
											<TD colSpan="6">
												<asp:textbox id="txtCustNameDispTab7" runat="server" Width="100%" CssClass="textField" Enabled="False"></asp:textbox></TD>
										</tr>
										<tr>
											<td colspan="20">&nbsp;</td>
										</tr>
										<tr>
											<td>&nbsp;</td>
											<td colSpan="17">
												<asp:button id="btnViewAllVAS" runat="server" CausesValidation="False" CssClass="queryButton"
													Text="View All" Width="70px"></asp:button>
												<asp:button id="btnInsertVAS" runat="server" CausesValidation="False" CssClass="queryButton"
													Text="Insert" Width="70px"></asp:button>
											</td>
										</tr>
										<tr>
											<td colspan="20">&nbsp;</td>
										</tr>
										<tr>
											<td>&nbsp;</td>
											<td colSpan="19">
												<asp:datagrid id="dgVAS" runat="server" Width="100%" HorizontalAlign="left" SelectedItemStyle-CssClass="gridFieldSelected"
													ItemStyle-Height="20" AutoGenerateColumns="False" PageSize="5" AllowPaging="True" AllowCustomPaging="True"
													OnDeleteCommand="dgVAS_Delete" OnSelectedIndexChanged="dgVAS_SelectedIndexChanged" OnUpdateCommand="dgVAS_Update"
													OnCancelCommand="dgVAS_Cancel" OnItemDataBound="dgVAS_Bound" OnPageIndexChanged="dgVAS_PageChange"
													OnEditCommand="dgVAS_Edit">
													<SelectedItemStyle CssClass="gridFieldSelected"></SelectedItemStyle>
													<ItemStyle Height="20px" CssClass="gridField"></ItemStyle>
													<Columns>
														<asp:EditCommandColumn ButtonType="LinkButton" UpdateText="&lt;IMG SRC='images/butt-update.gif' border=0 title='Update' &gt;"
															CancelText="&lt;IMG SRC='images/butt-cancel.gif' border=0 title='Cancel' &gt;" EditText="&lt;IMG SRC='images/butt-edit.gif' border=0 title='Edit' &gt;">
															<HeaderStyle Width="5%" CssClass="gridHeading"></HeaderStyle>
															<ItemStyle HorizontalAlign="Left"></ItemStyle>
														</asp:EditCommandColumn>
														<asp:ButtonColumn Text="&lt;IMG SRC='images/butt-delete.gif' border=0 title='Delete' &gt;" CommandName="Delete">
															<HeaderStyle HorizontalAlign="Center" Width="5%" CssClass="gridHeading"></HeaderStyle>
															<ItemStyle HorizontalAlign="Center"></ItemStyle>
														</asp:ButtonColumn>
														<asp:TemplateColumn HeaderText="VAS Code">
															<HeaderStyle Font-Bold="True" Width="15%" CssClass="gridHeading"></HeaderStyle>
															<ItemStyle Wrap="False"></ItemStyle>
															<ItemTemplate>
																<asp:Label CssClass="gridLabel" ID="lblVASCode" Text='<%#DataBinder.Eval(Container.DataItem,"vas_code")%>' Runat="server">
																</asp:Label>
															</ItemTemplate>
															<EditItemTemplate>
																<asp:DropDownList Width="86px" CssClass="gridDropDown" ID="ddlVASCode" Runat="server" AutoPostBack="True"
																	OnSelectedIndexChanged="ddlVASCode_SelectedIndexChanged"></asp:DropDownList>
															</EditItemTemplate>
														</asp:TemplateColumn>
														<asp:TemplateColumn HeaderText="VAS Description">
															<HeaderStyle Wrap="False" Width="35%" CssClass="gridHeading"></HeaderStyle>
															<ItemTemplate>
																<asp:Label CssClass="gridLabel" ID="lblVASDesc" Text='<%#DataBinder.Eval(Container.DataItem,"description")%>' Runat="server">
																</asp:Label>
															</ItemTemplate>
															<EditItemTemplate>
																<asp:TextBox CssClass="gridTextBox" ID="txtVASDesc" Text='<%#DataBinder.Eval(Container.DataItem,"description")%>' Runat="server" MaxLength="200">
																</asp:TextBox>
															</EditItemTemplate>
														</asp:TemplateColumn>
														<asp:TemplateColumn HeaderText="Surcharge">
															<HeaderStyle Width="10%" CssClass="gridHeading"></HeaderStyle>
															<ItemStyle Wrap="False"></ItemStyle>
															<ItemTemplate>
																<asp:Label CssClass="gridLabel" ID="lblSurcharge" Text='<%#DataBinder.Eval(Container.DataItem,"surcharge",(String)ViewState["m_format"])%>' Runat="server">
																</asp:Label>
															</ItemTemplate>
															<EditItemTemplate>
																<cc1:mstextbox CssClass="gridTextBoxNumber" ID="txtSurcharge" Text='<%#DataBinder.Eval(Container.DataItem,"surcharge",(String)ViewState["m_format"])%>' Runat="server" Enabled="True" MaxLength="9" TextMaskType="msNumeric" NumberMaxValue="10000" NumberMinValue="0" NumberPrecision="8" NumberScale="2">
																</cc1:mstextbox>
															</EditItemTemplate>
														</asp:TemplateColumn>
														<asp:TemplateColumn HeaderText="Percent">
															<HeaderStyle Width="10%" CssClass="gridHeading"></HeaderStyle>
															<ItemStyle Wrap="False"></ItemStyle>
															<ItemTemplate>
																<asp:Label CssClass="gridLabel" ID="lblPercent" Text='<%#DataBinder.Eval(Container.DataItem,"percent","{0:n}")%>' Runat="server">
																</asp:Label>
															</ItemTemplate>
															<EditItemTemplate>
																<cc1:mstextbox CssClass="gridTextBoxNumber" ID="txtPercent" Text='<%#DataBinder.Eval(Container.DataItem,"percent","{0:n}")%>' Runat="server" Enabled="True" MaxLength="9" TextMaskType="msNumericCustVAS" NumberMaxValue="300" NumberMinValueCustVAS="0.01" NumberPrecision="8" NumberScale="2">
																</cc1:mstextbox>
															</EditItemTemplate>
														</asp:TemplateColumn>
														<asp:TemplateColumn HeaderStyle-Wrap="False" HeaderText="Min Surch">
															<HeaderStyle Width="10%" CssClass="gridHeading"></HeaderStyle>
															<ItemStyle Wrap="False"></ItemStyle>
															<ItemTemplate>
																<asp:Label CssClass="gridLabel" ID="lblMinSurch" Text='<%#DataBinder.Eval(Container.DataItem,"min_surcharge",(String)ViewState["m_format"])%>' Runat="server">
																</asp:Label>
															</ItemTemplate>
															<EditItemTemplate>
																<cc1:mstextbox CssClass="gridTextBoxNumber" ID="txtMinSurch" Text='<%#DataBinder.Eval(Container.DataItem,"min_surcharge",(String)ViewState["m_format"])%>' Runat="server" Enabled="True" MaxLength="9" TextMaskType="msNumeric" NumberMaxValue="10000" NumberMinValue="0" NumberPrecision="8" NumberScale="2">
																</cc1:mstextbox>
															</EditItemTemplate>
														</asp:TemplateColumn>
														<asp:TemplateColumn HeaderStyle-Wrap="False" HeaderText="Max Surch">
															<HeaderStyle Width="10%" CssClass="gridHeading"></HeaderStyle>
															<ItemStyle Wrap="False"></ItemStyle>
															<ItemTemplate>
																<asp:Label CssClass="gridLabel" ID="lblMaxSurch" Text='<%#DataBinder.Eval(Container.DataItem,"max_surcharge",(String)ViewState["m_format"])%>' Runat="server">
																</asp:Label>
															</ItemTemplate>
															<EditItemTemplate>
																<cc1:mstextbox CssClass="gridTextBoxNumber" ID="txtMaxSurch" Text='<%#DataBinder.Eval(Container.DataItem,"max_surcharge",(String)ViewState["m_format"])%>' Runat="server" Enabled="True" MaxLength="9" TextMaskType="msNumeric" NumberMaxValue="10000" NumberMinValue="0" NumberPrecision="8" NumberScale="2">
																</cc1:mstextbox>
															</EditItemTemplate>
														</asp:TemplateColumn>
													</Columns>
													<PagerStyle NextPageText="Next" PrevPageText="Previous" HorizontalAlign="Right" CssClass="normalText"></PagerStyle>
												</asp:datagrid>
											</td>
										</tr>
									</TABLE>
								</fieldset>
							</TD>
						</TR>
					</TABLE>
				</iewc:PageView>
			</iewc:multipage><asp:validationsummary id="custValidationSummary" style="Z-INDEX: 113; LEFT: 303px; POSITION: absolute; TOP: 12px"
				Height="36px" Width="346px" ShowSummary="False" DisplayMode="BulletList" ShowMessageBox="True" HeaderText="Please enter the missing fields."
				Runat="server"></asp:validationsummary><asp:label id="lblCPErrorMessage" style="Z-INDEX: 114; LEFT: 26px; POSITION: absolute; TOP: 104px"
				runat="server" Height="19px" Width="585px" CssClass="errorMsgColor"></asp:label>
			<!--/FORM--></FORM>
	</BODY>
</HTML>
