using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Configuration;
using com.ties.DAL;
using com.common.classes;
using com.common.util;
using com.ties.classes;
using com.ties.BAL;
using com.common.applicationpages;
using System.Text;
using Cambro.Web.DbCombo;
using System.Xml;

namespace com.ties
{
	/// <summary>
	/// Summary description for ManualRatingOverride.
	/// </summary>
	public class ManualRatingOverride : BasePage
	{
		#region Controls
		protected System.Web.UI.WebControls.Label lblMainTitle;
		protected System.Web.UI.WebControls.Button btnQry;
		protected System.Web.UI.WebControls.Button btnExecQry;
		protected System.Web.UI.WebControls.Button btnSave;
		protected System.Web.UI.WebControls.Button btnGoToFirstPage;
		protected System.Web.UI.WebControls.Button btnPreviousPage;
		protected com.common.util.msTextBox txtGoToRec;
		protected System.Web.UI.WebControls.Button btnNextPage;
		protected System.Web.UI.WebControls.Button btnGoToLastPage;
		protected System.Web.UI.WebControls.Label lblErrorMsg;
		protected System.Web.UI.WebControls.Label lblNumRec;
		protected System.Web.UI.WebControls.ValidationSummary PageValidationSummary;
		protected System.Web.UI.WebControls.Label lblConsgmtNo;
		protected System.Web.UI.WebControls.Label lblRefNo;
		protected System.Web.UI.WebControls.TextBox txtRefNo;
		protected System.Web.UI.WebControls.Label lblRouteCode;
		protected System.Web.UI.WebControls.Label lblDelManifest;
		protected Cambro.Web.DbCombo.DbCombo DbComboPathCode;
		protected System.Web.UI.WebControls.Label lblCustID;
		protected System.Web.UI.WebControls.Label lblStatusCode;
		protected com.common.util.msTextBox txtAct_pod_From;
		protected System.Web.UI.WebControls.Label lblExcepCode;
		protected com.common.util.msTextBox txtAct_Pod_to;
		protected System.Web.UI.WebControls.Label Label59;
		protected System.Web.UI.WebControls.Label Label15;
		protected System.Web.UI.WebControls.Label Label53;
		protected System.Web.UI.HtmlControls.HtmlGenericControl divMain;
		protected System.Web.UI.HtmlControls.HtmlTable MainTable;
		protected System.Web.UI.HtmlControls.HtmlTable TblBookDtl;
		protected System.Web.UI.WebControls.Label lblFreightCharge;
		protected System.Web.UI.WebControls.Label lblInsrSurc;
		protected System.Web.UI.WebControls.Label lblOtherSurc;
		protected System.Web.UI.WebControls.Label lblTotVASSurc;
		protected System.Web.UI.WebControls.Label lblTotVASSurcOverride;
		protected System.Web.UI.WebControls.Label lblESASurc;
		protected System.Web.UI.WebControls.Label lblTotAmt;
		protected System.Web.UI.WebControls.Label lblLastUpdateDT;
		protected System.Web.UI.WebControls.Label FreightChargeLabel;
		protected System.Web.UI.WebControls.Label InsruranceSurcLabel;
		protected System.Web.UI.WebControls.Label OtherSurchargeLabel;
		protected System.Web.UI.WebControls.Label TotalVASSurcLabel;
		protected System.Web.UI.WebControls.Label ESASurcLabel;
		protected System.Web.UI.WebControls.Label TotalAmountLabel;
		protected System.Web.UI.WebControls.Label LastUserUpdateLabel;
		protected System.Web.UI.WebControls.Label lblLastUserUpd;
		protected com.common.util.msTextBox txtFreightCharge;
		protected com.common.util.msTextBox txtOtherSurc;
		protected com.common.util.msTextBox txtESASurc;
		protected com.common.util.msTextBox txtInsrSurc;
		protected System.Web.UI.WebControls.Label lblTotAmtOverride;
		protected System.Web.UI.WebControls.Button btnClearManual;
		protected System.Web.UI.HtmlControls.HtmlTable Table7;
		protected System.Web.UI.WebControls.DropDownList ddbCustomer;
		protected System.Web.UI.WebControls.TextBox txtConsigNo;
		protected Cambro.Web.DbCombo.DbCombo DbComboDestinationDC;
		protected System.Web.UI.WebControls.TextBox txtCustomer;
		protected System.Web.UI.WebControls.Label lblActualPodDate;
		#endregion		
				
		#region Properties & Variables
		static private int m_iSetSize = 20;	
		private SessionDS m_sdsShipment
		{
			get{return (SessionDS)ViewState["ViewStateData"];}
			set{ViewState["ViewStateData"]=value;}
		}

		private SessionDS m_sdsSearch
		{
			get{return (SessionDS)ViewState["ViewStateDatam_sdsSearch"];}
			set{ViewState["ViewStateDatam_sdsSearch"]=value;}
		}
		private int currentPage
		{
			get{return (int)ViewState["currentPage"];}
			set{ViewState.Add("currentPage",value);}
		}
		private int currentSet
		{
			get{return (int)ViewState["currentSet"];}
			set{ViewState.Add("currentSet", value);}
		}
		private string m_format
		{
			get
			{
				if(ViewState["m_format"]==null)
					ViewState["m_format"]="0.00";
				return ViewState["m_format"].ToString();
			}
			set{ViewState.Add("m_format", value);}
		}
		private int wt_rounding_method
		{
			get
			{
				if(ViewState["wt_rounding_method"]==null)
					ViewState["wt_rounding_method"]=0;
				return (int)ViewState["wt_rounding_method"];
			}
			set{ViewState.Add("wt_rounding_method", value);}
		}
		private decimal wt_increment_amt
		{
			get
			{
				if(ViewState["wt_increment_amt"]==null)
					ViewState["wt_increment_amt"]=1;
				return (decimal)ViewState["wt_increment_amt"];
			}
			set{ViewState.Add("wt_increment_amt", value);}
		}
		#endregion

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		private void InitializeComponent()
		{
			this.btnQry.Click += new System.EventHandler(this.btnQry_Click);
			this.btnExecQry.Click += new System.EventHandler(this.btnExecQry_Click);
			this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
			this.btnClearManual.Click += new System.EventHandler(this.btnClearManual_Click);
			this.btnGoToFirstPage.Click += new System.EventHandler(this.btnGoToFirstPage_Click);
			this.btnPreviousPage.Click += new System.EventHandler(this.btnPreviousPage_Click);
			this.txtGoToRec.TextChanged += new System.EventHandler(this.txtGoToRec_TextChanged);
			this.btnNextPage.Click += new System.EventHandler(this.btnNextPage_Click);
			this.btnGoToLastPage.Click += new System.EventHandler(this.btnGoToLastPage_Click);
			this.ddbCustomer.SelectedIndexChanged += new System.EventHandler(this.ddbCustomer_SelectedIndexChanged);
			this.txtFreightCharge.TextChanged += new System.EventHandler(this.Charge_TextChanged);
			this.txtInsrSurc.TextChanged += new System.EventHandler(this.Charge_TextChanged);
			this.txtOtherSurc.TextChanged += new System.EventHandler(this.Charge_TextChanged);
			this.txtESASurc.TextChanged += new System.EventHandler(this.Charge_TextChanged);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private void Page_Load(object sender, EventArgs e)
		{
			DbComboDestinationDC.RegistrationKey = System.Configuration.ConfigurationSettings.AppSettings["DbComboRegKey"];
			DbComboPathCode.RegistrationKey = System.Configuration.ConfigurationSettings.AppSettings["DbComboRegKey"];
			this.btnClearManual.Attributes.Add("onclick","javascript:document.getElementById('"+ this.btnSave.ClientID + "').disabled=true;" + this.GetPostBackEventReference(this.btnClearManual));
			if (!IsPostBack)
			{
				utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings,Page.Session);
				GetEnterpriseProfile();
				SetDbComboServerStates();
				ResetScreenForQuery();				
			}
		}

		
		private void GetEnterpriseProfile()
		{
			DataSet profileDS = SysDataManager1.GetEnterpriseProfile(utility.GetAppID(), utility.GetEnterpriseID());
			if(profileDS.Tables[0].Rows.Count > 0)
			{
				DataRow row = profileDS.Tables[0].Rows[0];
				if ( row["currency_decimal"] !=DBNull.Value ) m_format = "{0:F" + row["currency_decimal"] + "}";
				if ( row["wt_rounding_method"] != DBNull.Value) wt_rounding_method = Convert.ToInt32(row["wt_rounding_method"]);
				if ( row["wt_increment_amt"] != DBNull.Value) wt_increment_amt = Convert.ToDecimal(row["wt_increment_amt"]);
			}
		}

		private decimal Rounding(decimal beforeRound)
		{
			return TIESUtility.EnterpriseRounding(beforeRound,wt_rounding_method,wt_increment_amt);
		}
		private void FillForSearch()
		{
			//DataRow drCurrent = m_sdsShipment.ds.Tables[0].Rows[currentPage];
			m_sdsSearch.ds.Tables[0].Clear();
			DataRow drCurrent = m_sdsSearch.ds.Tables[0].NewRow();
			m_sdsSearch.ds.Tables[0].Rows.Add(drCurrent);

			if(txtConsigNo.Text != "") drCurrent["consignment_no"] = txtConsigNo.Text.Trim();
			else drCurrent["consignment_no"] = DBNull.Value;
			
			if(txtRefNo.Text != "") drCurrent["ref_no"] = txtRefNo.Text.Trim();
			else drCurrent["ref_no"] = DBNull.Value;
		
			if(ddbCustomer.SelectedIndex!=0) drCurrent["payerid"] = ddbCustomer.SelectedItem.Value;
			else drCurrent["payerid"] = DBNull.Value;
			
			if(DbComboPathCode.Text != "") drCurrent["route_code"] = DbComboPathCode.Value;
			else drCurrent["route_code"] = DBNull.Value;

			if(DbComboDestinationDC.Text != "") drCurrent["destination_station"] = DbComboDestinationDC.Value;
			else drCurrent["destination_station"] = DBNull.Value;

			if(txtAct_pod_From.Text != "") drCurrent["actual_pod_datefrom"] = DateTime.ParseExact(txtAct_pod_From.Text,"dd/MM/yyyy",null);
			else drCurrent["actual_pod_datefrom"] = DBNull.Value;

			if(txtAct_Pod_to.Text != "") drCurrent["actual_pod_dateto"] = DateTime.ParseExact(txtAct_Pod_to.Text + " " + "23:59:59","dd/MM/yyyy HH:mm:ss",null);
			else drCurrent["actual_pod_dateto"] = DBNull.Value;
		}
		private void GetRecSet()
		{
			String tmpStr = currentSet.ToString().Trim();
			int iStartIndex = 0;
			int intIndexOf = tmpStr.IndexOf(".");	
			iStartIndex = (intIndexOf > 0)?Convert.ToInt32(tmpStr.Substring(0, intIndexOf)) * m_iSetSize : Convert.ToInt32(tmpStr) * m_iSetSize;
			m_sdsShipment = TIESDAL.ManualRatingOverrideDAL.Get(utility, m_sdsSearch, iStartIndex, m_iSetSize, false);
			
			decimal pgCnt = (m_sdsShipment.QueryResultMaxSize - 1)/m_iSetSize;
			if(pgCnt < currentSet) currentSet = Convert.ToInt32(pgCnt);
		}

		private void SetDbComboServerStates()
		{
			String strDeliveryType=null;
			strDeliveryType="S";

			Hashtable hash = new Hashtable();
			hash.Add("strDeliveryType", strDeliveryType);
			DbComboPathCode.ServerState = hash;
			DbComboPathCode.ServerStateSecretString = "kdfsjgnskajh[apq2-4uhg465654435879mnbfdm cxs;q4";

			//Bind Combo Customer
			DataSet dataset = DbComboDAL.CustomerAccountQuery(utility.GetAppID(),utility.GetEnterpriseID());	
			ddbCustomer.DataSource = dataset;
			ddbCustomer.DataBind();			
			ddbCustomer.Items.Insert(0, new ListItem("Customer Account",""));
		}

		private void ResetScreenForQuery()
		{
			m_sdsShipment = TIESDAL.ManualRatingOverrideDAL.Get(utility, null,0, m_iSetSize, true);
			m_sdsShipment.ds.Clear();

			m_sdsSearch = new SessionDS();
			m_sdsSearch.ds = m_sdsShipment.ds.Clone();

			currentPage = 0;
			
			lblNumRec.Text = "";
			lblErrorMsg.Text = "";
			btnExecQry.Enabled = true;
			btnSave.Enabled=false;
			btnClearManual.Enabled=false;
			btnGoToFirstPage.Enabled = false;
			btnGoToLastPage.Enabled = false;
			btnPreviousPage.Enabled = false;
			btnNextPage.Enabled = false;
			txtGoToRec.Text="";

			txtConsigNo.Text="";
			ddbCustomer.ClearSelection(); txtCustomer.Text="";
			txtRefNo.Text="";			
			DbComboDestinationDC.Reset();
			DbComboPathCode.Reset();			
			txtAct_pod_From.Text="";
			txtAct_Pod_to.Text="";
			lblActualPodDate.Text = "";

			//Enable=true field
			txtConsigNo.Enabled=true;
			ddbCustomer.Enabled=true; 
			txtCustomer.Enabled=true;
			txtRefNo.Enabled = true;
			DbComboDestinationDC.Enabled = true;
			DbComboPathCode.Enabled = true;
			txtAct_pod_From.Enabled = true;
			txtAct_Pod_to.Enabled = true;

			//Surcharge
			//Rated
			lblFreightCharge.Text = "";
			lblInsrSurc.Text = "";
			lblOtherSurc.Text = "";
			lblTotVASSurc.Text = "";
			lblESASurc.Text = "";
			lblTotAmt.Text = "";
			
			//Override
			txtFreightCharge.Text = "";
			txtInsrSurc.Text = "";
			txtOtherSurc.Text = "";
			lblTotVASSurcOverride.Text = "";
			txtESASurc.Text = "";
			lblTotAmtOverride.Text = "";

			lblLastUserUpd.Text = "";
			lblLastUpdateDT.Text = "";
		}

		private void ReadOnlyCriteria()
		{
			//Enable creiteria field to false
			txtConsigNo.Enabled=false;
			ddbCustomer.Enabled=false; 
			txtCustomer.Enabled=false;
			txtRefNo.Enabled = false;
			DbComboDestinationDC.Enabled = false;
			DbComboPathCode.Enabled = false;
			txtAct_pod_From.Enabled = false;
			txtAct_Pod_to.Enabled = false;
		}

		private void DisplayCurrentPage()//(Operation op)
		{
			if(m_sdsShipment.ds.Tables[0].Rows.Count>0)
			{
				DataRow dr = m_sdsShipment.ds.Tables[0].Rows[currentPage];
			
				//Set controls
				txtConsigNo.Text = dr["consignment_no"].ToString();
				txtRefNo.Text = dr["ref_no"].ToString();
				DbComboDestinationDC.Text = DbComboDestinationDC.Value = dr["destination_station"].ToString();
				DbComboPathCode.Text = DbComboPathCode.Value = dr["route_code"].ToString();				
				ddbCustomer.SelectedIndex = ddbCustomer.Items.IndexOf(ddbCustomer.Items.FindByValue(dr["payerid"].ToString().ToUpper()));
				ShowCustomerName();
				if(!dr["tracking_datetime"].Equals(DBNull.Value))
					lblActualPodDate.Text = Convert.ToDateTime(dr["tracking_datetime"]).ToString("dd/MM/yyyy");

				//				//Rated
				//				lblFreightCharge.Text = SurcStr(dr["original_rated_freight"], false);
				//				lblInsrSurc.Text = SurcStr(dr["original_rated_ins"], false);
				//				lblOtherSurc.Text = SurcStr(dr["original_rated_other"], false);
				//				lblTotVASSurc.Text = SurcStr(dr["tot_vas_surcharge"], false);
				//				lblESASurc.Text = SurcStr(dr["original_rated_esa"], false);
				//				lblTotAmt.Text = SurcStr(dr["original_rated_total"], false);
				//			
				//				//Override
				//				txtFreightCharge.Text=SurcStr(dr["tot_freight_charge"], true);
				//				txtInsrSurc.Text=SurcStr(dr["insurance_surcharge"], true);
				//				txtOtherSurc.Text=SurcStr(dr["other_surch_amount"], true);
				//				lblTotVASSurcOverride.Text = SurcStr(dr["tot_vas_surcharge"], false);
				//				txtESASurc.Text = SurcStr(dr["esa_surcharge"], true);
				//				lblTotAmtOverride.Text = SurcStr(dr["total_rated_amount"], false);

				
				//Freight
				if(dr["original_rated_freight"]!=DBNull.Value)
				{
					lblFreightCharge.Text = SurcStr(dr["original_rated_freight"], false);
					txtFreightCharge.Text = SurcStr(dr["tot_freight_charge"], true);
				}
				else
				{
					lblFreightCharge.Text = SurcStr(dr["tot_freight_charge"], false);
					txtFreightCharge.Text = SurcStr(dr["original_rated_freight"], true);
				}
				

				//Insurance
				if(dr["original_rated_ins"]!=DBNull.Value)
				{
					lblInsrSurc.Text = SurcStr(dr["original_rated_ins"], false);
					txtInsrSurc.Text = SurcStr(dr["insurance_surcharge"], true);
				}
				else
				{
					lblInsrSurc.Text = SurcStr(dr["insurance_surcharge"], false);
					txtInsrSurc.Text = SurcStr(dr["original_rated_ins"], true);
				}

				//Other
				if(dr["original_rated_other"]!=DBNull.Value)
				{
					lblOtherSurc.Text = SurcStr(dr["original_rated_other"], false);
					txtOtherSurc.Text = SurcStr(dr["other_surch_amount"], true);
				}
				else
				{
					lblOtherSurc.Text = SurcStr(dr["other_surch_amount"], false);
					txtOtherSurc.Text = SurcStr(dr["original_rated_other"], true);
				}


				lblTotVASSurc.Text = SurcStr(dr["tot_vas_surcharge"], false);
				lblTotVASSurcOverride.Text = SurcStr(dr["tot_vas_surcharge"], false);
				
				//ESA Surcharge
				if(dr["original_rated_esa"]!=DBNull.Value)
				{
					lblESASurc.Text = SurcStr(dr["original_rated_esa"], false);
					txtESASurc.Text = SurcStr(dr["esa_surcharge"], true);
				}
				else
				{
					lblESASurc.Text = SurcStr(dr["esa_surcharge"], false);
					txtESASurc.Text = SurcStr(dr["original_rated_esa"], true);				
				}
				
				//Total Amount
				//if(dr["original_rated_total"]!=DBNull.Value)
				//{
				//	lblTotAmt.Text = SurcStr(dr["original_rated_total"], false);
				//	//lblTotAmtOverride.Text = SurcStr(dr["total_rated_amount"], false);
				//}
				//else
				//{
				//	lblTotAmt.Text = SurcStr(dr["total_rated_amount"], false);
				//	//lblTotAmtOverride.Text = SurcStr(dr["original_rated_total"], false);
				//}
				CalTotAmtRated();
				CalTotAmtOverride();

				lblLastUserUpd.Text = dr["manual_over_user"].ToString();
				if(dr["manual_over_datetime"]!=DBNull.Value)
					lblLastUpdateDT.Text = Convert.ToDateTime(dr["manual_over_datetime"]).ToString("dd/MM/yyyy HH:mm");

				btnSave.Enabled = dr["invoice_date"]==DBNull.Value; //if a consignment has been invoiced then Disable 
			}
		}

		private object SelectF(object original, object current)
		{
			if(original!=DBNull.Value) 
				return original;
			else 
				return current;
		}

		private decimal StrToDec(object str)
		{
			decimal d=0;
			if(str!=null)
			{
				if(str!=DBNull.Value && str.ToString()!="")
					d=Convert.ToDecimal(str);
			}
			return d;
		}

		private object StrToObj(string str)
		{
			object d=DBNull.Value;
			if(str!=null && str!="")
			{
				d=Convert.ToDecimal(str);
			}
			return d;
		}

		private string SurcStr(object val, bool isTextBox)
		{
			string retVal = "";
			if(val!=DBNull.Value)
			{
				retVal = String.Format(m_format, Convert.ToDecimal(val));
			}
			else
			{
				retVal = isTextBox?"":String.Format(m_format, 0);
			}
			return retVal;
		}
		private bool IsNotEqualOriginal(string strTbox, object rowfield)
		{
			object valTbox = (strTbox!="")?(object)Convert.ToDecimal(strTbox):DBNull.Value;
			object valField = (rowfield!=DBNull.Value)?(object)Convert.ToDecimal(rowfield):DBNull.Value;			

			return (!valTbox.Equals(valField));
		}

		private void ShowCustomerName()
		{
			Customer cust = new Customer();
			cust.Populate(utility.GetAppID(), utility.GetEnterpriseID(),ddbCustomer.SelectedItem.Value);
			txtCustomer.Text=cust.CustomerName;
		}
		private void ExcecuteOperation(Operation op)
		{
			//AcceptChanges For Rejected case error or no record update.
			m_sdsShipment.ds.Tables[0].AcceptChanges(); 

			DataRow drCur = m_sdsShipment.ds.Tables[0].Rows[currentPage];

			//Get Shipment info
			CustomerCreditused ocustused = new CustomerCreditused();
		
			ocustused.consignment_no = drCur["consignment_no"].ToString();
			ocustused.cusid =drCur["payerid"].ToString();
			ocustused.applicationid = drCur["applicationid"].ToString();
			ocustused.enterpriseid = drCur["enterpriseid"].ToString();
			ocustused.bookingNo = drCur["booking_no"].ToString();
			DataTable dt = ocustused.getinfoShipment();

			if(op.Equals(Operation.Saved))//Saved Operation
			{
				//Override
				
				//if(IsNotEqualOriginal(txtFreightCharge.Text,drCur["tot_freight_charge"]))
				if(txtFreightCharge.Text!="")
				{
					if(drCur["original_rated_freight"]==DBNull.Value)
						drCur["original_rated_freight"] = drCur["tot_freight_charge"];
					drCur["tot_freight_charge"] = StrToObj(txtFreightCharge.Text);
				}
				//if(IsNotEqualOriginal(txtInsrSurc.Text,drCur["insurance_surcharge"]))
				if(txtInsrSurc.Text !="")
				{
					if(drCur["original_rated_ins"]==DBNull.Value)
						drCur["original_rated_ins"] = drCur["insurance_surcharge"];
					drCur["insurance_surcharge"]= StrToObj(txtInsrSurc.Text);
				}
				//if(IsNotEqualOriginal(txtOtherSurc.Text,drCur["other_surch_amount"]))
				if(txtOtherSurc.Text!="")
				{
					if(drCur["original_rated_other"]==DBNull.Value)
						drCur["original_rated_other"] = drCur["other_surch_amount"];
					drCur["other_surch_amount"]= StrToObj(txtOtherSurc.Text);
				}
				//if(IsNotEqualOriginal(txtESASurc.Text,drCur["esa_surcharge"]))
				if(txtESASurc.Text!="")
				{
					if(drCur["original_rated_esa"]==DBNull.Value)
						drCur["original_rated_esa"] = drCur["esa_surcharge"];
					drCur["esa_surcharge"]= StrToObj(txtESASurc.Text);
				}
				
				//Comment: ��� Label ��ҹ Override �������������� surcharge �ҧ��������١ override
				//if(IsNotEqualOriginal(lblTotAmtOverride.Text,drCur["total_rated_amount"]))
				//{
				//	drCur["original_rated_total"]=drCur["total_rated_amount"];
				//	drCur["total_rated_amount"]=StrToDec(lblTotAmtOverride.Text);					
				//}

				//�ӹǳ������ ��Ҥ��� DataRow �ҤԴ�ҡ
				decimal total_rated_amount = StrToDec(drCur["tot_freight_charge"]) + 
					StrToDec(drCur["insurance_surcharge"]) + 
					StrToDec(drCur["other_surch_amount"]) + 
					StrToDec(drCur["tot_vas_surcharge"]) +
					StrToDec(drCur["esa_surcharge"]);
				//if(IsNotEqualOriginal(total_rated_amount.ToString(),drCur["total_rated_amount"]))
				//{
					if(drCur["original_rated_total"]==DBNull.Value)
						drCur["original_rated_total"]=drCur["total_rated_amount"];
					drCur["total_rated_amount"]=total_rated_amount;
				//}

			}
			else //Clear Manual Override Operation
			{
				if(!drCur["original_rated_freight"].Equals(DBNull.Value))
				{
					drCur["tot_freight_charge"] = drCur["original_rated_freight"];
					drCur["original_rated_freight"] = DBNull.Value;
				}
				if(!drCur["original_rated_ins"].Equals(DBNull.Value))
				{
					drCur["insurance_surcharge"] = drCur["original_rated_ins"];
					drCur["original_rated_ins"]= DBNull.Value;
				}
				if(!drCur["original_rated_other"].Equals(DBNull.Value))
				{
					drCur["other_surch_amount"]=drCur["original_rated_other"];
					drCur["original_rated_other"]= DBNull.Value;
				}
				if(!drCur["original_rated_esa"].Equals(DBNull.Value))
				{
					drCur["esa_surcharge"]=drCur["original_rated_esa"];
					drCur["original_rated_esa"]= DBNull.Value;
				}
				if(!drCur["original_rated_total"].Equals(DBNull.Value))
				{
					drCur["total_rated_amount"]=drCur["original_rated_total"];
					drCur["original_rated_total"]=DBNull.Value;
				}	
			}
			
			//return;

			try
			{
				bool IsUpdate=false;
				DataTable gcTb = m_sdsShipment.ds.Tables[0].GetChanges();
				if(gcTb!=null)
				{
					DataTable gcModified = m_sdsShipment.ds.Tables[0].GetChanges(DataRowState.Modified);
					if(Convert.ToBoolean(gcModified.Rows.Count))
					{
						drCur["manual_override"] = op.Equals(Operation.Saved)?"Y":"N";					
						DataSet ds = m_sdsShipment.ds;
						IsUpdate = Convert.ToBoolean(TIESDAL.ManualRatingOverrideDAL.Update(utility, ref ds));
					}				
				}

				if(IsUpdate)
				{
					lblErrorMsg.Text =Utility.GetLanguageText(ResourceType.UserMessage,"INS_SUCCESSFULLY",utility.GetUserCulture());
					GetRecSet();
					m_sdsShipment.ds.Tables[0].AcceptChanges(); //For next Save
					DisplayCurrentPage();

					//update history
					decimal  total_rated_amount = decimal.Parse(dt.Rows[0]["total_rated_amount"].ToString());
					decimal  d_total_rated_amount = decimal.Parse(drCur["total_rated_amount"].ToString());

					if(total_rated_amount >0)
					{
						if(drCur["total_rated_amount"] != drCur["original_rated_total"])
						{
							if(( d_total_rated_amount-total_rated_amount) !=0)
							{
								ocustused.amount = d_total_rated_amount-total_rated_amount;
								ocustused.updateCreditused();
							}
						}
					}
				}
				else
				{
					m_sdsShipment.ds.Tables[0].RejectChanges();
					lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"NO_RECORD_UPD",utility.GetUserCulture());
				}
			}
			catch(ApplicationException ex)
			{
				m_sdsShipment.ds.Tables[0].RejectChanges();
				lblErrorMsg.Text = ex.Message;
			}
		}

		private void ExcecuteQuery()
		{
			FillForSearch();
			currentPage = currentSet = 0;		
			GetRecSet();
						
			//btnExecQry.Enabled = false;
			if(m_sdsShipment.QueryResultMaxSize == 0)
			{
				lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"NO_RECORDS_FOUND",utility.GetUserCulture());
				EnableNavigationButtons(false,false,false,false);	return;
			}
			else lblErrorMsg.Text = "";

			//Have more than 1 row
			btnExecQry.Enabled = false;
			ReadOnlyCriteria();

			if(m_sdsShipment != null && m_sdsShipment.DataSetRecSize > 0)
			{
				EnableNavigationButtons(false,false,true,true);
				btnSave.Enabled=true;
				btnClearManual.Enabled=true;
			}
			else 
			{
				btnSave.Enabled=false;
				btnClearManual.Enabled=false;
			}

			DisplayCurrentPage();//DisplayCurrentPage(Operation.None);
			DisplayRecNum();
		}
		
		private bool IsNumeric(string str) 
		{
			try 
			{
				Convert.ToDecimal(str);
				return true;
			}
			catch(FormatException) 
			{
				return false;
			}
		}
		#region Events of control
		protected void txtRefNo_TextChanged(object sender, EventArgs e)
		{
			ExcecuteQuery();
		}
		private void ddbCustomer_SelectedIndexChanged(object sender, EventArgs e)
		{
			ShowCustomerName();
		}
		private void btnQry_Click(object sender, EventArgs e)
		{
			ResetScreenForQuery();
		}

		private void btnExecQry_Click(object sender, EventArgs e)
		{		
			ExcecuteQuery();
		}

		private void btnSave_Click(object sender, EventArgs e)
		{
			ExcecuteOperation(Operation.Saved);
		}
		private void btnClearManual_Click(object sender, EventArgs e)
		{
			ExcecuteOperation(Operation.Update);
		}
		
		private void btnNextPage_Click(object sender, EventArgs e)
		{
			if(currentPage < (Convert.ToInt32(m_sdsShipment.DataSetRecSize) - 1) )
			{
				currentPage += 1;				
			}
			else
			{
				decimal iTotalRec = (Convert.ToInt32(ViewState["currentRefSet"]) * m_iSetSize) + Convert.ToInt32(m_sdsShipment.DataSetRecSize);
				if( iTotalRec ==  m_sdsShipment.QueryResultMaxSize)
				{
					EnableNavigationButtons(true,true,false,false);
					return;
				}
				
				currentSet += 1;	
				GetRecSet();
				currentPage = 0;
			}
			DisplayCurrentPage();//DisplayCurrentPage(Operation.None);
			DisplayRecNum();
			EnableNavigationButtons(true,true,true,true);
		}

		private void btnPreviousPage_Click(object sender, EventArgs e)
		{
			if(currentPage  > 0 )
			{
				currentPage -= 1;				
			}
			else
			{
				if( (currentSet - 1) < 0)
				{
					EnableNavigationButtons(false,false,true,true);
					return;
				}
				currentSet -= 1;	
				GetRecSet();
				System.Threading.Thread.Sleep(3500);

				currentPage = Convert.ToInt32(m_sdsShipment.DataSetRecSize) - 1;				
			}
			DisplayCurrentPage();//DisplayCurrentPage(Operation.None);
			DisplayRecNum();
			EnableNavigationButtons(true,true,true,true);
		}

		private void btnGoToFirstPage_Click(object sender, EventArgs e)
		{
			currentSet = 0;	
			currentPage = 0;
			GetRecSet();
			DisplayCurrentPage();//DisplayCurrentPage(Operation.None);
			DisplayRecNum();
			EnableNavigationButtons(false,false,true,true);
		}
		private void btnGoToLastPage_Click(object sender, EventArgs e)
		{
			currentSet = (int)Math.Floor((double)(m_sdsShipment.QueryResultMaxSize - 1)/m_iSetSize);//(m_sdsShipment.QueryResultMaxSize - 1)/m_iSetSize;	
			GetRecSet();
			currentPage = (int)Math.Abs(Math.IEEERemainder((double)(m_sdsShipment.QueryResultMaxSize - 1),(double)m_iSetSize)); //m_sdsShipment.DataSetRecSize - 1;
			DisplayCurrentPage();//DisplayCurrentPage(Operation.None);	
			DisplayRecNum();
			EnableNavigationButtons(true,true,false,false);
		}

		private void DisplayRecNum()
		{
			int iCurrentRec = (currentPage + 1) + (currentSet * m_iSetSize) ;

			if(!utility.GetUserCulture().ToUpper().Equals("EN-US"))
			{
				lblNumRec.Text = ""+ iCurrentRec + " " + Utility.GetLanguageText(ResourceType.ScreenLabel, "of", utility.GetUserCulture()) + " " + m_sdsShipment.QueryResultMaxSize + " " + Utility.GetLanguageText(ResourceType.ScreenLabel, "record(s)", utility.GetUserCulture());
			}
			else
			{
				lblNumRec.Text = ""+ iCurrentRec + " of " + m_sdsShipment.QueryResultMaxSize + " record(s)";
			}

			if(iCurrentRec>=m_sdsShipment.QueryResultMaxSize)
			{
				EnableNavigationButtons(true,true,false,false);
				return;
			}
		}

		private void EnableNavigationButtons(bool bMoveFirst, bool bMoveNext,bool bEnableMovePrevious, bool bMoveLast)
		{
			btnGoToFirstPage.Enabled	= bMoveFirst;
			btnPreviousPage.Enabled		= bMoveNext;
			btnNextPage.Enabled			= bEnableMovePrevious;
			btnGoToLastPage.Enabled		= bMoveLast;
		}

		protected void Charge_TextChanged(object sender, EventArgs e)
		{
			lblErrorMsg.Text = "";
			TextBox tbox = (TextBox)sender;
			string val = tbox.Text.Trim();
			if(val!="")
			{
				if(!IsNumeric(val))
				{
					tbox.Text = "";
					lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"PLS_ENTER_NOS",utility.GetUserCulture());
					return;
				}
				else
				{
					tbox.Text = String.Format(m_format,Convert.ToDecimal(val));
				}
			}

			CalTotAmtOverride();
		}


		private void CalTotAmtRated()
		{
			decimal tot =	StrToDec(lblFreightCharge.Text) + 
				StrToDec(lblInsrSurc.Text) + 
				StrToDec(lblOtherSurc.Text) + 
				StrToDec(lblTotVASSurc.Text) +
				StrToDec(lblESASurc.Text);
			lblTotAmt.Text = String.Format(m_format,Rounding(tot));
		}
		private void CalTotAmtOverride()
		{
			if(m_sdsShipment.ds.Tables[0].Rows.Count>0)
			{
				DataRow dr = m_sdsShipment.ds.Tables[0].Rows[currentPage];
				decimal tot=0;
				//bool hastot=false;
				//if(txtFreightCharge.Text!=""){ tot+=Convert.ToDecimal(txtFreightCharge.Text); hastot=true;}
				//if(dr["tot_vas_surcharge"]!=DBNull.Value){ tot+=Convert.ToDecimal(dr["tot_vas_surcharge"]); hastot=true;}
				//if(txtInsrSurc.Text!=""){tot+=Convert.ToDecimal(txtInsrSurc.Text); hastot=true;}
				//if(txtOtherSurc.Text!=""){ tot+=Convert.ToDecimal(txtOtherSurc.Text);hastot=true;}
				//if(txtESASurc.Text!=""){ tot+=Convert.ToDecimal(txtESASurc.Text); hastot=true;}
				//lblTotAmtOverride.Text = hastot?String.Format("{0:#,##0.00}",tot):"";
				
				tot =	StrToDec(txtFreightCharge.Text) + 
					StrToDec(txtInsrSurc.Text) + 
					StrToDec(txtOtherSurc.Text) + 
					StrToDec(dr["tot_vas_surcharge"]) +
					StrToDec(txtESASurc.Text);
				lblTotAmtOverride.Text = String.Format(m_format,Rounding(tot));
			}
		}

		[Cambro.Web.DbCombo.ResultsMethodAttribute(true)]
		public static object DbComboDistributionCenterSelect(ServerMethodArgs args)
		{
			Utility utl = new Utility(ConfigurationSettings.AppSettings,HttpContext.Current.Session);
			DataSet dataset = com.ties.classes.DbComboDAL.DistributionCenterQuery(utl.GetAppID(), utl.GetEnterpriseID(),args);	
			return dataset;
		}

		[Cambro.Web.DbCombo.ResultsMethodAttribute(true)]
		public static object DbComboPathCodeSelect(ServerMethodArgs args)
		{
			Utility utl = new Utility(ConfigurationSettings.AppSettings,HttpContext.Current.Session);
			String strDelType = "L";			
			String strWhereClause=null;
			if(args.ServerState != null && args.ServerState.Authenticate("kdfsjgnskajh[apq2-4uhg465654435879mnbfdm cxs;q4"))
			{
				if(args.ServerState["strDeliveryType"] != null && args.ServerState["strDeliveryType"].ToString().Length > 0)
				{
					strDelType = args.ServerState["strDeliveryType"].ToString();
					strWhereClause=" and Delivery_Type='"+Utility.ReplaceSingleQuote(strDelType)+"'";
					if(strDelType == "S") 
					{
						strWhereClause += @" and path_code in ( 
												 select distinct delivery_route 
												 from   Zipcode 
												 where  applicationid = '"+utl.GetAppID()+@"' and 
													    enterpriseid = '"+ utl.GetEnterpriseID() +"') ";
					}
				}				
			}
			
			DataSet dataset = DbComboDAL.PathCodeQuery(utl.GetAppID(),utl.GetEnterpriseID(),args, strWhereClause);	
			return dataset;
		}

		private void txtGoToRec_TextChanged(object sender, EventArgs e)
		{
			int iPageEnter = Convert.ToInt32(txtGoToRec.Text);
			if(iPageEnter<=0) iPageEnter=0;

			int iSetSize = Convert.ToInt32(m_sdsShipment.DataSetRecSize);
			if( iPageEnter> m_sdsShipment.QueryResultMaxSize)
			{
				txtGoToRec.Text=""; return;
			}

			currentSet = (int)Math.Floor((iPageEnter-1)/iSetSize);			
			String tmpStr = currentSet.ToString().Trim();
			int iStartIndex = 0;
			int intIndexOf = tmpStr.IndexOf(".");

			iStartIndex = (intIndexOf > 0)?Convert.ToInt32(tmpStr.Substring(0, intIndexOf)) * m_iSetSize:Convert.ToInt32(tmpStr) * m_iSetSize;
			m_sdsShipment = TIESDAL.ManualRatingOverrideDAL.Get(utility, m_sdsSearch, iStartIndex, m_iSetSize, false);			
			//System.Threading.Thread.Sleep(3500);

			int iCurrentPage = (int)Math.Abs(Math.IEEERemainder((iPageEnter-1),iSetSize)); 
			currentPage = (iCurrentPage>=0)?(int)Math.Abs(iCurrentPage):iSetSize - (int)Math.Abs(iCurrentPage);
			
			DisplayCurrentPage();//DisplayCurrentPage(Operation.None);
			DisplayRecNum();
			EnableNavigationButtons(true,true,true,true);

			if( iPageEnter >=  m_sdsShipment.QueryResultMaxSize)
			{
				EnableNavigationButtons(true,true,false,false);
				txtGoToRec.Text=""; return;
			}

			if( ((currentSet - 1) < 0) && ((currentPage - 1) < 0) )
			{
				EnableNavigationButtons(false,false,true,true);
				txtGoToRec.Text=""; return;
			}
			txtGoToRec.Text="";

		}
		#endregion
		
	}

}

