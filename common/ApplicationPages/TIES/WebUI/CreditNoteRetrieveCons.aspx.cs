using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using com.common.util;
using com.ties;
using com.ties.classes;
using com.ties.DAL;
using com.ties.BAL;
using com.common.classes;
using Cambro.Web.DbCombo;
using System.Configuration;
using com.common.applicationpages;

namespace TIES.WebUI
{
	/// <summary>
	/// Summary description for CreditNoteRetrieveCons.
	/// </summary>
	public class CreditNoteRetrieveCons : BasePopupPage
	{
		protected System.Web.UI.WebControls.Label lblErrorMsg;
		protected System.Web.UI.HtmlControls.HtmlGenericControl mainPanel;
		protected System.Web.UI.HtmlControls.HtmlTable mainTable;
		protected System.Web.UI.WebControls.Button btnSave;
		protected System.Web.UI.WebControls.Button btnCancel;
		protected System.Web.UI.WebControls.Label lblInvoiceNo;
		protected System.Web.UI.WebControls.Label lblTotalCreditRow;
		protected System.Web.UI.WebControls.Label lblConsignment;
		protected System.Web.UI.WebControls.Label lblTotalInvoice;
		protected System.Web.UI.WebControls.Button btnSearch;
		protected System.Web.UI.WebControls.Label lblTotalSelected;
		protected System.Web.UI.WebControls.Button btnAllocate;
		protected System.Web.UI.WebControls.TextBox txtTotalCreditFRG;
		protected System.Web.UI.WebControls.TextBox txtTotalCreditINS;
		protected System.Web.UI.WebControls.TextBox txtTotalCreditVAS;
		protected System.Web.UI.WebControls.TextBox txtTotalCreditESA;
		protected System.Web.UI.WebControls.TextBox txtTotalCreditOTH;
		protected System.Web.UI.WebControls.TextBox txtTotalCreditPDX;
		protected System.Web.UI.WebControls.TextBox txtTotalCreditADJ;
		protected System.Web.UI.WebControls.TextBox txtTotalCreditTOT;
		protected System.Web.UI.WebControls.TextBox txtTotalInvoiceFRG;
		protected System.Web.UI.WebControls.TextBox txtTotalInvoiceINS;
		protected System.Web.UI.WebControls.TextBox txtTotalInvoiceVAS;
		protected System.Web.UI.WebControls.TextBox txtTotalInvoiceESA;
		protected System.Web.UI.WebControls.TextBox txtTotalInvoiceOTH;
		protected System.Web.UI.WebControls.TextBox txtTotalInvoicePDX;
		protected System.Web.UI.WebControls.TextBox txtTotalInvoiceADJ;
		protected System.Web.UI.WebControls.TextBox txtTotalInvoiceTOT;
		protected System.Web.UI.WebControls.TextBox txtTotalSelectFRG;
		protected System.Web.UI.WebControls.TextBox txtTotalSelectINS;
		protected System.Web.UI.WebControls.TextBox txtTotalSelectVAS;
		protected System.Web.UI.WebControls.TextBox txtTotalSelectESA;
		protected System.Web.UI.WebControls.TextBox txtTotalSelectOTH;
		protected System.Web.UI.WebControls.TextBox txtTotalSelectPDX;
		protected System.Web.UI.WebControls.TextBox txtTotalSelectADJ;
		protected System.Web.UI.WebControls.TextBox txtTotalSelectTOT;
		protected System.Web.UI.WebControls.Label lblTitle;

		SessionDS m_sdsCreditNote = null;
		//SessionDS m_sdsCreditDetail=null;

		String appID=null;
		String enterpriseID=null;
		String userID=null;

		
		protected System.Web.UI.WebControls.TextBox txtgTotalCredit;
		protected System.Web.UI.WebControls.TextBox txtgConsignment;
		protected System.Web.UI.WebControls.TextBox txtTotalCredit;
		protected System.Web.UI.WebControls.TextBox txtConsignment;

		//Invoice Header information
		//DataSet dsINVHeader;
		DataTable dtINVHeader;
		protected com.common.util.msTextBox txtTotalAllocateFRG;
		protected com.common.util.msTextBox txtTotalAllocateINS;
		protected com.common.util.msTextBox txtTotalAllocateVAS;
		protected com.common.util.msTextBox txtTotalAllocateESA;
		protected com.common.util.msTextBox txtTotalAllocateOTH;
		protected com.common.util.msTextBox txtTotalAllocatePDX;
		protected com.common.util.msTextBox txtTotalAllocateADJ;
		protected com.common.util.msTextBox txtTotalAllocateTOT;
		protected System.Web.UI.WebControls.Button btnSelectAll;
		protected com.common.util.msTextBox Mstextbox1;
		protected System.Web.UI.WebControls.RegularExpressionValidator RegularExpressionValidator1;
		protected System.Web.UI.WebControls.Label lblTotal;
		protected System.Web.UI.WebControls.Label lblFRG;
		protected System.Web.UI.WebControls.Label lblINS;
		protected System.Web.UI.WebControls.Label lblVAS;
		protected System.Web.UI.WebControls.Label lblESA;
		protected System.Web.UI.WebControls.Label lblOTH;
		protected System.Web.UI.WebControls.Label lblPDX;
		protected System.Web.UI.WebControls.Label lblADJ;
		protected System.Web.UI.WebControls.Label lblTOT;
		protected System.Web.UI.WebControls.Label lblTotalCreditCol;
		protected System.Web.UI.WebControls.DataGrid dgConsignment;
		//DataRow drINVHeader;
		
		//Invoice Detail Information
		//DataSet dsINVDetail;
		DataTable dtINVDetail;
		protected System.Web.UI.HtmlControls.HtmlInputHidden hidtxt;
		protected System.Web.UI.WebControls.Label lblRemark;
		//DataRow drINVDetail;
		public string strFormatCurrency ;
		private void Page_Load(object sender, System.EventArgs e)
		{
 			// Put user code to initialize the page here
			utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings,Page.Session);	

			appID = utility.GetAppID();
			enterpriseID = utility.GetEnterpriseID();
			userID = utility.GetUserID();

			if (!Page.IsPostBack)
			{

				DataSet profileDS = SysDataManager1.GetEnterpriseProfile(appID, enterpriseID);

				if(profileDS.Tables[0].Rows.Count > 0)
				{
					int currency_decimal = (int) profileDS.Tables[0].Rows[0]["currency_decimal"];
					ViewState["m_format"] = "{0:F" + currency_decimal.ToString() + "}";
					strFormatCurrency=ViewState["m_format"].ToString();
				}


				 

				ViewState.Add("PrevPageIndex", 0);

				// For test passing data from credit notes form, please comment before release.
				m_sdsCreditNote = CreditDebitMgrDAL.GetEmptyCreditNoteDS(1);
				DataRow drTest = m_sdsCreditNote.ds.Tables[0].Rows[0];

				drTest["invoice_no"] = (string)Session["Invoice_no"];
				drTest["invoice_date"] = DateTime.Now;					
				
				InitialDisplay();
				//Populate invoice detail datatable
				DisplayTotalConsignmentAmount(dtINVDetail);
				DisplayTotalInvoiceAmount(dtINVHeader.Rows[0]);

			}
			else
			{
				if(Session["SESSION_DS1"] != null)
				{
					//Get invoice header datarow from session
					dtINVHeader = (DataTable)Session["SESSION_DS1"];

				}
				if(Session["SESSION_DS2"] != null)
				{
					//Get invoice detail datatable from session
					dtINVDetail = (DataTable)Session["SESSION_DS2"];
				}				
			}
			strFormatCurrency=ViewState["m_format"].ToString();

		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
			this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
			this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
			this.btnAllocate.Click += new System.EventHandler(this.btnAllocate_Click);
			this.btnSelectAll.Click += new System.EventHandler(this.btnSelectAll_Click);
			this.dgConsignment.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.dgConsignment_ItemDataBound);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		public void DisplayTotalInvoiceAmount(DataRow drInvoiceInfo)
		{
			Decimal dFRG,dESA,dOTH = 0;
			object oFRG,oESA = null;			
			if(dtINVDetail.Rows.Count>0)
			{
				oFRG = dtINVDetail.Compute("Sum(tot_freight_charge)", "mbg_amount=0");			
				oESA = dtINVDetail.Compute("Sum(esa_surcharge)", "mbg_amount=0");				
				if(oFRG!=null && oFRG!=DBNull.Value) dFRG=Decimal.Parse(oFRG.ToString());
				else  dFRG=0;
				if(oESA!=null && oESA!=DBNull.Value) dESA=Decimal.Parse(oESA.ToString());
				else  dESA=0;							
			}
			else
			{
				dFRG=0;dESA=0;
			}
			if(Utility.IsNotDBNull(drInvoiceInfo["other_surcharge"]))
			{
				dOTH=Convert.ToDecimal(drInvoiceInfo["other_surcharge"]);
			}
			txtTotalInvoiceFRG.Text = String.Format((String)ViewState["m_format"],dFRG);
			txtTotalInvoiceINS.Text = String.Format((String)ViewState["m_format"],drInvoiceInfo["insurance_amt"]);
			txtTotalInvoiceVAS.Text = String.Format((String)ViewState["m_format"],drInvoiceInfo["tot_vas_surcharge"]);
			txtTotalInvoiceESA.Text = String.Format((String)ViewState["m_format"],dESA);
			txtTotalInvoiceOTH.Text = String.Format((String)ViewState["m_format"],dOTH);
			txtTotalInvoicePDX.Text = String.Format((String)ViewState["m_format"],drInvoiceInfo["total_exception"]);
			txtTotalInvoiceADJ.Text = String.Format((String)ViewState["m_format"],drInvoiceInfo["invoice_adj_amount"]);
			txtTotalInvoiceTOT.Text = String.Format((String)ViewState["m_format"],drInvoiceInfo["invoice_amt"]);
		}

		public void DisplayTotalConsignmentAmount(DataTable dtInvoiceDetail)
		{
			dgConsignment.PageSize = Convert.ToInt16(System.Configuration.ConfigurationSettings.AppSettings["RecordPerPage"]);
			dgConsignment.DataSource = dtInvoiceDetail; 
			dgConsignment.DataBind();
		}


		public void InitialDisplay()
		{
			//Get current record of credit note header
			DataRow drCNHeader = m_sdsCreditNote.ds.Tables[0].Rows[System.Convert.ToInt32(ViewState["currentPage"])];
			
			//Show invoice number
			if(Utility.IsNotDBNull(drCNHeader["Invoice_No"])==true && drCNHeader["Invoice_No"].ToString() !="")
			{
				lblInvoiceNo.Text=Utility.GetLanguageText(ResourceType.UserMessage, "COPY_FROM_INVOICE_NO",utility.GetUserCulture ());
				lblInvoiceNo.Text=lblInvoiceNo.Text + " " + (String) drCNHeader["Invoice_No"].ToString();
			}				
			else
			{
				lblInvoiceNo.Text="";
			}



			
			// Get invoice header information
			try
			{
				DataSet dsINVHeader = InvoiceManagementMgrDAL.GetInvoiceByInvNo(appID,enterpriseID,drCNHeader["Invoice_No"].ToString());
				dtINVHeader = dsINVHeader.Tables[0];
				DataRow drINVHeader = dtINVHeader.Rows[0];  
				//Keep invoice header information into viewstate
				Session["SESSION_DS1"]=dtINVHeader; 

				if ((drINVHeader["mbg_amount"] == DBNull.Value  || Convert.ToDecimal(drINVHeader["mbg_amount"]) == 0) && (drINVHeader["invoice_adj_amount"] == DBNull.Value || Convert.ToDecimal(drINVHeader["invoice_adj_amount"]) == 0))
				{	
					lblErrorMsg.Text = "";//return true;		
				}
				else
					lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage, "MSG_CONS_EX_MBG",utility.GetUserCulture ());//return false;

				//Show invoice date
				if (Utility.IsNotDBNull(drINVHeader["Invoice_Date"])==true && drINVHeader["Invoice_Date"].ToString() !="")
				{	
					DateTime dtInvoiceDate=System.Convert.ToDateTime(drINVHeader["Invoice_Date"]);
					lblInvoiceNo.Text=lblInvoiceNo.Text + " " + dtInvoiceDate.ToString("dd/MM/yyyy",null);			
				}
				else
					lblInvoiceNo.Text=lblInvoiceNo.Text + " " ;

			}
			catch (Exception ex)
			{
				throw ex;
			}


			//Get invoice detail information
			try
			{
				DataSet dsINVDetail = InvoiceManagementMgrDAL.GetInvoiceDetailForCreditNote(appID,enterpriseID,drCNHeader["Invoice_No"].ToString());
				dtINVDetail = dsINVDetail.Tables[0];
				//drINVDetail = dtINVDetail.Rows[0]; 
				//Keep invoice detail information into viewstate
				Session["SESSION_DS2"]=dtINVDetail;

			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		public void dgConsignment_PageIndexChanged(object source, System.Web.UI.WebControls.DataGridPageChangedEventArgs e)
		{
			this.lblErrorMsg.Text="";

			dgConsignment.CurrentPageIndex = e.NewPageIndex;
			dgConsignment.DataSource = dtINVDetail;
			dgConsignment.DataBind();
		}

		public void btnSearch_Click(object sender, System.EventArgs e)
		{
			this.lblErrorMsg.Text = "";
			//DataRow[] drConsignmentSelected;
			if (txtConsignment.Text!="")
			{
				DataRow[] drCONSelected=dtINVDetail.Select("Consignment_No like '%" + txtConsignment.Text  + "%'");


				if (drCONSelected.Length <= 0)
				{	
					lblErrorMsg.Text = "Consignment not on invoice.";//Utility.GetLanguageText(ResourceType.UserMessage, "msg_ConsNotFound",utility.GetUserCulture ());//return false;
					return;
				}
				else 
					lblErrorMsg.Text = "";
				dtINVDetail.DefaultView.RowFilter ="Consignment_No like '%" + txtConsignment.Text  + "%'";
				dgConsignment.CurrentPageIndex = 0;
				dgConsignment.DataSource = dtINVDetail.DefaultView ;
				dgConsignment.DataBind();
			}
			else
			{
				dtINVDetail.DefaultView.RowFilter="";
				dgConsignment.CurrentPageIndex = 0;
				dgConsignment.DataSource= dtINVDetail;
				dgConsignment.DataBind();
			}
		}

		public void dgConsignment_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			msTextBox txtFRG = (msTextBox) dgConsignment.Items[0].FindControl("txtFRG");
			lblErrorMsg.Text ="aaa" + txtFRG.Text + "aaa";
		}

		public DataSet CalculateAllocateAmount(DataSet dsDetail, decimal[] iAllocateAmount )
		{
			return  dsDetail;
		}

		public void btnAllocate_Click(object sender, System.EventArgs e)
		{
			this.lblErrorMsg.Text = "";
			//If none allocate amount input.
			if(
				((this.txtTotalAllocateFRG.Text=="")||((this.txtTotalAllocateFRG.Text!="")?Double.Parse(txtTotalAllocateFRG.Text):0)<0) &&
				((this.txtTotalAllocateINS.Text=="")||((this.txtTotalAllocateINS.Text!="")?Double.Parse(txtTotalAllocateINS.Text):0)<0) &&
				((this.txtTotalAllocateVAS.Text=="")||((this.txtTotalAllocateVAS.Text!="")?Double.Parse(txtTotalAllocateVAS.Text):0)<0) && 
				((this.txtTotalAllocateESA.Text=="")||((this.txtTotalAllocateESA.Text!="")?Double.Parse(txtTotalAllocateESA.Text):0)<0) && 
				((this.txtTotalAllocateOTH.Text=="")||((this.txtTotalAllocateOTH.Text!="")?Double.Parse(txtTotalAllocateOTH.Text):0)<0) && 
				((this.txtTotalAllocatePDX.Text=="")||((this.txtTotalAllocatePDX.Text!="")?Double.Parse(txtTotalAllocatePDX.Text):0)<0) && 
				((this.txtTotalAllocateADJ.Text=="")||((this.txtTotalAllocateADJ.Text!="")?Double.Parse(txtTotalAllocateADJ.Text):0)<0) && 
				((this.txtTotalAllocateTOT.Text=="")||((this.txtTotalAllocateTOT.Text!="")?Double.Parse(txtTotalAllocateTOT.Text):0)<0) 
				)	
			{
				lblErrorMsg.Text = "Please input allocate amount.";
				return;
			}
			if(
				 this.chkBeforeAllocate() && 
				((this.txtTotalAllocateTOT.Text!="")||((this.txtTotalAllocateTOT.Text!="")?Double.Parse(txtTotalAllocateTOT.Text):0)>0) 
				)	
			{
				lblErrorMsg.Text = "Please input either TOT or charges";
				return;
			}
			//If none Consignment Checked - Alert
			dtINVDetail=(DataTable)(Session["SESSION_DS2"])  ;
			int iCheckedRows = 0;
			object oCheckedCount = dtINVDetail.Compute("Sum(Checked)", "Checked = 1");
			if(oCheckedCount!=null && oCheckedCount!=DBNull.Value) iCheckedRows = Int32.Parse(oCheckedCount.ToString());
			if(iCheckedRows<=0)
			{
				lblErrorMsg.Text = "Please select consignment.";
				return;
			}

			//Sum total of  each selected consignments

			object oMBG = dtINVDetail.Compute("Sum(mbg_amount)", "Checked = 1");
			Decimal dMBG=0;
			if(oMBG!=null && oMBG!=DBNull.Value) dMBG = Decimal.Parse(oMBG.ToString());
			object oIADJ = dtINVDetail.Compute("Sum(invoice_adj_amount)", "Checked = 1");
			if(dMBG!=0)
			{
				lblErrorMsg.Text = "Exceed Consignment's Invoiced value.";
				return;
			}
//			Decimal dIADJ=0;
//			if(oIADJ!=null && oIADJ!=DBNull.Value) dIADJ = Decimal.Parse(oIADJ.ToString());
			object oSumInvoiceamt = dtINVDetail.Compute("Sum(invoice_amt)","Checked=1");
			double dConsignmentTotal= 0;
			if(oSumInvoiceamt!=null && oSumInvoiceamt!=DBNull.Value) dConsignmentTotal = double.Parse(oSumInvoiceamt.ToString());


			//All Allocate inputs < = Sum total of selected consignments
			double dSumAllocate = ((this.txtTotalAllocateFRG.Text!="")?Double.Parse(txtTotalAllocateFRG.Text):0) ;
			dSumAllocate += ((this.txtTotalAllocateINS.Text!="")?Double.Parse(txtTotalAllocateINS.Text):0) ;	
			dSumAllocate += ((this.txtTotalAllocateVAS.Text!="")?Double.Parse(txtTotalAllocateVAS.Text):0) ;	
			dSumAllocate += ((this.txtTotalAllocateESA.Text!="")?Double.Parse(txtTotalAllocateESA.Text):0) ;	
			dSumAllocate += ((this.txtTotalAllocateOTH.Text!="")?Double.Parse(txtTotalAllocateOTH.Text):0) ;	
			dSumAllocate += ((this.txtTotalAllocatePDX.Text!="")?Double.Parse(txtTotalAllocatePDX.Text):0) ;	
			dSumAllocate += ((this.txtTotalAllocateADJ.Text!="")?Double.Parse(txtTotalAllocateADJ.Text):0) ;	
			dSumAllocate += ((this.txtTotalAllocateTOT.Text!="")?Double.Parse(txtTotalAllocateTOT.Text):0) ;	
			
			if(dSumAllocate > dConsignmentTotal)
			{
				lblErrorMsg.Text="Allocate cannot exceed consignment total.";
				return;
			}

			//Verify Allocated does not exceed Selected Cons's Value
			//FRG
			object oSumFRG = dtINVDetail.Compute("Sum(tot_freight_charge)","Checked=1");
			Double dSumFRG= 0;			
			if(oSumFRG!=null && oSumFRG!=DBNull.Value)
			{
				dSumFRG = Double.Parse(oSumFRG.ToString());				
			}
			if(dSumFRG<((this.txtTotalAllocateFRG.Text!="")?Double.Parse(txtTotalAllocateFRG.Text):0))
			{
				lblErrorMsg.Text = "Exceed Consignment's Invoiced value.";
				return;
			}

			//INS
			object oSumINS = dtINVDetail.Compute("Sum(insurance_amt)","Checked=1");
			double dSumINS= 0;
			if(oSumINS!=null && oSumINS!=DBNull.Value) dSumINS = double.Parse(oSumINS.ToString());
			if(dSumINS<((this.txtTotalAllocateINS.Text!="")?Double.Parse(txtTotalAllocateINS.Text):0))
			{
				lblErrorMsg.Text = "Exceed Consignment's Invoiced value.";
				return;
			}
			//VAS
			object oSumVAS = dtINVDetail.Compute("Sum(tot_vas_surcharge)","Checked=1");
			double dSumVAS= 0;
			if(oSumVAS!=null && oSumVAS!=DBNull.Value) dSumVAS = double.Parse(oSumVAS.ToString());
			if(dSumVAS<((this.txtTotalAllocateVAS.Text!="")?Double.Parse(txtTotalAllocateVAS.Text):0))
			{
				lblErrorMsg.Text = "Exceed Consignment's Invoiced value.";
				return;
			}
			//ESA
			object oSumESA = dtINVDetail.Compute("Sum(esa_surcharge)","Checked=1");
			double dSumESA= 0;
			if(oSumESA!=null && oSumESA!=DBNull.Value) dSumESA = double.Parse(oSumESA.ToString());
			if(dSumESA<((this.txtTotalAllocateESA.Text!="")?Double.Parse(txtTotalAllocateESA.Text):0))
			{
				lblErrorMsg.Text = "Exceed Consignment's Invoiced value.";
				return;
			}
			//OTH
			object oSumOTH = dtINVDetail.Compute("Sum(other_surcharge)","Checked=1");
			double dSumOTH= 0;
			if(oSumOTH!=null && oSumOTH!=DBNull.Value) dSumOTH = double.Parse(oSumOTH.ToString());
			if(dSumOTH<((this.txtTotalAllocateOTH.Text!="")?Double.Parse(txtTotalAllocateOTH.Text):0))
			{
				lblErrorMsg.Text = "Exceed Consignment's Invoiced value.";
				return;
			}
			//PDX
			object oSumPDX = dtINVDetail.Compute("Sum(total_exception)","Checked=1");
			double dSumPDX= 0;
			if(oSumPDX!=null && oSumPDX!=DBNull.Value) dSumPDX = double.Parse(oSumPDX.ToString());
			if(dSumPDX<((this.txtTotalAllocatePDX.Text!="")?Double.Parse(txtTotalAllocatePDX.Text):0))
			{
				lblErrorMsg.Text = "Exceed Consignment's Invoiced value.";
				return;
			}
			//ADJ
			object oSumADJ = dtINVDetail.Compute("Sum(invoice_adj_amount)","Checked=1 and invoice_adj_amount>=0");
			double dSumADJ= 0;
			if(oSumADJ!=null && oSumADJ!=DBNull.Value) dSumADJ = double.Parse(oSumADJ.ToString());
			if(dSumADJ<((this.txtTotalAllocateADJ.Text!="")?Double.Parse(txtTotalAllocateADJ.Text):0))
			{
				lblErrorMsg.Text = "Exceed Consignment's Invoiced value.";
				return;
			}


			//Clear Data in unselected cons
			DataRow[] drUnselectedCons = this.dtINVDetail.Select(" Checked=0 ");
			foreach(DataRow dr in drUnselectedCons)
			{
				System.Reflection.FieldInfo fieldInfo = dr.GetType().GetField("rowID",System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance); 
				int rowID = (int)fieldInfo.GetValue(dr);
				dtINVDetail.Rows[rowID-1]["FRG"]=0.00;
				dtINVDetail.Rows[rowID-1]["INS"]=0.00;
				dtINVDetail.Rows[rowID-1]["VAS"]=0.00;
				dtINVDetail.Rows[rowID-1]["ESA"]=0.00;
				dtINVDetail.Rows[rowID-1]["OTH"]=0.00;
				dtINVDetail.Rows[rowID-1]["PDX"]=0.00;
				dtINVDetail.Rows[rowID-1]["ADJ"]=0.00;
				dtINVDetail.Rows[rowID-1]["TOT"]=0.00;
				dtINVDetail.Rows[rowID-1]["TotalCredit"] =0.00;
			}
			//assign selected con. with calculated allocated value
			DataRow[] drSelectedCons = this.dtINVDetail.Select(" Checked = 1 ");
			Enterprise enterprise = new Enterprise();
			enterprise.Populate(utility.GetAppID(),utility.GetEnterpriseID());
			int currencydec =0;
			currencydec = enterprise.CurrencyDecimal ;
			foreach(DataRow dr in drSelectedCons)
			{
				System.Reflection.FieldInfo fieldInfo = dr.GetType().GetField("rowID",System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance); 
				int rowID = (int)fieldInfo.GetValue(dr);
				//CASE FRG
				if((txtTotalAllocateFRG.Text!=""?Double.Parse(txtTotalAllocateFRG.Text):0.00)>=0)
					dSumFRG=(dSumFRG==0?1:dSumFRG);
					dtINVDetail.Rows[rowID-1]["FRG"]= this.ConvertSubString((txtTotalAllocateFRG.Text!=""?Double.Parse(txtTotalAllocateFRG.Text):0.00) * (Double.Parse(dtINVDetail.Rows[rowID-1]["tot_freight_charge"].ToString())/dSumFRG),currencydec) ;
				//CASE INS
				if((txtTotalAllocateINS.Text!=""?Double.Parse(txtTotalAllocateINS.Text):0.00)>=0)
					dSumINS=(dSumINS==0?1:dSumINS);
					dtINVDetail.Rows[rowID-1]["INS"]= this.ConvertSubString((txtTotalAllocateINS.Text!=""?Double.Parse(txtTotalAllocateINS.Text):0.00) * (Double.Parse(dtINVDetail.Rows[rowID-1]["insurance_amt"].ToString())/dSumINS),currencydec) ;
				//CASE VAS
				if((txtTotalAllocateVAS.Text!=""?Double.Parse(txtTotalAllocateVAS.Text):0.00)>=0)
					dSumVAS=(dSumVAS==0?1:dSumVAS);
					dtINVDetail.Rows[rowID-1]["VAS"]= this.ConvertSubString((txtTotalAllocateVAS.Text!=""?Double.Parse(txtTotalAllocateVAS.Text):0.00) * (Double.Parse(dtINVDetail.Rows[rowID-1]["tot_vas_surcharge"].ToString())/dSumVAS),currencydec) ;
				//CASE ESA
				if((txtTotalAllocateESA.Text!=""?Double.Parse(txtTotalAllocateESA.Text):0.00)>=0)
					dSumESA=(dSumESA==0?1:dSumESA);
					dtINVDetail.Rows[rowID-1]["ESA"]= this.ConvertSubString((txtTotalAllocateESA.Text!=""?Double.Parse(txtTotalAllocateESA.Text):0.00) * (Double.Parse(dtINVDetail.Rows[rowID-1]["esa_surcharge"].ToString())/dSumESA),currencydec) ;
				//CASE OTH
				if((txtTotalAllocateOTH.Text!=""?Double.Parse(txtTotalAllocateOTH.Text):0.00)>=0)
					dSumOTH=(dSumOTH==0?1:dSumOTH);
				dtINVDetail.Rows[rowID-1]["OTH"]= this.ConvertSubString((txtTotalAllocateOTH.Text!=""?Double.Parse(txtTotalAllocateOTH.Text):0.00) * ((dtINVDetail.Rows[rowID-1]["other_surcharge"].ToString()!=""?Double.Parse(dtINVDetail.Rows[rowID-1]["other_surcharge"].ToString()):0.00)/dSumOTH),currencydec) ;
				//CASE PDX
				if((txtTotalAllocatePDX.Text!=""?Double.Parse(txtTotalAllocatePDX.Text):0.00)>=0)
					dSumPDX=(dSumPDX==0?1:dSumPDX);
					dtINVDetail.Rows[rowID-1]["PDX"]= this.ConvertSubString((txtTotalAllocatePDX.Text!=""?Double.Parse(txtTotalAllocatePDX.Text):0.00) * (Double.Parse(dtINVDetail.Rows[rowID-1]["total_exception"].ToString())/dSumPDX),currencydec) ;
				//CASE ADJ
				if((txtTotalAllocateADJ.Text!=""?Double.Parse(txtTotalAllocateADJ.Text):0.00)>=0)
					dSumADJ=(dSumADJ==0?1:dSumADJ);
					dtINVDetail.Rows[rowID-1]["ADJ"]= this.ConvertSubString((txtTotalAllocateADJ.Text!=""?Double.Parse(txtTotalAllocateADJ.Text):0.00) * (Double.Parse(dtINVDetail.Rows[rowID-1]["invoice_adj_amount"].ToString())/dSumADJ),currencydec) ;
				//CASE TOT
				if((txtTotalAllocateTOT.Text!=""?Double.Parse(txtTotalAllocateTOT.Text):0.00)>=0)
					dtINVDetail.Rows[rowID-1]["TOT"]= this.ConvertSubString((txtTotalAllocateTOT.Text!=""?Double.Parse(txtTotalAllocateTOT.Text):0.00) * (Double.Parse(dtINVDetail.Rows[rowID-1]["invoice_amt"].ToString())/dConsignmentTotal),currencydec) ;

				//Sum Consignment			
				double dTotalCredit = Double.Parse(dtINVDetail.Rows[rowID-1]["FRG"].ToString());
				dTotalCredit += Double.Parse(dtINVDetail.Rows[rowID-1]["INS"].ToString());
				dTotalCredit += Double.Parse(dtINVDetail.Rows[rowID-1]["VAS"].ToString());
				dTotalCredit += Double.Parse(dtINVDetail.Rows[rowID-1]["ESA"].ToString());
				dTotalCredit += Double.Parse(dtINVDetail.Rows[rowID-1]["OTH"].ToString());
				dTotalCredit += Double.Parse(dtINVDetail.Rows[rowID-1]["PDX"].ToString());
				dTotalCredit += Double.Parse(dtINVDetail.Rows[rowID-1]["ADJ"].ToString());
				dTotalCredit += Double.Parse(dtINVDetail.Rows[rowID-1]["TOT"].ToString());
				dtINVDetail.Rows[rowID-1]["TotalCredit"] = dTotalCredit;
				double dInvAmnt = Double.Parse(dtINVDetail.Rows[rowID-1]["invoice_amt"].ToString());
				if(dInvAmnt<dTotalCredit)
				{
					dtINVDetail.RejectChanges();
					lblErrorMsg.Text = "Exceed Consignment's Invoiced value.";
					return;
				}
				
			}
			AdjustAllocateValue(drSelectedCons);
			dtINVDetail.AcceptChanges();
			Session["SESSION_DS2"] = dtINVDetail;

			UpdateTotalSelected();
			UpdateTotalCredit();
			dgConsignment.DataSource = dtINVDetail;
			dgConsignment.DataBind();

		}

		public void btnSave_Click(object sender, System.EventArgs e)
		{
			//Filter only credited cons
			if(this.lblErrorMsg.Text.Length>0)
			{
				return;
			}
			DataRow[] drCreditCons = this.dtINVDetail.Select(" TotalCredit <>0 ");
			if(drCreditCons.Length<=0)
				Session["dtRetrieveCons"]=CreditDebitMgrDAL.GetEmptyCreditDetailDS(0);
			else
				Session["dtRetrieveCons"] = ConvertInvConToCRCon(drCreditCons);
			Response.Redirect("CreditNote.aspx?"+(string)Session["CreditNoteParentID"]);
		}
		private void btnCancel_Click(object sender, System.EventArgs e)
		{
			Session["dtRetrieveCons"]=CreditDebitMgrDAL.GetEmptyCreditDetailDS(0);
			Response.Redirect("CreditNote.aspx?"+(string)Session["CreditNoteParentID"]);
		}
		//=======Event Handler =====

		public void chkAllConsignment_CheckChanged(object sender, System.EventArgs e)
		{
			CheckAllConsignments();
		}

		public void chkConsignment_CheckChanged(object sender, System.EventArgs e)
		{
			CheckBox chkCon = (CheckBox)sender;
			DataGridItem dgItem = (DataGridItem)chkCon.NamingContainer;
			
			TextBox dgConsignmenttxtConsingmentNo = (TextBox)dgConsignment.Items[dgItem.ItemIndex].FindControl("txtgConsignment");
			CheckConsignment(dgConsignmenttxtConsingmentNo.Text,chkCon.Checked);
			UpdateTotalSelected();
		}

		public void txtFRG_TextChanged(object sender, System.EventArgs e)
		{
			TextBox txtFRG = (TextBox)sender;
			DataGridItem dgItem = (DataGridItem)txtFRG.NamingContainer;
			
			if(VerifyConsignmentData(dgItem.ItemIndex,sender)==false)
				return;
			
			TextBox dgConsignmenttxtConsingmentNo = (TextBox)dgConsignment.Items[dgItem.ItemIndex].FindControl("txtgConsignment");

			DataRow dr = dtINVDetail.Rows.Find(dgConsignmenttxtConsingmentNo.Text);
			System.Reflection.FieldInfo fieldInfo = dr.GetType().GetField("rowID",System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance); 
			int rowID = (int)fieldInfo.GetValue(dr);
			dtINVDetail.Rows[rowID-1]["FRG"]=(txtFRG.Text!=""?Double.Parse(txtFRG.Text):0.00);
			dtINVDetail.AcceptChanges();
			Session["SESSION_DS2"] = dtINVDetail;

			UpdateTotalCredit(dgConsignmenttxtConsingmentNo.Text,dgItem.ItemIndex);
			TextBox txtINS = (TextBox)dgConsignment.Items[dgItem.ItemIndex].FindControl("txtINS");
			Page.RegisterStartupScript("SetFocus", "<script>document.getElementById('" + txtINS.ClientID + "').focus();document.getElementById('" + txtINS.ClientID + "').select();</script>");
		}

		public void txtINS_TextChanged(object sender, System.EventArgs e)
		{
			TextBox txtINS = (TextBox)sender;
			DataGridItem dgItem = (DataGridItem)txtINS.NamingContainer;
			
			if(VerifyConsignmentData(dgItem.ItemIndex,sender)==false)
				return;
			
			TextBox dgConsignmenttxtConsingmentNo = (TextBox)dgConsignment.Items[dgItem.ItemIndex].FindControl("txtgConsignment");

			DataRow dr = dtINVDetail.Rows.Find(dgConsignmenttxtConsingmentNo.Text);
			System.Reflection.FieldInfo fieldInfo = dr.GetType().GetField("rowID",System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance); 
			int rowID = (int)fieldInfo.GetValue(dr);
			dtINVDetail.Rows[rowID-1]["INS"]=(txtINS.Text!=""?Double.Parse(txtINS.Text):0.00);
			dtINVDetail.AcceptChanges();
			Session["SESSION_DS2"] = dtINVDetail;

			UpdateTotalCredit(dgConsignmenttxtConsingmentNo.Text,dgItem.ItemIndex);
			TextBox txtVAS = (TextBox)dgConsignment.Items[dgItem.ItemIndex].FindControl("txtVAS");
			Page.RegisterStartupScript("SetFocus", "<script>document.getElementById('" + txtVAS.ClientID + "').focus();document.getElementById('" + txtVAS.ClientID + "').select();</script>");
		}

		public void txtVAS_TextChanged(object sender, System.EventArgs e)
		{
			TextBox txtVAS = (TextBox)sender;
			DataGridItem dgItem = (DataGridItem)txtVAS.NamingContainer;
			
			if(VerifyConsignmentData(dgItem.ItemIndex,sender)==false)
				return;
			
			TextBox dgConsignmenttxtConsingmentNo = (TextBox)dgConsignment.Items[dgItem.ItemIndex].FindControl("txtgConsignment");

			DataRow dr = dtINVDetail.Rows.Find(dgConsignmenttxtConsingmentNo.Text);
			System.Reflection.FieldInfo fieldInfo = dr.GetType().GetField("rowID",System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance); 
			int rowID = (int)fieldInfo.GetValue(dr);
			dtINVDetail.Rows[rowID-1]["VAS"]=(txtVAS.Text!=""?Double.Parse(txtVAS.Text):0.00);
			dtINVDetail.AcceptChanges();
			Session["SESSION_DS2"] = dtINVDetail;

			UpdateTotalCredit(dgConsignmenttxtConsingmentNo.Text,dgItem.ItemIndex);
			TextBox txtESA = (TextBox)dgConsignment.Items[dgItem.ItemIndex].FindControl("txtESA");
			Page.RegisterStartupScript("SetFocus", "<script>document.getElementById('" + txtESA.ClientID + "').focus();document.getElementById('" + txtESA.ClientID + "').select();</script>");
		}

		public void txtESA_TextChanged(object sender, System.EventArgs e)
		{
			TextBox txtESA = (TextBox)sender;
			DataGridItem dgItem = (DataGridItem)txtESA.NamingContainer;
			
			if(VerifyConsignmentData(dgItem.ItemIndex,sender)==false)
				return;
			
			TextBox dgConsignmenttxtConsingmentNo = (TextBox)dgConsignment.Items[dgItem.ItemIndex].FindControl("txtgConsignment");

			DataRow dr = dtINVDetail.Rows.Find(dgConsignmenttxtConsingmentNo.Text);
			System.Reflection.FieldInfo fieldInfo = dr.GetType().GetField("rowID",System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance); 
			int rowID = (int)fieldInfo.GetValue(dr);
			dtINVDetail.Rows[rowID-1]["ESA"]=(txtESA.Text!=""?Double.Parse(txtESA.Text):0.00);
			dtINVDetail.AcceptChanges();
			Session["SESSION_DS2"] = dtINVDetail;

			UpdateTotalCredit(dgConsignmenttxtConsingmentNo.Text,dgItem.ItemIndex);
			TextBox txtOTH = (TextBox)dgConsignment.Items[dgItem.ItemIndex].FindControl("txtOTH");
			Page.RegisterStartupScript("SetFocus", "<script>document.getElementById('" + txtOTH.ClientID + "').focus();document.getElementById('" + txtOTH.ClientID + "').select();</script>");
		}

		public void txtOTH_TextChanged(object sender, System.EventArgs e)
		{
			TextBox txtOTH = (TextBox)sender;
			DataGridItem dgItem = (DataGridItem)txtOTH.NamingContainer;
			
			if(VerifyConsignmentData(dgItem.ItemIndex,sender)==false)
				return;
			
			TextBox dgConsignmenttxtConsingmentNo = (TextBox)dgConsignment.Items[dgItem.ItemIndex].FindControl("txtgConsignment");

			DataRow dr = dtINVDetail.Rows.Find(dgConsignmenttxtConsingmentNo.Text);
			System.Reflection.FieldInfo fieldInfo = dr.GetType().GetField("rowID",System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance); 
			int rowID = (int)fieldInfo.GetValue(dr);
			dtINVDetail.Rows[rowID-1]["OTH"]=(txtOTH.Text!=""?Double.Parse(txtOTH.Text):0.00);
			dtINVDetail.AcceptChanges();
			Session["SESSION_DS2"] = dtINVDetail;

			UpdateTotalCredit(dgConsignmenttxtConsingmentNo.Text,dgItem.ItemIndex);
			TextBox txtPDX = (TextBox)dgConsignment.Items[dgItem.ItemIndex].FindControl("txtPDX");
			Page.RegisterStartupScript("SetFocus", "<script>document.getElementById('" + txtPDX.ClientID + "').focus();document.getElementById('" + txtPDX.ClientID + "').select();</script>");
		}

		public void txtPDX_TextChanged(object sender, System.EventArgs e)
		{
			TextBox txtPDX = (TextBox)sender;
			DataGridItem dgItem = (DataGridItem)txtPDX.NamingContainer;
			
			if(VerifyConsignmentData(dgItem.ItemIndex,sender)==false)
				return;
			
			TextBox dgConsignmenttxtConsingmentNo = (TextBox)dgConsignment.Items[dgItem.ItemIndex].FindControl("txtgConsignment");

			DataRow dr = dtINVDetail.Rows.Find(dgConsignmenttxtConsingmentNo.Text);
			System.Reflection.FieldInfo fieldInfo = dr.GetType().GetField("rowID",System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance); 
			int rowID = (int)fieldInfo.GetValue(dr);
			dtINVDetail.Rows[rowID-1]["PDX"]=(txtPDX.Text!=""?Double.Parse(txtPDX.Text):0.00);
			dtINVDetail.AcceptChanges();
			Session["SESSION_DS2"] = dtINVDetail;

			UpdateTotalCredit(dgConsignmenttxtConsingmentNo.Text,dgItem.ItemIndex);
			TextBox txtADJ = (TextBox)dgConsignment.Items[dgItem.ItemIndex].FindControl("txtADJ");
			Page.RegisterStartupScript("SetFocus", "<script>document.getElementById('" + txtADJ.ClientID + "').focus();document.getElementById('" + txtADJ.ClientID + "').select();</script>");

		}

		public void txtADJ_TextChanged(object sender, System.EventArgs e)
		{
			TextBox txtADJ = (TextBox)sender;
			DataGridItem dgItem = (DataGridItem)txtADJ.NamingContainer;
			
			if(VerifyConsignmentData(dgItem.ItemIndex,sender)==false)
				return;
			
			TextBox dgConsignmenttxtConsingmentNo = (TextBox)dgConsignment.Items[dgItem.ItemIndex].FindControl("txtgConsignment");

			DataRow dr = dtINVDetail.Rows.Find(dgConsignmenttxtConsingmentNo.Text);
			System.Reflection.FieldInfo fieldInfo = dr.GetType().GetField("rowID",System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance); 
			int rowID = (int)fieldInfo.GetValue(dr);
			dtINVDetail.Rows[rowID-1]["ADJ"]=(txtADJ.Text!=""?Double.Parse(txtADJ.Text):0.00);
			dtINVDetail.AcceptChanges();
			Session["SESSION_DS2"] = dtINVDetail;

			UpdateTotalCredit(dgConsignmenttxtConsingmentNo.Text,dgItem.ItemIndex);
			TextBox txtTOT = (TextBox)dgConsignment.Items[dgItem.ItemIndex].FindControl("txtTOT");
			Page.RegisterStartupScript("SetFocus", "<script>document.getElementById('" + txtTOT.ClientID + "').focus();document.getElementById('" + txtTOT.ClientID + "').select();</script>");
		}

		public void txtTOT_TextChanged(object sender, System.EventArgs e)
		{
			TextBox txtTOT = (TextBox)sender;
			DataGridItem dgItem = (DataGridItem)txtTOT.NamingContainer;
			
			if(VerifyConsignmentData(dgItem.ItemIndex,sender)==false)
				return;
			
			TextBox dgConsignmenttxtConsingmentNo = (TextBox)dgConsignment.Items[dgItem.ItemIndex].FindControl("txtgConsignment");

			DataRow dr = dtINVDetail.Rows.Find(dgConsignmenttxtConsingmentNo.Text);
			System.Reflection.FieldInfo fieldInfo = dr.GetType().GetField("rowID",System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance); 
			int rowID = (int)fieldInfo.GetValue(dr);
			dtINVDetail.Rows[rowID-1]["TOT"]=(txtTOT.Text!=""?Double.Parse(txtTOT.Text):0.00);
			dtINVDetail.AcceptChanges();
			Session["SESSION_DS2"] = dtINVDetail;

			UpdateTotalCredit(dgConsignmenttxtConsingmentNo.Text,dgItem.ItemIndex);
			int idx = dgItem.ItemIndex>=dgConsignment.Items.Count ?dgItem.ItemIndex:dgItem.ItemIndex+1;
			TextBox txtFRG = (TextBox)dgConsignment.Items[idx].FindControl("txtFRG");
			Page.RegisterStartupScript("SetFocus", "<script>document.getElementById('" + txtFRG.ClientID + "').focus();document.getElementById('" + txtFRG.ClientID + "').select();</script>");
		}

		private void btnSelectAll_Click(object sender, System.EventArgs e)
		{
			try
			{
				if(btnSelectAll.Text == "Select All")
					CheckAllConsignments();
				else
					UnCheckAllConsignments();

				UpdateTotalSelected();
			}
			catch (Exception exc)
			{				
				Console.WriteLine(exc.Message);
			}
		}
		//====End Event Handler ====

		//==== Calculate Function ====

		public void UpdateTotalCredit(string strConsignmentNo, int iInputRow)
		{
			//in DataGrid
			TextBox txtTotalCredit = (TextBox)dgConsignment.Items[iInputRow].FindControl("txtgTotalCredit");

			TextBox txtConsignment = (TextBox)dgConsignment.Items[iInputRow].FindControl("txtgConsignment");
			TextBox txtTotal = (TextBox)dgConsignment.Items[iInputRow].FindControl("txtTotal");
			TextBox txtFRG = (TextBox)dgConsignment.Items[iInputRow].FindControl("txtFRG");
			TextBox txtINS = (TextBox)dgConsignment.Items[iInputRow].FindControl("txtINS");
			TextBox txtVAS = (TextBox)dgConsignment.Items[iInputRow].FindControl("txtVAS");
			TextBox txtESA = (TextBox)dgConsignment.Items[iInputRow].FindControl("txtESA");
			TextBox txtOTH = (TextBox)dgConsignment.Items[iInputRow].FindControl("txtOTH");
			TextBox txtPDX = (TextBox)dgConsignment.Items[iInputRow].FindControl("txtPDX");
			TextBox txtADJ = (TextBox)dgConsignment.Items[iInputRow].FindControl("txtADJ");
			TextBox txtTOT = (TextBox)dgConsignment.Items[iInputRow].FindControl("txtTOT");

			double dTotal = txtTotal.Text!="" ? Convert.ToDouble(txtTotal.Text) : 0;
			double dFRG = txtFRG.Text!="" ? Convert.ToDouble(txtFRG.Text) : 0;
			double dINS = txtINS.Text!="" ? Convert.ToDouble(txtINS.Text) : 0;
			double dVAS = txtVAS.Text!="" ? Convert.ToDouble(txtVAS.Text) : 0;
			double dESA = txtESA.Text!="" ? Convert.ToDouble(txtESA.Text) : 0;
			double dOTH = txtOTH.Text!="" ? Convert.ToDouble(txtOTH.Text) : 0;
			double dPDX = txtPDX.Text!="" ? Convert.ToDouble(txtPDX.Text) : 0;
			double dADJ = txtADJ.Text!="" ? Convert.ToDouble(txtADJ.Text) : 0;
			double dTOT = txtTOT.Text!="" ? Convert.ToDouble(txtTOT.Text) : 0;

			double dTotalCredit = dFRG+dINS+dVAS+dESA+dOTH+dPDX+dADJ+dTOT;
			txtTotalCredit.Text = String.Format((String)ViewState["m_format"], double.Parse(dTotalCredit.ToString()));
			DataRow dr = dtINVDetail.Rows.Find(strConsignmentNo);

			System.Reflection.FieldInfo fieldInfo = dr.GetType().GetField("rowID",System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance); 
			int rowID = (int)fieldInfo.GetValue(dr);
			dtINVDetail.Rows[rowID-1]["TotalCredit"]=dTotalCredit;
			dtINVDetail.AcceptChanges();
			Session["SESSION_DS2"] = dtINVDetail;

			//On TotalCredit Header
			object oFRG = dtINVDetail.Compute("Sum(FRG)", null);
			object oINS = dtINVDetail.Compute("Sum(INS)", null);
			object oVAS = dtINVDetail.Compute("Sum(VAS)", null);
			object oESA = dtINVDetail.Compute("Sum(ESA)", null);
			object oOTH = dtINVDetail.Compute("Sum(OTH)", null);
			object oPDX = dtINVDetail.Compute("Sum(PDX)", null);
			object oADJ = dtINVDetail.Compute("Sum(ADJ)", null);
			object oTOT = dtINVDetail.Compute("Sum(TOT)", null);
			
			if(oFRG!=null && oFRG!=DBNull.Value) this.txtTotalCreditFRG.Text = String.Format((String)ViewState["m_format"], double.Parse(oFRG.ToString()));
			if(oINS!=null && oINS!=DBNull.Value) this.txtTotalCreditINS.Text = String.Format((String)ViewState["m_format"], double.Parse(oINS.ToString()));
			if(oVAS!=null && oVAS!=DBNull.Value) this.txtTotalCreditVAS.Text = String.Format((String)ViewState["m_format"], double.Parse(oVAS.ToString()));
			if(oESA!=null && oESA!=DBNull.Value) this.txtTotalCreditESA.Text = String.Format((String)ViewState["m_format"], double.Parse(oESA.ToString()));
			if(oOTH!=null && oOTH!=DBNull.Value) this.txtTotalCreditOTH.Text = String.Format((String)ViewState["m_format"], double.Parse(oOTH.ToString()));
			if(oPDX!=null && oPDX!=DBNull.Value) this.txtTotalCreditPDX.Text = String.Format((String)ViewState["m_format"], double.Parse(oPDX.ToString()));
			if(oADJ!=null && oADJ!=DBNull.Value) this.txtTotalCreditADJ.Text = String.Format((String)ViewState["m_format"], double.Parse(oADJ.ToString()));
			if(oTOT!=null && oTOT!=DBNull.Value) this.txtTotalCreditTOT.Text = String.Format((String)ViewState["m_format"], double.Parse(oTOT.ToString()));
			
			this.txtTotalCredit.Text = String.Format((String)ViewState["m_format"], double.Parse(oFRG.ToString())+double.Parse(oINS.ToString())+double.Parse(oVAS.ToString())+double.Parse(oESA.ToString())+double.Parse(oOTH.ToString())+double.Parse(oPDX.ToString())+double.Parse(oADJ.ToString())+double.Parse(oTOT.ToString()));

		}
	

		public void UpdateTotalCredit()
		{
			//On TotalCredit Header
			object oFRG = dtINVDetail.Compute("Sum(FRG)", null);
			object oINS = dtINVDetail.Compute("Sum(INS)", null);
			object oVAS = dtINVDetail.Compute("Sum(VAS)", null);
			object oESA = dtINVDetail.Compute("Sum(ESA)", null);
			object oOTH = dtINVDetail.Compute("Sum(OTH)", null);
			object oPDX = dtINVDetail.Compute("Sum(PDX)", null);
			object oADJ = dtINVDetail.Compute("Sum(ADJ)", null);
			object oTOT = dtINVDetail.Compute("Sum(TOT)", null);
			
			if(oFRG!=null && oFRG!=DBNull.Value) this.txtTotalCreditFRG.Text = String.Format((String)ViewState["m_format"], double.Parse(oFRG.ToString()));
			if(oINS!=null && oINS!=DBNull.Value) this.txtTotalCreditINS.Text = String.Format((String)ViewState["m_format"], double.Parse(oINS.ToString()));
			if(oVAS!=null && oVAS!=DBNull.Value) this.txtTotalCreditVAS.Text = String.Format((String)ViewState["m_format"], double.Parse(oVAS.ToString()));
			if(oESA!=null && oESA!=DBNull.Value) this.txtTotalCreditESA.Text = String.Format((String)ViewState["m_format"], double.Parse(oESA.ToString()));
			if(oOTH!=null && oOTH!=DBNull.Value) this.txtTotalCreditOTH.Text = String.Format((String)ViewState["m_format"], double.Parse(oOTH.ToString()));
			if(oPDX!=null && oPDX!=DBNull.Value) this.txtTotalCreditPDX.Text = String.Format((String)ViewState["m_format"], double.Parse(oPDX.ToString()));
			if(oADJ!=null && oADJ!=DBNull.Value) this.txtTotalCreditADJ.Text = String.Format((String)ViewState["m_format"], double.Parse(oADJ.ToString()));
			if(oTOT!=null && oTOT!=DBNull.Value) this.txtTotalCreditTOT.Text = String.Format((String)ViewState["m_format"], double.Parse(oTOT.ToString()));
			
			this.txtTotalCredit.Text = String.Format((String)ViewState["m_format"], double.Parse(oFRG.ToString())+double.Parse(oINS.ToString())+double.Parse(oVAS.ToString())+double.Parse(oESA.ToString())+double.Parse(oOTH.ToString())+double.Parse(oPDX.ToString())+double.Parse(oADJ.ToString())+double.Parse(oTOT.ToString()));
		}
	


		public bool VerifyConsignmentData(int iInputRow,object sender)
		{
			this.lblErrorMsg.Text = "";
			TextBox txtConsignment = (TextBox)dgConsignment.Items[iInputRow].FindControl("txtgConsignment");
			TextBox txtTotal = (TextBox)dgConsignment.Items[iInputRow].FindControl("txtTotal");
			TextBox txtFRG = (TextBox)dgConsignment.Items[iInputRow].FindControl("txtFRG");
			TextBox txtINS = (TextBox)dgConsignment.Items[iInputRow].FindControl("txtINS");
			TextBox txtVAS = (TextBox)dgConsignment.Items[iInputRow].FindControl("txtVAS");
			TextBox txtESA = (TextBox)dgConsignment.Items[iInputRow].FindControl("txtESA");
			TextBox txtOTH = (TextBox)dgConsignment.Items[iInputRow].FindControl("txtOTH");
			TextBox txtPDX = (TextBox)dgConsignment.Items[iInputRow].FindControl("txtPDX");
			TextBox txtADJ = (TextBox)dgConsignment.Items[iInputRow].FindControl("txtADJ");
			TextBox txtTOT = (TextBox)dgConsignment.Items[iInputRow].FindControl("txtTOT");

			double dTotal = txtTotal.Text!="" ? Convert.ToDouble(txtTotal.Text) : 0;
			double dFRG = txtFRG.Text!="" ? Convert.ToDouble(txtFRG.Text) : 0;
			double dINS = txtINS.Text!="" ? Convert.ToDouble(txtINS.Text) : 0;
			double dVAS = txtVAS.Text!="" ? Convert.ToDouble(txtVAS.Text) : 0;
			double dESA = txtESA.Text!="" ? Convert.ToDouble(txtESA.Text) : 0;
			double dOTH = txtOTH.Text!="" ? Convert.ToDouble(txtOTH.Text) : 0;
			double dPDX = txtPDX.Text!="" ? Convert.ToDouble(txtPDX.Text) : 0;
			double dADJ = txtADJ.Text!="" ? Convert.ToDouble(txtADJ.Text) : 0;
			double dTOT = txtTOT.Text!="" ? Convert.ToDouble(txtTOT.Text) : 0;
			//either FRG-ADJ or TOT can input
			if(dTOT>0)
				if(dFRG>0||dINS>0||dVAS>0||dESA>0||dOTH>0||dPDX>0||dADJ>0)
				{
					this.lblErrorMsg.Text = "Please input either TOT or charges";
					TextBox txtSender = (TextBox)sender;
					txtSender.Text="0.00";
					return false;
				}
			//Cannot insert value<0
			TextBox txt = (TextBox)sender;
			if (double.Parse(txt.Text.ToString())<0)
			{
				
				lblErrorMsg.Text=Utility.GetLanguageText(ResourceType.UserMessage, "MSG_CDAMT_MORETHAN_ZERO", utility.GetUserCulture());
				return false;
			}
			//Exceed Consignment Total Checking

			if((dTotal<dTOT)||(dTotal<(dFRG+dINS+dVAS+dESA+dOTH+dPDX+dADJ)))
			{
				this.lblErrorMsg.Text = "Exceed total consignment value.";
				return false;
			}
			
			//And the value input is really existed in the consignment and not exceed
			DataRow dr = dtINVDetail.Rows.Find(txtConsignment.Text);
			System.Reflection.FieldInfo fieldInfo = dr.GetType().GetField("rowID",System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance); 
			int rowID = (int)fieldInfo.GetValue(dr);
 
			//FRG
 			if(Double.Parse(CreditDebitMgrDAL.GetAvaliableTotalCNAmount(appID,enterpriseID,(string)Session["Invoice_no"],txtConsignment.Text,"FRG").ToString()) < dFRG)
			{
					this.lblErrorMsg.Text = "Exceed Consignment's Invoiced value.";
					TextBox txtSender = (TextBox)sender;
					txtSender.Text= String.Format((String)ViewState["m_format"],0.00);
					return false;
			}

			//INS
 			if(Double.Parse(CreditDebitMgrDAL.GetAvaliableTotalCNAmount(appID,enterpriseID,(string)Session["Invoice_no"],txtConsignment.Text,"INS").ToString()) < dINS)
			{
				this.lblErrorMsg.Text = "Exceed Consignment's Invoiced value.";
					TextBox txtSender = (TextBox)sender;
					txtSender.Text= String.Format((String)ViewState["m_format"],0.00);
					return false;
			}
			//VAS
 			if(Double.Parse(CreditDebitMgrDAL.GetAvaliableTotalCNAmount(appID,enterpriseID,(string)Session["Invoice_no"],txtConsignment.Text,"VAS").ToString()) < dVAS)
			{
				this.lblErrorMsg.Text = "Exceed Consignment's Invoiced value.";
					TextBox txtSender = (TextBox)sender;
					txtSender.Text= String.Format((String)ViewState["m_format"],0.00);
					return false;
			}
			//ESA
 			if(Double.Parse(CreditDebitMgrDAL.GetAvaliableTotalCNAmount(appID,enterpriseID,(string)Session["Invoice_no"],txtConsignment.Text,"ESA").ToString()) < dESA)
			{
				this.lblErrorMsg.Text = "Exceed Consignment's Invoiced value.";
					TextBox txtSender = (TextBox)sender;
					txtSender.Text= String.Format((String)ViewState["m_format"],0.00);
					return false;
			}
			//OTH
 			if(Double.Parse(CreditDebitMgrDAL.GetAvaliableTotalCNAmount(appID,enterpriseID,(string)Session["Invoice_no"],txtConsignment.Text,"OTH").ToString()) < dOTH)
			{
				this.lblErrorMsg.Text = "Exceed Consignment's Invoiced value.";
					TextBox txtSender = (TextBox)sender;
					txtSender.Text= String.Format((String)ViewState["m_format"],0.00);
					return false;
			}
			//PDX
 			if(Double.Parse(CreditDebitMgrDAL.GetAvaliableTotalCNAmount(appID,enterpriseID,(string)Session["Invoice_no"],txtConsignment.Text,"PDX").ToString()) < dPDX)
			{
				this.lblErrorMsg.Text = "Exceed Consignment's Invoiced value.";
					TextBox txtSender = (TextBox)sender;
					txtSender.Text= String.Format((String)ViewState["m_format"],0.00);
					return false;
			}
			//ADJ
			Double daADJ = Double.Parse(CreditDebitMgrDAL.GetAvaliableTotalCNAmount(appID,enterpriseID,(string)Session["Invoice_no"],txtConsignment.Text,"ADJ").ToString());
			if(daADJ>=0)
			{
				if(daADJ < dADJ)
				{
					this.lblErrorMsg.Text = "Exceed Consignment's Invoiced value.";
					TextBox txtSender = (TextBox)sender;
					txtSender.Text= String.Format((String)ViewState["m_format"],0.00);
					return false;
				}
			}


			return true;
		}


		public void CheckAllConsignments()
		{
			int iUnCheckedRows = 0;
			object oUnCheckedCount = dtINVDetail.Compute("Count(Checked)", "Checked <> 1");
			if(oUnCheckedCount!=null && oUnCheckedCount!=DBNull.Value) iUnCheckedRows = Int32.Parse(oUnCheckedCount.ToString());

			if(iUnCheckedRows>0)
			{
				for(int i=0;i<dtINVDetail.Rows.Count;i++)
					dtINVDetail.Rows[i]["Checked"] = 1;
			}
			dtINVDetail.AcceptChanges();
			Session["SESSION_DS2"] = dtINVDetail;
			dgConsignment.DataSource = dtINVDetail;
			dgConsignment.DataBind();
			btnSelectAll.Text = "Deselect All";
		}

		public void UnCheckAllConsignments()
		{
			int iCheckedRows = 0;
			object oCheckedCount = dtINVDetail.Compute("Count(Checked)", "Checked <> 0");
			if(oCheckedCount!=null && oCheckedCount!=DBNull.Value) iCheckedRows = Int32.Parse(oCheckedCount.ToString());

			if(iCheckedRows>0)
			{
				for(int i=0;i<dtINVDetail.Rows.Count;i++)
					dtINVDetail.Rows[i]["Checked"] = 0;
			}
			dtINVDetail.AcceptChanges();
			Session["SESSION_DS2"] = dtINVDetail;
			dgConsignment.DataSource = dtINVDetail;
			dgConsignment.DataBind();
			btnSelectAll.Text = "Select All";
		}
		public void CheckConsignment(string strConsignmentNo,bool bCheckStatus)
		{
			DataRow dr = dtINVDetail.Rows.Find(strConsignmentNo);
			System.Reflection.FieldInfo fieldInfo = dr.GetType().GetField("rowID",System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance); 
			int rowID = (int)fieldInfo.GetValue(dr);
			dtINVDetail.Rows[rowID-1]["Checked"]=bCheckStatus;
			dtINVDetail.AcceptChanges();
			Session["SESSION_DS2"] = dtINVDetail;
		}


		public void UpdateTotalSelected()
		{
			object dFRG = dtINVDetail.Compute("Sum(tot_freight_charge)", "Checked = 1 and mbg_amount=0");
			object dINS = dtINVDetail.Compute("Sum(insurance_amt)", "Checked = 1");
			object dVAS = dtINVDetail.Compute("Sum(tot_vas_surcharge)", "Checked = 1");
			object dESA = dtINVDetail.Compute("Sum(esa_surcharge)", "Checked = 1 and mbg_amount=0");
			object dOTH = dtINVDetail.Compute("Sum(other_surcharge)", "Checked = 1");
			object dPDX = dtINVDetail.Compute("Sum(total_exception)", "Checked = 1");
			object dADJ = dtINVDetail.Compute("Sum(invoice_adj_amount)", "Checked = 1");
			object dTOT = dtINVDetail.Compute("Sum(invoice_amt)", "Checked = 1");
//			object dMBG = dtINVDetail.Compute("Sum(mbg_amount)", "Checked = 0");
//			Decimal TOT,MBG = 0;

			if(dFRG!=null && dFRG!=DBNull.Value) txtTotalSelectFRG.Text = String.Format((String)ViewState["m_format"], double.Parse(dFRG.ToString()));
			else  txtTotalSelectFRG.Text = String.Format((String)ViewState["m_format"], double.Parse("0"));
			if(dINS!=null && dINS!=DBNull.Value) txtTotalSelectINS.Text = String.Format((String)ViewState["m_format"], double.Parse(dINS.ToString()));
			else  txtTotalSelectINS.Text = String.Format((String)ViewState["m_format"], double.Parse("0"));
			if(dVAS!=null && dVAS!=DBNull.Value) txtTotalSelectVAS.Text = String.Format((String)ViewState["m_format"], double.Parse(dVAS.ToString()));
			else  txtTotalSelectVAS.Text = String.Format((String)ViewState["m_format"], double.Parse("0"));
			if(dESA!=null && dESA!=DBNull.Value) txtTotalSelectESA.Text = String.Format((String)ViewState["m_format"], double.Parse(dESA.ToString()));
			else  txtTotalSelectESA.Text = String.Format((String)ViewState["m_format"], double.Parse("0"));
			if(dOTH!=null && dOTH!=DBNull.Value) txtTotalSelectOTH.Text = String.Format((String)ViewState["m_format"], double.Parse(dOTH.ToString()));
			else  txtTotalSelectOTH.Text = String.Format((String)ViewState["m_format"], double.Parse("0"));
			if(dPDX!=null && dFRG!=DBNull.Value) txtTotalSelectPDX.Text = String.Format((String)ViewState["m_format"], double.Parse(dPDX.ToString()));
			else  txtTotalSelectPDX.Text = String.Format((String)ViewState["m_format"], double.Parse("0"));
			if(dADJ!=null && dFRG!=DBNull.Value) txtTotalSelectADJ.Text = String.Format((String)ViewState["m_format"], double.Parse(dADJ.ToString()));
			else  txtTotalSelectADJ.Text = String.Format((String)ViewState["m_format"], double.Parse("0"));
			if(dTOT!=null && dTOT!=DBNull.Value) txtTotalSelectTOT.Text = String.Format((String)ViewState["m_format"], double.Parse(dTOT.ToString()));
			else  txtTotalSelectTOT.Text = String.Format((String)ViewState["m_format"], double.Parse("0"));			/*if(dTOT!=null && dTOT!=DBNull.Value) TOT=Decimal.Parse(dTOT.ToString());
			else  TOT=0;
			if(dMBG!=null && dMBG!=DBNull.Value) MBG=Decimal.Parse(dMBG.ToString());
			else  MBG=0;
			
			txtTotalSelectTOT.Text = String.Format((String)ViewState["m_format"], (TOT));*/
		}


		//==== End Calculate Function ====
		public SessionDS ConvertInvConToCRCon(DataRow[] drCreditCons)
		{
			string strCreditNo = (string)Session["Credit_no"];
			string strInvoiceNo = (string)Session["Invoice_no"];
			SessionDS dsCreditDetail = CreditDebitMgrDAL.GetEmptyCreditDetailDS(0);
			DataTable dtCreditDetail = dsCreditDetail.ds.Tables[0];

			//Retrieve Charge Code Description & ReasonCode once from DB
			DataTable dtChargeCode = CreditDebitMgrDAL.getChargeCodeDetail(this.appID,this.enterpriseID,"","").Tables[0];
			DataColumn[] keys = new DataColumn[1];
			keys[0] = dtChargeCode.Columns["DbComboText"];
			dtChargeCode.PrimaryKey = keys;
					
			DataTable dtReasonCode = CreditDebitMgrDAL.GetReasonCodeFromChargeCode(this.appID,this.enterpriseID,-1,"").Tables[0];
			keys[0] = dtReasonCode.Columns["sequence"];
			dtReasonCode.PrimaryKey = keys;
			DataRow drChargeCode = dtChargeCode.NewRow();
			DataRow drReasonCode = dtReasonCode.NewRow();

			foreach(DataRow dr in drCreditCons)
			{

				//FRG
				if((dr["FRG"].ToString()!=""?Double.Parse(dr["FRG"].ToString()):0) > 0)
				{
					DataRow drCreditDetail = dtCreditDetail.NewRow();
					drCreditDetail["Credit_no"] = strCreditNo;
					drCreditDetail["Credit_no"] = strInvoiceNo;
					drCreditDetail["Consignment_no"] = dr["consignment_no"].ToString();

					drChargeCode = dtChargeCode.Rows.Find("FRG");

					drCreditDetail["charge_code"] = drChargeCode["DbComboText"].ToString();
					drCreditDetail["Invoice_Amt"] = dr["tot_freight_charge"].ToString();

					drReasonCode = dtReasonCode.Rows.Find(drChargeCode["DbComboValue"]);

					drCreditDetail["reason_code"] = drReasonCode["DbComboText"].ToString();
					drCreditDetail["Description"] = drReasonCode["DbComboValue"].ToString();
					drCreditDetail["Amt"] = dr["FRG"].ToString();
					dtCreditDetail.Rows.Add(drCreditDetail);
				}

				//INS
				if((dr["INS"].ToString()!=""?Double.Parse(dr["INS"].ToString()):0) > 0)
				{
					DataRow drCreditDetail = dtCreditDetail.NewRow();
					drCreditDetail["Credit_no"] = strCreditNo;
					drCreditDetail["Credit_no"] = strInvoiceNo;
					drCreditDetail["Consignment_no"] = dr["consignment_no"].ToString();

					drChargeCode = dtChargeCode.Rows.Find("INS");

					drCreditDetail["charge_code"] = drChargeCode["DbComboText"].ToString();
					drCreditDetail["Invoice_Amt"] = dr["insurance_amt"].ToString();

					drReasonCode = dtReasonCode.Rows.Find(drChargeCode["DbComboValue"]);

					drCreditDetail["reason_code"] = drReasonCode["DbComboText"].ToString();
					drCreditDetail["Description"] = drReasonCode["DbComboValue"].ToString();
					drCreditDetail["Amt"] = dr["INS"].ToString();
					dtCreditDetail.Rows.Add(drCreditDetail);
				}
				
				//VAS
				if((dr["VAS"].ToString()!=""?Double.Parse(dr["VAS"].ToString()):0) > 0)
				{
					DataRow drCreditDetail = dtCreditDetail.NewRow();
					drCreditDetail["Credit_no"] = strCreditNo;
					drCreditDetail["Credit_no"] = strInvoiceNo;
					drCreditDetail["Consignment_no"] = dr["consignment_no"].ToString();

					drChargeCode = dtChargeCode.Rows.Find("VAS");

					drCreditDetail["charge_code"] = drChargeCode["DbComboText"].ToString();
					drCreditDetail["Invoice_Amt"] = dr["tot_vas_surcharge"].ToString();

					drReasonCode = dtReasonCode.Rows.Find(drChargeCode["DbComboValue"]);

					drCreditDetail["reason_code"] = drReasonCode["DbComboText"].ToString();
					drCreditDetail["Description"] = drReasonCode["DbComboValue"].ToString();
					drCreditDetail["Amt"] = dr["VAS"].ToString();
					dtCreditDetail.Rows.Add(drCreditDetail);
				}

				//ESA
				if((dr["ESA"].ToString()!=""?Double.Parse(dr["ESA"].ToString()):0) > 0)
				{
					DataRow drCreditDetail = dtCreditDetail.NewRow();
					drCreditDetail["Credit_no"] = strCreditNo;
					drCreditDetail["Credit_no"] = strInvoiceNo;
					drCreditDetail["Consignment_no"] = dr["consignment_no"].ToString();

					drChargeCode = dtChargeCode.Rows.Find("ESA");

					drCreditDetail["charge_code"] = drChargeCode["DbComboText"].ToString();
					drCreditDetail["Invoice_Amt"] = dr["esa_surcharge"].ToString();

					drReasonCode = dtReasonCode.Rows.Find(drChargeCode["DbComboValue"]);

					drCreditDetail["reason_code"] = drReasonCode["DbComboText"].ToString();
					drCreditDetail["Description"] = drReasonCode["DbComboValue"].ToString();
					drCreditDetail["Amt"] = dr["ESA"].ToString();
					dtCreditDetail.Rows.Add(drCreditDetail);
				}

				//OTH
				if((dr["OTH"].ToString()!=""?Double.Parse(dr["OTH"].ToString()):0) > 0)
				{
					DataRow drCreditDetail = dtCreditDetail.NewRow();
					drCreditDetail["Credit_no"] = strCreditNo;
					drCreditDetail["Credit_no"] = strInvoiceNo;
					drCreditDetail["Consignment_no"] = dr["consignment_no"].ToString();

					drChargeCode = dtChargeCode.Rows.Find("OTH");

					drCreditDetail["charge_code"] = drChargeCode["DbComboText"].ToString();
					drCreditDetail["Invoice_Amt"] = dr["other_surcharge"].ToString();

					drReasonCode = dtReasonCode.Rows.Find(drChargeCode["DbComboValue"]);

					drCreditDetail["reason_code"] = drReasonCode["DbComboText"].ToString();
					drCreditDetail["Description"] = drReasonCode["DbComboValue"].ToString();
					drCreditDetail["Amt"] = dr["OTH"].ToString();
					dtCreditDetail.Rows.Add(drCreditDetail);
				}

				//PDX
				if((dr["PDX"].ToString()!=""?Double.Parse(dr["PDX"].ToString()):0) > 0)
				{
					DataRow drCreditDetail = dtCreditDetail.NewRow();
					drCreditDetail["Credit_no"] = strCreditNo;
					drCreditDetail["Credit_no"] = strInvoiceNo;
					drCreditDetail["Consignment_no"] = dr["consignment_no"].ToString();

					drChargeCode = dtChargeCode.Rows.Find("PDX");

					drCreditDetail["charge_code"] = drChargeCode["DbComboText"].ToString();
					drCreditDetail["Invoice_Amt"] = dr["total_exception"].ToString();

					drReasonCode = dtReasonCode.Rows.Find(drChargeCode["DbComboValue"]);

					drCreditDetail["reason_code"] = drReasonCode["DbComboText"].ToString();
					drCreditDetail["Description"] = drReasonCode["DbComboValue"].ToString();
					drCreditDetail["Amt"] = dr["PDX"].ToString();
					dtCreditDetail.Rows.Add(drCreditDetail);
				}

				//ADJ
				if((dr["ADJ"].ToString()!=""?Double.Parse(dr["ADJ"].ToString()):0) > 0)
				{
					DataRow drCreditDetail = dtCreditDetail.NewRow();
					drCreditDetail["Credit_no"] = strCreditNo;
					drCreditDetail["Credit_no"] = strInvoiceNo;
					drCreditDetail["Consignment_no"] = dr["consignment_no"].ToString();

					drChargeCode = dtChargeCode.Rows.Find("ADJ");

					drCreditDetail["charge_code"] = drChargeCode["DbComboText"].ToString();
					drCreditDetail["Invoice_Amt"] = dr["invoice_adj_amount"].ToString();

					drReasonCode = dtReasonCode.Rows.Find(drChargeCode["DbComboValue"]);

					drCreditDetail["reason_code"] = drReasonCode["DbComboText"].ToString();
					drCreditDetail["Description"] = drReasonCode["DbComboValue"].ToString();
					drCreditDetail["Amt"] = dr["ADJ"].ToString();
					dtCreditDetail.Rows.Add(drCreditDetail);
				}

				//TOT
				if((dr["TOT"].ToString()!=""?Double.Parse(dr["TOT"].ToString()):0) > 0)
				{
					DataRow drCreditDetail = dtCreditDetail.NewRow();
					drCreditDetail["Credit_no"] = strCreditNo;
					drCreditDetail["Credit_no"] = strInvoiceNo;
					drCreditDetail["Consignment_no"] = dr["consignment_no"].ToString();

					drChargeCode = dtChargeCode.Rows.Find("TOT");

					drCreditDetail["charge_code"] = drChargeCode["DbComboText"].ToString();
					drCreditDetail["Invoice_Amt"] = dr["invoice_amt"].ToString();

					drReasonCode = dtReasonCode.Rows.Find(drChargeCode["DbComboValue"]);

					drCreditDetail["reason_code"] = drReasonCode["DbComboText"].ToString();
					drCreditDetail["Description"] = drReasonCode["DbComboValue"].ToString();
					drCreditDetail["Amt"] = dr["TOT"].ToString();
					dtCreditDetail.Rows.Add(drCreditDetail);
				}

				//Add to dtCreditDetail


			}
			dtCreditDetail.AcceptChanges();
			dsCreditDetail.ds.Tables.Clear();
			dsCreditDetail.ds.Tables.Add(dtCreditDetail);
			dsCreditDetail.ds.AcceptChanges();
			return dsCreditDetail;
		}

		private void AdjustAllocateValue(DataRow[] drSelectedCons)
		{
			DataTable dtAdj = this.dtINVDetail.Clone();
			foreach(DataRow dr in drSelectedCons)
			{
				dtAdj.ImportRow(dr);
			}

					double dtmpAllocateVal=0 ;
					double dtmpConsignment=0 ;
					Enterprise enterprise = new Enterprise();
					enterprise.Populate(utility.GetAppID(),utility.GetEnterpriseID());
					int currencydec =0;
					int rowID =-1;
					string strConsNo;
					currencydec = enterprise.CurrencyDecimal ;

					//CASE FRG
			if (GetrowIDMaxValue(dtAdj,"tot_freight_charge")>-1)
			{
				if((txtTotalAllocateFRG.Text!=""?Double.Parse(txtTotalAllocateFRG.Text):0.00)>=0)
				dtmpConsignment =double.Parse(dtINVDetail.Compute("Sum(FRG)",null).ToString());
				dtmpAllocateVal= double.Parse((txtTotalAllocateFRG.Text!=""?Double.Parse(txtTotalAllocateFRG.Text.ToString()):0.00).ToString());
				if(dtmpAllocateVal!=dtmpConsignment)
				{
					rowID =GetrowIDMaxValue(dtAdj,"tot_freight_charge");
					strConsNo = dtINVDetail.Rows[rowID-1]["consignment_no"].ToString();
					dtmpConsignment =double.Parse(dtINVDetail.Compute("Sum(FRG)","consignment_no<>'" + strConsNo + "'").ToString());
					dtmpConsignment =this.ConvertSubString(dtmpConsignment,currencydec);
					dtINVDetail.Rows[rowID-1]["FRG"]= Math.Round((dtmpAllocateVal-dtmpConsignment),currencydec) ;
				}
			}
					//CASE INS
			if (GetrowIDMaxValue(dtAdj,"insurance_amt")>-1)
			{

				if((txtTotalAllocateINS.Text!=""?Double.Parse(txtTotalAllocateINS.Text):0.00)>=0)
				dtmpConsignment= double.Parse(dtINVDetail.Compute("Sum(INS)",null).ToString());
				dtmpAllocateVal= double.Parse((txtTotalAllocateINS.Text!=""?Double.Parse(txtTotalAllocateINS.Text.ToString()):0.00).ToString());
				if(dtmpAllocateVal!=dtmpConsignment)
				{
					rowID =GetrowIDMaxValue(dtAdj,"insurance_amt");
					strConsNo = dtINVDetail.Rows[rowID-1]["consignment_no"].ToString();
					dtmpConsignment =double.Parse(dtINVDetail.Compute("Sum(INS)","consignment_no<>'" + strConsNo + "'").ToString());
					dtmpConsignment =this.ConvertSubString(dtmpConsignment,currencydec);
					dtINVDetail.Rows[rowID-1]["INS"]= Math.Round((dtmpAllocateVal-dtmpConsignment),currencydec) ;
				}
			}	
					//CASE VAS
			if (GetrowIDMaxValue(dtAdj,"tot_vas_surcharge")>-1)
			{
				if((txtTotalAllocateVAS.Text!=""?Double.Parse(txtTotalAllocateVAS.Text):0.00)>=0)
				dtmpConsignment= double.Parse(dtINVDetail.Compute("Sum(VAS)",null).ToString());
				dtmpAllocateVal= double.Parse((txtTotalAllocateVAS.Text!=""?Double.Parse(txtTotalAllocateVAS.Text.ToString()):0.00).ToString());
				if(dtmpAllocateVal!=dtmpConsignment)
				{
					rowID =GetrowIDMaxValue(dtAdj,"tot_vas_surcharge");
					strConsNo = dtINVDetail.Rows[rowID-1]["consignment_no"].ToString();
					dtmpConsignment =double.Parse(dtINVDetail.Compute("Sum(VAS)","consignment_no<>'" + strConsNo + "'").ToString());
					dtmpConsignment =this.ConvertSubString(dtmpConsignment,currencydec);
					dtINVDetail.Rows[rowID-1]["VAS"]= Math.Round((dtmpAllocateVal-dtmpConsignment),currencydec) ;
				}					
			}
					//CASE ESA
			if (GetrowIDMaxValue(dtAdj,"esa_surcharge")>-1)
			{
				if((txtTotalAllocateESA.Text!=""?Double.Parse(txtTotalAllocateESA.Text):0.00)>=0)
				dtmpConsignment= double.Parse(dtINVDetail.Compute("Sum(ESA)",null).ToString());
				dtmpAllocateVal= double.Parse((txtTotalAllocateESA.Text!=""?Double.Parse(txtTotalAllocateESA.Text.ToString()):0.00).ToString());
				if(dtmpAllocateVal!=dtmpConsignment)
				{
					rowID =GetrowIDMaxValue(dtAdj,"esa_surcharge");
					strConsNo = dtINVDetail.Rows[rowID-1]["consignment_no"].ToString();
					dtmpConsignment =double.Parse(dtINVDetail.Compute("Sum(ESA)","consignment_no<>'" + strConsNo + "'").ToString());
					dtmpConsignment =this.ConvertSubString(dtmpConsignment,currencydec);
					dtINVDetail.Rows[rowID-1]["ESA"]= Math.Round((dtmpAllocateVal-dtmpConsignment),currencydec) ;
				}					
			}
					//CASE OTH
			if (GetrowIDMaxValue(dtAdj,"other_surcharge")>-1)
			{
				if((txtTotalAllocateOTH.Text!=""?Double.Parse(txtTotalAllocateOTH.Text):0.00)>=0)
				dtmpConsignment= double.Parse(dtINVDetail.Compute("Sum(OTH)",null).ToString());
				dtmpAllocateVal= double.Parse((txtTotalAllocateOTH.Text!=""?Double.Parse(txtTotalAllocateOTH.Text.ToString()):0.00).ToString());
				if(dtmpAllocateVal!=dtmpConsignment)
				{
					rowID =GetrowIDMaxValue(dtAdj,"other_surcharge");
					strConsNo = dtINVDetail.Rows[rowID-1]["consignment_no"].ToString();
					dtmpConsignment =double.Parse(dtINVDetail.Compute("Sum(OTH)","consignment_no<>'" + strConsNo + "'").ToString());
					dtmpConsignment =this.ConvertSubString(dtmpConsignment,currencydec);
					dtINVDetail.Rows[rowID-1]["OTH"]= Math.Round((dtmpAllocateVal-dtmpConsignment),currencydec) ;
				}					
			}
					//CASE PDX
			if (GetrowIDMaxValue(dtAdj,"total_exception")>-1)
			{
				if((txtTotalAllocatePDX.Text!=""?Double.Parse(txtTotalAllocatePDX.Text):0.00)>=0)
				dtmpConsignment= double.Parse(dtINVDetail.Compute("Sum(PDX)",null).ToString());
				dtmpAllocateVal= double.Parse((txtTotalAllocatePDX.Text!=""?Double.Parse(txtTotalAllocatePDX.Text.ToString()):0.00).ToString());
				if(dtmpAllocateVal!=dtmpConsignment)
				{
					rowID =GetrowIDMaxValue(dtAdj,"total_exception");
					strConsNo = dtINVDetail.Rows[rowID-1]["consignment_no"].ToString();
					dtmpConsignment =double.Parse(dtINVDetail.Compute("Sum(PDX)","consignment_no<>'" + strConsNo + "'").ToString());
					dtmpConsignment =this.ConvertSubString(dtmpConsignment,currencydec);
					dtINVDetail.Rows[rowID-1]["PDX"]= Math.Round((dtmpAllocateVal-dtmpConsignment),currencydec) ;
				}					
			}
					//CASE ADJ
			if (GetrowIDMaxValue(dtAdj,"invoice_adj_amount")>-1)
			{
				if((txtTotalAllocateADJ.Text!=""?Double.Parse(txtTotalAllocateADJ.Text):0.00)>=0)
				dtmpConsignment= double.Parse(dtINVDetail.Compute("Sum(ADJ)",null).ToString());
				dtmpAllocateVal= double.Parse((txtTotalAllocateADJ.Text!=""?Double.Parse(txtTotalAllocateADJ.Text.ToString()):0.00).ToString());
				if(dtmpAllocateVal!=dtmpConsignment)
				{
					rowID =GetrowIDMaxValue(dtAdj,"invoice_adj_amount");
					strConsNo = dtINVDetail.Rows[rowID-1]["consignment_no"].ToString();
					dtmpConsignment =double.Parse(dtINVDetail.Compute("Sum(ADJ)","consignment_no<>'" + strConsNo + "'").ToString());
					dtmpConsignment =this.ConvertSubString(dtmpConsignment,currencydec);
					dtINVDetail.Rows[rowID-1]["ADJ"]= Math.Round((dtmpAllocateVal-dtmpConsignment),currencydec) ;
				}					
			}
					//CASE TOT
			if (GetrowIDMaxValue(dtAdj,"invoice_amt")>-1)
			{
				if((txtTotalAllocateTOT.Text!=""?Double.Parse(txtTotalAllocateTOT.Text):0.00)>=0)
				dtmpConsignment= double.Parse(dtINVDetail.Compute("Sum(TOT)",null).ToString());
				dtmpAllocateVal= double.Parse((txtTotalAllocateTOT.Text!=""?Double.Parse(txtTotalAllocateTOT.Text.ToString()):0.00).ToString());
				if(dtmpAllocateVal!=dtmpConsignment)
				{
					rowID =GetrowIDMaxValue(dtAdj,"invoice_amt");
					strConsNo = dtINVDetail.Rows[rowID-1]["consignment_no"].ToString();
					dtmpConsignment =double.Parse(dtINVDetail.Compute("Sum(TOT)","consignment_no<>'" + strConsNo + "'").ToString());
					dtmpConsignment =this.ConvertSubString(dtmpConsignment,currencydec);
					dtINVDetail.Rows[rowID-1]["TOT"]= Math.Round((dtmpAllocateVal-dtmpConsignment),currencydec) ;
				}				
			}
			//Sum Each rows after adjust agian.
			foreach(DataRow dr in drSelectedCons)
			{
				System.Reflection.FieldInfo fieldInfo = dr.GetType().GetField("rowID",System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance); 
				rowID = (int)fieldInfo.GetValue(dr);

				double dTotalCredit = Double.Parse(dtINVDetail.Rows[rowID-1]["FRG"].ToString());
				dTotalCredit += Double.Parse(dtINVDetail.Rows[rowID-1]["INS"].ToString());
				dTotalCredit += Double.Parse(dtINVDetail.Rows[rowID-1]["VAS"].ToString());
				dTotalCredit += Double.Parse(dtINVDetail.Rows[rowID-1]["ESA"].ToString());
				dTotalCredit += Double.Parse(dtINVDetail.Rows[rowID-1]["OTH"].ToString());
				dTotalCredit += Double.Parse(dtINVDetail.Rows[rowID-1]["PDX"].ToString());
				dTotalCredit += Double.Parse(dtINVDetail.Rows[rowID-1]["ADJ"].ToString());
				dTotalCredit += Double.Parse(dtINVDetail.Rows[rowID-1]["TOT"].ToString());


				dtINVDetail.Rows[rowID-1]["TotalCredit"] = dTotalCredit;
			}
					

				
			
			
		}
		private double ConvertSubString(double val,int lenght)
		{
			double result =val;
			if ((val>0.0)&& (val.ToString().IndexOf(".")>0))
			{
				try
				{
					string tmpval =val.ToString();
					if ((lenght+1)<=tmpval.ToString().Length)
					{
						tmpval=string.Format("{0:F4}",val);
					}
					
					tmpval= tmpval.Substring(0,tmpval.IndexOf(".")+1+lenght);
					result= double.Parse (tmpval);
				}

				catch (Exception ex)
				{
						throw ex;
				}
			}
			return result;
		}
		private bool chkBeforeAllocate()
		{
			if(
				((this.txtTotalAllocateFRG.Text!="")||((this.txtTotalAllocateFRG.Text!="")?Double.Parse(txtTotalAllocateFRG.Text):0)>0) ||
				((this.txtTotalAllocateINS.Text!="")||((this.txtTotalAllocateINS.Text!="")?Double.Parse(txtTotalAllocateINS.Text):0)>0) ||
				((this.txtTotalAllocateVAS.Text!="")||((this.txtTotalAllocateVAS.Text!="")?Double.Parse(txtTotalAllocateVAS.Text):0)>0) || 
				((this.txtTotalAllocateESA.Text!="")||((this.txtTotalAllocateESA.Text!="")?Double.Parse(txtTotalAllocateESA.Text):0)>0) || 
				((this.txtTotalAllocateOTH.Text!="")||((this.txtTotalAllocateOTH.Text!="")?Double.Parse(txtTotalAllocateOTH.Text):0)>0) || 
				((this.txtTotalAllocatePDX.Text!="")||((this.txtTotalAllocatePDX.Text!="")?Double.Parse(txtTotalAllocatePDX.Text):0)>0) || 
				((this.txtTotalAllocateADJ.Text!="")||((this.txtTotalAllocateADJ.Text!="")?Double.Parse(txtTotalAllocateADJ.Text):0)>0)
				)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		private int GetrowIDMaxValue(DataTable dt,string strFiltercol)
		{
			int rowID =-1;
			DataRow[] drMaxinvoice_amt = dt.Select(strFiltercol + "= MAX("+ strFiltercol +")");
			string strConsNo ="";
			foreach (DataRow drMax in drMaxinvoice_amt)
			{
				strConsNo=(string)(drMax["consignment_no"]);
				break;
			}
			if(strConsNo!="")
			{
				DataRow[] drMaxCons = this.dtINVDetail.Select(" consignment_no='" + strConsNo + "'");
				foreach (DataRow dr in drMaxCons)
				{
					System.Reflection.FieldInfo fieldInfo = dr.GetType().GetField("rowID",System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance); 
					rowID = (int)fieldInfo.GetValue(dr);
					break;
				}
			}
			return rowID;
		}

		private void dgConsignment_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
		{
			//TextBox txtTotal = (TextBox)e.Item.FindControl("txtTotal");
			int item_index = e.Item.ItemIndex;
			int page = dgConsignment.CurrentPageIndex;
			int pagesize =dgConsignment.PageSize;
			int index;
			Decimal mbg_amount = 0;
			Decimal invoice_adj_amount=0;
			bool mbg = false;
			if(item_index>=0)
			{
				index = item_index+(page*pagesize);
				if(dtINVDetail.DefaultView.RowFilter=="")
				{
					if(Utility.IsNotDBNull(dtINVDetail.Rows[index]["mbg"]))
					{
						if(dtINVDetail.Rows[index]["mbg"].ToString().ToUpper()=="Y")
						{
							mbg = true;
						}
						else
						{
							mbg = false;
						}
					}
					else
					{
						mbg = false;
					}
					if(Utility.IsNotDBNull(dtINVDetail.Rows[index]["invoice_adj_amount"]))
					{
						invoice_adj_amount = Convert.ToDecimal(dtINVDetail.Rows[index]["invoice_adj_amount"]);
					}
					else
					{
						invoice_adj_amount=0;
					}
					if(Utility.IsNotDBNull(dtINVDetail.Rows[index]["mbg_amount"]))
					{
						mbg_amount = Convert.ToDecimal(dtINVDetail.Rows[index]["mbg_amount"]);
					}
					else
					{
						mbg_amount=0;
					}
				}
				else
				{
					String strFilter = dtINVDetail.DefaultView.RowFilter;
					DataRow[] drCONSelected=dtINVDetail.Select(strFilter);					
					if(drCONSelected.Length>0)
					{
						if(Utility.IsNotDBNull( drCONSelected[index]["mbg"]))
						{
							if(drCONSelected[index]["mbg"].ToString().ToUpper()=="Y")
							{
								mbg = true;
							}
							else
							{
								mbg = false;
							}
						}
						else
						{
							mbg = false;
						}
						if(Utility.IsNotDBNull(drCONSelected[index]["invoice_adj_amount"]))
						{
							invoice_adj_amount = Convert.ToDecimal(drCONSelected[index]["invoice_adj_amount"]);
						}
						else
						{
							invoice_adj_amount=0;
						}
						if(Utility.IsNotDBNull(drCONSelected[index]["mbg_amount"]))
						{
							mbg_amount = Convert.ToDecimal(drCONSelected[index]["mbg_amount"]);
						}
						else
						{
							mbg_amount=0;
						}
					}
				}
				
				if(mbg || invoice_adj_amount!=0 || mbg_amount!=0)
				{
					/*if(txtTotal.Text.Trim()!=""&&Convert.ToDecimal(txtTotal.Text.Trim())<=0)
					{*/
					TextBox txtgConsignment = (TextBox)e.Item.FindControl("txtgConsignment");
					TextBox txtFRG = (TextBox)e.Item.FindControl("txtFRG");
					TextBox txtINS = (TextBox)e.Item.FindControl("txtINS");
					TextBox txtVAS = (TextBox)e.Item.FindControl("txtVAS");
					TextBox txtESA = (TextBox)e.Item.FindControl("txtESA");
					TextBox txtOTH = (TextBox)e.Item.FindControl("txtOTH");
					TextBox txtPDX = (TextBox)e.Item.FindControl("txtPDX");
					TextBox txtADJ = (TextBox)e.Item.FindControl("txtADJ");
					TextBox txtTOT = (TextBox)e.Item.FindControl("txtTOT");
					TextBox txtTotal = (TextBox)e.Item.FindControl("txtTotal");
					TextBox txtgTotalCredit = (TextBox)e.Item.FindControl("txtgTotalCredit");
					txtTotal.ForeColor = System.Drawing.Color.Red;
					txtFRG.ForeColor = System.Drawing.Color.Red;
					txtINS.ForeColor = System.Drawing.Color.Red;
					txtVAS.ForeColor = System.Drawing.Color.Red;
					txtESA.ForeColor = System.Drawing.Color.Red;
					txtOTH.ForeColor = System.Drawing.Color.Red;
					txtPDX.ForeColor = System.Drawing.Color.Red;
					txtADJ.ForeColor = System.Drawing.Color.Red;
					txtTOT.ForeColor = System.Drawing.Color.Red;
					txtgConsignment.ForeColor = System.Drawing.Color.Red;
					txtgTotalCredit.ForeColor = System.Drawing.Color.Red;
					//}
				}
			}
			//if(e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
			//{
//				TextBox txtFRG = (TextBox)e.Item.FindControl("txtFRG");
//				TextBox txtINS = (TextBox)e.Item.FindControl("txtINS");
//				TextBox txtVAS = (TextBox)e.Item.FindControl("txtVAS");
//				TextBox txtESA = (TextBox)e.Item.FindControl("txtESA");
//				TextBox txtOTH = (TextBox)e.Item.FindControl("txtOTH");
//				TextBox txtPDX = (TextBox)e.Item.FindControl("txtPDX");
//				TextBox txtADJ = (TextBox)e.Item.FindControl("txtADJ");
//				TextBox txtTOT = (TextBox)e.Item.FindControl("txtTOT");
//
//				txtFRG.Attributes.Add("onblur","__doPostBack(this.id,'');document.getElementById('hidtxt').value='"+txtINS.ClientID+"');");
//				txtINS.Attributes.Add("onblur","__doPostBack(this.id,'');document.getElementById('hidtxt').value='"+txtVAS.ClientID+"');");
//				txtVAS.Attributes.Add("onblur","__doPostBack(this.id,'');document.getElementById('hidtxt').value='"+txtESA.ClientID+"');");
//				txtESA.Attributes.Add("onblur","__doPostBack(this.id,'');document.getElementById('hidtxt').value='"+txtOTH.ClientID+"');");
//				txtOTH.Attributes.Add("onblur","__doPostBack(this.id,'');document.getElementById('hidtxt').value='"+txtPDX.ClientID+"');");
//				txtPDX.Attributes.Add("onblur","__doPostBack(this.id,'');document.getElementById('hidtxt').value='"+txtADJ.ClientID+"');");
//				txtADJ.Attributes.Add("onblur","__doPostBack(this.id,'');document.getElementById('hidtxt').value='"+txtTOT.ClientID+"');");
//				txtTOT.Attributes.Add("onblur","__doPostBack(this.id,'');document.getElementById('hidtxt').value='"+txtFRG.ClientID+"');");

			//}
		}

	}
}