using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.common.classes;
using com.common.util;
using com.ties.DAL;
using com.ties.classes;
using com.common.applicationpages;

namespace TIES.WebUI
{
	/// <summary>
	/// Summary description for PackageDetails.
	/// </summary>
	public class PackageDetails : BasePopupPage
	{
		DataSet dsPkgDetails = null;
		DataSet dsPackageDtls = null;
		decimal decTotDimWt = 0;
		decimal decTotWt = 0;
		decimal  TOTVol = 0; //Jeab 21 Feb 2011
		decimal decTot_act_Wt = 0;//TU on 17June08
		int iTotPkgs = 0;
		int iTotPackage = 0;
		int iTotPackage2 = 0;
		decimal iTotHeight = 0;
		decimal iTotLength = 0;
		decimal iTotBreadth = 0;
		decimal iTotHeight2 = 0;
		decimal iTotLength2 = 0;
		decimal iTotBreadth2 = 0;
		decimal iTotActWt = 0;
		decimal iTotDimWt = 0;
		decimal iTotActWt2 = 0;
		decimal iTotDimWt2 = 0;
		decimal decChrgWt = 0;
		protected System.Web.UI.WebControls.Label lblErrorMsg;
		String strApplyDimWt = null;
		String strBtnActive = null;
		int iBookingNo = 0;
		String appID = null;
		String enterpriseID = null;
		string packages="";
		bool isExec = false;
		public string strCloseWindowStatus;
		protected System.Web.UI.WebControls.Panel panelPackageDetails;
		protected System.Web.UI.WebControls.Button btnInsert;
		protected System.Web.UI.WebControls.Button btnCancel;
		protected System.Web.UI.WebControls.Button btnOk;
		protected System.Web.UI.WebControls.DataGrid dgPkgInfo;
		protected System.Web.UI.WebControls.Button retriveSWBPackage;
		protected System.Web.UI.WebControls.Button btnRetrievePkgs;
		protected System.Web.UI.WebControls.Button btnBind;
		protected System.Web.UI.HtmlControls.HtmlTable Table1;
		protected System.Web.UI.WebControls.Button btnRetrievePRPkgs;
		protected System.Web.UI.WebControls.Panel panelPRPkgs;
		protected System.Web.UI.WebControls.Panel panelPRPkgsConfirm;
		protected System.Web.UI.WebControls.Button btnConfirmRetrievePRPkgsOK;
		protected System.Web.UI.WebControls.Button btnConfirmRetrievePRPkgsCancel;
		protected System.Web.UI.WebControls.Label lblBookDate;
		protected System.Web.UI.WebControls.Label Label1;
		protected System.Web.UI.WebControls.Label Label2;
		protected System.Web.UI.WebControls.Label Label3;
		protected System.Web.UI.WebControls.Label Label4;
		protected System.Web.UI.WebControls.Label Label5;
		protected System.Web.UI.WebControls.CheckBox chkCopyServiceType;
		protected System.Web.UI.WebControls.CheckBox chkCopyCODAmount;
		protected System.Web.UI.WebControls.CheckBox chkCopyDeclaredValue;
		protected com.common.util.msTextBox txtConsServiceType;
		protected com.common.util.msTextBox txtPRServiceType;
		protected System.Web.UI.WebControls.TextBox txtConsCODAmount;
		protected System.Web.UI.WebControls.TextBox txtPRCODAmount;
		protected System.Web.UI.WebControls.TextBox txtConsDeclaredValue;
		protected System.Web.UI.WebControls.TextBox txtPRDeclaredValue;
		protected System.Web.UI.WebControls.DataGrid dgRetrievePRPkg;
		protected System.Web.UI.WebControls.Button Button1;
		protected System.Web.UI.WebControls.Button Button2;
		protected System.Web.UI.WebControls.Label Label6;
		protected System.Web.UI.WebControls.Label Label7;
		protected com.common.util.msTextBox txtServiceType;
		protected System.Web.UI.WebControls.TextBox txtPostalCode;
		protected System.Web.UI.WebControls.Button btnRetrievePRPkgOK;
		protected System.Web.UI.WebControls.Button btnRetrievePRPkgCancel;
		protected System.Web.UI.WebControls.Button btnRetrievePRPkgShowAll;

		String strConsignment = null;
		String strTotPackage = "0";
		String strTotActWt = "0";
		String strTotDimWt = "0";
		String strBookingNo="";

		String strShipDclrVal = "0";
		String strServiceCode = "";
		String strServiceDesc = "";
		String strTotVASSurch = "0";
		String strCODAmount = "0.00";
		String strRecipZip = "";

		String strSelectedSeqNo = "";
		String strSelectedServiceCode ="";
		String strSelectedCODAmount ="";
		String strSelectedDeclaredValue ="";

		DataTable dtPRConsignment;
		DataTable dtPRConsignmentPkgs;
		protected System.Web.UI.WebControls.Label lblMainTitle;
		DataTable dtPRConsignmentVAS;
		DataSet dsShipmentPKG = new DataSet();
		DataTable dtShipmentPKG = null;

		string status = "";

		private bool OnlyShow
		{
			get
			{
				if(ViewState["OnlyShow"]==null)
					ViewState["OnlyShow"] = false;
				return (bool)ViewState["OnlyShow"];
			}
			set{ViewState["OnlyShow"]=value;}
		}

		private bool PartialCon
		{
			get
			{
				if(ViewState["PartialCon"]==null)
					ViewState["PartialCon"] = false;
				return (bool)ViewState["PartialCon"];
			}
			set{ViewState["PartialCon"]=value;}
		}

		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here			
			//utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings,Page.Session);
			appID = utility.GetAppID();
			enterpriseID = utility.GetEnterpriseID();
			String custid = null;
				
			string strFlgAutoRetrieve = "";
			if(Request.Params["flgAutoRetrieve"] != null)
				strFlgAutoRetrieve = Request.Params["flgAutoRetrieve"].ToString();
			
			strApplyDimWt = Request.Params["ApplyDimWt"].ToString();
			strBtnActive = Request.Params["BTNACTIVE"].ToString();
			strBookingNo = Request.Params["BOOKINGNO"].ToString();
			strConsignment = Request.Params["ConsignmentNumber"].ToString();
			if(Request.Params["TotPackage"].ToString() != "")
				strTotPackage = Request.Params["TotPackage"].ToString();
			if(Request.Params["ActWeight"].ToString() != "")
				strTotActWt = Request.Params["ActWeight"].ToString();

			if(Request.Params["CODAmount"].ToString() != "")
				strCODAmount = Request.Params["CODAmount"].ToString();
			if(Request.Params["ShipDclrVal"].ToString() != "")
				strShipDclrVal = Request.Params["ShipDclrVal"].ToString();
			if(Request.Params["ServiceCode"].ToString() != "")
				strServiceCode = Request.Params["ServiceCode"].ToString();
			if(Request.Params["ServiceDesc"].ToString() != "")
				strServiceDesc = Request.Params["ServiceDesc"].ToString();
			if(Request.Params["TotVASSurch"].ToString() != "")
				strTotVASSurch = Request.Params["TotVASSurch"].ToString();
			if(Request.Params["RecipZip"].ToString() != "")
				strRecipZip = Request.Params["RecipZip"].ToString();

	
			if(Request.Params["DimWeight"].ToString() != "")
				strTotDimWt = Request.Params["DimWeight"].ToString();
			if(Request.Params["CUSTID"].ToString() != "")
				custid = Request.Params["CUSTID"].ToString();
			
			try
			{
				if(Request.Params["OnlyShow"] !=null)
					OnlyShow = Convert.ToBoolean(Request.Params["OnlyShow"]);

				if(Request.Params["status"] !=null)
					status = Request.Params["status"].ToString();

				if(Request.Params["PartialCon"]!=null)
				{
					PartialCon = Convert.ToBoolean(Request.Params["PartialCon"]);
					Request.Params["PartialCon"] = null;
				}
				
			}
			catch{}

			if(strBookingNo.Length > 0)
			{
				if(Convert.ToInt32(strBookingNo) > 0 )
					iBookingNo = Convert.ToInt32(strBookingNo);
			}

			if((strBtnActive != null)&&(strBtnActive.Equals("No")))
			{
				btnRetrievePkgs.Enabled = false;
				btnBind.Enabled = false;
			}
			else if((strBtnActive != null)&&(strBtnActive.Equals("Yes")))
			{
				btnRetrievePkgs.Enabled = true;
				btnBind.Enabled = true;
			}

			if(!Page.IsPostBack)
			{
				if(Session["SESSION_DS_PKG"] != null)
				{
					dsPackageDtls = (DataSet)Session["SESSION_DS_PKG"];
				}
				else
				{
					dsPackageDtls = DomesticShipmentMgrDAL.GetEmptyPkgDtlDS();
				}

				if(strTotVASSurch =="0")
					this.btnRetrievePRPkgs.Enabled=true;
				else
					this.btnRetrievePRPkgs.Enabled=false;

				DataTable dtPRConsignmentCheck = PRBMgrDAL.ReadPickupConsignment(this.appID,this.enterpriseID,this.strBookingNo,"");

				if(dtPRConsignmentCheck!=null)
				{
					if(dtPRConsignmentCheck.Rows.Count<=0)
						this.btnRetrievePRPkgs.Enabled=false;
				}
				else
					this.btnRetrievePRPkgs.Enabled=false;

				if(this.strConsignment.Length>0 && this.strBookingNo.Length>0)
				{
					if(Request.Params["FormBtn"]!=null && Request.Params["FormBtn"].ToString()=="ViewOldPD" )
					{
						lblMainTitle.Visible=true;
						dsShipmentPKG = DomesticShipmentMgrDAL.GetPakageDtls_Original(this.appID,this.enterpriseID,int.Parse(this.strBookingNo),this.strConsignment);
						string ReplacedDate = DomesticShipmentMgrDAL.GetShipment_ReplaceDate(this.appID,this.enterpriseID,int.Parse(this.strBookingNo),this.strConsignment);
						if(ReplacedDate!="")
						{
							lblMainTitle.Text = "Old Package Detail : Replaced on " + ReplacedDate; 
						}
						retriveSWBPackage.Visible=false;
						btnRetrievePRPkgs.Visible=false;
						btnCancel.Visible=false;
						btnInsert.Visible=false;
					}
					else
					{
						if(Session["SESSION_DS_PKG"] != null)
							dsShipmentPKG = ((DataSet)Session["SESSION_DS_PKG"]);//dsShipmentPKG = DomesticShipmentMgrDAL.GetPakageDtls(this.appID,this.enterpriseID,int.Parse(this.strBookingNo),this.strConsignment);
					}
					//DataTable dtShipmentPKG = null;
					if(dsShipmentPKG!=null)
						dtShipmentPKG = dsShipmentPKG.Tables[0];

					if(dtShipmentPKG!=null)
					{
						if(dtShipmentPKG.Rows.Count<=0)
							this.btnRetrievePRPkgs.Enabled=true;
						else
							this.btnRetrievePRPkgs.Enabled=false;
					}
				}

				dsPkgDetails = (DataSet)Session["SESSION_DS_PKG"];//(DataSet)Session["SESSION_DS2"];
				//Modified by GwanG on 11Feb08
				ViewState["VIEWSTATE_DStemp"] = dsPkgDetails;
				//Modified by GwanG on 12Feb08
				ViewState["VIEWSTATE_DS2"] = dsPkgDetails;

				ShowOnly();
				
				BindPkgGrid();

				if((strBtnActive != null)&&(strBtnActive.Equals("Yes")) && (dsPkgDetails.Tables[0].Rows.Count > 0))
				{
					dgPkgInfo.Enabled = false;
				}
				else
				{
					dgPkgInfo.Enabled = true;
				}
				
				DataSet profileDS = SysDataManager1.GetEnterpriseProfile(appID, enterpriseID);

				if(profileDS.Tables[0].Rows.Count > 0)
				{
					if((profileDS.Tables[0].Rows[0]["wt_rounding_method"]!= null) && (!profileDS.Tables[0].Rows[0]["wt_rounding_method"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						ViewState["wt_rounding_method"] = Convert.ToInt32(profileDS.Tables[0].Rows[0]["wt_rounding_method"]);
					else
						ViewState["wt_rounding_method"] = 0;

					if((profileDS.Tables[0].Rows[0]["wt_increment_amt"]!= null) && (!profileDS.Tables[0].Rows[0]["wt_increment_amt"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						ViewState["wt_increment_amt"] = Convert.ToDecimal(profileDS.Tables[0].Rows[0]["wt_increment_amt"]);
					else
						ViewState["wt_increment_amt"] = 1;
				}

//				if(dgPkgInfo.Items.Count == 0)
//					retriveSWBPackage_Click(null, null);
			}
			else
			{
				if(ViewState["VIEWSTATE_DS2"]  ==null)
				{
					ViewState["VIEWSTATE_DS2"] = (DataSet)Session["SESSION_DS_PKG"];
				}				
				dsPkgDetails = (DataSet)ViewState["VIEWSTATE_DS2"];
				dtPRConsignment = (DataTable)ViewState["dtPRConsignment"];
				dtPRConsignmentPkgs = (DataTable)ViewState["dtPRConsignmentPkgs"];
				dtPRConsignmentVAS = (DataTable)ViewState["dtPRConsignmentVAS"];
			}

			if(Page.IsPostBack == false)
			{
				if(dsPkgDetails != null)
				{
					int cnt = dsPkgDetails.Tables[0].Rows.Count;
					int i = 0;
					for(i = 0;i<cnt;i++)
					{
						DataRow drEach = dsPkgDetails.Tables[0].Rows[i];
						iTotPackage += (int)drEach["pkg_qty"];
						//Modified by Tom 10/6/09
						iTotHeight += Convert.ToDecimal(drEach["pkg_height"]);
						iTotLength += Convert.ToDecimal(drEach["pkg_length"]);
						iTotBreadth += Convert.ToDecimal(drEach["pkg_breadth"]);
						//End modified by Tom 10/6/09
						iTotActWt += (decimal)drEach["tot_act_wt"];
						if(drEach["tot_dim_wt"] != System.DBNull.Value)
						{
							iTotDimWt += (decimal)drEach["tot_dim_wt"];
						}
					}
				}

				if(dsPackageDtls != null)
				{
					int cntPkg = dsPackageDtls.Tables[0].Rows.Count;
					int k = 0;
					for(k = 0;k<cntPkg;k++)
					{
						DataRow drRows = dsPackageDtls.Tables[0].Rows[k];
						iTotPackage2 += (int)drRows["pkg_qty"];
						//Modified by Tom 10/6/09
						iTotHeight2 += Convert.ToDecimal(drRows["pkg_height"]);
						iTotLength2 += Convert.ToDecimal(drRows["pkg_length"]);
						iTotBreadth2 += Convert.ToDecimal(drRows["pkg_breadth"]);
						//End modified by Tom 10/6/09
						iTotActWt2 += (decimal)drRows["tot_act_wt"];
						iTotDimWt2 += (decimal)drRows["tot_dim_wt"];
					}
				}
				if(iTotPackage2 == 0)
				{
					retriveSWBPackage.Enabled = false;
				}
				else
				{
					if(iTotActWt != decimal.Parse(strTotActWt) || iTotDimWt != decimal.Parse(strTotDimWt)
						|| iTotPackage != iTotPackage2 || iTotActWt != iTotActWt2 || iTotDimWt != iTotDimWt2
						|| iTotHeight != iTotHeight2 || iTotLength != iTotLength2 || iTotBreadth != iTotBreadth2)
					{
						retriveSWBPackage.Enabled = true;
					}	
				}
			}

			
		}
		
		private void ShowOnly()
		{
			if(OnlyShow)
			{
				dgPkgInfo.Columns[0].Visible = false;
				dgPkgInfo.Columns[1].Visible = false;
				btnInsert.Enabled = false;
			}
		}
		private void BindPkgGrid()
		{
			if(Request.Params["FormBtn"]!=null && Request.Params["FormBtn"].ToString()=="ViewOldPD" )
			{
				dgPkgInfo.DataSource = dtShipmentPKG;
				dgPkgInfo.DataBind();

				dgPkgInfo.Columns[0].Visible=false;
				dgPkgInfo.Columns[1].Visible=false;
			}
			else
			{
				dgPkgInfo.DataSource = dsPkgDetails;
				dgPkgInfo.DataBind();
				ViewState["VIEWSTATE_DS2"] = dsPkgDetails;
			}
		}

		public void dgPkgInfo_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			Label lblMPSNo = (Label)dgPkgInfo.SelectedItem.FindControl("lblMPSNo");
			String strMPSNo = lblMPSNo.Text;
		}

		public void dgPkgInfo_Update(object sender, DataGridCommandEventArgs e)
		{
			lblErrorMsg.Text = "";
			decimal iLength = 0;
			decimal iBreadth = 0;
			decimal iHeight = 0;
			decimal iWeight = 0;
			int iQty = 0;

			try
			{
				msTextBox txtMPSNo = (msTextBox)e.Item.FindControl("txtMPSNo");
				int k = (e.Item.ItemIndex + ((dgPkgInfo.CurrentPageIndex) * dgPkgInfo.PageSize));
				if(txtMPSNo != null)
				{
					int cnt = dsPkgDetails.Tables[0].Rows.Count;
					for(int i=0; i < cnt; i++)
					{
						DataRow drTmp = dsPkgDetails.Tables[0].Rows[i];
						if(i != k && cnt != 1)
							if(drTmp["mps_code"].ToString().Trim() == txtMPSNo.Text.Trim())
							{
								lblErrorMsg.Text = "Duplicated MPS No.";
								return;
							}
					}					
				}

				msTextBox txtLength = (msTextBox)e.Item.FindControl("txtLength");
				if(txtLength != null && txtLength.Text != "")
				{
					iLength =  Convert.ToDecimal((txtLength.Text.ToString()));
					if(iLength == 0)
					{
						lblErrorMsg.Text = "Please enter the length";
						return;
					}
				}					
				else
				{
					lblErrorMsg.Text = "Please enter the length";
					return;
				}

				msTextBox txtBreadth = (msTextBox)e.Item.FindControl("txtBreadth");
				if(txtBreadth != null && txtBreadth.Text != "")
				{					
					iBreadth = Convert.ToDecimal(txtBreadth.Text.ToString());
					if(iBreadth == 0)
					{
						lblErrorMsg.Text = "Please enter the breadth";
						return;
					}
				}					
				else
				{
					lblErrorMsg.Text = "Please enter the breadth";
					return;
				}

				msTextBox txtHeight = (msTextBox)e.Item.FindControl("txtHeight");
				if(txtHeight != null && txtHeight.Text != "")
				{
					iHeight = Convert.ToDecimal(txtHeight.Text.ToString());
					if(iHeight == 0)
					{
						lblErrorMsg.Text = "Please enter the height";
						return;
					}
				}					
				else
				{
					lblErrorMsg.Text = "Please enter the height";
					return;
				}
			
				Label lblVolume = (Label)e.Item.FindControl("lblVolume");
				if((iLength != 0 ) && (iBreadth != 0) && (iHeight != 0))
				{
					lblVolume.Text = String.Format("{0:#,##0}",(Math.Round((iLength * iBreadth * iHeight),2)));
				}

				msTextBox txtQty = (msTextBox)e.Item.FindControl("txtQty");
				if(txtQty != null && txtQty.Text != "")
				{					
					iQty = Convert.ToInt16(txtQty.Text.ToString());
					if(iQty == 0)
					{
						lblErrorMsg.Text = "Please enter the Quantity";
						return;
					}
				}					
				else
				{
					lblErrorMsg.Text = "Please enter the Quantity";
					return;
				}

				msTextBox txtWeight = (msTextBox)e.Item.FindControl("txtWeight");
				if(txtWeight != null && txtWeight.Text != "")
				{					
					iWeight = Convert.ToDecimal(txtWeight.Text.ToString());
					if(iWeight == 0)
					{
						lblErrorMsg.Text = "Please enter the weight";
						return;
					}
				}					
				else
				{
					lblErrorMsg.Text = "Please enter the weight";
					return;
				}

				string custid="";
				if(Request.Params["CUSTID"].ToString() != "")
					custid = Request.Params["CUSTID"].ToString();
				DataSet dsCalc = DomesticShipmentMgrDAL.CalcPackageDetailsWeights(appID , enterpriseID,
																					custid,txtLength.Text.Trim(),
																					txtBreadth.Text.Trim(), txtHeight.Text.Trim(),
																					txtWeight.Text.Trim(),txtQty.Text.Trim());
				string error_code="";
				if(dsCalc.Tables.Count>0 && dsCalc.Tables[0].Rows.Count>0)
				{
					DataRow dr= dsCalc.Tables[0].Rows[0];
					error_code = dr["error_code"].ToString();
					if(error_code =="0")
					{
						dsPkgDetails.Tables[0].Rows[e.Item.ItemIndex]["tot_wt"] = dr["tot_wt"].ToString();
						dsPkgDetails.Tables[0].Rows[e.Item.ItemIndex]["tot_dim_wt"] = dr["tot_dim_wt"].ToString();
						dsPkgDetails.Tables[0].Rows[e.Item.ItemIndex]["chargeable_wt"] = dr["chargeable_wt"].ToString();
						dsPkgDetails.Tables[0].Rows[e.Item.ItemIndex]["tot_act_wt"] = dr["tot_act_wt"].ToString();

						DataRow drCurrent = dsPkgDetails.Tables[0].Rows[k];
						drCurrent["mps_code"] = txtMPSNo.Text.Trim();
						drCurrent["pkg_length"] = txtLength.Text;
						drCurrent["pkg_breadth"] = txtBreadth.Text;
						drCurrent["pkg_height"] = txtHeight.Text;

						drCurrent["pkg_volume"] = lblVolume.Text;
						drCurrent["pkg_qty"] = txtQty.Text;
						drCurrent["pkg_wt"] = txtWeight.Text;
					}
				}
				string str_error="";
				lblErrorMsg.Text="";
				if(error_code =="0")
				{
					dgPkgInfo.EditItemIndex = -1;
					BindPkgGrid();
					Logger.LogDebugInfo("PackageDetails","dgPkgInfo_Update","Info001","updating data grid... : "+txtMPSNo);
					btnOk.Enabled=true;
					btnCancel.Enabled=true;
					btnInsert.Enabled=true;
				}					
				if(error_code =="1")
				{
					str_error = "Volume of package exceeds the Enterprise Limit.";
				}
				if(error_code=="2")
				{
					str_error = "Individual Package Weight exceeds the Enterprise limit.";
				}
				if(error_code=="4")
				{
					str_error = "Total number of packages exceeds the Enterprise Limit.";
				}
				if(error_code=="8")
				{
					str_error = "Package Weight x Quantity exceeds the limit that can be saved.";
				}

				if(error_code=="3")
				{
					str_error += "Volume of package exceeds the Enterprise Limit.<br/>";
					str_error += "Individual Package Weight exceeds the Enterprise limit.";
				}


				if(error_code=="6")
				{
					str_error += "Individual Package Weight exceeds the Enterprise limit.<br/>";
					str_error += "Total number of packages exceeds the Enterprise Limit.";
				}
				if(error_code=="7")
				{
					str_error += "Volume of package exceeds the Enterprise Limit.<br/>";
					str_error += "Individual Package Weight exceeds the Enterprise limit.<br/>";
					str_error += "Total number of packages exceeds the Enterprise Limit.";
				}
				if(error_code=="9")
				{
					str_error += "Volume of package exceeds the Enterprise Limit.<br/>";
					str_error += "Package Weight x Quantity exceeds the limit that can be saved.";
				}
				if(error_code=="10")
				{
					str_error += "Individual Package Weight exceeds the Enterprise limit.<br/>";
					str_error += "Package Weight x Quantity exceeds the limit that can be saved.";
				}
				if(error_code=="11")
				{
					str_error += "Volume of package exceeds the Enterprise Limit.<br/>";
					str_error += "Individual Package Weight exceeds the Enterprise limit.<br/>";
					str_error += "Package Weight x Quantity exceeds the limit that can be saved.";
				}
				if(error_code=="12")
				{
					str_error += "Total number of packages exceeds the Enterprise Limit.<br/>";
					str_error += "Package Weight x Quantity exceeds the limit that can be saved.";
				}
				if(error_code=="13")
				{
					str_error += "Volume of package exceeds the Enterprise Limit.<br/>";
					str_error += "Total number of packages exceeds the Enterprise Limit.<br/>";
					str_error += "Package Weight x Quantity exceeds the limit that can be saved.";
				}
				if(error_code=="14")
				{
					str_error += "Individual Package Weight exceeds the Enterprise limit.<br/>";
					str_error += "Total number of packages exceeds the Enterprise Limit.<br/>";
					str_error += "Package Weight x Quantity exceeds the limit that can be saved.";
				}
				if(error_code=="15")
				{
					str_error += "Volume of package exceeds the Enterprise Limit.<br/>";
					str_error += "Individual Package Weight exceeds the Enterprise limit.<br/>";
					str_error += "Total number of packages exceeds the Enterprise Limit.<br/>";
					str_error += "Package Weight x Quantity exceeds the limit that can be saved.";
				}
				lblErrorMsg.Text=str_error;
			}
			catch(System.Data.ConstraintException constrntExp)
			{
				String msg = constrntExp.ToString();
				lblErrorMsg.Text = "The code already exists";
				return;
			}

		}

		protected void dgPkgInfo_Edit(object sender, DataGridCommandEventArgs e)
		{
			dgPkgInfo.EditItemIndex = e.Item.ItemIndex;
			BindPkgGrid();
			btnOk.Enabled=false;
			btnCancel.Enabled=false;
			btnInsert.Enabled=false;
			ViewState["InsertdgVAS"] = false;
		}

		protected void dgPkgInfo_Cancel(object sender, DataGridCommandEventArgs e)
		{
			lblErrorMsg.Text = "";
			dgPkgInfo.EditItemIndex = -1;

			if((bool)ViewState["InsertdgVAS"] == true)
			{
				DataRow drCurrent = dsPkgDetails.Tables[0].Rows[e.Item.ItemIndex];
				drCurrent.Delete();
			}

			ViewState["InsertdgVAS"] = false;
			BindPkgGrid();
			btnOk.Enabled=true;
			btnCancel.Enabled=true;
			btnInsert.Enabled=true;
		}

		protected void dgPkgInfo_Delete(object sender, DataGridCommandEventArgs e)
		{
			lblErrorMsg.Text = "";
			//			DataRow drCurrent = dsPkgDetails.Tables[0].Rows[e.Item.ItemIndex];
			//			drCurrent.Delete();
			dsPkgDetails.Tables[0].Rows.RemoveAt(e.Item.ItemIndex);

			dgPkgInfo.EditItemIndex = -1;


			//drCurrent.AcceptChanges();
			BindPkgGrid();
			btnOk.Enabled=true;
			btnCancel.Enabled=true;
			btnInsert.Enabled=true;
		}

		protected void dgPkgInfo_PageChange(object sender, DataGridPageChangedEventArgs e)
		{
			dgPkgInfo.CurrentPageIndex = e.NewPageIndex;
			BindPkgGrid();
			Logger.LogDebugInfo("PkgDetailsPopup","dgPkgInfo_PageChange","Info005","On Page Change "+e.NewPageIndex);
		}
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			base.OnInit(e);
			InitializeComponent();
			
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.retriveSWBPackage.Click += new System.EventHandler(this.retriveSWBPackage_Click);
			this.btnRetrievePRPkgs.Click += new System.EventHandler(this.btnRetrievePRPkgs_Click);
			this.dgPkgInfo.SelectedIndexChanged += new System.EventHandler(this.dgPkgInfo_SelectedIndexChanged);
			this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
			this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
			this.btnInsert.Click += new System.EventHandler(this.btnInsert_Click);
			this.btnRetrievePRPkgOK.Click += new System.EventHandler(this.btnRetrievePRPkgOK_Click);
			this.btnRetrievePRPkgCancel.Click += new System.EventHandler(this.btnRetrievePRPkgCancel_Click);
			this.btnRetrievePRPkgShowAll.Click += new System.EventHandler(this.btnRetrievePRPkgShowAll_Click);
			this.dgRetrievePRPkg.PageIndexChanged += new System.Web.UI.WebControls.DataGridPageChangedEventHandler(this.dgRetrievePRPkg_PageIndexChanged);
			this.chkCopyServiceType.CheckedChanged += new System.EventHandler(this.chkCopyServiceType_CheckedChanged);
			this.chkCopyCODAmount.CheckedChanged += new System.EventHandler(this.chkCopyCODAmount_CheckedChanged);
			this.chkCopyDeclaredValue.CheckedChanged += new System.EventHandler(this.chkCopyDeclaredValue_CheckedChanged);
			this.btnConfirmRetrievePRPkgsOK.Click += new System.EventHandler(this.btnConfirmRetrievePRPkgsOK_Click);
			this.btnConfirmRetrievePRPkgsCancel.Click += new System.EventHandler(this.btnConfirmRetrievePRPkgsCancel_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
		private void btnCancel_Click(object sender, System.EventArgs e)
		{
			if(dgPkgInfo.EditItemIndex != -1)
			{
				//				DataRow drCurrent = dsPkgDetails.Tables[0].Rows[dgPkgInfo.EditItemIndex];
				//				drCurrent.Delete();
				dsPkgDetails.Tables[0].Rows.RemoveAt(dgPkgInfo.EditItemIndex);


				dgPkgInfo.EditItemIndex = -1;


			}

			//Modified by GwanG on 11Feb08
			if(ViewState["VIEWSTATE_DStemp"] != null)
			
			{			
				dsPkgDetails = (DataSet)ViewState["VIEWSTATE_DStemp"];
				ViewState.Remove("VIEWSTATE_DStemp");
				//Session["SESSION_DS2"] = dsPkgDetails;
			}

			/* Comment by GwanG on 12June08
			//Get the total of the columns of grid
			GetGridValuesTotal();

			String sScript = "";
			sScript += "<script language=javascript>";
			sScript += "  window.opener.DomesticShipment.txtPkgActualWt.value = "+decTotWt+";" ;
			sScript += "  window.opener.DomesticShipment.txtPkgDimWt.value ="+ decTotDimWt+";" ;
			sScript += "  window.opener.DomesticShipment.txtPkgTotpkgs.value ="+ iTotPkgs+";" ;
			sScript += "  window.opener.DomesticShipment.txtPkgChargeWt.value ="+ decChrgWt+";" ;
		//	sScript += "  window.close();";
			sScript += "</script>";
			Response.Write(sScript);*/
			strCloseWindowStatus="C";

		}

		private void btnInsert_Click(object sender, System.EventArgs e)
		{
			lblErrorMsg.Text = "";
			AddRowInPkgGrid();	
			dgPkgInfo.EditItemIndex = dsPkgDetails.Tables[0].Rows.Count - 1;
			
			Logger.LogDebugInfo("Packagedetails","btnInsert_Click","inf0002","Data Grid Items count"+dgPkgInfo.Items.Count);
			BindPkgGrid();
			btnOk.Enabled=false;
			btnCancel.Enabled=false;
			btnInsert.Enabled=false;
			ViewState["InsertdgVAS"] = true;
		
		}
		private void AddRowInPkgGrid()
		{
			lblErrorMsg.Text = "";
			try
			{
				//Modified by GwanG on 12Feb08
				//dsPkgDetails = (DataSet)Session["SESSION_DS2"];//DomesticShipmentMgrDAL.GetEmptyPkgDtlDS();
				dsPkgDetails = (DataSet)ViewState["VIEWSTATE_DS2"];
				DomesticShipmentMgrDAL.AddNewRowInPkgDS(dsPkgDetails);
			}
			catch(System.Data.ConstraintException appConstraintExp)
			{
				String msg = appConstraintExp.ToString();
				lblErrorMsg.Text = "Please enter the Value.";
				return;
			}
			//Modified by GwanG on 12Feb08
			//Session["SESSION_DS2"] = dsPkgDetails;
			ViewState["VIEWSTATE_DS2"]= dsPkgDetails;
			
		}
		
		private void GetGridValuesTotal()
		{
			string Dim_By_tot = Request.Params["DimByTOT"].ToString();
			if(dsPkgDetails != null)
			{
				int cnt = dsPkgDetails.Tables[0].Rows.Count;
				int i = 0;


				//Jeab 28 Dec 10
				if (Dim_By_tot =="Y")
				{
					decTotDimWt = 0;
					decTotWt = 0;
					for(i = 0;i<cnt;i++)
					{
						DataRow drEach = dsPkgDetails.Tables[0].Rows[i];
						decTotDimWt += (decimal)drEach["tot_dim_wt"];
						decTotWt += (decimal)drEach["tot_wt"];
						if (drEach["tot_act_wt"]== System.DBNull.Value )
						{
							drEach["tot_act_wt"]= 0	;
						}
						decTot_act_Wt += (decimal)drEach["tot_act_wt"];
						iTotPkgs += (int)drEach["pkg_qty"];
					
						//					if (drEach["pkg_TOTvolume"] != null)
						//					{
						//						TOTVol += (decimal)drEach["pkg_TOTvolume"];
						//					}
						//					else
						//					{
						TOTVol += Convert.ToDecimal(drEach["pkg_length"])*Convert.ToDecimal(drEach["pkg_breadth"])*Convert.ToDecimal(drEach["pkg_height"])*Convert.ToDecimal(drEach["pkg_qty"]); //Jeab 22 Feb 2011
						//					}
					}

					if(decTotDimWt < decTotWt)  // �� decTotWt  ᷹ decTot_act_Wt (�������Ѻ������͹���)  Jeab 28 Feb 2011
					{
						decChrgWt = decTotWt;  // �� decTotWt  ᷹ decTot_act_Wt  (�������Ѻ������͹���)    Jeab 28 Feb 2011
						for(i = 0;i<cnt;i++)
						{
							DataRow drEach = dsPkgDetails.Tables[0].Rows[i];
							drEach["chargeable_wt"] = drEach["tot_wt"];
						}
					}
					else
					{
						decChrgWt = decTotDimWt;
						for(i = 0;i<cnt;i++)
						{
							DataRow drEach = dsPkgDetails.Tables[0].Rows[i];
							drEach["chargeable_wt"] = drEach["tot_dim_wt"];
						}
					}
				}
				else
				{
					packages="";
					for(i = 0;i<cnt;i++)
					{
						string package = "";
						DataRow drEach = dsPkgDetails.Tables[0].Rows[i];
						decTotDimWt += (decimal)drEach["tot_dim_wt"];
						decTotWt += (decimal)drEach["tot_wt"];
						//TU on 17June08
						if (drEach["tot_act_wt"]== System.DBNull.Value )
						{
							drEach["tot_act_wt"]= 0	;
						}
						decTot_act_Wt += (decimal)drEach["tot_act_wt"];
						TOTVol += Convert.ToDecimal(drEach["pkg_length"])*Convert.ToDecimal(drEach["pkg_breadth"])*Convert.ToDecimal(drEach["pkg_height"])*Convert.ToDecimal(drEach["pkg_qty"]); //Jeab 22 Feb 2011

						iTotPkgs += (int)drEach["pkg_qty"];
						//decChrgWt += (decimal)drEach["chargeable_wt"];
						if(Request.Params["ApplyDimWt"].ToString().Trim() == "Y" || Request.Params["ApplyDimWt"].ToString().Trim() == "")
						{
							if((decimal)drEach["tot_wt"] < (decimal)drEach["tot_dim_wt"])
							{
								decChrgWt +=  (decimal)drEach["tot_dim_wt"];
								drEach["chargeable_wt"] = drEach["tot_dim_wt"];
							}
							else
							{
								decChrgWt += (decimal)drEach["tot_wt"];
								drEach["chargeable_wt"] = drEach["tot_wt"];
							}
						}
						else if(Request.Params["ApplyDimWt"].ToString() == "N")
						{
							decChrgWt += (decimal)drEach["tot_wt"];
							drEach["chargeable_wt"] = drEach["tot_wt"];
						}

						package =drEach["mps_code"].ToString() + "," + drEach["pkg_length"].ToString();
						package+="," + drEach["pkg_breadth"].ToString() + "," + drEach["pkg_height"].ToString();
						package+="," + drEach["pkg_wt"].ToString() + "," + drEach["pkg_qty"].ToString();
						package+="," + drEach["tot_wt"].ToString() + "," + drEach["tot_dim_wt"].ToString();
						package+="," + drEach["chargeable_wt"].ToString() + "," + drEach["tot_act_wt"].ToString();

						if(packages.Length>0)
						{
							packages+=";";
						}
						packages+=package;
					}

				}
				//Jeab 28 Dec 10 =========> End
			}

		}

		private void btnOk_Click(object sender, System.EventArgs e)
		{
			if(Request.Params["FormBtn"]!=null && Request.Params["FormBtn"].ToString()=="ViewOldPD" )
			{
				String CloseScript = "";
				CloseScript += "<script language=javascript>";
				CloseScript += "  Window.close();";
				//sScript += " window.opener.DomesticShipment.btnBind.click();";
				CloseScript += "</script>";
				Response.Write(CloseScript);
				strCloseWindowStatus="C";
			}
			else
			{
				if(dsPkgDetails == null)
				{
					String CloseScript = "";
					CloseScript += "<script language=javascript>";
					CloseScript += "  Window.close();";
					//sScript += " window.opener.DomesticShipment.btnBind.click();";
					CloseScript += "</script>";
					Response.Write(CloseScript);
					strCloseWindowStatus="C";
					return;
				}
				if(dgPkgInfo.EditItemIndex != -1)
				{
					dsPkgDetails.Tables[0].Rows.RemoveAt(dgPkgInfo.EditItemIndex);
					dgPkgInfo.EditItemIndex = -1;
				}
				//Calculate the total of Dimensional Weight & total weight
				GetGridValuesTotal();
				if(dsPkgDetails.Tables[0].Rows.Count >0)
				{
					DataSet dsPackges = DomesticShipmentMgrDAL.CalcPackageDetailsSummary(appID , enterpriseID,0,packages);							
					//decimal pkg_volume = TOTVol;
					if(dsPackges.Tables[0].Rows.Count>0)
					{
						DataRow dr = dsPackges.Tables[0].Rows[0];
						if(dr["error_code"].ToString() =="0")
						{
							iTotPkgs = (int)dr["tot_pkg"];
							decTotWt = (decimal)dr["tot_wt"];
							decTotDimWt = (decimal)dr["tot_dim_wt"];
							decChrgWt = (decimal)dr["chargeable_wt"];
							decTot_act_Wt = (decimal)dr["tot_act_wt"];
							TOTVol = (decimal)dr["volume"];
						}
						else
						{
							lblErrorMsg.Text="Total Consignment Weight exceeds the Enterprise Limit.";
							return;
						}
					}
				}

				//Put the Package Dataset in the Session
				Session["SESSION_DS2"] = dsPkgDetails;
				Session["SESSION_DS_PKG"] = dsPkgDetails;

				Session["dtPRConsignmentVAS"] = dtPRConsignmentVAS;
	
				decimal decESADlvrySurchrg = 0;
				int i = 0;
				String strRecipient = "";
				//String strServiceCode = this.strServiceCode;//"";
				//String strServiceDesc = this.strServiceDesc;//"";
				String strRecipState = "";
				String strRecipZip = this.strRecipZip;
				String strPayerCountry = "";
				String strDlvryRoute = "";
				decimal decDeclrValue = decimal.Parse((this.strShipDclrVal==""?"0":this.strShipDclrVal));//0;
				decimal decPercntDVadd = 0;
				DateTime dtEstDlvryTime = DateTime.Now;
				decimal decTotFrghtChrg = 0;
				decimal decTotVASSurchrg = 0;
				decimal decInsSurcharge = 0;
				decimal decMaxCovrge = 0;
				decimal decCODAmount = decimal.Parse((this.strCODAmount==""?"0":this.strCODAmount));
			
				Enterprise enterprise = new Enterprise();
				//change by aoo session_ds6
				if (Session["SESSION_DS6"] != null)
				{
					isExec = true;
					//change by aoo session_ds6
					DataSet dsShpmntData = (DataSet)Session["SESSION_DS6"];
					DataRow drEach = dsShpmntData.Tables[0].Rows[0];
				
					if((drEach["serial_no"]!= null) && (!drEach["serial_no"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						i = (int)drEach["serial_no"] ;
					}
				
					if((drEach["recipient_zipcode"]!= null) && (!drEach["recipient_zipcode"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						strRecipient = (String)drEach["recipient_zipcode"] ;
						//Get the State & Country
					
						enterprise.Populate(appID,enterpriseID);
						strPayerCountry = enterprise.Country;
						Zipcode zipCode = new Zipcode();
						zipCode.Populate(appID,enterpriseID,strPayerCountry,strRecipient);
						strRecipState = zipCode.StateName;
						strDlvryRoute = zipCode.DeliveryRoute;
					}
				
				
					if((drEach["service_code"]!= null) && (!drEach["service_code"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						strServiceCode = (String)drEach["service_code"] ;
						Service service = new Service();
						service.Populate(appID,enterpriseID,strServiceCode);
						strServiceDesc = service.ServiceDescription;
					}
				
				
					if((drEach["declare_value"]!= null) && (!drEach["declare_value"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						decDeclrValue = (decimal)drEach["declare_value"];
					}
				
				
					if((drEach["percent_dv_additional"]!= null) && (!drEach["percent_dv_additional"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						decPercntDVadd = (decimal)drEach["percent_dv_additional"] ;
					}
				
				
					if((drEach["est_delivery_datetime"]!= null) && (!drEach["est_delivery_datetime"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						dtEstDlvryTime = (DateTime)drEach["est_delivery_datetime"] ;
					}
				
				
					if((drEach["tot_freight_charge"]!= null) && (!drEach["tot_freight_charge"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						decTotFrghtChrg = (decimal)drEach["tot_freight_charge"] ;
					}
				
				
					if((drEach["tot_vas_surcharge"]!= null) && (!drEach["tot_vas_surcharge"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						decTotVASSurchrg = (decimal)drEach["tot_vas_surcharge"];
					}
				
				
					if((drEach["insurance_surcharge"]!= null) && (!drEach["insurance_surcharge"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						decInsSurcharge = (decimal)drEach["insurance_surcharge"] ;
					}
				
				
					if((drEach["esa_delivery_surcharge"]!= null) && (!drEach["esa_delivery_surcharge"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						decESADlvrySurchrg = (decimal)drEach["esa_delivery_surcharge"] ;
					}
				}

				if(this.txtConsServiceType.Text.Trim().Length>0)
					if(this.strServiceCode.Trim()!=this.txtConsServiceType.Text.Trim())
					{
						strServiceCode = this.txtConsServiceType.Text.Trim();
						Service service = new Service();
						service.Populate(appID,enterpriseID,strServiceCode);
						strServiceDesc = service.ServiceDescription;
					}

				if(this.txtConsDeclaredValue.Text.Trim().Length>0)
					if(Convert.ToDecimal(decDeclrValue)!=Convert.ToDecimal(this.txtConsDeclaredValue.Text.Trim()))
					{
						decDeclrValue = Convert.ToDecimal(this.txtConsDeclaredValue.Text.Trim());
					}

				if(this.txtConsCODAmount.Text.Trim().Length>0)
					if(Convert.ToDecimal(strCODAmount)!=Convert.ToDecimal(this.txtConsCODAmount.Text.Trim()))
					{
						decCODAmount = Convert.ToDecimal(this.txtConsCODAmount.Text.Trim());
					}
					else
						decCODAmount = Convert.ToDecimal(strCODAmount);


				decimal decTotalAmt = decTotFrghtChrg+decInsSurcharge+decTotVASSurchrg+decESADlvrySurchrg+decESADlvrySurchrg;
		
				enterprise.Populate(appID,enterpriseID);
				decMaxCovrge = enterprise.MaxInsuranceAmt;

				String sScript = "";
				sScript += "<script language=javascript>";
				sScript += "  window.opener.DomesticShipment.txtPkgActualWt.value = '"+decTotWt.ToString("#,##0")+"';" ;
				sScript += "  window.opener.DomesticShipment.txtPkgDimWt.value ='"+ decTotDimWt.ToString("#,##0")+"';" ;
				sScript += "  window.opener.DomesticShipment.txtPkgTotpkgs.value ='"+ iTotPkgs.ToString("#,##0")+"';" ;
				sScript += "  window.opener.DomesticShipment.txtPkgChargeWt.value ='"+ decChrgWt.ToString("#,##0")+"';" ;
				sScript += "  window.opener.DomesticShipment.txttotVol.value ='"+ TOTVol.ToString("#,##0")+"';" ; //Jeab 21 Feb 2011
				sScript += "  window.opener.DomesticShipment.txtActualWeight.value ='"+ decTot_act_Wt.ToString("#,##0.00")+"';" ;//TU on 17June08

				if(decTotFrghtChrg > 0)
				{
					sScript += "  window.opener.DomesticfShipment.txtFreightChrg.value ="+ decTotFrghtChrg+";" ;
				}

				if(decInsSurcharge > 0)
				{
					sScript += "  window.opener.DomesticShipment.txtInsChrg.value ="+ decInsSurcharge+";" ;
				}
				if(decTotVASSurchrg > 0)
				{
					sScript += "  window.opener.DomesticShipment.txtTotVASSurChrg.value ="+ decTotVASSurchrg+";" ;
				}
				if(decESADlvrySurchrg > 0)
				{
					sScript += "  window.opener.DomesticShipment.txtESASurchrg.value ="+ decESADlvrySurchrg+";" ;
				}
				if(decTotalAmt > 0)
				{
					sScript += "  window.opener.DomesticShipment.txtShpTotAmt.value ="+ decTotalAmt+";" ;
				}
				//  Comment By Aoo  21/03/2008
				//			if(decMaxCovrge > 0)
				//			{
				//				sScript += "  window.opener.DomesticShipment.txtShpMaxCvrg.value ="+ decMaxCovrge+";" ;
				//			}
				if(decInsSurcharge > 0)
				{
					sScript += "  window.opener.DomesticShipment.txtShpInsSurchrg.value ="+ decInsSurcharge+";" ;
				}
				if(isExec == true)
				{
				
					sScript += "  window.opener.DomesticShipment.txtDestination.value = '" + strRecipState + "';";
					sScript += "  window.opener.DomesticShipment.txtRouteCode.value = '" + strDlvryRoute + "';";
					sScript += "  window.opener.DomesticShipment.txtRecipCity.value = '" + strRecipState + "';";
					sScript += "  window.opener.DomesticShipment.txtRecipState.value = '" + strPayerCountry + "';";
					sScript += "  window.opener.DomesticShipment.txtRecipZip.value = '" + strRecipient + "';";
					sScript += "  window.opener.DomesticShipment.txtShpSvcCode.value = '" + strServiceCode + "';";
					//sScript += "  window.opener.DomesticShipment.txtShpSvcDesc.value = '" + strServiceDesc + "';";
					sScript += "  window.opener.DomesticShipment.txtShpDclrValue.value ="+ decDeclrValue+";" ;
					sScript += "  window.opener.DomesticShipment.txtCODAmount.value ="+ decCODAmount+";" ;
					sScript += "  window.opener.DomesticShipment.txtShpAddDV.value ="+ decPercntDVadd+";" ;
					sScript += "  window.opener.DomesticShipment.txtShipEstDlvryDt.value = '" + dtEstDlvryTime.ToString("dd/MM/yyyy HH:mm") + "';";
				}
				else
				{
					sScript += "  window.opener.DomesticShipment.txtShpSvcCode.value = '" + strServiceCode + "';";
					//sScript += "  window.opener.DomesticShipment.txtShpSvcDesc.value = '" + strServiceDesc + "';";
					sScript += "  window.opener.DomesticShipment.txtShpDclrValue.value ="+ decDeclrValue+";" ;
					sScript += "  window.opener.DomesticShipment.txtCODAmount.value ="+ decCODAmount+";" ;
				}
				//sScript += "  Window.close();";
				//sScript += " window.opener.DomesticShipment.btnBind.click();";
				sScript += "</script>";
				Response.Write(sScript);
				strCloseWindowStatus="C";
			}
		}

		private void btnRetrievePkgs_Click(object sender, System.EventArgs e)
		{
			Session["SESSION_DS5"] = null;
			String sUrl = "RetrievePickUpPkgsPopup.aspx?BOOKINGNO="+iBookingNo;
			//String sFeatures = "'height=320;width=160;left=100;top=250;location=no;menubar=no;resizable=yes;scrollbars=no;status=no;titlebar=yes;toolbar=no'";
			String sFeatures = "'height=320,width=650,left=100,top=50,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,titlebar=yes,toolbar=no'";
			String sScript ="";
			sScript += "<script language=javascript>";
			sScript += "window.open('" + sUrl + "',''," + sFeatures + ");";
			sScript += "</script>";
			Response.Write(sScript);
		}

		private void btnBind_Click(object sender, System.EventArgs e)
		{
			BindPkgGrid();
			if((strBtnActive != null)&&(strBtnActive.Equals("Yes")))
			{
				dgPkgInfo.Enabled = false;
			}

		}

		private decimal EnterpriseRounding(decimal beforeRound)
		{
			int wt_rounding_method = 0;
			decimal wt_increment_amt = 1;
			decimal reVal = beforeRound;

			if (ViewState["wt_rounding_method"] != null)
				wt_rounding_method = (int)ViewState["wt_rounding_method"];

			if (ViewState["wt_increment_amt"] != null)
				wt_increment_amt = (decimal)ViewState["wt_increment_amt"];
		
			decimal BeforDot = 0;
			decimal AfterDot = 0;
			decimal tmp05 = Convert.ToDecimal(0.5);
			string strBeforeRound = beforeRound.ToString();
			int dotIndex = Convert.ToInt32(strBeforeRound.IndexOf(".", 0));

			if(dotIndex < 0) 
			{
				BeforDot = Convert.ToDecimal(strBeforeRound);
				AfterDot = Convert.ToDecimal("0.00");
			}
			else 
			{ 
				BeforDot = Convert.ToDecimal(strBeforeRound.Substring(0, dotIndex));
				AfterDot = Convert.ToDecimal("0." + strBeforeRound.Substring(dotIndex + 1, strBeforeRound.Length - (dotIndex + 1)));
			}

			if(wt_increment_amt == tmp05)
			{
				if(AfterDot == tmp05) 
				{
					reVal = beforeRound;
				}
				else if (AfterDot > tmp05)
				{
					if(wt_rounding_method == 0)
						reVal = BeforDot + 1;
					else if (wt_rounding_method == 1)
						reVal = BeforDot + 1;
					else if (wt_rounding_method == 2)
						reVal = BeforDot + tmp05;
				}
				else if (AfterDot < tmp05)
				{
					if(wt_rounding_method == 0)
						reVal = BeforDot + tmp05;
					else if (wt_rounding_method == 1)
						reVal = BeforDot + tmp05;
					else if (wt_rounding_method == 2)
						reVal = BeforDot;
				}
			}
			else if(wt_increment_amt == 1)
			{
				if(AfterDot == tmp05) 
				{
					if(wt_rounding_method == 0)
						reVal = BeforDot + 1;
					else if (wt_rounding_method == 1)
						reVal = BeforDot + 1;
					else if (wt_rounding_method == 2)
						reVal = BeforDot;
				}
				else if (AfterDot > tmp05)
				{
					if(wt_rounding_method == 0)
						reVal = BeforDot + 1;
					else if (wt_rounding_method == 1)
						reVal = BeforDot + 1;
					else if (wt_rounding_method == 2)
						reVal = BeforDot;
				}
				else if (AfterDot < tmp05)
				{
					if(wt_rounding_method == 0)
						reVal = BeforDot;
					else if (wt_rounding_method == 1)
						reVal = BeforDot + 1;
					else if (wt_rounding_method == 2)
						reVal = BeforDot;
				}
			}

			return reVal;
		}

		private void retriveSWBPackage_Click(object sender, System.EventArgs e)
		{
			this.lblErrorMsg.Text = "";

			DataSet dsQryPkg = null;
			String custid = null;
			try
			{
				dsPkgDetails = DomesticShipmentMgrDAL.GetEmptyPkgDtlDS();
				//BindPkgGrid();
				//call the method for getting the package details during query
				if(Request.Params["CUSTID"].ToString() != "")
					custid = Request.Params["CUSTID"].ToString();
				dsQryPkg = DomesticShipmentMgrDAL.GetPakageDtlsSWB(appID,enterpriseID,strConsignment,custid);
			}
			catch(ApplicationException appException)
			{
				String strMsg = appException.Message;
				lblErrorMsg.Text = strMsg;
				return;
			}
			if(dsQryPkg != null)
			{
				dsPkgDetails = dsQryPkg;
			}
			else
			{
				dsPkgDetails = DomesticShipmentMgrDAL.GetEmptyPkgDtlDS();
			}
			//ShowOnly();
			BindPkgGrid();
			if(sender != null)
				retriveSWBPackage.Enabled = false;
			else
				retriveSWBPackage.Enabled = true;
		}

		private void btnRetrievePRPkgs_Click(object sender, System.EventArgs e)
		{
			this.lblErrorMsg.Text = "";

			DataTable dtPRConsignmentPKGTemp = PRBMgrDAL.ReadPickupConsignmentPkg(this.appID,this.enterpriseID,this.strBookingNo,"");

			if(dtPRConsignmentPKGTemp==null)
			{
				this.lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"NO_PACKAGE_AVAILABLE",utility.GetUserCulture());
				return;
			}
			if(dtPRConsignmentPKGTemp.Rows.Count<=0)
			{
				this.lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"NO_PACKAGE_AVAILABLE",utility.GetUserCulture());
				return;
			}

			dtPRConsignment = PRBMgrDAL.ReadPickupConsignment(this.appID,this.enterpriseID,this.strBookingNo,"");

			DataRow[] drPRConsignment = dtPRConsignment.Select("RecPostalcode<>''");
			
			if(this.strServiceCode.Trim()!="" && this.strRecipZip.Trim()!="")
				drPRConsignment = dtPRConsignment.Select("ServiceType='"+this.strServiceCode+"' and RecPostalCode='"+this.strRecipZip+"'","consignmentSeqNo ASC");

			if(this.strServiceCode.Trim()=="" && this.strRecipZip.Trim()!="")
				drPRConsignment = dtPRConsignment.Select("RecPostalCode='"+this.strRecipZip+"'","consignmentSeqNo ASC");

			if(this.strServiceCode.Trim()!="" && this.strRecipZip.Trim()=="")
				drPRConsignment = dtPRConsignment.Select("RecPostalCode='"+this.strRecipZip+"'","consignmentSeqNo ASC");

			if(this.strServiceCode.Trim()=="" && this.strRecipZip.Trim()=="")
				drPRConsignment = dtPRConsignment.Select("ServiceType<>'"+this.strServiceCode+"' and RecPostalCode<>'"+this.strRecipZip+"'","consignmentSeqNo ASC");


			//dtPRConsignment.Rows.Clear();
			DataTable dtPRConsignmentTemp = new DataTable();

			dtPRConsignmentTemp = dtPRConsignment.Copy();

			dtPRConsignmentTemp.Rows.Clear();
			dtPRConsignmentTemp.AcceptChanges();


			if(drPRConsignment.Length>0)
			{
				for(int i=0;i<drPRConsignment.Length;i++)
					dtPRConsignmentTemp.ImportRow(drPRConsignment[i]);
			}
			dtPRConsignmentTemp.AcceptChanges();
			ViewState["dtPRConsignment"] = dtPRConsignmentTemp;

			this.dgRetrievePRPkg.DataSource = dtPRConsignmentTemp;
			this.dgRetrievePRPkg.DataBind();

			this.txtPostalCode.Text = this.strRecipZip;
			this.txtServiceType.Text = this.strServiceCode;

			this.panelPackageDetails.Visible=false;
			this.panelPRPkgs.Style.Clear();
			this.panelPRPkgs.Style.Add("Z-INDEX","102");
			this.panelPRPkgs.Style.Add("LEFT","8px");		
			this.panelPRPkgs.Style.Add("POSITION","absolute");
			this.panelPRPkgs.Style.Add("TOP","26px");

			this.panelPRPkgs.Visible=true;
			this.panelPRPkgsConfirm.Visible=false;
		}

		private void btnRetrievePRPkgOK_Click(object sender, System.EventArgs e)
		{
			this.lblErrorMsg.Text = "";

			bool bFoundChecked = false;
			int iFoundChecked=0;
			for(int i = 0; i <= this.dgRetrievePRPkg.Items.Count - 1; i++)
			{
				CheckBox chkSelect = (CheckBox)this.dgRetrievePRPkg.Items[i].FindControl("chkSelect");

				if(chkSelect.Checked)
				{
					bFoundChecked = true;
					iFoundChecked = iFoundChecked +1;
					ViewState["strSelectedSeqNo"]=dgRetrievePRPkg.Items[i].Cells[1].Text;
					strSelectedSeqNo = dgRetrievePRPkg.Items[i].Cells[1].Text;
					strSelectedServiceCode = dgRetrievePRPkg.Items[i].Cells[2].Text;
					strSelectedCODAmount = dgRetrievePRPkg.Items[i].Cells[4].Text;
					strSelectedDeclaredValue = dgRetrievePRPkg.Items[i].Cells[5].Text;
				}
			}
		
			if(bFoundChecked==false || iFoundChecked>1)
			{
				this.lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"PLS_SELECT_CONSIGNMENT",utility.GetUserCulture());
				return;
			}

			this.txtConsCODAmount.Text = this.strCODAmount;
			this.txtConsDeclaredValue.Text=this.strShipDclrVal;
			this.txtConsServiceType.Text=this.strServiceCode;

			if((this.strServiceCode!=strSelectedServiceCode || Convert.ToDecimal(this.strCODAmount) != Convert.ToDecimal(strSelectedCODAmount) || Convert.ToDecimal(this.strShipDclrVal) != Convert.ToDecimal(strSelectedDeclaredValue)) && (this.strServiceCode!=""||Convert.ToDecimal(this.strCODAmount)!=Convert.ToDecimal("0")||Convert.ToDecimal(this.strShipDclrVal)!=Convert.ToDecimal("0")))
			{
				
				this.panelPRPkgsConfirm.Style.Clear();
				this.panelPRPkgsConfirm.Style.Add("Z-INDEX","102");
				this.panelPRPkgsConfirm.Style.Add("LEFT","174px");		
				this.panelPRPkgsConfirm.Style.Add("POSITION","absolute");
				this.panelPRPkgsConfirm.Style.Add("TOP","100px");

				this.txtPRCODAmount.Text = strSelectedCODAmount.Trim();
				this.txtPRDeclaredValue.Text = strSelectedDeclaredValue.Trim();
				this.txtPRServiceType.Text = strSelectedServiceCode.Trim();

				if(Convert.ToDecimal(this.txtConsCODAmount.Text)==Convert.ToDecimal(this.txtPRCODAmount.Text) || this.txtConsCODAmount.Text=="")
				{
					this.txtConsCODAmount.Text = this.txtPRCODAmount.Text;
					this.chkCopyCODAmount.Checked = true;
				}
				else
					this.chkCopyCODAmount.Checked = false;

				if(Convert.ToDecimal(this.txtConsDeclaredValue.Text)==Convert.ToDecimal(this.txtPRDeclaredValue.Text) || this.txtConsDeclaredValue.Text=="")
				{
					this.txtConsDeclaredValue.Text = this.txtPRDeclaredValue.Text;
					this.chkCopyDeclaredValue.Checked = true;
				}
				else
					this.chkCopyDeclaredValue.Checked = false;

				if(this.txtConsServiceType.Text.Trim()==this.txtPRServiceType.Text.Trim() || this.txtConsServiceType.Text=="")
				{
					this.txtConsServiceType.Text = this.txtPRServiceType.Text;
					this.chkCopyServiceType.Checked = true;
				}
				else
					this.chkCopyServiceType.Checked = false;

				this.panelPRPkgsConfirm.Visible=true;
				this.panelPRPkgs.Visible=false;
				this.panelPackageDetails.Visible=false;
			}
			else
			{
				this.panelPRPkgsConfirm.Visible=false;
				this.panelPRPkgs.Visible=false;
				
				this.txtConsCODAmount.Text = strSelectedCODAmount;
				this.txtConsDeclaredValue.Text = strSelectedDeclaredValue;
				this.txtConsServiceType.Text = strSelectedServiceCode;

				dtPRConsignmentPkgs = PRBMgrDAL.ReadPickupConsignmentPkg(this.appID,this.enterpriseID,this.iBookingNo.ToString(),strSelectedSeqNo);
				dtPRConsignmentVAS = PRBMgrDAL.ReadPickupConsignmentVAS(this.appID,this.enterpriseID,this.iBookingNo.ToString(),strSelectedSeqNo);

				dtPRConsignmentPkgs.TableName = "PackageDetails";

				dtPRConsignmentPkgs.Columns[4].ColumnName="mps_code";
				dtPRConsignmentPkgs.Columns.Add(new DataColumn("chargeable_wt",typeof(decimal)));
				
				dtPRConsignmentPkgs.AcceptChanges();

				foreach(DataRow drPRConsignmentPkgs in dtPRConsignmentPkgs.Rows)
				{
					decimal decChrgeable = 0;
					
					decimal decTotalDimWt = Convert.ToDecimal(drPRConsignmentPkgs["tot_dim_wt"].ToString());
					decimal decTotalWt = Convert.ToDecimal(drPRConsignmentPkgs["tot_wt"].ToString());
			
					if(strApplyDimWt.ToString().Trim() == "Y" || strApplyDimWt.ToString().Trim() == "")
					{
						if(decTotalWt < decTotalDimWt)
						{
							decChrgeable = decTotalDimWt;
						}
						else
						{
							decChrgeable = decTotalWt;
						}
					}
					else
					{
						decChrgeable = decTotalWt;
					}

					drPRConsignmentPkgs["chargeable_wt"] = decChrgeable;
				}

				dtPRConsignmentPkgs.AcceptChanges();

				ViewState["dtPRConsignmentPkgs"] = dtPRConsignmentPkgs;
				ViewState["dtPRConsignmentVAS"] = dtPRConsignmentVAS;

				dsPkgDetails.Tables.Clear();
				dsPkgDetails.Tables.Add(dtPRConsignmentPkgs.Copy());
				dsPkgDetails.AcceptChanges();
				ViewState["VIEWSTATE_DS2"] = dsPkgDetails;
				//ViewState["VIEWSTATE_DStemp"] = dsPkgDetails;

				ShowOnly();
				BindPkgGrid();

				this.panelPackageDetails.Visible=true;
			}

		}

		private void btnRetrievePRPkgCancel_Click(object sender, System.EventArgs e)
		{
			this.lblErrorMsg.Text = "";
			this.panelPRPkgsConfirm.Visible=false;
			this.panelPRPkgs.Visible=false;
			this.panelPackageDetails.Visible=true;
		}

		private void btnRetrievePRPkgShowAll_Click(object sender, System.EventArgs e)
		{
			this.lblErrorMsg.Text = "";
			dtPRConsignment = PRBMgrDAL.ReadPickupConsignment(this.appID,this.enterpriseID,this.strBookingNo,"");
			ViewState["dtPRConsignment"] = dtPRConsignment;
			this.dgRetrievePRPkg.DataSource = dtPRConsignment;
			this.dgRetrievePRPkg.DataBind();

		}

		private void btnConfirmRetrievePRPkgsOK_Click(object sender, System.EventArgs e)
		{
			this.lblErrorMsg.Text = "";
			this.panelPRPkgsConfirm.Visible=false;
			this.panelPRPkgs.Visible=false;
			if ((ViewState["strSelectedSeqNo"]!=null) && (ViewState["strSelectedSeqNo"].ToString()!=""))
			{
				strSelectedSeqNo=ViewState["strSelectedSeqNo"].ToString();
				ViewState["strSelectedSeqNo"]=null;
			}
			dtPRConsignmentPkgs = PRBMgrDAL.ReadPickupConsignmentPkg(this.appID,this.enterpriseID,this.iBookingNo.ToString(),strSelectedSeqNo);
			dtPRConsignmentVAS = PRBMgrDAL.ReadPickupConsignmentVAS(this.appID,this.enterpriseID,this.iBookingNo.ToString(),strSelectedSeqNo);

			dtPRConsignmentPkgs.TableName = "PackageDetails";

			dtPRConsignmentPkgs.Columns[4].ColumnName="mps_code";
			dtPRConsignmentPkgs.Columns.Add(new DataColumn("chargeable_wt",typeof(decimal)));
				
			dtPRConsignmentPkgs.AcceptChanges();

			foreach(DataRow drPRConsignmentPkgs in dtPRConsignmentPkgs.Rows)
			{
				decimal decChrgeable = 0;
					
				decimal decTotalDimWt = Convert.ToDecimal(drPRConsignmentPkgs["tot_dim_wt"].ToString());
				decimal decTotalWt = Convert.ToDecimal(drPRConsignmentPkgs["tot_wt"].ToString());
			
				if(strApplyDimWt.ToString().Trim() == "Y" || strApplyDimWt.ToString().Trim() == "")
				{
					if(decTotalWt < decTotalDimWt)
					{
						decChrgeable = decTotalDimWt;
					}
					else
					{
						decChrgeable = decTotalWt;
					}
				}
				else
				{
					decChrgeable = decTotalWt;
				}

				drPRConsignmentPkgs["chargeable_wt"] = decChrgeable;
			}

			dtPRConsignmentPkgs.AcceptChanges();

			ViewState["dtPRConsignmentPkgs"] = dtPRConsignmentPkgs;
			ViewState["dtPRConsignmentVAS"] = dtPRConsignmentVAS;

			dsPkgDetails.Tables.Clear();
			dsPkgDetails.Tables.Add(dtPRConsignmentPkgs.Copy());
			dsPkgDetails.AcceptChanges();
			ViewState["VIEWSTATE_DS2"] = dsPkgDetails;
			//ViewState["VIEWSTATE_DStemp"] = dsPkgDetails;

			ShowOnly();
			BindPkgGrid();
			this.panelPackageDetails.Visible=true;
		}

		private void btnConfirmRetrievePRPkgsCancel_Click(object sender, System.EventArgs e)
		{
			this.lblErrorMsg.Text = "";
			this.panelPackageDetails.Visible=false;
			this.panelPRPkgs.Style.Clear();
			this.panelPRPkgs.Style.Add("Z-INDEX","102");
			this.panelPRPkgs.Style.Add("LEFT","8px");		
			this.panelPRPkgs.Style.Add("POSITION","absolute");
			this.panelPRPkgs.Style.Add("TOP","26px");

			this.panelPRPkgs.Visible=true;
			this.panelPRPkgsConfirm.Visible=false;
		}

		private void chkCopyServiceType_CheckedChanged(object sender, System.EventArgs e)
		{
			if(this.chkCopyServiceType.Checked)
			{
				this.txtConsServiceType.Text= this.txtPRServiceType.Text.Trim();
			}
			else
				this.txtConsServiceType.Text=this.strServiceCode;
		}

		private void chkCopyCODAmount_CheckedChanged(object sender, System.EventArgs e)
		{
			if(this.chkCopyCODAmount.Checked)
			{
				this.txtConsCODAmount.Text= this.txtPRCODAmount.Text.Trim();
			}
			else
				this.txtConsCODAmount.Text=this.strCODAmount;
		}

		private void chkCopyDeclaredValue_CheckedChanged(object sender, System.EventArgs e)
		{
			if(this.chkCopyDeclaredValue.Checked)
			{
				this.txtConsDeclaredValue.Text= this.txtPRDeclaredValue.Text.Trim();
			}
			else
				this.txtConsDeclaredValue.Text=this.strShipDclrVal;
		}

		private void dgRetrievePRPkg_PageIndexChanged(object source, System.Web.UI.WebControls.DataGridPageChangedEventArgs e)
		{
			this.lblErrorMsg.Text="";
			dtPRConsignment=(DataTable)ViewState["dtPRConsignment"];
			this.dgRetrievePRPkg.DataSource = dtPRConsignment;
			this.dgRetrievePRPkg.CurrentPageIndex = e.NewPageIndex;
			this.dgRetrievePRPkg.DataBind();

		}

	}
}
