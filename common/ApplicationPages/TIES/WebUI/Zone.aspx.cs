using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.common.util;
using com.ties.DAL;
using com.common.classes;
using com.common.applicationpages;

namespace com.ties
{
	/// <summary>
	/// Summary description for Zone.
	/// </summary>
	public class Zone : BasePage
	{
		protected System.Web.UI.WebControls.Button btnExecuteQuery;
		protected System.Web.UI.WebControls.Button btnInsert;
		protected System.Web.UI.WebControls.DataGrid dgZone;
		protected System.Web.UI.WebControls.Button btnQuery;

		//Utility utility = null;
		private String m_strAppID;
		private String m_strEnterpriseID;
		SessionDS m_sdsZone = null;
		protected System.Web.UI.WebControls.Label lblErrorMessage;
		protected System.Web.UI.WebControls.ValidationSummary ValidationSummary1;
		protected System.Web.UI.WebControls.Label lblTitle;

		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			//utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings,Page.Session);
			m_strAppID = utility.GetAppID();
			m_strEnterpriseID = utility.GetEnterpriseID();

			if (!Page.IsPostBack)
			{
				QueryMode();
			}
			else
			{
				m_sdsZone = (SessionDS)Session["SESSION_DS1"];
			}
			lblErrorMessage.Text = "";
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			base.OnInit(e);
			InitializeComponent();
		
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnQuery.Click += new System.EventHandler(this.btnQuery_Click);
			this.btnExecuteQuery.Click += new System.EventHandler(this.btnExecuteQuery_Click);
			this.btnInsert.Click += new System.EventHandler(this.btnInsert_Click);
			this.dgZone.SelectedIndexChanged += new System.EventHandler(this.dgZone_SelectedIndexChanged);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		protected void dgZone_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			Logger.LogDebugInfo("Zone","dgZone_SelectedIndexChanged","INF004","updating data grid...");
		}

		protected void dgZone_Update(object sender, DataGridCommandEventArgs e)
		{
			lblErrorMessage.Text = "";
			if (!btnExecuteQuery.Enabled)
			{
				int iRowsAffected = 0;
				int rowIndex = e.Item.ItemIndex;
				GetChangedRow(e.Item, e.Item.ItemIndex);

				//int iMode = (int)ViewState["Mode"];
				int iOperation = (int)ViewState["Operation"];
				try
				{
					if (iOperation == (int)Operation.Insert)
					{					
						iRowsAffected = SysDataManager2.InsertZone(m_sdsZone.ds, rowIndex, m_strAppID, m_strEnterpriseID);
						ArrayList paramValues = new ArrayList();
						paramValues.Add(""+iRowsAffected);						
						lblErrorMessage.Text=Utility.GetLanguageText(ResourceType.UserMessage,"ROWS_INS",utility.GetUserCulture(),paramValues);
					}
					else
					{
						DataSet dsChangedRow = m_sdsZone.ds.GetChanges();
						iRowsAffected = SysDataManager2.UpdateZone(dsChangedRow, m_strAppID, m_strEnterpriseID);
						ArrayList paramValues = new ArrayList();
						paramValues.Add(""+iRowsAffected);						
						lblErrorMessage.Text=Utility.GetLanguageText(ResourceType.UserMessage,"ROWS_UPD",utility.GetUserCulture(),paramValues);
						m_sdsZone.ds.Tables[0].Rows[e.Item.ItemIndex].AcceptChanges();
					}
				}
				catch(ApplicationException appException)
				{
					String strMsg = appException.Message;
					strMsg = appException.InnerException.Message;
					strMsg = appException.InnerException.InnerException.Message;
					if(strMsg.IndexOf("duplicate key") != -1 || strMsg.IndexOf("PRIMARY KEY") != -1 )
					{
						lblErrorMessage.Text =  Utility.GetLanguageText(ResourceType.UserMessage,"DUP_KEY_FOUND_ZONE",utility.GetUserCulture());
							
					}
					if(strMsg.IndexOf("unique") != -1 )
					{
						lblErrorMessage.Text =  Utility.GetLanguageText(ResourceType.UserMessage,"DUP_KEY_FOUND_ZONE",utility.GetUserCulture());
							
					}				

					Logger.LogTraceError("Zone.aspx.cs","dgZone_Update","RBAC003",appException.Message.ToString());
					//lblErrorMessage.Text = appException.Message.ToString();
					return;
				}

				if (iRowsAffected == 0)
				{
					return;
				}
				Utility.EnableButton(ref btnInsert,ButtonType.Insert,true,m_moduleAccessRights);
				ViewState["Operation"] = Operation.None;
				dgZone.EditItemIndex = -1;
				BindGrid();
				Logger.LogDebugInfo("Zone","dgZone_Update","INF004","updating data grid...");			
			}
		}

		protected void dgZone_Cancel(object sender, DataGridCommandEventArgs e)
		{
			lblErrorMessage.Text = "";
			//int iMode = (int)ViewState["Mode"];
			int rowIndex = e.Item.ItemIndex;
			if ((int)ViewState["Mode"] == (int)ScreenMode.Insert && (int)ViewState["Operation"] == (int)Operation.Insert)
			{
				Utility.EnableButton(ref btnInsert,ButtonType.Insert,true,m_moduleAccessRights);
				m_sdsZone.ds.Tables[0].Rows.RemoveAt(rowIndex);
				//ViewState["Mode"] = ScreenMode.None;
			}
			ViewState["Operation"] = Operation.None;
			dgZone.EditItemIndex = -1;
			BindGrid();
			Logger.LogDebugInfo("Zone","dgZone_Cancel","INF004","updating data grid...");			
		}

		protected void dgZone_Edit(object sender, DataGridCommandEventArgs e)
		{
			if((int)ViewState["Mode"] == (int)ScreenMode.Insert && (int) ViewState["Operation"] == (int)Operation.Insert && dgZone.EditItemIndex > 0)
			{
				m_sdsZone.ds.Tables[0].Rows.RemoveAt(dgZone.EditItemIndex);
				m_sdsZone.QueryResultMaxSize--;
				dgZone.CurrentPageIndex = 0;
			}
			dgZone.EditItemIndex = e.Item.ItemIndex;
			ViewState["Operation"] = Operation.Update;
			BindGrid();
			Logger.LogDebugInfo("Zone","dgZone_Edit","INF004","updating data grid...");			
		}

		protected void dgZone_Delete(object sender, DataGridCommandEventArgs e)
		{
			if (!btnExecuteQuery.Enabled)
			{
				if ((int)ViewState["Operation"] == (int)Operation.Update)
				{
					dgZone.EditItemIndex = -1;
				}
				BindGrid();

				int rowIndex = e.Item.ItemIndex;
				m_sdsZone = (SessionDS)Session["SESSION_DS1"];

				try
				{
					// delete from table
					int iRowsDeleted = SysDataManager2.DeleteZone(m_sdsZone.ds, rowIndex, m_strAppID, m_strEnterpriseID);
					ArrayList paramValues = new ArrayList();
					paramValues.Add(""+iRowsDeleted);						
					lblErrorMessage.Text=Utility.GetLanguageText(ResourceType.UserMessage,"DELETED_SUCCESSFULLY",utility.GetUserCulture(),paramValues);
				}
				catch(ApplicationException appException)
				{
					String strMsg = appException.Message;
					if(strMsg.IndexOf("FK") != -1)
					{
						lblErrorMessage.Text =  Utility.GetLanguageText(ResourceType.UserMessage,"ERR_DEL_ZONE_TRANS",utility.GetUserCulture());
					}
					if(strMsg.ToLower().IndexOf("child record found") != -1 || strMsg.IndexOf("REFERENCE") !=-1)
					{
						lblErrorMessage.Text =  Utility.GetLanguageText(ResourceType.UserMessage,"ERR_DEL_ZONE_TRANS",utility.GetUserCulture());
					}
					else
					{
						lblErrorMessage.Text = strMsg;
					}

					Logger.LogTraceError("Zone.aspx.cs","dgZone_Delete","RBAC003",appException.Message.ToString());
					return;
				}

				// remove the data from grid

				if((int)ViewState["Mode"] == (int)ScreenMode.Insert)
				{
					m_sdsZone.ds.Tables[0].Rows.RemoveAt(rowIndex);
				}
				else
				{
					RetreiveSelectedPage();
				}

				//Set ViewStates to None
				ViewState["Operation"] = Operation.None;

				//Make the row in non-edit Mode
				EditRow(false);

				BindGrid();
				Logger.LogDebugInfo("Zone","dgZone_Delete","INF004","updating data grid...");			
			}
		}

		protected void dgZone_Bound(object sender, DataGridItemEventArgs e)
		{
			if(e.Item.ItemIndex == -1)
			{
				return;
			}

			DataRow drSelected = m_sdsZone.ds.Tables[0].Rows[e.Item.ItemIndex];
			if(drSelected.RowState == DataRowState.Deleted)
			{
				return;
			}

			if ((int)ViewState["Operation"] != (int)Operation.Insert && (!btnExecuteQuery.Enabled))
			{
				msTextBox txtZoneCode = (msTextBox)e.Item.FindControl("txtZoneCode");
				if(txtZoneCode != null) 
				{
					txtZoneCode.Enabled = false;
				}
			}
			if ((int)ViewState["Mode"] == (int)ScreenMode.Insert && (int)ViewState["Operation"] == (int)Operation.Insert)
			{
				e.Item.Cells[1].Enabled=false;
			}
		}

		protected void dgZone_PageChange(object sender, DataGridPageChangedEventArgs e)
		{
			dgZone.CurrentPageIndex = e.NewPageIndex;
			RetreiveSelectedPage();
			Logger.LogDebugInfo("Zone","dgZone_PageChange","INF009","On Page Change "+e.NewPageIndex);
		}

		private void btnQuery_Click(object sender, System.EventArgs e)
		{
			lblErrorMessage.Text = "";
			dgZone.CurrentPageIndex = 0;
			QueryMode();
		}

		protected void btnExecuteQuery_Click(object sender, System.EventArgs e)
		{
			lblErrorMessage.Text = "";
			btnExecuteQuery.Enabled = false;

			//Since Query mode is always one row in edit mode, the rowindex is set to zero
			int rowIndex = 0; 

			GetChangedRow(dgZone.Items[0], rowIndex);
			Session["QUERY_DS"] = m_sdsZone.ds;

			RetreiveSelectedPage();

			//set all records to non-edit mode after execute query
			EditRow(false);
			dgZone.Columns[0].Visible=true;
			dgZone.Columns[1].Visible=true;
		}

		private void btnInsert_Click(object sender, System.EventArgs e)
		{
			//clear the rows from the datagrid
			//m_sdsZone.ds.Clear();
			if (btnExecuteQuery.Enabled == true)
			{
				btnExecuteQuery.Enabled = false;
			}
			if((int)ViewState["Mode"] != (int)ScreenMode.Insert || m_sdsZone.ds.Tables[0].Rows.Count >= dgZone.PageSize)
			{
				m_sdsZone = SysDataManager2.GetEmptyZoneCodeDS();
				//m_sdsZone.ds.Clear();
			}
			else
			{
				AddRow();
			}

			Utility.EnableButton(ref btnInsert,ButtonType.Insert,false,m_moduleAccessRights);
			EditRow(false);
			ViewState["Mode"] = ScreenMode.Insert;
			ViewState["Operation"] = Operation.Insert;
			dgZone.Columns[0].Visible=true;
			dgZone.Columns[1].Visible=true;
			//AddRow();
			dgZone.EditItemIndex = m_sdsZone.ds.Tables[0].Rows.Count - 1;
			dgZone.CurrentPageIndex = 0;
			BindGrid();
			getPageControls(Page);
			Logger.LogDebugInfo("Zone","btnInsert_Click","INF003","Data Grid Items count"+dgZone.Items.Count);
		}

		private void BindGrid()
		{
			dgZone.VirtualItemCount = System.Convert.ToInt32(m_sdsZone.QueryResultMaxSize);
			dgZone.DataSource = m_sdsZone.ds;
			dgZone.DataBind();
			Session["SESSION_DS1"] = m_sdsZone;
		}

		private void EditRow(bool bItemIndex)
		{
			foreach (DataGridItem item in dgZone.Items)
			{ 
				if (bItemIndex) 
				{
					dgZone.EditItemIndex = item.ItemIndex; }
				else 
				{
					dgZone.EditItemIndex = -1; }
			}
			dgZone.DataBind();
		}

		private void QueryMode()
		{
			//Set ViewStates to None
			ViewState["Mode"] = ScreenMode.None;
			ViewState["Operation"] = Operation.None;

			//Set Button Enable Properties
			btnQuery.Enabled=true;
			btnExecuteQuery.Enabled=true;
			Utility.EnableButton(ref btnInsert,ButtonType.Insert,true,m_moduleAccessRights);

			//inset an empty row for query 
			m_sdsZone = SysDataManager2.GetEmptyZoneCodeDS();
			m_sdsZone.DataSetRecSize = 1;
			Session["SESSION_DS1"] = m_sdsZone;
			BindGrid();

			//Make the row in Edit Mode
			EditRow(true);
			dgZone.Columns[0].Visible=false;
			dgZone.Columns[1].Visible=false;
		}

		private void AddRow()
		{
			SysDataManager2.AddNewRowInZoneCodeDS(ref m_sdsZone);
			BindGrid();
		}

		private void GetChangedRow(DataGridItem item, int rowIndex)
		{
			msTextBox txtZoneCode = (msTextBox)item.FindControl("txtZoneCode");
			TextBox txtZoneName = (TextBox)item.FindControl("txtZoneName");
				
			string strCode = txtZoneCode.Text.ToString().ToUpper();  //Jeab 04 Mar 11
			string strName = txtZoneName.Text.ToString();

			DataRow dr = m_sdsZone.ds.Tables[0].Rows[rowIndex];
			dr[0] = strCode;
			dr[1] = strName;
		}

		private void RetreiveSelectedPage()
		{
			int iStartRow = dgZone.CurrentPageIndex * dgZone.PageSize;
			DataSet dsQuery = (DataSet)Session["QUERY_DS"];
			m_sdsZone = SysDataManager2.GetZoneCodeDS(utility.GetAppID(), utility.GetEnterpriseID(), iStartRow, dgZone.PageSize, dsQuery);
			int iPageCnt = Convert.ToInt32((m_sdsZone.QueryResultMaxSize - 1))/dgZone.PageSize;
			if(iPageCnt < dgZone.CurrentPageIndex)
			{
				dgZone.CurrentPageIndex = System.Convert.ToInt32(iPageCnt);
			}
			dgZone.SelectedIndex = -1;
			dgZone.EditItemIndex = -1;

			BindGrid();
			Session["SESSION_DS1"] = m_sdsZone;
		}

	}
}
