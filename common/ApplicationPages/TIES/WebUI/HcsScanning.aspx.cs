using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.common.applicationpages;
using com.common.util;
using com.ties.DAL;
using com.common.RBAC;
using com.common.classes;
using System.Globalization;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;


namespace com.ties
{
	/// <summary>
	/// Summary description for HcsScanning.
	/// </summary>
	public class HcsScanning : BasePage
	{
		protected System.Web.UI.WebControls.Label lblSaveInDBDisplay;
		protected com.common.util.msTextBox txtDate;	
		protected System.Web.UI.WebControls.Label lblErrorMessage;
		protected System.Web.UI.WebControls.DataGrid dgHcs;
		protected System.Web.UI.WebControls.TextBox txtProcesstoDMS;
		protected System.Web.UI.WebControls.ListBox lbScanningPackages;
		protected System.Web.UI.WebControls.Label lblUserID;
		protected System.Web.UI.WebControls.Label lblLocation;
		protected System.Web.UI.WebControls.Label lblSubDc;
		protected System.Web.UI.WebControls.TextBox hidAppID;
		protected System.Web.UI.WebControls.TextBox hidEnterpriseID;
		protected System.Web.UI.WebControls.Label lblHcsNumber;
		protected System.Web.UI.WebControls.Label lblHcsDate;
		protected System.Web.UI.WebControls.TextBox txtHcsNumber;
		protected System.Web.UI.WebControls.ListBox lbViewData;
		protected System.Web.UI.WebControls.Button btnCallServer;
		protected System.Web.UI.WebControls.TextBox txtViewDate;
		protected System.Web.UI.WebControls.Button btnNew;
		protected System.Web.UI.WebControls.Button btnDelete;
		protected System.Web.UI.WebControls.Button btnClose;
		protected System.Web.UI.WebControls.Button btnPrint;
		protected System.Web.UI.WebControls.Button btnExport;
		protected System.Web.UI.WebControls.Button btnScan;
		protected System.Web.UI.WebControls.Button btnStop;
		protected System.Web.UI.WebControls.DataGrid dgError;		
		protected System.Web.UI.HtmlControls.HtmlGenericControl divEdit;
		protected System.Web.UI.HtmlControls.HtmlGenericControl divView;
		protected System.Web.UI.HtmlControls.HtmlGenericControl divSearch;
		protected System.Web.UI.HtmlControls.HtmlGenericControl divSubDc;
		protected System.Web.UI.HtmlControls.HtmlGenericControl divTab;
		protected System.Web.UI.WebControls.TextBox txtMode;
		protected System.Web.UI.WebControls.Button btnEdit;
		protected System.Web.UI.WebControls.Button btnView;
		protected System.Web.UI.WebControls.ListBox lbEditData;
		protected System.Web.UI.WebControls.TextBox txtScanerID;
		protected System.Web.UI.WebControls.TextBox txtBarCode;
		protected System.Web.UI.WebControls.Button btnClear;
		protected System.Web.UI.WebControls.Label lblStatusCode;
		protected System.Web.UI.WebControls.Button btnCancel;		
		protected System.Web.UI.WebControls.TextBox txtSearchCons;
		protected System.Web.UI.WebControls.Label lblSHcsNo;
		protected System.Web.UI.WebControls.Label lblSHcsDate;
		protected System.Web.UI.WebControls.Label lblSHcsStatus;
		protected System.Web.UI.WebControls.Label lblSCreatedBy;
		protected System.Web.UI.WebControls.Label lblSCreatedDate;
		protected System.Web.UI.WebControls.Button btnSearch;
		protected System.Web.UI.WebControls.Button btnSearchCons;
		protected System.Web.UI.WebControls.Label lblSResult;
		protected System.Web.UI.WebControls.ListBox lbSubDc;		
		protected System.Web.UI.WebControls.TextBox txtGridMode;
		protected ArrayList userRoleArray;
		private string FileName = "";
		string m_strAppID = null;
		string m_strEnterpriseID = null;
		SessionDS m_sdsHCS = null;
		SessionDS m_sdsERROR = null;


		private void Page_Load(object sender, System.EventArgs e)
		{
			utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings,Page.Session);
			m_strAppID = utility.GetAppID();
			m_strEnterpriseID = utility.GetEnterpriseID();

			if (!Page.IsPostBack)
			{
				com.common.classes.User user = RBACManager.GetUser(m_strAppID,m_strEnterpriseID, (String)Session["userID"]);
				hidAppID.Text = m_strAppID;
				hidEnterpriseID.Text = m_strEnterpriseID;

				lblUserID.Text = user.UserID;
				lblLocation.Text = user.UserLocation;
			}			 

			m_sdsHCS  = (SessionDS)Session["SESSION_DS_HcS"];
			m_sdsERROR = (SessionDS)Session["SESSION_DS_ErroR"];

			lblHcsNumber.Text = (string)ViewState["VIEWSTATE_STRING_HCSNO"];
			lblHcsDate.Text = (string)ViewState["VIEWSTATE_STRING_HCSDATE"];

			ActionMode(txtMode.Text);

			if (txtMode.Text == "EDIT"  &&  txtGridMode.Text == "SCAN")
			{
				txtBarCode.Attributes.Add("OnBlur","OnFocus();");
			}
		}

		
		public void RefreshGrid()
		{
			ClearGrid();
			GetDgHcs();
			GetDgHcsError();
		}


		public void ActionMode(string strMode)
		{
			txtMode.Text = strMode;			

			lblErrorMessage.Text = "";

			divSubDc.Style.Add("VISIBILITY", "hidden");

			if (strMode == "CLEAR")
			{
				divEdit.Style.Add("VISIBILITY", "hidden");
				divView.Style.Add("VISIBILITY", "hidden");
				divSearch.Style.Add("VISIBILITY", "hidden");

				btnNew.Enabled = false;
				btnDelete.Enabled = false;
				btnClose.Enabled = false;
				btnPrint.Enabled = false;
				btnExport.Enabled = false;
				btnCancel.Enabled = false;
			
				lblHcsNumber.Text = "";
				lblHcsDate.Text = "";

				ViewState["VIEWSTATE_STRING_TXTGRIDMODE"] = "";
				txtGridMode.Text = (string)ViewState["VIEWSTATE_STRING_TXTGRIDMODE"];
			}
			if (strMode == "EDIT")
			{
				divEdit.Style.Add("VISIBILITY", "visible");
				divView.Style.Add("VISIBILITY", "hidden");
				divSearch.Style.Add("VISIBILITY", "hidden");

				btnNew.Enabled = true;
				btnDelete.Enabled = true;
				btnClose.Enabled = true;
				btnPrint.Enabled = false;
				btnExport.Enabled = false;
				btnCancel.Enabled = false;

				lblHcsNumber.Text = (string)ViewState["VIEWSTATE_STRING_HCSNO"];
				lblHcsDate.Text = (string)ViewState["VIEWSTATE_STRING_HCSDATE"];

				ViewState["VIEWSTATE_STRING_TXTGRIDMODE"] = txtGridMode.Text;
			}
			if (strMode == "VIEW")
			{
				divEdit.Style.Add("VISIBILITY", "hidden");
				divView.Style.Add("VISIBILITY", "visible");
				divSearch.Style.Add("VISIBILITY", "hidden");

				btnNew.Enabled = false;
				btnDelete.Enabled = false;
				btnClose.Enabled = false;
				btnPrint.Enabled = true;
				btnExport.Enabled = true;

				/// check role HCSSUPERVISOR
				if (getRole(lblUserID.Text, "HCSSUPERVISOR") == 1)
					btnCancel.Enabled = true;
				else
					btnCancel.Enabled = false;

				lblHcsNumber.Text = (string)ViewState["VIEWSTATE_STRING_HCSNO"];
				lblHcsDate.Text = (string)ViewState["VIEWSTATE_STRING_HCSDATE"];

				ViewState["VIEWSTATE_STRING_TXTGRIDMODE"] = "";
				txtGridMode.Text = (string)ViewState["VIEWSTATE_STRING_TXTGRIDMODE"];
			}
			if (strMode == "SEARCH")
			{
				divEdit.Style.Add("VISIBILITY", "hidden");
				divView.Style.Add("VISIBILITY", "hidden");
				divSearch.Style.Add("VISIBILITY", "visible");

				btnNew.Enabled = false;
				btnDelete.Enabled = false;
				btnClose.Enabled = false;
				btnPrint.Enabled = false;
				btnExport.Enabled = false;
				btnCancel.Enabled = false;
			
				lblHcsNumber.Text = "";
				lblHcsDate.Text = "";

				ViewState["VIEWSTATE_STRING_TXTGRIDMODE"] = "";
				txtGridMode.Text = (string)ViewState["VIEWSTATE_STRING_TXTGRIDMODE"];
			}
		}


		public void ClearGrid()		////For clear datagrid , both dgHcs and dgError
		{
			ViewState["DPOperation"] = Operation.None;

			///---------------------------------- for dgHcs
			dgHcs.CurrentPageIndex = 0;
			
			m_sdsHCS = HcsScanDAL.GetEmptyHcs();
			m_sdsHCS.DataSetRecSize = 1;
			Session["SESSION_DS_HcS"] = m_sdsHCS;

			BindHcsDataGrid();


			///---------------------------------- for dgError
			dgError.CurrentPageIndex = 0;

			m_sdsERROR = HcsScanDAL.GetEmptyHcsError();
			m_sdsERROR.DataSetRecSize = 1;
			Session["SESSION_DS_ErroR"] = m_sdsERROR;

			BindErrorDataGrid();
		}

	
		public void BindHcsDataGrid()
		{
			dgHcs.VirtualItemCount = System.Convert.ToInt32(m_sdsHCS.QueryResultMaxSize);
			dgHcs.DataSource = m_sdsHCS.ds;
			dgHcs.DataBind(); 
			Session["SESSION_DS_HcS"] = m_sdsHCS;

			if (dgHcs.Items.Count == 0)
				dgHcs.Visible = false;
			else
				dgHcs.Visible = true;
		}


		public void BindErrorDataGrid()
		{
			dgError.VirtualItemCount = System.Convert.ToInt32(m_sdsERROR.QueryResultMaxSize);
			dgError.DataSource = m_sdsERROR.ds;
			dgError.DataBind(); 
			Session["SESSION_DS_Error"] = m_sdsERROR;

			if (dgError.Items.Count == 0)
				dgError.Visible = false;
			else
				dgError.Visible = true;
		}

		
		protected void dgHcs_Update(object sender, DataGridCommandEventArgs e)
		{
			lblErrorMessage.Text = "";

			int iRowsAffected = 0;
			int rowIndex = e.Item.ItemIndex;

			GetChangedRow(e.Item, e.Item.ItemIndex);

			DataRow drSelected = m_sdsHCS.ds.Tables[0].Rows[e.Item.ItemIndex];

			//// ==Process Update Hcs_Dt
			{
				int iOperation = (int)ViewState["DPOperation"];
				try
				{//// UPDATE HCS_DT
					DataSet dsChangedRow = m_sdsHCS.ds.GetChanges();
					iRowsAffected = HcsScanDAL.HcsDtUpdate(m_strAppID, m_strEnterpriseID, dsChangedRow);

					ArrayList paramValues = new ArrayList();
					paramValues.Add("" + iRowsAffected);
					lblErrorMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage, "ROWS_UPD", utility.GetUserCulture(), paramValues);
							
					m_sdsHCS.ds.Tables[0].Rows[e.Item.ItemIndex].AcceptChanges();

					dsChangedRow.Dispose();
				}
				catch(ApplicationException appException)
				{
					String strMsg = appException.Message;
					strMsg = appException.InnerException.Message;
					strMsg = appException.InnerException.InnerException.Message;
					if(strMsg.IndexOf("PRIMARY KEY") != -1)
					{
						lblErrorMessage.Text = "Duplication of HCS_NO is Found"; 
					}
					else
					{
						lblErrorMessage.Text = strMsg;
					}

					Logger.LogTraceError("DeliveryPathCode.aspx.cs","dgHcs_Update","RBAC003",appException.Message.ToString());
					return;
				}

				if (iRowsAffected == 0)
				{
					return;
				}

				ViewState["DPOperation"] = Operation.None;
				dgHcs.EditItemIndex = -1;
				BindHcsDataGrid();
				Logger.LogDebugInfo("HcsScanning","dgHcs_Update","INF004","updating data grid...");			
			}
		}

		
		protected void dgHcs_Edit(object sender, DataGridCommandEventArgs e)
		{
			m_sdsHCS  = (SessionDS)Session["SESSION_DS_HcS"];

			ViewState["EditRow"] = e.Item.ItemIndex;

			lblErrorMessage.Text = "";

			if(dgHcs.EditItemIndex > 0)
			{				
				m_sdsHCS.ds.Tables[0].Rows.RemoveAt(dgHcs.EditItemIndex);
				dgHcs.CurrentPageIndex = 0;
			}

			dgHcs.EditItemIndex = e.Item.ItemIndex;
			ViewState["DPOperation"] = Operation.Update;
			BindHcsDataGrid();
			getPageControls(Page);
		}

		
		protected void dgHcs_ItemDataBound(object sender, DataGridItemEventArgs e)
		{
			if(e.Item.ItemIndex == -1)
				return;

			DataRow drSelected = m_sdsHCS.ds.Tables[0].Rows[e.Item.ItemIndex];
			   
			if(drSelected.RowState == DataRowState.Deleted)
				return;
		}
		
		
		protected void dgHcs_Cancel(object sender, DataGridCommandEventArgs e)
		{
			lblErrorMessage.Text = "";

			int rowIndex = e.Item.ItemIndex;

			ViewState["DPOperation"] = Operation.None;

			dgHcs.EditItemIndex = -1;

			BindHcsDataGrid();
		}

		
		protected void dgHcs_Delete(object sender, DataGridCommandEventArgs e)
		{
			if ((int)ViewState["DPOperation"] == (int)Operation.Update)
			{
				dgHcs.EditItemIndex = -1;
			}

			int rowIndex = e.Item.ItemIndex;
			m_sdsHCS  = (SessionDS)Session["SESSION_DS_HcS"];

			BindHcsDataGrid();

			try
			{
				DataRow dr = m_sdsHCS.ds.Tables[0].Rows[rowIndex];
				string strHcsNo = (string)dr["hcs_no"];
					
				////---- DELETE
				int iRowsDeleted = HcsScanDAL.HcsDtDelete(m_strAppID, m_strEnterpriseID, m_sdsHCS.ds, rowIndex);
				lblErrorMessage.Text = iRowsDeleted + " row(s) deleted.";
			}
			catch(ApplicationException appException)
			{
				String strMsg = appException.Message;
				if(strMsg.IndexOf("FK") != -1)
				{
					lblErrorMessage.Text =  Utility.GetLanguageText(ResourceType.UserMessage,"ERR_DEL_PATH_TRANS",utility.GetUserCulture());
				}
				if(strMsg.ToLower().IndexOf("child record found") != -1 )
				{
					lblErrorMessage.Text =  Utility.GetLanguageText(ResourceType.UserMessage,"ERR_DEL_PATH_TRANS",utility.GetUserCulture());							
				}			
				else
				{
					lblErrorMessage.Text = strMsg;
				}

				Logger.LogTraceError("HcsScanning", "dgHcs_Delete", "RBAC003", appException.Message.ToString());

				return;
			}

			RetreiveSelectedPage();

			ViewState["DPOperation"] = Operation.None;

			BindHcsDataGrid();
			Logger.LogDebugInfo("HcsScanning", "dgHcs_Delete", "INF004", "deleting data grid...");
		}		
	
		
		protected void dgError_ItemDataBound(object sender, DataGridItemEventArgs e)
		{
			if(e.Item.ItemIndex == -1)
			{
				return;
			}

			DataRow drSelected = m_sdsERROR.ds.Tables[0].Rows[e.Item.ItemIndex];
			   
			if(drSelected.RowState == DataRowState.Deleted)
			{
				return;
			}
		}
		
		
		private void GetChangedRow(DataGridItem item, int rowIndex)
		{
			TextBox txtInvrRemark = (TextBox)item.FindControl("txtInvrRemark");

			DataRow dr = m_sdsHCS.ds.Tables[0].Rows[rowIndex];

			dr["invrremark"] = txtInvrRemark.Text.ToString();

			Session["SESSION_DS_HcS"] = m_sdsHCS;
		}

		
		private void RetreiveSelectedPage()
		{
			int iStartRow = dgHcs.CurrentPageIndex * dgHcs.PageSize;

			string strHcsNo = (string)ViewState["VIEWSTATE_STRING_HCSNO"];

			m_sdsHCS = HcsScanDAL.GetHcsDS(utility.GetAppID(), utility.GetEnterpriseID(), iStartRow, dgHcs.PageSize, strHcsNo);

			int iPageCnt = Convert.ToInt32((m_sdsHCS.QueryResultMaxSize - 1))/dgHcs.PageSize;

			if(iPageCnt < dgHcs.CurrentPageIndex)
			{
				dgHcs.CurrentPageIndex = System.Convert.ToInt32(iPageCnt);
			}
			
			dgHcs.SelectedIndex = -1;
			dgHcs.EditItemIndex = -1;
			
			if (txtMode.Text == "EDIT")
			{
				dgHcs.Columns[0].Visible = true;
				dgHcs.Columns[1].Visible = true;
			}
			else
			{
				dgHcs.Columns[0].Visible = false;
				dgHcs.Columns[1].Visible = false;
			}

			BindHcsDataGrid();
			Session["SESSION_DS_HcS"] = m_sdsHCS;
		}

		
		private void RetreiveSelectedErrorPage()
		{
			int iStartRow = dgError.CurrentPageIndex * dgError.PageSize;

			string strHcsNo = (string)ViewState["VIEWSTATE_STRING_HCSNO"];

			m_sdsERROR = HcsScanDAL.GetHcsErrorDS(utility.GetAppID(), utility.GetEnterpriseID(), iStartRow, dgError.PageSize, strHcsNo);

			int iPageCnt = Convert.ToInt32((m_sdsERROR.QueryResultMaxSize - 1))/dgError.PageSize;

			if(iPageCnt < dgError.CurrentPageIndex)
			{
				dgError.CurrentPageIndex = System.Convert.ToInt32(iPageCnt);
			}
			
			dgError.SelectedIndex = -1;
			dgError.EditItemIndex = -1;

			BindErrorDataGrid();
			Session["SESSION_DS_ErroR"] = m_sdsERROR;
		}

		
		private void AddRowDgHcs()
		{
			HcsScanDAL.AddNewRowHcsDS(ref m_sdsHCS);
		}


		private void AddRowDgHcsError()
		{
			HcsScanDAL.AddNewRowHcsErrorDS(ref m_sdsERROR);
		}


		private void GetDgHcs()
		{
			ViewState["DPOperation"] = Operation.Update;

			AddRowDgHcs();

			RetreiveSelectedPage();

			if(m_sdsHCS.ds.Tables[0].Rows.Count == 0)
			{
				dgHcs.EditItemIndex = -1; 
				return;
			}

			Session["SESSION_DS_HcS"] = m_sdsHCS;
		}

		
		private void GetDgHcsError()
		{
			ViewState["DPOperation"] = Operation.Update;

			AddRowDgHcsError();

			RetreiveSelectedErrorPage();

			if(m_sdsERROR.ds.Tables[0].Rows.Count == 0)
			{
				dgError.EditItemIndex = -1; 
				return;
			}

			Session["SESSION_DS_ErroR"] = m_sdsERROR;
		}

		
		private void btnCallServer_Click(object sender, System.EventArgs e)
		{
			lblErrorMessage.Text = "";

			lblHcsNumber.Text = (string)ViewState["VIEWSTATE_STRING_HCSNO"];
			lblHcsDate.Text = (string)ViewState["VIEWSTATE_STRING_HCSDATE"];

			RefreshGrid();
		}


		private void txtViewDate_TextChanged(object sender, System.EventArgs e)
		{
			GetDataViewListBox();
		}


		private void btnClear_Click(object sender, System.EventArgs e)
		{
			ActionMode("CLEAR");
			ClearGrid();
		}


		private void btnEdit_Click(object sender, System.EventArgs e)
		{
			ActionMode("EDIT");	
			GetDataEditListBox();					
		}


		private void btnView_Click(object sender, System.EventArgs e)
		{
			txtViewDate.Text = "";
			lbViewData.Items.Clear();

			ActionMode("VIEW");
			ClearGrid();
		}

		
		private void btnNew_Click(object sender, System.EventArgs e)
		{
			string strLocation;

			strLocation = lblLocation.Text;

			if (strLocation == "BKK")
			{
				GetSubDcListBox();
				divSubDc.Style.Add("VISIBILITY", "visible");
			}
			else
			{
				CreateNewHcsNo();

				///Bind data in lbEditData
				GetDataEditListBox();
			}
		}


		private void btnDelete_Click(object sender, System.EventArgs e)
		{
			divSubDc.Style.Add("VISIBILITY", "hidden");

			string strHcsNo = (string)ViewState["VIEWSTATE_STRING_HCSNO"];

			if (strHcsNo != null && strHcsNo != "" && strHcsNo != " ")
			{
				if (dgHcs.Items.Count > 0)
				{
					lblErrorMessage.Text = "Cannot Delete HCS, there are consignmnet(s) in detail.";
				}
				else
				{
					if (HcsScanDAL.HcsHdDelete(m_strAppID, m_strEnterpriseID, strHcsNo) == 1)
					{
						lblErrorMessage.Text = "Delete HCS Number : " + strHcsNo + "  successfully.";
						RefreshGrid();
					}
					else
						lblErrorMessage.Text = "Delete HCS fail.";
				}
			}
			else
			{
				lblErrorMessage.Text = "Cannot Delete HCS.&nbsp;&nbsp;&nbsp Not Found Hcs Number.";	
			}

			///Bind data in lbEditData
			GetDataEditListBox();
		}


		private void btnClose_Click(object sender, System.EventArgs e)
		{
			divSubDc.Style.Add("VISIBILITY", "hidden");

			string strHcsNo = (string)ViewState["VIEWSTATE_STRING_HCSNO"];
			string strUserId = lblUserID.Text;
			string strLocation = lblLocation.Text;			

			if (strHcsNo != null && strHcsNo != "" && strHcsNo != " ")
			{
				if (m_sdsHCS.ds.Tables[0].Rows.Count > 0)
				{
					///check IsInvr for each row in dgHcs
					///
					string strChkInvr = "";

					foreach (DataRow dr in m_sdsHCS.ds.Tables[0].Rows)
					{
						if (!System.DBNull.Value.Equals(dr["isinvr"]) && (string)dr["isinvr"]=="Y")
                            if (System.DBNull.Value.Equals(dr["InvrRemark"]) || (string)dr["InvrRemark"]=="")
								strChkInvr += (string)dr["consignment_no"]+"; ";
					}

					if (strChkInvr != "")
					{
						lblErrorMessage.Text = strChkInvr + " must to entry Remark Data.";
						return;
					}
					else
					{
						string strRet = HcsScanDAL.HcsClosed(m_strAppID, m_strEnterpriseID, strHcsNo, strUserId, strLocation);
				
						if (strRet == "SUCCESSFULLY")
							lblErrorMessage.Text = "Closed HCS Number : " + strHcsNo + "  successfully.";
						else
							lblErrorMessage.Text = strRet;
					}					
				}
				else
				{
					lblErrorMessage.Text = "Cannot Close HCS.&nbsp;&nbsp;&nbsp Not Found Hcs Detail.";
				}
			}
			else
			{
				lblErrorMessage.Text = "Cannot Close HCS.&nbsp;&nbsp;&nbsp Not Found Hcs Number.";
			}

			///Bind data in lbEditData
			GetDataEditListBox();
		}

		
		private void btnCancel_Click(object sender, System.EventArgs e)
		{
			string strHcsNo = (string)ViewState["VIEWSTATE_STRING_HCSNO"];
			string strUserId = lblUserID.Text;
			string strLocation = lblLocation.Text;

			if (strHcsNo != null && strHcsNo != "" && strHcsNo != " ")
			{
				string strRet = HcsScanDAL.HcsCancelClosed(m_strAppID, m_strEnterpriseID, strHcsNo, strUserId, strLocation);
				if (strRet == "SUCCESSFULLY")
					lblErrorMessage.Text = "Cancel Closed HCS Number : " + strHcsNo + "  successfully.";
				else
					lblErrorMessage.Text = strRet;
			}
			else
			{
				lblErrorMessage.Text = "Cannot Cancel Close HCS.&nbsp;&nbsp;&nbsp Not Found Hcs Number.";
			}

			///Bind data in lbEditData
			GetDataViewListBox();
		}

		
		private void lbViewData_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			lbViewDataSelected();
		}


		private void lbEditData_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			divSubDc.Style.Add("VISIBILITY", "hidden");
			lbEditDataSelected();			
		}

		
		private void lbSubDc_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			lbSubDcSelected();
		}

		
		private void lbViewDataSelected()
		{
			DataSet ds = new DataSet();
			DataRow dr = null;
			string strHcsNo = "";

			strHcsNo = lbViewData.SelectedItem.Text;

			try
			{
				ds = HcsScanDAL.GetByNoHcsHdDS(m_strAppID, m_strEnterpriseID, strHcsNo, "Y");

				if (ds.Tables[0].Rows.Count > 0)
				{
					dr = ds.Tables[0].Rows[0];

					strHcsNo = (string)dr["hcs_no"];

					lblHcsNumber.Text = (string)dr["hcs_no"];
					lblHcsDate.Text = Convert.ToDateTime(dr["hcs_date"]).ToString("dd/MM/yyyy HH:mm:ss");

					ViewState["VIEWSTATE_STRING_HCSNO"] = strHcsNo;
					ViewState["VIEWSTATE_STRING_HCSDATE"] = Convert.ToDateTime(dr["hcs_date"]).ToString("dd/MM/yyyy HH:mm:ss");

					RefreshGrid();
				}
				else 
				{
					strHcsNo = "";

					lblHcsNumber.Text = "";
					lblHcsDate.Text = "";

					ViewState["VIEWSTATE_STRING_HCSNO"] = " ";
					ViewState["VIEWSTATE_STRING_HCSDATE"] = " ";

					ClearGrid();
				}	
			}
			catch(Exception ex)
			{
				lblErrorMessage.Text = ex.Message.ToString();
			}
			finally
			{
				ds.Dispose();
			}
		}

		
		private void lbEditDataSelected()
		{
			DataSet ds = new DataSet();
			DataRow dr = null;
			string strHcsNo = "";

			strHcsNo = lbEditData.SelectedItem.Text;

			try
			{
				ds = HcsScanDAL.GetByNoHcsHdDS(m_strAppID, m_strEnterpriseID, strHcsNo, "");

				if (ds.Tables[0].Rows.Count > 0)
				{
					dr = ds.Tables[0].Rows[0];

					strHcsNo = (string)dr["hcs_no"];

					lblHcsNumber.Text = (string)dr["hcs_no"];
					lblHcsDate.Text = Convert.ToDateTime(dr["hcs_date"]).ToString("dd/MM/yyyy HH:mm:ss");

					ViewState["VIEWSTATE_STRING_HCSNO"] = strHcsNo;
					ViewState["VIEWSTATE_STRING_HCSDATE"] = Convert.ToDateTime(dr["hcs_date"]).ToString("dd/MM/yyyy HH:mm:ss");

					RefreshGrid();
				}
				else
				{
					strHcsNo = "";

					lblHcsNumber.Text = "";
					lblHcsDate.Text = "";

					ViewState["VIEWSTATE_STRING_HCSNO"] = " ";
					ViewState["VIEWSTATE_STRING_HCSDATE"] = " ";

					ClearGrid();
				}	
			}
			catch(Exception ex)
			{
				lblErrorMessage.Text = ex.Message.ToString();
			}
			finally
			{
				ds.Dispose();
			}
		}
		
		
		private void GetDataEditListBox()
		{
			DataSet dsHcs = new DataSet();	
		
			string strLocation = "";

			/// check role ALLOWHCS
			if (getRole(lblUserID.Text, "ALLOWHCS") == 1)
                strLocation = "";
			else
                strLocation = lblLocation.Text;

			try
			{
				dsHcs = HcsScanDAL.GetAllOpenHcsHdDS(m_strAppID, m_strEnterpriseID,strLocation);

				if (dsHcs.Tables[0].Rows.Count > 0)
				{
					ArrayList arrData = new ArrayList();

					foreach (DataRow dRow in dsHcs.Tables[0].Rows)
					{
						arrData.Add((string)dRow["hcs_no"]);
					}

					lbEditData.Items.Clear();
					lbEditData.DataSource = arrData;
					lbEditData.DataBind();

					if (lbEditData.Items.Count == 1)
					{
						lbEditData.SelectedIndex = 0;
						lbEditDataSelected();
					}
					else
					{
						ViewState["VIEWSTATE_STRING_HCSNO"] = " ";
						ViewState["VIEWSTATE_STRING_HCSDATE"] = " ";

						lblHcsNumber.Text = " ";
						lblHcsDate.Text = " ";

						RefreshGrid();
					}
				}
				else
				{
					ViewState["VIEWSTATE_STRING_HCSNO"] = " ";
					ViewState["VIEWSTATE_STRING_HCSDATE"] = " ";

					lblHcsNumber.Text = " ";
					lblHcsDate.Text = " ";

					lbEditData.Items.Clear();
					RefreshGrid();
				}
			}
			catch(Exception ex)
			{
				lblErrorMessage.Text = ex.Message.ToString();
			} 
			finally
			{
				dsHcs.Dispose();
			}
		}


		private void GetDataViewListBox()
		{
			DataSet DS = null;
			string strViewDate = txtViewDate.Text;
			string strLocation = "";

			/// check role ALLOWHCS
			if (getRole(lblUserID.Text, "ALLOWHCS") == 1)
				strLocation = "";
			else
				strLocation = lblLocation.Text;

			try
			{
				DS = HcsScanDAL.GetByDateHcsHdDS(m_strAppID, m_strEnterpriseID, strViewDate, strLocation);

				if (DS.Tables[0].Rows.Count > 0)
				{
					ArrayList arrData = new ArrayList();

					foreach (DataRow dRow in DS.Tables[0].Rows)
					{
						arrData.Add((string)dRow["hcs_no"]);
					}

					lbViewData.Items.Clear();
					lbViewData.DataSource = arrData;
					lbViewData.DataBind();

					if (lbViewData.Items.Count == 1)
					{
						lbViewData.SelectedIndex = 0;
						lbViewDataSelected();
					}
					else
					{
						ViewState["VIEWSTATE_STRING_HCSNO"] = " ";
						ViewState["VIEWSTATE_STRING_HCSDATE"] = " ";

						lblHcsNumber.Text = " ";
						lblHcsDate.Text = " ";

						RefreshGrid();
					}
				}
				else
				{
					ViewState["VIEWSTATE_STRING_HCSNO"] = " ";
					ViewState["VIEWSTATE_STRING_HCSDATE"] = " ";

					lblHcsNumber.Text = " ";
					lblHcsDate.Text = " ";

					lbViewData.Items.Clear();
					RefreshGrid();
				}
			}
			catch(Exception ex)
			{
				lblErrorMessage.Text = ex.Message.ToString();
			} 
			finally
			{
				DS.Dispose();
			}
		}

		
		private void GetSubDcListBox()
		{
			DataSet DS = null;
			string strLoc = lblLocation.Text;

			try
			{
				DS = HcsScanDAL.GetSubDcByLoc(m_strAppID, m_strEnterpriseID, strLoc);

				if (DS.Tables[0].Rows.Count > 0)
				{
					ArrayList arrData = new ArrayList();

					foreach (DataRow dRow in DS.Tables[0].Rows)
					{
						arrData.Add((string)dRow["subdc_code"]);
					}

					lbSubDc.Items.Clear();
					lbSubDc.DataSource = arrData;
					lbSubDc.DataBind();
				}
				else
				{
					lbSubDc.Items.Clear();
				}
			}
			catch(Exception ex)
			{
				lblErrorMessage.Text = ex.Message.ToString() ;
			}
			finally
			{
				DS.Dispose();
			}			
		}

		
		private void lbSubDcSelected()
		{
			string strSubDc = "";

			strSubDc = lbSubDc.SelectedItem.Text;

			if (strSubDc != "")
			{
				lblSubDc.Text = strSubDc;
				
				CreateNewHcsNo();

				///Bind data in lbEditData
				GetDataEditListBox();

				divSubDc.Style.Add("VISIBILITY", "hidden");
			}
			else
			{
				lblSubDc.Text = " ";
			}			
		}
		
		
		private void CreateNewHcsNo()
		{
			string strHcsNumber;
			string strUserId;
			string strLocation;
			string strSubDc;
			string strYear;
			string strMonth;
			string strDay;
			string strTmp;

			strUserId = lblUserID.Text;
			strLocation = lblLocation.Text;
			strSubDc = lblSubDc.Text;			

			strYear = Convert.ToString((System.DateTime.Now).Year);

			strTmp = "0" + Convert.ToString((System.DateTime.Now).Month);
			strMonth = strTmp.Substring(strTmp.Length - 2);

			strTmp = "0" + Convert.ToString((System.DateTime.Now).Day);
			strDay = strTmp.Substring(strTmp.Length - 2);


			strHcsNumber = strLocation + strSubDc + strYear + strMonth + strDay;


			//Check Opening HCS No.
			if(HcsScanDAL.GetOpenHcsNumber(m_strAppID, m_strEnterpriseID, strLocation, strSubDc) > 0)
			{
				lblErrorMessage.Text = "Cannot create HCS Number. There is an opened HCS Number.";
			}
			else if (HcsScanDAL.HcsHdInsert(m_strAppID, m_strEnterpriseID, strHcsNumber, strUserId, strLocation, strSubDc) == 1)
			{
				lblErrorMessage.Text = "Create new HCS Number successfully.";
			}
			else
			{
				lblErrorMessage.Text = "Create new HCS Number fail.";
			}
		}



#region ExportXLS

		private void btnExport_Click(object sender, System.EventArgs e)
		{
			string strHcsNo = (string)ViewState["VIEWSTATE_STRING_HCSNO"];
			string strLocation = lblLocation.Text;

			if (strHcsNo != null && strHcsNo != "" && strHcsNo != " ")
			{

				DataSet dsHcs = new DataSet();
				DataTable dtExport = new DataTable();

				try 
				{
					dsHcs = HcsScanDAL.GetHcsReportDS(m_strAppID, m_strEnterpriseID, strHcsNo);				
	
					dtExport.Columns.Add("Row");
					dtExport.Columns.Add("Hcs_No");
					dtExport.Columns.Add("Consignment_No");
					dtExport.Columns.Add("Cust_Ref");
					dtExport.Columns.Add("PayerId");
					dtExport.Columns.Add("Last_Status_Datetime");
					dtExport.Columns.Add("ScanUserId");
					dtExport.Columns.Add("Org_Dc");
					dtExport.Columns.Add("Del_Route");
					dtExport.Columns.Add("Last_Status_Code");
					dtExport.Columns.Add("IsInvr");
					dtExport.Columns.Add("InvrRemark");

					if (dsHcs != null && dsHcs.Tables.Count > 0 && dsHcs.Tables[0] != null && dsHcs.Tables[0].Rows.Count > 0)
					{
						for (int i = 0; i < dsHcs.Tables[0].Rows.Count; i++) 
						{
							DataRow dr;
							dr = dtExport.NewRow();

							dr["Row"] = dsHcs.Tables[0].Rows[i]["Row"];
							dr["Hcs_No"] = dsHcs.Tables[0].Rows[i]["Hcs_No"];
							dr["Consignment_No"] = dsHcs.Tables[0].Rows[i]["Consignment_No"];				
							dr["Cust_Ref"] = dsHcs.Tables[0].Rows[i]["Cust_Ref"];
							dr["PayerId"] = dsHcs.Tables[0].Rows[i]["PayerId"];
							dr["Last_Status_Datetime"] = dsHcs.Tables[0].Rows[i]["Last_Status_Datetime"];
							dr["ScanUserId"] = dsHcs.Tables[0].Rows[i]["ScanUserId"];
							dr["Org_Dc"] = dsHcs.Tables[0].Rows[i]["Org_Dc"];
							dr["Del_Route"] = dsHcs.Tables[0].Rows[i]["Del_Route"];
							dr["Last_Status_Code"] = dsHcs.Tables[0].Rows[i]["Last_Status_Code"];						
							dr["IsInvr"] = dsHcs.Tables[0].Rows[i]["IsInvr"];
							dr["InvrRemark"] = dsHcs.Tables[0].Rows[i]["InvrRemark"];
											
							dtExport.Rows.Add(dr);
						}
					}
					else
					{
						DataRow dr;
						dr = dtExport.NewRow();

						dr["Row"] = " ";
						dr["Hcs_No"] = " ";
						dr["Consignment_No"] = " ";				
						dr["Cust_Ref"] = " ";
						dr["PayerId"] = " ";
						dr["Last_Status_Datetime"] = " ";
						dr["ScanUserId"] = " ";
						dr["Org_Dc"] = " ";
						dr["Del_Route"] = " ";
						dr["Last_Status_Code"] = " ";
						dr["IsInvr"] = " ";
						dr["InvrRemark"] = " ";
						
						dtExport.Rows.Add(dr);
					}	

					ExportData(dtExport);
				}
				catch(Exception ex)
				{
					lblErrorMessage.Text = ex.Message.ToString();
				} 
				finally
				{
					dsHcs.Dispose();
					dtExport.Dispose();
				}
			}
			else
			{
				lblErrorMessage.Text = "Cannot Export XLS.&nbsp;&nbsp;&nbsp Not Found Hcs Number.";
			}
			
			
		}	

		
		private void ExportData(DataTable dtSource)
		{
			DataTable dtHcs = new DataTable();
			string Res = "";			
 
			try
			{
				dtHcs = dtSource;

				if ((dtHcs != null) && (dtHcs.Columns.Count > 0) && (dtHcs.Rows.Count > 0))
				{
					TIESClasses.CreateXls Xls = new TIESClasses.CreateXls();

					// Set type column
					string[] ColumnType = new string[dtHcs.Columns.Count];
					ColumnType[1] = Xls.TypeText; 

					Res = Xls.createXlsConsign(dtHcs, GetPathExport(), ColumnType);

					if (Res == "0")
					{	
						GetDownloadFile(Res);
				
						// Kill Process after save
						Xls.KillProcesses("EXCEL");
					} 
					else
					{
						lblErrorMessage.Text = Res;
					}
				}
			}
			catch(Exception ex)
			{
				lblErrorMessage.Text = ex.Message.ToString() ;
			}
			finally
			{
				dtHcs.Dispose();
				dtSource.Dispose();
			}
		}

		
		private string  GetPathExport()
		{
			FileName = Session["userID"].ToString().ToUpper() + "_Hcs_" + String.Format("{0:yyyyMMdd}", DateTime.Now);
			string mapFile = Server.MapPath("..\\WebUI\\Export") + "\\" + FileName + ".xls";
			return mapFile;
		}


		private void GetDownloadFile(string Res)
		{
			if (Res == "0")
			{
				FileName = Session["userID"].ToString().ToUpper() + "_Hcs_" + String.Format("{0:yyyyMMdd}", DateTime.Now);
				Response.ContentType="application/ms-excel";
				Response.AddHeader( "content-disposition","attachment; filename=" + FileName + ".xls");
				FileStream fs = new FileStream(GetPathExport(),FileMode.Open);
				long FileSize;
				FileSize = fs.Length;
				byte[] getContent = new byte[(int)FileSize];
				fs.Read(getContent, 0, (int)fs.Length);
				fs.Close();

				Response.BinaryWrite(getContent);
			} 
		}

		
#endregion ExportXLS


#region PrintPdf

		private void btnPrint_Click(object sender, System.EventArgs e)
		{	
			string strAppId = m_strAppID;
			string strEntId = m_strEnterpriseID;
			string strHcsNo = (string)ViewState["VIEWSTATE_STRING_HCSNO"];
			string strUserId = lblUserID.Text;
			string strLocation = lblLocation.Text;

			if (strHcsNo != null && strHcsNo != "" && strHcsNo != " ")
			{
				DataSet dsHcs = new DataSet();
				dsHcs = HcsScanDAL.GetHcsReportDS(strAppId, strEntId, strHcsNo);
			
				DataTable dtReport = new DataTable();
						
				dtReport.Columns.Add("Row");
//				dtReport.Columns.Add("Hcs No");
				dtReport.Columns.Add("Consignment_No");
				dtReport.Columns.Add("Cust_Ref");
				dtReport.Columns.Add("Payer_ID");
				dtReport.Columns.Add("Last_Status_Datetime");
				dtReport.Columns.Add("Scan_User_ID");
				dtReport.Columns.Add("Org_Dc");
				dtReport.Columns.Add("Del_Route");
				dtReport.Columns.Add("Last_Status_Code");
				dtReport.Columns.Add("Invr");
				dtReport.Columns.Add("Invr_Remark");

				if (dsHcs != null && dsHcs.Tables.Count > 0 && dsHcs.Tables[0] != null && dsHcs.Tables[0].Rows.Count > 0)
				{
					for (int i = 0; i < dsHcs.Tables[0].Rows.Count; i++) 
					{
						DataRow dr;
						dr = dtReport.NewRow();

						dr["Row"] = dsHcs.Tables[0].Rows[i]["Row"];
//						dr["Hcs_No"] = dsHcs.Tables[0].Rows[i]["Hcs_No"];
						dr["Consignment_No"] = dsHcs.Tables[0].Rows[i]["Consignment_No"];				
						dr["Cust_Ref"] = dsHcs.Tables[0].Rows[i]["Cust_Ref"];
						dr["Payer_ID"] = dsHcs.Tables[0].Rows[i]["PayerId"];
						dr["Last_Status_Datetime"] = dsHcs.Tables[0].Rows[i]["Last_Status_Datetime"];
						dr["Scan_User_ID"] = dsHcs.Tables[0].Rows[i]["ScanUserId"];
						dr["Org_Dc"] = dsHcs.Tables[0].Rows[i]["Org_Dc"];
						dr["Del_Route"] = dsHcs.Tables[0].Rows[i]["Del_Route"];
						dr["Last_Status_Code"] = dsHcs.Tables[0].Rows[i]["Last_Status_Code"];						
						dr["Invr"] = dsHcs.Tables[0].Rows[i]["IsInvr"];
						dr["Invr_Remark"] = dsHcs.Tables[0].Rows[i]["InvrRemark"];
											
						dtReport.Rows.Add(dr);
					}
				}
				else
				{
					DataRow dr;
					dr = dtReport.NewRow();

					dr["Row"] = " ";
//					dr["Hcs_No"] = " ";
					dr["Consignment_No"] = " ";				
					dr["Cust_Ref"] = " ";
					dr["Payer_ID"] = " ";
					dr["Last_Status_Datetime"] = " ";
					dr["Scan_User_ID"] = " ";
					dr["Org_Dc"] = " ";
					dr["Del_Route"] = " ";
					dr["Last_Status_Code"] = " ";
					dr["Invr"] = " ";
					dr["Invr_Remark"] = " ";
						
					dtReport.Rows.Add(dr);
				}		

				GeneratePDF(strHcsNo, strUserId, strLocation, dtReport);

			}
			else
			{
				lblErrorMessage.Text = "Cannot Print PDF.&nbsp;&nbsp;&nbsp Not Found Hcs Number.";
			}
		}



		private void GeneratePDF(string strHcsNo, string strUserId, string strLocation, DataTable dataTable)
		{
			Document pdfDoc = new Document(iTextSharp.text.PageSize.LETTER, 10, 10, 30, 20);

			System.IO.MemoryStream mStream = new System.IO.MemoryStream();
			PdfWriter writer = PdfWriter.GetInstance(pdfDoc, mStream);
			Phrase phrase = new iTextSharp.text.Phrase(DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"), new iTextSharp.text.Font(iTextSharp.text.Font.COURIER, 5));

			pdfDoc.Open();

			iTextSharp.text.Rectangle page = pdfDoc.PageSize;

			PdfPTable head = new PdfPTable(1);
			head.TotalWidth = page.Width;

			PdfPCell c = new PdfPCell(phrase);              

			c.Border = iTextSharp.text.Rectangle.NO_BORDER;
			c.VerticalAlignment = Element.ALIGN_TOP;
			c.HorizontalAlignment = Element.ALIGN_CENTER;

			head.AddCell(c);
			head.WriteSelectedRows(
				/// first/last row; -1 writes all rows
				0, -1,
				/// left offset
				0,
				/// ** bottom** yPos of the table
				page.Height - pdfDoc.TopMargin + head.TotalHeight + 20,
				writer.DirectContent);


			/// add image to document
			iTextSharp.text.Image logo = iTextSharp.text.Image.GetInstance(Request.MapPath("../WebUI/images/KL_logo.jpg")); 

			logo.Alignment = iTextSharp.text.Image.ALIGN_LEFT;
			logo.ScalePercent(30f);
			pdfDoc.Add(logo);


			/// report "title"
//			int cols = dataTable.Columns.Count;
			int rows = dataTable.Rows.Count;

			string sTitle = "HCS Cover Sheet \n";

			Chunk chunk = new Chunk(sTitle,FontFactory.GetFont("Verdana", 15));
			Paragraph p = new Paragraph();
			p.Alignment = Element.ALIGN_CENTER;
			p.Add(chunk);

			string sHeader1 = "";
			sHeader1 = "HCS Number: " + strHcsNo + " ,   Total Rows: " + rows;
			sHeader1 += "                                                                      ";
			sHeader1 += "                                                                      ";
			sHeader1 += "By: " + strUserId + "  Location: " + strLocation; //+ " \n ";

			Chunk chunk1 = new Chunk(sHeader1,FontFactory.GetFont("Verdana", 8));
			Paragraph p1 = new Paragraph();
			p1.Alignment = Element.ALIGN_LEFT;
			p1.Add(chunk1);
			
			pdfDoc.Add(p);
			pdfDoc.Add(p1);

			/// add tabular data
			pdfDoc.Add( GenerateTable(dataTable) );

			pdfDoc.Close();

			///Open in new popup window
			Response.ContentType = "application/octet-stream";
			Response.AddHeader("Content-Disposition", "attachment; filename=ReportPdf.pdf");
			Response.Clear();
			Response.BinaryWrite(mStream.ToArray());
			Response.End();
		}



		private iTextSharp.text.Table GenerateTable(DataTable myDataTable)
		{
			int cols = myDataTable.Columns.Count;
			int rows = myDataTable.Rows.Count;

			///Header ----------------------- start
			iTextSharp.text.Table iTable = new iTextSharp.text.Table(11);
			iTable.SetWidths(new int[11] {4, 10, 10, 8, 15, 8, 5, 6, 10, 4, 20});

			// set table style properties
			iTable.BorderWidth = 1;
			iTable.BorderColor = new iTextSharp.text.Color(0, 0, 255);
			iTable.Padding = 2;
			iTable.Width = 100;

			//creating table headers
			for (int i = 0; i < cols; i++)
			{
				Cell cellCols = new Cell();
				iTextSharp.text.Font ColFont = FontFactory.GetFont(FontFactory.HELVETICA, 6, iTextSharp.text.Font.BOLD);
				cellCols.Header = true;
				cellCols.BackgroundColor = new iTextSharp.text.Color(204, 204, 204);
				Chunk chunkCols = new Chunk(myDataTable.Columns[i].ColumnName, ColFont);
				cellCols.Add(chunkCols);
				iTable.AddCell(cellCols);
			}

			iTable.EndHeaders();
			///Header ----------------------- end


			///Detail ----------------------- start	
			///
			//creating table data (actual result)
			for (int k = 0; k < rows; k++)
			{
				for (int j = 0; j < cols; j++)
				{
					Cell cellRows = new Cell();
					iTextSharp.text.Font RowFont = FontFactory.GetFont(FontFactory.HELVETICA, 6);
					Chunk chunkRows = new Chunk(myDataTable.Rows[k][j].ToString(), RowFont);
					cellRows.Add(chunkRows);
					iTable.AddCell(cellRows);
				}
			}
			///Detail ----------------------- end

			return iTable;
		}	





#endregion PrintPdf


		private void btnSearch_Click(object sender, System.EventArgs e)
		{
			ActionMode("SEARCH");
			ClearGrid();
			ClearSearch();
		}


		private void btnSearchCons_Click(object sender, System.EventArgs e)
		{
			DataSet DS = null;
			string strCons = txtSearchCons.Text;

			try
			{

				DS = HcsScanDAL.GetByConsHcsHdDS(m_strAppID, m_strEnterpriseID, strCons);

				if (DS.Tables[0].Rows.Count > 0)
				{
					DataRow dr = DS.Tables[0].Rows[0];

					lblSResult.Text = "Found";

					lblSHcsNo.Text = (string)dr["hcs_no"];
					lblSHcsDate.Text = Convert.ToDateTime(dr["hcs_date"]).ToString("dd/MM/yyyy HH:mm:ss");
					lblSCreatedBy.Text = (string)dr["crt_userid"];
					lblSCreatedDate.Text = Convert.ToDateTime(dr["crt_datetime"]).ToString("dd/MM/yyyy HH:mm:ss");
					lblSHcsStatus.Text = dr["isclosed"]==System.DBNull.Value?"Opened":"Closed";

				}
				else 
				{
					lblSResult.Text = "Not Found";

					lblSHcsNo.Text = "";
					lblSHcsDate.Text = "";
					lblSCreatedBy.Text = "";
					lblSCreatedDate.Text = "";
					lblSHcsStatus.Text = "";
				}
			}
			catch(Exception ex)
			{
				lblErrorMessage.Text = ex.Message.ToString();
			} 
			finally
			{
				DS.Dispose();
			}
		}


		private void ClearSearch()
		{
			txtSearchCons.Text = "";
			lblSResult.Text = "";
			lblSHcsNo.Text = "";
			lblSHcsDate.Text = "";
			lblSHcsStatus.Text = "";
			lblSCreatedBy.Text = "";
			lblSCreatedDate.Text = "";
		}


		private int getRole(string strUserId, string strRole)
		{
			User user = new User();
			user.UserID = strUserId;
			userRoleArray = com.common.RBAC.RBACManager.GetAllRoles(m_strAppID, m_strEnterpriseID, user);
			Role role;

			for(int i=0;i<userRoleArray.Count;i++)
			{
				role = (Role)userRoleArray[i];				
				if (role.RoleName.ToUpper().Trim() == strRole) 
					return 1;		
			}

			return 0;
		}

		
		private void btnScan_Click(object sender, System.EventArgs e)
		{		
			txtGridMode.Text = "SCAN";
			ViewState["VIEWSTATE_STRING_TXTGRIDMODE"] = "SCAN";
		}	

		
		private void btnStop_Click(object sender, System.EventArgs e)
		{
			txtGridMode.Text = "";
			ViewState["VIEWSTATE_STRING_TXTGRIDMODE"] = "";			
		}
		


		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnCallServer.Click += new System.EventHandler(this.btnCallServer_Click);
			this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
			this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
			this.btnNew.Click += new System.EventHandler(this.btnNew_Click);
			this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
			this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
			this.btnView.Click += new System.EventHandler(this.btnView_Click);
			this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
			this.btnExport.Click += new System.EventHandler(this.btnExport_Click);
			this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
			this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
			this.btnScan.Click += new System.EventHandler(this.btnScan_Click);
			this.btnStop.Click += new System.EventHandler(this.btnStop_Click);
			this.lbEditData.SelectedIndexChanged += new System.EventHandler(this.lbEditData_SelectedIndexChanged);
			this.txtViewDate.TextChanged += new System.EventHandler(this.txtViewDate_TextChanged);
			this.lbViewData.SelectedIndexChanged += new System.EventHandler(this.lbViewData_SelectedIndexChanged);
			this.btnSearchCons.Click += new System.EventHandler(this.btnSearchCons_Click);
			this.lbSubDc.SelectedIndexChanged += new System.EventHandler(this.lbSubDc_SelectedIndexChanged);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion


	}
}
