using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.common.util;
using com.common.classes;
using com.common.DAL;
using System.Text;
using com.common.applicationpages;
namespace TIES.WebUI
{
	/// <summary>
	/// Summary description for StatusCodePopup.
	/// </summary>
	public class StatusCodePopup : BasePopupPage
	{
		protected System.Web.UI.WebControls.Button btnSearch;
		protected System.Web.UI.WebControls.TextBox txtStatusSrch;
		protected System.Web.UI.WebControls.Label Label1;
		protected System.Web.UI.WebControls.Label Label2;
		protected System.Web.UI.WebControls.Label Label3;
		protected System.Web.UI.WebControls.Label Label4;
		protected System.Web.UI.WebControls.TextBox txtDescSrch;
		protected System.Web.UI.WebControls.DataGrid m_dgStatusCode;
		//Utility utility = null;
		private String appID = null;
		private String enterpriseID = null;
		private String strStatusCode = null;
		//private String strExceptionCode = null;
		protected System.Web.UI.WebControls.Label Label5;
		private SessionDS m_sdsStatusCode = null;
		protected System.Web.UI.WebControls.Label Label6;
		protected System.Web.UI.WebControls.DropDownList ddlStatusType;
		protected System.Web.UI.WebControls.DropDownList ddlCloseSrch;
		protected System.Web.UI.WebControls.DropDownList ddlInvoiceSrch;
		//protected System.Web.UI.WebControls.TextBox txtExceptionSrch;
		
		private DataView m_dvStatusTypeOptions;
		private DataView m_dvStatusCloseOptions;
		private DataView m_dvInvoiceableOptions;
		private String m_strAppID, m_strEnterpriseID, m_strCulture;
		protected System.Web.UI.WebControls.Button btnClose;
		protected System.Web.UI.WebControls.Label Label7;

		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			//Utility utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings,Page.Session);
			m_strAppID = utility.GetAppID();
			m_strEnterpriseID = utility.GetEnterpriseID();
			m_strCulture = utility.GetUserCulture();
			if(!Page.IsPostBack)
			{
				txtStatusSrch.Text = Request.Params["STATUSCODE"];							
				LoadComboLists();
				BindComboLists();
				// To Select the DropDownList Item by default ****
				ListItem tempItem = ddlStatusType.Items.FindByValue(Request.Params["STATUSTYPE"]); // there's also a FindByValue method.
				int  index = ddlStatusType.Items.IndexOf(tempItem);
				ddlStatusType.SelectedIndex = index;

				ddlStatusType.SelectedItem.Value = (String) Request.Params["STATUSTYPE"];
				RefreshData();
				BindStatusCode();
				getPageControls(Page);
				ViewState["STATUS_DS"] = m_sdsStatusCode;
			}
			else
			{
				m_sdsStatusCode = (SessionDS)ViewState["STATUS_DS"];
			}
		}
		private void RefreshData()
		{
			DbConnection dbConApp = null;
			
			utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings,Page.Session);
			appID = utility.GetAppID();
			enterpriseID = utility.GetEnterpriseID();

			dbConApp = DbConnectionManager.GetInstance().GetDbConnection(appID,enterpriseID);

			if(dbConApp == null)
			{
				Logger.LogTraceError("StatusCodePopup.aspx.cs","RefreshData","StatusCodePopupl001","dbConApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			StringBuilder strQry = new StringBuilder();
			String strFormID = Request.Params["FORMID"];
			if(strFormID.Equals("ShipmentUpdate"))
			{
				strQry.Append("select * from (");
				strQry.Append( "select distinct status_code,status_description,Status_type,status_close,invoiceable,system_code,tt_display_status,tt_display_remarks,auto_process,auto_location,auto_remark,tt_display_location, scanning_type ,AllowRoles, isnull(AllowInSWB_Emu,'N') as AllowInSWB_Emu, isnull(AllowUPD_ByBatch,'N') as AllowUPD_ByBatch from Status_Code ");
				strQry.Append(" inner join  ( SELECT   distinct  Role_Master.role_name fROM  User_Role_Relation INNER JOIN Role_Master ON User_Role_Relation.applicationid = Role_Master.applicationid AND User_Role_Relation.enterpriseid = Role_Master.enterpriseid AND ");
				strQry.Append(" User_Role_Relation.roleid = Role_Master.roleid WHERE  (User_Role_Relation.userid = '"+ utility.GetUserID() +"')) a on   status_code.allowRoles like '%' + a.role_name + '%' ");
				strQry.Append( " where status_code.applicationid = '"+appID+"' and status_code.enterpriseid = '"+enterpriseID+"' ");
				strQry.Append( " union ");
				strQry.Append( " select status_code,status_description,Status_type,status_close,invoiceable, ");
				strQry.Append( " system_code,tt_display_status,tt_display_remarks,auto_process,auto_location,auto_remark, ");
				strQry.Append( " tt_display_location, scanning_type ,AllowRoles, isnull(AllowInSWB_Emu,'N') as AllowInSWB_Emu, ");
				strQry.Append( " isnull(AllowUPD_ByBatch,'N') as AllowUPD_ByBatch from Status_Code where (AllowRoles is null or Rtrim(Ltrim(AllowRoles))='') ");
				strQry.Append(") a where 1=1");
			}
			else
			{
//				strQry.Append("select status_code,status_description,status_type,status_close,invoiceable from Status_Code where applicationid='");
//				strQry.Append(appID);
//				strQry.Append("' and enterpriseid = '");
//				strQry.Append(enterpriseID+"'");
				//Jeab 22 Mar 2012
				strQry.Append("select status_code,status_description,Status_type,status_close,invoiceable from (");
				strQry.Append( "select distinct status_code,status_description,Status_type,status_close,invoiceable,system_code,tt_display_status,tt_display_remarks,auto_process,auto_location,auto_remark,tt_display_location, scanning_type ,AllowRoles, isnull(AllowInSWB_Emu,'N') as AllowInSWB_Emu, isnull(AllowUPD_ByBatch,'N') as AllowUPD_ByBatch from Status_Code ");
				strQry.Append(" inner join  ( SELECT   distinct  Role_Master.role_name fROM  User_Role_Relation INNER JOIN Role_Master ON User_Role_Relation.applicationid = Role_Master.applicationid AND User_Role_Relation.enterpriseid = Role_Master.enterpriseid AND ");
				strQry.Append(" User_Role_Relation.roleid = Role_Master.roleid WHERE  (User_Role_Relation.userid = '"+ utility.GetUserID() +"')) a on   status_code.allowRoles like '%' + a.role_name + '%' ");
				strQry.Append( " where status_code.applicationid = '"+appID+"' and status_code.enterpriseid = '"+enterpriseID+"' ");
				strQry.Append( " union ");
				strQry.Append( " select status_code,status_description,Status_type,status_close,invoiceable, ");
				strQry.Append( " system_code,tt_display_status,tt_display_remarks,auto_process,auto_location,auto_remark, ");
				strQry.Append( " tt_display_location, scanning_type ,AllowRoles, isnull(AllowInSWB_Emu,'N') as AllowInSWB_Emu, ");
				strQry.Append( " isnull(AllowUPD_ByBatch,'N') as AllowUPD_ByBatch from Status_Code where (AllowRoles is null or Rtrim(Ltrim(AllowRoles))='') ");
				strQry.Append(") a where 1=1");
				//Jeab 22 Mar 2012  =========> End
			}
			

			if(txtStatusSrch.Text.ToString()!="")
			{
				strQry.Append(" and status_code like '%");
				strQry.Append(Utility.ReplaceSingleQuote(txtStatusSrch.Text.ToString())+"%'");
			}

			if(txtDescSrch.Text.ToString()!="")
			{
				strQry.Append(" and status_description like N'%");
				strQry.Append(Utility.ReplaceSingleQuote(txtDescSrch.Text.ToString())+"%'");
			}
			if(ddlStatusType.SelectedItem.Value!="")
			{
				strQry.Append(" and status_type in ('");
				strQry.Append(ddlStatusType.SelectedItem.Value+"','B')");
			}
			if(ddlCloseSrch.SelectedItem.Value!="")
			{
				strQry.Append(" and status_close like '");
				strQry.Append(ddlCloseSrch.SelectedItem.Value+"'");
			}
			if(ddlInvoiceSrch.SelectedItem.Value!="")
			{
				strQry.Append(" and invoiceable like '");
				strQry.Append(ddlInvoiceSrch.SelectedItem.Value+"'");
			}			
			String strQuery = strQry.ToString();

			IDbCommand idbCom = dbConApp.CreateCommand(strQuery, CommandType.Text);
			try
			{
				int iStartIndex = m_dgStatusCode.CurrentPageIndex * m_dgStatusCode.PageSize;
				m_sdsStatusCode = dbConApp.ExecuteQuery(idbCom, iStartIndex,m_dgStatusCode.PageSize,"Status_Code");
			}
			catch(ApplicationException appExpection)
			{
				Logger.LogTraceError("StatusCodePopup.aspx.cs","RefreshData","Statel002","Error in the query String");
				throw appExpection;
			}
			ViewState["STATUS_DS"] = m_sdsStatusCode;
		}
		private void BindStatusCode()
		{
			m_dgStatusCode.VirtualItemCount = System.Convert.ToInt32(m_sdsStatusCode.QueryResultMaxSize);
			m_dgStatusCode.DataSource = m_sdsStatusCode.ds;
			m_dgStatusCode.DataBind();
			ViewState["STATUS_DS"] = m_sdsStatusCode;
		}
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			base.OnInit(e);
			InitializeComponent();
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
			this.m_dgStatusCode.SelectedIndexChanged += new System.EventHandler(this.m_dgStatusCode_SelectedIndexChanged);
			this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
		public void OnPaging_Dispatch(Object sender, DataGridPageChangedEventArgs e)
		{
			m_dgStatusCode.CurrentPageIndex = e.NewPageIndex;
			RefreshData();
			BindStatusCode();
		}

		private void btnSearch_Click(object sender, System.EventArgs e)
		{
			m_dgStatusCode.CurrentPageIndex=0;
			RefreshData();
			BindStatusCode();
			getPageControls(Page);
		}

		private void m_dgStatusCode_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			String strFormID = Request.Params["FORMID"];
			int iSelIndex = m_dgStatusCode.SelectedIndex;
			DataGridItem dgRow = m_dgStatusCode.Items[iSelIndex];
			strStatusCode = dgRow.Cells[0].Text;
			String strStatusDescription = dgRow.Cells[1].Text;
			if(strStatusDescription == "&nbsp;")
			{
				strStatusDescription = "";
			}
			String strExcpDescription = dgRow.Cells[3].Text;
			if(strExcpDescription == "&nbsp;")
			{
				strExcpDescription = "";
			}
			DataRow dr = m_sdsStatusCode.ds.Tables[0].Rows[iSelIndex];			
			windowClose(strFormID, "txtStatus", "txtException",strStatusDescription,strExcpDescription);
			
		}
		private void windowClose(String formID, String strStatusTextBoxID, String strExceptTextBoxID,String strStatusDescription,String strExcpDescription)
		{
			String sScript = "";
			sScript += "<script language=javascript>";
			if(formID.Equals("ShipmentUpdate") || (formID.Equals("ShipmentUpdateRoute")))
			{
				sScript += "  window.opener."+formID+".txtStatusCode.value =\""+strStatusCode+"\";" ;				
				sScript += "  window.opener."+formID+".txtStatusDesc.value =\""+strStatusDescription+"\";" ;				
				if(formID.Equals("ShipmentUpdate"))
				{
					if(Request.Params["STATUSTYPE"].ToString()!="P")
					{
						if (strStatusCode=="SIP"||strStatusCode=="SOP" || strStatusCode=="UNSIP" || strStatusCode=="UNSOP"|| strStatusCode=="CLS"||strStatusCode=="UNCLS")
						{
							sScript += "  window.opener."+formID+".btnScanComplete.disabled =false;" ;
							sScript += "  window.opener."+formID+".btnGridInsertRow.disabled =true;" ;
						}
						else if(strStatusCode!="")
						{
							sScript += "  window.opener."+formID+".btnScanComplete.disabled =true;" ;
							sScript += "  window.opener."+formID+".btnGridInsertRow.disabled =false;" ;
						}
					}
					else
					{
						sScript += "  window.opener."+formID+".btnScanComplete.disabled =true;" ;
						sScript += "  window.opener."+formID+".btnGridInsertRow.disabled =false;" ;
					}
					sScript += "  window.opener."+formID+".btnClearHide.click();" ;
				}
			}
			else if(formID.Equals("ShipmentTrackingQuery"))
			{
				sScript += "  window.opener."+formID+".txtStatusCode.value =\""+strStatusCode+"\";" ;				
			}
			else if(formID.Equals("ShipmentTrackingQuery_New"))
			{
				sScript += "  window.opener."+formID+".txtStatusCode.value =\""+strStatusCode+"\";" ;				
			}
			else if(formID.Equals("ShipmentTrackingQueryRpt"))
			{
				sScript += "  window.opener."+formID+".txtStatusCode.value =\""+strStatusCode+"\";" ;				
			}
			else if(formID.Equals("ShipmentExceptionReport"))
			{
				sScript += "  window.opener."+formID+".txtStatus.value = \""+strStatusCode+"\";" ;				
			}
			else
			{
				sScript += "  window.opener."+formID+"."+strStatusTextBoxID+".value = \""+strStatusCode+"\";" ;				
			}
			sScript += "  window.close();";
			sScript += "</script>";
			Response.Write(sScript);
		}
		
		private DataView GetStatusTypeOptions(bool showNilOption)
		{
			DataTable dtStatusTypeOptions = new DataTable();
			
			dtStatusTypeOptions.Columns.Add(new DataColumn("Text", typeof(string)));
			dtStatusTypeOptions.Columns.Add(new DataColumn("StringValue", typeof(string)));

			ArrayList statusTypeOptionArray = Utility.GetCodeValues(m_strAppID,m_strCulture,"status_type",CodeValueType.StringValue);
			if(showNilOption)
			{
				DataRow drNilRow = dtStatusTypeOptions.NewRow();
				drNilRow[0] = "";
				drNilRow[1] = "";
				dtStatusTypeOptions.Rows.Add(drNilRow);
			}
			foreach(SystemCode statusTypeSysCode in statusTypeOptionArray)
			{
				DataRow drEach = dtStatusTypeOptions.NewRow();
				drEach[0] = statusTypeSysCode.Text;
				drEach[1] = statusTypeSysCode.StringValue;
				dtStatusTypeOptions.Rows.Add(drEach);
			}

			DataView dvStatusTypeOptions = new DataView(dtStatusTypeOptions);
			return dvStatusTypeOptions;

		}

		private DataView GetStatusCloseOptions(bool showNilOption)
		{
			DataTable dtStatusCloseOptions = new DataTable();
			
			dtStatusCloseOptions.Columns.Add(new DataColumn("Text", typeof(string)));
			dtStatusCloseOptions.Columns.Add(new DataColumn("StringValue", typeof(string)));

			ArrayList statusCloseOptionArray = Utility.GetCodeValues(m_strAppID,m_strCulture,"close_status",CodeValueType.StringValue);


			if(showNilOption)
			{
				DataRow drNilRow = dtStatusCloseOptions.NewRow();
				drNilRow[0] = "";
				drNilRow[1] = "";
				dtStatusCloseOptions.Rows.Add(drNilRow);
			}
			foreach(SystemCode statusCloseSysCode in statusCloseOptionArray)
			{
				DataRow drEach = dtStatusCloseOptions.NewRow();
				drEach[0] = statusCloseSysCode.Text;
				drEach[1] = statusCloseSysCode.StringValue;
				dtStatusCloseOptions.Rows.Add(drEach);
			}

			DataView dvStatusCloseOptions = new DataView(dtStatusCloseOptions);
			return dvStatusCloseOptions;

		}


		private DataView GetInvoiceableOptions(bool showNilOption)
		{
			DataTable dtInvoiceableOptions = new DataTable();
			
			dtInvoiceableOptions.Columns.Add(new DataColumn("Text", typeof(string)));
			dtInvoiceableOptions.Columns.Add(new DataColumn("StringValue", typeof(string)));

			ArrayList invoiceableOptionArray = Utility.GetCodeValues(m_strAppID,m_strCulture,"invoiceable",CodeValueType.StringValue);


			if(showNilOption)
			{
				DataRow drNilRow = dtInvoiceableOptions.NewRow();
				drNilRow[0] = "";
				drNilRow[1] = "";
				dtInvoiceableOptions.Rows.Add(drNilRow);
			}
	


			foreach(SystemCode invoiceableSysCode in invoiceableOptionArray)
			{
				DataRow drEach = dtInvoiceableOptions.NewRow();
				drEach[0] = invoiceableSysCode.Text;
				drEach[1] = invoiceableSysCode.StringValue;
				dtInvoiceableOptions.Rows.Add(drEach);
			}

			DataView dvInvoiceableOptions = new DataView(dtInvoiceableOptions);
			return dvInvoiceableOptions;

		}

		private void LoadComboLists()
		{
			m_dvStatusTypeOptions = GetStatusTypeOptions(true);
			m_dvStatusCloseOptions = GetStatusCloseOptions(true);
			m_dvInvoiceableOptions = GetInvoiceableOptions(true);
		}

		private void BindComboLists()
		{
			ddlStatusType.DataSource = (ICollection)m_dvStatusTypeOptions;
			ddlStatusType.DataTextField = "Text";
			ddlStatusType.DataValueField = "StringValue";
			ddlStatusType.DataBind();

			ddlCloseSrch.DataSource = (ICollection)m_dvStatusCloseOptions;
			ddlCloseSrch.DataTextField = "Text";
			ddlCloseSrch.DataValueField = "StringValue";
			ddlCloseSrch.DataBind();

			ddlInvoiceSrch.DataSource = (ICollection)m_dvInvoiceableOptions;
			ddlInvoiceSrch.DataTextField = "Text";
			ddlInvoiceSrch.DataValueField = "StringValue";
			ddlInvoiceSrch.DataBind();
	
		}

		private void btnClose_Click(object sender, System.EventArgs e)
		{
			String sScript = "";
			sScript += "<script language=javascript>";
			sScript += "  window.close();";
			sScript += "</script>";
			Response.Write(sScript);
		}

	}
}
