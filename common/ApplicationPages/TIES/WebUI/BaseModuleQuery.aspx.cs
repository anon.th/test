using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.common.classes;
using com.common.DAL;
using com.common.RBAC;
using com.ties.DAL;
using com.common.util;
using com.ties.classes;
using com.common.applicationpages;
using System.Globalization; 
namespace com.ties
{
	/// <summary>
	/// Summary description for BaseModuleQuery.
	/// </summary>
	public class BaseModuleQuery : BasePage 
	{
		protected System.Web.UI.WebControls.Button btnQuery;
		protected System.Web.UI.WebControls.Button btnGenerate;
		protected System.Web.UI.WebControls.Label lblErrorMessage;
		protected System.Web.UI.WebControls.RadioButton rbMonth;
		protected System.Web.UI.WebControls.DropDownList ddMonth;
		protected System.Web.UI.WebControls.Label lblYear;
		protected com.common.util.msTextBox txtYear;
		protected System.Web.UI.WebControls.RadioButton rbPeriod;
		protected com.common.util.msTextBox txtPeriod;
		protected System.Web.UI.WebControls.Label lblTo;
		protected com.common.util.msTextBox txtTo;
		protected System.Web.UI.WebControls.RadioButton rbDate;
		protected com.common.util.msTextBox txtDate;
		protected System.Web.UI.WebControls.Label lblStatusCode;
		protected System.Web.UI.WebControls.Label lblStatusClose;
		protected System.Web.UI.WebControls.RadioButton rbSCYes;
		protected System.Web.UI.WebControls.Label lblInV;
		protected System.Web.UI.WebControls.RadioButton rbInVYes;
		protected System.Web.UI.WebControls.RadioButton rbInVNo;
		protected System.Web.UI.WebControls.Label lblStatusType;
		protected System.Web.UI.WebControls.RadioButton rbPickup;
		protected System.Web.UI.WebControls.RadioButton rbDomestic;
		protected System.Web.UI.WebControls.RadioButton rbBoth;
		protected System.Web.UI.WebControls.Label lblMbg;
		protected System.Web.UI.WebControls.RadioButton rbMBGYes;
		protected System.Web.UI.WebControls.RadioButton rbMBGNo;
		protected Cambro.Web.DbCombo.DbCombo dbCmbStatusCode;
		protected Cambro.Web.DbCombo.DbCombo dbCmbExceptionCode;
		protected System.Web.UI.WebControls.RadioButton rbSCNo;
		protected System.Web.UI.WebControls.RadioButton rbStatus;
		protected System.Web.UI.WebControls.RadioButton rbException;
		protected System.Web.UI.WebControls.Label lblTitle;
		protected System.Web.UI.HtmlControls.HtmlGenericControl divDate;
		protected System.Web.UI.HtmlControls.HtmlGenericControl divStatusExcp;
		private DataView m_dvMonths;
		private DataView m_dvCmdType;
		private DataView m_dvTransitDays;
		private DataView m_dvTransitHrs;
		protected System.Web.UI.WebControls.Label lblCmdType;
		protected Cambro.Web.DbCombo.DbCombo dbCmbCmdCode;
		protected System.Web.UI.HtmlControls.HtmlGenericControl divCommodity;
		protected System.Web.UI.WebControls.Label lblCmdCode;
		protected System.Web.UI.WebControls.DropDownList ddCmdType;
		protected System.Web.UI.WebControls.Label lblIndusCode;
		protected System.Web.UI.HtmlControls.HtmlGenericControl divIndusSct;
		protected Cambro.Web.DbCombo.DbCombo dbCmbIndusCode;
		protected System.Web.UI.WebControls.Label Label2;
		protected Cambro.Web.DbCombo.DbCombo dbCmbVas;
		protected System.Web.UI.HtmlControls.HtmlGenericControl divVas;
		protected System.Web.UI.WebControls.Label lblServiceCode;
		protected Cambro.Web.DbCombo.DbCombo dbCmbServiceCode;
		protected System.Web.UI.WebControls.Label lblTransitDays;
		protected System.Web.UI.WebControls.DropDownList ddTransitDays;
		protected System.Web.UI.WebControls.Label lblTransitHrs;
		protected System.Web.UI.WebControls.DropDownList ddTransitHrs;
		protected System.Web.UI.HtmlControls.HtmlGenericControl divService;
		protected System.Web.UI.WebControls.Label lblOriZone;
		protected Cambro.Web.DbCombo.DbCombo dbCmbOrgCode;
		protected System.Web.UI.WebControls.Label lblDestZone;
		protected System.Web.UI.HtmlControls.HtmlGenericControl divBaseZone;
		protected Cambro.Web.DbCombo.DbCombo dbCmbDestCode;
		protected System.Web.UI.WebControls.Label lblCountry;
		protected System.Web.UI.WebControls.Label lblZipcode;
		protected System.Web.UI.WebControls.Label lblState;
		protected System.Web.UI.WebControls.Label lblStateName;
		protected System.Web.UI.WebControls.Label lblZoneCode;
		protected System.Web.UI.WebControls.Label lblDelvRoute;
		protected System.Web.UI.WebControls.Label lblPckRouteCode;
		protected System.Web.UI.HtmlControls.HtmlGenericControl divZipSate;
		protected Cambro.Web.DbCombo.DbCombo dbCmbCountry;
		protected Cambro.Web.DbCombo.DbCombo dbCmbZipCode;
		protected Cambro.Web.DbCombo.DbCombo dbCmbState;
		protected Cambro.Web.DbCombo.DbCombo dbCmbDlvRouteCode;
		protected Cambro.Web.DbCombo.DbCombo dbCmbPckRouteCode;
		protected Cambro.Web.DbCombo.DbCombo dbCmbZoneCode;
		protected Cambro.Web.DbCombo.DbCombo dbCmbStateName;
		private string strModuleID=null;
		protected com.common.util.msTextBox Mstextbox1;
		protected System.Web.UI.HtmlControls.HtmlTable tbDate;
		protected System.Web.UI.HtmlControls.HtmlTable tbStatusException;
		protected System.Web.UI.HtmlControls.HtmlTable tbCommodity;
		protected System.Web.UI.HtmlControls.HtmlTable tbIndusSct;
		protected System.Web.UI.HtmlControls.HtmlTable tbVas;
		protected System.Web.UI.HtmlControls.HtmlTable tbService;
		protected System.Web.UI.HtmlControls.HtmlTable tbBaseZone;
		protected System.Web.UI.HtmlControls.HtmlTable tbZipSate;
		protected System.Web.UI.WebControls.Label lblPayerid;
		protected System.Web.UI.WebControls.Label lblCustName;
		protected System.Web.UI.HtmlControls.HtmlGenericControl divCustProfile;
		protected System.Web.UI.HtmlControls.HtmlTable tbCustProfile;
		protected Cambro.Web.DbCombo.DbCombo dbCmbPayerId;
		protected Cambro.Web.DbCombo.DbCombo dbCmbPayerName;
		protected System.Web.UI.WebControls.Label lblAgentId;
		protected Cambro.Web.DbCombo.DbCombo dbCmbAgentId;
		protected System.Web.UI.WebControls.Label lblAgentName;
		protected Cambro.Web.DbCombo.DbCombo dbCmbAgentName;
		protected System.Web.UI.HtmlControls.HtmlGenericControl divAgent;
		protected System.Web.UI.HtmlControls.HtmlTable tbAgent;
		protected System.Web.UI.WebControls.Label Label3;
		protected System.Web.UI.WebControls.Label Label1;
		protected System.Web.UI.WebControls.Label Label4;
		protected System.Web.UI.WebControls.Label Label5;
		protected System.Web.UI.WebControls.Label Label6;
		protected System.Web.UI.WebControls.Label Label7;
		protected System.Web.UI.WebControls.Label Label8;
		protected System.Web.UI.WebControls.Label Label9;
		protected System.Web.UI.WebControls.Label Label10;
		protected System.Web.UI.WebControls.Label Label11;
		protected System.Web.UI.WebControls.Label Label12;
		protected System.Web.UI.WebControls.Label Lb_PaymentMode;
		protected System.Web.UI.WebControls.Label lblCustMbg;
		protected System.Web.UI.WebControls.RadioButton CustMbgYes;
		protected System.Web.UI.WebControls.RadioButton CustMbgNo;
		protected System.Web.UI.WebControls.Label lblStatusActive;
		protected System.Web.UI.WebControls.Label lblCustApplyWt;
		protected System.Web.UI.WebControls.RadioButton CustApplyWtYes;
		protected System.Web.UI.WebControls.RadioButton CustApplyWtNo;
		protected System.Web.UI.WebControls.Label lblApplyEsaSurchg;
		protected System.Web.UI.WebControls.RadioButton CustApplyEsaYes;
		protected System.Web.UI.WebControls.RadioButton CustApplyEsaNo;
		protected System.Web.UI.WebControls.Label lblPODSlip;
		protected System.Web.UI.WebControls.RadioButton CustPODYes;
		protected System.Web.UI.WebControls.RadioButton CustPODNo;
		protected System.Web.UI.WebControls.Label lblCustIndustSect;
		protected Cambro.Web.DbCombo.DbCombo CustDbCmbIndustSect;
		protected System.Web.UI.WebControls.RadioButton Rd_C_Cash;
		protected System.Web.UI.WebControls.RadioButton rd_C_Credit;
		protected System.Web.UI.WebControls.RadioButton CustStatusYes;
		protected System.Web.UI.WebControls.RadioButton CustStatusNo;
		protected System.Web.UI.WebControls.Label A_Lb_PaymentMode;
		protected System.Web.UI.WebControls.Label lblAgentMbg;
		protected System.Web.UI.WebControls.RadioButton AgentMbgYes;
		protected System.Web.UI.WebControls.RadioButton AgentMbgNo;
		protected System.Web.UI.WebControls.Label Label14;
		protected System.Web.UI.WebControls.RadioButton AgentStatusYes;
		protected System.Web.UI.WebControls.RadioButton AgentStatusNo;
		protected System.Web.UI.WebControls.Label lblAgentApplyWt;
		protected System.Web.UI.WebControls.RadioButton AgentApplyWtYes;
		protected System.Web.UI.WebControls.RadioButton AgentApplyWtNo;
		protected System.Web.UI.WebControls.Label Label15;
		protected System.Web.UI.WebControls.RadioButton AgentApplyEsaYes;
		protected System.Web.UI.WebControls.RadioButton AgentApplyEsaNo;
		protected System.Web.UI.WebControls.Label Label16;
		protected System.Web.UI.WebControls.RadioButton AgentPODYes;
		protected System.Web.UI.WebControls.RadioButton AgentPODNo;
		protected System.Web.UI.WebControls.Label lblA_IndustSect;
		protected Cambro.Web.DbCombo.DbCombo lblA_dbcmbIndustSect;
		protected System.Web.UI.WebControls.RadioButton Rd_A_Cash;
		protected System.Web.UI.WebControls.RadioButton Rd_A_Credit;
		protected System.Web.UI.WebControls.Label Label13;
		protected Cambro.Web.DbCombo.DbCombo dbCmbServiceCodeRate;
		protected System.Web.UI.WebControls.Label Label17;
		protected System.Web.UI.WebControls.Label Label18;
		protected Cambro.Web.DbCombo.DbCombo Dbcombo1;
		protected System.Web.UI.WebControls.Label Label19;
		protected System.Web.UI.WebControls.DropDownList Dropdownlist1;
		protected System.Web.UI.WebControls.Label Label20;
		protected System.Web.UI.WebControls.DropDownList Dropdownlist2;
		protected System.Web.UI.HtmlControls.HtmlTable Table1;
		protected System.Web.UI.HtmlControls.HtmlGenericControl divStaff;
		protected System.Web.UI.HtmlControls.HtmlGenericControl divVehicle;
		protected System.Web.UI.HtmlControls.HtmlTable Table2;
		protected System.Web.UI.WebControls.Label lblStaffId;
		protected Cambro.Web.DbCombo.DbCombo dbCmbStaffId;
		protected System.Web.UI.WebControls.DropDownList ddlStaff1;
		protected System.Web.UI.WebControls.DropDownList ddlStaff2;
		protected System.Web.UI.WebControls.Label Label21;
		protected System.Web.UI.WebControls.Label lblTruckId;
		protected Cambro.Web.DbCombo.DbCombo dbCmbTruckId;
		protected System.Web.UI.WebControls.Label lblVehicle1;
		protected System.Web.UI.WebControls.DropDownList ddlVehicle1;
		protected System.Web.UI.WebControls.Label lblVehicle2;
		protected System.Web.UI.WebControls.DropDownList ddlVehicle2;
		public static string strCountry1 = null;

		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			if(!IsPostBack)
			{
				strModuleID = Request.Params["MODID"]; 
				DefaultScreen();
			}
			
			SetComboProperties();
		}
		
		private void SetScreenTitle()
		{
			if(Request.Params["MODID"].Substring(2) == "StatusExceptionRpt")
			{
				lblTitle.Text = "Status/Exception Report";
				divCommodity.Visible = false;
				divIndusSct.Visible = false;
				divVas.Visible = false;
				divService.Visible = false;
				divBaseZone.Visible = false;
				divZipSate.Visible = false;
				divCustProfile.Visible = false;
				divAgent.Visible = false;
				divStaff.Visible = false;
				divVehicle.Visible = false;
			}
			else if(Request.Params["MODID"].Substring(2) == "CommodityRpt")
			{
				lblTitle.Text = "Commodity Report";
				divStatusExcp.Visible = false;
				divIndusSct.Visible = false;
				divVas.Visible = false;
				divService.Visible = false;
				divBaseZone.Visible = false;
				divZipSate.Visible = false;
				divCustProfile.Visible = false;
				divAgent.Visible = false;
				divStaff.Visible = false;
				divVehicle.Visible = false;
			}
			else if(Request.Params["MODID"].Substring(2) == "IndustSectorRpt")
			{
				lblTitle.Text = "Industrial Sector Report";
				divStatusExcp.Visible = false; 
				divCommodity.Visible = false;
				divVas.Visible = false;
				divService.Visible = false;
				divBaseZone.Visible = false;
				divZipSate.Visible = false;
				divCustProfile.Visible = false;
				divAgent.Visible = false;
				divStaff.Visible = false;
				divVehicle.Visible = false;
			}
			else if(Request.Params["MODID"].Substring(2) == "VasRpt")
			{
				lblTitle.Text = "Vas Report";
				divStatusExcp.Visible = false; 
				divCommodity.Visible = false;
				divIndusSct.Visible = false;
				divService.Visible = false;
				divBaseZone.Visible = false;
				divZipSate.Visible = false;
				divCustProfile.Visible = false;
				divAgent.Visible = false;
				divStaff.Visible = false;
				divVehicle.Visible = false;
			}
			else if(Request.Params["MODID"].Substring(2) == "ServiceRpt")
			{
				lblTitle.Text = "Service Report";
				divStatusExcp.Visible = false; 
				divCommodity.Visible = false;
				divIndusSct.Visible = false;
				divVas.Visible = false;
				divBaseZone.Visible = false;
				divZipSate.Visible = false;
				divCustProfile.Visible = false;
				divAgent.Visible = false;
				divStaff.Visible = false;
				divVehicle.Visible = false;
			}
			//// boon -- 2011-09-14 -- start
			else if(Request.Params["MODID"].Substring(2) == "StaffRpt")
			{
				lblTitle.Text = "Staff Report";
				divStatusExcp.Visible = false; 
				divCommodity.Visible = false;
				divIndusSct.Visible = false;
				divVas.Visible = false;
				divService.Visible = false;
				divBaseZone.Visible = false;
				divZipSate.Visible = false;
				divCustProfile.Visible = false;
				divAgent.Visible = false;
				divVehicle.Visible = false;
			}
			else if(Request.Params["MODID"].Substring(2) == "VehicleRpt")
			{
				lblTitle.Text = "Vehicle Report";
				divStatusExcp.Visible = false; 
				divCommodity.Visible = false;
				divIndusSct.Visible = false;
				divVas.Visible = false;
				divService.Visible = false;
				divBaseZone.Visible = false;
				divZipSate.Visible = false;
				divCustProfile.Visible = false;
				divAgent.Visible = false;
				divStaff.Visible = false;
			}
			//// boon -- 2011-09-14 -- end
			else if(Request.Params["MODID"].Substring(2) == "BaseZoneRpt")
			{
				lblTitle.Text = "Base Zone Rates Report";
				divStatusExcp.Visible = false; 
				divCommodity.Visible = false;
				divIndusSct.Visible = false;
				divVas.Visible = false;
				divService.Visible = false;
				divZipSate.Visible = false;
				divCustProfile.Visible = false;
				divAgent.Visible = false;
				divStaff.Visible = false;
				divVehicle.Visible = false;
			}
			else if(Request.Params["MODID"].Substring(2) == "ZipCodeStateRpt")
			{
				lblTitle.Text = "Postal Code and Route Report";
				divStatusExcp.Visible = false; 
				divCommodity.Visible = false;
				divIndusSct.Visible = false;
				divVas.Visible = false;
				divService.Visible = false;
				divBaseZone.Visible = false;
				divCustProfile.Visible = false;
				divAgent.Visible = false;
				divStaff.Visible = false;
				divVehicle.Visible = false;
			}
			else if(Request.Params["MODID"].Substring(2) == "CustomerProfileRpt")
			{
				lblTitle.Text = "Customer Profile Report";
				divStatusExcp.Visible = false; 
				divCommodity.Visible = false;
				divIndusSct.Visible = false;
				divVas.Visible = false;
				divService.Visible = false;
				divBaseZone.Visible = false;
				divZipSate.Visible = false;
				divAgent.Visible = false; 
				divStaff.Visible = false;
				divVehicle.Visible = false;
			}
			else if(Request.Params["MODID"].Substring(2) == "AgentProfileRpt")
			{
				lblTitle.Text = "Agent Profile Report";
				divStatusExcp.Visible = false; 
				divCommodity.Visible = false;
				divIndusSct.Visible = false;
				divVas.Visible = false;
				divService.Visible = false;
				divBaseZone.Visible = false;
				divZipSate.Visible = false;
				divCustProfile.Visible = false;
				divStaff.Visible = false;
				divVehicle.Visible = false;
			}
			lblTitle.Text = Utility.GetLanguageText(ResourceType.ScreenLabel, lblTitle.Text, utility.GetUserCulture());
		}

		private void DefaultScreen()
		{
			SetScreenTitle();
			rbMonth.Checked = true; 
			ddMonth.SelectedIndex =0;
			txtYear.Text ="";
			txtPeriod .Text="";
			txtTo.Text="";
			txtDate.Text="";
			m_dvMonths = CreateMonths(true);
			BindMonths(m_dvMonths);
			m_dvCmdType  =CreateCmdType(true); 
			BindCmdType(m_dvCmdType);
			m_dvTransitDays = TransitDays(true);
			BindTransitDays(m_dvTransitDays); 
			m_dvTransitHrs = TransitHrs(true);
			BindTransitHrs(m_dvTransitHrs); 
			ClearCombo();
			ddMonth.Enabled = true; 
			txtYear.Enabled = true; 
			txtPeriod.Text ="";
			txtTo.Text="";
			rbSCYes.Checked = false;
			rbSCNo.Checked = false;
			rbInVYes.Checked = false;
			rbInVNo.Checked = false; 
			rbPickup.Checked = false;
			rbDomestic.Checked = false;
			rbBoth.Checked = false;
			rbMBGYes .Checked = false;
			rbMBGNo.Checked = false;
			dbCmbStatusCode.Text = "";
			dbCmbExceptionCode .Text ="";
			CustDbCmbIndustSect.Text ="";
			txtDate.Text ="";
			lblErrorMessage .Text ="";
			divDate.Visible = false; 
			Rd_C_Cash.Checked = false;
			rd_C_Credit.Checked = false;
			CustMbgYes.Checked = false;
			CustMbgNo.Checked = false;
			CustStatusYes.Checked = false;
			CustStatusNo.Checked = false;
			CustApplyWtYes.Checked = false;
			CustApplyWtNo.Checked = false;
			CustApplyEsaYes.Checked = false;
			CustApplyEsaNo.Checked = false;
			CustPODYes.Checked = false;
			CustPODNo.Checked = false;
			Rd_A_Cash.Checked = false;
			Rd_A_Credit.Checked = false;
			AgentMbgYes.Checked = false;
			AgentMbgNo.Checked = false;
			AgentStatusYes.Checked = false;
            AgentStatusNo.Checked = false;
			AgentApplyWtYes.Checked = false;
			AgentApplyWtNo.Checked = false;
			AgentApplyEsaYes.Checked = false;
			AgentApplyEsaNo.Checked = false;
			AgentPODYes.Checked = false;
			rbStatus.Checked = true;
			rbException.Checked = false;			
		}

		private DataView CreateMonths(bool showNilOption)
		{
			DataTable dtMonths = new DataTable();
			dtMonths.Columns.Add(new DataColumn("Text", typeof(string)));
			dtMonths.Columns.Add(new DataColumn("StringValue", typeof(string)));
			ArrayList listMonthTxt = Utility.GetCodeValues(utility.GetAppID(),utility.GetUserCulture(), "months", CodeValueType.StringValue);
			if(showNilOption)
			{
				DataRow drNilRow = dtMonths.NewRow();
				drNilRow[0] = "";
				drNilRow[1] = "";
				dtMonths.Rows.Add(drNilRow);
			}
			foreach(SystemCode typeSysCode in listMonthTxt)
			{
				DataRow drEach = dtMonths.NewRow();
				drEach[0] = typeSysCode.Text;
				drEach[1] = typeSysCode.StringValue;
				dtMonths.Rows.Add(drEach);
			}
			DataView dvMonths = new DataView(dtMonths);
			return dvMonths;
		}

		private void BindMonths(DataView dvMonths)
		{
			ddMonth.DataSource = dvMonths;
			ddMonth.DataTextField = "Text";
			ddMonth.DataValueField = "StringValue";
			ddMonth.DataBind();	
		}

		private void ClearCombo()
		{
			dbCmbStatusCode.Text="";dbCmbStatusCode.Value="";
			dbCmbExceptionCode.Text="";dbCmbExceptionCode.Value="";
			dbCmbDestCode.Text = "";dbCmbDestCode.Value = "";
			dbCmbOrgCode.Text = "";dbCmbOrgCode.Value = "";
			dbCmbIndusCode.Text = "";dbCmbIndusCode.Value = "";
			dbCmbServiceCode.Text = "";dbCmbServiceCode.Value = "";
			dbCmbVas.Text = "";dbCmbVas.Value = "";
			dbCmbCountry.Text = "";dbCmbCountry.Value = "";
			dbCmbState.Text ="";dbCmbState.Value ="";
			dbCmbZipCode.Text = "";dbCmbZipCode.Value = "";
			dbCmbDestCode.Text = "";dbCmbDestCode.Value = "";
			dbCmbPckRouteCode.Text = "";dbCmbPckRouteCode.Value = "";
			dbCmbCmdCode.Text="";dbCmbCmdCode.Value="";
			dbCmbPayerId.Text="";dbCmbPayerId.Value="";
			dbCmbAgentId.Text="";dbCmbAgentId.Value="";	
			dbCmbZoneCode.Text="";dbCmbZoneCode.Value="";
			dbCmbDlvRouteCode.Text="";dbCmbDlvRouteCode.Value="";
			dbCmbStateName.Text="";dbCmbStateName.Value="";
			dbCmbPayerName.Text="";dbCmbPayerName.Value="";
			dbCmbAgentName.Text="";dbCmbAgentName.Value="";
			dbCmbServiceCodeRate.Text = "";dbCmbServiceCodeRate.Value = "";

			dbCmbStaffId.Text = "";	dbCmbStaffId.Value = "";	//boon 2011-09-16
			dbCmbTruckId.Text = "";	dbCmbTruckId.Value = "";	//boon 2011-09-16
		}

		private void SetComboProperties()
		{					
			dbCmbStatusCode .RegistrationKey = System.Configuration.ConfigurationSettings.AppSettings["DbComboRegKey"];
			dbCmbExceptionCode.RegistrationKey = System.Configuration.ConfigurationSettings.AppSettings["DbComboRegKey"];
			dbCmbCmdCode.RegistrationKey = System.Configuration.ConfigurationSettings.AppSettings["DbComboRegKey"];
			dbCmbIndusCode.RegistrationKey = System.Configuration.ConfigurationSettings.AppSettings["DbComboRegKey"];
			dbCmbVas.RegistrationKey = System.Configuration.ConfigurationSettings.AppSettings["DbComboRegKey"];
			dbCmbServiceCode.RegistrationKey = System.Configuration.ConfigurationSettings.AppSettings["DbComboRegKey"];
			dbCmbOrgCode.RegistrationKey = System.Configuration.ConfigurationSettings.AppSettings["DbComboRegKey"];
			dbCmbDestCode.RegistrationKey = System.Configuration.ConfigurationSettings.AppSettings["DbComboRegKey"];
			//ZipCodeState
			dbCmbCountry.RegistrationKey = System.Configuration.ConfigurationSettings.AppSettings["DbComboRegKey"];
			dbCmbZipCode.RegistrationKey = System.Configuration.ConfigurationSettings.AppSettings["DbComboRegKey"];
			dbCmbState.RegistrationKey = System.Configuration.ConfigurationSettings.AppSettings["DbComboRegKey"];
			dbCmbStateName.RegistrationKey = System.Configuration.ConfigurationSettings.AppSettings["DbComboRegKey"];
			dbCmbZoneCode.RegistrationKey = System.Configuration.ConfigurationSettings.AppSettings["DbComboRegKey"];
			dbCmbDlvRouteCode.RegistrationKey = System.Configuration.ConfigurationSettings.AppSettings["DbComboRegKey"];
			dbCmbPckRouteCode.RegistrationKey = System.Configuration.ConfigurationSettings.AppSettings["DbComboRegKey"];
			//
			dbCmbPayerId .RegistrationKey = System.Configuration.ConfigurationSettings.AppSettings["DbComboRegKey"];
			dbCmbPayerName.RegistrationKey = System.Configuration.ConfigurationSettings.AppSettings["DbComboRegKey"];
			dbCmbAgentId.RegistrationKey = System.Configuration.ConfigurationSettings.AppSettings["DbComboRegKey"];
			dbCmbAgentName.RegistrationKey = System.Configuration.ConfigurationSettings.AppSettings["DbComboRegKey"];
			CustDbCmbIndustSect.RegistrationKey = System.Configuration.ConfigurationSettings.AppSettings["DbComboRegKey"];

			dbCmbServiceCodeRate.RegistrationKey = System.Configuration.ConfigurationSettings.AppSettings["DbComboRegKey"];

			//boon 2012/02/06 - start
			dbCmbStaffId.RegistrationKey = System.Configuration.ConfigurationSettings.AppSettings["DbComboRegKey"];
			dbCmbTruckId.RegistrationKey = System.Configuration.ConfigurationSettings.AppSettings["DbComboRegKey"];
			//boon 2012/02/06 - end

			SetExceptionCodeServerStates();
			//SetZipCodeServerStates();
			//SetStateCodeServerStates();
			//SetStateNameCodeServerStates();

		}
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			base.OnInit(e);
			InitializeComponent();
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.rbMonth.CheckedChanged += new System.EventHandler(this.rbMonth_CheckedChanged);
			this.rbPeriod.CheckedChanged += new System.EventHandler(this.rbPeriod_CheckedChanged);
			this.rbDate.CheckedChanged += new System.EventHandler(this.rbDate_CheckedChanged);
			this.btnGenerate.Click += new System.EventHandler(this.btnGenerate_Click);
			this.btnQuery.Click += new System.EventHandler(this.btnQuery_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
		[Cambro.Web.DbCombo.ResultsMethodAttribute(true)]
		public static object StatusCodeServerMethod(Cambro.Web.DbCombo.ServerMethodArgs args)
		{
			Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings,HttpContext.Current.Session);
			String strAppID = util.GetAppID();
			String strEnterpriseID = util.GetEnterpriseID();

			String strStatusCode="";

			if(args.ServerState != null && args.ServerState.Authenticate("kdfsjgnskajh[apq2-4uhg465654435879mnbfdm qrs;q4"))
			{
				if(args.ServerState["strStatusCode"] != null && args.ServerState["strStatusCode"].ToString().Length > 0)
				{
					strStatusCode = args.ServerState["strStatusCode"].ToString();					
				}
			}	
			DataSet dataset = DbComboDAL.PickupStatusCodeQuery  (strAppID,strEnterpriseID,args);	
			return dataset;                
		}

		private void SetExceptionCodeServerStates()
		{
			String strStatusCode=dbCmbStatusCode .Value;
			Hashtable hash = new Hashtable();
			hash.Add("strStatusCode", strStatusCode);
			dbCmbExceptionCode  .ServerState = hash;
			dbCmbExceptionCode.ServerStateSecretString = "kdfsjgnskajh[apq2-4uhg465654435879mnbfdm qrs;q4";			
		}
		
		[Cambro.Web.DbCombo.ResultsMethodAttribute(true)]
		public static object ExceptionCodeServerMethod(Cambro.Web.DbCombo.ServerMethodArgs args)
		{
			Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings,HttpContext.Current.Session);
			String strAppID = util.GetAppID();
			String strEnterpriseID = util.GetEnterpriseID();

			String strStatusCode="";

			if(args.ServerState != null && args.ServerState.Authenticate("kdfsjgnskajh[apq2-4uhg465654435879mnbfdm qrs;q4"))
			{
				if(args.ServerState["strStatusCode"] != null && args.ServerState["strStatusCode"].ToString().Length > 0)
				{
					strStatusCode = args.ServerState["strStatusCode"].ToString();					
				}
			}	
			DataSet dataset = DbComboDAL.ExceptioCodeQuery  (strAppID,strEnterpriseID,args,strStatusCode);	
			return dataset;                
		}

		private void rbMonth_CheckedChanged(object sender, System.EventArgs e)
		{
			ddMonth.Enabled = true; 
			txtYear.Enabled = true; 
			txtPeriod.Text ="";
			txtTo.Text="";
			txtDate.Text ="";
		}

		private void rbPeriod_CheckedChanged(object sender, System.EventArgs e)
		{
			txtPeriod.Enabled = true;
			txtTo.Enabled = true;
			
			ddMonth.Enabled = false; 
			txtYear.Enabled = false;
			ddMonth.SelectedIndex=0;
			txtYear.Text ="";
		}

		private void rbDate_CheckedChanged(object sender, System.EventArgs e)
		{
			txtDate.Enabled = true;
			txtPeriod.Enabled = false;
			txtTo.Enabled = false;
			txtPeriod.Text ="";
			txtTo.Text ="";
			ddMonth.SelectedIndex =0;
			txtYear.Text="";
			ddMonth.Enabled = false;
			txtYear.Enabled = false;
		}

		private void btnGenerate_Click(object sender, System.EventArgs e)
		{
			lblErrorMessage.Text="";
			if(Request.Params["MODID"].Substring(2) == "StatusExceptionRpt")
			{
				Session["FORMID"]="STATUS EXCEP REPORT"; 
				setParamDataset();
				String strUrl = null;
				strUrl = "ReportViewer.aspx";
				OpenWindowpage(strUrl);
			}
			if(Request.Params["MODID"].Substring(2) == "CommodityRpt")
			{
				Session["FORMID"]="COMMODITY REPORT"; 
				setParamDataset();
				String strUrl = null;
				strUrl = "ReportViewer.aspx";
				OpenWindowpage(strUrl);
			}
			if(Request.Params["MODID"].Substring(2) == "IndustSectorRpt")
			{
				Session["FORMID"]="INDUSTRIAL SECTOR REPORT"; 
				setParamDataset();
				String strUrl = null;
				strUrl = "ReportViewer.aspx";
				OpenWindowpage(strUrl);
			}
			else if(Request.Params["MODID"].Substring(2) == "VasRpt")
			{
				Session["FORMID"]="VAS REPORT"; 
				setParamDataset();
				String strUrl = null;
				strUrl = "ReportViewer.aspx";
				OpenWindowpage(strUrl);
			}
			else if(Request.Params["MODID"].Substring(2) == "ServiceRpt")
			{
				Session["FORMID"]="SERVICE REPORT"; 
				setParamDataset();
				String strUrl = null;
				strUrl = "ReportViewer.aspx";
				OpenWindowpage(strUrl);
			}
				////boon -- 2011-0914 -- start
			else if(Request.Params["MODID"].Substring(2) == "StaffRpt")
			{
				Session["FORMID"]="STAFF REPORT"; 
				setParamDataset();
				String strUrl = null;
				strUrl = "ReportViewer.aspx";
				OpenWindowpage(strUrl);
			}
			else if(Request.Params["MODID"].Substring(2) == "VehicleRpt")
			{
				Session["FORMID"]="VEHICLE REPORT"; 
				setParamDataset();
				String strUrl = null;
				strUrl = "ReportViewer.aspx";
				OpenWindowpage(strUrl);
			}
				////boon -- 2011-0914 -- end
			else if(Request.Params["MODID"].Substring(2) == "BaseZoneRpt")
			{
				Session["FORMID"]="BASE ZONE RATES REPORT"; 
				setParamDataset();
				String strUrl = null;
				strUrl = "ReportViewer.aspx";
				OpenWindowpage(strUrl);
			}
			else if(Request.Params["MODID"].Substring(2) == "ZipCodeStateRpt")
			{
				Session["FORMID"]="ZIPCODE STATE REPORT"; 
				setParamDataset();
				String strUrl = null;
				strUrl = "ReportViewer.aspx";
				OpenWindowpage(strUrl);
			}
			else if(Request.Params["MODID"].Substring(2) == "CustomerProfileRpt")
			{
			//	if(dbCmbPayerId.Text =="" && dbCmbPayerName.Text =="") 
			//	{
			//		lblErrorMessage.Text  = Utility.GetLanguageText(ResourceType.UserMessage,"PLS_ENTER_PAYERID ",utility.GetUserCulture());
			//		return;
			//	}
				Session["FORMID"]="CUSTOMER PROFILE REPORT"; 
				setParamDataset();
				String strUrl = null;
				strUrl = "ReportViewer.aspx";
				OpenWindowpage(strUrl);
			}
			else if(Request.Params["MODID"].Substring(2) == "AgentProfileRpt")
			{
			//	if(dbCmbAgentId.Text =="" && dbCmbAgentName .Text =="") 
			//	{
			//		lblErrorMessage.Text  = Utility.GetLanguageText(ResourceType.UserMessage,"PLS_ENTER_AGENTID ",utility.GetUserCulture());;
			//		return;
			//	}
				Session["FORMID"]="AGENT PROFILE REPORT"; 
				setParamDataset();
				String strUrl = null;
				strUrl = "ReportViewer.aspx";
				OpenWindowpage(strUrl);
			}
		}
		private bool ValidateValues()
		{
			bool iCheck=false;
			if((ddMonth.SelectedIndex==0)&&(txtYear.Text=="")&&(txtPeriod.Text=="")&&(txtTo.Text=="")&&(txtDate.Text==""))
			{
				lblErrorMessage.Text=Utility.GetLanguageText(ResourceType.UserMessage,"PLS_ENTER_EST_DVL",utility.GetUserCulture());
				return iCheck=true;
			
			}
			if(rbMonth.Checked == true)
			{
				if((ddMonth.SelectedIndex>0) &&(txtYear.Text==""))
				{
					lblErrorMessage.Text=Utility.GetLanguageText(ResourceType.UserMessage,"PLS_ENTER_YEAR ",utility.GetUserCulture());
					return iCheck=true;
				}
				else if((ddMonth.SelectedIndex==0) &&(txtYear.Text!=""))
				{
					lblErrorMessage.Text=Utility.GetLanguageText(ResourceType.UserMessage,"MM_YEAR_REQ",utility.GetUserCulture());
					return iCheck=true;
				}
				else
				{
					return iCheck=false;
				}
			}
			if(rbPeriod.Checked == true )
			{
				if((txtPeriod.Text!="")&&(txtTo.Text==""))
				{
					lblErrorMessage.Text=Utility.GetLanguageText(ResourceType.UserMessage,"PLS_END_DT  ",utility.GetUserCulture());
					return iCheck=true;
				}
				else if((txtPeriod.Text=="")&&(txtTo.Text!=""))
				{
					lblErrorMessage.Text=Utility.GetLanguageText(ResourceType.UserMessage,"DT_REQ",utility.GetUserCulture());
					return iCheck=true;
			
				}
				else
				{
					return iCheck=false;
				}
			}
			if(rbDate.Checked == true )
			{
				if(txtDate.Text=="")
				{
					lblErrorMessage.Text=Utility.GetLanguageText(ResourceType.UserMessage,"DT_REQ",utility.GetUserCulture()); 
					return iCheck=true;
				
				}
			}
			

			return iCheck;
			
		}

		private void OpenWindowpage(String strUrl)
		{
//			String strOpenWin = "<script language=javascript>";
//			String url = strUrl;
//			String features = "'height=570,width=950,left=20,top=20,location=no,menubar=no,scrollbars=yes,resizable=yes,status=yes,titlebar=no,toolbar=no'";
//			strOpenWin += "window.open('" + url+"', '',"+features +");";
//			strOpenWin += "</script>";
//			Response.Write(strOpenWin);
			ArrayList paramList = new ArrayList();
			paramList.Add(strUrl);
			String sScript = Utility.GetScript("openParentWindow.js",paramList);
			Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
		}
		private void setParamDataset()
		{
			String strEmpId = null;	////boon -- 2011-09-14
			String strTruckId = null;	////boon -- 2011-09-14
			String strStatusType = null,strStartDate = null;
			String strStatusClose = null;
			String strStatusCode = null;
			String strExceptionCode  = null;
			String strStatusExceptType = null;
			String strInvoice = null,strMBG = null;
			String strCmdCode = null,strCmdType = null;
			String strIndusCode = null;
			String strVasCode = null;
			String strServiceCode = null;
			String strOrigZone = null;
			String strDestZone = null;
			String strCountry = null;
			String strState = null;
			String strZipCode = null;
			//Customer
			String strC_PaymentMode = null,strC_Mbg = null,strC_StatusAct = null;
			String strC_IndustSect = null,strC_ApplyDimWt = null,strC_PODSlip = null,strC_EsaSurchg = null;
			//Agent
			String strA_PaymentMode = null,strA_Mbg = null,strA_StatusAct = null;
			String strA_IndustSect = null,strA_ApplyDimWt = null,strA_PODSlip = null,strA_EsaSurchg = null;

			strState = null;
			String strStateNam = null;
			String strPayerId = null,strPayerName  = null;
			String strZoneCode = null,strDelvRouteCode = null,strPckRouteCode = null;
			String strAgentId = null, strAgentName = null;
			int iTransitDay = 0;
			int iTransitHr = 0;
			DateTime dtStartDate = DateTime.Now;
			DateTime dtEndDate = DateTime.Now;
			if(Request.Params["MODID"].Substring(2) == "StatusExceptionRpt")
			{
				//Status/Exceoption Code
				strStatusCode = dbCmbStatusCode.Value;
				strExceptionCode = dbCmbExceptionCode.Value ;
				if(rbStatus.Checked == true) 
					strStatusExceptType = "S";
				else if(rbException.Checked == true) 
					strStatusExceptType = "E";
													
				//Status Type
				if(rbDomestic.Checked == true)
					strStatusType = "D";
				else if(rbPickup.Checked == true)
					strStatusType = "P";
				else if(rbBoth.Checked == true)
					strStatusType = "B";
					
				//Status Close
				if(rbSCYes.Checked == true)
				{
					strStatusClose= "Y";
				}
				else if (rbSCNo.Checked ==true)
				{
					strStatusClose = "N";
				}
				
				//Invoiceable
				if(rbInVYes.Checked == true)
				{
					strInvoice= "Y";
				}
				else if (rbSCNo.Checked ==true)
				{
					strInvoice = "N";
				}
				
				//MBG
				if(rbMBGYes.Checked == true)
				{
					strMBG= "Y";
				}
				else if (rbMBGNo.Checked ==true)
				{
					strMBG = "N";
				}
			
				dtStartDate=DateTime.Now;
				dtEndDate=DateTime.Now;
			}
			else if(Request.Params["MODID"].Substring(2) == "CommodityRpt")
			{	
				//Commodity
				strCmdCode = dbCmbCmdCode.Value ;
				strCmdType = ddCmdType.SelectedItem.Text;  
				dtStartDate=DateTime.Now;
				dtEndDate=DateTime.Now;
			}
			else if(Request.Params["MODID"].Substring(2) == "IndustSectorRpt")
			{
				//Industrial Sector
				strIndusCode = dbCmbIndusCode.Value ;
				dtStartDate=DateTime.Now;
				dtEndDate=DateTime.Now;
			}
			else if(Request.Params["MODID"].Substring(2) == "VasRpt")
			{
				//Vas
				strVasCode = dbCmbVas.Text ;
				dtStartDate=DateTime.Now;
				dtEndDate=DateTime.Now;
			}
			else if(Request.Params["MODID"].Substring(2) == "ServiceRpt")
			{
				//ServiceCode
				strServiceCode = dbCmbServiceCode.Value ;
				iTransitDay = Convert.ToInt32(ddTransitDays.SelectedItem.Value );
				iTransitHr = Convert.ToInt32(ddTransitHrs.SelectedItem.Value) ;  
				dtStartDate=DateTime.Now;
				dtEndDate=DateTime.Now;
			}
				////boon -- 2011-09-14 -- start
			else if(Request.Params["MODID"].Substring(2) == "StaffRpt")
			{
				//ServiceCode
				strEmpId = dbCmbStaffId.Value ;
				dtStartDate=DateTime.Now;
				dtEndDate=DateTime.Now;
			}
			else if(Request.Params["MODID"].Substring(2) == "VehicleRpt")
			{
				//ServiceCode
				strTruckId = dbCmbTruckId.Value ;
				dtStartDate=DateTime.Now;
				dtEndDate=DateTime.Now;
			}
				////boon -- 2011-09-14 -- end
			else if(Request.Params["MODID"].Substring(2) == "BaseZoneRpt")
			{
				//BaseZoneRates
				strServiceCode = dbCmbServiceCodeRate.Value ;
				strOrigZone = dbCmbOrgCode.Value ;
				strDestZone = dbCmbDestCode .Value ;
				dtStartDate=DateTime.Now;
				dtEndDate=DateTime.Now;

			}
			else if(Request.Params["MODID"].Substring(2) == "ZipCodeStateRpt")
			{
				//ZipCodeState 
				strCountry  = dbCmbCountry .Text ;
				strZipCode = dbCmbZipCode.Text ;
				strState = dbCmbState .Value ;
				strStateNam = dbCmbStateName .Text ; 
				strZoneCode = dbCmbZoneCode .Text ;
				strDelvRouteCode = dbCmbDlvRouteCode .Text ;
				strPckRouteCode = dbCmbPckRouteCode .Text ;
				dtStartDate = DateTime.Now;
				dtEndDate = DateTime.Now;
			}
			else if(Request.Params["MODID"].Substring(2) == "CustomerProfileRpt")
			{
				//Customer Profile
				strPayerId = dbCmbPayerId .Value ;
				strPayerName = dbCmbPayerName .Value ;
				dtStartDate = DateTime.Now;
				dtEndDate = DateTime.Now;
				//Payment Mode
				if(rd_C_Credit.Checked == true)
				{
					strC_PaymentMode ="R";
				}
				else if(Rd_C_Cash.Checked == true )
				{
					strC_PaymentMode ="C";
				}
				//MBG
				if(CustMbgNo.Checked == true)
				{
					strC_Mbg ="N";
				}
				else if(CustMbgYes.Checked == true)
				{
					strC_Mbg ="Y";
				}
				//Status Active
				if(CustStatusYes.Checked == true)
				{
					strC_StatusAct ="Y";
				}
				else if(CustStatusNo.Checked == true)
				{
					strC_StatusAct ="N";
				}
				//Industrial Sector
				strC_IndustSect = CustDbCmbIndustSect.Value;
				//Apply Dim Wt
				if(CustApplyWtYes.Checked == true)
				{
					strC_ApplyDimWt ="Y";
				}
				else if(CustApplyWtNo.Checked == true)
				{
					strC_ApplyDimWt ="N";
				}
				//POD Slip
				if(CustPODYes.Checked == true)
				{
					strC_PODSlip ="Y";
				}
				else if(CustPODNo.Checked == true)
				{
					strC_PODSlip ="N";
				}
				//Esa Surcarge
				
				if(CustApplyEsaYes.Checked == true)
				{
					strC_EsaSurchg ="Y";
				}
				else if(CustApplyEsaYes.Checked == true)
				{
					strC_EsaSurchg ="N";
				}

			}
			else if(Request.Params["MODID"].Substring(2) == "AgentProfileRpt")
			{
				//Agent Profile
				strAgentId = dbCmbAgentId.Value ;
				strAgentName = dbCmbAgentName.Value ;
				dtStartDate = DateTime.Now;
				dtEndDate = DateTime.Now;
				//Payment Mode
				if(Rd_A_Credit.Checked == true)
				{
					strA_PaymentMode ="R";
				}
				else if(Rd_A_Cash.Checked == true )
				{
					strA_PaymentMode ="C";
				}
				//MBG
				if(AgentMbgNo.Checked == true)
				{
					strA_Mbg ="N";
				}
				else if(AgentMbgYes.Checked == true)
				{
					strA_Mbg ="Y";
				}
				//Status Active
				if(AgentStatusYes.Checked == true)
				{
					strA_StatusAct ="Y";
				}
				else if(AgentStatusNo.Checked == true)
				{
					strA_StatusAct ="N";
				}
				//Apply Dim Wt
				if(AgentApplyWtYes.Checked == true)
				{
					strA_ApplyDimWt ="Y";
				}
				else if(AgentApplyWtNo.Checked == true)
				{
					strA_ApplyDimWt ="N";
				}
				//POD Slip
				if(AgentPODYes.Checked == true)
				{
					strA_PODSlip ="Y";
				}
				else if(AgentPODNo.Checked == true)
				{
					strA_PODSlip ="N";
				}
				//Esa Surcarge
				
				if(AgentApplyEsaYes.Checked == true)
				{
					strA_EsaSurchg ="Y";
				}
				else if(AgentApplyEsaNo.Checked == true)
				{
					strA_EsaSurchg ="N";
				}

			}
			else
			{
				//BookingDate 
				if((ddMonth.SelectedIndex == 0) && (txtYear.Text == "")&&(txtPeriod.Text == "")&&(txtTo.Text == "")&&(txtDate.Text == ""))
				{
					lblErrorMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"PLS_ENTER_EST_DVL ",utility.GetUserCulture());
					return;
				}
					
				if (rbMonth.Checked == true)
				{					
					if(ddMonth.SelectedItem.Value != null && Convert.ToInt32(ddMonth.SelectedItem.Value) < 10)
						strStartDate="01"+"/0"+ddMonth.SelectedItem.Value+"/"+txtYear.Text.Trim();
					else
						strStartDate="01"+"/"+ddMonth.SelectedItem.Value+"/"+txtYear.Text.Trim();
					dtStartDate=System.DateTime.ParseExact(strStartDate,"dd/MM/yyyy",null);				
					dtEndDate=dtStartDate.AddMonths(1).AddDays(-1);				
				}
				if (rbPeriod.Checked == true)
				{	
					dtStartDate=System.DateTime.ParseExact(txtPeriod.Text.Trim(),"dd/MM/yyyy",null);									
					dtEndDate=System.DateTime.ParseExact(txtTo.Text.Trim(),"dd/MM/yyyy",null);
				}

				if (rbDate.Checked == true)
				{				
					dtStartDate=System.DateTime.ParseExact(txtDate.Text.Trim(),"dd/MM/yyyy",null);
					dtEndDate=dtStartDate.AddDays(1).AddMinutes(-1);				
				}
								
			}
			//Creating DataSet Structure..
			DataSet m_dsQuery =  new DataSet();
			DataTable dtParam = new DataTable();
			dtParam.Columns.Add("ApplicationId",typeof(string));
			dtParam.Columns.Add("EnterpriseId",typeof(string));
			dtParam.Columns.Add("From_Date",typeof(DateTime));
			dtParam.Columns.Add("To_Date",typeof(DateTime));
			if(Request.Params["MODID"].Substring(2) == "StatusExceptionRpt")
			{
				//Status/Exception
				dtParam.Columns.Add("StatusExcpType",typeof(string));
				dtParam.Columns.Add("StatusType",typeof(string));
				dtParam.Columns.Add("StatusClose",typeof(string));
				dtParam.Columns.Add("Invoice",typeof(string));
				dtParam.Columns.Add("MBG",typeof(string));
				dtParam.Columns.Add("StatusCode",typeof(string));
				dtParam.Columns.Add("ExceptionCode",typeof(string));
			}
			if(Request.Params["MODID"].Substring(2) == "CommodityRpt")
			{
				//Commodity
				dtParam.Columns.Add("CommodityCode",typeof(string));
				dtParam.Columns.Add("CommodityType",typeof(string));
			}
			if(Request.Params["MODID"].Substring(2) == "IndustSectorRpt")
			{
				//Industrial Sector
				dtParam.Columns.Add("IndustrialSectorCode",typeof(string));

			}
			else if(Request.Params["MODID"].Substring(2) == "VasRpt")
			{
				//Vas
				dtParam.Columns.Add("VasCode",typeof(string));

			}
			else if(Request.Params["MODID"].Substring(2) == "ServiceRpt")
			{
				//ServiceCode
				dtParam.Columns.Add("ServiceCode",typeof(string));
				dtParam.Columns.Add("TransitDays",typeof(int));
				dtParam.Columns.Add("TransitHrs",typeof(int));
			}
			////boon -- 2011-09-14 -- start
			else if(Request.Params["MODID"].Substring(2) == "StaffRpt")
			{
				//StaffId
				dtParam.Columns.Add("EmpId",typeof(string));
			}
			else if(Request.Params["MODID"].Substring(2) == "VehicleRpt")
			{
				//TruckId
				dtParam.Columns.Add("TruckId",typeof(string));
			}
			////boon -- 2011-09-14 -- end
			else if(Request.Params["MODID"].Substring(2) == "BaseZoneRpt")
			{
				//BaseZoneRates
				dtParam.Columns.Add("ServiceCode",typeof(string));
				dtParam.Columns.Add("OriginZoneCode",typeof(string));
				dtParam.Columns.Add("DestZoneCode",typeof(string));

			}
			else if(Request.Params["MODID"].Substring(2) == "ZipCodeStateRpt")
			{
				//ZipCodeState
				dtParam.Columns.Add("Country",typeof(string));
				dtParam.Columns.Add("ZipCode",typeof(string));
				dtParam.Columns.Add("State",typeof(string));
				dtParam.Columns.Add("StateName",typeof(string));
				dtParam.Columns.Add("ZoneCode",typeof(string));
				dtParam.Columns.Add("DlvRouteCode",typeof(string));
				dtParam.Columns.Add("PckRouteCode",typeof(string));

			}
			else if(Request.Params["MODID"].Substring(2) == "CustomerProfileRpt")
			{
				//Customer Profile 
				dtParam.Columns.Add("PayerId",typeof(string));
				dtParam.Columns.Add("PayerName",typeof(string));
				dtParam.Columns.Add("PaymentMode",typeof(string));
				dtParam.Columns.Add("Mbg",typeof(string));
				dtParam.Columns.Add("StatusActive",typeof(string));
				dtParam.Columns.Add("IndustSect",typeof(string));
				dtParam.Columns.Add("ApplyDimWt",typeof(string));
				dtParam.Columns.Add("PODSlip",typeof(string));
				dtParam.Columns.Add("ESASurchg",typeof(string));


			}
			else if(Request.Params["MODID"].Substring(2) == "AgentProfileRpt")
			{
				//Agent Profile 
				dtParam.Columns.Add("AgentId",typeof(string));
				dtParam.Columns.Add("AgentName",typeof(string));
				dtParam.Columns.Add("PaymentMode",typeof(string));
				dtParam.Columns.Add("Mbg",typeof(string));
				dtParam.Columns.Add("StatusActive",typeof(string));
				dtParam.Columns.Add("IndustSect",typeof(string));
				dtParam.Columns.Add("ApplyDimWt",typeof(string));
				dtParam.Columns.Add("PODSlip",typeof(string));
				dtParam.Columns.Add("ESASurchg",typeof(string));

			}
			//Populating the DataSet with values..
			DataRow drEach = dtParam.NewRow();
			drEach["ApplicationId"] = utility.GetAppID();
			drEach["EnterpriseId"] = utility.GetEnterpriseID();
			drEach["From_Date"] = dtStartDate;
			drEach["To_Date"] = dtEndDate;
			if(Request.Params["MODID"].Substring(2) == "StatusExceptionRpt")
			{
				//Status/Exception
				drEach["StatusExcpType"] = strStatusExceptType;
				drEach["StatusType"] = strStatusType ;
				drEach["StatusClose"] = strStatusClose;
				drEach["Invoice"] = strInvoice ;
				drEach["MBG"] = strMBG;
				drEach["StatusCode"] = strStatusCode ;
				drEach["ExceptionCode"] = strExceptionCode;
				 
			}	
			if(Request.Params["MODID"].Substring(2) == "CommodityRpt")
			{
				//Commodity
				drEach["CommodityCode"] = strCmdCode;
				drEach["CommodityType"] = strCmdType  ;

			}
			if(Request.Params["MODID"].Substring(2) == "IndustSectorRpt")
			{
				//Commodity
				drEach["IndustrialSectorCode"] = strIndusCode;
			 
			}
			else if(Request.Params["MODID"].Substring(2) == "VasRpt")
			{
				//Vas
				drEach["VasCode"] = strVasCode;
			}
			else if(Request.Params["MODID"].Substring(2) == "ServiceRpt")
			{
				//ServiceCode
				drEach["ServiceCode"] = strServiceCode ;
				drEach["TransitDays"] = iTransitDay ;
				drEach["TransitHrs"] = iTransitHr ;
			}
				////boon -- 2011-09-14 -- start
			else if(Request.Params["MODID"].Substring(2) == "StaffRpt")
			{
				//EmpId
				drEach["EmpId"] = strEmpId ;
			}
			else if(Request.Params["MODID"].Substring(2) == "VehicleRpt")
			{
				//TruckId
				drEach["TruckId"] = strTruckId ;
			}
				////boon -- 2011-09-14 -- end
			else if(Request.Params["MODID"].Substring(2) == "BaseZoneRpt")
			{
				//BaseZoneRates
				drEach["ServiceCode"] = strServiceCode ;
				drEach["OriginZoneCode"] = strOrigZone ;
				drEach["DestZoneCode"] = strDestZone ;
			}
			else if(Request.Params["MODID"].Substring(2) == "ZipCodeStateRpt")
			{
				//ZipCodeState
				drEach["Country"] = strCountry  ;
				drEach["ZipCode"] = strZipCode;
				drEach["State"] = strState  ;
				drEach["StateName"] = strStateNam  ;
				drEach["ZoneCode"] = strZoneCode  ;
				drEach["DlvRouteCode"] = strDelvRouteCode  ;
				drEach["PckRouteCode"] = strPckRouteCode ;
			}
			else if(Request.Params["MODID"].Substring(2) == "CustomerProfileRpt")
			{
				//Customer Profile
				drEach["PayerId"] = strPayerId   ;
				drEach["PayerName"] = strPayerName ; 
				drEach["PaymentMode"] = strC_PaymentMode   ; 
				drEach["Mbg"] = strC_Mbg   ; 
				drEach["StatusActive"] = strC_StatusAct  ;
				drEach["IndustSect"] = strC_IndustSect ;
				drEach["ApplyDimWt"] = strC_ApplyDimWt ;
				drEach["PODSlip"] = strC_PODSlip ;
				drEach["ESASurchg"] = strC_EsaSurchg ;
			}
			else if(Request.Params["MODID"].Substring(2) == "AgentProfileRpt")
			{
				//Agent Profile
				drEach["AgentId"] = strAgentId    ;
				drEach["AgentName"] = strAgentName  ; 
				drEach["PaymentMode"] = strA_PaymentMode   ; 
				drEach["Mbg"] = strA_Mbg   ; 
				drEach["StatusActive"] = strA_StatusAct  ;
				drEach["IndustSect"] = strA_IndustSect ;
				drEach["ApplyDimWt"] = strA_ApplyDimWt ;
				drEach["PODSlip"] = strA_PODSlip ;
				drEach["ESASurchg"] = strA_EsaSurchg ;
			}
			m_dsQuery.Tables.Add(dtParam) ; 
			m_dsQuery.Tables[0].Rows.Add(drEach);
			Session["SESSION_DS1"] = m_dsQuery;

		}

		private void btnQuery_Click(object sender, System.EventArgs e)
		{
			DefaultScreen(); 
		}

		[Cambro.Web.DbCombo.ResultsMethodAttribute(true)]
		public static object ServiceCodeServerMethod(Cambro.Web.DbCombo.ServerMethodArgs args)
		{
			Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings,HttpContext.Current.Session);
			String strAppID = util.GetAppID();
			String strEnterpriseID = util.GetEnterpriseID();

			String strServiceCode="";

			if(args.ServerState != null && args.ServerState.Authenticate("kdfsjgnskajh[apq2-4uhg465654435879mnbfdm qrs;q4"))
			{
				if(args.ServerState["strServiceCode"] != null && args.ServerState["strServiceCode"].ToString().Length > 0)
				{
					strServiceCode = args.ServerState["strServiceCode"].ToString();					
				}
			}	
			DataSet dataset = DbComboDAL.ServiceCodeQuery (strAppID,strEnterpriseID,args);	
			return dataset;                
		}

		//boon 2011-09-14 -- start
		[Cambro.Web.DbCombo.ResultsMethodAttribute(true)]
		public static object StaffIdServerMethod(Cambro.Web.DbCombo.ServerMethodArgs args)
		{
			Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings,HttpContext.Current.Session);
			String strAppID = util.GetAppID();
			String strEnterpriseID = util.GetEnterpriseID();

			String strStaffId = "";

			if(args.ServerState != null && args.ServerState.Authenticate("kdfsjgnskajh[apq2-4uhg465654435879mnbfdm qrs;q4"))
			{
				if(args.ServerState["strStaffId"] != null && args.ServerState["strStaffId"].ToString().Length > 0)
				{
					strStaffId = args.ServerState["strStaffId"].ToString();					
				}
			}	
			DataSet dataset = DbComboDAL.StaffIdQuery (strAppID,strEnterpriseID,args);	
			return dataset;                
		}

		[Cambro.Web.DbCombo.ResultsMethodAttribute(true)]
		public static object VehicleIdServerMethod(Cambro.Web.DbCombo.ServerMethodArgs args)
		{
			Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings,HttpContext.Current.Session);
			String strAppID = util.GetAppID();
			String strEnterpriseID = util.GetEnterpriseID();

			String strTruckId = "";

			if(args.ServerState != null && args.ServerState.Authenticate("kdfsjgnskajh[apq2-4uhg465654435879mnbfdm qrs;q4"))
			{
				if(args.ServerState["strTruckId"] != null && args.ServerState["strTruckId"].ToString().Length > 0)
				{
					strTruckId = args.ServerState["strTruckId"].ToString();					
				}
			}	
			DataSet dataset = DbComboDAL.VehicleIdQuery (strAppID,strEnterpriseID,args);	
			return dataset;                
		}
		//boon 2011-09-14 -- end

	   [Cambro.Web.DbCombo.ResultsMethodAttribute(true)]
		public static object CmdCodeServerMethod(Cambro.Web.DbCombo.ServerMethodArgs args)
		{
			Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings,HttpContext.Current.Session);
			String strAppID = util.GetAppID();
			String strEnterpriseID = util.GetEnterpriseID();

			String strCommodityCode="";

			if(args.ServerState != null && args.ServerState.Authenticate("kdfsjgnskajh[apq2-4uhg465654435879mnbfdm qrs;q4"))
			{
				if(args.ServerState["strCommodityCode"] != null && args.ServerState["strCommodityCode"].ToString().Length > 0)
				{
					strCommodityCode = args.ServerState["strCommodityCode"].ToString();					
				}
			}	
			DataSet dataset = DbComboDAL.CommodityCodeQuery(strAppID,strEnterpriseID,args);	
			return dataset;                
		}


		[Cambro.Web.DbCombo.ResultsMethodAttribute(true)]
		public static object CmdTypeServerMethod(Cambro.Web.DbCombo.ServerMethodArgs args)
		{
			Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings,HttpContext.Current.Session);
			String strAppID = util.GetAppID();
			String strEnterpriseID = util.GetEnterpriseID();

			String strCommodityType="";

			if(args.ServerState != null && args.ServerState.Authenticate("kdfsjgnskajh[apq2-4uhg465654435879mnbfdm qrs;q4"))
			{
				if(args.ServerState["strCommodityType"] != null && args.ServerState["strCommodityType"].ToString().Length > 0)
				{
					strCommodityType = args.ServerState["strCommodityType"].ToString();					
				}
			}	
			DataSet dataset = DbComboDAL.CommodityTypeQuery(strAppID,strEnterpriseID,args);	
			return dataset;                
		}

		private DataView CreateCmdType(bool showNilOption)
		{
			DataTable dtCmdType = new DataTable();
			dtCmdType.Columns.Add(new DataColumn("Text", typeof(string)));
			dtCmdType.Columns.Add(new DataColumn("StringValue", typeof(string)));
			ArrayList listMonthTxt = Utility.GetCodeValues(utility.GetAppID(),utility.GetUserCulture(),"commodity_type",CodeValueType.StringValue);
			if(showNilOption)
			{
				DataRow drNilRow = dtCmdType.NewRow();
				drNilRow[0] = "";
				drNilRow[1] = "";
				dtCmdType.Rows.Add(drNilRow);
			}
			foreach(SystemCode typeSysCode in listMonthTxt)
			{
				DataRow drEach = dtCmdType.NewRow();
				drEach[0] = typeSysCode.Text;
				drEach[1] = typeSysCode.StringValue;
				dtCmdType.Rows.Add(drEach);
			}
			DataView dvCmdType= new DataView(dtCmdType);
			return dvCmdType;
		}

		private void BindCmdType(DataView dvMonths)
		{
			ddCmdType.DataSource = m_dvCmdType;
			ddCmdType.DataTextField = "Text";
			ddCmdType.DataValueField = "StringValue";
			ddCmdType.DataBind();	
		}

		[Cambro.Web.DbCombo.ResultsMethodAttribute(true)]
		public static object IndusCodeServerMethod(Cambro.Web.DbCombo.ServerMethodArgs args)
		{
			Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings,HttpContext.Current.Session);
			String strAppID = util.GetAppID();
			String strEnterpriseID = util.GetEnterpriseID();

			String strIndusCode="";

			if(args.ServerState != null && args.ServerState.Authenticate("kdfsjgnskajh[apq2-4uhg465654435879mnbfdm qrs;q4"))
			{
				if(args.ServerState["strIndusCode"] != null && args.ServerState["strIndusCode"].ToString().Length > 0)
				{
					strIndusCode = args.ServerState["strIndusCode"].ToString();					
				}
			}	
			DataSet dataset = DbComboDAL.IndustrialCodeQuery(strAppID,strEnterpriseID,args);	
			return dataset;                
		}

		[Cambro.Web.DbCombo.ResultsMethodAttribute(true)]
		public static object VasServerMethod(Cambro.Web.DbCombo.ServerMethodArgs args)
		{
			Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings,HttpContext.Current.Session);
			String strAppID = util.GetAppID();
			String strEnterpriseID = util.GetEnterpriseID();

			String strVasCode="";

			if(args.ServerState != null && args.ServerState.Authenticate("kdfsjgnskajh[apq2-4uhg465654435879mnbfdm qrs;q4"))
			{
				if(args.ServerState["strVasCode"] != null && args.ServerState["strVasCode"].ToString().Length > 0)
				{
					strVasCode = args.ServerState["strVasCode"].ToString();					
				}
			}	
			DataSet dataset = DbComboDAL.VasCodeQuery (strAppID,strEnterpriseID,args);	
			return dataset;                
		}

		private DataView TransitDays(bool showNilOption)
		{
			int i=0;
			DataTable dtTdays = new DataTable();
			dtTdays.Columns.Add(new DataColumn("Days", typeof(int)));
			
					
			if(showNilOption)
			{
				DataRow drNilRow = dtTdays.NewRow();
				drNilRow[0] = 0;
				dtTdays.Rows.Add(drNilRow);
			}
			for(i=0;i<=30;i++)
			{

				DataRow drEach = dtTdays.NewRow();
				drEach[0] = i+1;
				dtTdays.Rows.Add(drEach);
			}
			DataView dvTdays = new DataView(dtTdays);
			return dvTdays;
		}

		private void BindTransitDays(DataView dvTdays)
		{
			ddTransitDays.DataSource = dvTdays;
			ddTransitDays.DataValueField = "Days";
			ddTransitDays.DataBind();	
		}

		private DataView TransitHrs(bool showNilOption)
		{
			int i=0;
			DataTable dtTHrs = new DataTable();
			dtTHrs.Columns.Add(new DataColumn("Hours", typeof(int)));
			
					
			if(showNilOption)
			{
				DataRow drNilRow = dtTHrs.NewRow();
				drNilRow[0] = 0;
				dtTHrs.Rows.Add(drNilRow);
			}
			for(i=0;i<=23;i++)
			{

				DataRow drEach = dtTHrs.NewRow();
				drEach[0] = i+1;
				dtTHrs.Rows.Add(drEach);
			}
			DataView dvTHrs = new DataView(dtTHrs);
			return dvTHrs;
		}

		private void BindTransitHrs(DataView dtTHrs)
		{
			ddTransitHrs.DataSource = dtTHrs;
			ddTransitHrs.DataValueField = "Hours";
			ddTransitHrs.DataBind();	
		}

		[Cambro.Web.DbCombo.ResultsMethodAttribute(true)]
		public static object OriginCodeServerMethod(Cambro.Web.DbCombo.ServerMethodArgs args)
		{
			Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings,HttpContext.Current.Session);
			String strAppID = util.GetAppID();
			String strEnterpriseID = util.GetEnterpriseID();

			String strOrigCode="";

			if(args.ServerState != null && args.ServerState.Authenticate("kdfsjgnskajh[apq2-4uhg465654435879mnbfdm qrs;q4"))
			{
				if(args.ServerState["strOrigCode"] != null && args.ServerState["strOrigCode"].ToString().Length > 0)
				{
					strOrigCode = args.ServerState["strOrigCode"].ToString();					
				}
			}	
			DataSet dataset = DbComboDAL.OriginZoneCodeQuery(strAppID,strEnterpriseID,args);	
			return dataset;                
		}

		[Cambro.Web.DbCombo.ResultsMethodAttribute(true)]
		public static object DestCodeServerMethod(Cambro.Web.DbCombo.ServerMethodArgs args)
		{
			Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings,HttpContext.Current.Session);
			String strAppID = util.GetAppID();
			String strEnterpriseID = util.GetEnterpriseID();

			String strDestCode="";

			if(args.ServerState != null && args.ServerState.Authenticate("kdfsjgnskajh[apq2-4uhg465654435879mnbfdm qrs;q4"))
			{
				if(args.ServerState["strOrigCode"] != null && args.ServerState["strOrigCode"].ToString().Length > 0)
				{
					strDestCode = args.ServerState["strDestCode"].ToString();					
				}
			}	
			DataSet dataset = DbComboDAL.DestZoneCodeQuery(strAppID,strEnterpriseID,args);	
			return dataset;                
		}

		[Cambro.Web.DbCombo.ResultsMethodAttribute(true)]
		public static object CountryServerMethod(Cambro.Web.DbCombo.ServerMethodArgs args)
		{
			Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings,HttpContext.Current.Session);
			String strAppID = util.GetAppID();
			String strEnterpriseID = util.GetEnterpriseID();
			DataSet dataset = DbComboDAL.CountryQuery(strAppID,strEnterpriseID,args);	
			return dataset ;              
			
		}
		//
//		private void SetZipCodeServerStates()
//		{
//			strCountry1= dbCmbCountry.Value  ;
//			String strState=dbCmbState.Value  ;
//			Hashtable hash = new Hashtable();
//			hash.Add("strCountry1", strCountry1);
//			hash.Add("strState", strState);
//			dbCmbZipCode.ServerState = hash;
//			dbCmbZipCode.ServerStateSecretString = "kdfsjgnskajh[apq2-4uhg465654435879mnbfdm qrs;q5";	
//
//			dbCmbCountry .Value = "";
//		
//		}
//		
//		
		[Cambro.Web.DbCombo.ResultsMethodAttribute(true)]
		public static object ZipCodeServerMethod(Cambro.Web.DbCombo.ServerMethodArgs args)
		{
			Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings,HttpContext.Current.Session);
			String strAppID = util.GetAppID();
			String strEnterpriseID = util.GetEnterpriseID();
			
			DataSet dsData = null;
			String strCountry ="";
			String strState ="";
			if (args.ClientState!=null)
			{
				if (args.ClientState["Country"] != null && args.ClientState["Country"].ToString()!="-1" )
				{
					dsData = DbComboDAL.ZipCodeQuery(strAppID,strEnterpriseID,args,args.ClientState["Country"].ToString() ,strState);
					return dsData;
				}
			}

			dsData = DbComboDAL.ZipCodeQuery(strAppID,strEnterpriseID,args,strCountry,strState);
			return dsData;   
		}    
		
		[Cambro.Web.DbCombo.ResultsMethodAttribute(true)]
		public static object StateServerMethod(Cambro.Web.DbCombo.ServerMethodArgs args)
		{
			Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings,HttpContext.Current.Session);
			String strAppID = util.GetAppID();
			String strEnterpriseID = util.GetEnterpriseID();
			
			DataSet dsData = null;
			String strCountry ="";
			if (args.ClientState!=null)
			{
				if (args.ClientState["Country"] != null && args.ClientState["Country"].ToString()!="-1" )
				{
					dsData = DbComboDAL.StateCodeQuery(strAppID,strEnterpriseID,args,args.ClientState["Country"].ToString());
					return dsData;
				}
			}

			dsData = DbComboDAL.StateCodeQuery(strAppID,strEnterpriseID,args,strCountry);
			return dsData;              
		}

		[Cambro.Web.DbCombo.ResultsMethodAttribute(true)]
		public static object RouteCodeServerMethod(Cambro.Web.DbCombo.ServerMethodArgs args)
		{
			Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings,HttpContext.Current.Session);
			String strAppID = util.GetAppID();
			String strEnterpriseID = util.GetEnterpriseID();

			string strRouteCode="";
			string strPathCode="";

			if(args.ServerState != null && args.ServerState.Authenticate("kdfsjgnskajh[apq2-4uhg465654435879mnbfdm cxs;q4"))
			{
				if(args.ServerState["strRouteCode"] != null && args.ServerState["strRouteCode"].ToString().Length > 0)
				{
					strRouteCode = args.ServerState["strRouteCode"].ToString();					
				}
			}	
			DataSet dataset = DbComboDAL.RouteCodeQuery(strAppID,strEnterpriseID,args,strPathCode);	
			return dataset;                
		}
		[Cambro.Web.DbCombo.ResultsMethodAttribute(true)]
		public static object ZoneCodeServerMethod(Cambro.Web.DbCombo.ServerMethodArgs args)
		{
			Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings,HttpContext.Current.Session);
			String strAppID = util.GetAppID();
			String strEnterpriseID = util.GetEnterpriseID();

			
			string strZoneCode="";

			if(args.ServerState != null && args.ServerState.Authenticate("kdfsjgnskajh[apq2-4uhg465654435879mnbfdm cxs;q4"))
			{
				if(args.ServerState["strZoneCode"] != null && args.ServerState["strZoneCode"].ToString().Length > 0)
				{
					strZoneCode = args.ServerState["strZoneCode"].ToString();					
				}
			}	
			DataSet dataset = DbComboDAL.ZoneCodeQuery(strAppID,strEnterpriseID,args);	
			return dataset;                
		}
	
		[Cambro.Web.DbCombo.ResultsMethodAttribute(true)]
		public static object StateNameServerMethod(Cambro.Web.DbCombo.ServerMethodArgs args)
		{
			Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings,HttpContext.Current.Session);
			String strAppID = util.GetAppID();
			String strEnterpriseID = util.GetEnterpriseID();
			
			DataSet dsData = null;
			String strState ="";
			if (args.ClientState!=null)
			{
				if (args.ClientState["State"] != null && args.ClientState["State"].ToString()!="-1" )
				{
					dsData = DbComboDAL.StateNameQuery(strAppID,strEnterpriseID,args,args.ClientState["State"].ToString());
					return dsData;
				}
			}

			dsData = DbComboDAL.StateNameQuery(strAppID,strEnterpriseID,args,strState);
			return dsData;                  
		}

		[Cambro.Web.DbCombo.ResultsMethodAttribute(true)]
		public static object PayerIdServerMethod(Cambro.Web.DbCombo.ServerMethodArgs args)
		{
			Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings,HttpContext.Current.Session);
			String strAppID = util.GetAppID();
			String strEnterpriseID = util.GetEnterpriseID();
			String strPayerType ="C";
			String strPayerName = "";
			DataSet dataset = DbComboDAL.PayerIDQuery(strAppID,strEnterpriseID,args,strPayerType,strPayerName);	
			return dataset ;              
			            
		}

		[Cambro.Web.DbCombo.ResultsMethodAttribute(true)]
		public static object PayerNameServerMethod(Cambro.Web.DbCombo.ServerMethodArgs args)
		{
			Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings,HttpContext.Current.Session);
			String strAppID = util.GetAppID();
			String strEnterpriseID = util.GetEnterpriseID();
			
			DataSet dsData = null;
			String strPayerType ="C";
			String strPayerId = "";

			if (args.ClientState!=null)
			{
				if (args.ClientState["Payerid"] != null && args.ClientState["Payerid"].ToString()!="-1" )
				{
					dsData = DbComboDAL.PayerNameQuery(strAppID,strEnterpriseID,args,strPayerType,args.ClientState["Payerid"].ToString());
					return dsData;
				}
			}

			dsData = DbComboDAL.PayerNameQuery(strAppID,strEnterpriseID,args,strPayerType,strPayerId);
			return dsData;                  
		}

		[Cambro.Web.DbCombo.ResultsMethodAttribute(true)]
		public static object AgentIdServerMethod(Cambro.Web.DbCombo.ServerMethodArgs args)
		{
			Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings,HttpContext.Current.Session);
			String strAppID = util.GetAppID();
			String strEnterpriseID = util.GetEnterpriseID();
			String strPayerType ="A";
			String strAgentName = "";
			DataSet dataset = DbComboDAL.PayerIDQuery(strAppID,strEnterpriseID,args,strPayerType,strAgentName);	
			return dataset ;              
			            
		}

		[Cambro.Web.DbCombo.ResultsMethodAttribute(true)]
		public static object AgentNameServerMethod(Cambro.Web.DbCombo.ServerMethodArgs args)
		{
			Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings,HttpContext.Current.Session);
			String strAppID = util.GetAppID();
			String strEnterpriseID = util.GetEnterpriseID();
			
			DataSet dsData = null;
			String strPayerType ="A";
			String strAgentId = "";

			if (args.ClientState!=null)
			{
				if (args.ClientState["Agentid"] != null && args.ClientState["Agentid"].ToString()!="-1" )
				{
					dsData = DbComboDAL.PayerNameQuery(strAppID,strEnterpriseID,args,strPayerType,args.ClientState["Agentid"].ToString());
					return dsData;
				}
			}

			dsData = DbComboDAL.PayerNameQuery(strAppID,strEnterpriseID,args,strPayerType,strAgentId);
			return dsData;                  
		}

		
	}
}
