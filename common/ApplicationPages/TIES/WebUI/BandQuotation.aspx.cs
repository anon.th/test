using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using com.common.DAL;  
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.ties.DAL;
using com.common.util;
using com.common.classes;
using com.ties.classes; 
using TIES;
using com.common.applicationpages;


namespace com.ties
{
	/// <summary>
	/// Summary description for BandQuotation.
	/// </summary>
	public class BandQuotation : BasePage
	{
		protected System.Web.UI.WebControls.DataGrid dgVas;
		protected System.Web.UI.WebControls.Label lblErrrMsg;
		protected System.Web.UI.WebControls.Label Header1;
		protected System.Web.UI.WebControls.Label lblQuoatationNo;
		protected System.Web.UI.WebControls.TextBox txtQuoationno;
		protected System.Web.UI.WebControls.Label lblStatus;
		protected System.Web.UI.WebControls.TextBox txtStatus;
		protected System.Web.UI.WebControls.Label lblVersionno;
		protected System.Web.UI.WebControls.TextBox txtversionno;
		protected System.Web.UI.WebControls.Label lblQuotationDate;
		protected System.Web.UI.WebControls.Label lblSalesmanId;
		protected System.Web.UI.WebControls.TextBox txtSalesmanId;
		protected System.Web.UI.WebControls.Label lblBandId;
		protected System.Web.UI.WebControls.TextBox txtbandId;
		protected System.Web.UI.WebControls.Label lblPerctDis;
		protected System.Web.UI.WebControls.Label header2;
		protected System.Web.UI.WebControls.Label lblAttentionTo;
		protected System.Web.UI.WebControls.TextBox txtAttentionTo;
		protected System.Web.UI.WebControls.Label lblCopyto;
		protected System.Web.UI.WebControls.TextBox txtCopyTo;
		protected System.Web.UI.WebControls.Label lblRemarks;
		protected System.Web.UI.WebControls.TextBox txtRemarks;
		protected System.Web.UI.WebControls.Button btnQuery;
		protected System.Web.UI.WebControls.Button btnInsert;
		protected System.Web.UI.WebControls.Button btnSave;
		protected System.Web.UI.WebControls.Button btnDelete;
		protected System.Web.UI.WebControls.Button btnQuoted;
		protected System.Web.UI.WebControls.Button btnPrint;
		protected System.Web.UI.WebControls.Button btnNewVersion;
		protected System.Web.UI.WebControls.Button btnExceuteQuery;
		protected System.Web.UI.WebControls.Button btnEmail;
		protected System.Web.UI.WebControls.Button btnMoveFirst;
		protected System.Web.UI.WebControls.Button btnMovePrevious;
		protected System.Web.UI.WebControls.Button btnMoveNext;
		protected System.Web.UI.WebControls.Button btnMoveLast;
		SessionDS m_dsVAS=null;
		DataSet m_dsService=null;
		SessionDS m_sdsCustQuotation =null;
	//	Utility utility =null;
		string strappid=null;
		protected System.Web.UI.WebControls.DropDownList DrpCustType;
		protected System.Web.UI.WebControls.Label Label2;
		protected com.common.util.msTextBox txtQuotationDate;
		protected com.common.util.msTextBox txtPctDisc;
		protected System.Web.UI.WebControls.TextBox txtCustID;
		protected System.Web.UI.WebControls.TextBox txtCustName;
		protected System.Web.UI.WebControls.RequiredFieldValidator ValidCustCode;
		protected System.Web.UI.WebControls.RequiredFieldValidator VaildCustType;
		protected System.Web.UI.WebControls.ValidationSummary vaildPage;
		protected System.Web.UI.WebControls.TextBox txtSalesPername;
		string strenterpriseid=null;
		private String strID=null;
		//private int Crow=0;
		private int Trow=0;
		//private int CurrentSetSize=0;
		protected System.Web.UI.WebControls.RequiredFieldValidator RequiredFieldValidator1;
		private static int m_SetSize=10;
		private Agent agent;
		protected System.Web.UI.WebControls.TextBox txtRecCnt;
//		protected CrystalDecisions.Web.CrystalReportViewer CrystBandQ;
		protected System.Web.UI.HtmlControls.HtmlGenericControl divMain;
		protected System.Web.UI.HtmlControls.HtmlTable Table1;
		protected System.Web.UI.WebControls.Label lblConfirmMsg;
		protected System.Web.UI.WebControls.Button btnToSaveChanges;
		protected System.Web.UI.WebControls.Button btnNotToSave;
		protected System.Web.UI.HtmlControls.HtmlGenericControl PickupPanel;
		protected System.Web.UI.WebControls.Button btnCancel;
		protected System.Web.UI.WebControls.DataGrid dgVAS;
		protected System.Web.UI.WebControls.Button btndgVasInsert;
		protected System.Web.UI.WebControls.Label Label1;
		protected System.Web.UI.HtmlControls.HtmlTable Table2;
		protected System.Web.UI.HtmlControls.HtmlTable Table3;
		protected System.Web.UI.WebControls.Button btnCustomerId;
		protected System.Web.UI.WebControls.Button btnSalesmanId;
		protected System.Web.UI.WebControls.Button btnBandId;
		protected System.Web.UI.WebControls.Label lblCustomerName;
		protected System.Web.UI.WebControls.Label lblCustomerCode;
		protected System.Web.UI.WebControls.Label lblSalesmanName;
		protected System.Web.UI.WebControls.Label Label4;
		protected System.Web.UI.WebControls.DataGrid dgService;
		protected System.Web.UI.HtmlControls.HtmlGenericControl divVAS;
		protected System.Web.UI.HtmlControls.HtmlGenericControl divService;
		protected System.Web.UI.WebControls.Button btnServiceInsert;
		protected System.Web.UI.WebControls.ValidationSummary PageValidationSummary;
		private bool isNewRow=false;
	
		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here ***
			//utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings,Page.Session);
			strappid = utility.GetAppID();
			strenterpriseid=utility.GetEnterpriseID(); 
			
			if (!IsPostBack)
			{
				//ViewState initialisation.
				ViewState["CurrentSetSize"]=0;
				ViewState["isTextChanged"] = false;
				ViewState["ToNewVersion"] = false;
				ViewState["MovePrevious"]=false;
				ViewState["MoveNext"]=false;
				ViewState["MoveFirst"]=false;
				ViewState["MoveLast"]=false;
				ViewState["BQMode"] = ScreenMode.Query ;
				ViewState["BQOperation"]=Operation.None; 
				ViewState["ServiceMode"]=ScreenMode.Query;
				ViewState["ServiceOperation"]=Operation.None;

				m_dsVAS = new SessionDS();
				//Display grid with single row.
				m_dsVAS.ds = QuotationMgrDAl.GetEmptyVASDS();
				m_dsService = QuotationMgrDAl.GetEmptyServiceDS();
				//AddRowInVASGrid();
				dgVAS.EditItemIndex = m_dsVAS.ds.Tables[0].Rows.Count - 1;
				dgVAS.EditItemIndex = -1;
				BindVASGrid();
				//Populate CustomerType.
				LoadCustomerTypeList();
				//control validations.
				txtQuoationno.Enabled = false;
				txtversionno.Enabled = false;
				
				btnMoveFirst.Enabled = false;
				btnMoveLast.Enabled=false;
				btnMoveNext.Enabled  =false;
				btnMovePrevious.Enabled=false;  

				btnExceuteQuery.Enabled =false;				
				Utility.EnableButton(ref btnDelete,ButtonType.Save,false,m_moduleAccessRights);
				Utility.EnableButton(ref btnSave,ButtonType.Save,true,m_moduleAccessRights);
				btnQuoted.Enabled =false;
				//btnPrint.Enabled =false;
				btnNewVersion.Enabled =false;
				btnEmail.Enabled =false;
				
 
				ShowDeleteColumn(false);
				ShowServiceDeleteColumn(false);
				ShowEditableColumn(false);
				ShowServiceEditableColumn(false);
				btndgVasInsert.Enabled = false;
				btnServiceInsert.Enabled = false;
				btnServiceInsert.Enabled=false;
				//SessionDS initialisation.
				Session["SESSION_DS1"] = m_dsVAS;
				Session["SESSION_DS3"] = m_dsService;
				Session["SESSION_DS2"]=m_sdsCustQuotation;
				//START  QUERYING FIRST
				Query();
				//Display grid with single row.
				m_dsVAS.ds = QuotationMgrDAl.GetEmptyVASDS();
				m_dsService = QuotationMgrDAl.GetEmptyServiceDS();
				//AddRowInVASGrid();
				dgVAS.EditItemIndex = m_dsVAS.ds.Tables[0].Rows.Count - 1;
				dgVAS.EditItemIndex = -1;
				BindVASGrid();
				dgService.EditItemIndex = m_dsService.Tables[0].Rows.Count-1;
				dgService.EditItemIndex = -1;
				BindServiceGrid();

				btnQuoted.Enabled=false;
				btndgVasInsert.Enabled=false;
				btnServiceInsert.Enabled = false;
				toEnablePageButtons(false);
				PickupPanel.Visible = false;
				PickupPanel.Visible = false;
				divMain.Visible=true;
				btnPrint.Enabled=false;
			}
			else
			{
				m_dsVAS = (SessionDS)Session["SESSION_DS1"];
				m_dsService = (DataSet)Session["SESSION_DS3"];
				m_sdsCustQuotation = (SessionDS)Session["SESSION_DS2"];
			}
			PageValidationSummary.HeaderText = Utility.GetLanguageText(ResourceType.UserMessage, "PLS_ENTER_MISS_FIELD", utility.GetUserCulture());
		}


		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			base.OnInit(e);
			InitializeComponent();
			
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.txtQuotationDate.TextChanged += new System.EventHandler(this.txtQuotationDate_TextChanged);
			this.txtversionno.TextChanged += new System.EventHandler(this.txtversionno_TextChanged);
			this.DrpCustType.SelectedIndexChanged += new System.EventHandler(this.DrpCustType_SelectedIndexChanged);
			this.txtCustID.TextChanged += new System.EventHandler(this.txtCustID_TextChanged);
			this.btnCustomerId.Click += new System.EventHandler(this.btnCustomerId_Click);
			this.txtCustName.TextChanged += new System.EventHandler(this.txtCustName_TextChanged);
			this.txtSalesmanId.TextChanged += new System.EventHandler(this.txtSalesmanId_TextChanged);
			this.btnSalesmanId.Click += new System.EventHandler(this.btnSalesmanId_Click);
			this.txtSalesPername.TextChanged += new System.EventHandler(this.txtSalesPername_TextChanged);
			this.txtbandId.TextChanged += new System.EventHandler(this.txtbandId_TextChanged);
			this.btnBandId.Click += new System.EventHandler(this.btnBandId_Click);
			this.txtPctDisc.TextChanged += new System.EventHandler(this.txtPctDisc_TextChanged);
			this.btnQuery.Click += new System.EventHandler(this.btnQuery_Click);
			this.btnExceuteQuery.Click += new System.EventHandler(this.btnExceuteQuery_Click);
			this.btnInsert.Click += new System.EventHandler(this.btnInsert_Click);
			this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
			this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
			this.btnQuoted.Click += new System.EventHandler(this.btnQuoted_Click);
			this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
			this.btnNewVersion.Click += new System.EventHandler(this.btnNewVersion_Click);
			this.btnMoveFirst.Click += new System.EventHandler(this.btnMoveFirst_Click);
			this.btnMovePrevious.Click += new System.EventHandler(this.btnMovePrevious_Click);
			this.txtRecCnt.TextChanged += new System.EventHandler(this.txtRecCnt_TextChanged);
			this.btnMoveNext.Click += new System.EventHandler(this.btnMoveNext_Click);
			this.btnMoveLast.Click += new System.EventHandler(this.btnMoveLast_Click);
			this.txtAttentionTo.TextChanged += new System.EventHandler(this.txtAttentionTo_TextChanged);
			this.txtCopyTo.TextChanged += new System.EventHandler(this.txtCopyTo_TextChanged);
			this.txtRemarks.TextChanged += new System.EventHandler(this.txtRemarks_TextChanged);
			this.btndgVasInsert.Click += new System.EventHandler(this.btndgVasInsert_Click);
			this.btnServiceInsert.Click += new System.EventHandler(this.btnServiceInsert_Click);
			this.btnToSaveChanges.Click += new System.EventHandler(this.btnToSaveChanges_Click);
			this.btnNotToSave.Click += new System.EventHandler(this.btnNotToSave_Click);
			this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		#region VAS Grid Methods
		private void BindVASGrid()
		{
			dgVAS.DataSource = m_dsVAS.ds;
			dgVAS.DataBind();
			Session["SESSION_DS1"] = m_dsVAS;
		}
		
		private void AddRowInVASGrid()
		{
			QuotationMgrDAl.AddNewRowInVAS(m_dsVAS.ds);
			Session["SESSION_DS1"] = m_dsVAS;
		}
				
		private void ShowSearchVASButton(bool onShow)
		{
			dgVAS.Columns[3].Visible=onShow;//Allow search to be available
		}
		
		private void ShowDeleteColumn(bool onShow)
		{
			dgVAS.Columns[0].Visible=onShow;
		}
		
		private void ShowEditableColumn(bool onShow)
		{
			dgVAS.Columns[1].Visible=onShow;
		}
		
		protected void dgVAS_Cancel(object sender, DataGridCommandEventArgs e)
		{
			
			//TextBox txtVASCode = (TextBox)e.Item.FindControl("txtVASCode");
			DataRow dr = m_dsVAS.ds.Tables[0].Rows[e.Item.ItemIndex];
			if(dr["vas_code"].ToString()=="")
			{
				m_dsVAS.ds.Tables[0].Rows.RemoveAt(dgVAS.EditItemIndex);
				
			}
			dgVAS.EditItemIndex = -1;
			ShowSearchVASButton(false);
			BindVASGrid();
			btndgVasInsert.Enabled=true;
		}
		
		protected void dgVAS_Edit(object sender, DataGridCommandEventArgs e)
		{
			ShowSearchVASButton(true);
			
			dgVAS.EditItemIndex = e.Item.ItemIndex;
			
			BindVASGrid();
		}
		
		protected void dgVAS_ItemDataBound(object sender, DataGridItemEventArgs e)
		{
			if(e.Item.ItemIndex != dgVAS.EditItemIndex)
			{
				e.Item.Cells[3].Enabled = false;
			}
			if(isNewRow)
			{
				TextBox txtVASCode = (TextBox)e.Item.FindControl("txtVASCode");
				if(txtVASCode!=null)
				{
					txtVASCode.ReadOnly = false;
				}
				TextBox txtVASDesc = (TextBox)e.Item.FindControl("txtVASDesc");
				if(txtVASDesc!=null)
				{
					txtVASDesc.ReadOnly = false;
				}
			}
			
		}
		
		private void btndgVasInsert_Click(object sender, System.EventArgs e)
		{
			if(dgService.EditItemIndex != -1)
			{
				DataRow dr = m_dsService.Tables[0].Rows[dgService.EditItemIndex];
				if(dr["service_code"].ToString()=="")
				{
					m_dsService.Tables[0].Rows.RemoveAt(dgService.EditItemIndex);
				
				}
				dgService.EditItemIndex = -1;
				ShowSearchServiceButton(false);
				BindServiceGrid();
				btnServiceInsert.Enabled=true;
			}
			isNewRow=true;
			btndgVasInsert.Enabled=false;
			AddRowInVASGrid();
			ShowEditableColumn(true);
			ShowSearchVASButton(true);
			dgVAS.EditItemIndex = m_dsVAS.ds.Tables[0].Rows.Count - 1;
			BindVASGrid();
		}

		protected void dgVAS_PageChange(object sender, DataGridPageChangedEventArgs e)
		{
			dgVAS.CurrentPageIndex = e.NewPageIndex;
			BindVASGrid();
		}
		public void dgVAS_Button(object sender, DataGridCommandEventArgs e)
		{
			String strCmdNm = e.CommandName;
			if(strCmdNm.Equals("Search"))
			{
				
				TextBox txtVASCode = (TextBox)e.Item.FindControl("txtVASCode");
				String strtxtVASCode = null;
				if(txtVASCode != null)
				{
					strtxtVASCode = txtVASCode.ClientID;
				}
				
				TextBox txtVASDesc = (TextBox)e.Item.FindControl("txtVASDesc");
				String strtxtVASDesc = null;
				if (txtVASDesc != null)
				{
					strtxtVASDesc = txtVASDesc.ClientID;
				}

				msTextBox txtSurcharge = (msTextBox)e.Item.FindControl("txtSurcharge");
				String strtxtSurCharge = null;
				if (txtSurcharge != null)
				{
					strtxtSurCharge = txtSurcharge.ClientID;
				}
				String strFORMID = "BandQuotation";
				String sUrl = "VASPopup.aspx?VASID="+strtxtVASCode+"&VASDESC="+strtxtVASDesc+"&FORMID="+strFORMID+"&VASSURCHARGE="+strtxtSurCharge+"&VASCODE="+txtVASCode.Text+"&VASDESCRIPTION="+txtVASDesc.Text+" ";
				ArrayList paramList = new ArrayList();
				paramList.Add(sUrl);
				String sScript = Utility.GetScript("openWindowParam.js",paramList);
				Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
			}
			
		}

		protected void dgVAS_Delete(object sender, DataGridCommandEventArgs e)
		{
			btndgVasInsert.Enabled=true;
			DataRow drCurrent = m_dsVAS.ds.Tables[0].Rows[e.Item.ItemIndex];
			drCurrent.Delete();
			BindVASGrid();
			dgVAS.EditItemIndex = -1;
			if (((int)ViewState["BQMode"] == (int)ScreenMode.ExecuteQuery))
			{
				ViewState["BQOperation"]=Operation.Update;
			}
			if( (int)ViewState["BQMode"] == (int)ScreenMode.Insert)
			{
				ViewState["BQOperation"]=Operation.Insert;
			}
		}

		public void dgVAS_Update(object sender, DataGridCommandEventArgs e)
		{
			String strErrorMsg = "";
			String strChkVasCode = null;
			TextBox txtdgVASCode = (TextBox)e.Item.FindControl("txtVASCode");
			if(txtdgVASCode != null && txtdgVASCode.Text!="")
			{
				if(txtdgVASCode.Text.Length<12) 
				{
					VAS vas = new VAS();
					vas.Populate(strappid,strenterpriseid,txtdgVASCode.Text.Trim());
					strChkVasCode = vas.VASCode;
					if(strChkVasCode != null)
					{
						//						int cnt = m_dsVAS.ds.Tables[0].Rows.Count;
						//						int i = 0;
						//						String strDSVASCode = "";
						//
						//						for(i = 0;i<cnt;i++)
						//						{
						//							DataRow drEach = m_dsVAS.ds.Tables[0].Rows[i];
						//							strDSVASCode = (String)drEach["vas_code"];
						//							if(strDSVASCode.Equals(strChkVasCode))
						//							{
						//								lblErrrMsg.Text = strErrorMsg =  Utility.GetLanguageText(ResourceType.UserMessage,"VAS_PRESENT",utility.GetUserCulture());
						//								dgVAS.SelectedItemStyle.ForeColor = System.Drawing.Color.Red;
						//								break;
						//							}
						//						}
					}
					else
					{
						lblErrrMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"INVALID_VAS_CODE",utility.GetUserCulture());
						return;
					}
				}
				else
				{
					lblErrrMsg.Text= Utility.GetLanguageText(ResourceType.UserMessage,"VAS_MAX_INVALID",utility.GetUserCulture());
					return;
				}
				
				if(strErrorMsg.Equals(""))
				{
					lblErrrMsg.Text = "";
					DataRow drCurrent = m_dsVAS.ds.Tables[0].Rows[e.Item.ItemIndex];
					TextBox txtVASCode = (TextBox)e.Item.FindControl("txtVASCode");
					if(txtVASCode != null)
					{
						
						drCurrent["vas_code"] = txtVASCode.Text;
					}

					TextBox txtVASDesc = (TextBox)e.Item.FindControl("txtVASDesc");
					if(txtVASDesc != null)
					{
						//DataRow drCurrent = m_dsVAS.ds.Tables[0].Rows[e.Item.ItemIndex];
						drCurrent["vas_description"] = txtVASDesc.Text;
					}
			
					msTextBox txtSurcharge = (msTextBox)e.Item.FindControl("txtSurcharge");
					if(txtSurcharge != null)
					{
						//DataRow drCurrent = m_dsVAS.ds.Tables[0].Rows[e.Item.ItemIndex];
						drCurrent["surcharge"] = Convert.ToDecimal(txtSurcharge.Text);
					}

					Session["SESSION_DS1"] = m_dsVAS;
					dgVAS.EditItemIndex = -1;
					BindVASGrid();
					btndgVasInsert.Enabled=true;
				}
				//	lblErrorMsg.Text = strErrorMsg;

			}
			else
			{
				m_dsVAS.ds.Tables[0].Rows.RemoveAt(dgVAS.EditItemIndex);
				dgVAS.EditItemIndex = -1;
				BindVASGrid();
				btndgVasInsert.Enabled=true;
				
			}
			if (((int)ViewState["BQMode"] == (int)ScreenMode.ExecuteQuery))
			{
				ViewState["BQOperation"]=Operation.Update;
			}
			if( (int)ViewState["BQMode"] == (int)ScreenMode.Insert)
			{
				ViewState["BQOperation"]=Operation.Insert;
			}
			
		}

		#endregion
        
		#region Service Grid Methods
		private void BindServiceGrid()
		{
			dgService.DataSource = m_dsService;
			dgService.DataBind();
			Session["SESSION_DS3"] = m_dsService;
		}
		private void ShowSearchServiceButton(bool onShow)
		{
			dgService.Columns[3].Visible=onShow;//Allow search to be available
		}
		private void AddRowInServiceGrid()
		{
			QuotationMgrDAl.AddNewRowInService(m_dsService);
			Session["SESSION_DS3"] = m_dsService;
		}

		private void ShowServiceEditableColumn(bool onShow)
		{
			dgService.Columns[1].Visible=onShow;
		}
		private void ShowServiceDeleteColumn(bool onShow)
		{
			dgService.Columns[0].Visible=onShow;
		}

		private void btnServiceInsert_Click(object sender, System.EventArgs e)
		{
			if(dgVAS.EditItemIndex != -1)
			{
				DataRow dr = m_dsVAS.ds.Tables[0].Rows[dgVAS.EditItemIndex];
				if(dr["vas_code"].ToString()=="")
				{
					m_dsVAS.ds.Tables[0].Rows.RemoveAt(dgVAS.EditItemIndex);
				
				}
				dgVAS.EditItemIndex = -1;
				ShowSearchVASButton(false);
				BindVASGrid();
				btndgVasInsert.Enabled=true;

			}
			isNewRow=true;
			btnServiceInsert.Enabled=false;
			AddRowInServiceGrid();
			ShowServiceEditableColumn(true);
			ShowSearchServiceButton(true);
			dgService.EditItemIndex = m_dsService.Tables[0].Rows.Count - 1;
			BindServiceGrid();
		}	

		protected void dgService_PageChange(object sender, DataGridPageChangedEventArgs e)
		{
			dgService.CurrentPageIndex = e.NewPageIndex;
			BindServiceGrid();
		}
		
		public void dgService_Button(object sender, DataGridCommandEventArgs e)
		{
			String strCmdNm = e.CommandName;
			if(strCmdNm.Equals("Search"))
			{				
				TextBox txtServiceCode = (TextBox)e.Item.FindControl("txtServiceCode");
				TextBox txtServiceDesc = (TextBox)e.Item.FindControl("txtServiceDesc");
				msTextBox txtServiceChargePercent = (msTextBox)e.Item.FindControl("txtServiceChargePercent");
				msTextBox txtServiceChargeAmt = (msTextBox)e.Item.FindControl("txtServiceChargeAmt");

				String strtxtServiceCode = null;
				if(txtServiceCode != null)
					strtxtServiceCode = txtServiceCode.ClientID;			
				
				String strtxtServiceDesc = null;
				if (txtServiceDesc != null)
					strtxtServiceDesc = txtServiceDesc.ClientID;
				
				String strtxtSurCharge = null;
				if (txtServiceChargePercent != null)
					strtxtSurCharge = txtServiceChargePercent.ClientID;
				
				String strtxtSurChargeAmt = null;
				if (txtServiceChargeAmt != null)
					strtxtSurChargeAmt = txtServiceChargeAmt.ClientID;

				String strFORMID = "BandQuotation";
				String sUrl = "ServiceCodePopup.aspx?SERVICECODE="+txtServiceCode.Text+"&SERVICEDESC="+txtServiceDesc.Text+"&FORMID="+strFORMID+"&SURCHARGEID="+strtxtSurCharge+"&CODEID="+strtxtServiceCode+"&DESCID="+strtxtServiceDesc+"&SERVICE_CHARGE_AMT_ID="+strtxtSurChargeAmt+"";
				ArrayList paramList = new ArrayList();
				paramList.Add(sUrl);
				String sScript = Utility.GetScript("openWindowParam.js",paramList);
				Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
			}
			
		}
		
		protected void dgService_Delete(object sender, DataGridCommandEventArgs e)
		{
			btnServiceInsert.Enabled=true;
			DataRow drCurrent = m_dsService.Tables[0].Rows[e.Item.ItemIndex];
			drCurrent.Delete();
			BindServiceGrid();
			dgService.EditItemIndex = -1;
			if (((int)ViewState["BQMode"] == (int)ScreenMode.ExecuteQuery))
			{
				ViewState["BQOperation"]=Operation.Update;
			}
			if( (int)ViewState["BQMode"] == (int)ScreenMode.Insert)
			{
				ViewState["BQOperation"]=Operation.Insert;
			}
		}
		
		public void dgService_Update(object sender, DataGridCommandEventArgs e)
		{
			decimal intChargeP=0;
			decimal intChargeD=0;
			String strErrorMsg = "";
			String strChkServiceCode = null;
			TextBox txtServiceCode = (TextBox)e.Item.FindControl("txtServiceCode");
			TextBox txtServiceDesc = (TextBox)e.Item.FindControl("txtServiceDesc");
			msTextBox txtServiceChargePercent = (msTextBox)e.Item.FindControl("txtServiceChargePercent");
			msTextBox txtServiceChargeAmt = (msTextBox)e.Item.FindControl("txtServiceChargeAmt");

			if(txtServiceCode == null || txtServiceCode.Text=="")
			{			
				m_dsService.Tables[0].Rows.RemoveAt(dgService.EditItemIndex);
				dgService.EditItemIndex=-1;
				BindServiceGrid();
				btnServiceInsert.Enabled=true;	
				if (((int)ViewState["BQMode"] == (int)ScreenMode.ExecuteQuery))
					ViewState["BQOperation"]=Operation.Update;
				if( (int)ViewState["BQMode"] == (int)ScreenMode.Insert)
					ViewState["BQOperation"]=Operation.Insert;

				return;
			}

			if(txtServiceCode.Text.Length<12) 
			{
				Service service = new Service();
				service.Populate(strappid,strenterpriseid,txtServiceCode.Text.Trim());
				strChkServiceCode = service.ServiceCode;
				if(strChkServiceCode == null)
				{					
					lblErrrMsg.Text= Utility.GetLanguageText(ResourceType.UserMessage,"INVALID_SERVICE_CODE",utility.GetUserCulture());
					return;
				}
			}
			else
			{
				lblErrrMsg.Text= Utility.GetLanguageText(ResourceType.UserMessage,"SERVICE_EXCEED_12CHAR",utility.GetUserCulture());
				return;
			}			

			if(strErrorMsg.Equals(""))
			{

				lblErrrMsg.Text = "";		
//				try
//				{
					if(txtServiceCode != null)
					{
						DataRow drCurrent = m_dsService.Tables[0].Rows[e.Item.ItemIndex];
						drCurrent["service_code"] = txtServiceCode.Text;
					}				
					if(txtServiceDesc != null)
					{
						DataRow drCurrent = m_dsService.Tables[0].Rows[e.Item.ItemIndex];
						drCurrent["service_description"] = txtServiceDesc.Text;
					}	
					if(txtServiceChargeAmt != null)
					{
						DataRow drCurrent = m_dsService.Tables[0].Rows[e.Item.ItemIndex];
						if(txtServiceChargeAmt.Text !="")
						{
							drCurrent["service_charge_amt"] = Convert.ToDecimal(txtServiceChargeAmt.Text.ToString());						
							intChargeD=Convert.ToDecimal(txtServiceChargeAmt.Text.ToString());
						}
					}
					if(txtServiceChargePercent != null)
					{
						DataRow drCurrent = m_dsService.Tables[0].Rows[e.Item.ItemIndex];
						if(txtServiceChargePercent.Text !="")
						{
							drCurrent["service_charge_percent"] = Convert.ToDecimal(txtServiceChargePercent.Text.ToString());						
							intChargeP=Convert.ToDecimal(txtServiceChargePercent.Text.ToString());
						}
					}

					if ((intChargeD == 0) && (intChargeP == 0))
					{
						lblErrrMsg.Text= Utility.GetLanguageText(ResourceType.UserMessage,"PLS_ENTER_SERVICE_CHR",utility.GetUserCulture());
						return;
					}
					if ((intChargeD != 0) && (intChargeP != 0))
					{
						lblErrrMsg.Text= Utility.GetLanguageText(ResourceType.UserMessage,"PLS_ENTER_SERVICE_CHR_EITHER",utility.GetUserCulture());
						return;
					}
//				}
//				catch(Exception ex)
//				{
//					lblErrrMsg.Text= Utility.GetLanguageText(ResourceType.UserMessage,"DUP_KEY_FOUND_SRV_CDE",utility.GetUserCulture());
//					return;
//				}
				
				Session["SESSION_DS3"] = m_dsService;
				dgService.EditItemIndex = -1;
				BindServiceGrid();
				btnServiceInsert.Enabled=true;
			}

			if (((int)ViewState["BQMode"] == (int)ScreenMode.ExecuteQuery))
				ViewState["BQOperation"]=Operation.Update;
			if( (int)ViewState["BQMode"] == (int)ScreenMode.Insert)
				ViewState["BQOperation"]=Operation.Insert;
			
		}

		protected void dgService_Cancel(object sender, DataGridCommandEventArgs e)
		{	
			DataRow dr = m_dsService.Tables[0].Rows[e.Item.ItemIndex];
			if(dr["service_code"].ToString()=="")
			{
				m_dsService.Tables[0].Rows.RemoveAt(dgService.EditItemIndex);
				
			}
			dgService.EditItemIndex = -1;
			ShowSearchServiceButton(false);
			BindServiceGrid();
			btnServiceInsert.Enabled=true;
		}

		protected void dgService_Edit(object sender, DataGridCommandEventArgs e)
		{
			ShowSearchServiceButton(true);
			
			dgService.EditItemIndex = e.Item.ItemIndex;
			
			BindServiceGrid();
		}
		protected void dgService_ItemDataBound(object sender, DataGridItemEventArgs e)
		{
			if(e.Item.ItemIndex != dgService.EditItemIndex)
			{
				e.Item.Cells[3].Enabled = false;
			}
			if(isNewRow)
			{
				TextBox txtServiceCode = (TextBox)e.Item.FindControl("txtServiceCode");	
				TextBox txtServiceDesc = (TextBox)e.Item.FindControl("txtServiceDesc");
				if(txtServiceCode!=null)
					txtServiceCode.ReadOnly = false;
				if(txtServiceDesc!=null)
					txtServiceDesc.ReadOnly = false;
			}
		}

		#endregion

		private void btnSave_Click(object sender, System.EventArgs e)
		{
			InsertAndSaveLogic();
		}

		private bool InsertAndSaveLogic()
		{
			DateTime dtCurrentDate = DateTime.Now;  
			string strPrefix=null ;
			string strMonth=null; 
			string strYear=null;
			string strQuotNo=null;
			btnQuoted.Enabled =true;
			lblErrrMsg.Text="";
			if(dgVAS.EditItemIndex != -1)
			{
				DataRow drCurrent = m_dsVAS.ds.Tables[0].Rows[dgVAS.EditItemIndex];
				drCurrent.Delete();
				BindVASGrid();
			}
			if(dgService.EditItemIndex != -1)
			{
				DataRow drCurrent = m_dsService.Tables[0].Rows[dgService.EditItemIndex];
				drCurrent.Delete();
				BindServiceGrid();
			}
			//UPDATING LOGIC
			if ( (int)ViewState["BQOperation"]!=(int)Operation.Insert || ((int)ViewState["BQOperation"]==(int)Operation.Saved || (int)ViewState["BQOperation"] == (int)Operation.Update))
			{
				InsertCustQuotation(0);
			

				if(DrpCustType.Items.FindByValue("C").Selected)
				{
					Customer customer = new Customer();
					customer.Populate(strappid,strenterpriseid,txtCustID.Text);
					if(txtbandId.Text!="")
					{
						BandDisc banddisc = new BandDisc();
						banddisc.Populate(strappid,strenterpriseid,txtbandId.Text);
						if(banddisc.PercentageDiscount==decimal.Parse(txtPctDisc.Text))
						{
							if(customer.CustomerName != null && customer.CustomerName!="")
							{
								int iRowsEffected =0;
								if((bool)ViewState["ToNewVersion"])
								{
									iRowsEffected = QuotationMgrDAl.UpdateToNewVision(strappid,strenterpriseid,m_sdsCustQuotation.ds,m_dsVAS.ds,m_dsService);
									ViewState["ToNewVersion"] = false;
								}
								else
								{
									iRowsEffected = QuotationMgrDAl.UpdateCustQuotation(strappid,strenterpriseid,m_sdsCustQuotation.ds,m_dsVAS.ds, m_dsService);    
								}
								if(iRowsEffected>0)
								{
									ViewState["isTextChanged"] = false;
									lblErrrMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"UPD_SUCCESSFULLY",utility.GetUserCulture());
									ViewState["BQOperation"] = Operation.Saved;
									ViewState["ServiceOperation"] = Operation.Saved;
									DrpCustType.Enabled=false;
									return true;
								}
								else
								{
									lblErrrMsg.Text =Utility.GetLanguageText(ResourceType.UserMessage,"NO_RECORD_UPD",utility.GetUserCulture());
									ViewState["BQOperation"] = Operation.None;
									return false;
								}
							}
							else
							{
								lblErrrMsg.Text =Utility.GetLanguageText(ResourceType.UserMessage,"INVALID_CUSTOMER_ID",utility.GetUserCulture());
								return false;
							}
						}
						else
						{
							lblErrrMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"INVALID_BAND_ID",utility.GetUserCulture());
							return false;
						}
					}
					else
					{
						if(customer.CustomerName != null && customer.CustomerName!="")
						{
							int iRowsEffected =0;
							if((bool)ViewState["ToNewVersion"])
							{
								iRowsEffected = QuotationMgrDAl.UpdateToNewVision(strappid,strenterpriseid,m_sdsCustQuotation.ds,m_dsVAS.ds,m_dsService);
								ViewState["ToNewVersion"] = false;
							}
							else
							{
								iRowsEffected = QuotationMgrDAl.UpdateCustQuotation(strappid,strenterpriseid,m_sdsCustQuotation.ds,m_dsVAS.ds, m_dsService);    
							}
							if(iRowsEffected>0)
							{
								ViewState["isTextChanged"] = false;
								lblErrrMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"UPD_SUCCESSFULLY",utility.GetUserCulture());
								ViewState["BQOperation"] = Operation.Saved;
								//ViewState["ServiceOperation"] = Operation.Saved;
								DrpCustType.Enabled=false;
								return true;
							}
							else
							{
								lblErrrMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"NO_RECORD_UPD",utility.GetUserCulture());
								ViewState["BQOperation"] = Operation.None;
								//ViewState["ServiceOperation"] = Operation.None;
								return false;
							}
						}
						else
						{
							lblErrrMsg.Text ="Invalid customer id. Please enter a valid customer id";
							return false;
						}
					}
				}
				if(DrpCustType.Items.FindByValue("A").Selected)
				{
					agent = new Agent();
					agent.Populate(strappid,strenterpriseid,txtCustID.Text);
					if(txtbandId.Text!="")
					{
						BandDisc banddisc = new BandDisc();
						banddisc.Populate(strappid,strenterpriseid,txtbandId.Text);
						if(banddisc.PercentageDiscount==decimal.Parse(txtPctDisc.Text))
						{
							if(agent.AgentName!=null && agent.AgentName!="")
							{
								int iRowsEffected =0;
								if((bool)ViewState["ToNewVersion"])
								{
									iRowsEffected = QuotationMgrDAl.UpdateToNewVision(strappid,strenterpriseid,m_sdsCustQuotation.ds,m_dsVAS.ds,m_dsService);
									ViewState["ToNewVersion"] = false;
								}
								else
								{
									iRowsEffected = QuotationMgrDAl.UpdateAgentQuotation(strappid,strenterpriseid,m_sdsCustQuotation.ds,m_dsVAS.ds,m_dsService);
								}
								if(iRowsEffected>0)
								{
									ViewState["isTextChanged"] = false;
									lblErrrMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"UPD_SUCCESSFULLY",utility.GetUserCulture());
									ViewState["BQOperation"] = Operation.Saved;
									//ViewState["ServiceOperation"] = Operation.Saved;
									DrpCustType.Enabled=false;
									return true;
								}
								else
								{
									lblErrrMsg.Text =Utility.GetLanguageText(ResourceType.UserMessage,"NO_RECORD_UPD",utility.GetUserCulture());
									ViewState["BQOperation"] = Operation.None;
									//ViewState["ServiceOperation"] = Operation.None;
									return false;
								}
							}
							else
							{
								lblErrrMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"INVALID_AGENT_ID",utility.GetUserCulture());
								return false;
							}
						}
						else
						{
							lblErrrMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"INVALID_BAND_ID",utility.GetUserCulture());
							return false;
						}
					}
					else
					{
						if(agent.AgentName!=null && agent.AgentName!="")
						{
							int iRowsEffected =0;
							if((bool)ViewState["ToNewVersion"])
							{
								iRowsEffected = QuotationMgrDAl.UpdateToNewVision(strappid,strenterpriseid,m_sdsCustQuotation.ds,m_dsVAS.ds,m_dsService);
								ViewState["ToNewVersion"] = false;
							}
							else
							{
								iRowsEffected = QuotationMgrDAl.UpdateAgentQuotation(strappid,strenterpriseid,m_sdsCustQuotation.ds,m_dsVAS.ds, m_dsService);
							}
							if(iRowsEffected>0)
							{
								ViewState["isTextChanged"] = false;
								lblErrrMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"UPD_SUCCESSFULLY",utility.GetUserCulture());
								ViewState["BQOperation"] = Operation.Saved; 
								//ViewState["ServiceOperation"] = Operation.Saved; 
								DrpCustType.Enabled=false;
								return true;
							}
							else
							{
								lblErrrMsg.Text =Utility.GetLanguageText(ResourceType.UserMessage,"NO_RECORD_UPD",utility.GetUserCulture());
								ViewState["BQOperation"] = Operation.None;
								//ViewState["ServiceOperation"] = Operation.None;
								return false;
							}
						}
						else
						{
							lblErrrMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"INVALID_AGENT_ID",utility.GetUserCulture());
							return false;
						}
					}

				}
			
			}//INSERTING LOGIC
			if((int)ViewState["BQOperation"]==(int)Operation.Insert ) 
			{
				
					if (DrpCustType.SelectedIndex.Equals(1)) 
					{
						//InsertCustQuotation(0);
						Customer customer = new Customer();
						customer.Populate(strappid,strenterpriseid,txtCustID.Text);

						if(txtbandId.Text!="")
						{
							BandDisc banddisc = new BandDisc();
							banddisc.Populate(strappid,strenterpriseid,txtbandId.Text);
							if(txtPctDisc.Text.Trim () != "")
							{
										if(banddisc.PercentageDiscount==decimal.Parse(txtPctDisc.Text.Trim() ))
										{
											if(customer.CustomerName != null && customer.CustomerName!="")
											{
												String strNumber  = Counter.GetNextResetEveryMonth(strappid,strenterpriseid,"quotation_number") ;
												if(strNumber != null)
												{
													strPrefix = "QB";
													strMonth = dtCurrentDate.Month.ToString() ;
													if(strMonth.Length ==1)
														strMonth="0"+strMonth;
													strYear = dtCurrentDate.Year.ToString(); 
													strQuotNo = strPrefix+strYear+strMonth;
													txtQuoationno.Text =strQuotNo+strNumber; 
													InsertCustQuotation(0);
													int iRowsEffected =0;
													if((bool)ViewState["ToNewVersion"])
													{
														iRowsEffected = QuotationMgrDAl.UpdateToNewVision(strappid,strenterpriseid,m_sdsCustQuotation.ds,m_dsVAS.ds,m_dsService);
														ViewState["ToNewVersion"] = false;
													}
													else
													{
														iRowsEffected = QuotationMgrDAl.AddCustQuotation(strappid,strenterpriseid,m_sdsCustQuotation.ds,m_dsVAS.ds,m_dsService);  
													}
													lblErrrMsg.Text=Utility.GetLanguageText(ResourceType.UserMessage,"INS_SUCCESSFULLY",utility.GetUserCulture());
													Utility.EnableButton(ref btnDelete,ButtonType.Delete,true,m_moduleAccessRights);
													ViewState["BQMode"] = ScreenMode.ExecuteQuery;
													ViewState["BQOperation"] = Operation.Saved;
													DrpCustType.Enabled=false;
													return true;
												}
												else
												{
													lblErrrMsg.Text= Utility.GetLanguageText(ResourceType.UserMessage,"NO_RECORD_FOUND_QUOTN",utility.GetUserCulture());
													return false;
												}
								}
								else
								{
									lblErrrMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"INVALID_BAND_ID",utility.GetUserCulture());
									return false;
								}
							}
							else
							{
								lblErrrMsg.Text =  Utility.GetLanguageText(ResourceType.UserMessage,"INVALID_CUSTOMER_ID",utility.GetUserCulture());
								return false;
							}
						}
						else
						{
								lblErrrMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"INVALID_BAND_ID",utility.GetUserCulture());
								return false;
						}
					}
					else
					{
					if(customer.CustomerName != null && customer.CustomerName!="")
						{
								String strNumber  = Counter.GetNextResetEveryMonth(strappid,strenterpriseid,"quotation_number") ;
								if(strNumber != null)
						{
									strPrefix = "QB";
									strMonth = dtCurrentDate.Month.ToString() ;
									strYear = dtCurrentDate.Year.ToString() ; 
									strQuotNo = strPrefix+strYear+strMonth;
									txtQuoationno.Text =strQuotNo+strNumber; 
									InsertCustQuotation(0);
									int iRowsEffected =0;
									if((bool)ViewState["ToNewVersion"])
									{
										iRowsEffected = QuotationMgrDAl.UpdateToNewVision(strappid,strenterpriseid,m_sdsCustQuotation.ds,m_dsVAS.ds,m_dsService);
										ViewState["ToNewVersion"] = false;
									}
									else
									{
										iRowsEffected = QuotationMgrDAl.AddCustQuotation(strappid,strenterpriseid,m_sdsCustQuotation.ds,m_dsVAS.ds,m_dsService);  
									}
									lblErrrMsg.Text=Utility.GetLanguageText(ResourceType.UserMessage,"INS_SUCCESSFULLY",utility.GetUserCulture());
									Utility.EnableButton(ref btnDelete,ButtonType.Delete,true,m_moduleAccessRights);
									ViewState["BQOperation"] = Operation.Saved;
									DrpCustType.Enabled=false;
									return true;
								}
								else
								{
									lblErrrMsg.Text= Utility.GetLanguageText(ResourceType.UserMessage,"NO_RECORD_FOUND_QUOTN",utility.GetUserCulture());
									return false;
								}
								
							}
							else
							{
								lblErrrMsg.Text =  Utility.GetLanguageText(ResourceType.UserMessage,"INVALID_CUSTOMER_ID",utility.GetUserCulture());
								return false;
							}
						}
						
					}
					else
					{
						
						Agent agent = new Agent();
						agent.Populate(strappid,strenterpriseid,txtCustID.Text);
						if(txtbandId.Text!="")
						{
							BandDisc banddisc = new BandDisc();
							banddisc.Populate(strappid,strenterpriseid,txtbandId.Text);

						if(txtPctDisc.Text.Trim () != "")
						{
							if(banddisc.PercentageDiscount==decimal.Parse(txtPctDisc.Text))
							{
								if(agent.AgentName!=null && agent.AgentName!="")
								{
									String strNumber  = Counter.GetNextResetEveryMonth(strappid,strenterpriseid,"quotation_number") ;
									if(strNumber != null)
									{
										strPrefix = "QB";
										strMonth = dtCurrentDate.Month.ToString() ;
										strYear = dtCurrentDate.Year.ToString() ; 
										strQuotNo = strPrefix+strYear+strMonth;
										txtQuoationno.Text =strQuotNo+strNumber;
										InsertAgentQuotation(0);
										int iRowsEffected =0;
										if((bool)ViewState["ToNewVersion"])
										{
											iRowsEffected = QuotationMgrDAl.UpdateToNewVision(strappid,strenterpriseid,m_sdsCustQuotation.ds,m_dsVAS.ds,m_dsService);
											ViewState["ToNewVersion"] = false;
										}
										else
										{
											iRowsEffected = QuotationMgrDAl.AddAgentQuotation(strappid,strenterpriseid,m_sdsCustQuotation.ds,m_dsVAS.ds,m_dsService);  
										}
										lblErrrMsg.Text=Utility.GetLanguageText(ResourceType.UserMessage,"INS_SUCCESSFULLY",utility.GetUserCulture());
										Utility.EnableButton(ref btnDelete,ButtonType.Delete,true,m_moduleAccessRights);
										ViewState["BQOperation"] = Operation.Saved;
										DrpCustType.Enabled=false;
										return true;
									}
									else
									{
										lblErrrMsg.Text= Utility.GetLanguageText(ResourceType.UserMessage,"NO_RECORD_FOUND_QUOTN",utility.GetUserCulture());
										return false;
									}
								}
								else
								{
									lblErrrMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"INVALID_BAND_ID",utility.GetUserCulture());
									return false;
								}
									
								}
								else
								{
									lblErrrMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"INVALID_AGENT_ID",utility.GetUserCulture());
									return false;
								}
							}
							else
							{
								lblErrrMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"INVALID_BAND_ID",utility.GetUserCulture());
								return false;
							}
						}
						else
						{
							if(agent.AgentName!=null && agent.AgentName!="")
							{
								String strNumber  = Counter.GetNextResetEveryMonth(strappid,strenterpriseid,"quotation_number") ;
								if(strNumber != null)
								{
									strPrefix = "QB";
									strMonth = dtCurrentDate.Month.ToString() ;
									strYear = dtCurrentDate.Year.ToString() ; 
									strQuotNo = strPrefix+strYear+strMonth;
									txtQuoationno.Text =strQuotNo+strNumber;
									InsertAgentQuotation(0);
									int iRowsEffected =0;
									if((bool)ViewState["ToNewVersion"])
									{
										iRowsEffected = QuotationMgrDAl.UpdateToNewVision(strappid,strenterpriseid,m_sdsCustQuotation.ds,m_dsVAS.ds,m_dsService);
										ViewState["ToNewVersion"] = false;
									}
									else
									{
										iRowsEffected = QuotationMgrDAl.AddAgentQuotation(strappid,strenterpriseid,m_sdsCustQuotation.ds,m_dsVAS.ds,m_dsService);  
									}
									lblErrrMsg.Text=Utility.GetLanguageText(ResourceType.UserMessage,"INS_SUCCESSFULLY",utility.GetUserCulture());
									Utility.EnableButton(ref btnDelete,ButtonType.Delete,true,m_moduleAccessRights);
								
									ViewState["BQOperation"] = Operation.Saved;
									DrpCustType.Enabled=false;
									return true;
								}
								else
								{
									lblErrrMsg.Text= Utility.GetLanguageText(ResourceType.UserMessage,"NO_RECORD_FOUND_QUOTN",utility.GetUserCulture());
									return false;
								}
							}
							else
							{
								lblErrrMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"INVALID_AGENT_ID",utility.GetUserCulture());
								return false;
							}
						}
					}
				
			}
			ViewState["BQOperation"]=Operation.Update;  
			ViewState["BQMode"] = ScreenMode.ExecuteQuery;
			return false;
		}
		private void InsertCustQuotation(int iRowIndex)
		{
			DateTime strDateTime;
			m_sdsCustQuotation = QuotationMgrDAl.GetEmptyCustQuoationds();
			DataRow drEach =  m_sdsCustQuotation.ds.Tables[0].Rows[0];
  
			
			drEach["custid"] = txtCustID.Text; 
			drEach["quotation_no"]=txtQuoationno.Text;
			drEach["quotation_version"]=txtversionno.Text;
			drEach["band_code"]=txtbandId.Text;
			if (txtQuotationDate.Text.Trim().Length>0)
			{ 
					strDateTime = System.DateTime.ParseExact(txtQuotationDate.Text.Trim(),"dd/MM/yyyy HH:mm",null);
					drEach["quotation_date"]=strDateTime;
			}
			else
			{
					drEach["quotation_date"]="";
			}
			
			drEach["salesmanid"]=txtSalesmanId.Text;
			drEach["attention_to_person"]=txtAttentionTo.Text;
			drEach["copy_to_person"]=txtCopyTo.Text;
			drEach["type"]=DrpCustType.SelectedItem.Value;
			if (txtRemarks.Text.Length>200)
				drEach["remark"] = txtRemarks.Text.Substring(0,200);//txtRemarks.Text;			
			else
				drEach["remark"] = txtRemarks.Text;

			drEach["salesman_name"] = txtSalesPername.Text;
			if (txtStatus.Text.Equals("DRAFT"))
			{
					drEach["quotation_status"]="D";
			}
			if (txtStatus.Text.Equals("QUOTED"))
			{
				drEach["quotation_status"]="Q";
			}
			if (txtStatus.Text.Equals("CANCELLED"))
			{
				drEach["quotation_status"]="C";
			}
			if (txtStatus.Text.Equals("SUPERCEDED"))
			{
				drEach["quotation_status"]="S";
			}
			Session["DS2"]= m_sdsCustQuotation.ds;
			
		}
		private void InsertAgentQuotation(int iRowIndex)
		{
			DateTime strDateTime;
			m_sdsCustQuotation = QuotationMgrDAl.GetEmptyCustQuoationds();
			DataRow drEach =  m_sdsCustQuotation.ds.Tables[0].Rows[0];
  

			drEach["custid"] = txtCustID.Text; 
			drEach["quotation_no"]=txtQuoationno.Text;
			drEach["quotation_version"]=txtversionno.Text;
			drEach["band_code"]=txtbandId.Text;
			if (txtQuotationDate.Text.Trim().Length>0)
			{	 
					strDateTime = System.DateTime.ParseExact(txtQuotationDate.Text.Trim(),"dd/MM/yyyy HH:mm",null);
					drEach["quotation_date"]=strDateTime;
			}
			else
			{
					drEach["quotation_date"]="";
			}
			
			drEach["attention_to_person"]=txtAttentionTo.Text;
			drEach["type"]=DrpCustType.SelectedItem.Value;
			drEach["copy_to_person"]=txtCopyTo.Text;
			if (txtRemarks.Text.Length>200)
				drEach["remark"]=txtRemarks.Text.Substring(0,200);//txtRemarks.Text;
			else
				drEach["remark"] = txtRemarks.Text;

			drEach["salesman_name"]=txtSalesPername.Text;
			if (txtStatus.Text.Equals("DRAFT"))
			{
				drEach["quotation_status"]="D";
			}
			if (txtStatus.Text.Equals("QUOTED"))
			{
				drEach["quotation_status"]="Q";
			}
			if (txtStatus.Text.Equals("CANCELLED"))
			{
				drEach["quotation_status"]="C";
			}
			if (txtStatus.Text.Equals("SUPERCEDED"))
			{
				drEach["quotation_status"]="S";
			}
				//int iRowsEffected = QuotationMgrDAl.AddCustQuotation(strappid,strenterpriseid,m_sdsCustQuotation.ds);  
			Session["DS2"]= m_sdsCustQuotation.ds;
		}
			
		public void LoadCustomerTypeList()
		{
			ListItem defItem = new ListItem();
//			defItem.Text = "CustomeType";
//			defItem.Value = "0";
			DrpCustType.Items.Add(defItem);
			ArrayList systemCodes = Utility.GetCodeValues(strappid,utility.GetUserCulture(),"all_customer_type",CodeValueType.StringValue);
			foreach(SystemCode sysCode in systemCodes)
			{	
				ListItem lstItem = new ListItem();
				lstItem.Text = sysCode.Text;
				lstItem.Value = sysCode.StringValue;
				DrpCustType.Items.Add(lstItem);
			}
		}

		private void btnInsert_Click(object sender, System.EventArgs e)
		{
			lblErrrMsg.Text="";
			btnExceuteQuery.Enabled = false;
			btnNewVersion.Enabled = false;
			toEnablePageButtons(false);
			txtCustID.ReadOnly = false;
			ShowDeleteColumn(true);
			ShowServiceDeleteColumn(true);
			btndgVasInsert.Enabled = true;
			btnServiceInsert.Enabled = true;
			ShowSearchVASButton(false);
			toEnableForEdit(true);
			DrpCustType.Enabled = true;
			txtCustID.Enabled = true;
			btnCustomerId.Enabled = true;
			btnQuoted.Enabled=false;
			Utility.EnableButton(ref btnDelete,ButtonType.Delete,false,m_moduleAccessRights);
			Insert();
			//ViewState["BQMode"] = ScreenMode.Insert;
			//ViewState["BQOperation"] = Operation.Insert;

		}
		private void toEnablePageButtons(bool enable)
		{
			btnMoveFirst.Enabled = enable;
			btnMovePrevious.Enabled = enable;
			txtRecCnt.Enabled = enable;
			btnMoveNext.Enabled = enable;
			btnMoveLast.Enabled = enable;
		}
		private void Insert()
		{
			if ( (int)ViewState["BQOperation"] == (int)Operation.Update )
			{
			 lblConfirmMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"SAVE_PROMPT",utility.GetUserCulture());
			 PickupPanel.Visible = true;
			 divMain.Visible=false;
			 DrpCustType.Visible =false; 
			 PickupPanel.Visible = true;
			 ViewState["isTextChanged"] = false;
			 ViewState["BQMode"] = ScreenMode.Insert;
			 return;
			}
			ViewState["BQMode"] = ScreenMode.Insert;
			ViewState["BQOperation"]=Operation.Insert;  
			Utility.EnableButton(ref btnSave,ButtonType.Save,true,m_moduleAccessRights);
			txtQuoationno.Text ="";
			txtversionno.Text ="";
			txtQuotationDate.Text = DateTime.Now.ToString("dd/MM/yyyy HH:mm" ) ;
			txtversionno.Text="1";
			txtCustID.Text="";
			txtCustName.Text="";
			txtSalesmanId.Text="";
			txtSalesPername.Text="";
			txtbandId.Text="";
			txtPctDisc.Text=""; 
			txtAttentionTo.Text="";
			txtCopyTo.Text="";
			txtRemarks.Text="";
			txtStatus.Enabled = false;
			txtversionno.Text= "1";
			txtStatus.Text = "DRAFT";
			DrpCustType.SelectedItem.Selected=false;
			DrpCustType.Items.FindByValue("C").Selected=true; 
			
			m_dsVAS.ds = QuotationMgrDAl.GetEmptyVASDS();
			dgVAS.EditItemIndex = -1;
			BindVASGrid();

			m_dsService = QuotationMgrDAl.GetEmptyServiceDS();
			dgService.EditItemIndex = -1;
			BindServiceGrid();
		}
		private void btnCustomerId_Click(object sender, System.EventArgs e)
		{
			if(DrpCustType.SelectedIndex==1)
			{
				string strCustId =null;
				string strtxtCustId=null;
				if(txtCustID != null)
				{
					strtxtCustId = txtCustID.ClientID;
					strCustId = txtCustID.Text;
				}
				String sUrl = "CustomerPopup.aspx?FORMID=BandQuotation&CUSTID_TEXT="+strCustId+"&BANDCODE="+strtxtCustId;
				ArrayList paramList = new ArrayList();
				paramList.Add(sUrl);
				String sScript = Utility.GetScript("openWindowParam.js",paramList);
				Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
			}
			else if(DrpCustType.SelectedIndex==2)
			{
				string strCustId =null;
				string strtxtCustId=null;
				if(txtCustID != null)
				{
					strtxtCustId = txtCustID.ClientID;
					strCustId = txtCustID.Text;
				}
				String sUrl = "AgentPopup.aspx?FORMID=BandQuotation&CUSTID_TEXT="+strCustId+"&BANDCODE="+strtxtCustId;
				ArrayList paramList = new ArrayList();
				paramList.Add(sUrl);
				String sScript = Utility.GetScript("openWindowParam.js",paramList);
				Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
			}
		}
		private void btnBandId_Click(object sender, System.EventArgs e)
		{
			string strBandId =null;
			string strtxtBandId=null;
			if(txtbandId != null)
			{
				strtxtBandId = txtbandId.ClientID;
				strBandId = txtbandId.Text;
			}
				
			String sUrl = "BandIdPopup.aspx?FORMID=BandQuotation&BANDID_TEXT="+strBandId+"&BANDCODE="+strtxtBandId;
			ArrayList paramList = new ArrayList();
			paramList.Add(sUrl);
			String sScript = Utility.GetScript("openWindowParam.js",paramList);
			Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
		}
		private void btnDelete_Click(object sender, System.EventArgs e)
		{
			ViewState["BQOperation"] = Operation.None;
			if ((int)ViewState["BQOperation"]==(int)Operation.None )
			{
				lblConfirmMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"DEL_PROMPT",utility.GetUserCulture());
				PickupPanel.Visible = true;
				divMain.Visible=false;
				DrpCustType.Visible =false;
				ViewState["isTextChanged"] = false;
				ViewState["BQOperation"] = Operation.Delete;
			}
			else
			{
				delete();		
			}
		}
		private void delete()
		{
			int iRowEffected = QuotationMgrDAl.DeleteCustQuotation(strappid,strenterpriseid,txtCustID.Text,txtQuoationno.Text,System.Convert.ToInt16(txtversionno.Text),DrpCustType.SelectedItem.Value);  
			if(iRowEffected>0)
			{
				EmptyFields();
				lblErrrMsg.Text= Utility.GetLanguageText(ResourceType.UserMessage,"DLD_SUCCESSFULLY",utility.GetUserCulture()); 
				m_dsVAS.ds = QuotationMgrDAl.GetEmptyVASDS();
				dgVAS.EditItemIndex = -1;
				BindVASGrid();

				m_dsService = QuotationMgrDAl.GetEmptyServiceDS();
				dgService.EditItemIndex = -1;
				BindServiceGrid();

				m_sdsCustQuotation.ds.Tables[0].Rows[Convert.ToInt16(ViewState["Crow"])].Delete();
				m_sdsCustQuotation.ds.AcceptChanges();
				m_sdsCustQuotation.DataSetRecSize -=1;
				Session["SESSION_DS2"] = m_sdsCustQuotation;
				Session["SESSION_COUNT"] = m_sdsCustQuotation;
				if(Convert.ToInt16(ViewState["Crow"])>0)
				{
					ViewState["Crow"] = Convert.ToInt16(ViewState["Crow"])-1;
				}
			}
			else
			{
				lblErrrMsg.Text= Utility.GetLanguageText(ResourceType.UserMessage,"ERROR",utility.GetUserCulture());
			}	
		}

		private void btnQuery_Click(object sender, System.EventArgs e)
		{
			btndgVasInsert.Enabled=false;
			btnServiceInsert.Enabled = false;
			btnNewVersion.Enabled=false;
			btnPrint.Enabled=false;
			Query();

			dgVAS.EditItemIndex = m_dsVAS.ds.Tables[0].Rows.Count - 1;
			dgVAS.EditItemIndex = -1;
			BindVASGrid();
			dgService.EditItemIndex = m_dsService.Tables[0].Rows.Count - 1;
			dgService.EditItemIndex = -1;
			BindServiceGrid();
			m_dsService = QuotationMgrDAl.GetEmptyServiceDS();
			BindServiceGrid();

			btnQuoted.Enabled=false;
			DrpCustType.Enabled = true;
			txtCustID.Enabled = true;
			txtCustID.ReadOnly=false;
			txtQuotationDate.ReadOnly=false;
			btnCustomerId.Enabled = true;
			toEnablePageButtons(false);
		}
		private void Query()
		{
			if ((bool)ViewState["ToNewVersion"] || ((int)ViewState["BQOperation"] == (int)Operation.Update && (int)ViewState["BQMode"]==(int)ScreenMode.ExecuteQuery) )
			{
					lblConfirmMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"SAVE_PROMPT",utility.GetUserCulture());
					PickupPanel.Visible = true;
					divMain.Visible=false;
					DrpCustType.Visible =false; 
					PickupPanel.Visible = true; 

					ViewState["BQMode"] = ScreenMode.Query;
					return;
			}
				lblErrrMsg.Text="";
				ViewState["BQMode"] = ScreenMode.Query; 
				EmptyFields(); 
				txtQuoationno.Enabled = true;
				txtversionno.Enabled =true; 
				txtPctDisc.Enabled =true; 
				txtCustID.Enabled=true; 
				btnExceuteQuery.Enabled = true; 
				Utility.EnableButton(ref btnInsert,ButtonType.Insert,true,m_moduleAccessRights);
				Utility.EnableButton(ref btnSave,ButtonType.Save,true,m_moduleAccessRights);

			DrpCustType.SelectedItem.Selected=false;   
			DrpCustType.Items.FindByText("").Selected=true;
		}
		private void btnExceuteQuery_Click(object sender, System.EventArgs e)
		{
			btnExceuteQuery.Enabled = false;
			toEnablePageButtons(true);
			DrpCustType.Enabled = false;
			txtCustID.Enabled = false;
			btnCustomerId.Enabled = false;
			btnPrint.Enabled =true;
			lblErrrMsg.Text="";
			btnPrint.Enabled=true;
			string strCType = DrpCustType.SelectedItem.Value;
		
			//Populating the CustomerQuoation Dataset
			ViewState["QryRes"]=0;
			ViewState["CurrentSetSize"]=0;
			ViewState["isTextChanged"] = false;
			InsertCustQuotation(0);
			ViewState["QUERY_DS"] = m_sdsCustQuotation.ds;
			//m_sdsCustQuotation.DataSetRecSize = 10;
			m_sdsCustQuotation = QuotationMgrDAl.GetCustQuotation(strappid,strenterpriseid,0,m_SetSize,m_sdsCustQuotation.ds,strCType);  
			ViewState["QryRes"] =  m_sdsCustQuotation.QueryResultMaxSize; 

			if ((decimal)ViewState["QryRes"]!=0)
			{
				Trow = System.Convert.ToInt32(m_sdsCustQuotation.DataSetRecSize);
				ExtractData(0);
				ViewState["Crow"]=0;

				//btnQuoted.Enabled =true;
				//btndgVasInsert.Enabled = true;
				ViewState["BQMode"] = ScreenMode.ExecuteQuery ;
				//ViewState["BQOperation"]=Operation.Update;  
				txtQuoationno.Enabled = false;
				txtversionno.Enabled =false;
				txtStatus.Enabled =false;
				//txtCustID.Enabled =false;
				txtCustID.ReadOnly = true;
				txtPctDisc.ReadOnly =true; 
				  
				btnMoveFirst.Enabled = true;
				btnMoveLast.Enabled=true;
				btnMoveNext.Enabled =true;
				btnMovePrevious.Enabled=true;  
			}
			else
			{
				lblErrrMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"NO_RECORD_FOUND",utility.GetUserCulture());
			}

			Session["SESSION_DS2"] = m_sdsCustQuotation;
			Session["SESSION_COUNT"] = m_sdsCustQuotation;
		}
		private void ExtractData(int iRow)
		{
			
			DataRow drEach = m_sdsCustQuotation.ds.Tables[0].Rows[iRow];
			if(!drEach["quotation_no"].GetType().Equals(System.Type.GetType("System.DBNull")))
			{
				txtQuoationno.Text = drEach["quotation_no"].ToString();
			}
			if(!drEach["quotation_version"].GetType().Equals(System.Type.GetType("System.DBNull")))
			{
				txtversionno.Text = drEach["quotation_version"].ToString();
			}
			if(!drEach["cust_name"].GetType().Equals(System.Type.GetType("System.DBNull")))
			{
				
				txtCustName.Text = drEach["cust_name"].ToString();
			}
			if(!drEach["quotation_date"].GetType().Equals(System.Type.GetType("System.DBNull")))
			{
				DateTime dt = (DateTime)drEach["quotation_date"];
				txtQuotationDate.Text = dt.ToString("dd/MM/yyyy HH:mm");
			}
			if(!drEach["remark"].GetType().Equals(System.Type.GetType("System.DBNull")))
			{
				txtRemarks.Text = drEach["remark"].ToString();
			}
			if(m_sdsCustQuotation.ds.Tables[0].Columns.Contains("salesmanid"))   
			{
				
				if(!drEach["salesmanid"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					txtSalesmanId.Text = drEach["salesmanid"].ToString();
				}
			}
			if(m_sdsCustQuotation.ds.Tables[0].Columns.Contains("salesman_name"))   
			{
				
				if(!drEach["salesman_name"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					txtSalesPername.Text = drEach["salesman_name"].ToString();
				}
			}
			if(drEach["type"].ToString() == "C")
			{
				DrpCustType.SelectedIndex = 1;
			}
			else
			{
				DrpCustType.SelectedIndex = 2;
			}
			if(m_sdsCustQuotation.ds.Tables[0].Columns.Contains("custid"))   
			{
				if(!drEach["custid"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					txtCustID.Text = drEach["custid"].ToString();
				}
			}
			else
			{
				if(!drEach["agentid"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					txtCustID.Text = drEach["agentid"].ToString();
				}
			}
			BandDisc  banddisc = new BandDisc();
			if(!drEach["band_code"].GetType().Equals(System.Type.GetType("System.DBNull")))
			{
				txtbandId.Text = drEach["band_code"].ToString();
			}
			banddisc.Populate(strappid,strenterpriseid,	txtbandId.Text.Trim());
			txtPctDisc.Text = banddisc.PercentageDiscount.ToString() ;  
			if(!drEach["attention_to_person"].GetType().Equals(System.Type.GetType("System.DBNull")))
			{
				txtAttentionTo.Text = drEach["attention_to_person"].ToString() ;
			}
			if(!drEach["copy_to_person"].GetType().Equals(System.Type.GetType("System.DBNull")))
			{
				txtCopyTo.Text = drEach["copy_to_person"].ToString() ;
			}
			if(!drEach["quotation_status"].GetType().Equals(System.Type.GetType("System.DBNull")))
			{
				txtStatus.Text =drEach["quotation_status"].ToString();
				if(txtStatus.Text=="D")
				{
					toEnableForEdit(true);
					btnNewVersion.Enabled=false;
					ShowDeleteColumn(true);
					ShowServiceDeleteColumn(true);
					ShowEditableColumn(true);
					ShowSearchServiceButton(true);
					ShowSearchVASButton(true);
					ShowServiceDeleteColumn(true);
					ShowServiceEditableColumn(true);
					Utility.EnableButton(ref btnSave,ButtonType.Save,true,m_moduleAccessRights);
				}
				else
				{
					toEnableForEdit(false);
					ShowDeleteColumn(false);
					ShowEditableColumn(false);
					ShowSearchServiceButton(false);
					ShowSearchVASButton(false);
					ShowServiceDeleteColumn(false);
					ShowServiceEditableColumn(false);
					if(txtStatus.Text=="Q")
					{
						btnNewVersion.Enabled=true;
					}
					else
					{
						btnNewVersion.Enabled=false;
					}
					if(txtStatus.Text.ToUpper()=="C")
					{
						Utility.EnableButton(ref btnSave,ButtonType.Save,false,m_moduleAccessRights);
					}
					else
					{
						Utility.EnableButton(ref btnSave,ButtonType.Save,true,m_moduleAccessRights);
					}
				}
			}
			ArrayList systemCodes = Utility.GetCodeValues(strappid,utility.GetUserCulture(),"quotation_status",CodeValueType.StringValue);
			foreach(SystemCode sysCode in systemCodes)
			{	
				if (sysCode.StringValue.Equals(drEach["quotation_status"]))
				{
					txtStatus.Text = sysCode.Text;
					ViewState["Status"]=sysCode.Text;
				}
			}
			
			if(m_sdsCustQuotation.ds.Tables[0].Columns.Contains("agentid"))
			{
				strID = drEach["agentid"].ToString();
			}
			else
			{
				strID = drEach["custid"].ToString();
			}

			if(drEach["type"].ToString()=="C")   
			{
				m_dsVAS = QuotationMgrDAl.GetCustVas(strappid,strenterpriseid,strID ,drEach["quotation_no"].ToString(),Convert.ToDecimal(drEach["quotation_version"]), 0, dgVAS.PageSize);
				m_dsService = QuotationMgrDAl.GetCustService(strappid,strenterpriseid,strID ,drEach["quotation_no"].ToString(),Convert.ToDecimal(drEach["quotation_version"]));
			
			}
			else   
			{
				m_dsVAS.ds = QuotationMgrDAl.GetAgentVas(strappid,strenterpriseid, strID,drEach["quotation_no"].ToString(),Convert.ToDecimal(drEach["quotation_version"]));
				m_dsService = QuotationMgrDAl.GetAgentService(strappid,strenterpriseid, strID,drEach["quotation_no"].ToString(),Convert.ToDecimal(drEach["quotation_version"]));
			}
			BindVASGrid();
			BindServiceGrid();
  		}
		private void toEnableForEdit(bool enable)
		{
			btnQuoted.Enabled = enable;
			Utility.EnableButton(ref btnDelete,ButtonType.Delete,enable,m_moduleAccessRights);
			
			btndgVasInsert.Enabled = enable;
			btnServiceInsert.Enabled = enable;
			ShowDeleteColumn(enable);
			ShowEditableColumn(enable);
			txtQuoationno.Enabled = false;
			txtQuotationDate.Enabled = enable;

			txtCustName.Enabled = enable;
			txtSalesmanId.Enabled = enable;
			btnSalesmanId.Enabled = enable;
			txtSalesPername.Enabled = enable;
			txtbandId.Enabled = enable;
			btnBandId.Enabled = enable;
			txtPctDisc.ReadOnly = true;
			txtAttentionTo.Enabled = enable;
			txtCopyTo.Enabled = enable;
			txtRemarks.ReadOnly = !enable;

		}
		private void btnMoveNext_Click(object sender, System.EventArgs e)
		{
		MoveNext();	
		}
		private void MoveNext()
		{
			m_sdsCustQuotation =(SessionDS)Session["SESSION_COUNT"];
			lblErrrMsg.Text="";
			if ((bool)ViewState["isTextChanged"] == true)
			{
				lblConfirmMsg.Text =  Utility.GetLanguageText(ResourceType.UserMessage,"SAVE_PROMPT",utility.GetUserCulture());
				PickupPanel.Visible = true;
				divMain.Visible=false;
				DrpCustType.Visible =false;
				PickupPanel.Visible = true; 
				ViewState["isTextChanged"] = false;
				ViewState["MoveNext"]=true;
				return;
			}
			EmptyFields();
			ViewState["iTotalRec"]=0;																			
			if(Convert.ToDecimal(ViewState["Crow"])  < (m_sdsCustQuotation.DataSetRecSize-1) )
			{
				ViewState["Crow"]= Convert.ToDecimal(ViewState["Crow"])+1;
				ExtractData(Convert.ToInt32(ViewState["Crow"])); 	
			}
			else
			{				
				int m_StartIndex = (int)ViewState["CurrentSetSize"]* m_SetSize;
				ViewState["iTotalRec"] = m_StartIndex + m_sdsCustQuotation.DataSetRecSize;
				if( (decimal)ViewState["iTotalRec"] >=  m_sdsCustQuotation.QueryResultMaxSize)
				{
					return;
				}
				
				ViewState["CurrentSetSize"]=(int)ViewState["CurrentSetSize"]+1;	
				m_StartIndex = (int)ViewState["CurrentSetSize"]* m_SetSize; 
				
				DataSet dsQuery = (DataSet)ViewState["QUERY_DS"];
				DataRow dr = dsQuery.Tables[0].Rows[0];
				m_sdsCustQuotation = QuotationMgrDAl.GetCustQuotation(strappid,strenterpriseid,m_StartIndex,m_SetSize,dsQuery,dr["type"].ToString() );  
				Session["SESSION_DS1"] = m_sdsCustQuotation;
				ViewState["Crow"] = 0;
				ExtractData(Convert.ToInt32(ViewState["Crow"]));
			
			}
			Session["SESSION_DS1"] = m_dsVAS; 
			Session["SESSION_DS2"] = m_sdsCustQuotation;
			Session["SESSION_COUNT"] = m_sdsCustQuotation;
		}
		private void btnMovePrevious_Click(object sender, System.EventArgs e)
		{
			MovePrevious();
		}
		private void MovePrevious()
		{
			m_sdsCustQuotation =(SessionDS)Session["SESSION_COUNT"];
			lblErrrMsg.Text="";

			if ((bool)ViewState["isTextChanged"] == true)
			{
				lblConfirmMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"SAVE_PROMPT",utility.GetUserCulture());
				PickupPanel.Visible = true;
				divMain.Visible=false;
				DrpCustType.Visible =false; 
				PickupPanel.Visible = true; 
				ViewState["isTextChanged"] = false;
				ViewState["MovePrevious"]=true;
				return;
			}
			EmptyFields();
			if(Convert.ToInt32(ViewState["Crow"])  > 0 )
			{
				ViewState["Crow"]= Convert.ToDecimal ( ViewState["Crow"])-1;
				ExtractData(Convert.ToInt32(ViewState["Crow"]));
							
			}
			else
			{		
				if ((int)ViewState["CurrentSetSize"]==0)
				{			
					ExtractData(Convert.ToInt32(ViewState["Crow"]));
					
					return;
				}
				else
				{	
					ViewState["CurrentSetSize"]=(int)ViewState["CurrentSetSize"]-1;	
				}
				int m_StartIndex = (int)ViewState["CurrentSetSize"]* m_SetSize; 
				
				DataSet dsQuery = (DataSet)ViewState["QUERY_DS"];
				DataRow dr = dsQuery.Tables[0].Rows[0];
				//m_sdsCustQuotation.DataSetRecSize=10;
				m_sdsCustQuotation = QuotationMgrDAl.GetCustQuotation(strappid,strenterpriseid,m_StartIndex,m_SetSize,dsQuery,dr["type"].ToString() );  
				Session["SESSION_DS1"] = m_sdsCustQuotation;
				ViewState["Crow"]=m_sdsCustQuotation.DataSetRecSize-1; 
				ExtractData(Convert.ToInt32(ViewState["Crow"]));
			}
			Session["SESSION_DS2"] = m_sdsCustQuotation;
			Session["SESSION_COUNT"] = m_sdsCustQuotation;
		}
		private void btnMoveFirst_Click(object sender, System.EventArgs e)
		{
			MoveFirst();
		}
		private void MoveFirst()
		{
			m_sdsCustQuotation =(SessionDS)Session["SESSION_COUNT"];
			lblErrrMsg.Text="";

			if ((bool)ViewState["isTextChanged"] == true)
			{
				lblConfirmMsg.Text =  Utility.GetLanguageText(ResourceType.UserMessage,"SAVE_PROMPT",utility.GetUserCulture());
				PickupPanel.Visible = true;
				divMain.Visible=false;
				DrpCustType.Visible =false; 
				PickupPanel.Visible = true; 
				ViewState["isTextChanged"] = false;
				ViewState["MoveFirst"]=true;			
				return;
			}
			EmptyFields(); 

			m_sdsCustQuotation.ds = (DataSet)ViewState["QUERY_DS"];
			DataRow dr = m_sdsCustQuotation.ds.Tables[0].Rows[0];
						
			m_sdsCustQuotation = QuotationMgrDAl.GetCustQuotation(strappid,strenterpriseid,0,m_SetSize,m_sdsCustQuotation.ds ,dr["type"].ToString() );  
			ViewState["Crow"]=0;
			ExtractData((int)ViewState["Crow"]);
			ViewState["CurrentSetSize"]=0;
			Session["SESSION_DS2"] = m_sdsCustQuotation;
			Session["SESSION_COUNT"] = m_sdsCustQuotation;
		}
		private void EmptyFields()
		{
			txtQuoationno.Text="";
			txtRecCnt.Text="";
			txtversionno.Text="";
			txtQuotationDate.Text="";
			txtStatus.Text="";
			txtCustID.Text="";
			txtCustName.Text="";
			txtSalesmanId.Text="";
			txtSalesPername.Text="";
			txtbandId.Text="";
			txtPctDisc.Text="";
			txtAttentionTo.Text="";
			txtCopyTo.Text="";
			txtRemarks.Text="";
			m_dsVAS.ds = QuotationMgrDAl.GetEmptyVASDS();
			BindVASGrid();
		}
		private void btnMoveLast_Click(object sender, System.EventArgs e)
		{
			MoveLast();
		}
		private void MoveLast()
		{
			m_sdsCustQuotation =(SessionDS)Session["SESSION_COUNT"];
			lblErrrMsg.Text="";

			if ((bool)ViewState["isTextChanged"] == true)
			{
				lblConfirmMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"SAVE_PROMPT",utility.GetUserCulture());
				PickupPanel.Visible = true;
				divMain.Visible=false;
				DrpCustType.Visible = false; 
				PickupPanel.Visible = true; 
				ViewState["isTextChanged"] = false;
				ViewState["MoveLast"]=true;			
				return;
			}
					
			int Qrysize=0;
			EmptyFields(); 
			if (m_sdsCustQuotation.QueryResultMaxSize > 10)
			{
				Qrysize = System.Convert.ToInt32(m_sdsCustQuotation.QueryResultMaxSize-10);
			}
			else
			{
				Qrysize=0;
			}
					
			m_sdsCustQuotation.ds = (DataSet)ViewState["QUERY_DS"];
			DataRow dr = m_sdsCustQuotation.ds.Tables[0].Rows[0];
			
			m_sdsCustQuotation = QuotationMgrDAl.GetCustQuotation (strappid,strenterpriseid,Qrysize,m_SetSize,m_sdsCustQuotation.ds,dr["type"].ToString());  
			ViewState["Crow"]=m_sdsCustQuotation.DataSetRecSize-1;
			ExtractData(Convert.ToInt32(ViewState["Crow"]));

			ViewState["CurrentSetSize"] = System.Convert.ToInt32(m_sdsCustQuotation.QueryResultMaxSize )/ m_SetSize;
			
			Session["SESSION_DS2"] = m_sdsCustQuotation;
			Session["SESSION_COUNT"] = m_sdsCustQuotation;
		}


		private void TextChanged()
		{
			if((int)ViewState["BQMode"] == (int)ScreenMode.ExecuteQuery)
			{
				ViewState["BQOperation"]=Operation.Update;
			}
			else
			{
				ViewState["BQOperation"]=Operation.Insert;
			}
			ViewState["isTextChanged"]=true;
		}

		private void txtversionno_TextChanged(object sender, System.EventArgs e)
		{
			TextChanged();
		}

		private void txtQuotationDate_TextChanged(object sender, System.EventArgs e)
		{			
			TextChanged();
		}

		private void DrpCustType_SelectedIndexChanged(object sender, System.EventArgs e)
		{			
			txtCustID.Text="";
			txtCustName.Text="";
			TextChanged();
		}

		private void txtCustID_TextChanged(object sender, System.EventArgs e)
		{		
			TextChanged();
		}

		private void txtSalesmanId_TextChanged(object sender, System.EventArgs e)
		{
			TextChanged();
		}

		private void txtCustName_TextChanged(object sender, System.EventArgs e)
		{
			TextChanged();
		}

		private void txtSalesPername_TextChanged(object sender, System.EventArgs e)
		{
			TextChanged();
		}

		private void txtbandId_TextChanged(object sender, System.EventArgs e)
		{
			TextChanged();
		}

		private void txtPctDisc_TextChanged(object sender, System.EventArgs e)
		{
			TextChanged();
		}

		private void txtAttentionTo_TextChanged(object sender, System.EventArgs e)
		{
			TextChanged();
		}

		private void txtCopyTo_TextChanged(object sender, System.EventArgs e)
		{
			TextChanged();
		}

		private void txtRemarks_TextChanged(object sender, System.EventArgs e)
		{
			TextChanged();
			if (txtRemarks.Text.Length > 200) 
			{
				lblErrrMsg.Text="Remarks exceeds limit. First 200 Characters will be used.";
				txtRemarks.Text=txtRemarks.Text.Substring(0,200);
			}
		}
		
		private void txtRecCnt_TextChanged(object sender, System.EventArgs e)
		{
			ViewState["Crow"]= int.Parse(txtRecCnt.Text);
			RecCntMove();

		}
		
		private void btnQuoted_Click(object sender, System.EventArgs e)
		{
			InsertCustQuotation(0);
			int numAffected = QuotationMgrDAl.MarkQuoted(strappid,strenterpriseid,m_sdsCustQuotation.ds);
			if(numAffected>0)
			{
				SessionDS sdsRow = (SessionDS)Session["SESSION_DS2"]; 
				lblErrrMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"QUOTN_QUOTED",utility.GetUserCulture()); 
				txtStatus.Text = "QUOTED";
				DataRow dr = sdsRow.ds.Tables[0].Rows[Convert.ToInt32(ViewState["Crow"])];
				dr["quotation_status"] = txtStatus.Text;
				btnNewVersion.Enabled = true;
				toEnableForEdit(false);
			}
			else
			{
				lblErrrMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"QUOTN_NO_QUOTED",utility.GetUserCulture());
				btnQuoted.Enabled = true;
			}
		}

		private void btnNewVersion_Click(object sender, System.EventArgs e)
		{
			lblErrrMsg.Text = "";
			toEnableForEdit(true);
			ShowServiceDeleteColumn(true);
			ShowDeleteColumn(true);
			toEnablePageButtons(false);
			btnNewVersion.Enabled=false;
			String strversionNum = txtversionno.Text;
			int iVer = int.Parse(strversionNum);
			txtversionno.Text = iVer+1+"";
			txtCustID.Enabled = false;
			txtStatus.Text = "DRAFT";

			Utility.EnableButton(ref btnInsert,ButtonType.Insert,false,m_moduleAccessRights);
			txtQuotationDate.ReadOnly = false;
			txtQuotationDate.Enabled=true;
			txtCustID.Enabled =false;
			btnCustomerId.Enabled=true;
			Utility.EnableButton(ref btnDelete,ButtonType.Delete,false,m_moduleAccessRights);
			btnQuoted.Enabled = false;
			
			ViewState["BQOperation"] = Operation.None;

			m_dsVAS.ds = QuotationMgrDAl.GetEmptyVASDS();
			dgVAS.EditItemIndex = -1;
			BindVASGrid();

			m_dsService = QuotationMgrDAl.GetEmptyServiceDS();
			dgService.EditItemIndex = -1;
			BindServiceGrid();

			ViewState["ToNewVersion"] = true;
		}

		
		private void RecCntMove()
		{
			m_sdsCustQuotation =(SessionDS)Session["SESSION_COUNT"];
			lblErrrMsg.Text="";

			if ((bool)ViewState["isTextChanged"] == true)
			{
				lblConfirmMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"SAVE_PROMPT",utility.GetUserCulture());
				PickupPanel.Visible = true;
				divMain.Visible=false;
				DrpCustType.Visible =false;
				PickupPanel.Visible = true; 
				ViewState["isTextChanged"] = false;
				ViewState["MoveNext"]=true;
				return;
			}
			ViewState["iTotalRec"]=0;																			
			if((int)ViewState["Crow"]  < (m_sdsCustQuotation.DataSetRecSize-1) )
			{
				ViewState["Crow"]= int.Parse(txtRecCnt.Text);
				ExtractData((int)ViewState["Crow"]); 
	
			}
			else
			{
				
				int m_StartIndex = (int)ViewState["CurrentSetSize"]* m_SetSize;
				ViewState["iTotalRec"] = m_StartIndex + m_sdsCustQuotation.DataSetRecSize;
				if( (int)ViewState["iTotalRec"] >=  m_sdsCustQuotation.QueryResultMaxSize)
				{
					return;

				}
				
				ViewState["CurrentSetSize"]=(int)ViewState["CurrentSetSize"]+1;	
				m_StartIndex = (int)ViewState["CurrentSetSize"]* m_SetSize; 
				
				DataSet dsQuery = (DataSet)ViewState["QUERY_DS"];
				DataRow dr = dsQuery.Tables[0].Rows[0];
				m_sdsCustQuotation = QuotationMgrDAl.GetCustQuotation(strappid,strenterpriseid,m_StartIndex,m_SetSize,dsQuery,dr["type"].ToString() );  
				Session["SESSION_DS1"] = m_sdsCustQuotation;
				ViewState["Crow"] = 0;
				ExtractData((int)ViewState["Crow"]);
			
			}
			Session["SESSION_DS1"] = m_dsVAS; 
			Session["SESSION_DS2"] = m_sdsCustQuotation;
			Session["SESSION_COUNT"] = m_sdsCustQuotation;
		}

		private void btnPrint_Click(object sender, System.EventArgs e)
		{
			Session["QUOTATIONNO"] =txtQuoationno.Text;
			Session["CUSTTYPE"]= DrpCustType.SelectedItem.Value;
			Session["FORMID"] = "BandQuotation";

			//String sUrl = "BandQuotationRpt.aspx?QUOTATIONNO="+txtQuoationno.Text+"&CUSTTYPE="+DrpCustType.SelectedItem.Value+"&FORMID="+"BandQuotation"; 
			String sUrl = "BandQuotationReport.aspx";
			ArrayList paramList = new ArrayList();
			paramList.Add(sUrl);
			String sScript = Utility.GetScript("openParentWindow.js",paramList);
			Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
		}

		private void btnSalesmanId_Click(object sender, System.EventArgs e)
		{
			string sQrySalesmanId =txtSalesmanId.Text.Trim();			
			String sUrl = "SalesPersonPopup.aspx?FORMID=BandQuotation&QUERY_SALESMANID="+sQrySalesmanId;
			ArrayList paramList = new ArrayList();
			paramList.Add(sUrl);
			String sScript = Utility.GetScript("openWindowParam.js",paramList);
			Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);			
		}

		private void btnCancel_Click(object sender, System.EventArgs e)
		{
			PickupPanel.Visible = false;
			divMain.Visible=true;
		}	
		
		private void btnToSaveChanges_Click(object sender, System.EventArgs e)
		{
			bool success=false;
			if((int)ViewState["BQOperation"] != (int)Operation.Delete)
			{
				success = InsertAndSaveLogic();
			}
			bool toQuery =false;
			bool toInsert = false;
			if((int)ViewState["BQOperation"] == (int)Operation.Delete)
			{
				ViewState["BQOperation"] = Operation.None;
				delete();
			}
			
			if((bool)ViewState["MoveNext"])
			{
				ViewState["MoveNext"]=false;
				MoveNext();
			}
			if((bool)ViewState["MovePrevious"])
			{
				ViewState["MovePrevious"]=false;
				MovePrevious();
			}
			if((bool)ViewState["MoveFirst"])
			{
				ViewState["MoveFirst"]=false;
				MoveFirst();
			}
			if((bool)ViewState["MoveLast"])
			{
				ViewState["MoveLast"]=false;
				MoveLast();
			}
			if( (int)ViewState["BQMode"] == (int)ScreenMode.Query && (int)ViewState["BQOperation"] == (int)Operation.Saved) 
			{
				toQuery=true;
				
			}											  
			if( (int)ViewState["BQMode"] == (int)ScreenMode.Insert && (int)ViewState["BQOperation"] == (int)Operation.Saved)
			{
				toInsert=true;
			}
			ViewState["isTextChanged"] = false;
			PickupPanel.Visible = false;
			divMain.Visible=true;
			ViewState["isTextChange"] = false;
			DrpCustType.Visible =true; 
			btndgVasInsert.Enabled = true;
			btnServiceInsert.Enabled = true;
			
			if(success)
			{
				
				if(toInsert)
				{
					EmptyFields();
					toEnableForEdit(true);
					DrpCustType.Enabled = true;
					txtCustID.Enabled = true;
					btnCustomerId.Enabled = true;
					btnQuoted.Enabled=false;
					Utility.EnableButton(ref btnDelete,ButtonType.Delete,false,m_moduleAccessRights);
					txtversionno.Text="1";//HARDCODED
					txtQuotationDate.Text = DateTime.Now.ToString("dd/MM/yyyy HH:mm" ) ;
					txtQuotationDate.ReadOnly=true;
				}
				if(toQuery)
				{
					EmptyFields();
					lblErrrMsg.Text="";
					ViewState["BQMode"] = ScreenMode.Query; 
					EmptyFields(); 
					txtQuoationno.Enabled = true;
					txtversionno.Enabled =true; 
					txtPctDisc.Enabled =true; 
					txtCustID.Enabled=true; 
					btnExceuteQuery.Enabled = true; 
					Utility.EnableButton(ref btnInsert,ButtonType.Insert,true,m_moduleAccessRights);
					Utility.EnableButton(ref btnSave,ButtonType.Save,true,m_moduleAccessRights);

					DrpCustType.SelectedItem.Selected=false;   
					DrpCustType.Items.FindByText("").Selected=true;

					dgVAS.EditItemIndex = m_dsVAS.ds.Tables[0].Rows.Count - 1;
					dgVAS.EditItemIndex = -1;
					BindVASGrid();
					btnQuoted.Enabled=false;
					DrpCustType.Enabled = true;
					txtCustID.Enabled = true;
					txtCustID.ReadOnly=false;
					txtQuotationDate.ReadOnly=false;
					btnCustomerId.Enabled = true;
					toEnablePageButtons(false);
					btndgVasInsert.Enabled=false;
					btnServiceInsert.Enabled = false;
				}
			}
			
		}
		private void btnToCancel_Click(object sender, System.EventArgs e)
		{
			ViewState["isTextChanged"] = false;
			PickupPanel.Visible = false;
			divMain.Visible=true;
			DrpCustType.Visible =true; 
			//EmptyFields();
		}

		private void btnNotToSave_Click(object sender, System.EventArgs e)
		{		
			if ((int)ViewState["BQMode"]==(int)ScreenMode.Insert) 
			{
				PickupPanel.Visible = false;
				divMain.Visible=true;
				ViewState["isTextChange"] = false;
				DrpCustType.Visible =true; 
				EmptyFields();
				ViewState["BQOperation"]= Operation.Insert;

				lblErrrMsg.Text="";
				btnExceuteQuery.Enabled = false;
				btnNewVersion.Enabled = false;
				toEnablePageButtons(false);
				txtCustID.ReadOnly = false;
			
				btndgVasInsert.Enabled = true;
				btnServiceInsert.Enabled = true;
				ShowSearchVASButton(false);
				toEnableForEdit(true);
				DrpCustType.Enabled = true;
				txtCustID.Enabled = true;
				btnCustomerId.Enabled = true;
				btnQuoted.Enabled=false;
				Utility.EnableButton(ref btnDelete,ButtonType.Delete,false,m_moduleAccessRights);
				ViewState["BQOperation"] = Operation.Insert;
				Insert();				
			}

			else if ((int)ViewState["BQMode"]==(int)ScreenMode.Query) 
			{
				PickupPanel.Visible = false;
				divMain.Visible=true;
				ViewState["isTextChange"] = false;
				ViewState["ToNewVersion"] = false;
				DrpCustType.Visible =true; 
				EmptyFields(); 
				Query();
			}
			else if((bool)ViewState["MovePrevious"]==true) 
			{
				ViewState["isTextChange"] = false;
				MovePrevious();
				PickupPanel.Visible = false;
				divMain.Visible=true;				
				DrpCustType.Visible =true; 
			}

			else if((bool)ViewState["MoveNext"]==true) 
			{
				ViewState["isTextChange"] = false;
				ViewState["BQOperation"] = Operation.None;
				MoveNext();
				PickupPanel.Visible = false;
				divMain.Visible=true;				
				DrpCustType.Visible =true;
			}

			else if((bool)ViewState["MoveLast"]==true) 
			{
				ViewState["isTextChange"] = false;
				ViewState["BQOperation"] = Operation.None;
				MoveLast();
				PickupPanel.Visible = false;
				divMain.Visible=true;	
				DrpCustType.Visible =true;
			}

			else if((bool)ViewState["MoveFirst"]==true) 
			{
				ViewState["isTextChange"] = false;
				ViewState["BQOperation"] = Operation.None;
				MoveFirst();
				PickupPanel.Visible = false;
				divMain.Visible=true;	
				DrpCustType.Visible =true;
			}

			else
			{
				PickupPanel.Visible = false;
				ViewState["BQOperation"] = Operation.None;
				divMain.Visible=true;	
				DrpCustType.Visible =true;
				ViewState["isTextChange"] = false;
			}
		}

		private void rblQuoteType_SelectedIndexChanged(object sender, System.EventArgs e)
		{
		
		}
		
		

	}
}
