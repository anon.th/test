using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.common.classes;
using com.common.util;
using com.ties.DAL;
using com.common.applicationpages;

namespace com.ties
{
	/// <summary>
	/// Summary description for TopNReport.
	/// </summary>
	public class TopNReport : BasePage
	{
		#region Web Form Designer generated code

		protected System.Web.UI.WebControls.Button btnQuery;
		protected System.Web.UI.WebControls.Label lblErrorMessage;
		protected System.Web.UI.WebControls.Button btnGenerate;
		protected System.Web.UI.WebControls.RadioButton rbBooked;
		protected System.Web.UI.WebControls.RadioButton rbInvoiced;
		protected System.Web.UI.WebControls.RadioButton rbDM;
		protected System.Web.UI.WebControls.RadioButton rbRevenue;
		protected System.Web.UI.WebControls.RadioButton rbChrgWt;
		protected System.Web.UI.WebControls.RadioButton rbVolume;
		protected System.Web.UI.WebControls.RadioButton rbConsignment;
		protected System.Web.UI.WebControls.Label lblTitle;
		protected System.Web.UI.HtmlControls.HtmlTable tblDates;
		protected System.Web.UI.WebControls.Label Label2;
		protected System.Web.UI.WebControls.Label Label4;
		protected System.Web.UI.WebControls.Label lblDates;
		protected System.Web.UI.WebControls.Label Label5;
		protected System.Web.UI.WebControls.Label lblPayerType;
		protected System.Web.UI.WebControls.Label Label6;
		protected System.Web.UI.WebControls.Label lblRouteType;
		protected System.Web.UI.WebControls.RadioButton rbLongRoute;
		protected System.Web.UI.WebControls.RadioButton rbShortRoute;
		protected System.Web.UI.WebControls.RadioButton rbAirRoute;
		protected System.Web.UI.WebControls.Label lblRouteCode;
		protected System.Web.UI.WebControls.Label Label7;
		protected System.Web.UI.WebControls.Label Label8;
		protected System.Web.UI.WebControls.Label lblDestination;
		protected System.Web.UI.WebControls.Label lblZipCode;
		protected com.common.util.msTextBox txtZipCode;
		protected System.Web.UI.WebControls.Button btnZipCode;
		protected System.Web.UI.WebControls.Label lblStateCode;
		protected com.common.util.msTextBox txtStateCode;
		protected System.Web.UI.WebControls.Button btnStateCode;
		protected System.Web.UI.HtmlControls.HtmlTable Table1;
		protected System.Web.UI.HtmlControls.HtmlTable tblPayerType;
		protected System.Web.UI.HtmlControls.HtmlTable tblDestination;
		protected System.Web.UI.WebControls.Label Label10;
		protected Cambro.Web.DbCombo.DbCombo DbComboPathCode;
		protected Cambro.Web.DbCombo.DbCombo DbComboOriginDC;
		protected Cambro.Web.DbCombo.DbCombo DbComboDestinationDC;
		protected System.Web.UI.HtmlControls.HtmlTable tblShipmentTracking;
		protected System.Web.UI.WebControls.DropDownList ddTop;
		protected System.Web.UI.WebControls.ListBox lsbCustType;
		protected System.Web.UI.WebControls.DropDownList ddMonth;
		protected System.Web.UI.WebControls.RadioButton rbMonth;
		protected System.Web.UI.WebControls.RadioButton rbPeriod;
		protected System.Web.UI.WebControls.RadioButton rbDate;
		protected com.common.util.msTextBox txtYear;
		protected com.common.util.msTextBox txtTo;
		protected com.common.util.msTextBox txtPeriod;
		protected com.common.util.msTextBox txtDate;
		protected System.Web.UI.HtmlControls.HtmlTable tblRouteType;

		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			base.OnInit(e);
			InitializeComponent();
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnQuery.Click += new System.EventHandler(this.btnQuery_Click);
			this.btnGenerate.Click += new System.EventHandler(this.btnGenerate_Click);
			this.rbMonth.CheckedChanged += new System.EventHandler(this.rbMonth_CheckedChanged);
			this.ddMonth.SelectedIndexChanged += new System.EventHandler(this.ddMonth_SelectedIndexChanged);
			this.rbPeriod.CheckedChanged += new System.EventHandler(this.rbPeriod_CheckedChanged);
			this.rbDate.CheckedChanged += new System.EventHandler(this.rbDate_CheckedChanged);
			this.rbLongRoute.CheckedChanged += new System.EventHandler(this.rbLongRoute_CheckedChanged);
			this.rbShortRoute.CheckedChanged += new System.EventHandler(this.rbShortRoute_CheckedChanged);
			this.rbAirRoute.CheckedChanged += new System.EventHandler(this.rbAirRoute_CheckedChanged);
			this.btnZipCode.Click += new System.EventHandler(this.btnZipCode_Click);
			this.btnStateCode.Click += new System.EventHandler(this.btnStateCode_Click);
			this.rbBooked.CheckedChanged += new System.EventHandler(this.rbBooked_CheckedChanged);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private String m_strAppID;
		private String m_strEnterpriseID;
		private String m_strCulture;
		DataSet m_dsQuery=null;

		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings,Page.Session);
			m_strAppID = utility.GetAppID();
			m_strEnterpriseID = utility.GetEnterpriseID();
			m_strCulture = utility.GetUserCulture();

			DbComboOriginDC.ClientOnSelectFunction="makeUppercase('DbComboOriginDC:ulbTextBox');";
			DbComboDestinationDC.ClientOnSelectFunction="makeUppercase('DbComboDestinationDC:ulbTextBox');";

			DbComboPathCode.RegistrationKey = System.Configuration.ConfigurationSettings.AppSettings["DbComboRegKey"];
			DbComboOriginDC.RegistrationKey = System.Configuration.ConfigurationSettings.AppSettings["DbComboRegKey"];
			DbComboDestinationDC.RegistrationKey = System.Configuration.ConfigurationSettings.AppSettings["DbComboRegKey"];
			
			if(!Page.IsPostBack)
			{
				DefaultScreen();
				ddlmonths();
				ddlTopN();
				LoadCustomerTypeList();
			}

			SetDbComboServerStates();
		}


		private void btnGenerate_Click(object sender, System.EventArgs e)
		{
			lblErrorMessage.Text = "";
			int intChk = 0;
			intChk = ValidateData();
			if(intChk < 1)
			{
				return;
			}

			if(ValidateValues() == false)
			{
				m_dsQuery = GetShipmentQueryData();

				String strUrl = null;
				strUrl = "ReportViewerDataSet.aspx";
				Session["FORMID"] = "TopNReport";
				Session["SESSION_DS1"] = m_dsQuery;
				OpenWindowpage(strUrl);
			}
		}


		private void btnQuery_Click(object sender, System.EventArgs e)
		{
			DefaultScreen();
		}


		#region "Page Functions"

		private DataTable CreateEmptyDataTable()
		{
			DataTable dtShipment = new DataTable();

			#region "Dates"
			dtShipment.Columns.Add(new DataColumn("start_date", typeof(DateTime)));
			dtShipment.Columns.Add(new DataColumn("end_date", typeof(DateTime)));
			#endregion
			
			#region "Payer Type"
			dtShipment.Columns.Add(new DataColumn("payer_type", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("top_n", typeof(int)));
			#endregion
		
			#region "Route / DC Selection"
			dtShipment.Columns.Add(new DataColumn("route_type", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("route_code", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("origin_dc", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("destination_dc", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("delPath_origin_dc", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("delPath_destination_dc", typeof(string)));
			#endregion

			#region "Destination"
			dtShipment.Columns.Add(new DataColumn("zip_code", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("state_code", typeof(string)));
			#endregion

			#region "Retrieval Basis on"
			dtShipment.Columns.Add(new DataColumn("basis", typeof(string)));
			#endregion

			#region "Sort By"
			dtShipment.Columns.Add(new DataColumn("sort_by", typeof(string)));
			#endregion

			return dtShipment;
		}


		private DataSet GetShipmentQueryData()
		{
			DataSet dsShipment = new DataSet();
			DataTable dtShipment = CreateEmptyDataTable();
			DataRow dr = dtShipment.NewRow(); 
			string sStartDate = string.Empty;
			#region "Dates"
	
			string strMonth =null;
			if (rbMonth.Checked)
			{
				strMonth = ddMonth.SelectedItem.Value;
				if (strMonth != "" && txtYear.Text != "")
				{
					// Thosapol Yennam (08/08/2013) Modify Code
					if(Convert.ToInt32(strMonth) < 10){ strMonth = "0"+strMonth;}

					sStartDate="01"+"/"+strMonth+"/"+txtYear.Text;
					DateTime dtStartDate = DateTime.ParseExact (sStartDate, "dd/MM/yyyy", null);
					DateTime dtEndDate = dtStartDate.AddMonths(1).AddDays(-1);
					dtEndDate = DateTime.ParseExact(dtEndDate.ToString("dd/MM/yyyy")+" 23:59", "dd/MM/yyyy HH:mm", null);
					dr["start_date"] = dtStartDate;
					dr["end_date"] = dtEndDate;
				}
			}
			else if (rbPeriod.Checked)
			{
				if (txtPeriod.Text != ""  && txtTo.Text != "")
				{
					dr["start_date"] = DateTime.ParseExact(txtPeriod.Text ,"dd/MM/yyyy",null);
					dr["end_date"] = DateTime.ParseExact(txtTo.Text+" 23:59" ,"dd/MM/yyyy HH:mm",null);
				}
			}
			else 
			{
				if (txtDate.Text != "")
				{
					dr["start_date"] = DateTime.ParseExact(txtDate.Text, "dd/MM/yyyy",null);
					dr["end_date"] = DateTime.ParseExact(txtDate.Text+" 23:59", "dd/MM/yyyy HH:mm",null);
				}
			}
			#endregion

			#region "Payer Type"

			string strCustPayerType = "";

			for (int i = 0; i <= lsbCustType.Items.Count - 1; i ++)
			{
				if(lsbCustType.Items[i].Selected == true)
				{
					strCustPayerType += "'" + lsbCustType.Items[i].Value + "', ";
				}
			}
			if((strCustPayerType != null) && (strCustPayerType.Trim().Length > 0))
				strCustPayerType = strCustPayerType.Substring(0, strCustPayerType.Length - 2);

			if (strCustPayerType != "") 
				dr["payer_type"] = strCustPayerType;
			else
				dr["payer_type"] = System.DBNull.Value;
			
			dr["top_n"] = Convert.ToInt16(ddTop.SelectedItem.Value.Trim());
			#endregion
			
			#region "Route / DC Selection"
			if (rbLongRoute.Checked) 
			{
				dr["route_type"] = "L";
			}
			if (rbShortRoute.Checked) 
			{
				dr["route_type"] = "S";
			}
			if (rbAirRoute.Checked) 
			{
				dr["route_type"] = "A";
			}

			dr["route_code"] = DbComboPathCode.Value;
			dr["origin_dc"] = DbComboOriginDC.Value.ToUpper();
			dr["destination_dc"] = DbComboDestinationDC.Value.ToUpper();
			
			if (rbLongRoute.Checked || rbAirRoute.Checked)
			{
				com.ties.classes.DeliveryPath delPath = new com.ties.classes.DeliveryPath();
				delPath.Populate(m_strAppID, m_strEnterpriseID, DbComboPathCode.Value);

				dr["delPath_origin_dc"] = delPath.OriginStation;
				dr["delPath_destination_dc"] = delPath.DestinationStation;
			}
			else
			{
				dr["delPath_origin_dc"] = "";
				dr["delPath_destination_dc"] = "";
			}
			#endregion

			#region "Destination"
			dr["zip_code"] = txtZipCode.Text;
			dr["state_code"] = txtStateCode.Text;
			#endregion

			#region "Retrieval Basis on"

			if (rbBooked.Checked) 
				dr["basis"] = "B";
			else if (rbInvoiced.Checked)
				dr["basis"] = "I";
			else
				dr["basis"] = "D";

			
			#endregion

			#region "Sort By"

			if (rbRevenue.Checked)
				dr["sort_by"] = "R";
			else if (rbChrgWt.Checked)
				dr["sort_by"] = "W";
			else if (rbVolume.Checked)
				dr["sort_by"] = "V";
			else
				dr["sort_by"] = "C";

			#endregion

			dtShipment.Rows.Add(dr);
			dsShipment.Tables.Add(dtShipment);
			return dsShipment;
		}


		private void DefaultScreen()
		{
			#region "Dates"
			rbMonth.Checked = true;
			rbPeriod.Checked = false;
			rbDate.Checked = false;

			ddMonth.Enabled = true;
			ddMonth.SelectedIndex = -1;
			txtYear.Text = null;
			txtYear.Enabled = true;
			txtPeriod.Text = null;
			txtPeriod.Enabled = false;
			txtTo.Text = null;
			txtTo.Enabled = false;
			txtDate.Text = null;
			txtDate.Enabled = false;

			#endregion

			#region "Payer Type"

			lsbCustType.SelectedIndex = -1;
			ddTop.SelectedIndex = -1;

			#endregion

			#region "Route / DC Selection"
			DbComboPathCode.Text="";
			DbComboPathCode.Value="";

			DbComboOriginDC.Text="";
			DbComboOriginDC.Value="";

			DbComboDestinationDC.Text="";
			DbComboDestinationDC.Value="";
			#endregion

			#region "Destination"

			txtZipCode.Text = null;
			txtStateCode.Text = null;

			#endregion

			#region "Retrieval Basis on"

			rbBooked.Checked = true;
			rbInvoiced.Checked = false;
			rbDM.Checked = false;

			#endregion

			#region "Sort By"

			rbRevenue.Checked = true;
			rbChrgWt.Checked = false;
			rbVolume.Checked = false;
			rbConsignment.Checked = false;

			#endregion
			
			lblErrorMessage.Text = "";
		}


		private bool ValidateValues()
		{
			bool iCheck=false;
	
			if((ddMonth.SelectedIndex==0)&&(txtYear.Text=="")&&(txtPeriod.Text=="")&&(txtTo.Text=="")&&(txtDate.Text==""))
			{				
				lblErrorMessage.Text=Utility.GetLanguageText(ResourceType.UserMessage,"PLS_ENTER_EST_DVL",utility.GetUserCulture());
				return iCheck=true;
			
			}
			if(rbMonth.Checked == true)
			{
				if((ddMonth.SelectedIndex>0) &&(txtYear.Text==""))
				{
					lblErrorMessage.Text=Utility.GetLanguageText(ResourceType.UserMessage,"MM_YEAR_REQ",utility.GetUserCulture());
					return iCheck=true;
				}
				else if((ddMonth.SelectedIndex==0) &&(txtYear.Text!=""))
				{
					lblErrorMessage.Text=Utility.GetLanguageText(ResourceType.UserMessage,"MM_YEAR_REQ",utility.GetUserCulture());
					return iCheck=true;
				}
				else
				{
					return iCheck=false;
				}
			}
			if(rbPeriod.Checked == true )
			{
				if((txtPeriod.Text!="")&&(txtTo.Text==""))
				{					
					lblErrorMessage.Text=Utility.GetLanguageText(ResourceType.UserMessage,"PLS_ENTER_END_DT",utility.GetUserCulture());
					return iCheck=true;
				}
				else if((txtPeriod.Text=="")&&(txtTo.Text!=""))
				{
					lblErrorMessage.Text=Utility.GetLanguageText(ResourceType.UserMessage,"PLS_START_DT",utility.GetUserCulture());					
					return iCheck=true;			
				}
				else
				{
					return iCheck=false;
				}
			}
			if(rbDate.Checked == true )
			{
				if(txtDate.Text=="")
				{					
					lblErrorMessage.Text=Utility.GetLanguageText(ResourceType.UserMessage,"PLS_ENTER_DT",utility.GetUserCulture());	
					return iCheck=true;				
				}
			}			

			return iCheck;
			
		}


		private int ValidateData()
		{
			if (txtYear.Text != "" )
			{
				if (Convert.ToInt32(txtYear.Text) < 2000 || Convert.ToInt32(txtYear.Text) > 2099)
				{
					lblErrorMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"YEAR_00_99",utility.GetUserCulture());
					return -1;
				}
			}
			return 1;
		}

		
		private int LastDayOfMonth(String strMonth, String strYear)
		{
			int intLastDay = 0;
			switch (strMonth)
			{
				case "01":
				case "03":
				case "05":
				case "07":
				case "08":
				case "10":
				case "12":
					intLastDay = 31;
					break;
				case "04":
				case "06":
				case "09":
				case "11":
					intLastDay = 30;
					break;
				case "02":
					intLastDay = 28;
					break;
			}
			return intLastDay;
		}


		private void OpenWindowpage(String strUrl)
		{
			ArrayList paramList = new ArrayList();
			paramList.Add(strUrl);
			String sScript = Utility.GetScript("openParentWindow.js",paramList);
			Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
		}



		#endregion


		#region "Dates Part : Controls"

		private void rbMonth_CheckedChanged(object sender, System.EventArgs e)
		{
			ddMonth.Enabled = true;
			txtYear.Text = null;
			txtYear.Enabled = true;
			txtPeriod.Text = null;
			txtPeriod.Enabled = false;
			txtTo.Text = null;
			txtTo.Enabled = false;
			txtDate.Text = null;
			txtDate.Enabled = false;
		}


		private void rbPeriod_CheckedChanged(object sender, System.EventArgs e)
		{
			ddMonth.Enabled = false;
			txtYear.Text = null;
			txtYear.Enabled = false;
			txtPeriod.Text = null;
			txtPeriod.Enabled = true;
			txtTo.Text = null;
			txtTo.Enabled = true;
			txtDate.Text = null;
			txtDate.Enabled = false;
		}


		private void rbDate_CheckedChanged(object sender, System.EventArgs e)
		{
			ddMonth.Enabled = false;
			txtYear.Text = null;
			txtYear.Enabled = false;
			txtPeriod.Text = null;
			txtPeriod.Enabled = false;
			txtTo.Text = null;
			txtTo.Enabled = false;
			txtDate.Text = null;
			txtDate.Enabled = true;
		}


		#endregion

		#region "Payer Type Part : Controls

		private void btnPayerCode_Click(object sender, System.EventArgs e)
		{
			string strCustPayerType = "";
			for (int i = 0; i <= lsbCustType.Items.Count - 1; i ++)
			{
				if(lsbCustType.Items[i].Selected == true)
				{
					strCustPayerType += lsbCustType.Items[i].Value + ",";
				}
			}
			if((strCustPayerType != null) && (strCustPayerType.Trim().Length > 0))
				strCustPayerType = strCustPayerType.Substring(0, strCustPayerType.Length - 1);

			String sUrl = "CustomerPopup.aspx?FORMID="+"TopNReport"+
				"&CustType="+strCustPayerType.ToString();
			ArrayList paramList = new ArrayList();
			paramList.Add(sUrl);
			String sScript = Utility.GetScript("openWindowParam.js",paramList);
			Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
		}


		#endregion

		#region "Route / DC Selection : Controls"

		private void rbLongRoute_CheckedChanged(object sender, System.EventArgs e)
		{
			DbComboPathCode.Text="";
			DbComboPathCode.Value="";
		}


		private void rbShortRoute_CheckedChanged(object sender, System.EventArgs e)
		{
			DbComboPathCode.Text="";
			DbComboPathCode.Value="";	
		}


		private void rbAirRoute_CheckedChanged(object sender, System.EventArgs e)
		{
			DbComboPathCode.Text="";
			DbComboPathCode.Value="";
		}


		#endregion

		#region "Destination : Controls"

		private void btnZipCode_Click(object sender, System.EventArgs e)
		{
			String sUrl = "ZipCodePopup.aspx?FORMID=TopNReport"+"&ZIPCODE_CID="+txtZipCode.ClientID+"&ZIPCODE="+txtZipCode.Text.Trim().ToString();
			ArrayList paramList = new ArrayList();
			paramList.Add(sUrl);
			String sScript = Utility.GetScript("openPostalWindow.js",paramList); //Thosapol.y  Modify (26/06/2013)
			//String sScript = Utility.GetScript("openParentWindow.js",paramList); //Thosapol.y  Comment (26/06/2013)
			Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
		}


		private void btnStateCode_Click(object sender, System.EventArgs e)
		{
			String sUrl = "StatePopup.aspx?FORMID=TopNReport"+"&STATECODE="+txtStateCode.ClientID+"&STATECODE_TEXT="+txtStateCode.Text.Trim().ToString();
			ArrayList paramList = new ArrayList();
			paramList.Add(sUrl);
			String sScript = Utility.GetScript("openParentWindow.js",paramList);
			Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
		}


		#endregion


		#region "Dates Part : DropDownList"

		private void ddlmonths()
		{
			DataTable dtMonths = new DataTable();
			dtMonths.Columns.Add(new DataColumn("Text", typeof(string)));
			dtMonths.Columns.Add(new DataColumn("StringValue", typeof(string)));
		
			DataRow drNilRow = dtMonths.NewRow();
			drNilRow[0] = "";
			drNilRow[1] = "";
			dtMonths.Rows.Add(drNilRow);

			ArrayList listMonthTxt = Utility.GetCodeValues(m_strAppID,m_strCulture, "months", CodeValueType.StringValue);
			foreach(SystemCode typeSysCode in listMonthTxt)
			{
				DataRow drEach = dtMonths.NewRow();
				drEach[0] = typeSysCode.Text;
				drEach[1] = typeSysCode.StringValue;
				dtMonths.Rows.Add(drEach);
			}
			DataView dvMonths = new DataView(dtMonths);

			ddMonth.DataSource = dvMonths;
			ddMonth.DataTextField = "Text";
			ddMonth.DataValueField = "StringValue";
			ddMonth.DataBind();	
		}


		#endregion

		#region "Payer Type Part : DropDownList"

		public void LoadCustomerTypeList()
		{
			ArrayList systemCodes = Utility.GetCodeValues(utility.GetAppID(),utility.GetUserCulture(),"customer_type",CodeValueType.StringValue);
			foreach(SystemCode sysCode in systemCodes)
			{	
				ListItem lstItem = new ListItem();
				lstItem.Text = sysCode.Text;
				lstItem.Value = sysCode.StringValue;
				lsbCustType.Items.Add(lstItem);
			}
		}


		private void ddlTopN()
		{
			DataTable dtTopN = new DataTable();
			dtTopN.Columns.Add(new DataColumn("Text", typeof(int)));
			dtTopN.Columns.Add(new DataColumn("StringValue", typeof(int)));
		
			DataRow drNilRow = dtTopN.NewRow();
			drNilRow[0] = 0;
			drNilRow[1] = 0;
			dtTopN.Rows.Add(drNilRow);

			for(int i=1;i<=20;i++)
			{
				DataRow drEach = dtTopN.NewRow();
				drEach[0] = i;
				drEach[1] = i;
				dtTopN.Rows.Add(drEach);
			}
			DataView dvTopN = new DataView(dtTopN);

			ddTop.DataSource = dvTopN;
			ddTop.DataTextField = "Text";
			ddTop.DataValueField = "StringValue";
			ddTop.DataBind();	
			ddTop.SelectedIndex = 19;
		}


		#endregion

		#region "Route / DC Selection : DropDownList"

		private void SetDbComboServerStates()
		{
			String strDeliveryType=null;
			if (rbLongRoute.Checked==true)
				strDeliveryType="L";
			else if (rbShortRoute.Checked==true)
				strDeliveryType="S";
			else if (rbAirRoute.Checked==true)
				strDeliveryType="A";

			Hashtable hash = new Hashtable();
			hash.Add("strDeliveryType", strDeliveryType);
			DbComboPathCode.ServerState = hash;
			DbComboPathCode.ServerStateSecretString = "kdfsjgnskajh[apq2-4uhg465654435879mnbfdm cxs;q4";
		}


		[Cambro.Web.DbCombo.ResultsMethodAttribute(true)]
		public static object DbComboPathCodeSelect(Cambro.Web.DbCombo.ServerMethodArgs args)
		{
			Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings,HttpContext.Current.Session);
			String strAppID = util.GetAppID();
			String strEnterpriseID = util.GetEnterpriseID();
			
			String strDelType = "L";			
			String strWhereClause=null;
			if(args.ServerState != null && args.ServerState.Authenticate("kdfsjgnskajh[apq2-4uhg465654435879mnbfdm cxs;q4"))
			{
				if(args.ServerState["strDeliveryType"] != null && args.ServerState["strDeliveryType"].ToString().Length > 0)
				{
					strDelType = args.ServerState["strDeliveryType"].ToString();
					strWhereClause=" and Delivery_Type='"+Utility.ReplaceSingleQuote(strDelType)+"'";
					if(strDelType == "S") 
					{
						strWhereClause=strWhereClause+"and path_code in ( ";
						strWhereClause=strWhereClause+"select distinct delivery_route ";
						strWhereClause=strWhereClause+"from Zipcode ";
						strWhereClause=strWhereClause+"where applicationid = '"+strAppID+"' ";
						strWhereClause=strWhereClause+"and enterpriseid = '"+ strEnterpriseID +"') ";
					}
				}				
			}
			
			DataSet dataset = com.ties.classes.DbComboDAL.PathCodeQuery(strAppID,strEnterpriseID,args, strWhereClause);	
			return dataset;
		}


		[Cambro.Web.DbCombo.ResultsMethodAttribute(true)]
		public static object DbComboDistributionCenterSelect(Cambro.Web.DbCombo.ServerMethodArgs args)
		{
			Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings,HttpContext.Current.Session);
			String strAppID = util.GetAppID();
			String strEnterpriseID = util.GetEnterpriseID();
			
			DataSet dataset = com.ties.classes.DbComboDAL.DistributionCenterQuery(strAppID,strEnterpriseID,args);	
			return dataset;
		}


		#endregion

		private void ddMonth_SelectedIndexChanged(object sender, System.EventArgs e)
		{
		
		}

		private void rbBooked_CheckedChanged(object sender, System.EventArgs e)
		{
		
		}
	}
}