using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.common.classes;
using com.common.util;
using com.ties.DAL;
using com.common.applicationpages;
using Cambro.Web.DbCombo;
using com.common.RBAC;


using com.common.DAL;
using System.Text;
using com.ties.classes;

namespace com.ties
{
	/// <summary>
	/// Summary description for ShipmentTrackingQuery.
	/// </summary>
	public class CustomerLastShipmentDate : BasePage
	{
		
		#region Web Form Designer generated code

		protected System.Web.UI.WebControls.Button btnExecuteQuery;
		protected System.Web.UI.WebControls.Button btnQuery;
		protected System.Web.UI.WebControls.Label Label1;
		protected System.Web.UI.WebControls.TextBox txtPeriod2;
		protected System.Web.UI.WebControls.ValidationSummary ValidationSummary1;
		protected System.Web.UI.HtmlControls.HtmlTable tblInternal;
		protected System.Web.UI.WebControls.DropDownList cboMonthend;
		protected System.Web.UI.WebControls.RadioButton rdMonth;
		protected System.Web.UI.WebControls.DropDownList CboMonth;
		protected System.Web.UI.WebControls.DropDownList cboyear;
		protected System.Web.UI.WebControls.Label lblErrorMessage;

		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnQuery.Click += new System.EventHandler(this.btnQuery_Click);
			this.btnExecuteQuery.Click += new System.EventHandler(this.btnExecuteQuery_Click);
			this.rdMonth.CheckedChanged += new System.EventHandler(this.rdMonth_CheckedChanged);
			this.CboMonth.SelectedIndexChanged += new System.EventHandler(this.CboMonth_SelectedIndexChanged);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		String m_strAppID=null;
		String m_strEnterpriseID=null;
		String m_strCulture=null;
	
		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings,Page.Session);
			m_strAppID = utility.GetAppID();
			m_strEnterpriseID = utility.GetEnterpriseID();
			m_strCulture = utility.GetUserCulture();

//			DbComboPathCode.RegistrationKey = System.Configuration.ConfigurationSettings.AppSettings["DbComboRegKey"];
//			DbComboOriginDC.RegistrationKey = System.Configuration.ConfigurationSettings.AppSettings["DbComboRegKey"];
//			DbComboDestinationDC.RegistrationKey = System.Configuration.ConfigurationSettings.AppSettings["DbComboRegKey"];

			com.common.classes.User user = RBACManager.GetUser(m_strAppID, m_strEnterpriseID, (String)Session["userID"]);
			ViewState["usertype"] = user.UserType;
			ViewState["PayerID"] = user.PayerID;

//			if ((String)ViewState["usertype"]  == "C")
//			{
//				tblInternal.Visible = false;
//				tblExternal.Visible = true;
//			}
//			else
//			{
//				tblInternal.Visible = true;
//				tblExternal.Visible = false;
//			}

			if(!Page.IsPostBack)
			{
				DefaultScreen();
				ddlmonths();
				ddlmonthsCust();
				LoadCustomerTypeList();
				LoadBookingTypeList();
				Session["toRefresh"]=false;
				LoadCombo();
				EnableControl(false);
			}

//			SetDbComboServerStates();
		}

		private void LoadCombo()
		{
			for(int i=2003 ;i<= DateTime.Now.Year;i++)
			{
				cboyear.Items.Add(i.ToString());
			}
		}

		private void btnExecuteQuery_Click(object sender, System.EventArgs e)
		{

			//btnExecuteQuery.Enabled = false;
			lblErrorMessage.Text = "";
//
//			bool icheck = ValidateValues();
//		
//			if(icheck==false)
//			{
//			if(rdMonth.Checked) 
//			{
				if (CboMonth.SelectedIndex.ToString().Trim() == "0" && cboMonthend.SelectedIndex.ToString().Trim() =="0") 
				{
					lblErrorMessage.Text ="Please Choose Start Month or End Month";
					return ;
				}
				if (CboMonth.SelectedIndex.ToString().Trim() =="0" || cboMonthend.SelectedIndex.ToString().Trim()=="0") 
				{
					lblErrorMessage.Text ="Select Start Date or End Date";
					return ;
				}
				if (CboMonth.SelectedIndex > cboMonthend.SelectedIndex) 
				{
					lblErrorMessage.Text ="Start Date must less than End Date";
					return ;
				}

//			}
//			if (rdDate.Checked)
//			{
//				if(txtDate.Text =="")
//				{
//					lblErrorMessage.Text ="Please Choose Date";
//					return ;
//				}
//			}

				DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(m_strAppID,m_strEnterpriseID);
				IDbConnection conApp = dbCon.GetConnection();

				IDbCommand dbCmd = null;
				string sqlCommand = "exec uspGenCustomerLastShipmentDate '"+  cboyear.SelectedItem.Value.Trim() + "'," + CboMonth.SelectedItem.Value.Trim() + "," + cboMonthend.SelectedItem.Value.Trim() ;
				dbCmd = dbCon.CreateCommand(sqlCommand, CommandType.Text);
				dbCmd.Connection = conApp;
				dbCon.ExecuteNonQuery(dbCmd);
				

				String strModuleID = Request.Params["MODID"];
				DataSet dsShipment = GetShipmentQueryData();

			
				String strUrl = null;
				//String sFeatures = "'height=700,width=900,left=20,top=5,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,titlebar=yes,toolbar=no'";
				strUrl = "ReportViewer.aspx";
				//String strFeatures = "'height=700,width=900,left=20,top=5,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,titlebar=yes,toolbar=no'";
				Session["FORMID"] = "CustomerLastShipmentDate";
				Session["SESSION_DS1"] = dsShipment;
				//OpenWindowpage(strUrl, strFeatures);
				ArrayList paramList = new ArrayList();
				paramList.Add(strUrl);
				String sScript = Utility.GetScript("openParentWindow.js",paramList);
				Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
//			}

		}


		private void btnQuery_Click(object sender, System.EventArgs e)
		{
			DefaultScreen();
		}


		#region "Page Functions"

		private DataTable CreateEmptyDataTable()
		{
			DataTable dtShipment = new DataTable();

			dtShipment.Columns.Add(new DataColumn("Date", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("SMonths", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("EMonths", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("Dates", typeof(string)));
			//Add By Tom 18/5/09
			dtShipment.Columns.Add(new DataColumn("SeYear", typeof(string)));
			//End Add By Tom

			#region "Dates"
//			dtShipment.Columns.Add(new DataColumn("tran_date", typeof(string)));
//			dtShipment.Columns.Add(new DataColumn("start_date", typeof(DateTime)));
//			dtShipment.Columns.Add(new DataColumn("end_date", typeof(DateTime)));
			#endregion
			
			#region "Payer Type"
//			dtShipment.Columns.Add(new DataColumn("payer_type", typeof(string)));
//			dtShipment.Columns.Add(new DataColumn("payer_code", typeof(string)));
			#endregion
		
			#region "Route / DC Selection"
//			dtShipment.Columns.Add(new DataColumn("route_type", typeof(string)));
//			dtShipment.Columns.Add(new DataColumn("route_code", typeof(string)));
//			dtShipment.Columns.Add(new DataColumn("origin_dc", typeof(string)));
//			dtShipment.Columns.Add(new DataColumn("destination_dc", typeof(string)));
//			dtShipment.Columns.Add(new DataColumn("delPath_origin_dc", typeof(string)));
//			dtShipment.Columns.Add(new DataColumn("delPath_destination_dc", typeof(string)));
			#endregion

			#region "Destination"
//			dtShipment.Columns.Add(new DataColumn("zip_code", typeof(string)));
//			dtShipment.Columns.Add(new DataColumn("state_code", typeof(string)));
			#endregion

			#region "Shipment"
//			dtShipment.Columns.Add(new DataColumn("booking_type", typeof(string)));
//			dtShipment.Columns.Add(new DataColumn("consignment_no", typeof(string)));
//			dtShipment.Columns.Add(new DataColumn("booking_no", typeof(string)));
//			dtShipment.Columns.Add(new DataColumn("status_code", typeof(string)));
//			dtShipment.Columns.Add(new DataColumn("exception_code", typeof(string)));
//			dtShipment.Columns.Add(new DataColumn("shipment_closed", typeof(string)));
//			dtShipment.Columns.Add(new DataColumn("shipment_invoiced", typeof(string)));
			#endregion

			#region "Report To Show"
			dtShipment.Columns.Add(new DataColumn("RptType", typeof(string)));
			#endregion

			return dtShipment;
		}

		private bool ValidateValues()
		{
			bool iCheck=false;
	
			if(rdMonth.Checked) 
			{
				if (CboMonth.SelectedIndex.ToString().Trim() == "0" && cboMonthend.SelectedIndex.ToString().Trim() =="0") 
				{
					lblErrorMessage.Text ="Please Choose Start Month or End Month";
					return iCheck=true;
				}
				else
				{
					lblErrorMessage.Text="";
				}

				if (CboMonth.SelectedIndex > cboMonthend.SelectedIndex) 
				{
					lblErrorMessage.Text ="Start Date must less than End Date";
					return iCheck=true;
				}
				else
				{
					lblErrorMessage.Text="";
				}
			}
//			if (rdDate.Checked)
//			{
//				if(txtDate.Text =="")
//				{
//					lblErrorMessage.Text ="Please Choose Date";
//					return iCheck=true;
//				}
//				else
//				{
//					lblErrorMessage.Text="";
//				}
//			}
			return iCheck;
		}

		private DataSet GetShipmentQueryData()
		{
			DataSet dsShipment = new DataSet();
			DataTable dtShipment = CreateEmptyDataTable();
			DataRow dr = dtShipment.NewRow(); 

//			dr["Date"] = txtDate.Text; 
//			if(rdMonth.Checked) 
//			{
//				if (CboMonth.SelectedIndex.ToString().Trim() != "0" && cboMonthend.SelectedIndex.ToString().Trim() !="0") 
//				{
					dr["SMonths"] = CboMonth.SelectedIndex;
					dr["EMonths"] = cboMonthend.SelectedIndex;
					//Add By Tom 18/5/09
					dr["SeYear"] = cboyear.SelectedItem;
					//End add By Tom
//				}
//				else
//				{
//					lblErrorMessage.Text ="Please Choose Start Month or End Month";
					
//				}
//			}
//			if (rdDate.Checked)
//			{
////				if(txtDate.Text !="")
////				{
//					dr["Dates"] = txtDate.Text;
////				}
////				else
////				{
////					lblErrorMessage.Text ="Please Choose Date";
////					return iCheck=true;
////				}
//			}
			
			#region "Dates"

//			if ((String)ViewState["usertype"] != "C")
//			{
//				if (rdactpickupdate.Checked) 
//				{	// Booking Date Selected
//					dr["tran_date"] = "U";
//				}
//				if (rdactdelDate.Checked) 
//				{	// Pickup Date Selected
//					dr["tran_date"] = "D";
//				}
//				if (rdPodDate.Checked) 
//				{	// Status Date Selected
//					dr["tran_date"] = "P";
//				}
			
//				string strMonth =null;
//
//				if (rbMonth.Checked == true) 
//				{	// Month Selected
//					strMonth = ddMonth.SelectedItem.Value;
//					if (strMonth != "" && txtYear.Text != "") 
//					{
//						dr["start_date"] = DateTime.ParseExact ("01/"+strMonth.ToString()+"/"+txtYear.Text, "dd/MM/yyyy", null);
//						int intLastDay = 0;
//						intLastDay = LastDayOfMonth(strMonth, txtYear.Text);
//						dr["end_date"] = DateTime.ParseExact (intLastDay.ToString()+"/"+strMonth.ToString()+"/"+txtYear.Text+" 23:59", "dd/MM/yyyy HH:mm", null);
//					}
//				}
//				if (rbPeriod.Checked == true) 
//				{	// Period Selected
//					if (txtPeriod.Text != ""  && txtTo.Text != "") 
//					{
//						dr["start_date"] = DateTime.ParseExact(txtPeriod.Text ,"dd/MM/yyyy",null);
//						dr["end_date"] = DateTime.ParseExact(txtTo.Text+" 23:59" ,"dd/MM/yyyy HH:mm",null);
//					}
//				}
//				if (rbDate.Checked == true) 
//				{	// Date Selected
//					if (txtDate.Text != "") 
//					{
//						dr["start_date"] = DateTime.ParseExact(txtDate.Text, "dd/MM/yyyy",null);
//						dr["end_date"] = DateTime.ParseExact(txtDate.Text+" 23:59", "dd/MM/yyyy HH:mm",null);
//					}
//				}
//			}
//			else
//			{
//				if (rbBookingDateCust.Checked) 
//				{	// Booking Date Selected
//					dr["tran_date"] = "B";
//				}
//				if (rbPickupDateCust.Checked) 
//				{	// Pickup Date Selected
//					dr["tran_date"] = "P";
//				}
//				if (rbStatusDateCust.Checked) 
//				{	// Status Date Selected
//					dr["tran_date"] = "S";
//				}
			
//				string strMonth =null;

//				if (rbMonthCust.Checked == true) 
//				{	// Month Selected
//					strMonth = ddMonthCust.SelectedItem.Value;
//					if (strMonth != "" && txtYearCust.Text != "") 
//					{
//						dr["start_date"] = DateTime.ParseExact ("01/"+strMonth.ToString()+"/"+txtYearCust.Text, "dd/MM/yyyy", null);
//						int intLastDay = 0;
//						intLastDay = LastDayOfMonth(strMonth, txtYearCust.Text);
//						dr["end_date"] = DateTime.ParseExact (intLastDay.ToString()+"/"+strMonth.ToString()+"/"+txtYearCust.Text+" 23:59", "dd/MM/yyyy HH:mm", null);
//					}
//				}
//				if (rbPeriodCust.Checked == true) 
//				{	// Period Selected
//					if (txtPeriodCust.Text != ""  && txtToCust.Text != "") 
//					{
//						dr["start_date"] = DateTime.ParseExact(txtPeriodCust.Text ,"dd/MM/yyyy",null);
//						dr["end_date"] = DateTime.ParseExact(txtToCust.Text+" 23:59" ,"dd/MM/yyyy HH:mm",null);
//					}
//				}
//				if (rbDateCust.Checked == true) 
//				{	// Date Selected
//					if (txtDateCust.Text != "") 
//					{
//						dr["start_date"] = DateTime.ParseExact(txtDateCust.Text, "dd/MM/yyyy",null);
//						dr["end_date"] = DateTime.ParseExact(txtDateCust.Text+" 23:59", "dd/MM/yyyy HH:mm",null);
//					}
//				}
//			}
		
			#endregion

			#region "Payer Type"

//			string strCustPayerType = "";

//			for (int i = 0; i <= lsbCustType.Items.Count - 1; i ++)
//			{
//				if(lsbCustType.Items[i].Selected == true)
//				{
//					strCustPayerType += lsbCustType.Items[i].Value;
//				}
//			}

//			if (strCustPayerType != "") 
//				dr["payer_type"] = strCustPayerType;
//			else
//				dr["payer_type"] = System.DBNull.Value;
//			
//			if ((String)ViewState["usertype"]  == "C")
//				dr["payer_code"] = (String)ViewState["PayerID"];
//			else
//				dr["payer_code"] = txtPayerCode.Text.Trim();

			#endregion
			
			#region "Route / DC Selection"
//			if (rbLongRoute.Checked) 
//			{
//				dr["route_type"] = "L";
//			}
//			if (rbShortRoute.Checked) 
//			{
//				dr["route_type"] = "S";
//			}
//			if (rbAirRoute.Checked) 
//			{
//				dr["route_type"] = "A";
//			}
//
//			dr["route_code"] = DbComboPathCode.Value;
//			dr["origin_dc"] = DbComboOriginDC.Value;
//			dr["destination_dc"] = DbComboDestinationDC.Value;
			
//			if (rbLongRoute.Checked || rbAirRoute.Checked)
//			{
//				com.ties.classes.DeliveryPath delPath = new com.ties.classes.DeliveryPath();
//				delPath.Populate(m_strAppID, m_strEnterpriseID, DbComboPathCode.Value);
//
//				dr["delPath_origin_dc"] = delPath.OriginStation;
//				dr["delPath_destination_dc"] = delPath.DestinationStation;
//			}
//			else
//			{
//				dr["delPath_origin_dc"] = "";
//				dr["delPath_destination_dc"] = "";
//			}
			#endregion

			#region "Destination"
//			if((String)ViewState["usertype"] != "C")
//			{
//				dr["zip_code"] = txtZipCode.Text.Trim();
//				dr["state_code"] = txtStateCode.Text.Trim();
//			}
//			else
//			{
//				dr["zip_code"] = txtZipCodeCust.Text.Trim();
//				dr["state_code"] = txtStateCodeCust.Text.Trim();
//			}
			#endregion

			#region "Shipment"
//			dr["booking_type"] = Drp_BookingType.SelectedItem.Value;
//			dr["consignment_no"] = txtConsignmentNo.Text;
//			dr["booking_no"] = txtBookingNo.Text;
//			dr["status_code"] = txtStatusCode.Text;
//			dr["exception_code"] = txtExceptionCode.Text;
//
//			if (rbShipmentYes.Checked)
//				dr["shipment_closed"] = "Y";
//			else
//				dr["shipment_closed"] = "N";

//			if (rbInvoiceYes.Checked)
//				dr["shipment_invoiced"] = "Y";
//			else
//				dr["shipment_invoiced"] = "N";
			#endregion

			#region "Report To Show"

			if ((String)ViewState["usertype"] != "C")
			{
				dr["RptType"] = "N";
			}
			else
			{
				dr["RptType"] = "C";
			}

			#endregion

			dtShipment.Rows.Add(dr);
			dsShipment.Tables.Add(dtShipment);
			return dsShipment;
		}


		private void DefaultScreen()
		{
			#region "Dates"

//			rbMonth.Checked = true;
//			rbPeriod.Checked = false;
//			rbDate.Checked = false;
//
//			ddMonth.Enabled = true;
//			ddMonth.SelectedIndex = -1;
//			txtYear.Text = null;
//			txtYear.Enabled = true;
//			txtPeriod.Text = null;
//			txtPeriod.Enabled = false;
//			txtTo.Text = null;
//			txtTo.Enabled = false;
//			txtDate.Text = null;
//			txtDate.Enabled = false;

			#endregion

			#region "Payer Type"

//			lsbCustType.SelectedIndex = -1;
//			txtPayerCode.Text = null;

			#endregion

			#region "Destination"

//			txtZipCode.Text = null;
//			txtStateCode.Text = null;

			#endregion

			//Cust LogOn
		
			lblErrorMessage.Text = "";
		}


//		private int ValidateData()
//		{
//			if (txtYear.Text != "" )
//			{
//				if (Convert.ToInt32(txtYear.Text) < 2000 || Convert.ToInt32(txtYear.Text) > 2099)
//				{
//					lblErrorMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"YEAR_00_99",utility.GetUserCulture());
//					return -1;
//				}
//			}
//			return 1;
//		}
		

		private int LastDayOfMonth(String strMonth, String strYear)
		{
			int intLastDay = 0;
			switch (strMonth)
			{
				case "01":
				case "03":
				case "05":
				case "07":
				case "08":
				case "10":
				case "12":
					intLastDay = 31;
					break;
				case "04":
				case "06":
				case "09":
				case "11":
					intLastDay = 30;
					break;
				case "02":
					//intLastDay = 28;
					//by ching 19/02/2008
					DateTime dt = System.DateTime.ParseExact("01/02/"+strYear,"dd/MM/yyyy",null);
					DateTime firstDayOfThisMonth = dt.Subtract(TimeSpan.FromDays(dt.Day - 1));
					DateTime firstDayOfNextMonth = firstDayOfThisMonth.AddMonths(1);
					DateTime lastDayOfThisMonth = firstDayOfNextMonth.Subtract(TimeSpan.FromDays(1)) ;

					intLastDay = Int32.Parse(lastDayOfThisMonth.Day.ToString());
					break;
			}
			return intLastDay;
		}


		private void OpenWindowpage(String strUrl, String strFeatures)
		{
			
			ArrayList paramList = new ArrayList();
			paramList.Add(strUrl);
			String sScript = Utility.GetScript("openParentWindow.js",paramList);
			Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
		}


		#endregion


		#region "Dates Part : Controls"

//		private void rbMonth_CheckedChanged(object sender, System.EventArgs e)
//		{
//			ddMonth.Enabled = true;
//			txtYear.Text = null;
//			txtYear.Enabled = true;
//			txtPeriod.Text = null;
//			txtPeriod.Enabled = false;
//			txtTo.Text = null;
//			txtTo.Enabled = false;
//			txtDate.Text = null;
//			txtDate.Enabled = false;
//		}


//		private void rbPeriod_CheckedChanged(object sender, System.EventArgs e)
//		{
//			ddMonth.Enabled = false;
//			txtYear.Text = null;
//			txtYear.Enabled = false;
//			txtPeriod.Text = null;
//			txtPeriod.Enabled = true;
//			txtTo.Text = null;
//			txtTo.Enabled = true;
//			txtDate.Text = null;
//			txtDate.Enabled = false;
//		}


//		private void rbDate_CheckedChanged(object sender, System.EventArgs e)
//		{
//			ddMonth.Enabled = false;
//			txtYear.Text = null;
//			txtYear.Enabled = false;
//			txtPeriod.Text = null;
//			txtPeriod.Enabled = false;
//			txtTo.Text = null;
//			txtTo.Enabled = false;
//			txtDate.Text = null;
//			txtDate.Enabled = true;
//		}

		
		#endregion

		
		#region "Payer Type Part : Controls

		private void btnPayerCode_Click(object sender, System.EventArgs e)
		{
			string strCustPayerType = "";
//			for (int i = 0; i <= lsbCustType.Items.Count - 1; i ++)
//			{
//				if(lsbCustType.Items[i].Selected == true)
//				{
//					strCustPayerType += lsbCustType.Items[i].Value + ",";
//				}
//			}
			if((strCustPayerType != null) && (strCustPayerType.Trim().Length > 0))
				strCustPayerType = strCustPayerType.Substring(0, strCustPayerType.Length - 1);

			String sUrl = "CustomerPopup.aspx?FORMID="+"ShipmentTrackingQueryRpt"+
				"&CustType="+strCustPayerType.ToString();
			ArrayList paramList = new ArrayList();
			paramList.Add(sUrl);
			String sScript = Utility.GetScript("openWindowParam.js",paramList);
			Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
		}


		#endregion

		#region "Destination : Controls"

//		private void btnZipCode_Click(object sender, System.EventArgs e)
//		{
//			String sUrl = "ZipCodePopup.aspx?FORMID=ShipmentTrackingQueryRpt"+"&ZIPCODE_CID="+txtZipCode.ClientID+"&ZIPCODE="+txtZipCode.Text.Trim().ToString();
//			ArrayList paramList = new ArrayList();
//			paramList.Add(sUrl);
//			String sScript = Utility.GetScript("openParentWindow.js",paramList);
//			Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
//		}


//		private void btnStateCode_Click(object sender, System.EventArgs e)
//		{
//			String sUrl = "StatePopup.aspx?FORMID=ShipmentTrackingQueryRpt"+"&STATECODE="+txtStateCode.ClientID+"&STATECODE_TEXT="+txtStateCode.Text.Trim().ToString();
//			ArrayList paramList = new ArrayList();
//			paramList.Add(sUrl);
//			String sScript = Utility.GetScript("openParentWindow.js",paramList);
//			Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
//		}


		#endregion

		#region "Dates Part : DropDownList"

		private void ddlmonths()
		{
			DataTable dtMonths = new DataTable();
			dtMonths.Columns.Add(new DataColumn("Text", typeof(string)));
			dtMonths.Columns.Add(new DataColumn("StringValue", typeof(string)));
		
			DataRow drNilRow = dtMonths.NewRow();
			drNilRow[0] = "";
			drNilRow[1] = "";
			dtMonths.Rows.Add(drNilRow);

			ArrayList listMonthTxt = Utility.GetCodeValues(m_strAppID,m_strCulture, "months", CodeValueType.StringValue);
			foreach(SystemCode typeSysCode in listMonthTxt)
			{
				DataRow drEach = dtMonths.NewRow();
				drEach[0] = typeSysCode.Text;
				drEach[1] = typeSysCode.StringValue;
				dtMonths.Rows.Add(drEach);
			}
			DataView dvMonths = new DataView(dtMonths);

			CboMonth.DataSource = dvMonths;
			CboMonth.DataTextField = "Text";
			CboMonth.DataValueField = "StringValue";
			CboMonth.DataBind();
	
			cboMonthend.DataSource = dvMonths;
			cboMonthend.DataTextField ="Text";
			cboMonthend.DataValueField = "StringValue";
			cboMonthend.DataBind();

//			ddMonth.DataSource = dvMonths;
//			ddMonth.DataTextField = "Text";
//			ddMonth.DataValueField = "StringValue";
//			ddMonth.DataBind();	
		}


		#endregion

		#region "Date Part : DropDownList - Cust"

		private void ddlmonthsCust()
		{
			DataTable dtMonthsCust = new DataTable();
			dtMonthsCust.Columns.Add(new DataColumn("Text", typeof(string)));
			dtMonthsCust.Columns.Add(new DataColumn("StringValue", typeof(string)));
		
			DataRow drNilRow = dtMonthsCust.NewRow();
			drNilRow[0] = "";
			drNilRow[1] = "";
			dtMonthsCust.Rows.Add(drNilRow);

			ArrayList listMonthTxt = Utility.GetCodeValues(m_strAppID,m_strCulture, "months", CodeValueType.StringValue);
			foreach(SystemCode typeSysCode in listMonthTxt)
			{
				DataRow drEach = dtMonthsCust.NewRow();
				drEach[0] = typeSysCode.Text;
				drEach[1] = typeSysCode.StringValue;
				dtMonthsCust.Rows.Add(drEach);
			}
			DataView dvMonths = new DataView(dtMonthsCust);

			
		}


		#endregion

		#region "Payer Type Part : DropDownList"

		public void LoadCustomerTypeList()
		{
//			ArrayList systemCodes = Utility.GetCodeValues(utility.GetAppID(),utility.GetUserCulture(),"customer_type",CodeValueType.StringValue);
//			foreach(SystemCode sysCode in systemCodes)
//			{	
//				ListItem lstItem = new ListItem();
//				lstItem.Text = sysCode.Text;
//				lstItem.Value = sysCode.StringValue;
//				lsbCustType.Items.Add(lstItem);
//			}
		}



		#endregion

		#region "Route / DC Selection : DropDownList"

		
		[Cambro.Web.DbCombo.ResultsMethodAttribute(true)]
		public static object DbComboPathCodeSelect(Cambro.Web.DbCombo.ServerMethodArgs args)
		{
			Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings,HttpContext.Current.Session);
			String strAppID = util.GetAppID();
			String strEnterpriseID = util.GetEnterpriseID();
			
			String strDelType = "L";			
			String strWhereClause=null;
			if(args.ServerState != null && args.ServerState.Authenticate("kdfsjgnskajh[apq2-4uhg465654435879mnbfdm cxs;q4"))
			{
				if(args.ServerState["strDeliveryType"] != null && args.ServerState["strDeliveryType"].ToString().Length > 0)
				{
					strDelType = args.ServerState["strDeliveryType"].ToString();
					strWhereClause=" and Delivery_Type='"+Utility.ReplaceSingleQuote(strDelType)+"'";
					if(strDelType == "S") 
					{
						strWhereClause=strWhereClause+"and path_code in ( ";
						strWhereClause=strWhereClause+"select distinct delivery_route ";
						strWhereClause=strWhereClause+"from Zipcode ";
						strWhereClause=strWhereClause+"where applicationid = '"+strAppID+"' ";
						strWhereClause=strWhereClause+"and enterpriseid = '"+ strEnterpriseID +"') ";
					}
				}				
			}
			
			DataSet dataset = com.ties.classes.DbComboDAL.PathCodeQuery(strAppID,strEnterpriseID,args, strWhereClause);	
			return dataset;
		}


		[Cambro.Web.DbCombo.ResultsMethodAttribute(true)]
		public static object DbComboDistributionCenterSelect(Cambro.Web.DbCombo.ServerMethodArgs args)
		{
			Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings,HttpContext.Current.Session);
			String strAppID = util.GetAppID();
			String strEnterpriseID = util.GetEnterpriseID();
			
			DataSet dataset = com.ties.classes.DbComboDAL.DistributionCenterQuery(strAppID,strEnterpriseID,args);	
			return dataset;
		}


		#endregion

		#region "Shipment : DropDownList"

		public void LoadBookingTypeList()
		{
			ListItem defItem = new ListItem();
			defItem.Text = "";
			defItem.Value = "0";
			
			ArrayList systemCodes = Utility.GetCodeValues(utility.GetAppID(),utility.GetUserCulture(),"booking_type",CodeValueType.StringValue);
			
		}

		#endregion

		private void rdDate_CheckedChanged(object sender, System.EventArgs e)
		{
			EnableControl(true);
			lblErrorMessage.Text ="";

			CboMonth.SelectedIndex =0;
			cboMonthend.SelectedIndex =0;
		}

		private void EnableControl(bool flag)
		{
//			txtDate.Enabled = flag;
			CboMonth.Enabled = !flag;
			cboMonthend.Enabled = !flag;

		}

		private void rdMonth_CheckedChanged(object sender, System.EventArgs e)
		{
			EnableControl(false);
			lblErrorMessage.Text ="";
//			txtDate.Text =""; 
		}

		private void CboMonth_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			//Add By Tom 18/5/09
			cboMonthend.SelectedIndex = CboMonth.SelectedIndex;
			//End Add By Tom
		}
//		private void btnZipCodeCust_Click(object sender, System.EventArgs e)
//		{
//			String sUrl = "ZipCodePopup.aspx?FORMID=ShipmentTrackingQueryRpt"+"&ZIPCODE_CID="+txtZipCode.ClientID+"&ZIPCODE="+txtZipCode.Text.Trim().ToString();
//			ArrayList paramList = new ArrayList();
//			paramList.Add(sUrl);
//			String sScript = Utility.GetScript("openParentWindow.js",paramList);
//			Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
//		}
//
//		private void btnStateCodeCust_Click(object sender, System.EventArgs e)
//		{
//			String sUrl = "StatePopup.aspx?FORMID=ShipmentTrackingQueryRpt"+"&STATECODE="+txtStateCode.ClientID+"&STATECODE_TEXT="+txtStateCode.Text.Trim().ToString();
//			ArrayList paramList = new ArrayList();
//			paramList.Add(sUrl);
//			String sScript = Utility.GetScript("openParentWindow.js",paramList);
//			Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
//		}	
		
	}
}