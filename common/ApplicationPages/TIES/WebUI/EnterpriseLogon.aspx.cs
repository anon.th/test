using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Web.Security;
using com.common.RBAC;
using com.common.DAL;
using com.common.util;
using com.common.classes;
using TIES; //Add By Lin 29/4/2010
using com.ties.DAL;    //Add By Lin 29/4/2010

//namespace TIES.WebUI
namespace com.ties
{
	/// <summary>
	/// Summary description for EnterpriseLogon.
	/// </summary>
	public class EnterpriseLogon : System.Web.UI.Page 
	{
		protected System.Web.UI.WebControls.RequiredFieldValidator Requiredfieldvalidator1;
		protected System.Web.UI.WebControls.RequiredFieldValidator Requiredfieldvalidator2;
		protected System.Web.UI.WebControls.RequiredFieldValidator Requiredfieldvalidator3;
		protected System.Web.UI.WebControls.Label Label1;
		protected com.common.util.msTextBox txtUserID;
		protected System.Web.UI.WebControls.Label Label2;
		protected System.Web.UI.WebControls.TextBox txtUserPswd;
		protected System.Web.UI.WebControls.Label Label3;
		protected System.Web.UI.WebControls.TextBox txtEnterpriseID;
		protected System.Web.UI.WebControls.Label lblErrMsg;
		protected System.Web.UI.WebControls.Button btnSubmit;
		protected System.Web.UI.WebControls.Button btnCancel;
		Utility utility = null;
	
		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings,Page.Session);
			if(!Page.IsPostBack)
			{
				Response.CacheControl = "no-cache";
				Response.AddHeader("Pragma","no-cache");
				Response.Expires = -1;
				if(lblErrMsg.Text == "")
				{
					if (Request.Params["errMsg"] != null)
					{
						//lblErrMsg.Text=Request.Params["errMsg"].ToString();
						if (Request.Params["errMsg"].ToString().Equals("TIMEOUT"))
						{
							if (Request.Params["culture"] != null)
							{
								lblErrMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage, "SESSION_TIMEOUT", Request.Params["culture"].ToString());
							}
							else
							{
								lblErrMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage, "SESSION_TIMEOUT", utility.GetUserCulture());
							}
						}
						else
						{
							if (Request.Params["culture"] != null)
							{
								lblErrMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage, "SESSION_LOGOUT", Request.Params["culture"].ToString());
							}
							else
							{
								lblErrMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage, "SESSION_LOGOUT", utility.GetUserCulture());
							}
						}
					}
				}
			}
			
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnSubmit.Click += new System.EventHandler(this.btnSubmit_Click);
			this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private void btnSubmit_Click(object sender, System.EventArgs e)
		{
			ArrayList userRolesList = null;
			bool isValid = RBACManager.AuthenticateUser(utility.GetAppID(),txtEnterpriseID.Text.Trim(),txtUserID.Text.Trim(),txtUserPswd.Text.Trim());
			DataSet dsCon = new DataSet();  //Add By Lin 29/4/2010 
			DataTable dtCon = new DataTable(); //Add By Lin 29/4/2010
			DataRow drCon;  //Add By Lin 29/4/2010
			DataSet dsParam = new DataSet();  //Add By Lin 29/4/2010 
			DataTable dtParam = new DataTable(); //Add By Lin 29/4/2010
			string ParamName = "";

			if (isValid)
			{
				lblErrMsg.Text = "";

				com.common.classes.User user = RBACManager.GetUser(utility.GetAppID(),txtEnterpriseID.Text.Trim(),txtUserID.Text.Trim());

				user.UserID = txtUserID.Text.Trim();
				userRolesList = RBACManager.GetAllRoles(utility.GetAppID(),txtEnterpriseID.Text.Trim(),user);
				
						
				String strRoles = null;
				foreach (Role role in userRolesList)
				{
					strRoles += role.RoleName;
					strRoles += ";";
				}
			
				Session["userID"] = user.UserID;
				Session["applicationID"] = System.Configuration.ConfigurationSettings.AppSettings["applicationID"];
				Session["enterpriseID"] = txtEnterpriseID.Text.Trim();
				Session["userCulture"] = user.UserCulture;
				Session["UserName"] = user.UserName; //Add By Lin 28/4/2010
				Session["PayerID"] = user.PayerID; //Add By Lin 28/4/2010
				Session["UserLocation"] = user.UserLocation;  //Add By Lin 28/4/2010
				Session["UserRole"] = strRoles;  //Add By Lin 28/4/2010


				//start Add By Lin 29/4/2010
				if (user.PayerID.ToString() != "")
				{
					// Get Customer Data
					dsCon = ConsignmentNoteDAL.GetInitConData(utility.GetAppID(), utility.GetEnterpriseID(), user.PayerID);     
					dtCon = dsCon.Tables[0]; 
					if (dtCon != null && dtCon.Rows.Count > 0)
					{
						drCon = dtCon.Rows[0];
						Session["PayerID"] = drCon["custid"] + "";
						Session["CustID"] = drCon["custid"] + "";
						Session["CustName"] = drCon["cust_name"] + "";
						Session["Remark2"] = drCon["Remark2"] + "";
						Session["SenderZipcode"] = drCon["zipcode"] + "";
					}

					#region Edit by Sompote 2010-08-06
					// Get Parameter ConsignmentNote
					try
					{
						dsParam = ConsignmentNoteDAL.GetParameterConsignment(utility.GetAppID(), utility.GetEnterpriseID(), user.PayerID);
						dtParam = dsParam.Tables[0];
						if (dtParam != null && dtParam.Rows.Count > 0)
						{
							foreach (DataRow drParam in dtParam.Rows)
							{
								ParamName = "";
								ParamName = drParam["ParamName"] + "";
							
								if (ParamName == "Sender_Contact_Name") {Session["SenderContactName"] = drParam["ParamValue"] + "";}
								if (ParamName == "Sender_Telephone") {Session["SenderTelephone"] = drParam["ParamValue"] + "";}
								if (ParamName == "Default_Service_Type") {Session["DefaultServiceType"] = drParam["ParamValue"] + "";}
								if (ParamName == "Special_Instruction") {Session["SpecialInstruction"] = drParam["ParamValue"] + "";}
							}
						}
					}
					catch(Exception ex)
					{
						//String msg = "This user is unavailable in this version.";
						//lblErrMsg.Text = msg; 
					}
//					dsParam = ConsignmentNoteDAL.GetParameterConsignment(utility.GetAppID(), utility.GetEnterpriseID(), user.PayerID);
//					dtParam = dsParam.Tables[0];
//					if (dtParam != null && dtParam.Rows.Count > 0)
//					{
//						foreach (DataRow drParam in dtParam.Rows)
//						{
//							ParamName = "";
//							ParamName = drParam["ParamName"] + "";
//							
//							if (ParamName == "Sender_Contact_Name") {Session["SenderContactName"] = drParam["ParamValue"] + "";}
//							if (ParamName == "Sender_Telephone") {Session["SenderTelephone"] = drParam["ParamValue"] + "";}
//							if (ParamName == "Default_Service_Type") {Session["DefaultServiceType"] = drParam["ParamValue"] + "";}
//							if (ParamName == "Special_Instruction") {Session["SpecialInstruction"] = drParam["ParamValue"] + "";}
//						}
//					}
					#endregion
				}
				//end Add By Lin 29/4/2010

				FormsAuthenticationTicket formsAuthenticationTicket = null;
				//Context.User.Identity.Name = user.UserID;


				try
				{
					formsAuthenticationTicket = new FormsAuthenticationTicket(1,Context.User.Identity.Name,DateTime.Now,DateTime.Now.AddHours(1), false, strRoles);
					//Encrypt the ticket
					String strCookie  = FormsAuthentication.Encrypt(formsAuthenticationTicket);

					//Send the cookie to the client
					String strCookieName = System.Configuration.ConfigurationSettings.AppSettings["appcookiename"];
					Response.Cookies[strCookieName].Value = strCookie;
					Response.Cookies[strCookieName].Path = "/";
					Response.Cookies[strCookieName].Expires = DateTime.Now.AddMinutes(1);

					FormsAuthentication.SetAuthCookie(user.UserID,false);

					//Response.Redirect("LaunchPage.aspx");
					
					Response.Redirect("MainFrame.aspx");
				}
				catch(Exception exp)
				{
					Console.WriteLine("ERRRRor "+ exp.Message.ToString());
				}
			}
			else
			{
				//String msg = Utility.GetLanguageText(ResourceType.UserMessage,"INVALID_LOGIN",utility.GetUserCulture());
				String msg = "Please enter User ID/Password/Enterprise ID to login";
				lblErrMsg.Text = msg; 
			}
			
		}

		private void btnCancel_Click(object sender, System.EventArgs e)
		{
			lblErrMsg.Text = "";
			txtEnterpriseID.Text = "";
			txtUserID.Text = "";
			txtUserPswd.Text = "";
		}

	}
}
