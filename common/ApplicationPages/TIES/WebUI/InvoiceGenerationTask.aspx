<%@ Page language="c#" Codebehind="InvoiceGenerationTask.aspx.cs" AutoEventWireup="false" Inherits="com.ties.InvoiceGenerationTask" %>
<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<%@ Import Namespace="com.common.util" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>InvoiceGenerationTask</title>
		<LINK href="css/Styles.css" type="text/css" rel="stylesheet">
		<meta name="GENERATOR" Content="Microsoft Visual Studio 7.0">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<!--#INCLUDE FILE="msFormValidations.inc"-->
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="InvoiceGenerationTask" method="post" runat="server">
			<asp:button id="btnQuery" style="Z-INDEX: 105; LEFT: 32px; POSITION: absolute; TOP: 65px" runat="server" CssClass="queryButton" Text="Query" CausesValidation="False"></asp:button>
			<asp:button id="btnExecuteQuery" style="Z-INDEX: 102; LEFT: 91px; POSITION: absolute; TOP: 65px" runat="server" CssClass="queryButton" Text="Execute Query" CausesValidation="False" Width="139px"></asp:button>
			<asp:datagrid id="dgParent" style="Z-INDEX: 101; LEFT: 30px; POSITION: absolute; TOP: 150px" runat="server" Width="777px" AllowCustomPaging="True" AllowPaging="True" OnDeleteCommand="dgParent_Delete" OnItemDataBound="dgParent_Bound" OnPageIndexChanged="dgParent_PageChange" AutoGenerateColumns="False" ItemStyle-Height="20">
				<ItemStyle Height="20px" CssClass="gridField"></ItemStyle>
				<Columns>
					<asp:ButtonColumn Text="D" ButtonType="PushButton" Visible="False" CommandName="Delete">
						<HeaderStyle Width="2%" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle HorizontalAlign="Center"></ItemStyle>
					</asp:ButtonColumn>
					<asp:TemplateColumn HeaderText="JobID">
						<HeaderStyle Width="8%" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle CssClass="gridField"></ItemStyle>
						<ItemTemplate>
							<asp:Label CssClass="gridLabelNumber" ID="lblJobID" Text='<%#DataBinder.Eval(Container.DataItem,"jobid")%>' Runat="server">
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<cc1:mstextbox CssClass="gridTextBoxNumber" ID="txtJobID" Text='<%#DataBinder.Eval(Container.DataItem,"jobid")%>' Runat="server" MaxLength="9" TextMaskType="msNumeric" NumberMaxValue="99999999" NumberMinValue="1" NumberPrecision="8" NumberScale="0">
							</cc1:mstextbox>
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Start Datetime">
						<HeaderStyle Width="11%" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle CssClass="gridField"></ItemStyle>
						<ItemTemplate>
							<asp:Label CssClass="gridLabelNumber" ID="lblStartDateTime" Text='<%#DataBinder.Eval(Container.DataItem,"start_datetime","{0:dd/MM/yyyy HH:mm}")%>' Runat="server">
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<cc1:mstextbox CssClass="gridTextBoxNumber" ID="txtStartDateTime" Text='<%#DataBinder.Eval(Container.DataItem,"start_datetime","{0:dd/MM/yyyy}")%>' Runat="server" Enabled="True" TextMaskType="msDate" TextMaskString="99/99/9999" MaxLength="10">
							</cc1:mstextbox>
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="End Datetime">
						<HeaderStyle Width="11%" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle CssClass="gridField"></ItemStyle>
						<ItemTemplate>
							<asp:Label CssClass="gridLabelNumber" ID="lblEndDateTime" Text='<%#DataBinder.Eval(Container.DataItem,"end_datetime","{0:dd/MM/yyyy HH:mm}")%>' Runat="server">
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<cc1:mstextbox CssClass="gridTextBoxNumber" ID="txtEndDateTime" Text='<%#DataBinder.Eval(Container.DataItem,"end_datetime","{0:dd/MM/yyyy}")%>' Runat="server" Enabled="True" TextMaskType="msDate" TextMaskString="99/99/9999" MaxLength="10">
							</cc1:mstextbox>
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Job Status">
						<HeaderStyle Width="15%" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle VerticalAlign="Middle"></ItemStyle>
						<ItemTemplate>
							<asp:DropDownList CssClass="gridDropDown" ID="ddlJobStatus" Runat="server"></asp:DropDownList>
						</ItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Remark">
						<HeaderStyle Width="55%" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle CssClass="gridField"></ItemStyle>
						<ItemTemplate>
							<asp:Label CssClass="gridLabel" ID="lblRemark" Text='<%#DataBinder.Eval(Container.DataItem,"remark")%>' Runat="server" Enabled="True">
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<asp:TextBox CssClass="gridTextBox" ID="txtRemark" Text='<%#DataBinder.Eval(Container.DataItem,"remark")%>' Runat="server">
							</asp:TextBox>
						</EditItemTemplate>
					</asp:TemplateColumn>
				</Columns>
				<PagerStyle NextPageText="Next" PrevPageText="Previous" HorizontalAlign="Left" CssClass="normalText"></PagerStyle>
			</asp:datagrid>
			<asp:label id="lblTitle" style="Z-INDEX: 111; LEFT: 30px; POSITION: absolute; TOP: 12px" runat="server" CssClass="mainTitleSize" Height="26px" Width="477px">Query on Invoice Generation</asp:label>
			<asp:label id="lblErrorMessage" style="Z-INDEX: 105; LEFT: 30px; POSITION: absolute; TOP: 96px" runat="server" CssClass="errorMsgColor" Height="42px" Width="595px">Place holder for err msg</asp:label>
		</form>
	</body>
</HTML>
