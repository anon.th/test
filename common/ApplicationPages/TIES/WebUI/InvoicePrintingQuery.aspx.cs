using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.common.util;
using com.common.DAL;
using com.ties.DAL;
using com.ties.BAL;
using com.common.classes;
using com.common.applicationpages;

namespace com.ties
{
	/// <summary>
	/// Summary description for InvoicePrintingQuery.
	/// </summary>
	public class InvoicePrintingQuery : BasePage
	{
		protected System.Web.UI.WebControls.Button btnPrint;
		protected System.Web.UI.WebControls.Button btnClear;
		protected System.Web.UI.WebControls.Label lblErrorMessage;
		protected System.Web.UI.WebControls.Label lblCustomerType;
		protected System.Web.UI.WebControls.RadioButton rbAgent;
		protected System.Web.UI.WebControls.RadioButton rbCustomer;
		protected System.Web.UI.WebControls.RadioButton rbBoth;
		protected System.Web.UI.WebControls.Label lblCustomerCode;
		protected com.common.util.msTextBox txtCustomerCode;
		protected System.Web.UI.WebControls.Button btnCustomerCode;
		protected System.Web.UI.WebControls.Label lblCustomerName;
		protected com.common.util.msTextBox txtCustomerName;
		protected System.Web.UI.WebControls.Label lblTitle;
		protected System.Web.UI.WebControls.RadioButton rbSelective;
		protected System.Web.UI.WebControls.RadioButton rbAll;
		protected System.Web.UI.WebControls.Label lblFromInvNo;
		protected System.Web.UI.WebControls.Label lblToInvNo;
		protected System.Web.UI.WebControls.Label lblInvoiceDate;
		protected System.Web.UI.WebControls.Label lblInvoiceNo;
		protected com.common.util.msTextBox txtToInvNo;
		protected System.Web.UI.WebControls.Label lblFromInvDate;
		protected com.common.util.msTextBox txtFromInvDate;
		protected com.common.util.msTextBox txtToInvDate;
		protected com.common.util.msTextBox txtFromInvNo;
		protected System.Web.UI.WebControls.Label lblToInvDate;
		protected System.Web.UI.HtmlControls.HtmlTable tblInvoicePrintingQry;
		
		//Utility utility = null;
		private String m_strAppID;
		private String m_strEnterpriseID;
		protected System.Web.UI.HtmlControls.HtmlTable tblInvoiceQuery;
		protected System.Web.UI.WebControls.Label Label2;
		protected System.Web.UI.WebControls.Label Label1;
		DataSet m_dsQuery=null;

		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			//utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings,Page.Session);
			if (!Page.IsPostBack)
			{
				m_strAppID = utility.GetAppID();
				m_strEnterpriseID = utility.GetEnterpriseID();
				QueryMode();
			}
			else
				lblErrorMessage.Text = "";
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			base.OnInit(e);
			InitializeComponent();
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
			this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
			this.rbAll.CheckedChanged += new System.EventHandler(this.rbAll_CheckedChanged);
			this.rbSelective.CheckedChanged += new System.EventHandler(this.rbSelective_CheckedChanged);
			this.rbAgent.CheckedChanged += new System.EventHandler(this.rbAgent_CheckedChanged);
			this.rbCustomer.CheckedChanged += new System.EventHandler(this.rbCustomer_CheckedChanged);
			this.rbBoth.CheckedChanged += new System.EventHandler(this.rbBoth_CheckedChanged);
			this.btnCustomerCode.Click += new System.EventHandler(this.btnCustomerCode_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private void btnPrint_Click(object sender, System.EventArgs e)
		{
			CreateParamDS();
			Session["FORMID"] = "InvoicePrintingQuery";
			Session["SESSION_DS1"] = m_dsQuery;

			String sUrl = "ReportViewer.aspx";
			ArrayList paramList = new ArrayList();
			paramList.Add(sUrl);
			String sScript = Utility.GetScript("openParentWindow.js",paramList);
			Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
		}

		private void btnClear_Click(object sender, System.EventArgs e)
		{
			QueryMode();
		}

		private void rbAll_CheckedChanged(object sender, System.EventArgs e)
		{
		
		}

		private void rbSelective_CheckedChanged(object sender, System.EventArgs e)
		{
		
		}

		private void rbAgent_CheckedChanged(object sender, System.EventArgs e)
		{
			EnableCustomer();
		}

		private void rbCustomer_CheckedChanged(object sender, System.EventArgs e)
		{
			EnableCustomer();
		}

		private void rbBoth_CheckedChanged(object sender, System.EventArgs e)
		{
			DisableCustomer();
		}

		private void btnCustomerCode_Click(object sender, System.EventArgs e)
		{
			if (rbCustomer.Checked == true)
			{
				String sUrl = "CustomerPopup.aspx?FORMID="+"InvoicePrintingQuery";
				ArrayList paramList = new ArrayList();
				paramList.Add(sUrl);
				String sScript = Utility.GetScript("openWindowParam.js",paramList);
				Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
			}
			else if (rbAgent.Checked == true)
			{
				String sUrl = "AgentPopup.aspx?FORMID=InvoicePrintingQuery"+"&CUSTID_TEXT="+txtCustomerCode.Text.Trim().ToString();
				ArrayList paramList = new ArrayList();
				paramList.Add(sUrl);
				String sScript = Utility.GetScript("openWindowParam.js",paramList);
				Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
			}
		}

		private void QueryMode()
		{
			rbAll.Checked = true;
			rbSelective.Checked = false;
			rbAgent.Checked = false;
			rbCustomer.Checked = false;
			rbBoth.Checked = true;
			txtFromInvNo.Text = null;
			txtToInvNo.Text = null;
			txtFromInvDate.Text = null;
			txtToInvDate.Text = null;
			lblErrorMessage.Text = "";
			DisableCustomer();
		}

		private void EnableCustomer()
		{
			txtCustomerCode.Text = null;
			txtCustomerCode.Enabled = true;
			txtCustomerCode.BackColor = System.Drawing.Color.White;
			btnCustomerCode.Enabled = true;
			txtCustomerName.Text = null;
			txtCustomerName.Enabled = true;
			txtCustomerName.BackColor = System.Drawing.Color.White;
			if (rbAgent.Checked==true)
			{
				lblCustomerCode.Text = "Agent Code";
				lblCustomerName.Text = "Agent Name";
			}
			else if(rbCustomer.Checked==true)
			{
				lblCustomerCode.Text = "Customer Code";
				lblCustomerName.Text = "Customer Name";
			}
			if(!utility.GetUserCulture().ToUpper().Equals("EN-US"))
			{
				lblCustomerCode.Text = Utility.GetLanguageText(ResourceType.ScreenLabel, lblCustomerCode.Text, utility.GetUserCulture());
				lblCustomerName.Text = Utility.GetLanguageText(ResourceType.ScreenLabel, lblCustomerName.Text, utility.GetUserCulture());
			}
		}

		private void DisableCustomer()
		{
			txtCustomerCode.Text = null;
			txtCustomerCode.BackColor = System.Drawing.Color.LightGray;
			txtCustomerCode.Enabled = false;
			btnCustomerCode.Enabled = false;
			txtCustomerName.Text = null;
			txtCustomerName.BackColor = System.Drawing.Color.LightGray;
			txtCustomerName.Enabled = false;
		}

		private void CreateParamDS()
		{
			//create table for dataset
			DataTable dtParam = new DataTable();
			dtParam.Columns.Add(new DataColumn("unprinted", typeof(string)));
			dtParam.Columns.Add(new DataColumn("payertype",typeof(string)));
			dtParam.Columns.Add(new DataColumn("payerid",typeof(string)));
			dtParam.Columns.Add(new DataColumn("payername",typeof(string)));
			dtParam.Columns.Add(new DataColumn("startno",typeof(string)));
			dtParam.Columns.Add(new DataColumn("endno",typeof(string)));
			dtParam.Columns.Add(new DataColumn("startdate",typeof(string)));
			dtParam.Columns.Add(new DataColumn("enddate",typeof(string)));

			m_dsQuery = new DataSet();
			m_dsQuery.Tables.Add(dtParam);

			//insert row for dataset table
			DataRow drNew = m_dsQuery.Tables[0].NewRow();

			if (rbAll.Checked)
				drNew["unprinted"] = "Y";
			else if (rbSelective.Checked)
				drNew["unprinted"] = "N";

			if (rbAgent.Checked)
				drNew["payertype"] = "A";
			else if (rbCustomer.Checked)
				drNew["payertype"] = "C";

			if (Utility.IsNotDBNull(txtCustomerCode.Text))
				drNew["payerid"] = txtCustomerCode.Text;

			if (Utility.IsNotDBNull(txtCustomerName.Text))
				drNew["payername"]=txtCustomerName.Text;

			if (Utility.IsNotDBNull(txtFromInvNo.Text))
				drNew["startno"]=txtFromInvNo.Text;

			if (Utility.IsNotDBNull(txtToInvNo.Text))
				drNew["endno"]=txtToInvNo.Text;

			if (Utility.IsNotDBNull(txtFromInvDate.Text))
				drNew["startdate"]=txtFromInvDate.Text;

			if (Utility.IsNotDBNull(txtToInvDate.Text))
				drNew["enddate"]=txtToInvDate.Text;

			m_dsQuery.Tables[0].Rows.Add(drNew);
		}

	}
}
