using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.common.util;
using com.ties.DAL;
using com.common.classes;
using com.ties.classes;
using com.common.applicationpages;
using Cambro.Web.DbCombo;

namespace com.ties
{
	/// <summary>
	/// Summary description for AgentConveyanceRates.
	/// </summary>
	public class AgentConveyanceRates : com.common.applicationpages.BasePage
	{
	
		//		Utility utility=null;
		string m_strAppID=null;
		string m_strEnterpriseID=null;
		SessionDS m_sdsConveyance = null;
		SessionDS m_sdsAgent = null;
		static private int m_iSetSize = 4;
		private string sDbComboRegKey="";
//		protected AccessRights m_moduleAccessRights = null;

		protected System.Web.UI.HtmlControls.HtmlTable tblMain;
		protected System.Web.UI.WebControls.DataGrid dgBaseConveyance;
		protected System.Web.UI.WebControls.Label lblErrorMessage;
		protected System.Web.UI.WebControls.Button btnExecuteQuery;
		protected System.Web.UI.WebControls.Button btnQuery;
		protected System.Web.UI.WebControls.Label lblAgentCode;
		protected System.Web.UI.WebControls.Label lblAgentName;
		protected System.Web.UI.WebControls.Button btnGoToLastPage;
		protected System.Web.UI.WebControls.Button btnNextPage;
		protected System.Web.UI.WebControls.TextBox txtCountRec;
		protected System.Web.UI.WebControls.Button btnPreviousPage;
		protected System.Web.UI.WebControls.Button btnGoToFirstPage;
		protected Cambro.Web.DbCombo.DbCombo dbCmbAgentId;
		protected Cambro.Web.DbCombo.DbCombo dbCmbAgentName;
		protected System.Web.UI.WebControls.RequiredFieldValidator reqAgentId;
		protected System.Web.UI.WebControls.ValidationSummary PageValidationSummary;
		protected System.Web.UI.WebControls.Label lblTitle;
		protected System.Web.UI.WebControls.Button btnInsert;
		protected System.Web.UI.WebControls.TextBox txtConveyance;
		protected System.Web.UI.WebControls.TextBox txtOZCode;
		protected System.Web.UI.WebControls.TextBox txtDZCode;
		

		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here			

			sDbComboRegKey=System.Configuration.ConfigurationSettings.AppSettings["DbComboRegKey"]; //"aeaaaaU99999baaaaaaaaaEbbaaaauxm6rdDVU1m6ndDVULm6fdDFn1yJ3gBFnx-FZGmfn0~RVxqXnwzSr1xd7w~RvL~NnwyYJ0~Sn1xqrxzDVeBIVLmUadm6fdDVU1m2yto6fdDVULm6fh~vZGmZq0yReHqyZcmdZsnfJwlUqdCIzZrVZWmcZsnczXc";//System.Configuration.ConfigurationSettings.AppSettings["DbComboRegKey"];
			DbComboRegKey();
			m_strAppID = utility.GetAppID();
			m_strEnterpriseID = utility.GetEnterpriseID();			
			if(!IsPostBack)
			{
				QueryMode();		
				ViewState["AgentOperation"] = Operation.None;
				ViewState["AgentMode"]= ScreenMode.None;
				ViewState["MovePrevious"]=false;
				ViewState["MoveNext"]=false;
				ViewState["MoveFirst"]=false;
				ViewState["MoveLast"]=false;
			}
			else
			{
				if (Session["SESSION_DS1"] !=null)
				{
					m_sdsConveyance = (SessionDS) Session["SESSION_DS1"];
				}
				if (Session["SESSION_DS3"] !=null)
				{
					m_sdsAgent	= (SessionDS)Session["SESSION_DS3"];
				}
			}
			addDbComboEventHandler();
			PageValidationSummary.HeaderText = Utility.GetLanguageText(ResourceType.UserMessage, "PLS_ENTER_MISS_FIELD", utility.GetUserCulture());
		}
	
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			base.OnInit(e);
			InitializeComponent();
			
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnInsert.Click += new System.EventHandler(this.btnInsert_Click);
			this.btnGoToFirstPage.Click += new System.EventHandler(this.btnGoToFirstPage_Click);
			this.btnPreviousPage.Click += new System.EventHandler(this.btnPreviousPage_Click);
			this.btnNextPage.Click += new System.EventHandler(this.btnNextPage_Click);
			this.btnGoToLastPage.Click += new System.EventHandler(this.btnGoToLastPage_Click);
			this.btnQuery.Click += new System.EventHandler(this.btnQuery_Click);
			this.btnExecuteQuery.Click += new System.EventHandler(this.btnExecuteQuery_Click);
			this.dbCmbAgentName.SelectedItemChanged += new System.EventHandler(this.dbCmbAgentName_SelectedItemChanged);
			this.dbCmbAgentId.SelectedItemChanged += new System.EventHandler(this.dbCmbAgentId_SelectedItemChanged);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private void addDbComboEventHandler()
		{
			if(dgBaseConveyance.EditItemIndex == -1)
				return;

			if(dgBaseConveyance.Items.Count==0)
				return;

			try
			{
				this.txtOZCode = (msTextBox)dgBaseConveyance.Items[dgBaseConveyance.EditItemIndex].FindControl("txtOZCode");
				this.txtDZCode = (msTextBox)dgBaseConveyance.Items[dgBaseConveyance.EditItemIndex].FindControl("txtDZCode");
				this.txtConveyance = (msTextBox)dgBaseConveyance.Items[dgBaseConveyance.EditItemIndex].FindControl("txtConveyance");
				if(this.txtOZCode != null && this.txtDZCode !=null)
				{
					SetAgentIDServerStates();
					SetAgentNameServerStates();
				}
			}
			catch (Exception ex)
			{
				lblErrorMessage.Text=ex.Message.ToString();
			}
		}
				
		private void SetAgentNameServerStates()
		{						
			String strAgentId=dbCmbAgentId.Value;			
			Hashtable hash = new Hashtable();		
			if (strAgentId !="")
			{
				hash.Add("strAgentId", strAgentId);
			}			
			dbCmbAgentName.ServerState = hash;
			dbCmbAgentName.ServerStateSecretString = "kdfsjgnskajh[apq2-4uhg465654435879mnbfdm cxs;q4";

		}

		private void SetAgentIDServerStates()
		{			
			String strAgentName=dbCmbAgentName.Value;			
			Hashtable hash = new Hashtable();			
			if (strAgentName !="")
			{
				hash.Add("strAgentName", strAgentName);
			}						

			dbCmbAgentId.ServerState = hash;
			dbCmbAgentId.ServerStateSecretString = "kdfsjgnskajh[apq2-4uhg465654435879mnbfdm cxs;q4";			
		}

		[Cambro.Web.DbCombo.ResultsMethodAttribute(true)]		
		public static object AgentIdServerMethod(Cambro.Web.DbCombo.ServerMethodArgs args)
		{
			Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings,HttpContext.Current.Session);
			String strAppID = util.GetAppID();
			String strEnterpriseID = util.GetEnterpriseID();
						
			String strAgentName="";
			if(args.ServerState != null && args.ServerState.Authenticate("kdfsjgnskajh[apq2-4uhg465654435879mnbfdm cxs;q4"))
			{			
				if(args.ServerState["strAgentName"] != null && args.ServerState["strAgentName"].ToString().Length > 0)
				{
					strAgentName = args.ServerState["strAgentName"].ToString();					
				}	
			}
			
			DataSet dataset = DbComboDAL.PayerIDQuery(strAppID,strEnterpriseID,args, "A", strAgentName);	
			return dataset;
		}

		[Cambro.Web.DbCombo.ResultsMethod(true)]
		public static object AgentNameServerMethod(Cambro.Web.DbCombo.ServerMethodArgs args)
		{
			Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings,HttpContext.Current.Session);
			String strAppID = util.GetAppID();
			String strEnterpriseID = util.GetEnterpriseID();
	
			String strAgentId= "";

			if(args.ServerState != null && args.ServerState.Authenticate("kdfsjgnskajh[apq2-4uhg465654435879mnbfdm cxs;q4"))
			{				
				if (args.ServerState["strAgentId"] != null && args.ServerState["strAgentId"].ToString().Length > 0 )
				{
					strAgentId = (args.ServerState["strAgentId"].ToString());
				}
			}	

			DataSet dataset = DbComboDAL.PayerNameQuery(strAppID,strEnterpriseID,args, "A", strAgentId);	
			return dataset;                
		}

		public void BindConveyanceGrid()
		{
			dgBaseConveyance.VirtualItemCount = System.Convert.ToInt32(m_sdsConveyance.QueryResultMaxSize);
			dgBaseConveyance.DataSource = m_sdsConveyance.ds;
			dgBaseConveyance.DataBind(); 
			Session["SESSION_DS1"] = m_sdsConveyance;
		}

		public void QueryMode()
		{
			ViewState["AgentOperation"] = Operation.None;
			ViewState["AgentMode"]		= ScreenMode.Query;
			ViewState["ConvOperation"]=Operation.None;
			ViewState["ConvMode"]=ScreenMode.Query;
			
			m_sdsAgent = AgentProfileMgrDAL.GetEmptyAgentDS(1);
			EnableNavigationButtons(false,false,false,false);

			btnQuery.Enabled=true;
			btnExecuteQuery.Enabled=true;
			Utility.EnableButton(ref btnInsert,ButtonType.Insert,true,m_moduleAccessRights);
			
			dbCmbAgentId.Value="";dbCmbAgentName.Value="";
			dbCmbAgentId.Text="";dbCmbAgentName.Text="";

			ClearDbCombos();
			
			m_sdsConveyance = SysDataManager1.GetEmptyBaseConvey() ;  
			m_sdsConveyance.DataSetRecSize = 1;
			AddRow(); 
			Session["SESSION_DS1"] = m_sdsConveyance;
			BindConveyanceGrid();
			lblErrorMessage.Text="";
			
			EditHRow(true);
			dgBaseConveyance.Columns[0].Visible=false;
			dgBaseConveyance.Columns[1].Visible=false;
			dbCmbAgentId.Enabled=true;
			dbCmbAgentName.Enabled=true;
		}

		private void AddRow()
		{
			SysDataManager1.AddRowInBaseConveyDS(ref m_sdsConveyance);
			BindConveyanceGrid();
		}

		private void EditHRow(bool bItemIndex)
		{
			foreach (DataGridItem item in dgBaseConveyance.Items)
			{ 
				if (bItemIndex) 
					dgBaseConveyance.EditItemIndex = item.ItemIndex; 
				else 
					dgBaseConveyance.EditItemIndex = -1; 
			}
			dgBaseConveyance.DataBind();
		}

		private void GetChangedRow(DataGridItem item, int rowIndex)
		{
			this.txtOZCode = (msTextBox)item.FindControl("txtOZCode");
			this.txtDZCode = (msTextBox)item.FindControl("txtDZCode");			
			this.txtConveyance = (msTextBox) item.FindControl("txtConveyance");						
			msTextBox txtStartPrice = (msTextBox)item.FindControl("txtStartPrice");
			msTextBox txtIncrementPrice = (msTextBox)item.FindControl("txtIncrementPrice");
			
			string strOZCode = this.txtOZCode.Text;
			string strDZCode = this.txtDZCode.Text;			
			string strConveyance = this.txtConveyance.Text;
			decimal dStart_Price = Convert.ToDecimal(txtStartPrice.Text);
			decimal dInc_Price = Convert.ToDecimal(txtIncrementPrice.Text);
									
			DataRow dr = m_sdsConveyance.ds.Tables[0].Rows[rowIndex];
			dr[0] = strOZCode;
			dr[1] = strDZCode;
			dr[2] = strConveyance;
			dr[3] = dStart_Price;
			dr[4] = dInc_Price;
			
			Session["SESSION_DS1"] = m_sdsConveyance;
			
		}


		/*---*/
		#region Base Conveyance Grid functions
		private void RetreiveSelectedPage()
		{
			String strAgentId=(String)ViewState["AgentID"];
			int iStartRow = dgBaseConveyance.CurrentPageIndex * dgBaseConveyance.PageSize;
			DataSet dsQuery = (DataSet)ViewState["QUERY_DSC"];			
			//			
			m_sdsConveyance = SysDataManager1.GetBaseConveyDS(m_strAppID, m_strEnterpriseID, strAgentId, iStartRow, dgBaseConveyance.PageSize, dsQuery);
			decimal iPageCnt = Convert.ToInt32((m_sdsConveyance.QueryResultMaxSize - 1))/dgBaseConveyance.PageSize;
			if(iPageCnt < dgBaseConveyance.CurrentPageIndex)
			{
				dgBaseConveyance.CurrentPageIndex = System.Convert.ToInt32(iPageCnt);
			}
			dgBaseConveyance.SelectedIndex = -1;
			dgBaseConveyance.EditItemIndex = -1;
			lblErrorMessage.Text = "";
			BindConveyanceGrid();
			Session["SESSION_DS1"] = m_sdsConveyance;
		}

		public void dgBaseConvey_Button(object sender, DataGridCommandEventArgs e)
		{
			int iOperation = (int)ViewState["ConvOperation"];
			int iMode =(int)ViewState["ConvMode"];
			String strCmdNm = e.CommandName;
			String strtxtZoneCode = null; 
			String strZoneCode = null;
			String strtxtConveyanceCode=null;
			String strConveyanceCode=null;
			String sUrl=null;
			String sScript=null;
			ArrayList paramList=null;
		
			if (strCmdNm.Equals("Update") || strCmdNm.Equals("Edit") || strCmdNm.Equals("Delete") || strCmdNm.Equals("Cancel"))
			{
				return;
			}
			if (iMode==(int)ScreenMode.ExecuteQuery)
			{
				return;
			}
			if (strCmdNm.Equals("OriginSearch") )
			{
				this.txtOZCode = (msTextBox)e.Item.FindControl("txtOZCode");
				if(this.txtOZCode != null)
				{
					strtxtZoneCode = this.txtOZCode.ClientID;
					strZoneCode = this.txtOZCode.Text;
				}
			}
			else if (strCmdNm.Equals("DestinationSearch") )
			{
				this.txtDZCode = (msTextBox)e.Item.FindControl("txtDZCode");
				if(txtDZCode != null)
				{
					strtxtZoneCode = this.txtDZCode.ClientID;
					strZoneCode = this.txtDZCode.Text;
				}
			}
			else if (strCmdNm.Equals("ConveyanceSearch"))
			{
				this.txtConveyance = (msTextBox)e.Item.FindControl("txtConveyance");
				if(this.txtConveyance != null)
				{
					strtxtConveyanceCode = this.txtConveyance.ClientID;
					strConveyanceCode = this.txtConveyance.Text;
				}
			
				sUrl = "ConveyancePopup.aspx?FORMID=AgentConveyanceRates&CONVEYANCECODE_TEXT="+strtxtConveyanceCode+"&CONVEYANCECODE="+strConveyanceCode;
				paramList = new ArrayList();
				paramList.Add(sUrl);
				sScript = Utility.GetScript("openWindowParam.js",paramList);
				Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
				return;
			}

			sUrl = "ZonePopup.aspx?FORMID=AgentConveyanceRates&ZONECODE_TEXT="+strZoneCode+"&ZONECODE="+strtxtZoneCode;
			paramList = new ArrayList();
			paramList.Add(sUrl);
			sScript = Utility.GetScript("openWindowParam.js",paramList);
			Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
						
		}
		
		protected void  dgBaseConvey_Edit(object sender, DataGridCommandEventArgs e)
		{		
			lblErrorMessage.Text = "";
			if((int)ViewState["ConvMode"] == (int)ScreenMode.Insert && (int) ViewState["ConvOperation"] == (int)Operation.Insert &&  dgBaseConveyance.EditItemIndex > 0)
			{
				m_sdsConveyance.ds.Tables[0].Rows.RemoveAt(dgBaseConveyance.EditItemIndex);		
				dgBaseConveyance.CurrentPageIndex = 0;
			}
			dgBaseConveyance.EditItemIndex = e.Item.ItemIndex;
			ViewState["ConvOperation"] = Operation.Update;
			BindConveyanceGrid();
		}

		protected void dgBaseConvey_ItemDataBound(object sender, DataGridItemEventArgs e)
		{
			if(e.Item.ItemIndex == -1)
				return;

			DataRow drSelected = m_sdsConveyance.ds.Tables[0].Rows[e.Item.ItemIndex];
			if (drSelected ==null)
				return;
			if(drSelected.RowState == DataRowState.Deleted)
				return;
			
			if ((int)ViewState["ConvOperation"] != (int)Operation.Insert && (!btnExecuteQuery.Enabled))
			{
				this.txtOZCode = (msTextBox)e.Item.FindControl("txtOZCode");
				this.txtDZCode = (msTextBox)e.Item.FindControl("txtDZCode");
				this.txtConveyance = (msTextBox)e.Item.FindControl("txtConveyance");			
				
				if(this.txtOZCode !=null)
				{
					this.txtOZCode.Text=drSelected[0].ToString();
					this.txtOZCode.Enabled=false;
				}
				
				if(this.txtDZCode !=null)
				{
					this.txtDZCode.Text=drSelected[1].ToString();
					this.txtDZCode.Enabled=false;
				}
				
				if(this.txtConveyance !=null)
				{
					this.txtConveyance.Text=drSelected[2].ToString();
					this.txtConveyance.Enabled=false;
				}
                
			}

			if ((int)ViewState["ConvOperation"]==(int)Operation.Update)
			{
				return;
			}

			if ((int)ViewState["ConvMode"] == (int)ScreenMode.Insert && (int)ViewState["ConvOperation"] == (int)Operation.Insert)
				e.Item.Cells[1].Enabled=false;

			if((int)ViewState["ConvMode"]==(int)ScreenMode.Insert)
				e.Item.Cells[1].Enabled = false;	
			else
			{
				e.Item.Cells[1].Enabled = true;
			}
			SetAgentIDServerStates();
			SetAgentNameServerStates();
			
		}

		protected void dgBaseConvey_Cancel(object sender, DataGridCommandEventArgs e)
		{
			lblErrorMessage.Text = "";
			int rowIndex = e.Item.ItemIndex;
			if ((int)ViewState["ConvMode"] == (int)ScreenMode.Insert && (int)ViewState["ConvOperation"] == (int)Operation.Insert)
			{
				Utility.EnableButton(ref btnInsert,ButtonType.Insert,true,m_moduleAccessRights);
				m_sdsConveyance.ds.Tables[0].Rows.RemoveAt(rowIndex);				
			}
			ViewState["ConvOperation"] = Operation.None;
			dgBaseConveyance.EditItemIndex = -1;
			BindConveyanceGrid();			
		}

		protected void dgBaseConvey_PageChange(object sender, DataGridPageChangedEventArgs e)
		{
			lblErrorMessage.Text = "";
			dgBaseConveyance.CurrentPageIndex = e.NewPageIndex;
			dgBaseConveyance.SelectedIndex = -1;
			dgBaseConveyance.EditItemIndex = -1;
			RetreiveSelectedPage();
		}
		
		protected void dgBaseConvey_Delete(object sender, DataGridCommandEventArgs e)
		{
//			if (btnExecuteQuery.Enabled)
//				return;

			if ((int)ViewState["ConvOperation"] == (int)Operation.Update)
			{
				dgBaseConveyance.EditItemIndex = -1;
			}
			BindConveyanceGrid();
			String strAgentId=(String)ViewState["AgentID"];
			if (strAgentId =="")
			{
				lblErrorMessage.Text="Agent Id cannot be null";
			}

			int rowIndex = e.Item.ItemIndex;
			m_sdsConveyance  = (SessionDS)Session["SESSION_DS1"];
			DataRow dr =m_sdsConveyance.ds.Tables[0].Rows[rowIndex];
			
			String strOZCode=(string) dr[0];
			String strDZCode=(string) dr[1];
			String strConveyance =(string) dr[2];

			if (strConveyance==null || strConveyance =="")
				return;
			if (strOZCode==null || strOZCode =="")
				return;
			if (strDZCode==null || strDZCode =="")
				return;
			
			try
			{
				// delete from table
				
				int iRowsDeleted = SysDataManager1.DeleteBaseConvey(m_strAppID, m_strEnterpriseID, strAgentId ,strOZCode, strDZCode, strConveyance);
				ArrayList paramValues = new ArrayList();
				paramValues.Add(iRowsDeleted.ToString());
				lblErrorMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"DLD_SUCCESSFULLY",utility.GetUserCulture(),paramValues);
			}
			catch(ApplicationException appException)
			{
				String strMsg = appException.Message;
				if(strMsg.IndexOf("FK") != -1)
					lblErrorMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"ERR_DEL_COST",utility.GetUserCulture());
				if(strMsg.ToLower().IndexOf("child record found") != -1 )
					lblErrorMessage.Text =  Utility.GetLanguageText(ResourceType.UserMessage,"CHILD_FOUND_CANNOT_DEL",utility.GetUserCulture());
				else
				{
					lblErrorMessage.Text = strMsg;
				}

				Logger.LogTraceError("BaseConveyance","dgBaseConvey_Delete","RBAC003",appException.Message.ToString());
				return;
			}

			if((int)ViewState["ConvMode"] == (int)ScreenMode.Insert)
				m_sdsConveyance.ds.Tables[0].Rows.RemoveAt(rowIndex);
			else
			{
				RetreiveSelectedPage();
			}
			ViewState["ConvOperation"] = Operation.None;
			//Make the row in non-edit Mode
			EditHRow(false);
			BindConveyanceGrid();
			Logger.LogDebugInfo("BaseConveyance","dgBaseConvey_Delete","INF004","updating data grid...");			
			
		}

		protected void dgBaseConvey_Update(object sender, DataGridCommandEventArgs e)
		{
			lblErrorMessage.Text = "";
			String strAgentId=dbCmbAgentId.Value.Trim();
			if (strAgentId=="")
			{
				lblErrorMessage.Text="Agent Id cannot be null";
				return;
			}
			if (btnExecuteQuery.Enabled)
			{
				return;
			}
			int iRowsAffected = 0;
			int rowIndex = e.Item.ItemIndex;
			GetChangedRow(e.Item, e.Item.ItemIndex);
			
		
			int iOperation = (int)ViewState["ConvOperation"];
			try
			{
				if (iOperation == (int)Operation.Insert)
				{
					iRowsAffected = SysDataManager1.InsertBaseConvey(m_sdsConveyance.ds, rowIndex, m_strAppID, m_strEnterpriseID,strAgentId);
					ArrayList paramValues = new ArrayList();
					paramValues.Add(iRowsAffected.ToString());
					lblErrorMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"ROWS_INS",utility.GetUserCulture(),paramValues);;
				}
				else
				{
					DataSet dsChangedRow = m_sdsConveyance.ds.GetChanges();
					iRowsAffected = SysDataManager1.UpdateBaseConvey(dsChangedRow,m_strAppID, m_strEnterpriseID,strAgentId);
					ArrayList paramValues = new ArrayList();
					paramValues.Add(iRowsAffected.ToString());
					lblErrorMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"ROWS_UPD",utility.GetUserCulture(),paramValues);;
					m_sdsConveyance.ds.Tables[0].Rows[e.Item.ItemIndex].AcceptChanges();
				}
			}
			catch(ApplicationException appException)
			{
				String strMsg = appException.Message;
				strMsg = appException.InnerException.Message;
				strMsg = appException.InnerException.InnerException.Message;
				if(strMsg.IndexOf("PRIMARY KEY") != -1)
					lblErrorMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"DUP_KEY_FOUND",utility.GetUserCulture());
				else if(strMsg.IndexOf("FOREIGN KEY") != -1 )
					lblErrorMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"MASTER_NOT_FOUND",utility.GetUserCulture());
				else if(strMsg.IndexOf("duplicate key") != -1 )	
					lblErrorMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"DUP_KEY_FOUND",utility.GetUserCulture());
				else if(strMsg.IndexOf("unique") != -1 )
					lblErrorMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"DUP_KEY_FOUND",utility.GetUserCulture());
				else
				{
					lblErrorMessage.Text=strMsg;
				}
				
				Logger.LogTraceError("BaseConveyanceRates","dgBaseConvey_Update","RBAC003",appException.Message.ToString());
				return;
			}

			if (iRowsAffected == 0)
			{
				return;
			}
			Utility.EnableButton(ref btnInsert,ButtonType.Insert,true,m_moduleAccessRights);
			ViewState["ConvOperation"] = Operation.None;
			dgBaseConveyance.EditItemIndex = -1;
			BindConveyanceGrid();
			Logger.LogDebugInfo("BaseConveyanceRates","dgBaseConvey_Update","INF004","updating data grid...");			
			
		}
		
		#endregion

		private void InsertAgent(int iRowIndex)
		{
			m_sdsAgent = AgentProfileMgrDAL.GetEmptyAgentDS(1);
			DataRow drEach =  m_sdsAgent.ds.Tables[0].Rows[0];	
			drEach["agentid"]	= dbCmbAgentId.Value; 
			drEach["agent_name"]= dbCmbAgentName.Value;
			Session["SESSION_DS3"]= m_sdsAgent;
		}
		
		private void GetAgentRecSet()
		{
			int iStartIndex = Convert.ToInt32(ViewState["currentSet"]) * m_iSetSize;
			DataSet dsQuery = (DataSet)ViewState["QUERY_DS"];
			m_sdsAgent = AgentProfileMgrDAL.GetAgentDS(m_strAppID,m_strEnterpriseID,iStartIndex,m_iSetSize,dsQuery);
			Session["SESSION_DS3"] = m_sdsAgent;
			ViewState["QryRes"] =  m_sdsAgent.QueryResultMaxSize; 			
			decimal pgCnt = (m_sdsAgent.QueryResultMaxSize - 1)/m_iSetSize;
			if(pgCnt < Convert.ToDecimal(ViewState["currentSet"]))
			{
				ViewState["currentSet"] = System.Convert.ToInt32(pgCnt);
			}
		}
		
		private void btnQuery_Click(object sender, System.EventArgs e)
		{
			lblErrorMessage.Text = "";
			dgBaseConveyance.CurrentPageIndex = 0;
			QueryMode();
		}

		private void btnInsert_Click(object sender, System.EventArgs e)
		{			
			bool bCanAddRow=true;
			
			lblErrorMessage.Text = "";
			if (btnExecuteQuery.Enabled == true)
			{
				btnExecuteQuery.Enabled = false;
			}
						
			if((int)ViewState["ConvMode"] != (int)ScreenMode.Query)
			{
				for(int i=0;i<m_sdsConveyance.ds.Tables[0].Rows.Count;i++)
				{
					DataRow dr=m_sdsConveyance.ds.Tables[0].Rows[i];
					if(dr[0].ToString()=="" && dr[1].ToString() =="" && dr[2].ToString()=="")
						bCanAddRow=false;
				}
			}

			if((int)ViewState["ConvMode"] != (int)ScreenMode.Insert || m_sdsConveyance.ds.Tables[0].Rows.Count >= dgBaseConveyance.PageSize)
			{
				m_sdsConveyance = SysDataManager1.GetEmptyBaseConvey();
				dgBaseConveyance.EditItemIndex = m_sdsConveyance.ds.Tables[0].Rows.Count - 1;
				dgBaseConveyance.CurrentPageIndex = 0;								
			}
			if (bCanAddRow==true)
			{
				AddRow();
			}
			
			btnInsert.Enabled=false;
		
			EditHRow(false);
			ViewState["ConvMode"] = ScreenMode.Insert;
			ViewState["ConvOperation"] = Operation.Insert;
			dgBaseConveyance.Columns[0].Visible=true;
			dgBaseConveyance.Columns[1].Visible=true;
			dgBaseConveyance.EditItemIndex = m_sdsConveyance.ds.Tables[0].Rows.Count - 1;
			dgBaseConveyance.CurrentPageIndex = 0;
			BindConveyanceGrid();
			DisplayCurrentPage();
			getPageControls(Page);
		}
		
		private void btnExecuteQuery_Click(object sender, System.EventArgs e)
		{
			lblErrorMessage.Text = "";
			btnExecuteQuery.Enabled = false;
			InsertAgent(0);
			ViewState["QUERY_DS"] = m_sdsAgent.ds;
			ViewState["currentPage"] = 0;
			ViewState["currentSet"] = 0;
			GetAgentRecSet();	
			
			if ((decimal)ViewState["QryRes"]!=0)
			{
				DisplayCurrentPage();
				//Conveyance Rates				
				GetChangedRow(dgBaseConveyance.Items[0],0);
				ViewState["QUERY_DSC"] = m_sdsConveyance.ds;
				dgBaseConveyance.CurrentPageIndex = 0;
				RetreiveSelectedPage();	
				if(m_sdsConveyance.ds.Tables[0].Rows.Count==0)
				{
					lblErrorMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"NO_RECORD_FOUND",utility.GetUserCulture());
					dgBaseConveyance.EditItemIndex = -1; 
					return;
				}
				//set all records to non-edit mode after execute query
				EditHRow(false);
				dgBaseConveyance.Columns[0].Visible=true;
				dgBaseConveyance.Columns[1].Visible=true;
				
				ViewState["ConvMode"] = ScreenMode.ExecuteQuery;
				ViewState["AgentMode"] = ScreenMode.ExecuteQuery;
				EnableNavigationButtons(true,true,true,true);
				dbCmbAgentId.Enabled=false;
				dbCmbAgentName.Enabled=false;
			}
			else
			{
				lblErrorMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"NO_RECORD_FOUND",utility.GetUserCulture());
			}
			
		}

		private void DisplayCurrentPage()
		{
			if (m_sdsAgent.ds.Tables[0].Rows.Count<=0)
				return;

			DataRow drCurrent = m_sdsAgent.ds.Tables[0].Rows[Convert.ToInt32(ViewState["currentPage"])];
						
			if(!drCurrent["agentid"].GetType().Equals(System.Type.GetType("System.DBNull")))
			{
				dbCmbAgentId.Text		= drCurrent["agentid"].ToString();
				dbCmbAgentId.Value		= drCurrent["agentid"].ToString();
				ViewState["AgentID"]	= drCurrent["agentid"].ToString();
			}
			if(!drCurrent["agent_name"].GetType().Equals(System.Type.GetType("System.DBNull")))
			{
				dbCmbAgentName.Text		= drCurrent["agent_name"].ToString();
				dbCmbAgentName.Value	= drCurrent["agent_name"].ToString();
			}
		}
		
		private void EnableAgentCombos()
		{
			if((!btnExecuteQuery.Enabled))
			{
				dbCmbAgentId.Enabled=false;
				dbCmbAgentName.Enabled=false;
			}
			else
			{
				dbCmbAgentId.Enabled=true;
				dbCmbAgentName.Enabled=true;
			}
		}
		
		private void btnGoToLastPage_Click(object sender, System.EventArgs e)
		{
			ViewState["currentSet"] = Convert.ToInt32((m_sdsAgent.QueryResultMaxSize - 1))/m_iSetSize;	
			GetAgentRecSet();
			ViewState["currentPage"] = m_sdsAgent.DataSetRecSize - 1;
			DisplayCurrentPage();
			RetreiveSelectedPage();
			EnableNavigationButtons(true,true,false,false);
			if (!btnInsert.Enabled)
			{
				Utility.EnableButton(ref btnInsert,ButtonType.Insert,true,m_moduleAccessRights);
			}

			SetAgentIDServerStates();
			SetAgentNameServerStates();
		}

		private void btnPreviousPage_Click(object sender, System.EventArgs e)
		{
			int iCurrentPage = Convert.ToInt32(ViewState["currentPage"]);

			if(Convert.ToInt32(ViewState["currentPage"])  > 0 )
			{
				ViewState["currentPage"] = Convert.ToInt32(ViewState["currentPage"]) - 1;
				DisplayCurrentPage();	
				RetreiveSelectedPage();
			}
			else
			{
				if( (Convert.ToInt32(ViewState["currentSet"]) - 1) < 0)
				{
					EnableNavigationButtons(false,false,true,true);
					return;
				}
				
				ViewState["currentSet"] = Convert.ToInt32(ViewState["currentSet"]) - 1;	
				GetAgentRecSet();
				ViewState["currentPage"] = m_sdsAgent.DataSetRecSize - 1;
				DisplayCurrentPage();
				RetreiveSelectedPage();
			}
			EnableNavigationButtons(true,true,true,true);
			if (!btnInsert.Enabled)
			{
				Utility.EnableButton(ref btnInsert,ButtonType.Insert,true,m_moduleAccessRights);
			}
			
			SetAgentIDServerStates();
			SetAgentNameServerStates();
		}

		private void btnNextPage_Click(object sender, System.EventArgs e)
		{			
			int iCurrentPage = Convert.ToInt32(ViewState["currentPage"]);
			if(Convert.ToInt32(ViewState["currentPage"])  < (m_sdsAgent.DataSetRecSize - 1) )
			{
				ViewState["currentPage"] = Convert.ToInt32(ViewState["currentPage"]) + 1;
				DisplayCurrentPage();
				RetreiveSelectedPage();
			}
			else
			{
				decimal iTotalRec = (Convert.ToInt32(ViewState["currentSet"]) * m_iSetSize) + m_sdsAgent.DataSetRecSize;
				if( iTotalRec ==  m_sdsAgent.QueryResultMaxSize)
				{
					EnableNavigationButtons(true,true,false,false);
					return;
				}
				
				ViewState["currentSet"] = Convert.ToInt32(ViewState["currentSet"]) + 1;	
				ViewState["currentPage"] = 0;
				DisplayCurrentPage();
				RetreiveSelectedPage();
			}
			EnableNavigationButtons(true,true,true,true);
			if (!btnInsert.Enabled)
			{
				Utility.EnableButton(ref btnInsert,ButtonType.Insert,true,m_moduleAccessRights);
			}

			SetAgentIDServerStates();
			SetAgentNameServerStates();
		}

		private void btnGoToFirstPage_Click(object sender, System.EventArgs e)
		{
			ViewState["currentSet"] = 0;	
			GetAgentRecSet();
			ViewState["currentPage"] = 0;
			DisplayCurrentPage();
			RetreiveSelectedPage();
			EnableNavigationButtons(false,false,true,true);		
			if (!btnInsert.Enabled)
			{
				Utility.EnableButton(ref btnInsert,ButtonType.Insert,true,m_moduleAccessRights);
			}
			
			SetAgentIDServerStates();
			SetAgentNameServerStates();
		}
						
		private void EnableNavigationButtons(bool bMoveFirst, bool bMoveNext,bool bEnableMovePrevious, bool bMoveLast)
		{
			btnGoToFirstPage.Enabled	= bMoveFirst;
			btnPreviousPage.Enabled		= bMoveNext;
			btnNextPage.Enabled			= bEnableMovePrevious;
			btnGoToLastPage.Enabled		= bMoveLast;
		}


		private void dbCmbAgentId_SelectedItemChanged(object sender, System.EventArgs e)
		{
			DbComboRegKey();
			SetAgentNameServerStates();			
		}

		private void ClearDbCombos()
		{
			if(dgBaseConveyance.EditItemIndex == -1)
				return;

			if(dgBaseConveyance.Items.Count==0)
				return;

			this.txtOZCode = (msTextBox)dgBaseConveyance.Items[dgBaseConveyance.EditItemIndex].FindControl("txtOZCode");
			this.txtDZCode = (msTextBox)dgBaseConveyance.Items[dgBaseConveyance.EditItemIndex].FindControl("txtDZCode");
			this.txtConveyance = (msTextBox)dgBaseConveyance.Items[dgBaseConveyance.EditItemIndex].FindControl("txtConveyance");

			if(this.txtOZCode != null && this.txtDZCode !=null && this.txtConveyance!=null)
			{
				this.txtOZCode.Text="";
				this.txtDZCode.Text="";
				this.txtConveyance.Text="";
			}			
		}

		private void dbCmbAgentName_SelectedItemChanged(object sender, System.EventArgs e)
		{
			DbComboRegKey();
			SetAgentIDServerStates();
		}

		private void DbComboRegKey()
		{
			dbCmbAgentId.RegistrationKey=sDbComboRegKey;//System.Configuration.ConfigurationSettings.AppSettings["DbComboRegKey"];
			dbCmbAgentName.RegistrationKey=sDbComboRegKey;//System.Configuration.ConfigurationSettings.AppSettings["DbComboRegKey"];
		}
			
	}
}
