<%@ Import Namespace="com.common.util" %>
<%@ Import Namespace="Cambro.Web.DbCombo" %>
<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<%@ Register TagPrefix="DbCombo" Namespace="Cambro.Web.DbCombo" Assembly="Cambro.Web.DbCombo" %>
<%@ Page language="c#" Codebehind="PODEXRptQuery.aspx.cs" AutoEventWireup="false" Inherits="com.ties.PODEXRptQuery" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Daily PODEX Report</title>
		<LINK href="css/Styles.css" type="text/css" rel="stylesheet" name="stylesheet">
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<!--#INCLUDE FILE="msFormValidations.inc"-->
		<script language="javascript" src="Scripts/settingScrollPosition.js"></script>
	</HEAD>
	<body onclick="GetScrollPosition();" onload="SetScrollPosition();" onunload="window.top.displayBanner.fnCloseAll(0);"
		MS_POSITIONING="GridLayout">
		<form id="PODEXRptQuery" method="post" runat="server">
			<asp:button id="btnQuery" style="Z-INDEX: 100; LEFT: 20px; POSITION: absolute; TOP: 40px" runat="server"
				Width="64px" CssClass="queryButton" Text="Query"></asp:button><asp:label id="lblErrorMessage" style="Z-INDEX: 105; LEFT: 24px; POSITION: absolute; TOP: 69px"
				runat="server" Width="700px" CssClass="errorMsgColor" Height="17px">Place holder for err msg</asp:label>&nbsp;
			<asp:button id="btnExecuteQuery" style="Z-INDEX: 101; LEFT: 90px; POSITION: absolute; TOP: 40px"
				runat="server" Width="123px" CssClass="queryButton" Text="Execute Query"></asp:button>
			<TABLE id="tblInternal" style="Z-INDEX: 104; LEFT: 21px; WIDTH: 800px; POSITION: absolute; TOP: 90px"
				width="800" border="0" runat="server">
				<TR>
					<TD style="WIDTH: 401px; HEIGHT: 150px" vAlign="top">
						<fieldset style="WIDTH: 390px; HEIGHT: 129px"><LEGEND><asp:label id="lblDates" runat="server" Width="20px" CssClass="tableHeadingFieldset" Font-Bold="True">Dates</asp:label></LEGEND>
							<TABLE id="tblDates" style="WIDTH: 360px; HEIGHT: 91px" cellSpacing="0" cellPadding="0"
								align="left" border="0" runat="server">
								<TR>
									<TD style="HEIGHT: 22px"></TD>
									<TD style="HEIGHT: 22px" colSpan="3">&nbsp;
										<asp:radiobutton id="rbBookingDate" runat="server" Width="115px" CssClass="tableRadioButton" Text="Booking Date"
											Height="22px" Checked="True" GroupName="QueryByDate" AutoPostBack="True"></asp:radiobutton>&nbsp;&nbsp;&nbsp;
										<asp:radiobutton id="rbEstDelDate" runat="server" Width="180px" CssClass="tableRadioButton" Text="Estimate Delivery Date"
											Height="22px" GroupName="QueryByDate" AutoPostBack="True"></asp:radiobutton>&nbsp;
										<asp:radiobutton id="rbPickUpDate" runat="server" Width="128px" CssClass="tableRadioButton" Text="Actual Pickup Date"
											Height="22px" GroupName="QueryByDate" AutoPostBack="True"></asp:radiobutton>&nbsp;
										<asp:radiobutton id="rbPODDate" runat="server" Width="180px" CssClass="tableRadioButton" Text="Actual POD Date"
											Height="22px" GroupName="QueryByDate" AutoPostBack="True"></asp:radiobutton></TD>
									<TD style="HEIGHT: 22px"></TD>
								</TR>
								<TR>
									<TD></TD>
									<TD colSpan="3">&nbsp;
										<asp:radiobutton id="rbMonth" runat="server" Width="73px" CssClass="tableRadioButton" Text="Month"
											Height="21px" Checked="True" GroupName="EnterDate" AutoPostBack="True"></asp:radiobutton>&nbsp;
										<asp:dropdownlist id="ddMonth" runat="server" Width="88px" CssClass="textField" Height="19px"></asp:dropdownlist>&nbsp;
										<asp:label id="lblYear" runat="server" Width="30px" CssClass="tableLabel" Height="22px">Year</asp:label>&nbsp;
										<cc1:mstextbox id="txtYear" runat="server" Width="83" CssClass="textField" NumberPrecision="4"
											NumberMinValue="1" NumberMaxValue="2999" TextMaskType="msNumeric" MaxLength="5"></cc1:mstextbox></TD>
									<TD></TD>
								</TR>
								<TR>
									<TD style="HEIGHT: 20px"></TD>
									<TD style="HEIGHT: 20px" colSpan="3">&nbsp;
										<asp:radiobutton id="rbPeriod" runat="server" Width="62px" CssClass="tableRadioButton" Text="Period"
											GroupName="EnterDate" AutoPostBack="True"></asp:radiobutton>&nbsp;&nbsp;&nbsp;
										<cc1:mstextbox id="txtPeriod" runat="server" Width="82" CssClass="textField" TextMaskType="msDate"
											MaxLength="10" TextMaskString="99/99/9999"></cc1:mstextbox>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
										&nbsp;
										<cc1:mstextbox id="txtTo" runat="server" Width="83" CssClass="textField" TextMaskType="msDate"
											MaxLength="10" TextMaskString="99/99/9999"></cc1:mstextbox></TD>
									<TD style="HEIGHT: 20px"></TD>
								</TR>
								<TR>
									<TD></TD>
									<TD colSpan="3">&nbsp;
										<asp:radiobutton id="rbDate" runat="server" Width="68px" CssClass="tableRadioButton" Text="Date"
											Height="22px" GroupName="EnterDate" AutoPostBack="True"></asp:radiobutton>&nbsp;&nbsp;
										<cc1:mstextbox id="txtDate" runat="server" Width="82" CssClass="textField" TextMaskType="msDate"
											MaxLength="10" TextMaskString="99/99/9999"></cc1:mstextbox></TD>
									<TD></TD>
								</TR>
							</TABLE>
						</fieldset>
					</TD>
					<TD style="HEIGHT: 150px" vAlign="top"><FONT face="Tahoma"></FONT>
						<fieldset style="WIDTH: 320px; HEIGHT: 120px"><LEGEND><asp:label id="lblPayerType" runat="server" Width="79px" CssClass="tableHeadingFieldset" Font-Bold="True">Payer Type</asp:label></LEGEND>
							<TABLE id="tblPayerType" style="WIDTH: 300px; HEIGHT: 73px" cellSpacing="0" cellPadding="0"
								align="left" border="0" runat="server">
								<TR>
									<TD></TD>
									<TD colSpan="2" height="1">&nbsp;</TD>
									<TD></TD>
									<TD></TD>
								</TR>
								<TR>
									<TD></TD>
									<TD class="tableLabel" vAlign="top" colSpan="2">&nbsp;
										<asp:label id="Label4" runat="server" Width="79px" CssClass="tableLabel" Height="22px">Payer Type</asp:label></TD>
									<TD><asp:listbox id="lsbCustType" runat="server" Width="137px" Height="61px" SelectionMode="Multiple"></asp:listbox></TD>
									<TD></TD>
								</TR>
								<TR height="33">
									<TD bgColor="blue"></TD>
									<TD class="tableLabel" vAlign="top" colSpan="2">&nbsp;
										<asp:label id="lblPayerCode" runat="server" Width="79px" CssClass="tableLabel" Height="22px">Payer Code</asp:label>&nbsp;&nbsp;</TD>
									<TD><asp:textbox id="txtPayerCode" runat="server" Width="139px" CssClass="textField"></asp:textbox>&nbsp;
										<asp:button id="btnPayerCode" runat="server" CssClass="searchButton" Text="..." CausesValidation="False"></asp:button></TD>
								</TR>
							</TABLE>
						</fieldset>
						&nbsp;
					</TD>
				</TR>
				<TR>
					<TD style="WIDTH: 401px" vAlign="top">
						<fieldset style="WIDTH: 390px; HEIGHT: 140px"><legend><asp:label id="lblRouteType" runat="server" Width="124px" CssClass="tableHeadingFieldset" Font-Bold="True">Route / DC Selection</asp:label></legend>
							<TABLE id="tblRouteType" style="WIDTH: 374px; HEIGHT: 113px" cellSpacing="0" cellPadding="0"
								align="left" border="0" runat="server">
								<TR>
									<TD class="tableLabel" colSpan="2">&nbsp;
										<asp:radiobutton id="rbLongRoute" runat="server" Width="106px" CssClass="tableRadioButton" Text="Linehaul"
											Height="22px" Checked="True" GroupName="QueryByRoute" AutoPostBack="True"></asp:radiobutton><asp:radiobutton id="rbShortRoute" runat="server" Width="131px" CssClass="tableRadioButton" Text="Delivery Route"
											Height="22px" GroupName="QueryByRoute" AutoPostBack="True"></asp:radiobutton><asp:radiobutton id="rbAirRoute" runat="server" Width="120px" CssClass="tableRadioButton" Text="Air Route"
											Height="22px" GroupName="QueryByRoute" AutoPostBack="True"></asp:radiobutton>&nbsp;</TD>
								</TR>
								<tr>
									<TD class="tableLabel" width="20%">&nbsp;<asp:label id="lblRouteCode" runat="server" Width="111px" CssClass="tableLabel" Height="22px">Route Code</asp:label></TD>
									<td><DBCOMBO:DBCOMBO id="DbComboPathCode" tabIndex="14" Width="25px" CssClass="tableLabel" AutoPostBack="True"
											ServerMethod="DbComboPathCodeSelect" ShowDbComboLink="False" TextUpLevelSearchButton="v" RegistrationKey=" "
											TextBoxColumns="18" Runat="server"></DBCOMBO:DBCOMBO></td>
								</tr>
								<TR>
									<TD class="tableLabel">&nbsp;<asp:label id="Label2" runat="server" Width="154px" CssClass="tableLabel" Height="22px">Origin Distribution Center</asp:label></TD>
									<TD><DBCOMBO:DBCOMBO id="DbComboOriginDC" tabIndex="14" Width="25px" CssClass="tableLabel" AutoPostBack="True"
											ServerMethod="DbComboDistributionCenterSelect" ShowDbComboLink="False" TextUpLevelSearchButton="v"
											RegistrationKey=" " TextBoxColumns="18" Runat="server"></DBCOMBO:DBCOMBO></TD>
								</TR>
								<TR>
									<TD class="tableLabel">&nbsp;<asp:label id="Label3" runat="server" Width="181px" CssClass="tableLabel" Height="22px">Destination Distribution Center</asp:label></TD>
									<TD><DBCOMBO:DBCOMBO id="DbComboDestinationDC" tabIndex="14" Width="25px" CssClass="tableLabel" AutoPostBack="True"
											ServerMethod="DbComboDistributionCenterSelect" ShowDbComboLink="False" TextUpLevelSearchButton="v"
											RegistrationKey=" " TextBoxColumns="18" Runat="server"></DBCOMBO:DBCOMBO></TD>
								</TR>
							</TABLE>
						</fieldset>
					</TD>
					<TD vAlign="top">
						<FIELDSET style="WIDTH: 320px; HEIGHT: 80px"><LEGEND><asp:label id="Label7" runat="server" Width="79px" CssClass="tableHeadingFieldset" Font-Bold="True">Destination</asp:label></LEGEND>
							<TABLE id="Table5" style="WIDTH: 300px; HEIGHT: 57px" cellSpacing="0" cellPadding="0" align="left"
								border="0" runat="server">
								<TR height="37">
									<TD style="HEIGHT: 33px"></TD>
									<TD class="tableLabel" style="WIDTH: 360px; HEIGHT: 33px" colSpan="3">&nbsp;
										<asp:label id="Label6" runat="server" Width="84px" CssClass="tableLabel" Height="22px">Postal Code</asp:label>&nbsp;&nbsp;&nbsp;
										<cc1:mstextbox id="txtZipCode" runat="server" Width="139px" CssClass="textField" TextMaskType="msUpperAlfaNumericWithUnderscore"
											MaxLength="10"></cc1:mstextbox>&nbsp;&nbsp;
										<asp:button id="btnZipCode" runat="server" CssClass="searchButton" Text="..." CausesValidation="False"></asp:button></TD>
									<TD style="HEIGHT: 33px">&nbsp;</TD>
								</TR>
								<TR>
									<TD style="HEIGHT: 33px"></TD>
									<TD class="tableLabel" style="WIDTH: 360px; HEIGHT: 33px" colSpan="3">&nbsp;
										<asp:label id="Label5" runat="server" Width="100px" CssClass="tableLabel" Height="22px"> Province</asp:label><cc1:mstextbox id="txtStateCode" runat="server" Width="139" CssClass="textField" Height="22" TextMaskType="msUpperAlfaNumericWithUnderscore"
											MaxLength="10"></cc1:mstextbox>&nbsp;&nbsp;
										<asp:button id="btnStateCode" runat="server" CssClass="searchButton" Text="..." CausesValidation="False"></asp:button></TD>
									<TD style="HEIGHT: 33px">&nbsp;</TD>
								</TR>
							</TABLE>
						</FIELDSET>
						<FIELDSET style="WIDTH: 320px; HEIGHT: 50px"><LEGEND><asp:label id="Label9" runat="server" Width="80px" CssClass="tableHeadingFieldset" Font-Bold="True">Service Type</asp:label></LEGEND>
							<TABLE id="Table3" style="WIDTH: 312px" cellSpacing="0" cellPadding="0" align="left" border="0"
								runat="server">
								<TR>
									<TD>&nbsp;
										<asp:label id="Label10" runat="server" Width="100px" CssClass="tableLabel">Service Code</asp:label></TD>
									<TD class="tableLabel" style="WIDTH: 197px" colSpan="3"><dbcombo:dbcombo id="dbCmbServiceCode" tabIndex="1" runat="server" Width="80px" AutoPostBack="True"
											ServerMethod="ServiceCodeServerMethod" ShowDbComboLink="False" TextUpLevelSearchButton="v" TextBoxColumns="4"></dbcombo:dbcombo></TD>
								</TR>
							</TABLE>
						</FIELDSET>
					</TD>
				</TR>
				<TR>
					<TD style="WIDTH: 401px" vAlign="top">
						<FIELDSET style="WIDTH: 392px; HEIGHT: 50px"><LEGEND>
								<asp:label id="Label11" runat="server" CssClass="tableHeadingFieldset" Width="96px" Font-Bold="True">Exception Type</asp:label></LEGEND>
							<TABLE id="Table1" style="WIDTH: 312px" cellSpacing="0" cellPadding="0" align="left" border="0"
								runat="server">
								<TR>
									<TD>&nbsp;
										<asp:label id="Label8" runat="server" CssClass="tableLabel" Width="100px">Exception Type</asp:label></TD>
									<TD class="tableLabel" style="WIDTH: 197px" colSpan="3">
										<dbcombo:dbcombo id="dbCmbExceptionType" tabIndex="1" runat="server" Width="80px" AutoPostBack="True"
											TextBoxColumns="4" TextUpLevelSearchButton="v" ShowDbComboLink="False" ServerMethod="ExceptionTypeServerMethod"></dbcombo:dbcombo></TD>
								</TR>
							</TABLE>
						</FIELDSET>
					</TD>
					<TD style="WIDTH: 400px" vAlign="top">&nbsp;
					</TD>
				</TR>
			</TABLE>
			<asp:label id="Label1" style="Z-INDEX: 102; LEFT: 20px; POSITION: absolute; TOP: 4px" runat="server"
				Width="365px" CssClass="maintitleSize" Height="27px">Daily PODEX Report</asp:label></TR></TABLE></TR></TABLE><input 
style="Z-INDEX: 106; LEFT: 222px; POSITION: absolute; TOP: 35px" type=hidden 
value="<%=strScrollPosition%>" name=ScrollPosition>
		</form>
	</body>
</HTML>
