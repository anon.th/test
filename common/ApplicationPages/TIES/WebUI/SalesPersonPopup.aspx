<%@ Page language="c#" Codebehind="SalesPersonPopup.aspx.cs" AutoEventWireup="false" Inherits="TIES.WebUI.SalesPersonPopup" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Sales Man Popup</title>
		<LINK href="css/Styles.css" type="text/css" rel="stylesheet" name="stylesheet">
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<script language="javascript" src="Scripts/settingScrollPosition.js"></script>
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="SalesManDetails" method="post" runat="server">
			<asp:Label id="SalesErrMsg" style="Z-INDEX: 106; LEFT: 18px; POSITION: absolute; TOP: 79px" runat="server" Width="598px" CssClass="errorMsgColor" Height="16px"></asp:Label>
			<asp:Label id="lblSalesManName" style="Z-INDEX: 109; LEFT: 162px; POSITION: absolute; TOP: 17px" runat="server" CssClass="tablelabel">Salesman Name</asp:Label>
			<asp:button id="btnClose" style="Z-INDEX: 108; LEFT: 456px; POSITION: absolute; TOP: 48px" runat="server" Height="21" CssClass="buttonProp" Width="85" CausesValidation="False" Text="Close"></asp:button>
			<asp:button id="btnSearch" style="Z-INDEX: 104; LEFT: 370px; POSITION: absolute; TOP: 48px" runat="server" Width="85" CssClass="buttonProp" Height="21" Text="Search" CausesValidation="False"></asp:button><asp:textbox id="txtSalesManID" style="Z-INDEX: 103; LEFT: 18px; POSITION: absolute; TOP: 45px" runat="server" CssClass="textField" Width="116px" Height="21"></asp:textbox><asp:label id="lblSalesManID" style="Z-INDEX: 101; LEFT: 18px; POSITION: absolute; TOP: 15px" runat="server" CssClass="tableLabel" Width="100px" Height="16">Salesman ID</asp:label><asp:button id="btnRefresh" style="DISPLAY: none; Z-INDEX: 105; LEFT: 542px; POSITION: absolute; TOP: 57px" runat="server" Width="50px" Text="HIDDEN" Height="23px"></asp:button><asp:textbox id="txtSalesManName" style="Z-INDEX: 102; LEFT: 159px; POSITION: absolute; TOP: 45px" runat="server" CssClass="textField" Width="116px" Height="21"></asp:textbox>
			<asp:datagrid id="dgSalesman" runat="server" AllowCustomPaging="True" OnPageIndexChanged="Paging" PageSize="5" AllowPaging="True" Width="522px" AutoGenerateColumns="False" BorderColor="#CCCCCC" BackColor="White" CellPadding="3" BorderWidth="1px" BorderStyle="None" CssClass="gridHeading" Height="121px" style="Z-INDEX: 107; LEFT: 17px; POSITION: absolute; TOP: 105px">
				<SelectedItemStyle Font-Bold="True" ForeColor="White" BackColor="#669999"></SelectedItemStyle>
				<ItemStyle CssClass="drilldownField"></ItemStyle>
				<HeaderStyle Width="10%" CssClass="gridHeading"></HeaderStyle>
				<FooterStyle ForeColor="#000066" BackColor="White"></FooterStyle>
				<Columns>
					<asp:BoundColumn DataField="SalesmanID" HeaderText="Salesman ID"></asp:BoundColumn>
					<asp:BoundColumn DataField="Salesman_Name" HeaderText="Name"></asp:BoundColumn>
					<asp:BoundColumn DataField="Designation" HeaderText="Designation"></asp:BoundColumn>
					<asp:ButtonColumn Text="Select" CommandName="Select"></asp:ButtonColumn>
				</Columns>
				<PagerStyle HorizontalAlign="Left" ForeColor="#000066" BackColor="White" Mode="NumericPages"></PagerStyle>
			</asp:datagrid>
		</form>
	</body>
</HTML>
