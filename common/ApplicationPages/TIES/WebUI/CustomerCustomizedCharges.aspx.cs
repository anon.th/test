using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.common.util;
using com.ties.DAL;
using com.common.classes;
using com.ties.classes;
using com.common.applicationpages;

namespace com.ties
{
	/// <summary>
	/// Summary description for CustomerCustomizedCharges.
	/// </summary>
	public class CustomerCustomizedCharges : BasePage
	{
		protected System.Web.UI.WebControls.Label lblTitle;
		protected System.Web.UI.WebControls.Button btnQuery;
		protected System.Web.UI.WebControls.Button btnExecQry;
		protected System.Web.UI.WebControls.Button btnInsertVAS;
		protected System.Web.UI.WebControls.Button btnGoToLastPage;
		protected System.Web.UI.WebControls.Button btnNextPage;
		protected System.Web.UI.WebControls.TextBox txtCountRec;
		protected System.Web.UI.WebControls.Button btnPreviousPage;
		protected System.Web.UI.WebControls.Button btnGoToFirstPage;
		protected System.Web.UI.WebControls.Label lblAgentName;
		protected Cambro.Web.DbCombo.DbCombo dbCmbAgentId;
		protected Cambro.Web.DbCombo.DbCombo dbCmbAgentName;
		protected System.Web.UI.WebControls.Label lblAgentCode;
		SessionDS m_sdsAgent = null;
		SessionDS m_sdsCustomizedCode = null;
		//private static int m_SetSize=10;
		protected System.Web.UI.WebControls.DataGrid dgCustomizedCode;
		protected System.Web.UI.WebControls.Label lblServiceMessage;
		protected System.Web.UI.WebControls.RequiredFieldValidator RequiredFieldValidator1;
		protected System.Web.UI.WebControls.ValidationSummary ValidationSummary1;
		static private int m_iSetSize = 4;
	
		private void Page_Load(object sender, System.EventArgs e)
		{
			dbCmbAgentId.RegistrationKey=System.Configuration.ConfigurationSettings.AppSettings["DbComboRegKey"];
			dbCmbAgentName.RegistrationKey=System.Configuration.ConfigurationSettings.AppSettings["DbComboRegKey"];		
			if(!Page.IsPostBack)
			{
				ViewState["AgentOperation"] = Operation.None;
				ViewState["AgentMode"]		= ScreenMode.None;
				ViewState["MovePrevious"]	= false;
				ViewState["MoveNext"]		= false;
				ViewState["MoveFirst"]		= false;
				ViewState["MoveLast"]		= false;
				ResetScreenForQuery();				
				ResetDetailsCustomizedGrid();
			}
			else
			{
				m_sdsAgent					= (SessionDS)Session["SESSION_DS3"];
				m_sdsCustomizedCode			= (SessionDS)Session["SESSION_DS1"];
				
			}
			SetAgentIDServerStates();
			SetAgentNameServerStates();
		}
		

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.dgCustomizedCode.SelectedIndexChanged += new System.EventHandler(this.dgCustomizedCode_SelectedIndexChanged);
			this.btnGoToFirstPage.Click += new System.EventHandler(this.btnGoToFirstPage_Click);
			this.btnPreviousPage.Click += new System.EventHandler(this.btnPreviousPage_Click);
			this.btnNextPage.Click += new System.EventHandler(this.btnNextPage_Click);
			this.btnGoToLastPage.Click += new System.EventHandler(this.btnGoToLastPage_Click);
			this.btnInsertVAS.Click += new System.EventHandler(this.btnInsertVAS_Click);
			this.btnExecQry.Click += new System.EventHandler(this.btnExecQry_Click);
			this.btnQuery.Click += new System.EventHandler(this.btnQuery_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		#region Customer details
		private void ResetScreenForQuery()
		{		
			btnExecQry.Enabled				= true;
			m_sdsAgent						= SysDataMgrDAL.GetCustomerDS(1);
			Session["SESSION_DS3"]			= m_sdsAgent;		
			ViewState["AgentOperation"]		= Operation.None;
			ViewState["AgentMode"]			= ScreenMode.Query;
			ViewState["CustomizedOperation"]= Operation.None;
			ViewState["CustomizedMode"]		= ScreenMode.Query;
			EnableNavigationButtons(false,false,false,false);
			btnExecQry.Enabled = true;
			dbCmbAgentId.Text		="";
			dbCmbAgentName.Text		="";
			dbCmbAgentId.Value		="";
			dbCmbAgentName.Value	="";
			lblServiceMessage.Text	="";
			EnablingDBCombo(true);
			dbCmbAgentId.EnableViewState =false;
			EnableNavigationButtons(false,false,false,false);		
		}
		[Cambro.Web.DbCombo.ResultsMethod(true)]
		public static object AgentNameServerMethod(Cambro.Web.DbCombo.ServerMethodArgs args)
		{
			Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings,HttpContext.Current.Session);
			String strAppID = util.GetAppID();
			String strEnterpriseID = util.GetEnterpriseID();
	
			String strAgentID= "";

			if(args.ServerState != null && args.ServerState.Authenticate("kdfsjgnskajh[apq2-4uhg465654435879mnbfdm cxs;q4"))
			{				
				if (args.ServerState["strAgentID"] != null && args.ServerState["strAgentID"].ToString().Length > 0 )
				{
					strAgentID = (args.ServerState["strAgentID"].ToString());
				}
			}	

			DataSet dataset = DbComboDAL.PayerNameQuery(strAppID,strEnterpriseID,args, "C", strAgentID);	
			return dataset;                
		}
		[Cambro.Web.DbCombo.ResultsMethodAttribute(true)]		
		public static object AgentIdServerMethod(Cambro.Web.DbCombo.ServerMethodArgs args)
		{
			Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings,HttpContext.Current.Session);
			String strAppID = util.GetAppID();
			String strEnterpriseID = util.GetEnterpriseID();
						
			String strAgentName="";
			if(args.ServerState != null && args.ServerState.Authenticate("kdfsjgnskajh[apq2-4uhg465654435879mnbfdm cxs;q4"))
			{			
				if(args.ServerState["strAgentName"] != null && args.ServerState["strAgentName"].ToString().Length > 0)
				{
					strAgentName = args.ServerState["strAgentName"].ToString();					
				}	
			}
			
			DataSet dataset = DbComboDAL.PayerIDQuery(strAppID,strEnterpriseID,args, "C", strAgentName);	
			return dataset;
		}
		private void SetAgentNameServerStates()
		{						
			String strAgentID=dbCmbAgentId.Value;			
			Hashtable hash = new Hashtable();		
			if (strAgentID !="")
			{
				hash.Add("strAgentID", strAgentID);
			}			
			dbCmbAgentName.ServerState = hash;
			dbCmbAgentName.ServerStateSecretString = "kdfsjgnskajh[apq2-4uhg465654435879mnbfdm cxs;q4";
		}

		private void SetAgentIDServerStates()
		{			
			String strAgentName=dbCmbAgentName.Value;			
			Hashtable hash = new Hashtable();			
			if (strAgentName !="")
			{
				hash.Add("strAgentName", strAgentName);
			}						

			dbCmbAgentId.ServerState = hash;
			dbCmbAgentId.ServerStateSecretString = "kdfsjgnskajh[apq2-4uhg465654435879mnbfdm cxs;q4";			
		}

		private void btnQry_Click(object sender, System.EventArgs e)
		{
			ResetScreenForQuery();
		}
		
		private void InsertAgent(int iRowIndex)
		{
			m_sdsAgent = SysDataMgrDAL.GetCustomerDS(1);		
			DataRow drEach =  m_sdsAgent.ds.Tables[0].Rows[0];	
			drEach["custid"]	= dbCmbAgentId.Text; 
			drEach["cust_name"]= dbCmbAgentName.Text;
			Session["SESSION_DS3"]= m_sdsAgent;
		}
		private void GetAgentRecSet()
		{
			int iStartIndex = Convert.ToInt32(ViewState["currentSet"]) * m_iSetSize;
			DataSet dsQuery = (DataSet)ViewState["QUERY_DS"];
			m_sdsAgent = SysDataMgrDAL.GetCustomerDS(utility.GetAppID(),utility.GetEnterpriseID(),iStartIndex,m_iSetSize,dsQuery);
			Session["SESSION_DS3"] = m_sdsAgent;
			ViewState["QryRes"] =  m_sdsAgent.QueryResultMaxSize; 			
			decimal pgCnt = (m_sdsAgent.QueryResultMaxSize - 1)/m_iSetSize;
			if(pgCnt < Convert.ToDecimal(ViewState["currentSet"]))
			{
				ViewState["currentSet"] = System.Convert.ToInt32(pgCnt);
			}
		}
		private void btnExecQry_Click(object sender, System.EventArgs e)
		{
			lblServiceMessage.Text = "";
			InsertAgent(0);
			ViewState["QUERY_DS"] = m_sdsAgent.ds;
			ViewState["currentPage"] = 0;
			ViewState["currentSet"] = 0;
			GetAgentRecSet();			
			if ((decimal)ViewState["QryRes"]!=0)
			{
				DisplayCurrentPage();

				ResetDetailsCustomizedGridData();
				FillVASDataRow(dgCustomizedCode.Items[0],0);
				ViewState["QUERY_DSV"] = m_sdsCustomizedCode.ds;
				dgCustomizedCode.CurrentPageIndex = 0;
				ShowCurrentServicePage();

				btnExecQry.Enabled = false;
				ViewState["CustomizedMode"] = ScreenMode.ExecuteQuery;
				ViewState["AgentMode"] = ScreenMode.ExecuteQuery;
				btnGoToFirstPage.Enabled = true;
				btnGoToLastPage.Enabled=true;
				btnPreviousPage.Enabled =true;
				btnNextPage.Enabled=true;  
				btnInsertVAS.Enabled=true;
				EnablingDBCombo(false);
				
			}
			else
			{
				lblServiceMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"NO_RECORD_FOUND",utility.GetUserCulture());
			}
		}  
		private void EnablingDBCombo(bool val)
		{
			dbCmbAgentId.Enabled=val;
			dbCmbAgentName.Enabled=val;
		}
		private void DisplayCurrentPage()
		{
			DataRow drCurrent = m_sdsAgent.ds.Tables[0].Rows[Convert.ToInt32(ViewState["currentPage"])];
						
			if(!drCurrent["custid"].GetType().Equals(System.Type.GetType("System.DBNull")))
			{
				dbCmbAgentId.Text		= drCurrent["custid"].ToString();
				dbCmbAgentId.Value		= drCurrent["custid"].ToString();
				ViewState["CustID"]	= drCurrent["custid"].ToString();
				
			}
			if(!drCurrent["cust_name"].GetType().Equals(System.Type.GetType("System.DBNull")))
			{
				dbCmbAgentName.Text		= drCurrent["cust_name"].ToString();
				dbCmbAgentName.Value	= drCurrent["cust_name"].ToString();
			}

		}
		private void btnInsert_Click(object sender, System.EventArgs e)
		{
			btnExecQry.Enabled = false;
		}
	
		private void btnNextPage_Click(object sender, System.EventArgs e)
		{
			
			int iCurrentPage = Convert.ToInt32(ViewState["currentPage"]);

			if(Convert.ToInt32(ViewState["currentPage"])  < (m_sdsAgent.DataSetRecSize - 1) )
			{
				ViewState["currentPage"] = Convert.ToInt32(ViewState["currentPage"]) + 1;
				DisplayCurrentPage();
			}
			else
			{
				decimal iTotalRec = (Convert.ToInt32(ViewState["currentSet"]) * m_iSetSize) + m_sdsAgent.DataSetRecSize;
				if( iTotalRec ==  m_sdsAgent.QueryResultMaxSize)
				{
					EnableNavigationButtons(true,true,false,false);
					return;
				}
				
				ViewState["currentSet"] = Convert.ToInt32(ViewState["currentSet"]) + 1;	
				ViewState["currentPage"] = 0;
				DisplayCurrentPage();
			}
			EnableNavigationButtons(true,true,true,true);
			SetAgentIDServerStates();
			SetAgentNameServerStates();
			EnablingDBCombo(false);
			ShowCurrentServicePage();
		}
		private void btnGoToLastPage_Click(object sender, System.EventArgs e)
		{
			ViewState["currentSet"] = Convert.ToInt32((m_sdsAgent.QueryResultMaxSize - 1))/m_iSetSize;	
			GetAgentRecSet();
			ViewState["currentPage"] = m_sdsAgent.DataSetRecSize - 1;
			DisplayCurrentPage();
			EnableNavigationButtons(true,true,false,false);
			SetAgentIDServerStates();
			SetAgentNameServerStates();
			EnablingDBCombo(false);
			ShowCurrentServicePage();
		}

		private void btnPreviousPage_Click(object sender, System.EventArgs e)
		{
			int iCurrentPage = Convert.ToInt32(ViewState["currentPage"]);

			if(Convert.ToInt32(ViewState["currentPage"])  > 0 )
			{
				ViewState["currentPage"] = Convert.ToInt32(ViewState["currentPage"]) - 1;
				DisplayCurrentPage();	
			}
			else
			{
				if( (Convert.ToInt32(ViewState["currentSet"]) - 1) < 0)
				{
					EnableNavigationButtons(false,false,true,true);
					return;
				}
				
				ViewState["currentSet"] = Convert.ToInt32(ViewState["currentSet"]) - 1;	
				GetAgentRecSet();
				ViewState["currentPage"] = m_sdsAgent.DataSetRecSize - 1;
				DisplayCurrentPage();
			}
			EnableNavigationButtons(true,true,true,true);
			SetAgentIDServerStates();
			SetAgentNameServerStates();
			EnablingDBCombo(false);
			ShowCurrentServicePage();
		}

		private void btnGoToFirstPage_Click(object sender, System.EventArgs e)
		{
			ViewState["currentSet"] = 0;	
			GetAgentRecSet();
			ViewState["currentPage"] = 0;
			DisplayCurrentPage();
			EnableNavigationButtons(false,false,true,true);		
			SetAgentIDServerStates();
			SetAgentNameServerStates();
			EnablingDBCombo(false);
			ShowCurrentServicePage();
		}

		private void btnQuery_Click(object sender, System.EventArgs e)
		{
			ResetScreenForQuery();
			ResetDetailsCustomizedGrid();
			ResetDetailsCustomizedGridData();
			btnInsertVAS.Enabled=true;

		}

		private void EnableNavigationButtons(bool bMoveFirst, bool bMoveNext,bool bEnableMovePrevious, bool bMoveLast)
		{
			btnGoToFirstPage.Enabled	= bMoveFirst;
			btnPreviousPage.Enabled		= bMoveNext;
			btnNextPage.Enabled			= bEnableMovePrevious;
			btnGoToLastPage.Enabled		= bMoveLast;
		}
		#endregion

		#region Code for Costumized code
		/// <summary>
		///Costumized code
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void btnInsertVAS_Click(object sender, System.EventArgs e)
		{
			btnInsertVAS.Enabled	= false;
			lblServiceMessage.Text	= "";
			if( m_sdsCustomizedCode.ds.Tables[0].Rows.Count >= dgCustomizedCode.PageSize)
			{
				ResetDetailsCustomizedGridData();
			}

			ViewState["CustomizedMode"]			= ScreenMode.Insert;
			ViewState["CustomizedOperation"]	= Operation.Insert;
			AddRowInCustomizedGrid();
			dgCustomizedCode.EditItemIndex		= m_sdsCustomizedCode.ds.Tables[0].Rows.Count - 1;
			BindServiceGrid();
		}
		
		public void dgCustomizedCode_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			lblServiceMessage.Text="";
			Label lblServiceCode = (Label)dgCustomizedCode.SelectedItem.FindControl("lblServiceCode");
			msTextBox txtServiceCode = (msTextBox)dgCustomizedCode.SelectedItem.FindControl("txtServiceCode");
			String strServiceCode = null;

			if(lblServiceCode != null)
			{
				strServiceCode = lblServiceCode.Text;
			}

			if(txtServiceCode != null)
			{
				strServiceCode = txtServiceCode.Text;
			}
			if((int)ViewState["CustomizedMode"] != (int)ScreenMode.Insert || (int)ViewState["CustomizedOperation"] != (int)Operation.Insert)
			{
				
				ViewState["CurrentService"] = strServiceCode;

				Logger.LogDebugInfo("VASZipcodeExcluded","dgCustomizedCode_SelectedIndexChanged","INF004","updating data grid..."+dgCustomizedCode.SelectedIndex+"  : "+strServiceCode);
				
				ViewState["ZServiceEMode"] = ScreenMode.ExecuteQuery;

				if((int)ViewState["CustomizedMode"] != (int)ScreenMode.Insert && (int)ViewState["CustomizedOperation"] != (int)Operation.Insert)
				{
					//	Utility.EnableButton(ref btnInsert,ButtonType.Insert,true,m_moduleAccessRights);
				}
				
			}
			lblServiceMessage.Text	= "";			
		}

		protected void dgCustomizedCode_Edit(object sender, DataGridCommandEventArgs e)
		{
			if((int)ViewState["CustomizedMode"] == (int)ScreenMode.Insert && (int) ViewState["CustomizedOperation"] == (int)Operation.Insert && dgCustomizedCode.EditItemIndex > 0)
			{
				m_sdsCustomizedCode.ds.Tables[0].Rows.RemoveAt(dgCustomizedCode.EditItemIndex);
				m_sdsCustomizedCode.QueryResultMaxSize--;
				dgCustomizedCode.CurrentPageIndex = 0;
			}
			dgCustomizedCode.EditItemIndex = e.Item.ItemIndex;
			ViewState["CustomizedOperation"] = Operation.Update;
			BindServiceGrid();
			lblServiceMessage.Text = "";
			Logger.LogDebugInfo("VASZipcodeExcluded","dgCustomizedCode_Edit","INF004","updating data grid...");
			SetAgentIDServerStates();
			SetAgentNameServerStates();
		}
		
		public void dgCustomizedCode_Update(object sender, DataGridCommandEventArgs e)
		{
			lblServiceMessage.Text="";
			String strAgentID=(String)ViewState["CustID"];
			FillVASDataRow(e.Item,e.Item.ItemIndex);
			int intChkRet = CheckTransit(e.Item.ItemIndex);
			if (intChkRet < 0)
			{
				return;

			}
			int iOperation = (int)ViewState["CustomizedOperation"];
			switch(iOperation)
			{

				case (int)Operation.Update:
					break;
				case (int)Operation.Insert:
					DataSet dsToInsert = m_sdsCustomizedCode.ds.GetChanges();
					try
					{
						SysDataMgrDAL.AddCustCustomizedCodeDS(utility.GetAppID(),utility.GetEnterpriseID(),strAgentID,dsToInsert);
						lblServiceMessage.Text=Utility.GetLanguageText(ResourceType.UserMessage,"INS_SUCCESSFULLY",utility.GetUserCulture());
						//		Utility.EnableButton(ref btnInsert,ButtonType.Insert,true,m_moduleAccessRights);
						btnInsertVAS.Enabled=true;
									
									
					}
					catch(ApplicationException appException)
					{
						String strMsg = appException.Message;
						strMsg = appException.InnerException.Message;
						strMsg = appException.InnerException.InnerException.Message;
						if(strMsg.IndexOf("duplicate key") != -1 )
						{
							lblServiceMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"DUP_KEY_FOUND_CUSTOMIZED",utility.GetUserCulture());
							return;
						}
						if(strMsg.IndexOf("unique constraint ") != -1 )
						{
							lblServiceMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"DUP_KEY_FOUND_CUSTOMIZED",utility.GetUserCulture());
							return;
						}
						
					}
					m_sdsCustomizedCode.ds.Tables[0].Rows[e.Item.ItemIndex].AcceptChanges();
					Logger.LogDebugInfo("VASZipcodeExcluded","dgCustomizedCode_OnUpdate","INF004","update in Insert mode");
					break;
			}
			dgCustomizedCode.EditItemIndex = -1;
			ViewState["CustomizedOperation"] = Operation.None;
			BindServiceGrid();
		}

		protected void dgCustomizedCode_Cancel(object sender, DataGridCommandEventArgs e)
		{
			lblServiceMessage.Text="";

			dgCustomizedCode.EditItemIndex = -1;

			if((int)ViewState["CustomizedMode"] == (int)ScreenMode.Insert && (int)ViewState["CustomizedOperation"] == (int)Operation.Insert)
			{
				m_sdsCustomizedCode.ds.Tables[0].Rows.RemoveAt(e.Item.ItemIndex);
				m_sdsCustomizedCode.QueryResultMaxSize--;
				dgCustomizedCode.CurrentPageIndex = 0;
			}
			ViewState["CustomizedOperation"] = Operation.None;
			BindServiceGrid();
			//		Utility.EnableButton(ref btnInsert,ButtonType.Insert,true,m_moduleAccessRights);
			lblServiceMessage.Text = "";
			Logger.LogDebugInfo("VASZipcodeExcluded","dgCustomizedCode_Cancel","INF004","updating data grid...");			
		}

		public void dgCustomizedCode_Delete(object sender, DataGridCommandEventArgs e)
		{
			String strAgentID=(String)ViewState["CustID"];
			String strServiceCode = null;
			try
			{
				msTextBox txtAssemblyid = (msTextBox)e.Item.FindControl("txtAssemblyid");
				Label lblAssemblyid = (Label)e.Item.FindControl("lblAssemblyid");
				if(txtAssemblyid != null)
				{
					strServiceCode = txtAssemblyid.Text;
				}

				if(lblAssemblyid != null)
				{
					strServiceCode = lblAssemblyid.Text;
				}
			}
			catch(Exception ex)
			{
				String msg = ex.ToString();
				return;
			}
			try
			{
				SysDataMgrDAL.DeleteCustCustomizedCodeDS(utility.GetAppID(),utility.GetEnterpriseID(),strAgentID,strServiceCode);
				lblServiceMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"DLD_SUCCESSFULLY",utility.GetUserCulture());
				
			}
			catch(ApplicationException appException)
			{
				String strMsg = appException.Message;
				if(strMsg.IndexOf("FK") != -1)
				{
					lblServiceMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"ERR_DEL_VAS_TRANS",utility.GetUserCulture());
				}
				if(strMsg.IndexOf("child record") != -1)
				{
					lblServiceMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"ERR_DEL_VAS_TRANS",utility.GetUserCulture());
				}
				return;

			}

			if((int)ViewState["CustomizedMode"] == (int)ScreenMode.Insert)
			{
				m_sdsCustomizedCode.ds.Tables[0].Rows.RemoveAt(e.Item.ItemIndex);
				m_sdsCustomizedCode.QueryResultMaxSize--;
				dgCustomizedCode.CurrentPageIndex = 0;

				if((int) ViewState["CustomizedOperation"] == (int)Operation.Insert && dgCustomizedCode.EditItemIndex > 0)
				{
					m_sdsCustomizedCode.ds.Tables[0].Rows.RemoveAt(dgCustomizedCode.EditItemIndex-1);
					m_sdsCustomizedCode.QueryResultMaxSize--;
				}
				dgCustomizedCode.EditItemIndex = -1;
				dgCustomizedCode.SelectedIndex = -1;
				BindServiceGrid();
			}
			else
			{
				ShowCurrentServicePage();
			}
			Logger.LogDebugInfo("VASZipcodeExcluded","dgCustomizedCode_Delete","INF004","Deleted row in VAS_Code table..");
			ViewState["CustomizedOperation"] = Operation.None;
			//	Utility.EnableButton(ref btnInsert,ButtonType.Insert,true,m_moduleAccessRights);
		}

		private void BindServiceGrid()
		{
			dgCustomizedCode.VirtualItemCount = System.Convert.ToInt32(m_sdsCustomizedCode.QueryResultMaxSize);
			dgCustomizedCode.DataSource = m_sdsCustomizedCode.ds;
			dgCustomizedCode.DataBind();
			Session["SESSION_DS1"] = m_sdsCustomizedCode;
		}

		private void AddRowInCustomizedGrid()
		{
			AgentProfileMgrDAL.AddAgentNewRowInCustomizedCodeDS(m_sdsCustomizedCode);
			Session["SESSION_DS1"] = m_sdsCustomizedCode;
		}

		protected void dgCustomizedCode_Bound(object sender, DataGridItemEventArgs e)
		{
			SetAgentIDServerStates();
			SetAgentNameServerStates();

			if(e.Item.ItemIndex == -1)
			{
				return;
			}

			DataRow drSelected = m_sdsCustomizedCode.ds.Tables[0].Rows[e.Item.ItemIndex];
			if(drSelected.RowState == DataRowState.Deleted)
			{
				return;
			}

			msTextBox txtServiceCode = (msTextBox)e.Item.FindControl("txtServiceCode");


			int iOperation = (int) ViewState["CustomizedOperation"];
			if(txtServiceCode != null && iOperation == (int)Operation.Update)
			{
				txtServiceCode.Enabled = false;
				e.Item.Cells[3].Enabled=false;
			}
			

		}

		protected void dgCustomizedCode_PageChange(object sender, DataGridPageChangedEventArgs e)
		{
			lblServiceMessage.Text="";
			dgCustomizedCode.CurrentPageIndex = e.NewPageIndex;
			ShowCurrentServicePage();
			Logger.LogDebugInfo("VASZipcodeExcluded","dgCustomizedCode_PageChange","INF009","On Page Change "+e.NewPageIndex);
		}
		
		public void dgCustomizedCode_Button(object sender, DataGridCommandEventArgs e)
		{
			lblServiceMessage.Text="";
			String strCmdNm = e.CommandName;			
			if(strCmdNm.Equals("search"))
			{
				int iOperation = (int) ViewState["CustomizedOperation"];
				if(dgCustomizedCode.EditItemIndex  != e.Item.ItemIndex)
				{
					return;
				}
				if(iOperation == (int)Operation.Update)
				{
					return;
				}
				
				msTextBox txtCustomizedDesc			= (msTextBox)e.Item.FindControl("txtCustDescription");
				msTextBox txtCustomizedCode			= (msTextBox)e.Item.FindControl("txtCustomizedCode");
				TextBox txtAssemblyid				= (TextBox)e.Item.FindControl("txtAssemblyid");
				TextBox txtDescription				= (TextBox)e.Item.FindControl("txtDescription");
				String strCustomizedCodeClientID	= null;
				String strAssemblyIdClientID		= null;				
				String strCustomizedCode			= null;
				String strAssemblyId				= null;
				String strCustomizedCodeClientDesc	= null;

				String strDesc						= null;
				String strDescClientID				= null;
				if(txtCustomizedDesc != null)
				{
					strCustomizedCodeClientDesc		= txtCustomizedDesc.ClientID;
					//strCustomizedCode				= txtCustomizedDesc.Text;
				}
				if(txtCustomizedCode != null)
				{
					strCustomizedCodeClientID		= txtCustomizedCode.ClientID;
					strCustomizedCode				= txtCustomizedCode.Text;
				}
				if(txtAssemblyid != null)
				{
					strAssemblyIdClientID			= txtAssemblyid.ClientID;
					strAssemblyId					= txtAssemblyid.Text;
				}	
			
				if(txtAssemblyid != null)
				{
					strDescClientID					= txtDescription.ClientID;
					strDesc							= txtDescription.Text;
				}	

				String sUrl = "CustomizedCodePopup.aspx?FORMID=CustomerCustomizedCharges&DESC="+strDescClientID+"&ASSEMBLYID="+strAssemblyIdClientID+"&CODEID="+strCustomizedCodeClientID+"&CODEDESC="+strCustomizedCodeClientDesc;
				ArrayList paramList = new ArrayList();
				paramList.Add(sUrl);
				String sScript = Utility.GetScript("openWindowParam.js",paramList);
				Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);	
			}		
		}
		private void ShowCurrentServicePage()
		{
			String strAgentID=(String)ViewState["CustID"];
			int iStartIndex = dgCustomizedCode.CurrentPageIndex * dgCustomizedCode.PageSize;
			DataSet dsQuery = (DataSet)ViewState["QUERY_DSV"];
			m_sdsCustomizedCode = SysDataMgrDAL.GetCustCustomizedCodeDS(utility.GetAppID(),utility.GetEnterpriseID(),strAgentID,iStartIndex,dgCustomizedCode.PageSize,dsQuery);
			int pgCnt = (Convert.ToInt32(m_sdsCustomizedCode.QueryResultMaxSize) - 1)/dgCustomizedCode.PageSize;
			if(pgCnt < dgCustomizedCode.CurrentPageIndex)
			{
				dgCustomizedCode.CurrentPageIndex = System.Convert.ToInt32(pgCnt);
			}
			dgCustomizedCode.SelectedIndex = -1;
			dgCustomizedCode.EditItemIndex = -1;
			BindServiceGrid();
		}

		private void FillVASDataRow(DataGridItem item, int drIndex)
		{
			msTextBox txtCustDescription = (msTextBox)item.FindControl("txtCustDescription");
			if(txtCustDescription != null)
			{
				DataRow drCurrent = m_sdsCustomizedCode.ds.Tables[0].Rows[drIndex];
				drCurrent["code_text"] = txtCustDescription.Text;
			}

			msTextBox txtCustomizedCode = (msTextBox)item.FindControl("txtCustomizedCode");
			if(txtCustomizedCode != null)
			{
				DataRow drCurrent = m_sdsCustomizedCode.ds.Tables[0].Rows[drIndex];
				drCurrent["customized_assembly_code"] = txtCustomizedCode.Text;
			}

			TextBox txtAssemblyid = (TextBox)item.FindControl("txtAssemblyid");
			if(txtAssemblyid != null)
			{
				DataRow drCurrent = m_sdsCustomizedCode.ds.Tables[0].Rows[drIndex];
				drCurrent["assemblyid"] = txtAssemblyid.Text;
			}

			TextBox txtDescription = (TextBox)item.FindControl("txtDescription");
			if(txtDescription != null)
			{
				DataRow drCurrent = m_sdsCustomizedCode.ds.Tables[0].Rows[drIndex];
				drCurrent["Assembly_description"] = txtDescription.Text;
			}
		}

		private void ResetDetailsCustomizedGrid()
		{
			m_sdsCustomizedCode = AgentProfileMgrDAL.GetAgentEmptyCustomizedCodeDS(0);			// 11/11/2002 
			Session["SESSION_DS1"]	= m_sdsCustomizedCode;		
			dgCustomizedCode.EditItemIndex = 0;
			dgCustomizedCode.CurrentPageIndex = 0;
			BindServiceGrid();
		}
		private void ResetDetailsCustomizedGridData()
		{
			m_sdsCustomizedCode = AgentProfileMgrDAL.GetAgentEmptyCustomizedCodeDS(1);			// 11/11/2002 
			Session["SESSION_DS1"]	= m_sdsCustomizedCode;		
			dgCustomizedCode.EditItemIndex = 0;
			dgCustomizedCode.CurrentPageIndex = 0;
			BindServiceGrid();
		}

		private int CheckTransit(int drIndex)
		{		
			return drIndex;
		}

		#endregion
		
	}
}
