<%@ Import Namespace="com.common.util" %>
<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<%@ Page language="c#" Codebehind="EnterpriseLogon.aspx.cs" AutoEventWireup="false" Inherits="com.ties.EnterpriseLogon" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>EnterpriseLogon</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<base target="_top">
		<script language="javascript" src="Scripts/msFormValidation.js"></script>
		<style type="text/css">TD { FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; COLOR: #ffffff; FONT-SIZE: 11px; FONT-WEIGHT: bold }
	INPUT { FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: 11px }
	.shdw { FILTER: DropShadow(Color=#000000, OffX=1, OffY=1, Positive=true) }
	BODY { BACKGROUND-IMAGE: url(images/login.jpg); BACKGROUND-REPEAT: repeat-x; BACKGROUND-POSITION: 0px 60px; BACKGROUND-COLOR: Black}
		</style>
		<script language="JavaScript" type="text/JavaScript">
<!--
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);
//-->
		</script>
		<script>
			function getFrames()
			{
				
				if(window.top.frames.length>0)
				{
					window.top.location.href="EnterpriseLogon.aspx?errMsg=Authentication expired.Please login again";
					if(document.forms[0].txtUserID.value=="" || document.forms[0].txtUserPswd.value=="" || document.forms[0].txtEnterpriseID.value=="")
						document.forms[0].txtUserID.focus();
					
				}
				else
				{
					
					if(document.forms[0].txtUserID.value=="" || document.forms[0].txtUserPswd.value=="" || document.forms[0].txtEnterpriseID.value=="")
					{
						document.forms[0].txtUserID.focus();						
					}
				}
				
			}
		</script>
	</HEAD>
	<body bgColor="#133d49" onload="return getFrames();">
		<form id="EnterpriseLogon" method="post" runat="server">
			<table height="460" cellSpacing="0" cellPadding="5" width="100%" border="0">
				<tr>
					<td vAlign="bottom">
						<table cellSpacing="0" cellPadding="3" width="880" border="0">
							<tr>
								<td width="530">&nbsp;</td>
								<td class="shdw" width="80"><asp:label id="Label1" runat="server" Font-Size="X-Small" Font-Names="HandelGotDLig" BackColor="Transparent"
										BorderColor="Transparent" Font-Bold="True" ForeColor="WhiteSmoke" Height="16px" Width="97px">User ID</asp:label></td>
								<td class="shdw" width="10">:</td>
								<td><cc1:mstextbox id="txtUserID" style="TEXT-TRANSFORM: uppercase" runat="server" Width="155px" TextMaskType="msUpperAlfaNumericWithUnderscore"
										name="txtUserID"></cc1:mstextbox></td>
							</tr>
							<tr>
								<td>&nbsp;</td>
								<td class="shdw"><asp:label id="Label2" runat="server" Font-Size="X-Small" Font-Names="HandelGotDLig" BackColor="Transparent"
										BorderColor="Transparent" Font-Bold="True" ForeColor="WhiteSmoke" Height="16px" Width="97px">Password</asp:label></td>
								<td class="shdw">:</td>
								<td><asp:textbox id="txtUserPswd" runat="server" Width="155px" name="txtUserPswd" TextMode="Password"></asp:textbox></td>
							</tr>
							<tr>
								<td>&nbsp;</td>
								<td class="shdw"><asp:label id="Label3" runat="server" Font-Size="X-Small" Font-Names="HandelGotDLig" BackColor="Transparent"
										BorderColor="Transparent" Font-Bold="True" ForeColor="WhiteSmoke" Height="16px" Width="97px">Enterprise ID</asp:label></td>
								<td class="shdw">:</td>
								<td><asp:textbox id="txtEnterpriseID" runat="server" Width="155px" name="txtEnterpriseID">PNGAF</asp:textbox></td>
							</tr>
							<tr>
								<td colSpan="4">&nbsp;</td>
							</tr>
							<tr>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td><asp:button id="btnSubmit" tabIndex="3" runat="server" Height="20" Width="61" Text="Submit"></asp:button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									<asp:button id="btnCancel" tabIndex="4" runat="server" Height="20px" Width="61px" Text="Cancel"></asp:button></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<asp:label id="lblErrMsg" runat="server" BackColor="Transparent" BorderColor="Transparent"
				Font-Bold="True" ForeColor="WhiteSmoke" Width="422px"></asp:label></form>
	</body>
</HTML>
