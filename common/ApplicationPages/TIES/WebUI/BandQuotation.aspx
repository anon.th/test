<%@ Import Namespace="com.common.util" %>
<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<%@ Page language="c#" Codebehind="BandQuotation.aspx.cs" AutoEventWireup="false" Inherits="com.ties.BandQuotation" SmartNavigation="false" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>BandQuotation</title>
		<LINK href="css/Styles.css" type="text/css" rel="stylesheet" name="stylesheet">
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<!--#INCLUDE FILE="msFormValidations.inc"-->
		<script language="javascript" src="Scripts/settingScrollPosition.js"></script>
	</HEAD>
	<body onclick="GetScrollPosition();" onload="SetScrollPosition();" onunload="window.top.displayBanner.fnCloseAll(0);" MS_POSITIONING="GridLayout">
		<form id="BandQuotation" method="post" runat="server">
			<P>&nbsp;</P>
			<P><asp:label id="Header1" style="Z-INDEX: 103; LEFT: 14px; POSITION: absolute; TOP: 23px" CssClass="mainTitleSize" Runat="server" Width="408px">Band Quotation</asp:label></P>
			<P></P>
			<P>&nbsp;</P>
			<P></P>
			<div id="divMain" style="Z-INDEX: 101; LEFT: -221px; WIDTH: 1075px; POSITION: absolute; TOP: 48px; HEIGHT: 762px" MS_POSITIONING="GridLayout" runat="server">
				<table id="Table1" style="Z-INDEX: 100; LEFT: 229px; WIDTH: 720px; POSITION: absolute; TOP: 59px; HEIGHT: 190px" runat="server">
					<TR>
						<TD></TD>
						<TD><asp:label id="lblQuoatationNo" runat="server" CssClass="tableLabel">Quotation No.</asp:label></TD>
						<TD><asp:textbox id="txtQuoationno" tabIndex="1" runat="server" CssClass="textField" Width="100px"></asp:textbox></TD>
						<td></td>
						<TD><asp:label id="lblStatus" runat="server" CssClass="tableLabel" Width="86px">Status</asp:label></TD>
						<TD><asp:textbox id="txtStatus" tabIndex="2" runat="server" CssClass="textField" Width="100px"></asp:textbox></TD>
						<TD></TD>
					</TR>
					<TR>
						<TD></TD>
						<TD><asp:label id="lblQuotationDate" runat="server" CssClass="tableLabel" Width="100px">Quotation Date</asp:label></TD>
						<TD><cc1:mstextbox id="txtQuotationDate" tabIndex="5" CssClass="textField" Runat="server" Width="100px" TextMaskString="99/99/9999 99:99" MaxLength="16" TextMaskType="msDateTime" Enabled="True" Text=""></cc1:mstextbox></TD>
						<td></td>
						<TD><asp:label id="lblVersionno" runat="server" CssClass="tableLabel">Version No</asp:label></TD>
						<TD><asp:textbox id="txtversionno" tabIndex="3" runat="server" CssClass="textField" Width="100px"></asp:textbox><asp:requiredfieldvalidator id="RequiredFieldValidator1" runat="server" ControlToValidate="txtversionno" ErrorMessage="Version No">*</asp:requiredfieldvalidator></TD>
						<td></td>
					</TR>
					<TR>
						<TD><asp:requiredfieldvalidator id="VaildCustType" runat="server" ControlToValidate="DrpCustType" ErrorMessage="Select Customer Type">*</asp:requiredfieldvalidator></TD>
						<TD><asp:label id="Label2" runat="server" CssClass="tableLabel">Customer Type</asp:label></TD>
						<TD><asp:dropdownlist id="DrpCustType" tabIndex="6" runat="server" Width="100px" AutoPostBack="True"></asp:dropdownlist></TD>
						<TD></TD>
						<TD></TD>
						<TD></TD>
						<TD></TD>
					</TR>
					<TR>
						<TD><asp:requiredfieldvalidator id="ValidCustCode" runat="server" ControlToValidate="txtCustID" ErrorMessage="Enter Customer Code">*</asp:requiredfieldvalidator></TD>
						<TD><asp:label id="lblCustomerCode" runat="server" CssClass="tableLabel">Customer ID</asp:label></TD>
						<TD><asp:textbox id="txtCustID" tabIndex="7" runat="server" CssClass="textField" Width="100px" MaxLength="20"></asp:textbox><asp:button id="btnCustomerId" tabIndex="8" runat="server" CssClass="searchButton" Text="..." CausesValidation="False"></asp:button></TD>
						<TD></TD>
						<TD><asp:label id="lblCustomerName" runat="server" CssClass="tableLabel">Customer Name</asp:label></TD>
						<TD><asp:textbox id="txtCustName" tabIndex="9" runat="server" CssClass="textField" Width="189px" ReadOnly="True"></asp:textbox></TD>
						<TD></TD>
					</TR>
					<TR>
						<TD></TD>
						<TD><asp:label id="lblSalesmanId" runat="server" CssClass="tableLabel">Salesman ID</asp:label></TD>
						<TD><asp:textbox id="txtSalesmanId" tabIndex="10" runat="server" CssClass="textField" Width="100px" MaxLength="20"></asp:textbox><asp:button id="btnSalesmanId" tabIndex="11" runat="server" CssClass="searchButton" Text="..." CausesValidation="False"></asp:button></TD>
						<TD></TD>
						<TD><asp:label id="lblSalesmanName" runat="server" CssClass="tableLabel">Salesman Name</asp:label></TD>
						<TD><asp:textbox id="txtSalesPername" runat="server" CssClass="textField" Width="189px" ReadOnly="True"></asp:textbox></TD>
						<TD></TD>
					</TR>
					<TR>
						<TD></TD>
						<TD><asp:label id="lblBandId" runat="server" CssClass="tableLabel">Band ID</asp:label></TD>
						<TD><asp:textbox id="txtbandId" tabIndex="13" runat="server" CssClass="textField" Width="100px" MaxLength="12"></asp:textbox><asp:button id="btnBandId" tabIndex="14" runat="server" CssClass="searchButton" Text="..." CausesValidation="False"></asp:button></TD>
						<TD></TD>
						<TD><asp:label id="lblPerctDis" runat="server" CssClass="tableLabel" Width="134px">Percentage Discount</asp:label></TD>
						<TD><cc1:mstextbox id="txtPctDisc" tabIndex="15" runat="server" CssClass="textFieldRightAlign" Width="100px" TextMaskType="msNumeric" NumberMaxValue="100" NumberMinValue="0" NumberPrecision="5" NumberScale="2"></cc1:mstextbox></TD>
						<TD></TD>
					</TR>
				</table>
				<TABLE id="Table2" style="Z-INDEX: 101; LEFT: 227px; WIDTH: 848px; POSITION: absolute; TOP: 18px; HEIGHT: 34px" border="0" runat="server">
					<TR>
						<TD style="WIDTH: 327px"><asp:button id="btnQuery" runat="server" CssClass="queryButton" Width="67px" Text="Query" CausesValidation="False"></asp:button><asp:button id="btnExceuteQuery" runat="server" CssClass="queryButton" Width="107px" Text="Execute Query" CausesValidation="False"></asp:button><asp:button id="btnInsert" runat="server" CssClass="queryButton" Width="50px" Text="Insert" CausesValidation="False"></asp:button><asp:button id="btnSave" runat="server" CssClass="queryButton" Width="45px" Text="Save"></asp:button><asp:button id="btnDelete" runat="server" CssClass="queryButton" Width="50px" Text="Delete" CausesValidation="False"></asp:button></TD>
						<td style="WIDTH: 354px"><asp:button id="btnQuoted" runat="server" CssClass="queryButton" Width="67px" Text="Quoted" CausesValidation="False"></asp:button><asp:button id="btnPrint" runat="server" CssClass="queryButton" Width="52px" Text="Print" CausesValidation="False"></asp:button><asp:button id="btnNewVersion" runat="server" CssClass="queryButton" Width="96px" Text="New Version" CausesValidation="False"></asp:button><asp:button id="btnEmail" runat="server" CssClass="queryButton" Width="134px" Text="Email" CausesValidation="False"></asp:button></td>
						<td><asp:button id="btnMoveFirst" runat="server" CssClass="queryButton" Width="22px" Text="|<" CausesValidation="False"></asp:button><asp:button id="btnMovePrevious" runat="server" CssClass="queryButton" Width="22px" Text="<" CausesValidation="False"></asp:button><asp:textbox id="txtRecCnt" runat="server" Width="22px" Height="18px"></asp:textbox><asp:button id="btnMoveNext" runat="server" CssClass="queryButton" Width="22px" Text=">" CausesValidation="False"></asp:button><asp:button id="btnMoveLast" runat="server" CssClass="queryButton" Width="22px" Text=">|" CausesValidation="False"></asp:button></td>
					</TR>
				</TABLE>
				<fieldset style="Z-INDEX: 102; LEFT: 229px; WIDTH: 730px; POSITION: absolute; TOP: 256px; HEIGHT: 110px"><LEGEND><asp:label id="header2" runat="server" CssClass="tableHeadingFieldset" Width="94px">Contact Person</asp:label></LEGEND>
					<TABLE id="Table4" style="WIDTH: 720px">
						<TR>
							<TD></TD>
							<TD style="WIDTH: 132px"><asp:label id="lblAttentionTo" runat="server" CssClass="tableLabel">Attention To</asp:label></TD>
							<TD style="WIDTH: 134px"><asp:textbox id="txtAttentionTo" tabIndex="16" runat="server" CssClass="textField" Width="160px" MaxLength="100"></asp:textbox></TD>
							<TD style="WIDTH: 6px"></TD>
							<TD style="WIDTH: 9px"><asp:label id="lblCopyto" runat="server" CssClass="tableLabel" Width="51px">Copy To</asp:label></TD>
							<TD><asp:textbox id="txtCopyTo" tabIndex="17" runat="server" CssClass="textField" Width="144px" MaxLength="100"></asp:textbox></TD>
							<TD></TD>
						</TR>
						<TR>
							<TD></TD>
							<TD style="WIDTH: 132px"><asp:label id="lblRemarks" runat="server" CssClass="tableLabel">Remarks</asp:label></TD>
							<TD colSpan="4"><asp:textbox id="txtRemarks" tabIndex="18" runat="server" CssClass="textField" Width="528px" Height="48px" MaxLength="200" TextMode="MultiLine"></asp:textbox></TD>
							<TD></TD>
						</TR>
					</TABLE>
				</fieldset>
				<asp:label id="lblErrrMsg" style="Z-INDEX: 103; LEFT: 228px; POSITION: absolute; TOP: 0px" runat="server" CssClass="errorMsgColor" Width="720px" Height="2px"></asp:label>
				<DIV id="divVAS" style="Z-INDEX: 106; LEFT: 229px; WIDTH: 730px; POSITION: absolute; TOP: 568px; HEIGHT: 106px" runat="server">&nbsp;
					<FIELDSET style="WIDTH: 728px; HEIGHT: 104px" DESIGNTIMEDRAGDROP="537"><LEGEND>
							<asp:label id="Label1" runat="server" CssClass="tableHeadingFieldset" Height="16px" Width="151px" Font-Bold="True">Value Added Services</asp:label></LEGEND>
						<TABLE id="Table6">
							<TR>
								<TD>
									<asp:datagrid id="dgVAS" runat="server" AutoGenerateColumns="False" OnCancelCommand="dgVAS_Cancel" OnEditCommand="dgVAS_Edit" OnItemCommand="dgVAS_Button" OnUpdateCommand="dgVAS_Update" OnDeleteCommand="dgVAS_Delete" OnItemDataBound="dgVAS_ItemDataBound" width="693" PageSize="10" AllowPaging="False">
										<Columns>
											<asp:ButtonColumn Text="&lt;IMG SRC='images/butt-delete.gif' border=0 title='Delete' &gt;" ButtonType="LinkButton" CommandName="Delete">
												<HeaderStyle Width="1%" CssClass="gridHeading"></HeaderStyle>
												<ItemStyle CssClass="gridField"></ItemStyle>
											</asp:ButtonColumn>
											<asp:EditCommandColumn ButtonType="LinkButton" UpdateText="&lt;IMG SRC='images/butt-update.gif' border=0 title='Update' &gt;" CancelText="&lt;IMG SRC='images/butt-cancel.gif' border=0 title='Cancel' &gt;" EditText="&lt;IMG SRC='images/butt-edit.gif' border=0 title='Edit' &gt;">
												<HeaderStyle Width="1%" CssClass="gridHeading"></HeaderStyle>
												<ItemStyle CssClass="gridField"></ItemStyle>
											</asp:EditCommandColumn>
											<asp:TemplateColumn HeaderText="VAS Code">
												<HeaderStyle Width="3%" CssClass="gridHeading"></HeaderStyle>
												<ItemStyle Width="3%" CssClass="gridField"></ItemStyle>
												<ItemTemplate>
													<asp:Label ID="lblVASCode" CssClass="gridLabel" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"vas_code")%>'>
													</asp:Label>
												</ItemTemplate>
												<EditItemTemplate>
													<asp:TextBox CssClass="gridTextBox" ReadOnly=True runat="server" MaxLength=12 ID="txtVASCode" Text='<%#DataBinder.Eval(Container.DataItem,"vas_code")%>'>
													</asp:TextBox>
													<asp:RequiredFieldValidator ID="Requiredfieldvalidator2" Runat="server" BorderWidth="0" Display="None" ControlToValidate="txtVASCode" ErrorMessage="VAS Code "></asp:RequiredFieldValidator>
												</EditItemTemplate>
											</asp:TemplateColumn>
											<asp:ButtonColumn Visible="False" Text="&lt;IMG SRC='images/butt-search.gif' border=0 title='Search' &gt;" ButtonType="LinkButton" CommandName="Search">
												<HeaderStyle Width="1%" CssClass="gridHeading"></HeaderStyle>
												<ItemStyle CssClass="gridField"></ItemStyle>
											</asp:ButtonColumn>
											<asp:TemplateColumn HeaderText="Description">
												<HeaderStyle Width="15%" CssClass="gridHeading"></HeaderStyle>
												<ItemStyle CssClass="gridField"></ItemStyle>
												<ItemTemplate>
													<asp:Label runat="server" CssClass="gridLabel" ID="lblVASDesc" Text='<%#DataBinder.Eval(Container.DataItem,"vas_description")%>'>
													</asp:Label>
												</ItemTemplate>
												<EditItemTemplate>
													<asp:TextBox CssClass="gridTextBox" runat="server" ReadOnly=True ID="txtVASDesc" Text='<%#DataBinder.Eval(Container.DataItem,"vas_description")%>' MaxLength="200">
													</asp:TextBox>
												</EditItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Surcharge">
												<HeaderStyle Width="3%" CssClass="gridHeading"></HeaderStyle>
												<ItemStyle CssClass="gridField"></ItemStyle>
												<ItemTemplate>
													<asp:Label runat="server" CssClass="gridLabelNumber" ID="lblSurcharge" Text='<%#DataBinder.Eval(Container.DataItem,"surcharge","{0:n}")%>'>
													</asp:Label>
												</ItemTemplate>
												<EditItemTemplate>
													<cc1:mstextbox CssClass="gridTextBoxNumber" runat="server" ID="txtSurcharge" Text='<%#DataBinder.Eval(Container.DataItem,"surcharge","{0:n}")%>' TextMaskType="msNumeric" NumberMaxValue="9999999" NumberMinValue="0" NumberPrecision="10" NumberScale="2">
													</cc1:mstextbox>
												</EditItemTemplate>
											</asp:TemplateColumn>
										</Columns>
									</asp:datagrid></TD>
								<TD vAlign="bottom">
									<asp:button id="btndgVasInsert" tabIndex="20" runat="server" CssClass="queryButton" Width="22px" Text="+" CausesValidation="true"></asp:button></TD>
							</TR>
						</TABLE>
					</FIELDSET>
				</DIV>
				<DIV id="divService" style="Z-INDEX: 107; LEFT: 229px; WIDTH: 730px; POSITION: absolute; TOP: 368px; HEIGHT: 180px" runat="server">&nbsp;&nbsp;
					<FIELDSET style="WIDTH: 728px; HEIGHT: 178px"><LEGEND>
							<asp:label id="Label4" runat="server" CssClass="tableHeadingFieldset" Width="68px" Font-Bold="True">Service</asp:label></LEGEND>
						<TABLE id="Table3" cellSpacing="1" cellPadding="1" width="693" border="0" runat="server">
							<TR>
								<TD>
									<asp:datagrid id="dgService" runat="server" AutoGenerateColumns="False" OnCancelCommand="dgService_Cancel" OnEditCommand="dgService_Edit" OnItemCommand="dgService_Button" OnUpdateCommand="dgService_Update" OnDeleteCommand="dgService_Delete" OnItemDataBound="dgService_ItemDataBound" width="693px">
										<Columns>
											<asp:ButtonColumn Text="&lt;IMG SRC='images/butt-delete.gif' border=0 title='Delete' &gt;" CommandName="Delete">
												<HeaderStyle CssClass="gridHeading"></HeaderStyle>
												<ItemStyle CssClass="gridField"></ItemStyle>
											</asp:ButtonColumn>
											<asp:EditCommandColumn ButtonType="LinkButton" UpdateText="&lt;IMG SRC='images/butt-update.gif' border=0 title='Update' &gt;" CancelText="&lt;IMG SRC='images/butt-cancel.gif' border=0 title='Cancel' &gt;" EditText="&lt;IMG SRC='images/butt-edit.gif' border=0 title='Edit' &gt;">
												<HeaderStyle CssClass="gridHeading"></HeaderStyle>
												<ItemStyle CssClass="gridField"></ItemStyle>
											</asp:EditCommandColumn>
											<asp:TemplateColumn HeaderText="Service Code">
												<HeaderStyle CssClass="gridHeading"></HeaderStyle>
												<ItemStyle CssClass="gridField"></ItemStyle>
												<ItemTemplate>
													<asp:Label runat="server" CssClass="gridLabel" ID="lblServiceCode" Text='<%#DataBinder.Eval(Container.DataItem,"service_code")%>'>
													</asp:Label>
												</ItemTemplate>
												<EditItemTemplate>
													<asp:TextBox CssClass="gridTextBox" runat="server" MaxLength=12 ReadOnly=True ID="txtServiceCode" Text='<%#DataBinder.Eval(Container.DataItem,"service_code")%>'>
													</asp:TextBox>
													<asp:RequiredFieldValidator ID="reqServiceCode" Runat="server" BorderWidth="0" Display="None" ControlToValidate="txtServiceCode" ErrorMessage="Service Code "></asp:RequiredFieldValidator>
												</EditItemTemplate>
											</asp:TemplateColumn>
											<asp:ButtonColumn Visible="False" Text="&lt;IMG SRC='images/butt-search.gif' border=0 title='Search' &gt;" CommandName="Search">
												<HeaderStyle CssClass="gridHeading"></HeaderStyle>
												<ItemStyle CssClass="gridField"></ItemStyle>
											</asp:ButtonColumn>
											<asp:TemplateColumn HeaderText="Description">
												<HeaderStyle CssClass="gridHeading"></HeaderStyle>
												<ItemStyle CssClass="gridField"></ItemStyle>
												<ItemTemplate>
													<asp:Label runat="server" CssClass="gridLabel" ID="lblServiceDesc" Text='<%#DataBinder.Eval(Container.DataItem,"service_description")%>'>
													</asp:Label>
												</ItemTemplate>
												<EditItemTemplate>
													<asp:TextBox CssClass="gridTextBox" runat="server" ID="txtServiceDesc" ReadOnly=True Text='<%#DataBinder.Eval(Container.DataItem,"service_description")%>' MaxLength="200">
													</asp:TextBox>
												</EditItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Service Charge (%)">
												<HeaderStyle CssClass="gridHeading"></HeaderStyle>
												<ItemStyle CssClass="gridField"></ItemStyle>
												<ItemTemplate>
													<asp:Label runat="server" CssClass="gridLabelNumber" ID="lblServiceChargePercent" Text='<%#DataBinder.Eval(Container.DataItem,"service_charge_percent","{0:n}")%>'>
													</asp:Label>
												</ItemTemplate>
												<EditItemTemplate>
													<cc1:mstextbox CssClass="gridTextBoxNumber" runat="server" ID="txtServiceChargePercent" Text='<%#DataBinder.Eval(Container.DataItem,"service_charge_percent","{0:n}")%>' TextMaskType="msNumeric" NumberMaxValue="9999999" NumberMinValue="-9999999" NumberPrecision="10" NumberScale="2">
													</cc1:mstextbox>
												</EditItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Service Charge ($)">
												<HeaderStyle CssClass="gridHeading"></HeaderStyle>
												<ItemStyle CssClass="gridField"></ItemStyle>
												<ItemTemplate>
													<asp:Label runat="server" CssClass="gridLabelNumber" ID="lblServiceChargeAmt" Text='<%#DataBinder.Eval(Container.DataItem,"service_charge_amt","{0:n}")%>'>
													</asp:Label>
												</ItemTemplate>
												<EditItemTemplate>
													<cc1:mstextbox CssClass="gridTextBoxNumber" runat="server" ID="txtServiceChargeAmt" Text='<%#DataBinder.Eval(Container.DataItem,"service_charge_amt","{0:n}")%>' TextMaskType="msNumeric" NumberMaxValue="9999999" NumberMinValue="-9999999" NumberPrecision="10" NumberScale="2">
													</cc1:mstextbox>
												</EditItemTemplate>
											</asp:TemplateColumn>
										</Columns>
									</asp:datagrid></TD>
								<TD vAlign="bottom">
									<asp:button id="btnServiceInsert" tabIndex="19" runat="server" CssClass="searchButton" Width="22px" Text="+" CausesValidation="true"></asp:button></TD>
							</TR>
						</TABLE>
					</FIELDSET>
				</DIV>
				<input style="Z-INDEX: 105; LEFT: 277px; POSITION: absolute; TOP: 696px" type=hidden 
value="<%=strScrollPosition%>" name=ScrollPosition>
			</div>
			<DIV id="PickupPanel" style="Z-INDEX: 100; LEFT: 9px; WIDTH: 694px; POSITION: relative; TOP: 40px; HEIGHT: 149px" MS_POSITIONING="GridLayout" runat="server"><br>
				<P align="center"><asp:label id="lblConfirmMsg" runat="server"></asp:label><br>
					<br>
					<asp:button id="btnToSaveChanges" runat="server" CssClass="queryButton" Text="Yes" CausesValidation="False"></asp:button>&nbsp;
					<asp:button id="btnNotToSave" runat="server" CssClass="queryButton" Text="No" CausesValidation="False"></asp:button>&nbsp;
					<asp:button id="btnCancel" runat="server" CssClass="queryButton" Text="Cancel" CausesValidation="False"></asp:button></P>
			</DIV>
			<asp:validationsummary id="PageValidationSummary" style="Z-INDEX: 108; LEFT: 457px; POSITION: absolute; TOP: -44px" runat="server" HeaderText="Please enter the missing  fields." DisplayMode="BulletList" ShowSummary="False" ShowMessageBox="True" Width="185px" Height="105px"></asp:validationsummary>
		</form>
	</body>
</HTML>
