using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.common.util;
using com.ties.DAL;
using com.common.classes;
using com.common.applicationpages;

namespace com.ties
{
	/// <summary>
	/// Summary description for Conveyance.
	/// </summary>
	public class Conveyance : com.common.applicationpages.BasePage
	{
		protected System.Web.UI.WebControls.Label lblErrorMessage;
		protected System.Web.UI.WebControls.ValidationSummary Pagevalid;
		protected System.Web.UI.WebControls.Button btnInsert;
		protected System.Web.UI.WebControls.Button btnExecuteQuery;
		protected System.Web.UI.WebControls.Button btnQuery;
		protected System.Web.UI.WebControls.Label lblTitle;
		protected System.Web.UI.WebControls.DataGrid dgConveyance;
//		Utility utility=null;
		string m_strAppID=null;
		string m_strEnterpriseID=null;
		SessionDS m_sdsConveyance = null;

		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			m_strAppID = utility.GetAppID();
			m_strEnterpriseID = utility.GetEnterpriseID();
			if(!IsPostBack)
			{
				QueryMode();					  
			}
			else
			{
				m_sdsConveyance = (SessionDS) Session["SESSION_DS1"];
			}
			Pagevalid.HeaderText = Utility.GetLanguageText(ResourceType.UserMessage, "PLS_ENTER_MISS_FIELD", utility.GetUserCulture());
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			base.OnInit(e);
			InitializeComponent();			
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnInsert.Click += new System.EventHandler(this.btnInsert_Click);
			this.btnExecuteQuery.Click += new System.EventHandler(this.btnExecuteQuery_Click);
			this.btnQuery.Click += new System.EventHandler(this.btnQuery_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		public void BindConveyanceGrid()
		{
			dgConveyance.VirtualItemCount = System.Convert.ToInt32(m_sdsConveyance.QueryResultMaxSize);
			dgConveyance.DataSource = m_sdsConveyance.ds;
			dgConveyance.DataBind(); 
			Session["SESSION_DS1"] = m_sdsConveyance;
		}

		public void QueryMode()
		{
			ViewState["ConvOperation"]=Operation.None;
			ViewState["ConvMode"]=ScreenMode.Query;
			 	
			btnQuery.Enabled=true;
			btnExecuteQuery.Enabled=true;
			Utility.EnableButton(ref btnInsert,ButtonType.Insert,true,m_moduleAccessRights);

			m_sdsConveyance = SysDataMgrDAL.GetEmptyConveyance() ;  

			m_sdsConveyance.DataSetRecSize = 1;
			AddRow(); 
			Session["SESSION_DS1"] = m_sdsConveyance;
			BindConveyanceGrid();
			lblErrorMessage.Text="";
			
			EditHRow(true);
			dgConveyance.Columns[0].Visible=false;
			dgConveyance.Columns[1].Visible=false;
		}

		private void AddRow()
		{
			SysDataMgrDAL.AddNewRowInConveyanceDS(ref m_sdsConveyance);
			BindConveyanceGrid();
		}

		private void EditHRow(bool bItemIndex)
		{
			foreach (DataGridItem item in dgConveyance.Items)
			{ 
				if (bItemIndex) 
					dgConveyance.EditItemIndex = item.ItemIndex; 
				else 
					dgConveyance.EditItemIndex = -1; 
			}
			dgConveyance.DataBind();
		}

		private void GetChangedRow(DataGridItem item, int rowIndex)
		{
			msTextBox txtConveyance = (msTextBox)item.FindControl("txtConveyance");
			TextBox txtCCDescp = (TextBox)item.FindControl("txtCCDescp");
			msTextBox txtLength = (msTextBox)item.FindControl("txtLength");
			msTextBox txtBreadth = (msTextBox)item.FindControl("txtBreadth");
			msTextBox txtHeight = (msTextBox)item.FindControl("txtHeight");
			msTextBox txtWeight = (msTextBox)item.FindControl("txtWeight");
				
			string strConveyance = txtConveyance.Text.ToString();
			string strCCDescp = txtCCDescp.Text.ToString();
			string strLength = txtLength.Text.ToString();
			string strBreadth = txtBreadth.Text.ToString();
			string strHeight = txtHeight.Text.ToString();
			string strWeight = txtWeight.Text.ToString();
						
			DataRow dr = m_sdsConveyance.ds.Tables[0].Rows[rowIndex];
			dr[0] = strConveyance;
			dr[1] = strCCDescp;
			dr[2] = strLength;
			dr[3] = strBreadth;
			dr[4] = strHeight;			
			dr[5] = strWeight;

			Session["SESSION_DS1"] = m_sdsConveyance;
			
		}

		protected void  dgConveyance_Edit(object sender, DataGridCommandEventArgs e)
		{		
			lblErrorMessage.Text = "";
			if((int)ViewState["ConvMode"] == (int)ScreenMode.Insert && (int) ViewState["ConvOperation"] == (int)Operation.Insert &&  dgConveyance.EditItemIndex > 0)
			{
				m_sdsConveyance.ds.Tables[0].Rows.RemoveAt(dgConveyance.EditItemIndex);		
				dgConveyance.CurrentPageIndex = 0;
			}
			dgConveyance.EditItemIndex = e.Item.ItemIndex;
			ViewState["ConvOperation"] = Operation.Update;
			BindConveyanceGrid();
		}
/*---*/
		private void btnQuery_Click(object sender, System.EventArgs e)
		{
			lblErrorMessage.Text = "";
			dgConveyance.CurrentPageIndex = 0;
			QueryMode();
		}

		private void btnInsert_Click(object sender, System.EventArgs e)
		{			
			lblErrorMessage.Text = "";
			if (btnExecuteQuery.Enabled == true)
				btnExecuteQuery.Enabled = false;

			if((int)ViewState["ConvMode"] != (int)ScreenMode.Insert || m_sdsConveyance.ds.Tables[0].Rows.Count >= dgConveyance.PageSize)
			{
				m_sdsConveyance = SysDataMgrDAL.GetEmptyConveyance();
				dgConveyance.EditItemIndex = m_sdsConveyance.ds.Tables[0].Rows.Count - 1;
				dgConveyance.CurrentPageIndex = 0;
				AddRow();				
			}
			else
			{
				AddRow();
			}
			Utility.EnableButton(ref btnInsert,ButtonType.Insert,false,m_moduleAccessRights);
			EditHRow(false);
			ViewState["ConvMode"] = ScreenMode.Insert;
			ViewState["ConvOperation"] = Operation.Insert;
			dgConveyance.Columns[0].Visible=true;
			dgConveyance.Columns[1].Visible=true;
			dgConveyance.EditItemIndex = m_sdsConveyance.ds.Tables[0].Rows.Count - 1;
			dgConveyance.CurrentPageIndex = 0;
			BindConveyanceGrid();
			getPageControls(Page);
		}
		
		protected void dgConveyance_ItemDataBound(object sender, DataGridItemEventArgs e)
		{
			if(e.Item.ItemIndex == -1)
				return;

			DataRow drSelected = m_sdsConveyance.ds.Tables[0].Rows[e.Item.ItemIndex];
			if(drSelected.RowState == DataRowState.Deleted)
				return;
			
			if ((int)ViewState["ConvOperation"] != (int)Operation.Insert && (!btnExecuteQuery.Enabled))
			{
				msTextBox txtConveyance = (msTextBox)e.Item.FindControl("txtConveyance");
				if(txtConveyance != null ) 
					txtConveyance.Enabled = false;
			}
			if ((int)ViewState["ConvMode"] == (int)ScreenMode.Insert && (int)ViewState["ConvOperation"] == (int)Operation.Insert)
				e.Item.Cells[1].Enabled=false;

			if((int)ViewState["ConvMode"]==(int)ScreenMode.Insert)
				e.Item.Cells[1].Enabled = false;	
			else
				e.Item.Cells[1].Enabled = true;
		}

		protected void dgConveyance_Cancel(object sender, DataGridCommandEventArgs e)
		{
			lblErrorMessage.Text = "";
			int rowIndex = e.Item.ItemIndex;
			if ((int)ViewState["ConvMode"] == (int)ScreenMode.Insert && (int)ViewState["ConvOperation"] == (int)Operation.Insert)
			{
				Utility.EnableButton(ref btnInsert,ButtonType.Insert,true,m_moduleAccessRights);
				m_sdsConveyance.ds.Tables[0].Rows.RemoveAt(rowIndex);				
			}
			ViewState["ConvOperation"] = Operation.None;
			dgConveyance.EditItemIndex = -1;
			BindConveyanceGrid();			
		}

		protected void dgConveyance_PageChange(object sender, DataGridPageChangedEventArgs e)
		{
			lblErrorMessage.Text = "";
			dgConveyance.CurrentPageIndex = e.NewPageIndex;
			dgConveyance.SelectedIndex = -1;
			dgConveyance.EditItemIndex = -1;
			RetreiveSelectedPage();
		}

		private void RetreiveSelectedPage()
		{
			int iStartRow = dgConveyance.CurrentPageIndex * dgConveyance.PageSize;
			DataSet dsQuery = (DataSet)Session["QUERY_DS"];
			
			m_sdsConveyance = SysDataMgrDAL.GetConveyanceDS(utility.GetAppID(), utility.GetEnterpriseID(), iStartRow, dgConveyance.PageSize, dsQuery);
			int iPageCnt = Convert.ToInt32((m_sdsConveyance.QueryResultMaxSize - 1))/dgConveyance.PageSize;
			if(iPageCnt < dgConveyance .CurrentPageIndex)
			{
				dgConveyance.CurrentPageIndex = System.Convert.ToInt32(iPageCnt);
			}
			BindConveyanceGrid();
			Session["SESSION_DS1"] = m_sdsConveyance;
		}

		protected void dgConveyance_Delete(object sender, DataGridCommandEventArgs e)
		{
			if (btnExecuteQuery.Enabled)
				return;

			if ((int)ViewState["ConvOperation"] == (int)Operation.Update)
			{
				dgConveyance.EditItemIndex = -1;
			}
			BindConveyanceGrid();

			int rowIndex = e.Item.ItemIndex;
			m_sdsConveyance  = (SessionDS)Session["SESSION_DS1"];
			DataRow dr =m_sdsConveyance.ds.Tables[0].Rows[rowIndex];
			String strConveyance =(string) dr[0];
			
			if (strConveyance==null || strConveyance =="")			
				return;
		
			try
			{
				// delete from table
				int iRowsDeleted = SysDataMgrDAL.DeleteConveyance(m_strAppID, m_strEnterpriseID, strConveyance);
				ArrayList paramValues = new ArrayList();
				paramValues.Add(iRowsDeleted.ToString());
				lblErrorMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"DLD_SUCCESSFULLY",utility.GetUserCulture(),paramValues);
			}
			catch(ApplicationException appException)
			{
				String strMsg = appException.Message;
				if(strMsg.IndexOf("FK") != -1)
				{
					lblErrorMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"CHILD_FOUND_CANNOT_DEL",utility.GetUserCulture());					
				}
				if(strMsg.ToLower().IndexOf("child record found") != -1 )
				{
					lblErrorMessage.Text =  Utility.GetLanguageText(ResourceType.UserMessage,"CHILD_FOUND_CANNOT_DEL",utility.GetUserCulture());				
				}				

				Logger.LogTraceError("Conveyance.aspx.cs","dgConveyance_Delete","RBAC003",appException.Message.ToString());
				return;
			}

			if((int)ViewState["ConvMode"] == (int)ScreenMode.Insert)
				m_sdsConveyance.ds.Tables[0].Rows.RemoveAt(rowIndex);
			else
			{
				RetreiveSelectedPage();
			}
			ViewState["ConvOperation"] = Operation.None;
			//Make the row in non-edit Mode
			EditHRow(false);
			BindConveyanceGrid();
			Logger.LogDebugInfo("Conveyance","dgConveyance_Delete","INF004","updating data grid...");			
			
		}

		private void btnExecuteQuery_Click(object sender, System.EventArgs e)
		{
			lblErrorMessage.Text = "";
			btnExecuteQuery.Enabled = false;
			ViewState["ConvMode"]=ScreenMode.ExecuteQuery; 
			//Since Query mode is always one row in edit mode, the rowindex is set to zero
			int rowIndex = 0; 
			GetChangedRow(dgConveyance.Items[0], rowIndex);
			Session["QUERY_DS"] = m_sdsConveyance.ds;
			RetreiveSelectedPage();
			if(m_sdsConveyance.ds.Tables[0].Rows.Count==0)
			{
				lblErrorMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"NO_RECORD_FOUND",utility.GetUserCulture());
				dgConveyance.EditItemIndex = -1; 
				return;
			}
			//set all records to non-edit mode after execute query
			EditHRow(false);
			dgConveyance.Columns[0].Visible=true;
			dgConveyance.Columns[1].Visible=true;
		}

		protected void dgConveyance_Update(object sender, DataGridCommandEventArgs e)
		{
			lblErrorMessage.Text = "";
			if (btnExecuteQuery.Enabled)
			{
				return;
			}
			int iRowsAffected = 0;
			int rowIndex = e.Item.ItemIndex;
			GetChangedRow(e.Item, e.Item.ItemIndex);
		
			int iOperation = (int)ViewState["ConvOperation"];
			try
			{
				if (iOperation == (int)Operation.Insert)
				{
					iRowsAffected = SysDataMgrDAL.InsertConveyance(m_sdsConveyance.ds, rowIndex, m_strAppID, m_strEnterpriseID);
					ArrayList paramValues = new ArrayList();
					paramValues.Add(iRowsAffected.ToString());
					lblErrorMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"ROWS_INS",utility.GetUserCulture(),paramValues);;
				}
				else
				{
					DataSet dsChangedRow = m_sdsConveyance.ds.GetChanges();
					iRowsAffected = SysDataMgrDAL.UpdateConveyance(dsChangedRow,m_strAppID, m_strEnterpriseID);
					ArrayList paramValues = new ArrayList();
					paramValues.Add(iRowsAffected.ToString());
					lblErrorMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"ROWS_UPD",utility.GetUserCulture(),paramValues);;
					m_sdsConveyance.ds.Tables[0].Rows[e.Item.ItemIndex].AcceptChanges();
				}
			}
			catch(ApplicationException appException)
			{
				String strMsg = appException.Message;
				strMsg = appException.InnerException.Message;
				strMsg = appException.InnerException.InnerException.Message;
				if(strMsg.IndexOf("PRIMARY KEY") != -1)
					lblErrorMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"NO_DUPLICATE",utility.GetUserCulture());
				else if(strMsg.IndexOf("duplicate key") != -1 )
				{
					lblErrorMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"NO_DUPLICATE",utility.GetUserCulture());				
				}
				
				Logger.LogTraceError("DeliveryPathCode.aspx.cs","dgState_Update","RBAC003",appException.Message.ToString());
				return;
			}

			if (iRowsAffected == 0)
			{
				return;
			}
			Utility.EnableButton(ref btnInsert,ButtonType.Insert,true,m_moduleAccessRights);
			ViewState["ConvOperation"] = Operation.None;
			dgConveyance.EditItemIndex = -1;
			BindConveyanceGrid();
			Logger.LogDebugInfo("State","dgDelvPath_Update","INF004","updating data grid...");			
			
		}
	}
}
