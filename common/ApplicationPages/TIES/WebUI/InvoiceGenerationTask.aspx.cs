using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.common.util;
using com.ties.DAL;
using com.common.classes;
using com.common.applicationpages;

namespace com.ties
{
	/// <summary>
	/// Summary description for InvoiceGenerationTask.
	/// </summary>
	public class InvoiceGenerationTask : BasePage
	{
		protected System.Web.UI.WebControls.Button btnQuery;
		protected System.Web.UI.WebControls.Button btnExecuteQuery;
		protected System.Web.UI.WebControls.Label lblTitle;
		protected System.Web.UI.WebControls.Label lblErrorMessage;
		protected System.Web.UI.WebControls.DataGrid dgParent;
		//Utility utility = null;
		private String m_strAppID;
		private String m_strEnterpriseID;
		SessionDS m_sdsParent = null;
		DataView m_dvJobStatus = null;

		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			//utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings,Page.Session);
			m_strAppID = utility.GetAppID();
			m_strEnterpriseID = utility.GetEnterpriseID();

			if (!Page.IsPostBack)
			{
				QueryMode();
			}
			else
			{
				m_sdsParent = (SessionDS) Session["SESSION_DS1"];
				if (btnExecuteQuery.Enabled == false)
                    LoadComboLists(true);
				else
					LoadComboLists(false);
			}
			lblErrorMessage.Text="";
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			base.OnInit(e);
			InitializeComponent();
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnQuery.Click += new System.EventHandler(this.btnQuery_Click);
			this.btnExecuteQuery.Click += new System.EventHandler(this.btnExecuteQuery_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		protected void dgParent_Delete(object sender, DataGridCommandEventArgs e)
		{
			if (!btnExecuteQuery.Enabled)
			{
				BindGrid();

				int rowIndex = e.Item.ItemIndex;
				m_sdsParent = (SessionDS)Session["SESSION_DS1"];

				try
				{
					// delete from table
					int iRowsDeleted = InvoiceJobMgrDAL.DeleteInvoiceTask(m_sdsParent.ds, rowIndex, m_strAppID, m_strEnterpriseID);
					lblErrorMessage.Text = iRowsDeleted + " row(s) deleted.";
				}
				catch(ApplicationException appException)
				{
					String strMsg = appException.Message;
					if(strMsg.IndexOf("FK") != -1)
					{
						lblErrorMessage.Text = "Error Deleting zip code. Zip Code being used in some transaction(s)";
					}
					else
					{
						lblErrorMessage.Text = strMsg;
					}

					Logger.LogTraceError("InvoiceGenerationTasx.aspx.cs","dgParent_Delete","RBAC003",appException.Message.ToString());
					return;
				}

				RetreiveSelectedPage();
				
				//Make the row in non-edit Mode
				EditHRow(false);
			
				BindGrid();
				Logger.LogDebugInfo("InvoiceGenerationTasx.aspx.cs","dgParent_Delete","INF004","updating data grid...");			
			}
		}

		protected void dgParent_Bound(object sender, DataGridItemEventArgs e)
		{
			if(e.Item.ItemIndex == -1)
			{
				return;
			}

			DataRow drSelected = m_sdsParent.ds.Tables[0].Rows[e.Item.ItemIndex];
			if(drSelected.RowState == DataRowState.Deleted)
			{
				return;
			}

			DropDownList ddlJobStatus = (DropDownList)e.Item.FindControl("ddlJobStatus");			
			if(ddlJobStatus != null)
			{
				ddlJobStatus.DataSource = LoadJobStatus();
				ddlJobStatus.DataTextField = "Text";
				ddlJobStatus.DataValueField = "StringValue";
				ddlJobStatus.DataBind();
				if(Utility.IsNotDBNull(drSelected["job_status"]))
				{							
					String strJobStatus = (String)drSelected["job_status"];
					ddlJobStatus.SelectedIndex = ddlJobStatus.Items.IndexOf(ddlJobStatus.Items.FindByValue(strJobStatus));//.FindByValue("N"));
					Logger.LogDebugInfo("InvoiceGenerationTask.aspx.cs","dgParent_Bound","INF006","Selecting Job Status......"+e.Item.ItemIndex+" Selected Index is : "+ddlJobStatus.SelectedIndex+"   "+strJobStatus);
				}
				if (btnExecuteQuery.Enabled == false)
					ddlJobStatus.Enabled = false;
				else
					ddlJobStatus.Enabled = true;
			}

		}

		protected void dgParent_PageChange(object sender, DataGridPageChangedEventArgs e)
		{
			dgParent.CurrentPageIndex = e.NewPageIndex;
			RetreiveSelectedPage();
			Logger.LogDebugInfo("InvoiceGenerationTasx.aspx.cs","dgParent_Delete","INF009","On Page Change "+e.NewPageIndex);
		}

		private void btnQuery_Click(object sender, System.EventArgs e)
		{
			lblErrorMessage.Text = "";
			dgParent.CurrentPageIndex = 0;
			QueryMode();
		}

		private void btnExecuteQuery_Click(object sender, System.EventArgs e)
		{
			//Since Query mode is always one row in edit mode, the rowindex is set to zero
			int rowIndex = 0; 

			//load drop down list
			LoadComboLists(false);

			GetChangedRow(dgParent.Items[0], rowIndex);
			Session["QUERY_DS"] = m_sdsParent.ds;

			RetreiveSelectedPage();

			lblErrorMessage.Text = "";
			btnExecuteQuery.Enabled = false;

			//set all records to non-edit mode after execute query
			EditHRow(false);
		}

		private void BindGrid()
		{
			dgParent.VirtualItemCount = System.Convert.ToInt32(m_sdsParent.QueryResultMaxSize);
			dgParent.DataSource = m_sdsParent.ds;
			dgParent.DataBind();
			Session["SESSION_DS1"] = m_sdsParent;
		}

		private void EditHRow(bool bItemIndex)
		{
			foreach (DataGridItem item in dgParent.Items)
			{ 
				if (bItemIndex) 
				{
					dgParent.EditItemIndex = item.ItemIndex; }
				else 
				{
					dgParent.EditItemIndex = -1; }
			}
			dgParent.DataBind();
		}

		private void QueryMode()
		{
			//load drop down list
			LoadComboLists(true);

			//Set Button Enable Properties
			btnQuery.Enabled=true;
			btnExecuteQuery.Enabled=true;

			//inset an empty row for query 
			m_sdsParent = InvoiceJobMgrDAL.GetEmptyInvoiceJob();
			m_sdsParent.DataSetRecSize = 1;
			Session["SESSION_DS1"] = m_sdsParent;
			BindGrid();

			//Make the row in Edit Mode
			EditHRow(true);
		}

		private void GetChangedRow(DataGridItem item, int rowIndex)
		{
			msTextBox txtJobID = (msTextBox)item.FindControl("txtJobID");
			msTextBox txtStartDateTime = (msTextBox)item.FindControl("txtStartDateTime");
			msTextBox txtEndDateTime = (msTextBox)item.FindControl("txtEndDateTime");
			DropDownList ddlJobStatus = (DropDownList)item.FindControl("ddlJobStatus");
			TextBox txtRemark = (TextBox)item.FindControl("txtRemark");

			int intJobID = 0; //txtTaskID.Text.ToString();
			string strStartDateTime = txtStartDateTime.Text.ToString();
			string strEndDateTime = txtEndDateTime.Text.ToString();
			string strJobStatus = null;
			string strRemark = txtRemark.Text.ToString();

			if (Utility.IsNotDBNull(txtJobID.Text) && (txtJobID.Text != ""))
				intJobID = Convert.ToInt32(txtJobID.Text);

			DataRow dr = m_sdsParent.ds.Tables[0].Rows[rowIndex];
			dr["jobid"] = intJobID;
			if ((strStartDateTime.ToString() != null) && (strStartDateTime.ToString() != ""))
				dr["start_datetime"] = System.DateTime.ParseExact(strStartDateTime.Trim()+" 00:00", "dd/MM/yyyy hh:mm", null);

			if ((strEndDateTime.ToString() != null) && (strEndDateTime.ToString() != ""))
				dr["end_datetime"] = System.DateTime.ParseExact(strEndDateTime.Trim()+" 00:00", "dd/MM/yyyy hh:mm", null);

			if(ddlJobStatus != null)
			{
				strJobStatus = ddlJobStatus.SelectedItem.Value;
				dr["job_status"] = strJobStatus;
			}

			dr["remark"] = strRemark;
		}

		private void RetreiveSelectedPage()
		{
			int iStartRow = dgParent.CurrentPageIndex * dgParent.PageSize;
			DataSet dsQuery = (DataSet)Session["QUERY_DS"];
			m_sdsParent = InvoiceJobMgrDAL.GetInvoiceJobDS(m_strAppID, m_strEnterpriseID, iStartRow, dgParent.PageSize, dsQuery);
			decimal iPageCnt = (m_sdsParent.QueryResultMaxSize - 1)/dgParent.PageSize;
			if(iPageCnt < dgParent.CurrentPageIndex)
			{
				dgParent.CurrentPageIndex = System.Convert.ToInt32(iPageCnt);
			}
			BindGrid();
			Session["SESSION_DS1"] = m_sdsParent;
		}

		protected ICollection LoadJobStatus()
		{
			return m_dvJobStatus;
		}

		private void LoadComboLists(bool bNilValue)
		{
			DataTable dtJobStatus = new DataTable();
 			dtJobStatus.Columns.Add(new DataColumn("Text", typeof(string)));
			dtJobStatus.Columns.Add(new DataColumn("StringValue", typeof(string)));

			ArrayList jobStatusArray = Utility.GetCodeValues(m_strAppID,utility.GetUserCulture(),"job_status_type",CodeValueType.StringValue);
			if(bNilValue)
			{
				DataRow drNilRow = dtJobStatus.NewRow();
				drNilRow[0] = "";
				drNilRow[1] = "";
				dtJobStatus.Rows.Add(drNilRow);
			}
			if(jobStatusArray != null)
			{
				foreach(SystemCode jobStatusSysCode in jobStatusArray)
				{
					DataRow drEach = dtJobStatus.NewRow();
					drEach[0] = jobStatusSysCode.Text;
					drEach[1] = jobStatusSysCode.StringValue;
					dtJobStatus.Rows.Add(drEach);
				}
			}
			m_dvJobStatus = new DataView(dtJobStatus);
			return;
		}

	}
}
