using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.common.util;
using com.ties.DAL;
using com.common.classes;
using com.common.applicationpages;

namespace com.ties
{
	/// <summary>
	/// Summary description for Zone.
	/// </summary>
	public class BaseServiceAvailable : BasePage
	{
		protected System.Web.UI.WebControls.Button btnExecuteQuery;
		protected System.Web.UI.WebControls.Button btnInsert;
		protected System.Web.UI.WebControls.Button btnQuery;

		//Utility utility = null;
		private String m_strAppID;
		private String m_strEnterpriseID;
		SessionDS m_sdsZone = null;
		protected System.Web.UI.WebControls.Label lblErrorMessage;
		protected System.Web.UI.WebControls.ValidationSummary ValidationSummary1;
		protected System.Web.UI.WebControls.DataGrid dgBestService;
		protected System.Web.UI.WebControls.Label lblTitle;

		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			//utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings,Page.Session);
			m_strAppID = utility.GetAppID();
			m_strEnterpriseID = utility.GetEnterpriseID();

			if (!Page.IsPostBack)
			{
				QueryMode();
			}
			else
			{
				m_sdsZone = (SessionDS)Session["SESSION_DS1"];
			}
			lblErrorMessage.Text = "";
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			base.OnInit(e);
			InitializeComponent();
		
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnQuery.Click += new System.EventHandler(this.btnQuery_Click);
			this.btnExecuteQuery.Click += new System.EventHandler(this.btnExecuteQuery_Click);
			this.btnInsert.Click += new System.EventHandler(this.btnInsert_Click);
			this.dgBestService.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.dgBestService_ItemDataBound);
			this.dgBestService.SelectedIndexChanged += new System.EventHandler(this.dgBestService_SelectedIndexChanged);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		protected void dgBestService_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			Logger.LogDebugInfo("Zone","dgBestService_SelectedIndexChanged","INF004","updating data grid...");
		}

		protected void dgBestService_Update(object sender, DataGridCommandEventArgs e)
		{
			lblErrorMessage.Text = "";
			if (!btnExecuteQuery.Enabled)
			{
				int iRowsAffected = 0;
				int iRowsZipcode = 0;
				int iRowsOriginCode = 0;
				int rowIndex = e.Item.ItemIndex;
				GetChangedRow(e.Item, e.Item.ItemIndex);

				//int iMode = (int)ViewState["Mode"];
				int iOperation = (int)ViewState["Operation"];				
				
				iRowsOriginCode = SysDataManager2.VerifyOriginCode(m_sdsZone.ds, rowIndex, m_strAppID, m_strEnterpriseID);
				iRowsZipcode = SysDataManager2.VerifyZipcode(m_sdsZone.ds, rowIndex, m_strAppID, m_strEnterpriseID);
				if(iRowsOriginCode <= 0)
				{
					lblErrorMessage.Text=Utility.GetLanguageText(ResourceType.UserMessage,"INVALID_ORIGINCODE",utility.GetUserCulture());
					if(iRowsZipcode <= 0)
					{
						lblErrorMessage.Text+= "<br>";
						lblErrorMessage.Text+=Utility.GetLanguageText(ResourceType.UserMessage,"NO_RECIP_ZIP",utility.GetUserCulture());
					}
					return;
				}
				else
				{
					if(iRowsZipcode <= 0)
					{
						lblErrorMessage.Text=Utility.GetLanguageText(ResourceType.UserMessage,"NO_RECIP_ZIP",utility.GetUserCulture());
						return;
					}					
				}

				try
				{
					if (iOperation == (int)Operation.Insert)
					{	
						iRowsAffected = SysDataManager2.InsertBestService(m_sdsZone.ds, rowIndex, m_strAppID, m_strEnterpriseID);
						ArrayList paramValues = new ArrayList();
						paramValues.Add(""+iRowsAffected);						
						lblErrorMessage.Text=Utility.GetLanguageText(ResourceType.UserMessage,"ROWS_INS",utility.GetUserCulture(),paramValues);
					}
					else
					{
						DataSet dsChangedRow = m_sdsZone.ds.GetChanges();
						iRowsAffected = SysDataManager2.UpdateBestService(dsChangedRow, m_strAppID, m_strEnterpriseID);
						ArrayList paramValues = new ArrayList();
						paramValues.Add(""+iRowsAffected);						
						lblErrorMessage.Text=Utility.GetLanguageText(ResourceType.UserMessage,"ROWS_UPD",utility.GetUserCulture(),paramValues);
						m_sdsZone.ds.Tables[0].Rows[e.Item.ItemIndex].AcceptChanges();
					}
				}
				catch(ApplicationException appException)
				{
					String strMsg = appException.Message;
					strMsg = appException.InnerException.Message;
					strMsg = appException.InnerException.InnerException.Message;
					if(strMsg.IndexOf("duplicate key") != -1 || strMsg.IndexOf("PRIMARY KEY") != -1 )
					{
						lblErrorMessage.Text =  Utility.GetLanguageText(ResourceType.UserMessage,"DUP_KEY_FOUND_BESTSERVICE",utility.GetUserCulture());
							
					}
					if(strMsg.IndexOf("unique") != -1 )
					{
						lblErrorMessage.Text =  Utility.GetLanguageText(ResourceType.UserMessage,"DUP_KEY_FOUND_BESTSERVICE",utility.GetUserCulture());
							
					}				

					Logger.LogTraceError("Zone.aspx.cs","dgBestService_Update","RBAC003",appException.Message.ToString());
					//lblErrorMessage.Text = appException.Message.ToString();
					return;
				}

				if (iRowsAffected == 0)
				{
					return;
				}
				Utility.EnableButton(ref btnInsert,ButtonType.Insert,true,m_moduleAccessRights);
				ViewState["Operation"] = Operation.None;
				dgBestService.EditItemIndex = -1;
				BindGrid();
				Logger.LogDebugInfo("Zone","dgBestService_Update","INF004","updating data grid...");			
			}
		}

		protected void dgBestService_Cancel(object sender, DataGridCommandEventArgs e)
		{
			lblErrorMessage.Text = "";
			//int iMode = (int)ViewState["Mode"];
			int rowIndex = e.Item.ItemIndex;
			if ((int)ViewState["Mode"] == (int)ScreenMode.Insert && (int)ViewState["Operation"] == (int)Operation.Insert)
			{
				Utility.EnableButton(ref btnInsert,ButtonType.Insert,true,m_moduleAccessRights);
				m_sdsZone.ds.Tables[0].Rows.RemoveAt(rowIndex);
				//ViewState["Mode"] = ScreenMode.None;
			}
			ViewState["Operation"] = Operation.None;
			dgBestService.EditItemIndex = -1;
			BindGrid();
			Logger.LogDebugInfo("Zone","dgBestService_Cancel","INF004","updating data grid...");			
		}

		protected void dgBestService_Edit(object sender, DataGridCommandEventArgs e)
		{
			if((int)ViewState["Mode"] == (int)ScreenMode.Insert && (int) ViewState["Operation"] == (int)Operation.Insert && dgBestService.EditItemIndex > 0)
			{
				m_sdsZone.ds.Tables[0].Rows.RemoveAt(dgBestService.EditItemIndex);
				m_sdsZone.QueryResultMaxSize--;
				dgBestService.CurrentPageIndex = 0;
			}
			dgBestService.EditItemIndex = e.Item.ItemIndex;
			ViewState["Operation"] = Operation.Update;
			BindGrid();
			Logger.LogDebugInfo("Zone","dgBestService_Edit","INF004","updating data grid...");			
		}

		protected void dgBestService_Delete(object sender, DataGridCommandEventArgs e)
		{
			if (!btnExecuteQuery.Enabled)
			{
				if ((int)ViewState["Operation"] == (int)Operation.Update)
				{
					dgBestService.EditItemIndex = -1;
				}
				BindGrid();

				int rowIndex = e.Item.ItemIndex;
				m_sdsZone = (SessionDS)Session["SESSION_DS1"];

				try
				{
					// delete from table
					int iRowsDeleted = SysDataManager2.DeleteBestService(m_sdsZone.ds, rowIndex, m_strAppID, m_strEnterpriseID);
					ArrayList paramValues = new ArrayList();
					paramValues.Add(""+iRowsDeleted);						
					lblErrorMessage.Text=Utility.GetLanguageText(ResourceType.UserMessage,"DELETED_SUCCESSFULLY",utility.GetUserCulture(),paramValues);
				}
				catch(ApplicationException appException)
				{
					String strMsg = appException.Message;
					if(strMsg.IndexOf("FK") != -1)
					{
						lblErrorMessage.Text =  Utility.GetLanguageText(ResourceType.UserMessage,"ERR_DEL_ZONE_TRANS",utility.GetUserCulture());
					}
					if(strMsg.ToLower().IndexOf("child record found") != -1 || strMsg.IndexOf("REFERENCE") !=-1)
					{
						lblErrorMessage.Text =  Utility.GetLanguageText(ResourceType.UserMessage,"ERR_DEL_ZONE_TRANS",utility.GetUserCulture());
					}
					else
					{
						lblErrorMessage.Text = strMsg;
					}

					Logger.LogTraceError("Zone.aspx.cs","dgBestService_Delete","RBAC003",appException.Message.ToString());
					return;
				}

				// remove the data from grid

				if((int)ViewState["Mode"] == (int)ScreenMode.Insert)
				{
					m_sdsZone.ds.Tables[0].Rows.RemoveAt(rowIndex);
				}
				else
				{
					RetreiveSelectedPage();
				}

				//Set ViewStates to None
				ViewState["Operation"] = Operation.None;

				//Make the row in non-edit Mode
				EditRow(false);

				BindGrid();
				Logger.LogDebugInfo("Zone","dgBestService_Delete","INF004","updating data grid...");			
			}
		}

		protected void dgBestService_Bound(object sender, DataGridItemEventArgs e)
		{
			if(e.Item.ItemIndex == -1)
			{
				return;
			}

			DataRow drSelected = m_sdsZone.ds.Tables[0].Rows[e.Item.ItemIndex];
			if(drSelected.RowState == DataRowState.Deleted)
			{
				return;
			}

			if ((int)ViewState["Operation"] != (int)Operation.Insert && (!btnExecuteQuery.Enabled))
			{
				msTextBox txtZoneCode = (msTextBox)e.Item.FindControl("txtZoneCode");
				if(txtZoneCode != null) 
				{
					txtZoneCode.Enabled = false;
				}
			}
			if ((int)ViewState["Mode"] == (int)ScreenMode.Insert && (int)ViewState["Operation"] == (int)Operation.Insert)
			{
				e.Item.Cells[1].Enabled=false;
			}
			TextBox txtZRecipPostCode = (TextBox)e.Item.FindControl("txtZRecipPostCode");
			TextBox txtOriginDC = (TextBox)e.Item.FindControl("txtOriginDC");
			int iOperation = (int)ViewState["Operation"];
			if (txtOriginDC != null && iOperation == (int)Operation.Update)
			{
				txtZRecipPostCode.Enabled = false;
				txtOriginDC.Enabled = false;
			}
		}

		protected void dgBestService_PageChange(object sender, DataGridPageChangedEventArgs e)
		{
			dgBestService.CurrentPageIndex = e.NewPageIndex;
			RetreiveSelectedPage();
			Logger.LogDebugInfo("BestServiceAvailable","dgBestService_PageChange","INF009","On Page Change "+e.NewPageIndex);
		}

		private void btnQuery_Click(object sender, System.EventArgs e)
		{
			lblErrorMessage.Text = "";
			dgBestService.CurrentPageIndex = 0;
			QueryMode();
		}

		protected void btnExecuteQuery_Click(object sender, System.EventArgs e)
		{
			lblErrorMessage.Text = "";
			btnExecuteQuery.Enabled = false;

			//Since Query mode is always one row in edit mode, the rowindex is set to zero
			int rowIndex = 0; 

			GetChangedRow(dgBestService.Items[0], rowIndex);
			Session["QUERY_DS"] = m_sdsZone.ds;

			RetreiveSelectedPage();

			//set all records to non-edit mode after execute query
			EditRow(false);
			dgBestService.Columns[0].Visible=true;
			dgBestService.Columns[1].Visible=true;
		}

		private void btnInsert_Click(object sender, System.EventArgs e)
		{
			//clear the rows from the datagrid
			//m_sdsZone.ds.Clear();
			if (btnExecuteQuery.Enabled == true)
			{
				btnExecuteQuery.Enabled = false;
			}
			if((int)ViewState["Mode"] != (int)ScreenMode.Insert || m_sdsZone.ds.Tables[0].Rows.Count >= dgBestService.PageSize)
			{
				m_sdsZone = SysDataManager2.GetEmptyPostCodeDS();
				//m_sdsZone.ds.Clear();
			}
			else
			{
				AddRow();
			}

			Utility.EnableButton(ref btnInsert,ButtonType.Insert,false,m_moduleAccessRights);
			EditRow(false);
			ViewState["Mode"] = ScreenMode.Insert;
			ViewState["Operation"] = Operation.Insert;
			dgBestService.Columns[0].Visible=true;
			dgBestService.Columns[1].Visible=true;
			//AddRow();
			dgBestService.EditItemIndex = m_sdsZone.ds.Tables[0].Rows.Count - 1;
			dgBestService.CurrentPageIndex = 0;
			BindGrid();
			getPageControls(Page);
			Logger.LogDebugInfo("Best Service","btnInsert_Click","INF003","Data Grid Items count"+dgBestService.Items.Count);
		}

		private void BindGrid()
		{
			dgBestService.VirtualItemCount = System.Convert.ToInt32(m_sdsZone.QueryResultMaxSize);
			dgBestService.DataSource = m_sdsZone.ds;
			dgBestService.DataBind();
			Session["SESSION_DS1"] = m_sdsZone;
		}

		private void EditRow(bool bItemIndex)
		{
			foreach (DataGridItem item in dgBestService.Items)
			{ 
				if (bItemIndex) 
				{
					dgBestService.EditItemIndex = item.ItemIndex; }
				else 
				{
					dgBestService.EditItemIndex = -1; }
			}
			dgBestService.DataBind();
		}
		#region Comment by Sompote 2010-05-07 (Fix Database Lock)
		/*
		private void QueryMode()
		{
			//Set ViewStates to None
			ViewState["Mode"] = ScreenMode.None;
			ViewState["Operation"] = Operation.None;

			//Set Button Enable Properties
			btnQuery.Enabled=true;
			btnExecuteQuery.Enabled=true;
			Utility.EnableButton(ref btnInsert,ButtonType.Insert,true,m_moduleAccessRights);

			//inset an empty row for query 
			m_sdsZone = SysDataManager2.GetEmptyPostCodeDS();
			m_sdsZone.DataSetRecSize = 1;
			Session["SESSION_DS1"] = m_sdsZone;
			BindGrid();

			//Make the row in Edit Mode
			EditRow(true);
			dgBestService.Columns[0].Visible=false;
			dgBestService.Columns[1].Visible=false;

			DataSet dsServiceCode = LoadService();
		}

		*/
		#endregion
		private void QueryMode()
		{
			//Set ViewStates to None
			ViewState["Mode"] = ScreenMode.None;
			ViewState["Operation"] = Operation.None;

			//Set Button Enable Properties
			btnQuery.Enabled=true;
			btnExecuteQuery.Enabled=true;
			Utility.EnableButton(ref btnInsert,ButtonType.Insert,true,m_moduleAccessRights);

			//inset an empty row for query 
			m_sdsZone = SysDataManager2.GetEmptyPostCodeDS();
			m_sdsZone.DataSetRecSize = 1;
			Session["SESSION_DS1"] = m_sdsZone;
			BindGrid();

			//Make the row in Edit Mode
			EditRow(true);
			dgBestService.Columns[0].Visible=false;
			dgBestService.Columns[1].Visible=false;

			DataSet dsServiceCode = LoadService(-1);
		}

		private void AddRow()
		{
			SysDataManager2.AddNewRowInPostCodeDS(ref m_sdsZone);
			BindGrid();
		}

		private void GetChangedRow(DataGridItem item, int rowIndex)
		{
			msTextBox txtOriginDC = (msTextBox)item.FindControl("txtOriginDC");
			TextBox txtZRecipPostCode = (TextBox)item.FindControl("txtZRecipPostCode");
			DropDownList ddlBestService = (DropDownList)item.FindControl("ddlBestService");
				
			string strOriginDC = txtOriginDC.Text.ToString();
			string strRecipPostCode = txtZRecipPostCode.Text.ToString();
			string strBestService = ddlBestService.SelectedValue;

			DataRow dr = m_sdsZone.ds.Tables[0].Rows[rowIndex];
			dr[0] = strOriginDC;
			dr[1] = strRecipPostCode;
			dr[2] = strBestService;
		}
		#region Comment by Sompote 2010-05-07 (Fix Database Lock)
		/*
		private DataSet  LoadService()
		{			
			DataSet dsService = new DataSet();
			DataTable dtService = new DataTable();
			dtService.Columns.Add("ServiceText",typeof(string));
			dtService.Columns.Add("ServiceValue",typeof(string));
 
			ArrayList serviceCodes = Utility.GetServiceValues(m_strAppID, m_strEnterpriseID);
			DataRow dr; 
			//			DataRow dr =  dtDlvType.NewRow();
			//			dr[1] = System.DBNull.Value;
			//			dr[0] = System.DBNull.Value;
			//			dtDlvType.Rows.Add(dr); 
			if((int)ViewState["Mode"] != (int)ScreenMode.Insert)
			{
				dr = dtService.NewRow();
				dr[0] = "";
				dr[1] = "";
				dtService.Rows.Add(dr);
			}
			if(serviceCodes != null)
			{
				foreach(SystemCode serviceCode in serviceCodes)
				{
					dr = dtService.NewRow();
					dr[1] = serviceCode.Text;
					dr[0] = serviceCode.StringValue;
					dtService.Rows.Add(dr); 			
				}
			}
			dsService.Tables.Add(dtService);
			int cnt = dsService.Tables [0].Rows.Count; 
			return dsService;
		}

		*/
		#endregion
		private DataSet  LoadService(int rowIndex)
		{			
			DataSet dsService = new DataSet();
			DataTable dtService = new DataTable();
			dtService.Columns.Add("ServiceText",typeof(string));
			dtService.Columns.Add("ServiceValue",typeof(string));
 
			string strPostCode = "";
			
			if(rowIndex != -1)
			{
				DataRow rows = m_sdsZone.ds.Tables[0].Rows[rowIndex];
				strPostCode = (string)rows[1];
			}

			ArrayList serviceCodes = Utility.GetServiceValues(m_strAppID, m_strEnterpriseID,strPostCode);
			DataRow dr; 
			//			DataRow dr =  dtDlvType.NewRow();
			//			dr[1] = System.DBNull.Value;
			//			dr[0] = System.DBNull.Value;
			//			dtDlvType.Rows.Add(dr); 
			if((int)ViewState["Mode"] != (int)ScreenMode.Insert)
			{
				dr = dtService.NewRow();
				dr[0] = "";
				dr[1] = "";
				dtService.Rows.Add(dr);
			}
			if(serviceCodes != null)
			{
				foreach(SystemCode serviceCode in serviceCodes)
				{
					dr = dtService.NewRow();
					dr[1] = serviceCode.Text;
					dr[0] = serviceCode.StringValue;
					dtService.Rows.Add(dr); 			
				}
			}
			dsService.Tables.Add(dtService);
			int cnt = dsService.Tables [0].Rows.Count; 
			return dsService;
		}

		private void RetreiveSelectedPage()
		{
			int iStartRow = dgBestService.CurrentPageIndex * dgBestService.PageSize;
			DataSet dsQuery = (DataSet)Session["QUERY_DS"];
			m_sdsZone = SysDataManager2.GetBestServiceDS(utility.GetAppID(), utility.GetEnterpriseID(), iStartRow, dgBestService.PageSize, dsQuery);
			int iPageCnt = Convert.ToInt32((m_sdsZone.QueryResultMaxSize - 1))/dgBestService.PageSize;
			if(iPageCnt < dgBestService.CurrentPageIndex)
			{
				dgBestService.CurrentPageIndex = System.Convert.ToInt32(iPageCnt);
			}
			dgBestService.SelectedIndex = -1;
			dgBestService.EditItemIndex = -1;

			BindGrid();
			Session["SESSION_DS1"] = m_sdsZone;
		}
		#region Comment by Sompote 2010-05-07 
		/*
		private void dgBestService_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
		{
			DropDownList DrpDlvType = (DropDownList)e.Item.FindControl("ddlBestService");
						
			String strType=null;
			DataSet dsDlvType = LoadService();
			if (DrpDlvType!=null)
			{
				DrpDlvType.DataSource = dsDlvType;
				DrpDlvType.DataTextField = "ServiceValue"; 
				DrpDlvType.DataValueField="ServiceText";
				DrpDlvType.DataBind();
			}
			
			if(e.Item.ItemIndex != -1)
			{
				DataRow drSelected = m_sdsZone.ds.Tables[0].Rows[e.Item.ItemIndex];			
			   
				if (DrpDlvType!=null)
				{
					strType = (String)drSelected["service_code"];
					DrpDlvType.SelectedIndex = DrpDlvType.Items.IndexOf(DrpDlvType.Items.FindByValue(strType));	

				}
			}
			
		}

		*/
		#endregion
		private void dgBestService_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
		{
			DropDownList DrpDlvType = (DropDownList)e.Item.FindControl("ddlBestService");
						
			String strType=null;
			DataSet dsDlvType = LoadService(e.Item.ItemIndex);
			if (DrpDlvType!=null)
			{
				DrpDlvType.DataSource = dsDlvType;
				DrpDlvType.DataTextField = "ServiceValue"; 
				DrpDlvType.DataValueField="ServiceText";
				DrpDlvType.DataBind();
			}
			
			if(e.Item.ItemIndex != -1)
			{
				DataRow drSelected = m_sdsZone.ds.Tables[0].Rows[e.Item.ItemIndex];			
			   
				if (DrpDlvType!=null)
				{
					strType = (String)drSelected["service_code"];
					DrpDlvType.SelectedIndex = DrpDlvType.Items.IndexOf(DrpDlvType.Items.FindByValue(strType));	

				}
			}
			
		}
	}
}
