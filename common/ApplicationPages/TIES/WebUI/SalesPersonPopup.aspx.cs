using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Text;
using com.common.DAL;
using com.common.classes;
using com.common.util;
using com.ties.classes;
using com.common.applicationpages;

namespace TIES.WebUI
{
	/// <summary>
	/// Summary description for SalesPersonPopup.
	/// </summary>
	public class SalesPersonPopup : BasePopupPage
	{		
		private String appID = null;
		private String enterpriseID = null;
		SessionDS m_sdsSalesman =null;
		//Utility utility = null;
		string strFORMID=null;

		protected System.Web.UI.WebControls.Label SalesErrMsg;
		protected System.Web.UI.WebControls.Button btnSearch;
		protected System.Web.UI.WebControls.TextBox txtSalesManID;
		protected System.Web.UI.WebControls.Label lblSalesManID;
		protected System.Web.UI.WebControls.Button btnRefresh;
		protected System.Web.UI.WebControls.TextBox txtSalesManName;
		protected System.Web.UI.WebControls.DataGrid dgSalesman;
		protected System.Web.UI.WebControls.Label lblSalesManName;
		protected System.Web.UI.WebControls.Button btnClose;
		
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			base.OnInit(e);
			InitializeComponent();
			
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{   	
			this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
			this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
			this.dgSalesman.SelectedIndexChanged += new System.EventHandler(this.dgSalesman_SelectedIndexChanged);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private void Page_Load(object sender, System.EventArgs e)
		{			
			// Put user code to initialize the page here			
			//utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings,Page.Session);
			appID = utility.GetAppID();
			enterpriseID = utility.GetEnterpriseID();						
			strFORMID= Request.Params["FORMID"]; 
			if (!IsPostBack)
			{
				m_sdsSalesman = GetEmptySalesman(0); 
				txtSalesManID.Text = Request.Params["QUERY_SALESMANID"]; //Query string entered by USER...
				BindGrid();
			}
			else
			{
				m_sdsSalesman = (SessionDS)ViewState["Sales_DS"];
			}
			
			SalesErrMsg.Text="";
	   	
		}
		public static SessionDS GetEmptySalesman(int intRows)
		{
			DataTable dtSalesman = new DataTable();
			dtSalesman.Columns.Add(new DataColumn("SalesManID", typeof(string)));
			dtSalesman.Columns.Add(new DataColumn("SalesMan_Name", typeof(string)));
			dtSalesman.Columns.Add(new DataColumn("Designation", typeof(string)));
			
			for(int i=0; i < intRows; i++)
			{
				DataRow drEach = dtSalesman.NewRow();
				drEach[0] = "";
				drEach[1] = "";
				dtSalesman.Rows.Add(drEach);
			}
			DataSet dsSales = new DataSet();
			dsSales.Tables.Add(dtSalesman);
			SessionDS sessionDS = new SessionDS();
			sessionDS.ds = dsSales;
			sessionDS.DataSetRecSize = dsSales.Tables[0].Rows.Count;
			sessionDS.QueryResultMaxSize = sessionDS.DataSetRecSize;
			return sessionDS;
		}

		public void Paging(Object sender, DataGridPageChangedEventArgs e)
		{
			//runs when the paging text is clicked
			//update the current page from the parameter values
			dgSalesman.CurrentPageIndex = e.NewPageIndex;
			ShowCurrentPage();
			//bind data to the grid			
		}
	
		private void dgSalesman_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			// Get the value and return it to the parent window
			int iSelIndex = dgSalesman.SelectedIndex;
			DataGridItem dgRow = dgSalesman.Items[iSelIndex];
			String strSalesManID = dgRow.Cells[0].Text;
			String strSalesName = dgRow.Cells[1].Text;
			String strDesignation = dgRow.Cells[2].Text;			
	
			if(strSalesManID == "&nbsp;")
				strSalesManID = "";
			if(strSalesName == "&nbsp;")
				strSalesName = "";
			if(strDesignation == "&nbsp;")
				strDesignation = "";
									
			String sScript = "";
			sScript += "<script language=javascript>";			
			
			if(strFORMID.Equals("BandQuotation"))
			{
				//txtSalesmanId, txtSalesPername are the two controls(BandQuotation FORM) to be populated
				sScript += "  window.opener."+strFORMID+".txtSalesmanId.value = \"" + strSalesManID + "\";";
				sScript += "  window.opener."+strFORMID+".txtSalesPername.value = \"" + strSalesName + "\";";
			}			
			else if (strFORMID.Equals("CustomerProfile"))
			{
				//ONLY SalesmanName is required for Customer and Agent Profile
				//txtSalesmanId, txtSalesPername are the two controls(BandQuotation FORM) to be populated
				//sScript += "  window.opener."+strFORMID+".txtSalesmanId.value = '" + strSalesManID + "';";
				sScript += "  window.opener."+strFORMID+".txtSalesmanID.value = \"" + strSalesManID + "\";";
			}
			else if (strFORMID.Equals("AgentProfile"))
			{
				//ONLY SalesmanName is required for Customer and Agent Profile
				//txtSalesmanId, txtSalesPername are the two controls(BandQuotation FORM) to be populated
				//sScript += "  window.opener."+strFORMID+".txtSalesmanId.value = '" + strSalesManID + "';";
				sScript += "  window.opener."+strFORMID+".txtSalesmanID.value = \"" + strSalesManID + "\";";
			}

			else
			{
				sScript += "  window.opener."+strFORMID+".txtSalesManID.value = \"" + strSalesManID + "\";";
				sScript += "  window.opener."+strFORMID+".txtSalesManName.value = \"" + strSalesName + "\";";
				sScript += "  window.opener."+strFORMID+".txtSalesAddr1.value = \"" + strDesignation + "\";";
				
			}

			sScript += "  window.close();";
			sScript += "</script>";
			Response.Write(sScript);
		}

		private void btnSearch_Click(object sender, System.EventArgs e)
		{
			StringBuilder strQry = new StringBuilder();
			strQry.Append("select * from Salesman where applicationid='"+appID+"' and enterpriseid = '"+enterpriseID+"'");

			if (txtSalesManID.Text.ToString() != null && txtSalesManID.Text.ToString() != "")			
				strQry.Append(" and SalesManID like '%"+Utility.ReplaceSingleQuote(txtSalesManID.Text.ToString())+"%'");
			
			if (txtSalesManName.Text.ToString() != null && txtSalesManName.Text.ToString() != "")			
				strQry.Append(" and SalesMan_Name like N'%"+Utility.ReplaceSingleQuote(txtSalesManName.Text.ToString())+"%'");
		
			String strSQLQuery = strQry.ToString();
			ViewState["SQL_QUERY"] = strSQLQuery;
			dgSalesman.CurrentPageIndex = 0;
			ShowCurrentPage();			
		}

		private void BindGrid()
		{
			dgSalesman.VirtualItemCount = System.Convert.ToInt32(m_sdsSalesman.QueryResultMaxSize);
			dgSalesman.DataSource = m_sdsSalesman.ds;
			dgSalesman.DataBind();
			ViewState["Sales_DS"] = m_sdsSalesman;
		}

		private void ShowCurrentPage()
		{
			int iStartIndex = dgSalesman.CurrentPageIndex * dgSalesman.PageSize;
			String sqlQuery = (String)ViewState["SQL_QUERY"];
			m_sdsSalesman = GetSalesManDS(iStartIndex,dgSalesman.PageSize,sqlQuery);
			
			decimal pgCnt = (m_sdsSalesman.QueryResultMaxSize - 1)/dgSalesman.PageSize;
			if(pgCnt < dgSalesman.CurrentPageIndex)
				dgSalesman.CurrentPageIndex = System.Convert.ToInt32(pgCnt);

			dgSalesman.SelectedIndex = -1;
			dgSalesman.EditItemIndex = -1;
			BindGrid();
			getPageControls(Page);
		}

		private SessionDS GetSalesManDS(int iCurrent, int iDSRecSize, String strQuery)
		{
			SessionDS sessionDS = null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(appID,enterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("SalesmanPopup.aspx.cs","GetSalesManDS","ERR001","DbConnection object is null!!");
				return sessionDS;
			}
			sessionDS = dbCon.ExecuteQuery(strQuery,iCurrent,iDSRecSize,"Salesman");
			return  sessionDS;
		}

		private void btnClose_Click(object sender, System.EventArgs e)
		{
			String sScript = "";
			sScript += "<script language=javascript>";
			sScript += "  window.close();";
			sScript += "</script>";
			Response.Write(sScript);
		}
	}
}
