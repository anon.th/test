<%@ Import Namespace="com.common.util" %>
<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<%@ Page language="c#" Codebehind="VASZipcodeExcluded.aspx.cs" AutoEventWireup="false" Inherits="com.ties.VASZipcodeExcluded" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>VASZipcodeExcluded</title>
		<LINK href="css/Styles.css" type="text/css" rel="stylesheet" name="stylesheet">
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<!--#INCLUDE FILE="msFormValidations.inc"-->
		<script runat="server">
		</script>
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<FORM id="VASZipcodeExcluded" method="post" runat="server">
			<asp:button id="btnExecQry" style="Z-INDEX: 104; LEFT: 92px; POSITION: absolute; TOP: 43px" runat="server" Width="130px" CausesValidation="False" Text="Execute Query" CssClass="queryButton" Enabled="False"></asp:button><asp:label id="lblTitle" style="Z-INDEX: 113; LEFT: 31px; POSITION: absolute; TOP: 6px" runat="server" Width="436px" CssClass="mainTitleSize" Height="26px">Value Added Services</asp:label><asp:button id="btnInsert" style="Z-INDEX: 101; LEFT: 221px; POSITION: absolute; TOP: 43px" runat="server" Width="62px" CausesValidation="False" Text="Insert" CssClass="queryButton"></asp:button><asp:button id="btnSave" style="Z-INDEX: 102; LEFT: 283px; POSITION: absolute; TOP: 43px" runat="server" Width="66px" CausesValidation="True" Text="save" CssClass="queryButton" Visible="False"></asp:button><asp:button id="btnDelete" style="Z-INDEX: 103; LEFT: 349px; POSITION: absolute; TOP: 43px" runat="server" Width="62px" CausesValidation="False" Text="Delete" CssClass="queryButton" Visible="False"></asp:button><asp:button id="btnQry" style="Z-INDEX: 105; LEFT: 31px; POSITION: absolute; TOP: 43px" runat="server" Width="62px" CausesValidation="False" Text="Query" CssClass="queryButton"></asp:button>
			<table style="Z-INDEX: 100; LEFT: 30px; POSITION: absolute; TOP: 100px">
				<tr>
					<td>
						<asp:datagrid id="dgVAS" runat="server" Width="590px" ItemStyle-Height="20" AutoGenerateColumns="False" OnPageIndexChanged="dgVAS_PageChange" OnItemDataBound="dgVAS_Bound" OnEditCommand="dgVAS_Edit" OnCancelCommand="dgVAS_Cancel" OnUpdateCommand="dgVAS_Update" OnSelectedIndexChanged="dgVAS_SelectedIndexChanged" AllowPaging="True" AllowCustomPaging="True" OnDeleteCommand="dgVAS_Delete" SelectedItemStyle-CssClass="gridFieldSelected" PageSize="25">
							<SelectedItemStyle CssClass="gridFieldSelected"></SelectedItemStyle>
							<ItemStyle Height="20px" CssClass="gridField"></ItemStyle>
							<Columns>
								<asp:ButtonColumn Text="&lt;IMG SRC='images/butt-select.gif' border=0 title='Select' &gt;" CommandName="Select">
									<HeaderStyle HorizontalAlign="Center" Width="3%" CssClass="gridHeading"></HeaderStyle>
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
								</asp:ButtonColumn>
								<asp:EditCommandColumn ButtonType="LinkButton" UpdateText="&lt;IMG SRC='images/butt-update.gif' border=0 title='Update' &gt;" CancelText="&lt;IMG SRC='images/butt-cancel.gif' border=0 title='Cancel' &gt;" EditText="&lt;IMG SRC='images/butt-edit.gif' border=0 title='Edit' &gt;">
									<HeaderStyle Width="5%" CssClass="gridHeading"></HeaderStyle>
									<ItemStyle HorizontalAlign="Left"></ItemStyle>
								</asp:EditCommandColumn>
								<asp:ButtonColumn Text="&lt;IMG SRC='images/butt-delete.gif' border=0 title='Delete' &gt;" CommandName="Delete">
									<HeaderStyle HorizontalAlign="Center" Width="3%" CssClass="gridHeading"></HeaderStyle>
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
								</asp:ButtonColumn>
								<asp:TemplateColumn HeaderText="VAS Code">
									<HeaderStyle Font-Bold="True" Width="19%" CssClass="gridHeading"></HeaderStyle>
									<ItemStyle Wrap="False"></ItemStyle>
									<ItemTemplate>
										<asp:Label CssClass="gridLabel" ID="lblVASCode" Text='<%#DataBinder.Eval(Container.DataItem,"vas_code")%>' Runat="server">
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<cc1:mstextbox CssClass="gridTextBox" ID="txtVASCode" Text='<%#DataBinder.Eval(Container.DataItem,"vas_code")%>' Runat="server" Enabled="True" MaxLength="12" TextMaskType="msUpperAlfaNumericWithUnderscore">
										</cc1:mstextbox>
										<asp:RequiredFieldValidator ID="scValidator" Runat="server" BorderWidth="0" Display="None" ControlToValidate="txtVASCode" ErrorMessage="VAS Code is required field<br>"></asp:RequiredFieldValidator>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="VAS Description">
									<HeaderStyle Width="60%" CssClass="gridHeading"></HeaderStyle>
									<ItemTemplate>
										<asp:Label CssClass="gridLabel" ID="lblVASDescription" Text='<%#DataBinder.Eval(Container.DataItem,"vas_description")%>' Runat="server" Enabled="True">
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:TextBox CssClass="gridTextBox" ID="txtVASDescription" Text='<%#DataBinder.Eval(Container.DataItem,"vas_description")%>' Runat="server" MaxLength="200">
										</asp:TextBox>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Surcharge">
									<HeaderStyle Width="10%" CssClass="gridHeading"></HeaderStyle>
									<ItemStyle Wrap="False"></ItemStyle>
									<ItemTemplate>
										<asp:Label CssClass="gridLabelNumber" ID="lblSurcharge" Text='<%#DataBinder.Eval(Container.DataItem,"surcharge","{0:n}")%>' Runat="server">
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<cc1:mstextbox CssClass="gridTextBoxNumber" ID="txtSurcharge" Text='<%#DataBinder.Eval(Container.DataItem,"surcharge","{0:n}")%>' Runat="server" Enabled="True" MaxLength="9" TextMaskType="msNumeric" NumberMaxValue="999999" NumberMinValue="0" NumberPrecision="8" NumberScale="2">
										</cc1:mstextbox>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Allow for Customer">
									<HeaderStyle Width="23%" CssClass="gridHeading"></HeaderStyle>
									<ItemTemplate>
										<asp:DropDownList id="ddlAllowForCust" runat="server" CssClass="gridTextBox"></asp:DropDownList>
										<%--<asp:RequiredFieldValidator id="ValidatorAllowCust" runat="server" ErrorMessage="Allow for Customer flag must be Yes or No.<br>" ControlToValidate="ddlAllowForCust" Display="None"></asp:RequiredFieldValidator>--%>
									</ItemTemplate>
								</asp:TemplateColumn>
							</Columns>
							<PagerStyle NextPageText="Next" PrevPageText="Previous" HorizontalAlign="Right" CssClass="normalText"></PagerStyle>
						</asp:datagrid>
					</td>
				</tr>
				<%--<asp:ValidationSummary ID="sumValidateVas" DisplayMode="BulletList" ShowMessageBox="True" Runat="server" ></asp:ValidationSummary>--%>
				<tr>
					<td><asp:button id="btnZVASEInsert" runat="server" Width="62px" CausesValidation="False" Text="Insert" CssClass="queryButton" Enabled="False"></asp:button><asp:button id="btnZVASEInsertMultiple" runat="server" Width="128px" CausesValidation="False" Text="Insert Multiple" CssClass="queryButton" Enabled="False" Visible="False"></asp:button></td>
				</tr>
				<tr>
					<td><asp:label id="lblZVASEMessage" runat="server" Width="490px" CssClass="errorMsgColor" Height="28px"></asp:label></td>
				</tr>
				<tr>
					<td>
						<fieldset style="WIDTH: 592px; HEIGHT: 178px"><legend><asp:label id="Label1" runat="server" CssClass="tableField">VAS Unavailable Postal Codes</asp:label></legend><asp:datagrid id="dgZipcodeVASExcluded" runat="server" Width="587px" ItemStyle-Height="20" AutoGenerateColumns="False" OnPageIndexChanged="dgZipcodeVASExcluded_PageChange" OnItemDataBound="dgZipcodeVASExcluded_Bound" OnEditCommand="dgZipcodeVASExcluded_Edit" OnCancelCommand="dgZipcodeVASExcluded_Cancel" OnUpdateCommand="dgZipcodeVASExcluded_Update" AllowPaging="True" PageSize="10" AllowCustomPaging="True" OnDeleteCommand="dgZipcodeVASExcluded_Delete" OnItemCommand="dgZipcodeVASExcluded_Button">
								<ItemStyle Height="20px"></ItemStyle>
								<Columns>
									<asp:EditCommandColumn ButtonType="LinkButton" UpdateText="&lt;IMG SRC='images/butt-update.gif' border=0 title='Update' &gt;" CancelText="&lt;IMG SRC='images/butt-cancel.gif' border=0 title='Cancel' &gt;" EditText="&lt;IMG SRC='images/butt-edit.gif' border=0 title='Edit' &gt;" Visible="False">
										<HeaderStyle Width="4%" CssClass="gridHeading"></HeaderStyle>
										<ItemStyle CssClass="gridField"></ItemStyle>
									</asp:EditCommandColumn>
									<asp:ButtonColumn Text="&lt;IMG SRC='images/butt-delete.gif' border=0 title='Delete' &gt;" ButtonType="LinkButton" CommandName="Delete">
										<HeaderStyle HorizontalAlign="Center" Width="3%" CssClass="gridHeading"></HeaderStyle>
										<ItemStyle HorizontalAlign="Center" CssClass="gridField"></ItemStyle>
									</asp:ButtonColumn>
									<asp:TemplateColumn HeaderText="Postal Code">
										<HeaderStyle Width="30%" Font-Bold="True" CssClass="gridHeading"></HeaderStyle>
										<ItemStyle CssClass="gridField"></ItemStyle>
										<ItemTemplate>
											<asp:Label CssClass="gridLabel" ID="lblZipcode" Text='<%#DataBinder.Eval(Container.DataItem,"zipcode")%>' Runat="server">
											</asp:Label>
										</ItemTemplate>
										<EditItemTemplate>
											<cc1:mstextbox CssClass="gridTextBox" ID="txtZipcode" Text='<%#DataBinder.Eval(Container.DataItem,"zipcode")%>' Runat="server" Enabled="True" MaxLength="10" TextMaskType="msUpperAlfaNumericWithUnderscore">
											</cc1:mstextbox>
											<asp:RequiredFieldValidator ID="exValidator" Runat="server" BorderWidth="0" Display="None" ControlToValidate="txtZipcode" ErrorMessage="Zipcode is required field"></asp:RequiredFieldValidator>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:ButtonColumn Text="&lt;IMG SRC='images/butt-search.gif' border=0 title='Search' &gt;" ButtonType="LinkButton" CommandName="search" Visible="False">
										<HeaderStyle HorizontalAlign="Center" Width="3%" CssClass="gridHeading"></HeaderStyle>
										<ItemStyle HorizontalAlign="Center" CssClass="gridField"></ItemStyle>
									</asp:ButtonColumn>
									<asp:TemplateColumn HeaderText="State">
										<HeaderStyle Width="30%" CssClass="gridHeading"></HeaderStyle>
										<ItemStyle CssClass="gridField"></ItemStyle>
										<ItemTemplate>
											<asp:Label CssClass="gridLabel" ID="lblState" Text='<%#DataBinder.Eval(Container.DataItem,"state_name")%>' Runat="server" >
											</asp:Label>
										</ItemTemplate>
										<EditItemTemplate>
											<asp:TextBox CssClass="gridTextBox" ID="txtState" Text='<%#DataBinder.Eval(Container.DataItem,"state_name")%>' Runat="server" ReadOnly="true">
											</asp:TextBox>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Country">
										<HeaderStyle Width="30%" CssClass="gridHeading"></HeaderStyle>
										<ItemStyle CssClass="gridField"></ItemStyle>
										<ItemTemplate>
											<asp:Label CssClass="gridLabel" ID="lblCountry" Text='<%#DataBinder.Eval(Container.DataItem,"country")%>' Runat="server" >
											</asp:Label>
										</ItemTemplate>
										<EditItemTemplate>
											<asp:TextBox CssClass="gridTextBox" ID="txtCountry" Text='<%#DataBinder.Eval(Container.DataItem,"country")%>' Runat="server" ReadOnly="true">
											</asp:TextBox>
										</EditItemTemplate>
									</asp:TemplateColumn>
								</Columns>
								<PagerStyle NextPageText="Next" PrevPageText="Previous" HorizontalAlign="Right" CssClass="normalText"></PagerStyle>
							</asp:datagrid></fieldset>
					</td>
				</tr>
			</table>
			<asp:label id="lblVASMessage" style="Z-INDEX: 108; LEFT: 31px; POSITION: absolute; TOP: 70px" runat="server" Width="483px" CssClass="errorMsgColor" Height="26px"></asp:label><asp:validationsummary id="ValidationSummary1" style="Z-INDEX: 99; LEFT: 52px; POSITION: absolute; TOP: 76px" Width="444px" Height="23px" Runat="server" ShowMessageBox="True" DisplayMode="SingleParagraph" ShowSummary="False"></asp:validationsummary></FORM>
	</body>
</HTML>
