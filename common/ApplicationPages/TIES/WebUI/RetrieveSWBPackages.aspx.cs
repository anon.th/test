using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using TIESBAL;
using TIESDAL;
using com.common.applicationpages;
using com.ties;
using com.ties.DAL;
using com.ties.BAL;
using com.ties.classes;
using com.common.AppExtension;
using com.common.classes;
using com.common.util;

namespace TIES.WebUI
{
	/// <summary>
	/// Summary description for RetrievePickUpPkgs.
	/// </summary>
	public class RetrieveSWBPackages : BasePage
	{
		protected System.Web.UI.WebControls.Button btnRetrieve;
		string strAppId,strEnterpriseId,userID;
		protected System.Web.UI.WebControls.Label lblTitle;
		protected System.Web.UI.WebControls.Label lbl;
		
		string m_format;
		int wt_rounding_method;
		protected System.Web.UI.WebControls.Label lblErrorMsg;
		decimal wt_increment_amt;
		private void Page_Load(object sender, System.EventArgs e)
		{
			if (Page.IsPostBack==false)
			{
				CheckAnotherProcess();
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			base.OnInit(e);
			InitializeComponent();
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnRetrieve.Click += new System.EventHandler(this.btnRetrieve_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private bool CheckAnotherProcess()
		{
			bool status =true;
			
			strAppId= System.Configuration.ConfigurationSettings.AppSettings["applicationID"];
			strEnterpriseId= System.Configuration.ConfigurationSettings.AppSettings["enterpriseID"];

			string msgbox = "";
			DataSet dsJob = RetSWBPcksBatchDAL.RetrieveSWBPkg_CheckJob(strAppId,strEnterpriseId);
			lblErrorMsg.Text="";
			if(dsJob.Tables[0].Rows.Count>0)
			{
				msgbox = Utility.GetLanguageText(ResourceType.UserMessage, "Retrieve_SWB_Pkg_Process_started", utility.GetUserCulture());
				msgbox = msgbox.Replace("{userid}",dsJob.Tables[0].Rows[0]["userid"].ToString());
				msgbox = msgbox.Replace("{start_DT}",dsJob.Tables[0].Rows[0]["start_DT"].ToString());
				msgbox = msgbox.Replace("{running_time}",dsJob.Tables[0].Rows[0]["running_time"].ToString());
				lblErrorMsg.Text=msgbox;
				btnRetrieve.Enabled=false;
				status=false;
			}
			else
			{
				lblErrorMsg.Text =Utility.GetLanguageText(ResourceType.UserMessage,"Retrieve_SWB_Pkg_Not_start",utility.GetUserCulture());
				btnRetrieve.Enabled=true;
				status=true;
			}

			return status;
		}

		private void btnRetrieve_Click(object sender, System.EventArgs e)
		{
			string sScript;

			if (CheckAnotherProcess()==true)
			{
				lblErrorMsg.Text =Utility.GetLanguageText(ResourceType.UserMessage,"Retrieve_SWB_Pkg_started",utility.GetUserCulture());
				userID = utility.GetUserID();
				
				bool fComplete = false;
				try
				{
					sScript = "<script langauge='javacript'>";
					sScript+="document.body.style.cursor = 'wait';";
					sScript+="</script>";

					Page.RegisterStartupScript("Cursorwait",sScript);
				
					strAppId= System.Configuration.ConfigurationSettings.AppSettings["applicationID"];
					strEnterpriseId= System.Configuration.ConfigurationSettings.AppSettings["enterpriseID"];
					DataSet profileDS = SysDataManager1.GetEnterpriseProfile(strAppId, strEnterpriseId);

					//				if(profileDS.Tables[0].Rows.Count > 0)
					//				{
					//					int currency_decimal = (int) profileDS.Tables[0].Rows[0]["currency_decimal"];
					//					m_format = "{0:F" + currency_decimal.ToString() + "}";
					//					if  ( profileDS.Tables[0].Rows[0]["wt_rounding_method"] != System.DBNull.Value)
					//					{
					//						
					//						wt_rounding_method = Convert.ToInt32(profileDS.Tables[0].Rows[0]["wt_rounding_method"]);
					//					}
					//
					//					if  ( profileDS.Tables[0].Rows[0]["wt_increment_amt"] != System.DBNull.Value)
					//					{
					//						wt_increment_amt = Convert.ToDecimal(profileDS.Tables[0].Rows[0]["wt_increment_amt"]);
					//					}
					//				}
					//			
					//				DataSet ds = new DataSet();
					//				ds=RetSWBPcksBatchDAL.getPackageDetail(strAppId,strEnterpriseId);
					//				if(ds.Tables[0].Rows.Count>0)
					//				{
					//					foreach(DataRow dr in ds.Tables[0].Rows)
					//					{
					//						if(!RetSWBPcksBatchBAL.isConsignmentHasInv(strAppId,strEnterpriseId,dr["consignment_no"].ToString()))
					//						{
					//							if(!RetSWBPcksBatchBAL.isPreshipment(strAppId,strEnterpriseId,dr["consignment_no"].ToString()))
					//							{
					//								if(!RetSWBPcksBatchBAL.isManual_Rating_Override(strAppId,strEnterpriseId,dr["consignment_no"].ToString()))
					//								{
					//									if(RetSWBPcksBatchBAL.isExistInSWB(strAppId,strEnterpriseId,dr["consignment_no"].ToString()))
					//									{
					//										if(!RetSWBPcksBatchBAL.isIdenticalPackage(strAppId,strEnterpriseId,dr["booking_no"].ToString(),dr["consignment_no"].ToString()))
					//										{
					//											DataSet dsQryPkg =null;
					//											DataSet dsPkgDetails =null;
					//											dsQryPkg = DomesticShipmentMgrDAL.GetPakageDtlsSWB(strAppId,strEnterpriseId,dr["consignment_no"].ToString(),dr["payerid"].ToString());
					//											if(dsQryPkg != null)
					//											{
					//												dsPkgDetails = dsQryPkg;
					//
					//											}
					//											else
					//											{
					//												dsPkgDetails = DomesticShipmentMgrDAL.GetEmptyPkgDtlDS();
					//											}
					//
					//											CalculateChargeable_wt(dsPkgDetails, dr["payerid"].ToString());
					//											
					//											decimal fCalcFrgtChrg=0;
					//											
					//											decimal chargeable_wt = 0;
					//											if (dsPkgDetails.Tables[0].Rows.Count > 0)
					//											{
					//												for(int i=0;i < dsPkgDetails.Tables[0].Rows.Count;i++)
					//												{
					//													chargeable_wt += decimal.Parse(dsPkgDetails.Tables[0].Rows[i]["chargeable_wt"].ToString());
					//												}
					//												dr["char_wt"] = chargeable_wt;
					//											}
					//											
					//
					//											fCalcFrgtChrg= CalculateAllFreightCharge(strAppId,strEnterpriseId,dr["payerid"].ToString() ,dr["service_code"].ToString() ,
					//												dr["sender_zipcode"].ToString(),dr["recipient_zipcode"].ToString(),dr["char_wt"].ToString(),dr["declare_value"].ToString() ,dsPkgDetails);
					//											if(fCalcFrgtChrg.ToString("#0.00").Equals("0.00") && dr["char_wt"].ToString().Length >0 && dr["declare_value"].ToString().Trim().Length >0)
					//											{
					//												continue;
					//											}
					//											else
					//											{
					//												if( dr["sender_zipcode"].ToString()!="" && dr["recipient_zipcode"].ToString()!="")
					//												{
					//													RetSWBPcksBatchDAL.MoveDataToOriginal_Insert(strAppId,strEnterpriseId,dr["booking_no"].ToString(),dr["consignment_no"].ToString());
					//													//RetSWBPcksBatchDAL.UpdateFreightCharge(strAppId,strEnterpriseId,dr["booking_no"].ToString(),dr["consignment_no"].ToString(),RoundingFreightCharge(fCalcFrgtChrg));
					//													// ESA Service Type
					//													decimal decESASSurcharge = DomesticShipmentMgrDAL.calCulateESA_ServiceType(strAppId,strEnterpriseId,dr["payerid"].ToString(),dr["sender_zipcode"].ToString(),dr["recipient_zipcode"].ToString(),dr["service_code"].ToString(),fCalcFrgtChrg.ToString());
					//													//GetOtherSurcharge
					//													decimal decTotOtherChrg= Rounding(DomesticShipmentMgrDAL.GetOtherSurcharge(strAppId,strEnterpriseId,dr["payerid"].ToString(),fCalcFrgtChrg.ToString()));
					//													//Insurance Surcharge
					//													decimal decInsuranceChrg =  RetSWBPcksBatchBAL.calInsuranceSurcharge(strAppId, strEnterpriseId, dr["payerid"].ToString().Trim(), Convert.ToDecimal( dr["declare_value"].ToString()));
					//													// VAS Surcharge
					//													DataSet m_dsVAS = new DataSet();
					//													m_dsVAS = DomesticShipmentMgrDAL.GetVASData(strAppId,strEnterpriseId,Convert.ToDecimal(dr["booking_no"]),dr["consignment_no"].ToString());
					//													if(m_dsVAS == null)
					//													{
					//														m_dsVAS = DomesticShipmentMgrDAL.GetEmptyVASDS();
					//													}
					//													decimal decTotVASSurcharge = DomesticShipmentMgrDAL.GetTotalSurcharge(m_dsVAS);
					//													
					//													decTotVASSurcharge =  Convert.ToDecimal(String.Format(m_format,Convert.ToDecimal(dr["tot_vas_surcharge"].ToString())));
					//													decInsuranceChrg =   Convert.ToDecimal(String.Format(m_format,Convert.ToDecimal(dr["insurance_surcharge"].ToString())));
					//													decTotOtherChrg =  Convert.ToDecimal(String.Format(m_format,Convert.ToDecimal(dr["other_surch_amount"].ToString())));
					//													decESASSurcharge =  Convert.ToDecimal(String.Format(m_format,Convert.ToDecimal(dr["esa_surcharge"].ToString())));
					//
					//													decimal decTotalAmt = fCalcFrgtChrg + decTotVASSurcharge + decInsuranceChrg + decTotOtherChrg + decESASSurcharge;	
					//													decTotalAmt =  Convert.ToDecimal(String.Format(m_format,Rounding(decTotalAmt)));
					//												
					//													RetSWBPcksBatchDAL.UpdateShipmentSurcharge(strAppId,strEnterpriseId,dr["booking_no"].ToString(),dr["consignment_no"].ToString(),fCalcFrgtChrg,decInsuranceChrg,decTotVASSurcharge,decESASSurcharge,decTotOtherChrg,decTotalAmt);
					//													RetSWBPcksBatchDAL.UpdateShipment_PKG_BOX(strAppId,strEnterpriseId,(int)dr["booking_no"],dr["consignment_no"].ToString(),dr["payerid"].ToString(),dr["sender_zipcode"].ToString(),dr["recipient_zipcode"].ToString(),dr["service_code"].ToString(),dsPkgDetails);
					//												}
					//											}
					//										
					//										}
					//									}
					//								}
					//							}
					//						}
					//					}
					//				}
					RetSWBPcksBatchDAL.RetrieveSWBPkg_Details_to_DMS(strAppId,strEnterpriseId,userID);
					fComplete = true;
				}
				catch(Exception ex)
				{
					Logger.LogTraceError("RetrieveSWBPackages","btnRetrieve_Click","btnRetrieve_Click",ex.Message);
					throw ex;
				}
				finally
				{
					sScript = "<script langauge='javacript'>";
					sScript+="document.body.style.cursor = 'default';";
					sScript+="</script>";

					Page.RegisterStartupScript("CursorClear",sScript);

					if(fComplete)
						lblErrorMsg.Text =Utility.GetLanguageText(ResourceType.UserMessage,"RET_SUCCESSFULLY",utility.GetUserCulture());
				}
			}
			else
			{
				sScript = "<script langauge='javacript'>";
				sScript+="document.body.style.cursor = 'default';";
				sScript+="</script>";

				Page.RegisterStartupScript("CursorClear",sScript);
				//lblErrorMsg.Text =Utility.GetLanguageText(ResourceType.UserMessage,"RET_SUCCESSFULLY",utility.GetUserCulture());
			}			
		}
		private decimal Rounding(decimal beforeRound)
		{
			return TIESUtility.EnterpriseRounding(beforeRound,wt_rounding_method,wt_increment_amt);
		}
		//Add by Panas 24/04/2009
		private decimal RoundingFreightCharge(decimal decFreight)
		{
			return TIESUtility.EnterpriseRounding(decFreight,wt_rounding_method,wt_increment_amt);
		}

		
		// edit by Tumz.
		//		fCalcFrgtChrg= DomesticShipmentMgrDAL.CalculateAllFreightCharge(strAppId,strEnterpriseId,dr["payerid"].ToString() ,dr["service_code"].ToString() ,
		//		dr["sender_zipcode"].ToString(),dr["recipient_zipcode"].ToString(),dr["char_wt"].ToString(),dr["declare_value"].ToString() ,dsPkgDetails,fCalcFrgtChrg);
			
		private decimal CalculateAllFreightCharge(string appID,string enterpriseID,
			string payerID,string ServiceCode,string senderZipcode,string RecipZipCode,string PkgChargeWt,
			string ShpDclrValue,DataSet dsPkgDetails)
		{			
			decimal fCalcFrgtChrg = 0;
			String strLocalErrorMsg = "";

			if( payerID != "" && senderZipcode.Trim() != "" &&
				RecipZipCode.Trim() != "" && PkgChargeWt.Trim() != "" &&
				ShpDclrValue.Trim() != "" /*&& IsCalculate("original_rated_freight")*/)
			{
				String strWeight = PkgChargeWt;

				if (strLocalErrorMsg.Trim() == "")
				{
					
					try
					{
						DataSet tmpCustZone = null;
						string sendCustZone = null;
						string recipCustZone = null;
						tmpCustZone = SysDataMgrDAL.GetCustZoneData(appID, enterpriseID,
							0, 0, payerID.Trim(), senderZipcode.Trim()).ds;
						if(tmpCustZone.Tables[0].Rows.Count > 0)
						{
							foreach(DataRow dr in tmpCustZone.Tables[0].Rows)
							{
								sendCustZone = dr["zone_code"].ToString().Trim();
							}
						}
						tmpCustZone = SysDataMgrDAL.GetCustZoneData(appID, enterpriseID,
							0, 0, payerID.Trim(), RecipZipCode).ds;
						if(tmpCustZone.Tables[0].Rows.Count > 0)
						{
							foreach(DataRow dr in tmpCustZone.Tables[0].Rows)
							{
								recipCustZone = dr["zone_code"].ToString().Trim();
							}
						}
						Customer customer = new Customer();
						customer.Populate(appID,enterpriseID, payerID.Trim());
						if(!customer.IsBoxRateAvailable(appID,enterpriseID,sendCustZone,recipCustZone,ServiceCode))
						{
							//lblErrorMsg.Text = "Customer has no Box Rate defined.";
							return 0;
						}
	
						fCalcFrgtChrg= DomesticShipmentMgrDAL.CalculateAllFreightCharge(appID,enterpriseID,payerID ,ServiceCode ,
							senderZipcode,RecipZipCode ,PkgChargeWt ,ShpDclrValue , dsPkgDetails,fCalcFrgtChrg);
						
						fCalcFrgtChrg = Convert.ToDecimal(String.Format(m_format,RoundingFreightCharge(fCalcFrgtChrg))); 
						return fCalcFrgtChrg;
					}
					catch(ApplicationException appException)
					{
						String strMsg = appException.Message;
						strLocalErrorMsg = strMsg;
						lblErrorMsg.Text = strMsg;

					}
				}
			}

			return 0;
			//return strLocalErrorMsg;
		}
	


		private void CalculateChargeable_wt(DataSet dsPkg, string payer_id)
		{
			Customer customer = new Customer();
			customer.Populate(strAppId,strEnterpriseId,payer_id);
			int cnt = dsPkg.Tables[0].Rows.Count;
			int i = 0;

			decimal decTotDimWt = 0;
			decimal decTotWt = 0;
			decimal  TOTVol = 0; 
			decimal decTot_act_Wt = 0;
			int iTotPkgs = 0;
			decimal decChrgWt = 0;


			//Jeab 28 Dec 10
			if (customer.Dim_By_tot =="Y")
			{
				decTotDimWt = 0;
				decTotWt = 0;
				for(i = 0;i<cnt;i++)
				{
					DataRow drEach = dsPkg.Tables[0].Rows[i];
					decTotDimWt += (decimal)drEach["tot_dim_wt"];
					decTotWt += (decimal)drEach["tot_wt"];
					if (drEach["tot_act_wt"]== System.DBNull.Value )
					{
						drEach["tot_act_wt"]= 0	;
					}
					decTot_act_Wt += (decimal)drEach["tot_act_wt"];
					iTotPkgs += (int)drEach["pkg_qty"];
					
					//					if (drEach["pkg_TOTvolume"] != null)
					//					{
					//						TOTVol += (decimal)drEach["pkg_TOTvolume"];
					//					}
					//					else
					//					{
					TOTVol += Convert.ToDecimal(drEach["pkg_length"])*Convert.ToDecimal(drEach["pkg_breadth"])*Convert.ToDecimal(drEach["pkg_height"])*Convert.ToDecimal(drEach["pkg_qty"]); //Jeab 22 Feb 2011
					//					}
					//edit by Tumz.
					////drEach["pkg_volume"] = Convert.ToDecimal(drEach["pkg_length"])*Convert.ToDecimal(drEach["pkg_breadth"])*Convert.ToDecimal(drEach["pkg_height"])*Convert.ToDecimal(drEach["pkg_qty"]);;
				}

				if(decTotDimWt < decTotWt)  // �� decTotWt  ᷹ decTot_act_Wt (�������Ѻ������͹���)  Jeab 28 Feb 2011
				{
					decChrgWt = decTotWt;  // �� decTotWt  ᷹ decTot_act_Wt  (�������Ѻ������͹���)    Jeab 28 Feb 2011
					for(i = 0;i<cnt;i++)
					{
						DataRow drEach = dsPkg.Tables[0].Rows[i];
						drEach["chargeable_wt"] = drEach["tot_wt"];
					}
				}
				else
				{
					decChrgWt = decTotDimWt;
					for(i = 0;i<cnt;i++)
					{
						DataRow drEach = dsPkg.Tables[0].Rows[i];
						drEach["chargeable_wt"] = drEach["tot_dim_wt"];
					}
				}
			}
			else
			{
				for(i = 0;i<cnt;i++)
				{
					DataRow drEach = dsPkg.Tables[0].Rows[i];
					decTotDimWt += (decimal)drEach["tot_dim_wt"];
					decTotWt += (decimal)drEach["tot_wt"];
					//TU on 17June08
					if (drEach["tot_act_wt"]== System.DBNull.Value )
					{
						drEach["tot_act_wt"]= 0	;
					}
					decTot_act_Wt += (decimal)drEach["tot_act_wt"];
					TOTVol += Convert.ToDecimal(drEach["pkg_length"])*Convert.ToDecimal(drEach["pkg_breadth"])*Convert.ToDecimal(drEach["pkg_height"])*Convert.ToDecimal(drEach["pkg_qty"]); //Jeab 22 Feb 2011

					//edit by Tumz.
					////drEach["pkg_volume"] = Convert.ToDecimal(drEach["pkg_length"])*Convert.ToDecimal(drEach["pkg_breadth"])*Convert.ToDecimal(drEach["pkg_height"])*Convert.ToDecimal(drEach["pkg_qty"]);

					iTotPkgs += (int)drEach["pkg_qty"];
					//decChrgWt += (decimal)drEach["chargeable_wt"];
					if(customer.ApplyDimWt == "Y" || customer.ApplyDimWt == "")
					{
						if((decimal)drEach["tot_wt"] < (decimal)drEach["tot_dim_wt"])
						{
							decChrgWt +=  (decimal)drEach["tot_dim_wt"];
							drEach["chargeable_wt"] = drEach["tot_dim_wt"];
						}
						else
						{
							decChrgWt += (decimal)drEach["tot_wt"];
							drEach["chargeable_wt"] = drEach["tot_wt"];
						}
					}
					else if(customer.ApplyDimWt == "N")
					{
						decChrgWt += (decimal)drEach["tot_wt"];
						drEach["chargeable_wt"] = drEach["tot_wt"];
					}
				}
			}
		}
		
	}
}
