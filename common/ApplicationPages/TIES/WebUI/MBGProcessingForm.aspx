<%@ Page language="c#" Codebehind="MBGProcessingForm.aspx.cs" AutoEventWireup="false" Inherits="com.ties.MBGProcessingForm" %>
<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<%@ Register TagPrefix="dbCombo" Namespace="Cambro.Web.DbCombo" Assembly="Cambro.Web.DbCombo" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Force MBG</title>
		<LINK rel="stylesheet" type="text/css" href="css/Styles.css" name="stylesheet">
		<meta name="GENERATOR" content="Microsoft Visual Studio 7.0">
		<meta name="CODE_LANGUAGE" content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<!--#INCLUDE FILE="msFormValidations.inc"-->
	</HEAD>
	<BODY onunload="window.top.displayBanner.fnCloseAll(0);" MS_POSITIONING="GridLayout">
		<FORM id="MBGProcessingForm" method="post" runat="server">
			<P><asp:label style="Z-INDEX: 102; POSITION: absolute; TOP: 8px; LEFT: 7px" id="lblMainTitle"
					runat="server" CssClass="mainTitleSize" Height="26px" Width="477px"> MBG Claim Processing</asp:label></P>
			<DIV style="Z-INDEX: 101; POSITION: relative; WIDTH: 1270px; HEIGHT: 879px; TOP: 46px; LEFT: 0px"
				id="divMain" MS_POSITIONING="GridLayout" runat="server">
				<TABLE style="Z-INDEX: 101; POSITION: absolute; WIDTH: 726px; TOP: 3px; LEFT: 0px" id="MainTable"
					border="0" width="726" runat="server">
					<TR vAlign="top">
						<TD vAlign="top" width="738"><asp:button id="btnQry" tabIndex="1" runat="server" CssClass="queryButton" Width="61px" CausesValidation="False"
								Text="Query"></asp:button><asp:button id="btnExecQry" tabIndex="2" runat="server" CssClass="queryButton" Width="130px"
								CausesValidation="False" Text="Execute Query"></asp:button><asp:button id="btnSave" tabIndex="3" runat="server" CssClass="queryButton" Width="66px" Text="Save"></asp:button><asp:button id="btnApplyMBG" tabIndex="4" runat="server" CssClass="queryButton" Width="130px"
								Text="Apply MBG"></asp:button><asp:button style="Z-INDEX: 0" id="btnRevertMBG" tabIndex="4" runat="server" CssClass="queryButton"
								Width="130px" Text="Reverse MBG"></asp:button><asp:label id="lblErrorMsg" runat="server" CssClass="errorMsgColor" Height="19px" Width="537px"></asp:label><asp:validationsummary id="PageValidationSummary" runat="server" Height="39px" Width="346px" Visible="True"
								ShowSummary="False" ShowMessageBox="True"></asp:validationsummary></TD>
					</TR>
					<TR>
						<TD width="726">
							<TABLE id="TblDtl" border="0" width="737" runat="server">
								<TR height="25">
									<TD width="3%"></TD>
									<TD style="WIDTH: 120px" width="120"><asp:label id="lblConsgmtNo" runat="server" CssClass="tableLabel" Height="15px" Width="113px">Consignment No</asp:label></TD>
									<TD style="WIDTH: 156px" width="156"><asp:textbox id="txtConsigNo" tabIndex="10" runat="server" CssClass="textField" Width="141px"
											MaxLength="20"></asp:textbox></TD>
									<TD style="WIDTH: 120px" width="120"><FONT face="Tahoma"><asp:label style="Z-INDEX: 0" id="lblMBGStatus" runat="server" CssClass="tableHeadingFieldset"
												Height="15px"></asp:label></FONT></TD>
									<TD style="WIDTH: 156px" width="156"></TD>
								</TR>
								<TR height="25">
									<TD style="HEIGHT: 25px" width="3%"></TD>
									<TD style="WIDTH: 120px; HEIGHT: 25px" width="120"></TD>
									<TD style="WIDTH: 156px; HEIGHT: 25px" width="156"></TD>
									<TD style="WIDTH: 120px; HEIGHT: 25px" width="120"></TD>
									<TD style="WIDTH: 156px" width="156"></TD>
								</TR>
								<TR height="25">
									<TD style="HEIGHT: 25px" width="3%"></TD>
									<TD style="WIDTH: 120px; HEIGHT: 25px" width="120"><asp:label id="lblInvoinceNo" runat="server" CssClass="tableLabel" Height="15px">Invoice No</asp:label></TD>
									<TD style="WIDTH: 156px; HEIGHT: 25px" width="156"><asp:textbox style="Z-INDEX: 0" id="txtInvoinceNo" tabIndex="10" runat="server" CssClass="textField"
											Width="141px" MaxLength="20" ReadOnly="True"></asp:textbox></TD>
									<TD style="WIDTH: 120px; HEIGHT: 25px" width="120"><asp:label style="Z-INDEX: 0" id="lblPayerID" runat="server" CssClass="tableLabel" Height="15px">Payer ID</asp:label></TD>
									<TD style="WIDTH: 156px" width="156"><asp:textbox style="Z-INDEX: 0" id="txtPayerID" tabIndex="10" runat="server" CssClass="textField"
											Width="141px" MaxLength="20" ReadOnly="True"></asp:textbox></TD>
								</TR>
								<TR height="25">
									<TD style="HEIGHT: 25px" width="3%"></TD>
									<TD style="WIDTH: 120px; HEIGHT: 25px" width="120"><asp:label id="lblInvoiceStatus" runat="server" CssClass="tableLabel" Height="15px">Invoice Status</asp:label></TD>
									<TD style="WIDTH: 156px; HEIGHT: 25px" width="156"><asp:textbox style="Z-INDEX: 0" id="txtInvoiceStatus" tabIndex="10" runat="server" CssClass="textField"
											Width="141px" MaxLength="20" ReadOnly="True"></asp:textbox></TD>
									<TD style="WIDTH: 120px; HEIGHT: 25px" width="120"><asp:label style="Z-INDEX: 0" id="lblLastExcepCode" runat="server" CssClass="tableLabel" Height="15px">Last Exception Code</asp:label></TD>
									<TD style="WIDTH: 156px" width="156"><asp:textbox style="Z-INDEX: 0" id="txtLastExcepCode" tabIndex="10" runat="server" CssClass="textField"
											Width="141px" MaxLength="20" ReadOnly="True"></asp:textbox></TD>
								</TR>
								<TR height="25">
									<TD style="HEIGHT: 25px" width="3%"></TD>
									<TD style="WIDTH: 120px; HEIGHT: 25px" width="120"></TD>
									<TD style="WIDTH: 156px; HEIGHT: 25px" width="156"></TD>
									<TD style="WIDTH: 120px; HEIGHT: 25px" width="120"></TD>
									<TD style="WIDTH: 156px" width="156"></TD>
								</TR>
								<TR height="25">
									<TD style="HEIGHT: 25px" width="3%"></TD>
									<TD style="WIDTH: 120px; HEIGHT: 25px" width="120"><asp:label id="lblFrightCharge" runat="server" CssClass="tableLabel" Height="15px">Freight Charge</asp:label></TD>
									<TD style="WIDTH: 156px; HEIGHT: 25px" width="156"><cc1:mstextbox style="Z-INDEX: 0" id="txtFrightCharge" tabIndex="18" runat="server" CssClass="textFieldRightAlign"
											Width="141px" MaxLength="9" ReadOnly="True" Enabled="True" TextMaskType="msNumeric" NumberScale="2" NumberPrecision="8" NumberMinValue="0"
											NumberMaxValue="99999999" AutoPostBack="True"></cc1:mstextbox></TD>
									<TD style="WIDTH: 120px; HEIGHT: 25px" width="120"><asp:label style="Z-INDEX: 0" id="lblTotalVAS" runat="server" CssClass="tableLabel" Height="15px"
											Width="126px">Total VAS Surcharge</asp:label></TD>
									<TD style="WIDTH: 156px" width="156"><cc1:mstextbox style="Z-INDEX: 0" id="txtTotalVAS" tabIndex="18" runat="server" CssClass="textFieldRightAlign"
											Width="141px" MaxLength="9" ReadOnly="True" Enabled="True" TextMaskType="msNumeric" NumberScale="2" NumberPrecision="8" NumberMinValue="0"
											NumberMaxValue="99999999" AutoPostBack="True"></cc1:mstextbox></TD>
								</TR>
								<TR height="25">
									<TD style="HEIGHT: 25px" width="3%"></TD>
									<TD style="WIDTH: 120px; HEIGHT: 25px" width="120"><asp:label id="lblInsurance" runat="server" CssClass="tableLabel" Height="15px" Width="128px">Insurance Surcharse</asp:label></TD>
									<TD style="WIDTH: 156px; HEIGHT: 25px" width="156"><cc1:mstextbox style="Z-INDEX: 0" id="txtInsurance" tabIndex="18" runat="server" CssClass="textFieldRightAlign"
											Width="141px" MaxLength="9" ReadOnly="True" Enabled="True" TextMaskType="msNumeric" NumberScale="2" NumberPrecision="8" NumberMinValue="0"
											NumberMaxValue="99999999" AutoPostBack="True"></cc1:mstextbox></TD>
									<TD style="WIDTH: 120px; HEIGHT: 25px" width="120"><asp:label style="Z-INDEX: 0" id="lblESA" runat="server" CssClass="tableLabel" Height="15px">ESA Surcharge</asp:label></TD>
									<TD style="WIDTH: 156px" width="156"><cc1:mstextbox style="Z-INDEX: 0" id="txtESA" tabIndex="18" runat="server" CssClass="textFieldRightAlign"
											Width="141px" MaxLength="9" ReadOnly="True" Enabled="True" TextMaskType="msNumeric" NumberScale="2" NumberPrecision="8" NumberMinValue="0"
											NumberMaxValue="99999999" AutoPostBack="True"></cc1:mstextbox></TD>
								</TR>
								<TR height="25">
									<TD style="HEIGHT: 25px" width="3%"></TD>
									<TD style="WIDTH: 120px; HEIGHT: 25px" width="120"><asp:label id="lblOther" runat="server" CssClass="tableLabel" Height="15px">Other Surcharge</asp:label></TD>
									<TD style="WIDTH: 156px; HEIGHT: 25px" width="156"><cc1:mstextbox style="Z-INDEX: 0" id="txtOther" tabIndex="18" runat="server" CssClass="textFieldRightAlign"
											Width="141px" MaxLength="9" ReadOnly="True" Enabled="True" TextMaskType="msNumeric" NumberScale="2" NumberPrecision="8" NumberMinValue="0"
											NumberMaxValue="99999999" AutoPostBack="True"></cc1:mstextbox></TD>
									<TD style="WIDTH: 120px; HEIGHT: 25px" width="120"><asp:label style="Z-INDEX: 0" id="lblTotalRate" runat="server" CssClass="tableLabel" Height="15px">Total Rated Amount</asp:label></TD>
									<TD style="WIDTH: 156px" width="156"><cc1:mstextbox style="Z-INDEX: 0" id="txtTotalRate" tabIndex="18" runat="server" CssClass="textFieldRightAlign"
											Width="141px" MaxLength="9" ReadOnly="True" Enabled="True" TextMaskType="msNumeric" NumberScale="2" NumberPrecision="8" NumberMinValue="0"
											NumberMaxValue="99999999" AutoPostBack="True"></cc1:mstextbox></TD>
								</TR>
								<TR height="25">
									<TD style="HEIGHT: 25px" width="3%"></TD>
									<TD style="WIDTH: 120px; HEIGHT: 25px" width="120"></TD>
									<TD style="WIDTH: 156px; HEIGHT: 25px" width="156"></TD>
									<TD style="WIDTH: 120px; HEIGHT: 25px" width="120"><asp:label style="Z-INDEX: 0" id="lblMBGAmount" runat="server" CssClass="tableLabel" Height="15px">MBG Amount</asp:label></TD>
									<TD style="WIDTH: 156px" width="156"><cc1:mstextbox style="Z-INDEX: 0" id="txtMBGAmount" tabIndex="18" runat="server" CssClass="textFieldRightAlign"
											Width="141px" MaxLength="9" ReadOnly="True" Enabled="True" TextMaskType="msNumeric" NumberScale="2" NumberPrecision="8" NumberMinValue="-99999999"
											AutoPostBack="True"></cc1:mstextbox></TD>
								</TR>
								<tr>
									<td style="HEIGHT: 25px"></td>
								</tr>
								<tr>
									<td colSpan="5" align="center"><asp:button id="btnClose" tabIndex="1" runat="server" CssClass="queryButton" Width="61px" CausesValidation="False"
											Text="Close"></asp:button></td>
								</tr>
							</TABLE>
						</TD>
					</TR>
				</TABLE>
			</DIV>
		</FORM>
	</BODY>
</HTML>
