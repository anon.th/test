<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<%@ Import Namespace="com.common.util" %>
<%@ Page smartnavigation="false" language="c#" Codebehind="AdminRoleInfo.aspx.cs" AutoEventWireup="false" Inherits="TIES.WebUI.AdminRoleInfo" %>
<%@ Register TagPrefix="iewc" Namespace="Microsoft.Web.UI.WebControls" Assembly="Microsoft.Web.UI.WebControls, Version=1.0.2.226, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>AdminRoleInfo</title>
		<script runat="server">
		String backImagePath = "images/icons/invoice1.gif";

		</script>
		<LINK href="css/Styles.css" type="text/css" rel="stylesheet" name="stylesheet">
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<!--#INCLUDE FILE="msFormValidations.inc"-->
	</HEAD>
	<body MS_POSITIONING="GridLayout" onunload="window.top.displayBanner.fnCloseAll(0);">
		<form id="AdminRoleInfo" method="post" runat="server">
			<DIV id="divMain" style="Z-INDEX: 101; LEFT: 1px; WIDTH: 735px; POSITION: relative; TOP: 24px; HEIGHT: 432px" MS_POSITIONING="GridLayout" runat="server">&nbsp;
				<asp:button id="btnDelete" CausesValidation="False" style="Z-INDEX: 110; LEFT: 348px; POSITION: absolute; TOP: 16px" runat="server" Width="62px" Height="21px" Text="Delete" CssClass="queryButton" tabIndex="5"></asp:button>
				<asp:button id="btnInsert" CausesValidation="False" style="Z-INDEX: 109; LEFT: 220px; POSITION: absolute; TOP: 16px" runat="server" Width="62px" Height="21px" Text="Insert" CssClass="queryButton" tabIndex="3"></asp:button>
				<asp:button id="btnGoToFirstPage" CausesValidation="False" style="Z-INDEX: 108; LEFT: 576px; POSITION: absolute; TOP: 24px" runat="server" Width="24px" Text="|<" CssClass="queryButton" tabIndex="6"></asp:button>
				<asp:button id="btnPreviousPage" CausesValidation="False" style="Z-INDEX: 104; LEFT: 599px; POSITION: absolute; TOP: 24px" runat="server" Width="24px" Text="<" CssClass="queryButton" tabIndex="7"></asp:button>
				<asp:textbox id="txtCountRec" style="Z-INDEX: 107; LEFT: 623px; POSITION: absolute; TOP: 24px" runat="server" Width="24px" tabIndex="8" Enabled="False"></asp:textbox>
				<asp:button id="btnNextPage" CausesValidation="False" style="Z-INDEX: 105; LEFT: 647px; POSITION: absolute; TOP: 24px" runat="server" Width="24px" Text=">" CssClass="queryButton" tabIndex="9"></asp:button>
				<asp:button id="btnGoToLastPage" CausesValidation="False" style="Z-INDEX: 106; LEFT: 670px; POSITION: absolute; TOP: 24px" runat="server" Width="24px" Text=">|" CssClass="queryButton" tabIndex="10"></asp:button>
				<asp:button id="btnSave" CausesValidation="true" style="Z-INDEX: 101; LEFT: 282px; POSITION: absolute; TOP: 16px" runat="server" Width="66px" Height="22px" Text="Save" CssClass="queryButton" tabIndex="4"></asp:button>
				<asp:button id="btnQry" CausesValidation="False" style="Z-INDEX: 102; LEFT: 28px; POSITION: absolute; TOP: 16px" runat="server" Width="62px" Height="22px" Text="Query" CssClass="queryButton" tabIndex="1"></asp:button>
				<asp:button id="btnExecQry" CausesValidation="False" style="Z-INDEX: 103; LEFT: 90px; POSITION: absolute; TOP: 16px" runat="server" Height="22px" Width="130px" Text="Execute Query" CssClass="queryButton" tabIndex="2"></asp:button>
				<fieldset style="Z-INDEX: 111; LEFT: 31px; WIDTH: 357px; POSITION: absolute; TOP: 64px; HEIGHT: 143px"><LEGEND>
						<asp:Label id="lblRlInfo" Runat="server" CssClass="tableHeadingFieldset">Role Information</asp:Label></LEGEND>
					<TABLE id="tblRoleInfo" style="WIDTH: 358px; HEIGHT: 140px" height="140" width="358" border="0" runat="server">
						<TR>
							<TD width="10%">
								<asp:label id="reqRoleID" ForeColor="#cc0000" Runat="server">*</asp:label></TD>
							<TD class="tableLabel" style="WIDTH: 134px" width="134">
								<asp:label id="lblRoleID" tabIndex="11" Runat="server">Role ID</asp:label></TD>
							<TD>
								<cc1:mstextbox id="txtRoleID" tabIndex="12" Runat="server" NumberScale="0" NumberPrecision="4" NumberMinValue="0" NumberMaxValue="9999" MaxLength="4" TextMaskType="msNumeric" Enabled="False" Width="200px" CssClass="textfield"></cc1:mstextbox></TD>
						</TR>
						<TR>
							<TD width="10%">
								<asp:label id="reqRoleNm" ForeColor="#cc0000" Runat="server">*</asp:label></TD>
							<TD class="tableLabel" style="WIDTH: 134px" width="134">
								<asp:label id="lblRoleName" tabIndex="13" Runat="server">Role Name</asp:label></TD>
							<TD>
								<cc1:mstextbox id="txtRoleName" tabIndex="14" Runat="server" MaxLength="40" TextMaskType="msAlfaNumericWithSpace" Width="201px" CssClass="textfield"></cc1:mstextbox></TD>
						</TR>
						<TR>
							<TD width="10%"></TD>
							<TD class="tableLabel" style="WIDTH: 134px" width="134">
								<asp:label id="lblRoleDesc" tabIndex="15" Runat="server" Width="127px">Role Description</asp:label></TD>
							<TD>
								<cc1:mstextbox id="txtRoleDesc" tabIndex="16" Runat="server" MaxLength="200" TextMaskType="msAlfaNumericWithSpace" Width="269px" TextMode="MultiLine" CssClass="textfield" Height="43px"></cc1:mstextbox></TD>
						</TR>
					</TABLE>
				</fieldset>
				<fieldset style="Z-INDEX: 112; LEFT: 464px; WIDTH: 210px; POSITION: absolute; TOP: 64px; HEIGHT: 354px"><LEGEND>
						<asp:Label id="lblAddFnRl" Runat="server" CssClass="tableHeadingFieldset">Additional Functional Roles</asp:Label></LEGEND>
					<TABLE id="tblAdditionalRoles" style="WIDTH: 250px; HEIGHT: 323px" cellSpacing="1" cellPadding="1" width="250" border="0" runat="server">
						<TR>
							<TD>
								<iewc:treeview id="funcTree" tabIndex="17" runat="server" Width="220px" Height="217px" AutoPostBack="True"></iewc:treeview></TD>
						</TR>
					</TABLE>
				</fieldset>
				<FIELDSET style="Z-INDEX: 115; LEFT: 31px; WIDTH: 417px; POSITION: absolute; TOP: 240px; HEIGHT: 175px"><LEGEND>
						<asp:Label id="lblprevillage" Runat="server" CssClass="tableHeadingFieldset">Permission Grants</asp:Label></LEGEND>
					<TABLE id="tblButtons" style="WIDTH: 228px; HEIGHT: 43px" cellSpacing="1" cellPadding="1" width="228" border="0" runat="server">
						<TR>
							<TD style="WIDTH: 103px">
								<asp:button id="btnSelectAll" tabIndex="18" Runat="server" CssClass="pageButton" Text="Select ALL"></asp:button></TD>
							<TD>
								<asp:button id="btnUnSelectAll" tabIndex="19" Runat="server" CssClass="pageButton" Text="Unselect ALL"></asp:button></TD>
						</TR>
					</TABLE>
					<TABLE id="tblPermissions" style="WIDTH: 235px; HEIGHT: 101px" cellSpacing="1" cellPadding="1" width="235" border="0" runat="server">
						<TR>
							<TD>
								<asp:checkbox id="chkGrantsIns" tabIndex="20" Runat="server" AutoPostBack="True"></asp:checkbox></TD>
							<TD class="tableLabel">
								<asp:label id="lblInsRec" Runat="server">Insert Record</asp:label></TD>
						</TR>
						<TR>
							<TD>
								<asp:checkbox id="chkGrantsUpd" tabIndex="21" Runat="server" AutoPostBack="True"></asp:checkbox></TD>
							<TD class="tableLabel">
								<asp:label id="lblUpdRec" Runat="server">Update Record</asp:label></TD>
						</TR>
						<TR>
							<TD>
								<asp:checkbox id="chkGrantsDel" tabIndex="22" Runat="server" AutoPostBack="True"></asp:checkbox></TD>
							<TD class="tableLabel">
								<asp:label id="lblDelRec" Runat="server">Delete Record</asp:label></TD>
						</TR>
					</TABLE>
				</FIELDSET>
			</DIV>
			<asp:label id="lblMessages" style="Z-INDEX: 115; LEFT: 48px; POSITION: absolute; TOP: 80px" runat="server" Width="420px" Height="20px" ForeColor="Red"></asp:label>
			<asp:Label id="lblMainHeader" CssClass="mainTitleSize" style="Z-INDEX: 116; LEFT: 33px; POSITION: absolute; TOP: 10px" runat="server" Width="259px" Height="23px">Role Management</asp:Label>
			<DIV id="RoleAdminPanel" style="Z-INDEX: 101; LEFT: -4px; WIDTH: 726px; POSITION: relative; TOP: 46px; HEIGHT: 325px" MS_POSITIONING="GridLayout" runat="server">&nbsp;
				<fieldset style="Z-INDEX: 104; LEFT: 101px; WIDTH: 447px; POSITION: absolute; TOP: 22px; HEIGHT: 193px"><LEGEND>
						<asp:Label id="lblConfirmation" Runat="server" CssClass="tableHeadingFieldset">Confirmation</asp:Label></LEGEND>
					<P align="center">
						<asp:Label id="lblConfirmMsg" runat="server"></asp:Label>
					</P>
					<P align="center">
						<asp:Button id="btnToSaveChanges" runat="server" Width="32px" Text="Yes" Height="24px" CausesValidation="False" CssClass="queryButton"></asp:Button>
						<asp:Button id="btnNotToSave" runat="server" Width="24px" Text="No" Height="24px" CausesValidation="False" CssClass="queryButton"></asp:Button>
						<asp:Button id="btnToCancel" runat="server" Width="64" Text="Cancel" Height="24" CausesValidation="False" CssClass="queryButton"></asp:Button>
					</P>
				</fieldset>
			</DIV>
		</form>
	</body>
</HTML>
