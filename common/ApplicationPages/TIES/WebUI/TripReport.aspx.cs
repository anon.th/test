using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.common.classes;
using com.common.util;
using com.ties.DAL;
using com.common.applicationpages;
using Cambro.Web.DbCombo;
using com.common.RBAC;

namespace com.ties
{
	/// <summary>
	/// Summary description for TripReport.
	/// </summary>
	public class TripReport : BasePage
	{
		#region Web Form Designer generated code

		protected System.Web.UI.WebControls.Button btnQuery;
		protected System.Web.UI.WebControls.Label Label6;
		protected System.Web.UI.WebControls.Label Label9;
		protected System.Web.UI.WebControls.Label lblErrorMessage;
		protected System.Web.UI.WebControls.Button btnExecuteQuery;
		protected System.Web.UI.WebControls.RadioButton rbMonth;
		protected System.Web.UI.WebControls.DropDownList ddMonth;
		protected System.Web.UI.WebControls.Label lblYear;
		protected com.common.util.msTextBox txtYear;
		protected System.Web.UI.WebControls.RadioButton rbPeriod;
		protected com.common.util.msTextBox txtPeriod;
		protected com.common.util.msTextBox txtTo;
		protected System.Web.UI.WebControls.RadioButton rbDate;
		protected com.common.util.msTextBox txtDate;
		protected System.Web.UI.WebControls.Label lblRouteType;
		protected System.Web.UI.WebControls.Label lblRouteCode;
		protected Cambro.Web.DbCombo.DbCombo DbComboPathCode;
		protected System.Web.UI.WebControls.Label Label2;
		protected Cambro.Web.DbCombo.DbCombo DbComboOriginDC;
		protected System.Web.UI.WebControls.Label Label3;
		protected Cambro.Web.DbCombo.DbCombo DbComboDestinationDC;
		protected System.Web.UI.WebControls.Label Label1;
		protected System.Web.UI.HtmlControls.HtmlTable tblExternal;
		protected System.Web.UI.HtmlControls.HtmlTable Table2;
		protected System.Web.UI.HtmlControls.HtmlTable tblInternal;
		protected System.Web.UI.WebControls.Label Label4;
		protected System.Web.UI.WebControls.Label lblDate;
		protected System.Web.UI.WebControls.RadioButton rbReport;
		protected System.Web.UI.WebControls.RadioButton rbSummaryReport;
		protected System.Web.UI.HtmlControls.HtmlTable tbReportType;
		protected System.Web.UI.HtmlControls.HtmlTable tbDates;
		protected System.Web.UI.HtmlControls.HtmlTable tblRouteType;
	
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnQuery.Click += new System.EventHandler(this.btnQuery_Click);
			this.btnExecuteQuery.Click += new System.EventHandler(this.btnExecuteQuery_Click);
			this.rbMonth.CheckedChanged += new System.EventHandler(this.rbMonth_CheckedChanged);
			this.rbPeriod.CheckedChanged += new System.EventHandler(this.rbPeriod_CheckedChanged);
			this.rbDate.CheckedChanged += new System.EventHandler(this.rbDate_CheckedChanged);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
	
		String m_strAppID=null;
		String m_strEnterpriseID=null;
		String m_strCulture=null;

		private void Page_Load(object sender, System.EventArgs e)
		{
			utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings,Page.Session);
			m_strAppID = utility.GetAppID();
			m_strEnterpriseID = utility.GetEnterpriseID();
			m_strCulture = utility.GetUserCulture();

			DbComboPathCode.RegistrationKey = System.Configuration.ConfigurationSettings.AppSettings["DbComboRegKey"];
			DbComboOriginDC.RegistrationKey = System.Configuration.ConfigurationSettings.AppSettings["DbComboRegKey"];
			DbComboDestinationDC.RegistrationKey = System.Configuration.ConfigurationSettings.AppSettings["DbComboRegKey"];

			com.common.classes.User user = RBACManager.GetUser(m_strAppID, m_strEnterpriseID, (String)Session["userID"]);
			ViewState["usertype"] = user.UserType;

			if(!Page.IsPostBack)
			{
				DefaultScreen();
				ddlmonths();
				Session["toRefresh"]=false;
			}

			SetDbComboServerStates();
		}


		private void btnExecuteQuery_Click(object sender, System.EventArgs e)
		{
			lblErrorMessage.Text = "";
			bool icheck =  ValidateValues();
			if(icheck == false)
			{
				String strModuleID = Request.Params["MODID"];
				DataSet dsShipment = GetShipmentQueryData();

				String strUrl = null;
				strUrl = "ReportViewer.aspx";
				Session["FORMID"] = "TripReport";
				Session["SESSION_DS1"] = dsShipment;
				ArrayList paramList = new ArrayList();
				paramList.Add(strUrl);
				String sScript = Utility.GetScript("openParentWindow.js",paramList);
				Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
			}
		}


		private void btnQuery_Click(object sender, System.EventArgs e)
		{
			DefaultScreen();
		}


		private DataTable CreateEmptyDataTable()
		{
			DataTable dtShipment = new DataTable();

			dtShipment.Columns.Add(new DataColumn("report_type", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("start_date", typeof(DateTime)));
			dtShipment.Columns.Add(new DataColumn("end_date", typeof(DateTime)));
			dtShipment.Columns.Add(new DataColumn("route_code", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("origin_dc", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("destination_dc", typeof(string)));

			return dtShipment;
		}


		private DataSet GetShipmentQueryData()
		{
			DataSet dsShipment = new DataSet();
			DataTable dtShipment = CreateEmptyDataTable();
			DataRow dr = dtShipment.NewRow(); 

			if (rbReport.Checked) 
				dr["report_type"] = "TripReport";
			else 
				dr["report_type"] = "SummaryTripReport";
					
			string strMonth = null;
			if (rbMonth.Checked) 
			{	// Month Selected
				strMonth = ddMonth.SelectedItem.Value;
				if (strMonth != "" && txtYear.Text != "") 
				{
					dr["start_date"] = DateTime.ParseExact ("01/"+strMonth.ToString()+"/"+txtYear.Text, "dd/MM/yyyy", null);
					int intLastDay = 0;
					intLastDay = LastDayOfMonth(strMonth, txtYear.Text);
					dr["end_date"] = DateTime.ParseExact (intLastDay.ToString()+"/"+strMonth.ToString()+"/"+txtYear.Text+" 23:59", "dd/MM/yyyy HH:mm", null);
				}
			}
			if (rbPeriod.Checked) 
			{	// Period Selected
				if (txtPeriod.Text != ""  && txtTo.Text != "") 
				{
					dr["start_date"] = DateTime.ParseExact(txtPeriod.Text ,"dd/MM/yyyy",null);
					dr["end_date"] = DateTime.ParseExact(txtTo.Text+" 23:59" ,"dd/MM/yyyy HH:mm",null);
				}
			}
			if (rbDate.Checked) 
			{	// Date Selected
				if (txtDate.Text != "") 
				{
					dr["start_date"] = DateTime.ParseExact(txtDate.Text, "dd/MM/yyyy",null);
					dr["end_date"] = DateTime.ParseExact(txtDate.Text+" 23:59", "dd/MM/yyyy HH:mm",null);
				}
			}
			
			dr["route_code"] = DbComboPathCode.Value;
			dr["origin_dc"] = DbComboOriginDC.Value;
			dr["destination_dc"] = DbComboDestinationDC.Value;
			
			dtShipment.Rows.Add(dr);
			dsShipment.Tables.Add(dtShipment);

			return dsShipment;
		}


		private void DefaultScreen()
		{
			rbMonth.Checked = true;
			rbPeriod.Checked = false;
			rbDate.Checked = false;

			ddMonth.Enabled = true;
			ddMonth.SelectedIndex = -1;
			txtYear.Text = null;
			txtYear.Enabled = true;
			txtPeriod.Text = null;
			txtPeriod.Enabled = false;
			txtTo.Text = null;
			txtTo.Enabled = false;
			txtDate.Text = null;
			txtDate.Enabled = false;



			DbComboPathCode.Text="";
			DbComboPathCode.Value="";

			DbComboOriginDC.Text="";
			DbComboOriginDC.Value="";

			DbComboDestinationDC.Text="";
			DbComboDestinationDC.Value="";

			lblErrorMessage.Text = "";

		}


		private bool ValidateValues()
		{
			bool iCheck=false;
	
			if((ddMonth.SelectedIndex==0)&&(txtYear.Text=="")&&(txtPeriod.Text=="")&&(txtTo.Text=="")&&(txtDate.Text==""))
			{				
				lblErrorMessage.Text=Utility.GetLanguageText(ResourceType.UserMessage,"PLS_ENTER_EST_DVL",utility.GetUserCulture());
				return iCheck=true;
		
			}
			if(rbMonth.Checked == true)
			{
				if((ddMonth.SelectedIndex>0) &&(txtYear.Text==""))
				{
					lblErrorMessage.Text=Utility.GetLanguageText(ResourceType.UserMessage,"MM_YEAR_REQ",utility.GetUserCulture());
					return iCheck=true;
				}
				else if((ddMonth.SelectedIndex==0) &&(txtYear.Text!=""))
				{
					lblErrorMessage.Text=Utility.GetLanguageText(ResourceType.UserMessage,"MM_YEAR_REQ",utility.GetUserCulture());
					return iCheck=true;
				}
				else
				{
					return iCheck=false;
				}
			}
			if(rbPeriod.Checked == true )
			{
				if((txtPeriod.Text!="")&&(txtTo.Text==""))
				{					
					lblErrorMessage.Text=Utility.GetLanguageText(ResourceType.UserMessage,"PLS_ENTER_END_DT",utility.GetUserCulture());
					return iCheck=true;
				}
				else if((txtPeriod.Text=="")&&(txtTo.Text!=""))
				{
					lblErrorMessage.Text=Utility.GetLanguageText(ResourceType.UserMessage,"PLS_START_DT",utility.GetUserCulture());					
					return iCheck=true;			
				}
				else
				{
					return iCheck=false;
				}
			}
			if(rbDate.Checked == true )
			{
				if(txtDate.Text=="")
				{					
					lblErrorMessage.Text=Utility.GetLanguageText(ResourceType.UserMessage,"PLS_ENTER_DT",utility.GetUserCulture());	
					return iCheck=true;				
				}
			}
			
			return iCheck;
		}

		
		private int ValidateData()
		{
			if (txtYear.Text != "" )
			{
				if (Convert.ToInt32(txtYear.Text) < 2000 || Convert.ToInt32(txtYear.Text) > 2099)
				{
					lblErrorMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"YEAR_00_99",utility.GetUserCulture());
					return -1;
				}
			}
			return 1;
		}
		

		private int LastDayOfMonth(String strMonth, String strYear)
		{
			int intLastDay = 0;
			switch (strMonth)
			{
				case "01":
				case "03":
				case "05":
				case "07":
				case "08":
				case "10":
				case "12":
					intLastDay = 31;
					break;
				case "04":
				case "06":
				case "09":
				case "11":
					intLastDay = 30;
					break;
				case "02":
					//intLastDay = 28;
					DateTime dt = System.DateTime.ParseExact("01/02/"+strYear,"dd/MM/yyyy",null);
					DateTime firstDayOfThisMonth = dt.Subtract(TimeSpan.FromDays(dt.Day - 1));
					DateTime firstDayOfNextMonth = firstDayOfThisMonth.AddMonths(1);
					DateTime lastDayOfThisMonth = firstDayOfNextMonth.Subtract(TimeSpan.FromDays(1)) ;

					intLastDay = Int32.Parse(lastDayOfThisMonth.Day.ToString());
					break;
			}
			return intLastDay;
		}


		private void OpenWindowpage(String strUrl, String strFeatures)
		{			
			ArrayList paramList = new ArrayList();
			paramList.Add(strUrl);
			String sScript = Utility.GetScript("openParentWindow.js",paramList);
			Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
		}


		private void rbMonth_CheckedChanged(object sender, System.EventArgs e)
		{
			ddMonth.Enabled = true;
			txtYear.Text = null;
			txtYear.Enabled = true;
			txtPeriod.Text = null;
			txtPeriod.Enabled = false;
			txtTo.Text = null;
			txtTo.Enabled = false;
			txtDate.Text = null;
			txtDate.Enabled = false;
		}


		private void rbPeriod_CheckedChanged(object sender, System.EventArgs e)
		{
			ddMonth.Enabled = false;
			txtYear.Text = null;
			txtYear.Enabled = false;
			txtPeriod.Text = null;
			txtPeriod.Enabled = true;
			txtTo.Text = null;
			txtTo.Enabled = true;
			txtDate.Text = null;
			txtDate.Enabled = false;
		}


		private void rbDate_CheckedChanged(object sender, System.EventArgs e)
		{
			ddMonth.Enabled = false;
			txtYear.Text = null;
			txtYear.Enabled = false;
			txtPeriod.Text = null;
			txtPeriod.Enabled = false;
			txtTo.Text = null;
			txtTo.Enabled = false;
			txtDate.Text = null;
			txtDate.Enabled = true;
		}


		private void rbLongRoute_CheckedChanged(object sender, System.EventArgs e)
		{
			DbComboPathCode.Text="";
			DbComboPathCode.Value="";
		}


		private void rbShortRoute_CheckedChanged(object sender, System.EventArgs e)
		{
			DbComboPathCode.Text="";
			DbComboPathCode.Value="";	
		}


		private void rbAirRoute_CheckedChanged(object sender, System.EventArgs e)
		{
			DbComboPathCode.Text="";
			DbComboPathCode.Value="";
		}


		private void ddlmonths()
		{
			DataTable dtMonths = new DataTable();
			dtMonths.Columns.Add(new DataColumn("Text", typeof(string)));
			dtMonths.Columns.Add(new DataColumn("StringValue", typeof(string)));
		
			DataRow drNilRow = dtMonths.NewRow();
			drNilRow[0] = "";
			drNilRow[1] = "";
			dtMonths.Rows.Add(drNilRow);

			ArrayList listMonthTxt = Utility.GetCodeValues(m_strAppID,m_strCulture, "months", CodeValueType.StringValue);
			foreach(SystemCode typeSysCode in listMonthTxt)
			{
				DataRow drEach = dtMonths.NewRow();
				drEach[0] = typeSysCode.Text;
				drEach[1] = typeSysCode.StringValue;
				dtMonths.Rows.Add(drEach);
			}
			DataView dvMonths = new DataView(dtMonths);

			ddMonth.DataSource = dvMonths;
			ddMonth.DataTextField = "Text";
			ddMonth.DataValueField = "StringValue";
			ddMonth.DataBind();	
		}


		private void SetDbComboServerStates()
		{
			Hashtable hash = new Hashtable();
			DbComboPathCode.ServerState = hash;
			DbComboPathCode.ServerStateSecretString = "kdfsjgnskajh[apq2-4uhg465654435879mnbfdm cxs;q4";
		}


		[Cambro.Web.DbCombo.ResultsMethodAttribute(true)]
		public static object DbComboPathCodeSelect(Cambro.Web.DbCombo.ServerMethodArgs args)
		{
			Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings,HttpContext.Current.Session);
			String strAppID = util.GetAppID();
			String strEnterpriseID = util.GetEnterpriseID();
			
			String strWhereClause=null;
			if(args.ServerState != null && args.ServerState.Authenticate("kdfsjgnskajh[apq2-4uhg465654435879mnbfdm cxs;q4"))
			{
				strWhereClause=strWhereClause+"and path_code in ( ";
				strWhereClause=strWhereClause+"select distinct delivery_route ";
				strWhereClause=strWhereClause+"from Zipcode ";
				strWhereClause=strWhereClause+"where applicationid = '"+strAppID+"' ";
				strWhereClause=strWhereClause+"and enterpriseid = '"+ strEnterpriseID +"') ";
			}
			
			DataSet dataset = com.ties.classes.DbComboDAL.PathCodeQuery(strAppID,strEnterpriseID,args, strWhereClause);	
			return dataset;
		}
		
		
		[Cambro.Web.DbCombo.ResultsMethodAttribute(true)]
		public static object DbComboDistributionCenterSelect(Cambro.Web.DbCombo.ServerMethodArgs args)
		{
			Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings,HttpContext.Current.Session);
			String strAppID = util.GetAppID();
			String strEnterpriseID = util.GetEnterpriseID();
			
			DataSet dataset = com.ties.classes.DbComboDAL.DistributionCenterQuery(strAppID,strEnterpriseID,args);	
			return dataset;
		}


	}
}
