<%@ Import Namespace="com.common.util" %>
<%@ Page language="c#" Codebehind="EnterpriseLogonCopy.aspx.cs" AutoEventWireup="false" Inherits="com.ties.EnterpriseLogonCopy" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>EnterpriseLogon</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<base target="_top">
		<style type="text/css"> TD { FONT-WEIGHT: bold; FONT-SIZE: 11px; COLOR: #ffffff; FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif } INPUT { FONT-SIZE: 11px; FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif } .shdw { FILTER: DropShadow(Color=#000000, OffX=1, OffY=1, Positive=true) } BODY { BACKGROUND-POSITION: 0px 60px; BACKGROUND-IMAGE: url(images/login.jpg); BACKGROUND-REPEAT: repeat-x; BACKGROUND-COLOR: #1b4251 } </style>
		<script language="JavaScript" type="text/JavaScript">
<!--
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);
//-->
		</script>
		<script>
			
		</script>
	</HEAD>
	<body bgcolor="#133d49">
		<form id="EnterpriseLogon" method="post" runat="server">
			<div id="div_export" runat="server">
				<table cellpadding="0" cellspacing="0" border="0" width="80%">
					<tr>
						<td>
							<table cellpadding="0" cellspacing="0" border="0" width="70%">
								<tr>
									<td rowspan="4" width="100" align="center">
										<a href="images/pngaf_logo.jpg"></a>
									</td>
									<td>
										<asp:Label id="Label7" runat="server" Width="97px" Height="16px" ForeColor="Black" Font-Bold="True"
											BorderColor="Transparent" Font-Names="HandelGotDLig" Font-Size="10px"></asp:Label>
									</td>
								</tr>
								<tr>
									<td>
										<asp:Label id="Label1" runat="server" Width="97px" Height="16px" ForeColor="Black" Font-Bold="True"
											BorderColor="Transparent" Font-Names="HandelGotDLig" Font-Size="10px">Service Type :</asp:Label>
										<asp:Label id="Label2" runat="server" Width="97px" Height="16px" ForeColor="Black" Font-Bold="True"
											BorderColor="Transparent" Font-Names="HandelGotDLig" Font-Size="10px"></asp:Label>
									</td>
								</tr>
								<tr>
									<td>
										<asp:Label id="Label3" runat="server" Width="97px" Height="16px" ForeColor="Black" Font-Bold="True"
											BorderColor="Transparent" Font-Names="HandelGotDLig" Font-Size="10px">Location :</asp:Label>
										<asp:Label id="Label4" runat="server" Width="97px" Height="16px" ForeColor="Black" Font-Bold="True"
											BorderColor="Transparent" Font-Names="HandelGotDLig" Font-Size="10px"></asp:Label>
									</td>
								</tr>
								<tr>
									<td>
										<asp:Label id="Label5" runat="server" Width="97px" Height="16px" ForeColor="Black" Font-Bold="True"
											BorderColor="Transparent" Font-Names="HandelGotDLig" Font-Size="10px">Batch Date :</asp:Label>
										<asp:Label id="Label6" runat="server" Width="97px" Height="16px" ForeColor="Black" Font-Bold="True"
											BorderColor="Transparent" Font-Names="HandelGotDLig" Font-Size="10px"></asp:Label>
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td>
							<asp:Label id="Label8" runat="server" Width="97px" Height="16px" ForeColor="Black" Font-Bold="True"
								BorderColor="Transparent" Font-Names="ZebraBarcode" Font-Size="10px">1234567890</asp:Label>
						</td>
					</tr>
					<tr>
						tblData
						<td>
							<table id="tblData" runat="server" cellpadding="0" cellspacing="0" border="0" width="80%">
							</table>
						</td>
					</tr>
				</table>
			</div>
		</form>
	</body>
</HTML>
