using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.common.util;
using com.ties.DAL;
using com.common.classes;
using com.ties.classes;
using com.common.applicationpages;

namespace com.ties
{
	/// <summary>
	/// Summary description for AgentService.
	/// </summary>
	public class AgentService : BasePage
	{
		protected System.Web.UI.WebControls.Label lblTitle;
		protected System.Web.UI.WebControls.Label lblZVASEMessage;
		protected System.Web.UI.WebControls.Button btnGoToFirstPage;
		protected System.Web.UI.WebControls.Button btnPreviousPage;
		protected System.Web.UI.WebControls.TextBox txtCountRec;
		protected System.Web.UI.WebControls.Button btnNextPage;
		protected System.Web.UI.WebControls.Button btnGoToLastPage;
		protected System.Web.UI.WebControls.Label lblAgentName;
		protected System.Web.UI.WebControls.Label lblAgentCode;
		protected System.Web.UI.WebControls.Button btnQuery;
		protected System.Web.UI.WebControls.Button btnExecQry;
		protected System.Web.UI.WebControls.Button btnInsertVAS;
		protected System.Web.UI.WebControls.Button btnZVASEInsert;
		protected System.Web.UI.WebControls.Label lblEXMessage;
		protected System.Web.UI.WebControls.RequiredFieldValidator reqAgentId;
		protected Cambro.Web.DbCombo.DbCombo dbCmbAgentName;
		protected System.Web.UI.WebControls.ValidationSummary PageValidationSummary;
		protected Cambro.Web.DbCombo.DbCombo dbCmbAgentId;
		SessionDS m_sdsAgent = null;
		SessionDS m_sdsService = null;
		SessionDS m_sdsZipcodeServiceExcluded = null;	
		//private static int m_SetSize=10;
		protected System.Web.UI.WebControls.DataGrid dgServiceCodes;
		protected System.Web.UI.WebControls.Label lblServiceMessage;
		protected System.Web.UI.WebControls.DataGrid dgZipcodeServiceExcluded;
		protected System.Web.UI.WebControls.Label lblServiceExcludedTitle;
		protected System.Web.UI.WebControls.RequiredFieldValidator scValidator;
		static private int m_iSetSize = 4;
		private void Page_Load(object sender, System.EventArgs e)
		{
			dbCmbAgentId.RegistrationKey=System.Configuration.ConfigurationSettings.AppSettings["DbComboRegKey"];
			dbCmbAgentName.RegistrationKey=System.Configuration.ConfigurationSettings.AppSettings["DbComboRegKey"];		
			if(!Page.IsPostBack)
			{
				ViewState["AgentOperation"] = Operation.None;
				ViewState["AgentMode"]		= ScreenMode.None;
				ViewState["MovePrevious"]=false;
				ViewState["MoveNext"]=false;
				ViewState["MoveFirst"]=false;
				ViewState["MoveLast"]=false;
				ResetScreenForQuery();
				ResetDetailsServiceGrid();
				ResetDetailsZipcodeGrid();
			}
			else
			{
				m_sdsAgent				= (SessionDS)Session["SESSION_DS3"];
				m_sdsService				= (SessionDS)Session["SESSION_DS1"];
				m_sdsZipcodeServiceExcluded	= (SessionDS)Session["SESSION_DS2"];
				if(Session["ServiceZipCode"] != null)
				{
					Session["ServiceZipCode"]=null;
					ShowCurrentZServiceEPage();
					ViewState["ZServiceEMode"]		= ScreenMode.ExecuteQuery;
				}
				
			}
			SetAgentIDServerStates();
			SetAgentNameServerStates();
			PageValidationSummary.HeaderText = Utility.GetLanguageText(ResourceType.UserMessage, "PLS_ENTER_MISS_FIELD", utility.GetUserCulture());
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			base.OnInit(e);
			InitializeComponent();
			
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnGoToFirstPage.Click += new System.EventHandler(this.btnGoToFirstPage_Click);
			this.btnPreviousPage.Click += new System.EventHandler(this.btnPreviousPage_Click);
			this.btnNextPage.Click += new System.EventHandler(this.btnNextPage_Click);
			this.btnGoToLastPage.Click += new System.EventHandler(this.btnGoToLastPage_Click);
			this.btnQuery.Click += new System.EventHandler(this.btnQuery_Click);
			this.btnExecQry.Click += new System.EventHandler(this.btnExecQry_Click);
			this.btnInsertVAS.Click += new System.EventHandler(this.btnInsertVAS_Click);
			this.btnZVASEInsert.Click += new System.EventHandler(this.btnZVASEInsert_Click);
			this.dbCmbAgentName.SelectedItemChanged += new System.EventHandler(this.dbCmbAgentName_SelectedItemChanged);
			this.dbCmbAgentId.SelectedItemChanged += new System.EventHandler(this.dbCmbAgentId_SelectedItemChanged);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private void SetAgentNameServerStates()
		{						
			String strAgentID=dbCmbAgentId.Value;			
			Hashtable hash = new Hashtable();		
			if (strAgentID !="")
			{
				hash.Add("strAgentID", strAgentID);
			}			
			dbCmbAgentName.ServerState = hash;
			dbCmbAgentName.ServerStateSecretString = "kdfsjgnskajh[apq2-4uhg465654435879mnbfdm cxs;q4";
		}

		private void SetAgentIDServerStates()
		{			
			String strAgentName=dbCmbAgentName.Value;			
			Hashtable hash = new Hashtable();			
			if (strAgentName !="")
			{
				hash.Add("strAgentName", strAgentName);
			}						

			dbCmbAgentId.ServerState = hash;
			dbCmbAgentId.ServerStateSecretString = "kdfsjgnskajh[apq2-4uhg465654435879mnbfdm cxs;q4";			
		}


		private void btnQry_Click(object sender, System.EventArgs e)
		{
			ResetScreenForQuery();
		}
		
		private void InsertAgent(int iRowIndex)
		{
			m_sdsAgent = AgentProfileMgrDAL.GetEmptyAgentDS(1);		
			DataRow drEach =  m_sdsAgent.ds.Tables[0].Rows[0];	
			drEach["agentid"]	= dbCmbAgentId.Text; 
			drEach["agent_name"]= dbCmbAgentName.Text;
			Session["SESSION_DS3"]= m_sdsAgent;
		}
		private void GetAgentRecSet()
		{
			int iStartIndex = Convert.ToInt32(ViewState["currentSet"]) * m_iSetSize;
			DataSet dsQuery = (DataSet)ViewState["QUERY_DS"];
			m_sdsAgent = AgentProfileMgrDAL.GetAgentDS(utility.GetAppID(),utility.GetEnterpriseID(),iStartIndex,m_iSetSize,dsQuery);
			Session["SESSION_DS3"] = m_sdsAgent;
			ViewState["QryRes"] =  m_sdsAgent.QueryResultMaxSize; 			
			decimal pgCnt = (m_sdsAgent.QueryResultMaxSize - 1)/m_iSetSize;
			if(pgCnt < Convert.ToDecimal(ViewState["currentSet"]))
			{
				ViewState["currentSet"] = System.Convert.ToInt32(pgCnt);
			}
		}
		private void btnExecQry_Click(object sender, System.EventArgs e)
		{
			
			InsertAgent(0);
			ViewState["QUERY_DS"] = m_sdsAgent.ds;
			ViewState["currentPage"] = 0;
			ViewState["currentSet"] = 0;
			GetAgentRecSet();			
			if ((decimal)ViewState["QryRes"]!=0)
			{
				DisplayCurrentPage();
				//VAS
				ResetDetailsServiceGridData();
				FillVASDataRow(dgServiceCodes.Items[0],0);
				ViewState["QUERY_DSV"] = m_sdsService.ds;
				dgServiceCodes.CurrentPageIndex = 0;
				ShowCurrentServicePage();
				btnExecQry.Enabled = false;
				//		Utility.EnableButton(ref btnInsert,ButtonType.Insert,true,m_moduleAccessRights);
				lblServiceMessage.Text = "";
				ViewState["ServiceMode"] = ScreenMode.ExecuteQuery;
				ViewState["AgentMode"] = ScreenMode.ExecuteQuery;
				btnGoToFirstPage.Enabled = true;
				btnGoToLastPage.Enabled=true;
				btnPreviousPage.Enabled =true;
				btnNextPage.Enabled=true;  
				btnInsertVAS.Enabled=true;
				EnablingDBCombo(false);
				
			}
			else
			{
				lblServiceMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"NO_RECORD_FOUND",utility.GetUserCulture());
			}
		}  
		private void EnablingDBCombo(bool val)
		{
			dbCmbAgentId.Enabled=val;
			dbCmbAgentName.Enabled=val;
		}
		private void DisplayCurrentPage()
		{
			DataRow drCurrent = m_sdsAgent.ds.Tables[0].Rows[Convert.ToInt32(ViewState["currentPage"])];
						
			if(!drCurrent["agentid"].GetType().Equals(System.Type.GetType("System.DBNull")))
			{
				dbCmbAgentId.Text		= drCurrent["agentid"].ToString();
				dbCmbAgentId.Value		= drCurrent["agentid"].ToString();
				ViewState["AgentID"]	= drCurrent["agentid"].ToString();
				
			}
			if(!drCurrent["agent_name"].GetType().Equals(System.Type.GetType("System.DBNull")))
			{
				dbCmbAgentName.Text		= drCurrent["agent_name"].ToString();
				dbCmbAgentName.Value	= drCurrent["agent_name"].ToString();
			}

		}
		private void btnInsert_Click(object sender, System.EventArgs e)
		{
			btnExecQry.Enabled = false;
			lblServiceMessage.Text = "";
		}
	
		private void btnNextPage_Click(object sender, System.EventArgs e)
		{
			
			int iCurrentPage = Convert.ToInt32(ViewState["currentPage"]);

			if(Convert.ToInt32(ViewState["currentPage"])  < (m_sdsAgent.DataSetRecSize - 1) )
			{
				ViewState["currentPage"] = Convert.ToInt32(ViewState["currentPage"]) + 1;
				DisplayCurrentPage();
				ShowCurrentServicePage();
			}
			else
			{
				decimal iTotalRec = (Convert.ToInt32(ViewState["currentSet"]) * m_iSetSize) + m_sdsAgent.DataSetRecSize;
				if( iTotalRec ==  m_sdsAgent.QueryResultMaxSize)
				{
					EnableNavigationButtons(true,true,false,false);
					return;
				}
				
				ViewState["currentSet"] = Convert.ToInt32(ViewState["currentSet"]) + 1;	
				ViewState["currentPage"] = 0;
				DisplayCurrentPage();
				ShowCurrentServicePage();
			}
			EnableNavigationButtons(true,true,true,true);
			SetAgentIDServerStates();
			SetAgentNameServerStates();
			EnablingDBCombo(false);
		}
		private void btnGoToLastPage_Click(object sender, System.EventArgs e)
		{
			ViewState["currentSet"] = Convert.ToInt32((m_sdsAgent.QueryResultMaxSize - 1))/m_iSetSize;	
			GetAgentRecSet();
			ViewState["currentPage"] = m_sdsAgent.DataSetRecSize - 1;
			DisplayCurrentPage();
			ShowCurrentServicePage();
			EnableNavigationButtons(true,true,false,false);
			SetAgentIDServerStates();
			SetAgentNameServerStates();
			EnablingDBCombo(false);
		}

		private void btnPreviousPage_Click(object sender, System.EventArgs e)
		{
			int iCurrentPage = Convert.ToInt32(ViewState["currentPage"]);

			if(Convert.ToInt32(ViewState["currentPage"])  > 0 )
			{
				ViewState["currentPage"] = Convert.ToInt32(ViewState["currentPage"]) - 1;
				DisplayCurrentPage();	
				ShowCurrentServicePage();
			}
			else
			{
				if( (Convert.ToInt32(ViewState["currentSet"]) - 1) < 0)
				{
					EnableNavigationButtons(false,false,true,true);
					return;
				}
				
				ViewState["currentSet"] = Convert.ToInt32(ViewState["currentSet"]) - 1;	
				GetAgentRecSet();
				ViewState["currentPage"] = m_sdsAgent.DataSetRecSize - 1;
				DisplayCurrentPage();
				ShowCurrentServicePage();
			}
			EnableNavigationButtons(true,true,true,true);
			SetAgentIDServerStates();
			SetAgentNameServerStates();
			EnablingDBCombo(false);
		}

		private void btnGoToFirstPage_Click(object sender, System.EventArgs e)
		{
			ViewState["currentSet"] = 0;	
			GetAgentRecSet();
			ViewState["currentPage"] = 0;
			DisplayCurrentPage();
			ShowCurrentServicePage();
			EnableNavigationButtons(false,false,true,true);		
			SetAgentIDServerStates();
			SetAgentNameServerStates();
			EnablingDBCombo(false);
		}

		private void btnQuery_Click(object sender, System.EventArgs e)
		{
			ResetScreenForQuery();
			ResetDetailsServiceGrid();
			ResetDetailsZipcodeGrid();
			btnInsertVAS.Enabled=true;

		}

		private void EnableNavigationButtons(bool bMoveFirst, bool bMoveNext,bool bEnableMovePrevious, bool bMoveLast)
		{
			btnGoToFirstPage.Enabled	= bMoveFirst;
			btnPreviousPage.Enabled		= bMoveNext;
			btnNextPage.Enabled			= bEnableMovePrevious;
			btnGoToLastPage.Enabled		= bMoveLast;
		}

		private void ResetScreenForQuery()
		{
		
			btnExecQry.Enabled=true;
			m_sdsAgent = AgentProfileMgrDAL.GetEmptyAgentDS(1);
			Session["SESSION_DS3"] = m_sdsAgent;		
			ViewState["AgentOperation"] = Operation.None;
			ViewState["AgentMode"]		= ScreenMode.Query;
			ViewState["ServiceOperation"]	= Operation.None;
			ViewState["VASMode"]		= ScreenMode.Query;
			ViewState["ZServiceEMode"]		= ScreenMode.None;
			ViewState["ZVASEOperation"] = Operation.None;
			EnableNavigationButtons(false,false,false,false);
			m_sdsZipcodeServiceExcluded = AgentProfileMgrDAL.GetEmptyZipcodeServiceExcludedDS(1);
			Session["SESSION_DS2"]	= m_sdsZipcodeServiceExcluded;
			btnExecQry.Enabled = true;
			//Utility.EnableButton(ref btnInsert,ButtonType.Insert,true,m_moduleAccessRights);
			lblServiceMessage.Text = "";
			dbCmbAgentId.Text="";
			dbCmbAgentName.Text="";
			dbCmbAgentId.Value="";
			dbCmbAgentName.Value="";
			EnablingDBCombo(true);
			EnableNavigationButtons(false,false,false,false);
			ResetDetailsZipcodeGrid();
		
		}
		[Cambro.Web.DbCombo.ResultsMethod(true)]
		public static object AgentNameServerMethod(Cambro.Web.DbCombo.ServerMethodArgs args)
		{
			Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings,HttpContext.Current.Session);
			String strAppID = util.GetAppID();
			String strEnterpriseID = util.GetEnterpriseID();
	
			String strAgentID= "";

			if(args.ServerState != null && args.ServerState.Authenticate("kdfsjgnskajh[apq2-4uhg465654435879mnbfdm cxs;q4"))
			{				
				if (args.ServerState["strAgentID"] != null && args.ServerState["strAgentID"].ToString().Length > 0 )
				{
					strAgentID = (args.ServerState["strAgentID"].ToString());
				}
			}	

			DataSet dataset = DbComboDAL.PayerNameQuery(strAppID,strEnterpriseID,args, "A", strAgentID);	
			return dataset;                
		}
		[Cambro.Web.DbCombo.ResultsMethodAttribute(true)]		
		public static object AgentIdServerMethod(Cambro.Web.DbCombo.ServerMethodArgs args)
		{
			Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings,HttpContext.Current.Session);
			String strAppID = util.GetAppID();
			String strEnterpriseID = util.GetEnterpriseID();
						
			String strAgentName="";
			if(args.ServerState != null && args.ServerState.Authenticate("kdfsjgnskajh[apq2-4uhg465654435879mnbfdm cxs;q4"))
			{			
				if(args.ServerState["strAgentName"] != null && args.ServerState["strAgentName"].ToString().Length > 0)
				{
					strAgentName = args.ServerState["strAgentName"].ToString();					
				}	
			}
			
			DataSet dataset = DbComboDAL.PayerIDQuery(strAppID,strEnterpriseID,args, "A", strAgentName);	
			return dataset;
		}

		#region Code for Service
		/// <summary>
		/// VAS
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void btnInsertVAS_Click(object sender, System.EventArgs e)
		{
			lblServiceMessage.Text="";
			btnInsertVAS.Enabled=false;
			if( m_sdsService.ds.Tables[0].Rows.Count >= dgServiceCodes.PageSize)
			{
				ResetDetailsServiceGridData();
			}

			ViewState["ServiceMode"] = ScreenMode.Insert;
			ViewState["ServiceOperation"] = Operation.Insert;
			AddRowInServiceGrid();
			dgServiceCodes.EditItemIndex = m_sdsService.ds.Tables[0].Rows.Count - 1;
			BindServiceGrid();
			getPageControls(Page);
		}
		
		public void dgServiceCodes_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			lblServiceMessage.Text="";
			Label lblServiceCode = (Label)dgServiceCodes.SelectedItem.FindControl("lblServiceCode");
			msTextBox txtServiceCode = (msTextBox)dgServiceCodes.SelectedItem.FindControl("txtServiceCode");
			String strServiceCode = null;

			if(lblServiceCode != null)
			{
				strServiceCode = lblServiceCode.Text;
			}

			if(txtServiceCode != null)
			{
				strServiceCode = txtServiceCode.Text;
			}
			if((int)ViewState["ServiceMode"] != (int)ScreenMode.Insert || (int)ViewState["ServiceOperation"] != (int)Operation.Insert)
			{
				
				ViewState["CurrentService"] = strServiceCode;
				dgZipcodeServiceExcluded.CurrentPageIndex = 0;

				Logger.LogDebugInfo("VASZipcodeExcluded","dgServiceCodes_SelectedIndexChanged","INF004","updating data grid..."+dgServiceCodes.SelectedIndex+"  : "+strServiceCode);
				ShowCurrentZServiceEPage();

				ViewState["ZServiceEMode"] = ScreenMode.ExecuteQuery;

				if((int)ViewState["ServiceMode"] != (int)ScreenMode.Insert && (int)ViewState["ServiceOperation"] != (int)Operation.Insert)
				{
					//	Utility.EnableButton(ref btnInsert,ButtonType.Insert,true,m_moduleAccessRights);
				}

				if((int)ViewState["ServiceMode"] == (int)ScreenMode.Insert && dgServiceCodes.EditItemIndex == dgServiceCodes.SelectedIndex && (int)ViewState["ServiceOperation"] == (int)Operation.Insert)
				{
					btnZVASEInsert.Enabled = false;
				}
				else
				{
					btnZVASEInsert.Enabled = true;
				}
			}
			lblServiceMessage.Text	= "";
			lblEXMessage.Text	= "";
			
		}

		protected void dgServiceCodes_Edit(object sender, DataGridCommandEventArgs e)
		{
			if((int)ViewState["ServiceMode"] == (int)ScreenMode.Insert && (int) ViewState["ServiceOperation"] == (int)Operation.Insert && dgServiceCodes.EditItemIndex > 0)
			{
				m_sdsService.ds.Tables[0].Rows.RemoveAt(dgServiceCodes.EditItemIndex);
				m_sdsService.QueryResultMaxSize--;
				dgServiceCodes.CurrentPageIndex = 0;
			}
			dgServiceCodes.EditItemIndex = e.Item.ItemIndex;
			ViewState["ServiceOperation"] = Operation.Update;
			BindServiceGrid();
			lblServiceMessage.Text = "";
			Logger.LogDebugInfo("VASZipcodeExcluded","dgServiceCodes_Edit","INF004","updating data grid...");
			SetAgentIDServerStates();
			SetAgentNameServerStates();
		}
		
		public void dgServiceCodes_Update(object sender, DataGridCommandEventArgs e)
		{
			lblServiceMessage.Text="";
			//String strAgentID=(String)ViewState["AgentID"];
			String strAgentID = dbCmbAgentId.Value;
			FillVASDataRow(e.Item,e.Item.ItemIndex);
			int intChkRet = CheckTransit(e.Item.ItemIndex);
			if (intChkRet < 0)
			{
				return;

			}
			int iOperation = (int)ViewState["ServiceOperation"];
			switch(iOperation)
			{

				case (int)Operation.Update:

					DataSet dsToUpdate = m_sdsService.ds.GetChanges();
					AgentProfileMgrDAL.ModifyAgentServiceCodeDS(utility.GetAppID(),utility.GetEnterpriseID(),strAgentID,dsToUpdate);
					lblServiceMessage.Text=Utility.GetLanguageText(ResourceType.UserMessage,"UPD_SUCCESSFULLY",utility.GetUserCulture());
					//	Utility.EnableButton(ref btnInsert,ButtonType.Insert,true,m_moduleAccessRights);
					m_sdsService.ds.Tables[0].Rows[e.Item.ItemIndex].AcceptChanges();
					Logger.LogDebugInfo("ServiceZipcodeExcluded","dgServiceCodes_OnUpdate","INF004","update in modify mode..");
					btnInsertVAS.Enabled=true;
					break;
				case (int)Operation.Insert:
					DataSet dsToInsert = m_sdsService.ds.GetChanges();
					try
					{
						AgentProfileMgrDAL.AddAgentServiceCodeDS(utility.GetAppID(),utility.GetEnterpriseID(),strAgentID,dsToInsert);
						lblServiceMessage.Text=Utility.GetLanguageText(ResourceType.UserMessage,"INS_SUCCESSFULLY",utility.GetUserCulture());
						//		Utility.EnableButton(ref btnInsert,ButtonType.Insert,true,m_moduleAccessRights);
						btnInsertVAS.Enabled=true;
									
									
					}
					catch(ApplicationException appException)
					{
						String strMsg = appException.Message;
						strMsg = appException.InnerException.Message;
						strMsg = appException.InnerException.InnerException.Message;
						if(strMsg.IndexOf("duplicate key") != -1 )
						{
							lblServiceMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"DUP_KEY_FOUND_VAS",utility.GetUserCulture());
							return;
						}
						if(strMsg.IndexOf("unique constraint ") != -1 )
						{
							lblServiceMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"DUP_KEY_FOUND_VAS",utility.GetUserCulture());
							return;
						}
						
					}
					m_sdsService.ds.Tables[0].Rows[e.Item.ItemIndex].AcceptChanges();
					Logger.LogDebugInfo("VASZipcodeExcluded","dgServiceCodes_OnUpdate","INF004","update in Insert mode");
					break;
			}
			dgServiceCodes.EditItemIndex = -1;
			ViewState["ServiceOperation"] = Operation.None;
			lblZVASEMessage.Text="";
			BindServiceGrid();
		}

		protected void dgServiceCodes_Cancel(object sender, DataGridCommandEventArgs e)
		{
			lblServiceMessage.Text="";

			dgServiceCodes.EditItemIndex = -1;

			if((int)ViewState["ServiceMode"] == (int)ScreenMode.Insert && (int)ViewState["ServiceOperation"] == (int)Operation.Insert)
			{
				m_sdsService.ds.Tables[0].Rows.RemoveAt(e.Item.ItemIndex);
				m_sdsService.QueryResultMaxSize--;
				dgServiceCodes.CurrentPageIndex = 0;
			}
			ViewState["ServiceOperation"] = Operation.None;
			BindServiceGrid();
			//		Utility.EnableButton(ref btnInsert,ButtonType.Insert,true,m_moduleAccessRights);
			lblServiceMessage.Text = "";
			Logger.LogDebugInfo("VASZipcodeExcluded","dgServiceCodes_Cancel","INF004","updating data grid...");			
		}

		public void dgServiceCodes_Delete(object sender, DataGridCommandEventArgs e)
		{
			String strAgentID=(String)ViewState["AgentID"];
			msTextBox txtServiceCode = (msTextBox)e.Item.FindControl("txtServiceCode");
			Label lblServiceCode = (Label)e.Item.FindControl("lblServiceCode");
			String strServiceCode = null;
			if(txtServiceCode != null)
			{
				strServiceCode = txtServiceCode.Text;
			}

			if(lblServiceCode != null)
			{
				strServiceCode = lblServiceCode.Text;
			}

			try
			{
				AgentProfileMgrDAL.DeleteAgentServiceCodeDS(utility.GetAppID(),utility.GetEnterpriseID(),strAgentID,strServiceCode);
				lblServiceMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"DLD_SUCCESSFULLY",utility.GetUserCulture());
				
			}
			catch(ApplicationException appException)
			{
				String strMsg = appException.Message;
				if(strMsg.IndexOf("FK") != -1)
				{
					lblServiceMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"ERR_DEL_VAS_TRANS",utility.GetUserCulture());
				}
				if(strMsg.IndexOf("child record") != -1)
				{
					lblServiceMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"ERR_DEL_VAS_TRANS",utility.GetUserCulture());
				}
				return;

			}

			if((int)ViewState["ServiceMode"] == (int)ScreenMode.Insert)
			{
				m_sdsService.ds.Tables[0].Rows.RemoveAt(e.Item.ItemIndex);
				m_sdsService.QueryResultMaxSize--;
				dgServiceCodes.CurrentPageIndex = 0;

				if((int) ViewState["ServiceOperation"] == (int)Operation.Insert && dgServiceCodes.EditItemIndex > 0)
				{
					m_sdsService.ds.Tables[0].Rows.RemoveAt(dgServiceCodes.EditItemIndex-1);
					m_sdsService.QueryResultMaxSize--;
				}
				dgServiceCodes.EditItemIndex = -1;
				dgServiceCodes.SelectedIndex = -1;
				BindServiceGrid();
				ResetDetailsZipcodeGrid();
			}
			else
			{
				ShowCurrentServicePage();
			}
			Logger.LogDebugInfo("VASZipcodeExcluded","dgServiceCodes_Delete","INF004","Deleted row in VAS_Code table..");
			ViewState["ServiceOperation"] = Operation.None;
			lblEXMessage.Text="";
			//	Utility.EnableButton(ref btnInsert,ButtonType.Insert,true,m_moduleAccessRights);
		}

		private void BindServiceGrid()
		{
			dgServiceCodes.VirtualItemCount = System.Convert.ToInt32(m_sdsService.QueryResultMaxSize);
			dgServiceCodes.DataSource = m_sdsService.ds;
			dgServiceCodes.DataBind();
			Session["SESSION_DS1"] = m_sdsService;
		}

		private void AddRowInServiceGrid()
		{
			AgentProfileMgrDAL.AddAgentNewRowInServiceCodeDS(m_sdsService);
			Session["SESSION_DS1"] = m_sdsService;
		}

		protected void dgServiceCodes_Bound(object sender, DataGridItemEventArgs e)
		{
			SetAgentIDServerStates();
			SetAgentNameServerStates();

			if(e.Item.ItemIndex == -1)
			{
				return;
			}

			DataRow drSelected = m_sdsService.ds.Tables[0].Rows[e.Item.ItemIndex];
			if(drSelected.RowState == DataRowState.Deleted)
			{
				return;
			}

			msTextBox txtServiceCode = (msTextBox)e.Item.FindControl("txtServiceCode");


			int iOperation = (int) ViewState["ServiceOperation"];
			if(txtServiceCode != null && iOperation == (int)Operation.Update)
			{
				txtServiceCode.Enabled = false;
				e.Item.Cells[3].Enabled=false;
			}
			

		}

		protected void dgServiceCodes_PageChange(object sender, DataGridPageChangedEventArgs e)
		{
			lblServiceMessage.Text="";
			dgServiceCodes.CurrentPageIndex = e.NewPageIndex;
			ShowCurrentServicePage();
			Logger.LogDebugInfo("VASZipcodeExcluded","dgServiceCodes_PageChange","INF009","On Page Change "+e.NewPageIndex);
		}
		
		public void dgServiceCodes_Button(object sender, DataGridCommandEventArgs e)
		{
			lblServiceMessage.Text="";
			String strCmdNm = e.CommandName;
			
			if(strCmdNm.Equals("search"))
			{
				int iOperation = (int) ViewState["ServiceOperation"];
				if(dgServiceCodes.EditItemIndex  != e.Item.ItemIndex)
				{
					return;
				}
				if(iOperation == (int)Operation.Update)
				{
					return;
				}
				msTextBox txtServiceCode = (msTextBox)e.Item.FindControl("txtServiceCode");
				TextBox txtServiceDescription = (TextBox)e.Item.FindControl("txtServiceDescription");				
				String strServiceClientID = null;
				String strServiceDescriptionClientID = null;				
				String strServiceCode	= null;
				String strServiceDesc	= null;
				if(txtServiceCode != null)
				{
					strServiceClientID	= txtServiceCode.ClientID;
					strServiceCode		= txtServiceCode.Text;
				}
				if(txtServiceDescription != null)
				{
					strServiceDescriptionClientID	= txtServiceDescription.ClientID;
					strServiceDesc					= txtServiceDescription.Text;
				}				
				String sUrl = "ServiceCodePopup.aspx?FORMID=AgentService&SERVICECODE="+strServiceCode+"&SERVICEDESC="+strServiceDesc+"&CODEID="+strServiceClientID+"&DESCID="+strServiceDescriptionClientID;
				ArrayList paramList = new ArrayList();
				paramList.Add(sUrl);
				String sScript = Utility.GetScript("openWindowParam.js",paramList);
				Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);	
			}
		
		}
		private void ShowCurrentServicePage()
		{
			String strAgentID=(String)ViewState["AgentID"];
			int iStartIndex = dgServiceCodes.CurrentPageIndex * dgServiceCodes.PageSize;
			DataSet dsQuery = (DataSet)ViewState["QUERY_DSV"];
			m_sdsService = AgentProfileMgrDAL.GetAgentServiceCodeDS(utility.GetAppID(),utility.GetEnterpriseID(),strAgentID,iStartIndex,dgServiceCodes.PageSize,dsQuery);
			int pgCnt = (Convert.ToInt32(m_sdsService.QueryResultMaxSize) - 1)/dgServiceCodes.PageSize;
			if(pgCnt < dgServiceCodes.CurrentPageIndex)
			{
					dgServiceCodes.CurrentPageIndex = System.Convert.ToInt32(pgCnt);
			}
			dgServiceCodes.SelectedIndex = -1;
			dgServiceCodes.EditItemIndex = -1;
			//lblServiceMessage.Text	= "";
			lblEXMessage.Text	= "";
			BindServiceGrid();
			ResetDetailsZipcodeGrid();
		}

		private void FillVASDataRow(DataGridItem item, int drIndex)
		{
			msTextBox txtServiceCode = (msTextBox)item.FindControl("txtServiceCode");
			if(txtServiceCode != null)
			{
				DataRow drCurrent = m_sdsService.ds.Tables[0].Rows[drIndex];
				drCurrent["service_code"] = txtServiceCode.Text;
			}

			TextBox txtServiceDescription = (TextBox)item.FindControl("txtServiceDescription");
			if(txtServiceDescription != null)
			{
				DataRow drCurrent = m_sdsService.ds.Tables[0].Rows[drIndex];
				drCurrent["service_description"] = txtServiceDescription.Text;
			}

			TextBox txtPercentDiscount = (TextBox)item.FindControl("txtPercentDiscount");
			if(txtPercentDiscount != null )
			{
				DataRow drCurrent = m_sdsService.ds.Tables[0].Rows[drIndex];
				if(txtPercentDiscount.Text == "")
				{
					drCurrent["service_charge_percent"] = System.DBNull.Value;
				}
				else 
				{
					try
					{
						drCurrent["service_charge_percent"] = txtPercentDiscount.Text;
					}
					catch(Exception e)
					{
						String msg = e.ToString();
						drCurrent["service_charge_percent"] = "-101";
						//lblSCMessage.Text = "Enter value between -100 to 100 for Service Charge %";
					}
					//drCurrent["service_charge_percent"] = txtPercentDiscount.Text;
				}
			}

			TextBox txtServiceAmt = (TextBox)item.FindControl("txtServiceAmt");
			if(txtServiceAmt != null )
			{
				DataRow drCurrent = m_sdsService.ds.Tables[0].Rows[drIndex];
				if(txtServiceAmt.Text == "")
				{
					drCurrent["service_charge_amt"] = System.DBNull.Value;
				}
				else 
				{
					try
					{
						drCurrent["service_charge_amt"] = txtServiceAmt.Text;
					}
					catch(Exception e)
					{
						String msg = e.ToString();
						drCurrent["service_charge_amt"] = "-101";
					}
				}
			}			
			msTextBox txtCommitTime = (msTextBox)item.FindControl("txtCommitTime");
			if(txtCommitTime != null )
			{
				DataRow drCurrent = m_sdsService.ds.Tables[0].Rows[drIndex];
				if(txtCommitTime.Text == "")
				{
					drCurrent["commit_time"] = System.DBNull.Value;
				}
				else
				{
					drCurrent["commit_time"] = txtCommitTime.Text;
				}
				
			}

			msTextBox txtTransitDay = (msTextBox)item.FindControl("txtTransitDay");
			if(txtTransitDay != null)
			{
				DataRow drCurrent = m_sdsService.ds.Tables[0].Rows[drIndex];
				if(txtTransitDay.Text == "")
				{
					drCurrent["transit_day"]= System.DBNull.Value;
				}
				else
				{
					drCurrent["transit_day"] = txtTransitDay.Text;
				}
				
			}

			msTextBox txtTransitHour = (msTextBox)item.FindControl("txtTransitHour");
			if(txtTransitHour != null)
			{
				DataRow drCurrent = m_sdsService.ds.Tables[0].Rows[drIndex];
				if(txtTransitHour.Text == "")
				{
					drCurrent["transit_hour"] = System.DBNull.Value;
				}
				else
				{
					drCurrent["transit_hour"] = txtTransitHour.Text;
				}
				
			}


		}

		private void ResetDetailsServiceGrid()
		{
			m_sdsService = AgentProfileMgrDAL.GetAgentEmptyServiceCodeDS(0);			// 11/11/2002 
			Session["SESSION_DS1"]	= m_sdsService;		
			dgServiceCodes.EditItemIndex = 0;
			dgServiceCodes.CurrentPageIndex = 0;
			BindServiceGrid();
		}
		private void ResetDetailsServiceGridData()
		{
			m_sdsService = AgentProfileMgrDAL.GetAgentEmptyServiceCodeDS(1);			// 11/11/2002 
			Session["SESSION_DS1"]	= m_sdsService;		
			dgServiceCodes.EditItemIndex = 0;
			dgServiceCodes.CurrentPageIndex = 0;
			BindServiceGrid();
		}

		private int CheckTransit(int drIndex)
		{
			int intRet = 1;
			int intTHour = 0;
			int intTDay = 0;
			int intChargeP=0;
			int intChargeD=0;
			String strTime = null;
			DataRow drCurrent = m_sdsService.ds.Tables[0].Rows[drIndex];

			if(Utility.IsNotDBNull(drCurrent["transit_day"]) && drCurrent["transit_day"].ToString() !="")
			{
				intTDay = System.Convert.ToInt32(drCurrent["transit_day"]);
			}
			if(Utility.IsNotDBNull(drCurrent["transit_hour"]) && drCurrent["transit_hour"].ToString() !="")
			{
				intTHour = System.Convert.ToInt32(drCurrent["transit_hour"]);
			}
			if(Utility.IsNotDBNull(drCurrent["commit_time"]) && drCurrent["commit_time"].ToString() !="")
			{
				DateTime dtCommitTime = (DateTime) drCurrent["commit_time"];
				strTime = dtCommitTime.Hour.ToString() + dtCommitTime.Minute.ToString();
			}

			if ((intTDay == 0) && (intTHour == 0))
			{
				lblServiceMessage.Text =Utility.GetLanguageText(ResourceType.UserMessage,"PLS_ENTER_TRANSIT_DT",utility.GetUserCulture());
				intRet = -1;
			}
			else if ((intTDay > 0) && (intTHour > 0))
			{
				lblServiceMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"PLS_ENTER_TRANSIT_DT",utility.GetUserCulture());
				intRet = -1;
			}
			else if ((intTDay > 0) && ((strTime == null) || strTime == "00"))
			{
				lblServiceMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"PLS_ENTER_COMMIT_TIME",utility.GetUserCulture());
				intRet = -1;
			}

			if(Utility.IsNotDBNull(drCurrent["service_charge_percent"]) && drCurrent["service_charge_percent"].ToString() !="")
			{
				intChargeP = System.Convert.ToInt32(drCurrent["service_charge_percent"]);
			}
			if(Utility.IsNotDBNull(drCurrent["service_charge_amt"]) && drCurrent["service_charge_amt"].ToString() !="")
			{
				intChargeD = System.Convert.ToInt32(drCurrent["service_charge_amt"]);
			}
			if ((intChargeD == 0) && (intChargeP == 0))
			{
				lblServiceMessage.Text= Utility.GetLanguageText(ResourceType.UserMessage,"PLS_ENTER_SERVICE_CHR",utility.GetUserCulture());
				intRet = -1;
			}
			if((intChargeD != 0) && (intChargeP != 0))
			{
				lblServiceMessage.Text= Utility.GetLanguageText(ResourceType.UserMessage,"PLS_ENTER_SERVICE_CHR_EITHER",utility.GetUserCulture());
				intRet = -1;
			}

			return intRet;
		}

		#endregion
		#region Code for VAS excluded zipcode
		/// ZVASE Grid Methos..
		/// 
	
		private void btnZVASEInsert_Click(object sender, System.EventArgs e)
		{
			lblEXMessage.Text		= "";
			lblServiceMessage.Text	= "";
			String strAgentID	= (String)ViewState["AgentID"];
			String strServicCode	= (String)ViewState["CurrentService"];				
			String sUrl = "AgentZipCodePopup.aspx?FORMID=AgentService&SERVICECODE="+strServicCode+"&AGENTID="+strAgentID;
			ArrayList paramList = new ArrayList();
			paramList.Add(sUrl);
			String sScript = Utility.GetScript("openParentWindow.js",paramList);
			Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
			
		}

		protected void dgZipcodeServiceExcluded_Edit(object sender, DataGridCommandEventArgs e)
		{
			dgZipcodeServiceExcluded.EditItemIndex = e.Item.ItemIndex;
			ViewState["ZVASEOperation"] = Operation.Update;
			BindZVASEGrid();
			lblZVASEMessage.Text = "";
			Logger.LogDebugInfo("VASZipcodeExcluded","dgZipcodeServiceExcluded_Edit","INF004","updating data grid...");			
		}

		public void dgZipcodeServiceExcluded_Update(object sender, DataGridCommandEventArgs e)
		{
			FillZVASEDataRow(e.Item,e.Item.ItemIndex);

			int iOperation = (int)ViewState["ZVASEOperation"];

			String strVASCode = (String)ViewState["CurrentService"];
			switch(iOperation)
			{

				case (int)Operation.Insert:

					DataSet dsToInsert = m_sdsZipcodeServiceExcluded.ds.GetChanges();
					try
					{
						SysDataMgrDAL.AddZipcodeVASExcludedDS(utility.GetAppID(),utility.GetEnterpriseID(),strVASCode,dsToInsert);
						lblZVASEMessage.Text=Utility.GetLanguageText(ResourceType.UserMessage,"INS_SUCCESSFULLY",utility.GetUserCulture());
						btnZVASEInsert.Enabled = true;
						
					}
					catch(ApplicationException appException)
					{
						String strMsg = appException.Message;
						strMsg = appException.InnerException.Message;
						strMsg = appException.InnerException.InnerException.Message;
						if(strMsg.IndexOf("duplicate key") != -1 )
						{
							lblZVASEMessage.Text = "Duplicate Zipcode Code is not allowed.";
						} 
						else if(strMsg.IndexOf("FOREIGN KEY") != -1 || strMsg.IndexOf("parent key") != -1)
						{
							lblZVASEMessage.Text = "Zipcode not found. Please create Zipcode in Zipcode Master.";
						}
						else if(strMsg.IndexOf("unique") != -1 )
						{
							lblZVASEMessage.Text = "Duplicate Zipcode Code is not allowed.";
						} 
						else if(strMsg.IndexOf("APPDB.SYS_C002182") != -1 )
						{
							//violated - parent key not found //Zipcode not present in parent
							lblZVASEMessage.Text = "Zipcode not found. Please create Zipcode in Zipcode Master.";
						}
						else
						{
							lblZVASEMessage.Text = strMsg;
						}
						return;
						
					}

					m_sdsZipcodeServiceExcluded.ds.Tables[0].Rows[e.Item.ItemIndex].AcceptChanges();
					Logger.LogDebugInfo("VASZipcodeExcluded","dgZipcodeServiceExcluded_OnUpdate","INF004","update in Insert mode");
					break;

			}
			dgZipcodeServiceExcluded.EditItemIndex = -1;
			ViewState["ZVASEOperation"] = Operation.None;
			BindZVASEGrid();		
			lblServiceMessage.Text = "";
		}

		protected void dgZipcodeServiceExcluded_Cancel(object sender, DataGridCommandEventArgs e)
		{
			dgZipcodeServiceExcluded.EditItemIndex = -1;
			if((int)ViewState["ZServiceEMode"] == (int)ScreenMode.Insert && (int)ViewState["ZVASEOperation"] == (int)Operation.Insert)
			{
				m_sdsZipcodeServiceExcluded.ds.Tables[0].Rows.RemoveAt(e.Item.ItemIndex);
				m_sdsZipcodeServiceExcluded.QueryResultMaxSize--;
				dgZipcodeServiceExcluded.CurrentPageIndex = 0;
			}

			btnZVASEInsert.Enabled = true;
		
			lblZVASEMessage.Text = "";

			ViewState["ZVASEOperation"] = Operation.None;
			BindZVASEGrid();
			Logger.LogDebugInfo("VASZipcodeExcluded","dgZipcodeServiceExcluded_Cancel","INF004","updating data grid...");			
		}

		protected void dgZipcodeServiceExcluded_Delete(object sender, DataGridCommandEventArgs e)
		{
			String strServiceCode = (String)ViewState["CurrentService"];
			String strAgentID	= (String)ViewState["AgentID"];
			msTextBox txtZipcode = (msTextBox)e.Item.FindControl("txtZipcode");
			Label lblZipcode = (Label)e.Item.FindControl("lblZipcode");
			String strZipcode = null;
			if(txtZipcode != null)
			{
				strZipcode = txtZipcode.Text;
			}

			if(lblZipcode != null)
			{
				strZipcode = lblZipcode.Text;
			}
			try
			{
				AgentProfileMgrDAL.DeleteAgentZipcodeServiceExcludedDS(utility.GetAppID(),utility.GetEnterpriseID(),strAgentID,strServiceCode,strZipcode);				
				lblEXMessage.Text=Utility.GetLanguageText(ResourceType.UserMessage,"DLD_SUCCESSFULLY",utility.GetUserCulture());

			}
			catch(ApplicationException appException)
			{
				String strMsg = appException.Message;
				if(strMsg.IndexOf("FK") != -1)
				{
					lblEXMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"ERROR",utility.GetUserCulture());
				}
				if(strMsg.IndexOf("child record") != -1)
				{
					lblEXMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"ERROR",utility.GetUserCulture());
				}
				return;

			}
			if((int)ViewState["ZServiceEMode"] == (int)ScreenMode.Insert)
			{
				m_sdsZipcodeServiceExcluded.ds.Tables[0].Rows.RemoveAt(e.Item.ItemIndex);
				m_sdsZipcodeServiceExcluded.QueryResultMaxSize--;
				dgZipcodeServiceExcluded.CurrentPageIndex = 0;

				if((int) ViewState["ZVASEOperation"] == (int)Operation.Insert && dgZipcodeServiceExcluded.EditItemIndex > 0)
				{
					m_sdsZipcodeServiceExcluded.ds.Tables[0].Rows.RemoveAt(dgZipcodeServiceExcluded.EditItemIndex-1);
					m_sdsZipcodeServiceExcluded.QueryResultMaxSize--;
				}
				dgZipcodeServiceExcluded.EditItemIndex = -1;
				BindZVASEGrid();
			}
			else
			{
				ShowCurrentZServiceEPage();
			}

			Logger.LogDebugInfo("VASZipcodeExcluded","dgZipcodeServiceExcluded_Delete","INF004","Deleted row in Zipcode_VAS_Excluded table..");

			ViewState["ZVASEOperation"] = Operation.None;
			btnZVASEInsert.Enabled = true;
			lblServiceMessage.Text = "";
		}

		private void BindZVASEGrid()
		{
			dgZipcodeServiceExcluded.VirtualItemCount = System.Convert.ToInt32(m_sdsZipcodeServiceExcluded.QueryResultMaxSize);
			dgZipcodeServiceExcluded.DataSource = m_sdsZipcodeServiceExcluded.ds;
			dgZipcodeServiceExcluded.DataBind();
			Session["SESSION_DS2"] = m_sdsZipcodeServiceExcluded;
		}

		private void AddRowInZVASEGrid()
		{
			SysDataMgrDAL.AddNewRowInZipcodeVASExcludedDS(m_sdsZipcodeServiceExcluded);
			Session["SESSION_DS2"] = m_sdsZipcodeServiceExcluded;
		}

		public void dgZipcodeServiceExcluded_Button(object sender, DataGridCommandEventArgs e)
		{
			String strCmdNm = e.CommandName;
			if(strCmdNm.Equals("search"))
			{
				TextBox txtZipcode = (TextBox)e.Item.FindControl("txtZipcode");
				TextBox txtState = (TextBox)e.Item.FindControl("txtState");
				TextBox txtCountry = (TextBox)e.Item.FindControl("txtCountry");

				String strZipcodeClientID = null;
				String strStateClientID = null;
				String strCountryClientID = null;

				String strZipcode = null;
				
				if(txtZipcode != null)
				{
					strZipcodeClientID = txtZipcode.ClientID;
					strZipcode = txtZipcode.Text;
				}

				if(txtState != null)
				{
					strStateClientID = txtState.ClientID;
				}
				
				if(txtCountry != null)
				{
					strCountryClientID = txtCountry.ClientID;
				}



				String sUrl = "ZipcodePopup.aspx?FORMID=VASZipcodeExcluded&ZIPCODE="+strZipcode+"&ZIPCODE_CID="+strZipcodeClientID+"&STATE_CID="+strStateClientID+"&COUNTRY_CID="+strCountryClientID;
				ArrayList paramList = new ArrayList();
				paramList.Add(sUrl);
				String sScript = Utility.GetScript("openWindowParam.js",paramList);
				Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);	
			}
			
		}

		protected void dgZipcodeServiceExcluded_Bound(object sender, DataGridItemEventArgs e)
		{
			if(e.Item.ItemIndex == -1)
			{
				return;
			}

			DataRow drSelected = m_sdsZipcodeServiceExcluded.ds.Tables[0].Rows[e.Item.ItemIndex];
			if(drSelected.RowState == DataRowState.Deleted)
			{
				return;
			}
			msTextBox txtZipcode = (msTextBox)e.Item.FindControl("txtZipcode");
			
			int iOperation = (int) ViewState["ZVASEOperation"];
			if(txtZipcode != null && iOperation == (int)Operation.Update)
			{
				txtZipcode.Enabled = true;
			}

			if(e.Item.ItemType == ListItemType.EditItem)
			{
				e.Item.Cells[0].Enabled = true;
				e.Item.Cells[3].Enabled = true;
			}
			else
			{
				e.Item.Cells[0].Enabled = true;
				e.Item.Cells[3].Enabled = true;
			}
		}

		protected void dgZipcodeServiceExcluded_PageChange(object sender, DataGridPageChangedEventArgs e)
		{
			dgZipcodeServiceExcluded.CurrentPageIndex = e.NewPageIndex;
			ShowCurrentZServiceEPage();
			Logger.LogDebugInfo("VASZipcodeExcluded","dgZipcodeServiceExcluded_PageChange","INF009","On Page Change "+e.NewPageIndex);
		}

		private void ShowCurrentZServiceEPage()
		{
			int iStartIndex = dgZipcodeServiceExcluded.CurrentPageIndex * dgZipcodeServiceExcluded.PageSize;
			String strServiceCode = (String)ViewState["CurrentService"];
			String strAgentID	= (String)ViewState["AgentID"];
			m_sdsZipcodeServiceExcluded = AgentProfileMgrDAL.GetAgentZipcodeServiceExcludedDS(utility.GetAppID(),utility.GetEnterpriseID(),strAgentID,strServiceCode,iStartIndex,dgZipcodeServiceExcluded.PageSize);

			int pgCnt = (Convert.ToInt32(m_sdsZipcodeServiceExcluded.QueryResultMaxSize) - 1)/dgZipcodeServiceExcluded.PageSize;
			if(pgCnt < dgZipcodeServiceExcluded.CurrentPageIndex)
			{
					dgZipcodeServiceExcluded.CurrentPageIndex = System.Convert.ToInt32(pgCnt);
			}
			dgZipcodeServiceExcluded.EditItemIndex = -1;
			BindZVASEGrid();
			lblZVASEMessage.Text = "";
		}
		
		private void FillZVASEDataRow(DataGridItem item, int drIndex)
		{
			msTextBox txtZipcode = (msTextBox)item.FindControl("txtZipcode");
			if(txtZipcode != null)
			{
				DataRow drCurrent = m_sdsZipcodeServiceExcluded.ds.Tables[0].Rows[drIndex];
				drCurrent["zipcode"] = txtZipcode.Text;
			}

			TextBox txtState = (TextBox)item.FindControl("txtState");
			if(txtState != null)
			{
				DataRow drCurrent = m_sdsZipcodeServiceExcluded.ds.Tables[0].Rows[drIndex];
				drCurrent["state_name"] = txtState.Text;
			}

			TextBox txtCountry = (TextBox)item.FindControl("txtCountry");
			if(txtCountry != null)
			{
				DataRow drCurrent = m_sdsZipcodeServiceExcluded.ds.Tables[0].Rows[drIndex];
				drCurrent["country"] = txtCountry.Text;
			}

		}
		private void ResetDetailsZipcodeGrid()
		{
			dgZipcodeServiceExcluded.CurrentPageIndex = 0;
			btnZVASEInsert.Enabled = false;			
			m_sdsZipcodeServiceExcluded = SysDataMgrDAL.GetEmptyZipcodeVASExcludedDS(0);
			Session["SESSION_DS2"] = m_sdsZipcodeServiceExcluded;
			lblZVASEMessage.Text = "";
			BindZVASEGrid();
		}

		private void dgZipcodeServiceExcluded_SelectedIndexChanged(object sender, System.EventArgs e)
		{
		
		}

		#endregion

		private void dbCmbAgentId_SelectedItemChanged(object sender, System.EventArgs e)
		{
			SetAgentNameServerStates();
		}

		private void dbCmbAgentName_SelectedItemChanged(object sender, System.EventArgs e)
		{
			SetAgentIDServerStates();
		}
	}
}
