<%@ Import Namespace="com.common.util" %>
<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<%@ Register TagPrefix="cr" Namespace="CrystalDecisions.Web" Assembly="CrystalDecisions.Web, Version=9.2.3300.0, Culture=neutral, PublicKeyToken=692fbea5521e1304" %>
<%@ Page language="c#" Codebehind="BandQuotationReport.aspx.cs" AutoEventWireup="false" Inherits="com.ties.BandQuotationReport" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>BandQuotationRpt</title> 
		<!--Commented temporarily for testing--><LINK href="css/Styles.css" type="text/css" rel="stylesheet" name="stylesheet">
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<script>
		//alert(document.BandQuotationReport.elements[2])
		</script>
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="BandQuotationReport" name="BandQuotationReport" method="post" runat="server">
			<CR:CRYSTALREPORTVIEWER id="ReportViewer" style="Z-INDEX: 101; LEFT: 0px; POSITION: absolute; TOP: 56px" runat="server" BestFitPage="False" DisplayToolbar="False" Height="1200px" Width="800px"></CR:CRYSTALREPORTVIEWER><asp:button id="btnShowGrpTree" style="Z-INDEX: 113; LEFT: 8px; POSITION: absolute; TOP: 8px" runat="server" Height="19px" Width="121px" CssClass="queryButton" Text="Show Group Tree"></asp:button><asp:button id="btnMoveFirst" style="Z-INDEX: 103; LEFT: 130px; POSITION: absolute; TOP: 8px" runat="server" Height="19px" Width="25px" CssClass="queryButton" Text="|<"></asp:button><asp:button id="btnMovePrevious" style="Z-INDEX: 104; LEFT: 156px; POSITION: absolute; TOP: 8px" runat="server" Height="19px" Width="25" CssClass="queryButton" Text="<"></asp:button><cc1:mstextbox id="txtGoTo" style="Z-INDEX: 107; LEFT: 182px; POSITION: absolute; TOP: 8px" runat="server" Height="19px" Width="56px" CssClass="textFieldRightAlign" TextMaskType="msNumeric" MaxLength="6" NumberMaxValue="999999" NumberPrecision="6" NumberScale="0"></cc1:mstextbox><asp:button id="btnMoveNext" style="Z-INDEX: 105; LEFT: 239px; POSITION: absolute; TOP: 8px" runat="server" Height="19px" Width="25" CssClass="queryButton" Text=">"></asp:button><asp:button id="btnMoveLast" style="Z-INDEX: 106; LEFT: 265px; POSITION: absolute; TOP: 8px" runat="server" Height="19px" Width="24px" CssClass="queryButton" Text=">|"></asp:button><asp:dropdownlist id="ddbExport" style="Z-INDEX: 108; LEFT: 294px; POSITION: absolute; TOP: 8px" runat="server" Height="19" Width="173px"></asp:dropdownlist><asp:button id="btnExport" style="Z-INDEX: 102; LEFT: 465px; POSITION: absolute; TOP: 8px" runat="server" Height="19px" CssClass="queryButton" Text="Export"></asp:button><asp:label id="lblZoom" style="Z-INDEX: 112; LEFT: 586px; POSITION: absolute; TOP: 11px" runat="server" Height="16px" Width="62px" CssClass="tableLabel" Font-Bold="True">Zoom</asp:label><asp:dropdownlist id="ddbZoom" style="Z-INDEX: 109; LEFT: 656px; POSITION: absolute; TOP: 9px" runat="server" Height="19" Width="59px" AutoPostBack="True">
				<asp:ListItem Value="25">25%</asp:ListItem>
				<asp:ListItem Value="50">50%</asp:ListItem>
				<asp:ListItem Value="75">75%</asp:ListItem>
				<asp:ListItem Value="100" Selected="True">100%</asp:ListItem>
				<asp:ListItem Value="125">125%</asp:ListItem>
				<asp:ListItem Value="150">150%</asp:ListItem>
				<asp:ListItem Value="175">175%</asp:ListItem>
				<asp:ListItem Value="200">200%</asp:ListItem>
			</asp:dropdownlist><asp:textbox id="txtTextToSearch" style="Z-INDEX: 111; LEFT: 717px; POSITION: absolute; TOP: 8px" runat="server" Height="19px" Width="96px" CssClass="textField"></asp:textbox><asp:button id="btnSearch" style="Z-INDEX: 110; LEFT: 815px; POSITION: absolute; TOP: 8px" runat="server" Height="19px" CssClass="queryButton" Text="Search"></asp:button><asp:label id="lblErrorMesssage" style="Z-INDEX: 114; LEFT: 16px; POSITION: absolute; TOP: 32px" runat="server" Height="24px" Width="797px" CssClass="errorMsgColor"></asp:label></form>
	</body>
</HTML>
