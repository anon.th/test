using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.common.classes;
using com.common.DAL;
using com.common.RBAC;
using com.ties.DAL;
using com.common.util;
using com.ties.classes;
using com.common.applicationpages;
using System.Text.RegularExpressions;
namespace com.ties
{
	/// <summary>
	/// Summary description for IndustrialSector.
	/// </summary>
	public class DistributionCenter : BasePage
	{
		protected System.Web.UI.WebControls.Button btnQuery;
		protected System.Web.UI.WebControls.Button btnInsert;
		protected System.Web.UI.WebControls.Button btnExecuteQuery;
		private SessionDS m_sdsDCenter;
		private SessionDS m_sdsDCenter2;
		private String m_strAppID;
		protected System.Web.UI.WebControls.Label lblValISector;
		protected System.Web.UI.WebControls.Label Label1;
		protected System.Web.UI.WebControls.ValidationSummary PageValidationSummary;
		protected System.Web.UI.WebControls.DataGrid dgDCenter;
		private String m_strEnterpriseID;
		private DataView m_dvHubOptions = null;
		private DataView m_dvManifestOptions = null;
		private DataView m_DCOperatedByOptions = null;
		private String m_strCulture;

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			base.OnInit(e);
			InitializeComponent();
			
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnQuery.Click += new System.EventHandler(this.btnQuery_Click);
			this.btnExecuteQuery.Click += new System.EventHandler(this.btnExecuteQuery_Click);
			this.btnInsert.Click += new System.EventHandler(this.btnInsert_Click);
			this.dgDCenter.SelectedIndexChanged += new System.EventHandler(this.dgDCenter_SelectedIndexChanged);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		//private bool toRemoveBlankRow = false;

		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			//Utility utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings,Page.Session);
			m_strAppID = utility.GetAppID();
			m_strEnterpriseID = utility.GetEnterpriseID();
			m_strCulture = utility.GetUserCulture();

			if(!Page.IsPostBack)
			{
				ViewState["m_strCode"] = "";
				ViewState["index"] = 0;
				ViewState["prevRow"] = 0;

				ViewState["SCMode"] = ScreenMode.None;
				ViewState["SCOperation"] = Operation.None;
				
				dgDCenter.EditItemIndex = -1;
				m_sdsDCenter = SysDataManager1.GetEmptyDCenter(1);
				enableEditColumn(false);
				BindDCenter();
				

				//-----START QUERYING------
				lblValISector.Text = "";
				m_sdsDCenter.ds.Tables[0].Rows.Clear();
				dgDCenter.CurrentPageIndex = 0;
				btnExecuteQuery.Enabled = true;
				Utility.EnableButton(ref btnInsert,ButtonType.Insert,true,m_moduleAccessRights);
				enableEditColumn(false);
				ViewState["SCMode"] = ScreenMode.Query;
				ViewState["SCOperation"] = Operation.None;
				ViewState["m_strCode"] = "";
				AddRow();
				ViewState["prevRow"]=0;
				ViewState["index"]=0;
				dgDCenter.EditItemIndex = 0;
				m_dvHubOptions = CreateHubOptions(true);
				m_dvManifestOptions = CreateManifestOptions(true);
				m_DCOperatedByOptions = CreateDCOperatedByOptions(true);
				BindDCenter();

				Session["SESSION_DS1"] = m_sdsDCenter;
				Session["QUERY_DS"] = m_sdsDCenter;

				
				if((ScreenMode)ViewState["SCMode"] == ScreenMode.Query)
				{
					DropDownList ddlOperated = (DropDownList)dgDCenter.Items[0].FindControl("ddlOperated");
					if(ddlOperated != null)
					{
						ListItem lt = new ListItem("All","All");
						ddlOperated.Items.Insert(0,lt);
					}
				}
			}
			else
			{
				m_sdsDCenter = (SessionDS)Session["SESSION_DS1"];
				m_dvHubOptions = CreateHubOptions(true);
				m_dvManifestOptions = CreateManifestOptions(true);
				m_DCOperatedByOptions = CreateDCOperatedByOptions(true);
			}
			PageValidationSummary.HeaderText = Utility.GetLanguageText(ResourceType.UserMessage, "PLS_ENTER_MISS_FIELD", utility.GetUserCulture());
		}
		/// <summary>
		/// To bind data to datagrid
		/// </summary>
		private void BindDCenter()
		{
			//lblValISector.Text ="";
			dgDCenter.VirtualItemCount = System.Convert.ToInt32(m_sdsDCenter.QueryResultMaxSize);
			dgDCenter.DataSource = m_sdsDCenter.ds;
			dgDCenter.DataBind();
			Session["SESSION_DS1"] = m_sdsDCenter;
		}
		/// <summary>
		/// On editing allows text input
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void OnEdit_ISector(object sender, DataGridCommandEventArgs e)
		{		
			lblValISector.Text ="";
			Utility.EnableButton(ref btnInsert,ButtonType.Insert,true,m_moduleAccessRights);
			int rowIndex = e.Item.ItemIndex;
			ViewState["index"] = rowIndex;
			Label lblCode = (Label)dgDCenter.Items[rowIndex].FindControl("lblCode");
			ViewState["m_strCode"] = lblCode.Text;
			if((int)ViewState["SCOperation"] == (int)Operation.Insert)
			{
				ViewState["SCMode"]= ScreenMode.Insert;
			}
			else
			{
				ViewState["SCMode"] = ScreenMode.None;

			}
			if(dgDCenter.EditItemIndex >0)
			{
				msTextBox txtCode = (msTextBox)dgDCenter.Items[dgDCenter.EditItemIndex].FindControl("txtCode");
				
				if(txtCode.Text!=null && txtCode.Text=="")	
				{
					ViewState["m_strCode"] = txtCode.Text;
					m_sdsDCenter.ds.Tables[0].Rows.RemoveAt(dgDCenter.EditItemIndex);
				}
			}
			dgDCenter.EditItemIndex = rowIndex;
			BindDCenter();
	
		}
		/// <summary>
		/// To return to viewing mode
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void OnCancel_ISector(object sender, DataGridCommandEventArgs e)
		{
			lblValISector.Text ="";
			dgDCenter.EditItemIndex = -1;
			m_sdsDCenter = (SessionDS)Session["SESSION_DS1"];
			int rowIndex = e.Item.ItemIndex;

			if((int)ViewState["SCMode"] == (int)ScreenMode.Insert)
			{
				m_sdsDCenter.ds.Tables[0].Rows.RemoveAt(rowIndex);
			}
			enableDeleteColumn(true);
			Utility.EnableButton(ref btnInsert,ButtonType.Insert,true,m_moduleAccessRights);
			BindDCenter();

		}
		/// <summary>
		/// To update where new fields entered and store to database
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void OnUpdate_ISector(object sender, DataGridCommandEventArgs e)
		{
			lblValISector.Text = "";
			int rowIndex = e.Item.ItemIndex;

			TextBox txtEmail = (TextBox)dgDCenter.Items[rowIndex].FindControl("txtEmail");
			if (!IsValidEmailAddress(txtEmail.Text.Trim()))
			{
				lblValISector.Text="Invalid format for email address.";
				return;
			}

//			com.common.util.msTextBox txtTurnaroundTime = (TextBox)dgDCenter.Items[rowIndex].FindControl("txtTurnaroundTime");
//			if(txtTurnaroundTime.Text == "")
//			{
//				lblValISector.Text="Invalid format for email address.";
//				return;
//			}

			updateLatestISector(rowIndex);

			if(lblValISector.Text == "")
			{
			
				DataSet ds = new DataSet();
				m_sdsDCenter = (SessionDS)Session["SESSION_DS1"];
				DataSet dsISector = m_sdsDCenter.ds.GetChanges();
				SessionDS sdsISector = new SessionDS();
				sdsISector.ds = dsISector;
				m_sdsDCenter2 = SysDataManager1.QueryDCenternonQ(m_sdsDCenter,m_strAppID,m_strEnterpriseID, 0 ,dgDCenter.PageSize);

			
				if((int)ViewState["SCMode"] == (int)ScreenMode.Insert)
				{
				
					if( !isCodeEmpty( rowIndex ) )
					{
						try
						{
							if (IsValidEmailAddress(dsISector.Tables[0].Rows[0].ItemArray[4].ToString()))
							{
								SysDataManager1.InsertDCenter(sdsISector,m_strAppID, m_strEnterpriseID);
								m_sdsDCenter.ds.Tables[0].Rows[rowIndex].AcceptChanges();
								lblValISector.Text = Utility.GetLanguageText(ResourceType.UserMessage,"INS_SUCCESSFULLY",utility.GetUserCulture());	
								
							}
							else
							{
								lblValISector.Text="Invalid format for email address.";
								return;
							}


						}
						catch(ApplicationException appException)
						{
							String strMsg = appException.Message;
							strMsg = appException.InnerException.Message;

							if(strMsg.IndexOf("duplicate key") != -1  || strMsg.IndexOf("unique") != -1)
							{
								lblValISector.Text = Utility.GetLanguageText(ResourceType.UserMessage,"DUP_KEY_FOUND",utility.GetUserCulture());
							}
							else if(strMsg.IndexOf("DCCK_TT") != -1)
							{
								lblValISector.Text = "Turnaround time may be from 00:00 to 08:00 (HH:MM)";
							}
							else 
							{
								lblValISector.Text=strMsg;
							}
							return;				
						}
						catch(Exception err)
						{
							String msg = err.ToString();
							lblValISector.Text = Utility.GetLanguageText(ResourceType.UserMessage,"ERROR",utility.GetUserCulture());
						}
					

					}	
				}
				else
				{
					try
					{
						if (IsValidEmailAddress(dsISector.Tables[0].Rows[0].ItemArray[4].ToString()))
						{
							SysDataManager1.UpdateDCenter(m_sdsDCenter,rowIndex,m_strAppID, m_strEnterpriseID);
							m_sdsDCenter.ds.Tables[0].Rows[rowIndex].AcceptChanges();
							lblValISector.Text = Utility.GetLanguageText(ResourceType.UserMessage,"UPD_SUCCESSFULLY",utility.GetUserCulture());					
						}
						else
						{
							lblValISector.Text="Invalid format for email address.";
							return;
						}
					}
					catch(ApplicationException appException)
					{
						String strMsg = appException.Message;
						strMsg = appException.InnerException.Message;

						if(strMsg.IndexOf("duplicate key") != -1  || strMsg.IndexOf("unique") != -1)
						{
							lblValISector.Text = Utility.GetLanguageText(ResourceType.UserMessage,"DUP_KEY_FOUND",utility.GetUserCulture());
						} 
						else if(strMsg.IndexOf("DCCK_TT") != -1)
						{
							lblValISector.Text = "Turnaround time may be from 00:00 to 08:00 (HH:MM)";
						}
						else 
						{
							lblValISector.Text=strMsg;
						}
						return;				
					}
					catch(Exception err)
					{
						String msg = err.ToString();
						lblValISector.Text = Utility.GetLanguageText(ResourceType.UserMessage,"ERROR",utility.GetUserCulture());
					}				
				}
				ViewState["SCOperation"] = Operation.None;
				Utility.EnableButton(ref btnInsert,ButtonType.Insert,true,m_moduleAccessRights);
				dgDCenter.EditItemIndex = -1;
				BindDCenter();
			}
		}
		/// <summary>
		/// to delete certain row
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void OnDelete_ISector(object sender, DataGridCommandEventArgs e)
		{
			//*Raj*//
			if (dgDCenter.EditItemIndex == e.Item.ItemIndex)
			{
				return;
			}
			//*Raj*//
			ViewState["SCOperation"] = Operation.Delete;
			int rowNum = e.Item.ItemIndex;
			if(checkEmptyFields(rowNum))
			{
				if((int)ViewState["SCMode"] == (int)ScreenMode.Insert)
				{
					DeleteISector(rowNum);
				}
				else
				{
					DeleteISector(rowNum);
					showCurrentPage();
				}
				if(dgDCenter.EditItemIndex <-1)
				{
					//-original m_sdsDCenter.ds.Tables[0].Rows.RemoveAt(dgDCenter.EditItemIndex-1);
					//*Raj*//
					m_sdsDCenter.ds.Tables[0].Rows.RemoveAt(dgDCenter.EditItemIndex);
					//*Raj*//
				}	
				Utility.EnableButton(ref btnInsert,ButtonType.Insert,true,m_moduleAccessRights);
				BindDCenter();
			}
			
		}
		/// <summary>
		/// to delete row
		/// </summary>
		/// <param name="rowIndex">to delete at this row index</param>
		private void DeleteISector(int rowIndex)
		{
			lblValISector.Text = "";
			//m_sdsDCenter = (SessionDS)Session["SESSION_DS1"];
			DataRow dr = m_sdsDCenter.ds.Tables[0].Rows[rowIndex];
			String strCode = (String)dr[0];
			try
			{
				SysDataManager1.DeleteDCenter(m_strAppID,m_strEnterpriseID,strCode);
				m_sdsDCenter.ds.Tables[0].Rows.RemoveAt(rowIndex);
				lblValISector.Text=Utility.GetLanguageText(ResourceType.UserMessage,"DLD_SUCCESSFULLY",utility.GetUserCulture());
			}
			catch(ApplicationException appException)
			{
				String strMsg = appException.Message;
				if(strMsg.IndexOf("FK") != -1 || strMsg.IndexOf("REFERENCE") != -1)
				{
					lblValISector.Text =  Utility.GetLanguageText(ResourceType.UserMessage,"CHILD_FOUND_CANNOT_DEL",utility.GetUserCulture());
				}
				if(strMsg.IndexOf("child record found") != -1)
				{
					lblValISector.Text =  Utility.GetLanguageText(ResourceType.UserMessage,"CHILD_FOUND_CANNOT_DEL",utility.GetUserCulture());
				}				

				Logger.LogTraceError("ZipCode.aspx.cs","dgZipCode_Delete","RBAC003",appException.Message.ToString());
				//lblErrorMessage.Text = appException.Message.ToString();
				return;				
			}
		}
		/// <summary>
		/// to show code column or not
		/// </summary>
		/// <param name="toEnable">true to enable , false to disable</param>
		private void enableCodeColumn(bool toEnable)
		{
			dgDCenter.Columns[2].Visible=toEnable;//Code column
		}
		/// <summary>
		/// to show delete column or not
		/// </summary>
		/// <param name="toEnable">true to enable, false to disable</param>
		private void enableDeleteColumn(bool toEnable)
		{
			dgDCenter.Columns[1].Visible=toEnable;
		}
		/// <summary>
		/// to show edit column or not
		/// </summary>
		/// <param name="toEnable">true to enable, false to disable</param>
		private void enableEditColumn(bool toEnable)
		{
			dgDCenter.Columns[0].Visible=toEnable;
			dgDCenter.Columns[1].Visible=toEnable;
		}
		
		private void AddRow()
		{
			SysDataManager1.AddNewRowInDCenterDS(ref m_sdsDCenter);
			BindDCenter();
		}

		/// <summary>
		/// To add a new row
		/// </summary>
		//		private void AddRow()
		//		{
		//			m_sdsDCenter = (SessionDS)Session["SESSION_DS1"];
		//			DataRow drNew = m_sdsDCenter.ds.Tables[0].NewRow();
		//
		//			drNew[0] = "";
		//			drNew[1] = "";
		//			
		//			m_sdsDCenter.ds.Tables[0].Rows.Add(drNew);
		//			
		//		}
		/// <summary>
		/// to get text out from textboxes and store it to a dataset
		/// </summary>
		/// <param name="rowIndex">to get at this row index</param>
		private void updateLatestISector(int rowIndex)
		{
			msTextBox txtCode = (msTextBox)dgDCenter.Items[rowIndex].FindControl("txtCode");
			TextBox txtDescription = (TextBox)dgDCenter.Items[rowIndex].FindControl("txtDescription");
			//			TextBox txtHub = (TextBox)dgDCenter.Items[rowIndex].FindControl("txtHub");
			DropDownList ddlHub = (DropDownList)dgDCenter.Items[rowIndex].FindControl("ddlHub");
			DropDownList ddlManifest = (DropDownList)dgDCenter.Items[rowIndex].FindControl("ddlManifest");
			TextBox txtEmail = (TextBox)dgDCenter.Items[rowIndex].FindControl("txtEmail");
			TextBox txtZipcode = (TextBox)dgDCenter.Items[rowIndex].FindControl("txtZipcode");
			DropDownList ddlOperated = (DropDownList)dgDCenter.Items[rowIndex].FindControl("ddlOperated");
			msTextBox txtTurnaroundTime = (msTextBox)dgDCenter.Items[rowIndex].FindControl("txtTurnaroundTime");
			TextBox txtNearestAirport = (TextBox)dgDCenter.Items[rowIndex].FindControl("txtNearestAirport");

			DataRow dr = m_sdsDCenter.ds.Tables[0].Rows[rowIndex];

			if(ddlHub.SelectedItem.Value == "Y")
			{
				m_sdsDCenter = (SessionDS)Session["SESSION_DS1"];
				DataSet dsISector = m_sdsDCenter.ds;

				int counts =0;
				foreach(DataRow dr_ in m_sdsDCenter.ds.Tables[0].Rows)
				{
					if (dr_.ItemArray[2].ToString() == "Y")
					{
						counts++;
					}
				}

				if(counts > 1)
				{
					lblValISector.Text = Utility.GetLanguageText(ResourceType.UserMessage,"DC_HUB1",utility.GetUserCulture());
					return;
				}
			}

			string[] arrStrThun = txtTurnaroundTime.Text.Split(':');
			if(arrStrThun.Length ==2)
			{	
				if(Convert.ToInt32(arrStrThun[0]) > 8 || (Convert.ToInt32(arrStrThun[0]) ==  8 && Convert.ToInt32(arrStrThun[1]) != 0) || Convert.ToInt32(arrStrThun[1]) > 59)
				{
					lblValISector.Text = "Turnaround time may be from 00:00 to 08:00 (HH:MM)";
					return;
				}
			}

			if((int)ViewState["SCMode"] == (int)ScreenMode.Insert)
			{
				dr[0] = txtCode.Text;
			}
			else
			{
				dr[0] = (String)ViewState["m_strCode"];
			}
			String strDesc = txtDescription.Text;
			dr[1] = strDesc;

			String strHub = ddlHub.SelectedItem.Value; // txtHub.Text;
			dr[2] = strHub;

			String strManifest = ddlManifest.SelectedItem.Value; // txtManifest.Text;
			dr[3] = strManifest;
			
			String strEmail = txtEmail.Text;
			dr[4] = strEmail;

			dr[5] = txtZipcode.Text.Trim();
			dr[6] = ddlOperated.SelectedValue;
			dr[7] = txtTurnaroundTime.Text.Trim();
			dr[8] = txtNearestAirport.Text.Trim();

			Session["SESSION_DS1"] = m_sdsDCenter;


			
		}
		/// <summary>
		/// to check if fields are empty
		/// </summary>
		/// <param name="rowIndex">to check at this row index</param>
		/// <returns>return true or false</returns>
		private bool checkEmptyFields(int rowIndex)
		{
			msTextBox txtCode = (msTextBox)dgDCenter.Items[rowIndex].FindControl("txtCode");
			TextBox txtDescription = (TextBox)dgDCenter.Items[rowIndex].FindControl("txtDescription");
			//			TextBox txtHub = (TextBox)dgDCenter.Items[rowIndex].FindControl("txtHub");
			DropDownList ddlHub = (DropDownList)dgDCenter.Items[rowIndex].FindControl("ddlHub");
			DropDownList ddlManifest = (DropDownList)dgDCenter.Items[rowIndex].FindControl("ddlManifest");
			TextBox txtEmail = (TextBox)dgDCenter.Items[rowIndex].FindControl("txtEmail");
			TextBox txtZipcode = (TextBox)dgDCenter.Items[rowIndex].FindControl("txtZipcode");
			DropDownList ddlOperated = (DropDownList)dgDCenter.Items[rowIndex].FindControl("ddlOperated");
			msTextBox txtTurnaroundTime = (msTextBox)dgDCenter.Items[rowIndex].FindControl("txtTurnaroundTime");
			TextBox txtNearestAirport = (TextBox)dgDCenter.Items[rowIndex].FindControl("txtNearestAirport");

			if(txtCode!=null && txtDescription!=null )
			{
				DataRow dr = m_sdsDCenter.ds.Tables[0].Rows[rowIndex];
				if((int)ViewState["SCMode"] == (int)ScreenMode.Query)
				
					dr[0] = txtCode.Text;
				else
					dr[0] = (String)ViewState["m_strCode"];

				dr[1] = txtDescription.Text;

				String strHub = ddlHub.SelectedItem.Value; // txtHub.Text;
				dr[2] = strHub;

				String strManifest = ddlManifest.SelectedItem.Value; // txtManifest.Text;
				dr[3] = strManifest;
			
				String strEmail = txtEmail.Text;
				dr[4] = strEmail;

				dr[5] = txtZipcode.Text.Trim();
				dr[6] = ddlOperated.SelectedValue;
				dr[7] = txtTurnaroundTime.Text.Trim();
				dr[8] = txtNearestAirport.Text.Trim();

				Session["SESSION_DS1"] = m_sdsDCenter;
				Session["QUERY_DS"] = m_sdsDCenter;
				return false;
			}
			return true;
		}
		/// <summary>
		/// To query from databse and display results
		/// </summary>
		private void QueryISector()
		{
			checkEmptyFields((int)ViewState["index"]);

			m_sdsDCenter = SysDataManager1.QueryDCenter(m_sdsDCenter,m_strAppID,m_strEnterpriseID, 0 ,dgDCenter.PageSize);		
			dgDCenter.VirtualItemCount = System.Convert.ToInt32(m_sdsDCenter.QueryResultMaxSize);
			if(m_sdsDCenter.QueryResultMaxSize<1)
			{
				lblValISector.Text =Utility.GetLanguageText(ResourceType.UserMessage,"NO_RECORD_FOUND",utility.GetUserCulture());
			}
			Session["SESSION_DS1"] = m_sdsDCenter;

		}
		/// <summary>
		/// To check if code text is empty and do validation
		/// </summary>
		/// <param name="rowIndex"></param>
		/// <returns></returns>
		private bool isCodeEmpty(int rowIndex)
		{
			msTextBox txtCode = (msTextBox)dgDCenter.Items[rowIndex].FindControl("txtCode");
			if(txtCode!=null)
			{
				DataRow dr = m_sdsDCenter.ds.Tables[0].Rows[rowIndex];
				if(txtCode.Text!="")
				{
					if(txtCode.Text.IndexOf(" ",0)>0)
					{
						lblValISector.Text = Utility.GetLanguageText(ResourceType.UserMessage,"CDE_NO_SPACE",utility.GetUserCulture());
						return true;
					}
					return false;
				}
			}
			lblValISector.Text =Utility.GetLanguageText(ResourceType.UserMessage,"CDE_COLUMN_REQ",utility.GetUserCulture());
			ViewState["SCOperation"] = Operation.Insert;
			return true;
		}
		/// <summary>
		/// Insert button event handler, where it enables and disables 
		/// certain wweb form components
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void btnInsert_Click(object sender, System.EventArgs e)
		{
			lblValISector.Text = "";
			Utility.EnableButton(ref btnInsert,ButtonType.Insert,false,m_moduleAccessRights);
			btnExecuteQuery.Enabled = false;
			dgDCenter.CurrentPageIndex = 0;
			if((int)ViewState["SCMode"] != (int)ScreenMode.Insert || m_sdsDCenter.ds.Tables[0].Rows.Count >= dgDCenter.PageSize)
			{
				m_sdsDCenter = SysDataManager1.GetEmptyDCenter(1);
				dgDCenter.EditItemIndex = m_sdsDCenter.ds.Tables[0].Rows.Count-1;
			}
			else
			{
				AddRow();
				dgDCenter.EditItemIndex = m_sdsDCenter.ds.Tables[0].Rows.Count-1;
			}
			//dgDCenter.VirtualItemCount = m_sdsDCenter.ds.Tables[0].Rows.Count;

			ViewState["SCMode"] = ScreenMode.Insert;
			enableEditColumn(true);		
			BindDCenter();
			getPageControls(Page);
		}

		/// <summary>
		/// Query event handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void btnQuery_Click(object sender, System.EventArgs e)
		{
			lblValISector.Text = "";
			m_sdsDCenter.ds.Tables[0].Rows.Clear();
			dgDCenter.CurrentPageIndex = 0;
			btnExecuteQuery.Enabled = true;
			Utility.EnableButton(ref btnInsert,ButtonType.Insert,true,m_moduleAccessRights);
			enableEditColumn(false);
			ViewState["SCMode"] = ScreenMode.Query;
			ViewState["SCOperation"] = Operation.None;
			ViewState["m_strCode"] = "";
			AddRow();
			ViewState["prevRow"]=0;
			ViewState["index"]=0;
			dgDCenter.EditItemIndex = 0;
			BindDCenter();
			
			if((ScreenMode)ViewState["SCMode"] == ScreenMode.Query)
			{
				DropDownList ddlOperated = (DropDownList)dgDCenter.Items[0].FindControl("ddlOperated");
				if(ddlOperated != null)
				{
					ListItem lt = new ListItem("All","All");
					ddlOperated.Items.Insert(0,lt);
				}
			}
		}

		/// <summary>
		/// Execute event handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void btnExecuteQuery_Click(object sender, System.EventArgs e)
		{
			dgDCenter.EditItemIndex = -1;
			QueryISector();
			ViewState["toQuery"] = false;
			btnExecuteQuery.Enabled = false;
			enableEditColumn(true);
			enableDeleteColumn(true);
			Utility.EnableButton(ref btnInsert,ButtonType.Insert,true,m_moduleAccessRights);
			BindDCenter();
		}
		/// <summary>
		/// on page change event handler where data is refresh
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void OnISector_PageChange(object sender, DataGridPageChangedEventArgs e)
		{
			dgDCenter.CurrentPageIndex = e.NewPageIndex;
			showCurrentPage();
			dgDCenter.SelectedIndex = -1;
			dgDCenter.EditItemIndex = -1;
			BindDCenter();
		}
		/// <summary>
		/// to refresh data
		/// </summary>
		private void showCurrentPage()
		{
			SessionDS m_sdsQueryISector = (SessionDS)Session["QUERY_DS"];
			int iStartIndex = dgDCenter.CurrentPageIndex * dgDCenter.PageSize;
			
			m_sdsDCenter = SysDataManager1.QueryDCenter(m_sdsQueryISector,m_strAppID,m_strEnterpriseID, iStartIndex,dgDCenter.PageSize);
			int pgCnt = (Convert.ToInt32( m_sdsDCenter.QueryResultMaxSize - 1))/dgDCenter.PageSize;
			if(pgCnt < dgDCenter.CurrentPageIndex)
			{
				dgDCenter.CurrentPageIndex = System.Convert.ToInt32(pgCnt);
			}
			Session["SESSION_DS1"] = m_sdsDCenter;
			
		}
		/// <summary>
		/// on item bound event handler where is checks if code column should
		/// be enables or not
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void OnItemBound_ISector(object sender, DataGridItemEventArgs e)
		{
			if(e.Item.ItemIndex == -1)
			{
				return;
			}
			DropDownList ddlHub = (DropDownList)e.Item.FindControl("ddlHub");
			DropDownList ddlManifest = (DropDownList)e.Item.FindControl("ddlManifest");
			DropDownList ddlOperated = (DropDownList)e.Item.FindControl("ddlOperated");
			DataRow drSelected = m_sdsDCenter.ds.Tables[0].Rows[e.Item.ItemIndex];
			if(ddlHub != null)
			{
				ddlHub.DataSource = LoadHubOptions();
				ddlHub.DataTextField = "Text";
				ddlHub.DataValueField = "StringValue";
				ddlHub.DataBind();

				String strType = (String)drSelected["hub"];

				ddlHub.SelectedIndex = ddlHub.Items.IndexOf(ddlHub.Items.FindByValue(strType));
			}

			if(ddlManifest != null)
			{
				ddlManifest.DataSource = LoadManifestOptions();
				ddlManifest.DataTextField = "Text";
				ddlManifest.DataValueField = "StringValue";
				ddlManifest.DataBind();

				String strType = (String)drSelected["manifest"];

				ddlManifest.SelectedIndex = ddlManifest.Items.IndexOf(ddlManifest.Items.FindByValue(strType));
			}
			if(ddlOperated != null)
			{
				ddlOperated.DataSource = LoadDCOperatedByOptions();
				ddlOperated.DataTextField = "Text";
				ddlOperated.DataValueField = "StringValue";
				ddlOperated.DataBind();

				if(ddlOperated.Items.Count >0)
					ddlOperated.Items.RemoveAt(0);

				string strType = (string)drSelected["OperatedBy"];

				ddlOperated.SelectedIndex = ddlOperated.Items.IndexOf(ddlOperated.Items.FindByValue(strType));

			}


			msTextBox txtCode = (msTextBox)e.Item.FindControl("txtCode");
			if(txtCode!=null && ( (int)ViewState["SCMode"] != (int)ScreenMode.Insert && (int)ViewState["SCMode"] != (int)ScreenMode.Query) )
			{
				txtCode.Enabled = false;
			}

		}
		
		/// <summary>
		/// To populate options type in the drop down list
		/// </summary>
		/// <param name="showNilOption">to show an empty option or not</param>
		/// <returns></returns>
		private DataView CreateHubOptions(bool showNilOption) 
		{
			DataTable dtHubOptions = new DataTable();
 
			dtHubOptions.Columns.Add(new DataColumn("Text", typeof(string)));
			dtHubOptions.Columns.Add(new DataColumn("StringValue", typeof(string)));

			ArrayList hubOptionArray = Utility.GetCodeValues(m_strAppID,m_strCulture,"hubdc",CodeValueType.StringValue);

			if(showNilOption)
			{
				DataRow drNilRow = dtHubOptions.NewRow();
				drNilRow[0] = "";
				drNilRow[1] = "";
				dtHubOptions.Rows.Add(drNilRow);
			}
			foreach(SystemCode typeSysCode in hubOptionArray)
			{
				DataRow drEach = dtHubOptions.NewRow();
				drEach[0] = typeSysCode.Text;
				drEach[1] = typeSysCode.StringValue;
				dtHubOptions.Rows.Add(drEach);
			}

			DataView dvHubOptions = new DataView(dtHubOptions);
			return dvHubOptions;
		}
		//ST081209 : Create Manifest Option for manifest dropdown lists
		private DataView CreateManifestOptions(bool showNilOption) 
		{
			DataTable dtManifestOptions = new DataTable();
 
			dtManifestOptions.Columns.Add(new DataColumn("Text", typeof(string)));
			dtManifestOptions.Columns.Add(new DataColumn("StringValue", typeof(string)));

			ArrayList manifestOptionArray = Utility.GetCodeValues(m_strAppID,m_strCulture,"manifestdc",CodeValueType.StringValue);

			if(showNilOption)
			{
				DataRow drNilRow = dtManifestOptions.NewRow();
				drNilRow[0] = "";
				drNilRow[1] = "";
				dtManifestOptions.Rows.Add(drNilRow);
			}
			if(manifestOptionArray != null)
			foreach(SystemCode typeSysCode in manifestOptionArray)
			{
				DataRow drEach = dtManifestOptions.NewRow();
				drEach[0] = typeSysCode.Text;
				drEach[1] = typeSysCode.StringValue;
				dtManifestOptions.Rows.Add(drEach);
			}

			DataView dvManifestOptions = new DataView(dtManifestOptions);
			return dvManifestOptions;
		}

		private DataView CreateDCOperatedByOptions(bool showNilOption) 
		{
			DataTable dtDCOperatedByOptions = new DataTable();
 
			dtDCOperatedByOptions.Columns.Add(new DataColumn("Text", typeof(string)));
			dtDCOperatedByOptions.Columns.Add(new DataColumn("StringValue", typeof(string)));

			ArrayList dcOperatedByOptionArray = Utility.GetCodeValues(m_strAppID,m_strCulture,"DCOperatedBy",CodeValueType.StringValue);

			if(showNilOption)
			{
				DataRow drNilRow = dtDCOperatedByOptions.NewRow();
				drNilRow[0] = "";
				drNilRow[1] = "";
				dtDCOperatedByOptions.Rows.Add(drNilRow);
			}
			if(dcOperatedByOptionArray != null)
			foreach(SystemCode typeSysCode in dcOperatedByOptionArray)
			{
				DataRow drEach = dtDCOperatedByOptions.NewRow();
				drEach[0] = typeSysCode.Text;
				drEach[1] = typeSysCode.StringValue;
				dtDCOperatedByOptions.Rows.Add(drEach);
			}

			DataView dvDCOperatedByOptions = new DataView(dtDCOperatedByOptions);
			return dvDCOperatedByOptions;
		}

		/// <summary>
		/// To load the options
		/// </summary>
		/// <returns></returns>
		private ICollection LoadHubOptions()
		{
			return m_dvHubOptions;
		}
		private ICollection LoadManifestOptions()
		{
			return m_dvManifestOptions;
		}
		private ICollection LoadDCOperatedByOptions()
		{
			return m_DCOperatedByOptions;
		}

		private void dgDCenter_SelectedIndexChanged(object sender, System.EventArgs e)
		{
		
		}

		public bool IsValidEmailAddress(string sEmail)
		{
//			if (sEmail == null)
//			{
//				return false;
//			}

			int nFirstAT = sEmail.IndexOf('@');
			int nLastAT = sEmail.LastIndexOf('@');

			if ( (nFirstAT > 0) && (nLastAT == nFirstAT) &&
				(nFirstAT < (sEmail.Length - 1)) )
			{
				// address is ok regarding the single @ sign
				return (Regex.IsMatch(sEmail, @"(\w+)@(\w+)\.(\w+)"));
			}
			else
			{
				return false;
			}
		}

	
	
	}
}
