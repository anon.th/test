using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.common.classes;
using com.common.util;
using com.ties.DAL;
using com.common.applicationpages;
using Cambro.Web.DbCombo;
using com.common.RBAC;
using com.ties.classes;
using System.Text;
using com.common.DAL;
using com.common.classes;

namespace com.ties
{
	/// <summary>
	/// Summary description for SalesTerritoryReportQuery.
	/// </summary>
	public class PODEXRptQuery : BasePage
	{
		#region Web Form Designer generated code

		protected System.Web.UI.WebControls.Button btnQuery;
		protected System.Web.UI.WebControls.Label lblErrorMessage;
		protected System.Web.UI.WebControls.Button btnExecuteQuery;
		protected System.Web.UI.WebControls.Label Label1;
		protected System.Web.UI.WebControls.Label lblDates;
		protected System.Web.UI.WebControls.RadioButton rbBookingDate;
		protected System.Web.UI.WebControls.RadioButton rbPickUpDate;
		protected System.Web.UI.WebControls.RadioButton rbMonth;
		protected System.Web.UI.WebControls.DropDownList ddMonth;
		protected System.Web.UI.WebControls.Label lblYear;
		protected com.common.util.msTextBox txtYear;
		protected System.Web.UI.WebControls.RadioButton rbPeriod;
		protected com.common.util.msTextBox txtPeriod;
		protected com.common.util.msTextBox txtTo;
		protected System.Web.UI.WebControls.RadioButton rbDate;
		protected com.common.util.msTextBox txtDate;
		protected System.Web.UI.HtmlControls.HtmlTable tblDates;
		protected System.Web.UI.WebControls.RadioButton rbEstDelDate;
		protected System.Web.UI.WebControls.RadioButton rbPODDate;
		protected System.Web.UI.WebControls.Label lblRouteType;
		protected System.Web.UI.WebControls.RadioButton rbLongRoute;
		protected System.Web.UI.WebControls.RadioButton rbShortRoute;
		protected System.Web.UI.WebControls.RadioButton rbAirRoute;
		protected System.Web.UI.WebControls.Label lblRouteCode;
		protected Cambro.Web.DbCombo.DbCombo DbComboPathCode;
		protected System.Web.UI.WebControls.Label Label2;
		protected Cambro.Web.DbCombo.DbCombo DbComboOriginDC;
		protected System.Web.UI.WebControls.Label Label3;
		protected Cambro.Web.DbCombo.DbCombo DbComboDestinationDC;
		protected System.Web.UI.HtmlControls.HtmlTable tblRouteType;
		protected System.Web.UI.WebControls.Label Label7;
		protected System.Web.UI.WebControls.Label Label6;
		protected System.Web.UI.WebControls.Label Label5;
		protected System.Web.UI.HtmlControls.HtmlTable Table5;
		protected System.Web.UI.WebControls.Label Label9;
		protected System.Web.UI.WebControls.Label Label10;
		protected System.Web.UI.HtmlControls.HtmlTable Table3;
		protected System.Web.UI.WebControls.Label lblPayerType;
		protected System.Web.UI.WebControls.Label Label4;
		protected System.Web.UI.WebControls.ListBox lsbCustType;
		protected System.Web.UI.WebControls.Label lblPayerCode;
		protected System.Web.UI.WebControls.TextBox txtPayerCode;
		protected System.Web.UI.WebControls.Button btnPayerCode;
		protected System.Web.UI.HtmlControls.HtmlTable tblPayerType;
		protected Cambro.Web.DbCombo.DbCombo dbCmbServiceCode;
		protected com.common.util.msTextBox txtZipCode;
		protected com.common.util.msTextBox txtStateCode;
		protected System.Web.UI.WebControls.Button btnZipCode;
		protected System.Web.UI.WebControls.Button btnStateCode;
		protected Cambro.Web.DbCombo.DbCombo dbCmbExceptionType;
		protected System.Web.UI.WebControls.Label Label8;
		protected System.Web.UI.WebControls.Label Label11;
		protected System.Web.UI.HtmlControls.HtmlTable Table1;
		protected System.Web.UI.HtmlControls.HtmlTable tblInternal;

		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnQuery.Click += new System.EventHandler(this.btnQuery_Click);
			this.btnExecuteQuery.Click += new System.EventHandler(this.btnExecuteQuery_Click);
			this.rbMonth.CheckedChanged += new System.EventHandler(this.rbMonth_CheckedChanged);
			this.rbPeriod.CheckedChanged += new System.EventHandler(this.rbPeriod_CheckedChanged);
			this.rbDate.CheckedChanged += new System.EventHandler(this.rbDate_CheckedChanged);
			this.btnPayerCode.Click += new System.EventHandler(this.btnPayerCode_Click);
			this.btnZipCode.Click += new System.EventHandler(this.btnZipCode_Click);
			this.btnStateCode.Click += new System.EventHandler(this.btnStateCode_Click);
			this.ID = "SalesTerritoryReportQuery";
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		String m_strAppID=null;
		String m_strEnterpriseID=null;
		String m_strCulture=null;

		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings,Page.Session);
			m_strAppID = utility.GetAppID();
			m_strEnterpriseID = utility.GetEnterpriseID();
			m_strCulture = utility.GetUserCulture();
			
			DbComboPathCode.RegistrationKey = System.Configuration.ConfigurationSettings.AppSettings["DbComboRegKey"];
			DbComboOriginDC.RegistrationKey = System.Configuration.ConfigurationSettings.AppSettings["DbComboRegKey"];
			DbComboDestinationDC.RegistrationKey = System.Configuration.ConfigurationSettings.AppSettings["DbComboRegKey"];
			dbCmbServiceCode.RegistrationKey = System.Configuration.ConfigurationSettings.AppSettings["DbComboRegKey"];
			dbCmbExceptionType.RegistrationKey = System.Configuration.ConfigurationSettings.AppSettings["DbComboRegKey"];
			
			com.common.classes.User user = RBACManager.GetUser(m_strAppID, m_strEnterpriseID, (String)Session["userID"]);
			ViewState["usertype"] = user.UserType;
			
			if(!Page.IsPostBack)
			{
				DefaultScreen();
				ddlmonths();					
				LoadCustomerTypeList();
				Session["toRefresh"]=false;
			}

			SetDbComboServerStates();
		}


		private void btnExecuteQuery_Click(object sender, System.EventArgs e)
		{
			lblErrorMessage.Text = "";
			
			bool icheck =  ValidateValues();
			if(icheck == false)
			{
				String strModuleID = Request.Params["MODID"];
				DataSet dsShipment = GetShipmentQueryData();

				String strUrl = null;
				strUrl = "ReportViewer.aspx";
								
				Session["FORMID"] = "SQI_PODEX_All";
				
				Session["SESSION_DS1"] = dsShipment;
				ArrayList paramList = new ArrayList();
				paramList.Add(strUrl);
				String sScript = Utility.GetScript("openParentWindow.js",paramList);
				Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
			}
		}


		private void btnQuery_Click(object sender, System.EventArgs e)
		{
			DefaultScreen();
		}


		#region "Page Functions"

		private DataTable CreateEmptyDataTable()
		{
			DataTable dtShipmentPODEX = new DataTable();

			#region "Dates"
			dtShipmentPODEX.Columns.Add(new DataColumn("tran_date", typeof(string)));
			dtShipmentPODEX.Columns.Add(new DataColumn("start_date", typeof(DateTime)));
			dtShipmentPODEX.Columns.Add(new DataColumn("end_date", typeof(DateTime)));

//			dtShipmentPODEX.Columns.Add(new DataColumn("booking_datetime", typeof(string)));
//			dtShipmentPODEX.Columns.Add(new DataColumn("est_delivery_datetime", typeof(DateTime)));
//			dtShipmentPODEX.Columns.Add(new DataColumn("act_pickup_datetime", typeof(DateTime)));
//			dtShipmentPODEX.Columns.Add(new DataColumn("pod_date", typeof(DateTime)));
			#endregion
			
			#region "Payer Type"
			dtShipmentPODEX.Columns.Add(new DataColumn("payer_type", typeof(string)));
			dtShipmentPODEX.Columns.Add(new DataColumn("payerid", typeof(string)));
			#endregion
		
			#region "Service Code"
			dtShipmentPODEX.Columns.Add(new DataColumn("service_code", typeof(string)));
			#endregion

			#region "Exception Code"
			dtShipmentPODEX.Columns.Add(new DataColumn("exception_type", typeof(string)));
			#endregion
			
			#region "Exception"
			dtShipmentPODEX.Columns.Add(new DataColumn("consignment_no", typeof(string)));
			dtShipmentPODEX.Columns.Add(new DataColumn("exception_code", typeof(string)));
			dtShipmentPODEX.Columns.Add(new DataColumn("exception_description", typeof(string)));
			dtShipmentPODEX.Columns.Add(new DataColumn("destination_station", typeof(string)));
			dtShipmentPODEX.Columns.Add(new DataColumn("recipient_zipcode", typeof(string)));
			dtShipmentPODEX.Columns.Add(new DataColumn("area", typeof(string)));
			#endregion

			#region "Route / DC Selection"
			dtShipmentPODEX.Columns.Add(new DataColumn("route_type", typeof(string)));
			dtShipmentPODEX.Columns.Add(new DataColumn("route_code", typeof(string)));
			dtShipmentPODEX.Columns.Add(new DataColumn("origin_dc", typeof(string)));
			dtShipmentPODEX.Columns.Add(new DataColumn("destination_dc", typeof(string)));
			dtShipmentPODEX.Columns.Add(new DataColumn("delPath_origin_dc", typeof(string)));
			dtShipmentPODEX.Columns.Add(new DataColumn("delPath_destination_dc", typeof(string)));
			#endregion

			#region "Destination"
			dtShipmentPODEX.Columns.Add(new DataColumn("zip_code", typeof(string)));
			dtShipmentPODEX.Columns.Add(new DataColumn("state_code", typeof(string)));
			#endregion

			return dtShipmentPODEX;
		}


		private DataSet GetShipmentQueryData()
		{
			DataSet dsShipment = new DataSet();
			DataTable dtShipmentPODEX = CreateEmptyDataTable();
			DataRow dr = dtShipmentPODEX.NewRow(); 
			
			#region "Dates"
			
				if (rbBookingDate.Checked) 
				{	// Booking Date Selected
					dr["tran_date"] = "B";
				}
				if (rbPickUpDate.Checked) 
				{	// Actual Pickup  Date Selected
					dr["tran_date"] = "A";
				}
				if (rbEstDelDate.Checked)
				{ // Estimate Delivery Date Selected
					dr["tran_date"] = "E";
				}
				if (rbPODDate.Checked)
				{ // Actual POD Date Selected
					dr["tran_date"] = "P";
				}
			
				string strMonth =null;
				if (rbMonth.Checked) 
				{	// Month Selected
					strMonth = ddMonth.SelectedItem.Value;
					if (strMonth != "" && txtYear.Text != "") 
					{
						dr["start_date"] = DateTime.ParseExact ("01/"+strMonth.ToString()+"/"+txtYear.Text+" 00:00:01", "dd/MM/yyyy HH:mm:ss", null);
						int intLastDay = 0;
						intLastDay = LastDayOfMonth(strMonth, txtYear.Text);
						dr["end_date"] = DateTime.ParseExact (intLastDay.ToString()+"/"+strMonth.ToString()+"/"+txtYear.Text+" 23:59:59", "dd/MM/yyyy HH:mm:ss", null);
					}
				}
				if (rbPeriod.Checked) 
				{	// Period Selected
					if (txtPeriod.Text != ""  && txtTo.Text != "") 
					{
						dr["start_date"] = DateTime.ParseExact(txtPeriod.Text+" 00:00:01","dd/MM/yyyy HH:mm:ss",null);
						dr["end_date"] = DateTime.ParseExact(txtTo.Text+" 23:59:59" ,"dd/MM/yyyy HH:mm:ss",null);
					}
				}
				if (rbDate.Checked) 
				{	// Date Selected
					if (txtDate.Text != "") 
					{
						dr["start_date"] = DateTime.ParseExact(txtDate.Text+" 00:00:01", "dd/MM/yyyy HH:mm:ss",null);
						dr["end_date"] = DateTime.ParseExact(txtDate.Text+" 23:59:59", "dd/MM/yyyy HH:mm:ss",null);
					}
				}			
			#endregion

			#region "Payer Type"
			string strCustPayerType = "";

			for (int i = 0; i <= lsbCustType.Items.Count - 1; i ++)
			{
				if(lsbCustType.Items[i].Selected == true)
				{
					strCustPayerType += lsbCustType.Items[i].Value;
				}
			}

			if (strCustPayerType != "") 
				dr["payer_type"] = strCustPayerType;
			else
				dr["payer_type"] = System.DBNull.Value;
					
			dr["payerid"] = txtPayerCode.Text.Trim();		
			#endregion

			#region "Route / DC Selection"
			if (rbLongRoute.Checked) 
			{
				dr["route_type"] = "L";
			}
			if (rbShortRoute.Checked) 
			{
				dr["route_type"] = "S";
			}
			if (rbAirRoute.Checked) 
			{
				dr["route_type"] = "A";
			}

			dr["route_code"] = DbComboPathCode.Value;
			dr["origin_dc"] = DbComboOriginDC.Value;
			dr["destination_dc"] = DbComboDestinationDC.Value;
			
			if (rbLongRoute.Checked || rbAirRoute.Checked)
			{
				com.ties.classes.DeliveryPath delPath = new com.ties.classes.DeliveryPath();
				delPath.Populate(m_strAppID, m_strEnterpriseID, DbComboPathCode.Value);

				dr["delPath_origin_dc"] = delPath.OriginStation;
				dr["delPath_destination_dc"] = delPath.DestinationStation;
			}
			else
			{
				dr["delPath_origin_dc"] = "";
				dr["delPath_destination_dc"] = "";
			}
			#endregion
			
			#region "Destination"			
				dr["zip_code"] = txtZipCode.Text.Trim();
				dr["state_code"] = txtStateCode.Text.Trim();			
			#endregion

			#region "Service Code"
			dr["service_code"] = dbCmbServiceCode.Value;
			#endregion

			#region "Exception Code"
			dr["exception_type"] = dbCmbExceptionType.Value;
			#endregion

			#region "Exception"
			dr["consignment_no"] = (String)ViewState["Consignment_no"];
			dr["exception_code"] = (String)ViewState["Exception_Code"];
			dr["exception_description"] = (String)ViewState["Exception_Description"];
			dr["destination_station"] = (String)ViewState["Destination_Station"];
			dr["recipient_zipcode"] = (String)ViewState["Recipient_Zipcode"];
			dr["area"] = (String)ViewState["Area"];
			#endregion

			dtShipmentPODEX.Rows.Add(dr);
			dsShipment.Tables.Add(dtShipmentPODEX);
			return dsShipment;
		}


		private void DefaultScreen()
		{
			#region "Dates"

			rbBookingDate.Checked = true;
			rbPickUpDate.Checked = false;
			rbEstDelDate.Checked = false;
			rbPODDate.Checked=false;
						
			ddMonth.Enabled = true;
			ddMonth.SelectedIndex = -1;
			txtYear.Text = null;
			txtYear.Enabled = true;
			txtPeriod.Text = null;
			txtPeriod.Enabled = false;
			txtTo.Text = null;
			txtTo.Enabled = false;
			txtDate.Text = null;
			txtDate.Enabled = false;				

			#endregion			

			#region "Payer Type"

			lsbCustType.SelectedIndex = -1;
			txtPayerCode.Text = null;

			#endregion

			#region "Route / DC Selection"
			DbComboPathCode.Text="";
			DbComboPathCode.Value="";

			DbComboOriginDC.Text="";
			DbComboOriginDC.Value="";

			DbComboDestinationDC.Text="";
			DbComboDestinationDC.Value="";
			#endregion

			#region "Destination"

			txtZipCode.Text = null;
			txtStateCode.Text = null;

			#endregion

			#region "Service Type"
			dbCmbServiceCode.Text="";
			dbCmbServiceCode.Value="";
			#endregion

			#region "Exception Type"
			dbCmbExceptionType.Text="";
			dbCmbExceptionType.Value="";
			#endregion

			lblErrorMessage.Text = "";			
		}


		private bool ValidateValues()
		{
			bool iCheck=false;
	
				if((ddMonth.SelectedIndex==0)&&(txtYear.Text=="")&&(txtPeriod.Text=="")&&(txtTo.Text=="")&&(txtDate.Text==""))
				{				
					lblErrorMessage.Text=Utility.GetLanguageText(ResourceType.UserMessage,"PLS_ENTER_EST_DVL",utility.GetUserCulture());
					return iCheck=true;
			
				}
				if(rbMonth.Checked == true)
				{
					if((ddMonth.SelectedIndex>0) &&(txtYear.Text==""))
					{
						lblErrorMessage.Text=Utility.GetLanguageText(ResourceType.UserMessage,"MM_YEAR_REQ",utility.GetUserCulture());
						return iCheck=true;
					}
					else if((ddMonth.SelectedIndex==0) &&(txtYear.Text!=""))
					{
						lblErrorMessage.Text=Utility.GetLanguageText(ResourceType.UserMessage,"MM_YEAR_REQ",utility.GetUserCulture());
						return iCheck=true;
					}
					else
					{
						return iCheck=false;
					}
				}
				if(rbPeriod.Checked == true )
				{
					if((txtPeriod.Text!="")&&(txtTo.Text==""))
					{					
						lblErrorMessage.Text=Utility.GetLanguageText(ResourceType.UserMessage,"PLS_ENTER_END_DT",utility.GetUserCulture());
						return iCheck=true;
					}
					else if((txtPeriod.Text=="")&&(txtTo.Text!=""))
					{
						lblErrorMessage.Text=Utility.GetLanguageText(ResourceType.UserMessage,"PLS_START_DT",utility.GetUserCulture());					
						return iCheck=true;			
					}
					else
					{
						return iCheck=false;
					}
				}
				if(rbDate.Checked == true )
				{
					if(txtDate.Text=="")
					{					
						lblErrorMessage.Text=Utility.GetLanguageText(ResourceType.UserMessage,"PLS_ENTER_DT",utility.GetUserCulture());	
						return iCheck=true;				
					}
				}			

				return iCheck;		
		}

		private int ValidateData()
		{
			if (txtYear.Text != "" )
			{
				if (Convert.ToInt32(txtYear.Text) < 2000 || Convert.ToInt32(txtYear.Text) > 2099)
				{
					lblErrorMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"YEAR_00_99",utility.GetUserCulture());
					return -1;
				}
			}
			return 1;
		}
		

		private int LastDayOfMonth(String strMonth, String strYear)
		{
			int intLastDay = 0;
			switch (strMonth)
			{
				case "01":
				case "03":
				case "05":
				case "07":
				case "08":
				case "10":
				case "12":
					intLastDay = 31;
					break;
				case "04":
				case "06":
				case "09":
				case "11":
					intLastDay = 30;
					break;
				case "02":
					//intLastDay = 28;
					//by sittichai 18/02/2008
					DateTime dt = System.DateTime.ParseExact("01/02/"+strYear,"dd/MM/yyyy",null);
					DateTime firstDayOfThisMonth = dt.Subtract(TimeSpan.FromDays(dt.Day - 1));
					DateTime firstDayOfNextMonth = firstDayOfThisMonth.AddMonths(1);
					DateTime lastDayOfThisMonth = firstDayOfNextMonth.Subtract(TimeSpan.FromDays(1)) ;

					intLastDay = Int32.Parse(lastDayOfThisMonth.Day.ToString());
					break;
			}
			return intLastDay;
		}


		private void OpenWindowpage(String strUrl, String strFeatures)
		{
			
			ArrayList paramList = new ArrayList();
			paramList.Add(strUrl);
			String sScript = Utility.GetScript("openParentWindow.js",paramList);
			Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
		}


		#endregion

		#region "Dates Part : Controls"

		private void rbMonth_CheckedChanged(object sender, System.EventArgs e)
		{
			ddMonth.Enabled = true;
			txtYear.Text = null;
			txtYear.Enabled = true;
			txtPeriod.Text = null;
			txtPeriod.Enabled = false;
			txtTo.Text = null;
			txtTo.Enabled = false;
			txtDate.Text = null;
			txtDate.Enabled = false;
		}


		private void rbPeriod_CheckedChanged(object sender, System.EventArgs e)
		{
			ddMonth.Enabled = false;
			txtYear.Text = null;
			txtYear.Enabled = false;
			txtPeriod.Text = null;
			txtPeriod.Enabled = true;
			txtTo.Text = null;
			txtTo.Enabled = true;
			txtDate.Text = null;
			txtDate.Enabled = false;
		}


		private void rbDate_CheckedChanged(object sender, System.EventArgs e)
		{
			ddMonth.Enabled = false;
			txtYear.Text = null;
			txtYear.Enabled = false;
			txtPeriod.Text = null;
			txtPeriod.Enabled = false;
			txtTo.Text = null;
			txtTo.Enabled = false;
			txtDate.Text = null;
			txtDate.Enabled = true;
		}	
		

		#endregion

		#region "Payer Type Part : Controls

		private void btnPayerCode_Click(object sender, System.EventArgs e)
		{
			string strCustPayerType = "";
			for (int i = 0; i <= lsbCustType.Items.Count - 1; i ++)
			{
				if(lsbCustType.Items[i].Selected == true)
				{
					strCustPayerType += lsbCustType.Items[i].Value + ",";
				}
			}
			if((strCustPayerType != null) && (strCustPayerType.Trim().Length > 0))
				strCustPayerType = strCustPayerType.Substring(0, strCustPayerType.Length - 1);

			String sUrl = "CustomerPopup.aspx?FORMID="+"PODEXRptQuery"+
				"&CustType="+strCustPayerType.ToString();
			ArrayList paramList = new ArrayList();
			paramList.Add(sUrl);
			String sScript = Utility.GetScript("openWindowParam.js",paramList);
			Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
		}


		#endregion

		#region "Dates Part : DropDownList"

		private void ddlmonths()
		{
			DataTable dtMonths = new DataTable();
			dtMonths.Columns.Add(new DataColumn("Text", typeof(string)));
			dtMonths.Columns.Add(new DataColumn("StringValue", typeof(string)));
		
			DataRow drNilRow = dtMonths.NewRow();
			drNilRow[0] = "";
			drNilRow[1] = "";
			dtMonths.Rows.Add(drNilRow);

			ArrayList listMonthTxt = Utility.GetCodeValues(m_strAppID,m_strCulture, "months", CodeValueType.StringValue);
			foreach(SystemCode typeSysCode in listMonthTxt)
			{
				DataRow drEach = dtMonths.NewRow();
				drEach[0] = typeSysCode.Text;
				drEach[1] = typeSysCode.StringValue;
				dtMonths.Rows.Add(drEach);
			}
			DataView dvMonths = new DataView(dtMonths);

			ddMonth.DataSource = dvMonths;
			ddMonth.DataTextField = "Text";
			ddMonth.DataValueField = "StringValue";
			ddMonth.DataBind();	
		}
		#endregion

		#region "Route / DC Selection : Controls"

		private void rbLongRoute_CheckedChanged(object sender, System.EventArgs e)
		{
			DbComboPathCode.Text="";
			DbComboPathCode.Value="";
		}


		private void rbShortRoute_CheckedChanged(object sender, System.EventArgs e)
		{
			DbComboPathCode.Text="";
			DbComboPathCode.Value="";	
		}


		private void rbAirRoute_CheckedChanged(object sender, System.EventArgs e)
		{
			DbComboPathCode.Text="";
			DbComboPathCode.Value="";
		}


		#endregion

		#region "Destination : Controls"

		private void btnZipCode_Click(object sender, System.EventArgs e)
		{
			String sUrl = "ZipCodePopup.aspx?FORMID=PODEXRptQuery"+"&ZIPCODE_CID="+txtZipCode.ClientID+"&ZIPCODE="+txtZipCode.Text.Trim().ToString();
			ArrayList paramList = new ArrayList();
			paramList.Add(sUrl);
			String sScript = Utility.GetScript("openParentWindow.js",paramList);
			Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
		}
		
		private void btnStateCode_Click(object sender, System.EventArgs e)
		{
			String sUrl = "StatePopup.aspx?FORMID=PODEXRptQuery"+"&STATECODE="+txtStateCode.ClientID+"&STATECODE_TEXT="+txtStateCode.Text.Trim().ToString();
			ArrayList paramList = new ArrayList();
			paramList.Add(sUrl);
			String sScript = Utility.GetScript("openParentWindow.js",paramList);
			Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
		}	

		#endregion

		#region "Payer Type Part : DropDownList"

		public void LoadCustomerTypeList()
		{
			ArrayList systemCodes = Utility.GetCodeValues(utility.GetAppID(),utility.GetUserCulture(),"customer_type",CodeValueType.StringValue);
			foreach(SystemCode sysCode in systemCodes)
			{	
				ListItem lstItem = new ListItem();
				lstItem.Text = sysCode.Text;
				lstItem.Value = sysCode.StringValue;
				lsbCustType.Items.Add(lstItem);
			}
		}

		#endregion

		#region "Route / DC Selection : DropDownList"

		private void SetDbComboServerStates()
		{
			String strDeliveryType=null;
			if (rbLongRoute.Checked==true)
				strDeliveryType="L";
			else if (rbShortRoute.Checked==true)
				strDeliveryType="S";
			else if (rbAirRoute.Checked==true)
				strDeliveryType="A";

			Hashtable hash = new Hashtable();
			hash.Add("strDeliveryType", strDeliveryType);
			DbComboPathCode.ServerState = hash;
			DbComboPathCode.ServerStateSecretString = "kdfsjgnskajh[apq2-4uhg465654435879mnbfdm cxs;q4";
		}


		[Cambro.Web.DbCombo.ResultsMethodAttribute(true)]
		public static object DbComboPathCodeSelect(Cambro.Web.DbCombo.ServerMethodArgs args)
		{
			Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings,HttpContext.Current.Session);
			String strAppID = util.GetAppID();
			String strEnterpriseID = util.GetEnterpriseID();
			
			String strDelType = "L";			
			String strWhereClause=null;
			if(args.ServerState != null && args.ServerState.Authenticate("kdfsjgnskajh[apq2-4uhg465654435879mnbfdm cxs;q4"))
			{
				if(args.ServerState["strDeliveryType"] != null && args.ServerState["strDeliveryType"].ToString().Length > 0)
				{
					strDelType = args.ServerState["strDeliveryType"].ToString();
					strWhereClause=" and Delivery_Type='"+Utility.ReplaceSingleQuote(strDelType)+"'";
					if(strDelType == "S") 
					{
						strWhereClause=strWhereClause+"and path_code in ( ";
						strWhereClause=strWhereClause+"select distinct delivery_route ";
						strWhereClause=strWhereClause+"from Zipcode ";
						strWhereClause=strWhereClause+"where applicationid = '"+strAppID+"' ";
						strWhereClause=strWhereClause+"and enterpriseid = '"+ strEnterpriseID +"') ";
					}
				}				
			}
			
			DataSet dataset = com.ties.classes.DbComboDAL.PathCodeQuery(strAppID,strEnterpriseID,args, strWhereClause);	
			return dataset;
		}


		[Cambro.Web.DbCombo.ResultsMethodAttribute(true)]
		public static object DbComboDistributionCenterSelect(Cambro.Web.DbCombo.ServerMethodArgs args)
		{
			Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings,HttpContext.Current.Session);
			String strAppID = util.GetAppID();
			String strEnterpriseID = util.GetEnterpriseID();
			
			DataSet dataset = com.ties.classes.DbComboDAL.DistributionCenterQuery(strAppID,strEnterpriseID,args);	
			return dataset;
		}	

		#endregion

		#region "Service Type : DropDownList"

		[Cambro.Web.DbCombo.ResultsMethodAttribute(true)]
		public static object ServiceCodeServerMethod(Cambro.Web.DbCombo.ServerMethodArgs args)
		{
			Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings,HttpContext.Current.Session);
			String strAppID = util.GetAppID();
			String strEnterpriseID = util.GetEnterpriseID();

			String strServiceCode="";

			if(args.ServerState != null && args.ServerState.Authenticate("kdfsjgnskajh[apq2-4uhg465654435879mnbfdm qrs;q4"))
			{
				if(args.ServerState["strServiceCode"] != null && args.ServerState["strServiceCode"].ToString().Length > 0)
				{
					strServiceCode = args.ServerState["strServiceCode"].ToString();					
				}
			}	
			DataSet dataset = com.ties.classes.DbComboDAL.ServiceCodeQuery (strAppID,strEnterpriseID,args);	
			return dataset;                
		}

		#endregion	

		//Jeab 20 Jun 2011
		#region "Exception Type : DropDownList"

		[Cambro.Web.DbCombo.ResultsMethodAttribute(true)]
		public static object ExceptionTypeServerMethod(Cambro.Web.DbCombo.ServerMethodArgs args)
		{
			Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings,HttpContext.Current.Session);
			String strAppID = util.GetAppID();
			String strEnterpriseID = util.GetEnterpriseID();

			String strExceptionType="";

			if(args.ServerState != null && args.ServerState.Authenticate("kdfsjgnskajh[apq2-4uhg465654435879mnbfdm qrs;q4"))
			{
				if(args.ServerState["strExceptionType"] != null && args.ServerState["strExceptionType"].ToString().Length > 0)
				{
					strExceptionType = args.ServerState["strExceptionType"].ToString();					
				}
			}	
			DataSet dataset = com.ties.classes.DbComboDAL.ExceptionTypeQuery(strAppID,strEnterpriseID,args);	
			return dataset;                
		}

		#endregion	
		//Jeab 20 Jun 2011  =========> End
		
		//Jeab 27 Jun 2011
		#region "Count Total Consignment"

		public struct CountConsignment
		{
			public int totCons;
		}

		public static int  GetTotalConsignment(String a_strAppID,String a_strEnterpriseID,String strDateType,String strDateFrom,String strDateTo,String strPayerID
										,String strDestinationDC,String strPostalCode,String strDestinationState,String strOriginDC,String strRouteType,String strRouteCode
										,String strPayerType,String strService_Code ,String strPathOriginDC ,String strPathDestinationDC)
		{
			DbConnection dbConApp = null;
			IDbCommand dbCmd = null;
			DataSet dsTotalCons = null;
			CountConsignment CountConsignment = new CountConsignment();
			int totCons = 0;

			dbConApp = DbConnectionManager.GetInstance().GetDbConnection(a_strAppID,a_strEnterpriseID);

			if(dbConApp == null)
			{
				Logger.LogTraceError("PODEXRptQuery","GetTotalConsignment","ERR001","dbConApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			StringBuilder strQry = new StringBuilder();
			strQry.Append(" Select  Count(consignment_no)   as totCons   from shipment  where applicationid = '");
			strQry.Append(a_strAppID);
			strQry.Append("' and enterpriseid = '");
			strQry.Append(a_strEnterpriseID);
			strQry.Append("' ");

			if (strDateType.ToString() != "")
			{
				if(strDateType == "B" ) 
				{
					strQry.Append(" and booking_datetime > = '");
					strQry.Append(strDateFrom+"'");
					strQry.Append(" and booking_datetime < = '");
					strQry.Append(strDateTo+"'");
				}
				else if(strDateType == "E" ) 
				{
					strQry.Append(" and est_delivery_datetime > = '");
					strQry.Append(strDateFrom+"'");
					strQry.Append(" and est_delivery_datetime < = '");
					strQry.Append(strDateTo+"'");
				}			
				else if(strDateType == "A" ) 
				{
					strQry.Append(" and act_pickup_datetime > = '");
					strQry.Append(strDateFrom+"'");
					strQry.Append(" and act_pickup_datetime < = '");
					strQry.Append(strDateTo+"'");
				}			
				else if(strDateType == "P" ) 
				{
					strQry.Append(" and act_delivery_date > = '");
					strQry.Append(strDateFrom+"'");
					strQry.Append(" and act_delivery_date < = '");
					strQry.Append(strDateTo+"'");
				}			
			}
//			if (strPayerID.ToString() != "")
//			{
//				strQry.Append(" and payerid = '");
//				strQry.Append(strPayerID+"'");
//			}
//			if (strDestinationDC.ToString() !="")
//			{
//				strQry.Append(" and destination_station = '");
//				strQry.Append(strDestinationDC+"'");
//			}
//			if (strPostalCode.ToString() !="")
//			{
//				strQry.Append(" and recipient_zipcode = '");
//				strQry.Append(strPostalCode+"'");
//			}
//			if (strDestinationState.ToString() !="")
//			{
//				strQry.Append(" and destination_state_code = '");
//				strQry.Append(strDestinationState+"'");
//			}
//			if (strOriginDC.ToString() !="")
//			{
//				strQry.Append(" and origin_station = '");
//				strQry.Append(strOriginDC+"'");
//			}
//			if (strService_Code.ToString() !="")
//			{
//				strQry.Append(" and service_code = '");
//				strQry.Append(strService_Code+"'");
//			}
//			if (strRouteCode.ToString() !="")
//			{
//				if (strRouteType.ToString() =="L" || strRouteType.ToString() == "A" )
//				{
//					strQry.Append(" and origin_station = '");
//					strQry.Append(strPathOriginDC+"'");
//					strQry.Append(" and destination_station = '");
//					strQry.Append(strPathDestinationDC+"'");
//				}
//				else
//				{
//					strQry.Append(" and route_code = '");
//					strQry.Append(strRouteCode+"'");
//				}
//			}
//			if (strPayerType.ToString() !="")
//			{
//				if (strPayerType.ToString() == "C")
//				{
//					strQry.Append(" and payer_type = 'C' ");
//				}
//				else if (strPayerType.ToString() == "A")
//				{
//					strQry.Append(" and payer_type = 'A' ");
//				}
//				else if (strPayerType.ToString() == "N")
//				{
//					strQry.Append(" and payer_type = 'N' ");
//				}
//				else if (strPayerType.ToString() == "CA")
//				{
//					strQry.Append(" and ( payer_type = 'A'  or payer_type = 'C' ) ");
//				}
//				else if (strPayerType.ToString() == "CN")
//				{
//					strQry.Append(" and ( payer_type = 'N'  or payer_type = 'C' ) ");
//				}
//				else if (strPayerType.ToString() == "AN")
//				{
//					strQry.Append(" and ( payer_type = 'A'  or payer_type = 'N' ) ");
//				}
//				else if (strPayerType.ToString() == "CAN")
//				{
//					strQry.Append(" and ( payer_type = 'A'  or payer_type = 'C'  or payer_type = 'N'  ) ");
//				}
//			}


			
			dbCmd = dbConApp.CreateCommand(strQry.ToString(),CommandType.Text);
			try
			{
				dsTotalCons =(DataSet)dbConApp.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
			}
			catch(ApplicationException appExpection)
			{
				Logger.LogTraceError("TIESUtility","GetTotalConsignment","ERR002","Error in the query String");
				throw appExpection;
			}
			if(dsTotalCons.Tables[0].Rows.Count > 0)
			{
				DataRow drEach = dsTotalCons.Tables[0].Rows[0];

				if((drEach["totCons"] != null) && (!drEach["totCons"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					totCons = (int)drEach["totCons"];					
				}				
			}
			return (int)totCons;
		}
		#endregion
		//Jeab 27 Jun 2011  =========> End

	}
}