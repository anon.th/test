<%@ Import Namespace="com.common.util" %>
<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<%@ Page language="c#" Codebehind="AgentZone.aspx.cs" AutoEventWireup="false" Inherits="TIES.WebUI.AgentZone" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>AgentZone</title>
		<LINK href="css/Styles.css" type="text/css" rel="stylesheet" name="stylesheet">
		<meta name="GENERATOR" Content="Microsoft Visual Studio 7.0">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<!--#INCLUDE FILE="msFormValidations.inc"-->
	</HEAD>
	<BODY MS_POSITIONING="GridLayout">
		<form id="AgentZone" method="post" runat="server">
			<asp:button id="btnExecQry" style="Z-INDEX: 103; LEFT: 91px; POSITION: absolute; TOP: 44px" runat="server" CssClass="queryButton" Text="Execute Query" CausesValidation="False" Width="130px"></asp:button>
			<asp:button id="btnMovefirst" style="Z-INDEX: 114; LEFT: 483px; POSITION: absolute; TOP: 44px" runat="server" Width="30px" CausesValidation="False" Text="|<" CssClass="queryButton" Height="20"></asp:button>
			<asp:button id="btnAgentName" style="Z-INDEX: 113; LEFT: 626px; POSITION: absolute; TOP: 80px" runat="server" CausesValidation="False" Text="...." CssClass="searchButton"></asp:button>
			<asp:button id="BtnStatusCode" style="Z-INDEX: 112; LEFT: 241px; POSITION: absolute; TOP: 80px" runat="server" CausesValidation="False" Text="...." CssClass="searchButton"></asp:button>
			<asp:label id="lblTitle" style="Z-INDEX: 107; LEFT: 31px; POSITION: absolute; TOP: 6px" runat="server" Height="26px" CssClass="mainTitleSize" Width="196px"> Agent Zone</asp:label><asp:button id="btnQry" style="Z-INDEX: 104; LEFT: 31px; POSITION: absolute; TOP: 44px" runat="server" CssClass="queryButton" Text="Query" CausesValidation="False" Width="62px"></asp:button>
			<asp:datagrid id="dgAgentZone" style="Z-INDEX: 102; LEFT: 31px; POSITION: absolute; TOP: 142px" runat="server" Width="642px" SelectedItemStyle-CssClass="gridFieldSelected" AllowCustomPaging="True" PageSize="5" AllowPaging="True" AutoGenerateColumns="False" ItemStyle-Height="20">
				<SelectedItemStyle CssClass="gridFieldSelected"></SelectedItemStyle>
				<ItemStyle Height="20px" CssClass="gridField"></ItemStyle>
				<Columns>
					<asp:ButtonColumn Text="&lt;IMG SRC='images/butt-select.gif' border=0 title='Select' &gt;" CommandName="Select">
						<HeaderStyle HorizontalAlign="Center" Width="3%" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle HorizontalAlign="Center"></ItemStyle>
					</asp:ButtonColumn>
					<asp:EditCommandColumn ButtonType="LinkButton" UpdateText="&lt;IMG SRC='images/butt-update.gif' border=0 title='Update' &gt;" CancelText="&lt;IMG SRC='images/butt-cancel.gif' border=0 title='Cancel' &gt;" EditText="&lt;IMG SRC='images/butt-edit.gif' border=0 title='Edit' &gt;">
						<HeaderStyle Width="5%" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle HorizontalAlign="Left"></ItemStyle>
					</asp:EditCommandColumn>
					<asp:ButtonColumn Text="&lt;IMG SRC='images/butt-delete.gif' border=0 title='Delete' &gt;" CommandName="Delete">
						<HeaderStyle HorizontalAlign="Center" Width="3%" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle HorizontalAlign="Center"></ItemStyle>
					</asp:ButtonColumn>
					<asp:TemplateColumn HeaderText="Zone Code">
						<HeaderStyle Font-Bold="True" Width="19%" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle Wrap="False"></ItemStyle>
						<ItemTemplate>
							<asp:Label CssClass="gridLabel" ID="lblZoneCode" Runat="server"></asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<cc1:mstextbox CssClass="gridTextBox" ID="txtZoneCode" Runat="server" Enabled="True" MaxLength="12" TextMaskType="msUpperAlfaNumericWithUnderscore"></cc1:mstextbox>
							<asp:RequiredFieldValidator ID="scValidator" Runat="server" BorderWidth="0" Display="None" ControlToValidate="txtZoneCode" ErrorMessage="Zip Code is required field"></asp:RequiredFieldValidator>
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Zone Description">
						<HeaderStyle Width="60%" CssClass="gridHeading"></HeaderStyle>
						<ItemTemplate>
							<asp:Label CssClass="gridLabel" ID="lblDescription" Runat="server" Enabled="True"></asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<asp:TextBox CssClass="gridTextBox" ID="txtVDescription" Runat="server" MaxLength="200"></asp:TextBox>
						</EditItemTemplate>
					</asp:TemplateColumn>
				</Columns>
				<PagerStyle NextPageText="Next" PrevPageText="Previous" HorizontalAlign="Right" CssClass="normalText"></PagerStyle>
			</asp:datagrid>
			<table style="Z-INDEX: 105; LEFT: 25px; WIDTH: 647px; POSITION: absolute; TOP: 323px; HEIGHT: 257px">
				<tr>
					<td><asp:button id="btnZipInsert" runat="server" CssClass="queryButton" Text="Insert" CausesValidation="False" Width="62px"></asp:button></td>
				</tr>
				<tr>
					<td><asp:label id="lblZipIncludedMessage" runat="server" CssClass="errorMsgColor" Height="28px" Width="490px"></asp:label></td>
				</tr>
				<tr>
					<td>
						<fieldset style="WIDTH: 592px; HEIGHT: 178px"><legend><asp:Label ID="Label1" runat="server" CssClass="tableField">Zip Code Included</asp:Label></legend>
							<asp:datagrid id="dgZipcodIncluded" runat="server" Width="630px" AllowCustomPaging="True" PageSize="5" AllowPaging="True" AutoGenerateColumns="False" ItemStyle-Height="20">
								<ItemStyle Height="20px"></ItemStyle>
								<Columns>
									<asp:ButtonColumn Text="&lt;IMG SRC='images/butt-delete.gif' border=0 title='Delete' &gt;" ButtonType="LinkButton" CommandName="Delete">
										<HeaderStyle HorizontalAlign="Center" Width="3%" CssClass="gridHeading"></HeaderStyle>
										<ItemStyle HorizontalAlign="Center" CssClass="gridField"></ItemStyle>
									</asp:ButtonColumn>
									<asp:TemplateColumn HeaderText="Zip Code">
										<HeaderStyle Width="20%" Font-Bold="True" CssClass="gridHeading"></HeaderStyle>
										<ItemStyle CssClass="gridField"></ItemStyle>
										<ItemTemplate>
											<asp:Label CssClass="gridLabel" ID="lblZipcode" Text='<%#DataBinder.Eval(Container.DataItem,"zipcode")%>' Runat="server">
											</asp:Label>
										</ItemTemplate>
										<EditItemTemplate>
											<cc1:mstextbox CssClass="gridTextBox" ID="txtZipcode" Text='<%#DataBinder.Eval(Container.DataItem,"zipcode")%>' Runat="server" Enabled="True" MaxLength="10" TextMaskType="msUpperAlfaNumericWithUnderscore">
											</cc1:mstextbox>
											<asp:RequiredFieldValidator ID="exValidator" Runat="server" BorderWidth="0" Display="None" ControlToValidate="txtZipcode" ErrorMessage="Zipcode is required field"></asp:RequiredFieldValidator>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:ButtonColumn Text="&lt;IMG SRC='images/butt-search.gif' border=0 title='Search' &gt;" ButtonType="LinkButton" CommandName="search">
										<HeaderStyle HorizontalAlign="Center" Width="3%" CssClass="gridHeading"></HeaderStyle>
										<ItemStyle HorizontalAlign="Center" CssClass="gridField"></ItemStyle>
									</asp:ButtonColumn>
									<asp:TemplateColumn HeaderText="State">
										<HeaderStyle Width="25%" CssClass="gridHeading"></HeaderStyle>
										<ItemStyle CssClass="gridField"></ItemStyle>
										<ItemTemplate>
											<asp:Label CssClass="gridLabel" ID="lblState" Text='<%#DataBinder.Eval(Container.DataItem,"state_name")%>' Runat="server" >
											</asp:Label>
										</ItemTemplate>
										<EditItemTemplate>
											<asp:TextBox CssClass="gridTextBox" ID="txtState" Text='<%#DataBinder.Eval(Container.DataItem,"state_name")%>' Runat="server" ReadOnly="true">
											</asp:TextBox>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Country">
										<HeaderStyle Width="25%" CssClass="gridHeading"></HeaderStyle>
										<ItemStyle CssClass="gridField"></ItemStyle>
										<ItemTemplate>
											<asp:Label CssClass="gridLabel" ID="lblCountry" Text='<%#DataBinder.Eval(Container.DataItem,"country")%>' Runat="server" >
											</asp:Label>
										</ItemTemplate>
										<EditItemTemplate>
											<asp:TextBox CssClass="gridTextBox" ID="txtCountry" Text='<%#DataBinder.Eval(Container.DataItem,"country")%>' Runat="server" ReadOnly="true">
											</asp:TextBox>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="VAS Surcharge">
										<HeaderStyle Width="35%" CssClass="gridHeading"></HeaderStyle>
										<ItemStyle CssClass="gridField"></ItemStyle>
										<ItemTemplate>
											<asp:Label CssClass="gridLabel" ID="lblVAS" Text='<%#DataBinder.Eval(Container.DataItem,"country")%>' Runat="server" >
											</asp:Label>
										</ItemTemplate>
										<EditItemTemplate>
											<asp:TextBox CssClass="gridTextBox" ID="txtVAS" Text='<%#DataBinder.Eval(Container.DataItem,"country")%>' Runat="server" ReadOnly="true">
											</asp:TextBox>
										</EditItemTemplate>
									</asp:TemplateColumn>
								</Columns>
								<PagerStyle NextPageText="Next" PrevPageText="Previous" HorizontalAlign="Right" CssClass="normalText"></PagerStyle>
							</asp:datagrid>
						</fieldset>
					</td>
				</tr>
			</table>
			<asp:label id="lblAgentZoneMessage" style="Z-INDEX: 106; LEFT: 310px; POSITION: absolute; TOP: 108px" runat="server" CssClass="errorMsgColor" Height="7px" Width="348px"></asp:label><asp:validationsummary id="ValidationSummary1" style="Z-INDEX: 101; LEFT: 36px; POSITION: absolute; TOP: 108px" Height="20px" Width="258px" ShowSummary="False" DisplayMode="SingleParagraph" ShowMessageBox="True" Runat="server"></asp:validationsummary>
			<asp:Label id="lblAgentCode" style="Z-INDEX: 108; LEFT: 37px; POSITION: absolute; TOP: 83px" runat="server" CssClass="tablelabel">Agent Code</asp:Label>
			<asp:TextBox id="txtAgentID" style="Z-INDEX: 109; LEFT: 110px; POSITION: absolute; TOP: 79px" runat="server" CssClass="textfield"></asp:TextBox>
			<asp:Label id="lblAgentName" style="Z-INDEX: 110; LEFT: 284px; POSITION: absolute; TOP: 83px" runat="server" CssClass="tablelabel">Agent Name</asp:Label>
			<asp:TextBox id="txtAgentName" style="Z-INDEX: 111; LEFT: 367px; POSITION: absolute; TOP: 79px" runat="server" Width="237px" CssClass="textfield"></asp:TextBox>
			<asp:button id="btnMovePrevious" style="Z-INDEX: 115; LEFT: 517px; POSITION: absolute; TOP: 44px" runat="server" Width="30px" CausesValidation="False" Text="<" CssClass="queryButton" Height="20"></asp:button>
			<asp:textbox id="Txt_RecCnt" style="Z-INDEX: 116; LEFT: 552px; POSITION: absolute; TOP: 44px" runat="server" Width="29px" Enabled="False" Height="18px"></asp:textbox>
			<asp:button id="btnMovenext" style="Z-INDEX: 117; LEFT: 585px; POSITION: absolute; TOP: 44px" runat="server" Width="30px" CausesValidation="False" Text=">" CssClass="queryButton" Height="20"></asp:button>
			<asp:button id="btnMoveLast" style="Z-INDEX: 118; LEFT: 619px; POSITION: absolute; TOP: 44px" runat="server" Width="30px" CausesValidation="False" Text=">|" CssClass="queryButton" Height="20"></asp:button></form>
	</BODY>
</HTML>
