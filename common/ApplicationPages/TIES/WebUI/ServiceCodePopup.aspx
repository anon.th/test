<%@ Page language="c#" Codebehind="ServiceCodePopup.aspx.cs" AutoEventWireup="false" Inherits="TIES.WebUI.ServiceCodePopup" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>ServiceCodePopup</title>
		<LINK href="css/Styles.css" type="text/css" rel="stylesheet" name="stylesheet">
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<script language="javascript" src="Scripts/settingScrollPosition.js"></script>
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="ServiceCodePopup" method="post" runat="server">
			<asp:datagrid id="dgServiceCode" style="Z-INDEX: 101; POSITION: absolute; TOP: 135px; LEFT: 45px"
				runat="server" OnPageIndexChanged="Paging" PageSize="10" AllowPaging="True" Width="522px"
				AutoGenerateColumns="False" BorderColor="#CCCCCC" BackColor="White" CellPadding="3" BorderWidth="1px"
				BorderStyle="None" CssClass="gridHeading">
				<SelectedItemStyle Font-Bold="True" ForeColor="White" BackColor="#669999"></SelectedItemStyle>
				<HeaderStyle Width="10%" CssClass="gridHeading"></HeaderStyle>
				<ItemStyle CssClass="popupGridField"></ItemStyle>
				<FooterStyle ForeColor="#000066" BackColor="White"></FooterStyle>
				<Columns>
					<asp:BoundColumn DataField="service_code" HeaderText="Service Code"></asp:BoundColumn>
					<asp:BoundColumn DataField="service_description" HeaderText="Description"></asp:BoundColumn>
					<asp:BoundColumn DataField="service_charge_percent" HeaderText="Service Charge percent (%)" Visible="False"></asp:BoundColumn>
					<asp:BoundColumn DataField="service_charge_amt" HeaderText="Order By" DataFormatString="{0:#0;-#0}"></asp:BoundColumn>
					<asp:ButtonColumn Text="Select" CommandName="Select"></asp:ButtonColumn>
				</Columns>
				<PagerStyle HorizontalAlign="Left" ForeColor="#000066" BackColor="White" Mode="NumericPages"></PagerStyle>
			</asp:datagrid><asp:label id="lblErrorMsg2" style="Z-INDEX: 115; POSITION: absolute; TOP: 32px; LEFT: 328px"
				runat="server" Width="249px" CssClass="errorMsgColor" Height="19px"></asp:label><asp:button id="btnClose" style="Z-INDEX: 110; POSITION: absolute; TOP: 93px; LEFT: 509px" runat="server"
				Width="52px" CssClass="buttonProp" CausesValidation="False" Text="Close" Height="21px"></asp:button><asp:label id="lblCustName" style="Z-INDEX: 109; POSITION: absolute; TOP: 72px; LEFT: 216px"
				runat="server" CssClass="tablelabel">Description</asp:label><asp:label id="lblErrorMsg" style="Z-INDEX: 106; POSITION: absolute; TOP: 114px; LEFT: 44px"
				runat="server" Width="566px" CssClass="errorMsgColor" Height="19px"></asp:label><asp:button id="btnSearch" style="Z-INDEX: 104; POSITION: absolute; TOP: 93px; LEFT: 431px"
				runat="server" Width="77px" CssClass="buttonProp" CausesValidation="False" Text="Search" Height="21px"></asp:button><asp:textbox id="txtServiceCode" style="Z-INDEX: 103; POSITION: absolute; TOP: 95px; LEFT: 49px"
				runat="server" Width="116px" CssClass="textField" Height="19px"></asp:textbox><asp:label id="lblCustID" style="Z-INDEX: 100; POSITION: absolute; TOP: 72px; LEFT: 49px" runat="server"
				Width="115px" CssClass="tableLabel" Height="16px">Service Code</asp:label><asp:button id="btnRefresh" style="Z-INDEX: 105; POSITION: absolute; DISPLAY: none; TOP: 108px; LEFT: 569px"
				runat="server" Width="67px" Text="HIDDEN"></asp:button><asp:textbox id="txtServiceDescription" style="Z-INDEX: 102; POSITION: absolute; TOP: 94px; LEFT: 209px"
				runat="server" Width="211px" CssClass="textField" Height="19px"></asp:textbox><asp:label id="lblServiceTitle" style="Z-INDEX: 108; POSITION: absolute; TOP: 15px; LEFT: 20px"
				runat="server" Width="585px" CssClass="mainTitleSize" Height="24px">Service Code</asp:label><asp:textbox id="txtSendPostCode" style="Z-INDEX: 111; POSITION: absolute; TOP: 56px; LEFT: 48px"
				runat="server" Width="120px" Visible="False"></asp:textbox><asp:textbox id="txtRecipPostcode" style="Z-INDEX: 112; POSITION: absolute; TOP: 56px; LEFT: 184px"
				runat="server" Width="136px" Visible="False"></asp:textbox><asp:label id="lblSendPostCode" style="Z-INDEX: 113; POSITION: absolute; TOP: 40px; LEFT: 48px"
				runat="server" Width="120px" CssClass="tableLabel" Visible="False">Sender Postal Code</asp:label>
			<asp:Label id="lblRecipPostCode" style="Z-INDEX: 114; POSITION: absolute; TOP: 40px; LEFT: 184px"
				runat="server" Width="136px" CssClass="tableLabel" Visible="False">Recipient Postal Code</asp:Label></form>
	</body>
</HTML>
