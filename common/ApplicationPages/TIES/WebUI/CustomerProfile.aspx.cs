using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.common.classes;
using com.common.RBAC;
using com.common.util;
using com.common.DAL;
using com.common.applicationpages;
using com.ties.DAL;
using com.ties.classes;
using System.Text;
using System.Configuration;
using System.Data.OleDb;
using Cambro.Web.DbCombo;
using TIESDAL;
using com.common.util;

namespace com.ties
{
	public class CustomerProfile : com.common.applicationpages.BasePage
	{
		protected System.Web.UI.WebControls.Button btnQry;
		protected System.Web.UI.WebControls.Button btnExecQry;
		protected System.Web.UI.WebControls.Button btnSave;
		protected System.Web.UI.WebControls.Button btnDelete;
		protected System.Web.UI.WebControls.Button btnGoToFirstPage;
		protected System.Web.UI.WebControls.Button btnPreviousPage;
		protected System.Web.UI.WebControls.Button btnNextPage;
		protected System.Web.UI.WebControls.Button btnGoToLastPage;
		protected System.Web.UI.WebControls.TextBox txtCountRec;
		protected System.Web.UI.WebControls.Label lblCPErrorMessage;
		protected System.Web.UI.WebControls.DropDownList ddlCreditTerm;
		protected System.Web.UI.WebControls.Button btnZipcodePopup;
		protected System.Web.UI.WebControls.Button btnSetBillPlacementRules;
		protected System.Web.UI.WebControls.Button btnChangeStatus;
		protected Cambro.Web.DbCombo.DbCombo DbComboMasterAccount;
		protected System.Web.UI.HtmlControls.HtmlTable CustomerInfoTable;
		protected Microsoft.Web.UI.WebControls.TabStrip TabStrip1;
		protected System.Web.UI.WebControls.DataGrid dgStatusCodes;
		protected System.Web.UI.WebControls.DataGrid dgExceptionCodes;
		protected System.Web.UI.WebControls.DataGrid dgQuotation;
		protected Microsoft.Web.UI.WebControls.MultiPage customerProfileMultiPage;
		protected System.Web.UI.WebControls.ValidationSummary custValidationSummary;
		protected System.Web.UI.WebControls.Label lblMainTitle;
		protected System.Web.UI.WebControls.Label lblNumRec;
		protected System.Web.UI.WebControls.Label lblNumRefRec;
		protected System.Web.UI.WebControls.Button btnSCInsert;
		protected System.Web.UI.WebControls.Button btnExInsert;
		protected System.Web.UI.WebControls.Label lblSCMessage;
		protected System.Web.UI.WebControls.Label lblEXMessage;
		protected System.Web.UI.WebControls.DataGrid dgCustZones;
		protected System.Web.UI.WebControls.Button btnInsertFR;
		protected System.Web.UI.WebControls.Button btnInsert;
		protected System.Web.UI.WebControls.Button btnExecuteQueryFR;
		protected System.Web.UI.WebControls.DataGrid dgVAS;
		protected System.Web.UI.WebControls.Button btnInsertVAS;
		protected System.Web.UI.WebControls.Button btnViewAllVAS;
		protected System.Web.UI.WebControls.Label Label8;
		protected System.Web.UI.WebControls.Label Label9;
		protected System.Web.UI.WebControls.TextBox txtCustAccDispTab6;
		protected System.Web.UI.WebControls.Label Label10;
		protected System.Web.UI.WebControls.TextBox txtCustNameDispTab6;
		protected System.Web.UI.WebControls.Button btnQueryFR;
		protected System.Web.UI.HtmlControls.HtmlTable Table4;
		protected System.Web.UI.HtmlControls.HtmlTable Table6;
		protected System.Web.UI.WebControls.Button btnImport;
		protected System.Web.UI.WebControls.TextBox txtFilePath;
		protected System.Web.UI.HtmlControls.HtmlInputFile inFile;
		protected System.Web.UI.WebControls.Button btnImport_Save;
		protected System.Web.UI.WebControls.Button btnImport_Cancel;
		protected System.Web.UI.WebControls.Button btnImport_Show;
		protected System.Web.UI.HtmlControls.HtmlGenericControl divBrowse;
		protected System.Web.UI.WebControls.TextBox txtRefNo;
		protected System.Web.UI.WebControls.DropDownList ddlStatus;
		protected System.Web.UI.WebControls.TextBox txtCustomerName;
		protected System.Web.UI.WebControls.TextBox txtContactPerson;
		protected System.Web.UI.WebControls.TextBox txtAddress1;
		protected System.Web.UI.WebControls.TextBox txtTelephone;
		protected System.Web.UI.WebControls.TextBox txtAddress2;
		protected System.Web.UI.WebControls.TextBox txtFax;
		protected System.Web.UI.WebControls.TextBox txtZipCode;
		protected System.Web.UI.WebControls.TextBox txtState;
		protected System.Web.UI.WebControls.TextBox txtEmail;
		protected System.Web.UI.WebControls.TextBox txtCountry;
		protected System.Web.UI.WebControls.TextBox txtCreatedBy;
		protected System.Web.UI.WebControls.TextBox txtCreditLimit;
		protected System.Web.UI.WebControls.TextBox txtCreditOutstanding;
		protected System.Web.UI.WebControls.RadioButton radioBtnCash;
		protected System.Web.UI.WebControls.RadioButton radioBtnCredit;
		protected System.Web.UI.WebControls.DropDownList ddlMBG;
		protected System.Web.UI.WebControls.DropDownList ddlDimByTOT;  //Jeab 8 Dec 10
		protected System.Web.UI.WebControls.TextBox txtFirstShipDate;  //Jeab  17 Feb 12
		protected System.Web.UI.WebControls.TextBox txtCategory;  //Jeab  17 Feb 12
		protected System.Web.UI.WebControls.TextBox txtActiveQuatationNo;
		protected System.Web.UI.WebControls.TextBox txtNextBillPlacementDate;
		protected System.Web.UI.WebControls.TextBox txtRemarkBp;
		protected System.Web.UI.WebControls.CheckBox chkBox;
		protected System.Web.UI.WebControls.TextBox txtISectorCode;
		protected System.Web.UI.WebControls.Button btnISectorSearch;
		protected System.Web.UI.WebControls.TextBox txtSalesmanID;
		protected System.Web.UI.WebControls.Button btnSearchSalesman;
		protected System.Web.UI.WebControls.TextBox txtPromisedTotWt;
		protected System.Web.UI.WebControls.TextBox txtInsurancePercentSurcharge;
		protected System.Web.UI.WebControls.TextBox txtPromisedPeriod;
		protected System.Web.UI.WebControls.TextBox txtFreeInsuranceAmt;
		protected System.Web.UI.WebControls.TextBox txtPromisedTotPkg;
		protected System.Web.UI.WebControls.TextBox txtMaxAmt;
		protected System.Web.UI.WebControls.DropDownList ddlApplyDimWt;
		protected System.Web.UI.WebControls.TextBox txtMinInsuranceSurcharge;
		protected System.Web.UI.WebControls.DropDownList ddlApplyEsaSurcharge;
		protected System.Web.UI.WebControls.TextBox txtMaxInsuranceSurcharge;
		protected System.Web.UI.WebControls.DropDownList ddlApplyEsaSurchargeDel;
		protected System.Web.UI.WebControls.DropDownList ddlPodSlipRequired;
		protected System.Web.UI.WebControls.TextBox txtCODSurchargeAmt;
		protected System.Web.UI.WebControls.DropDownList ddlInvoiceRequried;
		protected System.Web.UI.WebControls.TextBox txtCODSurchargePercent;
		protected System.Web.UI.WebControls.TextBox msDensityFactor;
		protected System.Web.UI.WebControls.TextBox txtMaxCODSurcharge;
		protected System.Web.UI.WebControls.DropDownList ddlCustomerType;
		protected System.Web.UI.WebControls.TextBox txtMinCODSurcharge;
		protected System.Web.UI.WebControls.TextBox msDiscountBand;
		protected System.Web.UI.WebControls.TextBox msOtherSurchargeAmount;
		protected System.Web.UI.WebControls.TextBox msOtherSurchargePercentage;
		protected System.Web.UI.WebControls.TextBox msOtherSurchargeMinimum;
		protected System.Web.UI.WebControls.TextBox msOtherSurchargeMaximum;
		protected System.Web.UI.WebControls.TextBox msOtherSurchargeDescription;
		protected System.Web.UI.WebControls.TextBox txtRemark;
		protected System.Web.UI.WebControls.TextBox txtSpecialInstruction;

		SessionDS m_sdsCustomerProfile = null;
		SessionDS m_sdsReference = null;
		SessionDS m_sdsStatusCode = null;
		SessionDS m_sdsExceptionCode = null;
		SessionDS m_sdsQuotation=null;
		SessionDS m_sdsVAS=null;		
		DataView m_dvApplyDimWtOptions = null;
		DataView m_dvPODSlipRequiredOptions = null;
		DataView m_dvInvoiceRequiredOptions = null;//HC Return Task
		DataView m_dvApplyESASurchargeOptions = null;
		DataView m_dvStatusOptions = null;
		DataView m_dvMBGOptions = null;
		DataView m_dvDim_By_TOTOptions = null;  //Jeab 8 Dec 2010
		DataView m_dvDensityFactorOptions = null;
		DataView m_dvCustomerTypeOptions = null;
		DataView m_dvCreditTerm  = null;
		DataSet dsCustZones = null;
		static private int m_iSetSize = 4;
		static private int m_iQuoteSize=7;
		static private int m_iVASSize=5;

		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			DbComboMasterAccount.RegistrationKey = System.Configuration.ConfigurationSettings.AppSettings["DbComboRegKey"];
			if(!Page.IsPostBack)
			{				
				EnableZoneUpload(false);
				Utility.EnableButton(ref btnSave, ButtonType.Save, true, m_moduleAccessRights);
				Utility.EnableButton(ref btnDelete, ButtonType.Delete, true, m_moduleAccessRights);
				ViewState["CPOperation"] = Operation.None;
				ViewState["SCOperation"] = Operation.None;
				ViewState["EXMode"] = ScreenMode.None;
				ViewState["EXOperation"] = Operation.None;
				ViewState["VASMode"] = ScreenMode.None;
				ViewState["VASOperation"] = Operation.None;
				ViewState["currentQuoteSet"] = 0;	
				m_sdsQuotation = SysDataMgrDAL.GetEmptyQuotation(1);								
				BindQuotation();
				Session["SESSION_DS5"] = m_sdsQuotation;				
				ViewState["sZipcode"] = "";
				ViewState["sZone"]= "";
				ViewState["sEffectiveDate"]= "";
				DataSet profileDS = SysDataManager1.GetEnterpriseProfile(utility.GetAppID(),utility.GetEnterpriseID());

				//Call function for rounding in TIESUtility  (by Sittichai 08/01/2008)
				if(profileDS.Tables[0].Rows.Count > 0)
				{
					int currency_decimal = (int) profileDS.Tables[0].Rows[0]["currency_decimal"];
					ViewState["m_format"] = "{0:F" + currency_decimal.ToString() + "}";
					Session["m_format"] = "{0:F" + currency_decimal.ToString() + "}";

					if  ( profileDS.Tables[0].Rows[0]["wt_rounding_method"] != System.DBNull.Value)
					{
						
						ViewState["wt_rounding_method"] = Convert.ToInt32(profileDS.Tables[0].Rows[0]["wt_rounding_method"]);
					}

					if  ( profileDS.Tables[0].Rows[0]["wt_increment_amt"] != System.DBNull.Value)
					{
						ViewState["wt_increment_amt"] = Convert.ToDecimal(profileDS.Tables[0].Rows[0]["wt_increment_amt"]);
					}
				}	
				ResetScreenForQuery();
				this.btnQry_Click(this,System.EventArgs.Empty);
				EnterpriseConfigurations();
			}
			else
			{
				if(Session["SESSION_DS1"] != null)
				{
					m_sdsCustomerProfile = (SessionDS)Session["SESSION_DS1"];
				}

				if(Session["SESSION_DS2"] != null)
				{
					m_sdsReference = (SessionDS)Session["SESSION_DS2"];
				}

				if(Session["SESSION_DS3"] != null)
				{
					m_sdsStatusCode = (SessionDS)Session["SESSION_DS3"];
				}

				if(Session["SESSION_DS4"] != null)
				{
					m_sdsExceptionCode = (SessionDS)Session["SESSION_DS4"];
				}

				if(Session["SESSION_DS5"] != null)
				{
					m_sdsQuotation = (SessionDS)Session["SESSION_DS5"];
				}
				if(Session["SESSION_DSVASCUST"] != null)
				{
					m_sdsVAS = (SessionDS)Session["SESSION_DSVASCUST"];
				}
			}
			lblNumRefRec = (Label)customerProfileMultiPage.FindControl("lblNumRefRec");
			dgStatusCodes = (DataGrid)customerProfileMultiPage.FindControl("dgStatusCodes");
			dgExceptionCodes = (DataGrid)customerProfileMultiPage.FindControl("dgExceptionCodes");
			dgCustZones = (DataGrid)customerProfileMultiPage.FindControl("dgCustZones");
			dgVAS = (DataGrid)customerProfileMultiPage.FindControl("dgVAS");
			btnSCInsert = (Button)customerProfileMultiPage.FindControl("btnSCInsert");
			btnInsertVAS = (Button)customerProfileMultiPage.FindControl("btnInsertVAS");
			btnViewAllVAS = (Button)customerProfileMultiPage.FindControl("btnViewAllVAS");
			custValidationSummary.HeaderText = Utility.GetLanguageText(ResourceType.UserMessage, "PLS_ENTER_MISS_FIELD", utility.GetUserCulture());			
		}


		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			base.OnInit(e);
			InitializeComponent();
			addCustomerButtonEventHandler();
			addChangeEventHandlers();
			addZipCodePopupButtonEventHandler();
			addRefChangeEventHandlers();
			addRefButtonEventHandlers();
			addCustFRButtonEventHandlers();
			addbtnChangeStatusEventHandler();

		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.TabStrip1.SelectedIndexChange += new System.EventHandler(this.TabStrip1_SelectedIndexChange);
			this.btnQry.Click += new System.EventHandler(this.btnQry_Click);
			this.btnExecQry.Click += new System.EventHandler(this.btnExecQry_Click);
			this.btnInsert.Click += new System.EventHandler(this.btnInsert_Click);
			this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
			this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
			this.btnGoToFirstPage.Click += new System.EventHandler(this.btnGoToFirstPage_Click);
			this.btnPreviousPage.Click += new System.EventHandler(this.btnPreviousPage_Click);
			this.btnNextPage.Click += new System.EventHandler(this.btnNextPage_Click);
			this.btnGoToLastPage.Click += new System.EventHandler(this.btnGoToLastPage_Click);
			this.txtCountRec.TextChanged += new System.EventHandler(this.txtCountRec_TextChanged);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
		
		public bool ckEffectiveDate(DateTime effDate)
		{
			DateTime effStart = System.DateTime.Now;
			ViewState["effStart"] = effStart;
			DateTime effEnd = System.DateTime.Now;
			bool ckDate = false;

			DataSet effDS = new DataSet();
			effDS = SysDataMgrDAL.GetEndRoundEffectiveDate(utility.GetAppID(),utility.GetEnterpriseID(),effStart.ToString("dd/MM/yyyy"));
			DataTable effDT = new DataTable();
			effDT = effDS.Tables[0];
			foreach(DataRow dr in effDT.Rows)
			{
				effEnd = Convert.ToDateTime(dr["endEffDate"].ToString());
				ViewState["effEnd"] = effEnd;
			}

			string getdate = "";
			if(effDate.Day.ToString().Length == 1)
				getdate = "0"+effDate.Day.ToString();
			else
				getdate = effDate.Day.ToString();

			if(effDate.Month.ToString().Length == 1)
				getdate += "/0"+effDate.Month.ToString()+"/"+effDate.Year.ToString();
			else
				getdate += "/"+effDate.Month.ToString()+"/"+effDate.Year.ToString();

			DateTime dt = DateTime.ParseExact(getdate,"dd/MM/yyyy",null);

			if(dt >= effStart && dt <= effEnd)
			{
				ckDate = true;
			}
			else
			{
				ckDate = false;
			}

			return ckDate;
		}

		protected void EnableACCTMGR1(bool b)
		{
			//---------------- ReferencePage --------------------------------------------------
			Button btnRefInsert = (Button)customerProfileMultiPage.FindControl("btnRefInsert");
			if(btnRefInsert != null)
			{
				btnRefInsert.Enabled=b;
			}
			Button btnRefSave = (Button)customerProfileMultiPage.FindControl("btnRefSave");
			if(btnRefSave != null)
			{
				btnRefSave.Enabled=b;
			}
			Button btnRefDelete = (Button)customerProfileMultiPage.FindControl("btnRefDelete");
			if(btnRefDelete != null)
			{
				btnRefDelete.Enabled=b;
			}			

			//---------------- StatusPage --------------------------------------------------
			btnSCInsert.Enabled=b;		
			dgStatusCodes.Columns[1].Visible=b;
			dgStatusCodes.Columns[2].Visible=b;

			//---------------- Zones --------------------------------------------------
//			this.btnQueryFR.Visible=b;
//			this.btnExecuteQueryFR.Visible=b;
			this.btnInsertFR.Enabled=b;
			this.btnImport_Show.Enabled=b;
			dgCustZones.Columns[1].Visible = b;  
			dgCustZones.Columns[2].Visible = b; 

			//---------------- VAS --------------------------------------------------
			btnInsertVAS.Enabled=b;
			dgVAS.Columns[0].Visible=b;
			dgVAS.Columns[1].Visible=b;
		}

		protected void EnableCSM(bool b)
		{
			Label lblRemark = (Label)customerProfileMultiPage.FindControl("lblRemark");
			Label lblSpecialInstruction = (Label)customerProfileMultiPage.FindControl("lblSpecialInstruction");
			
				
			txtRemark.Enabled = b;
			txtSpecialInstruction.Enabled = b;

			lblRemark.Visible=b;
			lblSpecialInstruction.Visible=b;
			txtRemark.Visible=b;
			txtSpecialInstruction.Visible=b;
		}
		protected void EnableBillPlacement(bool b)
		{
			txtNextBillPlacementDate.Enabled = b;
			txtRemarkBp.Enabled = b;
			btnSetBillPlacementRules.Enabled = b;
		}

		protected void EnablePayment(bool b)
		{
			RadioButton radioBtnCash = (RadioButton)customerProfileMultiPage.FindControl("radioBtnCash");
			RadioButton radioBtnCredit = (RadioButton)customerProfileMultiPage.FindControl("radioBtnCredit");
			msTextBox txtCreditOutstanding = (msTextBox)customerProfileMultiPage.FindControl("txtCreditOutstanding");
			msTextBox txtCreditAvailable = (msTextBox)customerProfileMultiPage.FindControl("txtCreditAvailable");
			msTextBox txtCreditLimit = (msTextBox)customerProfileMultiPage.FindControl("txtCreditLimit");
			msTextBox txtCreditUsed = (msTextBox)customerProfileMultiPage.FindControl("txtCreditUsed");
			msTextBox txtCreditTerm = (msTextBox)customerProfileMultiPage.FindControl("txtCreditTerm");
			msTextBox txtNextBillPlacementDate = (msTextBox)customerProfileMultiPage.FindControl("txtNextBillPlacementDate");
			msTextBox txtCreditStatus = (msTextBox)customerProfileMultiPage.FindControl("txtCreditStatus");
			msTextBox txtCreditThredholds = (msTextBox)customerProfileMultiPage.FindControl("txtCreditThredholds");
			msTextBox txtActiveQuatationNo = (msTextBox)customerProfileMultiPage.FindControl("txtActiveQuatationNo");
			DropDownList ddlMBG = (DropDownList)customerProfileMultiPage.FindControl("ddlMBG");
			CheckBox  chkBox = (CheckBox)customerProfileMultiPage.FindControl("chkBox");
			TextBox txtMinimumBox = (TextBox)customerProfileMultiPage.FindControl("txtMinimumBox");
			msTextBox txtRemarkBp = (msTextBox)customerProfileMultiPage.FindControl("txtRemarkBp");

			System.Web.UI.HtmlControls.HtmlTableRow tbTrPaymentInfo = (System.Web.UI.HtmlControls.HtmlTableRow)customerProfileMultiPage.FindControl("tbTrPaymentInfo");
		
			radioBtnCash.Enabled=b;
			radioBtnCredit.Enabled=b;
			txtCreditOutstanding.Enabled=b;
			txtCreditAvailable.Enabled=b;
			txtCreditLimit.Enabled=b;
			txtCreditUsed.Enabled=b;
			txtCreditTerm.Enabled=b;
			txtNextBillPlacementDate.Enabled=b;
			txtCreditStatus.Enabled=b;
			txtCreditThredholds.Enabled=b;
			txtActiveQuatationNo.Enabled=b;
			ddlMBG.Enabled=b;
			chkBox.Enabled=b;
			txtMinimumBox.Enabled=b;
			txtRemarkBp.Enabled=b;

			tbTrPaymentInfo.Visible=b;
		}

		protected void EnablePaymentButton(bool b)
		{
			btnChangeStatus.Enabled=b;
			btnSetBillPlacementRules.Enabled=b;
			btnChangeStatus.Visible=b;
			btnSetBillPlacementRules.Visible=b;
		}

		protected void CheckRole()
		{
			//ACCTMGR1 
			btnInsert.Enabled=false;
			btnSave.Enabled=false;
			btnDelete.Enabled=false;			
			EnableACCTMGR1(false);

			//ACCTMGR2 
			EnableCSM(false);
			//ACCTMGR3 
			EnablePayment(false);
			EnableBillPlacement(false);
			//ACCTMGR4 
			EnablePaymentButton(false);

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(utility.GetAppID(),utility.GetEnterpriseID());			
			string strQry = null;
			strQry =  "SELECT  DISTINCT  User_Role_Relation.roleid,Role_Name " ;
			strQry += " FROM User_Role_Relation INNER JOIN ";
			strQry += " Role_Master ON User_Role_Relation.applicationid = Role_Master.applicationid AND User_Role_Relation.enterpriseid = Role_Master.enterpriseid AND ";
			strQry += " User_Role_Relation.roleid = Role_Master.roleid ";
			strQry += " WHERE User_Role_Relation.applicationid = '" + utility.GetAppID() + "' and User_Role_Relation.enterpriseid = '" + utility.GetEnterpriseID() + "' and userid = '"+ utility.GetUserID() + "'";
			IDbCommand dbCmd = dbCon.CreateCommand(strQry,CommandType.Text);
			DataSet roleDataSet =(DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
		 
			for(int i = 0; i < roleDataSet.Tables[0].Rows.Count;i++)
			{
				string strRoleName =(string)roleDataSet.Tables[0].Rows[i]["Role_Name"];
				switch(strRoleName)
				{
					case "ACCTMGR1":
						btnInsert.Enabled=true;
						btnSave.Enabled=true;
						btnDelete.Enabled=true;
						EnableNew(true);
						EnableACCTMGR1(true);

						break;
					case "ACCTMGR2":
						EnableCSM(true);
						break;
					case "ACCTMGR3":
						EnablePayment(true);
						EnableBillPlacement(true);
						break;
					case "ACCTMGR4":
						EnablePaymentButton(true);
						break;
					default:
						break;
				}
			}	
			//txtRemarkBp.Enabled = false;
			txtNextBillPlacementDate.Enabled=false;
		}

		//Jeab 09 Mar 2012
		protected bool CheckCommissionDTChanged(DataSet dsCustomer)
		{

			bool iChange = false;

			DbConnection dbCon = null;

			IDbCommand dbCmd = null;

			String strQry = null;

			DataRow drEach = dsCustomer.Tables[0].Rows[0];

			DateTime dtCommission = (DateTime)drEach["Commission_datetime"];

			String strCustid = (String)drEach["custid"];


			dbCon = DbConnectionManager.GetInstance().GetDbConnection(utility.GetAppID(),utility.GetEnterpriseID());

			strQry = "select Commission_Datetime  from v_Customer_Category  where  custid = '"+ strCustid + "'";

			dbCmd = dbCon.CreateCommand(strQry,CommandType.Text);

			DataSet commissionDataSet =(DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);

 			
			DataRow drEach2 = commissionDataSet.Tables[0].Rows[0];

			DateTime idtCommission = (DateTime)drEach2["Commission_datetime"];

 
			if(dtCommission != idtCommission) 
			{
				iChange = true;
			}
			else 

				iChange = false;
			
			return iChange;

		}
		//Jeab 09 Mar 2012  =========>  End

		protected void EnableNew(bool b)
		{
			txtCustomerName.Enabled = b;

			txtAddress1.Enabled = b;

			txtTelephone.Enabled = b;

			txtAddress2.Enabled = b;

			txtZipCode.Enabled = b;

			btnZipcodePopup.Enabled = b;
		}
		protected void Enable(bool b)

		{

			btnInsert.Enabled= b;
			btnSave.Enabled= b;

			// Customer Page

			txtRefNo.Enabled = b;

			ddlStatus.Enabled = b;

			txtCustomerName.Enabled = b;

			txtContactPerson.Enabled = b;

			txtAddress1.Enabled = b;

			txtTelephone.Enabled = b;

			txtAddress2.Enabled = b;

			txtFax.Enabled = b;

			txtZipCode.Enabled = b;

			btnZipcodePopup.Enabled = b;

			btnSetBillPlacementRules.Enabled = b;

			btnChangeStatus.Enabled=b;

			txtState.Enabled = b;

			txtEmail.Enabled = b;

			txtCountry.Enabled = b;

			txtCreatedBy.Enabled = b;

			ddlCreditTerm.Enabled = b;

			// Comment by tu 8/03/2010
			//txtCreditLimit.Enabled = b;

			txtCreditOutstanding.Enabled = b;

			// Comment by tu 8/03/2010
			//radioBtnCash.Enabled = b;
			// Comment by tu 8/03/2010
			//radioBtnCredit.Enabled = b;

			ddlMBG.Enabled = b;

			ddlDimByTOT.Enabled=b;  // Jeab 8 Dec 10

			txtActiveQuatationNo.Enabled = b;

			chkBox.Enabled = b;

			// Special Information

			txtISectorCode.Enabled = b;

			btnISectorSearch.Enabled = b;

			txtSalesmanID.Enabled = b;

			btnSearchSalesman.Enabled = b;

			txtPromisedTotWt.Enabled = b;

			txtInsurancePercentSurcharge.Enabled = b;

			txtPromisedPeriod.Enabled = b;

			txtFreeInsuranceAmt.Enabled = b;

			txtPromisedTotPkg.Enabled = b;

			txtMaxAmt.Enabled = b;

			ddlApplyDimWt.Enabled = b;

			txtMinInsuranceSurcharge.Enabled = b;

			ddlApplyEsaSurcharge.Enabled = b;

			txtMaxInsuranceSurcharge.Enabled = b;

			ddlApplyEsaSurchargeDel.Enabled = b;

			ddlPodSlipRequired.Enabled = b;

			txtCODSurchargeAmt.Enabled = b;

			ddlInvoiceRequried.Enabled = b;

			txtCODSurchargePercent.Enabled = b;

			msDensityFactor.Enabled = b;

			txtMaxCODSurcharge.Enabled = b;

			ddlCustomerType.Enabled = b;

			txtMinCODSurcharge.Enabled = b;

			msDiscountBand.Enabled = b;

			msOtherSurchargeAmount.Enabled = b;

			msOtherSurchargePercentage.Enabled = b;

			msOtherSurchargeMinimum.Enabled = b;

			msOtherSurchargeMaximum.Enabled = b;

			msOtherSurchargeDescription.Enabled = b;

			txtRemark.Enabled = b;
			txtNextBillPlacementDate.Enabled = b;
			txtRemarkBp.Enabled = b;

			//Added by Tom 22/7/09
			txtSpecialInstruction.Enabled = b;
			//End Added By Tom 22/7/09

		}

		//End: PSA 2009-019 by Gwang; 05Mar09
		private int getCountofCreditused()
		{
	
			DbConnection dbCon = null;
			IDbCommand dbCmd = null;
			String strQry = "";

			msTextBox txtAccNo = (msTextBox)customerProfileMultiPage.FindControl("txtAccNo");

			strQry = " select count(custid) as ncust from customer_credit ";
			strQry +=  " where custid = '" + txtAccNo.Text +  "' ";
			strQry += " and applicationid = '" + utility.GetAppID()  +  "' ";
			strQry += " and enterpriseid = '" + utility.GetEnterpriseID() +  "' ";
			strQry += " and (isMasterAccount = 'N' or isMasterAccount is null) ";
			
			dbCon = DbConnectionManager.GetInstance().GetDbConnection(utility.GetAppID(),utility.GetEnterpriseID());
			dbCmd = dbCon.CreateCommand(strQry,CommandType.Text);
			DataSet  roleData = (DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);

			//cnt = roleData.Tables[0].Rows.Count;
			if(roleData.Tables[0].Rows.Count > 0)
			{
				return int.Parse(roleData.Tables[0].Rows[0]["ncust"].ToString());
			}
			else
			{
				return 0;
			}
		}

		private void addAgentButtonEventHandler()
		{
			Button btnSearchSalesman = (Button)customerProfileMultiPage.FindControl("btnSearchSalesman");
			if(btnSearchSalesman != null)
			{
				btnSearchSalesman.Click += new System.EventHandler(this.btnSearchSalesman_Click);
			}
		}

		private void addbtnChangeStatusEventHandler()
		{
			Button btnChangeStatus = (Button)customerProfileMultiPage.FindControl("btnChangeStatus");
			if(btnChangeStatus != null)
			{
				btnChangeStatus.Click += new System.EventHandler(this.btnChangeStatus_Click);
			}
		}
		//		public void btnSearchSalesman_Click(object sender, System.EventArgs e)
		//		{
		//			String sQrySalesmanId ="";
		//			TextBox txtSalesmanID = (TextBox)customerProfileMultiPage.FindControl("txtSalesmanID");
		//			if(txtSalesmanID != null && txtSalesmanID.Text != "")
		//			{
		//				sQrySalesmanId =txtSalesmanID.Text.Trim();
		//			}
		//			
		//			String sUrl = "SalesPersonPopup.aspx?FORMID=CustomerProfile&QUERY_SALESMANID="+sQrySalesmanId;
		//			ArrayList paramList = new ArrayList();
		//			paramList.Add(sUrl);
		//			String sScript = Utility.GetScript("openWindowParam.js",paramList);
		//			Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);	
		//		}

		private void addChangeEventHandlers()
		{


			msTextBox txtAccNo = (msTextBox)customerProfileMultiPage.FindControl("txtAccNo");

			if(txtAccNo != null)
			{
				txtAccNo.TextChanged += new System.EventHandler(this.txtAccNo_TextChanged);
			}

			DbCombo DbComboMasterAccount = (DbCombo)customerProfileMultiPage.FindControl("DbComboMasterAccount");
			if(DbComboMasterAccount!=null)
			{
				DbComboMasterAccount.SelectedItemChanged+= new System.EventHandler(this.DbComboMasterAccount_SelectedItemChanged);
			}

			msTextBox txtRefNo = (msTextBox)customerProfileMultiPage.FindControl("txtRefNo");

			if(txtRefNo != null)
			{
				txtRefNo.TextChanged += new System.EventHandler(this.txtRefNo_TextChanged);
			}

			TextBox txtCustomerName = (TextBox)customerProfileMultiPage.FindControl("txtCustomerName");

			if(txtCustomerName != null)
			{
				txtCustomerName.TextChanged += new System.EventHandler(this.txtCustomerName_TextChanged);
			}

			TextBox txtContactPerson = (TextBox)customerProfileMultiPage.FindControl("txtContactPerson");

			if(txtContactPerson != null)
			{
				txtContactPerson.TextChanged += new System.EventHandler(this.txtContactPerson_TextChanged);
			}

			TextBox txtAddress1 = (TextBox)customerProfileMultiPage.FindControl("txtAddress1");

			if(txtAddress1 != null)
			{
				txtAddress1.TextChanged += new System.EventHandler(this.txtAddress1_TextChanged);
			}

			TextBox txtAddress2 = (TextBox)customerProfileMultiPage.FindControl("txtAddress2");

			if(txtAddress2 != null)
			{
				txtAddress2.TextChanged += new System.EventHandler(this.txtAddress2_TextChanged);
			}

			TextBox txtCountry = (TextBox)customerProfileMultiPage.FindControl("txtCountry");

			if(txtCountry != null)
			{
				txtCountry.TextChanged += new System.EventHandler(this.txtCountry_TextChanged);
			}

			msTextBox txtZipCode = (msTextBox)customerProfileMultiPage.FindControl("txtZipCode");

			if(txtZipCode != null)
			{
				txtZipCode.TextChanged += new System.EventHandler(this.txtZipCode_TextChanged);
			}

			TextBox txtTelephone = (TextBox)customerProfileMultiPage.FindControl("txtTelephone");

			if(txtTelephone != null)
			{
				txtTelephone.TextChanged += new System.EventHandler(this.txtTelephone_TextChanged);
			}

			
			TextBox txtFax = (TextBox)customerProfileMultiPage.FindControl("txtFax");

			if(txtFax != null)
			{
				txtFax.TextChanged += new System.EventHandler(this.txtFax_TextChanged);
			}

			//			msTextBox txtCreditTerm = (msTextBox)customerProfileMultiPage.FindControl("txtCreditTerm");

			//			if(txtCreditTerm != null)
			//			{
			//				txtCreditTerm.TextChanged += new System.EventHandler(this.txtCreditTerm_TextChanged);
			//			}

			msTextBox txtISectorCode = (msTextBox)customerProfileMultiPage.FindControl("txtISectorCode");

			if(txtISectorCode != null)
			{
				txtISectorCode.TextChanged += new System.EventHandler(this.txtISectorCode_TextChanged);
			}

			msTextBox txtCreditLimit = (msTextBox)customerProfileMultiPage.FindControl("txtCreditLimit");

			if(txtCreditLimit != null)
			{
				txtCreditLimit.TextChanged += new System.EventHandler(this.txtCreditLimit_TextChanged);
			}

			msTextBox txtActiveQuatationNo = (msTextBox)customerProfileMultiPage.FindControl("txtActiveQuatationNo");

			if(txtActiveQuatationNo != null)
			{
				txtActiveQuatationNo.TextChanged += new System.EventHandler(this.txtActiveQuatationNo_TextChanged);
			}


			msTextBox txtCreditOutstanding = (msTextBox)customerProfileMultiPage.FindControl("txtCreditOutstanding");

			if(txtCreditOutstanding != null)
			{
				txtCreditOutstanding.TextChanged += new System.EventHandler(this.txtCreditOutstanding_TextChanged);
			}

			TextBox txtRemark = (TextBox)customerProfileMultiPage.FindControl("txtRemark");

			if(txtRemark != null)
			{
				txtRemark.TextChanged += new System.EventHandler(this.txtRemark_TextChanged);
			}

			TextBox txtSalesmanID = (TextBox)customerProfileMultiPage.FindControl("txtSalesmanID");

			if(txtSalesmanID != null)
			{
				txtSalesmanID.TextChanged += new System.EventHandler(this.txtSalesmanID_TextChanged);
			}

			msTextBox txtPromisedTotWt = (msTextBox)customerProfileMultiPage.FindControl("txtPromisedTotWt");


			if(txtPromisedTotWt != null)
			{
				txtPromisedTotWt.TextChanged += new System.EventHandler(this.txtPromisedTotWt_TextChanged);
			}

			RadioButton radioBtnCash = (RadioButton)customerProfileMultiPage.FindControl("radioBtnCash");

			if(radioBtnCash != null)
			{
				radioBtnCash.CheckedChanged += new System.EventHandler(this.radioBtnCash_CheckedChanged);
			}

			RadioButton radioBtnCredit = (RadioButton)customerProfileMultiPage.FindControl("radioBtnCredit");

			if(radioBtnCredit != null)
			{
				radioBtnCredit.CheckedChanged += new System.EventHandler(this.radioBtnCredit_CheckedChanged);
			}

			msTextBox txtPromisedTotPkg = (msTextBox)customerProfileMultiPage.FindControl("txtPromisedTotPkg");

			if(txtPromisedTotPkg != null)
			{
				txtPromisedTotPkg.TextChanged += new System.EventHandler(this.txtPromisedTotPkg_TextChanged);
			}

			msTextBox txtPromisedPeriod = (msTextBox)customerProfileMultiPage.FindControl("txtPromisedPeriod");


			if(txtPromisedPeriod != null)
			{
				txtPromisedPeriod.TextChanged += new System.EventHandler(this.txtPromisedPeriod_TextChanged);
			}
			
			msTextBox txtFreeInsuranceAmt = (msTextBox)customerProfileMultiPage.FindControl("txtFreeInsuranceAmt");

			if(txtFreeInsuranceAmt != null)
			{
				txtFreeInsuranceAmt.TextChanged += new System.EventHandler(this.txtFreeInsuranceAmt_TextChanged);
			}

			// for Invoice Return Days
			msTextBox txtInvoiceReturn = (msTextBox)customerProfileMultiPage.FindControl("txtInvoiceReturn");

			if(txtInvoiceReturn != null)
			{
				txtInvoiceReturn.TextChanged += new System.EventHandler(this.txtInvoiceReturn_TextChanged);
			}

			msTextBox txtInsurancePercentSurcharge = (msTextBox)customerProfileMultiPage.FindControl("txtInsurancePercentSurcharge");

			if(txtInsurancePercentSurcharge != null)
			{
				txtInsurancePercentSurcharge.TextChanged += new System.EventHandler(this.txtInsurancePercentSurcharge_TextChanged);
			}

			TextBox txtEmail = (TextBox)customerProfileMultiPage.FindControl("txtEmail");

			if(txtEmail != null)
			{
				txtEmail.TextChanged += new System.EventHandler(this.txtEmail_TextChanged);
			}

			TextBox txtCreatedBy = (TextBox)customerProfileMultiPage.FindControl("txtCreatedBy");

			if(txtCreatedBy != null)
			{
				txtCreatedBy.TextChanged += new System.EventHandler(this.txtCreatedBy_TextChanged);
			}

			DropDownList ddlApplyDimWt = (DropDownList)customerProfileMultiPage.FindControl("ddlApplyDimWt");
			
			if(ddlApplyDimWt != null)
			{
				ddlApplyDimWt.SelectedIndexChanged += new System.EventHandler(this.ddlApplyDimWt_SelectedIndexChanged);
			}

			DropDownList ddlPodSlipRequired = (DropDownList)customerProfileMultiPage.FindControl("ddlPodSlipRequired");
			
			if(ddlPodSlipRequired != null)
			{
				ddlPodSlipRequired.SelectedIndexChanged += new System.EventHandler(this.ddlPodSlipRequired_SelectedIndexChanged);
			}

			//HC Return Task
			DropDownList ddlInvoiceRequried = (DropDownList)customerProfileMultiPage.FindControl("ddlInvoiceRequried");
			
			if(ddlInvoiceRequried != null)
			{
				ddlInvoiceRequried.SelectedIndexChanged += new System.EventHandler(this.ddlInvoiceRequried_SelectedIndexChanged);
			}
			//HC Return Task

			DropDownList ddlApplyEsaSurcharge = (DropDownList)customerProfileMultiPage.FindControl("ddlApplyEsaSurcharge");
			
			if(ddlApplyEsaSurcharge != null)
			{
				ddlApplyEsaSurcharge.SelectedIndexChanged += new System.EventHandler(this.ddlApplyEsaSurcharge_SelectedIndexChanged);
			}

			//Add Event Of Dorpdownlist of ESA Surcharge for Delivery(ddlApplyEsaSurchargeDel)
			DropDownList ddlApplyEsaSurchargeDel = (DropDownList)customerProfileMultiPage.FindControl("ddlApplyEsaSurchargeDel");
			
			if(ddlApplyEsaSurchargeDel != null)
			{
				ddlApplyEsaSurchargeDel.SelectedIndexChanged += new System.EventHandler(this.ddlApplyEsaSurchargeDel_SelectedIndexChanged);
			}

			DropDownList ddlStatus = (DropDownList)customerProfileMultiPage.FindControl("ddlStatus");
			
			if(ddlStatus != null)
			{
				ddlStatus.SelectedIndexChanged += new System.EventHandler(this.ddlStatus_SelectedIndexChanged);
			}

			DropDownList ddlMBG = (DropDownList)customerProfileMultiPage.FindControl("ddlMBG");

			if(ddlMBG != null)
			{
				ddlMBG.SelectedIndexChanged += new System.EventHandler(this.ddlMBG_SelectedIndexChanged);
			}
			//Jeab 8 Dec 2010
			DropDownList ddlDimByTOT= (DropDownList)customerProfileMultiPage.FindControl("ddlDimByTOT");

			if(ddlDimByTOT != null)
			{
				ddlDimByTOT.SelectedIndexChanged += new System.EventHandler(this.ddlDimByTOT_SelectedIndexChanged);
			}
			//Jeab 8 Dec 2010  =========> End
			CheckBox  chkBox = (CheckBox)customerProfileMultiPage.FindControl("chkBox");

			if(chkBox != null)
			{
				this.chkBox.CheckedChanged  += new System.EventHandler(this.chkBox_CheckedChanged);
			}

			DropDownList ddlDensityFactor = (DropDownList)customerProfileMultiPage.FindControl("ddlDensityFactor");

			if(ddlDensityFactor != null)
			{
				ddlDensityFactor.SelectedIndexChanged += new System.EventHandler(this.ddlDensityFactor_SelectedIndexChanged);
			}


			DropDownList ddlCustomerType = (DropDownList)customerProfileMultiPage.FindControl("ddlCustomerType");

			if(ddlCustomerType != null)
			{
				ddlCustomerType.SelectedIndexChanged += new System.EventHandler(this.ddlCustomerType_SelectedIndexChanged);
			}

			msTextBox txtMaxAmt = (msTextBox)customerProfileMultiPage.FindControl("txtMaxAmt");

			if(txtMaxAmt != null)
			{
				txtMaxAmt.TextChanged += new System.EventHandler(this.txtMaxAmt_TextChanged);
			}
			
			msTextBox txtCODSurchargeAmt = (msTextBox)customerProfileMultiPage.FindControl("txtCODSurchargeAmt");

			if(txtCODSurchargeAmt != null)
			{
				txtCODSurchargeAmt.TextChanged += new System.EventHandler(this.txtCODSurchargeAmt_TextChanged);
			}

			msTextBox txtCODSurchargePercent = (msTextBox)customerProfileMultiPage.FindControl("txtCODSurchargePercent");

			if(txtCODSurchargePercent != null)
			{
				txtCODSurchargePercent.TextChanged += new System.EventHandler(this.txtCODSurchargePercent_TextChanged);
			}
			
			// by Ching Nov 29,2007
			msTextBox txtMaxCODSurcharge = (msTextBox)customerProfileMultiPage.FindControl("txtMaxCODSurcharge");

			if(txtMaxCODSurcharge != null)
			{
				txtMaxCODSurcharge.TextChanged += new System.EventHandler(this.txtMaxCODSurcharge_TextChanged);
			}

			msTextBox txtMinCODSurcharge = (msTextBox)customerProfileMultiPage.FindControl("txtMinCODSurcharge");

			if(txtMinCODSurcharge != null)
			{
				txtMinCODSurcharge.TextChanged += new System.EventHandler(this.txtMinCODSurcharge_TextChanged);
			}
			// End
			// by Aoo
			// By Aoo

			DropDownList  ddlCreditTerm = (DropDownList)customerProfileMultiPage.FindControl("ddlCreditTerm");
			if(ddlCreditTerm != null)
			{
				ddlCreditTerm.SelectedIndexChanged += new System.EventHandler(this.ddlCreditTerm_SelectedIndexChanged);
			}
			msTextBox msOtherSurchargeAmount = (msTextBox)customerProfileMultiPage.FindControl("msOtherSurchargeAmount");
			if(msOtherSurchargeAmount != null)
			{
				msOtherSurchargeAmount.TextChanged += new System.EventHandler(this.msOtherSurchargeAmount_TextChanged);
			}
			msTextBox msOtherSurchargePercentage = (msTextBox)customerProfileMultiPage.FindControl("msOtherSurchargePercentage");
			if(msOtherSurchargePercentage != null)
			{
				msOtherSurchargePercentage.TextChanged += new System.EventHandler(this.msOtherSurchargePercentage_TextChanged);
			}
			msTextBox msOtherSurchargeMinimum = (msTextBox)customerProfileMultiPage.FindControl("msOtherSurchargeMinimum");
			if(msOtherSurchargeMinimum != null)
			{
				msOtherSurchargeMinimum.TextChanged += new System.EventHandler(this.msOtherSurchargeMinimum_TextChanged); 
			}
			msTextBox msOtherSurchargeMaximum = (msTextBox)customerProfileMultiPage.FindControl("msOtherSurchargeMaximum");
			if(msOtherSurchargeMaximum != null)
			{
				msOtherSurchargeMaximum.TextChanged += new System.EventHandler(this.msOtherSurchargeMaximum_TextChanged);
			}
			msTextBox msOtherSurchargeDescription = (msTextBox)customerProfileMultiPage.FindControl("msOtherSurchargeDescription");
			if(msOtherSurchargeDescription != null)
			{
				msOtherSurchargeDescription.TextChanged += new System.EventHandler(this.msOtherSurchargeDescription_TextChanged);
			}

			msTextBox txtMaxInsuranceSurcharge = (msTextBox)customerProfileMultiPage.FindControl("txtMaxInsuranceSurcharge");
			if(txtMaxInsuranceSurcharge != null)
			{
				txtMaxInsuranceSurcharge.TextChanged += new System.EventHandler(this.txtMaxInsuranceSurcharge_TextChanged);
			}
			msTextBox txtMinInsuranceSurcharge = (msTextBox)customerProfileMultiPage.FindControl("txtMinInsuranceSurcharge");
			if(txtMinInsuranceSurcharge != null)
			{
				txtMinInsuranceSurcharge.TextChanged += new System.EventHandler(this.txtMinInsuranceSurcharge_TextChanged);
			}

			msTextBox msDiscountBand = (msTextBox)customerProfileMultiPage.FindControl("msDiscountBand");
			if(msDiscountBand != null)
			{
				msDiscountBand.TextChanged += new System.EventHandler(this.msDiscountBand_TextChanged);
			}
			msTextBox msDensityFactor = (msTextBox)customerProfileMultiPage.FindControl("msDensityFactor");

			if(msDensityFactor != null)
			{
				msDensityFactor.TextChanged += new System.EventHandler(this.msDensityFactor_TextChanged);
			}
			//end
			//Added By Tom 22/7/09
			TextBox txtSpecialInstruction = (TextBox)customerProfileMultiPage.FindControl("txtSpecialInstruction");

			if(txtSpecialInstruction != null)
			{
				txtSpecialInstruction.TextChanged += new System.EventHandler(this.txtSpecialInstruction_TextChanged);
			}
			//End Added By Tom 22/7/09
			TextBox txtNextBillPlacementDate = (TextBox)customerProfileMultiPage.FindControl("txtNextBillPlacementDate");
			if(txtNextBillPlacementDate != null)
			{
				txtNextBillPlacementDate.TextChanged += new System.EventHandler(this.txtNextBillPlacementDate_TextChanged);
			}
			TextBox txtMinimumBox = (TextBox)customerProfileMultiPage.FindControl("txtMinimumBox");
			if(txtMinimumBox != null)
			{
				txtMinimumBox.TextChanged += new System.EventHandler(this.txtMinimumBox_TextChanged);
			}

			//Jeab 12 Mar 2012
			TextBox txtFirstShipDate = (TextBox)customerProfileMultiPage.FindControl("txtFirstShipDate");
			if(txtFirstShipDate != null)
			{
				txtFirstShipDate.TextChanged += new System.EventHandler(this.txtFirstShipDate_TextChanged);
			}
			//Jeab 12 Mar 2012  =========>  End
		}
		private void addCustomerButtonEventHandler()
		{
			Button btnSearchSalesman = (Button)customerProfileMultiPage.FindControl("btnSearchSalesman");
			if(btnSearchSalesman != null)
			{
				btnSearchSalesman.Click += new System.EventHandler(this.btnSearchSalesman_Click);
			}

		}

		private void addZipCodePopupButtonEventHandler()
		{
			Button btnZipcodePopup = (Button)customerProfileMultiPage.FindControl("btnZipcodePopup");

			if(btnZipcodePopup != null)
			{
				btnZipcodePopup.Click += new System.EventHandler(this.btnZipcodePopup_Click);
			}

			Button btnSetBillPlacementRules = (Button)customerProfileMultiPage.FindControl("btnSetBillPlacementRules");
			if(btnSetBillPlacementRules != null)
			{
				btnSetBillPlacementRules.Click += new System.EventHandler(this.btnSetBillPlacementRules_Click);
			}

			Button btnRefZipcodeSearch = (Button)customerProfileMultiPage.FindControl("btnRefZipcodeSearch");

			if(btnRefZipcodeSearch != null)
			{
				btnRefZipcodeSearch.Click += new System.EventHandler(this.btnRefZipcodeSearch_Click);
			}

			Button btnISectorSearch = (Button)customerProfileMultiPage.FindControl("btnISectorSearch");

			if(btnISectorSearch != null)
			{
				btnISectorSearch.Click += new System.EventHandler(this.btnISectorSearch_Click);
			}

			
		}
		private void addRefChangeEventHandlers()
		{


			TextBox txtRefSndRecName = (TextBox)customerProfileMultiPage.FindControl("txtRefSndRecName");

			if(txtRefSndRecName != null)
			{
				txtRefSndRecName.TextChanged += new System.EventHandler(this.txtRefSndRecName_TextChanged);
			}

			TextBox txtRefContactPerson = (TextBox)customerProfileMultiPage.FindControl("txtRefContactPerson");

			if(txtRefContactPerson != null)
			{
				txtRefContactPerson.TextChanged += new System.EventHandler(this.txtRefContactPerson_TextChanged);
			}


			TextBox txtRefAddress1 = (TextBox)customerProfileMultiPage.FindControl("txtRefAddress1");

			if(txtRefAddress1 != null)
			{
				txtRefAddress1.TextChanged += new System.EventHandler(this.txtRefAddress1_TextChanged);
			}

			TextBox txtRefAddress2 = (TextBox)customerProfileMultiPage.FindControl("txtRefAddress2");

			if(txtRefAddress2 != null)
			{
				txtRefAddress2.TextChanged += new System.EventHandler(this.txtRefAddress2_TextChanged);
			}

			TextBox txtRefCountry = (TextBox)customerProfileMultiPage.FindControl("txtRefCountry");

			if(txtRefCountry != null)
			{
				txtRefCountry.TextChanged += new System.EventHandler(this.txtRefCountry_TextChanged);
			}

			msTextBox txtRefZipcode = (msTextBox)customerProfileMultiPage.FindControl("txtRefZipcode");

			if(txtRefZipcode != null)
			{
				txtRefZipcode.TextChanged += new System.EventHandler(this.txtRefZipcode_TextChanged);
			}

			TextBox txtRefTelephone = (TextBox)customerProfileMultiPage.FindControl("txtRefTelephone");

			if(txtRefTelephone != null)
			{
				txtRefTelephone.TextChanged += new System.EventHandler(this.txtRefTelephone_TextChanged);
			}


			TextBox txtRefFax = (TextBox)customerProfileMultiPage.FindControl("txtRefFax");

			if(txtRefFax != null)
			{
				txtRefFax.TextChanged += new System.EventHandler(this.txtRefFax_TextChanged);
			}


			CheckBox chkSender = (CheckBox)customerProfileMultiPage.FindControl("chkSender");

			if(chkSender != null)
			{
				chkSender.CheckedChanged += new System.EventHandler(this.chkSender_CheckedChanged);
			}

			CheckBox chkRecipient = (CheckBox)customerProfileMultiPage.FindControl("chkRecipient");

			if(chkRecipient != null)
			{
				chkRecipient.CheckedChanged += new System.EventHandler(this.chkRecipient_CheckedChanged);
			}


			TextBox txtRefEmail = (TextBox)customerProfileMultiPage.FindControl("txtRefEmail");

			if(txtRefEmail != null)
			{
				txtRefEmail.TextChanged += new System.EventHandler(this.txtRefEmail_TextChanged);
			}

			// By Aoo
			msTextBox msOtherSurchargeAmount = (msTextBox)customerProfileMultiPage.FindControl("msOtherSurchargeAmount");
			if(msOtherSurchargeAmount != null)
			{
				msOtherSurchargeAmount.TextChanged += new System.EventHandler(this.msOtherSurchargeAmount_TextChanged);
			}
			msTextBox msOtherSurchargePercentage = (msTextBox)customerProfileMultiPage.FindControl("msOtherSurchargePercentage");
			if(msOtherSurchargePercentage != null)
			{
				msOtherSurchargePercentage.TextChanged += new System.EventHandler(this.msOtherSurchargePercentage_TextChanged);
			}
			msTextBox msOtherSurchargeMinimum = (msTextBox)customerProfileMultiPage.FindControl("msOtherSurchargeMinimum");
			if(msOtherSurchargeMinimum != null)
			{
				msOtherSurchargeMinimum.TextChanged += new System.EventHandler(this.msOtherSurchargeMinimum_TextChanged); 
			}
			msTextBox msOtherSurchargeMaximum = (msTextBox)customerProfileMultiPage.FindControl("msOtherSurchargeMaximum");
			if(msOtherSurchargeMaximum != null)
			{
				msOtherSurchargeMaximum.TextChanged += new System.EventHandler(this.msOtherSurchargeMaximum_TextChanged);
			}
			msTextBox msOtherSurchargeDescription = (msTextBox)customerProfileMultiPage.FindControl("msOtherSurchargeDescription");
			if(msOtherSurchargeDescription != null)
			{
				msOtherSurchargeDescription.TextChanged += new System.EventHandler(this.msOtherSurchargeDescription_TextChanged);
			}

			msTextBox txtMaxInsuranceSurcharge = (msTextBox)customerProfileMultiPage.FindControl("txtMaxInsuranceSurcharge");
			if(txtMaxInsuranceSurcharge != null)
			{
				txtMaxInsuranceSurcharge.TextChanged += new System.EventHandler(this.txtMaxInsuranceSurcharge_TextChanged);
			}
			msTextBox txtMinInsuranceSurcharge = (msTextBox)customerProfileMultiPage.FindControl("txtMinInsuranceSurcharge");
			if(txtMinInsuranceSurcharge != null)
			{
				txtMinInsuranceSurcharge.TextChanged += new System.EventHandler(this.txtMinInsuranceSurcharge_TextChanged);
			}

			msTextBox msDiscountBand = (msTextBox)customerProfileMultiPage.FindControl("msDiscountBand");
			if(msDiscountBand != null)
			{
				msDiscountBand.TextChanged += new System.EventHandler(this.msDiscountBand_TextChanged);
			}
			msTextBox msDensityFactor = (msTextBox)customerProfileMultiPage.FindControl("msDensityFactor");

			if(msDensityFactor != null)
			{
				msDensityFactor.TextChanged += new System.EventHandler(this.msDensityFactor_TextChanged);
			}

		}

		private void addRefButtonEventHandlers()
		{
			Button btnRefViewAll = (Button)customerProfileMultiPage.FindControl("btnRefViewAll");

			if(btnRefViewAll != null)
			{
				btnRefViewAll.Click += new System.EventHandler(this.btnRefViewAll_Click);
			}

			Button btnRefInsert = (Button)customerProfileMultiPage.FindControl("btnRefInsert");

			if(btnRefInsert != null)
			{
				btnRefInsert.Click += new System.EventHandler(this.btnRefInsert_Click);
			}

			Button btnRefSave = (Button)customerProfileMultiPage.FindControl("btnRefSave");

			if(btnRefSave != null)
			{
				btnRefSave.Click += new System.EventHandler(this.btnRefSave_Click);
			}

			Button btnRefDelete = (Button)customerProfileMultiPage.FindControl("btnRefDelete");

			if(btnRefDelete != null)
			{
				btnRefDelete.Click += new System.EventHandler(this.btnRefDelete_Click);
			}
			
			Button btnFirstRef = (Button)customerProfileMultiPage.FindControl("btnFirstRef");

			if(btnFirstRef != null)
			{
				btnFirstRef.Click += new System.EventHandler(this.btnFirstRef_Click);
			}
			
			Button btnPreviousRef = (Button)customerProfileMultiPage.FindControl("btnPreviousRef");

			if(btnPreviousRef != null)
			{
				btnPreviousRef.Click += new System.EventHandler(this.btnPreviousRef_Click);
			}
			
			Button btnNextRef = (Button)customerProfileMultiPage.FindControl("btnNextRef");

			if(btnNextRef != null)
			{
				btnNextRef.Click += new System.EventHandler(this.btnNextRef_Click);
			}
			
			Button btnLastRef = (Button)customerProfileMultiPage.FindControl("btnLastRef");

			if(btnLastRef != null)
			{
				btnLastRef.Click += new System.EventHandler(this.btnLastRef_Click);
			}

			Button btnQuoteViewAll = (Button)customerProfileMultiPage.FindControl("btnQuoteViewAll");

			if(btnQuoteViewAll != null)
			{
				btnQuoteViewAll.Click += new System.EventHandler(this.btnQuoteViewAll_Click);
			}

			Button btnViewAllVAS = (Button)customerProfileMultiPage.FindControl("btnViewAllVAS");

			if(btnViewAllVAS != null)
			{
				btnViewAllVAS.Click += new System.EventHandler(this.btnViewAllVAS_Click);
			}

			Button btnInsertVAS = (Button)customerProfileMultiPage.FindControl("btnInsertVAS");

			if(btnInsertVAS != null)
			{
				btnInsertVAS.Click += new System.EventHandler(this.btnInsertVAS_Click);
			}

		}


		private void txtCustStateCode_TextChanged(object sender, System.EventArgs e)
		{
		
		}


		private void btnQry_Click(object sender, System.EventArgs e)
		{
			ResetScreenForQuery();
			EnterpriseConfigurations();
		}

		private void ResetScreenForQuery()
		{
			//BY X MAY 29 08
			msTextBox txtAccNo = (msTextBox)customerProfileMultiPage.FindControl("txtAccNo");
			msTextBox txtInsurancePercentSurcharge = (msTextBox)customerProfileMultiPage.FindControl("txtInsurancePercentSurcharge");
			msTextBox txtFreeInsuranceAmt = (msTextBox)customerProfileMultiPage.FindControl("txtFreeInsuranceAmt");
			msTextBox txtMaxAmt = (msTextBox)customerProfileMultiPage.FindControl("txtMaxAmt");
			msTextBox txtMinimumBox = (msTextBox)customerProfileMultiPage.FindControl("txtMinimumBox");
			Button btnSetBillPlacementRules = (Button)customerProfileMultiPage.FindControl("btnSetBillPlacementRules");

			txtInsurancePercentSurcharge.Text="";
			txtFreeInsuranceAmt.Text="";
			txtMaxAmt.Text="";
			//END BY X MAY 29 08

			m_sdsCustomerProfile = SysDataMgrDAL.GetEmptyCustomerProfileDS(1);
			Session["SESSION_DS1"] = m_sdsCustomerProfile;
			m_sdsReference = SysDataMgrDAL.GetEmptyReferenceDS(1);
			Session["SESSION_DS2"] = m_sdsReference;
			m_sdsStatusCode = SysDataMgrDAL.GetEmptyCPStatusCodeDS(1);
			Session["SESSION_DS3"] = m_sdsStatusCode;
			m_sdsExceptionCode = SysDataMgrDAL.GetEmptyExceptionCodeDS(1);
			Session["SESSION_DS4"] = m_sdsExceptionCode;
			m_sdsQuotation = SysDataMgrDAL.GetEmptyQuotation(1);
			Session["SESSION_DS5"] = m_sdsQuotation;
			//Added by GwanG on 19March08
			m_sdsVAS = SysDataMgrDAL.GetEmptyVASCustDS(0);
			Session["SESSION_DSVASCUST"] = m_sdsVAS ;

			//Reset the Created By field
			TextBox txtCreatedBy = (TextBox)customerProfileMultiPage.FindControl("txtCreatedBy");
			txtCreatedBy.Text="";

			ViewState["CPMode"] = ScreenMode.Query;
			ViewState["CPOperation"] = Operation.None;
			ViewState["IsTextChanged"] = false;
			ViewState["currentPage"] = 0;

			btnExecQry.Enabled = true;
			Utility.EnableButton(ref btnInsert,ButtonType.Insert,true,m_moduleAccessRights);
			btnSave.Enabled=true;
			//btnInsert.Enabled = true;
			LoadComboLists();
			EnableTabs(true,true,false,false,false,false,false);
			DisplayCurrentPage();
			lblNumRec.Text = "";
			lblNumRefRec.Text = "";
			lblCPErrorMessage.Text = "";
			lblSCMessage.Text="";
			lblEXMessage.Text="";
			TabStrip1.SelectedIndex = 0;
			btnGoToFirstPage.Enabled = false;
			btnGoToLastPage.Enabled = false;
			btnPreviousPage.Enabled = false;
			btnNextPage.Enabled = false;
			btnSetBillPlacementRules.Enabled = false;

			txtRemarkBp.Text = "";
			btnChangeStatus.Enabled=false;
			txtMinimumBox.Text = "";
		
			CheckRole();

			if(txtAccNo != null)
			{
				SetInitialFocus(txtAccNo);
			}	
		}


		private void btnExecQry_Click(object sender, System.EventArgs e)
		{
			msTextBox txtAccNo = (msTextBox)customerProfileMultiPage.FindControl("txtAccNo");
			FillCustomerProfileDataRow(0);
			ViewState["QUERY_DS"] = m_sdsCustomerProfile.ds;
			ViewState["CPMode"] = ScreenMode.ExecuteQuery;
			ViewState["CPOperation"] = Operation.None;
			ViewState["IsTextChanged"] = false;
			ViewState["currentPage"] = 0;
			ViewState["currentSet"] = 0;
			ViewState["Master_Account"] = null;	
			GetCPRecSet();
			foreach(DataRow dr in m_sdsCustomerProfile.ds.Tables[0].Rows)
			{
				if(dr["master_account"].GetType().Equals(System.Type.GetType("System.DBNull"))==false)
				{
					ViewState["Master_Account"] = dr["master_account"];
				}					
			}

			btnExecQry.Enabled = false;
			btnChangeStatus.Enabled=true;
			Utility.EnableButton(ref btnInsert,ButtonType.Insert,true,m_moduleAccessRights);
			if(m_sdsCustomerProfile.QueryResultMaxSize == 0)
			{
				lblCPErrorMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"NO_RECORDS_FOUND",utility.GetUserCulture());
				EnableNavigationButtons(false,false,false,false);
				CheckRole();
				return;
			}
			LoadComboLists();
			EnableTabs(true,true,true,true,true,true,true);
			DisplayCurrentPage();			
			btnExecQry.Enabled = false;
			Utility.EnableButton(ref btnInsert,ButtonType.Insert,true,m_moduleAccessRights);
			if(m_sdsCustomerProfile != null && m_sdsCustomerProfile.DataSetRecSize > 0)
			{
				EnableNavigationButtons(false,false,true,true);
			}
			lblCPErrorMessage.Text = "";
			DisplayRecNum();
			setQueryFR();
			EnterpriseConfigurations();

			CheckRole();
		}

		private void btnGoToFirstPage_Click(object sender, System.EventArgs e)
		{
			ViewState["currentSet"] = 0;	
			GetCPRecSet();
			ViewState["currentPage"] = 0;
			DisplayCurrentPage();
			DisplayRecNum();
			EnableNavigationButtons(false,false,true,true);
			dgQuotation.CurrentPageIndex=0; // to avoid Invalid Current Page Index
			ShowQuotationsForCurrentCustomer();
			BindQuotation();
			setQueryFR();
			BindVAS();
			btnViewAllVAS_Click(this, new System.EventArgs());

		}

		private void btnPreviousPage_Click(object sender, System.EventArgs e)
		{
			int iCurrentPage = Convert.ToInt32(ViewState["currentPage"]);

			if(Convert.ToInt32(ViewState["currentPage"])  > 0 )
			{
				ViewState["currentPage"] = Convert.ToInt32(ViewState["currentPage"]) - 1;
				DisplayCurrentPage();
				dgQuotation.CurrentPageIndex=0; // to avoid Invalid Current Page Index
				ShowQuotationsForCurrentCustomer();
				BindQuotation();
			}
			else
			{
				if( (Convert.ToInt32(ViewState["currentSet"]) - 1) < 0)
				{
					EnableNavigationButtons(false,false,true,true);
					return;
				}
				
				ViewState["currentSet"] = Convert.ToInt32(ViewState["currentSet"]) - 1;	
				GetCPRecSet();
				ViewState["currentPage"] = m_sdsCustomerProfile.DataSetRecSize - 1;
				DisplayCurrentPage();
				dgQuotation.CurrentPageIndex=0; // to avoid Invalid Current Page Index
				ShowQuotationsForCurrentCustomer();
				BindQuotation();
			}
			DisplayRecNum();
			EnableNavigationButtons(true,true,true,true);
				
			setQueryFR();
			btnViewAllVAS_Click(this, new System.EventArgs());

		}

		private void btnNextPage_Click(object sender, System.EventArgs e)
		{
			int iCurrentPage =Convert.ToInt32(ViewState["currentPage"]);

			if(Convert.ToInt32(ViewState["currentPage"])  < (m_sdsCustomerProfile.DataSetRecSize - 1) )
			{
				btnNextPage.Enabled = true;
				ViewState["currentPage"] = Convert.ToInt32(ViewState["currentPage"]) + 1;
				DisplayCurrentPage();
				dgQuotation.CurrentPageIndex=0; // to avoid Invalid Current Page Index
				ShowQuotationsForCurrentCustomer();
				BindQuotation();
			}
			else
			{
				decimal iTotalRec = (Convert.ToInt32(ViewState["currentSet"]) * m_iSetSize) + m_sdsCustomerProfile.DataSetRecSize;
				if( iTotalRec ==  m_sdsCustomerProfile.QueryResultMaxSize)
				{
					EnableNavigationButtons(true,true,false,false);
					return;
				}
				
				ViewState["currentSet"] = Convert.ToInt16(ViewState["currentSet"]) + 1;	
				GetCPRecSet();
				ViewState["currentPage"] = 0;
				DisplayCurrentPage();
				dgQuotation.CurrentPageIndex=0; // to avoid Invalid Current Page Index
				ShowQuotationsForCurrentCustomer();
				BindQuotation();
						
			}
			DisplayRecNum();
			EnableNavigationButtons(true,true,true,true);
			btnViewAllVAS_Click(this, new System.EventArgs());

	
			setQueryFR();
		}

		private void btnGoToLastPage_Click(object sender, System.EventArgs e)
		{
			ViewState["currentSet"] = (m_sdsCustomerProfile.QueryResultMaxSize - 1)/m_iSetSize;	
			GetCPRecSet();
			ViewState["currentPage"] = m_sdsCustomerProfile.DataSetRecSize - 1;
			DisplayCurrentPage();	
			DisplayRecNum();
			EnableNavigationButtons(true,true,false,false);
			dgQuotation.CurrentPageIndex=0; // to avoid Invalid Current Page Index
			ShowQuotationsForCurrentCustomer();
			BindQuotation();

			setQueryFR();
			btnViewAllVAS_Click(this, new System.EventArgs());

		}

		private void EnableNavigationButtons(bool bMoveFirst, bool bMoveNext,bool bEnableMovePrevious, bool bMoveLast)
		{
			btnGoToFirstPage.Enabled	= bMoveFirst;
			btnPreviousPage.Enabled		= bMoveNext;
			btnNextPage.Enabled			= bEnableMovePrevious;
			btnGoToLastPage.Enabled		= bMoveLast;
		}

		private void GetCPRecSet()
		{
			//int iStartIndex = Convert.ToInt32(ViewState["currentSet"]) * m_iSetSize;
			String tmpStr = ViewState["currentSet"].ToString().Trim();
			int iStartIndex = 0;
			int intIndexOf = tmpStr.IndexOf(".");
	
			if (intIndexOf > 0)
			{
				iStartIndex = Convert.ToInt32(tmpStr.Substring(0, intIndexOf)) * m_iSetSize;
			}
			else
			{
				iStartIndex = Convert.ToInt32(tmpStr) * m_iSetSize;
			}
			DataSet dsQuery = (DataSet)ViewState["QUERY_DS"];
			m_sdsCustomerProfile = SysDataMgrDAL.GetCustomerProfileDS(utility.GetAppID(),utility.GetEnterpriseID(),iStartIndex,m_iSetSize,dsQuery);
			Session["SESSION_DS1"] = m_sdsCustomerProfile;
			decimal pgCnt = (m_sdsCustomerProfile.QueryResultMaxSize - 1)/m_iSetSize;
			if(pgCnt < Convert.ToInt32(ViewState["currentSet"]))
			{
				ViewState["currentSet"] = System.Convert.ToInt32(pgCnt);
			}
		}

		private void DisplayCurrentPage()
		{

			m_sdsCustomerProfile = (SessionDS)Session["SESSION_DS1"];
			DataRow drCurrent =null;
			if(m_sdsCustomerProfile!=null)
				drCurrent = m_sdsCustomerProfile.ds.Tables[0].Rows[Convert.ToInt32(ViewState["currentPage"])];
			//else
			//	drCurrent = SysDataMgrDAL.GetEmptyCustomerProfileDS(1).ds.Tables[0].Rows[iCurrentRow];


			//DataRow drCurrent = m_sdsCustomerProfile.ds.Tables[0].Rows[Convert.ToInt32(ViewState["currentPage"])];
			RadioButton radioBtnCash;
			msTextBox txtAccNo = (msTextBox)customerProfileMultiPage.FindControl("txtAccNo");
			TextBox txtCustAccDispTab2 = (TextBox)customerProfileMultiPage.FindControl("txtCustAccDispTab2");
			TextBox txtCustAccDispTab3 = (TextBox)customerProfileMultiPage.FindControl("txtCustAccDispTab3");
			TextBox txtCustAccDispTab5 = (TextBox)customerProfileMultiPage.FindControl("txtCustAccDispTab5");
			TextBox txtCustAccDispTab6 = (TextBox)customerProfileMultiPage.FindControl("txtCustAccDispTab6");
			TextBox txtCustAccDispTab7 = (TextBox)customerProfileMultiPage.FindControl("txtCustAccDispTab7");
			msTextBox txtNextBillPlacementDate = (msTextBox)customerProfileMultiPage.FindControl("txtNextBillPlacementDate");
			msTextBox txtRemarkBp = (msTextBox)customerProfileMultiPage.FindControl("txtRemarkBp");
			
			if(txtAccNo != null)
			{
				if(drCurrent["custid"]!= null && (!drCurrent["custid"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					txtAccNo.Text = drCurrent["custid"].ToString();
					ViewState["currentCustomer"] = txtAccNo.Text; 
				}
				else
				{
					txtAccNo.Text = "";
				}
				EnableTxtAccNo();
			}

			if(txtCustAccDispTab2 != null)
			{
				if(drCurrent["custid"]!= null && (!drCurrent["custid"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					txtCustAccDispTab2.Text = drCurrent["custid"].ToString();
				}
				else
				{
					txtCustAccDispTab2.Text = "";
				}
			}

			if(txtCustAccDispTab3 != null)
			{
				if(drCurrent["custid"]!= null && (!drCurrent["custid"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					txtCustAccDispTab3.Text = drCurrent["custid"].ToString();
				}
				else
				{
					txtCustAccDispTab3.Text = "";
				}
			}

			if(txtCustAccDispTab5 != null)
			{
				if(drCurrent["custid"]!= null && (!drCurrent["custid"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					txtCustAccDispTab5.Text = drCurrent["custid"].ToString();
				}
				else
				{
					txtCustAccDispTab5.Text = "";
				}
			}

			if(txtCustAccDispTab6 != null)
			{
				if(drCurrent["custid"]!= null && (!drCurrent["custid"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					txtCustAccDispTab6.Text = drCurrent["custid"].ToString();
				}
				else
				{
					txtCustAccDispTab6.Text = "";
				}
			}

			if(txtCustAccDispTab7 != null)
			{
				if(drCurrent["custid"]!= null && (!drCurrent["custid"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					txtCustAccDispTab7.Text = drCurrent["custid"].ToString();
				}
				else
				{
					txtCustAccDispTab7.Text = "";
				}
			}


			msTextBox txtRefNo = (msTextBox)customerProfileMultiPage.FindControl("txtRefNo");

			if(txtRefNo != null)
			{
				if(drCurrent["ref_code"]!= null && (!drCurrent["ref_code"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					txtRefNo.Text = drCurrent["ref_code"].ToString() ;
				}
				else
				{
					txtRefNo.Text = "";
				}

			}


			TextBox txtCustomerName = (TextBox)customerProfileMultiPage.FindControl("txtCustomerName");
			TextBox txtCustNameDispTab2 = (TextBox)customerProfileMultiPage.FindControl("txtCustNameDispTab2");
			TextBox txtCustNameDispTab3 = (TextBox)customerProfileMultiPage.FindControl("txtCustNameDispTab3");
			TextBox txtCustNameDispTab5 = (TextBox)customerProfileMultiPage.FindControl("txtCustNameDispTab5");
			TextBox txtCustNameDispTab6 = (TextBox)customerProfileMultiPage.FindControl("txtCustNameDispTab6");
			TextBox txtCustNameDispTab7 = (TextBox)customerProfileMultiPage.FindControl("txtCustNameDispTab7");

			if(txtCustomerName != null)
			{
				if(drCurrent["cust_name"]!= null && (!drCurrent["cust_name"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					txtCustomerName.Text = drCurrent["cust_name"].ToString();
				}
				else
				{
					txtCustomerName.Text = "";
				}
			}

			if(txtCustNameDispTab2 != null)
			{
				if(drCurrent["cust_name"]!= null && (!drCurrent["cust_name"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					txtCustNameDispTab2.Text =  drCurrent["cust_name"].ToString();
				}
				else
				{
					txtCustNameDispTab2.Text = "";
				}
			}

			if(txtCustNameDispTab3 != null)
			{
				if(drCurrent["cust_name"]!= null && (!drCurrent["cust_name"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					txtCustNameDispTab3.Text = drCurrent["cust_name"].ToString();
				}
				else
				{
					txtCustNameDispTab3.Text = "";
				}
			}

			if(txtCustNameDispTab5 != null)
			{
				if(drCurrent["cust_name"]!= null && (!drCurrent["cust_name"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					txtCustNameDispTab5.Text = drCurrent["cust_name"].ToString();
				}
				else
				{
					txtCustNameDispTab5.Text = "";
				}
			}

			if(txtCustNameDispTab6 != null)
			{
				if(drCurrent["cust_name"]!= null && (!drCurrent["cust_name"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					txtCustNameDispTab6.Text = drCurrent["cust_name"].ToString();
				}
				else
				{
					txtCustNameDispTab6.Text = "";
				}
			}

			if(txtCustNameDispTab7 != null)
			{
				if(drCurrent["cust_name"]!= null && (!drCurrent["cust_name"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					txtCustNameDispTab7.Text = drCurrent["cust_name"].ToString();
				}
				else
				{
					txtCustNameDispTab7.Text = "";
				}
			}

			

			TextBox txtContactPerson = (TextBox)customerProfileMultiPage.FindControl("txtContactPerson");

			if(txtContactPerson != null)
			{
				if(drCurrent["contact_person"]!= null && (!drCurrent["contact_person"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					txtContactPerson.Text = drCurrent["contact_person"].ToString();
				}
				else
				{
					txtContactPerson.Text = "";
				}

			}
			

			TextBox txtAddress1 = (TextBox)customerProfileMultiPage.FindControl("txtAddress1");

			if(txtAddress1 != null)
			{
				if(drCurrent["address1"]!= null && (!drCurrent["address1"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					txtAddress1.Text = drCurrent["address1"].ToString();
				}
				else
				{
					txtAddress1.Text = "";
				}
			}
			

			TextBox txtAddress2 = (TextBox)customerProfileMultiPage.FindControl("txtAddress2");

			if(txtAddress2 != null)
			{
				if(drCurrent["address2"]!= null && (!drCurrent["address2"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					txtAddress2.Text = drCurrent["address2"].ToString();
				}
				else
				{
					txtAddress2.Text = "";
				}
			}
			

			TextBox txtCountry = (TextBox)customerProfileMultiPage.FindControl("txtCountry");

			if(txtCountry != null)
			{
				if(drCurrent["country"]!= null && (!drCurrent["country"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					txtCountry.Text = drCurrent["country"].ToString();
				}
				else
				{
					txtCountry.Text = "";
				}
			}
			

			msTextBox txtZipCode = (msTextBox)customerProfileMultiPage.FindControl("txtZipCode");
			TextBox txtState = (TextBox)customerProfileMultiPage.FindControl("txtState");


			if(txtZipCode != null && txtState != null)
			{
				if(drCurrent["zipcode"]!= null && (!drCurrent["zipcode"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					txtZipCode.Text = drCurrent["zipcode"].ToString();
					Zipcode zipcode = new Zipcode();
					zipcode.Populate(utility.GetAppID(),utility.GetEnterpriseID(),txtZipCode.Text);
					zipcode.Populate(utility.GetAppID(),utility.GetEnterpriseID(),zipcode.Country,txtZipCode.Text);
					txtState.Text = zipcode.StateName;
				}
				else
				{
					txtZipCode.Text = "";
					txtState.Text = "";
				}
			}
			

			TextBox txtTelephone = (TextBox)customerProfileMultiPage.FindControl("txtTelephone");

			if(txtTelephone != null)
			{
				if(drCurrent["telephone"]!= null && (!drCurrent["telephone"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					txtTelephone.Text = drCurrent["telephone"].ToString();
				}
				else
				{
					txtTelephone.Text = "";
				}
			}
			
			TextBox txtFax = (TextBox)customerProfileMultiPage.FindControl("txtFax");

			if(txtFax != null)
			{
				if(drCurrent["fax"]!= null && (!drCurrent["fax"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					txtFax.Text = drCurrent["fax"].ToString();
				}
				else
				{
					txtFax.Text = "";
				}
			}
			
			//			msTextBox txtCreditTerm = (msTextBox)customerProfileMultiPage.FindControl("txtCreditTerm");
			//
			//			if(txtCreditTerm != null)
			//			{
			//				if(drCurrent["credit_term"]!= null && (!drCurrent["credit_term"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			//				{
			//					txtCreditTerm.Text = drCurrent["credit_term"].ToString();
			//				}
			//				else
			//				{
			//					txtCreditTerm.Text = "";
			//				}
			//			}
	
			msTextBox txtISectorCode = (msTextBox)customerProfileMultiPage.FindControl("txtISectorCode");

			if(txtISectorCode != null)
			{
				if(drCurrent["industrial_sector_code"]!= null && (!drCurrent["industrial_sector_code"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					txtISectorCode.Text = drCurrent["industrial_sector_code"].ToString();
				}
				else
				{
					txtISectorCode.Text = "";
				}
			}
			

			msTextBox txtCreditLimit = (msTextBox)customerProfileMultiPage.FindControl("txtCreditLimit");
			
			if(txtCreditLimit != null)
			{
				if(drCurrent["credit_limit"]!= null && (!drCurrent["credit_limit"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					txtCreditLimit.Text = drCurrent["credit_limit"].ToString();

					decimal decCreditLimit = Convert.ToDecimal(drCurrent["credit_limit"]);
					txtCreditLimit.Text = String.Format((String)ViewState["m_format"], decCreditLimit);

					
				}
				else
				{
					txtCreditLimit.Text = "";
					
				}
			}


			msTextBox txtCreditAvailable = (msTextBox)customerProfileMultiPage.FindControl("txtCreditAvailable");

			//		radioBtnCash = (RadioButton)customerProfileMultiPage.FindControl("radioBtnCash");
			//			if(radioBtnCash != null && !radioBtnCash.Checked)
			//			{
			if(txtCreditAvailable != null)
			{
				if(drCurrent["custid"]!= null && (!drCurrent["custid"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					CustomerCreditused cs = new CustomerCreditused();
					//Comment by chai 03/02/2012

					double	dCreditLimit=0;
					double	dCreditUsed =0;
					if(drCurrent["credit_limit"] != DBNull.Value && drCurrent["credit_limit"].ToString().Length>0)
					{
						dCreditLimit = double.Parse(drCurrent["credit_limit"].ToString());
					}
					if(drCurrent["credit_used"] != DBNull.Value && drCurrent["credit_used"].ToString().Length>0)
					{
						dCreditUsed = double.Parse(drCurrent["credit_used"].ToString());
					}
					txtCreditAvailable.Text =  String.Format((String)ViewState["m_format"],dCreditLimit-dCreditUsed);
					//-------------End

					if(Decimal.Parse(txtCreditAvailable.Text) < 0)
						txtCreditAvailable.ForeColor = Color.Red;
					else
						txtCreditAvailable.ForeColor = Color.Black;											  
				}
				else
				{
					txtCreditAvailable.ForeColor = Color.Black;
					txtCreditAvailable.Text = "";
				}
			}
			//}
			msTextBox txtCreditUsed = (msTextBox)customerProfileMultiPage.FindControl("txtCreditUsed");
			
			if(txtCreditUsed != null)
			{
				if(drCurrent["custid"]!= null && (!drCurrent["custid"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					CustomerCreditused cs = new CustomerCreditused();												
					double decMaxAmt=Double.Parse(drCurrent["credit_used"].ToString());
					txtCreditUsed.Text = String.Format((String)ViewState["m_format"], decMaxAmt);
					
				}
				else
				{
					//txtCreditUsed.ForeColor = Color.Black;
					txtCreditUsed.Text = "";
					txtCreditAvailable.Text = "";
					
				}
			}

			msTextBox txtCreditTerm = (msTextBox)customerProfileMultiPage.FindControl("txtCreditTerm");
			
			if(txtCreditTerm != null)
			{
				if(drCurrent["credit_term"]!= null && (!drCurrent["credit_term"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					txtCreditTerm.Text = drCurrent["credit_term"].ToString();
				}
				else
				{
					txtCreditTerm.Text = "";
					
				}
			}

			msTextBox txtCreditStatus = (msTextBox)customerProfileMultiPage.FindControl("txtCreditStatus");
			
			if(txtCreditStatus != null)
			{
				if(drCurrent["creditstatus"]!= null && (!drCurrent["creditstatus"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					if (drCurrent["creditstatus"].ToString()=="A")
					{
						txtCreditStatus.Text = "AVAILABLE";
					}
					else
					{
						txtCreditStatus.Text = "NOT AVAILABLE";
					}
					
				}
				else
				{
					txtCreditStatus.Text = "";
					
				}
			}
			
			msTextBox txtCreditThredholds = (msTextBox)customerProfileMultiPage.FindControl("txtCreditThredholds");
			
			if(txtCreditThredholds != null)
			{
				if(drCurrent["creditthreshold"]!= null && (!drCurrent["creditthreshold"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					txtCreditThredholds.Text = drCurrent["creditthreshold"].ToString();
				}
				else
				{
					txtCreditThredholds.Text = "";
					
				}
			}


			msTextBox txtActiveQuatationNo = (msTextBox)customerProfileMultiPage.FindControl("txtActiveQuatationNo");

			if(txtActiveQuatationNo != null)
			{
				if(drCurrent["active_quotation_no"]!= null && (!drCurrent["active_quotation_no"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					txtActiveQuatationNo.Text = drCurrent["active_quotation_no"].ToString() ;
				}
				else
				{
					txtActiveQuatationNo.Text = "";
				}
			}

			msTextBox txtCreditOutstanding = (msTextBox)customerProfileMultiPage.FindControl("txtCreditOutstanding");

			if(txtCreditOutstanding != null)
			{
				if(drCurrent["credit_outstanding"]!= null && (!drCurrent["credit_outstanding"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					txtCreditOutstanding.Text = drCurrent["credit_outstanding"].ToString();
				}
				else
				{
					txtCreditOutstanding.Text = "";
				}
			}

			TextBox txtRemark = (TextBox)customerProfileMultiPage.FindControl("txtRemark");

			if(txtRemark != null)
			{
				if(drCurrent["remark"]!= null && (!drCurrent["remark"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					txtRemark.Text = drCurrent["remark"].ToString();
				}
				else
				{
					txtRemark.Text = "";
				}
			}
			

			TextBox txtSalesmanID = (TextBox)customerProfileMultiPage.FindControl("txtSalesmanID");

			if(txtSalesmanID != null)
			{
				if(drCurrent["SalesmanID"]!= null && (!drCurrent["SalesmanID"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					txtSalesmanID.Text = drCurrent["SalesmanID"].ToString();
				}
				else
				{
					txtSalesmanID.Text = "";
				}
			}
			

			msTextBox txtPromisedTotWt = (msTextBox)customerProfileMultiPage.FindControl("txtPromisedTotWt");


			if(txtPromisedTotWt != null)
			{
				if(drCurrent["prom_tot_wt"]!= null && (!drCurrent["prom_tot_wt"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					txtPromisedTotWt.Text = drCurrent["prom_tot_wt"].ToString();
				}
				else
				{
					txtPromisedTotWt.Text = "";
				}
			}
			

			radioBtnCash = (RadioButton)customerProfileMultiPage.FindControl("radioBtnCash");
			RadioButton radioBtnCredit = (RadioButton)customerProfileMultiPage.FindControl("radioBtnCredit");

			if(radioBtnCash != null && radioBtnCash != null)
			{
				if(drCurrent["payment_mode"]!= null && (!drCurrent["payment_mode"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					String strPaymentMode = drCurrent["payment_mode"].ToString();

					if(strPaymentMode == "C")
					{
						radioBtnCash.Checked = true;
						radioBtnCredit.Checked = false;
						txtNextBillPlacementDate.Enabled = false;
						btnSetBillPlacementRules.Enabled = false;
						txtRemarkBp.Enabled = false;

						txtCreditAvailable.Text = "";
						txtCreditUsed.Text = "";
					}
					else if(strPaymentMode == "R")
					{
						radioBtnCredit.Checked = true;
						radioBtnCash.Checked = false;
						txtNextBillPlacementDate.Enabled = true;
						EnableBillPlacement(true);
					}
				}
				else
				{
					radioBtnCash.Checked = false;
					radioBtnCredit.Checked = false;
				}
			}
			

			msTextBox txtPromisedTotPkg = (msTextBox)customerProfileMultiPage.FindControl("txtPromisedTotPkg");

			if(txtPromisedTotPkg != null)
			{
				if(drCurrent["prom_tot_package"]!= null && (!drCurrent["prom_tot_package"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					txtPromisedTotPkg.Text = drCurrent["prom_tot_package"].ToString();
				}
				else
				{
					txtPromisedTotPkg.Text = "";
				}
			}
			

			msTextBox txtPromisedPeriod = (msTextBox)customerProfileMultiPage.FindControl("txtPromisedPeriod");


			if(txtPromisedPeriod != null)
			{
				if(drCurrent["prom_period"]!= null && (!drCurrent["prom_period"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					txtPromisedPeriod.Text = drCurrent["prom_period"].ToString();
				}
				else
				{
					txtPromisedPeriod.Text = "";
				}
			}
			
			
			msTextBox txtFreeInsuranceAmt = (msTextBox)customerProfileMultiPage.FindControl("txtFreeInsuranceAmt");

			if(txtFreeInsuranceAmt != null)
			{
				if(drCurrent["free_insurance_amt"]!= null && (!drCurrent["free_insurance_amt"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{

					decimal decfree_insurance_amt = Convert.ToDecimal(drCurrent["free_insurance_amt"]);
					txtFreeInsuranceAmt.Text = String.Format((String)ViewState["m_format"], decfree_insurance_amt);
				}
				//				else
				//				{
				//					txtFreeInsuranceAmt.Text = "";
				//				}
			}
			

			msTextBox txtInsurancePercentSurcharge = (msTextBox)customerProfileMultiPage.FindControl("txtInsurancePercentSurcharge");

			if(txtInsurancePercentSurcharge != null)
			{
				if(drCurrent["insurance_percent_surcharge"]!= null && (!drCurrent["insurance_percent_surcharge"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					txtInsurancePercentSurcharge.Text = drCurrent["insurance_percent_surcharge"].ToString();
				}
				//				else
				//				{
				//					txtInsurancePercentSurcharge.Text = "";
				//				}
			}

			// for Invoice Return Days
			msTextBox txtInvoiceReturn = (msTextBox)customerProfileMultiPage.FindControl("txtInvoiceReturn");

			if(txtInvoiceReturn != null)
			{
				if(drCurrent["invoice_return_days"]!= null && (!drCurrent["invoice_return_days"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					txtInvoiceReturn.Text = drCurrent["invoice_return_days"].ToString();
				}
				else
				{
					txtInvoiceReturn.Text = "";
				}
			}
			

			TextBox txtEmail = (TextBox)customerProfileMultiPage.FindControl("txtEmail");

			if(txtEmail != null)
			{
				if(drCurrent["email"]!= null && (!drCurrent["email"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					txtEmail.Text = drCurrent["email"].ToString();
				}
				else
				{
					txtEmail.Text = "";
				}
			}
			

			TextBox txtCreatedBy = (TextBox)customerProfileMultiPage.FindControl("txtCreatedBy");

			if(txtCreatedBy != null)
			{
				if(drCurrent["created_by"]!= null && (!drCurrent["created_by"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					txtCreatedBy.Text = drCurrent["created_by"].ToString();
				}
				else
				{
					txtCreatedBy.Text = "";
				}
			}

			msTextBox txtCODSurchargeAmt = (msTextBox)customerProfileMultiPage.FindControl("txtCODSurchargeAmt");

			if(txtCODSurchargeAmt != null)
			{
				if(drCurrent["cod_surcharge_amt"]!= null && (!drCurrent["cod_surcharge_amt"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					//Chang integer to double numeric By GwanG
					//No rounding when DisplayCurrentPage (by Sittichai (09/01/2008))
					//decimal decCODSurchargeAmt =TIESUtility.EnterpriseRounding(Convert.ToDecimal(drCurrent["cod_surcharge_amt"]),(int)ViewState["wt_rounding_method"],(decimal)ViewState["wt_increment_amt"]);
					decimal decCODSurchargeAmt = Convert.ToDecimal(drCurrent["cod_surcharge_amt"]);
					txtCODSurchargeAmt.Text = String.Format((String)ViewState["m_format"], decCODSurchargeAmt);
					//txtCODSurchargeAmt.Text = String.Format("{0:#,###.00}",Convert.ToDouble(drCurrent["cod_surcharge_amt"].ToString()));
				}
				else
				{
					txtCODSurchargeAmt.Text = "";
				}
			}

			msTextBox txtCODSurchargePercent = (msTextBox)customerProfileMultiPage.FindControl("txtCODSurchargePercent");

			if(txtCODSurchargePercent != null)
			{
				if(drCurrent["cod_surcharge_percent"]!= null && (!drCurrent["cod_surcharge_percent"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					//Chang integer to double numeric By GwanG
					//txtCODSurchargePercent.Text = drCurrent["cod_surcharge_percent"].ToString();
					txtCODSurchargePercent.Text = String.Format("{0:#,###.00}",Convert.ToDouble(drCurrent["cod_surcharge_percent"].ToString()));
				}
				else
				{
					txtCODSurchargePercent.Text = "";
				}
			}

			msTextBox txtMaxAmt = (msTextBox)customerProfileMultiPage.FindControl("txtMaxAmt");

			if(txtMaxAmt != null)
			{
				if(drCurrent["insurance_maximum_amt"]!= null && (!drCurrent["insurance_maximum_amt"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					decimal decMaxAmt = Convert.ToDecimal(drCurrent["insurance_maximum_amt"]);
					txtMaxAmt.Text = String.Format((String)ViewState["m_format"], decMaxAmt);
				}
				//				else
				//				{
				//					txtMaxAmt.Text = "";
				//				}
			}


			// by Ching Nov 29,2007
			msTextBox txtMaxCODSurcharge = (msTextBox)customerProfileMultiPage.FindControl("txtMaxCODSurcharge");

			if(txtMaxCODSurcharge != null)
			{
				if(drCurrent["max_cod_surcharge"]!= null && (!drCurrent["max_cod_surcharge"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					//No rounding when DisplayCurrentPage (by Sittichai (09/01/2008))
					//decimal decMaxCODSurcharge = TIESUtility.EnterpriseRounding(Convert.ToDecimal(drCurrent["max_cod_surcharge"]),(int)ViewState["wt_rounding_method"], (decimal)ViewState["wt_increment_amt"]);
					decimal decMaxCODSurcharge = Convert.ToDecimal(drCurrent["max_cod_surcharge"]);
					txtMaxCODSurcharge.Text = string.Format((string)ViewState["m_format"], decMaxCODSurcharge);
					//txtMaxCODSurcharge.Text = String.Format("{0:#,###.00}",Convert.ToDouble(drCurrent["max_cod_surcharge"].ToString()));
				}
				else
				{
					txtMaxCODSurcharge.Text = "";
				}
			}

			msTextBox txtMinCODSurcharge = (msTextBox)customerProfileMultiPage.FindControl("txtMinCODSurcharge");

			if(txtMinCODSurcharge != null)
			{
				if(drCurrent["min_cod_surcharge"]!= null && (!drCurrent["min_cod_surcharge"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					//No rounding when DisplayCurrentPage (by Sittichai (09/01/2008))
					//decimal decMinCODsurecharge = TIESUtility.EnterpriseRounding(Convert.ToDecimal(drCurrent["min_cod_surcharge"]),(int)ViewState["wt_rounding_method"], (decimal)ViewState["wt_increment_amt"]);
					decimal decMinCODsurecharge = Convert.ToDecimal(drCurrent["min_cod_surcharge"]);
					txtMinCODSurcharge.Text = string.Format((string)ViewState["m_format"], decMinCODsurecharge);
					//txtMinCODSurcharge.Text = String.Format("{0:#,###.00}",Convert.ToDouble(drCurrent["min_cod_surcharge"].ToString()));
						
				}
				else
				{
					txtMinCODSurcharge.Text = "";
				}
			}


			// End 
			//No Format
			//By Aoo
			msTextBox msOtherSurchargeAmount = (msTextBox)customerProfileMultiPage.FindControl("msOtherSurchargeAmount");
			if(msOtherSurchargeAmount != null)
			{
				if(drCurrent["other_surcharge_amount"]!= null && (!drCurrent["other_surcharge_amount"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					decimal decOtherSurAmt = Convert.ToDecimal(drCurrent["other_surcharge_amount"].ToString());
					msOtherSurchargeAmount.Text = String.Format((String)ViewState["m_format"],decOtherSurAmt);
				}
				else
				{
					msOtherSurchargeAmount.Text = "";
				}
			}
			msTextBox msOtherSurchargePercentage = (msTextBox)customerProfileMultiPage.FindControl("msOtherSurchargePercentage");
			if(msOtherSurchargePercentage != null)
			{
				if(drCurrent["other_surcharge_percentage"]!= null && (!drCurrent["other_surcharge_percentage"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					msOtherSurchargePercentage.Text = drCurrent["other_surcharge_percentage"].ToString();
				}
				else
				{
					msOtherSurchargePercentage.Text = "";
				}
			}
			msTextBox msOtherSurchargeMinimum = (msTextBox)customerProfileMultiPage.FindControl("msOtherSurchargeMinimum");
			if(msOtherSurchargeMinimum != null)
			{
				if(drCurrent["other_surcharge_min"]!= null && (!drCurrent["other_surcharge_min"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					decimal decOtherSurMin = Convert.ToDecimal(drCurrent["other_surcharge_min"]);
					msOtherSurchargeMinimum.Text = String.Format((String)ViewState["m_format"], decOtherSurMin);
				}
				else
				{
					msOtherSurchargeMinimum.Text = "";
				}
			}
			msTextBox msOtherSurchargeMaximum = (msTextBox)customerProfileMultiPage.FindControl("msOtherSurchargeMaximum");
			if(msOtherSurchargeMaximum != null)
			{
				if(drCurrent["other_surcharge_max"]!= null && (!drCurrent["other_surcharge_max"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					decimal decOtherSurMax = Convert.ToDecimal(drCurrent["other_surcharge_max"]);
					msOtherSurchargeMaximum.Text = String.Format((String)ViewState["m_format"], decOtherSurMax);
				}
				else
				{
					msOtherSurchargeMaximum.Text = "";
				}
			}
			msTextBox msOtherSurchargeDescription = (msTextBox)customerProfileMultiPage.FindControl("msOtherSurchargeDescription");
			if(msOtherSurchargeDescription != null)
			{
				if(drCurrent["other_surcharge_desc"]!= null && (!drCurrent["other_surcharge_desc"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					msOtherSurchargeDescription.Text = drCurrent["other_surcharge_desc"].ToString();
				}
				else
				{
					msOtherSurchargeDescription.Text = "";
				}
			}

			msTextBox txtMaxInsuranceSurcharge = (msTextBox)customerProfileMultiPage.FindControl("txtMaxInsuranceSurcharge");
			if(txtMaxInsuranceSurcharge != null)
			{
				if(drCurrent["max_insurance_surcharge"]!= null && (!drCurrent["max_insurance_surcharge"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					decimal decMaxInsSurcharge = Convert.ToDecimal(drCurrent["max_insurance_surcharge"]);
					txtMaxInsuranceSurcharge.Text = String.Format((String)ViewState["m_format"], decMaxInsSurcharge);
				}
				else
				{
					txtMaxInsuranceSurcharge.Text = "";
				}
			}
			msTextBox txtMinInsuranceSurcharge = (msTextBox)customerProfileMultiPage.FindControl("txtMinInsuranceSurcharge");
			if(txtMinInsuranceSurcharge != null)
			{
				if(drCurrent["min_insurance_surcharge"]!= null && (!drCurrent["min_insurance_surcharge"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					decimal decMinInsSurcharge = Convert.ToDecimal(drCurrent["min_insurance_surcharge"]);
					txtMinInsuranceSurcharge.Text = String.Format((String)ViewState["m_format"], decMinInsSurcharge);
				}
				else
				{
					txtMinInsuranceSurcharge.Text = "";
				}
			}

			msTextBox msDiscountBand = (msTextBox)customerProfileMultiPage.FindControl("msDiscountBand");
			if(msDiscountBand != null)
			{
				if(drCurrent["discount_band"]!= null && (!drCurrent["discount_band"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					msDiscountBand.Text = drCurrent["discount_band"].ToString();
				}
				else
				{
					msDiscountBand.Text = "";
				}
			}
			
			msTextBox msDensityFactor = (msTextBox)customerProfileMultiPage.FindControl("msDensityFactor");
			if(msDensityFactor != null)
			{
				if(drCurrent["density_factor"]!= null && (!drCurrent["density_factor"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					msDensityFactor.Text = Convert.ToDecimal(drCurrent["density_factor"]).ToString("0");
				}
				else
				{
					msDensityFactor.Text = "";
				}
			}

			//Jeab 08 Mar  12
			msTextBox txtFirstShipDate = (msTextBox)customerProfileMultiPage.FindControl("txtFirstShipDate");
			if(txtFirstShipDate != null)
			{
				if(drCurrent["Commission_Datetime"]!= null && (!drCurrent["Commission_Datetime"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{					
					txtFirstShipDate.Text = ((DateTime)drCurrent["Commission_Datetime"]).ToString("dd/MM/yyyy");
				}
				else
				{
					txtFirstShipDate.Text = "";
				}
			}
			// Check  customer category
			TextBox txtCategory = (TextBox)customerProfileMultiPage.FindControl("txtCategory");
			if(txtCategory != null)
			{
				if(drCurrent["custCategory"]!= null && (!drCurrent["custCategory"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{					
					txtCategory.Text  =  drCurrent["custCategory"].ToString();
				}						
			}
			//Jeab 08 Mar 12  =========>  End

			CheckBox chkBox = (CheckBox)customerProfileMultiPage.FindControl("chkBox");
			if(chkBox != null)
			{
				if(drCurrent["customerBox"]!= null && (!drCurrent["customerBox"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					if(drCurrent["customerBox"].ToString()=="Y")
					{
						chkBox.Checked = true;
					}
					else
					{
						chkBox.Checked = false;
					}
				}
				else
				{
					chkBox.Checked = false;
				}
			}

			TextBox txtMinimumBox = (TextBox)customerProfileMultiPage.FindControl("txtMinimumBox");
			if(chkBox.Checked != false)
			{
				if(drCurrent["Minimum_Box"]!= null && (!drCurrent["Minimum_Box"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{		
					int minimumBox = Convert.ToInt32(drCurrent["Minimum_Box"]);
					txtMinimumBox.Text = minimumBox.ToString();
					txtMinimumBox.ReadOnly = false;
				}
				else
				{
					txtMinimumBox.Text = "";
				}
			}
			else
				txtMinimumBox.Text = "";
			//end By Aoo
			
			//Added By Tom 22/7/09
			TextBox txtSpecialInstruction = (TextBox)customerProfileMultiPage.FindControl("txtSpecialInstruction");

			if(txtSpecialInstruction != null)
			{
				if(drCurrent["remark2"]!= null && (!drCurrent["remark2"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					txtSpecialInstruction.Text = drCurrent["remark2"].ToString();
				}
				else
				{
					txtSpecialInstruction.Text = "";
				}

			}
			//End Added By Tom 22/7/09

			if(txtNextBillPlacementDate != null)
			{
				if(drCurrent["next_bill_placement_date"]!= null && (!drCurrent["next_bill_placement_date"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					txtNextBillPlacementDate.Text = ((DateTime)drCurrent["next_bill_placement_date"]).ToString("dd/MM/yyyy");
				}
				else
				{
					txtNextBillPlacementDate.Text = "";
				}

			}
			DataTable tmpBPRule = SysDataMgrDAL.GetCustomerProfile_BillPlacementRules(utility.GetAppID(),utility.GetEnterpriseID(),txtAccNo.Text);
			if(tmpBPRule.Rows.Count > 0)
			{
				foreach(DataRow drBp in tmpBPRule.Rows)
				{
					if(drBp["remark"].GetType()!= System.Type.GetType("System.DBNull"))
					{
						txtRemarkBp.Text = (String)drBp["remark"];
					}
				}
			}
			else
				txtRemarkBp.Text = "";

			BindComboLists();

			ShowReferencesForCurrentCustomer();
			ShowSCForCurrentCustomer();

			dgQuotation.CurrentPageIndex=0; // to avoid Invalid Current Page Index
			ShowQuotationsForCurrentCustomer();
			BindQuotation();

			//Added by Gwang on 27Feb08
			dgVAS.CurrentPageIndex=0;
			BindVAS();
	
		}

		private void DisplayRecNum()
		{
			
			int iCurrentRec = (Convert.ToInt32(ViewState["currentPage"]) + 1) + (Convert.ToInt32(ViewState["currentSet"]) * m_iSetSize) ;

			if(iCurrentRec >= m_sdsCustomerProfile.QueryResultMaxSize)
			{
				iCurrentRec = int.Parse( m_sdsCustomerProfile.QueryResultMaxSize.ToString());
			}

			if(!utility.GetUserCulture().ToUpper().Equals("EN-US"))
			{
				lblNumRec.Text = ""+ iCurrentRec + " " + Utility.GetLanguageText(ResourceType.ScreenLabel, "of", utility.GetUserCulture()) + " " + m_sdsCustomerProfile.QueryResultMaxSize + " " + Utility.GetLanguageText(ResourceType.ScreenLabel, "record(s)", utility.GetUserCulture());
			}
			else
			{
				lblNumRec.Text = ""+ iCurrentRec + " of " + m_sdsCustomerProfile.QueryResultMaxSize + " record(s)";
				//ViewState["currentPage"] = iCurrentRec - 1;
			}
		}

		private void btnInsert_Click(object sender, System.EventArgs e)
		{
			msTextBox txtAccNo = (msTextBox)customerProfileMultiPage.FindControl("txtAccNo");
			EnableBillPlacement(true);
			btnSetBillPlacementRules.Enabled = true;
			txtRemarkBp.Text = "";

			m_sdsCustomerProfile = SysDataMgrDAL.GetEmptyCustomerProfileDS(1);
			Session["SESSION_DS1"] = m_sdsCustomerProfile;
			
			TextBox txtCreatedBy = (TextBox)customerProfileMultiPage.FindControl("txtCreatedBy");				
			txtCreatedBy.Text = utility.GetUserID();
			
			ViewState["CPMode"] = ScreenMode.Insert;
			ViewState["CPOperation"] = Operation.None;

			ViewState["IsTextChanged"] = false;
			ViewState["currentPage"] = 0;
			//btnInsert.Enabled = false;
			Utility.EnableButton(ref btnInsert, ButtonType.Insert , false, m_moduleAccessRights);
			btnExecQry.Enabled = false;
			LoadComboLists();
			EnableTabs(true,true,false,false,false,false,false);
			DisplayCurrentPage();
			lblNumRec.Text = "";
			lblNumRefRec.Text = "";
			lblCPErrorMessage.Text = "";
			TabStrip1.SelectedIndex = 0;
			btnChangeStatus.Enabled=true;
			if(txtAccNo != null)
			{
				SetInitialFocus(txtAccNo);
			}	
			EnterpriseConfigurations();
			CheckRole();
		}

		private void btnSave_Click(object sender, System.EventArgs e)
		{			
			int iOperation2 = (int)ViewState["CPOperation"];
			
			TextBox txtCreatedBy = (TextBox)customerProfileMultiPage.FindControl("txtCreatedBy");
			if (txtCreatedBy.Text == "") 
			{
				txtCreatedBy.Text =utility.GetUserID();
			}
			CheckBox chkBox = (CheckBox)customerProfileMultiPage.FindControl("chkBox");
			TextBox txtMinimumBox = (TextBox)customerProfileMultiPage.FindControl("txtMinimumBox");
			if(chkBox.Checked)
			{
				if(txtMinimumBox.Text=="" || Convert.ToInt32(txtMinimumBox.Text)==0)
				{
					lblCPErrorMessage.Text = "Customer Box Minimum is Required.";
					return;
				}
			}
			msTextBox txtCODSurchargeAmt = (msTextBox)customerProfileMultiPage.FindControl("txtCODSurchargeAmt");
			msTextBox txtCODSurchargePercent = (msTextBox)customerProfileMultiPage.FindControl("txtCODSurchargePercent");
			if(txtCODSurchargeAmt != null && txtCODSurchargePercent != null)
			{
				if((txtCODSurchargeAmt.Text.Trim() != "") && (txtCODSurchargePercent.Text.Trim() != ""))
				{
					lblCPErrorMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"ISCODSAVALID",utility.GetUserCulture());
					return;
				}
			}

			msTextBox msOtherSurchargeAmount = (msTextBox)customerProfileMultiPage.FindControl("msOtherSurchargeAmount");
			msTextBox msOtherSurchargePercentage = (msTextBox)customerProfileMultiPage.FindControl("msOtherSurchargePercentage");
			if(msOtherSurchargeAmount != null && msOtherSurchargePercentage != null)
			{
				if((msOtherSurchargeAmount.Text.Trim() != "") && (msOtherSurchargePercentage.Text.Trim() != ""))
				{
					lblCPErrorMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"ISOTHERSURCHARGEVALID",utility.GetUserCulture());
					return;
				}
			}


			msTextBox txtMaxCODSurcharge = (msTextBox)customerProfileMultiPage.FindControl("txtMaxCODSurcharge");
			msTextBox txtMinCODSurcharge = (msTextBox)customerProfileMultiPage.FindControl("txtMinCODSurcharge");
			if(txtMaxCODSurcharge != null && txtMinCODSurcharge != null)
			{

				if((txtMaxCODSurcharge.Text.Trim() != "") && (txtMinCODSurcharge.Text.Trim() != ""))
					if(Double.Parse(txtMaxCODSurcharge.Text)<Double.Parse(txtMinCODSurcharge.Text))
					{
						
						lblCPErrorMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"Max COD Surcharge must more than Min COD Surcharge",utility.GetUserCulture());

						//"Max COD Surcharge must more than Min COD Surcharge";
						return;
					}
			}

			msTextBox txtMinInsuranceSurcharge = (msTextBox)customerProfileMultiPage.FindControl("txtMinInsuranceSurcharge");
			msTextBox txtMaxInsuranceSurcharge = (msTextBox)customerProfileMultiPage.FindControl("txtMaxInsuranceSurcharge");

			if(txtMaxInsuranceSurcharge != null && txtMinInsuranceSurcharge != null)
			{
				if((txtMaxInsuranceSurcharge.Text.Trim() != "") && (txtMinInsuranceSurcharge.Text.Trim() != ""))
					if(Double.Parse(txtMaxInsuranceSurcharge.Text)<Double.Parse(txtMinInsuranceSurcharge.Text))
					{
						lblCPErrorMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"INSURANCE_SURCHARGE_MAXMIN",utility.GetUserCulture());
						return;
					}
			}
			
			msTextBox msOtherSurchargeMaximum = (msTextBox)customerProfileMultiPage.FindControl("msOtherSurchargeMaximum");
			msTextBox msOtherSurchargeMinimum = (msTextBox)customerProfileMultiPage.FindControl("msOtherSurchargeMinimum");

			if(msOtherSurchargeMaximum != null && msOtherSurchargeMinimum != null)
			{
				if((msOtherSurchargeMaximum.Text.Trim() != "") && (msOtherSurchargeMinimum.Text.Trim() != ""))
					if(Double.Parse(msOtherSurchargeMaximum.Text)<Double.Parse(msOtherSurchargeMinimum.Text))
					{
						lblCPErrorMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"OTHER_SURCHARGE_MAXMIN",utility.GetUserCulture());
						return;
					}
			}

			msTextBox msDensityFactor = (msTextBox)customerProfileMultiPage.FindControl("msDensityFactor");
			if(msDensityFactor != null)
			{
				if(msDensityFactor.Text.Trim() != "")
					if(Double.Parse(msDensityFactor.Text) == 0)
					{
					
						lblCPErrorMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"INPUTDENSITYFACTOR",utility.GetUserCulture());
						return;
					}	
			}

			//Jeab  09 Mar 2012
			msTextBox mstxtFirstShipDate = (msTextBox)customerProfileMultiPage.FindControl("txtFirstShipDate");
			if(mstxtFirstShipDate != null)
			{
				if((mstxtFirstShipDate.Text.Trim() == "") )
				{
					lblCPErrorMessage.Text = "First ship date is Required.";
					return;
				}
			}
			//Jeab  09 Mar 2012  =========>  End

			FillCustomerProfileDataRow(Convert.ToInt32(ViewState["currentPage"]));
			RadioButton radioBtnCredit = (RadioButton)customerProfileMultiPage.FindControl("radioBtnCredit");

			int iOperation = (int)ViewState["CPOperation"];			
			DbCombo DbComboMasterAccount = (DbCombo)customerProfileMultiPage.FindControl("DbComboMasterAccount");			
			String strMasterAccount = "N";
						
			msTextBox txtAccNo = (msTextBox)customerProfileMultiPage.FindControl("txtAccNo");
			string sessionName = "dtBillPlacementRules_"+txtAccNo.Text.Trim();
			//			TextBox hdCreditLimit = (TextBox)customerProfileMultiPage.FindControl("hdCreditLimit");	
			//			TextBox hdCreditTerm = (TextBox)customerProfileMultiPage.FindControl("hdCreditTerm");	
			Boolean boolFlagUPdateCredit=false;
			if(Session["CustCreditInfo"] != null)
			{
				Hashtable ht = (Hashtable)Session["CustCreditInfo"];
				boolFlagUPdateCredit=true;
				if(ht["CreditLimit"] != null && ht["CreditTerm"]!=null)
				{
					if(ht["CreditLimit"].ToString()!= "" && ht["CreditTerm"].ToString() !="" && (int)ViewState["CPOperation"]==(int)Operation.None && txtAccNo.Text!="" )
					{
						ChangeCPState();
					}
				}
			}
			
	
			
			switch(iOperation)
			{

				case (int)Operation.Update:

					DataSet dsToUpdate = m_sdsCustomerProfile.ds.GetChanges();
					try
					{
						if(radioBtnCredit.Checked == true)
						{
							if(Session[sessionName] == null)
							{
								Session[sessionName] = SysDataMgrDAL.GetCustomerProfile_BillPlacementRules(utility.GetAppID(),utility.GetEnterpriseID(),txtAccNo.Text);
								if(((DataTable)Session[sessionName]).Rows.Count < 1)
								{
									//lblCPErrorMessage.Text =  Utility.GetLanguageText(ResourceType.UserMessage,"MSG_MUST_ESTABLISH_BILLRULE",utility.GetUserCulture());
									//return;
								}
								else
									Session[sessionName] = null;
							}
							else
							{
								if(((DataTable)Session[sessionName]).Rows.Count > 0)
								{
									SysDataMgrDAL.DeleteCustomerProfile_BillPlacementRules(utility.GetAppID(),utility.GetEnterpriseID(),txtAccNo.Text.Trim());
									if(((DataTable)Session[sessionName]).Rows.Count > 0)
									{
										SysDataMgrDAL.AddCustomerProfile_BillPlacementRules(utility.GetAppID(),utility.GetEnterpriseID(),((DataTable)Session[sessionName]));
									}
								}
								else
								{
									//lblCPErrorMessage.Text =  Utility.GetLanguageText(ResourceType.UserMessage,"MSG_MUST_ESTABLISH_BILLRULE",utility.GetUserCulture());
									//return;
								}
							}

						}

						if (boolFlagUPdateCredit==true)
						{
							Hashtable ht = (Hashtable)Session["CustCreditInfo"];
							SysDataMgrDAL.AddCustomerCredit(utility.GetAppID(),utility.GetEnterpriseID(),
								txtAccNo.Text,
								ht["CreditLimit"].ToString(),
								ht["CreditTerm"].ToString(),
								ht["PaymentMode"].ToString(),
								ht["CreditStatus"].ToString(),
								ht["CreditThreshold"].ToString(),
								ht["ReasonCode"].ToString(),
								ht["ReasonDesc"].ToString(),
								"",strMasterAccount,utility.GetUserID());
							DataRow drEach = dsToUpdate.Tables[0].Rows[0];
							if(ht["CreditLimit"].ToString().Trim()!="")
							{
								drEach["credit_limit"]=Convert.ToDecimal(ht["CreditLimit"].ToString());
							}
							else
							{
								drEach["credit_limit"]=DBNull.Value;
							}
						}
						SysDataMgrDAL.ModifyCustomerProfileDS(utility.GetAppID(),utility.GetEnterpriseID(),dsToUpdate,DbComboMasterAccount.Value);

						//Jeab 12 Mar 2012
						if (CheckCommissionDTChanged(dsToUpdate) ==true)
						{
							SysDataMgrDAL.AddCommissionData(utility.GetAppID(),utility.GetEnterpriseID(),dsToUpdate,utility.GetUserID());
						}
						//Jeab 12 Mar 2012  =========>  End

						Utility.EnableButton(ref btnInsert,ButtonType.Insert,true,m_moduleAccessRights);
						ViewState["IsTextChanged"] = false;
						ChangeCPState();
						m_sdsCustomerProfile.ds.Tables[0].Rows[Convert.ToInt32(ViewState["currentPage"])].AcceptChanges();
						lblCPErrorMessage.Text =  Utility.GetLanguageText(ResourceType.UserMessage,"MODIFIED_SUCCESSFULLY",utility.GetUserCulture());
						Logger.LogDebugInfo("DEBUG MODE : CustomerProfile","btnSave_Click","INF004","save in modify mode..");
						Logger.LogDebugInfo("module2","DEBUG MODE :(module2 only) CustomerProfile","btnSave_Click","INF004","save in modify mode...");
						Session["CustCreditInfo"]=null;

					}
					catch(ApplicationException appException)
					{
						String strMsg = appException.Message;

						if(strMsg.IndexOf("FK") != -1 )
						{
							if(strMsg.IndexOf("Industrial_Sector") != -1)
							{
								lblCPErrorMessage.Text =  Utility.GetLanguageText(ResourceType.UserMessage,"INVALID_INDS_SECTOR_CODE",utility.GetUserCulture());
							}
							else if(strMsg.IndexOf("Zipcode") != -1)
							{
								lblCPErrorMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"INVALID_ZIP",utility.GetUserCulture());
							}
						}
						else if(strMsg.IndexOf("APPDB.SYS_C002164") != -1 )
						{
							if(strMsg.IndexOf("Industrial_Sector") != -1)
							{
								lblCPErrorMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"INVALID_INDS_SECTOR_CODE",utility.GetUserCulture());
							}
							else if(strMsg.IndexOf("Zipcode") != -1)
							{
								lblCPErrorMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"INVALID_ZIP",utility.GetUserCulture());
							}
						}
						else
						{
							lblCPErrorMessage.Text = strMsg;
						}


						Logger.LogTraceError("TRACE MODE : CustomerProfile","btnSave_Click","ERR004","Error updating record :--- "+strMsg);
						Logger.LogTraceError("module2","TRACE MODE : (module2 only)CustomerProfile","btnSave_Click","ERR004","Error updating record : "+strMsg);

						return;
						
					}
					break;

				case (int)Operation.Insert:
					
					DataSet dsToInsert = m_sdsCustomerProfile.ds.GetChanges();

					try
					{
						//dsToInsert.WriteXml("D:/XML_Folder/test_insert.xml");
						
						if(Session["CustCreditInfo"] != null)
						{
							Hashtable ht = (Hashtable)Session["CustCreditInfo"];

							SysDataMgrDAL.AddCustomerCredit(utility.GetAppID(),utility.GetEnterpriseID(),
								txtAccNo.Text,
								ht["CreditLimit"].ToString(),
								ht["CreditTerm"].ToString(),
								ht["PaymentMode"].ToString(),
								ht["CreditStatus"].ToString(),
								ht["CreditThreshold"].ToString(),
								ht["ReasonCode"].ToString(),
								ht["ReasonDesc"].ToString(),
								"",strMasterAccount,utility.GetUserID());
							DataRow drEach = dsToInsert.Tables[0].Rows[0];
							drEach["credit_limit"]=Convert.ToDecimal(ht["CreditLimit"].ToString());
							CustomerProfileConfigurations conf = new CustomerProfileConfigurations();
							conf = EnterpriseConfigMgrDAL.GetCustomerProfileConfigurations(utility.GetAppID(),utility.GetEnterpriseID());
							if(conf.HCSupportedbyEnterprise == false)
							{
								drEach["pod_slip_required"]="N";
								drEach["hc_invoice_required"]="N";
							}
						}
						SysDataMgrDAL.AddCustomerProfileDS(utility.GetAppID(),utility.GetEnterpriseID(),dsToInsert);
						
						
						SysDataMgrDAL.AddCommissionData(utility.GetAppID(),utility.GetEnterpriseID(),dsToInsert,utility.GetUserID());  //Jeab 12 Mar 2012
						
						//btnInsert.Enabled = true;
						if(radioBtnCredit.Checked == true)
						{
							if(Session[sessionName] == null)
							{
								//lblCPErrorMessage.Text =  Utility.GetLanguageText(ResourceType.UserMessage,"MSG_MUST_ESTABLISH_BILLRULE",utility.GetUserCulture());
								//return;
							}
							else
							{
								if(((DataTable)Session[sessionName]).Rows.Count > 0)
								{
									SysDataMgrDAL.DeleteCustomerProfile_BillPlacementRules(utility.GetAppID(),utility.GetEnterpriseID(),txtAccNo.Text.Trim());
									if(((DataTable)Session[sessionName]).Rows.Count > 0)
									{
										SysDataMgrDAL.AddCustomerProfile_BillPlacementRules(utility.GetAppID(),utility.GetEnterpriseID(),((DataTable)Session[sessionName]));
									}
								}
								else
								{
									//lblCPErrorMessage.Text =  Utility.GetLanguageText(ResourceType.UserMessage,"MSG_MUST_ESTABLISH_BILLRULE",utility.GetUserCulture());
									//return;
								}
							}

						}
						Utility.EnableButton(ref btnInsert,ButtonType.Insert,true,m_moduleAccessRights);
						ViewState["IsTextChanged"] = false;

						//						msTextBox txtAccNo = (msTextBox)customerProfileMultiPage.FindControl("txtAccNo");
						if(txtAccNo != null && txtAccNo.Text != "")
						{
							ViewState["currentCustomer"] =  txtAccNo.Text;
							txtAccNo.Enabled = false;
						}

						EnableTabs(true,true,true,true,true,true,true);

						ChangeCPState();
						m_sdsCustomerProfile.ds.Tables[0].Rows[Convert.ToInt32(ViewState["currentPage"])].AcceptChanges();
						lblCPErrorMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"INS_SUCCESSFULLY",utility.GetUserCulture());
						Logger.LogDebugInfo("CustomerProfile","btnSave_Click","INF004","save in Insert mode");
						Session["CustCreditInfo"]=null;
					}
					catch(ApplicationException appException)
					{
						String strMsg = appException.Message;

						if(strMsg.IndexOf("duplicate key") != -1 )
						{
							lblCPErrorMessage.Text =Utility.GetLanguageText(ResourceType.UserMessage,"DUP_KEY_FOUND_CUST_NO",utility.GetUserCulture());
						}
						else if(strMsg.IndexOf("FK") != -1 )
						{
							if(strMsg.IndexOf("Industrial_Sector") != -1)
							{
								lblCPErrorMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"INVALID_INDS_SECTOR_CODE",utility.GetUserCulture());
							}
							else if(strMsg.IndexOf("Zipcode") != -1)
							{
								lblCPErrorMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"INVALID_ZIP",utility.GetUserCulture());
							}
						}
						else if(strMsg.IndexOf("APPDB.SYS_C001818") != -1 )
						{
							lblCPErrorMessage.Text =Utility.GetLanguageText(ResourceType.UserMessage,"DUP_KEY_FOUND_CUST_NO",utility.GetUserCulture());
						}
						else if(strMsg.IndexOf("APPDB.SYS_C002164") != -1 )
						{
							if(strMsg.IndexOf("Industrial_Sector") != -1)
							{
								lblCPErrorMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"INVALID_INDS_SECTOR_CODE",utility.GetUserCulture());
							}
							else if(strMsg.IndexOf("Zipcode") != -1)
							{
								lblCPErrorMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"INVALID_ZIP",utility.GetUserCulture());
							}
						}
						else
						{
							lblCPErrorMessage.Text = strMsg;
						}

						return;
					}

					break;

			}

			

		}

		private void btnDelete_Click(object sender, System.EventArgs e)
		{
		
		}

		private void btnZipcodePopup_Click(object sender, System.EventArgs e)
		{
			msTextBox txtZipcode = (msTextBox)customerProfileMultiPage.FindControl("txtZipCode");
			TextBox txtState = (TextBox)customerProfileMultiPage.FindControl("txtState");
			TextBox txtCountry = (TextBox)customerProfileMultiPage.FindControl("txtCountry");

			String strZipcodeClientID = null;
			String strStateClientID = null;
			String strCountryClientID = null;

			String strZipcode = null;
				
			if(txtZipcode != null)
			{
				strZipcodeClientID = txtZipcode.ClientID;
				strZipcode = txtZipcode.Text;
			}

			if(txtState != null)
			{
				strStateClientID = txtState.ClientID;
			}
				
			if(txtCountry != null)
			{
				strCountryClientID = txtCountry.ClientID;
			}
			String sUrl = "ZipcodePopup.aspx?FORMID=CustomerProfile&ZIPCODE="+strZipcode+
				"&ZIPCODE_CID="+strZipcodeClientID+
				"&STATE_CID="+strStateClientID+
				"&COUNTRY_CID="+strCountryClientID+
				"&ISFR=N";
			ArrayList paramList = new ArrayList();
			paramList.Add(sUrl);
			String sScript = Utility.GetScript("openWindowParam.js",paramList);
			Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);

		}

		private void btnISectorSearch_Click(object sender, System.EventArgs e)
		{


			msTextBox txtISectorCode = (msTextBox)customerProfileMultiPage.FindControl("txtISectorCode");

			String strISectorClientID = null;

			String strISector = null;
				
			if(txtISectorCode != null)
			{
				strISectorClientID = txtISectorCode.ClientID;
				strISector = txtISectorCode.Text;
			}

			String sUrl = "IndustrialSectorPopup.aspx?FORMID=CustomerProfile&ISECTOR="+strISector+"&ISECTOR_CID="+strISectorClientID;
			ArrayList paramList = new ArrayList();
			paramList.Add(sUrl);
			String sScript = Utility.GetScript("openWindowParam.js",paramList);
			Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
		}

		public void btnSearchSalesman_Click(object sender, System.EventArgs e)
		{
			String sQrySalesmanId ="";
			TextBox txtSalesmanID = (TextBox)customerProfileMultiPage.FindControl("txtSalesmanID");
			if(txtSalesmanID != null && txtSalesmanID.Text != "")
			{
				sQrySalesmanId =txtSalesmanID.Text.Trim();
			}
			
			String sUrl = "SalesPersonPopup.aspx?FORMID=CustomerProfile&QUERY_SALESMANID="+sQrySalesmanId;
			ArrayList paramList = new ArrayList();
			paramList.Add(sUrl);
			String sScript = Utility.GetScript("openWindowParam.js",paramList);
			Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);	
		}

		private void btnRefZipcodeSearch_Click(object sender, System.EventArgs e)
		{


			msTextBox txtZipcode = (msTextBox)customerProfileMultiPage.FindControl("txtRefZipcode");
			TextBox txtState = (TextBox)customerProfileMultiPage.FindControl("txtRefState");
			TextBox txtCountry = (TextBox)customerProfileMultiPage.FindControl("txtRefCountry");

			String strZipcodeClientID = null;
			String strStateClientID = null;
			String strCountryClientID = null;

			String strZipcode = null;
					
			if(txtZipcode != null)
			{
				strZipcodeClientID = txtZipcode.ClientID;
				strZipcode = txtZipcode.Text;
			}

			if(txtState != null)
			{
				strStateClientID = txtState.ClientID;
			}
					
			if(txtCountry != null)
			{
				strCountryClientID = txtCountry.ClientID;
			}



			String sUrl = "ZipcodePopup.aspx?FORMID=CustomerProfile&ZIPCODE="+strZipcode+
				"&ZIPCODE_CID="+strZipcodeClientID+
				"&STATE_CID="+strStateClientID+
				"&COUNTRY_CID="+strCountryClientID+
				"&ISFR=N";
			ArrayList paramList = new ArrayList();
			paramList.Add(sUrl);
			String sScript = Utility.GetScript("openWindowParam.js",paramList);
			Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);

		}

		private void FillCustomerProfileDataRow(int iCurrentRow)
		{
			CustomerProfileConfigurations conf = new CustomerProfileConfigurations();
			conf = EnterpriseConfigMgrDAL.GetCustomerProfileConfigurations(utility.GetAppID(),utility.GetEnterpriseID());

			m_sdsCustomerProfile = (SessionDS)Session["SESSION_DS1"];
			DataRow drCurrent;
			if(m_sdsCustomerProfile!=null)
				drCurrent = m_sdsCustomerProfile.ds.Tables[0].Rows[iCurrentRow];
			else
				drCurrent = SysDataMgrDAL.GetEmptyCustomerProfileDS(1).ds.Tables[0].Rows[iCurrentRow];

			msTextBox txtAccNo = (msTextBox)customerProfileMultiPage.FindControl("txtAccNo");

			if(txtAccNo != null && txtAccNo.Text != "")
			{
				drCurrent["custid"] = txtAccNo.Text;
			}

			msTextBox txtRefNo = (msTextBox)customerProfileMultiPage.FindControl("txtRefNo");

			if(txtRefNo != null)// && txtRefNo.Text != "")
			{
				drCurrent["ref_code"] = txtRefNo.Text;
			}

			TextBox txtCustomerName = (TextBox)customerProfileMultiPage.FindControl("txtCustomerName");

			if(txtCustomerName != null && txtCustomerName.Text != "")
			{
				drCurrent["cust_name"] = txtCustomerName.Text;
			}

			TextBox txtContactPerson = (TextBox)customerProfileMultiPage.FindControl("txtContactPerson");

			if(txtContactPerson != null)// && txtContactPerson.Text != "")
			{
				drCurrent["contact_person"] = txtContactPerson.Text;
			}

			TextBox txtAddress1 = (TextBox)customerProfileMultiPage.FindControl("txtAddress1");

			if(txtAddress1 != null && txtAddress1.Text != "")
			{
				drCurrent["address1"] = txtAddress1.Text;
			}

			TextBox txtAddress2 = (TextBox)customerProfileMultiPage.FindControl("txtAddress2");

			if(txtAddress2 != null)// && txtAddress2.Text != "")
			{
				drCurrent["address2"] = txtAddress2.Text;
			}

			TextBox txtCountry = (TextBox)customerProfileMultiPage.FindControl("txtCountry");

			if(txtCountry != null && txtCountry.Text != "")
			{
				drCurrent["country"] = txtCountry.Text;
			}

			msTextBox txtZipCode = (msTextBox)customerProfileMultiPage.FindControl("txtZipCode");

			if(txtZipCode != null && txtZipCode.Text != "")
			{
				drCurrent["zipcode"] = txtZipCode.Text;

				com.ties.classes.Zipcode objZipcode = new com.ties.classes.Zipcode();
				objZipcode.Populate(utility.GetAppID(),utility.GetEnterpriseID(), txtZipCode.Text.Trim());
				drCurrent["state_code"] = objZipcode.StateCode;
			}

			TextBox txtTelephone = (TextBox)customerProfileMultiPage.FindControl("txtTelephone");

			if(txtTelephone != null && txtTelephone.Text != "")
			{
				drCurrent["telephone"] = txtTelephone.Text;
			}

			TextBox txtMBG = (TextBox)customerProfileMultiPage.FindControl("txtMBG");

			if(txtMBG != null && txtMBG.Text != "")
			{
				drCurrent["mbg"] = txtMBG.Text;
			}

			TextBox txtFax = (TextBox)customerProfileMultiPage.FindControl("txtFax");

			if(txtFax != null)// && txtFax.Text != "")
			{
				drCurrent["fax"] = txtFax.Text;
			}

			//			msTextBox txtCreditTerm = (msTextBox)customerProfileMultiPage.FindControl("txtCreditTerm");
			//
			//			if(txtCreditTerm != null && txtCreditTerm.Text != "")
			//			{
			//				drCurrent["credit_term"] = txtCreditTerm.Text;
			//			}

			msTextBox txtISectorCode = (msTextBox)customerProfileMultiPage.FindControl("txtISectorCode");

			if(txtISectorCode != null)// && txtISectorCode.Text != "")
			{
				drCurrent["industrial_sector_code"] = txtISectorCode.Text;
			}

			msTextBox txtCreditLimit = (msTextBox)customerProfileMultiPage.FindControl("txtCreditLimit");

			if(txtCreditLimit != null && txtCreditLimit.Text != "")
			{
				drCurrent["credit_limit"] = txtCreditLimit.Text;

				decimal decMaxAmt = Convert.ToDecimal(drCurrent["credit_limit"]);
				txtCreditLimit.Text = String.Format((String)ViewState["m_format"], decMaxAmt);
			}

			msTextBox txtCreditUsed = (msTextBox)customerProfileMultiPage.FindControl("txtCreditUsed");

			if(txtCreditUsed != null && txtCreditUsed.Text != "")
			{
				drCurrent["credit_used"] = txtCreditUsed.Text;
				decimal decMaxAmt = Convert.ToDecimal(drCurrent["credit_used"]);
				txtCreditUsed.Text = String.Format((String)ViewState["m_format"], decMaxAmt);
			}

			msTextBox txtCreditTerm = (msTextBox)customerProfileMultiPage.FindControl("txtCreditTerm");

			if(txtCreditTerm != null && txtCreditTerm.Text != "")
			{
				drCurrent["credit_term"] = txtCreditTerm.Text;
			}

			msTextBox txtCreditStatus = (msTextBox)customerProfileMultiPage.FindControl("txtCreditStatus");

			if(txtCreditStatus != null && txtCreditStatus.Text != "")
			{
				if(txtCreditStatus.Text=="AVAILABLE")
				{
					drCurrent["creditstatus"] = "A";
				}
				else
				{
					drCurrent["creditstatus"] = "N";
				}
				
			}
			msTextBox txtCreditThredholds = (msTextBox)customerProfileMultiPage.FindControl("txtCreditThredholds");

			if(txtCreditThredholds != null && txtCreditThredholds.Text != "")
			{
				drCurrent["creditthreshold"] = txtCreditThredholds.Text;
			}



			msTextBox txtActiveQuatationNo = (msTextBox)customerProfileMultiPage.FindControl("txtActiveQuatationNo");

			if(txtActiveQuatationNo != null)// && txtActiveQuatationNo.Text != "")
			{
				drCurrent["active_quotation_no"] = txtActiveQuatationNo.Text;
			}


			msTextBox txtCreditOutstanding = (msTextBox)customerProfileMultiPage.FindControl("txtCreditOutstanding");

			if(txtCreditOutstanding != null && txtCreditOutstanding.Text != "")
			{
				drCurrent["credit_outstanding"] = txtCreditOutstanding.Text;
			}

			TextBox txtRemark = (TextBox)customerProfileMultiPage.FindControl("txtRemark");

			if(txtRemark != null && txtRemark.Text != "")
			{
				drCurrent["remark"] = txtRemark.Text;
			}

			TextBox txtSalesmanID = (TextBox)customerProfileMultiPage.FindControl("txtSalesmanID");

			if(txtSalesmanID != null && txtSalesmanID.Text != "")
			{
				drCurrent["SalesmanID"] = txtSalesmanID.Text;
			}

			msTextBox txtPromisedTotWt = (msTextBox)customerProfileMultiPage.FindControl("txtPromisedTotWt");


			if(txtPromisedTotWt != null && txtPromisedTotWt.Text != "")
			{
				drCurrent["prom_tot_wt"] = txtPromisedTotWt.Text;
			}

			RadioButton radioBtnCash = (RadioButton)customerProfileMultiPage.FindControl("radioBtnCash");

			String strPaymentMode = "";
			if(radioBtnCash != null && radioBtnCash.Checked == true)
			{
				strPaymentMode = "C";
			}
			RadioButton radioBtnCredit = (RadioButton)customerProfileMultiPage.FindControl("radioBtnCredit");

			if(radioBtnCredit != null && radioBtnCredit.Checked == true)
			{
				strPaymentMode = "R";
			}
			
			if(strPaymentMode != "")
			{
				drCurrent["payment_mode"] = strPaymentMode;
			}

			msTextBox txtPromisedTotPkg = (msTextBox)customerProfileMultiPage.FindControl("txtPromisedTotPkg");

			if(txtPromisedTotPkg != null && txtPromisedTotPkg.Text != "")
			{
				drCurrent["prom_tot_package"] = txtPromisedTotPkg.Text;
			}

			msTextBox txtPromisedPeriod = (msTextBox)customerProfileMultiPage.FindControl("txtPromisedPeriod");


			if(txtPromisedPeriod != null && txtPromisedPeriod.Text != "")
			{
				drCurrent["prom_period"] = txtPromisedPeriod.Text;
			}
			
			msTextBox txtFreeInsuranceAmt = (msTextBox)customerProfileMultiPage.FindControl("txtFreeInsuranceAmt");

			if(txtFreeInsuranceAmt != null && txtFreeInsuranceAmt.Text != "")
			{
				drCurrent["free_insurance_amt"] = txtFreeInsuranceAmt.Text;
			}
			else
			{
				drCurrent["free_insurance_amt"] = System.DBNull.Value;
			}

			msTextBox txtInsurancePercentSurcharge = (msTextBox)customerProfileMultiPage.FindControl("txtInsurancePercentSurcharge");

			if(txtInsurancePercentSurcharge != null && txtInsurancePercentSurcharge.Text != "")
			{
				drCurrent["insurance_percent_surcharge"] = txtInsurancePercentSurcharge.Text;
			}
			else
			{
				drCurrent["insurance_percent_surcharge"] = System.DBNull.Value;
			}

			// for Invoice Return Days
			msTextBox txtInvoiceReturn = (msTextBox)customerProfileMultiPage.FindControl("txtInvoiceReturn");

			if(txtInvoiceReturn != null && txtInvoiceReturn.Text != "")
			{
				drCurrent["invoice_return_days"] = txtInvoiceReturn.Text;
			}


			TextBox txtEmail = (TextBox)customerProfileMultiPage.FindControl("txtEmail");

			if(txtEmail != null)// && txtEmail.Text != "")
			{
				drCurrent["email"] = txtEmail.Text;
			}

			TextBox txtCreatedBy = (TextBox)customerProfileMultiPage.FindControl("txtCreatedBy");

			if(txtCreatedBy != null && txtCreatedBy.Text != "")
			{
				drCurrent["created_by"] = txtCreatedBy.Text;
			}

			DropDownList ddlApplyDimWt = (DropDownList)customerProfileMultiPage.FindControl("ddlApplyDimWt");
			
			if(ddlApplyDimWt != null)
			{
				String strApplyDimWt = ddlApplyDimWt.SelectedItem.Value;
				if(strApplyDimWt != "")
				{
					drCurrent["apply_dim_wt"] = strApplyDimWt;
				}
			}

			if(conf.HCSupportedbyEnterprise == true)
			{
				DropDownList ddlPodSlipRequired = (DropDownList)customerProfileMultiPage.FindControl("ddlPodSlipRequired");
			
				if(ddlPodSlipRequired != null)
				{
					String strPodSlipRequired = ddlPodSlipRequired.SelectedItem.Value;
					if(strPodSlipRequired != "")
					{
						drCurrent["pod_slip_required"] = strPodSlipRequired;
					}
				}


				//HC Return Task
				DropDownList ddlInvoiceRequried = (DropDownList)customerProfileMultiPage.FindControl("ddlInvoiceRequried");
			
				if(ddlInvoiceRequried != null)
				{
					String strInvoiceRequired = ddlInvoiceRequried.SelectedItem.Value;
					if(strInvoiceRequired != "")
					{
						drCurrent["hc_invoice_required"] = strInvoiceRequired;
					}
				}
				//HC Return Task
			}



			DropDownList ddlApplyEsaSurcharge = (DropDownList)customerProfileMultiPage.FindControl("ddlApplyEsaSurcharge");
			
			if(ddlApplyEsaSurcharge != null)
			{
				String strApplyEsaSurcharge = ddlApplyEsaSurcharge.SelectedItem.Value;

				if(strApplyEsaSurcharge != "")
				{
					drCurrent["apply_esa_surcharge"] = strApplyEsaSurcharge;
				}
			}

			//Fill ESA Surcharge Delivery to ["apply_esa_surcharge_del"]
			DropDownList ddlApplyEsaSurchargeDel = (DropDownList)customerProfileMultiPage.FindControl("ddlApplyEsaSurchargeDel");
			
			if(ddlApplyEsaSurchargeDel != null)
			{
				String strApplyEsaSurchargeDel = ddlApplyEsaSurchargeDel.SelectedItem.Value;

				if(strApplyEsaSurchargeDel != "")
				{
					drCurrent["apply_esa_surcharge_rep"] = strApplyEsaSurchargeDel;
				}
			}

			DropDownList ddlStatus = (DropDownList)customerProfileMultiPage.FindControl("ddlStatus");
			
			if(ddlStatus != null)
			{
				String strStatus = ddlStatus.SelectedItem.Value;
				if(strStatus != "")
				{
					drCurrent["status_active"] = strStatus;
				}
			}

			DropDownList ddlMBG = (DropDownList)customerProfileMultiPage.FindControl("ddlMBG");
			
			if(ddlMBG != null)
			{
				String strMBG = ddlMBG.SelectedItem.Value;
				if(strMBG != "")
				{
					drCurrent["mbg"] = strMBG;
				}
			}
			//Jeab 8 Dec 2010
			DropDownList ddlDimByTOT = (DropDownList)customerProfileMultiPage.FindControl("ddlDimByTOT");

			if(ddlDimByTOT != null)
			{
				String strMDim_By_TOT = ddlDimByTOT.SelectedItem.Value;
				if(strMDim_By_TOT != "")
				{
					drCurrent["Dim_By_TOT"] = strMDim_By_TOT;
				}
			}
			//Jeab 8 Dec 2010  =========> End

			//Jeab 08 Mar  2012
			TextBox txtFirstShipDate  = (TextBox)customerProfileMultiPage.FindControl("txtFirstShipDate");

			if(txtFirstShipDate != null && txtFirstShipDate.Text != "")
			{
				drCurrent["Commission_Datetime"] = DateTime.ParseExact(txtFirstShipDate.Text.Trim(),"dd/MM/yyyy",null);
			}
			else
			{
				drCurrent["Commission_Datetime"] = DateTime.Now.ToString("dd/MM/yyyy");
			}

			TextBox txtCategory  = (TextBox)customerProfileMultiPage.FindControl("txtCategory");

			if(txtCategory != null && txtCategory.Text != "")
			{
				drCurrent["custCategory"] = txtCategory.Text;
			}
			else
			{
				drCurrent["custCategory"] = "New";
			}

			//Jeab 08 Mar  2012  =========>  End

			//			DropDownList ddlCreditTerm = (DropDownList)customerProfileMultiPage.FindControl("ddlCreditTerm");
			//
			//			if(ddlCreditTerm != null)
			//			{
			//				String strCreditTerm = ddlCreditTerm.SelectedItem.Value;
			//				if(strCreditTerm != "")
			//				{
			//					drCurrent["credit_term"] = strCreditTerm;
			//				}
			//				else
			//				{
			//					drCurrent["credit_term"] = System.DBNull.Value;
			//				}
			//			}

			msTextBox txtMaxAmt = (msTextBox)customerProfileMultiPage.FindControl("txtMaxAmt");

			if(txtMaxAmt != null && txtMaxAmt.Text != "")
			{
				drCurrent["insurance_maximum_amt"] = txtMaxAmt.Text;
			}
			else
			{
				drCurrent["insurance_maximum_amt"] = System.DBNull.Value;
			}

			msTextBox txtCODSurchargeAmt = (msTextBox)customerProfileMultiPage.FindControl("txtCODSurchargeAmt");

			if(txtCODSurchargeAmt != null && txtCODSurchargeAmt.Text != "")
			{
				drCurrent["cod_surcharge_amt"] = txtCODSurchargeAmt.Text;
			}
			else
			{
				drCurrent["cod_surcharge_amt"] = System.DBNull.Value;
			}

			msTextBox txtCODSurchargePercent = (msTextBox)customerProfileMultiPage.FindControl("txtCODSurchargePercent");

			if(txtCODSurchargePercent != null && txtCODSurchargePercent.Text != "")
			{
				drCurrent["cod_surcharge_percent"] = txtCODSurchargePercent.Text;
			}
			else
			{
				drCurrent["cod_surcharge_percent"] = System.DBNull.Value;
			}
			
			// by Ching Nov 28,2007
			msTextBox txtMaxCODSurcharge = (msTextBox)customerProfileMultiPage.FindControl("txtMaxCODSurcharge");

			if(txtMaxCODSurcharge != null && txtMaxCODSurcharge.Text != "")
			{
				drCurrent["max_cod_surcharge"] = txtMaxCODSurcharge.Text;
			}
			else
			{
				drCurrent["max_cod_surcharge"] = System.DBNull.Value;
			}
			
			msTextBox txtMinCODSurcharge = (msTextBox)customerProfileMultiPage.FindControl("txtMinCODSurcharge");

			if(txtMinCODSurcharge != null && txtMinCODSurcharge.Text != "")
			{
				drCurrent["min_cod_surcharge"] = txtMinCODSurcharge.Text;
			}
			else
			{
				drCurrent["min_cod_surcharge"] = System.DBNull.Value;
			}

			// by Aoo 11/02/2008
			msTextBox msOtherSurchargeAmount = (msTextBox)customerProfileMultiPage.FindControl("msOtherSurchargeAmount");
			if(msOtherSurchargeAmount != null && msOtherSurchargeAmount.Text != "")
			{
				drCurrent["other_surcharge_amount"]= msOtherSurchargeAmount.Text;
				//Response.Write("<script>alert('"+ drCurrent["other_surcharge_amount"].ToString() +"');</script>");
			}
			else
			{
				drCurrent["other_surcharge_amount"] = System.DBNull.Value;	
			}
			msTextBox msOtherSurchargePercentage = (msTextBox)customerProfileMultiPage.FindControl("msOtherSurchargePercentage");
			if(msOtherSurchargePercentage != null && msOtherSurchargePercentage.Text != "")
			{
				drCurrent["other_surcharge_percentage"] = msOtherSurchargePercentage.Text;
			}
			else
			{
				drCurrent["other_surcharge_percentage"] = System.DBNull.Value;	
				
			}
			msTextBox msOtherSurchargeMinimum = (msTextBox)customerProfileMultiPage.FindControl("msOtherSurchargeMinimum");
			if(msOtherSurchargeMinimum != null && msOtherSurchargeMinimum.Text != "")
			{
				
				drCurrent["other_surcharge_min"] = msOtherSurchargeMinimum.Text;
			}
			else
			{
				drCurrent["other_surcharge_min"] = System.DBNull.Value;	
			}
			msTextBox msOtherSurchargeMaximum = (msTextBox)customerProfileMultiPage.FindControl("msOtherSurchargeMaximum");
			if(msOtherSurchargeMaximum != null && msOtherSurchargeMaximum.Text != "")
			{
				drCurrent["other_surcharge_max"] = msOtherSurchargeMaximum.Text;
			}
			else
			{
				drCurrent["other_surcharge_max"] = System.DBNull.Value;	
			
			}
			msTextBox msOtherSurchargeDescription = (msTextBox)customerProfileMultiPage.FindControl("msOtherSurchargeDescription");
			if(msOtherSurchargeDescription != null && msOtherSurchargeDescription.Text != "")
			{
				drCurrent["other_surcharge_desc"] = msOtherSurchargeDescription.Text;
			}
			else
			{
				drCurrent["other_surcharge_desc"] = System.DBNull.Value;	
				
			}

			msTextBox txtMaxInsuranceSurcharge = (msTextBox)customerProfileMultiPage.FindControl("txtMaxInsuranceSurcharge");
			if(txtMaxInsuranceSurcharge != null && txtMaxInsuranceSurcharge.Text != "")
			{
				
				drCurrent["max_insurance_surcharge"] = txtMaxInsuranceSurcharge.Text;
			}
			else
			{
				drCurrent["max_insurance_surcharge"] = System.DBNull.Value;	
			}
			msTextBox txtMinInsuranceSurcharge = (msTextBox)customerProfileMultiPage.FindControl("txtMinInsuranceSurcharge");
			if(txtMinInsuranceSurcharge != null && txtMinInsuranceSurcharge.Text != "")
			{
				drCurrent["min_insurance_surcharge"] = txtMinInsuranceSurcharge.Text;
			}
			else
			{
				drCurrent["min_insurance_surcharge"] = System.DBNull.Value;	
			}

			msTextBox msDiscountBand = (msTextBox)customerProfileMultiPage.FindControl("msDiscountBand");
			if(msDiscountBand != null && msDiscountBand.Text != "")
			{
				drCurrent["discount_band"] = msDiscountBand.Text ;
			}
			else
			{
				drCurrent["discount_band"] = System.DBNull.Value;	
			}
			

			msTextBox msDensityFactor = (msTextBox)customerProfileMultiPage.FindControl("msDensityFactor");
			
			//			Response.Write("<script>alert('"+drCurrent["density_factor"].ToString()+"');</script>");

			if(msDensityFactor != null && msDensityFactor.Text != "")
			{
				drCurrent["density_factor"] = Convert.ToDecimal(msDensityFactor.Text);
			}
			else
			{
				drCurrent["density_factor"] = System.DBNull.Value;
			}
			// End
			

			DropDownList ddlDensityFactor = (DropDownList)customerProfileMultiPage.FindControl("ddlDensityFactor");
			
			if(ddlDensityFactor != null)
			{
				String strDensityFactor = ddlDensityFactor.SelectedItem.Value;
				if(strDensityFactor != "")
				{
					drCurrent["density_factor"] = strDensityFactor;
				}
			}

			DropDownList ddlCustomerType = (DropDownList)customerProfileMultiPage.FindControl("ddlCustomerType");
			
			if(ddlCustomerType != null)
			{
				String strCustomerType = ddlCustomerType.SelectedItem.Value;
				if(strCustomerType != "")
				{
					drCurrent["payer_type"] = strCustomerType;
				}
			}
			//add by X FEB 19 09
			DbCombo DbComboMasterAccount = (DbCombo)customerProfileMultiPage.FindControl("DbComboMasterAccount");			
			if(DbComboMasterAccount != null)
			{
				String strMasterAccount = DbComboMasterAccount.Value;
				if(strMasterAccount != "")
				{
					drCurrent["master_account"] = strMasterAccount;
				}
			}
			
			CheckBox CustBox = (CheckBox)customerProfileMultiPage.FindControl("ChkBox");
			
			if(CustBox != null)
			{
				String strCustBox="";
				if(CustBox.Checked)
				{
					strCustBox = "Y";
				}
				else 
				{
					strCustBox = "N";
				}
				if(strCustBox != "")
				{
					drCurrent["CustomerBox"] = strCustBox;
				}
			}
			//Added By Tom 22/7/09
			TextBox txtSpecialInstruction = (TextBox)customerProfileMultiPage.FindControl("txtSpecialInstruction");
			TextBox txtMinimumBox = (TextBox)customerProfileMultiPage.FindControl("txtMinimumBox");

			if(txtSpecialInstruction != null && txtSpecialInstruction.Text != "")
			{
				drCurrent["remark2"] = txtSpecialInstruction.Text;
			}
			//End Added By Tom 22/7/09
			msTextBox txtNextBillPlacementDate = (msTextBox)customerProfileMultiPage.FindControl("txtNextBillPlacementDate");

			if(txtNextBillPlacementDate != null && txtNextBillPlacementDate.Text != "")
			{
				drCurrent["next_bill_placement_date"] = DateTime.ParseExact(txtNextBillPlacementDate.Text.Trim(),"dd/MM/yyyy",null);
			}
			if(txtMinimumBox != null && txtMinimumBox.Text != "")
			{
				drCurrent["Minimum_Box"] = Convert.ToInt32(txtMinimumBox.Text);
			}
			else
			{
				drCurrent["Minimum_Box"] = System.DBNull.Value;
			}

			//drCurrent.AcceptChanges();
		}


		private DataView GetApplyDimWtOptions(bool showNilOption) 
		{
			DataTable dtApplyDimWtOptions = new DataTable();
 
			dtApplyDimWtOptions.Columns.Add(new DataColumn("Text", typeof(string)));
			dtApplyDimWtOptions.Columns.Add(new DataColumn("StringValue", typeof(string)));

			ArrayList applyDimWtOptionArray = Utility.GetCodeValues(utility.GetAppID(),utility.GetUserCulture(),"apply_dim_wt",CodeValueType.StringValue);

			if(showNilOption)
			{
				DataRow drNilRow = dtApplyDimWtOptions.NewRow();
				drNilRow[0] = "";
				drNilRow[1] = "";
				dtApplyDimWtOptions.Rows.Add(drNilRow);
			}

			foreach(SystemCode applyDimWtSysCode in applyDimWtOptionArray)
			{
				DataRow drEach = dtApplyDimWtOptions.NewRow();
				drEach[0] = applyDimWtSysCode.Text;
				drEach[1] = applyDimWtSysCode.StringValue;
				dtApplyDimWtOptions.Rows.Add(drEach);
			}

			DataView dvApplyDimWtOptions = new DataView(dtApplyDimWtOptions);
			return dvApplyDimWtOptions;
		}

		private DataView GetPODSlipRequirdOptions(bool showNilOption) 
		{
			DataTable dtPODSlipRequirdOptions = new DataTable();
 
			dtPODSlipRequirdOptions.Columns.Add(new DataColumn("Text", typeof(string)));
			dtPODSlipRequirdOptions.Columns.Add(new DataColumn("StringValue", typeof(string)));

			ArrayList podSlipRequirdOptionArray = Utility.GetCodeValues(utility.GetAppID(),utility.GetUserCulture(),"pod_slip_required",CodeValueType.StringValue);

			if(showNilOption)
			{
				DataRow drNilRow = dtPODSlipRequirdOptions.NewRow();
				drNilRow[0] = "";
				drNilRow[1] = "";
				dtPODSlipRequirdOptions.Rows.Add(drNilRow);
			}

			foreach(SystemCode podSlipRequirdSysCode in podSlipRequirdOptionArray)
			{
				DataRow drEach = dtPODSlipRequirdOptions.NewRow();
				drEach[0] = podSlipRequirdSysCode.Text;
				drEach[1] = podSlipRequirdSysCode.StringValue;
				dtPODSlipRequirdOptions.Rows.Add(drEach);
			}

			DataView dvPODSlipRequirdOptions = new DataView(dtPODSlipRequirdOptions);
			return dvPODSlipRequirdOptions;
		}

		private DataView GetInvoiceRequirdOptions(bool showNilOption) 
		{
			DataTable dtInvoiceRequirdOptions = new DataTable();
 
			dtInvoiceRequirdOptions.Columns.Add(new DataColumn("Text", typeof(string)));
			dtInvoiceRequirdOptions.Columns.Add(new DataColumn("StringValue", typeof(string)));

			ArrayList InvoiceRequirdOptionArray = Utility.GetCodeValues(utility.GetAppID(),utility.GetUserCulture(),"hc_invoice_required",CodeValueType.StringValue);

			if(showNilOption)
			{
				DataRow drNilRow = dtInvoiceRequirdOptions.NewRow();
				drNilRow[0] = "";
				drNilRow[1] = "";
				dtInvoiceRequirdOptions.Rows.Add(drNilRow);
			}
	


			foreach(SystemCode InvoiceRequirdSysCode in InvoiceRequirdOptionArray)
			{
				DataRow drEach = dtInvoiceRequirdOptions.NewRow();
				drEach[0] = InvoiceRequirdSysCode.Text;
				drEach[1] = InvoiceRequirdSysCode.StringValue;
				dtInvoiceRequirdOptions.Rows.Add(drEach);
			}

			DataView dvInvoiceRequirdOptions = new DataView(dtInvoiceRequirdOptions);
			return dvInvoiceRequirdOptions;
		}

		private DataView GetApplyESASurchargeOptions(bool showNilOption) 
		{
			
			DataTable dtApplyESASurchargeOptions = new DataTable();
			
			dtApplyESASurchargeOptions.Columns.Add(new DataColumn("Text", typeof(string)));
			dtApplyESASurchargeOptions.Columns.Add(new DataColumn("StringValue", typeof(string)));

			ArrayList applyESASurchargeOptionArray = Utility.GetCodeValues(utility.GetAppID(),utility.GetUserCulture(),"apply_esa_surcharge",CodeValueType.StringValue);


			if(showNilOption)
			{
				DataRow drNilRow = dtApplyESASurchargeOptions.NewRow();
				drNilRow[0] = "";
				drNilRow[1] = "";
				dtApplyESASurchargeOptions.Rows.Add(drNilRow);
			}
	


			foreach(SystemCode applyESASurchargeSysCode in applyESASurchargeOptionArray)
			{
				DataRow drEach = dtApplyESASurchargeOptions.NewRow();
				drEach[0] = applyESASurchargeSysCode.Text;
				drEach[1] = applyESASurchargeSysCode.StringValue;
				dtApplyESASurchargeOptions.Rows.Add(drEach);
			}

			DataView dvApplyESASurchargeOptions = new DataView(dtApplyESASurchargeOptions);
			return dvApplyESASurchargeOptions;
		}

		private DataView GetStatusOptions(bool showNilOption) 
		{
			
			DataTable dtStatusOptions = new DataTable();
			
			dtStatusOptions.Columns.Add(new DataColumn("Text", typeof(string)));
			dtStatusOptions.Columns.Add(new DataColumn("StringValue", typeof(string)));

			ArrayList statusOptionArray = Utility.GetCodeValues(utility.GetAppID(),utility.GetUserCulture(),"active_status",CodeValueType.StringValue);


			if(showNilOption)
			{
				DataRow drNilRow = dtStatusOptions.NewRow();
				drNilRow[0] = "";
				drNilRow[1] = "";
				dtStatusOptions.Rows.Add(drNilRow);
			}
	


			foreach(SystemCode statusSysCode in statusOptionArray)
			{
				DataRow drEach = dtStatusOptions.NewRow();
				drEach[0] = statusSysCode.Text;
				drEach[1] = statusSysCode.StringValue;
				dtStatusOptions.Rows.Add(drEach);
			}

			DataView dvStatusOptions = new DataView(dtStatusOptions);
			return dvStatusOptions;
		}

		private DataView GetMBGOptions(bool showNilOption) 
		{
			
			DataTable dtMBGOptions = new DataTable();
			
			dtMBGOptions.Columns.Add(new DataColumn("Text", typeof(string)));
			dtMBGOptions.Columns.Add(new DataColumn("StringValue", typeof(string)));

			ArrayList mbgOptionArray = Utility.GetCodeValues(utility.GetAppID(),utility.GetUserCulture(),"mbg",CodeValueType.StringValue);


			if(showNilOption)
			{
				DataRow drNilRow = dtMBGOptions.NewRow();
				drNilRow[0] = "";
				drNilRow[1] = "";
				dtMBGOptions.Rows.Add(drNilRow);
			}
	


			foreach(SystemCode mbgSysCode in mbgOptionArray)
			{
				DataRow drEach = dtMBGOptions.NewRow();
				drEach[0] = mbgSysCode.Text;
				drEach[1] = mbgSysCode.StringValue;
				dtMBGOptions.Rows.Add(drEach);
			}

			DataView dvMBGOptions = new DataView(dtMBGOptions);
			return dvMBGOptions;
		}
		//Jeab 8 Dec 2010
		private DataView GetDim_By_TOTOptions(bool showNilOption) 
		{
			
			DataTable dtDim_By_TOTOptions = new DataTable();
			
			dtDim_By_TOTOptions.Columns.Add(new DataColumn("Text", typeof(string)));
			dtDim_By_TOTOptions.Columns.Add(new DataColumn("StringValue", typeof(string)));

			ArrayList Dim_By_TOTOptionArray = Utility.GetCodeValues(utility.GetAppID(),utility.GetUserCulture(),"Dim_By_TOT",CodeValueType.StringValue);


			if(showNilOption)
			{
				DataRow drNilRow = dtDim_By_TOTOptions.NewRow();
				drNilRow[0] = "";
				drNilRow[1] = "";
				dtDim_By_TOTOptions.Rows.Add(drNilRow);
			}

			foreach(SystemCode Dim_By_TOTSysCode in Dim_By_TOTOptionArray)
			{
				DataRow drEach = dtDim_By_TOTOptions.NewRow();
				drEach[0] = Dim_By_TOTSysCode.Text;
				drEach[1] = Dim_By_TOTSysCode.StringValue;
				dtDim_By_TOTOptions.Rows.Add(drEach);
			}

			DataView dvDim_By_TOTOptions = new DataView(dtDim_By_TOTOptions);
			return dvDim_By_TOTOptions;
		}
		//Jeab 8 Dec 2010  =========> End

		private DataView GetDensityFactorOptions(bool showNilOption) 
		{
			
			DataTable dtDensityFactorOptions = new DataTable();
			
			dtDensityFactorOptions.Columns.Add(new DataColumn("Text", typeof(string)));
			dtDensityFactorOptions.Columns.Add(new DataColumn("StringValue", typeof(string)));

			ArrayList mbgOptionArray = Utility.GetCodeValues(utility.GetAppID(),utility.GetUserCulture(),"density_factor",CodeValueType.StringValue);


			if(showNilOption)
			{
				DataRow drNilRow = dtDensityFactorOptions.NewRow();
				drNilRow[0] = "";
				drNilRow[1] = "";
				dtDensityFactorOptions.Rows.Add(drNilRow);
			}
	


			foreach(SystemCode mbgSysCode in mbgOptionArray)
			{
				DataRow drEach = dtDensityFactorOptions.NewRow();
				drEach[0] = mbgSysCode.Text;
				drEach[1] = mbgSysCode.StringValue;
				dtDensityFactorOptions.Rows.Add(drEach);
			}

			DataView dvDensityFactorOptions = new DataView(dtDensityFactorOptions);
			return dvDensityFactorOptions;
		}


		private DataView GetCustomerTypeOptions(bool showNilOption) 
		{
			
			DataTable dtCustomerTypeOptions = new DataTable();
			
			dtCustomerTypeOptions.Columns.Add(new DataColumn("Text", typeof(string)));
			dtCustomerTypeOptions.Columns.Add(new DataColumn("StringValue", typeof(string)));

			ArrayList mbgOptionArray = Utility.GetCodeValues(utility.GetAppID(),utility.GetUserCulture(),"customer_type",CodeValueType.StringValue);


			if(showNilOption)
			{
				DataRow drNilRow = dtCustomerTypeOptions.NewRow();
				drNilRow[0] = "";
				drNilRow[1] = "";
				dtCustomerTypeOptions.Rows.Add(drNilRow);
			}
	


			foreach(SystemCode mbgSysCode in mbgOptionArray)
			{
				DataRow drEach = dtCustomerTypeOptions.NewRow();
				drEach[0] = mbgSysCode.Text;
				drEach[1] = mbgSysCode.StringValue;
				dtCustomerTypeOptions.Rows.Add(drEach);
			}

			DataView dvCustomerTypeOptions = new DataView(dtCustomerTypeOptions);
			return dvCustomerTypeOptions;
		}

		#region Bind DropDownList Aoo-Panas
		//Begin By Aoo-Panas
		private DataView GetCreditTerm(bool showNilOption)
		{
			DataTable dtCreditTerm = new DataTable();

			dtCreditTerm.Columns.Add(new DataColumn("Text",typeof(string)));
			dtCreditTerm.Columns.Add(new DataColumn("StringValue",typeof(string)));

			ArrayList mbgOptionArray = Utility.GetCodeValues(utility.GetAppID(),utility.GetUserCulture(),"customer_credit_term",CodeValueType.StringValue);
			
			if(showNilOption)
			{
				DataRow drNilRow = dtCreditTerm.NewRow();
				drNilRow[0] = "";
				drNilRow[1] = "";
				dtCreditTerm.Rows.Add(drNilRow);
			}
		
			foreach(SystemCode mbgSysCode in mbgOptionArray)
			{
				DataRow drEach = dtCreditTerm.NewRow();
				drEach[0] = mbgSysCode.Text;
				drEach[1] = mbgSysCode.StringValue;
				dtCreditTerm.Rows.Add(drEach);
			}
		
			DataView dvCreditTerm = new DataView(dtCreditTerm);
			return dvCreditTerm;
		}
		//End By Aoo-Panas
		#endregion

		private void LoadComboLists()
		{

			if((int) ViewState["CPMode"] == (int)ScreenMode.Query)
			{
				m_dvApplyDimWtOptions = GetApplyDimWtOptions(true);
				m_dvPODSlipRequiredOptions = GetPODSlipRequirdOptions(true);
				m_dvInvoiceRequiredOptions = GetInvoiceRequirdOptions(true);
				m_dvApplyESASurchargeOptions = GetApplyESASurchargeOptions(true);
				m_dvStatusOptions = GetStatusOptions(true);
				m_dvMBGOptions = GetMBGOptions(true);
				m_dvDim_By_TOTOptions = GetDim_By_TOTOptions(true);  //Jeab 8 Dec 2010
				m_dvDensityFactorOptions = GetDensityFactorOptions(true);
				m_dvCustomerTypeOptions = GetCustomerTypeOptions(true);
				m_dvCreditTerm = GetCreditTerm(true);
			}
			else if ((int) ViewState["CPMode"] == (int)ScreenMode.Insert)
			{
				m_dvApplyDimWtOptions = GetApplyDimWtOptions(true);
				m_dvPODSlipRequiredOptions = GetPODSlipRequirdOptions(true);
				m_dvInvoiceRequiredOptions = GetInvoiceRequirdOptions(true);
				m_dvApplyESASurchargeOptions = GetApplyESASurchargeOptions(true);
				m_dvMBGOptions = GetMBGOptions(true);
				m_dvDim_By_TOTOptions = GetDim_By_TOTOptions(true);  //Jeab 8 Dec 2010
				m_dvStatusOptions = GetStatusOptions(false);
				m_dvDensityFactorOptions = GetDensityFactorOptions(true);
				m_dvCustomerTypeOptions = GetCustomerTypeOptions(true);
				m_dvCreditTerm = GetCreditTerm(true);
			}
			else if((int) ViewState["CPMode"] == (int)ScreenMode.ExecuteQuery)
			{
				m_dvApplyDimWtOptions = GetApplyDimWtOptions(false);
				m_dvPODSlipRequiredOptions = GetPODSlipRequirdOptions(false);
				m_dvInvoiceRequiredOptions = GetInvoiceRequirdOptions(false);
				m_dvApplyESASurchargeOptions = GetApplyESASurchargeOptions(false);
				m_dvMBGOptions = GetMBGOptions(false);
				m_dvDim_By_TOTOptions = GetDim_By_TOTOptions(false);  //Jeab 8 Dec 2010
				m_dvStatusOptions = GetStatusOptions(false);
				m_dvDensityFactorOptions = GetDensityFactorOptions(true);
				m_dvCustomerTypeOptions = GetCustomerTypeOptions(true);
				m_dvCreditTerm = GetCreditTerm(true);
			}

		}

		private void BindComboLists()
		{
			DataRow drCurrent = m_sdsCustomerProfile.ds.Tables[0].Rows[Convert.ToInt32(ViewState["currentPage"])];

			DropDownList ddlApplyDimWt = (DropDownList)customerProfileMultiPage.FindControl("ddlApplyDimWt");
			
			if(ddlApplyDimWt != null)
			{
				ddlApplyDimWt.DataSource = (ICollection)m_dvApplyDimWtOptions;
				ddlApplyDimWt.DataTextField = "Text";
				ddlApplyDimWt.DataValueField = "StringValue";
				ddlApplyDimWt.DataBind();


				if((drCurrent["apply_dim_wt"]!= null) && (!drCurrent["apply_dim_wt"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					String strApplyDimWt = (String)drCurrent["apply_dim_wt"];
					ddlApplyDimWt.SelectedIndex = ddlApplyDimWt.Items.IndexOf(ddlApplyDimWt.Items.FindByValue(strApplyDimWt));//.FindByValue("N"));
				}
				//Logger.LogDebugInfo("CustomerProfile","BindComboLists","INF006","Selecting Apply......"+e.Item.ItemIndex+" Selected Index is : "+ddlSCCloseStatus.SelectedIndex+"   "+strCloseStatus);
			}

			DropDownList ddlPodSlipRequired = (DropDownList)customerProfileMultiPage.FindControl("ddlPodSlipRequired");
			
			if(ddlPodSlipRequired != null)
			{
				ddlPodSlipRequired.DataSource = (ICollection)m_dvPODSlipRequiredOptions;
				ddlPodSlipRequired.DataTextField = "Text";
				ddlPodSlipRequired.DataValueField = "StringValue";
				ddlPodSlipRequired.DataBind();

				if((drCurrent["pod_slip_required"]!= null) && (!drCurrent["pod_slip_required"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					String strPODSlipRequired = (String)drCurrent["pod_slip_required"];
					ddlPodSlipRequired.SelectedIndex = ddlPodSlipRequired.Items.IndexOf(ddlPodSlipRequired.Items.FindByValue(strPODSlipRequired));//.FindByValue("N"));
				}
				//Logger.LogDebugInfo("CustomerProfile","BindComboLists","INF006","Selecting Apply......"+e.Item.ItemIndex+" Selected Index is : "+ddlSCCloseStatus.SelectedIndex+"   "+strCloseStatus);
			}

			//HC Return Task
			DropDownList ddlInvoiceRequried = (DropDownList)customerProfileMultiPage.FindControl("ddlInvoiceRequried");
			
			if(ddlInvoiceRequried != null)
			{
				ddlInvoiceRequried.DataSource = (ICollection)m_dvInvoiceRequiredOptions;
				ddlInvoiceRequried.DataTextField = "Text";
				ddlInvoiceRequried.DataValueField = "StringValue";
				ddlInvoiceRequried.DataBind();

				if((drCurrent["hc_invoice_required"]!= null) && (!drCurrent["hc_invoice_required"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					String strInvoiceRequired = (String)drCurrent["hc_invoice_required"];
					ddlInvoiceRequried.SelectedIndex = ddlInvoiceRequried.Items.IndexOf(ddlInvoiceRequried.Items.FindByValue(strInvoiceRequired));//.FindByValue("N"));
				}
				//Logger.LogDebugInfo("CustomerProfile","BindComboLists","INF006","Selecting Apply......"+e.Item.ItemIndex+" Selected Index is : "+ddlSCCloseStatus.SelectedIndex+"   "+strCloseStatus);
			}
			//HC Return Task

			DropDownList ddlApplyEsaSurcharge = (DropDownList)customerProfileMultiPage.FindControl("ddlApplyEsaSurcharge");
			
			if(ddlApplyEsaSurcharge != null)
			{
				ddlApplyEsaSurcharge.DataSource = (ICollection)m_dvApplyESASurchargeOptions;
				ddlApplyEsaSurcharge.DataTextField = "Text";
				ddlApplyEsaSurcharge.DataValueField = "StringValue";
				ddlApplyEsaSurcharge.DataBind();

				if((drCurrent["apply_esa_surcharge"]!= null) && (!drCurrent["apply_esa_surcharge"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					String strApplyEsaSurcharge = (String)drCurrent["apply_esa_surcharge"];
					ddlApplyEsaSurcharge.SelectedIndex = ddlApplyEsaSurcharge.Items.IndexOf(ddlApplyEsaSurcharge.Items.FindByValue(strApplyEsaSurcharge));//.FindByValue("N"));
				}
				//Logger.LogDebugInfo("CustomerProfile","BindComboLists","INF006","Selecting Apply......"+e.Item.ItemIndex+" Selected Index is : "+ddlSCCloseStatus.SelectedIndex+"   "+strCloseStatus);
			}

			//Binding [apply_esa_surcharge_rep] to Dropdownlist is ddlApplyEsaSurchargeDel
			DropDownList ddlApplyEsaSurchargeDel = (DropDownList)customerProfileMultiPage.FindControl("ddlApplyEsaSurchargeDel");
			
			if(ddlApplyEsaSurchargeDel != null)
			{
				ddlApplyEsaSurchargeDel.DataSource = (ICollection)m_dvApplyESASurchargeOptions;
				ddlApplyEsaSurchargeDel.DataTextField = "Text";
				ddlApplyEsaSurchargeDel.DataValueField = "StringValue";
				ddlApplyEsaSurchargeDel.DataBind();

				if((drCurrent["apply_esa_surcharge_rep"]!= null) && (!drCurrent["apply_esa_surcharge_rep"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					String strApplyEsaSurchargeDel = (String)drCurrent["apply_esa_surcharge_rep"];
					ddlApplyEsaSurchargeDel.SelectedIndex = ddlApplyEsaSurchargeDel.Items.IndexOf(ddlApplyEsaSurchargeDel.Items.FindByValue(strApplyEsaSurchargeDel));//.FindByValue("N"));
				}
				//Logger.LogDebugInfo("CustomerProfile","BindComboLists","INF006","Selecting Apply......"+e.Item.ItemIndex+" Selected Index is : "+ddlSCCloseStatus.SelectedIndex+"   "+strCloseStatus);
			}

			DropDownList ddlStatus = (DropDownList)customerProfileMultiPage.FindControl("ddlStatus");
			
			if(ddlStatus != null)
			{
				ddlStatus.DataSource = (ICollection)m_dvStatusOptions;
				ddlStatus.DataTextField = "Text";
				ddlStatus.DataValueField = "StringValue";
				ddlStatus.DataBind();

				if((drCurrent["status_active"]!= null) && (!drCurrent["status_active"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					String strStatusActive = (String)drCurrent["status_active"];
					ddlStatus.SelectedIndex = ddlStatus.Items.IndexOf(ddlStatus.Items.FindByValue(strStatusActive));//.FindByValue("N"));
				}
				//Logger.LogDebugInfo("CustomerProfile","BindComboLists","INF006","Selecting Apply......"+e.Item.ItemIndex+" Selected Index is : "+ddlSCCloseStatus.SelectedIndex+"   "+strCloseStatus);
			}

			DropDownList ddlMBG = (DropDownList)customerProfileMultiPage.FindControl("ddlMBG");
			
			if(ddlMBG != null)
			{
				ddlMBG.DataSource = (ICollection)m_dvMBGOptions;
				ddlMBG.DataTextField = "Text";
				ddlMBG.DataValueField = "StringValue";
				ddlMBG.DataBind();

				if((drCurrent["mbg"]!= null) && (!drCurrent["mbg"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					String strMBG = (String)drCurrent["mbg"];
					ddlMBG.SelectedIndex = ddlMBG.Items.IndexOf(ddlMBG.Items.FindByValue(strMBG));//.FindByValue("N"));
				}
				//Logger.LogDebugInfo("CustomerProfile","BindComboLists","INF006","Selecting Apply......"+e.Item.ItemIndex+" Selected Index is : "+ddlSCCloseStatus.SelectedIndex+"   "+strCloseStatus);
			}
			//Jeab 8 Dec 2010
			DropDownList ddlDimByTOT = (DropDownList)customerProfileMultiPage.FindControl("ddlDimByTOT");
			
			if(ddlDimByTOT != null)
			{
				ddlDimByTOT.DataSource = (ICollection)m_dvDim_By_TOTOptions;
				ddlDimByTOT.DataTextField = "Text";
				ddlDimByTOT.DataValueField = "StringValue";
				ddlDimByTOT.DataBind();

				if((drCurrent["Dim_By_TOT"]!= null) && (!drCurrent["Dim_By_TOT"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					String strDim_By_TOT = (String)drCurrent["Dim_By_TOT"];
					ddlDimByTOT.SelectedIndex = ddlDimByTOT.Items.IndexOf(ddlDimByTOT.Items.FindByValue(strDim_By_TOT));//.FindByValue("N"));
				}
				//				else
				//				{
				//					ddlDimByTOT.SelectedIndex = ddlDimByTOT.Items.IndexOf(ddlDimByTOT.Items.FindByValue("N"));
				//				}
			}
			//Jeab 8 Dec 2010  =========> End

			DropDownList ddlDensityFactor = (DropDownList)customerProfileMultiPage.FindControl("ddlDensityFactor");
			
			if(ddlDensityFactor != null)
			{
				ddlDensityFactor.DataSource = (ICollection)m_dvDensityFactorOptions;
				ddlDensityFactor.DataTextField = "Text";
				ddlDensityFactor.DataValueField = "StringValue";
				ddlDensityFactor.DataBind();

				if((drCurrent["density_factor"]!= null) && (!drCurrent["density_factor"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					int tmpInt = Convert.ToInt32(drCurrent["density_factor"]);
					String strdensity_factor = Convert.ToString(tmpInt);
					ddlDensityFactor.SelectedIndex = ddlDensityFactor.Items.IndexOf(ddlDensityFactor.Items.FindByValue(strdensity_factor));//.FindByValue("N"));
				}
				else
				{
					ddlDensityFactor.SelectedIndex = 0;
				}
				//Logger.LogDebugInfo("CustomerProfile","BindComboLists","INF006","Selecting Apply......"+e.Item.ItemIndex+" Selected Index is : "+ddlSCCloseStatus.SelectedIndex+"   "+strCloseStatus);
			}

			DropDownList ddlCustomerType = (DropDownList)customerProfileMultiPage.FindControl("ddlCustomerType");
			
			if(ddlCustomerType != null)
			{
				ddlCustomerType.DataSource = (ICollection)m_dvCustomerTypeOptions;
				ddlCustomerType.DataTextField = "Text";
				ddlCustomerType.DataValueField = "StringValue";
				ddlCustomerType.DataBind();

				if((drCurrent["payer_type"]!= null) && (!drCurrent["payer_type"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					String strpayer_type = (String)drCurrent["payer_type"];
					ddlCustomerType.SelectedIndex = ddlCustomerType.Items.IndexOf(ddlCustomerType.Items.FindByValue(strpayer_type));//.FindByValue("N"));
				}
				else
				{
					ddlCustomerType.SelectedIndex = 0;
				}
				//Logger.LogDebugInfo("CustomerProfile","BindComboLists","INF006","Selecting Apply......"+e.Item.ItemIndex+" Selected Index is : "+ddlSCCloseStatus.SelectedIndex+"   "+strCloseStatus);
			}
			// By Aoo-Panas
			DropDownList ddlCreditTerm  = (DropDownList)customerProfileMultiPage.FindControl("ddlCreditTerm");
			
			if(ddlCreditTerm != null)
			{
				ddlCreditTerm.DataSource = (ICollection)m_dvCreditTerm;
				ddlCreditTerm.DataTextField = "Text";
				ddlCreditTerm.DataValueField = "StringValue";
				ddlCreditTerm.DataBind();

				if((drCurrent["credit_term"]!= null) && (!drCurrent["credit_term"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					String strCredit_term = drCurrent["credit_term"].ToString();
					ddlCreditTerm.SelectedIndex = ddlCreditTerm.Items.IndexOf(ddlCreditTerm.Items.FindByValue(strCredit_term));
					//					ddlCreditTerm.SelectedItem.Value = drCurrent["credit_term"].ToString();
				}
				else
				{
					ddlCreditTerm.SelectedItem.Value = "";
				}
			}


			//ADD BY X FEB 19 09
			DbCombo DbComboMasterAccount  = (DbCombo)customerProfileMultiPage.FindControl("DbComboMasterAccount");
			if(DbComboMasterAccount != null)
			{
				if((drCurrent["master_account"]!= null) && (!drCurrent["master_account"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					String strMaster_account = drCurrent["master_account"].ToString();
					DbComboMasterAccount.Text = strMaster_account;
					DbComboMasterAccount.Value = strMaster_account;
					ViewState["Master_Account"] = drCurrent["master_account"];
				}
				else
				{
					DbComboMasterAccount.Text = "";
					DbComboMasterAccount.Value = "";
					ViewState["Master_Account"] = null;
				}
			}


		}


		private void EnableTabs(bool bTab0, bool bTab1,bool bTab2, bool bTab3, bool bTab4,bool bTab5,bool bTab6)
		{
			Microsoft.Web.UI.WebControls.TabItem tabItem0 = TabStrip1.Items[0];
			tabItem0.Enabled = bTab0;
	
			Microsoft.Web.UI.WebControls.TabItem tabItem1 = TabStrip1.Items[1];
			tabItem1.Enabled = bTab1;		

			Microsoft.Web.UI.WebControls.TabItem tabItem2 = TabStrip1.Items[2];
			tabItem2.Enabled = bTab2;		

			Microsoft.Web.UI.WebControls.TabItem tabItem3 = TabStrip1.Items[3];
			tabItem3.Enabled = bTab3;		

			Microsoft.Web.UI.WebControls.TabItem tabItem4 = TabStrip1.Items[4];
			tabItem4.Enabled = bTab4;		

			Microsoft.Web.UI.WebControls.TabItem tabItem5 = TabStrip1.Items[5];
			tabItem5.Enabled = bTab5;		

			
			Microsoft.Web.UI.WebControls.TabItem tabItem6 = TabStrip1.Items[6];
			tabItem6.Enabled = bTab6;		
		}


		private bool IsRefTabEnabled()
		{
			Microsoft.Web.UI.WebControls.TabItem tabItem2 = TabStrip1.Items[2];
			return tabItem2.Enabled;
		}
		private void ChangeCPState()
		{
			if((int)ViewState["CPMode"] == (int) ScreenMode.Insert && (int)ViewState["CPOperation"] == (int)Operation.None)
			{
				ViewState["CPOperation"] = Operation.Insert;
			}
			else if((int)ViewState["CPMode"] == (int) ScreenMode.Insert && (int)ViewState["CPOperation"] == (int)Operation.Insert)
			{
				ViewState["CPOperation"] = Operation.Saved;
			}
			else if((int)ViewState["CPMode"] == (int) ScreenMode.Insert && (int)ViewState["CPOperation"] == (int)Operation.Saved)
			{
				ViewState["CPOperation"] = Operation.Update;
			}
			else if((int)ViewState["CPMode"] == (int) ScreenMode.Insert && (int)ViewState["CPOperation"] == (int)Operation.Update)
			{
				ViewState["CPOperation"] = Operation.Saved;
			}
			else if((int)ViewState["CPMode"] == (int) ScreenMode.ExecuteQuery && (int)ViewState["CPOperation"] == (int)Operation.None)
			{
				ViewState["CPOperation"] = Operation.Update;
			}
			else if((int)ViewState["CPMode"] == (int) ScreenMode.ExecuteQuery && (int)ViewState["CPOperation"] == (int)Operation.Update)
			{
				ViewState["CPOperation"] = Operation.None;
			}

			
		}


		private void EnableTxtAccNo()
		{

			msTextBox txtAccNo = (msTextBox)customerProfileMultiPage.FindControl("txtAccNo");
			
			if(txtAccNo != null)
			{
				if((int)ViewState["CPMode"] == (int) ScreenMode.Insert && (int)ViewState["CPOperation"] == (int)Operation.None)
				{
					txtAccNo.Enabled = true;
				}
				else if((int)ViewState["CPMode"] == (int) ScreenMode.Insert && (int)ViewState["CPOperation"] == (int)Operation.Saved)
				{
					txtAccNo.Enabled = false;
				}
				else if((int)ViewState["CPMode"] == (int) ScreenMode.ExecuteQuery)
				{
					txtAccNo.Enabled = false;
					EnableNew(false);
				}
				else if((int)ViewState["CPMode"] == (int) ScreenMode.Query)
				{
					txtAccNo.Enabled = true;
				}

			}


			
		}


		private void txtAccNo_TextChanged(object sender,System.EventArgs e)
		{
			if((bool)ViewState["IsTextChanged"] == false)
			{
				ChangeCPState();
				ViewState["IsTextChanged"] = true;
			}
		}

		private void txtFirstShipDate_TextChanged(object sender,System.EventArgs e)
		{
			if((bool)ViewState["IsTextChanged"] == false)
			{
				ChangeCPState();
				ViewState["IsTextChanged"] = true;
			}
		}

		private void txtRefNo_TextChanged(object sender,System.EventArgs e)
		{
			if((bool)ViewState["IsTextChanged"] == false)
			{
				ChangeCPState();
				ViewState["IsTextChanged"] = true;
			}
		}

		private void txtCustomerName_TextChanged(object sender,System.EventArgs e)
		{
			if((bool)ViewState["IsTextChanged"] == false)
			{
				ChangeCPState();
				ViewState["IsTextChanged"] = true;
			}
		}

		private void txtContactPerson_TextChanged(object sender,System.EventArgs e)
		{
			if((bool)ViewState["IsTextChanged"] == false)
			{
				ChangeCPState();
				ViewState["IsTextChanged"] = true;
			}
		}

		private void txtAddress1_TextChanged(object sender,System.EventArgs e)
		{
			if((bool)ViewState["IsTextChanged"] == false)
			{
				ChangeCPState();
				ViewState["IsTextChanged"] = true;
			}
		}

		private void txtAddress2_TextChanged(object sender,System.EventArgs e)
		{
			if((bool)ViewState["IsTextChanged"] == false)
			{
				ChangeCPState();
				ViewState["IsTextChanged"] = true;
			}
		}

		private void txtCountry_TextChanged(object sender,System.EventArgs e)
		{
			if((bool)ViewState["IsTextChanged"] == false)
			{
				ChangeCPState();
				ViewState["IsTextChanged"] = true;
			}
		}

		private void txtZipCode_TextChanged(object sender,System.EventArgs e)
		{
			if((bool)ViewState["IsTextChanged"] == false)
			{
				ChangeCPState();
				ViewState["IsTextChanged"] = true;
			}
		}

		private void txtTelephone_TextChanged(object sender,System.EventArgs e)
		{
			if((bool)ViewState["IsTextChanged"] == false)
			{
				ChangeCPState();
				ViewState["IsTextChanged"] = true;
			}
		}

		private void txtMBG_TextChanged(object sender,System.EventArgs e)
		{
			if((bool)ViewState["IsTextChanged"] == false)
			{
				ChangeCPState();
				ViewState["IsTextChanged"] = true;
			}
		}

		private void txtFax_TextChanged(object sender,System.EventArgs e)
		{
			if((bool)ViewState["IsTextChanged"] == false)
			{
				ChangeCPState();
				ViewState["IsTextChanged"] = true;
			}
		}

		//		private void txtCreditTerm_TextChanged(object sender,System.EventArgs e)
		//		{
		//			if((bool)ViewState["IsTextChanged"] == false)
		//			{
		//				ChangeCPState();
		//				ViewState["IsTextChanged"] = true;
		//			}
		//		}

		private void txtISectorCode_TextChanged(object sender,System.EventArgs e)
		{
			if((bool)ViewState["IsTextChanged"] == false)
			{
				ChangeCPState();
				ViewState["IsTextChanged"] = true;
			}
		}

		private void txtCreditLimit_TextChanged(object sender,System.EventArgs e)
		{
			if((bool)ViewState["IsTextChanged"] == false)
			{
				ChangeCPState();
				ViewState["IsTextChanged"] = true;
			}
		}

		private void txtActiveQuatationNo_TextChanged(object sender,System.EventArgs e)
		{
			if((bool)ViewState["IsTextChanged"] == false)
			{
				ChangeCPState();
				ViewState["IsTextChanged"] = true;
			}
		}

		private void txtCreditOutstanding_TextChanged(object sender,System.EventArgs e)
		{
			if((bool)ViewState["IsTextChanged"] == false)
			{
				ChangeCPState();
				ViewState["IsTextChanged"] = true;
			}
		}

		private void txtRemark_TextChanged(object sender,System.EventArgs e)
		{
			if((bool)ViewState["IsTextChanged"] == false)
			{
				ChangeCPState();
				ViewState["IsTextChanged"] = true;
			}
		}

		private void txtSalesmanID_TextChanged(object sender,System.EventArgs e)
		{
			if((bool)ViewState["IsTextChanged"] == false)
			{
				ChangeCPState();
				ViewState["IsTextChanged"] = true;
			}
		}

		private void txtPromisedTotWt_TextChanged(object sender,System.EventArgs e)
		{
			if((bool)ViewState["IsTextChanged"] == false)
			{
				ChangeCPState();
				ViewState["IsTextChanged"] = true;
			}
		}

		private void txtPromisedTotPkg_TextChanged(object sender,System.EventArgs e)
		{
			if((bool)ViewState["IsTextChanged"] == false)
			{
				ChangeCPState();
				ViewState["IsTextChanged"] = true;
			}
		}

		private void txtPromisedPeriod_TextChanged(object sender,System.EventArgs e)
		{
			if((bool)ViewState["IsTextChanged"] == false)
			{
				ChangeCPState();
				ViewState["IsTextChanged"] = true;
			}
		}

		private void txtFreeInsuranceAmt_TextChanged(object sender,System.EventArgs e)
		{
			if((bool)ViewState["IsTextChanged"] == false)
			{
				ChangeCPState();
				ViewState["IsTextChanged"] = true;
			}

			msTextBox txtFreeInsuranceAmt = (msTextBox)customerProfileMultiPage.FindControl("txtFreeInsuranceAmt");
			if(txtFreeInsuranceAmt.Text!="")
			{
				//By Aoo
				if(Convert.ToDecimal(txtFreeInsuranceAmt.Text) != 0)
				{
					decimal FreeInsuranceAmt = Convert.ToDecimal(txtFreeInsuranceAmt.Text.Trim()) ;
					txtFreeInsuranceAmt.Text = String.Format((String)ViewState["m_format"], FreeInsuranceAmt);
				}
				//				else
				//				{
				//					txtFreeInsuranceAmt.Text = "";
				//				}
			}
		}

		private void txtInsurancePercentSurcharge_TextChanged(object sender,System.EventArgs e)
		{
			if((bool)ViewState["IsTextChanged"] == false)
			{
				ChangeCPState();
				ViewState["IsTextChanged"] = true;
			}
		}

		// for Invoice Return Days
		private void txtInvoiceReturn_TextChanged(object sender,System.EventArgs e)
		{
			if((bool)ViewState["IsTextChanged"] == false)
			{
				ChangeCPState();
				ViewState["IsTextChanged"] = true;
			}
		}

		private void txtEmail_TextChanged(object sender,System.EventArgs e)
		{
			if((bool)ViewState["IsTextChanged"] == false)
			{
				ChangeCPState();
				ViewState["IsTextChanged"] = true;
			}
		}

		private void txtCreatedBy_TextChanged(object sender,System.EventArgs e)
		{
			if((bool)ViewState["IsTextChanged"] == false)
			{
				ChangeCPState();
				ViewState["IsTextChanged"] = true;
			}
		}

		private void radioBtnCash_CheckedChanged(object sender,System.EventArgs e)
		{
			if((bool)ViewState["IsTextChanged"] == false)
			{
				ChangeCPState();
				ViewState["IsTextChanged"] = true;
			}
			ddlCreditTerm.Enabled = false;
			txtNextBillPlacementDate.Enabled = false;
			btnSetBillPlacementRules.Enabled = false;
		}

		private void radioBtnCredit_CheckedChanged(object sender,System.EventArgs e)
		{
			if((bool)ViewState["IsTextChanged"] == false)
			{
				ChangeCPState();
				ViewState["IsTextChanged"] = true;
			}
			ddlCreditTerm.Enabled = true;
			EnableBillPlacement(true);
			btnSetBillPlacementRules.Enabled = true;
		}

		private void ddlApplyDimWt_SelectedIndexChanged(object sender,System.EventArgs e)
		{
			if((bool)ViewState["IsTextChanged"] == false)
			{
				ChangeCPState();
				ViewState["IsTextChanged"] = true;
			}
		}

		private void ddlPodSlipRequired_SelectedIndexChanged(object sender,System.EventArgs e)
		{
			if((bool)ViewState["IsTextChanged"] == false)
			{
				ChangeCPState();
				ViewState["IsTextChanged"] = true;
			}
		}

		//HC Return Task
		private void ddlInvoiceRequried_SelectedIndexChanged(object sender,System.EventArgs e)
		{
			if((bool)ViewState["IsTextChanged"] == false)
			{
				ChangeCPState();
				ViewState["IsTextChanged"] = true;
			}
		}
		//HC Return Task

		private void ddlApplyEsaSurcharge_SelectedIndexChanged(object sender,System.EventArgs e)
		{
			if((bool)ViewState["IsTextChanged"] == false)
			{
				ChangeCPState();
				ViewState["IsTextChanged"] = true;
			}
		}

		private void ddlApplyEsaSurchargeDel_SelectedIndexChanged(object sender,System.EventArgs e)
		{
			if((bool)ViewState["IsTextChanged"] == false)
			{
				ChangeCPState();
				ViewState["IsTextChanged"] = true;
			}
		}

		private void ddlStatus_SelectedIndexChanged(object sender,System.EventArgs e)
		{
			if((bool)ViewState["IsTextChanged"] == false)
			{
				ChangeCPState();
				ViewState["IsTextChanged"] = true;
			}
		}

		private void ddlMBG_SelectedIndexChanged(object sender,System.EventArgs e)
		{
			if((bool)ViewState["IsTextChanged"] == false)
			{
				ChangeCPState();
				ViewState["IsTextChanged"] = true;
			}
		}
		//Jeab 8 Dec 2010
		private void ddlDimByTOT_SelectedIndexChanged(object sender,System.EventArgs e)
		{
			if((bool)ViewState["IsTextChanged"] == false)
			{
				ChangeCPState();
				ViewState["IsTextChanged"] = true;
			}
		}
		//Jeab 8 Dec 2010  =========> End

		private void chkBox_CheckedChanged(object sender,System.EventArgs e)
		{
			if((bool)ViewState["IsTextChanged"] == false)
			{
				ChangeCPState();
				ViewState["IsTextChanged"] = true;
			}
			TextBox  txtMinimumBox = (TextBox)customerProfileMultiPage.FindControl("txtMinimumBox");
			if(chkBox.Checked)
			{
				txtMinimumBox.ReadOnly = false;
				txtMinimumBox.Text = "1";
			}
			else
			{
				txtMinimumBox.ReadOnly = true;
				txtMinimumBox.Text = "";
			}
				
		}
		

		private void ddlDensityFactor_SelectedIndexChanged(object sender,System.EventArgs e)
		{
			if((bool)ViewState["IsTextChanged"] == false)
			{
				ChangeCPState();
				ViewState["IsTextChanged"] = true;
			}
		}

		private void ddlCustomerType_SelectedIndexChanged(object sender,System.EventArgs e)
		{
			if((bool)ViewState["IsTextChanged"] == false)
			{
				ChangeCPState();
				ViewState["IsTextChanged"] = true;
			}
		}

		private void DbComboMasterAccount_SelectedItemChanged(object sender,System.EventArgs e)
		{
			if((bool)ViewState["IsTextChanged"] == false)
			{
				ChangeCPState();
				ViewState["IsTextChanged"] = true;
			}
		}

		private void txtMaxAmt_TextChanged(object sender, System.EventArgs e)
		{
			if((bool)ViewState["IsTextChanged"] == false)
			{
				ChangeCPState();
				ViewState["IsTextChanged"] = true;
			}

			msTextBox txtMaxAmt = (msTextBox)customerProfileMultiPage.FindControl("txtMaxAmt");
			if(txtMaxAmt.Text!="")
			{
				//By Aoo
				if(Convert.ToDecimal(txtMaxAmt.Text) != 0)
				{
					
					decimal MaxAmt = Convert.ToDecimal(txtMaxAmt.Text.Trim()) ;
					txtMaxAmt.Text = String.Format((String)ViewState["m_format"], MaxAmt);
				}
				//				else
				//				{
				//					txtMaxAmt.Text = "";
				//				}
			}
		}

		private void txtCODSurchargeAmt_TextChanged(object sender, System.EventArgs e)
		{

			if((bool)ViewState["IsTextChanged"] == false)
			{
				ChangeCPState();
				ViewState["IsTextChanged"] = true;
			}
			//Call function rounding when textchang (by sittichai 10/01/2008)
			msTextBox txtCODSurchargeAmt = (msTextBox)customerProfileMultiPage.FindControl("txtCODSurchargeAmt");
			if(txtCODSurchargeAmt.Text!="")
			{
				//decimal tmpCODSurchargeAMD = TIESUtility.EnterpriseRounding(Convert.ToDecimal(txtCODSurchargeAmt.Text.Trim()),(int)ViewState["wt_rounding_method"],(decimal)ViewState["wt_increment_amt"]);
				decimal tmpCODSurchargeAMD = Convert.ToDecimal(txtCODSurchargeAmt.Text.Trim());
				txtCODSurchargeAmt.Text = String.Format((String)ViewState["m_format"], tmpCODSurchargeAMD);
			}


		}

		private void txtCODSurchargePercent_TextChanged(object sender, System.EventArgs e)
		{
			if((bool)ViewState["IsTextChanged"] == false)
			{
				ChangeCPState();
				ViewState["IsTextChanged"] = true;
			}
		}

		private void ddlCreditTerm_SelectedIndexChanged(object sender,System.EventArgs e)
		{
			if((bool)ViewState["IsTextChanged"] == false)
			{
				ChangeCPState();
				ViewState["IsTextChanged"] = true;
			}
		}
		//������ա������¹�ŧ���� TextBox �ͧ Maximum COD Surcharge �Ѻ Maximum COD Surcharge by Ching Nov 28,2007
		private void txtMaxCODSurcharge_TextChanged(object sender, System.EventArgs e)
		{
			if((bool)ViewState["IsTextChanged"] == false)
			{
				ChangeCPState();
				ViewState["IsTextChanged"] = true;
			}
			//Call function rounding when textchang (by sittichai 10/01/2008)
			msTextBox txtMaxCODSurcharge = (msTextBox)customerProfileMultiPage.FindControl("txtMaxCODSurcharge");
			if(txtMaxCODSurcharge.Text!="")
			{
				//Not Allow key 0 by GwanG Jan 16,2008
				if(Convert.ToDecimal(txtMaxCODSurcharge.Text) != 0)
				{
					//decimal MaxCODSurcharge = TIESUtility.EnterpriseRounding(Convert.ToDecimal(txtMaxCODSurcharge.Text.Trim()),(int)ViewState["wt_rounding_method"],(decimal)ViewState["wt_increment_amt"]);
					decimal MaxCODSurcharge = Convert.ToDecimal(txtMaxCODSurcharge.Text.Trim()) ;
					txtMaxCODSurcharge.Text = String.Format((String)ViewState["m_format"], MaxCODSurcharge);
				}
				else
				{
					txtMaxCODSurcharge.Text = "";
				}
			}
		}

		private void txtMinCODSurcharge_TextChanged(object sender, System.EventArgs e)
		{
			if((bool)ViewState["IsTextChanged"] == false)
			{
				ChangeCPState();
				ViewState["IsTextChanged"] = true;
			}
			//Call function rounding when textchang (by sittichai 10/01/2008)
			msTextBox txtMinCODSurcharge = (msTextBox)customerProfileMultiPage.FindControl("txtMinCODSurcharge");
			if(txtMinCODSurcharge.Text!="")
			{
				//Not Allow key 0 by GwanG Jan 16,2008
				if(Convert.ToDecimal(txtMinCODSurcharge.Text) != 0)
				{
					//decimal MinCODSurcharge = TIESUtility.EnterpriseRounding(Convert.ToDecimal(txtMinCODSurcharge.Text.Trim()),(int)ViewState["wt_rounding_method"],(decimal)ViewState["wt_increment_amt"]);
					decimal MinCODSurcharge = Convert.ToDecimal(txtMinCODSurcharge.Text.Trim());
					txtMinCODSurcharge.Text = String.Format((String)ViewState["m_format"], MinCODSurcharge);
				}
				else
				{
					txtMinCODSurcharge.Text = "";
				}
			}
		}
		// End

		//By Aoo
		private void msOtherSurchargePercentage_TextChanged(object sender,System.EventArgs e)
		{
			if((bool)ViewState["IsTextChanged"] == false)
			{
				ChangeCPState();
				ViewState["IsTextChanged"] = true;
			}
		}
	
		private void msOtherSurchargeMinimum_TextChanged(object sender,System.EventArgs e)
		{
			if((bool)ViewState["IsTextChanged"] == false)
			{
				ChangeCPState();
				ViewState["IsTextChanged"] = true;
			}

			msTextBox msOtherSurchargeMinimum = (msTextBox)customerProfileMultiPage.FindControl("msOtherSurchargeMinimum");
			if(msOtherSurchargeMinimum.Text!="")
			{
				//By Aoo
				if(Convert.ToDecimal(msOtherSurchargeMinimum.Text) != 0)
				{
					
					decimal decOthersurc = Convert.ToDecimal(msOtherSurchargeMinimum.Text.Trim()) ;
					msOtherSurchargeMinimum.Text = String.Format((String)ViewState["m_format"], decOthersurc);
				}
				//				else
				//				{
				//					msOtherSurchargeMinimum.Text = "";
				//				}
			}
		}

		private void msOtherSurchargeMaximum_TextChanged(object sender,System.EventArgs e)
		{
			if((bool)ViewState["IsTextChanged"] == false)
			{
				ChangeCPState();
				ViewState["IsTextChanged"] = true;
			}

			msTextBox msOtherSurchargeMaximum = (msTextBox)customerProfileMultiPage.FindControl("msOtherSurchargeMaximum");
			if(msOtherSurchargeMaximum.Text!="")
			{
				//By Aoo
				if(Convert.ToDecimal(msOtherSurchargeMaximum.Text) != 0)
				{
					
					decimal decOthersurc = Convert.ToDecimal(msOtherSurchargeMaximum.Text.Trim()) ;
					msOtherSurchargeMaximum.Text = String.Format((String)ViewState["m_format"], decOthersurc);
				}
				//				else
				//				{
				//					msOtherSurchargeMaximum.Text = "";
				//				}
			}
		}

		private void msOtherSurchargeDescription_TextChanged(object sender,System.EventArgs e)
		{
			if((bool)ViewState["IsTextChanged"] == false)
			{
				ChangeCPState();
				ViewState["IsTextChanged"] = true;
			}
		}
		
		private void txtMaxInsuranceSurcharge_TextChanged(object sender,System.EventArgs e)
		{
			if((bool)ViewState["IsTextChanged"] == false)
			{
				ChangeCPState();
				ViewState["IsTextChanged"] = true;
			}

			//Call function rounding when textchang (by Aoo)
			msTextBox txtMaxInsuranceSurcharge = (msTextBox)customerProfileMultiPage.FindControl("txtMaxInsuranceSurcharge");
			if(txtMaxInsuranceSurcharge.Text!="")
			{
				if(Convert.ToDecimal(txtMaxInsuranceSurcharge.Text) != 0)
				{
					
					decimal MaxInsuranceSurcharge = Convert.ToDecimal(txtMaxInsuranceSurcharge.Text.Trim());
					txtMaxInsuranceSurcharge.Text = String.Format((String)ViewState["m_format"], MaxInsuranceSurcharge);
				}
				//				else
				//				{
				//					txtMaxInsuranceSurcharge.Text = "";
				//				}
			}
		}

		private void txtMinInsuranceSurcharge_TextChanged(object sender,System.EventArgs e)
		{
			if((bool)ViewState["IsTextChanged"] == false)
			{
				ChangeCPState();
				ViewState["IsTextChanged"] = true;
			}
			//Call function rounding when textchang (by Aoo)
			msTextBox txtMinInsuranceSurcharge = (msTextBox)customerProfileMultiPage.FindControl("txtMinInsuranceSurcharge");
			if(txtMinInsuranceSurcharge.Text!="")
			{
				if(Convert.ToDecimal(txtMinInsuranceSurcharge.Text) != 0)
				{
					
					decimal MinInsuranceSurcharge = Convert.ToDecimal(txtMinInsuranceSurcharge.Text.Trim());
					txtMinInsuranceSurcharge.Text = String.Format((String)ViewState["m_format"], MinInsuranceSurcharge);
				}
				//				else
				//				{
				//					txtMinInsuranceSurcharge.Text = "";
				//				}
			}
		}

		private void msDiscountBand_TextChanged(object sender,System.EventArgs e)
		{
			if((bool)ViewState["IsTextChanged"] == false)
			{
				ChangeCPState();
				ViewState["IsTextChanged"] = true;
			}
		}

		private void msOtherSurchargeAmount_TextChanged(object sender,System.EventArgs e)
		{
			if((bool)ViewState["IsTextChanged"] == false)
			{
				ChangeCPState();
				ViewState["IsTextChanged"] = true;
			}

			msTextBox msOtherSurchargeAmount = (msTextBox)customerProfileMultiPage.FindControl("msOtherSurchargeAmount");
			if(msOtherSurchargeAmount.Text != "")
			{
				decimal decOtherSurAmt = Convert.ToDecimal(msOtherSurchargeAmount.Text.Trim());
				msOtherSurchargeAmount.Text = String.Format((String)ViewState["m_format"],decOtherSurAmt);
			}
		}

		private void msDensityFactor_TextChanged(object sender,System.EventArgs e)
		{
			if((bool)ViewState["IsTextChanged"] == false)
			{
				ChangeCPState();
				ViewState["IsTextChanged"] = true;
			}
		}

		//End By Aoo

		private void txtCountRec_TextChanged(object sender, System.EventArgs e)
		{
		
		}

		
		/// Reference Tab
		/// 
		private void btnRefViewAll_Click(object sender, System.EventArgs e)
		{
			ShowReferencesForCurrentCustomer();
			this.CheckRole();
		}

		private void btnRefInsert_Click(object sender, System.EventArgs e)
		{
			EnableRefPage(true);
			lblNumRefRec.Text = "";
			ViewState["RefMode"] = ScreenMode.Insert;
			ViewState["RefOperation"] = Operation.None;
			ClearRefPageForInsert();
			Button btnRefInsert = (Button)customerProfileMultiPage.FindControl("btnRefInsert");
			if(btnRefInsert != null)
			{
				btnRefInsert.Enabled	= false;
			}
		}

		private void ShowReferencesForCurrentCustomer()
		{
			ViewState["RefMode"] = ScreenMode.ExecuteQuery;
			ViewState["RefOperation"] = Operation.None;
			ViewState["IsRefTextChanged"] = false;
			ViewState["currentRefPage"] = 0;
			ViewState["currentRefSet"] = 0;
			GetRefRecSet();
			Button btnRefInsert = (Button)customerProfileMultiPage.FindControl("btnRefInsert");
			if(m_sdsReference.QueryResultMaxSize == 0)
			{
				lblNumRefRec.Text = Utility.GetLanguageText(ResourceType.UserMessage, "NO_REF_ADDED", utility.GetUserCulture());
				EnableRefNavigationButtons(false,false,false,false);
//				if(btnRefInsert != null)
//				{
//					btnRefInsert.Enabled	= true;
//				}
				ViewState["RefMode"] = ScreenMode.None;
				ViewState["RefOperation"] = Operation.None;
				ClearRefPageForInsert();
				EnableRefPage(false);
				return;
			}
			EnableRefPage(true);
			DisplayCurrentRefPage();

//			if(btnRefInsert != null)
//			{
//				btnRefInsert.Enabled = true;
//			}

			if(m_sdsReference != null && m_sdsReference.DataSetRecSize > 0)
			{
				EnableRefNavigationButtons(false,false,true,true);
			}

			lblCPErrorMessage.Text = "";
			DisplayRefRecNum();
		}

		private void btnFirstRef_Click(object sender, System.EventArgs e)
		{
			ViewState["currentRefSet"] = 0;	
			GetRefRecSet();
			ViewState["currentRefPage"] = 0;
			DisplayCurrentRefPage();
			DisplayRefRecNum();
			EnableRefNavigationButtons(false,false,true,true);
		}

		private void btnPreviousRef_Click(object sender, System.EventArgs e)
		{
			int iCurrentPage = Convert.ToInt32(ViewState["currentRefPage"]);

			if(Convert.ToInt32(ViewState["currentRefPage"])  > 0 )
			{
				ViewState["currentRefPage"] = Convert.ToInt32(ViewState["currentRefPage"]) - 1;
				DisplayCurrentRefPage();
			}
			else
			{
				if( (Convert.ToInt32(ViewState["currentRefSet"]) - 1) < 0)
				{
					EnableRefNavigationButtons(false,false,true,true);
					return;
				}
				
				ViewState["currentRefSet"] = Convert.ToInt32(ViewState["currentRefSet"])- 1;	
				GetRefRecSet();
				ViewState["currentRefPage"] = Convert.ToInt32(m_sdsReference.DataSetRecSize) - 1;
				DisplayCurrentRefPage();
			}
			DisplayRefRecNum();
			EnableRefNavigationButtons(true,true,true,true);
			btnViewAllVAS_Click(this, new System.EventArgs());

	
		}

		private void btnNextRef_Click(object sender, System.EventArgs e)
		{
			int iCurrentPage = Convert.ToInt32(ViewState["currentRefPage"]);

			if(Convert.ToInt32(ViewState["currentRefPage"])  < (Convert.ToInt32(m_sdsReference.DataSetRecSize) - 1) )
			{
				ViewState["currentRefPage"] = Convert.ToInt32(ViewState["currentRefPage"]) + 1;
				DisplayCurrentRefPage();
			}
			else
			{
				decimal iTotalRec = (Convert.ToInt32(ViewState["currentRefSet"]) * m_iSetSize) + Convert.ToInt32(m_sdsReference.DataSetRecSize);
				if( iTotalRec ==  m_sdsReference.QueryResultMaxSize)
				{
					EnableRefNavigationButtons(true,true,false,false);
					return;
				}
				
				ViewState["currentRefSet"] = Convert.ToInt32(ViewState["currentRefSet"]) + 1;	
				GetRefRecSet();
				ViewState["currentRefPage"] = 0;
				DisplayCurrentRefPage();
			}
			DisplayRefRecNum();
			EnableRefNavigationButtons(true,true,true,true);
			btnViewAllVAS_Click(this, new System.EventArgs());


	
		}

		private void btnLastRef_Click(object sender, System.EventArgs e)
		{
			ViewState["currentRefSet"] = Convert.ToInt32((m_sdsReference.QueryResultMaxSize - 1))/m_iSetSize;	
			GetRefRecSet();
			ViewState["currentRefPage"] = Convert.ToInt32(m_sdsReference.DataSetRecSize) - 1;
			DisplayCurrentRefPage();	
			DisplayRefRecNum();
			EnableRefNavigationButtons(true,true,false,false);
			btnViewAllVAS_Click(this, new System.EventArgs());
	
		}

		private void EnableRefNavigationButtons(bool bMoveFirst, bool bMoveNext,bool bEnableMovePrevious, bool bMoveLast)
		{

			Button btnFirstRef = (Button)customerProfileMultiPage.FindControl("btnFirstRef");

			if(btnFirstRef != null)
			{
				btnFirstRef.Enabled	= bMoveFirst;
			}
			
			Button btnPreviousRef = (Button)customerProfileMultiPage.FindControl("btnPreviousRef");

			if(btnPreviousRef != null)
			{
				btnPreviousRef.Enabled	= bMoveNext;
			}
			
			Button btnNextRef = (Button)customerProfileMultiPage.FindControl("btnNextRef");

			if(btnNextRef != null)
			{
				btnNextRef.Enabled	= bEnableMovePrevious;
			}
			
			Button btnLastRef = (Button)customerProfileMultiPage.FindControl("btnLastRef");

			if(btnLastRef != null)
			{
				btnLastRef.Enabled	= bMoveLast;
			}
		}

		private void GetRefRecSet()
		{
			decimal iStartIndex = System.Convert.ToDecimal(ViewState["currentRefSet"]) * m_iSetSize;
			
			String strCustID = (String)ViewState["currentCustomer"];

			m_sdsReference = SysDataMgrDAL.GetReferenceDS(utility.GetAppID(),utility.GetEnterpriseID(),strCustID,System.Convert.ToInt32(iStartIndex),System.Convert.ToInt32(m_iSetSize));
			
			Session["SESSION_DS2"] = m_sdsReference;
			decimal pgCnt = (m_sdsReference.QueryResultMaxSize - 1)/m_iSetSize;
			if(pgCnt < System.Convert.ToDecimal(ViewState["currentRefSet"]))
			{
				ViewState["currentRefSet"] = pgCnt;
			}
		}

		private void DisplayCurrentRefPage()
		{
			DataRow drCurrent = m_sdsCustomerProfile.ds.Tables[0].Rows[Convert.ToInt32(ViewState["currentPage"])];
			int iRefPage=Convert.ToInt32(ViewState["currentRefPage"]);
			if (iRefPage < 0)
			{
				return;				
			}
			DataRow drCurrentRef = m_sdsReference.ds.Tables[0].Rows[iRefPage];
			TextBox txtCustAccDispTab3 = (TextBox)customerProfileMultiPage.FindControl("txtCustAccDispTab3");
			
			if(txtCustAccDispTab3 != null)
			{
				if(drCurrent["custid"]!= null && (!drCurrent["custid"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					txtCustAccDispTab3.Text = drCurrent["custid"].ToString();
				}
				else
				{
					txtCustAccDispTab3.Text = "";
				}
			}

			TextBox txtCustNameDispTab3 = (TextBox)customerProfileMultiPage.FindControl("txtCustNameDispTab3");
			if(txtCustNameDispTab3 != null)
			{
				if(drCurrent["cust_name"]!= null && (!drCurrent["cust_name"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					txtCustNameDispTab3.Text = drCurrent["cust_name"].ToString();
				}
				else
				{
					txtCustNameDispTab3.Text = "";
				}
			}
			
			TextBox txtRefSndRecName = (TextBox)customerProfileMultiPage.FindControl("txtRefSndRecName");
			if(txtRefSndRecName != null)
			{
				if(drCurrentRef["snd_rec_name"]!= null && (!drCurrentRef["snd_rec_name"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					txtRefSndRecName.Text = drCurrentRef["snd_rec_name"].ToString();
				}
				else
				{
					txtRefSndRecName.Text = "";
				}

				EnableTxtRefName();
			}

			TextBox txtRefContactPerson = (TextBox)customerProfileMultiPage.FindControl("txtRefContactPerson");
			if(txtRefContactPerson != null)
			{
				if(drCurrentRef["contact_person"]!= null && (!drCurrentRef["contact_person"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					txtRefContactPerson.Text = drCurrentRef["contact_person"].ToString();
				}
				else
				{
					txtRefContactPerson.Text = "";
				}

			}

			
			TextBox txtRefEmail = (TextBox)customerProfileMultiPage.FindControl("txtRefEmail");

			if(txtRefEmail != null)
			{
				if(drCurrentRef["email"]!= null && (!drCurrentRef["email"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					txtRefEmail.Text = drCurrentRef["email"].ToString();
				}
				else
				{
					txtRefEmail.Text = "";
				}
			}

			TextBox txtRefAddress1 = (TextBox)customerProfileMultiPage.FindControl("txtRefAddress1");
			if(txtRefAddress1 != null)
			{
				if(drCurrentRef["address1"]!= null && (!drCurrentRef["address1"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					txtRefAddress1.Text = drCurrentRef["address1"].ToString();
				}
				else
				{
					txtRefAddress1.Text = "";
				}
			}
			//By Aoo
			

			msTextBox msOtherSurchargeAmount = (msTextBox)customerProfileMultiPage.FindControl("msOtherSurchargeAmount");
			if(msOtherSurchargeAmount != null)
			{
				if(drCurrent["other_surcharge_amount"]!= null && (!drCurrent["other_surcharge_amount"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					decimal decOtherSurAmt = Convert.ToDecimal(drCurrent["other_surcharge_amount"].ToString());
					msOtherSurchargeAmount.Text = String.Format((String)ViewState["m_format"],decOtherSurAmt);
				}
				else
				{
					msOtherSurchargeAmount.Text = "";
				}
			}
			msTextBox msOtherSurchargePercentage = (msTextBox)customerProfileMultiPage.FindControl("msOtherSurchargePercentage");
			if(msOtherSurchargePercentage != null)
			{
				if(drCurrent["other_surcharge_percentage"]!= null && (!drCurrent["other_surcharge_percentage"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					msOtherSurchargePercentage.Text = drCurrent["other_surcharge_percentage"].ToString();
				}
				else
				{
					msOtherSurchargePercentage.Text = "";
				}
			}
			msTextBox msOtherSurchargeMinimum = (msTextBox)customerProfileMultiPage.FindControl("msOtherSurchargeMinimum");
			if(msOtherSurchargeMinimum != null)
			{
				if(drCurrent["other_surcharge_min"]!= null && (!drCurrent["other_surcharge_min"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					decimal decOtherSurMin = Convert.ToDecimal(drCurrent["other_surcharge_min"].ToString());
					msOtherSurchargeMinimum.Text = String.Format((String)ViewState["m_format"],decOtherSurMin);
				}
				else
				{
					msOtherSurchargeMinimum.Text = "";
				}
			}
			msTextBox msOtherSurchargeMaximum = (msTextBox)customerProfileMultiPage.FindControl("msOtherSurchargeMaximum");
			if(msOtherSurchargeMaximum != null)
			{
				if(drCurrent["other_surcharge_max"]!= null && (!drCurrent["other_surcharge_max"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					decimal decOtherSurMax = Convert.ToDecimal(drCurrent["other_surcharge_max"].ToString());
					msOtherSurchargeMaximum.Text = String.Format((String)ViewState["m_format"],decOtherSurMax);
				}
				else
				{
					msOtherSurchargeMaximum.Text = "";
				}
			}
			msTextBox msOtherSurchargeDescription = (msTextBox)customerProfileMultiPage.FindControl("msOtherSurchargeDescription");
			if(msOtherSurchargeDescription != null)
			{
				if(drCurrent["other_surcharge_desc"]!= null && (!drCurrent["other_surcharge_desc"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					msOtherSurchargeDescription.Text = drCurrent["other_surcharge_desc"].ToString();
				}
				else
				{
					msOtherSurchargeDescription.Text = "";
				}
			}

			msTextBox txtMaxInsuranceSurcharge = (msTextBox)customerProfileMultiPage.FindControl("txtMaxInsuranceSurcharge");
			if(txtMaxInsuranceSurcharge != null)
			{
				if(drCurrent["max_insurance_surcharge"]!= null && (!drCurrent["max_insurance_surcharge"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					decimal decMaxInsSurcharge = Convert.ToDecimal(drCurrent["max_insurance_surcharge"].ToString());
					txtMaxInsuranceSurcharge.Text = String.Format((String)ViewState["m_format"],decMaxInsSurcharge);
				}
				else
				{
					txtMaxInsuranceSurcharge.Text = "";
				}
			}
			msTextBox txtMinInsuranceSurcharge = (msTextBox)customerProfileMultiPage.FindControl("txtMinInsuranceSurcharge");
			if(txtMinInsuranceSurcharge != null)
			{
				if(drCurrent["min_insurance_surcharge"]!= null && (!drCurrent["min_insurance_surcharge"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					decimal decMinInsSurcharge = Convert.ToDecimal(drCurrent["min_insurance_surcharge"].ToString());
					txtMinInsuranceSurcharge.Text = String.Format((String)ViewState["m_format"],decMinInsSurcharge);
				}
				else
				{
					txtMinInsuranceSurcharge.Text = "";
				}
			}

			msTextBox msDiscountBand = (msTextBox)customerProfileMultiPage.FindControl("msDiscountBand");
			if(msDiscountBand != null)
			{
				if(drCurrent["discount_band"]!= null && (!drCurrent["discount_band"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					msDiscountBand.Text = drCurrent["discount_band"].ToString();
				}
				else
				{
					msDiscountBand.Text = "";
				}
			}
			
			msTextBox msDensityFactor = (msTextBox)customerProfileMultiPage.FindControl("msDensityFactor");
			if(msDensityFactor != null)
			{
				if(drCurrent["density_factor"]!= null && (!drCurrent["density_factor"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					msDensityFactor.Text = Convert.ToDecimal(drCurrent["density_factor"]).ToString("0");
				}
				else
				{
					msDensityFactor.Text = "";
				}
			}
			//end by Aoo.
			TextBox txtRefAddress2 = (TextBox)customerProfileMultiPage.FindControl("txtRefAddress2");
			if(txtRefAddress2 != null)
			{
				if(drCurrentRef["address2"]!= null && (!drCurrentRef["address2"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					txtRefAddress2.Text = drCurrentRef["address2"].ToString();
				}
				else
				{
					txtRefAddress2.Text = "";
				}
			}
			

			TextBox txtRefCountry = (TextBox)customerProfileMultiPage.FindControl("txtRefCountry");
			if(txtRefCountry != null)
			{
				if(drCurrentRef["country"]!= null && (!drCurrentRef["country"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					txtRefCountry.Text = drCurrentRef["country"].ToString();
				}
				else
				{
					txtRefCountry.Text = "";
				}
			}
			

			msTextBox txtRefZipcode = (msTextBox)customerProfileMultiPage.FindControl("txtRefZipcode");
			TextBox txtRefState = (TextBox)customerProfileMultiPage.FindControl("txtRefState");

			if(txtRefZipcode != null)
			{
				if(drCurrentRef["zipcode"]!= null && (!drCurrentRef["zipcode"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					txtRefZipcode.Text = drCurrentRef["zipcode"].ToString();
					Zipcode zipcode = new Zipcode();
					zipcode.Populate(utility.GetAppID(),utility.GetEnterpriseID(),txtRefZipcode.Text);
					zipcode.Populate(utility.GetAppID(),utility.GetEnterpriseID(),zipcode.Country,txtRefZipcode.Text);
					txtRefState.Text = zipcode.StateName;

				}
				else
				{
					txtRefZipcode.Text = "";
					txtRefState.Text = "";

				}
			}
			
			TextBox txtRefTelephone = (TextBox)customerProfileMultiPage.FindControl("txtRefTelephone");
			if(txtRefTelephone != null)
			{
				if(drCurrentRef["telephone"]!= null && (!drCurrentRef["telephone"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					txtRefTelephone.Text = drCurrentRef["telephone"].ToString();
				}
				else
				{
					txtRefTelephone.Text = "";
				}
			}			

			TextBox txtRefFax = (TextBox)customerProfileMultiPage.FindControl("txtRefFax");
			if(txtRefFax != null)
			{
				if(drCurrentRef["fax"]!= null && (!drCurrentRef["fax"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					txtRefFax.Text = drCurrentRef["fax"].ToString();
				}
				else
				{
					txtRefFax.Text = "";
				}
			}

			CheckBox chkSender = (CheckBox)customerProfileMultiPage.FindControl("chkSender");
			CheckBox chkRecipient = (CheckBox)customerProfileMultiPage.FindControl("chkRecipient");

			if(chkSender != null && chkRecipient != null)
			{
				if(drCurrentRef["snd_rec_type"]!= null && (!drCurrentRef["snd_rec_type"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					String strPaymentMode = drCurrentRef["snd_rec_type"].ToString();

					if(strPaymentMode == "S")
					{
						chkSender.Checked = true;
						chkRecipient.Checked = false;
					}
					else if(strPaymentMode == "R")
					{
						chkSender.Checked = false;
						chkRecipient.Checked = true;
					}
					else if(strPaymentMode == "B")
					{
						chkSender.Checked = true;
						chkRecipient.Checked = true;
					}
				}
				else
				{
					chkSender.Checked = false;
					chkRecipient.Checked = false;
				}
			}			

		}

		private void EnableTxtRefName()
		{

			TextBox txtRefSndRecName = (TextBox)customerProfileMultiPage.FindControl("txtRefSndRecName");
			RequiredFieldValidator validateRefSndRecName = (RequiredFieldValidator)customerProfileMultiPage.FindControl("validateRefSndRecName");
			// Ref Contact Person
			TextBox txtRefContactPerson = (TextBox)customerProfileMultiPage.FindControl("txtRefContactPerson");
			RequiredFieldValidator valContactPerson = (RequiredFieldValidator)customerProfileMultiPage.FindControl("valContactPerson");
			// Ref Address1
			TextBox txtRefAddress1 = (TextBox)customerProfileMultiPage.FindControl("txtRefAddress1");
			RequiredFieldValidator valAddress1 = (RequiredFieldValidator)customerProfileMultiPage.FindControl("valAddress1");
			// Ref Telephone
			TextBox txtRefTelephone = (TextBox)customerProfileMultiPage.FindControl("txtRefTelephone");
			RequiredFieldValidator valTelephone = (RequiredFieldValidator)customerProfileMultiPage.FindControl("valTelephone");
			// Ref Zipcode
			TextBox txtRefZipcode = (TextBox)customerProfileMultiPage.FindControl("txtRefZipcode");
			RequiredFieldValidator valZipcode = (RequiredFieldValidator)customerProfileMultiPage.FindControl("valZipcode");

			if(txtRefSndRecName != null)
			{
				if((int)ViewState["RefMode"] == (int) ScreenMode.Insert && (int)ViewState["RefOperation"] == (int)Operation.None)
				{
					txtRefSndRecName.Enabled = true;
					
				}
				else if((int)ViewState["RefMode"] == (int) ScreenMode.Insert && (int)ViewState["RefOperation"] == (int)Operation.Saved)
				{
					txtRefSndRecName.Enabled = false;
				}
				else if((int)ViewState["RefMode"] == (int) ScreenMode.ExecuteQuery)
				{
					txtRefSndRecName.Enabled = false;
				}
				else if((int)ViewState["RefMode"] == (int) ScreenMode.Query)
				{
					txtRefSndRecName.Enabled = true;;
				}
				else
				{
					txtRefSndRecName.Enabled = false;
				}


				if(validateRefSndRecName != null)
				{
					if(txtRefSndRecName.Enabled && IsRefTabEnabled())
					{
						validateRefSndRecName.ControlToValidate = "txtRefSndRecName";
					}
					else
					{
						validateRefSndRecName.ControlToValidate = "supposrtValidator";
					}
				}
				//
				if(valContactPerson != null)
				{
					if(txtRefSndRecName.Enabled && txtRefContactPerson.Enabled && IsRefTabEnabled())
					{
						valContactPerson.ControlToValidate = "txtRefContactPerson";
					}
					else
					{
						valContactPerson.ControlToValidate = "suppContactPerson";
					}
				}
				//
				if(valAddress1 != null)
				{
					if(txtRefSndRecName.Enabled && txtRefAddress1.Enabled && IsRefTabEnabled())
					{
						valAddress1.ControlToValidate = "txtRefAddress1";
					}
					else
					{
						valAddress1.ControlToValidate = "suppAddress1";
					}
				}
				//
				if(valTelephone != null)
				{
					if(txtRefSndRecName.Enabled && txtRefTelephone.Enabled && IsRefTabEnabled())
					{
						valTelephone.ControlToValidate = "txtRefTelephone";
					}
					else
					{
						valTelephone.ControlToValidate = "suppTelephone";
					}
				}
				//
				if(valZipcode != null)
				{
					if(txtRefSndRecName.Enabled && txtRefZipcode.Enabled && IsRefTabEnabled())
					{
						valZipcode.ControlToValidate = "txtRefZipcode";
					}
					else
					{
						valZipcode.ControlToValidate = "suppZipcode";
					}
				}
			}
		}

		private void DisplayRefRecNum()
		{
			int iCurrentRec = (System.Convert.ToInt32(ViewState["currentRefPage"]) + 1) + (Convert.ToInt32(ViewState["currentRefSet"]) * m_iSetSize) ;
			if(lblNumRefRec != null)
			{
				if(!utility.GetUserCulture().ToUpper().Equals("EN-US"))
				{
					lblNumRec.Text = ""+ iCurrentRec + " " + Utility.GetLanguageText(ResourceType.ScreenLabel, "of", utility.GetUserCulture()) + " " + m_sdsCustomerProfile.QueryResultMaxSize + " " + Utility.GetLanguageText(ResourceType.ScreenLabel, "record(s)", utility.GetUserCulture());
				}
				else
				{
					lblNumRec.Text = ""+ iCurrentRec + " of " + m_sdsCustomerProfile.QueryResultMaxSize + " record(s)";
				}
			}
		}

		private void ClearRefPageForInsert()
		{
			m_sdsReference = SysDataMgrDAL.GetEmptyReferenceDS(1);
			Session["SESSION_DS2"] = m_sdsReference;


			ViewState["IsRefTextChanged"] = false;
			ViewState["currentRefPage"] = 0;

			DisplayCurrentRefPage();
			
			//lblRefNumRec.Text = "";
			lblCPErrorMessage.Text = "";

		}

		private void EnableRefPage(bool enable)
		{
			
			TextBox txtRefSndRecName = (TextBox)customerProfileMultiPage.FindControl("txtRefSndRecName");

			if(txtRefSndRecName != null)
			{
				txtRefSndRecName.Enabled = enable;
			}

			TextBox txtRefContactPerson = (TextBox)customerProfileMultiPage.FindControl("txtRefContactPerson");

			if(txtRefContactPerson != null)
			{
				txtRefContactPerson.Enabled = enable;
			}
			
			TextBox txtRefEmail = (TextBox)customerProfileMultiPage.FindControl("txtRefEmail");

			if(txtRefEmail != null)
			{
				txtRefEmail.Enabled = enable;				
			}

			TextBox txtRefAddress1 = (TextBox)customerProfileMultiPage.FindControl("txtRefAddress1");

			if(txtRefAddress1 != null)
			{
				txtRefAddress1.Enabled = enable;
			}
			

			TextBox txtRefAddress2 = (TextBox)customerProfileMultiPage.FindControl("txtRefAddress2");

			if(txtRefAddress2 != null)
			{
				txtRefAddress2.Enabled = enable;
			}
			

			TextBox txtRefCountry = (TextBox)customerProfileMultiPage.FindControl("txtRefCountry");

			if(txtRefCountry != null)
			{
				txtRefCountry.Enabled = enable;
			}
			

			msTextBox txtRefZipcode = (msTextBox)customerProfileMultiPage.FindControl("txtRefZipcode");

			if(txtRefZipcode != null)
			{
				txtRefZipcode.Enabled = enable;
			}
			

			TextBox txtRefTelephone = (TextBox)customerProfileMultiPage.FindControl("txtRefTelephone");

			if(txtRefTelephone != null)
			{
				txtRefTelephone.Enabled = enable;
			}



			TextBox txtRefFax = (TextBox)customerProfileMultiPage.FindControl("txtRefFax");

			if(txtRefFax != null)
			{
				txtRefFax.Enabled	= enable;
			}

			CheckBox chkSender = (CheckBox)customerProfileMultiPage.FindControl("chkSender");
			CheckBox chkRecipient = (CheckBox)customerProfileMultiPage.FindControl("chkRecipient");

			if(chkSender != null && chkRecipient != null)
			{
				chkSender.Enabled = enable;
				chkRecipient.Enabled = enable;
			}

			Button btnRefSave = (Button)customerProfileMultiPage.FindControl("btnRefSave");
			Button btnRefZipcodeSearch = (Button)customerProfileMultiPage.FindControl("btnRefZipcodeSearch");

			btnRefSave.Enabled = enable;
			btnRefZipcodeSearch.Enabled = enable;

		}

		private void btnRefSave_Click(object sender, System.EventArgs e)
		{
			FillReferenceDataRow((int)ViewState["currentRefPage"]);
			
			String strCustID = (String)ViewState["currentCustomer"];
			CheckBox chkS=(CheckBox) customerProfileMultiPage.FindControl("chkSender");
			CheckBox chkR=(CheckBox) customerProfileMultiPage.FindControl("chkRecipient");
			if (chkS.Checked==false && chkR.Checked==false)
			{
				lblCPErrorMessage.Text=Utility.GetLanguageText(ResourceType.UserMessage,"SEL_REF_TYPE",utility.GetUserCulture());
				return;
			}
			else
			{
				lblCPErrorMessage.Text="";
			}
			
			int iOperation = (int)ViewState["RefOperation"];
						
			switch(iOperation)
			{
			
				case (int)Operation.Update:
			
					DataSet dsToUpdate = m_sdsReference.ds.GetChanges();
			
					try
					{
						SysDataMgrDAL.ModifyReferenceDS(utility.GetAppID(),utility.GetEnterpriseID(),strCustID,dsToUpdate);
//						Button btnRefInsert = (Button)customerProfileMultiPage.FindControl("btnRefInsert");
//			
//						if(btnRefInsert != null)
//						{
//							btnRefInsert.Enabled	= true;
//						}
			
						ViewState["IsRefTextChanged"] = false;
						ChangeRefState();
						m_sdsReference.ds.Tables[0].Rows[(int)ViewState["currentRefPage"]].AcceptChanges();
			
						if(lblNumRefRec != null)
						{
							int iCurrentRec = (int)ViewState["currentRefPage"] + 1;
							lblCPErrorMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"UPD_SUCCESSFULLY",utility.GetUserCulture());
						}
			
			
						Logger.LogDebugInfo("CustomerProfile","btnSave_Click","INF004","save in modify mode..");
					}
					catch(ApplicationException appException)
					{
						String strMsg = appException.Message;
			
						if(strMsg.IndexOf("FK") != -1 )
						{
							if(strMsg.IndexOf("Zipcode") != -1)
							{
								lblCPErrorMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"INVALID_ZIP",utility.GetUserCulture());
							}
						}
						if(strMsg.IndexOf("child record") != -1 )
						{					
							lblCPErrorMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"INVALID_ZIP",utility.GetUserCulture());
						}
						if(strMsg.IndexOf("parent") != -1 )
						{
							lblCPErrorMessage.Text =Utility.GetLanguageText(ResourceType.UserMessage,"INVALID_ZIP",utility.GetUserCulture());
						}
						return;
									
					}
					break;
			
				case (int)Operation.Insert:
			
					DataSet dsToInsert = m_sdsReference.ds.GetChanges();
					try
					{

						SysDataMgrDAL.AddReferenceDS(utility.GetAppID(),utility.GetEnterpriseID(),strCustID,dsToInsert);
			
//						Button btnRefInsert = (Button)customerProfileMultiPage.FindControl("btnRefInsert");
//			
//						if(btnRefInsert != null)
//						{
//							btnRefInsert.Enabled	= true;
//						}
			
						TextBox txtRefSndRecName = (TextBox)customerProfileMultiPage.FindControl("txtRefSndRecName");
						if(txtRefSndRecName != null)
						{
							txtRefSndRecName.Enabled = false;
						}
			
						ViewState["IsRefTextChanged"] = false;
						ChangeRefState();
						m_sdsReference.ds.Tables[0].Rows[(int)ViewState["currentRefPage"]].AcceptChanges();
			
						if(lblNumRefRec != null)
						{
							lblCPErrorMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"INS_SUCCESSFULLY",utility.GetUserCulture());
						}
			
						Logger.LogDebugInfo("CustomerProfile","btnSave_Click","INF004","save in Insert mode");
					}
					catch(ApplicationException appException)
					{
						String strMsg = appException.Message;
			
						if(strMsg.IndexOf("duplicate key") != -1 )
						{
							lblCPErrorMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"DUP_KEY_FOUND_NAME",utility.GetUserCulture());
						}
						else if(strMsg.IndexOf("FK") != -1 )
						{
							if(strMsg.IndexOf("Zipcode") != -1)
							{
								lblCPErrorMessage.Text =Utility.GetLanguageText(ResourceType.UserMessage,"INVALID_ZIP",utility.GetUserCulture());
							}
						}
						else if(strMsg.IndexOf("child record") != -1 )
						{
							lblCPErrorMessage.Text =Utility.GetLanguageText(ResourceType.UserMessage,"INVALID_ZIP",utility.GetUserCulture());
						}
			
						else if(strMsg.IndexOf("parent") != -1 )
						{
							lblCPErrorMessage.Text =Utility.GetLanguageText(ResourceType.UserMessage,"INVALID_ZIP",utility.GetUserCulture());
						}
						else
						{
							lblCPErrorMessage.Text = strMsg;
						}
			
						return;
					}

					break;
			
			}

		}

		private void btnRefDelete_Click(object sender, System.EventArgs e)
		{
		
		}

		private void FillReferenceDataRow(int iCurrentRow)
		{
			DataRow drCurrentRef = m_sdsReference.ds.Tables[0].Rows[iCurrentRow];

			TextBox txtRefSndRecName = (TextBox)customerProfileMultiPage.FindControl("txtRefSndRecName");

			if(txtRefSndRecName != null && txtRefSndRecName.Text != "")
			{
				drCurrentRef["snd_rec_name"] = txtRefSndRecName.Text;
			}

			TextBox txtRefContactPerson = (TextBox)customerProfileMultiPage.FindControl("txtRefContactPerson");

			if(txtRefContactPerson != null && txtRefContactPerson.Text != "")
			{
				drCurrentRef["contact_person"] = txtRefContactPerson.Text;
			}

			TextBox txtRefAddress1 = (TextBox)customerProfileMultiPage.FindControl("txtRefAddress1");

			if(txtRefAddress1 != null && txtRefAddress1.Text != "")
			{
				drCurrentRef["address1"] = txtRefAddress1.Text;
			}

			TextBox txtRefAddress2 = (TextBox)customerProfileMultiPage.FindControl("txtRefAddress2");

			if(txtRefAddress2 != null && txtRefAddress2.Text != "")
			{
				drCurrentRef["address2"] = txtRefAddress2.Text;
			}

			TextBox txtRefCountry = (TextBox)customerProfileMultiPage.FindControl("txtRefCountry");

			if(txtRefCountry != null && txtRefCountry.Text != "")
			{
				drCurrentRef["country"] = txtRefCountry.Text;
			}

			msTextBox txtRefZipcode = (msTextBox)customerProfileMultiPage.FindControl("txtRefZipcode");

			if(txtRefZipcode != null && txtRefZipcode.Text != "")
			{
				drCurrentRef["zipcode"] = txtRefZipcode.Text;
			}

			TextBox txtRefTelephone = (TextBox)customerProfileMultiPage.FindControl("txtRefTelephone");

			if(txtRefTelephone != null && txtRefTelephone.Text != "")
			{
				drCurrentRef["telephone"] = txtRefTelephone.Text;
			}


			TextBox txtRefFax = (TextBox)customerProfileMultiPage.FindControl("txtRefFax");

			if(txtRefFax != null && txtRefFax.Text != "")
			{
				drCurrentRef["fax"] = txtRefFax.Text;
			}

			TextBox txtRefEmail = (TextBox)customerProfileMultiPage.FindControl("txtRefEmail");

			if(txtRefEmail != null && txtRefEmail.Text != "")
			{
				drCurrentRef["email"] = txtRefEmail.Text;
			}

			CheckBox chkSender = (CheckBox)customerProfileMultiPage.FindControl("chkSender");
			CheckBox chkRecipient = (CheckBox)customerProfileMultiPage.FindControl("chkRecipient");

			if(chkSender != null && chkRecipient != null)
			{

				if(chkSender.Checked == true && chkRecipient.Checked == false)
				{
					drCurrentRef["snd_rec_type"] = "S";
				}
				else if(chkSender.Checked == false && chkRecipient.Checked == true)
				{
					drCurrentRef["snd_rec_type"] = "R";
				}
				else if(chkSender.Checked == true && chkRecipient.Checked == true)
				{
					drCurrentRef["snd_rec_type"] = "B";
				}
				else if(chkSender.Checked == false && chkRecipient.Checked == false)
				{
					drCurrentRef["snd_rec_type"] = System.DBNull.Value;
				}
			}
		}

		private void ChangeRefState()
		{
			if((int)ViewState["RefMode"] == (int) ScreenMode.Insert && (int)ViewState["RefOperation"] == (int)Operation.None)
			{
				ViewState["RefOperation"] = Operation.Insert;
			}
			else if((int)ViewState["RefMode"] == (int) ScreenMode.Insert && (int)ViewState["RefOperation"] == (int)Operation.Insert)
			{
				ViewState["RefOperation"] = Operation.Saved;
			}
			else if((int)ViewState["RefMode"] == (int) ScreenMode.Insert && (int)ViewState["RefOperation"] == (int)Operation.Saved)
			{
				ViewState["RefOperation"] = Operation.Update;
			}
			else if((int)ViewState["RefMode"] == (int) ScreenMode.Insert && (int)ViewState["RefOperation"] == (int)Operation.Update)
			{
				ViewState["RefOperation"] = Operation.Saved;
			}
			else if((int)ViewState["RefMode"] == (int) ScreenMode.ExecuteQuery && (int)ViewState["RefOperation"] == (int)Operation.None)
			{
				ViewState["RefOperation"] = Operation.Update;
			}
			else if((int)ViewState["RefMode"] == (int) ScreenMode.ExecuteQuery && (int)ViewState["RefOperation"] == (int)Operation.Update)
			{
				ViewState["RefOperation"] = Operation.None;
			}

		}

		private void txtRefSndRecName_TextChanged(object sender,System.EventArgs e)
		{
			if((bool)ViewState["IsRefTextChanged"] == false)
			{
				ChangeRefState();
				ViewState["IsRefTextChanged"] = true;
			}
		}

		private void txtRefContactPerson_TextChanged(object sender,System.EventArgs e)
		{
			if((bool)ViewState["IsRefTextChanged"] == false)
			{
				ChangeRefState();
				ViewState["IsRefTextChanged"] = true;
			}
		}

		private void txtRefAddress1_TextChanged(object sender,System.EventArgs e)
		{
			if((bool)ViewState["IsRefTextChanged"] == false)
			{
				ChangeRefState();
				ViewState["IsRefTextChanged"] = true;
			}
		}
		// By Aoo
		
		
		
		//		private void msOtherSurchargePercentage_TextChanged(object sender,System.EventArgs e)
		//		{
		//			if((bool)ViewState["IsRefTextChanged"] == false)
		//			{
		//				ChangeRefState();
		//				ViewState["IsRefTextChanged"] = true;
		//			}
		//		}
		//	
		//		private void msOtherSurchargeMinimum_TextChanged(object sender,System.EventArgs e)
		//		{
		//			if((bool)ViewState["IsRefTextChanged"] == false)
		//			{
		//				ChangeRefState();
		//				ViewState["IsRefTextChanged"] = true;
		//			}
		//		}
		//
		//		private void msOtherSurchargeMaximum_TextChanged(object sender,System.EventArgs e)
		//		{
		//			if((bool)ViewState["IsRefTextChanged"] == false)
		//			{
		//				ChangeRefState();
		//				ViewState["IsRefTextChanged"] = true;
		//			}
		//		}
		//
		//		private void msOtherSurchargeDescription_TextChanged(object sender,System.EventArgs e)
		//		{
		//			if((bool)ViewState["IsRefTextChanged"] == false)
		//			{
		//				ChangeRefState();
		//				ViewState["IsRefTextChanged"] = true;
		//			}
		//		}
		//		
		//		private void txtMaxInsuranceSurcharge_TextChanged(object sender,System.EventArgs e)
		//		{
		//			if((bool)ViewState["IsRefTextChanged"] == false)
		//			{
		//				ChangeRefState();
		//				ViewState["IsRefTextChanged"] = true;
		//			}
		//		}
		//
		//		private void txtMinInsuranceSurcharge_TextChanged(object sender,System.EventArgs e)
		//		{
		//			if((bool)ViewState["IsRefTextChanged"] == false)
		//			{
		//				ChangeRefState();
		//				ViewState["IsRefTextChanged"] = true;
		//			}
		//		}
		//
		//		private void msDiscountBand_TextChanged(object sender,System.EventArgs e)
		//		{
		//			if((bool)ViewState["IsRefTextChanged"] == false)
		//			{
		//				ChangeRefState();
		//				ViewState["IsRefTextChanged"] = true;
		//			}
		//		}
		//
		//		private void msOtherSurchargeAmount_TextChanged(object sender,System.EventArgs e)
		//		{
		//			if((bool)ViewState["IsRefTextChanged"] == false)
		//			{
		//				ChangeRefState();
		//				ViewState["IsRefTextChanged"] = true;
		//			}
		//		}
		//
		//		private void msDensityFactor_TextChanged(object sender,System.EventArgs e)
		//		{
		//			if((bool)ViewState["IsRefTextChanged"] == false)
		//			{
		//				ChangeRefState();
		//				ViewState["IsRefTextChanged"] = true;
		//			}
		//		}
		// end By Aoo.
		private void txtRefAddress2_TextChanged(object sender,System.EventArgs e)
		{
			if((bool)ViewState["IsRefTextChanged"] == false)
			{
				ChangeRefState();
				ViewState["IsRefTextChanged"] = true;
			}
		}

		private void txtRefCountry_TextChanged(object sender,System.EventArgs e)
		{
			if((bool)ViewState["IsRefTextChanged"] == false)
			{
				ChangeRefState();
				ViewState["IsRefTextChanged"] = true;
			}
		}

		private void txtRefZipcode_TextChanged(object sender,System.EventArgs e)
		{
			if((bool)ViewState["IsRefTextChanged"] == false)
			{
				ChangeRefState();
				ViewState["IsRefTextChanged"] = true;
			}
		}

		private void txtRefTelephone_TextChanged(object sender,System.EventArgs e)
		{
			if((bool)ViewState["IsRefTextChanged"] == false)
			{
				ChangeRefState();
				ViewState["IsRefTextChanged"] = true;
			}
		}

		private void txtRefFax_TextChanged(object sender,System.EventArgs e)
		{
			if((bool)ViewState["IsRefTextChanged"] == false)
			{
				ChangeRefState();
				ViewState["IsRefTextChanged"] = true;
			}
		}

		private void txtRefEmail_TextChanged(object sender,System.EventArgs e)
		{
			if((bool)ViewState["IsRefTextChanged"] == false)
			{
				ChangeRefState();
				ViewState["IsRefTextChanged"] = true;
			}
		}

		private void chkSender_CheckedChanged(object sender,System.EventArgs e)
		{
			if((bool)ViewState["IsRefTextChanged"] == false)
			{
				ChangeRefState();
				ViewState["IsRefTextChanged"] = true;
			}
		}

		private void chkRecipient_CheckedChanged(object sender,System.EventArgs e)
		{
			if((bool)ViewState["IsRefTextChanged"] == false)
			{
				ChangeRefState();
				ViewState["IsRefTextChanged"] = true;
			}
		}

		// Status and Exception Surcharges Section...

		private void ShowSCForCurrentCustomer()
		{
			ViewState["SCMode"] = ScreenMode.ExecuteQuery;
			ViewState["SCOperation"] = Operation.None;

			//ViewState["QUERY_DS"] = m_sdsStatusCode.ds;
			dgStatusCodes.CurrentPageIndex = 0;
			ShowCurrentSCPage();			
			//btnSCInsert.Enabled = true;
			lblSCMessage.Text = "";
			this.CheckRole();
		}
		protected void btnSCViewAll_Click(object sender, System.EventArgs e)
		{
			ShowSCForCurrentCustomer();
		}

		protected void btnSCInsert_Click(object sender, System.EventArgs e)
		{

			if((int)ViewState["SCMode"] != (int)ScreenMode.Insert || m_sdsStatusCode.ds.Tables[0].Rows.Count >= dgStatusCodes.PageSize)
			{
				m_sdsStatusCode = SysDataMgrDAL.GetEmptyCPStatusCodeDS(0);
			}

			AddRowInSCGrid();	
			dgStatusCodes.EditItemIndex = m_sdsStatusCode.ds.Tables[0].Rows.Count - 1;
			dgStatusCodes.CurrentPageIndex = 0;
			Logger.LogDebugInfo("Customer Profile","btnSCInsert_Click","INF003","Data Grid Items count"+dgStatusCodes.Items.Count);

			ViewState["SCMode"] = ScreenMode.Insert;
			ViewState["SCOperation"] = Operation.Insert;
			btnSCInsert.Enabled = false;
			lblSCMessage.Text = "";
			BindSCGrid();
			ResetDetailsGrid();
			getPageControls(Page);
		}


		public void dgStatusCodes_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			Label lblStatusCode = (Label)dgStatusCodes.SelectedItem.FindControl("lblStatusCode");
			msTextBox txtStatusCode = (msTextBox)dgStatusCodes.SelectedItem.FindControl("txtStatusCode");
			String strStatusCode = null;

			if(lblStatusCode != null)
			{
				strStatusCode = lblStatusCode.Text;
			}

			if(txtStatusCode != null)
			{
				strStatusCode = txtStatusCode.Text;
			}

			ViewState["CurrentSC"] = strStatusCode;
			dgExceptionCodes.CurrentPageIndex = 0;

			Logger.LogDebugInfo("StatusException","dgStatusCodes_SelectedIndexChanged","INF004","updating data grid..."+dgStatusCodes.SelectedIndex+"  : "+strStatusCode);
			ShowCurrentEXPage();

			ViewState["EXMode"] = ScreenMode.ExecuteQuery;

			if((int)ViewState["SCMode"] != (int)ScreenMode.Insert && (int)ViewState["SCOperation"] != (int)Operation.Insert)
			{
				this.CheckRole();
			}

			if((int)ViewState["SCMode"] == (int)ScreenMode.Insert && dgStatusCodes.EditItemIndex == dgStatusCodes.SelectedIndex && (int)ViewState["SCOperation"] == (int)Operation.Insert)
			{
				btnExInsert.Enabled = false;
			}
			else
			{
				btnExInsert.Enabled = true;
				btnExInsert.Enabled=btnSCInsert.Enabled;
			}

			lblSCMessage.Text = "";

		}

		protected void dgStatusCodes_Edit(object sender, DataGridCommandEventArgs e)
		{
			if((int)ViewState["SCMode"] == (int)ScreenMode.Insert && (int) ViewState["SCOperation"] == (int)Operation.Insert && dgStatusCodes.EditItemIndex > 0)
			{
				m_sdsStatusCode.ds.Tables[0].Rows.RemoveAt(dgStatusCodes.EditItemIndex);
				m_sdsStatusCode.QueryResultMaxSize--;
				dgStatusCodes.CurrentPageIndex = 0;
			}

			dgStatusCodes.EditItemIndex = e.Item.ItemIndex;
			ViewState["SCOperation"] = Operation.Update;
			BindSCGrid();
			lblSCMessage.Text = "";
			Logger.LogDebugInfo("StatusException","dgStatusCodes_Edit","INF004","updating data grid...");	
		}
		
		public void dgStatusCodes_Update(object sender, DataGridCommandEventArgs e)
		{

			FillSCDataRow(e.Item,e.Item.ItemIndex);
			String strCustID = (String)ViewState["currentCustomer"];

			int iOperation = (int)ViewState["SCOperation"];
			switch(iOperation)
			{

				case (int)Operation.Update:

					DataSet dsToUpdate = m_sdsStatusCode.ds.GetChanges();
					SysDataMgrDAL.ModifyCPStatusCodeDS(utility.GetAppID(),utility.GetEnterpriseID(),strCustID,dsToUpdate);
					btnSCInsert.Enabled = true;
					m_sdsStatusCode.ds.Tables[0].Rows[e.Item.ItemIndex].AcceptChanges();
					Logger.LogDebugInfo("StatusException","dgStatusCodes_OnUpdate","INF004","update in modify mode..");

					break;

				case (int)Operation.Insert:

					DataSet dsToInsert = m_sdsStatusCode.ds.GetChanges();
					try
					{
						SysDataMgrDAL.AddCPStatusCodeDS(utility.GetAppID(),utility.GetEnterpriseID(),strCustID,dsToInsert);
						btnSCInsert.Enabled = true;
					}
					catch(ApplicationException appException)
					{
						String strMsg = appException.Message;
						strMsg = appException.InnerException.Message;
						strMsg = appException.InnerException.InnerException.Message;
						if(strMsg.IndexOf("duplicate key") != -1 || strMsg.IndexOf("unique") != -1)
						{
							lblSCMessage.Text = "Duplicate Status Code is not allowed. Please select another status code name.";							
						}
						return;
						
					}
					m_sdsStatusCode.ds.Tables[0].Rows[e.Item.ItemIndex].AcceptChanges();
					Logger.LogDebugInfo("StatusException","dgStatusCodes_OnUpdate","INF004","update in Insert mode");
					break;

			}
			dgStatusCodes.EditItemIndex = -1;
			ViewState["SCOperation"] = Operation.None;
			lblSCMessage.Text = "";
			BindSCGrid();
		}

		protected void dgStatusCodes_Cancel(object sender, DataGridCommandEventArgs e)
		{
			dgStatusCodes.EditItemIndex = -1;

			if((int)ViewState["SCMode"] == (int)ScreenMode.Insert && (int)ViewState["SCOperation"] == (int)Operation.Insert)
			{
				m_sdsStatusCode.ds.Tables[0].Rows.RemoveAt(e.Item.ItemIndex);
				m_sdsStatusCode.QueryResultMaxSize--;
				dgStatusCodes.CurrentPageIndex = 0;
			}
			ViewState["SCOperation"] = Operation.None;
			BindSCGrid();
			btnSCInsert.Enabled = true;
			lblSCMessage.Text = "";
			Logger.LogDebugInfo("StatusException","dgStatusCodes_Cancel","INF004","updating data grid...");			
		}

		public void dgStatusCodes_Delete(object sender, DataGridCommandEventArgs e)
		{
			msTextBox txtStatusCode = (msTextBox)e.Item.FindControl("txtStatusCode");
			String strCustID = (String)ViewState["currentCustomer"];

			Label lblStatusCode = (Label)e.Item.FindControl("lblStatusCode");
			String strStatusCode = null;
			if(txtStatusCode != null)
			{
				strStatusCode = txtStatusCode.Text;
			}

			if(lblStatusCode != null)
			{
				strStatusCode = lblStatusCode.Text;
			}

			try
			{
				SysDataMgrDAL.DeleteCPStatusCodeDS(utility.GetAppID(),utility.GetEnterpriseID(),strCustID,strStatusCode);
			}
			catch(ApplicationException appException)
			{
				String strMsg = appException.Message;
				if(strMsg.IndexOf("FK") != -1 || strMsg.IndexOf("child record") != -1)
				{
					lblSCMessage.Text = "Error Deleting status code. Status Code or Exception code being used in some transaction(s)";					
				}
				return;
			}

			if((int)ViewState["SCMode"] == (int)ScreenMode.Insert)
			{
				m_sdsStatusCode.ds.Tables[0].Rows.RemoveAt(e.Item.ItemIndex);
				m_sdsStatusCode.QueryResultMaxSize--;
				dgStatusCodes.CurrentPageIndex = 0;

				if((int) ViewState["SCOperation"] == (int)Operation.Insert && dgStatusCodes.EditItemIndex > 0)
				{
					m_sdsStatusCode.ds.Tables[0].Rows.RemoveAt(dgStatusCodes.EditItemIndex-1);
					m_sdsStatusCode.QueryResultMaxSize--;
				}
				dgStatusCodes.EditItemIndex = -1;
				dgStatusCodes.SelectedIndex = -1;
				BindSCGrid();
				ResetDetailsGrid();
			}
			else
			{
				ShowCurrentSCPage();
			}

			Logger.LogDebugInfo("StatusException","dgStatusCodes_Delete","INF004","Deleted row in Status_Code table..");
			ViewState["SCOperation"] = Operation.None;
			btnSCInsert.Enabled = true;
		}

		private void BindSCGrid()
		{
			dgStatusCodes.VirtualItemCount = System.Convert.ToInt32(m_sdsStatusCode.QueryResultMaxSize);
			dgStatusCodes.DataSource = m_sdsStatusCode.ds;
			dgStatusCodes.DataBind();
			Session["SESSION_DS3"] = m_sdsStatusCode;
		}

		private void AddRowInSCGrid()
		{
			SysDataMgrDAL.AddNewRowInCPStatusCodeDS(m_sdsStatusCode);
			Session["SESSION_DS3"] = m_sdsStatusCode;
		}

		public void dgStatusCodes_Button(object sender, DataGridCommandEventArgs e)
		{
			if(!e.Item.Cells[4].Enabled)
			{
				return;
			}
			String strCmdNm = e.CommandName;
			if(strCmdNm.Equals("search"))
			{
				msTextBox txtStatusCode = (msTextBox)e.Item.FindControl("txtStatusCode");
				//msTextBox txtStatusCode = (msTextBox)dgStatusCodes.Items[e.Item.ItemIndex].FindControl("txtStatusCode");

				String strStatusCodeClientID = null;
				String strStatusDescriptionClientID = null;

				String strStatusCode = null;
				
				if(txtStatusCode != null)
				{
					strStatusCodeClientID = txtStatusCode.ClientID;
					strStatusCode = txtStatusCode.Text;
				}

				TextBox txtStatusDescription = (TextBox)e.Item.FindControl("txtStatusDescription");
				if(txtStatusDescription != null)
				{
					strStatusDescriptionClientID = txtStatusDescription.ClientID;
				}

				String sUrl = "StatusCodePopup1.aspx?FORMID=CustomerProfile&STATUSCODE="+strStatusCode+"&STATUSCODE_CID="+strStatusCodeClientID+"&SCDESCRIPTION_CID="+strStatusDescriptionClientID;
				ArrayList paramList = new ArrayList();
				paramList.Add(sUrl);
				String sScript = Utility.GetScript("openWindowParam.js",paramList);
				Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
			}
			
		}
		
		protected void dgStatusCodes_Bound(object sender, DataGridItemEventArgs e)
		{
			if(e.Item.ItemIndex == -1)
			{
				return;
			}

			DataRow drSelected = m_sdsStatusCode.ds.Tables[0].Rows[e.Item.ItemIndex];

			if(drSelected.RowState == DataRowState.Deleted)
			{
				return;
			}

			msTextBox txtStatusCode = (msTextBox)e.Item.FindControl("txtStatusCode");

			int iOperation = (int) ViewState["SCOperation"];
			if(txtStatusCode != null && iOperation == (int)Operation.Update)
			{
				txtStatusCode.Enabled = false;
			}

			if(e.Item.ItemType == ListItemType.EditItem && (int)ViewState["SCMode"] == (int)ScreenMode.Insert && (int)ViewState["SCOperation"] == (int)Operation.Insert)
			{
				e.Item.Cells[4].Enabled = true;
			}
			else
			{
				e.Item.Cells[4].Enabled = false;
			} 

			e.Item.Cells[1].Enabled = btnSCInsert.Enabled;
			e.Item.Cells[2].Enabled = btnSCInsert.Enabled;
		}

		protected void dgStatusCodes_PageChange(object sender, DataGridPageChangedEventArgs e)
		{
			dgStatusCodes.CurrentPageIndex = e.NewPageIndex;
			ShowCurrentSCPage();
			Logger.LogDebugInfo("StatusException","dgStatusCodes_PageChange","INF009","On Page Change "+e.NewPageIndex);
		}

		private void ShowCurrentSCPage()
		{
			DataRow drCurrent = m_sdsCustomerProfile.ds.Tables[0].Rows[Convert.ToInt32(ViewState["currentPage"])];

			TextBox txtCustAccDispTab4 = (TextBox)customerProfileMultiPage.FindControl("txtCustAccDispTab4");
			

			if(txtCustAccDispTab4 != null)
			{
				if(drCurrent["custid"]!= null && (!drCurrent["custid"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					txtCustAccDispTab4.Text = drCurrent["custid"].ToString();
				}
				else
				{
					txtCustAccDispTab4.Text = "";
				}
			}

			TextBox txtCustNameDispTab4 = (TextBox)customerProfileMultiPage.FindControl("txtCustNameDispTab4");


			if(txtCustNameDispTab4 != null)
			{
				if(drCurrent["cust_name"]!= null && (!drCurrent["cust_name"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					txtCustNameDispTab4.Text = drCurrent["cust_name"].ToString();
				}
				else
				{
					txtCustNameDispTab4.Text = "";
				}
			}

			int iStartIndex = dgStatusCodes.CurrentPageIndex * dgStatusCodes.PageSize;
			String strCustID = (String)ViewState["currentCustomer"];
			m_sdsStatusCode = SysDataMgrDAL.GetCPStatusCodeDS(utility.GetAppID(),utility.GetEnterpriseID(),strCustID,iStartIndex,dgStatusCodes.PageSize);
			decimal pgCnt = (m_sdsStatusCode.QueryResultMaxSize - 1)/dgStatusCodes.PageSize;
			if(pgCnt < dgStatusCodes.CurrentPageIndex)
			{
				dgStatusCodes.CurrentPageIndex = System.Convert.ToInt32(pgCnt);
			}
			dgStatusCodes.SelectedIndex = -1;
			dgStatusCodes.EditItemIndex = -1;
			lblSCMessage.Text = "";

			BindSCGrid();
			ResetDetailsGrid();
		}

		protected void btnExInsert_Click(object sender, System.EventArgs e)
		{
			//---- If coming to any other mode to Insert Mode then create the empty data set.

			if((int)ViewState["EXMode"] != (int)ScreenMode.Insert || m_sdsExceptionCode.ds.Tables[0].Rows.Count >= dgExceptionCodes.PageSize)
			{
				m_sdsExceptionCode = SysDataMgrDAL.GetEmptyCPExceptionCodeDS(0);
			}

			AddRowInExGrid();	
			dgExceptionCodes.EditItemIndex = m_sdsExceptionCode.ds.Tables[0].Rows.Count - 1;
			dgExceptionCodes.CurrentPageIndex = 0;
			Logger.LogDebugInfo("StatusException","btnExInsert_Click","INF003","Data Grid Items count"+dgExceptionCodes.Items.Count);
		
			ViewState["EXMode"] = ScreenMode.Insert;
			ViewState["EXOperation"] = Operation.Insert;
			BindExGrid();
			btnExInsert.Enabled = false;
			getPageControls(Page);
		}

		protected void dgExceptionCodes_Edit(object sender, DataGridCommandEventArgs e)
		{
			if((int)ViewState["EXMode"] == (int)ScreenMode.Insert && (int) ViewState["EXOperation"] == (int)Operation.Insert && dgExceptionCodes.EditItemIndex > 0)
			{
				m_sdsExceptionCode.ds.Tables[0].Rows.RemoveAt(dgExceptionCodes.EditItemIndex);
				m_sdsExceptionCode.QueryResultMaxSize--;
				dgExceptionCodes.CurrentPageIndex = 0;
			}
		
			dgExceptionCodes.EditItemIndex = e.Item.ItemIndex;
			ViewState["EXOperation"] = Operation.Update;
			BindExGrid();
			lblEXMessage.Text = "";
			Logger.LogDebugInfo("StatusException","dgExceptionCodes_Edit","INF004","updating data grid...");
		}

		public void dgExceptionCodes_Update(object sender, DataGridCommandEventArgs e)
		{
			FillEXDataRow(e.Item,e.Item.ItemIndex);
			String strCustID = (String)ViewState["currentCustomer"];


			int iOperation = (int)ViewState["EXOperation"];

			String strStatusCode = (String)ViewState["CurrentSC"];
			switch(iOperation)
			{

				case (int)Operation.Update:

					DataSet dsToUpdate = m_sdsExceptionCode.ds.GetChanges();
					SysDataMgrDAL.ModifyCPExceptionCodeDS(utility.GetAppID(),utility.GetEnterpriseID(),strCustID,strStatusCode,dsToUpdate);
					btnExInsert.Enabled = true;
					m_sdsExceptionCode.ds.Tables[0].Rows[e.Item.ItemIndex].AcceptChanges();
					Logger.LogDebugInfo("StatusException","dgExceptionCodes_OnUpdate","INF004","update in modify mode..");

					break;

				case (int)Operation.Insert:

					DataSet dsToInsert = m_sdsExceptionCode.ds.GetChanges();
					try
					{
						SysDataMgrDAL.AddCPExceptionCodeDS(utility.GetAppID(),utility.GetEnterpriseID(),strCustID,strStatusCode,dsToInsert);
						btnExInsert.Enabled = true;
					}
					catch(ApplicationException appException)
					{
						String strMsg = appException.Message;
						strMsg = appException.InnerException.Message;
						strMsg = appException.InnerException.InnerException.Message;
						if(strMsg.IndexOf("duplicate key") != -1 || strMsg.IndexOf("unique")!=-1)
						{
							lblEXMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"DUP_KEY_FOUND_EXP_CODE_NO",utility.GetUserCulture());
						}
						else if (strMsg.IndexOf("conflict") != -1 ) 
						{
							lblEXMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"INVALID_EXCEPTION_CODE",utility.GetUserCulture());
						}
						else if (strMsg.IndexOf("parent") != -1 ) 
						{
							lblEXMessage.Text = "Exception Code is not valid. Please use a valid Exception Code.";
						}
						else
						{
							lblEXMessage.Text = strMsg;
						}
						return;
						
					}

					m_sdsExceptionCode.ds.Tables[0].Rows[e.Item.ItemIndex].AcceptChanges();
					Logger.LogDebugInfo("StatusException","dgExceptionCodes_OnUpdate","INF004","update in Insert mode");
					break;

			}
			dgExceptionCodes.EditItemIndex = -1;
			ViewState["EXOperation"] = Operation.None;
			BindExGrid();		
			lblEXMessage.Text = "";
		}

		protected void dgExceptionCodes_Cancel(object sender, DataGridCommandEventArgs e)
		{
			dgExceptionCodes.EditItemIndex = -1;
			if((int)ViewState["EXMode"] == (int)ScreenMode.Insert && (int)ViewState["EXOperation"] == (int)Operation.Insert)
			{
				m_sdsExceptionCode.ds.Tables[0].Rows.RemoveAt(e.Item.ItemIndex);
				m_sdsExceptionCode.QueryResultMaxSize--;
				dgExceptionCodes.CurrentPageIndex = 0;
			}
			btnExInsert.Enabled = true;
			lblEXMessage.Text = "";

			ViewState["EXOperation"] = Operation.None;
			BindExGrid();
			Logger.LogDebugInfo("StatusException","dgExceptionCodes_Cancel","INF004","updating data grid...");			
		}

		protected void dgExceptionCodes_Delete(object sender, DataGridCommandEventArgs e)
		{
			msTextBox txtExceptionCode = (msTextBox)e.Item.FindControl("txtExceptionCode");
			Label lblExceptionCode = (Label)e.Item.FindControl("lblExceptionCode");
			String strExceptionCode = null;
			if(txtExceptionCode != null)
			{
				strExceptionCode = txtExceptionCode.Text;
			}

			if(lblExceptionCode != null)
			{
				strExceptionCode = lblExceptionCode.Text;
			}

			String strStatusCode = (String)ViewState["CurrentSC"];
			String strCustID = (String)ViewState["currentCustomer"];

			try
			{
				SysDataMgrDAL.DeleteCPExceptionCodeDS(utility.GetAppID(),utility.GetEnterpriseID(),strCustID,strStatusCode,strExceptionCode);
			}
			catch(ApplicationException appException)
			{
				String strMsg = appException.Message;
				if(strMsg.IndexOf("FK") != -1 || strMsg.IndexOf("child record") != -1)
				{
					lblEXMessage.Text =Utility.GetLanguageText(ResourceType.UserMessage,"ERR_DEL_EXP_TRANS",utility.GetUserCulture());
					
				}

				return;
			}

			if((int)ViewState["EXMode"] == (int)ScreenMode.Insert)
			{
				m_sdsExceptionCode.ds.Tables[0].Rows.RemoveAt(e.Item.ItemIndex);
				m_sdsExceptionCode.QueryResultMaxSize--;
				dgExceptionCodes.CurrentPageIndex = 0;

				if((int) ViewState["EXOperation"] == (int)Operation.Insert && dgExceptionCodes.EditItemIndex > 0)
				{
					m_sdsExceptionCode.ds.Tables[0].Rows.RemoveAt(dgExceptionCodes.EditItemIndex-1);
					m_sdsExceptionCode.QueryResultMaxSize--;
				}
				dgExceptionCodes.EditItemIndex = -1;
				BindExGrid();
			}
			else
			{
				ShowCurrentEXPage();
			}


			Logger.LogDebugInfo("StatusException","dgExceptionCodes_Delete","INF004","Deleted row in Exception_Code table..");

			ViewState["EXOperation"] = Operation.None;
			btnExInsert.Enabled = true;
			lblEXMessage.Text = "";
		}

		private void BindExGrid()
		{
			dgExceptionCodes.VirtualItemCount = System.Convert.ToInt32(m_sdsExceptionCode.QueryResultMaxSize);
			dgExceptionCodes.DataSource = m_sdsExceptionCode.ds;
			dgExceptionCodes.DataBind();
			Session["SESSION_DS4"] = m_sdsExceptionCode;
		}

		private void AddRowInExGrid()
		{
			SysDataMgrDAL.AddNewRowInCPExceptionCodeDS(m_sdsExceptionCode);
			Session["SESSION_DS4"] = m_sdsExceptionCode;
		}

		public void dgExceptionCodes_Button(object sender, DataGridCommandEventArgs e)
		{
			String strCmdNm = e.CommandName;
			if(strCmdNm.Equals("search"))
			{
				msTextBox txtExceptionCode = (msTextBox)e.Item.FindControl("txtExceptionCode");


				String strExceptionCodeClientID = null;
				String strExceptionDescriptionClientID = null;
				String strStatusCode = (String)ViewState["CurrentSC"];

				String strExceptionCode = null;
				
				if(txtExceptionCode != null)
				{
					strExceptionCodeClientID = txtExceptionCode.ClientID;
					strExceptionCode = txtExceptionCode.Text;
				}

				TextBox txtExceptionDescription = (TextBox)e.Item.FindControl("txtExceptionDescription");
				if(txtExceptionDescription != null)
				{
					strExceptionDescriptionClientID = txtExceptionDescription.ClientID;
				}

				String sUrl = "ExceptionCodePopup.aspx?FORMID=CustomerProfile&STATUSCODE="+strStatusCode+"&EXCEPTIONCODE="+strExceptionCode+"&EXCEPTIONCODE_CID="+strExceptionCodeClientID+"&EXDESCRIPTION_CID="+strExceptionDescriptionClientID;
				ArrayList paramList = new ArrayList();
				paramList.Add(sUrl);
				String sScript = Utility.GetScript("openWindowParam.js",paramList);
				Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
			}
			
		}

		protected void dgExceptionCodes_Bound(object sender, DataGridItemEventArgs e)
		{
			if(e.Item.ItemIndex == -1)
			{
				return;
			}

			DataRow drSelected = m_sdsExceptionCode.ds.Tables[0].Rows[e.Item.ItemIndex];

			if(drSelected.RowState == DataRowState.Deleted)
			{
				return;
			}

			msTextBox txtExceptionCode = (msTextBox)e.Item.FindControl("txtExceptionCode");
			
			int iOperation = (int) ViewState["EXOperation"];
			
			if(txtExceptionCode != null && iOperation == (int)Operation.Update)
			{
				txtExceptionCode.Enabled = false;
			}

			if(e.Item.ItemType == ListItemType.EditItem && (int)ViewState["EXMode"] == (int)ScreenMode.Insert && (int)ViewState["EXOperation"] == (int)Operation.Insert)
			{
				e.Item.Cells[3].Enabled = true;
			}
			else
			{
				e.Item.Cells[3].Enabled = false;
			} 

		}

		protected void dgExceptionCodes_PageChange(object sender, DataGridPageChangedEventArgs e)
		{
			dgExceptionCodes.CurrentPageIndex = e.NewPageIndex;
			ShowCurrentEXPage();
			Logger.LogDebugInfo("StatusException","dgExceptionCodes_PageChange","INF009","On Page Change "+e.NewPageIndex);
		}

		private void ShowCurrentEXPage()
		{
			int iStartIndex = dgExceptionCodes.CurrentPageIndex * dgExceptionCodes.PageSize;
			String strStatusCode = (String)ViewState["CurrentSC"];
			String strCustID = (String)ViewState["currentCustomer"];

			m_sdsExceptionCode = SysDataMgrDAL.GetCPExceptionCodeDS(utility.GetAppID(),utility.GetEnterpriseID(),strCustID,strStatusCode,iStartIndex,dgExceptionCodes.PageSize);

			decimal pgCnt = (m_sdsExceptionCode.QueryResultMaxSize - 1)/dgExceptionCodes.PageSize;
			if(pgCnt < dgExceptionCodes.CurrentPageIndex)
			{
				dgExceptionCodes.CurrentPageIndex = System.Convert.ToInt32(pgCnt);
			}
			dgExceptionCodes.EditItemIndex = -1;
			BindExGrid();
			lblEXMessage.Text = "";
		}

		private void FillSCDataRow(DataGridItem item, int drIndex)
		{

			DataRow drCurrent = m_sdsStatusCode.ds.Tables[0].Rows[drIndex];
			msTextBox txtStatusCode = (msTextBox)item.FindControl("txtStatusCode");
			if(txtStatusCode != null)
			{
				
				drCurrent["status_code"] = txtStatusCode.Text;
			}

			TextBox txtStatusDescription = (TextBox)item.FindControl("txtStatusDescription");
			if(txtStatusDescription != null)
			{
				//DataRow drCurrent = m_sdsStatusCode.ds.Tables[0].Rows[drIndex];
				drCurrent["status_description"] = txtStatusDescription.Text;
			}


			msTextBox txtSCChargeAmount = (msTextBox)item.FindControl("txtSCChargeAmount");
			if(txtSCChargeAmount != null)
			{
				//DataRow drCurrent = m_sdsStatusCode.ds.Tables[0].Rows[drIndex];
			
				if(txtSCChargeAmount.Text == "")
				{
					drCurrent["surcharge"] = System.DBNull.Value;
				}
				else
				{
					drCurrent["surcharge"] = txtSCChargeAmount.Text;
				}
			}
		}

		private void FillEXDataRow(DataGridItem item, int drIndex)
		{

			DataRow drCurrent = m_sdsExceptionCode.ds.Tables[0].Rows[drIndex];
			msTextBox txtExceptionCode = (msTextBox)item.FindControl("txtExceptionCode");
			if(txtExceptionCode != null)
			{
				
				drCurrent["exception_code"] = txtExceptionCode.Text;
			}

			TextBox txtExceptionDescription = (TextBox)item.FindControl("txtExceptionDescription");
			if(txtExceptionDescription != null)
			{
				//DataRow drCurrent = m_sdsExceptionCode.ds.Tables[0].Rows[drIndex];
				drCurrent["exception_description"] = txtExceptionDescription.Text;
			}

			msTextBox txtEXChargeAmount = (msTextBox)item.FindControl("txtEXChargeAmount");
			if(txtEXChargeAmount != null)
			{
				//DataRow drCurrent = m_sdsStatusCode.ds.Tables[0].Rows[drIndex];

				if(txtEXChargeAmount.Text == "")
				{
					drCurrent["surcharge"] = System.DBNull.Value;
				}
				else
				{
					drCurrent["surcharge"] = txtEXChargeAmount.Text;
				}
			}

		}

		private void ResetDetailsGrid()
		{
			dgExceptionCodes.CurrentPageIndex = 0;
			btnExInsert.Enabled = false;			
			m_sdsExceptionCode = SysDataMgrDAL.GetEmptyCPExceptionCodeDS(0);

			BindExGrid();

		}		

		public String ShowLocalizedLabel()
		{
			return Utility.GetLanguageText(ResourceType.ModuleName,"Customer Profile","zh-CHS");
		}

		/* Quotation Procedures */

		protected void OnItemBound_Quotation(object sender, DataGridItemEventArgs e)
		{
			if(e.Item.ItemIndex == -1)
			{
				return;
			}
			DataRow drSelected = m_sdsQuotation.ds.Tables[0].Rows[e.Item.ItemIndex];			
			String strStatus=drSelected.ItemArray[3].ToString();
			//DataGrid dgQuotation=(DataGrid) customerProfileMultiPage.FindControl("dgQuotation");
			
			Label lblQuoteStatus =(Label) e.Item.FindControl("lblQuoteStatus");
			if (lblQuoteStatus != null)
			{
				if (strStatus=="D")
				{					
					lblQuoteStatus.Text="Draft";
				}
				else if (strStatus=="Q")
				{
					lblQuoteStatus.Text="Quoted";
				}
				else if	(strStatus=="C")
				{
					lblQuoteStatus.Text="Cancelled";
				}
				else
				{
					lblQuoteStatus.Text="";
				}
			}

		}

		protected void OnQuotation_PageChange(object sender, DataGridPageChangedEventArgs e)
		{
			dgQuotation.CurrentPageIndex = e.NewPageIndex;
			int iStartIndex = dgQuotation.CurrentPageIndex * dgQuotation.PageSize;
			String strCustID = (String)ViewState["currentCustomer"];
			
			m_sdsQuotation = SysDataMgrDAL.QueryQuotation(utility.GetAppID(),utility.GetEnterpriseID(), strCustID, iStartIndex,dgQuotation.PageSize);
			Session["SESSION_DS5"] = m_sdsQuotation;
			dgQuotation.VirtualItemCount = System.Convert.ToInt32(m_sdsQuotation.QueryResultMaxSize);
			dgQuotation.CurrentPageIndex = e.NewPageIndex;
			BindQuotation();
		}

		private void BindQuotation()
		{
			dgQuotation.VirtualItemCount = System.Convert.ToInt32(m_sdsQuotation.QueryResultMaxSize);
			dgQuotation.DataSource = m_sdsQuotation.ds;
			dgQuotation.DataBind();
			Session["SESSION_DS5"] = m_sdsQuotation;
			
		}

		private void btnQuoteViewAll_Click(object sender, System.EventArgs e)
		{
			ShowQuotationsForCurrentCustomer();
			BindQuotation();
		}

		private void ShowQuotationsForCurrentCustomer()
		{
			ViewState["currentQuoteSet"] = 0;			
			GetQuoteRecSet();
			if(m_sdsQuotation.QueryResultMaxSize == 0)
			{
				if(!utility.GetUserCulture().ToUpper().Equals("EN-US"))
				{
					lblNumRefRec.Text = Utility.GetLanguageText(ResourceType.UserMessage, "QUOTN_NO_ADDED", utility.GetUserCulture());	
				} 
				else 
				{
					lblNumRefRec.Text = "No Quotations added.";
				}
				return;
			}

			lblCPErrorMessage.Text = "";			
			BindQuotation();

		}

		private void GetQuoteRecSet()
		{
			int iStartIndex = (int)ViewState["currentQuoteSet"] * m_iQuoteSize;			
			String strCustID = (String)ViewState["currentCustomer"];
			m_sdsQuotation = SysDataMgrDAL.QueryQuotation(utility.GetAppID(),utility.GetEnterpriseID(),strCustID,iStartIndex,m_iQuoteSize);			
			Session["SESSION_DS5"] = m_sdsQuotation;
			decimal pgCnt = (m_sdsQuotation.QueryResultMaxSize - 1)/m_iQuoteSize;
			if(pgCnt < (int)ViewState["currentQuoteSet"])
			{
				ViewState["currentQuoteSet"] = System.Convert.ToInt32(pgCnt);
			}
			//			else if (pgCnt <=0)
			//			{
			//				pgCnt=0;
			//			}
		}

		private void customerProfileMultiPage_SelectedIndexChange(object sender, System.EventArgs e)
		{
		
		}

		private void setQueryFR()
		{
			this.lblCPErrorMessage.Text= "";
			dsCustZones = SysDataMgrDAL.GetEmptyCustZones(1);
			Session["dsCustZones"] = dsCustZones;
			dgCustZones.EditItemIndex = 0;
			BindCustZonesGrid();
			BindVAS();
			//DataGridItem dgRow = dgCustZones.Items[dgCustZones.EditItemIndex];

			//msTextBox txtextended_area_surcharge = (msTextBox)dgRow.Cells[6].FindControl("txtextended_area_surcharge");
			//if(txtextended_area_surcharge != null)
			//{
			//txtextended_area_surcharge.Enabled = false;
			//}

			//msTextBox txtextended_area_surcharge_percent = (msTextBox)dgRow.Cells[7].FindControl("txtextended_area_surcharge_percent");
			//if(txtextended_area_surcharge_percent != null)
			//{
			//txtextended_area_surcharge_percent.Enabled = false;
			//}
			btnExecuteQueryFR.Enabled = true;
			//btnInsertFR.Enabled = true;

			dgCustZones.Columns[0].Visible = false;  
			dgCustZones.Columns[1].Visible = false;  
			dgCustZones.Columns[2].Visible = false; 
		}

		private void setExecuteFR()
		{
			this.lblCPErrorMessage.Text= "";
			if(dgCustZones.EditItemIndex==-1)
				return;
			DataGridItem dgRow = dgCustZones.Items[dgCustZones.EditItemIndex];
			String strZipcode = "";
			String strZone = "";
			//			String strextended_area_surcharge = "";
			//			String strextended_area_surcharge_percent = "";
			String strEffective_date = "";
			String strCustID = (String)ViewState["currentCustomer"];

			TextBox txtzipcode = (TextBox)dgRow.Cells[2].FindControl("Textbox1");
			if(txtzipcode != null)
			{
				strZipcode = txtzipcode.Text.Trim();
			}

			TextBox txtzone = (TextBox)dgRow.Cells[4].FindControl("txtzone");
			if(txtzone != null)
			{
				strZone = txtzone.Text.Trim();
			}

			TextBox txtEffectivedate = (TextBox)dgRow.Cells[6].FindControl("txtEffectivedate");
			if(txtEffectivedate != null)
			{
				if(txtEffectivedate.Text.Trim() != "")
					strEffective_date = Convert.ToString(txtEffectivedate.Text.Trim());
				else
					strEffective_date = "";
			}
			dsCustZones = SysDataMgrDAL.GetCustFRData(utility.GetAppID(), utility.GetEnterpriseID(),
				strCustID, strZipcode, strZone, strEffective_date);
			Session["dsCustZones"] = dsCustZones;
			dgCustZones.EditItemIndex = -1;
			BindCustZonesGrid();
			btnExecuteQueryFR.Enabled = false;

			dgCustZones.Columns[0].Visible = false;  
			dgCustZones.Columns[1].Visible = true;  
			dgCustZones.Columns[2].Visible = true; 
		}

		private void addCustFRButtonEventHandlers()
		{
			Button btnInsertFR = (Button)customerProfileMultiPage.FindControl("btnInsertFR");
			if(btnInsertFR != null)
			{
				btnInsertFR.Click += new System.EventHandler(this.btnInsertFR_Click);
			}

			Button btnQueryFR = (Button)customerProfileMultiPage.FindControl("btnQueryFR");
			if(btnQueryFR != null)
			{
				btnQueryFR.Click += new System.EventHandler(this.btnQueryFR_Click);
			}

			
			Button btnExecuteQueryFR = (Button)customerProfileMultiPage.FindControl("btnExecuteQueryFR");
			if(btnExecuteQueryFR != null)
			{
				btnExecuteQueryFR.Click += new System.EventHandler(this.btnExecuteQueryFR_Click);
			}
			
			//PSA 2009-016 by Gwang ;04Mar09
			Button btnImport_Show = (Button)customerProfileMultiPage.FindControl("btnImport_Show");
			if(btnImport_Show != null)
			{
				btnImport_Show.Click += new System.EventHandler(this.btnImport_Show_Click);
			}

			Button btnImport_Cancel = (Button)customerProfileMultiPage.FindControl("btnImport_Cancel");
			if(btnImport_Cancel != null)
			{
				btnImport_Cancel.Click += new System.EventHandler(this.btnImport_Cancel_Click);
			}

			Button btnImport_Save = (Button)customerProfileMultiPage.FindControl("btnImport_Save");
			if(btnImport_Save != null)
			{
				btnImport_Save.Click += new System.EventHandler(this.btnImport_Save_Click);
			}

			Button btnImport = (Button)customerProfileMultiPage.FindControl("btnImport");
			if(btnImport != null)
			{
				btnImport.Click += new System.EventHandler(this.btnImport_Click);
			}
			
		}

	
		private void btnInsertFR_Click(object sender, System.EventArgs e)
		{
			lblCPErrorMessage.Text = "";
			btnInsertFR.Enabled = false;
			btnExecuteQueryFR.Enabled = false;
			dgCustZones.CurrentPageIndex = 0;

			DataSet tmpDsCustZones = SysDataMgrDAL.GetEmptyCustZones(1);

			dgCustZones.EditItemIndex = 0;

			dgCustZones.DataSource = tmpDsCustZones;
			dgCustZones.DataBind();

			dgCustZones.Columns[0].Visible = false;  
			dgCustZones.Columns[1].Visible = true;  
			dgCustZones.Columns[2].Visible = true; 

			TextBox txtzipcode = (TextBox)dgCustZones.Items[0].Cells[3].FindControl("Textbox1");
			TextBox txtzone = (TextBox)dgCustZones.Items[0].Cells[3].FindControl("txtzone");

			if(txtzipcode != null)
			{
				txtzipcode.Attributes.Add("onpast","return false;");
				txtzipcode.Attributes.Add("onkeypress","return false;");
				txtzipcode.Attributes.Add("onkeydown","return false;");
			}
			if(txtzone != null)
			{
				txtzone.Attributes.Add("onpast","return false;");
				txtzone.Attributes.Add("onkeypress","return false;");
				txtzone.Attributes.Add("onkeydown","return false;");
			}
		}

		private void btnQueryFR_Click(object sender, System.EventArgs e)
		{
			setQueryFR();
			this.CheckRole();
		}

		private void btnExecuteQueryFR_Click(object sender, System.EventArgs e)
		{
			setExecuteFR();
			this.CheckRole();
		}


		public void BindCustZonesGrid()
		{
			if(this.btnImport_Cancel.Visible==true)
				dsCustZones = (DataSet) Session["dsCustZone_Import"];
			else
				dsCustZones = (DataSet)Session["dsCustZones"];

			try
			{
				dgCustZones.DataSource = dsCustZones;
				dgCustZones.DataBind(); 
			}
			catch
			{
				dgCustZones.CurrentPageIndex = 0;
				dgCustZones.DataSource = dsCustZones;
				dgCustZones.DataBind(); 
			}
		}


		public void dgCustZones_Update(object sender, DataGridCommandEventArgs e)
		{
			//for edit 
			String sZipCode = "";
			String sZoneCode = "";
			String sEffectiveDate = "";

			if ((String)ViewState["sZipcode"] != "")
			{
				sZipCode = ViewState["sZipcode"].ToString();
			}
			else
			{
				sZipCode = "";
			}
			if ((String)ViewState["sZone"]  != "")
			{
				sZoneCode = ViewState["sZone"].ToString();
			}
			else
			{
				sZoneCode = "";
			}
			if ((String)ViewState["sEffectiveDate"]  != "")
			{
				sEffectiveDate = ViewState["sEffectiveDate"].ToString();
			}
			else
			{
				sEffectiveDate = "";
			}

			lblCPErrorMessage.Text = "";
			int iSelIndex = e.Item.ItemIndex;
			DataGridItem dgRow = dgCustZones.Items[iSelIndex];
			TextBox txtzipcode = (TextBox)dgRow.Cells[3].FindControl("Textbox1");
			TextBox txtzone = (TextBox)dgRow.Cells[5].FindControl("txtzone");
			msTextBox txtEffectiveDate = (msTextBox)dgRow.Cells[7].FindControl("txtEffectiveDate");
	
			//			msTextBox txtextended_area_surcharge = (msTextBox)dgRow.Cells[6].FindControl("txtextended_area_surcharge");
			//			msTextBox txtextended_area_surcharge_percent = (msTextBox)dgRow.Cells[7].FindControl("txtextended_area_surcharge_percent");


			String strCustID = (String)ViewState["currentCustomer"];
			object effective_date = System.DBNull.Value;
			object percentEAS = System.DBNull.Value;

			if(txtEffectiveDate.Text.ToString().Trim() != "")
				effective_date =  DateTime.ParseExact(txtEffectiveDate.Text.ToString().Trim(),"dd/MM/yyyy",null);

			if(txtzipcode != null)
			{
				
				//IN CASE OF IMPORT MODE
				if(this.btnImport_Cancel.Visible == true)
				{ //iSelIndex
					

					DataSet dsCustZone_Import = (DataSet) Session["dsCustZone_Import"];
					DataSet dsZones = new DataSet();


					dsZones = SysDataMgrDAL.GetCustFRData(utility.GetAppID(), utility.GetEnterpriseID(),
						strCustID, txtzipcode.Text, txtzone.Text, txtEffectiveDate.Text);
				
					//					if(dsZones.Tables[0].Rows.Count>0)
					//					{
					//						this.lblCPErrorMessage.Text= "Error. Duplicate Data.";
					//						//Response.Write("<script>alert('Error. Duplicate Data.');</script>");
					//						return;
					//					}
					//					else
					//					{
					//Check ZipCode (in Zipcode Table)
					DataRow drCustZone = dsCustZone_Import.Tables[0].Rows[e.Item.ItemIndex];
					
					if(txtzipcode != null)
					{
						drCustZone[0] = txtzipcode.Text;
					}
					if(txtzone != null)
					{
						drCustZone[1] = txtzone.Text;
					}
					if(txtEffectiveDate != null)
					{
						drCustZone[2] = DateTime.ParseExact(txtEffectiveDate.Text,"dd/MM/yyyy",null);
					}

					this.dgCustZones.EditItemIndex = -1;

					BindCustZonesGrid();

					this.dgCustZones.Columns[0].Visible  = true;

					String ckSaveButton = "1";

					foreach(DataGridItem dgItem in dgCustZones.Items)
					{
						DataSet dsZone = new DataSet();
					
						ImageButton imgSelect = (ImageButton)dgItem.Cells[0].Controls[1];
						Label lblzipcode = (Label)dgItem.FindControl("lblzipcode");
						Label lblzone = (Label)dgItem.FindControl("lblzone");
						Label lblEffectiveDate = (Label)dgItem.FindControl("lblEffectiveDate");
						
						String strZipcode = "1";
						String strZone = "1";
						String strEffectiveDate = "1";
						String strDupStatus = "1";

						String strDupInDs = "1";
						int intDupInDs = 0;

						DataSet ckDupInGrid = (DataSet)ViewState["beginCuszoneImport"];
						foreach(DataRow dr in ckDupInGrid.Tables["tempck"].Rows)
						{
							object objEffectivedate = System.DBNull.Value;
							if(dr[2].ToString() != "")
								objEffectivedate = dr[2].ToString();

							string strDay = Convert.ToDateTime(objEffectivedate).Day.ToString().Trim();
							string strMonth = Convert.ToDateTime(objEffectivedate).Month.ToString().Trim();
							string strYear = Convert.ToDateTime(objEffectivedate).Year.ToString().Trim();

							DateTime dtCk = DateTime.ParseExact(lblEffectiveDate.Text.ToString().Trim(),"dd/MM/yyyy",null);
							String strDtCk = "";
							String strDtOld = "";

							if(dtCk.Day.ToString().Length == 1)
								strDtCk = "0"+dtCk.Day.ToString();
							else
								strDtCk = dtCk.Day.ToString();

							if(dtCk.Month.ToString().Length == 1)
								strDtCk += "/0"+dtCk.Month.ToString()+"/"+dtCk.Year.ToString();
							else
								strDtCk += "/"+dtCk.Month.ToString()+"/"+dtCk.Year.ToString();

							if(strDay.Length == 1)
								strDtOld = "0"+strDay;
							else
								strDtOld = strDay;

							if(strMonth.Length == 1)
								strDtOld += "/0"+strMonth+"/"+strYear;
							else
								strDtOld += "/"+strMonth+"/"+strYear;

							//Response.Write(strDtCk+","+strDtOld);

							if((dr[0].ToString().Trim().Equals(lblzipcode.Text.Trim()) && (dr[1].ToString().Trim().Equals(lblzone.Text.Trim()) && (strDtOld.Trim().Equals(strDtOld.Trim())))))
							{
								intDupInDs = intDupInDs + 1;
							}

							//Response.Write("("+dr["zipcode"].ToString()+"/"+dr["zonecode"].ToString()+"/"+dr["effectivedate"].ToString()+"/"+intDupInDs.ToString()+")");
						}

						if(intDupInDs > 1)
							strDupInDs = "0";

						

						object objEffectiveDate = System.DBNull.Value;
						if(lblEffectiveDate != null)
						{
							objEffectiveDate = DateTime.ParseExact(lblEffectiveDate.Text.ToString().Trim(),"dd/MM/yyyy",null);
						}



						if(SysDataMgrDAL.IsCustFRExist(utility.GetAppID(),utility.GetEnterpriseID(), strCustID,lblzipcode.Text.ToString(),objEffectiveDate))
						{
							strDupStatus = "0";
						}

						//Check ZipCode (in Zipcode Table)
						Zipcode zipcode = new Zipcode();
						DataSet dsCountZip = new DataSet();

						dsCountZip = zipcode.ckZipcodeInSystem(utility.GetAppID(), utility.GetEnterpriseID(),lblzipcode.Text);
						//zipcode.Populate(utility.GetAppID(),utility.GetEnterpriseID(),);

		
						if(dsCountZip.Tables[0].Rows.Count<= 0)
						{
							//imgSelect.ImageUrl  = "images/butt-red.gif";
							strZipcode = "0";
						}
				
						//CheckZoneCode
						DataSet zoneCount = new DataSet();
						zoneCount = SysDataMgrDAL.Get_CheckCustZoneCode(utility.GetAppID(), utility.GetEnterpriseID(),lblzone.Text);

						if(zoneCount.Tables[0].Rows.Count<=0)
						{
							//imgSelect.ImageUrl  = "images/butt-red.gif";
							strZone = "0";
						}


						//Assign Effective_Date (in case not assigned)
						if(txtEffectiveDate == null)
						{
							strEffectiveDate = "0";
						}
						else
						{
							//object objEffectiveDate =  DateTime.ParseExact(lblEffectiveDate.Text.ToString().Trim(),"dd/MM/yyyy",null);
							if(!ckEffectiveDate(Convert.ToDateTime(objEffectiveDate)))
							{
								strEffectiveDate = "0";
							}
						}

						if(strZipcode.Equals("0")||strZone.Equals("0")||strEffectiveDate.Equals("0")||strDupStatus.Equals("0")|| strDupInDs.Equals("0"))
						{
							imgSelect.ImageUrl  = "images/butt-red.gif";
							ckSaveButton = "0";
						}
						else
						{
							imgSelect.ImageUrl  = "images/butt-select.gif";
						}

						if(ckSaveButton.Equals("0"))
							this.btnImport_Save.Enabled = false;
						else
							this.btnImport_Save.Enabled = true;
					}
				
					//Session["dsCustZone_Import"] = dsCustZone_Import;					
					//BindCustZonesGrid();
					//}
				}
					//END IN CASE OF IMPORT MODE
				else
				{
					try
					{
						//						Response.Write("<script>alert('"+effective_date.ToString().Trim()+" 3');</script>");
						//					//Modify by GwanG on 5-Feb-08
						//					// Amount & Percentage is more than zero. 
						//					if((txtextended_area_surcharge.Text.ToString().Trim() != "" && txtextended_area_surcharge_percent.Text.ToString().Trim() != "") && 
						//						(Convert.ToDecimal(txtextended_area_surcharge.Text.Trim()) != 0 && Convert.ToDecimal(txtextended_area_surcharge_percent.Text.Trim()) != 0))
						//					{
						//						lblCPErrorMessage.Text =  Utility.GetLanguageText(ResourceType.UserMessage,"ISESAVALID",utility.GetUserCulture());
						//						return;
						//					}
						//					else
						//					{	DateTime.ParseExact(strEffectiveDate,"dd/MM/yyyy",null);
						//						//Amount & Percentage is zero
						
						//
						//						if(txtextended_area_surcharge_percent.Text.ToString().Trim() != "")
						//							percentEAS = txtextended_area_surcharge_percent.Text.ToString().Trim();
			
						//						DateTime effStart = System.DateTime.Now;
						//						DateTime effEnd = System.DateTime.Now;
						//						DataSet effDS = new DataSet();
						//						effDS = SysDataMgrDAL.GetEndRoundEffectiveDate(utility.GetAppID(),utility.GetEnterpriseID(),effStart.ToString("dd/MM/yyyy"));
						//						DataTable effDT = new DataTable();
						//						effDT = effDS.Tables[0];
						//						foreach(DataRow dr in effDT.Rows)
						//						{
						//							effEnd = Convert.ToDateTime(dr["endEffDate"].ToString());
						//						}

						if(ckEffectiveDate(Convert.ToDateTime(effective_date)))
						{
							if(!sZipCode.Equals("") && !sZoneCode.Equals("") && !sEffectiveDate.Equals(""))
							{	
	
								//							if (txtzipcode.Enabled == false)
								//							{CheckDupUpdateCustFR
								//if(SysDataMgrDAL.IsCustFRExist(utility.GetAppID(),utility.GetEnterpriseID(), strCustID,txtzipcode.Text.ToString(),effective_date))
	
								if(SysDataMgrDAL.CheckDupUpdateCustFR(utility.GetAppID(),utility.GetEnterpriseID(), strCustID, 
									txtzipcode.Text.ToString(),
									txtzone.Text.ToString(),effective_date,
									sZipCode,sZoneCode,sEffectiveDate))
								{
									this.lblCPErrorMessage.Text= "Error. Duplicate Data. Aoo ";
								}
								else
								{
									SysDataMgrDAL.ModifyCustFR(utility.GetAppID(),utility.GetEnterpriseID(), strCustID, 
										txtzipcode.Text.ToString(),
										txtzone.Text.ToString(),effective_date,
										sZipCode,sZoneCode,sEffectiveDate);
									dsCustZones = SysDataMgrDAL.GetCustFRData(utility.GetAppID(), utility.GetEnterpriseID(), strCustID, "", "", "");
									Session["dsCustZones"] = dsCustZones;

									dgCustZones.EditItemIndex = -1;
									BindCustZonesGrid();
									btnInsertFR.Enabled = true;
								}
							}
							else
							{
								if(SysDataMgrDAL.IsCustFRExist(utility.GetAppID(),utility.GetEnterpriseID(), strCustID,txtzipcode.Text.ToString(),effective_date))
								{
									this.lblCPErrorMessage.Text= "Error. Duplicate Data.";
								}
								else
								{
									int intRow = 0;
									intRow = SysDataMgrDAL.InsertCustFR(utility.GetAppID(),utility.GetEnterpriseID(), strCustID, 
										txtzipcode.Text.ToString(),
										txtzone.Text.ToString(),
										effective_date);


									dsCustZones = SysDataMgrDAL.GetCustFRData(utility.GetAppID(), utility.GetEnterpriseID(), strCustID, "", "", "");
									Session["dsCustZones"] = dsCustZones;

									dgCustZones.EditItemIndex = -1;
									BindCustZonesGrid();

									if(intRow > 0)
									{
										this.lblCPErrorMessage.Text= "1 row(s) inserted.";
									}

									btnInsertFR.Enabled = true;
								}
							}
						}
						else
						{
							DateTime effStart = (DateTime)ViewState["effStart"];
							DateTime effEnd = (DateTime)ViewState["effEnd"];
							this.lblCPErrorMessage.Text= "Effective date must between "+effStart.ToString("dd/MM/yyyy")+" and "+effEnd.ToString("dd/MM/yyyy")+" ";
						}
					}
					catch(ApplicationException appException)
					{
						String strMsg = appException.Message;
						strMsg = appException.InnerException.Message;
						strMsg = appException.InnerException.InnerException.Message;
		
						if(strMsg.IndexOf("FOREIGN KEY") != -1 )
						{
							lblCPErrorMessage.Text =  Utility.GetLanguageText(ResourceType.UserMessage,"NO_ZONE_POSTAL",utility.GetUserCulture());
						}			
						return;
					}
				}	
			}
		}


		public void dgCustZones_Delete(object sender, DataGridCommandEventArgs e)
		{
			if(this.btnImport_Save.Visible == true)
			{
				DataSet dsZone = new DataSet();
				dsZone = (DataSet)Session["dsCustZone_Import"];

				dsZone.Tables[0].Rows.RemoveAt(e.Item.ItemIndex);
				dsZone.AcceptChanges();
				
				Session["dsCustZone_Import"] = dsZone;
				//dgCustZones.EditItemIndex = -1;
				BindCustZonesGrid();


				this.dgCustZones.Columns[0].Visible  = true;

				String ckSaveButton = "1";

				String strCustID = (String)ViewState["currentCustomer"];

				

				foreach(DataGridItem dgItem in dgCustZones.Items)
				{
					//DataSet dsZone = new DataSet();
					
					ImageButton imgSelect = (ImageButton)dgItem.Cells[0].Controls[1];
					Label lblzipcode = (Label)dgItem.FindControl("lblzipcode");
					Label lblzone = (Label)dgItem.FindControl("lblzone");
					Label lblEffectiveDate = (Label)dgItem.FindControl("lblEffectiveDate");
						
					String strZipcode = "1";
					String strZone = "1";
					String strEffectiveDate = "1";
					String strDupStatus = "1";

					String strDupInDs = "1";
					int intDupInDs = 0;

					DataSet ckDupInGrid = (DataSet)ViewState["beginCuszoneImport"];
					foreach(DataRow dr in ckDupInGrid.Tables["tempck"].Rows)
					{
						object objEffectivedate = System.DBNull.Value;
						if(dr[2].ToString() != "")
							objEffectivedate = dr[2].ToString();

						string strDay = Convert.ToDateTime(objEffectivedate).Day.ToString().Trim();
						string strMonth = Convert.ToDateTime(objEffectivedate).Month.ToString().Trim();
						string strYear = Convert.ToDateTime(objEffectivedate).Year.ToString().Trim();

						DateTime dtCk = DateTime.ParseExact(lblEffectiveDate.Text.ToString().Trim(),"dd/MM/yyyy",null);
						String strDtCk = "";
						String strDtOld = "";

						if(dtCk.Day.ToString().Length == 1)
							strDtCk = "0"+dtCk.Day.ToString();
						else
							strDtCk = dtCk.Day.ToString();

						if(dtCk.Month.ToString().Length == 1)
							strDtCk += "/0"+dtCk.Month.ToString()+"/"+dtCk.Year.ToString();
						else
							strDtCk += "/"+dtCk.Month.ToString()+"/"+dtCk.Year.ToString();

						if(strDay.Length == 1)
							strDtOld = "0"+strDay;
						else
							strDtOld = strDay;

						if(strMonth.Length == 1)
							strDtOld += "/0"+strMonth+"/"+strYear;
						else
							strDtOld += "/"+strMonth+"/"+strYear;

						//Response.Write(strDtCk+","+strDtOld);
						//Response.Write("("+dr[0].ToString()+"/"+lblzipcode.Text+"/"+dr[1].ToString()+"/"+lblzone.Text+"/"+strDtOld+"/"+strDtOld+")");
						if((dr[0].ToString().Trim().Equals(lblzipcode.Text.Trim()) && (dr[1].ToString().Trim().Equals(lblzone.Text.Trim()) && (strDtOld.Trim().Equals(strDtOld.Trim())))))
						{
							intDupInDs = intDupInDs + 1;
						}
						
						if(intDupInDs == 1)
							break;
						//Response.Write("("+dr["zipcode"].ToString()+"/"+dr["zonecode"].ToString()+"/"+dr["effectivedate"].ToString()+"/"+intDupInDs.ToString()+")");
					}

					if(intDupInDs > 1)
						strDupInDs = "0";

						

					object objEffectiveDate = System.DBNull.Value;
					if(lblEffectiveDate != null)
					{
						objEffectiveDate = DateTime.ParseExact(lblEffectiveDate.Text.ToString().Trim(),"dd/MM/yyyy",null);
					}



					if(SysDataMgrDAL.IsCustFRExist(utility.GetAppID(),utility.GetEnterpriseID(), strCustID,lblzipcode.Text.ToString(),objEffectiveDate))
					{
						strDupStatus = "0";
					}

					//Check ZipCode (in Zipcode Table)
					Zipcode zipcode = new Zipcode();
					DataSet dsCountZip = new DataSet();

					dsCountZip = zipcode.ckZipcodeInSystem(utility.GetAppID(), utility.GetEnterpriseID(),lblzipcode.Text);
					//zipcode.Populate(utility.GetAppID(),utility.GetEnterpriseID(),);

		
					if(dsCountZip.Tables[0].Rows.Count<= 0)
					{
						//imgSelect.ImageUrl  = "images/butt-red.gif";
						strZipcode = "0";
					}
				
					//CheckZoneCode
					DataSet zoneCount = new DataSet();
					zoneCount = SysDataMgrDAL.Get_CheckCustZoneCode(utility.GetAppID(), utility.GetEnterpriseID(),lblzone.Text);

					if(zoneCount.Tables[0].Rows.Count<=0)
					{
						//imgSelect.ImageUrl  = "images/butt-red.gif";
						strZone = "0";
					}


					//Assign Effective_Date (in case not assigned)
					if(lblEffectiveDate == null)
					{
						strEffectiveDate = "0";
					}
					else
					{
						//object objEffectiveDate =  DateTime.ParseExact(lblEffectiveDate.Text.ToString().Trim(),"dd/MM/yyyy",null);
						if(!ckEffectiveDate(Convert.ToDateTime(objEffectiveDate)))
						{
							strEffectiveDate = "0";
						}
					}

					if(strZipcode.Equals("0")||strZone.Equals("0")||strEffectiveDate.Equals("0")||strDupStatus.Equals("0")|| strDupInDs.Equals("0"))
					{
						imgSelect.ImageUrl  = "images/butt-red.gif";
						ckSaveButton = "0";
					}
					else
					{
						imgSelect.ImageUrl  = "images/butt-select.gif";
					}

					if(ckSaveButton.Equals("0"))
						this.btnImport_Save.Enabled = false;
					else
						this.btnImport_Save.Enabled = true;
				}
				
			}
			else
			{
				lblCPErrorMessage.Text = "";
				int iSelIndex = e.Item.ItemIndex;
				DataGridItem dgRow = dgCustZones.Items[iSelIndex];
				Label lblzipcode = (Label)dgRow.Cells[3].FindControl("lblzipcode");
				Label lblzone = (Label)dgRow.Cells[5].FindControl("lblzone");
				Label lblEffectiveDate = (Label)dgRow.Cells[7].FindControl("lblEffectiveDate");
				TextBox txtzipcode = (TextBox)dgRow.Cells[3].FindControl("Textbox1");
				TextBox txtzone = (TextBox)dgRow.Cells[5].FindControl("txtzone");
				msTextBox txtEffectiveDate = (msTextBox)dgRow.Cells[7].FindControl("txtEffectiveDate");

				String strzipcode = "";
				String strzone = "";
				String strEffective_date = "";

				if(lblzipcode == null)
					strzipcode = txtzipcode.Text;
				else
					strzipcode = lblzipcode.Text;

				if(lblzone == null)
					strzone = txtzone.Text;
				else
					strzone = lblzone.Text;

				if(lblEffectiveDate == null)
					strEffective_date = txtEffectiveDate.Text;
				else
					strEffective_date = lblEffectiveDate.Text;

				String strCustID = (String)ViewState["currentCustomer"];
				try
				{
					int iRowsDeleted = 0;
					if(!strzipcode.Equals("") && !strzone.Equals("") && !strEffective_date.Equals(""))
					{
						iRowsDeleted = SysDataMgrDAL.DeleteFR(utility.GetAppID(),utility.GetEnterpriseID(), strCustID, 
							strzipcode,
							strzone,
							strEffective_date);
					}
					else
					{
						iRowsDeleted = 0;
					}
					lblCPErrorMessage.Text = iRowsDeleted + " row(s) deleted.";
				}
				catch(ApplicationException appException)
				{
					String strMsg = appException.Message;
					if(strMsg.IndexOf("FK") != -1)
					{
						lblCPErrorMessage.Text =  Utility.GetLanguageText(ResourceType.UserMessage,"ERR_DEL_FR",utility.GetUserCulture());
					}
					if(strMsg.ToLower().IndexOf("child record found") != -1 )
					{
						lblCPErrorMessage.Text =  Utility.GetLanguageText(ResourceType.UserMessage,"ERR_DEL_FR",utility.GetUserCulture());	
					}			
					else
					{
						lblCPErrorMessage.Text = strMsg;
					}
					return;
				}

				dsCustZones = SysDataMgrDAL.GetCustFRData(utility.GetAppID(), utility.GetEnterpriseID(), strCustID, "", "", "");
				Session["dsCustZones"] = dsCustZones;
				dgCustZones.EditItemIndex = -1;
				BindCustZonesGrid();
			}
		}


		public void dgCustZones_Button(object sender, DataGridCommandEventArgs e)
		{
			String strCmdNm = e.CommandName;

			if(dgCustZones.EditItemIndex != -1)
			{
				if(strCmdNm.Equals("zoneSearch"))
				{
					TextBox txtzone = (TextBox)e.Item.FindControl("txtzone");
					String strZoneCodeClientID = null;
					String strZoneCode = null;

					if(txtzone != null)
					{
						strZoneCodeClientID = txtzone.ClientID;
						strZoneCode = txtzone.Text;
					}

					String sUrl = "ZonePopup.aspx?FORMID=CustomerProfile&ZONECODE_TEXT="+strZoneCode+
						"&ZONECODE="+strZoneCodeClientID;

					ArrayList paramList = new ArrayList();
					paramList.Add(sUrl);
					String sScript = Utility.GetScript("openWindowParam.js",paramList);
					Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
				
				}
				else if(strCmdNm.Equals("zipcodeSearch"))
				{
					TextBox txtzipcode	= (TextBox)e.Item.FindControl("Textbox1");
					String strZipcodeClientID = null;
					String strZipcode = null;
				
					if(txtzipcode != null)
					{
						strZipcodeClientID = txtzipcode.ClientID;
						strZipcode = txtzipcode.Text;
					}

					String sUrl = "ZipcodePopup.aspx?FORMID=CustomerProfile&ZIPCODE="+strZipcode+
						"&ZIPCODE_CID="+strZipcodeClientID +
						"&ISFR=Y";
	
					ArrayList paramList = new ArrayList();
					paramList.Add(sUrl);
					String sScript = Utility.GetScript("openWindowParam.js",paramList);
					Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
				}
			}
		}


		protected void dgCustZones_PageChange(object sender, DataGridPageChangedEventArgs e)
		{

			dgCustZones.CurrentPageIndex = e.NewPageIndex;
			dgCustZones.EditItemIndex = -1;
			BindCustZonesGrid();

			DataSet ckDupInGrid2 = (DataSet)ViewState["beginCuszoneImport"];
			if(ckDupInGrid2!=null)
				this.dgCustZones.Columns[0].Visible  = true;

			String ckSaveButton = "1";

			String strCustID = (String)ViewState["currentCustomer"];

			foreach(DataGridItem dgItem in dgCustZones.Items)
			{
				//DataSet dsZone = new DataSet();
					
				ImageButton imgSelect = (ImageButton)dgItem.Cells[0].Controls[1];
				Label lblzipcode = (Label)dgItem.FindControl("lblzipcode");
				Label lblzone = (Label)dgItem.FindControl("lblzone");
				Label lblEffectiveDate = (Label)dgItem.FindControl("lblEffectiveDate");
						
				String strZipcode = "1";
				String strZone = "1";
				String strEffectiveDate = "1";
				String strDupStatus = "1";

				String strDupInDs = "1";
				int intDupInDs = 0;

				DataSet ckDupInGrid = (DataSet)ViewState["beginCuszoneImport"];
				if(ckDupInGrid==null)
					return;
				foreach(DataRow dr in ckDupInGrid.Tables["tempck"].Rows)
				{
					object objEffectivedate = System.DBNull.Value;
					if(dr[2].ToString() != "")
						objEffectivedate = dr[2].ToString();

					string strDay = Convert.ToDateTime(objEffectivedate).Day.ToString().Trim();
					string strMonth = Convert.ToDateTime(objEffectivedate).Month.ToString().Trim();
					string strYear = Convert.ToDateTime(objEffectivedate).Year.ToString().Trim();

					DateTime dtCk = DateTime.ParseExact(lblEffectiveDate.Text.ToString().Trim(),"dd/MM/yyyy",null);
					String strDtCk = "";
					String strDtOld = "";

					if(dtCk.Day.ToString().Length == 1)
						strDtCk = "0"+dtCk.Day.ToString();
					else
						strDtCk = dtCk.Day.ToString();

					if(dtCk.Month.ToString().Length == 1)
						strDtCk += "/0"+dtCk.Month.ToString()+"/"+dtCk.Year.ToString();
					else
						strDtCk += "/"+dtCk.Month.ToString()+"/"+dtCk.Year.ToString();

					if(strDay.Length == 1)
						strDtOld = "0"+strDay;
					else
						strDtOld = strDay;

					if(strMonth.Length == 1)
						strDtOld += "/0"+strMonth+"/"+strYear;
					else
						strDtOld += "/"+strMonth+"/"+strYear;

					//Response.Write(strDtCk+","+strDtOld);

					if((dr[0].ToString().Trim().Equals(lblzipcode.Text.Trim()) && (dr[1].ToString().Trim().Equals(lblzone.Text.Trim()) && (strDtOld.Trim().Equals(strDtOld.Trim())))))
					{
						intDupInDs = intDupInDs + 1;
					}

					//Response.Write("("+dr["zipcode"].ToString()+"/"+dr["zonecode"].ToString()+"/"+dr["effectivedate"].ToString()+"/"+intDupInDs.ToString()+")");
				}

				if(intDupInDs > 1)
					strDupInDs = "0";

						

				object objEffectiveDate = System.DBNull.Value;
				if(lblEffectiveDate != null)
				{
					objEffectiveDate = DateTime.ParseExact(lblEffectiveDate.Text.ToString().Trim(),"dd/MM/yyyy",null);
				}



				if(SysDataMgrDAL.IsCustFRExist(utility.GetAppID(),utility.GetEnterpriseID(), strCustID,lblzipcode.Text.ToString(),objEffectiveDate))
				{
					strDupStatus = "0";
				}

				//Check ZipCode (in Zipcode Table)
				Zipcode zipcode = new Zipcode();
				DataSet dsCountZip = new DataSet();

				dsCountZip = zipcode.ckZipcodeInSystem(utility.GetAppID(), utility.GetEnterpriseID(),lblzipcode.Text);
				//zipcode.Populate(utility.GetAppID(),utility.GetEnterpriseID(),);

		
				if(dsCountZip.Tables[0].Rows.Count<= 0)
				{
					//imgSelect.ImageUrl  = "images/butt-red.gif";
					strZipcode = "0";
				}
				
				//CheckZoneCode
				DataSet zoneCount = new DataSet();
				zoneCount = SysDataMgrDAL.Get_CheckCustZoneCode(utility.GetAppID(), utility.GetEnterpriseID(),lblzone.Text);

				if(zoneCount.Tables[0].Rows.Count<=0)
				{
					//imgSelect.ImageUrl  = "images/butt-red.gif";
					strZone = "0";
				}


				//Assign Effective_Date (in case not assigned)
				if(lblEffectiveDate == null)
				{
					strEffectiveDate = "0";
				}
				else
				{
					//object objEffectiveDate =  DateTime.ParseExact(lblEffectiveDate.Text.ToString().Trim(),"dd/MM/yyyy",null);
					if(!ckEffectiveDate(Convert.ToDateTime(objEffectiveDate)))
					{
						strEffectiveDate = "0";
					}
				}

				if(strZipcode.Equals("0")||strZone.Equals("0")||strEffectiveDate.Equals("0")||strDupStatus.Equals("0")|| strDupInDs.Equals("0"))
				{
					imgSelect.ImageUrl  = "images/butt-red.gif";
					ckSaveButton = "0";
				}
				else
				{
					imgSelect.ImageUrl  = "images/butt-select.gif";
				}

				if(ckSaveButton.Equals("0"))
					this.btnImport_Save.Enabled = false;
				else
					this.btnImport_Save.Enabled = true;
			}
		}


		protected void dgCustZones_Cancel(object sender, DataGridCommandEventArgs e)
		{
			String strCustID = (String)ViewState["currentCustomer"];

			if(btnImport.Visible == false)
			{
				dgCustZones.EditItemIndex = -1;
				dsCustZones = (DataSet)Session["dsCustZones"];
			
				for(int i = 0; i <= dsCustZones.Tables[0].Rows.Count - 1; i++)
				{
					if(((dsCustZones.Tables[0].Rows[i]["zipcode"] == null) || 
						(dsCustZones.Tables[0].Rows[i]["zipcode"].GetType().Equals(System.Type.GetType("System.DBNull"))) ||
						(dsCustZones.Tables[0].Rows[i]["zipcode"].ToString() == ""))
						&&
						((dsCustZones.Tables[0].Rows[i]["zone_code"] == null) || 
						(dsCustZones.Tables[0].Rows[i]["zone_code"].GetType().Equals(System.Type.GetType("System.DBNull"))) ||
						(dsCustZones.Tables[0].Rows[i]["zone_code"].ToString() == "")))
					{
						dsCustZones.Tables[0].Rows[i].Delete();
						dsCustZones.Tables[0].AcceptChanges();
						i = 0;
					}
				}
				dgCustZones.Columns[0].Visible = false;

				BindCustZonesGrid();
				btnInsertFR.Enabled = true;
			}
			else
			{
				dgCustZones.EditItemIndex = -1;
				dsCustZones = (DataSet)Session["dsCustZone_Import"];
			
				for(int i = 0; i <= dsCustZones.Tables[0].Rows.Count - 1; i++)
				{
					if(((dsCustZones.Tables[0].Rows[i]["zipcode"] == null) || 
						(dsCustZones.Tables[0].Rows[i]["zipcode"].GetType().Equals(System.Type.GetType("System.DBNull"))) ||
						(dsCustZones.Tables[0].Rows[i]["zipcode"].ToString() == ""))
						&&
						((dsCustZones.Tables[0].Rows[i]["zone_code"] == null) || 
						(dsCustZones.Tables[0].Rows[i]["zone_code"].GetType().Equals(System.Type.GetType("System.DBNull"))) ||
						(dsCustZones.Tables[0].Rows[i]["zone_code"].ToString() == "")))
					{
						dsCustZones.Tables[0].Rows[i].Delete();
						dsCustZones.Tables[0].AcceptChanges();
						i = 0;
					}
				}

				BindCustZonesGrid();
				
				String ckSaveButton = "1";

				foreach(DataGridItem dgItem in dgCustZones.Items)
				{
					DataSet dsZone = new DataSet();
						
					ImageButton imgSelect = (ImageButton)dgItem.Cells[0].Controls[1];
					Label lblzipcode = (Label)dgItem.FindControl("lblzipcode");
					Label lblzone = (Label)dgItem.FindControl("lblzone");
					Label lblEffectiveDate = (Label)dgItem.FindControl("lblEffectiveDate");

					String strZipcode = "1";
					String strZone = "1";
					String strEffectiveDate = "1";
					String strDupStatus = "1";

					String strDupInDs = "1";
					int intDupInDs = 0;

					DataSet ckDupInGrid = (DataSet)ViewState["beginCuszoneImport"];
					foreach(DataRow dr in ckDupInGrid.Tables["tempck"].Rows)
					{
						object objEffectivedate = System.DBNull.Value;
						if(dr[2].ToString() != "")
							objEffectivedate = dr[2].ToString();

						string strDay = Convert.ToDateTime(objEffectivedate).Day.ToString().Trim();
						string strMonth = Convert.ToDateTime(objEffectivedate).Month.ToString().Trim();
						string strYear = Convert.ToDateTime(objEffectivedate).Year.ToString().Trim();

						DateTime dtCk = DateTime.ParseExact(lblEffectiveDate.Text.ToString().Trim(),"dd/MM/yyyy",null);
						String strDtCk = "";
						String strDtOld = "";

						if(dtCk.Day.ToString().Length == 1)
							strDtCk = "0"+dtCk.Day.ToString();
						else
							strDtCk = dtCk.Day.ToString();

						if(dtCk.Month.ToString().Length == 1)
							strDtCk += "/0"+dtCk.Month.ToString()+"/"+dtCk.Year.ToString();
						else
							strDtCk += "/"+dtCk.Month.ToString()+"/"+dtCk.Year.ToString();

						if(strDay.Length == 1)
							strDtOld = "0"+strDay;
						else
							strDtOld = strDay;

						if(strMonth.Length == 1)
							strDtOld += "/0"+strMonth+"/"+strYear;
						else
							strDtOld += "/"+strMonth+"/"+strYear;

						//Response.Write(strDtCk+","+strDtOld);

						if((dr[0].ToString().Trim().Equals(lblzipcode.Text.Trim()) && (dr[1].ToString().Trim().Equals(lblzone.Text.Trim()) && (strDtOld.Trim().Equals(strDtOld.Trim())))))
						{
							intDupInDs = intDupInDs + 1;
						}

						//Response.Write("("+dr["zipcode"].ToString()+"/"+dr["zonecode"].ToString()+"/"+dr["effectivedate"].ToString()+"/"+intDupInDs.ToString()+")");
					}

					if(intDupInDs > 1)
						strDupInDs = "0";


					object objEffectiveDate = System.DBNull.Value;
					if(lblEffectiveDate != null)
					{
						objEffectiveDate =  DateTime.ParseExact(lblEffectiveDate.Text.ToString().Trim(),"dd/MM/yyyy",null);
					}

					if(SysDataMgrDAL.IsCustFRExist(utility.GetAppID(),utility.GetEnterpriseID(), strCustID,lblzipcode.Text.ToString(),objEffectiveDate))
					{
						strDupStatus = "0";
					}

					//Check ZipCode (in Zipcode Table)
					Zipcode zipcode = new Zipcode();
					DataSet dsCountZip = new DataSet();

					dsCountZip = zipcode.ckZipcodeInSystem(utility.GetAppID(), utility.GetEnterpriseID(),lblzipcode.Text);
		
					if(dsCountZip.Tables[0].Rows.Count<= 0)
					{
						strZipcode = "0";
					}
					
					//CheckZoneCode
					DataSet zoneCount = new DataSet();
					zoneCount = SysDataMgrDAL.Get_CheckCustZoneCode(utility.GetAppID(), utility.GetEnterpriseID(),
						lblzone.Text);

					if(zoneCount.Tables[0].Rows.Count<=0)
					{
						strZone = "0";
					}

					//Assign Effective_Date (in case not assigned)
					if(lblEffectiveDate == null)
					{
						strEffectiveDate = "0";
					}
					else
					{
						//object objEffectiveDate =  DateTime.ParseExact(lblEffectiveDate.Text.ToString().Trim(),"dd/MM/yyyy",null);
						if(!ckEffectiveDate(Convert.ToDateTime(objEffectiveDate)))
						{
							strEffectiveDate = "0";
						}
					}

					if(strZipcode.Equals("0")||strZone.Equals("0")||strEffectiveDate.Equals("0")||strDupStatus.Equals("0")|| strDupInDs.Equals("0"))
					{
						imgSelect.ImageUrl  = "images/butt-red.gif";
						ckSaveButton = "0";
					}
					else
					{
						imgSelect.ImageUrl  = "images/butt-select.gif";
					}


					if(ckSaveButton.Equals("0"))
						this.btnImport_Save.Enabled = false;
					else
						this.btnImport_Save.Enabled = true;
				}
			}
			dgCustZones.Columns[0].Visible = true;
		}


		protected void dgCustZones_Edit(object sender, DataGridCommandEventArgs e)
		{
			dgCustZones.EditItemIndex = e.Item.ItemIndex;
			BindCustZonesGrid();

			TextBox txtzipcode = (TextBox)dgCustZones.Items[e.Item.ItemIndex].Cells[3].FindControl("Textbox1");
			if(txtzipcode != null)
			{	
				//txtzipcode.Enabled = false;
				txtzipcode.Attributes.Add("onpast","return false;");
				txtzipcode.Attributes.Add("onkeypress","return false;");
				txtzipcode.Attributes.Add("onkeydown","return false;");
				ViewState["sZipcode"] = txtzipcode.Text;
			}

			TextBox txtzone = (TextBox)dgCustZones.Items[e.Item.ItemIndex].Cells[5].FindControl("txtzone");
			if(txtzone != null)
			{
				//txtzone.Enabled = false;
				txtzone.Attributes.Add("onpast","return false;");
				txtzone.Attributes.Add("onkeypress","return false;");
				txtzone.Attributes.Add("onkeydown","return false;");
				ViewState["sZone"] = txtzone.Text;
			}

			msTextBox txtEffectiveDate = (msTextBox)dgCustZones.Items[e.Item.ItemIndex].Cells[7].FindControl("txtEffectiveDate");
			if(txtEffectiveDate != null)
			{
				ViewState["sEffectiveDate"] = txtEffectiveDate.Text;
			}

			dgCustZones.Columns[0].Visible = false;
		}

		private void TabStrip1_SelectedIndexChange(object sender, System.EventArgs e)
		{
		
		}

		#region "VAS"
		//New Tab
		//Created By GwanG on 24March08

		private void BindVAS()
		{
			dgVAS.VirtualItemCount = System.Convert.ToInt32(m_sdsVAS.QueryResultMaxSize);
			dgVAS.DataSource = m_sdsVAS.ds;
			dgVAS.DataBind();
			Session["SESSION_DSVASCUST"] = m_sdsVAS;			
		}

		private void btnViewAllVAS_Click(object sender, System.EventArgs e)
		{
			//query record	
			lblCPErrorMessage.Text = "";

			if((int)ViewState["VASMode"] == (int)ScreenMode.Insert && (int)ViewState["VASOperation"] == (int)Operation.Insert)
			{
				m_sdsVAS.ds.Tables[0].Rows.RemoveAt(dgVAS.EditItemIndex);
				m_sdsVAS.QueryResultMaxSize--;
				dgVAS.CurrentPageIndex = 0;
				dgVAS.EditItemIndex = -1;
			}
							
			ViewState["VASMode"] = ScreenMode.ExecuteQuery;
			ViewState["VASOperation"] = Operation.None;
			ShowVASForCurrentCustomer();
		}

		private void btnInsertVAS_Click(object sender, System.EventArgs e)
		{
			lblCPErrorMessage.Text = "";
			if((int)ViewState["VASMode"] != (int)ScreenMode.Insert || m_sdsVAS.ds.Tables[0].Rows.Count >= dgVAS.PageSize)
			{
				m_sdsVAS = SysDataMgrDAL.GetEmptyVASCustDS(0);
			}

			AddRowInVASGrid();	
			dgVAS.EditItemIndex = m_sdsVAS.ds.Tables[0].Rows.Count - 1;
			dgVAS.CurrentPageIndex = 0;
			Logger.LogDebugInfo("StatusException","btnInsertVAS_Click","INF003","Data Grid Items count"+dgVAS.Items.Count);
		
			ViewState["VASMode"] = ScreenMode.Insert;
			ViewState["VASOperation"] = Operation.Insert;
			BindVAS();
			btnInsertVAS.Enabled = false;
			//getPageControls(Page);
		}

		
		private void ShowVASForCurrentCustomer()
		{
			ViewState["currentVASSet"] = 0;	
			//ShowCurrentVASPage();
			GetVASRecSet();
			if(m_sdsVAS.QueryResultMaxSize == 0)
			{
				if(!utility.GetUserCulture().ToUpper().Equals("EN-US"))
				{
					lblCPErrorMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage, "CPVAS_NO_ADDED", utility.GetUserCulture());	
				} 
				else 
				{
					lblCPErrorMessage.Text = "No VAS added.";
				}
				return;
			}
		
			BindVAS();
			this.CheckRole();
		}

		private void GetVASRecSet()
		{
			int iStartIndex = (int)ViewState["currentVASSet"] * m_iVASSize;			
			String strCustID = (String)ViewState["currentCustomer"];
			m_sdsVAS = SysDataMgrDAL.QueryVAS(utility.GetAppID(),utility.GetEnterpriseID(),strCustID,iStartIndex,m_iVASSize);			
			Session["SESSION_DSVASCUST"] = m_sdsVAS;
			decimal pgCnt = (m_sdsQuotation.QueryResultMaxSize - 1)/m_iVASSize;
			if(pgCnt < (int)ViewState["currentVASSet"])
			{
				ViewState["currenVASSet"] = System.Convert.ToInt32(pgCnt);
			}
			//			else if (pgCnt <=0)
			//			{
			//				pgCnt=0;
			//			}
		}

		private void ShowCurrentVASPage()
		{
			DataRow drCurrent = m_sdsCustomerProfile.ds.Tables[0].Rows[Convert.ToInt32(ViewState["currentPage"])];

			TextBox txtCustAccDispTab7 = (TextBox)customerProfileMultiPage.FindControl("txtCustAccDispTab7");
			

			if(txtCustAccDispTab7 != null)
			{
				if(drCurrent["custid"]!= null && (!drCurrent["custid"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					txtCustAccDispTab7.Text = drCurrent["custid"].ToString();
				}
				else
				{
					txtCustAccDispTab7.Text = "";
				}
			}

			TextBox txtCustNameDispTab7 = (TextBox)customerProfileMultiPage.FindControl("txtCustNameDispTab7");


			if(txtCustNameDispTab7 != null)
			{
				if(drCurrent["cust_name"]!= null && (!drCurrent["cust_name"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					txtCustNameDispTab7.Text = drCurrent["cust_name"].ToString();
				}
				else
				{
					txtCustNameDispTab7.Text = "";
				}
			}


			int iStartIndex = dgVAS.CurrentPageIndex * dgVAS.PageSize;
			String strCustID = (String)ViewState["currentCustomer"];
			m_sdsVAS = SysDataMgrDAL.QueryVAS(utility.GetAppID(),utility.GetEnterpriseID(),strCustID,iStartIndex,dgVAS.PageSize);
			//m_sdsVAS = SysDataMgrDAL.GetCPVASDS(utility.GetAppID(),utility.GetEnterpriseID(),strCustID,iStartIndex,m_sdsVAS.PageSize);
			
			//			decimal pgCnt = (m_sdsVAS.QueryResultMaxSize - 1)/dgVAS.PageSize;
			//			if(pgCnt < dgVAS.CurrentPageIndex)
			//			{
			//				dgVAS.CurrentPageIndex = System.Convert.ToInt32(pgCnt);
			//			}


			int pgCnt = Convert.ToInt16(Math.Ceiling((double)m_sdsVAS.QueryResultMaxSize/dgVAS.PageSize));            
			if(pgCnt != 0)
			{
				if(dgVAS.CurrentPageIndex > pgCnt-1)
				{
					dgVAS.CurrentPageIndex = System.Convert.ToInt32(pgCnt-1);
					iStartIndex = dgVAS.CurrentPageIndex * dgVAS.PageSize;
					strCustID = (String)ViewState["currentCustomer"];
					m_sdsVAS = SysDataMgrDAL.QueryVAS(utility.GetAppID(),utility.GetEnterpriseID(),strCustID,iStartIndex,dgVAS.PageSize);
				}
			}


			dgVAS.SelectedIndex = -1;
			dgVAS.EditItemIndex = -1;
			//lblCPErrorMessage.Text = "";

			BindVAS();
			btnInsertVAS.Enabled = true;
			//ResetDgVAS();
		}

		private void ResetDgVAS()
		{
			dgVAS.CurrentPageIndex = 0; 
			btnInsertVAS.Enabled = true;			
			m_sdsVAS = SysDataMgrDAL.GetEmptyVASCustDS(0);
			BindVAS();
		}	

		private void AddRowInVASGrid()
		{
			SysDataMgrDAL.AddNewRowInCPVASDS(m_sdsVAS);
			Session["SESSION_DSVASCUST"] = m_sdsVAS;
		}

		public void dgVAS_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			//			Label lblVASCode = (Label)dgVAS.SelectedItem.FindControl("lblVASCode");
			//			msTextBox txtVASCode = (msTextBox)dgVAS.SelectedItem.FindControl("txtVASCode");
			//			String strVASCode = null;
			//
			//			if(lblVASCode != null)
			//			{
			//				strVASCode = lblVASCode.Text;
			//			}
			//
			//			if(txtVASCode != null)
			//			{
			//				strVASCode = txtVASCode.Text;
			//			}
			//			ViewState["CurrentVAS"] = strVASCode;
			//			dgZipcodeVASExcluded.CurrentPageIndex = 0;
			//
			//			Logger.LogDebugInfo("VASZipcodeExcluded","dgVAS_SelectedIndexChanged","INF004","updating data grid..."+dgVAS.SelectedIndex+"  : "+strVASCode);
			//			ShowCurrentZVASEPage();
			//
			//			ViewState["ZVASEMode"] = ScreenMode.ExecuteQuery;
			//
			//			if((int)ViewState["VASMode"] != (int)ScreenMode.Insert && (int)ViewState["VASOperation"] != (int)Operation.Insert)
			//			{
			//				Utility.EnableButton(ref btnInsert,ButtonType.Insert,true,m_moduleAccessRights);
			//			}
			//
			//			if((int)ViewState["VASMode"] == (int)ScreenMode.Insert && dgVAS.EditItemIndex == dgVAS.SelectedIndex && (int)ViewState["VASOperation"] == (int)Operation.Insert)
			//			{
			//				btnZVASEInsert.Enabled = false;
			//				btnZVASEInsertMultiple.Enabled = false;
			//			}
			//			else
			//			{
			//				btnZVASEInsert.Enabled = true;
			//				btnZVASEInsertMultiple.Enabled = true;
			//			}
			//			lblVASMessage.Text = "";
			//			lblZVASEMessage.Text="";
		}

		public void dgVAS_Update(object sender, DataGridCommandEventArgs e)
		{
			int iOperation = (int)ViewState["VASOperation"];
			DataSet dsNewVASCode = new DataSet();
			DataTable dtNewVASCode = new DataTable();
 
			dtNewVASCode.Columns.Add(new DataColumn("custid", typeof(string)));
			dtNewVASCode.Columns.Add(new DataColumn("vas_code", typeof(string)));
			dtNewVASCode.Columns.Add(new DataColumn("description", typeof(string)));
			dtNewVASCode.Columns.Add(new DataColumn("surcharge", typeof(decimal)));
			dtNewVASCode.Columns.Add(new DataColumn("percent", typeof(decimal)));
			dtNewVASCode.Columns.Add(new DataColumn("min_surcharge", typeof(decimal)));
			dtNewVASCode.Columns.Add(new DataColumn("max_surcharge", typeof(decimal)));
			DataRow drEach = dtNewVASCode.NewRow();

			TextBox txtCustAccDispTab7 = (TextBox)customerProfileMultiPage.FindControl("txtCustAccDispTab4");
			if(Utility.IsNotDBNull(txtCustAccDispTab7))
			{
				drEach["custid"] = txtCustAccDispTab7.Text;
			}

			DropDownList ddlVASCode = (DropDownList)e.Item.FindControl("ddlVASCode");

			if(Utility.IsNotDBNull(ddlVASCode))
			{
				if(iOperation == (int)Operation.Insert)
				{
					if(ddlVASCode.SelectedIndex != 0 )
					{
						drEach["vas_code"] = ddlVASCode.SelectedItem.Value;
					}
					else
					{
						lblCPErrorMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"INVALVAS_BLANK",utility.GetUserCulture());
						return;
					}
				}
				else
				{
					drEach["vas_code"] = ddlVASCode.SelectedItem.Value;
				}
			}


			TextBox  txtVASDesc = (TextBox)e.Item.FindControl("txtVASDesc");

			if(Utility.IsNotDBNull(txtVASDesc))
			{
				if(txtVASDesc.Text.Trim() != "")
				{
					drEach["description"] = txtVASDesc.Text;
				}
				else
				{
					lblCPErrorMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"INVALDESC_BLANK",utility.GetUserCulture());
					return;

				}
			}


			msTextBox txtSurcharge = (msTextBox)e.Item.FindControl("txtSurcharge");
			msTextBox txtPercent = (msTextBox)e.Item.FindControl("txtPercent");
			if(Utility.IsNotDBNull(txtSurcharge) && Utility.IsNotDBNull(txtPercent) && txtSurcharge.Text.Trim() == "" && txtPercent.Text.Trim() == "")
			{
				lblCPErrorMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"INVALVAS_BLANKBOTH",utility.GetUserCulture());
				return;
			}
			else if(Utility.IsNotDBNull(txtSurcharge) && Utility.IsNotDBNull(txtPercent) && txtSurcharge.Text.Trim() != "" && txtPercent.Text.Trim() != "")
			{
				lblCPErrorMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"INVALVAS_BOTH",utility.GetUserCulture());
				return;
			}
			else
			{
				if(Utility.IsNotDBNull(txtSurcharge) && txtSurcharge.Text.Trim() != "")
				{
					decimal tempSurch = Convert.ToDecimal(txtSurcharge.Text);
					if((0<= tempSurch) &&(tempSurch <= 10000))
					{
						drEach["surcharge"] = Convert.ToDecimal(txtSurcharge.Text);
					}
					else
					{
						lblCPErrorMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"INVALVAS_AMOUNT",utility.GetUserCulture());
						return;
					}
				}			

				else if(Utility.IsNotDBNull(txtPercent) && txtPercent.Text.Trim() != "")
				{
					decimal tempPercent = Convert.ToDecimal(txtPercent.Text);
					if((0 < tempPercent)&& (tempPercent <= 300))
					{
						drEach["percent"] = Convert.ToDecimal(txtPercent.Text);
					}
					else
					{
						lblCPErrorMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"INVALVAS_PER",utility.GetUserCulture());
						return;
					}
					
				}
			}

			decimal tempMin = 0;
			decimal tempMax = 0;
			msTextBox txtMinSurch = (msTextBox)e.Item.FindControl("txtMinSurch");
			if(Utility.IsNotDBNull(txtSurcharge) && Utility.IsNotDBNull(txtMinSurch) && txtSurcharge.Text.Trim() != "" && txtMinSurch.Text.Trim() != "")
			{
				lblCPErrorMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"INVALVAS_AMOUNTMIN",utility.GetUserCulture());
				return;
			}

			if(Utility.IsNotDBNull(txtMinSurch) && txtMinSurch.Text.Trim() != "")
			{
				tempMin = Convert.ToDecimal(txtMinSurch.Text);
				if((0<= tempMin)&&( tempMin<= 10000))
				{
					drEach["min_surcharge"] = Convert.ToDecimal(txtMinSurch.Text);
				}
				else
				{
					lblCPErrorMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"INVALVAS_MIN",utility.GetUserCulture());
					return;
				}

			}
		

			msTextBox txtMaxSurch = (msTextBox)e.Item.FindControl("txtMaxSurch");
			if(Utility.IsNotDBNull(txtSurcharge) && Utility.IsNotDBNull(txtMaxSurch) && txtSurcharge.Text.Trim() != "" && txtMaxSurch.Text.Trim() != "")
			{
				lblCPErrorMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"INVALVAS_AMOUNTMAX",utility.GetUserCulture());
				return;
			}
		
			if(Utility.IsNotDBNull(txtMaxSurch)  && txtMaxSurch.Text.Trim() != "")
			{
				tempMax = Convert.ToDecimal(txtMaxSurch.Text);
				if((0<= tempMax)&&( tempMax<= 10000))
				{
					drEach["max_surcharge"] = Convert.ToDecimal(txtMaxSurch.Text);
				}
				else
				{
					lblCPErrorMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"INVALVAS_MAX",utility.GetUserCulture());
					return;
				}

			}
			
			if(tempMax<tempMin)
			{
				lblCPErrorMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"INVALVAS_MINMAX",utility.GetUserCulture());
				return;
			}
			
			
			dtNewVASCode.Rows.Add(drEach);
			dsNewVASCode.Tables.Add(dtNewVASCode);
			
			switch(iOperation)
			{

				case (int)Operation.Update:

					//DataSet dsToUpdate = m_sdsVAS.ds.GetChanges();
					SysDataMgrDAL.ModifyCustVAS(utility.GetAppID(),utility.GetEnterpriseID(),dsNewVASCode);
					lblCPErrorMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"INS_SUCCESSFULLY",utility.GetUserCulture());
					break;
				case (int)Operation.Insert:

					SysDataMgrDAL.AddCustVAS(utility.GetAppID(),utility.GetEnterpriseID(),dsNewVASCode);
					lblCPErrorMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"INS_SUCCESSFULLY",utility.GetUserCulture());
					break;
			}

			ViewState["VASOperation"] = Operation.None;
			ViewState["VASMode"] = ScreenMode.ExecuteQuery;

			ShowCurrentVASPage();
		}

		protected void dgVAS_Cancel(object sender, DataGridCommandEventArgs e)
		{
			lblCPErrorMessage.Text = "";
			dgVAS.EditItemIndex = -1;			
			if((int)ViewState["VASMode"] == (int)ScreenMode.Insert && (int)ViewState["VASOperation"] == (int)Operation.Insert)
			{
				m_sdsVAS.ds.Tables[0].Rows.RemoveAt(e.Item.ItemIndex);
				m_sdsVAS.QueryResultMaxSize--;
				dgVAS.CurrentPageIndex = 0;
			}
		
			
			ViewState["VASMode"] = ScreenMode.ExecuteQuery;
			ViewState["VASOperation"] = Operation.None;
			//ShowVASForCurrentCustomer();	
			ShowCurrentVASPage();

			btnInsertVAS.Enabled = true;
		}

		public void dgVAS_Delete(object sender, DataGridCommandEventArgs e)
		{
			String strCustID = "";
			String strVASCode = "";
			if((int)ViewState["VASMode"] == (int)ScreenMode.ExecuteQuery && (int)ViewState["VASOperation"] == (int)Operation.None )
			{
				TextBox txtCustAccDispTab7 = (TextBox)customerProfileMultiPage.FindControl("txtCustAccDispTab7");
				if(Utility.IsNotDBNull(txtCustAccDispTab7))
				{
					strCustID = txtCustAccDispTab7.Text;
				}

				Label lblVASCode = (Label)e.Item.FindControl("lblVASCode");

				if(Utility.IsNotDBNull(lblVASCode))
				{
					strVASCode = lblVASCode.Text;
				}
				int i = SysDataMgrDAL.DeleteCustVAS(utility.GetAppID(),utility.GetEnterpriseID(),strCustID,strVASCode);
				lblCPErrorMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"DLD_SUCCESSFULLY",utility.GetUserCulture());
				ShowCurrentVASPage();
			}
			else if((int)ViewState["VASMode"] == (int)ScreenMode.Insert && (int)ViewState["VASOperation"] == (int)Operation.Insert )
			{
				lblCPErrorMessage.Text ="";
				BindVAS();				
			}
			else
			{
				lblCPErrorMessage.Text = "";
				ShowCurrentVASPage();
			}
		}
		
		protected void dgVAS_Bound(object sender, DataGridItemEventArgs e)
		{
			if(e.Item.ItemIndex == -1)
			{
				return;
			}

			DropDownList ddlVASCode = (DropDownList) e.Item.FindControl("ddlVASCode");
			if(ddlVASCode != null)
			{
				if((int)ViewState["VASMode"] == (int)ScreenMode.Insert && (int)ViewState["VASOperation"] == (int)Operation.Insert)
				{

					//btnInsertVAS.Click += new System.EventHandler(this.btnInsertVAS_Click);	
					TextBox txtCustAccDispTab7 = (TextBox)customerProfileMultiPage.FindControl("txtCustAccDispTab4");
					DataSet dsVasCode = SysDataMgrDAL.GetCustVasCode(utility.GetAppID(),utility.GetEnterpriseID(),txtCustAccDispTab7.Text);
					if(dsVasCode.Tables[0].Rows.Count > 0)
					{
						DataView dvVASCode = new DataView(dsVasCode.Tables[0]);					
						ddlVASCode.DataSource =  dvVASCode;
						ddlVASCode.DataTextField =  "vas_code";
						ddlVASCode.DataValueField =	 "vas_code";
						ddlVASCode.DataBind();
						ddlVASCode.Enabled = true;
					}			
					
				}
				else
				{
					if(Utility.IsNotDBNull(ViewState["VASCode"]))
					{
						String strVasCode = ViewState["VASCode"].ToString();
						ddlVASCode.Items.Add(new ListItem(strVasCode,strVasCode));
						ddlVASCode.Enabled = false;
					}
					
				}
				ddlVASCode.SelectedIndex = 0;
			}

		}

		
		protected void dgVAS_Edit(object sender, DataGridCommandEventArgs e)
		{
			lblCPErrorMessage.Text = "";
			dgVAS.EditItemIndex = e.Item.ItemIndex;
			ViewState["VASOperation"] = Operation.Update;			
			Label lblVASCode = (Label)e.Item.FindControl("lblVASCode");
			if(Utility.IsNotDBNull(lblVASCode))
			{
				ViewState["VASCode"] = lblVASCode.Text.Trim();
			}
			BindVAS();			
			Logger.LogDebugInfo("VASZipcodeExcluded","dgVAS_Edit","INF004","updating data grid...");				
		}

		protected void dgVAS_PageChange(object sender, DataGridPageChangedEventArgs e)
		{
			lblCPErrorMessage.Text = "";
			dgVAS.CurrentPageIndex = e.NewPageIndex;
			ShowCurrentVASPage();
			//			Logger.LogDebugInfo("VASZipcodeExcluded","dgVAS_PageChange","INF009","On Page Change "+e.NewPageIndex);
		}

		
		protected void ddlVASCode_SelectedIndexChanged(object sender,System.EventArgs e)
		{

			if(dgVAS.EditItemIndex == -1)
				return ;

			DropDownList ddlVASCode = (DropDownList)sender;
			DataSet ds = SysDataMgrDAL.GetCustDescSurch(utility.GetAppID(),utility.GetEnterpriseID(),ddlVASCode.SelectedItem.Value);
			if(ds.Tables[0].Rows.Count > 0)
			{
				if (dgVAS.Items.Count > 0)
				{
					DataRow dr = ds.Tables[0].Rows[0];

					foreach(DataGridItem dgItem in dgVAS.Items)
					{
						TextBox  txtVASDesc = (TextBox)dgItem.FindControl("txtVASDesc");

						if(txtVASDesc != null)
						{
							if(Utility.IsNotDBNull(dr["description"]))
							{
								txtVASDesc.Text = dr["description"].ToString();
							}
						}

						msTextBox txtSurcharge = (msTextBox)dgItem.FindControl("txtSurcharge");
						if(txtSurcharge != null)
						{
							if(Utility.IsNotDBNull(dr["surcharge"]))
							{
								txtSurcharge.Text = dr["surcharge"].ToString();
							}
						}
					}
				}
				
			}
		}




		#endregion

		private void btnImport_Show_Click(object sender, System.EventArgs e)
		{
			EnableZoneUpload(true);
		}

		private void EnableZoneUpload(bool b)
		{
			this.btnImport_Cancel.Visible=b;
			this.btnImport_Save.Visible=b;
			this.btnImport.Visible=b;
			this.txtFilePath.Visible=b;

			this.btnQueryFR.Visible=!b;
			this.btnExecuteQueryFR.Visible=!b;
			this.btnInsertFR.Visible=!b;
			this.btnImport_Show.Visible=!b;

			if(b)
			{
				dgCustZones.CurrentPageIndex = 0;
				dgCustZones.DataSource = SysDataMgrDAL.GetEmptyCustZones(0);
				dgCustZones.DataBind();
				dgCustZones.EditItemIndex=-1;
			}
			else
			{
				BindCustZonesGrid();
				dgCustZones.EditItemIndex=-1;
			}
			this.CheckRole();
		}

		private void btnImport_Cancel_Click(object sender, System.EventArgs e)
		{
			this.lblCPErrorMessage.Text= "";
			EnableZoneUpload(false);
			//bind old data
			//			Session["dsCustZones"]
			//				Session["dsCustZones"] = dsCustZones;
			dgCustZones.EditItemIndex = -1;
			BindCustZonesGrid();

			dgCustZones.Columns[0].Visible = false; 
		}

		

		private void btnImport_Click(object sender, System.EventArgs e)
		{
			
			this.lblCPErrorMessage.Text = "";

			try
			{
				if(uploadFiletoServer())
				{
					//Cleare existing data
					//ImportConsignmentsDAL.ClearTempDataByUserId(appID, enterpriseID, userID);


					//Get data from Excel WorkSheet
					getExcelFiles();

					DataSet ds = (DataSet) Session["dsCustZone_Import"];

					//					ViewState["beginCuszoneImport"] = ds;

					//					ds = EliminateDuplicateRecords(ds);


					//					ds = ValidateCustZones(ds);


					this.dgCustZones.DataSource = ds;
					this.dgCustZones.EditItemIndex=-1;
					this.dgCustZones.DataBind();
					dgCustZones.Columns[0].Visible = true;  
					dgCustZones.Columns[1].Visible = true;
					dgCustZones.Columns[2].Visible = true; 

					String strCustID = (String)ViewState["currentCustomer"];

					String ckSaveButton = "1";

					foreach(DataGridItem dgItem in dgCustZones.Items)
					{
						DataSet dsZone = new DataSet();
						
						ImageButton imgSelect = (ImageButton)dgItem.Cells[0].Controls[1];
						Label lblzipcode = (Label)dgItem.Cells[3].FindControl("lblzipcode");
						Label lblzone = (Label)dgItem.Cells[5].FindControl("lblzone");
						Label lblEffectiveDate = (Label)dgItem.Cells[7].FindControl("lblEffectiveDate");

						String strZipcode = "1";
						String strZone = "1";
						String strEffectiveDate = "1";
						String strDupStatus = "1";
						String strDupInDs = "1";
						int intDupInDs = 0;

						DataSet ckDupInGrid = (DataSet)ViewState["beginCuszoneImport"];
						foreach(DataRow dr in ckDupInGrid.Tables["tempck"].Rows)
						{
							object objEffectivedate = System.DBNull.Value;
							if(dr[2].ToString() != "")
								objEffectivedate = dr[2].ToString();

							string strDay = Convert.ToDateTime(objEffectivedate).Day.ToString().Trim();
							string strMonth = Convert.ToDateTime(objEffectivedate).Month.ToString().Trim();
							string strYear = Convert.ToDateTime(objEffectivedate).Year.ToString().Trim();

							DateTime dtCk = DateTime.ParseExact(lblEffectiveDate.Text.ToString().Trim(),"dd/MM/yyyy",null);
							String strDtCk = "";
							String strDtOld = "";

							if(dtCk.Day.ToString().Length == 1)
								strDtCk = "0"+dtCk.Day.ToString();
							else
								strDtCk = dtCk.Day.ToString();

							if(dtCk.Month.ToString().Length == 1)
								strDtCk += "/0"+dtCk.Month.ToString()+"/"+dtCk.Year.ToString();
							else
								strDtCk += "/"+dtCk.Month.ToString()+"/"+dtCk.Year.ToString();

							if(strDay.Length == 1)
								strDtOld = "0"+strDay;
							else
								strDtOld = strDay;

							if(strMonth.Length == 1)
								strDtOld += "/0"+strMonth+"/"+strYear;
							else
								strDtOld += "/"+strMonth+"/"+strYear;

							//Response.Write(strDtCk+","+strDtOld);

							if((dr[0].ToString().Trim().Equals(lblzipcode.Text.Trim()) && (dr[1].ToString().Trim().Equals(lblzone.Text.Trim()) && (strDtOld.Trim().Equals(strDtOld.Trim())))))
							{
								intDupInDs = intDupInDs + 1;
							}

							//Response.Write("("+dr["zipcode"].ToString()+"/"+dr["zonecode"].ToString()+"/"+dr["effectivedate"].ToString()+"/"+intDupInDs.ToString()+")");
						}

						if(intDupInDs > 1)
							strDupInDs = "0";
						
						

						
						object objEffectiveDate = System.DBNull.Value;
						if(lblEffectiveDate != null)
						{
							objEffectiveDate = DateTime.ParseExact(lblEffectiveDate.Text.ToString().Trim(),"dd/MM/yyyy",null);
						}

						if(SysDataMgrDAL.IsCustFRExist(utility.GetAppID(),utility.GetEnterpriseID(), strCustID,lblzipcode.Text.ToString(),objEffectiveDate))
						{
							strDupStatus = "0";
						}

						//Check ZipCode (in Zipcode Table)
						Zipcode zipcode = new Zipcode();
						DataSet dsCountZip = new DataSet();

						dsCountZip = zipcode.ckZipcodeInSystem(utility.GetAppID(), utility.GetEnterpriseID(),lblzipcode.Text);
						//zipcode.Populate(utility.GetAppID(),utility.GetEnterpriseID(),);

						TextBox txtCountry = (TextBox)customerProfileMultiPage.FindControl("txtCountry");	
						//Response.Write("<script>alert('zipcode.Country : "+zipcode.Country.ToString()+"');</script>");
						//	if(dsZones.Tables[0].Rows.Count>0)
						//if(zipcode.Country!=txtCountry.Text)
						if(dsCountZip.Tables[0].Rows.Count<= 0)
						{
							strZipcode = "0";
						}
					
						//CheckZoneCode
						DataSet zoneCount = new DataSet();
						zoneCount = SysDataMgrDAL.Get_CheckCustZoneCode(utility.GetAppID(), utility.GetEnterpriseID(),
							lblzone.Text);

						if(zoneCount.Tables[0].Rows.Count<=0)
						{
							strZone = "0";
						}


						//Assign Effective_Date (in case not assigned)
						if(lblEffectiveDate == null)
						{
							strEffectiveDate = "0";
						}
						else
						{
							//object objEffectiveDate =  DateTime.ParseExact(lblEffectiveDate.Text.ToString().Trim(),"dd/MM/yyyy",null);
							if(!ckEffectiveDate(Convert.ToDateTime(objEffectiveDate)))
							{
								strEffectiveDate = "0";
							}
						}

						if(strZipcode.Equals("0")||strZone.Equals("0")||strEffectiveDate.Equals("0")||strDupStatus.Equals("0")||strDupInDs.Equals("0"))
						{
							imgSelect.ImageUrl  = "images/butt-red.gif";
							ckSaveButton = "0";
						}
						else
						{
							imgSelect.ImageUrl  = "images/butt-select.gif";
						}

						
					}

					if(ds.Tables[0].Rows.Count>0)
					{
						if(ckSaveButton.Equals("0"))
							this.btnImport_Save.Enabled = false;
						else
							this.btnImport_Save.Enabled = true;
					}
					Session["dsCustZone_Import"] = ds;
				}
			}
			catch(Exception ex)
			{
				this.lblCPErrorMessage.Text = ex.Message.ToString();
				txtFilePath.Text = "";
			}
		}
		//		private void dgCustZones_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
		//		{
		//			ImageButton imgSelect =  new ImageButton();
		//			imgSelect = (ImageButton)dgCustZones.FindControl("imgSelect");
		//			DataSet ds = (DataSet) Session["dsCustZone_Import"];
		//
		//			if(ds != null)
		//			{
		//				ds = EliminateDuplicateRecords(ds);
		//				if (ds.Tables[0].Rows.Count > 0)
		//				{
		//					imgSelect.ImageUrl  = "images/butt-red.gif";
		//				}
		//				else
		//				{
		//					imgSelect.ImageUrl  = "images/butt-select.gif";
		//				}
		//			}
		//		}
		private bool uploadFiletoServer()
		{
			bool status = true;

			if((inFile.PostedFile != null ) && (inFile.PostedFile.ContentLength > 0))
			{
				string extension = System.IO.Path.GetExtension(inFile.PostedFile.FileName);

				if(extension.ToUpper() == ".XLS")
				{
					string fn = System.IO.Path.GetFileName(inFile.PostedFile.FileName);
					string path = Server.MapPath("Excel") + "\\";
					string pathFn = Server.MapPath("Excel") + "\\" + this.User.ToString() + "_" + fn;

					ViewState["FileName"] = pathFn;

					try
					{
						if(System.IO.Directory.Exists(path) == false)
							System.IO.Directory.CreateDirectory(path);

						if (System.IO.File.Exists(pathFn)) 
							System.IO.File.Delete(pathFn);

						inFile.PostedFile.SaveAs(pathFn);

						this.lblCPErrorMessage.Text = "";
					}
					catch(Exception ex)
					{
						throw new ApplicationException("Error during upload file to the server", null);
					}
				}
				else
				{
					this.lblCPErrorMessage.Text = "Not a valid Excel Workbook.";
					txtFilePath.Text = "";
					status = false;
				}
			}
			else
			{
				lblCPErrorMessage.Text = "Please select a file to upload.";
				status = false;
			}

			return status;
		}


		private void getExcelFiles()
		{
			int i = 0;

			try
			{	
				String connString = ConfigurationSettings.AppSettings["ExcelConn"];
				connString = connString.Replace("(DestinationFile)", (String)ViewState["FileName"]);

				int recCouter = 1;

				StringBuilder strBuilder = new StringBuilder();
				strBuilder.Append("SELECT  zipcode, zone_code, effective_date ");
				//HC Return Task
				strBuilder.Append("FROM [Customer_Zones$] ");
				OleDbDataAdapter daCustZone = new OleDbDataAdapter(strBuilder.ToString(), connString);

				DataSet dsExcel = new DataSet();
				daCustZone.Fill(dsExcel);

				DataSet beginCustZone = new DataSet();
				DataTable dt = new DataTable("tempck");
				dt.Columns.Add(new DataColumn("zipcode",typeof(string)));
				dt.Columns.Add(new DataColumn("zonecode",typeof(string)));
				dt.Columns.Add(new DataColumn("effectivedate",typeof(string)));
				beginCustZone.Tables.Add(dt);

				if(beginCustZone.Tables[0].Rows.Count > 0)
				{
					beginCustZone.Tables[0].Rows[0].Delete();
					beginCustZone.AcceptChanges();
				}


				DataSet dsCustZone = new DataSet();
				dsCustZone = SysDataMgrDAL.GetEmptyCustZones(0);
				if(dsCustZone.Tables[0].Rows.Count == 1)
				{
					dsCustZone.Tables[0].Rows[0].Delete();
					dsCustZone.AcceptChanges();
				}
				foreach(DataRow dr in dsExcel.Tables[0].Rows)
				{
					if((dr["zipcode"].ToString().Trim() != "") ||
						(dr["zone_code"].ToString().Trim() != "") || 
						(dr["effective_date"].ToString().Trim() != ""))//HC Return Task
					{
						DataRow tmpDr = dsCustZone.Tables[0].NewRow();
						DataRow drBegin = beginCustZone.Tables["tempck"].NewRow();

						//tmpDr["recordid"] = recCouter.ToString();
						recCouter++;

						//						tmpDr["custid"] = Convert.ToString(dr["custid"]);
						tmpDr["zipcode"] = Convert.ToString(dr["zipcode"]);
						tmpDr["zone_code"] = Convert.ToString(dr["zone_code"]);
						tmpDr["effective_date"] = Convert.ToString(dr["effective_date"]);//HC Return Task

						drBegin["zipcode"] = Convert.ToString(dr["zipcode"]);
						drBegin["zonecode"] = Convert.ToString(dr["zone_code"]);
						drBegin["effectivedate"] = Convert.ToString(dr["effective_date"]);


						dsCustZone.Tables[0].Rows.Add(tmpDr);
						dsCustZone.AcceptChanges();

						beginCustZone.Tables[0].Rows.Add(drBegin);
						beginCustZone.AcceptChanges();
					}					
				}

				dsExcel.Dispose();
				dsExcel = null;
				i++;
				recCouter = 1;
				ViewState["beginCuszoneImport"] = beginCustZone;
				Session["dsCustZone_Import"] = dsCustZone;
			}
			catch(Exception ex)
			{
				String strMsg = ex.Message;


				if(strMsg.IndexOf("'Consignments$' is not a valid name") != -1)
				{
					throw new ApplicationException("Excel Workbook must contain two named sheets: Consignments and Packages.", null);
				}
				else if(strMsg.IndexOf("'Packages$' is not a valid name") != -1)
				{
					throw new ApplicationException("Excel Workbook must contain two named sheets: Consignments and Packages.", null);
				}
				else if(strMsg.IndexOf("No value given for one or more required parameters") != -1)
				{
					if(i == 0)
						throw new ApplicationException("Consignments worksheet has invalid columns", null);
					else
						throw new ApplicationException("Packages worksheet has invalid columns", null);
				}
				else
				{
					throw ex;
				}
			}
		}


		private DataSet EliminateDuplicateRecords(DataSet ds)
		{
			

			if(ds.Tables[0].Rows.Count<=0)
				return ds;

			DataSet dsZones = new DataSet();
			String strCustID = (String)ViewState["currentCustomer"];

			for(int i=0;i<ds.Tables[0].Rows.Count;i++)
			{
				dsZones = SysDataMgrDAL.GetCustFRData(utility.GetAppID(), utility.GetEnterpriseID(),
					strCustID, ds.Tables[0].Rows[i][0].ToString(), ds.Tables[0].Rows[i][1].ToString(), ds.Tables[0].Rows[i][2].ToString());
				
				if(dsZones.Tables[0].Rows.Count>0)
				{
					ds.Tables[0].Rows.RemoveAt(i);
					ds.AcceptChanges();
				}
			}

			return ds;
		}


		private DataSet ValidateCustZones(DataSet ds)
		{
			if(ds.Tables[0].Rows.Count<=0)
				return ds;

			DataSet dsZones = new DataSet();
			String strCustID = (String)ViewState["currentCustomer"];

			for(int i=0;i<ds.Tables[0].Rows.Count;i++)
			{
				//Check ZipCode (in Zipcode Table)
				Zipcode zipcode = new Zipcode();
				zipcode.Populate(utility.GetAppID(),utility.GetEnterpriseID(),ds.Tables[0].Rows[i][0].ToString());

				TextBox txtCountry = (TextBox)customerProfileMultiPage.FindControl("txtCountry");				

				//	if(dsZones.Tables[0].Rows.Count>0)
				if(zipcode.Country!=txtCountry.Text)
				{
					ds.Tables[0].Rows.RemoveAt(i);
					ds.AcceptChanges();
				}

				//CheckZoneCode
				DataSet zoneCount = new DataSet();
				zoneCount = SysDataMgrDAL.Get_CheckCustZoneCode(utility.GetAppID(), utility.GetEnterpriseID(),
					ds.Tables[0].Rows[i][1].ToString());
				if(zoneCount.Tables[0].Rows.Count<=0)
				{
					if(ds.Tables[0].Rows.Count<=0)
						return ds;
					ds.Tables[0].Rows.RemoveAt(i);
					ds.AcceptChanges();
				}

				if(ds.Tables[0].Rows.Count<=0)
					return ds;
				//Assign Effective_Date (in case not assigned)
				if(ds.Tables[0].Rows[i][2]==null)
				{
					ds.Tables[0].Rows[i][2] = System.DateTime.Now.ToString("dd/MM/yyyy");
					ds.AcceptChanges();
				}
				if(ds.Tables[0].Rows[i][2].ToString()=="")
				{
					ds.Tables[0].Rows[i][2] = System.DateTime.Now.ToString("dd/MM/yyyy");
					ds.AcceptChanges();
				}
			}

			return ds;

		}

		private void btnImport_Save_Click(object sender, System.EventArgs e)
		{
			String strCustID = (String)ViewState["currentCustomer"];
			DataSet dsCustZone_Import = (DataSet) Session["dsCustZone_Import"];
			DataSet dsZones = new DataSet();

			int iRows = 0;

			for(int i=0;i<dsCustZone_Import.Tables[0].Rows.Count;i++)
			{

				//				dsZones = SysDataMgrDAL.GetCustFRData(utility.GetAppID(), utility.GetEnterpriseID(),
				//					strCustID, dsCustZone_Import.Tables[0].Rows[i][0].ToString(), dsCustZone_Import.Tables[0].Rows[i][1].ToString(), dsCustZone_Import.Tables[0].Rows[i][2].ToString());
				
				//				if(dsZones.Tables[0].Rows.Count<=0)
				//				{
					
				//				}
				//				else
				//				{
				//					if(dsZones.Tables[0].Rows.Count > 0)
				//					{
				//						this.lblCPErrorMessage.Text= "Error. Duplicate Data.";
				//						//Response.Write("<script>alert('Error. Duplicate Data.');</script>");
				//						return;
				//					}
				object objeffective = System.DBNull.Value;
				if(dsCustZone_Import.Tables[0].Rows[i][2].ToString() != "")
					objeffective = dsCustZone_Import.Tables[0].Rows[i][2].ToString();

				if(SysDataMgrDAL.IsCustFRExist(utility.GetAppID(),utility.GetEnterpriseID(), strCustID,dsCustZone_Import.Tables[0].Rows[i][0].ToString(),objeffective))
				{
					this.lblCPErrorMessage.Text = "Data is duplicate.";
					//Response.Write("<script>alert('Incorrect ZipCode.');</script>");
					return;
				}

				Zipcode zipcode = new Zipcode();
				zipcode.Populate(utility.GetAppID(),utility.GetEnterpriseID(),dsCustZone_Import.Tables[0].Rows[i][0].ToString());

				TextBox txtCountry = (TextBox)customerProfileMultiPage.FindControl("txtCountry");				

				if(zipcode.Country!=txtCountry.Text)
				{
					this.lblCPErrorMessage.Text = "Incorrect ZipCode.";
					//Response.Write("<script>alert('Incorrect ZipCode.');</script>");
					return;
				}

				//CheckZoneCode

				DataSet zoneCount = new DataSet();
				zoneCount = SysDataMgrDAL.Get_CheckCustZoneCode(utility.GetAppID(), utility.GetEnterpriseID(),
					dsCustZone_Import.Tables[0].Rows[i][1].ToString());
				if(zoneCount.Tables[0].Rows.Count<=0)
				{
					this.lblCPErrorMessage.Text = "Incorrect ZoneCode.";
					//Response.Write("<script>alert('Incorrect ZoneCode.');</script>");
					return;
				}


				//Assign Effective_Date (in case not assigned)
				if(dsCustZone_Import.Tables[0].Rows[i][2].ToString() =="")
				{
					this.lblCPErrorMessage.Text = "Incorrect Date.";
					return;
				}

				iRows += SysDataMgrDAL.InsertCustFR(utility.GetAppID(),utility.GetEnterpriseID(), strCustID, 
					dsCustZone_Import.Tables[0].Rows[i][0].ToString(),
					dsCustZone_Import.Tables[0].Rows[i][1].ToString(),
					dsCustZone_Import.Tables[0].Rows[i][2].ToString());
				//}
			}

			EnableZoneUpload(false);

			if(iRows > 0)
			{
				this.lblCPErrorMessage.Text = "Insert "+iRows+" row(s).";

				dsCustZones = SysDataMgrDAL.GetCustFRData(utility.GetAppID(), utility.GetEnterpriseID(),
					strCustID, "", "", "");
				Session["dsCustZones"] = dsCustZones;
				dgCustZones.EditItemIndex = -1;
				BindCustZonesGrid();

				dgCustZones.Columns[0].Visible = false;
			}

		}
		private void btnSetBillPlacementRules_Click(object sender, System.EventArgs e)
		{
			lblCPErrorMessage.Text = "";
			msTextBox txtAccNo = (msTextBox)customerProfileMultiPage.FindControl("txtAccNo");
			string sessionName = "dtBillPlacementRules_"+txtAccNo.Text.Trim();
			if(Session["CustID"] == null)
				Session["CustID"] = "";
			if(Session[sessionName] == null || (String)Session["CustID"] != txtAccNo.Text.Trim())
				Session[sessionName] = SysDataMgrDAL.GetCustomerProfile_BillPlacementRules(utility.GetAppID(),utility.GetEnterpriseID(),txtAccNo.Text);
			//			Session["CustID"] = null;
			if(txtAccNo.Text.Length > 0)
				Session["CustID"] = txtAccNo.Text.Trim();
			
			if((bool)ViewState["IsTextChanged"] == false)
			{
				ChangeCPState();
				ViewState["IsTextChanged"] = true;
			}

			String sUrl = "BillPlacementRulesPopUp.aspx?AccNo="+txtAccNo.Text.Trim();
			ArrayList paramList = new ArrayList();
			paramList.Add(sUrl);
			String sScript = Utility.GetScript("openParentWindow.js",paramList);
			Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);	
		}

		[Cambro.Web.DbCombo.ResultsMethodAttribute(true)]
		public static object MasterAccountServerMethod(Cambro.Web.DbCombo.ServerMethodArgs args)
		{
			Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings,HttpContext.Current.Session);
			String strAppID = util.GetAppID();
			String strEnterpriseID = util.GetEnterpriseID();
			DataSet dataset = com.ties.classes.DbComboDAL.MasterAccountQuery(strAppID,strEnterpriseID,args);
			return dataset;
		}
		private void txtSpecialInstruction_TextChanged(object sender, System.EventArgs e)
		{
			if((bool)ViewState["IsTextChanged"] == false)
			{
				ChangeCPState();
				ViewState["IsTextChanged"] = true;
			}
		}
		private void txtNextBillPlacementDate_TextChanged(object sender, System.EventArgs e)
		{
			if((bool)ViewState["IsTextChanged"] == false)
			{
				DateTime tmpDate = DateTime.ParseExact(txtNextBillPlacementDate.Text.Trim(),"dd/MM/yyyy",null);
				if(tmpDate < DateTime.Now.Date)
				{
					lblCPErrorMessage.Text = "Next Bill Placement Date can't less than presents";
					return;
				}
				ChangeCPState();
				ViewState["IsTextChanged"] = true;
			}
		}

		private void txtMinimumBox_TextChanged(object sender, System.EventArgs e)
		{
			if((bool)ViewState["IsTextChanged"] == false)
			{
				ChangeCPState();
				ViewState["IsTextChanged"] = true;
			}
		}

		private void btnChangeStatus_Click(object sender,System.EventArgs e)
		{
			//			String sUrl = "CreditStatusTermPopup.aspx?FORMID=CustomerProfile&ZIPCODE="+strZipcode+
			//				"&ZIPCODE_CID="+strZipcodeClientID+
			//				"&STATE_CID="+strStateClientID+
			//				"&COUNTRY_CID="+strCountryClientID+
			//			String sUrl = "CustomerPopup.aspx?FORMID="+"DomesticShipment"+
			//			"&CustType="+ddbCustType.SelectedItem.Value;
			//			ArrayList paramList = new ArrayList();	"&ISFR=N";



			string strCustID = "";
			TextBox txtCustName = (TextBox)customerProfileMultiPage.FindControl("txtCustomerName");	
	
			string mode = "";
			if(ViewState["CPOperation"] != null)
			{
				int iOperation = (int)ViewState["CPOperation"];
		
				if(iOperation == (int)Operation.Insert)
				{
					mode = "A";
				}
				else if((iOperation == (int)Operation.Update) || (iOperation == (int)Operation.None))
				{
					
					//	TextBox txtAccNo = (TextBox)customerProfileMultiPage.FindControl("txtAccNo");	
					if(getCountofCreditused() <= 0)
					{
						mode = "E";
						TextBox txtCreditLimit = (TextBox)customerProfileMultiPage.FindControl("txtCreditLimit");	
						TextBox txtAccNo = (TextBox)customerProfileMultiPage.FindControl("txtAccNo");	
						TextBox txtCreditTerm = (TextBox)customerProfileMultiPage.FindControl("txtCreditTerm");	
						TextBox txtCreditThredholds = (TextBox)customerProfileMultiPage.FindControl("txtCreditThredholds");	
						if(txtCreditLimit.Text != "" && Decimal.Parse(txtCreditLimit.Text) >= 0)
						{
							
							SysDataMgrDAL.AddCustomerCredit(utility.GetAppID(),utility.GetEnterpriseID(),
								txtAccNo.Text,
								txtCreditLimit.Text,
								txtCreditTerm.Text,
								"R",
								"A",
								txtCreditThredholds.Text.Length > 0 ? txtCreditThredholds.Text : "0",
								"",
								"",
								"","N",utility.GetUserID());
							//DataRow drEach = dsToInsert.Tables[0].Rows[0];
							//drEach["credit_limit"]=Convert.ToDecimal(ht["CreditLimit"].ToString());
						}
					}
					else { mode="E"; }
				}
				else
				{
					mode="E";
				}
			}
			

			//			if(ViewState["currentCustomer"] != null)
			//			{
			//				mode = "A";
			//
			//			}
			//			else
			//			{
			//					mode = "E";
			//			}
			string strCustName = "";
			TextBox txtcustid = (TextBox)customerProfileMultiPage.FindControl("txtAccNo");		
			strCustID = txtcustid.Text;
			
			if(strCustID == "")
			{
				lblCPErrorMessage.Text = "Please insert Account No";
				this.RegisterStartupScript("pageload","<script language=javascript>document.getElementById('" + txtcustid.ClientID + "').select();</script>");
				return;
			}
			if(txtCustName != null)

			{
				strCustName = txtCustName.Text;
			}
			string type = "";
			RadioButton rdCredit=(RadioButton)customerProfileMultiPage.FindControl("radioBtnCredit");
			if(rdCredit.Checked)
			{
				type = "R";//credit
			}
			else
			{
				type = "C";//cash
			}
			DbCombo db = (DbCombo)customerProfileMultiPage.FindControl("DbComboMasterAccount");		
			string masteracc = "";
			
			if(db != null)
			{
				masteracc = db.Value;
			}

			String sUrl ="CreditStatusTermPopup.aspx?FORMID="+"CustomerProfile&CustID=" + strCustID + "&CUSTNAME=" + strCustName + "&masteraccont=" + masteracc + "&mode=" + mode+"&type="+type;
			// add by chai 03/02/2012
			m_sdsCustomerProfile = (SessionDS)Session["SESSION_DS1"];
			DataRow drCurrent =null;
			if(m_sdsCustomerProfile!=null)
				drCurrent = m_sdsCustomerProfile.ds.Tables[0].Rows[Convert.ToInt32(ViewState["currentPage"])];
			if(drCurrent != null)
			{
				double dCreditLimit=0;
				double dCreditUsed =0;
				double decMaxAmt=0;
				if(drCurrent["credit_limit"] != DBNull.Value && drCurrent["credit_limit"].ToString().Length>0)
				{
					dCreditLimit = double.Parse(drCurrent["credit_limit"].ToString());
				}
				if(drCurrent["credit_used"] != DBNull.Value && drCurrent["credit_used"].ToString().Length>0)
				{
					dCreditUsed = double.Parse(drCurrent["credit_used"].ToString());
				}


				sUrl+="&creditused="+ String.Format((String)ViewState["m_format"], dCreditUsed);
				sUrl+="&creditavailable="+ String.Format((String)ViewState["m_format"],dCreditLimit-dCreditUsed);;
			}
			//---------End
			ArrayList paramList = new ArrayList();
			paramList.Add(sUrl);
			String sScript = Utility.GetScript("openWindowParam.js",paramList);
			Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);

		}

		private void EnterpriseConfigurations()
		{
			CustomerProfileConfigurations conf = new CustomerProfileConfigurations();
			conf = EnterpriseConfigMgrDAL.GetCustomerProfileConfigurations(utility.GetAppID(),utility.GetEnterpriseID());
			
			//----------------- HCSupportedbyEnterprise ---------------------------------------
			RequiredFieldValidator validatePodSlipRequired = (RequiredFieldValidator)customerProfileMultiPage.FindControl("validatePodSlipRequired");
			Label lblreqPodSlipRequired = (Label)customerProfileMultiPage.FindControl("lblreqPodSlipRequired");
			RequiredFieldValidator validateInvoiceRequired = (RequiredFieldValidator)customerProfileMultiPage.FindControl("validateInvoiceRequired");
			Label lblreqInvoiceRequired = (Label)customerProfileMultiPage.FindControl("lblreqInvoiceRequired");
			Label lblPodSlipRequired = (Label)customerProfileMultiPage.FindControl("lblPodSlipRequired");
			Label lblInvoiceRequired = (Label)customerProfileMultiPage.FindControl("lblInvoiceRequired");
			DropDownList ddlPodSlipRequired = (DropDownList)customerProfileMultiPage.FindControl("ddlPodSlipRequired");
			DropDownList ddlInvoiceRequried = (DropDownList)customerProfileMultiPage.FindControl("ddlInvoiceRequried");

			validatePodSlipRequired.Enabled = conf.HCSupportedbyEnterprise;
			lblreqPodSlipRequired.Visible = conf.HCSupportedbyEnterprise;
			validateInvoiceRequired.Enabled = conf.HCSupportedbyEnterprise;
			lblreqInvoiceRequired.Visible = conf.HCSupportedbyEnterprise;
			lblPodSlipRequired.Visible = conf.HCSupportedbyEnterprise;
			ddlPodSlipRequired.Visible = conf.HCSupportedbyEnterprise;
			lblInvoiceRequired.Visible = conf.HCSupportedbyEnterprise;
			ddlInvoiceRequried.Visible = conf.HCSupportedbyEnterprise;
			if(conf.HCSupportedbyEnterprise == false)
			{
				int selectedY = ddlPodSlipRequired.Items.IndexOf(ddlPodSlipRequired.Items.FindByValue("Y"));
				if(selectedY != ddlPodSlipRequired.SelectedIndex)
				{
					ddlPodSlipRequired.SelectedIndex=ddlPodSlipRequired.Items.IndexOf(ddlPodSlipRequired.Items.FindByValue("N"));
					ddlInvoiceRequried.SelectedIndex=ddlInvoiceRequried.Items.IndexOf(ddlInvoiceRequried.Items.FindByValue("N"));
				}
			}
			//----------------- CODSupportedbyEnterprise ---------------------------------------
			Label Label13 = (Label)customerProfileMultiPage.FindControl("Label13");
			msTextBox txtCODSurchargeAmt = (msTextBox)customerProfileMultiPage.FindControl("txtCODSurchargeAmt");
			Label Label16 = (Label)customerProfileMultiPage.FindControl("Label16");
			msTextBox txtCODSurchargePercent = (msTextBox)customerProfileMultiPage.FindControl("txtCODSurchargePercent");
			Label lblMaxCODSurcharge = (Label)customerProfileMultiPage.FindControl("lblMaxCODSurcharge");
			msTextBox txtMaxCODSurcharge = (msTextBox)customerProfileMultiPage.FindControl("txtMaxCODSurcharge");
			Label lblMinCODSurcharge = (Label)customerProfileMultiPage.FindControl("lblMinCODSurcharge");
			msTextBox txtMinCODSurcharge = (msTextBox)customerProfileMultiPage.FindControl("txtMinCODSurcharge");

			Label13.Visible = conf.CODSupportedbyEnterprise;
			txtCODSurchargeAmt.Visible = conf.CODSupportedbyEnterprise;
			Label16.Visible = conf.CODSupportedbyEnterprise;
			txtCODSurchargePercent.Visible = conf.CODSupportedbyEnterprise;
			lblMaxCODSurcharge.Visible = conf.CODSupportedbyEnterprise;
			txtMaxCODSurcharge.Visible = conf.CODSupportedbyEnterprise;
			lblMinCODSurcharge.Visible = conf.CODSupportedbyEnterprise;
			txtMinCODSurcharge.Visible = conf.CODSupportedbyEnterprise;
			//----------------- InsSupportedbyEnterprise ---------------------------------------
			Label lblInsurancePercentSurcharge = (Label)customerProfileMultiPage.FindControl("lblInsurancePercentSurcharge");
			msTextBox txtInsurancePercentSurcharge = (msTextBox)customerProfileMultiPage.FindControl("txtInsurancePercentSurcharge");
			Label lblFreeInsuranceAmt = (Label)customerProfileMultiPage.FindControl("lblFreeInsuranceAmt");
			msTextBox txtFreeInsuranceAmt = (msTextBox)customerProfileMultiPage.FindControl("txtFreeInsuranceAmt");
			Label Label14 = (Label)customerProfileMultiPage.FindControl("Label14");
			msTextBox txtMaxAmt = (msTextBox)customerProfileMultiPage.FindControl("txtMaxAmt");
			Label lblreqMinInsuranceSurcharge = (Label)customerProfileMultiPage.FindControl("lblreqMinInsuranceSurcharge");
			msTextBox txtMinInsuranceSurcharge = (msTextBox)customerProfileMultiPage.FindControl("txtMinInsuranceSurcharge");
			Label lblregMaxInsuranceSurcharge = (Label)customerProfileMultiPage.FindControl("lblregMaxInsuranceSurcharge");
			msTextBox txtMaxInsuranceSurcharge = (msTextBox)customerProfileMultiPage.FindControl("txtMaxInsuranceSurcharge");

			lblInsurancePercentSurcharge.Visible = conf.InsSupportedbyEnterprise;
			txtInsurancePercentSurcharge.Visible = conf.InsSupportedbyEnterprise;
			lblFreeInsuranceAmt.Visible = conf.InsSupportedbyEnterprise;
			txtFreeInsuranceAmt.Visible = conf.InsSupportedbyEnterprise;
			Label14.Visible = conf.InsSupportedbyEnterprise;
			txtMaxAmt.Visible = conf.InsSupportedbyEnterprise;
			lblreqMinInsuranceSurcharge.Visible = conf.InsSupportedbyEnterprise;
			txtMinInsuranceSurcharge.Visible = conf.InsSupportedbyEnterprise;
			lblregMaxInsuranceSurcharge.Visible = conf.InsSupportedbyEnterprise;
			txtMaxInsuranceSurcharge.Visible = conf.InsSupportedbyEnterprise;
		}


		public static void SetInitialFocus(Control control) 
		{ 
			if (control.Page == null) 
			{ 
				throw new ArgumentException( 
					"The Control must be added to a Page before you can set the IntialFocus to it."); 
			} 
			if (control.Page.Request.Browser.JavaScript == true) 
			{ 
				// Create JavaScript 
				StringBuilder s = new StringBuilder(); 
				s.Append("\n<SCRIPT LANGUAGE='JavaScript'>\n"); 
				s.Append("<!--\n"); 
				s.Append("function SetInitialFocus()\n"); 
				s.Append("{\n"); 
				s.Append("   document."); 

				// Find the Form 
				Control p = control.Parent; 
				while (!(p is System.Web.UI.HtmlControls.HtmlForm)) 
					p = p.Parent; 
				s.Append(p.ClientID); 

				s.Append("['"); 
				s.Append(control.ClientID); 
				s.Append("'].focus();\n"); 
				s.Append("}\n"); 
				s.Append("// -->\n"); 
				s.Append("window.onload = SetInitialFocus;\n"); 
				s.Append("</SCRIPT>"); 

				// Register Client Script 
				control.Page.RegisterClientScriptBlock("InitialFocus", s.ToString()); 
			} 
		}
	}
}
