<%@ Import Namespace="com.common.util" %>
<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<%@ Page language="c#" Codebehind="Conveyance.aspx.cs" AutoEventWireup="false" Inherits="com.ties.Conveyance" smartNavigation="true" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Conveyance</title>
		<link href="css/styles.css" rel="stylesheet" rev="stylesheet">
		<meta name="GENERATOR" Content="Microsoft Visual Studio 7.0">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<!--#INCLUDE FILE="msFormValidations.inc"-->
		<script language="javascript" src="Scripts/settingScrollPosition.js"></script>
	</HEAD>
	<body onclick="GetScrollPosition();" onload="SetScrollPosition();" MS_POSITIONING="GridLayout">
		<form id="Conveyance" method="post" runat="server">
			<asp:Label id="lblTitle" style="Z-INDEX: 101; LEFT: 20px; POSITION: absolute; TOP: 6px" runat="server" Width="330px" Height="32px" CssClass="mainTitleSize">Conveyance</asp:Label>
			<asp:Label id="lblErrorMessage" style="Z-INDEX: 107; LEFT: 24px; POSITION: absolute; TOP: 71px" runat="server" Width="656px" CssClass="errorMsgColor">Error Message</asp:Label>
			<asp:ValidationSummary id="Pagevalid" style="Z-INDEX: 106; LEFT: 359px; POSITION: absolute; TOP: 4px" runat="server" ShowMessageBox="True" ShowSummary="False" HeaderText="Please enter the missing  fields."></asp:ValidationSummary>
			<asp:Button id="btnInsert" style="Z-INDEX: 104; LEFT: 217px; POSITION: absolute; TOP: 41px" runat="server" Text="Insert" CausesValidation="False" CssClass="queryButton"></asp:Button>
			<asp:Button id="btnExecuteQuery" style="Z-INDEX: 103; LEFT: 87px; POSITION: absolute; TOP: 41px" runat="server" Text="Execute Query" CausesValidation="False" Width="130px" CssClass="queryButton"></asp:Button>
			<asp:Button id="btnQuery" style="Z-INDEX: 102; LEFT: 22px; POSITION: absolute; TOP: 41px" runat="server" Text="Query" CausesValidation="False" Width="65px" CssClass="queryButton"></asp:Button>
			<asp:DataGrid id="dgConveyance" runat="server" AllowCustomPaging="True" AllowPaging="True" AutoGenerateColumns="False" style="Z-INDEX: 105; LEFT: 21px; POSITION: absolute; TOP: 98px" HorizontalAlign="Left" Width="776px" OnEditCommand="dgConveyance_Edit" OnCancelCommand="dgConveyance_Cancel" OnItemDataBound="dgConveyance_ItemDataBound" OnDeleteCommand="dgConveyance_Delete" OnUpdateCommand="dgConveyance_Update" OnPageIndexChanged="dgConveyance_PageChange">
				<ItemStyle HorizontalAlign="Left" CssClass="gridfield"></ItemStyle>
				<Columns>
					<asp:EditCommandColumn ButtonType="LinkButton" UpdateText="&lt;IMG SRC='images/butt-update.gif' border=0 title='Update' &gt;" CancelText="&lt;IMG SRC='images/butt-cancel.gif' border=0 title='Cancel' &gt;" EditText="&lt;IMG SRC='images/butt-edit.gif' border=0 title='Edit' &gt;">
						<HeaderStyle Width="0.5%" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle HorizontalAlign="Left"></ItemStyle>
					</asp:EditCommandColumn>
					<asp:ButtonColumn Text="&lt;IMG SRC='images/butt-delete.gif' border=0 title='Delete' &gt;" ButtonType="LinkButton" CommandName="Delete">
						<HeaderStyle Width="0.5%" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle HorizontalAlign="Left"></ItemStyle>
					</asp:ButtonColumn>
					<asp:TemplateColumn HeaderText="Conveyance Code" HeaderStyle-Font-Bold="true">
						<HeaderStyle HorizontalAlign="Left" Width="2%" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle CssClass="gridField"></ItemStyle>
						<ItemTemplate>
							<asp:Label ID="lblConveyance" CssClass="gridLabel" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"conveyance_code")%>'>
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<cc1:mstextbox CssClass="gridTextBox" ID="txtConveyance" Text='<%#DataBinder.Eval(Container.DataItem,"conveyance_code")%>' Runat="server" Enabled="True" TextMaskType="msUpperAlfaNumericWithUnderscore" MaxLength="12">
							</cc1:mstextbox>
							<asp:RequiredFieldValidator ID="cConveyance" Runat="server" BorderWidth="0" Display="None" ControlToValidate="txtConveyance" ErrorMessage="Conveyance Code "></asp:RequiredFieldValidator>
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Description">
						<HeaderStyle Width="3%" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle CssClass="gridField"></ItemStyle>
						<ItemTemplate>
							<asp:Label ID="lblCCDescp" CssClass="gridLabel" Text='<%#DataBinder.Eval(Container.DataItem,"conveyance_description")%>' Runat="server">
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<asp:TextBox ID="txtCCDescp" CssClass="gridTextBox" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"conveyance_description")%>' MaxLength="200">
							</asp:TextBox>
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Length">
						<HeaderStyle HorizontalAlign="Left" Width="2%" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle CssClass="gridField"></ItemStyle>
						<ItemTemplate>
							<asp:Label ID="lblLength" CssClass="gridLabel" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Length","{0:#0.00}")%>'>
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<cc1:mstextbox CssClass="gridTextBox" ID="txtLength" Text='<%#DataBinder.Eval(Container.DataItem,"Length")%>' Runat="server" Enabled="True" MaxLength="10" TextMaskType="msNumeric" NumberMaxValue="999999" NumberMinValue="-999999" NumberPrecision="8" NumberScale="2" TextMaskString="#.00" >
							</cc1:mstextbox>
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Breadth">
						<HeaderStyle HorizontalAlign="Left" Width="2%" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle CssClass="gridField"></ItemStyle>
						<ItemTemplate>
							<asp:Label ID="lblBreadth" CssClass="gridLabel" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Breadth","{0:#0.00}")%>'>
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<cc1:mstextbox CssClass="gridTextBox" ID="txtBreadth" Text='<%#DataBinder.Eval(Container.DataItem,"Breadth")%>' Runat="server" Enabled="True" MaxLength="10" TextMaskType="msNumeric" NumberMaxValue="999999" NumberMinValue="-999999" NumberPrecision="8" NumberScale="2" TextMaskString="#.00" >
							</cc1:mstextbox>
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Height">
						<HeaderStyle HorizontalAlign="Left" Width="2%" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle CssClass="gridField"></ItemStyle>
						<ItemTemplate>
							<asp:Label ID="lblHeight" CssClass="gridLabel" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Height","{0:#0.00}")%>'>
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<cc1:mstextbox CssClass="gridTextBox" ID="txtHeight" Text='<%#DataBinder.Eval(Container.DataItem,"Height")%>' Runat="server" Enabled="True" MaxLength="10" TextMaskType="msNumeric" NumberMaxValue="999999" NumberMinValue="-999999" NumberPrecision="8" NumberScale="2" TextMaskString="#.00" >
							</cc1:mstextbox>
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Max Weight">
						<HeaderStyle HorizontalAlign="Left" Width="2%" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle CssClass="gridField"></ItemStyle>
						<ItemTemplate>
							<asp:Label ID="lblWeight" CssClass="gridLabel" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"max_wt","{0:#0.00}")%>'>
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<cc1:mstextbox CssClass="gridTextBox" ID="txtWeight" Text='<%#DataBinder.Eval(Container.DataItem,"max_wt")%>' Runat="server" Enabled="True" MaxLength="10" TextMaskType="msNumeric" NumberMaxValue="999999" NumberMinValue="-999999" NumberPrecision="8" NumberScale="2" TextMaskString="#.00" >
							</cc1:mstextbox>
						</EditItemTemplate>
					</asp:TemplateColumn>
				</Columns>
				<PagerStyle NextPageText="Next" PrevPageText="Previous" CssClass="normalText" Position="Bottom" HorizontalAlign="Left"></PagerStyle>
			</asp:DataGrid>
			<input type=hidden value="<%=strScrollPosition%>" name=ScrollPosition>
		</form>
	</body>
</HTML>
