using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Text;
using com.common.DAL;
using com.common.classes;
using com.common.util;
using com.ties.classes;
using com.common.applicationpages;
using com.ties.DAL;
//using com.ties;

namespace TIES.WebUI
{
	/// <summary>
	/// Summary description for InvoicePaymentsUpdate.
	/// </summary>
	public class InvoicePaymentsUpdate : BasePopupPage
	{
		protected System.Web.UI.WebControls.TextBox txtCustID;
		protected System.Web.UI.WebControls.Label lblCustName;
		protected System.Web.UI.WebControls.Label lblCustID;
		protected System.Web.UI.WebControls.Button btnRefresh;
		protected System.Web.UI.WebControls.Button btnOK;
		protected System.Web.UI.WebControls.Label lblInvoiceStatus;
		protected System.Web.UI.WebControls.Label lblInvDispStat;
		protected System.Web.UI.WebControls.Label lblInvNo;
		protected System.Web.UI.WebControls.Label lblPrintedDueDate;
		protected System.Web.UI.WebControls.Label lblInternalDueDate;
		protected System.Web.UI.WebControls.Label lblCustDispName;
		protected System.Web.UI.WebControls.Label lblLastUpdate;
		protected System.Web.UI.WebControls.Label lblApproveDate;
		protected System.Web.UI.WebControls.Label lblActualBPD;
		protected System.Web.UI.WebControls.Label lblPPD;
		protected System.Web.UI.WebControls.Label lblAmntDate;
		protected System.Web.UI.WebControls.Label lblDispApprove;
		protected System.Web.UI.WebControls.Label lblDispUpdate;
		protected System.Web.UI.WebControls.Label lblPaidDate;
		protected System.Web.UI.WebControls.Label Label2;
		protected System.Web.UI.WebControls.Label lblBatchNo;
		protected System.Web.UI.WebControls.Label lblTotalInvAmnt;
		protected System.Web.UI.WebControls.Label lblCurrentAmntPaid;
		protected System.Web.UI.WebControls.Label lblBalanceDue;
		protected System.Web.UI.WebControls.DropDownList ddlLinkToCDN;
		protected com.common.util.msTextBox txtPromisedPaymentDate;
		protected com.common.util.msTextBox txtPaidDate;
		protected System.Web.UI.WebControls.Label LblHeading;		
		protected System.Web.UI.WebControls.TextBox txtInvNo;
		protected com.common.util.msTextBox txtActualBPD;
		protected System.Web.UI.WebControls.TextBox txtBatchNo;
		protected com.common.util.msTextBox txtAmntPaid;
		private String InvNo;
		protected int currency_decimal;
		protected String strFormatCurrency;
		protected com.common.util.msTextBox txtTotalInvAmnt;
		protected com.common.util.msTextBox txtBalanceDue;
		protected com.common.util.msTextBox txtCurrAmntPaid;
		protected com.common.util.msTextBox txtInternalDueDate;
		protected com.common.util.msTextBox txtPrintedDueDate;
		private String CustId;
		private String entID,appID,culture;
		protected System.Web.UI.WebControls.Label Label1;
		protected System.Web.UI.WebControls.DataGrid dgAssociatedCNDN;
		protected System.Web.UI.WebControls.Label ErrMsg;
		protected System.Web.UI.WebControls.Label lblPaymentUpd;
		protected System.Web.UI.WebControls.Label lblDispPaymentUpdate;
		protected System.Web.UI.WebControls.Label lblApplyAmt;
		protected com.common.util.msTextBox txtApplyAmt;
		protected System.Web.UI.WebControls.TextBox txtUserName;
		private DataSet dsASCNDN;
	
		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			//getPageControls(Page);
			DataSet profileDS = SysDataManager1.GetEnterpriseProfile(utility.GetAppID(), utility.GetEnterpriseID());

			entID = utility.GetEnterpriseID();
			appID = utility.GetAppID();
			culture = utility.GetUserCulture();			
			if(profileDS.Tables[0].Rows.Count > 0)
			{
				currency_decimal = (int) profileDS.Tables[0].Rows[0]["currency_decimal"];
				ViewState["m_format"] = "{0:n" + currency_decimal.ToString() + "}";
				strFormatCurrency=ViewState["m_format"].ToString();
			}
			if(!Page.IsPostBack)
			{
				CustId=Request.Params["CUSTID_TEXT"];
				InvNo=Request.Params["INVOICE_ID"];
				getInvoiceDetail();				
				ViewState["CUSTID_TEXT"]=CustId;
				ViewState["INVOICE_ID"]=InvNo;	
				DisplayCurrentPage();
				setDGAssociatedCNDN();			
			}
			else
			{
				InvNo=(String)ViewState["INVOICE_ID"];
				dsASCNDN = (DataSet)Session["dsASCNDN"];
				Session["dsASCNDN"] = dsASCNDN;
			}
			
			getPageControls(Page);
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			
			base.OnInit(e);
			InitializeComponent();
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
			this.dgAssociatedCNDN.PageIndexChanged += new System.Web.UI.WebControls.DataGridPageChangedEventHandler(this.dgAssociatedCNDN_PageIndexChanged);
			this.dgAssociatedCNDN.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.dgAssociatedCNDN_ItemDataBound);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private void getInvoiceDetail()
		{
			try
			{
				DataSet InvDS = InvoiceManagementMgrDAL.GetInvoiceByInvNo(appID,entID,InvNo);
				Decimal dCAmntPaid = InvoicePaymentUpdateMgrDAL.getTotalCurrentAmntPaid(appID,entID,InvNo);
				Session["InvDS"] = InvDS;
				Session["dCAmntPaid"]=dCAmntPaid;
			}
			catch
			{
			}
		}
		private void DisplayCurrentPage()
		{
			try
			{
				if(Session["InvDS"]!=null)
				{
					txtUserName.Text = utility.GetUserID();
					Decimal dInvAmnt,dDN,dCN,dBalanceDue,dCAmntPaid =0;
					DataSet InvDS = (DataSet)Session["InvDS"];
					dCAmntPaid = (Decimal)Session["dCAmntPaid"];
					setInputFormat();
					setInputMode(InvDS.Tables[0].Rows[0]["invoice_status"].ToString().ToUpper());					
					txtCustID.Text = InvDS.Tables[0].Rows[0]["payerid"].ToString();
					lblCustDispName.Text = InvDS.Tables[0].Rows[0]["payer_name"].ToString();
					txtInvNo.Text = InvDS.Tables[0].Rows[0]["invoice_no"].ToString().ToUpper();
					DataSet dsInvStat = InvoiceManagementMgrDAL.GetInvoiceStatus(appID,entID,InvDS.Tables[0].Rows[0]["invoice_status"].ToString().ToUpper(),culture);
					lblInvDispStat.Text = dsInvStat.Tables[0].Rows[0]["code_text"].ToString().ToUpper();
					
					//txtBatchNo.TabIndex = InvDS.Tables[0].Rows[0]["invoice_status"].ToString().ToUpper();
					txtTotalInvAmnt.Text = String.Format(strFormatCurrency,InvDS.Tables[0].Rows[0]["invoice_amt"]);					
					dInvAmnt = Convert.ToDecimal(InvDS.Tables[0].Rows[0]["invoice_amt"]);					
					dDN = CreditDebitMgrDAL.getTotalDebitNote(appID,entID,InvDS.Tables[0].Rows[0]["invoice_no"].ToString().ToUpper());
					dCN = CreditDebitMgrDAL.getTotalCreditNote(appID,entID,InvDS.Tables[0].Rows[0]["invoice_no"].ToString().ToUpper());
					dBalanceDue = dInvAmnt+dDN-(dCN+dCAmntPaid);
					Session["dBalanceDue"]=dBalanceDue;
					txtBalanceDue.Text = String.Format(strFormatCurrency,dBalanceDue);						
					txtCurrAmntPaid.Text = String.Format(strFormatCurrency,dCAmntPaid);	
					if(Utility.IsNotDBNull(InvDS.Tables[0].Rows[0]["updated_by"])&&Utility.IsNotDBNull(InvDS.Tables[0].Rows[0]["updated_date"]))
					{
						lblDispUpdate.Text = InvDS.Tables[0].Rows[0]["updated_by"].ToString().ToUpper()+" "+Convert.ToDateTime(InvDS.Tables[0].Rows[0]["updated_date"]).ToString("dd/MM/yyyy");
					}
					else
					{
						lblDispUpdate.Text = "";
					}
					if(Utility.IsNotDBNull(InvDS.Tables[0].Rows[0]["approved_by"])&&Utility.IsNotDBNull(InvDS.Tables[0].Rows[0]["approved_date"]))
					{
						lblDispApprove.Text = InvDS.Tables[0].Rows[0]["approved_by"].ToString().ToUpper()+" "+Convert.ToDateTime(InvDS.Tables[0].Rows[0]["approved_date"]).ToString("dd/MM/yyyy");
					}
					else
					{
						lblDispApprove.Text = "";
					}
					if(Utility.IsNotDBNull(InvDS.Tables[0].Rows[0]["printed_due_date"]))
					{
						txtPrintedDueDate.Text = Convert.ToDateTime(InvDS.Tables[0].Rows[0]["printed_due_date"]).ToString("dd/MM/yyyy");
					}
					else
					{
						txtPrintedDueDate.Text = "";
					}
					if(Utility.IsNotDBNull(InvDS.Tables[0].Rows[0]["internal_due_date"]))
					{
						txtInternalDueDate.Text = Convert.ToDateTime(InvDS.Tables[0].Rows[0]["internal_due_date"]).ToString("dd/MM/yyyy");
					}
					else
					{
						txtInternalDueDate.Text = "";
					}
					if(Utility.IsNotDBNull(InvDS.Tables[0].Rows[0]["actual_bill_placement_date"]))
					{
						txtActualBPD.Text = Convert.ToDateTime(InvDS.Tables[0].Rows[0]["actual_bill_placement_date"]).ToString("dd/MM/yyyy");
					}
					else
					{
						txtActualBPD.Text = "";
					}
					if(Utility.IsNotDBNull(InvDS.Tables[0].Rows[0]["promised_payment_date"]))
					{
						txtPromisedPaymentDate.Text = Convert.ToDateTime(InvDS.Tables[0].Rows[0]["promised_payment_date"]).ToString("dd/MM/yyyy");
					}
					else
					{
						txtPromisedPaymentDate.Text = "";
					}
					//lblLastUpdate.Text = (String)InvDS.Tables[0].Rows[0]["update_by"]+" "+InvDS.Tables[0].Rows[0]["update_date"];
					//lblDispApprove.Text = (String)InvDS.Tables[0].Rows[0]["approved_by"]+" "+System.DateTime.ParseExact((String)InvDS.Tables[0].Rows[0]["approved_date"],"dd/MM/yyyy",null);;

				}
			}
			catch
			{
			}
		}
		private void setInputFormat()
		{
			try
			{
				//currency
				txtAmntPaid.Text = txtAmntPaid.Text.Replace(",","");
				txtAmntPaid.Attributes.Add("onblur","round(this,"+currency_decimal+");validateInput();");
				txtAmntPaid.NumberScale = currency_decimal;
				txtAmntPaid.MaxLength = 8+currency_decimal+1;								
				txtAmntPaid.NumberPrecision = 8+currency_decimal;
				
				txtTotalInvAmnt.Text = txtAmntPaid.Text.Replace(",","");
				//txtTotalInvAmnt.Attributes.Add("onblur","round(this,"+currency_decimal+")");
				txtTotalInvAmnt.NumberScale = currency_decimal;
				txtTotalInvAmnt.MaxLength = 8+currency_decimal+1;								
				txtTotalInvAmnt.NumberPrecision = 8+currency_decimal;
				//txtTotalInvAmnt.Attributes.Remove("onkeypress");


				txtBalanceDue.Text = txtAmntPaid.Text.Replace(",","");
				//txtBalanceDue.Attributes.Add("onblur","round(this,"+currency_decimal+")");
				txtBalanceDue.NumberScale = currency_decimal;
				txtBalanceDue.MaxLength = 8+currency_decimal+1;								
				txtBalanceDue.NumberPrecision = 8+currency_decimal;
				//txtBalanceDue.Attributes.Remove("onkeypress");

				txtCurrAmntPaid.Text = txtAmntPaid.Text.Replace(",","");
				//txtCurrAmntPaid.Attributes.Add("onblur","round(this,"+currency_decimal+")");
				txtCurrAmntPaid.NumberScale = currency_decimal;
				txtCurrAmntPaid.MaxLength = 8+currency_decimal+1;								
				txtCurrAmntPaid.NumberPrecision = 8+currency_decimal;
				//end currency

			}
			catch
			{
			}
		}
		private void setInputMode(String strMode)
		{
			switch (strMode)
			{
				case "A" : // approve
					txtActualBPD.ReadOnly=false;
					txtAmntPaid.ReadOnly=true;
					txtAmntPaid.Attributes.Remove("onblur");
					txtBalanceDue.ReadOnly=true;
					txtBatchNo.ReadOnly=true;
					txtCurrAmntPaid.ReadOnly=true;
					txtCustID.ReadOnly=true;
					txtInternalDueDate.ReadOnly=false;
					txtInvNo.ReadOnly=true;
					txtPaidDate.ReadOnly=true;
					txtPrintedDueDate.ReadOnly=true;
					txtPromisedPaymentDate.ReadOnly=false;
					txtTotalInvAmnt.ReadOnly=true;
					btnOK.Enabled=true;
					break;				
				case "L" : // placed
					txtActualBPD.ReadOnly=true;
					txtAmntPaid.ReadOnly=false;
					txtBalanceDue.ReadOnly=true;
					txtBatchNo.ReadOnly=false;
					txtCurrAmntPaid.ReadOnly=true;
					txtCustID.ReadOnly=true;
					txtInternalDueDate.ReadOnly=true;
					txtInvNo.ReadOnly=true;
					txtPaidDate.ReadOnly=false;
					txtPrintedDueDate.ReadOnly=true;
					txtPromisedPaymentDate.ReadOnly=false;
					txtTotalInvAmnt.ReadOnly=true;
					btnOK.Enabled=true;
					break;
				case "N" : // not yet approve
					txtActualBPD.ReadOnly=true;
					txtAmntPaid.ReadOnly=true;
					txtAmntPaid.Attributes.Remove("onblur");
					txtBalanceDue.ReadOnly=true;
					txtBatchNo.ReadOnly=true;
					txtCurrAmntPaid.ReadOnly=true;
					txtCustID.ReadOnly=true;
					txtInternalDueDate.ReadOnly=false;
					txtInvNo.ReadOnly=true;
					txtPaidDate.ReadOnly=true;
					txtPrintedDueDate.ReadOnly=true;
					txtPromisedPaymentDate.ReadOnly=true;
					txtTotalInvAmnt.ReadOnly=true;
					btnOK.Enabled=true;
					break;
				default :
					txtActualBPD.ReadOnly=true;
					txtAmntPaid.ReadOnly=true;
					txtAmntPaid.Attributes.Remove("onblur");
					txtBalanceDue.ReadOnly=true;
					txtBatchNo.ReadOnly=true;
					txtCurrAmntPaid.ReadOnly=true;
					txtCustID.ReadOnly=true;
					txtInternalDueDate.ReadOnly=true;
					txtInvNo.ReadOnly=true;
					txtPaidDate.ReadOnly=true;
					txtPrintedDueDate.ReadOnly=true;
					txtPromisedPaymentDate.ReadOnly=true;
					txtTotalInvAmnt.ReadOnly=true;
					btnOK.Enabled=false;
					break;
			}
		}
		private void setDGAssociatedCNDN()
		{
			try
			{
				dsASCNDN = InvoicePaymentUpdateMgrDAL.getAssociatedCNDN(appID,entID,InvNo,culture);
				dgAssociatedCNDN.DataSource = dsASCNDN;
				Session["dsASCNDN"] = dsASCNDN;
				dgAssociatedCNDN.DataBind();
			}
			catch
			{
			}
		}

		private void dgAssociatedCNDN_PageIndexChanged(object source, System.Web.UI.WebControls.DataGridPageChangedEventArgs e)
		{
			try
			{
				dgAssociatedCNDN.CurrentPageIndex = e.NewPageIndex;
				dgAssociatedCNDN.DataSource = dsASCNDN;
				dgAssociatedCNDN.DataBind();
			}
			catch
			{
			}
		}

		private void dgAssociatedCNDN_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
		{
			int page,index,pagesize,page_index=0;
			page = dgAssociatedCNDN.CurrentPageIndex;
			index = e.Item.ItemIndex;
			pagesize = dgAssociatedCNDN.PageSize;
			page_index = index+(page*pagesize);
			if(e.Item.ItemIndex>-1)
			{
				e.Item.Cells[3].Text = String.Format(strFormatCurrency,dsASCNDN.Tables[0].Rows[page_index]["total_amnt"]);
			}
		}

		private void btnOK_Click(object sender, System.EventArgs e)
		{
			ErrMsg.Text = "";
			try
			{
				if(validateForm())
				{
					DataSet InvDS = (DataSet)Session["InvDS"];
					Decimal dCAmntPaid=0;
					
					if(Session["dCAmntPaid"]!=null)
					{
						dCAmntPaid = (Decimal)Session["dCAmntPaid"];
					}
					Decimal dDN = CreditDebitMgrDAL.getTotalDebitNote(appID,entID,InvDS.Tables[0].Rows[0]["invoice_no"].ToString().ToUpper());
					Decimal dCN = CreditDebitMgrDAL.getTotalCreditNote(appID,entID,InvDS.Tables[0].Rows[0]["invoice_no"].ToString().ToUpper());
					Decimal dInvAmnt = 0;
					if(Utility.IsNotDBNull(InvDS.Tables[0].Rows[0]["invoice_amt"]))
					{
						dInvAmnt = Convert.ToDecimal(InvDS.Tables[0].Rows[0]["invoice_amt"]);
					}
						
					String Stat = InvDS.Tables[0].Rows[0]["invoice_status"].ToString().Trim().ToUpper();
					Decimal dBalanceDue = 0;					
					DataTable dtParam = new DataTable();
					dtParam.Columns.Add("invoice_no");//
					dtParam.Columns.Add("invoice_status");//
					dtParam.Columns.Add("amount_paid");//
					dtParam.Columns.Add("batch_no");//
					dtParam.Columns.Add("user");//
					dtParam.Columns.Add("promised_payment_date");//
					dtParam.Columns.Add("paid_date");//
					dtParam.Columns.Add("internal_due_date");
					dtParam.Columns.Add("actual_bill_placement_date");
					DataRow drData = dtParam.NewRow();

					drData["invoice_no"]=InvDS.Tables[0].Rows[0]["invoice_no"].ToString().ToUpper();
					drData["user"]=utility.GetUserID();
					if(txtBatchNo.Text.Trim()!="")
					{
						drData["batch_no"]=txtBatchNo.Text.Trim();
					}
					else
					{
						drData["batch_no"]=DBNull.Value;
					}
					if(txtInternalDueDate.Text.Trim()!="")
					{
						drData["internal_due_date"]=DateTime.ParseExact(txtInternalDueDate.Text.Trim(),"dd/MM/yyyy",null);
					}
					else
					{
						drData["internal_due_date"]=DBNull.Value;
					}
					if(txtActualBPD.Text.Trim()!="")
					{
						drData["actual_bill_placement_date"]=DateTime.ParseExact(txtActualBPD.Text.Trim(),"dd/MM/yyyy",null);
					}
					else
					{
						drData["actual_bill_placement_date"]=DBNull.Value;
					}

					if(txtAmntPaid.Text.Trim()!="")
					{
						drData["amount_paid"]=Convert.ToDecimal(txtAmntPaid.Text.Trim());
						dBalanceDue = dInvAmnt+dDN-(dCN+dCAmntPaid+Convert.ToDecimal(txtAmntPaid.Text.Trim()));
					}
					else
					{
						drData["amount_paid"]=DBNull.Value;
						dBalanceDue = dInvAmnt+dDN-(dCN+dCAmntPaid);
					}
					if(Stat =="L")
					{
						int day=0;
						if(txtPaidDate.Text!="")
						{
							DateTime dtPayment = DateTime.ParseExact(txtPaidDate.Text.Trim(),"dd/MM/yyyy",System.Globalization.CultureInfo.CurrentUICulture);
							DateTime dtToday = DateTime.Now;
							TimeSpan span = dtPayment-dtToday;
							day = (int)span.TotalDays;	
						}
						if(day>0)
						{							
							drData["invoice_status"]=Stat;														
						}
						else
						{
							if(dBalanceDue>0)
							{
								drData["invoice_status"]=Stat;
							
							}
							else
							{
								drData["invoice_status"]="D";
							}
						}
						
					}
					else if(Stat=="A")
					{
						if(txtActualBPD.Text.Trim()!="")
						{
							drData["invoice_status"]="L";
						}
						else
						{
							drData["invoice_status"]=Stat;
						}
					}
					else
					{
						drData["invoice_status"]=Stat;
					}					

					if(txtPromisedPaymentDate.Text.Trim()!="")
					{
						drData["promised_payment_date"]=DateTime.ParseExact(txtPromisedPaymentDate.Text.Trim(),"dd/MM/yyyy",null);
					}
					else
					{
						drData["promised_payment_date"]=DBNull.Value;
					}

					if(txtPaidDate.Text.Trim()!="")
					{
						drData["paid_date"]=DateTime.ParseExact(txtPaidDate.Text.Trim(),"dd/MM/yyyy",null);
					}
					else
					{
						drData["paid_date"]=DBNull.Value;
					}
					dtParam.Rows.Add(drData);
					int pass = InvoicePaymentUpdateMgrDAL.updateInvoice(appID,entID,dtParam);
					if(pass==0)
					{
						txtAmntPaid.Text = "";
						getInvoiceDetail();
						DisplayCurrentPage();
						setDGAssociatedCNDN();
						ErrMsg.Text=Utility.GetLanguageText(ResourceType.UserMessage,"MSG_SAVESUCCESS",utility.GetUserCulture());
						Response.Write("<script language=javascript>window.close();</script>");

					}

				
				}
			}
			catch
			{
			}

		}

		private bool validateForm()
		{
			bool pass = true;
			try
			{
				DataSet InvDS = (DataSet)Session["InvDS"];
				DateTime actBPD,internalDD,promisedPD;
				DateTime dbActBPD,dbInternalDD,dbNextBPD;
				switch (InvDS.Tables[0].Rows[0]["invoice_status"].ToString().ToUpper())
				{
					case "A" : // approve
						if(txtInternalDueDate.Text.Trim()!="")
						{
							internalDD = DateTime.ParseExact(txtInternalDueDate.Text.Trim(),"dd/MM/yyyy",null);
							if(Utility.IsNotDBNull(InvDS.Tables[0].Rows[0]["internal_due_date"]))
							{
								dbInternalDD = Convert.ToDateTime(InvDS.Tables[0].Rows[0]["internal_due_date"]);
								if(internalDD.Date != dbInternalDD.Date)//internal DD is update
								{
									if(txtActualBPD.Text.Trim()!="")
									{
										ErrMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"MSG_IPU_INDD_NOT_CHANGE",utility.GetUserCulture());
										return false;
									}
									
								}
							}
							/*else
							{
								ErrMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"MSG_IPU_NULL_FIELD",utility.GetUserCulture()).Replace("[field]","internal_due_date");								
								return false;
							}*/
							if(Utility.IsNotDBNull(InvDS.Tables[0].Rows[0]["next_bill_placement_date"]))
							{
								dbNextBPD = Convert.ToDateTime(InvDS.Tables[0].Rows[0]["next_bill_placement_date"]);										
								if(dbNextBPD.Date > internalDD.Date)
								{
									ErrMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"MSG_IPU_DD_ELIER_NEXTBPD",utility.GetUserCulture());
									return false;
								}
							}
							else
							{
								ErrMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"MSG_IPU_NULL_FIELD",utility.GetUserCulture()).Replace("[field]","next_bill_placement_date");
								return false;
							}
						}
						if(txtActualBPD.Text.Trim()!="")
						{
							actBPD = DateTime.ParseExact(txtActualBPD.Text.Trim(),"dd/MM/yyyy",null);
							if(Utility.IsNotDBNull(InvDS.Tables[0].Rows[0]["actual_bill_placement_date"]))
							{
								dbActBPD = Convert.ToDateTime(InvDS.Tables[0].Rows[0]["actual_bill_placement_date"]);
								if(actBPD.Date != dbActBPD.Date)//actual BPD is update
								{
									if(Utility.IsNotDBNull(InvDS.Tables[0].Rows[0]["internal_due_date"]))
									{
										dbInternalDD = Convert.ToDateTime(InvDS.Tables[0].Rows[0]["internal_due_date"]);										
										if(dbInternalDD.Date < actBPD.Date)
										{
											ErrMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"MSG_IPU_ACTBPD_NOT_LATER_INDD",utility.GetUserCulture());
											return false;
										}
									}
									else
									{
										ErrMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"MSG_IPU_NULL_FIELD",utility.GetUserCulture()).Replace("[field]","internal_due_date");										
										return false;
									}
									if(Utility.IsNotDBNull(InvDS.Tables[0].Rows[0]["next_bill_placement_date"]))
									{
//										dbNextBPD = Convert.ToDateTime(InvDS.Tables[0].Rows[0]["next_bill_placement_date"]);										
//										if(dbNextBPD>actBPD)
//										{
//											ErrMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"MSG_IPU_ACTBPD_NOT_ELIER_NEXTBPD",utility.GetUserCulture());
//											return false;
//										}
									}
									else
									{
										ErrMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"MSG_IPU_NULL_FIELD",utility.GetUserCulture()).Replace("[field]","next_bill_placement_date");
										return false;
									}
								}
							}
							else // dbActualBPD null
							{
								if(Utility.IsNotDBNull(InvDS.Tables[0].Rows[0]["internal_due_date"]))
								{
									dbInternalDD = Convert.ToDateTime(InvDS.Tables[0].Rows[0]["internal_due_date"]);										
									if(dbInternalDD.Date < actBPD.Date)
									{
										ErrMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"MSG_IPU_ACTBPD_NOT_LATER_INDD",utility.GetUserCulture());
										return false;
									}
								}
								else
								{
									ErrMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"MSG_IPU_NULL_FIELD",utility.GetUserCulture()).Replace("[field]","internal_due_date");
									return false;
								}
								if(Utility.IsNotDBNull(InvDS.Tables[0].Rows[0]["next_bill_placement_date"]))
								{
//									dbNextBPD = Convert.ToDateTime(InvDS.Tables[0].Rows[0]["next_bill_placement_date"]);										
//									if(dbNextBPD>actBPD)
//									{
//										ErrMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"MSG_IPU_ACTBPD_NOT_ELIER_NEXTBPD",utility.GetUserCulture());
//										return false;
//									}
								}
								else
								{
									ErrMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"MSG_IPU_NULL_FIELD",utility.GetUserCulture()).Replace("[field]","next_bill_placement_date");
									return false;
								}							
							}
							if(txtPromisedPaymentDate.Text.Trim()!="")
							{
								promisedPD = DateTime.ParseExact(txtPromisedPaymentDate.Text.Trim(),"dd/MM/yyyy",null);
								if(actBPD.Date > promisedPD.Date)
								{
									ErrMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"MSG_IPU_PPD_NOT_ELIER_ACTBPD",utility.GetUserCulture());
									return false;
								}
							}
						}
						else
						{
							if(txtPromisedPaymentDate.Text.Trim()!="")
							{
								ErrMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"MSG_IPU_PPD_NOT_INPUT",utility.GetUserCulture());
								return false;
							}
						}
						break;				
					case "L" : // placed
						if(txtAmntPaid.Text.Trim()!="")
						{
							Decimal dCAmntPaid = 0;
							Decimal dAmntPaid = 0;
							Decimal dBalanceDue = 0;
							if(Session["dCAmntPaid"]!=null)
							{
								dCAmntPaid = (Decimal)Session["dCAmntPaid"];
							}
							if(Session["dBalanceDue"]!=null)
							{
								dBalanceDue = (Decimal)Session["dBalanceDue"];
							}
							if(txtAmntPaid.Text.Trim()!="")
							{
								dAmntPaid = Convert.ToDecimal(txtAmntPaid.Text.Trim());
								if(dAmntPaid<0)
								{
									if(Math.Abs(dAmntPaid)>dCAmntPaid)
									{
										ErrMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"MSG_IPU_NEGATIVE_AMNT_EXCEEDS_PAID",utility.GetUserCulture());
										return false;
									}
								}
								if(dAmntPaid!=0)
								{
									if(dAmntPaid>dBalanceDue)
									{
										ErrMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"MSG_IPU_PAY_AMNT_EXCEEDS_BALDUE",utility.GetUserCulture());
										return false;
									}
									if(txtPaidDate.Text.Trim()!="")
									{
										DateTime dtPayment = DateTime.ParseExact(txtPaidDate.Text.Trim(),"dd/MM/yyyy",System.Globalization.CultureInfo.CurrentUICulture);
										DateTime dtToday = DateTime.Now;
										TimeSpan span;
										if(Utility.IsNotDBNull(InvDS.Tables[0].Rows[0]["actual_bill_placement_date"]))
										{
											dbActBPD = Convert.ToDateTime(InvDS.Tables[0].Rows[0]["actual_bill_placement_date"]);
										}
										else
										{
											ErrMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"MSG_IPU_NULL_FIELD",utility.GetUserCulture()).Replace("[field]","actual_bill_placement_date");
											return false;
										}
										if(dtPayment.Day!=dtToday.Day ||dtPayment.Month!=dtToday.Month||dtPayment.Year!=dtToday.Year )
										{										
											span = dtPayment-dtToday;
											int day = (int)span.TotalDays;										
											if(day>90)
											{
												ErrMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"MSG_IPU_F_PAYMENT_MUST_IN_90_DAY",utility.GetUserCulture());
												return false;
											}
											if(dAmntPaid<0)
											{
												ErrMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"MSG_IPU_F_PAYMENT_MUST_NOT_NEGATIVE",utility.GetUserCulture());
												return false;
											}
											span = dtPayment-dbActBPD;
											day = (int)span.TotalDays;
											if(day<0)
											{
												ErrMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"MSG_IPU_PAY_DATE_ELIER_PL_DATE",utility.GetUserCulture());
												return false;
											}


										}
									}
									else
									{
										ErrMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"MSG_IPU_PAY_DATE_REQ",utility.GetUserCulture());
										return false;
									}
								}
							}														
						}
						if(txtActualBPD.Text.Trim()=="")
						{
							if(txtPromisedPaymentDate.Text.Trim()!="")
							{
								ErrMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"MSG_IPU_PPD_NOT_INPUT",utility.GetUserCulture());
								return false;
							}
						}
						break;
					case "N" : // not yet approve
						if(txtInternalDueDate.Text.Trim()!="")
						{
							internalDD = DateTime.ParseExact(txtInternalDueDate.Text.Trim(),"dd/MM/yyyy",null);
							if(Utility.IsNotDBNull(InvDS.Tables[0].Rows[0]["internal_due_date"]))
							{
								dbInternalDD = Convert.ToDateTime(InvDS.Tables[0].Rows[0]["internal_due_date"]);
								if(internalDD.Date != dbInternalDD.Date)//internal DD is update
								{
									if(Utility.IsNotDBNull(InvDS.Tables[0].Rows[0]["next_bill_placement_date"]))
									{
										dbNextBPD = Convert.ToDateTime(InvDS.Tables[0].Rows[0]["next_bill_placement_date"]);										
										if(dbNextBPD.Date > internalDD.Date)
										{
											ErrMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"MSG_IPU_DD_NOT_ELIER_NEXTBPD",utility.GetUserCulture());
											return false;
										}
									}									
								}
							}
							else
							{
								if(Utility.IsNotDBNull(InvDS.Tables[0].Rows[0]["next_bill_placement_date"]))
								{
									dbNextBPD = Convert.ToDateTime(InvDS.Tables[0].Rows[0]["next_bill_placement_date"]);										
									if(dbNextBPD.Date > internalDD.Date)
									{
										ErrMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"MSG_IPU_DD_NOT_ELIER_NEXTBPD",utility.GetUserCulture());
										return false;
									}
								}
//								ErrMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"MSG_IPU_NULL_FIELD",utility.GetUserCulture()).Replace("[field]","internal_due_date");
//								return false;
							}
							
						}
						break;
					default :

						break;
				}
			}
			catch (Exception e)
			{
				Console.WriteLine(e.Message);
				pass = false;
			}
			return pass;
		}
	}
}
