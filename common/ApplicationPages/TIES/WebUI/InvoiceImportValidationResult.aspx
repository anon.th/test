<%@ Page language="c#" Codebehind="InvoiceImportValidationResult.aspx.cs" AutoEventWireup="false" Inherits="TIES.WebUI.InvoiceImportValidationResult" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>InvoiceImportValidationResult</title>
		<LINK href="css/Styles.css" type="text/css" rel="stylesheet" name="stylesheet">
		<meta name="GENERATOR" Content="Microsoft Visual Studio 7.0">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<asp:datagrid id="dgImportInvoice" style="Z-INDEX: 101; LEFT: 21px; POSITION: absolute; TOP: 66px"
				runat="server" AllowPaging="True" AutoGenerateColumns="False" BorderColor="#CCCCCC" BackColor="White"
				CellPadding="3" BorderWidth="1px" BorderStyle="None" AllowCustomPaging="True" Width="617px"
				CssClass="gridHeading">
				<FooterStyle ForeColor="#000066" BackColor="White"></FooterStyle>
				<SelectedItemStyle Font-Bold="True" ForeColor="White" CssClass="drilldownFieldSelected"></SelectedItemStyle>
				<ItemStyle CssClass="drilldownField"></ItemStyle>
				<HeaderStyle Width="10%" CssClass="drilldownHeading"></HeaderStyle>
				<Columns>
					<asp:TemplateColumn HeaderText="Invoice No.">
						<HeaderStyle Wrap="False" Width="100px" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle Wrap="False" HorizontalAlign="Left"></ItemStyle>
						<ItemTemplate>
							<asp:Label CssClass="gridLabel" ID="dgInvoiceNo" Text='<%#DataBinder.Eval(Container.DataItem,"Invoice_Number")%>' Runat="server">
							</asp:Label>
						</ItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Batch Number">
						<HeaderStyle Wrap="False" Width="100px" CssClass="gridHeading" HorizontalAlign="Center"></HeaderStyle>
						<ItemStyle Wrap="False" HorizontalAlign="Center"></ItemStyle>
						<ItemTemplate>
							<asp:Label CssClass="gridLabel" ID="dgBatchNo" Text='<%#DataBinder.Eval(Container.DataItem,"Batch_No")%>' Runat="server">
							</asp:Label>
						</ItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Pay Date">
						<HeaderStyle Wrap="False" Width="100px" CssClass="gridHeading" HorizontalAlign="Center"></HeaderStyle>
						<ItemStyle Wrap="False" HorizontalAlign="Center"></ItemStyle>
						<ItemTemplate>
							<asp:Label CssClass="gridLabel" ID="dgPayDate" Text='<%#DataBinder.Eval(Container.DataItem,"Payment_Date")%>' Runat="server">
							</asp:Label>
						</ItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Pay Amount">
						<HeaderStyle Wrap="False" Width="100px" CssClass="gridHeading" HorizontalAlign="Center"></HeaderStyle>
						<ItemStyle Wrap="False" HorizontalAlign="Right"></ItemStyle>
						<ItemTemplate>
							<asp:Label CssClass="gridLabelNumber" ID="dgPayAmount" Text='<%#DataBinder.Eval(Container.DataItem,"Amount_Paid")%>' Runat="server">
							</asp:Label>
						</ItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Error">
						<HeaderStyle Wrap="False" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle Wrap="False" HorizontalAlign="Left"></ItemStyle>
						<ItemTemplate>
							<asp:Label CssClass="gridLabel" ID="dgError" Text='<%#DataBinder.Eval(Container.DataItem,"error")%>' Runat="server">
							</asp:Label>
						</ItemTemplate>
					</asp:TemplateColumn>
				</Columns>
				<PagerStyle NextPageText="Next" PrevPageText="Previous" HorizontalAlign="Left" ForeColor="#000066"
					BackColor="White" CssClass="normalText"></PagerStyle>
			</asp:datagrid>
			<asp:label id="lblTitle" style="Z-INDEX: 102; LEFT: 24px; POSITION: absolute; TOP: 23px" runat="server"
				Width="477px" CssClass="mainTitleSize" Height="26px">Import Validation Results</asp:label>
		</form>
	</body>
</HTML>
