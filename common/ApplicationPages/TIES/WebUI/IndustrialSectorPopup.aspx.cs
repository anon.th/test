using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.common.util;
using com.common.classes;
using com.common.DAL;
using com.ties.classes;   
using System.Text;
using com.common.applicationpages;
namespace com.ties
{
	/// <summary>
	/// Summary description for IndustrialSectorPopup.
	/// </summary>
	public class IndustrialSectorPopup : BasePopupPage
	{
		protected System.Web.UI.WebControls.Button btnSearch;
		protected System.Web.UI.WebControls.Button btnOk;
		//Utility utility = null;
		private String strIndustrialSectorClientID = null;
		protected System.Web.UI.WebControls.DataGrid dgIndustrialSector;
		protected System.Web.UI.WebControls.TextBox txtIndustrialSector;

		SessionDS m_sdsIndustrialSector = null;

		String strAppID = "";
		String strEnterpriseID = "";
		String strFormID = "";
		
		String strIndustrialSector = null;
		//String strState = null;
		protected System.Web.UI.WebControls.Label lblVASCode;
		//String strCountry = null;
		

		private void Page_Load(object sender, System.EventArgs e)
		{
		//	utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings,Page.Session);
			strAppID = utility.GetAppID();
			strEnterpriseID = utility.GetEnterpriseID();

			strIndustrialSectorClientID = Request.Params["ISECTOR_CID"];
			strFormID = Request.Params["FORMID"];
		

			if(!Page.IsPostBack)
			{
				txtIndustrialSector.Text = Request.Params["ISECTOR"];
				m_sdsIndustrialSector = GetEmptyIndustrialSectorDS(0);
				BindGrid();
			}
			else
			{
				m_sdsIndustrialSector = (SessionDS)ViewState["ZIPCODE_DS"];

			}


		}

		public void Paging(Object sender, DataGridPageChangedEventArgs e)
		{
			dgIndustrialSector.CurrentPageIndex = e.NewPageIndex;
			ShowCurrentPage();
		}
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			base.OnInit(e);
			InitializeComponent();
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.dgIndustrialSector.SelectedIndexChanged += new System.EventHandler(this.dgIndustrialSector_SelectedIndexChanged);
			this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
			this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		public static SessionDS GetEmptyIndustrialSectorDS(int iNumRows)
		{
			

			DataTable dtIndustrialSector = new DataTable();
 
			dtIndustrialSector.Columns.Add(new DataColumn("industrial_sector_code", typeof(string)));
			dtIndustrialSector.Columns.Add(new DataColumn("industrial_sector_description", typeof(string)));




			for(int i=0; i < iNumRows; i++)
			{
				DataRow drEach = dtIndustrialSector.NewRow();
				drEach[0] = "";
				drEach[1] = "";


				dtIndustrialSector.Rows.Add(drEach);
			}

			DataSet dsIndustrialSector = new DataSet();
			dsIndustrialSector.Tables.Add(dtIndustrialSector);

			SessionDS sessionDS = new SessionDS();
			sessionDS.ds = dsIndustrialSector;
			sessionDS.DataSetRecSize = dsIndustrialSector.Tables[0].Rows.Count;
			sessionDS.QueryResultMaxSize = sessionDS.DataSetRecSize;


			return  sessionDS;
	
		}

		private SessionDS GetIndustrialSectorDS(int iCurrent, int iDSRecSize, String strQuery)
		{

			SessionDS sessionDS = null;


			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("IndustrialSectorPopup.aspx.cs","GetIndustrialSectorDS","EDB101","DbConnection object is null!!");
				return sessionDS;
			}

			
			sessionDS = dbCon.ExecuteQuery(strQuery,iCurrent,iDSRecSize,"IndustrialSectorStateCountry");

			return  sessionDS;
			
		}

		private void BindGrid()
		{
			dgIndustrialSector.VirtualItemCount = System.Convert.ToInt32(m_sdsIndustrialSector.QueryResultMaxSize);
			dgIndustrialSector.DataSource = m_sdsIndustrialSector.ds;
			dgIndustrialSector.DataBind();
			ViewState["ZIPCODE_DS"] = m_sdsIndustrialSector;
		}

		private void ShowCurrentPage()
		{
			int iStartIndex = dgIndustrialSector.CurrentPageIndex * dgIndustrialSector.PageSize;
			String sqlQuery = (String)ViewState["SQL_QUERY"];
			m_sdsIndustrialSector = GetIndustrialSectorDS(iStartIndex,dgIndustrialSector.PageSize,sqlQuery);
			decimal pgCnt = (m_sdsIndustrialSector.QueryResultMaxSize - 1)/dgIndustrialSector.PageSize;
			if(pgCnt < dgIndustrialSector.CurrentPageIndex)
			{
				dgIndustrialSector.CurrentPageIndex = System.Convert.ToInt32(pgCnt);
			}
			dgIndustrialSector.SelectedIndex = -1;
			dgIndustrialSector.EditItemIndex = -1;

			BindGrid();
			getPageControls(Page);
		}

		private void btnSearch_Click(object sender, System.EventArgs e)
		{

			String strISector = txtIndustrialSector.Text;

			StringBuilder strQry = new StringBuilder();
			strQry.Append("select industrial_sector_code,industrial_sector_description from Industrial_Sector ");

			if(strISector != null && strISector != "")
			{
				strQry.Append(" where industrial_sector_code like '%");
				strQry.Append(Utility.ReplaceSingleQuote(strISector));
				strQry.Append("%' ");
				strQry.Append(" and applicationid = '" + utility.GetAppID()+ "'");
				strQry.Append(" and enterpriseid = '" + utility.GetEnterpriseID() + "'");
			}
			else
			{
				strQry.Append(" where applicationid = '" + utility.GetAppID()+ "'");
				strQry.Append(" and enterpriseid = '" + utility.GetEnterpriseID() + "'");
			}

			String strSQLQuery = strQry.ToString();

			ViewState["SQL_QUERY"] = strSQLQuery;
			dgIndustrialSector.CurrentPageIndex = 0;

			ShowCurrentPage();

		}

		private void btnOk_Click(object sender, System.EventArgs e)
		{
			// Get the value and return it to the parent window
			String sScript = "";
			sScript += "<script language=javascript>";
			sScript += "  window.close();";
			sScript += "</script>";
			Response.Write(sScript);
		}

		private void dgIndustrialSector_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			// Get the value and return it to the parent window
			
			int iSelIndex = dgIndustrialSector.SelectedIndex;
			DataGridItem dgRow = dgIndustrialSector.Items[iSelIndex];
			strIndustrialSector = dgRow.Cells[0].Text;

			String sScript = "";
			sScript += "<script language=javascript>";
			sScript += "  window.opener."+strFormID+"."+strIndustrialSectorClientID+".value = '"+strIndustrialSector+"';" ;
			sScript += "  window.close();";
			sScript += "</script>";
			Response.Write(sScript);


		}
	}
}
