<%@ Page language="c#" Codebehind="CustomerPopup.aspx.cs" AutoEventWireup="false" Inherits="TIES.WebUI.CustomerDetails" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>CustomerPopup</title>
		<LINK rel="stylesheet" type="text/css" href="css/Styles.css" name="stylesheet">
		<meta name="GENERATOR" content="Microsoft Visual Studio 7.0">
		<meta name="CODE_LANGUAGE" content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<script language="javascript" src="Scripts/settingScrollPosition.js"></script>
				<script language="javascript">	
			function UpperMaskSpecialWithHyphen(toField)
			{
				var lcNewChar = String.fromCharCode(window.event.keyCode);
				var llRetVal = ((lcNewChar >= '0' && lcNewChar <= '9')||(lcNewChar >= 'A' && lcNewChar <= 'Z') || (lcNewChar >= 'a' && lcNewChar <= 'z') || (lcNewChar == '-'));
				window.event.keyCode = lcNewChar.toUpperCase().charCodeAt(0);
				return llRetVal;
			}
		</script>
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="CustomerDetails" method="post" runat="server">
			<div style="TEXT-ALIGN: center">
				<table border="0" cellSpacing="0" cellPadding="0" width="100%">
					<tr>
						<td width="120"><asp:label id="lblCustID" runat="server" Height="16" CssClass="tableLabel" Width="100px">Customer ID</asp:label></td>
						<td width="120"><asp:label id="lblCustName" runat="server" Height="16" CssClass="tableLabel" Width="108px">Customer Name</asp:label></td>
						<td width="120"><asp:label id="lblZipcode" runat="server" Height="16" CssClass="tableLabel" Width="108px">Zip Code</asp:label></td>
						<td><asp:button style="DISPLAY: none" id="btnRefresh" runat="server" Height="23px" Width="50px"
								Text="HIDDEN"></asp:button></td>
					</tr>
					<tr>
						<td><asp:textbox id="txtCustID" runat="server" CssClass="textField" Width="116px"></asp:textbox></td>
						<td><asp:textbox id="txtCustName" runat="server" CssClass="textField" Width="116px"></asp:textbox></td>
						<td><asp:textbox id="txtZipCode" runat="server" CssClass="textField" Width="116px"></asp:textbox></td>
						<td align="right">
							<asp:button id="btnSearch" runat="server" Height="21" CssClass="buttonProp" Width="85" Text="Search"
								CausesValidation="False"></asp:button>
							<FONT face="Tahoma">&nbsp;</FONT>
							<asp:button id="btnClose" runat="server" Height="21" CssClass="buttonProp" Width="85" Text="Close"
								CausesValidation="False"></asp:button></td>
					</tr>
					<tr>
						<td colSpan="4"><asp:label id="CustErrMsg" runat="server" Height="16px" CssClass="errorMsgColor" Width="598px"></asp:label></td>
					</tr>
					<tr>
						<td colSpan="4"><asp:datagrid id="dgCustomer" runat="server" CssClass="gridHeading" Width="100%" BorderStyle="None"
								BorderWidth="1px" CellPadding="3" BackColor="White" BorderColor="#CCCCCC" AutoGenerateColumns="False"
								AllowPaging="True" PageSize="10" OnPageIndexChanged="Paging" AllowCustomPaging="True">
								<SelectedItemStyle Font-Bold="True" ForeColor="White" BackColor="#669999"></SelectedItemStyle>
								<ItemStyle CssClass="drilldownField"></ItemStyle>
								<HeaderStyle CssClass="gridHeading"></HeaderStyle>
								<FooterStyle ForeColor="#000066" BackColor="White"></FooterStyle>
								<Columns>
									<asp:BoundColumn DataField="custid" HeaderText="Customer ID"></asp:BoundColumn>
									<asp:BoundColumn DataField="cust_name" HeaderText="Name"></asp:BoundColumn>
									<asp:BoundColumn DataField="address1" HeaderText="Address 1"></asp:BoundColumn>
									<asp:BoundColumn DataField="address2" HeaderText="Address 2"></asp:BoundColumn>
									<asp:BoundColumn DataField="zipcode" HeaderText="Postal Code"></asp:BoundColumn>
									<asp:BoundColumn DataField="country" HeaderText="Country"></asp:BoundColumn>
									<asp:BoundColumn Visible="False" DataField="pod_slip_required" HeaderText="pod_slip_required"></asp:BoundColumn>
									<asp:BoundColumn Visible="False" DataField="apply_esa_surcharge" HeaderText="ESA"></asp:BoundColumn>
									<asp:BoundColumn DataField="payment_mode" HeaderText="Payment Mode"></asp:BoundColumn>
									<asp:BoundColumn Visible="False" DataField="telephone" HeaderText="telephone"></asp:BoundColumn>
									<asp:BoundColumn Visible="False" DataField="fax" HeaderText="fax"></asp:BoundColumn>
									<asp:BoundColumn Visible="False" DataField="contact_person"></asp:BoundColumn>
									<asp:BoundColumn Visible="False" DataField="salesmanid"></asp:BoundColumn>
									<asp:ButtonColumn Text="Select" CommandName="Select"></asp:ButtonColumn>
								</Columns>
								<PagerStyle HorizontalAlign="Left" ForeColor="#000066" BackColor="White" Mode="NumericPages"></PagerStyle>
							</asp:datagrid></td>
					</tr>
				</table>
			</div>
		</form>
	</body>
</HTML>
