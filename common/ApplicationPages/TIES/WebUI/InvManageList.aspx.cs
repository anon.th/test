using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.common.util;
using com.ties.DAL;
using com.ties.BAL;
using com.ties.classes;
using com.common.classes;
using com.common.applicationpages;
using System.Text;

namespace com.ties
{
	/// <summary>
	/// Summary description for InvoiceGenerationPreview.
	/// </summary>
	public class InvManageList : BasePage  
	{

		private string m_strAppID, m_strEnterpriseID;
		protected System.Web.UI.WebControls.Label lblMainTitle;
		protected System.Web.UI.WebControls.DataGrid dgPreview;
		protected System.Web.UI.WebControls.Button btnSelectAll;
		protected System.Web.UI.WebControls.Button btnCancel;
		protected System.Web.UI.WebControls.Button btnApproveSelected;
		protected System.Web.UI.WebControls.Button btnCancelSelected;
		protected System.Web.UI.WebControls.Button btnPrintHeader;
		protected System.Web.UI.WebControls.Label Label1;
		protected System.Web.UI.WebControls.Label lblShowStatust;
		protected System.Web.UI.WebControls.Button btnPrintDetail;

		private DataSet dsInvoiceQuery;
		private DataSet m_sdsInvoiceQuery = null;
		//DataSet dsInvoice = new DataSet();

	
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			Utility ut = new Utility(System.Configuration.ConfigurationSettings.AppSettings,Page.Session);
			
			m_strAppID = ut.GetAppID();
            m_strEnterpriseID = ut.GetEnterpriseID();

			if(!Page.IsPostBack)
			{
				GetRecSet();			    
			}
				
		}

		private void GetRecSet()
		{
//			dsInvoiceQuery = (DataSet)Session["dsPreviewQUERY"];
//
//			DataRow drInvoiceNo = dsInvoiceQuery.Tables[0].Rows[0];
//			StringBuilder strBuild;
//			strBuild = new StringBuilder();
//			strBuild.Append("Update Invoice Set ");
//			strBuild.Append("invoice_no = ");
//
//			if((drInvoiceNo["invoice_no"]!= null) && (!drInvoiceNo["invoice_no"].GetType().Equals(System.Type.GetType("System.DBNull"))))
//			{
//				string strinvoice_no = (string) drInvoiceNo["invoice_no"].ToString();
//				strBuild.Append("'");
//				strBuild.Append(Utility.ReplaceSingleQuote(strinvoice_no));
//				strBuild.Append("'");
//			}
//			else
//			{
//				strBuild.Append("null");
//			}
//
//			DateTime dtdrInvdate_from = DateTime.Parse(drInvoiceNo["invdate_from"].ToString());
//
//			DateTime dtdrInvdate_to = DateTime.Parse(drInvoiceNo["invdate_to"].ToString());
//
//			DateTime dtdrduedate_from = DateTime.Parse(drInvoiceNo["duedate_from"].ToString());
//
//			DateTime dtdrduedate_to = DateTime.Parse(drInvoiceNo["duedate_to"].ToString());
//
//			string strinv_type = (string) drInvoiceNo["invoice_type"].ToString();
//
//			string strcus_id = (string) drInvoiceNo["customer_id"].ToString();
//
//			string strstatus = (string) drInvoiceNo["status"].ToString();
//
//			string strinvoice_no = (string) drInvoiceNo["invoice_no"].ToString();
//
//
//			m_sdsInvoiceQuery  = InvoiceManagementMgrDAL.GetInvoice(m_strAppID, m_strEnterpriseID,dtdrInvdate_from,dtdrInvdate_to,strinvoice_no,dtdrduedate_from,dtdrduedate_to,strinv_type,strcus_id,strstatus);
//			
//			this.dgPreview.DataSource = m_sdsInvoiceQuery;
//			this.dgPreview.DataBind();
		}

		private void btnCancel_Click(object sender, System.EventArgs e)
		{
			Response.Redirect("InvManageSelect.aspx?PARENTMODID=07Account&MODID=12InvManageSel");

		}

		private void movePage()
		{
			Response.Redirect("InvManageSelect.aspx?PARENTMODID=07Account&MODID=12InvManageSel");
		}

	}
}
