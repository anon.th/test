using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.common.classes;
using com.common.util;
using com.ties.DAL;
using com.common.applicationpages;


namespace com.ties
{
	/// <summary>
	/// Summary description for DeliveryManifestList.
	/// </summary>
	public class DeliveryManifestList : BasePopupPage
	{
		protected System.Web.UI.WebControls.Button btnQry;
		protected System.Web.UI.WebControls.Button btnPrint;
		protected System.Web.UI.WebControls.Label lblMainTitle;
		protected System.Web.UI.WebControls.DataGrid dgDMList;
		protected System.Web.UI.WebControls.Label lblErrorMsg;

		//Utility utility=null;
		SessionDS m_sdsDMList = null;
		SessionDS m_sdsConsignment=null;
		DataSet dsQuery=null;
		String deliveryType="L",sPathCode=null, sFlightVehNo=null, sManifestDateTime=null;
		String sDepDateTime=null, sAWBDriverName=null, sLineHaulCost =null;
		/// <summary>
		/// Gathers all checked Path_Code,Flight_Vehicle_No,Delivery_Manifest_DateTime to Delete data from Delivery_Manifest, Delivery_Manifest_Detail
		/// </summary>
		DataSet dsDelete=null;
		DataSet dsPrint=null;
		String appID=null;
		String enterpriseID=null;
		String userID=null;

		/// <summary>
		/// used to TRACK DELETE/NAVIGATION Operation
		/// D - Deletion, N - Navigation
		/// </summary>
		String msgFlag=null;			
		protected System.Web.UI.WebControls.Button btnDelete;
		protected System.Web.UI.WebControls.DataGrid dgConsignment;
		protected System.Web.UI.HtmlControls.HtmlGenericControl gridPanel;
		protected System.Web.UI.HtmlControls.HtmlGenericControl msgPanel;
		protected System.Web.UI.WebControls.Label lblMessage;
		protected System.Web.UI.WebControls.Button btnOk;
		protected System.Web.UI.WebControls.Button btnCancel;
		protected System.Web.UI.WebControls.Button btnCheckAll;
		protected System.Web.UI.WebControls.Button btnClearAll;
		String PathCodeID=null;
	
		private void Page_Load(object sender, System.EventArgs e)
		{			
			// Put user code to initialize the page here
//			Utility utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings,Page.Session);
			appID = utility.GetAppID();
			enterpriseID = utility.GetEnterpriseID();
			userID = utility.GetUserID();
			PathCodeID=Request.Params["PATH_CODEID"];
			deliveryType=Request.Params["DELIVERY_TYPE"];
			sPathCode=Request.Params["PATH_CODE"];
			sFlightVehNo=Request.Params["FLIGHT_VEHICLENO"];
			sDepDateTime=Request.Params["DEPARTURE_DATE"];
			sManifestDateTime=Request.Params["MANIFEST_DATE"];
			sAWBDriverName=Request.Params["AWB_DRIVERNAME"];
			sLineHaulCost=Request.Params["LINE_HAULCOST"];

//			msgPanel.Visible=false;	//DIV Panel Visible set to false
			msgPanel.Style["display"]="none";
//			gridPanel.Style["display"]="inline";

			if (!Page.IsPostBack)
			{
				ViewState["PATH_CODEID"]=PathCodeID;
				ViewState["DELIVERY_TYPE"]=deliveryType;
				ViewState["PATH_CODE"]=sPathCode;
				ViewState["FLIGHT_VEHICLENO"]=sFlightVehNo;
				ViewState["DEPARTURE_DATE"]=sDepDateTime;
				ViewState["MANIFEST_DATE"]=sManifestDateTime;
				ViewState["AWB_DRIVERNAME"]=sAWBDriverName;
				ViewState["LINE_HAULCOST"]=sLineHaulCost;
				
				ShowCurrentPage();
				m_sdsDMList=(SessionDS) Session["SESSION_DS4"];	
				dsQuery=(DataSet) Session["SESSION_DS5"];
				BindDMList();
			}
			else
			{	
				PathCodeID=(String)ViewState["PATH_CODEID"];
				deliveryType=(String)ViewState["DELIVERY_TYPE"];
				sPathCode=(String)ViewState["PATH_CODE"];
				sFlightVehNo=(String)ViewState["FLIGHT_VEHICLENO"];
				sDepDateTime=(String)ViewState["DEPARTURE_DATE"];
				sManifestDateTime=(String)ViewState["MANIFEST_DATE"];
				sAWBDriverName=(String)ViewState["AWB_DRIVERNAME"];
				sLineHaulCost=(String)ViewState["LINE_HAULCOST"];

				if (Session["SESSION_DS4"] != null)
				{
					m_sdsDMList = (SessionDS)Session["SESSION_DS4"];
				}
				if (Session["SESSION_DS5"] != null)
				{
					dsQuery = (DataSet)Session["SESSION_DS5"];
				}
				if (Session["SESSION_DS6"] != null)
				{
					m_sdsConsignment = (SessionDS)Session["SESSION_DS6"];
				}
			}
			//if(!Utility.IsDeliveryManifestCountCorrected(this.appID,this.enterpriseID,this.sPathCode,this.sFlightVehNo,this.sManifestDateTime))
			//{
				//this.btnPrint.Enabled=false;
			//}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			base.OnInit(e);
			InitializeComponent();
			
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnQry.Click += new System.EventHandler(this.btnQry_Click);
			this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
			this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
			this.btnCheckAll.Click += new System.EventHandler(this.btnCheckAll_Click);
			this.btnClearAll.Click += new System.EventHandler(this.btnClearAll_Click);
			this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
			this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		public void OnPaging_DMList(Object sender, DataGridPageChangedEventArgs e)
		{	
			lblErrorMsg.Text="";
			bool somethingChecked=false;
			for(int i=0;i<dgDMList.Items.Count;i++)
			{
				CheckBox chkSelect=(CheckBox) dgDMList.Items[i].FindControl("chkSelect");
				if ((chkSelect !=null) && (chkSelect.Checked==true))
				{					
					somethingChecked=true;
					break;
				}
			}

			if (somethingChecked==true)
			{
				lblMessage.Text="You have some checkboxes checked.<br> Page Navigation will clear them, Are you sure?";
				msgFlag="N";
				ViewState["msgFlag"]=msgFlag;
//				msgPanel.Visible=true;
//				gridPanel.Visible=false;				
				msgPanel.Style["display"]="inline";
				gridPanel.Style["display"]="none";

			}
			else
			{
				dgDMList.CurrentPageIndex = e.NewPageIndex;
				ShowCurrentPage();
				BindDMList();
			}	
		
			if (m_sdsConsignment != null)
			{
				m_sdsConsignment.ds.Tables[0].Rows.Clear();
				BindConsGrid();
			}

		}

		private void UnCheckAllCheckBoxes()
		{			
			for(int i=0;i<dgDMList.Items.Count;i++)
			{
				CheckBox chkSelect=(CheckBox) dgDMList.Items[i].FindControl("chkSelect");
				if ((chkSelect !=null) && (chkSelect.Checked==true))
				{
					chkSelect.Checked=false;
				}
			}
		}

		private void ShowCurrentPage()
		{
			int iStartIndex = dgDMList.CurrentPageIndex * dgDMList.PageSize;
			//m_sdsDMList = DeliveryManifestMgrDAL.GetDMListDS(appID,enterpriseID,iStartIndex,dgDMList.PageSize,dsQuery);		
			m_sdsDMList = DeliveryManifestMgrDAL.GetDMListDS(appID,enterpriseID,iStartIndex,dgDMList.PageSize, deliveryType, sPathCode, sFlightVehNo, sDepDateTime, sManifestDateTime, sAWBDriverName, sLineHaulCost);	
			//Session["SESSION_DS5"]=dsQuery;
			decimal pgCnt = (m_sdsDMList.QueryResultMaxSize - 1)/dgDMList.PageSize;
			if(pgCnt < dgDMList.CurrentPageIndex)
			{
				dgDMList.CurrentPageIndex = System.Convert.ToInt32(pgCnt);
			}			
		}

		private void BindDMList()
		{

			dgDMList.VirtualItemCount = System.Convert.ToInt32(m_sdsDMList.QueryResultMaxSize);
			dgDMList.DataSource = m_sdsDMList.ds;
			dgDMList.DataBind();
		}

		private void btnQry_Click(object sender, System.EventArgs e)
		{
			returnSelected();
		}

		private void returnSelected()
		{
			lblErrorMsg.Text="";
			if (dgDMList.SelectedItem == null) 
			{
				String sScript = "";
				sScript += "<script language=javascript>";
				sScript += "  window.close();";
				sScript += "</script>";
				Response.Write(sScript);
			}
			else
			{
				int iSelIndex = dgDMList.SelectedItem.ItemIndex;
				if (iSelIndex< 0)
				{
					return;
				}
				DataGridItem dgRow = dgDMList.Items[iSelIndex];
				String strDelvryPathCode = dgRow.Cells[3].Text;
				String strDelvryType = dgRow.Cells[4].Text;
				String strDepDate = dgRow.Cells[5].Text;
				String strFlightVehNo = dgRow.Cells[6].Text;
				String strAWBDriverName = dgRow.Cells[7].Text;
				String strPathDesc = dgRow.Cells[12].Text;
				String strManifestDate = dgRow.Cells[13].Text;
				String strLineHaulCost = dgRow.Cells[14].Text;
			
				if(strDelvryPathCode == "&nbsp;")			
					strDelvryPathCode = "";			
				if (strDelvryType =="&nbsp;")
					strDelvryType="";
				if (strDepDate =="&nbsp;")
					strDepDate="";
				if (strFlightVehNo =="&nbsp;")
					strFlightVehNo="";
				if (strAWBDriverName =="&nbsp;")
					strAWBDriverName="";
				if (strPathDesc =="&nbsp;")
					strPathDesc="";
				if (strManifestDate =="&nbsp;")
					strManifestDate="";
				if (strLineHaulCost =="&nbsp;")
					strLineHaulCost="";
		
				Session["ASSIGNED"] = "TRUE";
				String sScript = "";
				sScript += "<script language=javascript>";
				//sScript += " DbComboSelectByValue("+PathCodeID+","+strDelvryPathCode+");";
				sScript += "  window.opener.DeliveryManifest."+PathCodeID+".value = \"" + strDelvryPathCode +"\";";
				sScript += "  window.opener.DeliveryManifest.txtPathDesc.value = \"" + strPathDesc +"\";";
				sScript += "  window.opener.DeliveryManifest.txtVehicle_Flight_No.value = \"" + strFlightVehNo +"\";";
				if (strDelvryType=="L")
				{
					sScript += "  window.opener.DeliveryManifest.rbtnLong.checked = true;";
					sScript += "  window.opener.DeliveryManifest.rbtnShort.checked = false;";
					sScript += "  window.opener.DeliveryManifest.rbtnAir.checked = false;";
				}
				else if(strDelvryType=="S")
				{
					sScript += "  window.opener.DeliveryManifest.rbtnShort.checked = true;";
					sScript += "  window.opener.DeliveryManifest.rbtnLong.checked = false;";
					sScript += "  window.opener.DeliveryManifest.rbtnAir.checked = false;";
				}
				else if(strDelvryType=="A")
				{
					sScript += "  window.opener.DeliveryManifest.rbtnAir.checked = true;";
					sScript += "  window.opener.DeliveryManifest.rbtnShort.checked = false;";
					sScript += "  window.opener.DeliveryManifest.rbtnLong.checked = false;";
				}
				sScript += "  window.opener.DeliveryManifest.txtDriver_Flight_AWB.value = '" + strAWBDriverName + "';";
				sScript += "  window.opener.DeliveryManifest.txtDepartDate.value = '" + strDepDate + "';";
				sScript += "  window.opener.DeliveryManifest.txtDelManifestDate.value = '" + strManifestDate + "';";
				sScript += "  window.opener.DeliveryManifest.txtLineHaulCost.value = '" + strLineHaulCost + "';";
				sScript += "  window.opener.DeliveryManifest.action='DeliveryManifest.aspx?REFRESH=YES';";
				sScript += "  window.opener.DeliveryManifest.submit();";
				sScript += "  window.close();";
				sScript += "</script>";
				Response.Write(sScript);
			}
		}

		public void dgDMList_Button(object sender, DataGridCommandEventArgs e)
		{
			lblErrorMsg.Text="";
			int iSelIndex = e.Item.ItemIndex;
			if (iSelIndex< 0)
			{
				return;
			}
			DataGridItem dgRow = dgDMList.Items[iSelIndex];
			String strDelvryPathCode = dgRow.Cells[3].Text;
			String strDelvryType = dgRow.Cells[4].Text;
			String strDepDate = dgRow.Cells[5].Text;
			String strFlightVehNo = dgRow.Cells[6].Text;
			String strAWBDriverName = dgRow.Cells[7].Text;
			String strPathDesc = dgRow.Cells[12].Text;
			String strManifestDate = dgRow.Cells[13].Text;
			String strLineHaulCost = dgRow.Cells[14].Text;
			
			if(strDelvryPathCode == "&nbsp;")			
				strDelvryPathCode = "";			
			if (strDelvryType =="&nbsp;")
				strDelvryType="";
			if (strDepDate =="&nbsp;")
				strDepDate="";
			if (strFlightVehNo =="&nbsp;")
				strFlightVehNo="";
			if (strAWBDriverName =="&nbsp;")
				strAWBDriverName="";
			if (strPathDesc =="&nbsp;")
				strPathDesc="";
			if (strManifestDate =="&nbsp;")
				strManifestDate="";
			if (strLineHaulCost =="&nbsp;")
				strLineHaulCost="";
		
			String strCmdNm = e.CommandName;
			if (strCmdNm.Equals("Select"))
			{			
				ViewState["DelvryPathCode"] = strDelvryPathCode;
				ViewState["DelvryType"] = strDelvryType;
				ViewState["FlightVehNo"] = strFlightVehNo;
				ViewState["ManifestDate"] = strManifestDate;
				ShowCurrentConsginment();
				BindConsGrid();		
			}
			else if(strCmdNm.Equals("Choose"))
			{
				Session["ASSIGNED"] = "TRUE";
				String sScript = "";
				sScript += "<script language=javascript>";
				//sScript += " DbComboSelectByValue("+PathCodeID+","+strDelvryPathCode+");";
				sScript += "  window.opener.DeliveryManifest."+PathCodeID+".value = \"" + strDelvryPathCode +"\";";
				sScript += "  window.opener.DeliveryManifest.txtPathDesc.value = \"" + strPathDesc +"\";";
				sScript += "  window.opener.DeliveryManifest.txtVehicle_Flight_No.value = \"" + strFlightVehNo +"\";";
				if (strDelvryType=="L")
				{
					sScript += "  window.opener.DeliveryManifest.rbtnLong.checked = true;";
					sScript += "  window.opener.DeliveryManifest.rbtnShort.checked = false;";
					sScript += "  window.opener.DeliveryManifest.rbtnAir.checked = false;";
				}
				else if(strDelvryType=="S")
				{
					sScript += "  window.opener.DeliveryManifest.rbtnShort.checked = true;";
					sScript += "  window.opener.DeliveryManifest.rbtnLong.checked = false;";
					sScript += "  window.opener.DeliveryManifest.rbtnAir.checked = false;";
				}
				else if(strDelvryType=="A")
				{
					sScript += "  window.opener.DeliveryManifest.rbtnAir.checked = true;";
					sScript += "  window.opener.DeliveryManifest.rbtnShort.checked = false;";
					sScript += "  window.opener.DeliveryManifest.rbtnLong.checked = false;";
				}
				sScript += "  window.opener.DeliveryManifest.txtDriver_Flight_AWB.value = '" + strAWBDriverName + "';";
				sScript += "  window.opener.DeliveryManifest.txtDepartDate.value = '" + strDepDate + "';";
				sScript += "  window.opener.DeliveryManifest.txtDelManifestDate.value = '" + strManifestDate + "';";
				sScript += "  window.opener.DeliveryManifest.txtLineHaulCost.value = '" + strLineHaulCost + "';";
				sScript += "  window.opener.DeliveryManifest.action='DeliveryManifest.aspx?REFRESH=YES';";
				sScript += "  window.opener.DeliveryManifest.submit();";
				sScript += "  window.close();";
				sScript += "</script>";
				Response.Write(sScript);
			}

		}

		private void ShowCurrentConsginment()
		{
			String strDelvryPathCode=(String) ViewState["DelvryPathCode"];
			String strDelvryType = (String) ViewState["DelvryType"];
			String strFlightVehNo=(String) ViewState["FlightVehNo"];
			String strManifestDate=(String) ViewState["ManifestDate"];

			int iStartIndex = dgConsignment.CurrentPageIndex * dgConsignment.PageSize;			
			m_sdsConsignment = DeliveryManifestMgrDAL.GetConsignmentDS(appID,enterpriseID,strDelvryPathCode,strDelvryType, strFlightVehNo, strManifestDate, iStartIndex, dgConsignment.PageSize);
			decimal pgCnt = (m_sdsConsignment.QueryResultMaxSize - 1)/dgConsignment.PageSize;
			if(pgCnt < dgConsignment.CurrentPageIndex)
			{
				dgConsignment.CurrentPageIndex = System.Convert.ToInt32(pgCnt);
			}
			dgConsignment.EditItemIndex = -1;			
		}

		private void BindConsGrid()
		{
			dgConsignment.VirtualItemCount = System.Convert.ToInt32(m_sdsConsignment.QueryResultMaxSize);
			dgConsignment.DataSource = m_sdsConsignment.ds;
			dgConsignment.DataBind();
			Session["SESSION_DS6"] = m_sdsConsignment;
		}

		public void OnPaging_ConsignmentList(Object sender, DataGridPageChangedEventArgs e)
		{			
			dgConsignment.CurrentPageIndex = e.NewPageIndex;
			ShowCurrentConsginment();
			BindConsGrid();
		}

		private void DeleteDM_Cons()
		{
			dsDelete=(DataSet)ViewState["dsDelete"];
			try
			{
				DeliveryManifestMgrDAL.DeleteDMsNConsignments(appID,enterpriseID,dsDelete);
				lblErrorMsg.Text=" The selected Delivery Manifest and the Consignment(s) are deleted successfully.";
			}
			catch (Exception exp)
			{
				String strMessage = exp.Message;
				lblErrorMsg.Text = strMessage;
			}	
			ShowCurrentPage();
			BindDMList();
			ShowCurrentConsginment();
			BindConsGrid();
		}

		private void btnDelete_Click(object sender, System.EventArgs e)
		{		
			bool itemsChecked=false;
			lblErrorMsg.Text="";
			dsDelete=GetDMDS();
			for(int i=0;i< dgDMList.Items.Count;i++)
			{
				CheckBox chkSelect=(CheckBox) dgDMList.Items[i].FindControl("chkSelect");
				if ((chkSelect !=null) && (chkSelect.Checked==true))
				{
					DataRow drNew=dsDelete.Tables[0].NewRow();					
					DataGridItem dgRow = dgDMList.Items[i];

					drNew["Path_Code"]= dgRow.Cells[3].Text;										
					drNew["Flight_Vehicle_No"]= dgRow.Cells[6].Text;					
					drNew["Delivery_Manifest_DateTime"]= System.DateTime.ParseExact(dgRow.Cells[13].Text,"dd/MM/yyyy HH:mm",null);
					dsDelete.Tables[0].Rows.Add(drNew);					
					itemsChecked=true;
				}
			}	
			
			if (itemsChecked==false)
			{
				if(!utility.GetUserCulture().ToUpper().Equals("EN-US"))
				{
					lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage, "PLS_CHK_DLV_MNFST_DEL", utility.GetUserCulture());
				} 
				else
				{
					lblErrorMsg.Text = "Please check at least a Delivery Manifest to Delete.";
				}
				return;
			}
				ViewState["dsDelete"]=dsDelete;
				lblMessage.Text="Are you sure to Delete the selected Delivery Manifest and <br>  the Consignment records(s) ?";
				msgFlag="D";
				ViewState["msgFlag"]=msgFlag;
//				msgPanel.Visible=true;				
//				gridPanel.Visible=false;								
				msgPanel.Style["display"]="inline";
				gridPanel.Style["display"]="none";

		}


		private void btnOk_Click(object sender, System.EventArgs e)
		{
			msgFlag=(String)ViewState["msgFlag"];
			if (msgFlag=="N")
			{
				UnCheckAllCheckBoxes();	
				ShowCurrentPage();
			}
			else if(msgFlag=="D")
			{
				DeleteDM_Cons();
			}
//			msgPanel.Visible=false;
//			gridPanel.Visible=true;
			msgPanel.Style["display"]="none";
			gridPanel.Style["display"]="inline";

			
		}

		private void btnCancel_Click(object sender, System.EventArgs e)
		{
			msgFlag=(String)ViewState["msgFlag"];
			if (msgFlag=="D")
			{
				UnCheckAllCheckBoxes();	
			}
			else if(msgFlag=="N")
			{

			}
//			msgPanel.Visible=false;
//			gridPanel.Visible=true;
			msgPanel.Style["display"]="none";
			gridPanel.Style["display"]="inline";

		}

		private void btnPrint_Click(object sender, System.EventArgs e)
		{
			bool itemsChecked=false;
			lblErrorMsg.Text="";
			dsPrint=GetPrintDS();
			for(int i=0;i< dgDMList.Items.Count;i++)
			{
				CheckBox chkSelect=(CheckBox) dgDMList.Items[i].FindControl("chkSelect");
				if ((chkSelect !=null) && (chkSelect.Checked==true))
				{
					DataRow drNew=dsPrint.Tables[0].NewRow();					
					DataGridItem dgRow = dgDMList.Items[i];
				
					drNew["delivery_type"]= dgRow.Cells[4].Text;
					drNew["path_code"]= dgRow.Cells[3].Text;
					drNew["flight_vehicle_no"]= dgRow.Cells[6].Text;
					drNew["delivery_manifest_datetime"]= System.DateTime.ParseExact(dgRow.Cells[13].Text,"dd/MM/yyyy HH:mm",null);
					dsPrint.Tables[0].Rows.Add(drNew);					
					itemsChecked=true;
				}
			}	
			//PLS_CHK_DLV_MNFST_PRT
			if (itemsChecked==false)
			{
				if(!utility.GetUserCulture().ToUpper().Equals("EN-US"))
				{
					lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage, "PLS_CHK_DLV_MNFST_PRT", utility.GetUserCulture());
				} 
				else
				{
					lblErrorMsg.Text = "Please check at least a Delivery Manifest to Print.";
				}
				//lblErrorMsg.Text="Please check at least a Delivery Manifest to Print.";
				return;
			}

			//Added by Tom 11/6/09
			if(deliveryType == "L")
				Session["FORMID"] = "DeliveryManifestListLH";
			else if(deliveryType == "S" || deliveryType == "A") 
				Session["FORMID"] = "DeliveryManifestList";
			//End added by Tom 11/6/09 
			Session["SESSION_DS1"] = dsPrint;

			String sUrl = "ReportViewer.aspx";
			ArrayList paramList = new ArrayList();
			paramList.Add(sUrl);

			String sScript = Utility.GetScript("PkgDetails.js",paramList);
			Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
		}
			
		private DataSet GetPrintDS()
		{
			DataTable dtDM = new DataTable(); 
			dtDM.Columns.Add(new DataColumn("delivery_type", typeof(string)));
			dtDM.Columns.Add(new DataColumn("path_code", typeof(string)));
			dtDM.Columns.Add(new DataColumn("flight_vehicle_no", typeof(string)));
			dtDM.Columns.Add(new DataColumn("delivery_manifest_dateTime", typeof(DateTime)));
		
			DataSet dsDM = new DataSet();
			dsDM.Tables.Add(dtDM);
		
			return  dsDM;				
		}

		private DataSet GetDMDS()
		{
			DataTable dtDM = new DataTable(); 
			dtDM.Columns.Add(new DataColumn("Path_Code", typeof(string)));
			dtDM.Columns.Add(new DataColumn("Flight_Vehicle_No", typeof(string)));
			dtDM.Columns.Add(new DataColumn("Delivery_Manifest_DateTime", typeof(DateTime)));
			
			DataSet dsDM = new DataSet();
			dsDM.Tables.Add(dtDM);
			
			return  dsDM;				
		}

		private DataSet GetCons()
		{
			DataTable dtDMCons = new DataTable();
			dtDMCons.Columns.Add(new DataColumn("Consignment_no", typeof(string)));
			dtDMCons.Columns.Add(new DataColumn("Origin_State_Code", typeof(string)));
			dtDMCons.Columns.Add(new DataColumn("Destination_State_Code", typeof(string)));
			dtDMCons.Columns.Add(new DataColumn("Route_Code", typeof(string)));

			DataSet dsDMCons = new DataSet();
			dsDMCons.Tables.Add(dtDMCons);
			
			return  dsDMCons;	
			
		}
		public void dgCons_Delete(object sender, DataGridCommandEventArgs e)
		{
			lblErrorMsg.Text="";
			int iSelIndex = e.Item.ItemIndex;
			if (iSelIndex< 0)
			{
				return;
			}
			DataGridItem dgRow = dgConsignment.Items[iSelIndex];
			String strConsignment = dgRow.Cells[1].Text;
			String strOrigin = dgRow.Cells[2].Text;
			String strDestination = dgRow.Cells[3].Text;
			String strRoute = dgRow.Cells[4].Text;

			if(strConsignment == "&nbsp;")
				strConsignment = "";			
			if (strOrigin =="&nbsp;")
				strOrigin="";
			if (strDestination =="&nbsp;")
				strDestination="";
			if (strRoute =="&nbsp;")
				strRoute="";

			DataSet dsConsignment=GetCons();
			
			DataRow drConsDel = dsConsignment.Tables[0].NewRow();
			drConsDel["Consignment_No"]=strConsignment;
			drConsDel["Origin_State_Code"]=strOrigin;
			drConsDel["Destination_State_Code"]=strDestination;
			drConsDel["Route_Code"]=strRoute;	
																						
			dsConsignment.Tables[0].Rows.Add(drConsDel);

			//Read From ViewState
			String strDelvryPathCode =(String)ViewState["DelvryPathCode"];
			String strFlightVehNo =(String)ViewState["FlightVehNo"];
			String strManifestDate =(String)ViewState["ManifestDate"];

			DataSet dsDM=GetDMDS();
			DataRow drDMDel = dsDM.Tables[0].NewRow();

			if (strDelvryPathCode !="")
				drDMDel["Path_Code"]=strDelvryPathCode;
			if (strFlightVehNo !="")
				drDMDel["Flight_Vehicle_No"]=strFlightVehNo;
			if (strManifestDate !="")
				drDMDel["Delivery_Manifest_DateTime"]=System.DateTime.ParseExact(strManifestDate,"dd/MM/yyyy HH:mm",null);
			
			dsDM.Tables[0].Rows.Add(drDMDel);

			try
			{
				DeliveryManifestMgrDAL.DeleteConsignment(appID,enterpriseID,dsDM,dsConsignment);
				lblErrorMsg.Text ="The selected Consignment is deleted successfully.";	
			}
			catch(ApplicationException appException)
			{
				String strMsg = appException.Message;
				if(strMsg.IndexOf("FK") != -1)
				{
					lblErrorMsg.Text = "Error in Deleting Consignment.";					
				}

				return;
			}
			m_sdsConsignment.ds.Tables[0].Rows.RemoveAt(iSelIndex);
			m_sdsConsignment.QueryResultMaxSize--;
			dgConsignment.CurrentPageIndex = 0;

			dgConsignment.SelectedIndex = -1;
			BindConsGrid();
				
		}

		private void btnCheckAll_Click(object sender, System.EventArgs e)
		{
			lblErrorMsg.Text="";
			for(int i=0;i<dgDMList.Items.Count;i++)
			{
				CheckBox chkSelect = (CheckBox)dgDMList.Items[i].FindControl("chkSelect");
				chkSelect.Checked=true;
			}
		}

		private void btnClearAll_Click(object sender, System.EventArgs e)
		{
			lblErrorMsg.Text="";
			for(int i=0;i<dgDMList.Items.Count;i++)
			{
				CheckBox chkSelect = (CheckBox)dgDMList.Items[i].FindControl("chkSelect");
				chkSelect.Checked=false;
			}
		}
	}
}
