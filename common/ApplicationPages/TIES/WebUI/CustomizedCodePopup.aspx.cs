using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Text;
using com.common.DAL;
using com.common.util;
using com.common.classes;
using com.ties.BAL;
using com.common.applicationpages;

namespace TIES.WebUI
{
	/// <summary>
	/// Summary description for CustomizedCodePopup.
	/// </summary>
	public class CustomizedCodePopup : BasePopupPage
	{
		protected System.Web.UI.WebControls.DataGrid dgServiceCode;
		protected System.Web.UI.WebControls.Label lblCustName;
		protected System.Web.UI.WebControls.Label lblErrorMsg;
		protected System.Web.UI.WebControls.Button btnSearch;
		protected System.Web.UI.WebControls.TextBox txtServiceCode;
		protected System.Web.UI.WebControls.Label lblCustID;
		protected System.Web.UI.WebControls.Button btnRefresh;
		protected System.Web.UI.WebControls.TextBox txtServiceDescription;
		protected System.Web.UI.WebControls.Label lblServiceTitle;
		SessionDS m_sdsService = null;
	
		//Utility utility = null;

		private String appID = null;
		private String enterpriseID = null;
		protected System.Web.UI.WebControls.Button btnClose;
		private String strFormId=null;

		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			
			strFormId=Request.Params["FORMID"];  
			
			//utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings,Page.Session);
			appID = utility.GetAppID();
			enterpriseID = utility.GetEnterpriseID();
			if(!Page.IsPostBack)
			{
				txtServiceCode.Text = Request.Params["SERVICECODE"];
				txtServiceDescription.Text = Request.Params["SERVICEDESC"];
				m_sdsService = GetEmptyServiceDS(0);
				BindGrid();
			}
			else
			{
				m_sdsService = (SessionDS)ViewState["SERVICE_DS"];

			}
		}
		public void Paging(Object sender, DataGridPageChangedEventArgs e)
		{
			//runs when one of the page control is clicked
			//update the current page from the parameter values
			dgServiceCode.CurrentPageIndex = e.NewPageIndex;
			//bind data to the grid
			ShowCurrentPage();
		}
		
		public static SessionDS GetEmptyServiceDS(int iNumRows)
		{
			DataTable dtService = new DataTable();

			dtService.Columns.Add(new DataColumn("code_text", typeof(string)));
			dtService.Columns.Add(new DataColumn("code_num_value", typeof(decimal)));
			dtService.Columns.Add(new DataColumn("assemblyid", typeof(string)));
			dtService.Columns.Add(new DataColumn("Assembly_description", typeof(string)));


			for(int i=0; i < iNumRows; i++)
			{
				DataRow drEach = dtService.NewRow();
				drEach[0] = "";
				drEach[1] = 0;
				drEach[2] = "";
				drEach[3] = "";

				dtService.Rows.Add(drEach);
			}

			DataSet dsService = new DataSet();
			dsService.Tables.Add(dtService);

			SessionDS sessionDS = new SessionDS();
			sessionDS.ds = dsService;
			sessionDS.DataSetRecSize = dsService.Tables[0].Rows.Count;
			sessionDS.QueryResultMaxSize = sessionDS.DataSetRecSize;

			return  sessionDS;	
		}

		private SessionDS GetServiceDS(int iCurrent, int iDSRecSize, String strQuery)
		{
			SessionDS sessionDS = null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(appID,enterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("ServicePopup.aspx.cs","GetServiceDS","ERR001","DbConnection object is null!!");
				return sessionDS;
			}
			
			sessionDS = dbCon.ExecuteQuery(strQuery,iCurrent,iDSRecSize,"SenderRecipentData");
			return  sessionDS;			
		}

		private void BindGrid()
		{
			dgServiceCode.VirtualItemCount = System.Convert.ToInt32(m_sdsService.QueryResultMaxSize);
			dgServiceCode.DataSource = m_sdsService.ds;
			dgServiceCode.DataBind();
			ViewState["SERVICE_DS"] = m_sdsService;
		}

		private void ShowCurrentPage()
		{
			int iStartIndex = dgServiceCode.CurrentPageIndex * dgServiceCode.PageSize;
			String sqlQuery = (String)ViewState["SQL_QUERY"];
			m_sdsService = GetServiceDS(iStartIndex,dgServiceCode.PageSize,sqlQuery);
			int pgCnt = (Convert.ToInt32(m_sdsService.QueryResultMaxSize - 1))/dgServiceCode.PageSize;
			if(pgCnt < dgServiceCode.CurrentPageIndex)
			{
				dgServiceCode.CurrentPageIndex = System.Convert.ToInt32(pgCnt);
			}
			dgServiceCode.SelectedIndex = -1;
			dgServiceCode.EditItemIndex = -1;
			BindGrid();
		}


		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			base.OnInit(e);
			InitializeComponent();
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
			this.dgServiceCode.SelectedIndexChanged += new System.EventHandler(this.dgServiceCode_SelectedIndexChanged);
			this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private void btnSearch_Click(object sender, System.EventArgs e)
		{
			String strServiceCode = txtServiceCode.Text.Trim();
			String strServiceDesc = txtServiceDescription.Text.Trim();

			StringBuilder strQry = new StringBuilder();
			strQry.Append(" select distinct s.code_text, s.code_num_value, a.assemblyid,a.assembly_description, s.codeid from core_system_code s,assembly a "); 
			if(strFormId != "CustomerCustomizedCharges")
				strQry.Append(" where  codeid='customized_assembly'  and a.applicationid='"+appID+"' and a.enterpriseid='"+enterpriseID+"' and s.applicationid='"+appID+"' and code_text not like '%Customer%'");
			else
				strQry.Append(" where  codeid='customized_assembly'  and a.applicationid='"+appID+"' and a.enterpriseid='"+enterpriseID+"' and s.applicationid='"+appID+"' and code_text like '%Customer%'");
			

			if(strServiceCode != null && strServiceCode != "")
			{
				strQry.Append(" and s.code_num_value like '%"+Utility.ReplaceSingleQuote(strServiceCode)+"%' ");
			}

			String strSQLQuery = strQry.ToString();

			ViewState["SQL_QUERY"] = strSQLQuery;
			dgServiceCode.CurrentPageIndex = 0;

			ShowCurrentPage();
			if(!utility.GetUserCulture().ToUpper().Equals("EN-US"))
			{
				getPageControls(Page);		
			}
		}

		
		private void dgServiceCode_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			//Get the value and return it to the parent window
			int iSelIndex = dgServiceCode.SelectedIndex;
			DataGridItem dgRow			= dgServiceCode.Items[iSelIndex];
			String strCustomizedDesc	= dgRow.Cells[0].Text;
			String strCustomizedCode	= dgRow.Cells[1].Text;
			String strAssemblyid		= dgRow.Cells[2].Text;
			String strDesc				= dgRow.Cells[3].Text;
			
			String sScript = "";
			sScript += "<script language=javascript>";

				String strCodeClientDescid	= Request.Params["CODEDESC"];
				String strCodeClientid		= Request.Params["CODEID"];
				String strDescClientid		= Request.Params["DESC"];
				String strSurchargeClientid = Request.Params["ASSEMBLYID"];
			if(Request.Params["FORMID"].ToString() != "CustomerCustomizedCharges")
			{
				sScript += "  window.opener.AgentCustomableCharge."+strCodeClientDescid+".value = \"" + strCustomizedDesc + "\";";
				sScript += "  window.opener.AgentCustomableCharge."+strCodeClientid+".value = \"" + strCustomizedCode + "\";";
				sScript += "  window.opener.AgentCustomableCharge."+strSurchargeClientid+".value = \"" + strAssemblyid + "\";";
				sScript += "  window.opener.AgentCustomableCharge."+strDescClientid+".value = \"" + strDesc +"\";";
			}
			else
			{
				sScript += "  window.opener.CustomerCustomizedCharges."+strCodeClientDescid+".value = \"" + strCustomizedDesc + "\";";
				sScript += "  window.opener.CustomerCustomizedCharges."+strCodeClientid+".value = \"" + strCustomizedCode + "\";";
				sScript += "  window.opener.CustomerCustomizedCharges."+strSurchargeClientid+".value = \"" + strAssemblyid + "\";";
				sScript += "  window.opener.CustomerCustomizedCharges."+strDescClientid+".value = \"" + strDesc +"\";";
			}

			sScript += "  window.close();";
			sScript += "</script>";
			Response.Write(sScript);
		}

		private void btnClose_Click(object sender, System.EventArgs e)
		{
			String sScript = "";
			sScript += "<script language=javascript>";
			sScript += "  window.close();";
			sScript += "</script>";
			Response.Write(sScript);
		}
	}
}
