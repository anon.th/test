<%@ Page language="c#" Codebehind="InvoiceGenerationPreview.aspx.cs" AutoEventWireup="false" Inherits="com.ties.InvoiceGenerationPreview" %>
<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<%@ Import Namespace="com.common.util" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>InvoiceGenerationPreview</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<LINK href="css/Styles.css" type="text/css" rel="stylesheet" name="stylesheet">
		<script language="javascript" src="Scripts/settingScrollPosition.js"></script>
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
	</HEAD>
	<body onkeypress="microsoftKeyPress()" onclick="GetScrollPosition();" onload="SetScrollPosition();" onunload="window.top.displayBanner.fnCloseAll(0);" MS_POSITIONING="GridLayout">
		<form id="InvoiceGenerationPreview" method="post" runat="server">
			<FONT face="Tahoma">
				<asp:label id="lblMainTitle" style="Z-INDEX: 101; LEFT: 8px; POSITION: absolute; TOP: 8px" runat="server" Height="26px" Width="477px" CssClass="mainTitleSize">Invoice Generation Preview</asp:label><asp:datagrid id="dgPreview" style="Z-INDEX: 115; LEFT: 5px; POSITION: absolute; TOP: 253px" runat="server" Height="95px" Width="890px" SelectedItemStyle-CssClass="gridFieldSelected" AutoGenerateColumns="False" ItemStyle-Height="20" AllowPaging="True">
					<SelectedItemStyle CssClass="gridFieldSelected"></SelectedItemStyle>
					<ItemStyle Height="10px" CssClass="gridField"></ItemStyle>
					<Columns>
						<asp:TemplateColumn>
							<HeaderStyle Width="20px" CssClass="gridHeading"></HeaderStyle>
							<ItemTemplate>
								<asp:CheckBox ID="chkSelect" Runat="server"></asp:CheckBox>
							</ItemTemplate>
						</asp:TemplateColumn>
						<asp:BoundColumn DataField="consignment_no" HeaderText="Consignment No.">
							<HeaderStyle HorizontalAlign="Center" CssClass="gridHeading"></HeaderStyle>
							<ItemStyle CssClass="gridField"></ItemStyle>
						</asp:BoundColumn>
						<asp:BoundColumn DataField="ref_no" HeaderText="Ref No.">
							<HeaderStyle HorizontalAlign="Center" CssClass="gridHeading"></HeaderStyle>
							<ItemStyle CssClass="gridField"></ItemStyle>
						</asp:BoundColumn>
						<asp:TemplateColumn HeaderText="Customer/ Sender">
							<HeaderStyle HorizontalAlign="Center" Width="180px" CssClass="gridHeading"></HeaderStyle>
							<ItemStyle Wrap="False" VerticalAlign="Middle" CssClass="gridField"></ItemStyle>
							<ItemTemplate>
								<asp:DropDownList Width="150px" CssClass="gridDropDown" ID="ddlSCShipmentUpdate" Runat="server"></asp:DropDownList>
								<asp:ImageButton CommandName="SaveCust" id="imgSave" runat="server" ImageUrl="images/butt-update.gif"></asp:ImageButton>
							</ItemTemplate>
						</asp:TemplateColumn>
						<asp:BoundColumn DataField="sender_zipcode" HeaderText="Origin">
							<HeaderStyle HorizontalAlign="Center" CssClass="gridHeading"></HeaderStyle>
							<ItemStyle HorizontalAlign="Left" CssClass="gridField"></ItemStyle>
						</asp:BoundColumn>
						<asp:BoundColumn DataField="act_pickup_datetime" HeaderText="Pickup" DataFormatString="{0:dd/MM/yyyy HH:mm}">
							<HeaderStyle HorizontalAlign="Center" CssClass="gridHeading"></HeaderStyle>
							<ItemStyle HorizontalAlign="Left" CssClass="gridField"></ItemStyle>
						</asp:BoundColumn>
						<asp:BoundColumn DataField="recipient_name" HeaderText="Recipient">
							<HeaderStyle HorizontalAlign="Center" CssClass="gridHeading"></HeaderStyle>
							<ItemStyle HorizontalAlign="Left" CssClass="gridField"></ItemStyle>
						</asp:BoundColumn>
						<asp:BoundColumn DataField="recipient_zipcode" HeaderText="Destination">
							<HeaderStyle HorizontalAlign="Center" CssClass="gridHeading"></HeaderStyle>
							<ItemStyle HorizontalAlign="Left" CssClass="gridField"></ItemStyle>
						</asp:BoundColumn>
						<asp:BoundColumn DataField="act_delivery_date" HeaderText="Delivery" DataFormatString="{0:dd/MM/yyyy HH:mm}">
							<HeaderStyle HorizontalAlign="Center" CssClass="gridHeading"></HeaderStyle>
							<ItemStyle HorizontalAlign="Left" CssClass="gridField"></ItemStyle>
						</asp:BoundColumn>
						<asp:BoundColumn DataField="service_code" HeaderText="Service">
							<HeaderStyle HorizontalAlign="Center" CssClass="gridHeading"></HeaderStyle>
							<ItemStyle HorizontalAlign="Center" CssClass="gridField"></ItemStyle>
						</asp:BoundColumn>
						<asp:BoundColumn DataField="tot_freight_charge" HeaderText="Freight" DataFormatString="{0:#,##0.00}">
							<HeaderStyle HorizontalAlign="Center" CssClass="gridHeading"></HeaderStyle>
							<ItemStyle HorizontalAlign="Right" CssClass="gridField"></ItemStyle>
						</asp:BoundColumn>
						<asp:BoundColumn DataField="insurance_surcharge" HeaderText="Insurance" DataFormatString="{0:#,##0.00}">
							<HeaderStyle HorizontalAlign="Center" CssClass="gridHeading"></HeaderStyle>
							<ItemStyle HorizontalAlign="Right" CssClass="gridField"></ItemStyle>
						</asp:BoundColumn>
						<asp:BoundColumn DataField="other_surch_amount" HeaderText="Other" DataFormatString="{0:#,##0.00}">
							<HeaderStyle HorizontalAlign="Center" CssClass="gridHeading"></HeaderStyle>
							<ItemStyle HorizontalAlign="Right" CssClass="gridField"></ItemStyle>
						</asp:BoundColumn>
						<asp:BoundColumn DataField="tot_vas_surcharge" HeaderText="VAS" DataFormatString="{0:#,##0.00}">
							<HeaderStyle HorizontalAlign="Center" CssClass="gridHeading"></HeaderStyle>
							<ItemStyle HorizontalAlign="Right" CssClass="gridField"></ItemStyle>
						</asp:BoundColumn>
						<asp:BoundColumn DataField="esa_surcharge" HeaderText="ESA" DataFormatString="{0:#,##0.00}">
							<HeaderStyle HorizontalAlign="Center" CssClass="gridHeading"></HeaderStyle>
							<ItemStyle HorizontalAlign="Right" CssClass="gridField"></ItemStyle>
						</asp:BoundColumn>
						<asp:BoundColumn DataField="last_exception_code" HeaderText="PODEX Code">
							<HeaderStyle HorizontalAlign="Center" CssClass="gridHeading"></HeaderStyle>
							<ItemStyle HorizontalAlign="Center" CssClass="gridField"></ItemStyle>
						</asp:BoundColumn>
					</Columns>
					<PagerStyle NextPageText="Next" PrevPageText="Previous" HorizontalAlign="Left" CssClass="normalText"></PagerStyle>
				</asp:datagrid>
				<TABLE id="Table10" style="Z-INDEX: 114; LEFT: 7px; WIDTH: 727px; POSITION: absolute; TOP: 95px; HEIGHT: 139px" cellSpacing="1" width="727" align="left" border="0" runat="server">
					<TR height="25">
						<TD style="WIDTH: 96px" vAlign="center" align="left" width="96" bgColor="#008499">&nbsp;
							<asp:label id="Label5" runat="server" Height="15px" Width="88px" ForeColor="White" BackColor="#008499" Font-Size="11px">Invoice Type</asp:label></TD>
						<TD style="WIDTH: 9px" vAlign="top" width="9"></TD>
						<TD style="WIDTH: 1001px" vAlign="center" width="1001"><asp:label id="lblInvoiceType" runat="server" Width="143px" CssClass="tableLabel"></asp:label></TD>
						<TD style="WIDTH: 148px" align="left" width="148">&nbsp;</TD>
						<TD style="WIDTH: 8px" width="8"></TD>
						<TD style="WIDTH: 61px" width="61"></TD>
					</TR>
					<TR height="25">
						<TD style="WIDTH: 96px" vAlign="center" align="left" width="96" bgColor="#008499">&nbsp;
							<asp:label id="Label50" runat="server" Height="16px" Width="88px" CssClass="tableLabel" ForeColor="White" BackColor="#008499" Font-Size="11px">Customer ID</asp:label></TD>
						<TD style="WIDTH: 9px" vAlign="top" width="9"></TD>
						<TD style="WIDTH: 1001px" vAlign="center" width="1001"><asp:label id="lblCustID" runat="server" Width="143px" CssClass="tableLabel"></asp:label></TD>
						<TD style="WIDTH: 148px" align="left" width="148" bgColor="#008499">&nbsp;
							<asp:label id="Label6" runat="server" Height="15px" Width="81px" CssClass="tableLabel" ForeColor="White" BackColor="#008499" Font-Size="11px"> Province</asp:label></TD>
						<TD style="WIDTH: 8px" width="8"></TD>
						<TD style="WIDTH: 61px" width="61"><asp:label id="lblProvince" runat="server" Width="133px" CssClass="tableLabel"></asp:label></TD>
					</TR>
					<TR height="25">
						<TD style="WIDTH: 96px; HEIGHT: 25px" align="left" width="96" bgColor="#008499">&nbsp;
							<asp:label id="Label48" runat="server" Height="15px" Width="96px" CssClass="tableLabel" ForeColor="White" BackColor="#008499" Font-Size="11px">Customer Name</asp:label></TD>
						<TD style="WIDTH: 9px; HEIGHT: 25px" width="9"></TD>
						<TD style="WIDTH: 1001px; HEIGHT: 25px" vAlign="center" width="1001"><asp:label id="lblCustName" runat="server" Width="260px" CssClass="tableLabel"></asp:label></TD>
						<TD style="WIDTH: 148px; HEIGHT: 25px" align="left" width="148" bgColor="#008499">&nbsp;
							<asp:label id="Label49" runat="server" Height="14px" Width="93px" CssClass="tableLabel" ForeColor="White" BackColor="#008499" Font-Size="11px"> Telephone</asp:label></TD>
						<TD style="WIDTH: 8px; HEIGHT: 25px" width="8"></TD>
						<TD style="WIDTH: 61px; HEIGHT: 25px" width="61"><asp:label id="lblTelephone" runat="server" Width="133px" CssClass="tableLabel"></asp:label></TD>
					</TR>
					<TR height="25">
						<TD style="WIDTH: 96px" vAlign="center" align="left" width="96" bgColor="#008499">&nbsp;
							<asp:label id="Label45" runat="server" Height="15px" Width="90px" CssClass="tableLabel" ForeColor="White" BackColor="#008499" Font-Size="11px"> Address 1</asp:label></TD>
						<TD style="WIDTH: 9px" width="9"></TD>
						<TD style="WIDTH: 1001px" width="1001"><asp:label id="lblAddress1" runat="server" Width="303px" CssClass="tableLabel"></asp:label></TD>
						<TD style="WIDTH: 148px" align="left" width="148" bgColor="#008499">&nbsp;
							<asp:label id="Label47" tabIndex="100" runat="server" Height="13px" Width="83px" CssClass="tableLabel" ForeColor="White" BackColor="#008499" Font-Size="11px">Fax</asp:label></TD>
						<TD style="WIDTH: 8px" width="8"></TD>
						<TD style="WIDTH: 61px" width="61"><asp:label id="lblFax" runat="server" Width="133px" CssClass="tableLabel"></asp:label></TD>
					</TR>
					<TR height="25">
						<TD style="WIDTH: 96px" width="96" bgColor="#008499">&nbsp;
							<asp:label id="Label1" tabIndex="100" runat="server" Height="16px" Width="88px" CssClass="tableLabel" ForeColor="White" BackColor="#008499" Font-Size="11px">Address 2</asp:label></TD>
						<TD style="WIDTH: 9px" width="9"></TD>
						<TD style="WIDTH: 1001px" width="1001"><asp:label id="lblAddress2" runat="server" Width="301px" CssClass="tableLabel"></asp:label></TD>
						<TD style="WIDTH: 148px" width="148" bgColor="#008499">&nbsp;</TD>
						<TD style="WIDTH: 8px" align="left" width="8"></TD>
						<TD style="WIDTH: 61px" align="left" width="61">
							<asp:label id="lblHC_POD" runat="server" CssClass="tableLabel" Width="127px" Font-Size="11px"></asp:label></TD>
					</TR>
					<TR height="25">
						<TD style="WIDTH: 96px" vAlign="center" width="96" bgColor="#008499">&nbsp;
							<asp:label id="Label2" tabIndex="100" runat="server" Height="16px" Width="99px" CssClass="tableLabel" ForeColor="White" BackColor="#008499" Font-Size="11px">Postal Code</asp:label></TD>
						<TD style="WIDTH: 9px" width="9"></TD>
						<TD style="WIDTH: 1001px" width="1001"><asp:label id="lblPostalCode" runat="server" Width="143px" CssClass="tableLabel"></asp:label></TD>
						<TD style="WIDTH: 148px" width="148" bgColor="#008499">&nbsp;
							<asp:label id="Label46" tabIndex="100" runat="server" Height="16px" Width="108px" CssClass="tableLabel" ForeColor="White" BackColor="#008499" Font-Size="11px">HC POD Required</asp:label></TD>
						<TD style="WIDTH: 8px" align="left" width="8"></TD>
						<TD style="WIDTH: 61px" align="left" width="61"><asp:label id="lblHC_POD_Required" runat="server" Width="127px" CssClass="tableLabel" Font-Size="11px"></asp:label></TD>
					</TR>
				</TABLE>
				<asp:button id="btnSelectAll" style="Z-INDEX: 113; LEFT: 370px; POSITION: absolute; TOP: 37px" runat="server" Width="102px" CssClass="queryButton" Text="Select All" CausesValidation="False"></asp:button><asp:button id="btnCancel" style="Z-INDEX: 102; LEFT: 11px; POSITION: absolute; TOP: 37px" runat="server" Width="61px" CssClass="queryButton" Text="Cancel" CausesValidation="False"></asp:button><asp:button id="btnGenerate" style="Z-INDEX: 103; LEFT: 74px; POSITION: absolute; TOP: 37px" runat="server" Width="130px" CssClass="queryButton" Text="Generate" CausesValidation="False"></asp:button><asp:button id="btnSelectAllHC" style="Z-INDEX: 104; LEFT: 205px; POSITION: absolute; TOP: 37px" runat="server" Width="163px" CssClass="queryButton" Text="Select all HC Returned"></asp:button><asp:button id="btnGoToFirstPage" style="Z-INDEX: 105; LEFT: 597px; POSITION: absolute; TOP: 35px" runat="server" Width="24px" CssClass="queryButton" Text="|<" CausesValidation="False"></asp:button><asp:button id="btnPreviousPage" style="Z-INDEX: 106; LEFT: 622px; POSITION: absolute; TOP: 35px" runat="server" Width="24px" CssClass="queryButton" Text="<" CausesValidation="False"></asp:button><cc1:mstextbox id="txtGoToRec" style="Z-INDEX: 107; LEFT: 648px; POSITION: absolute; TOP: 37px" tabIndex="8" runat="server" Width="28px" CssClass="textFieldRightAlign" Wrap="False" AutoPostBack="True" NumberMaxValue="999999" NumberMinValue="1" NumberPrecision="8" NumberScale="0" TextMaskType="msNumeric" MaxLength="5" Enabled="True"></cc1:mstextbox><asp:button id="btnNextPage" style="Z-INDEX: 108; LEFT: 677px; POSITION: absolute; TOP: 36px" runat="server" Width="24px" CssClass="queryButton" Text=">" CausesValidation="False"></asp:button><asp:button id="btnGoToLastPage" style="Z-INDEX: 109; LEFT: 702px; POSITION: absolute; TOP: 36px" runat="server" Width="24px" CssClass="queryButton" Text=">|" CausesValidation="False"></asp:button><asp:label id="lblErrorMsg" style="Z-INDEX: 110; LEFT: 18px; POSITION: absolute; TOP: 62px" runat="server" Height="19px" Width="537px" CssClass="errorMsgColor"></asp:label><asp:label id="lblNumRec" style="Z-INDEX: 111; LEFT: 538px; POSITION: absolute; TOP: 60px" runat="server" Height="19px" Width="187px" CssClass="RecMsg"></asp:label><asp:validationsummary id="PageValidationSummary" style="Z-INDEX: 112; LEFT: 14px; POSITION: absolute; TOP: 62px" runat="server" Height="39px" Width="346px" ShowMessageBox="True" ShowSummary="False" Visible="True"></asp:validationsummary></FONT></form>
	</body>
</HTML>
