<%@ Page language="c#" Codebehind="PendingHCReturnQuery.aspx.cs" AutoEventWireup="false" Inherits="com.ties.PendingHCReturnQuery" %>
<%@ Import Namespace="com.common.util" %>
<%@ Import Namespace="Cambro.Web.DbCombo" %>
<%@ Register TagPrefix="dbCombo" Namespace="Cambro.Web.DbCombo" Assembly="Cambro.Web.DbCombo" %>
<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>ShipmentPendingPODReport</title>
		<meta content="False" name="vs_showGrid">
		<LINK href="css/Styles.css" type="text/css" rel="stylesheet" name="stylesheet">
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<!--#INCLUDE FILE="msFormValidations.inc"-->
	</HEAD>
	<body onunload="window.top.displayBanner.fnCloseAll(0);" MS_POSITIONING="GridLayout">
		<form id="PendingHCReport" method="post" runat="server">
			<asp:button id="btnQuery" style="Z-INDEX: 101; LEFT: 21px; POSITION: absolute; TOP: 39px" runat="server" Width="64px" Text="Query" CssClass="queryButton"></asp:button><asp:label id="lblErrorMessage" style="Z-INDEX: 105; LEFT: 23px; POSITION: absolute; TOP: 64px" runat="server" Width="589px" CssClass="errorMsgColor" Height="27px">Place holder for err msg</asp:label>&nbsp;
			<asp:button id="btnGenerate" style="Z-INDEX: 102; LEFT: 86px; POSITION: absolute; TOP: 39px" runat="server" Width="82px" Text="Generate" CssClass="queryButton"></asp:button><asp:label id="lblTitle" style="Z-INDEX: 103; LEFT: 20px; POSITION: absolute; TOP: 3px" runat="server" Width="514px" CssClass="maintitleSize" Height="35px">Pending HC Return Report</asp:label>
			<TABLE id="tblShipmentTracking" style="Z-INDEX: 104; LEFT: 15px; WIDTH: 818px; POSITION: absolute; TOP: 92px; HEIGHT: 306px" border="0" runat="server">
				<TR>
					<TD style="WIDTH: 470px; HEIGHT: 27px" vAlign="top"><FONT face="Tahoma">
							<asp:RadioButton id="rbHCtoHub" runat="server" CssClass="tableRadioButton" Text="Pending Hardcopy to Hub" Checked="True" GroupName="HCReport" Width="271px"></asp:RadioButton>
							<asp:RadioButton id="rbHCtoCustomer" runat="server" CssClass="tableRadioButton" Text="Pending INVR to Customer" GroupName="HCReport" Width="191px"></asp:RadioButton></FONT></TD>
					<TD style="HEIGHT: 27px" vAlign="top"></TD>
				</TR>
				<TR>
					<TD style="WIDTH: 470px; HEIGHT: 137px" vAlign="top">
						<fieldset style="WIDTH: 453px; HEIGHT: 129px"><legend><asp:label id="lblDates" runat="server" CssClass="tableHeadingFieldset" Width="173px" Font-Bold="True">Estimated HC Return Date</asp:label></legend>
							<TABLE id="Table1" style="WIDTH: 360px; HEIGHT: 91px" cellSpacing="0" cellPadding="0" align="left" border="0" runat="server">
								<TR>
									<td></td>
									<TD colSpan="3">&nbsp;
										<asp:radiobutton id="rbMonth" runat="server" Text="Month" CssClass="tableRadioButton" Width="73px" Height="21px" AutoPostBack="True" GroupName="EnterDate" Checked="True"></asp:radiobutton>&nbsp;
										<asp:dropdownlist id="ddMonth" runat="server" CssClass="textField" Width="88px" Height="19px"></asp:dropdownlist>&nbsp;
										<asp:label id="lblYear" runat="server" CssClass="tableLabel" Width="30px" Height="22px">Year</asp:label>&nbsp;
										<cc1:mstextbox id="txtYear" runat="server" CssClass="textField" Width="83" MaxLength="5" TextMaskType="msNumeric" NumberMaxValue="2999" NumberMinValue="1" NumberPrecision="4"></cc1:mstextbox></TD>
									<td></td>
								</TR>
								<TR>
									<td style="HEIGHT: 20px"></td>
									<TD style="HEIGHT: 20px" colSpan="3">&nbsp;
										<asp:radiobutton id="rbPeriod" runat="server" Text="Period" CssClass="tableRadioButton" Width="62px" AutoPostBack="True" GroupName="EnterDate"></asp:radiobutton>&nbsp;&nbsp;&nbsp;
										<cc1:mstextbox id="txtPeriod" runat="server" CssClass="textField" Width="82" MaxLength="10" TextMaskType="msDate" TextMaskString="99/99/9999"></cc1:mstextbox>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
										&nbsp;
										<cc1:mstextbox id="txtTo" runat="server" CssClass="textField" Width="83" MaxLength="10" TextMaskType="msDate" TextMaskString="99/99/9999"></cc1:mstextbox></TD>
									<td style="HEIGHT: 20px"></td>
								</TR>
								<TR>
									<td></td>
									<TD colSpan="3">&nbsp;
										<asp:radiobutton id="rbDate" runat="server" Text="Date" CssClass="tableRadioButton" Width="74px" Height="22px" AutoPostBack="True" GroupName="EnterDate"></asp:radiobutton>&nbsp;&nbsp;<cc1:mstextbox id="txtDate" runat="server" CssClass="textField" Width="82" MaxLength="10" TextMaskType="msDate" TextMaskString="99/99/9999"></cc1:mstextbox></TD>
									<td></td>
								</TR>
							</TABLE>
						</fieldset>
					</TD>
					<TD style="HEIGHT: 137px" vAlign="top">
						<fieldset style="WIDTH: 320px; HEIGHT: 105px"><legend><asp:label id="lblPayerType" runat="server" CssClass="tableHeadingFieldset" Width="79px" Font-Bold="True">Payer Type</asp:label></legend>
							<TABLE id="tblPayerType" style="WIDTH: 300px; HEIGHT: 73px" cellSpacing="0" cellPadding="0" align="left" border="0" runat="server">
								<tr>
									<td></td>
									<td colSpan="2" height="1">&nbsp;</td>
									<td></td>
									<td></td>
								</tr>
								<TR>
									<td></td>
									<TD class="tableLabel" vAlign="top" colSpan="2">&nbsp;
										<asp:label id="Label4" runat="server" CssClass="tableLabel" Width="87px" Height="22px">Payer Type</asp:label></TD>
									<td><asp:listbox id="lsbCustType" runat="server" Width="137px" Height="61px" SelectionMode="Multiple"></asp:listbox></td>
									<td></td>
								</TR>
								<TR height="33">
									<td bgColor="blue"></td>
									<TD class="tableLabel" vAlign="top" colSpan="2">&nbsp;
										<asp:label id="lblPayerCode" runat="server" CssClass="tableLabel" Width="79px" Height="22px">Payer Code</asp:label>&nbsp;&nbsp;</TD>
									<td><asp:TextBox id="txtPayerCode" runat="server" Width="148px" CssClass="textField"></asp:TextBox>&nbsp;&nbsp;
										<asp:button id="btnPayerCode" runat="server" Text="..." CssClass="searchButton" CausesValidation="False"></asp:button></td>
									<td></td>
								</TR>
							</TABLE>
						</fieldset>
					</TD>
				</TR>
				<TR>
					<TD style="WIDTH: 470px; HEIGHT: 134px" vAlign="top">
						<fieldset style="WIDTH: 451px; HEIGHT: 127px"><legend><asp:label id="lblRouteType" runat="server" CssClass="tableHeadingFieldset" Width="145px" Font-Bold="True">Route / DC Selection</asp:label></legend>
							<TABLE id="tblRouteType" style="WIDTH: 444px; HEIGHT: 113px" cellSpacing="0" cellPadding="0" align="left" border="0" runat="server">
								<TR>
									<TD class="tableLabel" colSpan="2">&nbsp;
										<asp:radiobutton id="rbLongRoute" runat="server" Text="Linehaul" CssClass="tableRadioButton" Width="143px" Height="22px" AutoPostBack="True" GroupName="QueryByRoute" Checked="True"></asp:radiobutton><asp:radiobutton id="rbShortRoute" runat="server" Text="Delivery Route" CssClass="tableRadioButton" Width="126px" Height="22px" AutoPostBack="True" GroupName="QueryByRoute"></asp:radiobutton><asp:radiobutton id="rbAirRoute" runat="server" Text="Air Route" CssClass="tableRadioButton" Width="158px" Height="22px" AutoPostBack="True" GroupName="QueryByRoute"></asp:radiobutton></TD>
								</TR>
								<tr>
									<TD class="tableLabel" width="20%">&nbsp;<asp:label id="lblRouteCode" runat="server" CssClass="tableLabel" Width="111px" Height="22px">Route Code</asp:label></TD>
									<td><DBCOMBO:DBCOMBO id="DbComboPathCode" tabIndex="14" CssClass="tableLabel" Width="25px" AutoPostBack="True" Runat="server" TextBoxColumns="18" RegistrationKey=" " TextUpLevelSearchButton="v" ShowDbComboLink="False" ServerMethod="DbComboPathCodeSelect"></DBCOMBO:DBCOMBO></td>
								</tr>
								<TR>
									<TD class="tableLabel">&nbsp;<asp:label id="Label2" runat="server" CssClass="tableLabel" Width="154px" Height="22px">Origin Distribution Center</asp:label></TD>
									<TD><DBCOMBO:DBCOMBO id="DbComboOriginDC" tabIndex="14" CssClass="tableLabel" Width="25px" AutoPostBack="True" Runat="server" TextBoxColumns="18" RegistrationKey=" " TextUpLevelSearchButton="v" ShowDbComboLink="False" ServerMethod="DbComboDistributionCenterSelect"></DBCOMBO:DBCOMBO></TD>
								</TR>
								<TR>
									<TD class="tableLabel">&nbsp;<asp:label id="Label3" runat="server" CssClass="tableLabel" Width="181px" Height="22px">Destination Distribution Center</asp:label></TD>
									<TD><DBCOMBO:DBCOMBO id="DbComboDestinationDC" tabIndex="14" CssClass="tableLabel" Width="25px" AutoPostBack="True" Runat="server" TextBoxColumns="18" RegistrationKey=" " TextUpLevelSearchButton="v" ShowDbComboLink="False" ServerMethod="DbComboDistributionCenterSelect"></DBCOMBO:DBCOMBO></TD>
								</TR>
							</TABLE>
						</fieldset>
					</TD>
					<TD style="HEIGHT: 134px" vAlign="top">
						<fieldset style="WIDTH: 320px; HEIGHT: 89px"><legend><asp:label id="lblDestination" runat="server" CssClass="tableHeadingFieldset" Width="79px" Font-Bold="True">Destination</asp:label></legend>
							<TABLE id="tblDestination" style="WIDTH: 300px; HEIGHT: 89px" cellSpacing="0" cellPadding="0" align="left" border="0" runat="server">
								<tr height="37">
									<td></td>
									<TD class="tableLabel" style="WIDTH: 360px" colSpan="3">&nbsp;
										<asp:label id="lblZipCode" runat="server" CssClass="tableLabel" Width="84px" Height="22px">Postal Code</asp:label>&nbsp;&nbsp;&nbsp;
										<cc1:mstextbox id="txtZipCode" runat="server" CssClass="textField" Width="139px" MaxLength="10" TextMaskType="msUpperAlfaNumericWithUnderscore"></cc1:mstextbox>&nbsp;&nbsp;
										<asp:button id="btnZipCode" runat="server" Text="..." CssClass="searchButton" CausesValidation="False"></asp:button></TD>
									<td></td>
								</tr>
								<tr>
									<td></td>
									<TD class="tableLabel" style="WIDTH: 360px" colSpan="3">&nbsp;
										<asp:label id="lblStateCode" runat="server" CssClass="tableLabel" Width="100px" Height="22px">State / Province</asp:label><cc1:mstextbox id="txtStateCode" runat="server" CssClass="textField" Width="139" Height="22" MaxLength="10" TextMaskType="msUpperAlfaNumericWithUnderscore"></cc1:mstextbox>&nbsp;&nbsp;
										<asp:button id="btnStateCode" runat="server" Text="..." CssClass="searchButton" CausesValidation="False"></asp:button></TD>
									<td></td>
								</tr>
							</TABLE>
						</fieldset>
					</TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>
