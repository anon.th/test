using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.common.util;
using com.ties.classes;
using com.ties.DAL;
using com.ties.BAL;
using com.common.classes;
using Cambro.Web.DbCombo;
using System.Configuration;
using com.common.applicationpages;


namespace com.ties
{
	/// <summary>
	/// Summary description for DeliveryManifestNew.
	/// </summary>
	public class DeliveryManifestNew : com.common.applicationpages.BasePage
	{
		protected System.Web.UI.WebControls.Button btnQry;
		protected System.Web.UI.WebControls.Button btnSave;
		protected System.Web.UI.WebControls.Button btnInsert;
		protected System.Web.UI.WebControls.Button btnExecQry;		
		protected System.Web.UI.WebControls.Label lblMainTitle;
		protected System.Web.UI.WebControls.Label lblErrorMsg;

				
		//		Utility utility = null;
		String appID = null;
		String enterpriseID = null;
		String deliveryType="L",sPathCode=null, sFlightVehNo=null, sManifestDateTime=null;
		String sDepDateTime=null, sAWBDriverName=null, sLineHaulCost =null;
		bool bTextChanged=false;
		DataSet dsPrint=null;
		
		protected com.common.util.msTextBox txtVehicle_Flight_No;
		//protected System.Web.UI.WebControls.TextBox txtDriver_Flight_AWB;
		protected System.Web.UI.WebControls.TextBox txtDriver_Flight_AWB;
		protected System.Web.UI.WebControls.RequiredFieldValidator reqDeliveryPathCode;
		protected System.Web.UI.WebControls.RequiredFieldValidator reqFlightVehicleNo;		
		protected System.Web.UI.WebControls.RequiredFieldValidator reqDepartDate;
		protected System.Web.UI.HtmlControls.HtmlTable DeliveryManifestTable;
		protected System.Web.UI.WebControls.Button btnRetrieveConsignment;
		protected System.Web.UI.WebControls.Button btnDeliveryCodeSearch;			
		protected System.Web.UI.WebControls.Label lblVehicle_Flight_No;	
		protected System.Web.UI.WebControls.Label lblDriver_Flight_AWB;	
		protected System.Web.UI.WebControls.Label lblDeliveryPathCode;
		protected com.common.util.msTextBox txtLineHaulCost;
		protected System.Web.UI.WebControls.Label lblLineHaulCost;
		protected System.Web.UI.WebControls.RadioButton rbtnShort;
		protected System.Web.UI.WebControls.RadioButton rbtnLong;		
		protected System.Web.UI.WebControls.Button btnRemoveAll;
		protected System.Web.UI.WebControls.RadioButton rbtnAir;
		protected System.Web.UI.WebControls.Label lblDepartDate;		
		protected System.Web.UI.WebControls.Button btnRemove;
		protected System.Web.UI.WebControls.Button btnAddAll;		
		protected System.Web.UI.WebControls.Button btnAdd;		
		protected System.Web.UI.WebControls.Button btnPrint;		
		String userID = null;
		static int m_iSetSize = 10;
		protected System.Web.UI.WebControls.Label lblDelManifestDate;
		protected com.common.util.msTextBox txtDelManifestDate;
		DataSet m_sdsDeliveryManifest = null;	
		DataSet m_sdsAvailConsignment=null;
		DataSet m_sdsAssignedConsignment=null;
		SessionDS m_sdsDMList=null;
		protected System.Web.UI.WebControls.RequiredFieldValidator validDlvryPath;
		protected System.Web.UI.WebControls.Label lblDlvryPath;
		protected System.Web.UI.WebControls.Label lblAssigned;
		protected System.Web.UI.WebControls.TextBox txtPathDesc;
		protected System.Web.UI.WebControls.TextBox txtDelvryType;
		protected System.Web.UI.WebControls.Panel pnlAssigned;
		protected System.Web.UI.WebControls.Panel pnlAvailable;
		protected System.Web.UI.WebControls.RequiredFieldValidator reqLineHaulCost;
		protected System.Web.UI.WebControls.RequiredFieldValidator reqDriverFlightAWB;
		protected System.Web.UI.WebControls.RequiredFieldValidator reqDepartureDate;
		protected System.Web.UI.HtmlControls.HtmlGenericControl gridPanel;
		protected System.Web.UI.WebControls.Label lblMessage;
		protected System.Web.UI.WebControls.ValidationSummary PageValidationSummary;
		protected System.Web.UI.HtmlControls.HtmlGenericControl msgPanel;
		protected System.Web.UI.WebControls.Label lblTitle;
		protected System.Web.UI.WebControls.Button btnToSaveChanges;
		protected System.Web.UI.WebControls.Button btnNotToSave;
		protected System.Web.UI.WebControls.Button btnNO;
		protected System.Web.UI.WebControls.DataGrid dgAvailConsignment;
		protected System.Web.UI.WebControls.DataGrid dgAssignedConsignment;
		protected Cambro.Web.DbCombo.DbCombo DbComboPathCode;
		protected Cambro.Web.DbCombo.DbCombo dbDepartDate;
		protected System.Web.UI.WebControls.Label lblAvailable;

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			base.OnInit(e);	
			InitializeComponent();							
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnQry.Click += new System.EventHandler(this.btnQry_Click);
			this.btnExecQry.Click += new System.EventHandler(this.btnExecQry_Click);
			this.btnInsert.Click += new System.EventHandler(this.btnInsert_Click);
			this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
			this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
			this.rbtnLong.CheckedChanged += new System.EventHandler(this.rbtnLong_CheckedChanged);
			this.rbtnShort.CheckedChanged += new System.EventHandler(this.rbtnShort_CheckedChanged);
			this.rbtnAir.CheckedChanged += new System.EventHandler(this.rbtnAir_CheckedChanged);
			this.DbComboPathCode.SelectedItemChanged += new System.EventHandler(this.DbComboPathCode_OnSelectedIndexChanged);
			this.txtDriver_Flight_AWB.TextChanged += new System.EventHandler(this.txtDriver_Flight_AWB_TextChanged);
			this.dbDepartDate.SelectedItemChanged += new System.EventHandler(this.dbDepartDate_SelectedItemChanged);
			this.btnRetrieveConsignment.Click += new System.EventHandler(this.btnRetrieveConsignment_Click);
			this.dgAvailConsignment.SelectedIndexChanged += new System.EventHandler(this.dgAvailConsignment_SelectedIndexChanged);
			this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
			this.btnRemove.Click += new System.EventHandler(this.btnRemove_Click);
			this.btnAddAll.Click += new System.EventHandler(this.btnAddAll_Click);
			this.btnRemoveAll.Click += new System.EventHandler(this.btnRemoveAll_Click);
			this.dgAssignedConsignment.SelectedIndexChanged += new System.EventHandler(this.dgAssignedConsignment_SelectedIndexChanged);
			this.btnToSaveChanges.Click += new System.EventHandler(this.btnToSaveChanges_Click);
			this.btnNotToSave.Click += new System.EventHandler(this.btnNotToSave_Click);
			this.btnNO.Click += new System.EventHandler(this.btnNO_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion 

		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			///Added regkey here
			DbComboPathCode.RegistrationKey = System.Configuration.ConfigurationSettings.AppSettings["DbComboRegKey"];
			dbDepartDate.RegistrationKey = System.Configuration.ConfigurationSettings.AppSettings["DbComboRegKey"];
			//this.DbComboOriState.RegistrationKey = System.Configuration.ConfigurationSettings.AppSettings["DbComboRegKey"];
			///Added regKey here			
			//utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings,Page.Session);
			appID = utility.GetAppID();
			enterpriseID = utility.GetEnterpriseID();
			userID = utility.GetUserID();

			if (!IsPostBack)
			{
				//Initialize the Error message to Vehicle No
				reqFlightVehicleNo.ErrorMessage=lblVehicle_Flight_No.Text;
				reqDriverFlightAWB.ErrorMessage=lblDriver_Flight_AWB.Text;				
				//Reset Buttons and get Empty Delivery Manifest
				ResetForQuery();
				msgPanel.Style["display"]="none";
				ViewState["IsTextChanged"]=bTextChanged;
				m_sdsAvailConsignment=DeliveryManifestMgrDAL.GetEmptyAvailSessionDS();
				m_sdsAvailConsignment.Tables[0].Rows.Clear();				
				m_sdsAssignedConsignment=DeliveryManifestMgrDAL.GetEmptyAssignedSessionDS();
				m_sdsAssignedConsignment.Tables[0].Rows.Clear();
				BindAvailConsignment();
				BindAssignedConsignment();
			}
			else
			{
				if (Session["SESSION_DS1"] !=null)
				{
					m_sdsDeliveryManifest = (DataSet) Session["SESSION_DS1"];
				}
				if(Session["SESSION_DS2"] != null)
				{
					m_sdsAvailConsignment = (DataSet)Session["SESSION_DS2"];
				}								
				if(Session["SESSION_DS3"] != null)
				{
					m_sdsAssignedConsignment = (DataSet)Session["SESSION_DS3"];
				}			
	
				bTextChanged=(bool)ViewState["IsTextChanged"];
			}						
			if (Request.Params["REFRESH"] != null)
			{
				if (Request.Params["REFRESH"].ToString().Equals("YES"))
				{
					if (Session["ASSIGNED"] != null)
					{
						String checkAssigned = (String) Session["ASSIGNED"];
						if (checkAssigned.Equals("TRUE"))
						{
							GetValuesIntoDS();
							m_sdsAssignedConsignment = (DataSet)DeliveryManifestMgrDAL.GetAssignedConsignmentList(appID,enterpriseID,0,0,m_sdsDeliveryManifest);
							Session["SESSION_DS3"] = m_sdsAssignedConsignment;
							BindAssignedConsignment();
							Session["ASSIGNED"] = "FALSE";
						}
					}
				}
			}
			SetDbComboServerStates();
			SetdbManifestDateStates();
			PageValidationSummary.HeaderText = Utility.GetLanguageText(ResourceType.UserMessage, "PLS_ENTER_MISS_FIELD", utility.GetUserCulture());
		}

		private void SetDbComboServerStates()
		{
			String strDeliveryType=null;
			if (rbtnLong.Checked==true)
				strDeliveryType="L";
			else if (rbtnShort.Checked==true)
				strDeliveryType="S";
			else if (rbtnAir.Checked==true)
				strDeliveryType="A";

			Hashtable hash = new Hashtable();
			hash.Add("strDeliveryType", strDeliveryType);
			DbComboPathCode.ServerState = hash;
			DbComboPathCode.ServerStateSecretString = "kdfsjgnskajh[apq2-4uhg465654435879mnbfdm cxs;q4";
		}

		private void SetdbManifestDateStates()
		{			
			String strDeliveryPath="";			
			String strFlightVehicle="";
			String strDeliveryPath1="";			
			String strFlightVehicle1="";
			//query			
			strDeliveryPath=DbComboPathCode.Value;	
			Hashtable hash = new Hashtable();
			if (strDeliveryPath !=null && strDeliveryPath !="")
			{
				hash.Add("strDeliveryPath", strDeliveryPath);
			}
			if (strFlightVehicle != null && strFlightVehicle !="")
			{
				hash.Add("strFlightVehicle", strFlightVehicle);
			}
			dbDepartDate.ServerState = hash;
			dbDepartDate.ServerStateSecretString = "kdfsjgnskajh[apq2-4uhg465654435879mnbfdm cxs;q4";
			Hashtable hash1 = new Hashtable();
			if (strDeliveryPath1 !=null && strDeliveryPath1 !="")
			{
				hash1.Add("strDeliveryPath", strDeliveryPath1);
			}
			if (strFlightVehicle1 != null && strFlightVehicle1 !="")
			{
				hash1.Add("strFlightVehicle", strFlightVehicle1);
			}
		}

		private void ResetForQuery()
		{		
			//Initialize ViewState				
			ViewState["DMMode"] = ScreenMode.Query;
			ViewState["DMOperation"] = Operation.None;
			ViewState["CurrentSetSize"]=0;
			ViewState["currConsSet"] = 0;
			ViewState["Operations"] = Operation.None;												
			Session["SESSION_DS1"] = m_sdsDeliveryManifest;		
			ViewState["NextOperation"] = "None";

			SessionDS sessionDS = new SessionDS();
			sessionDS=DeliveryManifestMgrDAL.GetDelManifestSessionDS();
			//m_sdsDeliveryManifest = DeliveryManifestMgrDAL.GetDelManifestSessionDS();
			m_sdsDeliveryManifest=sessionDS.ds;
			Session["SESSION_DS1"] = m_sdsDeliveryManifest;				
			Utility.EnableButton(ref btnInsert,ButtonType.Insert,true,m_moduleAccessRights);
			btnQry.Enabled = true;	
			Utility.EnableButton(ref btnSave,ButtonType.Save,false,m_moduleAccessRights);
			btnPrint.Enabled=false;
			btnExecQry.Enabled = true;			

			btnRemove.Enabled = false;
			btnAdd.Enabled = false;
			btnRemoveAll.Enabled = false;
			btnAddAll.Enabled = false;
			rbtnLong.Enabled=true;rbtnShort.Enabled=true;rbtnAir.Enabled=true;
			ViewState["IsTextChanged"] = false;
		}

		private bool AnyTextBoxChanged()
		{
			bool isChanged=false;
			if (DbComboPathCode.Value!="" || DbComboPathCode.Text !="")
				isChanged=true;
			if (txtDriver_Flight_AWB.Text.Trim() !="")			
				isChanged=true;							
			if (txtVehicle_Flight_No.Text.Trim() !="")			
				isChanged=true;							
			if (txtLineHaulCost.Text.Trim() !="")			
				isChanged=true;							
			if (dbDepartDate.Value!="" || dbDepartDate.Text !="")			
				isChanged=true;							
			if (txtDelManifestDate.Text.Trim() !="")
			{
				isChanged=true;	
			}
		
			ViewState["IsTextChanged"] = isChanged;
			return isChanged;
		}

		private void InsertMode()
		{
			if ((bool)ViewState["IsTextChanged"]==true)
			{
				ViewState["DMOperation"]=Operation.Insert;				
				lblMessage.Text=Utility.GetLanguageText(ResourceType.UserMessage,"SAVE_PROMPT",utility.GetUserCulture());
				msgPanel.Style["display"]="inline";
				gridPanel.Style["display"]="none";
				//				msgPanel.Visible=true;				
				//				gridPanel.Visible=false;
				ViewState["NextOperation"]="Insert";
				return;
			}
			else
			{
				ClearAllFields();
				ClearConsignmentList();
			}

			ViewState["DMMode"]=ScreenMode.Insert;
			ViewState["DMOperation"]=Operation.Insert;						
			String strDt = DateTime.Now.ToString("dd/MM/yyyy HH:mm");
			txtDelManifestDate.Text=strDt;
			dbDepartDate.Text=strDt;
			Utility.EnableButton(ref btnSave,ButtonType.Save,true,m_moduleAccessRights);			
			btnExecQry.Enabled=false;
			btnSave.Enabled=true;
			ViewState["NextOperation"]="Insert";
			ViewState["IsTextChanged"]=true;
		}

		private void btnInsert_Click(object sender, System.EventArgs e)
		{
			lblMessage.Text="";
			InsertMode();
			SetDbComboServerStates();
		}

		private void ClearAllFields()
		{
			DbComboPathCode.Text="";
			DbComboPathCode.Value="";
			txtPathDesc.Text="";
			dbDepartDate.Text="";
			dbDepartDate.Value="";
			txtDelManifestDate.Text="";
			txtDriver_Flight_AWB.Text="";
			txtLineHaulCost.Text="";
			txtVehicle_Flight_No.Text="";
			if (rbtnShort.Checked==true)
			{
				rbtnShort.Checked=false;
			}
			if (rbtnAir.Checked==true)
			{
				rbtnAir.Checked=false;
			}
			rbtnLong.Checked=true;
			deliveryType="L";
			ClearDBComboPathCode();
			//lblVehicle_Flight_No.Text="Vehicle No";
			//lblDriver_Flight_AWB.Text="Driver Name";
			if(!utility.GetUserCulture().ToUpper().Equals("EN-US"))
			{
				lblVehicle_Flight_No.Text = Utility.GetLanguageText(ResourceType.ScreenLabel, "Vehicle No", utility.GetUserCulture());
				lblDriver_Flight_AWB.Text = Utility.GetLanguageText(ResourceType.ScreenLabel, "Driver Name", utility.GetUserCulture());
			}
			else
			{
				lblVehicle_Flight_No.Text = "Vehicle No";
				lblDriver_Flight_AWB.Text = "Driver Name";
			}			
			reqFlightVehicleNo.ErrorMessage=lblVehicle_Flight_No.Text;
			reqDriverFlightAWB.ErrorMessage=lblDriver_Flight_AWB.Text;
			btnRetrieveConsignment.Enabled=false;
			//this.chkOriginalState.Enabled = false;
			//this.DbComboOriState.Enabled = false;
		}

		private void GetValues()
		{
			if (DbComboPathCode.Value.Trim() != "")
			{
				sPathCode=DbComboPathCode.Value.Trim();
			}
			if (txtVehicle_Flight_No.Text.Trim() != "") 
			{
				sFlightVehNo=txtVehicle_Flight_No.Text.Trim();
			}
			if (txtDriver_Flight_AWB.Text.Trim() != "" )
			{
				sAWBDriverName=txtDriver_Flight_AWB.Text.Trim();
			}
			if (txtLineHaulCost.Text.Trim() != "" )
			{
				sLineHaulCost=txtLineHaulCost.Text.Trim();
			}
			if (dbDepartDate.Value.Trim().Length > 0 ) 
			{
				sDepDateTime=dbDepartDate.Value.Trim();
			}
			if (txtDelManifestDate.Text.Trim().Length > 0 ) 
			{
				sManifestDateTime=txtDelManifestDate.Text.Trim();
			}
			if (rbtnLong.Checked==true)
			{				
				deliveryType="L";
			}
			else if (rbtnShort.Checked==true)
			{				
				deliveryType="S";
			}
			else if (rbtnAir.Checked==true)
			{			
				deliveryType="A";
			}
		}

		private void GetValuesIntoDS()
		{								
			m_sdsDeliveryManifest = DeliveryManifestMgrDAL.GetDelManifestSessionDS().ds;
			DataRow drEach = m_sdsDeliveryManifest.Tables[0].Rows[0];

			if (DbComboPathCode.Value.Trim() != "")
			{
				drEach["path_code"] = DbComboPathCode.Value.Trim();	
			}			
			if (txtVehicle_Flight_No.Text.Trim() != "") 
			{ 
				drEach["flight_vehicle_no"]=txtVehicle_Flight_No.Text.Trim();	
			}
			if (txtDriver_Flight_AWB.Text.Trim() != "" )
			{
				drEach["awb_driver_name"]=txtDriver_Flight_AWB.Text.Trim();
			}
			if (txtLineHaulCost.Text.Trim() != "" )
			{
				drEach["line_haul_cost"]=Convert.ToDecimal(txtLineHaulCost.Text.Trim());
			}
			if (dbDepartDate.Value.Trim().Length > 0 ) 
			{
				drEach["departure_datetime"]=System.DateTime.ParseExact(dbDepartDate.Value.Trim(),"dd/MM/yyyy HH:mm",null);				
			}	
			if (txtDelManifestDate.Text.Trim().Length > 0 ) 
			{
				drEach["delivery_manifest_datetime"]=System.DateTime.ParseExact(txtDelManifestDate.Text.Trim(),"dd/MM/yyyy HH:mm",null);
			}		
			if (rbtnLong.Checked==true)
			{
				drEach["delivery_type"] = "L";				
			}
			else if (rbtnShort.Checked==true)
			{
				drEach["delivery_type"] = "S";				
			}
			else if (rbtnAir.Checked==true)
			{
				drEach["delivery_type"] = "A";				
			}			
		}
				
		private void ExecuteQueryDS()
		{			
			//GetValuesIntoDS();
			GetValues();
			ViewState["QUERY_DS"] = m_sdsDeliveryManifest;
			ViewState["DMMode"] = ScreenMode.ExecuteQuery;
			ViewState["DMOperation"] = Operation.None;
			ViewState["IsTextChanged"] = false;
			
			ViewState["currentSet"] = 0;
			
			FetchDMRecSet();
					
			//			Call the popup window.
			String DbComboID=null;
			//			DbComboID=DbComboPathCode.ClientID;
			DbComboID="DbComboPathCode_ulbTextBox";
			//DbComboID = "DbComboPathCode";
			//			DbComboID=DbComboPathCode.Controls[2].ClientID;			

			String	sUrl  = "DeliveryManifestList.aspx?PATH_CODEID="+DbComboID+"&DELIVERY_TYPE="+deliveryType;
			sUrl += "&PATH_CODE="+sPathCode+"&FLIGHT_VEHICLENO="+sFlightVehNo+"&DEPARTURE_DATE="+sDepDateTime;
			sUrl += "&MANIFEST_DATE="+sManifestDateTime+"&AWB_DRIVERNAME="+sAWBDriverName+"&LINE_HAULCOST="+sLineHaulCost;
			
			ArrayList paramList = new ArrayList();
			paramList.Add(sUrl);
			String sScript = Utility.GetScript("openWindowParam.js",paramList);
			Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
		
		}

		private void FetchDMRecSet()
		{
			int iStartIndex = (int)ViewState["currentSet"] * m_iSetSize;
			DataSet dsQuery = (DataSet)ViewState["QUERY_DS"];		
			
			//m_sdsDMList = DeliveryManifestMgrDAL.GetDMListDS(appID,enterpriseID,iStartIndex,m_iSetSize,dsQuery);
			m_sdsDMList = DeliveryManifestMgrDAL.GetDMListDS(appID,enterpriseID,iStartIndex,m_iSetSize, deliveryType, sPathCode, sFlightVehNo, sDepDateTime, sManifestDateTime, sAWBDriverName, sLineHaulCost);			

			Session["SESSION_DS4"] = m_sdsDMList;
			Session["SESSION_DS5"]=dsQuery; //CHECK HERE
			decimal pgCnt = (m_sdsDMList.QueryResultMaxSize - 1)/m_iSetSize;
			if(pgCnt < (int)ViewState["currentSet"])
			{
				ViewState["currentSet"] = System.Convert.ToInt32(pgCnt);
			}
		}

		private void ExecuteQueryMode()
		{
			btnPrint.Enabled=false;
			ViewState["DMMode"]=ScreenMode.ExecuteQuery;
			ViewState["DMOperation"] = Operation.None;
			btnExecQry.Enabled = true;  //set of Operation buttons
			btnQry.Enabled = true;
			
			Utility.EnableButton(ref btnInsert,ButtonType.Insert,true,m_moduleAccessRights);
			Utility.EnableButton(ref btnSave,ButtonType.Save,false,m_moduleAccessRights);			

			ExecuteQueryDS();						
			btnRetrieveConsignment.Enabled=true;
			//this.chkOriginalState.Enabled = true;
			//this.DbComboOriState.Enabled = true;
			txtVehicle_Flight_No.ReadOnly=true;
			txtDelManifestDate.ReadOnly=true;

			ViewState["NextOperation"] = "ExecuteQuery";
			btnSave.Enabled=true;

			//if(!Utility.IsDeliveryManifestCountCorrected(this.appID,this.enterpriseID,this.sPathCode,this.sFlightVehNo,this.sManifestDateTime))
			//{
			//this.btnSave.Enabled=false;
			//this.btnPrint.Enabled=false;
			//}
		}

		private void btnExecQry_Click(object sender, System.EventArgs e)
		{
			lblErrorMsg.Text="";
			bool icheck = ValidateValues();
			if(rbtnLong.Checked)
				Session["FORMID"] = "DeliveryManifestListLH";
			else if(rbtnShort.Checked || rbtnAir.Checked)
				Session["FORMID"] = "DeliveryManifestList";
			if(icheck == false)
			{

				dsPrint=GetPrintDS();
				Session["SESSION_DS1"] = dsPrint;

				String strUrl = null;
				strUrl = "ReportViewer.aspx";
				OpenWindowpage(strUrl);
			}
			
		}
		
		private bool ValidateValues()
		{
			bool iCheck=false;
	
			if(DbComboPathCode.Value.ToString()=="" && dbDepartDate.Value.ToString()=="")
			{					
				lblErrorMsg.Text="Please Enter Select Delivery Path Code and Departure Date";	
				return iCheck=true;				
			}
			else if(DbComboPathCode.Value.ToString()=="" && dbDepartDate.Value.ToString()!="")
			{					
				lblErrorMsg.Text="Please Select Delivery Path Code";	
				return iCheck=true;				
			}
			else if(DbComboPathCode.Value.ToString()!="" && dbDepartDate.Value.ToString()=="")
			{					
				lblErrorMsg.Text="Please Select Departure Date";	
				return iCheck=true;				
			}

			return iCheck;
			
		}

		private DataSet GetPrintDS()
		{
			
			DataSet dsShipment = new DataSet();
			DataTable dtShipment = CreateEmptyDataTable();
			DataRow dr = dtShipment.NewRow(); 
			String DelType = "";
			
			if(rbtnLong.Checked)
				DelType = "L";
			if(rbtnShort.Checked)
				DelType = "S";
			if(rbtnAir.Checked)
				DelType = "A";


			dr["delivery_type"] = DelType;
			dr["path_code"] = DbComboPathCode.Value;
			dr["flight_vehicle_no"] = txtVehicle_Flight_No.Text;
			dr["delivery_manifest_dateTime"] = System.DateTime.ParseExact(txtDelManifestDate.Text,"dd/MM/yyyy HH:mm",null);

			dtShipment.Rows.Add(dr);
			dsShipment.Tables.Add(dtShipment);
			return dsShipment;
		}

		private DataTable CreateEmptyDataTable()
		{
			DataTable dtShipment = new DataTable();

			dtShipment.Columns.Add(new DataColumn("delivery_type", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("path_code", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("flight_vehicle_no", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("delivery_manifest_dateTime", typeof(DateTime)));	

			return dtShipment;
		}


		private void ClearDBComboPathCode()
		{
			DbComboPathCode.Text="";
			DbComboPathCode.Value="";
			txtPathDesc.Text="";
			txtVehicle_Flight_No.Text="";
			txtDriver_Flight_AWB.Text="";	
			if ((int) ViewState["DMOperation"] != (int)Operation.Insert)
			{
				txtDelManifestDate.Text="";
				dbDepartDate.Text="";
				dbDepartDate.Value="";
			}
			txtLineHaulCost.Text="";
			lblErrorMsg.Text =""; 
			m_sdsAvailConsignment.Tables[0].Rows.Clear();			//if it is NOT refreshed
			BindAvailConsignment();
			m_sdsAssignedConsignment.Tables[0].Rows.Clear();		//if it is NOT cleared
			BindAssignedConsignment();
		}

		private void rbtnAir_CheckedChanged(object sender, System.EventArgs e)
		{
			if (rbtnAir.Checked==true)
			{
				deliveryType="A";
				if(!utility.GetUserCulture().ToUpper().Equals("EN-US"))
				{
					lblVehicle_Flight_No.Text = Utility.GetLanguageText(ResourceType.ScreenLabel, "Flight No", utility.GetUserCulture());
					lblDriver_Flight_AWB.Text = Utility.GetLanguageText(ResourceType.ScreenLabel, "Flight AWB", utility.GetUserCulture());
				}
				else
				{
					lblVehicle_Flight_No.Text = "Flight No";
					lblDriver_Flight_AWB.Text = "Flight AWB";
				}		
			}
			else if (rbtnAir.Checked==false)
			{
				if(!utility.GetUserCulture().ToUpper().Equals("EN-US"))
				{
					lblVehicle_Flight_No.Text = Utility.GetLanguageText(ResourceType.ScreenLabel, "Vehicle No", utility.GetUserCulture());
					lblDriver_Flight_AWB.Text = Utility.GetLanguageText(ResourceType.ScreenLabel, "Driver Name", utility.GetUserCulture());
				}
				else
				{
					lblVehicle_Flight_No.Text = "Vehicle No";
					lblDriver_Flight_AWB.Text = "Driver Name";
				}
			}
			reqFlightVehicleNo.ErrorMessage=lblVehicle_Flight_No.Text;
			reqDriverFlightAWB.ErrorMessage=lblDriver_Flight_AWB.Text;

			ClearDBComboPathCode();
		}
		
		private void rbtnLong_CheckedChanged(object sender, System.EventArgs e)
		{
			if (rbtnLong.Checked==true)
			{
				deliveryType="L";
				if(!utility.GetUserCulture().ToUpper().Equals("EN-US"))
				{
					lblVehicle_Flight_No.Text = Utility.GetLanguageText(ResourceType.ScreenLabel, "Vehicle No", utility.GetUserCulture());
					lblDriver_Flight_AWB.Text = Utility.GetLanguageText(ResourceType.ScreenLabel, "Driver Name", utility.GetUserCulture());
				}
				else
				{
					lblVehicle_Flight_No.Text = "Vehicle No";
					lblDriver_Flight_AWB.Text = "Driver Name";
				}
			}
			reqFlightVehicleNo.ErrorMessage=lblVehicle_Flight_No.Text;
			reqDriverFlightAWB.ErrorMessage=lblDriver_Flight_AWB.Text;

			ClearDBComboPathCode();
		}

		private void rbtnShort_CheckedChanged(object sender, System.EventArgs e)
		{
			if (rbtnShort.Checked==true)
			{
				deliveryType="S";
				if(!utility.GetUserCulture().ToUpper().Equals("EN-US"))
				{
					lblVehicle_Flight_No.Text = Utility.GetLanguageText(ResourceType.ScreenLabel, "Vehicle No", utility.GetUserCulture());
					lblDriver_Flight_AWB.Text = Utility.GetLanguageText(ResourceType.ScreenLabel, "Driver Name", utility.GetUserCulture());
				}
				else
				{
					lblVehicle_Flight_No.Text = "Vehicle No";
					lblDriver_Flight_AWB.Text = "Driver Name";
				}			
			}
			reqFlightVehicleNo.ErrorMessage=lblVehicle_Flight_No.Text;
			reqDriverFlightAWB.ErrorMessage=lblDriver_Flight_AWB.Text;

			ClearDBComboPathCode();
		}

		private void QueryClick()
		{
			if ((bool)ViewState["IsTextChanged"]==false)
			{
				ClearAllFields();
				ClearConsignmentList();
				ResetForQuery();
				return;
			}
			if (AnyTextBoxChanged()==true)
			{			
				msgPanel.Style["display"]="inline";
				gridPanel.Style["display"]="none";
				//				gridPanel.Visible=false;
				//				msgPanel.Visible=true;
				ViewState["DMOperation"]=Operation.None;
				lblMessage.Text=Utility.GetLanguageText(ResourceType.UserMessage,"SAVE_PROMPT",utility.GetUserCulture());				
				ViewState["NextOperation"] = "Query";
				Session["SESSION_DS2"]=m_sdsAvailConsignment;
				Session["SESSION_DS3"]=m_sdsAssignedConsignment;
				return;
			}
		}

		private void btnQry_Click(object sender, System.EventArgs e)
		{
			lblErrorMsg.Text ="";					
			QueryClick();
		}	
					
		private void ClearConsignmentList()
		{			
			m_sdsAvailConsignment.Tables[0].Rows.Clear();
			m_sdsAssignedConsignment.Tables[0].Rows.Clear();
			BindAvailConsignment();
			BindAssignedConsignment();
		}
		
		private void QueryMode()
		{		
			txtVehicle_Flight_No.ReadOnly=false;
			txtDelManifestDate.ReadOnly=false;

			lblErrorMsg.Text ="";	
			rbtnLong.Enabled=true;
			rbtnShort.Enabled=true;
			rbtnAir.Enabled=true;
							
			ClearAllFields();
			ResetForQuery();	
			ClearConsignmentList();
			ViewState["IsTextChanged"] = false;
		}
	
		private void RetrieveConsignment()
		{
			if ((int)ViewState["DMMode"]==(int)ScreenMode.ExecuteQuery && (int) ViewState["DMOperation"] == (int)Operation.None)
			{
				if (DbComboPathCode.Text.Trim() !="")
				{
					DbComboPathCode.Value=DbComboPathCode.Text.Trim();
				}
			}

			ViewState["currConsSet"] = 0;
			GetShipConsignmentRecSet();
			ViewState["RetCons"]="Y";								
			BindAvailConsignment();
			//BindAssignedConsignment();
			btnAdd.Enabled=true;
			btnRemove.Enabled=true;
			btnAddAll.Enabled=true;
			btnRemoveAll.Enabled=true;
			btnRetrieveConsignment.Enabled=false;
			//this.chkOriginalState.Enabled = false;
			//this.DbComboOriState.Enabled = false;
			btnPrint.Enabled=true;
			ViewState["IsTextChanged"]=true;
		}

		private void btnRetrieveConsignment_Click(object sender, System.EventArgs e)
		{
			RetrieveConsignment();
		}
				
		[Cambro.Web.DbCombo.ResultsMethodAttribute(true)]
		public static object DbComboPathCodeSelect(Cambro.Web.DbCombo.ServerMethodArgs args)
		{
			Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings,HttpContext.Current.Session);
			String strAppID = util.GetAppID();
			String strEnterpriseID = util.GetEnterpriseID();
			
			String strDelType = "L";			
			String strWhereClause=null;
			if(args.ServerState != null && args.ServerState.Authenticate("kdfsjgnskajh[apq2-4uhg465654435879mnbfdm cxs;q4"))
			{
				if(args.ServerState["strDeliveryType"] != null && args.ServerState["strDeliveryType"].ToString().Length > 0)
				{
					strDelType = args.ServerState["strDeliveryType"].ToString();
					strWhereClause=" and Delivery_Type='"+Utility.ReplaceSingleQuote(strDelType)+"'";
				}				
			}
			
			DataSet dataset = DbComboDAL.PathCodeQuery(strAppID,strEnterpriseID,args, strWhereClause);	
			return dataset;
		}
		
		[Cambro.Web.DbCombo.ResultsMethodAttribute(true)]
		public static object ShowManifestedDate(Cambro.Web.DbCombo.ServerMethodArgs args)
		{
			Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings,HttpContext.Current.Session);
			String strAppID = util.GetAppID();
			String strEnterpriseID = util.GetEnterpriseID();			
			String strDelPath="";
			String strFltVeh="";
			String strWhereClause="";

			if(args.ServerState != null && args.ServerState.Authenticate("kdfsjgnskajh[apq2-4uhg465654435879mnbfdm cxs;q4"))
			{			
				if(args.ServerState["strDeliveryPath"] != null && args.ServerState["strDeliveryPath"].ToString().Length > 0)
				{
					strDelPath = args.ServerState["strDeliveryPath"].ToString();
					strWhereClause+=" and Path_Code='"+strDelPath+"'";
				}				
				if(args.ServerState["strFlightVehicle"] != null && args.ServerState["strFlightVehicle"].ToString().Length > 0)
				{
					strFltVeh = args.ServerState["strFlightVehicle"].ToString();
					strWhereClause+=" and Flight_Vehicle_No='"+strFltVeh+"'";
				}				
			}
			//Comment by Gwang on 16June08 
			//DataSet dataset = DbComboDAL.DeliveryDatetime(strAppID,strEnterpriseID,args, strWhereClause);	
			DataSet dataset = DbComboDAL.DepartDatetime(strAppID,strEnterpriseID,args, strWhereClause);
			//End Comment & Added
			return dataset;
		}

		private void BindAvailConsignment()
		{			
			dgAvailConsignment.DataSource = m_sdsAvailConsignment;
			dgAvailConsignment.DataBind();
			Session["SESSION_DS2"] = m_sdsAvailConsignment;
		}

		/*private void GetShipConsignmentRecSetForAssigned()
		{
			String strDelvryPathCode=null;
			strDelvryPathCode=DbComboPathCode.Value;

			if (txtVehicle_Flight_No.Text.Trim() !="" &&  txtDelManifestDate.Text.Trim() !="" && strDelvryPathCode !="")
			{				
				GetValuesIntoDS();
				m_sdsAssignedConsignment=(DataSet)DeliveryManifestMgrDAL.GetAssignedConsignmentList(appID,enterpriseID,0,0,m_sdsDeliveryManifest);
				Session["SESSION_DS3"]=m_sdsAssignedConsignment;
			}
		}*/

		private void GetShipConsignmentRecSet()
		{	
			lblErrorMsg.Text="";
			String strDelvryPathCode=null;
			strDelvryPathCode=DbComboPathCode.Value;
			if (strDelvryPathCode =="" || strDelvryPathCode==null)
			{
				strDelvryPathCode=DbComboPathCode.Text;
				if (strDelvryPathCode=="")
				{
					lblErrorMsg.Text=Utility.GetLanguageText(ResourceType.UserMessage,"INVALID_PATH_CODE",utility.GetUserCulture());									
				}
			}	
			if (rbtnAir.Checked==true)
				deliveryType="A";
			else if (rbtnLong.Checked==true)
				deliveryType="L";
			else if (rbtnShort.Checked==true)
				deliveryType="S";

			/*if (this.chkOriginalState.Checked && this.DbComboOriState.Value != null)
			{
				m_sdsAvailConsignment = (DataSet)DeliveryManifestMgrDAL.GetAvailConsignmentList(appID,enterpriseID,0,0,deliveryType,strDelvryPathCode, this.DbComboOriState.Value);
			}
			else
			{*/
			m_sdsAvailConsignment = (DataSet)DeliveryManifestMgrDAL.GetAvailConsignmentList(appID,enterpriseID,0,0,deliveryType,strDelvryPathCode);
			/*}*/
			Session["SESSION_DS2"] = m_sdsAvailConsignment;
			if (m_sdsAvailConsignment.Tables[0].Rows.Count <=0)
			{
				lblErrorMsg.Text=Utility.GetLanguageText(ResourceType.UserMessage,"NO_CONSIGN_FOUND",utility.GetUserCulture());
				//return;
			}
			//			retrieve Assigned Consignments ONLY when the COMPULSORY fields are FILLED!!!			
			//Suthat commented
			/*if (txtVehicle_Flight_No.Text.Trim() !="" &&  txtDelManifestDate.Text.Trim() !="" && strDelvryPathCode !="")
			{				
				GetValuesIntoDS();
				m_sdsAssignedConsignment=(DataSet)DeliveryManifestMgrDAL.GetAssignedConsignmentList(appID,enterpriseID,0,0,m_sdsDeliveryManifest);
				Session["SESSION_DS3"]=m_sdsAssignedConsignment;
			}*/
		}

		/*GetShipConsignmentRecSet() old version*/
		/*private void GetShipConsignmentRecSet()
		{	
			lblErrorMsg.Text="";
			String strDelvryPathCode=null;
			strDelvryPathCode=DbComboPathCode.Value;
			if (strDelvryPathCode =="" || strDelvryPathCode==null)
			{
				strDelvryPathCode=DbComboPathCode.Text;
				if (strDelvryPathCode=="")
				{
					lblErrorMsg.Text=Utility.GetLanguageText(ResourceType.UserMessage,"INVALID_PATH_CODE",utility.GetUserCulture());									
				}
			}	
			if (rbtnAir.Checked==true)
				deliveryType="A";
			else if (rbtnLong.Checked==true)
				deliveryType="L";
			else if (rbtnShort.Checked==true)
				deliveryType="S";

			m_sdsAvailConsignment = (DataSet)DeliveryManifestMgrDAL.GetAvailConsignmentList(appID,enterpriseID,0,0,deliveryType,strDelvryPathCode);			
			Session["SESSION_DS2"] = m_sdsAvailConsignment;
			if (m_sdsAvailConsignment.Tables[0].Rows.Count <=0)
			{
				lblErrorMsg.Text=Utility.GetLanguageText(ResourceType.UserMessage,"NO_CONSIGN_FOUND",utility.GetUserCulture());
			}
	
			if (txtVehicle_Flight_No.Text.Trim() !="" &&  txtDelManifestDate.Text.Trim() !="" && strDelvryPathCode !="")
			{				
				GetValuesIntoDS();
				m_sdsAssignedConsignment=(DataSet)DeliveryManifestMgrDAL.GetAssignedConsignmentList(appID,enterpriseID,0,0,m_sdsDeliveryManifest);
				Session["SESSION_DS3"]=m_sdsAssignedConsignment;
			}		
		}*/

		private void BindAssignedConsignment()
		{			
			dgAssignedConsignment.DataSource = m_sdsAssignedConsignment;
			dgAssignedConsignment.DataBind();
			Session["SESSION_DS3"] = m_sdsAssignedConsignment;
		}
	
		private void btnAdd_Click(object sender, System.EventArgs e)
		{	
			lblErrorMsg.Text = "";
			int j=0;
			for (int i=0;i< dgAvailConsignment.Items.Count;i++)
			{						
				CheckBox chkSelect = (CheckBox)dgAvailConsignment.Items[i].FindControl("chkSelect");
				if((chkSelect != null) && (chkSelect.Checked))
				{
					Label lblConsgmntNo=(Label) dgAvailConsignment.Items[i].FindControl("lblConsgmntNo");
					Label lblDGOrigin=(Label) dgAvailConsignment.Items[i].FindControl("lblDGOrigin");
					Label lblDGDestination=(Label) dgAvailConsignment.Items[i].FindControl("lblDGDestination");
					Label lblDGRoute=(Label) dgAvailConsignment.Items[i].FindControl("lblDGRoute");
				
					DataRow drEach = m_sdsAssignedConsignment.Tables[0].NewRow();
						
					drEach["Consignment_No"]=lblConsgmntNo.Text.Trim();
					drEach["Origin_State_Code"]=lblDGOrigin.Text.Trim();
					drEach["Destination_State_Code"]=lblDGDestination.Text.Trim();
					drEach["Route_Code"]=lblDGRoute.Text.Trim();
																		
					m_sdsAssignedConsignment.Tables[0].Rows.Add(drEach);																			
					try
					{
						m_sdsAvailConsignment.Tables[0].Rows.RemoveAt(i-j);	
						j++;						
					}
					catch
					{						
					}
				}				
			}			

			Session["SESSION_DS2"]=m_sdsAvailConsignment;
			Session["SESSION_DS3"]=m_sdsAssignedConsignment;

			BindAvailConsignment();
			BindAssignedConsignment();
			ViewState["IsTextChanged"]=true;
			btnRetrieveConsignment.Enabled=false;
			//this.chkOriginalState.Enabled = false;
			//this.DbComboOriState.Enabled = false;
		}

		private void btnRemove_Click(object sender, System.EventArgs e)
		{	
			lblErrorMsg.Text = "";		
			int j=0;
			for (int i=0;i< dgAssignedConsignment.Items.Count;i++)
			{		
				CheckBox chkSelect1 = (CheckBox) dgAssignedConsignment.Items[i].FindControl("chkSelect1");
				if((chkSelect1 != null) && (chkSelect1.Checked))
				{
					Label lblConsgmntNo=(Label) dgAssignedConsignment.Items[i].FindControl("lblConsgmntNo1");
					Label lblDGOrigin=(Label) dgAssignedConsignment.Items[i].FindControl("lblDGOrigin1");
					Label lblDGDestination=(Label) dgAssignedConsignment.Items[i].FindControl("lblDGDestination1");
					Label lblDGRoute=(Label) dgAssignedConsignment.Items[i].FindControl("lblDGRoute1");

					DataRow drEach = m_sdsAvailConsignment.Tables[0].NewRow();
					drEach["Consignment_No"]=lblConsgmntNo.Text.Trim();
					drEach["Origin_State_Code"]=lblDGOrigin.Text.Trim();
					drEach["Destination_State_Code"]=lblDGDestination.Text.Trim();
					drEach["Route_Code"]=lblDGRoute.Text.Trim();

					m_sdsAvailConsignment.Tables[0].Rows.Add(drEach);
					
					try
					{
						m_sdsAssignedConsignment.Tables[0].Rows.RemoveAt(i-j);						
						j++;
					}
					catch
					{						
					}
				}
			}

			Session["SESSION_DS2"]=m_sdsAvailConsignment;
			Session["SESSION_DS3"]=m_sdsAssignedConsignment;
			BindAssignedConsignment();
			BindAvailConsignment();
			ViewState["IsTextChanged"]=true;
			btnRetrieveConsignment.Enabled=false;
			//this.chkOriginalState.Enabled = false;
			//this.DbComboOriState.Enabled = false;	
		}

		private void btnAddAll_Click(object sender, System.EventArgs e)
		{	
			lblErrorMsg.Text = "";		
			if (m_sdsAvailConsignment.Tables[0].Rows.Count > 0)
			{				
				for (int i=0;i<m_sdsAvailConsignment.Tables[0].Rows.Count;i++)
				{
					DataRow drNew=m_sdsAssignedConsignment.Tables[0].NewRow();
					DataRow drOld=m_sdsAvailConsignment.Tables[0].Rows[i];
							
					drNew["Consignment_No"]=drOld["Consignment_No"];
					drNew["Origin_State_Code"]=drOld["Origin_State_Code"];
					drNew["Destination_State_Code"]=drOld["Destination_State_Code"];
					drNew["Route_Code"]=drOld["Route_Code"];
					m_sdsAssignedConsignment.Tables[0].Rows.Add (drNew);
				}
				m_sdsAvailConsignment.Tables[0].Rows.Clear();

				BindAvailConsignment();
				BindAssignedConsignment();
				ViewState["IsTextChanged"]=true;
			}
			btnRetrieveConsignment.Enabled=false;
			//this.chkOriginalState.Enabled = false;
			//this.DbComboOriState.Enabled = false;
		}

		private void btnRemoveAll_Click(object sender, System.EventArgs e)
		{
			lblErrorMsg.Text = "";
			if (m_sdsAssignedConsignment.Tables[0].Rows.Count > 0)
			{				
				for(int i=0;i<m_sdsAssignedConsignment.Tables[0].Rows.Count;i++)
				{
					DataRow drNew=m_sdsAvailConsignment.Tables[0].NewRow();
					DataRow drOld=m_sdsAssignedConsignment.Tables[0].Rows[i];

					drNew["Consignment_No"]=drOld["Consignment_No"];
					drNew["Origin_State_Code"]=drOld["Origin_State_Code"];
					drNew["Destination_State_Code"]=drOld["Destination_State_Code"];
					drNew["Route_Code"]=drOld["Route_Code"];
					m_sdsAvailConsignment.Tables[0].Rows.Add (drNew);

				}
				m_sdsAssignedConsignment.Tables[0].Rows.Clear();
			
				BindAvailConsignment();
				BindAssignedConsignment();
				ViewState["IsTextChanged"]=true;
			}				
			btnRetrieveConsignment.Enabled=false;
			//this.chkOriginalState.Enabled = false;
			//this.DbComboOriState.Enabled = false;
		}
		protected void DbComboPathCode_OnSelectedIndexChanged(object sender, System.EventArgs e)
		{
			lblErrorMsg.Text="";
			dbDepartDate.Text="";
			dbDepartDate.Value="";
			txtDelManifestDate.Text="";
			txtDriver_Flight_AWB.Text="";
			txtLineHaulCost.Text="";
			txtVehicle_Flight_No.Text="";
			String strPathCode = DbComboPathCode.Value;
			if (strPathCode.Trim()!="")			
			{
				btnRetrieveConsignment.Enabled=true;
				//this.chkOriginalState.Enabled = true;
				//this.DbComboOriState.Enabled = true;
				m_sdsAvailConsignment.Tables[0].Rows.Clear();			//if it is NOT refreshed
				BindAvailConsignment();
				m_sdsAssignedConsignment.Tables[0].Rows.Clear();		//if it is NOT cleared
				BindAssignedConsignment();
			}
			else
			{
				strPathCode =DbComboPathCode.Text;
				//btnRetrieveConsignment.Enabled=false;
			}
			String strPathDescription=null;
			strPathDescription = TIESUtility.GetPathCodeDesc( appID,enterpriseID,strPathCode);
			if (strPathDescription==null)
			{				
				lblErrorMsg.Text=Utility.GetLanguageText(ResourceType.UserMessage,"NO_RECORDS_FOUND",utility.GetUserCulture());
				return;
			}
			txtPathDesc.Text = 	strPathDescription;			
			ViewState["IsTextChanged"]=true;
			btnRetrieveConsignment.Enabled=true;
			//this.chkOriginalState.Enabled = true;
			//this.DbComboOriState.Enabled = true;
		}

		private bool SaveUpdateRecord()
		{
			bool isError = true;

			if (DbComboPathCode.Text != "" || DbComboPathCode.Value != "")
			{
				btnRetrieveConsignment.Enabled=true;
				//this.chkOriginalState.Enabled = true;
				//this.DbComboOriState.Enabled = true;
				//				return isError;
			}
			m_sdsAvailConsignment = (DataSet)Session["SESSION_DS2"];
			m_sdsAssignedConsignment = (DataSet)Session["SESSION_DS3"];

			
			
			GetValuesIntoDS();
			DataSet dsInsert=m_sdsDeliveryManifest;			
			String strDelPathCode="";
			String strFlightVehNo="";			
			String strDelManifestDate="";
			DataRow drEach = dsInsert.Tables[0].Rows[0];			
			
			if((drEach["path_code"]!= null) && (!drEach["path_code"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				strDelPathCode = (String) drEach["path_code"];				
			}
			if((drEach["flight_vehicle_no"]!= null) && (!drEach["flight_vehicle_no"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				strFlightVehNo = (String) drEach["flight_vehicle_no"];				
			}				
			if((drEach["delivery_manifest_datetime"]!= null) && (!drEach["delivery_manifest_datetime"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{							
				strDelManifestDate=drEach["delivery_manifest_datetime"].ToString();
			}
			
			Boolean bDMExists=DeliveryManifestMgrDAL.DeliveryManifestExists(appID,enterpriseID,strDelPathCode, strFlightVehNo,  strDelManifestDate);
			//			if (m_sdsAssignedConsignment.Tables[0].Rows.Count<=0) 
			//			{		
			//				lblErrorMsg.Text=Utility.GetLanguageText(ResourceType.UserMessage,"NO_CONSIGN_SELECTED",utility.GetUserCulture());
			//				return isError;
			//			}
			if (bDMExists==false)
			{
				try
				{
					DeliveryManifestMgrDAL.CreateDMsNConsignments(appID,enterpriseID, m_sdsAssignedConsignment,dsInsert);
					isError=false;									
				}
				catch(ApplicationException appException)
				{
					String strMessage = appException.Message;
					if(strMessage.IndexOf("duplicate key") != -1 || strMessage.IndexOf("unique")!=-1)
					{
						lblErrorMsg.Text=Utility.GetLanguageText(ResourceType.UserMessage,"DUP_KEY_FOUND",utility.GetUserCulture());
					}
					else if(strMessage.IndexOf("FK") != -1 )
					{
						lblErrorMsg.Text=Utility.GetLanguageText(ResourceType.UserMessage,"FOREIGN_KEY_FOUND",utility.GetUserCulture());
					}
					else
					{
						lblErrorMsg.Text = strMessage;
					}					
					return isError;						
				}
				lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"INS_SUCCESSFULLY",utility.GetUserCulture());
				
			}
			else
			{
				try
				{
					DeliveryManifestMgrDAL.ModifyDMsNConsignments( appID,enterpriseID, m_sdsAssignedConsignment,dsInsert );
					isError=false;										
				}
				catch(ApplicationException appException)
				{
					String strMessage = appException.Message;
					if(strMessage.IndexOf("duplicate key") != -1 || strMessage.IndexOf("unique")!=-1)
					{
						lblErrorMsg.Text=Utility.GetLanguageText(ResourceType.UserMessage,"DUP_KEY_FOUND",utility.GetUserCulture());
						//						lblErrorMsg.Text = "Duplicate Key found. Cannot save the record";
					}
					else if(strMessage.IndexOf("FK") != -1 )
					{
						lblErrorMsg.Text=Utility.GetLanguageText(ResourceType.UserMessage,"FOREIGN_KEY_FOUND",utility.GetUserCulture());						
					}
					else
					{
						lblErrorMsg.Text = strMessage;
					}
						
					return isError;
				}
				lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"MODIFIED_SUCCESSFULLY",utility.GetUserCulture());
				
			}
			
			//Disable Changing of radio buttons
			if (rbtnAir.Checked==true)
			{
				rbtnLong.Enabled=false;
				rbtnShort.Enabled=false;
			}
			else if (rbtnShort.Checked==true)
			{
				rbtnLong.Enabled=false;
				rbtnAir.Enabled=false;
			}
			else if (rbtnLong.Checked==true)
			{
				rbtnAir.Enabled=false;
				rbtnShort.Enabled=false;
			}
			//			m_sdsAssignedConsignment.Tables[0].Rows.Clear();
			//			BindAssignedConsignment();
			
			btnPrint.Enabled=true;		//Enable the PRINT button ONLY if saved
			ViewState["DMOperation"]=Operation.Saved;
			btnRetrieveConsignment.Enabled=false;
			//this.chkOriginalState.Enabled = false;
			//this.DbComboOriState.Enabled = false;
			return isError;
		}

		private void btnSave_Click(object sender, System.EventArgs e)
		{			
			//Save the record and return the status
			ViewState["IsTextChanged"]=false;
			bool isError = SaveUpdateRecord();
			
			if (isError==true)
			{
				return;
			}	
		}

		private void dgAvailConsignment_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			int i=dgAvailConsignment.Items.Count;
		}

		private void btnDelete_Click(object sender, System.EventArgs e)
		{
			ViewState["DMOperation"]=Operation.Delete;
			GetValuesIntoDS();
			DataSet dsDelete=m_sdsDeliveryManifest;
			try
			{
				DeliveryManifestMgrDAL.DeleteDMsNConsignments(appID,enterpriseID,dsDelete);
			}
			catch (Exception exp)
			{
				String strMessage = exp.Message;
				lblErrorMsg.Text = strMessage;
			}
		}

		private void btnPrint_Click(object sender, System.EventArgs e)
		{
			GetValuesIntoDS();
			Session["FORMID"] = "DeliveryManifest";
			Session["SESSION_DS1"] = m_sdsDeliveryManifest;

			String sUrl = "ReportViewer.aspx";
			
			String sFeatures = "'height=700,width=900,left=20,top=5,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,titlebar=yes,toolbar=no'";
			String sScript ="";
			sScript += "<script language=javascript>";
			sScript += "window.open('" + sUrl + "',''," + sFeatures + ");";
			sScript += "</script>";
			Response.Write(sScript);	
		}

		private void txtVehicle_Flight_No_TextChanged(object sender, System.EventArgs e)
		{
			bTextChanged=true;
			ViewState["IsTextChanged"]=bTextChanged;
		}

		private void txtLineHaulCost_TextChanged(object sender, System.EventArgs e)
		{
			bTextChanged=true;
			ViewState["IsTextChanged"]=bTextChanged;
		}

		private void txtDriver_Flight_AWB_TextChanged(object sender, System.EventArgs e)
		{
			bTextChanged=true;
			ViewState["IsTextChanged"]=bTextChanged;
		}

		private void txtDepartDate_TextChanged(object sender, System.EventArgs e)
		{
			bTextChanged=true;
			ViewState["IsTextChanged"]=bTextChanged;
		}

		private void txtDelManifestDate_TextChanged(object sender, System.EventArgs e)
		{
			bTextChanged=true;
			ViewState["IsTextChanged"]=bTextChanged;
		}

		private void btnToSaveChanges_Click(object sender, System.EventArgs e)
		{
			ViewState["IsTextChanged"] = false;				
			bool isError = SaveUpdateRecord();		
			if(isError == false)
			{
				if((String)ViewState["NextOperation"] == "Insert")
					InsertMode();
				else if((String)ViewState["NextOperation"] == "ExecuteQuery")
					ExecuteQueryMode();
				else if((String)ViewState["NextOperation"] == "Query")
				{
					QueryClick();
					ClearConsignmentList();
				}
			}

			msgPanel.Style["display"]="none";
			gridPanel.Style["display"]="inline";
			//			msgPanel.Visible=false;
			//			gridPanel.Visible=true;			
		}

		private void btnNotToSave_Click(object sender, System.EventArgs e)
		{	
			lblErrorMsg.Text="";
			ViewState["IsTextChanged"]=false;
			if((String)ViewState["NextOperation"] == "Insert")
				InsertMode();
			else if((String)ViewState["NextOperation"] == "ExecuteQuery")
				ExecuteQueryMode();
			else if((String)ViewState["NextOperation"] == "Query")
				QueryMode();			

			msgPanel.Style["display"]="none";
			gridPanel.Style["display"]="inline";
			SetDbComboServerStates();
			
			//			style.display="inline";
			//			msgPanel.Visible=false;
			//			gridPanel.Visible=true;
		}

		private void btnNO_Click(object sender, System.EventArgs e)
		{
			msgPanel.Style["display"]="none";
			gridPanel.Style["display"]="inline";
			//			msgPanel.Visible=false;
			//			gridPanel.Visible=true;
		}

		private void dgAssignedConsignment_SelectedIndexChanged(object sender, System.EventArgs e)
		{
		
		}

		private void chkOriginalState_CheckedChanged(object sender, System.EventArgs e)
		{
		}

		private void dbDepartDate_SelectedItemChanged(object sender, System.EventArgs e)
		{
			txtVehicle_Flight_No.Text= "";
			txtDriver_Flight_AWB.Text = "";
			txtLineHaulCost.Text= "";
			txtDelManifestDate.Text= "";
			String strDelPathCode=DbComboPathCode.Value;
			String strFlightVehicleNo=txtVehicle_Flight_No.Text;
			String strDepartDate=dbDepartDate.Value;		
			String strFlightVehicleNoDR="";		
			String strDriverFlightAWB="";
			String strLineHaulCost="";
			String strManifestDate="";
	

			DataSet dsDMRecord=ReadDMRecord(strDelPathCode,strFlightVehicleNo,strDepartDate);
				
			if (dsDMRecord.Tables[0].Rows.Count<=0)
			{
				lblErrorMsg.Text= Utility.GetLanguageText(ResourceType.UserMessage,"NO_RECORD_FOUND_PATH",utility.GetUserCulture());
				return;
			}			
			DataRow drEach=dsDMRecord.Tables[0].Rows[0];
			
			if ((drEach["flight_vehicle_no"] != null) && (drEach["flight_vehicle_no"].ToString() != ""))
			{
				strFlightVehicleNoDR = (String) drEach["flight_vehicle_no"];
			}
			txtVehicle_Flight_No.Text=strFlightVehicleNoDR;
			if ((drEach["awb_driver_name"] != null) && (drEach["awb_driver_name"].ToString() != ""))
			{
				strDriverFlightAWB = (String) drEach["awb_driver_name"];
			}
			txtDriver_Flight_AWB.Text=strDriverFlightAWB;

			if ((drEach["line_haul_cost"] != null) && (drEach["line_haul_cost"].ToString() != ""))
			{
				decimal dLineHaulCost = Convert.ToDecimal(drEach["line_haul_cost"]);
				strLineHaulCost = dLineHaulCost.ToString("#.00");				
			}
			txtLineHaulCost.Text=strLineHaulCost;
			
			if ((drEach["Delivery_Manifest_DateTime"] != null) && (drEach["Delivery_Manifest_DateTime"].ToString() != ""))
			{
				DateTime dtManiDate = (DateTime) drEach["Delivery_Manifest_DateTime"];//DateTime dtDepartDate = (DateTime) drEach["Delivery_Manifest_DateTime"];
				strManifestDate = dtManiDate.ToString("dd/MM/yyyy HH:mm");//strManifestDate = dtDepartDate.ToString("dd/MM/yyyy HH:mm");
			}
			txtDelManifestDate.Text=strManifestDate;
			/*
			string strDepartDate = dbDepartDate.Value;
			DateTime ManifestDT = System.DateTime.ParseExact(strDepartDate,"dd/MM/yyyy HH:mm",null);
			txtDelManifestDate.Text = "1";*/
		}
		
		private DataSet ReadDMRecord(String strDelPathCode, String strFlightVehicleNo, String strDepartDate)
		{
			lblErrorMsg.Text="";
			DataSet dsQuery =GetAssignedDM();			
			DataRow drDMNew = dsQuery.Tables[0].NewRow();

			drDMNew["Path_Code"]=strDelPathCode;
			if (strFlightVehicleNo !="")
				drDMNew["Flight_Vehicle_No"]=strFlightVehicleNo;
			if (strDepartDate !="")
			{
				drDMNew["Departure_datetime"]=System.DateTime.ParseExact(strDepartDate,"dd/MM/yyyy HH:mm",null);				
			}
			dsQuery.Tables[0].Rows.Add(drDMNew);
			//Comment by Gwang on 16June08 
			//DataSet dsDMRec=DeliveryManifestMgrDAL.DelManifestRecord(appID,enterpriseID,dsQuery); 
			DataSet dsDMRec = DeliveryManifestMgrDAL.DepartureRecordDR(appID,enterpriseID,dsQuery);
			//End Comment & Added
			return dsDMRec;
		}

		private DataSet GetAssignedDM()
		{
			DataTable dtDM = new DataTable(); 
			dtDM.Columns.Add(new DataColumn("Path_Code", typeof(string)));
			dtDM.Columns.Add(new DataColumn("Flight_Vehicle_No", typeof(string)));
			dtDM.Columns.Add(new DataColumn("Delivery_Manifest_DateTime", typeof(DateTime)));
			dtDM.Columns.Add(new DataColumn("AWB_Driver_Name", typeof(string)));
			dtDM.Columns.Add(new DataColumn("Line_Haul_Cost", typeof(Decimal)));
			dtDM.Columns.Add(new DataColumn("Departure_DateTime", typeof(DateTime)));
			dtDM.Columns.Add(new DataColumn("Remark", typeof(string)));

			DataSet dsDM = new DataSet();
			dsDM.Tables.Add(dtDM);
			
			return  dsDM;				
		}

		private void OpenWindowpage(String strUrl)
		{
			ArrayList paramList = new ArrayList();
			paramList.Add(strUrl);
			String sScript = Utility.GetScript("openParentWindow.js",paramList);
			Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
		}
	}
}
