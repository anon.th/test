<%@ Page language="c#" Codebehind="DEOManual.aspx.cs" AutoEventWireup="false" Inherits="com.ties.DEOManual" %>
<%@ Import Namespace="com.common.util" %>
<%@ Import Namespace="Cambro.Web.DbCombo" %>
<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<%@ Register TagPrefix="DbCombo" Namespace="Cambro.Web.DbCombo" Assembly="Cambro.Web.DbCombo" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>ShipmentTrackingQuery</title>
		<LINK href="css/Styles.css" type="text/css" rel="stylesheet" name="stylesheet">
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<!--#INCLUDE FILE="msFormValidations.inc"-->
		<script language="javascript" src="Scripts/settingScrollPosition.js"></script>
	</HEAD>
	<body onunload="window.top.displayBanner.fnCloseAll(0);"
		MS_POSITIONING="GridLayout">
		<form id="ShipmentTrackingQueryRpt" method="post" runat="server">
			<TABLE id="Table5" style="Z-INDEX: 101; LEFT: 8px; POSITION: absolute; TOP: 16px; HEIGHT: 75px"
				cellSpacing="0" cellPadding="0" width="100%" border="0">
				<TR>
					<TD align="center"><asp:label id="Label1" runat="server" Height="27px" Width="200px" CssClass="maintitleSize"> Delete Consignment</asp:label></TD>
				</TR>
				<TR>
					<TD>
						<TABLE id="Table2" style="WIDTH: 300px; HEIGHT: 32px" cellSpacing="0" cellPadding="0" width="300"
							border="0">
							<TR>
								<TD style="WIDTH: 115px"><asp:button id="btQuery" tabIndex="5" runat="server" Width="104px" CssClass="queryButton" Text="Query"
										Height="23px"></asp:button></TD>
								<TD><asp:button id="btExecute" tabIndex="6" runat="server" Width="112px" CssClass="queryButton"
										Text="Delete" Height="23px"></asp:button></TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
			</TABLE>
			<TABLE id="Table1" style="Z-INDEX: 102; LEFT: 8px; POSITION: absolute; TOP: 104px; HEIGHT: 176px"
				cellSpacing="0" cellPadding="0" width="100%" border="0">
				<TR>
					<TD vAlign="top" width="40%" colSpan="1" rowSpan="1">
						<TABLE class="tableLabel" id="Table3" cellSpacing="0" cellPadding="0" width="100%" border="0">
							<TR>
								<TD align="right" width="25%" colSpan="1" rowSpan="1"><asp:label id="Label8" runat="server" Width="132px" Font-Bold="True">Consignment No.:</asp:label></TD>
								<TD width="80%"><cc1:mstextbox id="txtConsignmentNo" style="TEXT-TRANSFORM: uppercase" tabIndex="1" runat="server"
										Height="20px" Width="160px" CssClass="textField" TextMaskType="msUpperAlfaNumericWithUnderscore" MaxLength="30"
										AutoPostBack="True"></cc1:mstextbox><asp:button id="Button1" runat="server" Height="23px" Width="72px" CssClass="queryButton" Text="Search"></asp:button></TD>
							</TR>
							<TR>
								<TD style="WIDTH: 184px" align="right"><asp:label id="lblBooking" runat="server" Width="134px" Font-Bold="True">Booking No.:</asp:label></TD>
								<TD>
									<cc1:mstextbox id="txtBooking" style="TEXT-TRANSFORM: uppercase" tabIndex="1" runat="server" Height="20px"
										Width="160px" CssClass="textField" TextMaskType="msUpperAlfaNumericWithUnderscore" MaxLength="30"
										AutoPostBack="True"></cc1:mstextbox>
								</TD>
							</TR>
							<TR>
								<TD style="WIDTH: 184px" align="right"><asp:label id="Label2" runat="server" Width="134px" Font-Bold="True">Reason Code:</asp:label></TD>
								<TD><asp:dropdownlist id="dlReasonCode" runat="server" Width="160px" AutoPostBack="True"></asp:dropdownlist></TD>
							</TR>
							<TR>
								<TD style="WIDTH: 184px" align="right"></TD>
								<TD><asp:textbox id="txtReasonDesc" runat="server" Height="33px" Width="328px" BorderStyle="Solid"
										TextMode="MultiLine" Enabled="False"></asp:textbox></TD>
							</TR>
							<TR>
								<TD style="WIDTH: 184px" vAlign="top" align="right"><asp:label id="Label3" runat="server" Width="139px" Font-Bold="True">Remark:</asp:label></TD>
								<TD><asp:textbox id="txtRemark" tabIndex="3" runat="server" Height="64px" Width="328px" TextMode="MultiLine"></asp:textbox></TD>
							</TR>
						</TABLE>
					</TD>
					<TD vAlign="top" width="50%">
						<TABLE class="tableLabel" id="Table4" style="HEIGHT: 87px" cellSpacing="0" cellPadding="0"
							width="100%" border="0">
							<TR>
								<TD align="right" width="10%" colSpan="1" rowSpan="1"><asp:label id="Label4" runat="server" Width="95px" Font-Bold="True">Booking No.:</asp:label></TD>
								<TD width="90%" colSpan="1" rowSpan="1"><asp:label id="lbBooking" runat="server" Width="100%"></asp:label></TD>
							</TR>
							<TR>
								<TD style="WIDTH: 150px" align="right"><asp:label id="Label5" runat="server" Width="100px" Font-Bold="True">Last Status:</asp:label></TD>
								<TD><asp:label id="lbstatus" runat="server" Width="100%"></asp:label></TD>
							</TR>
							<TR>
								<TD style="WIDTH: 150px" align="right"><asp:label id="Label6" runat="server" Width="132px" Font-Bold="True">Pickup Route Code:</asp:label></TD>
								<TD><asp:label id="lbPickupRoute" runat="server" Width="100%"></asp:label></TD>
							</TR>
							<TR>
								<TD style="WIDTH: 150px" align="right"><asp:label id="Label7" runat="server" Width="121px" Font-Bold="True">Payer Name:</asp:label></TD>
								<TD><asp:label id="lbPayerName" runat="server" Width="100%"></asp:label></TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
				<TR>
					<TD vAlign="top" width="40%"><FONT face="Tahoma"><asp:label id="lbDeleted" runat="server" Width="488px" Font-Bold="True" ForeColor="Red"></asp:label></FONT></TD>
					<TD vAlign="top" width="50%"><FONT face="Tahoma"></FONT></TD>
				</TR>
			</TABLE>
			&nbsp; </TR></TABLE></TR></TABLE></form>
	</body>
</HTML>
