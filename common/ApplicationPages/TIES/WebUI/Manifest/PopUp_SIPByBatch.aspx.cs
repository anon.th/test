using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.common.util;
using com.common.classes;
using com.common.DAL;
using System.Text;
using com.common.applicationpages;
using com.ties.DAL;
using com.common.RBAC;

namespace TIES.WebUI.Manifest
{
	/// <summary>
	/// Summary description for PopUp_CreateUpdateBatch.
	/// </summary>
	public class PopUp_SIPByBatch : BasePopupPage
	{
		protected System.Web.UI.WebControls.Button btnCancel;
		protected System.Web.UI.WebControls.Label lblError;
		protected System.Web.UI.WebControls.Button btnOk;
		protected System.Web.UI.WebControls.Label lblDisCurrentLocatrion;
		protected System.Web.UI.WebControls.Label lblDisDesctination;
		protected System.Web.UI.WebControls.Label lblCurrentLocation;
		protected System.Web.UI.WebControls.CheckBoxList ckboxdestination;

		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			getSessionTimeOut(true);
			utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings,Page.Session);
			if(!Page.IsPostBack)
			{
				if (Request.QueryString["BatchNo"] !=null)
				{	
					string strBatchNo="";
					strBatchNo= Request.QueryString["BatchNo"].ToString().Trim();
					string strCurrentLocation="";
					strCurrentLocation= Request.QueryString["CurrentLocation"].ToString().Trim();
					string strBatchID="";
					strBatchID= Request.QueryString["BatchID"].ToString().Trim();
					lblCurrentLocation.Text=strCurrentLocation +"  Batch No: " + strBatchNo;
					getDescDC(strBatchNo,strCurrentLocation,strBatchID);
				}

			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
			this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
			this.ckboxdestination.SelectedIndexChanged += new System.EventHandler(this.ckboxdestination_SelectedIndexChanged);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
		
		
		private void getDescDC(string strBatchNo,string strCurrentLocation,string strBatchID)
		{
			DataSet ds=null;
			ds = BatchManifest.GetDestDCForSIPByBatch(utility.GetAppID(),utility.GetEnterpriseID(),strBatchNo);
			if (ds.Tables[0].Rows.Count>0)
			{
				ckboxdestination.DataTextField="dest_dc";
				ckboxdestination.DataValueField="dest_dc";
				ckboxdestination.DataSource=ds.Tables[0];
				ckboxdestination.DataBind();
				string strDes ="";
				bool blnCheck=false;
				DataSet dest =BatchManifest.GetDestinationAllType(utility.GetAppID(),utility.GetEnterpriseID(),strBatchID);
				if (dest.Tables[0].Rows.Count > 0 )
				{
					strDes = dest.Tables[0].Rows[0]["destination_station"].ToString(); 
				}
				if(strDes!=strCurrentLocation)
				{
					for(int i=0; i<ckboxdestination.Items.Count; i++)
					{
						if(ckboxdestination.Items[i].Value.ToString().Trim()== strCurrentLocation)
						{
							ckboxdestination.Items[i].Selected=true;
							blnCheck=true;
							break;
						}
					}
				}
				else
				{
					for(int i=0; i<ckboxdestination.Items.Count; i++)
					{
						blnCheck=true;
						ckboxdestination.Items[i].Selected=true;
					}
				}
				if(!blnCheck)
				{
					btnOk.Enabled=false;
				}
			}
		}


		private void btnCancel_Click(object sender, System.EventArgs e)
		{
			String sScript = "";
			sScript += "<script language=javascript>";
			sScript += "  window.close();";
			sScript += "</script>";
			Response.Write(sScript);
		}

		private void btnOk_Click(object sender, System.EventArgs e)
		{
			string strBatchNo="";
			strBatchNo= Request.QueryString["BatchNo"].ToString().Trim();
			string strCurrentLocation="";
			strCurrentLocation= Request.QueryString["CurrentLocation"].ToString().Trim();
			string strBatchID="";
			strBatchID= Request.QueryString["BatchID"].ToString().Trim();
			string strDesc="";
			string strPersonInchange=Request.QueryString["PersonInchange"].ToString().Trim();

			for(int i=0; i<ckboxdestination.Items.Count; i++)
			{
				if(ckboxdestination.Items[i].Selected)
				{
					strDesc += ckboxdestination.Items[i].Value + "|" ;
				}
			}
			if(strDesc.Trim()!="")
			{
				if(strDesc.Substring(strDesc.Length-1,1)=="|")
				{
					strDesc=strDesc.Substring(0,strDesc.Length-1);
				}
			}
			int rowAffect=0;
			rowAffect= BatchManifest.InsertToStategingSIPbyBatch(utility.GetAppID(),utility.GetEnterpriseID(),utility.GetUserID(),strCurrentLocation,strBatchNo,strDesc,strPersonInchange);
//				if(rowAffect>0)
//				{
					Session["SIPbyBatchRowEffect"]=rowAffect;
					String sScript = "";
					sScript += "<script language=javascript> ";
					sScript += "  window.opener.document.getElementById('btnSubmitHidden').click();";
					sScript += "  window.close();";
					sScript += "</script>";
					Response.Write(sScript);
//				}
//				else
//				{
//					//lblError.Text = Utility.GetLanguageText(ResourceType.UserMessage,"NO_RECORD_UPD",utility.GetUserCulture());
//				}
		}
		private void ckboxdestination_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			foreach(ListItem chk in ckboxdestination.Items)
			{
				if(chk.Selected==true)
				{
					btnOk.Enabled=true;
					break;
				}
				else
				{
					btnOk.Enabled=false;
				}
			}
		}

		
	}
}