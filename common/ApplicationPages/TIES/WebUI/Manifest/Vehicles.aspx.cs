using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.common.util;
using com.common.classes;
using com.common.DAL;
using System.Text;
using com.common.applicationpages;

namespace TIES.WebUI
{
	/// <summary>
	/// Summary description for Vehicles.
	/// </summary>
	public class Vehicles : BasePopupPage////System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.Label lblTruckID;
		protected System.Web.UI.WebControls.Label lblType;
		protected System.Web.UI.WebControls.Label lblLocation;
		protected System.Web.UI.WebControls.Label lblDesc;
		protected System.Web.UI.WebControls.Label lblLicense;
		protected System.Web.UI.WebControls.Button btnClose;
		protected System.Web.UI.WebControls.Button btnSearch;
		protected System.Web.UI.WebControls.DropDownList ddlLocation;
		protected System.Web.UI.WebControls.DropDownList ddlType;
		protected System.Web.UI.WebControls.Label lblMainTitle;
	
		SessionDS m_sdsTruckCode = null;
		String strAppID = "";
		String strEnterpriseID = "";
		String strCulture ="";
		protected System.Web.UI.WebControls.TextBox txtTruckID;
		protected System.Web.UI.WebControls.TextBox txtDesc;
		protected System.Web.UI.WebControls.TextBox txtLicense;
		private String strTruckCode = null;
		protected System.Web.UI.WebControls.DataGrid dgTruck;
		private DataView m_dvTruckTypeOptions;

		private void Page_Load(object sender, System.EventArgs e)
		{
			getSessionTimeOut(true);
			// Put user code to initialize the page here
			Utility utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings,Page.Session);
			strAppID = utility.GetAppID();
			strEnterpriseID = utility.GetEnterpriseID();
			strCulture = utility.GetUserCulture();
			if(!Page.IsPostBack)
			{
				txtTruckID.Text = Request.Params["TruckID"];
				txtDesc.Text = Request.Params["Description"];
				txtLicense.Text = Request.Params["LicensePlate"];
				LoadUserLocation();	
				LoadComboLists();
				BindComboLists();
				m_sdsTruckCode = GetEmptyTruckCodeDS(0);
				BindGrid();
			}

		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.dgTruck.PageIndexChanged += new System.Web.UI.WebControls.DataGridPageChangedEventHandler(this.dgTruck_PageIndexChanged);
			this.dgTruck.SelectedIndexChanged += new System.EventHandler(this.dgTruck_SelectedIndexChanged);
			this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
			this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private void btnClose_Click(object sender, System.EventArgs e)
		{
			String sScript = "";
			sScript += "<script language=javascript>";
			sScript += "  window.close();";
			sScript += "</script>";
			Response.Write(sScript);
		}
		private void LoadComboLists()
		{
			m_dvTruckTypeOptions = GetTruckTypeOptions(true);
		}
		
		private void BindComboLists()
		{
			ddlType.DataSource = (ICollection)m_dvTruckTypeOptions;
			ddlType.DataTextField = "Text";
			ddlType.DataValueField = "StringValue";
			ddlType.DataBind();
		}

		private DataView GetTruckTypeOptions(bool showNilOption)
		{
			DataTable dtTruckTypeOptions = new DataTable();
			
			dtTruckTypeOptions.Columns.Add(new DataColumn("Text", typeof(string)));
			dtTruckTypeOptions.Columns.Add(new DataColumn("StringValue", typeof(string)));

			ArrayList TruckTypeOptionArray = Utility.GetCodeValues(strAppID,strCulture,"truck_type",CodeValueType.StringValue);
			if(showNilOption)
			{
				DataRow drNilRow = dtTruckTypeOptions.NewRow();
				drNilRow[0] = "";
				drNilRow[1] = "";
				dtTruckTypeOptions.Rows.Add(drNilRow);
			}
			foreach(SystemCode TruckTypeSysCode in TruckTypeOptionArray)
			{
				DataRow drEach = dtTruckTypeOptions.NewRow();
				drEach[0] = TruckTypeSysCode.Text;
				drEach[1] = TruckTypeSysCode.StringValue;
				dtTruckTypeOptions.Rows.Add(drEach);
			}

			DataView dvTruckTypeOptions = new DataView(dtTruckTypeOptions);
			return dvTruckTypeOptions;

		}

		public void LoadUserLocation()
		{
			Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings,HttpContext.Current.Session);
			String strAppID = util.GetAppID();
			String strEnterpriseID = util.GetEnterpriseID();

			DataSet dataset = com.ties.classes.DbComboDAL.UserLocationQuery(strAppID,strEnterpriseID);	
			this.ddlLocation.DataSource = dataset;

			ddlLocation.DataTextField = dataset.Tables[0].Columns["origin_code"].ToString() ;
			ddlLocation.DataValueField = dataset.Tables[0].Columns["origin_code"].ToString() ;
			this.ddlLocation.DataBind();
			ListItem defItem = new ListItem();
			defItem.Text = "";
			
			defItem.Value = "";
			ddlLocation.Items.Insert(0,(defItem));
		}

		private void BindGrid()
		{
			dgTruck.VirtualItemCount = System.Convert.ToInt32(m_sdsTruckCode.QueryResultMaxSize);
			dgTruck.DataSource = m_sdsTruckCode.ds;
			dgTruck.DataBind();
			ViewState["TRUCKCODE_DS"] = m_sdsTruckCode;
		}

		public static SessionDS GetEmptyTruckCodeDS(int intRows)
		{
			DataTable dtTruck = new DataTable();
			dtTruck.Columns.Add(new DataColumn("truckid", typeof(string)));
			dtTruck.Columns.Add(new DataColumn("description", typeof(string)));
			dtTruck.Columns.Add(new DataColumn("licenseplate", typeof(string)));
			dtTruck.Columns.Add(new DataColumn("type", typeof(string)));
			dtTruck.Columns.Add(new DataColumn("location", typeof(string)));

			for(int i=0; i < intRows; i++)
			{
				DataRow drEach = dtTruck.NewRow();
				drEach[0] = "";
				drEach[1] = "";
				drEach[2] = "";
				drEach[3] = "";
				drEach[4] = "";

				dtTruck.Rows.Add(drEach);
			}

			DataSet dsTruckFields = new DataSet();
			dsTruckFields.Tables.Add(dtTruck);
			SessionDS sessionDS = new SessionDS();
			sessionDS.ds = dsTruckFields;
			sessionDS.DataSetRecSize = dsTruckFields.Tables[0].Rows.Count;
			sessionDS.QueryResultMaxSize = sessionDS.DataSetRecSize;
			return sessionDS;
		}

		private void btnSearch_Click(object sender, System.EventArgs e)
		{
			StringBuilder strQry = new StringBuilder();
			strQry.Append("select TruckID, Description, LicensePlate, Type,Base_Location from MF_Vehicles where applicationid='");
			strQry.Append(strAppID);
			strQry.Append("' and enterpriseid = '");
			strQry.Append(strEnterpriseID);
			strQry.Append("' and ((convert(varchar(8),getdate(),112) between convert(varchar(8),isnull(Initial_in_service_date,''),112) "+
							"	and convert(varchar(8),isnull(Disposal_Date,getdate()),112) and Date_Out_Of_Service is null and Date_In_Service is null) "+
							"	or "+
							"	(convert(varchar(8),getdate(),112) between convert(varchar(8),isnull(Initial_in_service_date,''),112) "+
   							"	and convert(varchar(8),isnull(Date_Out_Of_Service,''),112) and Date_Out_Of_Service is not null and Date_In_Service is null) "+
							"	or "+
							"	(convert(varchar(8),getdate(),112) between convert(varchar(8),isnull(Date_In_Service,''),112) "+
   							"	and convert(varchar(8),isnull(Disposal_Date,getdate()),112) and Date_Out_Of_Service is not null and Date_In_Service is not null)) ");
			//strQry.Append("'");
			if (txtTruckID.Text.ToString() != null && txtTruckID.Text.ToString() != "")
			{
				strQry.Append(" and TruckID like '%");
				strQry.Append(Utility.ReplaceSingleQuote(txtTruckID.Text.ToString()));
				strQry.Append("%'");
			}
			if (txtDesc.Text.ToString() != null && txtDesc.Text.ToString() != "")
			{
				strQry.Append(" and Description like '%");
				strQry.Append(Utility.ReplaceSingleQuote(txtDesc.Text.ToString()));
				strQry.Append("%'");
			}
			if (txtLicense.Text.ToString() != null && txtLicense.Text.ToString() != "")
			{
				strQry.Append(" and licenseplate like N'%");
				strQry.Append(Utility.ReplaceSingleQuote(txtLicense.Text.ToString()));
				strQry.Append("%'");
			}
			if(ddlType.SelectedItem.Value!="")
			{
				strQry.Append(" and type = '");
				strQry.Append(ddlType.SelectedItem.Value+"'");
			}

			if(ddlLocation.SelectedItem.Value!="")
			{
				strQry.Append(" and Base_Location = '");
				strQry.Append(ddlLocation.SelectedItem.Value+"'");
			}
			else
			{

				string strBatchID = Request.QueryString["BatchID"];
				if(strBatchID != null)
				{
					strQry.Append(" and (Base_Location IN (SELECT origin_station from Delivery_Path WHERE  (delivery_path.applicationid = '" + strAppID +  "') AND (delivery_path.enterpriseid = '" + strEnterpriseID + "')");
					if(strBatchID != "")
						strQry.Append("  AND (delivery_path.path_code = '" + strBatchID + "')");
					strQry.Append("))");
				}
			}

			String strSQLQuery = strQry.ToString();
			ViewState["SQL_QUERY"] = strSQLQuery;
			dgTruck.CurrentPageIndex = 0;
			ViewState["STATUS_DS"] = m_sdsTruckCode;
			ShowCurrentPage();
		}

		private void ShowCurrentPage()
		{
			int iStartIndex = dgTruck.CurrentPageIndex * dgTruck.PageSize;
			String sqlQuery = (String)ViewState["SQL_QUERY"];
			m_sdsTruckCode = GetTruckCodeDS(iStartIndex,dgTruck.PageSize,sqlQuery);
			decimal pgCnt = (m_sdsTruckCode.QueryResultMaxSize - 1)/dgTruck.PageSize;
			if(pgCnt < dgTruck.CurrentPageIndex)
			{
				dgTruck.CurrentPageIndex = System.Convert.ToInt32(pgCnt);
			}
			dgTruck.SelectedIndex = -1;
			dgTruck.EditItemIndex = -1;

			BindGrid();
			//getPageControls(Page);
		}

		private SessionDS GetTruckCodeDS(int iCurrent, int iDSRecSize, String strQuery)
		{
			SessionDS sessionDS = null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("Vehicles.aspx.cs","GetTruckCodeDS","EDB101","DbConnection object is null!!");
				return sessionDS;
			}
			sessionDS = dbCon.ExecuteQuery(strQuery,iCurrent,iDSRecSize,"CountryStateCode");
			return  sessionDS;
		}

		private void dgTruck_PageIndexChanged(object source, System.Web.UI.WebControls.DataGridPageChangedEventArgs e)
		{
			dgTruck.CurrentPageIndex = e.NewPageIndex;
			ShowCurrentPage();
			dgTruck.SelectedIndex = -1;
			dgTruck.EditItemIndex = -1;
			BindGrid();
		}

		private void dgTruck_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			String strFormID = Request.Params["FORMID"];
			int iSelIndex = dgTruck.SelectedIndex;
			DataGridItem dgRow = dgTruck.Items[iSelIndex];
			strTruckCode = dgRow.Cells[0].Text;
			String strAirTruck = Request.Params["AirTruck"];
			if (strAirTruck != null)
			{
				windowClose(strFormID, "ddlAirTruckID",strTruckCode);
			}
			else
			{
				windowClose(strFormID, "ddlTruckID",strTruckCode);
			}
		}

		private void windowClose(String formID, String strddlTruckID, String strExceptTextBoxID)
		{
			String sScript = "";
			sScript += "<script language=javascript>";
			if(formID.Equals("ShipmentUpdate") || (formID.Equals("ShipmentUpdateRoute")))
			{
				sScript += "  window.opener."+formID+".txtStatusCode.value =\""+strTruckCode+"\";" ;				
			}
			else if(formID.Equals("ShipmentTrackingQuery"))
			{
				sScript += "  window.opener."+formID+".txtStatusCode.value =\""+strTruckCode+"\";" ;				
			}
			else if(formID.Equals("ShipmentTrackingQueryRpt"))
			{
				sScript += "  window.opener."+formID+".txtStatusCode.value =\""+strTruckCode+"\";" ;				
			}
			else if(formID.Equals("ShipmentExceptionReport"))
			{
				sScript += "  window.opener."+formID+".txtStatus.value = \""+strTruckCode+"\";" ;				
			}
			else
			{
				sScript += "  window.opener."+formID+"."+strddlTruckID+".value = \""+strTruckCode+"\";" ;				
			}
			sScript += "  window.close();";
			sScript += "</script>";
			Response.Write(sScript);
		}
	}
}
