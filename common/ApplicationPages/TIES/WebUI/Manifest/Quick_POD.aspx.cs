using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.common.classes;
using com.common.util;
using com.common.applicationpages;
using com.ties.DAL;
using com.common.RBAC;
using com.ties.classes;
using TIESClasses;
using TIESDAL;
using System.Text;
using System.Configuration;
using System.Data.OleDb;
using System.Text.RegularExpressions;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using com.common.DAL;
using Cambro.Web.DbCombo;

namespace TIES.WebUI.Manifest
{
	/// <summary>
	/// Summary description for Quick_POD.
	/// </summary>
	public class Quick_POD : BasePage
	{
		#region Web Form Designer generated code

		protected System.Web.UI.WebControls.Label Header1;
		protected System.Web.UI.WebControls.Label lblErrrMsg;
		protected System.Web.UI.WebControls.ValidationSummary reqSummary;
		protected System.Web.UI.WebControls.Label lblColConsignment;
		protected System.Web.UI.WebControls.Label lblColService;
		protected System.Web.UI.WebControls.Label lblColDestination;
		protected System.Web.UI.WebControls.Label lblColPkgs;
		protected System.Web.UI.WebControls.Label lblColDataTime;
		protected System.Web.UI.WebControls.Label lblColLoc;
		protected System.Web.UI.WebControls.Label lblColDestDC;
		protected System.Web.UI.WebControls.ListBox lbSIPCon;
		protected System.Web.UI.HtmlControls.HtmlInputButton hddConNo;
		protected System.Web.UI.HtmlControls.HtmlInputHidden hidHost;
		protected System.Web.UI.WebControls.Button btnExecQry;
		protected System.Web.UI.WebControls.Button btnQry;
		protected System.Web.UI.WebControls.Button btnSave;
		protected System.Web.UI.HtmlControls.HtmlGenericControl fsCreateSIP;
		protected System.Web.UI.WebControls.Label lblColPOD;
		protected System.Web.UI.WebControls.Label lblColUser;
		protected System.Web.UI.WebControls.Label lblConNo;
		protected com.common.util.msTextBox txtConNo;
		protected System.Web.UI.WebControls.Label lblOriginDC;
		protected System.Web.UI.WebControls.Label lblDestPostCode;
		protected com.common.util.msTextBox txtDestPostCode;
		protected System.Web.UI.WebControls.Label lblProvince;
		protected System.Web.UI.WebControls.Label lbltmpDestDC;
		protected System.Web.UI.WebControls.Label lblServiceType;
		protected System.Web.UI.WebControls.DropDownList ddlServiceType;
		protected System.Web.UI.WebControls.Label lblNumOfPackages;
		protected com.common.util.msTextBox txtPkgNum;
		protected System.Web.UI.WebControls.Label lblPOD_Detail;
		protected System.Web.UI.WebControls.Label lblActual_Datetine;
		protected com.common.util.msTextBox txtActual;
		protected System.Web.UI.WebControls.Label lblRemark;
		protected System.Web.UI.WebControls.TextBox txtRemark;
		protected System.Web.UI.WebControls.Label lblConsignee;
		protected com.common.util.msTextBox txtConsignee;
		protected System.Web.UI.HtmlControls.HtmlInputHidden hidPort;
		protected Cambro.Web.DbCombo.DbCombo DbComboOriginDC;

		#endregion

		String m_strAppID=null;
		String m_strEnterpriseID=null;
		String m_strCulture=null;

		protected System.Web.UI.HtmlControls.HtmlInputText txtConNo_;
		//protected System.Web.UI.HtmlControls.HtmlInputButton hddConNo;
		public int CharBarcodeCutStartIndex = Convert.ToInt32(ConfigurationSettings.AppSettings["32CharBarcodeCutStartIndex"]);
		
		//protected System.Web.UI.WebControls.ddlOriginDC;
		public int CharBarcodeCutLength= Convert.ToInt32(ConfigurationSettings.AppSettings["32CharBarcodeCutLength"]);

		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here

			getSessionTimeOut(true);
			// Put user code to initialize the page here'
			txtConNo.Attributes.Add("onblur","return suppressOnBluer(this);");
			//txtConNo.Attributes.Add("onkeyDown", "return suppressNonEng(event,this);");

			txtConNo.Attributes.Add("onpaste", "return true;");
			txtConNo.Attributes.Add("onkeyUp", "btnExecQry_Click();");

			utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings,Page.Session);
			m_strAppID = utility.GetAppID();
			m_strEnterpriseID = utility.GetEnterpriseID();
			m_strCulture = utility.GetUserCulture();

			DbComboOriginDC.ClientOnSelectFunction="makeUppercase('DbComboOriginDC:ulbTextBox');";
			DbComboOriginDC.RegistrationKey = System.Configuration.ConfigurationSettings.AppSettings["DbComboRegKey"];
			DbComboOriginDC.Text="";
			DbComboOriginDC.Value="";
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnQry.Click += new System.EventHandler(this.btnQry_Click);
			this.btnExecQry.Click += new System.EventHandler(this.btnExecQry_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		#region Event Method
		
		private void btnQry_Click(object sender, System.EventArgs e)
		{
			
		}

		private void btnExecQry_Click(object sender, System.EventArgs e)
		{
			string ConNo = "";
			if(Utility.ValidateConsignmentNo(txtConNo.Text.Trim(), ref ConNo)==false)
			{
				lblErrrMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"CREATE_SIP_MSG_CONSNO_INVALID",m_strCulture); 
				setFocusCtrl(txtConNo.ClientID);
				return;
			}
			txtConNo.Text = ConNo;
		}

		private void hddConNo_ServerClick(object sender, System.EventArgs e)
		{
			string ConNo="";
			if(Utility.ValidateConsignmentNo(txtConNo.Text.Trim(),ref ConNo)==false)
			{
				if(txtConNo.Text.Trim() != "")
				{
					lblErrrMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"CREATE_SIP_MSG_CONSNO_INVALID",m_strCulture); 
					setFocusCtrl(txtConNo.ClientID);
				}
				return;
			}
			txtConNo.Text=ConNo;
			
			if (txtConNo.Text!="")
			{
				DataSet ds = new DataSet();
				string con = txtConNo.Text.ToString();
				//				if(ValidateFedexFormat(txtConNo.Text.ToString().Trim()))
				//				{
				//					con = txtConNo.Text.Substring(1,CharBarcodeCutLength);
				//					//con = con.Substring(0,4)+" "+con.Substring(4,4)+" "+con.Substring(8);
				//
				//				}
				//				else 
				if(txtConNo.Text.Length==32)//Fedex32 Format
				{
					con= txtConNo.Text.Substring(CharBarcodeCutStartIndex,CharBarcodeCutLength);
				}
				else
				{
					string[] tmpcon = con.Split(' ');
					con=tmpcon[0];

				}
												
				ds= SWB_Emulator.SWB_SIP_Read(m_strAppID,m_strEnterpriseID,con);
				if (ds.Tables[0].Rows.Count>0)
				{
					txtConNo.Enabled=false;
					bindServiceTypefromPostalCode(ds.Tables[0].Rows[0]["dest_postal_code"].ToString());
					txtConNo.Text = ds.Tables[0].Rows[0]["consignment_no"].ToString();
					txtDestPostCode.Text = ds.Tables[0].Rows[0]["dest_postal_code"].ToString();
					bindServiceTypefromPostalCode(txtDestPostCode.Text);
					if(ddlServiceType.Items.FindByValue(ds.Tables[0].Rows[0]["service_code"].ToString())!=null)
					{
						ddlServiceType.SelectedValue= ds.Tables[0].Rows[0]["service_code"].ToString();
					}
					else
					{
						ddlServiceType.Items.Insert(0," ");
						ddlServiceType.SelectedIndex=0;
						lblErrrMsg.Text = "Invalid Service Type.";
						setFocusCtrl(ddlServiceType.ClientID);
						//return;
					}
					txtPkgNum.Text= ds.Tables[0].Rows[0]["number_of_package"].ToString();
					GetProvinceFromPostalCode();

					txtConNo.ReadOnly=true;

					//setFocusCtrl(btnUpdate.ClientID);				
				}
				else
				{
					txtConNo.Enabled=true;
					//txtConNo.Text =hddConNo.Value;
					txtPkgNum.Text="1";

					txtConNo.ReadOnly=false;

					//setFocusCtrl(btnInsert.ClientID);
				}
				setFocusCtrl(txtDestPostCode.ClientID);
				
			}	
		}


		[Cambro.Web.DbCombo.ResultsMethodAttribute(true)]
		public static object DbComboDistributionCenterSelectOrigin(Cambro.Web.DbCombo.ServerMethodArgs args)
		{
			Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings,HttpContext.Current.Session);
			string strAppID = util.GetAppID();
			string strEnterpriseID = util.GetEnterpriseID();	

			DataSet dataset = new DataSet();
			dataset = com.ties.classes.DbComboDAL.DistributionCenterQuery(strAppID,strEnterpriseID,args);
			return dataset;
		}
		#endregion

		#region Function

		private void setFocusCtrl(string ctrlName)
		{
			string script="<script langauge='javacript'>";
			script+= "document.all['"+ ctrlName +"'].focus();";
			script+= "</script>";
			Page.RegisterStartupScript("setFocus"+DateTime.Now.ToString("ddMMyyyyhhmmss"),script);
		
		}

		private void bindServiceTypefromPostalCode(string receipPostCode)
		{
			string OriginDC = string.Empty;
//			string OriginDC = DbComboOriginDC.Text;
			if(txtConNo.Text!="")
			{	
				DataSet ds = new DataSet();
				ds= SWB_Emulator.SWB_SIP_Read(m_strAppID,m_strEnterpriseID,txtConNo.Text);
				if(ds.Tables[0].Rows.Count>0)
				{
					OriginDC=ds.Tables[0].Rows[0]["origin_DC"].ToString();
				}
			}
			DataSet dstmp = ConsignmentNoteDAL.SearchBestService(m_strAppID,m_strEnterpriseID,OriginDC,receipPostCode);

			string strBestService="";
			if(dstmp.Tables[0].Rows.Count>0)
			{
				strBestService=dstmp.Tables[0].Rows[0]["service_code"].ToString();
			}
			DataSet dsDlvType = Service.ReadBestService(m_strAppID,m_strEnterpriseID,OriginDC,receipPostCode);
			if (dsDlvType!=null)
			{
				ddlServiceType.DataSource = dsDlvType;
				ddlServiceType.DataTextField = "service_code"; 
				ddlServiceType.DataValueField="service_code";
				ddlServiceType.DataBind();
			}
			if(dsDlvType == null || dsDlvType.Tables.Count == 0 || dsDlvType.Tables[0].Rows.Count == 0)
			{
				txtDestPostCode.Text = "";
				lblProvince.Text="";
				//ddlServiceType.Enabled=false;
				lblErrrMsg.Text = "No service to this postal code.";
				setFocusCtrl(txtDestPostCode.ClientID);
				return;

			}			
		}

		private void GetProvinceFromPostalCode()
		{
			Zipcode zipcode = new Zipcode();
			zipcode.Populate(m_strAppID,m_strEnterpriseID,txtDestPostCode.Text.ToString());
			DataSet dsCountZip = new DataSet();
			dsCountZip = zipcode.ckZipcodeInSystem(m_strAppID, m_strEnterpriseID,txtDestPostCode.Text.ToString());
			if(dsCountZip.Tables[0].Rows.Count>0)
			{
				lblProvince.Text = dsCountZip.Tables[0].Rows[0]["state_code"].ToString();
				DataSet dsDC = ImportConsignmentsDAL.getDCToDestStation(m_strAppID, m_strEnterpriseID,
					0, 0, zipcode.DeliveryRoute).ds;
				lbltmpDestDC.Text  = dsDC.Tables[0].Rows[0]["origin_station"].ToString();

			}
			else
			{
				lblErrrMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"CREATE_SIP_MSG_DESTPOSTALCODE_ERR",m_strCulture); 
			}
		}


		#endregion

	}
}
