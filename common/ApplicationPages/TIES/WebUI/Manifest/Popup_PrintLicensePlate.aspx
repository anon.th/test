<%@ Import Namespace="com.common.util" %>
<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<%@ Page language="c#" Codebehind="Popup_PrintLicensePlate.aspx.cs" AutoEventWireup="false" Inherits="TIES.WebUI.Manifest.Popup_PrintLicensePlate" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Popup_PrintLicensePlate</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../css/Styles.css" type="text/css" rel="stylesheet">
		<!--#INCLUDE FILE="../msFormValidations.inc"-->
		<script language="javascript" src="../Scripts/settingScrollPosition.js"></script>
		<script language="javascript">
		function PrintRemotePDFFile(pdfPath) 
		{
			//pdfPath = "C:\\Inetpub\\wwwroot\\BSADBL\\TIES\\WebUI\\Export\\999900065.pdf"
			//pdfPath.replace("\","\\");
			var retVal = Printer.PrintRemotePDFFile(pdfPath);	
			window.setTimeout(window.close(),10);
		}
		function SetScrollPosition_page()
		{
			var sHostname = window.location.hostname;
			var sPort = window.location.port;
			
			document.getElementById('hidHost').value = sHostname; 
			document.getElementById('hidPort').value = sPort; 
			//alert(document.getElementById('hidHost').value);
			
		}
		</script>
	</HEAD>
	<body MS_POSITIONING="GridLayout" onload="SetScrollPosition_page();">
		<form id="Form1" method="post" runat="server">
			<FONT face="Tahoma">
				<asp:button id="btnPrint" style="Z-INDEX: 101; POSITION: absolute; TOP: 56px; LEFT: 32px" runat="server"
					CssClass="queryButton" Width="120px" Text="Print"></asp:button><asp:button id="btnCancel" style="Z-INDEX: 102; POSITION: absolute; TOP: 56px; LEFT: 160px"
					runat="server" CssClass="queryButton" Width="120px" Text="Cancel"></asp:button><asp:radiobutton id="rdPrintAll" style="Z-INDEX: 103; POSITION: absolute; TOP: 128px; LEFT: 32px"
					runat="server" CssClass="tableRadioButton" Text=" Print license plates for all packages" GroupName="PrintLicensePlates" AutoPostBack="True"></asp:radiobutton><asp:radiobutton id="rdPrintPkgRange" style="Z-INDEX: 104; POSITION: absolute; TOP: 168px; LEFT: 32px"
					runat="server" CssClass="tableRadioButton" Text=" Print only packages :" Checked="True" GroupName="PrintLicensePlates" AutoPostBack="True"></asp:radiobutton><asp:radiobutton id="rdPrintPkgList" style="Z-INDEX: 105; POSITION: absolute; TOP: 208px; LEFT: 32px"
					runat="server" CssClass="tableRadioButton" Text=" Select packages to print from list :" GroupName="PrintLicensePlates" AutoPostBack="True"></asp:radiobutton><asp:label id="Label1" style="Z-INDEX: 106; POSITION: absolute; TOP: 16px; LEFT: 32px" runat="server"
					CssClass="mainTitleSize" Width="352px">Print Selected License Plates</asp:label><asp:textbox id="txtPrintPkgRange" style="Z-INDEX: 107; POSITION: absolute; TOP: 168px; LEFT: 184px"
					runat="server" Width="176px" Height="24px" CssClass="textField"></asp:textbox><asp:label id="Label2" style="Z-INDEX: 108; POSITION: absolute; TOP: 176px; LEFT: 392px" runat="server"
					CssClass="tableLabel">For example : 1-3,5,7</asp:label><asp:label id="lblConNo" style="Z-INDEX: 109; POSITION: absolute; TOP: 288px; LEFT: 288px"
					runat="server" CssClass="tableLabel"></asp:label><asp:listbox id="lbSelectedPackage" style="Z-INDEX: 110; POSITION: absolute; TOP: 240px; LEFT: 48px"
					runat="server" Width="64px" Height="280px" SelectionMode="Multiple"></asp:listbox>
				<asp:Label id="lblErrorMsg" style="Z-INDEX: 111; POSITION: absolute; TOP: 96px; LEFT: 40px"
					runat="server" CssClass="errorMsgColor"></asp:Label></FONT> <INPUT type="hidden" id="hidHost" runat="server" NAME="hhost">
			<INPUT type="hidden" id="hidPort" runat="server" NAME="hport">
		</form>
	</body>
</HTML>
