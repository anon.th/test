<%@ Import Namespace="com.common.util" %>
<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<%@ Page language="c#" Codebehind="Mf_Staff.aspx.cs" AutoEventWireup="false" Inherits="com.ties.Mf_Staff" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>MF_Staff</title>
		<LINK href="../css/styles.css" type="text/css" rel="stylesheet">
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<!--#INCLUDE FILE="../msFormValidations.inc"-->
		<script language="javascript" src="../Scripts/settingScrollPosition.js"></script>
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="DeliveryPath" method="post" runat="server">
			<asp:label id="lblMainheading" style="Z-INDEX: 101; LEFT: 33px; POSITION: absolute; TOP: 11px"
				runat="server" CssClass="mainTitleSize" Height="25px" Width="201px"> Staffs</asp:label><asp:label id="lblErrorMessage" style="Z-INDEX: 106; LEFT: 35px; POSITION: absolute; TOP: 78px"
				runat="server" CssClass="errorMsgColor" Width="669px"></asp:label><asp:button id="btnInsert" style="Z-INDEX: 105; LEFT: 229px; POSITION: absolute; TOP: 47px"
				runat="server" CssClass="queryButton" Width="51px" CausesValidation="False" Text="Insert"></asp:button><asp:button id="btnExecuteQuery" style="Z-INDEX: 104; LEFT: 99px; POSITION: absolute; TOP: 47px"
				runat="server" CssClass="queryButton" CausesValidation="False" Text="ExecuteQuery"></asp:button><asp:button id="btnQuery" style="Z-INDEX: 103; LEFT: 32px; POSITION: absolute; TOP: 47px" runat="server"
				CssClass="queryButton" Width="67px" CausesValidation="False" Text="Query"></asp:button><asp:datagrid id="dgStaff" style="Z-INDEX: 102; LEFT: 32px; POSITION: absolute; TOP: 113px" runat="server"
				Width="650px" OnDeleteCommand="dgStaff_Delete" OnPageIndexChanged="dgStaff_PageChange" AutoGenerateColumns="False" AllowPaging="True" AllowCustomPaging="True" OnItemDataBound="dgStaff_ItemDataBound"
				OnCancelCommand="dgStaff_Cancel" OnUpdateCommand="dgStaff_Update" OnEditCommand="dgStaff_Edit">
				<ItemStyle Height="20px" CssClass="gridfield"></ItemStyle>
				<Columns>
					<asp:EditCommandColumn ButtonType="LinkButton" UpdateText="&lt;IMG SRC='../images/butt-update.gif' border=0 title='Update' &gt;"
						CancelText="&lt;IMG SRC='../images/butt-cancel.gif' border=0 title='Cancel' &gt;" EditText="&lt;IMG SRC='../images/butt-edit.gif' border=0 title='Edit' &gt;">
						<HeaderStyle CssClass="gridHeading"></HeaderStyle>
						<ItemStyle HorizontalAlign="Left"></ItemStyle>
					</asp:EditCommandColumn>
					<asp:ButtonColumn Text="&lt;IMG SRC='../images/butt-delete.gif' border=0 title='Delete' &gt;" CommandName="Delete">
						<HeaderStyle CssClass="gridHeading"></HeaderStyle>
						<ItemStyle HorizontalAlign="Left"></ItemStyle>
					</asp:ButtonColumn>
					<asp:TemplateColumn HeaderText="Employee ID">
						<HeaderStyle Font-Bold="True" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle CssClass="gridField"></ItemStyle>
						<ItemTemplate>
							<asp:Label id=lblEmpId runat="server" Width="100px" CssClass="gridLabel" Text='<%#DataBinder.Eval(Container.DataItem,"emp_id")%>'>
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<cc1:mstextbox id=txtEmpId Width="100px" CssClass="gridTextBox" Text='<%#DataBinder.Eval(Container.DataItem,"emp_id")%>' Runat="server" MaxLength="10" TextMaskType="msUpperAlfaNumeric" Enabled="True">
							</cc1:mstextbox>
							<asp:RequiredFieldValidator id="rvEmpId" Runat="server" ErrorMessage="Employee ID " ControlToValidate="txtEmpId"
								Display="None" BorderWidth="0"></asp:RequiredFieldValidator>
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Employee Name">
						<HeaderStyle Font-Bold="True" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle CssClass="gridField"></ItemStyle>
						<ItemTemplate>
							<asp:Label id=lblEmpNa Width="200px" CssClass="gridLabel" Text='<%#DataBinder.Eval(Container.DataItem,"emp_name")%>' Runat="server">
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<asp:TextBox id=txtEmpNa runat="server" Width="200px" CssClass="gridTextBox" Text='<%#DataBinder.Eval(Container.DataItem,"emp_name")%>' MaxLength="200">
							</asp:TextBox>
							<asp:RequiredFieldValidator id="rvEmpName" Runat="server" ErrorMessage="Employee Name " ControlToValidate="txtEmpNa"
								Display="None" BorderWidth="0"></asp:RequiredFieldValidator>
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Employee Type">
						<HeaderStyle CssClass="gridHeading"></HeaderStyle>
						<ItemStyle CssClass="gridField"></ItemStyle>
						<ItemTemplate>
							<asp:DropDownList id="ddlEmpType" Width="100px" CssClass="gridTextBox" OnSelectedIndexChanged="chkDdlEmpType"
								Runat="server" AutoPostBack="True"></asp:DropDownList>
						</ItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Mobile No.">
						<HeaderStyle CssClass="gridHeading"></HeaderStyle>
						<ItemStyle CssClass="gridField"></ItemStyle>
						<ItemTemplate>
							<asp:Label ID="lblMobileNo" CssClass="gridLabel" Text='<%#DataBinder.Eval(Container.DataItem, "mobileno")%>' Runat=server Width=100px>
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<cc1:mstextbox id="txtMobileNo" CssClass="gridTextBox" Text='<%#DataBinder.Eval(Container.DataItem, "mobileno")%>' Runat="server" Width=100px MaxLength="10" TextMaskType="msUpperAlfaNumeric">
							</cc1:mstextbox>
							<asp:RequiredFieldValidator id="rvMobileNo" Runat="server" ErrorMessage="Moblile No. " ControlToValidate="txtMobileNo"
								Display="None" BorderWidth="0"></asp:RequiredFieldValidator>
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Base Location">
						<HeaderStyle CssClass="gridHeading"></HeaderStyle>
						<ItemStyle CssClass="gridField"></ItemStyle>
						<ItemTemplate>
							<asp:DropDownList id="ddlBaseLocation" Width="70px" CssClass="gridTextBox" Runat="server" AutoPostBack="True"></asp:DropDownList>
						</ItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Initial In Service Date">
						<HeaderStyle CssClass="gridheading"></HeaderStyle>
						<ItemStyle CssClass="gridField"></ItemStyle>
						<ItemTemplate>
							<asp:Label id=lblInitInServiceDate Width="110px" CssClass="gridlabel" Text='<%# DataBinder.Eval(Container.DataItem,"initial_in_service_date", "{0:dd/MM/yyyy}")%>' Runat="server">
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<cc1:mstextbox id=txtInitInServiceDate Width="110px" CssClass="textField" Text='<%# DataBinder.Eval(Container.DataItem,"initial_in_service_date", "{0:dd/MM/yyyy}")%>' Runat="server" AutoPostBack="True" MaxLength="16" TextMaskType="msDate" Enabled="True" TextMaskString="99/99/9999">
							</cc1:mstextbox>
							<asp:RequiredFieldValidator id="rvInitInServiceDate" Runat="server" ErrorMessage="Initial In Service Date" ControlToValidate="txtInitInServiceDate"
								Display="None" BorderWidth="0"></asp:RequiredFieldValidator>
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Separation Date">
						<HeaderStyle CssClass="gridheading"></HeaderStyle>
						<ItemStyle CssClass="gridField"></ItemStyle>
						<ItemTemplate>
							<asp:Label id=lblSeparationDate Width="110px" CssClass="gridlabel" Text='<%#DataBinder.Eval(Container.DataItem,"separation_date", "{0:dd/MM/yyyy}")%>' Runat="server">
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<cc1:mstextbox id=txtSeparationDate Width="110px" CssClass="textField" Text='<%#DataBinder.Eval(Container.DataItem,"separation_date", "{0:dd/MM/yyyy}")%>' Runat="server" AutoPostBack="True" MaxLength="16" TextMaskType="msDate" Enabled="True" TextMaskString="99/99/9999">
							</cc1:mstextbox>
							<asp:RequiredFieldValidator id="rvSeparationDate" Runat="server" ErrorMessage="Separation Date" ControlToValidate="txtSeparationDate"
								Display="None" BorderWidth="0"></asp:RequiredFieldValidator>
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Out Of Service Date">
						<HeaderStyle CssClass="gridheading"></HeaderStyle>
						<ItemStyle CssClass="gridField"></ItemStyle>
						<ItemTemplate>
							<asp:Label id=lblOutOfServiceDate Width="110px" CssClass="gridlabel" Text='<%#DataBinder.Eval(Container.DataItem,"out_of_service_date", "{0:dd/MM/yyyy}")%>' Runat="server">
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<cc1:mstextbox id=txtOutOfServiceDate Width="110px" CssClass="textField" Text='<%#DataBinder.Eval(Container.DataItem,"out_of_service_date", "{0:dd/MM/yyyy}")%>' Runat="server" AutoPostBack="True" MaxLength="16" TextMaskType="msDate" Enabled="True" TextMaskString="99/99/9999">
							</cc1:mstextbox>
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="In Service Date">
						<HeaderStyle CssClass="gridheading"></HeaderStyle>
						<ItemStyle HorizontalAlign="Right" CssClass="gridField"></ItemStyle>
						<ItemTemplate>
							<asp:Label id=lblInServiceDate Width="110px" CssClass="gridlabel" Text='<%#DataBinder.Eval(Container.DataItem,"in_service_date", "{0:dd/MM/yyyy}")%>' Runat="server">
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<cc1:mstextbox id=txtInServiceDate Width="110px" CssClass="textField" Text='<%#DataBinder.Eval(Container.DataItem,"in_service_date", "{0:dd/MM/yyyy}")%>' Runat="server" AutoPostBack="True" MaxLength="16" TextMaskType="msDate" Enabled="True" TextMaskString="99/99/9999">
							</cc1:mstextbox>
						</EditItemTemplate>
					</asp:TemplateColumn>
				</Columns>
				<PagerStyle NextPageText="Next" PrevPageText="Previous" HorizontalAlign="Right" CssClass="normalText"></PagerStyle>
			</asp:datagrid><asp:validationsummary id="ValidationSummary1" style="Z-INDEX: 111; LEFT: 384px; POSITION: absolute; TOP: 8px"
				Height="36px" Width="346px" Runat="server" HeaderText="The following field(s) are required:<br><br>" ShowMessageBox="True"
				DisplayMode="BulletList" ShowSummary="False"></asp:validationsummary></form>
	</body>
</HTML>
