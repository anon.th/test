using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.common.util;
using com.common.classes;
using com.common.DAL;
using System.Text;
using com.common.applicationpages;
using com.ties.DAL;
using com.common.RBAC;
using CrystalDecisions.ReportSource;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System.Text.RegularExpressions;

namespace TIES.WebUI.Manifest
{
	/// <summary>
	/// Summary description for Popup_PrintLicensePlate.
	/// </summary>
	public class Popup_PrintLicensePlate : BasePopupPage
	{
		protected System.Web.UI.WebControls.Button btnCancel;
		protected System.Web.UI.WebControls.Label Label1;
		protected System.Web.UI.WebControls.Label Label2;
		protected System.Web.UI.WebControls.TextBox txtPrintPkgRange;
		protected System.Web.UI.WebControls.ListBox lbSelectedPackage;
		protected System.Web.UI.WebControls.RadioButton rdPrintAll;
		protected System.Web.UI.WebControls.RadioButton rdPrintPkgRange;
		protected System.Web.UI.WebControls.RadioButton rdPrintPkgList;
		protected System.Web.UI.WebControls.Button btnPrint;
		private string strConsignmentNo = null;
		private string strPkgNo = null;
		private string strAppID = null;
		private string strEnterpriseID = null;
		private string strLocation = null;
		private int noOfPkg1 = 0;
		private int noOfPkg2 = 0;
		private string conName = null;
		protected System.Web.UI.WebControls.Label lblConNo;
		protected System.Web.UI.WebControls.Label lblErrorMsg;
		private string pdfFileName = null;

		ReportDocument rptSource = null;
	
		private void Page_Load(object sender, System.EventArgs e)
		{
			pdfFileName = Request.Params["PdfPath"];
//			btnPrint.Attributes.Add("onclick","javascript:PrintRemotePDFFile('"+pdfFileName+"')");
			utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings,Page.Session);
			strAppID = utility.GetAppID();
			strEnterpriseID = utility.GetEnterpriseID();
			strConsignmentNo = Request.Params["ConNo"];
			strPkgNo = Request.Params["PkgNum"];
			strLocation = Request.Params["Loc"];
			int intPkgNo = Convert.ToInt32(strPkgNo);
			if(!Page.IsPostBack)
			{
				lblConNo.Text = "Consignment "+strConsignmentNo+" has "+strPkgNo+" packages";
//				txtPrintPkgRange.Enabled = false;
				Page.RegisterStartupScript("SetFocus", "<script>document.getElementById('" + txtPrintPkgRange.ClientID + "').focus();</script>");
				for(int i=1;i<(intPkgNo+1);i++)
				{
					ListItem  listItemVar = new ListItem();

					listItemVar.Text = i.ToString();
					listItemVar.Value = i.ToString();
					lbSelectedPackage.Items.Add(listItemVar);
				}
				lbSelectedPackage.DataBind();
				lbSelectedPackage.Enabled = false;
			}
//			PrintReport(strAppID,strEnterpriseID,strConsignmentNo,strPkgNo);
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
			this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
			this.rdPrintAll.CheckedChanged += new System.EventHandler(this.rdPrintAll_CheckedChanged);
			this.rdPrintPkgRange.CheckedChanged += new System.EventHandler(this.rdPrintPkgRange_CheckedChanged);
			this.rdPrintPkgList.CheckedChanged += new System.EventHandler(this.rdPrintPkgList_CheckedChanged);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private void btnCancel_Click(object sender, System.EventArgs e)
		{
			String sScript = "";
			sScript += "<script language=javascript>";				
			sScript += "\n";
			sScript += "window.close();";
			sScript += "\n";
			sScript += "</script>";
			Response.Write(sScript);
		}

		private void btnPrint_Click(object sender, System.EventArgs e)
		{
			lblErrorMsg.Text = "";
			if(rdPrintPkgList.Checked == true && lbSelectedPackage.SelectedIndex < 0)
			{
				lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"PLS_SELECT_PKG",utility.GetUserCulture());
				return;
			}
			PrintReport(strAppID,strEnterpriseID,strConsignmentNo,strPkgNo);
		}

		private DataSet GetLicensePlateData(string strAppID, string strEnterprintID, string strConsignmentNo)
		{
			DbConnection dbCon = null;
			IDbCommand dbCmd = null;
			DataSet DS = null;
			strConsignmentNo=strConsignmentNo.Replace("'","''");
			dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterprintID);
			if (dbCon == null)
			{
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "GetPrintReportLicensePlate", "dbCon is null");
				throw new ApplicationException("Connection to Database failed", null); 
			}

			StringBuilder strQry = new StringBuilder();
			strQry = strQry.Append("select s.consignment_no, null as Payer_ID, s.service_code as Service_Type, state_name  AS Province, dest_postal_code as ZIP_Code, number_of_package, s.recipient_contact_person as REFERENCE_NAME, s.recipient_telephone as TELEPHONE");
			strQry = strQry.Append(", s.recipient_address1 as Address1, s.recipient_address2 as ADDRESS, s.sender_contact_person as CONTACTPERSON, s.sender_name AS SENDER_NAME, s.sender_telephone AS SENDER_TEL, '' AS CUST_REF, NULL AS linehaul1, NULL AS linehaul2, user_id_created, ' ' AS swb_id");
			strQry = strQry.Append(", origin_DC AS dc_location, created_datetime, scanned_in_datetime, delivery_route_code ");
			strQry = strQry.Append("from Consignment_Details left join (SELECT State.state_name, Zipcode, State.applicationid, State.enterpriseid FROM State INNER JOIN Zipcode ON State.applicationid = Zipcode.applicationid AND State.enterpriseid = Zipcode.enterpriseid AND State.country = Zipcode.country AND State.state_code = Zipcode.state_code "); 
			strQry = strQry.Append("WHERE (Zipcode.enterpriseid = '" + strEnterprintID + "')AND (Zipcode.applicationid = '" + strAppID + "') ) a on Consignment_Details.dest_postal_code = a.ZipCode LEFT JOIN Shipment s On Consignment_Details.consignment_no = s.consignment_no");
			strQry = strQry.Append(" where s.consignment_no = '" + strConsignmentNo + "'");

			dbCmd = dbCon.CreateCommand(strQry.ToString(), CommandType.Text);

			try
			{
				DS = (DataSet)dbCon.ExecuteQuery(dbCmd, com.common.DAL.ReturnType.DataSetType);
				return DS;
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("TIESDAL", "PopupPrintLicensePlate", "GetPrintReportLicensePlate", "Error in Query String " + appException.Message);
				throw appException;
			}
			finally
			{
				DS.Dispose();
			}
		}

		private void PrintReport(String strAppID, String strEnterpriseID, String strConsignmentNo, String strPageRange)
		{
			DataSet dsLicensePlate = GetLicensePlateData(strAppID,strEnterpriseID,strConsignmentNo);
			
			if(dsLicensePlate.Tables[0].Rows.Count > 0)
			{
				DataTable dtPrint = new DataTable();
				dtPrint.Columns.Add("consignment");
				dtPrint.Columns.Add("reference_name");
				dtPrint.Columns.Add("telephone");
				dtPrint.Columns.Add("address");
				dtPrint.Columns.Add("contactperson");
				dtPrint.Columns.Add("zip");
				dtPrint.Columns.Add("route1");
				dtPrint.Columns.Add("route2");
				dtPrint.Columns.Add("service");
				dtPrint.Columns.Add("derv");
				dtPrint.Columns.Add("province");
				dtPrint.Columns.Add("sender_name");
				dtPrint.Columns.Add("sender_tel");
				dtPrint.Columns.Add("cust_ref");
				dtPrint.Columns.Add("uid");
				dtPrint.Columns.Add("swbid");
				dtPrint.Columns.Add("dclocation");
				dtPrint.Columns.Add("dateprint");
				dtPrint.Columns.Add("datescan");
				dtPrint.Columns.Add("package");
				dtPrint.Columns.Add("Label");

				if(rdPrintAll.Checked)
				{
					noOfPkg1 = 1;
					noOfPkg2 = int.Parse(strPkgNo);
					dtPrint = fillLicensePlateData(dsLicensePlate,dtPrint);
				}
				else if(rdPrintPkgRange.Checked)
				{
					if(txtPrintPkgRange.Text != "")
					{
						string[] pageSplit = null;
						string[] pkgSplit = null;
						pageSplit = txtPrintPkgRange.Text.Trim().Split(',');
						for(int m=0;m<pageSplit.Length;m++)
						{
							pkgSplit = pageSplit[m].Split('-');
							if(pkgSplit.Length > 1 && pkgSplit.Length < 3)
							{
								if(Regex.IsMatch(pkgSplit[0].Trim(),@"^[0-9]+$") && Regex.IsMatch(pkgSplit[1].Trim(),@"^[0-9]+$"))
								{
									if(int.Parse(pkgSplit[0]) <= int.Parse(strPkgNo) && int.Parse(pkgSplit[1]) <= int.Parse(strPkgNo))
									{
										noOfPkg1 = int.Parse(pkgSplit[0]);
										noOfPkg2 = int.Parse(pkgSplit[1]);
										dtPrint = fillLicensePlateData(dsLicensePlate,dtPrint);
									}
									else
									{
										lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"INVL_PKG",utility.GetUserCulture());
										return;
									}
								}
								else
								{
									lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"INVL_PKG",utility.GetUserCulture());
									return;
								}
							}
							else if(pkgSplit.Length == 1)
							{
								if(Regex.IsMatch(pkgSplit[0].Trim(),@"^[0-9]+$"))
								{
									if(int.Parse(pkgSplit[0]) <= int.Parse(strPkgNo))
									{
										noOfPkg1 = int.Parse(pkgSplit[0]);
										noOfPkg2 = int.Parse(pkgSplit[0]);
										dtPrint = fillLicensePlateData(dsLicensePlate,dtPrint);
									}
									else
									{
										lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"INVL_PKG",utility.GetUserCulture());
										return;
									}
								}
								else
								{
									lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"INVL_PKG",utility.GetUserCulture());
									return;
								}
							}
							else
							{
								lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"INVL_PKG",utility.GetUserCulture());
								return;
							}
						}
					}
					else
					{
						lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"INVL_PKG",utility.GetUserCulture());
						return;
					}
				}
				else if(rdPrintPkgList.Checked)
				{			
					foreach(ListItem listItem in lbSelectedPackage.Items)
					{
						if(listItem.Selected)
						{
//							noOfPkg1 = int.Parse(lbSelectedPackage.SelectedItem.Value);
							noOfPkg1 = int.Parse(listItem.Value);
							noOfPkg2 = int.Parse(listItem.Value);
							dtPrint = fillLicensePlateData(dsLicensePlate,dtPrint);
						}
					}
				}
				
				if(dtPrint.Rows.Count > 0)
				{
					ReportDocument crReportDocument = new TIES.WebUI.ConsignmentNote.LicensePlate();
					string printer = System.Configuration.ConfigurationSettings.AppSettings["PrinterName"].ToString();
					crReportDocument.PrintOptions.PrinterName = System.Configuration.ConfigurationSettings.AppSettings["PrinterName"].ToString();
					
					//String test = System.Configuration.ConfigurationSettings.AppSettings["label_test"].ToString();

					ParameterFieldDefinitions paramFldDefs ;
					ParameterValues paramVals = new ParameterValues() ;
					ParameterDiscreteValue paramDVal = new ParameterDiscreteValue();

					//paramFldDefs = rptSource.DataDefinition.ParameterFields;

					paramFldDefs = crReportDocument.DataDefinition.ParameterFields;

					foreach(ParameterFieldDefinition paramFldDef in paramFldDefs)
					{
						switch(paramFldDef.ParameterFieldName)
						{
							case "label_test":
								paramDVal.Value = System.Configuration.ConfigurationSettings.AppSettings["label_test"].ToString();
								break;
							default:
								continue;
						}
						paramVals = paramFldDef.CurrentValues;
						paramVals.Add(paramDVal);
						paramFldDef.ApplyCurrentValues(paramVals);
					}

				
					//crReportDocument.PrintOptions.PageContentHeight = 
//					po.PageContentHeight = 6;
//					po.PageContentWidth = 4;
					//crReportDocument.PrintOptions.PaperSize = Convert.ChangeType(getPaperSize("9*11").RawKind, CrystalDecisions.Shared.PaperSize);

					ExportOptions crExportOptions  = new ExportOptions();
					
					DiskFileDestinationOptions crDiskFileDestinationOptions  = new DiskFileDestinationOptions();
 
					String Fname = "";

					crReportDocument.SetDataSource(dtPrint);
					//				pdfFileName = "http://"+Request.Url.Host+Request.Url.AbsolutePath;
					//				pdfFileName = pdfFileName.Substring(0,pdfFileName.Length-38)+"/Export/"+conName+".pdf";
					Fname = Server.MapPath("..\\Export") + "\\"+conName+".pdf";
					// Thosapol Yennam (03/09/2013) Comment
					pdfFileName = "http://"+Request.Url.Host+":"+ Request.Url.Port.ToString()+Request.ApplicationPath;
					//pdfFileName = pdfFileName.Substring(0,pdfFileName.Length-38)+"/Export/"+conName+".pdf";
					pdfFileName = pdfFileName+"/WebUI/Export/"+conName+".pdf";

					// Thosapol Yennam (03/09/2013) New Code
					System.Uri uri = HttpContext.Current.Request.Url;
					string sUrl = uri.Scheme + "://" + uri.Host;
					string sPath = PathExport(uri.PathAndQuery);
					string strHost = Request.Form["hidHost"];
					string strPort = Request.Form["hidPort"];
					if (strPort != null )
					{
						if(strPort != String.Empty)
						{
							pdfFileName = sUrl+":"+strPort+sPath+"/Export/"+ conName +".pdf";						
						}
						else
						{
							pdfFileName = sUrl+sPath+"/Export/"+ conName +".pdf";
						}
					}


					crDiskFileDestinationOptions = new DiskFileDestinationOptions();
					crDiskFileDestinationOptions.DiskFileName = Fname;
					crExportOptions = crReportDocument.ExportOptions;

					crExportOptions.DestinationOptions = crDiskFileDestinationOptions;
					crExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
					crExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;

					crExportOptions.FormatOptions = new PdfRtfWordFormatOptions();
					crReportDocument.Export();


					// Thosapol Yennam 26/08/2013 Option Print Auto and Manual
					bool flgAutoPrint = IsAutoPrint(strAppID,strEnterpriseID,strLocation);
					
					if(flgAutoPrint == true ) 
					{
						string strUrl = "./Popup_PrintLicensePlate_Direct.aspx?fname=" +conName+".pdf";
						Session["AutoPrint"] = "onload=\"PrintRemotePDFFile()\"";
						//Session["CreatePrint"] = "<object id=\"pdfDoc\"  name=\"pdfDoc\" classid=\"clsid:CA8A9780-280D-11CF-A24D-444553540000\" width=\"1px\" height=\"1px\" VIEWASTEXT><param name=\"SRC\" value=\""+pdfFileName+"\" /></object>";
						Session["CreatePrint"] = "<object id=\"pdfDoc\"  name=\"pdfDoc\" classid=\"clsid:CA8A9780-280D-11CF-A24D-444553540000\" width=\"1px\" height=\"1px\" VIEWASTEXT><param name=\"SRC\" value=\""+pdfFileName+"\" /></object>";
						ArrayList paramList = new ArrayList();
						paramList.Add(strUrl);
						String sScript = Utility.GetScript("openNewChildParam.js",paramList);
						Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
					}
					else
					{
						string sFilePdf = System.Configuration.ConfigurationSettings.AppSettings["InvoiceExportPath"] +conName+".pdf";
						//string sFilePdf = "C:\\Inetpub\\wwwroot\\PRD\\TIES\\WebUI\\Export\\" +conName+".pdf";			// For Test
						
						bool flgExists = false;
						do
						{
							if( IsfileExists(sFilePdf) == true )
							{
								flgExists = true;
							}
							else
							{
								flgExists = false;
								System.Threading.Thread.Sleep(3000);
							}
						} while (flgExists == false);
						
						//System.IO.FileStream fs = new System.IO.FileStream(sFilePdf, System.IO.FileMode.Open, System.IO.FileAccess.Read);
						//byte[] ar = new byte[(int)fs.Length];
						//fs.Read(ar, 0, (int)fs.Length);
						//fs.Close();

						//Response.AddHeader("content-disposition", "attachment;filename=" + conName+".pdf");
						//Response.ContentType = "application/octectstream";
						//Response.BinaryWrite(ar);
						//Response.End();

						//Response.Clear();
						//Response.ContentType = "application/pdf";
						//Response.WriteFile(sFilePdf);
						//Response.End();

						string paramUrl = "PopUp_PrintCreteSIP.aspx?fname=" + conName+".pdf";
						Response.Write("<script type='text/javascript'>window.open('" + paramUrl + "','_blank');</script>");
					}

				}
			}
		}
		private string PathExport(string sPath)
		{
			string[] arrurl = sPath.Split('/');
			string rUrl = string.Empty;
			for(int i=0; i<arrurl.Length; i++)
			{
				if(arrurl[i] != "Manifest" )    
				{		
					if(arrurl[i] != string.Empty)
					{
						rUrl = rUrl + "/" + arrurl[i];					
					}								
				}
				else
				{
					break;
				}
			}

			return rUrl;
		}
		private DataTable fillLicensePlateData(DataSet dsLicensePlate,DataTable dtPrint)
		{
			foreach (DataRow drLicensePlate in dsLicensePlate.Tables[0].Rows) 
			{
				for(int i=noOfPkg1;i<(noOfPkg2+1);i++)
				{
					DataRow dr;
					dr = dtPrint.NewRow();
					dr["consignment"] = drLicensePlate["consignment_no"];
					dr["reference_name"] = drLicensePlate["reference_name"];
					dr["telephone"] = drLicensePlate["telephone"];
					dr["address"] = drLicensePlate["address"];
					dr["contactperson"] = drLicensePlate["contactperson"];
					dr["zip"] = drLicensePlate["ZIP_Code"];
					dr["route1"] = drLicensePlate["linehaul1"];
					dr["route2"] = drLicensePlate["linehaul2"];
					dr["service"] = drLicensePlate["service_type"];
					dr["derv"] = drLicensePlate["delivery_route_code"];
					dr["province"] = drLicensePlate["province"];
					dr["sender_name"] = drLicensePlate["sender_name"];
					dr["sender_tel"] = drLicensePlate["sender_tel"];
					dr["cust_ref"] = drLicensePlate["cust_ref"];
					dr["uid"] = drLicensePlate["user_id_created"];
					dr["swbid"] = drLicensePlate["swb_id"];
					dr["dclocation"] = drLicensePlate["dc_location"];
					dr["dateprint"] = DateTime.Now.ToString("dd/MM/yyyy HH:mm");//drLicensePlate["created_datetime"];
					dr["datescan"] = drLicensePlate["scanned_in_datetime"];
					dr["package"] = i + " of " + strPkgNo;
					dr["Label"] = "*" + drLicensePlate["consignment_no"].ToString() + "=" + i + "=" + strPkgNo + "=" + drLicensePlate["delivery_route_code"].ToString()+ "*";
					dtPrint.Rows.Add(dr);
				}
				conName = drLicensePlate["consignment_no"].ToString();
				conName="PL"+System.DateTime.Now.ToString("ddMMyyyyhhmmss");
			}
			return dtPrint;
		}

		private void rdPrintAll_CheckedChanged(object sender, System.EventArgs e)
		{
			lblErrorMsg.Text = "";
			lbSelectedPackage.Enabled = false;
			txtPrintPkgRange.Text = "";
			txtPrintPkgRange.Enabled = false;
			lbSelectedPackage.SelectedIndex = -1;
		}

		private void rdPrintPkgList_CheckedChanged(object sender, System.EventArgs e)
		{
			lblErrorMsg.Text = "";
			lbSelectedPackage.Enabled = true;
			txtPrintPkgRange.Text = "";
			txtPrintPkgRange.Enabled = false;
			lbSelectedPackage.SelectedIndex = 0;
		}

		private void rdPrintPkgRange_CheckedChanged(object sender, System.EventArgs e)
		{
			lblErrorMsg.Text = "";
			lbSelectedPackage.Enabled = false;
			txtPrintPkgRange.Enabled = true;
			lbSelectedPackage.SelectedIndex = -1;
			Page.RegisterStartupScript("SetFocus", "<script>document.getElementById('" + txtPrintPkgRange.ClientID + "').focus();</script>");
		}
		private bool IsAutoPrint(string strAppID, string strEnterprintID, string strLocation)
		{
			bool booAutoPrint = false;
			DbConnection dbCon = null;
			IDbCommand dbCmd = null;
			DataSet DS = null;

			dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterprintID);
			if (dbCon == null)
			{
				Logger.LogTraceError("TIESDAL", "IsAutoPrint", "IsAutoPrint", "dbCon is null");
				throw new ApplicationException("Connection to Database failed", null); 
			}

			StringBuilder strQry = new StringBuilder();
			strQry = strQry.Append(" SELECT CanDirectPrint=ISNULL(( ");
			strQry = strQry.Append(" SELECT CASE WHEN Item IS NULL THEN 0 ELSE 1 END ");
			strQry = strQry.Append(" FROM dbo.Core_System_Code A CROSS APPLY dbo.DelimitedSplit8K(code_text, ',') B ");
			strQry = strQry.Append(" WHERE applicationid = '" + strAppID + "' AND culture = '" + strEnterprintID + "' AND codeid = 'LicensePlateDirectPrint' AND item = '" + strLocation + "'), 0) ");


			dbCmd = dbCon.CreateCommand(strQry.ToString(), CommandType.Text);

			try
			{
				DS = (DataSet)dbCon.ExecuteQuery(dbCmd, com.common.DAL.ReturnType.DataSetType);

				foreach (DataRow rw in DS.Tables[0].Rows)
				{
					if (rw[0].ToString() == "1") 
					{
						booAutoPrint = true; 
					}
				}
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("TIESDAL", "PopupPrintLicensePlate", "GetPrintReportLicensePlate", "Error in Query String " + appException.Message);
				throw appException;
			}
			finally
			{
				DS.Dispose();
			}

			return booAutoPrint;

		}

		private bool IsfileExists(string curFile)
		{
			//string curFile = @"c:\temp\test.txt";
			if (System.IO.File.Exists(curFile) == true)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


	}
}
