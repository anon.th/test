<%@ Page language="c#" Codebehind="PopUp_Arrival_Departure.aspx.cs" AutoEventWireup="false" Inherits="TIES.WebUI.Manifest.PopUp_Arrival_Departure" %>
<%@ Import Namespace="com.common.util" %>
<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>PopUp_Arrival_Departure</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK rel="stylesheet" type="text/css" href="../css/Styles.css">
		<!--#INCLUDE FILE="../msFormValidations.inc"-->
		<script type="text/javascript">
			 function KeyDownExe()
			 {
				if (event.keyCode == 9 || event.keyCode == 13)
				{
					document.getElementById("btnEnableRevCom").click();
					//document.getElementById("txtConNo").focus();
				}
			 }
			 
			 function ChkChangeActual()
			 {
				if (document.getElementById('txtValueActDT').value !="")
				{
					if (document.getElementById('txtActualDateDT').value  != document.getElementById('txtValueActDT').value)
					{
								
						if (confirm(document.getElementById('txtmsgActDT').value))
						{
							//alert('yes');
							document.getElementById("btnSave").click();
						}
						else
						{
							//alert(document.getElementById('txtActualDateDT').value);
							//document.getElementById("btnCancelChangeActual").click(); 
							//alert(document.getElementById('txtValueActDT').value);
							document.getElementById('txtActualDateDT').value  = document.getElementById('txtValueActDT').value;
							return false;
						}
					}
				}
			 }
		</script>
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="PopUp_Arrival_Departure" method="post" runat="server">
			<asp:textbox style="Z-INDEX: 106; POSITION: absolute; TOP: 5px; LEFT: 472px" id="hdnbox" runat="server"
				ForeColor="Transparent" BackColor="Transparent" BorderStyle="None" Font-Size="8px" Font-Names="Tahoma"
				Height="4px" Width="8px" AutoPostBack="True"></asp:textbox><asp:label style="Z-INDEX: 101; POSITION: absolute; TOP: 24px; LEFT: 8px" id="Header1" Width="490"
				CssClass="mainTitleSize" Runat="server">Batch Arrivals and Departures</asp:label><asp:label style="Z-INDEX: 107; POSITION: absolute; TOP: 56px; LEFT: 248px" id="lblValISector"
				runat="server" Width="456px" CssClass="errorMsgColor"></asp:label><asp:button style="Z-INDEX: 105; POSITION: absolute; TOP: 64px; LEFT: 720px" id="btnCancel"
				runat="server" Width="96px" CssClass="queryButton" Text="Cancel"></asp:button><asp:button style="Z-INDEX: 104; POSITION: absolute; TOP: 40px; LEFT: 720px" id="btnSave" runat="server"
				Width="96px" CssClass="queryButton" Text="Save"></asp:button>
			<FIELDSET style="Z-INDEX: 103; POSITION: absolute; WIDTH: 248px; HEIGHT: 222px; TOP: 112px; LEFT: 232px"><LEGEND><asp:label id="lblCreateOrUpdateBatch" Width="176px" CssClass="tableHeadingFieldset" Runat="server"
						HEIGHT="16px">Departure / Arrival Records</asp:label></LEGEND>
				<TABLE style="WIDTH: 242.5%; HEIGHT: 129px" id="Table2" class="tablelabel">
					<TR>
						<TD style="WIDTH: 372px" colSpan="2"><asp:radiobutton id="rdbDeparture" runat="server" AutoPostBack="True" CssClass="tablelabel" Text="Departure"
								GroupName="Batch"></asp:radiobutton></TD>
						<TD style="WIDTH: 113px"><asp:radiobutton id="rdbArrival" runat="server" Width="70px" AutoPostBack="True" CssClass="tablelabel"
								Text="Arrival" GroupName="Batch"></asp:radiobutton></TD>
						<TD><FONT face="Tahoma"></FONT></TD>
					</TR>
					<TR>
						<TD style="WIDTH: 148px; HEIGHT: 14px" noWrap>&nbsp;&nbsp;&nbsp;&nbsp;
							<asp:label id="Label7" runat="server" Width="120px" CssClass="tableLabel">Actual Date/ Time:</asp:label></TD>
						<TD style="WIDTH: 207px; HEIGHT: 14px"><cc1:mstextbox id="txtActualDateDT" runat="server" Width="105px" CssClass="textField" MaxLength="16"
								TextMaskString="99/99/9999 99:99" TextMaskType="msDateTime"></cc1:mstextbox></TD>
						<TD style="WIDTH: 113px; HEIGHT: 14px"><FONT face="Tahoma"><asp:label id="Label3" runat="server" Width="111px" CssClass="tableLabel">Actual Date/ Time:</asp:label></FONT></TD>
						<TD style="WIDTH: 100px; HEIGHT: 14px"><cc1:mstextbox id="txtActualDateAR" runat="server" Width="105px" AutoPostBack="True" CssClass="textField"
								MaxLength="16" TextMaskString="99/99/9999 99:99" TextMaskType="msDateTime" OnChange="EnableRevCom()"></cc1:mstextbox></TD>
					</TR>
					<TR>
						<TD style="WIDTH: 148px; HEIGHT: 17px">&nbsp;&nbsp;&nbsp;&nbsp;
							<asp:label id="Label6" runat="server" CssClass="tableLabel">Departure DC:</asp:label></TD>
						<TD style="WIDTH: 207px; HEIGHT: 17px"><asp:dropdownlist id="ddlDepartureDC" runat="server" Width="97px" AutoPostBack="True" CssClass="textField"></asp:dropdownlist></TD>
						<TD style="WIDTH: 113px; HEIGHT: 17px"><asp:label id="Label8" runat="server" CssClass="tableLabel">Arrival DC:</asp:label></TD>
						<TD style="WIDTH: 130px; HEIGHT: 17px"><asp:dropdownlist id="ddlArrivalDC" runat="server" Width="97px" AutoPostBack="True" CssClass="textField"></asp:dropdownlist></TD>
					</TR>
					<TR>
						<TD style="WIDTH: 148px; HEIGHT: 2px">&nbsp;&nbsp;&nbsp;&nbsp;
							<asp:label id="Label5" runat="server" CssClass="tableLabel">Driver ID:</asp:label></TD>
						<TD style="WIDTH: 207px; HEIGHT: 2px"><asp:textbox id="txtDriverID" CssClass="tableLabel" Runat="server"></asp:textbox></TD>
						<TD style="WIDTH: 113px; HEIGHT: 2px"><FONT face="Tahoma"></FONT></TD>
						<TD style="WIDTH: 130px; HEIGHT: 2px"><FONT face="Tahoma"></FONT></TD>
					</TR>
					<TR>
						<TD style="WIDTH: 148px; HEIGHT: 33px">&nbsp;&nbsp;&nbsp;&nbsp;
							<asp:label id="Label4" runat="server" Width="138px" CssClass="tableLabel">Est. Arrival Date/ Time:</asp:label></TD>
						<TD style="WIDTH: 207px; HEIGHT: 33px"><FONT face="Tahoma"><cc1:mstextbox id="txtArrivalDate" runat="server" Width="105px" CssClass="textField" MaxLength="16"
									TextMaskString="99/99/9999 99:99" TextMaskType="msDateTime"></cc1:mstextbox></FONT></TD>
						<TD style="WIDTH: 127px; HEIGHT: 33px" colSpan="2"><FONT face="Tahoma"></FONT><FONT face="Tahoma"><asp:button id="btnRevCom" runat="server" Width="188px" CssClass="queryButton" Text="Receiving is Complete"></asp:button></FONT></TD>
					</TR>
				</TABLE>
				<asp:panel id="pnlAir" runat="server" Height="64px" Width="592px">
					<TABLE style="WIDTH: 590px; HEIGHT: 64px" id="Table3" border="0" cellSpacing="1" cellPadding="1"
						width="590">
						<TR>
							<TD style="WIDTH: 18px"></TD>
							<TD style="WIDTH: 139px"><FONT face="Tahoma"></FONT></TD>
							<TD style="WIDTH: 117px"></TD>
							<TD style="WIDTH: 106px"></TD>
							<TD><FONT face="Tahoma"></FONT></TD>
						</TR>
						<TR>
							<TD style="WIDTH: 18px; HEIGHT: 17px"><FONT face="Tahoma"></FONT></TD>
							<TD style="WIDTH: 139px; HEIGHT: 17px"><FONT face="Tahoma">
									<asp:label id="Label9" runat="server" CssClass="tableLabel">Flight Number :</asp:label></FONT></TD>
							<TD style="WIDTH: 117px; HEIGHT: 17px">
								<cc1:mstextbox style="TEXT-TRANSFORM: uppercase" id="txtFightNo" runat="server" Width="104px" CssClass="textField"
									MaxLength="10" TextMaskType="msUpperAlfaNumericWithUnderscore" name="txtUserID"></cc1:mstextbox></TD>
							<TD style="WIDTH: 106px; HEIGHT: 17px"><FONT face="Tahoma">
									<asp:label id="Label12" runat="server" Width="110px" CssClass="tableLabel">Arriving Truck ID  :</asp:label></FONT></TD>
							<TD style="HEIGHT: 17px"><FONT face="Tahoma">
									<asp:dropdownlist id="ddlAirTruckID" runat="server" Width="152px" CssClass="textField"></asp:dropdownlist>
									<asp:button id="btnAirTruckSearch" runat="server" CssClass="searchButton" Text="..."></asp:button></FONT></TD>
						</TR>
						<TR>
							<TD style="WIDTH: 18px; HEIGHT: 1px"></TD>
							<TD style="WIDTH: 139px; HEIGHT: 1px"><FONT face="Tahoma">
									<asp:label id="Label10" runat="server" CssClass="tableLabel">Departure Date/Time  :</asp:label></FONT></TD>
							<TD style="WIDTH: 117px; HEIGHT: 1px">
								<cc1:mstextbox id="txtAirDepDT" runat="server" Width="105px" CssClass="textField" MaxLength="16"
									TextMaskString="99/99/9999 99:99" TextMaskType="msDateTime"></cc1:mstextbox></TD>
							<TD style="WIDTH: 106px; HEIGHT: 1px">
								<asp:label id="Label13" runat="server" CssClass="tableLabel">Driver ID  :</asp:label></TD>
							<TD style="HEIGHT: 1px">
								<asp:TextBox id="txtAirDriverID" CssClass="tableLabel" Runat="server"></asp:TextBox></TD>
						</TR>
						<TR>
							<TD style="WIDTH: 18px"></TD>
							<TD style="WIDTH: 139px">
								<asp:label id="Label11" runat="server" CssClass="tableLabel">AWB Number  :</asp:label></TD>
							<TD style="WIDTH: 117px"><FONT face="Tahoma">
									<asp:textbox id="txtAWBNo" tabIndex="1" runat="server" Width="104px" CssClass="textField" MaxLength="20"></asp:textbox></FONT></TD>
							<TD style="WIDTH: 106px"><FONT face="Tahoma"></FONT></TD>
							<TD><FONT face="Tahoma"></FONT></TD>
						</TR>
					</TABLE>
				</asp:panel></FIELDSET>
			<TABLE style="Z-INDEX: 102; POSITION: absolute; WIDTH: 232px; HEIGHT: 156px; TOP: 64px; LEFT: 8px"
				id="Table1" border="0" cellSpacing="1" cellPadding="1" width="232">
				<TR>
					<TD><asp:label id="lblBatchNumber" runat="server" CssClass="tableLabel">Batch Number:</asp:label></TD>
					<TD><asp:label id="lblBatchNoDisplay" runat="server" CssClass="tableLabel"></asp:label></TD>
				</TR>
				<TR>
					<TD></TD>
					<TD></TD>
				</TR>
				<TR>
					<TD><asp:label id="lblBatchDate" runat="server" CssClass="tableLabel">Batch Date:</asp:label></TD>
					<TD><FONT face="Tahoma"><asp:label id="lblBatchDateDisplay" runat="server" CssClass="tableLabel"></asp:label></FONT></TD>
				</TR>
				<TR>
					<TD><asp:label id="lblTruckID" runat="server" CssClass="tableLabel">Truck ID:</asp:label></TD>
					<TD><asp:label id="lblTruckIDDisplay" runat="server" CssClass="tableLabel"></asp:label></TD>
				</TR>
				<TR>
					<TD><asp:label id="lblBatchID" runat="server" CssClass="tableLabel">Batch ID:</asp:label></TD>
					<TD><asp:label id="lblBatchIDDisplay" runat="server" CssClass="tableLabel"></asp:label></TD>
				</TR>
				<TR>
					<TD><asp:label id="lblBatchType" runat="server" CssClass="tableLabel">Batch Type:</asp:label></TD>
					<TD><asp:label id="lblBatchTypeDisplay" runat="server" CssClass="tableLabel"></asp:label></TD>
				</TR>
				<TR>
					<TD><FONT face="Tahoma"></FONT></TD>
					<TD></TD>
				</TR>
				<TR>
					<TD><asp:label id="Label1" runat="server" CssClass="tableLabel">User:</asp:label></TD>
					<TD><asp:label id="lblUserDisplay" runat="server" CssClass="tableLabel"></asp:label></TD>
				</TR>
				<TR>
					<TD><asp:label id="Label2" runat="server" CssClass="tableLabel">Location:</asp:label></TD>
					<TD><asp:label id="lblLocDisplay" runat="server" CssClass="tableLabel"></asp:label></TD>
				</TR>
				<TR style="DISPLAY: none" id="trServiceType" runat="server">
					<TD><asp:label id="Label14" runat="server" CssClass="tableLabel">Service Type:</asp:label></TD>
					<TD><asp:label id="lblServiceType" runat="server" CssClass="tableLabel"></asp:label></TD>
				</TR>
			</TABLE>
			<asp:textbox style="Z-INDEX: 108; POSITION: absolute; TOP: 16px; LEFT: 576px" id="txtmsg" runat="server"
				Width="0px"></asp:textbox><asp:button style="Z-INDEX: 109; POSITION: absolute; TOP: 296px; LEFT: 176px" id="btnEnableRevCom"
				runat="server" Width="0px" Text="Button"></asp:button><asp:textbox style="Z-INDEX: 110; POSITION: absolute; TOP: 32px; LEFT: 496px" id="txtValueActDT"
				runat="server" Width="0px"></asp:textbox><asp:textbox style="Z-INDEX: 111; POSITION: absolute; TOP: 400px; LEFT: 512px" id="txtmsgActDT"
				runat="server" Width="0px"></asp:textbox><asp:button style="Z-INDEX: 112; POSITION: absolute; TOP: 376px; LEFT: 184px" id="btnCancelChangeActual"
				runat="server" Width="0px" Text="Button"></asp:button></form>
	</body>
</HTML>
