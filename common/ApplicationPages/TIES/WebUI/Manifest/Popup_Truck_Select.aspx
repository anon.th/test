<%@ Page language="c#" Codebehind="Popup_Truck_Select.aspx.cs" AutoEventWireup="false" Inherits="TIES.WebUI.Manifest.Popup_Truck_Select" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Popup_Truck_Select</title>
		<LINK rel="stylesheet" type="text/css" href="../css/Styles.css">
		<meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<asp:DropDownList id="ddlLocation" style="Z-INDEX: 110; LEFT: 136px; POSITION: absolute; TOP: 88px"
				runat="server" Width="144px" CssClass="ddlLocation"></asp:DropDownList>
			<asp:Button id="btnSearch" style="Z-INDEX: 113; LEFT: 392px; POSITION: absolute; TOP: 88px"
				runat="server" Width="84px" CssClass="buttonProp" Text="Search"></asp:Button>
			<asp:Button id="btnClose" style="Z-INDEX: 108; LEFT: 480px; POSITION: absolute; TOP: 88px" runat="server"
				Width="84px" CssClass="buttonProp" Text="Close"></asp:Button>
			<asp:datagrid id="dgDriver" style="Z-INDEX: 112; LEFT: 56px; POSITION: absolute; TOP: 136px" runat="server"
				Width="522px" SelectedItemStyle-CssClass="gridFieldSelected" OnItemDataBound="dgShipUpdate_Bound"
				AllowCustomPaging="True" PageSize="5" AllowPaging="True" AutoGenerateColumns="False">
				<SelectedItemStyle CssClass="gridFieldSelected"></SelectedItemStyle>
				<ItemStyle CssClass="drilldownField"></ItemStyle>
				<HeaderStyle CssClass="gridHeading"></HeaderStyle>
				<Columns>
					<asp:TemplateColumn HeaderText="Truck ID">
						<HeaderStyle Width="3%" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle Wrap="False"></ItemStyle>
						<ItemTemplate>
							<asp:Label CssClass="gridLabel" ID="lbTruckID" Text='<%#DataBinder.Eval(Container.DataItem,"serial_no")%>' Runat="server">
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<asp:TextBox CssClass="gridTextBox" ID="txTrucID" Text='<%#DataBinder.Eval(Container.DataItem,"serial_no")%>' Runat="server" Enabled="True" MaxLength="12">
							</asp:TextBox>
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Description">
						<HeaderStyle Width="5%" CssClass="gridHeading"></HeaderStyle>
						<ItemTemplate>
							<asp:Label CssClass="gridLabel" ID="lbDesc" Text='<%#DataBinder.Eval(Container.DataItem,"recipient_zipcode")%>' Runat="server" Enabled="True">
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<asp:TextBox CssClass="gridTextBox" ID="txDesc" Text='<%#DataBinder.Eval(Container.DataItem,"recipient_zipcode")%>' Runat="server">
							</asp:TextBox>
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="License Plate">
						<HeaderStyle Width="5%" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle VerticalAlign="Middle"></ItemStyle>
						<ItemTemplate>
							<asp:Label CssClass="gridLabel" ID="lbLicensePlate" Text='<%#DataBinder.Eval(Container.DataItem,"service_code")%>' Runat="server" Enabled="True">
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<asp:TextBox CssClass="gridTextBox" ID="txLicensePlate" Text='<%#DataBinder.Eval(Container.DataItem,"service_code")%>' Runat="server">
							</asp:TextBox>
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Type">
						<HeaderStyle Width="5%" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle VerticalAlign="Middle"></ItemStyle>
						<ItemTemplate>
							<asp:Label CssClass="gridLabel" ID="lbType" Text='<%#DataBinder.Eval(Container.DataItem,"service_code")%>' Runat="server" Enabled="True">
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<asp:TextBox CssClass="gridTextBox" ID="txType" Text='<%#DataBinder.Eval(Container.DataItem,"service_code")%>' Runat="server">
							</asp:TextBox>
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Location">
						<HeaderStyle Width="5%" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle VerticalAlign="Middle"></ItemStyle>
						<ItemTemplate>
							<asp:Label CssClass="gridLabel" ID="lbLocation" Text='<%#DataBinder.Eval(Container.DataItem,"service_code")%>' Runat="server" Enabled="True">
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<asp:TextBox CssClass="gridTextBox" ID="txLocation" Text='<%#DataBinder.Eval(Container.DataItem,"service_code")%>' Runat="server">
							</asp:TextBox>
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:ButtonColumn Text="Select" CommandName="Select">
						<HeaderStyle Width="3%" CssClass="gridHeading"></HeaderStyle>
					</asp:ButtonColumn>
				</Columns>
				<PagerStyle NextPageText="Next" PrevPageText="Previous" HorizontalAlign="Right" CssClass="normalText"></PagerStyle>
			</asp:datagrid>
			<asp:DropDownList id="ddlType" style="Z-INDEX: 111; LEFT: 136px; POSITION: absolute; TOP: 64px" runat="server"
				Width="144px" CssClass="ddlLocation"></asp:DropDownList>&nbsp;
			<asp:Label id="lblLocation" style="Z-INDEX: 109; LEFT: 56px; POSITION: absolute; TOP: 88px"
				runat="server" CssClass="tableLabel">Location</asp:Label>
			<asp:Label id="lblType" style="Z-INDEX: 107; LEFT: 56px; POSITION: absolute; TOP: 64px" runat="server"
				CssClass="tableLabel">Type</asp:Label>
			<asp:Label id="lblLicensePlate" style="Z-INDEX: 106; LEFT: 296px; POSITION: absolute; TOP: 64px"
				runat="server" CssClass="tableLabel">License Plate</asp:Label>
			<asp:Label id="lblTruckID" style="Z-INDEX: 104; LEFT: 56px; POSITION: absolute; TOP: 40px"
				runat="server" CssClass="tableLabel">Truck ID</asp:Label>
			<asp:TextBox id="txtLicensePlate" style="Z-INDEX: 102; LEFT: 376px; POSITION: absolute; TOP: 64px"
				runat="server" Width="194px" CssClass="textField"></asp:TextBox>
			<asp:TextBox id="txtDesc" style="Z-INDEX: 101; LEFT: 376px; POSITION: absolute; TOP: 40px" runat="server"
				Width="194px" CssClass="textField"></asp:TextBox>
			<asp:TextBox id="txtTruckID" style="Z-INDEX: 100; LEFT: 136px; POSITION: absolute; TOP: 40px"
				runat="server" Width="139px" CssClass="textField"></asp:TextBox>
			<asp:Label id="lblDesc" style="Z-INDEX: 103; LEFT: 296px; POSITION: absolute; TOP: 40px" runat="server"
				CssClass="tableLabel">Description</asp:Label></form>
	</body>
</HTML>
