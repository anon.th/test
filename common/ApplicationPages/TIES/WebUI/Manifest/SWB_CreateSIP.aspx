<%@ Import Namespace="com.common.util" %>
<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<%@ Page language="c#" Codebehind="SWB_CreateSIP.aspx.cs" AutoEventWireup="false" Inherits="TIES.WebUI.Manifest.SWB_CreateSIP" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
  <HEAD>
		<TITLE>Scanning Workbench Emulator</TITLE>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK rel="stylesheet" type="text/css" href="../css/Styles.css">
		<!--#INCLUDE FILE="../msFormValidations.inc"-->
		<script language="javascript" src="../Scripts/JScript_TextControl.js"></script>
		<script language="javascript" src="../Scripts/settingScrollPosition.js"></script>
		<script language="javascript">	
		
		function SetScrollPosition_page()
		{
			window.scroll(0,document.forms[0].ScrollPosition.value);
			var sHostname = window.location.hostname;
			var sPort = window.location.port;
			
			document.getElementById('hidHost').value = sHostname; 
			document.getElementById('hidPort').value = sPort; 
			//alert(document.getElementById('hidHost').value);
			
		}
		
		function setValue()
		{
			var s = document.all['inFile'];
			document.all['txtFilePath'].innerText = s.value;
			if(s.value != '') 
				document.all['btnImport'].disabled = false;
			else
				document.all['btnImport'].disabled = true;
		}
	
		function upBrowse()
		{
			document.all['divBrowse'].style.backgroundImage='url(../images/btn-browse-up.GIF)';
		}
		
		function downBrowse()
		{
			document.all['divBrowse'].style.backgroundImage='url(../images/btn-browse-down.GIF)';
		}		
		
		function clearValue()
		{
			if(document.all['txtFilePath'].innerText == '') {
				document.location.reload();
			}
		}

		function IsNumeric(sText)
			{
			var ValidChars = "0123456789.";
			var IsNumber=true;
			var Char;

			 
			for (i = 0; i < sText.length && IsNumber == true; i++) 
				{ 
				Char = sText.charAt(i); 
				if (ValidChars.indexOf(Char) == -1) 
					{
					IsNumber = false;
					}
				}
			return IsNumber;
			   
			}
	function TxtConNoFocus()
	{
		document.getElementById("txtConNo").value = '';
		document.getElementById("txtConNo").focus();
	}

    function KeyDownHandler()
    {
		//debugger;
        // process only the Enter key
         var key;
		if(window.event)  key = window.event.keyCode;     //IE
		else  key = e.which;     //firefox
        if (key == 13)
        {
            // cancel the default submit
            //event.returnValue=false;
            //event.cancel = true;
            // submit the form by programmatically clicking the specified button
            if(document.getElementById("btnInsert").disabled==false)
			{
				//btn = "btnInsert";
				
				 document.getElementById("btnInsert_Print").click();
				 document.getElementById("txtConNo").focus();
			}
			else if(document.getElementById("btnUpdate").disabled==false)
			{
				//btn = "btnUpdate";
				 document.getElementById("btnUpdate").click();
				 document.getElementById("txtConNo").focus();
			}
           
        }
    }
function  suppressNonEng(e,obj)
 {//debugger;
   var key;
   if(window.event)  key = window.event.keyCode;     //IE
   else  key = e.which;     //firefox
	
   if(key >128)  return false;
   else  return UpperMask(obj);//return true;*/
 }		
 
 function suppressOnBluer(e)
 {//alert("");
	//e.value = Check2FontChar2LastChar(e);
	
     var s= e.value;
     e.value = '';
     for(i=0;i<s.length;i++)
     {     if (s.charCodeAt(i)<=128)
			{
				e.value += s.charAt(i);
			}			
      }
    if(s.length!=e.value.length)
    {
    //alert('Other than english character are not supported')
		return false;
    }//debugger;
    //var hddConNo = document.getElementById("hddConNo");
	__doPostBack('hddConNo','');
 }	
		</script>
</HEAD>
	<body onclick="GetScrollPosition();" onunload="window.top.displayBanner.fnCloseAll(0);"
		onload="SetScrollPosition_page();" MS_POSITIONING="GridLayout">
		<form id="SWB_CreateSIP" method="post" runat="server">
			<input style="DISPLAY: none" id="hddConNo" type="button" name="hddCon" runat="server">
			<P><FONT face="Tahoma"></FONT>&nbsp;</P>
			<P><asp:label style="Z-INDEX: 101; POSITION: absolute; TOP: 23px; LEFT: 14px" id="Header1" Runat="server"
					CssClass="mainTitleSize" Width="490">Scanning Workbench Emulator - Create SIP</asp:label></P>
			<P></P>
			<asp:panel id="pnlImport" runat="server">
<FIELDSET 
style="Z-INDEX: 105; POSITION: absolute; WIDTH: 953px; HEIGHT: 48px; TOP: 88px; LEFT: 16px" 
id=fsImport runat="server"><LEGEND>
<asp:label id=lblImportSIPs runat="server" Width="80px" CssClass="tableHeadingFieldset" Height="16px">Import 
								SIPs</asp:label></LEGEND>
<TABLE style="WIDTH: 896px; HEIGHT: 49px" id=Table10>
  <TR>
    <TD>&nbsp;</TD>
    <TD vAlign=bottom>
      <DIV 
      style="BACKGROUND-IMAGE: url(../images/btn-browse-up.GIF); WIDTH: 70px; DISPLAY: inline; HEIGHT: 20px" 
      id=divBrowse onmouseup=upBrowse(); onmousedown=downBrowse();><INPUT 
      onblur=setValue(); 
      style="FILTER: alpha(opacity: 0); BORDER-LEFT: #88a0c8 1px outset; BACKGROUND-COLOR: #e9edf0; WIDTH: 20px; FONT-FAMILY: Arial; COLOR: #003068; FONT-SIZE: 11px; BORDER-TOP: #88a0c8 1px outset; BORDER-RIGHT: #88a0c8 1px outset; TEXT-DECORATION: none" 
      id=inFile onfocus=setValue(); type=file name=inFile 
    runat="server"></DIV></TD>
    <TD vAlign=bottom>&nbsp; 
<asp:textbox id=txtFilePath runat="server" Width="658px" CssClass="textField"></asp:textbox></TD>
    <TD vAlign=bottom>&nbsp; 
<asp:button id=btnImport runat="server" Width="104px" CssClass="queryButton" Text="Import"></asp:button></TD></TR>
  <TR>
    <TD colSpan=4>&nbsp;</TD></TR></TABLE></FIELDSET>
			</asp:panel>
			<P></P>
			<asp:label style="Z-INDEX: 102; POSITION: absolute; TOP: 56px; LEFT: 16px" id="lblErrrMsg"
				runat="server" CssClass="errorMsgColor" Width="720" Height="2"></asp:label><INPUT style="Z-INDEX: 103; POSITION: absolute; TOP: 432px; LEFT: 760px" type="hidden"
				name="ScrollPosition">&nbsp;&nbsp;
			<asp:validationsummary style="Z-INDEX: 104; POSITION: absolute; TOP: 16px; LEFT: 552px" id="reqSummary"
				Runat="server" ShowSummary="False" HeaderText="Please enter the missing  fields." ShowMessageBox="True"
				DisplayMode="BulletList"></asp:validationsummary>
			<FIELDSET style="Z-INDEX: 106; POSITION: absolute; WIDTH: 921px; HEIGHT: 256px; TOP: 176px; LEFT: 16px"
				id="fsCreateSIP" runat="server"><LEGEND><asp:label id="lblCreateSIP" runat="server" CssClass="tableHeadingFieldset" Width="68px" Height="18px">Create 
								SIP</asp:label></LEGEND>
				<TABLE id="Table1" cellSpacing="0" cellPadding="0">
					<TR>
						<TD vAlign="top">
							<TABLE style="WIDTH: 344px" id="Table8">
								<TBODY class="tableLabel">
									<TR>
										<TD style="WIDTH: 4px; HEIGHT: 26px"></TD>
										<TD style="WIDTH: 148px; HEIGHT: 26px"><asp:label id="lblConNo" runat="server" CssClass="tableLabel">Consignment 
																		Number:</asp:label></TD>
										<TD style="WIDTH: 134px; HEIGHT: 26px"><cc1:mstextbox id="txtConNo" runat="server" CssClass="tableTextbox" Width="172px" AutoPostBack="True"
												MaxLength="32" style="Z-INDEX: 0" DESIGNTIMEDRAGDROP="150"></cc1:mstextbox></TD>
									</TR>
									<TR>
										<TD style="WIDTH: 4px">&nbsp;</TD>
										<TD style="WIDTH: 148px" class="tableLabel"><asp:label id="lblDestPostCode" runat="server" CssClass="tableLabel">Destination Postal Code:</asp:label></TD>
										<TD style="WIDTH: 134px"><cc1:mstextbox id="txtDestPostCode" runat="server" CssClass="tableTextbox" Width="120px" AutoPostBack="True"
												MaxLength="10"></cc1:mstextbox><asp:label id="lblProvince" runat="server" CssClass="tableLabel"></asp:label><asp:label id="lbltmpDestDC" runat="server" Visible="False"></asp:label></TD>
									</TR>
									<TR>
										<TD style="WIDTH: 4px"></TD>
										<TD style="WIDTH: 148px" class="tableLabel"><asp:label id="lblServiceType" runat="server" CssClass="tableLabel">Service Type:</asp:label></TD>
										<TD style="WIDTH: 134px"><asp:dropdownlist id="ddlServiceType" runat="server" Width="106px"></asp:dropdownlist></TD>
									</TR>
									<TR>
										<TD style="WIDTH: 4px">&nbsp;</TD>
										<TD style="WIDTH: 148px" class="tableLabel"><asp:label id="lblNumOfPackages" runat="server" CssClass="tableLabel">Number of Packages:</asp:label></TD>
										<TD style="WIDTH: 134px"><cc1:mstextbox id="txtPkgNum" runat="server" CssClass="tableTextbox" Width="66px" NumberMaxValue="9999"
												NumberPrecision="3" NumberMinValue="1" TextMaskType="msNumeric"></cc1:mstextbox></TD>
									</TR>
									<TR>
										<TD style="WIDTH: 4px">&nbsp;</TD>
										<TD class="style1" colSpan="2"><asp:button id="btnClear" runat="server" CssClass="queryButton" Width="86px" Text="Clear" CausesValidation="False"></asp:button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
											<asp:button id="btnInsert" runat="server" CssClass="queryButton" Width="80px" Text="Insert"
												Enabled="False"></asp:button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
											<asp:button id="btnUpdate" runat="server" CssClass="queryButton" Width="94px" Text="Update"
												Enabled="False"></asp:button></TD>
									</TR>
									<TR>
										<TD style="WIDTH: 4px">&nbsp;</TD>
										<TD class="style1" colSpan="2"><asp:button id="btnInsert_Print" runat="server" CssClass="queryButton" Width="136px" Text="Insert &amp; Print"
												CausesValidation="False" Enabled="False"></asp:button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
											&nbsp;
											<asp:button id="btnPrintSelected" runat="server" CssClass="queryButton" Width="136px" Text="Print Selected"
												Enabled="False"></asp:button></TD>
									</TR>
									<TR>
										<TD style="WIDTH: 4px">&nbsp;</TD>
										<TD style="WIDTH: 148px" class="style2"><asp:label id="lblSipCount" runat="server" CssClass="tableLabel">SIP Count:</asp:label></TD>
										<TD style="WIDTH: 134px"><asp:label id="lblSipCountDisplay" runat="server" CssClass="tableLabel"></asp:label></TD>
									</TR>
									<TR>
										<TD style="WIDTH: 4px">&nbsp;</TD>
										<TD style="WIDTH: 148px" class="style2"><asp:label id="lblSubmitDateTime" runat="server" CssClass="tableLabel">Submit Date/Time:</asp:label></TD>
										<TD style="WIDTH: 134px"><asp:label id="lblSubmitDateTimeDisplay" runat="server" CssClass="tableLabel"></asp:label></TD>
									</TR>
									<TR>
										<TD style="WIDTH: 4px"></TD>
										<TD style="WIDTH: 148px" class="style2"><asp:label id="lblUser" runat="server" CssClass="tableLabel">User:</asp:label></TD>
										<TD style="WIDTH: 134px"><asp:label id="lblUserDisplay" runat="server" CssClass="tableLabel"></asp:label></TD>
									</TR>
									<TR>
										<TD style="WIDTH: 4px">&nbsp;</TD>
										<TD style="WIDTH: 148px" class="style2"><asp:label id="lblLocation" runat="server" CssClass="tableLabel">Location:</asp:label></TD>
										<TD style="WIDTH: 134px"><asp:label id="lblLocationDisplay" runat="server" CssClass="tableLabel"></asp:label></TD>
									</TR>
									<TR>
										<TD style="WIDTH: 4px">&nbsp;</TD>
										<TD style="WIDTH: 148px" class="style2">&nbsp;</TD>
										<TD style="WIDTH: 134px">&nbsp;</TD>
									</TR>
								</TBODY>
							</TABLE>
						</TD>
						<td width="15">&nbsp;</td>
						<TD vAlign="top">
							<TABLE style="WIDTH: 632px; HEIGHT: 231px" id="Table2">
								<TR class="tableLabel">
									<TD style="HEIGHT: 15px" noWrap><asp:label id="lblColConsignment" runat="server" CssClass="tableLabel" Width="120px">Consignment</asp:label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										<asp:label id="lblColService" runat="server" CssClass="tableLabel" Width="56px">Service</asp:label><asp:label id="lblColDestination" runat="server" CssClass="tableLabel" Width="26px">Destination</asp:label>&nbsp;&nbsp;&nbsp;
										<asp:label id="lblColPkgs" runat="server" CssClass="tableLabel">Pkgs</asp:label>&nbsp;&nbsp;&nbsp;
										<asp:label id="lblColSIP" runat="server" CssClass="tableLabel">SIP</asp:label>&nbsp;<asp:label id="lblColDataTime" runat="server" CssClass="tableLabel">Date/Time</asp:label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										<asp:label id="lblColUser" runat="server" CssClass="tableLabel">User</asp:label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										<asp:label id="lblColLoc" runat="server" CssClass="tableLabel" Width="4px">Loc.</asp:label>&nbsp;&nbsp;&nbsp;
										<asp:label id="lblColDestDC" runat="server" CssClass="tableLabel">Dest. DC</asp:label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</TD>
								</TR>
								<TR>
									<TD><asp:listbox style="FONT-FAMILY: 'Courier New', Courier, monospace" id="lbSIPCon" runat="server"
											Width="616px" Height="208px" AutoPostBack="True"></asp:listbox></TD>
								</TR>
							</TABLE>
						</TD>
					</TR>
				</TABLE>
			</FIELDSET>
			<INPUT type="hidden" id="hidHost" runat="server" NAME="hhost"> <INPUT type="hidden" id="hidPort" runat="server" NAME="hport">
		</form>
	</body>
</HTML>
