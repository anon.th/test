<%@ Register TagPrefix="DbCombo" Namespace="Cambro.Web.DbCombo" Assembly="Cambro.Web.DbCombo" %>
<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<%@ Import Namespace="Cambro.Web.DbCombo" %>
<%@ Import Namespace="com.common.util" %>
<%@ Page language="c#" Codebehind="Quick_POD.aspx.cs" AutoEventWireup="false" Inherits="TIES.WebUI.Manifest.Quick_POD" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<TITLE>Scanning Workbench Emulator</TITLE>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK rel="stylesheet" type="text/css" href="../css/Styles.css">
		<!--#INCLUDE FILE="../msFormValidations.inc"-->
		<script language="javascript" src="../Scripts/JScript_TextControl.js"></script>
		<script language="javascript" src="../Scripts/settingScrollPosition.js"></script>
		<script language="javascript">	
		
		function makeUppercase(objId)
		{
			document.getElementById(objId).value = document.getElementById(objId).value.toUpperCase();
		}
		
		function SetScrollPosition_page()
		{
			window.scroll(0,document.forms[0].ScrollPosition.value);
			var sHostname = window.location.hostname;
			var sPort = window.location.port;
			
			document.getElementById('hidHost').value = sHostname; 
			document.getElementById('hidPort').value = sPort; 
			//alert(document.getElementById('hidHost').value);			
		}
		
		function setValue()
		{
			var s = document.all['inFile'];
			document.all['txtFilePath'].innerText = s.value;
			if(s.value != '') 
				document.all['btnImport'].disabled = false;
			else
				document.all['btnImport'].disabled = true;
		}
	
		function upBrowse()
		{
			document.all['divBrowse'].style.backgroundImage='url(../images/btn-browse-up.GIF)';
		}
		
		function downBrowse()
		{
			document.all['divBrowse'].style.backgroundImage='url(../images/btn-browse-down.GIF)';
		}		
		
		function clearValue()
		{
			if(document.all['txtFilePath'].innerText == '') {
				document.location.reload();
			}
		}

		function IsNumeric(sText)
		{
			var ValidChars = "0123456789.";
			var IsNumber=true;
			var Char;
				
			for (i = 0; i < sText.length && IsNumber == true; i++) 
				{ 
				Char = sText.charAt(i); 
				if (ValidChars.indexOf(Char) == -1) 
					{
					IsNumber = false;
					}
				}
			return IsNumber;	
		}
		
		function TxtConNoFocus()
		{
			document.getElementById("txtConNo").value = '';
			document.getElementById("txtConNo").focus();
		}

		function KeyDownHandler()
		{
			//debugger;
			// process only the Enter key
			var key;
			if(window.event)  key = window.event.keyCode;     //IE
			else  key = e.which;     //firefox
			if (key == 13)
			{
				// cancel the default submit
				//event.returnValue=false;
				//event.cancel = true;
				// submit the form by programmatically clicking the specified button
	            
				document.getElementById("btnExecQry").click();
				document.getElementById("txtConNo").focus();
            
				/*
				if(document.getElementById("btnInsert").disabled==false)
				{
					//btn = "btnInsert";
					
					document.getElementById("btnExecQry").click();
					document.getElementById("txtConNo").focus();
				}
				else if(document.getElementById("btnUpdate").disabled==false)
				{
					//btn = "btnUpdate";
					document.getElementById("btnUpdate").click();
					document.getElementById("txtConNo").focus();
				}
				*/          
			}
		}
    
		function  suppressNonEng(e,obj)
		{	//debugger;
			var key;
			if(window.event)  key = window.event.keyCode;     //IE
			else  key = e.which;     //firefox
				
			if(key >128)  return false;
			else  return UpperMask(obj);//return true;*/
		}		
	 
		function suppressOnBluer(e)
		{	//alert("");
			//e.value = Check2FontChar2LastChar(e);
		
			var s= e.value;
			e.value = '';
			for(i=0;i<s.length;i++)
			{     if (s.charCodeAt(i)<=128)
					{
						e.value += s.charAt(i);
					}			
			}
			if(s.length!=e.value.length)
			{
			//alert('Other than english character are not supported')
				return false;
			}//debugger;
			//var hddConNo = document.getElementById("hddConNo");
			__doPostBack('hddConNo','');
		}	
	</script>
	</HEAD>
	<body onclick="GetScrollPosition();" onunload="window.top.displayBanner.fnCloseAll(0);"
		onload="SetScrollPosition_page();" MS_POSITIONING="GridLayout">
		<form id="SWB_CreateSIP" method="post" runat="server">
			<input style="DISPLAY: none" id="hddConNo" type="button" name="hddCon" runat="server">
			<P><FONT face="Tahoma"></FONT>&nbsp;</P>
			<P><asp:label style="Z-INDEX: 101; POSITION: absolute; TOP: 23px; LEFT: 14px" id="Header1" Runat="server"
					CssClass="mainTitleSize" Width="490">Quick POD</asp:label></P>
			<P></P>
			<TABLE style="Z-INDEX: 106; POSITION: absolute; WIDTH: 896px; HEIGHT: 49px; TOP: 56px; LEFT: 16px"
				id="Table10">
				<TR>
					<TD vAlign="bottom"><asp:button style="Z-INDEX: 0" id="btnQry" runat="server" CssClass="queryButton" Width="61px"
							Text="Query" CausesValidation="False"></asp:button><asp:button style="Z-INDEX: 0" id="btnExecQry" runat="server" CssClass="queryButton" Width="130px"
							Text="Execute Query" CausesValidation="False"></asp:button><asp:button style="Z-INDEX: 0" id="btnSave" runat="server" CssClass="queryButton" Width="86px"
							Text="Save" CausesValidation="False"></asp:button></TD>
					<TD vAlign="bottom">&nbsp;
					</TD>
					<TD vAlign="bottom">&nbsp;
					</TD>
				</TR>
				<TR>
					<TD colSpan="4">&nbsp;
						<asp:label style="Z-INDEX: 0" id="lblErrrMsg" runat="server" CssClass="errorMsgColor" Width="720"
							Height="2"></asp:label></TD>
				</TR>
			</TABLE>
			<P></P>
			<INPUT style="Z-INDEX: 103; POSITION: absolute; TOP: 432px; LEFT: 760px" type="hidden"
				name="ScrollPosition">&nbsp;&nbsp;
			<asp:validationsummary style="Z-INDEX: 104; POSITION: absolute; TOP: 16px; LEFT: 552px" id="reqSummary"
				Runat="server" ShowSummary="False" HeaderText="Please enter the missing  fields." ShowMessageBox="True"
				DisplayMode="BulletList"></asp:validationsummary>
			<FIELDSET style="Z-INDEX: 105; POSITION: absolute; WIDTH: 921px; HEIGHT: 256px; TOP: 120px; LEFT: 16px"
				id="fsCreateSIP" runat="server">
				<TABLE id="Table1" cellSpacing="0" cellPadding="0">
					<TR>
						<TD vAlign="top">
							<TABLE style="WIDTH: 600px" id="Table8">
								<TBODY class="tableLabel">
									<TR>
										<TD style="WIDTH: 4px; HEIGHT: 23px"></TD>
										<TD style="WIDTH: 148px; HEIGHT: 23px"><asp:label id="lblConNo" runat="server" CssClass="tableLabel">Consignment 
																		Number:</asp:label></TD>
										<TD style="WIDTH: 448px; HEIGHT: 23px"><cc1:mstextbox style="Z-INDEX: 0" id="txtConNo" runat="server" CssClass="tableTextbox" Width="172px"
												AutoPostBack="True" MaxLength="32"></cc1:mstextbox></TD>
									</TR>
									<TR>
										<TD style="WIDTH: 4px">&nbsp;</TD>
										<TD style="WIDTH: 148px" class="tableLabel"><asp:label id="lblOriginDC" runat="server" CssClass="tableLabel"> Origin DC:</asp:label></TD>
										<TD style="WIDTH: 448px"><DBCOMBO:DBCOMBO id="DbComboOriginDC" tabIndex="14" CssClass="tableLabel" Width="25px" AutoPostBack="True"
											Runat="server" TextBoxColumns="18" RegistrationKey=" " TextUpLevelSearchButton="v" ShowDbComboLink="False" ServerMethod="DbComboDistributionCenterSelectOrigin"></DBCOMBO:DBCOMBO></TD>
									</TR>
									<TR>
										<TD style="WIDTH: 4px">&nbsp;</TD>
										<TD style="WIDTH: 148px" class="tableLabel"><asp:label id="lblDestPostCode" runat="server" CssClass="tableLabel">Destination Postal Code:</asp:label></TD>
										<TD style="WIDTH: 448px"><cc1:mstextbox style="Z-INDEX: 0" id="txtDestPostCode" runat="server" CssClass="tableTextbox" Width="50px"
												AutoPostBack="True" MaxLength="10"></cc1:mstextbox><asp:label id="lblProvince" runat="server" CssClass="tableLabel"></asp:label><asp:label id="lbltmpDestDC" runat="server" Visible="False"></asp:label></TD>
									</TR>
									<TR>
										<TD style="WIDTH: 4px"></TD>
										<TD style="WIDTH: 148px" class="tableLabel"><asp:label id="lblServiceType" runat="server" CssClass="tableLabel">Service Type:</asp:label></TD>
										<TD style="WIDTH: 448px"><asp:dropdownlist id="ddlServiceType" runat="server" Width="106px"></asp:dropdownlist></TD>
									</TR>
									<TR>
										<TD style="WIDTH: 4px">&nbsp;</TD>
										<TD style="WIDTH: 148px" class="tableLabel"><asp:label id="lblNumOfPackages" runat="server" CssClass="tableLabel">Number of Packages:</asp:label></TD>
										<TD style="WIDTH: 448px"><cc1:mstextbox style="Z-INDEX: 0" id="txtPkgNum" runat="server" CssClass="tableTextbox" Width="40px"
												NumberMaxValue="9999" NumberPrecision="3" NumberMinValue="1" TextMaskType="msNumeric"></cc1:mstextbox></TD>
									</TR>
									<TR>
										<TD style="WIDTH: 4px">&nbsp;</TD>
										<TD style="WIDTH: 148px" class="tableLabel"></TD>
										<TD style="WIDTH: 448px"></TD>
									</TR>
									<tr>
										<TD style="WIDTH: 4px">&nbsp;</TD>
										<TD style="WIDTH: 100%" colSpan="2">
											<fieldset><LEGEND><asp:label id="lblPOD_Detail" runat="server" CssClass="tableHeadingFieldset" Width="80px" Height="16px">POD Details</asp:label></LEGEND>
												<table style="WIDTH: 100%">
													<tr>
														<td style="WIDTH: 148px"><asp:label style="Z-INDEX: 0" id="lblActual_Datetine" runat="server" CssClass="tableLabel">Actual D/T:</asp:label></td>
														<td style="WIDTH: 134px"><cc1:mstextbox style="Z-INDEX: 0" id="txtActual" runat="server" CssClass="tableTextbox" Width="136px"
																AutoPostBack="True" MaxLength="32"></cc1:mstextbox></td>
														<TD style="WIDTH: 10px">&nbsp;</TD>
														<td style="WIDTH: 100px"><asp:label style="Z-INDEX: 0" id="lblConsignee" runat="server" CssClass="tableLabel">Consignee:</asp:label></td>
														<td style="WIDTH: 172px"><cc1:mstextbox style="Z-INDEX: 0" id="txtConsignee" runat="server" CssClass="tableTextbox" Width="140px"
																AutoPostBack="True" MaxLength="32"></cc1:mstextbox></td>
													</tr>
													<tr>
														<td style="WIDTH: 148px; HEIGHT: 21px"><asp:label style="Z-INDEX: 0" id="lblRemark" runat="server" CssClass="tableLabel">Remark:</asp:label></td>
														<td style="WIDTH: 134px" colSpan="4"><asp:textbox id="txtRemark" runat="server" Width="422px" Height="82px" TextMode="MultiLine"></asp:textbox></td>
													</tr>
												</table>
											</fieldset>
										</TD>
									</tr>
								</TBODY>
							</TABLE>
						</TD>
					</TR>
				</TABLE>
				<TABLE style="WIDTH: 732px; HEIGHT: 131px" id="Table2">
					<TR class="tableLabel">
						<TD style="HEIGHT: 16px" noWrap><asp:label id="lblColConsignment" runat="server" CssClass="tableLabel" Width="120px">Consignment</asp:label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:label style="Z-INDEX: 0" id="lblColService" runat="server" CssClass="tableLabel" Width="56px">Service</asp:label>&nbsp;&nbsp;<asp:label style="Z-INDEX: 0" id="lblColDestination" runat="server" CssClass="tableLabel" Width="26px">Destination</asp:label>&nbsp;&nbsp;&nbsp;&nbsp;<asp:label style="Z-INDEX: 0" id="lblColPkgs" runat="server" CssClass="tableLabel">Pkgs</asp:label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:label style="Z-INDEX: 0" id="lblColPOD" runat="server" CssClass="tableLabel">POD</asp:label>&nbsp;<asp:label style="Z-INDEX: 0" id="lblColDataTime" runat="server" CssClass="tableLabel">Date/Time</asp:label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:label style="Z-INDEX: 0" id="lblColUser" runat="server" CssClass="tableLabel">Consignee</asp:label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:label style="Z-INDEX: 0" id="lblColLoc" runat="server" CssClass="tableLabel" Width="4px">Loc.</asp:label>&nbsp;&nbsp;<asp:label style="Z-INDEX: 0" id="lblColDestDC" runat="server" CssClass="tableLabel">Dest. DC</asp:label></TD>
					</TR>
					<TR>
						<TD><asp:listbox style="FONT-FAMILY: 'Courier New', Courier, monospace" id="lbSIPCon" runat="server"
								Width="706px" Height="120px" AutoPostBack="True"></asp:listbox></TD>
					</TR>
				</TABLE>
			</FIELDSET>
			<INPUT id="hidHost" type="hidden" name="hhost" runat="server"> <INPUT id="hidPort" type="hidden" name="hport" runat="server">
		</form>
	</body>
</HTML>
