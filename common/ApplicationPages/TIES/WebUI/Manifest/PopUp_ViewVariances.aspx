<%@ Page language="c#" Codebehind="PopUp_ViewVariances.aspx.cs" AutoEventWireup="false" Inherits="TIES.WebUI.Manifest.PopUp_ViewVariances" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
  <HEAD>
		<title>PopUp_ViewVariances</title>
		<meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK href="../css/Styles.css" type="text/css" rel="stylesheet">
		<!--#INCLUDE FILE="../msFormValidations.inc"-->
		<script language="javascript" src="../Scripts/settingScrollPosition.js"></script>
		<script language="javascript">
			function ShowStatus()
			{
				if(!document.getElementById("btnShowStatus").disabled)
					document.getElementById("btnShowStatus").click();
			}
		</script>
</HEAD>
	<body MS_POSITIONING="GridLayout" onunload="window.opener.top.displayBanner.fnCloseAll(1);">
		<form id="Form1" method="post" runat="server">
			<asp:label id="Header1" style="Z-INDEX: 100; LEFT: 8px; POSITION: absolute; TOP: 8px" Runat="server"
				Width="490" CssClass="mainTitleSize">Batch Manifests - View Details</asp:label>
			<asp:label id="lblErrorMessage" style="Z-INDEX: 126; LEFT: 16px; POSITION: absolute; TOP: 32px"
				runat="server" CssClass="errorMsgColor" Width="680px" Height="17px"></asp:label>
			<asp:Button id="btnExport" style="Z-INDEX: 125; LEFT: 224px; POSITION: absolute; TOP: 264px"
				runat="server" CssClass="queryButton" Width="96px" Text="Export Excel"></asp:Button>
			<asp:Button id="btnShowCon" style="Z-INDEX: 124; LEFT: 120px; POSITION: absolute; TOP: 264px"
				runat="server" CssClass="queryButton" Width="96px" Text="Show Cons"></asp:Button>
			<asp:Label id="Label12" style="Z-INDEX: 116; LEFT: 536px; POSITION: absolute; TOP: 304px" runat="server"
				CssClass="tableLabel">Var</asp:Label>
			<asp:Label id="Label11" style="Z-INDEX: 115; LEFT: 480px; POSITION: absolute; TOP: 304px" runat="server"
				CssClass="tableLabel">#Pkgs</asp:Label>
			<asp:listbox id="lbBatch" style="Z-INDEX: 106; LEFT: 16px; POSITION: absolute; TOP: 320px" runat="server"
				Width="770px" Height="300px" AutoPostBack="True" Font-Names="Courier New"></asp:listbox>
			<FIELDSET style="Z-INDEX: 105; LEFT: 544px; WIDTH: 240px; POSITION: absolute; TOP: 48px; HEIGHT: 216px"><LEGEND>
					<asp:label id="lblStatusList" Runat="server" Width="90px" CssClass="tableHeadingFieldset" HEIGHT="16px"
						BorderStyle="None"> Filter By Status</asp:label></LEGEND>
				<asp:CheckBoxList id="chkStatus" runat="server" CssClass="tablelabel" AutoPostBack="True"></asp:CheckBoxList></FIELDSET>
			<asp:button id="Button1" style="Z-INDEX: 101; LEFT: 16px; POSITION: absolute; TOP: 56px" runat="server"
				Width="80px" CssClass="queryButton" Text="Close"></asp:button>
			<FIELDSET style="Z-INDEX: 104; LEFT: 272px; WIDTH: 240px; POSITION: absolute; TOP: 80px; HEIGHT: 176px">
				<P><LEGEND>
						<asp:label id="lblVarianceList" Runat="server" Width="106px" CssClass="tableHeadingFieldset"
							HEIGHT="16px"> Filter By Variance</asp:label></LEGEND>
					<FONT face="Tahoma"></FONT>
					<asp:RadioButton id="rdoShowall" runat="server" Text="Show all" GroupName="VarianceList" Checked="True"
						AutoPostBack="True"></asp:RadioButton></P>
				<P>
					<asp:RadioButton id="rdoShippingVariances" runat="server" Text="Shipping Variances" GroupName="VarianceList"
						AutoPostBack="True"></asp:RadioButton></P>
				<P>
					<asp:RadioButton id="rdoReceiveVariances" runat="server" Text="Receiving Variances" GroupName="VarianceList"
						AutoPostBack="True"></asp:RadioButton></P>
				<P>
					<asp:RadioButton id="rdoAnyVariances" runat="server" Text="Any Variances" GroupName="VarianceList"
						AutoPostBack="True"></asp:RadioButton></P>
			</FIELDSET>
			<TABLE id="Table1" style="Z-INDEX: 103; LEFT: 16px; WIDTH: 232px; POSITION: absolute; TOP: 104px; HEIGHT: 156px"
				cellSpacing="1" cellPadding="1" width="232" border="0">
				<TR>
					<TD>
						<asp:label id="lblBatchNumber" runat="server" CssClass="tableLabel">Batch Number:</asp:label></TD>
					<TD>
						<asp:label id="lblBatchNoDisplay" runat="server" CssClass="tableLabel"></asp:label></TD>
				</TR>
				<TR>
					<TD>
						<asp:label id="lblBatchDate" runat="server" CssClass="tableLabel">Batch Date:</asp:label></TD>
					<TD><FONT face="Tahoma">
							<asp:label id="lblBatchDateDisplay" runat="server" CssClass="tableLabel"></asp:label></FONT></TD>
				</TR>
				<TR>
					<TD>
						<asp:label id="lblTruckID" runat="server" CssClass="tableLabel">Truck ID:</asp:label></TD>
					<TD>
						<asp:label id="lblTruckIDDisplay" runat="server" CssClass="tableLabel"></asp:label></TD>
				</TR>
				<TR>
					<TD>
						<asp:label id="lblBatchID" runat="server" CssClass="tableLabel">Batch ID:</asp:label></TD>
					<TD>
						<asp:label id="lblBatchIDDisplay" runat="server" CssClass="tableLabel"></asp:label></TD>
				</TR>
				<TR>
					<TD>
						<asp:label id="lblBatchType" runat="server" CssClass="tableLabel">Batch Type:</asp:label></TD>
					<TD>
						<asp:label id="lblBatchTypeDisplay" runat="server" CssClass="tableLabel"></asp:label></TD>
				</TR>
				<TR>
					<TD><FONT face="Tahoma"></FONT></TD>
					<TD></TD>
				</TR>
				<TR>
					<TD>
						<asp:label id="Label1" runat="server" CssClass="tableLabel">Dest. DC</asp:label></TD>
					<TD>
						<asp:dropdownlist id="ddlCurrentLocation" runat="server" Width="106px" CssClass="textField"></asp:dropdownlist></TD>
				</TR>
			</TABLE>
			<asp:button id="btnClose" style="Z-INDEX: 102; LEFT: 16px; POSITION: absolute; TOP: 56px" runat="server"
				Width="80px" CssClass="queryButton" Text="Close"></asp:button>
			<asp:Label id="Label2" style="Z-INDEX: 107; LEFT: 16px; POSITION: absolute; TOP: 304px" runat="server"
				CssClass="tableLabel">Consignment Number</asp:Label>
			<asp:Label id="Label3" style="Z-INDEX: 108; LEFT: 168px; POSITION: absolute; TOP: 304px" runat="server"
				CssClass="tableLabel">Origin</asp:Label>
			<asp:Label id="Label4" style="Z-INDEX: 109; LEFT: 224px; POSITION: absolute; TOP: 304px" runat="server"
				CssClass="tableLabel">Dest.</asp:Label>
			<asp:Label id="Label5" style="Z-INDEX: 110; LEFT: 176px; POSITION: absolute; TOP: 288px" runat="server"
				CssClass="tableLabel">---- DC----</asp:Label>
			<asp:Label id="Label6" style="Z-INDEX: 111; LEFT: 280px; POSITION: absolute; TOP: 304px" runat="server"
				CssClass="tableLabel">#Pkgs</asp:Label>
			<asp:Label id="Label7" style="Z-INDEX: 112; LEFT: 336px; POSITION: absolute; TOP: 304px" runat="server"
				CssClass="tableLabel">Weight</asp:Label>
			<asp:Label id="Label8" style="Z-INDEX: 113; LEFT: 392px; POSITION: absolute; TOP: 304px" runat="server"
				CssClass="tableLabel">#Pkgs</asp:Label>
			<asp:Label id="Label9" style="Z-INDEX: 114; LEFT: 448px; POSITION: absolute; TOP: 304px" runat="server"
				CssClass="tableLabel">Var</asp:Label>
			<asp:Label id="Label13" style="Z-INDEX: 117; LEFT: 608px; POSITION: absolute; TOP: 304px" runat="server"
				CssClass="tableLabel">Status</asp:Label>
			<asp:Label id="Label14" style="Z-INDEX: 118; LEFT: 712px; POSITION: absolute; TOP: 304px" runat="server"
				CssClass="tableLabel">Code</asp:Label>
			<asp:Label id="Label15" style="Z-INDEX: 119; LEFT: 712px; POSITION: absolute; TOP: 288px" runat="server"
				Width="68px" CssClass="tableLabel">Last Status</asp:Label>
			<asp:Label id="Label16" style="Z-INDEX: 120; LEFT: 488px; POSITION: absolute; TOP: 288px" runat="server"
				Width="102px" CssClass="tableLabel">SIP Scanned</asp:Label>
			<asp:Label id="Label17" style="Z-INDEX: 121; LEFT: 392px; POSITION: absolute; TOP: 288px" runat="server"
				CssClass="tableLabel">SOP Scanned</asp:Label>
			<asp:Button id="btnShowStatus" style="Z-INDEX: 123; LEFT: 16px; POSITION: absolute; TOP: 264px"
				runat="server" CssClass="queryButton" Width="96px" Text="Show Status" Enabled="False"></asp:Button>
		</form>
	</body>
</HTML>
