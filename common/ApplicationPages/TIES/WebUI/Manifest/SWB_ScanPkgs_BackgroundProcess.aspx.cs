using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.ties.DAL;
namespace TIES.WebUI.Manifest
{
	/// <summary>
	/// Summary description for SWB_ScanPkgs_BackgroundProcess.
	/// </summary>
	public class SWB_ScanPkgs_BackgroundProcess : System.Web.UI.Page
	{
		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			try
			{
				string strMode = Request.Form["tMode"];
				if(strMode=="refresh")
				{
					//Session["State"]="Alive";
					return;
				}
				string appId = Request.Form["tAppID"].ToString();
				string entId = Request.Form["tEntID"].ToString();
				string tmpBarID = Request.Form["tBarID"].ToString();
				//DateTime tmpTimeStamp= DateTime.ParseExact (Request.Form["tTimeStamp"], "dd/MM/yyyy HH:mm:ss", null);
				//DateTime tmpTimeStamp= Convert.ToDateTime(Request.Form["tTimeStamp"]);
				string tmpTimeStamp =Request.Form["tTimeStamp"].ToString();
				string tmpBatchNo = Request.Form["tBatchNo"];
				string tmpScannerId= Request.Form["tscannerId"];
				string tmpstatuscode=Request.Form["tstatus_code"];
				string tmpuserID=Request.Form["tuserId"];
				string tmplocation =Request.Form["tlocation"];
				int idbCnt = Convert.ToInt32( Request.Form["dbCnt"]);
							
//				if(chkValidPKG(appId,entId,tmpBarID,tmpBatchNo,tmpstatuscode,tmplocation))
//				{
					int ScanningPkg_Ins = SWB_Emulator.SWB_ScanPkg_Insert(appId,entId,
						tmpBarID,tmpstatuscode,tmplocation,tmpBatchNo,tmpScannerId,
						tmpTimeStamp,tmpuserID);
				
					if (ScanningPkg_Ins==0) // Not Dup
					{
						Response.Write("");
					}
					else if(ScanningPkg_Ins==2) //Dup
					{
						Response.Write("D");
					}
					else
					{
					Response.Write("I");
				}
//				}
//				else
//				{
//					Response.Write("I");
//				}
			}
			catch(Exception ex)
			{
				Response.Write("E");
			}


		}
		private bool chkValidPKG(string appId ,string EnterpriseId,string tmpBarID,string BatchNo,string StatusCode,string location)
		{
			bool result =true;
			if((StatusCode.ToUpper()!="UNCLS")&&(StatusCode.ToUpper()!="UNSIP")&&(StatusCode.ToUpper()!="UNSOP"))
			{
				return result;
			}
			else
			{
				string chkStatus="";
				if(StatusCode.ToUpper()=="UNCLS")
				{
					chkStatus="CLS";
				}
				else if (StatusCode.ToUpper()=="UNSIP")
				{
					chkStatus="SIP";
				}

				else//(StatusCode.ToUpper()=="UNSOP")
				{
					chkStatus="SOP";
				}
				DataSet ds = SWB_Emulator.SWB_ScanPkg_Read(appId,EnterpriseId,tmpBarID,chkStatus,location,BatchNo);
				if(ds.Tables[0].Rows.Count>0)
				{
					result=true;
				}
				else
				{
					result=false;
				}
				return result;
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
