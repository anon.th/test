<%@ Page language="c#" Codebehind="Vehicles.aspx.cs" AutoEventWireup="false" Inherits="TIES.WebUI.Vehicles" %>
<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<%@ Import Namespace="com.common.util" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Vehicles</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../css/Styles.css" type="text/css" rel="stylesheet">
		<!--#INCLUDE FILE="../msFormValidations.inc"-->
		<script language="javascript" src="../Scripts/settingScrollPosition.js"></script>
	</HEAD>
	<body onclick="GetScrollPosition();" onload="SetScrollPosition();" onunload="window.top.displayBanner.fnCloseAll(0);"
		MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<FONT face="Tahoma">
				<asp:label id="lblMainTitle" style="Z-INDEX: 100; LEFT: 40px; POSITION: absolute; TOP: 32px"
					runat="server" Width="477px" CssClass="mainTitleSize" Height="26px">Vehicles</asp:label><asp:datagrid id="dgTruck" style="Z-INDEX: 103; LEFT: 40px; POSITION: absolute; TOP: 176px" runat="server"
					AllowPaging="True" AllowCustomPaging="True" AutoGenerateColumns="False" HorizontalAlign="Center">
					<FooterStyle ForeColor="#000066" BackColor="White"></FooterStyle>
					<ItemStyle CssClass="popupGridField"></ItemStyle>
					<HeaderStyle Width="10%" CssClass="gridHeading"></HeaderStyle>
					<Columns>
						<asp:BoundColumn DataField="TruckID" HeaderText="Truck ID">
							<HeaderStyle Width="100px"></HeaderStyle>
						</asp:BoundColumn>
						<asp:BoundColumn DataField="Description" HeaderText="Description">
							<HeaderStyle Width="150px"></HeaderStyle>
						</asp:BoundColumn>
						<asp:BoundColumn DataField="LicensePlate" HeaderText="License Plate">
							<HeaderStyle Width="150px"></HeaderStyle>
						</asp:BoundColumn>
						<asp:BoundColumn DataField="Type" HeaderText="Type">
							<HeaderStyle Width="100px"></HeaderStyle>
						</asp:BoundColumn>
						<asp:BoundColumn DataField="Base_Location" HeaderText="Location">
							<HeaderStyle Width="100px"></HeaderStyle>
						</asp:BoundColumn>
						<asp:ButtonColumn Text="Select" CommandName="Select">
							<HeaderStyle Width="50px"></HeaderStyle>
						</asp:ButtonColumn>
					</Columns>
					<PagerStyle NextPageText="Next" PrevPageText="Previous" HorizontalAlign="Right" CssClass="normalText"></PagerStyle>
				</asp:datagrid>
				<TABLE id="Table1" style="Z-INDEX: 102; LEFT: 40px; WIDTH: 568px; POSITION: absolute; TOP: 72px; HEIGHT: 64px"
					cellSpacing="1" cellPadding="1" width="568" border="0">
					<TR>
						<TD style="WIDTH: 82px"><asp:label id="lblTruckID" runat="server" Width="100%" CssClass="tableLabel" Height="22px">Truck ID</asp:label></TD>
						<TD style="WIDTH: 173px"><asp:textbox id="txtTruckID" tabIndex="1" runat="server" Width="152px" CssClass="textField" MaxLength="100"></asp:textbox></TD>
						<TD style="WIDTH: 89px"><asp:label id="lblDesc" runat="server" Width="100%" CssClass="tableLabel" Height="22px">Description</asp:label></TD>
						<TD style="WIDTH: 195px"><asp:textbox id="txtDesc" tabIndex="1" runat="server" Width="152px" CssClass="textField" MaxLength="100"></asp:textbox></TD>
					</TR>
					<TR>
						<TD style="WIDTH: 82px"><asp:label id="lblType" runat="server" Width="100%" CssClass="tableLabel" Height="22px">Type</asp:label></TD>
						<TD style="WIDTH: 173px"><asp:dropdownlist id="ddlType" runat="server" CssClass="textField"></asp:dropdownlist></TD>
						<TD style="WIDTH: 89px"><asp:label id="lblLicense" runat="server" Width="100%" CssClass="tableLabel" Height="22px">License Plate</asp:label></TD>
						<TD style="WIDTH: 195px"><asp:textbox id="txtLicense" tabIndex="1" runat="server" Width="152px" CssClass="textField" MaxLength="100"></asp:textbox></TD>
					</TR>
					<TR>
						<TD style="WIDTH: 82px"><asp:label id="lblLocation" runat="server" Width="100%" CssClass="tableLabel" Height="22px">Location</asp:label></TD>
						<TD style="WIDTH: 173px"><asp:dropdownlist id="ddlLocation" runat="server" CssClass="textField"></asp:dropdownlist></TD>
						<TD style="WIDTH: 89px"></TD>
						<TD style="WIDTH: 195px" align="left"><asp:button id="btnSearch" tabIndex="4" runat="server" Width="62px" CssClass="queryButton" Height="22px"
								CausesValidation="False" Text="Search"></asp:button><asp:button id="btnClose" tabIndex="5" runat="server" Width="62px" CssClass="queryButton" Height="22px"
								CausesValidation="False" Text="Close"></asp:button></TD>
					</TR>
				</TABLE>
			</FONT>
		</form>
	</body>
</HTML>
