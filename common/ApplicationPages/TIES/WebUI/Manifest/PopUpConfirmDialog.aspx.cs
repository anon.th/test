using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls; 
using com.common.util;
using com.common.classes;
using com.common.DAL;
using System.Text;
using com.common.applicationpages;
using com.ties.DAL;
using com.common.RBAC;
using com.ties.classes;

namespace TIES.WebUI.Manifest
{
	/// <summary>
	/// Summary description for PopUpConfirmDialog.
	/// </summary>
	public class PopUpConfirmDialog : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.Button btnYes;
		protected System.Web.UI.WebControls.Label lbmsg;
		protected System.Web.UI.WebControls.Button btnNo;
		private String m_strAppID, m_strEnterpriseID, m_strCulture;
		String userID = null;
		String loc = null;
		String batchno =null;
		protected  string closeWindow = @"
		<script language='javascript'>
		window.close();
		<" + "/script" + ">";

		

		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
				
			
			if(!Page.IsPostBack)
			{
				ShowMessage();
				
			}

		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnYes.Click += new System.EventHandler(this.btnYes_Click);
			this.btnNo.Click += new System.EventHandler(this.btnNo_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private void ShowMessage()
		{
			Utility utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings,Page.Session);
			m_strAppID = utility.GetAppID();
			m_strEnterpriseID = utility.GetEnterpriseID();
			m_strCulture = utility.GetUserCulture();
			lbmsg.Text  = Request.QueryString["msg"].ToString() ;
			userID = Request.QueryString["userid"].ToString()  ;
			loc = Request.QueryString["loc"].ToString() ; 
			batchno = Request.QueryString["batchno"].ToString() ; 
		}
		private void btnNo_Click(object sender, System.EventArgs e)
		{
			//Page.RegisterClientScriptBlock("closeWindow",closeWindow);
			// closeWindow(); 
			//Response.Write("<script> window.close(); </script>");
			//Page.Response.Close();  
			String sScript = "";
			sScript += "<script language=javascript>";
			sScript += "  window.close();";
			sScript += "</script>";
			Response.Write(sScript);

			
		}

		private void btnYes_Click(object sender, System.EventArgs e)
		{
			ShowMessage();
			int result = BatchManifest.UpdateScannedPkgs(m_strAppID,m_strEnterpriseID,userID,loc, batchno); 
			String sScript = "";
			sScript += "<script language=javascript> ";
			sScript += "  alert('Your Manifest will be created as a background process. You will receive it in email shortly.')";
					
			sScript += "</script>";
			Response.Write(sScript);
			Page.RegisterClientScriptBlock("closeWindow",closeWindow);
		}
	}
}
