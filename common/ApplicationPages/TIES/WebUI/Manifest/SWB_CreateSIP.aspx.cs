using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.common.classes;
using com.common.util;
using com.common.applicationpages;
using com.ties.DAL;
using com.common.RBAC;
using com.ties.classes;
using TIESClasses;
using TIESDAL;
using System.Text;
using System.Configuration;
using System.Data.OleDb;
using System.Text.RegularExpressions;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using com.common.DAL;
namespace TIES.WebUI.Manifest
{
	/// <summary>
	/// Summary description for SWB_CreateSIP.
	/// </summary>
	public class SWB_CreateSIP : BasePage
	{
		protected System.Web.UI.WebControls.Label lblErrrMsg;
		protected System.Web.UI.WebControls.Label lblCreateSIP;
		protected System.Web.UI.WebControls.Label lblConNo;
		protected com.common.util.msTextBox txtConNo;
		protected System.Web.UI.WebControls.Label lblDestPostCode;
		protected com.common.util.msTextBox txtDestPostCode;
		protected System.Web.UI.WebControls.Label lblProvince;
		protected System.Web.UI.WebControls.Label lblServiceType;
		protected System.Web.UI.WebControls.DropDownList ddlServiceType;
		protected System.Web.UI.WebControls.Label lblNumOfPackages;
		protected System.Web.UI.WebControls.Button btnClear;
		protected System.Web.UI.WebControls.Button btnInsert;
		protected System.Web.UI.WebControls.Button btnUpdate;
		protected System.Web.UI.WebControls.Label lblSipCount;
		protected System.Web.UI.WebControls.Label lblSipCountDisplay;
		protected System.Web.UI.WebControls.Label lblSubmitDateTime;
		protected System.Web.UI.WebControls.Label lblSubmitDateTimeDisplay;
		protected System.Web.UI.WebControls.Label lblUserDisplay;
		protected System.Web.UI.WebControls.Label lblLocation;
		protected System.Web.UI.WebControls.Label lblLocationDisplay;
		protected System.Web.UI.WebControls.ListBox lbSIPCon;
		protected System.Web.UI.WebControls.Label lblImportSIPs;
		protected System.Web.UI.WebControls.TextBox txtFilePath;
		protected System.Web.UI.WebControls.Button btnImport;
		protected System.Web.UI.HtmlControls.HtmlInputFile inFile;
		protected System.Web.UI.WebControls.ValidationSummary reqSummary;
		protected System.Web.UI.WebControls.Label lblUser;
		protected System.Web.UI.WebControls.Label lblColConsignment;
		protected System.Web.UI.WebControls.Label lblColPkgs;
		protected System.Web.UI.WebControls.Label lblColSIP;
		protected System.Web.UI.WebControls.Label lblColDataTime;
		protected System.Web.UI.WebControls.Label lblColUser;
		protected System.Web.UI.WebControls.Label lblColLoc;
		protected System.Web.UI.WebControls.Label lblColDestDC;
		protected System.Web.UI.WebControls.Label Header1;
		protected com.common.util.msTextBox txtPkgNum;
		protected ArrayList userRoleArray;

		String m_strAppID=null;
		String m_strEnterpriseID=null;
		
		String m_strCulture=null;
		protected System.Web.UI.WebControls.Label lbltmpDestDC;
		String processid =null;
		protected System.Web.UI.HtmlControls.HtmlGenericControl fsImport;
		protected System.Web.UI.WebControls.Panel pnlImport;
		protected System.Web.UI.HtmlControls.HtmlGenericControl fsCreateSIP;
		protected System.Web.UI.WebControls.Label lblColService;
		protected System.Web.UI.WebControls.Label lblColDestination;
		protected System.Web.UI.WebControls.Button btnInsert_Print;
		protected System.Web.UI.WebControls.Button btnPrintSelected;
		String userID=null;
		private string pkgNum;
		private string consignmentNumber;
		private string conName = null;
		private string pdfFileName;
		public int CharBarcodeCutStartIndex = Convert.ToInt32(ConfigurationSettings.AppSettings["32CharBarcodeCutStartIndex"]);
		protected System.Web.UI.HtmlControls.HtmlInputText txtConNo_;
		protected System.Web.UI.HtmlControls.HtmlInputButton hddConNo;
		protected System.Web.UI.HtmlControls.HtmlInputHidden hidHost;
		protected System.Web.UI.HtmlControls.HtmlInputHidden hidPort;
		public int CharBarcodeCutLength= Convert.ToInt32(ConfigurationSettings.AppSettings["32CharBarcodeCutLength"]);
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnImport.Click += new System.EventHandler(this.btnImport_Click);
			this.txtConNo.TextChanged += new System.EventHandler(this.txtConNo_TextChanged);
			this.txtDestPostCode.TextChanged += new System.EventHandler(this.txtDestPostCode_TextChanged);
			this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
			this.btnInsert.Click += new System.EventHandler(this.btnInsert_Click);
			this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
			this.btnInsert_Print.Click += new System.EventHandler(this.btnInsert_Print_Click);
			this.btnPrintSelected.Click += new System.EventHandler(this.btnPrintSelected_Click);
			this.lbSIPCon.SelectedIndexChanged += new System.EventHandler(this.lbSIPCon_SelectedIndexChanged);
			this.hddConNo.ServerClick += new System.EventHandler(this.hddConNo_ServerClick);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
		private void Page_Load(object sender, System.EventArgs e)
		{
			
			getSessionTimeOut(true);
			// Put user code to initialize the page here'
			txtConNo.Attributes.Add("onblur","return suppressOnBluer(this);");
			//txtConNo.Attributes.Add("onkeyDown", "return suppressNonEng(event,this);");

			txtConNo.Attributes.Add("onpaste", "return true;");
			txtConNo.Attributes.Add("onkeyUp", "KeyDownHandler();");
			
			
			txtDestPostCode.Attributes.Add("onpaste", "return false;");			
			txtDestPostCode.Attributes.Add("onkeyDown", "KeyDownHandler();");

			ddlServiceType.Attributes.Add("onkeyUp", "KeyDownHandler();");

			txtPkgNum.Attributes.Add("onkeyUp", "KeyDownHandler();");
			txtPkgNum.Attributes.Add("onpaste", "return false;");

			utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings,Page.Session);
			m_strAppID = utility.GetAppID();
			m_strEnterpriseID = utility.GetEnterpriseID();
			m_strCulture = utility.GetUserCulture();
			DataView dv = new DataView();
			dv.Table =Utility.GetStatusCode(m_strAppID,m_strEnterpriseID,"SIP").Tables[0];
			dv.RowFilter="status_code='SIP'";
			processid=  dv[0]["auto_process"].ToString();
			userID = utility.GetUserID();
			lblErrrMsg.Text = "";
			this.btnImport.Enabled=false;
			
			if(!Page.IsPostBack)
			{
				//Check role
				if(!checkRole())
				{
					pnlImport.Visible=false;
					fsImport.Visible=false;
					fsCreateSIP.Style.Add("Z-INDEX","105");
					fsCreateSIP.Style.Add("POSITION","absolute");
					fsCreateSIP.Style.Add("TOP","88px");

				}
	
				com.common.classes.User user =  RBACManager.GetUser(m_strAppID,m_strEnterpriseID, (String)Session["userID"]);
				lblUserDisplay.Text = user.UserID;
				lblLocationDisplay.Text = user.UserLocation;
				BindServiceType();
				setFocusCtrl(txtConNo.ClientID);
			}
			
		}
		private void BindServiceType()
		{
			ddlServiceType.Items.Clear();
			// Thosapol Yennam 10/10/2013 Modify
//			Session["Service_Type"] = Service.ReadAll(m_strAppID,m_strEnterpriseID);
			Session["Service_Type"] = QuickPODMgrDAL.SearchAllServiceAvailable(m_strAppID,m_strEnterpriseID,"","");
			ddlServiceType.DataSource= (DataSet)Session["Service_Type"];
			ddlServiceType.DataTextField="service_code";
			ddlServiceType.DataValueField="service_code";
			ddlServiceType.DataBind();
			ddlServiceType.Items.Insert(0," ");
		}

		private void btnInsert_Click(object sender, System.EventArgs e)
		{				
			if(txtConNo.Text.ToString()=="")
			{
				// none consignmentno
				lblErrrMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"CREATE_SIP_MSG_CONSIGNMENTNO_ERR",m_strCulture); 
				setFocusCtrl(txtConNo.ClientID);
			}
			else
			{
				string ConNo = "";
				if(Utility.ValidateConsignmentNo(txtConNo.Text.Trim(), ref ConNo)==false)
				{
					lblErrrMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"CREATE_SIP_MSG_CONSNO_INVALID",m_strCulture); 
					setFocusCtrl(txtConNo.ClientID);
					return;
				}
				txtConNo.Text = ConNo;
				Zipcode zipcode = new Zipcode();
				DataSet dsCountZip = new DataSet();
				dsCountZip = zipcode.ckZipcodeInSystem(m_strAppID, m_strEnterpriseID,txtDestPostCode.Text.ToString());

				if(txtDestPostCode.Text.ToString()=="" )
				{
					// none destpostcode
					lblErrrMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"CREATE_SIP_MSG_DESTPOSTALCODE_ERR",m_strCulture); 
					setFocusCtrl(txtDestPostCode.ClientID);
				}
				else if(dsCountZip.Tables[0].Rows.Count<=0)
				{
					lblErrrMsg.Text = "Invalid postal code.";
					setFocusCtrl(txtDestPostCode.ClientID);
				}
				else
				{
					if(ddlServiceType.SelectedValue=="0"||Service.IsAvailableFromDC(m_strAppID,m_strEnterpriseID,ddlServiceType.SelectedValue.ToString(),lblLocationDisplay.Text.ToString(),txtDestPostCode.Text.ToString())==false)
					{
						// not select ddlService or missing service type
						lblErrrMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"CREATE_SIP_MSG_SERVICETYPE_ERR",m_strCulture); 
						setFocusCtrl(ddlServiceType.ClientID);
					}
					else
					{
						if(txtPkgNum.Text==""||txtPkgNum.Text=="0")
						{
							//invalid pkg num
							lblErrrMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"CREATE_SIP_MSG_PKGNUM_ERR",m_strCulture); 
							setFocusCtrl(txtPkgNum.ClientID);
						}
						else
						{
							if(!IsInteger(txtPkgNum.Text))
							{
								//invalid pkg num
								lblErrrMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"CREATE_SIP_MSG_PKGNUM_ERR",m_strCulture); 
								setFocusCtrl(txtPkgNum.ClientID);
							}
							else
							{
								// insert to db
								string con = txtConNo.Text.ToString();
								string[] tmpcon = con.Split(' ');
								con=tmpcon[0];
								int result = SWB_Emulator.SWB_SIP_Insert(m_strAppID,m_strEnterpriseID,con,txtDestPostCode.Text.ToString(),
									txtPkgNum.Text.ToString(),ddlServiceType.SelectedValue.ToString(),lblLocationDisplay.Text.ToString(),lblUserDisplay.Text.ToString(),processid);

								lblErrrMsg.Text ="Create successfully.";
								generateDataToListbox();
								pkgNum = txtPkgNum.Text.Trim();
								consignmentNumber = txtConNo.Text.Trim();
								ClearScreen();
								setFocusCtrl(txtConNo.ClientID);
								return ;
							}
						
						}

					}
			
				}

				if(!getRoleOPSSU())
				{
					txtPkgNum.Enabled = false;
				}
				else
					txtPkgNum.Enabled = true;

			}
			setFocusCtrl(txtConNo.ClientID);
		}

		private void bindServiceTypefromPostalCode(string receipPostCode)
		{
			//String receipPostCode = null;
			//receipPostCode = txtDestPostCode.Text.Trim();
			string OriginDC = lblLocationDisplay.Text;
			if(txtConNo.Text!="")
			{	
				DataSet ds = new DataSet();
				ds= SWB_Emulator.SWB_SIP_Read(m_strAppID,m_strEnterpriseID,txtConNo.Text);
				if(ds.Tables[0].Rows.Count>0)
				{
					OriginDC=ds.Tables[0].Rows[0]["origin_DC"].ToString();
				}

			}
			DataSet dstmp = ConsignmentNoteDAL.SearchBestService(m_strAppID,m_strEnterpriseID,OriginDC,receipPostCode);

			string strBestService="";
			if(dstmp.Tables[0].Rows.Count>0)
			{
				strBestService=dstmp.Tables[0].Rows[0]["service_code"].ToString();
			}
			// Thosapol Yennam 10/10/2013 
			// DataSet dsDlvType = Service.ReadBestService(m_strAppID,m_strEnterpriseID,OriginDC,receipPostCode);
			DataSet dsDlvType = QuickPODMgrDAL.SearchAllServiceAvailable(m_strAppID,m_strEnterpriseID,OriginDC,receipPostCode);
			if (dsDlvType!=null)
			{
				ddlServiceType.DataSource = dsDlvType;
				ddlServiceType.DataTextField = "service_code"; 
				ddlServiceType.DataValueField="service_code";
				ddlServiceType.DataBind();
			}
			if(dsDlvType == null || dsDlvType.Tables.Count == 0 || dsDlvType.Tables[0].Rows.Count == 0)
			{
				txtDestPostCode.Text = "";
				lblProvince.Text="";
				//ddlServiceType.Enabled=false;
				lblErrrMsg.Text = "No service to this postal code.";
				setFocusCtrl(txtDestPostCode.ClientID);
				return;

			}
//			if(dsDlvType.Tables.Count == 0)
//			{
//				txtDestPostCode.Text = "";
//				lblProvince.Text="";
//				//ddlServiceType.Enabled=false;
//				lblErrrMsg.Text = "Invalid postal code.";
//				setFocusCtrl(txtDestPostCode.ClientID);
//				return;
//
//			}
//			if(dsDlvType.Tables[0].Rows.Count == 0)
//			{
//				txtDestPostCode.Text = "";	
//				lblProvince.Text="";
//				//ddlServiceType.Enabled=false;
//				lblErrrMsg.Text = "Invalid postal code.";
//				setFocusCtrl(txtDestPostCode.ClientID);
//				return;
//
//			}

		}
		private void txtDestPostCode_TextChanged(object sender, System.EventArgs e)
		{
			if(txtConNo.Text.ToString()=="")
			{// none consignmentno
				lblErrrMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"CREATE_SIP_MSG_CONSIGNMENTNO_ERR",m_strCulture); 
				setFocusCtrl(txtConNo.ClientID);
				return;
			}
			Zipcode zipcode = new Zipcode();
			DataSet dsCountZip = new DataSet();
			dsCountZip = zipcode.ckZipcodeInSystem(m_strAppID, m_strEnterpriseID,txtDestPostCode.Text.ToString());
			if (dsCountZip.Tables[0].Rows.Count<=0)
			{
				ddlServiceType.Enabled=false;
				lblErrrMsg.Text = "Invalid postal code.";
				setFocusCtrl(txtDestPostCode.ClientID);
				return;
			}
			ddlServiceType.Enabled=true;
			GetProvinceFromPostalCode();
			bindServiceTypefromPostalCode(txtDestPostCode.Text.Trim());
			setFocusCtrl(ddlServiceType.ClientID);
		}

		private void txtConNo_TextChanged(object sender, System.EventArgs e)
		{

		}
		private bool ValidateFedexFormat(string strConsignment_no)
		{
			bool result =false;
			if (strConsignment_no.Length==18)
			{
				object Expression = strConsignment_no[0];
				if(Expression is Int16 || Expression is Int32 || Expression is Int64 || Expression is Decimal || Expression is Single || Expression is Double || Expression is Boolean)
				{
					return false;
				}
				Expression = strConsignment_no[strConsignment_no.Length-1];
				if(Expression is Int16 || Expression is Int32 || Expression is Int64 || Expression is Decimal || Expression is Single || Expression is Double || Expression is Boolean)
				{
					return false;
				}
				else 
				{
					result =true;
				}
			}
			return result;
		}
		private void btnClear_Click(object sender, System.EventArgs e)
		{
			ClearScreen();
			setFocusCtrl(txtConNo.ClientID);
			txtPkgNum.Enabled = true;
		}

		private void btnUpdate_Click(object sender, System.EventArgs e)
		{
			if(txtConNo.Text.ToString()=="")
			{
				// none consignmentno
				lblErrrMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"CREATE_SIP_MSG_CONSIGNMENTNO_ERR",m_strCulture); 
				setFocusCtrl(txtConNo.ClientID);
			}
			else
			{
				Zipcode zipcode = new Zipcode();
				DataSet dsCountZip = new DataSet();
				dsCountZip = zipcode.ckZipcodeInSystem(m_strAppID, m_strEnterpriseID,txtDestPostCode.Text.ToString());

				if(txtDestPostCode.Text.ToString()=="" )
				{
					// none destpostcode
					lblErrrMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"CREATE_SIP_MSG_DESTPOSTALCODE_ERR",m_strCulture); 
					setFocusCtrl(txtDestPostCode.ClientID);

				}
				else if(dsCountZip.Tables[0].Rows.Count<=0)
				{
					lblErrrMsg.Text = "Invalid postal code.";
					setFocusCtrl(txtDestPostCode.ClientID);

				}
				else
				{
					if(ddlServiceType.SelectedValue=="0"||Service.IsAvailableFromDC(m_strAppID,m_strEnterpriseID,ddlServiceType.SelectedValue.ToString(),lblLocationDisplay.Text.ToString(),txtDestPostCode.Text.ToString())==false)
					{
						// not select ddlService or missing service type
						lblErrrMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"CREATE_SIP_MSG_SERVICETYPE_ERR",m_strCulture); 
                       	setFocusCtrl(ddlServiceType.ClientID);				
					}
					else
					{
						if(txtPkgNum.Text==""||txtPkgNum.Text=="0")
						{
							//invalid pkg num
							lblErrrMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"CREATE_SIP_MSG_PKGNUM_ERR",m_strCulture); 
							setFocusCtrl(txtPkgNum.ClientID);
						}
						else
						{
							if(!IsInteger(txtPkgNum.Text))
							{
								//invalid pkg num
								lblErrrMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"CREATE_SIP_MSG_PKGNUM_ERR",m_strCulture); 
								setFocusCtrl(txtPkgNum.ClientID);
							}
							else
							{
								// insert to db
								string con = txtConNo.Text.ToString();
								string[] tmpcon = con.Split(' ');
								con=tmpcon[0];

								int result = SWB_Emulator.SWB_SIP_Update(m_strAppID,m_strEnterpriseID,con,txtDestPostCode.Text.ToString(),
									txtPkgNum.Text.ToString(),ddlServiceType.SelectedValue.ToString(),lblLocationDisplay.Text.ToString(),lblUserDisplay.Text.ToString());

								lblErrrMsg.Text ="Update successfully.";
								generateDataToListbox();
								pkgNum = txtPkgNum.Text.Trim();
								consignmentNumber = txtConNo.Text.Trim();
								ClearScreen();
							}
							
						}
						
					}

				}
			
			}
	setFocusCtrl(txtConNo.ClientID);
		}

		private void btnImport_Click(object sender, System.EventArgs e)
		{
			
			this.lblErrrMsg.Text = "";

			////			try
			////			{
			if(uploadFiletoServer())
			{

				getExcelFiles();

				DataSet ds = (DataSet) Session["dsCustZone_Import"];

				// Put data to listbox...
			}
		}

		private void generateDataToListbox()
		{
			utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings,Page.Session);
			string con = txtConNo.Text.ToString();
			string[] tmpcon = con.Split(' ');
			con=tmpcon[0];
			string data  = con.ToString()+utility.generateHTMLSpace(25-con.ToString().Length);
			data += ddlServiceType.SelectedValue.ToString()+utility.generateHTMLSpace(7- ddlServiceType.SelectedValue.ToString().Length);
			data += txtDestPostCode.Text .ToString()+utility.generateHTMLSpace(11- txtDestPostCode.Text.ToString().Length);
			data += txtPkgNum.Text.ToString()+utility.generateHTMLSpace(5-txtPkgNum.Text.ToString().Length);
			data += DateTime.Now.ToString("dd/MM/yyyy HH:mm")+utility.generateHTMLSpace(18-DateTime.Now.ToString("dd/MM/yyyy HH:mm").Length);
			data += lblUserDisplay.Text.ToString()+utility.generateHTMLSpace(8-lblUserDisplay.Text.ToString().Length);
			data += lblLocationDisplay.Text.ToString()+utility.generateHTMLSpace(5-lblLocationDisplay.Text.ToString().Length);
			data += lbltmpDestDC.Text.ToString()+utility.generateHTMLSpace(5-lbltmpDestDC.Text.ToString().Length);
			//ListItemCollection tmpListCollection  =  new ListItemCollection();
			foreach(ListItem list in lbSIPCon.Items)
			{
			 			
				string lValue = list.Value;
				string[] tmpval = lValue.Split('@');
				lValue=tmpval[0];
				if(lValue==con)
				{
					lbSIPCon.Items.Remove(list);
					break;
				}
			}

			string val = con.ToString()+"@"+DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
			ListItem lData = new ListItem(data,val);
			lbSIPCon.Items.Insert(lbSIPCon.Items.Count,lData);
			lblSipCountDisplay.Text =lbSIPCon.Items.Count.ToString();
			lblSubmitDateTimeDisplay.Text  = DateTime.Now.ToString("dd/MM/yyyy HH:mm");
			
		}

		private void generateDataToListboxFromImport(DataRow dr)
		{
			string strConsignmentno=(string)dr["Consignment_no"].ToString();
			string strService_Type=(string)dr["Service_Type"].ToString();
			string strDest_Postal=(string)dr["Dest_Postal_Code"].ToString();
			string  totalPKG=(string)dr["Total_Pkg"].ToString();
			string strCreateBy=(string)dr["CreatedBy"].ToString();
			DateTime dtCreateDate=Convert.ToDateTime(dr["CreateDT"]);
			string strDestDC =  GetProvinceFromPostalCode(strDest_Postal);

			utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings,Page.Session);
			string con = strConsignmentno.ToString();
			string[] tmpcon = con.Split(' ');
			con=tmpcon[0];
			string data  = con.ToString()+utility.generateHTMLSpace(25-con.ToString().Length);
			data += strService_Type.ToString()+utility.generateHTMLSpace(7- strService_Type.ToString().Length);
			data += strDest_Postal .ToString()+utility.generateHTMLSpace(11- strDest_Postal.ToString().Length);
			data += totalPKG.ToString()+utility.generateHTMLSpace(5-totalPKG.ToString().Length);
			data += DateTime.Now.ToString("dd/MM/yyyy HH:mm")+utility.generateHTMLSpace(18-DateTime.Now.ToString("dd/MM/yyyy HH:mm").Length);
			data += lblUserDisplay.Text.ToString()+utility.generateHTMLSpace(8-lblUserDisplay.Text.ToString().Length);
			data += lblLocationDisplay.Text.ToString()+utility.generateHTMLSpace(5-lblLocationDisplay.Text.ToString().Length);
			data += strDestDC.ToString()+utility.generateHTMLSpace(5-strDestDC.ToString().Length);
			//ListItemCollection tmpListCollection  =  new ListItemCollection();
			foreach(ListItem list in lbSIPCon.Items)
			{
			 			
				string lValue = list.Value;
				string[] tmpval = lValue.Split('@');
				lValue=tmpval[0];
				if(lValue==con)
				{
					lbSIPCon.Items.Remove(list);
					break;
				}
			}

			string val =con.ToString()+"@"+DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
			ListItem lData = new ListItem(data,val);
			lbSIPCon.Items.Insert(lbSIPCon.Items.Count,lData);
			lblSipCountDisplay.Text =lbSIPCon.Items.Count.ToString();
			lblSubmitDateTimeDisplay.Text  = DateTime.Now.ToString("dd/MM/yyyy HH:mm");
			
		}
		private void GetProvinceFromPostalCode()
		{
			Zipcode zipcode = new Zipcode();
			zipcode.Populate(m_strAppID,m_strEnterpriseID,txtDestPostCode.Text.ToString());
			DataSet dsCountZip = new DataSet();
			dsCountZip = zipcode.ckZipcodeInSystem(m_strAppID, m_strEnterpriseID,txtDestPostCode.Text.ToString());
			if(dsCountZip.Tables[0].Rows.Count>0)
			{
				lblProvince.Text = dsCountZip.Tables[0].Rows[0]["state_code"].ToString();
				DataSet dsDC = ImportConsignmentsDAL.getDCToDestStation(m_strAppID, m_strEnterpriseID,
					0, 0, zipcode.DeliveryRoute).ds;
				lbltmpDestDC.Text  = dsDC.Tables[0].Rows[0]["origin_station"].ToString();

			}
			else
			{
				lblErrrMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"CREATE_SIP_MSG_DESTPOSTALCODE_ERR",m_strCulture); 
			}
		}
		private string  GetProvinceFromPostalCode(string dest_PotalCode)
		{
			string strProvince="";
			Zipcode zipcode = new Zipcode();
			zipcode.Populate(m_strAppID,m_strEnterpriseID,dest_PotalCode);
			DataSet dsCountZip = new DataSet();
			dsCountZip = zipcode.ckZipcodeInSystem(m_strAppID, m_strEnterpriseID,dest_PotalCode);
			if(dsCountZip.Tables[0].Rows.Count>0)
			{
				lblProvince.Text = dsCountZip.Tables[0].Rows[0]["state_code"].ToString();
				DataSet dsDC = ImportConsignmentsDAL.getDCToDestStation(m_strAppID, m_strEnterpriseID,
					0, 0, zipcode.DeliveryRoute).ds;
				strProvince = dsDC.Tables[0].Rows[0]["origin_station"].ToString();

			}
			//			else
			//			{
			//				lblErrrMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"CREATE_SIP_MSG_DESTPOSTALCODE_ERR",m_strCulture); 
			//			}
			return strProvince;
		}
		private void ClearScreen()
		{
			btnInsert.Enabled= false;
			btnUpdate.Enabled=false;
			btnInsert_Print.Enabled=false;
			btnPrintSelected.Enabled=false;
			txtConNo.ReadOnly=false;
			txtConNo.Text="";
			txtConNo.Enabled=true;
			txtDestPostCode.Text="";
			txtPkgNum.Text="";
			lblProvince.Text="";
			lbltmpDestDC.Text="";
			txtFilePath.Text="";
			lblErrrMsg.Text="";
			BindServiceType();
			ddlServiceType.SelectedIndex=0;

		}
		public bool IsInteger(String strNumber)
		{
			Regex objNotNaturalPattern=new Regex("[^0-9]");
			Regex objNaturalPattern=new Regex("0*[1-9][0-9]*");
			return !objNotNaturalPattern.IsMatch(strNumber) &&
			objNaturalPattern.IsMatch(strNumber);
		} 
		private bool uploadFiletoServer()
		{
			bool status = true;

			if((inFile.PostedFile != null ) && (inFile.PostedFile.ContentLength > 0))
			{
				string extension = System.IO.Path.GetExtension(inFile.PostedFile.FileName);

				if(extension.ToUpper() == ".XLS")
				{
					string fn = System.IO.Path.GetFileName(inFile.PostedFile.FileName);
					string path = Server.MapPath("Excel") + "\\";
					string pathFn = Server.MapPath("Excel") + "\\" + this.User.ToString() + "_" + fn;

					ViewState["FileName"] = pathFn;

					try
					{
						if(System.IO.Directory.Exists(path) == false)
							System.IO.Directory.CreateDirectory(path);

						if (System.IO.File.Exists(pathFn)) 
							System.IO.File.Delete(pathFn);

						inFile.PostedFile.SaveAs(pathFn);

						this.lblErrrMsg.Text = "";
					}
					catch(Exception ex)
					{
						throw new ApplicationException("Error during upload file to the server", null);
					}
				}
				else
				{
					this.lblErrrMsg.Text = "Not a valid Excel Workbook.";
					txtFilePath.Text = "";
					status = false;
				}
			}
			else
			{
				lblErrrMsg.Text = "Please select a file to upload.";
				status = false;
			}

			return status;
		}
		private void getExcelFiles()
		{
			int i = 0;

			try
			{	
				String connString = ConfigurationSettings.AppSettings["ExcelConn"];
				connString = connString.Replace("(DestinationFile)", (String)ViewState["FileName"]);
				int recCouter = 1;

				StringBuilder strBuilder = new StringBuilder();
				strBuilder.Append("SELECT  Consignment_no, Service_Type, Dest_Postal_Code, Total_Pkg,CreatedBy,CreateDT ");
				//HC Return Task
				strBuilder.Append("FROM [Sheet1$] ");
				OleDbDataAdapter daConsignment = new OleDbDataAdapter(strBuilder.ToString(), connString);

				DataSet dsExcel = new DataSet();
				daConsignment.Fill(dsExcel);

				DataTable dt = new DataTable("tmpSIPCons");
				dt.Columns.Add(new DataColumn("Consignment_no",typeof(string)));
				dt.Columns.Add(new DataColumn("Service_Type",typeof(string)));
				dt.Columns.Add(new DataColumn("Dest_Postal_Code",typeof(string)));
				dt.Columns.Add(new DataColumn("Total_Pkg",typeof(string)));
				dt.Columns.Add(new DataColumn("CreatedBy",typeof(string)));
				dt.Columns.Add(new DataColumn("CreateDT",typeof(string)));

				dsExcel.Tables.Add(dt);
				//
				//				if(dsExcel.Tables[0].Rows.Count > 0)
				//				{
				//					dsExcel.Tables[0].Rows[0].Delete();
				//					dsExcel.AcceptChanges();
				//				}


				if(validateImportExcelFile(dsExcel))
				{
					int cntIns=0;
					int cntUpd=0;
					foreach(DataRow dr in dsExcel.Tables[0].Rows)
					{
					
						string strConsignmentno=(string)dr["Consignment_no"].ToString();
						string strService_Type=(string)dr["Service_Type"].ToString();
						string strDest_Postal=(string)dr["Dest_Postal_Code"].ToString();
						string  totalPKG=(string)dr["Total_Pkg"].ToString();
						string strCreateBy=(string)dr["CreatedBy"].ToString();
						DateTime dtCreateDate=Convert.ToDateTime(dr["CreateDT"]);
						DataSet ds = new DataSet();
						ds= SWB_Emulator.SWB_SIP_Read(m_strAppID,m_strEnterpriseID,strConsignmentno);
						if(ds.Tables[0].Rows.Count<=0)
						{
							int result = SWB_Emulator.SWB_SIP_Insert(m_strAppID,m_strEnterpriseID,strConsignmentno,strDest_Postal,
								totalPKG.ToString(),strService_Type,lblLocationDisplay.Text.ToString(),strCreateBy,processid);
							generateDataToListboxFromImport(dr);
							cntIns +=1;

						}
						else
						{
							int result = SWB_Emulator.SWB_SIP_Update(m_strAppID,m_strEnterpriseID,strConsignmentno,strDest_Postal,
								totalPKG.ToString(),strService_Type,lblLocationDisplay.Text.ToString(),strCreateBy);
							generateDataToListboxFromImport(dr);
							cntUpd +=1;
						}
						lblErrrMsg.Text ="Import SIP successfully: "+cntIns+" Inserted, "+cntUpd +" Updated";
					}
				}
				lblProvince.Text = "";
				txtFilePath.Text="";
				dsExcel.Dispose();
				dsExcel = null;
				i++;
				recCouter = 1;
				//ViewState["beginCuszoneImport"] = beginCustZone;
				//Session["dsCustZone_Import"] = dsCustZone;
			}
			catch(Exception ex)
			{
								String strMsg = ex.Message;
				
				
								if(strMsg.IndexOf("'Sheet1$' is not a valid name") != -1)
								{
									//throw new ApplicationException("Excel Workbook must contain  named sheets: Sheet1.", null);
									lblErrrMsg.Text = "Excel Workbook must contain  named sheets: Sheet1.";
								}
//								else if(strMsg.IndexOf("'Packages$' is not a valid name") != -1)
//								{
//									throw new ApplicationException("Excel Workbook must contain two named sheets: Consignments and Packages.", null);
//								}
								else if(strMsg.IndexOf("No value given for one or more required parameters") != -1)
								{
									if(i == 0)
										//throw new ApplicationException("Excel worksheet has invalid columns", null);
										lblErrrMsg.Text = "Excel worksheet has invalid columns";
								}
								else
								{
									throw ex;
								}
			}
		}
		private bool checkRole()
		{
			try
			{
				User user = new User();
				user.UserID = userID;
				ArrayList userRoleArray = com.common.RBAC.RBACManager.GetAllRoles(m_strAppID,m_strEnterpriseID,user);
				Role role;
				bool isSWRole = false;
				for(int i=0;i<userRoleArray.Count;i++)
				{
					role = (Role)userRoleArray[i];
					if(role.RoleName.ToUpper().Trim()=="SWIMPORT")				
					{
						isSWRole = true;
						return true;
					}
					else
					{
						isSWRole = false;
					}
				}
				return isSWRole;
			}
			catch
			{
				return false;
			}
		}

		private void lbSIPCon_SelectedIndexChanged(object sender, System.EventArgs e)
		{
		
			DataSet ds = new DataSet();
			string con = lbSIPCon.SelectedValue.ToString();
			string[] tmpcon = con.Split('@');
			con= tmpcon[0];
			ds= SWB_Emulator.SWB_SIP_Read(m_strAppID,m_strEnterpriseID,con);
			if (ds.Tables[0].Rows.Count>0)
			{

				bindServiceTypefromPostalCode(ds.Tables[0].Rows[0]["dest_postal_code"].ToString());
				txtConNo.Text = ds.Tables[0].Rows[0]["consignment_no"].ToString();
				txtDestPostCode.Text = ds.Tables[0].Rows[0]["dest_postal_code"].ToString();
				ddlServiceType.SelectedValue= ds.Tables[0].Rows[0]["service_code"].ToString();
				txtPkgNum.Text= ds.Tables[0].Rows[0]["number_of_package"].ToString();
				GetProvinceFromPostalCode();
				btnInsert.Enabled=false;
				txtConNo.Enabled=false;
				btnUpdate.Enabled=true;
				btnInsert_Print.Enabled=false;
				btnPrintSelected.Enabled=true;
				
				if(!getRoleOPSSU())
				{
					txtPkgNum.Enabled = false;
				}
				else
					txtPkgNum.Enabled = true;
			}
		}
		private bool validateImportExcelFile(DataSet ds)
		{
			bool result=true;
			foreach(DataRow dr in ds.Tables[0].Rows)
			{
				string strConsignmentno=(string)dr["Consignment_no"].ToString();
				string strService_Type=(string)dr["Service_Type"].ToString();
				string strDest_Postal=(string)dr["Dest_Postal_Code"].ToString();
				double totalPKG=Convert.ToDouble(dr["Total_Pkg"]);
				string strCreateBy=(string)dr["CreatedBy"].ToString();
				DateTime dtCreateDate=Convert.ToDateTime(dr["CreateDT"]);

				if(strConsignmentno.ToString()=="")
				{
					// none consignmentno
					lblErrrMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"CREATE_SIP_MSG_CONSIGNMENTNO_ERR",m_strCulture); 
					result = false;
					break;
				}
				else
				{
					Zipcode zipcode = new Zipcode();
					DataSet dsCountZip = new DataSet();
					dsCountZip = zipcode.ckZipcodeInSystem(m_strAppID, m_strEnterpriseID,strDest_Postal.ToString());

					if(strDest_Postal.ToString()==""||dsCountZip.Tables[0].Rows.Count<=0  )
					{
						// none destpostcode
						lblErrrMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"CREATE_SIP_MSG_DESTPOSTALCODE_ERR",m_strCulture); 
						result = false;
						break;
					}
					else
					{
						if(strService_Type==""||Service.IsAvailableFromDC(m_strAppID,m_strEnterpriseID,strService_Type,GetProvinceFromPostalCode(strDest_Postal),strDest_Postal.ToString())==false)
						{
							// missing service type
							lblErrrMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"CREATE_SIP_MSG_SERVICETYPE_ERR",m_strCulture); 
							result = false;
							break;
						}
						else
						{
							if(totalPKG==0||totalPKG>9999)
							{
								//invalid pkg num
								lblErrrMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"CREATE_SIP_MSG_PKGNUM_ERR",m_strCulture); 
								result = false;
								break;
							}
							else
							{
								lblErrrMsg.Text ="";
								result = true;
							}
						}
					}
				}
			}
			return result;
		}

		private void setFocusCtrl(string ctrlName)

		{
			string script="<script langauge='javacript'>";
					script+= "document.all['"+ ctrlName +"'].focus();";
					script+= "</script>";
			Page.RegisterStartupScript("setFocus"+DateTime.Now.ToString("ddMMyyyyhhmmss"),script);
		
		}

		private void btnInsert_Print_Click(object sender, System.EventArgs e)
		{
			string ConNo = "";
			if(Utility.ValidateConsignmentNo(txtConNo.Text.Trim(), ref ConNo)==false)
			{
				lblErrrMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"CREATE_SIP_MSG_CONSNO_INVALID",m_strCulture); 
				setFocusCtrl(txtConNo.ClientID);
				return;
			}
			txtConNo.Text = ConNo;
			btnInsert_Click(sender,e);
//			string pdfFileName;
			String strConNo = HttpUtility.UrlEncode(consignmentNumber);
			//strConNo="PL"+System.DateTime.Now.ToString("ddMMyyyyhhmmss");
//			String strPkgNum = pkgNum;
//			pdfFileName = "http://"+Request.Url.Host+Request.Url.AbsolutePath;
//			pdfFileName = pdfFileName.Substring(0,pdfFileName.Length-28)+"/Export/"+ strConNo +".pdf";

			pdfFileName = "http://"+Request.Url.Host+":"+ Request.Url.Port.ToString()+Request.ApplicationPath;
			pdfFileName = pdfFileName+"/WebUI/Export/"+ strConNo +".pdf";

//			String sUrl = "Popup_PrintLicensePlate.aspx?ConNo="+strConNo+"&PkgNum="+strPkgNum+"&PdfPath="+pdfFileName;
//			ArrayList paramList = new ArrayList();
//			paramList.Add(sUrl);
//			String sScript = Utility.GetScript("openWindowParam.js",paramList);
//			Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
			PrintReport(m_strAppID,m_strEnterpriseID,consignmentNumber,pkgNum);

			if(!getRoleOPSSU())
			{
				txtPkgNum.Enabled = false;
			}
			else
				txtPkgNum.Enabled = true;

		}

		private void btnPrintSelected_Click(object sender, System.EventArgs e)
		{
			btnUpdate_Click(sender,e);
			string pdfFileName;
			String strConNo = HttpUtility.UrlEncode(consignmentNumber);
			String strPkgNum = pkgNum;
//			pdfFileName = "http://"+Request.Url.Host+Request.Url.AbsolutePath;
//			pdfFileName = pdfFileName.Substring(0,pdfFileName.Length-28)+"/Export/"+Server.UrlEncode(strConNo)+".pdf";

			// Thosapol Yennam (02/09/2013) Comment
//			pdfFileName = "http://"+Request.Url.Host+":"+ Request.Url.Port.ToString()+Request.ApplicationPath;
//			pdfFileName = pdfFileName+"/WebUI/Export/"+Server.UrlEncode(strConNo)+".pdf";

			// Thosapol Yennam (02/09/2013) New Code
			string sAutoFilePdf = System.Configuration.ConfigurationSettings.AppSettings["InvoiceExportPath"];
			pdfFileName = sAutoFilePdf+ conName +".pdf";

			String sUrl = "Popup_PrintLicensePlate.aspx?ConNo=" + strConNo + "&PkgNum="+strPkgNum+"&PdfPath="+pdfFileName.Replace("'", "\\'") + "&Loc="+lblLocationDisplay.Text;
			ArrayList paramList = new ArrayList();
			paramList.Add(sUrl);
			String sScript = Utility.GetScriptPopupSetSize("OpenSetSize.js",paramList,600,550);
			Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
		}

		private void PrintReport(String strAppID, String strEnterpriseID, String strConsignmentNo, String strPageRange)
		{
			strConsignmentNo=strConsignmentNo.Replace("'","''");
			DataSet dsLicensePlate = GetLicensePlateData(strAppID,strEnterpriseID,strConsignmentNo);
			
			if(dsLicensePlate.Tables[0].Rows.Count > 0)
			{
				DataTable dtPrint = new DataTable();
				dtPrint.Columns.Add("consignment");
				dtPrint.Columns.Add("reference_name");
				dtPrint.Columns.Add("telephone");
				dtPrint.Columns.Add("address");
				dtPrint.Columns.Add("contactperson");
				dtPrint.Columns.Add("zip");
				dtPrint.Columns.Add("route1");
				dtPrint.Columns.Add("route2");
				dtPrint.Columns.Add("service");
				dtPrint.Columns.Add("derv");
				dtPrint.Columns.Add("province");
				dtPrint.Columns.Add("sender_name");
				dtPrint.Columns.Add("sender_tel");
				dtPrint.Columns.Add("cust_ref");
				dtPrint.Columns.Add("uid");
				dtPrint.Columns.Add("swbid");
				dtPrint.Columns.Add("dclocation");
				dtPrint.Columns.Add("dateprint");
				dtPrint.Columns.Add("datescan");
				dtPrint.Columns.Add("package");
				dtPrint.Columns.Add("Label");

				dtPrint = fillLicensePlateData(dsLicensePlate,dtPrint);
				
				if(dtPrint.Rows.Count > 0)
				{
					ReportDocument crReportDocument = new TIES.WebUI.ConsignmentNote.LicensePlate();

					ParameterFieldDefinitions paramFldDefs ;
					ParameterValues paramVals = new ParameterValues() ;
					ParameterDiscreteValue paramDVal = new ParameterDiscreteValue();

					//paramFldDefs = rptSource.DataDefinition.ParameterFields;

					paramFldDefs = crReportDocument.DataDefinition.ParameterFields;

					foreach(ParameterFieldDefinition paramFldDef in paramFldDefs)
					{
						switch(paramFldDef.ParameterFieldName)
						{
							case "label_test":
								paramDVal.Value = System.Configuration.ConfigurationSettings.AppSettings["label_test"].ToString();
								break;
							default:
								continue;
						}
						paramVals = paramFldDef.CurrentValues;
						paramVals.Add(paramDVal);
						paramFldDef.ApplyCurrentValues(paramVals);
					}

					ExportOptions crExportOptions  = new ExportOptions();
					DiskFileDestinationOptions crDiskFileDestinationOptions  = new DiskFileDestinationOptions();
 
					String Fname = "";

					crReportDocument.SetDataSource(dtPrint);
					//				pdfFileName = "http://"+Request.Url.Host+Request.Url.AbsolutePath;
					//				pdfFileName = pdfFileName.Substring(0,pdfFileName.Length-38)+"/Export/"+conName+".pdf";
					Fname = Server.MapPath("..\\Export") + "\\"+conName+".pdf";

					// Thosapol Yennam (02/09/2013) Comment
//					pdfFileName = "http://"+Request.Url.Host+Request.Url.AbsolutePath;
//					pdfFileName = pdfFileName.Substring(0,pdfFileName.Length-28)+"/Export/"+conName+".pdf";

					// Thosapol Yennam (03/09/2013) New Code
					System.Uri uri = HttpContext.Current.Request.Url;
					string sUrl = uri.Scheme + "://" + uri.Host;
					string sPath = PathExport(uri.PathAndQuery);
					string strHost = Request.Form["hidHost"];
					string strPort = Request.Form["hidPort"];
					if (strPort != null )
					{
						if(strPort != String.Empty)
						{
							pdfFileName = sUrl+":"+strPort+sPath+"/Export/"+ conName +".pdf";						
						}
						else
						{
							pdfFileName = sUrl+sPath+"/Export/"+ conName +".pdf";
						}
					}
				
					crDiskFileDestinationOptions = new DiskFileDestinationOptions();
					crDiskFileDestinationOptions.DiskFileName = Fname;
					crExportOptions = crReportDocument.ExportOptions;

					crExportOptions.DestinationOptions = crDiskFileDestinationOptions;
					crExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
					crExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;

					crExportOptions.FormatOptions = new PdfRtfWordFormatOptions();
					crReportDocument.Export();

					// Thosapol Yennam 26/08/2013 Option Print Auto and Manual
					bool flgAutoPrint = IsAutoPrint(strAppID,strEnterpriseID,lblLocationDisplay.Text);
					
					if(flgAutoPrint == true ) 
					{
						string strUrl = "./Popup_PrintLicensePlate_Direct.aspx";
						Session["AutoPrint"] = "onload=\"PrintRemotePDFFile()\"";
						Session["CreatePrint"] = "<object id=\"pdfDoc\"  name=\"pdfDoc\" classid=\"clsid:CA8A9780-280D-11CF-A24D-444553540000\" width=\"1px\" height=\"1px\" VIEWASTEXT><param name=\"SRC\" value=\""+pdfFileName+"\" /></object>";
						ArrayList paramList = new ArrayList();
						paramList.Add(strUrl);
						//String sScript = Utility.GetScript("openNewChildParamDirect.js",paramList);
						String sScript = Utility.GetScriptPopupSetSize("OpenSetSize.js",paramList,200,400);
						Utility.RegisterScriptString(sScript,"ShowPopupScript"+DateTime.Now.ToString("ddMMyyyyhhmmss"),this.Page);
					}
					else
					{
						string sFilePdf = ConfigurationSettings.AppSettings["InvoiceExportPath"] +conName+".pdf";
						//string sFilePdf = "C:\\Inetpub\\wwwroot\\PRD\\TIES\\WebUI\\Export\\" +conName+".pdf";		// For Test
						bool flgExists = false;
						do
						{
							if( IsfileExists(sFilePdf) == true )
							{
								flgExists = true;
							}
							else
							{
								flgExists = false;
								System.Threading.Thread.Sleep(3000);
							}
						} while (flgExists == false);
						
//						System.IO.FileStream fs = new System.IO.FileStream(sFilePdf, System.IO.FileMode.Open, System.IO.FileAccess.Read);
//						byte[] ar = new byte[(int)fs.Length];
//						fs.Read(ar, 0, (int)fs.Length);
//						fs.Close();

//						Response.AddHeader("content-disposition", "attachment;filename=" + conName+".pdf");
//						Response.ContentType = "application/octectstream";
//						Response.BinaryWrite(ar);
//						Response.End();
//
//						Response.Clear();
//						Response.ContentType = "application/pdf";
//						Response.WriteFile(sFilePdf);
//						Response.End();

						string paramUrl = "PopUp_PrintCreteSIP.aspx?fname=" + conName+".pdf";
						Response.Write("<script type='text/javascript'>window.open('" + paramUrl + "','_blank');</script>");
					}
				}
			}
		}
		private string PathExport(string sPath)
		{
			string[] arrurl = sPath.Split('/');
			string rUrl = string.Empty;
			for(int i=0; i<arrurl.Length; i++)
			{
				if(arrurl[i] != "Manifest" )    
				{		
					if(arrurl[i] != string.Empty)
					{
						rUrl = rUrl + "/" + arrurl[i];					
					}								
				}
				else
				{
					break;
				}
			}

			return rUrl;
		}
		private DataTable fillLicensePlateData(DataSet dsLicensePlate,DataTable dtPrint)
		{
			foreach (DataRow drLicensePlate in dsLicensePlate.Tables[0].Rows) 
			{
				for(int i=1;i<(int.Parse(pkgNum)+1);i++)
				{
					DataRow dr;
					dr = dtPrint.NewRow();
					dr["consignment"] = drLicensePlate["consignment_no"];
					dr["reference_name"] = drLicensePlate["reference_name"];
					dr["telephone"] = drLicensePlate["telephone"];
					dr["address"] = drLicensePlate["address"];
					dr["contactperson"] = drLicensePlate["contactperson"];
					dr["zip"] = drLicensePlate["ZIP_Code"];
					dr["route1"] = drLicensePlate["linehaul1"];
					dr["route2"] = drLicensePlate["linehaul2"];
					dr["service"] = drLicensePlate["service_type"];
					dr["derv"] = drLicensePlate["delivery_route_code"];
					dr["province"] = drLicensePlate["province"];
					dr["sender_name"] = drLicensePlate["sender_name"];
					dr["sender_tel"] = drLicensePlate["sender_tel"];
					dr["cust_ref"] = drLicensePlate["cust_ref"];
					dr["uid"] = drLicensePlate["user_id_created"];
					dr["swbid"] = drLicensePlate["swb_id"];
					dr["dclocation"] = drLicensePlate["dc_location"];
					dr["dateprint"] = DateTime.Now.ToString("dd/MM/yyyy HH:mm");//drLicensePlate["created_datetime"];
					dr["datescan"] = drLicensePlate["scanned_in_datetime"];
					dr["package"] = i + " of " + pkgNum;
					dr["Label"] = "*" + drLicensePlate["consignment_no"].ToString() + "=" + i + "=" + pkgNum + "=" + drLicensePlate["delivery_route_code"].ToString()+ "*";
					dtPrint.Rows.Add(dr);
				}
				conName = drLicensePlate["consignment_no"].ToString();
				conName="PL"+System.DateTime.Now.ToString("ddMMyyyyhhmmss");
			}
			return dtPrint;
		}
		private DataSet GetLicensePlateData(string strAppID, string strEnterprintID, string strConsignmentNo)
		{
			DbConnection dbCon = null;
			IDbCommand dbCmd = null;
			DataSet DS = null;

			dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterprintID);
			if (dbCon == null)
			{
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "GetPrintReportLicensePlate", "dbCon is null");
				throw new ApplicationException("Connection to Database failed", null); 
			}

			StringBuilder strQry = new StringBuilder();
			strQry = strQry.Append("select consignment_no, null as Payer_ID,service_code as Service_Type,state_name  AS Province, ");
			strQry = strQry.Append("dest_postal_code as ZIP_Code, number_of_package, null as REFERENCE_NAME, ");
			strQry = strQry.Append("null as TELEPHONE, null as Address1, null as ADDRESS, null as CONTACTPERSON, NULL AS SENDER_NAME, ");
			strQry = strQry.Append("NULL AS SENDER_TEL,NULL AS CUST_REF,NULL AS linehaul1,NULL AS linehaul2,user_id_created, ");
			strQry = strQry.Append("' ' AS swb_id,origin_DC AS dc_location,created_datetime,scanned_in_datetime,delivery_route_code ");
			strQry = strQry.Append("from Consignment_Details ");
			strQry = strQry.Append(" left join (SELECT     State.state_name,Zipcode, State.applicationid, State.enterpriseid FROM State INNER JOIN ");
			strQry = strQry.Append(" Zipcode ON State.applicationid = Zipcode.applicationid AND State.enterpriseid = Zipcode.enterpriseid AND State.country = Zipcode.country AND ");
			strQry = strQry.Append(" State.state_code = Zipcode.state_code ");
			strQry = strQry.Append(" WHERE     (Zipcode.enterpriseid = '"+strEnterprintID+"') AND (Zipcode.applicationid = '"+strAppID+"')) a on Consignment_Details.dest_postal_code = a.ZipCode ");
			strQry = strQry.Append("where consignment_no = '"+strConsignmentNo+"'");

			dbCmd = dbCon.CreateCommand(strQry.ToString(), CommandType.Text);

			try
			{
				DS = (DataSet)dbCon.ExecuteQuery(dbCmd, com.common.DAL.ReturnType.DataSetType);
				return DS;
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("TIESDAL", "PopupPrintLicensePlate", "GetPrintReportLicensePlate", "Error in Query String " + appException.Message);
				throw appException;
			}
			finally
			{
				DS.Dispose();
			}
		}

		private void hddConNo_ServerClick(object sender, System.EventArgs e)
		{
			string ConNo="";
			if(Utility.ValidateConsignmentNo(txtConNo.Text.Trim(),ref ConNo)==false)
			{
				if(txtConNo.Text.Trim() != "")
				{
					lblErrrMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"CREATE_SIP_MSG_CONSNO_INVALID",m_strCulture); 
					setFocusCtrl(txtConNo.ClientID);
				}
				return;
			}
			txtConNo.Text=ConNo;
			
			if (txtConNo.Text!="")
			{
				DataSet ds = new DataSet();
				string con = txtConNo.Text.ToString();
				//				if(ValidateFedexFormat(txtConNo.Text.ToString().Trim()))
				//				{
				//					con = txtConNo.Text.Substring(1,CharBarcodeCutLength);
				//					//con = con.Substring(0,4)+" "+con.Substring(4,4)+" "+con.Substring(8);
				//
				//				}
				//				else 
				if(txtConNo.Text.Length==32)//Fedex32 Format
				{
					con= txtConNo.Text.Substring(CharBarcodeCutStartIndex,CharBarcodeCutLength);
				}
				else
				{
					string[] tmpcon = con.Split(' ');
					con=tmpcon[0];

				}
				
				
				
				ds= SWB_Emulator.SWB_SIP_Read(m_strAppID,m_strEnterpriseID,con);
				if (ds.Tables[0].Rows.Count>0)
				{
					txtConNo.Enabled=false;
					bindServiceTypefromPostalCode(ds.Tables[0].Rows[0]["dest_postal_code"].ToString());
					txtConNo.Text = ds.Tables[0].Rows[0]["consignment_no"].ToString();
					txtDestPostCode.Text = ds.Tables[0].Rows[0]["dest_postal_code"].ToString();
					bindServiceTypefromPostalCode(txtDestPostCode.Text);
					if(ddlServiceType.Items.FindByValue(ds.Tables[0].Rows[0]["service_code"].ToString())!=null)
					{
						ddlServiceType.SelectedValue= ds.Tables[0].Rows[0]["service_code"].ToString();
					}
					else
					{
						ddlServiceType.Items.Insert(0," ");
						ddlServiceType.SelectedIndex=0;
						lblErrrMsg.Text = "Invalid Service Type.";
						setFocusCtrl(ddlServiceType.ClientID);
						//return;
					}
					txtPkgNum.Text= ds.Tables[0].Rows[0]["number_of_package"].ToString();
					GetProvinceFromPostalCode();
					btnInsert.Enabled=false;
					txtConNo.ReadOnly=true;
					btnUpdate.Enabled=true;
					btnPrintSelected.Enabled=true;
					btnInsert_Print.Enabled=false;
					//setFocusCtrl(btnUpdate.ClientID);

					
					if(!getRoleOPSSU())
					{
						txtPkgNum.Enabled = false;
					}
					else
						txtPkgNum.Enabled = true;
				}
				else
				{
					txtConNo.Enabled=true;
					//txtConNo.Text =hddConNo.Value;
					txtPkgNum.Text="1";
					btnInsert.Enabled=true;
					txtConNo.ReadOnly=false;
					btnUpdate.Enabled=false;
					btnInsert_Print.Enabled=true;
					btnPrintSelected.Enabled=false;
					//setFocusCtrl(btnInsert.ClientID);
				}
				setFocusCtrl(txtDestPostCode.ClientID);

			}

		
		}
		
		private bool getRoleOPSSU()
		{
			User user = new User();
			user.UserID = userID;
			userRoleArray = com.common.RBAC.RBACManager.GetAllRoles(m_strAppID, m_strEnterpriseID, user);
			Role role;

			for(int i=0;i<userRoleArray.Count;i++)
			{
				role = (Role)userRoleArray[i];				
				if (role.RoleName.ToUpper().Trim() == "OPSSU") return true;
			}
			return false;
		}

		private bool IsAutoPrint(string strAppID, string strEnterprintID, string strLocation)
		{
			bool booAutoPrint = false;
			DbConnection dbCon = null;
			IDbCommand dbCmd = null;
			DataSet DS = null;

			dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterprintID);
			if (dbCon == null)
			{
				Logger.LogTraceError("TIESDAL", "IsAutoPrint", "IsAutoPrint", "dbCon is null");
				throw new ApplicationException("Connection to Database failed", null); 
			}

			StringBuilder strQry = new StringBuilder();
			strQry = strQry.Append(" SELECT CanDirectPrint=ISNULL(( ");
			strQry = strQry.Append(" SELECT CASE WHEN Item IS NULL THEN 0 ELSE 1 END ");
			strQry = strQry.Append(" FROM dbo.Core_System_Code A CROSS APPLY dbo.DelimitedSplit8K(code_text, ',') B ");
			strQry = strQry.Append(" WHERE applicationid = '" + strAppID + "' AND culture = '" + strEnterprintID + "' AND codeid = 'LicensePlateDirectPrint' AND item = '" + strLocation + "'), 0) ");

		
			dbCmd = dbCon.CreateCommand(strQry.ToString(), CommandType.Text);

			try
			{
				DS = (DataSet)dbCon.ExecuteQuery(dbCmd, com.common.DAL.ReturnType.DataSetType);

				foreach (DataRow rw in DS.Tables[0].Rows)
				{
					if (rw[0].ToString() == "1") 
					{
						booAutoPrint = true; 
					}
				}
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("TIESDAL", "PopupPrintLicensePlate", "GetPrintReportLicensePlate", "Error in Query String " + appException.Message);
				throw appException;
			}
			finally
			{
				DS.Dispose();
			}

			return booAutoPrint;

		}

		private bool IsfileExists(string curFile)
		{
			//string curFile = @"c:\temp\test.txt";
			if (System.IO.File.Exists(curFile) == true)
			{
				return true;
			}
			else
			{
				return false;
			}
		}



	}
}
