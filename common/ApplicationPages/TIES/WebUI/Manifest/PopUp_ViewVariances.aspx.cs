using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.common.util;
using com.common.classes;
using com.common.DAL;
using System.Text;
using com.common.applicationpages;
using com.ties.DAL;
using com.common.RBAC;
using com.ties.classes;
using System.IO;


namespace TIES.WebUI.Manifest
{
	/// <summary>
	/// Summary description for PopUp_ViewVariances.
	/// </summary>
	public class PopUp_ViewVariances : BasePopupPage//System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.Button btnClose;
		protected System.Web.UI.WebControls.Label Label1;
		protected System.Web.UI.WebControls.Label lblBatchTypeDisplay;
		protected System.Web.UI.WebControls.Label lblBatchType;
		protected System.Web.UI.WebControls.Label lblBatchIDDisplay;
		protected System.Web.UI.WebControls.Label lblBatchID;
		protected System.Web.UI.WebControls.Label lblTruckIDDisplay;
		protected System.Web.UI.WebControls.Label lblTruckID;
		protected System.Web.UI.WebControls.Label lblBatchDateDisplay;
		protected System.Web.UI.WebControls.Label lblBatchDate;
		protected System.Web.UI.WebControls.Label lblBatchNoDisplay;
		protected System.Web.UI.WebControls.Label lblBatchNumber;
		protected System.Web.UI.WebControls.Button Button1;
		protected System.Web.UI.WebControls.Label lblStatusList;
		protected System.Web.UI.WebControls.Label lblVarianceList;
		protected System.Web.UI.WebControls.RadioButtonList RadioButtonList1;
		protected System.Web.UI.WebControls.DropDownList ddlCurrentLocation;
		protected System.Web.UI.WebControls.Label Header1;
		//private DataView m_dvBatchTypeOptions;
		private String m_strAppID, m_strEnterpriseID, m_strCulture,batch_no;
		protected ArrayList userRoleArray;
		protected System.Web.UI.WebControls.RadioButton rdoShowall;
		protected System.Web.UI.WebControls.RadioButton rdoShippingVariances;
		protected System.Web.UI.WebControls.RadioButton rdoReceiveVariances;
		protected System.Web.UI.WebControls.RadioButton rdoAnyVariances;
		protected System.Web.UI.WebControls.CheckBoxList chkStatus;
		protected System.Web.UI.WebControls.ListBox lbBatch;
		protected System.Web.UI.WebControls.Label Label2;
		protected System.Web.UI.WebControls.Label Label3;
		protected System.Web.UI.WebControls.Label Label4;
		protected System.Web.UI.WebControls.Label Label5;
		protected System.Web.UI.WebControls.Label Label6;
		protected System.Web.UI.WebControls.Label Label7;
		protected System.Web.UI.WebControls.Label Label8;
		protected System.Web.UI.WebControls.Label Label11;
		protected System.Web.UI.WebControls.Label Label12;
		protected System.Web.UI.WebControls.Label Label13;
		protected System.Web.UI.WebControls.Label Label14;
		protected System.Web.UI.WebControls.Label Label15;
		protected System.Web.UI.WebControls.Label Label16;
		protected System.Web.UI.WebControls.Label Label17;
		protected System.Web.UI.WebControls.Button btnShowStatus;
		protected System.Web.UI.WebControls.Label Label9;
		protected System.Web.UI.WebControls.Button btnShowCon;
		protected System.Web.UI.WebControls.Button btnExport;
		//String UserLocation = null;
		String userID = null;
		protected System.Web.UI.WebControls.Label lblErrorMessage;
		private string FileName = "";


		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnShowCon.Click += new System.EventHandler(this.btnShowCon_Click);
			this.lbBatch.SelectedIndexChanged += new System.EventHandler(this.lbBatch_SelectedIndexChanged);
			this.chkStatus.SelectedIndexChanged += new System.EventHandler(this.chkStatus_SelectedIndexChanged);
			this.rdoShowall.CheckedChanged += new System.EventHandler(this.rdoShowall_CheckedChanged);
			this.rdoShippingVariances.CheckedChanged += new System.EventHandler(this.rdoShippingVariances_CheckedChanged);
			this.rdoReceiveVariances.CheckedChanged += new System.EventHandler(this.rdoReceiveVariances_CheckedChanged);
			this.rdoAnyVariances.CheckedChanged += new System.EventHandler(this.rdoAnyVariances_CheckedChanged);
			this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
			this.btnShowStatus.Click += new System.EventHandler(this.btnShowStatus_Click);
			this.btnExport.Click += new System.EventHandler(this.btnExport_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			getSessionTimeOut(true);
			Utility utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings,Page.Session);
			m_strAppID = utility.GetAppID();
			m_strEnterpriseID = utility.GetEnterpriseID();
			m_strCulture = utility.GetUserCulture();
			userID = Session["userID"] + ""; 
			//Session["batch_type"] = "";
			batch_no =  Request.QueryString["BatchNo"] ;
//			if (getRole()=="OPSSU")
//			{
//				getUser();
//				ddlCurrentLocation.Enabled = true; 
//				//EnableControls(false);
//			}
//			else
//				ddlCurrentLocation.Enabled = false;

			if(!Page.IsPostBack)
			{
				GetInformation();
				LoadDestinationDC();

				LoadStatus();
				if(rdoShowall.Checked)
				{
					foreach(ListItem chk in chkStatus.Items)
					{
						chk.Selected=true;
					}
				}
			}
			initialCheckBox();
			this.lbBatch.Attributes.Add("ondblClick", "ShowStatus();");


		}


		private void btnClose_Click(object sender, System.EventArgs e)
		{
			String sScript = "";
			sScript += "<script language=javascript>";
			sScript += "  window.close();";
			sScript += "</script>";
			Response.Write(sScript);
		}

		public void LoadDestinationDC()
		{
			Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings,HttpContext.Current.Session);
			String strAppID = util.GetAppID();
			String strEnterpriseID = util.GetEnterpriseID();

			DataSet dataset = com.ties.classes.DbComboDAL.UserLocationQuery(strAppID,strEnterpriseID);	
			this.ddlCurrentLocation.DataSource = dataset;

			ddlCurrentLocation.DataTextField = dataset.Tables[0].Columns["origin_code"].ToString() ;
			ddlCurrentLocation.DataValueField = dataset.Tables[0].Columns["origin_code"].ToString() ;
			this.ddlCurrentLocation.DataBind();
			ListItem defItem = new ListItem();
			defItem.Text = " ";
			
			defItem.Value = "0";
			ddlCurrentLocation.Items.Insert(0,(defItem));
		}
		private string getRole()
		{
			User user = new User();
			user.UserID = userID;
			userRoleArray = com.common.RBAC.RBACManager.GetAllRoles(m_strAppID, m_strEnterpriseID, user);
			Role role;

			for(int i=0;i<userRoleArray.Count;i++)
			{
				role = (Role)userRoleArray[i];				
				if (role.RoleName.ToUpper().Trim() == "OPSSU") return "OPSSU";
			}
			return " ";
		}
		private void getUser()
		{
			string loc = com.common.RBAC.RBACManager.GetUser(m_strAppID,m_strEnterpriseID, userID).UserLocation.ToString();
			ddlCurrentLocation.SelectedValue = loc; 
		}
		private void GetInformation()
		{
			DataSet con = BatchManifest.GetDataArrivalDeparture( m_strAppID,m_strEnterpriseID,m_strCulture,batch_no);
			if (con.Tables[0].Rows.Count > 0 )
			{
				lblBatchNoDisplay.Text = batch_no;
				lblBatchDateDisplay.Text = String.Format("{0:dd/MM/yyyy}",con.Tables[0].Rows[0]["batch_date"]);  
				lblTruckIDDisplay.Text = con.Tables[0].Rows[0]["truck_id"].ToString(); 
				lblBatchIDDisplay.Text = con.Tables[0].Rows[0]["batch_id"].ToString();
				lblBatchTypeDisplay.Text = con.Tables[0].Rows[0]["code_text"].ToString(); 
				Session["batch_type"] = con.Tables[0].Rows[0]["batch_type"].ToString(); 
				if(con.Tables[0].Rows[0]["code_text"].ToString()=="Current Location")
				{
					rdoReceiveVariances.Enabled=false;
					rdoAnyVariances.Enabled=false;
				}

			}

		}
		private void LoadStatus()
        {
			string batch_typecode="" ;
			if(Session["batch_type"].ToString().ToUpper()=="S")
			{
				batch_typecode="delivery_variance";
			}
			else if(Session["batch_type"].ToString().ToUpper()=="L")
			{
				batch_typecode="linehaul_variance";
			}
			else if(Session["batch_type"].ToString().ToUpper()=="W")
			{
				batch_typecode="currloc_variance";
			}
			else if(Session["batch_type"].ToString().ToUpper()=="A")
			{
				batch_typecode="air_variance";
			}
			
			DataSet ds = BatchManifest.getManifestStatus(m_strAppID,m_strEnterpriseID,batch_typecode,m_strCulture);
			if(ds.Tables[0].Rows.Count>0)
			{
				chkStatus.DataTextField="code_text";
				chkStatus.DataValueField="code_num_value";
				chkStatus.DataSource=ds.Tables[0];
				chkStatus.DataBind();
			}

		}

		private void btnShowStatus_Click(object sender, System.EventArgs e)
		{
			//OpenWindowpage("ShipmentStatusDelete.aspx?FORMID="+strBookNo+"&CONSG_NO="+strConsgNo+"&BOOK_DATE="+strBookDate+"&INFO_INDEX="+e.Item.ItemIndex+" ");
			Session["toRefresh"]=false;
			string strConsgNo = lbBatch.SelectedValue.ToString();
			OpenWindowpage("../ShipmentStatusDelete.aspx?FORMID=ViewVariance&CONSG_NO="+strConsgNo);
		}

		private void rdoShowall_CheckedChanged(object sender, System.EventArgs e)
		{
			if(rdoShowall.Checked)
			{
				foreach(ListItem chk in chkStatus.Items)
				{
					chk.Selected=true;
				}
				chkStatus.Enabled=true;
				rdoShippingVariances.Enabled=true;
				rdoReceiveVariances.Enabled=true;
				rdoAnyVariances.Enabled=true;
				if(lblBatchTypeDisplay.Text.ToString()=="Current Location")
				{
					rdoReceiveVariances.Enabled=false;
					rdoAnyVariances.Enabled=false;
				}
			}
//			else
//			{
//				foreach(ListItem chk in chkStatus.Items)
//				{
//					chk.Selected=false;
//				}
//			}
		}

		private string getStatusList()
		{
			string StatusList ="";
			foreach(ListItem chk in chkStatus.Items)
			{
				if (chk.Selected==true)
				{
					StatusList += chk.Value+"|";
				}
			}
			StatusList =StatusList.TrimEnd('|');
			return StatusList;
		}
		private string getVarianceType()
		{
			string VarianceType ="All";
			if(rdoShowall.Checked)
			{
				VarianceType ="All";
			}
			else if (rdoShippingVariances.Checked)
			{
				VarianceType ="Ship";
			}
			else if (rdoReceiveVariances.Checked)
			{
				VarianceType ="Rec";
			}
			else if (rdoAnyVariances.Checked)
			{
				VarianceType ="Any";
			}
			return VarianceType;

		}
		private void rdoShippingVariances_CheckedChanged(object sender, System.EventArgs e)
		{
			if(rdoShippingVariances.Checked)
			{
				foreach(ListItem chk in chkStatus.Items)
				{
					chk.Selected=true;
				}
			}
			chkStatus.Enabled=false;

		}

		private void rdoReceiveVariances_CheckedChanged(object sender, System.EventArgs e)
		{
			if(rdoReceiveVariances.Checked)
			{
				foreach(ListItem chk in chkStatus.Items)
				{
					chk.Selected=true;
				}
				chkStatus.Enabled=false;


			}
		}

		private void rdoAnyVariances_CheckedChanged(object sender, System.EventArgs e)
		{
			if(rdoAnyVariances.Checked)
			{
				foreach(ListItem chk in chkStatus.Items)
				{
					chk.Selected=true;
				}
			}
			chkStatus.Enabled=false;
		}

		private void initialCheckBox()
		{
			if(rdoShippingVariances.Checked||rdoReceiveVariances.Checked||rdoAnyVariances.Checked)
			{
				foreach(ListItem chk in chkStatus.Items)
				{
					chk.Selected=true;
					chkStatus.Enabled=false;
				}
			}
			
		}

		private void chkStatus_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			foreach(ListItem chk in chkStatus.Items)
			{
				if(chk.Selected==false)
				{
					rdoShippingVariances.Enabled=false;
					rdoReceiveVariances.Enabled=false;
					rdoAnyVariances.Enabled=false;
					return;
				}
				else
				{
					rdoShippingVariances.Enabled=true;
					rdoReceiveVariances.Enabled=true;
					rdoAnyVariances.Enabled=true;
					if(lblBatchTypeDisplay.Text.ToString()=="Current Location")
					{
						rdoReceiveVariances.Enabled=false;
						rdoAnyVariances.Enabled=false;
					}

				}
			}
		}

		private void btnShowCon_Click(object sender, System.EventArgs e)
		{
			Utility utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings,Page.Session);
			DataSet ds = new DataSet();
			ds =BatchManifest.GetManifestDetail(m_strAppID,m_strEnterpriseID,batch_no,getStatusList(),getVarianceType(),ddlCurrentLocation.SelectedValue.ToString(),m_strCulture);
			lbBatch.Items.Clear();
			if(ds.Tables[0].Rows.Count>0)
			{
				foreach(DataRow dr in ds.Tables[0].Rows)
				{
					string data  = dr["Consignment_No"].ToString()+utility.generateHTMLSpace(22-dr["Consignment_No"].ToString().Length);
					data += dr["Origin_DC"].ToString()+utility.generateHTMLSpace(7- dr["Origin_DC"].ToString().Length);
					data += dr["Dest_DC"].ToString()+utility.generateHTMLSpace(3- dr["Dest_DC"].ToString().Length);
					data += utility.generateHTMLSpace(7-dr["Cons_Tot_Pkgs"].ToString().Length)+dr["Cons_Tot_Pkgs"].ToString();//
					data += utility.generateHTMLSpace(12-dr["Cons_Tot_Wt"].ToString().Length)+dr["Cons_Tot_Wt"].ToString();
					data += utility.generateHTMLSpace(6- dr["SOP_PKG"].ToString().Length)+dr["SOP_PKG"].ToString();
					data += utility.generateHTMLSpace(7- dr["SOP_VAR"].ToString().Length)+dr["SOP_VAR"].ToString();
					data += utility.generateHTMLSpace(5-dr["SIP_PKG"].ToString().Length)+dr["SIP_PKG"].ToString();
					data += utility.generateHTMLSpace(6-dr["SIP_Var"].ToString().Length)+dr["SIP_Var"].ToString();
					data += utility.generateHTMLSpace(8)+dr["xStatus"].ToString()+utility.generateHTMLSpace(15-dr["xStatus"].ToString().Length);
					data += dr["Last_Status_code"].ToString();//+utility.generateHTMLSpace(8-dr["Cons_Tot_Wt"].ToString().Length);

					ListItem lData = new ListItem(data,dr["Consignment_No"].ToString());
					lbBatch.Items.Insert(lbBatch.Items.Count,lData);

				}

			}
			btnShowStatus.Enabled=false;
		}

		private void lbBatch_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if(lbBatch.SelectedIndex>-1)
			{
				btnShowStatus.Enabled=true;
			}
			else
			{
				btnShowStatus.Enabled=false;
			}
		}
		private void OpenWindowpage(String strUrl)
		{
			ArrayList paramList = new ArrayList();
			paramList.Add(strUrl);
			String sScript = Utility.GetScript("openLargerChildParam.js",paramList);
			Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);

		}

		
		///boon - start 2012-02-15
		private void ExportData(DataTable dtSource)
		{
			DataTable dtCons = new DataTable();
			string Res = "";			
 
			try
			{
				dtCons = dtSource;

				if ((dtCons != null) && (dtCons.Columns.Count > 0) && (dtCons.Rows.Count > 0))
				{
					TIESClasses.CreateXls Xls = new TIESClasses.CreateXls();

					// Set type column
					string[] ColumnType = new string[dtCons.Columns.Count];
					ColumnType[1] = Xls.TypeText; 

					Res = Xls.createXlsConsign(dtCons, GetPathExport(), ColumnType);

					if (Res == "0")
					{	
						GetDownloadFile(Res);
				
						// Kill Process after save
						Xls.KillProcesses("EXCEL");
					} 
					else
					{
						lblErrorMessage.Text = Res;
					}
				}
			}
			catch(Exception ex)
			{
				lblErrorMessage.Text = ex.Message.ToString() ;
			}
			finally
			{
				dtCons.Dispose();
				dtSource.Dispose();
			}
		}

		
		private string  GetPathExport()
		{
			FileName = Session["userID"].ToString().ToUpper() + "_ViewVariances_" + String.Format("{0:yyyyMMdd}", DateTime.Now);
			string mapFile = Server.MapPath("..\\Export") + "\\" + FileName + ".xls";
			return mapFile;
		}


		private void GetDownloadFile(string Res)
		{
			if (Res == "0")
			{
				FileName = Session["userID"].ToString().ToUpper() + "_ViewVariances_" + String.Format("{0:yyyyMMdd}", DateTime.Now);
				Response.ContentType="application/ms-excel";
				Response.AddHeader( "content-disposition","attachment; filename=" + FileName + ".xls");
				FileStream fs = new FileStream(GetPathExport(),FileMode.Open);
				long FileSize;
				FileSize = fs.Length;
				byte[] getContent = new byte[(int)FileSize];
				fs.Read(getContent, 0, (int)fs.Length);
				fs.Close();

				Response.BinaryWrite(getContent);
			} 
		}

		
		private void btnExport_Click(object sender, System.EventArgs e)
		{
			DataSet ds = new DataSet();
			ds =BatchManifest.GetManifestDetail(m_strAppID,m_strEnterpriseID,batch_no,getStatusList(),getVarianceType(),ddlCurrentLocation.SelectedValue.ToString(),m_strCulture);
			
			DataTable dtExport = new DataTable();
	
			dtExport.Columns.Add("Consignment_No");
			dtExport.Columns.Add("Origin_DC");
			dtExport.Columns.Add("Dest_DC");
			dtExport.Columns.Add("Cons_Tot_Pkgs");
			dtExport.Columns.Add("Cons_Tot_Wt");
			dtExport.Columns.Add("SOP_PKG");
			dtExport.Columns.Add("SOP_VAR");
			dtExport.Columns.Add("SIP_PKG");
			dtExport.Columns.Add("SIP_Var");
			dtExport.Columns.Add("Status");
			dtExport.Columns.Add("Last_Status_code");

			if (ds != null && ds.Tables.Count > 0 && ds.Tables[0] != null && ds.Tables[0].Rows.Count > 0)
			{
				for (int i = 0; i < ds.Tables[0].Rows.Count; i++) 
				{
					DataRow dr;
					dr = dtExport.NewRow();

					dr["Consignment_No"] = ds.Tables[0].Rows[i]["Consignment_No"];
					dr["Origin_DC"] = ds.Tables[0].Rows[i]["Origin_DC"];
					dr["Dest_DC"] = ds.Tables[0].Rows[i]["Dest_DC"];
					dr["Cons_Tot_Pkgs"] = ds.Tables[0].Rows[i]["Cons_Tot_Pkgs"];
					dr["Cons_Tot_Wt"] = ds.Tables[0].Rows[i]["Cons_Tot_Wt"];
					dr["SOP_PKG"] = ds.Tables[0].Rows[i]["SOP_PKG"];
					dr["SOP_VAR"] = ds.Tables[0].Rows[i]["SOP_VAR"];
					dr["SIP_PKG"] = ds.Tables[0].Rows[i]["SIP_PKG"];
					dr["SIP_Var"] = ds.Tables[0].Rows[i]["SIP_Var"];
					dr["Status"] = ds.Tables[0].Rows[i]["xStatus"];
					dr["Last_Status_code"] = ds.Tables[0].Rows[i]["Last_Status_code"];
					
					dtExport.Rows.Add(dr);
				}
			}
			else
			{
				DataRow dr;
				dr = dtExport.NewRow();

				dr["Consignment_No"] = " ";
				dr["Origin_DC"] = " ";
				dr["Dest_DC"] = " ";
				dr["Cons_Tot_Pkgs"] = " ";
				dr["Cons_Tot_Wt"] = " ";
				dr["SOP_PKG"] = " ";
				dr["SOP_VAR"] = " ";
				dr["SIP_PKG"] = " ";
				dr["SIP_Var"] = " ";
				dr["Status"] = " ";
				dr["Last_Status_code"] = " ";
					
				dtExport.Rows.Add(dr);
			}

			ExportData(dtExport);
		}	
		///boon - end   2012-02-15
	}
}
