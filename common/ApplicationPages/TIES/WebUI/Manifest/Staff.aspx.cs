using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.common.util;
using com.common.classes;
using com.common.DAL;
using System.Text;
using com.common.applicationpages;

namespace TIES.WebUI
{
	/// <summary>
	/// Summary description for Staff.
	/// </summary>
	public class Staff : BasePopupPage// System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.DataGrid dgStaff;
		protected System.Web.UI.WebControls.Label lblDriverID;
		protected System.Web.UI.WebControls.Label lblName;
		protected System.Web.UI.WebControls.Label lblLocation;
		protected System.Web.UI.WebControls.Button btnSearch;
		protected System.Web.UI.WebControls.Button btnClose;
		protected System.Web.UI.WebControls.TextBox txtName;
		protected System.Web.UI.WebControls.TextBox txtdriverid;
		protected System.Web.UI.WebControls.DropDownList ddlLocation;
		protected System.Web.UI.WebControls.Label lblMainTitle;

		SessionDS m_sdsStaffCode = null;
		String strAppID = "";
		String strEnterpriseID = "";
		private String strStaffCode = null;
	
		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			getSessionTimeOut(true);
			Utility utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings,Page.Session);
			strAppID = utility.GetAppID();
			strEnterpriseID = utility.GetEnterpriseID();
			//m_strCulture = utility.GetUserCulture();
			if(!Page.IsPostBack)
			{
				txtdriverid.Text = Request.Params["Emp_ID"];
				txtName.Text = Request.Params["Emp_Name"];		
				LoadUserLocation();				
				m_sdsStaffCode = GetEmptyStaffCodeDS(0);
				BindGrid();
			}
			else
			{
				//m_sdsStatusCode = (SessionDS)ViewState["STATUS_DS"];
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.dgStaff.PageIndexChanged += new System.Web.UI.WebControls.DataGridPageChangedEventHandler(this.dgStaff_PageIndexChanged);
			this.dgStaff.SelectedIndexChanged += new System.EventHandler(this.dgStaff_SelectedIndexChanged);
			this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
			this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private void btnSearch_Click(object sender, System.EventArgs e)
		{
			StringBuilder strQry = new StringBuilder();
			strQry.Append("select Emp_ID, Emp_Name, Base_Location from mf_staff where applicationid='");
			strQry.Append(strAppID);
			strQry.Append("' and enterpriseid = '");
			strQry.Append(strEnterpriseID);
			strQry.Append("' and Emp_Type = '");
			strQry.Append("D");
			strQry.Append("' and ((convert(varchar(8),getdate(),112) between convert(varchar(8),isnull(Initial_in_service_date,''),112) "+
							" and convert(varchar(8),isnull(Separation_Date,getdate()),112) and Out_Of_Service_Date is null and In_Service_Date is null) "+
							" or (convert(varchar(8),getdate(),112) between convert(varchar(8),isnull(Initial_in_service_date,''),112) "+
							" and convert(varchar(8),isnull(Out_Of_Service_Date,''),112) and Out_Of_Service_Date is not null and In_Service_Date is null) "+
							" or (convert(varchar(8),getdate(),112) between convert(varchar(8),isnull(In_Service_Date,''),112) "+
							" and convert(varchar(8),isnull(Separation_Date,getdate()),112) and Out_Of_Service_Date is not null and In_Service_Date is not null ))");
			//strQry.Append("'");
			if (txtdriverid.Text.ToString() != null && txtdriverid.Text.ToString() != "")
			{
				strQry.Append(" and Emp_ID like '%");
				strQry.Append(Utility.ReplaceSingleQuote(txtdriverid.Text.ToString()));
				strQry.Append("%'");
			}
			if (txtName.Text.ToString() != null && txtName.Text.ToString() != "")
			{
				strQry.Append(" and Emp_Name like '%");
				strQry.Append(Utility.ReplaceSingleQuote(txtName.Text.ToString()));
				strQry.Append("%'");
			}
			if(ddlLocation.SelectedItem.Value!="")
			{
				strQry.Append(" and Base_Location = '");
				strQry.Append(ddlLocation.SelectedItem.Value+"'");
			}

			String strSQLQuery = strQry.ToString();
			ViewState["SQL_QUERY"] = strSQLQuery;
			dgStaff.CurrentPageIndex = 0;
			ViewState["STATUS_DS"] = m_sdsStaffCode;
			ShowCurrentPage();
			//BindGrid();
		}
		private void ShowCurrentPage()
		{
			int iStartIndex = dgStaff.CurrentPageIndex * dgStaff.PageSize;
			String sqlQuery = (String)ViewState["SQL_QUERY"];
			m_sdsStaffCode = GetStaffCodeDS(iStartIndex,dgStaff.PageSize,sqlQuery);
			decimal pgCnt = (m_sdsStaffCode.QueryResultMaxSize - 1)/dgStaff.PageSize;
			if(pgCnt < dgStaff.CurrentPageIndex)
			{
				dgStaff.CurrentPageIndex = System.Convert.ToInt32(pgCnt);
			}
			dgStaff.SelectedIndex = -1;
			dgStaff.EditItemIndex = -1;

			BindGrid();
			//getPageControls(Page);
		}
		public static SessionDS GetEmptyStaffCodeDS(int intRows)
		{
			DataTable dtStaff = new DataTable();
			dtStaff.Columns.Add(new DataColumn("emp_id", typeof(string)));
			dtStaff.Columns.Add(new DataColumn("emp_name", typeof(string)));
			dtStaff.Columns.Add(new DataColumn("base_location", typeof(string)));

			for(int i=0; i < intRows; i++)
			{
				DataRow drEach = dtStaff.NewRow();
				drEach[0] = "";
				drEach[1] = "";
				drEach[2] = "";

				dtStaff.Rows.Add(drEach);
			}

			DataSet dsStaffFields = new DataSet();
			dsStaffFields.Tables.Add(dtStaff);
			SessionDS sessionDS = new SessionDS();
			sessionDS.ds = dsStaffFields;
			sessionDS.DataSetRecSize = dsStaffFields.Tables[0].Rows.Count;
			sessionDS.QueryResultMaxSize = sessionDS.DataSetRecSize;
			return sessionDS;
		}
		private SessionDS GetStaffCodeDS(int iCurrent, int iDSRecSize, String strQuery)
		{
			SessionDS sessionDS = null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("Staff.aspx.cs","GetStaffCodeDS","EDB101","DbConnection object is null!!");
				return sessionDS;
			}
			sessionDS = dbCon.ExecuteQuery(strQuery,iCurrent,iDSRecSize,"CountryStateCode");
			return  sessionDS;
		}
		private void BindGrid()
		{
			dgStaff.VirtualItemCount = System.Convert.ToInt32(m_sdsStaffCode.QueryResultMaxSize);
			dgStaff.DataSource = m_sdsStaffCode.ds;
			dgStaff.DataBind();
			ViewState["STAFFCODE_DS"] = m_sdsStaffCode;
		}

		public void LoadUserLocation()
		{
			Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings,HttpContext.Current.Session);
			String strAppID = util.GetAppID();
			String strEnterpriseID = util.GetEnterpriseID();

			DataSet dataset = com.ties.classes.DbComboDAL.UserLocationQuery(strAppID,strEnterpriseID);	
			this.ddlLocation.DataSource = dataset;

			ddlLocation.DataTextField = dataset.Tables[0].Columns["origin_code"].ToString() ;
			ddlLocation.DataValueField = dataset.Tables[0].Columns["origin_code"].ToString() ;
			this.ddlLocation.DataBind();
			ListItem defItem = new ListItem();
			defItem.Text = "";
			
			defItem.Value = "";
			ddlLocation.Items.Insert(0,(defItem));

		}
		private void btnClose_Click(object sender, System.EventArgs e)
		{
			String sScript = "";
			sScript += "<script language=javascript>";
			sScript += "  window.close();";
			sScript += "</script>";
			Response.Write(sScript);
		}
		

		private void dgStaff_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			String strFormID = Request.Params["FORMID"];
			int iSelIndex = dgStaff.SelectedIndex;
			DataGridItem dgRow = dgStaff.Items[iSelIndex];
			strStaffCode = dgRow.Cells[0].Text;
			string strStaffName =dgRow.Cells[1].Text;
			String strAirDriver = Request.Params["AirDriver"];
			if (strAirDriver != null)
			{
				windowClose(strFormID, "ddlAirDriverID",strStaffCode);
			}
			else
			{
				windowClose(strFormID, "ddlDriverID",strStaffCode);
			}
			
		}
		private void windowClose(String formID, String strDriverIDddl, String strExceptTextBoxID)
		{
			String sScript = "";
			sScript += "<script language=javascript>";
			if(formID.Equals("ShipmentUpdate") || (formID.Equals("ShipmentUpdateRoute")))
			{
				sScript += "  window.opener."+formID+".txtStatusCode.value =\""+strStaffCode+"\";" ;				
			}
			else if(formID.Equals("ShipmentTrackingQuery"))
			{
				sScript += "  window.opener."+formID+".txtStatusCode.value =\""+strStaffCode+"\";" ;				
			}
			else if(formID.Equals("ShipmentTrackingQueryRpt"))
			{
				sScript += "  window.opener."+formID+".txtStatusCode.value =\""+strStaffCode+"\";" ;				
			}
			else if(formID.Equals("ShipmentExceptionReport"))
			{
				sScript += "  window.opener."+formID+".txtStatus.value = \""+strStaffCode+"\";" ;				
			}
			else
			{
				sScript += "  window.opener."+formID+"."+strDriverIDddl+".value = \""+strStaffCode+"\";" ;				
			}
			sScript += "  window.close();";
			sScript += "</script>";
			Response.Write(sScript);
		}

		private void dgStaff_PageIndexChanged(object source, System.Web.UI.WebControls.DataGridPageChangedEventArgs e)
		{
			dgStaff.CurrentPageIndex = e.NewPageIndex;
			ShowCurrentPage();
			dgStaff.SelectedIndex = -1;
			dgStaff.EditItemIndex = -1;
			BindGrid();
		}
	}
}
