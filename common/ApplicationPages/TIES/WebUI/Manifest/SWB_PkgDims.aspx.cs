using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.common.util;
using com.common.classes;
using com.common.DAL;
using System.Text;
using com.common.applicationpages;
using com.ties.DAL;
using com.common.RBAC;
using com.ties.classes;
using System.Configuration;
using TIESDAL;

namespace TIES.WebUI.Manifest
{
	/// <summary>
	/// Summary description for SWB_PkgDims.
	/// </summary>
	public class SWB_PkgDims : BasePage//System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.Label lblErrrMsg;
		protected System.Web.UI.WebControls.Label lblPackageWeightsAndDims;
		protected System.Web.UI.WebControls.Button btnInsert;
		protected System.Web.UI.WebControls.Button btnUpdate;
		protected System.Web.UI.WebControls.Button btnDelete;
		protected System.Web.UI.WebControls.Button btnQuery;
		protected System.Web.UI.WebControls.Button btnExcuteQuery;
		protected System.Web.UI.WebControls.TextBox txtConNo;
		protected System.Web.UI.WebControls.Label lblConNo;
		protected System.Web.UI.WebControls.Label lblUser;
		protected System.Web.UI.WebControls.Label lblUserDisplay;
		protected System.Web.UI.WebControls.Label lblLocation;
		protected System.Web.UI.WebControls.Label lblLocationDisplay;
		protected System.Web.UI.WebControls.Label lblConsignmentCount;
		protected System.Web.UI.WebControls.Label lblConsignmentCountDisplay;
		protected System.Web.UI.WebControls.Label lblPackageDetailsCount;
		protected System.Web.UI.WebControls.Label lblPackageDetailsCountDisplay;
		protected System.Web.UI.WebControls.Label lblCurrentDateTime;
		protected System.Web.UI.WebControls.Label lblCurrentDateTimeDisplay;
		protected System.Web.UI.WebControls.ListBox lbSIPCon;
		protected System.Web.UI.WebControls.ListBox lbSIPConDetail;
		protected System.Web.UI.WebControls.Label lblMPSNum;
		protected System.Web.UI.WebControls.Label lblLength;
		protected System.Web.UI.WebControls.Label lblQuantity;
		protected System.Web.UI.WebControls.Label lblBreadth;
		protected System.Web.UI.WebControls.Label lblHeight;
		protected com.common.util.msTextBox txtMPSNo;
		protected com.common.util.msTextBox txtLength;
		protected com.common.util.msTextBox txtBreadth;
		protected com.common.util.msTextBox txtHeight;
		protected com.common.util.msTextBox txtQuantity;
		protected com.common.util.msTextBox txtWeight;
		protected System.Web.UI.WebControls.ValidationSummary reqSummary;
		protected System.Web.UI.WebControls.Label lblColServiceType;
		protected System.Web.UI.WebControls.Label lblColDestPostCode;
		protected System.Web.UI.WebControls.Label lblColPkgs;
		protected System.Web.UI.WebControls.Label lblColSIPDateTime;
		protected System.Web.UI.WebControls.Label lblWeight;
		protected System.Web.UI.WebControls.Label lblColConsignment;
		protected System.Web.UI.WebControls.Label lblColConsignment02;
		protected System.Web.UI.WebControls.Label lblColMPSNo;
		protected System.Web.UI.WebControls.Label lblColQty;
		protected System.Web.UI.WebControls.Label lblColWeight;
		protected System.Web.UI.WebControls.Label lblColLength;
		protected System.Web.UI.WebControls.Label lblColBreadth;
		protected System.Web.UI.WebControls.Label lblColHeight;
		protected System.Web.UI.WebControls.Label Header1;
		protected ArrayList userRoleArray;
		String userID = null;
//		String ConNo = null;
		private String m_strAppID, m_strEnterpriseID, m_strCulture;
		public int CharBarcodeCutStartIndex = Convert.ToInt32(ConfigurationSettings.AppSettings["32CharBarcodeCutStartIndex"]);
		protected System.Web.UI.HtmlControls.HtmlInputButton hddConNo;
		protected System.Web.UI.WebControls.CheckBox chkCWE;
		public int CharBarcodeCutLength= Convert.ToInt32(ConfigurationSettings.AppSettings["32CharBarcodeCutLength"]);

		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			getSessionTimeOut(true);
			txtConNo.Attributes.Add("onkeyDown", "KeyDownExe();");
			txtConNo.Attributes.Add("onpaste", "return true;");
			txtMPSNo.Attributes.Add("onkeyDown", "KeyDownHandler();");
			txtMPSNo.Attributes.Add("onpaste", "return false;");
			txtQuantity.Attributes.Add("onkeyDown", "KeyDownHandler();");
			txtQuantity.Attributes.Add("onpaste", "return false;");
			txtWeight.Attributes.Add("onkeyDown", "KeyDownHandler();");
			txtWeight.Attributes.Add("onpaste", "return false;");
			txtLength.Attributes.Add("onkeyDown", "KeyDownHandler();");
			txtLength.Attributes.Add("onpaste", "return false;");
			txtBreadth.Attributes.Add("onkeyDown", "KeyDownHandler();");
			txtBreadth.Attributes.Add("onpaste", "return false;");
			txtHeight.Attributes.Add("onkeyDown", "KeyDownHandler();");
			txtHeight.Attributes.Add("onpaste", "return false;");

			Utility utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings,Page.Session);
			m_strAppID = utility.GetAppID();
			m_strEnterpriseID = utility.GetEnterpriseID();
			m_strCulture = utility.GetUserCulture();
			userID = Session["userID"] + ""; 

			if (!Page.IsPostBack)
			{
				SetInitialFocus(txtConNo);
				SetDefault();
				getUserAndLocation();

//				if(!getRoleOPSSU())
//					chkCWE.Visible = false;
//				else
//					chkCWE.Visible = true;

			}
//			else
//			{
//				txtConNo.Attributes.Add("onkeyDown", "KeyDownExe();");
//				txtMPSNo.Attributes.Add("onkeyDown", "KeyDownHandler();");
//				txtQuantity.Attributes.Add("onkeyDown", "KeyDownHandler();");
//				txtWeight.Attributes.Add("onkeyDown", "KeyDownHandler();");
//				txtLength.Attributes.Add("onkeyDown", "KeyDownHandler();");
//				txtBreadth.Attributes.Add("onkeyDown", "KeyDownHandler();");
//				txtHeight.Attributes.Add("onkeyDown", "KeyDownHandler();");
//			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnInsert.Click += new System.EventHandler(this.btnInsert_Click);
			this.txtQuantity.TextChanged += new System.EventHandler(this.txtQuantity_TextChanged);
			this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
			this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
			this.chkCWE.CheckedChanged += new System.EventHandler(this.chkCWE_CheckedChanged);
			this.lbSIPCon.SelectedIndexChanged += new System.EventHandler(this.lbSIPCon_SelectedIndexChanged);
			this.lbSIPConDetail.SelectedIndexChanged += new System.EventHandler(this.lbSIPConDetail_SelectedIndexChanged);
			this.btnQuery.Click += new System.EventHandler(this.btnQuery_Click);
			this.btnExcuteQuery.Click += new System.EventHandler(this.btnExcuteQuery_Click);
			this.hddConNo.ServerClick += new System.EventHandler(this.hddConNo_ServerClick);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
		
		public static void SetInitialFocus(Control control) 
		{ 
			if (control.Page == null) 
			{ 
				throw new ArgumentException( 
					"The Control must be added to a Page before you can set the IntialFocus to it."); 
			} 
			if (control.Page.Request.Browser.JavaScript == true) 
			{ 
				// Create JavaScript 
				StringBuilder s = new StringBuilder(); 
				s.Append("\n<SCRIPT LANGUAGE='JavaScript'>\n"); 
				s.Append("<!--\n"); 
				s.Append("function SetInitialFocus()\n"); 
				s.Append("{\n"); 
				s.Append("   document."); 

				// Find the Form 
				Control p = control.Parent; 
				while (!(p is System.Web.UI.HtmlControls.HtmlForm)) 
					p = p.Parent; 
				s.Append(p.ClientID); 

				s.Append("['"); 
				s.Append(control.UniqueID); 
				s.Append("'].focus();\n"); 
				s.Append("}\n"); 
				s.Append("// -->\n"); 
				s.Append("window.onload = SetInitialFocus;\n"); 
				s.Append("</SCRIPT>"); 

				// Register Client Script 
				control.Page.RegisterClientScriptBlock("InitialFocus", s.ToString()); 
			} 
		} 

		private void SetDefault()
		{
			SetInitialFocus(txtConNo);
			lblConsignmentCountDisplay.Text ="0";
			lblPackageDetailsCountDisplay.Text ="0";
			lblCurrentDateTimeDisplay.Text = DateTime.Now.ToString("dd/MM/yyyy HH:mm") ;  

			btnInsert.Enabled =false;
			btnUpdate.Enabled =false;
			btnDelete.Enabled =false; 
		}
		private void btnQuery_Click(object sender, System.EventArgs e)
		{
			txtConNo.Text = "";
			lblErrrMsg.Text =""; 
			btnExcuteQuery.Enabled =true; 
			ClearControlDisplayDetail();
			lbSIPConDetail.Items.Clear();  
			SetDefault();
			//txtConNo.ReadOnly = false;
			//lbSIPCon.Items.Clear();  
			
//			chkCWE.Checked = false;
		}
		private void getUserAndLocation()
		{
			com.common.classes.User user =  RBACManager.GetUser(m_strAppID,m_strEnterpriseID, userID);
			lblUserDisplay.Text  = user.UserID.ToString()  ; 
			lblLocationDisplay.Text  = user.UserLocation.ToString()  ; 
		}


		private void generateDataToListbox()
		{
			Utility utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings,Page.Session);
			DataSet con = SWB_Emulator.SWB_SIP_Read( m_strAppID,m_strEnterpriseID,txtConNo.Text );
			string data ="";
			if (con.Tables[0].Rows.Count >0)
			{
				data  = con.Tables[0].Rows[0]["consignment_no"].ToString();
				data += utility.generateHTMLSpace(25-(con.Tables[0].Rows[0]["consignment_no"].ToString()).Length);
				data += con.Tables[0].Rows[0]["service_code"].ToString();
				data += utility.generateHTMLSpace(10-(con.Tables[0].Rows[0]["service_code"].ToString()).Length);
				data += con.Tables[0].Rows[0]["dest_postal_code"].ToString();
				data += utility.generateHTMLSpace(8-(con.Tables[0].Rows[0]["dest_postal_code"].ToString()).Length);
				data += con.Tables[0].Rows[0]["number_of_package"].ToString();
				data += utility.generateHTMLSpace(5-(con.Tables[0].Rows[0]["number_of_package"].ToString()).Length);
				data += String.Format("{0:dd/MM/yyyy HH:mm}",con.Tables[0].Rows[0]["scanned_in_datetime"]);
				//Session["numpkg"] = 5-(con.Tables[0].Rows[0]["number_of_package"].ToString()).Length;
				ListItem li = new ListItem(data);
				
				try 
				{
					lbSIPCon.Items.RemoveAt(lbSIPCon.Items.IndexOf(li));
				}
				catch(Exception e)
				{}


				lbSIPCon.Items.Insert(lbSIPCon.Items.Count,data);

				
				ViewState["NumOfPKG"] = con.Tables[0].Rows[0]["number_of_package"].ToString();
			}
			else
			{
				lblErrrMsg.Text ="No matches found.";
			}
			lblConsignmentCountDisplay.Text =lbSIPCon.Items.Count.ToString();
			lblCurrentDateTimeDisplay.Text = DateTime.Now.ToString("dd/MM/yyyy HH:mm");
  			
			int idx =lbSIPCon.Items.Count-1;

			string script="<script langauge='javacript'>";
			script+= "document.all['"+ lbSIPCon.ClientID +"'].selectedIndex="+ idx.ToString() +";";
			script+= "__doPostBack('"+ lbSIPCon.ClientID +"','');";
			script+= "</script>";
			Page.RegisterStartupScript("onclick",script);

		}

		private void generateDataToListboxPackageDetail(string ConNo)
		{
			Utility utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings,Page.Session);
			DataSet con = SWB_Emulator.SWB_Pkg_Read( m_strAppID,m_strEnterpriseID,ConNo );
			DataSet conMPS = SWB_Emulator.SWB_GetMPSNo(m_strAppID,m_strEnterpriseID,ConNo );

			string data ="";
			int qty = 0;
			if (con.Tables[0].Rows.Count >0)
			{
				for(int i=0; i < con.Tables[0].Rows.Count; i++)
				{
					data  = con.Tables[0].Rows[i]["consignment_no"].ToString();
					data += utility.generateHTMLSpace(25-(con.Tables[0].Rows[i]["consignment_no"].ToString()).Length);
					data += con.Tables[0].Rows[i]["mps_no"].ToString();
					data += utility.generateHTMLSpace(6-(con.Tables[0].Rows[i]["mps_no"].ToString()).Length);
					data += con.Tables[0].Rows[i]["quantity"].ToString();
					data += utility.generateHTMLSpace(4-(con.Tables[0].Rows[i]["quantity"].ToString()).Length);
					data += con.Tables[0].Rows[i]["weight"].ToString();
					data += utility.generateHTMLSpace(8-(con.Tables[0].Rows[i]["weight"].ToString()).Length);
					data += con.Tables[0].Rows[i]["length"].ToString();
					data += utility.generateHTMLSpace(8-(con.Tables[0].Rows[i]["length"].ToString()).Length);
					data += con.Tables[0].Rows[i]["breadth"].ToString();
					data += utility.generateHTMLSpace(7-(con.Tables[0].Rows[i]["breadth"].ToString()).Length);
					data += con.Tables[0].Rows[i]["height"].ToString();
					//lbSIPConDetail.Items.Insert(lbSIPConDetail.Items.Count,data);
					ListItem li = new ListItem(data);
					if (!lbSIPConDetail.Items.Contains(li))
						lbSIPConDetail.Items.Insert(lbSIPConDetail.Items.Count,data);

					qty += Convert.ToInt32(con.Tables[0].Rows[i]["quantity"].ToString());
					ViewState["dtlQTY"] = qty;
				}
				lblPackageDetailsCountDisplay.Text =Convert.ToString(qty);

				//int numOfPKG =0;
				if(Session["Pkgs"] != null)
				{
					if(Convert.ToInt32(Session["Pkgs"].ToString())>qty)
					{
						btnInsert.Enabled=true;

						EnterpriseConfigurations();

						txtMPSNo.Text = conMPS.Tables[0].Rows[0]["mps_no"].ToString();   
						// by Tumz
						txtWeight.Text = "";
						txtQuantity.Text = "1";
						txtLength.Text = "1";
						txtBreadth.Text = "1";
						txtHeight.Text = "1";
					}
					else if (Convert.ToInt32(Session["Pkgs"].ToString())==qty)
					{
						//SetInitialFocus(txtConNo);
						btnInsert.Enabled=false;
						
					}
					else
					{
						btnInsert.Enabled=false;
					}
				}

//				int idx = 0;
//				string script="<script langauge='javacript'>";
//				script+= "document.all['"+ lbSIPConDetail.ClientID +"'].selectedIndex="+ idx.ToString() +";";
//				script+= "__doPostBack('"+ lbSIPConDetail.ClientID +"','');";
//				script+= "</script>";
//				Page.RegisterStartupScript("onclick",script);
			}
			else
			{
				lblPackageDetailsCountDisplay.Text ="0";
				
				EnterpriseConfigurations();

				txtMPSNo.Text = conMPS.Tables[0].Rows[0]["mps_no"].ToString(); 
				txtWeight.Text = "";
				txtQuantity.Text = "1";
				txtLength.Text = "1";
				txtBreadth.Text = "1";
				txtHeight.Text = "1";

				txtMPSNo.ReadOnly =false;
				btnInsert.Enabled =true; 
				btnUpdate.Enabled =false;
				btnDelete.Enabled =false;
			}	
			

			//int idx =lbSIPConDetail.Items.Count-1;
			

//			if (con.Tables[0].Rows.Count == 0)
//			{
//				SetInitialFocus(txtMPSNo);
//				txtMPSNo.ReadOnly =false;
//				btnInsert.Enabled =true; 
//				btnUpdate.Enabled =false;
//				btnDelete.Enabled =false;
//			}
//			else 
//			{
//				if (Convert.ToInt32(Session["Pkgs"].ToString() ) <= Convert.ToInt32(lblPackageDetailsCountDisplay.Text))
//				{
//					txtMPSNo.ReadOnly =true;
////					btnInsert.Enabled =false; 
////					btnUpdate.Enabled =true;
////					btnDelete.Enabled =true;
//				}
//				else
//				{
//					SetInitialFocus(txtMPSNo);
//					txtMPSNo.ReadOnly =false;
//					btnInsert.Enabled =true; 
//					btnUpdate.Enabled =false;
//					btnDelete.Enabled =false;
//				}
//			}

//			int idx = 1;
//			string script="<script langauge='javacript'>";
//			script+= "document.all['"+ lbSIPConDetail.ClientID +"'].selectedIndex="+ idx.ToString() +";";
//			script+= "__doPostBack('"+ lbSIPConDetail.ClientID +"','');";
//			script+= "</script>";
//			Page.RegisterStartupScript("onclick",script);
			
		}

		private void DisplayDetail(string ConNo)
		{
			DataSet con = SWB_Emulator.SWB_Pkg_Read( m_strAppID,m_strEnterpriseID,ConNo );
			if (con.Tables[0].Rows.Count >0)
			{
				EnterpriseConfigurations();
				
				DataSet profileDS = SysDataManager1.GetEnterpriseProfile(m_strAppID, m_strEnterpriseID);
				int rounding = Convert.ToInt32(profileDS.Tables[0].Rows[0]["currency_decimal"]);
				string rd = "";

				for(int j=0;j<rounding;j++)
				{
					if(j==0)
						rd = "#.";

					rd = rd + "0";
				}


				int i = 0;
				i = lbSIPConDetail.SelectedIndex ;
				txtMPSNo.Text =con.Tables[0].Rows[i]["mps_no"].ToString();
				txtQuantity.Text = con.Tables[0].Rows[i]["quantity"].ToString();
//				txtLength.Text = con.Tables[0].Rows[i]["quantity"].ToString();
//				txtBreadth.Text = con.Tables[0].Rows[i]["quantity"].ToString();
//				txtHeight.Text = con.Tables[0].Rows[i]["quantity"].ToString();

				if (con.Tables[0].Rows[i]["weight"].ToString() =="0")
					txtWeight.Text ="";
				else
				{
					if(con.Tables[0].Rows[i]["weight"].ToString() != "")
						txtWeight.Text = Convert.ToDecimal(con.Tables[0].Rows[i]["weight"]).ToString(rd);
				}

				if (con.Tables[0].Rows[i]["length"].ToString() =="0")
					txtLength.Text ="";
				else
					txtLength.Text = con.Tables[0].Rows[i]["length"].ToString();

				if (con.Tables[0].Rows[i]["breadth"].ToString() =="0")
					txtBreadth.Text ="";
				else
					txtBreadth.Text = con.Tables[0].Rows[i]["breadth"].ToString();

				if (con.Tables[0].Rows[i]["height"].ToString() =="0")
					txtHeight.Text ="";
				else 
					txtHeight.Text = con.Tables[0].Rows[i]["height"].ToString();

				ViewState["chkDtlQTY"] = con.Tables[0].Rows[i]["quantity"].ToString();
			}
			else
			{
				txtMPSNo.Text ="";
				txtQuantity.Text ="";
				txtWeight.Text ="";
				txtLength.Text ="";
				txtBreadth.Text ="";
				txtHeight.Text ="";
			}
		}
		private void DisplayDetailScan(string ConNo)
		{
			DataSet con = SWB_Emulator.SWB_Pkg_Read( m_strAppID,m_strEnterpriseID,ConNo );
			if (con.Tables[0].Rows.Count >0)
			{
				SetInitialFocus(txtWeight);

				
				DataSet profileDS = SysDataManager1.GetEnterpriseProfile(m_strAppID, m_strEnterpriseID);
				int rounding = Convert.ToInt32(profileDS.Tables[0].Rows[0]["currency_decimal"]);
				string rd = "";

				for(int j=0;j<rounding;j++)
				{
					if(j==0)
						rd = "#.";

					rd = rd + "0";
				}

				int i = 0;
				//i = lbSIPConDetail.SelectedIndex ;
				txtMPSNo.Text =con.Tables[0].Rows[i]["mps_no"].ToString();
				txtQuantity.Text = con.Tables[0].Rows[i]["quantity"].ToString();
				if (con.Tables[0].Rows[i]["weight"].ToString() =="0")
					txtWeight.Text ="";
				else
				{
					if(con.Tables[0].Rows[i]["weight"].ToString() != "")
						txtWeight.Text = Convert.ToDecimal(con.Tables[0].Rows[i]["weight"]).ToString(rd);
				}

				if (con.Tables[0].Rows[i]["length"].ToString() =="0")
					txtLength.Text ="";
				else
					txtLength.Text = con.Tables[0].Rows[i]["length"].ToString();

				if (con.Tables[0].Rows[i]["breadth"].ToString() =="0")
					txtBreadth.Text ="";
				else
					txtBreadth.Text = con.Tables[0].Rows[i]["breadth"].ToString();

				if (con.Tables[0].Rows[i]["height"].ToString() =="0")
					txtHeight.Text ="";
				else 
					txtHeight.Text = con.Tables[0].Rows[i]["height"].ToString();

				ViewState["chkDtlQTY"] = con.Tables[0].Rows[i]["quantity"].ToString();
			}
			else
			{
				txtMPSNo.Text ="";
				txtQuantity.Text ="";
				txtWeight.Text ="";
				txtLength.Text ="";
				txtBreadth.Text ="";
				txtHeight.Text ="";
			}
		}
		private void ClearControlDisplayDetail()
		{
			txtMPSNo.Text ="";
			txtQuantity.Text ="";
			txtWeight.Text ="";
			txtLength.Text ="";
			txtBreadth.Text ="";
			txtHeight.Text ="";
		}

		private void btnExcuteQuery_Click(object sender, System.EventArgs e)
		{
			//Utility utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings,Page.Session);
			btnExcuteQuery.Enabled = false; 
			string con = txtConNo.Text.ToString();
			string[] tmpcon = con.Split(' ');
			txtConNo.Text =con;
			
			generateDataToListbox();

			EnterpriseConfigurations();
		}

		private void lbSIPCon_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			lblErrrMsg.Text =""; 
			ClearControlDisplayDetail();
			string space = System.Web.HttpUtility.HtmlDecode("&nbsp;");
			string ndata = lbSIPCon.SelectedValue.Replace(space,","); 
			string[] data = ndata.Split(','); 
			Session["ConNoSelect"] = data[0];

			DataSet con = SWB_Emulator.SWB_SIP_Read( m_strAppID,m_strEnterpriseID,(string)Session["ConNoSelect"]  );

			if (con.Tables[0].Rows.Count > 0 )
			{
				Session["Pkgs"] = con.Tables[0].Rows[0]["number_of_package"].ToString();	
			}
			else
			{
				Session["Pkgs"] ="0";
			}

			lbSIPConDetail.Items.Clear();  
			generateDataToListboxPackageDetail((string)Session["ConNoSelect"]);
			
//			string pkgs ="";
//			string pkgdtl ="";
//			pkgs = Session["Pkgs"].ToString(); 
//			pkgdtl = ViewState["dtlQTY"].ToString(); 
//			
//			if (Convert.ToInt32(pkgdtl) < Convert.ToInt32(pkgs) )
//			{
//				btnInsert.Enabled=true;
//				SetInitialFocus(txtMPSNo);
//			}
//			else if (Convert.ToInt32(pkgdtl) == Convert.ToInt32(pkgs) )
//			{
//				SetInitialFocus(txtConNo);
//				btnInsert.Enabled=false;
//			}
//			else
//			{
//				btnInsert.Enabled=false;
//			}
			
			
		}

		private void btnInsert_Click(object sender, System.EventArgs e)
		{
			ValidatePkg("Insert");
			txtQuantity.Text="1";
//			chkCWE.Checked = false;
		}

		private void btnUpdate_Click(object sender, System.EventArgs e)
		{
			ValidatePkg("Modify");
			txtQuantity.Text="1";
//			chkCWE.Checked = false;
		}

		private void btnDelete_Click(object sender, System.EventArgs e)
		{
			int result = SWB_Emulator.SWB_Pkg_Delete(m_strAppID,m_strEnterpriseID,(string)Session["ConNoSelect"],txtMPSNo.Text);
			DataSet conMPS = SWB_Emulator.SWB_GetMPSNo(m_strAppID,m_strEnterpriseID,(string)Session["ConNoSelect"] );

			lblErrrMsg.Text ="Delete successfully.";
			lbSIPConDetail.Items.Clear();  
			generateDataToListboxPackageDetail((string)Session["ConNoSelect"]);
			//ClearControlDisplayDetail();
			if (lbSIPConDetail.Items.Count == 0 )
			{
				lblPackageDetailsCountDisplay.Text ="0";
			}

			string pkgs = Session["Pkgs"].ToString(); 
			if (Convert.ToInt32(lblPackageDetailsCountDisplay.Text) == Convert.ToInt32(pkgs) )
			{
				txtConNo.Text ="";
				SetInitialFocus(txtConNo); 
			}
			else if (Convert.ToInt32(lblPackageDetailsCountDisplay.Text) < Convert.ToInt32(pkgs) )
			{
				txtMPSNo.ReadOnly =false;

				EnterpriseConfigurations();

				txtMPSNo.Text = conMPS.Tables[0].Rows[0]["mps_no"].ToString();   

			}
			txtQuantity.Text="1";
			txtLength.Text="1";
			txtBreadth.Text="1";
			txtHeight.Text="1";
//			chkCWE.Checked = false;
		}

		private void lbSIPConDetail_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			DisplayDetail((string)Session["ConNoSelect"]);
			lblErrrMsg.Text =""; 
			
			if (lbSIPConDetail.Items.Count > 0 )
			{
				if (txtMPSNo.Text !="")
				{
					btnInsert.Enabled =false;
					txtMPSNo.ReadOnly =true;
					btnUpdate.Enabled =true;
					btnDelete.Enabled =true;
				}
			}
			else
			{
				if (Convert.ToInt32(Session["Pkgs"].ToString()) <= Convert.ToInt32(lblPackageDetailsCountDisplay.Text))
				{
					//SetInitialFocus(txtMPSNo);
					txtMPSNo.ReadOnly =true;
				}
				else
				{
					SetInitialFocus(txtMPSNo);
					txtMPSNo.ReadOnly =false;
					btnInsert.Enabled =true; 
					btnUpdate.Enabled =false;
					btnDelete.Enabled =false;
				}
			}
			
//			chkCWE.Checked = false;
		}

		private void ValidatePkg(string state)
		{
			if(txtMPSNo.Text == "" || txtQuantity.Text == "" || txtWeight.Text == "" || txtLength.Text == "" || txtBreadth.Text == "" || txtHeight.Text == "")
			{
				lblErrrMsg.Text = "MPS Number,Quantity,Weight,Length,Breadth,Height required field.";
			}
			else
			{
				string pkgs ="";
				pkgs = Session["Pkgs"].ToString(); 
				string  Length ="";
				string Weight="";
				string Breadth="";
				string Height="";
				int sumchkqty = 0;

				if (state == "Insert")
				{
					//				if (txtLength.Text =="")
					//					Length = "1";
					//				else
					Length = txtLength.Text;
					//				if (txtWeight.Text == "")
					//					Weight ="1";
					//				else
					Weight = txtWeight.Text;
					//				if (txtBreadth.Text =="")
					//					Breadth = "1";
					//				else
					Breadth =txtBreadth.Text;
					//				if (txtHeight.Text == "")
					//					Height ="1";
					//				else
					Height = txtHeight.Text;
					if (txtMPSNo.Text != "")
					{
						if(txtQuantity.Text != "" && txtQuantity.Text !="0")
						{
							if (txtWeight.Text !="" && txtWeight.Text !="0")
							{
								if (txtLength.Text !="" && txtBreadth.Text !="" && txtHeight.Text!="" && txtLength.Text !="0" && txtBreadth.Text !="0" && txtHeight.Text !="0")
								{
									if ( Convert.ToInt32(pkgs) >= Convert.ToInt32(lblPackageDetailsCountDisplay.Text))
									{
										if (Convert.ToInt32(pkgs) >= Convert.ToInt32(lblPackageDetailsCountDisplay.Text)+Convert.ToInt32(txtQuantity.Text))
										{
											if (BatchManifest.checkduplicate(m_strAppID,m_strEnterpriseID,Session["ConNoSelect"].ToString(),txtMPSNo.Text) == 0)
											{
												int result = SWB_Emulator.SWB_Pkg_Insert(m_strAppID,m_strEnterpriseID,Session["ConNoSelect"].ToString() ,txtMPSNo.Text
													,txtQuantity.Text ,Weight,Length ,Breadth ,Height ,lblUserDisplay.Text );
												lblErrrMsg.Text ="Insert successfully.";
												lbSIPConDetail.Items.Clear();  
												generateDataToListboxPackageDetail((string)Session["ConNoSelect"]);
												//ClearControlDisplayDetail();
												DataSet conMPS = SWB_Emulator.SWB_GetMPSNo(m_strAppID,m_strEnterpriseID,Session["ConNoSelect"].ToString() );
												if (Convert.ToInt32(lblPackageDetailsCountDisplay.Text) == Convert.ToInt32(pkgs) )
												{
													txtConNo.Text ="";
													SetInitialFocus(txtConNo); 
													//												lbSIPCon.Items.Clear();  
													//												ClearControlDisplayDetail();
													//	lbSIPConDetail.Items.Clear(); 
												}
												else if (Convert.ToInt32(lblPackageDetailsCountDisplay.Text) < Convert.ToInt32(pkgs) )
												{

													EnterpriseConfigurations();

													txtMPSNo.Text = conMPS.Tables[0].Rows[0]["mps_no"].ToString();  
												}
											}
											else
											{
												lblErrrMsg.Text ="Duplicate MPS No.";
												ClearControlDisplayDetail();
												SetInitialFocus(txtMPSNo); 
											}
					
										}
										else
										{
											lblErrrMsg.Text = "Package Details quantity does not match the quantity on the initial SIP. Please Complete the package details before proceeding.";
										}
									}
									else
									{
										lblErrrMsg.Text = "Package Details quantity does not match the quantity on the initial SIP. Please Complete the package details before proceeding.";
									}
								}
								else
								{
									if (txtLength.Text =="" && txtBreadth.Text =="" && txtHeight.Text=="")
									{
										if ( Convert.ToInt32(pkgs) >= Convert.ToInt32(lblPackageDetailsCountDisplay.Text))
										{
											if (Convert.ToInt32(pkgs) >= Convert.ToInt32(lblPackageDetailsCountDisplay.Text)+Convert.ToInt32(txtQuantity.Text))
											{
												if (BatchManifest.checkduplicate(m_strAppID,m_strEnterpriseID,Session["ConNoSelect"].ToString(),txtMPSNo.Text) == 0)
												{
													int result = SWB_Emulator.SWB_Pkg_Insert(m_strAppID,m_strEnterpriseID,Session["ConNoSelect"].ToString() ,txtMPSNo.Text
														,txtQuantity.Text ,Weight,Length ,Breadth ,Height ,lblUserDisplay.Text );
													lblErrrMsg.Text ="Insert successfully.";
													lbSIPConDetail.Items.Clear();  
													generateDataToListboxPackageDetail((string)Session["ConNoSelect"]);
													//ClearControlDisplayDetail();
													DataSet conMPS = SWB_Emulator.SWB_GetMPSNo(m_strAppID,m_strEnterpriseID,Session["ConNoSelect"].ToString() );

													if (Convert.ToInt32(lblPackageDetailsCountDisplay.Text) == Convert.ToInt32(pkgs) )
													{
														txtConNo.Text ="";
														SetInitialFocus(txtConNo); 
														//													lbSIPCon.Items.Clear();  
														//													ClearControlDisplayDetail();
														//													lbSIPConDetail.Items.Clear();
													}
													else if (Convert.ToInt32(lblPackageDetailsCountDisplay.Text) < Convert.ToInt32(pkgs) )
													{
														EnterpriseConfigurations(); 
														txtMPSNo.Text = conMPS.Tables[0].Rows[0]["mps_no"].ToString();  
													}
												}
												else
												{
													lblErrrMsg.Text ="Duplicate MPS No.";
													ClearControlDisplayDetail();
													SetInitialFocus(txtMPSNo); 
												}
											}
											else
											{
												lblErrrMsg.Text = "Package Details quantity does not match the quantity on the initial SIP. Please Complete the package details before proceeding.";
											}
										}
										else
										{
											lblErrrMsg.Text = "Package Details quantity does not match the quantity on the initial SIP. Please Complete the package details before proceeding.";
										}
									}
									else
									{
										lblErrrMsg.Text = "Length,Breadth,Height required field and value more than 0.";
									}
								}
							}
							else
							{
								lblErrrMsg.Text = "Weight is required field and weight more than 0.";
								SetInitialFocus(txtWeight); 
							}
						}
						else
						{
							lblErrrMsg.Text = "Quantity is required field and quantity more than 0. ";
							SetInitialFocus(txtQuantity); 
						}
					}
					else
					{
						lblErrrMsg.Text = "MPSNo is required field";
						SetInitialFocus(txtMPSNo); 
					}
			
				}		
				else if (state == "Modify")
				{
					if (txtLength.Text =="")
						Length = "0";
					else
						Length = txtLength.Text;
					if (txtWeight.Text == "")
						Weight ="0";
					else
						Weight = txtWeight.Text;
					if (txtBreadth.Text =="")
						Breadth = "0";
					else
						Breadth =txtBreadth.Text;
					if (txtHeight.Text == "")
						Height ="0";
					else
						Height = txtHeight.Text;
					if(txtQuantity.Text != "" && txtQuantity.Text !="0")
					{
						if (txtWeight.Text !="" && txtWeight.Text !="0")
						{
							if (txtLength.Text !="" && txtBreadth.Text !="" && txtHeight.Text !="" && txtLength.Text !="0" && txtBreadth.Text !="0" && txtHeight.Text !="0")
							{
								if (lbSIPConDetail.Items.Count >=1 )
								{
									if (Convert.ToInt32(txtQuantity.Text) < Convert.ToInt32(ViewState["chkDtlQTY"]) )
										sumchkqty = Convert.ToInt32(ViewState["dtlQTY"])-Convert.ToInt32(txtQuantity.Text);
									else if (Convert.ToInt32(txtQuantity.Text) > Convert.ToInt32(ViewState["chkDtlQTY"]) )
										sumchkqty = (Convert.ToInt32(ViewState["dtlQTY"])-Convert.ToInt32(ViewState["chkDtlQTY"]))+Convert.ToInt32(txtQuantity.Text);
									else
										sumchkqty = Convert.ToInt32(txtQuantity.Text);

									if (Convert.ToInt32(pkgs) >= sumchkqty)
									{
										int result = SWB_Emulator.SWB_Pkg_Update(m_strAppID,m_strEnterpriseID,Session["ConNoSelect"].ToString(),txtMPSNo.Text
											,txtQuantity.Text ,Weight ,Length ,Breadth ,Height ,lblUserDisplay.Text );
										lblErrrMsg.Text ="Update successfully.";
										lbSIPConDetail.Items.Clear();  
										generateDataToListboxPackageDetail((string)Session["ConNoSelect"]);
										//ClearControlDisplayDetail();
										DataSet conMPS = SWB_Emulator.SWB_GetMPSNo(m_strAppID,m_strEnterpriseID,Session["ConNoSelect"].ToString() );

										if (sumchkqty < Convert.ToInt32(pkgs))
										{
											txtMPSNo.ReadOnly =false;
											SetInitialFocus(txtWeight);
											txtMPSNo.Text = conMPS.Tables[0].Rows[0]["mps_no"].ToString();  
										}
										else if (sumchkqty == Convert.ToInt32(pkgs))
										{
											txtConNo.Text ="";
											SetInitialFocus(txtConNo);
											txtMPSNo.ReadOnly =true;
											//													lbSIPCon.Items.Clear();  
											//													ClearControlDisplayDetail();
											//													lbSIPConDetail.Items.Clear();
										}											
									}
									else
									{
										lblErrrMsg.Text = "Package Details quantity does not match the quantity on the initial SIP. Insert not allowed.";
										SetInitialFocus(txtWeight); 
									}
								}
								//										else
								//										{
								//											if (Convert.ToInt32(pkgs) >= Convert.ToInt32(txtQuantity.Text))
								//											{
								//												int result = SWB_Emulator.SWB_Pkg_Update(m_strAppID,m_strEnterpriseID,txtConNo.Text,txtMPSNo.Text
								//													,txtQuantity.Text ,Weight ,Length ,Breadth ,Height ,lblUserDisplay.Text );
								//												lblErrrMsg.Text ="Update successfully.";
								//												lbSIPConDetail.Items.Clear();  
								//												generateDataToListboxPackageDetail((string)Session["ConNoSelect"]);
								//												ClearControlDisplayDetail();
								//											}
								//											else
								//											{
								//												lblErrrMsg.Text = "Package Details quantity does not match the quantity on the initial SIP. Insert not allowed.";
								//												SetInitialFocus(txtQuantity); 
								//											}
								//										}
								//									}
									
							}
							else 
							{
								if (txtLength.Text =="" && txtBreadth.Text =="" && txtHeight.Text=="")
								{
									if (lbSIPConDetail.Items.Count >=1 )
									{
										if (Convert.ToInt32(txtQuantity.Text) < Convert.ToInt32(ViewState["chkDtlQTY"]) )
											sumchkqty = Convert.ToInt32(ViewState["dtlQTY"])-Convert.ToInt32(txtQuantity.Text);
										else if (Convert.ToInt32(txtQuantity.Text) > Convert.ToInt32(ViewState["chkDtlQTY"]) )
											sumchkqty = Convert.ToInt32(ViewState["dtlQTY"])+Convert.ToInt32(txtQuantity.Text);
										else
											sumchkqty = Convert.ToInt32(txtQuantity.Text);

										if (Convert.ToInt32(pkgs) >= sumchkqty)
										{
											int result = SWB_Emulator.SWB_Pkg_Update(m_strAppID,m_strEnterpriseID,Session["ConNoSelect"].ToString(),txtMPSNo.Text
												,txtQuantity.Text ,Weight ,Length ,Breadth ,Height ,lblUserDisplay.Text );
											lblErrrMsg.Text ="Update successfully.";
											lbSIPConDetail.Items.Clear();  
											generateDataToListboxPackageDetail((string)Session["ConNoSelect"]);
											//ClearControlDisplayDetail();
											DataSet conMPS = SWB_Emulator.SWB_GetMPSNo(m_strAppID,m_strEnterpriseID,Session["ConNoSelect"].ToString() );

											if (sumchkqty < Convert.ToInt32(pkgs))
											{
												txtMPSNo.ReadOnly =false;
												SetInitialFocus(txtWeight);
												txtMPSNo.Text = conMPS.Tables[0].Rows[0]["mps_no"].ToString();  
											}
											else if (sumchkqty == Convert.ToInt32(pkgs))
											{
												txtConNo.Text ="";
												SetInitialFocus(txtConNo);
												txtMPSNo.ReadOnly =true;
												//												lbSIPCon.Items.Clear(); 
												//												ClearControlDisplayDetail();
												//												lbSIPConDetail.Items.Clear();
											}							
										}
										else
										{
											lblErrrMsg.Text = "Package Details quantity does not match the quantity on the initial SIP. Insert not allowed.";
											SetInitialFocus(txtWeight); 
										}
									}
								}
								else
								{
									lblErrrMsg.Text = "Length,Breadth and Height required field and value more than 0.";
								}
								//SetInitialFocus(txtLength);
							}
						}
						else
						{
							lblErrrMsg.Text = "Weight is required field and weight more than 0.";
							SetInitialFocus(txtWeight); 
						}
					}
					else
					{
						lblErrrMsg.Text = "Quantity is required field and quantity more than 0. ";
						SetInitialFocus(txtWeight); 
					}

				}
			}
		}

		private void txtConNo_TextChanged(object sender, System.EventArgs e)
		{
			
		}

		private void txtMPSNo_TextChanged(object sender, System.EventArgs e)
		{
			txtLength.Text="1";
			txtBreadth.Text="1";
			txtHeight.Text="1";
			SetInitialFocus(txtWeight);
		}

		private void txtQuantity_TextChanged(object sender, System.EventArgs e)
		{
			SetInitialFocus(txtWeight);
			txtLength.Text="1";
			txtBreadth.Text="1";
			txtHeight.Text="1";
		}

		private void hddConNo_ServerClick(object sender, System.EventArgs e)
		{
			if (txtConNo.Text!="")
			{
				string con = txtConNo.Text.ToString();

				if(txtConNo.Text.Length==32)//Fedex32 Format
				{
					con= txtConNo.Text.Substring(CharBarcodeCutStartIndex,CharBarcodeCutLength);
				}
				else
				{
					string[] tmpcon = con.Split(' ');
					con=tmpcon[0];

				}
				string ConNo="";
				if(con.Length > 0 &&Utility.ValidateConsignmentNo(con, ref ConNo)==false)
				{
					lblErrrMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"CREATE_SIP_MSG_CONSNO_INVALID",utility.GetUserCulture()); 
					SetInitialFocus(txtConNo);
					return;
				}
				else
				{
					//lblErrorMsg.Text="";
					con=ConNo;
				}
				
				ViewState["conforscan"] = con ;
				txtConNo.Text = con;
				
				btnExcuteQuery_Click(this,null); 
				//				btnExcuteQuery.Enabled=false;
			}
		}

		private void chkCWE_CheckedChanged(object sender, System.EventArgs e)
		{
			if(chkCWE.Checked)
			{
				if(txtWeight.Text.Trim() != "" && txtQuantity.Text.Trim() != "")
				{
					DataSet profileDS = SysDataManager1.GetEnterpriseProfile(m_strAppID, m_strEnterpriseID);
					int rounding = Convert.ToInt32(profileDS.Tables[0].Rows[0]["currency_decimal"]);
					string rd = "";

					for(int i=0;i<rounding;i++)
					{
						if(i==0)
							rd = "#.";

						rd = rd + "0";
					}

					txtWeight.Text = (Convert.ToDecimal(txtWeight.Text)/Convert.ToDecimal(txtQuantity.Text)).ToString(rd);
					if(txtWeight.Text == ".00")
						txtWeight.Text = rd.Replace("#","0");
				}
			}
			else
			{
				if(txtWeight.Text.Trim() != "" && txtQuantity.Text.Trim() != "")
				{
					txtWeight.Text = (Convert.ToDecimal(txtWeight.Text)*Convert.ToDecimal(txtQuantity.Text)).ToString();
				}
			}
		}

		
		private bool getRoleOPSSU()
		{
			User user = new User();
			user.UserID = userID;
			userRoleArray = com.common.RBAC.RBACManager.GetAllRoles(m_strAppID, m_strEnterpriseID, user);
			Role role;

			for(int i=0;i<userRoleArray.Count;i++)
			{
				role = (Role)userRoleArray[i];				
				if (role.RoleName.ToUpper().Trim() == "OPSSU") return true;
			}
			return false;
		}

//		
//
		private void EnterpriseConfigurations()
		{
			PackageWtDimConfigurations conf = new PackageWtDimConfigurations();
			
		    conf = EnterpriseConfigMgrDAL.GetPackageWtDimConfigurations(m_strAppID, m_strEnterpriseID);
			this.txtMPSNo.Enabled=conf.Mpsnumber;
			this.txtQuantity.Enabled=conf.Quantity;
			this.txtWeight.Enabled=conf.Weight;
			this.txtLength.Enabled=conf.Length;
			this.txtBreadth.Enabled=conf.Breadth;
			this.txtHeight.Enabled=conf.Height;

			if(conf.DefaultFocus == "MPS Number")
			{
				SetInitialFocus(txtMPSNo);
			}
			else if(conf.DefaultFocus == "Quantity")
			{
				SetInitialFocus(txtQuantity);
			}
			else if(conf.DefaultFocus == "Weight")
			{
				SetInitialFocus(txtWeight);
			}
			else if(conf.DefaultFocus == "Length")
			{
				SetInitialFocus(txtLength);
			}
			else if(conf.DefaultFocus == "Breadth")
			{
				SetInitialFocus(txtBreadth);
			}
			else if(conf.DefaultFocus == "Height")
			{
				SetInitialFocus(txtHeight);
			}
		}

	}
}
