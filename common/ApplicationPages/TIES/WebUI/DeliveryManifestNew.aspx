<%@ Import Namespace="com.common.util" %>
<%@ Import Namespace="Cambro.Web.DbCombo" %>
<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<%@ Register TagPrefix="DbCombo" Namespace="Cambro.Web.DbCombo" Assembly="Cambro.Web.DbCombo" %>
<%@ Page language="c#" Codebehind="DeliveryManifestNew.aspx.cs" AutoEventWireup="false" Inherits="com.ties.DeliveryManifestNew" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Delivery Manifest</title>
		<LINK href="css/Styles.css" type="text/css" rel="stylesheet" name="stylesheet">
		<META content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<META content="C#" name="CODE_LANGUAGE">
		<META content="JavaScript" name="vs_defaultClientScript">
		<META content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema"> <!--#INCLUDE FILE="msFormValidations.inc"-->
		<script language="javascript" src="Scripts/settingScrollPosition.js"></script>
	</HEAD>
	<body onclick="GetScrollPosition();" onload="SetScrollPosition();" onunload="window.top.displayBanner.fnCloseAll(0);"
		MS_POSITIONING="GridLayout" XMLNS:DbCombo="http://schemas.cambro.net/dbcombo">
		<FORM id="DeliveryManifest" method="post" runat="server">
			<asp:label id="lblMainTitle" style="Z-INDEX: 103; LEFT: 15px; POSITION: absolute; TOP: 6px"
				runat="server" Width="477px" Height="26px" CssClass="mainTitleSize">Delivery Manifest</asp:label><asp:label id="lblErrorMsg" style="Z-INDEX: 111; LEFT: 14px; POSITION: absolute; TOP: 41px"
				runat="server" Width="566px" Height="19px" CssClass="errorMsgColor"></asp:label>
			<DIV id="gridPanel" style="Z-INDEX: 101; LEFT: 5px; WIDTH: 756px; POSITION: relative; TOP: 56px; HEIGHT: 709px"
				MS_POSITIONING="GridLayout" runat="server"><asp:button id="btnQry" tabIndex="100" runat="server" Width="62px" CssClass="queryButton" CausesValidation="False"
					Text="Query"></asp:button><asp:button id="btnExecQry" tabIndex="1" runat="server" CssClass="queryButton" CausesValidation="False"
					Text="Execute Query"></asp:button><asp:button id="btnInsert" tabIndex="3" runat="server" Width="62px" CssClass="queryButton" CausesValidation="False"
					Text="Insert"></asp:button><asp:button id="btnSave" tabIndex="4" runat="server" Width="66px" CssClass="queryButton" Text="Save"></asp:button><asp:button id="btnPrint" tabIndex="27" runat="server" Width="184px" CssClass="queryButton"
					CausesValidation="False" Text="Print Delivery Manifest"></asp:button>
				<TABLE id="DeliveryManifestTable" width="724" border="0" runat="server">
					<TR height="9">
						<TD width="22"></TD>
						<TD width="5%"></TD>
						<TD width="5%"></TD>
						<TD style="WIDTH: 209px" width="209"></TD>
						<TD width="5%"></TD>
						<TD width="5%"></TD>
						<TD width="5%"></TD>
						<TD width="5%"></TD>
						<TD width="5%"></TD>
						<TD width="5%"></TD>
						<TD width="5%"></TD>
						<TD width="5%"></TD>
						<TD width="5%"></TD>
						<TD width="5%"></TD>
						<TD width="5%"></TD>
						<TD style="WIDTH: 2px" width="2"></TD>
						<TD width="5%"></TD>
						<TD width="5%"></TD>
						<TD width="5%"></TD>
						<TD width="5%"></TD>
					</TR>
					<TR height="27">
						<TD colSpan="1"></TD>
						<TD colSpan="6"><asp:label id="lblTitle" runat="server" CssClass="tablelabel">Delivery Type</asp:label></TD>
						<TD colSpan="5"></TD>
						<TD colSpan="8"></TD>
					</TR>
					<TR height="27">
						<TD colSpan="1"></TD>
						<TD style="WIDTH: 231px" borderColor="gainsboro" colSpan="3"><asp:radiobutton id="rbtnLong" tabIndex="11" Width="179px" CssClass="tableRadioButton" Text="Linehaul"
								Runat="server" Checked="True" Font-Size="Smaller" AutoPostBack="True" GroupName="GrpDeliveryType"></asp:radiobutton></TD>
						<TD borderColor="gainsboro" colSpan="3"><asp:radiobutton id="rbtnShort" tabIndex="12" Width="136px" CssClass="tableRadioButton" Text="Short Route"
								Runat="server" Font-Size="Smaller" AutoPostBack="True" GroupName="GrpDeliveryType"></asp:radiobutton></TD>
						<TD style="WIDTH: 173px" borderColor="gainsboro" colSpan="6"><asp:radiobutton id="rbtnAir" tabIndex="13" Width="192px" CssClass="tableRadioButton" Text="Air Route"
								Runat="server" Font-Size="Smaller" AutoPostBack="True" GroupName="GrpDeliveryType"></asp:radiobutton></TD>
						<TD style="WIDTH: 106px" colSpan="3"></TD>
						<TD colSpan="3"><asp:textbox id="txtDelvryType" runat="server" Width="36px" Visible="False"></asp:textbox></TD>
						<TD colSpan="1"></TD>
					</TR>
					<TR height="1">
						<TD colSpan="20">&nbsp;</TD>
					</TR>
					<TR height="27">
						<TD align="right" colSpan="1"><asp:requiredfieldvalidator id="validDlvryPath" Runat="server" display="None" ErrorMessage="Delivery Path Code"
								ControlToValidate="DbComboPathCode">*</asp:requiredfieldvalidator></TD>
						<TD style="WIDTH: 231px" colSpan="3"><asp:label id="lblDlvryPath" runat="server" Width="100%" Height="22px" CssClass="tableLabel">Delivery Path Code</asp:label></TD>
						<TD colSpan="4"><DBCOMBO:DBCOMBO id="DbComboPathCode" tabIndex="14" Width="25px" CssClass="tableLabel" Runat="server"
								AutoPostBack="True" ServerMethod="DbComboPathCodeSelect" ShowDbComboLink="False" TextUpLevelSearchButton="v"
								RegistrationKey=" " TextBoxColumns="18"></DBCOMBO:DBCOMBO></TD>
						<TD style="WIDTH: 116px" colSpan="2"></TD>
						<TD colSpan="7"><asp:textbox id="txtPathDesc" tabIndex="15" runat="server" Width="100%" CssClass="textField"
								Enabled="False" ReadOnly="True"></asp:textbox></TD>
						<TD colSpan="2"></TD>
					</TR>
					<TR height="27">
						<TD align="right" colSpan="1"><asp:requiredfieldvalidator id="reqFlightVehicleNo" Runat="server" ErrorMessage="" ControlToValidate="txtVehicle_Flight_No">*</asp:requiredfieldvalidator></TD>
						<TD style="WIDTH: 231px" colSpan="3"><asp:label id="lblVehicle_Flight_No" runat="server" Width="184px" Height="22px" CssClass="tableLabel">Vehicle No</asp:label></TD>
						<TD colSpan="4"><cc1:mstextbox id="txtVehicle_Flight_No" tabIndex="16" runat="server" Width="100%" CssClass="textField"
								Enabled="False" TextMaskType="msUpperAlfaNumericWithUnderscore" MaxLength="12"></cc1:mstextbox></TD>
						<TD align="right" colSpan="1"><asp:requiredfieldvalidator id="reqLineHaulCost" Runat="server" ErrorMessage="Line Haul Cost" ControlToValidate="txtLineHaulCost">*</asp:requiredfieldvalidator></TD>
						<TD colSpan="4"><asp:label id="lblLineHaulCost" runat="server" Width="100%" Height="22px" CssClass="tableLabel">Line Haul Cost</asp:label></TD>
						<TD colSpan="4"><cc1:mstextbox id="txtLineHaulCost" onblur="round(this,2)" tabIndex="17" runat="server" Width="100%"
								CssClass="textFieldRightAlign" AutoPostBack="True" Enabled="False" TextMaskType="msNumeric" MaxLength="9"
								NumberMaxValue="999999" NumberMinValue="0" NumberPrecision="8" NumberScale="2" TextMaskString="#.00"></cc1:mstextbox></TD>
						<TD colSpan="2"></TD>
					</TR>
					<TR height="27">
						<TD align="right" colSpan="1"><asp:requiredfieldvalidator id="reqDriverFlightAWB" Runat="server" ErrorMessage="" ControlToValidate="txtDriver_Flight_AWB">*</asp:requiredfieldvalidator></TD>
						<TD style="WIDTH: 231px" colSpan="3"><asp:label id="lblDriver_Flight_AWB" runat="server" Width="100%" Height="22px" CssClass="tableLabel">Driver Name</asp:label></TD>
						<TD colSpan="4"><asp:textbox id="txtDriver_Flight_AWB" tabIndex="18" runat="server" Width="100%" CssClass="textField"
								Enabled="False" TextMaskType="msUpperAlfaNumericWithUnderscore" MaxLength="12"></asp:textbox></TD>
						<TD align="right" colSpan="1"><asp:requiredfieldvalidator id="reqDepartDate" tabIndex="-32768" Runat="server" ErrorMessage="Manifested Date"
								ControlToValidate="txtDelManifestDate">*</asp:requiredfieldvalidator></TD>
						<TD colSpan="4"><asp:label id="lblDelManifestDate" runat="server" Width="100%" Height="22px" CssClass="tableLabel">Manifested Date</asp:label></TD>
						<TD colSpan="4"><cc1:mstextbox id="txtDelManifestDate" tabIndex="19" runat="server" Width="100%" CssClass="textField"
								Enabled="False" TextMaskType="msDateTime" MaxLength="16" TextMaskString="99/99/9999 99:99"></cc1:mstextbox></TD>
						<TD colSpan="2"></TD>
					</TR>
					<TR height="27">
						<TD align="right" colSpan="1"><asp:requiredfieldvalidator id="reqDepartureDate" Runat="server" ErrorMessage="Departure Date" ControlToValidate="dbDepartDate">*</asp:requiredfieldvalidator></TD>
						<TD style="WIDTH: 231px" colSpan="3"><asp:label id="lblDepartDate" runat="server" Width="100%" Height="22px" CssClass="tableLabel">Departure Date</asp:label></TD>
						<TD colSpan="4"><DBCOMBO:DBCOMBO id="dbDepartDate" tabIndex="3" Runat="server" AutoPostBack="True" ServerMethod="ShowManifestedDate"
								ShowDbComboLink="False" TextUpLevelSearchButton="v" RegistrationKey=" " TextBoxColumns="18" NewVersionAvailable="True"></DBCOMBO:DBCOMBO></TD>
						<TD colSpan="1"></TD>
						<TD colSpan="4"></TD>
						<TD colSpan="4"><asp:button id="btnRetrieveConsignment" tabIndex="21" runat="server" Width="100%" CssClass="queryButton"
								CausesValidation="False" Text="Retrieve Consignment"></asp:button></TD>
						<TD colSpan="2"></TD>
					</TR>
					<TR height="1">
						<TD colSpan="20">&nbsp;</TD>
					</TR>
					<TR>
						<TD colSpan="1"></TD>
						<TD align="left" colSpan="8"><asp:label id="lblAssigned" runat="server" Height="22px" CssClass="tableLabel">Consignments Available</asp:label></TD>
						<TD style="WIDTH: 59px" colSpan="1"></TD>
						<TD align="left" colSpan="8"><asp:label id="lblAvailable" runat="server" Width="100%" Height="22px" CssClass="tableLabel">Consignments Assigned</asp:label></TD>
						<TD colSpan="1"></TD>
						<TD colSpan="1"></TD>
					</TR>
					<TR>
						<TD colSpan="1"></TD>
						<TD vAlign="top" colSpan="7">
							<DIV style="BORDER-RIGHT: #99ffcc 1px solid; BORDER-TOP: #99ffcc 1px solid; OVERFLOW: auto; BORDER-LEFT: #99ffcc 1px solid; BORDER-BOTTOM: #99ffcc 1px solid; HEIGHT: 245px"><asp:datagrid id="dgAvailConsignment" runat="server" Width="100%" AutoGenerateColumns="False"
									ItemStyle-Height="20">
									<SelectedItemStyle CssClass="gridFieldSelected"></SelectedItemStyle>
									<ItemStyle Height="20px" CssClass="gridField"></ItemStyle>
									<Columns>
										<asp:TemplateColumn>
											<HeaderStyle Width="5%" CssClass="gridHeading"></HeaderStyle>
											<ItemStyle CssClass="gridField" VerticalAlign="Middle"></ItemStyle>
											<ItemTemplate>
												<asp:CheckBox id="chkSelect" Runat="server" EnableViewState="True"></asp:CheckBox>
											</ItemTemplate>
										</asp:TemplateColumn>
										<asp:TemplateColumn HeaderText="Consignment">
											<HeaderStyle Width="50%" CssClass="gridHeading"></HeaderStyle>
											<ItemStyle Wrap="False"></ItemStyle>
											<ItemTemplate>
												<asp:Label CssClass="gridLabel" ID="lblConsgmntNo" Text='<%#DataBinder.Eval(Container.DataItem,"consignment_no")%>' Runat="server">
												</asp:Label>
											</ItemTemplate>
										</asp:TemplateColumn>
										<asp:TemplateColumn HeaderText="ORG">
											<HeaderStyle Width="15%" CssClass="gridHeading"></HeaderStyle>
											<ItemTemplate>
												<asp:Label CssClass="gridLabel" ID="lblDGOrigin" Text='<%#DataBinder.Eval(Container.DataItem,"origin_state_code")%>' Runat="server" Enabled="True">
												</asp:Label>
											</ItemTemplate>
										</asp:TemplateColumn>
										<asp:TemplateColumn HeaderText="DST">
											<HeaderStyle Width="15%" CssClass="gridHeading"></HeaderStyle>
											<ItemStyle VerticalAlign="Middle"></ItemStyle>
											<ItemTemplate>
												<asp:Label CssClass="gridLabel" ID="lblDGDestination" Text='<%#DataBinder.Eval(Container.DataItem,"destination_state_code")%>' Runat="server" Enabled="True">
												</asp:Label>
											</ItemTemplate>
										</asp:TemplateColumn>
										<asp:TemplateColumn HeaderText="RTE">
											<HeaderStyle Width="15%" CssClass="gridHeading"></HeaderStyle>
											<ItemStyle VerticalAlign="Middle"></ItemStyle>
											<ItemTemplate>
												<asp:Label CssClass="gridLabel" ID="lblDGRoute" Text='<%#DataBinder.Eval(Container.DataItem,"Route_Code")%>' Runat="server" Enabled="True">
												</asp:Label>
											</ItemTemplate>
										</asp:TemplateColumn>
									</Columns>
								</asp:datagrid></DIV>
						</TD>
						<TD style="WIDTH: 116px" vAlign="top" align="center" colSpan="2"><asp:button id="btnAdd" tabIndex="23" runat="server" Width="25px" CssClass="searchButton" CausesValidation="False"
								Text=">"></asp:button><BR>
							<asp:button id="btnRemove" tabIndex="24" runat="server" Width="25px" CssClass="searchButton"
								CausesValidation="False" Text="<"></asp:button><BR>
							<BR>
							<asp:button id="btnAddAll" tabIndex="25" runat="server" Width="25px" CssClass="searchButton"
								CausesValidation="False" Text=">>"></asp:button><BR>
							<asp:button id="btnRemoveAll" tabIndex="26" runat="server" Width="25px" CssClass="searchButton"
								CausesValidation="False" Text="<<"></asp:button><BR>
						</TD>
						<TD vAlign="top" colSpan="8">
							<DIV style="BORDER-RIGHT: #99ffcc 1px solid; BORDER-TOP: #99ffcc 1px solid; OVERFLOW: auto; BORDER-LEFT: #99ffcc 1px solid; BORDER-BOTTOM: #99ffcc 1px solid; HEIGHT: 245px"><asp:datagrid id="dgAssignedConsignment" runat="server" Width="100%" AutoGenerateColumns="False"
									ItemStyle-Height="20">
									<SelectedItemStyle CssClass="gridFieldSelected"></SelectedItemStyle>
									<ItemStyle Height="20px" CssClass="gridField"></ItemStyle>
									<Columns>
										<asp:TemplateColumn>
											<HeaderStyle Width="5%" CssClass="gridHeading"></HeaderStyle>
											<ItemStyle CssClass="gridField" VerticalAlign="Middle"></ItemStyle>
											<ItemTemplate>
												<asp:CheckBox id="chkSelect1" runat="server"></asp:CheckBox>
											</ItemTemplate>
										</asp:TemplateColumn>
										<asp:TemplateColumn HeaderText="Consignment">
											<HeaderStyle Width="50%" CssClass="gridHeading"></HeaderStyle>
											<ItemStyle Wrap="False"></ItemStyle>
											<ItemTemplate>
												<asp:Label CssClass="gridLabel" ID="lblConsgmntNo1" Text='<%#DataBinder.Eval(Container.DataItem,"consignment_no")%>' Runat="server">
												</asp:Label>
											</ItemTemplate>
										</asp:TemplateColumn>
										<asp:TemplateColumn HeaderText="ORG">
											<HeaderStyle Width="15%" CssClass="gridHeading"></HeaderStyle>
											<ItemTemplate>
												<asp:Label id=lblDGOrigin1 CssClass="gridLabel" Text='<%#DataBinder.Eval(Container.DataItem,"origin_state_code")%>' Runat="server" Enabled="True">
												</asp:Label>
											</ItemTemplate>
										</asp:TemplateColumn>
										<asp:TemplateColumn HeaderText="DST">
											<HeaderStyle Width="15%" CssClass="gridHeading"></HeaderStyle>
											<ItemStyle VerticalAlign="Middle"></ItemStyle>
											<ItemTemplate>
												<asp:Label CssClass="gridLabel" ID="lblDGDestination1" Text='<%#DataBinder.Eval(Container.DataItem,"destination_state_code")%>' Runat="server" Enabled="True">
												</asp:Label>
											</ItemTemplate>
										</asp:TemplateColumn>
										<asp:TemplateColumn HeaderText="RTE">
											<HeaderStyle Width="15%" CssClass="gridHeading"></HeaderStyle>
											<ItemStyle VerticalAlign="Middle"></ItemStyle>
											<ItemTemplate>
												<asp:Label CssClass="gridLabel" ID="lblDGRoute1" Text='<%#DataBinder.Eval(Container.DataItem,"Route_Code")%>' Runat="server" Enabled="True">
												</asp:Label>
											</ItemTemplate>
										</asp:TemplateColumn>
									</Columns>
								</asp:datagrid></DIV>
						</TD>
						<TD colSpan="1"></TD>
						<TD colSpan="1"></TD>
					</TR>
				</TABLE>
			</DIV>
			<DIV id="msgPanel" style="CLEAR: both; Z-INDEX: 108; LEFT: 0px; WIDTH: 472px; POSITION: relative; TOP: 49px; HEIGHT: 119px"
				runat="server" ms_positioning="GridLayout"><asp:label id="lblMessage" style="Z-INDEX: 100; LEFT: 88px; POSITION: absolute; TOP: 24px"
					runat="server" Width="264px" Height="28px" CssClass="tableLabel"></asp:label><asp:button id="btnToSaveChanges" style="Z-INDEX: 101; LEFT: 97px; POSITION: absolute; TOP: 70px"
					runat="server" Width="57px" CssClass="queryButton" Text="Yes"></asp:button><asp:button id="btnNotToSave" style="Z-INDEX: 102; LEFT: 155px; POSITION: absolute; TOP: 70px"
					runat="server" Width="57px" CssClass="queryButton" CausesValidation="False" Text="No"></asp:button><asp:button id="btnNO" style="Z-INDEX: 103; LEFT: 213px; POSITION: absolute; TOP: 70px" tabIndex="9"
					runat="server" Width="63" CssClass="queryButton" CausesValidation="False" Text="Cancel"></asp:button></DIV>
			<asp:validationsummary id="PageValidationSummary" style="Z-INDEX: 104; LEFT: 606px; POSITION: absolute; TOP: 5px"
				runat="server" ShowMessageBox="True" ShowSummary="False" DisplayMode="BulletList" HeaderText="Please enter the missing  fields."></asp:validationsummary><input 
type=hidden value="<%=strScrollPosition%>" name=ScrollPosition>
		</FORM>
	</body>
</HTML>
