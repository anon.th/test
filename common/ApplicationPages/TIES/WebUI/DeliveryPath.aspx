<%@ Page language="c#" Codebehind="DeliveryPath.aspx.cs" AutoEventWireup="false" Inherits="com.ties.DeliveryPath" %>
<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<%@ Import Namespace="com.common.util" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>DeliveryPath</title>
		<LINK href="css/styles.css" type="text/css" rel="stylesheet">
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<!--#INCLUDE FILE="msFormValidations.inc"-->
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="DeliveryPath" method="post" runat="server">
			<asp:label id="lblMainheading" style="Z-INDEX: 101; LEFT: 33px; POSITION: absolute; TOP: 11px"
				runat="server" CssClass="mainTitleSize" Height="25px" Width="201px">Delivery Path</asp:label>
			<asp:label id="lblErrorMessage" style="Z-INDEX: 106; LEFT: 35px; POSITION: absolute; TOP: 78px"
				runat="server" CssClass="errorMsgColor" Width="669px"></asp:label>
			<asp:button id="btnInsert" style="Z-INDEX: 105; LEFT: 229px; POSITION: absolute; TOP: 47px"
				runat="server" CssClass="queryButton" Width="51px" CausesValidation="False" Text="Insert"></asp:button>
			<asp:button id="btnExecuteQuery" style="Z-INDEX: 104; LEFT: 99px; POSITION: absolute; TOP: 47px"
				runat="server" CssClass="queryButton" CausesValidation="False" Text="ExecuteQuery"></asp:button><asp:button id="btnQuery" style="Z-INDEX: 103; LEFT: 32px; POSITION: absolute; TOP: 47px" runat="server"
				CssClass="queryButton" Width="67px" CausesValidation="False" Text="Query"></asp:button>
			<asp:datagrid id="dgDelvPath" style="Z-INDEX: 102; LEFT: 32px; POSITION: absolute; TOP: 113px"
				runat="server" Width="650px" OnDeleteCommand="dgDelvPath_Delete" OnPageIndexChanged="dgDelvPath_PageChange"
				AutoGenerateColumns="False" AllowPaging="True" AllowCustomPaging="True" OnItemDataBound="dgDelvPath_ItemDataBound"
				OnCancelCommand="dgDelvPath_Cancel" OnUpdateCommand="dgDelvPath_Update" OnEditCommand="dgDelvPath_Edit">
				<ItemStyle Height="20px" CssClass="gridfield"></ItemStyle>
				<Columns>
					<asp:EditCommandColumn ButtonType="LinkButton" UpdateText="&lt;IMG SRC='images/butt-update.gif' border=0 title='Update' &gt;"
						CancelText="&lt;IMG SRC='images/butt-cancel.gif' border=0 title='Cancel' &gt;" EditText="&lt;IMG SRC='images/butt-edit.gif' border=0 title='Edit' &gt;">
						<HeaderStyle CssClass="gridHeading"></HeaderStyle>
						<ItemStyle HorizontalAlign="Left"></ItemStyle>
					</asp:EditCommandColumn>
					<asp:ButtonColumn Text="&lt;IMG SRC='images/butt-delete.gif' border=0 title='Delete' &gt;" CommandName="Delete">
						<HeaderStyle CssClass="gridHeading"></HeaderStyle>
						<ItemStyle HorizontalAlign="Left"></ItemStyle>
					</asp:ButtonColumn>
					<asp:TemplateColumn HeaderText="Path Code">
						<HeaderStyle Font-Bold="True" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle CssClass="gridField"></ItemStyle>
						<ItemTemplate>
							<asp:Label ID="lblPathCode" CssClass="gridLabel" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"path_code")%>'>
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<cc1:mstextbox CssClass="gridTextBox" ID="txtPathCode" Text='<%#DataBinder.Eval(Container.DataItem,"path_code")%>' Runat="server" Enabled="True" TextMaskType="msUpperAlfaNumeric" MaxLength="10">
							</cc1:mstextbox>
							<asp:RequiredFieldValidator ID="odcValidator" Runat="server" BorderWidth="0" Display="None" ControlToValidate="txtPathCode"
								ErrorMessage="Path Code "></asp:RequiredFieldValidator>
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Path Description">
						<HeaderStyle CssClass="gridHeading"></HeaderStyle>
						<ItemStyle CssClass="gridField"></ItemStyle>
						<ItemTemplate>
							<asp:Label ID="lblPathDescp" CssClass="gridLabel" Text='<%#DataBinder.Eval(Container.DataItem,"path_description")%>' Runat=server >
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<asp:TextBox ID="txtPathDescp" CssClass="gridTextBox" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"path_description")%>' MaxLength="200">
							</asp:TextBox>
							<asp:RequiredFieldValidator ID="rvPathDescp" Runat="server" BorderWidth="0" Display="None" ControlToValidate="txtPathDescp"
								ErrorMessage="Path Description "></asp:RequiredFieldValidator>
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Delivery Type">
						<HeaderStyle CssClass="gridHeading"></HeaderStyle>
						<ItemStyle CssClass="gridField"></ItemStyle>
						<ItemTemplate>
							<asp:DropDownList ID="DrpDlvType" CssClass="gridTextBox" Runat="server" AutoPostBack="True" OnSelectedIndexChanged="chkDrpDlvType"
								style="width:180px"></asp:DropDownList>
						</ItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Line Haul Cost">
						<HeaderStyle CssClass="gridHeading"></HeaderStyle>
						<ItemStyle CssClass="gridField"></ItemStyle>
						<ItemTemplate>
							<asp:Label ID="lblLnHlCst" CssClass="gridLabelNumber" Text='<%#DataBinder.Eval(Container.DataItem,"line_haul_cost")%>' Runat=server>
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<cc1:mstextbox id="txtLnHlCst" CssClass="gridTextBox" Text='<%#DataBinder.Eval(Container.DataItem,"line_haul_cost")%>' TextMaskType="msNumeric" Runat="server" NumberMaxValue="100000" NumberScale="2" NumberPrecision="6" NumberMinValue="0">
							</cc1:mstextbox>
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Origin Station">
						<HeaderStyle CssClass="gridheading"></HeaderStyle>
						<ItemStyle CssClass="gridField"></ItemStyle>
						<ItemTemplate>
							<asp:Label ID="lblOrgStat" CssClass="gridlabel" Width="5%" Runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"origin_station")%>'>
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<asp:TextBox ID="txtOrgStat" CssClass="gridTextBox" Runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"origin_station")%>' MaxLength="12">
							</asp:TextBox>
							<asp:RequiredFieldValidator ID="rvOrgStat" Runat="server" BorderWidth="0" Display="None" ControlToValidate="txtOrgStat"
								ErrorMessage="Origin Station "></asp:RequiredFieldValidator>
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Destination Station">
						<HeaderStyle CssClass="gridheading"></HeaderStyle>
						<ItemStyle CssClass="gridField"></ItemStyle>
						<ItemTemplate>
							<asp:Label ID="lblDestStation" CssClass="gridlabel" Width="5%" Runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"destination_station")%>'>
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<asp:TextBox ID="txtDestStat" CssClass="gridTextBox" Runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"destination_station")%>' MaxLength="12">
							</asp:TextBox>
							<asp:RequiredFieldValidator ID="rvDestStat" Runat="server" BorderWidth="0" Display="None" ControlToValidate="txtDestStat"
								ErrorMessage="Destination Station "></asp:RequiredFieldValidator>
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Flight/Vehicle No">
						<HeaderStyle CssClass="gridheading"></HeaderStyle>
						<ItemStyle CssClass="gridField"></ItemStyle>
						<ItemTemplate>
							<asp:Label ID="lblFlightVehicle" CssClass="gridlabel" Width="5%" Runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"flight_vehicle_no")%>'>
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<asp:TextBox ID="txtFlightVehicle" CssClass="gridTextBox" Runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"flight_vehicle_no")%>' MaxLength="12">
							</asp:TextBox>
							<asp:RequiredFieldValidator ID="rvFlightVehicle" Runat="server" BorderWidth="0" Display="None" ControlToValidate="txtFlightVehicle"
								ErrorMessage="Flight/Vehicle No "></asp:RequiredFieldValidator>
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="AWB/Driver Name">
						<HeaderStyle CssClass="gridheading"></HeaderStyle>
						<ItemStyle CssClass="gridField"></ItemStyle>
						<ItemTemplate>
							<asp:Label ID="lblAWBDriver" CssClass="gridlabel" Width="5%" Runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"awb_driver_name")%>'>
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<asp:TextBox ID="txtAWBDriver" CssClass="gridTextBox" Runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"awb_driver_name")%>' MaxLength="12">
							</asp:TextBox>
							<asp:RequiredFieldValidator ID="rvAWBDriver" Runat="server" BorderWidth="0" Display="None" ControlToValidate="txtAWBDriver"
								ErrorMessage="AWB/Driver Name "></asp:RequiredFieldValidator>
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Approximate Departure Time">
						<HeaderStyle CssClass="gridheading"></HeaderStyle>
						<ItemStyle HorizontalAlign="Right" CssClass="gridField"></ItemStyle>
						<ItemTemplate>
							<asp:Label ID="lblApproxDepart" CssClass="gridlabel" Width="5%" Runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"departure_time","{0:HH:mm}")%>'>
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<cc1:mstextbox CssClass="gridTextBoxNumber" ID="txtApproxDepart" Visible=True Text='<%#DataBinder.Eval(Container.DataItem,"departure_time","{0:HH:mm}")%>' Runat="server" TextMaskString="99:99" TextMaskType="msTime">
							</cc1:mstextbox>
							<asp:RequiredFieldValidator ID="rvApproxDepart" Runat="server" BorderWidth="0" Display="None" ControlToValidate="txtApproxDepart"
								ErrorMessage="Approximate Departure Time "></asp:RequiredFieldValidator>
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Active">
						<HeaderStyle CssClass="gridHeading"></HeaderStyle>
						<ItemStyle CssClass="gridField"></ItemStyle>
						<ItemTemplate>
							<asp:DropDownList id="DrpActive" Width="50" CssClass="gridTextBox" OnSelectedIndexChanged="chkDrpDlvType"
								Runat="server" AutoPostBack="True">
								<asp:ListItem Value="Y">Yes</asp:ListItem>
								<asp:ListItem Value="N">No</asp:ListItem>
							</asp:DropDownList>
						</ItemTemplate>
					</asp:TemplateColumn>
				</Columns>
				<PagerStyle NextPageText="Next" PrevPageText="Previous" HorizontalAlign="Right" CssClass="normalText"></PagerStyle>
			</asp:datagrid><asp:validationsummary id="ValidationSummary1" style="Z-INDEX: 111; LEFT: 87px; POSITION: absolute; TOP: 97px"
				Height="36px" Width="346px" Runat="server" HeaderText="The following field(s) are required:<br><br>" ShowMessageBox="True"
				DisplayMode="BulletList" ShowSummary="False"></asp:validationsummary></form>
	</body>
</HTML>
