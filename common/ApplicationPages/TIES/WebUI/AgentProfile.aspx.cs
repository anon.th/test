using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.common.classes;
using com.common.util;
using com.ties.DAL;
using com.ties.classes;
using com.common.applicationpages;

namespace com.ties
{
	/// <summary>
	/// Summary description for AgentProfile.
	/// </summary>
	public class AgentProfile : BasePage
	{
		protected System.Web.UI.WebControls.Button btnQry;
		protected System.Web.UI.WebControls.Button btnExecQry;
		protected System.Web.UI.WebControls.Button btnSave;
		protected System.Web.UI.WebControls.Button btnDelete;
		protected System.Web.UI.WebControls.Button btnGoToFirstPage;
		protected System.Web.UI.WebControls.Button btnPreviousPage;
		protected System.Web.UI.WebControls.Button btnNextPage;
		protected System.Web.UI.WebControls.Button btnGoToLastPage;
		protected System.Web.UI.WebControls.TextBox txtCountRec;
		protected System.Web.UI.WebControls.Label lblCPErrorMessage;


		protected System.Web.UI.HtmlControls.HtmlTable AgentInfoTable;
		protected Microsoft.Web.UI.WebControls.TabStrip TabStrip1;

		SessionDS m_sdsAgentProfile = null;
		SessionDS m_sdsReference = null;
		SessionDS m_sdsStatusCode = null;
		SessionDS m_sdsExceptionCode = null;		
		SessionDS m_sdsQuotation=null;						// 12/11/2002 Mohan, For Quotation Tab Page		
		SessionDS m_sdsZoneCode=null;						// 28/11/2002 Mohan, for Agent Zipcode and Cost
		SessionDS m_sdsAgentZoneZip=null;

		protected System.Web.UI.WebControls.DataGrid dgStatusCodes;
		protected System.Web.UI.WebControls.DataGrid dgExceptionCodes;
		// 13/11/2002
		protected System.Web.UI.WebControls.DataGrid dgQuotation;
		
		protected Microsoft.Web.UI.WebControls.MultiPage agentProfileMultiPage;

		// Utility utility = null;

		DataView m_dvApplyDimWtOptions = null;
		DataView m_dvPODSlipRequiredOptions = null;
		DataView m_dvApplyESASurchargeOptions = null;
		protected System.Web.UI.WebControls.ValidationSummary agentValidationSummary;
		DataView m_dvStatusOptions = null;
		DataView m_dvMBGOptions = null;

		protected System.Web.UI.WebControls.Label lblMainTitle;
		protected System.Web.UI.WebControls.Label lblNumRec;
		protected System.Web.UI.WebControls.Label lblNumRefRec;
		protected System.Web.UI.WebControls.Button btnSCInsert;
		protected System.Web.UI.WebControls.Button btnExInsert;
		protected System.Web.UI.WebControls.Button btnInsert;

		protected System.Web.UI.WebControls.Label lblSCMessage;
		protected System.Web.UI.WebControls.Label lblEXMessage;
		
		protected System.Web.UI.WebControls.ValidationSummary custValidationSummary;

		// 28/11/2002
		protected System.Web.UI.WebControls.DataGrid dgAgentZonecodes;
		protected System.Web.UI.WebControls.Label lblZoneMessage;
		protected System.Web.UI.WebControls.Label lblAgentZoneZipMessage;
		protected System.Web.UI.WebControls.Button btnZipcodeViewAll;
		protected System.Web.UI.WebControls.Button btnZipcodeInsert;
		protected System.Web.UI.WebControls.Button btnAgentCostInsert;
		protected System.Web.UI.WebControls.DataGrid dgZoneZipcodIncluded;
		protected System.Web.UI.WebControls.Label lblZipcodeRecCnt;
		protected System.Web.UI.WebControls.Label lblAgentCostRecCnt;


		static private int m_iSetSize = 4;
		static private int m_iQuoteSize=7;

		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			// utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings,Page.Session);
			//lblMainTitle.Text = ShowLocalizedLabel();
			

			if(!Page.IsPostBack)
			{
				Utility.EnableButton(ref btnSave,ButtonType.Save,true,m_moduleAccessRights);
				Utility.EnableButton(ref btnDelete,ButtonType.Delete,true,m_moduleAccessRights);
				ViewState["APOperation"] = Operation.None;
				ViewState["SCOperation"] = Operation.None;
				ViewState["EXMode"] = ScreenMode.None;
				ViewState["EXOperation"] = Operation.None;
				//13/11/2002
				ViewState["currentQuoteSet"] = 0;
				m_sdsQuotation = AgentProfileMgrDAL.GetEmptyQuotation(1);								
				BindQuotation();
				Session["SESSION_DS5"] = m_sdsQuotation;				
				/* for Quotation*/
				ResetScreenForQuery();
				// 02/12/2002 
				ViewState["ZoneZipMode"] = ScreenMode.None;
				ViewState["ZoneZipOperation"] = Operation.None;
				
			}
			else
			{
				//ViewState["currentQuoteSet"] = 0;
				if(Session["SESSION_DS1"] != null)
				{
					m_sdsAgentProfile = (SessionDS)Session["SESSION_DS1"];
				}
				if(Session["SESSION_DS2"] != null)
				{
					m_sdsReference = (SessionDS)Session["SESSION_DS2"];
				}
				if(Session["SESSION_DS3"] != null)
				{
					m_sdsStatusCode = (SessionDS)Session["SESSION_DS3"];
				}			
				if(Session["SESSION_DS5"] != null)					// 13/11/2002
				{
					m_sdsQuotation = (SessionDS)Session["SESSION_DS5"];
				}
				if(Session["SESSION_DS4"] != null)
				{
					m_sdsExceptionCode = (SessionDS)Session["SESSION_DS4"];
				}
				if(Session["SESSION_DS6"] != null)					// 02/12/2002
				{
					m_sdsZoneCode  = (SessionDS)Session["SESSION_DS6"];
				}
				if(Session["SESSION_DS7"] != null)
				{
					m_sdsAgentZoneZip  = (SessionDS)Session["SESSION_DS7"];
				}
				if(Session["ZoneZipCode"] != null)
				{
					lblAgentZoneZipMessage.Text="";
					ShowCurrentZoneZipPage();
					ViewState["ZoneZipMode"] = ScreenMode.ExecuteQuery;
				}
			}

			lblNumRefRec = (Label)agentProfileMultiPage.FindControl("lblNumRefRec");
			dgStatusCodes = (DataGrid)agentProfileMultiPage.FindControl("dgStatusCodes");
			dgExceptionCodes = (DataGrid)agentProfileMultiPage.FindControl("dgExceptionCodes");
			custValidationSummary.HeaderText = Utility.GetLanguageText(ResourceType.UserMessage, "PLS_ENTER_MISS_FIELD", utility.GetUserCulture());
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			base.OnInit(e);
			InitializeComponent();
			addAgentButtonEventHandler();
			addChangeEventHandlers();
			addZipCodePopupButtonEventHandler();
			addRefChangeEventHandlers();
			addRefButtonEventHandlers();
			
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnQry.Click += new System.EventHandler(this.btnQry_Click);
			this.btnExecQry.Click += new System.EventHandler(this.btnExecQry_Click);
			this.btnInsert.Click += new System.EventHandler(this.btnInsert_Click);
			this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
			this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
			this.btnGoToFirstPage.Click += new System.EventHandler(this.btnGoToFirstPage_Click);
			this.btnPreviousPage.Click += new System.EventHandler(this.btnPreviousPage_Click);
			this.btnNextPage.Click += new System.EventHandler(this.btnNextPage_Click);
			this.btnGoToLastPage.Click += new System.EventHandler(this.btnGoToLastPage_Click);
			this.txtCountRec.TextChanged += new System.EventHandler(this.txtCountRec_TextChanged);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private void addAgentButtonEventHandler()
		{
			Button btnSearchSalesman = (Button)agentProfileMultiPage.FindControl("btnSearchSalesman");
			if(btnSearchSalesman != null)
			{
				btnSearchSalesman.Click += new System.EventHandler(this.btnSearchSalesman_Click);
			}

		}
		private void addChangeEventHandlers()
		{
			msTextBox txtAccNo = (msTextBox)agentProfileMultiPage.FindControl("txtAccNo");
			if(txtAccNo != null)
			{
				txtAccNo.TextChanged += new System.EventHandler(this.txtAccNo_TextChanged);
			}

			msTextBox txtRefNo = (msTextBox)agentProfileMultiPage.FindControl("txtRefNo");
			if(txtRefNo != null)
			{
				txtRefNo.TextChanged += new System.EventHandler(this.txtRefNo_TextChanged);
			}

			TextBox txtAgentName = (TextBox)agentProfileMultiPage.FindControl("txtAgentName");
			if(txtAgentName != null)
			{
				txtAgentName.TextChanged += new System.EventHandler(this.txtAgentName_TextChanged);
			}
			//-- 20/12/2002
			TextBox txtContactPerson = (TextBox)agentProfileMultiPage.FindControl("txtContactPerson");
			if(txtContactPerson != null)
			{
				txtContactPerson.TextChanged += new System.EventHandler(this.txtContactPerson_TextChanged);
			}

			TextBox txtAddress1 = (TextBox)agentProfileMultiPage.FindControl("txtAddress1");
			if(txtAddress1 != null)
			{
				txtAddress1.TextChanged += new System.EventHandler(this.txtAddress1_TextChanged);
			}

			TextBox txtAddress2 = (TextBox)agentProfileMultiPage.FindControl("txtAddress2");
			if(txtAddress2 != null)
			{
				txtAddress2.TextChanged += new System.EventHandler(this.txtAddress2_TextChanged);
			}

			TextBox txtCountry = (TextBox)agentProfileMultiPage.FindControl("txtCountry");
			if(txtCountry != null)
			{
				txtCountry.TextChanged += new System.EventHandler(this.txtCountry_TextChanged);
			}

			msTextBox txtZipCode = (msTextBox)agentProfileMultiPage.FindControl("txtZipCode");
			if(txtZipCode != null)
			{
				txtZipCode.TextChanged += new System.EventHandler(this.txtZipCode_TextChanged);
			}

			TextBox txtTelephone = (TextBox)agentProfileMultiPage.FindControl("txtTelephone");
			if(txtTelephone != null)
			{
				txtTelephone.TextChanged += new System.EventHandler(this.txtTelephone_TextChanged);
			}
			
			TextBox txtFax = (TextBox)agentProfileMultiPage.FindControl("txtFax");
			if(txtFax != null)
			{
				txtFax.TextChanged += new System.EventHandler(this.txtFax_TextChanged);
			}

			msTextBox txtActiveQuotationNo = (msTextBox)agentProfileMultiPage.FindControl("txtActiveQuotationNo");
			if(txtActiveQuotationNo != null)
			{
				txtActiveQuotationNo.TextChanged += new System.EventHandler(this.txtActiveQuotationNo_TextChanged);
			}

			TextBox txtRemark = (TextBox)agentProfileMultiPage.FindControl("txtRemark");
			if(txtRemark != null)
			{
				txtRemark.TextChanged += new System.EventHandler(this.txtRemark_TextChanged);
			}
			
			msTextBox txtFreeInsuranceAmt = (msTextBox)agentProfileMultiPage.FindControl("txtFreeInsuranceAmt");
			if(txtFreeInsuranceAmt != null)
			{
				txtFreeInsuranceAmt.TextChanged += new System.EventHandler(this.txtFreeInsuranceAmt_TextChanged);
			}

			msTextBox txtInsurancePercentSurcharge = (msTextBox)agentProfileMultiPage.FindControl("txtInsurancePercentSurcharge");
			if(txtInsurancePercentSurcharge != null)
			{
				txtInsurancePercentSurcharge.TextChanged += new System.EventHandler(this.txtInsurancePercentSurcharge_TextChanged);
			}

			TextBox txtEmail = (TextBox)agentProfileMultiPage.FindControl("txtEmail");
			if(txtEmail != null)
			{
				txtEmail.TextChanged += new System.EventHandler(this.txtEmail_TextChanged);
			}

//			TextBox txtCreatedBy = (TextBox)agentProfileMultiPage.FindControl("txtCreatedBy");
//
//			if(txtCreatedBy != null)
//			{
//				txtCreatedBy.TextChanged += new System.EventHandler(this.txtCreatedBy_TextChanged);
//			}

			DropDownList ddlApplyDimWt = (DropDownList)agentProfileMultiPage.FindControl("ddlApplyDimWt");			
			if(ddlApplyDimWt != null)
			{
				ddlApplyDimWt.SelectedIndexChanged += new System.EventHandler(this.ddlApplyDimWt_SelectedIndexChanged);
			}

			DropDownList ddlPodSlipRequired = (DropDownList)agentProfileMultiPage.FindControl("ddlPodSlipRequired");			
			if(ddlPodSlipRequired != null)
			{
				ddlPodSlipRequired.SelectedIndexChanged += new System.EventHandler(this.ddlPodSlipRequired_SelectedIndexChanged);
			}

			DropDownList ddlApplyEsaSurcharge = (DropDownList)agentProfileMultiPage.FindControl("ddlApplyEsaSurcharge");			
			if(ddlApplyEsaSurcharge != null)
			{
				ddlApplyEsaSurcharge.SelectedIndexChanged += new System.EventHandler(this.ddlApplyEsaSurcharge_SelectedIndexChanged);
			}

			DropDownList ddlStatus = (DropDownList)agentProfileMultiPage.FindControl("ddlStatus");			
			if(ddlStatus != null)
			{
				ddlStatus.SelectedIndexChanged += new System.EventHandler(this.ddlStatus_SelectedIndexChanged);
			}
			//Mohan, 21/11/02

			TextBox txtSalesmanID = (TextBox)agentProfileMultiPage.FindControl("txtSalesmanID");
			if(txtSalesmanID != null)
			{
				txtSalesmanID.TextChanged += new System.EventHandler(this.txtSalesmanID_TextChanged);
			}

			msTextBox txtCreditTerm = (msTextBox)agentProfileMultiPage.FindControl("txtCreditTerm");
			if(txtCreditTerm != null)
			{
				txtCreditTerm.TextChanged += new System.EventHandler(this.txtCreditTerm_TextChanged);
			}

			msTextBox txtCreditLimit = (msTextBox)agentProfileMultiPage.FindControl("txtCreditLimit");
			if(txtCreditLimit != null)
			{
				txtCreditLimit.TextChanged += new System.EventHandler(this.txtCreditLimit_TextChanged);
			}

			msTextBox txtActiveQuatationNo = (msTextBox)agentProfileMultiPage.FindControl("txtActiveQuatationNo");
			if(txtActiveQuatationNo != null)
			{
				txtActiveQuatationNo.TextChanged += new System.EventHandler(this.txtActiveQuatationNo_TextChanged);
			}

			msTextBox txtCreditOutstanding = (msTextBox)agentProfileMultiPage.FindControl("txtCreditOutstanding");
			if(txtCreditOutstanding != null)
			{
				txtCreditOutstanding.TextChanged += new System.EventHandler(this.txtCreditOutstanding_TextChanged);
			}
			
			RadioButton radioBtnCash = (RadioButton)agentProfileMultiPage.FindControl("radioBtnCash");
			if(radioBtnCash != null)
			{
				radioBtnCash.CheckedChanged += new System.EventHandler(this.radioBtnCash_CheckedChanged);
			}

			RadioButton radioBtnCredit = (RadioButton)agentProfileMultiPage.FindControl("radioBtnCredit");
			if(radioBtnCredit != null)
			{
				radioBtnCredit.CheckedChanged += new System.EventHandler(this.radioBtnCredit_CheckedChanged);
			}

			DropDownList ddlMBG = (DropDownList)agentProfileMultiPage.FindControl("ddlMBG");
			if(ddlMBG != null)
			{
				ddlMBG.SelectedIndexChanged += new System.EventHandler(this.ddlMBG_SelectedIndexChanged);
			}
		}

		private void addZipCodePopupButtonEventHandler()
		{
			Button btnZipcodePopup = (Button)agentProfileMultiPage.FindControl("btnZipcodePopup");
			if(btnZipcodePopup != null)
			{
				btnZipcodePopup.Click += new System.EventHandler(this.btnZipcodePopup_Click);
			}

			Button btnRefZipcodeSearch = (Button)agentProfileMultiPage.FindControl("btnRefZipcodeSearch");
			if(btnRefZipcodeSearch != null)
			{
				btnRefZipcodeSearch.Click += new System.EventHandler(this.btnRefZipcodeSearch_Click);
			}

			Button btnISectorSearch = (Button)agentProfileMultiPage.FindControl("btnISectorSearch");
			if(btnISectorSearch != null)
			{
				btnISectorSearch.Click += new System.EventHandler(this.btnISectorSearch_Click);
			}			
		}

		private void addRefChangeEventHandlers()
		{
			TextBox txtRefSndRecName = (TextBox)agentProfileMultiPage.FindControl("txtRefSndRecName");
			if(txtRefSndRecName != null)
			{
				txtRefSndRecName.TextChanged += new System.EventHandler(this.txtRefSndRecName_TextChanged);
			}

			TextBox txtRefContactPerson = (TextBox)agentProfileMultiPage.FindControl("txtRefContactPerson");
			if(txtRefContactPerson != null)
			{
				txtRefContactPerson.TextChanged += new System.EventHandler(this.txtRefContactPerson_TextChanged);
			}

			TextBox txtRefAddress1 = (TextBox)agentProfileMultiPage.FindControl("txtRefAddress1");
			if(txtRefAddress1 != null)
			{
				txtRefAddress1.TextChanged += new System.EventHandler(this.txtRefAddress1_TextChanged);
			}

			TextBox txtRefAddress2 = (TextBox)agentProfileMultiPage.FindControl("txtRefAddress2");
			if(txtRefAddress2 != null)
			{
				txtRefAddress2.TextChanged += new System.EventHandler(this.txtRefAddress2_TextChanged);
			}

			TextBox txtRefCountry = (TextBox)agentProfileMultiPage.FindControl("txtRefCountry");
			if(txtRefCountry != null)
			{
				txtRefCountry.TextChanged += new System.EventHandler(this.txtRefCountry_TextChanged);
			}

			msTextBox txtRefZipcode = (msTextBox)agentProfileMultiPage.FindControl("txtRefZipcode");
			if(txtRefZipcode != null)
			{
				txtRefZipcode.TextChanged += new System.EventHandler(this.txtRefZipcode_TextChanged);
			}

			TextBox txtRefTelephone = (TextBox)agentProfileMultiPage.FindControl("txtRefTelephone");
			if(txtRefTelephone != null)
			{
				txtRefTelephone.TextChanged += new System.EventHandler(this.txtRefTelephone_TextChanged);
			}

			TextBox txtRefFax = (TextBox)agentProfileMultiPage.FindControl("txtRefFax");
			if(txtRefFax != null)
			{
				txtRefFax.TextChanged += new System.EventHandler(this.txtRefFax_TextChanged);
			}

			CheckBox chkSender = (CheckBox)agentProfileMultiPage.FindControl("chkSender");
			if(chkSender != null)
			{
				chkSender.CheckedChanged += new System.EventHandler(this.chkSender_CheckedChanged);
			}

			CheckBox chkRecipient = (CheckBox)agentProfileMultiPage.FindControl("chkRecipient");
			if(chkRecipient != null)
			{
				chkRecipient.CheckedChanged += new System.EventHandler(this.chkRecipient_CheckedChanged);
			}

			TextBox txtRefEmail = (TextBox)agentProfileMultiPage.FindControl("txtRefEmail");
			if(txtRefEmail != null)
			{
				txtRefEmail.TextChanged += new System.EventHandler(this.txtRefEmail_TextChanged);
			}

		}

//from Customer Profile.aspx.cs
		public void dgStatusCodes_Delete(object sender, DataGridCommandEventArgs e)
		{
			msTextBox txtStatusCode = (msTextBox)e.Item.FindControl("txtStatusCode");
			String strAgentID = (String)ViewState["currentAgent"];

			Label lblStatusCode = (Label)e.Item.FindControl("lblStatusCode");
			String strStatusCode = null;
			if(txtStatusCode != null)
			{
				strStatusCode = txtStatusCode.Text;
			}

			if(lblStatusCode != null)
			{
				strStatusCode = lblStatusCode.Text;
			}

			try
			{
				AgentProfileMgrDAL.DeleteCPStatusCodeDS(utility.GetAppID(),utility.GetEnterpriseID(),strAgentID,strStatusCode);
			}
			catch(ApplicationException appException)
			{
				String strMsg = appException.Message;
				if(strMsg.IndexOf("FK") != -1 || strMsg.IndexOf("child record") !=-1)
				{
					lblSCMessage.Text =Utility.GetLanguageText(ResourceType.UserMessage,"ERR_DEL_STATUS",utility.GetUserCulture()); 
					
				}
				return;
			}

			if((int)ViewState["SCMode"] == (int)ScreenMode.Insert)
			{
				m_sdsStatusCode.ds.Tables[0].Rows.RemoveAt(e.Item.ItemIndex);
				m_sdsStatusCode.QueryResultMaxSize--;
				dgStatusCodes.CurrentPageIndex = 0;

				if((int) ViewState["SCOperation"] == (int)Operation.Insert && dgStatusCodes.EditItemIndex > 0)
				{
					m_sdsStatusCode.ds.Tables[0].Rows.RemoveAt(dgStatusCodes.EditItemIndex-1);
					m_sdsStatusCode.QueryResultMaxSize--;
				}
				dgStatusCodes.EditItemIndex = -1;
				dgStatusCodes.SelectedIndex = -1;
				BindSCGrid();
				ResetDetailsGrid();
			}
			else
			{
				ShowCurrentSCPage();
			}

			Logger.LogDebugInfo("StatusException","dgStatusCodes_Delete","INF004","Deleted row in Status_Code table..");
			ViewState["SCOperation"] = Operation.None;
			btnSCInsert.Enabled = true;
		}

		public void dgStatusCodes_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			Label lblStatusCode = (Label)dgStatusCodes.SelectedItem.FindControl("lblStatusCode");
			msTextBox txtStatusCode = (msTextBox)dgStatusCodes.SelectedItem.FindControl("txtStatusCode");
			String strStatusCode = null;

			if(lblStatusCode != null)
			{
				strStatusCode = lblStatusCode.Text;
			}

			if(txtStatusCode != null)
			{
				strStatusCode = txtStatusCode.Text;
			}

			ViewState["CurrentSC"] = strStatusCode;
			dgExceptionCodes.CurrentPageIndex = 0;

			Logger.LogDebugInfo("StatusException","dgStatusCodes_SelectedIndexChanged","INF004","updating data grid..."+dgStatusCodes.SelectedIndex+"  : "+strStatusCode);
			ShowCurrentEXPage();

			ViewState["EXMode"] = ScreenMode.ExecuteQuery;

			if((int)ViewState["SCMode"] != (int)ScreenMode.Insert && (int)ViewState["SCOperation"] != (int)Operation.Insert)
			{
				btnSCInsert.Enabled = true;
			}

			if((int)ViewState["SCMode"] == (int)ScreenMode.Insert && dgStatusCodes.EditItemIndex == dgStatusCodes.SelectedIndex && (int)ViewState["SCOperation"] == (int)Operation.Insert)
			{
				btnExInsert.Enabled = false;
			}
			else
			{
				btnExInsert.Enabled = true;
			}
			lblSCMessage.Text = "";

		}

		private void addRefButtonEventHandlers()
		{
			Button btnRefViewAll = (Button)agentProfileMultiPage.FindControl("btnRefViewAll");
			if(btnRefViewAll != null)
			{
				btnRefViewAll.Click += new System.EventHandler(this.btnRefViewAll_Click);
			}

			Button btnRefInsert = (Button)agentProfileMultiPage.FindControl("btnRefInsert");
			if(btnRefInsert != null)
			{
				btnRefInsert.Click += new System.EventHandler(this.btnRefInsert_Click);
			}

			Button btnRefSave = (Button)agentProfileMultiPage.FindControl("btnRefSave");
			if(btnRefSave != null)
			{
				btnRefSave.Click += new System.EventHandler(this.btnRefSave_Click);
			}

			Button btnRefDelete = (Button)agentProfileMultiPage.FindControl("btnRefDelete");
			if(btnRefDelete != null)
			{
				btnRefDelete.Click += new System.EventHandler(this.btnRefDelete_Click);
			}
			
			Button btnFirstRef = (Button)agentProfileMultiPage.FindControl("btnFirstRef");
			if(btnFirstRef != null)
			{
				btnFirstRef.Click += new System.EventHandler(this.btnFirstRef_Click);
			}
			
			Button btnPreviousRef = (Button)agentProfileMultiPage.FindControl("btnPreviousRef");
			if(btnPreviousRef != null)
			{
				btnPreviousRef.Click += new System.EventHandler(this.btnPreviousRef_Click);
			}
			
			Button btnNextRef = (Button)agentProfileMultiPage.FindControl("btnNextRef");
			if(btnNextRef != null)
			{
				btnNextRef.Click += new System.EventHandler(this.btnNextRef_Click);
			}
			
			Button btnLastRef = (Button)agentProfileMultiPage.FindControl("btnLastRef");
			if(btnLastRef != null)
			{
				btnLastRef.Click += new System.EventHandler(this.btnLastRef_Click);
			}
/* 13/12/2002 Mohan */
			Button btnQuoteViewAll = (Button)agentProfileMultiPage.FindControl("btnQuoteViewAll");
			if(btnQuoteViewAll != null)
			{
				btnQuoteViewAll.Click += new System.EventHandler(this.btnQuoteViewAll_Click);
			}
		}


		private void txtCustStateCode_TextChanged(object sender, System.EventArgs e)
		{
		
		}

		private void btnQry_Click(object sender, System.EventArgs e)
		{
			ResetScreenForQuery();
		}

		private void ResetScreenForQuery()
		{
			btnExecQry.Enabled = true;
			m_sdsAgentProfile = AgentProfileMgrDAL.GetEmptyAgentProfileDS(1);
			Session["SESSION_DS1"] = m_sdsAgentProfile;			
			m_sdsReference = AgentProfileMgrDAL.GetEmptyReferenceDS(1);			// 11/11/2002 
			Session["SESSION_DS2"] = m_sdsReference;			
			m_sdsStatusCode = AgentProfileMgrDAL.GetEmptyCPStatusCodeDS(1);
			Session["SESSION_DS3"] = m_sdsStatusCode;			
			m_sdsExceptionCode = AgentProfileMgrDAL.GetEmptyExceptionCodeDS(1);
			Session["SESSION_DS4"] = m_sdsExceptionCode;			
			m_sdsQuotation = AgentProfileMgrDAL.GetEmptyQuotation(1);			// 13/11/2002 
			Session["SESSION_DS5"] = m_sdsQuotation;
			m_sdsZoneCode = AgentProfileMgrDAL.GetEmptyAgentZoneDS(1);		// 02/12/2002
			Session["SESSION_DS6"] = m_sdsZoneCode;			
			m_sdsAgentZoneZip = AgentProfileMgrDAL.GetEmptyAgentZoneZipDS(1);
			Session["SESSION_DS7"] = m_sdsAgentZoneZip;			

			//Reset the Created By field
			TextBox txtCreatedBy = (TextBox)agentProfileMultiPage.FindControl("txtCreatedBy");
			txtCreatedBy.Text="";

			ViewState["APMode"] = ScreenMode.Query;
			ViewState["APOperation"] = Operation.None;
			ViewState["IsTextChanged"] = false;
			ViewState["currentPage"] = 0;

			Utility.EnableButton(ref btnInsert,ButtonType.Insert,true,m_moduleAccessRights);
			btnExecQry.Enabled = true;
			
			LoadComboLists();
			//EnableTabs(true,true,false,false,false,false);
			EnableTabs(true,false,false,false,false);
			DisplayCurrentPage();
			lblNumRec.Text = "";
			lblNumRefRec.Text = "";
			lblCPErrorMessage.Text = "";
			TabStrip1.SelectedIndex = 0;
		}


		private void btnExecQry_Click(object sender, System.EventArgs e)
		{			
			FillAgentProfileDataRow(0);

			ViewState["QUERY_DS"] = m_sdsAgentProfile.ds;

			ViewState["APMode"] = ScreenMode.ExecuteQuery;
			ViewState["APOperation"] = Operation.None;
			ViewState["IsTextChanged"] = false;
			ViewState["currentPage"] = 0;
			ViewState["currentSet"] = 0;
			btnExecQry.Enabled = false;
			GetCPRecSet();
	
			if(m_sdsAgentProfile.QueryResultMaxSize == 0)
			{
				lblCPErrorMessage.Text =Utility.GetLanguageText(ResourceType.UserMessage,"NO_RECORD_FOUND",utility.GetUserCulture());
				EnableNavigationButtons(false,false,false,false);
				return;
			}
			LoadComboLists();
			//EnableTabs(true,true,true,true,true,true);
			EnableTabs(true,true,true,true,true);
			DisplayCurrentPage();
		
			Utility.EnableButton(ref btnInsert,ButtonType.Insert,true,m_moduleAccessRights);

			if(m_sdsAgentProfile != null && m_sdsAgentProfile.DataSetRecSize > 0)
			{
				EnableNavigationButtons(false,false,true,true);
			}
			lblCPErrorMessage.Text = "";
			DisplayRecNum();
			
		}

		private void btnGoToFirstPage_Click(object sender, System.EventArgs e)
		{
			ViewState["currentSet"] = 0;	
			GetCPRecSet();
			ViewState["currentPage"] = 0;
			DisplayCurrentPage();
			DisplayRecNum();
			EnableNavigationButtons(false,false,true,true);
			// 14/11/2002 Mohan, to clear Quotation tab pages			
			dgQuotation.CurrentPageIndex=0; // to avoid Invalid Current Page Index
			ShowQuotationsForCurrentAgent();
			BindQuotation();
		}

		private void btnPreviousPage_Click(object sender, System.EventArgs e)
		{
			int iCurrentPage = Convert.ToInt32(ViewState["currentPage"]);

			if(Convert.ToInt32(ViewState["currentPage"])  > 0 )
			{
				ViewState["currentPage"] = Convert.ToInt32(ViewState["currentPage"]) - 1;
				DisplayCurrentPage();
				// 14/11/2002 Mohan, to clear Quotation tab pages			
				dgQuotation.CurrentPageIndex=0; // to avoid Invalid Current Page Index
				ShowQuotationsForCurrentAgent();
				BindQuotation();
			}
			else
			{
				if( (Convert.ToInt32(ViewState["currentSet"]) - 1) < 0)
				{
					EnableNavigationButtons(false,false,true,true);
					return;
				}
				
				ViewState["currentSet"] = Convert.ToInt32(ViewState["currentSet"]) - 1;	
				GetCPRecSet();
				ViewState["currentPage"] = m_sdsAgentProfile.DataSetRecSize - 1;
				DisplayCurrentPage();
			}
			DisplayRecNum();
			EnableNavigationButtons(true,true,true,true);
			// 14/11/2002 Mohan, to clear Quotation tab pages			
			dgQuotation.CurrentPageIndex=0; // to avoid Invalid Current Page Index
			ShowQuotationsForCurrentAgent();
			BindQuotation();	
		}

		private void btnNextPage_Click(object sender, System.EventArgs e)
		{
			int iCurrentPage = Convert.ToInt32(ViewState["currentPage"]);

			if(Convert.ToInt32(ViewState["currentPage"])  < (m_sdsAgentProfile.DataSetRecSize - 1) )
			{
				ViewState["currentPage"] = Convert.ToInt32(ViewState["currentPage"]) + 1;
				DisplayCurrentPage();
				// 14/11/2002 Mohan, to clear Quotation tab pages			
				dgQuotation.CurrentPageIndex=0; // to avoid Invalid Current Page Index
				ShowQuotationsForCurrentAgent();
				BindQuotation();
			}
			else
			{
				decimal iTotalRec = (Convert.ToInt32(ViewState["currentSet"]) * m_iSetSize) + m_sdsAgentProfile.DataSetRecSize;
				if( iTotalRec ==  m_sdsAgentProfile.QueryResultMaxSize)
				{
					EnableNavigationButtons(true,true,false,false);
					return;
				}
				
				ViewState["currentSet"] = Convert.ToInt32(ViewState["currentSet"]) + 1;	
				GetCPRecSet();
				ViewState["currentPage"] = 0;
				DisplayCurrentPage();
				// 14/11/2002 Mohan, to clear Quotation tab pages			
				dgQuotation.CurrentPageIndex=0; // to avoid Invalid Current Page Index
				ShowQuotationsForCurrentAgent();
				BindQuotation();
			}
			DisplayRecNum();
			EnableNavigationButtons(true,true,true,true);			
		}

		private void btnGoToLastPage_Click(object sender, System.EventArgs e)
		{
			ViewState["currentSet"] = Convert.ToInt32((m_sdsAgentProfile.QueryResultMaxSize - 1))/m_iSetSize;	
			GetCPRecSet();
			ViewState["currentPage"] = m_sdsAgentProfile.DataSetRecSize - 1;
			DisplayCurrentPage();	
			DisplayRecNum();
			EnableNavigationButtons(true,true,false,false);
			// 14/11/2002 Mohan, to clear Quotation tab pages			
			dgQuotation.CurrentPageIndex=0; // to avoid Invalid Current Page Index
			ShowQuotationsForCurrentAgent();
			BindQuotation();
		}

		private void EnableNavigationButtons(bool bMoveFirst, bool bMoveNext,bool bEnableMovePrevious, bool bMoveLast)
		{
			btnGoToFirstPage.Enabled	= bMoveFirst;
			btnPreviousPage.Enabled		= bMoveNext;
			btnNextPage.Enabled			= bEnableMovePrevious;
			btnGoToLastPage.Enabled		= bMoveLast;
		}

		private void GetCPRecSet()
		{
			int iStartIndex = Convert.ToInt32(ViewState["currentSet"]) * m_iSetSize;
			DataSet dsQuery = (DataSet)ViewState["QUERY_DS"];
			m_sdsAgentProfile = AgentProfileMgrDAL.GetAgentProfileDS(utility.GetAppID(),utility.GetEnterpriseID(),iStartIndex,m_iSetSize,dsQuery);
			Session["SESSION_DS1"] = m_sdsAgentProfile;
			decimal pgCnt = (m_sdsAgentProfile.QueryResultMaxSize - 1)/m_iSetSize;
			if(pgCnt < Convert.ToDecimal(ViewState["currentSet"]))
			{
				ViewState["currentSet"] = System.Convert.ToInt32(pgCnt);
			}
		}

		private void DisplayCurrentPage()
		{
			DataRow drCurrent = m_sdsAgentProfile.ds.Tables[0].Rows[Convert.ToInt32(ViewState["currentPage"])];
			msTextBox txtAccNo = (msTextBox)agentProfileMultiPage.FindControl("txtAccNo");
			TextBox txtCustAccDispTab2 = (TextBox)agentProfileMultiPage.FindControl("txtCustAccDispTab2");
			TextBox txtCustAccDispTab3 = (TextBox)agentProfileMultiPage.FindControl("txtCustAccDispTab3");
			TextBox txtCustAccDispTab5 = (TextBox)agentProfileMultiPage.FindControl("txtCustAccDispTab5");
			TextBox txtCustAccDispTab6 = (TextBox)agentProfileMultiPage.FindControl("txtCustAccDispTab6");
						
			if(txtAccNo != null)
			{
				if(drCurrent["agentid"]!= null && (!drCurrent["agentid"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					txtAccNo.Text = drCurrent["agentid"].ToString();
					ViewState["currentAgent"] = txtAccNo.Text; 
				}
				else
				{
					txtAccNo.Text = "";
				}
				EnableTxtAccNo();
			}

			if(txtCustAccDispTab2 != null)
			{
				if(drCurrent["agentid"]!= null && (!drCurrent["agentid"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					txtCustAccDispTab2.Text = drCurrent["agentid"].ToString();
				}
				else
				{
					txtCustAccDispTab2.Text = "";
				}
			}

			if(txtCustAccDispTab3 != null)
			{
				if(drCurrent["agentid"]!= null && (!drCurrent["agentid"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					txtCustAccDispTab3.Text = drCurrent["agentid"].ToString();
				}
				else
				{
					txtCustAccDispTab3.Text = "";
				}
			}

			if(txtCustAccDispTab5 != null)
			{
				if(drCurrent["agentid"]!= null && (!drCurrent["agentid"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					txtCustAccDispTab5.Text = drCurrent["agentid"].ToString();
				}
				else
				{
					txtCustAccDispTab5.Text = "";
				}
			}
			
			if(txtCustAccDispTab6 != null)
			{
				if(drCurrent["agentid"]!= null && (!drCurrent["agentid"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					txtCustAccDispTab6.Text = drCurrent["agentid"].ToString();
				}
				else
				{
					txtCustAccDispTab6.Text = "";
				}
			}

			msTextBox txtRefNo = (msTextBox)agentProfileMultiPage.FindControl("txtRefNo");
			if(txtRefNo != null)
			{
				if(drCurrent["ref_code"]!= null && (!drCurrent["ref_code"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					txtRefNo.Text = drCurrent["ref_code"].ToString() ;
				}
				else
				{
					txtRefNo.Text = "";
				}

			}

			TextBox txtAgentName = (TextBox)agentProfileMultiPage.FindControl("txtAgentName");
			TextBox txtContactPerson = (TextBox)agentProfileMultiPage.FindControl("txtContactPerson");
			TextBox txtCustNameDispTab2 = (TextBox)agentProfileMultiPage.FindControl("txtCustNameDispTab2");
			TextBox txtCustNameDispTab3 = (TextBox)agentProfileMultiPage.FindControl("txtCustNameDispTab3");
			TextBox txtCustNameDispTab5 = (TextBox)agentProfileMultiPage.FindControl("txtCustNameDispTab5");
			TextBox txtCustNameDispTab6 = (TextBox)agentProfileMultiPage.FindControl("txtCustNameDispTab6");

			if(txtAgentName != null)
			{
				if(drCurrent["agent_name"]!= null && (!drCurrent["agent_name"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					txtAgentName.Text = drCurrent["agent_name"].ToString();
				}
				else
				{
					txtAgentName.Text = "";
				}
			}

			if(txtContactPerson != null)
			{
				if(drCurrent["Contact_Person"]!= null && (!drCurrent["Contact_Person"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					txtContactPerson.Text = drCurrent["Contact_Person"].ToString();
				}
				else
				{
					txtContactPerson.Text = "";
				}
			}

			if(txtCustNameDispTab2 != null)
			{
				if(drCurrent["agent_name"]!= null && (!drCurrent["agent_name"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					txtCustNameDispTab2.Text =  drCurrent["agent_name"].ToString();
				}
				else
				{
					txtCustNameDispTab2.Text = "";
				}
			}

			if(txtCustNameDispTab3 != null)
			{
				if(drCurrent["agent_name"]!= null && (!drCurrent["agent_name"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					txtCustNameDispTab3.Text = drCurrent["agent_name"].ToString();
				}
				else
				{
					txtCustNameDispTab3.Text = "";
				}
			}

			if(txtCustNameDispTab5 != null)
			{
				if(drCurrent["agent_name"]!= null && (!drCurrent["agent_name"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					txtCustNameDispTab5.Text = drCurrent["agent_name"].ToString();
				}
				else
				{
					txtCustNameDispTab5.Text = "";
				}
			}

			if(txtCustNameDispTab6 != null)
			{
				if(drCurrent["agent_name"]!= null && (!drCurrent["agent_name"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					txtCustNameDispTab6.Text = drCurrent["agent_name"].ToString();
				}
				else
				{
					txtCustNameDispTab6.Text = "";
				}
			}

			TextBox txtAddress1 = (TextBox)agentProfileMultiPage.FindControl("txtAddress1");
			if(txtAddress1 != null)
			{
				if(drCurrent["agent_address1"]!= null && (!drCurrent["agent_address1"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					txtAddress1.Text = drCurrent["agent_address1"].ToString();
				}
				else
				{
					txtAddress1.Text = "";
				}
			}
			
			TextBox txtAddress2 = (TextBox)agentProfileMultiPage.FindControl("txtAddress2");
			if(txtAddress2 != null)
			{
				if(drCurrent["agent_address2"]!= null && (!drCurrent["agent_address2"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					txtAddress2.Text = drCurrent["agent_address2"].ToString();
				}
				else
				{
					txtAddress2.Text = "";
				}
			}
			
			TextBox txtCountry = (TextBox)agentProfileMultiPage.FindControl("txtCountry");
			if(txtCountry != null)
			{
				if(drCurrent["country"]!= null && (!drCurrent["country"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					txtCountry.Text = drCurrent["country"].ToString();
				}
				else
				{
					txtCountry.Text = "";
				}
			}

			msTextBox txtZipCode = (msTextBox)agentProfileMultiPage.FindControl("txtZipCode");
			TextBox txtState = (TextBox)agentProfileMultiPage.FindControl("txtState");
			if(txtZipCode != null && txtState != null)
			{
				if(drCurrent["zipcode"]!= null && (!drCurrent["zipcode"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					txtZipCode.Text = drCurrent["zipcode"].ToString();
					Zipcode zipcode = new Zipcode();
					zipcode.Populate(utility.GetAppID(),utility.GetEnterpriseID(),txtZipCode.Text);
					zipcode.Populate(utility.GetAppID(),utility.GetEnterpriseID(),zipcode.Country,txtZipCode.Text);
					txtState.Text = zipcode.StateName;
				}
				else
				{
					txtZipCode.Text = "";
					txtState.Text = "";
				}
			}
			
			TextBox txtTelephone = (TextBox)agentProfileMultiPage.FindControl("txtTelephone");
			if(txtTelephone != null)
			{
				if(drCurrent["telephone"]!= null && (!drCurrent["telephone"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					txtTelephone.Text = drCurrent["telephone"].ToString();
				}
				else
				{
					txtTelephone.Text = "";
				}
			}
			
			TextBox txtFax = (TextBox)agentProfileMultiPage.FindControl("txtFax");
			if(txtFax != null)
			{
				if(drCurrent["fax"]!= null && (!drCurrent["fax"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					txtFax.Text = drCurrent["fax"].ToString();
				}
				else
				{
					txtFax.Text = "";
				}
			}
			
			msTextBox txtActiveQuotationNo = (msTextBox)agentProfileMultiPage.FindControl("txtActiveQuotationNo");
			if(txtActiveQuotationNo != null)
			{
				if(drCurrent["active_quotation_no"]!= null && (!drCurrent["active_quotation_no"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					txtActiveQuotationNo.Text = drCurrent["active_quotation_no"].ToString() ;
				}
				else
				{
					txtActiveQuotationNo.Text = "";
				}
			}
			
			TextBox txtRemark = (TextBox)agentProfileMultiPage.FindControl("txtRemark");
			if(txtRemark != null)
			{
				if(drCurrent["remark"]!= null && (!drCurrent["remark"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					txtRemark.Text = drCurrent["remark"].ToString();
				}
				else
				{
					txtRemark.Text = "";
				}
			}
						
			msTextBox txtFreeInsuranceAmt = (msTextBox)agentProfileMultiPage.FindControl("txtFreeInsuranceAmt");
			if(txtFreeInsuranceAmt != null)
			{
				if(drCurrent["free_insurance_amt"]!= null && (!drCurrent["free_insurance_amt"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					decimal decFreeInsuranceAmt = Convert.ToDecimal(drCurrent["free_insurance_amt"]);
					txtFreeInsuranceAmt.Text =decFreeInsuranceAmt.ToString("#0.00");
				}
				else
				{
					txtFreeInsuranceAmt.Text = "";
				}
			}
			
			msTextBox txtInsurancePercentSurcharge = (msTextBox)agentProfileMultiPage.FindControl("txtInsurancePercentSurcharge");
			if(txtInsurancePercentSurcharge != null)
			{
				if(drCurrent["insurance_percent_surcharge"]!= null && (!drCurrent["insurance_percent_surcharge"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					txtInsurancePercentSurcharge.Text = drCurrent["insurance_percent_surcharge"].ToString();
				}
				else
				{
					txtInsurancePercentSurcharge.Text = "";
				}
			}
	
			TextBox txtEmail = (TextBox)agentProfileMultiPage.FindControl("txtEmail");
			if(txtEmail != null)
			{
				if(drCurrent["email"]!= null && (!drCurrent["email"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					txtEmail.Text = drCurrent["email"].ToString();
				}
				else
				{
					txtEmail.Text = "";
				}
			}

			TextBox txtCreatedBy = (TextBox)agentProfileMultiPage.FindControl("txtCreatedBy");
			if(txtCreatedBy != null)
			{
				if(drCurrent["created_by"]!= null && (!drCurrent["created_by"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					txtCreatedBy.Text = drCurrent["created_by"].ToString();
				}
				else
				{
					txtCreatedBy.Text = "";
				}
			}

			// Mohan, 21/11/02
			TextBox txtSalesmanID = (TextBox)agentProfileMultiPage.FindControl("txtSalesmanID");
			if(txtSalesmanID != null)
			{
				if(drCurrent["salesmanid"]!= null && (!drCurrent["salesmanid"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					txtSalesmanID.Text = drCurrent["salesmanid"].ToString();
				}
				else
				{
					txtSalesmanID.Text = "";
				}
			}
			msTextBox txtCreditTerm = (msTextBox)agentProfileMultiPage.FindControl("txtCreditTerm");
			if(txtCreditTerm != null)
			{
				if(drCurrent["credit_term"]!= null && (!drCurrent["credit_term"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					txtCreditTerm.Text = drCurrent["credit_term"].ToString();
				}
				else
				{
					txtCreditTerm.Text = "";
				}
			}
	
			msTextBox txtCreditLimit = (msTextBox)agentProfileMultiPage.FindControl("txtCreditLimit");
			if(txtCreditLimit != null)
			{
				if(drCurrent["credit_limit"]!= null && (!drCurrent["credit_limit"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					txtCreditLimit.Text = drCurrent["credit_limit"].ToString();
				}
				else
				{
					txtCreditLimit.Text = "";
				}
			}

			msTextBox txtActiveQuatationNo = (msTextBox)agentProfileMultiPage.FindControl("txtActiveQuatationNo");
			if(txtActiveQuatationNo != null)
			{
				if(drCurrent["active_quotation_no"]!= null && (!drCurrent["active_quotation_no"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					txtActiveQuatationNo.Text = drCurrent["active_quotation_no"].ToString() ;
				}
				else
				{
					txtActiveQuatationNo.Text = "";
				}
			}

			msTextBox txtCreditOutstanding = (msTextBox)agentProfileMultiPage.FindControl("txtCreditOutstanding");
			if(txtCreditOutstanding != null)
			{
				if(drCurrent["credit_outstanding"]!= null && (!drCurrent["credit_outstanding"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					txtCreditOutstanding.Text = drCurrent["credit_outstanding"].ToString();
				}
				else
				{
					txtCreditOutstanding.Text = "";
				}
			}		

			RadioButton radioBtnCash = (RadioButton)agentProfileMultiPage.FindControl("radioBtnCash");
			RadioButton radioBtnCredit = (RadioButton)agentProfileMultiPage.FindControl("radioBtnCredit");
			if(radioBtnCash != null && radioBtnCash != null)
			{
				if(drCurrent["payment_mode"]!= null && (!drCurrent["payment_mode"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					String strPaymentMode = drCurrent["payment_mode"].ToString();
					if(strPaymentMode == "C")
					{
						radioBtnCash.Checked = true;
					}
					else if(strPaymentMode == "R")
					{
						radioBtnCredit.Checked = true;
					}
				}
				else
				{
					radioBtnCash.Checked = false;
					radioBtnCredit.Checked = false;
				}
			}
			
			BindComboLists();

			ShowReferencesForCurrentAgent();
			ShowSCForCurrentAgent();

			dgQuotation.CurrentPageIndex=0; // to avoid Invalid Current Page Index
			ShowQuotationsForCurrentAgent();
			BindQuotation();

			dgAgentZonecodes.CurrentPageIndex=0;
			ShowZoneForCurrentAgent();		// 02/12/2002
			BindZoneGrid();
			dgZoneZipcodIncluded.CurrentPageIndex=0;
		}

		private void DisplayRecNum()
		{
			int iCurrentRec = (Convert.ToInt32(ViewState["currentPage"]) + 1) + (Convert.ToInt32(ViewState["currentSet"]) * m_iSetSize) ;

			if(!utility.GetUserCulture().ToUpper().Equals("EN-US"))
			{
				lblNumRec.Text = ""+ iCurrentRec + " " + Utility.GetLanguageText(ResourceType.ScreenLabel, "of", utility.GetUserCulture()) + " " + m_sdsAgentProfile.QueryResultMaxSize + " " + Utility.GetLanguageText(ResourceType.ScreenLabel, "record(s)", utility.GetUserCulture());
			}
			else
			{
				lblNumRec.Text = ""+ iCurrentRec + " of " + m_sdsAgentProfile.QueryResultMaxSize + " record(s)";
			}
		}

		private void btnInsert_Click(object sender, System.EventArgs e)
		{
			m_sdsAgentProfile = AgentProfileMgrDAL.GetEmptyAgentProfileDS(1);
			Session["SESSION_DS1"] = m_sdsAgentProfile;
			
			// 11/11/2002 Mohan, Show Session Userid in Created-By field
			TextBox txtCreatedBy = (TextBox)agentProfileMultiPage.FindControl("txtCreatedBy");				
			txtCreatedBy.Text=utility.GetUserID();

			ViewState["APMode"] = ScreenMode.Insert;
			ViewState["APOperation"] = Operation.None;

			ViewState["IsTextChanged"] = false;
			ViewState["currentPage"] = 0;
			Utility.EnableButton(ref btnInsert,ButtonType.Insert,false,m_moduleAccessRights);
			btnExecQry.Enabled = false;
			LoadComboLists();
			//EnableTabs(true,true,false,false,false,false);
			EnableTabs(true,false,false,false,false);
			DisplayCurrentPage();
			lblNumRec.Text = "";
			lblNumRefRec.Text = "";
			lblCPErrorMessage.Text = "";
			TabStrip1.SelectedIndex = 0;
		}

		private void btnSave_Click(object sender, System.EventArgs e)
		{
			TextBox txtCreatedBy = (TextBox)agentProfileMultiPage.FindControl("txtCreatedBy");
			if (txtCreatedBy.Text == "") 
			{
				txtCreatedBy.Text =utility.GetUserID();
			}

			FillAgentProfileDataRow(Convert.ToInt32(ViewState["currentPage"]));
			
			int iOperation = Convert.ToInt32(ViewState["APOperation"]);
			switch(iOperation)
			{

				case  (int)Operation.Update:

					DataSet dsToUpdate = m_sdsAgentProfile.ds.GetChanges();

					try
					{
						AgentProfileMgrDAL.ModifyAgentProfileDS(utility.GetAppID(),utility.GetEnterpriseID(),dsToUpdate);
						Utility.EnableButton(ref btnInsert,ButtonType.Insert,true,m_moduleAccessRights);
						ViewState["IsTextChanged"] = false;
						ChangeCPState();
						m_sdsAgentProfile.ds.Tables[0].Rows[Convert.ToInt32(ViewState["currentPage"])].AcceptChanges();
						lblCPErrorMessage.Text =Utility.GetLanguageText(ResourceType.UserMessage,"UPD_SUCCESSFULLY",utility.GetUserCulture());
						Logger.LogDebugInfo("AgentProfile","btnSave_Click","INF004","save in modify mode..");
					}
					catch(ApplicationException appException)
					{
						String strMsg = appException.Message;

						if(strMsg.IndexOf("duplicate key") != -1 )
						{
							lblCPErrorMessage.Text =Utility.GetLanguageText(ResourceType.UserMessage,"NO_DUPLICATE_ACC_NO",utility.GetUserCulture());
							return;	
						}
						else if(strMsg.IndexOf("APPDB.SYS_C001888") != -1 )
						{	//Unique Constraint Violated
							lblCPErrorMessage.Text =Utility.GetLanguageText(ResourceType.UserMessage,"NO_DUPLICATE_ACC_NO",utility.GetUserCulture());
							return;	
						}
						else if(strMsg.IndexOf("APPDB.SYS_C002180") != -1 )
						{	//Parent Key Violated // ZipCode doesnot exist in parent table
							lblCPErrorMessage.Text =Utility.GetLanguageText(ResourceType.UserMessage,"NO_DUPLICATE_ACC_NO",utility.GetUserCulture());
							return;	
						}
						else if(strMsg.IndexOf("FK") != -1 )
						{
							if(strMsg.IndexOf("Zipcode") != -1)
							{
								lblCPErrorMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"INVALID_ZIP",utility.GetUserCulture());
								return;	
							}
						}
						else
						{
							lblCPErrorMessage.Text = strMsg;
						}
											
					}
					break;

				case  (int)Operation.Insert:

					DataSet dsToInsert = m_sdsAgentProfile.ds.GetChanges();
					try
					{
						AgentProfileMgrDAL.AddAgentProfileDS(utility.GetAppID(),utility.GetEnterpriseID(),dsToInsert);
						Utility.EnableButton(ref btnInsert,ButtonType.Insert,true,m_moduleAccessRights);
						ViewState["IsTextChanged"] = false;
						lblCPErrorMessage.Text=Utility.GetLanguageText(ResourceType.UserMessage,"INS_SUCCESSFULLY",utility.GetUserCulture());
						msTextBox txtAccNo = (msTextBox)agentProfileMultiPage.FindControl("txtAccNo");
						if(txtAccNo != null && txtAccNo.Text != "")
						{
							ViewState["currentAgent"] =  txtAccNo.Text;
							txtAccNo.Enabled = false;
						}

						//EnableTabs(true,true,true,true,true,true);
						EnableTabs(true,true,true,true,true);

						ChangeCPState();
						m_sdsAgentProfile.ds.Tables[0].Rows[Convert.ToInt32(ViewState["currentPage"])].AcceptChanges();
						lblCPErrorMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"INS_SUCCESSFULLY",utility.GetUserCulture());
						Logger.LogDebugInfo("AgentProfile","btnSave_Click","INF004","save in Insert mode");
					}
					catch(ApplicationException appException)
					{
						String strMsg = appException.Message;
						if(strMsg.IndexOf("duplicate key") != -1 )
						{
							lblCPErrorMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"DUPLICATE_AGENT",utility.GetUserCulture());
							return;
						}
						else if(strMsg.IndexOf("APPDB.SYS_C001888") != -1 )
						{
							//Unique Key Violated.........
							lblCPErrorMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"DUPLICATE_AGENT",utility.GetUserCulture());
							return;
						}
						else if(strMsg.IndexOf("FK") != -1 )
						{
							if(strMsg.IndexOf("Zipcode") != -1)
							{
								lblCPErrorMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"INVALID_ZIP",utility.GetUserCulture());
								return;
							}
						}
						else if(strMsg.IndexOf("APPDB.SYS_C002180") != -1 )
						{
							//Zipocode Doesnot containt Parent Key
							if(strMsg.IndexOf("Zipcode") != -1)
							{
								lblCPErrorMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"INVALID_ZIP",utility.GetUserCulture());
								return;
							}
						}
						else
						{
							lblCPErrorMessage.Text = strMsg;
							return;
						}
						
					}
					break;
			}

		}

		private void btnDelete_Click(object sender, System.EventArgs e)
		{
		
		}

		private void btnZipcodePopup_Click(object sender, System.EventArgs e)
		{
			msTextBox txtZipCode = (msTextBox)agentProfileMultiPage.FindControl("txtZipCode");
			TextBox txtState = (TextBox)agentProfileMultiPage.FindControl("txtState");
			TextBox txtCountry = (TextBox)agentProfileMultiPage.FindControl("txtCountry");

			String strZipcodeClientID = null;
			String strStateClientID = null;
			String strCountryClientID = null;

			String strZipcode = null;
				
			if(txtZipCode != null)
			{
				strZipcodeClientID = txtZipCode.ClientID;
				strZipcode = txtZipCode.Text;
			}

			if(txtState != null)
			{
				strStateClientID = txtState.ClientID;
			}
				
			if(txtCountry != null)
			{
				strCountryClientID = txtCountry.ClientID;
			}

			String sUrl = "ZipcodePopup.aspx?FORMID=AgentProfile&ZIPCODE="+strZipcode+"&ZIPCODE_CID="+strZipcodeClientID+"&STATE_CID="+strStateClientID+"&COUNTRY_CID="+strCountryClientID;
			ArrayList paramList = new ArrayList();
			paramList.Add(sUrl);
			String sScript = Utility.GetScript("openWindowParam.js",paramList);
			Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
			
		}

		private void btnISectorSearch_Click(object sender, System.EventArgs e)
		{
			msTextBox txtISectorCode = (msTextBox)agentProfileMultiPage.FindControl("txtISectorCode");
			String strISectorClientID = null;
			String strISector = null;				
			if(txtISectorCode != null)
			{
				strISectorClientID = txtISectorCode.ClientID;
				strISector = txtISectorCode.Text;
			}

			String sUrl = "IndustrialSectorPopup.aspx?FORMID=AgentProfile&ISECTOR="+strISector+"&ISECTOR_CID="+strISectorClientID;
			ArrayList paramList = new ArrayList();
			paramList.Add(sUrl);
			String sScript = Utility.GetScript("openWindowParam.js",paramList);
			Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
		}

		public void btnSearchSalesman_Click(object sender, System.EventArgs e)
		{
			String sQrySalesmanId ="";
			TextBox txtSalesmanID = (TextBox)agentProfileMultiPage.FindControl("txtSalesmanID");
			if(txtSalesmanID != null && txtSalesmanID.Text != "")
			{
				sQrySalesmanId =txtSalesmanID.Text.Trim();
			}
			
			String sUrl = "SalesPersonPopup.aspx?FORMID=AgentProfile&QUERY_SALESMANID="+sQrySalesmanId;
			ArrayList paramList = new ArrayList();
			paramList.Add(sUrl);
			String sScript = Utility.GetScript("openWindowParam.js",paramList);
			Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);	
		}

		private void btnRefZipcodeSearch_Click(object sender, System.EventArgs e)
		{
			msTextBox txtZipCode = (msTextBox)agentProfileMultiPage.FindControl("txtRefZipcode");
			TextBox txtState = (TextBox)agentProfileMultiPage.FindControl("txtRefState");
			TextBox txtCountry = (TextBox)agentProfileMultiPage.FindControl("txtRefCountry");
			String strZipcodeClientID = null;
			String strStateClientID = null;
			String strCountryClientID = null;
			String strZipcode = null;					
			if(txtZipCode != null)
			{
				strZipcodeClientID = txtZipCode.ClientID;
				strZipcode = txtZipCode.Text;
			}

			if(txtState != null)
			{
				strStateClientID = txtState.ClientID;
			}					
			if(txtCountry != null)
			{
				strCountryClientID = txtCountry.ClientID;
			}
			String sUrl = "ZipcodePopup.aspx?FORMID=AgentProfile&ZIPCODE="+strZipcode+"&ZIPCODE_CID="+strZipcodeClientID+"&STATE_CID="+strStateClientID+"&COUNTRY_CID="+strCountryClientID;
			ArrayList paramList = new ArrayList();
			paramList.Add(sUrl);
			String sScript = Utility.GetScript("openWindowParam.js",paramList);
			Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);

		}

		private void FillAgentProfileDataRow(int iCurrentRow)
		{
			DataRow drCurrent = m_sdsAgentProfile.ds.Tables[0].Rows[iCurrentRow];
			msTextBox txtAccNo = (msTextBox)agentProfileMultiPage.FindControl("txtAccNo");
			if(txtAccNo != null && txtAccNo.Text != "")
			{
				drCurrent["agentid"] = txtAccNo.Text;
			}

			msTextBox txtRefNo = (msTextBox)agentProfileMultiPage.FindControl("txtRefNo");
			if(txtRefNo != null && txtRefNo.Text != "")
			{
				drCurrent["ref_code"] = txtRefNo.Text;
			}

			TextBox txtAgentName = (TextBox)agentProfileMultiPage.FindControl("txtAgentName");
			if(txtAgentName != null && txtAgentName.Text != "")
			{
				drCurrent["agent_name"] = txtAgentName.Text;
			}

			TextBox txtContactPerson = (TextBox)agentProfileMultiPage.FindControl("txtContactPerson");
			if(txtContactPerson != null && txtContactPerson.Text != "")
			{
				drCurrent["contact_person"] = txtContactPerson.Text;
			}

			TextBox txtAddress1 = (TextBox)agentProfileMultiPage.FindControl("txtAddress1");
			if(txtAddress1 != null && txtAddress1.Text != "")
			{
				drCurrent["agent_address1"] = txtAddress1.Text;
			}

			TextBox txtAddress2 = (TextBox)agentProfileMultiPage.FindControl("txtAddress2");
			if(txtAddress2 != null && txtAddress2.Text != "")
			{
				drCurrent["agent_address2"] = txtAddress2.Text;
			}

			TextBox txtCountry = (TextBox)agentProfileMultiPage.FindControl("txtCountry");
			if(txtCountry != null && txtCountry.Text != "")
			{
				drCurrent["country"] = txtCountry.Text;
			}

			msTextBox txtZipCode = (msTextBox)agentProfileMultiPage.FindControl("txtZipCode");
			if(txtZipCode != null && txtZipCode.Text != "")
			{
				drCurrent["zipcode"] = txtZipCode.Text;
			}

			TextBox txtTelephone = (TextBox)agentProfileMultiPage.FindControl("txtTelephone");
			if(txtTelephone != null && txtTelephone.Text != "")
			{
				drCurrent["telephone"] = txtTelephone.Text;
			}

			TextBox txtFax = (TextBox)agentProfileMultiPage.FindControl("txtFax");
			if(txtFax != null && txtFax.Text != "")
			{
				drCurrent["fax"] = txtFax.Text;
			}

			msTextBox txtActiveQuotationNo = (msTextBox)agentProfileMultiPage.FindControl("txtActiveQuotationNo");
			if(txtActiveQuotationNo != null && txtActiveQuotationNo.Text != "")
			{
				drCurrent["active_quotation_no"] = txtActiveQuotationNo.Text;
			}

			TextBox txtRemark = (TextBox)agentProfileMultiPage.FindControl("txtRemark");
			if(txtRemark != null && txtRemark.Text != "")
			{
				drCurrent["remark"] = txtRemark.Text;
			}
	
			msTextBox txtFreeInsuranceAmt = (msTextBox)agentProfileMultiPage.FindControl("txtFreeInsuranceAmt");
			if(txtFreeInsuranceAmt != null && txtFreeInsuranceAmt.Text != "")
			{
				drCurrent["free_insurance_amt"] = txtFreeInsuranceAmt.Text;
			}

			msTextBox txtInsurancePercentSurcharge = (msTextBox)agentProfileMultiPage.FindControl("txtInsurancePercentSurcharge");
			if(txtInsurancePercentSurcharge != null && txtInsurancePercentSurcharge.Text != "")
			{
				drCurrent["insurance_percent_surcharge"] = txtInsurancePercentSurcharge.Text;
			}

			TextBox txtEmail = (TextBox)agentProfileMultiPage.FindControl("txtEmail");
			if(txtEmail != null && txtEmail.Text != "")
			{
				drCurrent["email"] = txtEmail.Text;
			}

			TextBox txtCreatedBy = (TextBox)agentProfileMultiPage.FindControl("txtCreatedBy");
			if(txtCreatedBy != null && txtCreatedBy.Text != "")
			{
				drCurrent["created_by"] = txtCreatedBy.Text;
			}

			DropDownList ddlApplyDimWt = (DropDownList)agentProfileMultiPage.FindControl("ddlApplyDimWt");			
			if(ddlApplyDimWt != null)
			{
				String strApplyDimWt = ddlApplyDimWt.SelectedItem.Value;
				if(strApplyDimWt != "")
				{
					drCurrent["apply_dim_wt"] = strApplyDimWt;
				}
			}

			DropDownList ddlPodSlipRequired = (DropDownList)agentProfileMultiPage.FindControl("ddlPodSlipRequired");			
			if(ddlPodSlipRequired != null)
			{
				String strPodSlipRequired = ddlPodSlipRequired.SelectedItem.Value;
				if(strPodSlipRequired != "")
				{
					drCurrent["pod_slip_required"] = strPodSlipRequired;
				}
			}

			DropDownList ddlApplyEsaSurcharge = (DropDownList)agentProfileMultiPage.FindControl("ddlApplyEsaSurcharge");			
			if(ddlApplyEsaSurcharge != null)
			{
				String strApplyEsaSurcharge = ddlApplyEsaSurcharge.SelectedItem.Value;

				if(strApplyEsaSurcharge != "")
				{
					drCurrent["apply_esa_surcharge"] = strApplyEsaSurcharge;
				}
			}

			DropDownList ddlStatus = (DropDownList)agentProfileMultiPage.FindControl("ddlStatus");			
			if(ddlStatus != null)
			{
				String strStatus = ddlStatus.SelectedItem.Value;
				if(strStatus != "")
				{
					drCurrent["status_active"] = strStatus;
				}
			}

			//Mohan, 21/11/02
			TextBox txtSalesmanID = (TextBox)agentProfileMultiPage.FindControl("txtSalesmanID");
			if(txtSalesmanID != null && txtSalesmanID.Text != "")
			{
				drCurrent["salesmanid"] = txtSalesmanID.Text;
			}
			msTextBox txtCreditTerm = (msTextBox)agentProfileMultiPage.FindControl("txtCreditTerm");
			if(txtCreditTerm != null && txtCreditTerm.Text != "")
			{
				drCurrent["credit_term"] = txtCreditTerm.Text;
			}

			msTextBox txtCreditLimit = (msTextBox)agentProfileMultiPage.FindControl("txtCreditLimit");
			if(txtCreditLimit != null && txtCreditLimit.Text != "")
			{
				drCurrent["credit_limit"] = txtCreditLimit.Text;
			}
			
			msTextBox txtCreditOutstanding = (msTextBox)agentProfileMultiPage.FindControl("txtCreditOutstanding");
			if(txtCreditOutstanding != null && txtCreditOutstanding.Text != "")
			{
				drCurrent["credit_outstanding"] = txtCreditOutstanding.Text;
			}

			RadioButton radioBtnCash = (RadioButton)agentProfileMultiPage.FindControl("radioBtnCash");
			String strPaymentMode = "";
			if(radioBtnCash != null && radioBtnCash.Checked == true)
			{
				strPaymentMode = "C";
			}

			RadioButton radioBtnCredit = (RadioButton)agentProfileMultiPage.FindControl("radioBtnCredit");
			if(radioBtnCredit != null && radioBtnCredit.Checked == true)
			{
				strPaymentMode = "R";
			}			
			if(strPaymentMode != "")
			{
				drCurrent["payment_mode"] = strPaymentMode;
			}

			DropDownList ddlMBG = (DropDownList)agentProfileMultiPage.FindControl("ddlMBG");			
			if(ddlMBG != null)
			{
				String strMBG = ddlMBG.SelectedItem.Value;
				if(strMBG != "")
				{
					drCurrent["mbg"] = strMBG;
				}
			}
			

		}


		private DataView GetApplyDimWtOptions(bool showNilOption) 
		{
			DataTable dtApplyDimWtOptions = new DataTable();
 
			dtApplyDimWtOptions.Columns.Add(new DataColumn("Text", typeof(string)));
			dtApplyDimWtOptions.Columns.Add(new DataColumn("StringValue", typeof(string)));

			ArrayList applyDimWtOptionArray = Utility.GetCodeValues(utility.GetAppID(),utility.GetUserCulture(),"apply_dim_wt",CodeValueType.StringValue);

			if(showNilOption)
			{
				DataRow drNilRow = dtApplyDimWtOptions.NewRow();
				drNilRow[0] = "";
				drNilRow[1] = "";
				dtApplyDimWtOptions.Rows.Add(drNilRow);
			}
			foreach(SystemCode applyDimWtSysCode in applyDimWtOptionArray)
			{
				DataRow drEach = dtApplyDimWtOptions.NewRow();
				drEach[0] = applyDimWtSysCode.Text;
				drEach[1] = applyDimWtSysCode.StringValue;
				dtApplyDimWtOptions.Rows.Add(drEach);
			}

			DataView dvApplyDimWtOptions = new DataView(dtApplyDimWtOptions);
			return dvApplyDimWtOptions;
		}

		private DataView GetPODSlipRequirdOptions(bool showNilOption) 
		{
			DataTable dtPODSlipRequirdOptions = new DataTable();
 
			dtPODSlipRequirdOptions.Columns.Add(new DataColumn("Text", typeof(string)));
			dtPODSlipRequirdOptions.Columns.Add(new DataColumn("StringValue", typeof(string)));

			ArrayList podSlipRequirdOptionArray = Utility.GetCodeValues(utility.GetAppID(),utility.GetUserCulture(),"pod_slip_required",CodeValueType.StringValue);

			if(showNilOption)
			{
				DataRow drNilRow = dtPODSlipRequirdOptions.NewRow();
				drNilRow[0] = "";
				drNilRow[1] = "";
				dtPODSlipRequirdOptions.Rows.Add(drNilRow);
			}
			foreach(SystemCode podSlipRequirdSysCode in podSlipRequirdOptionArray)
			{
				DataRow drEach = dtPODSlipRequirdOptions.NewRow();
				drEach[0] = podSlipRequirdSysCode.Text;
				drEach[1] = podSlipRequirdSysCode.StringValue;
				dtPODSlipRequirdOptions.Rows.Add(drEach);
			}

			DataView dvPODSlipRequirdOptions = new DataView(dtPODSlipRequirdOptions);
			return dvPODSlipRequirdOptions;
		}

		private DataView GetApplyESASurchargeOptions(bool showNilOption) 
		{
			
			DataTable dtApplyESASurchargeOptions = new DataTable();
			
			dtApplyESASurchargeOptions.Columns.Add(new DataColumn("Text", typeof(string)));
			dtApplyESASurchargeOptions.Columns.Add(new DataColumn("StringValue", typeof(string)));

			ArrayList applyESASurchargeOptionArray = Utility.GetCodeValues(utility.GetAppID(),utility.GetUserCulture(),"apply_esa_surcharge",CodeValueType.StringValue);
			if(showNilOption)
			{
				DataRow drNilRow = dtApplyESASurchargeOptions.NewRow();
				drNilRow[0] = "";
				drNilRow[1] = "";
				dtApplyESASurchargeOptions.Rows.Add(drNilRow);
			}
			foreach(SystemCode applyESASurchargeSysCode in applyESASurchargeOptionArray)
			{
				DataRow drEach = dtApplyESASurchargeOptions.NewRow();
				drEach[0] = applyESASurchargeSysCode.Text;
				drEach[1] = applyESASurchargeSysCode.StringValue;
				dtApplyESASurchargeOptions.Rows.Add(drEach);
			}

			DataView dvApplyESASurchargeOptions = new DataView(dtApplyESASurchargeOptions);
			return dvApplyESASurchargeOptions;
		}

		private DataView GetStatusOptions(bool showNilOption) 
		{
			
			DataTable dtStatusOptions = new DataTable();
			
			dtStatusOptions.Columns.Add(new DataColumn("Text", typeof(string)));
			dtStatusOptions.Columns.Add(new DataColumn("StringValue", typeof(string)));

			ArrayList statusOptionArray = Utility.GetCodeValues(utility.GetAppID(),utility.GetUserCulture(),"active_status",CodeValueType.StringValue);
			if(showNilOption)
			{
				DataRow drNilRow = dtStatusOptions.NewRow();
				drNilRow[0] = "";
				drNilRow[1] = "";
				dtStatusOptions.Rows.Add(drNilRow);
			}
			foreach(SystemCode statusSysCode in statusOptionArray)
			{
				DataRow drEach = dtStatusOptions.NewRow();
				drEach[0] = statusSysCode.Text;
				drEach[1] = statusSysCode.StringValue;
				dtStatusOptions.Rows.Add(drEach);
			}

			DataView dvStatusOptions = new DataView(dtStatusOptions);
			return dvStatusOptions;
		}

		private DataView GetMBGOptions(bool showNilOption) 
		{
			
			DataTable dtMBGOptions = new DataTable();
			
			dtMBGOptions.Columns.Add(new DataColumn("Text", typeof(string)));
			dtMBGOptions.Columns.Add(new DataColumn("StringValue", typeof(string)));

			ArrayList mbgOptionArray = Utility.GetCodeValues(utility.GetAppID(),utility.GetUserCulture(),"mbg",CodeValueType.StringValue);
			if(showNilOption)
			{
				DataRow drNilRow = dtMBGOptions.NewRow();
				drNilRow[0] = "";
				drNilRow[1] = "";
				dtMBGOptions.Rows.Add(drNilRow);
			}
			foreach(SystemCode mbgSysCode in mbgOptionArray)
			{
				DataRow drEach = dtMBGOptions.NewRow();
				drEach[0] = mbgSysCode.Text;
				drEach[1] = mbgSysCode.StringValue;
				dtMBGOptions.Rows.Add(drEach);
			}

			DataView dvMBGOptions = new DataView(dtMBGOptions);
			return dvMBGOptions;
		}

		private void LoadComboLists()
		{

			if(Convert.ToInt32(ViewState["APMode"]) == Convert.ToInt32(ScreenMode.Query))
			{
				m_dvApplyDimWtOptions = GetApplyDimWtOptions(true);
				m_dvPODSlipRequiredOptions = GetPODSlipRequirdOptions(true);
				m_dvApplyESASurchargeOptions = GetApplyESASurchargeOptions(true);
				m_dvStatusOptions = GetStatusOptions(true);
				m_dvMBGOptions = GetMBGOptions(true);
			}
			else if ((int) ViewState["APMode"] == (int)ScreenMode.Insert)
			{
				m_dvApplyDimWtOptions = GetApplyDimWtOptions(true);
				m_dvPODSlipRequiredOptions = GetPODSlipRequirdOptions(true);
				m_dvApplyESASurchargeOptions = GetApplyESASurchargeOptions(true);
				m_dvMBGOptions = GetMBGOptions(true);
				m_dvStatusOptions = GetStatusOptions(false);
			}
			else if((int) ViewState["APMode"] == (int)ScreenMode.ExecuteQuery)
			{
				m_dvApplyDimWtOptions = GetApplyDimWtOptions(false);
				m_dvPODSlipRequiredOptions = GetPODSlipRequirdOptions(false);
				m_dvApplyESASurchargeOptions = GetApplyESASurchargeOptions(false);
				m_dvMBGOptions = GetMBGOptions(false);
				m_dvStatusOptions = GetStatusOptions(false);
			}

		}

		private void BindComboLists()
		{
			DataRow drCurrent = m_sdsAgentProfile.ds.Tables[0].Rows[Convert.ToInt32(ViewState["currentPage"])];

			DropDownList ddlApplyDimWt = (DropDownList)agentProfileMultiPage.FindControl("ddlApplyDimWt");
			
			if(ddlApplyDimWt != null)
			{
				ddlApplyDimWt.DataSource = (ICollection)m_dvApplyDimWtOptions;
				ddlApplyDimWt.DataTextField = "Text";
				ddlApplyDimWt.DataValueField = "StringValue";
				ddlApplyDimWt.DataBind();


				if((drCurrent["apply_dim_wt"]!= null) && (!drCurrent["apply_dim_wt"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					String strApplyDimWt = (String)drCurrent["apply_dim_wt"];
					ddlApplyDimWt.SelectedIndex = ddlApplyDimWt.Items.IndexOf(ddlApplyDimWt.Items.FindByValue(strApplyDimWt));//.FindByValue("N"));
				}
				//Logger.LogDebugInfo("AgentProfile","BindComboLists","INF006","Selecting Apply......"+e.Item.ItemIndex+" Selected Index is : "+ddlSCCloseStatus.SelectedIndex+"   "+strCloseStatus);
			}

			DropDownList ddlPodSlipRequired = (DropDownList)agentProfileMultiPage.FindControl("ddlPodSlipRequired");
			
			if(ddlPodSlipRequired != null)
			{
				ddlPodSlipRequired.DataSource = (ICollection)m_dvPODSlipRequiredOptions;
				ddlPodSlipRequired.DataTextField = "Text";
				ddlPodSlipRequired.DataValueField = "StringValue";
				ddlPodSlipRequired.DataBind();

				if((drCurrent["pod_slip_required"]!= null) && (!drCurrent["pod_slip_required"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					String strPODSlipRequired = (String)drCurrent["pod_slip_required"];
					ddlPodSlipRequired.SelectedIndex = ddlPodSlipRequired.Items.IndexOf(ddlPodSlipRequired.Items.FindByValue(strPODSlipRequired));//.FindByValue("N"));
				}
				//Logger.LogDebugInfo("AgentProfile","BindComboLists","INF006","Selecting Apply......"+e.Item.ItemIndex+" Selected Index is : "+ddlSCCloseStatus.SelectedIndex+"   "+strCloseStatus);
			}

			DropDownList ddlApplyEsaSurcharge = (DropDownList)agentProfileMultiPage.FindControl("ddlApplyEsaSurcharge");
			
			if(ddlApplyEsaSurcharge != null)
			{
				ddlApplyEsaSurcharge.DataSource = (ICollection)m_dvApplyESASurchargeOptions;
				ddlApplyEsaSurcharge.DataTextField = "Text";
				ddlApplyEsaSurcharge.DataValueField = "StringValue";
				ddlApplyEsaSurcharge.DataBind();

				if((drCurrent["apply_esa_surcharge"]!= null) && (!drCurrent["apply_esa_surcharge"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					String strApplyEsaSurcharge = (String)drCurrent["apply_esa_surcharge"];
					ddlApplyEsaSurcharge.SelectedIndex = ddlApplyEsaSurcharge.Items.IndexOf(ddlApplyEsaSurcharge.Items.FindByValue(strApplyEsaSurcharge));//.FindByValue("N"));
				}
				//Logger.LogDebugInfo("AgentProfile","BindComboLists","INF006","Selecting Apply......"+e.Item.ItemIndex+" Selected Index is : "+ddlSCCloseStatus.SelectedIndex+"   "+strCloseStatus);
			}

			DropDownList ddlStatus = (DropDownList)agentProfileMultiPage.FindControl("ddlStatus");
			
			if(ddlStatus != null)
			{
				ddlStatus.DataSource = (ICollection)m_dvStatusOptions;
				ddlStatus.DataTextField = "Text";
				ddlStatus.DataValueField = "StringValue";
				ddlStatus.DataBind();

				if((drCurrent["status_active"]!= null) && (!drCurrent["status_active"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					String strStatusActive = (String)drCurrent["status_active"];
					ddlStatus.SelectedIndex = ddlStatus.Items.IndexOf(ddlStatus.Items.FindByValue(strStatusActive));//.FindByValue("N"));
				}
				//Logger.LogDebugInfo("AgentProfile","BindComboLists","INF006","Selecting Apply......"+e.Item.ItemIndex+" Selected Index is : "+ddlSCCloseStatus.SelectedIndex+"   "+strCloseStatus);
			}

			DropDownList ddlMBG = (DropDownList)agentProfileMultiPage.FindControl("ddlMBG");			
			if(ddlMBG != null)
			{
				ddlMBG.DataSource = (ICollection)m_dvMBGOptions;
				ddlMBG.DataTextField = "Text";
				ddlMBG.DataValueField = "StringValue";
				ddlMBG.DataBind();

				if((drCurrent["mbg"]!= null) && (!drCurrent["mbg"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					String strMBG = (String)drCurrent["mbg"];
					ddlMBG.SelectedIndex = ddlMBG.Items.IndexOf(ddlMBG.Items.FindByValue(strMBG));//.FindByValue("N"));
				}
				//Logger.LogDebugInfo("CustomerProfile","BindComboLists","INF006","Selecting Apply......"+e.Item.ItemIndex+" Selected Index is : "+ddlSCCloseStatus.SelectedIndex+"   "+strCloseStatus);
			}
		}


		private void EnableTabs( bool bTab0, bool bTab1,bool bTab2,bool bTab3,bool bTab4)
		{
			Microsoft.Web.UI.WebControls.TabItem tabItem0 = TabStrip1.Items[0];
			tabItem0.Enabled = bTab0;
	
			Microsoft.Web.UI.WebControls.TabItem tabItem1 = TabStrip1.Items[1];
			tabItem1.Enabled = bTab1;		

			Microsoft.Web.UI.WebControls.TabItem tabItem2 = TabStrip1.Items[2];
			tabItem2.Enabled = bTab2;		

			Microsoft.Web.UI.WebControls.TabItem tabItem3 = TabStrip1.Items[3];
			tabItem3.Enabled = bTab3;		

			Microsoft.Web.UI.WebControls.TabItem tabItem4 = TabStrip1.Items[4];
			tabItem4.Enabled = bTab4;		

//			Microsoft.Web.UI.WebControls.TabItem tabItem5 = TabStrip1.Items[5];
//			tabItem5.Enabled = bTab5;	
			// No Special Information Tab!!!
		}


		private bool IsRefTabEnabled()
		{
			Microsoft.Web.UI.WebControls.TabItem tabItem2 = TabStrip1.Items[2];
			return tabItem2.Enabled;
		}
		private void ChangeCPState()
		{
			if((int)ViewState["APMode"] == (int) ScreenMode.Insert && (int)ViewState["APOperation"] == (int)Operation.None)
			{
				ViewState["APOperation"] = Operation.Insert;
			}
			else if((int)ViewState["APMode"] == (int) ScreenMode.Insert && (int)ViewState["APOperation"] == (int)Operation.Insert)
			{
				ViewState["APOperation"] = Operation.Saved;
			}
			else if((int)ViewState["APMode"] == (int) ScreenMode.Insert && (int)ViewState["APOperation"] == (int)Operation.Saved)
			{
				ViewState["APOperation"] = Operation.Update;
			}
			else if((int)ViewState["APMode"] == (int) ScreenMode.Insert && (int)ViewState["APOperation"] == (int)Operation.Update)
			{
				ViewState["APOperation"] = Operation.Saved;
			}
			else if((int)ViewState["APMode"] == (int) ScreenMode.ExecuteQuery && (int)ViewState["APOperation"] == (int)Operation.None)
			{
				ViewState["APOperation"] = Operation.Update;
			}
			else if((int)ViewState["APMode"] == (int) ScreenMode.ExecuteQuery && (int)ViewState["APOperation"] == (int)Operation.Update)
			{
				ViewState["APOperation"] = Operation.None;
			}
		
		}

		private void EnableTxtAccNo()
		{

			msTextBox txtAccNo = (msTextBox)agentProfileMultiPage.FindControl("txtAccNo");			
			if(txtAccNo != null)
			{
				if((int)ViewState["APMode"] == (int) ScreenMode.Insert && (int)ViewState["APOperation"] == (int)Operation.None)
				{
					txtAccNo.Enabled = true;
				}
				else if((int)ViewState["APMode"] == (int) ScreenMode.Insert && (int)ViewState["APOperation"] == (int)Operation.Saved)
				{
					txtAccNo.Enabled = false;
				}
				else if((int)ViewState["APMode"] == (int) ScreenMode.ExecuteQuery)
				{
					txtAccNo.Enabled = false;
				}
				else if((int)ViewState["APMode"] == (int) ScreenMode.Query)
				{
					txtAccNo.Enabled = true;;
				}
			}			
		}

		private void txtAccNo_TextChanged(object sender,System.EventArgs e)
		{
			if((bool)ViewState["IsTextChanged"] == false)
			{
				ChangeCPState();
				ViewState["IsTextChanged"] = true;
			}
		}

		private void txtRefNo_TextChanged(object sender,System.EventArgs e)
		{
			if((bool)ViewState["IsTextChanged"] == false)
			{
				ChangeCPState();
				ViewState["IsTextChanged"] = true;
			}
		}

		private void txtAgentName_TextChanged(object sender,System.EventArgs e)
		{
			if((bool)ViewState["IsTextChanged"] == false)
			{
				ChangeCPState();
				ViewState["IsTextChanged"] = true;
			}
		}

		private void txtContactPerson_TextChanged(object sender,System.EventArgs e)
		{
			if((bool)ViewState["IsTextChanged"] == false)
			{
				ChangeCPState();
				ViewState["IsTextChanged"] = true;
			}
		}

		private void txtAddress1_TextChanged(object sender,System.EventArgs e)
		{
			if((bool)ViewState["IsTextChanged"] == false)
			{
				ChangeCPState();
				ViewState["IsTextChanged"] = true;
			}
		}

		private void txtAddress2_TextChanged(object sender,System.EventArgs e)
		{
			if((bool)ViewState["IsTextChanged"] == false)
			{
				ChangeCPState();
				ViewState["IsTextChanged"] = true;
			}
		}

		private void txtCountry_TextChanged(object sender,System.EventArgs e)
		{
			if((bool)ViewState["IsTextChanged"] == false)
			{
				ChangeCPState();
				ViewState["IsTextChanged"] = true;
			}
		}

		private void txtZipCode_TextChanged(object sender,System.EventArgs e)
		{
			if((bool)ViewState["IsTextChanged"] == false)
			{
				ChangeCPState();
				ViewState["IsTextChanged"] = true;
			}
		}

		private void txtTelephone_TextChanged(object sender,System.EventArgs e)
		{
			if((bool)ViewState["IsTextChanged"] == false)
			{
				ChangeCPState();
				ViewState["IsTextChanged"] = true;
			}
		}

		private void txtFax_TextChanged(object sender,System.EventArgs e)
		{
			if((bool)ViewState["IsTextChanged"] == false)
			{
				ChangeCPState();
				ViewState["IsTextChanged"] = true;
			}
		}

		private void txtActiveQuotationNo_TextChanged(object sender,System.EventArgs e)
		{
			if((bool)ViewState["IsTextChanged"] == false)
			{
				ChangeCPState();
				ViewState["IsTextChanged"] = true;
			}
		}

		private void txtRemark_TextChanged(object sender,System.EventArgs e)
		{
			if((bool)ViewState["IsTextChanged"] == false)
			{
				ChangeCPState();
				ViewState["IsTextChanged"] = true;
			}
		}

		private void txtFreeInsuranceAmt_TextChanged(object sender,System.EventArgs e)
		{
			if((bool)ViewState["IsTextChanged"] == false)
			{
				ChangeCPState();
				ViewState["IsTextChanged"] = true;
			}
		}

		private void txtInsurancePercentSurcharge_TextChanged(object sender,System.EventArgs e)
		{
			if((bool)ViewState["IsTextChanged"] == false)
			{
				ChangeCPState();
				ViewState["IsTextChanged"] = true;
			}
		}

		private void txtEmail_TextChanged(object sender,System.EventArgs e)
		{
			if((bool)ViewState["IsTextChanged"] == false)
			{
				ChangeCPState();
				ViewState["IsTextChanged"] = true;
			}
		}

//		private void txtCreatedBy_TextChanged(object sender,System.EventArgs e)
//		{
//			if((bool)ViewState["IsTextChanged"] == false)
//			{
//				ChangeCPState();
//				ViewState["IsTextChanged"] = true;
//			}
//		}

		private void ddlApplyDimWt_SelectedIndexChanged(object sender,System.EventArgs e)
		{
			if((bool)ViewState["IsTextChanged"] == false)
			{
				ChangeCPState();
				ViewState["IsTextChanged"] = true;
			}
		}

		private void ddlPodSlipRequired_SelectedIndexChanged(object sender,System.EventArgs e)
		{
			if((bool)ViewState["IsTextChanged"] == false)
			{
				ChangeCPState();
				ViewState["IsTextChanged"] = true;
			}
		}

		private void ddlApplyEsaSurcharge_SelectedIndexChanged(object sender,System.EventArgs e)
		{
			if((bool)ViewState["IsTextChanged"] == false)
			{
				ChangeCPState();
				ViewState["IsTextChanged"] = true;
			}
		}

		private void ddlStatus_SelectedIndexChanged(object sender,System.EventArgs e)
		{
			if((bool)ViewState["IsTextChanged"] == false)
			{
				ChangeCPState();
				ViewState["IsTextChanged"] = true;
			}
		}


		//Mohan, 21/11/02
		private void txtSalesmanID_TextChanged(object sender,System.EventArgs e)
		{
			if((bool)ViewState["IsTextChanged"] == false)
			{
				ChangeCPState();
				ViewState["IsTextChanged"] = true;
			}
		}

		private void txtCreditTerm_TextChanged(object sender,System.EventArgs e)
		{
			if((bool)ViewState["IsTextChanged"] == false)
			{
				ChangeCPState();
				ViewState["IsTextChanged"] = true;
			}
		}

		private void txtCreditLimit_TextChanged(object sender,System.EventArgs e)
		{
			if((bool)ViewState["IsTextChanged"] == false)
			{
				ChangeCPState();
				ViewState["IsTextChanged"] = true;
			}
		}

		private void txtActiveQuatationNo_TextChanged(object sender,System.EventArgs e)
		{
			if((bool)ViewState["IsTextChanged"] == false)
			{
				ChangeCPState();
				ViewState["IsTextChanged"] = true;
			}
		}

		private void txtCreditOutstanding_TextChanged(object sender,System.EventArgs e)
		{
			if((bool)ViewState["IsTextChanged"] == false)
			{
				ChangeCPState();
				ViewState["IsTextChanged"] = true;
			}
		}

		private void radioBtnCash_CheckedChanged(object sender,System.EventArgs e)
		{
			if((bool)ViewState["IsTextChanged"] == false)
			{
				ChangeCPState();
				ViewState["IsTextChanged"] = true;
			}
		}

		private void radioBtnCredit_CheckedChanged(object sender,System.EventArgs e)
		{
			if((bool)ViewState["IsTextChanged"] == false)
			{
				ChangeCPState();
				ViewState["IsTextChanged"] = true;
			}
		}

		private void ddlMBG_SelectedIndexChanged(object sender,System.EventArgs e)
		{
			if((bool)ViewState["IsTextChanged"] == false)
			{
				ChangeCPState();
				ViewState["IsTextChanged"] = true;
			}
		}

		private void txtCountRec_TextChanged(object sender, System.EventArgs e)
		{
		
		}

		
		/// Reference Tab
		/// 
		private void btnRefViewAll_Click(object sender, System.EventArgs e)
		{
			ShowReferencesForCurrentAgent();
		}

		private void btnRefInsert_Click(object sender, System.EventArgs e)
		{
			EnableRefPage(true);

			ViewState["RefMode"] = ScreenMode.Insert;
			ViewState["RefOperation"] = Operation.None;
		
			ClearRefPageForInsert();

			Button btnRefInsert = (Button)agentProfileMultiPage.FindControl("btnRefInsert");

			if(btnRefInsert != null)
			{
				btnRefInsert.Enabled	= false;
			}

		}

		private void ShowReferencesForCurrentAgent()
		{
			ViewState["RefMode"] = ScreenMode.ExecuteQuery;
			ViewState["RefOperation"] = Operation.None;
			ViewState["IsRefTextChanged"] = false;
			ViewState["currentRefPage"] = 0;
			ViewState["currentRefSet"] = 0;
			
			GetRefRecSet();

			Button btnRefInsert = (Button)agentProfileMultiPage.FindControl("btnRefInsert");

			if(m_sdsReference.QueryResultMaxSize == 0)
			{
				lblNumRefRec.Text = "No reference added.";
				EnableRefNavigationButtons(false,false,false,false);
				if(btnRefInsert != null)
				{
					btnRefInsert.Enabled	= true;
				}

				ViewState["RefMode"] = ScreenMode.None;
				ViewState["RefOperation"] = Operation.None;
				ClearRefPageForInsert();
				EnableRefPage(false);
				return;
			}
			EnableRefPage(true);
			DisplayCurrentRefPage();

			if(btnRefInsert != null)
			{
				btnRefInsert.Enabled	= true;
			}

			if(m_sdsReference != null && m_sdsReference.DataSetRecSize > 0)
			{
				EnableRefNavigationButtons(false,false,true,true);
			}

			lblCPErrorMessage.Text = "";
			DisplayRefRecNum();

		}

		private void btnFirstRef_Click(object sender, System.EventArgs e)
		{
			ViewState["currentRefSet"] = 0;	
			GetRefRecSet();
			ViewState["currentRefPage"] = 0;
			DisplayCurrentRefPage();
			DisplayRefRecNum();
			EnableRefNavigationButtons(false,false,true,true);
		}

		private void btnPreviousRef_Click(object sender, System.EventArgs e)
		{
			int iCurrentPage = Convert.ToInt32(ViewState["currentRefPage"]);

			if(Convert.ToInt32(ViewState["currentRefPage"])  > 0 )
			{
				ViewState["currentRefPage"] =  Convert.ToInt32(ViewState["currentRefPage"]) - 1;
				DisplayCurrentRefPage();
			}
			else
			{
				if( (Convert.ToInt32(ViewState["currentRefSet"]) - 1) < 0)
				{
					EnableRefNavigationButtons(false,false,true,true);
					return;
				}
				
				ViewState["currentRefSet"] =  Convert.ToInt32(ViewState["currentRefSet"]) - 1;	
				GetRefRecSet();
				ViewState["currentRefPage"] = m_sdsReference.DataSetRecSize - 1;
				DisplayCurrentRefPage();
			}
			DisplayRefRecNum();
			EnableRefNavigationButtons(true,true,true,true);
	
		}

		private void btnNextRef_Click(object sender, System.EventArgs e)
		{
			int iCurrentPage = (int)ViewState["currentRefPage"];

			if((int)ViewState["currentRefPage"]  < (m_sdsReference.DataSetRecSize - 1) )
			{
				ViewState["currentRefPage"] = (int)ViewState["currentRefPage"] + 1;
				DisplayCurrentRefPage();
			}
			else
			{
				decimal iTotalRec = (System.Convert.ToDecimal(ViewState["currentRefSet"]) * m_iSetSize) + m_sdsReference.DataSetRecSize;
				if( iTotalRec ==  m_sdsReference.QueryResultMaxSize)
				{
					EnableRefNavigationButtons(true,true,false,false);
					return;
				}
				
				ViewState["currentRefSet"] = (int)ViewState["currentRefSet"] + 1;	
				GetRefRecSet();
				ViewState["currentRefPage"] = 0;
				DisplayCurrentRefPage();
			}
			DisplayRefRecNum();
			EnableRefNavigationButtons(true,true,true,true);	
		}

		private void btnLastRef_Click(object sender, System.EventArgs e)
		{
			ViewState["currentRefSet"] = (m_sdsReference.QueryResultMaxSize - 1)/m_iSetSize;	
			GetRefRecSet();
			ViewState["currentRefPage"] = m_sdsReference.DataSetRecSize - 1;
			DisplayCurrentRefPage();	
			DisplayRefRecNum();
			EnableRefNavigationButtons(true,true,false,false);
	
		}

		private void EnableRefNavigationButtons(bool bMoveFirst, bool bMoveNext,bool bEnableMovePrevious, bool bMoveLast)
		{

			Button btnFirstRef = (Button)agentProfileMultiPage.FindControl("btnFirstRef");
			if(btnFirstRef != null)
			{
				btnFirstRef.Enabled	= bMoveFirst;
			}
			
			Button btnPreviousRef = (Button)agentProfileMultiPage.FindControl("btnPreviousRef");
			if(btnPreviousRef != null)
			{
				btnPreviousRef.Enabled	= bMoveNext;
			}
			
			Button btnNextRef = (Button)agentProfileMultiPage.FindControl("btnNextRef");
			if(btnNextRef != null)
			{
				btnNextRef.Enabled	= bEnableMovePrevious;
			}
			
			Button btnLastRef = (Button)agentProfileMultiPage.FindControl("btnLastRef");
			if(btnLastRef != null)
			{
				btnLastRef.Enabled	= bMoveLast;
			}
		}

		private void GetRefRecSet()
		{
			decimal iStartIndex = Convert.ToInt32(ViewState["currentRefSet"]) * m_iSetSize;			
			String strAgentID = (String)ViewState["currentAgent"];
			m_sdsReference = AgentProfileMgrDAL.GetReferenceDS(utility.GetAppID(),utility.GetEnterpriseID(),strAgentID,Convert.ToInt32(iStartIndex),m_iSetSize);
			
			Session["SESSION_DS2"] = m_sdsReference;
			decimal pgCnt = (m_sdsReference.QueryResultMaxSize - 1)/m_iSetSize;
			if(pgCnt < Convert.ToDecimal(ViewState["currentRefSet"]))
			{
				ViewState["currentRefSet"] = System.Convert.ToInt32(pgCnt);
			}
		}

		private void DisplayCurrentRefPage()		
		{
			DataRow drCurrent = m_sdsAgentProfile.ds.Tables[0].Rows[Convert.ToInt32(ViewState["currentPage"])];
			DataRow drCurrentRef = m_sdsReference.ds.Tables[0].Rows[Convert.ToInt32(ViewState["currentRefPage"])];

			TextBox txtCustAccDispTab3 = (TextBox)agentProfileMultiPage.FindControl("txtCustAccDispTab3");			
			if(txtCustAccDispTab3 != null)
			{
				if(drCurrent["agentid"]!= null && (!drCurrent["agentid"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					txtCustAccDispTab3.Text = drCurrent["agentid"].ToString();
				}
				else
				{
					txtCustAccDispTab3.Text = "";
				}
			}

			TextBox txtCustNameDispTab3 = (TextBox)agentProfileMultiPage.FindControl("txtCustNameDispTab3");
			if(txtCustNameDispTab3 != null)
			{
				if(drCurrent["agent_name"]!= null && (!drCurrent["agent_name"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					txtCustNameDispTab3.Text = drCurrent["agent_name"].ToString();
				}
				else
				{
					txtCustNameDispTab3.Text = "";
				}
			}
			
			TextBox txtRefSndRecName = (TextBox)agentProfileMultiPage.FindControl("txtRefSndRecName");
			if(txtRefSndRecName != null)
			{
				if(drCurrentRef["snd_rec_name"]!= null && (!drCurrentRef["snd_rec_name"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					txtRefSndRecName.Text = drCurrentRef["snd_rec_name"].ToString();
				}
				else
				{
					txtRefSndRecName.Text = "";
				}

				EnableTxtRefName();
			}

			TextBox txtRefContactPerson = (TextBox)agentProfileMultiPage.FindControl("txtRefContactPerson");
			if(txtRefContactPerson != null)
			{
				if(drCurrentRef["contact_person"]!= null && (!drCurrentRef["contact_person"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					txtRefContactPerson.Text = drCurrentRef["contact_person"].ToString();
				}
				else
				{
					txtRefContactPerson.Text = "";
				}

			}
			
			TextBox txtRefEmail = (TextBox)agentProfileMultiPage.FindControl("txtRefEmail");
			if(txtRefEmail != null)
			{
				if(drCurrentRef["email"]!= null && (!drCurrentRef["email"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					txtRefEmail.Text = drCurrentRef["email"].ToString();
				}
				else
				{
					txtRefEmail.Text = "";
				}
			}

			TextBox txtRefAddress1 = (TextBox)agentProfileMultiPage.FindControl("txtRefAddress1");
			if(txtRefAddress1 != null)
			{
				if(drCurrentRef["address1"]!= null && (!drCurrentRef["address1"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					txtRefAddress1.Text = drCurrentRef["address1"].ToString();
				}
				else
				{
					txtRefAddress1.Text = "";
				}
			}
			
			TextBox txtRefAddress2 = (TextBox)agentProfileMultiPage.FindControl("txtRefAddress2");
			if(txtRefAddress2 != null)
			{
				if(drCurrentRef["address2"]!= null && (!drCurrentRef["address2"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					txtRefAddress2.Text = drCurrentRef["address2"].ToString();
				}
				else
				{
					txtRefAddress2.Text = "";
				}
			}			

			TextBox txtRefCountry = (TextBox)agentProfileMultiPage.FindControl("txtRefCountry");
			if(txtRefCountry != null)
			{
				if(drCurrentRef["country"]!= null && (!drCurrentRef["country"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					txtRefCountry.Text = drCurrentRef["country"].ToString();
				}
				else
				{
					txtRefCountry.Text = "";
				}
			}
			

			msTextBox txtRefZipcode = (msTextBox)agentProfileMultiPage.FindControl("txtRefZipcode");
			TextBox txtRefState = (TextBox)agentProfileMultiPage.FindControl("txtRefState");
			if(txtRefZipcode != null)
			{
				if(drCurrentRef["zipcode"]!= null && (!drCurrentRef["zipcode"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					txtRefZipcode.Text = drCurrentRef["zipcode"].ToString();
					Zipcode zipcode = new Zipcode();
					zipcode.Populate(utility.GetAppID(),utility.GetEnterpriseID(),txtRefZipcode.Text);
					zipcode.Populate(utility.GetAppID(),utility.GetEnterpriseID(),zipcode.Country,txtRefZipcode.Text);
					txtRefState.Text = zipcode.StateName;

				}
				else
				{
					txtRefZipcode.Text = "";
					txtRefState.Text = "";

				}
			}
			

			TextBox txtRefTelephone = (TextBox)agentProfileMultiPage.FindControl("txtRefTelephone");
			if(txtRefTelephone != null)
			{
				if(drCurrentRef["telephone"]!= null && (!drCurrentRef["telephone"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					txtRefTelephone.Text = drCurrentRef["telephone"].ToString();
				}
				else
				{
					txtRefTelephone.Text = "";
				}
			}
			

			TextBox txtRefFax = (TextBox)agentProfileMultiPage.FindControl("txtRefFax");
			if(txtRefFax != null)
			{
				if(drCurrentRef["fax"]!= null && (!drCurrentRef["fax"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					txtRefFax.Text = drCurrentRef["fax"].ToString();
				}
				else
				{
					txtRefFax.Text = "";
				}
			}

			CheckBox chkSender = (CheckBox)agentProfileMultiPage.FindControl("chkSender");
			CheckBox chkRecipient = (CheckBox)agentProfileMultiPage.FindControl("chkRecipient");

			if(chkSender != null && chkRecipient != null)
			{
				if(drCurrentRef["snd_rec_type"]!= null && (!drCurrentRef["snd_rec_type"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					String strPaymentMode = drCurrentRef["snd_rec_type"].ToString();

					if(strPaymentMode == "S")
					{
						chkSender.Checked = true;
						chkRecipient.Checked = false;
					}
					else if(strPaymentMode == "R")
					{
						chkSender.Checked = false;
						chkRecipient.Checked = true;
					}
					else if(strPaymentMode == "B")
					{
						chkSender.Checked = true;
						chkRecipient.Checked = true;
					}
				}
				else
				{
					chkSender.Checked = false;
					chkRecipient.Checked = false;
				}
			}			
		}

		private void EnableTxtRefName()
		{
// This method is to SUPPRESS the required field validation of another TAB PAGE
// for example Reference Page validated in Status & Exception field
			TextBox txtRefSndRecName = (TextBox)agentProfileMultiPage.FindControl("txtRefSndRecName");
			RequiredFieldValidator validateRefSndRecName = (RequiredFieldValidator)agentProfileMultiPage.FindControl("validateRefSndRecName");
			// Ref Contact Person
			TextBox txtRefContactPerson = (TextBox)agentProfileMultiPage.FindControl("txtRefContactPerson");
			RequiredFieldValidator valContactPerson = (RequiredFieldValidator)agentProfileMultiPage.FindControl("valContactPerson");
			// Ref Address1
			TextBox txtRefAddress1 = (TextBox)agentProfileMultiPage.FindControl("txtRefAddress1");
			RequiredFieldValidator valAddress1 = (RequiredFieldValidator)agentProfileMultiPage.FindControl("valAddress1");
			// Ref Telephone
			TextBox txtRefTelephone = (TextBox)agentProfileMultiPage.FindControl("txtRefTelephone");
			RequiredFieldValidator valTelephone = (RequiredFieldValidator)agentProfileMultiPage.FindControl("valTelephone");
			// Ref Zipcode
			TextBox txtRefZipcode = (TextBox)agentProfileMultiPage.FindControl("txtRefZipcode");
			RequiredFieldValidator valZipcode = (RequiredFieldValidator)agentProfileMultiPage.FindControl("valZipcode");

			if(txtRefSndRecName != null)
			{
				if((int)ViewState["RefMode"] == (int) ScreenMode.Insert && (int)ViewState["RefOperation"] == (int)Operation.None)
				{
					txtRefSndRecName.Enabled = true;
					
				}
				else if((int)ViewState["RefMode"] == (int) ScreenMode.Insert && (int)ViewState["RefOperation"] == (int)Operation.Saved)
				{
					txtRefSndRecName.Enabled = false;
				}
				else if((int)ViewState["RefMode"] == (int) ScreenMode.ExecuteQuery)
				{
					txtRefSndRecName.Enabled = false;
				}
				else if((int)ViewState["RefMode"] == (int) ScreenMode.Query)
				{
					txtRefSndRecName.Enabled = true;;
				}
				else
				{
					txtRefSndRecName.Enabled = false;
				}


				if(validateRefSndRecName != null)
				{
					if(txtRefSndRecName.Enabled && IsRefTabEnabled())
					{
						validateRefSndRecName.ControlToValidate = "txtRefSndRecName";
					}
					else
					{
						validateRefSndRecName.ControlToValidate = "supposrtValidator";
					}
				}
				//
				if(valContactPerson != null)
				{
					if(txtRefSndRecName.Enabled && txtRefContactPerson.Enabled && IsRefTabEnabled())
					{
						valContactPerson.ControlToValidate = "txtRefContactPerson";
					}
					else
					{
						valContactPerson.ControlToValidate = "suppContactPerson";
					}
				}
				//
				if(valAddress1 != null)
				{
					if(txtRefSndRecName.Enabled && txtRefAddress1.Enabled && IsRefTabEnabled())
					{
						valAddress1.ControlToValidate = "txtRefAddress1";
					}
					else
					{
						valAddress1.ControlToValidate = "suppAddress1";
					}
				}
				//
				if(valTelephone != null)
				{
					if(txtRefSndRecName.Enabled && txtRefTelephone.Enabled && IsRefTabEnabled())
					{
						valTelephone.ControlToValidate = "txtRefTelephone";
					}
					else
					{
						valTelephone.ControlToValidate = "suppTelephone";
					}
				}
				//
				if(valZipcode != null)
				{
					if(txtRefSndRecName.Enabled && txtRefZipcode.Enabled && IsRefTabEnabled())
					{
						valZipcode.ControlToValidate = "txtRefZipcode";
					}
					else
					{
						valZipcode.ControlToValidate = "suppZipcode";
					}
				}
			}
		}

		private void DisplayRefRecNum()
		{
			int iCurrentRec = (Convert.ToInt32(ViewState["currentRefPage"]) + 1) + (Convert.ToInt32(ViewState["currentRefSet"])* m_iSetSize) ;	
			if(lblNumRefRec != null)
			{
				if(!utility.GetUserCulture().ToUpper().Equals("EN-US"))
				{
					lblNumRec.Text = ""+ iCurrentRec + " " + Utility.GetLanguageText(ResourceType.ScreenLabel, "of", utility.GetUserCulture()) + " " + m_sdsReference.QueryResultMaxSize + " " + Utility.GetLanguageText(ResourceType.ScreenLabel, "record(s)", utility.GetUserCulture());
				}
				else
				{
					lblNumRec.Text = ""+ iCurrentRec + " of " + m_sdsReference.QueryResultMaxSize + " record(s)";
				}
			}
		}

		private void ClearRefPageForInsert()
		{
			m_sdsReference = AgentProfileMgrDAL.GetEmptyReferenceDS(1);
			Session["SESSION_DS2"] = m_sdsReference;
			ViewState["IsRefTextChanged"] = false;
			ViewState["currentRefPage"] = 0;

			DisplayCurrentRefPage();
			
			//lblRefNumRec.Text = "";
			lblCPErrorMessage.Text = "";
		}

		private void EnableRefPage(bool enable)
		{			
			TextBox txtRefSndRecName = (TextBox)agentProfileMultiPage.FindControl("txtRefSndRecName");
			if(txtRefSndRecName != null)
			{
				txtRefSndRecName.Enabled = enable;
			}

			TextBox txtRefContactPerson = (TextBox)agentProfileMultiPage.FindControl("txtRefContactPerson");
			if(txtRefContactPerson != null)
			{
				txtRefContactPerson.Enabled = enable;
			}
			
			TextBox txtRefEmail = (TextBox)agentProfileMultiPage.FindControl("txtRefEmail");
			if(txtRefEmail != null)
			{
				txtRefEmail.Enabled = enable;				
			}

			TextBox txtRefAddress1 = (TextBox)agentProfileMultiPage.FindControl("txtRefAddress1");
			if(txtRefAddress1 != null)
			{
				txtRefAddress1.Enabled = enable;
			}
			
			TextBox txtRefAddress2 = (TextBox)agentProfileMultiPage.FindControl("txtRefAddress2");
			if(txtRefAddress2 != null)
			{
				txtRefAddress2.Enabled = enable;
			}
			
			TextBox txtRefCountry = (TextBox)agentProfileMultiPage.FindControl("txtRefCountry");
			if(txtRefCountry != null)
			{
				txtRefCountry.Enabled = enable;
			}
			
			msTextBox txtRefZipcode = (msTextBox)agentProfileMultiPage.FindControl("txtRefZipcode");
			if(txtRefZipcode != null)
			{
				txtRefZipcode.Enabled = enable;
			}
			
			TextBox txtRefTelephone = (TextBox)agentProfileMultiPage.FindControl("txtRefTelephone");
			if(txtRefTelephone != null)
			{
				txtRefTelephone.Enabled = enable;
			}

			TextBox txtRefFax = (TextBox)agentProfileMultiPage.FindControl("txtRefFax");
			if(txtRefFax != null)
			{
				txtRefFax.Enabled	= enable;
			}

			CheckBox chkSender = (CheckBox)agentProfileMultiPage.FindControl("chkSender");
			CheckBox chkRecipient = (CheckBox)agentProfileMultiPage.FindControl("chkRecipient");

			if(chkSender != null && chkRecipient != null)
			{
				chkSender.Enabled = enable;
				chkRecipient.Enabled = enable;
			}

			Button btnRefSave = (Button)agentProfileMultiPage.FindControl("btnRefSave");
			Button btnRefZipcodeSearch = (Button)agentProfileMultiPage.FindControl("btnRefZipcodeSearch");

			btnRefSave.Enabled = enable;
			btnRefZipcodeSearch.Enabled = enable;

		}

		private void btnRefSave_Click(object sender, System.EventArgs e)
		{
			FillReferenceDataRow(Convert.ToInt32(ViewState["currentRefPage"]));
			String strAgentID = (String)ViewState["currentAgent"];
			
			CheckBox chkS=(CheckBox) agentProfileMultiPage.FindControl("chkSender");
			CheckBox chkR=(CheckBox) agentProfileMultiPage.FindControl("chkRecipient");
			if (chkS.Checked==false && chkR.Checked==false)
			{
				lblCPErrorMessage.Text= Utility.GetLanguageText(ResourceType.UserMessage,"SEL_REF_TYPE",utility.GetUserCulture());
				return;
			}
			else
			{
				lblCPErrorMessage.Text="";
			}
			
			int iOperation = Convert.ToInt32(ViewState["RefOperation"]);
			
			switch(iOperation)
			{

				case (int)Operation.Update:

					DataSet dsToUpdate = m_sdsReference.ds.GetChanges();

					try
					{
						AgentProfileMgrDAL.ModifyReferenceDS(utility.GetAppID(),utility.GetEnterpriseID(),strAgentID,dsToUpdate);
						Button btnRefInsert = (Button)agentProfileMultiPage.FindControl("btnRefInsert");

						if(btnRefInsert != null)
						{
							btnRefInsert.Enabled	= true;
						}

						ViewState["IsRefTextChanged"] = false;
						ChangeRefState();
						m_sdsReference.ds.Tables[0].Rows[Convert.ToInt32(ViewState["currentRefPage"])].AcceptChanges();

						if(lblNumRefRec != null)
						{
							int iCurrentRec = Convert.ToInt32(ViewState["currentRefPage"]) + 1;
							lblNumRefRec.Text = "Reference "+ iCurrentRec + " modified successfully.";
						}

						Logger.LogDebugInfo("AgentProfile","btnSave_Click","INF004","save in modify mode..");
					}
					catch(ApplicationException appException)
					{
						String strMsg = appException.Message;

						if(strMsg.IndexOf("duplicate key") != -1 )
						{
							lblCPErrorMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"DUP_KEY_FOUND_NAME",utility.GetUserCulture());
						}
						else if(strMsg.IndexOf("FK") != -1 )
						{
							if(strMsg.IndexOf("Zipcode") != -1)
							{
								lblCPErrorMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"INVALID_ZIP",utility.GetUserCulture());
							}
						}
						if(strMsg.IndexOf("parent") != -1 )
						{
							lblCPErrorMessage.Text =Utility.GetLanguageText(ResourceType.UserMessage,"INVALID_ZIP",utility.GetUserCulture());
						}
						return;						
					}
					break;

				case (int)Operation.Insert:

					DataSet dsToInsert = m_sdsReference.ds.GetChanges();
					try
					{
						AgentProfileMgrDAL.AddReferenceDS(utility.GetAppID(),utility.GetEnterpriseID(),strAgentID,dsToInsert);

						Button btnRefInsert = (Button)agentProfileMultiPage.FindControl("btnRefInsert");

						if(btnRefInsert != null)
						{
							btnRefInsert.Enabled	= true;
						}

						TextBox txtRefSndRecName = (TextBox)agentProfileMultiPage.FindControl("txtRefSndRecName");
						if(txtRefSndRecName != null)
						{
							txtRefSndRecName.Enabled = false;
						}

						ViewState["IsRefTextChanged"] = false;
						ChangeRefState();
						m_sdsReference.ds.Tables[0].Rows[(int)ViewState["currentRefPage"]].AcceptChanges();

						if(lblNumRefRec != null)
						{
							lblNumRefRec.Text = "1 reference added successfully.";
						}

						Logger.LogDebugInfo("AgentProfile","btnSave_Click","INF004","save in Insert mode");
					}
					catch(ApplicationException appException)
					{
						String strMsg = appException.Message;

						if(strMsg.IndexOf("duplicate key") != -1 )
						{
							lblCPErrorMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"DUP_KEY_FOUND_NAME",utility.GetUserCulture());
						}
						else if(strMsg.IndexOf("FK") != -1 )
						{
							if(strMsg.IndexOf("Zipcode") != -1)
							{
								lblCPErrorMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"INVALID_ZIP",utility.GetUserCulture());
							}
						}
						else if(strMsg.IndexOf("parent") != -1 )
						{
							lblCPErrorMessage.Text =Utility.GetLanguageText(ResourceType.UserMessage,"INVALID_ZIP",utility.GetUserCulture());
						}
						else
						{
							lblCPErrorMessage.Text = strMsg;
						}

						return;
					}
					break;
			}			

		}

		private void btnRefDelete_Click(object sender, System.EventArgs e)
		{
		
		}

		private void FillReferenceDataRow(int iCurrentRow)
		{
			DataRow drCurrentRef = m_sdsReference.ds.Tables[0].Rows[iCurrentRow];

			TextBox txtRefSndRecName = (TextBox)agentProfileMultiPage.FindControl("txtRefSndRecName");
			if(txtRefSndRecName != null && txtRefSndRecName.Text != "")
			{
				drCurrentRef["snd_rec_name"] = txtRefSndRecName.Text;
			}

			TextBox txtRefContactPerson = (TextBox)agentProfileMultiPage.FindControl("txtRefContactPerson");
			if(txtRefContactPerson != null && txtRefContactPerson.Text != "")
			{
				drCurrentRef["contact_person"] = txtRefContactPerson.Text;
			}

			TextBox txtRefAddress1 = (TextBox)agentProfileMultiPage.FindControl("txtRefAddress1");
			if(txtRefAddress1 != null && txtRefAddress1.Text != "")
			{
				drCurrentRef["address1"] = txtRefAddress1.Text;
			}

			TextBox txtRefAddress2 = (TextBox)agentProfileMultiPage.FindControl("txtRefAddress2");
			if(txtRefAddress2 != null && txtRefAddress2.Text != "")
			{
				drCurrentRef["address2"] = txtRefAddress2.Text;
			}

			TextBox txtRefCountry = (TextBox)agentProfileMultiPage.FindControl("txtRefCountry");
			if(txtRefCountry != null && txtRefCountry.Text != "")
			{
				drCurrentRef["country"] = txtRefCountry.Text;
			}

			msTextBox txtRefZipcode = (msTextBox)agentProfileMultiPage.FindControl("txtRefZipcode");
			if(txtRefZipcode != null && txtRefZipcode.Text != "")
			{
				drCurrentRef["zipcode"] = txtRefZipcode.Text;
			}

			TextBox txtRefTelephone = (TextBox)agentProfileMultiPage.FindControl("txtRefTelephone");
			if(txtRefTelephone != null && txtRefTelephone.Text != "")
			{
				drCurrentRef["telephone"] = txtRefTelephone.Text;
			}

			TextBox txtRefFax = (TextBox)agentProfileMultiPage.FindControl("txtRefFax");
			if(txtRefFax != null && txtRefFax.Text != "")
			{
				drCurrentRef["fax"] = txtRefFax.Text;
			}

			TextBox txtRefEmail = (TextBox)agentProfileMultiPage.FindControl("txtRefEmail");
			if(txtRefEmail != null && txtRefEmail.Text != "")
			{
				drCurrentRef["email"] = txtRefEmail.Text;
			}

			CheckBox chkSender = (CheckBox)agentProfileMultiPage.FindControl("chkSender");
			CheckBox chkRecipient = (CheckBox)agentProfileMultiPage.FindControl("chkRecipient");
			if(chkSender != null && chkRecipient != null)
			{

				if(chkSender.Checked == true && chkRecipient.Checked == false)
				{
					drCurrentRef["snd_rec_type"] = "S";
				}
				else if(chkSender.Checked == false && chkRecipient.Checked == true)
				{
					drCurrentRef["snd_rec_type"] = "R";
				}
				else if(chkSender.Checked == true && chkRecipient.Checked == true)
				{
					drCurrentRef["snd_rec_type"] = "B";
				}
				else if(chkSender.Checked == false && chkRecipient.Checked == false)
				{
					drCurrentRef["snd_rec_type"] = System.DBNull.Value;
				}
			}
		}

		private void ChangeRefState()
		{
			if((int)ViewState["RefMode"] == (int) ScreenMode.Insert && (int)ViewState["RefOperation"] == (int)Operation.None)
			{
				ViewState["RefOperation"] = Operation.Insert;
			}
			else if((int)ViewState["RefMode"] == (int) ScreenMode.Insert && (int)ViewState["RefOperation"] == (int)Operation.Insert)
			{
				ViewState["RefOperation"] = Operation.Saved;
			}
			else if((int)ViewState["RefMode"] == (int) ScreenMode.Insert && (int)ViewState["RefOperation"] == (int)Operation.Saved)
			{
				ViewState["RefOperation"] = Operation.Update;
			}
			else if((int)ViewState["RefMode"] == (int) ScreenMode.Insert && (int)ViewState["RefOperation"] == (int)Operation.Update)
			{
				ViewState["RefOperation"] = Operation.Saved;
			}
			else if((int)ViewState["RefMode"] == (int) ScreenMode.ExecuteQuery && (int)ViewState["RefOperation"] == (int)Operation.None)
			{
				ViewState["RefOperation"] = Operation.Update;
			}
			else if((int)ViewState["RefMode"] == (int) ScreenMode.ExecuteQuery && (int)ViewState["RefOperation"] == (int)Operation.Update)
			{
				ViewState["RefOperation"] = Operation.None;
			}

			
		}

		private void txtRefSndRecName_TextChanged(object sender,System.EventArgs e)
		{
			if((bool)ViewState["IsRefTextChanged"] == false)
			{
				ChangeRefState();
				ViewState["IsRefTextChanged"] = true;
			}
		}

		private void txtRefContactPerson_TextChanged(object sender,System.EventArgs e)
		{
			if((bool)ViewState["IsRefTextChanged"] == false)
			{
				ChangeRefState();
				ViewState["IsRefTextChanged"] = true;
			}
		}

		private void txtRefAddress1_TextChanged(object sender,System.EventArgs e)
		{
			if((bool)ViewState["IsRefTextChanged"] == false)
			{
				ChangeRefState();
				ViewState["IsRefTextChanged"] = true;
			}
		}

		private void txtRefAddress2_TextChanged(object sender,System.EventArgs e)
		{
			if((bool)ViewState["IsRefTextChanged"] == false)
			{
				ChangeRefState();
				ViewState["IsRefTextChanged"] = true;
			}
		}

		private void txtRefCountry_TextChanged(object sender,System.EventArgs e)
		{
			if((bool)ViewState["IsRefTextChanged"] == false)
			{
				ChangeRefState();
				ViewState["IsRefTextChanged"] = true;
			}
		}

		private void txtRefZipcode_TextChanged(object sender,System.EventArgs e)
		{
			if((bool)ViewState["IsRefTextChanged"] == false)
			{
				ChangeRefState();
				ViewState["IsRefTextChanged"] = true;
			}
		}

		private void txtRefTelephone_TextChanged(object sender,System.EventArgs e)
		{
			if((bool)ViewState["IsRefTextChanged"] == false)
			{
				ChangeRefState();
				ViewState["IsRefTextChanged"] = true;
			}
		}

		private void txtRefFax_TextChanged(object sender,System.EventArgs e)
		{
			if((bool)ViewState["IsRefTextChanged"] == false)
			{
				ChangeRefState();
				ViewState["IsRefTextChanged"] = true;
			}
		}

		private void txtRefEmail_TextChanged(object sender,System.EventArgs e)
		{
			if((bool)ViewState["IsRefTextChanged"] == false)
			{
				ChangeRefState();
				ViewState["IsRefTextChanged"] = true;
			}
		}


		private void chkSender_CheckedChanged(object sender,System.EventArgs e)
		{
			if((bool)ViewState["IsRefTextChanged"] == false)
			{
				ChangeRefState();
				ViewState["IsRefTextChanged"] = true;
			}
		}

		private void chkRecipient_CheckedChanged(object sender,System.EventArgs e)
		{
			if((bool)ViewState["IsRefTextChanged"] == false)
			{
				ChangeRefState();
				ViewState["IsRefTextChanged"] = true;
			}
			
		}

		// Status and Exception Surcharges Section...

		private void ShowSCForCurrentAgent()
		{
			ViewState["SCMode"] = ScreenMode.ExecuteQuery;
			ViewState["SCOperation"] = Operation.None;

			//ViewState["QUERY_DS"] = m_sdsStatusCode.ds;
			dgStatusCodes.CurrentPageIndex = 0;
			ShowCurrentSCPage();
			btnSCInsert.Enabled = true;
			lblSCMessage.Text = "";
		}
		protected void btnSCViewAll_Click(object sender, System.EventArgs e)
		{
			ShowSCForCurrentAgent();
		}

		protected void btnSCInsert_Click(object sender, System.EventArgs e)
		{
			if((int)ViewState["SCMode"] != (int)ScreenMode.Insert || m_sdsStatusCode.ds.Tables[0].Rows.Count >= dgStatusCodes.PageSize)
			{
				m_sdsStatusCode = AgentProfileMgrDAL.GetEmptyCPStatusCodeDS(0);
			}

			AddRowInSCGrid();	
			dgStatusCodes.EditItemIndex = m_sdsStatusCode.ds.Tables[0].Rows.Count - 1;
			dgStatusCodes.CurrentPageIndex = 0;
			Logger.LogDebugInfo("Agent Profile","btnSCInsert_Click","INF003","Data Grid Items count"+dgStatusCodes.Items.Count);
			
			ViewState["SCMode"] = ScreenMode.Insert;
			ViewState["SCOperation"] = Operation.Insert;
			btnSCInsert.Enabled = false;
			//btnExecQry.Enabled = false;
			lblSCMessage.Text = "";
			BindSCGrid();
			ResetDetailsGrid();
			getPageControls(Page);
		}
		
		protected void dgStatusCodes_Edit(object sender, DataGridCommandEventArgs e)
		{
			if((int)ViewState["SCMode"] == (int)ScreenMode.Insert && (int) ViewState["SCOperation"] == (int)Operation.Insert && dgStatusCodes.EditItemIndex > 0)
			{
				m_sdsStatusCode.ds.Tables[0].Rows.RemoveAt(dgStatusCodes.EditItemIndex);
				m_sdsStatusCode.QueryResultMaxSize--;
				dgStatusCodes.CurrentPageIndex = 0;
			}

			dgStatusCodes.EditItemIndex = e.Item.ItemIndex;
			ViewState["SCOperation"] = Operation.Update;
			BindSCGrid();
			lblSCMessage.Text = "";
			Logger.LogDebugInfo("StatusException","dgStatusCodes_Edit","INF004","updating data grid...");			
		}
		
		public void dgStatusCodes_Update(object sender, DataGridCommandEventArgs e)
		{

			FillSCDataRow(e.Item,e.Item.ItemIndex);
			String strAgentID = (String)ViewState["currentAgent"];

			int iOperation = (int)ViewState["SCOperation"];
			switch(iOperation)
			{

				case (int)Operation.Update:

					DataSet dsToUpdate = m_sdsStatusCode.ds.GetChanges();
					AgentProfileMgrDAL.ModifyCPStatusCodeDS(utility.GetAppID(),utility.GetEnterpriseID(),strAgentID,dsToUpdate);
					btnSCInsert.Enabled = true;
					m_sdsStatusCode.ds.Tables[0].Rows[e.Item.ItemIndex].AcceptChanges();
					Logger.LogDebugInfo("StatusException","dgStatusCodes_OnUpdate","INF004","update in modify mode..");

					break;

				case (int)Operation.Insert:

					DataSet dsToInsert = m_sdsStatusCode.ds.GetChanges();
					try
					{
						AgentProfileMgrDAL.AddCPStatusCodeDS(utility.GetAppID(),utility.GetEnterpriseID(),strAgentID,dsToInsert);
						btnSCInsert.Enabled = true;
					}
					catch(ApplicationException appException)
					{
						String strMsg = appException.Message;
						strMsg = appException.InnerException.Message;
						strMsg = appException.InnerException.InnerException.Message;
						if(strMsg.IndexOf("duplicate key") != -1 )
						{
							lblSCMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"DUP_KEY_FOUND",utility.GetUserCulture());							
						}
						else if (strMsg.IndexOf("conflict") != -1 )
						{
							lblSCMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"INVALID_STATUS_CODE",utility.GetUserCulture());												
						}
						else
						{
							lblSCMessage.Text=strMsg;
						}
						return;
						
					}
					m_sdsStatusCode.ds.Tables[0].Rows[e.Item.ItemIndex].AcceptChanges();
					Logger.LogDebugInfo("StatusException","dgStatusCodes_OnUpdate","INF004","update in Insert mode");
					break;

			}
			dgStatusCodes.EditItemIndex = -1;
			ViewState["SCOperation"] = Operation.None;
			lblSCMessage.Text = "";
			BindSCGrid();
		}

		protected void dgStatusCodes_Cancel(object sender, DataGridCommandEventArgs e)
		{
			dgStatusCodes.EditItemIndex = -1;

			if((int)ViewState["SCMode"] == (int)ScreenMode.Insert && (int)ViewState["SCOperation"] == (int)Operation.Insert)
			{
				m_sdsStatusCode.ds.Tables[0].Rows.RemoveAt(e.Item.ItemIndex);
				m_sdsStatusCode.QueryResultMaxSize--;
				dgStatusCodes.CurrentPageIndex = 0;
			}
			ViewState["SCOperation"] = Operation.None;
			BindSCGrid();
			btnSCInsert.Enabled = true;
			lblSCMessage.Text = "";
			Logger.LogDebugInfo("StatusException","dgStatusCodes_Cancel","INF004","updating data grid...");			
		}
		
		private void BindSCGrid()
		{
			dgStatusCodes.VirtualItemCount = System.Convert.ToInt32(m_sdsStatusCode.QueryResultMaxSize);
			dgStatusCodes.DataSource = m_sdsStatusCode.ds;
			dgStatusCodes.DataBind();
			Session["SESSION_DS3"] = m_sdsStatusCode;
		}

		private void AddRowInSCGrid()
		{
			AgentProfileMgrDAL.AddNewRowInCPStatusCodeDS(m_sdsStatusCode);
			Session["SESSION_DS3"] = m_sdsStatusCode;
		}

		public void dgStatusCodes_Button(object sender, DataGridCommandEventArgs e)
		{
			if(!e.Item.Cells[4].Enabled)
			{
				return;
			}
			String strCmdNm = e.CommandName;
			if(strCmdNm.Equals("search"))
			{
				msTextBox txtStatusCode = (msTextBox)e.Item.FindControl("txtStatusCode");
				//msTextBox txtStatusCode = (msTextBox)dgStatusCodes.Items[e.Item.ItemIndex].FindControl("txtStatusCode");


				String strStatusCodeClientID = null;
				String strStatusDescriptionClientID = null;

				String strStatusCode = null;
				
				if(txtStatusCode != null)
				{
					strStatusCodeClientID = txtStatusCode.ClientID;
					strStatusCode = txtStatusCode.Text;
				}

				TextBox txtStatusDescription = (TextBox)e.Item.FindControl("txtStatusDescription");
				if(txtStatusDescription != null)
				{
					strStatusDescriptionClientID = txtStatusDescription.ClientID;
				}

				String sUrl = "StatusCodePopup1.aspx?FORMID=AgentProfile&STATUSCODE="+strStatusCode+"&STATUSCODE_CID="+strStatusCodeClientID+"&SCDESCRIPTION_CID="+strStatusDescriptionClientID;
				ArrayList paramList = new ArrayList();
				paramList.Add(sUrl);
				String sScript = Utility.GetScript("openWindowParam.js",paramList);
				Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
			}			
		}
		
		protected void dgStatusCodes_Bound(object sender, DataGridItemEventArgs e)
		{
			if(e.Item.ItemIndex == -1)
			{
				return;
			}

			DataRow drSelected = m_sdsStatusCode.ds.Tables[0].Rows[e.Item.ItemIndex];

			if(drSelected.RowState == DataRowState.Deleted)
			{
				return;
			}

			msTextBox txtStatusCode = (msTextBox)e.Item.FindControl("txtStatusCode");

			int iOperation = (int) ViewState["SCOperation"];
			if(txtStatusCode != null && iOperation == (int)Operation.Update)
			{
				txtStatusCode.Enabled = false;
			}

			if(e.Item.ItemType == ListItemType.EditItem && (int)ViewState["SCMode"] == (int)ScreenMode.Insert && (int)ViewState["SCOperation"] == (int)Operation.Insert)
			{
				e.Item.Cells[4].Enabled = true;
			}
			else
			{
				e.Item.Cells[4].Enabled = false;
			} 
		}

		protected void dgStatusCodes_PageChange(object sender, DataGridPageChangedEventArgs e)
		{
			dgStatusCodes.CurrentPageIndex = e.NewPageIndex;
			ShowCurrentSCPage();
			Logger.LogDebugInfo("StatusException","dgStatusCodes_PageChange","INF009","On Page Change "+e.NewPageIndex);
		}

		private void ShowCurrentSCPage()
		{
			DataRow drCurrent = m_sdsAgentProfile.ds.Tables[0].Rows[Convert.ToInt32(ViewState["currentPage"])];

			TextBox txtCustAccDispTab4 = (TextBox)agentProfileMultiPage.FindControl("txtCustAccDispTab4");
			if(txtCustAccDispTab4 != null)
			{
				if(drCurrent["agentid"]!= null && (!drCurrent["agentid"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					txtCustAccDispTab4.Text = drCurrent["agentid"].ToString();
				}
				else
				{
					txtCustAccDispTab4.Text = "";
				}
			}

			TextBox txtCustNameDispTab4 = (TextBox)agentProfileMultiPage.FindControl("txtCustNameDispTab4");
			if(txtCustNameDispTab4 != null)
			{
				if(drCurrent["agent_name"]!= null && (!drCurrent["agent_name"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					txtCustNameDispTab4.Text = drCurrent["agent_name"].ToString();
				}
				else
				{
					txtCustNameDispTab4.Text = "";
				}
			}

			int iStartIndex = dgStatusCodes.CurrentPageIndex * dgStatusCodes.PageSize;
			String strAgentID = (String)ViewState["currentAgent"];
			m_sdsStatusCode = AgentProfileMgrDAL.GetCPStatusCodeDS(utility.GetAppID(),utility.GetEnterpriseID(),strAgentID,iStartIndex,dgStatusCodes.PageSize);
			decimal pgCnt = (m_sdsStatusCode.QueryResultMaxSize - 1)/dgStatusCodes.PageSize;
			if(pgCnt < dgStatusCodes.CurrentPageIndex)
			{
				dgStatusCodes.CurrentPageIndex = System.Convert.ToInt32(pgCnt);
			}
			dgStatusCodes.SelectedIndex = -1;
			dgStatusCodes.EditItemIndex = -1;
			lblSCMessage.Text = "";

			BindSCGrid();
			ResetDetailsGrid();
		}


		protected void btnExInsert_Click(object sender, System.EventArgs e)
		{
			//---- If coming to any other mode to Insert Mode then create the empty data set.

			if((int)ViewState["EXMode"] != (int)ScreenMode.Insert || m_sdsExceptionCode.ds.Tables[0].Rows.Count >= dgExceptionCodes.PageSize)
			{
				m_sdsExceptionCode = AgentProfileMgrDAL.GetEmptyCPExceptionCodeDS(0);
			}
			AddRowInExGrid();	
			dgExceptionCodes.EditItemIndex = m_sdsExceptionCode.ds.Tables[0].Rows.Count - 1;
			dgExceptionCodes.CurrentPageIndex = 0;
			Logger.LogDebugInfo("StatusException","btnExInsert_Click","INF003","Data Grid Items count"+dgExceptionCodes.Items.Count);
			
			ViewState["EXMode"] = ScreenMode.Insert;
			ViewState["EXOperation"] = Operation.Insert;
			BindExGrid();
			btnExInsert.Enabled = false;
			getPageControls(Page);
		}

		public void dgExceptionCodes_Button(object sender, DataGridCommandEventArgs e)
		{
			
			String strCmdNm = e.CommandName;
			if(strCmdNm.Equals("search"))
			{
				msTextBox txtExceptionCode = (msTextBox)e.Item.FindControl("txtExceptionCode");

				String strExceptionCodeClientID = null;
				String strExceptionDescriptionClientID = null;
				String strStatusCode = (String)ViewState["CurrentSC"];

				String strExceptionCode = null;
				
				if(txtExceptionCode != null)
				{
					strExceptionCodeClientID = txtExceptionCode.ClientID;
					strExceptionCode = txtExceptionCode.Text;
				}

				TextBox txtExceptionDescription = (TextBox)e.Item.FindControl("txtExceptionDescription");
				if(txtExceptionDescription != null)
				{
					strExceptionDescriptionClientID = txtExceptionDescription.ClientID;
				}

				String sUrl = "ExceptionCodePopup.aspx?FORMID=AgentProfile&STATUSCODE="+strStatusCode+"&EXCEPTIONCODE="+strExceptionCode+"&EXCEPTIONCODE_CID="+strExceptionCodeClientID+"&EXDESCRIPTION_CID="+strExceptionDescriptionClientID;
				ArrayList paramList = new ArrayList();
				paramList.Add(sUrl);
				String sScript = Utility.GetScript("openWindowParam.js",paramList);
				Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
			}
			
		}

		protected void dgExceptionCodes_Edit(object sender, DataGridCommandEventArgs e)
		{
			if((int)ViewState["EXMode"] == (int)ScreenMode.Insert && (int) ViewState["EXOperation"] == (int)Operation.Insert && dgExceptionCodes.EditItemIndex > 0)
			{
				m_sdsExceptionCode.ds.Tables[0].Rows.RemoveAt(dgExceptionCodes.EditItemIndex);
				m_sdsExceptionCode.QueryResultMaxSize--;
				dgExceptionCodes.CurrentPageIndex = 0;
			}

			dgExceptionCodes.EditItemIndex = e.Item.ItemIndex;
			ViewState["EXOperation"] = Operation.Update;
			BindExGrid();
			lblEXMessage.Text = "";
			Logger.LogDebugInfo("StatusException","dgExceptionCodes_Edit","INF004","updating data grid...");			
		}

		public void dgExceptionCodes_Update(object sender, DataGridCommandEventArgs e)
		{
			FillEXDataRow(e.Item,e.Item.ItemIndex);
			String strAgentID = (String)ViewState["currentAgent"];
			int iOperation = (int)ViewState["EXOperation"];

			String strStatusCode = (String)ViewState["CurrentSC"];
			switch(iOperation)
			{

				case (int)Operation.Update:

					DataSet dsToUpdate = m_sdsExceptionCode.ds.GetChanges();
					AgentProfileMgrDAL.ModifyCPExceptionCodeDS(utility.GetAppID(),utility.GetEnterpriseID(),strAgentID,strStatusCode,dsToUpdate);
					btnExInsert.Enabled = true;
					m_sdsExceptionCode.ds.Tables[0].Rows[e.Item.ItemIndex].AcceptChanges();
					Logger.LogDebugInfo("StatusException","dgExceptionCodes_OnUpdate","INF004","update in modify mode..");

					break;

				case (int)Operation.Insert:

					DataSet dsToInsert = m_sdsExceptionCode.ds.GetChanges();
					try
					{
						AgentProfileMgrDAL.AddCPExceptionCodeDS(utility.GetAppID(),utility.GetEnterpriseID(),strAgentID,strStatusCode,dsToInsert);
						btnExInsert.Enabled = true;
					}
					catch(ApplicationException appException)
					{
						String strMsg = appException.Message;
						strMsg = appException.InnerException.Message;
						strMsg = appException.InnerException.InnerException.Message;
						if(strMsg.IndexOf("duplicate key") != -1 )
						{
							lblEXMessage.Text = "Duplicate Exception Code is not allowed. Please select another Exception Code.";
						}
						else if (strMsg.IndexOf("conflict") != -1 ) 
						{
							if(!utility.GetUserCulture().ToUpper().Equals("EN-US"))
							{
								lblEXMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage, "INVALID_EXCEPTION_CODE", utility.GetUserCulture());	
							} 
							else 
							{
								lblEXMessage.Text = "Exception Code is not valid. Please use a valid Exception Code.";
							}
						}
						else if (strMsg.IndexOf("parent") != -1 ) 
						{
							if(!utility.GetUserCulture().ToUpper().Equals("EN-US"))
							{
								lblEXMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage, "INVALID_EXCEPTION_CODE", utility.GetUserCulture());	
							} 
							else 
							{
								lblEXMessage.Text = "Exception Code is not valid. Please use a valid Exception Code.";
							}
						}
						else
						{
							lblEXMessage.Text = strMsg;
						}
						return;
						
					}

					m_sdsExceptionCode.ds.Tables[0].Rows[e.Item.ItemIndex].AcceptChanges();
					Logger.LogDebugInfo("StatusException","dgExceptionCodes_OnUpdate","INF004","update in Insert mode");
					break;

			}
			dgExceptionCodes.EditItemIndex = -1;
			ViewState["EXOperation"] = Operation.None;
			BindExGrid();		
			lblEXMessage.Text = "";
		}

		protected void dgExceptionCodes_Cancel(object sender, DataGridCommandEventArgs e)
		{
			dgExceptionCodes.EditItemIndex = -1;
			if((int)ViewState["EXMode"] == (int)ScreenMode.Insert && (int)ViewState["EXOperation"] == (int)Operation.Insert)
			{
				m_sdsExceptionCode.ds.Tables[0].Rows.RemoveAt(e.Item.ItemIndex);
				m_sdsExceptionCode.QueryResultMaxSize--;
				dgExceptionCodes.CurrentPageIndex = 0;
			}
			btnExInsert.Enabled = true;
			lblEXMessage.Text = "";

			ViewState["EXOperation"] = Operation.None;
			BindExGrid();
			Logger.LogDebugInfo("StatusException","dgExceptionCodes_Cancel","INF004","updating data grid...");			
		}

		protected void dgExceptionCodes_Delete(object sender, DataGridCommandEventArgs e)
		{
			msTextBox txtExceptionCode = (msTextBox)e.Item.FindControl("txtExceptionCode");
			Label lblExceptionCode = (Label)e.Item.FindControl("lblExceptionCode");
			String strExceptionCode = null;
			if(txtExceptionCode != null)
			{
				strExceptionCode = txtExceptionCode.Text;
			}

			if(lblExceptionCode != null)
			{
				strExceptionCode = lblExceptionCode.Text;
			}

			String strStatusCode = (String)ViewState["CurrentSC"];
			String strAgentID = (String)ViewState["currentAgent"];

			try
			{
				AgentProfileMgrDAL.DeleteCPExceptionCodeDS(utility.GetAppID(),utility.GetEnterpriseID(),strAgentID,strStatusCode,strExceptionCode);
			}
			catch(ApplicationException appException)
			{
				String strMsg = appException.Message;
				if(strMsg.IndexOf("FK") != -1)
				{
					lblEXMessage.Text = "Error Deleting Exception code. Exception code being used in some transaction(s)";
					
				}

				return;
			}

			if((int)ViewState["EXMode"] == (int)ScreenMode.Insert)
			{
				m_sdsExceptionCode.ds.Tables[0].Rows.RemoveAt(e.Item.ItemIndex);
				m_sdsExceptionCode.QueryResultMaxSize--;
				dgExceptionCodes.CurrentPageIndex = 0;

				if((int) ViewState["EXOperation"] == (int)Operation.Insert && dgExceptionCodes.EditItemIndex > 0)
				{
					m_sdsExceptionCode.ds.Tables[0].Rows.RemoveAt(dgExceptionCodes.EditItemIndex-1);
					m_sdsExceptionCode.QueryResultMaxSize--;
				}
				dgExceptionCodes.EditItemIndex = -1;
				BindExGrid();
			}
			else
			{
				ShowCurrentEXPage();
			}


			Logger.LogDebugInfo("StatusException","dgExceptionCodes_Delete","INF004","Deleted row in Exception_Code table..");

			ViewState["EXOperation"] = Operation.None;
			btnExInsert.Enabled = true;
			lblEXMessage.Text = "";
		}

		private void BindExGrid()
		{
			try
			{
				dgExceptionCodes.VirtualItemCount = System.Convert.ToInt32(m_sdsExceptionCode.QueryResultMaxSize);
				dgExceptionCodes.DataSource = m_sdsExceptionCode.ds;
				dgExceptionCodes.DataBind();
				Session["SESSION_DS4"] = m_sdsExceptionCode;
			}
			catch(ApplicationException appException)
			{
				String strMsg = appException.Message;
				strMsg = appException.InnerException.Message;
				strMsg = appException.InnerException.InnerException.Message;
				lblEXMessage.Text = strMsg;				
				return;						
			}
		}

		private void AddRowInExGrid()
		{
			AgentProfileMgrDAL.AddNewRowInCPExceptionCodeDS(m_sdsExceptionCode);
			Session["SESSION_DS4"] = m_sdsExceptionCode;
		}

		protected void dgExceptionCodes_Bound(object sender, DataGridItemEventArgs e)
		{
			if(e.Item.ItemIndex == -1)
			{
				return;
			}
			DataRow drSelected = m_sdsExceptionCode.ds.Tables[0].Rows[e.Item.ItemIndex];
			if(drSelected.RowState == DataRowState.Deleted)
			{
				return;
			}

			msTextBox txtExceptionCode = (msTextBox)e.Item.FindControl("txtExceptionCode");			
			int iOperation = (int) ViewState["EXOperation"];			
			if(txtExceptionCode != null && iOperation == (int)Operation.Update)
			{
				txtExceptionCode.Enabled = false;
			}

			if(e.Item.ItemType == ListItemType.EditItem && (int)ViewState["EXMode"] == (int)ScreenMode.Insert && (int)ViewState["EXOperation"] == (int)Operation.Insert)
			{
				e.Item.Cells[3].Enabled = true;
			}
			else
			{
				e.Item.Cells[3].Enabled = false;
			} 

		}

		protected void dgExceptionCodes_PageChange(object sender, DataGridPageChangedEventArgs e)
		{
			dgExceptionCodes.CurrentPageIndex = e.NewPageIndex;
			ShowCurrentEXPage();
			Logger.LogDebugInfo("StatusException","dgExceptionCodes_PageChange","INF009","On Page Change "+e.NewPageIndex);
		}

		private void ShowCurrentEXPage()
		{
			int iStartIndex = dgExceptionCodes.CurrentPageIndex * dgExceptionCodes.PageSize;
			String strStatusCode = (String)ViewState["CurrentSC"];
			String strAgentID = (String)ViewState["currentAgent"];

			m_sdsExceptionCode = AgentProfileMgrDAL.GetCPExceptionCodeDS(utility.GetAppID(),utility.GetEnterpriseID(),strAgentID,strStatusCode,iStartIndex,dgExceptionCodes.PageSize);

			decimal pgCnt = (m_sdsExceptionCode.QueryResultMaxSize - 1)/dgExceptionCodes.PageSize;
			if(pgCnt < dgExceptionCodes.CurrentPageIndex)
			{
				dgExceptionCodes.CurrentPageIndex = System.Convert.ToInt32(pgCnt);
			}
			dgExceptionCodes.EditItemIndex = -1;
			BindExGrid();
			//lblEXMessage.Text = "";
		}


		private void FillSCDataRow(DataGridItem item, int drIndex)
		{

			DataRow drCurrent = m_sdsStatusCode.ds.Tables[0].Rows[drIndex];
			msTextBox txtStatusCode = (msTextBox)item.FindControl("txtStatusCode");
			if(txtStatusCode != null)
			{
				//DataRow drCurrent = m_sdsStatusCode.ds.Tables[0].Rows[drIndex];
				drCurrent["status_code"] = txtStatusCode.Text;
			}

			TextBox txtStatusDescription = (TextBox)item.FindControl("txtStatusDescription");
			if(txtStatusDescription != null)
			{
				//DataRow drCurrent = m_sdsStatusCode.ds.Tables[0].Rows[drIndex];
				drCurrent["status_description"] = txtStatusDescription.Text;
			}
			
			msTextBox txtSCChargeAmount = (msTextBox)item.FindControl("txtSCChargeAmount");
			if(txtSCChargeAmount != null && txtSCChargeAmount.Text != "")
			{
				//DataRow drCurrent = m_sdsStatusCode.ds.Tables[0].Rows[drIndex];
				drCurrent["surcharge"] = txtSCChargeAmount.Text;
			}
	
		}

		private void FillEXDataRow(DataGridItem item, int drIndex)
		{

			DataRow drCurrent = m_sdsExceptionCode.ds.Tables[0].Rows[drIndex];
			msTextBox txtExceptionCode = (msTextBox)item.FindControl("txtExceptionCode");
			if(txtExceptionCode != null)
			{
				//DataRow drCurrent = m_sdsExceptionCode.ds.Tables[0].Rows[drIndex];
				drCurrent["exception_code"] = txtExceptionCode.Text;
			}
			msTextBox txtEXChargeAmount = (msTextBox)item.FindControl("txtEXChargeAmount");
			if(txtEXChargeAmount != null && txtEXChargeAmount.Text != "")
			{
				//DataRow drCurrent = m_sdsStatusCode.ds.Tables[0].Rows[drIndex];
				drCurrent["surcharge"] = txtEXChargeAmount.Text;
			}

		}

		private void ResetDetailsGrid()
		{
			dgExceptionCodes.CurrentPageIndex = 0;
			btnExInsert.Enabled = false;			
			m_sdsExceptionCode = AgentProfileMgrDAL.GetEmptyCPExceptionCodeDS(0);
			//-lblEXMessage.Text = "";
			BindExGrid();

		}
		
		public String ShowLocalizedLabel()
		{			
			return Utility.GetLanguageText(ResourceType.ModuleName,"Agent Profile","zh-CHS");
		}

		/* Quotation Procedures */

		protected void OnItemBound_Quotation(object sender, DataGridItemEventArgs e)
		{
			if(e.Item.ItemIndex == -1)
			{
				return;
			}
			DataRow drSelected = m_sdsQuotation.ds.Tables[0].Rows[e.Item.ItemIndex];			
			String strStatus=drSelected.ItemArray[3].ToString();
			//DataGrid dgQuotation=(DataGrid) agentProfileMultiPage.FindControl("dgQuotation");
			Label lblQuoteStatus =(Label) e.Item.FindControl("lblQuoteStatus");
			if (lblQuoteStatus != null)
			{
				if (strStatus=="D")
				{					
					lblQuoteStatus.Text="Draft";
				}
				else if (strStatus=="Q")
				{
					lblQuoteStatus.Text="Quoted";
				}
				else if	(strStatus=="C")
				{
					lblQuoteStatus.Text="Cancelled";
				}
				else
				{
					lblQuoteStatus.Text="";
				}
			}
			
		}

		protected void OnQuotation_PageChange(object sender, DataGridPageChangedEventArgs e)
		{
			dgQuotation.CurrentPageIndex = e.NewPageIndex;
			int iStartIndex = dgQuotation.CurrentPageIndex * dgQuotation.PageSize;
			String strAgentID = (String)ViewState["currentAgent"];
			
			m_sdsQuotation = AgentProfileMgrDAL.QueryQuotation(utility.GetAppID(),utility.GetEnterpriseID(), strAgentID, iStartIndex,dgQuotation.PageSize);
			Session["SESSION_DS5"] = m_sdsQuotation;
			dgQuotation.VirtualItemCount = System.Convert.ToInt32(m_sdsQuotation.QueryResultMaxSize);
			dgQuotation.CurrentPageIndex = e.NewPageIndex;
			BindQuotation();
		}

		private void BindQuotation()
		{
			dgQuotation.VirtualItemCount = System.Convert.ToInt32(m_sdsQuotation.QueryResultMaxSize);
			dgQuotation.DataSource = m_sdsQuotation.ds;
			dgQuotation.DataBind();
			Session["SESSION_DS5"] = m_sdsQuotation;
		}

		private void btnQuoteViewAll_Click(object sender, System.EventArgs e)
		{
			ShowQuotationsForCurrentAgent();
			BindQuotation();
		}

		private void ShowQuotationsForCurrentAgent()
		{
			ViewState["currentQuoteSet"] = 0;			
			GetQuoteRecSet();
			if(m_sdsQuotation.QueryResultMaxSize == 0)
			{
				if(!utility.GetUserCulture().ToUpper().Equals("EN-US"))
				{
					lblNumRefRec.Text = Utility.GetLanguageText(ResourceType.UserMessage, "QUOTN_NO_ADDED", utility.GetUserCulture());	
				} 
				else 
				{
					lblNumRefRec.Text = "No Quotations added.";
				}
				return;
			}

			lblCPErrorMessage.Text = "";			
			BindQuotation();

		}

		private void GetQuoteRecSet()
		{
			int iStartIndex = (int)ViewState["currentQuoteSet"] * m_iQuoteSize;			
			String strAgentID = (String)ViewState["currentAgent"];
			m_sdsQuotation = AgentProfileMgrDAL.QueryQuotation(utility.GetAppID(),utility.GetEnterpriseID(),strAgentID,iStartIndex,m_iQuoteSize);			
			Session["SESSION_DS5"] = m_sdsQuotation;
			decimal pgCnt = (m_sdsQuotation.QueryResultMaxSize - 1)/m_iQuoteSize;
			if(pgCnt < (int)ViewState["currentQuoteSet"])
			{
				ViewState["currentQuoteSet"] = System.Convert.ToInt32(pgCnt);
			}
		}

		#region for Zone and Zip
		/// Agent Zone & Zip methods
		
		protected void btnZoneViewAll_Click(object sender, System.EventArgs e)
		{
			ShowZoneForCurrentAgent();					
		}

		private void ShowZoneForCurrentAgent()
		{
			ViewState["ZoneMode"] = ScreenMode.ExecuteQuery;
			ViewState["ZipOperation"] = Operation.None;
			//ViewState["QUERY_DS"] = m_sdsStatusCode.ds;
			dgAgentZonecodes.CurrentPageIndex = 0;
			ShowCurrentZonePage();
			btnZipcodeInsert.Enabled = true;
			lblZoneMessage.Text = "";
			lblZoneMessage.Text = "";
		}

		protected void btnZoneInsert_Click(object sender, System.EventArgs e)
		{
			//if((int)ViewState["ZoneMode"] != (int)ScreenMode.Insert || m_sdsZoneCode.ds.Tables[0].Rows.Count >= dgAgentZonecodes.PageSize)
			if( m_sdsZoneCode.ds.Tables[0].Rows.Count >= dgAgentZonecodes.PageSize)
			{
				m_sdsZoneCode = AgentProfileMgrDAL.GetEmptyAgentZoneDS(0);
			}

			AddRowInZoneGrid();	
			dgAgentZonecodes.EditItemIndex = m_sdsZoneCode.ds.Tables[0].Rows.Count - 1;
			dgAgentZonecodes.CurrentPageIndex = 0;
			Logger.LogDebugInfo("Agent Profile","btnZipcodeInsert_Click","INF003","Data Grid Items count"+dgAgentZonecodes.Items.Count);
			
			ViewState["ZoneMode"] = ScreenMode.Insert;
			ViewState["ZipOperation"] = Operation.Insert;
			btnZipcodeInsert.Enabled = false;
			//btnExecQry.Enabled = false;
			lblZoneMessage.Text = "";
			lblAgentZoneZipMessage.Text = "";
			BindZoneGrid();
			ResetZoneZipGrid();
			lblAgentZoneZipMessage.Text = "";
			lblZoneMessage.Text			= "";
			getPageControls(Page);
		}
		private void AddRowInZoneZipGrid()
		{
			AgentProfileMgrDAL.AddNewRowInAgentZoneZipDS(m_sdsAgentZoneZip);
			Session["SESSION_DS7"] = m_sdsAgentZoneZip;
		}
		
		protected void btnAgentZoneZipInsert_Click(object sender, System.EventArgs e)
		{
			String strAgentID	= (String)ViewState["currentAgent"];
			String strZoneCode	= (String)ViewState["CurrentZone"];				
			String sUrl = "AgentZipCodePopup.aspx?FORMID=AgentProfile&ZONECODE="+strZoneCode+"&AGENTID="+strAgentID;
			ArrayList paramList = new ArrayList();
			paramList.Add(sUrl);
			String sScript = Utility.GetScript("openParentWindow.js",paramList);
			Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
			lblAgentZoneZipMessage.Text = "";
			lblZoneMessage.Text			= "";			
		}

		private void AddRowInZoneGrid()
		{
			AgentProfileMgrDAL.AddNewRowInAgentZoneDS(m_sdsZoneCode);
			Session["SESSION_DS6"] = m_sdsZoneCode;
		}

		private void BindZoneGrid()
		{
			dgAgentZonecodes.VirtualItemCount = System.Convert.ToInt32(m_sdsZoneCode.QueryResultMaxSize);
			dgAgentZonecodes.DataSource = m_sdsZoneCode.ds;
			dgAgentZonecodes.DataBind();
			Session["SESSION_DS6"] = m_sdsZoneCode;
		}

		private void ResetZoneZipGrid()
		{
			dgZoneZipcodIncluded.CurrentPageIndex = 0;
			btnAgentCostInsert.Enabled = false;			
			m_sdsAgentZoneZip = AgentProfileMgrDAL.GetEmptyAgentZoneZipDS(0);
			BindAgentZoneZipGrid();

		}

		private void BindAgentZoneZipGrid()
		{
			try
			{
				dgZoneZipcodIncluded.VirtualItemCount = System.Convert.ToInt32(m_sdsAgentZoneZip.QueryResultMaxSize);
				dgZoneZipcodIncluded.DataSource = m_sdsAgentZoneZip.ds;
				dgZoneZipcodIncluded.DataBind();
				Session["SESSION_DS7"] = m_sdsAgentZoneZip;
			}
			catch(ApplicationException appException)
			{
				String strMsg = appException.Message;
				strMsg = appException.InnerException.Message;
				strMsg = appException.InnerException.InnerException.Message;
				lblAgentZoneZipMessage.Text = strMsg;				
				return;
			}
		}

		private void ShowCurrentZoneZipPage()
		{
			int iStartIndex = dgZoneZipcodIncluded.CurrentPageIndex * dgZoneZipcodIncluded.PageSize;
			String strZoneCode	= (String)ViewState["CurrentZone"];
			String strAgentID = (String)ViewState["currentAgent"];

			m_sdsAgentZoneZip = AgentProfileMgrDAL.GetAgentZoneZipDS(utility.GetAppID(),utility.GetEnterpriseID(),strAgentID,strZoneCode,iStartIndex,dgZoneZipcodIncluded.PageSize);

			int pgCnt = (Convert.ToInt32(m_sdsAgentZoneZip.QueryResultMaxSize) - 1)/dgZoneZipcodIncluded.PageSize;
			if(pgCnt < dgZoneZipcodIncluded.CurrentPageIndex)
			{
				if(pgCnt < 1)
				{
					dgZoneZipcodIncluded.CurrentPageIndex=0;
				}
				else
				{
					dgZoneZipcodIncluded.CurrentPageIndex = System.Convert.ToInt32(pgCnt);
				}
			}
			dgZoneZipcodIncluded.EditItemIndex = -1;
			BindAgentZoneZipGrid();
		}
		protected void dgZoneZipcodIncluded_Delete(object sender, DataGridCommandEventArgs e)
		{
			//zipcode
			msTextBox txtZoneZipCode = (msTextBox)e.Item.FindControl("txtZoneZipCode");
			Label lblZoneZipCode = (Label)e.Item.FindControl("lblZoneZipCode");
			String strZipCode = null;
			if(txtZoneZipCode != null)
			{
				strZipCode = txtZoneZipCode.Text;
			}

			if(lblZoneZipCode != null)
			{
				strZipCode = lblZoneZipCode.Text;
			}
			//country
			msTextBox txtZoneZipCountry = (msTextBox)e.Item.FindControl("txtZoneZipCountry");
			Label lblZoneZipCountry = (Label)e.Item.FindControl("lblZoneZipCountry");
			String strCountry = null;
			if(txtZoneZipCode != null)
			{
				strCountry = txtZoneZipCountry.Text;
			}

			if(lblZoneZipCode != null)
			{
				strCountry = lblZoneZipCountry.Text;
			}

			String strZoneCode = (String)ViewState["CurrentZone"];
			String strAgentID = (String)ViewState["currentAgent"];

			try
			{
				int intRow=AgentProfileMgrDAL.DeleteAgentZoneZipDS(utility.GetAppID(),utility.GetEnterpriseID(),strAgentID,strZoneCode,strZipCode,strCountry);
				lblAgentZoneZipMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"DELETED_SUCCESSFULLY",utility.GetUserCulture()); 
			}
			catch(ApplicationException appException)
			{
				String strMsg = appException.Message;
				if(strMsg.IndexOf("FK") != -1)
				{
					lblAgentZoneZipMessage.Text = "Error Deleting Exception code. Exception code being used in some transaction(s)";					
				}

				return;
			}			
			ShowCurrentZoneZipPage();
			Logger.LogDebugInfo("ZoneZipcode","dgZoneZipcodIncluded_Delete","INF007","Deleted row in agent_zone_zipcode table..");

		}
		public void dgAgentZonecodes_Delete(object sender, DataGridCommandEventArgs e)
		{
			msTextBox txtZoneCode = (msTextBox)e.Item.FindControl("txtZoneCode");
			String strAgentID = (String)ViewState["currentAgent"];

			Label lblZoneCode = (Label)e.Item.FindControl("lblZoneCode");
			String strZonecode = null;
			if(txtZoneCode != null)
			{
				strZonecode = txtZoneCode.Text;
			}

			if(lblZoneCode != null)
			{
				strZonecode = lblZoneCode.Text;
			}

			try
			{
				AgentProfileMgrDAL.DeleteAgentZoneDS(utility.GetAppID(),utility.GetEnterpriseID(),strAgentID,strZonecode);
			}
			catch(ApplicationException appException)
			{
				String strMsg = appException.Message;
				if(strMsg.IndexOf("FK") != -1)
				{
					lblZoneMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"AGENT_ZONT_ERR_DEL",utility.GetUserCulture());
				}
				return;
			}

			if((int)ViewState["ZoneMode"] == (int)ScreenMode.Insert)
			{
				m_sdsZoneCode.ds.Tables[0].Rows.RemoveAt(e.Item.ItemIndex);
				m_sdsZoneCode.QueryResultMaxSize--;
				dgAgentZonecodes.CurrentPageIndex = 0;

				if((int) ViewState["ZipOperation"] == (int)Operation.Insert && dgAgentZonecodes.EditItemIndex > 0)
				{
					m_sdsZoneCode.ds.Tables[0].Rows.RemoveAt(dgAgentZonecodes.EditItemIndex-1);
					m_sdsZoneCode.QueryResultMaxSize--;
				}
				dgAgentZonecodes.EditItemIndex = -1;
				dgAgentZonecodes.SelectedIndex = -1;
				BindZoneGrid();
				ResetZoneZipGrid();
			}
			else
			{
				ShowCurrentZonePage();
			}

			Logger.LogDebugInfo("ZipCode","dgAgentZonecodes_Delete","INF004","Deleted row in Agent_Zipcode table..");
			ViewState["ZipOperation"] = Operation.None;
			btnZipcodeInsert.Enabled = true;
		}

		public void dgAgentZonecodes_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			Label lblZoneCode = (Label)dgAgentZonecodes.SelectedItem.FindControl("lblZoneCode");
			msTextBox txtZoneCode = (msTextBox)dgAgentZonecodes.SelectedItem.FindControl("txtZoneCode");
			String strZonecode = null;

			if(lblZoneCode != null)
			{
				strZonecode = lblZoneCode.Text;
			}

			if(txtZoneCode != null)
			{
				strZonecode = txtZoneCode.Text;
			}

			ViewState["CurrentZone"] = strZonecode;
			//dgZoneZipcodIncluded.CurrentPageIndex = 0;

			Logger.LogDebugInfo("AgentZipcode","dgAgentZonecodes_SelectedIndexChanged","INF004","updating data grid..."+dgAgentZonecodes.SelectedIndex+"  : "+strZonecode);
			ResetZoneZipGrid();
			ShowCurrentZoneZipPage();
			ViewState["ZoneZipMode"] = ScreenMode.ExecuteQuery;

			if((int)ViewState["ZoneMode"] != (int)ScreenMode.Insert && (int)ViewState["ZipOperation"] != (int)Operation.Insert)
			{
				btnSCInsert.Enabled = true;
			}

			if((int)ViewState["ZoneMode"] == (int)ScreenMode.Insert && dgAgentZonecodes.EditItemIndex == dgAgentZonecodes.SelectedIndex && (int)ViewState["ZipOperation"] == (int)Operation.Insert)
			{
				btnAgentCostInsert.Enabled = false;
			}
			else
			{
				btnAgentCostInsert.Enabled = true;
			}			
			lblZoneMessage.Text			= "";
			lblAgentZoneZipMessage.Text = "";
		}

		public void dgAgentZonecodes_Update(object sender, DataGridCommandEventArgs e)
		{

			FillZoneDataRow(e.Item,e.Item.ItemIndex);
			String strAgentID	= (String)ViewState["currentAgent"];
			int iOperation = (int)ViewState["ZipOperation"];
			switch(iOperation)
			{

				case (int)Operation.Update:

					DataSet dsToUpdate = m_sdsZoneCode.ds.GetChanges();
					AgentProfileMgrDAL.ModifyAgentZoneDS(utility.GetAppID(),utility.GetEnterpriseID(),strAgentID,dsToUpdate);
					lblZoneMessage.Text = "Agent Zonecode updated successfully";
					btnZipcodeInsert.Enabled = true;
					m_sdsZoneCode.ds.Tables[0].Rows[e.Item.ItemIndex].AcceptChanges();
					Logger.LogDebugInfo("AgentZipCode","dgAgentZonecodes_OnUpdate","INF004","update in modify mode..");

					break;

				case (int)Operation.Insert:

					DataSet dsToInsert = m_sdsZoneCode.ds.GetChanges();
					try
					{
						AgentProfileMgrDAL.AddAgentZoneDS(utility.GetAppID(),utility.GetEnterpriseID(),strAgentID,dsToInsert);
						lblZoneMessage.Text =Utility.GetLanguageText(ResourceType.UserMessage,"AGENT_ZONT_INS_SUCC",utility.GetUserCulture());
						btnZipcodeInsert.Enabled = true;
					}
					catch(ApplicationException appException)
					{
						String strMsg = appException.Message;
						strMsg = appException.InnerException.Message;
						strMsg = appException.InnerException.InnerException.Message;
						if(strMsg.IndexOf("duplicate key") != -1 )
						{
							lblZoneMessage.Text =Utility.GetLanguageText(ResourceType.UserMessage,"DUP_KEY_FOUND_ZONE",utility.GetUserCulture());
						}
						else if (strMsg.IndexOf("conflict") != -1 )
						{
							lblZoneMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"DUP_KEY_FOUND_ZONE",utility.GetUserCulture());							
						}
						else
						{
							lblZoneMessage.Text=strMsg;
						}
						return;
						
					}
					m_sdsZoneCode.ds.Tables[0].Rows[e.Item.ItemIndex].AcceptChanges();
					Logger.LogDebugInfo("AgentZipCode","dgAgentZonecodes_OnUpdate","INF004","update in Insert mode");
					break;

			}
			dgAgentZonecodes.EditItemIndex = -1;
			ViewState["ZipOperation"] = Operation.None;
			//lblZoneMessage.Text = "";
			lblAgentZoneZipMessage.Text = "";
			BindZoneGrid();
		}

		protected void dgAgentZonecodes_Cancel(object sender, DataGridCommandEventArgs e)
		{
			dgAgentZonecodes.EditItemIndex = -1;

			if((int)ViewState["ZoneMode"] == (int)ScreenMode.Insert && (int)ViewState["ZipOperation"] == (int)Operation.Insert)
			{
				m_sdsZoneCode.ds.Tables[0].Rows.RemoveAt(e.Item.ItemIndex);
				m_sdsZoneCode.QueryResultMaxSize--;
				dgAgentZonecodes.CurrentPageIndex = 0;
			}
			ViewState["zipOperation"] = Operation.None;
			BindZoneGrid();
			btnZipcodeInsert.Enabled = true;
			lblZoneMessage.Text = "";
			lblAgentZoneZipMessage.Text = "";
			Logger.LogDebugInfo("AgentZipCode","dgAgentZonecodes_Cancel","INF004","updating data grid...");			
		}

		protected void dgAgentZonecodes_Edit(object sender, DataGridCommandEventArgs e)
		{
			if((int)ViewState["ZoneMode"] == (int)ScreenMode.Insert && (int) ViewState["ZipOperation"] == (int)Operation.Insert && dgAgentZonecodes.EditItemIndex > 0)
			{
				m_sdsZoneCode.ds.Tables[0].Rows.RemoveAt(dgAgentZonecodes.EditItemIndex);
				m_sdsZoneCode.QueryResultMaxSize--;
				dgAgentZonecodes.CurrentPageIndex = 0;
			}

			dgAgentZonecodes.EditItemIndex = e.Item.ItemIndex;
			ViewState["ZipOperation"] = Operation.Update;
			BindZoneGrid();
			lblZoneMessage.Text = "";
			lblAgentZoneZipMessage.Text = "";
			Logger.LogDebugInfo("AgentZipCode","dgAgentZonecodes_Edit","INF004","updating data grid...");			
		}

		protected void dgAgentZonecodes_Bound(object sender, DataGridItemEventArgs e)
		{
			if(e.Item.ItemIndex == -1)
			{
				return;
			}

			DataRow drSelected = m_sdsZoneCode.ds.Tables[0].Rows[e.Item.ItemIndex];
			if(drSelected.RowState == DataRowState.Deleted)
			{
				return;
			}

			msTextBox txtZoneCode = (msTextBox)e.Item.FindControl("txtZoneCode");
			int iOperation = (int) ViewState["ZipOperation"];
			if(txtZoneCode != null && iOperation == (int)Operation.Update)
			{
				txtZoneCode.Enabled = false;
			}

			if(e.Item.ItemType == ListItemType.EditItem && (int)ViewState["ZoneMode"] == (int)ScreenMode.Insert && (int)ViewState["ZipOperation"] == (int)Operation.Insert)
			{
				e.Item.Cells[4].Enabled = true;
			}
			else
			{
				e.Item.Cells[4].Enabled = false;
			} 
		}

		protected void dgAgentZonecodes_PageChange(object sender, DataGridPageChangedEventArgs e)
		{
			dgAgentZonecodes.CurrentPageIndex = e.NewPageIndex;
			ShowCurrentZonePage();
			Logger.LogDebugInfo("AgentZipcode","dgAgentZonecodes_PageChange","INF009","On Page Change "+e.NewPageIndex);
		}
		protected void dgAgentZoneZip_PageChange(object sender, DataGridPageChangedEventArgs e)
		{
			dgZoneZipcodIncluded.CurrentPageIndex = e.NewPageIndex;
			ShowCurrentZoneZipPage();
			Logger.LogDebugInfo("AgentZoneZipcode","dgAgentZonecodes_PageChange","INF009","On Page Change "+e.NewPageIndex);
		}

		private void ShowCurrentZonePage()
		{
			DataRow drCurrent = m_sdsAgentProfile.ds.Tables[0].Rows[Convert.ToInt32(ViewState["currentPage"])];

			TextBox txtCustAccDispTab6 = (TextBox)agentProfileMultiPage.FindControl("txtCustAccDispTab6");
			if(txtCustAccDispTab6 != null)
			{
				if(drCurrent["agentid"]!= null && (!drCurrent["agentid"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					txtCustAccDispTab6.Text = drCurrent["agentid"].ToString();
				}
				else
				{
					txtCustAccDispTab6.Text = "";
				}
			}

			TextBox txtCustNameDispTab6 = (TextBox)agentProfileMultiPage.FindControl("txtCustNameDispTab6");
			if(txtCustNameDispTab6 != null)
			{
				if(drCurrent["agent_name"]!= null && (!drCurrent["agent_name"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					txtCustNameDispTab6.Text = drCurrent["agent_name"].ToString();
				}
				else
				{
					txtCustNameDispTab6.Text = "";
				}
			}

			int iStartIndex = dgAgentZonecodes.CurrentPageIndex * dgAgentZonecodes.PageSize;
			String strAgentID = (String)ViewState["currentAgent"];
			m_sdsZoneCode = AgentProfileMgrDAL.GetAgentZoneDS(utility.GetAppID(),utility.GetEnterpriseID(),strAgentID,iStartIndex,dgAgentZonecodes.PageSize);
			int pgCnt = (Convert.ToInt32(m_sdsZoneCode.QueryResultMaxSize) - 1)/dgAgentZonecodes.PageSize;
			if(pgCnt < dgAgentZonecodes.CurrentPageIndex)
			{
					dgAgentZonecodes.CurrentPageIndex = System.Convert.ToInt32(pgCnt);
			}
			dgAgentZonecodes.SelectedIndex = -1;
			dgAgentZonecodes.EditItemIndex = -1;
			lblZoneMessage.Text = "";
			BindZoneGrid();
			ResetZoneZipGrid();
		}

		public void dgAgentZonecodes_Button(object sender, DataGridCommandEventArgs e)
		{
			String strAgentID = (String)ViewState["currentAgent"];			
			String strCmdNm = e.CommandName;
			if(strCmdNm.Equals("search"))
			{
				if(dgAgentZonecodes.EditItemIndex != e.Item.ItemIndex)
				{
					return;
				}
				if(!e.Item.Cells[4].Enabled)
				{
					return;
				}
				msTextBox txtZoneCode = (msTextBox)e.Item.FindControl("txtZoneCode");
				TextBox txtZoneDescription = (TextBox)e.Item.FindControl("txtZoneDescription");
				//msTextBox txtZipCode = (msTextBox)dgAgentZonecodes.Items[e.Item.ItemIndex].FindControl("txtZipCode");
				
				String strZoneCode			= null;	
				String strZoneCodeClientID	= null;	
				String strZoneDesc			= null;	
				String strZoneDescClientID	= null;	
				if(txtZoneCode != null)
				{
					strZoneCode			= txtZoneCode.Text;
					strZoneCodeClientID = txtZoneCode.ClientID;
				}
				if(txtZoneDescription != null )
				{
					strZoneDesc			= txtZoneDescription.Text;	
					strZoneDescClientID	= txtZoneDescription.ClientID;	
				}
				
					String sUrl = "ZonePopup.aspx?FORMID=AgentProfile&ZONECODE="+strZoneCodeClientID+"&ZONEDESC="+strZoneDescClientID+"&ZONECODE_TEXT="+strZoneCode;
					ArrayList paramList = new ArrayList();
					paramList.Add(sUrl);
					String sScript = Utility.GetScript("openWindowParam.js",paramList);
					Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
			}			
		}

		private void FillZoneDataRow(DataGridItem item, int drIndex)
		{
			DataRow drCurrent = m_sdsZoneCode.ds.Tables[0].Rows[drIndex];
			String strAgentID = (String)ViewState["currentAgent"];
			//drCurrent["AgentID"]=strAgentID;
			msTextBox txtZoneCode = (msTextBox)item.FindControl("txtZoneCode");
			if(txtZoneCode != null)
			{				
				drCurrent["zone_code"] = txtZoneCode.Text;
			}
			TextBox txtZoneDescription = (TextBox)item.FindControl("txtZoneDescription");
			if(txtZoneDescription != null)
			{			
				drCurrent["zone_description"] = txtZoneDescription.Text;
			}		
			
		}

		protected void dgAgentZoneZip_Edit(object sender, DataGridCommandEventArgs e)
		{
			lblZoneMessage.Text			= "";
			lblAgentZoneZipMessage.Text = "";
			dgZoneZipcodIncluded.EditItemIndex = e.Item.ItemIndex;
			ViewState["ZoneZipOperation"] = Operation.Update;
			BindAgentZoneZipGrid();
			Logger.LogDebugInfo("dgAgentZoneZip_Edit","dgZoneZipcodIncluded_Edit","INF004","updating data grid...");			
		}
		private void FillZoneZipDataRow(DataGridItem item, int drIndex)
		{
			DataRow drCurrent = m_sdsAgentZoneZip.ds.Tables[0].Rows[drIndex];
			msTextBox txtZoneZipCode = (msTextBox)item.FindControl("txtZoneZipCode");
			if(txtZoneZipCode != null)
			{				
				drCurrent["zipcode"] = txtZoneZipCode.Text;
			}
			TextBox txtZoneZipState = (TextBox)item.FindControl("txtZoneZipState");
			if(txtZoneZipState != null)
			{			
				drCurrent["state_name"] = txtZoneZipState.Text;
			}
			TextBox txtZoneZipCountry = (TextBox)item.FindControl("txtZoneZipCountry");
			if(txtZoneZipCountry != null)
			{				
				drCurrent["country"] = txtZoneZipCountry.Text;
			}
			msTextBox txtESA = (msTextBox)item.FindControl("txtESA");
			decimal decESurcharge = 0;
			if ((txtESA.Text !=null) && (txtESA.Text != ""))
			{
				decESurcharge = Convert.ToDecimal(txtESA.Text.ToString());
			}
			drCurrent["esa_surcharge"] = decESurcharge;

			
		}
		public void dgAgentZoneZip_Update(object sender, DataGridCommandEventArgs e)
		{
			FillZoneZipDataRow(e.Item,e.Item.ItemIndex);
			int iOperation = (int)ViewState["ZoneZipOperation"];
			String strAgentID = (String)ViewState["currentAgent"];
			
			String strZoneCode	= (String)ViewState["CurrentZone"];
			switch(iOperation)
			{
				case (int)Operation.Update:
					try
					{
						DataSet dsToInsert = m_sdsAgentZoneZip.ds.GetChanges();
						AgentProfileMgrDAL.ModifyAgentZipDS(utility.GetAppID(),utility.GetEnterpriseID(),strAgentID,strZoneCode,dsToInsert);
						lblAgentZoneZipMessage.Text = "Agent Zipcode updated successfully";
						btnZipcodeInsert.Enabled = true;
						m_sdsAgentZoneZip.ds.Tables[0].Rows[e.Item.ItemIndex].AcceptChanges();
						Logger.LogDebugInfo("AgentZipCode","dgAgentZonecodes_OnUpdate","INF004","update in modify mode..");
						
					}
					catch(ApplicationException appException)
					{
						String strMsg = appException.Message;
						strMsg = appException.InnerException.Message;
						strMsg = appException.InnerException.InnerException.Message;
						if(strMsg.IndexOf("duplicate key") != -1 )
						{
							lblAgentZoneZipMessage.Text =Utility.GetLanguageText(ResourceType.UserMessage,"DUP_KEY_FOUND_ZONE",utility.GetUserCulture());
						}
						else if (strMsg.IndexOf("conflict") != -1 )
						{
							lblAgentZoneZipMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"DUP_KEY_FOUND_ZONE",utility.GetUserCulture());							
						}
						else
						{
							lblAgentZoneZipMessage.Text=strMsg;
						}
						return;
						
					}
					m_sdsAgentZoneZip.ds.Tables[0].Rows[e.Item.ItemIndex].AcceptChanges();
					Logger.LogDebugInfo("AgentZipCode","dgAgentZonecodes_OnUpdate","INF004","update in Insert mode");
					break;

			}
			dgAgentZonecodes.EditItemIndex = -1;
			ViewState["ZoneZipOperation"] = Operation.None;
			ShowCurrentZoneZipPage();
		}

		protected void dgAgentZoneZip_Cancel(object sender, DataGridCommandEventArgs e)
		{
			dgZoneZipcodIncluded.EditItemIndex = -1;
			BindAgentZoneZipGrid();
			Logger.LogDebugInfo("BindAgentZoneZipGrid","dgZoneZipcodIncluded_Cancel","INF004","updating data grid...");			
		}
	
		#endregion

	}
}
