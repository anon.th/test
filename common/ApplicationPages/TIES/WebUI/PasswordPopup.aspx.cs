using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.common.applicationpages;
using com.common.util;

namespace TIES.WebUI
{
	/// <summary>
	/// Summary description for PasswordPopup.
	/// </summary>
	public class PasswordPopup : BasePopupPage
	{
		protected System.Web.UI.WebControls.Button btnOk;
		protected System.Web.UI.WebControls.Button btnCancel;
		protected System.Web.UI.WebControls.Label lblPassword;
		protected System.Web.UI.WebControls.TextBox txtPassword;
		protected System.Web.UI.WebControls.Label lblRetypePswd;
		protected System.Web.UI.WebControls.TextBox txtRetypePswd;
		protected System.Web.UI.WebControls.Label lblMainHeader;
		protected System.Web.UI.WebControls.Label lblErrorMsg;
		//String strVisibliliy = null;

		private void Page_Load(object sender, System.EventArgs e)
		{
//			strVisibliliy = Request.Params["HIDE"];
//			if(strVisibliliy.Equals("YES"))
//			{
//				lblPassword.Text = "New Password";
//				lblRetypePswd.Visible = true;
//				txtRetypePswd.Visible = true;
//			}
//			else if(strVisibliliy.Equals("NO"))
//			{
//				lblPassword.Text = "Password";
//				lblRetypePswd.Visible = false;
//				txtRetypePswd.Visible = false;
//			}
			// Put user code to initialize the page here
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			base.OnInit(e);
			InitializeComponent();
			
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
			this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
			this.txtPassword.TextChanged += new System.EventHandler(this.txtPassword_TextChanged);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private void txtPassword_TextChanged(object sender, System.EventArgs e)
		{
		
		}

		private void btnCancel_Click(object sender, System.EventArgs e)
		{
			String sScript = "";
			sScript += "<script language=javascript>";
			sScript += "  window.close();";
			sScript += "</script>";
			Response.Write(sScript);
		}

		private void btnOk_Click(object sender, System.EventArgs e)
		{
			lblErrorMsg.Text = "";

			if(txtPassword.Text.Length == 0)
			{
				//lblErrorMsg.Text = "Please enter the password";
				lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage, "PLS_ENTER_PWD", utility.GetUserCulture());
				return;
			}
			else if(txtRetypePswd.Text.Length == 0)
			{
				//lblErrorMsg.Text = "Please enter the password again for confirmation";
				lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage, "PLS_ENTER_PWD_AGAIN", utility.GetUserCulture());
				return;
			}
			else if(txtPassword.Text != txtRetypePswd.Text)
			{
				//lblErrorMsg.Text = "Both password's doesn't match";
				lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage, "PWD_NO_MATCH", utility.GetUserCulture());
				return;
			}
			Session["Password"] = txtPassword.Text.Trim();
			String sScript = "";
			sScript += "<script language=javascript>";
			sScript += "  window.close();";
			sScript += "</script>";
			Response.Write(sScript);
		}
	}
}
