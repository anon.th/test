using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.common.util;
using com.ties;
using com.ties.classes;
using com.ties.DAL;
using com.ties.BAL;
using com.common.classes;
using System.Configuration;
using com.common.applicationpages;
using com.common.DAL;
namespace TIES.WebUI
{
	/// <summary>
	/// Summary description for BillPlacementRulesPopUp.
	/// </summary>
	public class BillPlacementRulesPopUp :  BasePopupPage
	{
		protected System.Web.UI.WebControls.Label Label52;
		protected System.Web.UI.HtmlControls.HtmlTable Table10;
		protected System.Web.UI.HtmlControls.HtmlTable Table1;
		protected System.Web.UI.HtmlControls.HtmlTable Table2;
		protected System.Web.UI.WebControls.Label Label1;
		protected System.Web.UI.WebControls.Label Label2;
		protected System.Web.UI.HtmlControls.HtmlTable Table3;
		protected System.Web.UI.WebControls.RadioButton r_Monthly;
		protected System.Web.UI.WebControls.RadioButton r_Monthly_AllowPlacement;
		protected System.Web.UI.WebControls.RadioButton r_Monthly_AdjustToNext;
		protected System.Web.UI.WebControls.RadioButton r_Monthly_AdjustToPrevious;
		protected System.Web.UI.WebControls.Button btnCancel;
		protected System.Web.UI.WebControls.Button btnQK;
		protected System.Web.UI.WebControls.CheckBox cb_Weekly_Monday;
		protected System.Web.UI.WebControls.CheckBox cb_Weekly_TuesDay;
		protected System.Web.UI.WebControls.CheckBox cb_Weekly_Wednesday;
		protected System.Web.UI.WebControls.CheckBox cb_Weekly_Thursday;
		protected System.Web.UI.WebControls.CheckBox cb_Weekly_Friday;
		protected System.Web.UI.WebControls.RadioButton r_Weekly_AllowPlacement;
		protected System.Web.UI.WebControls.CheckBox cb_Weekly_Saturday;
		protected System.Web.UI.WebControls.CheckBox cb_Weekly_Sunday;
		protected System.Web.UI.WebControls.RadioButton r_Weekly_AdjustToNext;
		protected System.Web.UI.WebControls.RadioButton r_Weekly_AdjustToPrevious;
		protected System.Web.UI.WebControls.RadioButton r_Weekly_CancelPlacement;
		protected System.Web.UI.WebControls.RadioButton r_Semi_Monthly_AllowPlacement;
		protected System.Web.UI.WebControls.RadioButton r_Semi_Monthly_AdjustToNext;
		protected System.Web.UI.WebControls.RadioButton r_Semi_Monthly_AdjustToPrevious;
		protected System.Web.UI.WebControls.RadioButton r_Monthly_OnceAMonth;
		protected System.Web.UI.WebControls.DropDownList ddl_Monthly_OnceAMonth;
		protected System.Web.UI.WebControls.DropDownList ddl_Monthly_OnceAMonthOf;
		protected System.Web.UI.WebControls.RadioButton r_Monthly_DayRange;
		protected com.common.util.msTextBox txt_Monthly_DayStart;
		protected com.common.util.msTextBox txt_Monthly_DayEnd;
		protected System.Web.UI.WebControls.RadioButton r_Semi_Monthly_FirstPlacement;
		protected System.Web.UI.WebControls.DropDownList ddl_Semi_Monthly_OnceAMonth;
		protected System.Web.UI.WebControls.DropDownList ddl_Semi_Monthly_OnceAMonthOf;
		protected System.Web.UI.WebControls.RadioButton r_Semi_Monthly_DayRange;
		protected com.common.util.msTextBox txt_Semi_Monthly_DayStart;
		protected com.common.util.msTextBox txt_Semi_Monthly_DayEnd;
		protected System.Web.UI.WebControls.RadioButton r_Semi_Monthly_CancelPlacement;
		protected System.Web.UI.WebControls.RadioButton r_Weekly;
		protected System.Web.UI.WebControls.RadioButton r_Semi_Monthly;
		protected System.Web.UI.WebControls.Label Label4;
		protected System.Web.UI.WebControls.TextBox TextBox1;
		protected System.Web.UI.WebControls.Label Label5;
		protected System.Web.UI.WebControls.Label Label6;
		protected System.Web.UI.WebControls.Label lblUpdatedBy;
		protected System.Web.UI.WebControls.Label Label3;
		protected System.Web.UI.WebControls.Label lblErrorMsg;
		DataTable dtBillPlacementRules;
		String appID=null;
		String enterpriseID=null;
		protected com.common.util.msTextBox txtNETCODAmountCollected_Check;
		protected System.Web.UI.HtmlControls.HtmlGenericControl Weekly;
		protected System.Web.UI.WebControls.Label Label7;
		protected System.Web.UI.WebControls.Label Label8;
		protected System.Web.UI.WebControls.Label Label9;
		protected System.Web.UI.WebControls.TextBox txtRemark;
		String userID=null;
		string sessonName=null;
		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			sessonName="dtBillPlacementRules_";
			if(Request.QueryString["AccNo"] != null)
			{
				sessonName=sessonName+Request.QueryString["AccNo"].ToString().Trim();
			}
			
			utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings,Page.Session);	

			appID = utility.GetAppID();
			enterpriseID = utility.GetEnterpriseID();
			userID = utility.GetUserID();


			if (!Page.IsPostBack)
			{
				if (checkRole())
					btnQK.Visible=true;
				else
					btnQK.Visible=false;
					
				lblErrorMsg.Text = "";
				BindOnceAMonth();
				BindOnceAMonthOf();
				lblUpdatedBy.Text ="";
				
				if(Session[this.sessonName]!= null )
				{
					dtBillPlacementRules = (DataTable) Session[this.sessonName];
					DisplayRules(dtBillPlacementRules);
				}
				else
				{
					dtBillPlacementRules = SysDataMgrDAL.GetCustomerProfile_BillPlacementRules(appID,enterpriseID,"");
					Session[this.sessonName]=dtBillPlacementRules;
					DisplayRules(dtBillPlacementRules);
				}
			}
			else
			{
				if(Session[this.sessonName]!= null )
				{
					dtBillPlacementRules = (DataTable) Session[this.sessonName];
				}
			}


		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnQK.Click += new System.EventHandler(this.btnQK_Click);
			this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
			this.r_Weekly.CheckedChanged += new System.EventHandler(this.r_Weekly_CheckedChanged);
			this.r_Semi_Monthly.CheckedChanged += new System.EventHandler(this.r_Semi_Monthly_CheckedChanged);
			this.r_Monthly.CheckedChanged += new System.EventHandler(this.r_Monthly_CheckedChanged);
			this.r_Semi_Monthly_FirstPlacement.CheckedChanged += new System.EventHandler(this.r_Semi_Monthly_FirstPlacement_CheckedChanged);
			this.r_Semi_Monthly_DayRange.CheckedChanged += new System.EventHandler(this.r_Semi_Monthly_DayRange_CheckedChanged);
			this.r_Monthly_OnceAMonth.CheckedChanged += new System.EventHandler(this.r_Monthly_OnceAMonth_CheckedChanged);
			this.r_Monthly_DayRange.CheckedChanged += new System.EventHandler(this.r_Monthly_DayRange_CheckedChanged);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private void r_Weekly_CheckedChanged(object sender, System.EventArgs e)
		{
			EnableWeekly(true);
			EnableSemiMonth(false);
			EnableMonth(false);
			r_Weekly_AdjustToPrevious.Checked=true;
					
		}

		private void r_Semi_Monthly_CheckedChanged(object sender, System.EventArgs e)
		{
			EnableWeekly(false);
			EnableSemiMonth(true);
			EnableMonth(true);
			r_Semi_Monthly_AdjustToPrevious.Checked=true;
			r_Monthly_AdjustToPrevious.Checked=true;
		}

		private void r_Monthly_CheckedChanged(object sender, System.EventArgs e)
		{
			EnableWeekly(false);
			EnableSemiMonth(false);
			EnableMonth(true);
			r_Monthly_AdjustToPrevious.Checked=true;			
		}

		private void r_Semi_Monthly_FirstPlacement_CheckedChanged(object sender, System.EventArgs e)
		{
			if(r_Semi_Monthly_FirstPlacement.Checked)
			{
				txt_Semi_Monthly_DayStart.Enabled=false;
				txt_Semi_Monthly_DayEnd.Enabled=false;
				ddl_Semi_Monthly_OnceAMonth.Enabled=true;
				ddl_Semi_Monthly_OnceAMonthOf.Enabled=true;
			}
		}

		private void r_Semi_Monthly_DayRange_CheckedChanged(object sender, System.EventArgs e)
		{
			if(r_Semi_Monthly_DayRange.Checked)
			{
				txt_Semi_Monthly_DayStart.Enabled=true;
				txt_Semi_Monthly_DayEnd.Enabled=true;
				ddl_Semi_Monthly_OnceAMonth.Enabled=false;
				ddl_Semi_Monthly_OnceAMonthOf.Enabled=false;
			}
		}

		private void r_Monthly_OnceAMonth_CheckedChanged(object sender, System.EventArgs e) 
		{
			if(r_Monthly_OnceAMonth.Checked )
			{
				txt_Monthly_DayStart.Enabled=false;
				txt_Monthly_DayEnd.Enabled=false;
				ddl_Monthly_OnceAMonth.Enabled=true;
				ddl_Monthly_OnceAMonthOf.Enabled=true;
			}
		}

		private void r_Monthly_DayRange_CheckedChanged(object sender, System.EventArgs e)
		{
			if (r_Monthly_DayRange.Checked)
			{
				txt_Monthly_DayStart.Enabled=true;
				txt_Monthly_DayEnd.Enabled=true;
				ddl_Monthly_OnceAMonth.Enabled=false;
				ddl_Monthly_OnceAMonthOf.Enabled=false;
			}
		}

		private void btnQK_Click(object sender, System.EventArgs e)
		{
			if (this.r_Weekly.Checked==true)
			{
			clearSemiMonthArea();
				clearMonthArea();
			}
			else if(this.r_Semi_Monthly.Checked==true)
			{
				clearWeeklyArea();
			}
			else if(this.r_Monthly.Checked==true)
			{
				clearWeeklyArea();
				clearSemiMonthArea();
			}
			if (VerifyRules())
			{	
				Session[this.sessonName]= GetInputRules();
			}
			else
				return;
			String sScript = "";
			sScript += "<script language=javascript>";
			sScript += "    window.opener.CustomerProfile.txtRemarkBp.value = \"" + this.txtRemark.Text.ToString() +"\";";
			sScript += "  window.close();";
			sScript += "</script>";
			Response.Write(sScript);
		}

		private void btnCancel_Click(object sender, System.EventArgs e)
		{
			String sScript = "";
			sScript += "<script language=javascript>";
			sScript += "  window.close();";
			sScript += "</script>";
			Response.Write(sScript);
		}
		private bool VerifyRules()
		{
			if ((!r_Weekly.Checked)&&(!r_Semi_Monthly.Checked)&&(!r_Monthly.Checked))
			{
				lblErrorMsg.Text = "Please select accept placement frequency";
				return false;
			}
			if (r_Weekly.Checked)
			{
				if((!cb_Weekly_Monday.Checked)&&(!cb_Weekly_TuesDay.Checked )&&(!cb_Weekly_Wednesday.Checked )&&(!cb_Weekly_Thursday.Checked )&&(!cb_Weekly_Friday.Checked)&&(!cb_Weekly_Saturday.Checked)&&(!cb_Weekly_Sunday.Checked))
				{
					lblErrorMsg.Text = "Please select at least one day";
					return false;
				}
			}
			if (r_Monthly.Checked)
			{
				if((!r_Monthly_OnceAMonth.Checked )&&(!r_Monthly_DayRange.Checked ))
				{
					lblErrorMsg.Text="Please select monthly conditions";
					return false;
				}
				else
				{
					if ((r_Monthly_DayRange.Checked) && (txt_Monthly_DayStart.Text ==""))
					{
					lblErrorMsg.Text="Start day of weekly date range is missing";
					return false;
					}
					if ((r_Monthly_DayRange.Checked)&&(txt_Monthly_DayStart.Text!="")&&(txt_Monthly_DayEnd.Text!=""))
					{
						int iDayStart = int.Parse(txt_Monthly_DayStart.Text);
						int iDayEnd =int.Parse(txt_Monthly_DayEnd.Text);
						if (iDayStart > iDayEnd)
						{
							lblErrorMsg.Text="Start day of weekly date range must be less than ending day in the range";
							return false;
						}
					}
				}
			}
			
			if (r_Semi_Monthly.Checked)
			{
				if((!r_Semi_Monthly_FirstPlacement.Checked )&&(!r_Semi_Monthly_DayRange.Checked ))
				{
					lblErrorMsg.Text="Please select monthly conditions";
					return false;
				}
				else
				{
					if ((r_Semi_Monthly_DayRange.Checked) && (txt_Semi_Monthly_DayStart.Text ==""))
					{
						lblErrorMsg.Text="Start day of weekly date range is missing";
						return false;
					}
					if ((r_Semi_Monthly_DayRange.Checked)&&(txt_Semi_Monthly_DayStart.Text!="")&&(txt_Semi_Monthly_DayEnd.Text!=""))
					{
						int iDayStart =int.Parse(txt_Semi_Monthly_DayStart.Text);
						int iDayEnd =int.Parse(txt_Semi_Monthly_DayEnd.Text);
						if (iDayStart > iDayEnd)
						{
							lblErrorMsg.Text="Start day of weekly date range must be less than ending day in the range";
							return false;
						}
					}

					if ((r_Monthly_DayRange.Checked) && (txt_Monthly_DayStart.Text ==""))
					{
						lblErrorMsg.Text="Start day of weekly date range is missing";
						return false;
					}
					if ((r_Monthly_DayRange.Checked)&&(txt_Monthly_DayStart.Text!="")&&(txt_Monthly_DayEnd.Text!=""))
					{
						int iDayStart = int.Parse(txt_Monthly_DayStart.Text);
						int iDayEnd =int.Parse(txt_Monthly_DayEnd.Text);
						if (iDayStart > iDayEnd)
						{
							lblErrorMsg.Text="Start day of weekly date range must be less than ending day in the range";
							return false;
						}
					}
				}
			}

			if ((r_Semi_Monthly_FirstPlacement.Checked)&&(r_Monthly_OnceAMonth.Checked))
			{	
				int iMonth_OnceAMonth = int.Parse(ddl_Monthly_OnceAMonth.SelectedValue);
				int iSemi_Month_OnceAMonth =int.Parse(ddl_Semi_Monthly_OnceAMonth.SelectedValue);
				int iMonth_OnceAMonthOf = int.Parse(ddl_Monthly_OnceAMonthOf.SelectedValue);
				int iSemi_Month_OnceAMonthOf =int.Parse(ddl_Semi_Monthly_OnceAMonthOf.SelectedValue);
				

				if ((iMonth_OnceAMonth<iSemi_Month_OnceAMonth) || ((iMonth_OnceAMonth==iSemi_Month_OnceAMonth)&&(iMonth_OnceAMonthOf<=iSemi_Month_OnceAMonthOf)))
				{

					lblErrorMsg.Text="Monthly and Semi-monthly placement dates may not overlap";
					return false;
				}

			}

			if ((r_Semi_Monthly_DayRange.Checked) && ( r_Monthly_DayRange.Checked))
			{
				int iMonth_DayStart =0;
				int iSemi_Month_DayStart=0;
				int iMonth_DayEnd=0;
				int iSemi_Month_DayEnd=0;
				if (txt_Monthly_DayStart.Text != "")
				{
					iMonth_DayStart=int.Parse(txt_Monthly_DayStart.Text); 
				}
				if(txt_Semi_Monthly_DayStart.Text!="")
				{
					iSemi_Month_DayStart= int.Parse(txt_Semi_Monthly_DayStart.Text);
				}
				if(txt_Monthly_DayEnd.Text!="")
				{
					iMonth_DayEnd = int.Parse(txt_Monthly_DayEnd.Text);
				}
				if(txt_Semi_Monthly_DayEnd.Text!="")
				{
					iSemi_Month_DayEnd = int.Parse(txt_Semi_Monthly_DayEnd.Text);
				}


				if ((iSemi_Month_DayEnd>iMonth_DayStart)||(iMonth_DayEnd<iMonth_DayStart))
				{
					lblErrorMsg.Text="Monthly and Semi-monthly placement dates may not overlap";
					return false;
				}
			}
			lblErrorMsg.Text="";
			return true;																		
		}
		private DataTable GetInputRules()
		{
			DataTable dtRules;
			DataRow	drRules;
			dtRules=dtBillPlacementRules.Clone();
			dtRules.Columns["semi_monthly_daystart"].AllowDBNull =true;		
			dtRules.Columns["semi_monthly_dayend"].AllowDBNull =true;		
			dtRules.Columns["monthly_daystart"].AllowDBNull =true;		
			dtRules.Columns["monthly_dayend"].AllowDBNull =true;
			dtRules.Columns["semi_monthly_condition"].AllowDBNull=true;
			dtRules.Columns["monthly_condition"].AllowDBNull=true;

			drRules=dtRules.NewRow();
			drRules["applicationid"]=appID.ToString();
			drRules["enterpriseid"]=enterpriseID.ToString();
			drRules["last_updated_by"] = userID.ToUpper();
			drRules["last_updated"] = DateTime.Now.ToString();
			drRules["custid"]=(string)Session["CustID"];
			drRules["remark"]=txtRemark.Text.ToString();
			// Weekly
			if (r_Weekly.Checked)
			{
				drRules["placement_frequency"]="1";
			}
			else if (r_Semi_Monthly.Checked)
			{
				drRules["placement_frequency"]="2";
			}
			else if (r_Monthly.Checked)
			{
				drRules["placement_frequency"]="3";
			}
			string sDay="";
			if(cb_Weekly_Sunday.Checked)
			{
				sDay+="1,";
			}
			if(cb_Weekly_Monday.Checked)
			{
				sDay+="2,";
			}
			if(cb_Weekly_TuesDay.Checked)
			{
				sDay+="3,";
			}
			if(cb_Weekly_Wednesday.Checked)
			{
				sDay+="4,";
			}
			if(cb_Weekly_Thursday.Checked)
			{
				sDay+="5,";
			}
			if(cb_Weekly_Friday.Checked)
			{
				sDay+="6,";
			}
			if(cb_Weekly_Saturday.Checked)
			{
				sDay+="7,";
			}
			if (sDay!="")
			{
				sDay =sDay.Substring(0,sDay.LastIndexOf(","));
			}
			drRules["weekly_days"] = sDay.ToString().Trim();

			if (r_Weekly_AllowPlacement .Checked)
			{
				drRules["weekly_holiday"]="1";
			}
			else if (r_Weekly_AdjustToNext.Checked)
			{
				drRules["weekly_holiday"]="2";
			}
			else if (r_Weekly_AdjustToPrevious.Checked)
			{
				drRules["weekly_holiday"]="3";
			}
			else if (r_Weekly_CancelPlacement.Checked)
			{
				drRules["weekly_holiday"]="4";
			}
			// Semi Monthly
			if (r_Semi_Monthly_FirstPlacement.Checked)
			{
				drRules["semi_monthly_condition"]=true.ToString();
				drRules["semi_monthly_condition_every"]=ddl_Semi_Monthly_OnceAMonth.SelectedValue.ToString();
				drRules["semi_monthly_condition_day"]=ddl_Semi_Monthly_OnceAMonthOf.SelectedValue.ToString();
			}
			else if (r_Semi_Monthly_DayRange.Checked)
			{
				drRules["semi_monthly_condition"]=false.ToString();
				if (txt_Semi_Monthly_DayStart.Text.ToString()!="")
				{
					drRules["semi_monthly_daystart"]=txt_Semi_Monthly_DayStart.Text.ToString();
				}
				else
				{
					drRules["semi_monthly_daystart"]=System.DBNull.Value;
				}
				if (txt_Semi_Monthly_DayEnd.Text.ToString()!="")
				{
					drRules["semi_monthly_dayend"]=txt_Semi_Monthly_DayEnd.Text.ToString();
				}
				else
				{
					drRules["semi_monthly_dayend"]=System.DBNull.Value;
				}
	
			}
			else
			{
				
				drRules["semi_monthly_condition"]=System.DBNull.Value;
				drRules["semi_monthly_condition_every"]=System.DBNull.Value;
				drRules["semi_monthly_condition_day"]=System.DBNull.Value;
				drRules["semi_monthly_daystart"]=System.DBNull.Value;
				drRules["semi_monthly_dayend"]=System.DBNull.Value;
			}
			
			if (r_Semi_Monthly_AllowPlacement.Checked)
			{
				drRules["semi_monthly_holiday"]="1";
			}
			else if (r_Semi_Monthly_AdjustToNext.Checked)
			{
				drRules["semi_monthly_holiday"]="2";
			}
			else if (r_Semi_Monthly_AdjustToPrevious.Checked)
			{
				drRules["semi_monthly_holiday"]="3";
			}
			else if (r_Semi_Monthly_CancelPlacement.Checked)
			{
				drRules["semi_monthly_holiday"]="4";
			}
			// Monthly
			if (r_Monthly_OnceAMonth.Checked)
			{
				drRules["monthly_condition"]=true.ToString();
				drRules["monthly_condition_every"]=ddl_Monthly_OnceAMonth.SelectedValue.ToString();
				drRules["monthly_condition_day"]=ddl_Monthly_OnceAMonthOf.SelectedValue.ToString();
			}
			else if (r_Monthly_DayRange.Checked)
			{
				drRules["monthly_condition"]=false.ToString();
				if (txt_Monthly_DayStart.Text.ToString()!="") 
				{
					drRules["monthly_daystart"]=txt_Monthly_DayStart.Text.ToString();
				}
				else
				{
					drRules["monthly_daystart"]=System.DBNull.Value;
				}

				if (txt_Monthly_DayEnd.Text.ToString()!="")
				{
					drRules["monthly_dayend"]=txt_Monthly_DayEnd.Text.ToString();
				}
				else
				{
					drRules["monthly_dayend"]=System.DBNull.Value;
				}
			}
			else
			{
				drRules["monthly_condition"]=System.DBNull.Value;
				drRules["monthly_condition_every"]=System.DBNull.Value;
				drRules["monthly_condition_day"]=System.DBNull.Value;
				drRules["monthly_daystart"]=System.DBNull.Value;
				drRules["monthly_dayend"]=System.DBNull.Value;
			}

			if (r_Monthly_AllowPlacement.Checked)
			{
				drRules["monthly_holiday"]="1";
			}
			else if (r_Monthly_AdjustToNext.Checked)
			{
				drRules["monthly_holiday"]="2";
			}
			else if (r_Monthly_AdjustToPrevious.Checked)
			{
				drRules["monthly_holiday"]="3";
			}

			dtRules.Rows.Add(drRules);
			return dtRules;
		}
		private void DisplayRules(DataTable dtBillPlacmentRules)
		{
			//weekly
				EnableWeekly(false);
			//semi monthly
				EnableSemiMonth(false);
			//monthly
				EnableMonth(false);

			if(dtBillPlacementRules==null || dtBillPlacementRules .Rows.Count<=0)
			{
				this.r_Weekly.Enabled=true;
				this.r_Semi_Monthly.Enabled=true;
				this.r_Monthly.Enabled=true;
				this.lblUpdatedBy.Text  = "";
			}
			else
			{
				this.txtRemark.Text= dtBillPlacementRules.Rows[0]["remark"].ToString() ;
				this.lblUpdatedBy.Text  = dtBillPlacementRules.Rows[0]["last_updated_by"].ToString() +" " +dtBillPlacementRules.Rows[0]["last_updated"].ToString() ;
				switch(dtBillPlacementRules.Rows[0]["Placement_frequency"].ToString())
				{
					case "1":
						r_Weekly.Checked=true;  
						break;
					case "2":
						r_Semi_Monthly.Checked=true; 
						break;
					case "3":
						r_Monthly.Checked=true;
						break;
				}
				
				if (r_Weekly.Checked)
				{
					EnableWeekly(true);
					String strDay = dtBillPlacementRules.Rows[0]["weekly_days"].ToString();
					String[] arrDay = strDay.Split(',');
					foreach (string sDay in arrDay)
					{
						switch (sDay.ToString().Trim())
						{
							case"1":
								cb_Weekly_Sunday.Checked=true;
								break;
							case"2":
								cb_Weekly_Monday.Checked=true;
								break;
							case"3":
								cb_Weekly_TuesDay.Checked=true;
								break;
							case"4":
								cb_Weekly_Wednesday.Checked=true;
								break;
							case"5":
								cb_Weekly_Thursday.Checked=true;
								break;
							case"6":
								cb_Weekly_Friday.Checked=true;
								break;
							case"7":
								cb_Weekly_Saturday.Checked=true;
								break;
						}
					}
					switch (dtBillPlacementRules.Rows[0]["weekly_holiday"].ToString().Trim())
					{
						case"1":
							r_Weekly_AllowPlacement.Checked=true;
							break;
						case"2":
							r_Weekly_AdjustToNext.Checked=true;
							break;
						case"3":
							r_Weekly_AdjustToPrevious.Checked=true;
							break;
						case"4":
							r_Weekly_CancelPlacement.Checked=true;
							break;
					}
				}
				if (r_Semi_Monthly.Checked)
				{
					EnableSemiMonth(true);
					EnableMonth(true);
					switch (dtBillPlacementRules.Rows[0]["semi_monthly_condition"].ToString())
					{
						case"True":
							r_Semi_Monthly_FirstPlacement.Checked=true;
							ddl_Semi_Monthly_OnceAMonth.SelectedValue=dtBillPlacementRules.Rows[0]["semi_monthly_condition_every"].ToString();
							ddl_Semi_Monthly_OnceAMonthOf.SelectedValue=dtBillPlacementRules.Rows[0]["semi_monthly_condition_day"].ToString();
							break;
						case"False":
							r_Semi_Monthly_DayRange.Checked=true;
							txt_Semi_Monthly_DayStart.Text = dtBillPlacementRules.Rows[0]["semi_monthly_daystart"].ToString();
							txt_Semi_Monthly_DayEnd.Text = dtBillPlacementRules.Rows[0]["semi_monthly_dayend"].ToString();
							break;
					}
					switch (dtBillPlacementRules.Rows[0]["semi_monthly_holiday"].ToString().Trim())
					{
						case"1":
							r_Semi_Monthly_AllowPlacement.Checked=true;
							break;
 						case"2":
							r_Semi_Monthly_AdjustToNext.Checked=true;
							break;
 						case"3":
							r_Semi_Monthly_AdjustToPrevious.Checked=true;
							break;
						case"4":
							r_Semi_Monthly_CancelPlacement.Checked=true;
							break;
					}
					setMonthControl(dtBillPlacementRules);
				}
				if (r_Monthly.Checked)
				{
					EnableMonth(true);
					setMonthControl(dtBillPlacementRules);
 				}
			}
		}
		private void BindOnceAMonth()
		{
			DataTable dtOnceAMonth =  new DataTable();
			dtOnceAMonth.Columns.Add(new DataColumn("Text", typeof(string)));
			dtOnceAMonth.Columns.Add(new DataColumn("StringValue", typeof(string)));

			ArrayList alOnceAmonth = Utility.GetCodeValues(appID,utility.GetUserCulture(),"BP_OnceAMonth",CodeValueType.StringValue);
			
			foreach(SystemCode typeSysCode in alOnceAmonth)
			{
				DataRow drEach = dtOnceAMonth.NewRow();
				drEach[0] = typeSysCode.Text;
				drEach[1] = typeSysCode.StringValue;
				dtOnceAMonth.Rows.Add(drEach);
			}

			ddl_Monthly_OnceAMonth.DataSource =dtOnceAMonth;
			ddl_Monthly_OnceAMonth.DataTextField="Text" ;
			ddl_Monthly_OnceAMonth.DataValueField="StringValue";
			ddl_Monthly_OnceAMonth.DataBind();

			ddl_Semi_Monthly_OnceAMonth.DataSource =dtOnceAMonth;
			ddl_Semi_Monthly_OnceAMonth.DataTextField="Text" ;
			ddl_Semi_Monthly_OnceAMonth.DataValueField="StringValue";
			ddl_Semi_Monthly_OnceAMonth.DataBind();
		}

		private void BindOnceAMonthOf()
		{
			DataTable dtOnceAMonthOf =  new DataTable();
			dtOnceAMonthOf.Columns.Add(new DataColumn("Text", typeof(string)));
			dtOnceAMonthOf.Columns.Add(new DataColumn("StringValue", typeof(string)));

			ArrayList alOnceAmonthOf = Utility.GetCodeValues(appID,utility.GetUserCulture(),"BP_OnceAMonthOf",CodeValueType.StringValue);
			
			foreach(SystemCode typeSysCode in alOnceAmonthOf)
			{
				DataRow drEach = dtOnceAMonthOf.NewRow();
				drEach[0] = typeSysCode.Text;
				drEach[1] = typeSysCode.StringValue;
				dtOnceAMonthOf.Rows.Add(drEach);
			}

			ddl_Monthly_OnceAMonthOf.DataSource =dtOnceAMonthOf;
			ddl_Monthly_OnceAMonthOf.DataTextField="Text" ;
			ddl_Monthly_OnceAMonthOf.DataValueField="StringValue";
			ddl_Monthly_OnceAMonthOf.DataBind();

			ddl_Semi_Monthly_OnceAMonthOf.DataSource =dtOnceAMonthOf;
			ddl_Semi_Monthly_OnceAMonthOf.DataTextField="Text" ;
			ddl_Semi_Monthly_OnceAMonthOf.DataValueField="StringValue";
			ddl_Semi_Monthly_OnceAMonthOf.DataBind();
		}
		private void EnableWeekly(bool isEnabled)
		{
			this.cb_Weekly_Monday.Enabled=isEnabled;
			this.cb_Weekly_TuesDay.Enabled=isEnabled;
			this.cb_Weekly_Wednesday.Enabled=isEnabled;
			this.cb_Weekly_Thursday.Enabled=isEnabled;
			this.cb_Weekly_Friday.Enabled=isEnabled;
			this.cb_Weekly_Saturday.Enabled=isEnabled;
			this.cb_Weekly_Sunday.Enabled=isEnabled;
			this.r_Weekly_AllowPlacement.Enabled=isEnabled;
			this.r_Weekly_AdjustToNext.Enabled=isEnabled;
			this.r_Weekly_AdjustToPrevious.Enabled=isEnabled;
			this.r_Weekly_CancelPlacement.Enabled=isEnabled;
			this.Table1.Disabled=!isEnabled;
		}
		private void EnableSemiMonth(bool isEnabled)
		{
			this.r_Semi_Monthly_FirstPlacement.Enabled=isEnabled;
			this.ddl_Semi_Monthly_OnceAMonth.Enabled=isEnabled;
			this.ddl_Semi_Monthly_OnceAMonthOf.Enabled=isEnabled;
			this.r_Semi_Monthly_DayRange.Enabled=isEnabled;
			this.txt_Semi_Monthly_DayStart.Enabled=isEnabled;
			this.txt_Semi_Monthly_DayEnd.Enabled=isEnabled;
			this.r_Semi_Monthly_AllowPlacement.Enabled=isEnabled;
			this.r_Semi_Monthly_AdjustToNext.Enabled=isEnabled;
			this.r_Semi_Monthly_AdjustToPrevious.Enabled=isEnabled;
			this.r_Semi_Monthly_CancelPlacement.Enabled=isEnabled;
			this.Table10.Disabled=!isEnabled;

 
		}
		private void EnableMonth(bool isEnabled)
		{
			this.r_Monthly_OnceAMonth .Enabled=isEnabled;
			this.ddl_Monthly_OnceAMonth.Enabled=isEnabled;
			this.ddl_Monthly_OnceAMonthOf .Enabled=isEnabled;
			this.r_Monthly_DayRange.Enabled=isEnabled;
			this.txt_Monthly_DayStart.Enabled=isEnabled;
			this.txt_Monthly_DayEnd.Enabled=isEnabled;
			this.r_Monthly_AllowPlacement.Enabled=isEnabled;
			this.r_Monthly_AdjustToNext.Enabled=isEnabled;
			this.r_Monthly_AdjustToPrevious.Enabled=isEnabled;
			this.Table3.Disabled=!isEnabled;
 
		}
		private void setMonthControl(DataTable dtMonthRules)
		{
			switch (dtMonthRules.Rows[0]["monthly_condition"].ToString())
			{
				case"True":
					r_Monthly_OnceAMonth.Checked=true;
					ddl_Monthly_OnceAMonth.SelectedValue=dtMonthRules.Rows[0]["monthly_condition_every"].ToString();
					ddl_Monthly_OnceAMonthOf.SelectedValue=dtMonthRules.Rows[0]["monthly_condition_day"].ToString();
					break;
				case"False":
					r_Monthly_DayRange.Checked=true;
					txt_Monthly_DayStart.Text = dtMonthRules.Rows[0]["monthly_daystart"].ToString();
					txt_Monthly_DayEnd.Text = dtMonthRules.Rows[0]["monthly_dayend"].ToString();
					break;
			}
			switch (dtMonthRules.Rows[0]["monthly_holiday"].ToString())
			{
				case"1":
					r_Monthly_AllowPlacement.Checked=true;
					break;
				case"2":
					r_Monthly_AdjustToNext.Checked=true;
					break;
				case"3":
					r_Monthly_AdjustToPrevious.Checked=true;
					break;
			}
		}
		private void clearWeeklyArea()
		{
			this.cb_Weekly_Sunday.Checked=false;
			this.cb_Weekly_Monday.Checked=false;
			this.cb_Weekly_TuesDay.Checked=false;
			this.cb_Weekly_Wednesday.Checked=false;
			this.cb_Weekly_Thursday.Checked=false;
			this.cb_Weekly_Friday.Checked=false;
			this.cb_Weekly_Saturday.Checked=false;
			this.r_Weekly_AdjustToNext.Checked=false;
			this.r_Weekly_AdjustToPrevious.Checked=false;
			this.r_Weekly_AllowPlacement.Checked =false;
			this.r_Weekly_CancelPlacement.Checked=false;
		}

		private void clearSemiMonthArea()
		{
			this.ddl_Semi_Monthly_OnceAMonth.SelectedIndex=0 ;
			this.ddl_Semi_Monthly_OnceAMonthOf.SelectedIndex=0;
			this.txt_Semi_Monthly_DayStart.Text="";
			this.txt_Semi_Monthly_DayEnd.Text ="";
			this.r_Semi_Monthly_AdjustToNext.Checked=false;
			this.r_Semi_Monthly_AdjustToPrevious.Checked=false;
			this.r_Semi_Monthly_AllowPlacement.Checked=false;
			this.r_Semi_Monthly_CancelPlacement.Checked=false;
			this.r_Semi_Monthly_FirstPlacement.Checked=false;
			this.r_Semi_Monthly_DayRange.Checked=false;
		}
		private void clearMonthArea()
		{
			this.ddl_Monthly_OnceAMonth.SelectedIndex=0;
            this.ddl_Monthly_OnceAMonthOf.SelectedIndex=0;
			this.txt_Monthly_DayStart.Text="";
			this.txt_Monthly_DayEnd.Text="";
			this.r_Monthly_AdjustToNext.Checked=false;
			this.r_Monthly_AdjustToPrevious.Checked=false;
			this.r_Monthly_AllowPlacement.Checked=false;
			this.r_Monthly_OnceAMonth.Checked=false;
			this.r_Monthly_DayRange.Checked=false;
		}
		private bool checkRole()
		{
			try
			{
				User user = new User();
				user.UserID = userID;
				ArrayList userRoleArray = com.common.RBAC.RBACManager.GetAllRoles(appID,enterpriseID,user);
				Role role;
				bool isBPRRole = false;
				for(int i=0;i<userRoleArray.Count;i++)
				{
					role = (Role)userRoleArray[i];
					if(role.RoleName.ToUpper().Trim()=="ACCTBPR")				
					{
						isBPRRole = true;
						return true;
					}
					else
					{
						isBPRRole = false;
					}
				}
				return isBPRRole;
			}
			catch
			{
				return false;
			}
		}
	}
}
