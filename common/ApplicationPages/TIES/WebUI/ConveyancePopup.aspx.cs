using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.common.util;
using com.common.classes;
using com.common.DAL;
using System.Text;
using com.common.applicationpages;

namespace com.ties
{
	/// <summary>
	/// Summary description for ConveyancePopup.
	/// </summary>
	public class ConveyancePopup : BasePopupPage
	{
		protected System.Web.UI.WebControls.DataGrid dgConveyanceMaster;
		protected System.Web.UI.WebControls.TextBox txtConveyanceCode;
		protected System.Web.UI.WebControls.Label lblConveyanceCode;
		protected System.Web.UI.WebControls.Button btnSearch;
		protected System.Web.UI.WebControls.Button btnOk;
		
		String strAppID = "";
		String strEnterpriseID = "";
		String strConveyanceID="";String strCCDescpID="";
		String strLengthID="";String strBreadthID="";
		String strHeightID="";String strWeightID="";

		String strFormID="";
		SessionDS m_sdsConveyanceCode=null;
	
		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			//	utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings,Page.Session);
			strAppID = utility.GetAppID();
			strEnterpriseID = utility.GetEnterpriseID();

			strConveyanceID = Request.Params["CONVEYANCECODE_TEXT"];
			strCCDescpID = Request.Params["CCDESCP_TEXT"];
			strLengthID = Request.Params["LENGTH_TEXT"];
			strBreadthID = Request.Params["BREADTH_TEXT"];
			strHeightID = Request.Params["HEIGHT_TEXT"];
			strWeightID = Request.Params["WEIGHT_TEXT"];

			strFormID = Request.Params["FORMID"];

			if(!Page.IsPostBack)
			{
				txtConveyanceCode.Text = Request.Params["CONVEYANCECODE"];
				m_sdsConveyanceCode = GetEmptyConveyanceCodeDS(0);
				BindGrid();
			}
			else
			{
				m_sdsConveyanceCode = (SessionDS)ViewState["CONVEYANCECODE_DS"];
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			base.OnInit(e);
			InitializeComponent();
			
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
			this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

	

		public void Paging(Object sender, DataGridPageChangedEventArgs e)
		{
			//runs when one of the page control is clicked
			//update the current page from the parameter values
			//dgConveyanceMaster.CurrentPageIndex = e.NewPageIndex;
			//bind data to the grid
			//RefreshData();
			dgConveyanceMaster.CurrentPageIndex = e.NewPageIndex;
			ShowCurrentPage();
		}

		private void btnSearch_Click(object sender, System.EventArgs e)
		{
			StringBuilder strQry = new StringBuilder();
			strQry.Append("select Conveyance_code, Conveyance_Description,Length, Breadth, Height, Max_wt from Conveyance where applicationid='");
			strQry.Append(strAppID +"' and enterpriseid = '"+strEnterpriseID+"'");
			if (txtConveyanceCode.Text.ToString() != null && txtConveyanceCode.Text.ToString() != "")
			{
				strQry.Append(" and Conveyance_code like '%");
				strQry.Append(Utility.ReplaceSingleQuote(txtConveyanceCode.Text.ToString()));
				strQry.Append("%'");
			}

			String strSQLQuery = strQry.ToString();
			//RefreshData();
			ViewState["SQL_QUERY"] = strSQLQuery;
			dgConveyanceMaster.CurrentPageIndex = 0;

			ShowCurrentPage();
			getPageControls(Page);
		}

		private void btnOk_Click(object sender, System.EventArgs e)
		{
			// Get the value and return it to the parent window
			String sScript = "";
			sScript += "<script language=javascript>";
			sScript += "  window.close();";
			sScript += "</script>";
			Response.Write(sScript);
		}

		public void dgConveyanceMaster_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			// Get the value and return it to the parent window
			int iSelIndex = dgConveyanceMaster.SelectedIndex;
			DataGridItem dgRow = dgConveyanceMaster.Items[iSelIndex];
			String strConveyanceCode = dgRow.Cells[0].Text;
			String strCCDescp = dgRow.Cells[1].Text;
			String strLength = dgRow.Cells[2].Text;
			String strBreadth = dgRow.Cells[3].Text;
			String strHeight = dgRow.Cells[4].Text;
			String strWeight = dgRow.Cells[5].Text;

			
			String sScript = "";
			sScript += "<script language=javascript>";	
			sScript += "  window.opener."+strFormID+"."+strConveyanceID+".value = '"+strConveyanceCode+"';" ;
			if(strFormID.Equals("AgentConveyance"))
			{				
				sScript += "  window.opener."+strFormID+"."+strCCDescpID+".value = '"+strCCDescp+"';" ;
				sScript += "  window.opener."+strFormID+"."+strLengthID+".value = '"+strLength+"';" ;
				sScript += "  window.opener."+strFormID+"."+strBreadthID+".value = '"+strBreadth+"';" ;
				sScript += "  window.opener."+strFormID+"."+strHeightID+".value = '"+strHeight+"';" ;
				sScript += "  window.opener."+strFormID+"."+strWeightID+".value = '"+strWeight+"';" ;
			}
			sScript += "  window.close();";
			sScript += "</script>";
			Response.Write(sScript);
		}

		public static SessionDS GetEmptyConveyanceCodeDS(int intRows)
		{
			DataTable dtConveyance = new DataTable();
			dtConveyance.Columns.Add(new DataColumn("Conveyance_code", typeof(string)));
			dtConveyance.Columns.Add(new DataColumn("Conveyance_Description", typeof(string)));
			dtConveyance.Columns.Add(new DataColumn("Length", typeof(decimal)));
			dtConveyance.Columns.Add(new DataColumn("Breadth", typeof(decimal)));
			dtConveyance.Columns.Add(new DataColumn("Height", typeof(decimal)));
			dtConveyance.Columns.Add(new DataColumn("Max_Wt", typeof(decimal)));

			for(int i=0; i < intRows; i++)
			{
				DataRow drEach = dtConveyance.NewRow();
				drEach[0] = "";
				drEach[1] = "";
				drEach[2] = 0;
				drEach[3] = 0;
				drEach[4] = 0;
				
				dtConveyance.Rows.Add(drEach);
			}

			DataSet dsConveyanceFields = new DataSet();
			dsConveyanceFields.Tables.Add(dtConveyance);
			SessionDS sessionDS = new SessionDS();
			sessionDS.ds = dsConveyanceFields;
			sessionDS.DataSetRecSize = dsConveyanceFields.Tables[0].Rows.Count;
			sessionDS.QueryResultMaxSize = sessionDS.DataSetRecSize;
			return sessionDS;
		}

		private void BindGrid()
		{
			dgConveyanceMaster.VirtualItemCount = System.Convert.ToInt32(m_sdsConveyanceCode.QueryResultMaxSize);
			dgConveyanceMaster.DataSource = m_sdsConveyanceCode.ds;
			dgConveyanceMaster.DataBind();
			ViewState["CONVEYANCECODE_DS"] = m_sdsConveyanceCode;
		}

		private void ShowCurrentPage()
		{
			int iStartIndex = dgConveyanceMaster.CurrentPageIndex * dgConveyanceMaster.PageSize;
			String sqlQuery = (String)ViewState["SQL_QUERY"];
			m_sdsConveyanceCode = GetConveyanceCodeDS(iStartIndex,dgConveyanceMaster.PageSize,sqlQuery);
			decimal pgCnt = (m_sdsConveyanceCode.QueryResultMaxSize - 1)/dgConveyanceMaster.PageSize;
			if(pgCnt < dgConveyanceMaster.CurrentPageIndex)
			{
				dgConveyanceMaster.CurrentPageIndex = System.Convert.ToInt32(pgCnt);
			}
			dgConveyanceMaster.SelectedIndex = -1;
			dgConveyanceMaster.EditItemIndex = -1;

			BindGrid();
		}

		private SessionDS GetConveyanceCodeDS(int iCurrent, int iDSRecSize, String strQuery)
		{
			SessionDS sessionDS = null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("ConveyanceCodePopup.aspx.cs","GetConveyanceCodeDS","EDB101","DbConnection object is null!!");
				return sessionDS;
			}
			sessionDS = dbCon.ExecuteQuery(strQuery,iCurrent,iDSRecSize,"ConveyanceCode");
			return  sessionDS;
		}

	}
}
