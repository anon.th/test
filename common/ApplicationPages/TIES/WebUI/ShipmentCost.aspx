<%@ Page language="c#" Codebehind="ShipmentCost.aspx.cs" AutoEventWireup="false" Inherits="com.ties.ShipmentCost" %>
<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<%@ Import Namespace="com.common.util" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>ShipmentCost</title>
		<LINK href="css/Styles.css" type="text/css" rel="stylesheet" name="stylesheet">
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<!--#INCLUDE FILE="msFormValidations.inc"-->
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="ShipmentCost" method="post" runat="server">
			<asp:button id="btnExecQry" style="Z-INDEX: 107; LEFT: 100px; POSITION: absolute; TOP: 44px" runat="server" Width="120px" CssClass="queryButton" Text="Execute Query" CausesValidation="False"></asp:button>
			<asp:Label id="lblPanelMessage" style="Z-INDEX: 111; LEFT: 33px; POSITION: absolute; TOP: 49px" runat="server" CssClass="errorMsgColor" Width="508px"></asp:Label><asp:label id="lblMainTitle" style="Z-INDEX: 106; LEFT: 36px; POSITION: absolute; TOP: 8px" runat="server" Width="477px" CssClass="mainTitleSize" Height="26px"> Shipment Cost</asp:label><asp:button id="btnAllocateMonthlyCost" style="Z-INDEX: 101; LEFT: 225px; POSITION: absolute; TOP: 44px" runat="server" Width="150px" CssClass="queryButton" Text="Allocate Monthly Cost" CausesValidation="False"></asp:button><asp:button id="btnReAllocateMonthlyCost" style="Z-INDEX: 102; LEFT: 380px; POSITION: absolute; TOP: 45px" runat="server" Width="150px" CssClass="queryButton" Text="Re-allocate Monthly Cost" CausesValidation="True" Visible="False"></asp:button><asp:button id="btnQry" style="Z-INDEX: 103; LEFT: 36px; POSITION: absolute; TOP: 44px" runat="server" Width="62px" CssClass="queryButton" Text="Query" CausesValidation="False"></asp:button><asp:label id="lblMessage" style="Z-INDEX: 105; LEFT: 42px; POSITION: absolute; TOP: 75px" runat="server" Width="650px" CssClass="errorMsgColor" Height="19px">asdfas</asp:label>
			<TABLE id="tblShipmentCost" style="Z-INDEX: 104; LEFT: 26px; WIDTH: 730px; POSITION: absolute; TOP: 100px" width="730" border="0" runat="server">
				<TR height="25">
					<TD colSpan="1">&nbsp;</TD>
					<TD colSpan="19"><asp:label id="lblMCAJob" runat="server" Width="100%" CssClass="tableLabel" Height="22px">Monthly Cost Allocation Job</asp:label></TD>
				</TR>
				<TR>
					<TD colSpan="1">&nbsp;</TD>
					<TD colSpan="19"><asp:datagrid id="dgMCAJob" style="Z-INDEX: 100" runat="server" Width="100%" PageSize="5" AllowPaging="True" AllowCustomPaging="True" SelectedItemStyle-CssClass="gridFieldSelected" OnPageIndexChanged="dgMCAJob_PageChange" OnSelectedIndexChanged="dgMCAJob_SelectedIndexChanged" OnDeleteCommand="dgMCAJob_Delete" OnItemDataBound="dgMCAJob_Bound" AutoGenerateColumns="False" ItemStyle-Height="20">
							<SelectedItemStyle CssClass="gridFieldSelected"></SelectedItemStyle>
							<ItemStyle Height="20px" CssClass="gridField"></ItemStyle>
							<Columns>
								<asp:ButtonColumn Text="&lt;IMG SRC='images/butt-select.gif' border=0 title='Select' &gt;" ButtonType="LinkButton" HeaderText="" CommandName="Select">
									<HeaderStyle HorizontalAlign="Center" Width="3%" CssClass="gridHeading"></HeaderStyle>
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
								</asp:ButtonColumn>
								<asp:ButtonColumn Text="&lt;IMG SRC='images/butt-delete.gif' border=0 title='Delete' &gt;" ButtonType="LinkButton" Visible="True" HeaderText="" CommandName="Delete">
									<HeaderStyle HorizontalAlign="Center" Width="3%" CssClass="gridHeading"></HeaderStyle>
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
								</asp:ButtonColumn>
								<asp:TemplateColumn HeaderText="Job ID">
									<HeaderStyle Font-Bold="True" Width="8%" CssClass="gridHeading"></HeaderStyle>
									<ItemStyle Wrap="False"></ItemStyle>
									<ItemTemplate>
										<asp:Label CssClass="gridLabel" ID="lblJobID" Text='<%#DataBinder.Eval(Container.DataItem,"jobid")%>' Runat="server">
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<cc1:msTextBox CssClass="gridTextBoxNumber" ID="txtJobID" Text='<%#DataBinder.Eval(Container.DataItem,"jobid", "{0:n}")%>' Runat="server" Enabled="True" MaxLength="8" TextMaskType="msNumeric" NumberMaxValue="99999999" NumberMinValue="0" NumberPrecision="8" NumberScale="0">
										</cc1:msTextBox>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Year">
									<HeaderStyle Width="6%" CssClass="gridHeading"></HeaderStyle>
									<ItemTemplate>
										<asp:Label CssClass="gridLabel" ID="lblYear" Text='<%#DataBinder.Eval(Container.DataItem,"account_year")%>' Runat="server" Enabled="True">
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<cc1:msTextBox CssClass="gridTextBoxNumber" ID="txtYear" Text='<%#DataBinder.Eval(Container.DataItem,"account_year", "{0:n}")%>' Runat="server" Enabled="True" MaxLength="4" TextMaskType="msNumeric" NumberMaxValue="2100" NumberMinValue="0" NumberPrecision="4" NumberScale="0">
										</cc1:msTextBox>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Month">
									<HeaderStyle Width="14%" CssClass="gridHeading"></HeaderStyle>
									<ItemTemplate>
										<asp:DropDownList CssClass="gridDropDown" ID="ddlMonth" Runat="server"></asp:DropDownList>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Status">
									<HeaderStyle Width="15%" CssClass="gridHeading"></HeaderStyle>
									<ItemTemplate>
										<asp:DropDownList CssClass="gridDropDown" ID="ddlStatus" Runat="server"></asp:DropDownList>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Start Date Time">
									<HeaderStyle Font-Bold="True" Width="15%" CssClass="gridHeading"></HeaderStyle>
									<ItemStyle Wrap="False"></ItemStyle>
									<ItemTemplate>
										<asp:Label CssClass="gridLabel" ID="lblStartDateTime" Text='<%#DataBinder.Eval(Container.DataItem,"start_datetime","{0:dd/MM/yyyy HH:mm}")%>' Runat="server">
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<cc1:mstextbox CssClass="gridTextBoxNumber" ID="txtStartDateTime" Text='<%#DataBinder.Eval(Container.DataItem,"start_datetime","{0:dd/MM/yyyy HH:mm}")%>' Runat="server" Enabled="True" TextMaskType="msDateTime" TextMaskString="99/99/9999 99:99">
										</cc1:mstextbox>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="End Date Time">
									<HeaderStyle Font-Bold="True" Width="15%" CssClass="gridHeading"></HeaderStyle>
									<ItemStyle Wrap="False"></ItemStyle>
									<ItemTemplate>
										<asp:Label CssClass="gridLabel" ID="lblEndDateTime" Text='<%#DataBinder.Eval(Container.DataItem,"end_datetime","{0:dd/MM/yyyy HH:mm}")%>' Runat="server">
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<cc1:mstextbox CssClass="gridTextBoxNumber" ID="txtEndDateTime" Text='<%#DataBinder.Eval(Container.DataItem,"end_datetime","{0:dd/MM/yyyy HH:mm}")%>' Runat="server" Enabled="True" TextMaskType="msDateTime" TextMaskString="99/99/9999 99:99">
										</cc1:mstextbox>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Remarks">
									<HeaderStyle Font-Bold="True" Width="24%" CssClass="gridHeading"></HeaderStyle>
									<ItemStyle Wrap="True"></ItemStyle>
									<ItemTemplate>
										<asp:Label CssClass="gridLabel" ID="lblRemarks" Text='<%#DataBinder.Eval(Container.DataItem,"remark")%>' Runat="server">
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:TextBox CssClass="gridTextBox" ID="txtRemarks" Text='<%#DataBinder.Eval(Container.DataItem,"remark")%>' Runat="server">
										</asp:TextBox>
									</EditItemTemplate>
								</asp:TemplateColumn>
							</Columns>
							<PagerStyle HorizontalAlign="Right"></PagerStyle>
						</asp:datagrid></TD>
				</TR>
				<TR height="2">
					<TD width="2%"></TD>
					<TD width="5%"></TD>
					<TD width="5%"></TD>
					<TD width="5%"></TD>
					<TD width="5%"></TD>
					<TD width="5%"></TD>
					<TD width="5%"></TD>
					<TD width="5%"></TD>
					<TD width="5%"></TD>
					<TD width="5%"></TD>
					<TD width="5%"></TD>
					<TD width="5%"></TD>
					<TD width="5%"></TD>
					<TD width="5%"></TD>
					<TD width="5%"></TD>
					<TD width="5%"></TD>
					<TD width="5%"></TD>
					<TD width="5%"></TD>
					<TD width="5%"></TD>
					<TD width="8%"></TD>
				</TR>
				<TR>
					<TD colSpan="1">&nbsp;</TD>
					<TD colSpan="19"><asp:datagrid id="dgCostCodes" style="Z-INDEX: 100" runat="server" Width="100%" SelectedItemStyle-CssClass="gridFieldSelected" AutoGenerateColumns="False" ItemStyle-Height="20">
							<SelectedItemStyle CssClass="gridFieldSelected"></SelectedItemStyle>
							<ItemStyle Height="20px" CssClass="gridField"></ItemStyle>
							<Columns>
								<asp:TemplateColumn HeaderText="Cost Code">
									<HeaderStyle Font-Bold="True" Width="15%" CssClass="gridHeading"></HeaderStyle>
									<ItemStyle Wrap="False"></ItemStyle>
									<ItemTemplate>
										<asp:Label CssClass="gridLabel" ID="lblCostCode" Text='<%#DataBinder.Eval(Container.DataItem,"cost_code")%>' Runat="server">
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Cost Description">
									<HeaderStyle Width="60%" CssClass="gridHeading"></HeaderStyle>
									<ItemTemplate>
										<asp:Label CssClass="gridLabel" ID="lblCostDescription" Text='<%#DataBinder.Eval(Container.DataItem,"cost_code_description")%>' Runat="server" Enabled="True">
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Amount">
									<HeaderStyle Width="15%" CssClass="gridHeading"></HeaderStyle>
									<ItemTemplate>
										<asp:Label CssClass="gridLabelNumber" ID="lblAmount" Text='<%#DataBinder.Eval(Container.DataItem,"cost_amt","{0:n}")%>' Runat="server" Enabled="True" >
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Allocated">
									<HeaderStyle Width="10%" CssClass="gridHeading"></HeaderStyle>
									<ItemTemplate>
										<asp:Label CssClass="gridLabel" ID="lblIsAllocated" Text='<%#DataBinder.Eval(Container.DataItem,"allocated")%>' Runat="server" Enabled="True">
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
							</Columns>
							<PagerStyle HorizontalAlign="Right"></PagerStyle>
						</asp:datagrid></TD>
				</TR>
				<TR height="10">
					<TD colSpan="20"></TD>
				</TR>
			</TABLE>
			<asp:panel id="YearMonthInputPanel" style="Z-INDEX: 108; LEFT: 25px; POSITION: absolute; TOP: 41px" runat="server" Width="731px" Height="464px" Visible="False" ForeColor="Transparent" BackColor="#E9E9CB">
				<P>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				</P>
				<P>&nbsp;</P>
				<P>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<asp:RequiredFieldValidator id="validateYear" EnableClientScript="False" Runat="server" ControlToValidate="txtAccYear" Display="None" ErrorMessage="Account Year is required field.">*</asp:RequiredFieldValidator>
					<asp:Label id="lblAstrik" runat="server" CssClass="errorMsgColor"> * </asp:Label>
					<asp:Label id="lblInputYear" runat="server">Account Year</asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<cc1:mstextbox id="txtAccYear" runat="server" CssClass="textField" Width="41px" TextMaskType="msNumeric" MaxLength="4" NumberMaxValue="2100" NumberMinValue="0" NumberPrecision="4" NumberScale="0" AutoPostBack="True"></cc1:mstextbox></P>
				<P>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<asp:Label id="lblSelectMonth" runat="server">Account Month</asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;
					<asp:dropdownlist id="ddlAccMonth" runat="server" CssClass="textField" Width="92px"></asp:dropdownlist></P>
				<P>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				</P>
				<P>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<asp:Button id="btnOk" runat="server" CausesValidation="True" Text=" Ok " CssClass="queryButton"></asp:Button>
					<asp:Button id="btnYes" runat="server" CausesValidation="True" Text=" Yes " CssClass="queryButton" Visible="False"></asp:Button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<asp:Button id="btnCancel" runat="server" CausesValidation="False" Text="Cancel" CssClass="queryButton"></asp:Button>&nbsp;
					<asp:Button id="btnNo" runat="server" CausesValidation="False" Text=" No " CssClass="queryButton" Visible="False"></asp:Button></P>
			</asp:panel><asp:validationsummary id="shipmentCostValidation" style="Z-INDEX: 109; LEFT: 43px; POSITION: absolute; TOP: 75px" runat="server" ShowMessageBox="True" ShowSummary="False" DisplayMode="SingleParagraph"></asp:validationsummary><asp:button id="btnStopThread" style="Z-INDEX: 110; LEFT: 591px; POSITION: absolute; TOP: 12px" runat="server" CssClass="queryButton" Text="Stop Thread" Visible="False"></asp:button>
		</form>
	</body>
</HTML>
