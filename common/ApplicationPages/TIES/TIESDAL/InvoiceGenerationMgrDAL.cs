using System;
using System.Data;
using com.common.classes;
using com.common.DAL;
using com.common.util;
using System.Text;
using com.ties.classes;
using System.Collections;

namespace com.ties.DAL
{
	/// <summary>
	/// Summary description for InvoiceGenerationMgrDAL.
	/// </summary>
	public class InvoiceGenerationMgrDAL
	{
		public InvoiceGenerationMgrDAL()
		{
			//
			// TODO: Add constructor logic here
			//
		}

#region "INV INVOICE GEN"
		//�ѹ������������
		public static DataSet GetInvoiceableShipments(String strAppID, String strEnterpriseID, DataSet dsInvoiceCriteria)
		{
			DataRow dr = dsInvoiceCriteria.Tables[0].Rows[0];

			StringBuilder strBuilder = new StringBuilder();

			strBuilder.Append("select distinct shipment.consignment_no,shipment.ref_no,shipment.payerid,shipment.payer_type,shipment.payer_name,shipment.payer_address1,shipment.payer_address2,shipment.payer_zipcode,shipment.payer_country,shipment.payer_telephone,shipment.payer_fax,shipment.payment_mode,shipment.sender_name,shipment.sender_address1,shipment.sender_address2,shipment.sender_zipcode,shipment.sender_country,shipment.sender_telephone,shipment.sender_fax,shipment.sender_contact_person,shipment.recipient_name,shipment.recipient_address1,shipment.recipient_address2,shipment.recipient_zipcode,shipment.recipient_country,shipment.recipient_telephone,shipment.recipient_fax,shipment.service_code,shipment.recipient_contact_person,shipment.insurance_surcharge,shipment.act_pickup_datetime,shipment.act_delivery_date,shipment.payment_type,shipment.origin_state_code,shipment.destination_state_code,shipment.tot_freight_charge,shipment.invoice_date,shipment.tot_vas_surcharge,shipment.last_status_code,shipment.last_exception_code,shipment.esa_surcharge,shipment.cod_amount,shipment.origin_station,shipment.destination_station,shipment.return_invoice_hc,shipment.wh_tax_amt,shipment.net_cod_amt,shipment.total_cod_amt,shipment.other_surch_amount ");
			strBuilder.Append(" , customer.pod_slip_required, customer.payment_mode as CPayment_mode FROM  Shipment_Tracking RIGHT OUTER JOIN  Shipment ON Shipment_Tracking.applicationid = Shipment.applicationid AND Shipment_Tracking.enterpriseid = Shipment.enterpriseid AND "); 
			strBuilder.Append(" Shipment_Tracking.booking_no = Shipment.booking_no AND Shipment_Tracking.consignment_no = Shipment.consignment_no LEFT OUTER JOIN ");
			strBuilder.Append(" Customer ON Shipment.applicationid = Customer.applicationid AND Shipment.enterpriseid = Customer.enterpriseid AND Shipment.payerid = Customer.custid where ");
			strBuilder.Append(" shipment.enterpriseid ='"+strEnterpriseID+"'");
			strBuilder.Append(" and shipment.applicationid = '" +strAppID+ "'");
			strBuilder.Append(" and shipment.invoice_date IS NULL ");
			//EDITED BY X MAR 06 08	
			//ADD BY X MAR 10 08
			strBuilder.Append(" AND ((Shipment.est_delivery_datetime >= shipment_tracking.tracking_datetime  and Shipment_Tracking.status_code ='POD' ) or (Shipment.est_delivery_datetime < shipment_tracking.tracking_datetime and shipment_tracking.status_code ='PODEX' and isnull(shipment_tracking.status_code, '') <> ''))  ");

			//strBuilder.Append(" and ((Shipment.est_delivery_datetime >= shipment_tracking.tracking_datetime) or (Shipment.est_delivery_datetime < shipment_tracking.tracking_datetime and isnull(shipment_tracking.status_code, '') <> '')) ");
			strBuilder.Append("  AND (Shipment_Tracking.deleted IS NULL or Shipment_Tracking.deleted <> 'Y') ");
			//			strBuilder.Append(" and shipment.invoice_date IS NULL ");
			//			strBuilder.Append(" and Shipment_Tracking.status_code IN ('POD','PODEX') and shipment_tracking.tracking_datetime <= shipment.est_delivery_datetime AND (Shipment_Tracking.deleted IS NULL or Shipment_Tracking.deleted <> 'Y') ");
			
			string strPayment_mode = (string)dr["Payment_mode"]; //C or R in shipment.payment_mode

			if ((dr["Last_Pickup_Date"]!=null) && (!dr["Last_Pickup_Date"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				DateTime Last_Pickup_Date = (DateTime) dr["Last_Pickup_Date"];
				string strLast_Pickup_Date = Utility.DateFormat(strAppID, strEnterpriseID, Last_Pickup_Date, DTFormat.DateTime);
				strBuilder.Append(" and act_pickup_datetime <= "+strLast_Pickup_Date+"");
			}
			else //ADD BY X MAR 06 08
			{
				DateTime Last_Pickup_Date = DateTime.ParseExact(DateTime.Now.ToString("dd/MM/yyyy") +" 23:59:59","dd/MM/yyyy HH:mm:ss",null);
				string strLast_Pickup_Date = Utility.DateFormat(strAppID, strEnterpriseID, Last_Pickup_Date, DTFormat.DateTime);
				strBuilder.Append(" and act_pickup_datetime <= "+strLast_Pickup_Date+"");
			}

			if(strPayment_mode!=null || strPayment_mode !="")
			{
				strBuilder.Append(" and shipment.payment_mode ='");
				strBuilder.Append(strPayment_mode);
				strBuilder.Append("' ");

				if(strPayment_mode=="C")
				{
					if((bool)dr["Cash_Cust"]&&!(bool)dr["Credit_Cust"])
						strBuilder.Append(" and customer.payment_mode IN ('C') ");

					if(!(bool)dr["Cash_Cust"]&&(bool)dr["Credit_Cust"])
						strBuilder.Append(" and customer.payment_mode IN ('R') ");

					if(!(bool)dr["Cash_Cust"]&&!(bool)dr["Credit_Cust"])
						strBuilder.Append(" and customer.payment_mode IN ('C') ");

					if((bool)dr["Cash_Cust"]&&(bool)dr["Credit_Cust"])
						strBuilder.Append(" and customer.payment_mode IN ('C','R') ");

					//strBuilder.Append(" group by shipment.sender_name ");
				}
				else
				{
					string strCustomerCode = (string)dr["Customer_Code"];
					if(strCustomerCode!=null || strCustomerCode !="")
					{
						strBuilder.Append(" and shipment.payerid ='");
						strBuilder.Append(strCustomerCode);
						strBuilder.Append("' ");

						//strBuilder.Append(" group by shipment.payerid ");
					}
				}

			}


			DataSet dsIShip = null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("InvoiceGenerationMgrDAL","GetInvoiceableShipments","EDB101","DbConnection object is null!!");
				return dsIShip;
			}

			//			strBuilder.Append("select sh.payerid, sh.payer_type, sh.payer_name, sh.payer_address1, sh.payer_address2, ");
			//			strBuilder.Append("sh.payer_zipcode, sh.payer_country, sh.payer_telephone, sh.payer_fax, sh.payment_mode, ");
			//			strBuilder.Append("sh.consignment_no, sh.booking_no, sh.booking_datetime, sh.ref_no, sh.origin_state_code, ");
			//			strBuilder.Append("sh.destination_state_code, sh.tot_pkg, sh.tot_wt, sh.tot_dim_wt, sh.chargeable_wt, ");
			//			strBuilder.Append("sh.recipient_name, sh.recipient_address1, sh.recipient_address2, sh.recipient_zipcode, ");
			//			strBuilder.Append("sh.recipient_country, sh.sender_name, sh.sender_address1, sh.sender_address2, ");
			//			strBuilder.Append("sh.sender_zipcode, sh.sender_country, sh.service_code, sh.insurance_surcharge, ");
			//			strBuilder.Append("sh.tot_freight_charge, sh.tot_vas_surcharge, sh.esa_surcharge, ");
			//			strBuilder.Append("c.credit_term AS customer_credit_term, a.credit_term AS agent_credit_term, ");
			//			strBuilder.Append("sh.payment_mode, ");
			//			strBuilder.Append("c.mbg AS customer_mbg, a.mbg AS agent_mbg ");
			//			strBuilder.Append("from Shipment sh, Status_Code sc, Customer c, Agent a ");
			//			strBuilder.Append("where sh.enterpriseid = sc.enterpriseid AND sh.applicationid = sc.applicationid AND ");
			//			strBuilder.Append("sh.last_status_code = sc.status_code AND ");
			//			strBuilder.Append(Utility.FormatOuterJoinString(strAppID, strEnterpriseID, "sh.enterpriseid", "c.enterpriseid", OuterJoin.Left)+" AND ");
			//			strBuilder.Append(Utility.FormatOuterJoinString(strAppID, strEnterpriseID, "sh.applicationid", "c.applicationid", OuterJoin.Left)+" AND ");
			//			strBuilder.Append(Utility.FormatOuterJoinString(strAppID, strEnterpriseID, "sh.payerid", "c.custid", OuterJoin.Left)+" AND ");
			//			strBuilder.Append(Utility.FormatOuterJoinString(strAppID, strEnterpriseID, "sh.enterpriseid", "a.enterpriseid", OuterJoin.Left)+" AND ");
			//			strBuilder.Append(Utility.FormatOuterJoinString(strAppID, strEnterpriseID, "sh.applicationid", "a.applicationid", OuterJoin.Left)+" AND ");
			//			strBuilder.Append(Utility.FormatOuterJoinString(strAppID, strEnterpriseID, "sh.payerid", "a.agentid", OuterJoin.Left)+" AND ");
			//			strBuilder.Append("sh.applicationid = '"+strAppID+"' AND sh.enterpriseid = '"+strEnterpriseID+"' AND ");

			//			strBuilder.Append("sc.invoiceable IN ('Y', 'y') AND (sh.invoice_no is null OR sh.invoice_no = '') ");

			strBuilder.Append(" GROUP BY CUSTOMER.PAYMENT_MODE, CUSTOMER.POD_SLIP_REQUIRED, shipment.consignment_no,shipment.ref_no,shipment.payerid,shipment.payer_type,shipment.payer_name,shipment.payer_address1,shipment.payer_address2,shipment.payer_zipcode,shipment.payer_country,shipment.payer_telephone,shipment.payer_fax,shipment.payment_mode,shipment.sender_name,shipment.sender_address1,shipment.sender_address2,shipment.sender_zipcode,shipment.sender_country,shipment.sender_telephone,shipment.sender_fax,shipment.sender_contact_person,shipment.recipient_name,shipment.recipient_address1,shipment.recipient_address2,shipment.recipient_zipcode,shipment.recipient_country,shipment.recipient_telephone,shipment.recipient_fax,shipment.service_code,shipment.recipient_contact_person,shipment.insurance_surcharge,shipment.act_pickup_datetime,shipment.act_delivery_date,shipment.payment_type,shipment.origin_state_code,shipment.destination_state_code,shipment.tot_freight_charge,shipment.invoice_date,shipment.tot_vas_surcharge,shipment.last_status_code,shipment.last_exception_code,shipment.esa_surcharge,shipment.cod_amount,shipment.origin_station,shipment.destination_station,shipment.return_invoice_hc,shipment.wh_tax_amt,shipment.net_cod_amt,shipment.total_cod_amt,shipment.other_surch_amount ");
			strBuilder.Append(" Order by CUSTOMER.PAYMENT_MODE, shipment.consignment_no ASC  ");
			String strSQLQuery = strBuilder.ToString();

			dsIShip = (DataSet)dbCon.ExecuteQuery(strSQLQuery,ReturnType.DataSetType);

			return dsIShip;
		}
		
		public static bool CheckExistingInvoiceProcess(string strAppID, string strEnterpriseID, string userID, DataSet dsInvoiceCriteria, string sessionID){
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null){
				Logger.LogTraceError("InvoiceGenerationMgrDAL","CheckExistingInvoiceProcess","EDB101","DbConnection object is null!!");
			}

			/*
			System.Collections.ArrayList storedParams = new System.Collections.ArrayList();
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@userID", userID));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@sessionID", sessionID));
			int result = (int) dbCon.ExecuteProcedure("CheckExistingInvoiceProcess",storedParams);
			*/
			DataRow dr = dsInvoiceCriteria.Tables[0].Rows[0];
			string strCustomerCode = (string)dr["Customer_Code"];

			string strFindInvoiceProcessByCustID = "SELECT Data3 ";
			strFindInvoiceProcessByCustID += "FROM dbo.MF_Process_Log l WITH(NOLOCK) ";
			strFindInvoiceProcessByCustID += "WHERE Step = 'INITIALIZATION' and Job = 'GenerateInvoices' and DATEDIFF(minute,Start_DT,GETDATE()) < 45 and ";
			strFindInvoiceProcessByCustID += " NOT EXISTS ( ";
			strFindInvoiceProcessByCustID += " SELECT Step FROM dbo.MF_Process_Log l1 WITH(NOLOCK) ";
			strFindInvoiceProcessByCustID += "WHERE l1.applicationid = l.applicationid and l1.enterpriseid = l.enterpriseid and ";
			strFindInvoiceProcessByCustID += "l1.Step = 'JOB COMPLETE' and Job = 'GenerateInvoices' and l1.spid = l.spid and l1.dbid = l.dbid and l1.kpid = l.kpid and l1.Start_DT = l.Start_DT) ";
			strFindInvoiceProcessByCustID += "and Data3 = '" + strCustomerCode + "'";

			string strFindInvoiceProcess = "SELECT Data2 FROM dbo.MF_Process_Log l WITH(NOLOCK) ";
			strFindInvoiceProcess += "WHERE Step = 'INITIALIZATION' and Job = 'GenerateInvoices' and ";
			// strFindInvoiceProcess += "and DATEDIFF(minute,Start_DT,GETDATE()) < 45 and ";
			strFindInvoiceProcess += "NOT EXISTS ( ";
			strFindInvoiceProcess += "SELECT Step FROM dbo.MF_Process_Log l1 WITH(NOLOCK) ";
			strFindInvoiceProcess += "WHERE l1.applicationid = l.applicationid and l1.enterpriseid = l.enterpriseid and l1.Step = 'JOB COMPLETE' and Job = 'GenerateInvoices' ";
			strFindInvoiceProcess += "and l1.spid = l.spid and l1.dbid = l.dbid and l1.kpid = l.kpid and l1.Start_DT = l.Start_DT) ";
			strFindInvoiceProcess += "and Data2 = '" + userID + "/" + sessionID + "'"; 

			DataSet dsInvoiceProcessByCust = (DataSet)dbCon.ExecuteQuery(strFindInvoiceProcessByCustID, ReturnType.DataSetType);
			DataSet dsInvoiceProcess = (DataSet)dbCon.ExecuteQuery(strFindInvoiceProcess, ReturnType.DataSetType);
			if(dsInvoiceProcess.Tables[0].Rows.Count > 0 || dsInvoiceProcessByCust.Tables[0].Rows.Count > 0)
				return true;
			else
				return false;
		}
 
		public static bool GenTempInvoicePreview(string strAppID, string strEnterpriseID,string userID, DataSet dsInvoiceCriteria,string sessionID)
		{  //CLEAR ALL OLD DATA
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("InvoiceGenerationMgrDAL","GetInvoiceableShipments","EDB101","DbConnection object is null!!");
			}

			//string SQLinvoicePreviewHeader=null;
			int OK=0;
			
			IDbCommand dbCmd = dbCon.CreateCommand(" delete from InvoicePreview_Header where userID='"+userID+"' and enterpriseid ='"+strEnterpriseID+"' and applicationid = '" +strAppID+ "' and SessionID = '"+ sessionID +"' ",CommandType.Text);
			OK = dbCon.ExecuteNonQuery(dbCmd);

			dbCmd = dbCon.CreateCommand(" delete from InvoicePreview_Details where userID='"+userID+"' and enterpriseid ='"+strEnterpriseID+"' and applicationid = '" +strAppID+ "' and SessionID = '"+sessionID+"'",CommandType.Text);
			OK = dbCon.ExecuteNonQuery(dbCmd);
			#region"Comment :: Invoice can be generated without HCRHQ"
			//*************** Start	Invoice can be generated without HCRHQ ***********************************************************************
			//*************** Chai 08/08/2012  *********************
			//			#region"GET HEADER"
			//			//SELECT DATA THROUGH CRITERIA
			//			//GET THE LIST OF CUSTOMER
			//			DataRow dr = dsInvoiceCriteria.Tables[0].Rows[0];
			//
			//			string strPayment_mode = (string)dr["Payment_mode"]; //C or R in shipment.payment_mode
			//
			//			StringBuilder strSelectBuilder1 = new StringBuilder();
			//
			//			StringBuilder strSelectBuilder2 = new StringBuilder();
			//			StringBuilder strSelectBuilder3 = new StringBuilder();
			//
			//			StringBuilder strFromBuilder1 = new StringBuilder();
			//
			//			StringBuilder strFromBuilder2 = new StringBuilder();
			//			StringBuilder strFromBuilder3 = new StringBuilder();
			//
			//			StringBuilder strWhereBuilder1 = new StringBuilder();
			//			StringBuilder strWhereBuilder2 = new StringBuilder();
			//			StringBuilder strWhereBuilder3 = new StringBuilder();
			//
			//			//Modify by GwanG on 01/08/08 ## Change the table is pulled out from shipment to customer in case of consignment payment mode, customer payment mode is credit. ## 
			//			//for CREDIT AND OTHER
			//			if(strPayment_mode=="R")
			//			{
			//				strSelectBuilder1.Append("select distinct '"+strAppID+"' as applicationid,'"+strEnterpriseID+"' as enterpriseid,'"+ userID +"' as userID, ");
			//				strSelectBuilder1.Append("customer.custid,customer.cust_name as sender_name,customer.address1 as sender_address1,customer.address2 as sender_address2,customer.zipcode AS sender_zipcode,customer.telephone  AS sender_telephone,customer.fax  AS sender_fax, customer.pod_slip_required, 'R' as CPayment_Mode, '" + sessionID +"'");//strSelectBuilder1.Append("shipment.payerid,shipment.payer_name as sender_name,shipment.payer_address1 as sender_address1,shipment.payer_address2 as sender_address2,shipment.payer_zipcode AS sender_zipcode,shipment.payer_telephone  AS sender_telephone,shipment.payer_fax  AS sender_fax, customer.pod_slip_required, 'R' as CPayment_Mode ");
			//			}
			//			//for CONSIGNMENT GROUP CASH
			//			if(strPayment_mode=="C")
			//			{
			//				//CASE OF CASH & CREDIT CUST
			//				if((bool)dr["Cash_Cust"])
			//				{
			//					strSelectBuilder2.Append(" select distinct '"+strAppID+"' as applicationid,'"+strEnterpriseID+"' as enterpriseid,'"+ userID +"' as userID, min(payerID) as payerID, sender_name as Sender_Name,min(sender_address1) AS sender_address1,min(sender_address2) AS sender_address2, min(sender_zipcode) AS sender_zipcode,min(sender_telephone) AS sender_telephone, min(sender_fax) AS sender_fax,min(pod_slip_required)  AS pod_slip_required, CPayment_mode ,'" + sessionID +"' AS SessionID ");
			//					strSelectBuilder2.Append(" from ( select distinct ");
			//					strSelectBuilder2.Append(" CASE customer.payment_mode WHEN 'C' THEN shipment.payerid ELSE customer.custid END AS payerID, ");//strSelectBuilder2.Append(" CASE customer.payment_mode WHEN 'C' THEN customer.custid ELSE shipment.payerid END AS payerID, ");
			//					strSelectBuilder2.Append(" CASE customer.payment_mode WHEN 'C' THEN shipment.sender_name ELSE customer.cust_name END AS sender_name, ");//strSelectBuilder2.Append(" CASE customer.payment_mode WHEN 'C' THEN shipment.sender_name ELSE shipment.payer_name END AS sender_name, ");
			//					strSelectBuilder2.Append(" CASE customer.payment_mode WHEN 'C' THEN min(shipment.sender_address1) ELSE min(customer.address1) END AS sender_address1, ");//strSelectBuilder2.Append(" CASE customer.payment_mode WHEN 'C' THEN min(shipment.sender_address1) ELSE min(shipment.payer_address1) END AS sender_address1, ");
			//					strSelectBuilder2.Append(" CASE customer.payment_mode WHEN 'C' THEN min(shipment.sender_address2) ELSE min(customer.address2) END AS sender_address2, ");//strSelectBuilder2.Append(" CASE customer.payment_mode WHEN 'C' THEN min(shipment.sender_address2) ELSE min(shipment.payer_address2) END AS sender_address2, ");
			//					strSelectBuilder2.Append(" CASE customer.payment_mode WHEN 'C' THEN min(shipment.sender_zipcode) ELSE min(customer.zipcode) END AS sender_zipcode, ");//strSelectBuilder2.Append(" CASE customer.payment_mode WHEN 'C' THEN min(shipment.sender_zipcode) ELSE min(shipment.payer_zipcode) END AS sender_zipcode, ");
			//					strSelectBuilder2.Append(" CASE customer.payment_mode WHEN 'C' THEN min(shipment.sender_telephone) ELSE min(customer.telephone) END AS sender_telephone, ");//strSelectBuilder2.Append(" CASE customer.payment_mode WHEN 'C' THEN min(shipment.sender_telephone) ELSE min(shipment.payer_telephone) END AS sender_telephone, ");
			//					strSelectBuilder2.Append(" CASE customer.payment_mode WHEN 'C' THEN min(shipment.sender_fax) ELSE min(customer.fax) END AS sender_fax, 'NO' as pod_slip_required, ");//strSelectBuilder2.Append(" CASE customer.payment_mode WHEN 'C' THEN min(shipment.sender_fax) ELSE min(shipment.payer_fax) END AS sender_fax, 'NO' as pod_slip_required, ");
			//					strSelectBuilder2.Append(" customer.payment_mode as CPayment_mode");//strSelectBuilder2.Append(" customer.payment_mode as CPayment_mode ");
			//				}
			//
			//				if((bool)dr["Credit_Cust"])
			//				{
			//					strSelectBuilder3.Append(" select distinct '"+strAppID+"' as applicationid,'"+strEnterpriseID+"' as enterpriseid,'"+ userID +"' as userID,payerID,min(sender_name) as sender_name,min(sender_address1) as sender_address1,min(sender_address2) as sender_address2, min(sender_zipcode) as sender_zipcode,min(sender_telephone) as sender_telephone, min(sender_fax) as sender_fax,min(pod_slip_required) as pod_slip_required, CPayment_mode,'"+sessionID+"' AS SessionID from ( ");
			//					strSelectBuilder3.Append(" select distinct '"+strAppID+"' as applicationid,'"+strEnterpriseID+"' as enterpriseid,'"+ userID +"' as userID, ");//strSelectBuilder3.Append(" select distinct '"+strAppID+"' as applicationid,'"+strEnterpriseID+"' as enterpriseid,'"+ userID +"' as userID, ");
			//					strSelectBuilder3.Append(" CASE customer.payment_mode WHEN 'C' THEN shipment.payerid ELSE customer.custid END AS payerID, ");//strSelectBuilder2.Append(" CASE customer.payment_mode WHEN 'C' THEN customer.custid ELSE shipment.payerid END AS payerID, ");
			//					strSelectBuilder3.Append(" CASE customer.payment_mode WHEN 'C' THEN shipment.sender_name ELSE customer.cust_name END AS sender_name, ");//strSelectBuilder2.Append(" CASE customer.payment_mode WHEN 'C' THEN shipment.sender_name ELSE shipment.payer_name END AS sender_name, ");
			//					strSelectBuilder3.Append(" CASE customer.payment_mode WHEN 'C' THEN min(shipment.sender_address1) ELSE min(customer.address1) END AS sender_address1, ");//strSelectBuilder2.Append(" CASE customer.payment_mode WHEN 'C' THEN min(shipment.sender_address1) ELSE min(shipment.payer_address1) END AS sender_address1, ");
			//					strSelectBuilder3.Append(" CASE customer.payment_mode WHEN 'C' THEN min(shipment.sender_address2) ELSE min(customer.address2) END AS sender_address2, ");//strSelectBuilder2.Append(" CASE customer.payment_mode WHEN 'C' THEN min(shipment.sender_address2) ELSE min(shipment.payer_address2) END AS sender_address2, ");
			//					strSelectBuilder3.Append(" CASE customer.payment_mode WHEN 'C' THEN min(shipment.sender_zipcode) ELSE min(customer.zipcode) END AS sender_zipcode, ");//strSelectBuilder2.Append(" CASE customer.payment_mode WHEN 'C' THEN min(shipment.sender_zipcode) ELSE min(shipment.payer_zipcode) END AS sender_zipcode, ");
			//					strSelectBuilder3.Append(" CASE customer.payment_mode WHEN 'C' THEN min(shipment.sender_telephone) ELSE min(customer.telephone) END AS sender_telephone, ");//strSelectBuilder2.Append(" CASE customer.payment_mode WHEN 'C' THEN min(shipment.sender_telephone) ELSE min(shipment.payer_telephone) END AS sender_telephone, ");
			//					strSelectBuilder3.Append(" CASE customer.payment_mode WHEN 'C' THEN min(shipment.sender_fax) ELSE min(customer.fax) END AS sender_fax, 'NO' as pod_slip_required, ");//strSelectBuilder2.Append(" CASE customer.payment_mode WHEN 'C' THEN min(shipment.sender_fax) ELSE min(shipment.payer_fax) END AS sender_fax, 'NO' as pod_slip_required, ");
			//					strSelectBuilder3.Append(" customer.payment_mode as CPayment_mode");//strSelectBuilder2.Append(" customer.payment_mode as CPayment_mode ");
			//				}	
			//			}
			//			//end of modify
			//			strFromBuilder1.Append(" FROM  Shipment_Tracking WITH(NOLOCK) RIGHT OUTER JOIN  Shipment WITH(NOLOCK) ON Shipment_Tracking.applicationid = Shipment.applicationid AND Shipment_Tracking.enterpriseid = Shipment.enterpriseid AND "); 
			//			strFromBuilder1.Append(" Shipment_Tracking.booking_no = Shipment.booking_no AND Shipment_Tracking.consignment_no = Shipment.consignment_no LEFT OUTER JOIN ");
			//			strFromBuilder1.Append(" Customer WITH(NOLOCK) ON Shipment.applicationid = Customer.applicationid AND Shipment.enterpriseid = Customer.enterpriseid AND Shipment.payerid = Customer.custid where ");
			//			strFromBuilder1.Append(" shipment.enterpriseid ='"+strEnterpriseID+"'");
			//			strFromBuilder1.Append(" and shipment.applicationid = '" +strAppID+ "'");
			//			strFromBuilder1.Append(" and shipment.invoice_date IS NULL ");
			//			strFromBuilder1.Append(" AND ((Shipment.est_delivery_datetime >= shipment_tracking.tracking_datetime  and Shipment_Tracking.status_code ='POD' ) or (Shipment.est_delivery_datetime < shipment_tracking.tracking_datetime and shipment_tracking.status_code ='PODEX' and isnull(shipment_tracking.status_code, '') <> '') ");
			//			strFromBuilder1.Append(" OR (Shipment.est_delivery_datetime < shipment_tracking.tracking_datetime and shipment_tracking.status_code='POD' AND EXISTS(SELECT status_code FROM Shipment_Tracking AS Emp WHERE Emp.consignment_no = Shipment_Tracking.consignment_no AND Emp.applicationid = Shipment_Tracking.applicationid AND Emp.enterpriseid = Shipment_Tracking.enterpriseid  AND Emp.status_code='PODEX') )) ");
			//			strFromBuilder1.Append("  AND (Shipment_Tracking.deleted IS NULL or Shipment_Tracking.deleted <> 'Y') ");
			//					
			//
			//			if ((dr["Last_Pickup_Date"]!=null) && (!dr["Last_Pickup_Date"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			//			{
			//				DateTime Last_Pickup_Date = (DateTime) dr["Last_Pickup_Date"];
			//				string strLast_Pickup_Date = Utility.DateFormat(strAppID, strEnterpriseID, Last_Pickup_Date, DTFormat.DateTime);
			//				strFromBuilder1.Append(" and act_pickup_datetime <= "+strLast_Pickup_Date+"");
			//				strFromBuilder2.Append(" and act_pickup_datetime <= "+strLast_Pickup_Date+"");
			//				strFromBuilder3.Append(" and act_pickup_datetime <= "+strLast_Pickup_Date+"");
			//			}
			//			else
			//			{
			//				DateTime Last_Pickup_Date = DateTime.ParseExact(DateTime.Now.ToString("dd/MM/yyyy") +" 23:59:59","dd/MM/yyyy HH:mm:ss",null);
			//				string strLast_Pickup_Date = Utility.DateFormat(strAppID, strEnterpriseID, Last_Pickup_Date, DTFormat.DateTime);
			//				strFromBuilder1.Append(" and act_pickup_datetime <= "+strLast_Pickup_Date+"");
			//				strFromBuilder2.Append(" and act_pickup_datetime <= "+strLast_Pickup_Date+"");
			//				strFromBuilder3.Append(" and act_pickup_datetime <= "+strLast_Pickup_Date+"");
			//			}
			//
			//			if(strPayment_mode!=null || strPayment_mode !="")
			//			{
			//				strFromBuilder1.Append(" and shipment.payment_mode ='");
			//				strFromBuilder1.Append(strPayment_mode);
			//				strFromBuilder1.Append("' ");
			//
			//				if(strPayment_mode=="C")
			//				{
			//					if((bool)dr["Cash_Cust"])
			//						strFromBuilder2.Append(" and customer.payment_mode IN ('C') ");
			//
			//					if((bool)dr["Credit_Cust"])
			//						strFromBuilder3.Append(" and customer.payment_mode IN ('R') ");
			//					//Jeab 21 Dec 2011
			//					string strCustomerCode = (string)dr["Customer_Code"];
			//					if(strCustomerCode!=null || strCustomerCode !="")
			//					{
			//						strFromBuilder1.Append(" and shipment.payerid ='");
			//						strFromBuilder1.Append(strCustomerCode);
			//						strFromBuilder1.Append("' ");
			//					}
			//					//Jeab 21 Dec 2011  =========>  End
			//				}
			//				else
			//				{
			//					string strCustomerCode = (string)dr["Customer_Code"];
			//					if(strCustomerCode!=null || strCustomerCode !="")
			//					{
			//						strFromBuilder1.Append(" and shipment.payerid ='");
			//						strFromBuilder1.Append(strCustomerCode);
			//						strFromBuilder1.Append("' ");
			//					}
			//				}
			//
			//			}
			//
			//			//for CREDIT AND OTHER
			//			if(strPayment_mode=="R")
			//			{
			//				strWhereBuilder1.Append(" and shipment.consignment_no = (select top 1 shipment.consignment_no " + strFromBuilder1.ToString() + " )");
			//				//Modified by GwanG on 01/08/2008
			//				strWhereBuilder1.Append(" GROUP BY Customer.custid,Customer.cust_name,Customer.address1,Customer.address2,Customer.zipcode,Customer.telephone,Customer.fax ,Customer.pod_slip_required, Customer.payment_mode  ");//strWhereBuilder1.Append(" GROUP BY shipment.payerid,shipment.payer_name,shipment.payer_address1,shipment.payer_address2,shipment.payer_zipcode,shipment.payer_telephone,shipment.payer_fax, customer.pod_slip_required, customer.payment_mode, customer.custid  ");
			//				////strWhereBuilder1.Append(" Order by CUSTOMER.PAYMENT_MODE, shipment.payerid ASC  ");
			//				//end modified
			//			}
			//				
			//			//for CONSIGNMENT GROUP CASH
			//			if(strPayment_mode=="C")
			//			{
			//
			//				if((bool)dr["Cash_Cust"])
			//				{
			//					strWhereBuilder2.Append(" GROUP BY  Customer.custid ,Customer.cust_name,Customer.payment_mode ,Shipment.payerID,Shipment.sender_name, Shipment.payment_mode  ");
			//					strWhereBuilder2.Append(" ) as YYY group by Sender_Name, CPayment_Mode ");  
			//				}
			//
			//				if((bool)dr["Credit_Cust"])
			//				{
			//					//Modified by GwanG on 01/08/2008
			//					strWhereBuilder3.Append(" GROUP BY  Customer.custid ,Customer.cust_name,Customer.payment_mode ,Shipment.payerID,Shipment.sender_name, Shipment.payment_mode   ");
			//					//end modified
			//					strWhereBuilder3.Append(" ) as XXX Group By  payerID, CPayment_mode ");
			//					
			//				}
			//			}
			//
			//
			//			string strSQL1=null,strSQL2=null,strSQLQuery=null; // = strSelectBuilder.ToString() + strFromBuilder.ToString() + strWhereBuilder.ToString();
			//
			//			//IF CONSIGNMENT GROUP CASH && IT HAS CASH CUSTOMER ALSO , NEEDS UNION
			//			if(strPayment_mode=="C")
			//			{
			//
			//				if((bool)dr["Cash_Cust"])
			//				{
			//					strSQL1 = strSelectBuilder2.ToString() + strFromBuilder1.ToString() + strFromBuilder2.ToString() + strWhereBuilder2.ToString();
			//				}
			//
			//				if((bool)dr["Credit_Cust"])
			//				{
			//					strSQL2 = strSelectBuilder3.ToString() + strFromBuilder1.ToString() + strFromBuilder3.ToString() + strWhereBuilder3.ToString();
			//				}
			//
			//				if((bool)dr["Cash_Cust"]&&(bool)dr["Credit_Cust"]) //FOR CASH INV & BOTH CASH&CREDIT CUST, but line below is for SECOND UNION IS CASH
			//				{
			//					strSQLQuery = strSQL1.ToString() + " UNION " +strSQL2.ToString() + " order by CPayment_mode, Sender_Name";
			//				}
			//						
			//				if(strSQLQuery==null)
			//				{
			//					if(strSQL1!=null)
			//						strSQLQuery = strSQL1.ToString()+ " order by payerID";//Jeab  22 Dec 2011  
			//						//strSQLQuery = strSQL1.ToString()+ " order by sender_name";
			//					else
			//						strSQLQuery = strSQL2.ToString() + " order by payerID" ;
			//				}
			//			}
			//			else
			//				strSQLQuery = strSelectBuilder1.ToString() + strFromBuilder1.ToString() + strWhereBuilder1.ToString();
			//
			//
			//			SQLinvoicePreviewHeader = " Insert into InvoicePreview_Header " + strSQLQuery;
			//
			//			OK=0;
			//			dbCmd = dbCon.CreateCommand(SQLinvoicePreviewHeader,CommandType.Text);
			//			dbCmd.CommandTimeout = 600;  //Add Command TimeOut for case of serveral records: 600 seconds (10 minutes)
			//			OK = dbCon.ExecuteNonQuery(dbCmd);
			//
			//			if(OK<=0)
			//				return false;
			//
			//			#endregion
			//
			//			#region"GET DETAILS"
			//
			//			string strCustID=null,strCustomerName=null,strCHECKED=null,strPod_slip_required="",strCPayment_mode="";
			//
			//			DataSet dsHeader = (DataSet)dbCon.ExecuteQuery("select * from  InvoicePreview_Header where userID='"+userID+"' and enterpriseid ='"+strEnterpriseID+"' and applicationid = '" +strAppID+ "' and SessionID = '"+sessionID+"'",ReturnType.DataSetType);
			//
			//			if(!(dsHeader.Tables[0].Rows.Count>0))
			//				return false;
			//
			//			for(int i = 0;i<dsHeader.Tables[0].Rows.Count;i++)
			//			{
			//				DataRow drHeader = dsHeader.Tables[0].Rows[i];
			//				if(((drHeader["payerID"]!= null) && (!drHeader["payerID"].GetType().Equals(System.Type.GetType("System.DBNull")))))
			//					strCustID = drHeader["payerID"].ToString();
			//
			//				if(((drHeader["Sender_Name"]!= null) && (!drHeader["Sender_Name"].GetType().Equals(System.Type.GetType("System.DBNull")))))
			//					strCustomerName = drHeader["Sender_Name"].ToString();
			//
			//				if(((drHeader["CPayment_mode"]!= null) && (!drHeader["CPayment_mode"].GetType().Equals(System.Type.GetType("System.DBNull")))))
			//					strCPayment_mode = drHeader["CPayment_mode"].ToString();
			//
			//				if(((drHeader["pod_slip_required"]!= null) && (!drHeader["pod_slip_required"].GetType().Equals(System.Type.GetType("System.DBNull")))))
			//				{
			//					strPod_slip_required = drHeader["pod_slip_required"].ToString().Trim().ToString();
			//
			//					if(drHeader["pod_slip_required"].ToString().Trim().ToString()=="NO")
			//						strCHECKED = "N";
			//					else
			//						strCHECKED = "Y";
			//				}
			//
			//			
			//				#region"INSERT CONSIG TO TABLE"
			//				//��ͧ�� LIST OF CONSIGNMENT NO. �Ҵ֧仨��
			//				StringBuilder strSelectBuilder = new StringBuilder();
			//				StringBuilder strFromBuilder = new StringBuilder();
			//				StringBuilder strWhereBuilder = new StringBuilder();
			//
			//				//for CREDIT AND OTHER
			//				if(strPayment_mode=="R")
			//					strSelectBuilder.Append(" select distinct '"+strAppID+"' as applicationid,'"+strEnterpriseID+"' as enterpriseid,'"+ userID +"' as userID, '"+ strCHECKED +"' as CHECKED, shipment.payer_name as sender_name ,shipment.payerid, shipment.consignment_no,shipment.ref_no,shipment.sender_zipcode, shipment.act_pickup_datetime,shipment.recipient_name, shipment.recipient_zipcode, shipment.act_delivery_date, shipment.service_code, shipment.tot_freight_charge, shipment.insurance_surcharge, shipment.other_surch_amount, shipment.tot_vas_surcharge, shipment.esa_surcharge, shipment.last_exception_code , '"+strPod_slip_required+"' as pod_slip_required , '"+sessionID+"' ");
			//				//for CONSIGNMENT GROUP CASH
			//				if(strPayment_mode=="C")
			//					strSelectBuilder.Append(" select distinct '"+strAppID+"' as applicationid,'"+strEnterpriseID+"' as enterpriseid,'"+ userID +"' as userID,'"+ strCHECKED +"' as CHECKED, shipment.sender_name ,shipment.payerid, shipment.consignment_no,shipment.ref_no,shipment.sender_zipcode, shipment.act_pickup_datetime,shipment.recipient_name, shipment.recipient_zipcode, shipment.act_delivery_date, shipment.service_code, shipment.tot_freight_charge, shipment.insurance_surcharge, shipment.other_surch_amount, shipment.tot_vas_surcharge, shipment.esa_surcharge, shipment.last_exception_code  , 'NO' as pod_slip_required, '"+sessionID+"'  ");
			//
			//				strFromBuilder.Append(" FROM  Shipment_Tracking WITH(NOLOCK)  RIGHT OUTER JOIN  Shipment WITH(NOLOCK) ON Shipment_Tracking.applicationid = Shipment.applicationid AND Shipment_Tracking.enterpriseid = Shipment.enterpriseid AND "); 
			//				strFromBuilder.Append(" Shipment_Tracking.booking_no = Shipment.booking_no AND Shipment_Tracking.consignment_no = Shipment.consignment_no LEFT OUTER JOIN ");
			//				strFromBuilder.Append(" Customer WITH(NOLOCK) ON Shipment.applicationid = Customer.applicationid AND Shipment.enterpriseid = Customer.enterpriseid AND Shipment.payerid = Customer.custid where ");
			//				strFromBuilder.Append(" shipment.enterpriseid ='"+strEnterpriseID+"'");
			//				strFromBuilder.Append(" and shipment.applicationid = '" +strAppID+ "'");
			//				strFromBuilder.Append(" and shipment.invoice_date IS NULL ");
			//				strFromBuilder.Append(" AND ((Shipment.est_delivery_datetime >= shipment_tracking.tracking_datetime  and Shipment_Tracking.status_code ='POD' ) or (Shipment.est_delivery_datetime < shipment_tracking.tracking_datetime and shipment_tracking.status_code ='PODEX' and isnull(shipment_tracking.status_code, '') <> '') ");
			//				strFromBuilder.Append(" OR (Shipment.est_delivery_datetime < shipment_tracking.tracking_datetime and shipment_tracking.status_code='POD' AND EXISTS(SELECT status_code FROM Shipment_Tracking AS Emp WHERE Emp.consignment_no = Shipment_Tracking.consignment_no AND Emp.applicationid = Shipment_Tracking.applicationid AND Emp.enterpriseid = Shipment_Tracking.enterpriseid AND Emp.status_code='PODEX') ))  ");
			//				strFromBuilder.Append("  AND (Shipment_Tracking.deleted IS NULL or Shipment_Tracking.deleted <> 'Y') ");	
			//
			//				if ((dr["Last_Pickup_Date"]!=null) && (!dr["Last_Pickup_Date"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			//				{
			//					DateTime Last_Pickup_Date = (DateTime) dr["Last_Pickup_Date"];
			//					string strLast_Pickup_Date = Utility.DateFormat(strAppID, strEnterpriseID, Last_Pickup_Date, DTFormat.DateTime);
			//					strFromBuilder.Append(" and act_pickup_datetime <= "+strLast_Pickup_Date+"");
			//				}
			//				else
			//				{
			//					DateTime Last_Pickup_Date = DateTime.ParseExact(DateTime.Now.ToString("dd/MM/yyyy") +" 23:59:59","dd/MM/yyyy HH:mm:ss",null);
			//					string strLast_Pickup_Date = Utility.DateFormat(strAppID, strEnterpriseID, Last_Pickup_Date, DTFormat.DateTime);
			//					strFromBuilder.Append(" and act_pickup_datetime <= "+strLast_Pickup_Date+"");
			//				}
			//
			//				if(strPayment_mode!=null || strPayment_mode !="")
			//				{
			//					strFromBuilder.Append(" and shipment.payment_mode ='");
			//					strFromBuilder.Append(strPayment_mode);
			//					strFromBuilder.Append("' ");
			//
			//					if(strPayment_mode=="C" && strCPayment_mode.Trim().Equals("C".Trim()))
			//					{
			//						if((bool)dr["Cash_Cust"]&&!(bool)dr["Credit_Cust"])
			//							strFromBuilder.Append(" and customer.payment_mode IN ('C') ");
			//
			//						if(!(bool)dr["Cash_Cust"]&&(bool)dr["Credit_Cust"])
			//							strFromBuilder.Append(" and customer.payment_mode IN ('R') ");
			//
			//						if(!(bool)dr["Cash_Cust"]&&!(bool)dr["Credit_Cust"])
			//							strFromBuilder.Append(" and customer.payment_mode IN ('C') ");
			//
			//						if((bool)dr["Cash_Cust"]&&(bool)dr["Credit_Cust"])
			//							strFromBuilder.Append(" and customer.payment_mode IN ('C','R') ");
			//								
			//						strFromBuilder.Append(" and replace(shipment.sender_name,'''','') =N'");
			//						strFromBuilder.Append(strCustomerName.Replace("'","").ToString());
			//						strFromBuilder.Append("' ");
			//
			//					}
			//					else
			//					{
			//						strFromBuilder.Append(" and shipment.payerid ='");
			//						strFromBuilder.Append(strCustID);
			//						strFromBuilder.Append("' ");
			//					}
			//
			//				}
			//
			//				//for CREDIT AND OTHER
			//				if(strPayment_mode=="R")
			//				{
			//					strWhereBuilder.Append(" and shipment.consignment_no IN (select shipment.consignment_no " + strFromBuilder.ToString() + " )");
			//					strWhereBuilder.Append(" and shipment.payerid ='" + strCustID + "'");
			//				}
			//				//for CONSIGNMENT GROUP CASH
			//				if(strPayment_mode=="C")
			//				{
			//					//Jeab 27 Dec 2011
			//					strWhereBuilder.Append(" and shipment.payerid ='" + strCustID + "'");
			//					//Jeab 27 Dec 2011  =========>  End
			//					strWhereBuilder.Append(" Group BY shipment.consignment_no,shipment.ref_no,shipment.sender_zipcode, shipment.act_pickup_datetime,shipment.recipient_name, shipment.recipient_zipcode, shipment.act_delivery_date, shipment.service_code, shipment.tot_freight_charge, shipment.insurance_surcharge, shipment.other_surch_amount, shipment.tot_vas_surcharge, shipment.esa_surcharge, shipment.last_exception_code, shipment.sender_name ,shipment.payerid");
			//				}
			//
			//				strWhereBuilder.Append(" Order by shipment.consignment_no ASC  ");
			//
			//				string insDetails = "Insert into InvoicePreview_Details " + strSelectBuilder.ToString() + strFromBuilder.ToString() + strWhereBuilder.ToString();
			//
			//				dbCmd = dbCon.CreateCommand(insDetails,CommandType.Text);
			//				dbCmd.CommandTimeout = 600;  //Add Command TimeOut for case of serveral records: 600 seconds (10 minutes)
			//				OK = dbCon.ExecuteNonQuery(dbCmd);
			//
			//				if(OK<=0)
			//					return false;
			//
			//				#endregion
			//
			//			} //for loop of HEADER
			//
			//			#endregion //OF GET DETAILS

			//*************** End	Invoice can be generated without HCRHQ ***********************************************************************
			#endregion
			DataRow dr = dsInvoiceCriteria.Tables[0].Rows[0];
			string strPayment_mode = (string)dr["Payment_mode"]; //C or R in shipment.payment_mode
			string strPayerID = (string)dr["Customer_Code"]; //C or R in shipment.payment_mode
			DateTime dActualPickupDT = DateTime.Now;
			if ((dr["Last_Pickup_Date"]!=null) && (!dr["Last_Pickup_Date"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				dActualPickupDT = (DateTime) dr["Last_Pickup_Date"];	
			}
			else
			{
				dActualPickupDT = DateTime.ParseExact(DateTime.Now.ToString("dd/MM/yyyy") +" 23:59:59","dd/MM/yyyy HH:mm:ss",null);
			}
				


			IDbCommand dbCmdPreView = null;
			System.Collections.ArrayList storedParams = new System.Collections.ArrayList();
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@enterpriseid",strEnterpriseID));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@applicationid",strAppID));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@userID",userID));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@sessionID",sessionID));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@paymentMode",strPayment_mode));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@payerID",strPayerID));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@actualPickupDT",dActualPickupDT));

			dbCmdPreView = dbCon.CreateCommand("Generate_Invoice_Preview_Header_Details", storedParams, CommandType.StoredProcedure);

			int result = (int)dbCon.ExecuteNonQuery(dbCmdPreView);


//			#region"CHECKING HCR STATUS AND UPDATE CONSIG Y/N"//IN CASE OF HCR IN TRACKING, CHECK STATUS WILL BE "NO"
//			//FOR CREDIT CUST ONLY
//			//dsInvoiceResult = InvoiceGenerationMgrDAL.GetTempInvoiceableShipmentsList(strAppID,strEnterpriseID,userID,drCurrent["CPayment_mode"].ToString(),strCustID,strCustomerName);
//			if(strPayment_mode=="C") //�����ᤪ ������ͧ�� HCR STATUS ������ ���º�¨ҡ�ѧ���������
//				return true;
//			
//			DataSet dsPreview_Details = (DataSet)dbCon.ExecuteQuery("SELECT * FROM InvoicePreview_Details WITH(NOLOCK) WHERE userID='"+userID+"' and enterpriseid ='"+strEnterpriseID+"' and applicationid = '" +strAppID+ "' and payerID <> 'CASH' and SessionID = '"+sessionID+"'",ReturnType.DataSetType);
//
//			if(dsPreview_Details.Tables[0].Rows.Count>0)
//			{
//				for(int i = 0;i<dsPreview_Details.Tables[0].Rows.Count;i++)
//				{
//					if(((dsPreview_Details.Tables[0].Rows[i]["consignment_no"]!= null) && (!dsPreview_Details.Tables[0].Rows[i]["consignment_no"].GetType().Equals(System.Type.GetType("System.DBNull")))))
//					{
//						if(checkHCReturned(strAppID,strEnterpriseID,dsPreview_Details.Tables[0].Rows[i]["consignment_no"].ToString()))
//							UpdateTempCheckedConsignment(strAppID,strEnterpriseID,userID,dsPreview_Details.Tables[0].Rows[i]["consignment_no"].ToString(),true, sessionID);
//						else
//							UpdateTempCheckedConsignment(strAppID,strEnterpriseID,userID,dsPreview_Details.Tables[0].Rows[i]["consignment_no"].ToString(),false,sessionID);
//					}
//				}
//			}
//
//			#endregion



			return true;
		}


		public static SessionDS GetTempInvoiceableShipmentsHeader(string strAppID, string strEnterpriseID,int iCurrent,int iDSRecSize,string userID, string SessionID)
		{ //GET THE LIST OF CUSTOMER
			SessionDS dsIShip = null;

			IDbCommand dbCmd = null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("InvoiceGenerationMgrDAL","GetInvoiceableShipments","EDB101","DbConnection object is null!!");
				return dsIShip;
			}
			//for CREDIT AND OTHER
			string strQueryHeader = "select * from invoicePreview_header with(nolock) where userID='"+userID+"' and enterpriseid ='"+strEnterpriseID+"' and applicationid = '" +strAppID+ "' and SessionID = '"+SessionID+"'";
			strQueryHeader+= " order by payerID,Sender_Name ASC ";
			
			//dsIShip = dbCon.ExecuteQuery(strQueryHeader,iCurrent,iDSRecSize,"SHIPMENT");
			dbCmd = dbCon.CreateCommand(strQueryHeader, CommandType.Text);
			dbCmd.CommandTimeout = 600;  //Add Command TimeOut for case of serveral records: 600 seconds (10 minutes)
			dsIShip = dbCon.ExecuteQuery(dbCmd, iCurrent, iDSRecSize, "InvoiceableShipmentsHeader");

			return dsIShip;
		}


		public static DataSet GetTempInvoiceableShipmentsList(String strAppID, String strEnterpriseID,String userID, String strCustType, String strCustID, String strCustomerName, string sessionID)
		{ //GET THE LIST OF CUSTOMER
			//��Ҥ�ʷ��»� �ҡ��

			StringBuilder strBuilder1 = new StringBuilder();
			
			strBuilder1.Append("select consignment_no,ref_no,Sender_Name,sender_zipcode,act_pickup_datetime,");
			strBuilder1.Append("recipient_name,recipient_zipcode,act_delivery_date,service_code,tot_freight_charge,");
			strBuilder1.Append("insurance_surcharge,other_surch_amount,tot_vas_surcharge,esa_surcharge,last_exception_code,");
			strBuilder1.Append("pod_slip_required,applicationid,enterpriseid,userID,CHECKED,payerid ");
			strBuilder1.Append(" from invoicePreview_Details where userID='"+userID+"' and enterpriseid ='"+strEnterpriseID+"' and applicationid = '" +strAppID+ "' and SessionID = '"+sessionID+"'");

			if(strCustType=="C")
			{
				strBuilder1.Append(" and replace(invoicePreview_Details.sender_name,'''','') =N'");
				strBuilder1.Append(strCustomerName.Replace("'","").ToString());
				strBuilder1.Append("' ");
			}
			else
			{
				strBuilder1.Append(" and invoicePreview_Details.payerid ='");
				strBuilder1.Append(strCustID);
				strBuilder1.Append("' ");
			}
		

			DataSet dsIShip = null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("InvoiceGenerationMgrDAL","GetInvoiceableShipments","EDB101","DbConnection object is null!!");
				return dsIShip;
			}

			strBuilder1.Append(" order by payerID,Sender_Name ASC ");

			String strSQLQuery = strBuilder1.ToString();

			dsIShip = (DataSet)dbCon.ExecuteQuery(strSQLQuery,ReturnType.DataSetType);

			return dsIShip;
		}


		public static DataSet GetTempInvoiceableShipmentsCustomer(String strAppID, String strEnterpriseID,String userID, String strCustType, String strCustID, String strCustomerName, string sessionID)
		{ 
			StringBuilder strBuilder1 = new StringBuilder();
			strBuilder1.Append("select distinct Sender_Name from invoicePreview_Header where userID='"+userID+"' and enterpriseid ='"+strEnterpriseID+"' and applicationid = '" +strAppID+ "' and SessionID = '"+sessionID+"'");

			if(strCustType=="C")
			{
				strBuilder1.Append(" group by Sender_Name order by Sender_Name ASC ");
			}
			else
			{
				strBuilder1.Append(" group by payerID, Sender_Name order by Sender_Name ASC ");
			}

			DataSet dsIShip = null;
			
			IDbCommand dbCmd = null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("InvoiceGenerationMgrDAL","GetInvoiceableShipments","EDB101","DbConnection object is null!!");
				return dsIShip;
			}

			//String strSQLQuery = strBuilder1.ToString();
			//dsIShip = (DataSet)dbCon.ExecuteQuery(strSQLQuery,ReturnType.DataSetType);
			dbCmd = dbCon.CreateCommand(strBuilder1.ToString(), CommandType.Text);
			dbCmd.CommandTimeout = 600;  //Add Command TimeOut for case of serveral records: 600 seconds (10 minutes)
			dsIShip = (DataSet)dbCon.ExecuteQuery(dbCmd, ReturnType.DataSetType);

			return dsIShip;
		}


		public static bool UpdateTempSenderName(String strAppID, String strEnterpriseID,String userID, String strConsigNo, String strSenderName, string sessionID)
		{
			StringBuilder strUpdateBuilder = new StringBuilder();

			strUpdateBuilder.Append(" update invoicePreview_Details set invoicePreview_Details.sender_name='"+ strSenderName +"'");
			strUpdateBuilder.Append(" where invoicePreview_Details.consignment_no ='" + strConsigNo + "'");
			strUpdateBuilder.Append(" and invoicePreview_Details.enterpriseid ='"+strEnterpriseID+"'");
			strUpdateBuilder.Append(" and invoicePreview_Details.applicationid = '" +strAppID+ "'");
			strUpdateBuilder.Append(" and userID='"+userID+"' and SessionID = '"+sessionID+"' ");

			string strSQL = strUpdateBuilder.ToString();

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("InvoiceGenerationMgrDAL","GetInvoiceableShipments","EDB101","DbConnection object is null!!");
				return false;
			}

			int iUpdateOK;

			IDbCommand dbCmd = dbCon.CreateCommand(strSQL,CommandType.Text);


			iUpdateOK = dbCon.ExecuteNonQuery(dbCmd);

			if(iUpdateOK>0)
				return true;
			else
				return false;

		}


		public static void UpdateTempCheckedConsignment(String strAppID, String strEnterpriseID,String userID, String strConsigNo, bool bChecked, string sessionID)
		{
			string strYN=null;

			if(bChecked)
				strYN="Y";
			else
				strYN="N";

			StringBuilder strUpdateBuilder = new StringBuilder();
			strUpdateBuilder.Append(" update invoicePreview_Details with(rowlock) set invoicePreview_Details.CHECKED='"+ strYN +"'");
			strUpdateBuilder.Append(" where invoicePreview_Details.consignment_no ='" + strConsigNo + "'");
			strUpdateBuilder.Append(" and invoicePreview_Details.enterpriseid ='"+strEnterpriseID+"'");
			strUpdateBuilder.Append(" and invoicePreview_Details.applicationid = '" +strAppID+ "'");
			strUpdateBuilder.Append(" and userID='"+userID+"' and SessionID = '"+sessionID+"'");

			string strSQL = strUpdateBuilder.ToString();

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("InvoiceGenerationMgrDAL","GetInvoiceableShipments","EDB101","DbConnection object is null!!");
			}

			int iUpdateOK;

			IDbCommand dbCmd = dbCon.CreateCommand(strSQL,CommandType.Text);


			iUpdateOK = dbCon.ExecuteNonQuery(dbCmd);


		}


		//RETURN CUSTID/CUSTNAME/TYPE THAT CONTAINTS IN HADER WHICH HAS BEEN CHECKED
		public static DataSet GetTempListOfCHECKEDInvoiceHeader(string strAppID, string strEnterpriseID,string userID,string sessionID)
		{ 
			StringBuilder strBuilder1 = new StringBuilder();
			/*
			strBuilder1.Append(" SELECT DISTINCT InvoicePreview_Header.*, XXX.CHECKED, c.payer_type FROM ( select * from invoicePreview_details WHERE (InvoicePreview_Details.CHECKED = 'Y') ");
			strBuilder1.Append(" AND (InvoicePreview_Details.enterpriseid = '"+strEnterpriseID+"') ");
			strBuilder1.Append(" AND (InvoicePreview_Details.applicationid = '" +strAppID+ "'  ) ");
			//edit by Tumz
			strBuilder1.Append(" AND  (InvoicePreview_Details.userID = '"+userID+"') AND (InvoicePreview_Details.SessionID = '"+sessionID+"') ) AS XXX inner join Customer c on c.custid = XXX.payerid LEFT OUTER JOIN  InvoicePreview_Header ON XXX.applicationid = InvoicePreview_Header.applicationid AND XXX.enterpriseid = InvoicePreview_Header.enterpriseid AND XXX.userID = InvoicePreview_Header.userID AND XXX.SessionID = InvoicePreview_Header.SessionID AND ((InvoicePreview_Header.CPayment_mode='C' AND XXX.Sender_Name=InvoicePreview_Header.Sender_Name)OR (InvoicePreview_Header.CPayment_mode='R'AND XXX.Payerid=InvoicePreview_Header.PayerID)) ");
			*/
			strBuilder1.Append("SELECT TOP 1 * FROM InvoicePreview_Details WITH(NOLOCK) WHERE enterpriseid = '"+strEnterpriseID+"'");
			strBuilder1.Append(" AND applicationid = '" +strAppID+ "'");
			strBuilder1.Append(" AND userID = '" +userID+ "'");
			strBuilder1.Append(" AND SessionID = '" +sessionID+ "'");
			strBuilder1.Append(" AND Checked = 'Y'");

			DataSet dsIShip = null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("InvoiceGenerationMgrDAL","GetInvoiceableShipments","EDB101","DbConnection object is null!!");
				return dsIShip;
			}

			String strSQLQuery = strBuilder1.ToString();

			dsIShip = (DataSet)dbCon.ExecuteQuery(strSQLQuery,ReturnType.DataSetType);

			return dsIShip;
		}


		public static DataSet GetTempListOfCHECKEDConsignment(string strAppID, string strEnterpriseID,string userID, string strCustType, string strCustID, string strCustomerName, string sessionID)
		{ 

			StringBuilder strBuilder1 = new StringBuilder();


			strBuilder1.Append(" SELECT DISTINCT InvoicePreview_Details.consignment_no, InvoicePreview_Details.Sender_Name FROM InvoicePreview_Details WHERE ");
			strBuilder1.Append(" (InvoicePreview_Details.CHECKED = 'Y') AND (InvoicePreview_Details.enterpriseid = '"+strEnterpriseID+"') ");
			strBuilder1.Append(" AND (InvoicePreview_Details.applicationid = '"+strAppID+"') AND (InvoicePreview_Details.userID = '"+userID+"') AND (InvoicePreview_Details.SessionID = '"+sessionID+"') ");

			if(strCustType=="C")
				strBuilder1.Append(" AND (replace(InvoicePreview_Details.sender_name,'''','') = N'"+strCustomerName.Replace("'","").ToString()+"') ");
			else
				strBuilder1.Append(" AND (InvoicePreview_Details.payerID = '"+strCustID+"') ");


			//			strBuilder1.Append(" SELECT DISTINCT InvoicePreview_Details.consignment_no, InvoicePreview_Details.Sender_Name FROM InvoicePreview_Details RIGHT OUTER JOIN InvoicePreview_Header ON InvoicePreview_Details.applicationid = InvoicePreview_Header.applicationid AND InvoicePreview_Details.enterpriseid = InvoicePreview_Header.enterpriseid AND InvoicePreview_Details.userID = InvoicePreview_Header.userID WHERE (InvoicePreview_Details.CHECKED = 'Y')");
			//			strBuilder1.Append(" AND (InvoicePreview_Details.enterpriseid = 'kdth') AND (InvoicePreview_Details.applicationid = 'TIES') ");
			//			//strBuilder1.Append(" AND (InvoicePreview_Header.CPayment_mode = '"+strCustType+"') ");
			//			strBuilder1.Append(" AND (InvoicePreview_Details.userID = '"+userID+"') ");
			//			if(strCustType=="C")
			//
			//				//strBuilder1.Append(" AND (and replace(InvoicePreview_Header.sender_name,'''','') = N'"+strCustomerName.Replace("'","").ToString()+"') ");
			//			else
			//				strBuilder1.Append(" AND (InvoicePreview_Header.payerID = '"+strCustID+"') ");



			DataSet dsIShip = null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("InvoiceGenerationMgrDAL","GetInvoiceableShipments","EDB101","DbConnection object is null!!");
				return dsIShip;
			}

			String strSQLQuery = strBuilder1.ToString();

			dsIShip = (DataSet)dbCon.ExecuteQuery(strSQLQuery,ReturnType.DataSetType);

			return dsIShip;
		}

		public static void UndoInvoicedConsignment(String strAppID, String strEnterpriseID, String strConsigNo)
		{
			#region "UNDO STAMP DATETIME INTO CONSIGMENT"	//STAMP DATETIME INTO CONSIGMENT
			StringBuilder strUpdateBuilder;
			int iUpdateOK=0;
			IDbCommand dbCmd = null;

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("InvoiceGenerationMgrDAL","GetInvoiceableShipments","EDB101","DbConnection object is null!!");

			}

					strUpdateBuilder = new StringBuilder();

					strUpdateBuilder.Append(" update shipment set shipment.invoice_date=null");
					strUpdateBuilder.Append(" , invoice_no =null ");
					strUpdateBuilder.Append(" where shipment.consignment_no IN (" + strConsigNo + ")");
					strUpdateBuilder.Append(" and shipment.enterpriseid ='"+strEnterpriseID+"'");
					strUpdateBuilder.Append(" and shipment.applicationid = '" +strAppID+ "'");

					dbCmd = dbCon.CreateCommand(strUpdateBuilder.ToString(),CommandType.Text);
					
					//dbCmd.CommandText = strUpdateBuilder.ToString();	
					iUpdateOK = dbCon.ExecuteNonQuery(dbCmd);
					//dbCon.ExecuteNonQueryInTransaction(dbCmd);
	
					if(!(iUpdateOK>0))
						throw new Exception("760");
			//END OF UNDO STAMP DATETIME INTO CONSIGNMENT
#endregion

		}


		public static bool GenerateInvoice(Utility ut,String strAppID, String strEnterpriseID,DataRow drHeader, DataSet dsConsigNo,String strInvType,string strInvJobID)
		{
			Exception _ex = null;

			IDbCommand dbCmd = null;
			DbConnection dbCon = null;
			IDbConnection conApp = null;
			IDbTransaction tranApp = null;

			//Initial Connection     
			InitTran(ut, ref dbCmd, ref dbCon, ref conApp,ref tranApp);

			//GET INVOICE NUMBER
			string nn="000000";
			string strInvNo = Counter.GetNext(strAppID,strEnterpriseID,"invoice_number",dbCon,dbCmd).ToString();
			strInvNo = "IND" + DateTime.Today.Year.ToString().Remove(0,2) + nn.Substring(0,nn.Length-strInvNo.Length) + strInvNo; //String.Format("{0:000000}",strInvNo);
			//END GET INVOICE NUMBER
			
			//IDENTITY THE CURRENT BATCH JOB ID
			//string strInvJobID = Counter.GetNext(strAppID,strEnterpriseID,"InvoiceGenerationJobID").ToString();
			//END OF IDENTITY THE CURRENT BATCH JOB ID


			//			ArrayList n_cash_success = new ArrayList(), n_cash_error=new ArrayList();
			//			ArrayList n_credit_success = new ArrayList(), n_credit_error = new ArrayList();
			//			InvoiceJobMgrDAL.UpdateInvoiceJobStatus(strAppID, strEnterpriseID, Convert.ToDecimal(strInvJobID), DateTime.Now, "I","Job in Progress");

			try
			{
				dbCmd = dbCon.CreateCommand("", CommandType.Text);
				dbCmd.Connection = conApp;
				dbCmd.Transaction = tranApp;				

				StringBuilder strCountBuilder;
				int iUpdateOK=0;
				int iRows = dsConsigNo.Tables[0].Rows.Count;
				DataRow drEach; 

#region	"CHECK INVOICE DATE in CONSIGNMENT"		//CHECK INVOICE DATE in CONSIGNMENT
				DataSet dsCheck;
				for(int i=0;i<iRows;i++)
				{

					drEach = dsConsigNo.Tables[0].Rows[i];
					if(!drEach["consignment_no"].GetType().Equals(System.Type.GetType("System.DBNull")))
					{
						strCountBuilder = new StringBuilder();

						strCountBuilder.Append(" select count(*) from shipment where shipment.invoice_date is not null ");
						strCountBuilder.Append(" and shipment.consignment_no ='" + drEach["consignment_no"].ToString() + "'");
						strCountBuilder.Append(" and shipment.enterpriseid ='"+strEnterpriseID+"'");
						strCountBuilder.Append(" and shipment.applicationid = '" +strAppID+ "'");

						dbCmd.CommandText = strCountBuilder.ToString();
						dsCheck = (DataSet)dbCon.ExecuteQueryInTransaction(dbCmd,ReturnType.DataSetType);

						if(dsCheck.Tables[0].Rows.Count>0)
							if(Int32.Parse(dsCheck.Tables[0].Rows[0][0].ToString())>0)
								throw new Exception("742");
					}
				}


				iUpdateOK = 0;

#endregion			//END OF CHECK INVOICE DATE


#region "STAMP DATETIME INTO CONSIGMENT"	//STAMP DATETIME INTO CONSIGMENT
				StringBuilder strUpdateBuilder;
				DateTime dtInvDateTime = DateTime.Now;

				for(int i=0;i<iRows;i++)
				{
					drEach = dsConsigNo.Tables[0].Rows[i];
					if(!drEach["consignment_no"].GetType().Equals(System.Type.GetType("System.DBNull")))
					{
						strUpdateBuilder = new StringBuilder();

						strUpdateBuilder.Append(" update shipment set shipment.invoice_date='"+ Convert.ToDateTime(dtInvDateTime) +"'");
						strUpdateBuilder.Append(" , invoice_no = '"+ strInvNo +"'");
						strUpdateBuilder.Append(" where shipment.consignment_no ='" + drEach["consignment_no"].ToString() + "'");
						strUpdateBuilder.Append(" and shipment.enterpriseid ='"+strEnterpriseID+"'");
						strUpdateBuilder.Append(" and shipment.applicationid = '" +strAppID+ "'");

						//dbCmd = dbCon.CreateCommand(strUpdateBuilder.ToString(),CommandType.Text);
						dbCmd.CommandText = strUpdateBuilder.ToString();	
						iUpdateOK = dbCon.ExecuteNonQueryInTransaction(dbCmd);
	
						if(!(iUpdateOK>0))
							throw new Exception("773");
					}
				}
				//END OF STAMP DATETIME INTO CONSIGNMENT
#endregion



#region "INSERT INTO INVOICE_GENERATION_TASK"

				//				strUpdateBuilder = new StringBuilder();
				//
				//				strUpdateBuilder.Append(" INSERT INTO Invoice_Generation_Task(applicationid,enterpriseid,jobid,start_datetime,end_datetime,job_status,remark) ");
				//				strUpdateBuilder.Append(" VALUES('" + strAppID + "','" + strEnterpriseID + "'," + strInvJobID + ",'"+Convert.ToDateTime(dtInvDateTime)+"',null,'P','Job Awaiting Execution')");
				//
				//				//dbCmd = dbCon.CreateCommand(strUpdateBuilder.ToString(),CommandType.Text);
				//	
				//				dbCmd.CommandText = strUpdateBuilder.ToString();
				//				iUpdateOK = dbCon.ExecuteNonQueryInTransaction(dbCmd);
				//	
				//				if(!(iUpdateOK>0))
				//					throw new Exception("801");


#endregion
	
#region "INSERT INTO INVOICE" //INSERT INTO INVOICE 4.9.4 -- GET INFO FROM drHeader
				DateTime dtInvoiceDate;
				String strPaymentMode="";
				if(drHeader["CPayment_mode"]!=null&&(!drHeader["CPayment_mode"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					strPaymentMode = drHeader["CPayment_mode"].ToString();
				}
				StringBuilder strInsertBuilder = new StringBuilder();;

				if(strPaymentMode=="R")
				{
					strInsertBuilder.Append(" INSERT INTO Invoice WITH(Rowlock) (applicationid,enterpriseid,invoice_no,invoice_date,due_date,payerid,payer_type,payer_name,payer_address1,payer_address2,payer_zipcode,payer_country,payer_telephone,payer_fax,payment_mode,invoice_status,act_payment_date,printed_datetime,jobid,tot_pkg,tot_wt,tot_dim_wt,chargeable_wt,insurance_amt,other_surcharge,tot_freight_charge,tot_vas_surcharge,esa_surcharge,total_exception,mbg_amount,invoice_adj_amount,invoice_amt,tot_cons,approved_by,approved_date,cancelled_by,cancel_date,InvoiceCancelNo,CancelType,Reason,first_bill_placement_date,next_bill_placement_date,printed_due_date,internal_due_date,updated_by,updated_date) ");
				}
				else
				{
					strInsertBuilder.Append(" INSERT INTO Invoice WITH(Rowlock) (applicationid,enterpriseid,invoice_no,invoice_date,due_date,payerid,payer_type,payer_name,payer_address1,payer_address2,payer_zipcode,payer_country,payer_telephone,payer_fax,payment_mode,invoice_status,act_payment_date,printed_datetime,jobid,tot_pkg,tot_wt,tot_dim_wt,chargeable_wt,insurance_amt,other_surcharge,tot_freight_charge,tot_vas_surcharge,esa_surcharge,total_exception,mbg_amount,invoice_adj_amount,invoice_amt,tot_cons,approved_by,approved_date,cancelled_by,cancel_date,InvoiceCancelNo,CancelType,Reason,updated_by,updated_date) ");
				}
				strInsertBuilder.Append(" VALUES('TIES'");
				strInsertBuilder.Append(",'" + strEnterpriseID + "'");

				strInsertBuilder.Append(",'" + strInvNo + "'");
				strInsertBuilder.Append(",'" + Convert.ToDateTime(dtInvDateTime) + "'");

				string strPayerID="";

				if(!drHeader["payerid"].GetType().Equals(System.Type.GetType("System.DBNull")))
					strPayerID = Utility.ReplaceSingleQuote(drHeader["payerid"].ToString());
			
				string strDueDate = GetDueDateStr(strAppID,strEnterpriseID,strPayerID,strPaymentMode,ref dbCmd,ref dbCon);

				if(strDueDate!="")
				{
					DateTime dtInv = Convert.ToDateTime(dtInvDateTime).AddDays(double.Parse(strDueDate));
					dtInvoiceDate = dtInv;
					strInsertBuilder.Append(",'" + Convert.ToDateTime(dtInv) + "'");
				}
				else
				{
					dtInvoiceDate = dtInvDateTime;
					strInsertBuilder.Append(",'" + Convert.ToDateTime(dtInvDateTime) + "'");
				}

				if(drHeader["payerid"]!=null&&(!drHeader["payerid"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					strInsertBuilder.Append(",'" + drHeader["payerid"].ToString() + "'");
				else
					strInsertBuilder.Append(",''");

//				if(drHeader["CPayment_mode"]!=null&&(!drHeader["CPayment_mode"].GetType().Equals(System.Type.GetType("System.DBNull"))))
//					strInsertBuilder.Append(",'" + drHeader["CPayment_mode"].ToString() + "'");

				//edit by Tumz.
				if(drHeader["payer_type"]!=null&&(!drHeader["payer_type"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					strInsertBuilder.Append(",'" + drHeader["payer_type"].ToString() + "'");
				else
					strInsertBuilder.Append(",''");

				if(drHeader["sender_name"]!=null&&(!drHeader["sender_name"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					strInsertBuilder.Append(",N'" + Utility.ReplaceSingleQuote(drHeader["sender_name"].ToString()) + "'");
				else
					strInsertBuilder.Append(",''");

				if(drHeader["sender_address1"]!=null&&(!drHeader["sender_address1"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					strInsertBuilder.Append(",N'" + Utility.ReplaceSingleQuote(drHeader["sender_address1"].ToString()) + "'");
				else
					strInsertBuilder.Append(",''");

				if(drHeader["sender_address2"]!=null&&(!drHeader["sender_address2"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					strInsertBuilder.Append(",N'" + Utility.ReplaceSingleQuote(drHeader["sender_address2"].ToString()) + "'");
				else
					strInsertBuilder.Append(",''");

				if(drHeader["sender_zipcode"]!=null&&(!drHeader["sender_zipcode"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					strInsertBuilder.Append(",'" + drHeader["sender_zipcode"].ToString()+ "'");
					Zipcode zipCode = new Zipcode();
					zipCode.Populate(strAppID,strEnterpriseID,drHeader["sender_zipcode"].ToString());
					strInsertBuilder.Append(",'" + zipCode.Country + "'");
				}
				else
				{
					strInsertBuilder.Append(",''");
					strInsertBuilder.Append(",''");
				}

				if(drHeader["sender_telephone"]!=null&&(!drHeader["sender_telephone"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					strInsertBuilder.Append(",'" + drHeader["sender_telephone"].ToString() + "'");
				else
					strInsertBuilder.Append(",''");

				if(drHeader["sender_fax"]!=null&&(!drHeader["sender_fax"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					strInsertBuilder.Append(",'" + drHeader["sender_fax"].ToString() + "'");
				else
					strInsertBuilder.Append(",''");
           
				strInsertBuilder.Append(",'" + strInvType + "'");

				strInsertBuilder.Append(",'P',null,null");

				strInsertBuilder.Append("," + strInvJobID + "");
			
				strInsertBuilder.Append(",null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null");
				if(strPaymentMode=="R")
				{
					//first_bill_placement_date
					String strNextBillPlacement =  CustomerProfileDAL.GetNextBillPlacementDate(strAppID,strEnterpriseID,strPayerID,ref dbCmd,ref dbCon);;
					if(strNextBillPlacement!="")
					{
						strInsertBuilder.Append(",'" + Convert.ToDateTime(strNextBillPlacement) + "'");
					}
					else
					{
						strInsertBuilder.Append(",null");
					}
					//next_bill_placement_date
					if(strNextBillPlacement!="")
					{
						strInsertBuilder.Append(",'" + Convert.ToDateTime(strNextBillPlacement) + "'");
					}
					else
					{
						strInsertBuilder.Append(",null");
					}
					//printed_due_date
					if(strDueDate!="")
					{
						DateTime dtInv = Convert.ToDateTime(dtInvDateTime).AddDays(double.Parse(strDueDate));					
						strInsertBuilder.Append(",'" + Convert.ToDateTime(dtInv) + "'");
					}
					else
					{
						strInsertBuilder.Append(",'" + Convert.ToDateTime(dtInvoiceDate) + "'");
					}
					//internal_due_date
					if(strDueDate!=""&&strNextBillPlacement!="")
					{
						DateTime dtInv = Convert.ToDateTime(strNextBillPlacement).AddDays(double.Parse(strDueDate));					
						strInsertBuilder.Append(",'" + Convert.ToDateTime(dtInv) + "'");
					}
					else
					{
						strInsertBuilder.Append(",null");
					}
				}
				strInsertBuilder.Append(",'" + ut.GetUserID() + "'");
				strInsertBuilder.Append(",'" + DateTime.Now + "'");
				strInsertBuilder.Append(")");


				dbCmd.CommandText = strInsertBuilder.ToString();
				iUpdateOK = dbCon.ExecuteNonQueryInTransaction(dbCmd);

				if(!(iUpdateOK>0))
					throw new Exception("891");

		
#endregion //END INSERT INTO INVOICE 4.9.4

#region "INSERT INTO INVOICE_DETAIL" //"INSERT INTO INVOICE_DETAIL"

				for(int i=0;i<iRows;i++)
				{
					//get info from each consignment, then assigned them into invoice_detail
					drEach = dsConsigNo.Tables[0].Rows[i];
					if(!drEach["consignment_no"].GetType().Equals(System.Type.GetType("System.DBNull")))
					{
						if(!InsertInvoice_Detail(strAppID,strEnterpriseID,drEach["consignment_no"].ToString(),strInvNo,ref dbCmd,ref dbCon))
							throw new Exception("905");

					}
				}


			
				//ocustused.consignment_no = "";

	
#endregion //"INSERT INTO INVOICE_DETAIL"		

				//INVOICE BATCH
				//BatchInvoiceGenerationJob(ut,Decimal.Parse(strInvJobID), ref n_cash_success,ref n_cash_error, 
				//	ref n_credit_success, ref n_credit_error, ref dbCmd, ref dbCon);
			

				//CALL AutoConsignmentHistory
				//add return_pod_slip and return_invoice_hc in select statement 06/03/2009 ching
				string upShipment = "SELECT booking_no,consignment_no,return_pod_slip,return_invoice_hc FROM shipment WHERE invoice_no = '"+ strInvNo +"'";
				//IDbCommand dbCmd = dbCon.
				dbCmd.CommandText = upShipment;
				DataSet ds  = new DataSet();
				ds = (DataSet)dbCon.ExecuteQueryInTransaction(dbCmd,ReturnType.DataSetType);				

				foreach(DataRow dr in ds.Tables[0].Rows)
				{
					//check and add 2 status when generate HCR / INVR to shipment tracking 06/03/2009 ching
					Utility.CreateAutoConsignmentHistory(strAppID,strEnterpriseID,3,ut.GetUserID(),Convert.ToInt32(dr["booking_no"].ToString()),dr["consignment_no"].ToString(),System.DateTime.Now,ut.GetUserID(),ref dbCmd,ref dbCon);
					 
					//Added for PSA#2009-025
					//if(Convert.ToString(dr["return_pod_slip"]).Equals("Y"))
					//{
						//Utility.CreateAutoConsignmentHistory(strAppID,strEnterpriseID,27,ut.GetUserID(),Convert.ToInt32(dr["booking_no"].ToString()),dr["consignment_no"].ToString(),System.DateTime.Now.AddSeconds(3),ut.GetUserID(),ref dbCmd,ref dbCon);						
					//}
					//if(Convert.ToString(dr["return_invoice_hc"]).Equals("Y"))
					//{
						//Utility.CreateAutoConsignmentHistory(strAppID,strEnterpriseID,29,ut.GetUserID(),Convert.ToInt32(dr["booking_no"].ToString()),dr["consignment_no"].ToString(),System.DateTime.Now.AddSeconds(3),ut.GetUserID(),ref dbCmd,ref dbCon);
					//}
					//Added for PSA#2009-025
				}
			
			}
			catch(Exception ex)
			{
				_ex = ex;
			}
			finally
			{
				if(_ex!=null) //Error
				{
					tranApp.Rollback();
				}
				else//Success
				{
					tranApp.Commit();//tranApp.Rollback();

//					CustomerCreditused ocustused = new CustomerCreditused();
//					ocustused.cusid = drHeader["payerid"].ToString();
//					ocustused.applicationid = strAppID;
//					ocustused.enterpriseid = strEnterpriseID;
//					ocustused.amount = ocustused.getcreditUsedinvoice();
//					//	ocustused.updated_by = "";
//					ocustused.transcode = "INI";
//					ocustused.invoice_no = strInvNo;
//					ocustused.updateCreditused();

				}

				if(conApp!=null) conApp.Close();	
			}


			return (_ex==null);			

			#region Comment By Oak
			//			string jobsts="", remark="";
			//			jobsts=(_ex!=null)?"E":"C";
			//			
			//			//14. Update Job Statistics for Invoice Generation Task : Complete/Error
			//			//  Case Success
			//			//  if (cash ) remark = �xx Cash invoices processed� where xx is the number of invoices.
			//			//	else       remark = �Invoice for Customer: xxxx processed� where xxxx is the Credit Customer ID.
			//			//
			//			//  Case Error
			//			//  if (cash ) remark = �Error: xx processing Cash invoices (nn invoices processed successfully)� 
			//			//						where nn is the number of successfully processed invoices and xx is an error number to be defined later
			//			//	else       remark = �Error: yy processing Invoice for Customer: xxxx� 
			//			//						where xxxx is the Credit Customer ID and yy is an error number to be defined later
			//
			//			if(n_cash_error.Count>0)
			//			{
			//				remark+= "Error: "+n_cash_error.Count+" processing Cash invoices ("+n_cash_success.Count+" invoices processed successfully)";
			//			}
			//			else
			//			{
			//				if(n_cash_success.Count>0)
			//					remark+= " " + n_cash_success.Count+" Cash invoice(s) processed";
			//			}
			//	
			//			if(n_credit_error.Count>0)
			//			{
			//				string xxxx = "";
			//
			//				for(int i = 0 ; i<n_credit_error.Count;i++)
			//				{
			//					xxxx += n_credit_error[i];
			//					if(i<n_credit_error.Count-1) xxxx+=",";
			//				}
			//				remark+= (remark.Length>0?",":"") + " Error: "+n_credit_error.Count+" processing Invoice for Customer:"+xxxx;
			//			}
			//			else
			//			{
			//				if(n_credit_success.Count>0)
			//				{
			//					string xxxx = "";
			//					for(int i = 0 ; i<n_credit_success.Count;i++)
			//					{
			//						xxxx += n_credit_success[i];
			//						if(i<n_credit_success.Count-1) xxxx+=",";
			//					}
			//					remark+= (remark.Length>0?",":"") + "Invoice for Customer: "+xxxx+" processed";
			//				}
			//			}
			//			InvoiceJobMgrDAL.UpdateInvoiceJobStatus(strAppID, strEnterpriseID, Convert.ToDecimal(strInvJobID), DateTime.Now, jobsts, remark);
			#endregion

		}


		public static string GetInvNums(String strAppID, String strEnterpriseID,String strInvJobID)
		{

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("InvoiceGenerationMgrDAL","GetInvoiceableShipments","EDB101","DbConnection object is null!!");
			}

			DataSet dsInvNums;
			String strInvNums="";

			String strSQLQuery = "SELECT distinct invoice_no FROM Invoice WITH(NOLOCK) where applicationid='"+strAppID+"' and enterpriseid='"+strEnterpriseID+"' and jobid="+strInvJobID;

			dsInvNums = (DataSet)dbCon.ExecuteQuery(strSQLQuery,ReturnType.DataSetType);

			DataRow drInvNum;

			for(int i=0;i<=dsInvNums.Tables[0].Rows.Count-1;i++)
			{
				drInvNum = dsInvNums.Tables[0].Rows[i];
				if(((drInvNum["invoice_no"]!= null) && (!drInvNum["invoice_no"].GetType().Equals(System.Type.GetType("System.DBNull")))))
				{
					if(strInvNums.Length>0)
						strInvNums+=",";
					strInvNums = drInvNum["invoice_no"].ToString();
				}

			}

			return strInvNums;
		}


		public static bool DeleteTempInvoicePreview(string strAppID, string strEnterpriseID,string userID, DataSet dsInvoiceCriteria,string sessionID)
		{  //CLEAR ALL OLD DATA
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("InvoiceGenerationMgrDAL","GetInvoiceableShipments","EDB101","DbConnection object is null!!");
			}

			int OK=0;
			
			IDbCommand dbCmd = dbCon.CreateCommand(" DELETE FROM InvoicePreview_Header WITH(ROWLOCK) WHERE userID='"+userID+"' and enterpriseid ='"+strEnterpriseID+"' and applicationid = '" +strAppID+ "' and SessionID = '"+ sessionID +"' ",CommandType.Text);
			OK = dbCon.ExecuteNonQuery(dbCmd);

			dbCmd = dbCon.CreateCommand(" DELETE FROM InvoicePreview_Details WITH(ROWLOCK) WHERE userID='"+userID+"' and enterpriseid ='"+strEnterpriseID+"' and applicationid = '" +strAppID+ "' and SessionID = '"+sessionID+"'",CommandType.Text);
			OK = dbCon.ExecuteNonQuery(dbCmd);

			return true;
		}


		#region"OLD FROM REAL DB"

		public static bool checkHCReturned(String strAppID, String strEnterpriseID, String strConsigNo)
		{

			DataSet dsIShip = new DataSet();
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("InvoiceGenerationMgrDAL","GetInvoiceableShipments","EDB101","DbConnection object is null!!");
				//return dsIShip;
			}


			StringBuilder strBd = new StringBuilder();

			strBd.Append(" select count(*)  from shipment with(nolock) ");
			strBd.Append(" where (return_pod_slip='N' or return_pod_slip=null or return_pod_slip='') ");
			strBd.Append(" and enterpriseid ='"+strEnterpriseID+"'");
			strBd.Append(" and applicationid = '" +strAppID+ "'");
			strBd.Append(" and consignment_no = '"+ strConsigNo +"' ");

			dsIShip.Clear();

			dsIShip = (DataSet)dbCon.ExecuteQuery(strBd.ToString(),ReturnType.DataSetType);

			if(dsIShip.Tables[0].Rows.Count>0)
			{
				if(Int32.Parse(dsIShip.Tables[0].Rows[0][0].ToString())>0)
					return true;
			}

			//IF HC POD = Y in consignment, will searching for HCR in Shipment_Tracking

			StringBuilder strBuilder = new StringBuilder();

			strBuilder.Append(" SELECT count(*) FROM Shipment_Tracking WITH(NOLOCK) WHERE (status_code = 'HCRHQ') ");
			strBuilder.Append(" and enterpriseid ='"+strEnterpriseID+"'");
			strBuilder.Append(" and applicationid = '" +strAppID+ "'");
			strBuilder.Append(" and consignment_no = '"+ strConsigNo +"' ");

			String strSQLQuery = strBuilder.ToString();

			dsIShip.Clear();
			dsIShip = (DataSet)dbCon.ExecuteQuery(strSQLQuery,ReturnType.DataSetType);

			if(dsIShip.Tables[0].Rows.Count>0)
			{
				if(Int32.Parse(dsIShip.Tables[0].Rows[0][0].ToString())>0)
					return true;
				else
					return false;
			}
			else
				return false;
			//			StringBuilder strBuilder = new StringBuilder();
			//
			//			strBuilder.Append(" SELECT count(*) FROM Shipment_Tracking WHERE (status_code = 'HCR') ");
			//			strBuilder.Append(" and enterpriseid ='"+strEnterpriseID+"'");
			//			strBuilder.Append(" and applicationid = '" +strAppID+ "'");
			//			strBuilder.Append(" and consignment_no = '"+ strConsigNo +"' ");
			//
			//			DataSet dsIShip = null;
			//			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			//			if(dbCon == null)
			//			{
			//				Logger.LogTraceError("InvoiceGenerationMgrDAL","GetInvoiceableShipments","EDB101","DbConnection object is null!!");
			//				//return dsIShip;
			//			}
			//
			//			String strSQLQuery = strBuilder.ToString();
			//
			//			dsIShip = (DataSet)dbCon.ExecuteQuery(strSQLQuery,ReturnType.DataSetType);
			//
			//			if(dsIShip.Tables[0].Rows.Count>0)
			//			{
			//				if(Int32.Parse(dsIShip.Tables[0].Rows[0][0].ToString())>0)
			//					return true;
			//				else
			//					return false;
			//			}
			//			else
			//				return false;

		}


		public static SessionDS GetInvoiceableShipmentsHeader(String strAppID, String strEnterpriseID,int iCurrent,int iDSRecSize, DataSet dsInvoiceCriteria)
		{ //GET THE LIST OF CUSTOMER
			DataRow dr = dsInvoiceCriteria.Tables[0].Rows[0];

			string strPayment_mode = (string)dr["Payment_mode"]; //C or R in shipment.payment_mode

			StringBuilder strSelectBuilder1 = new StringBuilder();

			StringBuilder strSelectBuilder2 = new StringBuilder();
			StringBuilder strSelectBuilder3 = new StringBuilder();

			StringBuilder strFromBuilder1 = new StringBuilder();

			StringBuilder strFromBuilder2 = new StringBuilder();
			StringBuilder strFromBuilder3 = new StringBuilder();

			StringBuilder strWhereBuilder1 = new StringBuilder();
			StringBuilder strWhereBuilder2 = new StringBuilder();
			StringBuilder strWhereBuilder3 = new StringBuilder();


			//for CREDIT AND OTHER
			if(strPayment_mode=="R")
				strSelectBuilder1.Append("select shipment.payerid,shipment.payer_name as sender_name,shipment.payer_address1 as sender_address1,shipment.payer_address2 as sender_address2,shipment.payer_zipcode AS sender_zipcode,shipment.payer_telephone  AS sender_telephone,shipment.payer_fax  AS sender_fax, customer.pod_slip_required, 'R' as CPayment_Mode ");
			//for CONSIGNMENT GROUP CASH
			if(strPayment_mode=="C")
			{
				//CASE OF CASH & CREDIT CUST
				if((bool)dr["Cash_Cust"])
				{	//strSelectBuilder2.Append(" select distinct CASE customer.payment_mode WHEN 'C' THEN customer.custid ELSE shipment.payerid END AS payerID,CASE customer.payment_mode WHEN 'C' THEN shipment.sender_name ELSE shipment.payer_name END AS sender_name, CASE customer.payment_mode WHEN 'C' THEN shipment.sender_address1 ELSE shipment.payer_address1 END AS sender_address1, CASE customer.payment_mode WHEN 'C' THEN shipment.sender_address2 ELSE shipment.payer_address2 END AS sender_address2, CASE customer.payment_mode WHEN 'C' THEN shipment.sender_zipcode ELSE shipment.payer_zipcode END AS sender_zipcode, CASE customer.payment_mode WHEN 'C' THEN shipment.sender_telephone ELSE shipment.payer_telephone END AS sender_telephone, CASE customer.payment_mode WHEN 'C' THEN shipment.sender_fax ELSE shipment.payer_fax END AS sender_fax, 'NO' as pod_slip_required, customer.payment_mode as CPayment_mode  ");
					strSelectBuilder2.Append(" select min(payerID) as payerID, sender_name as Sender_Name,min(sender_address1) AS sender_address1,min(sender_address2) AS sender_address2, min(sender_zipcode) AS sender_zipcode,min(sender_telephone) AS sender_telephone, min(sender_fax) AS sender_fax,min(pod_slip_required)  AS pod_slip_required, CPayment_mode from ( select distinct CASE customer.payment_mode WHEN 'C' THEN customer.custid ELSE shipment.payerid END AS payerID, CASE customer.payment_mode WHEN 'C' THEN shipment.sender_name ELSE shipment.payer_name END AS sender_name, CASE customer.payment_mode WHEN 'C' THEN min(shipment.sender_address1) ELSE min(shipment.payer_address1) END AS sender_address1, CASE customer.payment_mode WHEN 'C' THEN min(shipment.sender_address2) ELSE min(shipment.payer_address2) END AS sender_address2, CASE customer.payment_mode WHEN 'C' THEN min(shipment.sender_zipcode) ELSE min(shipment.payer_zipcode) END AS sender_zipcode, CASE customer.payment_mode WHEN 'C' THEN min(shipment.sender_telephone) ELSE min(shipment.payer_telephone) END AS sender_telephone, CASE customer.payment_mode WHEN 'C' THEN min(shipment.sender_fax) ELSE min(shipment.payer_fax) END AS sender_fax, 'NO' as pod_slip_required, customer.payment_mode as CPayment_mode ");
				}

				if((bool)dr["Credit_Cust"])
				{
					strSelectBuilder3.Append(" select payerID,min(sender_name) as sender_name,min(sender_address1) as sender_address1,min(sender_address2) as sender_address2, min(sender_zipcode) as sender_zipcode,min(sender_telephone) as sender_telephone, min(sender_fax) as sender_fax,min(pod_slip_required) as pod_slip_required, CPayment_mode from ( ");
					//strSelectBuilder3.Append(" select payerID,min(sender_name),min(sender_address1),min(sender_address2), min(sender_zipcode),min(sender_telephone), min(sender_fax),min(pod_slip_required) ,CPayment_mode from ( ");
					strSelectBuilder3.Append(" select distinct CASE customer.payment_mode WHEN 'C' THEN customer.custid ELSE shipment.payerid END AS payerID, CASE customer.payment_mode WHEN 'C' THEN shipment.sender_name ELSE shipment.payer_name END AS sender_name, CASE customer.payment_mode WHEN 'C' THEN min(shipment.sender_address1) ELSE min(shipment.payer_address1) END AS sender_address1, CASE customer.payment_mode WHEN 'C' THEN min(shipment.sender_address2) ELSE min(shipment.payer_address2) END AS sender_address2, CASE customer.payment_mode WHEN 'C' THEN min(shipment.sender_zipcode) ELSE min(shipment.payer_zipcode) END AS sender_zipcode, CASE customer.payment_mode WHEN 'C' THEN min(shipment.sender_telephone) ELSE min(shipment.payer_telephone) END AS sender_telephone, CASE customer.payment_mode WHEN 'C' THEN min(shipment.sender_fax) ELSE min(shipment.payer_fax) END AS sender_fax, 'NO' as pod_slip_required, customer.payment_mode as CPayment_mode ");
				}
			}
			strFromBuilder1.Append(" FROM  Shipment_Tracking RIGHT OUTER JOIN  Shipment ON Shipment_Tracking.applicationid = Shipment.applicationid AND Shipment_Tracking.enterpriseid = Shipment.enterpriseid AND "); 
			strFromBuilder1.Append(" Shipment_Tracking.booking_no = Shipment.booking_no AND Shipment_Tracking.consignment_no = Shipment.consignment_no LEFT OUTER JOIN ");
			strFromBuilder1.Append(" Customer ON Shipment.applicationid = Customer.applicationid AND Shipment.enterpriseid = Customer.enterpriseid AND Shipment.payerid = Customer.custid where ");
			strFromBuilder1.Append(" shipment.enterpriseid ='"+strEnterpriseID+"'");
			strFromBuilder1.Append(" and shipment.applicationid = '" +strAppID+ "'");
			strFromBuilder1.Append(" and shipment.invoice_date IS NULL ");
			//			strFromBuilder1.Append(" and ((Shipment.est_delivery_datetime >= shipment_tracking.tracking_datetime) or (Shipment.est_delivery_datetime < shipment_tracking.tracking_datetime and isnull(shipment_tracking.status_code, '') <> '')) ");
			//ADD ABOVE BY X MAR 06 08	//strFromBuilder1.Append(" and Shipment_Tracking.status_code IN ('POD','PODEX') and shipment_tracking.tracking_datetime <= shipment.est_delivery_datetime ");
			//ADD BY X MAR 10 08
			strFromBuilder1.Append(" AND ((Shipment.est_delivery_datetime >= shipment_tracking.tracking_datetime  and Shipment_Tracking.status_code ='POD' ) or (Shipment.est_delivery_datetime < shipment_tracking.tracking_datetime and shipment_tracking.status_code ='PODEX' and isnull(shipment_tracking.status_code, '') <> ''))  ");
			strFromBuilder1.Append("  AND (Shipment_Tracking.deleted IS NULL or Shipment_Tracking.deleted <> 'Y') ");
			

			if ((dr["Last_Pickup_Date"]!=null) && (!dr["Last_Pickup_Date"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				DateTime Last_Pickup_Date = (DateTime) dr["Last_Pickup_Date"];
				string strLast_Pickup_Date = Utility.DateFormat(strAppID, strEnterpriseID, Last_Pickup_Date, DTFormat.DateTime);
				strFromBuilder1.Append(" and act_pickup_datetime <= "+strLast_Pickup_Date+"");
				strFromBuilder2.Append(" and act_pickup_datetime <= "+strLast_Pickup_Date+"");
				strFromBuilder3.Append(" and act_pickup_datetime <= "+strLast_Pickup_Date+"");
			}
			else //ADD BY X MAR 06 08
			{
				DateTime Last_Pickup_Date = DateTime.ParseExact(DateTime.Now.ToString("dd/MM/yyyy") +" 23:59:59","dd/MM/yyyy HH:mm:ss",null);
				string strLast_Pickup_Date = Utility.DateFormat(strAppID, strEnterpriseID, Last_Pickup_Date, DTFormat.DateTime);
				strFromBuilder1.Append(" and act_pickup_datetime <= "+strLast_Pickup_Date+"");
				strFromBuilder2.Append(" and act_pickup_datetime <= "+strLast_Pickup_Date+"");
				strFromBuilder3.Append(" and act_pickup_datetime <= "+strLast_Pickup_Date+"");
			}

			if(strPayment_mode!=null || strPayment_mode !="")
			{
				strFromBuilder1.Append(" and shipment.payment_mode ='");
				strFromBuilder1.Append(strPayment_mode);
				strFromBuilder1.Append("' ");

				if(strPayment_mode=="C")
				{
					if((bool)dr["Cash_Cust"])
						strFromBuilder2.Append(" and customer.payment_mode IN ('C') ");

					if((bool)dr["Credit_Cust"])
						strFromBuilder3.Append(" and customer.payment_mode IN ('R') ");
				}
				else
				{
					string strCustomerCode = (string)dr["Customer_Code"];
					if(strCustomerCode!=null || strCustomerCode !="")
					{
						strFromBuilder1.Append(" and shipment.payerid ='");
						strFromBuilder1.Append(strCustomerCode);
						strFromBuilder1.Append("' ");
					}
				}

			}

			SessionDS dsIShip = null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("InvoiceGenerationMgrDAL","GetInvoiceableShipments","EDB101","DbConnection object is null!!");
				return dsIShip;
			}

			//for CREDIT AND OTHER
			if(strPayment_mode=="R")
			{
				strWhereBuilder1.Append(" and shipment.consignment_no = (select top 1 shipment.consignment_no " + strFromBuilder1.ToString() + " )");
				strWhereBuilder1.Append(" GROUP BY shipment.payerid,shipment.payer_name,shipment.payer_address1,shipment.payer_address2,shipment.payer_zipcode,shipment.payer_telephone,shipment.payer_fax, customer.pod_slip_required, customer.payment_mode, customer.custid  ");
				strWhereBuilder1.Append(" Order by CUSTOMER.PAYMENT_MODE, shipment.payerid ASC  ");
			}
			//for CONSIGNMENT GROUP CASH
			if(strPayment_mode=="C")
			{

				if((bool)dr["Cash_Cust"])
				{
					strWhereBuilder2.Append(" GROUP BY customer.custid ,payerID,shipment.sender_name ,shipment.payer_name , shipment.payment_mode, customer.payment_mode ");
					strWhereBuilder2.Append(" ) as YYY group by Sender_Name, CPayment_Mode ");
				}

				if((bool)dr["Credit_Cust"])
				{
					strWhereBuilder3.Append(" GROUP BY customer.custid ,payerID,shipment.sender_name ,shipment.payer_name , shipment.payment_mode, customer.payment_mode ");
					strWhereBuilder3.Append(" ) as XXX Group By  payerID, CPayment_mode ");
				}
			}


			string strSQL1=null,strSQL2=null,strSQLQuery=null; // = strSelectBuilder.ToString() + strFromBuilder.ToString() + strWhereBuilder.ToString();

			//IF CONSIGNMENT GROUP CASH && IT HAS CASH CUSTOMER ALSO , NEEDS UNION
			if(strPayment_mode=="C")
			{

				if((bool)dr["Cash_Cust"])
				{
					strSQL1 = strSelectBuilder2.ToString() + strFromBuilder1.ToString() + strFromBuilder2.ToString() + strWhereBuilder2.ToString();
				}

				if((bool)dr["Credit_Cust"])
				{
					strSQL2 = strSelectBuilder3.ToString() + strFromBuilder1.ToString() + strFromBuilder3.ToString() + strWhereBuilder3.ToString();
				}

				if((bool)dr["Cash_Cust"]&&(bool)dr["Credit_Cust"]) //FOR CASH INV & BOTH CASH&CREDIT CUST, but line below is for SECOND UNION IS CASH
				{
					strSQLQuery = strSQL1.ToString() + " UNION " +strSQL2.ToString() + " order by CPayment_mode, Sender_Name";
				}
				
				if(strSQLQuery==null)
				{
					if(strSQL1!=null)
						strSQLQuery = strSQL1.ToString()+ " order by sender_name";
					else
						strSQLQuery = strSQL2.ToString() + " order by payerID" ;
				}
			}
			else
				strSQLQuery = strSelectBuilder1.ToString() + strFromBuilder1.ToString() + strWhereBuilder1.ToString();


			dsIShip = dbCon.ExecuteQuery(strSQLQuery,iCurrent,iDSRecSize,"SHIPMENT");

			return dsIShip;
		}


		public static DataSet GetInvoiceableShipmentsCustomer(String strAppID, String strEnterpriseID, DataSet dsInvoiceCriteria, String strCustID, String strCustomerName)
		{ //GET THE LIST OF CUSTOMER
			DataRow dr = dsInvoiceCriteria.Tables[0].Rows[0];

			string strPayment_mode = (string)dr["Payment_mode"]; //C or R in shipment.payment_mode

			StringBuilder strSelectBuilder = new StringBuilder();
			StringBuilder strFromBuilder = new StringBuilder();
			StringBuilder strWhereBuilder = new StringBuilder();

			//for CREDIT AND OTHER
			if(strPayment_mode=="R")
				strSelectBuilder.Append(" select distinct shipment.sender_name  ");
			//for CONSIGNMENT GROUP CASH
			if(strPayment_mode=="C")
				strSelectBuilder.Append(" select distinct CASE customer.payment_mode WHEN 'C' THEN shipment.sender_name ELSE shipment.payer_name END AS sender_name ");

			strFromBuilder.Append(" FROM  Shipment_Tracking RIGHT OUTER JOIN  Shipment ON Shipment_Tracking.applicationid = Shipment.applicationid AND Shipment_Tracking.enterpriseid = Shipment.enterpriseid AND "); 
			strFromBuilder.Append(" Shipment_Tracking.booking_no = Shipment.booking_no AND Shipment_Tracking.consignment_no = Shipment.consignment_no LEFT OUTER JOIN ");
			strFromBuilder.Append(" Customer ON Shipment.applicationid = Customer.applicationid AND Shipment.enterpriseid = Customer.enterpriseid AND Shipment.payerid = Customer.custid where ");
			strFromBuilder.Append(" shipment.enterpriseid ='"+strEnterpriseID+"'");
			strFromBuilder.Append(" and shipment.applicationid = '" +strAppID+ "'");
			strFromBuilder.Append(" and shipment.invoice_date IS NULL ");
			//EDITED BY X MAR 06 08	
			//ADD BY X MAR 10 08
			strFromBuilder.Append(" AND ((Shipment.est_delivery_datetime >= shipment_tracking.tracking_datetime  and Shipment_Tracking.status_code ='POD' ) or (Shipment.est_delivery_datetime < shipment_tracking.tracking_datetime and shipment_tracking.status_code ='PODEX' and isnull(shipment_tracking.status_code, '') <> ''))  ");

			//strFromBuilder.Append(" and ((Shipment.est_delivery_datetime >= shipment_tracking.tracking_datetime) or (Shipment.est_delivery_datetime < shipment_tracking.tracking_datetime and isnull(shipment_tracking.status_code, '') <> '')) ");
			strFromBuilder.Append("  AND (Shipment_Tracking.deleted IS NULL or Shipment_Tracking.deleted <> 'Y') ");
			//strFromBuilder.Append(" and Shipment_Tracking.status_code IN ('POD','PODEX') and shipment_tracking.tracking_datetime <= shipment.est_delivery_datetime AND (Shipment_Tracking.deleted IS NULL or Shipment_Tracking.deleted <> 'Y') ");
			

			if ((dr["Last_Pickup_Date"]!=null) && (!dr["Last_Pickup_Date"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				DateTime Last_Pickup_Date = (DateTime) dr["Last_Pickup_Date"];
				string strLast_Pickup_Date = Utility.DateFormat(strAppID, strEnterpriseID, Last_Pickup_Date, DTFormat.DateTime);
				strFromBuilder.Append(" and act_pickup_datetime <= "+strLast_Pickup_Date+"");
			}
			else
			{
				DateTime Last_Pickup_Date = DateTime.ParseExact(DateTime.Now.ToString("dd/MM/yyyy") +" 23:59:59","dd/MM/yyyy HH:mm:ss",null);
				string strLast_Pickup_Date = Utility.DateFormat(strAppID, strEnterpriseID, Last_Pickup_Date, DTFormat.DateTime);
				strFromBuilder.Append(" and act_pickup_datetime <= "+strLast_Pickup_Date+"");
			}

			if(strPayment_mode!=null || strPayment_mode !="")
			{
				strFromBuilder.Append(" and shipment.payment_mode ='");
				strFromBuilder.Append(strPayment_mode);
				strFromBuilder.Append("' ");

				if(strPayment_mode=="C" && strCustID.Trim().Equals("N/A".Trim()))
				{
					if((bool)dr["Cash_Cust"]&&!(bool)dr["Credit_Cust"])
						strFromBuilder.Append(" and customer.payment_mode IN ('C') ");

					if(!(bool)dr["Cash_Cust"]&&(bool)dr["Credit_Cust"])
						strFromBuilder.Append(" and customer.payment_mode IN ('R') ");

					if(!(bool)dr["Cash_Cust"]&&!(bool)dr["Credit_Cust"])
						strFromBuilder.Append(" and customer.payment_mode IN ('C') ");

					if((bool)dr["Cash_Cust"]&&(bool)dr["Credit_Cust"])
						strFromBuilder.Append(" and customer.payment_mode IN ('C','R') ");

					//					strFromBuilder.Append(" and shipment.sender_name =N'");
					//					strFromBuilder.Append(strCustomerName);
					//					strFromBuilder.Append("' ");
				}
				else
				{
					//string strCustomerCode = (string)dr["Customer_Code"];
					//if(strCustomerCode!=null || strCustomerCode !="")
					//{
					strFromBuilder.Append(" and shipment.payerid ='");
					strFromBuilder.Append(strCustID);
					strFromBuilder.Append("' ");
					//}
				}

			}

			DataSet dsIShip = null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("InvoiceGenerationMgrDAL","GetInvoiceableShipments","EDB101","DbConnection object is null!!");
				return dsIShip;
			}

			//for CREDIT AND OTHER
			if(strPayment_mode=="R")
			{
				strWhereBuilder.Append(" and shipment.consignment_no IN (select shipment.consignment_no " + strFromBuilder.ToString() + " )");
				strWhereBuilder.Append(" and shipment.payerid ='" + strCustID + "'");
				strWhereBuilder.Append(" Group by Shipment.Sender_name  "); //, shipment.Consignment_no
			}
			//for CONSIGNMENT GROUP CASH
			if(strPayment_mode=="C")
			{
				//strWhereBuilder.Append(" and shipment.consignment_no IN (select shipment.consignment_no " + strFromBuilder.ToString() + " )");
				//strWhereBuilder.Append(" and shipment.payerid ='" + strCustID + "'");
				//strWhereBuilder.Append(" GROUP BY shipment.payer_name ");
			}

			String strSQLQuery = strSelectBuilder.ToString() + strFromBuilder.ToString() + strWhereBuilder.ToString();

			dsIShip = (DataSet)dbCon.ExecuteQuery(strSQLQuery,ReturnType.DataSetType);

			return dsIShip;
		}


		public static DataSet GetInvoiceableShipmentsList(String strAppID, String strEnterpriseID, DataSet dsInvoiceCriteria, String strCustID, String strCustomerName)
		{ //GET THE LIST OF CUSTOMER
			DataRow dr = dsInvoiceCriteria.Tables[0].Rows[0];
			//��ͧ�� LIST OF CONSIGNMENT NO. �Ҵ֧仨��
			string strPayment_mode = (string)dr["Payment_mode"]; //C or R in shipment.payment_mode

			StringBuilder strSelectBuilder = new StringBuilder();
			StringBuilder strFromBuilder = new StringBuilder();
			StringBuilder strWhereBuilder = new StringBuilder();

			//for CREDIT AND OTHER
			if(strPayment_mode=="R")
				strSelectBuilder.Append(" select distinct shipment.payer_name as sender_name ,shipment.payerid, shipment.consignment_no,shipment.ref_no,shipment.sender_zipcode, shipment.act_pickup_datetime,shipment.recipient_name, shipment.recipient_zipcode, shipment.act_delivery_date, shipment.service_code, shipment.tot_freight_charge, shipment.insurance_surcharge, shipment.other_surch_amount, shipment.tot_vas_surcharge, shipment.esa_surcharge, shipment.last_exception_code  ");
			//for CONSIGNMENT GROUP CASH
			if(strPayment_mode=="C")
				strSelectBuilder.Append(" select distinct shipment.sender_name ,shipment.payerid, shipment.consignment_no,shipment.ref_no,shipment.sender_zipcode, shipment.act_pickup_datetime,shipment.recipient_name, shipment.recipient_zipcode, shipment.act_delivery_date, shipment.service_code, shipment.tot_freight_charge, shipment.insurance_surcharge, shipment.other_surch_amount, shipment.tot_vas_surcharge, shipment.esa_surcharge, shipment.last_exception_code  , 'NO' as pod_slip_required  ");
			//strSelectBuilder.Append(" select 'N / A' as payerID,shipment.sender_name,shipment.sender_address1,shipment.sender_address2,shipment.sender_zipcode,shipment.sender_telephone,shipment.sender_fax, 'NO' as pod_slip_required ");

			strFromBuilder.Append(" FROM  Shipment_Tracking RIGHT OUTER JOIN  Shipment ON Shipment_Tracking.applicationid = Shipment.applicationid AND Shipment_Tracking.enterpriseid = Shipment.enterpriseid AND "); 
			strFromBuilder.Append(" Shipment_Tracking.booking_no = Shipment.booking_no AND Shipment_Tracking.consignment_no = Shipment.consignment_no LEFT OUTER JOIN ");
			strFromBuilder.Append(" Customer ON Shipment.applicationid = Customer.applicationid AND Shipment.enterpriseid = Customer.enterpriseid AND Shipment.payerid = Customer.custid where ");
			strFromBuilder.Append(" shipment.enterpriseid ='"+strEnterpriseID+"'");
			strFromBuilder.Append(" and shipment.applicationid = '" +strAppID+ "'");
			strFromBuilder.Append(" and shipment.invoice_date IS NULL ");
			//EDITED BY X MAR 06 08	
			//ADD BY X MAR 10 08
			strFromBuilder.Append(" AND ((Shipment.est_delivery_datetime >= shipment_tracking.tracking_datetime  and Shipment_Tracking.status_code ='POD' ) or (Shipment.est_delivery_datetime < shipment_tracking.tracking_datetime and shipment_tracking.status_code ='PODEX' and isnull(shipment_tracking.status_code, '') <> ''))  ");

			//strFromBuilder.Append(" and ((Shipment.est_delivery_datetime >= shipment_tracking.tracking_datetime) or (Shipment.est_delivery_datetime < shipment_tracking.tracking_datetime and isnull(shipment_tracking.status_code, '') <> '')) ");
			strFromBuilder.Append("  AND (Shipment_Tracking.deleted IS NULL or Shipment_Tracking.deleted <> 'Y') ");
			//strFromBuilder.Append(" and Shipment_Tracking.status_code IN ('POD','PODEX') and shipment_tracking.tracking_datetime <= shipment.est_delivery_datetime AND (Shipment_Tracking.deleted IS NULL or Shipment_Tracking.deleted <> 'Y') ");
			

			if ((dr["Last_Pickup_Date"]!=null) && (!dr["Last_Pickup_Date"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				DateTime Last_Pickup_Date = (DateTime) dr["Last_Pickup_Date"];
				string strLast_Pickup_Date = Utility.DateFormat(strAppID, strEnterpriseID, Last_Pickup_Date, DTFormat.DateTime);
				strFromBuilder.Append(" and act_pickup_datetime <= "+strLast_Pickup_Date+"");
			}
			else
			{
				DateTime Last_Pickup_Date = DateTime.ParseExact(DateTime.Now.ToString("dd/MM/yyyy") +" 23:59:59","dd/MM/yyyy HH:mm:ss",null);
				string strLast_Pickup_Date = Utility.DateFormat(strAppID, strEnterpriseID, Last_Pickup_Date, DTFormat.DateTime);
				strFromBuilder.Append(" and act_pickup_datetime <= "+strLast_Pickup_Date+"");
			}


			if(strPayment_mode!=null || strPayment_mode !="")
			{
				strFromBuilder.Append(" and shipment.payment_mode ='");
				strFromBuilder.Append(strPayment_mode);
				strFromBuilder.Append("' ");

				if(strPayment_mode=="C" && strCustID.Trim().Equals("N/A".Trim()))
				{
					if((bool)dr["Cash_Cust"]&&!(bool)dr["Credit_Cust"])
						strFromBuilder.Append(" and customer.payment_mode IN ('C') ");

					if(!(bool)dr["Cash_Cust"]&&(bool)dr["Credit_Cust"])
						strFromBuilder.Append(" and customer.payment_mode IN ('R') ");

					if(!(bool)dr["Cash_Cust"]&&!(bool)dr["Credit_Cust"])
						strFromBuilder.Append(" and customer.payment_mode IN ('C') ");

					if((bool)dr["Cash_Cust"]&&(bool)dr["Credit_Cust"])
						strFromBuilder.Append(" and customer.payment_mode IN ('C','R') ");
					
					strFromBuilder.Append(" and replace(shipment.sender_name,'''','') =N'");
					strFromBuilder.Append(strCustomerName.Replace("'","").ToString());
					strFromBuilder.Append("' ");

				}
				else
				{
					//string strCustomerCode = (string)dr["Customer_Code"];
					//					if(strCustomerCode!=null || strCustomerCode !="")
					//					{
					strFromBuilder.Append(" and shipment.payerid ='");
					strFromBuilder.Append(strCustID);
					strFromBuilder.Append("' ");
					//					}
				}

			}

			DataSet dsIShip = null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("InvoiceGenerationMgrDAL","GetInvoiceableShipments","EDB101","DbConnection object is null!!");
				return dsIShip;
			}

			//for CREDIT AND OTHER
			if(strPayment_mode=="R")
			{
				strWhereBuilder.Append(" and shipment.consignment_no IN (select shipment.consignment_no " + strFromBuilder.ToString() + " )");
				strWhereBuilder.Append(" and shipment.payerid ='" + strCustID + "'");
			}
			//for CONSIGNMENT GROUP CASH
			if(strPayment_mode=="C")
			{
				//strWhereBuilder.Append(" and shipment.consignment_no IN (select shipment.consignment_no " + strFromBuilder.ToString() + " )");
				strWhereBuilder.Append(" Group BY shipment.consignment_no,shipment.ref_no,shipment.sender_zipcode, shipment.act_pickup_datetime,shipment.recipient_name, shipment.recipient_zipcode, shipment.act_delivery_date, shipment.service_code, shipment.tot_freight_charge, shipment.insurance_surcharge, shipment.other_surch_amount, shipment.tot_vas_surcharge, shipment.esa_surcharge, shipment.last_exception_code, shipment.sender_name ,shipment.payerid");
				//strWhereBuilder.Append(" GROUP BY customer.custid ,payerID,shipment.sender_name,shipment.sender_address1,shipment.sender_address2,shipment.sender_zipcode,shipment.sender_telephone,shipment.sender_fax, pod_slip_required ");
			}

			strWhereBuilder.Append(" Order by shipment.consignment_no ASC  ");

			String strSQLQuery = strSelectBuilder.ToString() + strFromBuilder.ToString() + strWhereBuilder.ToString();

			dsIShip = (DataSet)dbCon.ExecuteQuery(strSQLQuery,ReturnType.DataSetType);

			return dsIShip;
		}

		public static bool UpdateSenderName(String strAppID, String strEnterpriseID, String strConsigNo, String strSenderName)
		{
			StringBuilder strUpdateBuilder = new StringBuilder();

			strUpdateBuilder.Append(" update shipment set shipment.sender_name=N'"+ strSenderName +"'");
			strUpdateBuilder.Append(" where shipment.consignment_no ='" + strConsigNo + "'");
			strUpdateBuilder.Append(" and shipment.enterpriseid ='"+strEnterpriseID+"'");
			strUpdateBuilder.Append(" and shipment.applicationid = '" +strAppID+ "'");

			string strSQL = strUpdateBuilder.ToString();

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("InvoiceGenerationMgrDAL","GetInvoiceableShipments","EDB101","DbConnection object is null!!");
				return false;
			}

			int iUpdateOK;

			IDbCommand dbCmd = dbCon.CreateCommand(strSQL,CommandType.Text);


			iUpdateOK = dbCon.ExecuteNonQuery(dbCmd);

			if(iUpdateOK>0)
				return true;
			else
				return false;

		}


		public static string GetDueDateStr(String strAppID, String strEnterpriseID,String strPayerID,String strPaymentMode, ref IDbCommand dbCmd, ref DbConnection dbCon)
		{
			StringBuilder strSelectBuilder = new StringBuilder();

			strSelectBuilder.Append(" SELECT top 1 Customer.credit_term FROM Shipment LEFT OUTER JOIN Customer ON Shipment.payerid = Customer.custid AND Shipment.applicationid = Customer.applicationid AND Shipment.enterpriseid = Customer.enterpriseid");
			strSelectBuilder.Append(" where shipment.payerid='"+ strPayerID +"'");
			//strSelectBuilder.Append(" where shipment.consignment_no ='" + strConsigNo + "'");
			strSelectBuilder.Append(" and shipment.enterpriseid ='"+strEnterpriseID+"'");
			strSelectBuilder.Append(" and shipment.applicationid = '" +strAppID+ "'");
			strSelectBuilder.Append(" and Customer.payment_mode = '"+strPaymentMode+"'");

			string strSQL = strSelectBuilder.ToString();

			//dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("InvoiceGenerationMgrDAL","GetInvoiceableShipments","EDB101","DbConnection object is null!!");
			}

			dbCmd.CommandText = strSQL;
		
			DataSet dsIShip = (DataSet)dbCon.ExecuteQueryInTransaction(dbCmd,ReturnType.DataSetType);

			if(dsIShip.Tables[0].Rows.Count>0)
			{
				if(dsIShip.Tables[0].Rows[0][0].ToString().Equals("0"))
					return "";
				else
					return dsIShip.Tables[0].Rows[0][0].ToString();
			}
			else
				return "";

		}

		


		public static bool InsertInvoice_Detail(String strAppID, String strEnterpriseID,String strConsigNo, String strInvNo, ref IDbCommand dbCmd, ref DbConnection dbCon)
		{


			double tot_freight_charge=0.00,insurance_surcharge=0.00,other_surch_amount=0.00, 
				tot_vas_surcharge=0.00,esa_surcharge=0.00,total_exception=0.00,
				invoice_adj_amount=0.00,mbg_amount=0.00;

			StringBuilder strSelectBuilder = new StringBuilder();

			strSelectBuilder.Append(" SELECT top 1 shipment.* FROM Shipment ");
			//strSelectBuilder.Append(" select shipment set shipment.payerid='"+ strPayerID +"'");
			strSelectBuilder.Append(" where shipment.consignment_no ='" + strConsigNo + "'");
			strSelectBuilder.Append(" and shipment.enterpriseid ='"+strEnterpriseID+"'");
			strSelectBuilder.Append(" and shipment.applicationid = '" +strAppID+ "'");

			string strSQL = strSelectBuilder.ToString();

			dbCmd.CommandText = strSelectBuilder.ToString();
			DataSet dsIShip = (DataSet)dbCon.ExecuteQueryInTransaction(dbCmd,ReturnType.DataSetType);

			if(dsIShip.Tables[0].Rows.Count>0)
			{				
				DataRow drEach = dsIShip.Tables[0].Rows[0];
				//GET AND SET INTO INVOICE_DETAIL TABLE

				StringBuilder strInsertBuilder = new StringBuilder();;
				//TU on 23June08
				strInsertBuilder.Append(" INSERT INTO Invoice_Detail (applicationid,enterpriseid,invoice_no,consignment_no,booking_datetime,ref_no,origin_state_code,destination_state_code,tot_pkg,tot_wt,tot_act_wt,tot_dim_wt,chargeable_wt,pod_datetime,recipient_name,recipient_address1,recipient_address2,recipient_zipcode,recipient_country,sender_name,sender_address1,sender_address2,sender_country,sender_zipcode,mbg,service_code,pod_exception,person_incharge,pod_remark,insurance_amt,tot_freight_charge,tot_vas_surcharge,esa_surcharge,invoice_amt,consignee_name,other_surcharge,total_exception,mbg_amount,invoice_adj_amount,adjusted_by,adjusted_date,adjusted_remark) ");
				strInsertBuilder.Append(" VALUES('TIES'");
				strInsertBuilder.Append(",'" + strEnterpriseID + "'");
				strInsertBuilder.Append(",'" + strInvNo + "'");
				strInsertBuilder.Append(",'" + strConsigNo + "'");

				if(drEach["booking_datetime"]!=null&&(!drEach["booking_datetime"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					strInsertBuilder.Append(",'" + Convert.ToDateTime(drEach["booking_datetime"].ToString()) + "'");
				else
					strInsertBuilder.Append(",null");

				if(drEach["ref_no"]!=null&&(!drEach["ref_no"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					strInsertBuilder.Append(",'" + drEach["ref_no"].ToString() + "'");
				else
					strInsertBuilder.Append(",null");

				if(drEach["origin_state_code"]!=null&&(!drEach["origin_state_code"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					strInsertBuilder.Append(",'" + drEach["origin_state_code"].ToString() + "'");
				else
					strInsertBuilder.Append(",null");

				if(drEach["destination_state_code"]!=null&&(!drEach["destination_state_code"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					strInsertBuilder.Append(",'" + drEach["destination_state_code"].ToString() + "'");
				else
					strInsertBuilder.Append(",null");

				if(drEach["tot_pkg"]!=null&&(!drEach["tot_pkg"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					strInsertBuilder.Append("," + drEach["tot_pkg"].ToString() + "");
				else
					strInsertBuilder.Append(",null");

				if(drEach["tot_wt"]!=null&&(!drEach["tot_wt"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					strInsertBuilder.Append("," + drEach["tot_wt"].ToString() + "");
				else
					strInsertBuilder.Append(",null");

				//TU on 23June08
				if(drEach["tot_act_wt"]!=null&&(!drEach["tot_act_wt"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					strInsertBuilder.Append("," + drEach["tot_act_wt"].ToString() + "");
				else
					strInsertBuilder.Append(",null");
				//


				if(drEach["tot_dim_wt"]!=null&&(!drEach["tot_dim_wt"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					strInsertBuilder.Append("," + drEach["tot_dim_wt"].ToString() + "");
				else
					strInsertBuilder.Append(",null");

				if(drEach["chargeable_wt"]!=null&&(!drEach["chargeable_wt"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					strInsertBuilder.Append("," + drEach["chargeable_wt"].ToString() + "");
				else
					strInsertBuilder.Append(",null");

				if(drEach["act_delivery_date"]!=null&&(!drEach["act_delivery_date"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					strInsertBuilder.Append(",'" + Convert.ToDateTime(drEach["act_delivery_date"].ToString()) + "'");
				else
					strInsertBuilder.Append(",null");

				if(drEach["recipient_name"]!=null&&(!drEach["recipient_name"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					strInsertBuilder.Append(",N'" + Utility.ReplaceSingleQuote(drEach["recipient_name"].ToString()) + "'");
				else
					strInsertBuilder.Append(",null");

				if(drEach["recipient_address1"]!=null&&(!drEach["recipient_address1"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					strInsertBuilder.Append(",N'" + Utility.ReplaceSingleQuote(drEach["recipient_address1"].ToString()) + "'");
				else
					strInsertBuilder.Append(",null");

				if(drEach["recipient_address2"]!=null&&(!drEach["recipient_address2"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					strInsertBuilder.Append(",N'" + Utility.ReplaceSingleQuote(drEach["recipient_address2"].ToString()) + "'");
				else
					strInsertBuilder.Append(",null");

				if(drEach["recipient_zipcode"]!=null&&(!drEach["recipient_zipcode"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					strInsertBuilder.Append(",'" + drEach["recipient_zipcode"].ToString() + "'");
				else
					strInsertBuilder.Append(",null");

				if(drEach["recipient_country"]!=null&&(!drEach["recipient_country"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					strInsertBuilder.Append(",'" + drEach["recipient_country"].ToString() + "'");
				else
					strInsertBuilder.Append(",null");

				//************ MAR 12 08 ��ͧ��� SENDER NAME ��������Ѵ���ç��� �¹�Ҩҡ˹���Թ���ਹ��� by X * �ѧ�����
				if(drEach["sender_name"]!=null&&(!drEach["sender_name"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					strInsertBuilder.Append(",N'" + Utility.ReplaceSingleQuote(drEach["sender_name"].ToString()) + "'");
				else
					strInsertBuilder.Append(",null");

				if(drEach["sender_address1"]!=null&&(!drEach["sender_address1"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					strInsertBuilder.Append(",N'" + Utility.ReplaceSingleQuote(drEach["sender_address1"].ToString()) + "'");
				else
					strInsertBuilder.Append(",null");

				if(drEach["sender_address2"]!=null&&(!drEach["sender_address2"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					strInsertBuilder.Append(",N'" + Utility.ReplaceSingleQuote(drEach["sender_address2"].ToString()) + "'");
				else
					strInsertBuilder.Append(",null");

				if(drEach["sender_country"]!=null&&(!drEach["sender_country"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					strInsertBuilder.Append(",'" + drEach["sender_country"].ToString() + "'");
				else
					strInsertBuilder.Append(",null");

				if(drEach["sender_zipcode"]!=null&&(!drEach["sender_zipcode"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					strInsertBuilder.Append(",'" + drEach["sender_zipcode"].ToString() + "'");
				else
					strInsertBuilder.Append(",null");


				strInsertBuilder.Append(" ,'N' ");

				if(drEach["service_code"]!=null&&(!drEach["service_code"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					strInsertBuilder.Append(",'" + drEach["service_code"].ToString() + "'");
				else
					strInsertBuilder.Append(",null");


				strInsertBuilder.Append(",null,null,null");

				if(drEach["insurance_surcharge"]!=null&&(!drEach["insurance_surcharge"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					strInsertBuilder.Append("," + drEach["insurance_surcharge"].ToString() + "");
					insurance_surcharge = double.Parse(drEach["insurance_surcharge"].ToString());
				}
				else
					strInsertBuilder.Append(",null");

				if(drEach["tot_freight_charge"]!=null&&(!drEach["tot_freight_charge"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					strInsertBuilder.Append("," + drEach["tot_freight_charge"].ToString() + "");
					tot_freight_charge = double.Parse(drEach["tot_freight_charge"].ToString());
				}
				else
				{
					strInsertBuilder.Append(",null");
				}

				if(drEach["tot_vas_surcharge"]!=null&&(!drEach["tot_vas_surcharge"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					strInsertBuilder.Append("," + drEach["tot_vas_surcharge"].ToString() + "");
					tot_vas_surcharge = double.Parse(drEach["tot_vas_surcharge"].ToString());
				}
				else
					strInsertBuilder.Append(",null");

				if(drEach["esa_surcharge"]!=null&&(!drEach["esa_surcharge"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					strInsertBuilder.Append("," + drEach["esa_surcharge"].ToString() + "");
					esa_surcharge =  double.Parse(drEach["esa_surcharge"].ToString());
				}
				else
					strInsertBuilder.Append(",null");

				//for INVOICE_AMT Calculation only
				if(drEach["other_surch_amount"]!=null&&(!drEach["other_surch_amount"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					other_surch_amount =  double.Parse(drEach["other_surch_amount"].ToString());

				if(drEach["total_excp_charges"]!=null&&(!drEach["total_excp_charges"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					total_exception =  double.Parse(drEach["total_excp_charges"].ToString());

				if(drEach["invoice_adjustment"]!=null&&(!drEach["invoice_adjustment"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					invoice_adj_amount =  double.Parse(drEach["invoice_adjustment"].ToString());

				if(drEach["mbg_amount"]!=null&&(!drEach["mbg_amount"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					mbg_amount =  double.Parse(drEach["mbg_amount"].ToString());

				//end of for INVOICE_AMT
				double invoice_amt = tot_freight_charge + insurance_surcharge + other_surch_amount + tot_vas_surcharge + esa_surcharge + total_exception + invoice_adj_amount - mbg_amount;

				strInsertBuilder.Append("," + invoice_amt + "");

				strInsertBuilder.Append(",null");

				if(drEach["other_surch_amount"]!=null&&(!drEach["other_surch_amount"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					strInsertBuilder.Append("," + drEach["other_surch_amount"].ToString() + "");
				else
					strInsertBuilder.Append(",null");


				strInsertBuilder.Append(",0,0,0,null,null,null)");

				//dbCmd = dbCon.CreateCommand(strInsertBuilder.ToString(),CommandType.Text);
	
				dbCmd.CommandText = strInsertBuilder.ToString();
				int iUpdateOK = dbCon.ExecuteNonQueryInTransaction(dbCmd);
	
				if(!(iUpdateOK>0))
					return false;
			}
			else
				return false;
			return true;
		}

		
		public static bool Update_ForForceMBG(Utility util,String strInvNo,String strCusID, ref IDbCommand dbCmd, ref DbConnection dbCon)
		{
			
			string strSQL = "";
			strSQL ="  SELECT * ";
			strSQL+=" FROM Invoice ";
			strSQL+=" WHERE (applicationid = '"+util.GetAppID()+"') and (enterpriseid = '"+util.GetEnterpriseID()+"') and (invoice_no = '"+strInvNo+"') ";
			dbCmd.CommandText = strSQL;
			DataSet dsInv = (DataSet)dbCon.ExecuteQueryInTransaction(dbCmd,ReturnType.DataSetType);
			if(dsInv.Tables[0].Rows.Count>0)
			{	
				if(dsInv.Tables[0].Rows[0]["invoice_status"]!=DBNull.Value)
				{
					if(!dsInv.Tables[0].Rows[0]["invoice_status"].ToString().Equals("N"))
					{
						return false;
					}
				}
				else
				{
					return false;
				}
			}
			else
			{
				return false;
			}

			strSQL = "";
			strSQL ="  SELECT * ";
			strSQL+=" FROM Customer ";
			strSQL+=" WHERE (Customer.applicationid = '"+util.GetAppID()+"') and (Customer.enterpriseid = '"+util.GetEnterpriseID()+"') and (Customer.custid = '"+strCusID+"') ";
			dbCmd.CommandText = strSQL;
			DataSet dsCus = (DataSet)dbCon.ExecuteQueryInTransaction(dbCmd,ReturnType.DataSetType);
			if(dsCus.Tables[0].Rows.Count>0)
			{				
				if(dsCus.Tables[0].Rows[0]["MBG"]!=DBNull.Value)
				{
					if (dsCus.Tables[0].Rows[0]["MBG"].ToString().Equals("Y"))
					{
						return false;
					}
					else
					{
						strSQL ="update S set S.mbg_amount= S.forcembg_amount,S.invoice_amt=S.tot_freight_charge + S.insurance_surcharge + S.other_surch_amount + S.tot_vas_surcharge + S.esa_surcharge + S.total_excp_charges + S.invoice_adjustment + S.forcembg_amount ";
						strSQL+=" from Shipment S inner join Invoice_Detail IND ";
						strSQL+=" on S.applicationid=IND.applicationid ";
						strSQL+=" and S.enterpriseid=IND.enterpriseid ";
						strSQL+=" and S.consignment_no=IND.consignment_no ";
						strSQL+=" WHERE (s.applicationid = '"+util.GetAppID()+"') and (s.enterpriseid = '"+util.GetEnterpriseID()+"') ";
						strSQL+=" and (IND.invoice_no='"+ strInvNo +"') and (S.forcembg_amount is not null) and (S.forcembg_amount <> 0) ";
						strSQL+=" and (s.last_exception_code like 'PODEX%')";
						dbCmd.CommandText = strSQL;
						dbCon.ExecuteNonQueryInTransaction(dbCmd);
						
						strSQL =" Update IND";
						strSQL+=" Set IND.mbg_amount=qCons.forcembg_amount,IND.invoice_amt=IND.tot_freight_charge + IND.insurance_amt + IND.other_surcharge + IND.tot_vas_surcharge + IND.esa_surcharge + IND.total_exception + IND.invoice_adj_amount + qCons.forcembg_amount ";
						strSQL+=" FROM Invoice_Detail IND Inner join ";
						strSQL+=" ( ";
						strSQL+=" SELECT S.enterpriseid,S.applicationid,S.consignment_no,S.mbg_amount,S.forcembg_amount ";
						strSQL+=" FROM Shipment S inner join Invoice_Detail IND1 ";
						strSQL+=" ON S.applicationid=IND1.applicationid ";
						strSQL+=" and S.enterpriseid=IND1.enterpriseid ";
						strSQL+=" and S.consignment_no=IND1.consignment_no ";
						strSQL+=" WHERE IND1.invoice_no='"+ strInvNo +"' and  IND1.applicationid = '"+util.GetAppID()+"' and IND1.enterpriseid = '"+util.GetEnterpriseID()+"' ";
						strSQL+=" and S.last_exception_code like 'PODEX%' and S.forcembg_amount is not null and S.forcembg_amount <> 0 ";
						strSQL+=" ) qCons on qCons.consignment_no=IND.consignment_no ";
						strSQL+=" and qCons.applicationid=IND.applicationid";
						strSQL+=" and qCons.enterpriseid=IND.enterpriseid";
						dbCmd.CommandText = strSQL;
						dbCon.ExecuteNonQueryInTransaction(dbCmd);





						strSQL=""; 
						strSQL+=" UPDATE inv ";
						strSQL+=" SET inv.mbg_amount = ind.mbg_amount, inv.invoice_amt= ind.invoice_amt ";
						strSQL+=" FROM ( SELECT applicationid,enterpriseid,invoice_no,SUM(invoice_amt)AS invoice_amt, Case WHEN SUM(mbg_amount) is null then 0.00 else sum(mbg_amount) end AS mbg_amount ";
						strSQL+=" FROM Invoice_Detail ";
						strSQL+=" WHERE (invoice_no = '"+ strInvNo +"' and applicationid='"+ util.GetAppID() +"' and enterpriseid='"+util.GetEnterpriseID()+"' and  mbg_amount is not null) ";
						strSQL+=" group by invoice_no,applicationid,enterpriseid) ind ";
						strSQL+=" Inner join Invoice inv on inv.invoice_no=ind.invoice_no ";
						strSQL+=" and inv.enterpriseid=ind.enterpriseid";
						strSQL+=" and inv.applicationid=ind.applicationid";
						
						dbCmd.CommandText = strSQL;
						dbCon.ExecuteNonQueryInTransaction(dbCmd);   

						strSQL	=	""; 
						strSQL+="Update		Invoice_Summary ";
						strSQL+="Set		Invoice_Summary.invoice_amt = calSum.invoice_amt ";
						strSQL+="From		Invoice_Summary ";
						strSQL+="Inner join	( ";
						strSQL+="				SELECT  applicationid,	enterpriseid,	invoice_no, ";    
						strSQL+="				service_code,	'SHP' as summary_code_type, ";
						strSQL+="				SUM(invoice_amt) AS invoice_amt ";
						strSQL+="				FROM	Invoice_Detail   with(nolock) ";     
						strSQL+="				WHERE	applicationid		=	'"+ util.GetAppID() +"' "; 
						strSQL+="				AND		enterpriseid		=	'"+util.GetEnterpriseID()+"' "; 
						strSQL+="				AND		invoice_no			=	'"+ strInvNo +"' ";         
						strSQL+="				GROUP BY applicationid,	enterpriseid,	invoice_no, service_code) as calSum ";
						strSQL+="ON(Invoice_Summary.applicationid		=	calSum.applicationid ";  
						strSQL+="AND Invoice_Summary.enterpriseid		=	calSum.enterpriseid "; 
						strSQL+="AND Invoice_Summary.invoice_no			=	calSum.invoice_no ";
						strSQL+="AND Invoice_Summary.summary_code_type	=	calSum.summary_code_type "; 
						strSQL+="AND Invoice_Summary.summary_code		=	calSum.service_code) ";

						dbCmd.CommandText = strSQL;
						dbCon.ExecuteNonQueryInTransaction(dbCmd); 

						return true;
					}
				}
				else
				{
					strSQL ="update S set S.mbg_amount= S.forcembg_amount,S.invoice_amt=S.tot_freight_charge + S.insurance_surcharge + S.other_surch_amount + S.tot_vas_surcharge + S.esa_surcharge + S.total_excp_charges + S.invoice_adjustment + S.forcembg_amount ";
					strSQL+=" from Shipment S inner join Invoice_Detail IND ";
					strSQL+=" on S.applicationid=IND.applicationid ";
					strSQL+=" and S.enterpriseid=IND.enterpriseid ";
					strSQL+=" and S.consignment_no=IND.consignment_no ";
					strSQL+=" WHERE (s.applicationid = '"+util.GetAppID()+"') and (s.enterpriseid = '"+util.GetEnterpriseID()+"') ";
					strSQL+=" and (IND.invoice_no='"+ strInvNo +"') and (S.forcembg_amount is not null) and (S.forcembg_amount <> 0) ";
					strSQL+=" and (s.last_exception_code like 'PODEX%')";
					dbCmd.CommandText = strSQL;
					dbCon.ExecuteNonQueryInTransaction(dbCmd);
						
					strSQL =" Update IND";
					strSQL+=" Set IND.mbg_amount=qCons.forcembg_amount,IND.invoice_amt=IND.tot_freight_charge + IND.insurance_amt + IND.other_surcharge + IND.tot_vas_surcharge + IND.esa_surcharge + IND.total_exception + IND.invoice_adj_amount + qCons.forcembg_amount ";
					strSQL+=" FROM Invoice_Detail IND Inner join ";
					strSQL+=" ( ";
					strSQL+=" SELECT S.enterpriseid,S.applicationid,S.consignment_no,S.mbg_amount,S.forcembg_amount ";
					strSQL+=" FROM Shipment S inner join Invoice_Detail IND1 ";
					strSQL+=" ON S.applicationid=IND1.applicationid ";
					strSQL+=" and S.enterpriseid=IND1.enterpriseid ";
					strSQL+=" and S.consignment_no=IND1.consignment_no ";
					strSQL+=" WHERE IND1.invoice_no='"+ strInvNo +"' and  IND1.applicationid = '"+util.GetAppID()+"' and IND1.enterpriseid = '"+util.GetEnterpriseID()+"' ";
					strSQL+=" and S.last_exception_code like 'PODEX%' and S.forcembg_amount is not null and S.forcembg_amount <> 0 ";
					strSQL+=" ) qCons on qCons.consignment_no=IND.consignment_no ";
					strSQL+=" and qCons.applicationid=IND.applicationid";
					strSQL+=" and qCons.enterpriseid=IND.enterpriseid";
					dbCmd.CommandText = strSQL;
					dbCon.ExecuteNonQueryInTransaction(dbCmd);

					strSQL=""; 
					strSQL+=" UPDATE inv ";
					strSQL+=" SET inv.mbg_amount = ind.mbg_amount, inv.invoice_amt= ind.invoice_amt ";
					strSQL+=" FROM ( SELECT applicationid,enterpriseid,invoice_no,SUM(invoice_amt)AS invoice_amt, Case WHEN SUM(mbg_amount) is null then 0.00 else sum(mbg_amount) end AS mbg_amount ";
					strSQL+=" FROM Invoice_Detail ";
					strSQL+=" WHERE (invoice_no = '"+ strInvNo +"' and applicationid='"+ util.GetAppID() +"' and enterpriseid='"+util.GetEnterpriseID()+"' and  mbg_amount is not null) ";
					strSQL+=" group by invoice_no,applicationid,enterpriseid) ind ";
					strSQL+=" Inner join Invoice inv on inv.invoice_no=ind.invoice_no ";
					strSQL+=" and inv.enterpriseid=ind.enterpriseid";
					strSQL+=" and inv.applicationid=ind.applicationid";
						
					dbCmd.CommandText = strSQL;
					dbCon.ExecuteNonQueryInTransaction(dbCmd);   

					strSQL	=	""; 
					strSQL+="Update		Invoice_Summary ";
					strSQL+="Set		Invoice_Summary.invoice_amt = calSum.invoice_amt ";
					strSQL+="From		Invoice_Summary ";
					strSQL+="Inner join	( ";
					strSQL+="				SELECT  applicationid,	enterpriseid,	invoice_no, ";    
					strSQL+="				service_code,	'SHP' as summary_code_type, ";
					strSQL+="				SUM(invoice_amt) AS invoice_amt ";
					strSQL+="				FROM	Invoice_Detail   with(nolock) ";     
					strSQL+="				WHERE	applicationid		=	'"+ util.GetAppID() +"' "; 
					strSQL+="				AND		enterpriseid		=	'"+util.GetEnterpriseID()+"' "; 
					strSQL+="				AND		invoice_no			=	'"+ strInvNo +"' ";         
					strSQL+="				GROUP BY applicationid,	enterpriseid,	invoice_no, service_code) as calSum ";
					strSQL+="ON(Invoice_Summary.applicationid		=	calSum.applicationid ";  
					strSQL+="AND Invoice_Summary.enterpriseid		=	calSum.enterpriseid "; 
					strSQL+="AND Invoice_Summary.invoice_no			=	calSum.invoice_no ";
					strSQL+="AND Invoice_Summary.summary_code_type	=	calSum.summary_code_type "; 
					strSQL+="AND Invoice_Summary.summary_code		=	calSum.service_code) ";

					dbCmd.CommandText = strSQL;
					dbCon.ExecuteNonQueryInTransaction(dbCmd); 

					return true;
				}
			}
			else
			{
				return false;
			}
		}
		
		#endregion

#endregion


		#region Batch Invoice Generation Job
		public static string BatchInvoiceGenerationJob(Utility util, decimal iJobID, ref ArrayList n_cash_success, ref ArrayList n_cash_error, 
			ref ArrayList n_credit_success, ref ArrayList n_credit_error)//, ref IDbCommand dbCmd, ref DbConnection dbCon)
		{

			//Exception _ex = null;
			IDbCommand dbCmd = null;
			DbConnection dbCon = null;
			IDbConnection conApp = null;
			IDbTransaction tranApp = null;

			DataTable TbInvHead = null;

			try
			{
				//Initial Connection        
				InitTran(util, ref dbCmd, ref dbCon, ref conApp,ref tranApp);

				Exception _ex = null;
				
				//3. Identify the invoices ...
				TbInvHead = GetInvoice(util, iJobID.ToString(),ref dbCmd,ref dbCon);

				//Begin Loop for each Invoice
				foreach(DataRow rInv in TbInvHead.Rows)
				{
					_ex = null;

					//C or R ?
					string  invoice_no = rInv["invoice_no"].ToString(),
						payer_type = rInv["payer_type"].ToString().ToUpper(), 
						payment_mode = rInv["payment_mode"].ToString().ToUpper(),
						payerid=rInv["payerid"].ToString();
					bool IsCash = payment_mode.Equals("C"); //payer_type.Equals("C");

					try
					{
						//InitTran(util, ref dbCmd, ref dbCon, ref conApp, ref tranApp);
						DataTable TbConsignment = GetInvoiceDetail(util, invoice_no,ref  dbCmd, ref dbCon);

						// Begin Loop for each Consignment
						foreach(DataRow rC in TbConsignment.Rows)
						{
							//C or R? : I think that it's come from Invoice.payer_type, no come from Shipment.payment_mode
							//IsCash = rC["payment_mode"].ToString().ToUpper().Equals("C");

							//4.9.5.6 Initialize PODEX and MBG amounts to zero
							decimal MBG_Amount=0, PODEX_Amount=0;
							string consignment_no = rC["consignment_no"].ToString();
							string ConPayerid = rC["payerid"].ToString();
							DataRow rowFailure =  GetShipmentFailure(util, consignment_no, ConPayerid,ref dbCmd, ref dbCon).Rows[0];						

							//7 if (Is Act Del Date > Est Del Date)
							if((bool)rowFailure["IsShipmentFailure"])
							{
								bool isCustMBG_Yes=rowFailure["CustomerMBG"].ToString().ToUpper().Equals("Y"),
									isLastPODEX_MBG_Yes=rowFailure["LastPODEX_MBG"].ToString().ToUpper().Equals("Y");
							
								//if (is Customer entitled to MBG) 
								if(isCustMBG_Yes && isLastPODEX_MBG_Yes) 
								{
									//Have to send MBG to customer
									MBG_Amount = Convert.ToDecimal(rowFailure["MBG_Amount"])/-1;
									PODEX_Amount = Convert.ToDecimal(rowFailure["PODEX_Amount"]);
								}
								else
								{
									//No need to send MBG to customer
									DataTable tbPODEX = GetPODEX_listedInCustExCharge(util, consignment_no, ConPayerid, ref dbCmd,ref  dbCon);
									foreach(DataRow rPODEX in tbPODEX.Rows)
									{
										PODEX_Amount += rPODEX["surcharge"]==DBNull.Value? 0 : Convert.ToDecimal(rPODEX["surcharge"]);
										int r_eff_inv_sts_ex = InsertInvoice_Status_Exception(util, invoice_no, consignment_no, rPODEX,ref  dbCmd,ref  dbCon);
									}								
								}
							}

							//8. Update [Invoice Detail] record for consigment
							DataTable tbSpTrk= GetShpTrkForUpdateInvDetail(util, consignment_no,ref  dbCmd,ref  dbCon);
							if(tbSpTrk.Rows.Count>0)
							{
								DataRow rSpTrk=tbSpTrk.Rows[0];
								//bool hasPOD = Convert.ToBoolean(rSpTrk["hasPOD"]);
								//bool hasPODEX = Convert.ToBoolean(rSpTrk["hasPODEX"]);
								int r_eff_inv_d = UpdateInvoice_Detail(util, invoice_no, consignment_no, MBG_Amount, PODEX_Amount, rSpTrk,ref dbCmd,ref dbCon);
							}

							//9. Update the [Shipment] record
							int r_eff_shp = UpdateShipment(util, invoice_no, rC,ref  dbCmd,ref  dbCon);


							//Next consignment
						}//End Consignment Loop					

						//11. Update the Invoice Header record (Invoice table)
						int r_eff_inv = UpdateInvoice(util, invoice_no,ref  dbCmd,ref  dbCon);

						//12. Insert the Invoice Summary record(s) : edit from Update in spec.
						InsertInvoice_Sumary(util, invoice_no,ref  dbCmd,ref  dbCon);

						/* TU Update Force MBG 20/1/2011*/
						Update_ForForceMBG(util,invoice_no,payerid,ref dbCmd,ref dbCon);

					}
					catch(ApplicationException appEx)
					{
						_ex = appEx;
					}
					catch(Exception ex)
					{
						_ex = ex;
					}


					// Error Checking
					// 1.Exception happen? true ==> rollback
					// 2.Count cash or credit
					if(_ex!=null) //Error
					{
						if(IsCash) n_cash_error.Add("n_cash_error");
						else if(!n_credit_error.Contains(payerid)) n_credit_error.Add(payerid);//n_credit_error++;
					}
					else
					{
						if(IsCash) n_cash_success.Add("n_cash_success");
						else if(!n_credit_success.Contains(payerid)) n_credit_success.Add(payerid);//n_credit_success++;						
					}

				}//End Invoice Loop

				//End Process

				tranApp.Commit();
				if(conApp!=null) conApp.Close();
//				if(_ex!=null)
//                    return _ex.ToString();
//				else
					return "";//"xxx";
			}
			catch(ApplicationException appEx)
			{
				tranApp.Rollback();
				if(conApp!=null) conApp.Close();

				throw appEx;
			}
			catch(Exception ex)
			{
				tranApp.Rollback();
				if(conApp!=null) conApp.Close();
				throw ex;
			}
		}
		

		private static void InitTran(Utility util, ref IDbCommand dbCmd, ref DbConnection dbCon, ref IDbConnection conApp, ref IDbTransaction tranApp)
		{
			dbCmd = null;
			dbCon = null;
			conApp = null;
			tranApp = null;
			
			dbCon = DbConnectionManager.GetInstance().GetDbConnection(util.GetAppID(),util.GetEnterpriseID());
			if(dbCon == null)
			{
				Logger.LogTraceError("BatchInvoiceGenerationJob","InvoiceGenerationMgrDAL","ERR001","DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null",null);
			}

			conApp = dbCon.GetConnection();			
			if(conApp == null)
			{
				Logger.LogTraceError("BatchInvoiceGenerationJob","InvoiceGenerationMgrDAL","ERR002","conApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			try{ dbCon.OpenConnection(ref conApp); }
			catch(ApplicationException appEx)
			{
				if(conApp != null) conApp.Close();
				Logger.LogTraceError("BatchInvoiceGenerationJob","InvoiceGenerationMgrDAL","ERR003","Error opening connection : "+ appEx.Message);
				throw new ApplicationException("Error opening database connection ",appEx);
			}
			catch(Exception ex)
			{
				if(conApp != null) conApp.Close();
				Logger.LogTraceError("BatchInvoiceGenerationJob","InvoiceGenerationMgrDAL","ERR004","Error opening connection : "+ ex.Message);
				throw new ApplicationException("Error opening database connection ",ex);
			}

			tranApp = dbCon.BeginTransaction(conApp,IsolationLevel.ReadCommitted);
			if(tranApp == null)
			{
				Logger.LogTraceError("BatchInvoiceGenerationJob","InvoiceGenerationMgrDAL","ERR005","transactionApp is null");
				throw new ApplicationException("Failed Begining a App Transaction..",null);
			}

			dbCmd = dbCon.CreateCommand("select 1 ",CommandType.Text);
			dbCmd.Connection = conApp;
			dbCmd.Transaction = tranApp;
			dbCmd.CommandTimeout = 120; // BY X JUL 09 08
		}

		private static void CloseCon(ref IDbConnection conApp)
		{
			if(conApp != null) conApp.Close();
		}
		private static string strDBNull(object obj)
		{
			string retVal = "NULL";
			switch(System.Type.GetTypeCode(obj.GetType()))
			{
				case TypeCode.DBNull :
					retVal = "NULL";
					break;
				case TypeCode.Decimal :
				case TypeCode.Double : 
				case TypeCode.Int16 :
				case TypeCode.Int32 :
				case TypeCode.Int64 :
					retVal = obj.ToString();
					break;
				default : 
					retVal = "'"+obj.ToString()+"'";
					break;
			}
			return retVal;
		}
		private static int InsertInvoice_Status_Exception(Utility util, string invoice_no, string consignment_no, DataRow rIns,ref  IDbCommand dbCmd,ref  DbConnection dbCon)
		{
			int iRowsEffect = 0;
			try
			{
				dbCmd.CommandText = @"
					declare @count_rows int
					set @count_rows=0

					select @count_rows=count(*) 
					from   Invoice_Status_Exception 
					where   applicationid='"+util.GetAppID()+@"' and 
							enterpriseid='"+util.GetEnterpriseID()+@"' and 
							invoice_no='"+invoice_no+@"' and 
							consignment_no='"+consignment_no+@"' and 
							status_code='"+rIns["status_code"]+@"' and 
							exception_code='"+rIns["exception_code"]+@"' 
						
					If @count_rows=0 
						BEGIN					
							Insert into Invoice_Status_Exception 
							(
								applicationid, enterpriseid, invoice_no, consignment_no, status_code, exception_code, surcharge
							)
							values
							(
								'"+util.GetAppID()+@"',
								'"+util.GetEnterpriseID()+@"',
								'"+invoice_no+@"',
								'"+consignment_no+@"',
								'"+rIns["status_code"]+@"',
								'"+rIns["exception_code"]+@"',
								"+strDBNull(rIns["surcharge"])+@"
							) 
						END
					";
				iRowsEffect = dbCon.ExecuteNonQueryInTransaction(dbCmd);
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("ShipmentUpdateMgrDAL","InvoiceJobMgrDAL","RBAC003",appException.Message.ToString());
				//throw new ApplicationException("Delete operation failed",appException);
				throw new ApplicationException("Delete operation failed"+appException.Message,appException);
			}
			return iRowsEffect;
		}
				
		private static int UpdateInvoice(Utility util, string invoice_no,ref  IDbCommand dbCmd,ref  DbConnection dbCon)
		{
			int iRowsEffect = 0;
			try
			{
				dbCmd.CommandText = @"
						declare @tot_pkg int, @tot_wt decimal(10,2), @tot_dim_wt decimal(10,2), @chargeable_wt decimal(10,2), 
								@insurance_amt money, @other_surcharge money, @tot_freight_charge money, @tot_vas_surcharge money, 
								@esa_surcharge money, @total_exception money, @mbg_amount money, @invoice_adj_amount money, 
								@invoice_amt money,@tot_cons int

						SELECT  @tot_pkg=SUM(tot_pkg) ,
								@tot_wt=SUM(tot_wt),
								@tot_dim_wt=SUM(tot_dim_wt),
								@chargeable_wt=SUM(chargeable_wt),
								@insurance_amt=SUM(insurance_amt),
								@other_surcharge=SUM(other_surcharge),
								@tot_freight_charge=SUM(tot_freight_charge),
								@tot_vas_surcharge=SUM(tot_vas_surcharge),
								@esa_surcharge=SUM(esa_surcharge),
								@total_exception=SUM(total_exception),
								@mbg_amount=SUM(mbg_amount),
								@invoice_adj_amount=SUM(invoice_adj_amount),
								@invoice_amt=SUM(invoice_amt),
								@tot_cons=COUNT(*)
						FROM    Invoice_Detail 
						WHERE   applicationid='"+util.GetAppID()+@"' and enterpriseid = '"+util.GetEnterpriseID()+@"' and 
								invoice_no = '"+invoice_no+@"' 

						Update  Invoice 
						SET		tot_pkg=@tot_pkg, 
								tot_wt=@tot_wt,
								tot_dim_wt=@tot_dim_wt,
								chargeable_wt=@chargeable_wt,
								insurance_amt=@insurance_amt,
								other_surcharge=@other_surcharge,
								tot_freight_charge=@tot_freight_charge,
								tot_vas_surcharge=@tot_vas_surcharge,
								esa_surcharge=@esa_surcharge,
								total_exception=@total_exception,
								mbg_amount=@mbg_amount,
								invoice_adj_amount=@invoice_adj_amount,
								invoice_amt=@invoice_amt,
								tot_cons=@tot_cons,
								invoice_status='N'
								updated_by='"+util.GetUserID()+@"'
								updated_date='"+DateTime.Now+@"' 
						WHERE   @tot_cons > 0 and 
								applicationid='"+util.GetAppID()+@"' and enterpriseid = '"+util.GetEnterpriseID()+@"' and 
								invoice_no = '"+invoice_no+@"' ";
				iRowsEffect = dbCon.ExecuteNonQueryInTransaction(dbCmd);
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("ShipmentUpdateMgrDAL","InvoiceJobMgrDAL","RBAC003",appException.Message.ToString());
				throw new ApplicationException("Update invoice operation failed"+appException.Message,appException);
			}
			return iRowsEffect;
		}

		private static int UpdateInvoice_Detail(Utility util, string invoice_no, string consignment_no, decimal MBG_Amount, decimal PODEX_Amount, DataRow rTrk,ref  IDbCommand dbCmd,ref  DbConnection dbCon)
		{
			int iRowsEffect = 0;
			string strConsig_name="NULL";
			string strPerson_incharge ="''";
			string strPOD_remark = "''";
//strDBNull(rTrk["person_incharge"])
//	strDBNull(rTrk["remark"])
			//Utility.ReplaceSingleQuote
			try
			{ //2217 consignee_name = N"+ strDBNull(rTrk["consignee_name"]) +@", 
					//if(strDBNull(rTrk["consignee_name"])!="NULL") BY X MAY 21 08
				if((rTrk["consignee_name"]!= null) && (Utility.IsNotDBNull(rTrk["consignee_name"])))
					strConsig_name = "N'"+Utility.ReplaceSingleQuote(rTrk["consignee_name"].ToString())+"'";

				if((rTrk["person_incharge"]!= null) && (Utility.IsNotDBNull(rTrk["person_incharge"])))
					strPerson_incharge = "N'"+Utility.ReplaceSingleQuote(rTrk["person_incharge"].ToString())+"'";

				if((rTrk["remark"]!= null) && (Utility.IsNotDBNull(rTrk["remark"])))
					strPOD_remark = "N'"+Utility.ReplaceSingleQuote(rTrk["remark"].ToString())+"'";

				dbCmd.CommandText = @"
						UPDATE  Invoice_Detail
						SET		mbg_amount = "+MBG_Amount+@",
								total_exception = "+ PODEX_Amount +@",
								pod_exception = "+ strDBNull(rTrk["status_code"]) +@",
								person_incharge = "+ strPerson_incharge +@", 
								consignee_name = "+ strConsig_name +@", 
								pod_remark = "+ strPOD_remark +@", 
								invoice_amt = isnull(tot_freight_charge, 0) + isnull(insurance_amt, 0) + 
											  isnull(other_surcharge, 0) + isnull(tot_vas_surcharge, 0) + 
											  isnull(esa_surcharge, 0) + "+ PODEX_Amount +" + ("+MBG_Amount+@") 
						WHERE   applicationid='"+util.GetAppID()+@"' and 
								enterpriseid='"+util.GetEnterpriseID()+@"' and 
								invoice_no='"+invoice_no+@"' and 
								consignment_no='"+consignment_no+"' ";
				iRowsEffect = dbCon.ExecuteNonQueryInTransaction(dbCmd);
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("ShipmentUpdateMgrDAL","InvoiceJobMgrDAL","RBAC003",appException.Message.ToString());
				//throw new ApplicationException("Delete operation failed",appException);
				throw new ApplicationException("Delete operation failed"+appException.Message,appException);
			}
			return iRowsEffect;
		}
		private static int UpdateShipment(Utility util, string invoice_no, DataRow rCsgn,ref  IDbCommand dbCmd,ref  DbConnection dbCon)
		{
			int iRowsEffect = 0;
			try
			{
				dbCmd.CommandText = @" 
						declare @applicationid varchar(40), @enterpriseid varchar(40), 
								@invoice_no varchar(20), @consignment_no varchar(30) 
						set		@applicationid='"+util.GetAppID()+@"'
						set		@enterpriseid='"+util.GetEnterpriseID()+@"' 
						set		@invoice_no='"+invoice_no+@"'
						set		@consignment_no='"+rCsgn["consignment_no"].ToString()+@"'

						/* Select Value */
						declare @total_exception Dollar,@mbg_amount Dollar,
								@invoice_adj_amount Dollar,@invoice_amt Dollar, 
								@invoice_date datetime

						select  @total_exception=total_exception,
								@mbg_amount=mbg_amount, 
								@invoice_adj_amount=invoice_adj_amount, 
								@invoice_amt=invoice_amt, 
								@invoice_no=invoice_no
						from   Invoice_Detail 
						where  applicationid = @applicationid and enterpriseid = @enterpriseid and 
							   invoice_no=@invoice_no and consignment_no=@consignment_no 

						select @invoice_date=invoice_date
						from   Invoice
						where  applicationid = @applicationid and enterpriseid = @enterpriseid and invoice_no=@invoice_no


						/* Update Shipment */
						UPDATE	Shipment 
						SET		total_excp_charges = @total_exception,
								mbg_amount = @mbg_amount,
								invoice_adjustment = @invoice_adj_amount,
								invoice_amt = @invoice_amt,
								invoice_no = @invoice_no,
								invoice_date = @invoice_date
						WHERE   applicationid=@applicationid and 
								enterpriseid=@enterpriseid and 
								consignment_no=@consignment_no  ";
				iRowsEffect = dbCon.ExecuteNonQueryInTransaction(dbCmd);
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("ShipmentUpdateMgrDAL","InvoiceJobMgrDAL","RBAC003",appException.Message.ToString());
				//throw new ApplicationException("Delete operation failed",appException);
				throw new ApplicationException("Delete operation failed"+appException.Message,appException);
			}
			return iRowsEffect;
		}

		private static void InsertInvoice_Sumary(Utility util, string invoice_no,ref IDbCommand dbCmd,ref DbConnection dbCon)
		{
			try
			{				
				//Get from [Core_System_Code]
				dbCmd.CommandText = @"SELECT sequence, code_text, code_str_value 
									  FROM Core_System_Code
									  WHERE (codeid = 'invoice_summary') AND (culture = 'en-US') ";
				DataSet ds =(DataSet)dbCon.ExecuteQueryInTransaction(dbCmd, ReturnType.DataSetType);

				//Loop Core_System_Code by code_str_value
				foreach(DataRow r in ds.Tables[0].Rows)
				{
					int iRowsEffect = 0;
					string service_code = r["code_str_value"].ToString().ToUpper();
					string sql = @" 
						declare @applicationid varchar(40), 
								@enterpriseid varchar(40), 
								@invoice_no varchar(20),
								@service_code varchar(12)

						set		@applicationid = '"+util.GetAppID()+@"'
						set		@enterpriseid = '"+util.GetEnterpriseID()+@"'
						set		@invoice_no = '"+invoice_no+@"' 
						set		@service_code = '"+service_code+"' ";

					switch(service_code)
					{
						#region SHP
						case "SHP": 							
							sql += @"
							INSERT INTO Invoice_Summary
							(	
								applicationid,
								enterpriseid,
								invoice_no,
								tot_consignment,
								tot_pkg,
								tot_chargeable_wt,
								invoice_amt,
								summary_code,
								summary_code_type,
								sequence
							)
							SELECT  @applicationid,
									@enterpriseid,
									@invoice_no,
									COUNT(*)as rows,
									SUM(tot_pkg) AS tot_pkg,
									SUM(chargeable_wt) AS tot_chargeable_wt,
									SUM(invoice_amt) AS invoice_amt,
									service_code, 
									@service_code,
									0
							FROM	Invoice_Detail
							WHERE  applicationid=@applicationid and enterpriseid = @enterpriseid AND 
								   invoice_no = @invoice_no 
							GROUP BY invoice_no, service_code ";
							break;

						#endregion							

						#region INS
						case "INS": 
							sql += @"
							INSERT INTO Invoice_Summary
							(
								applicationid,
								enterpriseid,
								invoice_no,
								tot_consignment,
								tot_pkg,
								tot_chargeable_wt,
								invoice_amt,
								summary_code,
								summary_code_type,
								sequence
							)
							SELECT	@applicationid,
									@enterpriseid,
									@invoice_no,
									COUNT(*)as rows,
									SUM(tot_pkg) AS tot_pkg,
									SUM(chargeable_wt) AS tot_chargeable_wt,
									SUM(insurance_amt) AS invoice_amt,
									@service_code,
									@service_code,
									1
							FROM	Invoice_Detail
							WHERE   applicationid=@applicationid and enterpriseid = @enterpriseid AND 
									invoice_no = @invoice_no and (insurance_amt <> 0) 
							GROUP BY invoice_no ";
							break;
						#endregion

						#region VAS
						case "VAS": 
							sql += @"
							INSERT INTO Invoice_Summary
							(
								applicationid,
								enterpriseid,
								invoice_no,
								tot_consignment,
								tot_pkg,
								tot_chargeable_wt,
								invoice_amt,
								summary_code,
								summary_code_type,
								sequence
							)
							SELECT  @applicationid,
									@enterpriseid,
									@invoice_no,
									COUNT(*)as rows,
									SUM(tot_pkg) AS tot_pkg,
									SUM(chargeable_wt) AS tot_chargeable_wt,
									SUM(tot_vas_surcharge) AS invoice_amt,
									@service_code,
									@service_code,
									3
							FROM	Invoice_Detail
							WHERE   applicationid=@applicationid and enterpriseid=@enterpriseid and 
									invoice_no=@invoice_no and (tot_vas_surcharge <> 0) 
							GROUP BY invoice_no ";
							break;
						#endregion 

						#region ESA
						case "ESA": 
							sql += @"
							INSERT INTO Invoice_Summary
							(
								applicationid,
								enterpriseid,
								invoice_no,
								tot_consignment,
								tot_pkg,
								tot_chargeable_wt,
								invoice_amt,
								summary_code,
								summary_code_type,
								sequence
							)
							SELECT  @applicationid,
									@enterpriseid,
									@invoice_no,
									COUNT(*)as rows,
									SUM(tot_pkg) AS tot_pkg,
									SUM(chargeable_wt) AS tot_chargeable_wt,
									SUM(esa_surcharge) AS invoice_amt,
									@service_code,
									@service_code,
									4
							FROM	Invoice_Detail
							WHERE   applicationid=@applicationid and enterpriseid=@enterpriseid and 
									invoice_no=@invoice_no and (esa_surcharge <> 0) 
							GROUP BY invoice_no ";
							break;
						#endregion

						#region PEX
						case "PEX": 
							sql += @"
							INSERT INTO Invoice_Summary
							(
								applicationid,
								enterpriseid,
								invoice_no,
								tot_consignment,
								tot_pkg,
								tot_chargeable_wt,
								invoice_amt,
								summary_code,
								summary_code_type,
								sequence
							)
							SELECT  @applicationid,
									@enterpriseid,
									@invoice_no,
									COUNT(*)as rows,
									SUM(tot_pkg) AS tot_pkg,
									SUM(chargeable_wt) AS tot_chargeable_wt,
									SUM(total_exception) AS invoice_amt,
									@service_code,
									@service_code,
									2
							FROM	Invoice_Detail
							WHERE   applicationid=@applicationid and enterpriseid=@enterpriseid and 
									invoice_no=@invoice_no and (total_exception <> 0)
							GROUP BY invoice_no ";
							break;
						#endregion

						#region ADJ
						case "ADJ": 
							sql += @"
							INSERT INTO Invoice_Summary
							(
								applicationid,
								enterpriseid,
								invoice_no,
								tot_consignment,
								tot_pkg,
								tot_chargeable_wt,
								invoice_amt,
								summary_code,
								summary_code_type,
								sequence
							)
							SELECT  @applicationid,
									@enterpriseid,
									@invoice_no,
									COUNT(*)as rows,
									SUM(tot_pkg) AS tot_pkg,
									SUM(chargeable_wt) AS tot_chargeable_wt,
									SUM(invoice_adj_amount) AS invoice_amt,
									@service_code,
									@service_code,
									5
							FROM	Invoice_Detail
							WHERE   applicationid=@applicationid and enterpriseid=@enterpriseid and 
									invoice_no=@invoice_no and (invoice_adj_amount <> 0) 
							GROUP BY invoice_no ";
							break;
						#endregion

						#region OTH
						case "OTH": 
							sql += @"
							INSERT INTO Invoice_Summary
							(
								applicationid,
								enterpriseid,
								invoice_no,
								tot_consignment,
								tot_pkg,
								tot_chargeable_wt,
								invoice_amt,
								summary_code,
								summary_code_type,
								sequence
							)
							SELECT  @applicationid,
									@enterpriseid,
									@invoice_no,
									COUNT(*)as rows,
									SUM(tot_pkg) AS tot_pkg,
									SUM(chargeable_wt) AS tot_chargeable_wt,
									SUM(other_surcharge) AS invoice_amt,
									@service_code,
									@service_code,
									7
							FROM	Invoice_Detail
							WHERE   applicationid=@applicationid and enterpriseid=@enterpriseid and 
									invoice_no=@invoice_no and (other_surcharge <> 0) 
							GROUP BY invoice_no ";
							break;
						#endregion

						#region MBG
						case "MBG": 
							sql += @"
							INSERT INTO Invoice_Summary
							(
								applicationid,
								enterpriseid,
								invoice_no,
								tot_consignment,
								tot_pkg,
								tot_chargeable_wt,
								invoice_amt,
								summary_code,
								summary_code_type,
								sequence
							)
							SELECT  @applicationid,
									@enterpriseid,
									@invoice_no,
									COUNT(*),
									SUM(tot_pkg) AS tot_pkg,
									SUM(chargeable_wt) AS tot_chargeable_wt,
									SUM(mbg_amount) AS invoice_amt,
									@service_code,
									@service_code,
									6
							FROM	Invoice_Detail
							WHERE   applicationid=@applicationid and enterpriseid=@enterpriseid and 
									invoice_no=@invoice_no and (mbg_amount <> 0) 
							GROUP BY invoice_no ";
							break;
						#endregion
					}

					dbCmd.CommandText = sql;
					iRowsEffect = dbCon.ExecuteNonQueryInTransaction(dbCmd);
				}
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("ShipmentUpdateMgrDAL","InsertInvoice_Sumary","ERR002","Error : "+ appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ",appException);
			}
			catch(Exception exception)
			{
				Logger.LogTraceError("ShipmentUpdateMgrDAL","InsertInvoice_Sumary","ERR003","Error : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}
		}

		private static DataTable GetInvoice(Utility util, string jobid,ref IDbCommand dbCmd,ref DbConnection dbCon)
		{
			dbCmd.CommandText = @" 
						SELECT  *
						FROM	Invoice
						where	applicationid='"+util.GetAppID()+@"' and 
								enterpriseid='"+util.GetEnterpriseID()+@"' and 
								jobid='"+jobid+"' ";

			DataSet ds = new DataSet();
			try
			{
				ds =(DataSet)dbCon.ExecuteQueryInTransaction(dbCmd, ReturnType.DataSetType);
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("ShipmentUpdateMgrDAL","GetInvoice","ERR002","Error : "+ appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ",appException);
			}
			catch(Exception exception)
			{
				Logger.LogTraceError("ShipmentUpdateMgrDAL","GetInvoice","ERR003","Error : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}

			return ds.Tables[0];
		}

		private static DataTable GetInvoiceDetail(Utility util, string invoice_no,ref  IDbCommand dbCmd,ref DbConnection dbCon)
		{
			dbCmd.CommandText = @" 
						SELECT  /*Invoice_Detail fields*/
								ID.applicationid, ID.enterpriseid, ID.invoice_no, ID.consignment_no, 
								ID.total_exception, ID.mbg_amount, ID.invoice_adj_amount, ID.invoice_amt,

								/*Shipment fields*/
								S.booking_no, S.payerid, S.payer_name, S.sender_name, S.payer_type, S.payment_mode
						FROM	Invoice_Detail ID inner join 
								Shipment S ON ID.applicationid = S.applicationid AND ID.enterpriseid = S.enterpriseid AND 
											ID.consignment_no = S.consignment_no 
						WHERE	S.applicationid = '"+util.GetAppID()+@"' and 
								S.enterpriseid = '"+util.GetEnterpriseID()+@"' and 
								ID.invoice_no ='"+invoice_no+@"'
						ORDER BY S.consignment_no ";

			DataSet ds = new DataSet();
			try
			{
				ds =(DataSet)dbCon.ExecuteQueryInTransaction(dbCmd, ReturnType.DataSetType);
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("ShipmentUpdateMgrDAL","GetInvoice","ERR002","Error : "+ appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ",appException);
			}
			catch(Exception exception)
			{
				Logger.LogTraceError("ShipmentUpdateMgrDAL","GetInvoice","ERR003","Error : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}

			return ds.Tables[0];
		}

		private static DataTable GetShipmentFailure(Utility util, string consignment_no, string paeryid ,ref  IDbCommand dbCmd,ref DbConnection dbCon)
		{
			string sql1 = @"
				declare @applicationid varchar(40), @enterpriseid varchar(40), 
						@payerid varchar(20), @consignment_no varchar(30), 
						@IsShipmentFailure bit, @CustomerMBG char(1), @LastPODEX_MBG char(1),
						@MBG_Amount money, @PODEX_Amount money

				set		@applicationid = '"+util.GetAppID()+@"'
				set		@enterpriseid = '"+util.GetEnterpriseID()+@"'	
				set		@payerid = '"+paeryid+@"'			
				set		@consignment_no = '"+consignment_no+@"'

				set		@IsShipmentFailure = 0 
				set		@CustomerMBG = 'Y' 
				set		@LastPODEX_MBG = 'Y' 
				set		@MBG_Amount=0 
				set		@PODEX_Amount=0 

				--IsShipmentFailure?
				select  @IsShipmentFailure=(case when STG.tracking_datetime>S.est_delivery_datetime then 1 else 0 end),		
						@LastPODEX_MBG=(select  mbg from exception_code 
										where	applicationid=STG.applicationid and enterpriseid=STG.enterpriseid and 
												exception_code in 
													(select exception_code from shipment_tracking 
													where  applicationid=STG.applicationid and 
															enterpriseid=STG.enterpriseid and 
															consignment_no=STG.consignment_no and 
															booking_no=STG.booking_no and tracking_datetime=STG.tracking_datetime) ),
						@MBG_Amount=S.tot_freight_charge + S.esa_surcharge/*, @payerid=S.payerid*/
				from    (   select st.applicationid,st.enterpriseid,st.consignment_no, 
								   st.booking_no, max(st.tracking_datetime)as tracking_datetime
							from   shipment_tracking st
							where  st.status_code='PODEX' and isnull(st.deleted, 'N') <> 'Y'
							group by applicationid,enterpriseid,consignment_no, booking_no) STG left join 
						shipment S on STG.applicationid=S.applicationid and STG.enterpriseid=S.enterpriseid and 
									STG.consignment_no=S.consignment_no and STG.booking_no=S.booking_no 
				where   S.applicationid=@applicationid and S.enterpriseid=@enterpriseid and S.consignment_no=@consignment_no

				If @IsShipmentFailure=1 
					begin 
						select @CustomerMBG=mbg 
						from   Customer 
						where  applicationid=@applicationid and enterpriseid=@enterpriseid and custid=@payerid 

						/* PODEX Amount */
						If @CustomerMBG='Y' and @LastPODEX_MBG='Y'  
						Begin
							select @PODEX_Amount=isnull(sum(isnull(CEC.surcharge,0)), 0)
							from  Customer_Exception_Charge CEC inner join 
								Shipment_Tracking ST on  CEC.applicationid=ST.applicationid and CEC.enterpriseid=ST.enterpriseid and 
															CEC.status_code=ST.status_code and CEC.exception_code=ST.exception_code 
							where ST.applicationid=@applicationid and ST.enterpriseid=@enterpriseid and 
								ST.consignment_no=@consignment_no and ST.status_code='PODEX' and CEC.custid=@payerid and CEC.surcharge>0 
						End
					end

				select  @IsShipmentFailure as IsShipmentFailure, @CustomerMBG as CustomerMBG, 
						@LastPODEX_MBG as LastPODEX_MBG, @MBG_Amount as MBG_Amount, @PODEX_Amount as PODEX_Amount ";

			DataSet ds = new DataSet();
			try
			{
				dbCmd.CommandText = sql1;
				ds =(DataSet)dbCon.ExecuteQueryInTransaction(dbCmd,ReturnType.DataSetType);
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("ShipmentUpdateMgrDAL","GetShipmentPayerType","ERR002","Error : "+ appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ",appException);
			}
			catch(Exception exception)
			{
				Logger.LogTraceError("ShipmentUpdateMgrDAL","GetShipmentPayerType","ERR003","Error : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}

			return ds.Tables[0];
		}

		private static DataTable GetPODEX_listedInCustExCharge(Utility util, string consignment_no, string payerid,ref IDbCommand dbCmd,ref DbConnection dbCon)
		{
			//PODEX is listed in Status & Exception Surcharges for Customer			
			string sql1 = @"
				declare @applicationid varchar(40), 
						@enterpriseid varchar(40), 
						@consignment_no varchar(30),
						@payerid varchar(20)

				set		@applicationid = '"+util.GetAppID()+@"'
				set		@enterpriseid = '"+util.GetEnterpriseID()+@"'
				set		@consignment_no = '"+consignment_no+@"'
				set		@payerid = '"+payerid+@"' 

				select	CEC.*
				from	Customer_Exception_Charge CEC inner join 
						Shipment_Tracking ST on CEC.applicationid=ST.applicationid and CEC.enterpriseid=ST.enterpriseid and 
												CEC.status_code=ST.status_code and CEC.exception_code=ST.exception_code 
				where	CEC.applicationid=@applicationid and 
						CEC.enterpriseid=@enterpriseid and 
						CEC.custid=@payerid and 
						ST.consignment_no=@consignment_no and 
						(ST.deleted<>'Y' or ST.deleted is null) and 
						ST.status_code='PODEX' ";

			DataSet ds = new DataSet();
			try
			{
				dbCmd.CommandText = sql1;
				ds =(DataSet)dbCon.ExecuteQueryInTransaction(dbCmd,ReturnType.DataSetType);
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("ShipmentUpdateMgrDAL","GetShipmentPayerType","ERR002","Error : "+ appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ",appException);
			}
			catch(Exception exception)
			{
				Logger.LogTraceError("ShipmentUpdateMgrDAL","GetShipmentPayerType","ERR003","Error : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}

			return ds.Tables[0];
		}

		private static DataTable GetShpTrkForUpdateInvDetail(Utility util, string consignment_no,ref IDbCommand dbCmd,ref DbConnection dbCon)
		{
			dbCmd.CommandText = @" 
					declare @applicationid varchar(40), 
							@enterpriseid varchar(40), 
							@consignment_no varchar(30),
							@hasPOD bit, @hasPODEX bit

					set		@applicationid = '"+util.GetAppID()+@"'
					set		@enterpriseid = '"+util.GetEnterpriseID()+@"'
					set		@consignment_no = '"+consignment_no+@"' 
					set		@hasPOD = 0
					set		@hasPODEX = 0

					select  @hasPOD=count(ST.status_code)
					from    Shipment_Tracking ST
					where   ST.applicationid=@applicationid and ST.enterpriseid=@enterpriseid and 
							ST.consignment_no=@consignment_no and ST.status_code='POD'

					select  @hasPODEX=count(ST.status_code)
					from    Shipment_Tracking ST
					where   ST.applicationid=@applicationid and ST.enterpriseid=@enterpriseid and 
							ST.consignment_no=@consignment_no and ST.status_code='PODEX'

					IF @hasPOD=1 and @hasPODEX=0	
						select  @hasPOD as hasPOD, @hasPODEX as hasPODEX, ST.*
						from    Shipment_Tracking ST									
						where   ST.applicationid=@applicationid and ST.enterpriseid=@enterpriseid and 
								ST.consignment_no=@consignment_no and ST.status_code='POD'
					ELSE 
						select  @hasPOD as hasPOD, @hasPODEX as hasPODEX, ST.*
						from   (select  max(tracking_datetime) as tracking_datetime, applicationid, enterpriseid, 
										consignment_no, status_code
								from   Shipment_Tracking
								where  isnull(deleted, 'N') <> 'Y' 
								group by applicationid, enterpriseid, consignment_no, status_code) STG left join 
								Shipment_Tracking ST on STG.applicationid=ST.applicationid and 
														STG.enterpriseid=ST.enterpriseid and 
														STG.consignment_no=ST.consignment_no and 
														STG.tracking_datetime=ST.tracking_datetime and 
														STG.status_code=ST.status_code 
						where	STG.applicationid=@applicationid and STG.enterpriseid=@enterpriseid and 
								STG.consignment_no=@consignment_no and STG.status_code='PODEX'  ";

			DataSet ds = new DataSet();
			try
			{
				ds =(DataSet)dbCon.ExecuteQueryInTransaction(dbCmd, ReturnType.DataSetType);
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("ShipmentUpdateMgrDAL","GetInvoice","ERR002","Error : "+ appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ",appException);
			}
			catch(Exception exception)
			{
				Logger.LogTraceError("ShipmentUpdateMgrDAL","GetInvoice","ERR003","Error : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}

			return ds.Tables[0];
		}

		#endregion


#region "OLD INVOICE GEN"
		public static DataSet GetInvoiceableShipments_OLD(String strAppID, String strEnterpriseID, DataSet dsInvoiceCriteria)
		{
			string strSQLWhere = null;
			DataRow dr = dsInvoiceCriteria.Tables[0].Rows[0];
			string strInvoiceFor = (string)dr["Invoice_For"];
			string strCustomerType = (string)dr["Customer_Type"];
			if (strCustomerType!=null && strCustomerType!="" && strCustomerType!="B")
				strSQLWhere += "and sh.payer_type = '"+strCustomerType+"' ";

			string strCustomerCode = (string)dr["Customer_Code"];
			string strCustomerName = (string)dr["Customer_Name"];
			//if (strInvoiceFor!=null && strInvoiceFor!="")
			if (strInvoiceFor=="I")
			{
				if (strCustomerCode!=null && strCustomerCode!="")
					strSQLWhere += "and sh.payerid = '"+strCustomerCode+"' ";

				if (strCustomerName!=null && strCustomerName!="")
					strSQLWhere += "and sh.payer_name = N'"+Utility.ReplaceSingleQuote(strCustomerName)+"' ";
			}

			string strInvoiceToDate = (string)dr["Invoice_ToDate"];
			string strDateFrom = null;
			string strDateTo = null;

			if ((dr["DateFrom"]!=null) && (!dr["DateFrom"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				DateTime dtFrom = (DateTime) dr["DateFrom"];
				strDateFrom = Utility.DateFormat(strAppID, strEnterpriseID, dtFrom, DTFormat.DateTime);
				if (strInvoiceToDate == "N")
				{
					strSQLWhere += "and booking_datetime >= "+strDateFrom+" ";
				}
			}

			if ((dr["DateTo"]!=null) && (!dr["DateTo"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				DateTime dtTo = (DateTime) dr["DateTo"];
				strDateTo = Utility.DateFormat(strAppID, strEnterpriseID, dtTo, DTFormat.DateTime);
				strSQLWhere += "and booking_datetime <= "+strDateTo+" ";
			}

			DataSet dsIShip = null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("InvoiceGenerationMgrDAL","GetInvoiceableShipments","EDB101","DbConnection object is null!!");
				return dsIShip;
			}

			StringBuilder strBuilder = new StringBuilder();
			strBuilder.Append("select sh.payerid, sh.payer_type, sh.payer_name, sh.payer_address1, sh.payer_address2, ");
			strBuilder.Append("sh.payer_zipcode, sh.payer_country, sh.payer_telephone, sh.payer_fax, sh.payment_mode, ");
			strBuilder.Append("sh.consignment_no, sh.booking_no, sh.booking_datetime, sh.ref_no, sh.origin_state_code, ");
			strBuilder.Append("sh.destination_state_code, sh.tot_pkg, sh.tot_wt, sh.tot_dim_wt, sh.chargeable_wt, ");
			strBuilder.Append("sh.recipient_name, sh.recipient_address1, sh.recipient_address2, sh.recipient_zipcode, ");
			strBuilder.Append("sh.recipient_country, sh.sender_name, sh.sender_address1, sh.sender_address2, ");
			strBuilder.Append("sh.sender_zipcode, sh.sender_country, sh.service_code, sh.insurance_surcharge, ");
			strBuilder.Append("sh.tot_freight_charge, sh.tot_vas_surcharge, sh.esa_surcharge, ");
			strBuilder.Append("c.credit_term AS customer_credit_term, a.credit_term AS agent_credit_term, ");
			strBuilder.Append("sh.payment_mode, ");
			strBuilder.Append("c.mbg AS customer_mbg, a.mbg AS agent_mbg ");
			strBuilder.Append("from Shipment sh, Status_Code sc, Customer c, Agent a ");
			strBuilder.Append("where sh.enterpriseid = sc.enterpriseid AND sh.applicationid = sc.applicationid AND ");
			strBuilder.Append("sh.last_status_code = sc.status_code AND ");
			strBuilder.Append(Utility.FormatOuterJoinString(strAppID, strEnterpriseID, "sh.enterpriseid", "c.enterpriseid", OuterJoin.Left)+" AND ");
			strBuilder.Append(Utility.FormatOuterJoinString(strAppID, strEnterpriseID, "sh.applicationid", "c.applicationid", OuterJoin.Left)+" AND ");
			strBuilder.Append(Utility.FormatOuterJoinString(strAppID, strEnterpriseID, "sh.payerid", "c.custid", OuterJoin.Left)+" AND ");
			strBuilder.Append(Utility.FormatOuterJoinString(strAppID, strEnterpriseID, "sh.enterpriseid", "a.enterpriseid", OuterJoin.Left)+" AND ");
			strBuilder.Append(Utility.FormatOuterJoinString(strAppID, strEnterpriseID, "sh.applicationid", "a.applicationid", OuterJoin.Left)+" AND ");
			strBuilder.Append(Utility.FormatOuterJoinString(strAppID, strEnterpriseID, "sh.payerid", "a.agentid", OuterJoin.Left)+" AND ");
			strBuilder.Append("sh.applicationid = '"+strAppID+"' AND sh.enterpriseid = '"+strEnterpriseID+"' AND ");

			strBuilder.Append("sc.invoiceable IN ('Y', 'y') AND (sh.invoice_no is null OR sh.invoice_no = '') ");
			if (strSQLWhere!=null && strSQLWhere!="")
			{
				strBuilder.Append(strSQLWhere);
			}
			strBuilder.Append("Order by sh.payer_type, sh.payerid, sh.booking_no, sh.consignment_no ");
			String strSQLQuery = strBuilder.ToString();

			dsIShip = (DataSet)dbCon.ExecuteQuery(strSQLQuery,ReturnType.DataSetType);

			return dsIShip;
		}

		public static int InsertInvoiceHeader(string strAppID, string strEnterpriseID, string strSqlInsert)
		{
			int iRowsInserted = 0;
			IDbCommand dbCom = null;

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("InvoiceGenerationMgrDAL","InsertInvoiceHeader","SDMG001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			try
			{
				dbCom = dbCon.CreateCommand(strSqlInsert, CommandType.Text);
				iRowsInserted = dbCon.ExecuteNonQuery(dbCom);
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("InvoiceGenerationMgrDAL","InsertInvoiceHeader","RBAC003",appException.Message.ToString());
				throw new ApplicationException("Insert operation failed"+appException.Message,appException);
			}
			return iRowsInserted;
		}

		public static DataSet GetInvoiceableSurcharges(string strAppID, string strEnterpriseID, int intBookingNo, string strConsgNo, string strPayerType)
		{
			StringBuilder strBuilder = new StringBuilder();
			strBuilder.Append("SELECT st.consignment_no, st.status_code, st.exception_code, sum(sc.surcharge) status_surcharge, ");
			strBuilder.Append("sum(ec.surcharge) exception_surcharge ");
			if (strPayerType == "C")
				strBuilder.Append("FROM Shipment_Tracking st, shipment s, Customer_Status_Charge sc, Customer_Exception_Charge ec ");
			else
				strBuilder.Append("FROM Shipment_Tracking st, shipment s, Agent_Status_Charge sc, Agent_Exception_Charge ec ");

			strBuilder.Append("WHERE st.applicationid = s.applicationid and st.enterpriseid = s.enterpriseid and ");
			strBuilder.Append("st.booking_no = s.booking_no and st.consignment_no = s.consignment_no and ");
			strBuilder.Append(Utility.FormatOuterJoinString(strAppID, strEnterpriseID, "st.applicationid", "sc.applicationid", OuterJoin.Left)+" and ");
			strBuilder.Append(Utility.FormatOuterJoinString(strAppID, strEnterpriseID, "st.enterpriseid", "sc.enterpriseid", OuterJoin.Left)+" and ");
			if (strPayerType == "C")
				strBuilder.Append(Utility.FormatOuterJoinString(strAppID, strEnterpriseID, "s.payerid", "sc.custid", OuterJoin.Left)+" and ");
			else
				strBuilder.Append(Utility.FormatOuterJoinString(strAppID, strEnterpriseID, "s.payerid", "sc.agentid", OuterJoin.Left)+" and ");

			strBuilder.Append(Utility.FormatOuterJoinString(strAppID, strEnterpriseID, "st.status_code", "sc.status_code", OuterJoin.Left)+" and ");
			strBuilder.Append(Utility.FormatOuterJoinString(strAppID, strEnterpriseID, "st.applicationid", "ec.applicationid", OuterJoin.Left)+" and ");
			strBuilder.Append(Utility.FormatOuterJoinString(strAppID, strEnterpriseID, "st.enterpriseid", "ec.enterpriseid", OuterJoin.Left)+" and ");
			if (strPayerType == "C")
				strBuilder.Append(Utility.FormatOuterJoinString(strAppID, strEnterpriseID, "s.payerid", "ec.custid", OuterJoin.Left)+" and ");
			else
				strBuilder.Append(Utility.FormatOuterJoinString(strAppID, strEnterpriseID, "s.payerid", "ec.agentid", OuterJoin.Left)+" and ");

			strBuilder.Append(Utility.FormatOuterJoinString(strAppID, strEnterpriseID, "st.status_code", "ec.status_code", OuterJoin.Left)+" and ");
			strBuilder.Append(Utility.FormatOuterJoinString(strAppID, strEnterpriseID, "st.exception_code", "ec.exception_code", OuterJoin.Left)+" and ");
			strBuilder.Append("st.applicationid = '"+strAppID+"' and st.enterpriseid = '"+strEnterpriseID+"' and ");
			strBuilder.Append("st.booking_no = "+intBookingNo+" and st.consignment_no = '"+strConsgNo+"' ");
			strBuilder.Append("GROUP BY st.consignment_no, st.status_code, st.exception_code");
			String strSQLQuery = strBuilder.ToString();

			DataSet dsISurcharge = null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("InvoiceGenerationMgrDAL","GetInvoiceableSurcharges","EDB101","DbConnection object is null!!");
				return dsISurcharge;
			}

			dsISurcharge = (DataSet)dbCon.ExecuteQuery(strSQLQuery,ReturnType.DataSetType);

			return dsISurcharge;
		}

		public static DataSet GetInvoiceableSurcharges(string strAppID, string strEnterpriseID, int intBookingNo, string strConsgNo)
		{
			StringBuilder strBuilder = new StringBuilder();
			strBuilder.Append("SELECT st.consignment_no, st.status_code, st.exception_code, e.mbg, sum(st.surcharge) surcharge ");
			strBuilder.Append("FROM Shipment_Tracking st, shipment s, exception_code e ");
			strBuilder.Append("WHERE st.applicationid = s.applicationid and st.enterpriseid = s.enterpriseid and ");
			strBuilder.Append("st.booking_no = s.booking_no and st.consignment_no = s.consignment_no and ");
			strBuilder.Append(Utility.FormatOuterJoinString(strAppID, strEnterpriseID, "st.applicationid", "e.applicationid", OuterJoin.Left));
			strBuilder.Append(" and ");
			strBuilder.Append(Utility.FormatOuterJoinString(strAppID, strEnterpriseID, "st.enterpriseid", "e.enterpriseid", OuterJoin.Left));
			strBuilder.Append(" and ");
			strBuilder.Append(Utility.FormatOuterJoinString(strAppID, strEnterpriseID, "st.status_code", "e.status_code", OuterJoin.Left));
			strBuilder.Append(" and ");
			strBuilder.Append(Utility.FormatOuterJoinString(strAppID, strEnterpriseID, "st.exception_code", "e.exception_code", OuterJoin.Left));
			strBuilder.Append(" and st.applicationid = '");
			strBuilder.Append(strAppID);
			strBuilder.Append("' and st.enterpriseid = '");
			strBuilder.Append(strEnterpriseID);
			strBuilder.Append("' and st.booking_no = ");
			strBuilder.Append(intBookingNo);
			strBuilder.Append(" and st.consignment_no = '");
			strBuilder.Append(strConsgNo);
			strBuilder.Append("' GROUP BY st.consignment_no, st.status_code, st.exception_code, e.mbg");
			String strSQLQuery = strBuilder.ToString();

			DataSet dsISurcharge = null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("InvoiceGenerationMgrDAL","GetInvoiceableSurcharges","EDB101","DbConnection object is null!!");
				return dsISurcharge;
			}

			dsISurcharge = (DataSet)dbCon.ExecuteQuery(strSQLQuery,ReturnType.DataSetType);

			return dsISurcharge;
		}

		public static int UpdateShipment(string strAppID, string strEnterpriseID, int intBookingNo, string strConsgNo, string strInvoiceNo, string strInvoiceDate, decimal decInvoiceAmt)
		{
			StringBuilder strBuilder = new StringBuilder();
			strBuilder.Append("Update shipment set invoice_no = '"+strInvoiceNo+"', invoice_date = "+strInvoiceDate+", invoice_amt = "+decInvoiceAmt);
			strBuilder.Append(" where applicationid = '"+strAppID+"' and enterpriseid = '"+strEnterpriseID+"' and booking_no = "+intBookingNo+" and consignment_no = '"+strConsgNo+"'");
			string strSQLQuery = strBuilder.ToString();
			int iRowsUpdated = 0;

			IDbCommand dbCom = null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("InvoiceGenerationMgrDAL","UpdateShipment","SDMG001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			try
			{
				dbCom = dbCon.CreateCommand(strSQLQuery,CommandType.Text);
				iRowsUpdated = dbCon.ExecuteNonQuery(dbCom);
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("InvoiceGenerationMgrDAL","UpdateShipment","RBAC003",appException.Message.ToString());
				throw new ApplicationException("Update operation failed",appException);
				
			}
			return iRowsUpdated;
		}

#endregion 


	}
}