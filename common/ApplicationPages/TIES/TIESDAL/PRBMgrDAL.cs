using System;
using System.Data;
using System.Collections;
using System.Text;
using com.common.DAL;
using com.common.util;
using com.common.classes;
namespace com.ties.DAL
{
	/// <summary>
	/// Summary description for PRBManager.
	/// </summary>
	public class PRBMgrDAL
	{
		public PRBMgrDAL()
		{
		}	
		
		public static DataSet GetPickupRequestData(String strAppID,String strEnterpriseID)
		{
			string strPickupRequest=null;

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("PickupRequest","GetFromPickUp","PRBMGR01","DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null",null);
			}
			try
			{
				strPickupRequest ="select * from Pickup_Request";
				DataSet  dsPickupRequest =(DataSet)dbCon.ExecuteQuery(strPickupRequest,ReturnType.DataSetType); 
				return dsPickupRequest;		
					
			}
			catch
			{
				Logger.LogTraceInfo("PRBMgrDAL","GetPickupRequest","PRBMGR02","Select command failed");
				throw new ApplicationException("Select command failed");
			}
		}	

		public static string GetDC(String strAppID, String strEnterpriseID, String strPathCode )
		{
			string strSQLWhere = "";
			if (strPathCode!=null && strPathCode!="")
			{
				strSQLWhere += "and path_code like '%"+strPathCode+"%' ";
			}

			
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("SysDataManager","GetStateCodeDS","EDB101","DbConnection object is null!!");
			}
			
			String strSQLQuery = "select origin_station as DC from delivery_path where ";
			strSQLQuery += "applicationid = '"+strAppID+"' and enterpriseid = '"+strEnterpriseID+"'"; 
			if (strSQLWhere!=null && strSQLWhere!="")
			{
				strSQLQuery += strSQLWhere;
			}
			
			DataSet DS = (DataSet)dbCon.ExecuteQuery(strSQLQuery,ReturnType.DataSetType);
			if(DS.Tables[0].Rows.Count > 0)
				return  DS.Tables[0].Rows[0][0].ToString();
			else
				return "";
		}


		public static SessionDS GetEmptyPRBDS()
		{ 
			DataTable dtPickupRequest = new DataTable();
			dtPickupRequest.Columns.Add("booking_no",typeof(string));
			dtPickupRequest.Columns.Add("booking_datetime",typeof(DateTime));
			dtPickupRequest.Columns.Add("booking_type",typeof(string));
			dtPickupRequest.Columns.Add("new_account",typeof(string));
			dtPickupRequest.Columns.Add("payerid",typeof(string));
			dtPickupRequest.Columns.Add("payer_type",typeof(string));
			dtPickupRequest.Columns.Add("payer_name",typeof(string));
			dtPickupRequest.Columns.Add("payer_address1",typeof(string));
			dtPickupRequest.Columns.Add("payer_address2",typeof(string));
			dtPickupRequest.Columns.Add("payer_zipcode",typeof(string));
			dtPickupRequest.Columns.Add("payer_country",typeof(string));
			dtPickupRequest.Columns.Add("payer_telephone",typeof(string));
			dtPickupRequest.Columns.Add("payer_fax",typeof(string));
			dtPickupRequest.Columns.Add("payment_mode",typeof(string));
			dtPickupRequest.Columns.Add("sender_name",typeof(string));
			dtPickupRequest.Columns.Add("sender_address1",typeof(string));
			dtPickupRequest.Columns.Add("sender_address2",typeof(string));
			dtPickupRequest.Columns.Add("sender_zipcode",typeof(string));
			dtPickupRequest.Columns.Add("sender_country",typeof(string));
			dtPickupRequest.Columns.Add("sender_contact_person",typeof(string));
			dtPickupRequest.Columns.Add("sender_telephone",typeof(string));
			dtPickupRequest.Columns.Add("sender_fax",typeof(string));
			dtPickupRequest.Columns.Add("req_pickup_datetime",typeof(string));
			dtPickupRequest.Columns.Add("act_pickup_datetime",typeof(DateTime));
			dtPickupRequest.Columns.Add("payment_type",typeof(string));
			dtPickupRequest.Columns.Add("tot_pkg",typeof(int));
			dtPickupRequest.Columns.Add("tot_wt",typeof(decimal));
			dtPickupRequest.Columns.Add("tot_dim_wt",typeof(decimal));
			dtPickupRequest.Columns.Add("cash_amount",typeof(decimal));
			dtPickupRequest.Columns.Add("cash_collected",typeof(decimal));
			dtPickupRequest.Columns.Add("special_rates",typeof(string));
			dtPickupRequest.Columns.Add("latest_status_code",typeof(string));
			dtPickupRequest.Columns.Add("latest_exception_code",typeof(string));
			dtPickupRequest.Columns.Add("latest_status_datetime",typeof(DateTime));  
			dtPickupRequest.Columns.Add("remark",typeof(string));
			dtPickupRequest.Columns.Add("quotation_no",typeof(string));
			dtPickupRequest.Columns.Add("quotation_version",typeof(int));
			dtPickupRequest.Columns.Add("pickup_route",typeof(string));
			dtPickupRequest.Columns.Add("esa_pickup_surcharge",typeof(decimal));
			dtPickupRequest.Columns.Add("location",typeof(string));
		 

			DataSet dsPickupRequest = new DataSet();
			DataRow drEach =dtPickupRequest.NewRow();
			drEach[0]="";
			
			dtPickupRequest.Rows.Add (drEach);
			dsPickupRequest.Tables.Add(dtPickupRequest);

			SessionDS sessionDS = new SessionDS();
			sessionDS.ds = dsPickupRequest;
			
			sessionDS.DataSetRecSize = dsPickupRequest.Tables[0].Rows.Count;
			sessionDS.QueryResultMaxSize = sessionDS.DataSetRecSize;
			
			return sessionDS;
		}

		public static int AddPickupRequest(String AppID,String EnterpriseID,DataSet dsPickupRequest,DataSet dsDispatch)
		{
			string strInsertQry=null;
			string strDispatch=null;
			string appid=AppID;
			string Enterpriseid = EnterpriseID;
			int iRowsEffected=0;
			IDbConnection conApp =null;
			

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(AppID,EnterpriseID);
			if(dbCon==null)
			{
				Logger.LogTraceError("PRBMgrDAL","AddPickupRequest","PRB001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			try
			{
				conApp = dbCon.GetConnection();
				dbCon.OpenConnection(ref conApp);
			}
			catch(System.ApplicationException appException)
			{	
				if (conApp!=null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("PRBMgrDAL","AddPickupRequest","PRB002","Error opening database");
				throw new ApplicationException("Connection to database failed",null);
			}
			if(conApp == null)
			{
				Logger.LogTraceError("PRBMgrDAL","AddPickupRequest","PRB001","conApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			IDbTransaction transactionApp = dbCon.BeginTransaction(conApp,IsolationLevel.ReadCommitted);
			if(transactionApp == null)
			{
				Logger.LogTraceError("PRBMgrDAL","AddPickupRequest","PRB003","transactionApp is null");
				throw new ApplicationException("Failed Begining a App Transaction..",null);
			}
			try
			{
				//Inserting into PickupRequestBooking..
				DataRow drEach = dsPickupRequest.Tables[0].Rows[0];
				strInsertQry ="Insert into Pickup_Request(applicationid,enterpriseid,booking_no,booking_datetime,booking_type,";
				strInsertQry=strInsertQry+"new_account,payerid,payer_type,payer_name,payer_address1,payer_address2,";
				strInsertQry=strInsertQry+"payer_zipcode,payer_country,payer_telephone,payer_fax,payment_mode,";
				strInsertQry=strInsertQry+"sender_name,sender_address1,sender_address2,sender_zipcode,sender_country,";
				strInsertQry=strInsertQry+"sender_contact_person,sender_telephone,sender_fax,req_pickup_datetime,";
				strInsertQry=strInsertQry+"act_pickup_datetime,payment_type,tot_pkg,tot_wt,tot_dim_wt,cash_amount,";
				strInsertQry=strInsertQry+"cash_collected,latest_status_code,latest_exception_code,latest_status_datetime,";
				strInsertQry=strInsertQry+"remark,quotation_no,quotation_version,esa_pickup_surcharge,pickup_route";
				strInsertQry=strInsertQry+") values(";
				strInsertQry=strInsertQry+"'"+appid+"',";
				strInsertQry=strInsertQry+"'"+Enterpriseid+"',";
				strInsertQry=strInsertQry+drEach["booking_no"]+",";

			
				if((drEach["booking_datetime"]!= null) && (!drEach["booking_datetime"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
			
					DateTime dtBookingDateTime = Convert.ToDateTime(drEach["booking_datetime"]);
					String strbookingdatetime = Utility.DateFormat(AppID,EnterpriseID,dtBookingDateTime,DTFormat.DateTime);
					strInsertQry=strInsertQry+strbookingdatetime+",";
				}	 
				else
				{
					strInsertQry=strInsertQry+"null"+",";
				}
				if((drEach["booking_type"]!= null) && (!drEach["booking_type"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					strInsertQry=strInsertQry+"'"+(string)drEach["booking_type"]+"',";						
				}
				else
				{
					strInsertQry=strInsertQry+"null"+",";						
				}
			
				if((drEach["new_account"]!= null) && (!drEach["new_account"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{	
					strInsertQry=strInsertQry+"'"+(string)drEach["new_account"]+"',";	
				}
				else
				{
					strInsertQry=strInsertQry+"null"+",";	
				}
			
				if((drEach["payerid"]!= null) && (!drEach["payerid"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					strInsertQry=strInsertQry+"'"+(string)drEach["payerid"]+"',";
				}
				else
				{
					strInsertQry=strInsertQry+"null" +",";
				}
				if((drEach["payer_type"]!= null) && (!drEach["payer_type"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					strInsertQry=strInsertQry+"'"+(string)drEach["payer_type"]+"',";
				}
				else
				{
					strInsertQry=strInsertQry+"null"+",";
				}
			
				if((drEach["payer_name"]!= null) && (!drEach["payer_name"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					strInsertQry=strInsertQry+"N'"+Utility.ReplaceSingleQuote(drEach["payer_name"].ToString())+"',";
				}
				else
				{
					strInsertQry=strInsertQry+"null" +",";
				}

				if((drEach["payer_address1"]!= null) && (!drEach["payer_address1"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					strInsertQry=strInsertQry+"N'"+Utility.ReplaceSingleQuote(drEach["payer_address1"].ToString())+"',";
				
				}
				else
				{
					strInsertQry=strInsertQry+"null" +",";
				}
			
				if((drEach["payer_address2"]!= null) && (!drEach["payer_address2"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					strInsertQry=strInsertQry+"N'"+Utility.ReplaceSingleQuote(drEach["payer_address2"].ToString())+"',";
				
				}
				else
				{
					strInsertQry=strInsertQry+"null" +",";
				}
			
				if((drEach["payer_zipcode"]!= null) && (!drEach["payer_zipcode"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					strInsertQry=strInsertQry+"'"+(string)drEach["payer_zipcode"]+"',";
	
				}
				else
				{
					strInsertQry=strInsertQry+"null" +",";
				}
			
				if((drEach["payer_country"]!= null) && (!drEach["payer_country"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					strInsertQry=strInsertQry+"'"+(string)drEach["payer_country"]+"',";
				}
				else
				{
					strInsertQry=strInsertQry+"null" +",";
				}
				if((drEach["payer_telephone"]!= null) && (!drEach["payer_telephone"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					strInsertQry=strInsertQry+"'"+Utility.ReplaceSingleQuote(drEach["payer_telephone"].ToString())+"',";
				
				}
				else
				{
					strInsertQry=strInsertQry+"null" +",";
				}
				if((drEach["payer_fax"]!= null) && (!drEach["payer_fax"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					strInsertQry=strInsertQry+"'"+Utility.ReplaceSingleQuote(drEach["payer_fax"].ToString())+"',";
				}
				else
				{
					strInsertQry=strInsertQry+"null" +",";
				}
			
				if((drEach["payment_mode"]!= null) && (!drEach["payment_mode"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					strInsertQry=strInsertQry+"'"+(string)drEach["payment_mode"]+"',";
				}
				else
				{
					strInsertQry=strInsertQry+"null"+",";
							
				}
				if((drEach["sender_name"]!= null) && (!drEach["sender_name"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					strInsertQry=strInsertQry+"N'"+Utility.ReplaceSingleQuote(drEach["sender_name"].ToString())+"',";
				}
				else
				{
					strInsertQry=strInsertQry+"null" +",";
				}
	
				if((drEach["sender_address1"]!= null) && (!drEach["sender_address1"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					strInsertQry=strInsertQry+"N'"+Utility.ReplaceSingleQuote(drEach["sender_address1"].ToString())+"',";
				
				}
				else
				{
					strInsertQry=strInsertQry+"null" +",";
				}
						
				if((drEach["sender_address2"]!= null) && (!drEach["sender_address2"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					strInsertQry=strInsertQry+"N'"+Utility.ReplaceSingleQuote(drEach["sender_address2"].ToString())+"',";
				}
				else
				{
					strInsertQry=strInsertQry+"null" +",";
				}
			
				if(drEach["sender_zipcode"].ToString()=="")
				{
					strInsertQry=strInsertQry+"''" +",";
					
				}
				else
				{
					strInsertQry=strInsertQry+"'"+(string)drEach["sender_zipcode"]+"',";
				}			
				if((drEach["sender_country"]!= null) && (!drEach["sender_country"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					strInsertQry=strInsertQry+"'"+(string)drEach["sender_country"]+"',";
				}
				else
				{
					strInsertQry=strInsertQry+"null" +",";
					
				}
				if((drEach["sender_contact_person"]!= null) && (!drEach["sender_contact_person"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					strInsertQry=strInsertQry+"N'"+Utility.ReplaceSingleQuote(drEach["sender_contact_person"].ToString())+"',";
		
				}
				else
				{
					strInsertQry=strInsertQry+"null" +",";
				}
			
				if((drEach["sender_telephone"]!= null) && (!drEach["sender_telephone"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					strInsertQry=strInsertQry+"'"+Utility.ReplaceSingleQuote(drEach["sender_telephone"].ToString())+"',";
				}
				else
				{
					strInsertQry=strInsertQry+"null" +",";
				
				}
			
				if((drEach["sender_fax"]!= null) && (!drEach["sender_fax"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					strInsertQry=strInsertQry+"'"+Utility.ReplaceSingleQuote(drEach["sender_fax"].ToString())+"',";
				}	
				else
				{
					strInsertQry=strInsertQry+"null" +",";
				}
			
				if((drEach["req_pickup_datetime"]!= null) && (!drEach["req_pickup_datetime"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					DateTime dtreqpickupdatetime = Convert.ToDateTime(drEach["req_pickup_datetime"]);
					String strreqpickupdatetime = Utility.DateFormat(AppID,EnterpriseID,dtreqpickupdatetime,DTFormat.DateTime);

					strInsertQry=strInsertQry+strreqpickupdatetime+",";
				}	
				else
				{
					strInsertQry=strInsertQry+"''"+",";
				}
				if((drEach["act_pickup_datetime"]!= null) && (!drEach["act_pickup_datetime"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					DateTime dtactpickupdatetime = Convert.ToDateTime(drEach["act_pickup_datetime"]);
					String stractpickupdatetime = Utility.DateFormat(AppID,EnterpriseID, dtactpickupdatetime,DTFormat.DateTime);
					strInsertQry=strInsertQry+stractpickupdatetime+",";
				}	
				else	
				{
					strInsertQry=strInsertQry+"null"+",";
				}
			
				if((drEach["payment_type"]!= null) && (!drEach["payment_type"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					strInsertQry=strInsertQry+"'"+(string)drEach["payment_type"]+"',";
				}
				else
				{
					strInsertQry=strInsertQry+"null" +",";
							
				}
				
				if((drEach["tot_pkg"]!= null) && (!drEach["tot_pkg"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					strInsertQry=strInsertQry+"'"+(int)(drEach["tot_pkg"])+"',";
				}
				else
				{
					strInsertQry=strInsertQry+"null" +",";
				}
			
				if((drEach["tot_wt"]!= null) && (!drEach["tot_wt"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					strInsertQry=strInsertQry+"'"+(decimal)(drEach["tot_wt"])+"',";
				}
				else
				{
					strInsertQry=strInsertQry+"null" +",";
				}
			
				if((drEach["tot_dim_wt"]!= null) && (!drEach["tot_dim_wt"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					strInsertQry=strInsertQry+"'"+(decimal)(drEach["tot_dim_wt"])+"',";
				}
				else
				{
					strInsertQry=strInsertQry+"null" +",";
				}
			
				if((drEach["cash_amount"]!= null) && (!drEach["cash_amount"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					strInsertQry=strInsertQry+(decimal)(drEach["cash_amount"])+",";
				}
				else
				{
					strInsertQry=strInsertQry+"null" +",";
				}
			
				if((drEach["cash_collected"]!= null) && (!drEach["cash_collected"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					strInsertQry=strInsertQry+(decimal)(drEach["cash_collected"])+",";
				}
				else
				{
					strInsertQry=strInsertQry+"null" +",";
				}
					
				
				//				if(drEach["special_rates"].ToString()=="")
				//				{
				//						strInsertQry=strInsertQry+"null" +",";
				//				}
				//					else
				//				{
				//							strInsertQry=strInsertQry+"'"+(string)drEach["special_rates"]+"',";
				//				}
				if((drEach["latest_status_code"]!= null) && (!drEach["latest_status_code"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			
				{
					strInsertQry=strInsertQry+"'"+(string)drEach["latest_status_code"]+"',";
				}
				else
				{
					strInsertQry=strInsertQry+"null" +",";
				}
			
				if((drEach["latest_exception_code"]!= null) && (!drEach["latest_exception_code"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					strInsertQry=strInsertQry+"'"+(string)drEach["latest_exception_code"]+"',";
				}
				else
				{
					strInsertQry=strInsertQry+"null" +",";
				
				}
				if((drEach["latest_status_datetime"]!= null) && (!drEach["latest_status_datetime"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					DateTime dtlatest_status_datetime = Convert.ToDateTime(drEach["latest_status_datetime"]);
					//Jeab 19 Sep 2011
					DateTime Dispatch_status_datetime = new DateTime(); 
					if(dsDispatch!=null) 
					{						
						DataRow drEachDisp = dsDispatch.Tables[0].Rows[0];
						Dispatch_status_datetime =  (DateTime)drEachDisp["tracking_datetime"];  
						if(Dispatch_status_datetime.ToString() != dtlatest_status_datetime.ToString())
						{
							dtlatest_status_datetime = Dispatch_status_datetime;
						}
					}  //Jeab 19 Sep 2011  =========> End

					String strlatest_status_datetime = Utility.DateFormat(AppID,EnterpriseID, dtlatest_status_datetime,DTFormat.DateTime);
					strInsertQry=strInsertQry+strlatest_status_datetime+",";
				}	
				else	
				{
					strInsertQry=strInsertQry+"null"+",";
				}
			
				if((drEach["remark"]!= null) && (!drEach["remark"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					strInsertQry=strInsertQry+"N'"+Utility.ReplaceSingleQuote(drEach["remark"].ToString())+"',";
				}
				else
				{
					strInsertQry=strInsertQry+"null" +",";
				}
				if((drEach["quotation_no"]!= null) && (!drEach["quotation_no"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					strInsertQry=strInsertQry+System.Convert.ToInt64(drEach["quotation_no"])+",";
				}
				else
				{
					strInsertQry=strInsertQry+"null"+",";
				}
			
				if((drEach["quotation_version"]!= null) && (!drEach["quotation_version"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					strInsertQry=strInsertQry+System.Convert.ToInt64(drEach["quotation_version"])+",";
				}
				else
				{
					strInsertQry=strInsertQry+"null"+",";
				}
				if((drEach["esa_pickup_surcharge"]!= null) && (!drEach["esa_pickup_surcharge"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					strInsertQry=strInsertQry+System.Convert.ToDecimal(drEach["esa_pickup_surcharge"])+",";
				}
				else
				{
					strInsertQry=strInsertQry+"null"+",";
				}

				if((drEach["pickup_route"]!= null) && (!drEach["pickup_route"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					strInsertQry=strInsertQry+"N'"+drEach["pickup_route"]+"')";
				}
				else
				{
					strInsertQry=strInsertQry+"null"+")";
				}	
			
				IDbCommand  dbCmd= dbCon.CreateCommand(strInsertQry,CommandType.Text);
				dbCmd.Connection = conApp;
				dbCmd.Transaction = transactionApp;
				iRowsEffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
	
				if(dsDispatch!=null)
				{
					//Modified by GwanG on 05Feb08
					//Inserting into Dispatch Table.	
					DataRow drEachDisp = dsDispatch.Tables[0].Rows[0];
					//					strDispatch ="Insert into Dispatch_Tracking(applicationid,enterpriseid,booking_no,tracking_datetime,";
					//					strDispatch=strDispatch+"status_code,deleted,last_userid,last_updated, location";
					//					strDispatch=strDispatch+")values(";
					//					strDispatch=strDispatch+"'"+appid+"',";
					//					strDispatch=strDispatch+"'"+Enterpriseid+"',";
					//					strDispatch=strDispatch+System.Convert.ToInt64(drEach["booking_no"])+",";
					//					if((drEachDisp["tracking_datetime"]!= null) && (!drEachDisp["tracking_datetime"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					//					{
					//					
					//						DateTime dtTrackingDateTime = Convert.ToDateTime(drEachDisp["tracking_datetime"]);
					//						String strTrackingdatetime = Utility.DateFormat(AppID,EnterpriseID,dtTrackingDateTime,DTFormat.DateTime);
					//						strDispatch=strDispatch+strTrackingdatetime+",";
					//					}	 
					//					else
					//					{
					//						strDispatch=strDispatch+"null"+",";
					//					}
					//					if((drEachDisp["status_code"]!= null) && (!drEachDisp["status_code"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					//					
					//					{
					//						strDispatch=strDispatch+"'"+(string)drEachDisp["status_code"]+"',";
					//					}
					//					else
					//					{
					//						strDispatch=strDispatch+"null" +",";
					//					}
					//					if((drEachDisp["deleted"]!= null) && (!drEachDisp["deleted"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					//					{
					//						strDispatch=strDispatch+"'"+(string)drEachDisp["deleted"]+"',";
					//					}
					//					else
					//					{
					//						strDispatch=strDispatch+"null" +",";
					//					}
					//					if((drEachDisp["last_userid"]!= null) && (!drEachDisp["last_userid"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					//					{
					//						strDispatch=strDispatch+"'"+(string)drEachDisp["last_userid"]+"',";
					//					}
					//					else
					//					{
					//						strDispatch=strDispatch+"null" +",";
					//					}
					//					if((drEachDisp["last_updated"]!= null) && (!drEachDisp["last_updated"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					//					{
					//					
					//						DateTime dtlast_updated = Convert.ToDateTime(drEachDisp["last_updated"]);
					//						String strlast_updated = Utility.DateFormat(AppID,EnterpriseID,dtlast_updated,DTFormat.DateTime);
					//						strDispatch=strDispatch+strlast_updated+",";
					//					}	 
					//					else
					//					{
					//						strDispatch=strDispatch+"null"+",";
					//					}
					//
					//					if((drEachDisp["location"]!= null) && (!drEachDisp["location"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					//					{
					//						strDispatch=strDispatch+"'"+(string)drEachDisp["location"]+"')";
					//					}
					//					else
					//					{
					//						strDispatch=strDispatch+"null" +")";
					//					}
					//
					//					dbCmd.CommandText=strDispatch; 
					//					iRowsEffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);

					int booking_no;
					String remark;
					DateTime booking_dt;
					String last_user;

					booking_no = Convert.ToInt32(drEach["booking_no"].ToString());
					
					if((drEachDisp["tracking_datetime"]!= null) && (!drEachDisp["tracking_datetime"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						booking_dt = (DateTime)drEachDisp["tracking_datetime"];
					}	
					else
					{
						booking_dt = new DateTime();
					}

					//					if((drEachDisp["remark"]!= null) && (!drEachDisp["remark"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					//					{
					//						remark = "N'"+Utility.ReplaceSingleQuote(drEachDisp["remark"].ToString())+"'";
					//					}
					//					else
					//					{
					//						remark = null ;
					//					}

					if((drEachDisp["last_userid"]!= null) && (!drEachDisp["last_userid"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						last_user=(string)drEachDisp["last_userid"];
					}
					else
					{
						last_user = null;
					}

					Utility.CreateAutoConsignmentHistory(AppID,EnterpriseID,1,null,booking_no,null,booking_dt,last_user,ref dbCmd,ref dbCon);
				}
				transactionApp.Commit();
				//			
			}

			catch(ApplicationException appException)
			{
				try
				{
					transactionApp.Rollback();
					Logger.LogTraceInfo("PRBMgrDAL","AddPickupRequest","PRB011","App db insert transaction rolled back.");

				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("PRBMgrDAL","AddPickupRequest","PRB012","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}


				Logger.LogTraceError("PRBMgrDAL","AddPickupRequest","PRB013","Insert Transaction Failed : "+ appException.Message.ToString());
				throw new ApplicationException(appException.Message.ToString(),appException);
			}
			catch(Exception exception)
			{
				try
				{
					transactionApp.Rollback();
					Logger.LogTraceInfo("PRBMgrDAL","AddPickupRequest","PRB014","App db insert transaction rolled back.");

				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("PRBMgrDAL","AddPickupRequest","PRB015","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}

				Logger.LogTraceError("PRBMgrDAL","AddPickupRequest","PRB016","Insert Transaction Failed : "+ exception.Message.ToString());
				throw new ApplicationException("exception.Message.ToString() ",exception);
			}
			finally
			{
				if(conApp != null)
				{
					conApp.Close();
				}
			}
			//			
			return iRowsEffected;
		}
		
		public static int AddShpDetails(String AppID,String EnterpriseID,String strBookingNo,DataSet m_dsShpmDetls,DataSet m_dsVAS,DataSet m_dsPkgDetails)
		{
			
			string appid=AppID;
			string Enterpriseid = EnterpriseID;
			int iRowsEffected=0;
			IDbConnection conApp =null;
			IDbCommand  dbCmd=null;
			int cnt=0;
			int i=0;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(AppID,EnterpriseID);
			if(dbCon==null)
			{
				Logger.LogTraceError("PRBMgrDAL","AddShpDetails","PRB001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			try
			{
				conApp = dbCon.GetConnection();
				dbCon.OpenConnection(ref conApp);
			}
			catch(System.ApplicationException appException)
			{	
				if (conApp!=null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("PRBMgrDAL","AddShpDetails","PRB002","Error opening database");
				throw new ApplicationException("Connection to database failed",null);
			}
			if(conApp == null)
			{
				Logger.LogTraceError("PRBMgrDAL","AddShpDetails","PRB001","conApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			IDbTransaction transactionApp = dbCon.BeginTransaction(conApp,IsolationLevel.ReadCommitted);
			if(transactionApp == null)
			{
				Logger.LogTraceError("PRBMgrDAL","AddShpDetails","PRB003","transactionApp is null");
				throw new ApplicationException("Failed Begining a App Transaction..",null);
			}
			try
			{
				//insert into PickupShipment
				DataRow drEachPickShp = m_dsShpmDetls.Tables[0].Rows[0];
				string strPickShp=null;
				strPickShp ="Insert into Pickup_Shipment(applicationid,enterpriseid,booking_no,serial_no,";
				strPickShp=strPickShp+"recipient_zipcode,service_code,declare_value,percent_dv_additional,";
				strPickShp=strPickShp+"est_delivery_datetime,tot_freight_charge,tot_vas_surcharge,insurance_surcharge,esa_delivery_surcharge";
				strPickShp=strPickShp+")values(";
				strPickShp=strPickShp+"'"+appid+"',";
				strPickShp=strPickShp+"'"+Enterpriseid+"',";
				strPickShp=strPickShp+strBookingNo+",";

				cnt = m_dsShpmDetls.Tables[0].Rows.Count;
				i=0;

				if((drEachPickShp["serial_no"]!= null) && (!drEachPickShp["serial_no"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					strPickShp=strPickShp+(int)(drEachPickShp["serial_no"])+",";
				}
				else
				{
					strPickShp=strPickShp+"null";
				}
				if((drEachPickShp["recipient_zipcode"]!= null) && (!drEachPickShp["recipient_zipcode"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					strPickShp=strPickShp+"'"+(drEachPickShp["recipient_zipcode"])+"',";
				}
				else
				{
					strPickShp=strPickShp+"null"+",";
				}
				if((drEachPickShp["service_code"]!= null) && (!drEachPickShp["service_code"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					strPickShp=strPickShp+"'"+(drEachPickShp["service_code"])+"',";
				}
				else
				{
					strPickShp=strPickShp+"null"+",";
				}
				if((drEachPickShp["declare_value"]!= null) && (!drEachPickShp["declare_value"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					strPickShp=strPickShp+(decimal)(drEachPickShp["declare_value"])+",";
				}
				else
				{
					strPickShp=strPickShp+"null"+",";
				}
				if((drEachPickShp["percent_dv_additional"]!= null) && (!drEachPickShp["percent_dv_additional"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					strPickShp=strPickShp+(decimal)(drEachPickShp["percent_dv_additional"])+",";
				}
				else
				{
					strPickShp=strPickShp+"null"+",";
				}
				if((drEachPickShp["est_delivery_datetime"]!= null) && (!drEachPickShp["est_delivery_datetime"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					DateTime dtest_delivery_datetime = Convert.ToDateTime(drEachPickShp["est_delivery_datetime"]);
					String strest_delivery_datetime = Utility.DateFormat(AppID,EnterpriseID, dtest_delivery_datetime,DTFormat.DateTime);
					strPickShp=strPickShp+strest_delivery_datetime+",";
				}	
				else	
				{
					strPickShp=strPickShp+"null"+",";
				}
				if((drEachPickShp["tot_freight_charge"]!= null) && (!drEachPickShp["tot_freight_charge"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					strPickShp=strPickShp+(decimal)(drEachPickShp["tot_freight_charge"])+",";
				}
				else
				{
					strPickShp=strPickShp+"null"+",";
				}
				if((drEachPickShp["tot_vas_surcharge"]!= null) && (!drEachPickShp["tot_vas_surcharge"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					strPickShp=strPickShp+(decimal)(drEachPickShp["tot_vas_surcharge"])+",";
				}
				else
				{
					strPickShp=strPickShp+"null"+",";
				}
				if((drEachPickShp["insurance_surcharge"]!= null) && (!drEachPickShp["insurance_surcharge"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					strPickShp=strPickShp+(decimal)(drEachPickShp["insurance_surcharge"])+",";
				}
				else
				{
					strPickShp=strPickShp+"null"+",";
				}
				if((drEachPickShp["esa_delivery_surcharge"]!= null) && (!drEachPickShp["esa_delivery_surcharge"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					strPickShp=strPickShp+(decimal)(drEachPickShp["esa_delivery_surcharge"])+")";
				}
				else
				{
					strPickShp=strPickShp+"null"+")";
				}
				//}
				dbCmd = dbCon.CreateCommand(strPickShp,CommandType.Text);
				dbCmd.Connection = conApp;
				dbCmd.Transaction = transactionApp;
				iRowsEffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
					
			
				if(m_dsVAS !=null)
				{
					//insert into the shipment_vas table
					cnt = m_dsVAS.Tables[0].Rows.Count;
					i = 0;
										
					for(i=0;i<cnt;i++)
					{
						StringBuilder strBuild = new StringBuilder();
						DataRow drEachVAS = m_dsVAS.Tables[0].Rows[i];
												
						String strVASCode = "";
						if(drEachVAS["vas_code"].ToString() != "")
						{
							strVASCode = (String)drEachVAS["vas_code"];
						}
						decimal decSurcharge = (decimal)drEachVAS["surcharge"];
												
						String strRemarks = "";
						if(drEachVAS["remarks"].ToString() != "")
						{
							strRemarks = (String)drEachVAS["remarks"];
						}
																	
						strBuild.Append("insert into Pickup_VAS (applicationid,enterpriseid,booking_no,serial_no,vas_code,vas_surcharge,remark)values('");
						strBuild.Append(AppID +"','");
						strBuild.Append(EnterpriseID +"',");
						strBuild.Append(strBookingNo);
						strBuild.Append(",");
						strBuild.Append(drEachPickShp["serial_no"]);
						strBuild.Append(",'");
						strBuild.Append(strVASCode+"',");
						strBuild.Append(decSurcharge+",N'");
						if(strRemarks!=null)
						{
							strBuild.Append(Utility.ReplaceSingleQuote(strRemarks)+"')");
						}
						else
						{
							strBuild.Append(null+")");
						}
						dbCmd.CommandText=strBuild.ToString();
						iRowsEffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
					}
							
				}
								
				
				if(m_dsPkgDetails !=null)
				{
					cnt = m_dsPkgDetails.Tables[0].Rows.Count;
					i = 0;
			
							
					for(i=0;i<cnt;i++)
					{
						StringBuilder strBuildpkg = new StringBuilder();
						DataRow drEachPKG = m_dsPkgDetails.Tables[0].Rows[i];
						String strMpsNo = "";
						if(drEachPKG["mps_no"].ToString() != "")
						{
							strMpsNo = (String)drEachPKG["mps_no"];
						}
											
						decimal decPkgLgth = (decimal)drEachPKG["pkg_length"];
						decimal decPkgBrdth = (decimal)drEachPKG["pkg_breadth"];
						decimal decPkgHgth = (decimal)drEachPKG["pkg_height"];
						decimal decPkgWt = (decimal)drEachPKG["pkg_wt"];
						int iPkgQty = (int)drEachPKG["pkg_qty"];
						decimal decTotalWt = (decimal)drEachPKG["tot_wt"];
						decimal decTotalDimWt = (decimal)drEachPKG["tot_dim_wt"];
						decimal decChrgbleWt =0;
						if((drEachPKG["chargeable_wt"]!= null) && (!drEachPKG["chargeable_wt"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						{
							decChrgbleWt = (decimal)drEachPKG["chargeable_wt"];
						}
													
						strBuildpkg.Append("insert into Pickup_PKG (applicationid,enterpriseid,booking_no,serial_no,mps_code,pkg_length,pkg_breadth,pkg_height,pkg_wt,pkg_qty,tot_wt,tot_dim_wt,chargeable_wt)values('");
						strBuildpkg.Append(AppID+"','");
						strBuildpkg.Append(EnterpriseID+"',");
						strBuildpkg.Append(strBookingNo);
						strBuildpkg.Append(",");
						strBuildpkg.Append(drEachPickShp["serial_no"]);
						strBuildpkg.Append(",'");
						strBuildpkg.Append(strMpsNo+"',");
						strBuildpkg.Append(decPkgLgth+",");
						strBuildpkg.Append(decPkgBrdth+",");
						strBuildpkg.Append(decPkgHgth+",");
						strBuildpkg.Append(decPkgWt+",");
						strBuildpkg.Append(iPkgQty+",");
						strBuildpkg.Append(decTotalWt+",");
						strBuildpkg.Append(decTotalDimWt+",");
						strBuildpkg.Append(decChrgbleWt+")");
						dbCmd.CommandText = strBuildpkg.ToString();
						iRowsEffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
					}
						
				}	
					
				transactionApp.Commit();
			}
			catch(ApplicationException appException)
			{
				try
				{
					transactionApp.Rollback();
					Logger.LogTraceInfo("PRBMgrDAL","AddShpDetails","PRB011","App db insert transaction rolled back.");

				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("PRBMgrDAL","AddShpDetails","PRB012",rollbackException.Message.ToString());
				}


				Logger.LogTraceError("PRBMgrDAL","AddShpDetails","PRB013","Insert Transaction Failed : "+ appException.Message.ToString());
				throw new ApplicationException(appException.Message.ToString(),appException);
			}
			catch(Exception exception)
			{
				try
				{
					transactionApp.Rollback();
					Logger.LogTraceInfo("PRBMgrDAL","AddShpDetails","PRB014","App db insert transaction rolled back.");

				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("PRBMgrDAL","AddShpDetails","PRB015","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}

				Logger.LogTraceError("PRBMgrDAL","AddShpDetails","PRB016","Insert Transaction Failed : "+ exception.Message.ToString());
				throw new ApplicationException(exception.Message.ToString(),exception);
			}
			finally
			{
				if(conApp != null)
				{
					conApp.Close();
				}
			}
			return iRowsEffected; 

		}
		public static void  DeleteShpDetails(string AppID,String EnterpriseID,string strBookingNo,int iserialno,ref ArrayList QueryList,Array ValueList)
		{
			
			string strQry= null;
			string strQryVAS=null;
			string strQryPkg=null;

			//Delete from Pickup-Shipment...
			strQry =" Delete from Pickup_Shipment where";
			strQry = strQry+" applicationid = " +"'" +AppID +"'" +" and enterpriseid = " +"'"+ EnterpriseID +"'";
			strQry = strQry+" and booking_no =" + "'" + strBookingNo + "'";
			strQry = strQry+" and serial_no =" + "'" + iserialno + "'";
				
			if(QueryList!=null)
			{
				QueryList.Add(strQry);
			}

			//Update PickupRequest...
			String strModPickup = null;
			strModPickup =strModPickup+"update Pickup_Request set tot_pkg = ";
			strModPickup=strModPickup+ ValueList.GetValue(0);
			strModPickup = strModPickup +",tot_wt =";
			strModPickup=strModPickup+ValueList.GetValue(1);
			strModPickup = strModPickup +",tot_dim_wt =";
			strModPickup=strModPickup+ValueList.GetValue(2);
			strModPickup = strModPickup +",cash_amount =";
			strModPickup=strModPickup+ValueList.GetValue(3);
			strModPickup = strModPickup +" where";
			strModPickup = strModPickup +" applicationid =";
			strModPickup = strModPickup +"'"+AppID+"'";
			strModPickup = strModPickup +" and enterpriseid =";
			strModPickup = strModPickup +"'"+EnterpriseID+"'";
			strModPickup = strModPickup +" and booking_no =";
			strModPickup = strModPickup +strBookingNo;

			if(QueryList!=null)
			{
				QueryList.Add(strModPickup);
			}
			//Delete from Pickup_Vas...
			strQryVAS =strQryVAS+"Delete from Pickup_VAS where";
			strQryVAS = strQryVAS+" applicationid = " + "'" +AppID + "'" +" and enterpriseid = " +"'"+ EnterpriseID +"'";
			strQryVAS =strQryVAS +" and booking_no =" + "'" + strBookingNo + "'";
			strQryVAS = strQryVAS+" and serial_no =" + "'" + iserialno + "'";
			
			if(QueryList!=null)
			{
				QueryList.Add(strQryVAS);
			}
		
			//Delete from Pickup_PackageDetails...													
			strQryPkg =strQryPkg+"Delete from Pickup_PKG where";
			strQryPkg = strQryPkg+" applicationid = " + "'" +AppID + "'" +" and enterpriseid = " +"'"+ EnterpriseID +"'";
			strQryPkg =strQryPkg +" and booking_no =" + "'" + strBookingNo + "'";
			strQryPkg = strQryPkg+" and serial_no =" + "'" + iserialno + "'";
			if(QueryList!=null)
			{
				QueryList.Add(strQryPkg);
			}
																	
				

		}

		public static int DeletePickupRequest(string AppID,string EnterpriseID,int BookingNo,string strStatusCode)
		{
			int iRowsAffected = 0;
			IDbTransaction TransacPickup = null;
			DbConnection  dbCon = null;
			string strQry= null;
			string strQryDisp=null;
			string strQryPickup=null;
			IDbCommand dbcmd = null;
			IDbConnection con = null;
			try
			{			
				dbCon = DbConnectionManager.GetInstance().GetDbConnection(AppID,EnterpriseID) ;
				if(dbCon==null)
				{
					Logger.LogTraceError("PRBManger","DeletePickupRequest","PRB001","dbCon is null");
					throw new ApplicationException("Connection to Database failed",null);
				}
				try
				{
					con = dbCon.GetConnection();
					dbCon.OpenConnection(ref con); 
				}
				catch(System.ApplicationException appException)
				{	
					if (con!=null)
					{
						con.Close();
					}
					Logger.LogTraceError("PRBMgrDAL","DeletePickupRequest","PRB001","Error opening database");
					throw new ApplicationException("Connection to database failed",null);
				}

				TransacPickup = dbCon.BeginTransaction(con,IsolationLevel.ReadCommitted); 	
				strQry =" Delete from shipment where";
				strQry = strQry+" applicationid = " +"'" +AppID +"'" +" and enterpriseid = " +"'"+ EnterpriseID +"'";
				strQry = strQry+" and booking_no =" + "'" + BookingNo + "'";
				dbcmd = dbCon.CreateCommand(strQry,CommandType.Text);
				dbcmd.Connection = con;
				dbcmd.Transaction = TransacPickup;
				int iRowAffected = dbCon.ExecuteNonQueryInTransaction(dbcmd);   
								
				strQryDisp =strQryDisp+"Delete from Dispatch_Tracking where";
				strQryDisp = strQryDisp+" applicationid = " + "'" +AppID + "'" +" and enterpriseid = " +"'"+ EnterpriseID +"'";
				strQryDisp =strQryDisp +" and status_code =" + "'" + strStatusCode + "'";
											
				dbcmd.CommandText = strQryDisp;
				iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbcmd);
															
				strQryPickup =strQryPickup+"Delete from Pickup_Request where";
				strQryPickup = strQryPickup+" applicationid = " + "'" +AppID + "'" +" and enterpriseid = " +"'"+ EnterpriseID +"'";
				strQryPickup =strQryPickup +" and booking_no =" + "'" + BookingNo + "'";
				strQryPickup =strQryPickup +" and latest_status_code =" + "'" +strStatusCode + "'";
																	
				dbcmd.CommandText = strQryPickup;
				iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbcmd);
				TransacPickup.Commit();
			}	
			catch(Exception exception)
			{
				try
				{
					TransacPickup.Rollback();
					Logger.LogTraceError("PRBMgrDAL","DeletePickupRequest","PRB005","Deletion Failed");
				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("PRBMgrDAL","DeletePickupRequest","PRB015","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}

				Logger.LogTraceError("PRBMgrDAL","DeletePickupRequest","PRB016","Insert Transaction Failed : "+ exception.Message.ToString());
				throw new ApplicationException(exception.Message.ToString(),exception);
			}
			finally
			{
				if (con!=null)
				{
					con.Close();
				}
			}
			return iRowsAffected;
							
		}

		public static DateTime getLastDatetimeFromDate(string strAppID, string strEnterpriseID,string sdateTime)
		{
			DateTime minDatetime = new DateTime(1980,1,1);
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon==null)
			{
				Logger.LogTraceError("PRBMgrDAL","GetPickRequestDS","PRB101","DbConnectionis null!!");
				//				return sessionDS;
			}
			StringBuilder strQry = new StringBuilder();

			strQry.Append("select min(booking_datetime) from pickup_request ");
			strQry.Append(" where ApplicationId = '");
			strQry.Append(strAppID);
			strQry.Append("' and EnterpriseId = '");
			strQry.Append(strEnterpriseID);
			if (sdateTime.Substring(sdateTime.Length-3,1) == ":")
			{
				DateTime dtime = new DateTime();
				dtime = Convert.ToDateTime(sdateTime);
				strQry.Append("' and  booking_datetime = '");
				strQry.Append(dtime.ToString("dd/MM/yyyy HH:mm") +"'");
			}
			else
			{
				strQry.Append("' and  convert(varchar(20),booking_datetime,103) = '");
				strQry.Append(sdateTime+"'");
			}
			string str = strQry.ToString();
			object tmpObj = dbCon.ExecuteScalar(str);

			if(tmpObj != System.DBNull.Value) 
				minDatetime = (DateTime)tmpObj;

			return minDatetime;
		
		}
		public static SessionDS GetPickupRequestDS(String strAppID, String strEnterpriseID, int iCurrent, int iDSRecSize,DataSet dsQueryParam)
		{
			SessionDS sessionDS = new SessionDS(); 
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			String strbooking_no=null;
			String strbooking_datetime=null;
			String strbooking_type=null;
			//String strNew_Account=null;
			String strpayerid=null;
			String strpayer_type=null;
			String strpayer_name=null;
			String strpayer_address1=null;
			String strpayer_address2=null;
			String strpayer_zipcode=null;
			String strpayer_country=null;
			String strpayer_telephone=null;
			String strpayer_fax=null;
			String strpayment_mode=null;
			String strsender_name=null;
			String strsender_address1=null;
			String strsender_address2=null;
			String strsender_zipcode=null;
			String strsender_country=null;
			String strsender_contact_person=null;
			String strsender_telephone=null;
			String strsender_fax=null;
			String strreq_pickup_datetime=null;
			String stract_pickup_datetime=null;
			String strpayment_type=null;
			String strtot_pkg=null;
			String strtot_wt=null;
			String strtot_dim_wt=null;
			String strcash_amount=null;
			String strcash_collected=null;
			//	String strSpecial_Rates=null;
			String strlatest_status_code=null;
			String strlatest_exception_code=null;
			String stresa_pickup_surcharge=null;
			String strlatest_status_datetime=null;
			String strpickup_route=null;
			
			if(dbCon==null)
			{
				Logger.LogTraceError("PRBMgrDAL","GetPickRequestDS","PRB101","DbConnectionis null!!");
				return sessionDS;
			}
			StringBuilder strQry = new StringBuilder();
			DataRow drEach = dsQueryParam.Tables[0].Rows[0];
			
			strQry.Append("select booking_no,booking_datetime,booking_type,");
			strQry.Append("new_account,payerid,payer_type,payer_name,payer_address1,payer_address2,");
			strQry.Append("payer_zipcode,payer_country,payer_telephone,payer_fax,payment_mode,");
			strQry.Append("sender_name,sender_address1,sender_address2,sender_zipcode,sender_country,");
			strQry.Append("sender_contact_person,sender_telephone,sender_fax,req_pickup_datetime,");
			strQry.Append("act_pickup_datetime,payment_type,tot_pkg,tot_wt,tot_dim_wt,cash_amount,");
			strQry.Append("cash_collected,special_rates,latest_status_code,latest_exception_code,latest_status_datetime,esa_pickup_surcharge,");
			strQry.Append("remark,quotation_no,quotation_version, pickup_route, '' as location from Pickup_Request where applicationid= '");
			strQry.Append(strAppID);
			strQry.Append("' and enterpriseid = '");
			strQry.Append(strEnterpriseID);
			strQry.Append ("' ");

			strbooking_no = drEach["booking_no"].ToString() ;
			if ((strbooking_no !=null)&&(strbooking_no !=""))
			{
				//strQry.Append(" and booking_no =");
				//strQry.Append(strbooking_no);
				strQry.Append(" and cast(booking_no as varchar(12)) like '%");
				strQry.Append(strbooking_no+"%'");
			}
			
			strbooking_datetime = drEach["booking_datetime"].ToString() ;
			if ((strbooking_datetime !=null)&&(strbooking_datetime !=""))
			{
				DateTime dtbookingdatetime = Convert.ToDateTime( drEach["booking_datetime"]);
				strQry.Append(" and booking_datetime = ");
				String strbookingdatetime = Utility.DateFormat(strAppID,strEnterpriseID,dtbookingdatetime,DTFormat.DateTime);
				//strbookingdatetime = strbookingdatetime.Replace("0001", "01");
				strbooking_datetime =strbookingdatetime ;
				strQry.Append(strbooking_datetime);
				strQry.Append (" ");
				strQry.Append (" or convert(varchar(20),booking_datetime,101) = '");
				strQry.Append(strbooking_datetime.Substring(1,strbooking_datetime.Length-7));
				strQry.Append ("' ");
			}
			strbooking_type =drEach["booking_type"].ToString();
			if ((strbooking_type !=null)&&(strbooking_type !="")&&(strbooking_type !="0"))
			{
				strQry.Append(" and booking_type = '");
				strQry.Append(strbooking_type);
				strQry.Append ("' ");
			}
			//				strNew_Account = drEach["new_account"].ToString() ;
			//				if ((strNew_Account !=null)&&(strNew_Account !=""))
			//				{
			//					strQry.Append(" and new_account = '");
			//					strQry.Append(strNew_Account);
			//					strQry.Append ("' ");
			//				}
			strpayerid =drEach["payerid"].ToString() ;
			if ((strpayerid !=null)&&(strpayerid !=""))
			{
				strQry.Append(" and payerid = '");
				strQry.Append(Utility.ReplaceSingleQuote(strpayerid));
				strQry.Append ("' ");
			}
			strpayer_type =drEach["payer_type"].ToString();
			if ((strpayer_type !=null)&&(strpayer_type !="")&&(strpayer_type !="0"))
			{
				strQry.Append(" and payer_type = '");
				strQry.Append(strpayer_type);
				strQry.Append ("' ");
			}
			strpayer_name =drEach["payer_name"].ToString();
			if ((strpayer_name !=null)&&(strpayer_name !=""))
			{
				strQry.Append(" and payer_name = N'");
				strQry.Append(Utility.ReplaceSingleQuote(strpayer_name));
				strQry.Append ("' ");
			}
			strpayer_address1 =drEach["payer_address1"].ToString();
			if ((strpayer_address1 !=null)&&(strpayer_address1 !=""))
			{
				strQry.Append(" and payer_address1 = N'");
				strQry.Append(Utility.ReplaceSingleQuote(strpayer_address1));
				strQry.Append ("' ");
			}
			strpayer_address2 =drEach["payer_address2"].ToString();
			if ((strpayer_address2 !=null)&&(strpayer_address2 !=""))
			{
				strQry.Append(" and payer_address2 = N'");
				strQry.Append(Utility.ReplaceSingleQuote(strpayer_address2));
				strQry.Append ("' ");
			}
			strpayer_zipcode =drEach["payer_zipcode"].ToString() ;
			if ((strpayer_zipcode !=null)&&(strpayer_zipcode !=""))
			{
				strQry.Append(" and payer_zipcode = '");
				strQry.Append(strpayer_zipcode);
				strQry.Append ("' ");
			}
			strpayer_country =drEach["payer_country"].ToString();
			if ((strpayer_country !=null)&&(strpayer_country !=""))
			{
				strQry.Append(" and payer_country = '");
				strQry.Append(strpayer_country);
				strQry.Append ("' ");
			}
			strpayer_telephone =drEach["payer_telephone"].ToString();
			if ((strpayer_telephone !=null)&&(strpayer_telephone !=""))
			{
				strQry.Append(" and payer_telephone = '");
				strQry.Append(Utility.ReplaceSingleQuote(strpayer_telephone));
				strQry.Append ("' ");
			}
			strpayer_fax =drEach["payer_fax"].ToString();
			if ((strpayer_fax !=null)&&(strpayer_fax !=""))
			{
				strQry.Append(" and payer_fax = '");
				strQry.Append(Utility.ReplaceSingleQuote(strpayer_fax));
				strQry.Append ("' ");
			}
			strpayment_mode =drEach["payment_mode"].ToString() ;
			if ((strpayment_mode !=null)&&(strpayment_mode !=""))
			{
				strQry.Append(" and payment_mode = '");
				strQry.Append(strpayment_mode);
				strQry.Append ("' ");
			}
			strsender_name =drEach["sender_name"].ToString();
			if ((strsender_name !=null)&&(strsender_name !=""))
			{
				strQry.Append(" and sender_name = N'");
				strQry.Append(Utility.ReplaceSingleQuote(strsender_name));
				strQry.Append ("' ");
			}
			strsender_address1 =drEach["sender_address1"].ToString();
			if ((strsender_address1 !=null)&&(strsender_address1 !=""))
			{
				strQry.Append(" and sender_address1 = N'");
				strQry.Append(Utility.ReplaceSingleQuote(strsender_address1));
				strQry.Append ("' ");
			}
			strsender_address2 =drEach["sender_address2"].ToString();
			//if ((strsender_address2 !=null)&&(strsender_address1 !="")) fix by Suthat
			if ((strsender_address2 !=null)&&(strsender_address2 !=""))
			{
				//strQry.Append(" and sender_address1 = '"); fix by Suthat
				strQry.Append(" and sender_address2 = N'");
				//strQry.Append(Utility.ReplaceSingleQuote(strsender_address1)); fix by Suthat
				strQry.Append(Utility.ReplaceSingleQuote(strsender_address2));
				strQry.Append ("' ");
			}
			strsender_zipcode =drEach["sender_zipcode"].ToString();
			if ((strsender_zipcode !=null)&&(strsender_zipcode !=""))
			{
				strQry.Append(" and sender_zipcode = '");
				strQry.Append(strsender_zipcode);
				strQry.Append ("' ");
			}
			strsender_country =drEach["sender_country"].ToString();
			if ((strsender_country !=null)&&(strsender_country !=""))
			{
				strQry.Append(" and sender_country = '");
				strQry.Append(strsender_country);
				strQry.Append ("' ");
			}
			strsender_contact_person =drEach["sender_contact_person"].ToString();
			if ((strsender_contact_person !=null)&&(strsender_contact_person !=""))
			{
				strQry.Append(" and sender_contact_person = N'");
				strQry.Append(Utility.ReplaceSingleQuote(strsender_contact_person));
				strQry.Append ("' ");
			}
			strsender_telephone =drEach["sender_telephone"].ToString();
			if ((strsender_telephone !=null)&&(strsender_telephone !=""))
			{
				strQry.Append(" and sender_telephone = '");
				strQry.Append(Utility.ReplaceSingleQuote(strsender_telephone));
				strQry.Append ("' ");
			}
			strsender_fax =drEach["sender_fax"].ToString();
			if ((strsender_fax !=null)&&(strsender_fax !=""))
			{
				strQry.Append(" and sender_fax = '");
				strQry.Append(Utility.ReplaceSingleQuote(strsender_fax));
				strQry.Append ("' ");
			}
					
			strreq_pickup_datetime = drEach["req_pickup_datetime"].ToString () ;
			if ((strreq_pickup_datetime !=null)&&(strreq_pickup_datetime !=""))
			{
				DateTime dtreqdatetime = Convert.ToDateTime(drEach["req_pickup_datetime"]);
				strQry.Append(" and req_pickup_datetime = ");
				String strreqpickdatetime = Utility.DateFormat(strAppID,strEnterpriseID,dtreqdatetime,DTFormat.DateTime);
				strreq_pickup_datetime =strreqpickdatetime ;
				strQry.Append(strreq_pickup_datetime);
				strQry.Append (" ");
			}
			stract_pickup_datetime =drEach["act_pickup_datetime"].ToString() ;
						
			if ((stract_pickup_datetime !=null)&&(stract_pickup_datetime !=""))
			{
				DateTime dtactdatetime = Convert.ToDateTime(drEach["act_pickup_datetime"]);
				String stractdatetime  = Utility.DateFormat(strAppID,strEnterpriseID,dtactdatetime ,DTFormat.DateTime);
				strQry.Append(" and act_pickup_datetime = ");
				stract_pickup_datetime=stractdatetime;
				strQry.Append(stract_pickup_datetime);
				strQry.Append (" ");
			}
			
			strpayment_type =drEach["payment_type"].ToString() ;
			if ((strpayment_type !=null)&&(strpayment_type !=""))
			{
				strQry.Append(" and payment_type = '");
				strQry.Append(strpayment_type);
				strQry.Append ("' ");
			}
			strtot_pkg =drEach["tot_pkg"].ToString(); 
			if((strtot_pkg !=null) && (strtot_pkg !=""))
			{
				strQry.Append(" and tot_pkg = ");
				strQry.Append(strtot_pkg);
								
			}
			strtot_wt=drEach["tot_wt"].ToString() ;
			if((strtot_wt !=null) && (strtot_wt !=""))
			{
				strQry.Append(" and tot_wt = ");
				strQry.Append(strtot_wt);
								
			}
			strtot_dim_wt= drEach["tot_dim_wt"].ToString() ;
			if((strtot_dim_wt !=null) && (strtot_dim_wt !=""))
			{
				strQry.Append(" and tot_dim_wt = ");
				strQry.Append(strtot_dim_wt);
			}
			strcash_amount=drEach["cash_amount"].ToString() ;
			if((strcash_amount != null) && (strcash_amount !=""))
			{
				strQry.Append(" and cash_amount = ");
				strQry.Append(strcash_amount);
			}
			strcash_collected=drEach["cash_collected"].ToString() ;
			if((strcash_collected != null) && (strcash_collected !=""))
			{
				strQry.Append(" and cash_collected = ");
				strQry.Append(strcash_collected);
			}
			//			strSpecial_Rates=drEach["special_rates"].ToString() ;
			//			if((strSpecial_Rates != null) && (strSpecial_Rates !=""))
			//			{
			//				strQry.Append(" and special_rates = '");
			//				strQry.Append(strSpecial_Rates);
			//				strQry.Append ("' ");
			//			}
			strlatest_status_code=drEach["latest_status_code"].ToString();
			if((strlatest_status_code !=null) && (strlatest_status_code !=""))
			{
				strQry.Append(" and latest_status_code = '");
				strQry.Append(strlatest_status_code);
				strQry.Append ("' ");

			}
			strlatest_exception_code=drEach["latest_exception_code"].ToString();
			if((strlatest_exception_code !=null) && (strlatest_exception_code !=""))
			{
				strQry.Append(" and latest_exception_code = '");
				strQry.Append("strlatest_exception_code");
				strQry.Append ("' ");

			}

			stresa_pickup_surcharge=drEach["esa_pickup_surcharge"].ToString();
			if((stresa_pickup_surcharge !=null) && (stresa_pickup_surcharge !=""))
			{
				strQry.Append(" and esa_pickup_surcharge = '");
				strQry.Append("stresa_pickup_surcharge");
				strQry.Append ("' ");

			}

			strpickup_route=drEach["pickup_route"].ToString();
			if((strpickup_route !=null) && (strpickup_route !=""))
			{
				strQry.Append(" and pickup_route like '%");
				strQry.Append(strpickup_route);
				strQry.Append ("%' ");

			}

			strlatest_status_datetime="null"; 
			if ((strlatest_status_datetime !=null)&&(strlatest_status_datetime !=""))
			{
				//DateTime dtlatest_status_datetime  = Convert.ToDateTime(drEach["latest_status_datetime"]);
				//String strlateststatusdatetime  = Utility.DateFormat(strAppID,strEnterpriseID,dtlatest_status_datetime  ,DTFormat.DateTime);
				strQry.Append(" or latest_status_datetime = ");
				strQry.Append(strlatest_status_datetime );
				strQry.Append (" ");
			}
			
			strQry.Append("ORDER BY booking_no desc");
			String strSQLQry = strQry.ToString();
			sessionDS = dbCon.ExecuteQuery(strSQLQry,iCurrent,iDSRecSize,"Pickup_Request");
			decimal x = sessionDS.QueryResultMaxSize; 
			return sessionDS;
		}
		public static int ModifyPickupRequest(String AppID,String EnterpriseID,DataSet dsPickupRequest)
		{
			string strModifyQry=null;
			int iRowsAffected=0;
			DbConnection dbCon = null;
			IDbCommand dbUpdateQuery = null;
			dbCon = DbConnectionManager.GetInstance().GetDbConnection(AppID,EnterpriseID);
			DataRow drEach = dsPickupRequest.Tables[0].Rows[0];
		
			strModifyQry =strModifyQry+"update Pickup_Request set booking_datetime = ";
			if((drEach["booking_datetime"]!= null) && (!drEach["booking_datetime"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				DateTime dtbookingdatetime = Convert.ToDateTime(drEach["booking_datetime"]);
				String strbookingdatetime = Utility.DateFormat(AppID,EnterpriseID,dtbookingdatetime,DTFormat.DateTime);
				strModifyQry = strModifyQry+strbookingdatetime;
			}	
			else
			{
				strModifyQry=strModifyQry+"null";
			}	
			

			strModifyQry = strModifyQry+",booking_type = ";
			if((drEach["booking_type"]!= null) && (!drEach["booking_type"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				strModifyQry = strModifyQry+"'"+drEach["booking_type"]+"'";
			}
			else
			{
				
				strModifyQry=strModifyQry+"null";
			}	
			strModifyQry = strModifyQry+",new_account = ";
			if((drEach["new_account"]!= null) && (!drEach["new_account"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				strModifyQry = strModifyQry+"'"+drEach["new_account"]+"'";
			}
			else
			{
			
				strModifyQry=strModifyQry+"''";
			}	
			
			strModifyQry = strModifyQry+",payerid =";
			if((drEach["payerid"]!= null) && (!drEach["payerid"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				strModifyQry = strModifyQry+"'"+drEach["payerid"]+"'";
			}	
			else
			{
				strModifyQry=strModifyQry+"null";
			}	
			
			strModifyQry = strModifyQry+",payer_name =";
			if((drEach["payer_name"]!= null) && (!drEach["payer_name"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				strModifyQry = strModifyQry+"N'"+Utility.ReplaceSingleQuote(drEach["payer_name"].ToString())+"'";
			}
			else
			{
				strModifyQry=strModifyQry+"null";
			}	
				
			strModifyQry = strModifyQry +",payer_type =";
			if((drEach["payer_type"]!= null) && (!drEach["payer_type"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				strModifyQry = strModifyQry+"'"+drEach["payer_type"]+"'";
			}
			else
			{
				strModifyQry=strModifyQry+"null";
			}	
			
			strModifyQry = strModifyQry +",payer_address1 =";
			if((drEach["payer_address1"]!= null) && (!drEach["payer_address1"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				strModifyQry = strModifyQry+"N'"+Utility.ReplaceSingleQuote(drEach["payer_address1"].ToString())+"'";
			}
			else
			{
				strModifyQry=strModifyQry+"null";
			}	
			
			strModifyQry = strModifyQry +",payer_address2 =";
			if((drEach["payer_address2"]!= null) && (!drEach["payer_address2"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				strModifyQry = strModifyQry+"N'"+Utility.ReplaceSingleQuote(drEach["payer_address2"].ToString())+"'";
			}
			else
			{
				strModifyQry=strModifyQry+"null";
			}	
			
			strModifyQry = strModifyQry +",payer_zipcode =";
			if((drEach["payer_zipcode"]!= null) && (!drEach["payer_zipcode"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				strModifyQry = strModifyQry+"'"+drEach["payer_zipcode"]+"'";
			}	
			else
			{
				strModifyQry=strModifyQry+"null";
			}	

			strModifyQry = strModifyQry +",payer_country =";
			if((drEach["payer_country"]!= null) && (!drEach["payer_country"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				strModifyQry = strModifyQry+"'"+drEach["payer_country"]+"'";
			}
			else
			{
				strModifyQry=strModifyQry+"null";
			}	

			strModifyQry = strModifyQry +",payer_telephone =";
			if((drEach["payer_telephone"]!= null) && (!drEach["payer_telephone"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				strModifyQry = strModifyQry+"'"+Utility.ReplaceSingleQuote(drEach["payer_telephone"].ToString())+"'";
			}
			else
			{
				strModifyQry=strModifyQry+"null";
			}	
			strModifyQry = strModifyQry +",payer_fax =";
			if((drEach["payer_fax"]!= null) && (!drEach["payer_fax"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				strModifyQry = strModifyQry+"'"+Utility.ReplaceSingleQuote(drEach["payer_fax"].ToString())+"'";
			}	
			else
			{
				strModifyQry=strModifyQry+"null";
			}	

			strModifyQry = strModifyQry +",payment_mode =";
			if((drEach["payment_mode"]!= null) && (!drEach["payment_mode"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				strModifyQry = strModifyQry+"'"+drEach["payment_mode"]+"'";
			}
			else
			{
				strModifyQry=strModifyQry+"null";
			}	

			strModifyQry = strModifyQry +",sender_name =";
			if((drEach["sender_name"]!= null) && (!drEach["sender_name"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				strModifyQry = strModifyQry+"N'"+Utility.ReplaceSingleQuote(drEach["sender_name"].ToString())+"'";
			}
			else
			{
				strModifyQry=strModifyQry+"null";
			}	
			strModifyQry = strModifyQry +",sender_address1 =";
			if((drEach["sender_address1"]!= null) && (!drEach["sender_address1"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				strModifyQry = strModifyQry+"N'"+Utility.ReplaceSingleQuote(drEach["sender_address1"].ToString())+"'";
			}
			else
			{
				strModifyQry=strModifyQry+"null";
			}	

			strModifyQry = strModifyQry +",sender_address2 =";
			if((drEach["sender_address2"]!= null) && (!drEach["sender_address2"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				strModifyQry = strModifyQry+"N'"+Utility.ReplaceSingleQuote(drEach["sender_address2"].ToString())+"'";
			}
			else
			{
				strModifyQry=strModifyQry+"null";
			}	

			strModifyQry = strModifyQry +",sender_zipcode =";
			if((drEach["sender_zipcode"]!= null) && (!drEach["sender_zipcode"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				strModifyQry = strModifyQry+"'"+drEach["sender_zipcode"]+"'";
			}
			else
			{
				strModifyQry=strModifyQry+"null";
			}	
			strModifyQry = strModifyQry +",sender_country =";
			if((drEach["sender_country"]!= null) && (!drEach["sender_country"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				strModifyQry = strModifyQry+"'"+drEach["sender_country"]+"'";
				
			}	
			else
			{
				strModifyQry=strModifyQry+"null";
			}	
			strModifyQry = strModifyQry +",sender_contact_person =";
			if((drEach["sender_contact_person"]!= null) && (!drEach["sender_contact_person"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				strModifyQry = strModifyQry+"N'"+Utility.ReplaceSingleQuote(drEach["sender_contact_person"].ToString())+"'";
			}
			else
			{
				strModifyQry=strModifyQry+"null";
			}	


			strModifyQry = strModifyQry +",sender_telephone =";
			if((drEach["sender_telephone"]!= null) && (!drEach["sender_telephone"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				strModifyQry = strModifyQry+"'"+Utility.ReplaceSingleQuote(drEach["sender_telephone"].ToString())+"'";
			}
			else
			{
				strModifyQry=strModifyQry+"null";
			}	
		
			strModifyQry = strModifyQry +",sender_fax =";
			if((drEach["sender_fax"]!= null) && (!drEach["sender_fax"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				strModifyQry = strModifyQry+"'"+Utility.ReplaceSingleQuote(drEach["sender_fax"].ToString())+"'";
			}
			else
			{
				strModifyQry=strModifyQry+"null";
			}
			strModifyQry = strModifyQry +",req_pickup_datetime =";

			if((drEach["req_pickup_datetime"]!= null) && (!drEach["req_pickup_datetime"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				DateTime dtreqpickupdatetime = Convert.ToDateTime(drEach["req_pickup_datetime"]);
				String strreqpickupdatetime = Utility.DateFormat(AppID,EnterpriseID,dtreqpickupdatetime,DTFormat.DateTime);
				strModifyQry = strModifyQry+strreqpickupdatetime;

			}
			else
			{
				strModifyQry=strModifyQry+"null";
			}	
		
			strModifyQry = strModifyQry +",act_pickup_datetime =";
			if((drEach["act_pickup_datetime"]!= null) && (!drEach["act_pickup_datetime"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				DateTime dtactpickupdatetime = Convert.ToDateTime(drEach["act_pickup_datetime"]);
				String stractpickupdatetime = Utility.DateFormat(AppID,EnterpriseID,dtactpickupdatetime,DTFormat.DateTime);
				strModifyQry = strModifyQry+stractpickupdatetime;
			}
			else
			{
				strModifyQry=strModifyQry+"null";
			}
			strModifyQry = strModifyQry +",payment_type =";
			if((drEach["payment_type"]!= null) && (!drEach["payment_type"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				strModifyQry = strModifyQry+"'"+drEach["payment_type"]+"'";
			}	
			else
			{
				strModifyQry=strModifyQry+"null";
			}
			strModifyQry = strModifyQry +",tot_pkg =";
			if((drEach["tot_pkg"]!= null) && (!drEach["tot_pkg"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				strModifyQry=strModifyQry+"'"+System.Convert.ToInt64(drEach["tot_pkg"])+"'";
			}
			else
			{
				strModifyQry=strModifyQry+"null" ;
			}
	
			strModifyQry = strModifyQry +",tot_wt =";
			if((drEach["tot_wt"]!= null) && (!drEach["tot_wt"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				strModifyQry=strModifyQry+"'"+System.Convert.ToDecimal(drEach["tot_wt"])+"'";
			}		
			else
			{
				strModifyQry=strModifyQry+"null" ;
			}
		
			
			strModifyQry = strModifyQry +",tot_dim_wt =";
			if((drEach["tot_dim_wt"]!= null) && (!drEach["tot_dim_wt"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				strModifyQry=strModifyQry+"'"+System.Convert.ToDecimal(drEach["tot_dim_wt"])+"'";
			}
			else
			{
				strModifyQry=strModifyQry+"null" ;
			}
		
			strModifyQry = strModifyQry +",cash_amount =";
			if((drEach["cash_amount"]!= null) && (!drEach["cash_amount"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				strModifyQry=strModifyQry+System.Convert.ToDecimal(drEach["cash_amount"]);
			}
			else
			{
				strModifyQry=strModifyQry+"null" ;
			}
		
		
			strModifyQry = strModifyQry +",cash_collected =";
			if((drEach["cash_collected"]!= null) && (!drEach["cash_collected"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				strModifyQry=strModifyQry+System.Convert.ToDecimal(drEach["cash_collected"]);
			}	
			else
			{
				strModifyQry=strModifyQry+"null" ;
			}
		
			strModifyQry = strModifyQry +",latest_status_code =";
			if((drEach["latest_status_code"]!= null) && (!drEach["latest_status_code"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				strModifyQry=strModifyQry+"'"+(string)drEach["latest_status_code"]+"'";
			}
			else
			{
				strModifyQry=strModifyQry+"null" ;
			}
			

			strModifyQry = strModifyQry +",latest_exception_code =";
			if((drEach["latest_exception_code"]!= null) && (!drEach["latest_exception_code"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				strModifyQry=strModifyQry+"'"+(string)drEach["latest_exception_code"]+"'";
			}
			else
			{
				strModifyQry=strModifyQry+"null" ;
			
			}
		
			strModifyQry = strModifyQry +",remark =";
			if((drEach["remark"]!= null) && (!drEach["remark"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				strModifyQry=strModifyQry+"N'"+Utility.ReplaceSingleQuote(drEach["remark"].ToString())+"'";
			}
			else
			{
				strModifyQry=strModifyQry+"null" ;
			
			}
		
			strModifyQry = strModifyQry +",esa_pickup_surcharge =";
			if((drEach["esa_pickup_surcharge"]!= null) && (!drEach["esa_pickup_surcharge"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				strModifyQry=strModifyQry+System.Convert.ToDecimal(drEach["esa_pickup_surcharge"]);
			}
			else
			{
				strModifyQry=strModifyQry+"null" ;
			}
			strModifyQry = strModifyQry +",pickup_route =";
			if((drEach["pickup_route"]!= null) && (!drEach["pickup_route"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				strModifyQry=strModifyQry+"N'"+Utility.ReplaceSingleQuote(drEach["pickup_route"].ToString())+"'";
			}
			else
			{
				strModifyQry=strModifyQry+"null" ;
			}

			strModifyQry = strModifyQry +" where";
			strModifyQry = strModifyQry +" applicationid =";
			strModifyQry = strModifyQry +"'"+AppID+"'";
			strModifyQry = strModifyQry +" and enterpriseid =";
			strModifyQry = strModifyQry +"'"+EnterpriseID+"'";
			strModifyQry = strModifyQry +" and booking_no =";
			strModifyQry = strModifyQry +drEach["booking_no"];

			//Update Dispatch_Tracking
			//Modified by GwanG on 05Feb08
			String strUDPT;
			//			int booking_no;
			//			String remark;
			//			DateTime booking_dt;
			//
			//			booking_no = (int)drEach["booking_no"];
			//			
			//			if((drEach["booking_datetime"]!= null) && (!drEach["booking_datetime"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			//			{
			//				booking_dt = Convert.ToDateTime(drEach["booking_datetime"]);
			//			}	
			//			else
			//			{
			//				booking_dt = new DateTime();
			//			}
			//
			//			if((drEach["remark"]!= null) && (!drEach["remark"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			//			{
			//				remark = "N'"+Utility.ReplaceSingleQuote(drEach["remark"].ToString())+"'";
			//			}
			//			else
			//			{
			//				remark = null ;
			//			
			//			}



			if((drEach["location"]!= null) && (!drEach["location"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				strUDPT = " UPDATE Dispatch_Tracking  SET location='" + drEach["location"] + "'";
			else
				strUDPT = " UPDATE Dispatch_Tracking  SET location=null ";

			strUDPT = strUDPT +" where";
			strUDPT = strUDPT +" applicationid =";
			strUDPT = strUDPT +"'"+AppID+"'";
			strUDPT = strUDPT +" and enterpriseid =";
			strUDPT = strUDPT +"'"+EnterpriseID+"'";
			strUDPT = strUDPT +" and booking_no =";
			strUDPT = strUDPT +drEach["booking_no"];
			strUDPT = strUDPT + " and status_code='DISP' ";
				

			try
			{

				dbUpdateQuery = dbCon.CreateCommand(strModifyQry,CommandType.Text);
				dbCon.ExecuteNonQuery(dbUpdateQuery);

				dbUpdateQuery = dbCon.CreateCommand(strUDPT,CommandType.Text);
				dbCon.ExecuteNonQuery(dbUpdateQuery);
				//			Utility.CreateAutoConsignmentHistory(AppID,EnterpriseID,1,remark,booking_no,"",booking_dt,null,ref dbUpdateQuery,ref dbCon);


					
			}
			catch(System.Exception exception)
			{ 
				Logger.LogTraceError("PRBMgrDAL","ModifyPickupRequest","PRB008","Modification Failed");
				throw new ApplicationException("exception.message()",exception);
			}
			return iRowsAffected;
		}		
		
		/*	public static int UpdatePickupRequestFromShipment(String AppID,String EnterpriseID,String strBookingNo,int iQty,decimal decTot_wt,decimal decTot_dim_wt,decimal decCashAmt )
			{
				string strModifyQry=null;
				int iRowsAffected=0;
				DbConnection dbCon = null;
				IDbCommand dbUpdateQuery = null;
				dbCon = DbConnectionManager.GetInstance().GetDbConnection(AppID,EnterpriseID);
				//DataRow drEach = dsPickupRequest.Tables[0].Rows[0];
		
				strModifyQry =strModifyQry+"update Pickup_Request set tot_pkg = ";
				strModifyQry=strModifyQry+iQty;
				strModifyQry = strModifyQry +",tot_wt =";
				strModifyQry=strModifyQry+decTot_wt;
				strModifyQry = strModifyQry +",tot_dim_wt =";
				strModifyQry=strModifyQry+decTot_dim_wt;
				strModifyQry = strModifyQry +",cash_amount =";
				strModifyQry=strModifyQry+decCashAmt;

				strModifyQry = strModifyQry +" where";
				strModifyQry = strModifyQry +" applicationid =";
				strModifyQry = strModifyQry +"'"+AppID+"'";
				strModifyQry = strModifyQry +" and enterpriseid =";
				strModifyQry = strModifyQry +"'"+EnterpriseID+"'";
				strModifyQry = strModifyQry +" and booking_no =";
				strModifyQry = strModifyQry +strBookingNo;
				try
				{
					dbUpdateQuery = dbCon.CreateCommand(strModifyQry,CommandType.Text);
					dbCon.ExecuteNonQuery(dbUpdateQuery);
					
				}
				catch(System.Exception exception)
				{ 
					Logger.LogTraceError("PRBMgrDAL","ModifyPickupRequest","PRB008","Modification Failed");
					throw new ApplicationException("exception.message()",exception);
				}
				return iRowsAffected;
			}		
			*/
		public static DataSet GetCashCustomer(String strAppID,String strEnterpriseID)
		{
			string strCustomer=null;
			string strCustid="CASHCUSTOMER";
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon==null)
			{
				Logger.LogTraceError("PickupRequest","GetFromCustomer","PRBMGR09","DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null",null);
			}
			try
			{
				strCustomer ="select * from Customer where custid="+"'"+strCustid+"'";
				DataSet  dsCustomer =(DataSet)dbCon.ExecuteQuery(strCustomer,ReturnType.DataSetType); 
				return dsCustomer;		
			}
			catch
			{
				Logger.LogTraceInfo("PRBMgrDAL","GetCustomer","PRBMGR08","Select command failed");
				throw new ApplicationException("Select command failed");
			}
		}
		public static DataSet GetEmptyDispatchTracking(String strAppID,string strEnterpriseID)
		{
			DataTable  dtDispatch = new DataTable();
 
			dtDispatch.Columns.Add("booking_no",typeof(string));
			dtDispatch.Columns.Add("tracking_datetime",typeof(DateTime));
			dtDispatch.Columns.Add("status_code",typeof(string));
			dtDispatch.Columns.Add("deleted",typeof(string));
			dtDispatch.Columns.Add("last_userid",typeof(string));
			dtDispatch.Columns.Add("last_updated",typeof(DateTime));
			dtDispatch.Columns.Add("location",typeof(string));

  

			DataSet dsDispatch =  new DataSet ();
			DataRow drEach =  dtDispatch.NewRow();
			drEach[0]="";
			dtDispatch.Rows.Add(drEach);
			dsDispatch.Tables.Add(dtDispatch);

			//			SessionDS sessionds = new SessionDS ();
			//			sessionds.ds = dsDispatch;
			//			sessionds.DataSetRecSize = dsDispatch.Tables[0].Rows.Count;
			//			sessionds.QueryResultMaxSize =sessionDS.DataSetRecSize;

			return dsDispatch;
		}
		public static DataSet GetEmptyPickupRequestShipment()
		{
			DataTable dtPickupShipment = new DataTable();
			dtPickupShipment.Columns.Add("booking_no",typeof(string));
			dtPickupShipment.Columns.Add("serial_no",typeof(int));
			dtPickupShipment.Columns.Add("recipient_zipcode",typeof(string));
			dtPickupShipment.Columns.Add ("service_code",typeof(string)); 
			dtPickupShipment.Columns.Add("declare_value",typeof(decimal));  
			dtPickupShipment.Columns.Add("percent_dv_additional",typeof(decimal));
			dtPickupShipment.Columns.Add("est_delivery_datetime",typeof(DateTime));
			dtPickupShipment.Columns.Add("tot_freight_charge",typeof(decimal));  
			dtPickupShipment.Columns.Add("tot_vas_surcharge",typeof(decimal));
			dtPickupShipment.Columns.Add("insurance_surcharge",typeof(decimal));
			dtPickupShipment.Columns.Add("esa_delivery_surcharge",typeof(decimal));

			DataSet dsPickupShipment = new DataSet();
			DataRow drEach = dtPickupShipment.NewRow();
			drEach[0]="";
			dtPickupShipment.Rows.Add(drEach);
			dsPickupShipment.Tables.Add(dtPickupShipment);
			return dsPickupShipment; 
		}

		public static DataSet GetEmptyPkgDtlDS()
		{
			DataTable dtPkgDtl = new DataTable("PkgDtl");
			dtPkgDtl.Columns.Add(new DataColumn("serial_no", typeof(String)));
			dtPkgDtl.Columns.Add(new DataColumn("mps_no", typeof(String)));
			dtPkgDtl.Columns.Add(new DataColumn("pkg_length",typeof(decimal)));
			dtPkgDtl.Columns.Add(new DataColumn("pkg_breadth",typeof(decimal)));
			dtPkgDtl.Columns.Add(new DataColumn("pkg_height",typeof(decimal)));
			dtPkgDtl.Columns.Add(new DataColumn("pkg_volume",typeof(decimal)));
			dtPkgDtl.Columns.Add(new DataColumn("pkg_wt",typeof(decimal)));
			dtPkgDtl.Columns.Add(new DataColumn("pkg_qty",typeof(int)));
			dtPkgDtl.Columns.Add(new DataColumn("tot_wt",typeof(decimal)));
			dtPkgDtl.Columns.Add(new DataColumn("tot_dim_wt",typeof(decimal)));
			dtPkgDtl.Columns.Add(new DataColumn("chargeable_wt",typeof(decimal)));

			DataSet dsPkgDetails = new DataSet();
			dsPkgDetails.Tables.Add(dtPkgDtl);
			dsPkgDetails.Tables["PkgDtl"].Columns["mps_no"].Unique = true;
			dsPkgDetails.Tables["PkgDtl"].Columns["mps_no"].AllowDBNull = false;			
			return  dsPkgDetails;
		}
		public static void AddNewRowInPkgDS(DataSet sessionDS)
		{
			DataRow drNew = sessionDS.Tables[0].NewRow();

			drNew[0] = "";
			drNew[1] = 0;
			drNew[2] = 0;
			drNew[3] = 0;
			drNew[4] = 0;
			drNew[5] = 0;
			drNew[6] = 0;
			drNew[7] = 0;
			drNew[8] = 0;
			drNew[9]=  0;
			try
			{
				sessionDS.Tables[0].Rows.Add(drNew);
			}
			catch(Exception ex)
			{
				Console.WriteLine(ex.Message);
			}

		}
		public static DataSet GetEmptyVASDS()
		{
			DataTable dtVAS = new DataTable("VAS");
			dtVAS.Columns.Add(new DataColumn("serial_no", typeof(string)));
			dtVAS.Columns.Add(new DataColumn("vas_code", typeof(string)));
			dtVAS.Columns.Add(new DataColumn("vas_description",typeof(string)));
			dtVAS.Columns.Add(new DataColumn("surcharge",typeof(decimal)));
			dtVAS.Columns.Add(new DataColumn("remarks",typeof(string)));
			DataSet dsVAS = new DataSet();
			dsVAS.Tables.Add(dtVAS);
			dsVAS.Tables["VAS"].Columns["vas_code"].Unique = true;
			dsVAS.Tables["VAS"].Columns["vas_code"].AllowDBNull = false;
			
			return  dsVAS;
		}
		public static void AddNewRowInVAS(DataSet dsVAS)
		{
			DataRow drNew = dsVAS.Tables[0].NewRow();
			drNew[0] = "";
			drNew[1] = "";
			drNew[2] = 0;
			//drNew[3] = 0;
			//drNew[4]=  "";
			try
			{
				dsVAS.Tables[0].Rows.Add(drNew);
			}
			catch(Exception ex)
			{
				System.Console.Write(ex.Message);
			}
		}

		public static DataSet GetVASData(String appID, String enterpriseID, string bookingNo,int iserialno)
		{
			DbConnection dbCon= null;
			DataSet dsVAS =null;
			IDbCommand dbcmd=null;
			DataSet dsVASData = null;

			dbCon = DbConnectionManager.GetInstance().GetDbConnection(appID,enterpriseID);

			if (dbCon==null)
			{
				Logger.LogTraceError("PRBMgrDAL.cs","GetVASData","ERR001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			StringBuilder strQry =  new StringBuilder(); 
			strQry = strQry.Append("select a.serial_no,a.vas_code,a.vas_surcharge,a.remark,b.vas_description from Pickup_VAS a, vas b");
			strQry.Append(" where a.applicationid = '");
			strQry.Append(appID);
			strQry.Append ("' and a.enterpriseid = '");
			strQry.Append (enterpriseID);
			strQry.Append("' and a.serial_no = ");
			strQry.Append(iserialno);
			strQry.Append(" and a.booking_no = ");
			strQry.Append(bookingNo);
			strQry.Append(" and b.applicationid = '");
			strQry.Append(appID);
			strQry.Append ("' and b.enterpriseid = '");
			strQry.Append (enterpriseID);
			strQry.Append("' and b.vas_code = a.vas_code");
		
			dbcmd = dbCon.CreateCommand(strQry.ToString(),CommandType.Text);
 
			try
			{
				dsVAS = (DataSet)dbCon.ExecuteQuery(dbcmd,com.common.DAL.ReturnType.DataSetType);  
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("PRBMgr.cs","GetVASData","ERR002","Error in Query String "+appException.Message);
				throw appException;
			}
			
			dsVASData = GetEmptyVASDS();

			if(dsVAS.Tables[0].Rows.Count > 0)
			{
				int i = 0;
				for(i = 0;i<dsVAS.Tables[0].Rows.Count;i++)
				{
					DataRow drVAS = dsVAS.Tables[0].Rows[i];
					AddNewRowInVAS(dsVASData);
					DataRow drEach = dsVASData.Tables[0].Rows[i];
					if(((drVAS["vas_code"]!= null) && (!drVAS["vas_code"].GetType().Equals(System.Type.GetType("System.DBNull")))))
					{
						drEach["vas_code"] = drVAS["vas_code"];
					}
					if(((drVAS["vas_description"]!= null) && (!drVAS["vas_description"].GetType().Equals(System.Type.GetType("System.DBNull")))))
					{
						drEach["vas_description"] = drVAS["vas_description"];
					}
					if(((drVAS["vas_surcharge"]!= null) && (!drVAS["vas_surcharge"].GetType().Equals(System.Type.GetType("System.DBNull")))))
					{
						drEach["surcharge"] = drVAS["vas_surcharge"];
					}
					if(((drVAS["remark"]!= null) && (!drVAS["remark"].GetType().Equals(System.Type.GetType("System.DBNull")))))
					{
						drEach["remarks"] = drVAS["remark"];
					}
				}
			}
			return dsVASData;
		}
		public static DataSet GetPkgData(String appID, String enterpriseID,string  bookingNo,int iserialno)
		{
			DbConnection dbCon= null;
			DataSet dsPkg =null;
			IDbCommand dbcmd=null;
			DataSet dsPkgData = null;

			dbCon = DbConnectionManager.GetInstance().GetDbConnection(appID,enterpriseID);

			if (dbCon==null)
			{
				Logger.LogTraceError("DomesticShipmentMgrDAL.cs","GetVASData","ERR001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			StringBuilder strQry =  new StringBuilder(); 
			strQry = strQry.Append("select * from Pickup_PKG");
			strQry.Append(" where applicationid = '");
			strQry.Append(appID);
			strQry.Append ("' and enterpriseid = '");
			strQry.Append (enterpriseID);
			strQry.Append("' and booking_no = ");
			strQry.Append(bookingNo);
			strQry.Append(" and serial_no = ");
			strQry.Append(iserialno);
			
		
			dbcmd = dbCon.CreateCommand(strQry.ToString(),CommandType.Text);
 
			try
			{
				dsPkg = (DataSet)dbCon.ExecuteQuery(dbcmd,com.common.DAL.ReturnType.DataSetType);  
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("PRBMgr.cs","GetPkgData","ERR002","Error in Query String "+appException.Message);
				throw appException;
			}
			
			dsPkgData = GetEmptyPkgDtlDS();

			if(dsPkg.Tables[0].Rows.Count > 0)
			{
				int i = 0;
				for(i = 0;i<dsPkg.Tables[0].Rows.Count;i++)
				{
					DataRow drVAS = dsPkg.Tables[0].Rows[i];
					AddNewRowInPkgDS(dsPkgData);
					DataRow drEach = dsPkgData.Tables[0].Rows[i];
					
					if(((drVAS["mps_code"]!= null) && (!drVAS["mps_code"].GetType().Equals(System.Type.GetType("System.DBNull")))))
					{
						drEach["mps_no"] = drVAS["mps_code"];
					}
					if(((drVAS["pkg_length"]!= null) && (!drVAS["pkg_length"].GetType().Equals(System.Type.GetType("System.DBNull")))))
					{
						drEach["pkg_length"] = drVAS["pkg_length"];
					}
					if(((drVAS["pkg_breadth"]!= null) && (!drVAS["pkg_breadth"].GetType().Equals(System.Type.GetType("System.DBNull")))))
					{
						drEach["pkg_breadth"] = drVAS["pkg_breadth"];
					}
					if(((drVAS["pkg_height"]!= null) && (!drVAS["pkg_height"].GetType().Equals(System.Type.GetType("System.DBNull")))))
					{
						drEach["pkg_height"] = drVAS["pkg_height"];
					}
					if(((drVAS["pkg_wt"]!= null) && (!drVAS["pkg_wt"].GetType().Equals(System.Type.GetType("System.DBNull")))))
					{
						drEach["pkg_wt"]=drVAS["pkg_wt"];
					}
					if(((drVAS["pkg_qty"]!= null) && (!drVAS["pkg_qty"].GetType().Equals(System.Type.GetType("System.DBNull")))))
					{
						drEach["pkg_qty"] = drVAS["pkg_qty"];
					}
					if(((drVAS["tot_wt"]!= null) && (!drVAS["tot_wt"].GetType().Equals(System.Type.GetType("System.DBNull")))))
					{
						drEach["tot_wt"] = drVAS["tot_wt"];
					}
					if(((drVAS["tot_dim_wt"]!= null) && (!drVAS["tot_dim_wt"].GetType().Equals(System.Type.GetType("System.DBNull")))))
					{
						drEach["tot_dim_wt"] = drVAS["tot_dim_wt"];
					}
					if(((drVAS["chargeable_wt"]!= null) && (!drVAS["chargeable_wt"].GetType().Equals(System.Type.GetType("System.DBNull")))))
					{
						drEach["chargeable_wt"] = drVAS["chargeable_wt"];
					}
					if(((drEach["pkg_length"]!= null) && (!drEach["pkg_length"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						&& ((drEach["pkg_breadth"]!= null) && (!drEach["pkg_breadth"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						&& ((drEach["pkg_height"]!= null) && (!drEach["pkg_height"].GetType().Equals(System.Type.GetType("System.DBNull")))))
					{
						drEach["pkg_volume"] = Convert.ToDecimal(drVAS["pkg_length"])*Convert.ToDecimal(drVAS["pkg_breadth"])*Convert.ToDecimal(drVAS["pkg_height"]);
					}
				}
			}
			return dsPkgData;
		}
		public static DataSet GetPikupShp(String appID, String enterpriseID, int bookingNo)
		{
			DbConnection dbCon= null;
			DataSet dsPikupShp =null;
			IDbCommand dbcmd=null;
			dbCon = DbConnectionManager.GetInstance().GetDbConnection(appID,enterpriseID);

			if (dbCon==null)
			{
				Logger.LogTraceError("PRBMgrDAL.cs","GetPikupShp","ERR001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			StringBuilder strQry =  new StringBuilder(); 
			strQry = strQry.Append("select * from Pickup_Shipment");
			strQry.Append(" where applicationid = '");
			strQry.Append(appID);
			strQry.Append ("' and enterpriseid = '");
			strQry.Append (enterpriseID);
			strQry.Append("' and booking_no = ");
			strQry.Append(bookingNo);
			strQry.Append("order by booking_no"); 
				
			dbcmd = dbCon.CreateCommand(strQry.ToString(),CommandType.Text);
 
			try
			{
				dsPikupShp = (DataSet)dbCon.ExecuteQuery(dbcmd,com.common.DAL.ReturnType.DataSetType);  
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("PRBMgr.cs","GetPkgData","ERR002","Error in Query String "+appException.Message);
				throw appException;
			}
			
			//			dsPikupShpData = GetEmptyPickupRequestShipment();
			//			dsPikupShpData .Tables[0].Rows[0].Delete(); 
			//			int cnt = dsPikupShp.Tables[0].Rows.Count-1;
			//			if (cnt > -1)
			//			{
			//						
			//				if(dsPikupShp.Tables[0].Rows.Count > 0)
			//				{
			//					int i = 0;
			//					for(i = 0;i<dsPikupShp.Tables[0].Rows.Count;i++)
			//					{
			//						DataRow drPkg = dsPikupShp.Tables[0].Rows[i];
			//							
			//						DataRow drEach = dsPikupShpData.Tables[0].NewRow();
			//						if(((drPkg["serial_no"]!= null) && (!drPkg["serial_no"].GetType().Equals(System.Type.GetType("System.DBNull")))))
			//						{
			//							drEach["serial_no"] = drPkg["serial_no"];
			//						}
			//						if(((drPkg["recipient_zipcode"]!= null) && (!drPkg["recipient_zipcode"].GetType().Equals(System.Type.GetType("System.DBNull")))))
			//						{
			//							drEach["recipient_zipcode"] = drPkg["recipient_zipcode"];
			//						}
			//						if(((drPkg["service_code"]!= null) && (!drPkg["service_code"].GetType().Equals(System.Type.GetType("System.DBNull")))))
			//						{
			//							drEach["service_code"] = drPkg["service_code"];
			//						}
			//						if(((drPkg["declare_value"]!= null) && (!drPkg["declare_value"].GetType().Equals(System.Type.GetType("System.DBNull")))))
			//						{
			//							drEach["declare_value"] = drPkg["declare_value"];
			//						}
			//						if(((drPkg["percent_dv_additional"]!= null) && (!drPkg["percent_dv_additional"].GetType().Equals(System.Type.GetType("System.DBNull")))))
			//						{
			//							drEach["percent_dv_additional"] = drPkg["percent_dv_additional"];
			//						}
			//						if(((drPkg["est_delivery_datetime"]!= null) && (!drPkg["est_delivery_datetime"].GetType().Equals(System.Type.GetType("System.DBNull")))))
			//						{
			//							drEach["est_delivery_datetime"] = drPkg["est_delivery_datetime"];
			//						}
			//						if(((drPkg["tot_freight_charge"]!= null) && (!drPkg["tot_freight_charge"].GetType().Equals(System.Type.GetType("System.DBNull")))))
			//						{
			//							drEach["tot_freight_charge"] = drPkg["tot_freight_charge"];
			//						}
			//						if(((drPkg["tot_vas_surcharge"]!= null) && (!drPkg["tot_vas_surcharge"].GetType().Equals(System.Type.GetType("System.DBNull")))))
			//						{
			//							drEach["tot_vas_surcharge"] = drPkg["tot_vas_surcharge"];
			//						}
			//						if(((drPkg["insurance_surcharge"]!= null) && (!drPkg["insurance_surcharge"].GetType().Equals(System.Type.GetType("System.DBNull")))))
			//						{
			//							drEach["insurance_surcharge"] = drPkg["insurance_surcharge"];
			//						}
			//						if(((drPkg["esa_delivery_surcharge"]!= null) && (!drPkg["esa_delivery_surcharge"].GetType().Equals(System.Type.GetType("System.DBNull")))))
			//						{
			//							drEach["esa_delivery_surcharge"] = drPkg["esa_delivery_surcharge"];
			//						}
			//						dsPikupShpData.Tables[0].Rows.Add(drEach);  
			//						
			//					}
			//				}
			//			}
			//			else
			//			{
			//				dsPikupShpData = GetEmptyPickupRequestShipment();
			//				
			//			}
			
			return dsPikupShp;				
			
		}
		public static DataSet GetPikupShpSerial(String appID, String enterpriseID, int bookingNo, int iSerialNo)
		{
			DbConnection dbCon= null;
			DataSet dsPikupShp =null;
			IDbCommand dbcmd=null;
			DataSet dsPikupShpData = null;

			dbCon = DbConnectionManager.GetInstance().GetDbConnection(appID,enterpriseID);

			if (dbCon==null)
			{
				Logger.LogTraceError("PRBMgrDAL.cs","GetPikupShp","ERR001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			StringBuilder strQry =  new StringBuilder(); 
			strQry = strQry.Append("select * from Pickup_Shipment");
			strQry.Append(" where applicationid = '");
			strQry.Append(appID);
			strQry.Append ("' and enterpriseid = '");
			strQry.Append (enterpriseID);
			strQry.Append("' and booking_no = ");
			strQry.Append(bookingNo);
			strQry.Append(" and serial_no = ");
			strQry.Append(iSerialNo);
		
			dbcmd = dbCon.CreateCommand(strQry.ToString(),CommandType.Text);
 
			try
			{
				dsPikupShp = (DataSet)dbCon.ExecuteQuery(dbcmd,com.common.DAL.ReturnType.DataSetType);  
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("PRBMgr.cs","GetPkgData","ERR002","Error in Query String "+appException.Message);
				throw appException;
			}
			
			dsPikupShpData = GetEmptyPickupRequestShipment();
			dsPikupShpData .Tables[0].Rows[0].Delete(); 
			int cnt = dsPikupShp.Tables[0].Rows.Count -1;
			if (cnt > -1)
			{
						
				if(dsPikupShp.Tables[0].Rows.Count > 0)
				{
					int i = 0;
					for(i = 0;i<dsPikupShp.Tables[0].Rows.Count;i++)
					{
						DataRow drPkg = dsPikupShp.Tables[0].Rows[i];
							
						DataRow drEach = dsPikupShpData.Tables[0].NewRow();
								
						drEach["serial_no"] = drPkg["serial_no"];
						drEach["recipient_zipcode"] = drPkg["recipient_zipcode"];
						drEach["service_code"] = drPkg["service_code"];
						drEach["declare_value"] = drPkg["declare_value"];
						drEach["percent_dv_additional"] = drPkg["percent_dv_additional"];
						drEach["est_delivery_datetime"] = drPkg["est_delivery_datetime"];
						drEach["tot_freight_charge"] = drPkg["tot_freight_charge"];
						drEach["tot_vas_surcharge"] = drPkg["tot_vas_surcharge"];
						drEach["insurance_surcharge"] = drPkg["insurance_surcharge"];
						drEach["esa_delivery_surcharge"] = drPkg["esa_delivery_surcharge"];
						dsPikupShpData.Tables[0].Rows.Add(drEach);  
						
					}
				}
			}
			else
			{
				dsPikupShpData = GetEmptyPickupRequestShipment();
				
			}
			
			return dsPikupShpData;				
			
		}
		public static int UpdatePickupShipment(string AppID,String EnterpriseID,String strBookingNo,int iserialno,DataSet dsShpmDetls,DataSet m_dsVAS,DataSet dsPkgDetails)
		{
			string strModifyQry=null;
			int iRowsAffected=0;
			DbConnection dbCon = null;
			IDbCommand dbUpdateQuery = null;
			dbCon = DbConnectionManager.GetInstance().GetDbConnection(AppID,EnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("PRBMgrDAL","UpdateDomesticShp","ERR001","DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null",null);
			}
			//			if (m_dsShpmDetls == null)
			//			{
			//				Logger.LogTraceError("PickupShipment","UpdatePickupShp","ERR002","DataSet is null!!");
			//				throw new ApplicationException("The Shipment DataSet is null",null);
			//			}
			
			IDbConnection conApp = dbCon.GetConnection();
			
			if(conApp == null)
			{
				Logger.LogTraceError("PRBMgrDAL","UpdateDomesticShp","ERR004","conApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			try
			{
				dbCon.OpenConnection(ref conApp);
			}
			catch(ApplicationException appException)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("PRBMgrDAL","UpdateDomesticShp","ERR005","Error opening connection : "+ appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ",appException);
			}
			catch(Exception exception)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("PRBMgrDAL","UpdateDomesticShp","ERR006","Error opening connection : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}


			IDbTransaction transactionApp = dbCon.BeginTransaction(conApp,IsolationLevel.ReadCommitted);

			if(transactionApp == null)
			{
				Logger.LogTraceError("PRBMgrDAL","UpdateDomesticShp","ERR007","transactionApp is null");
				throw new ApplicationException("Failed Begining a App Transaction..",null);
			}
			try
			{
				int Rcnt = dsShpmDetls.Tables[0].Rows.Count;
				DataRow drEach = dsShpmDetls.Tables[0].Rows[0];
				strModifyQry =strModifyQry+"update Pickup_Shipment set recipient_zipcode = ";
				if((drEach["recipient_zipcode"]!= null) && (!drEach["recipient_zipcode"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					strModifyQry = strModifyQry+"'"+drEach["recipient_zipcode"]+"'";;
				}	
				else
				{
					strModifyQry=strModifyQry+"null";
				}	
				strModifyQry = strModifyQry+",service_code = ";
				if((drEach["service_code"]!= null) && (!drEach["service_code"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					strModifyQry = strModifyQry+"'"+drEach["service_code"]+"'";
				}
				else
				{
								
					strModifyQry=strModifyQry+"null";
				}	
				strModifyQry = strModifyQry+",declare_value = ";
				if((drEach["declare_value"]!= null) && (!drEach["declare_value"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					strModifyQry = strModifyQry+drEach["declare_value"];
				}
				else
				{
							
					strModifyQry=strModifyQry+"null";
				}	
							
				strModifyQry = strModifyQry+",percent_dv_additional =";
				if((drEach["percent_dv_additional"]!= null) && (!drEach["percent_dv_additional"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					strModifyQry = strModifyQry+drEach["percent_dv_additional"];
				}	
				else
				{
					strModifyQry=strModifyQry+"null";
				}	
							
				strModifyQry = strModifyQry+",est_delivery_datetime =";
				if((drEach["est_delivery_datetime"]!= null) && (!drEach["est_delivery_datetime"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
									
					DateTime dtest_delivery_datetime = Convert.ToDateTime(drEach["est_delivery_datetime"]);
					String strest_delivery_datetime= Utility.DateFormat(AppID,EnterpriseID,dtest_delivery_datetime,DTFormat.DateTime);
					strModifyQry = strModifyQry+strest_delivery_datetime;

					//strModifyQry = strModifyQry+"'"+drEach["est_delivery_datetime"]+"'";
				}
				else
				{
					strModifyQry=strModifyQry+"null";
				}	
								
				strModifyQry = strModifyQry +",tot_freight_charge =";
				if((drEach["tot_freight_charge"]!= null) && (!drEach["tot_freight_charge"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					strModifyQry = strModifyQry+drEach["tot_freight_charge"];
				}
				else
				{
					strModifyQry=strModifyQry+"null";
				}	
							
				strModifyQry = strModifyQry +",tot_vas_surcharge =";
				if((drEach["tot_vas_surcharge"]!= null) && (!drEach["tot_vas_surcharge"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					strModifyQry = strModifyQry+drEach["tot_vas_surcharge"];
				}
				else
				{
					strModifyQry=strModifyQry+"null";
				}	
							
				strModifyQry = strModifyQry +",insurance_surcharge =";
				if((drEach["insurance_surcharge"]!= null) && (!drEach["insurance_surcharge"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					strModifyQry = strModifyQry+drEach["insurance_surcharge"];
				}
				else
				{
					strModifyQry=strModifyQry+"null";
				}	
				strModifyQry = strModifyQry +",esa_delivery_surcharge =";
				if((drEach["esa_delivery_surcharge"]!= null) && (!drEach["esa_delivery_surcharge"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					strModifyQry = strModifyQry+drEach["esa_delivery_surcharge"];
				}
				else
				{
					strModifyQry=strModifyQry+"null";
				}	
				strModifyQry = strModifyQry +" where";
				strModifyQry = strModifyQry +" applicationid =";
				strModifyQry = strModifyQry +"'"+AppID+"'";
				strModifyQry = strModifyQry +" and enterpriseid =";
				strModifyQry = strModifyQry +"'"+EnterpriseID+"'";
				strModifyQry = strModifyQry +" and booking_no =";
				strModifyQry = strModifyQry +strBookingNo;
				strModifyQry = strModifyQry +" and serial_no =";
				strModifyQry = strModifyQry +iserialno;

				dbUpdateQuery = dbCon.CreateCommand(strModifyQry,CommandType.Text);

				dbUpdateQuery.Connection = conApp;
				dbUpdateQuery.Transaction = transactionApp;
				iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbUpdateQuery);
							
				if(dsPkgDetails != null)
				{
				
					StringBuilder  strBuild = new StringBuilder();
					//delete from the Pickup_PKG table
					strBuild.Append("delete from Pickup_PKG where applicationid = '");
					strBuild.Append(AppID+"' and enterpriseid = '");
					strBuild.Append(EnterpriseID);
					strBuild.Append("' and booking_no = ");
					strBuild.Append(strBookingNo);
					strBuild.Append(" and serial_no = ");
					strBuild.Append(iserialno);
					dbUpdateQuery.CommandText = strBuild.ToString();
					iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbUpdateQuery);
					Logger.LogTraceInfo("PRBMgrDAL","UpdatePickupShp","ERR005",iRowsAffected + " rows deleted from Pickup_PKG table");

					//insert into the Pickup_PKG table
					int cnt = dsPkgDetails.Tables[0].Rows.Count;
					int i = 0;
					for(i=0;i<cnt;i++)
					{
						StringBuilder strBuildpkg = new StringBuilder();
						DataRow drEachPKG = dsPkgDetails.Tables[0].Rows[i];
						String strMpsNo = "";
						if((drEachPKG["mps_no"].ToString() != "")&&(drEachPKG["mps_no"].ToString() != null)&&drEachPKG["mps_no"].ToString() != "''")
						{
							strMpsNo = (String)drEachPKG["mps_no"];
						}
											
						decimal decPkgLgth = (decimal)drEachPKG["pkg_length"];
						decimal decPkgBrdth = (decimal)drEachPKG["pkg_breadth"];
						decimal decPkgHgth = (decimal)drEachPKG["pkg_height"];
						decimal decPkgWt = (decimal)drEachPKG["pkg_wt"];
						int iPkgQty = (int)drEachPKG["pkg_qty"];
						decimal decTotalWt = (decimal)drEachPKG["tot_wt"];
						decimal decTotalDimWt = (decimal)drEachPKG["tot_dim_wt"];
						decimal decChrgbleWt =0;
						if((drEachPKG["chargeable_wt"]!= null) && (!drEachPKG["chargeable_wt"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						{
							decChrgbleWt = (decimal)drEachPKG["chargeable_wt"];
						}
													
						strBuildpkg.Append("insert into Pickup_PKG (applicationid,enterpriseid,booking_no,serial_no,mps_code,pkg_length,pkg_breadth,pkg_height,pkg_wt,pkg_qty,tot_wt,tot_dim_wt,chargeable_wt)values('");
						strBuildpkg.Append(AppID+"','");
						strBuildpkg.Append(EnterpriseID+"',");
						strBuildpkg.Append(strBookingNo);
						strBuildpkg.Append(",");
						strBuildpkg.Append(iserialno);
						strBuildpkg.Append(",'");
						strBuildpkg.Append(strMpsNo+"',");
						strBuildpkg.Append(decPkgLgth+",");
						strBuildpkg.Append(decPkgBrdth+",");
						strBuildpkg.Append(decPkgHgth+",");
						strBuildpkg.Append(decPkgWt+",");
						strBuildpkg.Append(iPkgQty+",");
						strBuildpkg.Append(decTotalWt+",");
						strBuildpkg.Append(decTotalDimWt+",");
						strBuildpkg.Append(decChrgbleWt+")");
						dbUpdateQuery.CommandText = strBuildpkg.ToString();
						iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbUpdateQuery);
						 
						Logger.LogTraceInfo("PRBMgrDAL","UpdatePickupShp","ERR006",iRowsAffected + " rows inserted into Pickup_pkg table");
					}
				}
						
				if(m_dsVAS != null)
				{
					//delete from the shipment_vas table
					StringBuilder  strBuildVAS = new StringBuilder();
					strBuildVAS.Append("delete from Pickup_VAS where applicationid = '");
					strBuildVAS.Append(AppID+"' and enterpriseid = '");
					strBuildVAS.Append(EnterpriseID);
					strBuildVAS.Append("' and booking_no = ");
					strBuildVAS.Append(strBookingNo );
					strBuildVAS.Append(" and serial_no = ");
					strBuildVAS.Append(iserialno);
							
					dbUpdateQuery.CommandText = strBuildVAS.ToString();
					iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbUpdateQuery);
					Logger.LogTraceInfo("PRBMgrDAL","UpdatePickupShp","ERR005",iRowsAffected + " rows deleted from Shipment_VAS table");
							
					//insert into the shipment_vas table
				
					int cnt = m_dsVAS.Tables[0].Rows.Count;
					int i = 0;

					for(i=0;i<cnt;i++)
					{
						DataRow drEachVAS = m_dsVAS.Tables[0].Rows[i];
					
						String strVASCode = "";
						if(drEachVAS["vas_code"].ToString() != "")
						{
							strVASCode = (String)drEachVAS["vas_code"];
						}
						decimal decSurcharge = (decimal)drEachVAS["surcharge"];
					
						String strRemarks = "";
						if(drEachVAS["remarks"].ToString() != "")
						{
							strRemarks = (String)drEachVAS["remarks"];
						}

						strBuildVAS = new StringBuilder();			
						strBuildVAS.Append("insert into Pickup_VAS (applicationid,enterpriseid,booking_no,serial_no,vas_code,vas_surcharge,remark)values('");
						strBuildVAS.Append(AppID+"','");
						strBuildVAS.Append(EnterpriseID+"',");
						strBuildVAS.Append(strBookingNo);
						strBuildVAS.Append(",");
						strBuildVAS.Append(iserialno);
						strBuildVAS.Append(",'");
						strBuildVAS.Append(strVASCode+"',");
						strBuildVAS.Append(decSurcharge+",N'");
						if(strRemarks!=null)
						{
							strBuildVAS.Append(Utility.ReplaceSingleQuote(strRemarks)+"')");
						}
						else
						{
							strBuildVAS.Append(null+")");
						}

						dbUpdateQuery.CommandText = strBuildVAS.ToString();
						iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbUpdateQuery);
						Logger.LogTraceInfo("PRBMgrDAL","UpdatePickupShp","ERR006",iRowsAffected + " rows inserted into shipment_vas table");
					}
					
				}
		
				transactionApp.Commit();
				Logger.LogTraceInfo("PRBMgrDAL","UpdatePickupShp","ERR015","App db insert transaction committed.");
			}
			catch(ApplicationException appException)
			{
				try
				{
					transactionApp.Rollback();
					Logger.LogTraceInfo("PRBMgrDAL","UpdatePickupShp","ERR008","App db insert transaction rolled back.");
				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("PRBMgrDAL","UpdatePickupShp","ERR009","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}
		
				Logger.LogTraceError("PRBMgrDAL","UpdatePickupShp","ERR010","Insert Transaction Failed : "+ appException.Message.ToString());
				throw new ApplicationException("Error adding Shipment ",appException);
			}
			catch(Exception exception)
			{
				try
				{
					transactionApp.Rollback();
					Logger.LogTraceInfo("DomesticShipmentMgrDAL","UpdateDomesticShp","ERR011","App db insert transaction rolled back.");
		
				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("DomesticShipmentMgrDAL","UpdateDomesticShp","ERR012","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}
		
				Logger.LogTraceError("DomesticShipmentMgrDAL","UpdateDomesticShp","ERR013","Insert Transaction Failed : "+ exception.Message.ToString());
				throw new ApplicationException("Error adding Shipment ",exception);
			}
			finally
			{
				if(conApp != null)
				{
					conApp.Close();
				}
			}
			return iRowsAffected;
	
		}

		public static DataSet GetTotPkgs(string strAppID,string strEnterpriseID,String strMps_code)
		{
			
			DataSet PickupPKG=null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("dsPickupPKG","GetTotPkgs","PRBMGR01","DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null",null);
			}
			try
			{
				StringBuilder strQry =  new StringBuilder(); 
				strQry = strQry.Append("select * from Pickup_PKG");
				strQry.Append(" where applicationid = '");
				strQry.Append(strAppID);
				strQry.Append ("' and enterpriseid = '");
				strQry.Append (strEnterpriseID);
				strQry.Append("' and mps_code = '");
				strQry.Append(strMps_code);
				strQry.Append("'");
				//				strQry.Append (" and serial_no = ");
				//				strQry.Append(strserialno);
				
				
						
				
				PickupPKG =(DataSet)dbCon.ExecuteQuery(strQry.ToString(),ReturnType.DataSetType); 
				int cnt = PickupPKG.Tables[0].Rows.Count;
				return PickupPKG;
			}		 
						
			catch
			{
				Logger.LogTraceInfo("PRBMgrDAL","GetTotPkgs","PRBMGR02","Select command failed");
				throw new ApplicationException("Select command failed");
			}
		}
		public static DataSet GetValuesfromPickup(string strAppID,String strEnterpriseID,String strBookingNo)
		{
			DataSet dsPickup=null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("PRbMgrDAL","GetLatusDispatchStatus","PRBMGR01","DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null",null);
			}
			try
			{
				StringBuilder strQry =  new StringBuilder(); 
				strQry = strQry.Append("select tot_pkg,tot_wt,tot_dim_wt,cash_amount from Pickup_Request");
				strQry.Append(" where applicationid = '");
				strQry.Append(strAppID);
				strQry.Append ("' and enterpriseid = '");
				strQry.Append (strEnterpriseID);
				strQry.Append("' and booking_no = ");
				strQry.Append(strBookingNo);
				dsPickup =(DataSet)dbCon.ExecuteQuery(strQry.ToString(),ReturnType.DataSetType); 
				return dsPickup;
			}		 
						
			catch
			{
				Logger.LogTraceInfo("PRBMgrDAL","GetValuesfromPickup","PRBMGR02","Select command failed");
				throw new ApplicationException("Select command failed");
			}
		}
		public static void GetInsertShipment(String AppID,String EnterpriseID,String strBookingNo,DataSet m_dsShpmDetls,DataSet m_dsVAS,DataSet m_dsPkgDetails,ref ArrayList QueryList,Array ValueList)
		{
			int cnt=0;
			int i=0;
			String strModifyQry=null;
			string strPickShp=null;
			DataRow drEachPickShp = m_dsShpmDetls.Tables[0].Rows[0];
			strPickShp ="Insert into Pickup_Shipment(applicationid,enterpriseid,booking_no,serial_no,";
			strPickShp=strPickShp+"recipient_zipcode,service_code,declare_value,percent_dv_additional,";
			strPickShp=strPickShp+"est_delivery_datetime,tot_freight_charge,tot_vas_surcharge,insurance_surcharge,esa_delivery_surcharge";
			strPickShp=strPickShp+")values(";
			strPickShp=strPickShp+"'"+AppID+"',";
			strPickShp=strPickShp+"'"+EnterpriseID+"',";
			strPickShp=strPickShp+strBookingNo+",";

			if((drEachPickShp["serial_no"]!= null) && (!drEachPickShp["serial_no"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				strPickShp=strPickShp+(int)(drEachPickShp["serial_no"])+",";
			}
			else
			{
				strPickShp=strPickShp+"null";
			}
			if((drEachPickShp["recipient_zipcode"]!= null) && (!drEachPickShp["recipient_zipcode"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				strPickShp=strPickShp+"'"+(drEachPickShp["recipient_zipcode"])+"',";
			}
			else
			{
				strPickShp=strPickShp+"null"+",";
			}
			if((drEachPickShp["service_code"]!= null) && (!drEachPickShp["service_code"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				strPickShp=strPickShp+"'"+(drEachPickShp["service_code"])+"',";
			}
			else
			{
				strPickShp=strPickShp+"null"+",";
			}
			if((drEachPickShp["declare_value"]!= null) && (!drEachPickShp["declare_value"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				strPickShp=strPickShp+(decimal)(drEachPickShp["declare_value"])+",";
			}
			else
			{
				strPickShp=strPickShp+"null"+",";
			}
			if((drEachPickShp["percent_dv_additional"]!= null) && (!drEachPickShp["percent_dv_additional"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				strPickShp=strPickShp+(decimal)(drEachPickShp["percent_dv_additional"])+",";
			}
			else
			{
				strPickShp=strPickShp+"null"+",";
			}
			if((drEachPickShp["est_delivery_datetime"]!= null) && (!drEachPickShp["est_delivery_datetime"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				DateTime dtest_delivery_datetime = Convert.ToDateTime(drEachPickShp["est_delivery_datetime"]);
				String strest_delivery_datetime = Utility.DateFormat(AppID,EnterpriseID, dtest_delivery_datetime,DTFormat.DateTime);
				strPickShp=strPickShp+strest_delivery_datetime+",";
			}	
			else	
			{
				strPickShp=strPickShp+"null"+",";
			}
			if((drEachPickShp["tot_freight_charge"]!= null) && (!drEachPickShp["tot_freight_charge"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				strPickShp=strPickShp+(decimal)(drEachPickShp["tot_freight_charge"])+",";
			}
			else
			{
				strPickShp=strPickShp+"null"+",";
			}
			if((drEachPickShp["tot_vas_surcharge"]!= null) && (!drEachPickShp["tot_vas_surcharge"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				strPickShp=strPickShp+(decimal)(drEachPickShp["tot_vas_surcharge"])+",";
			}
			else
			{
				strPickShp=strPickShp+"null"+",";
			}
			if((drEachPickShp["insurance_surcharge"]!= null) && (!drEachPickShp["insurance_surcharge"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				strPickShp=strPickShp+(decimal)(drEachPickShp["insurance_surcharge"])+",";
			}
			else
			{
				strPickShp=strPickShp+"null"+",";
			}
			if((drEachPickShp["esa_delivery_surcharge"]!= null) && (!drEachPickShp["esa_delivery_surcharge"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				strPickShp=strPickShp+(decimal)(drEachPickShp["esa_delivery_surcharge"])+")";
			}
			else
			{
				strPickShp=strPickShp+"null"+")";
			}
			
			if(QueryList != null)
			{
				QueryList.Add(strPickShp);
			}

		 
			strModifyQry =strModifyQry+"update Pickup_Request set tot_pkg = ";
			strModifyQry=strModifyQry+ ValueList.GetValue(0);
			strModifyQry = strModifyQry +",tot_wt =";
			strModifyQry=strModifyQry+ValueList.GetValue(1);
			strModifyQry = strModifyQry +",tot_dim_wt =";
			strModifyQry=strModifyQry+ValueList.GetValue(2);
			strModifyQry = strModifyQry +",cash_amount =";
			strModifyQry=strModifyQry+ValueList.GetValue(3);
			strModifyQry = strModifyQry +" where";
			strModifyQry = strModifyQry +" applicationid =";
			strModifyQry = strModifyQry +"'"+AppID+"'";
			strModifyQry = strModifyQry +" and enterpriseid =";
			strModifyQry = strModifyQry +"'"+EnterpriseID+"'";
			strModifyQry = strModifyQry +" and booking_no =";
			strModifyQry = strModifyQry +strBookingNo;
			
			if(QueryList != null)
			{
				QueryList.Add(strModifyQry);
			}
						
			if(m_dsVAS !=null)
			{
				//insert into the shipment_vas table
				cnt = m_dsVAS.Tables[0].Rows.Count;
				i = 0;
										
				for(i=0;i<cnt;i++)
				{
					StringBuilder strBuild = new StringBuilder();
					DataRow drEachVAS = m_dsVAS.Tables[0].Rows[i];
												
					String strVASCode = "";
					if(drEachVAS["vas_code"].ToString() != "")
					{
						strVASCode = (String)drEachVAS["vas_code"];
					}
					decimal decSurcharge = (decimal)drEachVAS["surcharge"];
												
					String strRemarks = "";
					if(drEachVAS["remarks"].ToString() != "")
					{
						strRemarks = (String)drEachVAS["remarks"];
					}
																	
					strBuild.Append("insert into Pickup_VAS (applicationid,enterpriseid,booking_no,serial_no,vas_code,vas_surcharge,remark)values('");
					strBuild.Append(AppID +"','");
					strBuild.Append(EnterpriseID +"',");
					strBuild.Append(strBookingNo);
					strBuild.Append(",");
					strBuild.Append(drEachPickShp["serial_no"]);
					strBuild.Append(",'");
					strBuild.Append(strVASCode+"',");
					strBuild.Append(decSurcharge+",N'");
					if(strRemarks!=null)
					{
						strBuild.Append(Utility.ReplaceSingleQuote(strRemarks)+"')");
					}
					else
					{
						strBuild.Append(null+")");
					}
						
					if(QueryList != null)
					{
						QueryList.Add(strBuild.ToString() );
					}
				}
							
			}
								
				
			if(m_dsPkgDetails !=null)
			{
				cnt = m_dsPkgDetails.Tables[0].Rows.Count;
				i = 0;
			
							
				for(i=0;i<cnt;i++)
				{
					StringBuilder strBuildpkg = new StringBuilder();
					DataRow drEachPKG = m_dsPkgDetails.Tables[0].Rows[i];
					String strMpsNo = "";
					if(drEachPKG["mps_no"].ToString() != "")
					{
						strMpsNo = (String)drEachPKG["mps_no"];
					}
											
					decimal decPkgLgth = (decimal)drEachPKG["pkg_length"];
					decimal decPkgBrdth = (decimal)drEachPKG["pkg_breadth"];
					decimal decPkgHgth = (decimal)drEachPKG["pkg_height"];
					decimal decPkgWt = (decimal)drEachPKG["pkg_wt"];
					int iPkgQty = (int)drEachPKG["pkg_qty"];
					decimal decTotalWt = (decimal)drEachPKG["tot_wt"];
					decimal decTotalDimWt = (decimal)drEachPKG["tot_dim_wt"];
					decimal decChrgbleWt =0;
					if((drEachPKG["chargeable_wt"]!= null) && (!drEachPKG["chargeable_wt"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						decChrgbleWt = (decimal)drEachPKG["chargeable_wt"];
					}
													
					strBuildpkg.Append("insert into Pickup_PKG (applicationid,enterpriseid,booking_no,serial_no,mps_code,pkg_length,pkg_breadth,pkg_height,pkg_wt,pkg_qty,tot_wt,tot_dim_wt,chargeable_wt)values('");
					strBuildpkg.Append(AppID+"','");
					strBuildpkg.Append(EnterpriseID+"',");
					strBuildpkg.Append(strBookingNo);
					strBuildpkg.Append(",");
					strBuildpkg.Append(drEachPickShp["serial_no"]);
					strBuildpkg.Append(",'");
					strBuildpkg.Append(strMpsNo+"',");
					strBuildpkg.Append(decPkgLgth+",");
					strBuildpkg.Append(decPkgBrdth+",");
					strBuildpkg.Append(decPkgHgth+",");
					strBuildpkg.Append(decPkgWt+",");
					strBuildpkg.Append(iPkgQty+",");
					strBuildpkg.Append(decTotalWt+",");
					strBuildpkg.Append(decTotalDimWt+",");
					strBuildpkg.Append(decChrgbleWt+")");
						
					if(QueryList != null)
					{
						QueryList.Add(strBuildpkg.ToString() );
					}
				}
						
			}	
					
			 

		}
		public static void  ModifyPickupShipment(string AppID,String EnterpriseID,String strBookingNo,int iserialno,DataSet dsShpmDetls,DataSet m_dsVAS,DataSet dsPkgDetails,ref ArrayList QueryList,Array ValueList )
		{
			string strModifyQry=null;
			
			//Update Pickup_Shipment ....
			DataRow drEach = dsShpmDetls.Tables[0].Rows[0];
			strModifyQry =strModifyQry+"update Pickup_Shipment set recipient_zipcode = ";
			if((drEach["recipient_zipcode"]!= null) && (!drEach["recipient_zipcode"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				strModifyQry = strModifyQry+"'"+drEach["recipient_zipcode"]+"'";;
			}	
			else
			{
				strModifyQry=strModifyQry+"null";
			}	
			strModifyQry = strModifyQry+",service_code = ";
			if((drEach["service_code"]!= null) && (!drEach["service_code"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				strModifyQry = strModifyQry+"'"+drEach["service_code"]+"'";
			}
			else
			{
								
				strModifyQry=strModifyQry+"null";
			}	
			strModifyQry = strModifyQry+",declare_value = ";
			if((drEach["declare_value"]!= null) && (!drEach["declare_value"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				strModifyQry = strModifyQry+drEach["declare_value"];
			}
			else
			{
							
				strModifyQry=strModifyQry+"null";
			}	
							
			strModifyQry = strModifyQry+",percent_dv_additional =";
			if((drEach["percent_dv_additional"]!= null) && (!drEach["percent_dv_additional"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				strModifyQry = strModifyQry+drEach["percent_dv_additional"];
			}	
			else
			{
				strModifyQry=strModifyQry+"null";
			}	
							
			strModifyQry = strModifyQry+",est_delivery_datetime =";
			if((drEach["est_delivery_datetime"]!= null) && (!drEach["est_delivery_datetime"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
									
				DateTime dtest_delivery_datetime = Convert.ToDateTime(drEach["est_delivery_datetime"]);
				String strest_delivery_datetime= Utility.DateFormat(AppID,EnterpriseID,dtest_delivery_datetime,DTFormat.DateTime);
				strModifyQry = strModifyQry+strest_delivery_datetime;

				//strModifyQry = strModifyQry+"'"+drEach["est_delivery_datetime"]+"'";
			}
			else
			{
				strModifyQry=strModifyQry+"null";
			}	
								
			strModifyQry = strModifyQry +",tot_freight_charge =";
			if((drEach["tot_freight_charge"]!= null) && (!drEach["tot_freight_charge"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				strModifyQry = strModifyQry+drEach["tot_freight_charge"];
			}
			else
			{
				strModifyQry=strModifyQry+"null";
			}	
							
			strModifyQry = strModifyQry +",tot_vas_surcharge =";
			if((drEach["tot_vas_surcharge"]!= null) && (!drEach["tot_vas_surcharge"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				strModifyQry = strModifyQry+drEach["tot_vas_surcharge"];
			}
			else
			{
				strModifyQry=strModifyQry+"null";
			}	
							
			strModifyQry = strModifyQry +",insurance_surcharge =";
			if((drEach["insurance_surcharge"]!= null) && (!drEach["insurance_surcharge"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				strModifyQry = strModifyQry+drEach["insurance_surcharge"];
			}
			else
			{
				strModifyQry=strModifyQry+"null";
			}	
			strModifyQry = strModifyQry +",esa_delivery_surcharge =";
			if((drEach["esa_delivery_surcharge"]!= null) && (!drEach["esa_delivery_surcharge"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				strModifyQry = strModifyQry+drEach["esa_delivery_surcharge"];
			}
			else
			{
				strModifyQry=strModifyQry+"null";
			}	
			strModifyQry = strModifyQry +" where";
			strModifyQry = strModifyQry +" applicationid =";
			strModifyQry = strModifyQry +"'"+AppID+"'";
			strModifyQry = strModifyQry +" and enterpriseid =";
			strModifyQry = strModifyQry +"'"+EnterpriseID+"'";
			strModifyQry = strModifyQry +" and booking_no =";
			strModifyQry = strModifyQry +strBookingNo;
			strModifyQry = strModifyQry +" and serial_no =";
			strModifyQry = strModifyQry +iserialno;

			if(QueryList !=null)
			{
				QueryList.Add(strModifyQry);
			}
			//Update PickupRequest...
			String strModPickup = null;
			strModPickup =strModPickup+"update Pickup_Request set tot_pkg = ";
			strModPickup=strModPickup+ ValueList.GetValue(0);
			strModPickup = strModPickup +",tot_wt =";
			strModPickup=strModPickup+ValueList.GetValue(1);
			strModPickup = strModPickup +",tot_dim_wt =";
			strModPickup=strModPickup+ValueList.GetValue(2);
			strModPickup = strModPickup +",cash_amount =";
			strModPickup=strModPickup+ValueList.GetValue(3);
			strModPickup = strModPickup +" where";
			strModPickup = strModPickup +" applicationid =";
			strModPickup = strModPickup +"'"+AppID+"'";
			strModPickup = strModPickup +" and enterpriseid =";
			strModPickup = strModPickup +"'"+EnterpriseID+"'";
			strModPickup = strModPickup +" and booking_no =";
			strModPickup = strModPickup +strBookingNo;
				
			if(QueryList != null)
			{
				QueryList.Add(strModPickup);
			}
							
			if(dsPkgDetails != null)
			{
				
				StringBuilder  strBuild = new StringBuilder();
				//delete from the Pickup_PKG table
				strBuild.Append("delete from Pickup_PKG where applicationid = '");
				strBuild.Append(AppID+"' and enterpriseid = '");
				strBuild.Append(EnterpriseID);
				strBuild.Append("' and booking_no = ");
				strBuild.Append(strBookingNo);
				strBuild.Append(" and serial_no = ");
				strBuild.Append(iserialno);
					
				if(QueryList != null)
				{
					QueryList.Add(strBuild.ToString());
				}
					
				//insert into the Pickup_PKG table
				int cnt = dsPkgDetails.Tables[0].Rows.Count;
				int i = 0;
				for(i=0;i<cnt;i++)
				{
					StringBuilder strBuildpkg = new StringBuilder();
					DataRow drEachPKG = dsPkgDetails.Tables[0].Rows[i];
					String strMpsNo = "";
					if((drEachPKG["mps_no"].ToString() != "")&&(drEachPKG["mps_no"].ToString() != null)&&drEachPKG["mps_no"].ToString() != "''")
					{
						strMpsNo = (String)drEachPKG["mps_no"];
					}
											
					decimal decPkgLgth = (decimal)drEachPKG["pkg_length"];
					decimal decPkgBrdth = (decimal)drEachPKG["pkg_breadth"];
					decimal decPkgHgth = (decimal)drEachPKG["pkg_height"];
					decimal decPkgWt = (decimal)drEachPKG["pkg_wt"];
					int iPkgQty = (int)drEachPKG["pkg_qty"];
					decimal decTotalWt = (decimal)drEachPKG["tot_wt"];
					decimal decTotalDimWt = (decimal)drEachPKG["tot_dim_wt"];
					decimal decChrgbleWt =0;
					if((drEachPKG["chargeable_wt"]!= null) && (!drEachPKG["chargeable_wt"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						decChrgbleWt = (decimal)drEachPKG["chargeable_wt"];
					}
													
					strBuildpkg.Append("insert into Pickup_PKG (applicationid,enterpriseid,booking_no,serial_no,mps_code,pkg_length,pkg_breadth,pkg_height,pkg_wt,pkg_qty,tot_wt,tot_dim_wt,chargeable_wt)values('");
					strBuildpkg.Append(AppID+"','");
					strBuildpkg.Append(EnterpriseID+"',");
					strBuildpkg.Append(strBookingNo);
					strBuildpkg.Append(",");
					strBuildpkg.Append(iserialno);
					strBuildpkg.Append(",'");
					strBuildpkg.Append(strMpsNo+"',");
					strBuildpkg.Append(decPkgLgth+",");
					strBuildpkg.Append(decPkgBrdth+",");
					strBuildpkg.Append(decPkgHgth+",");
					strBuildpkg.Append(decPkgWt+",");
					strBuildpkg.Append(iPkgQty+",");
					strBuildpkg.Append(decTotalWt+",");
					strBuildpkg.Append(decTotalDimWt+",");
					strBuildpkg.Append(decChrgbleWt+")");
					if(QueryList !=null)
					{
						QueryList.Add(strBuildpkg.ToString());
					}
					
						
				}
			}
						
			if(m_dsVAS != null)
			{
				//delete from the shipment_vas table
				StringBuilder  strBuildVAS = new StringBuilder();
				strBuildVAS.Append("delete from Pickup_VAS where applicationid = '");
				strBuildVAS.Append(AppID+"' and enterpriseid = '");
				strBuildVAS.Append(EnterpriseID);
				strBuildVAS.Append("' and booking_no = ");
				strBuildVAS.Append(strBookingNo );
				strBuildVAS.Append(" and serial_no = ");
				strBuildVAS.Append(iserialno);
							
				if(QueryList != null)
				{
					QueryList.Add(strBuildVAS.ToString());
				}
							
				//insert into the shipment_vas table
				
				int cnt = m_dsVAS.Tables[0].Rows.Count;
				int i = 0;

				for(i=0;i<cnt;i++)
				{
					DataRow drEachVAS = m_dsVAS.Tables[0].Rows[i];
					
					String strVASCode = "";
					if(drEachVAS["vas_code"].ToString() != "")
					{
						strVASCode = (String)drEachVAS["vas_code"];
					}
					decimal decSurcharge = (decimal)drEachVAS["surcharge"];
					
					String strRemarks = "";
					if(drEachVAS["remarks"].ToString() != "")
					{
						strRemarks = (String)drEachVAS["remarks"];
					}

					strBuildVAS = new StringBuilder();			
					strBuildVAS.Append("insert into Pickup_VAS (applicationid,enterpriseid,booking_no,serial_no,vas_code,vas_surcharge,remark)values('");
					strBuildVAS.Append(AppID+"','");
					strBuildVAS.Append(EnterpriseID+"',");
					strBuildVAS.Append(strBookingNo);
					strBuildVAS.Append(",");
					strBuildVAS.Append(iserialno);
					strBuildVAS.Append(",'");
					strBuildVAS.Append(strVASCode+"',");
					strBuildVAS.Append(decSurcharge+",N'");
					if(strRemarks!=null)
					{
						strBuildVAS.Append(Utility.ReplaceSingleQuote(strRemarks)+"')");
					}
					else
					{
						strBuildVAS.Append(null+")");
					}

					if(QueryList !=null)
					{
						QueryList.Add(strBuildVAS.ToString());
					}
				}
					
			}
				
		}

		public static DataSet IsVASExists(string AppID,String EnterpriseID,string strVasCode,string strBookingNo,String strSerialNo)
		{
			
			DbConnection dbCon= null;
			IDbCommand dbcmd=null;
			DataSet dsVas=null;

			dbCon = DbConnectionManager.GetInstance().GetDbConnection(AppID,EnterpriseID);

			if (dbCon==null)
			{
				Logger.LogTraceError("PRBMgrDAL.cs","IsVASExists","ERR001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			StringBuilder strQry =  new StringBuilder(); 
			strQry = strQry.Append("select vas_code  from Pickup_VAS");
			strQry.Append(" where applicationid = '");
			strQry.Append(AppID);
			strQry.Append ("' and enterpriseid = '");
			strQry.Append (EnterpriseID);
			strQry.Append("' and vas_code =  '");
			strQry.Append(strVasCode); 
			strQry.Append("' and booking_no = ");
			strQry.Append(strBookingNo );
			strQry.Append(" and serial_no = ");
			strQry.Append(strSerialNo);
		
			dbcmd = dbCon.CreateCommand(strQry.ToString(),CommandType.Text);
 
			try
			{
				dsVas =(DataSet) dbCon.ExecuteQuery(dbcmd,ReturnType.DataSetType);  
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("PRBMgr.cs","IsVASExists","ERR002","Error in Query String "+appException.Message);
				throw appException;
			}

			return dsVas;
		}

		public static bool isActiveCustomer(String AppID,String EnterpriseID, String cusiID)
		{
			DbConnection dbCon= null;
			IDbCommand dbcmd=null;
			DataSet dsCustomer;

			dbCon = DbConnectionManager.GetInstance().GetDbConnection(AppID, EnterpriseID);

			if (dbCon==null)
			{
				Logger.LogTraceError("PRBMgrDAL.cs","isActiveCustomer","ERR001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			StringBuilder strQry =  new StringBuilder(); 
			strQry = strQry.Append("select status_active  from customer");
			strQry.Append(" where applicationid = '");
			strQry.Append(AppID);
			strQry.Append ("' and enterpriseid = '");
			strQry.Append (EnterpriseID);
			strQry.Append("' and custid =  '");
			strQry.Append(cusiID); 
			strQry.Append("' ");
		
			dbcmd = dbCon.CreateCommand(strQry.ToString(),CommandType.Text);
 
			try
			{
				dsCustomer =(DataSet)dbCon.ExecuteQuery(dbcmd,ReturnType.DataSetType);  
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("PRBMgr.cs","isActiveCustomer","ERR002","Error in Query String "+appException.Message);
				throw appException;
			}

			if(dsCustomer != null)
			{
				if(dsCustomer.Tables[0].Rows.Count != 0)
				{
					if((dsCustomer.Tables[0].Rows[0]["status_active"]!= null) && 
						(!dsCustomer.Tables[0].Rows[0]["status_active"].GetType().Equals(System.Type.GetType("System.DBNull")))&& 
						(dsCustomer.Tables[0].Rows[0]["status_active"].ToString().Trim() != ""))
					{
						if(Convert.ToString(dsCustomer.Tables[0].Rows[0]["status_active"]) == "Y")
							return true;
						else
							return false;
					}
					else
					{
						return true;
					}
				}
				else
				{
					return true;
				}
			}
			else
			{
				return true;
			}
		}

		public static bool isExistCustomer(String AppID,String EnterpriseID, String cusiID)
		{
			DbConnection dbCon= null;
			IDbCommand dbcmd=null;
			DataSet dsCustomer;

			dbCon = DbConnectionManager.GetInstance().GetDbConnection(AppID, EnterpriseID);

			if (dbCon==null)
			{
				Logger.LogTraceError("PRBMgrDAL.cs","isActiveCustomer","ERR001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			StringBuilder strQry =  new StringBuilder(); 
			strQry = strQry.Append("select * from customer");
			strQry.Append(" where applicationid = '");
			strQry.Append(AppID);
			strQry.Append ("' and enterpriseid = '");
			strQry.Append (EnterpriseID);
			strQry.Append("' and custid =  '");
			strQry.Append(cusiID); 
			strQry.Append("' ");
		
			dbcmd = dbCon.CreateCommand(strQry.ToString(),CommandType.Text);
 
			try
			{
				dsCustomer =(DataSet)dbCon.ExecuteQuery(dbcmd,ReturnType.DataSetType);  
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("PRBMgr.cs","isExistCustomer","ERR002","Error in Query String "+appException.Message);
				throw appException;
			}

			if(dsCustomer != null)
			{
				if(dsCustomer.Tables[0].Rows.Count != 0)
				{					
					return true;
				}
				else
				{
					return false;
				}
			}
			else
			{
				return false;
			}
		}


		public static DataTable ReadPickupConsignment(String strAppID, String strEnterpriseID, String strBookingNo, String strConsignmentSeqNo)
		{
			DbConnection dbCon= null;
			IDbCommand dbcmd=null;
			DataSet dsPickupConsignment;
			DataTable dtPickupConsignment = new DataTable();

			dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);

			if (dbCon==null)
			{
				Logger.LogTraceError("PRBMgrDAL.cs","isActiveCustomer","ERR001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			StringBuilder strQry =  new StringBuilder(); 
			//strQry = strQry.Append(" Select * From Pickup_RequestConsignment ");
			strQry = strQry.Append(" SELECT applicationid, enterpriseid, booking_No, CAST(ConsignmentSeqNo as int) as [ConsignmentSeqNo] ");
			strQry.Append(" , RecPostalCode, ServiceType, DeclaredValue, CODAmount, TotalPkg, ActualWt, DimWt ");
			strQry.Append(" , ChgWt, TotalCharge, FreightCharge, InsSurch, OtherSurch, VasSurch, ESASurch, EstDelDate "); 
			strQry.Append(" From Pickup_RequestConsignment "); // end
			strQry.Append(" where applicationid = '");
			strQry.Append(strAppID);
			strQry.Append("' and enterpriseid = '");
			strQry.Append(strEnterpriseID);
			strQry.Append("' and booking_no = '");
			strQry.Append(strBookingNo + "'");
			if(strConsignmentSeqNo!="" && strConsignmentSeqNo!=null)
			{
				strQry.Append(" and ConsignmentSeqNo='");
				strQry.Append(strConsignmentSeqNo); 
				strQry.Append("'");
			}
		
			strQry.Append(" order by ConsignmentSeqNo ASC");

			dbcmd = dbCon.CreateCommand(strQry.ToString(),CommandType.Text);
 
			try
			{
				dsPickupConsignment =(DataSet)dbCon.ExecuteQuery(dbcmd,ReturnType.DataSetType);  
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("PRBMgr.cs","ReadPickupConsignment","ERR002","Error in Query String "+appException.Message);
				throw appException;
			}

			if(dsPickupConsignment != null)
			{
				if(dsPickupConsignment.Tables.Count>0)
					dtPickupConsignment = dsPickupConsignment.Tables[0];
			}
			return dtPickupConsignment;
		}
	

		public  static  DataTable ReadPickupConsignmentVAS(String strAppID, String strEnterpriseID, String strBookingNo, String strConsignmentSeqNo)
		{
			DbConnection dbCon= null;
			IDbCommand dbcmd=null;
			DataSet dsPickupConsignmentVAS;
			DataTable dtPickupConsignmentVAS = new DataTable();

			dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);

			if (dbCon==null)
			{
				Logger.LogTraceError("PRBMgrDAL.cs","isActiveCustomer","ERR001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			StringBuilder strQry =  new StringBuilder(); 
			strQry = strQry.Append(" Select * From Pickup_RequestConsignmentVAS ");
			strQry.Append(" where applicationid = '");
			strQry.Append(strAppID);
			strQry.Append("' and enterpriseid = '");
			strQry.Append(strEnterpriseID);
			strQry.Append("' and booking_no = '");
			strQry.Append(strBookingNo + "'");
			if(strConsignmentSeqNo!="" && strConsignmentSeqNo!=null)
			{
				strQry.Append(" and ConsignmentSeqNo='");
				strQry.Append(strConsignmentSeqNo); 
				strQry.Append("'");
			}
		
			dbcmd = dbCon.CreateCommand(strQry.ToString(),CommandType.Text);
 
			try
			{
				dsPickupConsignmentVAS =(DataSet)dbCon.ExecuteQuery(dbcmd,ReturnType.DataSetType);  
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("PRBMgr.cs","ReadPickupConsignment","ERR002","Error in Query String "+appException.Message);
				throw appException;
			}

			if(dsPickupConsignmentVAS != null)
			{
				if(dsPickupConsignmentVAS.Tables.Count>0)
					dtPickupConsignmentVAS = dsPickupConsignmentVAS.Tables[0];
			}
			return dtPickupConsignmentVAS;
		}

		public  static  DataTable ReadPickupConsignmentPkg(String strAppID, String strEnterpriseID, String strBookingNo, String strConsignmentSeqNo)
		{
			DbConnection dbCon= null;
			IDbCommand dbcmd=null;
			DataSet dsPickupConsignmentPKG;
			DataTable dtPickupConsignmentPKG = new DataTable();

			dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);

			if (dbCon==null)
			{
				Logger.LogTraceError("PRBMgrDAL.cs","isActiveCustomer","ERR001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			StringBuilder strQry =  new StringBuilder(); 
			//strQry = strQry.Append(" Select * From Pickup_RequestConsignmentPKG ");
			strQry = strQry.Append(" SELECT applicationid, enterpriseid, booking_no, CAST(consignmentSeqNo as int) as [consignmentSeqNo] ");
			strQry.Append(" , CAST(Pkg_MPSNo as int) as [Pkg_MPSNo], pkg_length, pkg_breadth, pkg_height, pkg_wt, pkg_qty, tot_act_wt, tot_wt ");
			strQry.Append(" , tot_dim_wt ");
			strQry.Append(" From Pickup_RequestConsignmentPkg "); // end
			strQry.Append(" where applicationid = '");
			strQry.Append(strAppID);
			strQry.Append("' and enterpriseid = '");
			strQry.Append(strEnterpriseID);
			strQry.Append("' and booking_no = '");
			strQry.Append(strBookingNo + "'");
			if(strConsignmentSeqNo!="" && strConsignmentSeqNo!=null)
			{
				strQry.Append(" and ConsignmentSeqNo='");
				strQry.Append(strConsignmentSeqNo); 
				strQry.Append("'");
			}
			strQry.Append(" order by Pkg_MPSNo ASC");

			dbcmd = dbCon.CreateCommand(strQry.ToString(),CommandType.Text);
 
			try
			{
				dsPickupConsignmentPKG =(DataSet)dbCon.ExecuteQuery(dbcmd,ReturnType.DataSetType);  
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("PRBMgr.cs","ReadPickupConsignment","ERR002","Error in Query String "+appException.Message);
				throw appException;
			}
			dsPickupConsignmentPKG.Tables[0].Columns.Add(new DataColumn("pkg_volume",typeof(decimal)));
			if(dsPickupConsignmentPKG.Tables[0].Rows.Count > 0)
			{
				int i = 0;
				for(i = 0;i<dsPickupConsignmentPKG.Tables[0].Rows.Count;i++)
				{
					DataRow drPickupConsignmentPKG = dsPickupConsignmentPKG.Tables[0].Rows[i];
					decimal decVolume = Convert.ToDecimal(drPickupConsignmentPKG["pkg_length"])*Convert.ToDecimal(drPickupConsignmentPKG["pkg_breadth"])*Convert.ToDecimal(drPickupConsignmentPKG["pkg_height"]);
					drPickupConsignmentPKG["pkg_volume"] = decVolume;
				}
			}

			if(dsPickupConsignmentPKG != null)
			{
				if(dsPickupConsignmentPKG.Tables.Count>0)
					dtPickupConsignmentPKG = dsPickupConsignmentPKG.Tables[0];
			}
			return dtPickupConsignmentPKG;
		}

		public  static  void InsertPickupConsignment(String strAppID, String strEnterpriseID, String strBookingNo, DataTable dtPickupConsignment)
		{
			try
			{
			DeletePickupConsignment(strAppID,strEnterpriseID,strBookingNo,null);
			StringBuilder strBuild = new StringBuilder();;
			DbConnection dbCon= null;
			IDbCommand dbcmd=null;
			dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);
			if (dbCon==null)
			{
				Logger.LogTraceError("PRBMgrDAL.cs","isActiveCustomer","ERR001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			int cnt = dtPickupConsignment.Rows.Count;
			int i = 0;

				for(i=0;i<cnt;i++)
				{
					DataRow drEach = dtPickupConsignment.Rows[i];
					int iBookingNo = Convert.ToInt32(strBookingNo);
					DateTime estDelDate = DateTime.Now;
					String strConsignmentSeq = (String)drEach["ConsignmentSeqNo"].ToString().Trim();
					String strRecPostalCode = (String)drEach["RecPostalCode"].ToString().Trim();
					String strServiceType = (String)drEach["ServiceType"].ToString().Trim();
					decimal decDeclaredValue = Convert.ToDecimal(drEach["DeclaredValue"]);
					decimal decCODAmount = Convert.ToDecimal(drEach["CODAmount"]);
					int intTotalPkg = Convert.ToInt32(drEach["TotalPkg"]);
					decimal decActualWt = Convert.ToDecimal(drEach["ActualWt"]);
					decimal decDimWt = Convert.ToDecimal(drEach["DimWt"]);
					decimal decChgWt = Convert.ToDecimal(drEach["ChgWt"]);
					decimal decTotalCharge = Convert.ToDecimal(drEach["TotalCharge"]);
					decimal decFreightCharge = Convert.ToDecimal(drEach["FreightCharge"]);
					decimal decInsSurch = Convert.ToDecimal(drEach["InsSurch"]);
					decimal decOtherSurch = Convert.ToDecimal(drEach["OtherSurch"]);
					decimal decVasSurch = Convert.ToDecimal(drEach["VasSurch"]);
					decimal decESASurch = Convert.ToDecimal(drEach["ESASurch"]);
					if(drEach["EstDelDate"].ToString().Trim() != "")
						estDelDate = (DateTime)drEach["EstDelDate"];
					String strEstdatetime = Utility.DateFormat(strAppID,strEnterpriseID,estDelDate,DTFormat.DateTime);
//					if(drEach["EstDelDate"].ToString().Trim() != "")
//						estDelDate = (DateTime)drEach["EstDelDate"];

					strBuild = new StringBuilder();		

					strBuild.Append("insert into Pickup_RequestConsignment (applicationid,enterpriseid,booking_no,ConsignmentSeqNo,");
					strBuild.Append("RecPostalCode,ServiceType,DeclaredValue,CODAmount,TotalPkg,ActualWt,DimWt,ChgWt,");
					strBuild.Append("TotalCharge,FreightCharge,InsSurch,OtherSurch,VasSurch,ESASurch,EstDelDate)");
					strBuild.Append("values('");
					strBuild.Append(strAppID+"','");
					strBuild.Append(strEnterpriseID+"',");
					strBuild.Append(iBookingNo);
					strBuild.Append(",'");
					strBuild.Append(strConsignmentSeq+"','");
					strBuild.Append(strRecPostalCode+"','");
					strBuild.Append(strServiceType+"',");
					strBuild.Append(decDeclaredValue+",");
					strBuild.Append(decCODAmount+",");
					strBuild.Append(intTotalPkg+",");
					strBuild.Append(decActualWt+",");
					strBuild.Append(decDimWt+",");
					strBuild.Append(decChgWt+",");
					strBuild.Append(decTotalCharge+",");
					strBuild.Append(decFreightCharge+",");
					strBuild.Append(decInsSurch+",");
					strBuild.Append(decOtherSurch+",");
					strBuild.Append(decVasSurch+",");
					strBuild.Append(decESASurch+",");
//					strBuild.Append(decESASurch+")");
					strBuild.Append(strEstdatetime+")");
				
					dbcmd = dbCon.CreateCommand(strBuild.ToString(),CommandType.Text);
					dbCon.ExecuteNonQuery(dbcmd);
				}
				
			}
			catch(Exception e)
			{
				throw new ApplicationException("Error adding PickupRequestConsignment ");
			}		
		}

		public  static  void InsertPickupConsignmentPkg(String strAppID, String strEnterpriseID, String strBookingNo, DataTable dtPickupConsignmentPKG)
		{
			try
			{
				DeletePickupConsignmentPKG(strAppID,strEnterpriseID, strBookingNo, null, null);
				StringBuilder strBuild = new StringBuilder();;
				DbConnection dbCon= null;
				IDbCommand dbcmd=null;
				dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);
				if (dbCon==null)
				{
					Logger.LogTraceError("PRBMgrDAL.cs","isActiveCustomer","ERR001","dbCon is null");
					throw new ApplicationException("Connection to Database failed",null);
				}

				int cnt = dtPickupConsignmentPKG.Rows.Count;
				int i = 0;

				for(i=0;i<cnt;i++)
				{
					DataRow drEach = dtPickupConsignmentPKG.Rows[i];
					int iBookingNo = Convert.ToInt32(strBookingNo);
					String strConsignmentSeq = (String)drEach["ConsignmentSeqNo"].ToString().Trim();
					String strPkg_MPSNo = (String)drEach["Pkg_MPSNo"].ToString().Trim();
					decimal decpkg_length = Convert.ToDecimal(drEach["pkg_length"]);
					decimal decpkg_breadth = Convert.ToDecimal(drEach["pkg_breadth"]);
					decimal decpkg_height = Convert.ToDecimal(drEach["pkg_height"]);
					decimal decpkg_wt = Convert.ToDecimal(drEach["pkg_wt"]);
					int ipkg_qty = Convert.ToInt32(drEach["pkg_qty"]);
					decimal dectot_act_wt = Convert.ToDecimal(drEach["tot_act_wt"]);
					decimal dectot_wt = Convert.ToDecimal(drEach["tot_wt"]);
					decimal dectot_dim_wt = Convert.ToDecimal(drEach["tot_dim_wt"]);

					strBuild = new StringBuilder();		

					strBuild.Append("insert into Pickup_RequestConsignmentPKG (applicationid,enterpriseid,booking_no,ConsignmentSeqNo,");
					strBuild.Append("Pkg_MPSNo,pkg_length,pkg_breadth,pkg_height,pkg_wt,pkg_qty,tot_act_wt,tot_wt,");
					strBuild.Append("tot_dim_wt)");
					strBuild.Append("values('");
					strBuild.Append(strAppID+"','");
					strBuild.Append(strEnterpriseID+"',");
					strBuild.Append(iBookingNo);
					strBuild.Append(",'");
					strBuild.Append(strConsignmentSeq+"','");
					strBuild.Append(strPkg_MPSNo+"',");
					strBuild.Append(decpkg_length+",");
					strBuild.Append(decpkg_breadth+",");
					strBuild.Append(decpkg_height+",");
					strBuild.Append(decpkg_wt+",");
					strBuild.Append(ipkg_qty+",");
					strBuild.Append(dectot_act_wt+",");
					strBuild.Append(dectot_wt+",");
					strBuild.Append(dectot_dim_wt+")");
				
					dbcmd = dbCon.CreateCommand(strBuild.ToString(),CommandType.Text);
					dbCon.ExecuteNonQuery(dbcmd);
				}
				
			}
			catch(Exception e)
			{
				throw new ApplicationException("Error adding PickupRequestConsignmentPKG ");
			}
		}

		public  static  void InsertPickupConsignmentVAS(String strAppID, String strEnterpriseID, String strBookingNo, DataTable dtPickupConsignmentVAS)
		{
			try
			{
				DeletePickupConsignmentVAS(strAppID,strEnterpriseID,strBookingNo,null,null);
				StringBuilder strBuild = new StringBuilder();;
				DbConnection dbCon= null;
				IDbCommand dbcmd=null;
				dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);
				if (dbCon==null)
				{
					Logger.LogTraceError("PRBMgrDAL.cs","isActiveCustomer","ERR001","dbCon is null");
					throw new ApplicationException("Connection to Database failed",null);
				}

				int cnt = dtPickupConsignmentVAS.Rows.Count;
				int i = 0;

				for(i=0;i<cnt;i++)
				{
					DataRow drEach = dtPickupConsignmentVAS.Rows[i];
					int iBookingNo = Convert.ToInt32(strBookingNo);
					String strConsignmentSeq = (String)drEach["ConsignmentSeqNo"].ToString().Trim();
					String strvas_code = (String)drEach["vas_code"].ToString().Trim();
					String strvas_description = (String)drEach["vas_description"];
					decimal decvas_surcharge = Convert.ToDecimal(drEach["vas_surcharge"]);

					strBuild = new StringBuilder();		

					strBuild.Append("insert into Pickup_RequestConsignmentVAS (applicationid,enterpriseid,booking_no,ConsignmentSeqNo,");
					strBuild.Append("vas_code,vas_description,vas_surcharge)");
					strBuild.Append("values('");
					strBuild.Append(strAppID+"','");
					strBuild.Append(strEnterpriseID+"',");
					strBuild.Append(iBookingNo);
					strBuild.Append(",'");
					strBuild.Append(strConsignmentSeq+"','");
					strBuild.Append(strvas_code+"','");
					strBuild.Append(strvas_description+"',");
					strBuild.Append(decvas_surcharge+")");
				
					dbcmd = dbCon.CreateCommand(strBuild.ToString(),CommandType.Text);
					dbCon.ExecuteNonQuery(dbcmd);
				}
				
			}
			catch(Exception e)
			{
				throw new ApplicationException("Error adding PickupRequestConsignmentVAS ");
			}
		}

		public static  void DeletePickupConsignment(String strAppID, String strEnterpriseID, String strBookingNo, String strConsignmentSeqNo)
		{
			DbConnection dbCon= null;
			IDbCommand dbcmd=null;

			dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);

			if (dbCon==null)
			{
				Logger.LogTraceError("PRBMgrDAL.cs","isActiveCustomer","ERR001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			StringBuilder strQry =  new StringBuilder(); 
			strQry = strQry.Append(" Delete From Pickup_RequestConsignment ");
			strQry.Append(" where applicationid = '");
			strQry.Append(strAppID);
			strQry.Append("' and enterpriseid = '");
			strQry.Append(strEnterpriseID);
			strQry.Append("' and booking_no = '");
			strQry.Append(strBookingNo + "'"); 

			if(strConsignmentSeqNo!="" && strConsignmentSeqNo!=null)
			{
				strQry.Append(" and ConsignmentSeqNo='");
				strQry.Append(strConsignmentSeqNo); 
				strQry.Append("'");
			}
		
			dbcmd = dbCon.CreateCommand(strQry.ToString(),CommandType.Text);
 
			try
			{
				dbCon.ExecuteNonQuery(dbcmd);  
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("PRBMgr.cs","ReadPickupConsignment","ERR002","Error in Query String "+appException.Message);
				throw appException;
			}						
		}

		public  static  void DeletePickupConsignmentPKG(String strAppID, String strEnterpriseID, String strBookingNo, String strConsignmentSeqNo, String strPackageMPSNo)
		{
			DbConnection dbCon= null;
			IDbCommand dbcmd=null;

			dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);

			if (dbCon==null)
			{
				Logger.LogTraceError("PRBMgrDAL.cs","isActiveCustomer","ERR001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			StringBuilder strQry =  new StringBuilder(); 
			strQry = strQry.Append(" Delete From Pickup_RequestConsignmentPKG ");
			strQry.Append(" where applicationid = '");
			strQry.Append(strAppID);
			strQry.Append("' and enterpriseid = '");
			strQry.Append(strEnterpriseID);
			strQry.Append("' and booking_no = '");
			strQry.Append(strBookingNo + "'"); 

			if(strConsignmentSeqNo!="" && strConsignmentSeqNo!=null)
			{
				strQry.Append(" and ConsignmentSeqNo='");
				strQry.Append(strConsignmentSeqNo); 
				strQry.Append("'");
			}
		
			dbcmd = dbCon.CreateCommand(strQry.ToString(),CommandType.Text);
 
			try
			{
				dbCon.ExecuteNonQuery(dbcmd);  
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("PRBMgr.cs","ReadPickupConsignment","ERR002","Error in Query String "+appException.Message);
				throw appException;
			}								
		}

		public  static  void DeletePickupConsignmentVAS(String strAppID, String strEnterpriseID, String strBookingNo, String strConsignmentSeqNo, String strConsignmentVASID)
		{
			DbConnection dbCon= null;
			IDbCommand dbcmd=null;

			dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);

			if (dbCon==null)
			{
				Logger.LogTraceError("PRBMgrDAL.cs","isActiveCustomer","ERR001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			StringBuilder strQry =  new StringBuilder(); 
			strQry = strQry.Append(" Delete From Pickup_RequestConsignmentVAS ");
			strQry.Append(" where applicationid = '");
			strQry.Append(strAppID);
			strQry.Append("' and enterpriseid = '");
			strQry.Append(strEnterpriseID);
			strQry.Append("' and booking_no = '");
			strQry.Append(strBookingNo + "'"); 

			if(strConsignmentSeqNo!="" && strConsignmentSeqNo!=null)
			{
				strQry.Append(" and ConsignmentSeqNo='");
				strQry.Append(strConsignmentSeqNo); 
				strQry.Append("'");
			}
		
			dbcmd = dbCon.CreateCommand(strQry.ToString(),CommandType.Text);
 
			try
			{
				dbCon.ExecuteNonQuery(dbcmd);  
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("PRBMgr.cs","ReadPickupConsignment","ERR002","Error in Query String "+appException.Message);
				throw appException;
			}								
		}

		public static void AddNewRowInConsignmentDT(DataTable sessionDT)
		{
			DataRow drNew = sessionDT.NewRow();

			drNew[0] = "";
			drNew[1] = "";
			drNew[2] = 0;
			drNew[3] = 0;
			drNew[4] = "";
			drNew[5] = "";
			drNew[6] = 0.00;
			drNew[7] = 0.00;
			drNew[8] = 0;
			drNew[9] = 0;
			drNew[10] = 0;
			drNew[11] = 0.00;
			drNew[12] = 0.00;
			drNew[13] = 0.00;
			drNew[14] = 0.00;
			drNew[15] = 0.00;
			drNew[16] = 0.00;
			drNew[17] = 0.00;

			sessionDT.Rows.Add(drNew);

		}
		public static void AddNewRowInPackageDT(DataTable sessionDT2)
		{
			DataRow drNew = sessionDT2.NewRow();

			drNew[0] = "";
			drNew[1] = 0;
			drNew[2] = 0;
			drNew[3] = 0;
			drNew[4] = 0;
			drNew[5] = 1;
			drNew[6] = 1;
			drNew[7] = 1;
			drNew[8] = 1;
			drNew[9] = 1;

			sessionDT2.Rows.Add(drNew);
		}
		public static void AddNewRowInVASDT(DataTable sessionDT3)
		{
			DataRow drNew = sessionDT3.NewRow();
			drNew[0] = "";
			drNew[1] = "";
			drNew[2] = 0;
			drNew[3] = "";
			drNew[4] = "";
			drNew[5] = "";
			drNew[6] = 0.00;

			sessionDT3.Rows.Add(drNew);
		}


		public static DataTable GetEstimatedPickupDate(string strAppID, string strEnterpriseID, string strZipcode,string PickupDT)
		{
			DataSet dsEst = new DataSet();
			DataTable dtEst = new DataTable();
			DbConnection dbCon= null;			
			IDbCommand dbcmd=null;

			dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if (dbCon==null)
			{
				Logger.LogTraceError("PRBMgrDAL.cs","GetEstimatedPickupDate","ERR001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			StringBuilder strQry =  new StringBuilder(); 
			//			strQry = strQry.Append("SELECT applicationid, enterpriseid, RequestedPickupDT, PUP_VAS_Code, Est_PickupDT, Prompt,'"+strZipcode+"' Zipcode ");
			//			strQry.Append(" FROM dbo.CalcEstimatedPickup('" + strEnterpriseID + "', N'"+strZipcode+"', '"+DateTime.Now.AddDays(1).ToString()+"')");
			strQry = strQry.Append("SELECT applicationid, enterpriseid, RequestedPickupDT, PUP_VAS_Code, Est_PickupDT, Prompt,'"+strZipcode+"' Zipcode ");
			if(PickupDT == null)
			{
				strQry.Append(" FROM dbo.CalcEstimatedPickup('" + strEnterpriseID + "', N'"+strZipcode+"', null)");
			}
			else
			{
				strQry.Append(" FROM dbo.CalcEstimatedPickup('" + strEnterpriseID + "', N'"+strZipcode+"', '" + PickupDT + "')");
			}
			

			dbcmd = dbCon.CreateCommand(strQry.ToString(),CommandType.Text);
			try
			{
				dtEst = null;
				dsEst =(DataSet)dbCon.ExecuteQuery(dbcmd,ReturnType.DataSetType);  
				if(dsEst.Tables.Count >0 && dsEst.Tables[0].Rows.Count>0)
				{
					dtEst = dsEst.Tables[0];				
				}
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("PRBMgr.cs","GetEstimatedPickupDate","ERR002","Error in Query String "+appException.Message);
				throw appException;
			}

			return dtEst;
		}


		public static DataTable GetEstimatedDeliveryDate(string strAppID, string strEnterpriseID, string ActualPickupDT, 
															string Service, string PUP_zipcode, string DEL_zipcode)
		{
			DataSet dsEst = new DataSet();
			DataTable dtEst = new DataTable();
			DbConnection dbCon= null;			
			IDbCommand dbcmd=null;

			dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if (dbCon==null)
			{
				Logger.LogTraceError("PRBMgrDAL.cs","GetEstimatedDeliveryDate","ERR001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			StringBuilder strQry =  new StringBuilder(); 			
			strQry = strQry.Append("SELECT applicationid, enterpriseid, ActualPickupDT, ServiceType, PUP_VAS_Code,DEL_VAS_Code, EstDel_Date, Prompt ");
			if(ActualPickupDT == null)
			{
				strQry.Append(" FROM dbo.CalcEstimatedDelivery('" + strEnterpriseID + "', null, N'"+Service+"', N'"+PUP_zipcode+"', N'"+DEL_zipcode+"')");
			}
			else
			{
				strQry.Append(" FROM dbo.CalcEstimatedDelivery('" + strEnterpriseID + "', '"+ActualPickupDT+"', N'"+Service+"', N'"+PUP_zipcode+"', N'"+DEL_zipcode+"')");
			}
			

			dbcmd = dbCon.CreateCommand(strQry.ToString(),CommandType.Text);
			try
			{
				dtEst = null;
				dsEst =(DataSet)dbCon.ExecuteQuery(dbcmd,ReturnType.DataSetType);  
				if(dsEst.Tables.Count >0 && dsEst.Tables[0].Rows.Count>0)
				{
					dtEst = dsEst.Tables[0];				
				}
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("PRBMgr.cs","GetEstimatedDeliveryDate","ERR002","Error in Query String "+appException.Message);
				throw appException;
			}

			return dtEst;
		}
	}
}





