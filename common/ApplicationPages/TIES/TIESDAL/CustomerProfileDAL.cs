using System;
using System.Data;
using System.Collections;
using System.Text;
using com.common.DAL;
using com.common.util;
using com.common.classes;
using System.Data.SqlClient;
namespace com.ties.DAL
{
	/// <summary>
	/// Summary description for CustomerProfileDAL.
	/// </summary>
	public class CustomerProfileDAL
	{
		public CustomerProfileDAL()
		{
			//
			// TODO: Add constructor logic here
			//
		}
		public static bool Check_IsCreditStatusNotAvail(String application_id,String enterprise_id,String customer_id)
		{
			DataSet dsResult=null;
			DataTable dtCustomer=null;
			try
			{
				DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(application_id,enterprise_id);
				if(dbCon == null)
				{
					Logger.LogTraceError("CustomerProfileDAL","CustomerProfileDAL","Check_Credit_Limit","EDB101","DbConnection object is null!!");
					return false;
				}
				ArrayList arrParam = new ArrayList();
				arrParam.Add(new SqlParameter("@application_id",application_id));
				arrParam.Add(new SqlParameter("@enterprise_id",enterprise_id));
				arrParam.Add(new SqlParameter("@cust_id",customer_id));
				dsResult = (DataSet)dbCon.ExecuteProcedure("Check_Credit_Limit",arrParam,ReturnType.DataSetType);

				if(Utility.IsNotDBNull(dsResult))
				{
					if(dsResult.Tables.Count>0)
					{	
						if(dsResult.Tables[0] != null)
						{								
							dtCustomer = dsResult.Tables[0];
								
							if(dtCustomer.Rows[0]["creditstatus"] != null && 
								dtCustomer.Rows[0]["creditstatus"].ToString().Trim().ToUpper().Equals("N"))
								return true;
							else
								return false;
						}
						else
							return false;	
					}
					else return false;
				}else return false;
			}
			catch (Exception e)
			{
				Console.WriteLine(e.Message);
			}
			return false;
		}
		public static String GetNextBillPlacementDate(String strAppID, String strEnterpriseID,String strPayerID, ref IDbCommand dbCmd, ref DbConnection dbCon)
		{
			StringBuilder strSelectBuilder = new StringBuilder();

			strSelectBuilder.Append(" SELECT top 1 Customer.next_bill_placement_date FROM Customer WITH(Rowlock)");
			strSelectBuilder.Append(" where Customer.custid='"+ strPayerID +"'");
			//strSelectBuilder.Append(" where shipment.consignment_no ='" + strConsigNo + "'");
			strSelectBuilder.Append(" and Customer.enterpriseid ='"+strEnterpriseID+"'");
			strSelectBuilder.Append(" and Customer.applicationid = '" +strAppID+ "'");

			string strSQL = strSelectBuilder.ToString();

			//dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("CustomerProfileDAL","GetNextBillPlacementDate","EDB101","DbConnection object is null!!");
			}

			dbCmd.CommandText = strSQL;
		
			DataSet dsIShip = (DataSet)dbCon.ExecuteQueryInTransaction(dbCmd,ReturnType.DataSetType);
			try
			{
				if(dsIShip.Tables[0].Rows.Count>0)
				{
					if(dsIShip.Tables[0].Rows[0][0].ToString().Equals("0"))
						return "";
					else
						return dsIShip.Tables[0].Rows[0][0].ToString();
				}
				else
					return "";
			}
			catch
			{
				return "";
			}

		}
	}
}
