using System;
using System.Data;
using System.Collections;
using System.Text;
using com.common.DAL;
using com.common.util;
using com.common.classes;
using com.ties.classes;
namespace com.ties.DAL
{
	/// <summary>
	/// Summary description for HcrStatusByBatchDAL.
	/// </summary>
	public class HcrStatusByBatchDAL
	{
		public HcrStatusByBatchDAL()
		{
			//
			// TODO: Add constructor logic here
			//
		}


		public static DataSet GetByDateHcsHdDS(string strAppId, string strEntId, string strDate, string strLoc)
		{
			DbConnection dbConnect = null;
			IDbCommand dbCmd = null;
			DataSet DS = null;

			dbConnect = DbConnectionManager.GetInstance().GetDbConnection(strAppId, strEntId);
  
			if (dbConnect == null)
			{
				Logger.LogTraceError("TIESDAL", "HcrStatusByBatchDAL", "GetByDateHcsHdDS", "dbConnect is null");
				throw new ApplicationException("Connection to Database failed", null); 
			}

			string strSQLQuery = "";

			strSQLQuery = "SELECT hcs_no, hcs_date, location, isclosed, total_cons, crt_userid, crt_datetime ";
			strSQLQuery += " FROM HCS_HD ";
			strSQLQuery += " WHERE ApplicationId = '" + strAppId + "'  AND enterpriseid = '" + strEntId + "'"; 
			strSQLQuery += " AND CONVERT(datetime, CONVERT(varchar(10),hcs_date,103), 103) = ";
			strSQLQuery += "     CONVERT(datetime, '" + strDate + "', 103) ";
			strSQLQuery += " AND isclosed = 'Y' ";

			if (strLoc != "")
				strSQLQuery += " AND location = '" + strLoc + "' ";

			strSQLQuery += " ORDER BY hcs_date DESC ";
 
			dbCmd = dbConnect.CreateCommand(strSQLQuery.ToString(), CommandType.Text);

			try
			{
				DS = (DataSet)dbConnect.ExecuteQuery(dbCmd, com.common.DAL.ReturnType.DataSetType);
				return DS;   
  
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("TIESDAL", "HcrStatusByBatchDAL", "GetByDateHcsHdDS", "Error in Query String " + appException.Message);
				throw appException;
			}
			finally
			{
				DS.Dispose();
			}
		}	
		
		
	}
}
