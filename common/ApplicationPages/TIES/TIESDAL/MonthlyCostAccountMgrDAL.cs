using System;
using System.Collections;
using System.Data;
using System.Text;
using com.common.classes;
using com.common.DAL;
using com.common.util;
using com.ties.classes;

namespace com.ties.DAL
{
	/// <summary>
	/// 	/// Summary description for MonthlyCostAccountMgrDAL.
	/// </summary>
	public class MonthlyCostAccountMgrDAL
	{
		public MonthlyCostAccountMgrDAL()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		//Mehtods for CP - Cost Code module

		public static DataSet GetEmptyCostCodeDS(int iNumRows)
		{
			

			DataTable dtCostCode = new DataTable();
 
			dtCostCode.Columns.Add(new DataColumn("cost_code", typeof(string)));
			dtCostCode.Columns.Add(new DataColumn("cost_code_description", typeof(string)));
			dtCostCode.Columns.Add(new DataColumn("cost_amt", typeof(decimal)));


			for(int i=0; i < iNumRows; i++)
			{
				DataRow drEach = dtCostCode.NewRow();
				drEach[0] = "";
				dtCostCode.Rows.Add(drEach);
			}

			DataSet dsCostCode = new DataSet();
			dsCostCode.Tables.Add(dtCostCode);
			return  dsCostCode;
		}

		public static void AddNewRowInCostCodeDS(DataSet dsMCA)
		{
			DataRow drNew = dsMCA.Tables[0].NewRow();
			drNew[0] = "";
			dsMCA.Tables[0].Rows.Add(drNew);
		}

		public static DataSet GetCostCodeDS(String strAppID, String strEnterpriseID,  int iAccountYear, int iAccountMonth)
		{

			DataSet dsMCA = null;

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("MonthlyCostAccountMgrDAL","GetCostCodeDS","EDB101","DbConnection object is null!!");
				return dsMCA;
			}

			StringBuilder strBuilder = new StringBuilder();

			strBuilder.Append("select mca.cost_code as cost_code, cc.cost_code_description as cost_code_description, mca.cost_amt as cost_amt from Cost_Code cc, Monthly_Cost_Account mca ");
			strBuilder.Append(" where mca.applicationid = cc.applicationid and mca.enterpriseid = mca.enterpriseid and mca.cost_code = cc.cost_code and mca.applicationid = '");
			strBuilder.Append(strAppID);
			strBuilder.Append("' and mca.enterpriseid = '");
			strBuilder.Append(strEnterpriseID);
			strBuilder.Append("' and mca.account_year = ");
			strBuilder.Append(iAccountYear);
			strBuilder.Append(" and mca.account_month = ");
			strBuilder.Append(iAccountMonth);

			String strSQLQuery = strBuilder.ToString();

			dsMCA = (DataSet)dbCon.ExecuteQuery(strSQLQuery,ReturnType.DataSetType);

			return  dsMCA;
		}

		public static DataSet GetCostCodeDS(String strAppID, String strEnterpriseID, decimal iJobID)
		{

			DataSet dsMCA = null;

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("MonthlyCostAccountMgrDAL","GetCostCodeDS","EDB101","DbConnection object is null!!");
				return dsMCA;
			}

			StringBuilder strBuilder = new StringBuilder();

			strBuilder.Append("select mca.cost_code as cost_code, cc.cost_code_description as cost_code_description, mca.cost_amt as cost_amt, mca.allocated as allocated from Cost_Code cc, Monthly_Cost_Account mca ");
			strBuilder.Append(" where mca.applicationid = cc.applicationid and mca.enterpriseid = mca.enterpriseid and mca.cost_code = cc.cost_code and mca.applicationid = '");
			strBuilder.Append(strAppID);
			strBuilder.Append("' and mca.enterpriseid = '");
			strBuilder.Append(strEnterpriseID);
			strBuilder.Append("' and mca.jobid = ");
			strBuilder.Append(iJobID);

			String strSQLQuery = strBuilder.ToString();

			dsMCA = (DataSet)dbCon.ExecuteQuery(strSQLQuery,ReturnType.DataSetType);

			return  dsMCA;
		}

		public static int AddCostCodeDS(String strAppID, String strEnterpriseID,  int iAccountYear, int iAccountMonth,DataSet dsToInsert)
		{
			int iRowsAffected = 0;

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("MonthlyCostAccountMgrDAL","AddCostCodeDS","EDB101","DbConnection object is null!!");
				return iRowsAffected;
			}

			StringBuilder strBuilder = new StringBuilder();
			strBuilder.Append("insert into Monthly_Cost_Account (applicationid,enterpriseid,account_year,account_month,cost_code,cost_amt,allocated) values ('");
			strBuilder.Append(strAppID);
			strBuilder.Append("','");
			strBuilder.Append(strEnterpriseID);
			strBuilder.Append("',");
			strBuilder.Append(iAccountYear);
			strBuilder.Append(",");
			strBuilder.Append(iAccountMonth);
			strBuilder.Append(",");

			int iRowCnt = dsToInsert.Tables[0].Rows.Count;

			DataRow drEach = dsToInsert.Tables[0].Rows[0];

			if((drEach["cost_code"]!= null) && (!drEach["cost_code"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				String strCostCode = (String) drEach["cost_code"];
				strBuilder.Append("'");
				strBuilder.Append(strCostCode);
				strBuilder.Append("'");
			}
			else
			{
				strBuilder.Append("null");
			}
				
			strBuilder.Append(",");

			if((drEach["cost_amt"]!= null) && (!drEach["cost_amt"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				decimal decCostAmt = (decimal) drEach["cost_amt"];
			
				strBuilder.Append(decCostAmt);
			}
			else
			{
				strBuilder.Append("null");
			}

			strBuilder.Append(",'N')");
			
			String strSQLQuery = strBuilder.ToString();

			IDbCommand dbCmd = dbCon.CreateCommand(strSQLQuery,CommandType.Text);
			

			try
			{
				iRowsAffected = dbCon.ExecuteNonQuery(dbCmd);
				Logger.LogTraceInfo("MonthlyCostAccountMgrDAL","AddCostCodeDS","SDM001",iRowsAffected+" rows inserted in to Cost_Code table");
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("RBACManager","AddCostCodeDS","SDM001","Error During Insert "+ appException.Message.ToString());
				throw new ApplicationException("Error inserting Cost Code ",appException);
			}
			return iRowsAffected;
		}

		public static int ModifyCostCodeDS(String strAppID, String strEnterpriseID,  int iAccountYear, int iAccountMonth,DataSet dsToModify)
		{
			int iRowsAffected = 0;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("MonthlyCostAccountMgrDAL","ModifyCostCodeDS","EDB101","DbConnection object is null!!");
				return iRowsAffected;
			}
			int iRowCnt = dsToModify.Tables[0].Rows.Count;
			DataRow drEach = dsToModify.Tables[0].Rows[0];


			StringBuilder strBuilder = new StringBuilder();
			
			strBuilder.Append("update Monthly_Cost_Account set cost_amt = ");//, status_close, invoiceable,system_code) values ('");
			if((drEach["cost_amt"]!= null) && (!drEach["cost_amt"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				decimal decCostAmt = (decimal) drEach["cost_amt"];
				strBuilder.Append(decCostAmt);
			}
			else
			{
				strBuilder.Append("null");
			}

			strBuilder.Append(" where applicationid = '");
			strBuilder.Append(strAppID);
			strBuilder.Append("' and enterpriseid = '");
			strBuilder.Append(strEnterpriseID);
			strBuilder.Append("' and account_year = ");
			strBuilder.Append(iAccountYear);
			strBuilder.Append(" and account_month = ");
			strBuilder.Append(iAccountMonth);
			strBuilder.Append(" and cost_code = '");
			String strCostCode = (String) drEach["cost_code"];
			strBuilder.Append(strCostCode);
			strBuilder.Append("'");

			String strSQLQuery = strBuilder.ToString();

			IDbCommand dbCmd = dbCon.CreateCommand(strSQLQuery,CommandType.Text);

			try
			{
				iRowsAffected = dbCon.ExecuteNonQuery(dbCmd);
				Logger.LogTraceInfo("MonthlyCostAccountMgrDAL","AddCostCodeDS","SDM001",iRowsAffected+" rows inserted in to Cost_Code table");
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("RBACManager","AddCostCodeDS","SDM001","Error During Insert "+ appException.Message.ToString());
				throw new ApplicationException("Error inserting Cost Code ",appException);
			}
			return iRowsAffected;
		}


		public static int DeleteCostCodeDS(String strAppID, String strEnterpriseID,  int iAccountYear, int iAccountMonth,String strCostCode)
		{
			int iRowsAffected = 0;

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("MonthlyCostAccountMgrDAL","DeleteCostCodeDS","EDB101","DbConnection object is null!!");
				return iRowsAffected;
			}



			StringBuilder strBuilder = new StringBuilder();
			
			strBuilder.Append("delete from Monthly_Cost_Account");//, status_close, invoiceable,system_code) values ('");
			strBuilder.Append(" where applicationid = '");
			strBuilder.Append(strAppID);
			strBuilder.Append("' and enterpriseid = '");
			strBuilder.Append(strEnterpriseID);
			strBuilder.Append("' and account_year = ");
			strBuilder.Append(iAccountYear);
			strBuilder.Append(" and account_month = ");
			strBuilder.Append(iAccountMonth);
			strBuilder.Append(" and cost_code = '");
			strBuilder.Append(strCostCode);
			strBuilder.Append("'");

			String strSQLQuery = strBuilder.ToString();

			IDbCommand dbCmd = dbCon.CreateCommand(strSQLQuery,CommandType.Text);

			try
			{
				iRowsAffected = dbCon.ExecuteNonQuery(dbCmd);
				Logger.LogTraceInfo("MonthlyCostAccountMgrDAL","DeleteCostCodeDS","SDM001",iRowsAffected+" rows deleted from Monthly_Cost_Account table");
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("MonthlyCostAccountMgrDAL","DeleteCostCodeDS","SDM001","Error During Delete "+ appException.Message.ToString());
				throw new ApplicationException("Error Deleting Monthly Cost Account ",appException);
			}
			return iRowsAffected;

		}

		public static String GetCostDescription(String strAppID, String strEnterpriseID,String strCostCode)
		{
			String strCostDescription = null;

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("MonthlyCostAccountMgrDAL","GetCostCodeDS","EDB101","DbConnection object is null!!");
				return strCostDescription;
			}

			StringBuilder strBuilder = new StringBuilder();

			strBuilder.Append("select cost_code_description from Cost_Code where applicationid = '");
			strBuilder.Append(strAppID);
			strBuilder.Append("' and enterpriseid = '");
			strBuilder.Append(strEnterpriseID);
			strBuilder.Append("' and cost_code = '");
			strBuilder.Append(strCostCode);
			strBuilder.Append("'");


			String strSQLQuery = strBuilder.ToString();

			DataSet dsMCA = (DataSet)dbCon.ExecuteQuery(strSQLQuery,ReturnType.DataSetType);

			int iRowCnt = dsMCA.Tables[0].Rows.Count;
			
			if(iRowCnt > 0)
			{
				DataRow drCurrent = dsMCA.Tables[0].Rows[0];
				strCostDescription = (String)drCurrent["cost_code_description"];
			}

			return  strCostDescription;
		}

		public static int AddAllCostCodes(String strAppID, String strEnterpriseID,  int iAccountYear, int iAccountMonth)
		{
			DataSet dsCostCode = null;

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("MonthlyCostAccountMgrDAL","GetCostCodeDS","EDB101","DbConnection object is null!!");
				return -1;
			}

			StringBuilder strBuilder = new StringBuilder();

			strBuilder.Append("Select cost_code from Cost_Code where applicationid = '");
			strBuilder.Append(strAppID);
			strBuilder.Append("' and enterpriseid = '");
			strBuilder.Append(strEnterpriseID);
			strBuilder.Append("' ");

			if(iAccountYear != -1 && iAccountMonth != -1)
			{
				strBuilder.Append(" and cost_code not in (select distinct cost_code from Monthly_Cost_Account where applicationid = '");
				strBuilder.Append(strAppID);
				strBuilder.Append("' and enterpriseid = '");
				strBuilder.Append(strEnterpriseID);
				strBuilder.Append("' and account_year = ");
				strBuilder.Append(iAccountYear);
				strBuilder.Append(" and account_month = ");
				strBuilder.Append(iAccountMonth);
				strBuilder.Append(" ) ");
			}

			strBuilder.Append(" ORDER BY cost_code");

			String strSQLQuery = strBuilder.ToString();

			dsCostCode = (DataSet)dbCon.ExecuteQuery(strSQLQuery,ReturnType.DataSetType);

			int iRowCount = dsCostCode.Tables[0].Rows.Count;

			ArrayList queryList = new ArrayList();

			for(int i = 0; i < iRowCount; i++)
			{
				StringBuilder strInsertQueryBuilder = new StringBuilder();

				strInsertQueryBuilder.Append("insert into Monthly_Cost_Account (applicationid,enterpriseid,account_year,account_month,cost_code,allocated) values ('");
				strInsertQueryBuilder.Append(strAppID);
				strInsertQueryBuilder.Append("','");
				strInsertQueryBuilder.Append(strEnterpriseID);
				strInsertQueryBuilder.Append("',");
				strInsertQueryBuilder.Append(iAccountYear);
				strInsertQueryBuilder.Append(",");
				strInsertQueryBuilder.Append(iAccountMonth);
				strInsertQueryBuilder.Append(",");

				

				DataRow drEach = dsCostCode.Tables[0].Rows[i];

				if((drEach["cost_code"]!= null) && (!drEach["cost_code"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					String strCostCode = (String) drEach["cost_code"];
					strInsertQueryBuilder.Append("'");
					strInsertQueryBuilder.Append(strCostCode);
					strInsertQueryBuilder.Append("'");
				}

				strInsertQueryBuilder.Append(",'N')");
				queryList.Add(strInsertQueryBuilder.ToString());

			}

			dbCon.ExecuteBatchSQLinTranscation(queryList);

			return iRowCount;

		}
			


		//Methods used in ShipmentCost module

		public static DataSet GetEmptyCostCodeDS()
		{
			DataTable dtCostCode = new DataTable();
 
			dtCostCode.Columns.Add(new DataColumn("cost_code", typeof(string)));
			dtCostCode.Columns.Add(new DataColumn("cost_code_description", typeof(string)));
			dtCostCode.Columns.Add(new DataColumn("cost_amt", typeof(decimal)));
			dtCostCode.Columns.Add(new DataColumn("allocated", typeof(string)));

			DataRow drEach = dtCostCode.NewRow();
			dtCostCode.Rows.Add(drEach);

			DataSet dsCostCode = new DataSet();
			dsCostCode.Tables.Add(dtCostCode);
			return  dsCostCode;
		}



		public static DataSet GetMCADS(String strAppID, String strEnterpriseID,  int iAccountYear, short iAccountMonth)
		{

			DataSet dsMCA = null;

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("MonthlyCostAccountMgrDAL","GetCostCodeDS","EDB101","DbConnection object is null!!");
				return dsMCA;
			}

			StringBuilder strBuilder = new StringBuilder();

			strBuilder.Append("select cost_code, cost_amt from Monthly_Cost_Account");
			strBuilder.Append(" where applicationid = '");
			strBuilder.Append(strAppID);
			strBuilder.Append("' and enterpriseid = '");
			strBuilder.Append(strEnterpriseID);
			strBuilder.Append("' and account_year = ");
			strBuilder.Append(iAccountYear);
			strBuilder.Append(" and account_month = ");
			strBuilder.Append(iAccountMonth);

			String strSQLQuery = strBuilder.ToString();

			dsMCA = (DataSet)dbCon.ExecuteQuery(strSQLQuery,ReturnType.DataSetType);

			return  dsMCA;
		}

		public static int GetMCARecordCount(String strAppID, String strEnterpriseID,  int iAccountYear, short iAccountMonth)
		{
			int iMCARecordCount = -1;


			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("MonthlyCostAccountMgrDAL","GetCostCodeDS","EDB101","DbConnection object is null!!");
				return iMCARecordCount;
			}

			StringBuilder strBuilder = new StringBuilder();

			strBuilder.Append("select count(*) as cnt from Monthly_Cost_Account");
			strBuilder.Append(" where applicationid = '");
			strBuilder.Append(strAppID);
			strBuilder.Append("' and enterpriseid = '");
			strBuilder.Append(strEnterpriseID);
			strBuilder.Append("' and account_year = ");
			strBuilder.Append(iAccountYear);
			strBuilder.Append(" and account_month = ");
			strBuilder.Append(iAccountMonth);

			String strSQLQuery = strBuilder.ToString();

			iMCARecordCount  = 0;

			Object objMCARecordCount = dbCon.ExecuteScalar(strSQLQuery);

			if(Utility.IsNotDBNull(objMCARecordCount) && objMCARecordCount!=null)
			{
				iMCARecordCount  = System.Convert.ToInt32(objMCARecordCount);
			}

			return iMCARecordCount;			
		}

		public static long GetMCARecordJobID(String strAppID, String strEnterpriseID,  int iAccountYear, short iAccountMonth)
		{
			long iMCARecordJobID = -1;


			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("MonthlyCostAccountMgrDAL","GetCostCodeDS","EDB101","DbConnection object is null!!");
				return iMCARecordJobID;
			}

			StringBuilder strBuilder = new StringBuilder();

			strBuilder.Append("select distinct jobid from Monthly_Cost_Account");
			strBuilder.Append(" where applicationid = '");
			strBuilder.Append(strAppID);
			strBuilder.Append("' and enterpriseid = '");
			strBuilder.Append(strEnterpriseID);
			strBuilder.Append("' and account_year = ");
			strBuilder.Append(iAccountYear);
			strBuilder.Append(" and account_month = ");
			strBuilder.Append(iAccountMonth);

			String strSQLQuery = strBuilder.ToString();


			Object objMCARecordJobID = dbCon.ExecuteScalar(strSQLQuery);

			if(Utility.IsNotDBNull(objMCARecordJobID))
			{
				iMCARecordJobID  = (long)objMCARecordJobID;
			}

			return iMCARecordJobID;			
		}

		public static int AddModifyMCAShipmentCost(String strAppID, String strEnterpriseID, MonthlyCostAccount structMCA, ShipmentCost structShipmentCost)
		{
			ArrayList QueryList = new ArrayList();

			QueryList.Add(FormShipmentCostInsertQuery(strAppID,strEnterpriseID,structShipmentCost));
			QueryList.Add(FormMCAUpdateQuery(strAppID,strEnterpriseID,structMCA));

			int iRows = 0;
			DbConnection dbConApp = null;
			
			//Get App dbConnection
			dbConApp = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbConApp == null)
			{
				Logger.LogTraceError("RBAC","AddModifyMCAShipmentCost","ERR0001","dbConApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			try
			{
				if(QueryList != null)
				{
					iRows = dbConApp.ExecuteBatchSQLinTranscation(QueryList);	
				}
				
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("RBAC","AddModifyMCAShipmentCost","ERR0002","Transaction Failed during batch execution : "+ appException.Message.ToString());
				throw new ApplicationException("Error during batch execution "+appException.Message,appException);
			}
			
			return iRows;
		}


		private static String FormMCAUpdateQuery(String strAppID, String strEnterpriseID, MonthlyCostAccount structMCA)
		{
			StringBuilder strBuilder = new StringBuilder();
			strBuilder.Append("update Monthly_Cost_Account set allocated = '");
			strBuilder.Append(structMCA.strAllocated);
			strBuilder.Append("', jobid = ");
			strBuilder.Append(structMCA.iJobID);
			strBuilder.Append(" where applicationid = '");
			strBuilder.Append(strAppID);
			strBuilder.Append("' and enterpriseid = '");
			strBuilder.Append(strEnterpriseID);
			strBuilder.Append("' and account_year = ");
			strBuilder.Append(structMCA.iAccountYear);
			strBuilder.Append(" and account_month = ");
			strBuilder.Append(structMCA.iAccountMonth);
			strBuilder.Append(" and cost_code = '");
			strBuilder.Append(structMCA.strCostCode);
			strBuilder.Append("'");

			return strBuilder.ToString();
		}

		private static String FormShipmentCostInsertQuery(String strAppID, String strEnterpriseID, ShipmentCost structShipmentCost)
		{
			StringBuilder strBuilder = new StringBuilder();

			strBuilder.Append("insert into Shipment_Cost (applicationid,enterpriseid,Booking_No,consignment_no,cost_code,amt_allocated_consignment, amt_allocated_wt, amt_allocated_vol,jobid) values ('");
			strBuilder.Append(strAppID);
			strBuilder.Append("','");
			strBuilder.Append(strEnterpriseID);
			strBuilder.Append("',");
			strBuilder.Append(structShipmentCost.iBookingNo);
			strBuilder.Append(",'");
			strBuilder.Append(structShipmentCost.strConsignmentNo);
			strBuilder.Append("','");
			strBuilder.Append(structShipmentCost.strCostCode);
			strBuilder.Append("',");
			strBuilder.Append(structShipmentCost.decAmtAllocByConsignment);
			strBuilder.Append(",");
			strBuilder.Append(structShipmentCost.decAmtAllocByWt);
			strBuilder.Append(",");
			strBuilder.Append(structShipmentCost.decAmtAllocByDimWt);
			strBuilder.Append(",");
			strBuilder.Append(structShipmentCost.iJobID);
			strBuilder.Append(")");

			return strBuilder.ToString();
		}
	}
}
