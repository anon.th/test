using System;
using System.Text;
using System.Data;
using com.common.classes;
using com.common.DAL;
using com.common.util;
using System.Data.SqlClient;

namespace com.ties.DAL
{
	/// <summary>
	/// Summary description for CustomerCreditused.
	/// </summary>
	public class CustomerCreditused
	{
		public string applicationid = "";
		public string enterpriseid = "";
		public string cusid = "";
		public string consignment_no = "";
		public string invoice_no = "";
		public string credit_no = "";
		public string debit_no = "";
		public string transcode = "";
		public decimal amount = 0;
		public string updated_by = "";
		public string process = "";
		public string bookingNo = "";


		public CustomerCreditused()
		{
			//
			// TODO: Add constructor logic here
			//
		}



		public  void updateCreditused()
		{
			try
			{
				string sql;
				sql = " insert into Customer_Creditused(applicationid,enterpriseid,cusid, ";
				sql +=" consignment_no,invoice_no,credit_no, ";
				sql +=" debit_no,transcode,amount, ";
				sql +=" updated_date,updated_by) ";
				sql += " values ";
				sql += " ('" + applicationid  + "','" + enterpriseid +"','" + cusid  + "','";
				sql += consignment_no +"','" + invoice_no + "' ,'" + credit_no + "',";
				sql +=  "'" + debit_no + "','" + transcode + "',+" + amount + ",";
				sql += "getdate(),'Automatic Update')";
//			
//			    
//				IDbCommand dbCom = null;
//				DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(applicationid,enterpriseid);
//				IDbConnection conApp = dbCon.GetConnection();
//			
//				
//				
//				if(dbCon == null)
//				{
//					Logger.LogTraceError("SysDataManager2","UpdateZone","SDMG001","dbCon is null");
//					throw new ApplicationException("Connection to Database failed",null);
//				}
//				dbCom = dbCon.CreateCommand(sql,CommandType.Text);
//				dbCom.ExecuteNonQuery();

				DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(applicationid,enterpriseid);
				IDbConnection conApp = dbCon.GetConnection();
				SqlConnection strcon = new SqlConnection(conApp.ConnectionString);
				strcon.Open();
			
					// insert new entry into table
					SqlCommand ssql =new SqlCommand(sql,strcon);
				
					//ssql.ExecuteNonQuery();
					//				dbExcute1 = dbCon.CreateCommand(ssql,CommandType.Text);
					//				dbCon.ExecuteNonQuery(dbExcute1);
					strcon.Close();
			}
			catch(Exception ex)
			{
				//throw new Exception(ex.Message);
			}
		}
		public double getcreditUsedinvoice()
		{
			DbConnection dbCon = null;
			IDbCommand dbCmd = null;
			String strQry = "";
	
			DataSet  roleData;
			
			dbCon = DbConnectionManager.GetInstance().GetDbConnection(applicationid,enterpriseid);
			//			dbCmd = dbCon.CreateCommand(strQry,CommandType.Text);
			//			DataSet  roleData = (DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
			//cnt = roleData.Tables[0].Rows.Count;
			
				//return double.Parse(roleData.Tables[0].Rows[0]["creditlimit"].ToString());
				//string macc = roleData.Tables[0].Rows[0]["master_account"].ToString();
				dbCmd = null;

				strQry = " select sum(id.mbg_amount) as amtin from Invoice_Detail id ";
				strQry += "  where id.invoice_no ='" + invoice_no +  "'";
				strQry += " and applicationid = '" + applicationid  + "' ";
				strQry += " and enterpriseid = '" + enterpriseid + "' ";

				dbCmd = dbCon.CreateCommand(strQry,CommandType.Text);
				roleData = (DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
				if(roleData.Tables[0].Rows.Count > 0)
				{
					double invoiceamt = 0;
					double sumexcp = 0;
					double totalall = 0;
					if(roleData.Tables[0].Rows[0]["amtin"].ToString().Length > 0)
					{
						 invoiceamt = double.Parse(roleData.Tables[0].Rows[0]["amtin"].ToString());
						strQry = " select sum(sh.total_excp_charges) as sumexcp from Shipment sh  ";
						strQry += "	inner join Invoice_Detail id " ;
						strQry += "	on id.consignment_no = sh.consignment_no ";
						strQry += "	inner join pickup_request pr " ;
						strQry += "	on pr.payerid = sh.payerid  ";
						strQry += "	where 	pr.payerid = '"  + cusid  + "' " ;
						strQry += "	and  sh.consignment_no in " ;
						strQry += "	(select  Invoice_Detail.consignment_no	from Invoice_Detail " ;
						strQry += "	where Invoice_Detail.invoice_no = '" + invoice_no + "')" ;
						dbCmd = dbCon.CreateCommand(strQry,CommandType.Text);
						roleData = (DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
						if(roleData.Tables[0].Rows[0]["sumexcp"].ToString().Length > 0)
						{
							sumexcp = double.Parse(roleData.Tables[0].Rows[0]["sumexcp"].ToString());
						}
						totalall = sumexcp - totalall;
						return totalall;
					}
				}
				return 0;
		}


		public DataTable getinfoShipment()
		{
			DbConnection dbCon = null;
			IDbCommand dbCmd = null;
			String strQry = "";
	
			DataSet  roleData;
			
			dbCon = DbConnectionManager.GetInstance().GetDbConnection(applicationid,enterpriseid);
			//			dbCmd = dbCon.CreateCommand(strQry,CommandType.Text);
			//			DataSet  roleData = (DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
			//cnt = roleData.Tables[0].Rows.Count;
			
			//return double.Parse(roleData.Tables[0].Rows[0]["creditlimit"].ToString());
			//string macc = roleData.Tables[0].Rows[0]["master_account"].ToString();
			dbCmd = null;

			strQry = " select invoice_no,isnull(total_rated_amount,0) as total_rated_amount,payerid from shipment ";
			strQry += "  where applicationid = '"  + applicationid + "' ";
			strQry += " and enterpriseid = '" + enterpriseid + "' ";
			//strQry += " and booking_no = '" + bookingNo + "' ";
			strQry += " and consignment_no = '" + consignment_no + "' ";

			dbCmd = dbCon.CreateCommand(strQry,CommandType.Text);
			roleData = (DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);

			return roleData.Tables[0];
			
		}

		public double getTotalCreditUsed(string strAppID, string strEnterpriseID,string strChildCustID)
		{
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			String strQry = "";
	
			DataSet  roleData;

			//HAS Master ACC?
			strQry = " select master_account,credit_limit,credit_used from customer where customer.custid='"+ strChildCustID +"'";
			roleData = (DataSet)dbCon.ExecuteQuery(strQry.ToString(),com.common.DAL.ReturnType.DataSetType);
			if(roleData.Tables[0].Rows.Count > 0)
			{
				DataRow drCurrent = roleData.Tables[0].Rows[0];
				if(drCurrent["credit_used"].ToString().Length==0)
					drCurrent["credit_used"] = 0;

				if(drCurrent["master_account"]!= null && (!drCurrent["master_account"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					if(drCurrent["master_account"].ToString()!="")
					{
						//If yes, Is it Limit Credit Used by Master?
						strQry = " select isnull(sum(customer.credit_used),0) as credit_used, isnull(customer_credit.MACC_Limits_credit_used,'N') as MACC_Limits_credit_used, customer.master_account from customer_credit inner join ( select  convert(varchar(19),A.UpdatedDate,121) as updateddate from ( ";
						strQry += "	select custid, max(updateddate) as updateddate from customer_credit where ismasteraccount='Y' group by custid) A inner join customer_credit on A.custid = customer_credit.custid ";
						strQry += "	and A.updateddate = customer_credit.updateddate ) B	on customer_credit.updateddate = B.updateddate inner join customer on customer.master_account=customer_credit.custid where customer.master_account='"+ drCurrent["master_account"].ToString() +"'";
						strQry += " group by customer_credit.custid,customer_credit.creditlimit,customer_credit.creditstatus,customer_credit.updateddate, customer_credit.MACC_Limits_credit_used, customer.master_account ";

						roleData = (DataSet)dbCon.ExecuteQuery(strQry.ToString(),com.common.DAL.ReturnType.DataSetType);
						if(roleData.Tables[0].Rows.Count > 0)
						{
							DataRow drMaster = roleData.Tables[0].Rows[0];
							//if it's not MACC_Credit Limit USED, return Customer's Value
							if(drMaster["master_account"]!= null && (!drMaster["master_account"].GetType().Equals(System.Type.GetType("System.DBNull"))))
							{
								if(drMaster["MACC_Limits_credit_used"].ToString()=="Y")
								{	
									return double.Parse(drMaster["credit_used"].ToString());
								}
								else
									return double.Parse(drCurrent["credit_used"].ToString());
							}
							
						}
						else
							return double.Parse(drCurrent["credit_used"].ToString());
					}
					else
                        return double.Parse(drCurrent["credit_used"].ToString());

				}
				else
					return double.Parse(drCurrent["credit_used"].ToString());
			}
			return 0;
		}

		public double getTotalCreditLimit(string strAppID, string strEnterpriseID,string strChildCustID)
		{
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			String strQry = "";
	
			DataSet  roleData;

			//HAS Master ACC?
			strQry = " select master_account,credit_limit,credit_used from customer where customer.custid='"+ strChildCustID +"'";
			roleData = (DataSet)dbCon.ExecuteQuery(strQry.ToString(),com.common.DAL.ReturnType.DataSetType);
			if(roleData.Tables[0].Rows.Count > 0)
			{
				DataRow drCurrent = roleData.Tables[0].Rows[0];
				if(drCurrent["credit_limit"].ToString().Length==0)
					drCurrent["credit_limit"] = 0;

				if(drCurrent["master_account"]!= null && (!drCurrent["master_account"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					if(drCurrent["master_account"].ToString()!="")
					{
						//If yes, Is it Limit Credit Used by Master?
						strQry = " select isnull(sum(customer.credit_used),0) as credit_used, customer_credit.creditlimit,isnull(customer_credit.MACC_Limits_credit_used,'N') as MACC_Limits_credit_used, customer.master_account from customer_credit inner join ( select  convert(varchar(19),A.UpdatedDate,121) as updateddate from ( ";
						strQry += "	select custid, max(updateddate) as updateddate from customer_credit where ismasteraccount='Y' group by custid) A inner join customer_credit on A.custid = customer_credit.custid ";
						strQry += "	and A.updateddate = customer_credit.updateddate ) B	on customer_credit.updateddate = B.updateddate inner join customer on customer.master_account=customer_credit.custid where customer.master_account='"+ drCurrent["master_account"].ToString() +"'";
						strQry += " group by customer_credit.custid,customer_credit.creditlimit,customer_credit.creditstatus,customer_credit.updateddate, customer_credit.MACC_Limits_credit_used, customer.master_account ";

						roleData = (DataSet)dbCon.ExecuteQuery(strQry.ToString(),com.common.DAL.ReturnType.DataSetType);
						if(roleData.Tables[0].Rows.Count > 0)
						{
							DataRow drMaster = roleData.Tables[0].Rows[0];
							//if it's not MACC_Credit Limit USED, return Customer's Value
							if(drMaster["master_account"]!= null && (!drMaster["master_account"].GetType().Equals(System.Type.GetType("System.DBNull"))))
							{
								if(drMaster["MACC_Limits_credit_used"].ToString()=="Y")
									return double.Parse(drMaster["creditlimit"].ToString());
								else
									return double.Parse(drCurrent["credit_limit"].ToString());

							}
						}
						else
							return double.Parse(drCurrent["credit_limit"].ToString());
					}
					else
						return double.Parse(drCurrent["credit_limit"].ToString());

				}
				else
					return double.Parse(drCurrent["credit_limit"].ToString());
			}
			return 0;
		}
//		public double getTotalCreditLimit(string strAppID, string strEnterpriseID,string strChildCustID)
//		{
//			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
//			String strQry = "";
//	
//			DataSet  roleData;
//
//			strQry = " select isnull(customer_credit.creditlimit,0) as CreditLimit from customer_credit, isnull(customer_credit.MACC_Limits_credit_used,'N') as MACC_Limits_credit_used, customer.master_account inner join ( select  convert(varchar(19),A.UpdatedDate,121) as updateddate from ( ";
//			strQry += "	select custid, max(updateddate) as updateddate from customer_credit where ismasteraccount='Y' group by custid) A inner join customer_credit on A.custid = customer_credit.custid ";
//			strQry += "	and A.updateddate = customer_credit.updateddate ) B	on customer_credit.updateddate = B.updateddate inner join customer on customer.master_account=customer_credit.custid where customer.custid='"+ strChildCustID +"'";
//			strQry += " group by customer_credit.custid,customer_credit.creditlimit,customer_credit.creditstatus,customer_credit.updateddate, customer_credit.MACC_Limits_credit_used, customer.master_account ";
//
//			roleData = (DataSet)dbCon.ExecuteQuery(strQry.ToString(),com.common.DAL.ReturnType.DataSetType);
//			if(roleData.Tables[0].Rows.Count > 0)
//			{
//				return double.Parse(roleData.Tables[0].Rows[0]["creditlimit"].ToString());
//			}
//			return 0;
//		}
//
		public string getCustIDfromInvoice()
		{
			DbConnection dbCon = null;
			IDbCommand dbCmd = null;
			String strQry = "";
	
			DataSet  roleData;
			
			dbCon = DbConnectionManager.GetInstance().GetDbConnection(applicationid,enterpriseid);
			dbCmd = null;

			strQry = " select top 1 Invoice.payerid from Invoice "; 
			strQry += "	  where Invoice.invoice_no = '" + invoice_no + "'";


			dbCmd = dbCon.CreateCommand(strQry,CommandType.Text);
			roleData = (DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
			if (roleData.Tables[0].Rows.Count  > 0)
			{
				return roleData.Tables[0].Rows[0]["payerid"].ToString();
			}
			else
			{
				return "";
			}
			
			
		}

		public string getcusidfordebitnode()
		{
			DbConnection dbCon = null;
			IDbCommand dbCmd = null;
			String strQry = "";
	
			DataSet  roleData;
			
			dbCon = DbConnectionManager.GetInstance().GetDbConnection(applicationid,enterpriseid);
			//			dbCmd = dbCon.CreateCommand(strQry,CommandType.Text);
			//			DataSet  roleData = (DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
			//cnt = roleData.Tables[0].Rows.Count;
			
			//return double.Parse(roleData.Tables[0].Rows[0]["creditlimit"].ToString());
			//string macc = roleData.Tables[0].Rows[0]["master_account"].ToString();
			dbCmd = null;

			strQry = " select top 1 Invoice.payerid from Invoice "; 
			strQry += "	  inner join debit_note ";
			strQry += "	  on Invoice.invoice_no = debit_note.invoice_no ";
			strQry += "	  where debit_note.invoice_no = '" + invoice_no + "'";


			dbCmd = dbCon.CreateCommand(strQry,CommandType.Text);
			roleData = (DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
			if (roleData.Tables[0].Rows.Count  > 0)
			{
				return roleData.Tables[0].Rows[0]["payerid"].ToString();
			}
			else
			{
				return "";
			}
			
			
		}

		public DataTable getdebit_info()
		{
			DbConnection dbCon = null;
			IDbCommand dbCmd = null;
			String strQry = "";
	
			DataSet  roleData;
			
			dbCon = DbConnectionManager.GetInstance().GetDbConnection(applicationid,enterpriseid);
			//			dbCmd = dbCon.CreateCommand(strQry,CommandType.Text);
			//			DataSet  roleData = (DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
			//cnt = roleData.Tables[0].Rows.Count;
			
			//return double.Parse(roleData.Tables[0].Rows[0]["creditlimit"].ToString());
			//string macc = roleData.Tables[0].Rows[0]["master_account"].ToString();
			dbCmd = null;

			strQry = " select sum(isnull(debit_amt,0)) as sumdebitamt  ,dm.status   ";
			strQry += " from debit_note_Detail dd  ";
			strQry += " inner join debit_note  dm  ";
			strQry += " on dm.debit_no = dd.debit_no  ";
			strQry += " where  dm.debit_no ='" + debit_no + "'";
			strQry += " group by dm.debit_no,dm.status  ";

			dbCmd = dbCon.CreateCommand(strQry,CommandType.Text);
			roleData = (DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);

			return roleData.Tables[0];
			
		}

	}
}
