using System;
using System.Data;
using System.Collections;
using System.Text;
using com.common.DAL;
using com.common.util;
using com.common.classes;
using com.ties.classes;

namespace com.ties.DAL
{
	/// <summary>
	/// Summary description for QuickPODMgrDAL.
	/// </summary>
	public class QuickPODMgrDAL
	{
		public QuickPODMgrDAL()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		public static DataSet QueryQuickPOD(string strAppID,string strEnterpriseID,string ConsignmentNo)
		{
			IDbCommand dbCmd = null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);
			System.Collections.ArrayList storedParams = new System.Collections.ArrayList();
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@enterpriseid",strEnterpriseID));

			//Check Consignment Number
			if(ConsignmentNo == "")
			{
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@consignment_no",DBNull.Value));
			}
			else
			{
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@consignment_no",ConsignmentNo));
			}

			dbCmd = dbCon.CreateCommand("QuickPODQuery",storedParams, CommandType.StoredProcedure);
			DataSet result = (DataSet)dbCon.ExecuteQuery(dbCmd, com.common.DAL.ReturnType.DataSetType);
			return result;
		}

		public static DataSet ReadOriginDCData(String strAppID, String strEnterpriseID)
		{	
			DbConnection dbCon= null;
			IDbCommand dbcmd=null;
			DataSet dsDt = null;

			dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if (dbCon==null)
			{
				Logger.LogTraceError("QuickPODMgrDAL.cs","CheckNumberOfPackages","ERR001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			StringBuilder strBuilder = new StringBuilder();
			strBuilder.Append("select origin_code from Distribution_center where applicationid = '"+strAppID+"' and enterpriseid = '"+strEnterpriseID+"' order by origin_code ");
			
			String strSQLQuery = strBuilder.ToString();
			dbcmd = dbCon.CreateCommand(strSQLQuery,CommandType.Text);
 
			try
			{
				dsDt = (DataSet)dbCon.ExecuteQuery(dbcmd,com.common.DAL.ReturnType.DataSetType);  
				//returnNumberOfPackages =  Convert.ToInt16(dsDt.Tables[0].Rows[0]["code_num_value"]);
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("QuickPODMgrDAL.cs","CheckNumberOfPackages","ERR002","Error in Query String "+appException.Message);
				throw appException;
			}
			return dsDt;		
		}

		public static int CheckNumberOfPackages(String strAppID, String strEnterpriseID)
		{	
			DbConnection dbCon= null;
			IDbCommand dbcmd=null;
			DataSet dsDt = null;
			int returnNumberOfPackages = 0;

			dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if (dbCon==null)
			{
				Logger.LogTraceError("QuickPODMgrDAL.cs","CheckNumberOfPackages","ERR001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			StringBuilder strBuilder = new StringBuilder();
			strBuilder.Append("select code_num_value from Core_System_Code where applicationid = '"+strAppID+"' and codeid = 'PackageDetailsLimits' and culture = 'PNGAF'");
			
			String strSQLQuery = strBuilder.ToString();
			dbcmd = dbCon.CreateCommand(strSQLQuery,CommandType.Text);
 
			try
			{
				dsDt = (DataSet)dbCon.ExecuteQuery(dbcmd,com.common.DAL.ReturnType.DataSetType);  
				returnNumberOfPackages =  Convert.ToInt16(dsDt.Tables[0].Rows[0]["code_num_value"]);
		}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("QuickPODMgrDAL.cs","CheckNumberOfPackages","ERR002","Error in Query String "+appException.Message);
				throw appException;
			}
			return returnNumberOfPackages;		
		}
		
		public static DataSet SaveQuickPOD(string strAppID,string strEnterpriseID,string struserID, string ConsignmentNo, string originDC, string destZipCode, string serviceType, string numberOfPgs, string podDate, string consigneeName, string remark)
		{
			IDbCommand dbCmd = null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);

			DateTime convertPodDate = DateTime.ParseExact(podDate,"dd/MM/yyyy HH:mm",null);

			System.Collections.ArrayList storedParams = new System.Collections.ArrayList();
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@enterpriseid",strEnterpriseID));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@userid",struserID));

			//Check Consignment Number
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@consignment_no",ConsignmentNo));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@Origin_DC",originDC));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@Destination_zipcode",destZipCode));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@Service_type",serviceType));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@Number_of_packages",numberOfPgs));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@POD_date",convertPodDate));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@consignee_name",consigneeName));
			if(remark == "")
			{
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@remark",DBNull.Value));
			} 
			else
			{
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@remark",remark));			
			}
		
			dbCmd = dbCon.CreateCommand("QuickPODUpdate",storedParams, CommandType.StoredProcedure);
			DataSet result = (DataSet)dbCon.ExecuteQuery(dbCmd, com.common.DAL.ReturnType.DataSetType);
			return result;
		}
		
		public static DataSet SearchAllServiceAvailable(String appID, String enterpriseID, String OriginDC, String DestPostCode)
		{
			DbConnection dbCon = null;
			IDbCommand dbCmd = null;
			DataSet DS = null;

			dbCon = DbConnectionManager.GetInstance().GetDbConnection(appID, enterpriseID);
  
			if (dbCon == null)
			{
				Logger.LogTraceError("TIESDAL", "QuickPODMgrDAL", "SearchAllServiceAvailable", "dbCon is null");
				throw new ApplicationException("Connection to Database failed", null); 
			}

			StringBuilder strQry = new StringBuilder();
			strQry = strQry.Append("SELECT service_code FROM dbo.AllServiceAvailable ('" + enterpriseID + "', '" + OriginDC + "', '" + DestPostCode + "') ORDER BY service_charge_amt ");
//			strQry = strQry.Append("WHERE enterpriseid = N'" + enterpriseID + "' ");// AND recipientZipCode = '77110'");
//			
//			strQry = strQry.Append(" AND origin_dc = N'" + OriginDC + "' ");
//
//			strQry = strQry.Append(" AND recipientZipCode = N'" + RecipientZipCode + "' ");

			dbCmd = dbCon.CreateCommand(strQry.ToString(), CommandType.Text);
			try
			{
				DS = (DataSet)dbCon.ExecuteQuery(dbCmd, com.common.DAL.ReturnType.DataSetType);
				return DS;
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("TIESDAL", "QuickPODMgrDAL", "SearchAllServiceAvailable", "Error in Query String " + appException.Message);
				throw appException;
			}
			finally
			{
				DS.Dispose();
			}
		}
	}
}
