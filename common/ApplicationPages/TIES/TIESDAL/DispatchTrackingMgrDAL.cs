using System;
using System.Data;
using System.Collections;
using System.Text;
using com.common.DAL;
using com.common.util;
using com.common.classes;
using com.ties.classes;
namespace com.ties.DAL
{
	/// <summary>
	/// Summary description for DispatchTrackingMgrDAL.
	/// </summary>
	public class DispatchTrackingMgrDAL
	{
		public DispatchTrackingMgrDAL()
		{
			//
			// TODO: Add constructor logic here
			//
		}
		//---------Dispatch Tracking----------
		/// <summary>
		/// To query from Pickup Request
		/// </summary>
		/// <param name="strStatusArray">An array of type String that contain the status code</param>
		/// <param name="dsDispatch">DataSet taht contains the data for query</param>
		/// <param name="strAppID">ApplicationID</param>
		/// <param name="strEnterpriseID">EnterpriseID</param>
		/// <param name="recStart">Start of record</param>
		/// <param name="recSize">Size of Record</param>
		/// <returns></returns>
		public static SessionDS QueryDispatch(String[] strStatusArray, DataSet dsDispatch, String strAppID, String strEnterpriseID, int recStart,int recSize,String strCulture)
		{
			DataProviderType dbProvider = DbConnectionManager.GetInstance().GetDbProviderType(strAppID,strEnterpriseID);
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			DataRow dr = dsDispatch.Tables[0].Rows[0];
			String strBookNo = dr[0].ToString();
			String strSender = dr[1].ToString();
			String strStatus = dr[2].ToString();
			String strException = dr[3].ToString();
			String strDispatchType = dr[8].ToString();
			//14Jan09;GwanG
			String strPickupRoute = dr["Pickup_Route"].ToString();
			//Added by Tom 9/6/09
			String strODC = dr["Origin_Distribution_Center"].ToString();
			//End add by Tom 9/6/09
			String strQuery=null;
			switch (dbProvider)
			{
				case DataProviderType.Sql :
					strQuery = "select a.booking_no, a.booking_datetime, a.payerid ";
					strQuery += " ,b.code_text as payment_mode";
					strQuery += " ,(select top 1 status_code ";
					strQuery += " from dispatch_tracking with(nolock) ";
					strQuery += " where applicationid = '"+strAppID+"' and enterpriseid = '"+strEnterpriseID+"'  ";
					strQuery += " and booking_no = a.booking_no and tracking_datetime = (select max(tracking_datetime) as  tracking_datetime ";
					strQuery += " from dispatch_tracking with(nolock) where applicationid = '"+strAppID+"' and enterpriseid = '"+strEnterpriseID+"' ";
					//Edit By Tom 29/6/09
//					strQuery += " and booking_no = a.booking_no ";
					strQuery += " and booking_no = a.booking_no   and  ( deleted  = 'N' or  deleted is  null ) ";  //Jeab 5 Sep 2011
					if(strStatusArray!=null)
					{
						String strAllStatusCodes = "(";
						int arrayCount = strStatusArray.Length;
						for(int i=0; i<arrayCount; i++)
						{
							strAllStatusCodes +=  "'"+strStatusArray[i]+"'" ;
							if(i < (arrayCount-1))
							{
								strAllStatusCodes += ",";
							}
							else
							{
								strAllStatusCodes += ") ";
							}
						}
						strQuery += " and status_code in "+ strAllStatusCodes; //Utility.ReplaceSingleQuote(strAllStatusCodes)+" ";
					}
					strQuery += ")) as latest_status_code";
					//End Edit By Tom 29/6/09
					strQuery += " , a.latest_exception_code ";
					strQuery += " , a.booking_type ";
					strQuery += " , a.sender_name, isnull(a.sender_address1, '') +' '+ isnull(a.sender_address2, '') as sender_address ";
					strQuery += " , a.req_pickup_datetime, a.act_pickup_datetime ";
					strQuery += " , a.tot_pkg, a.tot_wt, a.cash_amount, a.cash_collected, a.remark ,a.pickup_route , d.last_userid, a.sender_zipcode  ";  //Jeab 8 Sep 2011
					break;
				case DataProviderType.Oracle :
					strQuery = "select booking_no, booking_datetime, core_system_code.code_text as payer_type,(select top 1 status_code from dispatch_tracking with(nolock) where applicationid = '"+strAppID+"' and enterpriseid = '"+strEnterpriseID+"' and booking_no like '%"+Utility.ReplaceSingleQuote(strBookNo)+"%' order by tracking_datetime desc )as latest_status_code, latest_status_code, latest_exception_code, booking_type, sender_name, sender_address1 ||' '|| sender_address2 as sender_address, req_pickup_datetime, act_pickup_datetime, tot_pkg, tot_wt, cash_amount, cash_collected, remark ,pickup_route , d.last_userid   ";  //Jeab 8 Sep 2011
					break;
			}
			strQuery += " from Pickup_Request a with(nolock) inner join core_system_code b with(nolock) on a.payment_mode = b.code_str_value ";
			//Added by Tom 9/6/09
//			strQuery += " inner join Route_Code c with(nolock) on a.pickup_route = c.route_code ";
			strQuery += " inner join Delivery_Path c with(nolock) on a.pickup_route = c.path_code ";
			//End added by Tom 9/6/09
			//Jeab 8 Sep 2011
				strQuery += " inner  join  Dispatch_Tracking d with(nolock) ";
				strQuery += " on a.booking_no = d.booking_no  and  a.latest_status_code = d.status_code  and  a.latest_status_datetime = d.tracking_datetime ";
			//Jeab 8 Sep 2011  =========> End
			strQuery += " where a.applicationid = '"+strAppID+"' ";
			strQuery += " and a.enterpriseid = '"+strEnterpriseID+"'  ";
			//strQuery += " and b.code_str_value = 'C'";
			strQuery += " and b.culture = '"+strCulture+"' ";
			strQuery += " and b.codeid = 'payment_mode' ";

			if(strStatusArray!=null)
			{
				String strAllStatusCodes = "(";
				int arrayCount = strStatusArray.Length;
				for(int i=0; i<arrayCount; i++)
				{
					strAllStatusCodes +=  "'"+strStatusArray[i]+"'" ;
					if(i < (arrayCount-1))
					{
						strAllStatusCodes += ",";
					}
					else
					{
						strAllStatusCodes += ") ";
					}
				}
				strQuery += " and  a.latest_status_code in "+strAllStatusCodes; //Utility.ReplaceSingleQuote(strAllStatusCodes)+" ";
			}
			if(strBookNo!="")
			{
				strQuery += " and  a.booking_no like '%"+Utility.ReplaceSingleQuote(strBookNo)+"%' ";
			}
			if(strSender!="")
			{
				strQuery += " and  a.sender_name like N'%"+Utility.ReplaceSingleQuote(strSender)+"%' ";

			}
			if(strStatus!="")
			{
				//strQuery += " and (select count(status_code) from dispatch_tracking where applicationid = '"+strAppID+"' and enterpriseid = '"+strEnterpriseID+"' and  booking_no like '%"+Utility.ReplaceSingleQuote(strBookNo)+"%' and status_code like '%"+Utility.ReplaceSingleQuote(strStatus)+"%')>0 ";
				//strQuery += " and  latest_status_code like '%"+Utility.ReplaceSingleQuote(strStatus)+"%' ";
				strQuery += " and ((select top 1 status_code  from dispatch_tracking with(nolock) ";
				strQuery += " where applicationid = '"+strAppID+"' and enterpriseid = '"+strEnterpriseID+"'   and booking_no = a.booking_no ";
				strQuery += " and tracking_datetime = (select max(tracking_datetime) as  tracking_datetime  ";
				strQuery += " from dispatch_tracking with(nolock) ";
//				strQuery += " where applicationid = '"+strAppID+"' and enterpriseid = '"+strEnterpriseID+"'  and booking_no = a.booking_no)) = '"+strStatus+"') ";
				strQuery += " where applicationid = '"+strAppID+"' and enterpriseid = '"+strEnterpriseID+"'  and booking_no = a.booking_no  and  ( deleted  = 'N' or  deleted is  null ) )) = '"+strStatus+"') ";  //Jeab 5 Sep 2011
			}
			if(strException!="")
			{
				strQuery += " and  a.latest_exception_code like '%"+Utility.ReplaceSingleQuote(strException)+"%' ";
			}
			if(dr[4]!=System.DBNull.Value)
			{
				DateTime dateBookFromDate = (DateTime)dr[4];
				String strBookFromDate = Utility.DateFormat(strAppID, strEnterpriseID,dateBookFromDate,DTFormat.Date);
				strQuery += " and  a.booking_datetime >= "+strBookFromDate+" ";
			}
			if(dr[5]!=System.DBNull.Value)
			{
				DateTime dateBookToDate = (DateTime)dr[5];
				String strBookToDate = Utility.DateFormat(strAppID, strEnterpriseID,dateBookToDate,DTFormat.DateTime);
				strQuery += " and  a.booking_datetime <= "+strBookToDate+" ";
			}
			if(dr[6]!=System.DBNull.Value)
			{
				DateTime datePickFromDate = (DateTime)dr[6];
				String strPickFromDate = Utility.DateFormat(strAppID, strEnterpriseID,datePickFromDate,DTFormat.Date);
				strQuery += " and  a.req_pickup_datetime >= "+strPickFromDate+" ";
			}
			if(dr[7]!=System.DBNull.Value)
			{
				DateTime datePickToDate = (DateTime)dr[7];
				String strPickToDate = Utility.DateFormat(strAppID, strEnterpriseID,datePickToDate,DTFormat.DateTime);
				strQuery += " and  a.req_pickup_datetime <= "+strPickToDate+" ";
			}
			if(strDispatchType!="")
			{
				strQuery += " and  a.booking_type = '"+strDispatchType+"' ";
			}
			//14JAN09;GwanG
			if(strPickupRoute!="")
			{
				strQuery += " and  a.pickup_route = '"+strPickupRoute+"' ";
			}
			//Added by Tom 9/6/09
			if(strODC!="")
			{
				strQuery += " and  c.origin_station = '"+strODC+"' ";
			}
			if(dr["PayerID"] != System.DBNull.Value)
			{
				strQuery += " and a.payerid = '"+dr["PayerID"].ToString()+"' ";
			}
			//End added by Tom 9/6/09
			
			strQuery += "order by a.booking_no desc ";
			SessionDS sessionds = dbCon.ExecuteQuery(strQuery, recStart, recSize,"dispatch");
			return sessionds;
		}
		/// <summary>
		/// To query from Dispatch Tracking
		/// </summary>
		/// <param name="strAppID">ApplicationId</param>
		/// <param name="strEnterpriseID">enterpriseID</param>
		/// <param name="strBookNo">Booking Number</param>
		/// <param name="recStart">Start of Record</param>
		/// <param name="recSize">Size of Record</param>
		/// <returns></returns>
		public static SessionDS QueryDispatchForDeletion(String strAppID, String strEnterpriseID,String strBookNo, int recStart, int recSize)
		{
			
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon==null)
			{
				Logger.LogTraceError("DispatchTrackingMgrDAL","QueryDispatchForDeletion","SDM002","conApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			
			}
			String strQuery = "select d.booking_no, p.booking_datetime, p.sender_name, p.act_pickup_datetime,p.req_pickup_datetime, d.deleted, d.tracking_datetime, d.status_code, d.exception_code, d.person_incharge, d.remark, d.delete_remark, d.location  , d.last_userid ";  //Jeab 8 Sep 2011
			strQuery += "from Dispatch_Tracking d with(nolock), Pickup_Request p with(nolock) where d.applicationid = '"+strAppID+"' and d.enterpriseid = '"+strEnterpriseID+"' ";
			strQuery += " and  d.applicationid = p.applicationid and d.enterpriseid = p.enterpriseid and d.booking_no = p.booking_no ";
			strQuery += " and  d.booking_no = '"+Utility.ReplaceSingleQuote(strBookNo)+"' ";

			strQuery += " order by  d.tracking_datetime desc";
			SessionDS sessionds = dbCon.ExecuteQuery(strQuery, recStart, recSize,"dispatch");
			return sessionds;
		}

		/// <summary>
		/// To Update Dispatch tracking and Pickup reqeuest in Transaction
		/// </summary>
		/// <param name="dsDispatch">Dataset that stored data to be sotred to database</param>
		/// <param name="strAppID">ApplicationID</param>
		/// <param name="strEnterpriseID">enterprise ID</param>
		/// <returns></returns>
		public static int UpdateDispatch(DataSet dsDispatch, String strAppID, String strEnterpriseID)
		{
			int iRowsUpdated = 0;
			DbConnection dbConApp = null;
			IDbCommand dbCommandApp = null;
			IDbTransaction transactionApp = null;
			IDbConnection conApp = null;
			
			
			DataRow dr = dsDispatch.Tables[0].Rows[0];
			String strBookNo = (String)dr[0];
			
			String strStatus = (String)dr[1];
			String strException = (String)dr[2];
			DateTime dateStatusDateTime = (DateTime)dr[3];

			String strStatusDateTime = Utility.DateFormat(strAppID, strEnterpriseID, dateStatusDateTime,DTFormat.DateTime);
				
			String strPerson = (String)dr[4];
			String strRemarks = (String)dr[5];
			String strDeleted = "n";
			//Jeab 01 Sep 2011
			String strUser = (String)dr[6];
			String strUserLoc = (String)dr[7];
			//Jeab 01 Sep 2011  =========>  End
			String strActStatus = TIESUtility.getActualPickupStatusCode(strAppID, strEnterpriseID);
				
			
			//Get App dbConnection
			dbConApp = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			
			if(dbConApp == null)
			{
				Logger.LogTraceError("DispatchTrackingMgrDAL","UpdateDispatchDS","SDM002","dbConApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			DateTime dateLatestDate = GetLatestDateFromDispatch(dbConApp, strAppID, strEnterpriseID, strBookNo);

			//Begin Core and App Transaction
			conApp = dbConApp.GetConnection();
			if(conApp == null)
			{
				Logger.LogTraceError("DispatchTrackingMgrDAL","UpdateDispatchDS","SDM002","conApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			try
			{
				dbConApp.OpenConnection(ref conApp);
			}
			catch(ApplicationException appException)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("DispatchTrackingMgrDAL","UpdateDispatchDS","SDM003","Error opening connection : "+ appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ",appException);
			}
			catch(Exception exception)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("DispatchTrackingMgrDAL","UpdateDispatchDS","SDM003","Error opening connection : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}
			transactionApp = dbConApp.BeginTransaction(conApp,IsolationLevel.ReadCommitted);
			if(transactionApp == null)
			{
				Logger.LogTraceError("DispatchTrackingMgrDAL","UpdateDispatchDS","SDM002","transactionApp is null");
				throw new ApplicationException("Failed Begining a App Transaction..",null);
			}
			try
			{
				int iPUP = GetRowPUP(strAppID,strEnterpriseID,strBookNo);  //Jeab  5 Sep 2011
				if(iPUP == 0 )//Jeab  5 Sep 2011
				{	

					//INSERTING INTO DISPATCH_TRACKING
				
					StringBuilder strBuilder = new StringBuilder();
					strBuilder.Append("insert into Dispatch_Tracking with(rowlock) (applicationid, enterpriseid, booking_no, tracking_datetime, status_code, exception_code, person_incharge, remark, deleted ");
					strBuilder.Append(",last_userid ,last_updated ,location ) "); //Jeab 01 Sep 2011
					strBuilder.Append("values('"+strAppID+"', '"+strEnterpriseID+"', "+Utility.ReplaceSingleQuote(strBookNo)+", "+strStatusDateTime+", ");
					strBuilder.Append("'"+Utility.ReplaceSingleQuote(strStatus)+"', ");
					strBuilder.Append(" '"+Utility.ReplaceSingleQuote(strException)+"', N'"+Utility.ReplaceSingleQuote(strPerson)+"', N'"+Utility.ReplaceSingleQuote(strRemarks)+"', '"+strDeleted.ToUpper()+"' ");
					strBuilder.Append(" ,'" + strUser + "' ,'" + DateTime.Now + "'  ,'" + strUserLoc + "' )");  //Jeab 01 Sep 2011
					String strSQLQuery = strBuilder.ToString();
					dbCommandApp = dbConApp.CreateCommand(strSQLQuery,CommandType.Text); //Creating the command first time
					dbCommandApp.Connection = conApp;
					dbCommandApp.Transaction = transactionApp;

					iRowsUpdated = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);
					Logger.LogTraceInfo("DispatchTrackingMgrDAL","UpdateDispatchDS","INF001",iRowsUpdated + " rows updated from Exception_Code table");	
			
			
					if(DateTime.Compare(dateLatestDate,dateStatusDateTime) ==-1)
					{
						//---CHECK HERE FOR LATEST DATE if LATEST DATE IN UPDATE then exceute the query below
					
						//UPDATING INTO PICKUP_REQUEST
					
						strBuilder = new StringBuilder();
			
						strBuilder.Append("Update Pickup_Request with(rowlock) set latest_status_code = '"+strStatus+"', latest_exception_code = '"+strException+"',"); 
						strBuilder.Append(" latest_status_datetime = "+Utility.DateFormat(strAppID,strEnterpriseID,dateStatusDateTime,DTFormat.DateTime)+"");
						strBuilder.Append(" ,act_pickup_datetime =(SELECT MAX(tracking_datetime) FROM  Dispatch_Tracking with(rowlock)  WHERE   (applicationid = '"+strAppID+"')");
						strBuilder.Append(" AND (enterpriseid = '"+strEnterpriseID+"') AND (booking_no = "+Utility.ReplaceSingleQuote(strBookNo)+") AND");
						strBuilder.Append(" (status_code = '"+Utility.ReplaceSingleQuote(strActStatus)+"') AND (deleted NOT IN ('y', 'Y') OR deleted IS NULL)) where ");
						strBuilder.Append(" applicationid = '"+strAppID+"' and enterpriseid = '"+strEnterpriseID);
						strBuilder.Append("' and booking_no = "+Utility.ReplaceSingleQuote(strBookNo));
						strBuilder.Append("");

						strSQLQuery = strBuilder.ToString();
						dbCommandApp.CommandText = strSQLQuery;
						iRowsUpdated = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);
						Logger.LogTraceInfo("RBACManager","UpdateDispatchDS","INF001",iRowsUpdated + " rows updated from Dispatch_Tracking table");
					}
				
					//Commit application and core transaction.
					transactionApp.Commit();
					if(strActStatus==strStatus)
					{
						UpdateActualPickupDateTime(dbConApp,strAppID,strEnterpriseID,strBookNo,strStatusDateTime,strActStatus);
					}
				
					Logger.LogTraceInfo("DispatchTrackingMgrDAL","UpdateDispatchDS","SDM003","App db update transaction committed.");

				}
				
				Logger.LogTraceInfo("DispatchTrackingMgrDAL","UpdateDispatchDS","SDM003","App db update transaction committed.");
			}
			catch(ApplicationException appException)
			{
				try
				{
					transactionApp.Rollback();
					Logger.LogTraceInfo("DispatchTrackingMgrDAL","UpdateDispatchDS","SDM002","App db update transaction rolled back.");
				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("DispatchTrackingMgrDAL","UpdateDispatchDS","SDM002","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}
				Logger.LogTraceError("DispatchTrackingMgrDAL","UpdateDispatchDS","SDM002","Update Transaction Failed : "+ appException.Message.ToString());
				throw new ApplicationException("Error updating Dispatch Tracking "+appException.Message,appException);
			}
			catch(Exception exception)
			{
				try
				{
					transactionApp.Rollback();
					Logger.LogTraceInfo("DispatchTrackingMgrDAL","UpdateDispatchDS","SDM002","App db update transaction rolled back.");
				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("DispatchTrackingMgrDAL","UpdateDispatchDS","SDM002","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}

				Logger.LogTraceError("DispatchTrackingMgrDAL","UpdateDispatchDS","SDM004","update Transaction Failed : "+ exception.Message.ToString());
				throw new ApplicationException("Error updating Dispatch_Tracking "+exception.Message,exception);
			}
			finally
			{
				if(conApp != null)
				{
					conApp.Close();
				}
			}
			return iRowsUpdated;
			
		}
		/// <summary>
		/// Update acutal pickup
		/// time to pickup request
		/// </summary>
		/// <param name="dbCon">DataBase Connection</param>
		/// <param name="strAppID">ApplicationID</param>
		/// <param name="strEnterpriseID">EnterpriseID</param>
		/// <param name="strBookNo">Booking no</param>
		/// <param name="strActPickupDateTime">Actual PickupDatetime</param>
		private static void UpdateActualPickupDateTime(DbConnection dbCon,String strAppID, String strEnterpriseID, String strBookNo, String strActPickupDateTime,String strActStatusPickup)
		{
			String strQuery = "update Pickup_Request with(rowlock) set act_pickup_datetime = (SELECT MAX(tracking_datetime) FROM  Dispatch_Tracking with(rowlock) WHERE  (applicationid = '"+strAppID+"') AND (enterpriseid = '"+strEnterpriseID+"') AND (booking_no = "+Utility.ReplaceSingleQuote(strBookNo)+") AND (status_code = '"+Utility.ReplaceSingleQuote(strActStatusPickup)+"') AND (deleted NOT IN ('y', 'Y') OR deleted IS NULL)) where ";
			strQuery += "applicationid = '"+strAppID+"' and enterpriseid = '"+strEnterpriseID+"' and booking_no ="+Int32.Parse(Utility.ReplaceSingleQuote(strBookNo));
			IDbCommand dbCommandApp = dbCon.CreateCommand(strQuery,CommandType.Text);
		
			try
			{
				dbCon.ExecuteNonQuery(dbCommandApp);
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceInfo("DispatchTrackingMrgDAL","UpdateActualPickupDateTime","SDM002","unable to update actual pickup date time.");
				throw new ApplicationException("unable to update actual pickup date time.",null);
			}
		}
		
		/// <summary>
		/// Retrive all outstanding Dispatch
		/// </summary>
		/// <param name="strStatusArray">Array of string that is outstanding status code</param>
		/// <param name="strAppID">ApplicationID</param>
		/// <param name="strEnterpriseID">EnterprsieID</param>
		/// <param name="recStart">Start of record</param>
		/// <param name="recSize">Record Size</param>
		/// <returns></returns>
		public static SessionDS GetOutStandingDispatch(String[] strStatusArray, String strAppID, String strEnterpriseID,String fromPickupDate,String toPickupDate, int recStart, int recSize,String strCulture, string strPayerID)
		{
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			DataProviderType dbProvider = DbConnectionManager.GetInstance().GetDbProviderType(strAppID,strEnterpriseID);
			String strAllStatusCodes = "(";
			int arrayCount = strStatusArray.Length;
			for(int i=0; i<arrayCount; i++)
			{
				strAllStatusCodes +=  "'"+Utility.ReplaceSingleQuote(strStatusArray[i])+"'" ;
				if(i < (arrayCount-1))
				{
					strAllStatusCodes += ",";
				}
				else
				{
					strAllStatusCodes += ")";
				}
			}
			String strQuery=null;
			switch (dbProvider)
			{
				case DataProviderType.Sql :
					strQuery = "select pr.booking_no, pr.booking_datetime, pr.payerid ";
					strQuery += " ,b.code_text as payment_mode";
					strQuery += " ,(select top 1 status_code ";
					strQuery += " from dispatch_tracking with(nolock) ";
					strQuery += " where applicationid = '"+strAppID+"' and enterpriseid = '"+strEnterpriseID+"'  ";
					strQuery += " and booking_no = pr.booking_no and tracking_datetime = (select max(tracking_datetime) as  tracking_datetime ";
					strQuery += " from dispatch_tracking with(nolock) where applicationid = '"+strAppID+"' and enterpriseid = '"+strEnterpriseID+"' ";
					//Edit By Tom 29/6/09
					strQuery += " and booking_no = pr.booking_no and status_code in " + strAllStatusCodes + ")) as latest_status_code";
					//End Edit By Tom 29/6/09
					strQuery += " , pr.latest_exception_code ";
					strQuery += " , pr.booking_type ";
					strQuery += " , pr.sender_name, isnull(pr.sender_address1, '') +' '+ isnull(pr.sender_address2, '') as sender_address ";
					strQuery += " , pr.req_pickup_datetime, pr.act_pickup_datetime ";
					strQuery += " , pr.tot_pkg, pr.tot_wt, pr.cash_amount, pr.cash_collected, pr.remark ,pr.pickup_route ";
					//Jeab 04 Oct 2011
					strQuery += " ,(select top 1 last_userid  from dispatch_tracking with(nolock) ";
					strQuery += "  where applicationid = '"+strAppID+"' and enterpriseid = '"+strEnterpriseID+"'   ";
					strQuery += " and booking_no = pr.booking_no and tracking_datetime = (select max(tracking_datetime) as  tracking_datetime ";
					strQuery += " from dispatch_tracking with(nolock) where applicationid = '"+strAppID+"' and enterpriseid = '"+strEnterpriseID+"' ";
					strQuery += " and booking_no = pr.booking_no and status_code in " + strAllStatusCodes + "))   as last_userid ";
					//Jeab 04 Oct 2011  =========>  End
					break;
				case DataProviderType.Oracle :
					strQuery = "select pr.booking_no, pr.booking_datetime, pr.payerid, pr.payer_type, pr.latest_status_code, pr.latest_exception_code, pr.booking_type, pr.sender_name, pr.sender_address1 ||' '|| pr.sender_address2 as sender_address, pr.req_pickup_datetime, pr.act_pickup_datetime, pr.tot_pkg, pr.tot_wt, pr.cash_amount, pr.cash_collected, pr.remark, pr.pickup_route";
					break;
			}
			String pickupDate = "";
			if(fromPickupDate != "" || toPickupDate != "")
			{
				if(fromPickupDate != "" && toPickupDate == "")
					pickupDate = "and pr.req_pickup_datetime > CONVERT(DATETIME, '"+fromPickupDate+"', 103) ";
				else if(toPickupDate != "" && fromPickupDate == "")
					pickupDate = "and pr.req_pickup_datetime < CONVERT(DATETIME, '"+toPickupDate+"', 103)+1 ";
				else
				{
					pickupDate = "and req_pickup_datetime between CONVERT(DATETIME, '"+fromPickupDate+"', 103) ";
					pickupDate+= "and CONVERT(DATETIME, '"+toPickupDate+"', 103)+1 ";
				}
			}
			strQuery += "from Pickup_Request pr with(nolock) inner join core_system_code b with(nolock) on pr.payment_mode = b.code_str_value";
			strQuery +=	" where  b.culture = '"+strCulture+"' and b.codeid = 'payment_mode' and pr.applicationid = '"+strAppID+"' and pr.enterpriseid = '"+strEnterpriseID+"' and pr.latest_status_code in "+strAllStatusCodes+" ";
			strQuery += pickupDate;
			if(strPayerID != "")
			{
				strQuery += " and pr.payerid = '"+strPayerID+"' ";
			}
			strQuery += "order by pr.booking_no desc ";
			
			SessionDS sessionds = dbCon.ExecuteQuery(strQuery, recStart, recSize,"dispatch");
			return sessionds;
		}

		public static SessionDS GetOutStandingDispatch(String[] strStatusArray, String strAppID, String strEnterpriseID,String fromPickupDate,String toPickupDate, int recStart, int recSize,String strCulture)
		{
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			DataProviderType dbProvider = DbConnectionManager.GetInstance().GetDbProviderType(strAppID,strEnterpriseID);
			String strAllStatusCodes = "(";
			int arrayCount = strStatusArray.Length;
			for(int i=0; i<arrayCount; i++)
			{
				strAllStatusCodes +=  "'"+Utility.ReplaceSingleQuote(strStatusArray[i])+"'" ;
				if(i < (arrayCount-1))
				{
					strAllStatusCodes += ",";
				}
				else
				{
					strAllStatusCodes += ")";
				}
			}
			String strQuery=null;
			switch (dbProvider)
			{
				case DataProviderType.Sql :
					strQuery = "select pr.booking_no, pr.booking_datetime, pr.payerid ";
					strQuery += " ,b.code_text as payment_mode";
					strQuery += " ,(select top 1 status_code ";
					strQuery += " from dispatch_tracking with(nolock)  ";
					strQuery += " where applicationid = '"+strAppID+"' and enterpriseid = '"+strEnterpriseID+"'  ";
					strQuery += " and booking_no = pr.booking_no and tracking_datetime = (select max(tracking_datetime) as  tracking_datetime ";
					strQuery += " from dispatch_tracking with(nolock) where applicationid = '"+strAppID+"' and enterpriseid = '"+strEnterpriseID+"' ";
					//Edit By Tom 29/6/09
					strQuery += " and booking_no = pr.booking_no and status_code in " + strAllStatusCodes + ")) as latest_status_code";
					//End Edit By Tom 29/6/09
					strQuery += " , pr.latest_exception_code ";
					strQuery += " , pr.booking_type ";
					strQuery += " , pr.sender_name, isnull(pr.sender_address1, '') +' '+ isnull(pr.sender_address2, '') as sender_address ";
					strQuery += " , pr.req_pickup_datetime, pr.act_pickup_datetime ";
					strQuery += " , pr.tot_pkg, pr.tot_wt, pr.cash_amount, pr.cash_collected, pr.remark ,pr.pickup_route ";
					//Jeab 04 Oct 2011
					strQuery += " ,(select top 1 last_userid  from dispatch_tracking with(nolock)  ";
					strQuery += "  where applicationid = '"+strAppID+"' and enterpriseid = '"+strEnterpriseID+"'   ";
					strQuery += " and booking_no = pr.booking_no and tracking_datetime = (select max(tracking_datetime) as  tracking_datetime ";
					strQuery += " from dispatch_tracking with(nolock) where applicationid = '"+strAppID+"' and enterpriseid = '"+strEnterpriseID+"' ";
					strQuery += " and booking_no = pr.booking_no and status_code in " + strAllStatusCodes + "))   as last_userid ";
					//Jeab 04 Oct 2011  =========>  End
					break;
				case DataProviderType.Oracle :
					strQuery = "select pr.booking_no, pr.booking_datetime, pr.payerid, pr.payer_type, pr.latest_status_code, pr.latest_exception_code, pr.booking_type, pr.sender_name, pr.sender_address1 ||' '|| pr.sender_address2 as sender_address, pr.req_pickup_datetime, pr.act_pickup_datetime, pr.tot_pkg, pr.tot_wt, pr.cash_amount, pr.cash_collected, pr.remark, pr.pickup_route";
					break;
			}
			String pickupDate = "";
			if(fromPickupDate != "" || toPickupDate != "")
			{
				if(fromPickupDate != "" && toPickupDate == "")
					pickupDate = "and pr.req_pickup_datetime > CONVERT(DATETIME, '"+fromPickupDate+"', 103) ";
				else if(toPickupDate != "" && fromPickupDate == "")
					pickupDate = "and pr.req_pickup_datetime < CONVERT(DATETIME, '"+toPickupDate+"', 103)+1 ";
				else
				{
					pickupDate = "and req_pickup_datetime between CONVERT(DATETIME, '"+fromPickupDate+"', 103) ";
					pickupDate+= "and CONVERT(DATETIME, '"+toPickupDate+"', 103)+1 ";
				}
			}
			strQuery += "from Pickup_Request pr with(nolock) inner join core_system_code b with(nolock) on pr.payment_mode = b.code_str_value";
			strQuery +=	" where  b.culture = '"+strCulture+"' and b.codeid = 'payment_mode' and pr.applicationid = '"+strAppID+"' and pr.enterpriseid = '"+strEnterpriseID+"' and pr.latest_status_code in "+strAllStatusCodes+" ";
			strQuery += pickupDate;
			strQuery += "order by pr.booking_no desc ";
			
			SessionDS sessionds = dbCon.ExecuteQuery(strQuery, recStart, recSize,"dispatch");
			return sessionds;
		}
		/// <summary>
		/// To Update Dispatch tracking with Status and Exception Code
		/// </summary>
		/// <param name="strAppID">ApplicationID</param>
		/// <param name="strEnterpriseID">EnterprsieID</param>
		/// <param name="strBookNo">Booking No</param>
		/// <param name="strExceptionToRevert">Exception code</param>
		/// <param name="strStatusToRevert">Status Code</param>
		/// 

		public static void UpdatePickUpRoute(String strAppID, String strEnterpriseID, String strBookNo, String strNewPickupRoute)
		{
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("DispatchTrackingMgrDAL","UpdateRevertedDate","SDM002","dbConApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			
			StringBuilder strBuilder = new StringBuilder();
			strBuilder.Append("Update Pickup_Request with(rowlock) set pickup_route = '"+strNewPickupRoute+"' where ");
			strBuilder.Append(" applicationid = '");
			strBuilder.Append(strAppID);
			strBuilder.Append("' and enterpriseid = '");
			strBuilder.Append(strEnterpriseID);
			strBuilder.Append("' and booking_no = ");
			strBuilder.Append(Utility.ReplaceSingleQuote(strBookNo));

			String strSQLQuery = strBuilder.ToString();
			try
			{
				IDbCommand idbCom = dbCon.CreateCommand(strSQLQuery, CommandType.Text);
				dbCon.ExecuteNonQuery(idbCom);
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceInfo("DispatchTrackingMgrDAL","UpdateRevertedDate","SDM002","unable to update Reverted date.");
				throw new ApplicationException("Update reverted datetime fail",appException);
			}
		}


		public static void UpdateDeleteHistory(String strAppID, String strEnterpriseID, String strBookNo, String strExceptionToRevert, String strStatusToRevert, String strLatestTrackDate)
		{
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("DispatchTrackingMgrDAL","UpdateRevertedDate","SDM002","dbConApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			String strActStatusPickup = TIESUtility.getActualPickupStatusCode(strAppID, strEnterpriseID);
			
			StringBuilder strBuilder = new StringBuilder();
			DateTime dt=Convert.ToDateTime(strLatestTrackDate);			
			strBuilder.Append("Update Pickup_Request with(rowlock) set latest_exception_code = '"+strExceptionToRevert+"', latest_status_code = '"+Utility.ReplaceSingleQuote(strStatusToRevert)+"',latest_status_datetime ="+Utility.DateFormat(strAppID, strEnterpriseID, dt,DTFormat.DateTime)+", act_pickup_datetime = (SELECT MAX(tracking_datetime) FROM  Dispatch_Tracking  with(rowlock) WHERE  (applicationid = '"+strAppID+"') AND (enterpriseid = '"+strEnterpriseID+"') AND (booking_no = "+Utility.ReplaceSingleQuote(strBookNo)+") AND (status_code = '"+Utility.ReplaceSingleQuote(strActStatusPickup)+"') AND (deleted NOT IN ('y', 'Y') OR deleted IS NULL)) where ");
			strBuilder.Append(" applicationid = '");
			strBuilder.Append(strAppID);
			strBuilder.Append("' and enterpriseid = '");
			strBuilder.Append(strEnterpriseID);
			strBuilder.Append("' and booking_no = ");
			strBuilder.Append(Utility.ReplaceSingleQuote(strBookNo));
			//	strBuilder.Append("");

			String strSQLQuery = strBuilder.ToString();
			try
			{
				IDbCommand idbCom = dbCon.CreateCommand(strSQLQuery, CommandType.Text);
				dbCon.ExecuteNonQuery(idbCom);
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceInfo("DispatchTrackingMgrDAL","UpdateRevertedDate","SDM002","unable to update Reverted date.");
				throw new ApplicationException("Update reverted datetime fail",appException);
			}
		}
		/// <summary>
		/// To udpate dispatch tracking records upon deletion of record logically
		/// </summary>
		/// <param name="strAppID">ApplicationID</param>
		/// <param name="strEnterpriseID">enterpriseID</param>
		/// <param name="strBookNo">booking No</param>
		/// <param name="strTrackDate">Tracking Date</param>
		/// <param name="delete">Deleted or not</param>
		/// <param name="strDeleteRemark">Remarks for deleting</param>
		public static int UpdateDeleteHistory(String strAppID, String strEnterpriseID, String strBookNo, String strTrackDate, bool delete, String strDeleteRemark, String strStatusCheck)
		{
			int iRowsUpdated = 0;
			DbConnection dbConApp = null;
			IDbCommand dbCommandApp = null;
			IDbTransaction transactionApp = null;
			IDbConnection conApp = null;
			//Get App dbConnection
			dbConApp = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			
			if(dbConApp == null)
			{
				Logger.LogTraceError("DispatchTrackingMgrDAL","UpdateDeleteHistory","SDM002","dbConApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			//DateTime dateLatestDate = GetLatestDateFromDispatch(dbConApp, strAppID, strEnterpriseID, strBookNo);

			//Begin Core and App Transaction
			conApp = dbConApp.GetConnection();
			if(conApp == null)
			{
				Logger.LogTraceError("DispatchTrackingMgrDAL","UpdateDeleteHistory","SDM002","conApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			try
			{
				dbConApp.OpenConnection(ref conApp);
			}
			catch(ApplicationException appException)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("DispatchTrackingMgrDAL","UpdateDeleteHistory","SDM003","Error opening connection : "+ appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ",appException);
			}
			catch(Exception exception)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("DispatchTrackingMgrDAL","UpdateDeleteHistory","SDM003","Error opening connection : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}
			transactionApp = dbConApp.BeginTransaction(conApp,IsolationLevel.ReadCommitted);
			if(transactionApp == null)
			{
				Logger.LogTraceError("DispatchTrackingMgrDAL","UpdateDeleteHistory","SDM002","transactionApp is null");
				throw new ApplicationException("Failed Begining a App Transaction..",null);
			}
			try
			{
				DateTime dt = GetLatestDateFromDispatch(dbConApp, strAppID, strEnterpriseID, strBookNo);
				String strLatestDateTime = Utility.DateFormat(strAppID, strEnterpriseID, dt,DTFormat.DateTime);
				
				if(strTrackDate == strLatestDateTime)
				{
					RevertPRToEarlierStatus(dbConApp,strAppID,strEnterpriseID,strBookNo,strLatestDateTime);
				}
				StringBuilder strBuilder = new StringBuilder();
				String strDelete = null;
				if(delete)
				{
					strDelete = "y";
				}
				else if(!delete)
				{
					strDelete = "n";
				}
				strBuilder.Append("Update Dispatch_Tracking with(rowlock) set deleted = '"+strDelete.ToUpper()+"', delete_remark = N'"+strDeleteRemark +"' where ");
				strBuilder.Append(" applicationid = '");
				strBuilder.Append(strAppID);
				strBuilder.Append("' and enterpriseid = '");
				strBuilder.Append(strEnterpriseID);
				strBuilder.Append("' and booking_no = ");
				strBuilder.Append(Utility.ReplaceSingleQuote(strBookNo));
				strBuilder.Append(" and tracking_datetime = ");
				strBuilder.Append(strTrackDate);
				strBuilder.Append("");
				String strSQLQuery = strBuilder.ToString();

				//IDbCommand  dbCmd= dbCon.CreateCommand(strQry,CommandType.Text);
				//dbCmd.Connection = conApp;
				

				dbCommandApp = dbConApp.CreateCommand(strSQLQuery,CommandType.Text);
				dbCommandApp.Connection = conApp;
				dbCommandApp.Transaction = transactionApp;
				//dbCommandApp.CommandText = strSQLQuery;
				iRowsUpdated = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);
				Logger.LogTraceInfo("RBACManager","UpdateDeleteHistory","INF001",iRowsUpdated + " rows updated from Dispatch_Tracking table");
				
				
				
				//Commit application and core transaction.
				transactionApp.Commit();
				String strActStatus = TIESUtility.getActualPickupStatusCode(strAppID, strEnterpriseID);
				if(strStatusCheck==strActStatus)
				{
					UpdateActualPickupDateTime(dbConApp,strAppID,strEnterpriseID,strBookNo,"null",strActStatus);
				}
				Logger.LogTraceInfo("DispatchTrackingMgrDAL","UpdateDeleteHistory","SDM003","App db update transaction committed.");
			}
			catch(ApplicationException appException)
			{
				try
				{
					transactionApp.Rollback();
					Logger.LogTraceInfo("DispatchTrackingMgrDAL","UpdateDeleteHistory","SDM002","App db update transaction rolled back.");
				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("DispatchTrackingMgrDAL","UpdateDeleteHistory","SDM002","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}
				Logger.LogTraceError("DispatchTrackingMgrDAL","UpdateDeleteHistory","SDM002","Update Transaction Failed : "+ appException.Message.ToString());
				throw new ApplicationException("Error updating Dispatch Tracking "+appException.Message,appException);
			}
			catch(Exception exception)
			{
				try
				{
					transactionApp.Rollback();
					Logger.LogTraceInfo("DispatchTrackingMgrDAL","UpdateDeleteHistory","SDM002","App db update transaction rolled back.");
				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("DispatchTrackingMgrDAL","UpdateDeleteHistory","SDM002","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}

				Logger.LogTraceError("DispatchTrackingMgrDAL","UpdateDeleteHistory","SDM004","update Transaction Failed : "+ exception.Message.ToString());
				throw new ApplicationException("Error updating Dispatch_Tracking "+exception.Message,exception);
			}
			finally
			{
				if(conApp != null)
				{
					conApp.Close();
				}
			}


			
			return iRowsUpdated;

			//			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			//			if(dbCon == null)
			//			{
			//				Logger.LogTraceError("DispatchTrackingMgrDAL","DeleteHistory","SDM002","dbConApp is null");
			//				throw new ApplicationException("Connection to Database failed",null);
			//			}
			//			DateTime dt = GetLatestDateFromDispatch(dbCon, strAppID, strEnterpriseID, strBookNo);
			//			
			//			String strLatestDateTime = Utility.DateFormat(strAppID, strEnterpriseID, dt,DTFormat.DateTime);
			//			
			//
			//			if(strTrackDate == strLatestDateTime)
			//			{
			//				RevertPRToEarlierStatus(dbCon,strAppID,strEnterpriseID,strBookNo,strLatestDateTime);
			//			}
			//
			//
			//			StringBuilder strBuilder = new StringBuilder();
			//			String strDelete = null;
			//			if(delete)
			//			{
			//				strDelete = "y";
			//			}
			//			else if(!delete)
			//			{
			//				strDelete = "n";
			//			}
			//			strBuilder.Append("Update Dispatch_Tracking set deleted = '"+strDelete+"', delete_remark = '"+strDeleteRemark +"' where ");
			//			strBuilder.Append(" applicationid = '");
			//			strBuilder.Append(strAppID);
			//			strBuilder.Append("' and enterpriseid = '");
			//			strBuilder.Append(strEnterpriseID);
			//			strBuilder.Append("' and booking_no = ");
			//			strBuilder.Append(strBookNo);
			//			strBuilder.Append(" and tracking_datetime = ");
			//			strBuilder.Append(strTrackDate);
			//			
			//			strBuilder.Append("");
			//
			//			String strSQLQuery = strBuilder.ToString();
			//			try
			//			{
			//				IDbCommand idbCom = dbCon.CreateCommand(strSQLQuery, CommandType.Text);
			//				dbCon.ExecuteNonQuery(idbCom);
			//			}
			//			catch(ApplicationException appException)
			//			{
			//				Logger.LogTraceInfo("DispatchTrackingMgrDAL","UpdateDeleteHistory","SDM002","unable to update Reverted status and exception.");
			//				throw new ApplicationException("Update reverted status and exception fail",null);
			//			}
		}
		/// <summary>
		/// To revert to en earlier status code in the pickup Request
		/// </summary>
		/// <param name="dbCon">Database connetion</param>
		/// <param name="strAppID">ApplicationID</param>
		/// <param name="strEnterpriseID">EnterpriseID</param>
		/// <param name="strBookNo">Booking no</param>
		/// <param name="strLatestDateTime">Latest Date Time</param>
		private static void RevertPRToEarlierStatus(DbConnection dbCon, String strAppID, String strEnterpriseID, String strBookNo, String strLatestDateTime)
		{
			String strQuery = "select status_code, exception_code , tracking_datetime FROM Dispatch_Tracking with(nolock) where ";
			strQuery += " (tracking_datetime = (Select max(tracking_datetime) from Dispatch_Tracking with(nolock) ";
			strQuery += "where (enterpriseid = '"+strEnterpriseID+"') and (applicationid = '"+strAppID+"') and (booking_no = '"+Utility.ReplaceSingleQuote(strBookNo)+"') ";
			strQuery += " and  (tracking_datetime < "+strLatestDateTime+") and (deleted not in ('y','Y') or deleted is null)))";

			SessionDS sessionds;
			try
			{
				sessionds = dbCon.ExecuteQuery(strQuery,0,5,"revertEalierStatus");
				
				
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceInfo("DispatchTrackingMrgDAL","RevertPRToEarlierStatus","SDM002","unable to Reverted to earlier date.");				
				//throw new ApplicationException("unable to Reverted to earlier date.",null);
				throw appException;
			}
			
			DataRow dr = sessionds.ds.Tables[0].Rows[0];
			String strStatusCodeToRevert = Utility.ReplaceSingleQuote(dr["status_code"].ToString());
			String strExceptCodeToRevert = Utility.ReplaceSingleQuote(dr["exception_code"].ToString());
			String strLatestDate = dr["tracking_datetime"].ToString();
			UpdateDeleteHistory(strAppID,strEnterpriseID,strBookNo,strExceptCodeToRevert,strStatusCodeToRevert,strLatestDate);

		}
		/// <summary>
		/// Gets the latest date from dispatch tracking
		/// </summary>
		/// <param name="dbCon">Database Connection</param>
		/// <param name="strAppID">ApplicationID</param>
		/// <param name="strEnterpriseID">EnterprsieID</param>
		/// <param name="strBookNo">booking no</param>
		/// <returns></returns>
		private static DateTime GetLatestDateFromDispatch(DbConnection dbCon, String strAppID, String strEnterpriseID, String strBookNo)
		{	
			StringBuilder strBuilder = new StringBuilder();
			strBuilder.Append("select latest_status_datetime from Pickup_Request with(nolock) where applicationid = '"+strAppID+"' and enterpriseid = '"+strEnterpriseID+"' and booking_no = '"+Utility.ReplaceSingleQuote(strBookNo)+"' ");
			//strBuilder.Append(" and  deleted not in ('y','Y') ");
			String strSQLQuery = strBuilder.ToString();
			IDbCommand idbCom = dbCon.CreateCommand(strSQLQuery, CommandType.Text);
			SessionDS sessionds = dbCon.ExecuteQuery(idbCom, 0, 5,"dispatch");

			DataRow dr = sessionds.ds.Tables[0].Rows[0];
			DateTime dt = new DateTime(1,1,1);
			if(dr["latest_status_datetime"]!=System.DBNull.Value)
			{
				dt = (DateTime)dr["latest_status_datetime"];
				return dt;
			}
			
			return dt;
			
		}

		//Jeab 05 Sep 2011
		private static int GetRowPUP(String strAppID, String strEnterpriseID, String strBookNo)
		{
			DbConnection dbCon= null;
			dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if (dbCon==null)
			{
				Logger.LogTraceError("DispatchTrackingMgrDAL.cs","GetRowPUP","ERR001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			StringBuilder strBuilder = new StringBuilder();
			strBuilder.Append("select count(*)  as  cntRow  from dispatch_tracking with(nolock) where applicationid = '"+strAppID+"' and enterpriseid = '"+strEnterpriseID+"'" );
			strBuilder.Append(" and booking_no = '" + strBookNo +"'   and  status_code = 'PUP'  and  ( deleted  = 'N' or  deleted is  null )  ");

			String strSQLQuery = strBuilder.ToString();
			IDbCommand idbCom = dbCon.CreateCommand(strSQLQuery, CommandType.Text);
			SessionDS sessionds = dbCon.ExecuteQuery(idbCom, 0, 5,"dispatch");
			DataRow dr = sessionds.ds.Tables[0].Rows[0];
			int dt = 0 ;
			if(dr["cntRow"]!=System.DBNull.Value)
			{
				dt = (int)dr["cntRow"];
				return dt;
			}			
			return dt;			
		}
		//Jeab 05 Sep 2011  =========> End

		public static DateTime PickupBkgDateTime(String strAppID, String strEnterpriseID, int strBookNo)
		{	
			DbConnection dbCon= null;
			dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if (dbCon==null)
			{
				Logger.LogTraceError("DispatchTrackingMgrDAL.cs","GetLatestDateFromDispatch","ERR001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			StringBuilder strBuilder = new StringBuilder();
			strBuilder.Append("select booking_datetime from Pickup_Request with(nolock) where applicationid = '"+strAppID+"' and enterpriseid = '"+strEnterpriseID+"' and booking_no = "+strBookNo);
			//strBuilder.Append(" and  deleted not in ('y','Y') ");
			String strSQLQuery = strBuilder.ToString();
			IDbCommand idbCom = dbCon.CreateCommand(strSQLQuery, CommandType.Text);
			SessionDS sessionds = dbCon.ExecuteQuery(idbCom, 0, 5,"dispatch");
			DataRow dr = sessionds.ds.Tables[0].Rows[0];
			DateTime dt = new DateTime(1,1,1);
			if(dr["booking_datetime"]!=System.DBNull.Value)
			{
				dt = (DateTime)dr["booking_datetime"];
				return dt;
			}			
			return dt;			
		}
		public static DataSet GetLatestDateFromDispatch(String strAppID, String strEnterpriseID, int iBookNo)
		{	
			DbConnection dbCon= null;
			IDbCommand dbcmd=null;
			DataSet dsDt = null;

			dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if (dbCon==null)
			{
				Logger.LogTraceError("DispatchTrackingMgrDAL.cs","GetLatestDateFromDispatch","ERR001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}


			StringBuilder strBuilder = new StringBuilder();
			strBuilder.Append("select latest_status_datetime from Pickup_Request with(nolock) where applicationid = '"+strAppID+"' and enterpriseid = '"+strEnterpriseID+"' and booking_no ="+ iBookNo);
			
			String strSQLQuery = strBuilder.ToString();
			dbcmd = dbCon.CreateCommand(strSQLQuery,CommandType.Text);
 
			try
			{
				dsDt = (DataSet)dbCon.ExecuteQuery(dbcmd,com.common.DAL.ReturnType.DataSetType);  
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("DispatchTrackingMgrDAL.cs","GetLatestDateFromDispatch","ERR002","Error in Query String "+appException.Message);
				throw appException;
			}
			return dsDt;		
		}
		public static DataSet GetRouteCode(String strAppID,String strEnterpriseID,String strUserID,String strSendZip,String sRouteCode)
		{
			DbConnection dbCon= null;
			IDbCommand dbcmd=null;
			DataSet dsDt = null;

			dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if (dbCon==null)
			{
				Logger.LogTraceError("DispatchTrackingMgrDAL.cs","GetRouteCode","ERR001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			StringBuilder strQry = new StringBuilder();
			strQry.Append(string.Format("SELECT b.DC, a.route_code, a.route_description FROM GetRoutes('{0}', '{1}') a ",strEnterpriseID,strUserID));
			if (strSendZip==null || strSendZip == "")
			{
				strQry.Append(string.Format("INNER JOIN GetZipcodeInfo(1, '{0}', '{1}') b ",strEnterpriseID, null));
			}
			else
			{
				strQry.Append(string.Format("INNER JOIN GetZipcodeInfo(1, '{0}', '{1}') b ",strEnterpriseID, strSendZip));
			}
			
			strQry.Append(" ON b.DC = a.origin_DC ");
			if (sRouteCode != "" && sRouteCode != null )
			{
				strQry.Append(" WHERE a.route_code like '%");
				strQry.Append(sRouteCode.Trim());
				strQry.Append("%'");
			}
			strQry.Append(" ORDER BY a.route_code");
			String strSQLQuery = strQry.ToString();
			dbcmd = dbCon.CreateCommand(strSQLQuery,CommandType.Text);

			try
			{
				dsDt = (DataSet)dbCon.ExecuteQuery(dbcmd,com.common.DAL.ReturnType.DataSetType);  
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("DispatchTrackingMgrDAL.cs","GetRouteCode","ERR002","Error in Query String "+appException.Message);
				throw appException;
			}
			return dsDt;
		}




/*Comment by Sompote 2010-04-07
		//Phase2 - J02
		public static SessionDS QueryDispatchForShipmentTracking(String strAppID, String strEnterpriseID,long strBookNo, int recStart, int recSize)
		{
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon==null)
			{
				Logger.LogTraceError("DispatchTrackingMgrDAL","QueryDispatchForDeletion","SDM002","conApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			
			}
			String strQuery = "select * ";
			strQuery += " from Dispatch_Tracking ";
			strQuery += " where applicationid = '"+strAppID+"' ";
			strQuery += " and enterpriseid = '"+strEnterpriseID+"' ";
			strQuery += " and  booking_no = "+strBookNo+" ";
			strQuery += " order by  tracking_datetime desc";
			SessionDS sessionds = dbCon.ExecuteQuery(strQuery, recStart, recSize,"dispatch");
			return sessionds;
		}
		//Phase2 - J02
*/

		public static SessionDS QueryDispatchForShipmentTracking(String strAppID, String strEnterpriseID,long strBookNo, int recStart, int recSize)
		{
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon==null)
			{
				Logger.LogTraceError("DispatchTrackingMgrDAL","QueryDispatchForDeletion","SDM002","conApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			return QueryDispatchForShipmentTracking( strAppID,  strEnterpriseID, strBookNo,  recStart,  recSize ,dbCon);
		}
		public static SessionDS QueryDispatchForShipmentTracking(String strAppID, String strEnterpriseID,long strBookNo, int recStart, int recSize, DbConnection dbCon)
		{
			IDbConnection conApp = dbCon.GetConnection();
			if(conApp == null)
			{
				Logger.LogTraceError("DispatchTrackingMgrDAL","QueryDispatchForDeletion","ERR003","conApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			try
			{
				dbCon.OpenConnection(ref conApp);
			}
			catch(ApplicationException appException)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("DispatchTrackingMgrDAL","QueryDispatchForDeletion","ERR004","Error opening connection : "+ appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ",appException);
			}
			catch(Exception exception)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("DispatchTrackingMgrDAL","QueryDispatchForDeletion","ERR005","Error opening connection : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}
			
			IDbCommand dbCmd = null;		
			dbCmd = dbCon.CreateCommand("",CommandType.Text);
			dbCmd.Connection = conApp;	
			SessionDS sessionDS ;
			try
			{
				sessionDS = QueryDispatchForShipmentTracking( strAppID,  strEnterpriseID, strBookNo,  recStart,  recSize, dbCon, dbCmd);
			}
			catch(ApplicationException appException)
			{
				throw appException;
			}
			finally
			{
				if(conApp != null)
				{
					conApp.Close();
				}
			}
			return sessionDS;
		}
		/// <summary>
		/// Add by Sompote 2010-04-05
		/// </summary>
		/// <param name="strAppID"></param>
		/// <param name="strEnterpriseID"></param>
		/// <param name="strBookNo"></param>
		/// <param name="recStart"></param>
		/// <param name="recSize"></param>
		/// <returns></returns>
		public static SessionDS QueryDispatchForShipmentTracking(String strAppID, String strEnterpriseID,long strBookNo, int recStart, int recSize,DbConnection dbCon,IDbCommand dbCmd)
		{			
			String strQuery = "select * ";
			strQuery += " from Dispatch_Tracking with(nolock) ";
			strQuery += " where applicationid = '"+strAppID+"' ";
			strQuery += " and enterpriseid = '"+strEnterpriseID+"' ";
			strQuery += " and  booking_no = "+strBookNo+" ";
			strQuery += " order by  tracking_datetime desc";
			
			dbCmd.CommandText = strQuery;
			SessionDS sessionds = new SessionDS();
			try
			{
				//sessionds = dbCon.ExecuteQuery(dbCmd, recStart, recSize,"dispatch");
				sessionds.ds = (DataSet)dbCon.ExecuteQueryInTransaction(dbCmd,ReturnType.DataSetType);
			}
			catch(Exception exception)
			{
				Logger.LogTraceError("DispatchTrackingMgrDAL","QueryDispatchForShipmentTracking","ERR002","Error : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}
			return sessionds;
		}
	}
}
