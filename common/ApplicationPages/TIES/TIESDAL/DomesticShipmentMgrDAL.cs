using System;
using System.Data;
using com.common.classes;
using com.common.DAL;
using com.common.util;
using System.Text;
using com.ties.classes;

namespace com.ties.DAL
{
	/// <summary>
	/// Summary description for ShipmentManager.
	/// </summary>
	public class DomesticShipmentMgrDAL
	{
		//Add By Tom Mar 10,10
		public static string MDE_BY = string.Empty;
		public static string status = string.Empty;
		public static string route_code = string.Empty;
		public static string payer_name = string.Empty;
		public static string BD = string.Empty;
		public static string MD = string.Empty;
		public static string PD = string.Empty;
		public static string DD = string.Empty;
		public static string CA = string.Empty;
		public static string TRA = string.Empty;
		public static DateTime dtBookingDateTime = DateTime.Now;
		public static DateTime dtManifestDateTime = DateTime.Now;
		public static DateTime dtPickupDateTime = DateTime.Now;
		public static DateTime dtDeliveryDateTime = DateTime.Now;
		//End Add By Tom Mar 10, 10
		
		public string strAoo;
		public DomesticShipmentMgrDAL()
		{
		}
	

		/// <summary>
		/// This method gets a empty dataset for domestic shipment with an empty row. 
		/// </summary>
		/// <returns></returns>
		public static DataSet GetEmptyDomesticShipDS()
		{
			DataTable dtDomesticShipment = new DataTable();
			dtDomesticShipment.Columns.Add(new DataColumn("booking_no", typeof(int)));
			dtDomesticShipment.Columns.Add(new DataColumn("consignment_no", typeof(string)));
			dtDomesticShipment.Columns.Add(new DataColumn("ref_no", typeof(string)));
			dtDomesticShipment.Columns.Add(new DataColumn("booking_datetime", typeof(DateTime)));
			dtDomesticShipment.Columns.Add(new DataColumn("payerid", typeof(string)));
			dtDomesticShipment.Columns.Add(new DataColumn("payer_type", typeof(string)));
			dtDomesticShipment.Columns.Add(new DataColumn("new_account", typeof(string)));
			dtDomesticShipment.Columns.Add(new DataColumn("payer_name", typeof(string)));
			dtDomesticShipment.Columns.Add(new DataColumn("payer_address1", typeof(string)));
			dtDomesticShipment.Columns.Add(new DataColumn("payer_address2", typeof(string)));
			dtDomesticShipment.Columns.Add(new DataColumn("payer_zipcode", typeof(string)));
			dtDomesticShipment.Columns.Add(new DataColumn("payer_country", typeof(string)));
			dtDomesticShipment.Columns.Add(new DataColumn("payer_telephone", typeof(string)));
			dtDomesticShipment.Columns.Add(new DataColumn("payer_fax", typeof(string)));
			dtDomesticShipment.Columns.Add(new DataColumn("payment_mode", typeof(string)));
			dtDomesticShipment.Columns.Add(new DataColumn("sender_name", typeof(string)));
			dtDomesticShipment.Columns.Add(new DataColumn("sender_address1", typeof(string)));
			dtDomesticShipment.Columns.Add(new DataColumn("sender_address2", typeof(string)));
			dtDomesticShipment.Columns.Add(new DataColumn("sender_zipcode", typeof(string)));
			dtDomesticShipment.Columns.Add(new DataColumn("sender_country", typeof(string)));
			dtDomesticShipment.Columns.Add(new DataColumn("sender_telephone", typeof(string)));
			dtDomesticShipment.Columns.Add(new DataColumn("sender_fax", typeof(string)));
			dtDomesticShipment.Columns.Add(new DataColumn("sender_contact_person", typeof(string)));
			dtDomesticShipment.Columns.Add(new DataColumn("recipient_name", typeof(string)));
			dtDomesticShipment.Columns.Add(new DataColumn("recipient_address1", typeof(string)));
			dtDomesticShipment.Columns.Add(new DataColumn("recipient_address2", typeof(string)));
			dtDomesticShipment.Columns.Add(new DataColumn("recipient_zipcode", typeof(string)));
			dtDomesticShipment.Columns.Add(new DataColumn("recipient_country", typeof(string)));
			dtDomesticShipment.Columns.Add(new DataColumn("tot_wt", typeof(decimal)));
			dtDomesticShipment.Columns.Add(new DataColumn("tot_act_wt", typeof(decimal)));//TU on 17June08
			dtDomesticShipment.Columns.Add(new DataColumn("recipient_telephone", typeof(string)));
			dtDomesticShipment.Columns.Add(new DataColumn("tot_dim_wt", typeof(decimal)));
			dtDomesticShipment.Columns.Add(new DataColumn("recipient_fax", typeof(string)));
			dtDomesticShipment.Columns.Add(new DataColumn("tot_pkg", typeof(int)));
			dtDomesticShipment.Columns.Add(new DataColumn("service_code", typeof(string)));
			dtDomesticShipment.Columns.Add(new DataColumn("recipient_contact_person", typeof(string)));
			dtDomesticShipment.Columns.Add(new DataColumn("declare_value", typeof(decimal)));//money
			dtDomesticShipment.Columns.Add(new DataColumn("chargeable_wt", typeof(decimal)));
			dtDomesticShipment.Columns.Add(new DataColumn("insurance_surcharge", typeof(decimal)));//money
			dtDomesticShipment.Columns.Add(new DataColumn("max_insurance_cover", typeof(decimal)));//money
			dtDomesticShipment.Columns.Add(new DataColumn("act_pickup_datetime", typeof(string)));
			dtDomesticShipment.Columns.Add(new DataColumn("commodity_code", typeof(string)));
			dtDomesticShipment.Columns.Add(new DataColumn("est_delivery_datetime", typeof(DateTime)));
			dtDomesticShipment.Columns.Add(new DataColumn("percent_dv_additional", typeof(decimal)));
			dtDomesticShipment.Columns.Add(new DataColumn("act_delivery_date", typeof(DateTime)));
			dtDomesticShipment.Columns.Add(new DataColumn("payment_type", typeof(string)));
			dtDomesticShipment.Columns.Add(new DataColumn("return_pod_slip", typeof(string)));
			dtDomesticShipment.Columns.Add(new DataColumn("shipment_type", typeof(string)));
			dtDomesticShipment.Columns.Add(new DataColumn("origin_state_code", typeof(string)));
			dtDomesticShipment.Columns.Add(new DataColumn("invoice_no", typeof(string)));
			dtDomesticShipment.Columns.Add(new DataColumn("destination_state_code", typeof(string)));
			dtDomesticShipment.Columns.Add(new DataColumn("invoice_amt", typeof(decimal)));//money
			dtDomesticShipment.Columns.Add(new DataColumn("tot_vas_surcharge", typeof(decimal)));//money
			dtDomesticShipment.Columns.Add(new DataColumn("debit_amt", typeof(decimal)));//money
			dtDomesticShipment.Columns.Add(new DataColumn("credit_amt", typeof(decimal)));//money
			dtDomesticShipment.Columns.Add(new DataColumn("cash_collected", typeof(decimal)));//money
			dtDomesticShipment.Columns.Add(new DataColumn("last_status_code", typeof(string)));
			dtDomesticShipment.Columns.Add(new DataColumn("last_exception_code", typeof(string)));
			dtDomesticShipment.Columns.Add(new DataColumn("last_status_datetime", typeof(DateTime)));
			dtDomesticShipment.Columns.Add(new DataColumn("shpt_manifest_datetime", typeof(DateTime)));
			dtDomesticShipment.Columns.Add(new DataColumn("delivery_manifested", typeof(string)));
			dtDomesticShipment.Columns.Add(new DataColumn("route_code", typeof(string)));
			dtDomesticShipment.Columns.Add(new DataColumn("quotation_no", typeof(string)));
			dtDomesticShipment.Columns.Add(new DataColumn("quotation_version", typeof(int)));
			dtDomesticShipment.Columns.Add(new DataColumn("tot_freight_charge", typeof(decimal)));
			dtDomesticShipment.Columns.Add(new DataColumn("esa_surcharge", typeof(decimal)));
			dtDomesticShipment.Columns.Add(new DataColumn("remark", typeof(string)));
			dtDomesticShipment.Columns.Add(new DataColumn("cod_amount", typeof(decimal)));//money
			dtDomesticShipment.Columns.Add(new DataColumn("mbg", typeof(string)));
			dtDomesticShipment.Columns.Add(new DataColumn("origin_station", typeof(string)));
			dtDomesticShipment.Columns.Add(new DataColumn("destination_station", typeof(string)));

			//HC Return Task
			dtDomesticShipment.Columns.Add(new DataColumn("return_invoice_hc", typeof(string)));
			dtDomesticShipment.Columns.Add(new DataColumn("est_hc_return_datetime", typeof(DateTime)));
			dtDomesticShipment.Columns.Add(new DataColumn("act_hc_return_datetime", typeof(DateTime)));
			//HC Return Task

			//By Aoo 22/02/2008
			dtDomesticShipment.Columns.Add(new DataColumn("other_surch_amount", typeof(decimal)));
			//End By Aoo 

			//By Tumz 28/03/2011
			dtDomesticShipment.Columns.Add(new DataColumn("pkg_detail_replaced_by", typeof(string)));
			dtDomesticShipment.Columns.Add(new DataColumn("pkg_detail_replaced_datetime", typeof(DateTime)));

			//opal 7/11/2012
			dtDomesticShipment.Columns.Add(new DataColumn("created_by", typeof(string)));
			dtDomesticShipment.Columns.Add(new DataColumn("BookingDateTimePlus6", typeof(DateTime)));

			DataRow drEach = dtDomesticShipment.NewRow();
			
			drEach["booking_no"] = 0;
			drEach["tot_wt"] = 0;
			drEach["tot_act_wt"] = 0;//TU on 17June08
			drEach["tot_dim_wt"] = 0;
			drEach["tot_pkg"] = 0;
			drEach["declare_value"] = 0;
			drEach["chargeable_wt"] = 0;
			drEach["insurance_surcharge"] = 0;
			drEach["max_insurance_cover"] = 0;
			drEach["tot_vas_surcharge"] = 0;
			drEach["cod_amount"] = 0;


			dtDomesticShipment.Rows.Add(drEach);
			DataSet dsDomesticShipment = new DataSet();
			dsDomesticShipment.Tables.Add(dtDomesticShipment);
			return  dsDomesticShipment;	
		}

		/// <summary>
		/// This method inserts record into the shipment, shipment_pkg, shipment_vas, shipment_tracking tables and updates pickup_shipment.
		/// </summary>
		/// <param name="strAppID">Application ID</param>
		/// <param name="strEnterpriseID">Enterprise ID</param>
		/// <param name="dsDomesticShipment">Dataset for inserting into shipment</param>
		/// <param name="dsVAS">Dataset for inserting into shipment_vas</param>
		/// <param name="dsPkg">Dataset for inserting into shipment_pkg</param>
		/// <param name="userID">Logged in User ID</param>
		/// <param name="iSerialNo">Serial Number</param>
		/// <param name="RecipZipCode">Recipient Zip Code</param>
		/// <returns>Returns the booking number</returns>
		/// 
		#region Comment by Sompote 2010-05-10 (Fix Database Lock)
		/*
		public static int getTranSitDayForManifest(String strAppID, String strEnterpriseID, string serviceCode)
		{
			int numberOfTranDay = 0;
			SessionDS sessionDS = null;

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("DistributionCenter","GetHubStationOfAutoManifest","EDB101","DbConnection object is null!!");
				System.Diagnostics.Trace.WriteLineIf(Logger.AppSwitch.TraceError,"[DistributionCenter::GetHubStationOfAutoManifest()] DbConnection is null","ERROR");
				return numberOfTranDay;
			}

			StringBuilder str = new StringBuilder();
			str.Append("select transit_day ");
			str.Append(" from service  ");
			str.Append(" where applicationid = '");
			str.Append(strAppID);
			str.Append("' and enterpriseid = '");
			str.Append(strEnterpriseID);
			str.Append("' and service_code = '");
			str.Append(serviceCode + "'");

			sessionDS = dbCon.ExecuteQuery(str.ToString(),0, 0,"HubStationOfAutoManifest");

			if((sessionDS.ds.Tables[0].Rows[0]["transit_day"] != null) && 
				(!sessionDS.ds.Tables[0].Rows[0]["transit_day"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
				(sessionDS.ds.Tables[0].Rows[0]["transit_day"].ToString() != "")) 

			{
				numberOfTranDay = (int)sessionDS.ds.Tables[0].Rows[0]["transit_day"];
			}
				

			return  numberOfTranDay;
		}

		*/
		#endregion
		public static int getTranSitDayForManifest(String strAppID, String strEnterpriseID, string serviceCode)
		{
			int numberOfTranDay = 0;			

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("DomesticShipmentMgrDAL","getTranSitDayForManifest","EDB101","DbConnection object is null!!");
				System.Diagnostics.Trace.WriteLineIf(Logger.AppSwitch.TraceError,"[DomesticShipmentMgrDAL::getTranSitDayForManifest()] DbConnection is null","ERROR");
				return numberOfTranDay;
			}	

			return  getTranSitDayForManifest( strAppID,  strEnterpriseID,  serviceCode, dbCon);
		}
		public static int getTranSitDayForManifest(String strAppID, String strEnterpriseID, string serviceCode,DbConnection dbCon)
		{
			IDbConnection conApp = dbCon.GetConnection();
			if(conApp == null)
			{
				Logger.LogTraceError("DomesticShipmentMgrDAL","getTranSitDayForManifest","ERR003","conApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			try
			{
				dbCon.OpenConnection(ref conApp);
			}
			catch(ApplicationException appException)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("DomesticShipmentMgrDAL","getTranSitDayForManifest","ERR004","Error opening connection : "+ appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ",appException);
			}
			catch(Exception exception)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("DomesticShipmentMgrDAL","getTranSitDayForManifest","ERR005","Error opening connection : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}
			
			IDbCommand dbCmd = null;		
			dbCmd = dbCon.CreateCommand("",CommandType.Text);
			dbCmd.Connection = conApp;	

			return  getTranSitDayForManifest( strAppID,  strEnterpriseID,  serviceCode, dbCon, dbCmd);
		}

		public static int getTranSitDayForManifest(String strAppID, String strEnterpriseID, string serviceCode,DbConnection dbCon,IDbCommand dbCmd)
		{
			int numberOfTranDay = 0;
			SessionDS sessionDS = new SessionDS();

			StringBuilder str = new StringBuilder();
			str.Append("select transit_day ");
			str.Append(" from service  ");
			str.Append(" where applicationid = '");
			str.Append(strAppID);
			str.Append("' and enterpriseid = '");
			str.Append(strEnterpriseID);
			str.Append("' and service_code = '");
			str.Append(serviceCode + "'");

			//sessionDS = dbCon.ExecuteQuery(str.ToString(),0, 0,"HubStationOfAutoManifest");
			dbCmd.CommandText = str.ToString();
			sessionDS.ds = (DataSet)dbCon.ExecuteQueryInTransaction(dbCmd,ReturnType.DataSetType);

			if((sessionDS.ds.Tables[0].Rows[0]["transit_day"] != null) && 
				(!sessionDS.ds.Tables[0].Rows[0]["transit_day"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
				(sessionDS.ds.Tables[0].Rows[0]["transit_day"].ToString() != "")) 

			{
				numberOfTranDay = (int)sessionDS.ds.Tables[0].Rows[0]["transit_day"];
			}
				

			return  numberOfTranDay;
		}

		
		public static void setAutomaticManifest(String strAppID, String strEnterpriseID, 
			DataSet dsDomesticShipment, String pathCode, long bookingNo,
			String orig_station, String dest_station, String route_code, String service_code,int ClickNoHoliday)
		{
			IDbCommand dbCmd = null;
			StringBuilder strBuild = null;
			int iRowsAffected = 0;

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("module1","DomesticShipmentManager","setAutomaticManifest","ERR001","DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null",null);
			}
			if (dsDomesticShipment == null)
			{
				Logger.LogTraceError("module1","DomesticShipmentManager","setAutomaticManifest","ERR002","DataSet is null!!");
				throw new ApplicationException("The Shipment DataSet is null",null);
			}
			

			IDbConnection conApp = dbCon.GetConnection();
			
			if(conApp == null)
			{
				Logger.LogTraceError("module1","DomesticShipmentMgrDAL","setAutomaticManifest","ERR003","conApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			try
			{
				dbCon.OpenConnection(ref conApp);
			}
			catch(ApplicationException appException)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("module1","DomesticShipmentMgrDAL","setAutomaticManifest","ERR004","Error opening connection : "+ appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ",appException);
			}
			catch(Exception exception)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("module1","DomesticShipmentMgrDAL","setAutomaticManifest","ERR005","Error opening connection : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}


			IDbTransaction transactionApp = dbCon.BeginTransaction(conApp,IsolationLevel.ReadCommitted);

			if(transactionApp == null)
			{
				Logger.LogTraceError("module1","DomesticShipmentMgrDAL","setAutomaticManifest","ERR006","transactionApp is null");
				throw new ApplicationException("Failed Begining a App Transaction..",null);
			}

			try
			{
				dbCmd = dbCon.CreateCommand("", CommandType.Text);
				dbCmd.Connection = conApp;
				dbCmd.Transaction = transactionApp;

				DateTime delivery_date = new DateTime(1,1,1);
				DateTime delivery_manifest_datetime = new DateTime(1,1,1);
				DateTime pickup_date = new DateTime(1,1,1);
				
				String flight_vehicle_no = "";
				String strDelManifestDateTime = "";
				String local_path_code = "";
				String local_orig_station = "";
				String local_dest_station = "";
				String local_route_code = "";
				String linehaul_route = "";

				String consignment_no = "";
				String StateCode = "";
				long booking_no = 0;
				
				com.ties.classes.DeliveryPath delpathAM = null;

				DataRow drEach = dsDomesticShipment.Tables[0].Rows[0];
				local_path_code = pathCode;
				local_orig_station = orig_station;
				local_dest_station = dest_station;
				local_route_code = route_code;
				consignment_no = dsDomesticShipment.Tables[0].Rows[0]["consignment_no"].ToString();
				StateCode = dsDomesticShipment.Tables[0].Rows[0]["destination_state_code"].ToString();
				booking_no = bookingNo;

				delivery_date = (DateTime)drEach["est_delivery_datetime"];
				delpathAM = new com.ties.classes.DeliveryPath();
				delpathAM.Populate(strAppID, strEnterpriseID, local_path_code,dbCon,dbCmd);
				flight_vehicle_no = delpathAM.FlightVehicleNo;

				//###### This code added for new manifesting request ######
				DateTime departure_date = new DateTime(1,1,1);

				String currentHub = "";
				currentHub = DistributionCenterDAL.GetHubStationOfAutoManifest(strAppID, strEnterpriseID,dbCon,dbCmd);

				int tranSitDay = 0;
				tranSitDay = getTranSitDayForManifest(strAppID, strEnterpriseID, service_code,dbCon,dbCmd);
 
				int dayIncrement = 0;
				if (tranSitDay != 0)
				{
					//					if (local_orig_station == currentHub || local_dest_station == currentHub || local_orig_station==local_dest_station) // This mean only 1 linehaul
					//					{
					//						if (tranSitDay == 1)
					//							dayIncrement = 1;
					//						else if (tranSitDay == 2)
					//							dayIncrement = -1;
					//						else if (tranSitDay == 3)
					//							dayIncrement = -2;
					departure_date = DateTime.Parse(drEach["act_pickup_datetime"].ToString());//delivery_date;
					bool isHoliday=false;
					isHoliday = TIESUtility.IsDayHoliday(strAppID,strEnterpriseID,departure_date.AddDays(1),dbCon,dbCmd);

					if(ClickNoHoliday>0&&isHoliday)
						dayIncrement = ClickNoHoliday+1;
					else
						dayIncrement = 1;
					departure_date = departure_date.AddDays(dayIncrement); 
					//					}
					//					else // This mean only 2 linehaul *departure date must be today
					//					{
					//						if (tranSitDay == 1)
					//							dayIncrement = -1;
					//						else if (tranSitDay == 2)
					//							dayIncrement = -2;
					//						else if (tranSitDay == 3)
					//							dayIncrement = -3;

					//						departure_date = DateTime.Parse(drEach["act_pickup_datetime"].ToString());//delivery_date;
					//						departure_date = departure_date.AddDays(dayIncrement); 
					//
					//						if(departure_date.DayOfWeek  == System.DayOfWeek.Saturday)
					//						{
					//							departure_date = departure_date.AddDays(-1);
					//						}
					//							
					//						if(departure_date.DayOfWeek == System.DayOfWeek.Sunday) 
					//						{
					//							departure_date = departure_date.AddDays(-1);
					//						}
					//					}
				}

				//###### This code added for new manifesting request ######

				delivery_manifest_datetime = (DateTime)DeliveryManifestMgrDAL.GetExactDateTimeOfAutoManifest(strAppID, strEnterpriseID,
					local_path_code, flight_vehicle_no, departure_date,dbCon,dbCmd); // The old one is --> delivery_date);

				strBuild = new StringBuilder();
				strBuild.Append("select count(*) from Core_Enterprise");
				//dbCmd = dbCon.CreateCommand(strBuild.ToString(), CommandType.Text);
				//dbCmd.Connection = conApp;
				//dbCmd.Transaction = transactionApp;
				dbCmd.CommandText = strBuild.ToString();
				iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);

				if (delivery_manifest_datetime != new DateTime(1,1,1))
				{
					strDelManifestDateTime = Utility.DateFormat(strAppID,strEnterpriseID,delivery_manifest_datetime,DTFormat.DateLongTime);
					//Short Route
					strBuild = new StringBuilder();	
					strBuild.Append("INSERT INTO Delivery_Manifest_Detail(applicationid, enterpriseid, path_code, flight_vehicle_no, ");
					strBuild.Append("delivery_manifest_datetime, consignment_no, origin_state_code, destination_state_code, route_code) values('");
					strBuild.Append(strAppID+"','"+strEnterpriseID+"','"+local_path_code+"','"+flight_vehicle_no+"',"+strDelManifestDateTime+",'"+consignment_no+"'");

					strBuild.Append(",");
					if(local_orig_station.Trim() != "")
					{
						strBuild.Append("'");
						strBuild.Append(Utility.ReplaceSingleQuote(local_orig_station));
						strBuild.Append("'");
					}
					else
					{
						strBuild.Append("null");
					}
					strBuild.Append(",");

					if(local_dest_station.Trim() != "")
					{
						strBuild.Append("'");
						strBuild.Append(Utility.ReplaceSingleQuote(local_dest_station));
						strBuild.Append("'");
					}
					else
					{
						strBuild.Append("null");
					}
					strBuild.Append(",");

					if(local_route_code.Trim() != "")
					{
						strBuild.Append("'");
						strBuild.Append(Utility.ReplaceSingleQuote(local_route_code));
						strBuild.Append("'");
					}
					else
					{
						strBuild.Append("null");
					}
					strBuild.Append(")");
				
					dbCmd.CommandText = strBuild.ToString();
					iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
					Logger.LogDebugInfo("module1","DomesticShipmentMgrDAL","setAutomaticManifest","INF004",iRowsAffected + " rows inserted into shipment_pkg table");
				
					strBuild = new StringBuilder();
					strBuild.Append("update shipment ");
					strBuild.Append("set shpt_manifest_datetime = " + Utility.DateFormat(strAppID, strEnterpriseID,System.DateTime.Now,DTFormat.DateTime) + ", ");
					if(DistributionCenterDAL.IsDChasSWB(strAppID,strEnterpriseID,local_orig_station,dbCon,dbCmd))
						strBuild.Append("delivery_manifested = 'N'");	
					else
						strBuild.Append("delivery_manifested = 'Y'");
					strBuild.Append(" where booking_no = " + booking_no);
					strBuild.Append(" and consignment_no = '" + consignment_no + "'");

					dbCmd.CommandText = strBuild.ToString();
					iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
					Logger.LogDebugInfo("module1","DomesticShipmentMgrDAL","setAutomaticManifest","INF004",iRowsAffected + " rows inserted into shipment_pkg table");
										
					if(local_orig_station != local_dest_station)
					{
						linehaul_route = DeliveryManifestMgrDAL.GetLineHaulOfAutoManifest(strAppID, strEnterpriseID,
							local_orig_station, local_dest_station,dbCon,dbCmd);

						if(DistributionCenterDAL.IsDChasSWB(strAppID,strEnterpriseID,local_orig_station,dbCon,dbCmd))
							linehaul_route=""; 

						if(linehaul_route != "")
						{
							delivery_date = (DateTime)drEach["est_delivery_datetime"];
							
							delpathAM = null;
							delpathAM = new com.ties.classes.DeliveryPath();
							delpathAM.Populate(strAppID, strEnterpriseID, linehaul_route,dbCon,dbCmd);

							flight_vehicle_no = delpathAM.FlightVehicleNo;

							
							if (pickup_date == new DateTime(1,1,1))
							{
								pickup_date = Convert.ToDateTime(drEach["act_pickup_datetime"]);
							}
							else
							{
								pickup_date = (DateTime)pickup_date;
							}
			

							delivery_manifest_datetime = DeliveryManifestMgrDAL.GetExactDateTimeOfAutoManifest(strAppID, strEnterpriseID, 
								linehaul_route, flight_vehicle_no, pickup_date,dbCon,dbCmd);

							if (delivery_manifest_datetime != new DateTime(1,1,1))
							{
								strDelManifestDateTime =Utility.DateFormat(strAppID,strEnterpriseID,
									delivery_manifest_datetime, DTFormat.DateLongTime);

								//LH1
								strBuild = new StringBuilder();	
								strBuild.Append("INSERT INTO Delivery_Manifest_Detail(applicationid, enterpriseid, path_code, flight_vehicle_no, ");
								strBuild.Append("delivery_manifest_datetime, consignment_no, origin_state_code, destination_state_code, route_code) values('");
								strBuild.Append(strAppID+"','"+strEnterpriseID+"','"+linehaul_route+"','"+flight_vehicle_no+"',"+strDelManifestDateTime+",'"+consignment_no+"'");

								strBuild.Append(",");
								if(local_orig_station.Trim() != "")
								{
									strBuild.Append("'");
									strBuild.Append(Utility.ReplaceSingleQuote(local_orig_station));
									strBuild.Append("'");
								}
								else
								{
									strBuild.Append("null");
								}
								strBuild.Append(",");

								if(local_dest_station.Trim() != "")
								{
									strBuild.Append("'");
									strBuild.Append(Utility.ReplaceSingleQuote(local_dest_station));
									strBuild.Append("'");
								}
								else
								{
									strBuild.Append("null");
								}
								strBuild.Append(",");

								if(local_route_code.Trim() != "")
								{
									strBuild.Append("'");
									strBuild.Append(Utility.ReplaceSingleQuote(local_route_code));
									strBuild.Append("'");
								}
								else
								{
									strBuild.Append("null");
								}
								strBuild.Append(")");
				
								dbCmd.CommandText = strBuild.ToString();
								iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
								Logger.LogDebugInfo("module1","DomesticShipmentMgrDAL","setAutomaticManifest","INF004",iRowsAffected + " rows inserted into shipment_pkg table");
							}
						}
						else //2 LineHauls
						{

							if(currentHub != "")

							{
								linehaul_route = DeliveryManifestMgrDAL.GetLineHaulOfAutoManifest(strAppID, strEnterpriseID,
									local_orig_station, currentHub,dbCon,dbCmd);

								if(DistributionCenterDAL.IsDChasSWB(strAppID,strEnterpriseID,local_orig_station,dbCon,dbCmd))
									linehaul_route=""; 

								if(linehaul_route != "")
								{
									delivery_date = (DateTime)drEach["est_delivery_datetime"];

									delpathAM = null;
									delpathAM = new com.ties.classes.DeliveryPath();
									delpathAM.Populate(strAppID, strEnterpriseID, linehaul_route,dbCon,dbCmd);
									flight_vehicle_no = delpathAM.FlightVehicleNo;

							
									if (pickup_date == new DateTime(1,1,1))
									{
										pickup_date = Convert.ToDateTime(drEach["act_pickup_datetime"]);
									}
									else
									{
										pickup_date = (DateTime)pickup_date;
									}

									delivery_manifest_datetime = DeliveryManifestMgrDAL.GetExactDateTimeOfAutoManifest(strAppID, strEnterpriseID, 
										linehaul_route, flight_vehicle_no, pickup_date,dbCon,dbCmd);

									if (delivery_manifest_datetime != new DateTime(1,1,1))
									{
										strDelManifestDateTime =Utility.DateFormat(strAppID,strEnterpriseID,
											delivery_manifest_datetime, DTFormat.DateLongTime);

										//LineHaul 1 from Origin to Hub
										strBuild = new StringBuilder();	
										strBuild.Append("INSERT INTO Delivery_Manifest_Detail(applicationid, enterpriseid, path_code, flight_vehicle_no, ");
										strBuild.Append("delivery_manifest_datetime, consignment_no, origin_state_code, destination_state_code, route_code) values('");
										strBuild.Append(strAppID+"','"+strEnterpriseID+"','"+linehaul_route+"','"+flight_vehicle_no+"',"+strDelManifestDateTime+",'"+consignment_no+"'");

										strBuild.Append(",");
										if(local_orig_station.Trim() != "")
										{
											strBuild.Append("'");
											strBuild.Append(Utility.ReplaceSingleQuote(local_orig_station));
											strBuild.Append("'");
										}
										else
										{
											strBuild.Append("null");
										}
										strBuild.Append(",");

										if(local_dest_station.Trim() != "")
										{//Edited by X Sep 22,09 - Change to currentHub
											strBuild.Append("'");
											strBuild.Append(Utility.ReplaceSingleQuote(currentHub));
											strBuild.Append("'");
										}
										else
										{
											strBuild.Append("null");
										}
										strBuild.Append(",");

										if(local_route_code.Trim() != "")
										{
											strBuild.Append("'");
											strBuild.Append(Utility.ReplaceSingleQuote(local_route_code));
											strBuild.Append("'");
										}
										else
										{
											strBuild.Append("null");
										}
										strBuild.Append(")");
				
										dbCmd.CommandText = strBuild.ToString();
										iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
										Logger.LogDebugInfo("module1","DomesticShipmentMgrDAL","setAutomaticManifest","INF004",iRowsAffected + " rows inserted into shipment_pkg table");
									}

									pickup_date = pickupDateTimeIncrement(pickup_date, strAppID, strEnterpriseID, StateCode);

									linehaul_route = DeliveryManifestMgrDAL.GetLineHaulOfAutoManifest(strAppID, strEnterpriseID,
										currentHub, local_dest_station);

									if(DistributionCenterDAL.IsDChasSWB(strAppID,strEnterpriseID,currentHub,dbCon,dbCmd))
										linehaul_route=""; 
									
									//Second Route
									if(linehaul_route != "")
									{
										delivery_date = (DateTime)drEach["est_delivery_datetime"];

										delpathAM = null;
										delpathAM = new com.ties.classes.DeliveryPath();
										delpathAM.Populate(strAppID, strEnterpriseID, linehaul_route,dbCon,dbCmd);
										flight_vehicle_no = delpathAM.FlightVehicleNo;

							
										if (pickup_date == new DateTime(1,1,1))
										{
											pickup_date = Convert.ToDateTime(drEach["act_pickup_datetime"]);
											//pickup_date = System.DateTime.ParseExact((String)drEach["act_pickup_datetime"],"dd/MM/yyyy HH:mm", null);
										}
										else
										{
											pickup_date = (DateTime)pickup_date;
										}
			

										delivery_manifest_datetime = DeliveryManifestMgrDAL.GetExactDateTimeOfAutoManifest(strAppID, strEnterpriseID, 
											linehaul_route, flight_vehicle_no, pickup_date,dbCon,dbCmd);

										if (delivery_manifest_datetime != new DateTime(1,1,1))
										{
											strDelManifestDateTime =Utility.DateFormat(strAppID,strEnterpriseID,
												delivery_manifest_datetime, DTFormat.DateLongTime);

											//11. Insert a new Delivery_Manifest_Detail record where the fields are assigned as follows:
											strBuild = new StringBuilder();	
											strBuild.Append("INSERT INTO Delivery_Manifest_Detail(applicationid, enterpriseid, path_code, flight_vehicle_no, ");
											strBuild.Append("delivery_manifest_datetime, consignment_no, origin_state_code, destination_state_code, route_code) values('");
											strBuild.Append(strAppID+"','"+strEnterpriseID+"','"+linehaul_route+"','"+flight_vehicle_no+"',"+strDelManifestDateTime+",'"+consignment_no+"'");

											strBuild.Append(",");
											if(local_orig_station.Trim() != "")
											{
												strBuild.Append("'");
												strBuild.Append(Utility.ReplaceSingleQuote(local_orig_station));
												strBuild.Append("'");
											}
											else
											{
												strBuild.Append("null");
											}
											strBuild.Append(",");

											if(local_dest_station.Trim() != "")
											{
												strBuild.Append("'");
												strBuild.Append(Utility.ReplaceSingleQuote(local_dest_station));
												strBuild.Append("'");
											}
											else
											{
												strBuild.Append("null");
											}
											strBuild.Append(",");

											if(local_route_code.Trim() != "")
											{
												strBuild.Append("'");
												strBuild.Append(Utility.ReplaceSingleQuote(local_route_code));
												strBuild.Append("'");
											}
											else
											{
												strBuild.Append("null");
											}
											strBuild.Append(")");
				
											dbCmd.CommandText = strBuild.ToString();
											iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
											Logger.LogDebugInfo("module1","DomesticShipmentMgrDAL","AddDomesticShipment","INF004",iRowsAffected + " rows inserted into shipment_pkg table");
										}
									}
								}
							}
						}
					}
				}
				else
				{
					strBuild = new StringBuilder();
					strBuild.Append("update shipment ");
					strBuild.Append("set shpt_manifest_datetime = null, ");
					strBuild.Append("delivery_manifested = 'N'");
					strBuild.Append(" where booking_no = " + booking_no);
					strBuild.Append(" and consignment_no = '" + consignment_no + "'");

					dbCmd.CommandText = strBuild.ToString();
					iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
					Logger.LogDebugInfo("module1","DomesticShipmentMgrDAL","setAutomaticManifest","INF004",iRowsAffected + " rows inserted into shipment_pkg table");
				}

				transactionApp.Commit();
				Logger.LogDebugInfo("module1","DomesticShipmentMgrDAL","setAutomaticManifest","INF009","App db insert transaction committed.");
			
			}
			catch(ApplicationException appException)
			{
				try
				{
					transactionApp.Rollback();
					Logger.LogDebugInfo("module1","DomesticShipmentMgrDAL","setAutomaticManifest","INF010","App db insert transaction rolled back.");

				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("module1","DomesticShipmentMgrDAL","setAutomaticManifest","ERR008","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}


				Logger.LogTraceError("module1","DomesticShipmentMgrDAL","setAutomaticManifest","ERR009","Insert Transaction Failed : "+ appException.Message.ToString());
				throw new ApplicationException("Record Inserted - Not Manifested.",appException);
			}
			catch(Exception exception)
			{
				try
				{
					transactionApp.Rollback();
					Logger.LogDebugInfo("module1","DomesticShipmentMgrDAL","setAutomaticManifest","INF011","App db insert transaction rolled back.");

				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("module1","DomesticShipmentMgrDAL","setAutomaticManifest","ERR010","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}

				Logger.LogTraceError("module1","DomesticShipmentMgrDAL","setAutomaticManifest","ERR011","Insert Transaction Failed : "+ exception.Message.ToString());
				throw new ApplicationException("Record Inserted - Not Manifested.",exception);
			}
			finally
			{
				if(conApp != null)
				{
					conApp.Close();
				}
			}
		}
		
		
		public static decimal DBNullToZero(object obj)
		{
			return ((obj!=DBNull.Value)?Convert.ToDecimal(obj):0);
		}

		public static long AddDomesticShipment(String strAppID, String strEnterpriseID, DataSet dsDomesticShipment,DataSet dsVas,DataSet dsImportConsignment, DataSet dsPkg,String userID,String RecipZipCode, int importInsertUpdate)
		{
			string pickup_route = null, preshipment_booking_no = null;
			
			if(dsImportConsignment != null)
				if(dsImportConsignment.Tables.Count > 0)
					if(dsImportConsignment.Tables[0].Rows.Count > 0)
					{
						//return_pod_hc = dsImportConsignment.Tables[0].Rows[0]["return_pod_hc"].ToString();
						pickup_route = dsImportConsignment.Tables[0].Rows[0]["pickup_route"].ToString();
						preshipment_booking_no = dsImportConsignment.Tables[0].Rows[0]["preshipment_booking_no"].ToString();
					}

			IDbCommand dbCmd = null;
			int iRowsAffected = 0;
			StringBuilder strBuild = null;
			int cnt = 0;
			int i = 0;
			long iBooking_no = 0;
			//bool iBooking_State = false; //Phase2 - J02
			String strConsignmentNo = null;
			String strPayerID = null;
			String strSenderZipCode = null;
			String strRecipientZipCode = null;
			String strServiceCode = null;
			
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("module1","DomesticShipmentManager","AddDomesticShipment","ERR001","DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null",null);
			}
			if (dsDomesticShipment == null)
			{
				Logger.LogTraceError("module1","DomesticShipmentManager","AddDomesticShipment","ERR002","DataSet is null!!");
				throw new ApplicationException("The Shipment DataSet is null",null);
			}
			
			if(dsPkg == null)
			{
				Logger.LogTraceError("module1","DomesticShipmentManager","AddDomesticShipment","ERR003","DataSet is null!!");
				throw new ApplicationException("The Pakage DataSet is null",null);
			}

			//			if (dsDomesticShipment.Tables[0].Rows.Count > 0)
			//			{
			//				DataRow drEachTmp = dsDomesticShipment.Tables[0].Rows[0];
			//
			//				if((drEachTmp["consignment_no"]!= null) && (!drEachTmp["consignment_no"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			//				{
			//					if(IsAssignedConsignment(strAppID, strEnterpriseID, (String)drEachTmp["consignment_no"]))
			//					{
			//						Logger.LogTraceError("module1","DomesticShipmentManager","AddDomesticShipment","ERR003","Duplicate Consignment");
			//						throw new ApplicationException("Duplicate Consignment",null);
			//					}
			//				}	
			//			}

			IDbConnection conApp = dbCon.GetConnection();
			
			if(conApp == null)
			{
				Logger.LogTraceError("module1","DomesticShipmentMgrDAL","AddDomesticShipment","ERR004","conApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			try
			{
				dbCon.OpenConnection(ref conApp);
			}
			catch(ApplicationException appException)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("module1","DomesticShipmentMgrDAL","AddDomesticShipment","ERR005","Error opening connection : "+ appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ",appException);
			}
			catch(Exception exception)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("module1","DomesticShipmentMgrDAL","AddDomesticShipment","ERR006","Error opening connection : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}

			IDbTransaction transactionApp = dbCon.BeginTransaction(conApp,IsolationLevel.ReadCommitted);
			if(transactionApp == null)
			{
				Logger.LogTraceError("module1","DomesticShipmentMgrDAL","AddDomesticShipment","ERR007","transactionApp is null");
				throw new ApplicationException("Failed Begining a App Transaction..",null);
			}


			try
			{

				int result = ImportConsignmentsDAL.ImportConsignments_Clear(strAppID, strEnterpriseID, userID);

				if(result != 0)
				{
					string ErrorMsg = "";
					if(result == -1)
						ErrorMsg = "@userid parameter may not be NULL or blank.";
					else if(result == -2)
						ErrorMsg = "@userid is not a valid user ID for enterprise: PNGAF.";
					else if(result == -3)
						ErrorMsg = "@enterpriseid parameter is missing or invalid.";
					else if(result > 0)
						ErrorMsg = "SQL error message.";

					Logger.LogTraceError("module1","DomesticShipmentMgrDAL","AddDomesticShipment_ImportConsignments_Clear","","");
					throw new ApplicationException(ErrorMsg,null);
				}
				else
				{

					dbCmd = dbCon.CreateCommand("", CommandType.Text);
					dbCmd.Connection = conApp;
					dbCmd.Transaction = transactionApp;

					strPayerID = null;
					DataRow drEach = dsDomesticShipment.Tables[0].Rows[0];
					StringBuilder strBuilder = new StringBuilder();
					strBuilder.Append ("insert into ImportedConsignments(applicationid,enterpriseid,booking_no,");
					strBuilder.Append("consignment_no,ref_no,booking_datetime,payerid,");
					strBuilder.Append("new_account,payer_name,payer_address1,payer_address2,payer_zipcode,");
					strBuilder.Append("payer_country,payer_telephone,payer_fax,");
					strBuilder.Append("payment_mode,sender_name,sender_address1,sender_address2,sender_zipcode,");
					strBuilder.Append("sender_country,sender_telephone,sender_fax,");
					strBuilder.Append("sender_contact_person,recipient_name,recipient_address1,recipient_address2,");
					strBuilder.Append("recipient_zipcode,recipient_country,recipient_telephone,");//TU on 17June08
					strBuilder.Append("recipient_fax,service_code,recipient_contact_person,declare_value,");
					//strBuilder.Append("chargeable_wt,insurance_surcharge,max_insurance_cover,");
					strBuilder.Append("act_pickup_datetime,");
					strBuilder.Append("commodity_code,");
					strBuilder.Append("est_delivery_datetime,");
					//strBuilder.Append("act_delivery_date,");
					strBuilder.Append("payment_type,");
					//				strBuilder.Append("return_pod_slip,");
					//				strBuilder.Append("shipment_type,");
					//				strBuilder.Append("origin_state_code,");
					//				strBuilder.Append("invoice_no,destination_state_code,invoice_amt,tot_freight_charge,");
					//				strBuilder.Append("tot_vas_surcharge,debit_amt,credit_amt,cash_collected,last_status_code,");
					//				strBuilder.Append("last_exception_code,last_status_datetime,shpt_manifest_datetime,");
					//				strBuilder.Append("delivery_manifested,");
					//				strBuilder.Append("route_code,quotation_no,quotation_version,esa_surcharge,");
					//		strBuilder.Append("agent_pup_cost_consignment,agent_pup_cost_wt,agent_del_cost_consignment,agent_del_cost_wt,agent");
					strBuilder.Append("remark, cod_amount, ");
					//				strBuilder.Append("origin_station, destination_station, ");
					//HC Return Task
					strBuilder.Append("return_invoice_hc, userid, recordid, return_pod_hc, pickup_route, preshipment_booking_no");
					//HC Return Task
					//Insert Other surcharge amount By Aoo 22/02/2008
					//strBuilder.Append("other_surch_amount");
					strBuilder.Append(")values('"+strAppID+"',"+"'"+strEnterpriseID+"',");
					if((drEach["booking_no"]!= null) && (!drEach["booking_no"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						iBooking_no = Convert.ToInt64(drEach["booking_no"]);
						if(iBooking_no == 0)
							//generate the booking no for adhoc 
							iBooking_no = (long)Counter.GetNext(strAppID,strEnterpriseID,"booking_number",dbCon,dbCmd);
						//iBooking_State = true;//Phase2 - J02
					}
					else
					{
						//generate the booking no for adhoc 
						iBooking_no = (long)Counter.GetNext(strAppID,strEnterpriseID,"booking_number",dbCon,dbCmd);
					}
					strBuilder.Append(iBooking_no+",");
				
					if((drEach["consignment_no"]!= null) && (!drEach["consignment_no"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						strConsignmentNo = Utility.ReplaceSingleQuote(drEach["consignment_no"].ToString());
						strBuilder.Append("'");
						strBuilder.Append(strConsignmentNo);
						strBuilder.Append("'");
					}
					else
					{
						strBuilder.Append("null");
					}
					strBuilder.Append(",");


					if((drEach["ref_no"]!= null) && (!drEach["ref_no"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						String strRefNo = (String) drEach["ref_no"];
						strBuilder.Append("'");
						strBuilder.Append(Utility.ReplaceSingleQuote(strRefNo));
						strBuilder.Append("'");
					}
					else
					{
						strBuilder.Append("null");
					}
					strBuilder.Append(",");


					if((drEach["booking_datetime"]!= null) && (!drEach["booking_datetime"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						DateTime dtBookingDtTime = (DateTime) drEach["booking_datetime"];
						strBuilder.Append(Utility.DateFormat(strAppID,strEnterpriseID,dtBookingDtTime,DTFormat.DateTime));
					}
					else
					{
						strBuilder.Append("null");
					}
					strBuilder.Append(",");


					if((drEach["payerid"]!= null) && (!drEach["payerid"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						strPayerID = (String) drEach["payerid"];
						strBuilder.Append("'");
						strBuilder.Append(Utility.ReplaceSingleQuote(strPayerID));
						strBuilder.Append("'");
					}
					else
					{
						strBuilder.Append("null");
					}
					strBuilder.Append(",");

					//				if((drEach["payer_type"]!= null) && (!drEach["payer_type"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					//				{
					//					String strPayerType = (String) drEach["payer_type"];
					//					strBuilder.Append("'");
					//					strBuilder.Append(strPayerType);
					//					strBuilder.Append("'");
					//				}
					//				else
					//				{
					//					strBuilder.Append("null");
					//				}
					//				strBuilder.Append(",");

					if((drEach["new_account"]!= null) && (!drEach["new_account"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						String strNewAccount = (String) drEach["new_account"];
						strBuilder.Append("'");
						strBuilder.Append(strNewAccount);
						strBuilder.Append("'");
					}
					else
					{
						strBuilder.Append("null");
					}
					strBuilder.Append(",");

					if((drEach["payer_name"]!= null) && (!drEach["payer_name"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						String strPayerName = (String) drEach["payer_name"];
						strBuilder.Append("N'");
						strBuilder.Append(Utility.ReplaceSingleQuote(strPayerName));
						strBuilder.Append("'");
					}
					else
					{
						strBuilder.Append("null");
					}
					strBuilder.Append(",");

					if((drEach["payer_address1"]!= null) && (!drEach["payer_address1"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						String strPayerAddress1 = (String) drEach["payer_address1"];
						strBuilder.Append("N'");
						strBuilder.Append(Utility.ReplaceSingleQuote(strPayerAddress1));
						strBuilder.Append("'");
					}
					else
					{
						strBuilder.Append("null");
					}
					strBuilder.Append(",");

					if((drEach["payer_address2"]!= null) && (!drEach["payer_address2"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						String strPayerAddress2 = (String) drEach["payer_address2"];
						strBuilder.Append("N'");
						strBuilder.Append(Utility.ReplaceSingleQuote(strPayerAddress2));
						strBuilder.Append("'");
					}
					else
					{
						strBuilder.Append("null");
					}
					strBuilder.Append(",");

					if((drEach["payer_zipcode"]!= null) && (!drEach["payer_zipcode"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						String strPayerZipCode = (String) drEach["payer_zipcode"];
						strBuilder.Append("'");
						strBuilder.Append(Utility.ReplaceSingleQuote(strPayerZipCode));
						strBuilder.Append("'");
					}
					else
					{
						strBuilder.Append("null");
					}
					strBuilder.Append(",");
				

					if((drEach["payer_country"]!= null) && (!drEach["payer_country"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						String strPayerCountry = (String) drEach["payer_country"];
						strBuilder.Append("'");
						strBuilder.Append(strPayerCountry);
						strBuilder.Append("'");
					}
					else
					{
						strBuilder.Append("null");
					}
					strBuilder.Append(",");

					if((drEach["payer_telephone"]!= null) && (!drEach["payer_telephone"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						String strPayerTelephone = (String) drEach["payer_telephone"];
						strBuilder.Append("'");
						strBuilder.Append(Utility.ReplaceSingleQuote(strPayerTelephone));
						strBuilder.Append("'");
					}
					else
					{
						strBuilder.Append("null");
					}
					strBuilder.Append(",");

					if((drEach["payer_fax"]!= null) && (!drEach["payer_fax"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						String strPayerFax = (String) drEach["payer_fax"];
						strBuilder.Append("'");
						strBuilder.Append(Utility.ReplaceSingleQuote(strPayerFax));
						strBuilder.Append("'");
					}
					else
					{
						strBuilder.Append("null");
					}
					strBuilder.Append(",");

					if((drEach["payment_mode"]!= null) && (!drEach["payment_mode"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						String strPaymentMode = (String) drEach["payment_mode"];
						strBuilder.Append("'");
						strBuilder.Append(strPaymentMode);
						strBuilder.Append("'");
					}
					else
					{
						strBuilder.Append("null");
					}
					strBuilder.Append(",");

					if((drEach["sender_name"]!= null) && (!drEach["sender_name"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						String strSenderName = (String) drEach["sender_name"];
						strBuilder.Append("N'");
						strBuilder.Append(Utility.ReplaceSingleQuote(strSenderName));
						strBuilder.Append("'");
					}
					else
					{
						strBuilder.Append("null");
					}
					strBuilder.Append(",");

					if((drEach["sender_address1"]!= null) && (!drEach["sender_address1"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						String strSenderAddress1 = (String) drEach["sender_address1"];
						strBuilder.Append("N'");
						strBuilder.Append(Utility.ReplaceSingleQuote(strSenderAddress1));
						strBuilder.Append("'");
					}
					else
					{
						strBuilder.Append("null");
					}
					strBuilder.Append(",");

					if((drEach["sender_address2"]!= null) && (!drEach["sender_address2"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						String strSenderAddress2 = (String) drEach["sender_address2"];
						strBuilder.Append("N'");
						strBuilder.Append(Utility.ReplaceSingleQuote(strSenderAddress2));
						strBuilder.Append("'");
					}
					else
					{
						strBuilder.Append("null");
					}
					strBuilder.Append(",");

					if((drEach["sender_zipcode"]!= null) && (!drEach["sender_zipcode"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						strSenderZipCode = (String) drEach["sender_zipcode"];
						strBuilder.Append("'");
						strBuilder.Append(Utility.ReplaceSingleQuote(strSenderZipCode));
						strBuilder.Append("'");
					}
					else
					{
						strBuilder.Append("null");
					}
					strBuilder.Append(",");

					if((drEach["sender_country"]!= null) && (!drEach["sender_country"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						String strSenderCountry = (String) drEach["sender_country"];
						strBuilder.Append("'");
						strBuilder.Append(strSenderCountry);
						strBuilder.Append("'");
					}
					else
					{
						strBuilder.Append("null");
					}
					strBuilder.Append(",");

					if((drEach["sender_telephone"]!= null) && (!drEach["sender_telephone"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						String strSenderTelephone = (String) drEach["sender_telephone"];
						strBuilder.Append("'");
						strBuilder.Append(Utility.ReplaceSingleQuote(strSenderTelephone));
						strBuilder.Append("'");
					}
					else
					{
						strBuilder.Append("null");
					}
					strBuilder.Append(",");

					if((drEach["sender_fax"]!= null) && (!drEach["sender_fax"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						String strSenderFax = (String) drEach["sender_fax"];
						strBuilder.Append("'");
						strBuilder.Append(Utility.ReplaceSingleQuote(strSenderFax));
						strBuilder.Append("'");
					}
					else
					{
						strBuilder.Append("null");
					}
					strBuilder.Append(",");

					if((drEach["sender_contact_person"]!= null) && (!drEach["sender_contact_person"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						String strSenderContactPerson = (String) drEach["sender_contact_person"];
						strBuilder.Append("N'");
						strBuilder.Append(Utility.ReplaceSingleQuote(strSenderContactPerson));
						strBuilder.Append("'");
					}
					else
					{
						strBuilder.Append("null");
					}
					strBuilder.Append(",");

					if((drEach["recipient_name"]!= null) && (!drEach["recipient_name"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						String strRecipientName = (String) drEach["recipient_name"];
						strBuilder.Append("N'");
						strBuilder.Append(Utility.ReplaceSingleQuote(strRecipientName));
						strBuilder.Append("'");
					}
					else
					{
						strBuilder.Append("null");
					}
					strBuilder.Append(",");

					if((drEach["recipient_address1"]!= null) && (!drEach["recipient_address1"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						String strRecipientAddress1 = (String) drEach["recipient_address1"];
						strBuilder.Append("N'");
						strBuilder.Append(Utility.ReplaceSingleQuote(strRecipientAddress1));
						strBuilder.Append("'");
					}
					else
					{
						strBuilder.Append("null");
					}
					strBuilder.Append(",");

					if((drEach["recipient_address2"]!= null) && (!drEach["recipient_address2"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						String strRecipientAddress2 = (String) drEach["recipient_address2"];
						strBuilder.Append("N'");
						strBuilder.Append(Utility.ReplaceSingleQuote(strRecipientAddress2));
						strBuilder.Append("'");
					}
					else
					{
						strBuilder.Append("null");
					}
					strBuilder.Append(",");


					if((drEach["recipient_zipcode"]!= null) && (!drEach["recipient_zipcode"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						strRecipientZipCode = (String) drEach["recipient_zipcode"];
						strBuilder.Append("'");
						strBuilder.Append(Utility.ReplaceSingleQuote(strRecipientZipCode));
						strBuilder.Append("'");
					}
					else
					{
						strBuilder.Append("null");
					}
					strBuilder.Append(",");

					if((drEach["recipient_country"]!= null) && (!drEach["recipient_country"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						String strRecipientCountry = (String) drEach["recipient_country"];
						strBuilder.Append("'");
						strBuilder.Append(strRecipientCountry);
						strBuilder.Append("'");
					}
					else
					{
						strBuilder.Append("null");
					}
					strBuilder.Append(",");
					//
					//				if((drEach["tot_wt"]!= null) && (!drEach["tot_wt"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					//				{
					//					decimal decTotWt = (decimal) drEach["tot_wt"];
					//					strBuilder.Append(decTotWt);
					//				}
					//				else
					//				{
					//					strBuilder.Append("null");
					//				}
					//				//TU on 17June08
					//				strBuilder.Append(",");

					//				if((drEach["tot_act_wt"]!= null) && (!drEach["tot_act_wt"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					//				{
					//					decimal decTot_act_Wt = (decimal) drEach["tot_act_wt"];
					//					strBuilder.Append(decTot_act_Wt);
					//				}
					//				else
					//				{
					//					strBuilder.Append("null");
					//				}
					//				//
					//				strBuilder.Append(",");


					if((drEach["recipient_telephone"]!= null) && (!drEach["recipient_telephone"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						String strRecipientTelephone = (String) drEach["recipient_telephone"];
						strBuilder.Append("'");
						strBuilder.Append(Utility.ReplaceSingleQuote(strRecipientTelephone));
						strBuilder.Append("'");
					}
					else
					{
						strBuilder.Append("null");
					}
					strBuilder.Append(",");


					//				if((drEach["tot_dim_wt"]!= null) && (!drEach["tot_dim_wt"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					//				{
					//					decimal decTotDimWt = (decimal) drEach["tot_dim_wt"];
					//					strBuilder.Append(decTotDimWt);
					//				}
					//				else
					//				{
					//					strBuilder.Append("null");
					//				}
					//				strBuilder.Append(",");

					if((drEach["recipient_fax"]!= null) && (!drEach["recipient_fax"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						String strRecipientFax = (String) drEach["recipient_fax"];
						strBuilder.Append("'");
						strBuilder.Append(Utility.ReplaceSingleQuote(strRecipientFax));
						strBuilder.Append("'");
					}
					else
					{
						strBuilder.Append("null");
					}
					strBuilder.Append(",");

					//				
					//				if((drEach["tot_pkg"]!= null) && (!drEach["tot_pkg"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					//				{
					//					int iTotPkg = Convert.ToInt32(drEach["tot_pkg"]);
					//					strBuilder.Append(iTotPkg);
					//				}
					//				else
					//				{
					//					strBuilder.Append("null");
					//				}
					//				strBuilder.Append(",");



					if((drEach["service_code"]!= null) && (!drEach["service_code"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						strServiceCode = (String) drEach["service_code"];
						strBuilder.Append("'");
						strBuilder.Append(strServiceCode);
						strBuilder.Append("'");
					}
					else
					{
						strBuilder.Append("null");
					}
					strBuilder.Append(",");


					if((drEach["recipient_contact_person"]!= null) && (!drEach["recipient_contact_person"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						String strRecipientContactPerson = (String) drEach["recipient_contact_person"];
						strBuilder.Append("N'");
						strBuilder.Append(Utility.ReplaceSingleQuote(strRecipientContactPerson));
						strBuilder.Append("'");
					}
					else
					{
						strBuilder.Append("null");
					}
					strBuilder.Append(",");

					if((drEach["declare_value"]!= null) && (!drEach["declare_value"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						decimal decDeclareValue = (decimal) drEach["declare_value"];
						strBuilder.Append(decDeclareValue);
					}
					else
					{
						strBuilder.Append("null");
					}
					strBuilder.Append(",");
					//
					//				if((drEach["chargeable_wt"]!= null) && (!drEach["chargeable_wt"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					//				{
					//					decimal decChargeableWt = (decimal) drEach["chargeable_wt"];
					//					strBuilder.Append(decChargeableWt);
					//				}
					//				else
					//				{
					//					strBuilder.Append("null");
					//				}
					//				strBuilder.Append(",");
					//
					//				if((drEach["insurance_surcharge"]!= null) && (!drEach["insurance_surcharge"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					//				{
					//					decimal decInsuranceSurcharge = (decimal) drEach["insurance_surcharge"];
					//					strBuilder.Append(decInsuranceSurcharge);
					//				}
					//				else
					//				{
					//					strBuilder.Append("null");
					//				}
					//				strBuilder.Append(",");
					//
					//				if((drEach["max_insurance_cover"]!= null) && (!drEach["max_insurance_cover"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					//				{
					//					decimal decMaxInsuranceCover = (decimal) drEach["max_insurance_cover"];
					//					strBuilder.Append(decMaxInsuranceCover);
					//				}
					//				else
					//				{
					//					strBuilder.Append("null");
					//				}
					//				strBuilder.Append(",");

					if((drEach["act_pickup_datetime"]!= null) && (!drEach["act_pickup_datetime"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						DateTime dtActPickup = Convert.ToDateTime(drEach["act_pickup_datetime"]);
						strBuilder.Append(Utility.DateFormat(strAppID,strEnterpriseID,dtActPickup,DTFormat.DateTime));
					}
					else
					{
						strBuilder.Append("null");
					}
					strBuilder.Append(",");

					if((drEach["commodity_code"]!= null) && (!drEach["commodity_code"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						String strCommodityCode = (String) drEach["commodity_code"];
						strBuilder.Append("'");
						strBuilder.Append(strCommodityCode);
						strBuilder.Append("'");
					}
					else
					{
						strBuilder.Append("null");
					}
					strBuilder.Append(",");


					if((drEach["est_delivery_datetime"]!= null) && (!drEach["est_delivery_datetime"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						DateTime dtEstDeliveryDatetime = (DateTime) drEach["est_delivery_datetime"];
						strBuilder.Append(Utility.DateFormat(strAppID,strEnterpriseID,dtEstDeliveryDatetime,DTFormat.DateTime));
					}
					else
					{
						strBuilder.Append("null");
					}
					strBuilder.Append(",");

					//				if((drEach["percent_dv_additional"]!= null) && (!drEach["percent_dv_additional"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					//				{
					//					decimal decPercentDvAdditional = (decimal) drEach["percent_dv_additional"];
					//					strBuilder.Append(decPercentDvAdditional);
					//				}
					//				else
					//				{
					//					strBuilder.Append("null");
					//				}
					//				strBuilder.Append(",");

					//				if((drEach["act_delivery_date"]!= null) && (!drEach["act_delivery_date"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					//				{
					//					DateTime dtActDeliveryDate = (DateTime) drEach["act_delivery_date"];
					//					strBuilder.Append(Utility.DateFormat(strAppID,strEnterpriseID,dtActDeliveryDate,DTFormat.DateTime));
					//				}
					//				else
					//				{
					//					strBuilder.Append("null");
					//				}
					//				strBuilder.Append(",");

					if((drEach["payment_type"]!= null) && (!drEach["payment_type"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						String strPaymentType = (String) drEach["payment_type"];
						strBuilder.Append("'");
						strBuilder.Append(strPaymentType);
						strBuilder.Append("'");
					}
					else
					{
						strBuilder.Append("null");
					}
					strBuilder.Append(",");

				

					//				if((drEach["return_pod_slip"]!= null) && (!drEach["return_pod_slip"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					//				{
					//					String strReturnPODSlip = (String) drEach["return_pod_slip"];
					//					strBuilder.Append("'");
					//					strBuilder.Append(strReturnPODSlip);
					//					strBuilder.Append("'");
					//				}
					//				else
					//				{
					//					strBuilder.Append("null");
					//				}
					//				strBuilder.Append(",");

					//				if((drEach["shipment_type"]!= null) && (!drEach["shipment_type"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					//				{
					//					String strShipmentType = (String) drEach["shipment_type"];
					//					strBuilder.Append("'");
					//					strBuilder.Append(strShipmentType);
					//					strBuilder.Append("'");
					//				}
					//				else
					//				{
					//					strBuilder.Append("null");
					//				}
					//				strBuilder.Append(",");


					//				if((drEach["origin_state_code"]!= null) && (!drEach["origin_state_code"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					//				{
					//					String strOriginStateCode = (String) drEach["origin_state_code"];
					//					strBuilder.Append("'");
					//					strBuilder.Append(strOriginStateCode);
					//					strBuilder.Append("'");
					//				}
					//				else
					//				{
					//					strBuilder.Append("null");
					//				}
					//				strBuilder.Append(",");

					//				if((drEach["invoice_no"]!= null) && (!drEach["invoice_no"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					//				{
					//					strInvoiceNo = (String) drEach["invoice_no"];
					//					strBuilder.Append("'");
					//					strBuilder.Append(strInvoiceNo);
					//					strBuilder.Append("'");
					//				}
					//				else
					//				{
					//					strBuilder.Append("null");
					//				}
					//				strBuilder.Append(",");

					//				if((drEach["destination_state_code"]!= null) && (!drEach["destination_state_code"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					//				{
					//					String strDestinationStateCode = (String) drEach["destination_state_code"];
					//					strBuilder.Append("'");
					//					strBuilder.Append(strDestinationStateCode);
					//					strBuilder.Append("'");
					//				}
					//				else
					//				{
					//					strBuilder.Append("null");
					//				}
					//				strBuilder.Append(",");

					//				if((drEach["invoice_amt"]!= null) && (!drEach["invoice_amt"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					//				{
					//					decimal decInvoiceAmt = (decimal) drEach["invoice_amt"];
					//					
					//					strBuilder.Append(decInvoiceAmt);
					//					
					//				}
					//				else
					//				{
					//					strBuilder.Append("null");
					//				}
					//				strBuilder.Append(",");

					//				if((drEach["tot_freight_charge"]!= null) && (!drEach["tot_freight_charge"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					//				{
					//					decimal decTotFreight = (decimal) drEach["tot_freight_charge"];
					//					
					//					strBuilder.Append(decTotFreight);
					//					
					//				}
					//				else
					//				{
					//					strBuilder.Append("null");
					//				}
					//				strBuilder.Append(",");

					//				if((drEach["tot_vas_surcharge"]!= null) && (!drEach["tot_vas_surcharge"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					//				{
					//					decimal decTotVASSurcharge = (decimal) drEach["tot_vas_surcharge"];
					//					
					//					strBuilder.Append(decTotVASSurcharge);
					//					
					//				}
					//				else
					//				{
					//					strBuilder.Append("null");
					//				}
					//				strBuilder.Append(",");

					//				if((drEach["debit_amt"]!= null) && (!drEach["debit_amt"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					//				{
					//					decimal decDebitAmt = (decimal) drEach["debit_amt"];
					//					
					//					strBuilder.Append(decDebitAmt);
					//					
					//				}
					//				else
					//				{
					//					strBuilder.Append("null");
					//				}
					//				strBuilder.Append(",");
			
					//				if((drEach["credit_amt"]!= null) && (!drEach["credit_amt"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					//				{
					//					decimal decCreditAmt = (decimal) drEach["credit_amt"];
					//					
					//					strBuilder.Append(decCreditAmt);
					//					
					//				}
					//				else
					//				{
					//					strBuilder.Append("null");
					//				}
					//				strBuilder.Append(",");

					//				if((drEach["cash_collected"]!= null) && (!drEach["cash_collected"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					//				{
					//					decimal decCashCollected = (decimal) drEach["cash_collected"];
					//					strBuilder.Append(decCashCollected);	
					//				}
					//				else
					//				{
					//					strBuilder.Append("null");
					//				}
					//				strBuilder.Append(",");

				
					//				if((drEach["last_status_code"]!= null) && (!drEach["last_status_code"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					//				{
					//					String strLastStatusCode = (String) drEach["last_status_code"];
					//					strBuilder.Append("'");
					//					strBuilder.Append(strLastStatusCode);	
					//					strBuilder.Append("'");
					//				}
					//				else
					//				{
					//					strBuilder.Append("null");
					//				}
					//				strBuilder.Append(",");

					//				if((drEach["last_exception_code"]!= null) && (!drEach["last_exception_code"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					//				{
					//					String strLastExceptionCode = (String) drEach["last_exception_code"];
					//					strBuilder.Append("'");
					//					strBuilder.Append(strLastExceptionCode);	
					//					strBuilder.Append("'");
					//				}
					//				else
					//				{
					//					strBuilder.Append("null");
					//				}
					//				strBuilder.Append(",");

				
					//				if((drEach["last_status_datetime"]!= null) && (!drEach["last_status_datetime"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					//				{
					//					DateTime dtLastStatusDateTime = (DateTime) drEach["last_status_datetime"];
					//					strBuilder.Append(Utility.DateFormat(strAppID,strEnterpriseID,dtLastStatusDateTime,DTFormat.DateTime));
					//
					//
					//				}
					//				else
					//				{
					//					strBuilder.Append("null");
					//				}
					//				strBuilder.Append(",");

					//				if((drEach["shpt_manifest_datetime"]!= null) && (!drEach["shpt_manifest_datetime"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					//				{
					//					DateTime dtShptManifestDateTime = (DateTime) drEach["shpt_manifest_datetime"];
					//					strBuilder.Append(Utility.DateFormat(strAppID,strEnterpriseID,dtShptManifestDateTime,DTFormat.DateTime));
					//				}
					//				else
					//				{
					//					strBuilder.Append("null");
					//				}
					//				strBuilder.Append(",");

					//				if((drEach["delivery_manifested"]!= null) && (!drEach["delivery_manifested"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					//				{
					//					String strDeliveryManifested = (String) drEach["delivery_manifested"];
					//					strBuilder.Append("'");
					//					strBuilder.Append(strDeliveryManifested);	
					//					strBuilder.Append("'");
					//				}
					//				else
					//				{
					//					strBuilder.Append("null");
					//				}
					//				strBuilder.Append(",");

					//				if((drEach["route_code"]!= null) && (!drEach["route_code"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					//				{
					//					String strRouteCode = (String) drEach["route_code"];
					//					strBuilder.Append("'");
					//					strBuilder.Append(strRouteCode);	
					//					strBuilder.Append("'");
					//				}
					//				else
					//				{
					//					strBuilder.Append("null");
					//				}
					//				strBuilder.Append(",");

					//				if((drEach["quotation_no"]!= null) && (!drEach["quotation_no"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					//				{
					//					String strQuotationNo = (String) drEach["quotation_no"];
					//					strBuilder.Append("'");
					//					strBuilder.Append(strQuotationNo);	
					//					strBuilder.Append("'");
					//				}
					//				else
					//				{
					//					strBuilder.Append("null");
					//				}
					//				strBuilder.Append(",");			

					//				if((drEach["quotation_version"]!= null) && (!drEach["quotation_version"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					//				{
					//					int iQuotationVersion = Convert.ToInt32(drEach["quotation_version"]);
					//					strBuilder.Append(iQuotationVersion);
					//				}
					//				else
					//				{
					//					strBuilder.Append("null");
					//				}
					//				strBuilder.Append(",");
					
					//				if((drEach["esa_surcharge"]!= null) && (!drEach["esa_surcharge"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					//				{
					//					decimal decESASurcharge = (decimal) drEach["esa_surcharge"];
					//					strBuilder.Append(decESASurcharge);
					//				}
					//				else
					//				{
					//					strBuilder.Append("null");
					//				}
					//				strBuilder.Append(",");

					//Calculate the Agent Cost.
					//				if((drEach["payer_type"]!= null) && (!drEach["payer_type"].GetType().Equals(System.Type.GetType("System.DBNull"))) && drEach["payer_type"].ToString().Equals("A"))
					//				{
					//					
					//					decimal decTotWt = 0;
					//					decimal decTot_act_Wt = 0;
					//					AgentConsgCost agentConsgSnd;
					//					agentConsgSnd.decCost = 0;
					//					agentConsgSnd.strAgentID = null;
					//
					//					AgentWtCost agentWtSnd;
					//					agentWtSnd.decCost = 0;
					//					agentWtSnd.strAgentID = null;
					//
					//					AgentConsgCost agentConsgRcp;
					//					agentConsgRcp.decCost = 0;
					//					agentConsgRcp.strAgentID = null;
					//
					//					AgentWtCost agentWtRcp;
					//					agentWtRcp.decCost = 0;
					//					agentWtRcp.strAgentID = null;
					//
					//					if((drEach["tot_wt"]!= null) && (!drEach["tot_wt"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					//					{
					//						decTotWt = (decimal) drEach["tot_wt"];
					//					}
					//					//TU on 17June08
					//					if((drEach["tot_act_wt"]!= null) && (!drEach["tot_act_wt"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					//					{
					//						decTot_act_Wt = (decimal) drEach["tot_act_wt"];
					//					}
					//					//
					//					if((drEach["sender_zipcode"]!= null) && (!drEach["sender_zipcode"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					//					{
					//						strSenderZipCode = (String) drEach["sender_zipcode"];
					//						//Check whether the Sender Zipcode exists in Agent_Zipcode table.
					//						//				agentConsgSnd =  TIESUtility.GetAgentConsignmentCost(strAppID,strEnterpriseID,strSenderZipCode);
					//						
					//						//Agent pickup Cost Weight
					//						//				agentWtSnd = TIESUtility.GetAgentWtCost(strAppID,strEnterpriseID,strSenderZipCode,decTotWt);
					//					}
					//
					//					if((drEach["recipient_zipcode"]!= null) && (!drEach["recipient_zipcode"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					//					{
					//						strRecipientZipCode = (String) drEach["recipient_zipcode"];
					//						//decimal decDlvryCost = 0;
					//						//Check whether the Recipient Zipcode exists in Agent_Zipcode table.
					//						//				agentConsgRcp =  TIESUtility.GetAgentConsignmentCost(strAppID,strEnterpriseID,strRecipientZipCode);
					//						
					//						//						if(decDlvryCost > 0)
					//						//							strBuilder.Append(decDlvryCost);
					//						//						else
					//						//							strBuilder.Append("null");
					//						//						strBuilder.Append(",");
					//						
					//						//Agent Delivery Cost Weight
					//						//				//decimal decDlvryWt = 0;
					//						//				agentWtRcp = TIESUtility.GetAgentWtCost(strAppID,strEnterpriseID,strRecipientZipCode,decTotWt);
					//
					//						//						if(decDlvryWt > 0)
					//						//							strBuilder.Append(decDlvryWt);
					//						//						else
					//						//							strBuilder.Append("null");
					//					}
					//
					//					if((agentConsgSnd.strAgentID != null) && (agentConsgRcp.strAgentID != null) && (agentConsgSnd.strAgentID == agentConsgRcp.strAgentID))
					//					{
					//						//No charges
					//						strBuilder.Append("null");
					//						strBuilder.Append(",");
					//						strBuilder.Append("null");
					//						strBuilder.Append(",");
					//						strBuilder.Append("null");
					//						strBuilder.Append(",");
					//						strBuilder.Append("null");	
					//						strBuilder.Append(",");
					//					}
					//					else
					//					{
					//
					//						if(agentConsgSnd.decCost > 0)
					//							strBuilder.Append(agentConsgSnd.decCost);
					//						else
					//							strBuilder.Append("null");
					//						strBuilder.Append(",");
					//
					//						if(agentWtSnd.decCost > 0)
					//							strBuilder.Append(agentWtSnd.decCost);
					//						else
					//							strBuilder.Append("null");
					//						strBuilder.Append(",");
					//
					//
					//
					//						if(agentConsgRcp.decCost > 0)
					//							strBuilder.Append(agentConsgRcp.decCost);
					//						else
					//							strBuilder.Append("null");
					//						strBuilder.Append(",");
					//
					//						if(agentWtRcp.decCost > 0)
					//							strBuilder.Append(agentWtRcp.decCost);
					//						else
					//							strBuilder.Append("null");
					//						strBuilder.Append(",");
					//
					//					}
					//
					//				}
					//				else //case other (C,N,V,E,..) by Tumz 10.06.54
					//				{
					//					strBuilder.Append("null");
					//					strBuilder.Append(",");
					//					strBuilder.Append("null");
					//					strBuilder.Append(",");
					//					strBuilder.Append("null");
					//					strBuilder.Append(",");
					//					strBuilder.Append("null");
					//					strBuilder.Append(",");
					//				}

					//End Added By Tom Feb 4,2010
					//
					if((drEach["remark"]!= null) && (!drEach["remark"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						strBuilder.Append("N'");
						String strRemark = (String) drEach["remark"];			
						strBuilder.Append(Utility.ReplaceSingleQuote(strRemark));
						strBuilder.Append("'");
					}
					else
					{
						strBuilder.Append("null");
					}
					strBuilder.Append(",");

					//				if((drEach["mbg"]!= null) && (!drEach["mbg"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					//				{
					//					String strmbg = (String) drEach["mbg"];
					//					strBuilder.Append("'");
					//					strBuilder.Append(strmbg);	
					//					strBuilder.Append("'");
					//				}
					//				else
					//				{
					//					strBuilder.Append("null");
					//				}
					//				strBuilder.Append(",");

					if((drEach["cod_amount"]!= null) && (!drEach["cod_amount"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						decimal decCod_amount = (decimal) drEach["cod_amount"];
						strBuilder.Append(decCod_amount);
					}
					else
					{
						strBuilder.Append("0");
					}
					strBuilder.Append(",");

					//				if((drEach["origin_station"]!= null) && (!drEach["origin_station"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					//				{
					//					String strorigin_station = (String) drEach["origin_station"];
					//					strBuilder.Append("'");
					//					strBuilder.Append(strorigin_station);	
					//					strBuilder.Append("'");
					//				}
					//				else
					//				{
					//					strBuilder.Append("null");
					//				}
					//				strBuilder.Append(",");

					//				if((drEach["destination_station"]!= null) && (!drEach["destination_station"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					//				{
					//					String strdest_station = (String) drEach["destination_station"];
					//					strBuilder.Append("'");
					//					strBuilder.Append(strdest_station);	
					//					strBuilder.Append("'");
					//				}
					//				else
					//				{
					//					strBuilder.Append("null");
					//				}
					//				//HC Return Task
					//				strBuilder.Append(",");

					if((drEach["return_invoice_hc"]!= null) && (!drEach["return_invoice_hc"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						String strreturn_invoice_hc = (String) drEach["return_invoice_hc"];
						strBuilder.Append("'");
						strBuilder.Append(strreturn_invoice_hc);	
						strBuilder.Append("'");
					}
					else
					{
						strBuilder.Append("null");
					}
					strBuilder.Append(",");


					//				if((drEach["est_hc_return_datetime"]!= null) && (!drEach["est_hc_return_datetime"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					//				{
					//					DateTime dtest_hc_return_datetime = (DateTime) drEach["est_hc_return_datetime"];
					//					strBuilder.Append(Utility.DateFormat(strAppID,strEnterpriseID,dtest_hc_return_datetime,DTFormat.DateTime));
					//				}
					//				else
					//				{
					//					strBuilder.Append("null");
					//				}
					//
					//				strBuilder.Append(",");
					//
					//				if((drEach["act_hc_return_datetime"]!= null) && (!drEach["act_hc_return_datetime"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					//				{
					//					DateTime dtact_hc_return_datetime = (DateTime) drEach["act_hc_return_datetime"];
					//					strBuilder.Append(Utility.DateFormat(strAppID,strEnterpriseID,dtact_hc_return_datetime,DTFormat.DateTime));
					//				}
					//				else
					//				{
					//					strBuilder.Append("null");
					//				}
					//				//HC Return Task
					//				//Insert Other surcharge amount By Aoo 22/02/2008
					//				strBuilder.Append(",");
					//
					//				if((drEach["other_surch_amount"] != null) && (!drEach["other_surch_amount"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					//				{
					//					decimal strOtherSurchAmt = (decimal) drEach["other_surch_amount"];
					//					//strBuilder.Append("'");
					//					strBuilder.Append(strOtherSurchAmt);
					//					//strBuilder.Append("'");
					//				}
					//				else
					//				{
					//					strBuilder.Append("null");
					//				}
					//				strBuilder.Append(",");
					//
					//				//Calculate Total Amount (total_rated_amount)
					//				object  obj_tot_freight_charge = drEach["tot_freight_charge"], obj_insurance_surcharge = drEach["insurance_surcharge"],
					//					obj_other_surch_amount = drEach["other_surch_amount"], obj_tot_vas_surcharge = drEach["tot_vas_surcharge"],
					//					obj_esa_surcharge = drEach["esa_surcharge"];
					//				if( obj_tot_freight_charge!=DBNull.Value && obj_insurance_surcharge!=DBNull.Value && 
					//					obj_other_surch_amount!=DBNull.Value && obj_tot_vas_surcharge!=DBNull.Value && 
					//					obj_esa_surcharge!=DBNull.Value)
					//				{
					//					decimal d_tot_freight_charge=DBNullToZero(obj_tot_freight_charge), d_insurance_surcharge=DBNullToZero(obj_insurance_surcharge),
					//						d_other_surch_amount=DBNullToZero(obj_other_surch_amount),d_tot_vas_surcharge=DBNullToZero(obj_tot_vas_surcharge),
					//						d_esa_surcharge=DBNullToZero(obj_esa_surcharge);
					//					d_total_rated_amount = d_tot_freight_charge + d_insurance_surcharge + d_other_surch_amount + 
					//						d_tot_vas_surcharge + d_esa_surcharge;
					//					strBuilder.Append(d_total_rated_amount);
					//				}
					//				else
					//				{
					//					strBuilder.Append("null");
					//				}
					//
					//				//////////////// by Tumz ////////////////////////
					//				if(drEach["pkg_detail_replaced_by"] != null && drEach["pkg_detail_replaced_by"].ToString() != "")
					//				{
					//					strBuilder.Append(",'");
					//					strBuilder.Append(drEach["pkg_detail_replaced_by"]);
					//					strBuilder.Append("','");
					//					strBuilder.Append(drEach["pkg_detail_replaced_datetime"]);
					//					strBuilder.Append("'");
					//				}
					//				else
					//					strBuilder.Append(" , null, null ");

					//opal 20121108
					if(userID != null)
					{
						strBuilder.Append("'");
						strBuilder.Append(userID);	
						strBuilder.Append("',1,");
					
					}
					else
					{
						strBuilder.Append("null,1,");
					
					}

					if((drEach["return_pod_slip"]!= null) && (!drEach["return_pod_slip"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						String strreturn_pod_slip = (String) drEach["return_pod_slip"];
						strBuilder.Append("'");
						strBuilder.Append(strreturn_pod_slip);	
						strBuilder.Append("'");
					}
					else
					{
						strBuilder.Append("null");
					}
					strBuilder.Append(",");

					if(pickup_route != null)
					{
						strBuilder.Append("'");
						strBuilder.Append(pickup_route);	
						strBuilder.Append("'");
					}
					else
					{
						strBuilder.Append("null");
					}
					strBuilder.Append(",");

				
					if(preshipment_booking_no != null)
					{
						strBuilder.Append("'");
						strBuilder.Append(preshipment_booking_no);	
						strBuilder.Append("'");
					}
					else
					{
						strBuilder.Append("null");
					}

					strBuilder.Append(")");
					String strQuery = strBuilder.ToString();
					dbCmd = dbCon.CreateCommand(strQuery,CommandType.Text);
					dbCmd.Connection = conApp;
					dbCmd.Transaction = transactionApp;

					Logger.LogDebugInfo("module1","DomesticShipmentMgrDAL","AddDomesticShipment","INF001","QUERY IS : "+strQuery);
					iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
					Logger.LogDebugInfo("module1","DomesticShipmentMgrDAL","AddDomesticShipment","INF002",iRowsAffected + " rows inserted into Shipment table ....");
			

					//Inserting the data in the Shipment_PKG table
					cnt = dsPkg.Tables[0].Rows.Count;
					i = 0;
					Customer customer = new Customer();
					customer.Populate(strAppID,strEnterpriseID, strPayerID);
					Zipcode zipcode = new Zipcode();

					zipcode.Populate(strAppID,strEnterpriseID,strSenderZipCode);
					String strOrgZone = zipcode.ZoneCode;

					zipcode.Populate(strAppID,strEnterpriseID,strRecipientZipCode);
					String strDestnZone = zipcode.ZoneCode;
					//					int sumQty = 0;
					//					float weightToFreight = 0;
					//					decimal freightCharge = 0;
					for(i=0;i<cnt;i++)
					{
						DataRow drEachPKG = dsPkg.Tables[0].Rows[i];
						String strMpsNo = "";
						if(drEachPKG["mps_code"].ToString() != "")
						{
							strMpsNo = drEachPKG["mps_code"].ToString() ;
						}

						//					decimal decPkgLgth = (decimal)drEachPKG["pkg_length"];
						//					decimal decPkgBrdth = (decimal)drEachPKG["pkg_breadth"];
						//					decimal decPkgHgth = (decimal)drEachPKG["pkg_height"];
						//Modified By Tom 10/6/09
						decimal decPkgLgth = Convert.ToDecimal(drEachPKG["pkg_length"].ToString());
						decimal decPkgBrdth = Convert.ToDecimal(drEachPKG["pkg_breadth"].ToString());
						decimal decPkgHgth = Convert.ToDecimal(drEachPKG["pkg_height"].ToString());
						//decimal decPkgHgth = Convert.ToDecimal((string)drEachPKG["pkg_height"]);
						//End modified By Tom 10/6/09
						decimal decPkgWt = (decimal) drEachPKG["pkg_wt"];
						int iPkgQty = Convert.ToInt32(drEachPKG["pkg_qty"]);
						
						strBuild = new StringBuilder();	

						strBuild.Append("insert into ImportedConsignmentsPkgs (applicationid,enterpriseid,userid, recordid,consignment_no,mps_code,pkg_length,pkg_breadth,pkg_height,pkg_wt,pkg_qty)values('");
						strBuild.Append(strAppID+"','");
						strBuild.Append(strEnterpriseID+"','");
						strBuild.Append(userID+"'");
						strBuild.Append("," + i+1 + ",'");
						strBuild.Append(strConsignmentNo);
						strBuild.Append("','");
						strBuild.Append(strMpsNo+"',");
						strBuild.Append(decPkgLgth+",");
						strBuild.Append(decPkgBrdth+",");
						strBuild.Append(decPkgHgth+",");
						strBuild.Append(decPkgWt+",");
						strBuild.Append(iPkgQty+")");
			
						dbCmd.CommandText = strBuild.ToString();
						iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);

						Logger.LogDebugInfo("module1","DomesticShipmentMgrDAL","AddDomesticShipment","INF004",iRowsAffected + " rows inserted into shipment_pkg table");
					}

				}
				
				transactionApp.Commit();
				Logger.LogDebugInfo("module1","DomesticShipmentMgrDAL","AddDomesticShipment","INF009","App db insert transaction committed.");
				
				result = 0;
				string str_vas = DataSetVasToString(iBooking_no.ToString(),strConsignmentNo,dsVas);
				DataSet dsResultSave = ImportConsignmentsDAL.ImportConsignments_Save(strAppID, strEnterpriseID, userID, importInsertUpdate, str_vas);

				if(dsResultSave.Tables.Count == 4)
					if(dsResultSave.Tables[3].Rows.Count > 0)
					{
						result = (int)dsResultSave.Tables[3].Rows[0][0];
					}

				if(result != 0)
				{
					string errorMsgSave = "";
					if(result == -1)
						errorMsgSave = "@userid parameter may not be NULL or blank.";
					else if(result == -2)
						errorMsgSave = "@userid is not a valid user ID for enterprise: PNGAF.";
					else if(result == -3)
						errorMsgSave = "@enterpriseid parameter is missing or invalid.";
					else if(result > 0)
						errorMsgSave = "SQL error message.";

					Logger.LogTraceError("module1","DomesticShipmentMgrDAL","AddDomesticShipment_ImportConsignments_Save","","");
					throw new ApplicationException(errorMsgSave,null);
				}
			}
			catch(ApplicationException appException)
			{
				try
				{
					transactionApp.Rollback();
					Logger.LogDebugInfo("module1","DomesticShipmentMgrDAL","AddDomesticShipment","INF010","App db insert transaction rolled back.");

				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("module1","DomesticShipmentMgrDAL","AddDomesticShipment","ERR008","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}


				Logger.LogTraceError("module1","DomesticShipmentMgrDAL","AddDomesticShipment","ERR009","Insert Transaction Failed : "+ appException.Message.ToString());
				throw new ApplicationException("1Error adding Shipment: " + appException.Message,appException);
			}
			catch(Exception exception)
			{
				try
				{
					transactionApp.Rollback();
					Logger.LogDebugInfo("module1","DomesticShipmentMgrDAL","AddDomesticShipment","INF011","App db insert transaction rolled back.");

				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("module1","DomesticShipmentMgrDAL","AddDomesticShipment","ERR010","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}

				Logger.LogTraceError("module1","DomesticShipmentMgrDAL","AddDomesticShipment","ERR011","Insert Transaction Failed : "+ exception.Message.ToString());
				//
				//				throw new ApplicationException("2Error adding Shipment",exception);
				throw new ApplicationException("2Error adding Shipment",exception);
			}
			finally
			{
				if(conApp != null)
				{
					conApp.Close();
				}
			}
			return iBooking_no;
		}
		
		/// <summary>
		/// This method gets record from pickup_request table.
		/// </summary>
		/// <param name="strAppID">Application ID</param>
		/// <param name="strEnterpriseID">Enterprise ID</param>
		/// <param name="iBookingNo">Booking number</param>
		/// <returns>Returns dataset</returns>
		public static DataSet GetFromPickUp(String strAppID,String strEnterpriseID,int iBookingNo)
		{
			String strQry = null;
			IDbCommand dbCmd = null;
			DataSet dsDisplayShipmnt = null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("DomesticShipmentManager","GetFromPickUp","ERR001","DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null",null);
			}
			strQry = "select * from pickup_request where applicationid = ";
			strQry = strQry + "'"+strAppID+"'";
			strQry = strQry + " and EnterpriseID = "+"'"+strEnterpriseID+"'";
			strQry = strQry + " and booking_no = "+iBookingNo;

			dbCmd = dbCon.CreateCommand(strQry,CommandType.Text);
			try
			{
				dsDisplayShipmnt = (DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
			}
			catch(System.ApplicationException appException)
			{
				Logger.LogTraceError("DomesticShipmentManager","GetFromPickUp","ERR002","Error ");
				throw appException;
			}
			return dsDisplayShipmnt;
		}
		public static DataSet GetFromQueryInsertShipment(String strAppID,String strEnterpriseID,int iBookingNo, String conNo, string userID)
		{
			DataSet dsDisplayShipmnt = null;
			try
			{
				DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
				System.Collections.ArrayList storedParams = new System.Collections.ArrayList();
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@enterpriseid",strEnterpriseID));
				if(iBookingNo > 0)
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@booking_no",iBookingNo));
				else
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@booking_no",DBNull.Value));
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@consignment_no",conNo));
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@userid",userID));
				dsDisplayShipmnt =(DataSet)dbCon.ExecuteProcedure ("QueryInsertShipment",storedParams,common.DAL.ReturnType.DataSetType);
			}
			catch(System.ApplicationException appException)
			{
				Logger.LogTraceError("DomesticShipmentManager","GetFromQueryInsertShipment","ERR002","Error ");
				throw appException;
			}
			return dsDisplayShipmnt;
		}
		//added on 17/01/2003
		public static DateTime PickupBkgDateTimeForPOD(String strAppID, String strEnterpriseID, int strBookNo)
		{	
			IDbCommand dbCmd = null;
			DataSet dsDisplayShipmnt = null;
			
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("DomesticShipmentManager","GetFromPickUp","ERR001","DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null",null);
			}
			StringBuilder strBuilder = new StringBuilder();
			strBuilder.Append("SELECT min(tracking_DateTime) AS tracking_datetime from Shipment_tracking where applicationId = '"+strAppID+"' and enterpriseId = '"+strEnterpriseID+"' and booking_no = "+strBookNo +" and status_code='POD' and (Deleted <> 'Y' or deleted is null)");
			//strBuilder.Append("and deleted not in ('y','Y') ");
			String strSQLQuery = strBuilder.ToString();

			//
			dbCmd = dbCon.CreateCommand(strSQLQuery,CommandType.Text);
			try
			{
				dsDisplayShipmnt = (DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
				DataRow dr = dsDisplayShipmnt.Tables[0].Rows[0];
				DateTime dt = new DateTime();	
				if(dr["tracking_datetime"]!=System.DBNull.Value)
				{
					dt = (DateTime)dr["tracking_datetime"];
					return dt;
				}				
				return dt;	
			}
			catch(System.ApplicationException appException)
			{
				Logger.LogTraceError("DomesticShipmentManager","GetFromPickUp","ERR002","Error ");
				throw appException;
			}
			//
		}
		//
		
		/// <summary>
		/// This method returns an empty dataset for the shipment package detail.
		/// The column mps_no is defined as primary key in the dataset.
		/// </summary>
		/// <returns>Dataset</returns>
		public static DataSet GetEmptyPkgDtlDS()
		{
			DataTable dtPkgDtl = new DataTable("PackageDetails");
			dtPkgDtl.Columns.Add(new DataColumn("mps_code", typeof(String)));
			//Modified by Tom 10/6/09
			dtPkgDtl.Columns.Add(new DataColumn("pkg_length",typeof(string)));
			dtPkgDtl.Columns.Add(new DataColumn("pkg_breadth",typeof(string)));
			dtPkgDtl.Columns.Add(new DataColumn("pkg_height",typeof(string)));
			dtPkgDtl.Columns.Add(new DataColumn("pkg_volume",typeof(decimal)));
			//End modified by Tom 10/6/09
			dtPkgDtl.Columns.Add(new DataColumn("pkg_wt",typeof(decimal)));
			dtPkgDtl.Columns.Add(new DataColumn("pkg_qty",typeof(int)));
			dtPkgDtl.Columns.Add(new DataColumn("tot_wt",typeof(decimal)));
			dtPkgDtl.Columns.Add(new DataColumn("tot_act_wt",typeof(decimal)));//TU on 17June08
			dtPkgDtl.Columns.Add(new DataColumn("tot_dim_wt",typeof(decimal)));
			dtPkgDtl.Columns.Add(new DataColumn("chargeable_wt",typeof(decimal)));
			dtPkgDtl.Columns.Add(new DataColumn("pkg_TOTVolume",typeof(decimal)));    //Jeab  8 Dec 10
			DataSet dsPkgDetails = new DataSet();
			
			DataColumn[] PrimKey = new DataColumn[1];
			PrimKey[0] = dtPkgDtl.Columns["mps_code"];
			dtPkgDtl.PrimaryKey = PrimKey;

			dsPkgDetails.Tables.Add(dtPkgDtl);

			return  dsPkgDetails;
		}
		//Jeab 8 Dec 2010	
		public static DataSet GetEmptyCustDimByTOTDS()
		{
			DataTable dtCustDimByTOT = new DataTable("Customer");
			dtCustDimByTOT.Columns.Add(new DataColumn("dim_by_tot", typeof(String))); 
			DataSet dsCustDimByTOT = new DataSet();
			
			DataColumn[] PrimKey = new DataColumn[1];
			PrimKey[0] = dtCustDimByTOT.Columns["dim_by_tot"];  
			dtCustDimByTOT.PrimaryKey = PrimKey;

			dsCustDimByTOT.Tables.Add(dtCustDimByTOT);

			return  dsCustDimByTOT;
		}
		//Jeab 8 Dec 2010	=========> End

		/// <summary>
		/// This method adds a row in the dataset returned by the GetEmptyPkgDtlDS() method.
		/// </summary>
		/// <param name="sessionDS">Dataset in which the row is to be added</param>
		public static void AddNewRowInPkgDS(DataSet sessionDS)
		{
			DataRow drNew = sessionDS.Tables[0].NewRow();
			//Modified By Tom Jan 19, 10
			
			//Modified By Tom Feb 4,2010
			drNew[0] = "";
			//Modified By Tom Feb 4,2010
			drNew[1] = 1;
			drNew[2] = 1;
			drNew[3] = 1;
			drNew[4] = 1;
			drNew[5] = 1;
			drNew[6] = 1;
			drNew[7] = 1;
			drNew[8] = 1;
			//End Modified By Tom Jan 19, 10
			sessionDS.Tables[0].Rows.Add(drNew);

		}

		/// <summary>
		/// The method returns an empty dataset for the domestic shipment VAS details.
		/// </summary>
		/// <returns>Dataset</returns>
		public static DataSet GetEmptyVASDS()
		{
			DataTable dtVAS = new DataTable("VAS");
			dtVAS.Columns.Add(new DataColumn("vas_code", typeof(string)));
			dtVAS.Columns.Add(new DataColumn("vas_description",typeof(string)));
			dtVAS.Columns.Add(new DataColumn("surcharge",typeof(decimal)));
			dtVAS.Columns.Add(new DataColumn("remarks",typeof(string)));
			DataSet dsVAS = new DataSet();
			dsVAS.Tables.Add(dtVAS);

			dsVAS.Tables["VAS"].Columns["vas_code"].Unique = true;
			dsVAS.Tables["VAS"].Columns["vas_code"].AllowDBNull = false;
			
			return  dsVAS;
		}
		/// <summary>
		/// This method adds row in the dataset returned by the GetEmptyVASDS() method.
		/// </summary>
		/// <param name="dsVAS">Dataset returned by the GetEmptyVASDS() method</param>
		public static void AddNewRowInVAS(DataSet dsVAS)
		{
			DataRow drNew = dsVAS.Tables[0].NewRow();

			drNew[0] = "";
			drNew[1] = "";
			drNew[2] = 0;
			try
			{
				dsVAS.Tables[0].Rows.Add(drNew);
			}
			catch(Exception ex)
			{
				System.Console.Write(ex.Message);
			}

		}

		/// <summary>
		/// This method checks whether there is a record in the pickup_shipment.
		/// </summary>
		/// <param name="strAppID">Application ID</param>
		/// <param name="strEnterpriseID">Enterprise ID</param>
		/// <param name="iBookingNo">Booking number</param>
		/// <returns>returns boolean true if record exists in pickup_shipment else false</returns>
		public static bool IsDomesticShpExist(String strAppID, String strEnterpriseID,int iBookingNo)
		{
			IDbCommand dbCmd = null;
			StringBuilder strBuild = null;
			bool IscanCreate = false;

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("DomesticShipmentManager","IsDomesticShpExist","ERR001","DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null",null);
			}

			strBuild =  new StringBuilder();
			strBuild.Append("select * from shipment where applicationid = '");
			strBuild.Append(strAppID+"'");
			strBuild.Append(" and enterpriseid = '");
			strBuild.Append(strEnterpriseID+"'");
			strBuild.Append(" and booking_no = ");
			strBuild.Append(iBookingNo);
			//	strBuild.Append(" and consignment_no IS NULL " );
			
			try
			{
				dbCmd = dbCon.CreateCommand(strBuild.ToString(),CommandType.Text);
				DataSet dsShipmnt =(DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
				if (dsShipmnt.Tables[0].Rows.Count > 0)
				{
					IscanCreate = true;
				}

			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("DomesticShipmentMgrDAL","IsDomesticShpCreated","ERR002","Error : "+ appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ",appException);
			}
			catch(Exception exception)
			{
				Logger.LogTraceError("DomesticShipmentMgrDAL","IsDomesticShpCreated","ERR003","Error : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}
			return IscanCreate;
		}
		public static bool IsDomesticShpCreate(String strAppID, String strEnterpriseID,int iBookingNo)
		{
			IDbCommand dbCmd = null;
			StringBuilder strBuild = null;
			bool IscanCreate = false;

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("DomesticShipmentManager","IsDomesticShpCreated","ERR001","DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null",null);
			}

			strBuild =  new StringBuilder();
			strBuild.Append("select * from pickup_shipment where applicationid = '");
			strBuild.Append(strAppID+"'");
			strBuild.Append(" and enterpriseid = '");
			strBuild.Append(strEnterpriseID+"'");
			strBuild.Append(" and booking_no = ");
			strBuild.Append(iBookingNo);
			strBuild.Append(" and consignment_no IS NULL " );
			
			try
			{
				dbCmd = dbCon.CreateCommand(strBuild.ToString(),CommandType.Text);
				DataSet dsShipmnt =(DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
				if (dsShipmnt.Tables[0].Rows.Count > 0)
				{
					IscanCreate = true;
				}

			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("DomesticShipmentMgrDAL","IsDomesticShpCreated","ERR002","Error : "+ appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ",appException);
			}
			catch(Exception exception)
			{
				Logger.LogTraceError("DomesticShipmentMgrDAL","IsDomesticShpCreated","ERR003","Error : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}
			return IscanCreate;
		}
		public static bool IsPkg_detailsForCash(String strAppID, String strEnterpriseID,int iBookingNo)
		{
			IDbCommand dbCmd = null;
			StringBuilder strBuild = null;
			bool IscanCreate = false;

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("DomesticShipmentManager","IsPkg_detailsForCash","ERR001","DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null",null);
			}

			strBuild =  new StringBuilder();
			strBuild.Append("select * from pickup_pkg where applicationid = '");
			strBuild.Append(strAppID+"'");
			strBuild.Append(" and enterpriseid = '");
			strBuild.Append(strEnterpriseID+"'");
			strBuild.Append(" and booking_no = ");
			strBuild.Append(iBookingNo);
			
			try
			{
				dbCmd = dbCon.CreateCommand(strBuild.ToString(),CommandType.Text);
				DataSet dsShipmnt =(DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
				if (dsShipmnt.Tables[0].Rows.Count > 0)
				{
					IscanCreate = true;
				}

			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("DomesticShipmentMgrDAL","IsDomesticShpCreated","ERR002","Error : "+ appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ",appException);
			}
			catch(Exception exception)
			{
				Logger.LogTraceError("DomesticShipmentMgrDAL","IsDomesticShpCreated","ERR003","Error : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}
			return IscanCreate;
		}
		/// <summary>
		/// This method checks whether the consignment already exists in shipment table.
		/// returns true if it already exists else false.
		/// </summary>
		/// <param name="strAppID">Application ID</param>
		/// <param name="strEnterpriseID">Enterprise ID</param>
		/// <param name="consgmtNo">Consignment Number</param>
		/// <returns>boolean</returns>
		public static bool IsConsgmentExist(String strAppID, String strEnterpriseID,String consgmtNo)
		{
			IDbCommand dbCmd = null;
			StringBuilder strBuild = null;
			bool isExists = false;

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("DomesticShipmentManager","IsConsgmtExist","ERR001","DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null",null);
			}

			strBuild =  new StringBuilder();
			strBuild.Append("select * from shipment where applicationid = '");
			strBuild.Append(strAppID+"'");
			strBuild.Append(" and enterpriseid = '");
			strBuild.Append(strEnterpriseID+"'");
			strBuild.Append(" and consignment_no = '");
			strBuild.Append(Utility.ReplaceSingleQuote(consgmtNo)+"'");
			
			try
			{
				dbCmd = dbCon.CreateCommand(strBuild.ToString(),CommandType.Text);
				DataSet dsShipmnt =(DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
				if (dsShipmnt.Tables[0].Rows.Count > 0)
				{
					isExists = true;
				}

			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("DomesticShipmentMgrDAL","IsConsgmtExist","ERR002","Error : "+ appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ",appException);
			}
			catch(Exception exception)
			{
				Logger.LogTraceError("DomesticShipmentMgrDAL","IsConsgmtExist","ERR003","Error : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}
			return isExists;
		}


		public static bool IsConsgmentPurged(String strAppID, String strEnterpriseID,String consgmtNo)
		{
			IDbCommand dbCmd = null;
			StringBuilder strBuild = null;
			bool isExists = false;

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("DomesticShipmentManager","IsConsgmtExist","ERR001","DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null",null);
			}

			strBuild =  new StringBuilder();
			strBuild.Append("select * from Purged_Shipment where applicationid = '");
			strBuild.Append(strAppID+"'");
			strBuild.Append(" and enterpriseid = '");
			strBuild.Append(strEnterpriseID+"'");
			strBuild.Append(" and consignment_no = '");
			strBuild.Append(Utility.ReplaceSingleQuote(consgmtNo)+"'");
			
			try
			{
				dbCmd = dbCon.CreateCommand(strBuild.ToString(),CommandType.Text);
				DataSet dsShipmnt =(DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
				if (dsShipmnt.Tables[0].Rows.Count > 0)
				{
					isExists = true;
				}

			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("DomesticShipmentMgrDAL","IsConsgmtExist","ERR002","Error : "+ appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ",appException);
			}
			catch(Exception exception)
			{
				Logger.LogTraceError("DomesticShipmentMgrDAL","IsConsgmtExist","ERR003","Error : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}
			return isExists;
		}

		public static int getPkgfromConNo(String strAppID, String strEnterpriseID,String consgmtNo)
		{
			IDbCommand dbCmd = null;
			StringBuilder strBuild = null;
			int pkgno = 1;

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("DomesticShipmentManager","IsConsgmtExist","ERR001","DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null",null);
			}

			strBuild =  new StringBuilder();
			strBuild.Append("select tot_pkg from shipment where applicationid = '");
			strBuild.Append(strAppID+"'");
			strBuild.Append(" and enterpriseid = '");
			strBuild.Append(strEnterpriseID+"'");
			strBuild.Append(" and consignment_no = '");
			strBuild.Append(Utility.ReplaceSingleQuote(consgmtNo)+"'");
			
			try
			{
				dbCmd = dbCon.CreateCommand(strBuild.ToString(),CommandType.Text);
				DataSet dsShipmnt =(DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
				if (dsShipmnt.Tables[0].Rows.Count > 0)
				{
					pkgno =(int)dsShipmnt.Tables[0].Rows[0]["tot_pkg"];
				}

			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("DomesticShipmentMgrDAL","IsConsgmtExist","ERR002","Error : "+ appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ",appException);
			}
			catch(Exception exception)
			{
				Logger.LogTraceError("DomesticShipmentMgrDAL","IsConsgmtExist","ERR003","Error : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}
			return pkgno;
		}
		public static int getPkgfromConNo_ConsignDetail(String strAppID, String strEnterpriseID,String consgmtNo)
		{
			IDbCommand dbCmd = null;
			StringBuilder strBuild = null;
			int pkgno = 1;

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("DomesticShipmentManager","IsConsgmtExist","ERR001","DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null",null);
			}

			strBuild =  new StringBuilder();

			strBuild.Append("select isnull(number_of_package,tot_pkg) as number_of_package from Consignment_Details right join shipment on Shipment.consignment_no = Consignment_Details.consignment_no where ");
			strBuild.Append(" shipment.consignment_no = '");
			strBuild.Append(Utility.ReplaceSingleQuote(consgmtNo)+"'");
			
			try
			{
				dbCmd = dbCon.CreateCommand(strBuild.ToString(),CommandType.Text);
				DataSet dsShipmnt =(DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
				if (dsShipmnt.Tables[0].Rows.Count > 0)
				{
					pkgno =(int)dsShipmnt.Tables[0].Rows[0]["number_of_package"];
				}

			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("DomesticShipmentMgrDAL","IsConsgmtExist","ERR002","Error : "+ appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ",appException);
			}
			catch(Exception exception)
			{
				Logger.LogTraceError("DomesticShipmentMgrDAL","IsConsgmtExist","ERR003","Error : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}
			return pkgno;
		}
		public static int DeleteAndAutoDMD(String strAppID, String strEnterpriseID,DataSet dsDomesticShipment, String pathCode, 
			long bookingNo,String orig_station, String dest_station, String route_code,int ClickNoHoliday)
		{
			IDbCommand dbCmd = null;
			int iRowsAffected = 0;
			StringBuilder strBuild = null;

						
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("DomesticShipmentManager","DeleteAndAutoDMD","ERR001","DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null",null);
			}

			IDbConnection conApp = dbCon.GetConnection();
			if(conApp == null)
			{
				Logger.LogTraceError("DomesticShipmentMgrDAL","DeleteAndAutoDMD","ERR002","conApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			try
			{
				dbCon.OpenConnection(ref conApp);
			}
			catch(ApplicationException appException)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("DomesticShipmentMgrDAL","DeleteAndAutoDMD","ERR003","Error opening connection : "+ appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ",appException);
			}
			catch(Exception exception)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("DomesticShipmentMgrDAL","DeleteAndAutoDMD","ERR004","Error opening connection : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}
			IDbTransaction transactionApp = dbCon.BeginTransaction(conApp,IsolationLevel.ReadCommitted);

			if(transactionApp == null)
			{
				Logger.LogTraceError("DomesticShipmentMgrDAL","DeleteAndAutoDMD","ERR005","transactionApp is null");
				throw new ApplicationException("Failed Begining a App Transaction..",null);
			}

			try
			{
				dbCmd = dbCon.CreateCommand("", CommandType.Text);
				dbCmd.Connection = conApp;
				dbCmd.Transaction = transactionApp;

				DateTime delivery_date = new DateTime(1,1,1);
				DateTime delivery_manifest_datetime = new DateTime(1,1,1);
				DateTime pickup_date = new DateTime(1,1,1);

				String flight_vehicle_no = "";
				String strDelManifestDateTime = "";
				String local_path_code = "";
				String local_orig_station = "";
				String local_dest_station = "";
				String local_route_code = "";
				String linehaul_route = "";

				String consignment_no = "";
				String StateCode = "";
				long booking_no = 0;
				String service_code = "";

				com.ties.classes.DeliveryPath delpathAM = null;

				DataRow drEach = dsDomesticShipment.Tables[0].Rows[0];
				local_path_code = pathCode;
				local_orig_station = orig_station;
				local_dest_station = dest_station;
				local_route_code = route_code;
				consignment_no = dsDomesticShipment.Tables[0].Rows[0]["consignment_no"].ToString();
				StateCode = dsDomesticShipment.Tables[0].Rows[0]["destination_state_code"].ToString();
				booking_no = bookingNo;
				service_code = dsDomesticShipment.Tables[0].Rows[0]["service_code"].ToString();

				delivery_date = (DateTime)drEach["est_delivery_datetime"];
				delpathAM = new com.ties.classes.DeliveryPath();
				delpathAM.Populate(strAppID, strEnterpriseID, local_path_code,dbCon,dbCmd);
				flight_vehicle_no = delpathAM.FlightVehicleNo;

				//###### This code added for new manifesting request ######
				DateTime departure_date = new DateTime(1,1,1);

				String currentHub = "";
				currentHub = DistributionCenterDAL.GetHubStationOfAutoManifest(strAppID, strEnterpriseID,dbCon,dbCmd);

				int tranSitDay = 0;
				tranSitDay = getTranSitDayForManifest(strAppID, strEnterpriseID, service_code,dbCon,dbCmd);

				int dayIncrement = 0;
				if (tranSitDay != 0)
				{
					//					if (local_orig_station == currentHub || local_dest_station == currentHub || local_orig_station==local_dest_station) // This mean only 1 linehaul
					//					{
					//						if (tranSitDay == 1)
					//							dayIncrement = 1;
					//						else if (tranSitDay == 2)
					//							dayIncrement = -1;
					//						else if (tranSitDay == 3)
					//							dayIncrement = -2;

					departure_date = DateTime.Parse(drEach["act_pickup_datetime"].ToString());//delivery_date;
					bool isHoliday=false;
					isHoliday = TIESUtility.IsDayHoliday(strAppID,strEnterpriseID,departure_date.AddDays(1),dbCon,dbCmd);

					if(ClickNoHoliday>0&&isHoliday)
						dayIncrement = ClickNoHoliday+1;
					else
						dayIncrement = 1;
					departure_date = departure_date.AddDays(dayIncrement); 
					//					}
					//					else // This mean only 2 linehaul *departure date must be today
					//					{
					//						if (tranSitDay == 1)
					//							dayIncrement = -1;
					//						else if (tranSitDay == 2)
					//							dayIncrement = -2;
					//						else if (tranSitDay == 3)
					//							dayIncrement = -3;

					//						departure_date = DateTime.Parse(drEach["act_pickup_datetime"].ToString());//delivery_date;
					//						departure_date = departure_date.AddDays(1); 
					//
					//						if(departure_date.DayOfWeek  == System.DayOfWeek.Saturday)
					//						{
					//							departure_date = departure_date.AddDays(-1);
					//						}
					//							
					//						if(departure_date.DayOfWeek == System.DayOfWeek.Sunday) 
					//						{
					//							departure_date = departure_date.AddDays(-1);
					//						}
					//					}
				}
				//###### This code added for new manifesting request ######

				delivery_manifest_datetime = (DateTime)DeliveryManifestMgrDAL.GetExactDateTimeOfAutoManifest(strAppID, strEnterpriseID,
					local_path_code, flight_vehicle_no, departure_date,dbCon,dbCmd); //The old one --> delivery_date);

				dbCmd = dbCon.CreateCommand("",CommandType.Text);
				dbCmd.Connection = conApp;
				dbCmd.Transaction = transactionApp;

				if(DeliveryManifestMgrDAL.IsTruckLeaved(strAppID,strEnterpriseID, consignment_no,dbCon,dbCmd)==false)
				{ //Add by X OCT 01 09
					//delete from the Delivery_Manifest_Detail
					strBuild = new StringBuilder();
					strBuild.Append("delete from Delivery_Manifest_Detail where applicationid = '");
					strBuild.Append(strAppID+"' and enterpriseid = '");
					strBuild.Append(strEnterpriseID);
					strBuild.Append("' and consignment_no = '");
					strBuild.Append(consignment_no +"'");
				
					dbCmd = dbCon.CreateCommand(strBuild.ToString(),CommandType.Text);
					dbCmd.Connection = conApp;
					dbCmd.Transaction = transactionApp;
					iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
					Logger.LogDebugInfo("DomesticShipmentMgrDAL","DeleteAndAutoDMD","INF001",iRowsAffected + " rows deleted from Delivery_Manifest_Detail table");
				}
				//Code of setAutomaticManifest function
				if (delivery_manifest_datetime != new DateTime(1,1,1))
				{
					strDelManifestDateTime = Utility.DateFormat(strAppID,strEnterpriseID,delivery_manifest_datetime,DTFormat.DateLongTime);

					//Short Route
					strBuild = new StringBuilder();	
					strBuild.Append("INSERT INTO Delivery_Manifest_Detail(applicationid, enterpriseid, path_code, flight_vehicle_no, ");
					strBuild.Append("delivery_manifest_datetime, consignment_no, origin_state_code, destination_state_code, route_code) values('");
					strBuild.Append(strAppID+"','"+strEnterpriseID+"','"+local_path_code+"','"+flight_vehicle_no+"',"+strDelManifestDateTime+",'"+consignment_no+"'");

					strBuild.Append(",");
					if(local_orig_station.Trim() != "")
					{
						strBuild.Append("'");
						strBuild.Append(Utility.ReplaceSingleQuote(local_orig_station));
						strBuild.Append("'");
					}
					else
					{
						strBuild.Append("null");
					}
					strBuild.Append(",");

					if(local_dest_station.Trim() != "")
					{
						strBuild.Append("'");
						strBuild.Append(Utility.ReplaceSingleQuote(local_dest_station));
						strBuild.Append("'");
					}
					else
					{
						strBuild.Append("null");
					}
					strBuild.Append(",");

					if(local_route_code.Trim() != "")
					{
						strBuild.Append("'");
						strBuild.Append(Utility.ReplaceSingleQuote(local_route_code));
						strBuild.Append("'");
					}
					else
					{
						strBuild.Append("null");
					}
					strBuild.Append(")");
				
					dbCmd.CommandText = strBuild.ToString();
					iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
					Logger.LogDebugInfo("module1","DomesticShipmentMgrDAL","DeleteAndAutoDMD","INF004",iRowsAffected + " rows inserted into shipment_pkg table");
				
					strBuild = new StringBuilder();
					strBuild.Append("update shipment ");
					strBuild.Append("set shpt_manifest_datetime = " + Utility.DateFormat(strAppID, strEnterpriseID,System.DateTime.Now,DTFormat.DateTime) + ", ");
					if(DistributionCenterDAL.IsDChasSWB(strAppID,strEnterpriseID,local_orig_station,dbCon,dbCmd))
						strBuild.Append("delivery_manifested = 'N'");	
					else
						strBuild.Append("delivery_manifested = 'Y'");
					strBuild.Append(" where booking_no = " + booking_no);
					strBuild.Append(" and consignment_no = '" + consignment_no + "'");

					dbCmd.CommandText = strBuild.ToString();
					iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
					Logger.LogDebugInfo("module1","DomesticShipmentMgrDAL","DeleteAndAutoDMD","INF004",iRowsAffected + " rows inserted into shipment_pkg table");

					if(DeliveryManifestMgrDAL.IsTruckLeaved(strAppID,strEnterpriseID, consignment_no,dbCon,dbCmd))
					{
						transactionApp.Commit();
						return 1;
					}
					if(local_orig_station != local_dest_station)
					{
						linehaul_route = DeliveryManifestMgrDAL.GetLineHaulOfAutoManifest(strAppID, strEnterpriseID,
							local_orig_station, local_dest_station,dbCon,dbCmd);

						if(DistributionCenterDAL.IsDChasSWB(strAppID,strEnterpriseID,local_orig_station,dbCon,dbCmd))
							linehaul_route=""; 

						if(linehaul_route != "")
						{
							delivery_date = (DateTime)drEach["est_delivery_datetime"];
							
							delpathAM = null;
							delpathAM = new com.ties.classes.DeliveryPath();
							delpathAM.Populate(strAppID, strEnterpriseID, linehaul_route,dbCon,dbCmd);

							flight_vehicle_no = delpathAM.FlightVehicleNo;

							
							if (pickup_date == new DateTime(1,1,1))
							{
								pickup_date = Convert.ToDateTime(drEach["act_pickup_datetime"]);
									
							}
							else
							{
								pickup_date = (DateTime)pickup_date;
							}
			

							delivery_manifest_datetime = DeliveryManifestMgrDAL.GetExactDateTimeOfAutoManifest(strAppID, strEnterpriseID, 
								linehaul_route, flight_vehicle_no, pickup_date,dbCon,dbCmd);

							if (delivery_manifest_datetime != new DateTime(1,1,1))
							{
								strDelManifestDateTime =Utility.DateFormat(strAppID,strEnterpriseID,
									delivery_manifest_datetime, DTFormat.DateLongTime);

								//LH1
								strBuild = new StringBuilder();	
								strBuild.Append("INSERT INTO Delivery_Manifest_Detail(applicationid, enterpriseid, path_code, flight_vehicle_no, ");
								strBuild.Append("delivery_manifest_datetime, consignment_no, origin_state_code, destination_state_code, route_code) values('");
								strBuild.Append(strAppID+"','"+strEnterpriseID+"','"+linehaul_route+"','"+flight_vehicle_no+"',"+strDelManifestDateTime+",'"+consignment_no+"'");

								strBuild.Append(",");
								if(local_orig_station.Trim() != "")
								{
									strBuild.Append("'");
									strBuild.Append(Utility.ReplaceSingleQuote(local_orig_station));
									strBuild.Append("'");
								}
								else
								{
									strBuild.Append("null");
								}
								strBuild.Append(",");

								if(local_dest_station.Trim() != "")
								{
									strBuild.Append("'");
									strBuild.Append(Utility.ReplaceSingleQuote(local_dest_station));
									strBuild.Append("'");
								}
								else
								{
									strBuild.Append("null");
								}
								strBuild.Append(",");

								if(local_route_code.Trim() != "")
								{
									strBuild.Append("'");
									strBuild.Append(Utility.ReplaceSingleQuote(local_route_code));
									strBuild.Append("'");
								}
								else
								{
									strBuild.Append("null");
								}
								strBuild.Append(")");
				
								dbCmd.CommandText = strBuild.ToString();
								iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
								Logger.LogDebugInfo("module1","DomesticShipmentMgrDAL","DeleteAndAutoDMD","INF004",iRowsAffected + " rows inserted into shipment_pkg table");
							}
						}
						else //2 LineHauls
						{
							if(currentHub != "")

							{
								linehaul_route = DeliveryManifestMgrDAL.GetLineHaulOfAutoManifest(strAppID, strEnterpriseID,
									local_orig_station, currentHub,dbCon,dbCmd);

								if(DistributionCenterDAL.IsDChasSWB(strAppID,strEnterpriseID,local_orig_station,dbCon,dbCmd))
									linehaul_route = "";

								if(linehaul_route != "")
								{
									delivery_date = (DateTime)drEach["est_delivery_datetime"];

									delpathAM = null;
									delpathAM = new com.ties.classes.DeliveryPath();
									delpathAM.Populate(strAppID, strEnterpriseID, linehaul_route,dbCon,dbCmd);
									flight_vehicle_no = delpathAM.FlightVehicleNo;

							
									if (pickup_date == new DateTime(1,1,1))
									{
										pickup_date = Convert.ToDateTime(drEach["act_pickup_datetime"]);
									}
									else
									{
										pickup_date = (DateTime)pickup_date;
									}
			

									delivery_manifest_datetime = DeliveryManifestMgrDAL.GetExactDateTimeOfAutoManifest(strAppID, strEnterpriseID, 
										linehaul_route, flight_vehicle_no, pickup_date,dbCon,dbCmd);

									if (delivery_manifest_datetime != new DateTime(1,1,1))
									{
										strDelManifestDateTime =Utility.DateFormat(strAppID,strEnterpriseID,
											delivery_manifest_datetime, DTFormat.DateLongTime);

										//LH1
										strBuild = new StringBuilder();	
										strBuild.Append("INSERT INTO Delivery_Manifest_Detail(applicationid, enterpriseid, path_code, flight_vehicle_no, ");
										strBuild.Append("delivery_manifest_datetime, consignment_no, origin_state_code, destination_state_code, route_code) values('");
										strBuild.Append(strAppID+"','"+strEnterpriseID+"','"+linehaul_route+"','"+flight_vehicle_no+"',"+strDelManifestDateTime+",'"+consignment_no+"'");

										strBuild.Append(",");
										if(local_orig_station.Trim() != "")
										{
											strBuild.Append("'");
											strBuild.Append(Utility.ReplaceSingleQuote(local_orig_station));
											strBuild.Append("'");
										}
										else
										{
											strBuild.Append("null");
										}
										strBuild.Append(",");

										if(local_dest_station.Trim() != "")
										{
											strBuild.Append("'");
											strBuild.Append(Utility.ReplaceSingleQuote(local_dest_station));
											strBuild.Append("'");
										}
										else
										{
											strBuild.Append("null");
										}
										strBuild.Append(",");

										if(local_route_code.Trim() != "")
										{
											strBuild.Append("'");
											strBuild.Append(Utility.ReplaceSingleQuote(local_route_code));
											strBuild.Append("'");
										}
										else
										{
											strBuild.Append("null");
										}
										strBuild.Append(")");
				
										dbCmd.CommandText = strBuild.ToString();
										iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
										Logger.LogDebugInfo("module1","DomesticShipmentMgrDAL","DeleteAndAutoDMD","INF004",iRowsAffected + " rows inserted into shipment_pkg table");
									}

									pickup_date = pickupDateTimeIncrement(pickup_date, strAppID, strEnterpriseID, StateCode,dbCon,dbCmd);

									linehaul_route = DeliveryManifestMgrDAL.GetLineHaulOfAutoManifest(strAppID, strEnterpriseID,
										currentHub, local_dest_station,dbCon,dbCmd);

									if(DistributionCenterDAL.IsDChasSWB(strAppID,strEnterpriseID,currentHub,dbCon,dbCmd))
										linehaul_route = "";

									//Second Route
									if(linehaul_route != "")
									{
										delivery_date = (DateTime)drEach["est_delivery_datetime"];

										delpathAM = null;
										delpathAM = new com.ties.classes.DeliveryPath();
										delpathAM.Populate(strAppID, strEnterpriseID, linehaul_route,dbCon,dbCmd);
										flight_vehicle_no = delpathAM.FlightVehicleNo;

							
										if (pickup_date == new DateTime(1,1,1))
										{
											pickup_date = Convert.ToDateTime(drEach["act_pickup_datetime"]);
											//pickup_date = System.DateTime.ParseExact((String)drEach["act_pickup_datetime"],"dd/MM/yyyy HH:mm", null);
										}
										else
										{
											pickup_date = (DateTime)pickup_date;
										}
			

										delivery_manifest_datetime = DeliveryManifestMgrDAL.GetExactDateTimeOfAutoManifest(strAppID, strEnterpriseID, 
											linehaul_route, flight_vehicle_no, pickup_date,dbCon,dbCmd);


										if (delivery_manifest_datetime != new DateTime(1,1,1))
										{
											strDelManifestDateTime =Utility.DateFormat(strAppID,strEnterpriseID,
												delivery_manifest_datetime, DTFormat.DateLongTime);

											//11. Insert a new Delivery_Manifest_Detail record where the fields are assigned as follows:
											strBuild = new StringBuilder();	
											strBuild.Append("INSERT INTO Delivery_Manifest_Detail(applicationid, enterpriseid, path_code, flight_vehicle_no, ");
											strBuild.Append("delivery_manifest_datetime, consignment_no, origin_state_code, destination_state_code, route_code) values('");
											strBuild.Append(strAppID+"','"+strEnterpriseID+"','"+linehaul_route+"','"+flight_vehicle_no+"',"+strDelManifestDateTime+",'"+consignment_no+"'");

											strBuild.Append(",");
											if(local_orig_station.Trim() != "")
											{
												strBuild.Append("'");
												strBuild.Append(Utility.ReplaceSingleQuote(currentHub));
												strBuild.Append("'");
											}
											else
											{
												strBuild.Append("null");
											}
											strBuild.Append(",");

											if(local_dest_station.Trim() != "")
											{
												strBuild.Append("'");
												strBuild.Append(Utility.ReplaceSingleQuote(local_dest_station));
												strBuild.Append("'");
											}
											else
											{
												strBuild.Append("null");
											}
											strBuild.Append(",");

											if(local_route_code.Trim() != "")
											{
												strBuild.Append("'");
												strBuild.Append(Utility.ReplaceSingleQuote(local_route_code));
												strBuild.Append("'");
											}
											else
											{
												strBuild.Append("null");
											}
											strBuild.Append(")");
				
											dbCmd.CommandText = strBuild.ToString();
											iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
											Logger.LogDebugInfo("module1","DomesticShipmentMgrDAL","DeleteAndAutoDMD","INF004",iRowsAffected + " rows inserted into shipment_pkg table");
										}
									}
								}
							}
						}
					}
				}
				else
				{
					strBuild = new StringBuilder();
					strBuild.Append("update shipment ");
					strBuild.Append("set shpt_manifest_datetime = null, ");
					strBuild.Append("delivery_manifested = 'N'");
					strBuild.Append(" where booking_no = " + booking_no);
					strBuild.Append(" and consignment_no = '" + consignment_no + "'");

					dbCmd.CommandText = strBuild.ToString();
					iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
					Logger.LogDebugInfo("module1","DomesticShipmentMgrDAL","DeleteAndAutoDMD","INF004",iRowsAffected + " rows inserted into shipment_pkg table");
				}

				transactionApp.Commit();
				Logger.LogDebugInfo("module1","DomesticShipmentMgrDAL","DeleteAndAutoDMD","INF009","App db insert transaction committed.");
			
			}
			catch(ApplicationException appException)
			{
				try
				{
					transactionApp.Rollback();
					Logger.LogDebugInfo("module1","DomesticShipmentMgrDAL","DeleteAndAutoDMD","INF010","App db insert transaction rolled back.");

				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("module1","DomesticShipmentMgrDAL","DeleteAndAutoDMD","ERR008","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}


				Logger.LogTraceError("module1","DomesticShipmentMgrDAL","DeleteAndAutoDMD","ERR009","Insert Transaction Failed : "+ appException.Message.ToString());
				throw new ApplicationException("Record Inserted - Not Manifested.",appException);
			}
			catch(Exception exception)
			{
				try
				{
					transactionApp.Rollback();
					Logger.LogDebugInfo("module1","DomesticShipmentMgrDAL","DeleteAndAutoDMD","INF011","App db insert transaction rolled back.");

				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("module1","DomesticShipmentMgrDAL","DeleteAndAutoDMD","ERR010","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}

				Logger.LogTraceError("module1","DomesticShipmentMgrDAL","DeleteAndAutoDMD","ERR011","Insert Transaction Failed : "+ exception.Message.ToString());
				throw new ApplicationException("Record Inserted - Not Manifested.",exception);
			}
			finally
			{
				if(conApp != null)
				{
					conApp.Close();
				}
			}

			return iRowsAffected;

		}


		
		/// <summary>
		/// This method deletes records from shipment_pkg,shipment_vas tables & inserts into shipment_pkg,shipment_vas tables,
		/// updates shipment table.
		/// </summary>
		/// <param name="strAppID">Application ID</param>
		/// <param name="strEnterpriseID">Enterprise ID</param>
		/// <param name="dsDomesticShipment">Dataset contains domestic shipment data for updation in database</param>
		/// <param name="dsVAS">Dataset contains shipment VAS data for updation in database</param>
		/// <param name="dsPkg">Dataset contains shipment Package data for updation in database</param>
		/// <returns>int (the rows affected)</returns>
		public static int UpdateDomesticShp(String strAppID,String strEnterpriseID,DataSet dsDomesticShipment,DataSet dsVAS,DataSet dsPkg)
		{
			DataRow drEach = null;
		
			decimal d_total_rated_amount = 0;
			
			IDbCommand dbCmd = null;
			int iRowsAffected = 0;
			StringBuilder strBuild = null;
			int cnt = 0;
			int i = 0;
			String strPayerID = null;
			String strSenderZipCode = null;
			String strRecipientZipCode = null;
			String strServiceCode = null;
			
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("DomesticShipmentManager","UpdateDomesticShp","ERR001","DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null",null);
			}
			if (dsDomesticShipment == null)
			{
				Logger.LogTraceError("DomesticShipmentManager","UpdateDomesticShp","ERR002","DataSet is null!!");
				throw new ApplicationException("The Shipment DataSet is null",null);
			}
			
			IDbConnection conApp = dbCon.GetConnection();
			
			if(conApp == null)
			{
				Logger.LogTraceError("DomesticShipmentMgrDAL","UpdateDomesticShp","ERR003","conApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			try
			{
				dbCon.OpenConnection(ref conApp);
			}
			catch(ApplicationException appException)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("DomesticShipmentMgrDAL","UpdateDomesticShp","ERR004","Error opening connection : "+ appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ",appException);
			}
			catch(Exception exception)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("DomesticShipmentMgrDAL","UpdateDomesticShp","ERR005","Error opening connection : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}


			IDbTransaction transactionApp = dbCon.BeginTransaction(conApp,IsolationLevel.ReadCommitted);

			if(transactionApp == null)
			{
				Logger.LogTraceError("DomesticShipmentMgrDAL","UpdateDomesticShp","ERR006","transactionApp is null");
				throw new ApplicationException("Failed Begining a App Transaction..",null);
			}
			try
			{
				dbCmd = dbCon.CreateCommand("", CommandType.Text);
				dbCmd.Connection = conApp;
				dbCmd.Transaction = transactionApp;
				int iBooking_no = 0;
				//				String strConsgNo = null;


				//Get Shipment Before Commit shipment transaction
				CustomerCreditused ocustused = new CustomerCreditused();
		
				
				ocustused.consignment_no = Utility.ReplaceSingleQuote(dsDomesticShipment.Tables[0].Rows[0]["consignment_no"].ToString());;
				ocustused.cusid = dsDomesticShipment.Tables[0].Rows[0]["payerid"].ToString();
				ocustused.applicationid = strAppID;
				ocustused.enterpriseid = strEnterpriseID;
				ocustused.amount = d_total_rated_amount;
				ocustused.bookingNo = dsDomesticShipment.Tables[0].Rows[0]["booking_no"].ToString();
				
				
				//	ocustused.updated_by = "";
				ocustused.transcode = "CSU";
				ocustused.invoice_no = "";
				
				
				DataTable dt = ocustused.getinfoShipment();
				decimal  total_rated_amount = 0;
				if(dt.Rows.Count>0)
				{
					total_rated_amount = decimal.Parse(dt.Rows[0]["total_rated_amount"].ToString());
				}

				if(dsDomesticShipment.Tables[0].Rows.Count > 0)
				{
					drEach = dsDomesticShipment.Tables[0].Rows[0];
					iBooking_no = Convert.ToInt32(drEach["booking_no"]);
	
					//update the shipment the table
					strBuild = new StringBuilder();
					//					strBuild.Append("update shipment set consignment_no = ");
					//					if((drEach["consignment_no"]!= null) && (!drEach["consignment_no"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					//					{
					//						strConsgNo = Utility.ReplaceSingleQuote(drEach["consignment_no"].ToString());
					//						strBuild.Append("'");
					//						strBuild.Append(strConsgNo);
					//						strBuild.Append("'");
					//					}
					//					else
					//					{
					//						strBuild.Append("null");
					//					}
					//					strBuild.Append(",");
					//
					//					strBuild.Append("ref_no = ");

					strBuild.Append("update shipment set ref_no = ");
					if((drEach["ref_no"]!= null) && (!drEach["ref_no"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						String strRefNo = (String) drEach["ref_no"];
						strBuild.Append("'");
						strBuild.Append(Utility.ReplaceSingleQuote(strRefNo));
						strBuild.Append("'");
					}
					else
					{
						strBuild.Append("null");
					}
					strBuild.Append(",");

					strBuild.Append("booking_datetime = ");
					if((drEach["booking_datetime"]!= null) && (!drEach["booking_datetime"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						DateTime dtBookingDtTime = (DateTime)drEach["booking_datetime"];
						strBuild.Append(Utility.DateFormat(strAppID,strEnterpriseID,dtBookingDtTime,DTFormat.DateTime));
					}
					else
					{
						strBuild.Append("null");
					}
					strBuild.Append(",");
					strBuild.Append("payerid = ");
					if((drEach["payerid"]!= null) && (!drEach["payerid"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						strPayerID = (String) drEach["payerid"];
						strBuild.Append("'");
						strBuild.Append(Utility.ReplaceSingleQuote(strPayerID));
						strBuild.Append("'");
					}
					else
					{
						strBuild.Append("null");
					}
					strBuild.Append(",");
					strBuild.Append("payer_type = ");
					if((drEach["payer_type"]!= null) && (!drEach["payer_type"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						String strPayerType = (String) drEach["payer_type"];
						strBuild.Append("'");
						strBuild.Append(strPayerType);
						strBuild.Append("'");
					}
					else
					{
						strBuild.Append("null");
					}
					strBuild.Append(",");
					strBuild.Append("new_account = ");
					if((drEach["new_account"]!= null) && (!drEach["new_account"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						String strNewAccount = (String) drEach["new_account"];
						strBuild.Append("'");
						strBuild.Append(strNewAccount);
						strBuild.Append("'");
					}
					else
					{
						strBuild.Append("null");
					}
					strBuild.Append(",");
					strBuild.Append("payer_name = ");
					if((drEach["payer_name"]!= null) && (!drEach["payer_name"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						String strPayerName = (String) drEach["payer_name"];
						strBuild.Append("N'");
						strBuild.Append(Utility.ReplaceSingleQuote(strPayerName));
						strBuild.Append("'");
					}
					else
					{
						strBuild.Append("null");
					}
					strBuild.Append(",");
					strBuild.Append("payer_address1 = ");
					if((drEach["payer_address1"]!= null) && (!drEach["payer_address1"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						String strPayerAddress1 = (String) drEach["payer_address1"];
						strBuild.Append("N'");
						strBuild.Append(Utility.ReplaceSingleQuote(strPayerAddress1));
						strBuild.Append("'");
					}
					else
					{
						strBuild.Append("null");
					}
					strBuild.Append(",");
					strBuild.Append("payer_address2 = ");
					if((drEach["payer_address2"]!= null) && (!drEach["payer_address2"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						String strPayerAddress2 = (String) drEach["payer_address2"];
						strBuild.Append("N'");
						strBuild.Append(Utility.ReplaceSingleQuote(strPayerAddress2));
						strBuild.Append("'");
					}
					else
					{
						strBuild.Append("null");
					}
					strBuild.Append(",");
					strBuild.Append("payer_zipcode = " );
					if((drEach["payer_zipcode"]!= null) && (!drEach["payer_zipcode"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						String strPayerZipCode = (String) drEach["payer_zipcode"];
						strBuild.Append("'");
						strBuild.Append(Utility.ReplaceSingleQuote(strPayerZipCode));
						strBuild.Append("'");
					}
					else
					{
						strBuild.Append("null");
					}
					strBuild.Append(",");
					strBuild.Append("payer_country = ");
					if((drEach["payer_country"]!= null) && (!drEach["payer_country"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						String strPayerCountry = (String) drEach["payer_country"];
						strBuild.Append("'");
						strBuild.Append(strPayerCountry);
						strBuild.Append("'");
					}
					else
					{
						strBuild.Append("null");
					}
					strBuild.Append(",");
					strBuild.Append("payer_telephone = ");
					if((drEach["payer_telephone"]!= null) && (!drEach["payer_telephone"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						String strPayerTelephone = (String) drEach["payer_telephone"];
						strBuild.Append("'");
						strBuild.Append(Utility.ReplaceSingleQuote(strPayerTelephone));
						strBuild.Append("'");
					}
					else
					{
						strBuild.Append("null");
					}
					strBuild.Append(",");
					strBuild.Append("payment_mode = ");
					if((drEach["payment_mode"]!= null) && (!drEach["payment_mode"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						String strPaymentMode = (String) drEach["payment_mode"];
						strBuild.Append("'");
						strBuild.Append(strPaymentMode);
						strBuild.Append("'");
					}
					else
					{
						strBuild.Append("null");
					}
					strBuild.Append(",");
					strBuild.Append("sender_name = ");
					if((drEach["sender_name"]!= null) && (!drEach["sender_name"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						String strSenderName = (String) drEach["sender_name"];
						strBuild.Append("N'");
						strBuild.Append(Utility.ReplaceSingleQuote(strSenderName));
						strBuild.Append("'");
					}
					else
					{
						strBuild.Append("null");
					}
					strBuild.Append(",");
					strBuild.Append("sender_address1 = ");
					if((drEach["sender_address1"]!= null) && (!drEach["sender_address1"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						String strSenderAddress1 = (String) drEach["sender_address1"];
						strBuild.Append("N'");
						strBuild.Append(Utility.ReplaceSingleQuote(strSenderAddress1));
						strBuild.Append("'");
					}
					else
					{
						strBuild.Append("null");
					}
					strBuild.Append(",");
					strBuild.Append("sender_address2 = ");
					if((drEach["sender_address2"]!= null) && (!drEach["sender_address2"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						String strSenderAddress2 = (String) drEach["sender_address2"];
						strBuild.Append("N'");
						strBuild.Append(Utility.ReplaceSingleQuote(strSenderAddress2));
						strBuild.Append("'");
					}
					else
					{
						strBuild.Append("null");
					}
					strBuild.Append(",");
					strBuild.Append("sender_zipcode = ");
					if((drEach["sender_zipcode"]!= null) && (!drEach["sender_zipcode"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						strSenderZipCode = (String) drEach["sender_zipcode"];
						strBuild.Append("'");
						strBuild.Append(Utility.ReplaceSingleQuote(strSenderZipCode));
						strBuild.Append("'");
					}
					else
					{
						strBuild.Append("null");
					}
					strBuild.Append(",");
					strBuild.Append("sender_country = ");
					if((drEach["sender_country"]!= null) && (!drEach["sender_country"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						String strSenderCountry = (String) drEach["sender_country"];
						strBuild.Append("'");
						strBuild.Append(strSenderCountry);
						strBuild.Append("'");
					}
					else
					{
						strBuild.Append("null");
					}
					strBuild.Append(",");
					strBuild.Append("sender_telephone = ");
					if((drEach["sender_telephone"]!= null) && (!drEach["sender_telephone"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						String strSenderTelephone = (String) drEach["sender_telephone"];
						strBuild.Append("'");
						strBuild.Append(Utility.ReplaceSingleQuote(strSenderTelephone));
						strBuild.Append("'");
					}
					else
					{
						strBuild.Append("null");
					}
					strBuild.Append(",");
					strBuild.Append(" sender_fax = ");
					if((drEach["sender_fax"]!= null) && (!drEach["sender_fax"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						String strSenderFax = (String) drEach["sender_fax"];
						strBuild.Append("'");
						strBuild.Append(Utility.ReplaceSingleQuote(strSenderFax));
						strBuild.Append("'");
					}
					else
					{
						strBuild.Append("null");
					}
					strBuild.Append(",");
					strBuild.Append(" sender_contact_person = ");
					if((drEach["sender_contact_person"]!= null) && (!drEach["sender_contact_person"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						String strSenderContactPerson = (String) drEach["sender_contact_person"];
						strBuild.Append("N'");
						strBuild.Append(Utility.ReplaceSingleQuote(strSenderContactPerson));
						strBuild.Append("'");
					}
					else
					{
						strBuild.Append("null");
					}
					strBuild.Append(",");
					strBuild.Append("recipient_name = ");
					if((drEach["recipient_name"]!= null) && (!drEach["recipient_name"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						String strRecipientName = (String) drEach["recipient_name"];
						strBuild.Append("N'");
						strBuild.Append(Utility.ReplaceSingleQuote(strRecipientName));
						strBuild.Append("'");
					}
					else
					{
						strBuild.Append("null");
					}
					strBuild.Append(",");
					strBuild.Append("recipient_address1 = ");
					if((drEach["recipient_address1"]!= null) && (!drEach["recipient_address1"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						String strRecipientAddress1 = (String) drEach["recipient_address1"];
						strBuild.Append("N'");
						strBuild.Append(Utility.ReplaceSingleQuote(strRecipientAddress1));
						strBuild.Append("'");
					}
					else
					{
						strBuild.Append("null");
					}
					strBuild.Append(",");
					strBuild.Append("recipient_address2 = ");
					if((drEach["recipient_address2"]!= null) && (!drEach["recipient_address2"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						String strRecipientAddress2 = (String) drEach["recipient_address2"];
						strBuild.Append("N'");
						strBuild.Append(Utility.ReplaceSingleQuote(strRecipientAddress2));
						strBuild.Append("'");
					}
					else
					{
						strBuild.Append("null");
					}
					strBuild.Append(",");
					strBuild.Append("recipient_zipcode = ");
					if((drEach["recipient_zipcode"]!= null) && (!drEach["recipient_zipcode"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						strRecipientZipCode = (String) drEach["recipient_zipcode"];
						strBuild.Append("'");
						strBuild.Append(Utility.ReplaceSingleQuote(strRecipientZipCode));
						strBuild.Append("'");
					}
					else
					{
						strBuild.Append("null");
					}
					strBuild.Append(",");
					strBuild.Append("recipient_country = ");
					if((drEach["recipient_country"]!= null) && (!drEach["recipient_country"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						String strRecipientCountry = (String) drEach["recipient_country"];
						strBuild.Append("'");
						strBuild.Append(strRecipientCountry);
						strBuild.Append("'");
					}
					else
					{
						strBuild.Append("null");
					}
					strBuild.Append(",");
					strBuild.Append("tot_wt = ");
					if((drEach["tot_wt"]!= null) && (!drEach["tot_wt"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						decimal decTotWt = (decimal) drEach["tot_wt"];
						strBuild.Append(decTotWt);
					}
					else
					{
						strBuild.Append("null");
					}
					//TU on 17June08
					strBuild.Append(",");
					strBuild.Append("tot_act_wt = ");
					if((drEach["tot_act_wt"]!= null) && (!drEach["tot_act_wt"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						decimal decTot_act_Wt = (decimal) drEach["tot_act_wt"];
						strBuild.Append(decTot_act_Wt);
					}
					else
					{
						strBuild.Append("null");
					}
					//


					strBuild.Append(",");
					strBuild.Append("recipient_telephone = ");
					if((drEach["recipient_telephone"]!= null) && (!drEach["recipient_telephone"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						String strRecipientTelephone = (String) drEach["recipient_telephone"];
						strBuild.Append("'");
						strBuild.Append(Utility.ReplaceSingleQuote(strRecipientTelephone));
						strBuild.Append("'");
					}
					else
					{
						strBuild.Append("null");
					}
					strBuild.Append(",");
					
					strBuild.Append("recipient_fax = ");
					if((drEach["recipient_fax"]!= null) && (!drEach["recipient_fax"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						String strRecipientFax = (String) drEach["recipient_fax"];
						strBuild.Append("'");
						strBuild.Append(Utility.ReplaceSingleQuote(strRecipientFax));
						strBuild.Append("'");
					}
					else
					{
						strBuild.Append("null");
					}

					strBuild.Append(",");
					strBuild.Append("tot_pkg = ");
					if((drEach["tot_pkg"]!= null) && (!drEach["tot_pkg"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						int iTotPkg = Convert.ToInt32(drEach["tot_pkg"]);
						strBuild.Append(iTotPkg);
					}
					else
					{
						strBuild.Append("null");
					}
					strBuild.Append(",");
					strBuild.Append("tot_dim_wt =");
					if((drEach["tot_dim_wt"]!= null) && (!drEach["tot_dim_wt"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						decimal decTotDimWt = (decimal) drEach["tot_dim_wt"];
						strBuild.Append(decTotDimWt);
					}
					else
					{
						strBuild.Append("null");
					}
					strBuild.Append(",");
					
					strBuild.Append("service_code = ");
					if((drEach["service_code"]!= null) && (!drEach["service_code"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						strServiceCode = (String) drEach["service_code"];
						strBuild.Append("'");
						strBuild.Append(strServiceCode);
						strBuild.Append("'");
					}
					else
					{
						strBuild.Append("null");
					}
					strBuild.Append(",");
					strBuild.Append("recipient_contact_person = ");
					if((drEach["recipient_contact_person"]!= null) && (!drEach["recipient_contact_person"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						String strRecipientContactPerson = (String) drEach["recipient_contact_person"];
						strBuild.Append("N'");
						strBuild.Append(Utility.ReplaceSingleQuote(strRecipientContactPerson));
						strBuild.Append("'");
					}
					else
					{
						strBuild.Append("null");
					}
					strBuild.Append(",");
					strBuild.Append("declare_value = ");
					if((drEach["declare_value"]!= null) && (!drEach["declare_value"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						decimal decDeclareValue = (decimal) drEach["declare_value"];
						strBuild.Append(decDeclareValue);
					}
					else
					{
						strBuild.Append("null");
					}
					strBuild.Append(",");
					
					strBuild.Append("chargeable_wt = ");
					if((drEach["chargeable_wt"]!= null) && (!drEach["chargeable_wt"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						decimal decChargeableWt = (decimal) drEach["chargeable_wt"];
						strBuild.Append(decChargeableWt);
					}
					else
					{
						strBuild.Append("null");
					}

					strBuild.Append(",");
					strBuild.Append("insurance_surcharge = ");
					if((drEach["insurance_surcharge"]!= null) && (!drEach["insurance_surcharge"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						decimal decInsuranceSurcharge = (decimal) drEach["insurance_surcharge"];
						strBuild.Append(decInsuranceSurcharge);
					}
					else
					{
						strBuild.Append("null");
					}
					//max_insurance_cover
					strBuild.Append(",");
					strBuild.Append("max_insurance_cover = ");
					if((drEach["max_insurance_cover"]!= null) && (!drEach["max_insurance_cover"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						decimal decmax_insurance_cover = (decimal) drEach["max_insurance_cover"];
						strBuild.Append(decmax_insurance_cover);
					}
					else
					{
						strBuild.Append("null");
					}
					//
					//payer_fax
					strBuild.Append(",");
					strBuild.Append("payer_fax = ");
					if((drEach["payer_fax"]!= null) && (!drEach["payer_fax"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						String strpayer_fax = (String) drEach["payer_fax"];
						strBuild.Append("'");
						strBuild.Append(strpayer_fax);
						strBuild.Append("'");
					}
					else
					{
						strBuild.Append("null");
					}
					//

					strBuild.Append(",");

					strBuild.Append("act_pickup_datetime = ");
					if((drEach["act_pickup_datetime"]!= null) && (!drEach["act_pickup_datetime"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						DateTime dtActPickup = Convert.ToDateTime(drEach["act_pickup_datetime"]);
						strBuild.Append(Utility.DateFormat(strAppID,strEnterpriseID,dtActPickup,DTFormat.DateTime));
					}
					else
					{
						strBuild.Append("null");
					}
					strBuild.Append(",");
					strBuild.Append("commodity_code = ");
					if((drEach["commodity_code"]!= null) && (!drEach["commodity_code"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						String strCommodityCode = (String) drEach["commodity_code"];
						strBuild.Append("'");
						strBuild.Append(strCommodityCode);
						strBuild.Append("'");
					}
					else
					{
						strBuild.Append("null");
					}
					strBuild.Append(",");
					
					strBuild.Append("est_delivery_datetime = ");
					if((drEach["est_delivery_datetime"]!= null) && (!drEach["est_delivery_datetime"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						DateTime dtEstDeliveryDatetime = Convert.ToDateTime(drEach["est_delivery_datetime"]);
						strBuild.Append(Utility.DateFormat(strAppID,strEnterpriseID,dtEstDeliveryDatetime,DTFormat.DateTime));

					}
					else
					{
						strBuild.Append("null");
					}
					strBuild.Append(",");
					strBuild.Append("percent_dv_additional = ");
					if((drEach["percent_dv_additional"]!= null) && (!drEach["percent_dv_additional"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						decimal decPercentDvAdditional = (decimal) drEach["percent_dv_additional"];
						strBuild.Append(decPercentDvAdditional);
					}
					else
					{
						strBuild.Append("null");
					}
					strBuild.Append(",");
					strBuild.Append("act_delivery_date = ");
					if((drEach["act_delivery_date"]!= null) && (!drEach["act_delivery_date"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						DateTime dtActDeliveryDate = Convert.ToDateTime(drEach["act_delivery_date"]);
						strBuild.Append(Utility.DateFormat(strAppID,strEnterpriseID,dtActDeliveryDate,DTFormat.DateTime));
					}
					else
					{
						strBuild.Append("null");
					}
					strBuild.Append(",");
					strBuild.Append(" payment_type = ");
					if((drEach["payment_type"]!= null) && (!drEach["payment_type"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						String strPaymentType = (String) drEach["payment_type"];
						strBuild.Append("'");
						strBuild.Append(strPaymentType);
						strBuild.Append("'");
					}
					else
					{
						strBuild.Append("null");
					}
					strBuild.Append(",");
					
					strBuild.Append("return_pod_slip = ");
					if((drEach["return_pod_slip"]!= null) && (!drEach["return_pod_slip"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						String strReturnPODSlip = (String) drEach["return_pod_slip"];
						strBuild.Append("'");
						strBuild.Append(strReturnPODSlip);
						strBuild.Append("'");
					}
					else
					{
						strBuild.Append("null");
					}
					strBuild.Append(",");
					
					strBuild.Append("shipment_type = ");
					if((drEach["shipment_type"]!= null) && (!drEach["shipment_type"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						String strShipmentType = (String) drEach["shipment_type"];
						strBuild.Append("'");
						strBuild.Append(strShipmentType);
						strBuild.Append("'");
					}
					else
					{
						strBuild.Append("null");
					}
					strBuild.Append(",");
					
					strBuild.Append("origin_state_code = ");
					if((drEach["origin_state_code"]!= null) && (!drEach["origin_state_code"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						String strOriginStateCode = (String) drEach["origin_state_code"];
						strBuild.Append("'");
						strBuild.Append(strOriginStateCode);
						strBuild.Append("'");
					}
					else
					{
						strBuild.Append("null");
					}
					strBuild.Append(",");
					strBuild.Append(" destination_state_code = ");
					if((drEach["destination_state_code"]!= null) && (!drEach["destination_state_code"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						String strDestinationStateCode = (String) drEach["destination_state_code"];
						strBuild.Append("'");
						strBuild.Append(strDestinationStateCode);
						strBuild.Append("'");
					}
					else
					{
						strBuild.Append("null");
					}

					strBuild.Append(",");
					strBuild.Append(" tot_freight_charge = ");
					if((drEach["tot_freight_charge"]!= null) && (!drEach["tot_freight_charge"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						decimal decTotFreight = (decimal) drEach["tot_freight_charge"];
						strBuild.Append(decTotFreight);
					}
					else
					{
						strBuild.Append("null");
					}

					strBuild.Append(",");
					strBuild.Append(" tot_vas_surcharge = ");
					if((drEach["tot_vas_surcharge"]!= null) && (!drEach["tot_vas_surcharge"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						decimal decTotVASSurcharge = (decimal) drEach["tot_vas_surcharge"];
						strBuild.Append(decTotVASSurcharge);
					}
					else
					{
						strBuild.Append("null");
					}
					strBuild.Append(",");
					strBuild.Append(" last_status_code = ");
					if((drEach["last_status_code"]!= null) && (!drEach["last_status_code"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						String strLastStatusCode = (String) drEach["last_status_code"];
						strBuild.Append("'");
						strBuild.Append(strLastStatusCode);
						strBuild.Append("'");
					}
					else
					{
						strBuild.Append("null");
					}
					strBuild.Append(",");
					strBuild.Append(" last_exception_code = ");
					if((drEach["last_exception_code"]!= null) && (!drEach["last_exception_code"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						String strLastExceptionCode = (String) drEach["last_exception_code"];
						strBuild.Append("'");
						strBuild.Append(strLastExceptionCode);
						strBuild.Append("'");
					}
					else
					{
						strBuild.Append("null");
					}
					strBuild.Append(",");
					strBuild.Append("last_status_datetime = ");
					if((drEach["last_status_datetime"]!= null) && (!drEach["last_status_datetime"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						DateTime dtLastStatusDateTime = Convert.ToDateTime(drEach["last_status_datetime"]);
						strBuild.Append(Utility.DateFormat(strAppID,strEnterpriseID,dtLastStatusDateTime,DTFormat.DateTime));
					}
					else
					{
						strBuild.Append("null");
					}
					strBuild.Append(",");
					strBuild.Append(" shpt_manifest_datetime = ");
					if((drEach["shpt_manifest_datetime"]!= null) && (!drEach["shpt_manifest_datetime"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						DateTime dtShptManifestDateTime = Convert.ToDateTime(drEach["shpt_manifest_datetime"]);
						strBuild.Append(Utility.DateFormat(strAppID,strEnterpriseID,dtShptManifestDateTime,DTFormat.DateTime));
					}
					else
					{
						strBuild.Append("null");
					}
					strBuild.Append(",");
					strBuild.Append(" delivery_manifested = ");
					if((drEach["delivery_manifested"]!= null) && (!drEach["delivery_manifested"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						String strDeliveryManifested = (String) drEach["delivery_manifested"];
						strBuild.Append("'");
						strBuild.Append(strDeliveryManifested);
						strBuild.Append("'");
					}
					else
					{
						strBuild.Append("null");
					}
					strBuild.Append(",");
					
					strBuild.Append(" route_code = ");
					if((drEach["route_code"]!= null) && (!drEach["route_code"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						String strRouteCode = (String) drEach["route_code"];
						strBuild.Append("'");
						strBuild.Append(strRouteCode);
						strBuild.Append("'");
					}
					else
					{
						strBuild.Append("null");
					}
					strBuild.Append(",");

					strBuild.Append("quotation_no = ");
					if((drEach["quotation_no"]!= null) && (!drEach["quotation_no"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						String strQuotationNo = (String) drEach["quotation_no"];
						strBuild.Append("'");
						strBuild.Append(strQuotationNo);
						strBuild.Append("'");
					}
					else
					{
						strBuild.Append("null");
					}
					strBuild.Append(",");
					//
					strBuild.Append("remark = ");
					if((drEach["remark"]!= null) && (!drEach["remark"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						String strRemark = (String) drEach["remark"];
						strBuild.Append("N'");
						strBuild.Append(Utility.ReplaceSingleQuote(strRemark));
						strBuild.Append("'");
					}
					else
					{
						strBuild.Append("null");
					}
					strBuild.Append(",");
					//
					strBuild.Append(" Quotation_Version = ");
					if((drEach["Quotation_Version"]!= null) && (!drEach["Quotation_Version"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						int iQuotationVersion =Convert.ToInt32(drEach["Quotation_Version"]);
						strBuild.Append(iQuotationVersion);
					}
					else
					{
						strBuild.Append("null");
					}
					strBuild.Append(",");

					strBuild.Append(" cod_amount = ");
					if((drEach["cod_amount"]!= null) && (!drEach["cod_amount"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						decimal decCODAmount = (decimal) drEach["cod_amount"];
						strBuild.Append(decCODAmount);
					}
					else
					{
						strBuild.Append("0");
					}
					strBuild.Append(",");

					strBuild.Append(" origin_station = ");
					if((drEach["origin_station"]!= null) && (!drEach["origin_station"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						String strorigin_station = (String) drEach["origin_station"];
						strBuild.Append("N'");
						strBuild.Append(Utility.ReplaceSingleQuote(strorigin_station));
						strBuild.Append("'");
					}
					else
					{
						strBuild.Append("null");
					}
					strBuild.Append(",");

					strBuild.Append(" destination_station = ");
					if((drEach["destination_station"]!= null) && (!drEach["destination_station"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						String strdest_station = (String) drEach["destination_station"];
						strBuild.Append("N'");
						strBuild.Append(Utility.ReplaceSingleQuote(strdest_station));
						strBuild.Append("'");
					}
					else
					{
						strBuild.Append("null");
					}

					//HC Return Task
					strBuild.Append(",");
					strBuild.Append(" return_invoice_hc = ");
					if((drEach["return_invoice_hc"]!= null) && (!drEach["return_invoice_hc"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						String strreturn_invoice_hc = (String) drEach["return_invoice_hc"];
						strBuild.Append("N'");
						strBuild.Append(Utility.ReplaceSingleQuote(strreturn_invoice_hc));
						strBuild.Append("'");
					}
					else
					{
						strBuild.Append("null");
					}

					strBuild.Append(",");
					strBuild.Append(" est_hc_return_datetime = ");
					if((drEach["est_hc_return_datetime"]!= null) && (!drEach["est_hc_return_datetime"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						DateTime dtest_hc_return_datetime = Convert.ToDateTime(drEach["est_hc_return_datetime"]);
						strBuild.Append(Utility.DateFormat(strAppID,strEnterpriseID,dtest_hc_return_datetime,DTFormat.DateTime));
					}
					else
					{
						strBuild.Append("null");
					}

					strBuild.Append(",");
					strBuild.Append(" act_hc_return_datetime = ");
					if((drEach["act_hc_return_datetime"]!= null) && (!drEach["act_hc_return_datetime"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						DateTime dtact_hc_return_datetime = Convert.ToDateTime(drEach["act_hc_return_datetime"]);
						strBuild.Append(Utility.DateFormat(strAppID,strEnterpriseID,dtact_hc_return_datetime,DTFormat.DateTime));
					}
					else
					{
						strBuild.Append("null");
					}
					strBuild.Append(",");
					strBuild.Append(" esa_surcharge = ");
					if((drEach["esa_surcharge"]!= null) && (!drEach["esa_surcharge"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						decimal decESASurcharge = (decimal) drEach["esa_surcharge"];
						strBuild.Append(decESASurcharge);
					}
					else
					{
						strBuild.Append("null");
					}


					
					object  obj_tot_freight_charge = drEach["tot_freight_charge"], obj_insurance_surcharge = drEach["insurance_surcharge"],
						obj_other_surch_amount = drEach["other_surch_amount"], obj_tot_vas_surcharge = drEach["tot_vas_surcharge"],
						obj_esa_surcharge = drEach["esa_surcharge"];
					
					//other_surch_amount
					strBuild.Append(",");
					strBuild.Append(" other_surch_amount = ");
					if( obj_other_surch_amount!=DBNull.Value) strBuild.Append(DBNullToZero(obj_other_surch_amount));
					else strBuild.Append("null");


					//Calculate Total Amount (total_rated_amount)
					strBuild.Append(",");
					strBuild.Append(" total_rated_amount = ");
					if( obj_tot_freight_charge!=DBNull.Value && obj_insurance_surcharge!=DBNull.Value && 
						obj_other_surch_amount!=DBNull.Value && obj_tot_vas_surcharge!=DBNull.Value && 
						obj_esa_surcharge!=DBNull.Value)
					{
						
						decimal d_tot_freight_charge=DBNullToZero(obj_tot_freight_charge), d_insurance_surcharge=DBNullToZero(obj_insurance_surcharge),
							d_other_surch_amount=DBNullToZero(obj_other_surch_amount),d_tot_vas_surcharge=DBNullToZero(obj_tot_vas_surcharge),
							d_esa_surcharge=DBNullToZero(obj_esa_surcharge);
						d_total_rated_amount = d_tot_freight_charge + d_insurance_surcharge + d_other_surch_amount + 
							d_tot_vas_surcharge + d_esa_surcharge;
						strBuild.Append(d_total_rated_amount);
					}
					else
					{
						strBuild.Append("null");
					}

					//HC Return Task


					/////// by Tumz. /////////////////////
					if((drEach["pkg_detail_replaced_by"]!= null) && (!drEach["pkg_detail_replaced_by"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						String strPkg_detail_replaced_by = (String) drEach["pkg_detail_replaced_by"];
						DateTime dtPkg_detail_replaced_datetime = (DateTime)drEach["pkg_detail_replaced_datetime"];

						strBuild.Append(", pkg_detail_replaced_by = '");
						strBuild.Append(strPkg_detail_replaced_by);
						strBuild.Append("', pkg_detail_replaced_datetime ='");
						strBuild.Append(dtPkg_detail_replaced_datetime);
						strBuild.Append("' ");
					}


					strBuild.Append( " where applicationid = '");
					strBuild.Append(strAppID+"' and enterpriseid = '");
					strBuild.Append(strEnterpriseID);
					strBuild.Append("' and booking_no = ");
					strBuild.Append(iBooking_no);
					strBuild.Append(" and consignment_no = '");
					strBuild.Append(Utility.ReplaceSingleQuote(drEach["consignment_no"].ToString()));
					strBuild.Append("'");
					
					dbCmd = dbCon.CreateCommand(strBuild.ToString(),CommandType.Text);
					dbCmd.Connection = conApp;
					dbCmd.Transaction = transactionApp;

					iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
					Logger.LogDebugInfo("DomesticShipmentMgrDAL","UpdateDomesticShp","INF001",iRowsAffected + " rows updated in Shipment table");

					if(dsPkg != null)
					{
						//delete old pd fo insert new 
						strBuild = new StringBuilder();
						strBuild.Append("delete from Shipment_PKG_Original where applicationid = '");
						strBuild.Append(strAppID+"' and enterpriseid = '");
						strBuild.Append(strEnterpriseID);
						strBuild.Append("' and booking_no = ");
						strBuild.Append(iBooking_no);
						strBuild.Append(" and consignment_no = '");
						strBuild.Append(Utility.ReplaceSingleQuote(drEach["consignment_no"].ToString()));
						strBuild.Append("'");
						dbCmd.CommandText = strBuild.ToString();
						iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);

						//Select shipment_pkg & insert in to shipment_pkg_original
						SessionDS sessionDS_Select_S_PKG = new SessionDS();
						strBuild = new StringBuilder();
						strBuild.Append("insert into Shipment_PKG_Original select * from shipment_pkg where applicationid = '");
						strBuild.Append(strAppID+"' and enterpriseid = '");
						strBuild.Append(strEnterpriseID);
						if(iBooking_no != 0)
						{
							strBuild.Append("' and booking_no = ");
							strBuild.Append(iBooking_no);
						}
						else
							strBuild.Append("'");

						strBuild.Append(" and consignment_no = '");
						strBuild.Append(Utility.ReplaceSingleQuote(drEach["consignment_no"].ToString()));
						strBuild.Append("'");
						dbCmd.CommandText = strBuild.ToString();
						iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
						//						
						//						if(sessionDS_Select_S_PKG.ds.Tables[0].Rows.Count > 0)
						//						{
						//							
						//						}


						//delete from the shipment_pkg table
						strBuild = new StringBuilder();
						strBuild.Append("delete from shipment_pkg where applicationid = '");
						strBuild.Append(strAppID+"' and enterpriseid = '");
						strBuild.Append(strEnterpriseID);
						strBuild.Append("' and booking_no = ");
						strBuild.Append(iBooking_no);
						strBuild.Append(" and consignment_no = '");
						strBuild.Append(Utility.ReplaceSingleQuote(drEach["consignment_no"].ToString()));
						strBuild.Append("'");
						dbCmd.CommandText = strBuild.ToString();
						iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);

						strBuild = new StringBuilder();
						strBuild.Append("delete from shipment_pkg_box where applicationid = '");
						strBuild.Append(strAppID+"' and enterpriseid = '");
						strBuild.Append(strEnterpriseID);
						strBuild.Append("' and booking_no = ");
						strBuild.Append(iBooking_no);
						strBuild.Append(" and consignment_no = '");
						strBuild.Append(Utility.ReplaceSingleQuote(drEach["consignment_no"].ToString()));
						strBuild.Append("'");
						dbCmd.CommandText = strBuild.ToString();
						iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
						Logger.LogDebugInfo("DomesticShipmentMgrDAL","UpdateDomesticShp","INF002",iRowsAffected + " rows deleted from Shipment_pkg table");

						//insert into the shipment_pkg table
						cnt = dsPkg.Tables[0].Rows.Count;
						i = 0;
						Customer customer = new Customer();
						customer.Populate(strAppID,strEnterpriseID, strPayerID);
						Zipcode zipcode = new Zipcode();

						zipcode.Populate(strAppID,strEnterpriseID,strSenderZipCode);
						String strOrgZone = zipcode.ZoneCode;

						zipcode.Populate(strAppID,strEnterpriseID,strRecipientZipCode);
						String strDestnZone = zipcode.ZoneCode;
						int sumQty = 0;
						float weightToFreight = 0;
						decimal freightCharge = 0;

						for(i=0;i<cnt;i++)
						{
							DataRow drEachPKG = dsPkg.Tables[0].Rows[i];
							String strMpsNo = "";
							if(drEachPKG["mps_code"].ToString() != "")
							{
								strMpsNo = (String)drEachPKG["mps_code"];
							}

							decimal decPkgLgth = Convert.ToDecimal(drEachPKG["pkg_length"]);
							decimal decPkgBrdth = Convert.ToDecimal(drEachPKG["pkg_breadth"]);
							decimal decPkgHgth = Convert.ToDecimal(drEachPKG["pkg_height"]);
							decimal decPkgWt = Convert.ToDecimal(drEachPKG["pkg_wt"]);
							int iPkgQty = Convert.ToInt32(drEachPKG["pkg_qty"]);
							decimal decTotalWt = Convert.ToDecimal(drEachPKG["tot_wt"]);
							decimal decTotal_act_Wt = Convert.ToDecimal(drEachPKG["tot_act_wt"]);//TU on 17June08
							decimal decTotalDimWt = Convert.ToDecimal(drEachPKG["tot_dim_wt"]);
							decimal decChrgbleWt = Convert.ToDecimal(drEachPKG["chargeable_wt"]);

							if(float.Parse(drEachPKG["tot_wt"].ToString())>float.Parse(drEachPKG["tot_dim_wt"].ToString()))
							{
								if(customer.IsCustomer_Box== "Y")
									weightToFreight = float.Parse(drEachPKG["tot_wt"].ToString())/int.Parse(drEachPKG["pkg_qty"].ToString());
								else
									weightToFreight = float.Parse(drEachPKG["tot_wt"].ToString());

							}
							else
							{
								if(customer.IsCustomer_Box== "Y")
									weightToFreight = float.Parse(drEachPKG["tot_dim_wt"].ToString())/int.Parse(drEachPKG["pkg_qty"].ToString());
								else
									weightToFreight = float.Parse(drEachPKG["tot_dim_wt"].ToString());
							}
							sumQty = sumQty+int.Parse(drEachPKG["pkg_qty"].ToString());
							freightCharge = (decimal)TIESUtility.ComputeFreightCharge(strAppID, strEnterpriseID,
								strPayerID, "C", 
								strOrgZone, strDestnZone, weightToFreight,
								strServiceCode, strSenderZipCode, strRecipientZipCode);

							if(customer.IsCustomer_Box=="Y")
								freightCharge = (freightCharge * int.Parse(drEachPKG["pkg_qty"].ToString()));

							strBuild = new StringBuilder();		
							//TU on 17June08
							strBuild.Append("insert into shipment_pkg (applicationid,enterpriseid,booking_no,consignment_no,mps_code,pkg_length,pkg_breadth,pkg_height,pkg_wt,pkg_qty,tot_wt,tot_act_wt,tot_dim_wt,chargeable_wt)values('");
							strBuild.Append(strAppID+"','");
							strBuild.Append(strEnterpriseID+"',");
							strBuild.Append(iBooking_no);
							strBuild.Append(",'");
							strBuild.Append(Utility.ReplaceSingleQuote(drEach["consignment_no"].ToString()));
							strBuild.Append("','");
							strBuild.Append(strMpsNo+"',");
							strBuild.Append(decPkgLgth+",");
							strBuild.Append(decPkgBrdth+",");
							strBuild.Append(decPkgHgth+",");
							strBuild.Append(decPkgWt+",");
							strBuild.Append(iPkgQty+",");
							strBuild.Append(decTotalWt+",");
							strBuild.Append(decTotal_act_Wt+",");//TU on 17June08
							strBuild.Append(decTotalDimWt+",");
							strBuild.Append(decChrgbleWt+")");

				
							dbCmd.CommandText = strBuild.ToString();
							iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
							
							//Chai	18/04/2012
							/*
							strBuild = new StringBuilder();
							strBuild.Append("insert into shipment_pkg_box (applicationid,enterpriseid,booking_no,consignment_no,mps_code,pkg_length,pkg_breadth,pkg_height,pkg_wt,pkg_qty,tot_wt,tot_act_wt,tot_dim_wt,chargeable_wt,Freight_Charge)values('");
							strBuild.Append(strAppID+"','");
							strBuild.Append(strEnterpriseID+"',");
							strBuild.Append(iBooking_no); //New booking no
							strBuild.Append(",'");
							strBuild.Append(Utility.ReplaceSingleQuote(drEach["consignment_no"].ToString()));
							strBuild.Append("','");
							strBuild.Append(strMpsNo+"',");
							strBuild.Append(decPkgLgth+",");
							strBuild.Append(decPkgBrdth+",");
							strBuild.Append(decPkgHgth+",");
							strBuild.Append(decPkgWt+",");
							strBuild.Append(iPkgQty+",");
							strBuild.Append(decTotalWt+",");
							strBuild.Append(decTotal_act_Wt+",");//TU on 17June08
							strBuild.Append(decTotalDimWt+",");
							strBuild.Append(decChrgbleWt+",");
							strBuild.Append(freightCharge+")");
				
							dbCmd.CommandText = strBuild.ToString();
							iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
							*/
							Logger.LogDebugInfo("DomesticShipmentMgrDAL","AddDomesticShipment","INF003",iRowsAffected + " rows inserted into shipment_pkg table");
						}					
						if(sumQty < int.Parse(customer.Minimum_Box))
						{
							int packageAdd = int.Parse(customer.Minimum_Box)-sumQty;
							int mpsNo = sumQty+1;
							freightCharge = (decimal)TIESUtility.ComputeFreightCharge(strAppID, strEnterpriseID,
								strPayerID, "C", 
								strOrgZone, strDestnZone, 0,
								strServiceCode, strSenderZipCode, strRecipientZipCode);
							for(int k=0;k<packageAdd;k++)
							{
								//Chai	18/04/2012
								/*
								strBuild = new StringBuilder();
								strBuild.Append("insert into shipment_pkg_box (applicationid,enterpriseid,booking_no,consignment_no,mps_code,pkg_length,pkg_breadth,pkg_height,pkg_wt,pkg_qty,tot_wt,tot_act_wt,tot_dim_wt,chargeable_wt,Freight_Charge)values('");
								strBuild.Append(strAppID+"','");
								strBuild.Append(strEnterpriseID+"',");
								strBuild.Append(iBooking_no); //New booking no
								strBuild.Append(",'");
								strBuild.Append(Utility.ReplaceSingleQuote(drEach["consignment_no"].ToString()));
								strBuild.Append("','");
								strBuild.Append(mpsNo.ToString()+"',");
								strBuild.Append(0+",");
								strBuild.Append(0+",");
								strBuild.Append(0+",");
								strBuild.Append(0+",");
								strBuild.Append(0+",");
								strBuild.Append(0+",");
								strBuild.Append(0+",");//TU on 17June08
								strBuild.Append(0+",");
								strBuild.Append(0+",");
								strBuild.Append(freightCharge+")");

								dbCmd.CommandText = strBuild.ToString();
								iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
								*/
								mpsNo = mpsNo+1;
								Logger.LogDebugInfo("DomesticShipmentMgrDAL","AddDomesticShipment","INF003",iRowsAffected + " rows inserted into shipment_pkg_box table");
							}
						}
					}
					
					//delete from the shipment_vas table
					strBuild = new StringBuilder();
					strBuild.Append("delete from shipment_vas where applicationid = '");
					strBuild.Append(strAppID+"' and enterpriseid = '");
					strBuild.Append(strEnterpriseID);
					strBuild.Append("' and booking_no = ");
					strBuild.Append(iBooking_no);
					strBuild.Append(" and consignment_no = '");
					strBuild.Append(Utility.ReplaceSingleQuote(drEach["consignment_no"].ToString()));
					strBuild.Append("'");
				
					dbCmd.CommandText = strBuild.ToString();
					iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
					Logger.LogDebugInfo("DomesticShipmentMgrDAL","UpdateDomesticShp","INF004",iRowsAffected + " rows deleted from Shipment_VAS table");
					
					
					if(dsVAS.Tables[0].Rows.Count > 0)
					{
						//insert into the shipment_vas table
						cnt = dsVAS.Tables[0].Rows.Count;
						i = 0;

						for(i=0;i<cnt;i++)
						{
							DataRow drEachVAS = dsVAS.Tables[0].Rows[i];
			
							String strVASCode = "";
							if(drEachVAS["vas_code"].ToString() != "")
							{
								strVASCode = (String)drEachVAS["vas_code"];
							}
							decimal decSurcharge = (decimal)drEachVAS["surcharge"];
			
							String strRemarks = "";
							if(drEachVAS["remarks"].ToString() != "")
							{
								strRemarks = (String)drEachVAS["remarks"];
							}

							strBuild = new StringBuilder();			
							strBuild.Append("insert into shipment_vas (applicationid,enterpriseid,booking_no,consignment_no,vas_code,vas_surcharge,remark)values('");
							strBuild.Append(strAppID+"','");
							strBuild.Append(strEnterpriseID+"',");
							strBuild.Append(iBooking_no);
							strBuild.Append(",'");
							strBuild.Append(Utility.ReplaceSingleQuote(drEach["consignment_no"].ToString()));
							strBuild.Append("','");
							strBuild.Append(Utility.ReplaceSingleQuote(strVASCode)+"',");
							strBuild.Append(decSurcharge+",N'");
							strBuild.Append(Utility.ReplaceSingleQuote(strRemarks)+"')");

							dbCmd.CommandText = strBuild.ToString();
							iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
							Logger.LogDebugInfo("DomesticShipmentMgrDAL","UpdateDomesticShp","INF005",iRowsAffected + " rows inserted into shipment_vas table");
					
						}
					}
					/*if(dsVAS.Tables[0].Rows.Count > 0)
					{
						//delete from the shipment_vas table
						strBuild = new StringBuilder();
						strBuild.Append("delete from shipment_vas where applicationid = '");
						strBuild.Append(strAppID+"' and enterpriseid = '");
						strBuild.Append(strEnterpriseID);
						strBuild.Append("' and booking_no = ");
						strBuild.Append(iBooking_no);
						strBuild.Append(" and consignment_no = '");
						strBuild.Append(Utility.ReplaceSingleQuote(drEach["consignment_no"].ToString()));
						strBuild.Append("'");
					
						dbCmd.CommandText = strBuild.ToString();
						iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
						Logger.LogDebugInfo("DomesticShipmentMgrDAL","UpdateDomesticShp","INF004",iRowsAffected + " rows deleted from Shipment_VAS table");
					
					
					
						//insert into the shipment_vas table
						cnt = dsVAS.Tables[0].Rows.Count;
						i = 0;

						for(i=0;i<cnt;i++)
						{
							DataRow drEachVAS = dsVAS.Tables[0].Rows[i];
			
							String strVASCode = "";
							if(drEachVAS["vas_code"].ToString() != "")
							{
								strVASCode = (String)drEachVAS["vas_code"];
							}
							decimal decSurcharge = (decimal)drEachVAS["surcharge"];
			
							String strRemarks = "";
							if(drEachVAS["remarks"].ToString() != "")
							{
								strRemarks = (String)drEachVAS["remarks"];
							}

							strBuild = new StringBuilder();			
							strBuild.Append("insert into shipment_vas (applicationid,enterpriseid,booking_no,consignment_no,vas_code,vas_surcharge,remark)values('");
							strBuild.Append(strAppID+"','");
							strBuild.Append(strEnterpriseID+"',");
							strBuild.Append(iBooking_no);
							strBuild.Append(",'");
							strBuild.Append(Utility.ReplaceSingleQuote(drEach["consignment_no"].ToString()));
							strBuild.Append("','");
							strBuild.Append(Utility.ReplaceSingleQuote(strVASCode)+"',");
							strBuild.Append(decSurcharge+",N'");
							strBuild.Append(Utility.ReplaceSingleQuote(strRemarks)+"')");

							dbCmd.CommandText = strBuild.ToString();
							iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
							Logger.LogDebugInfo("DomesticShipmentMgrDAL","UpdateDomesticShp","INF005",iRowsAffected + " rows inserted into shipment_vas table");
					
						}
					}*/
					//check whether the consignment no. has changed & update the pickup_shipment table
					//					strBuild = new StringBuilder();
					//					strBuild.Append(" update pickup_shipment set consignment_no = ");
					//					strBuild.Append("'");
					//					strBuild.Append(strConsgNoCurr);
					//					strBuild.Append("'");
					//					strBuild.Append( " where applicationid = '");
					//					strBuild.Append(strAppID+"' and enterpriseid = '");
					//					strBuild.Append(strEnterpriseID);
					//					strBuild.Append("' and booking_no = ");
					//					strBuild.Append(iBooking_no);
					//					strBuild.Append(" and consignment_no = '");
					//					strBuild.Append(strConsgNoOrg);
					//					strBuild.Append("'");
					//					strBuild.Append(" and consignment_no != '");
					//					strBuild.Append(strConsgNoCurr);
					//					strBuild.Append("'");
					//
					//					dbCmd.CommandText = strBuild.ToString();
					//					iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
					//					Logger.LogDebugInfo("DomesticShipmentMgrDAL","UpdateDomesticShp","INF006",iRowsAffected + " rows updated in table pickup_shipment");
					//
					//
					//					strBuild = new StringBuilder();
					//					strBuild.Append("update shipment_tracking set tracking_datetime = ");
					//					if((drEach["shpt_manifest_datetime"]!= null) && (!drEach["shpt_manifest_datetime"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					//					{
					//						DateTime dtShptManifestDateTime = Convert.ToDateTime(drEach["shpt_manifest_datetime"]);
					//						strBuild.Append(Utility.DateFormat(strAppID,strEnterpriseID,dtShptManifestDateTime,DTFormat.DateTime));
					//					}
					//					else
					//					{
					//						strBuild.Append("null");
					//					}
					//					strBuild.Append(",");
					//					strBuild.Append("consignment_no = ");
					//					if((drEach["consignment_no"]!= null) && (!drEach["consignment_no"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					//					{
					//						String strConsgnmentNo = (String) drEach["consignment_no"];
					//						strBuild.Append("'");
					//						strBuild.Append(strConsgnmentNo);
					//						strBuild.Append("'");
					//					}
					//
					//				
					//					strBuild.Append( " where applicationid = '");
					//					strBuild.Append(strAppID+"' and enterpriseid = '");
					//					strBuild.Append(strEnterpriseID);
					//					strBuild.Append("' and booking_no = ");
					//					strBuild.Append(iBooking_no);
					//					strBuild.Append(" and consignment_no = ");
					//					if((drEach["consignment_no"]!= null) && (!drEach["consignment_no"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					//					{
					//						String strConsgNumber = (String)drEach["consignment_no",DataRowVersion.Original];
					//						strBuild.Append("'");
					//						strBuild.Append(strConsgNumber);
					//						strBuild.Append("'");
					//					}
					//					strBuild.Append(" and tracking_datetime = ");
					//					if((drEach["shpt_manifest_datetime"]!= null) && (!drEach["shpt_manifest_datetime"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					//					{
					//						DateTime dtShptManifestDateTime = Convert.ToDateTime(drEach["shpt_manifest_datetime",DataRowVersion.Original]);
					//
					//						strBuild.Append(Utility.DateFormat(strAppID,strEnterpriseID,dtShptManifestDateTime,DTFormat.DateTime));
					//					}
					//					else
					//					{
					//						strBuild.Append("null");
					//					}
					//
					//					dbCmd.CommandText = strBuild.ToString();
					//					iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
					//					Logger.LogDebugInfo("DomesticShipmentMgrDAL","UpdateDomesticShp","INF007",iRowsAffected + " rows inserted into shipment_tracking table");
				
				}

				transactionApp.Commit();
				if(total_rated_amount >0)
				{
					//decimal  total_rated_amount = decimal.Parse(dt.Rows[0]["total_rated_amount"].ToString());
					if(total_rated_amount != d_total_rated_amount)
					{
						if((total_rated_amount - d_total_rated_amount) !=0)
						{
							ocustused.amount = d_total_rated_amount-total_rated_amount;
							ocustused.updateCreditused();
						}
					}
				}
				Logger.LogDebugInfo("DomesticShipmentMgrDAL","UpdateDomesticShp","INF008","App db insert transaction committed.");

			}
			catch(ApplicationException appException)
			{
				try
				{
					transactionApp.Rollback();
					Logger.LogDebugInfo("DomesticShipmentMgrDAL","UpdateDomesticShp","INF009","App db insert transaction rolled back.");

				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("DomesticShipmentMgrDAL","UpdateDomesticShp","ERR007","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}


				Logger.LogTraceError("DomesticShipmentMgrDAL","UpdateDomesticShp","ERR008","Insert Transaction Failed : "+ appException.Message.ToString());
				throw new ApplicationException("Error adding Shipment : "+appException.Message,appException);
			}
			catch(Exception exception)
			{
				try
				{
					transactionApp.Rollback();
					Logger.LogDebugInfo("DomesticShipmentMgrDAL","UpdateDomesticShp","INF010","App db insert transaction rolled back.");

				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("DomesticShipmentMgrDAL","UpdateDomesticShp","ERR009","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}

				Logger.LogTraceError("DomesticShipmentMgrDAL","UpdateDomesticShp","ERR010","Insert Transaction Failed : "+ exception.Message.ToString());
				throw new ApplicationException("Error adding Shipment ",exception);
			}
			finally
			{
				if(conApp != null)
				{
					conApp.Close();
				}
			}
			return iRowsAffected;
		}



		public static int UpdateDomesticShp_Partial(String strAppID,String strEnterpriseID,DataSet dsDomesticShipment,DataSet dsVAS,DataSet dsPkg,string userID, long preShipBkNo)
		{
			
			IDbCommand dbCmd = null;
			int iRowsAffected = 0;
			StringBuilder strBuild = null;
			int cnt = 0;
			int i = 0;
			long iBooking_no = 0;
			String strPayerID = null;
			String strSenderZipCode = null;
			String strRecipientZipCode = null;
			String strServiceCode = null;
			DataTable dtCreditUsed = new DataTable();
			dtCreditUsed.Columns.Add("CustID");
			dtCreditUsed.Columns.Add("ConsignmentNo");
			dtCreditUsed.Columns.Add("CreditAmount");
			dtCreditUsed.AcceptChanges();
			
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("DomesticShipmentManager","UpdateDomesticShp","ERR001","DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null",null);
			}
			if (dsDomesticShipment == null)
			{
				Logger.LogTraceError("DomesticShipmentManager","UpdateDomesticShp","ERR002","DataSet is null!!");
				throw new ApplicationException("The Shipment DataSet is null",null);
			}
			
			IDbConnection conApp = dbCon.GetConnection();
			
			if(conApp == null)
			{
				Logger.LogTraceError("DomesticShipmentMgrDAL","UpdateDomesticShp","ERR003","conApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			try
			{
				dbCon.OpenConnection(ref conApp);
			}
			catch(ApplicationException appException)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("DomesticShipmentMgrDAL","UpdateDomesticShp","ERR004","Error opening connection : "+ appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ",appException);
			}
			catch(Exception exception)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("DomesticShipmentMgrDAL","UpdateDomesticShp","ERR005","Error opening connection : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}


			IDbTransaction transactionApp = dbCon.BeginTransaction(conApp,IsolationLevel.ReadCommitted);

			if(transactionApp == null)
			{
				Logger.LogTraceError("DomesticShipmentMgrDAL","UpdateDomesticShp","ERR006","transactionApp is null");
				throw new ApplicationException("Failed Begining a App Transaction..",null);
			}
			try
			{
				dbCmd = dbCon.CreateCommand("", CommandType.Text);
				dbCmd.Connection = conApp;
				dbCmd.Transaction = transactionApp;
				if(dsDomesticShipment.Tables[0].Rows.Count > 0)
				{
					DataRow drEach = dsDomesticShipment.Tables[0].Rows[0];

					string preConNo = Utility.ReplaceSingleQuote(dsDomesticShipment.Tables[0].Rows[0]["consignment_no"].ToString());
					iBooking_no = Convert.ToInt64(dsDomesticShipment.Tables[0].Rows[0]["booking_no"]);

					StringBuilder str = new StringBuilder();
					str.Append("select * ");
					str.Append(" from shipment  ");
					str.Append(" where applicationid = '");
					str.Append(strAppID);
					str.Append("' and enterpriseid = '");
					str.Append(strEnterpriseID);
					str.Append("' and consignment_no = '");
					str.Append(Utility.ReplaceSingleQuote(preConNo));
					str.Append("' and booking_no = " + iBooking_no);

					SessionDS sessionDS = dbCon.ExecuteQuery(str.ToString(),0, 0,"IsAutomanifest");

					if (iBooking_no != preShipBkNo)
					{
						strBuild = new StringBuilder();
						//						String strPayerID = null;
						
						StringBuilder strBuilder = new StringBuilder();
						strBuilder.Append ("insert into shipment(applicationid,enterpriseid,booking_no,");
						strBuilder.Append("consignment_no,ref_no,booking_datetime,payerid,payer_type,");
						strBuilder.Append("new_account,payer_name,payer_address1,payer_address2,payer_zipcode,");
						strBuilder.Append("payer_country,payer_telephone,payer_fax,");
						strBuilder.Append("payment_mode,sender_name,sender_address1,sender_address2,sender_zipcode,");
						strBuilder.Append("sender_country,sender_telephone,sender_fax,");
						strBuilder.Append("sender_contact_person,recipient_name,recipient_address1,recipient_address2,");
						strBuilder.Append("recipient_zipcode,recipient_country,tot_wt,tot_act_wt,recipient_telephone,tot_dim_wt,");//TU on 17June08
						strBuilder.Append("recipient_fax,tot_pkg,service_code,recipient_contact_person,declare_value,");
						strBuilder.Append("chargeable_wt,insurance_surcharge,max_insurance_cover,");
						strBuilder.Append("act_pickup_datetime,");
						strBuilder.Append("commodity_code,");
						strBuilder.Append("est_delivery_datetime,percent_dv_additional,");
						strBuilder.Append("act_delivery_date,");
						strBuilder.Append("payment_type,");
						strBuilder.Append("return_pod_slip,");
						strBuilder.Append("shipment_type,");
						strBuilder.Append("origin_state_code,");
						strBuilder.Append("invoice_no,destination_state_code,invoice_amt,tot_freight_charge,");
						strBuilder.Append("tot_vas_surcharge,debit_amt,credit_amt,cash_collected,last_status_code,");
						strBuilder.Append("last_exception_code,last_status_datetime,shpt_manifest_datetime,");
						strBuilder.Append("delivery_manifested,");
						strBuilder.Append("route_code,quotation_no,quotation_version,esa_surcharge,");
						//		strBuilder.Append("agent_pup_cost_consignment,agent_pup_cost_wt,agent_del_cost_consignment,agent_del_cost_wt,agent");
						strBuilder.Append("agent_pup_cost_consignment,agent_pup_cost_wt,agent_del_cost_consignment,agent_del_cost_wt,remark, mbg, cod_amount, ");
						strBuilder.Append("origin_station, destination_station, ");
						//HC Return Task
						strBuilder.Append("return_invoice_hc, est_hc_return_datetime, act_hc_return_datetime, other_surch_amount");
						strBuilder.Append(",total_rated_amount");
						//HC Return Task
						//Insert Other surcharge amount By Aoo 22/02/2008
						//strBuilder.Append("other_surch_amount");
						strBuilder.Append(")values('"+strAppID+"',"+"'"+strEnterpriseID+"',");
						if((drEach["booking_no"]!= null) && (!drEach["booking_no"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						{
							iBooking_no = Convert.ToInt64(drEach["booking_no"]);
							if(iBooking_no == 0)
								//generate the booking no for adhoc 
								iBooking_no = (long)Counter.GetNext(strAppID,strEnterpriseID,"booking_number",dbCon,dbCmd);
							//iBooking_State = true;//Phase2 - J02
						}
						else
						{
							//generate the booking no for adhoc 
							iBooking_no = (long)Counter.GetNext(strAppID,strEnterpriseID,"booking_number",dbCon,dbCmd);
						}
						strBuilder.Append(iBooking_no+",");
				
						if((drEach["consignment_no"]!= null) && (!drEach["consignment_no"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						{
							string strConsignmentNo = Utility.ReplaceSingleQuote(drEach["consignment_no"].ToString());
							strBuilder.Append("'");
							strBuilder.Append(strConsignmentNo);
							strBuilder.Append("'");
						}
						else
						{
							strBuilder.Append("null");
						}
						strBuilder.Append(",");


						if((drEach["ref_no"]!= null) && (!drEach["ref_no"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						{
							String strRefNo = (String) drEach["ref_no"];
							strBuilder.Append("'");
							strBuilder.Append(Utility.ReplaceSingleQuote(strRefNo));
							strBuilder.Append("'");
						}
						else
						{
							strBuilder.Append("null");
						}
						strBuilder.Append(",");


						if((drEach["booking_datetime"]!= null) && (!drEach["booking_datetime"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						{
							DateTime dtBookingDtTime = (DateTime) drEach["booking_datetime"];
							strBuilder.Append(Utility.DateFormat(strAppID,strEnterpriseID,dtBookingDtTime,DTFormat.DateTime));
						}
						else
						{
							strBuilder.Append("null");
						}
						strBuilder.Append(",");


						if((drEach["payerid"]!= null) && (!drEach["payerid"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						{
							strPayerID = (String) drEach["payerid"];
							strBuilder.Append("'");
							strBuilder.Append(Utility.ReplaceSingleQuote(strPayerID));
							strBuilder.Append("'");
						}
						else
						{
							strBuilder.Append("null");
						}
						strBuilder.Append(",");

						if((drEach["payer_type"]!= null) && (!drEach["payer_type"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						{
							String strPayerType = (String) drEach["payer_type"];
							strBuilder.Append("'");
							strBuilder.Append(strPayerType);
							strBuilder.Append("'");
						}
						else
						{
							strBuilder.Append("null");
						}
						strBuilder.Append(",");

						if((drEach["new_account"]!= null) && (!drEach["new_account"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						{
							String strNewAccount = (String) drEach["new_account"];
							strBuilder.Append("'");
							strBuilder.Append(strNewAccount);
							strBuilder.Append("'");
						}
						else
						{
							strBuilder.Append("null");
						}
						strBuilder.Append(",");

						if((drEach["payer_name"]!= null) && (!drEach["payer_name"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						{
							String strPayerName = (String) drEach["payer_name"];
							strBuilder.Append("N'");
							strBuilder.Append(Utility.ReplaceSingleQuote(strPayerName));
							strBuilder.Append("'");
						}
						else
						{
							strBuilder.Append("null");
						}
						strBuilder.Append(",");

						if((drEach["payer_address1"]!= null) && (!drEach["payer_address1"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						{
							String strPayerAddress1 = (String) drEach["payer_address1"];
							strBuilder.Append("N'");
							strBuilder.Append(Utility.ReplaceSingleQuote(strPayerAddress1));
							strBuilder.Append("'");
						}
						else
						{
							strBuilder.Append("null");
						}
						strBuilder.Append(",");

						if((drEach["payer_address2"]!= null) && (!drEach["payer_address2"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						{
							String strPayerAddress2 = (String) drEach["payer_address2"];
							strBuilder.Append("N'");
							strBuilder.Append(Utility.ReplaceSingleQuote(strPayerAddress2));
							strBuilder.Append("'");
						}
						else
						{
							strBuilder.Append("null");
						}
						strBuilder.Append(",");

						if((drEach["payer_zipcode"]!= null) && (!drEach["payer_zipcode"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						{
							String strPayerZipCode = (String) drEach["payer_zipcode"];
							strBuilder.Append("'");
							strBuilder.Append(Utility.ReplaceSingleQuote(strPayerZipCode));
							strBuilder.Append("'");
						}
						else
						{
							strBuilder.Append("null");
						}
						strBuilder.Append(",");
				

						if((drEach["payer_country"]!= null) && (!drEach["payer_country"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						{
							String strPayerCountry = (String) drEach["payer_country"];
							strBuilder.Append("'");
							strBuilder.Append(strPayerCountry);
							strBuilder.Append("'");
						}
						else
						{
							strBuilder.Append("null");
						}
						strBuilder.Append(",");

						if((drEach["payer_telephone"]!= null) && (!drEach["payer_telephone"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						{
							String strPayerTelephone = (String) drEach["payer_telephone"];
							strBuilder.Append("'");
							strBuilder.Append(Utility.ReplaceSingleQuote(strPayerTelephone));
							strBuilder.Append("'");
						}
						else
						{
							strBuilder.Append("null");
						}
						strBuilder.Append(",");

						if((drEach["payer_fax"]!= null) && (!drEach["payer_fax"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						{
							String strPayerFax = (String) drEach["payer_fax"];
							strBuilder.Append("'");
							strBuilder.Append(Utility.ReplaceSingleQuote(strPayerFax));
							strBuilder.Append("'");
						}
						else
						{
							strBuilder.Append("null");
						}
						strBuilder.Append(",");

						if((drEach["payment_mode"]!= null) && (!drEach["payment_mode"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						{
							String strPaymentMode = (String) drEach["payment_mode"];
							strBuilder.Append("'");
							strBuilder.Append(strPaymentMode);
							strBuilder.Append("'");
						}
						else
						{
							strBuilder.Append("null");
						}
						strBuilder.Append(",");

						if((drEach["sender_name"]!= null) && (!drEach["sender_name"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						{
							String strSenderName = (String) drEach["sender_name"];
							strBuilder.Append("N'");
							strBuilder.Append(Utility.ReplaceSingleQuote(strSenderName));
							strBuilder.Append("'");
						}
						else
						{
							strBuilder.Append("null");
						}
						strBuilder.Append(",");

						if((drEach["sender_address1"]!= null) && (!drEach["sender_address1"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						{
							String strSenderAddress1 = (String) drEach["sender_address1"];
							strBuilder.Append("N'");
							strBuilder.Append(Utility.ReplaceSingleQuote(strSenderAddress1));
							strBuilder.Append("'");
						}
						else
						{
							strBuilder.Append("null");
						}
						strBuilder.Append(",");

						if((drEach["sender_address2"]!= null) && (!drEach["sender_address2"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						{
							String strSenderAddress2 = (String) drEach["sender_address2"];
							strBuilder.Append("N'");
							strBuilder.Append(Utility.ReplaceSingleQuote(strSenderAddress2));
							strBuilder.Append("'");
						}
						else
						{
							strBuilder.Append("null");
						}
						strBuilder.Append(",");

						if((drEach["sender_zipcode"]!= null) && (!drEach["sender_zipcode"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						{
							strSenderZipCode = (String) drEach["sender_zipcode"];
							strBuilder.Append("'");
							strBuilder.Append(Utility.ReplaceSingleQuote(strSenderZipCode));
							strBuilder.Append("'");
						}
						else
						{
							strBuilder.Append("null");
						}
						strBuilder.Append(",");

						if((drEach["sender_country"]!= null) && (!drEach["sender_country"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						{
							String strSenderCountry = (String) drEach["sender_country"];
							strBuilder.Append("'");
							strBuilder.Append(strSenderCountry);
							strBuilder.Append("'");
						}
						else
						{
							strBuilder.Append("null");
						}
						strBuilder.Append(",");

						if((drEach["sender_telephone"]!= null) && (!drEach["sender_telephone"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						{
							String strSenderTelephone = (String) drEach["sender_telephone"];
							strBuilder.Append("'");
							strBuilder.Append(Utility.ReplaceSingleQuote(strSenderTelephone));
							strBuilder.Append("'");
						}
						else
						{
							strBuilder.Append("null");
						}
						strBuilder.Append(",");

						if((drEach["sender_fax"]!= null) && (!drEach["sender_fax"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						{
							String strSenderFax = (String) drEach["sender_fax"];
							strBuilder.Append("'");
							strBuilder.Append(Utility.ReplaceSingleQuote(strSenderFax));
							strBuilder.Append("'");
						}
						else
						{
							strBuilder.Append("null");
						}
						strBuilder.Append(",");

						if((drEach["sender_contact_person"]!= null) && (!drEach["sender_contact_person"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						{
							String strSenderContactPerson = (String) drEach["sender_contact_person"];
							strBuilder.Append("N'");
							strBuilder.Append(Utility.ReplaceSingleQuote(strSenderContactPerson));
							strBuilder.Append("'");
						}
						else
						{
							strBuilder.Append("null");
						}
						strBuilder.Append(",");

						if((drEach["recipient_name"]!= null) && (!drEach["recipient_name"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						{
							String strRecipientName = (String) drEach["recipient_name"];
							strBuilder.Append("N'");
							strBuilder.Append(Utility.ReplaceSingleQuote(strRecipientName));
							strBuilder.Append("'");
						}
						else
						{
							strBuilder.Append("null");
						}
						strBuilder.Append(",");

						if((drEach["recipient_address1"]!= null) && (!drEach["recipient_address1"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						{
							String strRecipientAddress1 = (String) drEach["recipient_address1"];
							strBuilder.Append("N'");
							strBuilder.Append(Utility.ReplaceSingleQuote(strRecipientAddress1));
							strBuilder.Append("'");
						}
						else
						{
							strBuilder.Append("null");
						}
						strBuilder.Append(",");

						if((drEach["recipient_address2"]!= null) && (!drEach["recipient_address2"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						{
							String strRecipientAddress2 = (String) drEach["recipient_address2"];
							strBuilder.Append("N'");
							strBuilder.Append(Utility.ReplaceSingleQuote(strRecipientAddress2));
							strBuilder.Append("'");
						}
						else
						{
							strBuilder.Append("null");
						}
						strBuilder.Append(",");


						if((drEach["recipient_zipcode"]!= null) && (!drEach["recipient_zipcode"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						{
							strRecipientZipCode = (String) drEach["recipient_zipcode"];
							strBuilder.Append("'");
							strBuilder.Append(Utility.ReplaceSingleQuote(strRecipientZipCode));
							strBuilder.Append("'");
						}
						else
						{
							strBuilder.Append("null");
						}
						strBuilder.Append(",");

						if((drEach["recipient_country"]!= null) && (!drEach["recipient_country"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						{
							String strRecipientCountry = (String) drEach["recipient_country"];
							strBuilder.Append("'");
							strBuilder.Append(strRecipientCountry);
							strBuilder.Append("'");
						}
						else
						{
							strBuilder.Append("null");
						}
						strBuilder.Append(",");

						if((drEach["tot_wt"]!= null) && (!drEach["tot_wt"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						{
							decimal decTotWt = (decimal) drEach["tot_wt"];
							strBuilder.Append(decTotWt);
						}
						else
						{
							strBuilder.Append("null");
						}
						//TU on 17June08
						strBuilder.Append(",");

						if((drEach["tot_act_wt"]!= null) && (!drEach["tot_act_wt"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						{
							decimal decTot_act_Wt = (decimal) drEach["tot_act_wt"];
							strBuilder.Append(decTot_act_Wt);
						}
						else
						{
							strBuilder.Append("null");
						}
						//
						strBuilder.Append(",");


						if((drEach["recipient_telephone"]!= null) && (!drEach["recipient_telephone"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						{
							String strRecipientTelephone = (String) drEach["recipient_telephone"];
							strBuilder.Append("'");
							strBuilder.Append(Utility.ReplaceSingleQuote(strRecipientTelephone));
							strBuilder.Append("'");
						}
						else
						{
							strBuilder.Append("null");
						}
						strBuilder.Append(",");


						if((drEach["tot_dim_wt"]!= null) && (!drEach["tot_dim_wt"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						{
							decimal decTotDimWt = (decimal) drEach["tot_dim_wt"];
							strBuilder.Append(decTotDimWt);
						}
						else
						{
							strBuilder.Append("null");
						}
						strBuilder.Append(",");

						if((drEach["recipient_fax"]!= null) && (!drEach["recipient_fax"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						{
							String strRecipientFax = (String) drEach["recipient_fax"];
							strBuilder.Append("'");
							strBuilder.Append(Utility.ReplaceSingleQuote(strRecipientFax));
							strBuilder.Append("'");
						}
						else
						{
							strBuilder.Append("null");
						}
						strBuilder.Append(",");

				
						if((drEach["tot_pkg"]!= null) && (!drEach["tot_pkg"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						{
							int iTotPkg = Convert.ToInt32(drEach["tot_pkg"]);
							strBuilder.Append(iTotPkg);
						}
						else
						{
							strBuilder.Append("null");
						}
						strBuilder.Append(",");



						if((drEach["service_code"]!= null) && (!drEach["service_code"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						{
							strServiceCode = (String) drEach["service_code"];
							strBuilder.Append("'");
							strBuilder.Append(strServiceCode);
							strBuilder.Append("'");
						}
						else
						{
							strBuilder.Append("null");
						}
						strBuilder.Append(",");


						if((drEach["recipient_contact_person"]!= null) && (!drEach["recipient_contact_person"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						{
							String strRecipientContactPerson = (String) drEach["recipient_contact_person"];
							strBuilder.Append("N'");
							strBuilder.Append(Utility.ReplaceSingleQuote(strRecipientContactPerson));
							strBuilder.Append("'");
						}
						else
						{
							strBuilder.Append("null");
						}
						strBuilder.Append(",");

						if((drEach["declare_value"]!= null) && (!drEach["declare_value"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						{
							decimal decDeclareValue = (decimal) drEach["declare_value"];
							strBuilder.Append(decDeclareValue);
						}
						else
						{
							strBuilder.Append("null");
						}
						strBuilder.Append(",");

						if((drEach["chargeable_wt"]!= null) && (!drEach["chargeable_wt"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						{
							decimal decChargeableWt = (decimal) drEach["chargeable_wt"];
							strBuilder.Append(decChargeableWt);
						}
						else
						{
							strBuilder.Append("null");
						}
						strBuilder.Append(",");

						if((drEach["insurance_surcharge"]!= null) && (!drEach["insurance_surcharge"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						{
							decimal decInsuranceSurcharge = (decimal) drEach["insurance_surcharge"];
							strBuilder.Append(decInsuranceSurcharge);
						}
						else
						{
							strBuilder.Append("null");
						}
						strBuilder.Append(",");

						if((drEach["max_insurance_cover"]!= null) && (!drEach["max_insurance_cover"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						{
							decimal decMaxInsuranceCover = (decimal) drEach["max_insurance_cover"];
							strBuilder.Append(decMaxInsuranceCover);
						}
						else
						{
							strBuilder.Append("null");
						}
						strBuilder.Append(",");

						if((drEach["act_pickup_datetime"]!= null) && (!drEach["act_pickup_datetime"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						{
							DateTime dtActPickup = Convert.ToDateTime(drEach["act_pickup_datetime"]);
							strBuilder.Append(Utility.DateFormat(strAppID,strEnterpriseID,dtActPickup,DTFormat.DateTime));
						}
						else
						{
							strBuilder.Append("null");
						}
						strBuilder.Append(",");

						if((drEach["commodity_code"]!= null) && (!drEach["commodity_code"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						{
							String strCommodityCode = (String) drEach["commodity_code"];
							strBuilder.Append("'");
							strBuilder.Append(strCommodityCode);
							strBuilder.Append("'");
						}
						else
						{
							strBuilder.Append("null");
						}
						strBuilder.Append(",");


						if((drEach["est_delivery_datetime"]!= null) && (!drEach["est_delivery_datetime"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						{
							DateTime dtEstDeliveryDatetime = (DateTime) drEach["est_delivery_datetime"];
							strBuilder.Append(Utility.DateFormat(strAppID,strEnterpriseID,dtEstDeliveryDatetime,DTFormat.DateTime));
						}
						else
						{
							strBuilder.Append("null");
						}
						strBuilder.Append(",");

						if((drEach["percent_dv_additional"]!= null) && (!drEach["percent_dv_additional"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						{
							decimal decPercentDvAdditional = (decimal) drEach["percent_dv_additional"];
							strBuilder.Append(decPercentDvAdditional);
						}
						else
						{
							strBuilder.Append("null");
						}
						strBuilder.Append(",");

						if((drEach["act_delivery_date"]!= null) && (!drEach["act_delivery_date"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						{
							DateTime dtActDeliveryDate = (DateTime) drEach["act_delivery_date"];
							strBuilder.Append(Utility.DateFormat(strAppID,strEnterpriseID,dtActDeliveryDate,DTFormat.DateTime));
						}
						else
						{
							strBuilder.Append("null");
						}
						strBuilder.Append(",");

						if((drEach["payment_type"]!= null) && (!drEach["payment_type"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						{
							String strPaymentType = (String) drEach["payment_type"];
							strBuilder.Append("'");
							strBuilder.Append(strPaymentType);
							strBuilder.Append("'");
						}
						else
						{
							strBuilder.Append("null");
						}
						strBuilder.Append(",");

				

						if((drEach["return_pod_slip"]!= null) && (!drEach["return_pod_slip"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						{
							String strReturnPODSlip = (String) drEach["return_pod_slip"];
							strBuilder.Append("'");
							strBuilder.Append(strReturnPODSlip);
							strBuilder.Append("'");
						}
						else
						{
							strBuilder.Append("null");
						}
						strBuilder.Append(",");

						if((drEach["shipment_type"]!= null) && (!drEach["shipment_type"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						{
							String strShipmentType = (String) drEach["shipment_type"];
							strBuilder.Append("'");
							strBuilder.Append(strShipmentType);
							strBuilder.Append("'");
						}
						else
						{
							strBuilder.Append("null");
						}
						strBuilder.Append(",");


						if((drEach["origin_state_code"]!= null) && (!drEach["origin_state_code"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						{
							String strOriginStateCode = (String) drEach["origin_state_code"];
							strBuilder.Append("'");
							strBuilder.Append(strOriginStateCode);
							strBuilder.Append("'");
						}
						else
						{
							strBuilder.Append("null");
						}
						strBuilder.Append(",");

						if((drEach["invoice_no"]!= null) && (!drEach["invoice_no"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						{
							String strInvoiceNo = (String) drEach["invoice_no"];
							strBuilder.Append("'");
							strBuilder.Append(strInvoiceNo);
							strBuilder.Append("'");
						}
						else
						{
							strBuilder.Append("null");
						}
						strBuilder.Append(",");

						if((drEach["destination_state_code"]!= null) && (!drEach["destination_state_code"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						{
							String strDestinationStateCode = (String) drEach["destination_state_code"];
							strBuilder.Append("'");
							strBuilder.Append(strDestinationStateCode);
							strBuilder.Append("'");
						}
						else
						{
							strBuilder.Append("null");
						}
						strBuilder.Append(",");

						if((drEach["invoice_amt"]!= null) && (!drEach["invoice_amt"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						{
							decimal decInvoiceAmt = (decimal) drEach["invoice_amt"];
					
							strBuilder.Append(decInvoiceAmt);
					
						}
						else
						{
							strBuilder.Append("null");
						}
						strBuilder.Append(",");

						if((drEach["tot_freight_charge"]!= null) && (!drEach["tot_freight_charge"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						{
							decimal decTotFreight = (decimal) drEach["tot_freight_charge"];
					
							strBuilder.Append(decTotFreight);
					
						}
						else
						{
							strBuilder.Append("null");
						}
						strBuilder.Append(",");

						if((drEach["tot_vas_surcharge"]!= null) && (!drEach["tot_vas_surcharge"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						{
							decimal decTotVASSurcharge = (decimal) drEach["tot_vas_surcharge"];
					
							strBuilder.Append(decTotVASSurcharge);
					
						}
						else
						{
							strBuilder.Append("null");
						}
						strBuilder.Append(",");

						if((drEach["debit_amt"]!= null) && (!drEach["debit_amt"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						{
							decimal decDebitAmt = (decimal) drEach["debit_amt"];
					
							strBuilder.Append(decDebitAmt);
					
						}
						else
						{
							strBuilder.Append("null");
						}
						strBuilder.Append(",");
			
						if((drEach["credit_amt"]!= null) && (!drEach["credit_amt"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						{
							decimal decCreditAmt = (decimal) drEach["credit_amt"];
					
							strBuilder.Append(decCreditAmt);
					
						}
						else
						{
							strBuilder.Append("null");
						}
						strBuilder.Append(",");

						if((drEach["cash_collected"]!= null) && (!drEach["cash_collected"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						{
							decimal decCashCollected = (decimal) drEach["cash_collected"];
							strBuilder.Append(decCashCollected);	
						}
						else
						{
							strBuilder.Append("null");
						}
						strBuilder.Append(",");

				
						if((drEach["last_status_code"]!= null) && (!drEach["last_status_code"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						{
							String strLastStatusCode = (String) drEach["last_status_code"];
							strBuilder.Append("'");
							strBuilder.Append(strLastStatusCode);	
							strBuilder.Append("'");
						}
						else
						{
							strBuilder.Append("null");
						}
						strBuilder.Append(",");

						if((drEach["last_exception_code"]!= null) && (!drEach["last_exception_code"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						{
							String strLastExceptionCode = (String) drEach["last_exception_code"];
							strBuilder.Append("'");
							strBuilder.Append(strLastExceptionCode);	
							strBuilder.Append("'");
						}
						else
						{
							strBuilder.Append("null");
						}
						strBuilder.Append(",");

				
						if((drEach["last_status_datetime"]!= null) && (!drEach["last_status_datetime"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						{
							DateTime dtLastStatusDateTime = (DateTime) drEach["last_status_datetime"];
							strBuilder.Append(Utility.DateFormat(strAppID,strEnterpriseID,dtLastStatusDateTime,DTFormat.DateTime));


						}
						else
						{
							strBuilder.Append("null");
						}
						strBuilder.Append(",");

						if((drEach["shpt_manifest_datetime"]!= null) && (!drEach["shpt_manifest_datetime"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						{
							DateTime dtShptManifestDateTime = (DateTime) drEach["shpt_manifest_datetime"];
							strBuilder.Append(Utility.DateFormat(strAppID,strEnterpriseID,dtShptManifestDateTime,DTFormat.DateTime));
						}
						else
						{
							strBuilder.Append("null");
						}
						strBuilder.Append(",");

						if((drEach["delivery_manifested"]!= null) && (!drEach["delivery_manifested"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						{
							String strDeliveryManifested = (String) drEach["delivery_manifested"];
							strBuilder.Append("'");
							strBuilder.Append(strDeliveryManifested);	
							strBuilder.Append("'");
						}
						else
						{
							strBuilder.Append("null");
						}
						strBuilder.Append(",");

						if((drEach["route_code"]!= null) && (!drEach["route_code"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						{
							String strRouteCode = (String) drEach["route_code"];
							strBuilder.Append("'");
							strBuilder.Append(strRouteCode);	
							strBuilder.Append("'");
						}
						else
						{
							strBuilder.Append("null");
						}
						strBuilder.Append(",");

						if((drEach["quotation_no"]!= null) && (!drEach["quotation_no"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						{
							String strQuotationNo = (String) drEach["quotation_no"];
							strBuilder.Append("'");
							strBuilder.Append(strQuotationNo);	
							strBuilder.Append("'");
						}
						else
						{
							strBuilder.Append("null");
						}
						strBuilder.Append(",");			

						if((drEach["quotation_version"]!= null) && (!drEach["quotation_version"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						{
							int iQuotationVersion = Convert.ToInt32(drEach["quotation_version"]);
							strBuilder.Append(iQuotationVersion);
						}
						else
						{
							strBuilder.Append("null");
						}
						strBuilder.Append(",");
					
						if((drEach["esa_surcharge"]!= null) && (!drEach["esa_surcharge"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						{
							decimal decESASurcharge = (decimal) drEach["esa_surcharge"];
							strBuilder.Append(decESASurcharge);
						}
						else
						{
							strBuilder.Append("null");
						}
						strBuilder.Append(",");

						//Calculate the Agent Cost.
						if((drEach["payer_type"]!= null) && (!drEach["payer_type"].GetType().Equals(System.Type.GetType("System.DBNull"))) && drEach["payer_type"].ToString().Equals("A"))
						{
					
							decimal decTotWt = 0;
							decimal decTot_act_Wt = 0;
							AgentConsgCost agentConsgSnd;
							agentConsgSnd.decCost = 0;
							agentConsgSnd.strAgentID = null;

							AgentWtCost agentWtSnd;
							agentWtSnd.decCost = 0;
							agentWtSnd.strAgentID = null;

							AgentConsgCost agentConsgRcp;
							agentConsgRcp.decCost = 0;
							agentConsgRcp.strAgentID = null;

							AgentWtCost agentWtRcp;
							agentWtRcp.decCost = 0;
							agentWtRcp.strAgentID = null;

							if((drEach["tot_wt"]!= null) && (!drEach["tot_wt"].GetType().Equals(System.Type.GetType("System.DBNull"))))
							{
								decTotWt = (decimal) drEach["tot_wt"];
							}
							//TU on 17June08
							if((drEach["tot_act_wt"]!= null) && (!drEach["tot_act_wt"].GetType().Equals(System.Type.GetType("System.DBNull"))))
							{
								decTot_act_Wt = (decimal) drEach["tot_act_wt"];
							}
							//
							if((drEach["sender_zipcode"]!= null) && (!drEach["sender_zipcode"].GetType().Equals(System.Type.GetType("System.DBNull"))))
							{
								strSenderZipCode = (String) drEach["sender_zipcode"];
								//Check whether the Sender Zipcode exists in Agent_Zipcode table.
								//				agentConsgSnd =  TIESUtility.GetAgentConsignmentCost(strAppID,strEnterpriseID,strSenderZipCode);
						
								//Agent pickup Cost Weight
								//				agentWtSnd = TIESUtility.GetAgentWtCost(strAppID,strEnterpriseID,strSenderZipCode,decTotWt);
							}

							if((drEach["recipient_zipcode"]!= null) && (!drEach["recipient_zipcode"].GetType().Equals(System.Type.GetType("System.DBNull"))))
							{
								strRecipientZipCode = (String) drEach["recipient_zipcode"];
								//decimal decDlvryCost = 0;
								//Check whether the Recipient Zipcode exists in Agent_Zipcode table.
								//				agentConsgRcp =  TIESUtility.GetAgentConsignmentCost(strAppID,strEnterpriseID,strRecipientZipCode);
						
								//						if(decDlvryCost > 0)
								//							strBuilder.Append(decDlvryCost);
								//						else
								//							strBuilder.Append("null");
								//						strBuilder.Append(",");
						
								//Agent Delivery Cost Weight
								//				//decimal decDlvryWt = 0;
								//				agentWtRcp = TIESUtility.GetAgentWtCost(strAppID,strEnterpriseID,strRecipientZipCode,decTotWt);

								//						if(decDlvryWt > 0)
								//							strBuilder.Append(decDlvryWt);
								//						else
								//							strBuilder.Append("null");
							}

							if((agentConsgSnd.strAgentID != null) && (agentConsgRcp.strAgentID != null) && (agentConsgSnd.strAgentID == agentConsgRcp.strAgentID))
							{
								//No charges
								strBuilder.Append("null");
								strBuilder.Append(",");
								strBuilder.Append("null");
								strBuilder.Append(",");
								strBuilder.Append("null");
								strBuilder.Append(",");
								strBuilder.Append("null");	
								strBuilder.Append(",");
							}
							else
							{

								if(agentConsgSnd.decCost > 0)
									strBuilder.Append(agentConsgSnd.decCost);
								else
									strBuilder.Append("null");
								strBuilder.Append(",");

								if(agentWtSnd.decCost > 0)
									strBuilder.Append(agentWtSnd.decCost);
								else
									strBuilder.Append("null");
								strBuilder.Append(",");



								if(agentConsgRcp.decCost > 0)
									strBuilder.Append(agentConsgRcp.decCost);
								else
									strBuilder.Append("null");
								strBuilder.Append(",");

								if(agentWtRcp.decCost > 0)
									strBuilder.Append(agentWtRcp.decCost);
								else
									strBuilder.Append("null");
								strBuilder.Append(",");

							}

						}
						else //case other (C,N,V,E,..) by Tumz 17.08.54
						{
							strBuilder.Append("null");
							strBuilder.Append(",");
							strBuilder.Append("null");
							strBuilder.Append(",");
							strBuilder.Append("null");
							strBuilder.Append(",");
							strBuilder.Append("null");
							strBuilder.Append(",");
						}
						//						else if((drEach["payer_type"]!= null) && (!drEach["payer_type"].GetType().Equals(System.Type.GetType("System.DBNull"))) && drEach["payer_type"].ToString().Equals("C"))
						//						{
						//							strBuilder.Append("null");
						//							strBuilder.Append(",");
						//							strBuilder.Append("null");
						//							strBuilder.Append(",");
						//							strBuilder.Append("null");
						//							strBuilder.Append(",");
						//							strBuilder.Append("null");
						//							strBuilder.Append(",");
						//						}
						//
						if((drEach["remark"]!= null) && (!drEach["remark"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						{
							strBuilder.Append("N'");
							String strRemark = (String) drEach["remark"];			
							strBuilder.Append(Utility.ReplaceSingleQuote(strRemark));
							strBuilder.Append("'");
						}
						else
						{
							strBuilder.Append("null");
						}
						strBuilder.Append(",");

						if((drEach["mbg"]!= null) && (!drEach["mbg"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						{
							String strmbg = (String) drEach["mbg"];
							strBuilder.Append("'");
							strBuilder.Append(strmbg);	
							strBuilder.Append("'");
						}
						else
						{
							strBuilder.Append("null");
						}
						strBuilder.Append(",");

						if((drEach["cod_amount"]!= null) && (!drEach["cod_amount"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						{
							decimal decCod_amount = (decimal) drEach["cod_amount"];
							strBuilder.Append(decCod_amount);
						}
						else
						{
							strBuilder.Append("0");
						}
						strBuilder.Append(",");

						if((drEach["origin_station"]!= null) && (!drEach["origin_station"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						{
							String strorigin_station = (String) drEach["origin_station"];
							strBuilder.Append("'");
							strBuilder.Append(strorigin_station);	
							strBuilder.Append("'");
						}
						else
						{
							strBuilder.Append("null");
						}
						strBuilder.Append(",");

						if((drEach["destination_station"]!= null) && (!drEach["destination_station"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						{
							String strdest_station = (String) drEach["destination_station"];
							strBuilder.Append("'");
							strBuilder.Append(strdest_station);	
							strBuilder.Append("'");
						}
						else
						{
							strBuilder.Append("null");
						}

						//HC Return Task
						strBuilder.Append(",");

						if((drEach["return_invoice_hc"]!= null) && (!drEach["return_invoice_hc"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						{
							String strreturn_invoice_hc = (String) drEach["return_invoice_hc"];
							strBuilder.Append("'");
							strBuilder.Append(strreturn_invoice_hc);	
							strBuilder.Append("'");
						}
						else
						{
							strBuilder.Append("null");
						}

						strBuilder.Append(",");

						if((drEach["est_hc_return_datetime"]!= null) && (!drEach["est_hc_return_datetime"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						{
							DateTime dtest_hc_return_datetime = (DateTime) drEach["est_hc_return_datetime"];
							strBuilder.Append(Utility.DateFormat(strAppID,strEnterpriseID,dtest_hc_return_datetime,DTFormat.DateTime));
						}
						else
						{
							strBuilder.Append("null");
						}

						strBuilder.Append(",");

						if((drEach["act_hc_return_datetime"]!= null) && (!drEach["act_hc_return_datetime"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						{
							DateTime dtact_hc_return_datetime = (DateTime) drEach["act_hc_return_datetime"];
							strBuilder.Append(Utility.DateFormat(strAppID,strEnterpriseID,dtact_hc_return_datetime,DTFormat.DateTime));
						}
						else
						{
							strBuilder.Append("null");
						}
						//HC Return Task
						//Insert Other surcharge amount By Aoo 22/02/2008
						strBuilder.Append(",");

						if((drEach["other_surch_amount"] != null) && (!drEach["other_surch_amount"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						{
							decimal strOtherSurchAmt = (decimal) drEach["other_surch_amount"];
							//strBuilder.Append("'");
							strBuilder.Append(strOtherSurchAmt);
							//strBuilder.Append("'");
						}
						else
						{
							strBuilder.Append("null");
						}
						strBuilder.Append(",");

						//Calculate Total Amount (total_rated_amount)
						object  obj_tot_freight_charge = drEach["tot_freight_charge"], obj_insurance_surcharge = drEach["insurance_surcharge"],
							obj_other_surch_amount = drEach["other_surch_amount"], obj_tot_vas_surcharge = drEach["tot_vas_surcharge"],
							obj_esa_surcharge = drEach["esa_surcharge"];
						if( obj_tot_freight_charge!=DBNull.Value && obj_insurance_surcharge!=DBNull.Value && 
							obj_other_surch_amount!=DBNull.Value && obj_tot_vas_surcharge!=DBNull.Value && 
							obj_esa_surcharge!=DBNull.Value)
						{
							decimal d_tot_freight_charge=DBNullToZero(obj_tot_freight_charge), d_insurance_surcharge=DBNullToZero(obj_insurance_surcharge),
								d_other_surch_amount=DBNullToZero(obj_other_surch_amount),d_tot_vas_surcharge=DBNullToZero(obj_tot_vas_surcharge),
								d_esa_surcharge=DBNullToZero(obj_esa_surcharge);
							decimal d_total_rated_amount = d_tot_freight_charge + d_insurance_surcharge + d_other_surch_amount + 
								d_tot_vas_surcharge + d_esa_surcharge;
							strBuilder.Append(d_total_rated_amount);

							DataRow drCreditUsed = dtCreditUsed.NewRow();
							drCreditUsed["CustID"] = strPayerID;
							drCreditUsed["ConsignmentNo"] = drEach["consignment_no"].ToString();
							drCreditUsed["CreditAmount"] = d_total_rated_amount;
							dtCreditUsed.Rows.Add(drCreditUsed);
							dtCreditUsed.AcceptChanges();

						}
						else
						{
							strBuilder.Append("null");
						}


						strBuilder.Append(")");
						String strQuery = strBuilder.ToString();
						dbCmd = dbCon.CreateCommand(strQuery,CommandType.Text);
						dbCmd.Connection = conApp;
						dbCmd.Transaction = transactionApp;

						Logger.LogDebugInfo("module1","DomesticShipmentMgrDAL","AddDomesticShipment","INF001","QUERY IS : "+strQuery);
						iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
					}
					else
					{	

						strBuild = new StringBuilder();
						strBuild.Append("update shipment set ref_no = ");
						if((drEach["ref_no"]!= null) && (!drEach["ref_no"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						{
							String strRefNo = (String) drEach["ref_no"];
							strBuild.Append("'");
							strBuild.Append(strRefNo);
							strBuild.Append("'");
						}
						else
						{
							strBuild.Append("null");
						}
						strBuild.Append(",");

						strBuild.Append("booking_datetime = ");
						if((drEach["booking_datetime"]!= null) && (!drEach["booking_datetime"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						{
							DateTime dtBookingDtTime = (DateTime)drEach["booking_datetime"];
							strBuild.Append(Utility.DateFormat(strAppID,strEnterpriseID,dtBookingDtTime,DTFormat.DateTime));
						}
						else
						{
							strBuild.Append("null");
						}
						strBuild.Append(",");

						strBuild.Append("payerid = ");
						if((drEach["payerid"]!= null) && (!drEach["payerid"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						{
							strPayerID = (String) drEach["payerid"];
							strBuild.Append("'");
							strBuild.Append(Utility.ReplaceSingleQuote(strPayerID));
							strBuild.Append("'");
						}
						else
						{
							strBuild.Append("null");
						}
						strBuild.Append(",");
						strBuild.Append("payer_type = ");
						if((drEach["payer_type"]!= null) && (!drEach["payer_type"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						{
							String strPayerType = (String) drEach["payer_type"];
							strBuild.Append("'");
							strBuild.Append(strPayerType);
							strBuild.Append("'");
						}
						else
						{
							strBuild.Append("null");
						}
						strBuild.Append(",");
						strBuild.Append("new_account = ");
						if((drEach["new_account"]!= null) && (!drEach["new_account"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						{
							String strNewAccount = (String) drEach["new_account"];
							strBuild.Append("'");
							strBuild.Append(strNewAccount);
							strBuild.Append("'");
						}
						else
						{
							strBuild.Append("null");
						}
						strBuild.Append(",");
						strBuild.Append("payer_name = ");
						if((drEach["payer_name"]!= null) && (!drEach["payer_name"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						{
							String strPayerName = (String) drEach["payer_name"];
							strBuild.Append("N'");
							strBuild.Append(Utility.ReplaceSingleQuote(strPayerName));
							strBuild.Append("'");
						}
						else
						{
							strBuild.Append("null");
						}
						strBuild.Append(",");
						strBuild.Append("payer_address1 = ");
						if((drEach["payer_address1"]!= null) && (!drEach["payer_address1"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						{
							String strPayerAddress1 = (String) drEach["payer_address1"];
							strBuild.Append("N'");
							strBuild.Append(Utility.ReplaceSingleQuote(strPayerAddress1));
							strBuild.Append("'");
						}
						else
						{
							strBuild.Append("null");
						}
						strBuild.Append(",");
						strBuild.Append("payer_address2 = ");
						if((drEach["payer_address2"]!= null) && (!drEach["payer_address2"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						{
							String strPayerAddress2 = (String) drEach["payer_address2"];
							strBuild.Append("N'");
							strBuild.Append(Utility.ReplaceSingleQuote(strPayerAddress2));
							strBuild.Append("'");
						}
						else
						{
							strBuild.Append("null");
						}
						strBuild.Append(",");
						strBuild.Append("payer_zipcode = " );
						if((drEach["payer_zipcode"]!= null) && (!drEach["payer_zipcode"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						{
							String strPayerZipCode = (String) drEach["payer_zipcode"];
							strBuild.Append("'");
							strBuild.Append(Utility.ReplaceSingleQuote(strPayerZipCode));
							strBuild.Append("'");
						}
						else
						{
							strBuild.Append("null");
						}
						strBuild.Append(",");
						strBuild.Append("payer_country = ");
						if((drEach["payer_country"]!= null) && (!drEach["payer_country"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						{
							String strPayerCountry = (String) drEach["payer_country"];
							strBuild.Append("'");
							strBuild.Append(strPayerCountry);
							strBuild.Append("'");
						}
						else
						{
							strBuild.Append("null");
						}
						strBuild.Append(",");
						strBuild.Append("payer_telephone = ");
						if((drEach["payer_telephone"]!= null) && (!drEach["payer_telephone"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						{
							String strPayerTelephone = (String) drEach["payer_telephone"];
							strBuild.Append("'");
							strBuild.Append(Utility.ReplaceSingleQuote(strPayerTelephone));
							strBuild.Append("'");
						}
						else
						{
							strBuild.Append("null");
						}
						strBuild.Append(",");
						strBuild.Append("payment_mode = ");
						if((drEach["payment_mode"]!= null) && (!drEach["payment_mode"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						{
							String strPaymentMode = (String) drEach["payment_mode"];
							strBuild.Append("'");
							strBuild.Append(strPaymentMode);
							strBuild.Append("'");
						}
						else
						{
							strBuild.Append("null");
						}
						strBuild.Append(",");
						strBuild.Append("sender_name = ");
						if((drEach["sender_name"]!= null) && (!drEach["sender_name"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						{
							String strSenderName = (String) drEach["sender_name"];
							strBuild.Append("N'");
							strBuild.Append(Utility.ReplaceSingleQuote(strSenderName));
							strBuild.Append("'");
						}
						else
						{
							strBuild.Append("null");
						}
						strBuild.Append(",");
						strBuild.Append("sender_address1 = ");
						if((drEach["sender_address1"]!= null) && (!drEach["sender_address1"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						{
							String strSenderAddress1 = (String) drEach["sender_address1"];
							strBuild.Append("N'");
							strBuild.Append(Utility.ReplaceSingleQuote(strSenderAddress1));
							strBuild.Append("'");
						}
						else
						{
							strBuild.Append("null");
						}
						strBuild.Append(",");
						strBuild.Append("sender_address2 = ");
						if((drEach["sender_address2"]!= null) && (!drEach["sender_address2"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						{
							String strSenderAddress2 = (String) drEach["sender_address2"];
							strBuild.Append("N'");
							strBuild.Append(Utility.ReplaceSingleQuote(strSenderAddress2));
							strBuild.Append("'");
						}
						else
						{
							strBuild.Append("null");
						}
						strBuild.Append(",");
						strBuild.Append("sender_zipcode = ");
						if((drEach["sender_zipcode"]!= null) && (!drEach["sender_zipcode"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						{
							strSenderZipCode = (String) drEach["sender_zipcode"];
							strBuild.Append("'");
							strBuild.Append(Utility.ReplaceSingleQuote(strSenderZipCode));
							strBuild.Append("'");
						}
						else
						{
							strBuild.Append("null");
						}
						strBuild.Append(",");
						strBuild.Append("sender_country = ");
						if((drEach["sender_country"]!= null) && (!drEach["sender_country"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						{
							String strSenderCountry = (String) drEach["sender_country"];
							strBuild.Append("'");
							strBuild.Append(strSenderCountry);
							strBuild.Append("'");
						}
						else
						{
							strBuild.Append("null");
						}
						strBuild.Append(",");
						strBuild.Append("sender_telephone = ");
						if((drEach["sender_telephone"]!= null) && (!drEach["sender_telephone"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						{
							String strSenderTelephone = (String) drEach["sender_telephone"];
							strBuild.Append("'");
							strBuild.Append(Utility.ReplaceSingleQuote(strSenderTelephone));
							strBuild.Append("'");
						}
						else
						{
							strBuild.Append("null");
						}
						strBuild.Append(",");
						strBuild.Append(" sender_fax = ");
						if((drEach["sender_fax"]!= null) && (!drEach["sender_fax"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						{
							String strSenderFax = (String) drEach["sender_fax"];
							strBuild.Append("'");
							strBuild.Append(Utility.ReplaceSingleQuote(strSenderFax));
							strBuild.Append("'");
						}
						else
						{
							strBuild.Append("null");
						}
						strBuild.Append(",");
						strBuild.Append(" sender_contact_person = ");
						if((drEach["sender_contact_person"]!= null) && (!drEach["sender_contact_person"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						{
							String strSenderContactPerson = (String) drEach["sender_contact_person"];
							strBuild.Append("N'");
							strBuild.Append(Utility.ReplaceSingleQuote(strSenderContactPerson));
							strBuild.Append("'");
						}
						else
						{
							strBuild.Append("null");
						}
						strBuild.Append(",");
						strBuild.Append("recipient_name = ");
						if((drEach["recipient_name"]!= null) && (!drEach["recipient_name"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						{
							String strRecipientName = (String) drEach["recipient_name"];
							strBuild.Append("N'");
							strBuild.Append(Utility.ReplaceSingleQuote(strRecipientName));
							strBuild.Append("'");
						}
						else
						{
							strBuild.Append("null");
						}
						strBuild.Append(",");
						strBuild.Append("recipient_address1 = ");
						if((drEach["recipient_address1"]!= null) && (!drEach["recipient_address1"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						{
							String strRecipientAddress1 = (String) drEach["recipient_address1"];
							strBuild.Append("N'");
							strBuild.Append(Utility.ReplaceSingleQuote(strRecipientAddress1));
							strBuild.Append("'");
						}
						else
						{
							strBuild.Append("null");
						}
						strBuild.Append(",");
						strBuild.Append("recipient_address2 = ");
						if((drEach["recipient_address2"]!= null) && (!drEach["recipient_address2"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						{
							String strRecipientAddress2 = (String) drEach["recipient_address2"];
							strBuild.Append("N'");
							strBuild.Append(Utility.ReplaceSingleQuote(strRecipientAddress2));
							strBuild.Append("'");
						}
						else
						{
							strBuild.Append("null");
						}
						strBuild.Append(",");
						strBuild.Append("recipient_zipcode = ");
						if((drEach["recipient_zipcode"]!= null) && (!drEach["recipient_zipcode"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						{
							strRecipientZipCode = (String) drEach["recipient_zipcode"];
							strBuild.Append("'");
							strBuild.Append(Utility.ReplaceSingleQuote(strRecipientZipCode));
							strBuild.Append("'");
						}
						else
						{
							strBuild.Append("null");
						}
						strBuild.Append(",");
						strBuild.Append("recipient_country = ");
						if((drEach["recipient_country"]!= null) && (!drEach["recipient_country"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						{
							String strRecipientCountry = (String) drEach["recipient_country"];
							strBuild.Append("'");
							strBuild.Append(strRecipientCountry);
							strBuild.Append("'");
						}
						else
						{
							strBuild.Append("null");
						}
						strBuild.Append(",");
						strBuild.Append("tot_wt = ");
						if((drEach["tot_wt"]!= null) && (!drEach["tot_wt"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						{
							decimal decTotWt = (decimal) drEach["tot_wt"];
							strBuild.Append(decTotWt);
						}
						else
						{
							strBuild.Append("null");
						}
						//TU on 17June08
						strBuild.Append(",");
						strBuild.Append("tot_act_wt = ");
						if((drEach["tot_act_wt"]!= null) && (!drEach["tot_act_wt"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						{
							decimal decTot_act_Wt = (decimal) drEach["tot_act_wt"];
							strBuild.Append(decTot_act_Wt);
						}
						else
						{
							strBuild.Append("null");
						}
						//


						strBuild.Append(",");
						strBuild.Append("recipient_telephone = ");
						if((drEach["recipient_telephone"]!= null) && (!drEach["recipient_telephone"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						{
							String strRecipientTelephone = (String) drEach["recipient_telephone"];
							strBuild.Append("'");
							strBuild.Append(Utility.ReplaceSingleQuote(strRecipientTelephone));
							strBuild.Append("'");
						}
						else
						{
							strBuild.Append("null");
						}
						strBuild.Append(",");
					
						strBuild.Append("recipient_fax = ");
						if((drEach["recipient_fax"]!= null) && (!drEach["recipient_fax"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						{
							String strRecipientFax = (String) drEach["recipient_fax"];
							strBuild.Append("'");
							strBuild.Append(Utility.ReplaceSingleQuote(strRecipientFax));
							strBuild.Append("'");
						}
						else
						{
							strBuild.Append("null");
						}

						strBuild.Append(",");
						strBuild.Append("tot_pkg = ");
						if((drEach["tot_pkg"]!= null) && (!drEach["tot_pkg"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						{
							int iTotPkg = Convert.ToInt32(drEach["tot_pkg"]);
							strBuild.Append(iTotPkg);
						}
						else
						{
							strBuild.Append("null");
						}
						strBuild.Append(",");
						strBuild.Append("tot_dim_wt =");
						if((drEach["tot_dim_wt"]!= null) && (!drEach["tot_dim_wt"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						{
							decimal decTotDimWt = (decimal) drEach["tot_dim_wt"];
							strBuild.Append(decTotDimWt);
						}
						else
						{
							strBuild.Append("null");
						}
						strBuild.Append(",");
					
						strBuild.Append("service_code = ");
						if((drEach["service_code"]!= null) && (!drEach["service_code"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						{
							strServiceCode = (String) drEach["service_code"];
							strBuild.Append("'");
							strBuild.Append(strServiceCode);
							strBuild.Append("'");
						}
						else
						{
							strBuild.Append("null");
						}
						strBuild.Append(",");
						strBuild.Append("recipient_contact_person = ");
						if((drEach["recipient_contact_person"]!= null) && (!drEach["recipient_contact_person"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						{
							String strRecipientContactPerson = (String) drEach["recipient_contact_person"];
							strBuild.Append("N'");
							strBuild.Append(Utility.ReplaceSingleQuote(strRecipientContactPerson));
							strBuild.Append("'");
						}
						else
						{
							strBuild.Append("null");
						}
						strBuild.Append(",");
						strBuild.Append("declare_value = ");
						if((drEach["declare_value"]!= null) && (!drEach["declare_value"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						{
							decimal decDeclareValue = (decimal) drEach["declare_value"];
							strBuild.Append(decDeclareValue);
						}
						else
						{
							strBuild.Append("null");
						}
						strBuild.Append(",");
					
						strBuild.Append("chargeable_wt = ");
						if((drEach["chargeable_wt"]!= null) && (!drEach["chargeable_wt"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						{
							decimal decChargeableWt = (decimal) drEach["chargeable_wt"];
							strBuild.Append(decChargeableWt);
						}
						else
						{
							strBuild.Append("null");
						}

						strBuild.Append(",");
						strBuild.Append("insurance_surcharge = ");
						if((drEach["insurance_surcharge"]!= null) && (!drEach["insurance_surcharge"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						{
							decimal decInsuranceSurcharge = (decimal) drEach["insurance_surcharge"];
							strBuild.Append(decInsuranceSurcharge);
						}
						else
						{
							strBuild.Append("null");
						}
						//max_insurance_cover
						strBuild.Append(",");
						strBuild.Append("max_insurance_cover = ");
						if((drEach["max_insurance_cover"]!= null) && (!drEach["max_insurance_cover"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						{
							decimal decmax_insurance_cover = (decimal) drEach["max_insurance_cover"];
							strBuild.Append(decmax_insurance_cover);
						}
						else
						{
							strBuild.Append("null");
						}
						//
						//payer_fax
						strBuild.Append(",");
						strBuild.Append("payer_fax = ");
						if((drEach["payer_fax"]!= null) && (!drEach["payer_fax"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						{
							String strpayer_fax = (String) drEach["payer_fax"];
							strBuild.Append("'");
							strBuild.Append(strpayer_fax);
							strBuild.Append("'");
						}
						else
						{
							strBuild.Append("null");
						}
						//

						strBuild.Append(",");

						strBuild.Append("act_pickup_datetime = ");
						if((drEach["act_pickup_datetime"]!= null) && (!drEach["act_pickup_datetime"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						{
							DateTime dtActPickup = Convert.ToDateTime(drEach["act_pickup_datetime"]);
							strBuild.Append(Utility.DateFormat(strAppID,strEnterpriseID,dtActPickup,DTFormat.DateTime));
						}
						else
						{
							strBuild.Append("null");
						}
						strBuild.Append(",");
						strBuild.Append("commodity_code = ");
						if((drEach["commodity_code"]!= null) && (!drEach["commodity_code"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						{
							String strCommodityCode = (String) drEach["commodity_code"];
							strBuild.Append("'");
							strBuild.Append(strCommodityCode);
							strBuild.Append("'");
						}
						else
						{
							strBuild.Append("null");
						}
						strBuild.Append(",");
					
						strBuild.Append("est_delivery_datetime = ");
						if((drEach["est_delivery_datetime"]!= null) && (!drEach["est_delivery_datetime"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						{
							DateTime dtEstDeliveryDatetime = Convert.ToDateTime(drEach["est_delivery_datetime"]);
							strBuild.Append(Utility.DateFormat(strAppID,strEnterpriseID,dtEstDeliveryDatetime,DTFormat.DateTime));

						}
						else
						{
							strBuild.Append("null");
						}
						strBuild.Append(",");
						strBuild.Append("percent_dv_additional = ");
						if((drEach["percent_dv_additional"]!= null) && (!drEach["percent_dv_additional"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						{
							decimal decPercentDvAdditional = (decimal) drEach["percent_dv_additional"];
							strBuild.Append(decPercentDvAdditional);
						}
						else
						{
							strBuild.Append("null");
						}
						strBuild.Append(",");
						strBuild.Append("act_delivery_date = ");
						if((drEach["act_delivery_date"]!= null) && (!drEach["act_delivery_date"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						{
							DateTime dtActDeliveryDate = Convert.ToDateTime(drEach["act_delivery_date"]);
							strBuild.Append(Utility.DateFormat(strAppID,strEnterpriseID,dtActDeliveryDate,DTFormat.DateTime));
						}
						else
						{
							strBuild.Append("null");
						}
						strBuild.Append(",");
						strBuild.Append(" payment_type = ");
						if((drEach["payment_type"]!= null) && (!drEach["payment_type"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						{
							String strPaymentType = (String) drEach["payment_type"];
							strBuild.Append("'");
							strBuild.Append(strPaymentType);
							strBuild.Append("'");
						}
						else
						{
							strBuild.Append("null");
						}
						strBuild.Append(",");
					
						strBuild.Append("return_pod_slip = ");
						if((drEach["return_pod_slip"]!= null) && (!drEach["return_pod_slip"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						{
							String strReturnPODSlip = (String) drEach["return_pod_slip"];
							strBuild.Append("'");
							strBuild.Append(strReturnPODSlip);
							strBuild.Append("'");
						}
						else
						{
							strBuild.Append("null");
						}
						strBuild.Append(",");
					
						strBuild.Append("shipment_type = ");
						if((drEach["shipment_type"]!= null) && (!drEach["shipment_type"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						{
							String strShipmentType = (String) drEach["shipment_type"];
							strBuild.Append("'");
							strBuild.Append(strShipmentType);
							strBuild.Append("'");
						}
						else
						{
							strBuild.Append("null");
						}
						strBuild.Append(",");
					
						strBuild.Append("origin_state_code = ");
						if((drEach["origin_state_code"]!= null) && (!drEach["origin_state_code"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						{
							String strOriginStateCode = (String) drEach["origin_state_code"];
							strBuild.Append("'");
							strBuild.Append(strOriginStateCode);
							strBuild.Append("'");
						}
						else
						{
							strBuild.Append("null");
						}
						strBuild.Append(",");
						strBuild.Append(" destination_state_code = ");
						if((drEach["destination_state_code"]!= null) && (!drEach["destination_state_code"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						{
							String strDestinationStateCode = (String) drEach["destination_state_code"];
							strBuild.Append("'");
							strBuild.Append(strDestinationStateCode);
							strBuild.Append("'");
						}
						else
						{
							strBuild.Append("null");
						}

						strBuild.Append(",");
						strBuild.Append(" tot_freight_charge = ");
						if((drEach["tot_freight_charge"]!= null) && (!drEach["tot_freight_charge"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						{
							decimal decTotFreight = (decimal) drEach["tot_freight_charge"];
							strBuild.Append(decTotFreight);
						}
						else
						{
							strBuild.Append("null");
						}

						strBuild.Append(",");
						strBuild.Append(" tot_vas_surcharge = ");
						if((drEach["tot_vas_surcharge"]!= null) && (!drEach["tot_vas_surcharge"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						{
							decimal decTotVASSurcharge = (decimal) drEach["tot_vas_surcharge"];
							strBuild.Append(decTotVASSurcharge);
						}
						else
						{
							strBuild.Append("null");
						}
						strBuild.Append(",");
						strBuild.Append(" last_status_code = ");
						if((drEach["last_status_code"]!= null) && (!drEach["last_status_code"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						{
							String strLastStatusCode = (String) drEach["last_status_code"];
							strBuild.Append("'");
							strBuild.Append(strLastStatusCode);
							strBuild.Append("'");
						}
						else
						{
							strBuild.Append("null");
						}
						strBuild.Append(",");
						strBuild.Append(" last_exception_code = ");
						if((drEach["last_exception_code"]!= null) && (!drEach["last_exception_code"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						{
							String strLastExceptionCode = (String) drEach["last_exception_code"];
							strBuild.Append("'");
							strBuild.Append(strLastExceptionCode);
							strBuild.Append("'");
						}
						else
						{
							strBuild.Append("null");
						}
						strBuild.Append(",");
						strBuild.Append("last_status_datetime = ");
						if((drEach["last_status_datetime"]!= null) && (!drEach["last_status_datetime"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						{
							DateTime dtLastStatusDateTime = Convert.ToDateTime(drEach["last_status_datetime"]);
							strBuild.Append(Utility.DateFormat(strAppID,strEnterpriseID,dtLastStatusDateTime,DTFormat.DateTime));
						}
						else
						{
							strBuild.Append("null");
						}
						strBuild.Append(",");
						strBuild.Append(" shpt_manifest_datetime = ");
						if((drEach["shpt_manifest_datetime"]!= null) && (!drEach["shpt_manifest_datetime"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						{
							DateTime dtShptManifestDateTime = Convert.ToDateTime(drEach["shpt_manifest_datetime"]);
							strBuild.Append(Utility.DateFormat(strAppID,strEnterpriseID,dtShptManifestDateTime,DTFormat.DateTime));
						}
						else
						{
							strBuild.Append("null");
						}
						strBuild.Append(",");
						strBuild.Append(" delivery_manifested = ");
						if((drEach["delivery_manifested"]!= null) && (!drEach["delivery_manifested"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						{
							String strDeliveryManifested = (String) drEach["delivery_manifested"];
							strBuild.Append("'");
							strBuild.Append(strDeliveryManifested);
							strBuild.Append("'");
						}
						else
						{
							strBuild.Append("null");
						}
						strBuild.Append(",");
					
						strBuild.Append(" route_code = ");
						if((drEach["route_code"]!= null) && (!drEach["route_code"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						{
							String strRouteCode = (String) drEach["route_code"];
							strBuild.Append("'");
							strBuild.Append(strRouteCode);
							strBuild.Append("'");
						}
						else
						{
							strBuild.Append("null");
						}
						strBuild.Append(",");

						strBuild.Append("quotation_no = ");
						if((drEach["quotation_no"]!= null) && (!drEach["quotation_no"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						{
							String strQuotationNo = (String) drEach["quotation_no"];
							strBuild.Append("'");
							strBuild.Append(strQuotationNo);
							strBuild.Append("'");
						}
						else
						{
							strBuild.Append("null");
						}
						strBuild.Append(",");
						//
						strBuild.Append("remark = ");
						if((drEach["remark"]!= null) && (!drEach["remark"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						{
							String strRemark = (String) drEach["remark"];
							strBuild.Append("N'");
							strBuild.Append(Utility.ReplaceSingleQuote(strRemark));
							strBuild.Append("'");
						}
						else
						{
							strBuild.Append("null");
						}
						strBuild.Append(",");
						//
						strBuild.Append(" Quotation_Version = ");
						if((drEach["Quotation_Version"]!= null) && (!drEach["Quotation_Version"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						{
							int iQuotationVersion =Convert.ToInt32(drEach["Quotation_Version"]);
							strBuild.Append(iQuotationVersion);
						}
						else
						{
							strBuild.Append("null");
						}
						strBuild.Append(",");

						strBuild.Append(" cod_amount = ");
						if((drEach["cod_amount"]!= null) && (!drEach["cod_amount"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						{
							decimal decCODAmount = (decimal) drEach["cod_amount"];
							strBuild.Append(decCODAmount);
						}
						else
						{
							strBuild.Append("0");
						}
						strBuild.Append(",");

						strBuild.Append(" origin_station = ");
						if((drEach["origin_station"]!= null) && (!drEach["origin_station"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						{
							String strorigin_station = (String) drEach["origin_station"];
							strBuild.Append("N'");
							strBuild.Append(Utility.ReplaceSingleQuote(strorigin_station));
							strBuild.Append("'");
						}
						else
						{
							strBuild.Append("null");
						}
						strBuild.Append(",");

						strBuild.Append(" destination_station = ");
						if((drEach["destination_station"]!= null) && (!drEach["destination_station"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						{
							String strdest_station = (String) drEach["destination_station"];
							strBuild.Append("N'");
							strBuild.Append(Utility.ReplaceSingleQuote(strdest_station));
							strBuild.Append("'");
						}
						else
						{
							strBuild.Append("null");
						}

						//HC Return Task
						strBuild.Append(",");
						strBuild.Append(" return_invoice_hc = ");
						if((drEach["return_invoice_hc"]!= null) && (!drEach["return_invoice_hc"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						{
							String strreturn_invoice_hc = (String) drEach["return_invoice_hc"];
							strBuild.Append("N'");
							strBuild.Append(Utility.ReplaceSingleQuote(strreturn_invoice_hc));
							strBuild.Append("'");
						}
						else
						{
							strBuild.Append("null");
						}

						strBuild.Append(",");
						strBuild.Append(" est_hc_return_datetime = ");
						if((drEach["est_hc_return_datetime"]!= null) && (!drEach["est_hc_return_datetime"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						{
							DateTime dtest_hc_return_datetime = Convert.ToDateTime(drEach["est_hc_return_datetime"]);
							strBuild.Append(Utility.DateFormat(strAppID,strEnterpriseID,dtest_hc_return_datetime,DTFormat.DateTime));
						}
						else
						{
							strBuild.Append("null");
						}

						strBuild.Append(",");
						strBuild.Append(" act_hc_return_datetime = ");
						if((drEach["act_hc_return_datetime"]!= null) && (!drEach["act_hc_return_datetime"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						{
							DateTime dtact_hc_return_datetime = Convert.ToDateTime(drEach["act_hc_return_datetime"]);
							strBuild.Append(Utility.DateFormat(strAppID,strEnterpriseID,dtact_hc_return_datetime,DTFormat.DateTime));
						}
						else
						{
							strBuild.Append("null");
						}
						strBuild.Append(",");
						strBuild.Append(" esa_surcharge = ");
						if((drEach["esa_surcharge"]!= null) && (!drEach["esa_surcharge"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						{
							decimal decESASurcharge = (decimal) drEach["esa_surcharge"];
							strBuild.Append(decESASurcharge);
						}
						else
						{
							strBuild.Append("null");
						}


					
						object  obj_tot_freight_charge = drEach["tot_freight_charge"], obj_insurance_surcharge = drEach["insurance_surcharge"],
							obj_other_surch_amount = drEach["other_surch_amount"], obj_tot_vas_surcharge = drEach["tot_vas_surcharge"],
							obj_esa_surcharge = drEach["esa_surcharge"];
					
						//other_surch_amount
						strBuild.Append(",");
						strBuild.Append(" other_surch_amount = ");
						if( obj_other_surch_amount!=DBNull.Value) strBuild.Append(DBNullToZero(obj_other_surch_amount));
						else strBuild.Append("null");


						//Calculate Total Amount (total_rated_amount)
						strBuild.Append(",");
						strBuild.Append(" total_rated_amount = ");
						if( obj_tot_freight_charge!=DBNull.Value && obj_insurance_surcharge!=DBNull.Value && 
							obj_other_surch_amount!=DBNull.Value && obj_tot_vas_surcharge!=DBNull.Value && 
							obj_esa_surcharge!=DBNull.Value)
						{
						
							decimal d_tot_freight_charge=DBNullToZero(obj_tot_freight_charge), d_insurance_surcharge=DBNullToZero(obj_insurance_surcharge),
								d_other_surch_amount=DBNullToZero(obj_other_surch_amount),d_tot_vas_surcharge=DBNullToZero(obj_tot_vas_surcharge),
								d_esa_surcharge=DBNullToZero(obj_esa_surcharge);
							decimal d_total_rated_amount = d_tot_freight_charge + d_insurance_surcharge + d_other_surch_amount + 
								d_tot_vas_surcharge + d_esa_surcharge;
							strBuild.Append(d_total_rated_amount);
						}
						else
						{
							strBuild.Append("null");
						}

						//HC Return Task

						strBuild.Append( " where applicationid = '");
						strBuild.Append(strAppID+"' and enterpriseid = '");
						strBuild.Append(strEnterpriseID);
						strBuild.Append("' and booking_no = ");
						strBuild.Append(iBooking_no);
						strBuild.Append(" and consignment_no = '");
						strBuild.Append(Utility.ReplaceSingleQuote(drEach["consignment_no"].ToString()));
						strBuild.Append("'");
					
						dbCmd = dbCon.CreateCommand(strBuild.ToString(),CommandType.Text);
						dbCmd.Connection = conApp;
						dbCmd.Transaction = transactionApp;

						iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
					}

					//Logger.LogDebugInfo("DomesticShipmentMgrDAL","UpdateDomesticShp","INF001",iRowsAffected + " rows updated in Shipment table");*/

					if(dsPkg != null)
					{
						//delete from the shipment_pkg table
						strBuild = new StringBuilder();
						strBuild.Append("delete from shipment_pkg where applicationid = '");
						strBuild.Append(strAppID+"' and enterpriseid = '");
						strBuild.Append(strEnterpriseID);
						strBuild.Append("' and booking_no <> ");
						strBuild.Append(iBooking_no); // Old booking No
						strBuild.Append(" and consignment_no = '");
						strBuild.Append(Utility.ReplaceSingleQuote(drEach["consignment_no"].ToString()));
						strBuild.Append("'");
						dbCmd.CommandText = strBuild.ToString();
						iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);

						strBuild = new StringBuilder();
						strBuild.Append("delete from shipment_pkg_box where applicationid = '");
						strBuild.Append(strAppID+"' and enterpriseid = '");
						strBuild.Append(strEnterpriseID);
						strBuild.Append("' and booking_no <> ");
						strBuild.Append(iBooking_no); // Old booking No
						strBuild.Append(" and consignment_no = '");
						strBuild.Append(Utility.ReplaceSingleQuote(drEach["consignment_no"].ToString()));
						strBuild.Append("'");
						dbCmd.CommandText = strBuild.ToString();
						iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
						Logger.LogDebugInfo("DomesticShipmentMgrDAL","UpdateDomesticShp","INF002",iRowsAffected + " rows deleted from Shipment_pkg table");

						//insert into the shipment_pkg table
						cnt = dsPkg.Tables[0].Rows.Count;
						i = 0;
						Customer customer = new Customer();
						customer.Populate(strAppID,strEnterpriseID, strPayerID);
						Zipcode zipcode = new Zipcode();

						zipcode.Populate(strAppID,strEnterpriseID,strSenderZipCode);
						String strOrgZone = zipcode.ZoneCode;

						zipcode.Populate(strAppID,strEnterpriseID,strRecipientZipCode);
						String strDestnZone = zipcode.ZoneCode;
						int sumQty = 0;
						float weightToFreight = 0;
						decimal freightCharge = 0;
						for(i=0;i<cnt;i++)
						{
							DataRow drEachPKG = dsPkg.Tables[0].Rows[i];
							String strMpsNo = "";
							if(drEachPKG["mps_code"].ToString() != "")
							{
								strMpsNo = (String)drEachPKG["mps_code"];
							}
							
							decimal decPkgLgth = Convert.ToDecimal(drEachPKG["pkg_length"]);
							decimal decPkgBrdth = Convert.ToDecimal(drEachPKG["pkg_breadth"]);
							decimal decPkgHgth = Convert.ToDecimal(drEachPKG["pkg_height"]);
							decimal decPkgWt = Convert.ToDecimal(drEachPKG["pkg_wt"]);
							int iPkgQty = Convert.ToInt32(drEachPKG["pkg_qty"]);
							decimal decTotalWt = Convert.ToDecimal(drEachPKG["tot_wt"]);
							decimal decTotal_act_Wt = Convert.ToDecimal(drEachPKG["tot_act_wt"]);//TU on 17June08
							decimal decTotalDimWt = Convert.ToDecimal(drEachPKG["tot_dim_wt"]);
							decimal decChrgbleWt = Convert.ToDecimal(drEachPKG["chargeable_wt"]);

							if(float.Parse(drEachPKG["tot_wt"].ToString())>float.Parse(drEachPKG["tot_dim_wt"].ToString()))
							{
								if(customer.IsCustomer_Box== "Y")
									weightToFreight = float.Parse(drEachPKG["tot_wt"].ToString())/int.Parse(drEachPKG["pkg_qty"].ToString());
								else
									weightToFreight = float.Parse(drEachPKG["tot_wt"].ToString());
							}
							else
							{
								if(customer.IsCustomer_Box== "Y")
									weightToFreight = float.Parse(drEachPKG["tot_dim_wt"].ToString())/int.Parse(drEachPKG["pkg_qty"].ToString());
								else
									weightToFreight = float.Parse(drEachPKG["tot_dim_wt"].ToString());
							}
							sumQty = sumQty+int.Parse(drEachPKG["pkg_qty"].ToString());
							freightCharge = (decimal)TIESUtility.ComputeFreightCharge(strAppID, strEnterpriseID,
								strPayerID, "C", 
								strOrgZone, strDestnZone, weightToFreight,
								strServiceCode, strSenderZipCode, strRecipientZipCode);

							if(customer.IsCustomer_Box == "Y")
								freightCharge = (freightCharge * int.Parse(drEachPKG["pkg_qty"].ToString()));

							strBuild = new StringBuilder();		
							//TU on 17June08
							strBuild.Append("insert into shipment_pkg (applicationid,enterpriseid,booking_no,consignment_no,mps_code,pkg_length,pkg_breadth,pkg_height,pkg_wt,pkg_qty,tot_wt,tot_act_wt,tot_dim_wt,chargeable_wt)values('");
							strBuild.Append(strAppID+"','");
							strBuild.Append(strEnterpriseID+"',");
							strBuild.Append(iBooking_no); //New booking no
							strBuild.Append(",'");
							strBuild.Append(Utility.ReplaceSingleQuote(drEach["consignment_no"].ToString()));
							strBuild.Append("','");
							strBuild.Append(strMpsNo+"',");
							strBuild.Append(decPkgLgth+",");
							strBuild.Append(decPkgBrdth+",");
							strBuild.Append(decPkgHgth+",");
							strBuild.Append(decPkgWt+",");
							strBuild.Append(iPkgQty+",");
							strBuild.Append(decTotalWt+",");
							strBuild.Append(decTotal_act_Wt+",");//TU on 17June08
							strBuild.Append(decTotalDimWt+",");
							strBuild.Append(decChrgbleWt+")");
				
							dbCmd.CommandText = strBuild.ToString();
							iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
							
							strBuild = new StringBuilder();
							//Chai	18/04/2012
							/*
							strBuild.Append("insert into shipment_pkg_box (applicationid,enterpriseid,booking_no,consignment_no,mps_code,pkg_length,pkg_breadth,pkg_height,pkg_wt,pkg_qty,tot_wt,tot_act_wt,tot_dim_wt,chargeable_wt,Freight_Charge)values('");
							strBuild.Append(strAppID+"','");
							strBuild.Append(strEnterpriseID+"',");
							strBuild.Append(iBooking_no); //New booking no
							strBuild.Append(",'");
							strBuild.Append(Utility.ReplaceSingleQuote(drEach["consignment_no"].ToString()));
							strBuild.Append("','");
							strBuild.Append(strMpsNo+"',");
							strBuild.Append(decPkgLgth+",");
							strBuild.Append(decPkgBrdth+",");
							strBuild.Append(decPkgHgth+",");
							strBuild.Append(decPkgWt+",");
							strBuild.Append(iPkgQty+",");
							strBuild.Append(decTotalWt+",");
							strBuild.Append(decTotal_act_Wt+",");//TU on 17June08
							strBuild.Append(decTotalDimWt+",");
							strBuild.Append(decChrgbleWt+",");
							strBuild.Append(freightCharge+")");
				
							dbCmd.CommandText = strBuild.ToString();
							iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
							*/
							Logger.LogDebugInfo("DomesticShipmentMgrDAL","AddDomesticShipment","INF003",iRowsAffected + " rows inserted into shipment_pkg table");
						}					
						if(sumQty < int.Parse(customer.Minimum_Box))
						{
							int packageAdd = int.Parse(customer.Minimum_Box)-sumQty;
							int mpsNo = sumQty+1;
							freightCharge = (decimal)TIESUtility.ComputeFreightCharge(strAppID, strEnterpriseID,
								strPayerID, "C", 
								strOrgZone, strDestnZone, 0,
								strServiceCode, strSenderZipCode, strRecipientZipCode);
							for(int k=0;k<packageAdd;k++)
							{
								//Chai	18/04/2012
								/*
								strBuild = new StringBuilder();
								strBuild.Append("insert into shipment_pkg_box (applicationid,enterpriseid,booking_no,consignment_no,mps_code,pkg_length,pkg_breadth,pkg_height,pkg_wt,pkg_qty,tot_wt,tot_act_wt,tot_dim_wt,chargeable_wt,Freight_Charge)values('");
								strBuild.Append(strAppID+"','");
								strBuild.Append(strEnterpriseID+"',");
								strBuild.Append(iBooking_no); //New booking no
								strBuild.Append(",'");
								strBuild.Append(Utility.ReplaceSingleQuote(drEach["consignment_no"].ToString()));
								strBuild.Append("','");
								strBuild.Append(mpsNo.ToString()+"',");
								strBuild.Append(0+",");
								strBuild.Append(0+",");
								strBuild.Append(0+",");
								strBuild.Append(0+",");
								strBuild.Append(0+",");
								strBuild.Append(0+",");
								strBuild.Append(0+",");//TU on 17June08
								strBuild.Append(0+",");
								strBuild.Append(0+",");
								strBuild.Append(freightCharge+")");

								dbCmd.CommandText = strBuild.ToString();
								iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
								*/
								mpsNo = mpsNo+1;
								Logger.LogDebugInfo("DomesticShipmentMgrDAL","AddDomesticShipment","INF003",iRowsAffected + " rows inserted into shipment_pkg_box table");
							}
						}
					}
					
					//delete from the shipment_vas table
					strBuild = new StringBuilder();
					strBuild.Append("delete from shipment_vas where applicationid = '");
					strBuild.Append(strAppID+"' and enterpriseid = '");
					strBuild.Append(strEnterpriseID);
					strBuild.Append("' and booking_no <> ");
					strBuild.Append(iBooking_no); //Old booking no
					strBuild.Append(" and consignment_no = '");
					strBuild.Append(Utility.ReplaceSingleQuote(drEach["consignment_no"].ToString()));
					strBuild.Append("'");
				
					dbCmd.CommandText = strBuild.ToString();
					iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
					Logger.LogDebugInfo("DomesticShipmentMgrDAL","UpdateDomesticShp","INF004",iRowsAffected + " rows deleted from Shipment_VAS table");
					
					
					if(dsVAS.Tables[0].Rows.Count > 0)
					{
						//insert into the shipment_vas table
						cnt = dsVAS.Tables[0].Rows.Count;
						i = 0;

						for(i=0;i<cnt;i++)
						{
							DataRow drEachVAS = dsVAS.Tables[0].Rows[i];
			
							String strVASCode = "";
							if(drEachVAS["vas_code"].ToString() != "")
							{
								strVASCode = (String)drEachVAS["vas_code"];
							}
							decimal decSurcharge = (decimal)drEachVAS["surcharge"];
			
							String strRemarks = "";
							if(drEachVAS["remarks"].ToString() != "")
							{
								strRemarks = (String)drEachVAS["remarks"];
							}

							strBuild = new StringBuilder();			
							strBuild.Append("insert into shipment_vas (applicationid,enterpriseid,booking_no,consignment_no,vas_code,vas_surcharge,remark)values('");
							strBuild.Append(strAppID+"','");
							strBuild.Append(strEnterpriseID+"',");
							strBuild.Append(iBooking_no);//New booking no
							strBuild.Append(",'");
							strBuild.Append(Utility.ReplaceSingleQuote(drEach["consignment_no"].ToString()));
							strBuild.Append("','");
							strBuild.Append(Utility.ReplaceSingleQuote(strVASCode)+"',");
							strBuild.Append(decSurcharge+",N'");
							strBuild.Append(Utility.ReplaceSingleQuote(strRemarks)+"')");

							dbCmd.CommandText = strBuild.ToString();
							iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
							Logger.LogDebugInfo("DomesticShipmentMgrDAL","UpdateDomesticShp","INF005",iRowsAffected + " rows inserted into shipment_vas table");
					
						}
					}
					/*if(dsVAS.Tables[0].Rows.Count > 0)
					{
						//delete from the shipment_vas table
						strBuild = new StringBuilder();
						strBuild.Append("delete from shipment_vas where applicationid = '");
						strBuild.Append(strAppID+"' and enterpriseid = '");
						strBuild.Append(strEnterpriseID);
						strBuild.Append("' and booking_no = ");
						strBuild.Append(iBooking_no);
						strBuild.Append(" and consignment_no = '");
						strBuild.Append(Utility.ReplaceSingleQuote(drEach["consignment_no"].ToString()));
						strBuild.Append("'");
					
						dbCmd.CommandText = strBuild.ToString();
						iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
						Logger.LogDebugInfo("DomesticShipmentMgrDAL","UpdateDomesticShp","INF004",iRowsAffected + " rows deleted from Shipment_VAS table");
					
					
					
						//insert into the shipment_vas table
						cnt = dsVAS.Tables[0].Rows.Count;
						i = 0;

						for(i=0;i<cnt;i++)
						{
							DataRow drEachVAS = dsVAS.Tables[0].Rows[i];
			
							String strVASCode = "";
							if(drEachVAS["vas_code"].ToString() != "")
							{
								strVASCode = (String)drEachVAS["vas_code"];
							}
							decimal decSurcharge = (decimal)drEachVAS["surcharge"];
			
							String strRemarks = "";
							if(drEachVAS["remarks"].ToString() != "")
							{
								strRemarks = (String)drEachVAS["remarks"];
							}

							strBuild = new StringBuilder();			
							strBuild.Append("insert into shipment_vas (applicationid,enterpriseid,booking_no,consignment_no,vas_code,vas_surcharge,remark)values('");
							strBuild.Append(strAppID+"','");
							strBuild.Append(strEnterpriseID+"',");
							strBuild.Append(iBooking_no);
							strBuild.Append(",'");
							strBuild.Append(Utility.ReplaceSingleQuote(drEach["consignment_no"].ToString()));
							strBuild.Append("','");
							strBuild.Append(Utility.ReplaceSingleQuote(strVASCode)+"',");
							strBuild.Append(decSurcharge+",N'");
							strBuild.Append(Utility.ReplaceSingleQuote(strRemarks)+"')");

							dbCmd.CommandText = strBuild.ToString();
							iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
							Logger.LogDebugInfo("DomesticShipmentMgrDAL","UpdateDomesticShp","INF005",iRowsAffected + " rows inserted into shipment_vas table");
					
						}
					}*/
					//check whether the consignment no. has changed & update the pickup_shipment table
					//					strBuild = new StringBuilder();
					//					strBuild.Append(" update pickup_shipment set consignment_no = ");
					//					strBuild.Append("'");
					//					strBuild.Append(strConsgNoCurr);
					//					strBuild.Append("'");
					//					strBuild.Append( " where applicationid = '");
					//					strBuild.Append(strAppID+"' and enterpriseid = '");
					//					strBuild.Append(strEnterpriseID);
					//					strBuild.Append("' and booking_no = ");
					//					strBuild.Append(iBooking_no);
					//					strBuild.Append(" and consignment_no = '");
					//					strBuild.Append(strConsgNoOrg);
					//					strBuild.Append("'");
					//					strBuild.Append(" and consignment_no != '");
					//					strBuild.Append(strConsgNoCurr);
					//					strBuild.Append("'");
					//
					//					dbCmd.CommandText = strBuild.ToString();
					//					iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
					//					Logger.LogDebugInfo("DomesticShipmentMgrDAL","UpdateDomesticShp","INF006",iRowsAffected + " rows updated in table pickup_shipment");
					//
					//
					//					strBuild = new StringBuilder();
					//					strBuild.Append("update shipment_tracking set tracking_datetime = ");
					//					if((drEach["shpt_manifest_datetime"]!= null) && (!drEach["shpt_manifest_datetime"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					//					{
					//						DateTime dtShptManifestDateTime = Convert.ToDateTime(drEach["shpt_manifest_datetime"]);
					//						strBuild.Append(Utility.DateFormat(strAppID,strEnterpriseID,dtShptManifestDateTime,DTFormat.DateTime));
					//					}
					//					else
					//					{
					//						strBuild.Append("null");
					//					}
					//					strBuild.Append(",");
					//					strBuild.Append("consignment_no = ");
					//					if((drEach["consignment_no"]!= null) && (!drEach["consignment_no"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					//					{
					//						String strConsgnmentNo = (String) drEach["consignment_no"];
					//						strBuild.Append("'");
					//						strBuild.Append(strConsgnmentNo);
					//						strBuild.Append("'");
					//					}
					//
					//				
					//					strBuild.Append( " where applicationid = '");
					//					strBuild.Append(strAppID+"' and enterpriseid = '");
					//					strBuild.Append(strEnterpriseID);
					//					strBuild.Append("' and booking_no = ");
					//					strBuild.Append(iBooking_no);
					//					strBuild.Append(" and consignment_no = ");
					//					if((drEach["consignment_no"]!= null) && (!drEach["consignment_no"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					//					{
					//						String strConsgNumber = (String)drEach["consignment_no",DataRowVersion.Original];
					//						strBuild.Append("'");
					//						strBuild.Append(strConsgNumber);
					//						strBuild.Append("'");
					//					}
					//					strBuild.Append(" and tracking_datetime = ");
					//					if((drEach["shpt_manifest_datetime"]!= null) && (!drEach["shpt_manifest_datetime"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					//					{
					//						DateTime dtShptManifestDateTime = Convert.ToDateTime(drEach["shpt_manifest_datetime",DataRowVersion.Original]);
					//
					//						strBuild.Append(Utility.DateFormat(strAppID,strEnterpriseID,dtShptManifestDateTime,DTFormat.DateTime));
					//					}
					//					else
					//					{
					//						strBuild.Append("null");
					//					}
					//
					//					dbCmd.CommandText = strBuild.ToString();
					//					iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
					//					Logger.LogDebugInfo("DomesticShipmentMgrDAL","UpdateDomesticShp","INF007",iRowsAffected + " rows inserted into shipment_tracking table");


					if (iBooking_no != preShipBkNo)
					{
						strBuild = new StringBuilder();			
						strBuild.Append("insert into shipment_tracking ");
						strBuild.Append("select applicationid, enterpriseid, consignment_no, tracking_datetime, " + iBooking_no + ", ");
						strBuild.Append("status_code, exception_code, person_incharge, remark, deleted, delete_remark, last_userid, last_updated, ");
						strBuild.Append("invoiceable, reason, surcharge, consignee_name, location ");
						strBuild.Append("from shipment_tracking ");
						strBuild.Append("where applicationid = '");
						strBuild.Append(strAppID);
						strBuild.Append("' and enterpriseid = '");
						strBuild.Append(strEnterpriseID);
						strBuild.Append("' and consignment_no = '");
						strBuild.Append(Utility.ReplaceSingleQuote(preConNo));
						strBuild.Append("' and booking_no = " + preShipBkNo);
					
						dbCmd.CommandText = strBuild.ToString();
						iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);

						strBuild = new StringBuilder();			
						strBuild.Append("delete from shipment_tracking ");
						strBuild.Append("where applicationid = '");
						strBuild.Append(strAppID);
						strBuild.Append("' and enterpriseid = '");
						strBuild.Append(strEnterpriseID);
						strBuild.Append("' and consignment_no = '");
						strBuild.Append(Utility.ReplaceSingleQuote(preConNo));
						strBuild.Append("' and booking_no <> " + iBooking_no);

						dbCmd.CommandText = strBuild.ToString();
						iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);

						strBuild = new StringBuilder();			
						strBuild.Append("delete from shipment ");
						strBuild.Append("where applicationid = '");
						strBuild.Append(strAppID);
						strBuild.Append("' and enterpriseid = '");
						strBuild.Append(strEnterpriseID);
						strBuild.Append("' and consignment_no = '");
						strBuild.Append(Utility.ReplaceSingleQuote(preConNo));
						strBuild.Append("' and booking_no <> " + iBooking_no);

						dbCmd.CommandText = strBuild.ToString();
						iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
					}

					DateTime Shpment_dt;
					if((drEach["shpt_manifest_datetime"]!= null) && (!drEach["shpt_manifest_datetime"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						Shpment_dt = (DateTime) drEach["shpt_manifest_datetime"];
					}
					else
					{
						Shpment_dt = new DateTime();
					}

					if (iBooking_no != preShipBkNo)
					{
						//Phase2 - J02
						DataSet dsDispatch = DispatchTrackingMgrDAL.QueryDispatchForShipmentTracking(strAppID,strEnterpriseID,
							iBooking_no, 0, 0,dbCon,dbCmd).ds;

						if(dsDispatch.Tables[0].Rows.Count > 0)
						{
							foreach(DataRow tmpDr in dsDispatch.Tables[0].Rows)
							{
							

								//Inserting data into shipment_tracking form Dispatch
								strBuild = new StringBuilder();
								strBuild.Append ("insert into shipment_tracking (applicationid,enterpriseid,consignment_no,tracking_datetime,booking_no,");
								strBuild.Append("status_code,exception_code,person_incharge, remark, deleted, delete_remark, last_userid, last_updated, invoiceable, location) ");
								strBuild.Append(" values('"+strAppID+"',"+"'"+strEnterpriseID+"',");
								if((drEach["consignment_no"]!= null) && (!drEach["consignment_no"].GetType().Equals(System.Type.GetType("System.DBNull"))))
								{
									String strConsgNo = (String) drEach["consignment_no"];
									strBuild.Append("'");
									strBuild.Append(strConsgNo);
									strBuild.Append("'");
								}
								else
								{
									strBuild.Append("null");
								}
								strBuild.Append(",");


								if((tmpDr["tracking_datetime"]!= null) && (!tmpDr["tracking_datetime"].GetType().Equals(System.Type.GetType("System.DBNull"))))
								{
									DateTime dttrackingDatetime= (DateTime)tmpDr["tracking_datetime"];
									strBuild.Append(Utility.DateFormat(strAppID,strEnterpriseID,dttrackingDatetime,DTFormat.DateTime));
								}
								else
								{
									strBuild.Append("null");
								}
								strBuild.Append(",");
								strBuild.Append(iBooking_no);
								strBuild.Append(",");

								if((tmpDr["status_code"]!= null) && (!tmpDr["status_code"].GetType().Equals(System.Type.GetType("System.DBNull"))))
								{
									String strStatusCode = (String) tmpDr["status_code"];
									strBuild.Append("'");
									strBuild.Append(strStatusCode);	
									strBuild.Append("'");
								}
								else
								{
									strBuild.Append("null");
								}
								strBuild.Append(",");

								if((tmpDr["exception_code"]!= null) && (!tmpDr["exception_code"].GetType().Equals(System.Type.GetType("System.DBNull"))))
								{
									String strExceptionCode = (String) tmpDr["exception_code"];
									strBuild.Append("'");
									strBuild.Append(strExceptionCode);	
									strBuild.Append("'");
								}
								else
								{
									strBuild.Append("null");
								}
								strBuild.Append(",");
				
								if((tmpDr["person_incharge"]!= null) && (!tmpDr["person_incharge"].GetType().Equals(System.Type.GetType("System.DBNull"))))
								{
									String strPersonIncharge = (String) tmpDr["person_incharge"];
									strBuild.Append("N'");
									strBuild.Append(Utility.ReplaceSingleQuote(strPersonIncharge));	
									strBuild.Append("'");
								}
								else
								{
									strBuild.Append("null");
								}
								strBuild.Append(",");

								if((tmpDr["remark"]!= null) && (!tmpDr["remark"].GetType().Equals(System.Type.GetType("System.DBNull"))))
								{
									String strRemark = (String) tmpDr["remark"];
									strBuild.Append("N'");
									strBuild.Append(Utility.ReplaceSingleQuote(strRemark));	
									strBuild.Append("'");
								}
								else
								{
									strBuild.Append("null");
								}
								strBuild.Append(",");

								if((tmpDr["deleted"]!= null) && (!tmpDr["deleted"].GetType().Equals(System.Type.GetType("System.DBNull"))))
								{
									String strDeleted = (String) tmpDr["deleted"];
									strBuild.Append("'");
									strBuild.Append(strDeleted);	
									strBuild.Append("'");
								}
								else
								{
									strBuild.Append("'N'");
								}
								strBuild.Append(",");

								if((tmpDr["delete_remark"]!= null) && (!tmpDr["delete_remark"].GetType().Equals(System.Type.GetType("System.DBNull"))))
								{
									String strDeletedRem = (String) tmpDr["delete_remark"];
									strBuild.Append("N'");
									strBuild.Append(Utility.ReplaceSingleQuote(strDeletedRem));	
									strBuild.Append("'");
								}
								else
								{
									strBuild.Append("null");
								}
								strBuild.Append(",");

								if((tmpDr["last_userid"]!= null) && (!tmpDr["last_userid"].GetType().Equals(System.Type.GetType("System.DBNull"))))
								{
									String strLastUserID = (String) tmpDr["last_userid"];
									strBuild.Append("'");
									strBuild.Append(strLastUserID);	
									strBuild.Append("'");

								}
								else
								{
									strBuild.Append("null");
								}
								strBuild.Append(",");

								if((tmpDr["last_updated"]!= null) && (!tmpDr["last_updated"].GetType().Equals(System.Type.GetType("System.DBNull"))))
								{
									DateTime dtLastUpdatedDateTime = (DateTime) tmpDr["last_updated"];
									strBuild.Append(Utility.DateFormat(strAppID,strEnterpriseID,dtLastUpdatedDateTime,DTFormat.DateTime));
								}
								else
								{
									strBuild.Append("null");
								}

								strBuild.Append(",'N'");
								strBuild.Append(",");
								if((tmpDr["location"]!= null) && (!tmpDr["location"].GetType().Equals(System.Type.GetType("System.DBNull"))))
								{
									String strlocation = (String) tmpDr["location"];
									strBuild.Append("'");
									strBuild.Append(strlocation);
									strBuild.Append("'");
								}
								else
								{
									strBuild.Append("null");
								}
								strBuild.Append(")");

								//Modified by GwanG on 05Feb08							
								dbCmd.CommandText = strBuild.ToString();
								iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);		
							

								Logger.LogDebugInfo("module1","DomesticShipmentMgrDAL","AddDomesticShipment","INF008",iRowsAffected + " rows inserted into shipment_tracking table");
							}
						}	
						//Phase2 - J02
					}


					while(com.ties.DAL.ShipmentUpdateMgrDAL.IsDupTrackDT_DISP(strAppID,strEnterpriseID,Shpment_dt,iBooking_no,dbCon,dbCmd))
					{Shpment_dt = Shpment_dt.AddMinutes(1);}
					// End Added
					Utility.CreateAutoConsignmentHistory(strAppID,strEnterpriseID,2,null,iBooking_no,Utility.ReplaceSingleQuote(drEach["consignment_no"].ToString()),
						Shpment_dt,userID,ref dbCmd,ref dbCon);

					//Add by X Mar 25 10 - Remove wrong Booking No of this Con
					strBuild = new StringBuilder();			
					strBuild.Append("delete from shipment_tracking ");
					strBuild.Append("where applicationid = '");
					strBuild.Append(strAppID);
					strBuild.Append("' and enterpriseid = '");
					strBuild.Append(strEnterpriseID);
					strBuild.Append("' and consignment_no = '");
					strBuild.Append(Utility.ReplaceSingleQuote(drEach["consignment_no"].ToString()));
					strBuild.Append("' and booking_no <> " + iBooking_no);

					dbCmd.CommandText = strBuild.ToString();
					iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
					//End Add by X Mar 25 10

					//#################### Clear Import Consignment Data ####################
					//get exist consignment record from the consignment table
					strBuild = new StringBuilder();
					strBuild.Append("SELECT recordid " );
					strBuild.Append("FROM ImportedConsignments ");
					strBuild.Append("WHERE consignment_no = '" + drEach["consignment_no"].ToString() + "' ");
					strBuild.Append("AND userid = '" + userID + "' ");

					dbCmd.CommandText = strBuild.ToString();
					DataSet dsConsignment = (DataSet)dbCon.ExecuteQueryInTransaction(dbCmd, ReturnType.DataSetType);

					//delete audit of consignment record from the audit table
					foreach(DataRow dr in dsConsignment.Tables[0].Rows)
					{
						strBuild = new StringBuilder();
						strBuild.Append("DELETE " );
						strBuild.Append("FROM ImportedConsignmentsAuditFailures ");
						strBuild.Append("WHERE recordid = '" + dr["recordid"].ToString() + "' ");
						strBuild.Append("AND userid = '" + userID + "' ");
						strBuild.Append("AND recordtype = 'Consignment' ");

						dbCmd.CommandText = strBuild.ToString();
						iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
					}

					//delete consignment record from the consignment table
					strBuild = new StringBuilder();
					strBuild.Append("DELETE " );
					strBuild.Append("FROM ImportedConsignments ");
					strBuild.Append("WHERE consignment_no = '" + drEach["consignment_no"].ToString() + "' ");
					strBuild.Append("AND userid = '" + userID + "' ");

					dbCmd.CommandText = strBuild.ToString();
					iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);

					//get exist package records from the package table
					strBuild = new StringBuilder();
					strBuild.Append("SELECT recordid " );
					strBuild.Append("FROM ImportedConsignmentsPkgs ");
					strBuild.Append("WHERE consignment_no = '" + drEach["consignment_no"].ToString() + "' ");
					strBuild.Append("AND userid = '" + userID + "' ");

					dbCmd.CommandText = strBuild.ToString();
					DataSet dsPackage = (DataSet)dbCon.ExecuteQueryInTransaction(dbCmd, ReturnType.DataSetType);

					//delete audit of package record from the audit table
					foreach(DataRow dr in dsPackage.Tables[0].Rows)
					{
						strBuild = new StringBuilder();
						strBuild.Append("DELETE " );
						strBuild.Append("FROM ImportedConsignmentsAuditFailures ");
						strBuild.Append("WHERE recordid = '" + dr["recordid"].ToString() + "' ");
						strBuild.Append("AND userid = '" + userID + "' ");
						strBuild.Append("AND recordtype = 'Package' ");

						dbCmd.CommandText = strBuild.ToString();
						iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
					}

					//delete package record from the package table
					strBuild = new StringBuilder();
					strBuild.Append("DELETE " );
					strBuild.Append("FROM ImportedConsignmentsPkgs ");
					strBuild.Append("WHERE consignment_no = '" + drEach["consignment_no"].ToString() + "' ");
					strBuild.Append("AND userid = '" + userID + "' ");

					dbCmd.CommandText = strBuild.ToString();
					iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);

					//#################### Clear Import Consignment Data ####################
				}

				transactionApp.Commit();

				CustomerCreditused ocustused = new CustomerCreditused();
				ocustused.applicationid = strAppID;
				ocustused.enterpriseid = strEnterpriseID;

				foreach(DataRow drCU in dtCreditUsed.Rows)
				{
					ocustused.consignment_no = drCU["ConsignmentNo"].ToString();
					ocustused.cusid = drCU["CustID"].ToString();
					ocustused.transcode = "UPD";
					ocustused.amount = decimal.Parse(drCU["CreditAmount"].ToString());
					ocustused.updateCreditused();
				}

				Logger.LogDebugInfo("DomesticShipmentMgrDAL","UpdateDomesticShp","INF008","App db insert transaction committed.");

			}
			catch(ApplicationException appException)
			{
				try
				{
					transactionApp.Rollback();
					Logger.LogDebugInfo("DomesticShipmentMgrDAL","UpdateDomesticShp","INF009","App db insert transaction rolled back.");

				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("DomesticShipmentMgrDAL","UpdateDomesticShp","ERR007","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}


				Logger.LogTraceError("DomesticShipmentMgrDAL","UpdateDomesticShp","ERR008","Insert Transaction Failed : "+ appException.Message.ToString());
				throw new ApplicationException("Error adding Shipment : "+appException.Message,appException);
			}
			catch(Exception exception)
			{
				try
				{
					transactionApp.Rollback();
					Logger.LogDebugInfo("DomesticShipmentMgrDAL","UpdateDomesticShp","INF010","App db insert transaction rolled back.");

				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("DomesticShipmentMgrDAL","UpdateDomesticShp","ERR009","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}

				Logger.LogTraceError("DomesticShipmentMgrDAL","UpdateDomesticShp","ERR010","Insert Transaction Failed : "+ exception.Message.ToString());
				throw new ApplicationException("Error adding Shipment ",exception);
			}
			finally
			{
				if(conApp != null)
				{
					conApp.Close();
				}
			}
			return iRowsAffected;
		}


		/// <summary>
		/// This method deletes records from shipment, shipment_pkg, shipment_vas, shipment_tracking tables.
		/// </summary>
		/// <param name="strAppID">Allication ID</param>
		/// <param name="strEnterpriseID">Enterprise ID</param>
		/// <param name="bookingNO">Booking Number</param>
		/// <param name="strDeleteStatusList">Valid list of Status Codes in String array</param>
		/// <returns>int</returns>
		 
		
		//Add By Tom Mar 10,2010
		public static void Populate(String strAppID,String strEnterpriseID,string consignmentNo)
		{
			DbConnection dbCon = null;
			IDbCommand dbCmd = null;

			dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			
			StringBuilder strQry = new StringBuilder();
			strQry = strQry.Append("select last_status_code,Route_Code,payer_name,booking_datetime,");
			strQry.Append("shpt_manifest_datetime,act_pickup_datetime,act_delivery_date,cod_amount,total_rated_amount from shipment  ");
			strQry.Append(" where consignment_no = '");
			strQry.Append(consignmentNo);
			strQry.Append("'");
			//strQry.Append(Utility.ReplaceSingleQuote(serviceCode));

			dbCmd = dbCon.CreateCommand(strQry.ToString(),CommandType.Text);

			DataSet dsCon = null;
			dsCon =(DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);

			if(dsCon.Tables[0].Rows.Count >0)
			{
				status = dsCon.Tables[0].Rows[0]["last_status_code"].ToString();
				route_code = dsCon.Tables[0].Rows[0]["Route_Code"].ToString();
				payer_name = dsCon.Tables[0].Rows[0]["Payer_Name"].ToString();
				try
				{
					dtBookingDateTime = (DateTime)dsCon.Tables[0].Rows[0]["booking_datetime"];
					BD = dtBookingDateTime.ToString("dd/MM/yyyy HH:mm");
				}
				catch(Exception ex)
				{
					BD = "Null";
				}
				try
				{
					dtManifestDateTime = (DateTime)dsCon.Tables[0].Rows[0]["shpt_manifest_datetime"];
					MD = dtManifestDateTime.ToString("dd/MM/yyyy HH:mm");
				}
				catch(Exception ex)
				{
					MD = "Null";
				}
				try
				{
					dtPickupDateTime = (DateTime)dsCon.Tables[0].Rows[0]["act_pickup_datetime"];
					PD = dtPickupDateTime.ToString("dd/MM/yyyy HH:mm");
				
				}
				catch(Exception ex)
				{
					PD = "Null";
				}
				try
				{
					dtDeliveryDateTime = (DateTime)dsCon.Tables[0].Rows[0]["act_delivery_date"];
					DD = dtDeliveryDateTime.ToString("dd/MM/yyyy HH:mm");
				}
				catch(Exception ex)
				{
					DD = "Null";
				}
				CA = dsCon.Tables[0].Rows[0]["cod_amount"].ToString();
				TRA = dsCon.Tables[0].Rows[0]["total_rated_amount"].ToString();
			}
			else
			{
				status = "";
				route_code = "";
				payer_name  = "";
				BD = "";
				MD = "";
				PD = "";
				DD = "";
				CA = "";
				TRA = "";
			}
		}
		
		public static void Populate2(String strAppID,String strEnterpriseID,string consignmentNo)
		{
			DbConnection dbCon = null;
			IDbCommand dbCmd = null;

			dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			
			StringBuilder strQry = new StringBuilder();
			strQry = strQry.Append("select last_userid from shipment_tracking");
			strQry.Append(" where consignment_no = '");
			strQry.Append(consignmentNo);
			strQry.Append("' and status_code = 'MDE'");
			//strQry.Append(Utility.ReplaceSingleQuote(serviceCode));

			dbCmd = dbCon.CreateCommand(strQry.ToString(),CommandType.Text);

			DataSet dsCon = null;
			dsCon =(DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);

			if(dsCon.Tables[0].Rows.Count >0)
			{
				MDE_BY = dsCon.Tables[0].Rows[0]["last_userid"].ToString();
			}
			else
			{
				MDE_BY = "";
			}
		}

		public static int DeleteDomesticShp(String strAppID,String strEnterpriseID,int bookingNO,string consignmentNo, String[] strDeleteStatusList)
		{


			IDbCommand dbCmd = null;
			int iRowsAffected = 0;
			StringBuilder strBuild = null;
			
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("DomesticShipmentManager","DeleteDomesticShp","ERR001","DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null",null);
			}

			IDbConnection conApp = dbCon.GetConnection();
			if(conApp == null)
			{
				Logger.LogTraceError("DomesticShipmentMgrDAL","DeleteDomesticShp","ERR002","conApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			try
			{
				dbCon.OpenConnection(ref conApp);
			}
			catch(ApplicationException appException)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("DomesticShipmentMgrDAL","DeleteDomesticShp","ERR003","Error opening connection : "+ appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ",appException);
			}
			catch(Exception exception)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("DomesticShipmentMgrDAL","DeleteDomesticShp","ERR004","Error opening connection : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}
			IDbTransaction transactionApp = dbCon.BeginTransaction(conApp,IsolationLevel.ReadCommitted);

			if(transactionApp == null)
			{
				Logger.LogTraceError("DomesticShipmentMgrDAL","DeleteDomesticShp","ERR005","transactionApp is null");
				throw new ApplicationException("Failed Begining a App Transaction..",null);
			}
			try
			{

				//Delete Shipment Audit Trial by X JAN 11 08
				strBuild = new StringBuilder();
				strBuild.Append("delete from Shipment_Audit_Trial where applicationid = '");
				strBuild.Append(strAppID+"' and enterpriseid = '");
				strBuild.Append(strEnterpriseID);
				strBuild.Append("' and booking_no = ");
				strBuild.Append(bookingNO);
				strBuild.Append(" and consignment_no = '");
				strBuild.Append(consignmentNo+"'");
				

				dbCmd = dbCon.CreateCommand(strBuild.ToString(),CommandType.Text);
				dbCmd.Connection = conApp;
				dbCmd.Transaction = transactionApp;

				iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
				Logger.LogDebugInfo("DomesticShipmentMgrDAL","DeleteDomesticShp","INF000",iRowsAffected + " rows deleted from Shipment_Audit_Trial table");

				//delete from the shipment_pkg table
				strBuild = new StringBuilder();
				strBuild.Append("delete from shipment_pkg where applicationid = '");
				strBuild.Append(strAppID+"' and enterpriseid = '");
				strBuild.Append(strEnterpriseID);
				strBuild.Append("' and booking_no = ");
				strBuild.Append(bookingNO);
				strBuild.Append(" and consignment_no = '");
				strBuild.Append(consignmentNo+"'");
				

				dbCmd = dbCon.CreateCommand(strBuild.ToString(),CommandType.Text);
				dbCmd.Connection = conApp;
				dbCmd.Transaction = transactionApp;

				iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);

				strBuild = new StringBuilder();
				strBuild.Append("delete from shipment_pkg_box where applicationid = '");
				strBuild.Append(strAppID+"' and enterpriseid = '");
				strBuild.Append(strEnterpriseID);
				strBuild.Append("' and booking_no = ");
				strBuild.Append(bookingNO);
				strBuild.Append(" and consignment_no = '");
				strBuild.Append(consignmentNo+"'");
				dbCmd = dbCon.CreateCommand(strBuild.ToString(),CommandType.Text);
				dbCmd.Connection = conApp;
				dbCmd.Transaction = transactionApp;
				iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);

				Logger.LogDebugInfo("DomesticShipmentMgrDAL","DeleteDomesticShp","INF001",iRowsAffected + " rows deleted from Shipment_pkg table");

				//delete from the shipment_vas table
				strBuild = new StringBuilder();
				strBuild.Append("delete from shipment_vas where applicationid = '");
				strBuild.Append(strAppID+"' and enterpriseid = '");
				strBuild.Append(strEnterpriseID);
				strBuild.Append("' and booking_no = ");
				strBuild.Append(bookingNO);
				strBuild.Append(" and consignment_no = '");
				strBuild.Append(consignmentNo+"'");
				
				dbCmd.CommandText = strBuild.ToString();
				iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
				Logger.LogDebugInfo("DomesticShipmentMgrDAL","DeleteDomesticShp","INF002",iRowsAffected + " rows deleted from Shipment_VAS table");


				//delete from the Shipment_tracking table 
				strBuild = new StringBuilder();
				strBuild.Append("delete from shipment_tracking  ");
				strBuild.Append(" where applicationid = '");
				strBuild.Append(strAppID+"' and enterpriseid = '");
				strBuild.Append(strEnterpriseID);
				strBuild.Append("' and booking_no = ");
				strBuild.Append(bookingNO);
				strBuild.Append(" and consignment_no = '");
				strBuild.Append(consignmentNo+"'");
				
				dbCmd.CommandText = strBuild.ToString();
				iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
				Logger.LogDebugInfo("DomesticShipmentMgrDAL","DeleteDomesticShp","INF003",iRowsAffected + " rows deleted from shipment_tracking table");


				//delete from shipment table
				strBuild = new StringBuilder();
				strBuild.Append("delete from shipment where applicationid = '");
				strBuild.Append(strAppID+"' and enterpriseid = '");
				strBuild.Append(strEnterpriseID);
				strBuild.Append("' and booking_no = ");
				strBuild.Append(bookingNO);
				strBuild.Append(" and consignment_no = '");
				strBuild.Append(consignmentNo+"'");
				
				//get shipment info before commit delete data
				CustomerCreditused ocustused = new CustomerCreditused();
				ocustused.consignment_no = consignmentNo;
				ocustused.applicationid = strAppID;
				ocustused.enterpriseid = strEnterpriseID;
				//ocustused.amount = d_total_rated_amount;
				ocustused.bookingNo = bookingNO.ToString();
				//	ocustused.updated_by = "";
				ocustused.transcode = "CSD";
				ocustused.invoice_no = "";
				
				if(strDeleteStatusList!=null)
				{
					strBuild.Append(" and last_status_code in (");

					String strTemp = "";
					int iCnt = strDeleteStatusList.Length;
					for(int i=0; i<iCnt; i++)
					{
						strTemp +=  "'"+strDeleteStatusList[i]+"'" ;
						if(i < (iCnt-1))
						{
							strTemp += ",";
						}
						else
						{
							strTemp += ")";
						}
					}
					strBuild.Append(strTemp);
				}
				

				dbCmd.CommandText = strBuild.ToString();
				iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
				Logger.LogDebugInfo("DomesticShipmentMgrDAL","DeleteDomesticShp","INF004",iRowsAffected + " rows deleted from Shipment_VAS table");
				transactionApp.Commit();
				Logger.LogDebugInfo("DomesticShipmentMgrDAL","DeleteDomesticShp","INF005","App db insert transaction committed.");

			}
			catch(ApplicationException appException)
			{
				try
				{
					transactionApp.Rollback();
					Logger.LogDebugInfo("DomesticShipmentMgrDAL","DeleteDomesticShp","INF006","App db insert transaction rolled back.");

				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("DomesticShipmentMgrDAL","DeleteDomesticShp","ERR006","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}


				Logger.LogTraceError("DomesticShipmentMgrDAL","DeleteDomesticShp","ERR007","Insert Transaction Failed : "+ appException.Message.ToString());
				throw new ApplicationException("Error deleting Shipment ",appException);
			}
			catch(Exception exception)
			{
				try
				{
					transactionApp.Rollback();
					Logger.LogDebugInfo("DomesticShipmentMgrDAL","UpdateDomesticShp","INF007","App db insert transaction rolled back.");

				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("DomesticShipmentMgrDAL","UpdateDomesticShp","ERR008","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}

				Logger.LogTraceError("DomesticShipmentMgrDAL","UpdateDomesticShp","ERR009","Insert Transaction Failed : "+ exception.Message.ToString());
				throw new ApplicationException("Error deleting Shipment ",exception);
			}
			finally
			{
				if(conApp != null)
				{
					conApp.Close();
				}
			}
			return iRowsAffected;

		}

		public static int DeleteDomesticShp(String strAppID,String strEnterpriseID,int bookingNO,string consignmentNo, String[] strDeleteStatusList, string USERID)
		{
			Populate(strAppID,strEnterpriseID,consignmentNo);
			Populate2(strAppID,strEnterpriseID,consignmentNo);
			IDbCommand dbCmd = null;
			int iRowsAffected = 0;
			StringBuilder strBuild = null;
			
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("DomesticShipmentManager","DeleteDomesticShp","ERR001","DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null",null);
			}

			IDbConnection conApp = dbCon.GetConnection();
			if(conApp == null)
			{
				Logger.LogTraceError("DomesticShipmentMgrDAL","DeleteDomesticShp","ERR002","conApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			try
			{
				dbCon.OpenConnection(ref conApp);
			}
			catch(ApplicationException appException)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("DomesticShipmentMgrDAL","DeleteDomesticShp","ERR003","Error opening connection : "+ appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ",appException);
			}
			catch(Exception exception)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("DomesticShipmentMgrDAL","DeleteDomesticShp","ERR004","Error opening connection : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}
			IDbTransaction transactionApp = dbCon.BeginTransaction(conApp,IsolationLevel.ReadCommitted);

			if(transactionApp == null)
			{
				Logger.LogTraceError("DomesticShipmentMgrDAL","DeleteDomesticShp","ERR005","transactionApp is null");
				throw new ApplicationException("Failed Begining a App Transaction..",null);
			}
		
			//get shipment info before commit delete data
			CustomerCreditused ocustused = new CustomerCreditused();
			ocustused.consignment_no = consignmentNo;
			ocustused.applicationid = strAppID;
			ocustused.enterpriseid = strEnterpriseID;
			//ocustused.amount = d_total_rated_amount;
			ocustused.bookingNo = bookingNO.ToString();
			//	ocustused.updated_by = "";
			ocustused.transcode = "CSD";
			ocustused.invoice_no = "";
				
			DataTable dt = ocustused.getinfoShipment();
			try
			{

				//Delete Shipment Audit Trial by X JAN 11 08
				strBuild = new StringBuilder();
				strBuild.Append("delete from Shipment_Audit_Trial where applicationid = '");
				strBuild.Append(strAppID+"' and enterpriseid = '");
				strBuild.Append(strEnterpriseID);
				strBuild.Append("' and booking_no = ");
				strBuild.Append(bookingNO);
				strBuild.Append(" and consignment_no = '");
				strBuild.Append(consignmentNo+"'");
				

				dbCmd = dbCon.CreateCommand(strBuild.ToString(),CommandType.Text);
				dbCmd.Connection = conApp;
				dbCmd.Transaction = transactionApp;

				iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
				Logger.LogDebugInfo("DomesticShipmentMgrDAL","DeleteDomesticShp","INF000",iRowsAffected + " rows deleted from Shipment_Audit_Trial table");

				//delete from the shipment_pkg table
				strBuild = new StringBuilder();
				strBuild.Append("delete from shipment_pkg where applicationid = '");
				strBuild.Append(strAppID+"' and enterpriseid = '");
				strBuild.Append(strEnterpriseID);
				strBuild.Append("' and booking_no = ");
				strBuild.Append(bookingNO);
				strBuild.Append(" and consignment_no = '");
				strBuild.Append(consignmentNo+"'");
				

				dbCmd = dbCon.CreateCommand(strBuild.ToString(),CommandType.Text);
				dbCmd.Connection = conApp;
				dbCmd.Transaction = transactionApp;

				iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);

				strBuild = new StringBuilder();
				strBuild.Append("delete from shipment_pkg where applicationid = '");
				strBuild.Append(strAppID+"' and enterpriseid = '");
				strBuild.Append(strEnterpriseID);
				strBuild.Append("' and booking_no = ");
				strBuild.Append(bookingNO);
				strBuild.Append(" and consignment_no = '");
				strBuild.Append(consignmentNo+"'");
				

				dbCmd = dbCon.CreateCommand(strBuild.ToString(),CommandType.Text);
				dbCmd.Connection = conApp;
				dbCmd.Transaction = transactionApp;
				iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
				Logger.LogDebugInfo("DomesticShipmentMgrDAL","DeleteDomesticShp","INF001",iRowsAffected + " rows deleted from Shipment_pkg table");

				//delete from the shipment_vas table
				strBuild = new StringBuilder();
				strBuild.Append("delete from shipment_vas where applicationid = '");
				strBuild.Append(strAppID+"' and enterpriseid = '");
				strBuild.Append(strEnterpriseID);
				strBuild.Append("' and booking_no = ");
				strBuild.Append(bookingNO);
				strBuild.Append(" and consignment_no = '");
				strBuild.Append(consignmentNo+"'");
				
				dbCmd.CommandText = strBuild.ToString();
				iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
				Logger.LogDebugInfo("DomesticShipmentMgrDAL","DeleteDomesticShp","INF002",iRowsAffected + " rows deleted from Shipment_VAS table");


				//delete from the Shipment_tracking table 
				strBuild = new StringBuilder();
				strBuild.Append("delete from shipment_tracking  ");
				strBuild.Append(" where applicationid = '");
				strBuild.Append(strAppID+"' and enterpriseid = '");
				strBuild.Append(strEnterpriseID);
				strBuild.Append("' and booking_no = ");
				strBuild.Append(bookingNO);
				strBuild.Append(" and consignment_no = '");
				strBuild.Append(consignmentNo+"'");
				
				dbCmd.CommandText = strBuild.ToString();
				iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
				Logger.LogDebugInfo("DomesticShipmentMgrDAL","DeleteDomesticShp","INF003",iRowsAffected + " rows deleted from shipment_tracking table");


				//delete from shipment table
				strBuild = new StringBuilder();
				strBuild.Append("delete from shipment where applicationid = '");
				strBuild.Append(strAppID+"' and enterpriseid = '");
				strBuild.Append(strEnterpriseID);
				strBuild.Append("' and booking_no = ");
				strBuild.Append(bookingNO);
				strBuild.Append(" and consignment_no = '");
				strBuild.Append(consignmentNo+"'");
				
				
				if(strDeleteStatusList!=null)
				{
					strBuild.Append(" and last_status_code in (");

					String strTemp = "";
					int iCnt = strDeleteStatusList.Length;
					for(int i=0; i<iCnt; i++)
					{
						strTemp +=  "'"+strDeleteStatusList[i]+"'" ;
						if(i < (iCnt-1))
						{
							strTemp += ",";
						}
						else
						{
							strTemp += ")";
						}
					}
					strBuild.Append(strTemp);
				}
				

				dbCmd.CommandText = strBuild.ToString();
				iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
				Logger.LogDebugInfo("DomesticShipmentMgrDAL","DeleteDomesticShp","INF004",iRowsAffected + " rows deleted from Shipment_VAS table");

				//Added By Tom Mar 10,10
				//delete from the Delivery_Manifest_Detail table 
				strBuild = new StringBuilder();
				strBuild.Append("delete from Delivery_Manifest_Detail  ");
				strBuild.Append(" where applicationid = '");
				strBuild.Append(strAppID+"' and enterpriseid = '");
				strBuild.Append(strEnterpriseID);
				strBuild.Append("' and consignment_no = '");
				strBuild.Append(consignmentNo+"'");
				
				dbCmd.CommandText = strBuild.ToString();
				iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
				Logger.LogDebugInfo("DomesticShipmentMgrDAL","DeleteDomesticShp","INF003",iRowsAffected + " rows deleted from shipment_tracking table");
				
				string strInsert = "insert into Consignment_Delete(consignment_no,Delete_Date,User_delete,Reason_code,booking_no,last_status_code,Route_code,Payer_name,Remark,booking_datetime,shpt_manifest_datetime,act_pickup_datetime,act_delivery_date,cod_amount,total_rated_amount,mde_by) ";
				strInsert +=" values('"+ consignmentNo +"',Getdate(),'"+ USERID +"','DE01','"+ bookingNO +"','"+ status +"','"+ route_code +"','"+ payer_name +"','Deleted From Domestic shipment Page',null,null,null,null,null,null,'" + MDE_BY + "')";
				IDbCommand dbCmdI = null;
				dbCmdI = dbCon.CreateCommand(strInsert, CommandType.Text);
				dbCmdI.Connection = conApp;
				dbCon.ExecuteNonQuery(dbCmdI);
				
				string strUpdate = "update Consignment_Delete set booking_datetime = ";
				if(BD != "Null")
					strUpdate += "'" + dtBookingDateTime+ "'";
				else
					strUpdate += "null";
				strUpdate += ", shpt_manifest_datetime = ";
				if(MD != "Null")
					strUpdate += "'" + dtManifestDateTime+ "'";
				else
					strUpdate += "null";
				strUpdate += ", act_pickup_datetime = ";
				if(PD != "Null")
					strUpdate += "'" + dtPickupDateTime+ "'";
				else
					strUpdate += "null";
				strUpdate += ", act_delivery_date = ";
				if(DD != "Null")
					strUpdate += "'" + dtDeliveryDateTime+ "'";
				else
					strUpdate += "null";
				strUpdate += ", cod_amount = " + CA;
				strUpdate += ", total_rated_amount = " + TRA;
				strUpdate += " where consignment_no ='" + consignmentNo + "'";
				
				IDbCommand dbCmdU = null;
				dbCmdU = dbCon.CreateCommand(strUpdate, CommandType.Text);
				dbCmdU.Connection = conApp;
				dbCon.ExecuteNonQuery(dbCmdU);
				
				//End Added By Tom Mar 10,10

				transactionApp.Commit();


				if(dt.Rows.Count >0)
				{
					ocustused.amount =  decimal.Parse(dt.Rows[0]["total_rated_amount"].ToString()) * -1;
					ocustused.cusid = dt.Rows[0]["payerid"].ToString();
					ocustused.updateCreditused();
				}

				Logger.LogDebugInfo("DomesticShipmentMgrDAL","DeleteDomesticShp","INF005","App db insert transaction committed.");

 
			}
			catch(ApplicationException appException)
			{
				try
				{
					transactionApp.Rollback();
					Logger.LogDebugInfo("DomesticShipmentMgrDAL","DeleteDomesticShp","INF006","App db insert transaction rolled back.");

				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("DomesticShipmentMgrDAL","DeleteDomesticShp","ERR006","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}


				Logger.LogTraceError("DomesticShipmentMgrDAL","DeleteDomesticShp","ERR007","Insert Transaction Failed : "+ appException.Message.ToString());
				throw new ApplicationException("Error deleting Shipment ",appException);
			}
			catch(Exception exception)
			{
				try
				{
					transactionApp.Rollback();
					Logger.LogDebugInfo("DomesticShipmentMgrDAL","UpdateDomesticShp","INF007","App db insert transaction rolled back.");

				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("DomesticShipmentMgrDAL","UpdateDomesticShp","ERR008","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}

				Logger.LogTraceError("DomesticShipmentMgrDAL","UpdateDomesticShp","ERR009","Insert Transaction Failed : "+ exception.Message.ToString());
				throw new ApplicationException("Error deleting Shipment ",exception);
			}
			finally
			{
				if(conApp != null)
				{
					conApp.Close();
				}
			}
			return iRowsAffected;

		}

		/// <summary>
		/// This method returns the SessionDS object.
		/// </summary>
		/// <returns>SessionDS</returns>
		public static SessionDS GetDomShipSessionDS()
		{
			DataSet dsDomesticShipment = GetEmptyDomesticShipDS();

			SessionDS sessionDS = new SessionDS();
			sessionDS.ds = dsDomesticShipment;
			
			sessionDS.DataSetRecSize = dsDomesticShipment.Tables[0].Rows.Count;
			sessionDS.QueryResultMaxSize = sessionDS.DataSetRecSize;
			
			return sessionDS;
		}

		/// <summary>
		/// This method gets the records from the shipment table.
		/// </summary>
		/// <param name="strAppID">Application ID</param>
		/// <param name="strEnterpriseID">Enterprise ID</param>
		/// <param name="iCurrent">Current Record Number</param>
		/// <param name="iDSRecSize">Record Size</param>
		/// <param name="dsQueryShipment">Dataset</param>
		/// <returns>SessionDS</returns>
		public static SessionDS GetShipmentData(String strAppID, String strEnterpriseID, int iCurrent, int iDSRecSize,DataSet dsQueryShipment)
		{
			SessionDS sessionDS = new SessionDS(); 
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon==null)
			{
				Logger.LogTraceError("DomesticShipmentMgrDAL","GetShipmentData","ERR001","DbConnectionis null!!");
				return sessionDS;
			}
			DataRow drEach = dsQueryShipment.Tables[0].Rows[0];

			StringBuilder strBuild = new StringBuilder();
			strBuild.Append("select *,dateadd(hh,6,booking_datetime) as BookingDateTimePlus6 from shipment where applicationid = '");
			strBuild.Append(strAppID);
			strBuild.Append("' and enterpriseid = '");
			strBuild.Append(strEnterpriseID+"'");
			
			if(Convert.ToDecimal(drEach["booking_no"]) > 0)
			{
				strBuild.Append(" and booking_no = ");
				strBuild.Append(Convert.ToInt32(drEach["booking_no"]));
			}
			
			if((drEach["consignment_no"]!= null) && (!drEach["consignment_no"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				String strConsgNo = (String) drEach["consignment_no"];
				strBuild.Append(" and consignment_no like ");
				//strBuild.Append("'%");   by sittichai  11/02/2008
				strBuild.Append("'"); 
				strBuild.Append(Utility.ReplaceSingleQuote(strConsgNo));
				strBuild.Append("%'");
			}

			if((drEach["ref_no"]!= null) && (!drEach["ref_no"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				String strRefNo = (String) drEach["ref_no"];
				strBuild.Append(" and ref_no like ");
				strBuild.Append("'%");
				strBuild.Append(strRefNo);
				strBuild.Append("%'");
			}

			//			if((drEach["booking_datetime"]!= null) && (!drEach["booking_datetime"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			//			{
			//				DateTime dtBookingDtTime = (DateTime)drEach["booking_datetime"];
			//				strBuild.Append(" and dtBookingDtTime = ");
			////				strBuild.Append("'");
			////				strBuild.Append(dtBookingDtTime);
			////				strBuild.Append("'");
			//				strBuild.Append(Utility.DateFormat(strAppID,strEnterpriseID,dtBookingDtTime,DTFormat.DateTime));
			//			}
			if((drEach["payerid"]!= null) && (!drEach["payerid"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				String strPayerID = (String) drEach["payerid"];
				strBuild.Append(" and payerid like ");
				strBuild.Append("'%");
				strBuild.Append(Utility.ReplaceSingleQuote(strPayerID));
				strBuild.Append("%'");
			}

			if((drEach["payer_type"]!= null) && (!drEach["payer_type"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				String strPayerType = (String) drEach["payer_type"];
				strBuild.Append(" and payer_type = ");
				strBuild.Append("'");
				strBuild.Append(strPayerType);
				strBuild.Append("'");
			}

			if((drEach["payer_name"]!= null) && (!drEach["payer_name"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				String strPayerName = (String) drEach["payer_name"];
				strBuild.Append(" and payer_name like N");
				strBuild.Append("'%");
				strBuild.Append(Utility.ReplaceSingleQuote(strPayerName));
				strBuild.Append("%'");
			}
			if((drEach["payer_address1"]!= null) && (!drEach["payer_address1"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				String strPayerAddress1 = (String) drEach["payer_address1"];
				strBuild.Append(" and payer_address1 like N");
				strBuild.Append("'%");
				strBuild.Append(Utility.ReplaceSingleQuote(strPayerAddress1));
				strBuild.Append("%'");
			}
			if((drEach["payer_address2"]!= null) && (!drEach["payer_address2"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				String strPayerAddress2 = (String) drEach["payer_address2"];
				strBuild.Append(" and payer_address2 like N");
				strBuild.Append("'%");
				strBuild.Append(Utility.ReplaceSingleQuote(strPayerAddress2));
				strBuild.Append("%'");
			}
			if((drEach["payer_zipcode"]!= null) && (!drEach["payer_zipcode"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				String strPayerZipCode = (String) drEach["payer_zipcode"];
				strBuild.Append(" and payer_zipcode like ");
				strBuild.Append("'%");
				strBuild.Append(Utility.ReplaceSingleQuote(strPayerZipCode));
				strBuild.Append("%'");
			}
			if((drEach["payer_country"]!= null) && (!drEach["payer_country"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				String strPayerCountry = (String) drEach["payer_country"];
				strBuild.Append(" and payer_country like ");
				strBuild.Append("'%");
				strBuild.Append(strPayerCountry);
				strBuild.Append("%'");
			}
			if((drEach["payer_telephone"]!= null) && (!drEach["payer_telephone"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				String strPayerTelephone = (String) drEach["payer_telephone"];
				strBuild.Append(" and payer_telephone like ");
				strBuild.Append("'%");
				strBuild.Append(Utility.ReplaceSingleQuote(strPayerTelephone));
				strBuild.Append("%'");
			}
			if((drEach["sender_name"]!= null) && (!drEach["sender_name"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				String strSenderName = (String) drEach["sender_name"];
				strBuild.Append(" and sender_name like N");
				strBuild.Append("'%");
				strBuild.Append(Utility.ReplaceSingleQuote(strSenderName));
				strBuild.Append("%'");
			}
			if((drEach["sender_address1"]!= null) && (!drEach["sender_address1"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				String strSenderAddress1 = (String) drEach["sender_address1"];
				strBuild.Append(" and sender_address1 like N");
				strBuild.Append("'%");
				strBuild.Append(Utility.ReplaceSingleQuote(strSenderAddress1));
				strBuild.Append("%'");
			}
			if((drEach["sender_address2"]!= null) && (!drEach["sender_address2"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				String strSenderAddress2 = (String) drEach["sender_address2"];
				strBuild.Append(" and sender_address2 like N");
				strBuild.Append("'%");
				strBuild.Append(Utility.ReplaceSingleQuote(strSenderAddress2));
				strBuild.Append("%'");
			}

			if((drEach["sender_zipcode"]!= null) && (!drEach["sender_zipcode"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				String strSenderZipCode = (String) drEach["sender_zipcode"];
				strBuild.Append(" and sender_zipcode like ");
				strBuild.Append("'%");
				strBuild.Append(Utility.ReplaceSingleQuote(strSenderZipCode));
				strBuild.Append("%'");
			}
			if((drEach["sender_country"]!= null) && (!drEach["sender_country"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				String strSenderCountry = (String) drEach["sender_country"];
				strBuild.Append(" and sender_country like ");
				strBuild.Append("'%");
				strBuild.Append(strSenderCountry);
				strBuild.Append("%'");
			}
			if((drEach["sender_telephone"]!= null) && (!drEach["sender_telephone"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				String strSenderTelephone = (String) drEach["sender_telephone"];
				strBuild.Append(" and sender_telephone like ");
				strBuild.Append("'%");
				strBuild.Append(Utility.ReplaceSingleQuote(strSenderTelephone));
				strBuild.Append("%'");
			}
			if((drEach["sender_fax"]!= null) && (!drEach["sender_fax"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				String strSenderFax = (String) drEach["sender_fax"];
				strBuild.Append(" and sender_fax like ");
				strBuild.Append("'%");
				strBuild.Append(Utility.ReplaceSingleQuote(strSenderFax));
				strBuild.Append("%'");
			}
			if((drEach["sender_contact_person"]!= null) && (!drEach["sender_contact_person"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				String strSenderContactPerson = (String) drEach["sender_contact_person"];
				strBuild.Append(" and sender_contact_person like N");
				strBuild.Append("'%");
				strBuild.Append(Utility.ReplaceSingleQuote(strSenderContactPerson));
				strBuild.Append("%'");
			}
			if((drEach["recipient_name"]!= null) && (!drEach["recipient_name"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				String strRecipientName = (String) drEach["recipient_name"];
				strBuild.Append(" and recipient_name like N");
				strBuild.Append("'%");
				strBuild.Append(Utility.ReplaceSingleQuote(strRecipientName));
				strBuild.Append("%'");
			}
			if((drEach["recipient_address1"]!= null) && (!drEach["recipient_address1"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				String strRecipientAddress1 = (String) drEach["recipient_address1"];
				strBuild.Append(" and recipient_address1 like N");
				strBuild.Append("'%");
				strBuild.Append(Utility.ReplaceSingleQuote(strRecipientAddress1));
				strBuild.Append("%'");
			}
			if((drEach["recipient_address2"]!= null) && (!drEach["recipient_address2"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				String strRecipientAddress2 = (String) drEach["recipient_address2"];
				strBuild.Append(" and recipient_address2 like N");
				strBuild.Append("'%");
				strBuild.Append(Utility.ReplaceSingleQuote(strRecipientAddress2));
				strBuild.Append("%'");
			}
			if((drEach["recipient_zipcode"]!= null) && (!drEach["recipient_zipcode"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				String strRecipientZipCode = (String) drEach["recipient_zipcode"];
				strBuild.Append(" and recipient_zipcode like ");
				strBuild.Append("'%");
				strBuild.Append(Utility.ReplaceSingleQuote(strRecipientZipCode));
				strBuild.Append("%'");
			}
			if((drEach["recipient_country"]!= null) && (!drEach["recipient_country"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				String strRecipientCountry = (String) drEach["recipient_country"];
				strBuild.Append(" and recipient_country like ");
				strBuild.Append("'%");
				strBuild.Append(strRecipientCountry);
				strBuild.Append("%'");
			}
			if((drEach["recipient_telephone"]!= null) && (!drEach["recipient_telephone"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				String strRecipientTelephone = (String) drEach["recipient_telephone"];
				strBuild.Append(" and recipient_telephone like ");
				strBuild.Append("'%");
				strBuild.Append(Utility.ReplaceSingleQuote(strRecipientTelephone));
				strBuild.Append("%'");
			}
			if((drEach["recipient_fax"]!= null) && (!drEach["recipient_fax"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				String strRecipientFax = (String) drEach["recipient_fax"];
				strBuild.Append(" and recipient_fax like ");
				strBuild.Append("'%");
				strBuild.Append(Utility.ReplaceSingleQuote(strRecipientFax));
				strBuild.Append("%'");
			}
			if((drEach["service_code"]!= null) && (!drEach["service_code"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				String strServiceCode = (String) drEach["service_code"];
				strBuild.Append(" and service_code like ");
				strBuild.Append("'%");
				strBuild.Append(strServiceCode);
				strBuild.Append("%'");
			}
			if((drEach["recipient_contact_person"]!= null) && (!drEach["recipient_contact_person"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				String strRecipientContactPerson = (String) drEach["recipient_contact_person"];
				strBuild.Append(" and recipient_contact_person like N");
				strBuild.Append("'%");
				strBuild.Append(Utility.ReplaceSingleQuote(strRecipientContactPerson));
				strBuild.Append("%'");
			}
			if((drEach["commodity_code"]!= null) && (!drEach["commodity_code"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				String strCommodityCode = (String) drEach["commodity_code"];
				strBuild.Append(" and commodity_code like ");
				strBuild.Append("'%");
				strBuild.Append(strCommodityCode);
				strBuild.Append("%'");
			}
			if((drEach["est_delivery_datetime"]!= null) && (!drEach["est_delivery_datetime"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				DateTime dtEstDeliveryDatetime = Convert.ToDateTime(drEach["est_delivery_datetime"]);
				strBuild.Append(" and est_delivery_datetime =");
				strBuild.Append(Utility.DateFormat(strAppID,strEnterpriseID,dtEstDeliveryDatetime,DTFormat.DateTime));
			}
			if((drEach["origin_state_code"]!= null) && (!drEach["origin_state_code"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				String strOriginStateCode = (String) drEach["origin_state_code"];
				strBuild.Append(" and origin_state_code like ");
				strBuild.Append("'%");
				strBuild.Append(strOriginStateCode);
				strBuild.Append("%'");
			}
			if((drEach["destination_state_code"]!= null) && (!drEach["destination_state_code"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				String strDestinationStateCode = (String) drEach["destination_state_code"];
				strBuild.Append(" and destination_state_code like ");
				strBuild.Append("'%");
				strBuild.Append(strDestinationStateCode);
				strBuild.Append("%'");
			}
			if((drEach["last_status_code"]!= null) && (!drEach["last_status_code"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				String strLastStatusCode = (String) drEach["last_status_code"];
				strBuild.Append(" and last_status_code like ");
				strBuild.Append("'%");
				strBuild.Append(strLastStatusCode);
				strBuild.Append("%'");
			}
			if((drEach["last_exception_code"]!= null) && (!drEach["last_exception_code"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				String strLastExceptionCode = (String) drEach["last_exception_code"];
				strBuild.Append(" and last_exception_code like ");
				strBuild.Append("'%");
				strBuild.Append(strLastExceptionCode);
				strBuild.Append("%'");
			}
			if((drEach["shpt_manifest_datetime"]!= null) && (!drEach["shpt_manifest_datetime"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				DateTime dtShptManifestDateTime = Convert.ToDateTime(drEach["shpt_manifest_datetime"]);
				strBuild.Append(" and shpt_manifest_datetime = ");
				strBuild.Append(Utility.DateFormat(strAppID,strEnterpriseID,dtShptManifestDateTime,DTFormat.DateTime));
			}
			if((drEach["delivery_manifested"]!= null) && (!drEach["delivery_manifested"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				String strDeliveryManifested = (String) drEach["delivery_manifested"];
				strBuild.Append(" and delivery_manifested like ");
				strBuild.Append("'%");
				strBuild.Append(strDeliveryManifested);
				strBuild.Append("%'");
			}
			if((drEach["route_code"]!= null) && (!drEach["route_code"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				String strRouteCode = (String) drEach["route_code"];
				strBuild.Append(" and route_code like ");
				strBuild.Append("'%");
				strBuild.Append(strRouteCode);
				strBuild.Append("%'");
			}
			if((drEach["payment_mode"]!= null) && (!drEach["payment_mode"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				String strPaymentMode = (String) drEach["payment_mode"];
				strBuild.Append(" and payment_mode = ");
				strBuild.Append("'");
				strBuild.Append(strPaymentMode);
				strBuild.Append("'");
			}
			if((drEach["payment_type"]!= null) && (!drEach["payment_type"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				String strPaymentType = (String) drEach["payment_type"];
				strBuild.Append(" and payment_type = ");
				strBuild.Append("'");
				strBuild.Append(strPaymentType);
				strBuild.Append("'");
			}
			if((drEach["return_pod_slip"]!= null) && (!drEach["return_pod_slip"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				String strPODSlip = (String) drEach["return_pod_slip"];
				if(strPODSlip == "Y")
				{
					strBuild.Append(" and return_pod_slip = ");
					strBuild.Append("'");
					strBuild.Append(strPODSlip);
					strBuild.Append("'");
				}
			}
			if((drEach["origin_state_code"]!= null) && (!drEach["origin_state_code"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				String strOriginCode = (String) drEach["origin_state_code"];
				strBuild.Append(" and origin_state_code like ");
				strBuild.Append("'%");
				strBuild.Append(strOriginCode);
				strBuild.Append("%'");
			}
			if((drEach["destination_state_code"]!= null) && (!drEach["destination_state_code"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				String strDestinationCode = (String) drEach["destination_state_code"];
				strBuild.Append(" and destination_state_code like ");
				strBuild.Append("'");
				strBuild.Append(strDestinationCode);
				strBuild.Append("'");
			}

			//
			if((drEach["remark"]!= null) && (!drEach["remark"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				String strRemark = (String) drEach["remark"];
				//strBuild.Append(" and destination_state_code like "); fix by Suthat
				strBuild.Append(" and remark like N");
				strBuild.Append("'%");
				strBuild.Append(Utility.ReplaceSingleQuote(strRemark));
				strBuild.Append("%'");
			}
			//

			strBuild.Append(" and booking_datetime is not null order by booking_no desc ");
			try
			{
				sessionDS = dbCon.ExecuteQuery(strBuild.ToString(),iCurrent,iDSRecSize,"DomesticShipment");
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("DomesticShipmentMgrDAL.cs","GetShipmentData","ERR002","Error in  Execute Query "+appException.Message);
				throw new ApplicationException("DomesticShipmentMgrDAL  (GetShipmentData) ",appException);
			}
			decimal iQryRslt = sessionDS.QueryResultMaxSize; 
			return sessionDS;
		}


		public static SessionDS GetShipmentData_Partial(String strAppID, String strEnterpriseID, int iCurrent, int iDSRecSize,DataSet dsQueryShipment)
		{
			SessionDS sessionDS = new SessionDS(); 
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon==null)
			{
				Logger.LogTraceError("DomesticShipmentMgrDAL","GetShipmentData","ERR001","DbConnectionis null!!");
				return sessionDS;
			}
			DataRow drEach = dsQueryShipment.Tables[0].Rows[0];

			StringBuilder strBuild = new StringBuilder();
			strBuild.Append("select *,dateadd(hh,6,booking_datetime) as BookingDateTimePlus6 from shipment where applicationid = '");
			strBuild.Append(strAppID);
			strBuild.Append("' and enterpriseid = '");
			strBuild.Append(strEnterpriseID+"'");
			
			//			if(Convert.ToDecimal(drEach["booking_no"]) > 0)
			//			{
			//				strBuild.Append(" and booking_no = ");
			//				strBuild.Append(Convert.ToInt32(drEach["booking_no"]));
			//			}
			
			if((drEach["consignment_no"]!= null) && (!drEach["consignment_no"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				String strConsgNo = (String) drEach["consignment_no"];
				strBuild.Append(" and consignment_no ='");
				strBuild.Append(Utility.ReplaceSingleQuote(strConsgNo));
				strBuild.Append("' and booking_datetime is null");
			}

			try
			{
				sessionDS = dbCon.ExecuteQuery(strBuild.ToString(),iCurrent,iDSRecSize,"DomesticShipment");
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("DomesticShipmentMgrDAL.cs","GetShipmentData","ERR002","Error in  Execute Query "+appException.Message);
				throw new ApplicationException("DomesticShipmentMgrDAL  (GetShipmentData) ",appException);
			}
			decimal iQryRslt = sessionDS.QueryResultMaxSize; 
			return sessionDS;
		}


		/// <summary>
		/// This method gets data from shipment_vas.
		/// </summary>
		/// <param name="strAppID">Application ID</param>
		/// <param name="strEnterpriseID">Enterprise ID</param>
		/// <param name="bookingNo">Booking Number</param>
		/// <returns>Dataset</returns>
		public static DataSet GetVASData(String strAppID, String strEnterpriseID, decimal bookingNo, String consignmentNo)
		{
			DbConnection dbCon= null;
			DataSet dsVAS =null;
			IDbCommand dbcmd=null;
			DataSet dsVASData = null;

			dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if (dbCon==null)
			{
				Logger.LogTraceError("DomesticShipmentMgrDAL.cs","GetVASData","ERR001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			StringBuilder strQry =  new StringBuilder(); 
			strQry = strQry.Append("select a.vas_code,a.vas_surcharge,a.remark, isnull(b.vas_description, '') as vas_description ");
			strQry.Append("from shipment_vas a left outer join vas b ");
			strQry.Append("on (a.vas_code = b.vas_code ");
			strQry.Append("and a.applicationid = b.applicationid ");
			strQry.Append("and a.enterpriseid = b.enterpriseid) ");
			strQry.Append(" where a.applicationid = '");
			strQry.Append(strAppID);
			strQry.Append ("' and a.enterpriseid = '");
			strQry.Append (strEnterpriseID);
			strQry.Append("' and a.booking_no = ");
			strQry.Append(bookingNo);
			strQry.Append(" and a.consignment_no = '");
			strQry.Append(consignmentNo);
			strQry.Append("' ");
			dbcmd = dbCon.CreateCommand(strQry.ToString(),CommandType.Text);
 
			try
			{
				dsVAS = (DataSet)dbCon.ExecuteQuery(dbcmd,com.common.DAL.ReturnType.DataSetType);  
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("DomesticShipmentMgrDAL.cs","GetVASData","ERR002","Error in Query String "+appException.Message);
				throw appException;
			}
			
			dsVASData = GetEmptyVASDS();

			if(dsVAS.Tables[0].Rows.Count > 0)
			{
				int i = 0;
				for(i = 0;i<dsVAS.Tables[0].Rows.Count;i++)
				{
					DataRow drVAS = dsVAS.Tables[0].Rows[i];
					AddNewRowInVAS(dsVASData);
					DataRow drEach = dsVASData.Tables[0].Rows[i];
					
					drEach["vas_code"] = drVAS["vas_code"];
					
					drEach["vas_description"] = drVAS["vas_description"];
					drEach["surcharge"] = drVAS["vas_surcharge"];
					drEach["remarks"] = drVAS["remark"];
				}
			}
			return dsVASData;
		}
		/// <summary>
		/// This method gets data from shipment_pkg table.
		/// </summary>
		/// <param name="strAppID">Application ID</param>
		/// <param name="strEnterpriseID">Enterprise ID</param>
		/// <param name="bookingNo">Booking Number</param>
		/// <returns></returns>
		public static DataSet GetPakageDtls(String strAppID, String strEnterpriseID, int bookingNo, string consignmentNo)
		{
			DbConnection dbCon= null;
			DataSet dsPkgDtls =null;
			IDbCommand dbcmd=null;
			DataSet dsPkgDtlsData =null;


			dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if (dbCon==null)
			{
				Logger.LogTraceError("DomesticShipmentMgrDAL.cs","GetPakageDtlsSWB","ERR001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			StringBuilder strQry =  new StringBuilder(); 
			strQry.Append("select * from shipment_pkg where applicationid = '");
			strQry.Append(strAppID);
			strQry.Append("' and enterpriseid = '");
			strQry.Append(strEnterpriseID+"'");
			strQry.Append(" and booking_no = ");
			strQry.Append(bookingNo);
			strQry.Append(" and consignment_no = '");
			strQry.Append(consignmentNo+"'");

			dbcmd = dbCon.CreateCommand(strQry.ToString(),CommandType.Text);
 
			try
			{
				dsPkgDtls = (DataSet)dbCon.ExecuteQuery(dbcmd,com.common.DAL.ReturnType.DataSetType);  
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("DomesticShipmentMgrDAL.cs","GetPakageDtlsSWB","ERR002","Error in Query String "+appException.Message);
				throw appException;
			}

			if(dsPkgDtls.Tables[0].Rows.Count > 0)
			{
				dsPkgDtlsData = GetEmptyPkgDtlDS();
				int i = 0;
				for(i = 0;i<dsPkgDtls.Tables[0].Rows.Count;i++)
				{
					//Modified by GwanG 15May08
					/*
					DataRow drPkgDtls = dsPkgDtls.Tables[0].Rows[i];
					AddNewRowInPkgDS(dsPkgDtlsData);
					DataRow drEach = dsPkgDtlsData.Tables[0].Rows[i];
					
					drEach["mps_no"] = drPkgDtls["mps_code"];
					drEach["pkg_length"] = drPkgDtls["pkg_length"];
					drEach["pkg_breadth"] = drPkgDtls["pkg_breadth"];
					drEach["pkg_height"] = drPkgDtls["pkg_height"];
					drEach["pkg_qty"] = drPkgDtls["pkg_qty"];
					drEach["pkg_wt"] = drPkgDtls["pkg_wt"];
					drEach["tot_wt"] = drPkgDtls["tot_wt"];
					drEach["tot_dim_wt"] = drPkgDtls["tot_dim_wt"];
					drEach["chargeable_wt"] = drPkgDtls["chargeable_wt"];
					decimal decVolume = (decimal)drEach["pkg_length"]*(decimal)drEach["pkg_breadth"]*(decimal)drEach["pkg_height"];
					drEach["pkg_volume"] = decVolume;*/


					DataRow drPkgDtls = dsPkgDtls.Tables[0].Rows[i];									
					DataRow drEach = dsPkgDtlsData.Tables[0].NewRow();
					drEach["mps_code"] = drPkgDtls["mps_code"];

					drEach["mps_code"] = drPkgDtls["mps_code"];
					//Modified by Tom 10/6/09
					//					string Length = Convert.ToString(drPkgDtls["pkg_length"]);
					//					string Breadth = Convert.ToString(drPkgDtls["pkg_breadth"]);
					//					string Height = Convert.ToString(drPkgDtls["pkg_height"]);
					//
					//					if(Length[Length.Length - 2] == '0')
					//						Length = Length.Remove(Length.Length - 3,3);
					//					else
					//						Length = Length.Remove(Length.Length - 1,1);
					//
					//					if(Breadth[Breadth.Length - 2] == '0')
					//						Breadth = Breadth.Remove(Breadth.Length - 3,3);
					//					else
					//						Breadth = Breadth.Remove(Breadth.Length - 1,1);
					//
					//					if(Height[Height.Length - 2] == '0')
					//						Height = Height.Remove(Height.Length - 3,3);
					//					else
					//						Height = Height.Remove(Height.Length - 1,1);
					//
					//					drEach["pkg_length"] = Length;
					//					drEach["pkg_breadth"] = Breadth;
					//					drEach["pkg_height"] = Height;
					//End modified by Tom 10/6/09

					drEach["pkg_length"] = drPkgDtls["pkg_length"];
					drEach["pkg_breadth"] = drPkgDtls["pkg_breadth"];
					drEach["pkg_height"] = drPkgDtls["pkg_height"];


					drEach["pkg_qty"] = drPkgDtls["pkg_qty"];
					drEach["pkg_wt"] = drPkgDtls["pkg_wt"];
					drEach["tot_wt"] = drPkgDtls["tot_wt"];
					//TU on 17June08
					//					if (drPkgDtls["tot_act_wt"]==null)
					//					{
					//						drEach["tot_act_wt"]=
					//					}
					drEach["tot_act_wt"]=drPkgDtls["tot_act_wt"];
					//

					drEach["tot_dim_wt"] = drPkgDtls["tot_dim_wt"];
					drEach["chargeable_wt"] = drPkgDtls["chargeable_wt"];
					decimal decVolume = Convert.ToDecimal(drEach["pkg_length"])*Convert.ToDecimal(drEach["pkg_breadth"])*Convert.ToDecimal(drEach["pkg_height"]);
					drEach["pkg_volume"] = decVolume;
					//Jeab 8 Dec 2010
					decimal decTOTVol  = Convert.ToDecimal(drEach["pkg_length"])*Convert.ToDecimal(drEach["pkg_breadth"])*Convert.ToDecimal(drEach["pkg_height"])*Convert.ToDecimal(drEach["pkg_qty"]);
					drEach["pkg_TOTvolume"] =   decTOTVol ;					
					//Jeab 8 Dec 2010  =========> End
					dsPkgDtlsData.Tables[0].Rows.Add(drEach);
					//

				}
			}

			return dsPkgDtlsData;
		}

		/// <summary>
		/// This method gets data from shipment_pkg table.
		/// </summary>
		/// <param name="strAppID">Application ID</param>
		/// <param name="strEnterpriseID">Enterprise ID</param>
		/// <param name="bookingNo">Booking Number</param>
		/// <returns></returns>
		public static DataSet GetPakageDtls_Original(String strAppID, String strEnterpriseID, int bookingNo, string consignmentNo)
		{
			DbConnection dbCon= null;
			DataSet dsPkgDtls =null;
			IDbCommand dbcmd=null;
			DataSet dsPkgDtlsData =null;


			dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if (dbCon==null)
			{
				Logger.LogTraceError("DomesticShipmentMgrDAL.cs","GetPakageDtlsSWB","ERR001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			StringBuilder strQry =  new StringBuilder(); 
			strQry.Append("select * from shipment_pkg_Original where applicationid = '");
			strQry.Append(strAppID);
			strQry.Append("' and enterpriseid = '");
			strQry.Append(strEnterpriseID+"'");
			strQry.Append(" and booking_no = ");
			strQry.Append(bookingNo);
			strQry.Append(" and consignment_no = '");
			strQry.Append(consignmentNo+"'");

			dbcmd = dbCon.CreateCommand(strQry.ToString(),CommandType.Text);
 
			try
			{
				dsPkgDtls = (DataSet)dbCon.ExecuteQuery(dbcmd,com.common.DAL.ReturnType.DataSetType);  
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("DomesticShipmentMgrDAL.cs","GetPakageDtlsSWB","ERR002","Error in Query String "+appException.Message);
				throw appException;
			}

			if(dsPkgDtls.Tables[0].Rows.Count > 0)
			{
				dsPkgDtlsData = GetEmptyPkgDtlDS();
				
				int i = 0;
				for(i = 0;i<dsPkgDtls.Tables[0].Rows.Count;i++)
				{
					//Modified by GwanG 15May08
					/*
					DataRow drPkgDtls = dsPkgDtls.Tables[0].Rows[i];
					AddNewRowInPkgDS(dsPkgDtlsData);
					DataRow drEach = dsPkgDtlsData.Tables[0].Rows[i];
					
					drEach["mps_no"] = drPkgDtls["mps_code"];
					drEach["pkg_length"] = drPkgDtls["pkg_length"];
					drEach["pkg_breadth"] = drPkgDtls["pkg_breadth"];
					drEach["pkg_height"] = drPkgDtls["pkg_height"];
					drEach["pkg_qty"] = drPkgDtls["pkg_qty"];
					drEach["pkg_wt"] = drPkgDtls["pkg_wt"];
					drEach["tot_wt"] = drPkgDtls["tot_wt"];
					drEach["tot_dim_wt"] = drPkgDtls["tot_dim_wt"];
					drEach["chargeable_wt"] = drPkgDtls["chargeable_wt"];
					decimal decVolume = (decimal)drEach["pkg_length"]*(decimal)drEach["pkg_breadth"]*(decimal)drEach["pkg_height"];
					drEach["pkg_volume"] = decVolume;*/


					DataRow drPkgDtls = dsPkgDtls.Tables[0].Rows[i];									
					DataRow drEach = dsPkgDtlsData.Tables[0].NewRow();
					drEach["mps_code"] = drPkgDtls["mps_code"];

					drEach["mps_code"] = drPkgDtls["mps_code"];
					//Modified by Tom 10/6/09
					//					string Length = Convert.ToString(drPkgDtls["pkg_length"]);
					//					string Breadth = Convert.ToString(drPkgDtls["pkg_breadth"]);
					//					string Height = Convert.ToString(drPkgDtls["pkg_height"]);
					//
					//					if(Length[Length.Length - 2] == '0')
					//						Length = Length.Remove(Length.Length - 3,3);
					//					else
					//						Length = Length.Remove(Length.Length - 1,1);
					//
					//					if(Breadth[Breadth.Length - 2] == '0')
					//						Breadth = Breadth.Remove(Breadth.Length - 3,3);
					//					else
					//						Breadth = Breadth.Remove(Breadth.Length - 1,1);
					//
					//					if(Height[Height.Length - 2] == '0')
					//						Height = Height.Remove(Height.Length - 3,3);
					//					else
					//						Height = Height.Remove(Height.Length - 1,1);
					//
					//					drEach["pkg_length"] = Length;
					//					drEach["pkg_breadth"] = Breadth;
					//					drEach["pkg_height"] = Height;
					//End modified by Tom 10/6/09

					drEach["pkg_length"] = drPkgDtls["pkg_length"];
					drEach["pkg_breadth"] = drPkgDtls["pkg_breadth"];
					drEach["pkg_height"] = drPkgDtls["pkg_height"];


					drEach["pkg_qty"] = drPkgDtls["pkg_qty"];
					drEach["pkg_wt"] = drPkgDtls["pkg_wt"];
					drEach["tot_wt"] = drPkgDtls["tot_wt"];
					//TU on 17June08
					//					if (drPkgDtls["tot_act_wt"]==null)
					//					{
					//						drEach["tot_act_wt"]=
					//					}
					drEach["tot_act_wt"]=drPkgDtls["tot_act_wt"];
					//

					drEach["tot_dim_wt"] = drPkgDtls["tot_dim_wt"];
					drEach["chargeable_wt"] = drPkgDtls["chargeable_wt"];
					decimal decVolume = Convert.ToDecimal(drEach["pkg_length"])*Convert.ToDecimal(drEach["pkg_breadth"])*Convert.ToDecimal(drEach["pkg_height"]);
					drEach["pkg_volume"] = decVolume;
					//Jeab 8 Dec 2010
					decimal decTOTVol  = Convert.ToDecimal(drEach["pkg_length"])*Convert.ToDecimal(drEach["pkg_breadth"])*Convert.ToDecimal(drEach["pkg_height"])*Convert.ToDecimal(drEach["pkg_qty"]);
					drEach["pkg_TOTvolume"] =   decTOTVol ;					
					//Jeab 8 Dec 2010  =========> End
					dsPkgDtlsData.Tables[0].Rows.Add(drEach);
					//

				}
			}

			return dsPkgDtlsData;
		}

		public static string GetShipment_ReplaceDate(String strAppID, String strEnterpriseID, int bookingNo, string consignmentNo)
		{
			DbConnection dbCon= null;
			DataSet dsShipment =null;
			IDbCommand dbcmd=null;
			//DataSet dsPkgDtlsData =null;
			string ReplaceDateTime="";

			dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if (dbCon==null)
			{
				Logger.LogTraceError("DomesticShipmentMgrDAL.cs","GetPakageDtlsSWB","ERR001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			StringBuilder strQry =  new StringBuilder(); 
			strQry.Append("select CONVERT(CHAR(10),pkg_detail_replaced_datetime, 103) + ' ' + CONVERT(CHAR(5), pkg_detail_replaced_datetime, 108)  as replacedDate,pkg_detail_replaced_datetime  from shipment where applicationid = '");
			strQry.Append(strAppID);
			strQry.Append("' and enterpriseid = '");
			strQry.Append(strEnterpriseID+"'");
			strQry.Append(" and booking_no = ");
			strQry.Append(bookingNo);
			strQry.Append(" and consignment_no = '");
			strQry.Append(consignmentNo+"'");

			dbcmd = dbCon.CreateCommand(strQry.ToString(),CommandType.Text);
 
			try
			{
				dsShipment = (DataSet)dbCon.ExecuteQuery(dbcmd,com.common.DAL.ReturnType.DataSetType);  
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("DomesticShipmentMgrDAL.cs","GetPakageDtlsSWB","ERR002","Error in Query String "+appException.Message);
				throw appException;
			}

			if(dsShipment.Tables[0].Rows.Count > 0)
			{

				ReplaceDateTime = dsShipment.Tables[0].Rows[0]["replacedDate"].ToString();
				
			}

			return ReplaceDateTime;
		}

		//Tumz
		public static DataSet GetShipmentPKGForCompare(String strAppID, String strEnterpriseID, int bookingNo, string consignmentNo)
		{
			DbConnection dbCon= null;
			DataSet dsPkgDtls =null;
			IDbCommand dbcmd=null;
			DataSet dsPkgDtlsData =null;


			dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if (dbCon==null)
			{
				Logger.LogTraceError("DomesticShipmentMgrDAL.cs","GetPakageDtlsSWB","ERR001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			StringBuilder strQry =  new StringBuilder(); 
			strQry.Append("select * from shipment_pkg where applicationid = '");
			strQry.Append(strAppID);
			strQry.Append("' and enterpriseid = '");
			strQry.Append(strEnterpriseID+"'");
			if (bookingNo != 0)
			{
				strQry.Append(" and booking_no = ");
				strQry.Append(bookingNo);
			}
			
			strQry.Append(" and consignment_no = '");
			strQry.Append(consignmentNo+"'");

			dbcmd = dbCon.CreateCommand(strQry.ToString(),CommandType.Text);
 
			try
			{
				dsPkgDtls = (DataSet)dbCon.ExecuteQuery(dbcmd,com.common.DAL.ReturnType.DataSetType);  
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("DomesticShipmentMgrDAL.cs","GetPakageDtlsSWB","ERR002","Error in Query String "+appException.Message);
				throw appException;
			}

			if(dsPkgDtls.Tables[0].Rows.Count > 0)
			{
				dsPkgDtlsData = GetEmptyPkgDtlDS();
				
				int i = 0;
				for(i = 0;i<dsPkgDtls.Tables[0].Rows.Count;i++)
				{
					DataRow drPkgDtls = dsPkgDtls.Tables[0].Rows[i];									
					DataRow drEach = dsPkgDtlsData.Tables[0].NewRow();
					drEach["mps_code"] = drPkgDtls["mps_code"];

					drEach["pkg_length"] = drPkgDtls["pkg_length"];
					drEach["pkg_breadth"] = drPkgDtls["pkg_breadth"];
					drEach["pkg_height"] = drPkgDtls["pkg_height"];


					drEach["pkg_qty"] = drPkgDtls["pkg_qty"];
					drEach["pkg_wt"] = drPkgDtls["pkg_wt"];
					drEach["tot_wt"] = drPkgDtls["tot_wt"];
					drEach["tot_act_wt"]=drPkgDtls["tot_act_wt"];
					//

					drEach["tot_dim_wt"] = drPkgDtls["tot_dim_wt"];
					drEach["chargeable_wt"] = drPkgDtls["chargeable_wt"];
					decimal decVolume = Convert.ToDecimal(drEach["pkg_length"])*Convert.ToDecimal(drEach["pkg_breadth"])*Convert.ToDecimal(drEach["pkg_height"]);
					drEach["pkg_volume"] = decVolume;
					//Jeab 8 Dec 2010
					decimal decTOTVol  = Convert.ToDecimal(drEach["pkg_length"])*Convert.ToDecimal(drEach["pkg_breadth"])*Convert.ToDecimal(drEach["pkg_height"])*Convert.ToDecimal(drEach["pkg_qty"]);
					drEach["pkg_TOTvolume"] =   decTOTVol ;					
					//Jeab 8 Dec 2010  =========> End
					dsPkgDtlsData.Tables[0].Rows.Add(drEach);
					//

				}
			}

			return dsPkgDtlsData;
		}

		/// <summary>
		/// This method gets data from Customer table.
		/// </summary>
		/// <param name="strAppID">Application ID</param>
		/// <param name="strEnterpriseID">Enterprise ID</param>
		/// <param name="Custid">Customer ID</param>
		/// <returns></returns>
		
		//Jeab 8 Dec 2010
		public static DataSet GetCustDimByTOTStatus(String strAppID, String strEnterpriseID,string custID)
		{
			DbConnection dbCon= null;
			DataSet dsCusts =null;
			IDbCommand dbcmd=null;
			DataSet dsCustsData =null;


			dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if (dbCon==null)
			{
				Logger.LogTraceError("DomesticShipmentMgrDAL.cs","GetCustDimByTOTStatus","ERR001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			StringBuilder strQry =  new StringBuilder(); 
			strQry.Append("select  dim_by_tot  from customer  where applicationid = '");
			strQry.Append(strAppID);
			strQry.Append("' and enterpriseid = '");
			strQry.Append(strEnterpriseID+"'");			
			strQry.Append(" and custid = '");
			strQry.Append(custID+"'");

			dbcmd = dbCon.CreateCommand(strQry.ToString(),CommandType.Text);
 
			try
			{
				dsCusts = (DataSet)dbCon.ExecuteQuery(dbcmd,com.common.DAL.ReturnType.DataSetType);  
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("DomesticShipmentMgrDAL.cs","GetCustDimByTOTStatus","ERR002","Error in Query String "+appException.Message);
				throw appException;
			}

			if(dsCusts.Tables[0].Rows.Count > 0)
			{
				dsCustsData = GetEmptyCustDimByTOTDS();				
				
				int i = 0;
				for(i = 0;i<dsCusts.Tables[0].Rows.Count;i++)
				{																		
					DataRow drCusts = dsCusts.Tables[0].Rows[i];									
					DataRow drEach = dsCustsData.Tables[0].NewRow();
					drEach["dim_by_tot"] = drCusts["dim_by_tot"];

					dsCustsData.Tables[0].Rows.Add(drEach);
				}
			}

			return dsCustsData;
		}
		//Jeab 8 Dec 2010  =========> End

		public static DataSet GetPakageDtlsSWB(String strAppID, String strEnterpriseID, string consignmentNo, String custid)
		{
			DbConnection dbCon= null;
			DataSet dsPkgDtls =null;
			IDbCommand dbcmd=null;
			DataSet dsPkgDtlsData =null;
			int wtRoundingMethod = 0;
			decimal wtIncrementAmt = 0;

			dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			DataSet profileDS = SysDataManager1.GetEnterpriseProfile(strAppID, strEnterpriseID);
			if(profileDS.Tables[0].Rows.Count > 0)
			{
				int currency_decimal = (int) profileDS.Tables[0].Rows[0]["currency_decimal"];
					
				if  ( profileDS.Tables[0].Rows[0]["wt_rounding_method"] != System.DBNull.Value)
				{
						
					wtRoundingMethod = Convert.ToInt32(profileDS.Tables[0].Rows[0]["wt_rounding_method"]);
				}

				if  ( profileDS.Tables[0].Rows[0]["wt_increment_amt"] != System.DBNull.Value)
				{
					wtIncrementAmt = Convert.ToDecimal(profileDS.Tables[0].Rows[0]["wt_increment_amt"]);
				}
			}

			if (dbCon==null)
			{
				Logger.LogTraceError("DomesticShipmentMgrDAL.cs","GetPakageDtlsSWB","ERR001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			StringBuilder strQry =  new StringBuilder(); 
			strQry.Append("select * from package_details where consignment_no = '");
			strQry.Append(consignmentNo+"'");

			dbcmd = dbCon.CreateCommand(strQry.ToString(),CommandType.Text);
 
			try
			{
				dsPkgDtls = (DataSet)dbCon.ExecuteQuery(dbcmd,com.common.DAL.ReturnType.DataSetType);  
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("DomesticShipmentMgrDAL.cs","GetPakageDtlsSWB","ERR002","Error in Query String "+appException.Message);
				throw appException;
			}

			if(dsPkgDtls.Tables[0].Rows.Count > 0)
			{
				dsPkgDtlsData = GetEmptyPkgDtlDS();
				int i = 0;
				for(i = 0;i<dsPkgDtls.Tables[0].Rows.Count;i++)
				{
					decimal fDimWt = 0;
					//					double fDimWt1 = 0;
					decimal roundWt = 0;
					double fDensityFactor = 0;
					if(custid != null)
					{
						Customer customer = new Customer();
						customer.Populate(strAppID,strEnterpriseID,custid);
						if(!customer.density_factor.GetType().Equals(System.Type.GetType("System.DBNull")))
							fDensityFactor = Double.Parse(customer.density_factor.ToString());
					}
					if(fDensityFactor == 0)
					{
						Enterprise enterprise = new Enterprise();
						enterprise.Populate(strAppID,strEnterpriseID);
						fDensityFactor = (double)enterprise.DensityFactor;
					}

					DataRow drPkgDtls = dsPkgDtls.Tables[0].Rows[i];									
					DataRow drEach = dsPkgDtlsData.Tables[0].NewRow();
					drEach["mps_code"] = drPkgDtls["mps_code"];
					drEach["pkg_length"] = drPkgDtls["length"];
					drEach["pkg_breadth"] = drPkgDtls["breadth"];
					drEach["pkg_height"] = drPkgDtls["height"];
					drEach["pkg_qty"] = drPkgDtls["quantity"];
					drEach["pkg_wt"] = drPkgDtls["weight"];
					if(Convert.ToDecimal(drEach["pkg_wt"])<1)
						roundWt = 1;
					else
						roundWt = TIESUtility.EnterpriseRounding(Convert.ToDecimal(drEach["pkg_wt"]),wtRoundingMethod,wtIncrementAmt);

					decimal decVolume = Convert.ToDecimal(drEach["pkg_length"])*Convert.ToDecimal(drEach["pkg_breadth"])*Convert.ToDecimal(drEach["pkg_height"]);
					decimal actTotWt = Convert.ToDecimal(drEach["pkg_wt"])*Convert.ToDecimal(drEach["pkg_qty"].ToString());
					decimal roundTotWt = roundWt*Convert.ToDecimal(drEach["pkg_qty"]);
					int iQty = Convert.ToInt16(drEach["pkg_qty"].ToString());

					//					fDimWt1 = (double)iQty*Math.Ceiling((double)decVolume/fDensityFactor);
					//					fDimWt = Convert.ToDecimal(fDimWt1);

					fDimWt = (decimal)((double)decVolume / fDensityFactor);

					fDimWt = TIESUtility.EnterpriseRounding(fDimWt,wtRoundingMethod,wtIncrementAmt);
					//on 3Jul 08
					if(fDimWt<1)
						fDimWt = Math.Round((decimal)iQty, 2); 
					else
						fDimWt = Math.Round(fDimWt * iQty, 2);
								
					//					decimal dimTotWt1 = (decimal)((float)drEach["pkg_volume"]/fDensityFactor);
					//					decimal dimTotWt  = Math.Round(dimTotWt1)*Convert.ToDecimal(drEach["pkg_qty"].ToString());
					 
					drEach["tot_wt"] = roundTotWt;					
					drEach["tot_act_wt"]=actTotWt;

					drEach["tot_dim_wt"] = fDimWt;
					drEach["chargeable_wt"] = roundTotWt;
					
					drEach["pkg_volume"] = decVolume;					
					dsPkgDtlsData.Tables[0].Rows.Add(drEach);

				}
			}

			return dsPkgDtlsData;
		}

		
		public static DataSet GetPakageDtlsSWB_ForNew(String strAppID, String strEnterpriseID, DataSet dsPkgDtls, String custid)
		{
			DbConnection dbCon= null;
			IDbCommand dbcmd=null;
			DataSet dsPkgDtlsData =null;
			int wtRoundingMethod = 0;
			decimal wtIncrementAmt = 0;

			
			if(dsPkgDtls.Tables[0].Rows.Count > 0)
			{
				dsPkgDtlsData = GetEmptyPkgDtlDS();
				int i = 0;
				for(i = 0;i<dsPkgDtls.Tables[0].Rows.Count;i++)
				{
					decimal fDimWt = 0;
					//					double fDimWt1 = 0;
					decimal roundWt = 0;
					double fDensityFactor = 0;
					if(custid != null)
					{
						Customer customer = new Customer();
						customer.Populate(strAppID,strEnterpriseID,custid);
						if(!customer.density_factor.GetType().Equals(System.Type.GetType("System.DBNull")))
							fDensityFactor = Double.Parse(customer.density_factor.ToString());
					}
					if(fDensityFactor == 0)
					{
						Enterprise enterprise = new Enterprise();
						enterprise.Populate(strAppID,strEnterpriseID);
						fDensityFactor = (double)enterprise.DensityFactor;
					}

					DataRow drPkgDtls = dsPkgDtls.Tables[0].Rows[i];									
					DataRow drEach = dsPkgDtlsData.Tables[0].NewRow();
					drEach["mps_code"] = drPkgDtls["mps_code"];
					drEach["pkg_length"] = drPkgDtls["length"];
					drEach["pkg_breadth"] = drPkgDtls["breadth"];
					drEach["pkg_height"] = drPkgDtls["height"];
					drEach["pkg_qty"] = drPkgDtls["quantity"];
					drEach["pkg_wt"] = drPkgDtls["weight"];
					if(Convert.ToDecimal(drEach["pkg_wt"])<1)
						roundWt = 1;
					else
						roundWt = TIESUtility.EnterpriseRounding(Convert.ToDecimal(drEach["pkg_wt"]),wtRoundingMethod,wtIncrementAmt);

					decimal decVolume = Convert.ToDecimal(drEach["pkg_length"])*Convert.ToDecimal(drEach["pkg_breadth"])*Convert.ToDecimal(drEach["pkg_height"]);
					decimal actTotWt = Convert.ToDecimal(drEach["pkg_wt"])*Convert.ToDecimal(drEach["pkg_qty"].ToString());
					decimal roundTotWt = roundWt*Convert.ToDecimal(drEach["pkg_qty"]);
					int iQty = Convert.ToInt16(drEach["pkg_qty"].ToString());

					//					fDimWt1 = (double)iQty*Math.Ceiling((double)decVolume/fDensityFactor);
					//					fDimWt = Convert.ToDecimal(fDimWt1);

					fDimWt = (decimal)((double)decVolume / fDensityFactor);

					fDimWt = TIESUtility.EnterpriseRounding(fDimWt,wtRoundingMethod,wtIncrementAmt);
					//on 3Jul 08
					if(fDimWt<1)
						fDimWt = Math.Round((decimal)iQty, 2); 
					else
						fDimWt = Math.Round(fDimWt * iQty, 2);
								
					//					decimal dimTotWt1 = (decimal)((float)drEach["pkg_volume"]/fDensityFactor);
					//					decimal dimTotWt  = Math.Round(dimTotWt1)*Convert.ToDecimal(drEach["pkg_qty"].ToString());
					 
					drEach["tot_wt"] = roundTotWt;					
					drEach["tot_act_wt"]=actTotWt;

					drEach["tot_dim_wt"] = fDimWt;
					drEach["chargeable_wt"] = roundTotWt;
					
					drEach["pkg_volume"] = decVolume;					
					dsPkgDtlsData.Tables[0].Rows.Add(drEach);

				}
			}

			return dsPkgDtlsData;
		}

		/// <summary>
		/// This method gets record from table pickup_shipment but doesn't exists in pickup_pkg table.
		/// </summary>
		/// <param name="strAppID">Application ID</param>
		/// <param name="strEnterpriseID">Enterprise ID</param>
		/// <param name="bookingNo">Booking Number</param>
		/// <returns>Dataset</returns>
		public static DataSet GetPickupShipment(String strAppID, String strEnterpriseID,int bookingNo)
		{
			DbConnection dbCon= null;
			IDbCommand dbcmd=null;
			DataSet dsPickUpShpmnt =null;


			dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if (dbCon==null)
			{
				Logger.LogTraceError("DomesticShipmentMgrDAL.cs","GetPakageDtlsSWB","ERR001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			StringBuilder strQry =  new StringBuilder(); 
			strQry.Append("select * from pickup_shipment where applicationid = '");
			strQry.Append(strAppID);
			strQry.Append("' and enterpriseid = '");
			strQry.Append(strEnterpriseID+"'");
			strQry.Append(" and booking_no = ");
			strQry.Append(bookingNo);
			strQry.Append(" and serial_no in (select serial_no from pickup_pkg where applicationid = '");
			strQry.Append(strAppID);
			strQry.Append("' and enterpriseid = '");
			strQry.Append(strEnterpriseID+"'");
			strQry.Append(" and booking_no = ");
			strQry.Append(bookingNo+")");

			dbcmd = dbCon.CreateCommand(strQry.ToString(),CommandType.Text);
 
			try
			{
				dsPickUpShpmnt = (DataSet)dbCon.ExecuteQuery(dbcmd,com.common.DAL.ReturnType.DataSetType);  
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("DomesticShipmentMgrDAL.cs","GetPakageDtlsSWB","ERR002","Error in Query String "+appException.Message);
				throw appException;
			}
			return dsPickUpShpmnt;
		}
		/// <summary>
		/// This method gets empty dataset for getting records from the pickup_shipment table.
		/// </summary>
		/// <returns>Dataset</returns>
		public static DataSet GetEmptyPickupPkgs()
		{
			DataTable dtPickupPkg = new DataTable();
			dtPickupPkg.Columns.Add(new DataColumn("booking_no", typeof(int)));
			dtPickupPkg.Columns.Add(new DataColumn("serial_no",typeof(int)));
			dtPickupPkg.Columns.Add(new DataColumn("recipient_zipcode",typeof(string)));
			dtPickupPkg.Columns.Add(new DataColumn("service_code",typeof(string)));
			dtPickupPkg.Columns.Add(new DataColumn("consignment_no",typeof(string)));
			
			DataSet dsPickupPkg = new DataSet();
			dsPickupPkg.Tables.Add(dtPickupPkg);
			
			return  dsPickupPkg;
		}
		/// <summary>
		/// This method adds a row in the empty dataset returned  by GetEmptyPickupPkgs.
		/// </summary>
		/// <param name="dsPickupPkg">Dataset</param>
		public static void AddNewRowPickupPkgs(DataSet dsPickupPkg)
		{
			DataRow drNew = dsPickupPkg.Tables[0].NewRow();
			dsPickupPkg.Tables[0].Rows.Add(drNew);
		}
		/// <summary>
		/// This method gets records from pickup_request table.
		/// </summary>
		/// <param name="strAppID">Application ID</param>
		/// <param name="strEnterpriseID">Enterprise ID</param>
		/// <param name="iBookingNo">Booking Number</param>
		/// <returns></returns>
		public static DataSet GetPickUpShipment(String strAppID,String strEnterpriseID,int iBookingNo)
		{
			StringBuilder strQry = null;
			IDbCommand dbCmd = null;
			DataSet dsPickUpPkgs = null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("DomesticShipmentManager","GetPickUpShipment","ERR001","DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null",null);
			}
			strQry = new StringBuilder();
			strQry.Append("select * from pickup_request where applicationid = '");
			strQry.Append(strAppID);
			strQry.Append("' and strEnterpriseID = '");
			strQry.Append(strEnterpriseID+"'");
			strQry.Append(" and booking_no = ");
			strQry.Append(iBookingNo);
			

			dbCmd = dbCon.CreateCommand(strQry.ToString(),CommandType.Text);
			try
			{
				dsPickUpPkgs = (DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
			}
			catch(System.ApplicationException appException)
			{
				Logger.LogTraceError("DomesticShipmentManager","GetPickUpShipment","ERR002","Error ");
				throw appException;
			}
			return dsPickUpPkgs;
		}
		/// <summary>
		/// This methods checks records from shipment table, whether  it can be deleted or not.
		/// Returns true if the record can be deleted else returns false.
		/// </summary>
		/// <param name="strAppID">Application ID</param>
		/// <param name="strEnterpriseID">Enterprise ID</param>
		/// <param name="iBookingNo">Booking Number</param>
		/// <param name="strStatusCode">Status Code</param>
		/// <returns>boolean</returns>
		public static bool CanDeleteShipment(String strAppID,String strEnterpriseID,int iBookingNo,String strStatusCode)
		{
			bool isDeleteable = false;
			StringBuilder strQry = null;
			IDbCommand dbCmd = null;
			DataSet dsShipment = null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("DomesticShipmentManager","CanDeleteShipment","ERR001","DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null",null);
			}
			strQry = new StringBuilder();
			strQry.Append("select * from shipment  where applicationid = '");
			strQry.Append(strAppID);
			strQry.Append("' and EnterpriseID = '");
			strQry.Append(strEnterpriseID+"'");
			strQry.Append(" and booking_no = ");
			strQry.Append(iBookingNo);
			strQry.Append(" and last_status_code in ('SIP','");
			strQry.Append(strStatusCode);
			strQry.Append("')");
			

			dbCmd = dbCon.CreateCommand(strQry.ToString(),CommandType.Text);
			try
			{
				dsShipment = (DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
				if(dsShipment.Tables[0].Rows.Count > 0)
				{
					isDeleteable = true;
				}
			}
			catch(System.ApplicationException appException)
			{
				Logger.LogTraceError("DomesticShipmentManager","CanDeleteShipment","ERR002","Error ");
				throw appException;
			}
			return isDeleteable;
		}


		/// <summary>
		/// //Automatically Assing to Manifests (DMS Phase 1)
		/// 17. Logic for the exit_w-o_manifesting subroutine appears below:
		/// </summary>
		public static int UpdateShipmentOfAutoManifest(String strAppID,String strEnterpriseID,
			int BookingNo, String consignmentNo)
		{
			IDbCommand dbCmd = null;
			int iRowsAffected = 0;
			StringBuilder strBuild = null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("DomesticShipmentMgrDAL","UpdateShipmentOfAutoManifest","ERR001","DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null",null);
			}

			IDbConnection conApp = dbCon.GetConnection();
			
			if(conApp == null)
			{
				Logger.LogTraceError("DomesticShipmentMgrDAL","UpdateShipmentOfAutoManifest","ERR002","conApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			try
			{
				dbCon.OpenConnection(ref conApp);
			}
			catch(ApplicationException appException)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("DomesticShipmentMgrDAL","UpdateShipmentOfAutoManifest","ERR003","Error opening connection : "+ appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ",appException);
			}
			catch(Exception exception)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("DomesticShipmentMgrDAL","UpdateShipmentOfAutoManifest","ERR004","Error opening connection : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}

			try
			{
				strBuild = new StringBuilder();
				strBuild.Append("update shipment ");
				strBuild.Append("set shpt_manifest_datetime = null, ");
				strBuild.Append("delivery_manifested = 'N'");
				strBuild.Append(" where booking_no = " + BookingNo);
				strBuild.Append(" and consignment_no = '" + consignmentNo + "'");

				dbCmd = dbCon.CreateCommand(strBuild.ToString(), CommandType.Text);
				iRowsAffected = dbCon.ExecuteNonQuery(dbCmd);
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceInfo("DomesticShipmentMgrDAL","UpdateShipmentOfAutoManifest","ERR005","unable to update shipment data.");
				throw new ApplicationException("Update shipment data fail",appException);
			}
			finally
			{
				if(conApp != null)
				{
					conApp.Close();
				}
			}
			return iRowsAffected;
		}

		#region Comment by Sompote 2010-05-10
		/*
		/// <summary>
		/// //Automatically Assing to Manifests (DMS Phase 1)
		/// GetEnterpriseOfAutoManifest
		/// </summary>
		public static SessionDS GetEnterpriseOfAutoManifest(String strAppID, String strEnterpriseID,
			int iCurrent, int iDSRecSize)
		{
			SessionDS sessionDS = null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("DomesticShipmentMgrDAL","GetEnterpriseOfAutoManifest","EDB101","DbConnection object is null!!");
				return sessionDS;
			}

			StringBuilder str = new StringBuilder();
			str.Append("SELECT * ");
			str.Append("FROM Enterprise ");
			str.Append("WHERE applicationid = '");
			str.Append(strAppID);
			str.Append("' and enterpriseid = '");
			str.Append(strEnterpriseID);
			str.Append("'");
			
			sessionDS = dbCon.ExecuteQuery(str.ToString(),iCurrent, iDSRecSize,"Enterprise");
			return  sessionDS;	
		}

		*/
		#endregion

		public static SessionDS GetEnterpriseOfAutoManifest(String strAppID, String strEnterpriseID,
			int iCurrent, int iDSRecSize)
		{		
			SessionDS sessionDS = null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("DomesticShipmentMgrDAL","GetEnterpriseOfAutoManifest","EDB101","DbConnection object is null!!");
				return sessionDS;
			}

			return  GetEnterpriseOfAutoManifest( strAppID,  strEnterpriseID, iCurrent,  iDSRecSize, dbCon);	
		}

		public static SessionDS GetEnterpriseOfAutoManifest(String strAppID, String strEnterpriseID,
			int iCurrent, int iDSRecSize,DbConnection dbCon)
		{
			IDbConnection conApp = dbCon.GetConnection();
			if(conApp == null)
			{
				Logger.LogTraceError("DomesticShipmentMgrDAL","GetEnterpriseOfAutoManifest","ERR003","conApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			try
			{
				dbCon.OpenConnection(ref conApp);
			}
			catch(ApplicationException appException)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("DomesticShipmentMgrDAL","GetEnterpriseOfAutoManifest","ERR004","Error opening connection : "+ appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ",appException);
			}
			catch(Exception exception)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("DomesticShipmentMgrDAL","GetEnterpriseOfAutoManifest","ERR005","Error opening connection : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}
			
			IDbCommand dbCmd = null;		
			dbCmd = dbCon.CreateCommand("",CommandType.Text);
			dbCmd.Connection = conApp;	

			return  GetEnterpriseOfAutoManifest( strAppID,  strEnterpriseID, iCurrent,  iDSRecSize, dbCon, dbCmd);	
		}

		public static SessionDS GetEnterpriseOfAutoManifest(String strAppID, String strEnterpriseID,
			int iCurrent, int iDSRecSize,DbConnection dbCon,IDbCommand dbCmd)
		{
			SessionDS sessionDS = new SessionDS();
			
			StringBuilder str = new StringBuilder();
			str.Append("SELECT * ");
			str.Append("FROM Enterprise ");
			str.Append("WHERE applicationid = '");
			str.Append(strAppID);
			str.Append("' and enterpriseid = '");
			str.Append(strEnterpriseID);
			str.Append("'");
			
			dbCmd.CommandText = str.ToString();

			//sessionDS = dbCon.ExecuteQuery(str.ToString(),iCurrent, iDSRecSize,"Enterprise");
			sessionDS.ds = (DataSet)dbCon.ExecuteQueryInTransaction(dbCmd,ReturnType.DataSetType);
			return  sessionDS;	
		}
		#region Comment by Sompote 2010-05-11 (Fix Database Lock)
		/*
				/// <summary>
				/// //Automatically Assing to Manifests (DMS Phase 1)
				/// GetEnterpriseHolidayOfAutoManifest
				/// </summary>
				public static bool IsEnterpriseHolidayOfAutoManifest(String strAppID, String strEnterpriseID, 
					DateTime dtNextDay, String StateCode)
				{
					int numOfRows = 0;

					DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

					if(dbCon == null)
					{
						Logger.LogTraceError("DomesticShipmentMgrDAL","IsEnterpriseHolidayOfAutoManifest","EDB101","DbConnection object is null!!");
						System.Diagnostics.Trace.WriteLineIf(Logger.AppSwitch.TraceError,"[DomesticShipmentMgrDAL::IsEnterpriseHolidayOfAutoManifest()] DbConnection is null","ERROR");
						return false;
					}

					String findData = dtNextDay.ToString("yyyy-MM-dd");

					StringBuilder str = new StringBuilder();
					str.Append("SELECT COUNT(*) ");
					str.Append("FROM Enterprise_Holiday ");
					str.Append("WHERE applicationid = '");
					str.Append(strAppID);
					str.Append("' and enterpriseid = '");
					str.Append(strEnterpriseID);
					str.Append("' and " + Utility.DateFormat(strAppID,strEnterpriseID,dtNextDay,DTFormat.Date) + " ");
					str.Append("between datefrom and dateto ");
					str.Append("and state_code = '" + StateCode + "'");
					numOfRows = (int)dbCon.ExecuteScalar(str.ToString());

					if (numOfRows > 0)
						return  true;
					else
						return false;
				}

				*/
		#endregion
		public static bool IsEnterpriseHolidayOfAutoManifest(String strAppID, String strEnterpriseID, 
			DateTime dtNextDay, String StateCode)
		{			

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("DomesticShipmentMgrDAL","IsEnterpriseHolidayOfAutoManifest","EDB101","DbConnection object is null!!");
				System.Diagnostics.Trace.WriteLineIf(Logger.AppSwitch.TraceError,"[DomesticShipmentMgrDAL::IsEnterpriseHolidayOfAutoManifest()] DbConnection is null","ERROR");
				return false;
			}

			return IsEnterpriseHolidayOfAutoManifest( strAppID,  strEnterpriseID, dtNextDay,  StateCode, dbCon);
		}

		public static bool IsEnterpriseHolidayOfAutoManifest(String strAppID, String strEnterpriseID, 
			DateTime dtNextDay, String StateCode,DbConnection dbCon)
		{			
			IDbConnection conApp = dbCon.GetConnection();
			if(conApp == null)
			{
				Logger.LogTraceError("DomesticShipmentMgrDAL","IsEnterpriseHolidayOfAutoManifest()","ERR003","conApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			try
			{
				dbCon.OpenConnection(ref conApp);
			}
			catch(ApplicationException appException)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("DomesticShipmentMgrDAL","IsEnterpriseHolidayOfAutoManifest()","ERR004","Error opening connection : "+ appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ",appException);
			}
			catch(Exception exception)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("DomesticShipmentMgrDAL","IsEnterpriseHolidayOfAutoManifest()","ERR005","Error opening connection : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}
			
			IDbCommand dbCmd = null;		
			dbCmd = dbCon.CreateCommand("",CommandType.Text);
			dbCmd.Connection = conApp;	


			return IsEnterpriseHolidayOfAutoManifest( strAppID,  strEnterpriseID, dtNextDay,  StateCode, dbCon, dbCmd);
		}

		public static bool IsEnterpriseHolidayOfAutoManifest(String strAppID, String strEnterpriseID, 
			DateTime dtNextDay, String StateCode,DbConnection dbCon,IDbCommand dbCmd)
		{
			int numOfRows = 0;
			
			String findData = dtNextDay.ToString("yyyy-MM-dd");

			StringBuilder str = new StringBuilder();
			str.Append("SELECT COUNT(*) ");
			str.Append("FROM Enterprise_Holiday ");
			str.Append("WHERE applicationid = '");
			str.Append(strAppID);
			str.Append("' and enterpriseid = '");
			str.Append(strEnterpriseID);
			str.Append("' and " + Utility.DateFormat(strAppID,strEnterpriseID,dtNextDay,DTFormat.Date) + " ");
			str.Append("between datefrom and dateto ");
			str.Append("and state_code = '" + StateCode + "'");
			//numOfRows = (int)dbCon.ExecuteScalar(str.ToString());
			dbCmd.CommandText = str.ToString();
			numOfRows = (int)dbCon.ExecuteScalarInTransaction(dbCmd);
			if (numOfRows > 0)
				return  true;
			else
				return false;
		}

		#region Comment by Sompote 2010-05-11 (Fix Database Lock)
		/*
		public static DateTime pickupDateTimeIncrement(DateTime pickupDate, String strAppID, String strEnterpriseID, String StateCode)
		{
			DataSet Enterprise = DomesticShipmentMgrDAL.GetEnterpriseOfAutoManifest(strAppID, strEnterpriseID,0,0).ds;
	
			String sat_delivery_avail = (String)Enterprise.Tables[0].Rows[0]["sat_delivery_avail"];
			String sun_delivery_avail = (String)Enterprise.Tables[0].Rows[0]["sun_delivery_avail"];
			String pub_delivery_avail = (String)Enterprise.Tables[0].Rows[0]["pub_delivery_avail"];

			DateTime newDate = pickupDate;
			bool flag = true;
			int incDay = 0;

			while(flag)
			{
				newDate = newDate.AddDays(1);
				
				// Saturday
				if(newDate.DayOfWeek == System.DayOfWeek.Saturday)
				{
					if(sat_delivery_avail.Trim() == "Y")
						flag = false;
					else
						incDay = incDay + 1;
				}

				// Sunday
				if(newDate.DayOfWeek == System.DayOfWeek.Sunday)
				{
					if(sun_delivery_avail.Trim() == "Y")
						flag = false;
					else
						incDay = incDay + 1;
				}

				// Monday - Friday
				if((newDate.DayOfWeek != System.DayOfWeek.Saturday) && (newDate.DayOfWeek != System.DayOfWeek.Sunday))
				{
					if(DomesticShipmentMgrDAL.IsEnterpriseHolidayOfAutoManifest(strAppID, strEnterpriseID, newDate, StateCode))
					{
						if(pub_delivery_avail.Trim() == "Y")
						{
							flag = false;
						}
						else
						{
							incDay = incDay + 1;
						}
					}
					else
					{
						flag = false;
					}
				}
			}

			incDay = incDay + 1;

			return pickupDate.AddDays(incDay);
		}

		*/
		#endregion

		public static DateTime pickupDateTimeIncrement(DateTime pickupDate, String strAppID, String strEnterpriseID, String StateCode)
		{			
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("DomesticShipmentMgrDAL","pickupDateTimeIncrement","EDB101","DbConnection object is null!!");
				System.Diagnostics.Trace.WriteLineIf(Logger.AppSwitch.TraceError,"[DomesticShipmentMgrDAL::pickupDateTimeIncrement()] DbConnection is null","ERROR");
				throw new ApplicationException("DbConnection is null ",null);
			}

			return pickupDateTimeIncrement( pickupDate,  strAppID,  strEnterpriseID,  StateCode, dbCon);
		}
		public static DateTime pickupDateTimeIncrement(DateTime pickupDate, String strAppID, String strEnterpriseID, String StateCode,DbConnection dbCon)
		{		
			IDbConnection conApp = dbCon.GetConnection();
			if(conApp == null)
			{
				Logger.LogTraceError("DomesticShipmentMgrDAL","pickupDateTimeIncrement","ERR003","conApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			try
			{
				dbCon.OpenConnection(ref conApp);
			}
			catch(ApplicationException appException)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("DomesticShipmentMgrDAL","pickupDateTimeIncrement","ERR004","Error opening connection : "+ appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ",appException);
			}
			catch(Exception exception)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("DomesticShipmentMgrDAL","pickupDateTimeIncrement","ERR005","Error opening connection : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}
			
			IDbCommand dbCmd = null;		
			dbCmd = dbCon.CreateCommand("",CommandType.Text);
			dbCmd.Connection = conApp;	

			return pickupDateTimeIncrement( pickupDate,  strAppID,  strEnterpriseID,  StateCode, dbCon, dbCmd);
		}

		public static DateTime pickupDateTimeIncrement(DateTime pickupDate, String strAppID, String strEnterpriseID, String StateCode,DbConnection dbCon,IDbCommand dbCmd)
		{
			DataSet Enterprise = DomesticShipmentMgrDAL.GetEnterpriseOfAutoManifest(strAppID, strEnterpriseID,0,0,dbCon,dbCmd).ds;
	
			String sat_delivery_avail = (String)Enterprise.Tables[0].Rows[0]["sat_delivery_avail"];
			String sun_delivery_avail = (String)Enterprise.Tables[0].Rows[0]["sun_delivery_avail"];
			String pub_delivery_avail = (String)Enterprise.Tables[0].Rows[0]["pub_delivery_avail"];

			DateTime newDate = pickupDate;
			bool flag = true;
			int incDay = 0;

			while(flag)
			{
				newDate = newDate.AddDays(1);
				
				// Saturday
				if(newDate.DayOfWeek == System.DayOfWeek.Saturday)
				{
					if(sat_delivery_avail.Trim() == "Y")
						flag = false;
					else
						incDay = incDay + 1;
				}

				// Sunday
				if(newDate.DayOfWeek == System.DayOfWeek.Sunday)
				{
					if(sun_delivery_avail.Trim() == "Y")
						flag = false;
					else
						incDay = incDay + 1;
				}

				// Monday - Friday
				if((newDate.DayOfWeek != System.DayOfWeek.Saturday) && (newDate.DayOfWeek != System.DayOfWeek.Sunday))
				{
					if(DomesticShipmentMgrDAL.IsEnterpriseHolidayOfAutoManifest(strAppID, strEnterpriseID, newDate, StateCode,dbCon,dbCmd))
					{
						if(pub_delivery_avail.Trim() == "Y")
						{
							flag = false;
						}
						else
						{
							incDay = incDay + 1;
						}
					}
					else
					{
						flag = false;
					}
				}
			}

			incDay = incDay + 1;

			return pickupDate.AddDays(incDay);
		}



		public static bool IsAssignedConsignment(String strAppID, String strEnterpriseID, String ConsignmentNo)
		{
			int numOfRows = 0;

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("DomesticShipmentMgrDAL","IsEnterpriseHolidayOfAutoManifest","EDB101","DbConnection object is null!!");
				System.Diagnostics.Trace.WriteLineIf(Logger.AppSwitch.TraceError,"[DomesticShipmentMgrDAL::IsEnterpriseHolidayOfAutoManifest()] DbConnection is null","ERROR");
				return false;
			}


			StringBuilder str = new StringBuilder();
			str.Append("SELECT COUNT(*) ");
			str.Append("FROM shipment ");
			str.Append("WHERE applicationid = '");
			str.Append(strAppID);
			str.Append("' and enterpriseid = '");
			str.Append(strEnterpriseID);
			str.Append("' and consignment_no = '");
			str.Append(ConsignmentNo+"'");

			numOfRows = (int)dbCon.ExecuteScalar(str.ToString());

			if (numOfRows > 0)
				return  true;
			else
				return false;
		}

		//Phase2 - J03
		public static SessionDS getReferencesByTelephoneNumber(String strAppID, String strEnterpriseID,
			int iCurrent, int iDSRecSize, String strTelephoneNumber)
		{
			SessionDS sessionDS = null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("DomesticShipmentMgrDAL","GetEnterpriseOfAutoManifest","EDB101","DbConnection object is null!!");
				return sessionDS;
			}

			StringBuilder strQry = new StringBuilder();
			strQry.Append("SELECT telephone, reference_name, address1, address2, zipcode, fax, contactperson ");
			strQry.Append("FROM [References] ");
			strQry.Append("WHERE applicationid='");
			strQry.Append(strAppID);
			strQry.Append("' and enterpriseid = '");
			strQry.Append(strEnterpriseID);
			strQry.Append("' and telephone = '");
			strQry.Append(strTelephoneNumber);
			strQry.Append("' ");
					
			sessionDS = dbCon.ExecuteQuery(strQry.ToString(),iCurrent, iDSRecSize,"Enterprise");
			return  sessionDS;	
		}
		//Phase2 - J03


		public static SessionDS getDCToOrigStation(String strAppID, String strEnterpriseID,
			int iCurrent, int iDSRecSize, String strSenderZipcode)
		{
			SessionDS sessionDS = null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("DomesticShipmentMgrDAL","getDCToOrigStation","EDB101","DbConnection object is null!!");
				return sessionDS;
			}

			StringBuilder strQry = new StringBuilder();
			strQry.Append("SELECT distinct b.origin_station ");
			strQry.Append("FROM zipcode a ");
			strQry.Append("INNER JOIN Delivery_Path b ");
			strQry.Append("ON (a.applicationid = b.applicationid ");
			strQry.Append("AND a.enterpriseid = b.enterpriseid ");
			strQry.Append("AND a.pickup_route = b.path_code) ");
			strQry.Append("WHERE a.applicationid = '" + strAppID + "' ");
			strQry.Append("AND a.enterpriseid = '" + strEnterpriseID + "' ");
			strQry.Append("AND a.zipcode = '" + strSenderZipcode + "'");
					
			sessionDS = dbCon.ExecuteQuery(strQry.ToString(),iCurrent, iDSRecSize,"DCOriginStation");
			return  sessionDS;	
		}


		public static SessionDS getDCToDestStation(String strAppID, String strEnterpriseID,
			int iCurrent, int iDSRecSize, String strRouteCode)
		{
			SessionDS sessionDS = null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("DomesticShipmentMgrDAL","getDCToDestStation","EDB101","DbConnection object is null!!");
				return sessionDS;
			}

			StringBuilder strQry = new StringBuilder();
			strQry.Append("SELECT distinct path_code, origin_station ");
			strQry.Append("FROM Delivery_Path ");
			strQry.Append("WHERE applicationid = '" + strAppID + "' ");
			strQry.Append("AND enterpriseid = '" + strEnterpriseID + "' ");
			strQry.Append("AND path_code = '" + strRouteCode + "'");
					
			sessionDS = dbCon.ExecuteQuery(strQry.ToString(),iCurrent, iDSRecSize,"DCDestStation");
			return  sessionDS;	
		}

		//HC Return Task
		public static DataSet GetCoreSysetmCodeNumVal(String strAppID, String strEnterpriseID,
			String strCodeID, int iCurRow, int iDSRowSize)
		{
			SessionDS sessionDS = null;			
			String strSQL = "";

			strSQL = "SELECT code_num_value "; 	
			strSQL += "FROM core_system_code ";
			strSQL += "WHERE applicationid = '" + strAppID + "' ";
			strSQL += "AND codeid = '" + strCodeID + "' ";
			strSQL += "ORDER BY sequence asc ";
	
			
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("ImportConsignmentsDAL","GetCoreSysetmCodeNumVal","GetCoreSysetmCodeNumVal","DbConnection object is null!!");
				return sessionDS.ds;
			}
			
			
			sessionDS = dbCon.ExecuteQuery(strSQL, iCurRow, iDSRowSize, "core_system_code_num_val");
			return  sessionDS.ds;
		}
		#region Comment by Sompote 2010-05-07 (Fix Database Lock)
		/*
		public static object getPODDateForHC(String strAppID, String strEnterpriseID, String ConsignmentNo,String strBookingNo)
		{
			SessionDS sessionDS = null;

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("DomesticShipmentMgrDAL","getPODDateForHC","EDB101","DbConnection object is null!!");
				return sessionDS;
			}
			
			StringBuilder strQry = new StringBuilder();
			strQry.Append("SELECT * ");
			strQry.Append("FROM shipment_tracking ");
			strQry.Append("WHERE applicationid = '"+strAppID+"' ");
			strQry.Append("AND enterpriseid = '"+strEnterpriseID+"' ");
			strQry.Append("AND consignment_no = '"+ConsignmentNo+"' ");
			strQry.Append("AND booking_no ="+ strBookingNo + " ");
			strQry.Append("AND (ltrim(rtrim(status_code)) = 'POD' and upper(rtrim(ltrim(isnull(deleted, 'N')))) <> 'Y')");

			sessionDS = dbCon.ExecuteQuery(strQry.ToString(), 0, 0, "PODHC");

			if((sessionDS != null) && 
				(!sessionDS.GetType().Equals(System.Type.GetType("System.DBNull"))))
		{
				if((sessionDS.ds != null) && 
					(!sessionDS.ds.GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					if (sessionDS.ds.Tables[0].Rows.Count > 0)
					{
						return (DateTime)sessionDS.ds.Tables[0].Rows[0]["tracking_datetime"];
					}
					else
					{
						return System.DBNull.Value;
					}
				}
				else
				{
					return System.DBNull.Value;
				}
			}
			else
			{
				return System.DBNull.Value;
			}
		}
		*/
		#endregion
		public static object getPODDateForHC(String strAppID, String strEnterpriseID, String ConsignmentNo,String strBookingNo)
		{			
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("DomesticShipmentMgrDAL","getPODDateForHC","EDB101","DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null",null);
			}
			return  getPODDateForHC( strAppID,  strEnterpriseID,  ConsignmentNo, strBookingNo, dbCon);
		}
		public static object getPODDateForHC(String strAppID, String strEnterpriseID, String ConsignmentNo,String strBookingNo,DbConnection dbCon)
		{
			IDbConnection conApp = dbCon.GetConnection();
			if(conApp == null)
			{
				Logger.LogTraceError("DomesticShipmentMgrDAL","getPODDateForHC","ERR003","conApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			try
			{
				dbCon.OpenConnection(ref conApp);
			}
			catch(ApplicationException appException)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("DomesticShipmentMgrDAL","getPODDateForHC","ERR004","Error opening connection : "+ appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ",appException);
			}
			catch(Exception exception)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("DomesticShipmentMgrDAL","getPODDateForHC","ERR005","Error opening connection : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}
			
			IDbCommand dbCmd = null;		
			dbCmd = dbCon.CreateCommand("",CommandType.Text);
			dbCmd.Connection = conApp;	
			return getPODDateForHC( strAppID,  strEnterpriseID,  ConsignmentNo, strBookingNo, dbCon, dbCmd);
		}

		public static object getPODDateForHC(String strAppID, String strEnterpriseID, String ConsignmentNo,String strBookingNo,DbConnection dbCon,IDbCommand dbCmd)
		{
			SessionDS sessionDS = null;			

			StringBuilder strQry = new StringBuilder();
			strQry.Append("SELECT * ");
			//strQry.Append("FROM shipment_tracking with(rowlock) ");
			strQry.Append("FROM shipment_tracking ");
			strQry.Append("WHERE applicationid = '"+strAppID+"' ");
			strQry.Append("AND enterpriseid = '"+strEnterpriseID+"' ");
			strQry.Append("AND consignment_no = '"+ConsignmentNo+"' ");
			strQry.Append("AND booking_no ="+ strBookingNo + " ");
			strQry.Append("AND (ltrim(rtrim(status_code)) = 'POD' and upper(rtrim(ltrim(isnull(deleted, 'N')))) <> 'Y')");

			dbCmd.CommandText = strQry.ToString();
			//sessionDS = (SessionDS)dbCon.ExecuteQueryInTransaction(dbCmd,ReturnType.DataSetType);
			sessionDS = new SessionDS();
			sessionDS.ds = (DataSet)dbCon.ExecuteQueryInTransaction(dbCmd,ReturnType.DataSetType);

			if((sessionDS != null) && 
				(!sessionDS.GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				if((sessionDS.ds != null) && 
					(!sessionDS.ds.GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					if (sessionDS.ds.Tables[0].Rows.Count > 0)
					{
						return (DateTime)sessionDS.ds.Tables[0].Rows[0]["tracking_datetime"];
					}
					else
					{
						return System.DBNull.Value;
					}
				}
				else
				{
					return System.DBNull.Value;
				}
			}
			else
			{
				return System.DBNull.Value;
			}
		}


		#region Comment by Sompote 2010-05-07 (Fix Database Lock)
		/*
		public static object calEstHCDateTime(DateTime EstDt, String strStateCode, 
			String strConsignmentNo, String strBookingNo, 
			String strAppID, String strEnterpriseID)
		{
			if (strConsignmentNo != "" && strBookingNo != "")
			{
				object ActPODDt = DomesticShipmentMgrDAL.getPODDateForHC(strAppID, strEnterpriseID, strConsignmentNo, strBookingNo);

				if((ActPODDt != null) && 
					(!ActPODDt.GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					EstDt = (DateTime)ActPODDt;
				}
			}
			
			DataSet tmpHCReturnDay = SysDataManager2.GetHCReturnDayFromState(strAppID, strEnterpriseID, strStateCode, 0, 0).ds;
				
			if (tmpHCReturnDay != null)
			{
				if((tmpHCReturnDay.Tables[0].Rows[0]["hc_return_within_days"] != null) && 
					(!tmpHCReturnDay.Tables[0].Rows[0]["hc_return_within_days"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
					(tmpHCReturnDay.Tables[0].Rows[0]["hc_return_within_days"].ToString() != ""))
				{
					int hc_return_days = 0;
					hc_return_days = Convert.ToInt32(tmpHCReturnDay.Tables[0].Rows[0]["hc_return_within_days"].ToString().Trim());

					String sat_delivery_avail = "Y";
					String sun_delivery_avail = "N";
					String pub_delivery_avail = "N";

					DataSet tmpDs = ImportConsignmentsDAL.GetCoreSysetmCodeNumVal(strAppID, strEnterpriseID,
						"hc_operates_sunday", 0, 0);

					if (tmpDs.Tables[0].Rows.Count > 0)
					{
						if((tmpDs.Tables[0].Rows[0]["code_num_value"] != null) && 
							(!tmpDs.Tables[0].Rows[0]["code_num_value"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
							(tmpDs.Tables[0].Rows[0]["code_num_value"].ToString() != ""))
						{
							if (tmpDs.Tables[0].Rows[0]["code_num_value"].ToString().Trim() == "0")
								sun_delivery_avail = "N";
							else
								sun_delivery_avail = "Y";
						}
					}

					bool flag = true;

					while(flag)
					{
						if (hc_return_days > 0)
						{
							EstDt = EstDt.AddDays(1);
						
							// Saturday
							if(EstDt.DayOfWeek == System.DayOfWeek.Saturday)
							{
								if(sat_delivery_avail.Trim() == "Y") 
									hc_return_days = hc_return_days - 1;
							}

							// Sunday
							if(EstDt.DayOfWeek == System.DayOfWeek.Sunday)
							{
								if(sun_delivery_avail.Trim() == "Y")
									hc_return_days = hc_return_days - 1;
							}

							
							// Monday - Friday
							if((EstDt.DayOfWeek != System.DayOfWeek.Saturday) && (EstDt.DayOfWeek != System.DayOfWeek.Sunday))
							{
								bool isPubDay = DomesticShipmentMgrDAL.IsEnterpriseHolidayOfAutoManifest(strAppID, strEnterpriseID, EstDt, strStateCode);

								if(isPubDay)
								{
									if(pub_delivery_avail.Trim() == "Y")
										hc_return_days = hc_return_days - 1;
								}
								else
								{
									hc_return_days = hc_return_days - 1;
								}
							}
						}
						else
						{
							flag = false;
						}
					}
				
					DateTime tmpestHCDt = new DateTime(EstDt.Year,EstDt.Month,EstDt.Day, 23, 59, 59);
					return tmpestHCDt;
				}
				else
				{
					return System.DBNull.Value;
				}
			}
			else
			{
				return System.DBNull.Value;
			}	
		}
	
		*/
		#endregion
		public static object calEstHCDateTime(DateTime EstDt, String strStateCode, 
			String strConsignmentNo, String strBookingNo, 
			String strAppID, String strEnterpriseID)
		{
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("DomesticShipmentMgrDAL","calEstHCDateTime","EDB101","DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null",null);
			}
			return calEstHCDateTime( EstDt,  strStateCode, 
				strConsignmentNo,  strBookingNo, 
				strAppID,  strEnterpriseID, dbCon);
		}

		public static object calEstHCDateTime(DateTime EstDt, String strStateCode, 
			String strConsignmentNo, String strBookingNo, 
			String strAppID, String strEnterpriseID,DbConnection dbCon)
		{
			IDbConnection conApp = dbCon.GetConnection();
			if(conApp == null)
			{
				Logger.LogTraceError("DomesticShipmentMgrDAL","calEstHCDateTime","ERR003","conApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			try
			{
				dbCon.OpenConnection(ref conApp);
			}
			catch(ApplicationException appException)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("DomesticShipmentMgrDAL","calEstHCDateTime","ERR004","Error opening connection : "+ appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ",appException);
			}
			catch(Exception exception)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("DomesticShipmentMgrDAL","calEstHCDateTime","ERR005","Error opening connection : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}
			
			IDbCommand dbCmd = null;		
			dbCmd = dbCon.CreateCommand("",CommandType.Text);
			dbCmd.Connection = conApp;	
			/*return calEstHCDateTime( EstDt,  strStateCode, 
				strConsignmentNo,  strBookingNo, 
				strAppID,  strEnterpriseID, dbCon, dbCmd);*/
			object result ;	
			try
			{
				result = calEstHCDateTime( EstDt,  strStateCode, 
					strConsignmentNo,  strBookingNo, 
					strAppID,  strEnterpriseID, dbCon, dbCmd);
			}
			catch(ApplicationException appExpection)
			{
				throw appExpection;
			}
			finally
			{
				if(conApp != null)
				{
					conApp.Close();
				}
			}
			return result;
		}

		public static object calEstHCDateTime(DateTime EstDt, String strStateCode, 
			String strConsignmentNo, String strBookingNo, 
			String strAppID, String strEnterpriseID,DbConnection dbCon,IDbCommand dbCmd)
		{
			if (strConsignmentNo != "" && strBookingNo != "")
			{
				object ActPODDt = DomesticShipmentMgrDAL.getPODDateForHC(strAppID, strEnterpriseID, strConsignmentNo, strBookingNo,dbCon,dbCmd);

				if((ActPODDt != null) && 
					(!ActPODDt.GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					EstDt = (DateTime)ActPODDt;
				}
			}

			DataSet tmpHCReturnDay = SysDataManager2.GetHCReturnDayFromState(strAppID, strEnterpriseID, strStateCode, 0, 0).ds;
				
			if (tmpHCReturnDay != null)
			{
				if((tmpHCReturnDay.Tables[0].Rows[0]["hc_return_within_days"] != null) && 
					(!tmpHCReturnDay.Tables[0].Rows[0]["hc_return_within_days"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
					(tmpHCReturnDay.Tables[0].Rows[0]["hc_return_within_days"].ToString() != ""))
				{
					int hc_return_days = 0;
					hc_return_days = Convert.ToInt32(tmpHCReturnDay.Tables[0].Rows[0]["hc_return_within_days"].ToString().Trim());

					String sat_delivery_avail = "Y";
					String sun_delivery_avail = "N";
					String pub_delivery_avail = "N";

					DataSet tmpDs = ImportConsignmentsDAL.GetCoreSysetmCodeNumVal(strAppID, strEnterpriseID,
						"hc_operates_sunday", 0, 0);

					if (tmpDs.Tables[0].Rows.Count > 0)
					{
						if((tmpDs.Tables[0].Rows[0]["code_num_value"] != null) && 
							(!tmpDs.Tables[0].Rows[0]["code_num_value"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
							(tmpDs.Tables[0].Rows[0]["code_num_value"].ToString() != ""))
						{
							if (tmpDs.Tables[0].Rows[0]["code_num_value"].ToString().Trim() == "0")
								sun_delivery_avail = "N";
							else
								sun_delivery_avail = "Y";
						}
					}

					bool flag = true;

					while(flag)
					{
						if (hc_return_days > 0)
						{
							EstDt = EstDt.AddDays(1);
							
							// Saturday
							if(EstDt.DayOfWeek == System.DayOfWeek.Saturday)
							{
								if(sat_delivery_avail.Trim() == "Y") 
									hc_return_days = hc_return_days - 1;
							}

							// Sunday
							if(EstDt.DayOfWeek == System.DayOfWeek.Sunday)
							{
								if(sun_delivery_avail.Trim() == "Y")
									hc_return_days = hc_return_days - 1;
							}

							// Monday - Friday
							if((EstDt.DayOfWeek != System.DayOfWeek.Saturday) && (EstDt.DayOfWeek != System.DayOfWeek.Sunday))
							{
								bool isPubDay = DomesticShipmentMgrDAL.IsEnterpriseHolidayOfAutoManifest(strAppID, strEnterpriseID, EstDt, strStateCode);

								if(isPubDay)
								{
									if(pub_delivery_avail.Trim() == "Y")
										hc_return_days = hc_return_days - 1;
								}
								else
								{
									hc_return_days = hc_return_days - 1;
								}
							}
						}
						else
						{
							flag = false;
						}
					}
				
					DateTime tmpestHCDt = new DateTime(EstDt.Year,EstDt.Month,EstDt.Day, 23, 59, 59);
					return tmpestHCDt;
				}
				else
				{
					return System.DBNull.Value;
				}
			}
			else
			{
				return System.DBNull.Value;
			}
		}

		//HC Return Task

		// DomesticShipment Calculate Freight charge
	
		public static decimal CalculateAllFreightCharge(string appID,string enterpriseID,
			string payerID,string ServiceCode,string senderZipcode,string RecipZipCode,string PkgChargeWt,
			string ShpDclrValue,DataSet dsPkgDetails,decimal fCalcFrgtChrg)
		{			
			fCalcFrgtChrg = 0;
			String strLocalErrorMsg = "";

			if (strLocalErrorMsg.Trim() == "")
			{
				Zipcode zipcode = new Zipcode();
				zipcode.Populate(appID,enterpriseID,senderZipcode);
				String strOrgZone = zipcode.ZoneCode;

				zipcode.Populate(appID,enterpriseID,RecipZipCode);
				String strDestnZone = zipcode.ZoneCode;

				DataSet tmpCustZone = null;
				string sendCustZone = null;
				string recipCustZone = null;
				tmpCustZone = SysDataMgrDAL.GetCustZoneData(appID, enterpriseID,
					0, 0, payerID.Trim(), 
					senderZipcode.Trim()).ds;
				if(tmpCustZone.Tables[0].Rows.Count > 0)
				{
					foreach(DataRow dr in tmpCustZone.Tables[0].Rows)
					{
						sendCustZone = dr["zone_code"].ToString().Trim();
					}
				}
				tmpCustZone = SysDataMgrDAL.GetCustZoneData(appID, enterpriseID,
					0, 0, payerID.Trim(), RecipZipCode.Trim()).ds;
				if(tmpCustZone.Tables[0].Rows.Count > 0)
				{
					foreach(DataRow dr in tmpCustZone.Tables[0].Rows)
					{
						recipCustZone = dr["zone_code"].ToString().Trim();
					}
				}

				//float fCalcFrgtChrg = 0;

				try
				{
					//Compute the Freight Charge	
					//Required fields before calculate the Freight Charge:
					//CustID, CustType, SendZip, RecipZip, Package Detail, Service Code
					///DataSet dsPkgDetails = (DataSet)Session["SESSION_DS2"];
					Customer customer = new Customer();
					customer.Populate(appID,enterpriseID, payerID);
					decimal freightCharge = 0;
					decimal freightCharge0 = 0;
					decimal sumFreight = 0;

					if(customer.IsCustomer_Box == "Y")
					{
						float weightToFreight = 0;
						int sumQty = 0;

						//						if(!customer.IsBoxRateAvailable(appID,enterpriseID,sendCustZone,recipCustZone,ServiceCode.Trim()))
						//						{
						////						lblErrorMsg.Text = "Customer has no Box Rate defined.";
						//							return -2;
						//						}
						//						else
						//						{
						foreach(DataRow dr in dsPkgDetails.Tables[0].Rows)
						{
							//--- Dim by total = Yes 
							//----------- �ҡ����������ҧ Total R Wt ��� Total R Dim Wt ����˹�ҡ���ҡѹ��ҽ�觹�� (�ء Rows ����ѹ)
							//----------- ���������ʢ�ҧ�� ����ҷ��� Row ��Ҥ�������ҧ Total R Wt ��� Total R Dim Wt ����˹�ҡ���ҡѹ ����˹�ҡ��ҽ�觹��
							//----------- ��Ҥ�Ңͧ Dim by total = No ��� Apply Dim Wt �� No �����Ҥ�Ҩҡ Total R Wt
							if(customer.Dim_By_tot == "Y")
							{
								weightToFreight = float.Parse(dr["chargeable_wt"].ToString())/int.Parse(dr["pkg_qty"].ToString());
							}
							else
							{
								if(customer.Dim_By_tot == "N" && customer.ApplyDimWt=="Y")
								{
									if(float.Parse(dr["tot_wt"].ToString())>float.Parse(dr["tot_dim_wt"].ToString()))
									{
										weightToFreight = float.Parse(dr["tot_wt"].ToString())/int.Parse(dr["pkg_qty"].ToString());
									}
									else
									{
										weightToFreight = float.Parse(dr["tot_dim_wt"].ToString())/int.Parse(dr["pkg_qty"].ToString());
									}									
								}
								else 
								{
									weightToFreight = float.Parse(dr["tot_wt"].ToString())/int.Parse(dr["pkg_qty"].ToString());
								}
							}


							sumQty = sumQty+int.Parse(dr["pkg_qty"].ToString());
							freightCharge = (decimal)TIESUtility.ComputeFreightCharge(appID, enterpriseID,
								payerID.Trim(), "C", 
								strOrgZone, strDestnZone, weightToFreight,
								ServiceCode, senderZipcode.Trim(), RecipZipCode.Trim());
							freightCharge = (freightCharge * (int.Parse(dr["pkg_qty"].ToString())));
							sumFreight = sumFreight+freightCharge;
						}
						if(sumQty < int.Parse(customer.Minimum_Box))
						{
							freightCharge0 = (decimal)TIESUtility.ComputeFreightCharge(appID, enterpriseID,
								payerID.Trim(), "C", 
								strOrgZone, strDestnZone, 0,
								ServiceCode, senderZipcode.Trim(), RecipZipCode.Trim());
							fCalcFrgtChrg = ((int.Parse(customer.Minimum_Box)-sumQty)*freightCharge0)+sumFreight;
						}
						else
						{
							fCalcFrgtChrg = sumFreight;
						}
					}
					else
					{
						fCalcFrgtChrg = (decimal)TIESUtility.ComputeFreightCharge(appID, enterpriseID,
							payerID.Trim(), "C", 
							strOrgZone, strDestnZone, (float)Convert.ToDecimal(PkgChargeWt),
							ServiceCode, senderZipcode.Trim(), RecipZipCode.Trim());
					}
					//						if(fCalcFrgtChrg.ToString("#0.00").Equals("0.00") && PkgChargeWt.Length >0 && ShpDclrValue.Trim().Length >0)
					//						{
					//							fCalcFrgtChrg= -1;
					//						}
					//						else
					//						{
					//Comment and add new statement by Panas 24/04/2009
					//fCalcFrgtChrg = String.Format((String)ViewState["m_format"], Rounding(fCalcFrgtChrg));
					return fCalcFrgtChrg;
					//						}
						
				}
				catch(ApplicationException appException)
				{
					throw  appException;
				}

			}
			return fCalcFrgtChrg;
		}

		public static decimal GetOtherSurcharge(string appID,string enterpriseID,string PayerID,string fCalcFrgtChrg)

		{
			Customer customer = new Customer();
			customer.Populate(appID,enterpriseID,PayerID);

			decimal OtherSurcharge = 0;
			if((customer.OtherSurchargeAmount == null || customer.OtherSurchargeAmount == "") && (customer.OtherSurchargeAmountPercentage == null || customer.OtherSurchargeAmountPercentage == ""))
			{
				OtherSurcharge = 0;
			}
			else
			{
				if(customer.OtherSurchargeAmount != "")
				{ 
					OtherSurcharge = Convert.ToDecimal(customer.OtherSurchargeAmount);
				}
				else if(customer.OtherSurchargeAmountPercentage != null || customer.OtherSurchargeAmountPercentage != "")
				{
					OtherSurcharge = (Convert.ToDecimal(customer.OtherSurchargeAmountPercentage) * Convert.ToDecimal(fCalcFrgtChrg))/100;

					if(customer.OtherSurchargeMin != null && customer.OtherSurchargeMin != "")
						OtherSurcharge = Math.Max(OtherSurcharge, Convert.ToDecimal(customer.OtherSurchargeMin));
					if(customer.OtherSurchargeMax != null && customer.OtherSurchargeMax != "")
						OtherSurcharge = Math.Min(OtherSurcharge, Convert.ToDecimal(customer.OtherSurchargeMax));
				}
			}
			return OtherSurcharge;
		}
		//NEW ESA CALCULATION by X OCT 25 08 Move Code & Revise By Hong 11/04/2011
		public static decimal calCulateESA_ServiceType(string appID,string enterpriseID,string PayerID,string SenderZipcode,string RecipZipcode,string serviceCode,string fCalcFrgtChrg)
		{
			decimal decSrchrg = 0;

			//Calculate the ESA surcharge Revise By Hong 11/04/2011
			if( RecipZipcode.Length > 0 && RecipZipcode != null && 
				SenderZipcode.Length > 0 && SenderZipcode != null /*&& IsCalculate("original_rated_esa")*/)
			{
				String strApplyDimWt = null;
				String strApplyESA = null;
				String strApplyESADel = null;

				Customer customer = new Customer();
				customer.Populate(appID, enterpriseID, PayerID);
				strApplyDimWt = customer.ApplyDimWt;
				strApplyESA = customer.ESASurcharge;
				strApplyESADel = customer.ESASurchargeDel;
				decimal surchargeOfSender = 0;
				decimal surchargeOfRecip = 0;
				

				if(strApplyESA == "Y")
				{					
					//***************** Get surcharge from Customer Zone by Sender Zipcode *************************
				
					DataSet tmpCustZone = SysDataManager2.GetESASurchargeDS(appID,enterpriseID,
						PayerID.Trim(), SenderZipcode.Trim(),serviceCode.Trim());
					//return ESA Surcharge All just send CustID and ZipCode 

					//					 tmpCustZone = SysDataMgrDAL.GetCustZoneData(appID, enterpriseID,
					//						0, 0, PayerID.Trim(), SenderZipcode.Trim()).ds;
					if(tmpCustZone.Tables[0].Rows.Count > 0)
					{
						//Modify by Gwang on 4-Feb-08
						//A both of them have a value
						if(((tmpCustZone.Tables[0].Rows[0]["esa_surcharge_amt"] != null) && 
							(!tmpCustZone.Tables[0].Rows[0]["esa_surcharge_amt"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
							(tmpCustZone.Tables[0].Rows[0]["esa_surcharge_amt"].ToString() != "")) &&
							((tmpCustZone.Tables[0].Rows[0]["esa_surcharge_percent"] != null) && 
							(!tmpCustZone.Tables[0].Rows[0]["esa_surcharge_percent"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
							(tmpCustZone.Tables[0].Rows[0]["esa_surcharge_percent"].ToString() != ""))
							)
						{
							//At least is more than zero
							if(((decimal)tmpCustZone.Tables[0].Rows[0]["esa_surcharge_amt"] != 0) || ((decimal)tmpCustZone.Tables[0].Rows[0]["esa_surcharge_percent"] != 0))
							{
								if((decimal)tmpCustZone.Tables[0].Rows[0]["esa_surcharge_amt"] != 0)
								{
									surchargeOfSender = (decimal)tmpCustZone.Tables[0].Rows[0]["esa_surcharge_amt"];
								}
								else if((decimal)tmpCustZone.Tables[0].Rows[0]["esa_surcharge_percent"] != 0)
								{
									decimal tmpFreight = 0;

									if (fCalcFrgtChrg.Trim() != "")
										tmpFreight = Convert.ToDecimal(fCalcFrgtChrg.Trim());

									surchargeOfSender = (tmpFreight * ((decimal)tmpCustZone.Tables[0].Rows[0]["esa_surcharge_percent"])) / 100;
								}

							}
								//A both of them is Zero.the surcharge is Zero
							else
							{
								surchargeOfSender = 0;								
							}
						}
							//At least one has a value
						else if(((tmpCustZone.Tables[0].Rows[0]["esa_surcharge_amt"] != null) && 
							(!tmpCustZone.Tables[0].Rows[0]["esa_surcharge_amt"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
							(tmpCustZone.Tables[0].Rows[0]["esa_surcharge_amt"].ToString() != "")) ||
							((tmpCustZone.Tables[0].Rows[0]["esa_surcharge_percent"] != null) && 
							(!tmpCustZone.Tables[0].Rows[0]["esa_surcharge_percent"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
							(tmpCustZone.Tables[0].Rows[0]["esa_surcharge_percent"].ToString() != ""))
							)
						{
							if((tmpCustZone.Tables[0].Rows[0]["esa_surcharge_amt"] != null) && 
								(!tmpCustZone.Tables[0].Rows[0]["esa_surcharge_amt"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
								(tmpCustZone.Tables[0].Rows[0]["esa_surcharge_amt"].ToString() != ""))
							{
								surchargeOfSender = (decimal)tmpCustZone.Tables[0].Rows[0]["esa_surcharge_amt"];
							}

							if((tmpCustZone.Tables[0].Rows[0]["esa_surcharge_percent"] != null) && 
								(!tmpCustZone.Tables[0].Rows[0]["esa_surcharge_percent"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
								(tmpCustZone.Tables[0].Rows[0]["esa_surcharge_percent"].ToString() != ""))
							{
								decimal tmpFreight = 0;
									
								if (fCalcFrgtChrg.Trim() != "")
									tmpFreight = Convert.ToDecimal(fCalcFrgtChrg.Trim());

								surchargeOfSender = (tmpFreight * ((decimal)tmpCustZone.Tables[0].Rows[0]["esa_surcharge_percent"])) / 100;
							}
						}
							//A both of them has Null/""
						else
						{	
							//							Zipcode zipcode = new Zipcode();
							//							zipcode.Populate(appID,enterpriseID,SenderZipcode.Trim());
							surchargeOfSender = 0;//zipcode.EASSurcharge;
						}

						//Apply MIN MAX ONLY FOR PERCENT

						if((tmpCustZone.Tables[0].Rows[0]["esa_surcharge_percent"] != null) && 
							(!tmpCustZone.Tables[0].Rows[0]["esa_surcharge_percent"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
							(tmpCustZone.Tables[0].Rows[0]["esa_surcharge_percent"].ToString() != ""))
						{
							if((decimal)tmpCustZone.Tables[0].Rows[0]["esa_surcharge_percent"] != 0)
							{
								if((tmpCustZone.Tables[0].Rows[0]["min_esa_amount"] != null) && 
									(!tmpCustZone.Tables[0].Rows[0]["min_esa_amount"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
									(tmpCustZone.Tables[0].Rows[0]["min_esa_amount"].ToString() != ""))
									if(surchargeOfSender<(decimal)tmpCustZone.Tables[0].Rows[0]["min_esa_amount"])					
										surchargeOfSender=(decimal)tmpCustZone.Tables[0].Rows[0]["min_esa_amount"];
							
								if((tmpCustZone.Tables[0].Rows[0]["max_esa_amount"] != null) && 
									(!tmpCustZone.Tables[0].Rows[0]["max_esa_amount"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
									(tmpCustZone.Tables[0].Rows[0]["max_esa_amount"].ToString() != ""))
									if(surchargeOfSender>(decimal)tmpCustZone.Tables[0].Rows[0]["max_esa_amount"])					
										surchargeOfSender=(decimal)tmpCustZone.Tables[0].Rows[0]["max_esa_amount"];
							}
						}
						//END Apply MIN MAX

					}
				
					else //NO DATA FROM DB
					{	
						//						Zipcode zipcode = new Zipcode();
						//						zipcode.Populate(appID,enterpriseID,SenderZipcode.Trim());
						surchargeOfSender = 0;//zipcode.EASSurcharge;
					}
				}
				else //NOT APPLY ESA
				{
					surchargeOfSender = 0;
				}

				if(strApplyESADel == "Y")
				{

					//***************** Get surcharge from Customer Zone by Recept Zipcode *************************
					DataSet tmpCustZone = SysDataManager2.GetESASurchargeDS(appID, enterpriseID,
						PayerID.Trim(), RecipZipcode.Trim(),serviceCode.Trim());

					//					DataSet tmpCustZone = SysDataMgrDAL.GetCustZoneData(appID, enterpriseID,
					//						0, 0, PayerID.Trim(), RecipZipcode.Trim()).ds;
					if(tmpCustZone.Tables[0].Rows.Count > 0)
					{
						//Modify by Gwang on 4-Feb-08
						//A both of them have a value
						if(((tmpCustZone.Tables[0].Rows[0]["esa_surcharge_amt"] != null) && 
							(!tmpCustZone.Tables[0].Rows[0]["esa_surcharge_amt"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
							(tmpCustZone.Tables[0].Rows[0]["esa_surcharge_amt"].ToString() != "")) &&
							((tmpCustZone.Tables[0].Rows[0]["esa_surcharge_percent"] != null) && 
							(!tmpCustZone.Tables[0].Rows[0]["esa_surcharge_percent"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
							(tmpCustZone.Tables[0].Rows[0]["esa_surcharge_percent"].ToString() != ""))
							)
						{
							//A is more than zero
							if(((decimal)tmpCustZone.Tables[0].Rows[0]["esa_surcharge_amt"] != 0) || ((decimal)tmpCustZone.Tables[0].Rows[0]["esa_surcharge_percent"] != 0))
							{
								if((decimal)tmpCustZone.Tables[0].Rows[0]["esa_surcharge_amt"] != 0)
								{
									surchargeOfRecip = (decimal)tmpCustZone.Tables[0].Rows[0]["esa_surcharge_amt"];
								}
								else if((decimal)tmpCustZone.Tables[0].Rows[0]["esa_surcharge_percent"] != 0)
								{
									decimal tmpFreight = 0;

									if (fCalcFrgtChrg.Trim() != "")
										tmpFreight = Convert.ToDecimal(fCalcFrgtChrg.Trim());

									surchargeOfRecip = (tmpFreight * ((decimal)tmpCustZone.Tables[0].Rows[0]["esa_surcharge_percent"])) / 100;
								}

							}
								//A both of them is Zero.the surcharge is Zero
							else
							{
								surchargeOfRecip = 0;								
							}
						}
							//At least one has a value
						else if(((tmpCustZone.Tables[0].Rows[0]["esa_surcharge_amt"] != null) && 
							(!tmpCustZone.Tables[0].Rows[0]["esa_surcharge_amt"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
							(tmpCustZone.Tables[0].Rows[0]["esa_surcharge_amt"].ToString() != "")) ||
							((tmpCustZone.Tables[0].Rows[0]["esa_surcharge_percent"] != null) && 
							(!tmpCustZone.Tables[0].Rows[0]["esa_surcharge_percent"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
							(tmpCustZone.Tables[0].Rows[0]["esa_surcharge_percent"].ToString() != ""))
							)
						{
							if((tmpCustZone.Tables[0].Rows[0]["esa_surcharge_amt"] != null) && 
								(!tmpCustZone.Tables[0].Rows[0]["esa_surcharge_amt"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
								(tmpCustZone.Tables[0].Rows[0]["esa_surcharge_amt"].ToString() != ""))
							{
								surchargeOfRecip = (decimal)tmpCustZone.Tables[0].Rows[0]["esa_surcharge_amt"];
							}

							if((tmpCustZone.Tables[0].Rows[0]["esa_surcharge_percent"] != null) && 
								(!tmpCustZone.Tables[0].Rows[0]["esa_surcharge_percent"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
								(tmpCustZone.Tables[0].Rows[0]["esa_surcharge_percent"].ToString() != ""))
							{
								decimal tmpFreight = 0;

								if (fCalcFrgtChrg.Trim() != "")
									tmpFreight = Convert.ToDecimal(fCalcFrgtChrg.Trim());

								surchargeOfRecip = (tmpFreight * ((decimal)tmpCustZone.Tables[0].Rows[0]["esa_surcharge_percent"])) / 100;
							}
						}	
						else
						{	
							//							Zipcode zipcode = new Zipcode();
							//							zipcode.Populate(appID,enterpriseID,RecipZipcode.Trim());
							surchargeOfRecip = 0;//zipcode.EASSurcharge;
						}

						//Apply MIN MAX
						if((tmpCustZone.Tables[0].Rows[0]["esa_surcharge_percent"] != null) && 
							(!tmpCustZone.Tables[0].Rows[0]["esa_surcharge_percent"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
							(tmpCustZone.Tables[0].Rows[0]["esa_surcharge_percent"].ToString() != ""))
						{
							if((decimal)tmpCustZone.Tables[0].Rows[0]["esa_surcharge_percent"] != 0)
							{

								if((tmpCustZone.Tables[0].Rows[0]["min_esa_amount"] != null) && 
									(!tmpCustZone.Tables[0].Rows[0]["min_esa_amount"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
									(tmpCustZone.Tables[0].Rows[0]["min_esa_amount"].ToString() != ""))
									if(surchargeOfRecip<(decimal)tmpCustZone.Tables[0].Rows[0]["min_esa_amount"])					
										surchargeOfRecip=(decimal)tmpCustZone.Tables[0].Rows[0]["min_esa_amount"];
							
								if((tmpCustZone.Tables[0].Rows[0]["max_esa_amount"] != null) && 
									(!tmpCustZone.Tables[0].Rows[0]["max_esa_amount"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
									(tmpCustZone.Tables[0].Rows[0]["max_esa_amount"].ToString() != ""))
									if(surchargeOfRecip>(decimal)tmpCustZone.Tables[0].Rows[0]["max_esa_amount"])					
										surchargeOfRecip=(decimal)tmpCustZone.Tables[0].Rows[0]["max_esa_amount"];
							}
						}
						//END Apply MIN MAX

					}
					else// no data from DB
					{	
						//						Zipcode zipcode = new Zipcode();
						//						zipcode.Populate(appID,enterpriseID,RecipZipcode.Trim());
						surchargeOfRecip = 0;//zipcode.EASSurcharge;
					}
				}
				else //NO ESA apply
				{
					surchargeOfRecip = 0;
				}
				//***************** Summary Surcharge *************************

				decSrchrg = surchargeOfSender + surchargeOfRecip;
				//txtESASurchrg.Text = String.Format((String)ViewState["m_format"], decSrchrg);
			}
			return decSrchrg;

		}
		//END OF NEW ESA CALCULATION by X OCT 25 08 Revise By Hong 11/04/2011

		// Get Total VAS Surcharge By Hong 11/04/2011
		public static decimal GetTotalSurcharge(DataSet m_dsVAS)
		{
			decimal decTotSurchrg = 0;
			if(m_dsVAS != null)
			{
				int cnt = m_dsVAS.Tables[0].Rows.Count;
				int i = 0;
				
				for(i = 0;i<cnt;i++)
				{
					DataRow drEach = m_dsVAS.Tables[0].Rows[i];

					String vas_code = "";
					if((drEach["vas_code"] != null) && 
						(!drEach["vas_code"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
						(drEach["vas_code"].ToString() != ""))
					{
						vas_code = drEach["vas_code"].ToString().Trim();
					}

					if(drEach["surcharge"]!=System.DBNull.Value)
						decTotSurchrg += (decimal)drEach["surcharge"];
				}
			}
			return decTotSurchrg;
		}
		
		
		public static int DomesticShipment_SavingPkgDetail(string strAppID,string strEnterpriseID,
			int BookingNo, string consignmentNo)
		{
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			System.Collections.ArrayList storedParams = new System.Collections.ArrayList();
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@enterpriseid",strEnterpriseID));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@booking_no",BookingNo));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@consignment_no",consignmentNo));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@applicationid",strAppID));

			int result = (int)dbCon.ExecuteProcedure("DomesticShipment",storedParams);
			
			return result;
		}

		public static DataSet CalcPackageDetailsWeights(string appid, string enterpriseid,string payerid, 
			string pkg_length, string pkg_breadth,
			string pkg_height, string pkg_wt,
			string pkg_qty)
		{
			DbConnection dbCon= null;
			IDbCommand dbcmd=null;
			DataSet dsCalcPackageDetails =null;


			dbCon = DbConnectionManager.GetInstance().GetDbConnection(appid,enterpriseid);

			if (dbCon==null)
			{
				Logger.LogTraceError("DomesticShipmentMgrDAL.cs","CalcPackageDetailsWeights","ERR001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			string strQry =  ""; 
			strQry = "SELECT tot_wt, tot_dim_wt, chargeable_wt, tot_act_wt, error_code ";
			strQry += "FROM dbo.CalcPackageDetailsWeights(";
			strQry += "N'" + enterpriseid + "', N'" + payerid + "',";
			strQry += pkg_length + "," + pkg_breadth + ","+ pkg_height + ",";
			strQry += pkg_wt + "," + pkg_qty;
			strQry += ")";

			dbcmd = dbCon.CreateCommand(strQry,CommandType.Text);
 
			try
			{
				dsCalcPackageDetails = (DataSet)dbCon.ExecuteQuery(dbcmd,com.common.DAL.ReturnType.DataSetType);  
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("DomesticShipmentMgrDAL.cs","CalcPackageDetailsWeights","ERR002","Error in Query String "+appException.Message);
				throw appException;
			}
			return dsCalcPackageDetails;
		}


		public static DataSet CalcPackageDetailsSummary(string appid, string enterpriseid,int ControlFlag,string Packages)
		{
			DbConnection dbCon= null;
			IDbCommand dbcmd=null;
			DataSet dsCalcPackageDetails =null;


			dbCon = DbConnectionManager.GetInstance().GetDbConnection(appid,enterpriseid);

			if (dbCon==null)
			{
				Logger.LogTraceError("DomesticShipmentMgrDAL.cs","CalcPackageDetailsSummary","ERR001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			string strQry =  ""; 
			strQry = "SELECT tot_pkg,tot_wt,tot_dim_wt,chargeable_wt ,tot_act_wt,volume,error_code ";
			strQry += "FROM dbo.CalcPackageDetailsSummary(";
			strQry += "N'" + enterpriseid + "',"+ControlFlag.ToString()+", N'" + Packages + "'";
			strQry += ")";

			dbcmd = dbCon.CreateCommand(strQry,CommandType.Text);
 
			try
			{
				dsCalcPackageDetails = (DataSet)dbCon.ExecuteQuery(dbcmd,com.common.DAL.ReturnType.DataSetType);  
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("DomesticShipmentMgrDAL.cs","CalcPackageDetailsSummary","ERR002","Error in Query String "+appException.Message);
				throw appException;
			}
			return dsCalcPackageDetails;
		}


		public static string DataSetVasToString(string Booking_no, string Consignment_no, DataSet dsVas)
		{
			string vas="";
			foreach(DataRow dr in dsVas.Tables[0].Rows)
			{
				string line="";				
				if(Booking_no != null && Booking_no.Trim() != "")
				{
					line+=Booking_no.ToString().Trim();
				}

				if(Consignment_no != null)
				{
					line+=","+Consignment_no.ToString();
				}
				else
				{
					line+=",";
				}

				if(dr["vas_code"] != DBNull.Value)
				{
					line+="," +dr["vas_code"].ToString();
				}
				else
				{
					line+=",";
				}
				if(dr["surcharge"] != DBNull.Value)
				{
					line+="," +dr["surcharge"].ToString();
				}
				else
				{
					line+=",";
				}
				if(dr["remarks"] != DBNull.Value)
				{
					line+="," +dr["remarks"].ToString();
				}
				else
				{
					line+=",";
				}

				if(vas.Length>0)
				{
					vas=vas+";";
				}
				vas=vas+line;
			}
			return vas;
		}
	}
}
