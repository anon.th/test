using System;
using System.Data;
using System.Collections;
using System.Text;
using com.common.DAL;
using com.common.util;
using com.common.classes;
using com.ties.classes;

namespace com.ties.DAL
{
	/// <summary>
	/// Summary description for ReportDAL.
	/// </summary>
	public class ReportDAL
	{
		public ReportDAL()
		{
			//
			// TODO: Add constructor logic here
			//
		}


		public static DataSet GetSopScaning(String strAppID, String strEnterpriseID, DataSet dsQuery)
		{	
			DbConnection dbCon= null;
			IDbCommand dbcmd=null;
			DataSet dsSop = null;

			DataProviderType dbProvider = DbConnectionManager.GetInstance().GetDbProviderType(strAppID,strEnterpriseID);
			dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if (dbCon==null)
			{
				Logger.LogTraceError("ReportDAL.cs","GetSopScaning","ERR001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			//@consignment_no 		varchar(30) = null,
			//	@user_id				varchar(30) = null,
			//	@service_type			varchar(5) = null,
			//	@delivery_route_code	varchar(12) = null,
			//	@destination_zipcode	varchar(5) = null,
			//	@scanning_datetime		varchar(100) = null

			DataRow dr = dsQuery.Tables[0].Rows[0];

			ArrayList sopscan_params = new ArrayList();
			//*************************** Consignment_no : Part ***************************

			string consignment_no = "";
			string user_id = "";
			string service_type = "";
			string delivery_route_code = "";
			string destination_zipcode = "";
			string scanning_datetime = "";
			string line_haul = "";
			if (Utility.IsNotDBNull(dr["consignment_no"]) )
				consignment_no = dr["consignment_no"].ToString();
			if (Utility.IsNotDBNull(dr["user_id"]) )
				user_id = dr["user_id"].ToString();
			if (Utility.IsNotDBNull(dr["service_type"]) )
				service_type = dr["service_type"].ToString();
			if (Utility.IsNotDBNull(dr["delivery_route_code"]) )
				delivery_route_code = dr["delivery_route_code"].ToString();
			if (Utility.IsNotDBNull(dr["destination_zipcode"]) )
				destination_zipcode = dr["destination_zipcode"].ToString();
			if (Utility.IsNotDBNull(dr["scanning_datetime"]) )
				scanning_datetime = dr["scanning_datetime"].ToString();
			if (Utility.IsNotDBNull(dr["line_haul"]) )
				line_haul = dr["line_haul"].ToString();

			sopscan_params.Add(dbCon.MakeParam("@consignment_no", System.Data.DbType.String , 30, ParameterDirection.Input, consignment_no));
			sopscan_params.Add(dbCon.MakeParam("@user_id", System.Data.DbType.String, 30, ParameterDirection.Input, user_id));
			sopscan_params.Add(dbCon.MakeParam("@service_type", System.Data.DbType.String, 10, ParameterDirection.Input, service_type));
			sopscan_params.Add(dbCon.MakeParam("@delivery_route_code", System.Data.DbType.String, 12, ParameterDirection.Input, delivery_route_code));
			sopscan_params.Add(dbCon.MakeParam("@destination_zipcode", System.Data.DbType.String, 5, ParameterDirection.Input, destination_zipcode));
			sopscan_params.Add(dbCon.MakeParam("@scanning_datetime", System.Data.DbType.String, 100, ParameterDirection.Input, scanning_datetime));
			sopscan_params.Add(dbCon.MakeParam("@line_haul", System.Data.DbType.String, 12, ParameterDirection.Input, line_haul));

			string strStoredName = "";
			if (Utility.IsNotDBNull(dr["missconsignment"]) )
			{
				strStoredName = "sp_SOP_Shipment_Report_SOPM";
			}
			else
			{
				strStoredName = "sp_SOP_Shipment_Report_SOP";
			}

			dbcmd = dbCon.CreateCommand(strStoredName, sopscan_params, CommandType.StoredProcedure);
			
			try
			{
				dsSop = (DataSet)dbCon.ExecuteQuery(dbcmd, com.common.DAL.ReturnType.DataSetType);  
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("ReportDAL.cs","GetSopScaning","ERR002","Error in Query String "+appException.Message);
				throw appException;
			}
			return dsSop;
		}


		public static DataSet GetTopNPayer(String strAppID, String strEnterpriseID, DataSet dsQuery)
		{	
			DbConnection dbCon= null;
			IDbCommand dbcmd=null;
			DataSet dsTopN = null;
			StringBuilder strBuilder = null;
			DataProviderType dbProvider = DbConnectionManager.GetInstance().GetDbProviderType(strAppID,strEnterpriseID);
			dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if (dbCon==null)
			{
				Logger.LogTraceError("ReportDAL.cs","GetTopNPayer","ERR001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			String strSqlQuery = null;
			String strSqlWhere = null;
			String strSqlSort = null;

			DataRow dr = dsQuery.Tables[0].Rows[0];
			strBuilder = new StringBuilder();

			//*************************** Booking Date : Part ***************************
			if (Utility.IsNotDBNull(dr["start_date"]) && Utility.IsNotDBNull(dr["end_date"]))
			{
				DateTime dateStartDate = (DateTime) dr["start_date"];
				DateTime dateEndDate = (DateTime) dr["end_date"];
				String strStartDate = Utility.DateFormat(strAppID, strEnterpriseID, dateStartDate, DTFormat.DateTime);
				String strEndDate = Utility.DateFormat(strAppID, strEnterpriseID, dateEndDate, DTFormat.DateTime);

				strBuilder.Append(" and s.booking_datetime >= ");
				strBuilder.Append(strStartDate);
				strBuilder.Append(" and s.booking_datetime <= ");
				strBuilder.Append(strEndDate);
				strBuilder.Append(" ");
			}

			//*************************** Payer Type : Part ***************************
			String strCustPayerType = dr["payer_type"].ToString() + "";
			if (strCustPayerType != "")
			{
				strBuilder.Append(" and c.payer_type in (");
				strBuilder.Append(strCustPayerType);
				strBuilder.Append(") ");
			}

			//*************************** Route / DC Selection : Part ***************************
			String strorigin_dc = dr["origin_dc"].ToString() + "";
			if (strorigin_dc != "")
			{
				strBuilder.Append(" and s.origin_station = '");
				strBuilder.Append(strorigin_dc);
				strBuilder.Append("' ");
			}
			String strdestination_dc = dr["destination_dc"].ToString() + "";
			if (strdestination_dc != "")
			{
				strBuilder.Append(" and s.destination_station = '");
				strBuilder.Append(strdestination_dc);
				strBuilder.Append("' ");
			}
			String strroute_code = dr["route_code"].ToString() + "";
			if (strroute_code != "")
			{
				String strroute_type = dr["route_type"].ToString() + "";
				if (strroute_type == "L" || strroute_type == "A")
				{
					String strdelPath_origin_dc = dr["delPath_origin_dc"].ToString() + "";
					strBuilder.Append(" and s.origin_station = '");
					strBuilder.Append(strdelPath_origin_dc);
					strBuilder.Append("' ");

					String strdelPath_destination_dc = dr["delPath_destination_dc"].ToString() + "";
					strBuilder.Append(" and s.destination_station = '");
					strBuilder.Append(strdelPath_destination_dc);
					strBuilder.Append("' ");
				}
				else
				{
					strBuilder.Append(" and s.route_code = '");
					strBuilder.Append(strroute_code);
					strBuilder.Append("' ");
				}
			}

			//*************************** Destination ***************************
			String strzip_code = dr["zip_code"].ToString() + "";
			if (strzip_code != "")
			{
				strBuilder.Append(" and s.recipient_zipcode = '");
				strBuilder.Append(strzip_code);
				strBuilder.Append("' ");
			}
			String strstate_code = dr["state_code"].ToString() + "";
			if (strstate_code != "")
			{
				strBuilder.Append(" and s.destination_state_code = '");
				strBuilder.Append(strstate_code);
				strBuilder.Append("' ");
			}

			//*************************** Retrieval Basis on : Part ***************************
			String strBasis = dr["basis"].ToString();
			if (strBasis == "I")
				strBuilder.Append(" and invoice_no is not null ");
			else if (strBasis == "D")
				strBuilder.Append(" and (delivery_manifested = 'Y' or delivery_manifested = 'y' ) ");

			strSqlWhere = strBuilder.ToString();

			int intTop = 0;
			intTop = (int)dr["top_n"];

			strSqlSort += " ORDER BY ";
			String strSort = dr["sort_by"].ToString();
			if (strSort == "R")
				strSqlSort += "revenue desc ";
			else if (strSort == "W")
				strSqlSort += "chargeable_wt desc ";
			else if (strSort == "V")
				strSqlSort += "tot_volume desc ";
			else
				strSqlSort += "no_of_consignment desc ";

			strBuilder = new StringBuilder();

			switch (dbProvider)
			{
				case DataProviderType.Sql :
					strBuilder.Append("select top ");
					strBuilder.Append(intTop);
					strBuilder.Append(" e.enterprise_name, e.currency, s.payerid, s.payer_name,s.payer_type, count(*) as no_of_consignment, ");
					strBuilder.Append("sum(s.chargeable_wt) as chargeable_wt, sum(s.tot_dim_wt*6000) as tot_volume, sum(case s.mbg ");
					strBuilder.Append("when 'Y' then 0 else (s.tot_freight_charge + s.tot_vas_surcharge + s.esa_surcharge + ");
					strBuilder.Append("s.insurance_surcharge + st.surcharge) end) as revenue from shipment s, ");
					strBuilder.Append("(select applicationid, enterpriseid, consignment_no, sum(surcharge) surcharge from shipment_tracking  ");
					strBuilder.Append("where applicationid = '");
					strBuilder.Append(strAppID);
					strBuilder.Append("' and enterpriseid = '");
					strBuilder.Append(strEnterpriseID);
					strBuilder.Append("' group by applicationid, enterpriseid, consignment_no) as st, enterprise e, Customer c ");
					strBuilder.Append("where st.applicationid = s.applicationid and st.enterpriseid = s.enterpriseid and ");
					strBuilder.Append("st.consignment_no = s.consignment_no and e.applicationid = s.applicationid and e.enterpriseid = s.enterpriseid and ");
					strBuilder.Append("s.applicationid = c.applicationid and s.enterpriseid = c.enterpriseid and s.payerid = c.custid and ");
					strBuilder.Append("s.applicationid = '");
					strBuilder.Append(strAppID);
					strBuilder.Append("' and s.enterpriseid = '");
					strBuilder.Append(strEnterpriseID);
					strBuilder.Append("' ");
					strBuilder.Append(strSqlWhere);
					strBuilder.Append(" group by s.applicationid, s.enterpriseid, e.enterprise_name, e.currency, s.payerid, s.payer_name ,s.payer_type ");
					strBuilder.Append(strSqlSort);						
					break;
				
				case DataProviderType.Oracle:
					strBuilder.Append("select ");
					strBuilder.Append(" e.enterprise_name, e.currency, s.payerid, s.payer_name,s.payer_type, count(*) as no_of_consignment, ");
					strBuilder.Append("sum(s.chargeable_wt) as chargeable_wt, sum(s.tot_dim_wt*6000) as tot_volume,");
					strBuilder.Append("Decode(s.mbg,'Y',0,'N',s.tot_freight_charge + s.tot_vas_surcharge + s.esa_surcharge + s.insurance_surcharge + st.surcharge)");
					strBuilder.Append("as revenue from shipment s,(select applicationid, enterpriseid, consignment_no, sum(surcharge) surcharge from shipment_tracking  ");
					strBuilder.Append("where applicationid = '");
					strBuilder.Append(strAppID);
					strBuilder.Append("' and enterpriseid = '");
					strBuilder.Append(strEnterpriseID);
					strBuilder.Append("'  group by applicationid, enterpriseid, consignment_no) st, enterprise e ");
					strBuilder.Append("where st.applicationid = s.applicationid and st.enterpriseid = s.enterpriseid and ");
					strBuilder.Append("st.consignment_no = s.consignment_no and e.applicationid = s.applicationid and e.enterpriseid = s.enterpriseid and ");
					strBuilder.Append("s.applicationid = '");
					strBuilder.Append(strAppID);
					strBuilder.Append("' and s.enterpriseid = '");
					strBuilder.Append(strEnterpriseID);
					strBuilder.Append("' ");
					strBuilder.Append(strSqlWhere);
					strBuilder.Append(" and rownum < 3");
					strBuilder.Append("  group by s.applicationid, s.enterpriseid, e.enterprise_name, e.currency, s.payerid, s.payer_name ,s.payer_type, ");
					strBuilder.Append("s.mbg,s.tot_freight_charge , s.tot_vas_surcharge , s.esa_surcharge , s.insurance_surcharge , st.surcharge");
					strBuilder.Append(strSqlSort);						

					break;
				
				case DataProviderType.Oledb:
					break;
			}			
		
			strSqlQuery = strBuilder.ToString();

			dbcmd = dbCon.CreateCommand(strSqlQuery,CommandType.Text);
 
			try
			{
				dsTopN = (DataSet)dbCon.ExecuteQuery(dbcmd,com.common.DAL.ReturnType.DataSetType);  
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("ReportDAL.cs","GetTopNPayer","ERR002","Error in Query String "+appException.Message);
				throw appException;
			}
			return dsTopN;
		}

		public static String GetSQICommandStr(String strAppID, String strEnterpriseID, DataSet dsQuery)
		{	
			#region member
			String strstart_date=null;
			String strend_date=null;
			String strtran_date=null;
			String strpayer_type=null;
			String strpayer_code = null;
			String strorigin_dc = null;
			String strdestination_dc = null;
			String strroute_code = null;
			String strroute_type = null;
			String strdelPath_origin_dc = null;
			String strdelPath_destination_dc = null;
			String strzipcode = null;
			String strstate_code = null;
			String strservice_type = null;
			#endregion
			
			String strSqlQuery = null;
			StringBuilder strBuilder = null;
			DataRow dr = dsQuery.Tables[0].Rows[0];
			strBuilder = new StringBuilder();

			//*************************** Booking Date : Part ***************************
			if (Utility.IsNotDBNull(dr["tran_date"]))
			{
				switch(dr["tran_date"].ToString().ToUpper())
				{
					case "E":
						strtran_date = " est_delivery_datetime";
						break;
					case "S":
						strtran_date = " act_pickup_datetime";
						break;					
					case "B":
						strtran_date = " booking_datetime";
						break;
				}
			}
			if (Utility.IsNotDBNull(dr["start_date"]) && Utility.IsNotDBNull(dr["end_date"]))
			{
				DateTime dateStartDate = (DateTime) dr["start_date"];
				DateTime dateEndDate = (DateTime) dr["end_date"];
				strstart_date = Utility.DateFormat(strAppID, strEnterpriseID, dateStartDate, DTFormat.DateTime);
				strend_date	= Utility.DateFormat(strAppID, strEnterpriseID, dateEndDate, DTFormat.DateTime);
			}			
			//*************************** Payer Type : Part ***************************
			String strpayer_type_tmp;
			strpayer_type_tmp = dr["payer_type"].ToString() + "";
			if (strpayer_type_tmp != "")
			{
				strpayer_type = " and payerid in ( select custid from customer where ";
				for (int i=0; i <= strpayer_type_tmp.Length - 1; i ++)
				{
					if (i==0)
					{
						strpayer_type += "payer_type = '" +strpayer_type_tmp.Substring(i,1).ToUpper()+ "'";
					}
					else 
					{
						strpayer_type += "or payer_type = '" +strpayer_type_tmp.Substring(i,1).ToUpper()+ "'";
					}
				}
				strpayer_type += " ) ";
			}

			strpayer_code = dr["payer_code"].ToString() + "";
			if (strpayer_code != "")
			{
				strpayer_code = "and payerid = '" +strpayer_code+ "'";
			}
			//*************************** Route / DC Selection : Part ***************************
			strroute_type = dr["route_type"].ToString() + "";
			strroute_code = dr["route_code"].ToString() + "";
			if (strroute_code != "")
			{
				
				if (strroute_type == "L" || strroute_type == "A")
				{
					strdelPath_origin_dc = "and origin_station = '" + dr["delPath_origin_dc"].ToString() + "'" ;
					strdelPath_destination_dc = "and destination_station = '" + dr["delPath_destination_dc"].ToString() + "'" ; 					
					strroute_code = "";
				}
				else
				{
					strroute_code = " and route_code = '" + strroute_code + "'";
				}
			}
			
			strorigin_dc = dr["origin_dc"].ToString() + "";
			if (strorigin_dc != "")
			{
				strorigin_dc = "and origin_station = '" +strorigin_dc+ "'";
			}

			strdestination_dc = dr["destination_dc"].ToString() + "";	
			if(strdestination_dc != "")
			{
				strdestination_dc = "and destination_station = '" +strdestination_dc+ "'";
			}
            
			//*************************** Destination ***************************
			strzipcode = dr["zip_code"].ToString() + "";
			if(strzipcode != "")
			{
				strzipcode = "and recipient_zipcode = '" +strzipcode+ "'";
			}
			strstate_code = dr["state_code"].ToString() + "";
			if(strstate_code != "")
			{
				strstate_code = "and destination_state_code = '" +strstate_code+ "'";
			}

			//*************************** Service Type ***************************
			strservice_type = dr["service_type"].ToString() + "";
			if(strservice_type != "")
			{
				strservice_type = "and service_code = '" +strservice_type+ "'";
			}
			string strMasterAcct = dr["master_account"].ToString();  //Jeab 18 Jul 2011

			/*
			//*********************************** Building Command String *****************************
			strBuilder.Append("SELECT SHP.consignment_no, ");
			strBuilder.Append("case isnull(SHP_DEL.consignment_no, '') ");
			strBuilder.Append("when '' then 'NOT DEL' ");
			strBuilder.Append("else 'DEL' end as DEL_STATUS, ");
			strBuilder.Append("case ");
			strBuilder.Append("when SHP_DEL.tracking_datetime is null then 'NOT DEL' ");
			strBuilder.Append("else (case ");
			strBuilder.Append("when SHP_DEL.tracking_datetime > SHP.est_delivery_datetime then ");
			strBuilder.Append("(case ");
			strBuilder.Append("when SHP_AUDIT.consignment_no is not null then 'LATE AUDIT' ");
			strBuilder.Append("else 'LATE NOT AUDIT' end) ");
			strBuilder.Append("else 'NOT LATE' end) ");
			strBuilder.Append("end as AUDIT_STATUS, ");
			strBuilder.Append(" ISNULL(SHP_AUDIT.exception_code, 'NOT EXCEPT') as exception_code, PODEX_CODE.exception_type, PODEX_CODE.exception_description ");
			strBuilder.Append("from ");
			//Jeab 18 Jul 2011
			if (strMasterAcct != "")
			{
				strBuilder.Append("(SELECT s.applicationid, s.enterpriseid, s.consignment_no, s.booking_no, s.est_delivery_datetime ");
				strBuilder.Append("FROM Shipment   s  Left Outer Join Customer c ON   ");
				strBuilder.Append(" s.applicationid = c.applicationid And  s.enterpriseid = c.enterpriseid And s.payerid = c.custid  ");
				strBuilder.Append("WHERE s.applicationid = '");
				strBuilder.Append(strAppID);
				strBuilder.Append("'");
				strBuilder.Append("AND s.enterpriseid = '");
				strBuilder.Append(strEnterpriseID);  
				strBuilder.Append("'");
				strBuilder.Append("AND c.master_account = '");
				strBuilder.Append(strMasterAcct);  
				strBuilder.Append("'");
				strBuilder.Append("AND s." + strtran_date + " between " + strstart_date + " and " + strend_date );			
			}
			else
			{
				strBuilder.Append("(SELECT applicationid, enterpriseid, consignment_no, booking_no, est_delivery_datetime ");
				strBuilder.Append("FROM Shipment   ");				
				strBuilder.Append("WHERE applicationid = '");
				strBuilder.Append(strAppID);
				strBuilder.Append("'");
				strBuilder.Append("AND enterpriseid = '");
				strBuilder.Append(strEnterpriseID);  
				strBuilder.Append("'");				
				strBuilder.Append("AND " + strtran_date + " between " + strstart_date + " and " + strend_date );
			}

			//Critiria
			strBuilder.Append(strdelPath_origin_dc +" ");
			strBuilder.Append(strdelPath_destination_dc +" ");
			strBuilder.Append(strroute_code +" ");
			strBuilder.Append(strorigin_dc +" ");
			strBuilder.Append(strdestination_dc +" ");
			strBuilder.Append(strzipcode +" ");
			strBuilder.Append(strstate_code +" ");
			strBuilder.Append(strpayer_code +" ");
			strBuilder.Append(strpayer_type +" ");
			strBuilder.Append(strservice_type +" ");


			strBuilder.Append(")SHP ");

			//Jeab 18 Jul 2011  =========> End

			strBuilder.Append("LEFT OUTER JOIN ");
			strBuilder.Append("(SELECT 	SUB_SHP_DEL2.applicationid, SUB_SHP_DEL2.enterpriseid, SUB_SHP_DEL2.consignment_no, SUB_SHP_DEL2.booking_no, SUB_SHP_DEL2.tracking_datetime ");
			strBuilder.Append("FROM ");
			//Jeab 18 Jul 2011 
			if (strMasterAcct !="")
			{
				strBuilder.Append("(select s.applicationid, s.enterpriseid, s.consignment_no, s.booking_no  ");
				strBuilder.Append("FROM Shipment  s  Left  Outer  Join  Customer c ON ");
				strBuilder.Append("s.applicationid = c.applicationid And  s.enterpriseid = c.enterpriseid And s.payerid = c.custid  ");
				strBuilder.Append("WHERE s.applicationid = '");
				strBuilder.Append(strAppID);
				strBuilder.Append("'");
				strBuilder.Append("AND s.enterpriseid = '");
				strBuilder.Append(strEnterpriseID);
				strBuilder.Append("'");
				strBuilder.Append("AND c.master_account = '");
				strBuilder.Append(strMasterAcct);  
				strBuilder.Append("'");
				strBuilder.Append("AND s." + strtran_date + " between " + strstart_date + " and " + strend_date );
			}
			else
			{
				strBuilder.Append("(select applicationid, enterpriseid, consignment_no, booking_no  ");
				strBuilder.Append("FROM Shipment  ");
				strBuilder.Append("WHERE applicationid = '");
				strBuilder.Append(strAppID);
				strBuilder.Append("'");
				strBuilder.Append("AND enterpriseid = '");
				strBuilder.Append(strEnterpriseID);
				strBuilder.Append("'");
				strBuilder.Append("AND " + strtran_date + " between " + strstart_date + " and " + strend_date );
			}
			//Critiria
			strBuilder.Append(strdelPath_origin_dc +" ");
			strBuilder.Append(strdelPath_destination_dc +" ");
			strBuilder.Append(strroute_code +" ");
			strBuilder.Append(strorigin_dc +" ");
			strBuilder.Append(strdestination_dc +" ");
			strBuilder.Append(strzipcode +" ");
			strBuilder.Append(strstate_code +" ");
			strBuilder.Append(strpayer_code +" ");
			strBuilder.Append(strpayer_type +" ");
			strBuilder.Append(strservice_type +" ");
			strBuilder.Append(")SUB_SHP_DEL1 ");

			//Jeab 18 Jul 2011  =========> End

			strBuilder.Append("	INNER JOIN");
			strBuilder.Append("	(SELECT applicationid, enterpriseid, consignment_no, booking_no, MAX(tracking_datetime) AS tracking_datetime ");
			strBuilder.Append("	FROM shipment_tracking ");
			strBuilder.Append("WHERE applicationid = '");
			strBuilder.Append(strAppID);
			strBuilder.Append("'");
			strBuilder.Append("AND enterpriseid = '");
			strBuilder.Append(strEnterpriseID);
			strBuilder.Append("'");			
			strBuilder.Append("	AND (status_code = 'POD' AND isnull(deleted, 'N') <> 'Y') ");	
			strBuilder.Append("	GROUP BY applicationid, enterpriseid, consignment_no, booking_no");
			strBuilder.Append("	)SUB_SHP_DEL2 ");
			strBuilder.Append("	ON 	(SUB_SHP_DEL1.applicationid = SUB_SHP_DEL2.applicationid ");
			strBuilder.Append("	AND SUB_SHP_DEL1.enterpriseid = SUB_SHP_DEL2.enterpriseid ");
			strBuilder.Append("	AND 	SUB_SHP_DEL1.consignment_no = SUB_SHP_DEL2.consignment_no ");
			strBuilder.Append("	AND 	SUB_SHP_DEL1.booking_no = SUB_SHP_DEL2.booking_no)");
			strBuilder.Append(" ) SHP_DEL ");			
			strBuilder.Append("ON 	(SHP.applicationid = SHP_DEL.applicationid ");
			strBuilder.Append("AND 	SHP.enterpriseid = SHP_DEL.enterpriseid "); 
			strBuilder.Append("AND 	SHP.consignment_no = SHP_DEL.consignment_no "); 
			strBuilder.Append("AND 	SHP.booking_no = SHP_DEL.booking_no) "); 
			strBuilder.Append("LEFT OUTER JOIN ");
			strBuilder.Append("(SELECT  SUB_SHP_AUDIT2.*");
			strBuilder.Append(" FROM ");
			strBuilder.Append("(SELECT 	SUB_SHP_AUDIT1_2.applicationid, SUB_SHP_AUDIT1_2.enterpriseid, SUB_SHP_AUDIT1_2.consignment_no, SUB_SHP_AUDIT1_2.booking_no, SUB_SHP_AUDIT1_2.tracking_datetime");
			strBuilder.Append(" FROM ");
			//Jeab 18 Jul 2011  
			if (strMasterAcct != "")
			{
				strBuilder.Append("(SELECT s.applicationid, s.enterpriseid, s.consignment_no, s.booking_no ");
				strBuilder.Append(" FROM Shipment  s  Left  Outer  Join  Customer  c  ON ");
				strBuilder.Append(" s.applicationid = c.applicationid  And s.enterpriseid  =  c.enterpriseid  And  s.payerid = c.custid ");
				strBuilder.Append("WHERE s.applicationid = '");
				strBuilder.Append(strAppID);
				strBuilder.Append("'");
				strBuilder.Append("AND s.enterpriseid = '");
				strBuilder.Append(strEnterpriseID);
				strBuilder.Append("'");
				strBuilder.Append("AND  c.master_account = '");
				strBuilder.Append(strMasterAcct);
				strBuilder.Append("'");
				strBuilder.Append("AND s." + strtran_date + " between " + strstart_date + " and " + strend_date );
			}
			else
			{
				strBuilder.Append("(SELECT applicationid, enterpriseid, consignment_no, booking_no ");
				strBuilder.Append(" FROM Shipment  ");
				strBuilder.Append("WHERE applicationid = '");
				strBuilder.Append(strAppID);
				strBuilder.Append("'");
				strBuilder.Append("AND enterpriseid = '");
				strBuilder.Append(strEnterpriseID);
				strBuilder.Append("'");
				strBuilder.Append("AND " + strtran_date + " between " + strstart_date + " and " + strend_date );				
			}

			//Critiria
			strBuilder.Append(strdelPath_origin_dc +" ");
			strBuilder.Append(strdelPath_destination_dc +" ");
			strBuilder.Append(strroute_code +" ");
			strBuilder.Append(strorigin_dc +" ");
			strBuilder.Append(strdestination_dc +" ");
			strBuilder.Append(strzipcode +" ");
			strBuilder.Append(strstate_code +" ");
			strBuilder.Append(strpayer_code +" ");
			strBuilder.Append(strpayer_type +" ");
			strBuilder.Append(strservice_type +" ");
			strBuilder.Append(") SUB_SHP_AUDIT1_1");
			//Jeab 18 Jul 2011  =========> End

			strBuilder.Append(" INNER JOIN ");
			strBuilder.Append("(SELECT applicationid, enterpriseid, consignment_no, booking_no, MAX(tracking_datetime) AS tracking_datetime ");
			strBuilder.Append(" FROM shipment_tracking ");
			strBuilder.Append("WHERE applicationid = '");
			strBuilder.Append(strAppID);
			strBuilder.Append("'");
			strBuilder.Append("AND enterpriseid = '");
			strBuilder.Append(strEnterpriseID);
			strBuilder.Append("'");			
			strBuilder.Append("	AND (status_code = 'PODEX' AND isnull(deleted, 'N') <> 'Y')");	
			strBuilder.Append("	GROUP BY applicationid, enterpriseid, consignment_no, booking_no");
			strBuilder.Append("	)SUB_SHP_AUDIT1_2 ");
			strBuilder.Append("ON 	(SUB_SHP_AUDIT1_1.applicationid = SUB_SHP_AUDIT1_2.applicationid ");
			strBuilder.Append("AND 	SUB_SHP_AUDIT1_1.enterpriseid = SUB_SHP_AUDIT1_2.enterpriseid "); 
			strBuilder.Append("AND 	SUB_SHP_AUDIT1_1.consignment_no = SUB_SHP_AUDIT1_2.consignment_no "); 
			strBuilder.Append("AND 	SUB_SHP_AUDIT1_1.booking_no = SUB_SHP_AUDIT1_2.booking_no) ");
			strBuilder.Append(") SUB_SHP_AUDIT1 ");	
			strBuilder.Append("LEFT OUTER JOIN ");
			strBuilder.Append("(SELECT * ");
			strBuilder.Append("FROM	shipment_tracking ");
			strBuilder.Append("WHERE applicationid = '");
			strBuilder.Append(strAppID);
			strBuilder.Append("'");
			strBuilder.Append("AND enterpriseid = '");
			strBuilder.Append(strEnterpriseID);
			strBuilder.Append("'");						
			strBuilder.Append(")SUB_SHP_AUDIT2 ");
			strBuilder.Append("ON (SUB_SHP_AUDIT1.applicationid = SUB_SHP_AUDIT2.applicationid ");
			strBuilder.Append("AND SUB_SHP_AUDIT1.enterpriseid = SUB_SHP_AUDIT2.enterpriseid "); 
			strBuilder.Append("AND SUB_SHP_AUDIT1.consignment_no = SUB_SHP_AUDIT2.consignment_no "); 
			strBuilder.Append("AND SUB_SHP_AUDIT1.booking_no = SUB_SHP_AUDIT2.booking_no ");
			strBuilder.Append("AND	SUB_SHP_AUDIT1.tracking_datetime = SUB_SHP_AUDIT2.tracking_datetime) ");
			strBuilder.Append(") SHP_AUDIT ");
			strBuilder.Append("ON	(SHP.applicationid = SHP_AUDIT.applicationid "); 
			strBuilder.Append("AND 	SHP.enterpriseid = SHP_AUDIT.enterpriseid ");
			strBuilder.Append("AND 	SHP.consignment_no = SHP_AUDIT.consignment_no "); 
			strBuilder.Append("AND 	SHP.booking_no = SHP_AUDIT.booking_no) ");
			strBuilder.Append("LEFT OUTER JOIN ");
			strBuilder.Append("(SELECT * "); 
			strBuilder.Append("FROM 	EXCEPTION_CODE "); 
			strBuilder.Append("WHERE applicationid = '");
			strBuilder.Append(strAppID);
			strBuilder.Append("'");
			strBuilder.Append("AND enterpriseid = '");
			strBuilder.Append(strEnterpriseID);
			strBuilder.Append("'");					
			strBuilder.Append("and 	status_code = 'PODEX' ");
			strBuilder.Append(") PODEX_CODE  ");
			strBuilder.Append("ON 	(SHP_AUDIT.applicationid = PODEX_CODE.applicationid ");
			strBuilder.Append("AND 	SHP_AUDIT.enterpriseid = PODEX_CODE.enterpriseid "); 
			strBuilder.Append("AND 	SHP_AUDIT.exception_code = PODEX_CODE.exception_code) "); 
			*/
			
			strBuilder.Append(" SELECT COUNT(consignment_no) AS consignment_no ");
			strBuilder.Append(",DEL_STATUS, AUDIT_STATUS, exception_code, exception_type, exception_description ");
			strBuilder.Append("FROM ( ");
			strBuilder.Append("SELECT s.consignment_no ");
			strBuilder.Append(",CASE WHEN pod.consignment_no IS NULL THEN 'NOT DEL' ELSE 'DEL' END as DEL_STATUS ");
			strBuilder.Append(",CASE WHEN pod.tracking_datetime IS NULL THEN 'NOT DEL' ");
			strBuilder.Append("ELSE ( ");
			strBuilder.Append("CASE WHEN pod.tracking_datetime > s.est_delivery_datetime ");
			strBuilder.Append("THEN ( ");
			strBuilder.Append("CASE WHEN px.consignment_no IS NOT NULL THEN 'LATE AUDIT' ELSE 'LATE NOT AUDIT' END) ");
			strBuilder.Append("ELSE 'NOT LATE' END) END as AUDIT_STATUS ");
			strBuilder.Append(", ISNULL(px.exception_code, 'NOT EXCEPT') as exception_code ");
			strBuilder.Append(",ec.exception_type, ec.exception_description, s.payerid ");
			strBuilder.Append("FROM ( ");
			strBuilder.Append("SELECT applicationid, enterpriseid, consignment_no, booking_no, est_delivery_datetime,payerid ");
			strBuilder.Append("FROM dbo.Shipment WITH(NOLOCK) ");
			strBuilder.Append("WHERE applicationid = '"+strAppID+"' AND enterpriseid = '"+strEnterpriseID+"' ");
			//strBuilder.Append("AND act_delivery_date between " + strstart_date + " and " + strend_date +" ");
			
			strBuilder.Append("AND" + strtran_date + " between " + strstart_date + " and " + strend_date);

			// Criteria
			strBuilder.Append(strdelPath_origin_dc +" ");
			strBuilder.Append(strdelPath_destination_dc +" ");
			strBuilder.Append(strroute_code +" ");
			strBuilder.Append(strorigin_dc +" ");
			strBuilder.Append(strdestination_dc +" ");
			strBuilder.Append(strzipcode +" ");
			strBuilder.Append(strstate_code +" ");
			strBuilder.Append(strpayer_code +" ");
			strBuilder.Append(strpayer_type +" ");
			strBuilder.Append(strservice_type +" ");
			
			strBuilder.Append(") s ");
			
			if (strMasterAcct != "")
			{
				// When selecting by customer ID or master account ID
				strBuilder.Append(" INNER JOIN Customer c ");
				strBuilder.Append("ON s.applicationid = c.applicationid And  s.enterpriseid = c.enterpriseid And c.custid = s.payerid "); 
				// When user selected master account
				strBuilder.Append("AND c.master_account = '" + strMasterAcct +"' ");
			}
			/*
			else
			{
				// When user selected customer account
				strBuilder.Append("WHERE c.custid = '" + strCustAcct + "' ");
			}
			*/               

			strBuilder.Append("LEFT OUTER JOIN ( ");
			strBuilder.Append("SELECT applicationid, enterpriseid, consignment_no, booking_no, MAX(tracking_datetime) AS tracking_datetime ");
			strBuilder.Append("FROM dbo.shipment_tracking WITH(NOLOCK) ");
			strBuilder.Append("WHERE applicationid = '"+strAppID+"' AND enterpriseid = '"+strEnterpriseID+"' AND (status_code = 'POD' AND isnull(deleted, 'N') <> 'Y') ");
			strBuilder.Append(" GROUP BY applicationid, enterpriseid, consignment_no, booking_no) pod ");
			strBuilder.Append("ON s.applicationid = pod.applicationid and s.enterpriseid = pod.enterpriseid and ");
			strBuilder.Append("s.booking_no = pod.booking_no and s.consignment_no = pod.consignment_no ");
			
			strBuilder.Append("LEFT OUTER JOIN ( ");
			strBuilder.Append("SELECT applicationid, enterpriseid, consignment_no, booking_no, MAX(tracking_datetime) AS tracking_datetime, MAX(exception_code) AS exception_code "); 
			strBuilder.Append("FROM dbo.shipment_tracking WITH(NOLOCK) ");
			strBuilder.Append("WHERE applicationid = '"+strAppID+"' AND enterpriseid = '"+strEnterpriseID+"' AND (status_code = 'PODEX' AND isnull(deleted, 'N') <> 'Y') ");
			strBuilder.Append(" GROUP BY applicationid, enterpriseid, consignment_no, booking_no) px ");
			strBuilder.Append("ON s.applicationid = px.applicationid and s.enterpriseid = px.enterpriseid and s.booking_no = px.booking_no and s.consignment_no = px.consignment_no ");
			
			strBuilder.Append("LEFT OUTER JOIN dbo.Exception_code ec WITH(NOLOCK) ");
			strBuilder.Append("ON s.applicationid = ec.applicationid and s.enterpriseid = px.enterpriseid and ec.status_code = 'PODEX' and px.exception_code = ec.exception_code) x ");
			strBuilder.Append("GROUP BY DEL_STATUS, AUDIT_STATUS, EXCEPTION_CODE, exception_type, exception_description ");

			strSqlQuery = strBuilder.ToString();
		
			return strSqlQuery;
		}


		public static String GetSQICmdStrByPODDate(String strAppID, String strEnterpriseID, DataSet dsQuery)
		{	
			#region member
			String strstart_date=null;
			String strend_date=null;
			String strtran_date=null;
			String strpayer_type=null;
			String strpayer_code = null;
			String strorigin_dc = null;
			String strdestination_dc = null;
			String strroute_code = null;
			String strroute_type = null;
			String strdelPath_origin_dc = null;
			String strdelPath_destination_dc = null;
			String strzipcode = null;
			String strstate_code = null;
			String strservice_type = null;
			#endregion
			
			String strSqlQuery = null;
			StringBuilder strBuilder = null;
			DataRow dr = dsQuery.Tables[0].Rows[0];
			strBuilder = new StringBuilder();

			//*************************** Booking Date : Part ***************************
			if (Utility.IsNotDBNull(dr["tran_date"]))
			{
				strtran_date = " tracking_datetime ";
			}
			if (Utility.IsNotDBNull(dr["start_date"]) && Utility.IsNotDBNull(dr["end_date"]))
			{
				DateTime dateStartDate = (DateTime) dr["start_date"];
				DateTime dateEndDate = (DateTime) dr["end_date"];
				strstart_date = Utility.DateFormat(strAppID, strEnterpriseID, dateStartDate, DTFormat.DateTime);
				strend_date	= Utility.DateFormat(strAppID, strEnterpriseID, dateEndDate, DTFormat.DateTime);
			}			
			//*************************** Payer Type : Part ***************************
			String strpayer_type_tmp;
			strpayer_type_tmp = dr["payer_type"].ToString() + "";
			if (strpayer_type_tmp != "")
			{
				strpayer_type = " and payerid in ( select custid from customer where ";
				for (int i=0; i <= strpayer_type_tmp.Length - 1; i ++)
				{
					if (i==0)
					{
						strpayer_type += "payer_type = '" +strpayer_type_tmp.Substring(i,1).ToUpper()+ "'";
					}
					else 
					{
						strpayer_type += "or payer_type = '" +strpayer_type_tmp.Substring(i,1).ToUpper()+ "'";
					}
				}
				strpayer_type += " ) ";
			}

			strpayer_code = dr["payer_code"].ToString() + "";
			if (strpayer_code != "")
			{
				strpayer_code = "and payerid = '" +strpayer_code+ "'";
			}
			//*************************** Route / DC Selection : Part ***************************
			strroute_type = dr["route_type"].ToString() + "";
			strroute_code = dr["route_code"].ToString() + "";
			if (strroute_code != "")
			{
				
				if (strroute_type == "L" || strroute_type == "A")
				{
					strdelPath_origin_dc = "and origin_station = '" + dr["delPath_origin_dc"].ToString() + "'" ;
					strdelPath_destination_dc = "and destination_station = '" + dr["delPath_destination_dc"].ToString() + "'" ; 					
				}
				else
				{
					strroute_code = " and route_code = '" + strroute_code + "'";
				}
			}
			
			strorigin_dc = dr["origin_dc"].ToString() + "";
			if (strorigin_dc != "")
			{
				strorigin_dc = "and origin_station = '" +strorigin_dc+ "'";
			}

			strdestination_dc = dr["destination_dc"].ToString() + "";	
			if(strdestination_dc != "")
			{
				strdestination_dc = "and destination_station = '" +strdestination_dc+ "'";
			}
            
			//*************************** Destination ***************************
			strzipcode = dr["zip_code"].ToString() + "";
			if(strzipcode != "")
			{
				strzipcode = "and recipient_zipcode = '" +strzipcode+ "'";
			}
			strstate_code = dr["state_code"].ToString() + "";
			if(strstate_code != "")
			{
				strstate_code = "and destination_state_code = '" +strstate_code+ "'";
			}

			//*************************** Service Type ***************************
			strservice_type = dr["service_type"].ToString() + "";
			if(strservice_type != "")
			{
				strservice_type = "and service_code= '" +strservice_type+ "'";
			}
			string strMasterAcct = dr["master_account"].ToString();  //Jeab 18 Jul 2011
			
			/*
			//*********************************** Building Command String *****************************
			strBuilder.Append(" SELECT SHP.consignment_no, ");
			strBuilder.Append(" case isnull(SHP_DEL.consignment_no, '') ");
			strBuilder.Append(" when '' then 'NOT DEL' ");
			strBuilder.Append(" else 'DEL' end as DEL_STATUS, ");
			strBuilder.Append(" case ");
			strBuilder.Append(" when SHP_DEL.tracking_datetime is null then 'NOT DEL' ");
			strBuilder.Append(" else (case ");
			strBuilder.Append(" when SHP_DEL.tracking_datetime > SHP.est_delivery_datetime then ");
			strBuilder.Append(" (case ");
			strBuilder.Append(" when SHP_AUDIT.consignment_no is not null then 'LATE AUDIT' ");
			strBuilder.Append(" else 'LATE NOT AUDIT' end) ");
			strBuilder.Append(" else 'NOT LATE' end) ");
			strBuilder.Append(" end as AUDIT_STATUS, ISNULL(SHP_AUDIT.exception_code, 'NOT EXCEPT') as exception_code,"); 
			strBuilder.Append(" PODEX_CODE.exception_type, PODEX_CODE.exception_description ");
			strBuilder.Append(" from ");

			strBuilder.Append("(SELECT	SUB_SHP2.applicationid, ");
			strBuilder.Append(" SUB_SHP2.enterpriseid, SUB_SHP2.consignment_no, ");
			strBuilder.Append(" SUB_SHP2.booking_no, SUB_SHP2.est_delivery_datetime ");
			strBuilder.Append(" FROM(SELECT applicationid, enterpriseid, consignment_no, booking_no, MAX(tracking_datetime) AS tracking_datetime ");
			strBuilder.Append(" FROM    shipment_tracking 	WHERE  applicationid = '");
			strBuilder.Append(strAppID);
			strBuilder.Append("'");
			strBuilder.Append(" AND enterpriseid = '");
			strBuilder.Append(strEnterpriseID);
			strBuilder.Append("'");
			strBuilder.Append("AND (status_code = 'POD' AND isnull(deleted, 'N') <> 'Y') ");
			strBuilder.Append("AND" + strtran_date + " between " + strstart_date + " and " + strend_date);
			strBuilder.Append("GROUP BY applicationid, enterpriseid, consignment_no, booking_no	");
			strBuilder.Append(") SUB_SHP1 ");
			strBuilder.Append(" INNER JOIN ");
			//Jeab 18 Jul 2011
			if (strMasterAcct != "")
			{
				strBuilder.Append("(SELECT s.applicationid, s.enterpriseid, s.consignment_no, s.booking_no, s.est_delivery_datetime ");
				strBuilder.Append(" FROM Shipment  s  Left  Outer  Join  Customer c  ON ");
				strBuilder.Append(" s.applicationid = c.applicationid  And  s.enterpriseid  =  c.enterpriseid  And  s.payerid = c.custid  ");
				strBuilder.Append(" WHERE s.applicationid = '");
				strBuilder.Append(strAppID);
				strBuilder.Append("'");
				strBuilder.Append("AND s.enterpriseid = '");
				strBuilder.Append(strEnterpriseID);
				strBuilder.Append("'");
				strBuilder.Append("AND c.master_account = '");
				strBuilder.Append(strMasterAcct);
				strBuilder.Append("'");				
			}
			else
			{
				strBuilder.Append("(SELECT applicationid, enterpriseid, consignment_no, booking_no, est_delivery_datetime ");
				strBuilder.Append(" FROM Shipment ");
				strBuilder.Append(" WHERE applicationid = '");
				strBuilder.Append(strAppID);
				strBuilder.Append("'");
				strBuilder.Append("AND enterpriseid = '");
				strBuilder.Append(strEnterpriseID);
				strBuilder.Append("'");				
			}

			//Critiria
			strBuilder.Append(strdelPath_origin_dc +" ");
			strBuilder.Append(strdelPath_destination_dc +" ");
			strBuilder.Append(strroute_code +" ");
			strBuilder.Append(strorigin_dc +" ");
			strBuilder.Append(strdestination_dc +" ");
			strBuilder.Append(strzipcode +" ");
			strBuilder.Append(strstate_code +" ");
			strBuilder.Append(strpayer_code +" ");
			strBuilder.Append(strpayer_type +" ");
			strBuilder.Append(strservice_type +" ");
			strBuilder.Append(") SUB_SHP2 ");
			strBuilder.Append(" ON	(SUB_SHP1.applicationid = SUB_SHP2.applicationid ");
			strBuilder.Append(" AND SUB_SHP1.enterpriseid = SUB_SHP2.enterpriseid ");
			strBuilder.Append(" AND SUB_SHP1.consignment_no = SUB_SHP2.consignment_no ");
			strBuilder.Append("	AND SUB_SHP1.booking_no = SUB_SHP2.booking_no)");
			strBuilder.Append(")SHP ");

			//Jeab 18 Jul 2011  =========> End
			strBuilder.Append("LEFT OUTER JOIN ");
			strBuilder.Append("(SELECT	applicationid, enterpriseid, consignment_no, booking_no, MAX(tracking_datetime) AS tracking_datetime ");
			strBuilder.Append(" FROM	shipment_tracking ");
			strBuilder.Append(" WHERE 	applicationid = '");
			strBuilder.Append(strAppID);
			strBuilder.Append("'");
			strBuilder.Append(" AND enterpriseid = '");
			strBuilder.Append(strEnterpriseID);
			strBuilder.Append("'");
			strBuilder.Append(" AND (status_code = 'POD' AND isnull(deleted, 'N') <> 'Y') ");
			strBuilder.Append(" AND" + strtran_date + " between " + strstart_date + " and " + strend_date);
			strBuilder.Append(" GROUP BY applicationid, enterpriseid, consignment_no, booking_no	");
			strBuilder.Append(") SHP_DEL");
			strBuilder.Append(" ON	(SHP.applicationid = SHP_DEL.applicationid ");
			strBuilder.Append(" AND SHP.enterpriseid = SHP_DEL.enterpriseid ");
			strBuilder.Append("	AND SHP.consignment_no = SHP_DEL.consignment_no ");
			strBuilder.Append(" AND SHP.booking_no = SHP_DEL.booking_no)");
			strBuilder.Append(" LEFT OUTER JOIN");
			strBuilder.Append(" (SELECT  SUB_SHP_AUDIT2.*");
			strBuilder.Append(" FROM (SELECT 	SUB_SHP_AUDIT1_2.applicationid, SUB_SHP_AUDIT1_2.enterpriseid, ");
			strBuilder.Append(" SUB_SHP_AUDIT1_2.consignment_no, SUB_SHP_AUDIT1_2.booking_no, SUB_SHP_AUDIT1_2.tracking_datetime");
			strBuilder.Append(" FROM (SELECT applicationid, enterpriseid, consignment_no, booking_no, MAX(tracking_datetime) AS tracking_datetime ");
			strBuilder.Append(" FROM    	shipment_tracking");
			strBuilder.Append(" WHERE 	applicationid = '");
			strBuilder.Append(strAppID);
			strBuilder.Append("'");
			strBuilder.Append(" AND enterpriseid = '");
			strBuilder.Append(strEnterpriseID);
			strBuilder.Append("'");
			strBuilder.Append(" AND (status_code = 'POD' AND isnull(deleted, 'N') <> 'Y') ");
			strBuilder.Append(" AND" + strtran_date + " between " + strstart_date + " and " + strend_date);
			strBuilder.Append(" GROUP BY applicationid, enterpriseid, consignment_no, booking_no	");
			strBuilder.Append(" ) SUB_SHP_AUDIT1_1");
			strBuilder.Append(" INNER JOIN ");
			strBuilder.Append(" (SELECT 		applicationid, enterpriseid, consignment_no, booking_no, MAX(tracking_datetime) AS tracking_datetime ");
			strBuilder.Append(" FROM 		shipment_tracking ");
			strBuilder.Append(" WHERE 	applicationid = '");
			strBuilder.Append(strAppID);
			strBuilder.Append("'");
			strBuilder.Append(" AND enterpriseid = '");
			strBuilder.Append(strEnterpriseID);
			strBuilder.Append("'");
			strBuilder.Append(" AND (status_code = 'PODEX' AND isnull(deleted, 'N') <> 'Y') ");	
			strBuilder.Append(" GROUP BY applicationid, enterpriseid, consignment_no, booking_no	");
			strBuilder.Append(" ) SUB_SHP_AUDIT1_2 ");
			strBuilder.Append(" ON 	(SUB_SHP_AUDIT1_1.applicationid = SUB_SHP_AUDIT1_2.applicationid ");
			strBuilder.Append(" AND 	SUB_SHP_AUDIT1_1.enterpriseid = SUB_SHP_AUDIT1_2.enterpriseid ");
			strBuilder.Append(" AND 	SUB_SHP_AUDIT1_1.consignment_no = SUB_SHP_AUDIT1_2.consignment_no ");
			strBuilder.Append(" AND 	SUB_SHP_AUDIT1_1.booking_no = SUB_SHP_AUDIT1_2.booking_no) ");
			strBuilder.Append(" ) SUB_SHP_AUDIT1 ");
			strBuilder.Append(" LEFT OUTER JOIN ");
			strBuilder.Append(" (SELECT * ");
			strBuilder.Append(" FROM	shipment_tracking ");
			strBuilder.Append(" WHERE 	applicationid = '");
			strBuilder.Append(strAppID);
			strBuilder.Append("'");
			strBuilder.Append(" AND enterpriseid = '");
			strBuilder.Append(strEnterpriseID);
			strBuilder.Append("'");
			strBuilder.Append(" )SUB_SHP_AUDIT2 ");
			strBuilder.Append(" ON 	(SUB_SHP_AUDIT1.applicationid = SUB_SHP_AUDIT2.applicationid ");
			strBuilder.Append(" AND 	SUB_SHP_AUDIT1.enterpriseid = SUB_SHP_AUDIT2.enterpriseid "); 
			strBuilder.Append(" AND 	SUB_SHP_AUDIT1.consignment_no = SUB_SHP_AUDIT2.consignment_no "); 
			strBuilder.Append(" AND 	SUB_SHP_AUDIT1.booking_no = SUB_SHP_AUDIT2.booking_no ");
			strBuilder.Append(" AND	SUB_SHP_AUDIT1.tracking_datetime = SUB_SHP_AUDIT2.tracking_datetime) ");
			strBuilder.Append(" ) SHP_AUDIT	");
			strBuilder.Append(" ON	(SHP.applicationid = SHP_AUDIT.applicationid ");  
			strBuilder.Append(" AND 	SHP.enterpriseid = SHP_AUDIT.enterpriseid ");  
			strBuilder.Append(" AND 	SHP.consignment_no = SHP_AUDIT.consignment_no ");  
			strBuilder.Append(" AND 	SHP.booking_no = SHP_AUDIT.booking_no) "); 
			strBuilder.Append(" LEFT OUTER JOIN ");	
			strBuilder.Append(" (SELECT * 	FROM 	EXCEPTION_CODE ");
			strBuilder.Append(" WHERE 	applicationid = '");
			strBuilder.Append(strAppID);
			strBuilder.Append("'");
			strBuilder.Append(" AND enterpriseid = '");
			strBuilder.Append(strEnterpriseID);
			strBuilder.Append("'");
			strBuilder.Append(" ) PODEX_CODE ");
			strBuilder.Append(" ON	(SHP_AUDIT.applicationid = PODEX_CODE.applicationid ");	 
			strBuilder.Append(" AND 	SHP_AUDIT.enterpriseid = PODEX_CODE.enterpriseid ");  
			strBuilder.Append(" AND 	SHP_AUDIT.exception_code = PODEX_CODE.exception_code) "); 
			*/

			strBuilder.Append(" SELECT COUNT(consignment_no) AS consignment_no ");
			strBuilder.Append(",DEL_STATUS, AUDIT_STATUS, exception_code, exception_type, exception_description ");
			strBuilder.Append("FROM ( ");
			strBuilder.Append("SELECT s.consignment_no ");
			strBuilder.Append(",CASE WHEN pod.consignment_no IS NULL THEN 'NOT DEL' ELSE 'DEL' END as DEL_STATUS ");
			strBuilder.Append(",CASE WHEN pod.tracking_datetime IS NULL THEN 'NOT DEL' ");
			strBuilder.Append("ELSE ( ");
			strBuilder.Append("CASE WHEN pod.tracking_datetime > s.est_delivery_datetime ");
			strBuilder.Append("THEN ( ");
			strBuilder.Append("CASE WHEN px.consignment_no IS NOT NULL THEN 'LATE AUDIT' ELSE 'LATE NOT AUDIT' END) ");
			strBuilder.Append("ELSE 'NOT LATE' END) END as AUDIT_STATUS ");
			strBuilder.Append(", ISNULL(px.exception_code, 'NOT EXCEPT') as exception_code ");
			strBuilder.Append(",ec.exception_type, ec.exception_description, s.payerid ");
			strBuilder.Append("FROM ( ");
			strBuilder.Append("SELECT applicationid, enterpriseid, consignment_no, booking_no, est_delivery_datetime, payerid ");
			strBuilder.Append("FROM dbo.Shipment WITH(NOLOCK) ");
			strBuilder.Append("WHERE applicationid = '"+strAppID+"' AND enterpriseid = '"+strEnterpriseID+"' ");
			//strBuilder.Append("AND act_delivery_date between " + strstart_date + " and " + strend_date +" ");
			strBuilder.Append("AND act_delivery_date between " + strstart_date + " and " + strend_date +" ");
			
			// Criteria
			strBuilder.Append(strdelPath_origin_dc +" ");
			strBuilder.Append(strdelPath_destination_dc +" ");
			strBuilder.Append(strroute_code +" ");
			strBuilder.Append(strorigin_dc +" ");
			strBuilder.Append(strdestination_dc +" ");
			strBuilder.Append(strzipcode +" ");
			strBuilder.Append(strstate_code +" ");
			strBuilder.Append(strpayer_code +" ");
			strBuilder.Append(strpayer_type +" ");
			strBuilder.Append(strservice_type +" ");
			
			strBuilder.Append(") s ");
			
			if (strMasterAcct != "")
			{
				// When selecting by customer ID or master account ID
				strBuilder.Append(" INNER JOIN Customer c ");
				strBuilder.Append("ON s.applicationid = c.applicationid And  s.enterpriseid = c.enterpriseid And c.custid = s.payerid "); 
				// When user selected master account
				strBuilder.Append("AND c.master_account = '" + strMasterAcct +"' ");
			}
			/*
			else
			{
				// When user selected customer account
				strBuilder.Append("WHERE c.custid = '" + strCustAcct + "' ");
			}
			*/               

			//strBuilder.Append("LEFT OUTER JOIN ( ");
			strBuilder.Append("INNER JOIN ( ");
			strBuilder.Append("SELECT applicationid, enterpriseid, consignment_no, booking_no, MAX(tracking_datetime) AS tracking_datetime ");
			strBuilder.Append("FROM dbo.shipment_tracking WITH(NOLOCK) ");
			strBuilder.Append("WHERE applicationid = '"+strAppID+"' AND enterpriseid = '"+strEnterpriseID+"' AND (status_code = 'POD' AND isnull(deleted, 'N') <> 'Y') ");
			// Actual POD Date criteria is selected
			strBuilder.Append("AND" + strtran_date + " between " + strstart_date + " and " + strend_date);
			strBuilder.Append(" GROUP BY applicationid, enterpriseid, consignment_no, booking_no) pod ");
			strBuilder.Append("ON s.applicationid = pod.applicationid and s.enterpriseid = pod.enterpriseid and ");
			strBuilder.Append("s.booking_no = pod.booking_no and s.consignment_no = pod.consignment_no ");
			
			strBuilder.Append("LEFT OUTER JOIN ( ");
			strBuilder.Append("SELECT applicationid, enterpriseid, consignment_no, booking_no, MAX(tracking_datetime) AS tracking_datetime, MAX(exception_code) AS exception_code "); 
            strBuilder.Append("FROM dbo.shipment_tracking WITH(NOLOCK) ");
			strBuilder.Append("WHERE applicationid = '"+strAppID+"' AND enterpriseid = '"+strEnterpriseID+"' AND (status_code = 'PODEX' AND isnull(deleted, 'N') <> 'Y') ");
			// Actual POD Date criteria is selected
			//strBuilder.Append("AND" + strtran_date + " between " + strstart_date + " and " + strend_date);
			strBuilder.Append(" GROUP BY applicationid, enterpriseid, consignment_no, booking_no) px ");
			strBuilder.Append("ON s.applicationid = px.applicationid and s.enterpriseid = px.enterpriseid and s.booking_no = px.booking_no and s.consignment_no = px.consignment_no ");
			
			strBuilder.Append("LEFT OUTER JOIN dbo.Exception_code ec WITH(NOLOCK) ");
			strBuilder.Append("ON s.applicationid = ec.applicationid and s.enterpriseid = px.enterpriseid and ec.status_code = 'PODEX' and px.exception_code = ec.exception_code) x ");
			strBuilder.Append("GROUP BY DEL_STATUS, AUDIT_STATUS, EXCEPTION_CODE, exception_type, exception_description ");

			strSqlQuery = strBuilder.ToString();
		
			return strSqlQuery;
		}


		//Modifid By Tom
		public static String GetSQIVersusByMissingCode(String strAppID, String strEnterpriseID, DataSet dsQuery)
		{	
			#region member
			String strstart_date=null;
			String strend_date=null;
			String strtran_date=null;
			String strorigin_dc = null;
			String strdestination_dc = null;
			String conditions = null;
			#endregion
			
			String strSqlQuery = null;
			StringBuilder strBuilder = null;
			DataRow dr = dsQuery.Tables[0].Rows[0];
			strBuilder = new StringBuilder();

			//*************************** Booking Date : Part ***************************
			if (Utility.IsNotDBNull(dr["tran_date"]))
			{
				conditions = " LEFT OUTER JOIN ";
				if(dr["tran_date"].ToString().ToUpper() == "U")
					strtran_date = " last_status_datetime ";
				else if(dr["tran_date"].ToString().ToUpper() == "M")
				{
					strtran_date = " shpt_manifest_datetime ";
				}
			}
			if (Utility.IsNotDBNull(dr["start_date"]) && Utility.IsNotDBNull(dr["end_date"]))
			{
				DateTime dateStartDate = (DateTime) dr["start_date"];
				DateTime dateEndDate = (DateTime) dr["end_date"];
				strstart_date = Utility.DateFormat(strAppID, strEnterpriseID, dateStartDate, DTFormat.DateTime);
				strend_date	= Utility.DateFormat(strAppID, strEnterpriseID, dateEndDate, DTFormat.DateTime);
			}

			//*************************** Route / DC Selection : Part ***************************
			strorigin_dc = dr["origin_dc"].ToString() + "";
			if (strorigin_dc != "")
			{
				strorigin_dc = "and origin_station = '" +strorigin_dc+ "'";
			}

			strdestination_dc = dr["destination_dc"].ToString() + "";	
			if(strdestination_dc != "")
			{
				strdestination_dc = "and destination_station = '" +strdestination_dc+ "'";
			}
			
			//*************************** Status Code ***************************
			String strStatusCode1 = "'"+dr["state_code1"].ToString()+"'";
			String strStatusCode2 = "'"+dr["state_code2"].ToString()+"'";
			String strStatusCode3 = "'"+dr["state_code3"].ToString()+"'";
			//*********************************** Building Command String *****************************
			if (dr["versus_type"].ToString() == "M1")
			{
				strBuilder.Append(" SELECT	 SHP.consignment_no, SHP.ref_no, SHP.booking_no, SHP.payerid, SHP.route_code, ");
				strBuilder.Append("SHP.act_pickup_datetime, SHP.shpt_manifest_datetime, SHP.origin_station, SHP.destination_station, ");
				strBuilder.Append("STATUS_CODE1.status_code as sCode1, STATUS_CODE1.last_userid, STATUS_CODE1.tracking_datetime as sCode1DT, ");
				strBuilder.Append(strStatusCode2 + "  as sCode2, null as sCode2DT,'-'  as sCode3, null as sCode3DT , 0 as TotalTime, SHP.last_status_datetime as last_updated_date");
			
				strBuilder.Append(" FROM (SELECT	* FROM Shipment ");
				strBuilder.Append(" WHERE applicationid = '");
				strBuilder.Append(strAppID);
				strBuilder.Append("'");
				strBuilder.Append(" AND enterpriseid = '");
				strBuilder.Append(strEnterpriseID);
				strBuilder.Append("'");
				if(dr["tran_date"].ToString().ToUpper() != "P")
				{
					strBuilder.Append(" AND" + strtran_date + " between " + strstart_date + " and " + strend_date);
				}
			
				strBuilder.Append(strorigin_dc +" ");
				strBuilder.Append(strdestination_dc +" ");
				strBuilder.Append(") SHP ");

				strBuilder.Append(" INNER JOIN ");
				strBuilder.Append(" (SELECT		applicationid, enterpriseid, ");
				strBuilder.Append(" consignment_no, booking_no, status_code, last_userid, ");
				strBuilder.Append(" MAX(tracking_datetime) AS tracking_datetime ");
				strBuilder.Append(" FROM shipment_tracking ");
				strBuilder.Append(" WHERE applicationid = '");
				strBuilder.Append(strAppID);
				strBuilder.Append("'");
				strBuilder.Append(" AND enterpriseid = '");
				strBuilder.Append(strEnterpriseID);
				strBuilder.Append("'");
				strBuilder.Append(" AND (status_code = "+strStatusCode1+" AND isnull(deleted, 'N') <> 'Y') "); //Temp_Status_code1
				if(dr["tran_date"].ToString().ToUpper() != "P")
				{
					strBuilder.Append(" AND last_updated between " + strstart_date + " and " + strend_date);
				}
				strBuilder.Append(" GROUP BY	applicationid, enterpriseid, consignment_no, booking_no, status_code, last_userid) STATUS_CODE1 ");
				strBuilder.Append(" ON (SHP.applicationid =	STATUS_CODE1.applicationid ");
				strBuilder.Append(" AND	SHP.enterpriseid = STATUS_CODE1.enterpriseid ");
				strBuilder.Append(" AND	SHP.consignment_no = STATUS_CODE1.consignment_no ");
				strBuilder.Append(" AND	SHP.booking_no = STATUS_CODE1.booking_no) ");
			
				strBuilder.Append(" INNER JOIN ");
				strBuilder.Append(" (SELECT		applicationid, enterpriseid, ");
				strBuilder.Append(" consignment_no, booking_no ");
				strBuilder.Append(" FROM Shipment ");
				strBuilder.Append(" WHERE applicationid = '");
				strBuilder.Append(strAppID);
				strBuilder.Append("'");
				strBuilder.Append(" AND enterpriseid = '");
				strBuilder.Append(strEnterpriseID);
				strBuilder.Append("'");
				strBuilder.Append(strorigin_dc +" ");
				strBuilder.Append(strdestination_dc +" ");
				strBuilder.Append(" AND	consignment_no not in (SELECT DISTINCT consignment_no ");
				strBuilder.Append(" FROM Shipment_Tracking ");
				strBuilder.Append(" WHERE applicationid = '");
				strBuilder.Append(strAppID);
				strBuilder.Append("'");
				strBuilder.Append(" AND enterpriseid = '");
				strBuilder.Append(strEnterpriseID);
				strBuilder.Append("'");
				strBuilder.Append(" AND (status_code = " + strStatusCode2 + "AND isnull(deleted, 'N') <> 'Y'))) MISSING_SAT2 ");
				strBuilder.Append(" ON (SHP.applicationid	=	MISSING_SAT2.applicationid ");
				strBuilder.Append(" AND	SHP.enterpriseid =	MISSING_SAT2.enterpriseid ");
				strBuilder.Append(" AND	SHP.consignment_no = MISSING_SAT2.consignment_no ");
				strBuilder.Append(" AND	SHP.booking_no = MISSING_SAT2.booking_no) ");
				strBuilder.Append(" ORDER BY SHP.last_updated_date, SHP.consignment_no ");
			} else if (dr["versus_type"].ToString() == "M2")
			{
				strBuilder.Append(" SELECT	 SHP.consignment_no, SHP.ref_no, SHP.booking_no, SHP.payerid, SHP.route_code, ");
				strBuilder.Append("SHP.act_pickup_datetime, SHP.shpt_manifest_datetime, SHP.origin_station, SHP.destination_station, ");
				strBuilder.Append(strStatusCode2 + " as sCode2, null as sCode2DT, ");
				strBuilder.Append(" '-'  as sCode1, '-' as last_userid, null as sCode1DT,'-'  as sCode3, null as sCode3DT , 0 as TotalTime,SHP.last_status_datetime as last_updated_date");
			
				strBuilder.Append(" FROM (SELECT	* FROM Shipment ");
				strBuilder.Append(" WHERE applicationid = '");
				strBuilder.Append(strAppID);
				strBuilder.Append("'");
				strBuilder.Append(" AND enterpriseid = '");
				strBuilder.Append(strEnterpriseID);
				strBuilder.Append("'");
				if(dr["tran_date"].ToString().ToUpper() != "P")
				{
					strBuilder.Append(" AND" + strtran_date + " between " + strstart_date + " and " + strend_date);
				}
			
				strBuilder.Append(strorigin_dc +" ");
				strBuilder.Append(strdestination_dc +" ");
				strBuilder.Append(") SHP ");

				strBuilder.Append(" INNER JOIN ");
				strBuilder.Append(" (SELECT		applicationid, enterpriseid, ");
				strBuilder.Append(" consignment_no, booking_no ");
				strBuilder.Append(" FROM Shipment ");
				strBuilder.Append(" WHERE applicationid = '");
				strBuilder.Append(strAppID);
				strBuilder.Append("'");
				strBuilder.Append(" AND enterpriseid = '");
				strBuilder.Append(strEnterpriseID);
				strBuilder.Append("'");
				strBuilder.Append(strorigin_dc +" ");
				strBuilder.Append(strdestination_dc +" ");
				strBuilder.Append(" AND	consignment_no not in (SELECT DISTINCT consignment_no ");
				strBuilder.Append(" FROM Shipment_Tracking ");
				strBuilder.Append(" WHERE applicationid = '");
				strBuilder.Append(strAppID);
				strBuilder.Append("'");
				strBuilder.Append(" AND enterpriseid = '");
				strBuilder.Append(strEnterpriseID);
				strBuilder.Append("'");
				strBuilder.Append(" AND (status_code = " + strStatusCode2 + "AND isnull(deleted, 'N') <> 'Y'))) MISSING_SAT2 ");
				strBuilder.Append(" ON (SHP.applicationid	=	MISSING_SAT2.applicationid ");
				strBuilder.Append(" AND	SHP.enterpriseid =	MISSING_SAT2.enterpriseid ");
				strBuilder.Append(" AND	SHP.consignment_no = MISSING_SAT2.consignment_no ");
				strBuilder.Append(" AND	SHP.booking_no = MISSING_SAT2.booking_no) ");
//				strBuilder.Append(" ORDER BY SHP.last_updated_date, SHP.consignment_no ");
			} else if (dr["versus_type"].ToString() == "M3")
			{
				strBuilder.Append("SELECT AAA.* FROM (");
				strBuilder.Append(" SELECT	 SHP.consignment_no, SHP.ref_no, SHP.booking_no, SHP.payerid, SHP.route_code, ");
				strBuilder.Append("SHP.act_pickup_datetime, SHP.shpt_manifest_datetime, SHP.origin_station, SHP.destination_station, ");
				strBuilder.Append("STATUS_CODE1.status_code as sCode1, STATUS_CODE1.last_userid, STATUS_CODE1.tracking_datetime as sCode1DT, ");
				strBuilder.Append(strStatusCode2 + "  as sCode2, null as sCode2DT,'-'  as sCode3, null as sCode3DT , 0 as TotalTime,SHP.last_status_datetime as last_updated_date");
			
				strBuilder.Append(" FROM (SELECT	* FROM Shipment ");
				strBuilder.Append(" WHERE applicationid = '");
				strBuilder.Append(strAppID);
				strBuilder.Append("'");
				strBuilder.Append(" AND enterpriseid = '");
				strBuilder.Append(strEnterpriseID);
				strBuilder.Append("'");
				if(dr["tran_date"].ToString().ToUpper() != "P")
				{
					strBuilder.Append(" AND" + strtran_date + " between " + strstart_date + " and " + strend_date);
				}
			
				strBuilder.Append(strorigin_dc +" ");
				strBuilder.Append(strdestination_dc +" ");
				strBuilder.Append(") SHP ");

				strBuilder.Append(" INNER JOIN ");
				strBuilder.Append(" (SELECT		applicationid, enterpriseid, ");
				strBuilder.Append(" consignment_no, booking_no, status_code, last_userid, ");
				strBuilder.Append(" MAX(tracking_datetime) AS tracking_datetime ");
				strBuilder.Append(" FROM shipment_tracking ");
				strBuilder.Append(" WHERE applicationid = '");
				strBuilder.Append(strAppID);
				strBuilder.Append("'");
				strBuilder.Append(" AND enterpriseid = '");
				strBuilder.Append(strEnterpriseID);
				strBuilder.Append("'");
				strBuilder.Append(" AND (status_code = "+strStatusCode1+" AND isnull(deleted, 'N') <> 'Y') "); //Temp_Status_code1
				if(dr["tran_date"].ToString().ToUpper() != "P")
				{
					strBuilder.Append(" AND last_updated between " + strstart_date + " and " + strend_date);
				}
				strBuilder.Append(" GROUP BY	applicationid, enterpriseid, consignment_no, booking_no, status_code, last_userid) STATUS_CODE1 ");
				strBuilder.Append(" ON (SHP.applicationid =	STATUS_CODE1.applicationid ");
				strBuilder.Append(" AND	SHP.enterpriseid = STATUS_CODE1.enterpriseid ");
				strBuilder.Append(" AND	SHP.consignment_no = STATUS_CODE1.consignment_no ");
				strBuilder.Append(" AND	SHP.booking_no = STATUS_CODE1.booking_no) ");
			
				strBuilder.Append(" INNER JOIN ");
				strBuilder.Append(" (SELECT		applicationid, enterpriseid, ");
				strBuilder.Append(" consignment_no, booking_no ");
				strBuilder.Append(" FROM Shipment ");
				strBuilder.Append(" WHERE applicationid = '");
				strBuilder.Append(strAppID);
				strBuilder.Append("'");
				strBuilder.Append(" AND enterpriseid = '");
				strBuilder.Append(strEnterpriseID);
				strBuilder.Append("'");
				strBuilder.Append(strorigin_dc +" ");
				strBuilder.Append(strdestination_dc +" ");
				strBuilder.Append(" AND	consignment_no not in (SELECT DISTINCT consignment_no ");
				strBuilder.Append(" FROM Shipment_Tracking ");
				strBuilder.Append(" WHERE applicationid = '");
				strBuilder.Append(strAppID);
				strBuilder.Append("'");
				strBuilder.Append(" AND enterpriseid = '");
				strBuilder.Append(strEnterpriseID);
				strBuilder.Append("'");
				strBuilder.Append(" AND (status_code = " + strStatusCode2 + "AND isnull(deleted, 'N') <> 'Y'))) MISSING_SAT2 ");
				strBuilder.Append(" ON (SHP.applicationid	=	MISSING_SAT2.applicationid ");
				strBuilder.Append(" AND	SHP.enterpriseid =	MISSING_SAT2.enterpriseid ");
				strBuilder.Append(" AND	SHP.consignment_no = MISSING_SAT2.consignment_no ");
				strBuilder.Append(" AND	SHP.booking_no = MISSING_SAT2.booking_no) ");
//				strBuilder.Append(" ORDER BY SHP.last_updated_date, SHP.consignment_no ");

				strBuilder.Append("UNION");

				strBuilder.Append(" SELECT	 SHP.consignment_no, SHP.ref_no, SHP.booking_no, SHP.payerid, SHP.route_code, ");
				strBuilder.Append("SHP.act_pickup_datetime, SHP.shpt_manifest_datetime, SHP.origin_station, SHP.destination_station, ");
				strBuilder.Append("STATUS_CODE1.status_code as sCode1, STATUS_CODE1.last_userid, STATUS_CODE1.tracking_datetime as sCode1DT, ");
				strBuilder.Append("'-' as sCode2, null as sCode2DT," + strStatusCode3 + " as sCode3, null as sCode3DT , 0 as TotalTime,SHP.last_status_datetime as last_updated_date");
			
				strBuilder.Append(" FROM (SELECT	* FROM Shipment ");
				strBuilder.Append(" WHERE applicationid = '");
				strBuilder.Append(strAppID);
				strBuilder.Append("'");
				strBuilder.Append(" AND enterpriseid = '");
				strBuilder.Append(strEnterpriseID);
				strBuilder.Append("'");
				if(dr["tran_date"].ToString().ToUpper() != "P")
				{
					strBuilder.Append(" AND" + strtran_date + " between " + strstart_date + " and " + strend_date);
				}
			
				strBuilder.Append(strorigin_dc +" ");
				strBuilder.Append(strdestination_dc +" ");
				strBuilder.Append(") SHP ");

				strBuilder.Append(" INNER JOIN ");
				strBuilder.Append(" (SELECT		applicationid, enterpriseid, ");
				strBuilder.Append(" consignment_no, booking_no, status_code, last_userid, ");
				strBuilder.Append(" MAX(tracking_datetime) AS tracking_datetime ");
				strBuilder.Append(" FROM shipment_tracking ");
				strBuilder.Append(" WHERE applicationid = '");
				strBuilder.Append(strAppID);
				strBuilder.Append("'");
				strBuilder.Append(" AND enterpriseid = '");
				strBuilder.Append(strEnterpriseID);
				strBuilder.Append("'");
				strBuilder.Append(" AND (status_code = "+strStatusCode1+" AND isnull(deleted, 'N') <> 'Y') "); //Temp_Status_code1
				if(dr["tran_date"].ToString().ToUpper() != "P")
				{
					strBuilder.Append(" AND last_updated between " + strstart_date + " and " + strend_date);
				}
				strBuilder.Append(" GROUP BY	applicationid, enterpriseid, consignment_no, booking_no, status_code, last_userid) STATUS_CODE1 ");
				strBuilder.Append(" ON (SHP.applicationid =	STATUS_CODE1.applicationid ");
				strBuilder.Append(" AND	SHP.enterpriseid = STATUS_CODE1.enterpriseid ");
				strBuilder.Append(" AND	SHP.consignment_no = STATUS_CODE1.consignment_no ");
				strBuilder.Append(" AND	SHP.booking_no = STATUS_CODE1.booking_no) ");
			
				strBuilder.Append(" INNER JOIN ");
				strBuilder.Append(" (SELECT		applicationid, enterpriseid, ");
				strBuilder.Append(" consignment_no, booking_no ");
				strBuilder.Append(" FROM Shipment ");
				strBuilder.Append(" WHERE applicationid = '");
				strBuilder.Append(strAppID);
				strBuilder.Append("'");
				strBuilder.Append(" AND enterpriseid = '");
				strBuilder.Append(strEnterpriseID);
				strBuilder.Append("'");
				strBuilder.Append(strorigin_dc +" ");
				strBuilder.Append(strdestination_dc +" ");
				strBuilder.Append(" AND	consignment_no not in (SELECT DISTINCT consignment_no ");
				strBuilder.Append(" FROM Shipment_Tracking ");
				strBuilder.Append(" WHERE applicationid = '");
				strBuilder.Append(strAppID);
				strBuilder.Append("'");
				strBuilder.Append(" AND enterpriseid = '");
				strBuilder.Append(strEnterpriseID);
				strBuilder.Append("'");
				strBuilder.Append(" AND (status_code = " + strStatusCode3 + "AND isnull(deleted, 'N') <> 'Y'))) MISSING_SAT2 ");
				strBuilder.Append(" ON (SHP.applicationid	=	MISSING_SAT2.applicationid ");
				strBuilder.Append(" AND	SHP.enterpriseid =	MISSING_SAT2.enterpriseid ");
				strBuilder.Append(" AND	SHP.consignment_no = MISSING_SAT2.consignment_no ");
				strBuilder.Append(" AND	SHP.booking_no = MISSING_SAT2.booking_no)) AAA where AAA.consignment_no not in ");

				
				
				strBuilder.Append("(( SELECT	 SHP.consignment_no FROM (SELECT	* FROM Shipment ");
				strBuilder.Append(" WHERE applicationid = '");
				strBuilder.Append(strAppID);
				strBuilder.Append("'");
				strBuilder.Append(" AND enterpriseid = '");
				strBuilder.Append(strEnterpriseID);
				strBuilder.Append("'");
				if(dr["tran_date"].ToString().ToUpper() != "P")
				{
					strBuilder.Append(" AND" + strtran_date + " between " + strstart_date + " and " + strend_date);
				}
			
				strBuilder.Append(strorigin_dc +" ");
				strBuilder.Append(strdestination_dc +" ");
				strBuilder.Append(") SHP ");

				strBuilder.Append(" INNER JOIN ");
				strBuilder.Append(" (SELECT		applicationid, enterpriseid, ");
				strBuilder.Append(" consignment_no, booking_no, status_code, ");
				strBuilder.Append(" MAX(tracking_datetime) AS tracking_datetime ");
				strBuilder.Append(" FROM shipment_tracking ");
				strBuilder.Append(" WHERE applicationid = '");
				strBuilder.Append(strAppID);
				strBuilder.Append("'");
				strBuilder.Append(" AND enterpriseid = '");
				strBuilder.Append(strEnterpriseID);
				strBuilder.Append("'");
				strBuilder.Append(" AND (status_code = "+strStatusCode1+" AND isnull(deleted, 'N') <> 'Y') "); //Temp_Status_code1
				strBuilder.Append(" GROUP BY	applicationid, enterpriseid, consignment_no, booking_no, status_code) STATUS_CODE1 ");
				strBuilder.Append(" ON (SHP.applicationid =	STATUS_CODE1.applicationid ");
				strBuilder.Append(" AND	SHP.enterpriseid = STATUS_CODE1.enterpriseid ");
				strBuilder.Append(" AND	SHP.consignment_no = STATUS_CODE1.consignment_no ");
				strBuilder.Append(" AND	SHP.booking_no = STATUS_CODE1.booking_no) ");
			
				strBuilder.Append(" INNER JOIN ");
				strBuilder.Append(" (SELECT		applicationid, enterpriseid, ");
				strBuilder.Append(" consignment_no, booking_no ");
				strBuilder.Append(" FROM Shipment ");
				strBuilder.Append(" WHERE applicationid = '");
				strBuilder.Append(strAppID);
				strBuilder.Append("'");
				strBuilder.Append(" AND enterpriseid = '");
				strBuilder.Append(strEnterpriseID);
				strBuilder.Append("'");
				strBuilder.Append(strorigin_dc +" ");
				strBuilder.Append(strdestination_dc +" ");
				strBuilder.Append(" AND	consignment_no not in (SELECT DISTINCT consignment_no ");
				strBuilder.Append(" FROM Shipment_Tracking ");
				strBuilder.Append(" WHERE applicationid = '");
				strBuilder.Append(strAppID);
				strBuilder.Append("'");
				strBuilder.Append(" AND enterpriseid = '");
				strBuilder.Append(strEnterpriseID);
				strBuilder.Append("'");
				strBuilder.Append(" AND (status_code = " + strStatusCode2 + "AND isnull(deleted, 'N') <> 'Y'))) MISSING_SAT1 ");
				strBuilder.Append(" ON (SHP.applicationid	=	MISSING_SAT1.applicationid ");
				strBuilder.Append(" AND	SHP.enterpriseid =	MISSING_SAT1.enterpriseid ");
				strBuilder.Append(" AND	SHP.consignment_no = MISSING_SAT1.consignment_no ");
				strBuilder.Append(" AND	SHP.booking_no = MISSING_SAT1.booking_no) ");
			
				strBuilder.Append(" INNER JOIN ");
				strBuilder.Append(" (SELECT		applicationid, enterpriseid, ");
				strBuilder.Append(" consignment_no, booking_no ");
				strBuilder.Append(" FROM Shipment ");
				strBuilder.Append(" WHERE applicationid = '");
				strBuilder.Append(strAppID);
				strBuilder.Append("'");
				strBuilder.Append(" AND enterpriseid = '");
				strBuilder.Append(strEnterpriseID);
				strBuilder.Append("'");
				strBuilder.Append(strorigin_dc +" ");
				strBuilder.Append(strdestination_dc +" ");
				strBuilder.Append(" AND	consignment_no not in (SELECT DISTINCT consignment_no ");
				strBuilder.Append(" FROM Shipment_Tracking ");
				strBuilder.Append(" WHERE applicationid = '");
				strBuilder.Append(strAppID);
				strBuilder.Append("'");
				strBuilder.Append(" AND enterpriseid = '");
				strBuilder.Append(strEnterpriseID);
				strBuilder.Append("'");
				strBuilder.Append(" AND (status_code = " + strStatusCode3 + "AND isnull(deleted, 'N') <> 'Y'))) MISSING_SAT2 ");
				strBuilder.Append(" ON (SHP.applicationid	=	MISSING_SAT2.applicationid ");
				strBuilder.Append(" AND	SHP.enterpriseid =	MISSING_SAT2.enterpriseid ");
				strBuilder.Append(" AND	SHP.consignment_no = MISSING_SAT2.consignment_no ");
				strBuilder.Append(" AND	SHP.booking_no = MISSING_SAT2.booking_no))) ");
//				strBuilder.Append(" ORDER BY SHP.last_updated_date, SHP.consignment_no ");

				strBuilder.Append(" UNION ");
			
				
				strBuilder.Append("( SELECT	 SHP.consignment_no, SHP.ref_no, SHP.booking_no, SHP.payerid, SHP.route_code, ");
				strBuilder.Append("SHP.act_pickup_datetime, SHP.shpt_manifest_datetime, SHP.origin_station, SHP.destination_station, ");
				strBuilder.Append("STATUS_CODE1.status_code as sCode1, STATUS_CODE1.last_userid, STATUS_CODE1.tracking_datetime as sCode1DT, ");
				strBuilder.Append(strStatusCode2 + "  as sCode2, null as sCode2DT," + strStatusCode3 + "  as sCode3, null as sCode3DT , 0 as TotalTime, SHP.last_status_datetime as last_updated_date");
			
				strBuilder.Append(" FROM (SELECT	* FROM Shipment ");
				strBuilder.Append(" WHERE applicationid = '");
				strBuilder.Append(strAppID);
				strBuilder.Append("'");
				strBuilder.Append(" AND enterpriseid = '");
				strBuilder.Append(strEnterpriseID);
				strBuilder.Append("'");
				if(dr["tran_date"].ToString().ToUpper() != "P")
				{
					strBuilder.Append(" AND" + strtran_date + " between " + strstart_date + " and " + strend_date);
				}
			
				strBuilder.Append(strorigin_dc +" ");
				strBuilder.Append(strdestination_dc +" ");
				strBuilder.Append(") SHP ");

				strBuilder.Append(" INNER JOIN ");
				strBuilder.Append(" (SELECT		applicationid, enterpriseid, ");
				strBuilder.Append(" consignment_no, booking_no, status_code, last_userid, ");
				strBuilder.Append(" MAX(tracking_datetime) AS tracking_datetime ");
				strBuilder.Append(" FROM shipment_tracking ");
				strBuilder.Append(" WHERE applicationid = '");
				strBuilder.Append(strAppID);
				strBuilder.Append("'");
				strBuilder.Append(" AND enterpriseid = '");
				strBuilder.Append(strEnterpriseID);
				strBuilder.Append("'");
				strBuilder.Append(" AND (status_code = "+strStatusCode1+" AND isnull(deleted, 'N') <> 'Y') "); //Temp_Status_code1
				if(dr["tran_date"].ToString().ToUpper() != "P")
				{
					strBuilder.Append(" AND last_updated between " + strstart_date + " and " + strend_date);
				}
				strBuilder.Append(" GROUP BY	applicationid, enterpriseid, consignment_no, booking_no, status_code, last_userid) STATUS_CODE1 ");
				strBuilder.Append(" ON (SHP.applicationid =	STATUS_CODE1.applicationid ");
				strBuilder.Append(" AND	SHP.enterpriseid = STATUS_CODE1.enterpriseid ");
				strBuilder.Append(" AND	SHP.consignment_no = STATUS_CODE1.consignment_no ");
				strBuilder.Append(" AND	SHP.booking_no = STATUS_CODE1.booking_no) ");
				strBuilder.Append(" INNER JOIN ");
				strBuilder.Append(" (SELECT		applicationid, enterpriseid, ");
				strBuilder.Append(" consignment_no, booking_no ");
				strBuilder.Append(" FROM Shipment ");
				strBuilder.Append(" WHERE applicationid = '");
				strBuilder.Append(strAppID);
				strBuilder.Append("'");
				strBuilder.Append(" AND enterpriseid = '");
				strBuilder.Append(strEnterpriseID);
				strBuilder.Append("'");
				strBuilder.Append(strorigin_dc +" ");
				strBuilder.Append(strdestination_dc +" ");
				strBuilder.Append(" AND	consignment_no not in (SELECT DISTINCT consignment_no ");
				strBuilder.Append(" FROM Shipment_Tracking ");
				strBuilder.Append(" WHERE applicationid = '");
				strBuilder.Append(strAppID);
				strBuilder.Append("'");
				strBuilder.Append(" AND enterpriseid = '");
				strBuilder.Append(strEnterpriseID);
				strBuilder.Append("'");
				strBuilder.Append(" AND (status_code = " + strStatusCode2 + "AND isnull(deleted, 'N') <> 'Y'))) MISSING_SAT1 ");
				strBuilder.Append(" ON (SHP.applicationid	=	MISSING_SAT1.applicationid ");
				strBuilder.Append(" AND	SHP.enterpriseid =	MISSING_SAT1.enterpriseid ");
				strBuilder.Append(" AND	SHP.consignment_no = MISSING_SAT1.consignment_no ");
				strBuilder.Append(" AND	SHP.booking_no = MISSING_SAT1.booking_no) ");
				strBuilder.Append(" INNER JOIN ");
				strBuilder.Append(" (SELECT		applicationid, enterpriseid, ");
				strBuilder.Append(" consignment_no, booking_no ");
				strBuilder.Append(" FROM Shipment ");
				strBuilder.Append(" WHERE applicationid = '");
				strBuilder.Append(strAppID);
				strBuilder.Append("'");
				strBuilder.Append(" AND enterpriseid = '");
				strBuilder.Append(strEnterpriseID);
				strBuilder.Append("'");
				strBuilder.Append(strorigin_dc +" ");
				strBuilder.Append(strdestination_dc +" ");
				strBuilder.Append(" AND	consignment_no not in (SELECT DISTINCT consignment_no ");
				strBuilder.Append(" FROM Shipment_Tracking ");
				strBuilder.Append(" WHERE applicationid = '");
				strBuilder.Append(strAppID);
				strBuilder.Append("'");
				strBuilder.Append(" AND enterpriseid = '");
				strBuilder.Append(strEnterpriseID);
				strBuilder.Append("'");
				strBuilder.Append(" AND (status_code = " + strStatusCode3 + "AND isnull(deleted, 'N') <> 'Y'))) MISSING_SAT2 ");
				strBuilder.Append(" ON (SHP.applicationid	=	MISSING_SAT2.applicationid ");
				strBuilder.Append(" AND	SHP.enterpriseid =	MISSING_SAT2.enterpriseid ");
				strBuilder.Append(" AND	SHP.consignment_no = MISSING_SAT2.consignment_no ");
				strBuilder.Append(" AND	SHP.booking_no = MISSING_SAT2.booking_no)) ");
				strBuilder.Append(" ORDER BY last_updated_date, consignment_no ");
			
			}
			strSqlQuery = strBuilder.ToString();
		
			return strSqlQuery;
		}

		//End Modified By Tom

		public static String GetSQIVersusByOrder(String strAppID, String strEnterpriseID, DataSet dsQuery)
		{	
			#region member
			String strstart_date=null;
			String strend_date=null;
			String strtran_date=null;
			String strpayer_type=null;
			String strpayer_code = null;
			String strorigin_dc = null;
			String strdestination_dc = null;
			String strroute_code = null;
			String strroute_type = null;
			String strdelPath_origin_dc = null;
			String strdelPath_destination_dc = null;
			String strzipcode = null;
			String strstate_code = null;
			String conditions = null;
			#endregion
			
			String strSqlQuery = null;
			StringBuilder strBuilder = null;
			DataRow dr = dsQuery.Tables[0].Rows[0];
			strBuilder = new StringBuilder();

			//*************************** Booking Date : Part ***************************
			if (Utility.IsNotDBNull(dr["tran_date"]))
			{
				conditions = " LEFT OUTER JOIN ";
				if(dr["tran_date"].ToString().ToUpper() == "B")
					strtran_date = " booking_datetime ";
				else if(dr["tran_date"].ToString().ToUpper() == "U")
					strtran_date = " last_status_datetime ";
				else if(dr["tran_date"].ToString().ToUpper() == "P")
				{
					strtran_date = " act_delivery_date ";
					conditions = " INNER JOIN ";
				}
				else if(dr["tran_date"].ToString().ToUpper() == "C")
					strtran_date = " act_hc_return_datetime ";
			}
			if (Utility.IsNotDBNull(dr["start_date"]) && Utility.IsNotDBNull(dr["end_date"]))
			{
				DateTime dateStartDate = (DateTime) dr["start_date"];
				DateTime dateEndDate = (DateTime) dr["end_date"];
				strstart_date = Utility.DateFormat(strAppID, strEnterpriseID, dateStartDate, DTFormat.DateTime);
				strend_date	= Utility.DateFormat(strAppID, strEnterpriseID, dateEndDate, DTFormat.DateTime);
			}			
			//*************************** Payer Type : Part ***************************
			String strpayer_type_tmp;
			strpayer_type_tmp = dr["payer_type"].ToString() + "";
			if (strpayer_type_tmp != "")
			{
				strpayer_type = " and payerid in ( select custid from customer where ";
				for (int i=0; i <= strpayer_type_tmp.Length - 1; i ++)
				{
					if (i==0)
					{
						strpayer_type += "payer_type = '" +strpayer_type_tmp.Substring(i,1).ToUpper()+ "'";
					}
					else 
					{
						strpayer_type += "or payer_type = '" +strpayer_type_tmp.Substring(i,1).ToUpper()+ "'";
					}
				}
				strpayer_type += " ) ";
			}

			strpayer_code = dr["payer_code"].ToString() + "";
			if (strpayer_code != "")
			{
				strpayer_code = "and payerid = '" +strpayer_code+ "'";
			}
			//*************************** Route / DC Selection : Part ***************************
			strroute_type = dr["route_type"].ToString() + "";
			strroute_code = dr["route_code"].ToString() + "";
			if (strroute_code != "")
			{
				
				if (strroute_type == "L" || strroute_type == "A")
				{
					strdelPath_origin_dc = "and origin_station = '" + dr["delPath_origin_dc"].ToString() + "'" ;
					strdelPath_destination_dc = "and destination_station = '" + dr["delPath_destination_dc"].ToString() + "'" ; 					
				}
				else
				{
					strroute_code = " and route_code = '" + strroute_code + "'";
				}
			}
			
			strorigin_dc = dr["origin_dc"].ToString() + "";
			if (strorigin_dc != "")
			{
				strorigin_dc = "and origin_station = '" +strorigin_dc+ "'";
			}

			strdestination_dc = dr["destination_dc"].ToString() + "";	
			if(strdestination_dc != "")
			{
				strdestination_dc = "and destination_station = '" +strdestination_dc+ "'";
			}
            
			//*************************** Destination ***************************
			strzipcode = dr["zip_code"].ToString() + "";
			if(strzipcode != "")
			{
				strzipcode = "and recipient_zipcode = '" +strzipcode+ "'";
			}
			strstate_code = dr["state_code"].ToString() + "";
			if(strstate_code != "")
			{
				strstate_code = "and destination_state_code = '" +strstate_code+ "'";
			}

			//*************************** Status Code ***************************
			String strStatusCode1 = "'"+dr["state_code1"].ToString()+"'";
			String strStatusCode2 = "'"+dr["state_code2"].ToString()+"'";
			//*********************************** Building Command String *****************************
			strBuilder.Append(" SELECT	SHP.applicationid, SHP.enterpriseid, ");
			strBuilder.Append(" SHP.booking_no, SHP.consignment_no, SHP.ref_no, ");
			strBuilder.Append(" SHP.recipient_telephone, SHP.recipient_name, ");
			strBuilder.Append(" SHP.recipient_address1, SHP.recipient_address2, ");
			strBuilder.Append(" (select state_name from state ");
			strBuilder.Append(" where applicationid = SHP.applicationid ");
			strBuilder.Append(" and enterpriseid = SHP.enterpriseid ");
			strBuilder.Append(" and state_code = SHP.destination_state_code) as recipient_state_name, SHP.recipient_zipcode, ");
			strBuilder.Append(" SHP.sender_name, SHP.sender_address1, SHP.sender_address2, ");
			strBuilder.Append(" (select state_name from state ");
			strBuilder.Append(" where applicationid = SHP.applicationid ");
			strBuilder.Append(" and enterpriseid = SHP.enterpriseid ");
			strBuilder.Append(" and state_code = SHP.origin_state_code) as sender_state_name, SHP.sender_zipcode, ");
			strBuilder.Append(" SHP.origin_state_code, SHP.destination_state_code, ");
			strBuilder.Append(" SHP.route_code as delivery_route, SHP.booking_datetime, SHP.act_pickup_datetime, ");
			strBuilder.Append(" SHP_DEL.tracking_datetime as act_delivery_date, SHP.act_hc_return_datetime, ");
			strBuilder.Append(" STATUS_CODE1.status_code as sCode1, STATUS_CODE1.tracking_datetime as sCode1DT, ");
			strBuilder.Append(" STATUS_CODE2.status_code as sCode2, STATUS_CODE2.tracking_datetime as sCode2DT, ");
			strBuilder.Append(" isnull(datediff(minute, STATUS_CODE1.tracking_datetime, STATUS_CODE2.tracking_datetime),0) as TotalTime, SHP.last_status_datetime as last_updated_date ");
			//strBuilder.Append(" datediff(hh, STATUS_CODE1.tracking_datetime,STATUS_CODE2.tracking_datetime) as varchar),2)+':' + right('0' + cast(datediff(mi, STATUS_CODE1.tracking_datetime,STATUS_CODE2.tracking_datetime) % 60 as varchar),2) as TotalTime ");			
			strBuilder.Append(" FROM (SELECT	* FROM Shipment ");
			strBuilder.Append(" WHERE applicationid = '");
			strBuilder.Append(strAppID);
			strBuilder.Append("'");
			strBuilder.Append(" AND enterpriseid = '");
			strBuilder.Append(strEnterpriseID);
			strBuilder.Append("'");
			//strBuilder.Append(" AND booking_datetime between '02/01/2008 00:00' and '02/29/2008 23:59' ");//temp_Date
			if(dr["tran_date"].ToString().ToUpper() != "P")
			{
				strBuilder.Append(" AND" + strtran_date + " between " + strstart_date + " and " + strend_date);
			}
			
			strBuilder.Append(strroute_code +" ");
			strBuilder.Append(strdelPath_origin_dc +" ");
			strBuilder.Append(strdelPath_destination_dc +" ");
			strBuilder.Append(strorigin_dc +" ");
			strBuilder.Append(strdestination_dc +" ");
			strBuilder.Append(strzipcode +" ");
			strBuilder.Append(strstate_code +" ");
			strBuilder.Append(strpayer_code +" ");
			strBuilder.Append(strpayer_type +" ");
			strBuilder.Append(") SHP ");

			strBuilder.Append(" INNER JOIN ");
			strBuilder.Append(" (SELECT		applicationid, enterpriseid, ");
			strBuilder.Append(" consignment_no, booking_no, status_code, ");
			strBuilder.Append(" MAX(tracking_datetime) AS tracking_datetime ");
			strBuilder.Append(" FROM shipment_tracking ");
			strBuilder.Append(" WHERE applicationid = '");
			strBuilder.Append(strAppID);
			strBuilder.Append("'");
			strBuilder.Append(" AND enterpriseid = '");
			strBuilder.Append(strEnterpriseID);
			strBuilder.Append("'");
			strBuilder.Append(" AND (status_code = "+strStatusCode1+" AND isnull(deleted, 'N') <> 'Y') "); //Temp_Status_code1
			strBuilder.Append(" GROUP BY	applicationid, enterpriseid, consignment_no, booking_no, status_code) STATUS_CODE1 ");
			strBuilder.Append(" ON (SHP.applicationid =	STATUS_CODE1.applicationid ");
			strBuilder.Append(" AND	SHP.enterpriseid = STATUS_CODE1.enterpriseid ");
			strBuilder.Append(" AND	SHP.consignment_no = STATUS_CODE1.consignment_no ");
			strBuilder.Append(" AND	SHP.booking_no = STATUS_CODE1.booking_no) ");
			
			strBuilder.Append(" INNER JOIN ");
			strBuilder.Append(" (SELECT		applicationid, enterpriseid, ");
			strBuilder.Append(" consignment_no, booking_no, status_code, ");
			strBuilder.Append(" MAX(tracking_datetime) AS tracking_datetime ");
			strBuilder.Append(" FROM shipment_tracking ");
			strBuilder.Append(" WHERE applicationid = '");
			strBuilder.Append(strAppID);
			strBuilder.Append("'");
			strBuilder.Append(" AND enterpriseid = '");
			strBuilder.Append(strEnterpriseID);
			strBuilder.Append("'");
			strBuilder.Append(" AND	(status_code = "+strStatusCode2+" AND isnull(deleted, 'N') <> 'Y') ");//Temp_Status_code2
			strBuilder.Append(" GROUP BY	applicationid, enterpriseid, consignment_no, booking_no, status_code) STATUS_CODE2 ");
			strBuilder.Append(" ON (SHP.applicationid =	STATUS_CODE2.applicationid ");
			strBuilder.Append(" AND	SHP.enterpriseid = STATUS_CODE2.enterpriseid ");
			strBuilder.Append(" AND	SHP.consignment_no = STATUS_CODE2.consignment_no ");
			strBuilder.Append(" AND	SHP.booking_no = STATUS_CODE2.booking_no) ");
			
			strBuilder.Append(conditions);
			strBuilder.Append(" (SELECT	applicationid, enterpriseid, ");
			strBuilder.Append(" consignment_no, booking_no, MAX(tracking_datetime) AS tracking_datetime ");
			strBuilder.Append(" FROM shipment_tracking ");
			strBuilder.Append(" WHERE applicationid = '");
			strBuilder.Append(strAppID);
			strBuilder.Append("'");
			strBuilder.Append(" AND enterpriseid = '");
			strBuilder.Append(strEnterpriseID);
			strBuilder.Append("'"); 
			strBuilder.Append(" AND	(status_code = 'POD' AND isnull(deleted, 'N') <> 'Y') ");
			if(dr["tran_date"].ToString().ToUpper() == "P")
			{
				strBuilder.Append(" AND	tracking_datetime  between "+strstart_date+" and "+strend_date);
			}
			//strBuilder.Append(" AND	tracking_datetime  between "+strstart_date+" and "+strend_date);
			strBuilder.Append(" GROUP BY applicationid, enterpriseid, consignment_no, booking_no) SHP_DEL ");
			strBuilder.Append(" ON (SHP.applicationid = SHP_DEL.applicationid ");
			strBuilder.Append(" AND	SHP.enterpriseid = SHP_DEL.enterpriseid ");
			strBuilder.Append(" AND	SHP.consignment_no = SHP_DEL.consignment_no ");
			strBuilder.Append(" AND	SHP.booking_no = SHP_DEL.booking_no) ");

			strBuilder.Append(" WHERE	STATUS_CODE1.tracking_datetime < STATUS_CODE2.tracking_datetime ");
			
			strSqlQuery = strBuilder.ToString();
		
			return strSqlQuery;
		}


		public static DataSet GetSQIData(String strAppID, String strEnterpriseID, DataSet dsQuery)
		{	
			DbConnection dbCon= null;
			IDbCommand dbcmd=null;
			DataSet dsTopN = null;
			DataProviderType dbProvider = DbConnectionManager.GetInstance().GetDbProviderType(strAppID,strEnterpriseID);
			dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if (dbCon==null)
			{
				Logger.LogTraceError("ReportDAL.cs","GetTopNPayer","ERR001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			String strSqlQuery = null;
//			String strSqlWhere = null;
//			String strSqlSort = null;
//
//			DataRow dr = dsQuery.Tables[0].Rows[0];
//			strBuilder = new StringBuilder();
//
//			strBuilder.Append("SELECT SHP.consignment_no, ");
//			strBuilder.Append("case isnull(SHP_DEL.consignment_no, '') ");
//			strBuilder.Append("when '' then 'NOT DEL' ");
//			strBuilder.Append("else 'DEL' end as DEL_STATUS, ");
//			strBuilder.Append("case ");
//			strBuilder.Append("when SHP_DEL.tracking_datetime is null then 'NOT DEL' ");
//			strBuilder.Append("else (case ");
//			strBuilder.Append("when SHP_DEL.tracking_datetime > SHP.est_delivery_datetime then ");
//			strBuilder.Append("(case ");
//			strBuilder.Append("when SHP_AUDIT.consignment_no is not null then 'LATE AUDIT' ");
//			strBuilder.Append("else 'LATE NOT AUDIT' end) ");
//			strBuilder.Append("else 'NOT LATE' end) ");
//			strBuilder.Append("end as AUDIT_STATUS, SHP_AUDIT.exception_code, PODEX_CODE.exception_type ");
//			strBuilder.Append("from ");
//			strBuilder.Append("(SELECT applicationid, enterpriseid, consignment_no, booking_no, est_delivery_datetime ");
//			strBuilder.Append("FROM Shipment ");
//			strBuilder.Append("WHERE applicationid = 'TIES' ");
//			strBuilder.Append("AND enterpriseid = 'KDTH' ");
//
//			strBuilder.Append("AND est_delivery_datetime between '2007-10-01 00:00:00' and '2007-10-31 23:59:59') SHP ");
//			strBuilder.Append("LEFT OUTER JOIN ");
//			strBuilder.Append("(SELECT shipment_tracking.* ");
//			strBuilder.Append("FROM shipment_tracking INNER JOIN ");
//			strBuilder.Append("(SELECT applicationid, enterpriseid, consignment_no, booking_no, MAX(tracking_datetime) AS tracking_datetime ");
//			strBuilder.Append("FROM shipment_tracking ");
//			strBuilder.Append("WHERE (status_code = 'POD' AND isnull(deleted, 'N') <> 'Y') ");
//			strBuilder.Append("AND consignment_no in (select distinct consignment_no ");
//
//			strBuilder.Append("from Shipment ");
//			strBuilder.Append("where applicationid = 'TIES' ");
//			strBuilder.Append("and enterpriseid = 'KDTH' ");
//			strBuilder.Append("and est_delivery_datetime between '2007-10-01 00:00:00' and '2007-10-31 23:59:59') ");
//
//			strBuilder.Append("GROUP BY applicationid, enterpriseid, consignment_no, booking_no) SUB_TRACK1 ON "); 
//			strBuilder.Append("(dbo.shipment_tracking.applicationid = SUB_TRACK1.applicationid AND ");
//			strBuilder.Append("dbo.shipment_tracking.enterpriseid = SUB_TRACK1.enterpriseid AND ");
//			strBuilder.Append("dbo.shipment_tracking.consignment_no = SUB_TRACK1.consignment_no AND ");
//			strBuilder.Append("dbo.shipment_tracking.booking_no = SUB_TRACK1.booking_no AND ");
//			strBuilder.Append("dbo.shipment_tracking.tracking_datetime = SUB_TRACK1.tracking_datetime)) SHP_DEL ");
//			strBuilder.Append("ON (SHP.applicationid = SHP_DEL.applicationid ");
//			strBuilder.Append("AND SHP.enterpriseid = SHP_DEL.enterpriseid ");
//			strBuilder.Append("AND SHP.consignment_no = SHP_DEL.consignment_no ");
//			strBuilder.Append("AND SHP.booking_no = SHP_DEL.booking_no) ");
//			strBuilder.Append("LEFT OUTER JOIN ");
//			strBuilder.Append("(SELECT shipment_tracking.* ");
//			strBuilder.Append("FROM shipment_tracking INNER JOIN ");
//			strBuilder.Append("(SELECT applicationid, enterpriseid, consignment_no, booking_no, MAX(tracking_datetime) AS tracking_datetime ");
//			strBuilder.Append("FROM shipment_tracking ");
//			strBuilder.Append("WHERE (status_code = 'PODEX' AND isnull(deleted, 'N') <> 'Y') ");
//			strBuilder.Append("AND consignment_no in (	select distinct consignment_no ");
//
//			strBuilder.Append("from Shipment ");
//			strBuilder.Append("where applicationid = 'TIES' ");
//			strBuilder.Append("and enterpriseid = 'KDTH' ");
//			strBuilder.Append("and est_delivery_datetime between '2007-10-01 00:00:00' and '2007-10-31 23:59:59') ");
//
//			strBuilder.Append("GROUP BY applicationid, enterpriseid, consignment_no, booking_no) SUB_TRACK2 ON ");
//			strBuilder.Append("(dbo.shipment_tracking.applicationid = SUB_TRACK2.applicationid AND ");
//			strBuilder.Append("dbo.shipment_tracking.enterpriseid = SUB_TRACK2.enterpriseid AND ");
//			strBuilder.Append("dbo.shipment_tracking.consignment_no = SUB_TRACK2.consignment_no AND ");
//			strBuilder.Append("dbo.shipment_tracking.booking_no = SUB_TRACK2.booking_no AND ");
//			strBuilder.Append("dbo.shipment_tracking.tracking_datetime = SUB_TRACK2.tracking_datetime)) SHP_AUDIT ");
//			strBuilder.Append("ON(SHP.applicationid = SHP_AUDIT.applicationid ");
//			strBuilder.Append("AND SHP.enterpriseid = SHP_AUDIT.enterpriseid ");
//			strBuilder.Append("AND SHP.consignment_no = SHP_AUDIT.consignment_no ");
//			strBuilder.Append("AND SHP.booking_no = SHP_AUDIT.booking_no) ");
//			strBuilder.Append("LEFT OUTER JOIN ");
//			strBuilder.Append("(SELECT * ");
//			strBuilder.Append("FROM EXCEPTION_CODE ");
//			strBuilder.Append("where applicationid = 'TIES' ");
//			strBuilder.Append("and enterpriseid = 'KDTH' ");
//			strBuilder.Append("and status_code = 'PODEX') PODEX_CODE ");
//			strBuilder.Append("ON (SHP_AUDIT.applicationid = PODEX_CODE.applicationid ");
//			strBuilder.Append("AND SHP_AUDIT.enterpriseid = PODEX_CODE.enterpriseid ");
//			strBuilder.Append("AND SHP_AUDIT.exception_code = PODEX_CODE.exception_code) ");
//
//		
			DataRow dr = dsQuery.Tables[0].Rows[0];
			if (Utility.IsNotDBNull(dr["tran_date"]))
			{
				if (dr["tran_date"].ToString().ToUpper() == "P")
				{	strSqlQuery = GetSQICmdStrByPODDate(strAppID,strEnterpriseID,dsQuery); }
				else
				{	strSqlQuery = GetSQICommandStr(strAppID,strEnterpriseID,dsQuery); }

			}
		
			dbcmd = dbCon.CreateCommand(strSqlQuery,CommandType.Text);
			dbcmd.CommandTimeout = 600;
 
			try
			{
				dsTopN = (DataSet)dbCon.ExecuteQuery(dbcmd,com.common.DAL.ReturnType.DataSetType);  
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("ReportDAL.cs","GetTopNPayer","ERR002","Error in Query String "+appException.Message);
				throw appException;
			}
			return dsTopN;
		}


		public static DataSet GetSQIDataVersus(String strAppID, String strEnterpriseID, DataSet dsQuery)
		{	
			DbConnection dbCon= null;
			IDbCommand dbcmd=null;
			DataSet dsTopN = null;
			DataProviderType dbProvider = DbConnectionManager.GetInstance().GetDbProviderType(strAppID,strEnterpriseID);
			dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if (dbCon==null)
			{
				Logger.LogTraceError("ReportDAL.cs","GetTopNPayer","ERR001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			String strSqlQuery = null;

			DataRow dr = dsQuery.Tables[0].Rows[0];
			if (Utility.IsNotDBNull(dr["tran_date"]))
			{
				if(dr["versus_type"] == "O")
					strSqlQuery = GetSQIVersusByOrder(strAppID,strEnterpriseID,dsQuery);
				else
					strSqlQuery = GetSQIVersusByMissingCode(strAppID,strEnterpriseID,dsQuery);
			}
		
			dbcmd = dbCon.CreateCommand(strSqlQuery,CommandType.Text);
			dbcmd.CommandTimeout = 1500;
 
			try
			{
				dsTopN = (DataSet)dbCon.ExecuteQuery(dbcmd,com.common.DAL.ReturnType.DataSetType);  
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("ReportDAL.cs","GetTopNPayer","ERR002","Error in Query String "+appException.Message);
				throw appException;
			}
			return dsTopN;
		}


		public static DataSet GetHeaderCompName(String strAppID, String strEnterpriseID)
		{
			DbConnection dbCon= null;
			IDbCommand dbcmd=null;
			DataSet dsHeader = null;
			DataProviderType dbProvider = DbConnectionManager.GetInstance().GetDbProviderType(strAppID,strEnterpriseID);
			dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if (dbCon==null)
			{
				Logger.LogTraceError("ReportDAL.cs","GetHeaderCompName","ERR001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			StringBuilder strBuilder = new StringBuilder();
			strBuilder.Append("select enterprise_name as companyName, ");
			strBuilder.Append("(Enterprise.address1+' '+Enterprise.address2) as companyAddress, ");
			strBuilder.Append("country+' '+zipcode as countryName ");
			strBuilder.Append("from Enterprise ");
			strBuilder.Append("where ");
			strBuilder.Append("applicationid=");
			strBuilder.Append("'"+strAppID+"'");
			strBuilder.Append("and enterpriseid=");
			strBuilder.Append("'"+strEnterpriseID+"'");

			dbcmd = dbCon.CreateCommand(strBuilder.ToString(),CommandType.Text);
			try
			{
				dsHeader = (DataSet)dbCon.ExecuteQuery(dbcmd,com.common.DAL.ReturnType.DataSetType); 
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("ReportDAL.cs","GetHeaderCompName","ERR002","Error in Query String "+appException.Message);
				throw appException;
			}

			return dsHeader;
		}
		//SOMPOTE
		public static DataSet GetCreditAging(DataSet dsQuery)
		{	
			DbConnection dbCon= null;
			IDbCommand dbcmd=null;
			DataSet dsCreditAging = null;
			//StringBuilder strBuilder = null;

			DataRow dr = dsQuery.Tables[0].Rows[0];
			string applicationid = null; 
			string enterpriseid = null; 
			string custid = null; 
			string salesmanid = null; 
			string status_active = null; 
			string master_account = null; 
			int ref_date_no = 0;
			bool isSummary = true; 
			bool isReport = true ;	

			if (Utility.IsNotDBNull(dr["applicationid"])) 
				applicationid = dr["applicationid"].ToString();
			if (Utility.IsNotDBNull(dr["enterpriseid"])) 
				enterpriseid = dr["enterpriseid"].ToString();
			if (Utility.IsNotDBNull(dr["custid"])) 
				custid = dr["custid"].ToString();
			if (Utility.IsNotDBNull(dr["salesmanid"])) 
				salesmanid = dr["salesmanid"].ToString();
			if (Utility.IsNotDBNull(dr["status_active"])) 
				status_active = dr["status_active"].ToString();
			if (Utility.IsNotDBNull(dr["master_account"])) 
				master_account = dr["master_account"].ToString();
			if (Utility.IsNotDBNull(dr["ref_date_no"])) 
				ref_date_no = Convert.ToInt16(dr["ref_date_no"].ToString());
			if (Utility.IsNotDBNull(dr["isSummary"])) 
				isSummary = Convert.ToBoolean(dr["isSummary"].ToString()); 
			if (Utility.IsNotDBNull(dr["isReport"])) 
				isReport = Convert.ToBoolean(dr["isReport"].ToString());

			DataProviderType dbProvider = DbConnectionManager.GetInstance().GetDbProviderType(applicationid,enterpriseid);
			dbCon = DbConnectionManager.GetInstance().GetDbConnection(applicationid,enterpriseid);

			if (dbCon==null)
			{
				Logger.LogTraceError("ReportDAL.cs","GetCreditAging","ERR001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			//String strSqlQuery = null;
			//String strSqlWhere = null;
			//String strSqlSort = null;

			
			//strBuilder = new StringBuilder();


			/*switch (dbProvider)
			{
				case DataProviderType.Sql :
										
					break;
				
				case DataProviderType.Oracle:
									

					break;
				
				case DataProviderType.Oledb:
					break;
			}*/
		

			//strSqlQuery = "GetCreditAging";
			//dbcmd.CommandType = CommandType.StoredProcedure;
			dbcmd = dbCon.CreateCommand("GetCreditAging",CommandType.StoredProcedure);

			IDbDataParameter paramApplicationid = dbcmd.CreateParameter();
			paramApplicationid.DbType = DbType.String;
			paramApplicationid.ParameterName = "@applicationid";
			paramApplicationid.Value = applicationid;			
			dbcmd.Parameters.Add(paramApplicationid);

			IDbDataParameter paramEnterpriseid = dbcmd.CreateParameter();
			paramEnterpriseid.DbType = DbType.String;
			paramEnterpriseid.ParameterName = "@enterpriseid";
			paramEnterpriseid.Value = enterpriseid;			
			dbcmd.Parameters.Add(paramEnterpriseid);

			IDbDataParameter paramCustid = dbcmd.CreateParameter();
			paramCustid.DbType = DbType.String;
			paramCustid.ParameterName = "@custid";
			paramCustid.Value = custid;			
			dbcmd.Parameters.Add(paramCustid);

			IDbDataParameter paramSalesmanid = dbcmd.CreateParameter();
			paramSalesmanid.DbType = DbType.String;
			paramSalesmanid.ParameterName = "@salesmanid";
			paramSalesmanid.Value = salesmanid;			
			dbcmd.Parameters.Add(paramSalesmanid);

			IDbDataParameter paramStatus_active = dbcmd.CreateParameter();
			paramStatus_active.DbType = DbType.String;
			paramStatus_active.ParameterName = "@status_active";
			paramStatus_active.Value = status_active;			
			dbcmd.Parameters.Add(paramStatus_active);

			IDbDataParameter paramMaster_account = dbcmd.CreateParameter();
			paramMaster_account.DbType = DbType.String;
			paramMaster_account.ParameterName = "@master_account";
			paramMaster_account.Value = master_account;			
			dbcmd.Parameters.Add(paramMaster_account);

			IDbDataParameter paramRef_date_no = dbcmd.CreateParameter();
			paramRef_date_no.DbType = DbType.Int16;
			paramRef_date_no.ParameterName = "@ref_date_no";
			paramRef_date_no.Value = ref_date_no;			
			dbcmd.Parameters.Add(paramRef_date_no);

			IDbDataParameter paramIsSummary = dbcmd.CreateParameter();
			paramIsSummary.DbType = DbType.Boolean;
			paramIsSummary.ParameterName = "@isSummary";
			paramIsSummary.Value = isSummary;			
			dbcmd.Parameters.Add(paramIsSummary);
			
			IDbDataParameter paramIsReport = dbcmd.CreateParameter();
			paramIsReport.DbType = DbType.Boolean;
			paramIsReport.ParameterName = "@isReport";
			paramIsReport.Value = isReport;			
			dbcmd.Parameters.Add(paramIsReport);

			try
			{
				dsCreditAging = (DataSet)dbCon.ExecuteQuery(dbcmd,com.common.DAL.ReturnType.DataSetType);  
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("ReportDAL.cs","GetTopNPayer","ERR002","Error in Query String "+appException.Message);
				throw appException;
			}
			return dsCreditAging;
		}
	}
}
