using System;
using System.Data;
using System.Collections;
using System.Text;
using com.common.DAL;
using com.common.util;
using com.common.classes;
namespace com.ties.DAL
{
	/// <summary>
	/// Summary description for SysDataManager1.
	/// </summary>
	public class SysDataManager1
	{
		public SysDataManager1()
		{
			
		}
		//-------Industrial Sector------
		/// <summary>
		/// Gets and empty dataSet
		/// </summary>
		/// <param name="iNumRows">Rows to set empty</param>
		/// <returns></returns>
		public static SessionDS GetEmptyISector(int iNumRows)
		{
			DataTable dtISector = new DataTable();

			dtISector.Columns.Add(new DataColumn("industrial_sector_code", typeof(string)));
			dtISector.Columns.Add(new DataColumn("industrial_sector_description", typeof(string)));
			dtISector.Columns.Add(new DataColumn("industrial_sector_type", typeof(string)));

			for(int i=0; i < iNumRows; i++)
			{
				DataRow drEach = dtISector.NewRow();
				drEach[0] = "";
				drEach[1] = "";
				drEach[2] = "";

				dtISector.Rows.Add(drEach);
			}
			SessionDS sdsISector = new SessionDS();
			DataSet dsISector = new DataSet();
			dsISector.Tables.Add(dtISector);

//			dsISector.Tables[0].Columns["industrial_sector_code"].Unique = true; //Checks duplicate records..
			dsISector.Tables[0].Columns["industrial_sector_code"].AllowDBNull = false;

			sdsISector.ds = dsISector;

			sdsISector.DataSetRecSize = dsISector.Tables[0].Rows.Count;
			sdsISector.QueryResultMaxSize = sdsISector.DataSetRecSize;

			return  sdsISector;
	
		}

		public static void AddNewRowInIndSectorDS(ref SessionDS dsIndSectorGrid)
		{
			DataRow drNew = dsIndSectorGrid.ds.Tables[0].NewRow();

			drNew[0] = "";
			drNew[1] = "";
			
			try
			{
				dsIndSectorGrid.ds.Tables[0].Rows.Add(drNew);
			}
			catch(Exception ex)
			{
				Logger.LogTraceError("SysDataMgrDAL1","AddNewRowInIndSectorDS","R005",ex.Message.ToString());
			}
		}
		/// <summary>
		/// Querying for Industrial Sector
		/// </summary>
		/// <param name="sdsISector">SessionDS to be pass in that contains data for querying</param>
		/// <param name="strAppID">ApplicationID</param>
		/// <param name="strEnterpriseID">EnterprsieID</param>
		/// <param name="recStart">Start of Record</param>
		/// <param name="recSize">Size of Record</param>
		/// <returns></returns>
		public static SessionDS QueryISector(SessionDS sdsISector,String strAppID, String strEnterpriseID, int recStart, int recSize)
		{
			SessionDS sdsBaseTables = null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
		
			DataRow dr = sdsISector.ds.Tables[0].Rows[0];
			String strCode = (String)dr[0];
			String strDesc = (String)dr[1];
			

			String strSQLQuery = "select industrial_sector_code, industrial_sector_description from Industrial_Sector where ";
			
			strSQLQuery += "applicationid = '"+strAppID+"' and enterpriseid = '"+strEnterpriseID+"' "; 
			if(strCode!=null && strCode!="")
			{
				strSQLQuery += "and industrial_sector_code like '%"+Utility.ReplaceSingleQuote(strCode)+"%' "; 
			}
			if(strDesc!=null && strDesc!="")
			{
				strSQLQuery += "and industrial_sector_description like N'%"+Utility.ReplaceSingleQuote(strDesc)+"%' "; 
			}
			//dsBaseTables = (DataSet)dbCon.ExecuteQuery(strSQLQuery,ReturnType.DataSetType);
			sdsBaseTables = (SessionDS)dbCon.ExecuteQuery(strSQLQuery,recStart, recSize,"ISector");

			return  sdsBaseTables;
		}
		/// <summary>
		/// to update new rows into Industrial sector database
		/// </summary>
		/// <param name="sdsBaseTables">SessionDS taht contain data to be inserted to database</param>
		/// <param name="rowIndex">row to be updated to database</param>
		/// <param name="strAppID">applicationid</param>
		/// <param name="strEnterpriseID">enterpriseid</param>
		/// <returns></returns>
		public static bool UpdateISector(SessionDS sdsBaseTables,int rowIndex, String strAppID, String strEnterpriseID)
		{
			IDbCommand dbCom = null;
			DataRow dr = sdsBaseTables.ds.Tables[0].Rows[rowIndex];
			String strCode = (String)dr[0];
			String strDescription = (String)dr[1];

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(strCode!=null && strCode!="")
			{
				String strSQLQuery = "update Industrial_Sector set industrial_sector_code = '"+strCode+"', industrial_sector_description = N'"+Utility.ReplaceSingleQuote(strDescription)+"' where ";

				strSQLQuery += "applicationid = '"+strAppID+"' and enterpriseid = '"+strEnterpriseID+"' and industrial_sector_code = '"+Utility.ReplaceSingleQuote(strCode)+"' "; 
	
				dbCom = dbCon.CreateCommand(strSQLQuery,CommandType.Text);
				if(dbCon.ExecuteNonQuery(dbCom)>0)
					return true;
			}
			return false;
		}
		/// <summary>
		/// To insert a new record to database
		/// </summary>
		/// <param name="sdsBaseTables">SesionDs that contain data for inserting</param>
		/// <param name="strAppID">applicationid</param>
		/// <param name="strEnterpriseID">enterprsieid</param>
		/// <returns></returns>
		public static bool InsertISector(SessionDS sdsBaseTables, String strAppID, String strEnterpriseID)
		{
			IDbCommand dbCom = null;
			DataRow dr = sdsBaseTables.ds.Tables[0].Rows[0];
			String strCode = (String)dr[0];
			String strDescription = (String)dr[1];

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
				
			if(strCode!=null && strCode!="")
			{
				String strSQLQuery = "insert into Industrial_Sector(applicationid,enterpriseid,industrial_sector_code, industrial_sector_description) ";
				strSQLQuery += " values('"+strAppID+"','"+strEnterpriseID+"', '"+Utility.ReplaceSingleQuote(strCode)+"',N'"+Utility.ReplaceSingleQuote(strDescription)+"') ";//= '"+strAppID+"' and enterpriseid = '"+strEnterpriseID+"' "; 
			
				dbCom = dbCon.CreateCommand(strSQLQuery,CommandType.Text);
				if(dbCon.ExecuteNonQuery(dbCom)>0)
					return true;
			}
			return false;
		}
		/// <summary>
		/// To delete record
		/// </summary>
		/// <param name="strAppID">applicationid</param>
		/// <param name="strEnterpriseID">enterpriseid</param>
		/// <param name="strCode">record to be delted base on this code</param>
		/// <returns></returns>
		public static bool DeleteISector(String strAppID, String strEnterpriseID, String strCode)
		{
			IDbCommand dbCom = null;
			String strSQLQuery = null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			
			strSQLQuery = "delete from Industrial_Sector where applicationid = ";
			strSQLQuery += "'"+strAppID+"' and enterpriseid ='"+strEnterpriseID+"'";
			strSQLQuery += " and industrial_sector_code = '"+strCode+"'";
			try
			{
				dbCom = dbCon.CreateCommand(strSQLQuery,CommandType.Text);
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("SysDataMgr1","DeleteISector","SDM001","Error During Delete "+ appException.Message.ToString());
				throw new ApplicationException("Error deleting Industrial Sector : "+appException.Message,appException);
			}

			if(dbCon.ExecuteNonQuery(dbCom)>0)
			{
				return true;
			}
			return false;
		}
		
	
		#region "Distribution Centers"

		//-------Distribution Centers------
		/// <summary>
		/// Gets and empty dataSet
		/// </summary>
		/// <param name="iNumRows">Rows to set empty</param>
		/// <returns></returns>
		public static SessionDS GetEmptyDCenter(int iNumRows)
		{
			DataTable dtDCenter = new DataTable();

			dtDCenter.Columns.Add(new DataColumn("origin_code", typeof(string)));
			dtDCenter.Columns.Add(new DataColumn("origin_name", typeof(string)));
			dtDCenter.Columns.Add(new DataColumn("hub", typeof(string)));
			dtDCenter.Columns.Add(new DataColumn("manifest", typeof(string)));
			dtDCenter.Columns.Add(new DataColumn("EmailAddress", typeof(string)));
			dtDCenter.Columns.Add(new DataColumn("Zipcode", typeof(string)));
			dtDCenter.Columns.Add(new DataColumn("OperatedBy", typeof(string)));
			dtDCenter.Columns.Add(new DataColumn("TurnaroundTime", typeof(string)));
			dtDCenter.Columns.Add(new DataColumn("NearestAirport", typeof(string)));
			for(int i=0; i < iNumRows; i++)
			{
				DataRow drEach = dtDCenter.NewRow();
				drEach[0] = "";
				drEach[1] = "";
				drEach[2] = "";
				drEach[3] = "";
				drEach[4] = "";
				drEach[5] = "";
				drEach[6] = "";
				drEach[7] = "";
				drEach[8] = "";
				dtDCenter.Rows.Add(drEach);
			}
			SessionDS sdsDCenter = new SessionDS();
			DataSet dsDCenter = new DataSet();
			dsDCenter.Tables.Add(dtDCenter);

			//			dsDCenter.Tables[0].Columns["industrial_sector_code"].Unique = true; //Checks duplicate records..
			dsDCenter.Tables[0].Columns["origin_code"].AllowDBNull = false;

			sdsDCenter.ds = dsDCenter;

			sdsDCenter.DataSetRecSize = dsDCenter.Tables[0].Rows.Count;
			sdsDCenter.QueryResultMaxSize = sdsDCenter.DataSetRecSize;

			return  sdsDCenter;
	
		}

		public static void AddNewRowInDCenterDS(ref SessionDS dsDCenterGrid)
		{
			DataRow drNew = dsDCenterGrid.ds.Tables[0].NewRow();

			drNew[0] = "";
			drNew[1] = "";
			drNew[2] = "";
			drNew[3] = "";
			drNew[4] = "";
			drNew[5] = "";
			drNew[6] = "";
			drNew[7] = "";
			drNew[8] = "";
			try
			{
				dsDCenterGrid.ds.Tables[0].Rows.Add(drNew);
			}
			catch(Exception ex)
			{
				Logger.LogTraceError("SysDataMgrDAL1","AddNewRowInDCenterDS","R005",ex.Message.ToString());
			}
		}
		/// <summary>
		/// Querying for Distribution Centers
		/// </summary>
		/// <param name="sdsDCenter">SessionDS to be pass in that contains data for querying</param>
		/// <param name="strAppID">ApplicationID</param>
		/// <param name="strEnterpriseID">EnterprsieID</param>
		/// <param name="recStart">Start of Record</param>
		/// <param name="recSize">Size of Record</param>
		/// <returns></returns>
		public static SessionDS QueryDCenter(SessionDS sdsDCenter,String strAppID, String strEnterpriseID, int recStart, int recSize)
		{
			SessionDS sdsBaseTables = null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
		
			DataRow dr = sdsDCenter.ds.Tables[0].Rows[0];
			string strCode = (string)dr[0];
			string strDesc = (string)dr[1];
			string strHub = (string)dr[2];
			string strManifest = (string)dr[3];
			string strEmailAddress = (string)dr[4];
			string strZipcode = (string)dr[5];
			string strOperatedBy = (string)dr[6];
			string strTurnaroundTime = (string)dr[7];
			string strNearestAirport = (string)dr[8];
			
			string strSQLQuery = "select origin_code , origin_name , hub, isnull(Manifest,'') Manifest, ";
			strSQLQuery += "isnull(EmailAddress,'') EmailAddress, ";
			strSQLQuery += "isnull(Zipcode,'') Zipcode, isnull(OperatedBy,'') OperatedBy, ";
			strSQLQuery += "convert(varchar(5), [TurnaroundTime], 108) TurnaroundTime, isnull(NearestAirport,'') NearestAirport ";
			strSQLQuery += "from distribution_center where ";			
			strSQLQuery += "applicationid = '"+strAppID+"' and enterpriseid = '"+strEnterpriseID+"' "; 
			if(strCode!=null && strCode!="")
			{
				strSQLQuery += "and origin_code like '%"+Utility.ReplaceSingleQuote(strCode)+"%' "; 
			}
			if(strDesc!=null && strDesc!="")
			{
				strSQLQuery += "and origin_name like N'%"+Utility.ReplaceSingleQuote(strDesc)+"%' "; 
			}
			if(strHub!=null && strHub!="")
			{
				strSQLQuery += "and hub like N'%"+Utility.ReplaceSingleQuote(strHub)+"%' "; 
			}
			if(strManifest!=null && strManifest!="")
			{
				strSQLQuery += "and Manifest like N'%"+Utility.ReplaceSingleQuote(strManifest)+"%' "; 
			}
			if(strEmailAddress!=null && strEmailAddress!="")
			{
				strSQLQuery += "and EmailAddress like N'%"+Utility.ReplaceSingleQuote(strEmailAddress)+"%' "; 
			}
			if(strZipcode !=null && strZipcode!="")
			{
				strSQLQuery += "and Zipcode like N'%"+Utility.ReplaceSingleQuote(strZipcode)+"%' "; 
			}
			
			if(strTurnaroundTime!=null && strTurnaroundTime!="")
			{
				strSQLQuery += "and TurnaroundTime like N'%"+Utility.ReplaceSingleQuote(strTurnaroundTime)+"%' "; 
			}
			if(strNearestAirport!=null && strNearestAirport!="")
			{
				strSQLQuery += "and NearestAirport like N'%"+Utility.ReplaceSingleQuote(strNearestAirport)+"%' "; 
			}
			if(strOperatedBy!=null && strOperatedBy!="")
			{
				if(strOperatedBy != "All")
				{
					strSQLQuery += "and OperatedBy like N'%"+Utility.ReplaceSingleQuote(strOperatedBy)+"%' ";
				}
				else
				{
					strSQLQuery += " order by OperatedBy";
				}
			}

			//dsBaseTables = (DataSet)dbCon.ExecuteQuery(strSQLQuery,ReturnType.DataSetType);
			sdsBaseTables = (SessionDS)dbCon.ExecuteQuery(strSQLQuery,recStart, recSize,"DCenter");

			return  sdsBaseTables;
		}

		public static SessionDS QueryDCenternonQ(SessionDS sdsDCenter,String strAppID, String strEnterpriseID, int recStart, int recSize)
		{
			SessionDS sdsBaseTables = null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
		
			DataRow dr = sdsDCenter.ds.Tables[0].Rows[0];
			String strCode = "";
			String strDesc = "";
			String strHub = "";
			String strManifest = "";
			String strEmailAddress = "";
			

			String strSQLQuery = "select origin_code , origin_name , hub ,EmailAddress from distribution_center where ";
			
			strSQLQuery += "applicationid = '"+strAppID+"' and enterpriseid = '"+strEnterpriseID+"' "; 
			if(strCode!=null && strCode!="")
			{
				strSQLQuery += "and origin_code like '"+Utility.ReplaceSingleQuote(strCode)+"' "; 
			}
			if(strDesc!=null && strDesc!="")
			{
				strSQLQuery += "and origin_name like N'"+Utility.ReplaceSingleQuote(strDesc)+"' "; 
			}
			if(strHub!=null && strHub!="")
			{
				strSQLQuery += "and hub like N'"+Utility.ReplaceSingleQuote(strHub)+"' "; 
			}
			if(strManifest!=null && strManifest!="")
			{
				strSQLQuery += "and Manifest like N'"+Utility.ReplaceSingleQuote(strManifest)+"' "; 
			}
			if(strEmailAddress!=null && strEmailAddress!="")
			{
				strSQLQuery += "and EmailAddress like N'"+Utility.ReplaceSingleQuote(strEmailAddress)+"' "; 
			}
			//dsBaseTables = (DataSet)dbCon.ExecuteQuery(strSQLQuery,ReturnType.DataSetType);
			sdsBaseTables = (SessionDS)dbCon.ExecuteQuery(strSQLQuery,recStart, recSize,"DCenter");

			return  sdsBaseTables;
		}


		/// <summary>
		/// to update new rows into Distribution Centers database
		/// </summary>
		/// <param name="sdsBaseTables">SessionDS taht contain data to be inserted to database</param>
		/// <param name="rowIndex">row to be updated to database</param>
		/// <param name="strAppID">applicationid</param>
		/// <param name="strEnterpriseID">enterpriseid</param>
		/// <returns></returns>
		public static bool UpdateDCenter(SessionDS sdsBaseTables,int rowIndex, String strAppID, String strEnterpriseID)
		{
			IDbCommand dbCom = null;
			DataRow dr = sdsBaseTables.ds.Tables[0].Rows[rowIndex];
			string strCode = (string)dr[0];
			string strName = (string)dr[1];
			string strHub = (string)dr[2];
			string strManifest = (string)dr[3];
			string strEmailAddress = (string)dr[4];
			string strZipcode = (string)dr[5];
			string strOperatedBy = (string)dr[6];
			string strTurnaroundTime  = (string)dr[7];
			string strNearestAirport = (string)dr[8];

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(strCode!=null && strCode!="")
			{
				string strSQLQuery = "update Distribution_Center set origin_code = '"+strCode+"'";
				strSQLQuery += ", origin_name = N'"+Utility.ReplaceSingleQuote(strName)+"'";
				strSQLQuery += ", hub ='"+strHub+"', Manifest ='"+strManifest+"' ";
				strSQLQuery += ",EmailAddress ='"+strEmailAddress+"' ";
				strSQLQuery += ",Zipcode = N'"+Utility.ReplaceSingleQuote(strZipcode)+"'";
				strSQLQuery += ",OperatedBy ='"+strOperatedBy+"' ";
				strSQLQuery += ",TurnaroundTime ='"+strTurnaroundTime+"' ";
				strSQLQuery += ",NearestAirport =N'"+Utility.ReplaceSingleQuote(strNearestAirport)+"' ";

				strSQLQuery += " where applicationid = '"+strAppID+"' and enterpriseid = '"+strEnterpriseID+"' and origin_code = '"+Utility.ReplaceSingleQuote(strCode)+"' "; 
	
				dbCom = dbCon.CreateCommand(strSQLQuery,CommandType.Text);
				if(dbCon.ExecuteNonQuery(dbCom)>0)
					return true;
			}
			return false;
		}

		public static bool ChkManifest(String strCode,String strAppID, String strEnterpriseID)
		{
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			IDbCommand dbCom = null;

			String strSQLQuery = "select origin_code , origin_name , hub, isnull(Manifest,'') Manifest , isnull(EmailAddress,'') EmailAddress from distribution_center where ";
			
			strSQLQuery += "applicationid = '"+strAppID+"' and enterpriseid = '"+strEnterpriseID+"' and Manifest = 'Yes'"; 
			if(strCode!=null && strCode!="")
			{
				strSQLQuery += "and origin_code like '%"+Utility.ReplaceSingleQuote(strCode)+"%' "; 
			}
	
			dbCom = dbCon.CreateCommand(strSQLQuery,CommandType.Text);
			if(dbCon.ExecuteNonQuery(dbCom)>0)
				return false;
			else
				return true;
		}
		/// <summary>
		/// To insert a new record to database
		/// </summary>
		/// <param name="sdsBaseTables">SesionDs that contain data for inserting</param>
		/// <param name="strAppID">applicationid</param>
		/// <param name="strEnterpriseID">enterprsieid</param>
		/// <returns></returns>
		public static bool InsertDCenter(SessionDS sdsBaseTables, String strAppID, String strEnterpriseID)
		{
			IDbCommand dbCom = null;
			DataRow dr = sdsBaseTables.ds.Tables[0].Rows[0];
			string strCode = (string)dr[0];
			string strName = (string)dr[1];
			string strHub = (string)dr[2];
			string strManifest = (string)dr[3];
			string strEmailAddress = (string)dr[4];
			string strZipcode = (string)dr[5];
			string strOperatedBy = (string)dr[6];
			string strTurnaroundTime  = (string)dr[7];
			string strNearestAirport = (string)dr[8];

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
				
			if(strCode!=null && strCode!="")
			{
				string strSQLQuery = "insert into Distribution_Center(applicationid,enterpriseid,origin_code,";
				strSQLQuery += "origin_name,hub,Manifest,EmailAddress,Zipcode,OperatedBy,TurnaroundTime,NearestAirport) ";
				strSQLQuery += " values('"+strAppID+"','"+strEnterpriseID+"', '"+Utility.ReplaceSingleQuote(strCode)+"'";
				strSQLQuery += ",N'"+Utility.ReplaceSingleQuote(strName)+"','"+Utility.ReplaceSingleQuote(strHub)+"'";
				strSQLQuery += ",'"+Utility.ReplaceSingleQuote(strManifest)+"','"+Utility.ReplaceSingleQuote(strEmailAddress)+"'";
				strSQLQuery += ",N'"+Utility.ReplaceSingleQuote(strZipcode)+"','"+strOperatedBy+"'";
				strSQLQuery += ",'"+strTurnaroundTime+"','"+Utility.ReplaceSingleQuote(strNearestAirport)+"'";
				strSQLQuery += ")";

			
				dbCom = dbCon.CreateCommand(strSQLQuery,CommandType.Text);
				if(dbCon.ExecuteNonQuery(dbCom)>0)
					return true;
			}
			return false;
		}
		/// <summary>
		/// To delete record
		/// </summary>
		/// <param name="strAppID">applicationid</param>
		/// <param name="strEnterpriseID">enterpriseid</param>
		/// <param name="strCode">record to be delted base on this code</param>
		/// <returns></returns>
		public static bool DeleteDCenter(String strAppID, String strEnterpriseID, String strCode)
		{
			IDbCommand dbCom = null;
			String strSQLQuery = null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			
			strSQLQuery = "delete from Distribution_Center where applicationid = ";
			strSQLQuery += "'"+strAppID+"' and enterpriseid ='"+strEnterpriseID+"'";
			strSQLQuery += " and origin_code = '"+strCode+"'";
			try
			{
				dbCom = dbCon.CreateCommand(strSQLQuery,CommandType.Text);
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("SysDataMgr1","DeleteDCenter","SDM001","Error During Delete "+ appException.Message.ToString());
				throw new ApplicationException("Error deleting Distribution Center : "+appException.Message,appException);
			}

			if(dbCon.ExecuteNonQuery(dbCom)>0)
			{
				return true;
			}
			return false;
		}
		



		#endregion
		//---------COMMODITY-------
		/// <summary>
		/// Gets an empty SessionDS
		/// </summary>
		/// <param name="iNumRows">number of empty rows to return</param>
		/// <returns></returns>
		public static SessionDS GetEmptyCommodity(int iNumRows)
		{
			DataTable dtCommodity = new DataTable();
 
			dtCommodity.Columns.Add(new DataColumn("commodity_code", typeof(string)));
			dtCommodity.Columns.Add(new DataColumn("commodity_type", typeof(string)));
			dtCommodity.Columns.Add(new DataColumn("commodity_description", typeof(string)));
			for(int i=0; i < iNumRows; i++)
			{
				DataRow drEach = dtCommodity.NewRow();
				drEach[0] = "";
				drEach[1] = "";
				drEach[2] = "";

				dtCommodity.Rows.Add(drEach);
			}
			DataSet dsCommodity = new DataSet();
			SessionDS sessionds = new SessionDS();
			dsCommodity.Tables.Add(dtCommodity);

//			dsCommodity.Tables[0].Columns["commodity_code"].Unique = true; //Checks duplicate records..
			dsCommodity.Tables[0].Columns["commodity_code"].AllowDBNull = false;

			sessionds.ds = dsCommodity;

			sessionds.DataSetRecSize = dsCommodity.Tables[0].Rows.Count;
			sessionds.QueryResultMaxSize = sessionds.DataSetRecSize;
			return  sessionds;
	
		}

		public static void AddNewRowInCommodityDS(ref SessionDS dsCommodityGrid)
		{
//			m_sdsCommodity = (SessionDS)Session["SESSION_DS1"];
			DataRow drNew = dsCommodityGrid.ds.Tables[0].NewRow();

			drNew[0] = "";
			drNew[1] = "";
			try
			{
				dsCommodityGrid.ds.Tables[0].Rows.Add(drNew);
			}
			catch(Exception ex)
			{
				Logger.LogTraceError("SysDataMgrDAL","AddNewRowInCommodityDS","R005",ex.Message.ToString());
			}
			
//			dsCommodityGrid.ds.Tables[0].Rows.Add(drNew);
			
		}

		/// <summary>
		/// To query for comodity
		/// </summary>
		/// <param name="sdsCommodity">Sessionds that contain data for querying</param>
		/// <param name="strAppID">applicationid</param>
		/// <param name="strEnterpriseID">enterpriseid</param>
		/// <param name="recStart">Start of record</param>
		/// <param name="recSize">Size of record</param>
		/// <returns></returns>
		public static SessionDS QueryCommodity(SessionDS sdsCommodity, String strAppID, String strEnterpriseID, int recStart, int recSize)
		{
			
			SessionDS sdsBaseTables = null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
		
			DataRow dr = sdsCommodity.ds.Tables[0].Rows[0];
			String strCode = (String)dr[0];
			String strType = (String)dr[1];
			String strDesc = (String)dr[2];

			String strSQLQuery = "select commodity_code,commodity_type, commodity_description from Commodity where ";
			
			strSQLQuery += "applicationid = '"+strAppID+"' and enterpriseid = '"+strEnterpriseID+"' "; 
			if(strCode!=null && strCode!="")
			{
				strSQLQuery += "and commodity_code like '%"+Utility.ReplaceSingleQuote(strCode)+"%' "; 
			}
			if(strType!=null && strType!="")
			{
				strSQLQuery += "and commodity_type like '%"+strType+"%' "; 
			}
			if(strDesc!=null && strDesc!="")
			{
				strSQLQuery += "and commodity_description like N'%"+Utility.ReplaceSingleQuote(strDesc)+"%' "; 
			}
			//sdsBaseTables = (SessionDS)dbCon.ExecuteQuery(strSQLQuery,ReturnType.DataSetType);
			sdsBaseTables = (SessionDS)dbCon.ExecuteQuery(strSQLQuery,recStart, recSize,"Commodity");

			return  sdsBaseTables;
		}
		/// <summary>
		/// To insert a new record to database
		/// </summary>
		/// <param name="sdsBaseTables">SessionDS taht contain data for inserting to database</param>
		/// <param name="strAppID">applicationid</param>
		/// <param name="strEnterpriseID">enterpriseid</param>
		/// <returns></returns>
		public static bool InsertCommodity(SessionDS sdsBaseTables, String strAppID, String strEnterpriseID)
		{
			IDbCommand dbCom = null;
			DataRow dr = sdsBaseTables.ds.Tables[0].Rows[0];
			String strCode = (String)dr[0];
			String strType = (String)dr[1];
			String strDescription = (String)dr[2];

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
				
			if(strCode!=null && strCode!="")
			{
				String strSQLQuery = "insert into Commodity(applicationid,enterpriseid,commodity_code,commodity_type, commodity_description) ";
				strSQLQuery += " values('"+strAppID+"','"+strEnterpriseID+"','"+Utility.ReplaceSingleQuote(strCode)+"','"+strType+"',N'"+Utility.ReplaceSingleQuote(strDescription)+"') "; 
			
				dbCom = dbCon.CreateCommand(strSQLQuery,CommandType.Text);
				if(dbCon.ExecuteNonQuery(dbCom)>0)
					return true;
			}
			return false;
		}
		/// <summary>
		/// To update records in commodity database
		/// </summary>
		/// <param name="sdsBaseTables">SessionDS taht contains data for updating</param>
		/// <param name="strAppID">applicationid</param>
		/// <param name="strEnterpriseID">enterprsieid</param>
		/// <returns></returns>
		public static bool UpdateCommodity(SessionDS sdsBaseTables, String strAppID, String strEnterpriseID)
		{
			IDbCommand dbCom = null;
			DataRow dr = sdsBaseTables.ds.Tables[0].Rows[0];
			String strCode = (String)dr[0];
			String strType = (String)dr[1];
			String strDescription = (String)dr[2];

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(strCode!=null && strCode!="")
			{
				String strSQLQuery = "update Commodity set commodity_type ='"+strType+"', commodity_description = N'"+Utility.ReplaceSingleQuote(strDescription)+"' where ";
				strSQLQuery += "applicationid = "+"'"+strAppID+"' and enterpriseid = '"+strEnterpriseID+"' and commodity_code = '"+Utility.ReplaceSingleQuote(strCode)+"' "; 
	
				dbCom = dbCon.CreateCommand(strSQLQuery,CommandType.Text);
				if(dbCon.ExecuteNonQuery(dbCom)>0)
					return true;
			}
			return false;
		}
		/// <summary>
		/// to delele record in commodity
		/// </summary>
		/// <param name="strAppID">applicationid</param>
		/// <param name="strEnterpriseID">entrpriseid</param>
		/// <param name="strCode">code</param>
		/// <returns></returns>
		public static bool DeleteCommodity(String strAppID, String strEnterpriseID, String strCode)
		{
			IDbCommand dbCom = null;
			String strSQLQuery = null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			
			strSQLQuery = "delete from Commodity where applicationid = ";
			strSQLQuery += "'"+strAppID+"' and enterpriseid = '"+strEnterpriseID+"'";
			strSQLQuery += " and commodity_code = '"+strCode+"'";
			try
			{
				dbCom = dbCon.CreateCommand(strSQLQuery,CommandType.Text);
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("SysDataMgr1","DeleteCommodity","SDM001","Error During Delete "+ appException.Message.ToString());
				throw new ApplicationException("Error deleting Commodity: "+appException.Message,appException);
			}

			if(dbCon.ExecuteNonQuery(dbCom)>0)
			{
				return true;
			}
			return false;
		}
		
		//-----------Band Discount-------------------
		/// <summary>
		/// To get an empty BandDisocunt dataset
		/// </summary>
		/// <param name="iNumRows">number of rows to be return empty</param>
		/// <returns></returns>
		public static SessionDS GetEmptyBandDiscount(int iNumRows)
		{
			DataTable dtBandDiscount = new DataTable();
 
			dtBandDiscount.Columns.Add(new DataColumn("band_code", typeof(string)));
			dtBandDiscount.Columns.Add(new DataColumn("band_description", typeof(string)));
			dtBandDiscount.Columns.Add(new DataColumn("percent_discount", typeof(decimal)));
			for(int i=0; i < iNumRows; i++)
			{
				DataRow drEach = dtBandDiscount.NewRow();
				drEach[0] = "";
				drEach[1] = "";
				drEach[2] = System.DBNull.Value;

				dtBandDiscount.Rows.Add(drEach);
			}
			DataSet dsBandDiscount = new DataSet();
			SessionDS sessionds = new SessionDS();
			dsBandDiscount.Tables.Add(dtBandDiscount);

//			dsBandDiscount.Tables[0].Columns["band_code"].Unique = true; //Checks duplicate records..
			dsBandDiscount.Tables[0].Columns["band_code"].AllowDBNull = false;

			sessionds.ds = dsBandDiscount;

			sessionds.DataSetRecSize = dsBandDiscount.Tables[0].Rows.Count;
			sessionds.QueryResultMaxSize = sessionds.DataSetRecSize;

			return  sessionds;
		}

		public static void AddNewRowInBandDiscountDS(ref SessionDS dsBandDiscountGrid)
		{
			DataRow drNew = dsBandDiscountGrid.ds.Tables[0].NewRow();

			drNew[0] = "";
			drNew[1] = "";
			drNew[2] = System.DBNull.Value;
			
			try
			{
				dsBandDiscountGrid.ds.Tables[0].Rows.Add(drNew);
			}
			catch(Exception ex)
			{
				Logger.LogTraceError("SysDataMgrDAL","DeleteRouteCodeDS","R005",ex.Message.ToString());
			}
			
		}

		/// <summary>
		/// to query from Band Discount
		/// </summary>
		/// <param name="sdsBandDiscount">Sessionds that contain data for querying</param>
		/// <param name="strAppID">applicationid</param>
		/// <param name="strEnterpriseID">enterprsieid</param>
		/// <param name="recStart">start of record</param>
		/// <param name="recSize">Size of record</param>
		/// <returns></returns>
		public static SessionDS QueryBandDiscount(SessionDS sdsBandDiscount,String strAppID, String strEnterpriseID, int recStart, int recSize)
		{
			SessionDS sdsBaseTables = null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
		
			DataRow dr = sdsBandDiscount.ds.Tables[0].Rows[0];

			String strCode = (String)dr[0];
			String strDesc = (String)dr[1];
			String strDiscount = null;
			if(!dr[2].GetType().Equals(System.Type.GetType("System.DBNull")) && dr[2]!=null)
			{
				strDiscount = dr[2].ToString();
			}

			String strSQLQuery = "select band_code, band_description, percent_discount from Band_Discount where ";
			
			strSQLQuery += "applicationid = '"+strAppID+"' and enterpriseid = '"+strEnterpriseID+"' "; 
			if(strCode!=null && strCode!="")
			{
				strSQLQuery += "and band_code like '"+Utility.ReplaceSingleQuote(strCode)+"' "; 
			}
			if(strDesc!=null && strDesc!="")
			{
				strSQLQuery += " and band_description like N'"+Utility.ReplaceSingleQuote(strDesc)+"' "; 
			}
			if(strDiscount!=null && strDiscount!="")
			{
				strSQLQuery += "and percent_discount like '"+strDiscount+"' "; 
			}
			try
			{
				sdsBaseTables = (SessionDS)dbCon.ExecuteQuery(strSQLQuery,recStart, recSize,"BandDiscount");
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("SysDataMgr1","QueryBandDiscount","SDM001","Error During Querying Band Discount "+ appException.Message.ToString());
				throw new ApplicationException("Error Query BandDiscount: "+appException.Message,appException);
			}
			return  sdsBaseTables;
		}
		/// <summary>
		/// To update BandDiscount
		/// </summary>
		/// <param name="sdsBaseTables">Sessionds that contain data for updating to database</param>
		/// <param name="strAppID">applicationid</param>
		/// <param name="strEnterpriseID">enterpriseid</param>
		/// <returns></returns>
		public static int UpdateBandDiscount(SessionDS sdsBaseTables, String strAppID, String strEnterpriseID)
		{
			IDbCommand dbCom = null;
			DataRow dr = sdsBaseTables.ds.Tables[0].Rows[0];
			String strCode = (String)dr[0];
			String strDescription = (String)dr[1];
			int iRowsAffected=0;

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			
			String strSQLQuery = "update Band_Discount set band_description = N'"+Utility.ReplaceSingleQuote(strDescription)+"' ";
			if(Utility.IsNotDBNull(dr[2]) && dr[2].ToString() !="")
			{
				decimal decDiscount = (decimal)dr[2];
				strSQLQuery += ", percent_discount = "+decDiscount;
			}
			else
			{
				strSQLQuery += ", percent_discount = null";
			}
			strSQLQuery += " where applicationid = '"+strAppID+"' and enterpriseid = '"+strEnterpriseID+"' and band_code = '"+strCode+"' "; 
			
			try
			{
				dbCom = dbCon.CreateCommand(strSQLQuery,CommandType.Text);
				iRowsAffected=dbCon.ExecuteNonQuery(dbCom);	
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("SysDataMgr1","UpdateBandDiscount","SDM001","Error During Updating Band Discount "+ appException.Message.ToString());
				throw new ApplicationException("Error Updating BandDiscount: "+appException.Message,appException);
			}
			return iRowsAffected;		
		}

		/// <summary>
		/// To insert new record to database
		/// </summary>
		/// <param name="sdsBaseTables">SessionDS that contain data for inserting to Database</param>
		/// <param name="strAppID"></param>
		/// <param name="strEnterpriseID"></param>
		/// <returns></returns>
		public static int InsertBandDiscount(SessionDS sdsBaseTables, String strAppID, String strEnterpriseID)
		{
			IDbCommand dbCom = null;
			DataRow dr = sdsBaseTables.ds.Tables[0].Rows[0];
			String strCode = (String)dr[0];
			String strDescription = (String)dr[1];
			String strDiscount = dr[2].ToString();
			int iRowsAffected=0;
				
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
							
			String strSQLQuery = "insert into Band_Discount(applicationid,enterpriseid,band_code, band_description,percent_discount) ";
			strSQLQuery += " values('"+strAppID+"','"+strEnterpriseID+"','"+strCode+"',N'";
			strSQLQuery += Utility.ReplaceSingleQuote(strDescription)+"',"; 

			if(strDiscount!="")
				strSQLQuery += strDiscount+")";
			else
				strSQLQuery+="null)";			

			dbCom = dbCon.CreateCommand(strSQLQuery,CommandType.Text);
			try
			{
				iRowsAffected=dbCon.ExecuteNonQuery(dbCom);
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("SysDataMgr1","InsertBandDiscount","SDM001","Error During Insert Band Discount "+ appException.Message.ToString());
				throw new ApplicationException("Error Inserting BandDiscount: "+appException.Message,appException);
			}
			return iRowsAffected;
		}

		/// <summary>
		/// To delele band discount
		/// </summary>
		/// <param name="strAppID">aplicationid</param>
		/// <param name="strEnterpriseID">enterprsieid</param>
		/// <param name="strCode">code</param>
		/// <returns></returns>
		public static int DeleteBandDiscount(String strAppID, String strEnterpriseID, String strCode)
		{
			IDbCommand dbCom = null;
			String strSQLQuery = null;
			int iRowsAffected=0;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			
			strSQLQuery = "delete from Band_Discount where applicationid = ";
			strSQLQuery += "'"+strAppID+"'"+" and enterpriseid = '"+strEnterpriseID+"'";
			strSQLQuery += " and band_code = '"+strCode+"'";
			try
			{
				dbCom = dbCon.CreateCommand(strSQLQuery,CommandType.Text);
				iRowsAffected=dbCon.ExecuteNonQuery(dbCom);
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("SysDataMgr1","DeleteBandDiscount","SDM001","Error During Delete "+ appException.Message.ToString());
				throw new ApplicationException("Error deleting BandDiscount: "+appException.Message,appException);
			}
			
			return iRowsAffected;
		}

		//----------ENTERPRISE PROFILE-------------------
		/// <summary>
		/// to update to Enterprise
		/// </summary>
		/// <param name="dsEnterprise">Dataset that contains data that for updating</param>
		/// <param name="strAppID">Applicationid</param>
		/// <param name="strEnterpriseID">enterpriseid</param>
		public static void UpdateEnterpriseProfile(DataSet dsEnterprise, String strAppID, String strEnterpriseID)
		{
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			IDbCommand dbCom = null;
			DataRow drEPrise = dsEnterprise.Tables[0].Rows[0];
			
			String strEnterpriseName = (String)drEPrise["enterprise_name"];
			String strContactPerson = (String)drEPrise["contact_person"];
			String strTelephone = (String)drEPrise["contact_telephone"];
			String strAddress1 = (String)drEPrise["address1"];
			String strAddress2 = (String)drEPrise["address2"];
			String strCountry = (String)drEPrise["country"];
			String strWTRounding = "";
			String strCurrency = (String)drEPrise["currency"];
			String strCurrencyDec = (String)drEPrise["currency_decimal"];
			if(drEPrise["wt_rounding_method"] != System.DBNull.Value)
			{
				strWTRounding = drEPrise["wt_rounding_method"].ToString();
			}
			String strZipCode = (String)drEPrise["zipcode"];
			String strWTInc = "";
			if(drEPrise["wt_increment_amt"] != System.DBNull.Value)
			{
				strWTInc = drEPrise["wt_increment_amt"].ToString();
			}
			String strFreeInsurance = "";
			if(drEPrise["free_insurance_amt"] != System.DBNull.Value)
			{
				strFreeInsurance = drEPrise["free_insurance_amt"].ToString();
			}
			String strMAXInsurance = "";
			if(drEPrise["max_insurance_amt"] != System.DBNull.Value)
			{
				strMAXInsurance = drEPrise["max_insurance_amt"].ToString();
			}
			String strInsuranceSurCharge = "";
			if(drEPrise["insurance_percent_surcharge"] != System.DBNull.Value)
			{
				strInsuranceSurCharge = drEPrise["insurance_percent_surcharge"].ToString();
			
			}
			String strDensityFact = (String)drEPrise["density_factor"];
			String strFax = (String)drEPrise["fax"];
			String strEmail = (String)drEPrise["email"];
			String strApplyTax = "";
			if(drEPrise["apply_tax"] != System.DBNull.Value)
			{
				strApplyTax = drEPrise["apply_tax"].ToString();
			}
			String strTaxPercent = "";
			if(drEPrise["tax_percent"] != System.DBNull.Value)
			{
				strTaxPercent = drEPrise["tax_percent"].ToString();
			}
			String strTaxID = (String)drEPrise["tax_id"];
			//drEPrise[""] = txtState.Text;
			String strSatDelAvail = drEPrise["sat_delivery_avail"].ToString();
			String strSunDelAvail = drEPrise["sun_delivery_avail"].ToString();
			String strPubDelAvail = drEPrise["pub_delivery_avail"].ToString();
			String strSatDelCode = drEPrise["sat_delivery_vas_code"].ToString();
			String strSunDelCode = drEPrise["sun_delivery_vas_code"].ToString();
			String strPubDelCode = drEPrise["pub_delivery_vas_code"].ToString();

			//Add New 10:39 20/3/2551
			String strSatPickupAvail = drEPrise["sat_pickup_avail"].ToString();
			String strSunPickupAvail = drEPrise["sun_pickup_avail"].ToString();
			String strPubPickupAvail = drEPrise["pub_pickup_avail"].ToString();
			String strSatPickupCode = drEPrise["sat_pickup_vas_code"].ToString();
			String strSunPickupCode = drEPrise["sun_pickup_vas_code"].ToString();
			String strPubPickupCode = drEPrise["pub_pickup_vas_code"].ToString();
			//End Add New 10:39 20/3/2551

			//add by Tumz.
			String strWD_Ends = (String)drEPrise["WD_Ends"];
			String strSatWD_Ends = (String)drEPrise["SatWD_Ends"];
			String strSunWD_Ends = (String)drEPrise["SunWD_Ends"];
			String strPubWD_Ends = (String)drEPrise["PubWD_Ends"];

			String strQuery = "update Enterprise set enterprise_name = N'"+strEnterpriseName+"', ";
			strQuery += "contact_person = N'"+Utility.ReplaceSingleQuote(strContactPerson)+"',  contact_telephone = '"+Utility.ReplaceSingleQuote(strTelephone)+"', ";
			strQuery += "address1 = N'"+Utility.ReplaceSingleQuote(strAddress1)+"', address2 = N'"+Utility.ReplaceSingleQuote(strAddress2)+"', "; //To accept Unicode
			strQuery += "country = '"+Utility.ReplaceSingleQuote(strCountry)+"',zipcode = '"+Utility.ReplaceSingleQuote(strZipCode)+"', ";

			if(strWTRounding != "" && strWTRounding !=null)
			{
				strQuery += " wt_rounding_method = "+strWTRounding+", ";
			}
			else
			{
				strQuery += " wt_rounding_method = NULL, ";
			}
			if(strCurrency!="" && strCurrency!=null)
			{
				strQuery += " currency = '"+strCurrency+"', ";
			}
			else
			{
				strQuery += " currency = NULL, ";
			}
			if(strCurrencyDec!="" && strCurrencyDec!=null)
			{
				strQuery += " currency_decimal = "+strCurrencyDec+", ";
			}
			else
			{
				strQuery += " currency_decimal = NULL, ";
			}
			if(strWTInc!="" && strWTInc!=null)
			{
				strQuery +="wt_increment_amt = "+strWTInc+", ";
			}
			else
			{
				strQuery += " wt_increment_amt = NULL, ";
			}
			if(strFreeInsurance !="" && strFreeInsurance!=null)
			{
				strQuery += "free_insurance_amt = "+strFreeInsurance+", ";
			}
			else
			{
				strQuery += " free_insurance_amt =NULL, ";
			}
			if(strMAXInsurance !="" && strMAXInsurance!=null)
			{
				strQuery +="max_insurance_amt = "+strMAXInsurance+", ";
			}
			else
			{
				strQuery += " max_insurance_amt = NULL, ";
			}
			if(strInsuranceSurCharge!="" && strInsuranceSurCharge!=null)
			{
				strQuery += "insurance_percent_surcharge = "+strInsuranceSurCharge+", ";
			}
			else
			{
				strQuery += " insurance_percent_surcharge = NULL, ";
			}
			if(strDensityFact!="" && strDensityFact!=null)
			{
				strQuery +="density_factor = "+strDensityFact+", ";
			}
			else
			{
				strQuery += " density_factor = NULL, ";
			}
			if(strApplyTax!=""&&strApplyTax!=null)
			{
				strQuery += "apply_tax= '"+ strApplyTax.ToUpper() +"', ";
			}
			else
			{
				strQuery += " apply_tax = NULL, ";
			}
			if(strTaxPercent!=""&&strTaxPercent!=null)
			{
				strQuery +="tax_percent = "+strTaxPercent+", ";
			}
			else
			{
				strQuery += " tax_percent = NULL, ";
			}
			if(strFax!="" && strFax!=null)
			{
				strQuery += "fax = '"+Utility.ReplaceSingleQuote(strFax)+"', ";
			}
			else
			{
				strQuery += " fax = NULL, ";
			}
			if(strEmail!=""&&strEmail!=null)
			{
				strQuery += "email = '"+Utility.ReplaceSingleQuote(strEmail)+"', ";
			}
			else
			{
				strQuery += " email = NULL, ";
			}
			if(strTaxID!=""&&strTaxID!=null)
			{
				strQuery += "tax_id = '"+strTaxID+"', ";
			}
			else
			{
				strQuery += " tax_id = NULL, ";
			}

			strQuery += "sat_delivery_avail = '"+strSatDelAvail.ToUpper() +"', ";
			strQuery += "sun_delivery_avail = '"+strSunDelAvail.ToUpper() +"', ";
			strQuery += "pub_delivery_avail = '"+strPubDelAvail.ToUpper() +"', ";

			//Add New 10:39 20/3/2551
			strQuery += "sat_pickup_avail = '"+strSatPickupAvail.ToUpper() +"', ";
			strQuery += "sun_pickup_avail = '"+strSunPickupAvail.ToUpper() +"', ";
			strQuery += "pub_pickup_avail = '"+strPubPickupAvail.ToUpper() +"', ";
			//End Add New 10:39 20/3/2551

			if(strSatDelCode!=""&&strSatDelCode!=null)
			{
				strQuery += "sat_delivery_vas_code = '"+Utility.ReplaceSingleQuote(strSatDelCode)+"', ";
			}
			else
			{
				strQuery += " sat_delivery_vas_code = NULL, ";
			}
			if(strSunDelCode!=""&&strSunDelCode!=null)
			{
				strQuery += "sun_delivery_vas_code = '"+Utility.ReplaceSingleQuote(strSunDelCode)+"', ";
			}
			else
			{
				strQuery += " sun_delivery_vas_code = NULL, ";
			}
			if(strPubDelCode!=""&&strPubDelCode!=null)
			{
				strQuery += "pub_delivery_vas_code = '"+Utility.ReplaceSingleQuote(strPubDelCode)+"', ";
			}
			else
			{
				strQuery += " pub_delivery_vas_code = NULL, ";
			}

			//Add New 10:39 20/3/2551
			if(strSatPickupCode!=""&&strSatPickupCode!=null)
			{
				strQuery += "sat_pickup_vas_code = '"+Utility.ReplaceSingleQuote(strSatPickupCode)+"', ";
			}
			else
			{
				strQuery += " sat_pickup_vas_code = NULL, ";
			}
			if(strSunPickupCode!=""&&strSunPickupCode!=null)
			{
				strQuery += "sun_pickup_vas_code = '"+Utility.ReplaceSingleQuote(strSunPickupCode)+"', ";
			}
			else
			{
				strQuery += " sun_pickup_vas_code = NULL, ";
			}
			if(strPubPickupCode!=""&&strPubPickupCode!=null)
			{
				strQuery += "pub_pickup_vas_code = '"+Utility.ReplaceSingleQuote(strPubPickupCode)+"' ,";
			}
			else
			{
				strQuery += " pub_pickup_vas_code = NULL ,";
			}
			//End Add New 10:39 20/3/2551

			//add by Tumz.
			if(strWD_Ends!=""&&strWD_Ends!=null)
			{
				strQuery += "WD_Ends = '"+Utility.ReplaceSingleQuote(strWD_Ends)+"' ,";
			}
			else
			{
				strQuery += " WD_Ends = default ,";
			}
			if(strSatWD_Ends!=""&&strSatWD_Ends!=null)
			{
				strQuery += "SatWD_Ends = '"+Utility.ReplaceSingleQuote(strSatWD_Ends)+"' ,";
			}
			else
			{
				strQuery += " SatWD_Ends = default ,";
			}
			if(strSunWD_Ends!=""&&strSunWD_Ends!=null)
			{
				strQuery += "SunWD_Ends = '"+Utility.ReplaceSingleQuote(strSunWD_Ends)+"' ,";
			}
			else
			{
				strQuery += " SunWD_Ends = default ,";
			}
			if(strPubWD_Ends!=""&&strPubWD_Ends!=null)
			{
				strQuery += "PubWD_Ends = '"+Utility.ReplaceSingleQuote(strPubWD_Ends)+"' ";
			}
			else
			{
				strQuery += " PubWD_Ends = default ";
			}


			strQuery += "where applicationid='"+strAppID+"' and enterpriseid='"+strEnterpriseID+"'";

			dbCom = dbCon.CreateCommand(strQuery,CommandType.Text);
			dbCon.ExecuteNonQuery(dbCom);
				
		}
		
		public static bool GetVASD_byCode(string appID, string enterpriseID, string vasCode)
		{
			DataSet ds  = new DataSet();
			StringBuilder strQry = new StringBuilder();

			strQry.Append("select * from VAS a where a.applicationid='" + appID + "' and a.enterpriseid = '" + enterpriseID + "' and a.vas_code like '%" + vasCode + "%'");
			
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(appID,enterpriseID);
			IDbCommand dbCmd = dbCon.CreateCommand(strQry.ToString(),CommandType.Text);

			ds = (DataSet)dbCon.ExecuteQuery(dbCmd, ReturnType.DataSetType);
			if(ds.Tables[0].Rows.Count > 0)
				return  true;
			else
				return false;
		}

		/// <summary>
		/// To Retrieve current Profile out and be displayed to user
		/// </summary>
		/// <param name="strAppID">applicationid</param>
		/// <param name="strEnterpriseID">enterpriseid</param>
		/// <returns></returns>
		public static DataSet GetEnterpriseProfile(String strAppID, String strEnterpriseID)
		{
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
		
			
			String strQuery = "select enterpriseid, enterprise_name,contact_person,contact_telephone, ";
			strQuery += "address1,address2, country, wt_rounding_method, currency, currency_decimal, zipcode,wt_increment_amt, free_insurance_amt, ";
			strQuery +="max_insurance_amt, insurance_percent_surcharge, density_factor, sat_delivery_avail, sat_delivery_vas_code, ";
			strQuery += "sun_delivery_avail, sun_delivery_vas_code, pub_delivery_avail, pub_delivery_vas_code, ";

			//Add New 10:39 20/3/2551
			strQuery += "sat_pickup_avail, sat_pickup_vas_code, sun_pickup_avail, sun_pickup_vas_code, pub_pickup_avail, pub_pickup_vas_code,";
			//End Add New 10:39 20/3/2551

			strQuery +="fax, email, apply_tax, tax_percent, tax_id, Enterprise_Type, ";
			//add by Tumz.
			strQuery +="WD_Ends, SatWD_Ends, SunWD_Ends, PubWD_Ends ";

			strQuery +="from Enterprise where applicationid = '"+strAppID+"' and enterpriseid = '"+strEnterpriseID+"' ";
			
			SessionDS sdsBaseTables = (SessionDS)dbCon.ExecuteQuery(strQuery,0, 5,"EnterpriseProfile");

			return sdsBaseTables.ds;
		}

		//---TO BE ADDED TO TIESUtility------
		public static String ValidateZipCode(String strAppID, String strEnterpriseID, String strCountry, String strState)
		{
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			String strQuery = "select country from ZipCode where applicationid = '"+strAppID+"' and ";
			strQuery += "enterpriseid = '"+strEnterpriseID+"' and country = '"+strCountry+"' and state_code = '"+strState+"' ";

			SessionDS sdsBaseTables = (SessionDS)dbCon.ExecuteQuery(strQuery,0, 5,"ValidateZipcode");
			if(sdsBaseTables.DataSetRecSize>0)
			{
				DataRow dr = sdsBaseTables.ds.Tables[0].Rows[0];
				return dr["country"].ToString();
			}
			return "";
			
		}
		//-----Salesman----
		public static DataSet GetEmptySalesman()
		{
			DataTable dtSalesman = new DataTable();
			dtSalesman.Columns.Add(new DataColumn("salesmanid", typeof(string)));
			dtSalesman.Columns.Add(new DataColumn("salesman_name",typeof(string)));
			dtSalesman.Columns.Add(new DataColumn("salesman_ref",typeof(string)));
			dtSalesman.Columns.Add(new DataColumn("commission",typeof(string)));
			dtSalesman.Columns.Add(new DataColumn("telephone",typeof(string)));
			dtSalesman.Columns.Add(new DataColumn("mobile_phone",typeof(string)));
			dtSalesman.Columns.Add(new DataColumn("email",typeof(string)));
			dtSalesman.Columns.Add(new DataColumn("designation",typeof(string)));
			
			DataSet dsSalesman = new DataSet();
			dsSalesman.Tables.Add(dtSalesman);

//			dsSalesman.Tables[0].Columns["salesmanid"].Unique = true; //Checks duplicate records..
			dsSalesman.Tables[0].Columns["salesmanid"].AllowDBNull = false;

			return  dsSalesman;
		}

		public static void AddNewRowInSalesManDS(ref SessionDS dsSalesManGrid)
		{
			DataRow drNew = dsSalesManGrid.ds.Tables[0].NewRow();

			drNew[0] = "";
			drNew[1] = "";
			drNew[2] = "";
			drNew[3] = "";
			drNew[4] = "";
			drNew[5] = "";
			drNew[6] = "";
			drNew[7] = "";

			try
			{
				dsSalesManGrid.ds.Tables[0].Rows.Add(drNew);
			}
			catch(Exception ex)
			{
				Logger.LogTraceError("SysDataMgrDAL1","AddNewRowInSalesManDS","R005",ex.Message.ToString());
			}
		}

		/// <summary>
		/// To query for salesman records
		/// </summary>
		/// <param name="strAppID">application id</param>
		/// <param name="strEnterpriseID">enterprise id</param>
		/// <param name="recStart">start of record</param>
		/// <param name="recSize">size of record</param>
		/// <param name="sdsSalesman">SessionDS to be return</param>
		/// <returns></returns>
		public static SessionDS QuerySalesman(String strAppID, String strEnterpriseID,int recStart,int recSize,SessionDS sdsSalesman)
		{
			SessionDS sdsBaseTables = null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
		
			DataRow dr = sdsSalesman.ds.Tables[0].Rows[0];
			String strSalesmanID = dr["salesmanid"].ToString();
			String strSalesmanRef = dr["salesman_ref"].ToString();
			String strSalesmanName = dr["salesman_name"].ToString();
			String strCommission = dr["commission"].ToString();
			String strTelephone = dr["telephone"].ToString();
			String strMobile = dr["mobile_phone"].ToString();
			String strEmail = dr["email"].ToString();
			String strDesign = dr["designation"].ToString();

			String strSQLQuery = "select * from Salesman where ";
			
			strSQLQuery += "applicationid = '"+strAppID+"' and enterpriseid = '"+strEnterpriseID+"' "; 
			if(strSalesmanID!=null && strSalesmanID!="")
			{
				strSQLQuery += "and salesmanid like '"+strSalesmanID+"' "; 
			}
			if(strSalesmanRef!=null && strSalesmanRef!="")
			{
				strSQLQuery += "and salesman_ref like '"+Utility.ReplaceSingleQuote(strSalesmanRef)+"' "; 
			}
			if(strSalesmanName!=null && strSalesmanName!="")
			{
				strSQLQuery += "and salesman_name like N'"+Utility.ReplaceSingleQuote(strSalesmanName)+"' "; 
			}
			if(strCommission!=null && strCommission!="")
			{
				strSQLQuery += "and commission like '"+strCommission+"' "; 
			}
			if(strTelephone!=null && strTelephone!="")
			{
				strSQLQuery += "and telephone like '"+Utility.ReplaceSingleQuote(strTelephone)+"' "; 
			}
			if(strMobile!=null && strMobile!="")
			{
				strSQLQuery += "and mobile_phone like '"+Utility.ReplaceSingleQuote(strMobile)+"' "; 
			}
			if(strEmail!=null && strEmail!="")
			{
				strSQLQuery += "and email like '"+Utility.ReplaceSingleQuote(strEmail)+"' "; 
			}
			if(strDesign!=null && strDesign!="")
			{
				strSQLQuery += "and designation like N'"+Utility.ReplaceSingleQuote(strDesign)+"' "; 
			}
			
			//dsBaseTables = (DataSet)dbCon.ExecuteQuery(strSQLQuery,ReturnType.DataSetType);
			sdsBaseTables = (SessionDS)dbCon.ExecuteQuery(strSQLQuery,recStart, recSize,"Salesman");

			return  sdsBaseTables;
		}

		/// <summary>
		/// To insert a new salesman record
		/// </summary>
		/// <param name="strAppID">application id</param>
		/// <param name="strEnterpriseID">enterprise id</param>
		/// <param name="sdsSalesman">SessionDS that holds data for insertion</param>
		/// <returns></returns>
		public static bool InsertSalesman(String strAppID, String strEnterpriseID, SessionDS sdsSalesman)
		{
			IDbCommand dbCom = null;
			DataRow dr = sdsSalesman.ds.Tables[0].Rows[0];
			String strSalesmanID = dr["salesmanid"].ToString();
			String strSalesmanName = dr["salesman_name"].ToString();
			String strSalesmanRef = dr["salesman_ref"].ToString();
			String strCommission = dr["commission"].ToString();
			if(strCommission=="")
			{
				strCommission="null";
			}
			String strTelephone = dr["telephone"].ToString();
			String strMobile = dr["mobile_phone"].ToString();
			String strEmail = dr["email"].ToString();
			String strDesignation = dr["designation"].ToString();


			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
				
				String strSQLQuery = "insert into Salesman(applicationid,enterpriseid,salesmanid, salesman_name, ";
				strSQLQuery += " salesman_ref,commission,telephone,mobile_phone,email, designation) ";
				strSQLQuery += " values('"+strAppID+"','"+strEnterpriseID+"','"+strSalesmanID+"',N'"+Utility.ReplaceSingleQuote(strSalesmanName)+"','";
				strSQLQuery += Utility.ReplaceSingleQuote(strSalesmanRef)+"',"+strCommission+",'"+Utility.ReplaceSingleQuote(strTelephone)+"','";
				strSQLQuery += strMobile+"', '"+Utility.ReplaceSingleQuote(strEmail)+"',N'"+Utility.ReplaceSingleQuote(strDesignation)+"') "; 
			
				dbCom = dbCon.CreateCommand(strSQLQuery,CommandType.Text);
				if(dbCon.ExecuteNonQuery(dbCom)>0)
					return true;
			
			return false;
		}
		/// <summary>
		/// To update existing salesman record
		/// </summary>
		/// <param name="strAppID">application id</param>
		/// <param name="strEnterpriseID">enterprise id</param>
		/// <param name="sdsSalesman">SessionDS that holds updated data</param>
		/// <returns></returns>
		public static bool UpdateSalesman(String strAppID, String strEnterpriseID, SessionDS sdsSalesman)
		{
			DataRow dr = sdsSalesman.ds.Tables[0].Rows[0];
			String strSalesmanID = dr["salesmanid"].ToString();
			String strSalesmanName = dr["salesman_name"].ToString();
			String strSalesmanRef = dr["salesman_ref"].ToString();
			String strCommission = dr["commission"].ToString();
			if(strCommission=="")
			{
				strCommission="null";
			}
			String strTelephone = dr["telephone"].ToString();
			String strMobile = dr["mobile_phone"].ToString();
			String strEmail = dr["email"].ToString();
			String strDesignation = dr["designation"].ToString();

			IDbCommand dbCom = null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
		
			String strSQLQuery = "update Salesman set salesmanid = '"+strSalesmanID+"', salesman_ref = '";
			strSQLQuery+= Utility.ReplaceSingleQuote(strSalesmanRef)+"', salesman_name = N'"+Utility.ReplaceSingleQuote(strSalesmanName)+"',commission = "+strCommission+", ";
			strSQLQuery+= " telephone = '"+strTelephone+"',mobile_phone = '"+strMobile+"',email='"+strEmail;
			strSQLQuery+= "', designation=N'"+Utility.ReplaceSingleQuote(strDesignation)+"'	where ";

			strSQLQuery += "applicationid = '"+strAppID+"' and enterpriseid = '"+strEnterpriseID+"' and salesmanid = '"+strSalesmanID+"' "; 

			dbCom = dbCon.CreateCommand(strSQLQuery,CommandType.Text);
			if(dbCon.ExecuteNonQuery(dbCom)>0)
				return true;
		
			return false;
		}
		/// <summary>
		/// to Delete a single salesman record
		/// </summary>
		/// <param name="strAppID">application id</param>
		/// <param name="strEnterpriseID">enterprise id</param>
		/// <param name="strSalesmanID">sales man id for querying the record to be deleted</param>
		/// <returns></returns>
		public static bool DeleteSalesman(String strAppID, String strEnterpriseID, String strSalesmanID)
		{
			IDbCommand dbCom = null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
		
			String strSQLQuery = "delete from Salesman where ";
			strSQLQuery += "applicationid = '"+strAppID+"' and enterpriseid = '"+strEnterpriseID+"' and salesmanid = '"+strSalesmanID+"' "; 

			try
			{
				dbCom = dbCon.CreateCommand(strSQLQuery,CommandType.Text);
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("SysDataMgr1","DeleteSalesman","SDM001","Error During Delete "+ appException.Message.ToString());
				throw new ApplicationException("Error deleting Salesman: "+appException.Message,appException);
			}

			if(dbCon.ExecuteNonQuery(dbCom)>0)
			{
				return true;
			}
			return false;
		}



		// Conveyance Methods

		public static SessionDS GetEmptyConveyance()
		{
			DataTable dtConveyance =  new DataTable();
			
			dtConveyance.Columns.Add(new DataColumn("conveyance_code",typeof(string)));
			dtConveyance.Columns.Add(new DataColumn("conveyance_description",typeof(string)));
			dtConveyance.Columns.Add(new DataColumn("Length",typeof(decimal))); 
			dtConveyance.Columns.Add(new DataColumn("Breadth",typeof(decimal))); 
			dtConveyance.Columns.Add(new DataColumn("Height",typeof(decimal))); 
			dtConveyance.Columns.Add(new DataColumn("Max_Wt",typeof(decimal))); 

			DataSet  dsConv = new DataSet();
			dsConv.Tables.Add(dtConveyance); 
 
//			dsConv.Tables[0].Columns["conveyance_code"].Unique = true; //Checks duplicate records..
			dsConv.Tables[0].Columns["conveyance_code"].AllowDBNull = false;

			SessionDS sessionds = new SessionDS();
			sessionds.ds = dsConv;
				
			sessionds.QueryResultMaxSize = sessionds.DataSetRecSize;
			return  sessionds;
		}

		public static SessionDS GetEmptyConveyance(int iNumRows)
		{
			DataTable dtConveyance =  new DataTable();
			
			dtConveyance.Columns.Add(new DataColumn("conveyance_code",typeof(string)));
			dtConveyance.Columns.Add(new DataColumn("conveyance_description",typeof(string)));
			dtConveyance.Columns.Add(new DataColumn("Length",typeof(decimal))); 
			dtConveyance.Columns.Add(new DataColumn("Breadth",typeof(decimal))); 
			dtConveyance.Columns.Add(new DataColumn("Height",typeof(decimal))); 
			dtConveyance.Columns.Add(new DataColumn("Max_Wt",typeof(decimal))); 

			for(int i=0; i < iNumRows; i++)
			{
				DataRow drEach = dtConveyance.NewRow();
				drEach[0] = "";
				drEach[1] = "";
				drEach[2] = 0;
				drEach[3] = 0;
				drEach[4] = 0;
				drEach[5] = 0;

				dtConveyance.Rows.Add(drEach);
			}

			DataSet  dsConv = new DataSet();
			dsConv.Tables.Add(dtConveyance); 
 
			dsConv.Tables[0].Columns["conveyance_code"].Unique = true; //Checks duplicate records..
			dsConv.Tables[0].Columns["conveyance_code"].AllowDBNull = false;

			SessionDS sessionds = new SessionDS();
			sessionds.ds = dsConv;
				
			sessionds.QueryResultMaxSize = sessionds.DataSetRecSize;
			return  sessionds;
		}

		public static void AddNewRowInConveyanceDS(ref SessionDS dsConveyance)
		{
			DataRow drNew = dsConveyance.ds.Tables[0].NewRow();
			drNew[0] = "";
			drNew[1] = "";
			drNew[2] = 0;
			drNew[3] = 0;
			drNew[4] = 0;
			drNew[5] = 0;

			try
			{
				dsConveyance.ds.Tables[0].Rows.Add(drNew);
			}
			catch(Exception ex)
			{
				Logger.LogTraceError("SysDataMgrDAL","AddNewRowInConveyanceDS","R005",ex.Message.ToString());
			}			
		}

		public static SessionDS GetConveyanceDS(String strAppID, String strEnterpriseID, String strAgentID, int iCurRow, int iDSRowSize, DataSet dsQuery)
		{
			SessionDS sessionDS = null;
			DataRow dr = dsQuery.Tables[0].Rows[0];
			string strConveyance = (string)dr[0];
			string strConvDescrp =(string)dr[1];
			decimal dLength=Convert.ToDecimal(dr[2]);
			decimal dBreadth=Convert.ToDecimal(dr[3]);
			decimal dHeight=Convert.ToDecimal(dr[4]);
			decimal dWeight=Convert.ToDecimal(dr[5]);
			
			StringBuilder strSQLQuery = new StringBuilder();
			strSQLQuery.Append(" Select conveyance_code, conveyance_description, Length, Breadth, Height, Max_Wt from Agent_Conveyance where ");
			if (strAgentID !="")
				strSQLQuery.Append(" AgentID='"+strAgentID+"' and ");
			if (Utility.IsNotDBNull(strConveyance) && strConveyance !="")
				strSQLQuery.Append(" conveyance_code like '%"+strConveyance+"%' and ");
			if (Utility.IsNotDBNull(strConvDescrp) && strConvDescrp !="")
				strSQLQuery.Append(" conveyance_description like N'%"+Utility.ReplaceSingleQuote(strConvDescrp)+"%' and ");
			if (dLength.ToString() !="" && dLength > 0)
				strSQLQuery.Append(" Length="+ dLength+" and ");
			if (dBreadth.ToString() !="" && dBreadth > 0)
				strSQLQuery.Append(" Breadth="+ dBreadth+" and ");
			if (dHeight.ToString() !="" && dHeight > 0)
				strSQLQuery.Append(" Height="+ dHeight+" and ");
			if (dWeight.ToString() !="" && dWeight > 0 )
				strSQLQuery.Append(" Max_Wt="+ dWeight+" and ");
			
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("SysDataManager","GetCostCodeDS","EDB111","DbConnection object is null!!");
				return sessionDS;
			}
						
			strSQLQuery.Append(" applicationid = '"+strAppID+"' and enterpriseid = '"+strEnterpriseID+"'"); 						
			sessionDS = dbCon.ExecuteQuery(strSQLQuery.ToString(),iCurRow,iDSRowSize,"CostCodeTable");
			return  sessionDS;
		}

		public static int InsertConveyance(DataSet dsConveyance, int rowIndex, String strAppID, String strEnterpriseID, String strAgentID)
		{
			int iRowsInserted = 0;
			IDbCommand dbCom = null;
			DataRow dr = dsConveyance.Tables[0].Rows[rowIndex];
			string strConveyance = (string)dr[0];
			string strDescription=(string)dr[1];			
			decimal dLength=(decimal)dr[2];
			decimal dBreadth=(decimal)dr[3];
			decimal dHeight=(decimal)dr[4];
			decimal dMax_Wt=(decimal)dr[5];
			
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("SysDataMgrDAL","InsertConveyance","DP001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			StringBuilder strSQLQuery=new StringBuilder();

			try
			{	
				strSQLQuery.Append(" Insert into Agent_Conveyance (applicationid, enterpriseid, AgentID, Conveyance_code,Conveyance_description,");
				strSQLQuery.Append(" Length, Breadth, Height, Max_Wt) values ('"+strAppID+"', '"+strEnterpriseID+"', '"+strAgentID+"','"+ strConveyance+"'");
				
				if (Utility.IsNotDBNull(strDescription) && strDescription !="")
				strSQLQuery.Append(",N'"+Utility.ReplaceSingleQuote(strDescription)+"',");
				else
				{
				strSQLQuery.Append(",null,");
				}

				strSQLQuery.Append(dLength+","+dBreadth+","+dHeight+","+dMax_Wt+")");
				dbCom = dbCon.CreateCommand(strSQLQuery.ToString(),CommandType.Text);
				iRowsInserted = dbCon.ExecuteNonQuery(dbCom);
			
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("SysDataMgrDAL","InsertConveyance","DP002",appException.Message.ToString());
				throw new ApplicationException("Insert operation failed"+appException.Message,appException);
			}
			return iRowsInserted;
		}

		public static int UpdateConveyance(DataSet dsCostCode, String strAppID, String strEnterpriseID, String strAgentID)
		{
			int iRowsUpdated = 0;
			
			IDbCommand dbCom = null;
			DataRow dr = dsCostCode.Tables[0].Rows[0];

			string strConveyance = (string)dr[0];
			string strDescription=(string)dr[1];			
			decimal dLength=(decimal)dr[2];
			decimal dBreadth=(decimal)dr[3];
			decimal dHeight=(decimal)dr[4];
			decimal dMax_Wt=(decimal)dr[5];			

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("SysDataMgrDAL","UpdateConveyance","DP004","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			StringBuilder strSQLQuery=new StringBuilder();

			try
			{
				if(Utility.IsNotDBNull(strConveyance) && strConveyance!="")
				{
					strSQLQuery.Append(" update Agent_Conveyance set Conveyance_Description = N'"+Utility.ReplaceSingleQuote(strDescription)+"',");
					strSQLQuery.Append(" Length= "+dLength+",Breadth="+dBreadth+",Height="+dHeight+",Max_Wt="+dMax_Wt);
					strSQLQuery.Append(" where applicationid = '"+strAppID+"' and enterpriseid = '"+strEnterpriseID);
					strSQLQuery.Append("' and Conveyance_code = '"+strConveyance+"' and AgentId='"+strAgentID+"'"); 
	
					dbCom = dbCon.CreateCommand(strSQLQuery.ToString(),CommandType.Text);
					iRowsUpdated = dbCon.ExecuteNonQuery(dbCom);
				}
			}

			catch(ApplicationException appException)
			{
				Logger.LogTraceError("SysDataMgrDAL","UpdateCostCode","DP005",appException.Message.ToString());
				throw new ApplicationException("Update operation failed",appException);
				
			}
			return iRowsUpdated;
		}

		public static int DeleteConveyance(String strAppID, String strEnterpriseID,String strAgentID, String strConveyance)
		{
			int iRowsDeleted = 0;

			DbConnection dbConApp = null;
			IDbCommand dbCommandApp = null;

			IDbTransaction transactionApp = null;
			IDbConnection conApp = null;

			dbConApp = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbConApp == null)
			{
				Logger.LogTraceError("SysDataMgrDAL1","DeleteConveyance","SDM002","dbConApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			conApp = dbConApp.GetConnection();
			
			if(conApp == null)
			{
				Logger.LogTraceError("SysDataMgrDAL1","DeleteConveyance","SDM002","conApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			try
			{
				dbConApp.OpenConnection(ref conApp);
			}
			catch(ApplicationException appException)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("SysDataMgrDAL1","DeleteConveyance","SDM003","Error opening connection : "+ appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ",appException);
			}
			catch(Exception exception)
			{
				if(conApp != null)
				{
					conApp.Close();
				}

				Logger.LogTraceError("SysDataMgrDAL1","DeleteConveyance","SDM003","Error opening connection : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}

			transactionApp = dbConApp.BeginTransaction(conApp,IsolationLevel.ReadCommitted);
			if(transactionApp == null)
			{
				Logger.LogTraceError("SysDataMgrDAL1","DeleteConveyance","SDM002","transactionApp is null");
				throw new ApplicationException("Failed Begining a App Transaction..",null);
			}

			try
			{
				//Update record into core_enterprise Table

				StringBuilder strBuilder = new StringBuilder();
			
				strBuilder.Append("Delete from Agent_Conveyance where applicationid = '"+ strAppID);
				strBuilder.Append("' and enterpriseid = '"+strEnterpriseID+"' and Conveyance_Code = '");
				strBuilder.Append(strConveyance+"' and AgentID='"+strAgentID+"'");

				String strSQLQuery = strBuilder.ToString();
				dbCommandApp = dbConApp.CreateCommand(strSQLQuery,CommandType.Text); //Creating the command first time
				dbCommandApp.Connection = conApp;
				dbCommandApp.Transaction = transactionApp;

				iRowsDeleted = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);
				Logger.LogTraceInfo("SysDataMgrDAL","DeleteConveyance","INF001",iRowsDeleted + " rows deleted from Exception_Code table");

				strBuilder = new StringBuilder();
			
				strBuilder.Append("Delete from Agent_Zone_Conveyance_Cost where applicationid = '");
				strBuilder.Append(strAppID+"' and enterpriseid = '"+strEnterpriseID);
				strBuilder.Append("' and Conveyance_code = '"+strConveyance+"' and AgentID='"+strAgentID+"'");

				strSQLQuery = strBuilder.ToString();

				dbCommandApp.CommandText = strSQLQuery;

				iRowsDeleted = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);
				Logger.LogTraceInfo("SysDataMgrDAL","DeleteConveyance","INF001",iRowsDeleted + " rows deleted from Status_Code table");
			
				//Commit application and core transaction.

				transactionApp.Commit();
				Logger.LogTraceInfo("SysDataMgrDAL","DeleteConveyance","SDM003","App db delete transaction committed.");

			}
			catch(ApplicationException appException)
			{				
				try
				{
					transactionApp.Rollback();
					Logger.LogTraceInfo("SysDataMgrDAL","DeleteConveyance","SDM002","App db delete transaction rolled back.");
				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("SysDataMgrDAL","DeleteConveyance","SDM002","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}

				Logger.LogTraceError("SysDataMgrDAL","DeleteConveyance","SDM002","Delete Transaction Failed : "+ appException.Message.ToString());
				throw new ApplicationException("Error deleting Status Code "+appException.Message,appException);
			}
			catch(Exception exception)
			{
				try
				{
					transactionApp.Rollback();
					Logger.LogTraceInfo("SysDataMgrDAL","DeleteConveyance","SDM002","App db delete transaction rolled back.");
				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("SysDataMgrDAL","DeleteConveyance","SDM002","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}

				Logger.LogTraceError("SysDataMgrDAL","DeleteConveyance","SDM004","delete Transaction Failed : "+ exception.Message.ToString());
				throw new ApplicationException("Error deleting Status Code "+exception.Message,exception);
			}
			finally
			{
				if(conApp != null)
					conApp.Close();
			}
			
			return iRowsDeleted;
		}
		
		/// <summary>
		/// GetConveyanceDetails for the selected Conveyance
		/// </summary>
		/// <param name="strAppID"></param>
		/// <param name="strEnterpriseID"></param>
		/// <param name="strConveyance"></param>
		/// <returns></returns>
		public static DataSet GetConveyanceDetails(String strAppID, String strEnterpriseID,String strConveyance)
		{
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("SysDataMgrDAL","UpdateConveyance","DP004","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			DataSet dsConveyance=new DataSet();

			StringBuilder strSQLQuery = new StringBuilder();
			strSQLQuery.Append(" Select Conveyance_description, Length, Breadth, Height, Max_Wt from Conveyance where ");
			
			if (Utility.IsNotDBNull(strConveyance) && strConveyance !="")
			{
				strSQLQuery.Append(" conveyance_code like '%"+strConveyance+"%' and ");
			}
			strSQLQuery.Append(" applicationid = '"+strAppID+"' and enterpriseid = '"+strEnterpriseID+"'"); 						
			dsConveyance = (DataSet)dbCon.ExecuteQuery(strSQLQuery.ToString(),ReturnType.DataSetType);
			return  dsConveyance;
		}

		//Base Rates By Conveyance
		public static SessionDS GetEmptyBaseConvey()
		{
			DataTable dtConveyance =  new DataTable();
			
			dtConveyance.Columns.Add(new DataColumn("Origin_Zone_Code",typeof(string)));
			dtConveyance.Columns.Add(new DataColumn("Destination_Zone_Code",typeof(string)));
			dtConveyance.Columns.Add(new DataColumn("Conveyance_Code",typeof(string))); 
			dtConveyance.Columns.Add(new DataColumn("Start_Price",typeof(decimal))); 
			dtConveyance.Columns.Add(new DataColumn("Increment_Price",typeof(decimal))); 
			
			DataSet  dsConv = new DataSet();
			dsConv.Tables.Add(dtConveyance); 
 
			dsConv.Tables[0].Columns["conveyance_code"].AllowDBNull = false; //Checks duplicate records..
			dsConv.Tables[0].Columns["Origin_Zone_Code"].AllowDBNull = false;
			dsConv.Tables[0].Columns["Destination_Zone_Code"].AllowDBNull = false;
			
			SessionDS sessionds = new SessionDS();
			sessionds.ds = dsConv;
			
			sessionds.QueryResultMaxSize = sessionds.DataSetRecSize;
			return  sessionds;
		}

		public static void AddRowInBaseConveyDS(ref SessionDS dsConveyance)
		{
			DataRow drNew = dsConveyance.ds.Tables[0].NewRow();
			drNew[0] = "";
			drNew[1] = "";
			drNew[2] = "";
			drNew[3] = 0;
			drNew[4] = 0;
			
			try
			{
				dsConveyance.ds.Tables[0].Rows.Add(drNew);
			}
			catch(Exception ex)
			{
				Logger.LogTraceError("SysDataMgrDAL","AddNewRowInConveyanceDS","R005",ex.Message.ToString());
			}			
		}

		public static SessionDS GetBaseConveyDS(String strAppID, String strEnterpriseID, String strAgentID, int iCurRow, int iDSRowSize, DataSet dsQuery)
		{
			SessionDS sessionDS = null;
			DataRow dr = dsQuery.Tables[0].Rows[0];
			string strOZCode = (string)dr[0];
			string strDZCode =(string)dr[1];
			string strConveyance=(string)dr[2];
			decimal dStart_Price=Convert.ToDecimal(dr[3]);
			decimal dInc_Price=Convert.ToDecimal(dr[4]);
						
			StringBuilder strSQLQuery = new StringBuilder();
			strSQLQuery.Append(" Select Origin_Zone_Code, Destination_Zone_Code, conveyance_code, ");
			strSQLQuery.Append(" Start_Price, Increment_Price from Agent_Zone_Conveyance_Cost where ");
			
			if (Utility.IsNotDBNull(strOZCode) && strOZCode !="")
				strSQLQuery.Append(" Origin_Zone_Code like '%"+strOZCode+"%' and ");
			if (Utility.IsNotDBNull(strDZCode) && strDZCode !="")
				strSQLQuery.Append(" Destination_Zone_Code like '%"+strDZCode+"%' and ");
			if (Utility.IsNotDBNull(strConveyance) && strConveyance !="")
				strSQLQuery.Append(" conveyance_code like '%"+strConveyance+"%' and ");
			if (Utility.IsNotDBNull(strAgentID) && strAgentID !="")
				strSQLQuery.Append(" AgentID like '%"+strAgentID+"%' and ");
			if (Utility.IsNotDBNull(dInc_Price) && dInc_Price!=0)
				strSQLQuery.Append(" Increment_Price ="+dInc_Price+" and ");
			if (Utility.IsNotDBNull(dStart_Price) && dStart_Price!=0)
				strSQLQuery.Append(" Start_Price ="+dStart_Price+" and ");
			
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("SysDataManager","GetBaseConveyDS","EDB111","DbConnection object is null!!");
				return sessionDS;
			}
						
			strSQLQuery.Append(" applicationid = '"+strAppID+"' and enterpriseid = '"+strEnterpriseID+"'"); 						
			sessionDS = dbCon.ExecuteQuery(strSQLQuery.ToString(),iCurRow,iDSRowSize,"CostCodeTable");
			return  sessionDS;
		}

		public static int InsertBaseConvey(DataSet dsConveyance, int rowIndex, String strAppID, String strEnterpriseID, String strAgentID)
		{
			int iRowsInserted = 0;
			IDbCommand dbCom = null;
			DataRow dr = dsConveyance.Tables[0].Rows[rowIndex];
			
			string strOZ_Code=(string)dr[0];			
			string strDZ_Code=(string)dr[1];
			string strConveyance = (string)dr[2];
			decimal dStart_Price=(decimal)dr[3];
			decimal dInc_Price=(decimal)dr[4];
						
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("SysDataMgrDAL","InsertBaseConvey","DP001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			StringBuilder strSQLQuery=new StringBuilder();

			try
			{
				if(Utility.IsNotDBNull(strConveyance) && strConveyance!="")
				{
					strSQLQuery.Append(" Insert into Agent_Zone_Conveyance_Cost (applicationid, enterpriseid, AgentID, Conveyance_code, Origin_Zone_Code,");
					strSQLQuery.Append(" Destination_Zone_Code, Start_Price, Increment_Price) values ('"+strAppID+"', '"+strEnterpriseID+"', '"+strAgentID+"','"+strConveyance+"'");
					strSQLQuery.Append(",'"+strOZ_Code+"','"+strDZ_Code+"',"+dStart_Price+","+dInc_Price+")");
					
					dbCom = dbCon.CreateCommand(strSQLQuery.ToString(),CommandType.Text);
					iRowsInserted = dbCon.ExecuteNonQuery(dbCom);
				}
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("SysDataMgrDAL","InsertBaseConvey","DP002",appException.Message.ToString());
				throw new ApplicationException("Insert operation failed"+appException.Message,appException);
			}
			return iRowsInserted;
		}
		//
		public static int UpdateBaseConvey(DataSet dsCostCode, String strAppID, String strEnterpriseID, String strAgentID)
		{
			int iRowsUpdated = 0;
			
			IDbCommand dbCom = null;
			DataRow dr = dsCostCode.Tables[0].Rows[0];

			string strOZ_Code=(string)dr[0];			
			string strDZ_Code=(string)dr[1];
			string strConveyance = (string)dr[2];
			decimal dStart_Price=(decimal)dr[3];
			decimal dInc_Price=(decimal)dr[4];

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("SysDataMgrDAL","UpdateBaseConvey","DP004","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			StringBuilder strSQLQuery=new StringBuilder();

			try
			{
				if(Utility.IsNotDBNull(strConveyance) && strConveyance!="")
				{
					strSQLQuery.Append(" update Agent_Zone_Conveyance_Cost set  Start_Price= "+dStart_Price+",Increment_Price="+dInc_Price);					
					strSQLQuery.Append(" where applicationid = '"+strAppID+"' and enterpriseid = '"+strEnterpriseID);
					strSQLQuery.Append("' and Conveyance_code = '"+strConveyance+"' and Origin_Zone_Code='"+strOZ_Code+"' and "); 
					strSQLQuery.Append(" Destination_Zone_Code='"+strDZ_Code+"' and AgentID='"+strAgentID+"'");
	
					dbCom = dbCon.CreateCommand(strSQLQuery.ToString(),CommandType.Text);
					iRowsUpdated = dbCon.ExecuteNonQuery(dbCom);
				}
			}

			catch(ApplicationException appException)
			{
				Logger.LogTraceError("SysDataMgrDAL","UpdateBaseConvey","DP005",appException.Message.ToString());
				throw new ApplicationException("Update operation failed",appException);
				
			}
			return iRowsUpdated;
		}
		//
		public static int DeleteBaseConvey(String strAppID, String strEnterpriseID, String strAgentID, String strOZCode, String strDZCode, String strConveyance)
		{
			int iRowsDeleted = 0;
		
			IDbCommand dbCom  = null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("SysDataMgr","DeleteCostCode","DPC001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			
			try
			{			
				StringBuilder strBuilder = new StringBuilder();
			
				strBuilder.Append("Delete from Agent_Zone_Conveyance_Cost where applicationid = '");
				strBuilder.Append(strAppID+"' and enterpriseid = '"+strEnterpriseID);
				strBuilder.Append("' and Conveyance_code = '"+strConveyance+"' and Origin_Zone_Code='"+strOZCode);
				strBuilder.Append("' and Destination_Zone_Code='"+strDZCode+"' and AgentID='"+strAgentID+"'");

				String strSQLQuery = strBuilder.ToString();

				dbCom = dbCon.CreateCommand(strSQLQuery,CommandType.Text);
				iRowsDeleted = dbCon.ExecuteNonQuery(dbCom);				

			}

			catch(ApplicationException appException)
			{
				Logger.LogTraceError("SysDataMgr","DeleteCostCode","DPC002",appException.Message.ToString());
			
				throw new ApplicationException("Delete operation failed"+appException.Message,appException);
			}
			return iRowsDeleted;

		}

		public static bool InsertReferences(SessionDS sdsBaseTables, String strAppID, String strEnterpriseID)
		{
			IDbCommand dbCom = null;
			DataRow dr = sdsBaseTables.ds.Tables[0].Rows[0];

			String strTelephone = (String)dr[0];
			String strRefenrenceName = (String)dr[1];
			String strAdd1 = (String)dr[2];
			String strAdd2 = (String)dr[3];
			String strPostalCode = (String)dr[4];
			String strFax = (String)dr[5];
			String strContactperson = (String)dr[6];

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
				
			if(strTelephone!=null && strTelephone!="")
			{
				String strSQLQuery = "INSERT INTO  [References] (applicationid,enterpriseid,telephone,reference_name, address1,address2,zipcode,fax,contactperson) ";
				strSQLQuery += " values('"+strAppID+"','"+strEnterpriseID+"','"+Utility.ReplaceSingleQuote(strTelephone)+"','"+Utility.ReplaceSingleQuote(strRefenrenceName)+"','"+Utility.ReplaceSingleQuote(strAdd1)+"','"+Utility.ReplaceSingleQuote(strAdd2)+"','"+Utility.ReplaceSingleQuote(strPostalCode)+"','"+Utility.ReplaceSingleQuote(strFax)+"','"+Utility.ReplaceSingleQuote(strContactperson)+"')";
			
				dbCom = dbCon.CreateCommand(strSQLQuery,CommandType.Text);
				if(dbCon.ExecuteNonQuery(dbCom)>0)
					return true;
			}
			return false;
		}

		public static bool UpdateReferences(SessionDS sdsBaseTables, String strAppID, String strEnterpriseID)
		{
			IDbCommand dbCom = null;
			DataRow dr = sdsBaseTables.ds.Tables[0].Rows[0];
			String strTelephone = (String)dr[0];
			String strRefenrenceName = (String)dr[1];
			String strAdd1 = (String)dr[2];
			String strAdd2 = (String)dr[3];
			String strPostalCode = (String)dr[4];
			String strFax = (String)dr[5];
			String strContactPerson = (String)dr[6];
			String strCurrentTelephone = (String)dr[7];

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(strTelephone!=null && strTelephone!="")
			{
				String strSQLQuery = "UPDATE [References] SET reference_name = N'"+Utility.ReplaceSingleQuote(strRefenrenceName)+"' ";
				strSQLQuery += ", address1= N'"+Utility.ReplaceSingleQuote(strAdd1)+"' ";
				strSQLQuery += ", address2= N'"+Utility.ReplaceSingleQuote(strAdd2)+"' ";
				strSQLQuery += ", zipcode = N'"+Utility.ReplaceSingleQuote(strPostalCode)+"' ";
				strSQLQuery += ", fax = N'"+Utility.ReplaceSingleQuote(strFax)+"' ";
				strSQLQuery += ", contactperson = N'"+Utility.ReplaceSingleQuote(strContactPerson)+"' ";
				strSQLQuery += ", telephone = N'"+Utility.ReplaceSingleQuote(strTelephone)+"' ";
				strSQLQuery += " where applicationid = "+"'"+strAppID+"' and enterpriseid = '"+strEnterpriseID+"' ";
				strSQLQuery += "and telephone = '"+Utility.ReplaceSingleQuote(strCurrentTelephone)+"' "; 
	
				dbCom = dbCon.CreateCommand(strSQLQuery,CommandType.Text);
				if(dbCon.ExecuteNonQuery(dbCom)>0)
					return true;
			}
			return false;
		}

		public static bool DeleteReferences(String strTelephone, String strAppID, String strEnterpriseID)
		{
			IDbCommand dbCom = null;

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(strTelephone!=null && strTelephone!="")
			{
				String strSQLQuery = "Delete from [References] where ";
				strSQLQuery += "applicationid = "+"'"+strAppID+"' and enterpriseid = '"+strEnterpriseID+"' and telephone = '"+Utility.ReplaceSingleQuote(strTelephone)+"' "; 
	
				dbCom = dbCon.CreateCommand(strSQLQuery,CommandType.Text);
				if(dbCon.ExecuteNonQuery(dbCom)>0)
					return true;
			}
			return false;
		}

		public static SessionDS GetEmptyReferences(int iNumRows)
		{
			DataTable dt = new DataTable();
			
			dt.Columns.Add(new DataColumn("telephone", typeof(string)));
			dt.Columns.Add(new DataColumn("reference_name", typeof(string)));
			dt.Columns.Add(new DataColumn("address1", typeof(string)));
			dt.Columns.Add(new DataColumn("address2", typeof(string)));
			dt.Columns.Add(new DataColumn("zipcode", typeof(string)));
			dt.Columns.Add(new DataColumn("fax", typeof(string)));
			dt.Columns.Add(new DataColumn("contactperson", typeof(string)));
			dt.Columns.Add(new DataColumn("current_telephone", typeof(string)));
			for(int i=0; i < iNumRows; i++)
			{
				DataRow dr = dt.NewRow();
				dr[0] = "";
				dr[1] = "";
				dr[2] = "";
				dr[3] = "";
				dr[4] = "";
				dr[5] = "";
				dr[6] = "";
				dr[7] = "";
				dt.Rows.Add(dr);
			}
			DataSet ds = new DataSet();
			SessionDS sessionds = new SessionDS();
			ds.Tables.Add(dt);

			//			dsCommodity.Tables[0].Columns["commodity_code"].Unique = true; //Checks duplicate records..
			ds.Tables[0].Columns["telephone"].Unique = true;
			ds.Tables[0].Columns["telephone"].AllowDBNull = false;

			sessionds.ds = ds;

			sessionds.DataSetRecSize = ds.Tables[0].Rows.Count;
			sessionds.QueryResultMaxSize = sessionds.DataSetRecSize;
			return  sessionds;
	
		}

		public static void AddNewRowReferencesDS(ref SessionDS ds)
		{
			//			m_sdsCommodity = (SessionDS)Session["SESSION_DS1"];
			DataRow drNew = ds.ds.Tables[0].NewRow();

			for (int i=0;i< ds.ds.Tables[0].Columns.Count; i++)
				drNew[i] = "";
			try
			{
				ds.ds.Tables[0].Rows.Add(drNew);
			}
			catch(Exception ex)
			{
				Logger.LogTraceError("SysDataMgrDAL","AddNewRowReferencesDS","R005",ex.Message.ToString());
			}

		}

	}

}


