using System;
using System.Data;
using System.Collections;
using System.Text;
using com.common.DAL;
using com.common.util;
using com.common.classes;
using com.ties.classes;

namespace com.ties.DAL
{
	/// <summary>
	/// Summary description for Vehicle.
	/// </summary>
	public class Vehicle
	{
		public Vehicle()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		
		public static SessionDS GetEmptyVehicle()
		{
			DataSet  dsVehicle = new DataSet();
			DataTable dtVehicle =  new DataTable();
			
			dtVehicle.Columns.Add(new DataColumn("TruckID",typeof(string)));
			dtVehicle.Columns.Add(new DataColumn("Description",typeof(string)));
			dtVehicle.Columns.Add(new DataColumn("LicensePlate",typeof(string))); 
			dtVehicle.Columns.Add(new DataColumn("Type",typeof(string)));			
			dtVehicle.Columns.Add(new DataColumn("Year",typeof(string))); 
			dtVehicle.Columns.Add(new DataColumn("Brand",typeof(string))); 
			dtVehicle.Columns.Add(new DataColumn("Model",typeof(string))); 
			dtVehicle.Columns.Add(new DataColumn("Color",typeof(string))); 
			dtVehicle.Columns.Add(new DataColumn("Owner",typeof(string))); 
			dtVehicle.Columns.Add(new DataColumn("Type_Of_Lease",typeof(string)));
			dtVehicle.Columns.Add(new DataColumn("Lease_Start_Date",typeof(DateTime))); 
			dtVehicle.Columns.Add(new DataColumn("Lease_End_Date",typeof(DateTime)));
			dtVehicle.Columns.Add(new DataColumn("Monthly_Cost",typeof(decimal))); 
			dtVehicle.Columns.Add(new DataColumn("base_location",typeof(string)));
			dtVehicle.Columns.Add(new DataColumn("Tare_Weight",typeof(int))); 
			dtVehicle.Columns.Add(new DataColumn("Gross_Weight",typeof(int))); 
			dtVehicle.Columns.Add(new DataColumn("Max_CBM",typeof(int))); 
			dtVehicle.Columns.Add(new DataColumn("Remark",typeof(string)));
			dtVehicle.Columns.Add(new DataColumn("Registration_date",typeof(DateTime))); 
			dtVehicle.Columns.Add(new DataColumn("Initial_in_service_date",typeof(DateTime))); 
			dtVehicle.Columns.Add(new DataColumn("disposal_date",typeof(DateTime))); 
			dtVehicle.Columns.Add(new DataColumn("date_out_of_service",typeof(DateTime)));
			dtVehicle.Columns.Add(new DataColumn("date_in_service",typeof(DateTime)));

			dsVehicle.Tables.Add(dtVehicle); 

			dsVehicle.Tables[0].Columns["TruckID"].AllowDBNull = false;

			SessionDS sessionds = new SessionDS();
			sessionds.ds = dsVehicle;
				
			sessionds.QueryResultMaxSize = sessionds.DataSetRecSize;
			return  sessionds;

		}

		
		public static SessionDS GetVehicleCodeDS(String strAppID, String strEnterpriseID, int iCurRow, int iDSRowSize, DataSet dsQuery)
		{
			SessionDS sessionDS = null;
			DataRow dr = dsQuery.Tables[0].Rows[0];

			DateTime dtLease_Start_Date;
			DateTime dtLease_End_Date;
			DateTime dtRegistration_Date;
			DateTime dtInitial_In_Service_Date;
			DateTime dtDisposal_Date;
			DateTime dtDate_Out_Of_Service;
			DateTime dtDate_In_Service;

			decimal decMonthly_Cost;
			int intTare_Weight;
			int intGross_Weight;
			int intMax_Cbm;

			string strTruckID = (string)dr["TruckID"];
			string strDescription = (string)dr["Description"];
			string strLicensePlate = (string)dr["LicensePlate"];
			string strType = (string)dr["Type"];
			string strYear = (string)dr["Year"];
			string strBrand = (string)dr["Brand"];
			string strModel = (string)dr["Model"];
			string strColor = (string)dr["Color"];
			string strOwner = (string)dr["Owner"];
			string strType_Of_Lease = (string)dr["Type_Of_Lease"];

			string strBaseLocation = (string)dr["base_location"];
			string strRemark = (string)dr["remark"];			

			String strSQLQuery;
			string strSQLWhere = "";

			if (strTruckID != null && strTruckID != "")
				strSQLWhere += " and truckid like '%" + strTruckID + "%' ";

			if (strDescription != null && strDescription != "")
				strSQLWhere += " and description like N'%" + strDescription + "%' ";			
			
			if (strLicensePlate != null && strLicensePlate != "")
				strSQLWhere += " and licenseplate like N'%" + Utility.ReplaceSingleQuote(strLicensePlate) + "%' ";
			
			if (strType != null && strType != "")
				strSQLWhere += " and type like '%" + Utility.ReplaceSingleQuote(strType) + "%' ";

			if (strYear != null && strYear != "")
				strSQLWhere += " and year like '%" + Utility.ReplaceSingleQuote(strYear) + "%' ";

			if (strBrand != null && strBrand != "")
				strSQLWhere += " and brand like '%" + Utility.ReplaceSingleQuote(strBrand) + "%' ";

			if (strModel != null && strModel != "")
				strSQLWhere += " and model like '%" + Utility.ReplaceSingleQuote(strModel) + "%' ";

			if (strColor != null && strColor != " ")
				strSQLWhere += " and color like '%" + Utility.ReplaceSingleQuote(strColor) + "%' ";

			if (strOwner != null && strOwner != "")
				strSQLWhere += " and owner like '%" + Utility.ReplaceSingleQuote(strOwner) + "%' ";

			if (strType_Of_Lease != null && strType_Of_Lease != "")
				strSQLWhere += " and type_of_lease like '%" + Utility.ReplaceSingleQuote(strType_Of_Lease) + "%' ";

			if (strBaseLocation != null && strBaseLocation != "")
				strSQLWhere += " and RTRIM(BASE_LOCATION) like '%" + Utility.ReplaceSingleQuote(strBaseLocation) + "%' ";

			if (strRemark != null && strRemark != "")
				strSQLWhere += " and remark like '%" + Utility.ReplaceSingleQuote(strRemark) + "%' ";

			if(Utility.IsNotDBNull(dr["lease_start_date"]) && dr["lease_start_date"].ToString()!="")
			{
				dtLease_Start_Date = System.Convert.ToDateTime(dr["lease_start_date"]);							
				strSQLWhere += " and lease_start_date ='"+dtLease_Start_Date+"'"; 								
			}

			if(Utility.IsNotDBNull(dr["lease_end_date"]) && dr["lease_end_date"].ToString()!="")
			{
				dtLease_End_Date = System.Convert.ToDateTime(dr["lease_end_date"]);							
				strSQLWhere += " and lease_end_date ='"+dtLease_End_Date+"'"; 								
			}

			if(Utility.IsNotDBNull(dr["registration_date"]) && dr["registration_date"].ToString()!="")
			{
				dtRegistration_Date = System.Convert.ToDateTime(dr["registration_date"]);							
				strSQLWhere += " and registration_date ='"+dtRegistration_Date+"'"; 								
			}
			
			if(Utility.IsNotDBNull(dr["initial_in_service_date"]) && dr["initial_in_service_date"].ToString()!="")
			{
				dtInitial_In_Service_Date = System.Convert.ToDateTime(dr["initial_in_service_date"]);							
				strSQLWhere += " and initial_in_service_date ='"+dtInitial_In_Service_Date+"'"; 								
			}
			
			if(Utility.IsNotDBNull(dr["disposal_date"]) && dr["disposal_date"].ToString()!="")
			{
				dtDisposal_Date = System.Convert.ToDateTime(dr["disposal_date"]);							
				strSQLWhere += " and disposal_date ='"+dtDisposal_Date+"'"; 								
			}

			if(Utility.IsNotDBNull(dr["date_out_of_service"]) && dr["date_out_of_service"].ToString()!="")
			{
				dtDate_Out_Of_Service = System.Convert.ToDateTime(dr["date_out_of_service"]);							
				strSQLWhere += " and date_out_of_service ='"+dtDate_Out_Of_Service+"'"; 								
			}

			if(Utility.IsNotDBNull(dr["date_in_service"]) && dr["date_in_service"].ToString()!="")
			{
				dtDate_In_Service = System.Convert.ToDateTime(dr["date_in_service"]);							
				strSQLWhere += " and date_in_service ='"+dtDate_In_Service+"'"; 								
			}

			if(Utility.IsNotDBNull(dr["monthly_cost"]) && dr["monthly_cost"].ToString()!= "0")
			{
				decMonthly_Cost = System.Convert.ToDecimal(dr["monthly_cost"]);							
				strSQLWhere += " and monthly_cost ='"+decMonthly_Cost+"'"; 								
			}

			if(Utility.IsNotDBNull(dr["tare_weight"]) && dr["tare_weight"].ToString()!= "0")
			{
				intTare_Weight = System.Convert.ToInt32(dr["tare_weight"]);							
				strSQLWhere += " and tare_weight ='"+intTare_Weight+"'"; 								
			}

			if(Utility.IsNotDBNull(dr["gross_weight"]) && dr["gross_weight"].ToString()!= "0")
			{
				intGross_Weight = System.Convert.ToInt32(dr["gross_weight"]);							
				strSQLWhere += " and gross_weight ='"+intGross_Weight+"'"; 								
			}

			if(Utility.IsNotDBNull(dr["max_cbm"]) && dr["max_cbm"].ToString()!= "0")
			{
				intMax_Cbm = System.Convert.ToInt32(dr["max_cbm"]);							
				strSQLWhere += " and max_Cbm ='"+intMax_Cbm+"'"; 								
			}
						
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("Vehicle", "GetVehicleCodeDS", "EDB101", "DbConnection object is null!!");
				return sessionDS;
			}

			strSQLQuery = "SELECT TRUCKID, DESCRIPTION, LICENSEPLATE, TYPE, YEAR, BRAND, MODEL, ";
			strSQLQuery += " COLOR, OWNER, TYPE_OF_LEASE, LEASE_START_DATE, LEASE_END_DATE, ";
			strSQLQuery += " MONTHLY_COST, RTRIM(BASE_LOCATION) AS BASE_LOCATION, TARE_WEIGHT, ";
			strSQLQuery += " GROSS_WEIGHT, MAX_CBM, REMARK, REGISTRATION_DATE, INITIAL_IN_SERVICE_DATE, ";
			strSQLQuery += " DISPOSAL_DATE, DATE_OUT_OF_SERVICE, DATE_IN_SERVICE ";
			strSQLQuery += " FROM MF_VEHICLES ";
			strSQLQuery += " WHERE ApplicationId = '" + strAppID + "'  AND enterpriseid = '" + strEnterpriseID + "'"; 

			if (strSQLWhere != null && strSQLWhere != "")
				strSQLQuery += strSQLWhere;
			
			sessionDS = dbCon.ExecuteQuery(strSQLQuery, iCurRow, iDSRowSize, "Mf_VehicleTable");

			return  sessionDS;
		}

		
		public static void AddNewRowVehicleCodeDS(ref SessionDS dsVehicleCode)
		{
			DataRow drNew = dsVehicleCode.ds.Tables[0].NewRow();
			drNew[0] = "";
			drNew[1] = "";
			drNew[2] = "";
			try
			{
				dsVehicleCode.ds.Tables[0].Rows.Add(drNew);
			}
			catch(Exception ex)
			{
				Logger.LogTraceError("Vehicle","AddNewRowVehicleCodeDS","R005",ex.Message.ToString());
			}
		}

		

		public static int InsertVehicle(DataSet dsVehicle, int rowIndex, String strAppID, String strEnterpriseID)
		{
			int iRowsInserted = 0;
			IDbCommand dbCom = null;
			DataRow dr = dsVehicle.Tables[0].Rows[rowIndex];

			string strTruckID = (string)dr["truckid"];
			string strLicensePlate = (string)dr["licenseplate"];
			string strDescription=(string)dr["description"];
			string strType = (string)dr["Type"];
			string strYear = (string)dr["year"];
			string strBrand = (string)dr["brand"];
			string strModel = (string)dr["model"];
			string strColor = (string)dr["color"];
			string strOwner = (string)dr["owner"];
			string strTypeOfLease = (string)dr["type_of_lease"];
			decimal decMonthlyCost = (decimal)dr["Monthly_Cost"];
			string strBaseLocation = (string)dr["base_location"];
			int intTareWeight = (int)dr["tare_weight"];
			int intGrossWeight = (int)dr["Gross_Weight"];
			int intMaxCBM = (int)dr["Max_CBM"];
			string strRemark = (string)dr["Remark"];
		
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("Vehicle", "InsertVehicle", "DP001", "dbCon is null");
				throw new ApplicationException("Connection to Database failed", null);
			}

			try
			{
				if(strTruckID != null && strTruckID != "")
				{
					string strSQLQuery = "";

					strSQLQuery = "INSERT INTO MF_VEHICLES ";
					strSQLQuery += " (ApplicationID, EnterpriseID, TruckID, ";
					strSQLQuery += " Description, LicensePlate, Type, Year, ";
					strSQLQuery += " Brand, Model, Color, Owner, Type_Of_Lease, Lease_Start_Date,  Lease_End_Date, ";
					strSQLQuery += " Monthly_Cost, Base_Location, Tare_Weight, Gross_Weight, Max_CBM, Remark, Registration_Date, ";
					strSQLQuery += " Initial_In_Service_Date, Disposal_Date, Date_Out_Of_Service, Date_In_Service) ";
					strSQLQuery += " VALUES ('" + strAppID + "', '" + strEnterpriseID + "', '" + strTruckID + "', ";
					strSQLQuery += " N'" + Utility.ReplaceSingleQuote(strDescription) + "', ";
					strSQLQuery += " N'" + Utility.ReplaceSingleQuote(strLicensePlate) + "', ";
					strSQLQuery += " '" + strType + "', '" + strYear + "', ";
					strSQLQuery += " '" + strBrand + "', '" + strModel + "', ";
					strSQLQuery += " '" + strColor + "', '" + strOwner + "', ";
					strSQLQuery += " '" + strTypeOfLease + "', ";

					if (!dr["lease_start_date"].GetType().Equals(System.Type.GetType("System.DBNull")))
						strSQLQuery += " '" + dr["lease_start_date"] + "',";
					else
						strSQLQuery += " NULL,";

					if (!dr["lease_end_date"].GetType().Equals(System.Type.GetType("System.DBNull")))
						strSQLQuery += " '" + dr["lease_end_date"] + "',";
					else
						strSQLQuery += " NULL,";

					strSQLQuery += " '" + decMonthlyCost + "', ";
					strSQLQuery += " '" + strBaseLocation + "', " + intTareWeight + ", ";
					strSQLQuery += " " + intGrossWeight + ", '" + intMaxCBM + "', ";
					strSQLQuery += " N'" + strRemark + "', ";

					if (!dr["registration_date"].GetType().Equals(System.Type.GetType("System.DBNull")))
						strSQLQuery += " '" + dr["registration_date"] + "',";
					else
						strSQLQuery += " NULL,";

					if (!dr["initial_in_service_date"].GetType().Equals(System.Type.GetType("System.DBNull")))
						strSQLQuery += " '" + dr["initial_in_service_date"] + "',";
					else
						strSQLQuery += " NULL,";

					if (!dr["disposal_date"].GetType().Equals(System.Type.GetType("System.DBNull")))
						strSQLQuery += " '" + dr["disposal_date"] + "',";
					else
						strSQLQuery += " NULL,";

					if (!dr["date_out_of_service"].GetType().Equals(System.Type.GetType("System.DBNull")))
						strSQLQuery += " '" + dr["date_out_of_service"] + "',";
					else
						strSQLQuery += " NULL,";

					if (!dr["date_in_service"].GetType().Equals(System.Type.GetType("System.DBNull")))
						strSQLQuery += " '" + dr["date_in_service"] + "'";
					else
						strSQLQuery += " NULL";

					strSQLQuery += ")";
					dbCom = dbCon.CreateCommand(strSQLQuery,CommandType.Text);

					iRowsInserted = dbCon.ExecuteNonQuery(dbCom);
				}
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("Vehicle","InsertVehicle","DP002",appException.Message.ToString());
				throw new ApplicationException("Insert operation failed"+appException.Message,appException);
			}
			return iRowsInserted;
		}


		
		public static int UpdateVehicle(DataSet dsVehicle, String strAppID, String strEnterpriseID)
		{
			int iRowsUpdated = 0;
			String strSQLQuery=null;
			IDbCommand dbCom = null;
			DataRow dr = dsVehicle.Tables[0].Rows[0];

			string strTruckID = (string)dr["truckid"];
			string strLicensePlate = (string)dr["licenseplate"];
			string strDescription=(string)dr["description"];
			string strType = (string)dr["Type"];
			string strYear = (string)dr["year"];
			string strBrand = (string)dr["brand"];
			string strModel = (string)dr["model"];
			string strColor = (string)dr["color"];
			string strOwner = (string)dr["owner"];
			string strTypeOfLease = (string)dr["type_of_lease"];
			decimal decMonthlyCost = (decimal)dr["Monthly_Cost"];
			string strBaseLocation = (string)dr["base_location"];
			int intTareWeight = (int)dr["tare_weight"];
			int intGrossWeight = (int)dr["Gross_Weight"];
			int intMaxCBM = (int)dr["Max_CBM"];
			string strRemark = (string)dr["Remark"];

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("Vehicle", "UpdateVehicle", "DP004", "dbCon is null");
				throw new ApplicationException("Connection to Database failed", null);
			}

			try
			{
				if(strTruckID != null && strTruckID != "")
				{
					strSQLQuery = "UPDATE MF_VEHICLES ";
					strSQLQuery +=" SET description = N'" + Utility.ReplaceSingleQuote(strDescription) + "'";
					strSQLQuery += ", licenseplate = N'" + Utility.ReplaceSingleQuote(strLicensePlate) + "'";
					strSQLQuery += ", type = N'" + Utility.ReplaceSingleQuote(strType) + "'";
					strSQLQuery += ", year = N'" + Utility.ReplaceSingleQuote(strYear) + "'";
					strSQLQuery += ", brand = N'" + Utility.ReplaceSingleQuote(strBrand) + "'";
					strSQLQuery += ", model = N'" + Utility.ReplaceSingleQuote(strModel) + "'";
					strSQLQuery += ", color = N'" + Utility.ReplaceSingleQuote(strColor) + "'";
					strSQLQuery += ", Owner = N'" + Utility.ReplaceSingleQuote(strOwner) + "'";
					strSQLQuery += ", Type_Of_Lease = N'" + Utility.ReplaceSingleQuote(strTypeOfLease) + "'";
					strSQLQuery += ", Monthly_Cost = " + decMonthlyCost;
					strSQLQuery += ", Base_Location = N'" + Utility.ReplaceSingleQuote(strBaseLocation) + "'";
					strSQLQuery += ", Tare_Weight = " + intTareWeight;
					strSQLQuery += ", Gross_Weight = " + intGrossWeight;
					strSQLQuery += ", Max_CBM = " + intMaxCBM;
					strSQLQuery += ", Remark = N'" + Utility.ReplaceSingleQuote(strRemark) + "'";

					if (!dr["lease_start_date"].GetType().Equals(System.Type.GetType("System.DBNull")))
						strSQLQuery += ", lease_start_date = '" + dr["lease_start_date"] + "'";
					else
						strSQLQuery += ", lease_start_date = NULL ";

					if (!dr["lease_end_date"].GetType().Equals(System.Type.GetType("System.DBNull")))
						strSQLQuery += ", lease_end_date = '" + dr["lease_end_date"] + "'";
					else
						strSQLQuery += ", lease_end_date = NULL ";

					if (!dr["registration_date"].GetType().Equals(System.Type.GetType("System.DBNull")))
						strSQLQuery += ", registration_date = '" + dr["registration_date"] + "'";
					else
						strSQLQuery += ", registration_date = NULL ";

					if (!dr["initial_in_service_date"].GetType().Equals(System.Type.GetType("System.DBNull")))
						strSQLQuery += ", initial_in_service_date = '" + dr["initial_in_service_date"] + "'";
					else
						strSQLQuery += ", initial_in_service_date = NULL ";

					if (!dr["disposal_date"].GetType().Equals(System.Type.GetType("System.DBNull")))
						strSQLQuery += ", disposal_date = '" + dr["disposal_date"] + "'";
					else
						strSQLQuery += ", disposal_date = NULL ";

					if (!dr["date_out_of_service"].GetType().Equals(System.Type.GetType("System.DBNull")))
						strSQLQuery += ", date_out_of_service = '" + dr["date_out_of_service"] + "'";
					else
						strSQLQuery += ", date_out_of_service = NULL ";

					if (!dr["date_in_service"].GetType().Equals(System.Type.GetType("System.DBNull")))
						strSQLQuery += ", date_in_service = '" + dr["date_in_service"] + "'";
					else
						strSQLQuery += ", date_in_service = NULL ";

					strSQLQuery += " WHERE applicationid = '" + strAppID + "' AND enterpriseid = '" + strEnterpriseID + "' ";
					strSQLQuery += " AND truckid = '" + strTruckID + "'"; 
	
					dbCom = dbCon.CreateCommand(strSQLQuery,CommandType.Text);
					iRowsUpdated = dbCon.ExecuteNonQuery(dbCom);
				}
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("Vehicle", "UpdateVehicle", "DP005", appException.Message.ToString());
				throw new ApplicationException("Update operation failed", appException);				
			}
			return iRowsUpdated;
		}
		

		public static DataSet GetDistributionCenter(String appID, String enterpriseID)
		{
			DbConnection dbConnect = null;
			IDbCommand dbCmd = null;
			DataSet DS = null;

			dbConnect = DbConnectionManager.GetInstance().GetDbConnection(appID, enterpriseID);
  
			if (dbConnect == null)
			{
				Logger.LogTraceError("TIESDAL", "Vehicle", "GetDestinationCode", "dbConnect is null");
				throw new ApplicationException("Connection to Database failed", null); 
			}

			StringBuilder strQry = new StringBuilder();

			strQry = strQry.Append("SELECT distinct origin_code ");
			strQry = strQry.Append(" FROM DISTRIBUTION_CENTER "); 
			strQry = strQry.Append(" WHERE APPLICATIONID = '" + appID + "' "); 
			strQry = strQry.Append(" AND ENTERPRISEID = '" + enterpriseID + "' ");
			strQry = strQry.Append(" UNION ALL SELECT '' ");
			strQry = strQry.Append(" ORDER BY origin_code");
 
			dbCmd = dbConnect.CreateCommand(strQry.ToString(), CommandType.Text);
			try
			{
				DS = (DataSet)dbConnect.ExecuteQuery(dbCmd, com.common.DAL.ReturnType.DataSetType);
				return DS;     
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("TIESDAL", "Vehicle", "GetDestinationCode", "Error in Query String " + appException.Message);
				throw appException;
			}
			finally
			{
				DS.Dispose();
			}
		}	
		
	
		
		public static bool IsTruckIdIsExistManifest(String strAppID, String strEnterpriseID, String strTruckID)
		{
			int iCurRow = 0;
			int iDSRowSize = 0;
			SessionDS sessionDS = null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("Vehicle", "SelectDistributionCenter", "DPC001", "dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			
			try
			{
				if(strTruckID != null && strTruckID != "")
				{
					string strSQLQuery = "";

					strSQLQuery = "SELECT * FROM dbo.MF_Batch_Manifest ";
					strSQLQuery += " WHERE applicationid = '" + strAppID + "' AND enterpriseid = '" + strEnterpriseID + "' ";
					strSQLQuery += " AND truck_id = '" + strTruckID + "' ";
	
					sessionDS = dbCon.ExecuteQuery(strSQLQuery, iCurRow, iDSRowSize, "MF_Batch_Manifest_Table");
				}
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("Vehicle", "IsTruckIdIsExistManifest", "DPC002", appException.Message.ToString());			
				throw new ApplicationException("Select operation failed" + appException.Message,appException);
			}

			if (sessionDS == null || sessionDS.ds.Tables[0].Rows.Count == 0)
				return false;
			else
				return true;
		}
		
		
		public static int DeleteVehicle(DataSet dsVehicle, int rowIndex, String strAppID, String strEnterpriseID)
		{
			int iRowsDeleted = 0;
			IDbCommand dbCom = null;
			DataRow dr = dsVehicle.Tables[0].Rows[rowIndex];
			string strTruckID = (string)dr["truckid"];		

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("Vehicle", "DeleteVehicle", "DPC001", "dbCon is null");
				throw new ApplicationException("Connection to Database failed", null);
			}
			
			try
			{
				if(strTruckID != null && strTruckID != "")
				{
					string strSQLQuery = "DELETE FROM MF_VEHICLES ";
					strSQLQuery += " WHERE applicationid = " + "'" + strAppID + "' AND enterpriseid = '" + strEnterpriseID + "' ";
					strSQLQuery += " AND truckid = '" + strTruckID + "'"; 
	
					dbCom = dbCon.CreateCommand(strSQLQuery,CommandType.Text);
					iRowsDeleted = dbCon.ExecuteNonQuery(dbCom);
				}
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("Vehicle", "DeleteVehicle", "DPC002", appException.Message.ToString());
			
				throw new ApplicationException("Delete operation failed" + appException.Message,appException);
			}
			return iRowsDeleted;
		}
	}
}
