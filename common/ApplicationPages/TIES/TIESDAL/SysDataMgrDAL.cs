using System;
using System.Data;
using System.Collections;
using System.Text;
using com.common.DAL;
using com.common.util;
using com.common.classes;


namespace com.ties.DAL
{
	/// <summary>
	/// Summary description for SysDataMgrDAL.
	/// </summary>
	/// 

	enum SelectOptions
	{
		WithNil,WithoutNil
	}

	public class SysDataMgrDAL
	{
		

		public SysDataMgrDAL()
		{
		}

		public static DataSet GetStatusCodeDS(String strAppID, String strEnterpriseID)
		{
			DataSet dsStatusCode = null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("SysDataMgrDAL","GetStatusCodeDS","EDB101","DbConnection object is null!!");
				return dsStatusCode;
			}

			String strSQLQuery = " select status_code,status_description,Status_type,status_close,invoiceable,system_code,tt_display_status,tt_display_remarks,auto_process,auto_location,auto_remark,tt_display_location, scanning_type ,AllowRoles, isnull(AllowInSWB_Emu,'N') as AllowInSWB_Emu, isnull(AllowUPD_ByBatch,'N') as AllowUPD_ByBatch from Status_Code where ";			
			strSQLQuery += " applicationid = '"+strAppID+"' and enterpriseid = '"+strEnterpriseID+"' ";
 		
			dsStatusCode = (DataSet)dbCon.ExecuteQuery(strSQLQuery,ReturnType.DataSetType);
			return  dsStatusCode;
	
		}

		public static DataSet GetStatusCodeDS(String strAppID, String strEnterpriseID, String strFormID,String strUserID)
		{
			DataSet dsStatusCode = null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("SysDataMgrDAL","GetStatusCodeDS","EDB101","DbConnection object is null!!");
				return dsStatusCode;
			}

			String strSQLQuery = "select distinct status_code,status_description,Status_type,status_close,invoiceable,system_code,tt_display_status,tt_display_remarks,auto_process,auto_location,auto_remark,tt_display_location, scanning_type ,AllowRoles, isnull(AllowInSWB_Emu,'N') as AllowInSWB_Emu, isnull(AllowUPD_ByBatch,'N') as AllowUPD_ByBatch from Status_Code ";
			strSQLQuery +=" inner join  ( SELECT   distinct  Role_Master.role_name fROM  User_Role_Relation INNER JOIN Role_Master ON User_Role_Relation.applicationid = Role_Master.applicationid AND User_Role_Relation.enterpriseid = Role_Master.enterpriseid AND ";
			strSQLQuery +=" User_Role_Relation.roleid = Role_Master.roleid WHERE  (User_Role_Relation.userid = '"+ strUserID +"')) a on   status_code.allowRoles like '%' + a.role_name + '%' ";

			strSQLQuery += " where status_code.applicationid = '"+strAppID+"' and status_code.enterpriseid = '"+strEnterpriseID+"' ";
			
			if(strFormID=="ShipmentUpdate_ByBatch")
				strSQLQuery += " and (status_code.AllowUPD_ByBatch = 'Y' or status_code.AllowUPD_ByBatch = 'A')";

			if(strFormID=="SWB_ScanPkgs")
				strSQLQuery += " and status_code.AllowInSWB_Emu = 'Y'";

			strSQLQuery += " union ";
			strSQLQuery += " select status_code,status_description,Status_type,status_close,invoiceable, ";
			strSQLQuery += " system_code,tt_display_status,tt_display_remarks,auto_process,auto_location,auto_remark, ";
			strSQLQuery += " tt_display_location, scanning_type ,AllowRoles, isnull(AllowInSWB_Emu,'N') as AllowInSWB_Emu, ";
			strSQLQuery += " isnull(AllowUPD_ByBatch,'N') as AllowUPD_ByBatch from Status_Code where (AllowRoles is null or Rtrim(Ltrim(AllowRoles))='') ";
			if(strFormID=="ShipmentUpdate_ByBatch")
				strSQLQuery += " and (status_code.AllowUPD_ByBatch = 'Y') ";

			if(strFormID=="SWB_ScanPkgs")
				strSQLQuery += " and (status_code.AllowInSWB_Emu = 'Y') ";
			dsStatusCode = (DataSet)dbCon.ExecuteQuery(strSQLQuery,ReturnType.DataSetType);
			return  dsStatusCode;
	
		}


		public static SessionDS GetStatusCodeDS(String strAppID, String strEnterpriseID, int iCurrent, int iDSRecSize,DataSet dsQueryParam)
		{

			SessionDS sessionDS = null;


			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("SysDataMgrDAL","GetStatusCodeDS","EDB101","DbConnection object is null!!");
				return sessionDS;
			}

			StringBuilder strBuilder = new StringBuilder();
			strBuilder.Append("select status_code, status_description, Status_type, status_close, invoiceable, system_code,tt_display_status,tt_display_remarks,auto_process,auto_location,auto_remark,tt_display_location,  scanning_type  ,AllowRoles, isnull(AllowInSWB_Emu,'N') as AllowInSWB_Emu, isnull(AllowUPD_ByBatch,'N') as AllowUPD_ByBatch from Status_Code where applicationid = '");
			strBuilder.Append(strAppID);
			strBuilder.Append("' and enterpriseid = '");
			strBuilder.Append(strEnterpriseID);
			strBuilder.Append("' ");

			DataRow drEach = dsQueryParam.Tables[0].Rows[0];
				
			String strStatusCode = (String) drEach["status_code"];
			if((strStatusCode != null) && (strStatusCode != ""))
			{
				strBuilder.Append(" and status_code like '%");
				strBuilder.Append(strStatusCode);
				strBuilder.Append("%' ");

			}

			String strStatusDescription = (String) drEach["status_description"];
			if((strStatusDescription != null) && (strStatusDescription != ""))
			{
				strBuilder.Append(" and status_description like N'%");
				strBuilder.Append(Utility.ReplaceSingleQuote(strStatusDescription));
				strBuilder.Append("%' ");

			}
			
			String strStatusType = (String) drEach["Status_type"];
			if((strStatusType != null) && (strStatusType != ""))
			{
				strBuilder.Append(" and Status_type = '");
				strBuilder.Append(strStatusType);
				strBuilder.Append("' ");
			}

			String strInvoiceable = (String) drEach["invoiceable"];
			if((strInvoiceable != null) && (strInvoiceable != ""))
			{
				strBuilder.Append(" and invoiceable = '");
				strBuilder.Append(strInvoiceable);
				strBuilder.Append("' ");
			}

			String strCloseStatus = (String) drEach["status_close"];
			if((strCloseStatus != null) && (strCloseStatus != ""))
			{
				strBuilder.Append(" and status_close = '");
				strBuilder.Append(strCloseStatus);
				strBuilder.Append("' ");
			}


			//BY X FEB 11 08
			String tt_display_status = (String) drEach["tt_display_status"];
			if((tt_display_status != null) && (tt_display_status != ""))
			{
				strBuilder.Append(" and tt_display_status = '");
				strBuilder.Append(tt_display_status);
				strBuilder.Append("' ");
			}
			String tt_display_remarks = (String) drEach["tt_display_remarks"];
			if((tt_display_remarks != null) && (tt_display_remarks != ""))
			{
				strBuilder.Append(" and tt_display_remarks = '");
				strBuilder.Append(tt_display_remarks);
				strBuilder.Append("' ");
			}
			String auto_location = (String) drEach["auto_location"];
			if((auto_location != null) && (auto_location != ""))
			{
				strBuilder.Append(" and auto_location = '");
				strBuilder.Append(auto_location);
				strBuilder.Append("' ");
			}
			String tt_display_location = (String) drEach["tt_display_location"];
			if((tt_display_location != null) && (tt_display_location != ""))
			{
				strBuilder.Append(" and tt_display_location = '");
				strBuilder.Append(tt_display_location);
				strBuilder.Append("' ");
			}
			String auto_process = (String) drEach["auto_process"];
			if((strCloseStatus != null) && (auto_process != ""))
			{
				strBuilder.Append(" and auto_process = ");
				strBuilder.Append(auto_process);
				strBuilder.Append(" ");
			}
			String auto_remark = (String) drEach["auto_remark"];
			if((auto_remark != null) && (auto_remark != ""))
			{
				strBuilder.Append(" and auto_remark  like N'%");
				strBuilder.Append(Utility.ReplaceSingleQuote(auto_remark));
				strBuilder.Append("%' ");
			}

			String strscanning_type = (String) drEach["scanning_type"];
			if((strscanning_type != null) && (strscanning_type != ""))
			{
				strBuilder.Append(" and scanning_type = '");
				strBuilder.Append(strscanning_type);
				strBuilder.Append("' ");
			}

			
			//END BY X FEB 11 08
			String strAllowRoles = (String) drEach["AllowRoles"];
			if((strAllowRoles != null) && (strAllowRoles != ""))
			{
				strBuilder.Append(" and AllowRoles like N'%");
				strBuilder.Append(Utility.ReplaceSingleQuote(strAllowRoles));
				strBuilder.Append("%' ");
			}

			String strAllowInSWB_Emu = (String) drEach["AllowInSWB_Emu"];
			if((strAllowInSWB_Emu != null) && (strAllowInSWB_Emu != ""))
			{
				strBuilder.Append(" and AllowInSWB_Emu = '");
				strBuilder.Append(strAllowInSWB_Emu);
				strBuilder.Append("' ");
			}

			String strAllowUPD_ByBatch = (String) drEach["AllowUPD_ByBatch"];
			if((strAllowUPD_ByBatch != null) && (strAllowUPD_ByBatch != ""))
			{
				strBuilder.Append(" and AllowUPD_ByBatch = '");
				strBuilder.Append(strAllowUPD_ByBatch);
				strBuilder.Append("' ");
			}

			String strSQLQuery = strBuilder.ToString();			
			sessionDS = dbCon.ExecuteQuery(strSQLQuery,iCurrent,iDSRecSize,"StatusCodeTable");
			return  sessionDS;
	
		}

		public static int AddStatusCodeDS(String strAppID, String strEnterpriseID,DataSet dsToInsert)
		{
			int iRowsAffected = 0;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("SysDataMgrDAL","GetStatusCodeDS","EDB101","DbConnection object is null!!");
				return iRowsAffected;
			}

			StringBuilder strBuilder = new StringBuilder();
			strBuilder.Append("insert into Status_Code (applicationid,enterpriseid,status_code,status_description,status_type,tt_display_status,tt_display_remarks,auto_process,auto_location,auto_remark,tt_display_location,status_close,invoiceable,system_code,scanning_type, AllowRoles, AllowInSWB_Emu, AllowUPD_ByBatch) values ('");
			strBuilder.Append(strAppID+"','"+strEnterpriseID+"','");

			int iRowCnt = dsToInsert.Tables[0].Rows.Count;
			DataRow drEach = dsToInsert.Tables[0].Rows[0];
				
			String strStatusCode = (String) drEach["status_code"];			
			strBuilder.Append(strStatusCode);
			strBuilder.Append("',N'");

			String strStatusDescription = (String) drEach["status_description"];
			strBuilder.Append(Utility.ReplaceSingleQuote(strStatusDescription));
			strBuilder.Append("','");
			
			String strStatusType = (String) drEach["Status_Type"];
			strBuilder.Append(strStatusType);
			strBuilder.Append("','");

			//ADD BY X JAN 23 08
			String tt_display_status = (String) drEach["tt_display_status"];
			strBuilder.Append(tt_display_status);
			strBuilder.Append("','");

			String tt_display_remarks = (String) drEach["tt_display_remarks"];
			strBuilder.Append(tt_display_remarks);
			strBuilder.Append("',");

			int auto_process = int.Parse(drEach["auto_process"].ToString());
			strBuilder.Append(auto_process);
			strBuilder.Append(",'");

			String auto_location = (String) drEach["auto_location"];
			strBuilder.Append(auto_location);
			strBuilder.Append("','");

			String auto_remark = (String) drEach["auto_remark"];
			strBuilder.Append(auto_remark);
			strBuilder.Append("','");

			String tt_display_location = (String) drEach["tt_display_location"];
			strBuilder.Append(tt_display_location);
			strBuilder.Append("','");

			//END ADD BY X JAN 23 08

			String strCloseStatus = (String) drEach["status_close"];
			strBuilder.Append(strCloseStatus);
			strBuilder.Append("','");

			String strInvoiceable = (String) drEach["invoiceable"];
			strBuilder.Append(strInvoiceable);
			strBuilder.Append("','N','");

			String str_scanning_type = (String) drEach["scanning_type"];
			strBuilder.Append(str_scanning_type);
			strBuilder.Append("','");
			// for new column
			String strAllowRoles = (String) drEach["AllowRoles"];
			strBuilder.Append(Utility.ReplaceSemicolon(Utility.ReplaceSingleQuote(strAllowRoles)));
			strBuilder.Append("','");

			String strAllowInSWB_Emu = (String) drEach["AllowInSWB_Emu"];
			strBuilder.Append(strAllowInSWB_Emu);
			strBuilder.Append("','");

			String strAllowUPD_ByBatch = (String) drEach["AllowUPD_ByBatch"];
			strBuilder.Append(strAllowUPD_ByBatch);
			strBuilder.Append("')");

			String strSQLQuery = strBuilder.ToString();
			IDbCommand dbCmd = dbCon.CreateCommand(strSQLQuery,CommandType.Text);
			try
			{

				iRowsAffected = dbCon.ExecuteNonQuery(dbCmd);
				Logger.LogTraceInfo("SysDataMgrDAL","AddStatusCodeDS","SDM001",iRowsAffected+" rows inserted in to Status_Code table");
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("RBACManager","AddStatusCodeDS","SDM001","Error During Insert "+ appException.Message.ToString());
				throw new ApplicationException("Error inserting Status Code ",appException);
			}

			return iRowsAffected;

		}

		public static int ModifyStatusCodeDS(String strAppID, String strEnterpriseID,DataSet dsToModify)
		{
			int iRowsAffected = 0;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("SysDataMgrDAL","GetStatusCodeDS","EDB101","DbConnection object is null!!");
				return iRowsAffected;
			}
			int iRowCnt = dsToModify.Tables[0].Rows.Count;
			DataRow drEach = dsToModify.Tables[0].Rows[0];

			StringBuilder strBuilder = new StringBuilder();
			
			strBuilder.Append("update Status_Code set ");//, status_close, invoiceable,system_code) values ('");
			strBuilder.Append("status_description = N'");
			String strStatusDescription = (String) drEach["status_description"];
			strBuilder.Append(Utility.ReplaceSingleQuote(strStatusDescription));
			
			strBuilder.Append("', status_type = '");
			String strStatusType = (String) drEach["Status_Type"];
			strBuilder.Append(strStatusType);

			strBuilder.Append("', status_close = '");
			String strCloseStatus = (String) drEach["status_close"];
			strBuilder.Append(strCloseStatus);

			//ADD BY X JAN 23 08

			strBuilder.Append("', tt_display_status = '");
			String tt_display_status = (String) drEach["tt_display_status"];
			strBuilder.Append(tt_display_status);

			strBuilder.Append("', tt_display_remarks = '");
			String tt_display_remarks = (String) drEach["tt_display_remarks"];
			strBuilder.Append(tt_display_remarks);
			
			strBuilder.Append("', auto_process = ");
			int auto_process = int.Parse(drEach["auto_process"].ToString());
			strBuilder.Append(auto_process);
			
			strBuilder.Append(", auto_location = '");
			String auto_location = (String) drEach["auto_location"];
			strBuilder.Append(auto_location);
			
			strBuilder.Append("', auto_remark = '");
			String auto_remark = (String) drEach["auto_remark"];
			strBuilder.Append(auto_remark);
			
			strBuilder.Append("', tt_display_location = '");
			String tt_display_location = (String) drEach["tt_display_location"];
			strBuilder.Append(tt_display_location);

			strBuilder.Append("', scanning_type = '");
			String str_scanning_type = (String) drEach["scanning_type"];
			strBuilder.Append(str_scanning_type);

			//END ADD BY X JAN 23 08

			strBuilder.Append("',AllowRoles = N'");
			String strAllowRoles = (String) drEach["AllowRoles"];
			strBuilder.Append(Utility.ReplaceSemicolon(Utility.ReplaceSingleQuote(strAllowRoles)));
			
			strBuilder.Append("', AllowInSWB_Emu = '");
			String strAllowInSWB_Emu = (String) drEach["AllowInSWB_Emu"];
			strBuilder.Append(strAllowInSWB_Emu);

			strBuilder.Append("', AllowUPD_ByBatch = '");
			String strAllowUPD_ByBatch = (String) drEach["AllowUPD_ByBatch"];
			strBuilder.Append(strAllowUPD_ByBatch);



			strBuilder.Append("', invoiceable = '");
			String strInvoiceable = (String) drEach["invoiceable"];
			strBuilder.Append(strInvoiceable);
			strBuilder.Append("' where applicationid = '");
			strBuilder.Append(strAppID);
			strBuilder.Append("' and enterpriseid = '");
			strBuilder.Append(strEnterpriseID);
			strBuilder.Append("' and status_code = '");
			String strStatusCode = (String) drEach["status_code"];
			strBuilder.Append(strStatusCode);
			strBuilder.Append("'");

			String strSQLQuery = strBuilder.ToString();

			IDbCommand dbCmd = dbCon.CreateCommand(strSQLQuery,CommandType.Text);
			

			try
			{

				iRowsAffected = dbCon.ExecuteNonQuery(dbCmd);
				Logger.LogTraceInfo("SysDataMgrDAL","AddStatusCodeDS","SDM001",iRowsAffected+" rows inserted in to Status_Code table");
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("RBACManager","AddStatusCodeDS","SDM001","Error During Insert "+ appException.Message.ToString());
				throw new ApplicationException("Error inserting Status Code ",appException);
			}
			return iRowsAffected;

		}
		public static SessionDS GetEmptyStatusCodeDS(int iNumRows)
		{
			
			DataTable dtStatusCode = new DataTable();
 
			dtStatusCode.Columns.Add(new DataColumn("status_code", typeof(string)));
			dtStatusCode.Columns.Add(new DataColumn("status_description", typeof(string)));			
			dtStatusCode.Columns.Add(new DataColumn("Status_type", typeof(string)));
			dtStatusCode.Columns.Add(new DataColumn("status_close", typeof(string)));
			dtStatusCode.Columns.Add(new DataColumn("invoiceable", typeof(string)));
			dtStatusCode.Columns.Add(new DataColumn("system_code", typeof(string)));

			//BY X JAN 23 08
			dtStatusCode.Columns.Add(new DataColumn("tt_display_status", typeof(string)));
			dtStatusCode.Columns.Add(new DataColumn("tt_display_remarks", typeof(string)));
			dtStatusCode.Columns.Add(new DataColumn("auto_process", typeof(string)));
			dtStatusCode.Columns.Add(new DataColumn("auto_location", typeof(string)));
			dtStatusCode.Columns.Add(new DataColumn("auto_remark", typeof(string)));
			dtStatusCode.Columns.Add(new DataColumn("tt_display_location", typeof(string)));
			dtStatusCode.Columns.Add(new DataColumn("scanning_type", typeof(string)));
			//END BY X

			dtStatusCode.Columns.Add(new DataColumn("AllowRoles", typeof(string)));
			dtStatusCode.Columns.Add(new DataColumn("AllowInSWB_Emu", typeof(string)));
			dtStatusCode.Columns.Add(new DataColumn("AllowUPD_ByBatch", typeof(string)));



			for(int i=0; i < iNumRows; i++)
			{
				DataRow drEach = dtStatusCode.NewRow();
				drEach[0] = "";
				drEach[1] = "";				
				drEach[2] = "";
				drEach[3] = "";
				drEach[4] = "";
				drEach[5] = "";
				drEach["tt_display_status"] = "";
				drEach["tt_display_remarks"] = "";
				drEach["auto_process"] = "";
				drEach["auto_location"] = "";
				drEach["auto_remark"] = "";
				drEach["tt_display_location"] = "";
				drEach["scanning_type"] = "";
				drEach["AllowRoles"] = "";
				drEach["AllowInSWB_Emu"] = "";
				drEach["AllowUPD_ByBatch"] = "";

				dtStatusCode.Rows.Add(drEach);
			}

			DataSet dsStatusCode = new DataSet();
			dsStatusCode.Tables.Add(dtStatusCode);

			//			dsStatusCode.Tables[0].Columns["status_code"].Unique = true; //Checks duplicate records..
			dsStatusCode.Tables[0].Columns["status_code"].AllowDBNull = false;

			SessionDS sessionDS = new SessionDS();
			sessionDS.ds = dsStatusCode;
			sessionDS.DataSetRecSize = dsStatusCode.Tables[0].Rows.Count;
			sessionDS.QueryResultMaxSize = sessionDS.DataSetRecSize;

			return  sessionDS;
	
		}

		public static void AddNewRowInStatusCodeDS(SessionDS sessionDS)
		{
			DataRow drNew = sessionDS.ds.Tables[0].NewRow();

			drNew[0] = "";
			drNew[1] = "";			
			drNew[2] = "";
			drNew[3] = "";
			drNew[4] = "";
			drNew[5] = "";
			drNew["tt_display_status"] = "";
			drNew["tt_display_remarks"] = "";
			drNew["auto_process"] = "";
			drNew["auto_location"] = "";
			drNew["auto_remark"] = "";
			drNew["tt_display_location"] = "";
			drNew["scanning_type"] = "";
			drNew["AllowRoles"] = "";
			drNew["AllowInSWB_Emu"] = "";
			drNew["AllowUPD_ByBatch"] = "";
			try
			{
				sessionDS.ds.Tables[0].Rows.Add(drNew);
			}
			catch(Exception ex)
			{
				Logger.LogTraceError("SysDataMgrDAL","DeleteRouteCodeDS","R005",ex.Message.ToString());
			}			
			sessionDS.QueryResultMaxSize++;
		}


		public static DataSet GetExceptionCodeDS(String strAppID, String strEnterpriseID,String strStatusCode)
		{
			DataSet dsExceptionCode = null;

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("SysDataMgrDAL","GetStatusCodeDS","EDB101","DbConnection object is null!!");
				return dsExceptionCode;
			}

			String strSQLQuery = "select exception_code, exception_description, mbg, status_close, invoiceable from Exception_Code where ";
			strSQLQuery += "applicationid = '"+strAppID+"' and enterpriseid = '"+strEnterpriseID+"' and status_code = '"+strStatusCode+"'"; 
			
			dsExceptionCode = (DataSet)dbCon.ExecuteQuery(strSQLQuery,ReturnType.DataSetType);

			int cnt = dsExceptionCode.Tables[0].Rows.Count;
			return  dsExceptionCode;
	
		}

		public static SessionDS GetExceptionCodeDS(String strAppID, String strEnterpriseID,String strStatusCode,int iCurrent, int iDSRecSize)
		{
			SessionDS sessionDS = null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("SysDataMgrDAL","GetStatusCodeDS","EDB101","DbConnection object is null!!");
				return sessionDS;
			}

			String strSQLQuery = "select exception_code, exception_description, mbg, status_close, invoiceable, system_code from Exception_Code where ";
			strSQLQuery += "applicationid = '"+strAppID+"' and enterpriseid = '"+strEnterpriseID+"' and status_code = '"+strStatusCode+"'"; 
						
			sessionDS = dbCon.ExecuteQuery(strSQLQuery,iCurrent,iDSRecSize,"ExceptionCodeTable");
			return  sessionDS;	
		}

		public static int AddExceptionCodeDS(String strAppID, String strEnterpriseID,String strStatusCode,DataSet dsToInsert)
		{
			int iRowsAffected = 0;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("SysDataMgrDAL","AddExceptionCodeDS","EDB101","DbConnection object is null!!");
				return iRowsAffected;
			}

			StringBuilder strBuilder = new StringBuilder();
			strBuilder.Append("insert into Exception_Code (applicationid,enterpriseid,status_code, exception_code, exception_description, mbg, status_close, invoiceable,system_code) values ('");
			strBuilder.Append(strAppID);
			strBuilder.Append("','");
			strBuilder.Append(strEnterpriseID);
			strBuilder.Append("','");
			strBuilder.Append(strStatusCode);
			strBuilder.Append("','");

			int iRowCnt = dsToInsert.Tables[0].Rows.Count;

			DataRow drEach = dsToInsert.Tables[0].Rows[0];
				
			String strExceptionCode = (String) drEach["exception_code"];		
			strBuilder.Append(strExceptionCode);
			strBuilder.Append("',N'");

			String strExceptionDescription = (String) drEach["exception_description"];
			strBuilder.Append(Utility.ReplaceSingleQuote(strExceptionDescription));
			strBuilder.Append("','");

			String strMBG = (String) drEach["mbg"];
			strBuilder.Append(strMBG);
			strBuilder.Append("','");

			String strCloseStatus = (String) drEach["status_close"];
			strBuilder.Append(strCloseStatus);
			strBuilder.Append("','");

			String strInvoiceable = (String) drEach["invoiceable"];
			strBuilder.Append(strInvoiceable);
			strBuilder.Append("','N')");

			String strSQLQuery = strBuilder.ToString();
			IDbCommand dbCmd = dbCon.CreateCommand(strSQLQuery,CommandType.Text);			

			try
			{
				iRowsAffected = dbCon.ExecuteNonQuery(dbCmd);
				Logger.LogTraceInfo("SysDataMgrDAL","AddExceptionCodeDS","SDM001",iRowsAffected+" rows inserted in to Status_Code table");
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("RBACManager","AddExceptionCodeDS","SDM001","Error During Insert "+ appException.Message.ToString());
				throw new ApplicationException("Error inserting Status Code ",appException);
			}
			return iRowsAffected;

		}

		public static int ModifyExceptionCodeDS(String strAppID, String strEnterpriseID,String strStatusCode,DataSet dsToModify)
		{
			int iRowsAffected = 0;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("SysDataMgrDAL","ModifyExceptionCodeDS","EDB101","DbConnection object is null!!");
				return iRowsAffected;
			}
			int iRowCnt = dsToModify.Tables[0].Rows.Count;
			DataRow drEach = dsToModify.Tables[0].Rows[0];


			StringBuilder strBuilder = new StringBuilder();
			
			strBuilder.Append("update Exception_Code set ");
			strBuilder.Append("exception_description = N'");
			String strExceptionDescription = (String) drEach["exception_description"];
			strBuilder.Append(Utility.ReplaceSingleQuote(strExceptionDescription));
			strBuilder.Append("', mbg = '");
			String strMBG = (String) drEach["mbg"];
			strBuilder.Append(strMBG);
			strBuilder.Append("', status_close = '");
			String strCloseStatus = (String) drEach["status_close"];
			strBuilder.Append(strCloseStatus);
			strBuilder.Append("', invoiceable = '");
			String strInvoiceable = (String) drEach["invoiceable"];
			strBuilder.Append(strInvoiceable);
			strBuilder.Append("' where applicationid = '");
			strBuilder.Append(strAppID);
			strBuilder.Append("' and enterpriseid = '");
			strBuilder.Append(strEnterpriseID);
			strBuilder.Append("' and status_code = '");
			strBuilder.Append(strStatusCode);
			strBuilder.Append("' and exception_code = '");
			String strExceptionCode = (String) drEach["exception_code"];
			strBuilder.Append(strExceptionCode);
			strBuilder.Append("'");


			String strSQLQuery = strBuilder.ToString();

			IDbCommand dbCmd = dbCon.CreateCommand(strSQLQuery,CommandType.Text);
			
			try
			{
				iRowsAffected = dbCon.ExecuteNonQuery(dbCmd);
				Logger.LogTraceInfo("SysDataMgrDAL","ModifyExceptionCodeDS","SDM001",iRowsAffected+" rows inserted in to Exception_Code table");
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("RBACManager","ModifyExceptionCodeDS","SDM001","Error During Insert "+ appException.Message.ToString());
				throw new ApplicationException("Error inserting Status Code ",appException);
			}

			return iRowsAffected;
		}

		public static SessionDS GetEmptyExceptionCodeDS(int iNumRows)
		{
			DataTable dtExceptionCode = new DataTable();
 
			dtExceptionCode.Columns.Add(new DataColumn("exception_code", typeof(string)));
			dtExceptionCode.Columns.Add(new DataColumn("exception_description", typeof(string)));
			dtExceptionCode.Columns.Add(new DataColumn("mbg", typeof(string)));
			dtExceptionCode.Columns.Add(new DataColumn("status_close", typeof(string)));
			dtExceptionCode.Columns.Add(new DataColumn("invoiceable", typeof(string)));
			dtExceptionCode.Columns.Add(new DataColumn("system_code", typeof(string)));



			for(int i=0; i < iNumRows; i++)
			{
				DataRow drEach = dtExceptionCode.NewRow();
				drEach[0] = "";
				drEach[1] = "";
				drEach[2] = "";
				drEach[3] = "";
				drEach[4] = "";
				drEach[5] = "";

				dtExceptionCode.Rows.Add(drEach);
			}

			DataSet dsExceptionCode = new DataSet();
			dsExceptionCode.Tables.Add(dtExceptionCode);

			dsExceptionCode.Tables[0].Columns["exception_code"].Unique = true; //Checks duplicate records..
			dsExceptionCode.Tables[0].Columns["exception_code"].AllowDBNull = false;

			SessionDS sessionDS = new SessionDS();
			sessionDS.ds = dsExceptionCode;
			sessionDS.DataSetRecSize = dsExceptionCode.Tables[0].Rows.Count;
			sessionDS.QueryResultMaxSize = sessionDS.DataSetRecSize;

			return  sessionDS;	
		}

		public static void AddNewRowInExceptionCodeDS(SessionDS sessionDS)
		{
			DataRow drNew = sessionDS.ds.Tables[0].NewRow();

			drNew[0] = "";
			drNew[1] = "";
			drNew[2] = "";
			drNew[3] = "";
			drNew[4] = "";
			drNew[5] = "";

			try
			{
				sessionDS.ds.Tables[0].Rows.Add(drNew);
			}
			catch(Exception ex)
			{
				Logger.LogTraceError("SysDataMgrDAL","DeleteRouteCodeDS","R005",ex.Message.ToString());
			}			
			sessionDS.QueryResultMaxSize++;
		}

		public static int DeleteStatusCodeDS(String strAppID, String strEnterpriseID,String strStatusCode)
		{
			int iRowsDeleted = 0;

			DbConnection dbConApp = null;
			IDbCommand dbCommandApp = null;

			IDbTransaction transactionApp = null;
			IDbConnection conApp = null;

			dbConApp = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbConApp == null)
			{
				Logger.LogTraceError("SysDataMgrDAL","DeleteStatusCodeDS","SDM002","dbConApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			conApp = dbConApp.GetConnection();
			
			if(conApp == null)
			{
				Logger.LogTraceError("SysDataMgrDAL","DeleteStatusCodeDS","SDM002","conApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			try
			{
				dbConApp.OpenConnection(ref conApp);
			}
			catch(ApplicationException appException)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("SysDataMgrDAL","DeleteStatusCodeDS","SDM003","Error opening connection : "+ appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ",appException);
			}
			catch(Exception exception)
			{
				if(conApp != null)
				{
					conApp.Close();
				}

				Logger.LogTraceError("SysDataMgrDAL","DeleteStatusCodeDS","SDM003","Error opening connection : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}

			transactionApp = dbConApp.BeginTransaction(conApp,IsolationLevel.ReadCommitted);
			if(transactionApp == null)
			{
				Logger.LogTraceError("SysDataMgrDAL","DeleteStatusCodeDS","SDM002","transactionApp is null");
				throw new ApplicationException("Failed Begining a App Transaction..",null);
			}

			try
			{
				//Update record into core_enterprise Table

				StringBuilder strBuilder = new StringBuilder();
			
				strBuilder.Append("Delete from Exception_Code where ");
				strBuilder.Append(" applicationid = '");
				strBuilder.Append(strAppID);
				strBuilder.Append("' and enterpriseid = '");
				strBuilder.Append(strEnterpriseID);
				strBuilder.Append("' and status_code = '");
				strBuilder.Append(strStatusCode);
				strBuilder.Append("'");


				String strSQLQuery = strBuilder.ToString();
				dbCommandApp = dbConApp.CreateCommand(strSQLQuery,CommandType.Text); //Creating the command first time
				dbCommandApp.Connection = conApp;
				dbCommandApp.Transaction = transactionApp;

				iRowsDeleted = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);
				Logger.LogTraceInfo("RBACManager","DeleteStatusCodeDS","INF001",iRowsDeleted + " rows deleted from Exception_Code table");

				strBuilder = new StringBuilder();
			
				strBuilder.Append("Delete from Status_Code where ");
				strBuilder.Append(" applicationid = '");
				strBuilder.Append(strAppID);
				strBuilder.Append("' and enterpriseid = '");
				strBuilder.Append(strEnterpriseID);
				strBuilder.Append("' and status_code = '");
				strBuilder.Append(strStatusCode);
				strBuilder.Append("'");

				strSQLQuery = strBuilder.ToString();

				dbCommandApp.CommandText = strSQLQuery;

				iRowsDeleted = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);
				Logger.LogTraceInfo("RBACManager","DeleteStatusCodeDS","INF001",iRowsDeleted + " rows deleted from Status_Code table");
			
				//Commit application and core transaction.

				transactionApp.Commit();
				Logger.LogTraceInfo("SysDataMgrDAL","DeleteStatusCodeDS","SDM003","App db delete transaction committed.");

			}
			catch(ApplicationException appException)
			{				
				try
				{
					transactionApp.Rollback();
					Logger.LogTraceInfo("SysDataMgrDAL","DeleteStatusCodeDS","SDM002","App db delete transaction rolled back.");
				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("SysDataMgrDAL","DeleteStatusCodeDS","SDM002","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}

				Logger.LogTraceError("SysDataMgrDAL","DeleteStatusCodeDS","SDM002","Delete Transaction Failed : "+ appException.Message.ToString());
				throw new ApplicationException("Error deleting Status Code "+appException.Message,appException);
			}
			catch(Exception exception)
			{
				try
				{
					transactionApp.Rollback();
					Logger.LogTraceInfo("SysDataMgrDAL","DeleteStatusCodeDS","SDM002","App db delete transaction rolled back.");

				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("SysDataMgrDAL","DeleteStatusCodeDS","SDM002","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}

				Logger.LogTraceError("SysDataMgrDAL","DeleteStatusCodeDS","SDM004","delete Transaction Failed : "+ exception.Message.ToString());
				throw new ApplicationException("Error deleting Status Code "+exception.Message,exception);
			}
			finally
			{
				if(conApp != null)
				{
					conApp.Close();
				}
			}
			
			return iRowsDeleted;
		}

		public static int DeleteExceptionCodeDS(String strAppID, String strEnterpriseID,String strStatusCode,String strExceptionCode)
		{
			int iRowsDeleted = 0;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("SysDataMgrDAL","DeleteExceptionCodeRecord","EDB101","DbConnection object is null!!");
				return iRowsDeleted;
			}

			StringBuilder strBuilder = new StringBuilder();
			
			strBuilder.Append("Delete from Exception_Code where ");
			strBuilder.Append(" applicationid = '");
			strBuilder.Append(strAppID);
			strBuilder.Append("' and enterpriseid = '");
			strBuilder.Append(strEnterpriseID);
			strBuilder.Append("' and status_code = '");
			strBuilder.Append(strStatusCode);
			strBuilder.Append("' and exception_code = '");
			strBuilder.Append(strExceptionCode);
			strBuilder.Append("'");

			String strSQLQuery = strBuilder.ToString();

			IDbCommand dbCmd = dbCon.CreateCommand(strSQLQuery,CommandType.Text);
			
			try
			{
				iRowsDeleted = dbCon.ExecuteNonQuery(dbCmd);
				Logger.LogTraceInfo("SysDataMgrDAL","DeleteExceptionCodeRecord","SDM001",iRowsDeleted+" rows deleted from  Exception_Code table");
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("SysDataMgrDAL","DeleteExceptionCodeRecord","SDM001","Error During Delete "+ appException.Message.ToString());
				throw new ApplicationException("Error inserting Status Code : "+appException.Message,appException);
			}

			return iRowsDeleted;		
		}


		//Methods for Service Code - ZipCode Service code Excluded....
		public static SessionDS GetServiceCodeDS(String strAppID, String strEnterpriseID, int iCurrent, int iDSRecSize,DataSet dsQueryParam)
		{

			SessionDS sessionDS = null;

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("SysDataMgrDAL","GetServiceCodeDS","EDB101","DbConnection object is null!!");
				return sessionDS;
			}

			StringBuilder strBuilder = new StringBuilder();
			strBuilder.Append("select service_code, service_description, service_charge_percent,service_charge_amt, commit_time, transit_day,transit_hour,automanifest from Service where applicationid = '");
			strBuilder.Append(strAppID);
			strBuilder.Append("' and enterpriseid = '");
			strBuilder.Append(strEnterpriseID);
			strBuilder.Append("' ");

			DataRow drEach = dsQueryParam.Tables[0].Rows[0];
				
			String strServiceCode = (String) drEach["service_code"];
			if((strServiceCode != null) && (strServiceCode != ""))
			{
				strBuilder.Append(" and service_code like '%");
				strBuilder.Append(strServiceCode);
				strBuilder.Append("%' ");

			}

			String strServiceDescription = (String) drEach["service_description"];
			if((strServiceDescription != null) && (strServiceDescription != ""))
			{
				strBuilder.Append(" and service_description like N'%");
				strBuilder.Append(Utility.ReplaceSingleQuote(strServiceDescription));
				strBuilder.Append("%' ");
			}

			if(Utility.IsNotDBNull(drEach["service_charge_percent"]) && drEach["service_charge_percent"].ToString() !="")
			{
				decimal decPercentDiscount = (decimal) drEach["service_charge_percent"];
				strBuilder.Append(" and service_charge_percent = ");
				strBuilder.Append(decPercentDiscount);
				strBuilder.Append("  ");
			}

			if(Utility.IsNotDBNull(drEach["service_charge_amt"]) && drEach["service_charge_amt"].ToString() !="")
			{
				decimal decAmtDiscount = (decimal) drEach["service_charge_amt"];
				strBuilder.Append(" and service_charge_amt = ");
				strBuilder.Append(decAmtDiscount);
				strBuilder.Append("  ");
			}

			if(Utility.IsNotDBNull(drEach["commit_time"]) && drEach["commit_time"].ToString()!="")
			{
				DateTime dtCommitTime = (DateTime) drEach["commit_time"];
				strBuilder.Append(" and substring(convert(varchar(100),commit_time , 114),1,5) = '");
				String strTime = Utility.DateFormat(strAppID,strEnterpriseID,dtCommitTime,DTFormat.Time);
				strBuilder.Append(strTime.Substring(strTime.Length-6,5)+"'");
				strBuilder.Append(" ");
			}

			if(Utility.IsNotDBNull(drEach["transit_day"]) && drEach["transit_day"].ToString()!="")
			{
				int iTransitDay = System.Convert.ToInt32(drEach["transit_day"]);
				strBuilder.Append(" and transit_day = ");
				strBuilder.Append(iTransitDay);
				strBuilder.Append("  ");
			}

			if(Utility.IsNotDBNull(drEach["transit_hour"]) && drEach["transit_hour"].ToString()!="")
			{
				int iTransitHour = System.Convert.ToInt32(drEach["transit_hour"]);
				strBuilder.Append(" and transit_hour = ");
				strBuilder.Append(iTransitHour);
				strBuilder.Append("  ");
			}

			if(Utility.IsNotDBNull(drEach["automanifest"]) && drEach["automanifest"].ToString()!="")
			{
				string iAutoManifest = System.Convert.ToString(drEach["automanifest"]);
				strBuilder.Append(" and automanifest = '");
				strBuilder.Append(iAutoManifest+"'");
				strBuilder.Append("  ");
			}

			String strSQLQuery = strBuilder.ToString();			
			sessionDS = dbCon.ExecuteQuery(strSQLQuery,iCurrent,iDSRecSize,"ServiceCodeTable");

			return  sessionDS;
	
		}

		public static int AddServiceCodeDS(String strAppID, String strEnterpriseID,DataSet dsToInsert)
		{
			int iRowsAffected = 0;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("SysDataMgrDAL","GetServiceCodeDS","EDB101","DbConnection object is null!!");
				return iRowsAffected;
			}

			StringBuilder strBuilder = new StringBuilder();
			strBuilder.Append("insert into Service (applicationid,enterpriseid,service_code, service_description, service_charge_percent,service_charge_amt, commit_time,transit_day,transit_hour,automanifest) values ('");
			strBuilder.Append(strAppID);
			strBuilder.Append("','");
			strBuilder.Append(strEnterpriseID);
			strBuilder.Append("','");

			int iRowCnt = dsToInsert.Tables[0].Rows.Count;

			DataRow drEach = dsToInsert.Tables[0].Rows[0];
				
			String strServiceCode = (String) drEach["service_code"];
			strBuilder.Append(strServiceCode);
			strBuilder.Append("',N'");

			String strServiceDescription = (String) drEach["service_description"];
			strBuilder.Append(Utility.ReplaceSingleQuote(strServiceDescription));
			strBuilder.Append("',");

			if(Utility.IsNotDBNull(drEach["service_charge_percent"]) && drEach["service_charge_percent"].ToString() !="")
			{
				decimal decPercentDiscount = (decimal) drEach["service_charge_percent"];
				strBuilder.Append(decPercentDiscount);
				strBuilder.Append(",");
			}
			else
			{
				strBuilder.Append("null");
				strBuilder.Append(",");
			}
			if(Utility.IsNotDBNull(drEach["service_charge_amt"]) && drEach["service_charge_amt"].ToString() !="")
			{
				decimal decPercentDoller = (decimal) drEach["service_charge_amt"];
				strBuilder.Append(decPercentDoller);
				strBuilder.Append(",");
			}
			else
			{
				strBuilder.Append("null");
				strBuilder.Append(",");
			}			
			if(Utility.IsNotDBNull(drEach["commit_time"]) && drEach["commit_time"].ToString()!="")
			{
				DateTime dtCommitTime = (DateTime) drEach["commit_time"];
				String strTime = Utility.DateFormat(strAppID,strEnterpriseID,dtCommitTime,DTFormat.Time);
				strBuilder.Append(strTime);
				strBuilder.Append(",");
			}
			else
			{
				strBuilder.Append("null");
				strBuilder.Append(",");
			}

			if(Utility.IsNotDBNull(drEach["transit_day"]) && drEach["transit_day"].ToString()!="")
			{
				int iTransitDay = System.Convert.ToInt32(drEach["transit_day"]);
				strBuilder.Append(iTransitDay);
				strBuilder.Append(",");
			}
			else
			{
				strBuilder.Append("null");
				strBuilder.Append(",");
			}

			if(Utility.IsNotDBNull(drEach["transit_hour"]) && drEach["transit_hour"].ToString()!="")
			{
				int iTransitHour = System.Convert.ToInt32( drEach["transit_hour"]);
				strBuilder.Append(iTransitHour);
				strBuilder.Append(",'");

			}
			else
			{
				strBuilder.Append("null");
				strBuilder.Append(",'");
			}

			if(Utility.IsNotDBNull(drEach["automanifest"]) && drEach["automanifest"].ToString()!="")
			{
				string iAuntoManifest = System.Convert.ToString( drEach["automanifest"]);
				strBuilder.Append(iAuntoManifest);
				strBuilder.Append("')");

			}
			else
			{
				strBuilder.Append("null");
				strBuilder.Append("')");
			}

			String strSQLQuery = strBuilder.ToString();

			IDbCommand dbCmd = dbCon.CreateCommand(strSQLQuery,CommandType.Text);
			

			try
			{
				iRowsAffected = dbCon.ExecuteNonQuery(dbCmd);
				Logger.LogTraceInfo("SysDataMgrDAL","AddServiceCodeDS","SDM001",iRowsAffected+" rows inserted in to Service table");
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("RBACManager","AddServiceCodeDS","SDM001","Error During Insert "+ appException.Message.ToString());
				throw new ApplicationException("Error inserting Service Code ",appException);
			}



			return iRowsAffected;

		}

		public static int ModifyServiceCodeDS(String strAppID, String strEnterpriseID,DataSet dsToModify)
		{
			int iRowsAffected = 0;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("SysDataMgrDAL","GetServiceCodeDS","EDB101","DbConnection object is null!!");
				return iRowsAffected;
			}
			int iRowCnt = dsToModify.Tables[0].Rows.Count;
			DataRow drEach = dsToModify.Tables[0].Rows[0];


			StringBuilder strBuilder = new StringBuilder();
			
			strBuilder.Append("update Service set ");
			strBuilder.Append("service_description = N'");
			String strServiceDescription = (String) drEach["service_description"];
			strBuilder.Append(Utility.ReplaceSingleQuote(strServiceDescription));
			strBuilder.Append("', service_charge_percent = ");

			if(Utility.IsNotDBNull(drEach["service_charge_percent"]) && drEach["service_charge_percent"].ToString()!="")
			{
				decimal decPercentDiscount = (decimal) drEach["service_charge_percent"];
				strBuilder.Append(decPercentDiscount);
			}
			else
			{
				strBuilder.Append("null");
			}
			strBuilder.Append(", service_charge_amt = ");

			if(Utility.IsNotDBNull(drEach["service_charge_amt"]) && drEach["service_charge_amt"].ToString()!="")
			{
				decimal decPercentDoller = (decimal) drEach["service_charge_amt"];
				strBuilder.Append(decPercentDoller);
			}
			else
			{
				strBuilder.Append("null");
			}

			strBuilder.Append(", commit_time = ");

			if(Utility.IsNotDBNull(drEach["commit_time"]) && drEach["commit_time"].ToString() !="")
			{
				DateTime dtCommitTime = (DateTime) drEach["commit_time"];
				String strTime = Utility.DateFormat(strAppID,strEnterpriseID,dtCommitTime,DTFormat.Time);
				strBuilder.Append(strTime);
			}
			else
			{
				strBuilder.Append("null");
			}

			strBuilder.Append(", transit_day = ");

			if(Utility.IsNotDBNull(drEach["transit_day"]) && drEach["transit_day"].ToString() !="")
			{
				int iTransitDay = System.Convert.ToInt32(drEach["transit_day"]);
				strBuilder.Append(iTransitDay);
			}
			else
			{
				strBuilder.Append("null");
			}

			strBuilder.Append(", transit_hour = ");

			if(Utility.IsNotDBNull(drEach["transit_hour"]) && drEach["transit_hour"].ToString() !="")
			{
				int iTransitHour = System.Convert.ToInt32(drEach["transit_hour"]);
				strBuilder.Append(iTransitHour);
			}
			else
			{
				strBuilder.Append("null");
			}

			strBuilder.Append(", automanifest = '");

			if(Utility.IsNotDBNull(drEach["automanifest"]) && drEach["automanifest"].ToString() !="")
			{
				string iAutoManifest = System.Convert.ToString(drEach["automanifest"])+"'";
				strBuilder.Append(iAutoManifest);
			}
			else
			{
				strBuilder.Append("null");
			}

			strBuilder.Append(" where applicationid = '");
			strBuilder.Append(strAppID);
			strBuilder.Append("' and enterpriseid = '");
			strBuilder.Append(strEnterpriseID);
			strBuilder.Append("' and service_code = '");
			String strServiceCode = (String) drEach["service_code"];
			strBuilder.Append(strServiceCode);
			strBuilder.Append("'");

			String strSQLQuery = strBuilder.ToString();

			IDbCommand dbCmd = dbCon.CreateCommand(strSQLQuery,CommandType.Text);
			

			try
			{

				iRowsAffected = dbCon.ExecuteNonQuery(dbCmd);
				Logger.LogTraceInfo("SysDataMgrDAL","AddServiceCodeDS","SDM001",iRowsAffected+" rows inserted in to Service_Code table");
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("RBACManager","AddServiceCodeDS","SDM001","Error During Insert "+ appException.Message.ToString());
				throw new ApplicationException("Error inserting Service Code ",appException);
			}



			return iRowsAffected;

		}
		public static SessionDS GetEmptyServiceCodeDS(int iNumRows)
		{
			

			DataTable dtServiceCode = new DataTable();
 
			dtServiceCode.Columns.Add(new DataColumn("service_code", typeof(string)));
			dtServiceCode.Columns.Add(new DataColumn("service_description", typeof(string)));
			dtServiceCode.Columns.Add(new DataColumn("service_charge_percent", typeof(decimal)));
			dtServiceCode.Columns.Add(new DataColumn("service_charge_amt", typeof(decimal)));			
			dtServiceCode.Columns.Add(new DataColumn("commit_time", typeof(DateTime)));
			dtServiceCode.Columns.Add(new DataColumn("transit_day", typeof(int)));
			dtServiceCode.Columns.Add(new DataColumn("transit_hour", typeof(int)));
			dtServiceCode.Columns.Add(new DataColumn("automanifest", typeof(string)));


			for(int i=0; i < iNumRows; i++)
			{
				DataRow drEach = dtServiceCode.NewRow();
				drEach[0] = "";
				drEach[1] = "";
				//drEach[7] = "";

				dtServiceCode.Rows.Add(drEach);
			}

			DataSet dsServiceCode = new DataSet();
			dsServiceCode.Tables.Add(dtServiceCode);

			//			dsServiceCode.Tables[0].Columns["service_code"].Unique = true; //Checks duplicate records..
			dsServiceCode.Tables[0].Columns["service_code"].AllowDBNull = false;

			SessionDS sessionDS = new SessionDS();
			sessionDS.ds = dsServiceCode;
			sessionDS.DataSetRecSize = dsServiceCode.Tables[0].Rows.Count;
			sessionDS.QueryResultMaxSize = sessionDS.DataSetRecSize;

			return  sessionDS;
	
		}

		public static void AddNewRowInServiceCodeDS(SessionDS sessionDS)
		{
			DataRow drNew = sessionDS.ds.Tables[0].NewRow();

			drNew[0] = "";
			drNew[1] = "";
			//			drNew[7] = "";

			try
			{
				sessionDS.ds.Tables[0].Rows.Add(drNew);
			}
			catch(Exception ex)
			{
				Logger.LogTraceError("SysDataMgrDAL","DeleteRouteCodeDS","R005",ex.Message.ToString());
			}			
			sessionDS.QueryResultMaxSize++;

		}

		public static int DeleteServiceCodeDS(String strAppID, String strEnterpriseID,String strServiceCode)
		{
			int iRowsDeleted = 0;

			DbConnection dbConApp = null;
			IDbCommand dbCommandApp = null;

			IDbTransaction transactionApp = null;
			IDbConnection conApp = null;

	
			//Get App dbConnection

			dbConApp = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbConApp == null)
			{
				Logger.LogTraceError("SysDataMgrDAL","DeleteServiceCodeDS","SDM002","dbConApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			//Begin Core and App Transaction

			conApp = dbConApp.GetConnection();
			
			if(conApp == null)
			{
				Logger.LogTraceError("SysDataMgrDAL","DeleteServiceCodeDS","SDM002","conApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			try
			{
				dbConApp.OpenConnection(ref conApp);
			}
			catch(ApplicationException appException)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("SysDataMgrDAL","DeleteServiceCodeDS","SDM003","Error opening connection : "+ appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ",appException);
			}
			catch(Exception exception)
			{
				if(conApp != null)
				{
					conApp.Close();
				}

				Logger.LogTraceError("SysDataMgrDAL","DeleteServiceCodeDS","SDM003","Error opening connection : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}


			transactionApp = dbConApp.BeginTransaction(conApp,IsolationLevel.ReadCommitted);

			if(transactionApp == null)
			{
				Logger.LogTraceError("SysDataMgrDAL","DeleteServiceCodeDS","SDM002","transactionApp is null");
				throw new ApplicationException("Failed Begining a App Transaction..",null);
			}


			try
			{

				//Update record into core_enterprise Table

				StringBuilder strBuilder = new StringBuilder();
			
				strBuilder.Append("Delete from Zipcode_Service_Excluded where ");
				strBuilder.Append(" applicationid = '");
				strBuilder.Append(strAppID);
				strBuilder.Append("' and enterpriseid = '");
				strBuilder.Append(strEnterpriseID);
				strBuilder.Append("' and service_code = '");
				strBuilder.Append(strServiceCode);
				strBuilder.Append("'");


				String strSQLQuery = strBuilder.ToString();
				dbCommandApp = dbConApp.CreateCommand(strSQLQuery,CommandType.Text); //Creating the command first time
				dbCommandApp.Connection = conApp;
				dbCommandApp.Transaction = transactionApp;

				iRowsDeleted = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);
				Logger.LogTraceInfo("RBACManager","DeleteServiceCodeDS","INF001",iRowsDeleted + " rows deleted from Exception_Code table");


				strBuilder = new StringBuilder();
			
				strBuilder.Append("Delete from Service where ");
				strBuilder.Append(" applicationid = '");
				strBuilder.Append(strAppID);
				strBuilder.Append("' and enterpriseid = '");
				strBuilder.Append(strEnterpriseID);
				strBuilder.Append("' and service_code = '");
				strBuilder.Append(strServiceCode);
				strBuilder.Append("'");

				strSQLQuery = strBuilder.ToString();

				dbCommandApp.CommandText = strSQLQuery;


				iRowsDeleted = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);
				Logger.LogTraceInfo("RBACManager","DeleteServiceCodeDS","INF001",iRowsDeleted + " rows deleted from Service table");
			
				//Commit application and core transaction.

				transactionApp.Commit();
				Logger.LogTraceInfo("SysDataMgrDAL","DeleteServiceCodeDS","SDM003","App db delete transaction committed.");

			}
			catch(ApplicationException appException)
			{
				
				try
				{
					transactionApp.Rollback();
					Logger.LogTraceInfo("SysDataMgrDAL","DeleteServiceCodeDS","SDM002","App db delete transaction rolled back.");

				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("SysDataMgrDAL","DeleteServiceCodeDS","SDM002","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}


				Logger.LogTraceError("SysDataMgrDAL","DeleteServiceCodeDS","SDM002","Delete Transaction Failed : "+ appException.Message.ToString());
				throw new ApplicationException("Error deleting Service Code "+appException.Message,appException);
			}
			catch(Exception exception)
			{
				try
				{
					transactionApp.Rollback();
					Logger.LogTraceInfo("SysDataMgrDAL","DeleteServiceCodeDS","SDM002","App db delete transaction rolled back.");

				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("SysDataMgrDAL","DeleteServiceCodeDS","SDM002","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}

				Logger.LogTraceError("SysDataMgrDAL","DeleteServiceCodeDS","SDM004","delete Transaction Failed : "+ exception.Message.ToString());
				throw new ApplicationException("Error deleting Service Code "+exception.Message,exception);
			}
			finally
			{
				if(conApp != null)
				{
					conApp.Close();
				}
			}
			
			return iRowsDeleted;
		}


		///Zipcode-service code excluded methods..
		///
		public static SessionDS GetZipcodeServiceExcludedDS(String strAppID, String strEnterpriseID,String strServiceCode,int iCurrent, int iDSRecSize)
		{
			SessionDS sessionDS = null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("SysDataMgrDAL","GetStatusCodeDS","EDB101","DbConnection object is null!!");
				return sessionDS;
			}

			String strSQLQuery = "select zse.zipcode as zipcode, s.state_name as state_name, z.country as country from Zipcode_Service_Excluded zse, Zipcode z, State s where ";
			strSQLQuery += " zse.applicationid = z.applicationid and zse.enterpriseid = z.enterpriseid and zse.zipcode = z.zipcode and ";
			strSQLQuery += " z.applicationid = s.applicationid and z.enterpriseid = s.enterpriseid and z.country = s.country and z.state_code = s.state_code and ";
			strSQLQuery += " zse.applicationid = '"+strAppID+"' and zse.enterpriseid = '"+strEnterpriseID+"' and zse.service_code = '"+strServiceCode+"' and zse.country = s.country and zse.state_code = s.state_code "; 
			
			sessionDS = dbCon.ExecuteQuery(strSQLQuery,iCurrent,iDSRecSize,"ZipcodeServiceExcludedTable");

			return  sessionDS;
	
		}

		public static int AddZipcodeServiceExcludedDS(String strAppID, String strEnterpriseID,String strServiceCode,DataSet dsToInsert)
		{
			int iRowsAffected = 0;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("SysDataMgrDAL","AddZipcodeServiceExcludedDS","EDB101","DbConnection object is null!!");
				return iRowsAffected;
			}

			StringBuilder strBuilder = new StringBuilder();
			strBuilder.Append("insert into Zipcode_Service_Excluded (applicationid,enterpriseid,service_code, zipcode ) values ('");
			strBuilder.Append(strAppID);
			strBuilder.Append("','");
			strBuilder.Append(strEnterpriseID);
			strBuilder.Append("','");
			strBuilder.Append(strServiceCode);
			strBuilder.Append("','");

			int iRowCnt = dsToInsert.Tables[0].Rows.Count;

			DataRow drEach = dsToInsert.Tables[0].Rows[0];
				
			String strZipcode = (String) drEach["zipcode"];
			strBuilder.Append(strZipcode);
			strBuilder.Append("')");

			String strSQLQuery = strBuilder.ToString();

			IDbCommand dbCmd = dbCon.CreateCommand(strSQLQuery,CommandType.Text);
			

			try
			{

				iRowsAffected = dbCon.ExecuteNonQuery(dbCmd);
				Logger.LogTraceInfo("SysDataMgrDAL","AddZipcodeServiceExcludedDS","SDM001",iRowsAffected+" rows inserted in to Status_Code table");
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("RBACManager","AddZipcodeServiceExcludedDS","SDM001","Error During Insert "+ appException.Message.ToString());
				throw new ApplicationException("Error inserting Status Code ",appException);
			}
			return iRowsAffected;

		}

		public static SessionDS GetEmptyZipcodeServiceExcludedDS(int iNumRows)
		{
			DataTable dtZipcodeServiceExcluded = new DataTable();
 
			dtZipcodeServiceExcluded.Columns.Add(new DataColumn("zipcode", typeof(string)));
			dtZipcodeServiceExcluded.Columns.Add(new DataColumn("state_name", typeof(string)));
			dtZipcodeServiceExcluded.Columns.Add(new DataColumn("country", typeof(string)));

			for(int i=0; i < iNumRows; i++)
			{
				DataRow drEach = dtZipcodeServiceExcluded.NewRow();
				drEach[0] = "";
				drEach[1] = "";
				drEach[2] = "";

				dtZipcodeServiceExcluded.Rows.Add(drEach);
			}

			DataSet dsZipcodeServiceExcluded = new DataSet();
			dsZipcodeServiceExcluded.Tables.Add(dtZipcodeServiceExcluded);

			dsZipcodeServiceExcluded.Tables[0].Columns["zipcode"].Unique = true; //Checks duplicate records..
			dsZipcodeServiceExcluded.Tables[0].Columns["zipcode"].AllowDBNull = false;

			SessionDS sessionDS = new SessionDS();
			sessionDS.ds = dsZipcodeServiceExcluded;
			sessionDS.DataSetRecSize = dsZipcodeServiceExcluded.Tables[0].Rows.Count;
			sessionDS.QueryResultMaxSize = sessionDS.DataSetRecSize;


			return  sessionDS;
	
		}

		public static void AddNewRowInZipcodeServiceExcludedDS(SessionDS sessionDS)
		{
			DataRow drNew = sessionDS.ds.Tables[0].NewRow();

			drNew[0] = "";
			drNew[1] = "";
			drNew[2] = "";
			try
			{
				sessionDS.ds.Tables[0].Rows.Add(drNew);
			}
			catch(Exception ex)
			{
				Logger.LogTraceError("SysDataMgrDAL","DeleteRouteCodeDS","R005",ex.Message.ToString());
			}
			sessionDS.QueryResultMaxSize++;
		}

		public static int DeleteZipcodeServiceExcludedDS(String strAppID, String strEnterpriseID,String strServiceCode,String strZipcode)
		{
			int iRowsDeleted = 0;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("SysDataMgrDAL","DeleteExceptionCodeRecord","EDB101","DbConnection object is null!!");
				return iRowsDeleted;
			}

			StringBuilder strBuilder = new StringBuilder();
			
			strBuilder.Append("Delete from Zipcode_Service_Excluded where ");
			strBuilder.Append(" applicationid = '");
			strBuilder.Append(strAppID);
			strBuilder.Append("' and enterpriseid = '");
			strBuilder.Append(strEnterpriseID);
			strBuilder.Append("' and service_code = '");
			strBuilder.Append(strServiceCode);
			strBuilder.Append("' and zipcode = '");
			strBuilder.Append(strZipcode);
			strBuilder.Append("'");

			String strSQLQuery = strBuilder.ToString();

			IDbCommand dbCmd = dbCon.CreateCommand(strSQLQuery,CommandType.Text);
			
			try
			{
				iRowsDeleted = dbCon.ExecuteNonQuery(dbCmd);
				Logger.LogTraceInfo("SysDataMgrDAL","DeleteExceptionCodeRecord","SDM001",iRowsDeleted+" rows deleted from  Exception_Code table");
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("SysDataMgrDAL","DeleteExceptionCodeRecord","SDM001","Error During Delete "+ appException.Message.ToString());
				throw new ApplicationException("Error inserting Status Code : "+appException.Message,appException);
			}

			return iRowsDeleted;		
		}


		//Methods for VAS Code - ZipCode VAS Excluded....

		public static SessionDS GetVASCodeDS(String strAppID, String strEnterpriseID, int iCurrent, int iDSRecSize,DataSet dsQueryParam)
		{

			SessionDS sessionDS = null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("SysDataMgrDAL","GetVASCodeDS","EDB101","DbConnection object is null!!");
				return sessionDS;
			}

			StringBuilder strBuilder = new StringBuilder();
			strBuilder.Append(@"select vas_code, vas_description, surcharge, allow_for_customer from  VAS where applicationid = '");
			strBuilder.Append(strAppID);
			strBuilder.Append("' and enterpriseid = '");
			strBuilder.Append(strEnterpriseID);
			strBuilder.Append("' ");

			DataRow drEach = dsQueryParam.Tables[0].Rows[0];
				
			String strVASCode = (String) drEach["vas_code"];
			if((strVASCode != null) && (strVASCode != ""))
			{
				strBuilder.Append(" and vas_code like '%");
				strBuilder.Append(strVASCode);
				strBuilder.Append("%' ");

			}

			String strVASDescription = (String) drEach["vas_description"];
			if((strVASDescription != null) && (strVASDescription != ""))
			{
				strBuilder.Append(" and vas_description like N'%");
				strBuilder.Append(Utility.ReplaceSingleQuote(strVASDescription));
				strBuilder.Append("%' ");

			}

			if(Utility.IsNotDBNull(drEach["surcharge"]) && drEach["surcharge"].ToString()!="")
			{
				decimal decSurcharge = (decimal) drEach["surcharge"];

				strBuilder.Append(" and surcharge = ");
				strBuilder.Append(decSurcharge);
				strBuilder.Append("  ");
			}

			if(Utility.IsNotDBNull(drEach["allow_for_customer"]) && drEach["allow_for_customer"].ToString()!="")
			{
				strBuilder.Append(" and allow_for_customer = '");
				strBuilder.Append(drEach["allow_for_customer"].ToString());
				strBuilder.Append("' ");
			}
			

			String strSQLQuery = strBuilder.ToString();			
			sessionDS = dbCon.ExecuteQuery(strSQLQuery,iCurrent,iDSRecSize,"VASCodeTable");
			return  sessionDS;
	
		}
		
		public static int AddCustomerProfile_BillPlacementRules(String strAppID,String strEnterpriseID,DataTable dtBillPlacementRules)
		{
			int iRowsAffected = 0;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("SysDataMgrDAL","AddCustomerProfile_BillPlacementRules","EDB101","DbConnection object is null!!");
				return iRowsAffected;
			}

			StringBuilder strBuilder = new StringBuilder();
			strBuilder.Append("insert into Customer_BillPlacementRules (applicationid,enterpriseid,custid,placement_frequency,");
			strBuilder.Append("weekly_days,weekly_holiday,semi_monthly_condition,semi_monthly_condition_every,");
			strBuilder.Append("semi_monthly_condition_day,semi_monthly_daystart,semi_monthly_dayend,semi_monthly_holiday,");
			strBuilder.Append("monthly_condition,monthly_condition_every,monthly_condition_day,");
			strBuilder.Append("monthly_daystart,monthly_dayend,monthly_holiday,last_updated,last_updated_by,remark)");
			strBuilder.Append("values ('");
			strBuilder.Append(strAppID);
			strBuilder.Append("','");
			strBuilder.Append(strEnterpriseID);
			strBuilder.Append("','");

			DataRow drEach = dtBillPlacementRules.Rows[0];
			String strCusid = (String)drEach["custid"];
			strBuilder.Append(Utility.ReplaceSingleQuote(strCusid)+"',");
			if(Utility.IsNotDBNull(drEach["placement_frequency"]) && drEach["placement_frequency"].ToString()!="")
			{
				byte iPlacementFrequency = (byte)drEach["placement_frequency"];
				strBuilder.Append(iPlacementFrequency+",");
			}
			else
			{
				strBuilder.Append("null");
				strBuilder.Append(",");

			}
			if(Utility.IsNotDBNull(drEach["weekly_days"]) && drEach["weekly_days"].ToString()!="")
			{
				strBuilder.Append("'");
				String strWeeklyDays = (String)drEach["weekly_days"];
				strBuilder.Append(Utility.ReplaceSingleQuote(strWeeklyDays)+"',");
			}
			else
			{
				strBuilder.Append("null");
				strBuilder.Append(",");

			}
			if(Utility.IsNotDBNull(drEach["weekly_holiday"]) && drEach["weekly_holiday"].ToString()!="")
			{
				byte iWeeklyHoliday = (byte)drEach["weekly_holiday"];
				strBuilder.Append(iWeeklyHoliday+",");
			}
			else
			{
				strBuilder.Append("null");
				strBuilder.Append(",");

			}
			if(Utility.IsNotDBNull(drEach["semi_monthly_condition"]) && drEach["semi_monthly_condition"].ToString()!="")
			{
				int sSemiMonthlyCondition = Convert.ToInt16((bool)drEach["semi_monthly_condition"]);
				strBuilder.Append(sSemiMonthlyCondition+",");
			}
			else
			{
				strBuilder.Append("null");
				strBuilder.Append(",");

			}
			if(Utility.IsNotDBNull(drEach["semi_monthly_condition_every"]) && drEach["semi_monthly_condition_every"].ToString()!="")
			{
				byte iSemiMonthlyConditionEvery = (byte)drEach["semi_monthly_condition_every"];
				strBuilder.Append(iSemiMonthlyConditionEvery+",");
			}
			else
			{
				strBuilder.Append("null");
				strBuilder.Append(",");

			}
			if(Utility.IsNotDBNull(drEach["semi_monthly_condition_day"]) && drEach["semi_monthly_condition_day"].ToString()!="")
			{
				byte iSemiMonthlyConditionDay = (byte)drEach["semi_monthly_condition_day"];
				strBuilder.Append(iSemiMonthlyConditionDay+",");
			}
			else
			{
				strBuilder.Append("null");
				strBuilder.Append(",");

			}
			if(Utility.IsNotDBNull(drEach["semi_monthly_daystart"]) && drEach["semi_monthly_daystart"].ToString()!="")
			{
				byte iSemiMonthlyDayStart = (byte)drEach["semi_monthly_daystart"];
				strBuilder.Append(iSemiMonthlyDayStart+",");
			}
			else
			{
				strBuilder.Append("null");
				strBuilder.Append(",");

			}
			if(Utility.IsNotDBNull(drEach["semi_monthly_dayend"]) && drEach["semi_monthly_dayend"].ToString()!="")
			{
				byte iSemiMonthlyDayEnd = (byte)drEach["semi_monthly_dayend"];
				strBuilder.Append(iSemiMonthlyDayEnd+",");
			}
			else
			{
				strBuilder.Append("null");
				strBuilder.Append(",");

			}
			if(Utility.IsNotDBNull(drEach["semi_monthly_holiday"]) && drEach["semi_monthly_holiday"].ToString()!="")
			{
				byte iSemiMonthlyHoliday = (byte)drEach["semi_monthly_holiday"];
				strBuilder.Append(iSemiMonthlyHoliday+",");
			}
			else
			{
				strBuilder.Append("null");
				strBuilder.Append(",");

			}
			if(Utility.IsNotDBNull(drEach["monthly_condition"]) && drEach["monthly_condition"].ToString()!="")
			{
				int sMonthlyCondition = Convert.ToInt16((bool)drEach["monthly_condition"]);
				strBuilder.Append(sMonthlyCondition+",");
			}
			else
			{
				strBuilder.Append("null");
				strBuilder.Append(",");

			}
			if(Utility.IsNotDBNull(drEach["monthly_condition_every"]) && drEach["monthly_condition_every"].ToString()!="")
			{
				byte iMonthlyConditionEvery = (byte)drEach["monthly_condition_every"];
				strBuilder.Append(iMonthlyConditionEvery+",");
			}
			else
			{
				strBuilder.Append("null");
				strBuilder.Append(",");

			}
			if(Utility.IsNotDBNull(drEach["monthly_condition_day"]) && drEach["monthly_condition_day"].ToString()!="")
			{
				byte iMonthlyConditionDay = (byte)drEach["monthly_condition_day"];
				strBuilder.Append(iMonthlyConditionDay+",");
			}
			else
			{
				strBuilder.Append("null");
				strBuilder.Append(",");

			}
			if(Utility.IsNotDBNull(drEach["monthly_daystart"]) && drEach["monthly_daystart"].ToString()!="")
			{
				byte iMonthlyDayStart = (byte)drEach["monthly_daystart"];
				strBuilder.Append(iMonthlyDayStart+",");
			}
			else
			{
				strBuilder.Append("null");
				strBuilder.Append(",");

			}
			if(Utility.IsNotDBNull(drEach["monthly_dayend"]) && drEach["monthly_dayend"].ToString()!="")
			{
				byte iMonthlyDayEnd = (byte)drEach["monthly_dayend"];
				strBuilder.Append(iMonthlyDayEnd+",");
			}
			else
			{
				strBuilder.Append("null");
				strBuilder.Append(",");

			}
			if(Utility.IsNotDBNull(drEach["monthly_holiday"]) && drEach["monthly_holiday"].ToString()!="")
			{
				byte iMonthlyHoliday = (byte)drEach["monthly_holiday"];
				strBuilder.Append(iMonthlyHoliday+",");
			}
			else
			{
				strBuilder.Append("null");
				strBuilder.Append(",");

			}
			if(Utility.IsNotDBNull(drEach["last_updated"]) && drEach["last_updated"].ToString()!="")
			{
				strBuilder.Append("'");
				DateTime dtLastUpdate = (DateTime)drEach["last_updated"];
				strBuilder.Append(dtLastUpdate+"',");
			}
			else
			{
				strBuilder.Append("null");
				strBuilder.Append(",");

			}
			if(Utility.IsNotDBNull(drEach["last_updated_by"]) && drEach["last_updated_by"].ToString()!="")
			{
				strBuilder.Append("'");
				String strLastUpdateBy = (String)drEach["last_updated_by"];
				strBuilder.Append(Utility.ReplaceSingleQuote(strLastUpdateBy)+"',");
			}
			else
			{
				strBuilder.Append("null)");
			}
			if(Utility.IsNotDBNull(drEach["remark"]) && drEach["remark"].ToString()!="")
			{
				strBuilder.Append("N'");
				String strRemark = (String)drEach["remark"];
				strBuilder.Append(Utility.ReplaceSingleQuote(strRemark)+"')");
			}
			else
			{
				strBuilder.Append("null)");
			}
						
			String strSQLQuery = strBuilder.ToString();
			IDbCommand dbCmd = dbCon.CreateCommand(strSQLQuery,CommandType.Text);

			try
			{

				iRowsAffected = dbCon.ExecuteNonQuery(dbCmd);
				Logger.LogTraceInfo("SysDataMgrDAL","AddCustomerProfile_BillPlacementRules","SDM001",iRowsAffected+" rows inserted in to Customer_BillPlacementRules table");
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("RBACManager","AddCustomerProfile_BillPlacementRules","SDM001","Error During Insert "+ appException.Message.ToString());
				throw new ApplicationException("Error inserting Bill Placement Rule Code ",appException);
			}
			return iRowsAffected;
		}
		public static int DeleteCustomerProfile_BillPlacementRules(String strAppID,String strEnterpriseID,String strCusid)
		{
			int iRowsAffected = 0;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("SysDataMgrDAL","AddCustomerProfile_BillPlacementRules","EDB101","DbConnection object is null!!");
				return iRowsAffected;
			}
			StringBuilder strBuilder = new StringBuilder();
			strBuilder.Append("Delete from Customer_BillPlacementRules ");
			strBuilder.Append("Where applicationid='");
			strBuilder.Append(strAppID);
			strBuilder.Append("' and enterpriseid='");
			strBuilder.Append(strEnterpriseID);
			strBuilder.Append("' and custid='");
			strBuilder.Append(strCusid);
			strBuilder.Append("'");

			String strSQLQuery = strBuilder.ToString();
			IDbCommand dbCmd = dbCon.CreateCommand(strSQLQuery,CommandType.Text);			

			try
			{

				iRowsAffected = dbCon.ExecuteNonQuery(dbCmd);
				Logger.LogTraceInfo("SysDataMgrDAL","ModifyCustomerProfile_BillPlacementRules","SDM001",iRowsAffected+" rows update Customer_BillPlacementRules table");
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("RBACManager","ModifyCustomerProfile_BillPlacementRules","SDM001","Error During Update "+ appException.Message.ToString());
				throw new ApplicationException("Error updating Customer_BillPlacementRules ",appException);
			}
			return iRowsAffected;
		}
		public static int ModifyCustomerProfile_BillPlacementRules(String strAppID,String strEnterpriseID,DataSet dsBillPlacementRules)
		{
			int iRowsAffected = 0;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("SysDataMgrDAL","AddCustomerProfile_BillPlacementRules","EDB101","DbConnection object is null!!");
				return iRowsAffected;
			}
			StringBuilder strBuilder = new StringBuilder();

			DataRow drEach = dsBillPlacementRules.Tables[0].Rows[0];
			String strCusid = (String)drEach["custid"];
			int iPlacementFrequency = (int)drEach["placement_frequency"];
			String strWeeklyDays = (String)drEach["weekly_days"];
			int iWeeklyHoliday = (int)drEach["weekly_holiday"];
			short sSemiMonthlyCondition = (short)drEach["semi_monthly_condition"];
			int iSemiMonthlyConditionEvery = (int)drEach["semi_monthly_condition_every"];
			int iSemiMonthlyConditionDay = (int)drEach["semi_montly_condition_day"];
			int iSemiMonthlyDayStart = (int)drEach["semi_monthly_daystart"];
			int iSemiMonthlyDayEnd = (int)drEach["semi_monthly_dayend"];
			int iSemiMonthlyHoliday = (int)drEach["semi_monthly_holiday"];
			short sMonthlyCondition = (short)drEach["monthly_condition"];
			int iMonthlyConditionEvery = (int)drEach["monthly_condition_every"];
			int iMonthlyConditionDay = (int)drEach["monthly_condition_day"];
			int iMonthlyDayStart = (int)drEach["monthly_daystart"];
			int iMonthlyDayEnd = (int)drEach["monthly_dayend"];
			int iMonthlyHoliday = (int)drEach["monthly_holiday"];
			DateTime dtLastUpdate = (DateTime)drEach["last_updated"];
			String strLastUpdateBy = (String)drEach["last_updated_by"];

			strBuilder.Append("update Customer_BillPlacementRules set ");
			strBuilder.Append("placement_frequency=");
			strBuilder.Append(iPlacementFrequency);
			strBuilder.Append(",weekly_days='");
			strBuilder.Append(Utility.ReplaceSingleQuote(strWeeklyDays));
			strBuilder.Append("',weekly_holiday=");
			strBuilder.Append(iWeeklyHoliday);
			strBuilder.Append(",semi_monthly_condition=");
			strBuilder.Append(sSemiMonthlyCondition);
			strBuilder.Append(",semi_monthly_condition_every=");
			strBuilder.Append(iSemiMonthlyConditionEvery);
			strBuilder.Append(",semi_montly_condition_day=");
			strBuilder.Append(iSemiMonthlyConditionDay);
			strBuilder.Append(",semi_monthly_daystart=");
			strBuilder.Append(iSemiMonthlyDayStart);
			strBuilder.Append(",semi_monthly_dayend=");
			strBuilder.Append(iSemiMonthlyDayEnd);
			strBuilder.Append(",semi_monthly_holiday=");
			strBuilder.Append(iSemiMonthlyHoliday);
			strBuilder.Append(",monthly_condition=");
			strBuilder.Append(sMonthlyCondition);
			strBuilder.Append(",monthly_condition_every=");
			strBuilder.Append(iMonthlyConditionEvery);
			strBuilder.Append(",monthly_condition_day=");
			strBuilder.Append(iMonthlyConditionDay);
			strBuilder.Append(",monthly_daystart=");
			strBuilder.Append(iMonthlyDayStart);
			strBuilder.Append(",monthly_dayend=");
			strBuilder.Append(iMonthlyDayEnd);
			strBuilder.Append(",monthly_holiday=");
			strBuilder.Append(iMonthlyHoliday);
			strBuilder.Append(",last_updated=");
			strBuilder.Append(dtLastUpdate);
			strBuilder.Append(",last_updated_by='");
			strBuilder.Append(Utility.ReplaceSingleQuote(strLastUpdateBy));
			strBuilder.Append("' Where applicationid='");
			strBuilder.Append(strAppID);
			strBuilder.Append("' and enterpriseid='");
			strBuilder.Append(strEnterpriseID);
			strBuilder.Append("' and custid='");
			strBuilder.Append(strCusid);
			strBuilder.Append("'");

			String strSQLQuery = strBuilder.ToString();
			IDbCommand dbCmd = dbCon.CreateCommand(strSQLQuery,CommandType.Text);			

			try
			{

				iRowsAffected = dbCon.ExecuteNonQuery(dbCmd);
				Logger.LogTraceInfo("SysDataMgrDAL","ModifyCustomerProfile_BillPlacementRules","SDM001",iRowsAffected+" rows update Customer_BillPlacementRules table");
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("RBACManager","ModifyCustomerProfile_BillPlacementRules","SDM001","Error During Update "+ appException.Message.ToString());
				throw new ApplicationException("Error updating Customer_BillPlacementRules ",appException);
			}



			return iRowsAffected;
		}

		
		//Jeab  09 Mar 2012
		public static int AddCommissionData(String strAppID,String strEnterpriseID,DataSet dsCommission ,String strUser )
		{
			int iRowsAffected = 0;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("SysDataMgrDAL","AddCommissionData","EDB101","DbConnection object is null!!");
				return iRowsAffected;
			}
			StringBuilder strBuilder = new StringBuilder();

			DataRow drEach = dsCommission.Tables[0].Rows[0];			
			String strCusid = (String)drEach["custid"];
			String[] strComDT = drEach["Commission_datetime"].ToString().Split('/');
			String strY = strComDT[2].Substring(0,4);
			String strM = strComDT[0];
			String strD = strComDT[1];
			String strComDate =  strM + "/" + strD + "/" + strY;
			String strUserid = strUser;
			DateTime dtCreated = DateTime.Now;			

			strBuilder.Append("Insert  into  tb_Customer_Commission(applicationid ,enterpriseid ,custid ,commission_datetime ,created_userid ,created_datetime) ");
			strBuilder.Append("Values('");
			strBuilder.Append(strAppID);
			strBuilder.Append("' ,'");
			strBuilder.Append(strEnterpriseID);
			strBuilder.Append("' ,'");
			strBuilder.Append(strCusid);
			strBuilder.Append("' ,'");
			strBuilder.Append(strComDate);  
			strBuilder.Append("' ,'");
			strBuilder.Append(strUserid);
			strBuilder.Append("' ,'");
			strBuilder.Append(dtCreated);

			strBuilder.Append("')");

			String strSQLQuery = strBuilder.ToString();
			IDbCommand dbCmd = dbCon.CreateCommand(strSQLQuery,CommandType.Text);			

			try
			{

				iRowsAffected = dbCon.ExecuteNonQuery(dbCmd);
				Logger.LogTraceInfo("SysDataMgrDAL","AddCommissionData","SDM001",iRowsAffected+" rows update tb_Customer_Commission table");
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("RBACManager","AddCommissionData","SDM001","Error During Update "+ appException.Message.ToString());
				throw new ApplicationException("Error inserting tb_Customer_Commission ",appException);
			}

			return iRowsAffected;
		}
		//Jeab 09 Mar 2012  =========>  End

		public static int AddVASCodeDS(String strAppID, String strEnterpriseID,DataSet dsToInsert)
		{
			int iRowsAffected = 0;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("SysDataMgrDAL","GetVASCodeDS","EDB101","DbConnection object is null!!");
				return iRowsAffected;
			}

			StringBuilder strBuilder = new StringBuilder();
			strBuilder.Append("insert into VAS (applicationid,enterpriseid,vas_code, vas_description, surcharge, allow_for_customer) values ('");
			strBuilder.Append(strAppID);
			strBuilder.Append("','");
			strBuilder.Append(strEnterpriseID);
			strBuilder.Append("','");

			int iRowCnt = dsToInsert.Tables[0].Rows.Count;

			DataRow drEach = dsToInsert.Tables[0].Rows[0];
				
			String strVASCode = (String) drEach["vas_code"];
			strBuilder.Append(strVASCode);
			strBuilder.Append("',N'");

			String strVASDescription = (String) drEach["vas_description"];
			strBuilder.Append(Utility.ReplaceSingleQuote(strVASDescription));
			strBuilder.Append("',");
			
			if(Utility.IsNotDBNull(drEach["surcharge"]) &&  drEach["surcharge"].ToString() !="")
			{
				decimal decSurcharge = (decimal) drEach["surcharge"];			
				strBuilder.Append(decSurcharge);
			}
			else
			{
				strBuilder.Append("null");				
			}
			strBuilder.Append(",");

			//allow_for_customer
			strBuilder.Append("'"+drEach["allow_for_customer"].ToString()+"'");

			strBuilder.Append(")");
			String strSQLQuery = strBuilder.ToString();
			IDbCommand dbCmd = dbCon.CreateCommand(strSQLQuery,CommandType.Text);			

			try
			{

				iRowsAffected = dbCon.ExecuteNonQuery(dbCmd);
				Logger.LogTraceInfo("SysDataMgrDAL","AddVASCodeDS","SDM001",iRowsAffected+" rows inserted in to VAS table");
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("RBACManager","AddVASCodeDS","SDM001","Error During Insert "+ appException.Message.ToString());
				throw new ApplicationException("Error inserting VAS Code ",appException);
			}



			return iRowsAffected;

		}

		public static int ModifyVASCodeDS(String strAppID, String strEnterpriseID,DataSet dsToModify)
		{
			int iRowsAffected = 0;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("SysDataMgrDAL","GetVASCodeDS","EDB101","DbConnection object is null!!");
				return iRowsAffected;
			}
			int iRowCnt = dsToModify.Tables[0].Rows.Count;
			DataRow drEach = dsToModify.Tables[0].Rows[0];


			StringBuilder strBuilder = new StringBuilder();
			
			strBuilder.Append("update VAS set ");
			strBuilder.Append("vas_description = N'");
			String strVASDescription = (String) drEach["vas_description"];
			strBuilder.Append(Utility.ReplaceSingleQuote(strVASDescription));
			strBuilder.Append("', surcharge = ");

			if(Utility.IsNotDBNull(drEach["surcharge"]) && drEach["surcharge"].ToString()!="")
			{
				decimal decSurcharge = (decimal) drEach["surcharge"];
				strBuilder.Append(decSurcharge);
			}
			else
			{
				strBuilder.Append("null");
			}
			strBuilder.Append(",");

			//allow_for_customer
			strBuilder.Append(" allow_for_customer='"+drEach["allow_for_customer"].ToString()+"'");

			strBuilder.Append(" where applicationid = '");
			strBuilder.Append(strAppID);
			strBuilder.Append("' and enterpriseid = '");
			strBuilder.Append(strEnterpriseID);
			strBuilder.Append("' and vas_code = '");
			String strVASCode = (String) drEach["vas_code"];
			strBuilder.Append(strVASCode);
			strBuilder.Append("'");

			String strSQLQuery = strBuilder.ToString();

			IDbCommand dbCmd = dbCon.CreateCommand(strSQLQuery,CommandType.Text);
			
			try
			{

				iRowsAffected = dbCon.ExecuteNonQuery(dbCmd);
				Logger.LogTraceInfo("SysDataMgrDAL","AddVASCodeDS","SDM001",iRowsAffected+" rows inserted in to VAS_Code table");
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("RBACManager","AddVASCodeDS","SDM001","Error During Insert "+ appException.Message.ToString());
				throw new ApplicationException("Error inserting VAS Code ",appException);
			}
			return iRowsAffected;

		}

		public static SessionDS GetEmptyVASCodeDS(int iNumRows)
		{
			
			DataTable dtVASCode = new DataTable();
 
			dtVASCode.Columns.Add(new DataColumn("vas_code", typeof(string)));
			dtVASCode.Columns.Add(new DataColumn("vas_description", typeof(string)));
			dtVASCode.Columns.Add(new DataColumn("surcharge", typeof(decimal)));
			dtVASCode.Columns.Add(new DataColumn("allow_for_customer", typeof(string))); 

			for(int i=0; i < iNumRows; i++)
			{
				DataRow drEach = dtVASCode.NewRow();
				drEach[0] = "";
				drEach[1] = "";

				dtVASCode.Rows.Add(drEach);
			}

			DataSet dsVASCode = new DataSet();
			dsVASCode.Tables.Add(dtVASCode);

			//			dsVASCode.Tables[0].Columns["vas_code"].Unique = true; //Checks duplicate records..
			dsVASCode.Tables[0].Columns["vas_code"].AllowDBNull = false;

			SessionDS sessionDS = new SessionDS();
			sessionDS.ds = dsVASCode;
			sessionDS.DataSetRecSize = dsVASCode.Tables[0].Rows.Count;
			sessionDS.QueryResultMaxSize = sessionDS.DataSetRecSize;

			return  sessionDS;
	
		}


		public static void AddNewRowInVASCodeDS(SessionDS sessionDS)
		{
			DataRow drNew = sessionDS.ds.Tables[0].NewRow();

			drNew[0] = "";
			drNew[1] = "";

			try
			{
				sessionDS.ds.Tables[0].Rows.Add(drNew);
			}
			catch(Exception ex)
			{
				Logger.LogTraceError("SysDataMgrDAL","DeleteRouteCodeDS","R005",ex.Message.ToString());
			}
			
			sessionDS.QueryResultMaxSize++;
		}

		public static int DeleteVASCodeDS(String strAppID, String strEnterpriseID,String strVASCode)
		{
			int iRowsDeleted = 0;

			DbConnection dbConApp = null;
			IDbCommand dbCommandApp = null;

			IDbTransaction transactionApp = null;
			IDbConnection conApp = null;

	
			//Get App dbConnection

			dbConApp = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbConApp == null)
			{
				Logger.LogTraceError("SysDataMgrDAL","DeleteVASCodeDS","SDM002","dbConApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			//Begin Core and App Transaction

			conApp = dbConApp.GetConnection();
			
			if(conApp == null)
			{
				Logger.LogTraceError("SysDataMgrDAL","DeleteVASCodeDS","SDM002","conApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			try
			{
				dbConApp.OpenConnection(ref conApp);
			}
			catch(ApplicationException appException)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("SysDataMgrDAL","DeleteVASCodeDS","SDM003","Error opening connection : "+ appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ",appException);
			}
			catch(Exception exception)
			{
				if(conApp != null)
				{
					conApp.Close();
				}

				Logger.LogTraceError("SysDataMgrDAL","DeleteVASCodeDS","SDM003","Error opening connection : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}


			transactionApp = dbConApp.BeginTransaction(conApp,IsolationLevel.ReadCommitted);

			if(transactionApp == null)
			{
				Logger.LogTraceError("SysDataMgrDAL","DeleteVASCodeDS","SDM002","transactionApp is null");
				throw new ApplicationException("Failed Begining a App Transaction..",null);
			}


			try
			{

				//Update record into core_enterprise Table

				StringBuilder strBuilder = new StringBuilder();
			
				strBuilder.Append("Delete from Zipcode_VAS_Excluded where ");
				strBuilder.Append(" applicationid = '");
				strBuilder.Append(strAppID);
				strBuilder.Append("' and enterpriseid = '");
				strBuilder.Append(strEnterpriseID);
				strBuilder.Append("' and vas_code = '");
				strBuilder.Append(strVASCode);
				strBuilder.Append("'");


				String strSQLQuery = strBuilder.ToString();
				dbCommandApp = dbConApp.CreateCommand(strSQLQuery,CommandType.Text); //Creating the command first time
				dbCommandApp.Connection = conApp;
				dbCommandApp.Transaction = transactionApp;

				iRowsDeleted = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);
				Logger.LogTraceInfo("RBACManager","DeleteVASCodeDS","INF001",iRowsDeleted + " rows deleted from Exception_Code table");


				strBuilder = new StringBuilder();
			
				strBuilder.Append("Delete from VAS where ");
				strBuilder.Append(" applicationid = '");
				strBuilder.Append(strAppID);
				strBuilder.Append("' and enterpriseid = '");
				strBuilder.Append(strEnterpriseID);
				strBuilder.Append("' and vas_code = '");
				strBuilder.Append(strVASCode);
				strBuilder.Append("'");

				strSQLQuery = strBuilder.ToString();

				dbCommandApp.CommandText = strSQLQuery;


				iRowsDeleted = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);
				Logger.LogTraceInfo("RBACManager","DeleteVASCodeDS","INF001",iRowsDeleted + " rows deleted from VAS table");
			
				//Commit application and core transaction.

				transactionApp.Commit();
				Logger.LogTraceInfo("SysDataMgrDAL","DeleteVASCodeDS","SDM003","App db delete transaction committed.");

			}
			catch(ApplicationException appException)
			{
				
				try
				{
					transactionApp.Rollback();
					Logger.LogTraceInfo("SysDataMgrDAL","DeleteVASCodeDS","SDM002","App db delete transaction rolled back.");

				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("SysDataMgrDAL","DeleteVASCodeDS","SDM002","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}


				Logger.LogTraceError("SysDataMgrDAL","DeleteVASCodeDS","SDM002","Delete Transaction Failed : "+ appException.Message.ToString());
				throw new ApplicationException("Error deleting VAS Code "+appException.Message,appException);
			}
			catch(Exception exception)
			{
				try
				{
					transactionApp.Rollback();
					Logger.LogTraceInfo("SysDataMgrDAL","DeleteVASCodeDS","SDM002","App db delete transaction rolled back.");

				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("SysDataMgrDAL","DeleteVASCodeDS","SDM002","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}

				Logger.LogTraceError("SysDataMgrDAL","DeleteVASCodeDS","SDM004","delete Transaction Failed : "+ exception.Message.ToString());
				throw new ApplicationException("Error deleting VAS Code "+exception.Message,exception);
			}
			finally
			{
				if(conApp != null)
				{
					conApp.Close();
				}
			}
			
			return iRowsDeleted;
		}


		///Zipcode-VAS code excluded methods..
		///
		public static SessionDS GetZipcodeVASExcludedDS(String strAppID, String strEnterpriseID,String strVASCode,int iCurrent, int iDSRecSize)
		{
			SessionDS sessionDS = null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("SysDataMgrDAL","GetStatusCodeDS","EDB101","DbConnection object is null!!");
				return sessionDS;
			}

			String strSQLQuery = "select zvase.zipcode as zipcode, s.state_name as state_name, z.country as country from Zipcode_VAS_Excluded zvase, Zipcode z, State s where ";
			strSQLQuery += " zvase.applicationid = z.applicationid and zvase.enterpriseid = z.enterpriseid and zvase.zipcode = z.zipcode and ";
			strSQLQuery += " z.applicationid = s.applicationid and z.enterpriseid = s.enterpriseid and z.country = s.country and z.state_code = s.state_code and ";
			strSQLQuery += " zvase.applicationid = '"+strAppID+"' and zvase.enterpriseid = '"+strEnterpriseID+"' and zvase.vas_code = '"+strVASCode+"' and zvase.country = s.country and zvase.state_code = s.state_code "; 
			
			sessionDS = dbCon.ExecuteQuery(strSQLQuery,iCurrent,iDSRecSize,"ZipcodeVASExcludedTable");

			return  sessionDS;
	
		}

		public static int AddZipcodeVASExcludedDS(String strAppID, String strEnterpriseID,String strVASCode,DataSet dsToInsert)
		{
			int iRowsAffected = 0;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("SysDataMgrDAL","AddZipcodeVASExcludedDS","EDB101","DbConnection object is null!!");
				return iRowsAffected;
			}

			StringBuilder strBuilder = new StringBuilder();
			strBuilder.Append("insert into Zipcode_VAS_Excluded (applicationid,enterpriseid,vas_code, zipcode ) values ('");
			strBuilder.Append(strAppID);
			strBuilder.Append("','");
			strBuilder.Append(strEnterpriseID);
			strBuilder.Append("','");
			strBuilder.Append(strVASCode);
			strBuilder.Append("','");

			int iRowCnt = dsToInsert.Tables[0].Rows.Count;

			DataRow drEach = dsToInsert.Tables[0].Rows[0];
				
			String strZipcode = (String) drEach["zipcode"];
			strBuilder.Append(strZipcode);
			strBuilder.Append("')");

			String strSQLQuery = strBuilder.ToString();

			IDbCommand dbCmd = dbCon.CreateCommand(strSQLQuery,CommandType.Text);
			
			try
			{
				iRowsAffected = dbCon.ExecuteNonQuery(dbCmd);
				Logger.LogTraceInfo("SysDataMgrDAL","AddZipcodeVASExcludedDS","SDM001",iRowsAffected+" rows inserted in to Status_Code table");
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("RBACManager","AddZipcodeVASExcludedDS","SDM001","Error During Insert "+ appException.Message.ToString());
				throw new ApplicationException("Error inserting Status Code ",appException);
			}

			return iRowsAffected;

		}

		public static SessionDS GetEmptyZipcodeVASExcludedDS(int iNumRows)
		{
			DataTable dtZipcodeVASExcluded = new DataTable();
 
			dtZipcodeVASExcluded.Columns.Add(new DataColumn("zipcode", typeof(string)));
			dtZipcodeVASExcluded.Columns.Add(new DataColumn("state_name", typeof(string)));
			dtZipcodeVASExcluded.Columns.Add(new DataColumn("country", typeof(string)));

			for(int i=0; i < iNumRows; i++)
			{
				DataRow drEach = dtZipcodeVASExcluded.NewRow();
				drEach[0] = "";
				drEach[1] = "";
				drEach[2] = "";

				dtZipcodeVASExcluded.Rows.Add(drEach);
			}

			DataSet dsZipcodeVASExcluded = new DataSet();
			dsZipcodeVASExcluded.Tables.Add(dtZipcodeVASExcluded);

			dsZipcodeVASExcluded.Tables[0].Columns["zipcode"].Unique = true; //Checks duplicate records..
			dsZipcodeVASExcluded.Tables[0].Columns["zipcode"].AllowDBNull = false;

			SessionDS sessionDS = new SessionDS();
			sessionDS.ds = dsZipcodeVASExcluded;
			sessionDS.DataSetRecSize = dsZipcodeVASExcluded.Tables[0].Rows.Count;
			sessionDS.QueryResultMaxSize = sessionDS.DataSetRecSize;
			return  sessionDS;
	
		}

		public static void AddNewRowInZipcodeVASExcludedDS(SessionDS sessionDS)
		{
			DataRow drNew = sessionDS.ds.Tables[0].NewRow();

			drNew[0] = "";
			drNew[1] = "";
			drNew[2] = "";

			try
			{
				sessionDS.ds.Tables[0].Rows.Add(drNew);
			}
			catch(Exception ex)
			{
				Logger.LogTraceError("SysDataMgrDAL","DeleteRouteCodeDS","R005",ex.Message.ToString());
			}
			
			sessionDS.QueryResultMaxSize++;
		}

		public static int DeleteZipcodeVASExcludedDS(String strAppID, String strEnterpriseID,String strVASCode,String strZipcode)
		{
			int iRowsDeleted = 0;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("SysDataMgrDAL","DeleteExceptionCodeRecord","EDB101","DbConnection object is null!!");
				return iRowsDeleted;
			}


			StringBuilder strBuilder = new StringBuilder();
			
			strBuilder.Append("Delete from Zipcode_VAS_Excluded where ");
			strBuilder.Append(" applicationid = '");
			strBuilder.Append(strAppID);
			strBuilder.Append("' and enterpriseid = '");
			strBuilder.Append(strEnterpriseID);
			strBuilder.Append("' and vas_code = '");
			strBuilder.Append(strVASCode);
			strBuilder.Append("' and zipcode = '");
			strBuilder.Append(strZipcode);
			strBuilder.Append("'");


			String strSQLQuery = strBuilder.ToString();

			IDbCommand dbCmd = dbCon.CreateCommand(strSQLQuery,CommandType.Text);
			
			try
			{
				iRowsDeleted = dbCon.ExecuteNonQuery(dbCmd);
				Logger.LogTraceInfo("SysDataMgrDAL","DeleteExceptionCodeRecord","SDM001",iRowsDeleted+" rows deleted from  Exception_Code table");
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("SysDataMgrDAL","DeleteExceptionCodeRecord","SDM001","Error During Delete "+ appException.Message.ToString());
				throw new ApplicationException("Error inserting Status Code : "+appException.Message,appException);
			}

			return iRowsDeleted;		
		}


		//Methods for Customer Profile Base Module

		public static SessionDS GetCustomerProfileDS(String strAppID, String strEnterpriseID, int iCurrent, int iDSRecSize,DataSet dsQueryParam)
		{

			SessionDS sessionDS = null;


			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("SysDataMgrDAL","GetCustomerProfileDS","EDB101","DbConnection object is null!!");
				return sessionDS;
			}

			StringBuilder strBuilder = new StringBuilder();
			//HC Return Task
//			strBuilder.Append("select custid,ref_code,cust_name,contact_person,address1,address2,country,zipcode,state_code,telephone,mbg,fax,credit_term,industrial_sector_code,credit_limit,active_quotation_no,status_active,credit_outstanding,remark,SalesmanID,prom_tot_wt,payment_mode,prom_tot_package,prom_period,free_insurance_amt,insurance_percent_surcharge,email,created_by,apply_dim_wt,pod_slip_required,apply_esa_surcharge,apply_esa_surcharge_rep,invoice_return_days,cod_surcharge_amt,cod_surcharge_percent,insurance_maximum_amt,density_factor,payer_type, ");
			// insert max_cod_surcharge,min_cod_surcharge by Ching Nov 29,2007
//			//Modified By tom 22/7/09
//			strBuilder.Append("hc_invoice_required,max_cod_surcharge,min_cod_surcharge,min_insurance_surcharge,max_insurance_surcharge,other_surcharge_amount,other_surcharge_percentage,other_surcharge_min,other_surcharge_max,other_surcharge_desc,discount_band,CustomerBOX,master_account,remark2,next_bill_placement_date,credit_used,creditstatus,creditthreshold,Minimum_Box,Dim_By_TOT  , FirstShipDate  , datediff(m,FirstShipDate,getdate()) as mDiff   from Customer where applicationid = '");
//			//End Modified By Tom 22/7/09
			//Jeab 08 Mar 2012
			strBuilder.Append("select C.custid,ref_code,cust_name,contact_person,address1,address2,country,zipcode,state_code,telephone,mbg,fax,credit_term,industrial_sector_code,credit_limit,active_quotation_no,status_active,credit_outstanding,remark,SalesmanID,prom_tot_wt,payment_mode,prom_tot_package,prom_period,free_insurance_amt,insurance_percent_surcharge,email,created_by,apply_dim_wt,pod_slip_required,apply_esa_surcharge,apply_esa_surcharge_rep,invoice_return_days,cod_surcharge_amt,cod_surcharge_percent,insurance_maximum_amt,density_factor,payer_type, ");
			strBuilder.Append("hc_invoice_required,max_cod_surcharge,min_cod_surcharge,min_insurance_surcharge,max_insurance_surcharge,other_surcharge_amount,other_surcharge_percentage,other_surcharge_min,other_surcharge_max,other_surcharge_desc,discount_band,CustomerBOX,master_account,remark2,next_bill_placement_date,credit_used,creditstatus,creditthreshold,Minimum_Box,Dim_By_TOT ");
			strBuilder.Append(" , V.Commission_Datetime  , V.custCategory ");
			strBuilder.Append("from Customer C  Inner  Join  v_Customer_Category   V  On  C.custid = V.custid ");
			strBuilder.Append("where applicationid = '");
			//Jeab 08 Mar 2012  =========>  End
			//HC Return Task
			strBuilder.Append(strAppID);
			strBuilder.Append("' and enterpriseid = '");
			strBuilder.Append(strEnterpriseID);
			strBuilder.Append("' ");

			DataRow drEach = dsQueryParam.Tables[0].Rows[0];

			if(Utility.IsNotDBNull(drEach["custid"]) && drEach["custid"].ToString()!="")
			{
				String strCustomerAccNo = (String) drEach["custid"];
				strBuilder.Append(" and C.custid like ");
//				strBuilder.Append("'%");
				strBuilder.Append("'");
				strBuilder.Append(strCustomerAccNo);
				strBuilder.Append("%' ");
			}

			

			if(Utility.IsNotDBNull(drEach["ref_code"]) && drEach["ref_code"].ToString()!="")
			{
				String strRefCode = (String) drEach["ref_code"];
				strBuilder.Append(" and ref_code like ");
				strBuilder.Append("'%");
				strBuilder.Append(strRefCode);
				strBuilder.Append("%' ");
			}

			
			
			if(Utility.IsNotDBNull(drEach["cust_name"]) && drEach["cust_name"].ToString()!="")
			{
				String strCustomerName = (String) drEach["cust_name"];
				strBuilder.Append(" and cust_name like ");
//				strBuilder.Append("N'%"); //To accept Unicode
				strBuilder.Append("N'");
				strBuilder.Append(Utility.ReplaceSingleQuote(strCustomerName));
				strBuilder.Append("%' ");
			}

			
			if(Utility.IsNotDBNull(drEach["contact_person"]) && drEach["contact_person"].ToString() !="")
			{
				String strContactPerson = (String) drEach["contact_person"];
				strBuilder.Append(" and contact_person like ");
				strBuilder.Append("N'%"); //To accept Unicode
				strBuilder.Append(Utility.ReplaceSingleQuote(strContactPerson));
				strBuilder.Append("%' ");
			}


			if(Utility.IsNotDBNull(drEach["address1"]) && drEach["address1"].ToString() !="")
			{
				String strAddress1 = (String) drEach["address1"];
				strBuilder.Append(" and address1 like ");
//				strBuilder.Append("N'%");
				strBuilder.Append("N'");
				strBuilder.Append(Utility.ReplaceSingleQuote(strAddress1));
				strBuilder.Append("%' ");
			}


			if(Utility.IsNotDBNull(drEach["address2"]) && drEach["address2"].ToString()!="")
			{
				String strAddress2 = (String) drEach["address2"];
				strBuilder.Append(" and address2 like ");
//				strBuilder.Append("N'%");
				strBuilder.Append("N'");
				strBuilder.Append(Utility.ReplaceSingleQuote(strAddress2));
				strBuilder.Append("%' ");
			}

			if(Utility.IsNotDBNull(drEach["country"]) && drEach["country"].ToString()!="")
			{
				String strCountry = (String) drEach["country"];
				strBuilder.Append(" and country like ");
				strBuilder.Append("'%");
				strBuilder.Append(strCountry);
				strBuilder.Append("%' ");
			}


			if(Utility.IsNotDBNull(drEach["zipcode"]) && drEach["zipcode"].ToString()!="")
			{
				String strZipcode = (String) drEach["zipcode"];
				strBuilder.Append(" and zipcode like ");
//				strBuilder.Append("'%");
				strBuilder.Append("'");
				strBuilder.Append(strZipcode);
				strBuilder.Append("%' ");
			}


			if(Utility.IsNotDBNull(drEach["telephone"]) && drEach["telephone"].ToString()!="")
			{
				String strTelephone = (String) drEach["telephone"];
				strBuilder.Append(" and telephone like ");
//				strBuilder.Append("'%");
				strBuilder.Append("'");
				strBuilder.Append(Utility.ReplaceSingleQuote(strTelephone));
				strBuilder.Append("%' ");
			}


			if(Utility.IsNotDBNull(drEach["mbg"]) && drEach["mbg"].ToString()!="")
			{
				String strMBG = (String) drEach["mbg"];
				strBuilder.Append(" and mbg like ");
				strBuilder.Append("'%");
				strBuilder.Append(strMBG);
				strBuilder.Append("%' ");
			}

			if(Utility.IsNotDBNull(drEach["fax"]) && drEach["fax"].ToString()!="")
			{
				String strFax = (String) drEach["fax"];
				strBuilder.Append(" and fax like ");
				strBuilder.Append("'%");
				strBuilder.Append(Utility.ReplaceSingleQuote(strFax));
				strBuilder.Append("%' ");
			}


			if(Utility.IsNotDBNull(drEach["credit_term"]) && drEach["credit_term"].ToString()!="")
			{
				int iCreditTerm = System.Convert.ToInt32( drEach["credit_term"]);
				strBuilder.Append(" and credit_term = ");
				strBuilder.Append("");
				strBuilder.Append(iCreditTerm);
				strBuilder.Append(" ");
			}


			if(Utility.IsNotDBNull(drEach["industrial_sector_code"]) && drEach["industrial_sector_code"].ToString()!="")
			{
				String strISectorCode = (String) drEach["industrial_sector_code"];
				strBuilder.Append(" and industrial_sector_code like ");
				strBuilder.Append("'%");
				strBuilder.Append(strISectorCode);
				strBuilder.Append("%' ");
			}


			if(Utility.IsNotDBNull(drEach["credit_limit"]) && drEach["credit_limit"].ToString()!="")
			{
				decimal decCreditLimit = (decimal) drEach["credit_limit"];
				strBuilder.Append(" and credit_limit = ");
				strBuilder.Append(decCreditLimit);
				strBuilder.Append(" ");
			}


			if(Utility.IsNotDBNull(drEach["active_quotation_no"]) && drEach["active_quotation_no"].ToString()!="")
			{
				String strActiveQuotationNo = (String) drEach["active_quotation_no"];
				strBuilder.Append(" and active_quotation_no like ");
				strBuilder.Append("'%");
				strBuilder.Append(strActiveQuotationNo);
				strBuilder.Append("%' ");
			}


			if(Utility.IsNotDBNull(drEach["status_active"]) && drEach["status_active"].ToString()!="")
			{
				String strStatusActive = (String) drEach["status_active"];
				strBuilder.Append(" and status_active like ");
				strBuilder.Append("'%");
				strBuilder.Append(strStatusActive);
				strBuilder.Append("%' ");
			}


			if(Utility.IsNotDBNull(drEach["credit_outstanding"]) && drEach["credit_outstanding"].ToString()!="")
			{
				decimal decCreditOutstanding = (decimal) drEach["credit_outstanding"];
				strBuilder.Append(" and credit_outstanding = ");
				strBuilder.Append(decCreditOutstanding);
				strBuilder.Append(" ");

			}


			if(Utility.IsNotDBNull(drEach["remark"]) && drEach["remark"].ToString()!="")
			{
				String strRemark = (String) drEach["remark"];
				strBuilder.Append(" and remark like ");
				strBuilder.Append("N'%");
				strBuilder.Append(Utility.ReplaceSingleQuote(strRemark));
				strBuilder.Append("%' ");
			}


			if(Utility.IsNotDBNull(drEach["SalesmanID"]) && drEach["SalesmanID"].ToString()!="")
			{
				String strSalesmanID = (String) drEach["SalesmanID"];
				strBuilder.Append(" and SalesmanID like ");
				strBuilder.Append("'%");
				strBuilder.Append(Utility.ReplaceSingleQuote(strSalesmanID));
				strBuilder.Append("%' ");
			}


			if(Utility.IsNotDBNull(drEach["prom_tot_wt"]) && drEach["prom_tot_wt"].ToString()!="")
			{
				decimal decPromisedTotalWt = (decimal) drEach["prom_tot_wt"];
				strBuilder.Append(" and prom_tot_wt = ");
				strBuilder.Append(decPromisedTotalWt);
				strBuilder.Append(" ");
			}


			if(Utility.IsNotDBNull(drEach["payment_mode"]) && drEach["payment_mode"].ToString()!="")
			{
				String strPaymentMode = (String) drEach["payment_mode"];
				strBuilder.Append(" and payment_mode like ");
				strBuilder.Append("'%");
				strBuilder.Append(strPaymentMode);
				strBuilder.Append("%' ");
			}


			if(Utility.IsNotDBNull(drEach["prom_tot_package"]) && drEach["prom_tot_package"].ToString()!="")
			{
				int iPromisedTotalPackage = System.Convert.ToInt32( drEach["prom_tot_package"]);
				strBuilder.Append(" and prom_tot_package = ");
				strBuilder.Append(iPromisedTotalPackage);
				strBuilder.Append(" ");
			}


			if(Utility.IsNotDBNull(drEach["prom_period"]) && drEach["prom_period"].ToString()!="")
			{
				int iPromisedPeriod = System.Convert.ToInt32( drEach["prom_period"]);
				strBuilder.Append(" and prom_period = ");
				strBuilder.Append(iPromisedPeriod);
				strBuilder.Append(" ");
			}


			if(Utility.IsNotDBNull(drEach["free_insurance_amt"]) && drEach["free_insurance_amt"].ToString()!="")
			{
				decimal decFreeInsuranceAmt = (decimal) drEach["free_insurance_amt"];
				strBuilder.Append(" and free_insurance_amt = ");
				strBuilder.Append(decFreeInsuranceAmt);
				strBuilder.Append(" ");
			}


			if(Utility.IsNotDBNull(drEach["insurance_percent_surcharge"]) && drEach["insurance_percent_surcharge"].ToString()!="")
			{
				decimal decInsurancePercentSurcharge = (decimal) drEach["insurance_percent_surcharge"];
				strBuilder.Append(" and insurance_percent_surcharge = ");
				strBuilder.Append(decInsurancePercentSurcharge);
				strBuilder.Append(" ");
			}


			if(Utility.IsNotDBNull(drEach["email"]) && drEach["email"].ToString()!="")
			{
				String strEmail = (String) drEach["email"];
				strBuilder.Append(" and email like ");
				strBuilder.Append("'%");
				strBuilder.Append(Utility.ReplaceSingleQuote(strEmail));
				strBuilder.Append("%' ");
			}


			if(Utility.IsNotDBNull(drEach["created_by"]) && drEach["created_by"].ToString()!="")
			{
				String strCreatedBy = (String) drEach["created_by"];
				strBuilder.Append(" and created_by like ");
				strBuilder.Append("'%");
				strBuilder.Append(strCreatedBy);
				strBuilder.Append("%' ");
			}


			if(Utility.IsNotDBNull(drEach["apply_dim_wt"]) && drEach["apply_dim_wt"].ToString()!="")
			{
				String strApplyDimWt = (String) drEach["apply_dim_wt"];
				strBuilder.Append(" and apply_dim_wt like ");
				strBuilder.Append("'%");
				strBuilder.Append(strApplyDimWt);
				strBuilder.Append("%' ");
			}


			if(Utility.IsNotDBNull(drEach["pod_slip_required"]) && drEach["pod_slip_required"].ToString()!="")
			{
				String strPodSlipRequired = (String) drEach["pod_slip_required"];
				strBuilder.Append(" and pod_slip_required like ");
				strBuilder.Append("'%");
				strBuilder.Append(strPodSlipRequired);
				strBuilder.Append("%' ");
			}


			if(Utility.IsNotDBNull(drEach["apply_esa_surcharge"]) && drEach["apply_esa_surcharge"].ToString()!="")
			{
				String strApplyESASurcharge = (String) drEach["apply_esa_surcharge"];
				strBuilder.Append(" and apply_esa_surcharge like ");
				strBuilder.Append("'%");
				strBuilder.Append(strApplyESASurcharge);
				strBuilder.Append("%' ");

			}
			// New Field [appply_esa_surcharge_rep]
			if(Utility.IsNotDBNull(drEach["apply_esa_surcharge_rep"]) && drEach["apply_esa_surcharge_rep"].ToString()!="")
			{
				String strApplyESASurchargeDel = (String) drEach["apply_esa_surcharge_rep"];
				strBuilder.Append(" and apply_esa_surcharge_rep like ");
				strBuilder.Append("'%");
				strBuilder.Append(strApplyESASurchargeDel);
				strBuilder.Append("%' ");

			}

			//HC Return Task
			if(Utility.IsNotDBNull(drEach["hc_invoice_required"]) && drEach["hc_invoice_required"].ToString()!="")
			{
				String strHCInvoiceRequired = (String) drEach["hc_invoice_required"];
				strBuilder.Append(" and hc_invoice_required like ");
				strBuilder.Append("'%");
				strBuilder.Append(strHCInvoiceRequired);
				strBuilder.Append("%' ");
			}
			
			// by Ching Nov 29,2007
			if(Utility.IsNotDBNull(drEach["max_cod_surcharge"]) && drEach["max_cod_surcharge"].ToString()!="")
			{
				decimal decMaxCODSurcharge = (decimal) drEach["max_cod_surcharge"];
				strBuilder.Append(" and max_cod_surcharge = ");
				strBuilder.Append(decMaxCODSurcharge);
				strBuilder.Append(" ");
			}

			if(Utility.IsNotDBNull(drEach["min_cod_surcharge"]) && drEach["min_cod_surcharge"].ToString()!="")
			{
				decimal decMinCODSurcharge = (decimal) drEach["min_cod_surcharge"];
				strBuilder.Append(" and min_cod_surcharge = ");
				strBuilder.Append(decMinCODSurcharge);
				strBuilder.Append(" ");
			}
			// End
			

			//By Aoo 14/02/2008
			if(Utility.IsNotDBNull(drEach["min_insurance_surcharge"]) && drEach["min_insurance_surcharge"].ToString()!="")
			{
				decimal decMinInsuranceSurcharge = (decimal) drEach["min_insurance_surcharge"];
				strBuilder.Append(" and min_insurance_surcharge = ");
				strBuilder.Append(decMinInsuranceSurcharge);
				strBuilder.Append(" ");
			}

			if(Utility.IsNotDBNull(drEach["max_insurance_surcharge"]) && drEach["max_insurance_surcharge"].ToString()!="")
			{
				decimal decMaxInsuranceSurcharge = (decimal) drEach["max_insurance_surcharge"];
				strBuilder.Append(" and max_insurance_surcharge = ");
				strBuilder.Append(decMaxInsuranceSurcharge);
				strBuilder.Append(" ");
			}

			if(Utility.IsNotDBNull(drEach["other_surcharge_amount"]) && drEach["other_surcharge_amount"].ToString()!="")
			{
				decimal decOtherSurchargeAmount = (decimal) drEach["other_surcharge_amount"];
				strBuilder.Append(" and other_surcharge_amount = ");
				strBuilder.Append(decOtherSurchargeAmount);
				strBuilder.Append(" ");
			}

			if(Utility.IsNotDBNull(drEach["other_surcharge_percentage"]) && drEach["other_surcharge_percentage"].ToString()!="")
			{
				decimal decOtherSurchargePercentage = (decimal) drEach["other_surcharge_percentage"];
				strBuilder.Append(" and other_surcharge_percentage = ");
				strBuilder.Append(decOtherSurchargePercentage);
				strBuilder.Append(" ");
			}
			if(Utility.IsNotDBNull(drEach["other_surcharge_min"]) && drEach["other_surcharge_min"].ToString()!="")
			{
				decimal decOtherSurchargeMin = (decimal) drEach["other_surcharge_min"];
				strBuilder.Append(" and other_surcharge_min = ");
				strBuilder.Append(decOtherSurchargeMin);
				strBuilder.Append(" ");
			}

			if(Utility.IsNotDBNull(drEach["other_surcharge_max"]) && drEach["other_surcharge_max"].ToString()!="")
			{
				decimal decOtherSurchargeMax = (decimal) drEach["other_surcharge_max"];
				strBuilder.Append(" and other_surcharge_max = ");
				strBuilder.Append(decOtherSurchargeMax);
				strBuilder.Append(" ");
			}
			if(Utility.IsNotDBNull(drEach["other_surcharge_desc"]) && drEach["other_surcharge_desc"].ToString()!="")
			{
				string strOtherSurchargeDesc = (string) drEach["other_surcharge_desc"];
				strBuilder.Append(" and other_surcharge_desc = ");
				strBuilder.Append(strOtherSurchargeDesc);
				strBuilder.Append(" ");
			}

			if(Utility.IsNotDBNull(drEach["discount_band"]) && drEach["discount_band"].ToString()!="")
			{
				string strDiscountBand = (string) drEach["discount_band"];
				strBuilder.Append(" and discount_band = ");
				strBuilder.Append(strDiscountBand);
				strBuilder.Append(" ");
			}
			// End By Aoo

			//Modified by GwanG on 04Mar08
			if(Utility.IsNotDBNull(drEach["payer_type"]) && drEach["payer_type"].ToString()!="")
			{
				String strCustomerType = (String) drEach["payer_type"];
				strBuilder.Append(" and payer_type = '");
				strBuilder.Append(strCustomerType);
				strBuilder.Append("' ");
			}

		//ADD BY X FEB 19 09
			if(Utility.IsNotDBNull(drEach["master_account"]) && drEach["master_account"].ToString()!="")
			{
				String strMaster_account = (String) drEach["master_account"];
				strBuilder.Append(" and master_account = '");
				strBuilder.Append(strMaster_account);
				strBuilder.Append("' ");
			}

			//Added By Tom 22/7/09
			if(Utility.IsNotDBNull(drEach["remark2"]) && drEach["remark2"].ToString()!="")
			{
				String strRemark2 = (String) drEach["remark2"];
				strBuilder.Append(" and remark2 = '");
				strBuilder.Append(strRemark2);
				strBuilder.Append("' ");
			}
			//End Added By Tom 22/7/09
			if(Utility.IsNotDBNull(drEach["next_bill_placement_date"]) && drEach["next_bill_placement_date"].ToString()!="")
			{
				DateTime strnbpd = (DateTime) drEach["next_bill_placement_date"];
				strBuilder.Append(" and next_bill_placement_date = ");
				strBuilder.Append(strnbpd);
			}

			if(Utility.IsNotDBNull(drEach["Dim_By_TOT"]) && drEach["Dim_By_TOT"].ToString()!="")
			{
				String strDim_By_TOT = (String) drEach["Dim_By_TOT"];
				strBuilder.Append(" and Dim_By_TOT like ");
				strBuilder.Append("'%");
				strBuilder.Append(strDim_By_TOT);
				strBuilder.Append("%' ");
			}

			//HC Return Task

			strBuilder.Append(" Order by Cust_name");	//Order by included by 20/11/2002 Mohan

			String strSQLQuery = strBuilder.ToString();
			
			sessionDS = dbCon.ExecuteQuery(strSQLQuery,iCurrent,iDSRecSize,"CustomerProfileTable");

			return  sessionDS;
	
		}

		public static int AddCustomerProfileDS(String strAppID, String strEnterpriseID,DataSet dsToInsert)
		{
			int iRowsAffected = 0;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("SysDataMgrDAL","AddCustomerProfileDS","EDB101","DbConnection object is null!!");
				return iRowsAffected;
			}



			StringBuilder strBuilder = new StringBuilder();
			//HC Return Task
			strBuilder.Append("insert into Customer (applicationid,enterpriseid,custid,ref_code,cust_name,contact_person,address1,address2,country,state_code,zipcode,telephone,mbg,fax,credit_term,industrial_sector_code,credit_limit,active_quotation_no,status_active,credit_outstanding,remark,SalesmanID,prom_tot_wt,payment_mode,prom_tot_package,prom_period,free_insurance_amt,insurance_percent_surcharge,email,created_by,apply_dim_wt,pod_slip_required,apply_esa_surcharge,apply_esa_surcharge_rep,invoice_return_days,cod_surcharge_amt,cod_surcharge_percent,insurance_maximum_amt,density_factor,payer_type, ");
			// insert max_cod_surcharge, min_cod_surcharge by Ching Nov 29,2007  
			//Modified By Tom 22/7/09
			strBuilder.Append("hc_invoice_required,max_cod_surcharge, min_cod_surcharge,min_insurance_surcharge,max_insurance_surcharge,other_surcharge_amount,other_surcharge_percentage,other_surcharge_min,other_surcharge_max,other_surcharge_desc,discount_band,CustomerBOX,master_account,remark2,next_bill_placement_date,credit_used,creditstatus,creditthreshold,Minimum_box,dim_by_tot) values ('");
			//End Modified By Tom 22/7/09
			//HC Return Task
			strBuilder.Append(strAppID);
			strBuilder.Append("','");
			strBuilder.Append(strEnterpriseID);
			strBuilder.Append("','");

			int iRowCnt = dsToInsert.Tables[0].Rows.Count;

			DataRow drEach = dsToInsert.Tables[0].Rows[0];
					


			String strCustID = (String) drEach["custid"];
			strBuilder.Append(strCustID.Trim().ToUpper());
			
			strBuilder.Append("',");
			if(Utility.IsNotDBNull(drEach["ref_code"]) && drEach["ref_code"].ToString()!="")
			{
				String strRefCode = (String) drEach["ref_code"];
				strBuilder.Append("'");
				strBuilder.Append(strRefCode);
				strBuilder.Append("'");
			}
			else
			{
				strBuilder.Append("null");
			}

			strBuilder.Append(",");
			if(Utility.IsNotDBNull(drEach["cust_name"]) && drEach["cust_name"].ToString()!="")
			{
				String strCustomerName = (String) drEach["cust_name"];
				strBuilder.Append("N'"); //To accept Unicode
				strBuilder.Append(Utility.ReplaceSingleQuote(strCustomerName));
				strBuilder.Append("'");
			}
			else
			{
				strBuilder.Append("null");
			}
			strBuilder.Append(",");
			if(Utility.IsNotDBNull(drEach["contact_person"]) && drEach["contact_person"].ToString()!="")
			{
				String strContactPerson = (String) drEach["contact_person"];
				strBuilder.Append("N'");
				strBuilder.Append(Utility.ReplaceSingleQuote(strContactPerson));
				strBuilder.Append("'");
			}
			else
			{
				strBuilder.Append("null");
			}
			strBuilder.Append(",");
			if(Utility.IsNotDBNull(drEach["address1"]) && drEach["address1"].ToString()!="")
			{
				String strAddress1 = (String) drEach["address1"];
				strBuilder.Append("N'");
				strBuilder.Append(Utility.ReplaceSingleQuote(strAddress1));
				strBuilder.Append("'");
			}
			else
			{
				strBuilder.Append("null");
			}
			strBuilder.Append(",");
			if(Utility.IsNotDBNull(drEach["address2"]) && drEach["address2"].ToString()!="")
			{
				String strAddress2 = (String) drEach["address2"];
				strBuilder.Append("N'");
				strBuilder.Append(Utility.ReplaceSingleQuote(strAddress2));
				strBuilder.Append("'");
			}
			else
			{
				strBuilder.Append("null");
			}
			strBuilder.Append(",");
			if(Utility.IsNotDBNull(drEach["country"]) && drEach["country"].ToString()!="")
			{
				String strCountry = (String) drEach["country"];
				strBuilder.Append("'");
				strBuilder.Append(strCountry);
				strBuilder.Append("'");
			}
			else
			{
				strBuilder.Append("null");
			}
			strBuilder.Append(",");
			if(Utility.IsNotDBNull(drEach["state_code"]) && drEach["state_code"].ToString()!="")
			{
				String strStateCode = (String) drEach["state_code"];
				strBuilder.Append("'");
				strBuilder.Append(strStateCode);
				strBuilder.Append("'");
			}
			else
			{
				strBuilder.Append("null");
			}
			strBuilder.Append(",");
			if(Utility.IsNotDBNull(drEach["zipcode"]) && drEach["zipcode"].ToString()!="")
			{
				String strZipcode = (String) drEach["zipcode"];
				strBuilder.Append("'");
				strBuilder.Append(strZipcode);
				strBuilder.Append("'");
			}
			else
			{
				strBuilder.Append("null");
			}
			strBuilder.Append(",");
			if(Utility.IsNotDBNull(drEach["telephone"]) && drEach["telephone"].ToString()!="")
			{
				String strTelephone = (String) drEach["telephone"];
				strBuilder.Append("'");
				strBuilder.Append(Utility.ReplaceSingleQuote(strTelephone));
				strBuilder.Append("'");
			}
			else
			{
				strBuilder.Append("null");
			}
			strBuilder.Append(",");
			if(Utility.IsNotDBNull(drEach["mbg"]) && drEach["mbg"].ToString()!="")
			{
				String strMBG = (String) drEach["mbg"];
				strBuilder.Append("'");
				strBuilder.Append(strMBG);
				strBuilder.Append("'");
			}
			else
			{
				strBuilder.Append("null");
			}
			strBuilder.Append(",");
			if(Utility.IsNotDBNull(drEach["fax"]) && drEach["fax"].ToString()!="")
			{
				String strFax = (String) drEach["fax"];
				strBuilder.Append("'");
				strBuilder.Append(Utility.ReplaceSingleQuote(strFax));
				strBuilder.Append("'");
			}
			else
			{
				strBuilder.Append("null");
			}
			strBuilder.Append(",");
			if(Utility.IsNotDBNull(drEach["credit_term"]) && drEach["credit_term"].ToString()!="")
			{
				int iCreditTerm = System.Convert.ToInt32( drEach["credit_term"]);
				strBuilder.Append("");
				strBuilder.Append(iCreditTerm);
				strBuilder.Append(" ");
			}
			else
			{
				strBuilder.Append("null");
			}
			strBuilder.Append(",");
			if(Utility.IsNotDBNull(drEach["industrial_sector_code"]) && drEach["industrial_sector_code"].ToString()!="")
			{
				String strISectorCode = (String) drEach["industrial_sector_code"];
				strBuilder.Append("'");
				strBuilder.Append(strISectorCode);
				strBuilder.Append("'");
			}
			else
			{
				strBuilder.Append("null");
			}
			strBuilder.Append(",");
			if(Utility.IsNotDBNull(drEach["credit_limit"]) && drEach["credit_limit"].ToString()!="")
			{
				decimal decCreditLimit = (decimal) drEach["credit_limit"];
				strBuilder.Append(decCreditLimit);
			}
			else
			{
				strBuilder.Append("null");
			}
			strBuilder.Append(",");
			if(Utility.IsNotDBNull(drEach["active_quotation_no"]) && drEach["active_quotation_no"].ToString()!="")
			{
				String strActiveQuotationNo = (String) drEach["active_quotation_no"];
				strBuilder.Append("'");
				strBuilder.Append(strActiveQuotationNo);
				strBuilder.Append("'");
			}
			else
			{
				strBuilder.Append("null");
			}
			strBuilder.Append(",");
			if(Utility.IsNotDBNull(drEach["status_active"]) && drEach["status_active"].ToString()!="")
			{
				String strStatusActive = (String) drEach["status_active"];
				strBuilder.Append("'");
				strBuilder.Append(strStatusActive);
				strBuilder.Append("'");
			}
			else
			{
				strBuilder.Append("null");
			}
			strBuilder.Append(",");
			if(Utility.IsNotDBNull(drEach["credit_outstanding"]) && drEach["credit_outstanding"].ToString()!="")
			{
				decimal decCreditOutstanding = (decimal) drEach["credit_outstanding"];
				strBuilder.Append(decCreditOutstanding);
			}
			else
			{
				strBuilder.Append("null");
			}
			strBuilder.Append(",");
			if(Utility.IsNotDBNull(drEach["remark"]) && drEach["remark"].ToString()!="")
			{
				String strRemark = (String) drEach["remark"];
				strBuilder.Append("N'");
				strBuilder.Append(Utility.ReplaceSingleQuote(strRemark));
				strBuilder.Append("'");
			}
			else
			{
				strBuilder.Append("null");
			}
			strBuilder.Append(",");
			if(Utility.IsNotDBNull(drEach["SalesmanID"]) && drEach["SalesmanID"].ToString()!="")
			{
				String strSalesmanID = (String) drEach["SalesmanID"];
				strBuilder.Append("'");
				strBuilder.Append(Utility.ReplaceSingleQuote(strSalesmanID));
				strBuilder.Append("'");
			}
			else
			{
				strBuilder.Append("null");
			}
			strBuilder.Append(",");
			if(Utility.IsNotDBNull(drEach["prom_tot_wt"]) && drEach["prom_tot_wt"].ToString()!="")
			{
				decimal decPromisedTotalWt = (decimal) drEach["prom_tot_wt"];
				strBuilder.Append(decPromisedTotalWt);
			}
			else
			{
				strBuilder.Append("null");
			}
			strBuilder.Append(",");
			if(Utility.IsNotDBNull(drEach["payment_mode"]) && drEach["payment_mode"].ToString()!="")
			{
				String strPaymentMode = (String) drEach["payment_mode"];
				strBuilder.Append("'");
				strBuilder.Append(strPaymentMode);
				strBuilder.Append("'");
			}
			else
			{
				strBuilder.Append("null");
			}
			strBuilder.Append(",");
			if(Utility.IsNotDBNull(drEach["prom_tot_package"]) && drEach["prom_tot_package"].ToString()!="")
			{
				int iPromisedTotalPackage = System.Convert.ToInt32( drEach["prom_tot_package"]);
				strBuilder.Append(iPromisedTotalPackage);
			}
			else
			{
				strBuilder.Append("null");
			}
			strBuilder.Append(",");

			if(Utility.IsNotDBNull(drEach["prom_period"]) && drEach["prom_period"].ToString()!="")
			{
				int iPromisedPeriod = System.Convert.ToInt32( drEach["prom_period"]);
				strBuilder.Append(iPromisedPeriod);
			}
			else
			{
				strBuilder.Append("null");
			}
			strBuilder.Append(",");
			if(Utility.IsNotDBNull(drEach["free_insurance_amt"]) && drEach["free_insurance_amt"].ToString()!="")
			{
				decimal decFreeInsuranceAmt = (decimal) drEach["free_insurance_amt"];
				strBuilder.Append(decFreeInsuranceAmt);
			}
			else
			{
				strBuilder.Append("null");
			}
			strBuilder.Append(",");
			if(Utility.IsNotDBNull(drEach["insurance_percent_surcharge"]) && drEach["insurance_percent_surcharge"].ToString()!="")
			{
				decimal decInsurancePercentSurcharge = (decimal) drEach["insurance_percent_surcharge"];
				strBuilder.Append(decInsurancePercentSurcharge);
			}
			else
			{
				strBuilder.Append("null");
			}
			strBuilder.Append(",");
			if(Utility.IsNotDBNull(drEach["email"]) && drEach["email"].ToString()!="")
			{
				String strEmail = (String) drEach["email"];
				strBuilder.Append("'");
				strBuilder.Append(Utility.ReplaceSingleQuote(strEmail));
				strBuilder.Append("'");
			}
			else
			{
				strBuilder.Append("null");
			}
			strBuilder.Append(",");
			if(Utility.IsNotDBNull(drEach["created_by"]) && drEach["created_by"].ToString()!="")
			{
				String strCreatedBy = (String) drEach["created_by"];
				strBuilder.Append("'");
				strBuilder.Append(strCreatedBy);
				strBuilder.Append("'");
			}
			else
			{
				strBuilder.Append("null");
			}
			strBuilder.Append(",");
			if(Utility.IsNotDBNull(drEach["apply_dim_wt"]) && drEach["apply_dim_wt"].ToString()!="")
			{
				String strApplyDimWt = (String) drEach["apply_dim_wt"];
				strBuilder.Append("'");
				strBuilder.Append(strApplyDimWt);
				strBuilder.Append("'");
			}
			else
			{
				strBuilder.Append("null");
			}
			strBuilder.Append(",");
			if(Utility.IsNotDBNull(drEach["pod_slip_required"]) && drEach["pod_slip_required"].ToString()!="")
			{
				String strPodSlipRequired = (String) drEach["pod_slip_required"];
				strBuilder.Append("'");
				strBuilder.Append(strPodSlipRequired);
				strBuilder.Append("'");
			}
			else
			{
				strBuilder.Append("null");
			}
			strBuilder.Append(",");
			if(Utility.IsNotDBNull(drEach["apply_esa_surcharge"]) && drEach["apply_esa_surcharge"].ToString()!="")
			{
				String strApplyESASurcharge = (String) drEach["apply_esa_surcharge"];
				strBuilder.Append("'");
				strBuilder.Append(strApplyESASurcharge);
				strBuilder.Append("'");

			}
			else
			{
				strBuilder.Append("null");
			}

			strBuilder.Append(",");
			if(Utility.IsNotDBNull(drEach["apply_esa_surcharge_rep"]) && drEach["apply_esa_surcharge_rep"].ToString()!="")
			{
				String strApplyESASurchargeDel = (String) drEach["apply_esa_surcharge_rep"];
				strBuilder.Append("'");
				strBuilder.Append(strApplyESASurchargeDel);
				strBuilder.Append("'");

			}
			else
			{
				strBuilder.Append("null");
			}




			// for Invoice Return Days
			strBuilder.Append(",");
			if(Utility.IsNotDBNull(drEach["invoice_return_days"]) && drEach["invoice_return_days"].ToString()!="")
			{
				String strInvoiceReturn = (String) drEach["invoice_return_days"].ToString();
				strBuilder.Append("'");
				strBuilder.Append(strInvoiceReturn);
				strBuilder.Append("'");

			}
			else
			{
				strBuilder.Append("null");
			}

			strBuilder.Append(",");
			if(Utility.IsNotDBNull(drEach["cod_surcharge_amt"]) && drEach["cod_surcharge_amt"].ToString()!="")
			{
				decimal deccod_surcharge_amt = (decimal) drEach["cod_surcharge_amt"];
				strBuilder.Append(deccod_surcharge_amt);
			}
			else
			{
				strBuilder.Append("null");
			}

			strBuilder.Append(",");
			if(Utility.IsNotDBNull(drEach["cod_surcharge_percent"]) && drEach["cod_surcharge_percent"].ToString()!="")
			{
				decimal deccod_surcharge_percent = (decimal) drEach["cod_surcharge_percent"];
				strBuilder.Append(deccod_surcharge_percent);
			}
			else
			{
				strBuilder.Append("null");
			}
			
			
			strBuilder.Append(",");
			if(Utility.IsNotDBNull(drEach["insurance_maximum_amt"]) && drEach["insurance_maximum_amt"].ToString()!="")
			{
				decimal decinsurance_maximum_amt = (decimal) drEach["insurance_maximum_amt"];
				strBuilder.Append(decinsurance_maximum_amt);
			}
			else
			{
				strBuilder.Append("null");
			}

			strBuilder.Append(",");
			if(Utility.IsNotDBNull(drEach["density_factor"]) && drEach["density_factor"].ToString()!="")
			{
				decimal decdensity_factor = (decimal) drEach["density_factor"];
				strBuilder.Append(decdensity_factor);
			}
			else
			{
				strBuilder.Append("null");
			}

			strBuilder.Append(",");
			if(Utility.IsNotDBNull(drEach["payer_type"]) && drEach["payer_type"].ToString()!="")
			{
				String strpayer_type = (String) drEach["payer_type"].ToString();
				strBuilder.Append("'");
				strBuilder.Append(strpayer_type);
				strBuilder.Append("'");

			}
			else
			{
				strBuilder.Append("null");
			}

			//HC Return Task
			strBuilder.Append(",");
			if(Utility.IsNotDBNull(drEach["hc_invoice_required"]) && drEach["hc_invoice_required"].ToString()!="")
			{
				String strHCInvoiceRequired = (String) drEach["hc_invoice_required"];
				strBuilder.Append("'");
				strBuilder.Append(strHCInvoiceRequired);
				strBuilder.Append("'");
			}
			else
			{
				strBuilder.Append("null");
			}
			//HC Return Task

			// by Ching Nov 29,2007
			strBuilder.Append(",");
			if(Utility.IsNotDBNull(drEach["max_cod_surcharge"]) && drEach["max_cod_surcharge"].ToString()!="")
			{
				decimal decMaxCODSurcharge = (decimal) drEach["max_cod_surcharge"];
				strBuilder.Append(decMaxCODSurcharge);
			}
			else
			{
				strBuilder.Append("null");
			}

			strBuilder.Append(",");
			if(Utility.IsNotDBNull(drEach["min_cod_surcharge"]) && drEach["min_cod_surcharge"].ToString()!="")
			{
				decimal decMinCODSurcharge = (decimal) drEach["min_cod_surcharge"];
				strBuilder.Append(decMinCODSurcharge);
			}
			else
			{
				strBuilder.Append("null");
			}

			// End

			//By Aoo 19/02/2008
			strBuilder.Append(",");
			if(Utility.IsNotDBNull(drEach["min_insurance_surcharge"]) && drEach["min_insurance_surcharge"].ToString()!="")
			{
				decimal decMin_insurance_surcharge = (decimal) drEach["min_insurance_surcharge"];
				strBuilder.Append(decMin_insurance_surcharge);
			}
			else
			{
				strBuilder.Append("null");
			}

			strBuilder.Append(",");
			if(Utility.IsNotDBNull(drEach["max_insurance_surcharge"]) && drEach["max_insurance_surcharge"].ToString()!="")
			{
				decimal decMax_insurance_surcharge = (decimal) drEach["max_insurance_surcharge"];
				strBuilder.Append(decMax_insurance_surcharge);
			}
			else
			{
				strBuilder.Append("null");
			}

			strBuilder.Append(",");
			if(Utility.IsNotDBNull(drEach["other_surcharge_amount"]) && drEach["other_surcharge_amount"].ToString()!="")
			{
				decimal decOther_surcharge_amount = (decimal) drEach["other_surcharge_amount"];
				strBuilder.Append(decOther_surcharge_amount);
			}
			else
			{
				strBuilder.Append("null");
			}

			strBuilder.Append(",");
			if(Utility.IsNotDBNull(drEach["other_surcharge_percentage"]) && drEach["other_surcharge_percentage"].ToString()!="")
			{
				decimal decOther_surcharge_percentage = (decimal) drEach["other_surcharge_percentage"];
				strBuilder.Append(decOther_surcharge_percentage);
			}
			else
			{
				strBuilder.Append("null");
			}

			strBuilder.Append(",");
			if(Utility.IsNotDBNull(drEach["other_surcharge_min"]) && drEach["other_surcharge_min"].ToString()!="")
			{
				decimal decOther_surcharge_min = (decimal) drEach["other_surcharge_min"];
				strBuilder.Append(decOther_surcharge_min);
			}
			else
			{
				strBuilder.Append("null");
			}

			strBuilder.Append(",");
			if(Utility.IsNotDBNull(drEach["other_surcharge_max"]) && drEach["other_surcharge_max"].ToString()!="")
			{
				decimal decOther_surcharge_max = (decimal) drEach["other_surcharge_max"];
				strBuilder.Append(decOther_surcharge_max);
			}
			else
			{
				strBuilder.Append("null");
			}

			strBuilder.Append(",");
			if(Utility.IsNotDBNull(drEach["other_surcharge_desc"]) && drEach["other_surcharge_desc"].ToString()!="")
			{
				string strOther_surcharge_desc = (String) drEach["other_surcharge_desc"];
				strBuilder.Append("N'");
				strBuilder.Append(Utility.ReplaceSingleQuote(strOther_surcharge_desc));
				strBuilder.Append("'");
			}
			else
			{
				strBuilder.Append("null");
			}

			strBuilder.Append(",");
			if(Utility.IsNotDBNull(drEach["discount_band"]) && drEach["discount_band"].ToString()!="")
			{
				string strDiscount_band = (String) drEach["discount_band"];
				strBuilder.Append("N'");
				strBuilder.Append(Utility.ReplaceSingleQuote(strDiscount_band));
				strBuilder.Append("'");
			}
			else
			{
				strBuilder.Append("null");
			}

			strBuilder.Append(",");
			if(Utility.IsNotDBNull(drEach["CustomerBOX"]) && drEach["CustomerBOX"].ToString()!="")
			{
				string strCustomerBOX = (String) drEach["CustomerBOX"];
				strBuilder.Append("N'");
				strBuilder.Append(Utility.ReplaceSingleQuote(strCustomerBOX));
				strBuilder.Append("'");
			}
			else
			{
				strBuilder.Append("null");
			}

			strBuilder.Append(",");
			if(Utility.IsNotDBNull(drEach["master_account"]) && drEach["master_account"].ToString()!="")
			{
				string strMaster_account = (String) drEach["master_account"];
				strBuilder.Append("N'");
				strBuilder.Append(Utility.ReplaceSingleQuote(strMaster_account));
				strBuilder.Append("'");
			}
			else
			{
				strBuilder.Append("null");
			}

			
			//End by Aoo
			//Added By Tom 22/7/09
			strBuilder.Append(",");
			if(Utility.IsNotDBNull(drEach["remark2"]) && drEach["remark2"].ToString()!="")
			{
				String strRemark2 = (String) drEach["remark2"];
				strBuilder.Append("N'"); //To accept Unicode
				strBuilder.Append(Utility.ReplaceSingleQuote(strRemark2));
				strBuilder.Append("'");
			}
			else
			{
				strBuilder.Append("null");
			}
			//End Added By Tom 22/7/09
			strBuilder.Append(",");
			if(Utility.IsNotDBNull(drEach["next_bill_placement_date"]) && drEach["next_bill_placement_date"].ToString()!="")
			{
				DateTime strnbpd = (DateTime) drEach["next_bill_placement_date"];
				strBuilder.Append("'");
				strBuilder.Append(strnbpd);
				strBuilder.Append("'");
			}
			else
			{
				strBuilder.Append("null");
			}
//			Comment by chai 03/02/2012
//			strBuilder.Append(",");
//			if(Utility.IsNotDBNull(drEach["credit_used"]) && drEach["credit_used"].ToString()!="")
//			{
//				decimal decCreditUsed = (decimal) drEach["credit_used"];
//				strBuilder.Append(decCreditUsed);
//			}
//			else
//			{
//				strBuilder.Append("null");
//			}
//			column credit_used
			strBuilder.Append(",0");
 
			strBuilder.Append(",");
			if(Utility.IsNotDBNull(drEach["creditstatus"]) && drEach["creditstatus"].ToString()!="")
			{
				string strCreditstatus = (String) drEach["creditstatus"];
				strBuilder.Append("N'");
				strBuilder.Append(Utility.ReplaceSingleQuote(strCreditstatus));
				strBuilder.Append("'");
			}
			else
			{
				strBuilder.Append("null");
			}
			strBuilder.Append(",");
			if(Utility.IsNotDBNull(drEach["creditthreshold"]) && drEach["creditthreshold"].ToString()!="")
			{
				decimal decCreditthreadhold = (decimal) drEach["creditthreshold"];
				strBuilder.Append(decCreditthreadhold);
			}
			else
			{
				strBuilder.Append("null");
			}

			strBuilder.Append(",");
			if(Utility.IsNotDBNull(drEach["Minimum_Box"]) && drEach["Minimum_Box"].ToString()!="")
			{
				int Minimum_Box = (int) drEach["Minimum_Box"];
				strBuilder.Append(Minimum_Box);
			}
			else
			{
				strBuilder.Append("null");
			}

			strBuilder.Append(",");
			if(Utility.IsNotDBNull(drEach["Dim_By_TOT"]) && drEach["Dim_By_TOT"].ToString()!="")
			{
				String strDim_By_TOT = (String) drEach["Dim_By_TOT"];
				strBuilder.Append("'");
				strBuilder.Append(strDim_By_TOT);
				strBuilder.Append("'");
			}
			else
			{
				strBuilder.Append("null");
			}


			strBuilder.Append(")");
		

			String strSQLQuery = strBuilder.ToString();

			IDbCommand dbCmd = dbCon.CreateCommand(strSQLQuery,CommandType.Text);
			

			try
			{

				iRowsAffected = dbCon.ExecuteNonQuery(dbCmd);
				Logger.LogTraceInfo("SysDataMgrDAL","AddStatusCodeDS","SDM001",iRowsAffected+" rows inserted in to Status_Code table");
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("RBACManager","AddCustomerProfileDS","SDM001","Error During Insert "+ appException.Message.ToString());
				throw new ApplicationException("Error inserting customer profile : "+appException.Message,appException);
			}



			return iRowsAffected;

		}

		public static DataTable GetCodeValues(String strAppID, String strCode,String enterpriseID)
		{
			DbConnection dbCon = null;
			IDbCommand dbCmd = null;
			String strQry = "";
			
			strQry = "select code_text,code_str_value from Core_System_Code  where  codeid = '" + strCode + "' and  applicationid ='" + strAppID + "'";
		
			dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,enterpriseID);
			dbCmd = dbCon.CreateCommand(strQry,CommandType.Text);
			DataSet  roleData = (DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
			//cnt = roleData.Tables[0].Rows.Count;
			
			return roleData.Tables[0];

		}
		public static int UpdateMasterAccountCostomer(double creditlimit , string masteracc , String strAppID, String strEnterpriseID)
		{
			int iRowsUpdated = 0;
			IDbCommand dbCom = null;
		
			

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("SysDataManager2","UpdateZone","SDMG001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			try
			{
				
					string strSQLQuery = " update customer ";
					strSQLQuery += " set credit_limit =" +  creditlimit.ToString();
					strSQLQuery += "where  master_account  ='" + masteracc + "' and applicationid = '" +  strAppID + "' and enterpriseid = '" + strEnterpriseID + "'";
	
					dbCom = dbCon.CreateCommand(strSQLQuery,CommandType.Text);
					iRowsUpdated = dbCon.ExecuteNonQuery(dbCom);
				
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("SysDataManager2","InsertZone","RBAC003",appException.Message.ToString());
				throw new ApplicationException("Update operation failed",appException);
				
			}
			return iRowsUpdated;
		}
		public static int AddCustomerCredit(String strApplicationid, String strEnterpriseID,
			String strCustid,String strCreditlimit,String  strCreditterms,
			String strPaymentmode,String strCreditstatus,String strCreditthreshold,String strReason,
			String strReasondescription,String strMACC_limits_credit_used,String strIsMasterAccount,String strUpdatedby)
		{
			int iRowsAffected = 0;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strApplicationid,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("SysDataMgrDAL","AddCustomerCredit","EDB101","DbConnection object is null!!");
				return iRowsAffected;
			}

			StringBuilder strBuilder = new StringBuilder();
			strBuilder.Append("insert into customer_Credit (applicationid, enterpriseid, custid, creditlimit, creditterms, paymentmode, creditstatus, creditthreshold, reason,reasondescription, MACC_limits_credit_used, isMasterAccount, updatedby, updateddate) values ('");;
			strBuilder.Append(strApplicationid.Trim().ToUpper());
			strBuilder.Append("'");
			strBuilder.Append(",");
			strBuilder.Append("'");
			strBuilder.Append(strEnterpriseID.Trim().ToUpper());
			strBuilder.Append("'");
			strBuilder.Append(",");
			strBuilder.Append("'");
			strBuilder.Append(strCustid.Trim().ToUpper());
			strBuilder.Append("'");
			strBuilder.Append(",");
			if(strCreditlimit.ToString()!="")
			{
				double decCreditOutstanding = Convert.ToDouble(strCreditlimit);
				strBuilder.Append(decCreditOutstanding);
			}
			else
			{
				strBuilder.Append("null");
			}
			strBuilder.Append(",");

			if(strCreditterms.ToString()!="")
			{
				double decCreditTerm = Convert.ToDouble(strCreditterms);
				strBuilder.Append(decCreditTerm);
			}
			else
			{
				strBuilder.Append("null");
			}
			strBuilder.Append(",");
			if(strPaymentmode.ToString()!="")
			{
				strBuilder.Append("'");
				strBuilder.Append(strPaymentmode);
				strBuilder.Append("'");
			}
			else
			{
				strBuilder.Append("null");
			}
			strBuilder.Append(",");

			if(strCreditstatus.ToString()!="")
			{
				strBuilder.Append("'");
				strBuilder.Append(strCreditstatus);
				strBuilder.Append("'");
			}
			else
			{
				strBuilder.Append("null");
			}
			strBuilder.Append(",");

			if(strCreditthreshold.ToString()!="")
			{
				string decCreditthreshold = strCreditthreshold.Replace("%","");
				strBuilder.Append(decCreditthreshold);
			}
			else
			{
				strBuilder.Append("null");
			}
			strBuilder.Append(",");

			if(strReason.ToString()!="")
			{
				strBuilder.Append("'");
				strBuilder.Append(strReason);
				strBuilder.Append("'");
			}
			else
			{
				strBuilder.Append("null");
			}
			strBuilder.Append(",");

			if(strReasondescription.ToString()!="")
			{
				strBuilder.Append("'");
				strBuilder.Append(strReasondescription);
				strBuilder.Append("'");
			}
			else
			{
				strBuilder.Append("null");
			}
			strBuilder.Append(",");



			if(strMACC_limits_credit_used.ToString()!="")
			{
				strBuilder.Append("'");
				strBuilder.Append(strMACC_limits_credit_used);
				strBuilder.Append("'");
			}
			else
			{
				strBuilder.Append("null");
			}
			strBuilder.Append(",");


			if(strIsMasterAccount.ToString()!="")
			{
				strBuilder.Append("'");
				strBuilder.Append(strIsMasterAccount);
				strBuilder.Append("'");
			}
			else
			{
				strBuilder.Append("null");
			}
			strBuilder.Append(",");

			if(strUpdatedby.ToString()!="")
			{
				strBuilder.Append("'");
				strBuilder.Append(strUpdatedby);
				strBuilder.Append("'");
			}
			else
			{
				strBuilder.Append("null");
			}
			strBuilder.Append(",");
			DateTime strNow = DateTime.Now ;
			strBuilder.Append("'");
			strBuilder.Append(strNow);
			strBuilder.Append("'");
			strBuilder.Append(")");
			String strSQLQuery = strBuilder.ToString();
			IDbCommand dbCmd = dbCon.CreateCommand(strSQLQuery,CommandType.Text);
			try
			{

				iRowsAffected = dbCon.ExecuteNonQuery(dbCmd);
				Logger.LogTraceInfo("SysDataMgrDAL","AddStatusCodeDS","SDM001",iRowsAffected+" rows inserted in to Status_Code table");
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("RBACManager","AddCustomerCredit","SDM001","Error During Insert "+ appException.Message.ToString());
				throw new ApplicationException("Error inserting customer profile : "+appException.Message,appException);
			}



			return iRowsAffected;

		}

		public static int ModifyCustomerProfileDS(String strAppID, String strEnterpriseID,DataSet dsToModify,string masteracc)
		{
			int iRowsAffected = 0;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("SysDataMgrDAL","ModifyCustomerProfileDS","EDB101","DbConnection object is null!!");
				return iRowsAffected;
			}
			int iRowCnt = dsToModify.Tables[0].Rows.Count;
			DataRow drEach = dsToModify.Tables[0].Rows[0];


			StringBuilder strBuilder = new StringBuilder();
			
			strBuilder.Append("update Customer set ref_code = ");//, status_close, invoiceable,system_code) values ('");
			if(Utility.IsNotDBNull(drEach["ref_code"]) && drEach["ref_code"].ToString()!="")
			{
				String strRefCode = (String) drEach["ref_code"];
				strBuilder.Append("'");
				strBuilder.Append(strRefCode);
				strBuilder.Append("'");
			}
			else
			{
				strBuilder.Append("null");
			}

			strBuilder.Append(", cust_name = ");
			if(Utility.IsNotDBNull(drEach["cust_name"]) && drEach["cust_name"].ToString()!="")
			{
				String strCustomerName = (String) drEach["cust_name"];
				strBuilder.Append("N'"); //To accept Unicode
				strBuilder.Append(Utility.ReplaceSingleQuote(strCustomerName));
				strBuilder.Append("'");
			}
			else
			{
				strBuilder.Append("null");
			}

			strBuilder.Append(", contact_person = ");
			if(Utility.IsNotDBNull(drEach["contact_person"]) && drEach["contact_person"].ToString()!="")
			{
				String strContactPerson = (String) drEach["contact_person"];
				strBuilder.Append("N'");
				strBuilder.Append(Utility.ReplaceSingleQuote(strContactPerson));
				strBuilder.Append("'");
			}
			else
			{
				strBuilder.Append("null");
			}
			strBuilder.Append(", address1 = ");
			if(Utility.IsNotDBNull(drEach["address1"]) && drEach["address1"].ToString()!="")
			{
				String strAddress1 = (String) drEach["address1"];
				strBuilder.Append("N'"); //To accept Unicode
				strBuilder.Append(Utility.ReplaceSingleQuote(strAddress1));
				strBuilder.Append("'");
			}
			else
			{
				strBuilder.Append("null");
			}
			strBuilder.Append(", address2 = ");
			if(Utility.IsNotDBNull(drEach["address2"]) && drEach["address2"].ToString()!="")
			{
				String strAddress2 = (String) drEach["address2"];
				strBuilder.Append("N'"); //To accept Unicode
				strBuilder.Append(Utility.ReplaceSingleQuote(strAddress2));
				strBuilder.Append("'");
			}
			else
			{
				strBuilder.Append("null");
			}
			strBuilder.Append(", country = ");
			if(Utility.IsNotDBNull(drEach["country"]) && drEach["country"].ToString()!="")
			{
				String strCountry = (String) drEach["country"];
				strBuilder.Append("'");
				strBuilder.Append(strCountry);
				strBuilder.Append("'");
			}
			else
			{
				strBuilder.Append("null");
			}
			strBuilder.Append(", zipcode = ");
			if(Utility.IsNotDBNull(drEach["zipcode"]) && drEach["zipcode"].ToString()!="")
			{
				String strZipcode = (String) drEach["zipcode"];
				strBuilder.Append("'");
				strBuilder.Append(strZipcode);
				strBuilder.Append("'");
			}
			else
			{
				strBuilder.Append("null");
			}

			strBuilder.Append(", state_code = ");
			if(Utility.IsNotDBNull(drEach["state_code"]) && drEach["state_code"].ToString()!="")
			{
				String strStateCode = (String) drEach["state_code"];
				strBuilder.Append("'");
				strBuilder.Append(strStateCode);
				strBuilder.Append("'");
			}
			else
			{
				strBuilder.Append("null");
			}

			strBuilder.Append(", telephone = ");
			if(Utility.IsNotDBNull(drEach["telephone"]) && drEach["telephone"].ToString()!="")
			{
				String strTelephone = (String) drEach["telephone"];
				strBuilder.Append("'");
				strBuilder.Append(Utility.ReplaceSingleQuote(strTelephone));
				strBuilder.Append("'");
			}
			else
			{
				strBuilder.Append("null");
			}
			strBuilder.Append(", mbg = ");
			if(Utility.IsNotDBNull(drEach["mbg"]) && drEach["mbg"].ToString()!="")
			{
				String strMBG = (String) drEach["mbg"];
				strBuilder.Append("'");
				strBuilder.Append(strMBG);
				strBuilder.Append("'");
			}
			else
			{
				strBuilder.Append("null");
			}
			strBuilder.Append(", dim_by_tot = ");
			if(Utility.IsNotDBNull(drEach["Dim_By_TOT"]) && drEach["Dim_By_TOT"].ToString()!="")
			{
				String strDim_By_TOT = (String) drEach["Dim_By_TOT"];
				strBuilder.Append("'");
				strBuilder.Append(strDim_By_TOT);
				strBuilder.Append("'");
			}
			else
			{
				strBuilder.Append("null");
			}
			strBuilder.Append(", fax = ");
			if(Utility.IsNotDBNull(drEach["fax"]) && drEach["fax"].ToString()!="")
			{
				String strFax = (String) drEach["fax"];
				strBuilder.Append("'");
				strBuilder.Append(Utility.ReplaceSingleQuote(strFax));
				strBuilder.Append("'");
			}
			else
			{
				strBuilder.Append("null");
			}
			strBuilder.Append(", credit_term = ");
			if(Utility.IsNotDBNull(drEach["credit_term"]) && drEach["credit_term"].ToString()!="")
			{
				int iCreditTerm = System.Convert.ToInt32( drEach["credit_term"]);
				strBuilder.Append(" ");
				strBuilder.Append(iCreditTerm);
				strBuilder.Append(" ");
			}
			else
			{
				strBuilder.Append("null");
			}
			strBuilder.Append(", industrial_sector_code = ");
			if(Utility.IsNotDBNull(drEach["industrial_sector_code"]) && drEach["industrial_sector_code"].ToString()!="")
			{
				String strISectorCode = (String) drEach["industrial_sector_code"];
				strBuilder.Append("'");
				strBuilder.Append(strISectorCode);
				strBuilder.Append("'");
			}
			else
			{
				strBuilder.Append("null");
			}
			strBuilder.Append(", credit_limit = ");
			if(Utility.IsNotDBNull(drEach["credit_limit"]) && drEach["credit_limit"].ToString()!="")
			{
				decimal decCreditLimit = (decimal) drEach["credit_limit"];
				strBuilder.Append(decCreditLimit);
			}
			else
			{
				strBuilder.Append("null");
			}
//			Comment by Chai 03/02/2012
//			strBuilder.Append(", credit_used = ");
//			if(Utility.IsNotDBNull(drEach["credit_used"]) && drEach["credit_used"].ToString()!="")
//			{
//				decimal decCreditUsed = (decimal) drEach["credit_used"];
//				strBuilder.Append(decCreditUsed);
//			}
//			else
//			{
//				strBuilder.Append("null");
//			}

			strBuilder.Append(", creditstatus = ");
			if(Utility.IsNotDBNull(drEach["creditstatus"]) && drEach["creditstatus"].ToString()!="")
			{
				String strCreditstatus = (String) drEach["creditstatus"];
				strBuilder.Append("'");
				strBuilder.Append(strCreditstatus);
				strBuilder.Append("'");
			}
			else
			{
				strBuilder.Append("null");
			}
			strBuilder.Append(", creditthreshold = ");
			if(Utility.IsNotDBNull(drEach["creditthreshold"]) && drEach["creditthreshold"].ToString()!="")
			{
				decimal decCreditthreadhold = (decimal) drEach["creditthreshold"];
				strBuilder.Append(decCreditthreadhold);
			}
			else
			{
				strBuilder.Append("null");
			}


			strBuilder.Append(", active_quotation_no = ");
			if(Utility.IsNotDBNull(drEach["active_quotation_no"]) && drEach["active_quotation_no"].ToString()!="")
			{
				String strActiveQuotationNo = (String) drEach["active_quotation_no"];
				strBuilder.Append("'");
				strBuilder.Append(strActiveQuotationNo);
				strBuilder.Append("'");
			}
			else
			{
				strBuilder.Append("null");
			}
			strBuilder.Append(", status_active = ");
			if(Utility.IsNotDBNull(drEach["status_active"]) && drEach["status_active"].ToString()!="")
			{
				String strStatusActive = (String) drEach["status_active"];
				strBuilder.Append("'");
				strBuilder.Append(strStatusActive);
				strBuilder.Append("'");
			}
			else
			{
				strBuilder.Append("null");
			}
			strBuilder.Append(", credit_outstanding = ");
			if(Utility.IsNotDBNull(drEach["credit_outstanding"]) && drEach["credit_outstanding"].ToString()!="")
			{
				decimal decCreditOutstanding = (decimal) drEach["credit_outstanding"];
				strBuilder.Append(decCreditOutstanding);
			}
			else
			{
				strBuilder.Append("null");
			}
			strBuilder.Append(", remark = ");
			if(Utility.IsNotDBNull(drEach["remark"]) && drEach["remark"].ToString()!="")
			{
				String strRemark = (String) drEach["remark"];
				strBuilder.Append("N'"); //To accept Unicode
				strBuilder.Append(Utility.ReplaceSingleQuote(strRemark));
				strBuilder.Append("'");
			}
			else
			{
				strBuilder.Append("null");
			}
			strBuilder.Append(", SalesmanID = ");
			if(Utility.IsNotDBNull(drEach["SalesmanID"]) && drEach["SalesmanID"].ToString()!="")
			{
				String strSalesmanID = (String) drEach["SalesmanID"];
				strBuilder.Append("'");
				strBuilder.Append(Utility.ReplaceSingleQuote(strSalesmanID));
				strBuilder.Append("'");
			}
			else
			{
				strBuilder.Append("null");
			}
			strBuilder.Append(", prom_tot_wt = ");
			if(Utility.IsNotDBNull(drEach["prom_tot_wt"]) && drEach["prom_tot_wt"].ToString()!="")
			{
				decimal decPromisedTotalWt = (decimal) drEach["prom_tot_wt"];
				strBuilder.Append(decPromisedTotalWt);
			}
			else
			{
				strBuilder.Append("null");
			}
			strBuilder.Append(", payment_mode = ");
			if(Utility.IsNotDBNull(drEach["payment_mode"]) && drEach["payment_mode"].ToString()!="")
			{
				String strPaymentMode = (String) drEach["payment_mode"];
				strBuilder.Append("'");
				strBuilder.Append(strPaymentMode);
				strBuilder.Append("'");
			}
			else
			{
				strBuilder.Append("null");
			}
			strBuilder.Append(", prom_tot_package = ");
			if(Utility.IsNotDBNull(drEach["prom_tot_package"]) && drEach["prom_tot_package"].ToString()!="")
			{
				int iPromisedTotalPackage = System.Convert.ToInt32( drEach["prom_tot_package"]);
				strBuilder.Append(iPromisedTotalPackage);
			}
			else
			{
				strBuilder.Append("null");
			}
			strBuilder.Append(", prom_period = ");
			if(Utility.IsNotDBNull(drEach["prom_period"]) && drEach["prom_period"].ToString()!="")
			{
				int iPromisedPeriod = System.Convert.ToInt32( drEach["prom_period"]);
				strBuilder.Append(iPromisedPeriod);
			}
			else
			{
				strBuilder.Append("null");
			}
			strBuilder.Append(", free_insurance_amt = ");
			if(Utility.IsNotDBNull(drEach["free_insurance_amt"]) && drEach["free_insurance_amt"].ToString()!="")
			{
				decimal decFreeInsuranceAmt = (decimal) drEach["free_insurance_amt"];
				strBuilder.Append(decFreeInsuranceAmt);
			}
			else
			{
				strBuilder.Append("null");
			}
			strBuilder.Append(", insurance_percent_surcharge = ");
			if(Utility.IsNotDBNull(drEach["insurance_percent_surcharge"]) && drEach["insurance_percent_surcharge"].ToString()!="")
			{
				decimal decInsurancePercentSurcharge = (decimal) drEach["insurance_percent_surcharge"];
				strBuilder.Append(decInsurancePercentSurcharge);
			}
			else
			{
				strBuilder.Append("null");
			}
			strBuilder.Append(", email = ");
			if(Utility.IsNotDBNull(drEach["email"]) && drEach["email"].ToString()!="")
			{
				String strEmail = (String) drEach["email"];
				strBuilder.Append("'");
				strBuilder.Append(Utility.ReplaceSingleQuote(strEmail));
				strBuilder.Append("'");
			}
			else
			{
				strBuilder.Append("null");
			}
			strBuilder.Append(", created_by = ");
			if(Utility.IsNotDBNull(drEach["created_by"]) && drEach["created_by"].ToString()!="")
			{
				String strCreatedBy = (String) drEach["created_by"];
				strBuilder.Append("'");
				strBuilder.Append(strCreatedBy);
				strBuilder.Append("'");
			}
			else
			{
				strBuilder.Append("null");
			}
			strBuilder.Append(", apply_dim_wt = ");
			if(Utility.IsNotDBNull(drEach["apply_dim_wt"]) && drEach["apply_dim_wt"].ToString()!="")
			{
				String strApplyDimWt = (String) drEach["apply_dim_wt"];
				strBuilder.Append("'");
				strBuilder.Append(strApplyDimWt);
				strBuilder.Append("'");
			}
			else
			{
				strBuilder.Append("null");
			}
			strBuilder.Append(", pod_slip_required = ");
			if(Utility.IsNotDBNull(drEach["pod_slip_required"]) && drEach["pod_slip_required"].ToString()!="")
			{
				String strPodSlipRequired = (String) drEach["pod_slip_required"];
				strBuilder.Append("'");
				strBuilder.Append(strPodSlipRequired);
				strBuilder.Append("'");
			}
			else
			{
				strBuilder.Append("null");
			}
			strBuilder.Append(", apply_esa_surcharge = ");
			if(Utility.IsNotDBNull(drEach["apply_esa_surcharge"]) && drEach["apply_esa_surcharge"].ToString()!="")
			{
				String strApplyESASurcharge = (String) drEach["apply_esa_surcharge"];
				strBuilder.Append("'");
				strBuilder.Append(strApplyESASurcharge);
				strBuilder.Append("'");

			}
			else
			{
				strBuilder.Append("null");
			}

			strBuilder.Append(", apply_esa_surcharge_rep = ");
			if(Utility.IsNotDBNull(drEach["apply_esa_surcharge_rep"]) && drEach["apply_esa_surcharge_rep"].ToString()!="")
			{
				String strApplyESASurchargeDel = (String) drEach["apply_esa_surcharge_rep"];
				strBuilder.Append("'");
				strBuilder.Append(strApplyESASurchargeDel);
				strBuilder.Append("'");

			}
			else
			{
				strBuilder.Append("null");
			}


			// for Invoice Return Days
			strBuilder.Append(", invoice_return_days = ");
			if(Utility.IsNotDBNull(drEach["invoice_return_days"]) && drEach["invoice_return_days"].ToString()!="")
			{
				String strInvoiceReturn = (String) drEach["invoice_return_days"].ToString();
				strBuilder.Append("'");
				strBuilder.Append(strInvoiceReturn);
				strBuilder.Append("'");

			}
			else
			{
				strBuilder.Append("null");
			}

			strBuilder.Append(", cod_surcharge_amt = ");
			if(Utility.IsNotDBNull(drEach["cod_surcharge_amt"]) && drEach["cod_surcharge_amt"].ToString()!="")
			{
				decimal deccod_surcharge_amt = (decimal) drEach["cod_surcharge_amt"];
				strBuilder.Append(deccod_surcharge_amt);

			}
			else
			{
				strBuilder.Append("null");
			}

			strBuilder.Append(", cod_surcharge_percent = ");
			if(Utility.IsNotDBNull(drEach["cod_surcharge_percent"]) && drEach["cod_surcharge_percent"].ToString()!="")
			{
				decimal deccod_surcharge_percent = (decimal) drEach["cod_surcharge_percent"];
				strBuilder.Append(deccod_surcharge_percent);

			}
			else
			{
				strBuilder.Append("null");
			}

			strBuilder.Append(", insurance_maximum_amt = ");
			if(Utility.IsNotDBNull(drEach["insurance_maximum_amt"]) && drEach["insurance_maximum_amt"].ToString()!="")
			{
				decimal decinsurance_maximum_amt = (decimal) drEach["insurance_maximum_amt"];
				strBuilder.Append(decinsurance_maximum_amt);

			}
			else
			{
				strBuilder.Append("null");
			}

			strBuilder.Append(", density_factor = ");
			if(Utility.IsNotDBNull(drEach["density_factor"]) && drEach["density_factor"].ToString()!="")
			{
				decimal decdensity_factor = (decimal) drEach["density_factor"];
				strBuilder.Append(decdensity_factor);

			}
			else
			{
				strBuilder.Append("null");
			}

			strBuilder.Append(", payer_type = ");
			if(Utility.IsNotDBNull(drEach["payer_type"]) && drEach["payer_type"].ToString()!="")
			{
				String strpayer_type = (String) drEach["payer_type"].ToString();
				strBuilder.Append("'");
				strBuilder.Append(strpayer_type);
				strBuilder.Append("'");

			}
			else
			{
				strBuilder.Append("null");
			}

			//HC Return Task
			strBuilder.Append(", hc_invoice_required = ");
			if(Utility.IsNotDBNull(drEach["hc_invoice_required"]) && drEach["hc_invoice_required"].ToString()!="")
			{
				String strHCInvoiceRequired = (String) drEach["hc_invoice_required"];
				strBuilder.Append("'");
				strBuilder.Append(strHCInvoiceRequired);
				strBuilder.Append("'");
			}
			else
			{
				strBuilder.Append("null");
			}
			
			// by Ching Nov 29,2007
			strBuilder.Append(", max_cod_surcharge = ");
			if(Utility.IsNotDBNull(drEach["max_cod_surcharge"]) && drEach["max_cod_surcharge"].ToString()!="")
			{
				decimal decMaxCODSurcharge = (decimal) drEach["max_cod_surcharge"];
				strBuilder.Append(decMaxCODSurcharge);

			}
			else
			{
				strBuilder.Append("null");
			}

			strBuilder.Append(", min_cod_surcharge = ");
			if(Utility.IsNotDBNull(drEach["min_cod_surcharge"]) && drEach["min_cod_surcharge"].ToString()!="")
			{
				decimal decMin_cod_surcharge = (decimal) drEach["min_cod_surcharge"];
				strBuilder.Append(decMin_cod_surcharge);

			}
			else
			{
				strBuilder.Append("null");
			}

			//By Aoo 18/02/2008
			strBuilder.Append(", min_insurance_surcharge = ");
			if(Utility.IsNotDBNull(drEach["min_insurance_surcharge"]) && drEach["min_insurance_surcharge"].ToString()!="")
			{
				decimal decMinInsuranceSurcharge = (decimal) drEach["min_insurance_surcharge"];
				strBuilder.Append(decMinInsuranceSurcharge);

			}
			else
			{
				strBuilder.Append("null");
			}
			strBuilder.Append(", max_insurance_surcharge = ");
			if(Utility.IsNotDBNull(drEach["max_insurance_surcharge"]) && drEach["max_insurance_surcharge"].ToString()!="")
			{
				decimal decMaxInsuranceSurcharge = (decimal) drEach["max_insurance_surcharge"];
				strBuilder.Append(decMaxInsuranceSurcharge);

			}
			else
			{
				strBuilder.Append("null");
			}
			strBuilder.Append(", other_surcharge_amount = ");
			if(Utility.IsNotDBNull(drEach["other_surcharge_amount"]) && drEach["other_surcharge_amount"].ToString()!="")
			{
				decimal decOtherSurchargeAmount = (decimal) drEach["other_surcharge_amount"];
				strBuilder.Append(decOtherSurchargeAmount);

			}
			else
			{
				strBuilder.Append("null");
			}
			strBuilder.Append(", other_surcharge_percentage = ");
			if(Utility.IsNotDBNull(drEach["other_surcharge_percentage"]) && drEach["other_surcharge_percentage"].ToString()!="")
			{
				decimal decOtherSurchargePercentage = (decimal) drEach["other_surcharge_percentage"];
				strBuilder.Append(decOtherSurchargePercentage);

			}
			else
			{
				strBuilder.Append("null");
			}
			strBuilder.Append(", other_surcharge_min = ");
			if(Utility.IsNotDBNull(drEach["other_surcharge_min"]) && drEach["other_surcharge_min"].ToString()!="")
			{
				decimal decOtherSurchargeMin = (decimal) drEach["other_surcharge_min"];
				strBuilder.Append(decOtherSurchargeMin);

			}
			else
			{
				strBuilder.Append("null");
			}
			strBuilder.Append(", other_surcharge_max = ");
			if(Utility.IsNotDBNull(drEach["other_surcharge_max"]) && drEach["other_surcharge_max"].ToString()!="")
			{
				decimal decOtherSurchargeMax = (decimal) drEach["other_surcharge_max"];
				strBuilder.Append(decOtherSurchargeMax);

			}
			else
			{
				strBuilder.Append("null");
			}
			strBuilder.Append(", other_surcharge_desc = ");
			if(Utility.IsNotDBNull(drEach["other_surcharge_desc"]) && drEach["other_surcharge_desc"].ToString()!="")
			{
				string strOtherSurchargeDesc = (String) drEach["other_surcharge_desc"];
				strBuilder.Append("N'");
				strBuilder.Append(Utility.ReplaceSingleQuote(strOtherSurchargeDesc));
				strBuilder.Append("'");
			}
			else
			{
				strBuilder.Append("null");
			}
			strBuilder.Append(", discount_band = ");
			if(Utility.IsNotDBNull(drEach["discount_band"]) && drEach["discount_band"].ToString()!="")
			{
				string strDiscountBand = (String) drEach["discount_band"];
				strBuilder.Append("N'");
				strBuilder.Append(Utility.ReplaceSingleQuote(strDiscountBand));
				strBuilder.Append("'");
			}
			else
			{
				strBuilder.Append("null");
			}
			strBuilder.Append(", CustomerBox = ");
			if(Utility.IsNotDBNull(drEach["CustomerBox"]) && drEach["CustomerBox"].ToString()!="")
			{
				string strBOX = (String) drEach["CustomerBox"];
				strBuilder.Append("N'");
				strBuilder.Append(Utility.ReplaceSingleQuote(strBOX));
				strBuilder.Append("'");
			}
			else
			{
				strBuilder.Append("null");
			}

			strBuilder.Append(", master_account = ");
			//if(Utility.IsNotDBNull(drEach["master_account"]) && drEach["master_account"].ToString()!="")
			if(masteracc.Length > 0)
			{
			
				strBuilder.Append("N'");
				strBuilder.Append(Utility.ReplaceSingleQuote(masteracc));
				strBuilder.Append("'");
			}
			else
			{
				strBuilder.Append("null");
			}

			//End By Aoo
			// End/
			//Added By Tom 22/7/09
			strBuilder.Append(", remark2 = ");
			if(Utility.IsNotDBNull(drEach["remark2"]) && drEach["remark2"].ToString()!="")
			{
				String strRemark2 = (String) drEach["remark2"];
				strBuilder.Append("N'"); //To accept Unicode
				strBuilder.Append(Utility.ReplaceSingleQuote(strRemark2));
				strBuilder.Append("'");
			}
			else
			{
				strBuilder.Append("null");
			}
			//End Added By Tom 22/7/09
			strBuilder.Append(", next_bill_placement_date = ");
			if(Utility.IsNotDBNull(drEach["next_bill_placement_date"]) && drEach["next_bill_placement_date"].ToString()!="")
			{
				DateTime strnbpd = (DateTime)drEach["next_bill_placement_date"];
				strBuilder.Append("'");
				strBuilder.Append(strnbpd);
				strBuilder.Append("'");
			}
			else
			{
				strBuilder.Append("null");
			}
			//HC Return Task
			strBuilder.Append(", Minimum_Box = ");
			if(Utility.IsNotDBNull(drEach["Minimum_Box"]) && drEach["Minimum_Box"].ToString()!="")
			{
				int minimumBox = (int) drEach["Minimum_Box"];
				strBuilder.Append(minimumBox);

			}
			else
			{
				strBuilder.Append("null");
			}

			strBuilder.Append(" where applicationid = '");
			strBuilder.Append(strAppID);
			strBuilder.Append("' and enterpriseid = '");
			strBuilder.Append(strEnterpriseID);
			strBuilder.Append("' and custid = '");
			String strCustomerAccNo = (String) drEach["custid"];
			strBuilder.Append(strCustomerAccNo);
			strBuilder.Append("' ");

			String strSQLQuery = strBuilder.ToString();

			IDbCommand dbCmd = dbCon.CreateCommand(strSQLQuery,CommandType.Text);
			
			try
			{
				iRowsAffected = dbCon.ExecuteNonQuery(dbCmd);
				Logger.LogTraceInfo("SysDataMgrDAL","ModifyCustomerProfileDS","SDM001",iRowsAffected+" rows inserted in to Customer table");
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("RBACManager","ModifyCustomerProfileDS","SDM001","Error During Insert "+ appException.Message.ToString());
				throw new ApplicationException("Error modifying Customer Profile : "+appException.Message,appException);
			}
			return iRowsAffected;
		}

		public static SessionDS GetEmptyCustomerProfileDS(int iNumRows)
		{
			

			DataTable dtCustomerProfile = new DataTable();
 
			dtCustomerProfile.Columns.Add(new DataColumn("custid", typeof(string)));
			dtCustomerProfile.Columns.Add(new DataColumn("ref_code", typeof(string)));
			dtCustomerProfile.Columns.Add(new DataColumn("cust_name", typeof(string)));
			dtCustomerProfile.Columns.Add(new DataColumn("contact_person", typeof(string)));
			dtCustomerProfile.Columns.Add(new DataColumn("address1", typeof(string)));
			dtCustomerProfile.Columns.Add(new DataColumn("address2", typeof(string)));
			dtCustomerProfile.Columns.Add(new DataColumn("country", typeof(string)));
			dtCustomerProfile.Columns.Add(new DataColumn("zipcode", typeof(string)));
			dtCustomerProfile.Columns.Add(new DataColumn("state_code", typeof(string)));
			dtCustomerProfile.Columns.Add(new DataColumn("telephone", typeof(string)));
			dtCustomerProfile.Columns.Add(new DataColumn("mbg", typeof(string)));

			dtCustomerProfile.Columns.Add(new DataColumn("fax", typeof(string)));
			dtCustomerProfile.Columns.Add(new DataColumn("credit_term", typeof(int)));
			dtCustomerProfile.Columns.Add(new DataColumn("industrial_sector_code", typeof(string)));
			dtCustomerProfile.Columns.Add(new DataColumn("credit_limit", typeof(decimal)));
			dtCustomerProfile.Columns.Add(new DataColumn("active_quotation_no", typeof(string)));
			dtCustomerProfile.Columns.Add(new DataColumn("status_active", typeof(string)));
			dtCustomerProfile.Columns.Add(new DataColumn("credit_outstanding", typeof(decimal)));
			dtCustomerProfile.Columns.Add(new DataColumn("remark", typeof(string)));
			dtCustomerProfile.Columns.Add(new DataColumn("SalesmanID", typeof(string)));
			dtCustomerProfile.Columns.Add(new DataColumn("prom_tot_wt", typeof(decimal)));

			dtCustomerProfile.Columns.Add(new DataColumn("payment_mode", typeof(string)));
			dtCustomerProfile.Columns.Add(new DataColumn("prom_tot_package", typeof(int)));
			dtCustomerProfile.Columns.Add(new DataColumn("prom_period", typeof(int)));
			dtCustomerProfile.Columns.Add(new DataColumn("free_insurance_amt", typeof(decimal)));
			dtCustomerProfile.Columns.Add(new DataColumn("insurance_percent_surcharge", typeof(decimal)));
			dtCustomerProfile.Columns.Add(new DataColumn("email", typeof(string)));
			dtCustomerProfile.Columns.Add(new DataColumn("created_by", typeof(string)));
			dtCustomerProfile.Columns.Add(new DataColumn("apply_dim_wt", typeof(string)));
			dtCustomerProfile.Columns.Add(new DataColumn("pod_slip_required", typeof(string)));
			dtCustomerProfile.Columns.Add(new DataColumn("apply_esa_surcharge", typeof(string)));
			//add new columns is "apply_esa_surcharge_rep"
			dtCustomerProfile.Columns.Add(new DataColumn("apply_esa_surcharge_rep",typeof(string)));
			dtCustomerProfile.Columns.Add(new DataColumn("invoice_return_days", typeof(int)));
			dtCustomerProfile.Columns.Add(new DataColumn("cod_surcharge_amt", typeof(decimal)));
			dtCustomerProfile.Columns.Add(new DataColumn("cod_surcharge_percent", typeof(decimal)));
			dtCustomerProfile.Columns.Add(new DataColumn("insurance_maximum_amt", typeof(decimal)));
			dtCustomerProfile.Columns.Add(new DataColumn("density_factor", typeof(decimal)));
			dtCustomerProfile.Columns.Add(new DataColumn("payer_type", typeof(string)));
			//HC Return Task
			dtCustomerProfile.Columns.Add(new DataColumn("hc_invoice_required", typeof(string)));
			//HC Return Task
			// by Ching Nov 29,2007
			dtCustomerProfile.Columns.Add(new DataColumn("max_cod_surcharge", typeof(decimal)));
			dtCustomerProfile.Columns.Add(new DataColumn("min_cod_surcharge", typeof(decimal)));
			// End
			// By Aoo
			dtCustomerProfile.Columns.Add(new DataColumn("min_insurance_surcharge",typeof(decimal)));
			dtCustomerProfile.Columns.Add(new DataColumn("max_insurance_surcharge",typeof(decimal)));
													
			dtCustomerProfile.Columns.Add(new DataColumn("other_surcharge_amount",typeof(decimal)));
			dtCustomerProfile.Columns.Add(new DataColumn("other_surcharge_percentage",typeof(decimal)));
			dtCustomerProfile.Columns.Add(new DataColumn("other_surcharge_min",typeof(decimal)));
			dtCustomerProfile.Columns.Add(new DataColumn("other_surcharge_max",typeof(decimal)));
			dtCustomerProfile.Columns.Add(new DataColumn("other_surcharge_desc",typeof(string)));
			dtCustomerProfile.Columns.Add(new DataColumn("discount_band",typeof(string)));
			dtCustomerProfile.Columns.Add(new DataColumn("master_account",typeof(string)));
			dtCustomerProfile.Columns.Add(new DataColumn("CustomerBox",typeof(string)));
			
			//end by aoo.
			//Added By Tom 22/7/09
			dtCustomerProfile.Columns.Add(new DataColumn("remark2",typeof(string)));
			//End Added By Tom 22/7/09
			dtCustomerProfile.Columns.Add(new DataColumn("next_bill_placement_date",typeof(DateTime)));
			dtCustomerProfile.Columns.Add(new DataColumn("Minimum_Box",typeof(int)));
			dtCustomerProfile.Columns.Add(new DataColumn("Dim_By_TOT", typeof(string)));  //Jeab 8 Dec 10
			dtCustomerProfile.Columns.Add(new DataColumn("Commission_Datetime", typeof(string)));  //Jeab 08 Mar 12
			dtCustomerProfile.Columns.Add(new DataColumn("CustCategory", typeof(string)));  //Jeab 08 Mar 12
			//Added By Tu 8/3/10
			dtCustomerProfile.Columns.Add(new DataColumn("credit_used",typeof(String)));
			dtCustomerProfile.Columns.Add(new DataColumn("creditstatus",typeof(String)));
			dtCustomerProfile.Columns.Add(new DataColumn("creditthreshold",typeof(decimal)));
			//End By Tu 8/3/10
			for(int i=0; i < iNumRows; i++)
			{
				DataRow drEach = dtCustomerProfile.NewRow();
				dtCustomerProfile.Rows.Add(drEach);
			}

			DataSet dsCustomerProfile = new DataSet();
			dsCustomerProfile.Tables.Add(dtCustomerProfile);

			SessionDS sessionDS = new SessionDS();
			sessionDS.ds = dsCustomerProfile;
			sessionDS.DataSetRecSize = dsCustomerProfile.Tables[0].Rows.Count;
			sessionDS.QueryResultMaxSize = sessionDS.DataSetRecSize;

			return  sessionDS;
	
		}

		/// <summary>
		/// Customized code for customer
		/// </summary>
		/// <param name="iNumRows"></param>
		/// <returns></returns>
		public static SessionDS GetCustomerDS(int iNumRows)
		{
			DataTable dtAgentProfile = new DataTable(); 
			dtAgentProfile.Columns.Add(new DataColumn("custid", typeof(string)));
			dtAgentProfile.Columns.Add(new DataColumn("cust_name", typeof(string)));
			
			for(int i=0; i < iNumRows; i++)
			{
				DataRow drEach = dtAgentProfile.NewRow();
				dtAgentProfile.Rows.Add(drEach);
			}

			DataSet dsAgentProfile = new DataSet();
			dsAgentProfile.Tables.Add(dtAgentProfile);
			SessionDS sessionDS = new SessionDS();
			sessionDS.ds = dsAgentProfile;
			sessionDS.DataSetRecSize = dsAgentProfile.Tables[0].Rows.Count;
			sessionDS.QueryResultMaxSize = sessionDS.DataSetRecSize;

			return  sessionDS;
	
		}
		public static SessionDS GetCustDS(String strAppID, String strEnterpriseID, int iCurrent, int iDSRecSize,DataSet dsQueryParam)
		{
			SessionDS sessionDS = null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("AgentDataMgrDAL","GetAgentProfileDS","EDB101","DbConnection object is null!!");
				return sessionDS;
			}

			StringBuilder strBuilder = new StringBuilder();
			//strBuilder.Append("select custid,ref_code,cust_name,contact_person,address1,address2,country,zipcode,telephone,mbg,fax,credit_term,industrial_sector_code,credit_limit,active_quotation_no,status_active,credit_outstanding,remark,salesmanid,prom_tot_wt,payment_mode,prom_tot_package,prom_period,free_insurance_amt,insurance_percent_surcharge,email,created_by,apply_dim_wt,pod_slip_required,apply_esa_surcharge from Agent where applicationid = '");
			strBuilder.Append ("Select custid,cust_name ");
			strBuilder.Append (" FROM customer where applicationid ='"+strAppID+"' and enterpriseid ='"+strEnterpriseID+"'");

			DataRow drEach = dsQueryParam.Tables[0].Rows[0];

			//agentid
			if((drEach["custid"]!= null) && (!drEach["custid"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				String strAgentAccNo = (String) drEach["agentid"];
				strBuilder.Append(" and custid like ");
				strBuilder.Append("'%");
				strBuilder.Append(strAgentAccNo);
				strBuilder.Append("%' ");
			}	
			if((drEach["cust_name"]!= null) && (!drEach["cust_name"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				String strAgentName = (String) drEach["agent_name"];
				strBuilder.Append(" and cust_name like ");
				strBuilder.Append("N'%");
				strBuilder.Append(Utility.ReplaceSingleQuote(strAgentName));
				strBuilder.Append("%' ");
			}

			strBuilder.Append (" Order by cust_name");
			String strSQLQuery = strBuilder.ToString();			
			sessionDS = dbCon.ExecuteQuery(strSQLQuery,iCurrent,iDSRecSize,"AgentProfileTable");

			return  sessionDS;
	
		}
		public static SessionDS GetCustomerDS(String strAppID, String strEnterpriseID, int iCurrent, int iDSRecSize,DataSet dsQueryParam)
		{
			SessionDS sessionDS = null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("AgentDataMgrDAL","GetAgentProfileDS","EDB101","DbConnection object is null!!");
				return sessionDS;
			}

			StringBuilder strBuilder = new StringBuilder();
			//strBuilder.Append("select custid,ref_code,cust_name,contact_person,address1,address2,country,zipcode,telephone,mbg,fax,credit_term,industrial_sector_code,credit_limit,active_quotation_no,status_active,credit_outstanding,remark,salesmanid,prom_tot_wt,payment_mode,prom_tot_package,prom_period,free_insurance_amt,insurance_percent_surcharge,email,created_by,apply_dim_wt,pod_slip_required,apply_esa_surcharge from Agent where applicationid = '");
			strBuilder.Append ("Select custid,cust_name ");
			strBuilder.Append (" FROM customer where applicationid ='"+strAppID+"' and enterpriseid ='"+strEnterpriseID+"'");

			DataRow drEach = dsQueryParam.Tables[0].Rows[0];

			//agentid
			if((drEach["custid"]!= null) && (!drEach["custid"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				String strAgentAccNo = (String) drEach["custid"];
				strBuilder.Append(" and custid like ");
				strBuilder.Append("'%");
				strBuilder.Append(strAgentAccNo);
				strBuilder.Append("%' ");
			}	
			if((drEach["cust_name"]!= null) && (!drEach["cust_name"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				String strAgentName = (String) drEach["cust_name"];
				strBuilder.Append(" and cust_name like ");
				strBuilder.Append("N'%");
				strBuilder.Append(Utility.ReplaceSingleQuote(strAgentName));
				strBuilder.Append("%' ");
			}

			strBuilder.Append (" Order by cust_name");
			String strSQLQuery = strBuilder.ToString();			
			sessionDS = dbCon.ExecuteQuery(strSQLQuery,iCurrent,iDSRecSize,"AgentProfileTable");

			return  sessionDS;
	
		}

		public static int AddCustCustomizedCodeDS(String strAppID, String strEnterpriseID,String strAgentID,DataSet dsToInsert)
		{
			int iRowsAffected = 0;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("SysDataMgrDAL","GetServiceCodeDS","EDB101","DbConnection object is null!!");
				return iRowsAffected;
			}
			StringBuilder strBuilder = new StringBuilder();
			strBuilder.Append("insert into customer_assembly (applicationid,enterpriseid,custid,customized_assembly_code,assemblyid) values ('");
			strBuilder.Append(strAppID);
			strBuilder.Append("','");
			strBuilder.Append(strEnterpriseID);
			strBuilder.Append("','");
			strBuilder.Append(strAgentID);
			strBuilder.Append("',");

			int iRowCnt = dsToInsert.Tables[0].Rows.Count;

			DataRow drEach = dsToInsert.Tables[0].Rows[0];
				
			Decimal strServiceCode = Convert.ToDecimal(drEach["customized_assembly_code"].ToString());
			strBuilder.Append(strServiceCode);
			strBuilder.Append(",'");

			String strServiceDescription = (String) drEach["assemblyid"];
			strBuilder.Append(Utility.ReplaceSingleQuote(strServiceDescription));
			strBuilder.Append("')");	

			String strSQLQuery = strBuilder.ToString();

			IDbCommand dbCmd = dbCon.CreateCommand(strSQLQuery,CommandType.Text);
			

			try
			{
				iRowsAffected = dbCon.ExecuteNonQuery(dbCmd);
				Logger.LogTraceInfo("SysDataMgrDAL","AddServiceCodeDS","SDM001",iRowsAffected+" rows inserted in to Service table");
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("RBACManager","AddServiceCodeDS","SDM001","Error During Insert "+ appException.Message.ToString());
				throw new ApplicationException("Error inserting Service Code ",appException);
			}
			return iRowsAffected;

		}

		public static SessionDS GetCustCustomizedCodeDS(String strAppID, String strEnterpriseID,String strAgentID,int iCurrent, int iDSRecSize,DataSet dsQueryParam)
		{

			SessionDS sessionDS = null;

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("SysDataMgrDAL","GetServiceCodeDS","EDB101","DbConnection object is null!!");
				return sessionDS;
			}

			StringBuilder strBuilder = new StringBuilder();
			strBuilder.Append(" select distinct ag.customized_assembly_code,a.assemblyid,a.assembly_description,s.code_text,s.code_num_value, s.codeid from customer_assembly ag ,core_system_code s,assembly a ");
			strBuilder.Append(" where  a.assemblyid = ag.assemblyid and ag.customized_assembly_code = s.code_num_value and ag.custid='"+strAgentID+"' and codeid='customized_assembly'  and ag.applicationid='"+strAppID +"' and ag.enterpriseid='"+strEnterpriseID+"'");			

			String strSQLQuery = strBuilder.ToString();			
			sessionDS = dbCon.ExecuteQuery(strSQLQuery,iCurrent,iDSRecSize,"ServiceCodeTable");

			return  sessionDS;
	
		}
		public static int DeleteCustCustomizedCodeDS(String strAppID, String strEnterpriseID,String strAgentID,String strServiceCode)
		{
			int iRowsDeleted = 0;
			DbConnection dbConApp = null;
			IDbCommand dbCommandApp = null;

			IDbTransaction transactionApp = null;
			IDbConnection conApp = null;

	
			//Get App dbConnection

			dbConApp = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbConApp == null)
			{
				Logger.LogTraceError("SysDataMgrDAL","DeleteServiceCodeDS","SDM002","dbConApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			//Begin Core and App Transaction

			conApp = dbConApp.GetConnection();
			
			if(conApp == null)
			{
				Logger.LogTraceError("SysDataMgrDAL","DeleteServiceCodeDS","SDM002","conApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			try
			{
				dbConApp.OpenConnection(ref conApp);
			}
			catch(ApplicationException appException)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("SysDataMgrDAL","DeleteServiceCodeDS","SDM003","Error opening connection : "+ appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ",appException);
			}
			catch(Exception exception)
			{
				if(conApp != null)
				{
					conApp.Close();
				}

				Logger.LogTraceError("SysDataMgrDAL","DeleteServiceCodeDS","SDM003","Error opening connection : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}


			transactionApp = dbConApp.BeginTransaction(conApp,IsolationLevel.ReadCommitted);

			if(transactionApp == null)
			{
				Logger.LogTraceError("SysDataMgrDAL","DeleteServiceCodeDS","SDM002","transactionApp is null");
				throw new ApplicationException("Failed Begining a App Transaction..",null);
			}


			try
			{

				//Update record into core_enterprise Table

				StringBuilder strBuilder = new StringBuilder();
			
				strBuilder.Append("Delete from customer_assembly where ");
				strBuilder.Append(" applicationid = '");
				strBuilder.Append(strAppID);
				strBuilder.Append("' and enterpriseid = '");
				strBuilder.Append(strEnterpriseID);
				strBuilder.Append("' and assemblyid = '");
				strBuilder.Append(strServiceCode);
				strBuilder.Append("' and custid = '");
				strBuilder.Append(strAgentID);
				strBuilder.Append("'");


				String strSQLQuery = strBuilder.ToString();
				dbCommandApp = dbConApp.CreateCommand(strSQLQuery,CommandType.Text); //Creating the command first time
				dbCommandApp.Connection = conApp;
				dbCommandApp.Transaction = transactionApp;

				iRowsDeleted = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);
				Logger.LogTraceInfo("RBACManager","DeleteCustomizedCodeDS","INF001",iRowsDeleted + " rows deleted from agent_assembly/customer_assembly table");

			
				//Commit application and core transaction.

				transactionApp.Commit();
				Logger.LogTraceInfo("SysDataMgrDAL","DeleteServiceCodeDS","SDM003","App db delete transaction committed.");

			}
			catch(ApplicationException appException)
			{
				
				try
				{
					transactionApp.Rollback();
					Logger.LogTraceInfo("SysDataMgrDAL","DeleteServiceCodeDS","SDM002","App db delete transaction rolled back.");

				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("SysDataMgrDAL","DeleteServiceCodeDS","SDM002","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}


				Logger.LogTraceError("SysDataMgrDAL","DeleteServiceCodeDS","SDM002","Delete Transaction Failed : "+ appException.Message.ToString());
				throw new ApplicationException("Error deleting Service Code "+appException.Message,appException);
			}
			catch(Exception exception)
			{
				try
				{
					transactionApp.Rollback();
					Logger.LogTraceInfo("SysDataMgrDAL","DeleteServiceCodeDS","SDM002","App db delete transaction rolled back.");

				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("SysDataMgrDAL","DeleteServiceCodeDS","SDM002","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}

				Logger.LogTraceError("SysDataMgrDAL","DeleteServiceCodeDS","SDM004","delete Transaction Failed : "+ exception.Message.ToString());
				throw new ApplicationException("Error deleting Service Code "+exception.Message,exception);
			}
			finally
			{
				if(conApp != null)
				{
					conApp.Close();
				}
			}
			
			return iRowsDeleted;
		}

		//Methods for CP - Reference Base Module


		public static SessionDS GetReferenceDS(String strAppID, String strEnterpriseID, String strCustID, int iCurrent, int iDSRecSize)
		{

			SessionDS sessionDS = null;


			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("SysDataMgrDAL","GetReferenceDS","EDB101","DbConnection object is null!!");
				return sessionDS;
			}

			StringBuilder strBuilder = new StringBuilder();
			strBuilder.Append("select snd_rec_name,snd_rec_type,contact_person,email,address1,address2,country,zipcode,telephone,fax from Customer_Snd_Rec where applicationid = '");
			strBuilder.Append(strAppID);
			strBuilder.Append("' and enterpriseid = '");
			strBuilder.Append(strEnterpriseID);
			strBuilder.Append("' and custid = '");
			strBuilder.Append(strCustID);
			strBuilder.Append("'");
			
			strBuilder.Append (" Order by snd_rec_name"); //Mohan, 20/11/2002
			String strSQLQuery = strBuilder.ToString();
			
			sessionDS = dbCon.ExecuteQuery(strSQLQuery,iCurrent,iDSRecSize,"ReferenceTable");

			return  sessionDS;
	
		}

		public static int AddReferenceDS(String strAppID, String strEnterpriseID, String strCustID, DataSet dsToInsert)
		{
			int iRowsAffected = 0;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("SysDataMgrDAL","AddReferenceDS","EDB101","DbConnection object is null!!");
				return iRowsAffected;
			}



			StringBuilder strBuilder = new StringBuilder();
			strBuilder.Append("insert into Customer_Snd_Rec (applicationid,enterpriseid,custid,snd_rec_name,snd_rec_type,contact_person,email,address1,address2,country,zipcode,telephone,fax) values ('");
			strBuilder.Append(strAppID);
			strBuilder.Append("','");
			strBuilder.Append(strEnterpriseID);
			strBuilder.Append("','");
			strBuilder.Append(strCustID);
			strBuilder.Append("',");

			int iRowCnt = dsToInsert.Tables[0].Rows.Count;
			DataRow drEach = dsToInsert.Tables[0].Rows[0];

			if(Utility.IsNotDBNull(drEach["snd_rec_name"]) && drEach["snd_rec_name"].ToString()!="")
			{
				String strSndRecName = (String) drEach["snd_rec_name"];
				strBuilder.Append("N'");
				strBuilder.Append(Utility.ReplaceSingleQuote(strSndRecName));
				strBuilder.Append("'");
			}

			strBuilder.Append(",");
			
			if(Utility.IsNotDBNull(drEach["snd_rec_type"]) && drEach["snd_rec_type"].ToString()!="")
			{
				String strSndRecType = (String) drEach["snd_rec_type"];
				strBuilder.Append("'");
				strBuilder.Append(strSndRecType);
				strBuilder.Append("'");
			}
			else
			{
				strBuilder.Append("null");
			}


			strBuilder.Append(",");

			if(Utility.IsNotDBNull(drEach["contact_person"]) && drEach["contact_person"].ToString()!="")
			{
				String strContactPerson = (String) drEach["contact_person"];
				strBuilder.Append("N'");
				strBuilder.Append(Utility.ReplaceSingleQuote(strContactPerson));
				strBuilder.Append("'");
			}
			else
			{
				strBuilder.Append("null");
			}

			strBuilder.Append(",");
			
			if(Utility.IsNotDBNull(drEach["email"]) && drEach["email"].ToString()!="")
			{
				String strEmail = (String) drEach["email"];
				strBuilder.Append("'");
				strBuilder.Append(Utility.ReplaceSingleQuote(strEmail));
				strBuilder.Append("'");
			}
			else
			{
				strBuilder.Append("null");
			}

			strBuilder.Append(",");
			
			if(Utility.IsNotDBNull(drEach["address1"]) && drEach["address1"].ToString()!="")
			{
				String strAddress1 = (String) drEach["address1"];
				strBuilder.Append("N'");
				strBuilder.Append(Utility.ReplaceSingleQuote(strAddress1));
				strBuilder.Append("'");
			}
			else
			{
				strBuilder.Append("null");
			}
			strBuilder.Append(",");
			if(Utility.IsNotDBNull(drEach["address2"]) && drEach["address2"].ToString()!="")
			{
				String strAddress2 = (String) drEach["address2"];
				strBuilder.Append("N'");
				strBuilder.Append(Utility.ReplaceSingleQuote(strAddress2));
				strBuilder.Append("'");
			}
			else
			{
				strBuilder.Append("null");
			}
			strBuilder.Append(",");
			if(Utility.IsNotDBNull(drEach["country"]) && drEach["country"].ToString()!="")
			{
				String strCountry = (String) drEach["country"];
				strBuilder.Append("'");
				strBuilder.Append(strCountry);
				strBuilder.Append("'");
			}
			else
			{
				strBuilder.Append("null");
			}
			strBuilder.Append(",");
			if(Utility.IsNotDBNull(drEach["zipcode"]) && drEach["zipcode"].ToString()!="")
			{
				String strZipcode = (String) drEach["zipcode"];
				strBuilder.Append("'");
				strBuilder.Append(strZipcode);
				strBuilder.Append("'");
			}
			else
			{
				strBuilder.Append("null");
			}
			strBuilder.Append(",");
			if(Utility.IsNotDBNull(drEach["telephone"]) && drEach["telephone"].ToString()!="")
			{
				String strTelephone = (String) drEach["telephone"];
				strBuilder.Append("'");
				strBuilder.Append(Utility.ReplaceSingleQuote(strTelephone));
				strBuilder.Append("'");
			}
			else
			{
				strBuilder.Append("null");
			}
			strBuilder.Append(",");
			if(Utility.IsNotDBNull(drEach["fax"]) && drEach["fax"].ToString()!="")
			{
				String strFax = (String) drEach["fax"];
				strBuilder.Append("'");
				strBuilder.Append(Utility.ReplaceSingleQuote(strFax));
				strBuilder.Append("'");
			}
			else
			{
				strBuilder.Append("null");
			}

			strBuilder.Append(")");


			String strSQLQuery = strBuilder.ToString();

			IDbCommand dbCmd = dbCon.CreateCommand(strSQLQuery,CommandType.Text);
			

			try
			{

				iRowsAffected = dbCon.ExecuteNonQuery(dbCmd);
				Logger.LogTraceInfo("SysDataMgrDAL","AddStatusCodeDS","SDM001",iRowsAffected+" rows inserted in to Status_Code table");
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("RBACManager","AddReferenceDS","SDM001","Error During Insert "+ appException.Message.ToString());
				throw new ApplicationException("Error inserting customer profile : "+appException.Message,appException);
			}

			return iRowsAffected;

		}

		public static int ModifyReferenceDS(String strAppID, String strEnterpriseID, String strCustID, DataSet dsToModify)
		{
			int iRowsAffected = 0;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("SysDataMgrDAL","ModifyReferenceDS","EDB101","DbConnection object is null!!");
				return iRowsAffected;
			}
			int iRowCnt = dsToModify.Tables[0].Rows.Count;
			DataRow drEach = dsToModify.Tables[0].Rows[0];


			StringBuilder strBuilder = new StringBuilder();
			
			strBuilder.Append("update Customer_Snd_Rec set snd_rec_type = ");//, status_close, invoiceable,system_code) values ('");
			if(Utility.IsNotDBNull(drEach["snd_rec_type"]) && drEach["snd_rec_type"].ToString()!="")
			{
				String strSndRecType = (String) drEach["snd_rec_type"];
				strBuilder.Append("'");
				strBuilder.Append(strSndRecType);
				strBuilder.Append("'");
			}
			else
			{
				strBuilder.Append("null");
			}
			
			strBuilder.Append(", contact_person = ");
			if(Utility.IsNotDBNull(drEach["contact_person"]) && drEach["contact_person"].ToString()!="")
			{
				String strContactPerson = (String) drEach["contact_person"];
				strBuilder.Append("N'");
				strBuilder.Append(Utility.ReplaceSingleQuote(strContactPerson));
				strBuilder.Append("'");
			}
			else
			{
				strBuilder.Append("null");
			}

			strBuilder.Append(", email = ");
			if(Utility.IsNotDBNull(drEach["email"]) && drEach["email"].ToString()!="")
			{
				String strEmail = (String) drEach["email"];
				strBuilder.Append("'");
				strBuilder.Append(Utility.ReplaceSingleQuote(strEmail));
				strBuilder.Append("'");
			}
			else
			{
				strBuilder.Append("null");
			}

			strBuilder.Append(", address1 = ");
			if(Utility.IsNotDBNull(drEach["address1"]) && drEach["address1"].ToString()!="")
			{
				String strAddress1 = (String) drEach["address1"];
				strBuilder.Append("N'");
				strBuilder.Append(Utility.ReplaceSingleQuote(strAddress1));
				strBuilder.Append("'");
			}
			else
			{
				strBuilder.Append("null");
			}
			strBuilder.Append(", address2 = ");
			if(Utility.IsNotDBNull(drEach["address2"]) && drEach["address2"].ToString()!="")
			{
				String strAddress2 = (String) drEach["address2"];
				strBuilder.Append("N'");
				strBuilder.Append(Utility.ReplaceSingleQuote(strAddress2));
				strBuilder.Append("'");
			}
			else
			{
				strBuilder.Append("null");
			}
			strBuilder.Append(", country = ");
			if(Utility.IsNotDBNull(drEach["country"]) && drEach["country"].ToString()!="")
			{
				String strCountry = (String) drEach["country"];
				strBuilder.Append("'");
				strBuilder.Append(strCountry);
				strBuilder.Append("'");
			}
			else
			{
				strBuilder.Append("null");
			}
			strBuilder.Append(", zipcode = ");
			if(Utility.IsNotDBNull(drEach["zipcode"]) && drEach["zipcode"].ToString()!="")
			{
				String strZipcode = (String) drEach["zipcode"];
				strBuilder.Append("'");
				strBuilder.Append(strZipcode);
				strBuilder.Append("'");
			}
			else
			{
				strBuilder.Append("null");
			}
			strBuilder.Append(", telephone = ");
			if(Utility.IsNotDBNull(drEach["telephone"]) && drEach["telephone"].ToString()!="")
			{
				String strTelephone = (String) drEach["telephone"];
				strBuilder.Append("'");
				strBuilder.Append(Utility.ReplaceSingleQuote(strTelephone));
				strBuilder.Append("'");
			}
			else
			{
				strBuilder.Append("null");
			}

			strBuilder.Append(", fax = ");
			if(Utility.IsNotDBNull(drEach["fax"]) && drEach["fax"].ToString()!="")
			{
				String strFax = (String) drEach["fax"];
				strBuilder.Append("'");
				strBuilder.Append(Utility.ReplaceSingleQuote(strFax));
				strBuilder.Append("'");
			}
			else
			{
				strBuilder.Append("null");
			}			

			strBuilder.Append(" where applicationid = '");
			strBuilder.Append(strAppID);
			strBuilder.Append("' and enterpriseid = '");
			strBuilder.Append(strEnterpriseID);
			strBuilder.Append("' and custid = '");
			strBuilder.Append(strCustID);
			strBuilder.Append("' and snd_rec_name = N'");
			String strSndRecName = (String) drEach["snd_rec_name"];
			strBuilder.Append(strSndRecName);
			strBuilder.Append("'");

			String strSQLQuery = strBuilder.ToString();

			IDbCommand dbCmd = dbCon.CreateCommand(strSQLQuery,CommandType.Text);
			
			try
			{
				iRowsAffected = dbCon.ExecuteNonQuery(dbCmd);
				Logger.LogTraceInfo("SysDataMgrDAL","ModifyReferenceDS","SDM001",iRowsAffected+" rows inserted in to Customer table");
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("RBACManager","ModifyReferenceDS","SDM001","Error During Insert "+ appException.Message.ToString());
				throw new ApplicationException("Error modifying Customer Profile : "+appException.Message,appException);
			}
			return iRowsAffected;
		}

		public static SessionDS GetEmptyReferenceDS(int iNumRows)
		{
			DataTable dtReference = new DataTable();
 
			dtReference.Columns.Add(new DataColumn("snd_rec_name", typeof(string)));
			dtReference.Columns.Add(new DataColumn("snd_rec_type", typeof(string)));
			dtReference.Columns.Add(new DataColumn("contact_person", typeof(string)));
			dtReference.Columns.Add(new DataColumn("email", typeof(string)));
			dtReference.Columns.Add(new DataColumn("address1", typeof(string)));
			dtReference.Columns.Add(new DataColumn("address2", typeof(string)));
			dtReference.Columns.Add(new DataColumn("country", typeof(string)));
			dtReference.Columns.Add(new DataColumn("zipcode", typeof(string)));
			dtReference.Columns.Add(new DataColumn("telephone", typeof(string)));
			dtReference.Columns.Add(new DataColumn("fax", typeof(string)));

			for(int i=0; i < iNumRows; i++)
			{
				DataRow drEach = dtReference.NewRow();
				dtReference.Rows.Add(drEach);
			}

			DataSet dsReference = new DataSet();
			dsReference.Tables.Add(dtReference);

			SessionDS sessionDS = new SessionDS();
			sessionDS.ds = dsReference;
			sessionDS.DataSetRecSize = dsReference.Tables[0].Rows.Count;
			sessionDS.QueryResultMaxSize = sessionDS.DataSetRecSize;

			return  sessionDS;
	
		}


		//Phase2 - L02
		//		public static int DeleteCustReferences(String strAppID, String strEnterpriseID,String strCustID, String strCustRefID)
		//		{
		//			int iRowsDeleted = 0;
		//			DbConnection dbConApp = null;
		//			IDbCommand dbCommandApp = null;
		//
		//			IDbTransaction transactionApp = null;
		//			IDbConnection conApp = null;
		//
		//	
		//			//Get App dbConnection
		//
		//			dbConApp = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
		//
		//			if(dbConApp == null)
		//			{
		//				Logger.LogTraceError("SysDataMgrDAL","DeleteServiceCodeDS","SDM002","dbConApp is null");
		//				throw new ApplicationException("Connection to Database failed",null);
		//			}
		//
		//			//Begin Core and App Transaction
		//
		//			conApp = dbConApp.GetConnection();
		//			
		//			if(conApp == null)
		//			{
		//				Logger.LogTraceError("SysDataMgrDAL","DeleteServiceCodeDS","SDM002","conApp is null");
		//				throw new ApplicationException("Connection to Database failed",null);
		//			}
		//
		//			try
		//			{
		//				dbConApp.OpenConnection(ref conApp);
		//			}
		//			catch(ApplicationException appException)
		//			{
		//				if(conApp != null)
		//				{
		//					conApp.Close();
		//				}
		//				Logger.LogTraceError("SysDataMgrDAL","DeleteServiceCodeDS","SDM003","Error opening connection : "+ appException.Message.ToString());
		//				throw new ApplicationException("Error opening database connection ",appException);
		//			}
		//			catch(Exception exception)
		//			{
		//				if(conApp != null)
		//				{
		//					conApp.Close();
		//				}
		//
		//				Logger.LogTraceError("SysDataMgrDAL","DeleteServiceCodeDS","SDM003","Error opening connection : "+ exception.Message.ToString());
		//				throw new ApplicationException("Error opening database connection ",exception);
		//			}
		//
		//
		//			transactionApp = dbConApp.BeginTransaction(conApp,IsolationLevel.ReadCommitted);
		//
		//			if(transactionApp == null)
		//			{
		//				Logger.LogTraceError("SysDataMgrDAL","DeleteServiceCodeDS","SDM002","transactionApp is null");
		//				throw new ApplicationException("Failed Begining a App Transaction..",null);
		//			}
		//
		//
		//			try
		//			{
		//
		//				//Update record into core_enterprise Table
		//
		//				StringBuilder strBuilder = new StringBuilder();
		//			
		//				strBuilder.Append("Delete from customer_assembly where ");
		//				strBuilder.Append(" applicationid = '");
		//				strBuilder.Append(strAppID);
		//				strBuilder.Append("' and enterpriseid = '");
		//				strBuilder.Append(strEnterpriseID);
		//				strBuilder.Append("' and assemblyid = '");
		//				strBuilder.Append(strServiceCode);
		//				strBuilder.Append("' and custid = '");
		//				strBuilder.Append(strAgentID);
		//				strBuilder.Append("'");
		//
		//
		//				String strSQLQuery = strBuilder.ToString();
		//				dbCommandApp = dbConApp.CreateCommand(strSQLQuery,CommandType.Text); //Creating the command first time
		//				dbCommandApp.Connection = conApp;
		//				dbCommandApp.Transaction = transactionApp;
		//
		//				iRowsDeleted = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);
		//				Logger.LogTraceInfo("RBACManager","DeleteCustomizedCodeDS","INF001",iRowsDeleted + " rows deleted from agent_assembly/customer_assembly table");
		//
		//			
		//				//Commit application and core transaction.
		//
		//				transactionApp.Commit();
		//				Logger.LogTraceInfo("SysDataMgrDAL","DeleteServiceCodeDS","SDM003","App db delete transaction committed.");
		//
		//			}
		//			catch(ApplicationException appException)
		//			{
		//				
		//				try
		//				{
		//					transactionApp.Rollback();
		//					Logger.LogTraceInfo("SysDataMgrDAL","DeleteServiceCodeDS","SDM002","App db delete transaction rolled back.");
		//
		//				}
		//				catch(Exception rollbackException)
		//				{
		//					Logger.LogTraceError("SysDataMgrDAL","DeleteServiceCodeDS","SDM002","Error during app transanction roll back "+ rollbackException.Message.ToString());
		//				}
		//
		//
		//				Logger.LogTraceError("SysDataMgrDAL","DeleteServiceCodeDS","SDM002","Delete Transaction Failed : "+ appException.Message.ToString());
		//				throw new ApplicationException("Error deleting Service Code "+appException.Message,appException);
		//			}
		//			catch(Exception exception)
		//			{
		//				try
		//				{
		//					transactionApp.Rollback();
		//					Logger.LogTraceInfo("SysDataMgrDAL","DeleteServiceCodeDS","SDM002","App db delete transaction rolled back.");
		//
		//				}
		//				catch(Exception rollbackException)
		//				{
		//					Logger.LogTraceError("SysDataMgrDAL","DeleteServiceCodeDS","SDM002","Error during app transanction roll back "+ rollbackException.Message.ToString());
		//				}
		//
		//				Logger.LogTraceError("SysDataMgrDAL","DeleteServiceCodeDS","SDM004","delete Transaction Failed : "+ exception.Message.ToString());
		//				throw new ApplicationException("Error deleting Service Code "+exception.Message,exception);
		//			}
		//			finally
		//			{
		//				if(conApp != null)
		//				{
		//					conApp.Close();
		//				}
		//			}
		//			
		//			return iRowsDeleted;
		//		}
		//		//Phase2 - L02

		//Mehtods for CP - Status Code module

		public static SessionDS GetEmptyCPStatusCodeDS(int iNumRows)
		{			
			DataTable dtStatusCode = new DataTable();
 
			dtStatusCode.Columns.Add(new DataColumn("status_code", typeof(string)));
			dtStatusCode.Columns.Add(new DataColumn("status_description", typeof(string)));
			dtStatusCode.Columns.Add(new DataColumn("surcharge", typeof(decimal)));

			for(int i=0; i < iNumRows; i++)
			{
				DataRow drEach = dtStatusCode.NewRow();
				drEach[0] = "";
				drEach[1] = "";

				dtStatusCode.Rows.Add(drEach);
			}

			DataSet dsStatusCode = new DataSet();
			dsStatusCode.Tables.Add(dtStatusCode);

			dsStatusCode.Tables[0].Columns["status_code"].Unique = true; //Checks duplicate records..
			dsStatusCode.Tables[0].Columns["status_code"].AllowDBNull = false;

			SessionDS sessionDS = new SessionDS();
			sessionDS.ds = dsStatusCode;
			sessionDS.DataSetRecSize = dsStatusCode.Tables[0].Rows.Count;
			sessionDS.QueryResultMaxSize = sessionDS.DataSetRecSize;

			return  sessionDS;
	
		}

		public static void AddNewRowInCPStatusCodeDS(SessionDS sessionDS)
		{
			DataRow drNew = sessionDS.ds.Tables[0].NewRow();

			drNew[0] = "";
			drNew[1] = "";
			try
			{
				sessionDS.ds.Tables[0].Rows.Add(drNew);
			}
			catch(Exception ex)
			{
				Logger.LogTraceError("SysDataMgrDAL","DeleteRouteCodeDS","R005",ex.Message.ToString());
			}			
			sessionDS.QueryResultMaxSize++;
		}

		public static SessionDS GetCPStatusCodeDS(String strAppID, String strEnterpriseID,  String strCustID , int iCurrent, int iDSRecSize)
		{

			SessionDS sessionDS = null;


			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("SysDataMgrDAL","GetStatusCodeDS","EDB101","DbConnection object is null!!");
				return sessionDS;
			}

			StringBuilder strBuilder = new StringBuilder();

			strBuilder.Append("select csc.status_code as status_code, sc.status_description as status_description, csc.surcharge as surcharge from Customer_Status_Charge csc, Status_Code sc ");
			strBuilder.Append(" where csc.applicationid = sc.applicationid and csc.enterpriseid = sc.enterpriseid and csc.status_code = sc.status_code and csc.applicationid = '");
			strBuilder.Append(strAppID);
			strBuilder.Append("' and csc.enterpriseid = '");
			strBuilder.Append(strEnterpriseID);
			strBuilder.Append("' and csc.custid = '");
			strBuilder.Append(strCustID);
			strBuilder.Append("'");

			String strSQLQuery = strBuilder.ToString();
			
			sessionDS = dbCon.ExecuteQuery(strSQLQuery,iCurrent,iDSRecSize,"StatusCodeTable");

			return  sessionDS;
	
		}

		public static int AddCPStatusCodeDS(String strAppID, String strEnterpriseID, String strCustID,DataSet dsToInsert)
		{
			int iRowsAffected = 0;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("SysDataMgrDAL","AddCPStatusCodeDS","EDB101","DbConnection object is null!!");
				return iRowsAffected;
			}

			StringBuilder strBuilder = new StringBuilder();
			strBuilder.Append("insert into Customer_Status_Charge (applicationid,enterpriseid,custid,status_code,surcharge) values ('");
			strBuilder.Append(strAppID);
			strBuilder.Append("','");
			strBuilder.Append(strEnterpriseID);
			strBuilder.Append("','");
			strBuilder.Append(strCustID);
			strBuilder.Append("',");

			int iRowCnt = dsToInsert.Tables[0].Rows.Count;

			DataRow drEach = dsToInsert.Tables[0].Rows[0];

			if(Utility.IsNotDBNull(drEach["status_code"]) && drEach["status_code"].ToString()!="")
			{
				String strStatusCode = (String) drEach["status_code"];
				strBuilder.Append("'");
				strBuilder.Append(strStatusCode);
				strBuilder.Append("'");
			}
			else
			{
				strBuilder.Append("null");
			}
				
			strBuilder.Append(",");

			if(Utility.IsNotDBNull(drEach["surcharge"]) && drEach["surcharge"].ToString()!="")
			{
				decimal decSurcharge = (decimal) drEach["surcharge"];
			
				strBuilder.Append(decSurcharge);
			}
			else
			{
				strBuilder.Append("null");
			}

			strBuilder.Append(")");
			
			String strSQLQuery = strBuilder.ToString();

			IDbCommand dbCmd = dbCon.CreateCommand(strSQLQuery,CommandType.Text);
			

			try
			{

				iRowsAffected = dbCon.ExecuteNonQuery(dbCmd);
				Logger.LogTraceInfo("SysDataMgrDAL","AddCPStatusCodeDS","SDM001",iRowsAffected+" rows inserted in to Status_Code table");
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("RBACManager","AddCPStatusCodeDS","SDM001","Error During Insert "+ appException.Message.ToString());
				throw new ApplicationException("Error inserting Status Code ",appException);
			}



			return iRowsAffected;

		}

		public static int ModifyCPStatusCodeDS(String strAppID, String strEnterpriseID, String strCustID,DataSet dsToModify)
		{
			int iRowsAffected = 0;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("SysDataMgrDAL","ModifyCPStatusCodeDS","EDB101","DbConnection object is null!!");
				return iRowsAffected;
			}
			int iRowCnt = dsToModify.Tables[0].Rows.Count;
			DataRow drEach = dsToModify.Tables[0].Rows[0];


			StringBuilder strBuilder = new StringBuilder();
			
			strBuilder.Append("update Customer_Status_Charge set surcharge = ");//, status_close, invoiceable,system_code) values ('");
			if(Utility.IsNotDBNull(drEach["surcharge"]) && drEach["surcharge"].ToString()!="")
			{
				decimal decSurcharge = (decimal) drEach["surcharge"];
				strBuilder.Append(decSurcharge);
			}
			else
			{
				strBuilder.Append("null");
			}

			strBuilder.Append(" where applicationid = '");
			strBuilder.Append(strAppID);
			strBuilder.Append("' and enterpriseid = '");
			strBuilder.Append(strEnterpriseID);
			strBuilder.Append("' and custid = '");
			strBuilder.Append(strCustID);
			strBuilder.Append("' and status_code = '");
			String strStatusCode = (String) drEach["status_code"];
			strBuilder.Append(strStatusCode);
			strBuilder.Append("'");

			String strSQLQuery = strBuilder.ToString();

			IDbCommand dbCmd = dbCon.CreateCommand(strSQLQuery,CommandType.Text);
			

			try
			{

				iRowsAffected = dbCon.ExecuteNonQuery(dbCmd);
				Logger.LogTraceInfo("SysDataMgrDAL","AddStatusCodeDS","SDM001",iRowsAffected+" rows inserted in to Status_Code table");
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("RBACManager","AddStatusCodeDS","SDM001","Error During Insert "+ appException.Message.ToString());
				throw new ApplicationException("Error inserting Status Code ",appException);
			}



			return iRowsAffected;

		}

		public static int DeleteCPStatusCodeDS(String strAppID, String strEnterpriseID, String strCustID,String strStatusCode)
		{
			int iRowsDeleted = 0;

			DbConnection dbConApp = null;
			IDbCommand dbCommandApp = null;

			IDbTransaction transactionApp = null;
			IDbConnection conApp = null;

	
			//Get App dbConnection

			dbConApp = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbConApp == null)
			{
				Logger.LogTraceError("SysDataMgrDAL","DeleteCPStatusCodeDS","SDM002","dbConApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			//Begin Core and App Transaction

			conApp = dbConApp.GetConnection();
			
			if(conApp == null)
			{
				Logger.LogTraceError("SysDataMgrDAL","DeleteCPStatusCodeDS","SDM002","conApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			try
			{
				dbConApp.OpenConnection(ref conApp);
			}
			catch(ApplicationException appException)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("SysDataMgrDAL","DeleteCPStatusCodeDS","SDM003","Error opening connection : "+ appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ",appException);
			}
			catch(Exception exception)
			{
				if(conApp != null)
				{
					conApp.Close();
				}

				Logger.LogTraceError("SysDataMgrDAL","DeleteCPStatusCodeDS","SDM003","Error opening connection : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}


			transactionApp = dbConApp.BeginTransaction(conApp,IsolationLevel.ReadCommitted);

			if(transactionApp == null)
			{
				Logger.LogTraceError("SysDataMgrDAL","DeleteCPStatusCodeDS","SDM002","transactionApp is null");
				throw new ApplicationException("Failed Begining a App Transaction..",null);
			}


			try
			{

				//Update record into core_enterprise Table

				StringBuilder strBuilder = new StringBuilder();
			
				strBuilder.Append("Delete from Customer_Exception_Charge where ");
				strBuilder.Append(" applicationid = '");
				strBuilder.Append(strAppID);
				strBuilder.Append("' and enterpriseid = '");
				strBuilder.Append(strEnterpriseID);
				strBuilder.Append("' and custid = '");
				strBuilder.Append(strCustID);
				strBuilder.Append("' and status_code = '");
				strBuilder.Append(strStatusCode);
				strBuilder.Append("'");


				String strSQLQuery = strBuilder.ToString();
				dbCommandApp = dbConApp.CreateCommand(strSQLQuery,CommandType.Text); //Creating the command first time
				dbCommandApp.Connection = conApp;
				dbCommandApp.Transaction = transactionApp;

				iRowsDeleted = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);
				Logger.LogTraceInfo("RBACManager","DeleteStatusCodeDS","INF001",iRowsDeleted + " rows deleted from Exception_Code table");


				strBuilder = new StringBuilder();
			
				strBuilder.Append("Delete from Customer_Status_Charge where ");
				strBuilder.Append(" applicationid = '");
				strBuilder.Append(strAppID);
				strBuilder.Append("' and enterpriseid = '");
				strBuilder.Append(strEnterpriseID);
				strBuilder.Append("' and custid = '");
				strBuilder.Append(strCustID);
				strBuilder.Append("' and status_code = '");
				strBuilder.Append(strStatusCode);
				strBuilder.Append("'");

				strSQLQuery = strBuilder.ToString();

				dbCommandApp.CommandText = strSQLQuery;


				iRowsDeleted = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);
				Logger.LogTraceInfo("RBACManager","DeleteCPStatusCodeDS","INF001",iRowsDeleted + " rows deleted from Status_Code table");
			
				//Commit application and core transaction.

				transactionApp.Commit();
				Logger.LogTraceInfo("SysDataMgrDAL","DeleteCPStatusCodeDS","SDM003","App db delete transaction committed.");

			}
			catch(ApplicationException appException)
			{
				
				try
				{
					transactionApp.Rollback();
					Logger.LogTraceInfo("SysDataMgrDAL","DeleteCPStatusCodeDS","SDM002","App db delete transaction rolled back.");

				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("SysDataMgrDAL","DeleteCPStatusCodeDS","SDM002","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}


				Logger.LogTraceError("SysDataMgrDAL","DeleteCPStatusCodeDS","SDM002","Delete Transaction Failed : "+ appException.Message.ToString());
				throw new ApplicationException("Error deleting Status Code "+appException.Message,appException);
			}
			catch(Exception exception)
			{
				try
				{
					transactionApp.Rollback();
					Logger.LogTraceInfo("SysDataMgrDAL","DeleteCPStatusCodeDS","SDM002","App db delete transaction rolled back.");

				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("SysDataMgrDAL","DeleteCPStatusCodeDS","SDM002","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}

				Logger.LogTraceError("SysDataMgrDAL","DeleteCPStatusCodeDS","SDM004","delete Transaction Failed : "+ exception.Message.ToString());
				throw new ApplicationException("Error deleting Status Code "+exception.Message,exception);
			}
			finally
			{
				if(conApp != null)
				{
					conApp.Close();
				}
			}
			
			return iRowsDeleted;
		}


		public static SessionDS GetEmptyCPExceptionCodeDS(int iNumRows)
		{
			DataTable dtExceptionCode = new DataTable();
 
			dtExceptionCode.Columns.Add(new DataColumn("exception_code", typeof(string)));
			dtExceptionCode.Columns.Add(new DataColumn("exception_description", typeof(string)));
			dtExceptionCode.Columns.Add(new DataColumn("surcharge", typeof(decimal)));

			for(int i=0; i < iNumRows; i++)
			{
				DataRow drEach = dtExceptionCode.NewRow();
				drEach[0] = "";
				drEach[1] = "";

				dtExceptionCode.Rows.Add(drEach);
			}

			DataSet dsExceptionCode = new DataSet();
			dsExceptionCode.Tables.Add(dtExceptionCode);

			dsExceptionCode.Tables[0].Columns["exception_code"].Unique = true; //Checks duplicate records..
			dsExceptionCode.Tables[0].Columns["exception_code"].AllowDBNull = false;

			SessionDS sessionDS = new SessionDS();
			sessionDS.ds = dsExceptionCode;
			sessionDS.DataSetRecSize = dsExceptionCode.Tables[0].Rows.Count;
			sessionDS.QueryResultMaxSize = sessionDS.DataSetRecSize;

			return  sessionDS;
	
		}

		public static void AddNewRowInCPExceptionCodeDS(SessionDS sessionDS)
		{
			DataRow drNew = sessionDS.ds.Tables[0].NewRow();

			drNew[0] = "";
			drNew[1] = "";
			try
			{
				sessionDS.ds.Tables[0].Rows.Add(drNew);
			}
			catch(Exception ex)
			{
				Logger.LogTraceError("SysDataMgrDAL","DeleteRouteCodeDS","R005",ex.Message.ToString());
			}

			
			sessionDS.QueryResultMaxSize++;
		}
		public static SessionDS GetCPExceptionCodeDS(String strAppID, String strEnterpriseID,String strCustID,String strStatusCode,int iCurrent, int iDSRecSize)
		{
			SessionDS sessionDS = null;


			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("SysDataMgrDAL","GetStatusCodeDS","EDB101","DbConnection object is null!!");
				return sessionDS;
			}

			//			String strSQLQuery = "select exception_code, exception_description, surcharge from Customer_Status_Charge where ";
			//			strSQLQuery += "applicationid = '"+strAppID+"' and enterpriseid = '"+strEnterpriseID+"' and status_code = '"+strStatusCode+"'"; 

			StringBuilder strBuilder = new StringBuilder();

			strBuilder.Append("select cec.exception_code as exception_code, ec.exception_description as exception_description, cec.surcharge as surcharge from Customer_Exception_Charge cec, Exception_Code ec ");
			strBuilder.Append(" where cec.applicationid = ec.applicationid and cec.enterpriseid = ec.enterpriseid and cec.exception_code = ec.exception_code and cec.applicationid = '");
			strBuilder.Append(strAppID);
			strBuilder.Append("' and cec.enterpriseid = '");
			strBuilder.Append(strEnterpriseID);
			strBuilder.Append("' and cec.custid = '");
			strBuilder.Append(strCustID);
			strBuilder.Append("' and cec.status_code = '");
			strBuilder.Append(strStatusCode);
			strBuilder.Append("'");

			String strSQLQuery = strBuilder.ToString();

			
			//dsExceptionCode = (DataSet)dbCon.ExecuteQuery(strSQLQuery,ReturnType.DataSetType);
			sessionDS = dbCon.ExecuteQuery(strSQLQuery,iCurrent,iDSRecSize,"ExceptionCodeTable");

			return  sessionDS;
	
		}

		public static int AddCPExceptionCodeDS(String strAppID, String strEnterpriseID,String strCustID,String strStatusCode,DataSet dsToInsert)
		{
			int iRowsAffected = 0;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("SysDataMgrDAL","AddCPExceptionCodeDS","EDB101","DbConnection object is null!!");
				return iRowsAffected;
			}

			StringBuilder strBuilder = new StringBuilder();
			strBuilder.Append("insert into Customer_Exception_Charge (applicationid,enterpriseid,custid,status_code,exception_code,surcharge) values ('");
			strBuilder.Append(strAppID);
			strBuilder.Append("','");
			strBuilder.Append(strEnterpriseID);
			strBuilder.Append("','");
			strBuilder.Append(strCustID);
			strBuilder.Append("','");
			strBuilder.Append(strStatusCode);
			strBuilder.Append("',");


			int iRowCnt = dsToInsert.Tables[0].Rows.Count;

			DataRow drEach = dsToInsert.Tables[0].Rows[0];

			if(Utility.IsNotDBNull(drEach["exception_code"]) && drEach["exception_code"].ToString()!="")
			{
				String strExceptionCode = (String) drEach["exception_code"];
				strBuilder.Append("'");
				strBuilder.Append(strExceptionCode);
				strBuilder.Append("'");
			}
			else
			{
				strBuilder.Append("null");
			}
				
			strBuilder.Append(",");

			if(Utility.IsNotDBNull(drEach["surcharge"]) && drEach["surcharge"].ToString()!="")
			{
				decimal decSurcharge = (decimal) drEach["surcharge"];
			
				strBuilder.Append(decSurcharge);
			}
			else
			{
				strBuilder.Append("null");
			}

			strBuilder.Append(")");
			
			String strSQLQuery = strBuilder.ToString();

			IDbCommand dbCmd = dbCon.CreateCommand(strSQLQuery,CommandType.Text);
			

			try
			{

				iRowsAffected = dbCon.ExecuteNonQuery(dbCmd);
				Logger.LogTraceInfo("SysDataMgrDAL","AddCPExceptionCodeDS","SDM001",iRowsAffected+" rows inserted in to Status_Code table");
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("RBACManager","AddCPExceptionCodeDS","SDM001","Error During Insert "+ appException.Message.ToString());
				throw new ApplicationException("Error inserting Status Code ",appException);
			}



			return iRowsAffected;

		}

		public static int ModifyCPExceptionCodeDS(String strAppID, String strEnterpriseID,String strCustID,String strStatusCode,DataSet dsToModify)
		{
			int iRowsAffected = 0;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("SysDataMgrDAL","ModifyCPExceptionCodeDS","EDB101","DbConnection object is null!!");
				return iRowsAffected;
			}
			int iRowCnt = dsToModify.Tables[0].Rows.Count;
			DataRow drEach = dsToModify.Tables[0].Rows[0];


			StringBuilder strBuilder = new StringBuilder();
			
			strBuilder.Append("update Customer_Exception_Charge set surcharge = ");
			if(Utility.IsNotDBNull(drEach["surcharge"]) && drEach["surcharge"].ToString()!="")
			{
				decimal decSurcharge = (decimal) drEach["surcharge"];
				strBuilder.Append(decSurcharge);
			}
			else
			{
				strBuilder.Append("null");
			}

			strBuilder.Append(" where applicationid = '");
			strBuilder.Append(strAppID);
			strBuilder.Append("' and enterpriseid = '");
			strBuilder.Append(strEnterpriseID);
			strBuilder.Append("' and custid = '");
			strBuilder.Append(strCustID);
			strBuilder.Append("' and status_code = '");
			strBuilder.Append(strStatusCode);
			strBuilder.Append("' and exception_code = '");
			String strExceptionCode = (String) drEach["exception_code"];
			strBuilder.Append(strExceptionCode);
			strBuilder.Append("'");

			String strSQLQuery = strBuilder.ToString();

			IDbCommand dbCmd = dbCon.CreateCommand(strSQLQuery,CommandType.Text);
			

			try
			{

				iRowsAffected = dbCon.ExecuteNonQuery(dbCmd);
				Logger.LogTraceInfo("SysDataMgrDAL","ModifyCPExceptionCodeDS","SDM001",iRowsAffected+" rows inserted in to Exception_Code table");
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("RBACManager","ModifyCPExceptionCodeDS","SDM001","Error During Insert "+ appException.Message.ToString());
				throw new ApplicationException("Error inserting Status Code ",appException);
			}

			return iRowsAffected;
		}

		public static int DeleteCPExceptionCodeDS(String strAppID, String strEnterpriseID,String strCustID,String strStatusCode,String strExceptionCode)
		{
			int iRowsDeleted = 0;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("SysDataMgrDAL","DeleteCPExceptionCodeDS","EDB101","DbConnection object is null!!");
				return iRowsDeleted;
			}


			StringBuilder strBuilder = new StringBuilder();
			
			strBuilder.Append("Delete from Customer_Exception_Charge where ");
			strBuilder.Append(" applicationid = '");
			strBuilder.Append(strAppID);
			strBuilder.Append("' and enterpriseid = '");
			strBuilder.Append(strEnterpriseID);
			strBuilder.Append("' and custid = '");
			strBuilder.Append(strCustID);
			strBuilder.Append("' and status_code = '");
			strBuilder.Append(strStatusCode);
			strBuilder.Append("' and exception_code = '");
			strBuilder.Append(strExceptionCode);
			strBuilder.Append("'");


			String strSQLQuery = strBuilder.ToString();

			IDbCommand dbCmd = dbCon.CreateCommand(strSQLQuery,CommandType.Text);
			
			try
			{
				iRowsDeleted = dbCon.ExecuteNonQuery(dbCmd);
				Logger.LogTraceInfo("SysDataMgrDAL","DeleteCPExceptionCodeDS","SDM001",iRowsDeleted+" rows deleted from  Exception_Code table");
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("SysDataMgrDAL","DeleteCPExceptionCodeDS","SDM001","Error During Delete "+ appException.Message.ToString());
				throw new ApplicationException("Error inserting Status Code : "+appException.Message,appException);
			}

			return iRowsDeleted;		
		}

		// 12/11/2002 Quotation Tab Page Methods

		public static SessionDS QueryQuotation(String strAppID, String strEnterpriseID, String strCustID, int recStart, int recSize)
		{
			
			SessionDS sdsBaseTables = null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);		
			String strSQLQuery = "select Quotation_No, Quotation_Version, Quotation_Date,Quotation_Status from Customer_Quotation where ";			
			strSQLQuery += "applicationid = '"+strAppID+"' and enterpriseid = '"+strEnterpriseID+"' and custid = '"+strCustID+"'"; 						
			sdsBaseTables = (SessionDS)dbCon.ExecuteQuery(strSQLQuery,recStart, recSize,"Quotation");
			return  sdsBaseTables;
		}


		public static SessionDS GetEmptyQuotation(int iNumRows)
		{
			DataTable dtQuotation = new DataTable();
 
			dtQuotation.Columns.Add(new DataColumn("Quotation_No", typeof(string)));
			dtQuotation.Columns.Add(new DataColumn("Quotation_Version", typeof(string)));
			dtQuotation.Columns.Add(new DataColumn("Quotation_Date", typeof(string)));
			dtQuotation.Columns.Add(new DataColumn("Quotation_Status", typeof(string)));

			for(int i=0; i < iNumRows; i++)
			{
				DataRow drEach = dtQuotation.NewRow();
				drEach[0] = "";
				drEach[1] = "";
				drEach[2] = "";
				drEach[3] = "";

				dtQuotation.Rows.Add(drEach);
			}
			DataSet dsQuotation = new DataSet();
			SessionDS sessionds = new SessionDS();
			dsQuotation.Tables.Add(dtQuotation);
			sessionds.ds = dsQuotation;

			sessionds.DataSetRecSize = dsQuotation.Tables[0].Rows.Count;
			sessionds.QueryResultMaxSize = sessionds.DataSetRecSize;
			return  sessionds;
	
		}

		//---------PUBLIC HOLIDAY---------------------- 22/11/2002
		
		public static bool IsState_Country_Valid(String strAppID, String strEnterpriseID, String sStateCode, String sCountryCode)
		{
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
					
			StringBuilder strSQLQuery = new StringBuilder();
			strSQLQuery.Append(" Select Count(*) from State where Applicationid = '"+strAppID+"'");
			strSQLQuery.Append(" and enterpriseid = '"+strEnterpriseID+"' and Country='"+sCountryCode+"' and State_Code='"+sStateCode+"'"); 
			int iRecordsAffected=(int)dbCon.ExecuteScalar(strSQLQuery.ToString());
			if (iRecordsAffected > 0)
				return true;
			else
				return false;

		}

		public static SessionDS GetEmptyPublicHoliday(int iNumRows)
		{		
			DataTable dtPublicHoliday = new DataTable();
 
			dtPublicHoliday.Columns.Add(new DataColumn("Country", typeof(string)));
			dtPublicHoliday.Columns.Add(new DataColumn("State_Code", typeof(string)));
			dtPublicHoliday.Columns.Add(new DataColumn("DateFrom", typeof(DateTime)));
			dtPublicHoliday.Columns.Add(new DataColumn("DateTo", typeof(DateTime)));
			dtPublicHoliday.Columns.Add(new DataColumn("Holiday_Description", typeof(string)));

			for(int i=0; i < iNumRows; i++)
			{
				DataRow drEach = dtPublicHoliday.NewRow();
				drEach[0] = "";
				drEach[1] = "";
				drEach[2] = System.DBNull.Value;
				drEach[3] = System.DBNull.Value;
				drEach[4] = "";
				dtPublicHoliday.Rows.Add(drEach);
			}
			DataSet dsPublicHoliday = new DataSet();
			SessionDS sessionds = new SessionDS();
			dsPublicHoliday.Tables.Add(dtPublicHoliday);

			sessionds.ds = dsPublicHoliday;

			sessionds.DataSetRecSize = dsPublicHoliday.Tables[0].Rows.Count;
			sessionds.QueryResultMaxSize = sessionds.DataSetRecSize;
			return  sessionds;
	
		}

		public static void AddNewRowInPHDS(ref SessionDS dsPubHolidayGrid)
		{
			DataRow drNew = dsPubHolidayGrid.ds.Tables[0].NewRow();

			drNew[0] = "";
			drNew[1] = "";
			drNew[2] = System.DBNull.Value;
			drNew[3] = System.DBNull.Value;			
			drNew[4] ="";
			
			try
			{
				dsPubHolidayGrid.ds.Tables[0].Rows.Add(drNew);
			}
			catch(Exception ex)
			{
				Logger.LogTraceError("SysDataMgrDAL","AddNewRowInPHDS","R005",ex.Message.ToString());
			}
			
		}


		/// To query for Enterprise Holiday
		public static SessionDS QueryPublicHoliday(SessionDS sdsPublicHoliday, String strAppID, String strEnterpriseID, int recStart, int recSize)
		{
			
			SessionDS sdsBaseTables = null;
			DateTime dtDateFrom;
			DateTime dtDateTo;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
		
			DataRow dr = sdsPublicHoliday.ds.Tables[0].Rows[0];
			String strCountry = (String)dr[0];
			String strStateCode = (String)dr[1];
			String strSQLQuery = "Select Country, State_code,DateFrom, DateTo, Holiday_Description from Enterprise_Holiday where ";			
			strSQLQuery += "applicationid = '"+strAppID+"' and enterpriseid = '"+strEnterpriseID+"'"; 
			if(strCountry != null && strCountry != "")
			{
				strSQLQuery += " and Country like '%"+strCountry+"%' "; 
			}

			if(strStateCode != null && strStateCode != "")
			{
				strSQLQuery += " and State_code like '%"+strStateCode+"%'"; 
			}

			if(Utility.IsNotDBNull(dr[2]) && dr[2].ToString()!="")
			{
				dtDateFrom = System.Convert.ToDateTime(dr[2]);							
				strSQLQuery += " and DateFrom ='"+dtDateFrom+"'"; 								
			}			
			
			if(Utility.IsNotDBNull(dr[3]) && dr[3].ToString()!="")
			{
				dtDateTo = System.Convert.ToDateTime(dr[3]);							
				strSQLQuery += " and DateTo ='"+dtDateTo+"'"; 								
			}			

			String strHolDesc = (String)dr[4];
			if(strHolDesc != null && strHolDesc != "")
			{
				strSQLQuery += " and Holiday_description like N'%"+Utility.ReplaceSingleQuote(strHolDesc)+"%'"; 
			}			
			sdsBaseTables = (SessionDS)dbCon.ExecuteQuery(strSQLQuery,recStart, recSize,"PublicHoliday");
			return  sdsBaseTables;
		}

		/// To insert a new record to database
		public static bool InsertPublicHoliday(SessionDS sdsBaseTables, int rowIndex, String strAppID, String strEnterpriseID, int applyHolAll)
		{
			IDbCommand dbCom = null;
			String strDateFrom = null;
			String strDateTo = null;
			DataRow dr = sdsBaseTables.ds.Tables[0].Rows[rowIndex];
			String strCountry = (String)dr[0];
			String strStateCode = (String)dr[1];
			if((dr[2].ToString()!="") && (dr[2]!=null)) 
			{
				DateTime dtDateFrom = System.Convert.ToDateTime(dr[2]);
				strDateFrom=Utility.DateFormat(strAppID,strEnterpriseID,dtDateFrom,DTFormat.Date);
				
			}						
			if((dr[3].ToString()!="") && (dr[3]!=null))
			{
				DateTime dtDateTo = System.Convert.ToDateTime(dr[3]);
				//strDateTo=dtDateTo.ToString("dd/MM/yyyy");
				strDateTo = Utility.DateFormat(strAppID,strEnterpriseID,dtDateTo,DTFormat.Date);				
			}						
			String strHolDesc = (String)dr[4];

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
				
			if(strStateCode!=null && strStateCode!="")
			{
				String strSQLQuery = "MERGE dbo.Enterprise_Holiday t ";//      -- target
				strSQLQuery += "USING (";
				strSQLQuery += "SELECT a.applicationid, a.enterpriseid, a.Country, b.State_Code, DateFrom, DateTo, Holiday_description ";
				strSQLQuery += "FROM ( ";
						//-- This is exactly the same VALUES clause that appears in the single row INSERT
				strSQLQuery += "VALUES('"+strAppID+"','"+strEnterpriseID+"','"+strCountry+"','"+strStateCode+"',";
				strSQLQuery += strDateFrom+","+strDateTo+",N'"+Utility.ReplaceSingleQuote(strHolDesc)+"') ";
				strSQLQuery += ") a (applicationid, enterpriseid, Country, State_Code, DateFrom, DateTo, Holiday_description) ";
				strSQLQuery += "INNER JOIN dbo.[State] b ";
				strSQLQuery += "ON a.applicationid = b.applicationid AND a.enterpriseid = b.enterpriseid AND ";
				strSQLQuery += "a.Country = b.Country AND ";
				strSQLQuery += "(a.State_Code = b.State_Code OR " + applyHolAll + " = 1) ) s ";//                            -- source
				strSQLQuery += "ON s.applicationid = t.applicationid AND s.enterpriseid = t.enterpriseid AND  ";
				strSQLQuery += "s.Country = t.Country AND s.state_code = t.state_code AND  ";
				strSQLQuery += "s.datefrom = t.datefrom AND s.dateto = t.dateto ";
				strSQLQuery += "WHEN NOT MATCHED  ";//       -- BY TARGET
				strSQLQuery += "THEN ";

				strSQLQuery += "INSERT (applicationid,enterpriseid,Country,State_Code, DateFrom, DateTo, Holiday_description) ";
				strSQLQuery += " VALUES (s.applicationid, s.enterpriseid, s.Country, s.State_Code, s.DateFrom, s.DateTo, s.Holiday_description); "; 			
				dbCom = dbCon.CreateCommand(strSQLQuery,CommandType.Text);
				if(dbCon.ExecuteNonQuery(dbCom)>0)
					return true;
			}
			return false;
		}

		/// To update records in Enterprise Holiday database
		//public static bool UpdatePublicHoliday(SessionDS sdsBaseTables, String strAppID, String strEnterpriseID)
		//public static bool UpdatePublicHoliday(SessionDS sdsBaseTables, DataRow newDr, int rowIndex, String strAppID, String strEnterpriseID)
		public static bool UpdatePublicHoliday(SessionDS sdsBaseTables, int rowIndex, String strAppID, String strEnterpriseID)
		{
			IDbCommand dbCom = null;
			//String strOldDateFrom = null;
			//String strNewDateFrom = null;
			String strDateFrom = null;
			//String strOldDateTo = null;
			//String strNewDateTo = null;
			String strDateTo = null;
			//DataRow oldDr = sdsBaseTables.ds.Tables[0].Rows[rowIndex];
			DataRow dr = sdsBaseTables.ds.Tables[0].Rows[rowIndex];
			//String strOldCountry = (String) oldDr[0];
			//String strNewCountry = (String) newDr[0];
			//String strOldStateCode = (String) oldDr[1];
			//String strNewStateCode = (String) newDr[1];
			String strCountry = (String) dr[0];
			String strStateCode = (String) dr[1];
			/*if((oldDr[2].ToString() != "") && (oldDr[2] != null))
			{
				DateTime dtOldDateFrom = System.Convert.ToDateTime(oldDr[2]);
				strOldDateFrom = Utility.DateFormat(strAppID, strEnterpriseID, dtOldDateFrom, DTFormat.Date);
			}
			if((newDr[2].ToString() != "") && (newDr[2] != null))
			{
				DateTime dtNewDateFrom = System.Convert.ToDateTime(newDr[2]);
				strNewDateFrom = Utility.DateFormat(strAppID, strEnterpriseID, dtNewDateFrom, DTFormat.Date);
			}*/
			if (dr[2] != null)
			{
				if (!dr[2].ToString().Trim().Equals(""))
				{
					strDateFrom = Utility.DateFormat(strAppID, strEnterpriseID, System.Convert.ToDateTime(dr[2]), DTFormat.Date);
				}
			}
			/*if((oldDr[3].ToString() !="") && (oldDr[3]!=null))
			{
				DateTime dtOldDateTo = System.Convert.ToDateTime(oldDr[3]);
				strOldDateTo = Utility.DateFormat(strAppID, strEnterpriseID, dtOldDateTo, DTFormat.Date);				
			}		
			if((newDr[3].ToString() !="") && (newDr[3]!=null))
			{
				DateTime dtNewDateTo = System.Convert.ToDateTime(newDr[3]);
				strNewDateTo = Utility.DateFormat(strAppID, strEnterpriseID, dtNewDateTo, DTFormat.Date);
			}*/
			if (dr[3] != null)
			{
				if (!dr[3].ToString().Trim().Equals(""))
				{
					strDateTo = Utility.DateFormat(strAppID, strEnterpriseID, System.Convert.ToDateTime(dr[3]), DTFormat.Date);
				}
			}
			//String strOldHolDesc = (String) oldDr[4];
			//String strNewHolDesc = (String) newDr[4];
			String strHolDesc = (String) dr[4];
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			/*if(strStateCode != null && strStateCode != "")
			{
				String	strSQLQuery =" Update Enterprise_Holiday set Holiday_description=N'"+ Utility.ReplaceSingleQuote(strHolDesc) +"' where ";
				strSQLQuery += " applicationid = '"+strAppID+"' and enterpriseid = '"+strEnterpriseID+"' and ";
				strSQLQuery += " Country ='"+strCountry+"' and state_code ='"+strStateCode+"' and DateFrom="+strDateFrom+" and DateTo="+strDateTo;
	
				dbCom = dbCon.CreateCommand(strSQLQuery,CommandType.Text);
				if(dbCon.ExecuteNonQuery(dbCom)>0)
					return true;
			}*/
			StringBuilder strBuilder = new StringBuilder("UPDATE Enterprise_Holiday SET holiday_description = '");
			//strBuilder.Append(strNewHolDesc);
			strBuilder.Append(strHolDesc);
			strBuilder.Append("' ");
			/*if (!strOldDateFrom.Equals(strNewDateFrom))
			{
				strBuilder.Append(", datefrom = ");
				strBuilder.Append(strNewDateFrom);
			}
			if (!strOldDateTo.Equals(strNewDateTo))
			{
				strBuilder.Append(" , dateto = ");
				strBuilder.Append(strNewDateTo);
			}
			if (!strOldCountry.Equals(strNewCountry))
			{
				strBuilder.Append(" , country = '");
				strBuilder.Append(strNewCountry);
				strBuilder.Append("'");
			}
			if (!strOldStateCode.Equals(strNewStateCode))
			{
				strBuilder.Append(" , state_code = '");
				strBuilder.Append(strNewStateCode);
				strBuilder.Append("'");
			}*/
			strBuilder.Append(" WHERE applicationid = '");
			strBuilder.Append(strAppID);
			strBuilder.Append("' AND enterpriseid = '");
			strBuilder.Append(strEnterpriseID);
			strBuilder.Append("' AND country = '");
			//strBuilder.Append(strOldCountry);
			strBuilder.Append(strCountry);
			strBuilder.Append("' AND state_code = '");
			//strBuilder.Append(strOldStateCode);
			strBuilder.Append(strStateCode);
			strBuilder.Append("' AND datefrom = ");
			//strBuilder.Append(strOldDateFrom);
			strBuilder.Append(strDateFrom);
			strBuilder.Append(" AND dateto = ");
			//strBuilder.Append(strOldDateTo);
			strBuilder.Append(strDateTo);
			String strSQLQuery = strBuilder.ToString();
			dbCom = dbCon.CreateCommand(strSQLQuery, CommandType.Text);
			if (dbCon.ExecuteNonQuery(dbCom) > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		//checking already public holiday is present or not	
		public static DataSet IsExistPublicHoliday(SessionDS sdsBaseTables, String strAppID, String strEnterpriseID)
		{
			DataSet dsIsHoliday = null;
			String strDateFrom = null;
			String strDateTo = null;
			DataRow dr = sdsBaseTables.ds.Tables[0].Rows[0];
			String strCountry = (String)dr[0];
			String strStateCode = (String)dr[1];
			if((dr[2].ToString()!="") && (dr[2]!=null))
			{
				DateTime dtDateFrom = System.Convert.ToDateTime(dr[2]);
				strDateFrom=Utility.DateFormat(strAppID,strEnterpriseID,dtDateFrom,DTFormat.Date);
				
			}						
			if((dr[3].ToString()!="") && (dr[3]!=null))
			{
				DateTime dtDateTo = System.Convert.ToDateTime(dr[3]);
				//strDateTo=dtDateTo.ToString("dd/MM/yyyy");
				strDateTo = Utility.DateFormat(strAppID,strEnterpriseID,dtDateTo,DTFormat.Date);				
			}		
			String strHolDesc = (String)dr[4];

			
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("SysDataMgrDAL","GetStatusCodeDS","EDB101","DbConnection object is null!!");
				return dsIsHoliday;
			}
			String	strSQLQuery="";
			if(strStateCode!=null && strStateCode!="")
			{
				strSQLQuery =" select * from Enterprise_Holiday where datefrom >="+strDateFrom+" or dateto >="+strDateTo ;
				strSQLQuery += " and applicationid='"+strAppID+"' and enterpriseid='"+strEnterpriseID+"'  and country='"+strCountry+"' and state_code='"+strStateCode+"'";
			}
 		
			dsIsHoliday = (DataSet)dbCon.ExecuteQuery(strSQLQuery,ReturnType.DataSetType);
			return  dsIsHoliday;	
		}
		
		public static bool IsPHUpdateAllowed(String strAppID,String strEnterpriseID,String strState,String strCountry, DateTime dtStartDate, DateTime dtEndDate)
		{
			bool bUpdateAllowed=false;
			DataSet dsIsHoliday = null;
			DataProviderType dbProvider = DbConnectionManager.GetInstance().GetDbProviderType(strAppID,strEnterpriseID);
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			StringBuilder strSQLQuery = new StringBuilder();
			
			strSQLQuery.Append(" SELECT  est_delivery_datetime, act_delivery_date FROM Shipment where Applicationid = '"+strAppID+"' and enterpriseid = '");
			strSQLQuery.Append(strEnterpriseID+"' and recipient_country='"+strCountry+"' and ( ");  
			switch (dbProvider)
			{
				case DataProviderType.Sql :			
					strSQLQuery.Append(" (Convert(varchar(10),est_delivery_datetime,101) between "+Utility.DateFormat(strAppID,strEnterpriseID,dtStartDate,DTFormat.Date));
					strSQLQuery.Append(" and " + Utility.DateFormat(strAppID,strEnterpriseID,dtEndDate,DTFormat.Date)+") ");
					strSQLQuery.Append(" OR (Convert(varchar(10),act_delivery_date,101) between "+Utility.DateFormat(strAppID,strEnterpriseID,dtStartDate,DTFormat.Date));
					strSQLQuery.Append(" and "+ Utility.DateFormat(strAppID,strEnterpriseID,dtEndDate,DTFormat.Date)+" )");
					strSQLQuery.Append(") ");
					
					break;
				case DataProviderType.Oracle:
					strSQLQuery.Append(" (est_delivery_datetime between "+Utility.DateFormat(strAppID,strEnterpriseID,dtStartDate,DTFormat.Date));
					strSQLQuery.Append(" and " + Utility.DateFormat(strAppID,strEnterpriseID,dtEndDate,DTFormat.Date)+") ");
					strSQLQuery.Append(" OR (act_delivery_date between "+Utility.DateFormat(strAppID,strEnterpriseID,dtStartDate,DTFormat.Date));
					strSQLQuery.Append(" and "+ Utility.DateFormat(strAppID,strEnterpriseID,dtEndDate,DTFormat.Date)+" )");
					strSQLQuery.Append(") ");

					break;
				case DataProviderType.Oledb:
					break;
			}		
			
			if(dbCon == null)
			{
				Logger.LogTraceError("SysDataMgrDAL","GetStatusCodeDS","EDB101","DbConnection object is null!!");				
				return  bUpdateAllowed;
			}

			dsIsHoliday = (DataSet)dbCon.ExecuteQuery(strSQLQuery.ToString(), ReturnType.DataSetType);
			
			DataRow drCurr=null;
			if(dsIsHoliday.Tables[0].Rows.Count >0)
			{
				drCurr=dsIsHoliday.Tables[0].Rows[0];				
			}

			DateTime dEstDelvyDate=System.DateTime.Now;
			if (Utility.IsNotDBNull(drCurr["est_delivery_datetime"]) && drCurr["est_delivery_datetime"].ToString()!="")
			{
				dEstDelvyDate=System.Convert.ToDateTime(drCurr["est_delivery_datetime"]);
			}
			DateTime dActDelvyDate=System.DateTime.Now;
			if (Utility.IsNotDBNull(drCurr["act_delivery_date"]) && drCurr["act_delivery_date"].ToString()!="")
			{
				dActDelvyDate=System.Convert.ToDateTime(drCurr["act_delivery_date"]);
			}
			if ((dtStartDate > dEstDelvyDate) || (dtStartDate > dEstDelvyDate))
			{
				return bUpdateAllowed;
			}
			if ((dtStartDate > dActDelvyDate) || (dtStartDate > dActDelvyDate))
			{
				return bUpdateAllowed;
			}

			if ((dtEndDate < dEstDelvyDate) || (dtEndDate < dEstDelvyDate))
			{
				return bUpdateAllowed;
			}
			if ((dtEndDate < dActDelvyDate) || (dtEndDate < dActDelvyDate))
			{
				return bUpdateAllowed;
			}


			strSQLQuery=new StringBuilder();
			strSQLQuery.Append(" SELECT act_pickup_datetime FROM Pickup_Request where ApplicationId = '"+strAppID+"' and EnterpriseId = '");
			strSQLQuery.Append(strEnterpriseID+"' and Sender_Country='"+strCountry+"' and ( ");  
			switch (dbProvider)
			{
				case DataProviderType.Sql :		
					strSQLQuery.Append(" (Convert(Varchar(10),act_pickup_datetime,101) between "+Utility.DateFormat(strAppID,strEnterpriseID,dtStartDate,DTFormat.Date));
					strSQLQuery.Append(" and " + Utility.DateFormat(strAppID,strEnterpriseID,dtEndDate,DTFormat.Date)+") ");
					strSQLQuery.Append(") ");
					
					break;
				case DataProviderType.Oracle:
					strSQLQuery.Append(" (act_pickup_datetime between "+Utility.DateFormat(strAppID,strEnterpriseID,dtStartDate,DTFormat.Date));
					strSQLQuery.Append(" and " + Utility.DateFormat(strAppID,strEnterpriseID,dtEndDate,DTFormat.Date)+") ");
					strSQLQuery.Append(") ");
					
					break;
				case DataProviderType.Oledb:
					break;
			}		

			
			dsIsHoliday = (DataSet)dbCon.ExecuteQuery(strSQLQuery.ToString(),ReturnType.DataSetType);
			
			if(dsIsHoliday.Tables[0].Rows.Count >0)
			{
				drCurr=dsIsHoliday.Tables[0].Rows[0];
			}
			
			DateTime dActPickupDate=System.DateTime.Now;
			if (Utility.IsNotDBNull(drCurr["act_pickup_datetime"]) && drCurr["act_pickup_datetime"].ToString()!="")
			{
				dActPickupDate=System.Convert.ToDateTime(drCurr["act_pickup_datetime"]);
			}
			if ((dtStartDate > dActPickupDate) || (dtStartDate > dActPickupDate))
			{
				return bUpdateAllowed;
			}			
			if ((dtEndDate < dActPickupDate) || (dtEndDate < dActPickupDate))
			{
				return bUpdateAllowed;
			}
			else
			{
				return true;
			}

		}

		public static bool IsHolidayUsed(String strAppID,String strEnterpriseID,String strCountry,String strState, DateTime dtStartDate, DateTime dtEndDate)
		{
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			DataProviderType dbProvider = DbConnectionManager.GetInstance().GetDbProviderType(strAppID,strEnterpriseID);
				
			StringBuilder strSQLQuery = new StringBuilder();
			
			strSQLQuery.Append(" SELECT Count(*) FROM Shipment where Applicationid = '"+strAppID+"' and enterpriseid = '");
			strSQLQuery.Append(strEnterpriseID+"' and recipient_country='"+strCountry+"' and ( ");  //State_Code='"+strState+"' and
			switch (dbProvider)
			{
				case DataProviderType.Sql :
					strSQLQuery.Append(" (Convert(Varchar(10),Est_delivery_datetime,101) between "+Utility.DateFormat(strAppID,strEnterpriseID,dtStartDate,DTFormat.Date));
					strSQLQuery.Append(" and " + Utility.DateFormat(strAppID,strEnterpriseID,dtEndDate,DTFormat.Date)+") ");
					strSQLQuery.Append(" OR (Convert(Varchar(10),act_delivery_date,101) between "+Utility.DateFormat(strAppID,strEnterpriseID,dtStartDate,DTFormat.Date));
					strSQLQuery.Append(" and "+ Utility.DateFormat(strAppID,strEnterpriseID,dtEndDate,DTFormat.Date)+" )");
					strSQLQuery.Append(") ");
					break;	
				case DataProviderType.Oracle:
					strSQLQuery.Append(" (Est_delivery_datetime between "+Utility.DateFormat(strAppID,strEnterpriseID,dtStartDate,DTFormat.Date));
					strSQLQuery.Append(" and " + Utility.DateFormat(strAppID,strEnterpriseID,dtEndDate,DTFormat.Date)+") ");
					strSQLQuery.Append(" OR (act_delivery_date between "+Utility.DateFormat(strAppID,strEnterpriseID,dtStartDate,DTFormat.Date));
					strSQLQuery.Append(" and "+ Utility.DateFormat(strAppID,strEnterpriseID,dtEndDate,DTFormat.Date)+" )");
					strSQLQuery.Append(") ");
					break;
				case DataProviderType.Oledb:
					break;
			}		

			int iRecordsAffected=(int)dbCon.ExecuteScalar(strSQLQuery.ToString());
			if (iRecordsAffected > 0)
			{
				return true;
			}
			
			strSQLQuery=new StringBuilder();
			strSQLQuery.Append(" SELECT Count(*) FROM Pickup_Request where ApplicationId = '"+strAppID+"' and EnterpriseId = '");
			strSQLQuery.Append(strEnterpriseID+"' and Sender_Country='"+strCountry+"' and ( ");  

			switch (dbProvider)
			{
				case DataProviderType.Sql :
					strSQLQuery.Append(" (Convert(Varchar(10),act_pickup_datetime,101) between "+Utility.DateFormat(strAppID,strEnterpriseID,dtStartDate,DTFormat.Date));
					strSQLQuery.Append(" and " + Utility.DateFormat(strAppID,strEnterpriseID,dtEndDate,DTFormat.Date)+") ");			
					strSQLQuery.Append(") ");

					break;	
				case DataProviderType.Oracle:
					strSQLQuery.Append(" (act_pickup_datetime between "+Utility.DateFormat(strAppID,strEnterpriseID,dtStartDate,DTFormat.Date));
					strSQLQuery.Append(" and " + Utility.DateFormat(strAppID,strEnterpriseID,dtEndDate,DTFormat.Date)+") ");			
					strSQLQuery.Append(") ");
					break;
				case DataProviderType.Oledb:
					break;
			}		

			
			iRecordsAffected=(int)dbCon.ExecuteScalar(strSQLQuery.ToString());
			if (iRecordsAffected > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
			
		}

		/// to delele record in Enterprise Holiday
		public static bool DeletePublicHoliday(String strAppID, String strEnterpriseID, String strCountry, String strStateCode, DateTime dtDateFrom, DateTime dtDateTo)
		{
			IDbCommand dbCom = null;
			String strSQLQuery = null;
			DataProviderType dbProvider = DbConnectionManager.GetInstance().GetDbProviderType(strAppID,strEnterpriseID);
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			switch (dbProvider)
			{
				case DataProviderType.Sql :
					strSQLQuery = "Delete from Enterprise_Holiday where applicationid = '";
					strSQLQuery += strAppID+"' and enterpriseid = '"+strEnterpriseID+"' and Country='"+strCountry+"'";
					//strSQLQuery += " and State_Code= '"+strStateCode+"' and DateFrom='"+dtDateFrom+"' and DateTo='"+dtDateTo+"'";
					
					strSQLQuery += " and State_Code= '"+strStateCode+"' and DateFrom="+Utility.DateFormat(strAppID,strEnterpriseID,dtDateFrom,DTFormat.DateTime)+" and DateTo="+Utility.DateFormat(strAppID,strEnterpriseID,dtDateTo,DTFormat.DateTime)+"";
					break;
				
				case DataProviderType.Oracle:
					strSQLQuery = "Delete from Enterprise_Holiday where applicationid = '";
					strSQLQuery += strAppID+"' and enterpriseid = '"+strEnterpriseID+"' and Country='"+strCountry+"'";
					strSQLQuery += " and State_Code= '"+strStateCode+"' and DateFrom = TO_date('"+dtDateFrom.ToString("MM/dd/yyyy HH:mm") +"','MM/dd/yyyy HH24:mi')";
					strSQLQuery += " and DateTo = TO_date('"+dtDateTo.ToString("MM/dd/yyyy HH:mm")+"','MM/dd/yyyy HH24:mi')";
					break;
				case DataProviderType.Oledb:
					break;
			}		
			


			dbCom = dbCon.CreateCommand(strSQLQuery,CommandType.Text);
			if(dbCon.ExecuteNonQuery(dbCom)>0)
				return true;
			return false;
		}


		//**RouteCode Base Table StartDate:26'DEC -Rajitha**//
		public static SessionDS GetEmptyRouteCodeDS()
		{

			DataTable dtRouteCode =  new DataTable();
			
			dtRouteCode.Columns.Add(new DataColumn("path_code",typeof(string)));
			dtRouteCode.Columns.Add(new DataColumn("route_code",typeof(string)));
			dtRouteCode.Columns.Add(new DataColumn("route_description",typeof(string))); 
			DataSet  dsRouteCode = new DataSet();
			dsRouteCode.Tables.Add(dtRouteCode);  

			dsRouteCode.Tables[0].Columns["path_code"].AllowDBNull = false;

			SessionDS sessionDS = new SessionDS();
			sessionDS.ds = dsRouteCode;
			sessionDS.DataSetRecSize = dsRouteCode.Tables[0].Rows.Count;
			sessionDS.QueryResultMaxSize = sessionDS.DataSetRecSize;

			return  sessionDS; 
		}

		public static void AddNewRowInRouteCode(ref SessionDS dsRouteCode)
		{
			DataRow drRow = dsRouteCode.ds.Tables[0].NewRow();
			drRow["path_code"]="";
			drRow["route_code"]="";
			drRow["route_description"]="";
			try
			{
				dsRouteCode.ds.Tables[0].Rows.Add(drRow); 
			}
			catch(Exception ex)
			{
				Logger.LogTraceError("SysDataMgrDAL","DeleteRouteCodeDS","R005",ex.Message.ToString());
			}							
		}

		public static int InsertRouteCode(String strAppId,String strEnterpriseId, int rowIndex, DataSet dsRouteCode)
		{
			int iRowsEffected=0;
			IDbCommand dbCmd =null;
			String strQry=null;

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppId,strEnterpriseId);

			if(dbCon == null)
			{
				Logger.LogTraceError("SysDataMgrDAL","InsertRouteCode","R001","DbConnection object is null!!");
				return iRowsEffected;
			}
			
			strQry="Insert into route_code (applicationid,enterpriseid,path_code,route_code,";
			strQry=strQry+"route_description)values('"+(strAppId)+"','"+strEnterpriseId+"',";

			DataRow drEach =  dsRouteCode.Tables[0].Rows[rowIndex];

			if(Utility.IsNotDBNull(drEach["path_code"]) && drEach["path_code"].ToString()!="")
			{
				strQry=strQry+"'"+drEach["path_code"].ToString()+"',";
			}
			else
			{
				strQry=strQry+"null";
			}
			if(Utility.IsNotDBNull(drEach["route_code"]) && drEach["route_code"].ToString()!="")
			{
				strQry=strQry+"'" +Utility.ReplaceSingleQuote(drEach["route_code"].ToString())+"',";
			}
			else
			{
				strQry=strQry+"null";
			}
			if(Utility.IsNotDBNull(drEach["route_description"]) && drEach["route_description"].ToString()!="")
			{
				strQry=strQry+"N'"+ Utility.ReplaceSingleQuote(drEach["route_description"].ToString())+"')";
			}
			else
			{
				strQry=strQry+"null"+")";
			}
			try
			{
				dbCmd = dbCon.CreateCommand(strQry,CommandType.Text);
				iRowsEffected = dbCon.ExecuteNonQuery(dbCmd);
				return iRowsEffected;
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("SysDataMgrDAL","InsertRouteCode","R002","Error During Insert "+ appException.Message.ToString());
				throw new ApplicationException("Error inserting Route Code ",appException);
			}

			
		}


		public static int UpdateRouteCode(String strAppId,String strEnterpriseId,DataSet dsRouteCode)
		{
			int iRowsEffected = 0;
			string strQry=null;

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppId,strEnterpriseId);
   
			if(dbCon==null)
			{
				Logger.LogTraceError("SysDataMgrDAL","UpdateRouteCode","R001","DbConnection object is null!!");
				return iRowsEffected;
			}
			int iRows = dsRouteCode.Tables[0].Rows.Count;
			DataRow drEach = dsRouteCode.Tables[0].Rows[0];  
			try
			{
				strQry="Update route_code set route_description = ";
				strQry=strQry+"N'"+Utility.ReplaceSingleQuote(drEach["route_description"].ToString())+"'  ";
				strQry=strQry+"where path_code = ";
				strQry=strQry+"'"+drEach["path_code"].ToString()+"' ";
				strQry=strQry+"and route_code = ";
				strQry=strQry+"'"+Utility.ReplaceSingleQuote(drEach["route_code"].ToString())+"'";
				strQry=strQry+" and applicationid = ";
				strQry=strQry+"'"+strAppId+"'";
				strQry=strQry+" and enterpriseid = ";
				strQry=strQry+"'"+strEnterpriseId+"'";


				IDbCommand dbcmd = dbCon.CreateCommand(strQry,CommandType.Text);
				iRowsEffected = dbCon.ExecuteNonQuery(dbcmd);
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("SysDataMgrDAL","UpdateRouteCode","R002","Error During Insert "+ appException.Message.ToString());
				throw new ApplicationException("Error Updating Route Code ",appException);
			}
  
			return iRowsEffected; 
  
		}

		public static int CheckRouteCodeRecord(String strAppId,String strEnterpriseId,String strPathCode,String strRouteCode)
		{
			int iRowsEffected = 0;
			string strQry=null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppId,strEnterpriseId);
			if(dbCon==null)
			{
				Logger.LogTraceError("SysDataMgrDAL","UpdateRouteCode","R001","DbConnection object is null!!");
				return iRowsEffected;
			}
			try
			{
				strQry ="Select * from route_code where path_code = "+"'"+strPathCode+"'"+"and route_code = "+"'"+Utility.ReplaceSingleQuote(strRouteCode)+"'";
				strQry = strQry+"and applicationid = "+"'"+strAppId+"'"+"and enterpriseid = "+"'"+strEnterpriseId+"'";
				IDbCommand dbCmd = dbCon.CreateCommand(strQry,CommandType.Text);
				DataSet dsRouteCode = (DataSet)dbCon.ExecuteQuery(dbCmd,ReturnType.DataSetType);  
				iRowsEffected = dsRouteCode.Tables[0].Rows.Count;  
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("SysDataMgrDAL","CheckRouteCodeRecord","R002",appException.Message.ToString());
				throw new ApplicationException("Error ",appException);
			}
			return iRowsEffected;
		}

		public static SessionDS GetRouteCodeDS(String strAppId,String strEnterpriseId, int iCurRow, int iDSRowSize, DataSet dsRouteCode)
		{
			SessionDS sessionDS = null;
			string strQry=null;
			string strPathCode=null;
			string strRouteCode=null;
			string strRouteDescription=null;
			
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppId,strEnterpriseId);
			if(dbCon==null)
			{
				Logger.LogTraceError("SysDataMgrDAL","UpdateRouteCode","R001","DbConnection object is null!!");
			}
			DataRow drEach = dsRouteCode.Tables[0].Rows[0];

			strQry= strQry +"Select path_code,route_code,route_description  from route_code";
			strQry=strQry+" where applicationid = '"+strAppId+"' and enterpriseid = '"+strEnterpriseId+"'";
 
			strPathCode = drEach["path_code"].ToString(); 
			if((strPathCode !=null)&&(strPathCode!=""))
			{
				strQry=strQry+" and  path_code like ";
				strQry=strQry +"'%"+strPathCode+"%'"; 
			}
			strRouteCode = drEach["route_code"].ToString(); 
			if((strRouteCode !=null)&&(strRouteCode!=""))
			{
				strQry=strQry+" and route_code like ";
				strQry=strQry +"'%"+Utility.ReplaceSingleQuote(strRouteCode)+"%'"; 
			}
			strRouteDescription = drEach["route_description"].ToString(); 
			if((strRouteDescription !=null)&&(strRouteDescription!=""))
			{
				strQry=strQry+" and route_description like ";
				strQry=strQry +"N'%"+Utility.ReplaceSingleQuote(strRouteDescription)+"%'"; 
			}
			try
			{				
				sessionDS=dbCon.ExecuteQuery(strQry,iCurRow,iDSRowSize,"RouteTable");				
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("SysDataMgrDAL","GetRouteCodeDS","R004",appException.Message.ToString());
				throw new ApplicationException("Error Retrieving records",appException);
			}
			return  sessionDS;
		}

		public static int DeleteRouteCodeDS(String strAppId,string strEnterpriseId,String strRouteCode, String strPathCode)
		{
			int iRowsEffected = 0;
			string strQry=null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppId,strEnterpriseId);
			if(dbCon==null)
			{
				Logger.LogTraceError("SysDataMgrDAL","UpdateRouteCode","R001","DbConnection object is null!!");
			}			

			strQry="Delete from  route_code where path_code ='"+strPathCode+"' and route_code = '";
			strQry+=Utility.ReplaceSingleQuote(strRouteCode)+"' and applicationid = '";
			strQry+=strAppId+"' and enterpriseid = '"+strEnterpriseId+"'";

			try
			{
				IDbCommand dbCmd = dbCon.CreateCommand(strQry,CommandType.Text);
				iRowsEffected =dbCon.ExecuteNonQuery(dbCmd);
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("SysDataMgrDAL","DeleteRouteCodeDS","R005",appException.Message.ToString());
				throw new ApplicationException("Error deleting records",appException);
			}		
			return iRowsEffected; 
		}
		
		public static int DeleteRouteCodeDS(String strAppId,string strEnterpriseId,DataSet dsRouteCode,int rowindex)
		{
			int iRowsEffected = 0;
			string strQry=null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppId,strEnterpriseId);
			if(dbCon==null)
			{
				Logger.LogTraceError("SysDataMgrDAL","UpdateRouteCode","R001","DbConnection object is null!!");
			}
			DataRow drEach = dsRouteCode.Tables[0].Rows[rowindex];

			strQry="Delete from  route_code  ";
			strQry=strQry+"where path_code = ";
			strQry=strQry+"'"+drEach["path_code"]+"' ";
			strQry=strQry+"and route_code = ";
			strQry=strQry+"'"+Utility.ReplaceSingleQuote(drEach["route_code"].ToString())+"'";
			strQry=strQry+" and applicationid = ";
			strQry=strQry+"'"+strAppId+"'";
			strQry=strQry+" and enterpriseid = ";
			strQry=strQry+"'"+strEnterpriseId+"'";
			try
			{
				IDbCommand dbCmd = dbCon.CreateCommand(strQry,CommandType.Text);
				iRowsEffected =dbCon.ExecuteNonQuery(dbCmd);
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("SysDataMgrDAL","DeleteRouteCodeDS","R005",appException.Message.ToString());
				throw new ApplicationException("Error deleting records",appException);
			}

		
			return iRowsEffected; 
		}
		//**DeliveryPathCode Base Table StartDate:30'DEC -Rajitha**//
		public static SessionDS GetEmptyDlvPathCode()
		{
			DataTable dtDeliveryPathCode =  new DataTable();
			
			dtDeliveryPathCode.Columns.Add(new DataColumn("path_code",typeof(string)));
			dtDeliveryPathCode.Columns.Add(new DataColumn("path_description",typeof(string)));
			dtDeliveryPathCode.Columns.Add(new DataColumn("delivery_type",typeof(string))); 
			dtDeliveryPathCode.Columns.Add(new DataColumn("line_haul_cost",typeof(decimal))); 
			dtDeliveryPathCode.Columns.Add(new DataColumn ("origin_station",typeof(string)));
			dtDeliveryPathCode.Columns.Add(new DataColumn("destination_station",typeof(string))); 
			dtDeliveryPathCode.Columns.Add(new DataColumn("flight_vehicle_no",typeof(string))); 
			dtDeliveryPathCode.Columns.Add(new DataColumn("awb_driver_name",typeof(string))); 
			dtDeliveryPathCode.Columns.Add(new DataColumn("departure_time",typeof(DateTime)));
			dtDeliveryPathCode.Columns.Add(new DataColumn("active",typeof(string)));

			DataSet  dsDeliveryPathCode = new DataSet();
			dsDeliveryPathCode.Tables.Add(dtDeliveryPathCode); 

			dsDeliveryPathCode.Tables[0].Columns["path_code"].AllowDBNull = false;

			SessionDS sessionds = new SessionDS();
			sessionds.ds = dsDeliveryPathCode;
				
			sessionds.QueryResultMaxSize = sessionds.DataSetRecSize;
			return  sessionds;

		}

		public static void AddNewRowInDeliveryPathCodeDS(ref SessionDS dsDeliveryPathCode)
		{
			DataRow drNew = dsDeliveryPathCode.ds.Tables[0].NewRow();
			drNew[0] = "";
			drNew[1] = "";
			drNew[2] = "";
			try
			{
				dsDeliveryPathCode.ds.Tables[0].Rows.Add(drNew);
			}
			catch(Exception ex)
			{
				Logger.LogTraceError("SysDataMgrDAL","DeleteRouteCodeDS","R005",ex.Message.ToString());
			}
		}

		public static int InsertDeliveryPathCode(DataSet dsDeliveryPathCode, int rowIndex, String strAppID, String strEnterpriseID)
		{
			int iRowsInserted = 0;
			IDbCommand dbCom = null;
			DataRow dr = dsDeliveryPathCode.Tables[0].Rows[rowIndex];
			string strPathCode = (string)dr[0];
			string strPathDescp=(string)dr[1];
			string strDelvyType=(string)dr[2];
			decimal decLnHaulCost=System.Convert.ToDecimal(dr[3]);
			string strOriginSt=(String)dr[4];
			string strDestSt=(String)dr[5];
			

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("SysDataMgrDAL","InsertDeliverypathCode","DP001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			try
			{
				if(strPathCode!=null && strPathCode!="")
				{
					string tmpOrg = Utility.ReplaceSingleQuote(strOriginSt);
					tmpOrg = tmpOrg.ToUpper();
					string tmpDest = Utility.ReplaceSingleQuote(strDestSt);
					tmpDest = tmpDest.ToUpper();
					string strSQLQuery = "insert into Delivery_Path (applicationid, enterpriseid, path_code,path_description,delivery_type,";
					strSQLQuery+="line_haul_cost,origin_station,destination_station, flight_vehicle_no, awb_driver_name, departure_time"+") ";
					strSQLQuery += "values ('"+strAppID+"', '"+strEnterpriseID+"', '"+strPathCode+"', N'"+Utility.ReplaceSingleQuote(strPathDescp)+"', '"+strDelvyType+"', ";
					strSQLQuery+=decLnHaulCost+", '"+ tmpOrg +"' , '"+ tmpDest +"',";
						
						
					if((dr[6]!= null) && (!dr[6].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						String strFlight = (String)dr[6];
						strSQLQuery =strSQLQuery+ "'" + Utility.ReplaceSingleQuote(strFlight)+"',";
					}
					else
					{
						strSQLQuery =strSQLQuery+ "null,";
					}

					if((dr[7]!= null) && (!dr[7].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						String strAWB = (String)dr[7];
						strSQLQuery =strSQLQuery+ "'" + Utility.ReplaceSingleQuote(strAWB)+"',";
					}
					else
					{
						strSQLQuery =strSQLQuery+ "null,";
					}

					if((dr[8]!= null) && (!dr[8].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						DateTime dtApproxDepart = Convert.ToDateTime(dr[8]);
						strSQLQuery =strSQLQuery+ Utility.DateFormat(strAppID,strEnterpriseID,dtApproxDepart,DTFormat.DateTime);
					}
					else
					{
						strSQLQuery =strSQLQuery+ "null";
					}
					
					strSQLQuery =strSQLQuery + ")";
					dbCom = dbCon.CreateCommand(strSQLQuery,CommandType.Text);

					iRowsInserted = dbCon.ExecuteNonQuery(dbCom);
				}
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("SysDataMgrDAL","InsertDeliverypathCode","DP002",appException.Message.ToString());
				throw new ApplicationException("Insert operation failed"+appException.Message,appException);
			}
			return iRowsInserted;
		}
		public static int UpdateDeliveryPathCode(DataSet dsDeliveryPathCode, String strAppID, String strEnterpriseID)
		{
			int iRowsUpdated = 0;
			String strSQLQuery=null;
			IDbCommand dbCom = null;
			DataRow dr = dsDeliveryPathCode.Tables[0].Rows[0];
			string strPathCode = (string)dr[0];
			string strPathDescp=(string)dr[1];
			string strDelvyType=(string)dr[2];
			decimal decLnHaulCost=System.Convert.ToDecimal(dr[3]);
			string strOriginSt=(String)dr[4];
			string strDestSt=dr[5].ToString();
			

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{

				Logger.LogTraceError("SysDataMgrDAL","UpdateDeliveryPathCode","DP004","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			try
			{
				if(strPathCode!=null && strPathCode!="")
				{
					string tmpOrg = Utility.ReplaceSingleQuote(strOriginSt);
					tmpOrg = tmpOrg.ToUpper();
					string tmpDest = Utility.ReplaceSingleQuote(strDestSt);
					tmpDest = tmpDest.ToUpper();

					strSQLQuery = "update delivery_path set path_description = N'"+Utility.ReplaceSingleQuote(strPathDescp)+"',";
					strSQLQuery =strSQLQuery+ "delivery_type  = '"+strDelvyType+"',";
					strSQLQuery =strSQLQuery+ "line_haul_cost  = "+decLnHaulCost+",";
					strSQLQuery =strSQLQuery+ "origin_station  = '"+tmpOrg+"',";
					strSQLQuery =strSQLQuery+ "destination_station  = '"+tmpDest+"', ";

					if((dr[6]!= null) && (!dr[6].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						String strFlight = (String)dr[6];
						strSQLQuery =strSQLQuery+ "flight_vehicle_no  = '"+Utility.ReplaceSingleQuote(strFlight)+"', ";
					}
					else
					{
						strSQLQuery =strSQLQuery+ "flight_vehicle_no  = null, ";
					}

					if((dr[7]!= null) && (!dr[7].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						String strAWB = (String)dr[7];
						strSQLQuery =strSQLQuery+ "awb_driver_name  = '"+Utility.ReplaceSingleQuote(strAWB)+"', ";
					}
					else
					{
						strSQLQuery =strSQLQuery+ "awb_driver_name  = null, ";
					}

					if((dr[8]!= null) && (!dr[8].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						DateTime dtApproxDepart = Convert.ToDateTime(dr[8]);
						strSQLQuery =strSQLQuery+ "departure_time  =  " + Utility.DateFormat(strAppID,strEnterpriseID,dtApproxDepart,DTFormat.DateTime);
					}
					else
					{
						strSQLQuery =strSQLQuery+ "departure_time  = null";
					}
					
					strSQLQuery += " where applicationid = '"+strAppID+"' and enterpriseid = '"+strEnterpriseID;
					strSQLQuery += "' and path_code = '"+strPathCode+"'"; 
	
					dbCom = dbCon.CreateCommand(strSQLQuery,CommandType.Text);
					iRowsUpdated = dbCon.ExecuteNonQuery(dbCom);
				}
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("SysDataMgrDAL","UpdateDeliveryPathCode","DP005",appException.Message.ToString());
				throw new ApplicationException("Update operation failed",appException);
				
			}
			return iRowsUpdated;
		}
		public static SessionDS GetDeliveryPathCodeDS(String strAppID, String strEnterpriseID, int iCurRow, int iDSRowSize, DataSet dsQuery)
		{
			SessionDS sessionDS = null;
			DataRow dr = dsQuery.Tables[0].Rows[0];
			string strPathCode = (string)dr[0];
			string strPathDescp=(string)dr[1];
			string strDelvyType=(string)dr[2];
			decimal decLnHaulCost=System.Convert.ToDecimal(dr[3]);
			string strOriginSt=(String)dr[4];
			string strDestSt=(String)dr[5];
			string strFlight=(String)dr[6];
			string strAWB=(String)dr[7];
			string strActive=(String)dr[9];
			DateTime dtApprox;
			if(strActive=="N")
			{
				if(strDelvyType=="A")
					strDelvyType = "1";
				else if(strDelvyType=="S")
					strDelvyType = "2";
				else if(strDelvyType=="L")
					strDelvyType = "3";
			}
			string strSQLWhere = "";
			if (strPathCode!=null && strPathCode!="")
			{
				strSQLWhere += "and path_code like '%"+strPathCode+"%' ";
			}
			if (strPathDescp!=null && strPathDescp!="")
			{
				strSQLWhere += "and path_description like N'%"+Utility.ReplaceSingleQuote(strPathDescp)+"%' ";
			}
			if (strDelvyType!=null && strDelvyType!="")
			{
				strSQLWhere += "and delivery_type like '%"+strDelvyType+"%' ";
			}
			if (decLnHaulCost.ToString() !="0")
			{
				strSQLWhere += "and line_haul_cost ="+decLnHaulCost;
			}
			if (strOriginSt!=null && strOriginSt!="")
			{
				strSQLWhere += "and origin_station like '%"+Utility.ReplaceSingleQuote(strOriginSt)+"%' ";
			}
			if (strDestSt!=null && strDestSt!="")
			{
				strSQLWhere += "and destination_station like '%"+Utility.ReplaceSingleQuote(strDestSt)+"%' ";
			}
			if (strFlight!=null && strFlight!="")
			{
				strSQLWhere += "and flight_vehicle_no like '%"+Utility.ReplaceSingleQuote(strFlight)+"%' ";
			}
			if (strAWB!=null && strAWB!="")
			{
				strSQLWhere += "and awb_driver_name like '%"+Utility.ReplaceSingleQuote(strAWB)+"%' ";
			}
			if(Utility.IsNotDBNull(dr[8]) && dr[8].ToString()!="")
			{
				dtApprox = System.Convert.ToDateTime(dr[8]);							
				strSQLWhere += " and departure_time ='"+dtApprox+"'"; 	
			}

			//DataSet dsStateCode = null;
			
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("SysDataManager","GetStateCodeDS","EDB101","DbConnection object is null!!");
				return sessionDS;
			}
			
			String strSQLQuery = "select path_code,path_description,delivery_type,line_haul_cost,origin_station,destination_station,flight_vehicle_no,awb_driver_name,departure_time from delivery_path where ";
			strSQLQuery += "applicationid = '"+strAppID+"' and enterpriseid = '"+strEnterpriseID+"'"; 
			if (strSQLWhere!=null && strSQLWhere!="")
			{
				strSQLQuery += strSQLWhere;
			}
			
			sessionDS = dbCon.ExecuteQuery(strSQLQuery,iCurRow,iDSRowSize,"DeliveryPathTable");
			DataSet dsDlvPathCode = sessionDS.ds;
			dsDlvPathCode.Tables[0].Columns.Add(new DataColumn("active",typeof(string)));
			if(dsDlvPathCode.Tables[0].Rows.Count > 0)
			{
				DataRow drTmp = dsDlvPathCode.Tables[0].Rows[0];
				drTmp[9] = strActive;
			}
			sessionDS.ds = dsDlvPathCode;
			return  sessionDS;
		}

		public static int DeleteDeliveryPathCode(DataSet dsDeliveryPathCode, int rowIndex, String strAppID, String strEnterpriseID)
		{
			int iRowsDeleted = 0;
			IDbCommand dbCom = null;
			DataRow dr = dsDeliveryPathCode.Tables[0].Rows[rowIndex];
			string strPathCode = (string)dr[0];
		

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("SysDataMgr","DeleteDeliveryPathCode","DPC001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			
			try
			{
				if(strPathCode!=null && strPathCode!="")
				{
					string strSQLQuery = "delete from delivery_path where ";
					strSQLQuery += "applicationid = "+"'"+strAppID+"' and enterpriseid = '"+strEnterpriseID;
					strSQLQuery += "' and path_code = '"+strPathCode+"'"; 
	
					dbCom = dbCon.CreateCommand(strSQLQuery,CommandType.Text);
					iRowsDeleted = dbCon.ExecuteNonQuery(dbCom);
				}
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("SysDataMgr","DeleteDeliveryPathCode","DPC002",appException.Message.ToString());
			
				throw new ApplicationException("Delete operation failed"+appException.Message,appException);
			}
			return iRowsDeleted;
		}
		

		public static bool IsCodeIsExistInDC(String strAppID, String strEnterpriseID,String strPathCode)
		{
			int iCurRow = 0;
			int iDSRowSize = 0;
			SessionDS sessionDS = null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("SysDataMgr","SelectDistributionCenter","DPC001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			
			try
			{
				if(strPathCode!=null && strPathCode!="")
				{
					string strSQLQuery = "select * from Distribution_center where ";
					strSQLQuery += "applicationid = "+"'"+strAppID+"' and enterpriseid = '"+strEnterpriseID;
					strSQLQuery += "' and origin_code = '"+strPathCode+"'"; 
	
					sessionDS = dbCon.ExecuteQuery(strSQLQuery,iCurRow,iDSRowSize,"DCTable");
				}
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("SysDataMgr","SelectDistributionCenter","DPC002",appException.Message.ToString());
			
				throw new ApplicationException("Select operation failed"+appException.Message,appException);
			}

			if (sessionDS.ds.Tables[0].Rows.Count == 0)
				return false;
			else
				return true;
		}
		
		
		public static bool IsPathCodeIsExistManifest(String strAppID, String strEnterpriseID,String strPathCode)
		{
			int iCurRow = 0;
			int iDSRowSize = 0;
			SessionDS sessionDS = null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("SysDataMgr","SelectDistributionCenter","DPC001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			
			try
			{
				if(strPathCode!=null && strPathCode!="")
				{
					string strSQLQuery = "select * from Delivery_Manifest where ";
					strSQLQuery += "applicationid = "+"'"+strAppID+"' and enterpriseid = '"+strEnterpriseID;
					strSQLQuery += "' and path_code = '"+strPathCode+"'"; 
	
					sessionDS = dbCon.ExecuteQuery(strSQLQuery,iCurRow,iDSRowSize,"DCTable");
				}
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("SysDataMgr","SelectDistributionCenter","DPC002",appException.Message.ToString());
			
				throw new ApplicationException("Select operation failed"+appException.Message,appException);
			}

			if (sessionDS == null || sessionDS.ds.Tables[0].Rows.Count == 0)
				return false;
			else
				return true;
		}
		
		
		//**DeliveryPathCode Base Table StartDate:2'Jan -Rajitha**//
		public static SessionDS GetEmptyCostCode()
		{
			DataTable dtCostCode =  new DataTable();
			
			dtCostCode.Columns.Add(new DataColumn("Cost_code",typeof(string)));
			dtCostCode.Columns.Add(new DataColumn("Cost_Code_description",typeof(string)));
			dtCostCode.Columns.Add(new DataColumn("cost_type",typeof(string))); 

			DataSet  dsCostCode = new DataSet();
			dsCostCode.Tables.Add(dtCostCode); 
 
			//			dsCostCode.Tables[0].Columns["Cost_code"].Unique = true; //Checks duplicate records..
			dsCostCode.Tables[0].Columns["Cost_code"].AllowDBNull = false;

			SessionDS sessionds = new SessionDS();
			sessionds.ds = dsCostCode;
				
			sessionds.QueryResultMaxSize = sessionds.DataSetRecSize;
			return  sessionds;

		}
		public static void AddNewRowInCostCodeDS(ref SessionDS dsCostCode)
		{
			DataRow drNew = dsCostCode.ds.Tables[0].NewRow();
			drNew[0] = "";
			drNew[1] = "";
			drNew[2] = "";
			try
			{
				dsCostCode.ds.Tables[0].Rows.Add(drNew);
			}
			catch(Exception ex)
			{
				Logger.LogTraceError("SysDataMgrDAL","DeleteRouteCodeDS","R005",ex.Message.ToString());
			}			
		}

		public static SessionDS GetCostCodeDS(String strAppID, String strEnterpriseID, int iCurRow, int iDSRowSize, DataSet dsQuery)
		{
			SessionDS sessionDS = null;
			DataRow dr = dsQuery.Tables[0].Rows[0];
			string strCostCode = (string)dr[0];
			string strCostDescp=(string)dr[1];
			string strCostType=(string)dr[2];
			
			string strSQLWhere = "";
			if (strCostCode!=null && strCostCode!="")
			{
				strSQLWhere += "and Cost_code like '%"+strCostCode+"%' ";
			}
			if (strCostDescp!=null && strCostDescp!="")
			{
				strSQLWhere += "and cost_code_description like N'%"+Utility.ReplaceSingleQuote(strCostDescp)+"%' ";
			}
			if (strCostType!=null && strCostType!="")
			{
				strSQLWhere += "and Cost_type like N'%"+Utility.ReplaceSingleQuote(strCostType)+"%' ";
			}
			

			//DataSet dsStateCode = null;
			
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("SysDataManager","GetCostCodeDS","EDB111","DbConnection object is null!!");
				return sessionDS;
			}
			
			String strSQLQuery = "select cost_code,cost_code_description,cost_type from cost_code where ";
			strSQLQuery += "applicationid = '"+strAppID+"' and enterpriseid = '"+strEnterpriseID+"'"; 
			if (strSQLWhere!=null && strSQLWhere!="")
			{
				strSQLQuery += strSQLWhere;
			}
			
			sessionDS = dbCon.ExecuteQuery(strSQLQuery,iCurRow,iDSRowSize,"CostCodeTable");
			return  sessionDS;
		}

		public static int DeleteCostCode(DataSet dsCostCode, int rowIndex, String strAppID, String strEnterpriseID)
		{
			int iRowsDeleted = 0;
			IDbCommand dbCom = null;
			DataRow dr = dsCostCode.Tables[0].Rows[rowIndex];
			string strCostCode=null;
			string strCostDescp=null;
			string strCostType=null;

			if(dr[0] != null)
				if(!dr[0].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					strCostCode = (string)dr[0];
				}
			if(!dr[1].GetType().Equals(System.Type.GetType("System.DBNull")))
			{
				strCostDescp = (string)dr[1];
			}
			if(!dr[2].GetType().Equals(System.Type.GetType("System.DBNull")))
			{
				strCostType = (string)dr[2];
			}
			

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("SysDataMgr","DeleteCostCode","DPC001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			
			try
			{
				if(strCostCode!=null && strCostCode!="")
				{
					string strSQLQuery = "delete from cost_code where ";
					strSQLQuery += "applicationid = "+"'"+strAppID+"' and enterpriseid = '"+strEnterpriseID;
					strSQLQuery += "' and cost_code = '"+strCostCode+"'"; 
	
					dbCom = dbCon.CreateCommand(strSQLQuery,CommandType.Text);
					iRowsDeleted = dbCon.ExecuteNonQuery(dbCom);
				}
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("SysDataMgr","DeleteCostCode","DPC002",appException.Message.ToString());
			
				throw new ApplicationException("Delete operation failed"+appException.Message,appException);
			}
			return iRowsDeleted;
		}

		public static int InsertCostCode(DataSet dsCostCode, int rowIndex, String strAppID, String strEnterpriseID)
		{
			int iRowsInserted = 0;
			IDbCommand dbCom = null;
			DataRow dr = dsCostCode.Tables[0].Rows[rowIndex];
			string strCostCode = (string)dr[0];
			string strCostDescp=(string)dr[1];
			string strCostType=(string)dr[2];
			
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("SysDataMgrDAL","InsertCostCode","DP001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			try
			{
				if(strCostCode!=null && strCostCode!="")
				{
					string strSQLQuery = " insert into Cost_Code (applicationid, enterpriseid, cost_code,cost_code_description,cost_type)";
					strSQLQuery += " values ('"+strAppID+"', '"+strEnterpriseID+"', '"+strCostCode+"', N'";
					strSQLQuery += Utility.ReplaceSingleQuote(strCostDescp)+ "', N'"+Utility.ReplaceSingleQuote(strCostType)+"')";
					dbCom = dbCon.CreateCommand(strSQLQuery,CommandType.Text);
					iRowsInserted = dbCon.ExecuteNonQuery(dbCom);
				}
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("SysDataMgrDAL","InsertCostCode","DP002",appException.Message.ToString());
				throw new ApplicationException("Insert operation failed"+appException.Message,appException);
			}
			return iRowsInserted;
		}
		public static int UpdateCostCode(DataSet dsCostCode, String strAppID, String strEnterpriseID)
		{
			int iRowsUpdated = 0;
			String strSQLQuery=null;
			IDbCommand dbCom = null;
			DataRow dr = dsCostCode.Tables[0].Rows[0];
			string strCostCode = (string)dr[0];
			string strCostDescp=(string)dr[1];
			string strCostType=(string)dr[2];
			

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{

				Logger.LogTraceError("SysDataMgrDAL","UpdateCostCode","DP004","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			try
			{
				if(strCostCode!=null && strCostCode!="")
				{
					strSQLQuery = " update cost_code set cost_code_description = N'"+Utility.ReplaceSingleQuote(strCostDescp)+"',";
					strSQLQuery += " cost_type  = N'"+Utility.ReplaceSingleQuote(strCostType)+"'";
					strSQLQuery += " where applicationid = '"+strAppID+"' and enterpriseid = '"+strEnterpriseID;
					strSQLQuery += "' and cost_code = '"+strCostCode+"'"; 
	
					dbCom = dbCon.CreateCommand(strSQLQuery,CommandType.Text);
					iRowsUpdated = dbCon.ExecuteNonQuery(dbCom);
				}
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("SysDataMgrDAL","UpdateCostCode","DP005",appException.Message.ToString());
				throw new ApplicationException("Update operation failed",appException);
				
			}
			return iRowsUpdated;
		}

		// Conveyance Methods

		public static SessionDS GetEmptyConveyance()
		{
			DataTable dtConveyance =  new DataTable();
			
			dtConveyance.Columns.Add(new DataColumn("conveyance_code",typeof(string)));
			dtConveyance.Columns.Add(new DataColumn("conveyance_description",typeof(string)));
			dtConveyance.Columns.Add(new DataColumn("Length",typeof(decimal))); 
			dtConveyance.Columns.Add(new DataColumn("Breadth",typeof(decimal))); 
			dtConveyance.Columns.Add(new DataColumn("Height",typeof(decimal))); 
			dtConveyance.Columns.Add(new DataColumn("Max_Wt",typeof(decimal))); 

			DataSet  dsConv = new DataSet();
			dsConv.Tables.Add(dtConveyance); 
 
			//			dsConv.Tables[0].Columns["conveyance_code"].Unique = true; //Checks duplicate records..
			dsConv.Tables[0].Columns["conveyance_code"].AllowDBNull = false;

			SessionDS sessionds = new SessionDS();
			sessionds.ds = dsConv;
				
			sessionds.QueryResultMaxSize = sessionds.DataSetRecSize;
			return  sessionds;
		}

		public static void AddNewRowInConveyanceDS(ref SessionDS dsConveyance)
		{
			DataRow drNew = dsConveyance.ds.Tables[0].NewRow();
			drNew[0] = "";
			drNew[1] = "";
			drNew[2] = 0;
			drNew[3] = 0;
			drNew[4] = 0;
			drNew[5] = 0;

			try
			{
				dsConveyance.ds.Tables[0].Rows.Add(drNew);
			}
			catch(Exception ex)
			{
				Logger.LogTraceError("SysDataMgrDAL","AddNewRowInConveyanceDS","R005",ex.Message.ToString());
			}			
		}

		public static SessionDS GetConveyanceDS(String strAppID, String strEnterpriseID, int iCurRow, int iDSRowSize, DataSet dsQuery)
		{
			SessionDS sessionDS = null;
			DataRow dr = dsQuery.Tables[0].Rows[0];
			string strConveyance = (string)dr[0];
			string strConvDescrp =(string)dr[1];
			decimal dLength=Convert.ToDecimal(dr[2]);
			decimal dBreadth=Convert.ToDecimal(dr[3]);
			decimal dHeight=Convert.ToDecimal(dr[4]);
			decimal dWeight=Convert.ToDecimal(dr[5]);
			
			StringBuilder strSQLQuery = new StringBuilder();
			strSQLQuery.Append(" Select conveyance_code, conveyance_description, Length, Breadth, Height, Max_Wt from Conveyance where ");
			
			if (Utility.IsNotDBNull(strConveyance) && strConveyance !="")
				strSQLQuery.Append(" conveyance_code like '%"+strConveyance+"%' and ");
			if (Utility.IsNotDBNull(strConvDescrp) && strConvDescrp !="")
				strSQLQuery.Append(" conveyance_description like N'%"+Utility.ReplaceSingleQuote(strConvDescrp)+"%' and ");
			if (dLength.ToString() !="" && dLength > 0)
				strSQLQuery.Append(" Length="+ dLength+" and ");
			if (dBreadth.ToString() !="" && dBreadth > 0)
				strSQLQuery.Append(" Breadth="+ dBreadth+" and ");
			if (dHeight.ToString() !="" && dHeight > 0)
				strSQLQuery.Append(" Height="+ dHeight+" and ");
			if (dWeight.ToString() !="" && dWeight > 0 )
				strSQLQuery.Append(" Max_Wt="+ dWeight+" and ");
			
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("SysDataManager","GetCostCodeDS","EDB111","DbConnection object is null!!");
				return sessionDS;
			}
						
			strSQLQuery.Append(" applicationid = '"+strAppID+"' and enterpriseid = '"+strEnterpriseID+"'"); 						
			sessionDS = dbCon.ExecuteQuery(strSQLQuery.ToString(),iCurRow,iDSRowSize,"CostCodeTable");
			return  sessionDS;
		}

		public static int InsertConveyance(DataSet dsConveyance, int rowIndex, String strAppID, String strEnterpriseID)
		{
			int iRowsInserted = 0;
			IDbCommand dbCom = null;
			DataRow dr = dsConveyance.Tables[0].Rows[rowIndex];
			string strConveyance = (string)dr[0];
			string strDescription=(string)dr[1];			
			decimal dLength=(decimal)dr[2];
			decimal dBreadth=(decimal)dr[3];
			decimal dHeight=(decimal)dr[4];
			decimal dMax_Wt=(decimal)dr[5];
			
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("SysDataMgrDAL","InsertConveyance","DP001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			StringBuilder strSQLQuery=new StringBuilder();

			try
			{
				if(Utility.IsNotDBNull(strConveyance) && strConveyance!="")
				{
					strSQLQuery.Append(" Insert into Conveyance (applicationid, enterpriseid, Conveyance_code,Conveyance_description,");
					strSQLQuery.Append(" Length, Breadth, Height, Max_Wt) values ('"+strAppID+"', '"+strEnterpriseID+"', '"+strConveyance+"'");
					strSQLQuery.Append(",N'"+Utility.ReplaceSingleQuote(strDescription)+"',"+dLength+","+dBreadth+","+dHeight+","+dMax_Wt+")");
					
					dbCom = dbCon.CreateCommand(strSQLQuery.ToString(),CommandType.Text);
					iRowsInserted = dbCon.ExecuteNonQuery(dbCom);
				}
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("SysDataMgrDAL","InsertConveyance","DP002",appException.Message.ToString());
				throw new ApplicationException("Insert operation failed"+appException.Message,appException);
			}
			return iRowsInserted;
		}

		public static int UpdateConveyance(DataSet dsCostCode, String strAppID, String strEnterpriseID)
		{
			int iRowsUpdated = 0;
			
			IDbCommand dbCom = null;
			DataRow dr = dsCostCode.Tables[0].Rows[0];

			string strConveyance = (string)dr[0];
			string strDescription=(string)dr[1];			
			decimal dLength=(decimal)dr[2];
			decimal dBreadth=(decimal)dr[3];
			decimal dHeight=(decimal)dr[4];
			decimal dMax_Wt=(decimal)dr[5];			

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("SysDataMgrDAL","UpdateConveyance","DP004","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			StringBuilder strSQLQuery=new StringBuilder();

			try
			{
				if(Utility.IsNotDBNull(strConveyance) && strConveyance!="")
				{
					strSQLQuery.Append(" update Conveyance set Conveyance_Description = N'"+Utility.ReplaceSingleQuote(strDescription)+"',");
					strSQLQuery.Append(" Length= "+dLength+",Breadth="+dBreadth+",Height="+dHeight+",Max_Wt="+dMax_Wt);
					strSQLQuery.Append(" where applicationid = '"+strAppID+"' and enterpriseid = '"+strEnterpriseID);
					strSQLQuery.Append("' and Conveyance_code = '"+strConveyance+"'"); 
	
					dbCom = dbCon.CreateCommand(strSQLQuery.ToString(),CommandType.Text);
					iRowsUpdated = dbCon.ExecuteNonQuery(dbCom);
				}
			}

			catch(ApplicationException appException)
			{
				Logger.LogTraceError("SysDataMgrDAL","UpdateCostCode","DP005",appException.Message.ToString());
				throw new ApplicationException("Update operation failed",appException);
				
			}
			return iRowsUpdated;
		}

		public static int DeleteConveyance(String strAppID, String strEnterpriseID,String strConveyance)
		{
			int iRowsDeleted = 0;

			DbConnection dbConApp = null;
			IDbCommand dbCommandApp = null;

			IDbTransaction transactionApp = null;
			IDbConnection conApp = null;

			dbConApp = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbConApp == null)
			{
				Logger.LogTraceError("SysDataMgrDAL","DeleteConveyance","SDM002","dbConApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			conApp = dbConApp.GetConnection();
			
			if(conApp == null)
			{
				Logger.LogTraceError("SysDataMgrDAL","DeleteConveyance","SDM002","conApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			try
			{
				dbConApp.OpenConnection(ref conApp);
			}
			catch(ApplicationException appException)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("SysDataMgrDAL","DeleteConveyance","SDM003","Error opening connection : "+ appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ",appException);
			}
			catch(Exception exception)
			{
				if(conApp != null)
				{
					conApp.Close();
				}

				Logger.LogTraceError("SysDataMgrDAL","DeleteConveyance","SDM003","Error opening connection : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}

			transactionApp = dbConApp.BeginTransaction(conApp,IsolationLevel.ReadCommitted);
			if(transactionApp == null)
			{
				Logger.LogTraceError("SysDataMgrDAL","DeleteConveyance","SDM002","transactionApp is null");
				throw new ApplicationException("Failed Begining a App Transaction..",null);
			}

			try
			{
				//Update record into core_enterprise Table

				StringBuilder strBuilder = new StringBuilder();
			
				strBuilder.Append("Delete from Conveyance where applicationid = '"+ strAppID);
				strBuilder.Append("' and enterpriseid = '"+strEnterpriseID+"' and Conveyance_Code = '");
				strBuilder.Append(strConveyance+"'");

				String strSQLQuery = strBuilder.ToString();
				dbCommandApp = dbConApp.CreateCommand(strSQLQuery,CommandType.Text); //Creating the command first time
				dbCommandApp.Connection = conApp;
				dbCommandApp.Transaction = transactionApp;

				iRowsDeleted = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);
				Logger.LogTraceInfo("SysDataMgrDAL","DeleteConveyance","INF001",iRowsDeleted + " rows deleted from Exception_Code table");

				strBuilder = new StringBuilder();
			
				strBuilder.Append("Delete from Base_Zone_Conveyance_Rates where applicationid = '");
				strBuilder.Append(strAppID+"' and enterpriseid = '"+strEnterpriseID);
				strBuilder.Append("' and Conveyance_code = '"+strConveyance+"'");

				strSQLQuery = strBuilder.ToString();

				dbCommandApp.CommandText = strSQLQuery;

				iRowsDeleted = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);
				Logger.LogTraceInfo("SysDataMgrDAL","DeleteConveyance","INF001",iRowsDeleted + " rows deleted from Status_Code table");
			
				//Commit application and core transaction.

				transactionApp.Commit();
				Logger.LogTraceInfo("SysDataMgrDAL","DeleteConveyance","SDM003","App db delete transaction committed.");

			}
			catch(ApplicationException appException)
			{				
				try
				{
					transactionApp.Rollback();
					Logger.LogTraceInfo("SysDataMgrDAL","DeleteConveyance","SDM002","App db delete transaction rolled back.");
				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("SysDataMgrDAL","DeleteConveyance","SDM002","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}

				Logger.LogTraceError("SysDataMgrDAL","DeleteConveyance","SDM002","Delete Transaction Failed : "+ appException.Message.ToString());
				throw new ApplicationException("Error deleting Status Code "+appException.Message,appException);
			}
			catch(Exception exception)
			{
				try
				{
					transactionApp.Rollback();
					Logger.LogTraceInfo("SysDataMgrDAL","DeleteConveyance","SDM002","App db delete transaction rolled back.");
				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("SysDataMgrDAL","DeleteConveyance","SDM002","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}

				Logger.LogTraceError("SysDataMgrDAL","DeleteConveyance","SDM004","delete Transaction Failed : "+ exception.Message.ToString());
				throw new ApplicationException("Error deleting Status Code "+exception.Message,exception);
			}
			finally
			{
				if(conApp != null)
					conApp.Close();
			}
			
			return iRowsDeleted;
		}

		//Base Rates By Conveyance
		public static SessionDS GetEmptyBaseConvey()
		{
			DataTable dtConveyance =  new DataTable();
			
			dtConveyance.Columns.Add(new DataColumn("Origin_Zone_Code",typeof(string)));
			dtConveyance.Columns.Add(new DataColumn("Destination_Zone_Code",typeof(string)));
			dtConveyance.Columns.Add(new DataColumn("Conveyance_Code",typeof(string))); 
			dtConveyance.Columns.Add(new DataColumn("Start_Price",typeof(decimal))); 
			dtConveyance.Columns.Add(new DataColumn("Increment_Price",typeof(decimal))); 
			
			DataSet  dsConv = new DataSet();
			dsConv.Tables.Add(dtConveyance); 
 
			dsConv.Tables[0].Columns["conveyance_code"].AllowDBNull = false; //Checks duplicate records..
			dsConv.Tables[0].Columns["Origin_Zone_Code"].AllowDBNull = false;
			dsConv.Tables[0].Columns["Destination_Zone_Code"].AllowDBNull = false;

			SessionDS sessionds = new SessionDS();
			sessionds.ds = dsConv;
				
			sessionds.QueryResultMaxSize = sessionds.DataSetRecSize;
			return  sessionds;
		}

		public static void AddRowInBaseConveyDS(ref SessionDS dsConveyance)
		{
			DataRow drNew = dsConveyance.ds.Tables[0].NewRow();
			drNew[0] = "";
			drNew[1] = "";
			drNew[2] = "";
			drNew[3] = 0;
			drNew[4] = 0;
			
			try
			{
				dsConveyance.ds.Tables[0].Rows.Add(drNew);
			}
			catch(Exception ex)
			{
				Logger.LogTraceError("SysDataMgrDAL","AddNewRowInConveyanceDS","R005",ex.Message.ToString());
			}			
		}

		public static SessionDS GetBaseConveyDS(String strAppID, String strEnterpriseID, int iCurRow, int iDSRowSize, DataSet dsQuery)
		{
			SessionDS sessionDS = null;
			DataRow dr = dsQuery.Tables[0].Rows[0];
			string strOZCode = (string)dr[0];
			string strDZCode =(string)dr[1];
			string strConveyance=(string)dr[2];
			decimal dStartPrice=Convert.ToDecimal(dr[3]);
			decimal dIncrementPrice=Convert.ToDecimal(dr[4]);
						
			StringBuilder strSQLQuery = new StringBuilder();
			strSQLQuery.Append(" Select Origin_Zone_Code, Destination_Zone_Code, conveyance_code, ");
			strSQLQuery.Append(" Start_Price, Increment_Price from Base_Zone_Conveyance_Rates where ");
			
			if (Utility.IsNotDBNull(strOZCode) && strOZCode !="")
				strSQLQuery.Append(" Origin_Zone_Code like '%"+strOZCode+"%' and ");
			if (Utility.IsNotDBNull(strDZCode) && strDZCode !="")
				strSQLQuery.Append(" Destination_Zone_Code like '%"+strDZCode+"%' and ");
			if (Utility.IsNotDBNull(strConveyance) && strConveyance !="")
				strSQLQuery.Append(" conveyance_code like '%"+strConveyance+"%' and ");
			if (Utility.IsNotDBNull(dStartPrice) && dStartPrice!=0)
				strSQLQuery.Append(" Start_Price ="+dStartPrice+" and ");
			if (Utility.IsNotDBNull(dIncrementPrice) && dIncrementPrice!=0)
				strSQLQuery.Append(" Increment_Price ="+dIncrementPrice+" and ");
			
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("SysDataManager","GetBaseConveyDS","EDB111","DbConnection object is null!!");
				return sessionDS;
			}
						
			strSQLQuery.Append(" applicationid = '"+strAppID+"' and enterpriseid = '"+strEnterpriseID+"'"); 						
			sessionDS = dbCon.ExecuteQuery(strSQLQuery.ToString(),iCurRow,iDSRowSize,"CostCodeTable");
			return  sessionDS;
		}

		public static int InsertBaseConvey(DataSet dsConveyance, int rowIndex, String strAppID, String strEnterpriseID)
		{
			int iRowsInserted = 0;
			IDbCommand dbCom = null;
			DataRow dr = dsConveyance.Tables[0].Rows[rowIndex];
			
			string strOZ_Code=(string)dr[0];			
			string strDZ_Code=(string)dr[1];
			string strConveyance = (string)dr[2];
			decimal dStartPrice=(decimal)dr[3];
			decimal dIncrementPrice=(decimal)dr[4];
						
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("SysDataMgrDAL","InsertBaseConvey","DP001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			StringBuilder strSQLQuery=new StringBuilder();

			try
			{
				if(Utility.IsNotDBNull(strConveyance) && strConveyance!="")
				{
					strSQLQuery.Append(" Insert into Base_Zone_Conveyance_Rates (applicationid, enterpriseid, Conveyance_code, Origin_Zone_Code,");
					strSQLQuery.Append(" Destination_Zone_Code, Start_Price,Increment_Price) values ('"+strAppID+"', '"+strEnterpriseID+"', '"+strConveyance+"'");
					strSQLQuery.Append(",'"+strOZ_Code+"','"+strDZ_Code+"',"+dStartPrice+","+dIncrementPrice+")");
					
					dbCom = dbCon.CreateCommand(strSQLQuery.ToString(),CommandType.Text);
					iRowsInserted = dbCon.ExecuteNonQuery(dbCom);
				}
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("SysDataMgrDAL","InsertBaseConvey","DP002",appException.Message.ToString());
				throw new ApplicationException("Insert operation failed"+appException.Message,appException);
			}
			return iRowsInserted;
		}
		//
		public static int UpdateBaseConvey(DataSet dsCostCode, String strAppID, String strEnterpriseID)
		{
			int iRowsUpdated = 0;
			
			IDbCommand dbCom = null;
			DataRow dr = dsCostCode.Tables[0].Rows[0];

			string strOZ_Code=(string)dr[0];			
			string strDZ_Code=(string)dr[1];
			string strConveyance = (string)dr[2];
			decimal dStart_Price=(decimal)dr[3];
			decimal dIncrement_Price=(decimal)dr[3];

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("SysDataMgrDAL","UpdateBaseConvey","DP004","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			StringBuilder strSQLQuery=new StringBuilder();

			try
			{
				if(Utility.IsNotDBNull(strConveyance) && strConveyance!="")
				{
					strSQLQuery.Append(" update Base_Zone_Conveyance_Rates set  Start_Price= "+dStart_Price+",Increment_Price="+dIncrement_Price);					
					strSQLQuery.Append(" where applicationid = '"+strAppID+"' and enterpriseid = '"+strEnterpriseID);
					strSQLQuery.Append("' and Conveyance_code = '"+strConveyance+"' and Origin_Zone_Code='"+strOZ_Code+"' and "); 
					strSQLQuery.Append(" Destination_Zone_Code='"+strDZ_Code+"'");
	
					dbCom = dbCon.CreateCommand(strSQLQuery.ToString(),CommandType.Text);
					iRowsUpdated = dbCon.ExecuteNonQuery(dbCom);
				}
			}

			catch(ApplicationException appException)
			{
				Logger.LogTraceError("SysDataMgrDAL","UpdateBaseConvey","DP005",appException.Message.ToString());
				throw new ApplicationException("Update operation failed",appException);
				
			}
			return iRowsUpdated;
		}
		//
		public static int DeleteBaseConvey(String strAppID, String strEnterpriseID,String strOZCode, String strDZCode, String strConveyance)
		{
			int iRowsDeleted = 0;
		
			IDbCommand dbCom  = null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("SysDataMgr","DeleteCostCode","DPC001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			
			try
			{			

				StringBuilder strBuilder = new StringBuilder();
			
				strBuilder.Append("Delete from Base_Zone_Conveyance_Rates where applicationid = '");
				strBuilder.Append(strAppID+"' and enterpriseid = '"+strEnterpriseID);
				strBuilder.Append("' and Conveyance_code = '"+strConveyance+"' and Origin_Zone_Code='"+strOZCode);
				strBuilder.Append("' and Destination_Zone_Code='"+strDZCode+"'");

				String strSQLQuery = strBuilder.ToString();

				dbCom = dbCon.CreateCommand(strSQLQuery,CommandType.Text);
				iRowsDeleted = dbCon.ExecuteNonQuery(dbCom);				

			}

			catch(ApplicationException appException)
			{
				Logger.LogTraceError("SysDataMgr","DeleteCostCode","DPC002",appException.Message.ToString());
			
				throw new ApplicationException("Delete operation failed"+appException.Message,appException);
			}
			return iRowsDeleted;

		}



		/* Agent Status and Exception Cost Module Methods
		* 
		* */

		public static void AddRowAgentStatusDS(SessionDS sessionDS)
		{
			DataRow drNew = sessionDS.ds.Tables[0].NewRow();

			drNew[0] = "";
			drNew[1] = "";			
			drNew[2] = 0;
			
			try
			{
				sessionDS.ds.Tables[0].Rows.Add(drNew);
			}
			catch(Exception ex)
			{
				Logger.LogTraceError("SysDataMgrDAL","AddRowAgentStatusDS","R005",ex.Message.ToString());
			}			
			sessionDS.QueryResultMaxSize++;
		}

		public static SessionDS GetEmptyAgentStatusDS(int iNumRows)
		{
			
			DataTable dtStatusCode = new DataTable();
 
			dtStatusCode.Columns.Add(new DataColumn("status_code", typeof(string)));
			dtStatusCode.Columns.Add(new DataColumn("status_description", typeof(string)));
			dtStatusCode.Columns.Add(new DataColumn("surcharge", typeof(decimal)));

			for(int i=0; i < iNumRows; i++)
			{
				DataRow drEach = dtStatusCode.NewRow();
				drEach[0] = "";
				drEach[1] = "";
				drEach[2] = 0;				
				
				dtStatusCode.Rows.Add(drEach);
			}

			DataSet dsStatusCode = new DataSet();
			dsStatusCode.Tables.Add(dtStatusCode);

			//			dsStatusCode.Tables[0].Columns["status_code"].Unique = true; //Checks duplicate records..
			dsStatusCode.Tables[0].Columns["status_code"].AllowDBNull = false;

			SessionDS sessionDS = new SessionDS();
			sessionDS.ds = dsStatusCode;
			sessionDS.DataSetRecSize = dsStatusCode.Tables[0].Rows.Count;
			sessionDS.QueryResultMaxSize = sessionDS.DataSetRecSize;

			return  sessionDS;
	
		}


		public static SessionDS GetStatusCodeDS(String strAppID, String strEnterpriseID, String strAgentID, int iCurrent, int iDSRecSize,DataSet dsQueryParam)
		{
			SessionDS sessionDS = null;

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("SysDataMgrDAL","GetStatusCodeDS","EDB101","DbConnection object is null!!");
				return sessionDS;
			}

			StringBuilder strBuilder = new StringBuilder();
			strBuilder.Append(" select AC.status_code, SC.status_description, AC.surcharge from Agent_Status_Cost AC ");
			strBuilder.Append(" INNER JOIN Status_Code SC ON AC.Status_Code=SC.Status_Code where AC.applicationid = '");
			strBuilder.Append(strAppID+"' and AC.enterpriseid = '"+strEnterpriseID+"'");
			
			if (strAgentID !="" )
				strBuilder.Append(" and AC.AgentId='"+strAgentID+"'");

			DataRow drEach = dsQueryParam.Tables[0].Rows[0];				
			String strStatusCode = (String) drEach["status_code"];
			String strStatusDescription = (String) drEach["status_description"];
			decimal dSurcharge = Convert.ToDecimal(drEach["Surcharge"]);

			if((strStatusCode != null) && (strStatusCode != ""))
				strBuilder.Append(" and AC.Status_code like '%"+strStatusCode+"%' ");			
			if((strStatusDescription != null) && (strStatusDescription != ""))
				strBuilder.Append(" and SC.Status_description like N'%"+strStatusDescription+"%' ");			
			if(dSurcharge.ToString()!= "" && dSurcharge.ToString()!= "0")
				strBuilder.Append(" and AC.Surcharge = "+dSurcharge);

			String strSQLQuery = strBuilder.ToString();			
			sessionDS = dbCon.ExecuteQuery(strSQLQuery,iCurrent,iDSRecSize,"StatusCodeTable");
			return  sessionDS;
	
		}

		public static int AddStatusCodeDS(String strAppID, String strEnterpriseID,String strAgentId, DataSet dsToInsert)
		{
			int iRowsAffected = 0;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("SysDataMgrDAL","GetStatusCodeDS","EDB101","DbConnection object is null!!");
				return iRowsAffected;
			}

			StringBuilder strBuilder = new StringBuilder();
			strBuilder.Append("insert into Agent_Status_Cost (applicationid,enterpriseid,AgentId, status_code, surcharge) ");
			strBuilder.Append(" values ('"+strAppID+"','"+strEnterpriseID+"','"+strAgentId+"','");

			int iRowCnt = dsToInsert.Tables[0].Rows.Count;
			DataRow drEach = dsToInsert.Tables[0].Rows[0];
				
			String strStatusCode = (String) drEach["status_code"];			
			strBuilder.Append(strStatusCode+"',");
			
			decimal dsurcharge= Convert.ToDecimal(drEach["surcharge"]);
			strBuilder.Append(dsurcharge+")");

			String strSQLQuery = strBuilder.ToString();
			IDbCommand dbCmd = dbCon.CreateCommand(strSQLQuery,CommandType.Text);
			try
			{
				iRowsAffected = dbCon.ExecuteNonQuery(dbCmd);
				Logger.LogTraceInfo("SysDataMgrDAL","AddStatusCodeDS","SDM001",iRowsAffected+" rows inserted in to Agent_Status_Cost table");
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("RBACManager","AddStatusCodeDS","SDM001","Error During Insert "+ appException.Message.ToString());
				throw new ApplicationException("Error inserting Agent Status Code ",appException);
			}

			return iRowsAffected;

		}


		public static int ModifyStatusCodeDS(String strAppID, String strEnterpriseID,String strAgentID, DataSet dsToModify)
		{
			int iRowsAffected = 0;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("SysDataMgrDAL","GetStatusCodeDS","EDB101","DbConnection object is null!!");
				return iRowsAffected;
			}
			int iRowCnt = dsToModify.Tables[0].Rows.Count;
			DataRow drEach = dsToModify.Tables[0].Rows[0];
			String strStatusCode = (String) drEach["status_code"];
			decimal dsurcharge = Convert.ToDecimal(drEach["surcharge"]);

			StringBuilder strBuilder = new StringBuilder();
			
			strBuilder.Append("update Agent_Status_Cost set surcharge="+dsurcharge);
			strBuilder.Append(" where applicationid = '"+strAppID+"' and enterpriseid = '"+strEnterpriseID);
			strBuilder.Append("' and status_code = '"+strStatusCode+"' and AgentID='"+strAgentID+"'");

			String strSQLQuery = strBuilder.ToString();

			IDbCommand dbCmd = dbCon.CreateCommand(strSQLQuery,CommandType.Text);			

			try
			{
				iRowsAffected = dbCon.ExecuteNonQuery(dbCmd);
				Logger.LogTraceInfo("SysDataMgrDAL","AddStatusCodeDS","SDM001",iRowsAffected+" rows inserted in to Agent_Status_Cost table");
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("RBACManager","AddStatusCodeDS","SDM001","Error During Insert "+ appException.Message.ToString());
				throw new ApplicationException("Error inserting Agent_Status_Cost",appException);
			}
			return iRowsAffected;

		}

		public static int DeleteStatusCodeDS(String strAppID, String strEnterpriseID, String strAgentId, String strStatusCode)
		{
			int iRowsDeleted = 0;

			DbConnection dbConApp = null;
			IDbCommand dbCommandApp = null;

			IDbTransaction transactionApp = null;
			IDbConnection conApp = null;

			dbConApp = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbConApp == null)
			{
				Logger.LogTraceError("SysDataMgrDAL","DeleteStatusCodeDS","SDM002","dbConApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			conApp = dbConApp.GetConnection();
			
			if(conApp == null)
			{
				Logger.LogTraceError("SysDataMgrDAL","DeleteStatusCodeDS","SDM002","conApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			try
			{
				dbConApp.OpenConnection(ref conApp);
			}
			catch(ApplicationException appException)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("SysDataMgrDAL","DeleteStatusCodeDS","SDM003","Error opening connection : "+ appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ",appException);
			}
			catch(Exception exception)
			{
				if(conApp != null)
				{
					conApp.Close();
				}

				Logger.LogTraceError("SysDataMgrDAL","DeleteStatusCodeDS","SDM003","Error opening connection : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}

			transactionApp = dbConApp.BeginTransaction(conApp,IsolationLevel.ReadCommitted);
			if(transactionApp == null)
			{
				Logger.LogTraceError("SysDataMgrDAL","DeleteStatusCodeDS","SDM002","transactionApp is null");
				throw new ApplicationException("Failed Begining a App Transaction..",null);
			}

			try
			{
				//Update record into core_enterprise Table
				StringBuilder strBuilder = new StringBuilder();
			
				strBuilder.Append("Delete from Agent_Exception_Cost where applicationid = '"+strAppID);
				strBuilder.Append("' and enterpriseid = '"+strEnterpriseID+"' and status_code = '");
				strBuilder.Append(strStatusCode +"' and AgentId='"+strAgentId+"'");


				String strSQLQuery = strBuilder.ToString();
				dbCommandApp = dbConApp.CreateCommand(strSQLQuery,CommandType.Text); //Creating the command first time
				dbCommandApp.Connection = conApp;
				dbCommandApp.Transaction = transactionApp;

				iRowsDeleted = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);
				Logger.LogTraceInfo("RBACManager","DeleteStatusCodeDS","INF001",iRowsDeleted + " rows deleted from Exception_Code table");

				strBuilder = new StringBuilder();
			
				strBuilder.Append("Delete from Agent_Status_Cost where applicationid = '"+strAppID);
				strBuilder.Append("' and enterpriseid = '"+strEnterpriseID+"' and status_code = '");
				strBuilder.Append(strStatusCode+"' and agentid='"+strAgentId+"'");

				strSQLQuery = strBuilder.ToString();

				dbCommandApp.CommandText = strSQLQuery;

				iRowsDeleted = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);
				Logger.LogTraceInfo("RBACManager","DeleteStatusCodeDS","INF001",iRowsDeleted + " rows deleted from Status_Code table");
			
				//Commit application and core transaction.

				transactionApp.Commit();
				Logger.LogTraceInfo("SysDataMgrDAL","DeleteStatusCodeDS","SDM003","App db delete transaction committed.");

			}
			catch(ApplicationException appException)
			{				
				try
				{
					transactionApp.Rollback();
					Logger.LogTraceInfo("SysDataMgrDAL","DeleteStatusCodeDS","SDM002","App db delete transaction rolled back.");
				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("SysDataMgrDAL","DeleteStatusCodeDS","SDM002","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}

				Logger.LogTraceError("SysDataMgrDAL","DeleteStatusCodeDS","SDM002","Delete Transaction Failed : "+ appException.Message.ToString());
				throw new ApplicationException("Error deleting Status Code "+appException.Message,appException);
			}
			catch(Exception exception)
			{
				try
				{
					transactionApp.Rollback();
					Logger.LogTraceInfo("SysDataMgrDAL","DeleteStatusCodeDS","SDM002","App db delete transaction rolled back.");

				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("SysDataMgrDAL","DeleteStatusCodeDS","SDM002","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}

				Logger.LogTraceError("SysDataMgrDAL","DeleteStatusCodeDS","SDM004","delete Transaction Failed : "+ exception.Message.ToString());
				throw new ApplicationException("Error deleting Status Code "+exception.Message,exception);
			}
			finally
			{
				if(conApp != null)
				{
					conApp.Close();
				}
			}
			
			return iRowsDeleted;
		}

		/* Agent Exception Cost methods */

		public static void AddRowAgentExcepDS(SessionDS sessionDS)
		{
			DataRow drNew = sessionDS.ds.Tables[0].NewRow();

			drNew[0] = "";
			drNew[1] = "";
			drNew[2] = "";
			drNew[3] = 0;
			
			try
			{
				sessionDS.ds.Tables[0].Rows.Add(drNew);
			}
			catch(Exception ex)
			{
				Logger.LogTraceError("SysDataMgrDAL","AddRowAgentExcepDS","R005",ex.Message.ToString());
			}			
			sessionDS.QueryResultMaxSize++;
		}

		public static SessionDS GetEmptyAgentExcepDS(int iNumRows)
		{
			DataTable dtExceptionCode = new DataTable();
 
			dtExceptionCode.Columns.Add(new DataColumn("exception_code", typeof(string)));
			dtExceptionCode.Columns.Add(new DataColumn("exception_Description", typeof(string)));
			dtExceptionCode.Columns.Add(new DataColumn("mbg", typeof(string)));
			dtExceptionCode.Columns.Add(new DataColumn("surcharge", typeof(decimal)));


			for(int i=0; i < iNumRows; i++)
			{
				DataRow drEach = dtExceptionCode.NewRow();
				drEach[0] = "";
				drEach[1] = "";
				drEach[2] = "";
				drEach[3] = 0;

				dtExceptionCode.Rows.Add(drEach);
			}

			DataSet dsExceptionCode = new DataSet();
			dsExceptionCode.Tables.Add(dtExceptionCode);

			dsExceptionCode.Tables[0].Columns["exception_code"].Unique = true; //Checks duplicate records..
			dsExceptionCode.Tables[0].Columns["exception_code"].AllowDBNull = false;

			SessionDS sessionDS = new SessionDS();
			sessionDS.ds = dsExceptionCode;
			sessionDS.DataSetRecSize = dsExceptionCode.Tables[0].Rows.Count;
			sessionDS.QueryResultMaxSize = sessionDS.DataSetRecSize;

			return  sessionDS;	
		}

		public static SessionDS GetExceptionCodeDS(String strAppID, String strEnterpriseID, String strAgentID, String strStatusCode,int iCurrent, int iDSRecSize)
		{
			SessionDS sessionDS = null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("SysDataMgrDAL","GetStatusCodeDS","EDB101","DbConnection object is null!!");
				return sessionDS;
			}

			StringBuilder strBuilder = new StringBuilder();
			strBuilder.Append(" select AEC.exception_code, EC.exception_Description, AEC.MBG, AEC.surcharge from Agent_Exception_Cost AEC ");
			strBuilder.Append(" INNER JOIN Exception_Code EC ON AEC.Exception_Code=EC.Exception_Code where ");
			strBuilder.Append(" AEC.applicationid = '"+strAppID+"' and AEC.enterpriseid = '"+strEnterpriseID+"' ");
			strBuilder.Append(" and AEC.AgentId='"+strAgentID+"' and AEC.Status_Code = '"+strStatusCode +"'"); 
			sessionDS = dbCon.ExecuteQuery(strBuilder.ToString(),iCurrent,iDSRecSize,"AgentExceptionCost");
			return  sessionDS;	
		}

		public static int AddExceptionCodeDS(String strAppID, String strEnterpriseID,String strAgentId, String strStatusCode,DataSet dsToInsert)
		{
			int iRowsAffected = 0;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("SysDataMgrDAL","AddExceptionCodeDS","EDB101","DbConnection object is null!!");
				return iRowsAffected;
			}

			StringBuilder strBuilder = new StringBuilder();
			strBuilder.Append(" insert into Agent_Exception_Cost (applicationid,enterpriseid,AgentId, status_code, exception_code,");
			strBuilder.Append(" MBG, surcharge) values ('"+strAppID+"','"+strEnterpriseID+"','"+strAgentId+"','"+strStatusCode+"','");

			int iRowCnt = dsToInsert.Tables[0].Rows.Count;
			DataRow drEach = dsToInsert.Tables[0].Rows[0];
				
			String strExceptionCode = (String) drEach["exception_code"];		
			strBuilder.Append(strExceptionCode+"'");
			
			String strMBG = (String) drEach["MBG"];		
			if (Utility.IsNotDBNull(strMBG) && strMBG!="")
				strBuilder.Append(",'"+strMBG+"'");
			else
			{
				strBuilder.Append(",null");
			}
			
			decimal dSurcharge = Convert.ToDecimal(drEach["Surcharge"]);
			if(Utility.IsNotDBNull(dSurcharge))			
				strBuilder.Append(","+dSurcharge +")");
			else
			{
				strBuilder.Append(",null)");
			}

			String strSQLQuery = strBuilder.ToString();
			IDbCommand dbCmd = dbCon.CreateCommand(strSQLQuery,CommandType.Text);			

			try
			{
				iRowsAffected = dbCon.ExecuteNonQuery(dbCmd);
				Logger.LogTraceInfo("SysDataMgrDAL","AddExceptionCodeDS","SDM001",iRowsAffected+" rows inserted in to Agent_Exception_Cost table");
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("RBACManager","AddExceptionCodeDS","SDM001","Error During Insert "+ appException.Message.ToString());
				throw new ApplicationException("Error inserting Status Code ",appException);
			}
			return iRowsAffected;

		}

		public static int ModifyExceptionCodeDS(String strAppID, String strEnterpriseID,String strAgentId, String strStatusCode,DataSet dsToModify)
		{
			int iRowsAffected = 0;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("SysDataMgrDAL","ModifyExceptionCodeDS","EDB101","DbConnection object is null!!");
				return iRowsAffected;
			}
			int iRowCnt = dsToModify.Tables[0].Rows.Count;
			DataRow drEach = dsToModify.Tables[0].Rows[0];

			String strExceptionCode = (String) drEach["exception_code"];
			String strMBG = (String) drEach["MBG"];
			if (! Utility.IsNotDBNull(strMBG) && strMBG !="")			
			{
				strMBG="null";
			}
			decimal dSurcharge = Convert.ToDecimal(drEach["Surcharge"]);
			if (! Utility.IsNotDBNull(dSurcharge))
			{
				dSurcharge=Convert.ToDecimal(System.DBNull.Value);
			}

			StringBuilder strBuilder = new StringBuilder();			
			strBuilder.Append(" update Agent_Exception_Cost set MBG='"+strMBG+"', Surcharge = "+dSurcharge);
			strBuilder.Append(" where applicationid = '"+strAppID+"' and enterpriseid = '"+strEnterpriseID+"' and ");
			strBuilder.Append(" status_code = '"+strStatusCode+"' and exception_code = '"+strExceptionCode+"' and ");
			strBuilder.Append(" AgentId='"+strAgentId+"'");

			String strSQLQuery = strBuilder.ToString();
			IDbCommand dbCmd = dbCon.CreateCommand(strSQLQuery,CommandType.Text);
			
			try
			{
				iRowsAffected = dbCon.ExecuteNonQuery(dbCmd);
				Logger.LogTraceInfo("SysDataMgrDAL","ModifyExceptionCodeDS","SDM001",iRowsAffected+" rows inserted in to Agent_Exception_Cost table");
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("RBACManager","ModifyExceptionCodeDS","SDM001","Error During Insert "+ appException.Message.ToString());
				throw new ApplicationException("Error inserting Status Code ",appException);
			}

			return iRowsAffected;
		}

		public static int DeleteExceptionCodeDS(String strAppID, String strEnterpriseID,String strStatusCode,String strExceptionCode, String strAgentId)
		{
			int iRowsDeleted = 0;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("SysDataMgrDAL","DeleteExceptionCodeRecord","EDB101","DbConnection object is null!!");
				return iRowsDeleted;
			}

			StringBuilder strBuilder = new StringBuilder();
			
			strBuilder.Append("Delete from Agent_Exception_Cost where applicationid = '"+strAppID+"' and enterpriseid = '");
			strBuilder.Append(strEnterpriseID +"' and status_code = '"+strStatusCode);
			strBuilder.Append("' and exception_code = '"+strExceptionCode+"' and AgentId='"+strAgentId+"'");

			String strSQLQuery = strBuilder.ToString();
			IDbCommand dbCmd = dbCon.CreateCommand(strSQLQuery,CommandType.Text);
			
			try
			{
				iRowsDeleted = dbCon.ExecuteNonQuery(dbCmd);
				Logger.LogTraceInfo("SysDataMgrDAL","DeleteExceptionCodeRecord","SDM001",iRowsDeleted+" rows deleted from  Exception_Code table");
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("SysDataMgrDAL","DeleteExceptionCodeRecord","SDM001","Error During Delete "+ appException.Message.ToString());
				throw new ApplicationException("Error inserting Status Code : "+appException.Message,appException);
			}

			return iRowsDeleted;		
		}

		//Fixible Rate

		public static DataSet GetEmptyESASectors(int iRows)
		{
			DataTable dtCustZones =  new DataTable();
			dtCustZones.Columns.Add(new DataColumn("postal_code",typeof(string)));
			dtCustZones.Columns.Add(new DataColumn("ESA_Sector",typeof(string)));
			dtCustZones.Columns.Add(new DataColumn("effective_date",typeof(DateTime))); 
			if(iRows>0)
			{
				DataRow drEach = dtCustZones.NewRow();
				dtCustZones.Rows.Add(drEach);
			}
			DataSet dsCustZones = new DataSet();
			dsCustZones.Tables.Add(dtCustZones);
			return  dsCustZones;

		}

		public static DataSet GetEmptyCustZones(int iRows)
		{
			DataTable dtCustZones =  new DataTable();
			dtCustZones.Columns.Add(new DataColumn("zipcode",typeof(string)));
			dtCustZones.Columns.Add(new DataColumn("zone_code",typeof(string)));
			dtCustZones.Columns.Add(new DataColumn("effective_date",typeof(DateTime))); 

			//			dtCustZones.Columns.Add(new DataColumn("extended_area_surcharge_amt",typeof(decimal))); 
			//			dtCustZones.Columns.Add(new DataColumn("extended_area_surcharge_percent",typeof(decimal))); 
			if(iRows>0)
			{
				DataRow drEach = dtCustZones.NewRow();
				//			drEach["extended_area_surcharge_amt"] = System.DBNull.Value;
				//			drEach["extended_area_surcharge_percent"] = System.DBNull.Value;
				dtCustZones.Rows.Add(drEach);
			}
			DataSet dsCustZones = new DataSet();
			dsCustZones.Tables.Add(dtCustZones);
			return  dsCustZones;

		}

		public static bool IsESASectorExist(String strAppID, String strEnterpriseID, 
			String zipcode, String zone_code, String effective_date)
		{
			IDbCommand dbCmd = null;
			StringBuilder strBuild = null;
			bool isExists = false;

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("SysDataMgrDAL","IsCustFRExist","SDM001","DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null",null);
			}

			strBuild =  new StringBuilder();
			strBuild.Append("select * from Enterprise_ESA_Sector where applicationid = '");
			strBuild.Append(strAppID+"'");
			strBuild.Append(" and enterpriseid = '");
			strBuild.Append(strEnterpriseID+"'");
			strBuild.Append(" and postal_code = '");
			strBuild.Append(Utility.ReplaceSingleQuote(zipcode)+"'");
//			strBuild.Append(" and ESA_Sector = '");
//			strBuild.Append(Utility.ReplaceSingleQuote(zone_code)+"'");
			strBuild.Append(" and effective_date = Convert(DateTime,'");
			strBuild.Append(effective_date+"',103)");
			
			try
			{
				dbCmd = dbCon.CreateCommand(strBuild.ToString(), CommandType.Text);
				DataSet dsCustZones =(DataSet)dbCon.ExecuteQuery(dbCmd, com.common.DAL.ReturnType.DataSetType);
				if (dsCustZones.Tables[0].Rows.Count > 0)
				{
					isExists = true;
				}

			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("SysDataMgrDAL","IsCustFRExist","SDM002","Error : "+ appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ",appException);
			}
			catch(Exception exception)
			{
				Logger.LogTraceError("SysDataMgrDAL","IsCustFRExist","SDM003","Error : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}
			return isExists;
		}



		public static bool IsCustFRExist(String strAppID, String strEnterpriseID, 
			String custid, String zipcode, object effective_date)
		{
			IDbCommand dbCmd = null;
			StringBuilder strBuild = null;
			bool isExists = false;

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("SysDataMgrDAL","IsCustFRExist","SDM001","DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null",null);
			}
			string strDay = Convert.ToDateTime(effective_date).Day.ToString().Trim();
			string strMonth = Convert.ToDateTime(effective_date).Month.ToString().Trim();
			string strYear = Convert.ToDateTime(effective_date).Year.ToString().Trim();

			string getDate = "";
			if(strDay.Length == 1)
				getDate = "0"+strDay;
			else
				getDate = strDay;

			if(strMonth.Length == 1)
				getDate +=  "/0"+strMonth+"/"+strYear;
			else
				getDate += "/"+strMonth+"/"+strYear;
			
			//DateTime dt = DateTime.ParseExact(getDate,"dd/MM/yyyy",null);

			strBuild =  new StringBuilder();
			strBuild.Append("select * from Customer_Zones where applicationid = '");
			strBuild.Append(strAppID+"'");
			strBuild.Append(" and enterpriseid = '");
			strBuild.Append(strEnterpriseID+"'");
			strBuild.Append(" and custid = '");
			strBuild.Append(Utility.ReplaceSingleQuote(custid)+"'");
			strBuild.Append(" and zipcode = '");
			strBuild.Append(Utility.ReplaceSingleQuote(zipcode)+"'");
			strBuild.Append(" and effective_date = Convert(DateTime,'");
			strBuild.Append(getDate);
			strBuild.Append("',103)");
			
			try
			{
				dbCmd = dbCon.CreateCommand(strBuild.ToString(), CommandType.Text);
				DataSet dsCustZones =(DataSet)dbCon.ExecuteQuery(dbCmd, com.common.DAL.ReturnType.DataSetType);
				if (dsCustZones.Tables[0].Rows.Count > 0)
				{
					isExists = true;
				}

			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("SysDataMgrDAL","IsCustFRExist","SDM002","Error : "+ appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ",appException);
			}
			catch(Exception exception)
			{
				Logger.LogTraceError("SysDataMgrDAL","IsCustFRExist","SDM003","Error : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}
			return isExists;
		}

		public static int ModifyESASector(String strAppID, String strEnterpriseID, 
			String zipcode, String zone_code, String effective_date,
			String sameZipcode, String sameZone_code, String sameEffective_date)
		{
			int iRowsAffected = 0;
		
			//int iRowsDup = 0;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("SysDataMgrDAL","ModifyCustFR","EDB101","DbConnection object is null!!");
				return iRowsAffected;
			}

			StringBuilder strBuilder = new StringBuilder();
			strBuilder.Append("update Enterprise_ESA_Sector "); 
			strBuilder.Append(" SET postal_code = '"+zipcode+"'");
			strBuilder.Append(",ESA_Sector = '"+zone_code+"'");
			strBuilder.Append(",effective_date ='");
			if((effective_date != null) && 
				(!effective_date.GetType().Equals(System.Type.GetType("System.DBNull"))) &&
				(effective_date.ToString() != ""))
			{
				strBuilder.Append(DateTime.ParseExact(effective_date,"dd/MM/yyyy",null));
			}
			else
			{	
				strBuilder.Append(DateTime.ParseExact(effective_date,"dd/MM/yyyy",null));
			}
			strBuilder.Append("'");

			strBuilder.Append(" where applicationid = '");
			strBuilder.Append(strAppID);
			strBuilder.Append("' and enterpriseid = '");
			strBuilder.Append(strEnterpriseID);
			strBuilder.Append("' and postal_code = '");
			strBuilder.Append(sameZipcode);
			strBuilder.Append("' and esa_sector = '");
			strBuilder.Append(sameZone_code);
			strBuilder.Append("' and effective_date = '");
			strBuilder.Append(DateTime.ParseExact(sameEffective_date,"dd/MM/yyyy",null));
			strBuilder.Append("'");
			String strSQLQuery = strBuilder.ToString();

			IDbCommand dbCmd = dbCon.CreateCommand(strSQLQuery,CommandType.Text);
			
			try
			{
				iRowsAffected = dbCon.ExecuteNonQuery(dbCmd);
				Logger.LogTraceInfo("SysDataMgrDAL","ModifyCustFR","SDM001",iRowsAffected+" rows inserted in to Customer table");
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("SysDataMgrDAL","ModifyCustFR","SDM001","Error During Insert "+ appException.Message.ToString());
				throw new ApplicationException("Error modifying Customer Fexible Rate : "+appException.Message,appException);
			}
			return iRowsAffected;
		}

		public static bool CheckDupUpdateCustFR(String strAppID, String strEnterpriseID, 
			String custid, String zipcode, String zone_code, object effective_date,
			String sameZipcode, String sameZone_code, String sameEffective_date)
		{
			DataSet ds = new DataSet();
			bool ckDup;// = false;
		
			//int iRowsDup = 0;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("SysDataMgrDAL","CheckDupUpdateCustFR","SDM001","DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null",null);
			}
			//string strSqlDup = "select count(*) from customer_zones where applicationid = '"+strAppID+"' and enterpriseid = '"+strEnterpriseID+"' and custid = '"+custid+"' and zipcode = '"+zipcode+"' and zone_code = '"+zone_code+"' and effective_date = CONVERT(DateTime,'"+effective_date+"',103)";

			StringBuilder strBuilder = new StringBuilder();
			strBuilder.Append("select * from  Customer_Zones "); 
			strBuilder.Append(" where applicationid = '");
			strBuilder.Append(strAppID);
			strBuilder.Append("' and enterpriseid = '");
			strBuilder.Append(strEnterpriseID);
			strBuilder.Append("' and custid = '");
			strBuilder.Append(custid);
			strBuilder.Append("' and zipcode = '");
			strBuilder.Append(zipcode);
			strBuilder.Append("' and zone_code = '");
			strBuilder.Append(zone_code);
			strBuilder.Append("' and effective_date = Convert(DateTime,'");
			strBuilder.Append(Convert.ToDateTime(effective_date).ToString("dd/MM/yyyy"));
			strBuilder.Append("',103)");
			strBuilder.Append(" and zipcode <> '");
			strBuilder.Append(sameZipcode);
			strBuilder.Append("' and zone_code <> '");
			strBuilder.Append(sameZone_code);
			strBuilder.Append("' and effective_date <> Convert(DateTime,'");
			strBuilder.Append(sameEffective_date);
			strBuilder.Append("',103)");
			String strSQLQuery = strBuilder.ToString();

			IDbCommand dbCmd = dbCon.CreateCommand(strSQLQuery,CommandType.Text);
			

			try
			{
				ds = (DataSet)dbCon.ExecuteQuery(dbCmd,ReturnType.DataSetType);
				if (ds.Tables[0].Rows.Count > 0)
				{
					ckDup = true;
				}
				else
				{
					ckDup = false;
				}
				Logger.LogTraceInfo("SysDataMgrDAL","CheckDupUpdateCustFR","SDM001",ds.Tables[0].Rows.Count+" rows inserted in to Customer table");
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("SysDataMgrDAL","CheckDupUpdateCustFR","SDM002","Error : "+ appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ",appException);
			}
			catch(Exception exception)
			{
				Logger.LogTraceError("SysDataMgrDAL","CheckDupUpdateCustFR","SDM003","Error : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}
			return ckDup;
		}

		public static int ModifyCustFR(String strAppID, String strEnterpriseID, 
			String custid, String zipcode, String zone_code, object effective_date,
			String sameZipcode, String sameZone_code, String sameEffective_date)
		{
			int iRowsAffected = 0;
		
			//int iRowsDup = 0;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("SysDataMgrDAL","ModifyCustFR","EDB101","DbConnection object is null!!");
				return iRowsAffected;
			}
			//string strSqlDup = "select count(*) from customer_zones where applicationid = '"+strAppID+"' and enterpriseid = '"+strEnterpriseID+"' and custid = '"+custid+"' and zipcode = '"+zipcode+"' and zone_code = '"+zone_code+"' and effective_date = CONVERT(DateTime,'"+effective_date+"',103)";

			StringBuilder strBuilder = new StringBuilder();
			strBuilder.Append("update Customer_Zones "); 
			strBuilder.Append("SET zipcode = '"+zipcode+"'");
			strBuilder.Append(",zone_code = '"+zone_code+"'");
			strBuilder.Append(",effective_date = '");
			if((effective_date != null) && 
				(!effective_date.GetType().Equals(System.Type.GetType("System.DBNull"))) &&
				(effective_date.ToString() != ""))
			{
				strBuilder.Append(Convert.ToDateTime(effective_date));
			}
			else
			{	
				//strBuilder.Append("null");
				strBuilder.Append(Convert.ToDateTime(effective_date));
			}
			strBuilder.Append("'");
			//			strBuilder.Append(", ");
			//			strBuilder.Append(" extended_area_surcharge_percent = ");
			//			if((percentEAS != null) && 
			//				(!percentEAS.GetType().Equals(System.Type.GetType("System.DBNull"))) &&
			//				(percentEAS.ToString() != ""))
			//			{
			//				strBuilder.Append(Convert.ToDecimal(percentEAS));
			//			}
			//			else
			//			{	
			//				strBuilder.Append("null");
			//			}
			strBuilder.Append(" where applicationid = '");
			strBuilder.Append(strAppID);
			strBuilder.Append("' and enterpriseid = '");
			strBuilder.Append(strEnterpriseID);
			strBuilder.Append("' and custid = '");
			strBuilder.Append(custid);
			strBuilder.Append("' and zipcode = '");
			strBuilder.Append(sameZipcode);
			strBuilder.Append("' and zone_code = '");
			strBuilder.Append(sameZone_code);
			strBuilder.Append("' and effective_date = Convert(DateTime,'");
			strBuilder.Append(sameEffective_date);
			strBuilder.Append("',103)");
			String strSQLQuery = strBuilder.ToString();

			IDbCommand dbCmd = dbCon.CreateCommand(strSQLQuery,CommandType.Text);
			
			try
			{
				iRowsAffected = dbCon.ExecuteNonQuery(dbCmd);
				Logger.LogTraceInfo("SysDataMgrDAL","ModifyCustFR","SDM001",iRowsAffected+" rows inserted in to Customer table");
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("SysDataMgrDAL","ModifyCustFR","SDM001","Error During Insert "+ appException.Message.ToString());
				throw new ApplicationException("Error modifying Customer Fexible Rate : "+appException.Message,appException);
			}
			return iRowsAffected;
		}

		public static int InsertESASector(String strAppID, String strEnterpriseID, 
			String zipcode, String zone_code, string effective_date)
		{
			int iRowsAffected = 0;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("SysDataMgrDAL","InsertCustFR","EDB101","DbConnection object is null!!");
				return iRowsAffected;
			}
		
			StringBuilder strBuilder = new StringBuilder();
			strBuilder.Append("insert into Enterprise_ESA_Sector (applicationid, enterpriseid,");
			strBuilder.Append(" postal_code,country, Esa_Sector, effective_date) values ('");
			strBuilder.Append(strAppID);
			strBuilder.Append("','");
			strBuilder.Append(strEnterpriseID);
			strBuilder.Append("','");
			strBuilder.Append(zipcode);
			strBuilder.Append("','");
			strBuilder.Append("THAILAND");
			strBuilder.Append("','");
			strBuilder.Append(zone_code);
			strBuilder.Append("','");
			if((effective_date != null) && 
				(!effective_date.GetType().Equals(System.Type.GetType("System.DBNull"))) &&
				(effective_date.ToString() != ""))
			{
				strBuilder.Append(DateTime.ParseExact(effective_date,"dd/MM/yyyy",null));
			}
			else
			{	
				strBuilder.Append(DateTime.ParseExact(effective_date,"dd/MM/yyyy",null));
			}
			strBuilder.Append("')");	
			

			IDbCommand dbCmd = dbCon.CreateCommand(strBuilder.ToString(),CommandType.Text);
			
			try
			{
				iRowsAffected = dbCon.ExecuteNonQuery(dbCmd);
				Logger.LogTraceInfo("SysDataMgrDAL","InsertCustFR","SDM001",iRowsAffected+" rows inserted in to Customer table");
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("SysDataMgrDAL","InsertCustFR","SDM001","Error During Insert "+ appException.Message.ToString());
				throw new ApplicationException("Error insert Customer Fexible Rate : "+appException.Message,appException);
			}
			return iRowsAffected;
		}
		public static int InsertCustFR(String strAppID, String strEnterpriseID, 
			String custid, String zipcode, String zone_code, object effective_date)
		{
			int iRowsAffected = 0;
			DateTime getDT = Convert.ToDateTime(effective_date);
			String getDate = "";

			if(getDT.Day.ToString().Length == 1)
				getDate = "0"+getDT.Day.ToString();
			else
				getDate = getDT.Day.ToString();

			if(getDT.Month.ToString().Length == 1)
				getDate += "/0"+getDT.Month.ToString()+"/"+getDT.Year.ToString();
			else
				getDate += "/"+getDT.Month.ToString()+"/"+getDT.Year.ToString();

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("SysDataMgrDAL","InsertCustFR","EDB101","DbConnection object is null!!");
				return iRowsAffected;
			}
		
			StringBuilder strBuilder = new StringBuilder();
			strBuilder.Append("insert into Customer_Zones (applicationid, enterpriseid,");
			strBuilder.Append("custid, zipcode, zone_code, effective_date) values ('");
			strBuilder.Append(strAppID);
			strBuilder.Append("','");
			strBuilder.Append(strEnterpriseID);
			strBuilder.Append("','");
			strBuilder.Append(custid);
			strBuilder.Append("','");
			strBuilder.Append(zipcode);
			strBuilder.Append("','");
			strBuilder.Append(zone_code);
			strBuilder.Append("',CONVERT(DateTime,'");
			strBuilder.Append(getDate);
			strBuilder.Append("',103)");	
			//			strBuilder.Append(",");
			//			if((percentEAS != null) && 
			//				(!percentEAS.GetType().Equals(System.Type.GetType("System.DBNull"))) &&
			//				(percentEAS.ToString() != ""))
			//			{
			//				strBuilder.Append(Convert.ToDecimal(percentEAS));
			//			}
			//			else
			//			{	
			//				strBuilder.Append("null");
			//			}
			strBuilder.Append(")");


			IDbCommand dbCmd = dbCon.CreateCommand(strBuilder.ToString(),CommandType.Text);
			
			try
			{
				iRowsAffected = dbCon.ExecuteNonQuery(dbCmd);
				Logger.LogTraceInfo("SysDataMgrDAL","InsertCustFR","SDM001",iRowsAffected+" rows inserted in to Customer table");
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("SysDataMgrDAL","InsertCustFR","SDM001","Error During Insert "+ appException.Message.ToString());
				throw new ApplicationException("Error insert Customer Fexible Rate : "+appException.Message,appException);
			}
			return iRowsAffected;
		}


		public static DataSet GetESASectors(String strAppID, String strEnterpriseID, 
			String zipcode, String zone_code, String strEffective_date)
		{
			SessionDS sessionDS = null;			
			StringBuilder strBuilder = new StringBuilder();

			strBuilder.Append("SELECT Postal_Code, ESA_Sector, Effective_date ");	
			strBuilder.Append("FROM Enterprise_ESA_Sector");
			strBuilder.Append(" where applicationid = '");
			strBuilder.Append(strAppID);
			strBuilder.Append("' and enterpriseid = '");
			strBuilder.Append(strEnterpriseID);
			strBuilder.Append("'");

			if (zipcode.Trim() != "")
			{
				strBuilder.Append(" and postal_code like '%");
				strBuilder.Append(zipcode+"%'");
			}
			
			if (zone_code.Trim() != "")
			{
				strBuilder.Append(" and Esa_Sector like '%");
				strBuilder.Append(zone_code+"%'");
			}

			if (strEffective_date.Trim() != "")
			{
//				strBuilder.Append(" and effective_date = CONVERT(DateTime,'");
//				strBuilder.Append(Convert.ToDateTime(strEffective_date));
//				strBuilder.Append("',103)");
				strBuilder.Append(" and effective_date ='");
				strBuilder.Append(DateTime.ParseExact(strEffective_date,"dd/MM/yyyy",null));
				strBuilder.Append("'");
			}
			
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("SysDataMgrDAL","GetCustFRData","GetCustFRData","DbConnection object is null!!");
				return sessionDS.ds;
			}
			
			
			sessionDS = dbCon.ExecuteQuery(strBuilder.ToString(), 0, 0, "FRTable");
			return  sessionDS.ds;
		}

		public static DataSet GetCustFRData(String strAppID, String strEnterpriseID, String custid,
			String zipcode, String zone_code, String strEffective_date)
		{
			SessionDS sessionDS = null;			
			String getDate = "";
			StringBuilder strBuilder = new StringBuilder();

			if(strEffective_date != "")
			{
				DateTime getDT = DateTime.ParseExact(strEffective_date,"dd/MM/yyyy",null);
				if(getDT.Day.ToString().Length == 1)
					getDate = "0"+getDT.Day.ToString();
				else
					getDate = getDT.Day.ToString();

				if(getDT.Month.ToString().Length == 1)
					getDate += "/0"+getDT.Month.ToString()+"/"+getDT.Year.ToString();
				else
					getDate += "/"+getDT.Month.ToString()+"/"+getDT.Year.ToString();
			}

			strBuilder.Append("SELECT zipcode, zone_code, Effective_date ");	
			strBuilder.Append("FROM Customer_Zones ");
			strBuilder.Append(" where applicationid = '");
			strBuilder.Append(strAppID);
			strBuilder.Append("' and enterpriseid = '");
			strBuilder.Append(strEnterpriseID);
			strBuilder.Append("' and custid = '");
			strBuilder.Append(custid);
			strBuilder.Append("'");

			if (zipcode.Trim() != "")
			{
				strBuilder.Append(" and zipcode like '%");
				strBuilder.Append(zipcode+"%'");
			}
			
			if (zone_code.Trim() != "")
			{
				strBuilder.Append(" and zone_code like '%");
				strBuilder.Append(zone_code+"%'");
			}

			if (strEffective_date.Trim() != "")
			{
				strBuilder.Append(" and effective_date = CONVERT(DateTime,'");
				strBuilder.Append(getDate);
				strBuilder.Append("',103)");
			}

			//			if (strextended_area_surcharge_percent.Trim() != "")
			//			{
			//				strBuilder.Append(" and extended_area_surcharge_percent = ");
			//				strBuilder.Append(strextended_area_surcharge_percent);
			//			}
			
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("SysDataMgrDAL","GetCustFRData","GetCustFRData","DbConnection object is null!!");
				return sessionDS.ds;
			}
			
			
			sessionDS = dbCon.ExecuteQuery(strBuilder.ToString(), 0, 0, "FRTable");
			return  sessionDS.ds;
		}
		//Get End round Effective Date by Panas
		public static DataSet GetEndRoundEffectiveDate(String strAppID, String strEnterpriseID,String startEffectiveDate)
		{
			DataSet ds  = new DataSet();
			StringBuilder strQry = new StringBuilder();

			strQry.Append("select dbo.ESA_EffectiveDate('");
			strQry.Append(startEffectiveDate);
			strQry.Append("') as endEffDate");
			
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			IDbCommand dbCmd = dbCon.CreateCommand(strQry.ToString(),CommandType.Text);

			ds = (DataSet)dbCon.ExecuteQuery(dbCmd, ReturnType.DataSetType);
			
			return  ds;
		}
		//Add by Panas
		public static bool ckDupUpdate_ESA_Sector(String strAppID, String strEnterpriseID, 
			String ZipCode,String ZoneCode,String EffectiveDate,String SZipCode,String SZoneCode,String SEffectiveDate)
		{
			DataSet ds = new DataSet();
			bool returnVal = false;

			StringBuilder strQry = new StringBuilder();
			strQry.Append("select * from Enterprise_ESA_Sector ");
			strQry.Append("where enterpriseid = '");
			strQry.Append(strEnterpriseID);
			strQry.Append("' and applicationid = '");
			strQry.Append(strAppID);
			strQry.Append("' and postal_code = '");
			strQry.Append(ZipCode+"'");
			strQry.Append(" and effective_date = Convert(DateTime,'");
			strQry.Append(EffectiveDate);
			strQry.Append("',103) and (postal_code <> '");
			strQry.Append(SZipCode+"'");
			strQry.Append(" or effective_date <> Convert(DateTime,'");
			strQry.Append(SEffectiveDate);
			strQry.Append("',103))");

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("ZipcodePopup.aspx.cs","GetZoneCodeDS","EDB101","DbConnection object is null!!");
			}

			IDbCommand dbCmd = dbCon.CreateCommand(strQry.ToString(),CommandType.Text);

			ds = (DataSet)dbCon.ExecuteQuery(dbCmd, ReturnType.DataSetType);

			if(ds.Tables[0].Rows.Count > 0)
			{
				returnVal = true;
			}
			else
			{
				returnVal = false;
			}
			return  returnVal;
		}
		
		// Add by Panas
		public static DataSet Get_CheckZipCode(String strAppID, String strEnterpriseID, String strZipCode)
		{
			DataSet ds = new DataSet();			

			StringBuilder strQry = new StringBuilder();
			strQry.Append("select zipcode from zipcode");
			strQry.Append(" where enterpriseid = '");
			strQry.Append(strEnterpriseID);
			strQry.Append("' and applicationid = '");
			strQry.Append(strAppID);
			strQry.Append("' and zipcode = '");
			strQry.Append(strZipCode);
			strQry.Append("'");

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("ZipcodePopup.aspx.cs","GetZoneCodeDS","EDB101","DbConnection object is null!!");
			}

			IDbCommand dbCmd = dbCon.CreateCommand(strQry.ToString(),CommandType.Text);

			ds = (DataSet)dbCon.ExecuteQuery(dbCmd, ReturnType.DataSetType);
			return  ds;
		}
		public static DataSet Get_CheckESA_ZoneCode(String strAppID, String strEnterpriseID, String strZoneCode)
		{
			SessionDS sessionDS = null;			

			StringBuilder strQry = new StringBuilder();
			strQry.Append(" select code_str_value as zone_code,code_text as zone_name from core_system_code where codeid = 'ESA_Sector'");
			strQry.Append(" and code_str_value = '");
			strQry.Append(strZoneCode);
			strQry.Append("' ");
			strQry.Append("order by code_str_value ");
			
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("ZoneCodePopup.aspx.cs","GetZoneCodeDS","EDB101","DbConnection object is null!!");
			}
			sessionDS = dbCon.ExecuteQuery(strQry.ToString(), 0, 0, "zone");
			return  sessionDS.ds;
		}

		public static DataSet Get_CheckCustZoneCode(String strAppID, String strEnterpriseID, String strZoneCode)
		{
			SessionDS sessionDS = null;			

			StringBuilder strQry = new StringBuilder();
			strQry.Append("select zone_code, zone_name from zone where applicationid='");
			strQry.Append(strAppID);
			strQry.Append("' and enterpriseid = '");
			strQry.Append(strEnterpriseID);
			strQry.Append("'");
			if (strZoneCode != null && strZoneCode != "")
			{
				strQry.Append(" and zone_code =N'");
				strQry.Append(Utility.ReplaceSingleQuote(strZoneCode));
				strQry.Append("'");
			}

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("ZoneCodePopup.aspx.cs","GetZoneCodeDS","EDB101","DbConnection object is null!!");
			}
			sessionDS = dbCon.ExecuteQuery(strQry.ToString(), 0, 0, "zone");
			return  sessionDS.ds;
		}


		public static DataSet GetCustZipCode(String strAppID, String strEnterpriseID, String custid,
			String strZipCode, String strCountry)
		{
			SessionDS sessionDS = null;			

			StringBuilder strQry = new StringBuilder();
			strQry.Append("select z.zipcode as zipcode,s.state_name as state_name,z.country as country,z.esa_surcharge as esa_surcharge from Zipcode z, State s where z.applicationid='");
			strQry.Append(strAppID);
			strQry.Append("' and z.enterpriseid = '");
			strQry.Append(strEnterpriseID);
			strQry.Append("' and z.applicationid = s.applicationid and z.enterpriseid = s.enterpriseid and z.country = s.country and z.state_code = s.state_code ");

			if(strZipCode != null && strZipCode != "")
			{
				strQry.Append(" and z.zipcode like '%");
				strQry.Append(Utility.ReplaceSingleQuote(strZipCode));
				strQry.Append("%' ");
			}
			if(strCountry != null && strCountry != "")
			{
				strQry.Append(" and z.country like '%");
				strQry.Append(Utility.ReplaceSingleQuote(strCountry));
				strQry.Append("%' ");
			}

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("SysDataMgrDAL","GetCustZipCode","GetCustZipCode","DbConnection object is null!!");
				return sessionDS.ds;
			}
			
			
			sessionDS = dbCon.ExecuteQuery(strQry.ToString(), 0, 0, "FRTable");
			return  sessionDS.ds;

		}

		public static int DeleteESASector(String strAppID, String strEnterpriseID,
			String zipcode, String zone_code, String effective_date)
		{
			IDbCommand dbCmd = null;
			int iRowsAffected = 0;
			StringBuilder strBuild = null;
			string getDate = "";

			if(effective_date != "")
			{
				DateTime getDT = DateTime.ParseExact(effective_date,"dd/MM/yyyy",null);

				if(getDT.Day.ToString().Length == 1)
					getDate = "0"+getDT.Day.ToString();
				else 
					getDate = getDT.Day.ToString();

				if(getDT.Month.ToString().Length == 1)
					getDate += "/0"+getDT.Month.ToString()+"/"+getDT.Year.ToString();
				else
					getDate += "/"+getDT.Month.ToString()+"/"+getDT.Year.ToString();
			}
			
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("SysDataMgrDAL","DeleteFR","DeleteFR","DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null",null);
			}

			IDbConnection conApp = dbCon.GetConnection();
			if(conApp == null)
			{
				Logger.LogTraceError("SysDataMgrDAL","DeleteFR","DeleteFR","conApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			try
			{
				dbCon.OpenConnection(ref conApp);
			}
			catch(ApplicationException appException)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("SysDataMgrDAL","DeleteFR","DeleteFR","Error opening connection : "+ appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ",appException);
			}
			catch(Exception exception)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("SysDataMgrDAL","DeleteFR","DeleteFR","Error opening connection : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}
			IDbTransaction transactionApp = dbCon.BeginTransaction(conApp,IsolationLevel.ReadCommitted);

			if(transactionApp == null)
			{
				Logger.LogTraceError("SysDataMgrDAL","DeleteFR","DeleteFR","transactionApp is null");
				throw new ApplicationException("Failed Begining a App Transaction..",null);
			}
			try
			{

				//delete from the audit table
				strBuild = new StringBuilder();
				strBuild.Append("DELETE " );
				strBuild.Append("FROM Enterprise_ESA_Sector ");
				strBuild.Append(" where applicationid = '");
				strBuild.Append(strAppID);
				strBuild.Append("' and enterpriseid = '");
				strBuild.Append(strEnterpriseID);
				strBuild.Append("' and postal_code = '");
				strBuild.Append(zipcode);
				strBuild.Append("' and ESA_Sector = '");
				strBuild.Append(zone_code);
				strBuild.Append("' and effective_date = Convert(datetime,'");
				strBuild.Append(getDate);
				strBuild.Append("',103)");

				dbCmd = dbCon.CreateCommand(strBuild.ToString(),CommandType.Text);
				dbCmd.Connection = conApp;
				dbCmd.Transaction = transactionApp;
				iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);

				transactionApp.Commit();
			}
			catch(Exception exception)
			{
				try
				{
					transactionApp.Rollback();
					Logger.LogDebugInfo("SysDataMgrDAL","DeleteFR","DeleteFR","App db insert transaction rolled back.");

				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("SysDataMgrDAL","DeleteFR","DeleteFR","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}

				throw new ApplicationException("Error deleting flexible rate data ",exception);
			}
			finally
			{
				if(conApp != null)
				{
					conApp.Close();
				}
			}
			return iRowsAffected;

		}



		public static int DeleteFR(String strAppID, String strEnterpriseID,
			String custid, String zipcode, String zone_code, String effective_date)
		{
			IDbCommand dbCmd = null;
			int iRowsAffected = 0;
			StringBuilder strBuild = null;
			
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("SysDataMgrDAL","DeleteFR","DeleteFR","DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null",null);
			}

			IDbConnection conApp = dbCon.GetConnection();
			if(conApp == null)
			{
				Logger.LogTraceError("SysDataMgrDAL","DeleteFR","DeleteFR","conApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			try
			{
				dbCon.OpenConnection(ref conApp);
			}
			catch(ApplicationException appException)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("SysDataMgrDAL","DeleteFR","DeleteFR","Error opening connection : "+ appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ",appException);
			}
			catch(Exception exception)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("SysDataMgrDAL","DeleteFR","DeleteFR","Error opening connection : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}
			IDbTransaction transactionApp = dbCon.BeginTransaction(conApp,IsolationLevel.ReadCommitted);

			if(transactionApp == null)
			{
				Logger.LogTraceError("SysDataMgrDAL","DeleteFR","DeleteFR","transactionApp is null");
				throw new ApplicationException("Failed Begining a App Transaction..",null);
			}
			try
			{

				//delete from the audit table
				strBuild = new StringBuilder();
				strBuild.Append("DELETE " );
				strBuild.Append("FROM Customer_Zones ");
				strBuild.Append(" where applicationid = '");
				strBuild.Append(strAppID);
				strBuild.Append("' and enterpriseid = '");
				strBuild.Append(strEnterpriseID);
				strBuild.Append("' and custid = '");
				strBuild.Append(custid);
				strBuild.Append("' and zipcode = '");
				strBuild.Append(zipcode);
				strBuild.Append("' and zone_code = '");
				strBuild.Append(zone_code);
				strBuild.Append("' and effective_date = Convert(datetime,'");
				strBuild.Append(effective_date);
				strBuild.Append("',103)");

				dbCmd = dbCon.CreateCommand(strBuild.ToString(),CommandType.Text);
				dbCmd.Connection = conApp;
				dbCmd.Transaction = transactionApp;
				iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);

				transactionApp.Commit();
			}
			catch(ApplicationException appException)
			{
				try
				{
					transactionApp.Rollback();
					Logger.LogDebugInfo("SysDataMgrDAL","DeleteFR","DeleteFR","App db insert transaction rolled back.");

				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("SysDataMgrDAL","DeleteFR","DeleteFR","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}


				throw new ApplicationException("Error deleting flexible rate data ",appException);
			}
			catch(Exception exception)
			{
				try
				{
					transactionApp.Rollback();
					Logger.LogDebugInfo("SysDataMgrDAL","DeleteFR","DeleteFR","App db insert transaction rolled back.");

				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("SysDataMgrDAL","DeleteFR","DeleteFR","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}

				throw new ApplicationException("Error deleting flexible rate data ",exception);
			}
			finally
			{
				if(conApp != null)
				{
					conApp.Close();
				}
			}
			return iRowsAffected;

		}


		public static SessionDS GetCustInforByCustID(String strAppID, String strEnterpriseID, 
			int iCurrent, int iDSRecSize,String custID)
		{
			SessionDS sessionDS = null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("AgentDataMgrDAL","GetAgentProfileDS","EDB101","DbConnection object is null!!");
				return sessionDS;
			}

			StringBuilder strBuilder = new StringBuilder();
			strBuilder.Append("SELECT * ");
			strBuilder.Append("FROM customer ");
			strBuilder.Append("WHERE applicationid ='"+strAppID+"' and enterpriseid ='"+strEnterpriseID+"' ");
			strBuilder.Append("and custid = '" + custID + "'");

			String strSQLQuery = strBuilder.ToString();			
			sessionDS = dbCon.ExecuteQuery(strSQLQuery,iCurrent,iDSRecSize,"AgentProfileTable");

			return  sessionDS;
	
		}


		public static SessionDS GetCustZoneData(String strAppID, String strEnterpriseID, 
			int iCurrent, int iDSRecSize, String custID, String strZipCode)
		{
			SessionDS sessionDS = null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("AgentDataMgrDAL","GetAgentProfileDS","EDB101","DbConnection object is null!!");
				return sessionDS;
			}

			StringBuilder strBuilder = new StringBuilder();
			strBuilder.Append("select top 1 * from customer_zones ");
			strBuilder.Append("where applicationid = '");
			strBuilder.Append(strAppID);
			strBuilder.Append("' and enterpriseid = '");
			strBuilder.Append(strEnterpriseID);
			strBuilder.Append("' and custid = '");
			strBuilder.Append(custID);
			strBuilder.Append("' and zipcode = '");
			strBuilder.Append(strZipCode + "'");
			//add by x nov 21 08
			strBuilder.Append(" and effective_date < getdate() order by effective_date DESC ");


			String strSQLQuery = strBuilder.ToString();			
			sessionDS = dbCon.ExecuteQuery(strSQLQuery,iCurrent,iDSRecSize,"AgentProfileTable");

			return  sessionDS;
	
		}

		//Created by GwanG on 19March08
		public static SessionDS GetEmptyVASCustDS(int iNumRows)
		{
			
			DataTable dtVASCode = new DataTable();
 
			dtVASCode.Columns.Add(new DataColumn("vas_code", typeof(string)));
			dtVASCode.Columns.Add(new DataColumn("description", typeof(string)));
			dtVASCode.Columns.Add(new DataColumn("surcharge", typeof(decimal)));
			dtVASCode.Columns.Add(new DataColumn("percent", typeof(decimal)));
			dtVASCode.Columns.Add(new DataColumn("min_surcharge", typeof(decimal)));
			dtVASCode.Columns.Add(new DataColumn("max_surcharge", typeof(decimal)));


			for(int i=0; i < iNumRows; i++)
			{
				DataRow drEach = dtVASCode.NewRow();
				drEach[0] = "";
				drEach[1] = "";

				//				drEach["surcharge"] = "";
				//				drEach["percent"] = "";
				//				drEach["minsurch"] = "";
				//				drEach["maxsurch"] = "";

				dtVASCode.Rows.Add(drEach);
			}

			DataSet dsVASCode = new DataSet();
			dsVASCode.Tables.Add(dtVASCode);

			//			dsVASCode.Tables[0].Columns["vas_code"].Unique = true; //Checks duplicate records..
			dsVASCode.Tables[0].Columns["vas_code"].AllowDBNull = false;

			SessionDS sessionDS = new SessionDS();
			sessionDS.ds = dsVASCode;
			sessionDS.DataSetRecSize = dsVASCode.Tables[0].Rows.Count;
			sessionDS.QueryResultMaxSize = sessionDS.DataSetRecSize;

			return  sessionDS;

		}

		
		public static SessionDS GetVASCust(String strAppID, String strEnterpriseID,int iCurrent, int iDSRecSize,String custID)
		{
			SessionDS sessionDS = null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("SysDataMgrDAL","GetVASCust","EDB101","DbConnection object is null!!");
				return sessionDS;
			}

			StringBuilder strBuilder = new StringBuilder();
			strBuilder.Append("SELECT * ");
			strBuilder.Append("FROM customer_vas ");
			strBuilder.Append("WHERE applicationid ='"+strAppID+"' and enterpriseid ='"+strEnterpriseID+"' ");
			strBuilder.Append("and custid = '" + custID + "'");

			String strSQLQuery = strBuilder.ToString();			
			sessionDS = dbCon.ExecuteQuery(strSQLQuery,iCurrent,iDSRecSize,"CustVAS");

			return  sessionDS;
	
		}
		

		public static void AddNewRowInCPVASDS(SessionDS sessionDS)
		{
			DataRow drNew = sessionDS.ds.Tables[0].NewRow();

			drNew[0] = "";
			drNew[1] = "";
			try
			{
				sessionDS.ds.Tables[0].Rows.Add(drNew);
			}
			catch(Exception ex)
			{
				Logger.LogTraceError("SysDataMgrDAL","AddNewRowInCPVASDS","R005",ex.Message.ToString());
			}

			
			sessionDS.QueryResultMaxSize++;
		}


		public static SessionDS QueryVAS(String strAppID, String strEnterpriseID, String strCustID, int recStart, int recSize)
		{
			
			SessionDS sdsBaseTables = null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("SysDataMgrDAL","QueryVAS","EDB101","DbConnection object is null!!");
				return sdsBaseTables;
			}	
			String strSQLQuery = "select [vas_code], [description], [surcharge], [percent], [min_surcharge], [max_surcharge] from Customer_VAS where ";			
			strSQLQuery += "applicationid = '"+strAppID+"' and enterpriseid = '"+strEnterpriseID+"' and custid = '"+strCustID+"'"; 		

			sdsBaseTables = (SessionDS)dbCon.ExecuteQuery(strSQLQuery,recStart, recSize,"VAS");
			return  sdsBaseTables;
		}

		public static DataSet QueryVAS(String strAppID, String strEnterpriseID, String strCustID)
		{			
			DataSet ds = null;
			IDbCommand dbCmd = null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("SysDataMgrDAL","QueryVAS","EDB101","DbConnection object is null!!");
				return ds;
			}	
			String strSQLQuery = @"
					select * 
					from	Customer_VAS 
					where	applicationid = '"+strAppID+"' and enterpriseid = '"+strEnterpriseID+"' and custid = '"+strCustID+"'"; 		

			//sdsBaseTables = (SessionDS)dbCon.ExecuteQuery(strSQLQuery,recStart, recSize,"VAS");

			dbCmd = dbCon.CreateCommand(strSQLQuery,CommandType.Text);
			dbCmd.CommandTimeout = 300; //5 minutes
			ds = (DataSet)dbCon.ExecuteQuery(dbCmd, ReturnType.DataSetType);
			
			return  ds;
		}

		public static int AddCustVAS(String strAppID, String strEnterpriseID,DataSet dsToInsert)
		{
			int iRowsAffected = 0;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("SysDataMgrDAL","AddCustVAS","EDB101","DbConnection object is null!!");
				return iRowsAffected;
			}

			StringBuilder strBuilder = new StringBuilder();
			strBuilder.Append("insert into Customer_VAS (applicationid,enterpriseid,custid, [vas_code], [description], [surcharge], [percent], [min_surcharge], [max_surcharge]) values ('");
			strBuilder.Append(strAppID);
			strBuilder.Append("','");
			strBuilder.Append(strEnterpriseID);
			strBuilder.Append("',N'");

			int iRowCnt = dsToInsert.Tables[0].Rows.Count;

			DataRow drEach = dsToInsert.Tables[0].Rows[0];
			String strCustID = (String) drEach["custid"];
			strBuilder.Append(strCustID);
			strBuilder.Append("','");
				
			String strVASCode = (String) drEach["vas_code"];
			strBuilder.Append(strVASCode);
			strBuilder.Append("',N'");

			String strVASDescription = (String) drEach["description"];
			strBuilder.Append(Utility.ReplaceSingleQuote(strVASDescription));
			strBuilder.Append("',");
			
			if(Utility.IsNotDBNull(drEach["surcharge"]) &&  drEach["surcharge"].ToString() !="")
			{
				decimal decSurcharge = (decimal) drEach["surcharge"];			
				strBuilder.Append(decSurcharge);
				strBuilder.Append(",");
			}
			else
			{
				strBuilder.Append("null");
				strBuilder.Append(",");

			}

			if(Utility.IsNotDBNull(drEach["percent"]) &&  drEach["percent"].ToString() !="")
			{
				decimal percent = (decimal) drEach["percent"];			
				strBuilder.Append(percent);
				strBuilder.Append(",");
			}
			else
			{
				strBuilder.Append("null");
				strBuilder.Append(",");

			}

			
			if(Utility.IsNotDBNull(drEach["min_surcharge"]) &&  drEach["min_surcharge"].ToString() !="")
			{
				decimal min_surcharge = (decimal) drEach["min_surcharge"];			
				strBuilder.Append(min_surcharge);
				strBuilder.Append(",");
			}
			else
			{
				strBuilder.Append("null");
				strBuilder.Append(",");

			}

						
			if(Utility.IsNotDBNull(drEach["max_surcharge"]) &&  drEach["max_surcharge"].ToString() !="")
			{
				decimal max_surcharge = (decimal) drEach["max_surcharge"];			
				strBuilder.Append(max_surcharge);
				strBuilder.Append(")");
			}
			else
			{
				strBuilder.Append("null");
				strBuilder.Append(")");

			}

			String strSQLQuery = strBuilder.ToString();

			IDbCommand dbCmd = dbCon.CreateCommand(strSQLQuery,CommandType.Text);
			

			try
			{

				iRowsAffected = dbCon.ExecuteNonQuery(dbCmd);
				Logger.LogTraceInfo("SysDataMgrDAL","AddCustVAS","SDM001",iRowsAffected+" rows inserted in to VAS table");
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("RBACManager","AddCustVAS","SDM001","Error During Insert "+ appException.Message.ToString());
				throw new ApplicationException("Error inserting VAS Code ",appException);
			}



			return iRowsAffected;

		}


		public static int ModifyCustVAS(String strAppID, String strEnterpriseID,DataSet dsToModify)
		{
			int iRowsAffected = 0;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("SysDataMgrDAL","ModifyCustVAS","EDB101","DbConnection object is null!!");
				return iRowsAffected;
			}
			int iRowCnt = dsToModify.Tables[0].Rows.Count;
			DataRow drEach = dsToModify.Tables[0].Rows[0];


			StringBuilder strBuilder = new StringBuilder();
			
			strBuilder.Append("update customer_vas set ");
			strBuilder.Append("[description] = N'");
			String strVASDescription = (String) drEach["description"];
			strBuilder.Append(Utility.ReplaceSingleQuote(strVASDescription));
			strBuilder.Append("', surcharge = ");

			if(Utility.IsNotDBNull(drEach["surcharge"]) && drEach["surcharge"].ToString()!="")
			{
				decimal decSurcharge = (decimal) drEach["surcharge"];
				strBuilder.Append(decSurcharge);
			}
			else
			{
				strBuilder.Append("null");
			}

			strBuilder.Append(", [percent] = ");
			if(Utility.IsNotDBNull(drEach["percent"]) &&  drEach["percent"].ToString() !="")
			{
				decimal percent = (decimal) drEach["percent"];			
				strBuilder.Append(percent);
			}
			else
			{
				strBuilder.Append("null");
			}
			
			strBuilder.Append(", [min_surcharge] = ");
			if(Utility.IsNotDBNull(drEach["min_surcharge"]) &&  drEach["min_surcharge"].ToString() !="")
			{
				decimal min_surcharge = (decimal) drEach["min_surcharge"];			
				strBuilder.Append(min_surcharge);
			}
			else
			{
				strBuilder.Append("null");
			}

			strBuilder.Append(", [max_surcharge] = ");			
			if(Utility.IsNotDBNull(drEach["max_surcharge"]) &&  drEach["max_surcharge"].ToString() !="")
			{
				decimal max_surcharge = (decimal) drEach["max_surcharge"];			
				strBuilder.Append(max_surcharge);
			}
			else
			{
				strBuilder.Append("null");
			}

			strBuilder.Append(" where applicationid = '");
			strBuilder.Append(strAppID);
			strBuilder.Append("' and enterpriseid = '");
			strBuilder.Append(strEnterpriseID);
			strBuilder.Append("' and vas_code = '");
			String strVASCode = (String) drEach["vas_code"];
			strBuilder.Append(strVASCode);
			strBuilder.Append("'");
			strBuilder.Append(" and custid = '");
			String strCustID = (String) drEach["custid"];
			strBuilder.Append(strCustID);
			strBuilder.Append("'");



			String strSQLQuery = strBuilder.ToString();

			IDbCommand dbCmd = dbCon.CreateCommand(strSQLQuery,CommandType.Text);
			
			try
			{

				iRowsAffected = dbCon.ExecuteNonQuery(dbCmd);
				Logger.LogTraceInfo("SysDataMgrDAL","ModifyCustVAS","SDM001",iRowsAffected+" rows inserted in to VAS_Code table");
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("RBACManager","ModifyCustVAS","SDM001","Error During Insert "+ appException.Message.ToString());
				throw new ApplicationException("Error inserting VAS Code ",appException);
			}
			return iRowsAffected;

		}


		public static int DeleteCustVAS(String strAppID, String strEnterpriseID,String strCustID,String strVAScode)
		{
			int iRowsDeleted = 0;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("SysDataMgrDAL","DeleteCustVAS","EDB101","DbConnection object is null!!");
				return iRowsDeleted;
			}


			StringBuilder strBuilder = new StringBuilder();
			
			strBuilder.Append("Delete from Customer_VAS where ");
			strBuilder.Append(" applicationid = '");
			strBuilder.Append(strAppID);
			strBuilder.Append("' and enterpriseid = '");
			strBuilder.Append(strEnterpriseID);
			strBuilder.Append("' and custid = '");
			strBuilder.Append(strCustID);
			strBuilder.Append("' and vas_code = '");
			strBuilder.Append(strVAScode);
			strBuilder.Append("'");


			String strSQLQuery = strBuilder.ToString();

			IDbCommand dbCmd = dbCon.CreateCommand(strSQLQuery,CommandType.Text);
			
			try
			{
				iRowsDeleted = dbCon.ExecuteNonQuery(dbCmd);
				Logger.LogTraceInfo("SysDataMgrDAL","DeleteCPExceptionCodeDS","SDM001",iRowsDeleted+" rows deleted from  Exception_Code table");
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("SysDataMgrDAL","DeleteCPExceptionCodeDS","SDM001","Error During Delete "+ appException.Message.ToString());
				throw new ApplicationException("Error inserting Status Code : "+appException.Message,appException);
			}

			return iRowsDeleted;		
		}



		public static DataSet GetCustVasCode(String strAppID, String strEnterpriseID,String custid)
		{
			DataSet dsResult = new DataSet();
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID);

			if(dbCon == null)
			{
				Logger.LogTraceError("Utility","GetCustVasCode","EDB101","DbConnection object is null!!");
				System.Diagnostics.Trace.WriteLineIf(Logger.AppSwitch.TraceError,"[Utility::GetCustVasCode()] DbConnection is null","ERROR");
				return dsResult;
			}

			String strSQLQuery	 = " select vas_code,vas_description,surcharge ";
			strSQLQuery			+= " from VAS ";
			strSQLQuery			+= " where vas_code not in ( ";
			strSQLQuery			+= " select vas_code from Customer_Vas ";
			strSQLQuery			+= " where applicationid='";
			strSQLQuery			+= strAppID ;
			strSQLQuery			+= " ' and enterpriseid='";
			strSQLQuery			+= strEnterpriseID ;
			strSQLQuery			+= " ' and custid='";
			strSQLQuery			+= custid ;
			strSQLQuery			+= "')and applicationid = '";
			strSQLQuery			+= strAppID ;
			strSQLQuery			+= "' and enterpriseid = '";
			strSQLQuery			+= strEnterpriseID ;
			strSQLQuery			+= "' ";
			strSQLQuery			+= " and allow_for_customer = 'Y'";
			
			DataSet dsVASCode = (DataSet)dbCon.ExecuteQuery(strSQLQuery,ReturnType.DataSetType);
			DataTable dtVASCode = new DataTable();
 
			dtVASCode.Columns.Add(new DataColumn("vas_code", typeof(string)));
			dtVASCode.Columns.Add(new DataColumn("description", typeof(string)));
			dtVASCode.Columns.Add(new DataColumn("surcharge", typeof(decimal)));

			DataRow drEach = dtVASCode.NewRow();
			drEach[0] = "";
			drEach[1] = "";
			dtVASCode.Rows.Add(drEach);
			int cnt = dsVASCode.Tables[0].Rows.Count;
			if(cnt > 0)
			{
				for(int i=0;i<cnt;i++)
				{					
					DataRow dr = dsVASCode.Tables[0].Rows[i];
					drEach = dtVASCode.NewRow();
					drEach[0] = dr["vas_code"];
					drEach[1] = dr["vas_description"];
					drEach[2] = dr["surcharge"];
					dtVASCode.Rows.Add(drEach);
				}

			}
			dsResult.Tables.Add(dtVASCode);
            

			return  dsResult;
		}


		public static DataSet GetCustDescSurch(String strAppID, String strEnterpriseID,String vascode)
		{
			DataSet dsResult = new DataSet();
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("SysDataMgrDAL.cs","GetCustVasDesc","EDB101","DbConnection object is null!!");
				return dsResult;
			}

			StringBuilder strBuilder = new StringBuilder();
			strBuilder.Append("SELECT * ");
			strBuilder.Append("FROM VAS ");
			strBuilder.Append("WHERE applicationid ='"+strAppID+"' and enterpriseid ='"+strEnterpriseID+"' ");
			strBuilder.Append("and vas_code = '" + vascode + "'");

			String strSQLQuery = strBuilder.ToString();	
			DataSet dsVASCode = (DataSet)dbCon.ExecuteQuery(strSQLQuery,ReturnType.DataSetType);
			DataTable dtVASCode = new DataTable();
 
			dtVASCode.Columns.Add(new DataColumn("vas_code", typeof(string)));
			dtVASCode.Columns.Add(new DataColumn("description", typeof(string)));
			dtVASCode.Columns.Add(new DataColumn("surcharge", typeof(decimal)));

			if(dsVASCode.Tables[0].Rows.Count > 0)
			{			
				DataRow dr = dsVASCode.Tables[0].Rows[0];
				DataRow drEach = dtVASCode.NewRow();
				drEach[0] = dr["vas_code"];
				drEach[1] = dr["vas_description"];
				drEach[2] = dr["surcharge"];
				dtVASCode.Rows.Add(drEach);

			}
			dsResult.Tables.Add(dtVASCode);
            
			return  dsResult;
		}

		public static DataSet GetVASCustDS(String strAppID, String strEnterpriseID,String vascode,String custid)
		{
			DataSet dsResult = new DataSet();
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("SysDataMgrDAL.cs","GetCustVasDesc","EDB101","DbConnection object is null!!");
				return dsResult;
			}

			StringBuilder strBuilder = new StringBuilder();
			strBuilder.Append("SELECT * ");
			strBuilder.Append("FROM Customer_VAS ");
			strBuilder.Append("WHERE applicationid ='"+strAppID+"' and enterpriseid ='"+strEnterpriseID+"' ");
			strBuilder.Append("and vas_code = '" + vascode + "' ");
			strBuilder.Append("and custid = '" + custid + "' ");

			String strSQLQuery = strBuilder.ToString();	
			DataSet dsVASCode = (DataSet)dbCon.ExecuteQuery(strSQLQuery,ReturnType.DataSetType);
			DataTable dtVASCode = new DataTable();
 
			dtVASCode.Columns.Add(new DataColumn("vas_code", typeof(string)));
			dtVASCode.Columns.Add(new DataColumn("description", typeof(string)));
			dtVASCode.Columns.Add(new DataColumn("surcharge", typeof(decimal)));
			dtVASCode.Columns.Add(new DataColumn("percent", typeof(decimal)));
			dtVASCode.Columns.Add(new DataColumn("min_surcharge", typeof(decimal)));
			dtVASCode.Columns.Add(new DataColumn("max_surcharge", typeof(decimal)));


			if(dsVASCode.Tables[0].Rows.Count > 0)
			{			
				DataRow dr = dsVASCode.Tables[0].Rows[0];
				DataRow drEach = dtVASCode.NewRow();
				drEach["vas_code"] = dr["vas_code"];
				drEach["description"] = dr["description"];
				drEach["surcharge"] = dr["surcharge"];
				drEach["percent"] = dr["percent"];
				drEach["min_surcharge"] = dr["min_surcharge"];
				drEach["max_surcharge"] = dr["max_surcharge"];
			
				dtVASCode.Rows.Add(drEach);

			}
			dsResult.Tables.Add(dtVASCode);
            
			return  dsResult;
		}

		public static DataTable GetCustomerProfile_BillPlacementRules (String strAppID, String strEnterpriseID,String custid)
		{
			DataSet dsResult = new DataSet();
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("SysDataMgrDAL.cs","GetCustomerProfile_BillPlacementRules","EDB101","DbConnection object is null!!");
				return dsResult.Tables[0];
			}

			StringBuilder strBuilder = new StringBuilder();
			strBuilder.Append("SELECT * ");
			strBuilder.Append("FROM Customer_BillPlacementRules ");
			strBuilder.Append("WHERE applicationid ='"+strAppID+"' and enterpriseid ='"+strEnterpriseID+"' ");
			strBuilder.Append("and custid = '" + custid + "' ");

			String strSQLQuery = strBuilder.ToString();	
			dsResult = (DataSet)dbCon.ExecuteQuery(strSQLQuery,ReturnType.DataSetType);
			return dsResult.Tables[0];

		}
	}
}
