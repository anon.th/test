using System;
using System.Data;
using System.Collections;
using System.Text;
using com.common.DAL;
using com.common.util;
using com.common.classes;
using System.IO.IsolatedStorage;  
using System.Xml;


namespace com.ties.DAL
{
	/// <summary>
	/// Summary description for QuotationMgrDAl.
	/// </summary>
	public class QuotationMgrDAl
	{
		public QuotationMgrDAl()
		{
			//
			// TODO: Add constructor logic here
			//
		}
		public static int GetQuotationNo(string strappid,string strenterpriseid,int iCustType)
		{
			DbConnection dbCon = null;
			IDbCommand dbCmd = null;
			DataSet dsQuotation=null;
			DateTime dtCurrenDate = DateTime.Now;
			string strPrefix = "QB";
			string strMonth = dtCurrenDate.Month.ToString() ;
			string strYear = dtCurrenDate.Year.ToString() ; 
			string strQry=null;
			int iRecCnt=0;
			int iMaxQuoationNo=0;
			bool iCheck=false;
				
			dbCon = DbConnectionManager.GetInstance().GetDbConnection(strappid,strenterpriseid);  

			if(dbCon == null)
			{
				Logger.LogTraceError("CustomerDetails.aspx.cs","RefreshData","CustDtl001","dbConApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			if (iCustType==0)
				iCheck=true;
			else if (iCustType==1) 
			{
				string strQuotationNo = strPrefix+strYear+strMonth;
					 
				strQry = "select count(*) from customer_quotation where quotation_no like '%"+strQuotationNo+"%'";
				dbCmd = dbCon.CreateCommand(strQry,CommandType.Text);
				try
				{
					dsQuotation =(DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);									  
				}
				catch(ApplicationException appExpection)
				{
					Logger.LogTraceError("CustomerDetails.aspx.cs","RefreshData","CustDtl002","Error in the query String");
					throw appExpection;
				}

				DataRow drEach = dsQuotation.Tables[0].Rows[0] ;
				iRecCnt = (int)drEach[0]; 
			}
			else if (iCustType==2) 
			{
				string strQuotationNo = strPrefix+strYear+strMonth;
					 
				strQry = "select count(*) from agent_quotation where quotation_no like '%"+strQuotationNo+"%'";
				dbCmd = dbCon.CreateCommand(strQry,CommandType.Text);
				try
				{
					dsQuotation =(DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);									  
				}
				catch(ApplicationException appExpection)
				{
					Logger.LogTraceError("CustomerDetails.aspx.cs","RefreshData","CustDtl002","Error in the query String");
					throw appExpection;
				}

				DataRow drEach = dsQuotation.Tables[0].Rows[0] ;
				iRecCnt = (int)drEach[0]; 

			}

			if (iCheck == false)
			{
				if  (iRecCnt  <= 9999)
					iMaxQuoationNo = iRecCnt +1;
				else if(iRecCnt ==0)
					iMaxQuoationNo = 1;
			}
			return iMaxQuoationNo; 
		}
		public static string  GetActualQuotationNo(int QuotNo)
		{
			DateTime dtCurrenDate = DateTime.Now;
			string strPrefix = "QB";
			string strMonth = dtCurrenDate.Month.ToString() ;
			string strYear = dtCurrenDate.Year.ToString() ; 
			string strQuotationNo=null;

			if (QuotNo.ToString().Length==1)   
				strQuotationNo = strPrefix+strYear+strMonth+"000"+QuotNo;
			else if(QuotNo.ToString().Length==2)   
				strQuotationNo = strPrefix+strYear+strMonth+"00"+QuotNo;
			else if (QuotNo.ToString().Length==3)
				strQuotationNo = strPrefix+strYear+strMonth+"0"+QuotNo;

			return strQuotationNo; 
		}
		public static SessionDS GetEmptyCustQuoationds()
		{
			DataTable dtCustQuoation = new DataTable();
			dtCustQuoation.Columns.Add("custid",typeof(string));
			dtCustQuoation.Columns.Add("quotation_no",typeof(string));	
			dtCustQuoation.Columns.Add("quotation_version",typeof(string));
			dtCustQuoation.Columns.Add("band_code",typeof(string));
			dtCustQuoation.Columns.Add("quotation_date",typeof(string));
			dtCustQuoation.Columns.Add("salesmanid",typeof(string));
			dtCustQuoation.Columns.Add("attention_to_person",typeof(string));
			dtCustQuoation.Columns.Add("copy_to_person",typeof(string));
			dtCustQuoation.Columns.Add ("quotation_status",typeof(string)); 
			dtCustQuoation.Columns.Add ("remark",typeof(string));
			dtCustQuoation.Columns.Add ("type",typeof(string));
			dtCustQuoation.Columns.Add("salesman_name",typeof(string));

			DataSet dsCustQuotation = new DataSet();
			DataRow drEach = dtCustQuoation.NewRow();
			drEach[0]="";

			dtCustQuoation.Rows.Add(drEach);
			dsCustQuotation.Tables.Add(dtCustQuoation);

			SessionDS sessionDS = new SessionDS();
			sessionDS.ds = dsCustQuotation;
			
			sessionDS.DataSetRecSize = dsCustQuotation.Tables[0].Rows.Count;
			sessionDS.QueryResultMaxSize = sessionDS.DataSetRecSize;
			
			return sessionDS;
		}

		public static DataSet GetEmptyVASDS()
		{
			DataTable dtVAS = new DataTable();
			dtVAS.Columns.Add(new DataColumn("vas_code", typeof(string)));
			dtVAS.Columns.Add(new DataColumn("vas_description",typeof(string)));
			dtVAS.Columns.Add(new DataColumn("surcharge",typeof(decimal)));
		
			DataSet dsVAS = new DataSet();
			dsVAS.Tables.Add(dtVAS);
			//dsVAS.Tables[0].Columns["vas_code"].Unique = true;
			dsVAS.Tables[0].Columns["vas_code"].Unique = false;
			dsVAS.Tables[0].Columns["vas_code"].AllowDBNull = false;
			
			return  dsVAS;
		}

		public static DataSet GetEmptyServiceDS()
		{
			DataTable dtService = new DataTable();
			dtService.Columns.Add(new DataColumn("service_code", typeof(string)));
			dtService.Columns.Add(new DataColumn("service_description",typeof(string)));
			dtService.Columns.Add(new DataColumn("service_charge_percent",typeof(decimal)));
			dtService.Columns.Add(new DataColumn("service_charge_amt",typeof(decimal)));

			DataSet dsService = new DataSet();
			dsService.Tables.Add(dtService);
			//dsService.Tables[0].Columns["service_code"].Unique = true;
			dsService.Tables[0].Columns["service_code"].Unique = false;
			dsService.Tables[0].Columns["service_code"].AllowDBNull = false;
		
			return  dsService;                                                                                                                                                                                                                                                                                                                 
		}

		public static void AddNewRowInVAS(DataSet dsVAS)
		{
			DataRow drNew = dsVAS.Tables[0].NewRow();
			drNew["vas_code"] = "";
			drNew["vas_description"] = "";
			drNew["surcharge"] = 0;
			try
			{
				dsVAS.Tables[0].Rows.Add(drNew);
			}
			catch(Exception ex)
			{
				Logger.LogTraceInfo("Quotation","AddNewRowInVAS","Error1",ex.Message.ToString());
			}
		}

		public static void AddNewRowInService(DataSet dsService)
		{
			DataRow drNew = dsService.Tables[0].NewRow();
			drNew["service_code"] = "";
			drNew["service_description"] = "";
			drNew["service_charge_percent"] = 0;
			drNew["service_charge_amt"]=0;

			try
			{
				dsService.Tables[0].Rows.Add(drNew);
			}
			catch(Exception ex)
			{
				Logger.LogTraceInfo("Quotation","AddNewRowInService","Error2",ex.Message.ToString());
			}
		}
		
		public static int  AddCustQuotation(string appid,string enterpriseid,DataSet dsCustQuotation,DataSet dsVAS, DataSet dsService)
		{
			string strQry =null;
			int iRowsEffected=0;
			DbConnection dbCon=null;
			IDbConnection conApp =null;
				
			int cnt = 0;
			int i = 0;
			
			dbCon = DbConnectionManager.GetInstance().GetDbConnection(appid,enterpriseid);
			
			if(dbCon==null)
			{
				Logger.LogTraceError("QuotationMgrDAL","AddCustQuotation","QMD001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			
			try
			{
				conApp = dbCon.GetConnection();
				dbCon.OpenConnection(ref conApp); 
			}
			catch(System.ApplicationException appException)
			{	
				if (conApp!=null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("QuotationMgrDAL","AddCustQuotation","QMD001",appException.Message);
				throw new ApplicationException("Connection to database failed",null);
			}
			if(conApp == null)
			{
				Logger.LogTraceError("QuotationMgrDAL","AddCustQuotation","QMD002","conApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			IDbTransaction transactionApp = dbCon.BeginTransaction(conApp,IsolationLevel.ReadCommitted);	
			if(transactionApp == null)
			{
				Logger.LogTraceError("QuotationMgrDAL","AddCustQuotation","ERR004","transactionApp is null");
				throw new ApplicationException("Failed Begining a App Transaction..",null);
			}
			try
			{				
					//Insert into Customer_Quotation table..
					DataRow drEach = dsCustQuotation.Tables[0].Rows[0]; 
					strQry ="Insert into Customer_quotation(applicationid,enterpriseid,custid,quotation_no,quotation_version,";
					strQry = strQry+"band_code,quotation_date,salesmanid,attention_to_person,copy_to_person,remark,quotation_status)";
					strQry =strQry+"values ('"+appid+"','"+enterpriseid+"',";

					if((drEach["custid"].ToString()!= "") && (drEach["custid"]!=null))
						strQry = strQry+"'"+(string)drEach["custid"]+"',";
					else
						strQry = strQry+"null,";
					if((drEach["quotation_no"].ToString()!="") && (drEach["quotation_no"]!=null))
						strQry = strQry+"'"+drEach["quotation_no"].ToString()+"',";
					else
						strQry = strQry+"null,";
					if((drEach["quotation_version"].ToString()!="") && (drEach["quotation_version"]!=null))
						strQry = strQry+System.Convert.ToInt64(drEach["quotation_version"]).ToString()+",";
					else
						strQry = strQry+"null,";
					if((drEach["band_code"].ToString()!="") && (drEach["band_code"]!=null))
						strQry = strQry+"'"+Utility.ReplaceSingleQuote(drEach["band_code"].ToString())+"',";
					else
						strQry = strQry+"null,";
					if((drEach["quotation_date"].ToString()!="") && (drEach["quotation_date"]!=null))
					{
						DateTime dtquotation_date = Convert.ToDateTime(drEach["quotation_date"]);
						String strdtquotation_date = Utility.DateFormat(appid,enterpriseid,dtquotation_date,DTFormat.DateTime);
						strQry=strQry+""+strdtquotation_date+""+",";
					}
					else
						strQry = strQry+"null,";
					if((drEach["salesmanid"].ToString()!="") && (drEach["salesmanid"]!=null))
						strQry = strQry+"'"+Utility.ReplaceSingleQuote(drEach["salesmanid"].ToString())+"',";
					else
						strQry = strQry+"null,";
					if((drEach["attention_to_person"].ToString()!="") && (drEach["attention_to_person"]!=null))
						strQry = strQry+"N'"+Utility.ReplaceSingleQuote(drEach["attention_to_person"].ToString())+"',";
					else
						strQry = strQry+"null,";
					if((drEach["copy_to_person"].ToString()!="") && (drEach["copy_to_person"]!=null))
						strQry = strQry+"N'"+Utility.ReplaceSingleQuote(drEach["copy_to_person"].ToString())+"',";
					else
						strQry = strQry+"null,";
					if((drEach["remark"].ToString()!="") && (drEach["remark"]!=null))
						strQry = strQry+"N'"+Utility.ReplaceSingleQuote(drEach["remark"].ToString())+"',";
					else
						strQry = strQry+"null,";
					if((drEach["quotation_status"].ToString()!="") && (drEach["quotation_status"]!=null))
						strQry = strQry+"'"+drEach["quotation_status"]+"'"+")";
					else
						strQry = strQry+ "'D')";				
					
					IDbCommand  dbCmd= dbCon.CreateCommand(strQry,CommandType.Text);
					dbCmd.Connection = conApp;
					dbCmd.Transaction = transactionApp;
					iRowsEffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
					Logger.LogTraceInfo("QuotationMgrDAL","AddCustQuoatation","QMD005",iRowsEffected + " rows inserted into customer_quotation table");
			

				//Inserting the data in the Quotation_VAS table
				cnt = dsVAS.Tables[0].Rows.Count;
				i = 0;
				for(i=0;i<cnt;i++)
				{
					DataRow drEachVAS = dsVAS.Tables[0].Rows[i];
					String strVASCode = "";
					string strQryVas=null;
					
					if(drEachVAS["vas_code"].ToString() != "")
						strVASCode = (String)drEachVAS["vas_code"];

					decimal decSurcharge = (decimal)drEachVAS["surcharge"];
					string strVASDesc=drEachVAS["vas_description"].ToString(); 

					strQryVas = ("insert into customer_quotation_vas (applicationid,enterpriseid,custid,quotation_no,quotation_version,vas_code,surcharge)values('");
					strQryVas=strQryVas+appid+"','"+enterpriseid+"',";

					if((drEach["custid"].ToString() !="") && (drEach["custid"]!=null))						
						strQryVas = strQryVas+"'"+(string)drEach["custid"]+"',";
					else
						strQryVas = strQryVas+"null,";
					if((drEach["quotation_no"].ToString()!="") && (drEach["quotation_no"]!=null))
						strQryVas = strQryVas+"'"+drEach["quotation_no"].ToString()+"',";
					else
						strQryVas = strQryVas+"null,";				
					if((drEach["quotation_version"].ToString()!="") && (drEach["quotation_version"]!=null))
						strQryVas = strQryVas+System.Convert.ToInt64(drEach["quotation_version"]).ToString()+",";
					else
						strQryVas = strQryVas+"null,";					
					if((drEachVAS["vas_code"].ToString()!="") && (drEachVAS["vas_code"]!=null))
						strQryVas = strQryVas+"'"+Utility.ReplaceSingleQuote(drEachVAS["vas_code"].ToString())+"',";
					else
						strQryVas = strQryVas+"null,";
					if((drEachVAS["surcharge"].ToString()!="") && (drEachVAS["surcharge"]!=null))
						strQryVas = strQryVas+(decimal)drEachVAS["surcharge"]+")";
					else
						strQryVas = strQryVas+"null,";
					
					dbCmd.CommandText=strQryVas; 
					iRowsEffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
					Logger.LogTraceInfo("QuotationMgrDAL","AddCustQuoatation","QMD007",iRowsEffected + " rows inserted into customer_vas table");
				}

				//Inserting the data in the Quotation_Service table
				cnt = dsService.Tables[0].Rows.Count;
				i = 0;
				for(i=0;i<cnt;i++)
				{
					DataRow drEachService = dsService.Tables[0].Rows[i];
					String strServiceCode = "";
					string strQryService=null;
					
					if(drEachService["service_code"].ToString() != "")
						strServiceCode = (String)drEachService["service_code"];

					decimal decSurcharge = (decimal)drEachService["service_charge_percent"];
					string strServiceDesc=drEachService["service_description"].ToString(); 

					strQryService =  " insert into customer_quotation_service (applicationid,enterpriseid,custid,quotation_no,";
					strQryService += " quotation_version,service_code,service_charge_percent,service_charge_amt)values('";
					strQryService += appid+"','"+enterpriseid+"',";
					if((drEach["custid"].ToString() !="") && (drEach["custid"]!=null))
						strQryService += "'"+(string)drEach["custid"]+"',";
					else
						strQryService += "null,";
					if((drEach["quotation_no"].ToString()!="") && (drEach["quotation_no"]!=null))
						strQryService += "'"+drEach["quotation_no"].ToString()+"',";
					else
						strQryService += "null,";				
					if((drEach["quotation_version"].ToString()!="") && (drEach["quotation_version"]!=null))
						strQryService += System.Convert.ToInt64(drEach["quotation_version"]).ToString()+",";
					else
						strQryService += "null,";					
					if((drEachService["service_code"].ToString()!="") && (drEachService["service_code"]!=null))
						strQryService += "'"+Utility.ReplaceSingleQuote(drEachService["service_code"].ToString()) +"',";
					else
						strQryService += "null,";
					if((drEachService["service_charge_percent"].ToString()!="") && (drEachService["service_charge_percent"]!=null))
						strQryService += (decimal)drEachService["service_charge_percent"]+",";
					else
						strQryService += "null,";
					if((drEachService["service_charge_amt"].ToString()!="") && (drEachService["service_charge_amt"]!=null))
						strQryService += (decimal)drEachService["service_charge_amt"]+")";
					else
						strQryService += "null)";
					
					dbCmd.CommandText=strQryService; 
					iRowsEffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
					Logger.LogTraceInfo("QuotationMgrDAL","AddCustQuoatation","QMD007",iRowsEffected + " rows inserted into customer_vas table");
				}
				transactionApp.Commit();
				Logger.LogTraceInfo("QuotationMgrDAL","AddCustQuoatation","QMD010","App db insert transaction committed.");
			}	
			catch(ApplicationException appException)
			{
				try
				{
					transactionApp.Rollback();
					Logger.LogTraceInfo("QuotationMgrDAL","AddCustQuoatation","QMD011","App db insert transaction rolled back.");
				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("QuotationMgrDAL","AddCustQuoatation","QMD012","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}
				Logger.LogTraceError("QuotationMgrDAL","AddCustQuoatation","QMD013","Insert Transaction Failed : "+ appException.Message.ToString());
				throw new ApplicationException("Error adding Customer ",appException);
			}
			catch(Exception exception)
			{
				try
				{
					transactionApp.Rollback();
					Logger.LogTraceInfo("QuotationMgrDAL","AddCustQuoatation","QMD014","App db insert transaction rolled back.");
				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("QuotationMgrDAL","AddCustQuoatation","QMD015","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}

				Logger.LogTraceError("QuotationMgrDAL","AddCustQuoatation","QMD016","Insert Transaction Failed : "+ exception.Message.ToString());
				throw new ApplicationException("Error adding Customer ",exception);
			}
			finally
			{
				if(conApp != null)
					conApp.Close();
			}
			return iRowsEffected;
		}

		public static int  AddAgentQuotation(string appid,string enterpriseid,DataSet dsAgentQuotation,DataSet dsVAS, DataSet dsService)
		{
			string strQry =null;
			int iRowsEffected=0;
			DbConnection dbCon=null;
			IDbConnection conApp =null;
				
			int cnt = 0;
			int i = 0;
			
			dbCon = DbConnectionManager.GetInstance().GetDbConnection(appid,enterpriseid);
			
			if(dbCon==null)
			{
				Logger.LogTraceError("QuotationMgrDAL","AddAgentQuotation","QMD001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			
			try
			{
				conApp = dbCon.GetConnection();
				dbCon.OpenConnection(ref conApp); 
			}
			catch(System.ApplicationException appException)
			{	
				if (conApp!=null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("QuotationMgrDAL","AddAgentQuotation","QMD001",appException.Message);
				throw new ApplicationException("Connection to database failed",null);
			}
			if(conApp == null)
			{
				Logger.LogTraceError("QuotationMgrDAL","AddAgentQuotation","QMD002","conApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			IDbTransaction transactionApp = dbCon.BeginTransaction(conApp,IsolationLevel.ReadCommitted);	
			if(transactionApp == null)
			{
				Logger.LogTraceError("QuotationMgrDAL","AddAgentQuotation","ERR004","transactionApp is null");
				throw new ApplicationException("Failed Begining a App Transaction..",null);
			}
			try
			{
				//Insert into Customer_Quotation table..
				DataRow drEach = dsAgentQuotation.Tables[0].Rows[0]; 
				strQry ="Insert into Agent_quotation(applicationid,enterpriseid,agentid,quotation_no,quotation_version,";
				strQry +=" band_code,quotation_date,salesmanid,attention_to_person,copy_to_person,remark,quotation_status)";
				strQry =strQry+"values ('"+appid+"','"+enterpriseid+"',";

				if((drEach["custid"].ToString()!= "") && (drEach["custid"]!=null))					
					strQry = strQry+"'"+(string)drEach["custid"]+"',";
				else
					strQry = strQry+"null,";
				if((drEach["quotation_no"].ToString()!="") && (drEach["quotation_no"]!=null))
					strQry = strQry+"'"+drEach["quotation_no"].ToString()+"',";
				else
					strQry = strQry+"null,";
				if((drEach["quotation_version"].ToString()!="") && (drEach["quotation_version"]!=null))
					strQry = strQry+System.Convert.ToInt64(drEach["quotation_version"]).ToString()+",";
				else
					strQry = strQry+"null,";
				if((drEach["band_code"].ToString()!="") && (drEach["band_code"]!=null))
					strQry = strQry+"'"+Utility.ReplaceSingleQuote(drEach["band_code"].ToString())+"',";
				else
					strQry = strQry+"null,";
				if((drEach["quotation_date"].ToString()!="") && (drEach["quotation_date"]!=null))
				{
					DateTime dtquotation_date = Convert.ToDateTime(drEach["quotation_date"]);
					String strdtquotation_date = Utility.DateFormat(appid,enterpriseid,dtquotation_date,DTFormat.DateTime);
					strQry=strQry+""+strdtquotation_date+""+",";
				}
				else
					strQry = strQry+"null,";
				if((drEach["salesmanid"].ToString()!="") && (drEach["salesmanid"]!=null))
					strQry = strQry+"'"+Utility.ReplaceSingleQuote(drEach["salesmanid"].ToString())+"',";
				else
					strQry = strQry+"null,";
				if((drEach["attention_to_person"].ToString()!="") && (drEach["attention_to_person"]!=null))
					strQry = strQry+"N'"+Utility.ReplaceSingleQuote(drEach["attention_to_person"].ToString())+"',";
				else
					strQry = strQry+"null,";
				if((drEach["copy_to_person"].ToString()!="") && (drEach["copy_to_person"]!=null))
					strQry = strQry+"N'"+Utility.ReplaceSingleQuote(drEach["copy_to_person"].ToString())+"',";
				else
					strQry = strQry+"null,";
				if((drEach["remark"].ToString()!="") && (drEach["remark"]!=null))
					strQry = strQry+"N'"+Utility.ReplaceSingleQuote(drEach["remark"].ToString())+"',";
				else
					strQry = strQry+"null,";
				if((drEach["quotation_status"].ToString()!="") && (drEach["quotation_status"]!=null))
					strQry = strQry+"'"+drEach["quotation_status"]+"'"+")";
				else
					strQry = strQry+"'D')"+"";
				
				IDbCommand  dbCmd= dbCon.CreateCommand(strQry,CommandType.Text);
				dbCmd.Connection = conApp;
				dbCmd.Transaction = transactionApp;
				iRowsEffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
				Logger.LogTraceInfo("QuotationMgrDAL","AddAgentQuotation","QMD005",iRowsEffected + " rows inserted into customer_quotation table");

				//Inserting the data in the Quotation_VAS table
				cnt = dsVAS.Tables[0].Rows.Count;
				i = 0;
				for(i=0;i<cnt;i++)
				{
					DataRow drEachVAS = dsVAS.Tables[0].Rows[i];
					String strVASCode = "";
					string strQryVas=null;
					
					if(drEachVAS["vas_code"].ToString() != "")
					{
						strVASCode = (String)drEachVAS["vas_code"];
					}
					decimal decSurcharge = (decimal)drEachVAS["surcharge"];
					string strVASDesc=drEachVAS["vas_description"].ToString(); 

					strQryVas = ("insert into agent_quotation_vas (applicationid,enterpriseid,agentid,quotation_no,quotation_version,vas_code,surcharge)values('");
					strQryVas=strQryVas+appid+"',";
					strQryVas=strQryVas+"'"+enterpriseid+"',";
					if((drEach["custid"].ToString() !="") && (drEach["custid"]!=null))
						strQryVas = strQryVas+"'"+(string)drEach["custid"]+"',";
					else
						strQryVas = strQryVas+"null,";
					if((drEach["quotation_no"].ToString()!="") && (drEach["quotation_no"]!=null))
						strQryVas = strQryVas+"'"+drEach["quotation_no"].ToString()+"',";
					else
						strQryVas = strQryVas+"null,";				
					if((drEach["quotation_version"].ToString()!="") && (drEach["quotation_version"]!=null))
						strQryVas = strQryVas+System.Convert.ToInt64(drEach["quotation_version"]).ToString()+",";
					else
						strQryVas = strQryVas+"null,";
					
					if((drEachVAS["vas_code"].ToString()!="") && (drEachVAS["vas_code"]!=null))
						strQryVas = strQryVas+"'"+Utility.ReplaceSingleQuote(drEachVAS["vas_code"].ToString()) +"',";
					else
						strQryVas = strQryVas+"null,";
					if((drEachVAS["surcharge"].ToString()!="") && (drEachVAS["surcharge"]!=null))
						strQryVas = strQryVas+(decimal)drEachVAS["surcharge"]+")";
					else
						strQryVas = strQryVas+"null,";
					
					dbCmd.CommandText=strQryVas; 
					iRowsEffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
					Logger.LogTraceInfo("QuotationMgrDAL","AddAgentQuotation","QMD007",iRowsEffected + " rows inserted into customer_vas table");
				}
				//Inserting the data in the Quotation_Service table
				cnt = dsService.Tables[0].Rows.Count;
				i = 0;
				for(i=0;i<cnt;i++)
				{
					DataRow drEachService = dsService.Tables[0].Rows[i];
					String strServiceCode = "";
					string strQryService=null;
					
					if(drEachService["service_code"].ToString() != "")
					{
						strServiceCode = (String)drEachService["service_code"];
					}
					decimal decSurcharge = (decimal)drEachService["service_charge_percent"];
					string strServiceDesc=drEachService["service_description"].ToString(); 

					strQryService = " insert into agent_quotation_service (applicationid,enterpriseid,agentid,quotation_no,quotation_version,";
					strQryService += " service_code,service_charge_percent,service_charge_amt)values('"+appid+"','"+enterpriseid+"',";

					if((drEach["custid"].ToString() !="") && (drEach["custid"]!=null))						
						strQryService = strQryService+"'"+(string)drEach["custid"]+"',";
					else
						strQryService = strQryService+"null,";
					if((drEach["quotation_no"].ToString()!="") && (drEach["quotation_no"]!=null))
						strQryService = strQryService+"'"+drEach["quotation_no"].ToString()+"',";
					else
						strQryService = strQryService+"null,";				
					if((drEach["quotation_version"].ToString()!="") && (drEach["quotation_version"]!=null))
						strQryService = strQryService+System.Convert.ToInt64(drEach["quotation_version"]).ToString()+",";
					else
						strQryService = strQryService+"null,";					
					if((drEachService["service_code"].ToString()!="") && (drEachService["service_code"]!=null))
						strQryService = strQryService+"'"+Utility.ReplaceSingleQuote(drEachService["service_code"].ToString())+"',";
					else
						strQryService = strQryService+"null,";
					if((drEachService["service_charge_percent"].ToString()!="") && (drEachService["service_charge_percent"]!=null))
						strQryService = strQryService+(decimal)drEachService["service_charge_percent"]+",";
					else
						strQryService = strQryService+"null,";
					if((drEachService["service_charge_amt"].ToString()!="") && (drEachService["service_charge_amt"]!=null))
						strQryService = strQryService+(decimal)drEachService["service_charge_amt"]+")";
					else
						strQryService = strQryService+"null,";
					
					dbCmd.CommandText=strQryService; 
					iRowsEffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
					Logger.LogTraceInfo("QuotationMgrDAL","AddAgentQuotation","QMD007",iRowsEffected + " rows inserted into agent_service table");
				}

				transactionApp.Commit();
				Logger.LogTraceInfo("QuotationMgrDAL","AddAgentQuotation","QMD010","App db insert transaction committed.");
			}	
			catch(ApplicationException appException)
			{
				try
				{
					transactionApp.Rollback();
					Logger.LogTraceInfo("QuotationMgrDAL","AddAgentQuotation","QMD011","App db insert transaction rolled back.");

				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("QuotationMgrDAL","AddAgentQuotation","QMD012","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}
				Logger.LogTraceError("QuotationMgrDAL","AddAgentQuotation","QMD013","Insert Transaction Failed : "+ appException.Message.ToString());
				throw new ApplicationException("Error adding Customer ",appException);
			}
			catch(Exception exception)
			{
				try
				{
					transactionApp.Rollback();
					Logger.LogTraceInfo("QuotationMgrDAL","AddAgentQuotation","QMD014","App db insert transaction rolled back.");

				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("QuotationMgrDAL","AddAgentQuotation","QMD015","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}
				Logger.LogTraceError("QuotationMgrDAL","AddAgentQuotation","QMD016","Insert Transaction Failed : "+ exception.Message.ToString());
				throw new ApplicationException("Error adding Customer ",exception);
			}
			finally
			{
				if(conApp != null)
					conApp.Close();
			}

			return iRowsEffected;
		}

		public static int  DeleteCustQuotation(string appid,string enterpriseid,string custid,string quotation_no,int quotation_version,String strType)
		{
			String strQryDel=null;
			DbConnection dbCon=null;
			IDbCommand dbCmd = null;
			IDbConnection conApp=null;
			IDbTransaction TranDel=null;
			int iRowsAffected=0;

			dbCon = DbConnectionManager.GetInstance().GetDbConnection(appid,enterpriseid);
    
			if(dbCon ==null)
			{
				Logger.LogTraceError("QuotationMgrDAL","DeleteCustQuoatation","QBD001","dbConnection failed");
				throw new ApplicationException("Connection in the Database failed"); 
			}
			try
			{
				conApp = dbCon.GetConnection();
				dbCon.OpenConnection(ref conApp);  
			}
			catch(System.ApplicationException appException) 
			{
				if(conApp != null)
				{
					conApp.Close(); 
				}
				Logger.LogTraceError("QuotationMgrDAL","DeleteCustQuoatation","QBD002",appException.Message);
				throw new ApplicationException("Connection to database failed"); 
			}
			TranDel = dbCon.BeginTransaction(conApp,IsolationLevel.ReadCommitted); 
			try
			{
				
//			//Delete from quotation_vas..
//				if(strType=="C")
//				{
//					strQryDel =" Delete from customer_quotation_vas where";
//
//				}
//				if(strType=="A")
//				{
//					strQryDel =" Delete from agent_quotation_vas where";
//				}
//				strQryDel = strQryDel+" applicationid = " +"'" +appid +"'" +" and enterpriseid = " +"'"+ enterpriseid +"'";
//				if(strType=="C")
//				{
//					strQryDel = strQryDel+" and custid =" + "'" + custid + "'" +" and quotation_no="+"'"+quotation_no+"'";
//				
//				}
//				if(strType=="A")
//				{
//					strQryDel = strQryDel+" and agentid =" + "'" + custid + "'" +" and quotation_no="+"'"+quotation_no+"'";
//				
//				}
//				strQryDel = strQryDel+" and quotation_version="+quotation_version;
//
//				dbCmd = dbCon.CreateCommand(strQryDel,CommandType.Text);
//				dbCmd.Connection = conApp;
//				dbCmd.Transaction = TranDel;
//				int iRowAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd); 
//  
//			//Delete from quotation_service..
//				if(strType=="C")
//				{
//					strQryDel =" Delete from customer_quotation_service where";
//				}
//				if(strType=="A")
//				{
//					strQryDel =" Delete from agent_quotation_service where";
//				}
//				strQryDel = strQryDel+" applicationid = " +"'" +appid +"'" +" and enterpriseid = " +"'"+ enterpriseid +"'";
//				if(strType=="C")
//				{
//					strQryDel = strQryDel+" and custid =" + "'" + custid + "'" +" and quotation_no="+"'"+quotation_no+"'";
//				
//				}
//				if(strType=="A")
//				{
//					strQryDel = strQryDel+" and agentid =" + "'" + custid + "'" +" and quotation_no="+"'"+quotation_no+"'";
//				
//				}
//				strQryDel = strQryDel+" and quotation_version="+quotation_version;
//
//				dbCmd = dbCon.CreateCommand(strQryDel,CommandType.Text);
//				dbCmd.Connection = conApp;
//				dbCmd.Transaction = TranDel;
//				iRowAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);   
				

				//Delete from quotation..
				if(strType=="C")
					strQryDel =" update customer_quotation set quotation_status='C' where";
				if(strType=="A")
					strQryDel =" update agent_quotation set quotation_status='C' where";
				
				strQryDel = strQryDel+" applicationid = " +"'" +appid +"'" +" and enterpriseid = " +"'"+ enterpriseid +"'";
				if(strType=="C")
					strQryDel = strQryDel+" and custid =" + "'" + custid + "'" +" and quotation_no="+"'"+quotation_no+"'";
				if(strType=="A")
					strQryDel = strQryDel+" and agentid =" + "'" + custid + "'" +" and quotation_no="+"'"+quotation_no+"'";
				
				strQryDel = strQryDel+" and quotation_version="+quotation_version;

				dbCmd = dbCon.CreateCommand(strQryDel,CommandType.Text);
				dbCmd.Connection = conApp;
				dbCmd.Transaction = TranDel;
				iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);  

				TranDel.Commit();
			}
			catch(System.ApplicationException appException)
			{ 
				TranDel.Rollback();
				Logger.LogTraceError("PRBMgrDAL","DeletePickupRequest","PRB005",appException.Message);
				throw new ApplicationException("Delete not successful");
			}
			finally
			{
				if(conApp != null)
				{
					conApp.Close(); 
				}
			}
			return iRowsAffected;
	
		}

		public static SessionDS GetCustQuotation(string appid, string enterpriseid, int iCurrent, int iDSRecSize,DataSet dsQueryParam,string strCustType)
		{
			SessionDS sessionDS = new SessionDS();
			DataProviderType dbProvider = DbConnectionManager.GetInstance().GetDbProviderType(appid,enterpriseid);
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(appid,enterpriseid);  
			
			string strcustid = null;
			string strquotation_no=null;
			string strquotation_version=null;
			string strband_code=null;
			string strquotation_date=null;
			string strsalesmanid=null;
			string strattention_to_person=null;
			string strcopy_to_person=null;
			string strquotation_status=null;
			string strremark=null;
			if(dbCon==null)
			{
				Logger.LogTraceError("QuotationMgrDAL","GetCustQuotation","QBD001","DbConnectionis null!!");
				return sessionDS;
			}

			// Populating records from Customer Quotation..	

			StringBuilder strQryCust = new StringBuilder();
			DataRow drEach = dsQueryParam.Tables[0].Rows[0];
			
			strQryCust.Append(" select  b.custid,b.cust_name,a.quotation_no,a.quotation_version, a.band_code, a.quotation_date,");
			strQryCust.Append(" a.salesmanid,a.attention_to_person, a.copy_to_person, a.remark, a.quotation_status, 'C' as type, ");
			strQryCust.Append(" c.salesman_name from customer_quotation a, customer b, salesman c where a.custid = b.custid and ");
			strQryCust.Append(" a.applicationid = b.applicationid and a.applicationid = '"+appid+"' and a.enterpriseid = b.enterpriseid ");
			strQryCust.Append(" and a.enterpriseid = '"+enterpriseid+"'");

			switch (dbProvider)
			{
				case DataProviderType.Sql :
					strQryCust.Append(" AND a.salesmanid *= c.salesmanid "); 
					break;	
			
				case DataProviderType.Oracle:
					strQryCust.Append(" AND a.salesmanid (+)= c.salesmanid "); 			
					break;

				case DataProviderType.Oledb:
					break;
			}			
		
			strcustid = drEach["custid"].ToString() ;
			if ((strcustid !=null)&&(strcustid !=""))
				strQryCust.Append(" and  b.custid ='"+Utility.ReplaceSingleQuote(strcustid)+"'"); 								

			strquotation_no = drEach["quotation_no"].ToString() ;
			if ((strquotation_no !=null)&&(strquotation_no !=""))
				strQryCust.Append(" and  a.quotation_no ='"+Utility.ReplaceSingleQuote(strquotation_no)+"'");  

			strquotation_version = drEach["quotation_version"].ToString() ;
			if ((strquotation_version !=null)&&(strquotation_version !=""))
				strQryCust.Append(" and  a.quotation_version ="+Utility.ReplaceSingleQuote(strquotation_version));

			strband_code = drEach["band_code"].ToString() ;
			if ((strband_code !=null)&&(strband_code !=""))
				strQryCust.Append(" and  a.band_code ='"+Utility.ReplaceSingleQuote(strband_code)+"'"); 

			strquotation_date = drEach["quotation_date"].ToString() ;
			if ((strquotation_date !=null)&&(strquotation_date !=""))
			{
				DateTime dtquotation_date = Convert.ToDateTime( drEach["quotation_date"]);
				strQryCust.Append(" and  a.quotation_date = ");
				strquotation_date = Utility.DateFormat(appid,enterpriseid,dtquotation_date,DTFormat.DateTime);
			
				strQryCust.Append(strquotation_date);
				strQryCust.Append ("");
			}
			
			strsalesmanid =drEach["salesmanid"].ToString(); 
			if((strsalesmanid !=null) && (strsalesmanid !=""))
				strQryCust.Append(" and  a.salesmanid = '"+Utility.ReplaceSingleQuote(strsalesmanid)+"'"); 

			strattention_to_person =drEach["attention_to_person"].ToString(); 
			if((strattention_to_person !=null) && (strattention_to_person !=""))
				strQryCust.Append(" and  a.attention_to_person = N'"+Utility.ReplaceSingleQuote(strattention_to_person)+"'"); 

			strcopy_to_person =drEach["copy_to_person"].ToString(); 
			if((strcopy_to_person !=null) && (strcopy_to_person !=""))
				strQryCust.Append(" and  a.copy_to_person = N'"+Utility.ReplaceSingleQuote(strcopy_to_person)+"'"); 

			strquotation_status =drEach["quotation_status"].ToString(); 
			if((strquotation_status !=null) && (strquotation_status !=""))
				strQryCust.Append(" and  a.quotation_status = '"+strquotation_status+"'");

			strremark = drEach["remark"].ToString();
			if((strremark !=null) && (strremark !=""))
				strQryCust.Append(" and  b.remark = N'"+Utility.ReplaceSingleQuote(strremark)+"'");

			// Populating records from Agent Quotation..	
			StringBuilder strQryAgent = new StringBuilder();			
			strQryAgent.Append("select a.agentid, b.agent_name as cust_name,a.quotation_no, a.quotation_version,a.band_code,a.quotation_date,a.salesmanid,a.attention_to_person,a.copy_to_person, a.remark, a.quotation_status, 'A' as type, c.salesman_name from agent_quotation a, Agent b, salesman c where a.agentid = b.agentid and a.applicationid = b.applicationid and a.applicationid = '");
			strQryAgent.Append(appid+"' and a.enterpriseid = b.enterpriseid and a.enterpriseid = '"+enterpriseid+"'");
			
			switch (dbProvider)
			{
				case DataProviderType.Sql :
					strQryAgent.Append(" AND a.salesmanid *= c.salesmanid "); 
					break;	
			
				case DataProviderType.Oracle:
					strQryAgent.Append(" AND a.salesmanid (+)= c.salesmanid "); 			
					break;

				case DataProviderType.Oledb:
					break;
			}			
			
			strcustid = drEach["custid"].ToString() ;
			if ((strcustid !=null)&&(strcustid !=""))
				strQryAgent.Append(" and  a.agentid ='"+Utility.ReplaceSingleQuote(strcustid)+"'");

			strquotation_no = drEach["quotation_no"].ToString() ;
			if ((strquotation_no !=null)&&(strquotation_no !=""))
				strQryAgent.Append(" and  a.quotation_no ='"+Utility.ReplaceSingleQuote(strquotation_no)+"'");

			strquotation_version = drEach["quotation_version"].ToString() ;
			if ((strquotation_version !=null)&&(strquotation_version !=""))
				strQryAgent.Append(" and  a.quotation_version ="+Utility.ReplaceSingleQuote(strquotation_version));
			
			strband_code = drEach["band_code"].ToString() ;
			if ((strband_code !=null)&&(strband_code !=""))
				strQryAgent.Append(" and  a.band_code ='"+Utility.ReplaceSingleQuote(strband_code)+"'");
				
			strquotation_date = drEach["quotation_date"].ToString() ;
			if ((strquotation_date !=null)&&(strquotation_date !=""))
			{
				DateTime dtquotation_date = Convert.ToDateTime( drEach["quotation_date"]);
				strQryAgent.Append(" and  a.quotation_date = ");
				strquotation_date = Utility.DateFormat(appid,enterpriseid,dtquotation_date,DTFormat.DateTime);
				
				strQryAgent.Append(strquotation_date);
				strQryAgent.Append ("");
			}

			strsalesmanid =drEach["salesmanid"].ToString(); 
			if((strsalesmanid !=null) && (strsalesmanid !=""))
				strQryCust.Append(" and  a.salesmanid = '"+Utility.ReplaceSingleQuote(strsalesmanid)+"'");

			strattention_to_person =drEach["attention_to_person"].ToString(); 
			if((strattention_to_person !=null) && (strattention_to_person !=""))
				strQryAgent.Append(" and  a.attention_to_person = N'"+Utility.ReplaceSingleQuote(strattention_to_person)+"'"); 

			strcopy_to_person =drEach["copy_to_person"].ToString(); 
			if((strcopy_to_person !=null) && (strcopy_to_person !=""))			
				strQryAgent.Append(" and  a.copy_to_person = N'"+Utility.ReplaceSingleQuote(strcopy_to_person)+"'");
							
			strquotation_status =drEach["quotation_status"].ToString(); 
			if((strquotation_status !=null) && (strquotation_status !=""))
				strQryAgent.Append(" and  a.quotation_status = '"+strquotation_status+"'");

			strremark = drEach["remark"].ToString();
			if((strremark !=null) && (strremark !=""))
				strQryAgent.Append(" and  b.remark = N'"+Utility.ReplaceSingleQuote(strremark)+"'");

			StringBuilder strUnionString = new StringBuilder();
			strUnionString.Append (" Union ");			
			string strUnionQry= strQryCust.ToString()+strUnionString.ToString()+strQryAgent.ToString();			
			if(strCustType.Equals("C"))
			{
				sessionDS = dbCon.ExecuteQuery(strQryCust.ToString()+" ORDER BY quotation_no desc" ,iCurrent,iDSRecSize,"Customer_Quotation");
				decimal x = sessionDS.QueryResultMaxSize; 
			}
			else if(strCustType.Equals("A"))
			{
				sessionDS = dbCon.ExecuteQuery(strQryAgent.ToString()+" ORDER BY quotation_no desc" ,iCurrent,iDSRecSize,"Customer_Quotation");
				decimal x = sessionDS.QueryResultMaxSize; 
			}
			else if(strCustType.Equals("B"))
			{
				sessionDS = dbCon.ExecuteQuery(strUnionQry.ToString()+" ORDER BY quotation_no desc",iCurrent,iDSRecSize,"Customer_Quotation");
				decimal x = sessionDS.QueryResultMaxSize; 
			}
			else
			{				
				sessionDS = dbCon.ExecuteQuery(strUnionQry.ToString()+" ORDER BY quotation_no desc",iCurrent,iDSRecSize,"Customer_Quotation");
			}

			return sessionDS;
		}
		public static SessionDS GetCustVas(string appid,string enterpriseid,string custid,string quotation_no,decimal quotation_version, int recStart, int recSize)
		{
			DbConnection dbCon= null;
			SessionDS sdsVAS =null;
			IDbCommand dbcmd=null;
			DataSet dsVASData = null;

			dbCon = DbConnectionManager.GetInstance().GetDbConnection(appid,enterpriseid);

			if (dbCon==null)
			{
				Logger.LogTraceError("QuotationMgrDAL.cs","GetCustVas","QBD001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			StringBuilder strQry =  new StringBuilder(); 
			strQry = strQry.Append("select a.vas_code,a.surcharge,b.vas_description  from customer_quotation_vas a,vas b");
			strQry.Append(" where a.applicationid = '"+appid+"' and a.enterpriseid = '"+enterpriseid+"' and a.custid = '");
			strQry.Append(custid+"' and a.quotation_no = '"+quotation_no+"' and a.quotation_version = "+quotation_version);
			strQry.Append (" and b.vas_code = a.vas_code");			
			
			dbcmd = dbCon.CreateCommand(strQry.ToString(),CommandType.Text); 
			try
			{
				sdsVAS = dbCon.ExecuteQuery(strQry.ToString(),recStart,1000,"BandQuotation");				
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("QuotationMgrDAL.cs","GetCustVas","QBD002","Error in Query String");
				throw appException;
			}
			
//			dsVASData = GetEmptyVASDS();
//
//			if(dsVAS.Tables[0].Rows.Count > 0)
//			{
//				int i = 0;
//				for(i = 0;i<dsVAS.Tables[0].Rows.Count;i++)
//				{
//					DataRow drVAS = dsVAS.Tables[0].Rows[i];
//					AddNewRowInVAS(dsVASData);
//					DataRow drEach = dsVASData.Tables[0].Rows[i];
//					
//					if(!drVAS["vas_code"].GetType().Equals(System.Type.GetType("System.DBNull")))
//					{
//						drEach["vas_code"] = drVAS["vas_code"];
//					}
//					if(!drVAS["vas_description"].GetType().Equals(System.Type.GetType("System.DBNull")))
//					{
//						drEach["vas_description"] = drVAS["vas_description"];
//					}
//					if(!drVAS["surcharge"].GetType().Equals(System.Type.GetType("System.DBNull")))
//					{
//						drEach["surcharge"] = drVAS["surcharge"];
//					}
//				}
//			}
//			return dsVASData;
			return sdsVAS;
		}

		public static DataSet GetCustService(string appid,string enterpriseid,string custid,string quotation_no,decimal quotation_version)
		{
			DbConnection dbCon= null;
			DataSet dsService =null;
			IDbCommand dbcmd=null;
			DataSet dsServiceData = null;

			dbCon = DbConnectionManager.GetInstance().GetDbConnection(appid,enterpriseid);

			if (dbCon==null)
			{
				Logger.LogTraceError("QuotationMgrDAL.cs","GetCustService","QBD001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			StringBuilder strQry =  new StringBuilder(); 
			strQry = strQry.Append("select a.service_code,a.service_charge_percent,a.service_charge_amt,");
			strQry.Append(" b.service_description from customer_quotation_service a,service b ");
			strQry.Append(" where a.applicationid = '"+appid+"' and a.enterpriseid = '"+enterpriseid);
			strQry.Append("' and a.custid = '"+custid+"' and a.quotation_no = '"+quotation_no+"' and a.quotation_version = ");
			strQry.Append (quotation_version+" and b.service_code = a.service_code");
						
			dbcmd = dbCon.CreateCommand(strQry.ToString(),CommandType.Text); 
			try
			{
				dsService = (DataSet)dbCon.ExecuteQuery(dbcmd,com.common.DAL.ReturnType.DataSetType);  
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("QuotationMgrDAL.cs","GetCustService","QBD002","Error in Query String");
				throw appException;
			}
			
			dsServiceData = GetEmptyServiceDS();
			if(dsService.Tables[0].Rows.Count > 0)
			{
				int i = 0;
				for(i = 0;i<dsService.Tables[0].Rows.Count;i++)
				{
					DataRow drService = dsService.Tables[0].Rows[i];
					AddNewRowInService(dsServiceData);
					DataRow drEach = dsServiceData.Tables[0].Rows[i];
					
					if(Utility.IsNotDBNull(drService["service_code"]))
					{
						drEach["service_code"] = drService["service_code"];
					}
					if(Utility.IsNotDBNull(drService["service_description"]) && drService["service_description"].ToString()!="")
					{
						drEach["service_description"] = drService["service_description"].ToString();
					}
					if(Utility.IsNotDBNull(drService["service_charge_percent"]))
					{
						drEach["service_charge_percent"] = drService["service_charge_percent"];
					}
					if(Utility.IsNotDBNull(drService["service_charge_amt"]))
					{
						drEach["service_charge_amt"] = drService["service_charge_amt"];
					}
				}
			}
			return dsServiceData;

		}
		public static DataSet GetAgentVas(string appid,string enterpriseid,string agentid,string quotation_no,decimal quotation_version)
		{
			DbConnection dbCon= null;
			DataSet dsVAS =null;
			IDbCommand dbcmd=null;
			DataSet dsVASData = null;

			dbCon = DbConnectionManager.GetInstance().GetDbConnection(appid,enterpriseid);

			if (dbCon==null)
			{
				Logger.LogTraceError("QuotationMgrDAL.cs","GetAgentVas","QBD001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			StringBuilder strQry =  new StringBuilder(); 
			strQry = strQry.Append("select a.vas_code,a.surcharge,b.vas_description  from agent_quotation_vas a,vas b");
			strQry.Append(" where a.applicationid = '"+appid+"' and a.enterpriseid = '"+enterpriseid);
			strQry.Append("' and a.agentid = '"+agentid+"' and a.quotation_no = '"+quotation_no+"' and a.quotation_version = ");
			strQry.Append(quotation_version+" and b.vas_code = a.vas_code");
						
			dbcmd = dbCon.CreateCommand(strQry.ToString(),CommandType.Text); 
			try
			{
				dsVAS = (DataSet)dbCon.ExecuteQuery(dbcmd,com.common.DAL.ReturnType.DataSetType);  
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("QuotationMgrDAL.cs","GetAgentVas","QBD002","Error in Query String");
				throw appException;
			}
			
			dsVASData = GetEmptyVASDS();
			if(dsVAS.Tables[0].Rows.Count > 0)
			{
				int i = 0;
				for(i = 0;i<dsVAS.Tables[0].Rows.Count;i++)
				{
					DataRow drVAS = dsVAS.Tables[0].Rows[i];
					AddNewRowInVAS(dsVASData);
					DataRow drEach = dsVASData.Tables[0].Rows[i];
					
					if(!drVAS["vas_code"].GetType().Equals(System.Type.GetType("System.DBNull")))
						drEach["vas_code"] = drVAS["vas_code"];
					if(!drVAS["vas_description"].GetType().Equals(System.Type.GetType("System.DBNull")))
						drEach["vas_description"] = drVAS["vas_description"];
					if(!drVAS["surcharge"].GetType().Equals(System.Type.GetType("System.DBNull")))
						drEach["surcharge"] = drVAS["surcharge"];
				}
			}
			return dsVASData;

		}
		public static DataSet GetAgentService(string appid,string enterpriseid,string agentid,string quotation_no,decimal quotation_version)
		{
			DbConnection dbCon= null;
			DataSet dsService =null;
			IDbCommand dbcmd=null;
			DataSet dsServiceData = null;

			dbCon = DbConnectionManager.GetInstance().GetDbConnection(appid,enterpriseid);

			if (dbCon==null)
			{
				Logger.LogTraceError("QuotationMgrDAL.cs","GetAgentService","QBD001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			StringBuilder strQry =  new StringBuilder(); 
			strQry = strQry.Append("select a.service_code,a.service_charge_amt,a.service_charge_percent,");
			strQry.Append(" b.service_description  from agent_quotation_service a,service b ");
			strQry.Append(" where a.applicationid = '"+appid+"' and a.enterpriseid = '"+enterpriseid);
			strQry.Append("' and a.agentid = '"+agentid+"' and a.quotation_no = '"+quotation_no);
			strQry.Append ("' and a.quotation_version = "+quotation_version+" and b.service_code = a.service_code ");
						
			dbcmd = dbCon.CreateCommand(strQry.ToString(),CommandType.Text); 
			try
			{
				dsService = (DataSet)dbCon.ExecuteQuery(dbcmd,com.common.DAL.ReturnType.DataSetType);  
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("QuotationMgrDAL.cs","GetAgentService","QBD002","Error in Query String");
				throw appException;
			}
			
			dsServiceData = GetEmptyServiceDS();

			if(dsService.Tables[0].Rows.Count > 0)
			{
				int i = 0;
				for(i = 0;i<dsService.Tables[0].Rows.Count;i++)
				{
					DataRow drService = dsService.Tables[0].Rows[i];
					AddNewRowInService(dsServiceData);
					DataRow drEach = dsServiceData.Tables[0].Rows[i];
					
					if(Utility.IsNotDBNull(drService["service_code"]))
						drEach["service_code"] = drService["service_code"];
					if(Utility.IsNotDBNull(drService["service_description"]))
						drEach["service_description"] = drService["service_description"];
					if(Utility.IsNotDBNull(drService["service_charge_percent"]))
						drEach["service_charge_percent"] = drService["service_charge_percent"];
					if(Utility.IsNotDBNull(drService["service_charge_amt"]))
						drEach["service_charge_amt"] = drService["service_charge_amt"];
				}
			}

			return dsServiceData;

		}

		public static int  UpdateCustQuotation(string appid,string enterpriseid,DataSet dsCustQuotation,DataSet dsVAS, DataSet dsService)
		{
				
			string strQry =null;
			string strQryVAS =null;
			int iRowsEffected=0;
			DbConnection dbCon=null;
			IDbConnection conApp =null;
			IDbCommand  dbCmd=null;
				
			int cnt = 0;
			int i = 0;
			
			dbCon = DbConnectionManager.GetInstance().GetDbConnection(appid,enterpriseid);
			
			if(dbCon==null)
			{
				Logger.LogTraceError("QuotationMgrDAL","UpdateCustQuotation","QMD001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			
			try
			{
				conApp = dbCon.GetConnection();
				dbCon.OpenConnection(ref conApp); 
			}
			catch(System.ApplicationException appException)
			{	
				if (conApp!=null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("QuotationMgrDAL","UpdateCustQuotation","QMD001",appException.Message);
				throw new ApplicationException("Connection to database failed",null);
			}
			if(conApp == null)
			{
				Logger.LogTraceError("QuotationMgrDAL","UpdateCustQuotation","QMD002","conApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			IDbTransaction transactionApp = dbCon.BeginTransaction(conApp,IsolationLevel.ReadCommitted);	
			if(transactionApp == null)
			{
				Logger.LogTraceError("QuotationMgrDAL","UpdateCustQuotation","ERR004","transactionApp is null");
				throw new ApplicationException("Failed Begining a App Transaction..",null);
			}
			try
			{
				//Insert into Customer_Quotation table..
				DataRow drEach = dsCustQuotation.Tables[0].Rows[0]; 
				strQry =strQry+"update customer_quotation set ";
				strQry = strQry+"band_code = ";
				if((drEach["band_code"].ToString()!="") && (drEach["band_code"]!=null))
					strQry = strQry+"'"+Utility.ReplaceSingleQuote(drEach["band_code"].ToString())+"',";
				else
					strQry = strQry+"null,";

				strQry = strQry+"quotation_date = ";
				if((drEach["quotation_date"].ToString()!="") && (drEach["quotation_date"]!=null))
				{
					DateTime dtquotation_date = Convert.ToDateTime(drEach["quotation_date"]);
					String strdtquotation_date = Utility.DateFormat(appid,enterpriseid,dtquotation_date,DTFormat.DateTime);
					strQry=strQry+""+strdtquotation_date+""+",";
				}
				else
				{
					strQry = strQry+"null,";
				}
				strQry = strQry+"salesmanid = ";
				if((drEach["salesmanid"].ToString()!="") && (drEach["salesmanid"]!=null))
					strQry = strQry+"'"+Utility.ReplaceSingleQuote(drEach["salesmanid"].ToString())+"',";
				else
					strQry = strQry+"null,";

				strQry = strQry+"attention_to_person = ";
				if((drEach["attention_to_person"].ToString()!="") && (drEach["attention_to_person"]!=null))
					strQry = strQry+"N'"+Utility.ReplaceSingleQuote(drEach["attention_to_person"].ToString())+"',";
				else
					strQry = strQry+"null,";

				strQry = strQry+"copy_to_person = ";

				if((drEach["copy_to_person"].ToString()!="") && (drEach["copy_to_person"]!=null))
					strQry = strQry+"N'"+Utility.ReplaceSingleQuote(drEach["copy_to_person"].ToString())+"',";
				else
					strQry = strQry+"null,";

				strQry = strQry+"remark = ";
				if((drEach["remark"].ToString()!="") && (drEach["remark"]!=null))
					strQry = strQry+"N'"+Utility.ReplaceSingleQuote(drEach["remark"].ToString())+"',";
				else
					strQry = strQry+"null,";

				strQry = strQry+"quotation_status = ";
				if((drEach["quotation_status"].ToString()!="") && (drEach["quotation_status"]!=null))
					strQry = strQry+"'"+drEach["quotation_status"]+"'";
				else
					strQry = strQry+ "'D'";
				
				strQry = strQry +" where  applicationid ='"+appid+"' and enterpriseid =";
				strQry = strQry +"'"+enterpriseid+"' and custid ='"+drEach["custid"]+"'";
				strQry = strQry +" and quotation_no ='"+drEach["quotation_no"]+"'";
				strQry = strQry +" and quotation_version ="+drEach["quotation_version"];

				dbCmd= dbCon.CreateCommand(strQry,CommandType.Text);
				dbCmd.Connection = conApp;
				dbCmd.Transaction = transactionApp;
				iRowsEffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
				Logger.LogTraceInfo("QuotationMgrDAL","AddCustQuoatation","QMD005",iRowsEffected + " rows inserted into customer_quotation table");

				///Does updating remark required???

//				strQry = "update customer set remark = ";
//				strQry = strQry +"'"+drEach["remark"].ToString()+"' ";
//				strQry = strQry +" where";
//				strQry = strQry +" applicationid =";
//				strQry = strQry +"'"+appid+"'";
//				strQry = strQry +" and enterpriseid =";
//				strQry = strQry +"'"+enterpriseid+"'";
//				strQry = strQry +" and custid =";
//				strQry = strQry +"'"+drEach["custid"]+"'";
//
//				dbCmd= dbCon.CreateCommand(strQry,CommandType.Text);
//				dbCmd.Connection = conApp;
//				dbCmd.Transaction = transactionApp;
//				iRowsEffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
//				Logger.LogTraceInfo("QuotationMgrDAL","AddCustQuoatation","QMD005",iRowsEffected + " rows inserted into customer_quotation table");
				
				//Delete all VAS records first
				strQry = " Delete from customer_quotation_vas where ";
				strQry += " applicationid = '"+appid+"' ";
				strQry += " and  enterpriseid = '"+enterpriseid+"' ";
				strQry += " and  custid = '"+drEach["custid"].ToString()+"' ";
				if(drEach["quotation_no"].ToString()!="")
				{
					strQry += " and  quotation_no = '"+drEach["quotation_no"].ToString()+"' ";
				}
				if(drEach["quotation_version"].ToString()!="")
				{
					strQry += " and  quotation_version = '"+drEach["quotation_version"].ToString()+"' ";
				}
				dbCmd= dbCon.CreateCommand(strQry,CommandType.Text);
				dbCmd.Connection = conApp;
				dbCmd.Transaction = transactionApp;
				dbCon.ExecuteNonQueryInTransaction(dbCmd);
				Logger.LogTraceInfo("QuotationMgrDAL","AddCustQuoatation","QMD005",iRowsEffected + " rows deleted into customer_quotation table");

				//Inserting the data in the Quotation_VAS table
				cnt = dsVAS.Tables[0].Rows.Count;
				i = 0;
				for(i=0;i<cnt;i++)
				{
					DataRow drEachVAS = dsVAS.Tables[0].Rows[i];
					String strVASCode = "";
					string strQryVas=null;
					
					if(drEachVAS["vas_code"].ToString() != "")
					{
						strVASCode = (String)drEachVAS["vas_code"];
					}
					decimal decSurcharge = (decimal)drEachVAS["surcharge"];
					string strVASDesc=drEachVAS["vas_description"].ToString(); 

					strQryVas = ("insert into customer_quotation_vas (applicationid,enterpriseid,custid,quotation_no,quotation_version,vas_code,surcharge)values('");
					strQryVas=strQryVas+appid+"','"+enterpriseid+"',";

					if((drEach["custid"].ToString() !="") && (drEach["custid"]!=null))						
						strQryVas = strQryVas+"'"+(string)drEach["custid"]+"',";
					else
						strQryVas = strQryVas+"null,";

					if((drEach["quotation_no"].ToString()!="") && (drEach["quotation_no"]!=null))
						strQryVas = strQryVas+"'"+drEach["quotation_no"].ToString()+"',";
					else
						strQryVas = strQryVas+"null,";				
					if((drEach["quotation_version"].ToString()!="") && (drEach["quotation_version"]!=null))
						strQryVas = strQryVas+System.Convert.ToInt64(drEach["quotation_version"]).ToString()+",";
					else
						strQryVas = strQryVas+"null,";					
					if((drEachVAS["vas_code"].ToString()!="") && (drEachVAS["vas_code"]!=null))
						strQryVas = strQryVas+"'"+Utility.ReplaceSingleQuote(drEachVAS["vas_code"].ToString())+"',";
					else
						strQryVas = strQryVas+"null,";
					if((drEachVAS["surcharge"].ToString()!="") && (drEachVAS["surcharge"]!=null))
						strQryVas = strQryVas+(decimal)drEachVAS["surcharge"]+")";
					else
						strQryVas = strQryVas+"null,";
					
					dbCmd.CommandText=strQryVas; 
					iRowsEffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
					Logger.LogTraceInfo("QuotationMgrDAL","AddCustQuoatation","QMD007",iRowsEffected + " rows inserted into customer_vas table");
				}

				//Delete all Service records first
				strQry = " delete from customer_quotation_service where applicationid = '"+appid+"' and  enterpriseid = '"+enterpriseid+"' ";
				strQry += " and  custid = '"+drEach["custid"].ToString()+"' ";

				if(drEach["quotation_no"].ToString()!="")
					strQry += " and  quotation_no = '"+drEach["quotation_no"].ToString()+"' ";
				if(drEach["quotation_version"].ToString()!="")
					strQry += " and  quotation_version = '"+drEach["quotation_version"].ToString()+"' ";

				dbCmd= dbCon.CreateCommand(strQry,CommandType.Text);
				dbCmd.Connection = conApp;
				dbCmd.Transaction = transactionApp;
				dbCon.ExecuteNonQueryInTransaction(dbCmd);
				Logger.LogTraceInfo("QuotationMgrDAL","AddCustQuoatation","QMD005",iRowsEffected + " rows deleted into customer_quotation table");

				//Inserting the data in the Quotation_VAS table
				cnt = dsService.Tables[0].Rows.Count;
				i = 0;
				for(i=0;i<cnt;i++)
				{
					DataRow drEachService = dsService.Tables[0].Rows[i];
					String strServiceCode = "";
					string strQryService=null;
					
					if(drEachService["service_code"].ToString() != "")
					{
						strServiceCode = (String)drEachService["service_code"];
					}
					decimal decSurcharge = (decimal)drEachService["service_charge_percent"];
					string strServiceDesc=drEachService["service_description"].ToString(); 

					strQryService = " insert into customer_quotation_service (applicationid,enterpriseid,custid,quotation_no,quotation_version,";
					strQryService +=" service_code,service_charge_percent,service_charge_amt)values('"+appid+"','"+enterpriseid+"',";

					if((drEach["custid"].ToString() !="") && (drEach["custid"]!=null))						
						strQryService = strQryService+"'"+(string)drEach["custid"]+"',";
					else
						strQryService = strQryService+"null,";

					if((drEach["quotation_no"].ToString()!="") && (drEach["quotation_no"]!=null))
						strQryService = strQryService+"'"+drEach["quotation_no"].ToString()+"',";
					else
						strQryService = strQryService+"null,";
				
					if((drEach["quotation_version"].ToString()!="") && (drEach["quotation_version"]!=null))
						strQryService = strQryService+System.Convert.ToInt64(drEach["quotation_version"]).ToString()+",";
					else
						strQryService = strQryService+"null,";
					
					if((drEachService["service_code"].ToString()!="") && (drEachService["service_code"]!=null))
						strQryService = strQryService+"'"+Utility.ReplaceSingleQuote(drEachService["service_code"].ToString()) +"',";
					else
						strQryService = strQryService+"null,";

					if((drEachService["service_charge_percent"].ToString()!="") && (drEachService["service_charge_percent"]!=null))
						strQryService = strQryService+(decimal)drEachService["service_charge_percent"]+",";
					else
						strQryService = strQryService+"null,";

					if((drEachService["service_charge_amt"].ToString()!="") && (drEachService["service_charge_amt"]!=null))
						strQryService = strQryService+(decimal)drEachService["service_charge_amt"]+")";
					else
						strQryService = strQryService+"null)";
					
					dbCmd.CommandText=strQryService; 
					iRowsEffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
					Logger.LogTraceInfo("QuotationMgrDAL","AddCustQuoatation","QMD007",iRowsEffected + " rows inserted into customer_vas table");
				}

				transactionApp.Commit();
				Logger.LogTraceInfo("QuotationMgrDAL","AddCustQuoatation","QMD010","App db insert transaction committed.");
			}	
			catch(ApplicationException appException)
			{
				try
				{
					transactionApp.Rollback();
					Logger.LogTraceInfo("QuotationMgrDAL","AddCustQuoatation","QMD011","App db insert transaction rolled back.");

				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("QuotationMgrDAL","AddCustQuoatation","QMD012","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}


				Logger.LogTraceError("QuotationMgrDAL","AddCustQuoatation","QMD013","Insert Transaction Failed : "+ appException.Message.ToString());
				throw new ApplicationException("Error adding Customer ",appException);
			}
			catch(Exception exception)
			{
				try
				{
					transactionApp.Rollback();
					Logger.LogTraceInfo("QuotationMgrDAL","AddCustQuoatation","QMD014","App db insert transaction rolled back.");

				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("QuotationMgrDAL","AddCustQuoatation","QMD015","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}

				Logger.LogTraceError("QuotationMgrDAL","AddCustQuoatation","QMD016","Insert Transaction Failed : "+ exception.Message.ToString());
				throw new ApplicationException("Error adding Customer ",exception);
			}
			finally
			{
				if(conApp != null)
					conApp.Close();
			}

			return iRowsEffected;
		}

		public static int  UpdateAgentQuotation(string appid,string enterpriseid,DataSet dsAgentQuotation,DataSet dsVAS, DataSet dsService)
		{
				
			string strQry =null;
			string strQryVAS =null;
			int iRowsEffected=0;
			DbConnection dbCon=null;
			IDbConnection conApp =null;
			IDbCommand  dbCmd=null;
				
			int cnt = 0;
			int i = 0;
			
			dbCon = DbConnectionManager.GetInstance().GetDbConnection(appid,enterpriseid);
			
			if(dbCon==null)
			{
				Logger.LogTraceError("QuotationMgrDAL","UpdateAgentQuotation","QMD001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			
			try
			{
				conApp = dbCon.GetConnection();
				dbCon.OpenConnection(ref conApp); 
			}
			catch(System.ApplicationException appException)
			{	
				if (conApp!=null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("QuotationMgrDAL","UpdateAgentQuotation","QMD001",appException.Message);
				throw new ApplicationException("Connection to database failed",null);
			}
			if(conApp == null)
			{
				Logger.LogTraceError("QuotationMgrDAL","UpdateAgentQuotation","QMD002","conApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			IDbTransaction transactionApp = dbCon.BeginTransaction(conApp,IsolationLevel.ReadCommitted);	
			if(transactionApp == null)
			{
				Logger.LogTraceError("QuotationMgrDAL","UpdateAgentQuotation","ERR004","transactionApp is null");
				throw new ApplicationException("Failed Begining a App Transaction..",null);
			}
			try
			{
				//Insert into Customer_Quotation table..
				DataRow drEach = dsAgentQuotation.Tables[0].Rows[0]; 
				strQry =strQry+"update agent_quotation set ";
						
				strQry = strQry+"band_code = ";
				if((drEach["band_code"].ToString()!="") && (drEach["band_code"]!=null))
					strQry = strQry+"'"+Utility.ReplaceSingleQuote(drEach["band_code"].ToString())+"',";
				else
					strQry = strQry+"null,";

				strQry = strQry+"quotation_date = ";
				if((drEach["quotation_date"].ToString()!="") && (drEach["quotation_date"]!=null))
				{
					DateTime dtquotation_date = Convert.ToDateTime(drEach["quotation_date"]);
					String strdtquotation_date = Utility.DateFormat(appid,enterpriseid,dtquotation_date,DTFormat.DateTime);
					strQry=strQry+""+strdtquotation_date+""+",";
				}
				else
					strQry = strQry+"null,";

				strQry = strQry+"attention_to_person = ";
				if((drEach["attention_to_person"].ToString()!="") && (drEach["attention_to_person"]!=null))
					strQry = strQry+"N'"+Utility.ReplaceSingleQuote(drEach["attention_to_person"].ToString())+"',";
				else
					strQry = strQry+"null,";

				strQry = strQry+"copy_to_person = ";
				if((drEach["copy_to_person"].ToString()!="") && (drEach["copy_to_person"]!=null))
					strQry = strQry+"N'"+Utility.ReplaceSingleQuote(drEach["copy_to_person"].ToString())+"',";
				else
					strQry = strQry+"null,";

				strQry = strQry+"remark = ";
				if((drEach["remark"].ToString()!="") && (drEach["remark"]!=null))
					strQry = strQry+"N'"+Utility.ReplaceSingleQuote(drEach["remark"].ToString())+"',";
				else
					strQry = strQry+"null,";

				strQry = strQry+"quotation_status = ";
				if((drEach["quotation_status"].ToString()!="") && (drEach["quotation_status"]!=null))
					strQry = strQry+"'"+drEach["quotation_status"]+"'";
				else
					strQry = strQry+"'D'"+"";

				strQry = strQry +" where  applicationid ='"+appid+"'  and enterpriseid ='"+enterpriseid+"' and agentid ='"+drEach["custid"]+"'";
				strQry = strQry +" and quotation_no ='"+drEach["quotation_no"]+"' and quotation_version ="+drEach["quotation_version"];

				dbCmd= dbCon.CreateCommand(strQry,CommandType.Text);
				dbCmd.Connection = conApp;
				dbCmd.Transaction = transactionApp;
				iRowsEffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
				Logger.LogTraceInfo("QuotationMgrDAL","AddCustQuoatation","QMD005",iRowsEffected + " rows inserted into customer_quotation table");

//				strQry = "update Agent set remark = ";
//				strQry = strQry +"'"+drEach["remark"].ToString()+"' ";
//				strQry = strQry +" where";
//				strQry = strQry +" applicationid =";
//				strQry = strQry +"'"+appid+"'";
//				strQry = strQry +" and enterpriseid =";
//				strQry = strQry +"'"+enterpriseid+"'";
//				strQry = strQry +" and agentid =";
//				strQry = strQry +"'"+drEach["custid"]+"'";
//
//				dbCmd= dbCon.CreateCommand(strQry,CommandType.Text);
//				dbCmd.Connection = conApp;
//				dbCmd.Transaction = transactionApp;
//				iRowsEffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
//				Logger.LogTraceInfo("QuotationMgrDAL","AddAgentQuoatation","QMD005",iRowsEffected + " rows inserted into customer_quotation table");

				//Delete all VAS records first
				strQry = "delete from agent_quotation_vas where ";
				strQry += "applicationid = '"+appid+"' and  enterpriseid = '"+enterpriseid+"' and  agentid = '"+drEach["custid"].ToString()+"' ";
				
				if(drEach["quotation_no"].ToString()!="")
					strQry += " and  quotation_no = '"+drEach["quotation_no"].ToString()+"' ";
				if(drEach["quotation_version"].ToString()!="")
					strQry += " and  quotation_version = '"+drEach["quotation_version"].ToString()+"' ";

				dbCmd= dbCon.CreateCommand(strQry,CommandType.Text);
				dbCmd.Connection = conApp;
				dbCmd.Transaction = transactionApp;
				dbCon.ExecuteNonQueryInTransaction(dbCmd);
				Logger.LogTraceInfo("QuotationMgrDAL","AddCustQuoatation","QMD005",iRowsEffected + " rows deleted into customer_quotation table");

				//Inserting the data in the Agent_VAS table
				cnt = dsVAS.Tables[0].Rows.Count;
				i = 0;
				for(i=0;i<cnt;i++)
				{
					DataRow drEachVAS = dsVAS.Tables[0].Rows[i];
					String strVASCode = "";
					string strQryVas=null;
					
					if(drEachVAS["vas_code"].ToString() != "")
					{
						strVASCode = (String)drEachVAS["vas_code"];
					}
					decimal decSurcharge = (decimal)drEachVAS["surcharge"];
					string strVASDesc=drEachVAS["vas_description"].ToString(); 

					strQryVas = ("insert into agent_quotation_vas (applicationid,enterpriseid,agentid,quotation_no,quotation_version,vas_code,surcharge)values('");
					strQryVas=strQryVas+appid+"',";
					strQryVas=strQryVas+"'"+enterpriseid+"',";
					if((drEach["custid"].ToString() !="") && (drEach["custid"]!=null))
						strQryVas = strQryVas+"'"+(string)drEach["custid"]+"',";
					else
						strQryVas = strQryVas+"null,";

					if((drEach["quotation_no"].ToString()!="") && (drEach["quotation_no"]!=null))
						strQryVas = strQryVas+"'"+drEach["quotation_no"].ToString()+"',";
					else
						strQryVas = strQryVas+"null,";
				
					if((drEach["quotation_version"].ToString()!="") && (drEach["quotation_version"]!=null))
						strQryVas = strQryVas+System.Convert.ToInt64(drEach["quotation_version"]).ToString()+",";
					else
						strQryVas = strQryVas+"null,";
					
					if((drEachVAS["vas_code"].ToString()!="") && (drEachVAS["vas_code"]!=null))
						strQryVas = strQryVas+"'"+Utility.ReplaceSingleQuote(drEachVAS["vas_code"].ToString())+"',";
					else
						strQryVas = strQryVas+"null,";

					if((drEachVAS["surcharge"].ToString()!="") && (drEachVAS["surcharge"]!=null))
						strQryVas = strQryVas+(decimal)drEachVAS["surcharge"]+")";
					else
						strQryVas = strQryVas+"null)";
					
					dbCmd.CommandText=strQryVas; 
					iRowsEffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
					Logger.LogTraceInfo("QuotationMgrDAL","AddAgentQuotation","QMD007",iRowsEffected + " rows inserted into customer_vas table");
				}

				//Delete all Service records first
				strQry = "delete from agent_quotation_service where ";
				strQry += "applicationid = '"+appid+"' ";
				strQry += " and  enterpriseid = '"+enterpriseid+"' ";
				strQry += " and  agentid = '"+drEach["custid"].ToString()+"' ";
				if(drEach["quotation_no"].ToString()!="")
					strQry += " and  quotation_no = '"+drEach["quotation_no"].ToString()+"' ";

				if(drEach["quotation_version"].ToString()!="")

					strQry += " and  quotation_version = '"+drEach["quotation_version"].ToString()+"' ";

				dbCmd= dbCon.CreateCommand(strQry,CommandType.Text);
				dbCmd.Connection = conApp;
				dbCmd.Transaction = transactionApp;
				dbCon.ExecuteNonQueryInTransaction(dbCmd);
				Logger.LogTraceInfo("QuotationMgrDAL","AddCustQuoatation","QMD005",iRowsEffected + " rows deleted into customer_quotation table");

				//Inserting the data in the Agent_Service table
				cnt = dsService.Tables[0].Rows.Count;
				i = 0;
				for(i=0;i<cnt;i++)
				{
					DataRow drEachService = dsService.Tables[0].Rows[i];
					String strServiceCode = "";
					string strQryService=null;
					
					if(drEachService["service_code"].ToString() != "")
					{
						strServiceCode = (String)drEachService["service_code"];
					}
					decimal decSurcharge = (decimal)drEachService["service_charge_percent"];
					string strServiceDesc=drEachService["service_description"].ToString(); 

					strQryService = ("insert into agent_quotation_service (applicationid,enterpriseid,agentid,quotation_no,");
					strQryService+=" quotation_version,service_code,service_charge_percent,service_charge_amt)values('"+appid+"',";
					strQryService=strQryService+"'"+enterpriseid+"',"; 

					if((drEach["custid"].ToString() !="") && (drEach["custid"]!=null))						
						strQryService = strQryService+"'"+(string)drEach["custid"]+"',";
					else
						strQryService = strQryService+"null,";

					if((drEach["quotation_no"].ToString()!="") && (drEach["quotation_no"]!=null))
						strQryService = strQryService+"'"+drEach["quotation_no"].ToString()+"',";
					else
						strQryService = strQryService+"null,";
				
					if((drEach["quotation_version"].ToString()!="") && (drEach["quotation_version"]!=null))
						strQryService = strQryService+System.Convert.ToInt64(drEach["quotation_version"]).ToString()+",";
					else
						strQryService = strQryService+"null,";
					
					if((drEachService["service_code"].ToString()!="") && (drEachService["service_code"]!=null))
						strQryService = strQryService+"'"+Utility.ReplaceSingleQuote(drEachService["service_code"].ToString()) +"',";
					else
						strQryService = strQryService+"null,";
					
					if((drEachService["service_charge_percent"].ToString()!="") && (drEachService["service_charge_percent"]!=null))
						strQryService = strQryService+(decimal)drEachService["service_charge_percent"]+",";
					else
						strQryService = strQryService+"null,";

					if((drEachService["service_charge_amt"].ToString()!="") && (drEachService["service_charge_amt"]!=null))
						strQryService = strQryService+(decimal)drEachService["service_charge_amt"]+")";
					else
						strQryService = strQryService+"null)";
					
					dbCmd.CommandText=strQryService; 
					iRowsEffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
					Logger.LogTraceInfo("QuotationMgrDAL","AddAgentQuotation","QMD007",iRowsEffected + " rows inserted into customer_vas table");
				}

				transactionApp.Commit();
				Logger.LogTraceInfo("QuotationMgrDAL","AddAgentQuoatation","QMD010","App db insert transaction committed.");
			}	
			catch(ApplicationException appException)
			{
				try
				{
					transactionApp.Rollback();
					Logger.LogTraceInfo("QuotationMgrDAL","AddAgentQuoatation","QMD011","App db insert transaction rolled back.");
				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("QuotationMgrDAL","AddAgentQuoatation","QMD012","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}
				Logger.LogTraceError("QuotationMgrDAL","AddAgentQuoatation","QMD013","Insert Transaction Failed : "+ appException.Message.ToString());
				throw new ApplicationException("Error adding Agent ",appException);
			}
			catch(Exception exception)
			{
				try
				{
					transactionApp.Rollback();
					Logger.LogTraceInfo("QuotationMgrDAL","AddAgentQuoatation","QMD014","App db insert transaction rolled back.");
				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("QuotationMgrDAL","AddAgentQuoatation","QMD015","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}
				Logger.LogTraceError("QuotationMgrDAL","AddAgentQuoatation","QMD016","Insert Transaction Failed : "+ exception.Message.ToString());
				throw new ApplicationException("Error adding Agent ",exception);
			}
			finally
			{
				if(conApp != null)
					conApp.Close();
			}

			return iRowsEffected;
		}

		public static int MarkQuoted(string appid, string enterpriseid, DataSet dsQuoted)
		{
			string strQry =null;
			string strQryVAS =null;
			int iRowsEffected=0;
			DbConnection dbCon=null;
			IDbConnection conApp =null;
			IDbCommand  dbCmd=null;
				
			int cnt = 0;
			int i = 0;
			
			dbCon = DbConnectionManager.GetInstance().GetDbConnection(appid,enterpriseid);
			
			if(dbCon==null)
			{
				Logger.LogTraceError("QuotationMgrDAL","MarkQuoted","QMD001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			
			try
			{
				conApp = dbCon.GetConnection();
				dbCon.OpenConnection(ref conApp); 
			}
			catch(System.ApplicationException appException)
			{	
				if (conApp!=null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("QuotationMgrDAL","MarkQuoted","QMD001","Error opening database");
				throw new ApplicationException("Connection to database failed",null);
			}
			if(conApp == null)
			{
				Logger.LogTraceError("QuotationMgrDAL","MarkQuoted","QMD002","conApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			IDbTransaction transactionApp = dbCon.BeginTransaction(conApp,IsolationLevel.ReadCommitted);	
			if(transactionApp == null)
			{
				Logger.LogTraceError("QuotationMgrDAL","MarkQuoted","ERR004","transactionApp is null");
				throw new ApplicationException("Failed Begining a App Transaction..",null);
			}
			try
			{
				DataRow drEach = dsQuoted.Tables[0].Rows[0];
				String strCustQuery=null;
				if(drEach["type"].ToString()=="C")
				{
					//Updating from QUOTED to Cancelled
					strCustQuery = "update Customer_Quotation set quotation_status = 'S' ";
				}
				if(drEach["type"].ToString()=="A")
				{
					//Updating from QUOTED to Cancelled
					strCustQuery = "update Agent_Quotation set quotation_status = 'S' ";
				}
				
				strCustQuery +=" where applicationid = '"+appid+"' and enterpriseid = '"+enterpriseid+"' ";
				if(drEach["type"].ToString()=="C")
				{
					strCustQuery +=" and custid = '"+drEach["custid"].ToString()+"' ";
				}
				if(drEach["type"].ToString()=="A")
				{
					strCustQuery +=" and agentid = '"+drEach["custid"].ToString()+"' ";
				}
				
				strCustQuery +=" and quotation_status = 'Q'";

				dbCmd= dbCon.CreateCommand(strCustQuery,CommandType.Text);
				dbCmd.Connection = conApp;
				dbCmd.Transaction = transactionApp;
				iRowsEffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
				Logger.LogTraceInfo("QuotationMgrDAL","MarkQuoted","QMD005",iRowsEffected + " rows inserted into customer_quotation table");
			
				//Insert into Customer_Quotation table..
				if(drEach["type"].ToString()=="C")
					strQry =strQry+"update customer_quotation set ";

				if(drEach["type"].ToString()=="A")
					strQry =strQry+"update agent_quotation set ";

				strQry = strQry+"quotation_status = 'Q' where applicationid ='"+appid+"' and enterpriseid ='"+enterpriseid+"'";

				if(drEach["type"].ToString()=="C")
					strQry = strQry +" and custid =";

				if(drEach["type"].ToString()=="A")
					strQry = strQry +" and agentid =";

				strQry = strQry +"'"+drEach["custid"]+"' and quotation_no ='"+drEach["quotation_no"]+"'";
				strQry = strQry +" and quotation_version ="+drEach["quotation_version"];

				dbCmd= dbCon.CreateCommand(strQry,CommandType.Text);
				dbCmd.Connection = conApp;
				dbCmd.Transaction = transactionApp;
				iRowsEffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
				Logger.LogTraceInfo("QuotationMgrDAL","MarkQuoted","QMD005",iRowsEffected + " rows inserted into customer_quotation table");
			
				//Updating the data in the Customer table
				DataRow dr = dsQuoted.Tables[0].Rows[0];
				String strQuery = "";
				if(drEach["type"].ToString()=="C")
					strQuery =strQuery+"update Customer set ";

				if(drEach["type"].ToString()=="A")
					strQuery =strQuery+"update Agent set ";

				strQuery = strQuery+"active_quotation_no = '"+dr["quotation_no"].ToString()+"' ";			
				strQuery = strQuery +" where  applicationid ='"+appid+"' and enterpriseid ='"+enterpriseid+"'";

				if(drEach["type"].ToString()=="C")
					strQuery = strQuery +" and custid =";

				if(drEach["type"].ToString()=="A")
					strQuery = strQuery +" and agentid =";


				strQuery = strQuery +"'"+dr["custid"]+"'";
				
				dbCmd= dbCon.CreateCommand(strQuery,CommandType.Text);
				dbCmd.Connection = conApp;
				dbCmd.Transaction = transactionApp; 
				iRowsEffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
				Logger.LogTraceInfo("QuotationMgrDAL","MarkQuoted","QMD007",iRowsEffected + " rows inserted into customer_vas table");
			
				transactionApp.Commit();
				Logger.LogTraceInfo("QuotationMgrDAL","MarkQuoted","QMD010","App db update transaction committed.");
			}	
			catch(ApplicationException appException)
			{
				try
				{
					transactionApp.Rollback();
					Logger.LogTraceInfo("QuotationMgrDAL","MarkQuoted","QMD011","App db update transaction rolled back.");

				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("QuotationMgrDAL","MarkQuoted","QMD012","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}


				Logger.LogTraceError("QuotationMgrDAL","MarkQuoted","QMD013","Update Transaction Failed : "+ appException.Message.ToString());
				throw new ApplicationException("Error adding Customer ",appException);
			}
			catch(Exception exception)
			{
				try
				{
					transactionApp.Rollback();
					Logger.LogTraceInfo("QuotationMgrDAL","MarkQuoted","QMD014","App db update transaction rolled back.");

				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("QuotationMgrDAL","MarkQuoted","QMD015","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}

				Logger.LogTraceError("QuotationMgrDAL","MarkQuoted","QMD016","Update Transaction Failed : "+ exception.Message.ToString());
				throw new ApplicationException("Error adding Customer ",exception);
			}
			finally
			{
				if(conApp != null)
					conApp.Close();
			}

			return iRowsEffected;
		}
		public static int UpdateToNewVision(String appid, String enterpriseid, DataSet dsCustQuotation, DataSet dsVAS, DataSet dsService)
		{
			string strQry =null;
			string strQryVAS =null;
			int iRowsEffected=0;
			DbConnection dbCon=null;
			IDbConnection conApp =null;
			IDbCommand  dbCmd=null;
				
			int cnt = 0;
			int i = 0;
			dbCon = DbConnectionManager.GetInstance().GetDbConnection(appid,enterpriseid);
			IDbCommand dbCom = null;

			
			if(dbCon==null)
			{
				Logger.LogTraceError("QuotationMgrDAL","UpdateToNewVision","QMD001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			
			try
			{
				conApp = dbCon.GetConnection();
				dbCon.OpenConnection(ref conApp); 
			}
			catch(System.ApplicationException appException)
			{	
				if (conApp!=null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("QuotationMgrDAL","UpdateToNewVision","QMD001","Error opening database");
				throw new ApplicationException("Connection to database failed",null);
			}
			if(conApp == null)
			{
				Logger.LogTraceError("QuotationMgrDAL","UpdateToNewVision","QMD002","conApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			IDbTransaction transactionApp = dbCon.BeginTransaction(conApp,IsolationLevel.ReadCommitted);	
			if(transactionApp == null)
			{
				Logger.LogTraceError("QuotationMgrDAL","UpdateToNewVision","ERR004","transactionApp is null");
				throw new ApplicationException("Failed Begining a App Transaction..",null);
			}
			try
			{
				DataRow drEach = dsCustQuotation.Tables[0].Rows[0];
				//Delete all VAS records first
				if(drEach["type"].ToString()=="C")
					strQry = "delete from customer_quotation_vas where ";
				if(drEach["type"].ToString()=="A")
					strQry = "delete from agent_quotation_vas where ";

				strQry += "applicationid = '"+appid+"' ";
				strQry += " and  enterpriseid = '"+enterpriseid+"' ";
				if(drEach["type"].ToString()=="C")
				{
					strQry += " and  custid = '"+drEach["custid"].ToString()+"' ";
				}
				if(drEach["type"].ToString()=="A")
				{
					strQry += " and  agentid = '"+drEach["custid"].ToString()+"' ";
				}
				if(drEach["quotation_no"].ToString()!="")
				{
					strQry += " and  quotation_no = '"+drEach["quotation_no"].ToString()+"' ";
				}
				if(drEach["quotation_version"].ToString()!="")
				{
					strQry += " and  quotation_version = '"+drEach["quotation_version"].ToString()+"' ";
				}
				dbCmd= dbCon.CreateCommand(strQry,CommandType.Text);
				dbCmd.Connection = conApp;
				dbCmd.Transaction = transactionApp;
				dbCon.ExecuteNonQueryInTransaction(dbCmd);
				Logger.LogTraceInfo("QuotationMgrDAL","AddCustQuoatation","QMD005",iRowsEffected + " rows deleted from customer_quotation_vas table");
				
				//Delete all Service records first
				if(drEach["type"].ToString()=="C")
				{
					strQry = "delete from customer_quotation_service where ";
				}
				if(drEach["type"].ToString()=="A")
				{
					strQry = "delete from agent_quotation_service where ";
				}
				strQry += "applicationid = '"+appid+"' and  enterpriseid = '"+enterpriseid+"' ";

				if(drEach["type"].ToString()=="C")
				{
					strQry += " and  custid = '"+drEach["custid"].ToString()+"' ";
				}
				if(drEach["type"].ToString()=="A")
				{
					strQry += " and  agentid = '"+drEach["custid"].ToString()+"' ";
				}
				if(drEach["quotation_no"].ToString()!="")
				{
					strQry += " and  quotation_no = '"+drEach["quotation_no"].ToString()+"' ";
				}
				if(drEach["quotation_version"].ToString()!="")
				{
					strQry += " and  quotation_version = '"+drEach["quotation_version"].ToString()+"' ";
				}
				dbCmd= dbCon.CreateCommand(strQry,CommandType.Text);
				dbCmd.Connection = conApp;
				dbCmd.Transaction = transactionApp;
				dbCon.ExecuteNonQueryInTransaction(dbCmd);
				Logger.LogTraceInfo("QuotationMgrDAL","AddCustQuoatation","QMD005",iRowsEffected + " rows deleted customer_quotation_service table");
				
				//Updateing
				if(drEach["type"].ToString()=="C")
				{
					strQry ="update customer_quotation set ";
				}
				if(drEach["type"].ToString()=="A")
				{
					strQry ="update agent_quotation set ";
				}

				strQry = strQry +"quotation_version = ";
				strQry = strQry +drEach["quotation_version"];

				strQry = strQry+",band_code = ";
				if((drEach["band_code"].ToString()!="") && (drEach["band_code"]!=null))
				{
					strQry = strQry+"'"+Utility.ReplaceSingleQuote(drEach["band_code"].ToString())+"',";
				}
				else
				{
					strQry = strQry+"null,";
				}
				strQry = strQry+"quotation_date = ";
				if((drEach["quotation_date"].ToString()!="") && (drEach["quotation_date"]!=null))
				{
					DateTime dtquotation_date = Convert.ToDateTime(drEach["quotation_date"]);
					String strdtquotation_date = Utility.DateFormat(appid,enterpriseid,dtquotation_date,DTFormat.DateTime);
					strQry=strQry+" "+strdtquotation_date+" "+",";
				}
				else
				{
					strQry = strQry+"null,";
				}
				if(drEach["type"].ToString()=="C")
				{
					strQry = strQry+"salesmanid = ";
					if((drEach["salesmanid"].ToString()!="") && (drEach["salesmanid"]!=null))
						strQry = strQry+"'"+Utility.ReplaceSingleQuote(drEach["salesmanid"].ToString())+"',";
					else
						strQry = strQry+"null,";
				}
				
				strQry = strQry+"attention_to_person = ";
				if((drEach["attention_to_person"].ToString()!="") && (drEach["attention_to_person"]!=null))
				{
					strQry = strQry+"N'"+Utility.ReplaceSingleQuote(drEach["attention_to_person"].ToString())+"',";
				}
				else
				{
					strQry = strQry+"null,";
				}
				strQry = strQry+"copy_to_person = ";
				if((drEach["copy_to_person"].ToString()!="") && (drEach["copy_to_person"]!=null))
				{
					strQry = strQry+"N'"+Utility.ReplaceSingleQuote(drEach["copy_to_person"].ToString())+"',";
				}
				else
				{
					strQry = strQry+"null,";
				}
				strQry = strQry+"quotation_status = ";
				if((drEach["quotation_status"].ToString()!="") && (drEach["quotation_status"]!=null))
				{
					strQry = strQry+"'"+drEach["quotation_status"]+"'";
				}
				else
				{
					strQry = strQry+ "'D'";
				}
				
				strQry = strQry +" where";
				strQry = strQry +" applicationid =";
				strQry = strQry +"'"+appid+"'";
				strQry = strQry +" and enterpriseid =";
				strQry = strQry +"'"+enterpriseid+"'";
				if(drEach["type"].ToString()=="C")
				{
					strQry = strQry +" and custid =";
				}
				if(drEach["type"].ToString()=="A")
				{
					strQry = strQry +" and agentid =";
				}
				
				strQry = strQry +"'"+drEach["custid"]+"'";
				strQry = strQry +" and quotation_no =";
				strQry = strQry +"'"+drEach["quotation_no"]+"'";
				strQry = strQry +" and quotation_version =";
				int ver = int.Parse(drEach["quotation_version"].ToString());
				ver =ver-1;
				strQry = strQry +ver;

				dbCmd= dbCon.CreateCommand(strQry,CommandType.Text);
				dbCmd.Connection = conApp;
				dbCmd.Transaction = transactionApp;
				iRowsEffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
				Logger.LogTraceInfo("QuotationMgrDAL","AddCustQuoatation","QMD005",iRowsEffected + " rows inserted into customer_quotation table");

				if(drEach["type"].ToString()=="C")
				{
					strQry = "update customer set remark = ";
				}
				if(drEach["type"].ToString()=="A")
				{
					strQry = "update agent set remark = ";
				}
				
				strQry = strQry +"N'"+Utility.ReplaceSingleQuote(drEach["remark"].ToString())+"' ";
				strQry = strQry +" where";
				strQry = strQry +" applicationid =";
				strQry = strQry +"'"+appid+"'";
				strQry = strQry +" and enterpriseid =";
				strQry = strQry +"'"+enterpriseid+"'";
				if(drEach["type"].ToString()=="C")
				{
					strQry = strQry +" and custid =";
				}
				if(drEach["type"].ToString()=="A")
				{
					strQry = strQry +" and agentid =";
				}
				
				strQry = strQry +"'"+drEach["custid"]+"'";

				dbCmd= dbCon.CreateCommand(strQry,CommandType.Text);
				dbCmd.Connection = conApp;
				dbCmd.Transaction = transactionApp;
				iRowsEffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
				Logger.LogTraceInfo("QuotationMgrDAL","UpdateToNewVision","QMD005",iRowsEffected + " rows updated into customer table");
				
				//Inserting the data in the Quotation_VAS table
				cnt = dsVAS.Tables[0].Rows.Count;
				i = 0;
				for(i=0;i<cnt;i++)
				{
					DataRow drEachVAS = dsVAS.Tables[0].Rows[i];
					String strVASCode = "";
					string strQryVas=null;
					
					if(drEachVAS["vas_code"].ToString() != "")
					{
						strVASCode = (String)drEachVAS["vas_code"];
					}
					decimal decSurcharge = (decimal)drEachVAS["surcharge"];
					string strVASDesc=drEachVAS["vas_description"].ToString(); 

					if(drEach["type"].ToString()=="C")
					{
						strQryVas = ("insert into customer_quotation_vas (applicationid,enterpriseid,custid,quotation_no,quotation_version,vas_code,surcharge)values('");
					
					}
					if(drEach["type"].ToString()=="A")
					{
						strQryVas = ("insert into agent_quotation_vas (applicationid,enterpriseid,agentid,quotation_no,quotation_version,vas_code,surcharge)values('");
					
					}
					strQryVas=strQryVas+appid+"',";
					strQryVas=strQryVas+"'"+enterpriseid+"',";
					if((drEach["custid"].ToString() !="") && (drEach["custid"]!=null))
					{
						
						strQryVas = strQryVas+"'"+(string)drEach["custid"]+"',";
					}
					else
					{
						strQryVas = strQryVas+"null,";
					}

					if((drEach["quotation_no"].ToString()!="") && (drEach["quotation_no"]!=null))
					{
						strQryVas = strQryVas+"'"+drEach["quotation_no"].ToString()+"',";
					}
					else
					{
						strQryVas = strQryVas+"null,";
					}
				
					if((drEach["quotation_version"].ToString()!="") && (drEach["quotation_version"]!=null))
					{
						strQryVas = strQryVas+System.Convert.ToInt64(drEach["quotation_version"]).ToString()+",";
					}
					else
					{
						strQryVas = strQryVas+"null,";
					}
					
					if((drEachVAS["vas_code"].ToString()!="") && (drEachVAS["vas_code"]!=null))
					{
						strQryVas = strQryVas+"'"+Utility.ReplaceSingleQuote(drEachVAS["vas_code"].ToString()) +"',";
					}
					else
					{
						strQryVas = strQryVas+"null,";
					}
					if((drEachVAS["surcharge"].ToString()!="") && (drEachVAS["surcharge"]!=null))
					{
						strQryVas = strQryVas+(decimal)drEachVAS["surcharge"]+")";
					}
					else
					{
						strQryVas = strQryVas+"null,";
					}
					
					dbCmd.CommandText=strQryVas; 
					iRowsEffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
					Logger.LogTraceInfo("QuotationMgrDAL","UpdateToNewVision","QMD007",iRowsEffected + " rows inserted into customer_quotation_vas table");
				}
				//Inserting the data in the Quotation_Service table
				cnt = dsService.Tables[0].Rows.Count;
				i = 0;
				for(i=0;i<cnt;i++)
				{
					DataRow drEachService = dsService.Tables[0].Rows[i];
					String strServiceCode = "";
					string strQryService=null;
					
					if(drEachService["service_code"].ToString() != "")
					{
						strServiceCode = (String)drEachService["service_code"];
					}
					decimal decSurcharge = (decimal)drEachService["service_charge_percent"];
					string strServiceDesc=drEachService["service_description"].ToString(); 

					if(drEach["type"].ToString()=="C")
					{
						strQryService = ("insert into customer_quotation_service (applicationid,enterpriseid,custid,quotation_no,");
						strQryService+= (" quotation_version,service_code,service_charge_percent,service_charge_amt)values('");					
					}
					if(drEach["type"].ToString()=="A")
					{
						strQryService = ("insert into agent_quotation_service (applicationid,enterpriseid,agentid,quotation_no,");
						strQryService+= (" quotation_version,service_code,service_charge_percent,service_charge_amt)values('");
					}
					strQryService=strQryService+appid+"','"+enterpriseid+"',";

					if((drEach["custid"].ToString() !="") && (drEach["custid"]!=null))
						strQryService = strQryService+"'"+(string)drEach["custid"]+"',";
					else
						strQryService = strQryService+"null,";

					if((drEach["quotation_no"].ToString()!="") && (drEach["quotation_no"]!=null))
						strQryService = strQryService+"'"+drEach["quotation_no"].ToString()+"',";
					else
						strQryService = strQryService+"null,";
				
					if((drEach["quotation_version"].ToString()!="") && (drEach["quotation_version"]!=null))
						strQryService = strQryService+System.Convert.ToInt64(drEach["quotation_version"]).ToString()+",";
					else
						strQryService = strQryService+"null,";
					
					if((drEachService["service_code"].ToString()!="") && (drEachService["service_code"]!=null))
						strQryService = strQryService+"'"+Utility.ReplaceSingleQuote(drEachService["service_code"].ToString())+"',";
					else
						strQryService = strQryService+"null,";

					if((drEachService["service_charge_percent"].ToString()!="") && (drEachService["service_charge_percent"]!=null))
						strQryService = strQryService+(decimal)drEachService["service_charge_percent"]+",";
					else
						strQryService = strQryService+"null,";

					if((drEachService["service_charge_amt"].ToString()!="") && (drEachService["service_charge_amt"]!=null))
						strQryService = strQryService+(decimal)drEachService["service_charge_amt"]+")";
					else
						strQryService = strQryService+"null)";
                    					
					dbCmd.CommandText=strQryService; 
					iRowsEffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
					Logger.LogTraceInfo("QuotationMgrDAL","UpdateToNewVision","QMD007",iRowsEffected + " rows inserted into customer_quotation_vas table");
				}
				//Update To Null
				DataRow dr = dsCustQuotation.Tables[0].Rows[0];
				String strQuery = "";
				if(drEach["type"].ToString()=="C")
				{
					strQuery =strQuery+" update Customer set ";
				}
				if(drEach["type"].ToString()=="A")
				{
					strQuery =strQuery+" update Agent set ";
				}
				
				strQuery = strQuery+" active_quotation_no = null ";			
				strQuery = strQuery +" where applicationid ='"+appid+"' and enterpriseid ='"+enterpriseid+"'";

				if(drEach["type"].ToString()=="C")
				{
					strQuery = strQuery +" and custid =";
				}
				if(drEach["type"].ToString()=="A")
				{
					strQuery = strQuery +" and agentid =";
				}				
				strQuery = strQuery +"'"+dr["custid"]+"'";

				dbCmd= dbCon.CreateCommand(strQuery,CommandType.Text);
				dbCmd.Connection = conApp;
				dbCmd.Transaction = transactionApp; 
				iRowsEffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
				Logger.LogTraceInfo("QuotationMgrDAL","UpdateToNewVision","QMD007",iRowsEffected + " rows updated into customer_vas table");
			
				transactionApp.Commit();
				Logger.LogTraceInfo("QuotationMgrDAL","AddCustQuoatation","QMD010","App db insert transaction committed.");

			}	
			
			catch(ApplicationException appException)
			{
				try
				{
					transactionApp.Rollback();
					Logger.LogTraceInfo("QuotationMgrDAL","UpdateToNewVision","QMD011","App db insert transaction rolled back.");

				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("QuotationMgrDAL","UpdateToNewVision","QMD012","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}
				Logger.LogTraceError("QuotationMgrDAL","UpdateToNewVision","QMD013","Insert Transaction Failed : "+ appException.Message.ToString());
				throw new ApplicationException("Error adding Customer ",appException);
			}
			catch(Exception exception)
			{
				try
				{
					transactionApp.Rollback();
					Logger.LogTraceInfo("QuotationMgrDAL","UpdateToNewVision","QMD014","App db insert transaction rolled back.");
				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("QuotationMgrDAL","UpdateToNewVision","QMD015","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}

				Logger.LogTraceError("QuotationMgrDAL","UpdateToNewVision","QMD016","Insert Transaction Failed : "+ exception.Message.ToString());
				throw new ApplicationException("Error adding Customer ",exception);
			}
			finally
			{
				if(conApp != null)
					conApp.Close();
			}

			return iRowsEffected;

		}
		
		public static DataSet  CreateBandQuotationReport(string appid ,string enterpriseid,string strQuotationNo,string strCustType)
		{
			StringBuilder strQry=new StringBuilder();
			DbConnection dbConn=null;
			DataSet dsBandQuotation = null;
			
					
			dbConn = DbConnectionManager.GetInstance().GetDbConnection(appid,enterpriseid);
			if(dbConn==null)
			{
				Logger.LogTraceError("QuotationMgrDAL","CreateBandQuotationReport","QMD009","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			if (strCustType=="C")
			{

				strQry.Append(" Select DISTINCT CQ.*, ");
				strQry.Append(" E.enterprise_name,E.contact_person e_contact_person, E.contact_telephone e_contact_telephone,");
 				strQry.Append(" E.address1 e_address1,E.address2 e_address2,E.country e_country,E.contact_telephone e_contact_telephone,");
				strQry.Append(" E.fax e_fax ,E.currency,E.zipcode e_zipcode,E.wt_increment_amt, ");
				strQry.Append(" C.cust_name c_cust_name, C.contact_person c_contact_person,C.email c_email,C.address1 c_address1, ");
				strQry.Append(" C.address2 c_address2, C.country c_country,C.zipcode c_zipcode,C.telephone c_telephone,C.fax c_fax, ");	
				strQry.Append(" CQS.service_code, S.service_description,CQS.service_charge_percent, CQS.service_charge_amt, ");	
				strQry.Append(" BD.band_description, BD.percent_discount,BZR.* ");
				
//	strQry.Append(" , CY.Conveyance_Code, CY.Conveyance_Description, CY.Length, CY.Breadth, CY.Height,CY.max_wt, ");
//	strQry.Append(" BZCR.Origin_Zone_Code Origin_Zone_Code_C, BZCR.Destination_Zone_Code Destination_Zone_Code_C,");
//	strQry.Append(" BZCR.Start_Price Start_Price_C, BZCR.Increment_Price Increment_Price_C ");
		
				strQry.Append(" From Customer_Quotation CQ INNER JOIN Enterprise E ON CQ.applicationid=E.applicationid and CQ.enterpriseid=E.enterpriseid ");
				strQry.Append(" INNER JOIN Customer C ON CQ.applicationid=C.applicationid and CQ.enterpriseid=C.enterpriseid and CQ.custid=C.custid ");
				strQry.Append(" INNER JOIN Base_Zone_Rates BZR ON CQ.applicationid=BZR.applicationid and CQ.enterpriseid=BZR.enterpriseid ");
	
//	strQry.Append(" INNER JOIN Conveyance CY ON CY.ApplicationId=E.ApplicationId and CY.EnterpriseId=E.EnterpriseId ");
//	strQry.Append(" INNER JOIN Base_Zone_Conveyance_Rates BZCR ON BZCR.applicationid=E.applicationid and BZCR.enterpriseid=E.Enterpriseid  and ");
//	strQry.Append(" CY.Conveyance_Code=BZCR.Conveyance_Code ");

				strQry.Append(" LEFT OUTER JOIN Band_Discount BD ON CQ.enterpriseid=BD.enterpriseid and CQ.applicationid = BD.applicationid and CQ.band_code = BD.band_code ");
				strQry.Append(" LEFT OUTER JOIN Customer_Quotation_Service CQS ON CQ.applicationid=CQS.applicationid and CQ.enterpriseid=CQS.enterpriseid and ");
				strQry.Append(" CQ.custid=CQS.custid  and CQ.quotation_no =CQS.quotation_no and CQ.quotation_version = CQS.quotation_version ");
				strQry.Append(" LEFT OUTER JOIN Service S ON CQ.applicationid=S.applicationid and CQ.enterpriseid=S.enterpriseid and CQS.service_code = S.service_code ");
				strQry.Append(" where CQ.quotation_no ='"+strQuotationNo+"'");

				dsBandQuotation = (DataSet)dbConn.ExecuteQuery(strQry.ToString(),ReturnType.DataSetType);

			}

			else if(strCustType=="A")
			{
				strQry.Append(" Select DISTINCT AQ.*, ");
				strQry.Append(" E.enterprise_name,E.contact_person e_contact_person,E.contact_telephone e_contact_telephone, ");
				strQry.Append(" E.address1 e_address1,E.address2 e_address2,E.country e_country,E.contact_telephone e_contact_telephone,");
				strQry.Append(" E.fax e_fax,E.currency, E.zipcode e_zipcode,E.wt_increment_amt, ");
				strQry.Append(" A.agent_name c_cust_name, A.contact_person c_contact_person,A.email c_email,A.agent_address1 c_address1, ");
				strQry.Append(" A.agent_address2 c_address2, A.country c_country, A.zipcode c_zipcode,A.telephone c_telephone, ");
				strQry.Append(" A.fax c_fax,AQS.service_code,S.service_description,AQS.service_charge_percent, ");
				strQry.Append(" AQS.service_charge_amt, BD.band_description,BD.percent_discount, BZR.* ");

//	strQry.Append(", CC.Conveyance_Code, CC.Conveyance_Description, CC.Length, CC.Breadth, CC.Height, CC.max_wt, ");
//	strQry.Append(" BZCR.Origin_Zone_Code Origin_Zone_Code_C, BZCR.Destination_Zone_Code Destination_Zone_Code_C,");
//	strQry.Append(" BZCR.Start_Price Start_Price_C, BZCR.Increment_Price Increment_Price_C ");

				strQry.Append(" FROM AGENT_QUOTATION AQ  INNER JOIN Enterprise E ON AQ.applicationid=E.applicationid and AQ.enterpriseid=E.enterpriseid ");
				strQry.Append(" INNER JOIN Agent A ON AQ.applicationid=A.applicationid and AQ.enterpriseid=A.enterpriseid and AQ.agentid=A.agentid ");
				strQry.Append(" INNER JOIN Base_Zone_Rates BZR ON AQ.applicationid=BZR.applicationid and AQ.enterpriseid=BZR.enterpriseid ");
//
//	strQry.Append(" INNER JOIN Conveyance CC ON CC.ApplicationId=E.ApplicationId and CC.EnterpriseId=E.EnterpriseId ");
//	strQry.Append(" INNER JOIN Base_Zone_Conveyance_Rates BZCR ON BZCR.applicationid=E.applicationid and BZCR.enterpriseid=E.Enterpriseid  and ");
//	strQry.Append(" CC.Conveyance_Code=BZCR.Conveyance_Code ");

				strQry.Append(" LEFT OUTER JOIN Agent_Quotation_Service AQS ON AQ.applicationid=AQS.applicationid and AQ.enterpriseid=AQS.enterpriseid and ");
				strQry.Append(" AQ.agentid=AQS.agentid  AND AQ.quotation_no =AQS.quotation_no and AQ.quotation_version = AQS.quotation_version ");
				strQry.Append(" LEFT OUTER JOIN Service S ON AQ.applicationid=S.applicationid and AQ.enterpriseid=S.enterpriseid and AQS.service_code = S.service_code ");
				strQry.Append(" LEFT OUTER JOIN Band_discount BD ON AQ.enterpriseid=BD.enterpriseid and AQ.applicationid = BD.applicationid and AQ.band_code= BD.band_code ");			
				strQry.Append(" WHERE AQ.quotation_no ='"+strQuotationNo+"'");

				dsBandQuotation = (DataSet)dbConn.ExecuteQuery(strQry.ToString(),ReturnType.DataSetType);

			}

			return dsBandQuotation;
		}

		public static DataSet GetVas(string appid ,string enterpriseid,string strQuotationNo,string strCustType)
		{
			string strQryVas=null;
			DbConnection dbConn=null;
			DataSet dsVas = null;

			dbConn = DbConnectionManager.GetInstance().GetDbConnection(appid,enterpriseid);
			if(dbConn==null)
			{
				Logger.LogTraceError("QuotationMgrDAL","GetVas","QMD009","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			if (strCustType =="C")
			{
				strQryVas = strQryVas+"Select CQV.*,E.currency,Vas.vas_description from customer_quotation_vas CQV,vas Vas,enterprise E ";
				strQryVas = strQryVas+"where CQV.vas_code =Vas.vas_code and CQV.quotation_no ="+"'"+strQuotationNo+"'"+" and " ; 
				strQryVas= strQryVas+"E.applicationid=CQV.applicationid and E.enterpriseid=CQV.enterpriseid";
			}
			else if (strCustType=="A")
			{
				strQryVas = strQryVas+"Select CQV.*,E.currency,Vas.vas_description from agent_quotation_vas CQV,vas Vas,enterprise E ";
				strQryVas = strQryVas+"where CQV.vas_code =Vas.vas_code and CQV.quotation_no ="+"'"+strQuotationNo+"'"+" and "; 
				strQryVas= strQryVas+"E.applicationid=CQV.applicationid and E.enterpriseid=CQV.enterpriseid";
			}
			dsVas = (DataSet)dbConn.ExecuteQuery(strQryVas.ToString(),ReturnType.DataSetType);
			
			return dsVas;
		}
	}
}