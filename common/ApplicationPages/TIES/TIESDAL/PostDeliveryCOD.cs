
using System;
using System.Data;
using com.common.classes;
using com.common.DAL;
using com.common.util;
using System.Text;
using com.ties.classes;
using System.Collections;
using System.Xml;

namespace TIESDAL
{
	/// <summary>
	/// Summary description for PostDeliveryCOD.
	/// </summary>
	public class PostDeliveryCOD
	{
		public PostDeliveryCOD()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		public static Double GetCODAmountVAS(String strAppID, String strEnterpriseID,int booking_No,String strConsig_No)
		{	
			DbConnection dbCon= null;
			IDbCommand dbcmd=null;
			DataSet dsDt = null;

			dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if (dbCon==null)
			{
				//	Logger.LogTraceError("ShipmentTrackingMgrDAL.cs","GetLatestDateFromShipment","ERR001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}


			StringBuilder strBuilder = new StringBuilder();
			strBuilder.Append("SELECT dbo.Shipment_VAS.vas_surcharge ");
			strBuilder.Append(" FROM dbo.Shipment INNER JOIN dbo.Shipment_VAS ON dbo.Shipment.applicationid = ");
			strBuilder.Append(" dbo.Shipment_VAS.applicationid AND dbo.Shipment.enterpriseid = ");
			strBuilder.Append(" dbo.Shipment_VAS.enterpriseid AND dbo.Shipment.booking_no = ");
			strBuilder.Append(" dbo.Shipment_VAS.booking_no AND dbo.Shipment.consignment_no = dbo.Shipment_VAS.consignment_no ");

			strBuilder.Append(" WHERE dbo.Shipment_VAS.vas_code = 'COD' AND dbo.Shipment.applicationid = '");
			strBuilder.Append(strAppID);
			strBuilder.Append("' AND dbo.Shipment.enterpriseid = '");
			strBuilder.Append(strEnterpriseID);
			strBuilder.Append("' AND dbo.Shipment.booking_no = ");
			strBuilder.Append(booking_No);
			strBuilder.Append(" AND dbo.Shipment.consignment_no = '");
			strBuilder.Append(strConsig_No);
			strBuilder.Append("'");

			String strSQLQuery = strBuilder.ToString();
			dbcmd = dbCon.CreateCommand(strSQLQuery,CommandType.Text);
 
			try
			{
				dsDt = (DataSet)dbCon.ExecuteQuery(dbcmd,com.common.DAL.ReturnType.DataSetType);  
			}
			catch(ApplicationException appException)
			{
				//	Logger.LogTraceError("ShipmentTrackingMgrDAL.cs","GetLatestDateFromShipment","ERR002","Error in Query String "+appException.Message);
				throw appException;
			}



				if(dsDt.Tables[0].Rows.Count>0)
				{
					if(!dsDt.Tables[0].Rows[0][0].GetType().Equals(System.Type.GetType("System.DBNull")))
						return Double.Parse(dsDt.Tables[0].Rows[0][0].ToString());
					else
						return 0.00;
				}
				else
					return 0.00;

			}
		

		public static DataSet GetCODUpdateUser(String strAppID, String strEnterpriseID,int booking_No,String strConsig_No,String strAudit_Type)
		{	
			DbConnection dbCon= null;
			IDbCommand dbcmd=null;
			DataSet dsDt = null;

			dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if (dbCon==null)
			{
				//	Logger.LogTraceError("ShipmentTrackingMgrDAL.cs","GetLatestDateFromShipment","ERR001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}


			StringBuilder strBuilder = new StringBuilder();
			strBuilder.Append(" SELECT top 1 dbo.User_Master.user_name, dbo.Shipment_Audit_Trial.last_updated ");
			strBuilder.Append(" FROM dbo.Shipment_Audit_Trial LEFT OUTER JOIN ");
			strBuilder.Append(" dbo.User_Master ON dbo.Shipment_Audit_Trial.last_userid = dbo.User_Master.userid RIGHT OUTER JOIN ");
			strBuilder.Append(" dbo.Shipment ON dbo.Shipment_Audit_Trial.applicationid = dbo.Shipment.applicationid AND ");
			strBuilder.Append(" dbo.Shipment_Audit_Trial.enterpriseid = dbo.Shipment.enterpriseid AND dbo.Shipment_Audit_Trial.booking_no = dbo.Shipment.booking_no AND ");
			strBuilder.Append(" dbo.Shipment_Audit_Trial.consignment_no = dbo.Shipment.consignment_no AND dbo.User_Master.applicationid = dbo.Shipment.applicationid AND ");
			strBuilder.Append(" dbo.User_Master.enterpriseid = dbo.Shipment.enterpriseid ");
                       
			strBuilder.Append(" WHERE dbo.Shipment.applicationid = '");
			strBuilder.Append(strAppID);
			strBuilder.Append("' AND dbo.Shipment.enterpriseid = '");
			strBuilder.Append(strEnterpriseID);
			strBuilder.Append("' AND dbo.Shipment.booking_no = ");
			strBuilder.Append(booking_No);
			strBuilder.Append(" AND dbo.Shipment.consignment_no = '");
			strBuilder.Append(strConsig_No);
			strBuilder.Append("' AND dbo.Shipment_Audit_Trial.audit_type = '");
			strBuilder.Append(strAudit_Type);
			strBuilder.Append("' order by dbo.Shipment_Audit_Trial.last_updated DESC ");

			String strSQLQuery = strBuilder.ToString();
			dbcmd = dbCon.CreateCommand(strSQLQuery,CommandType.Text);
 
			try
			{
				dsDt = (DataSet)dbCon.ExecuteQuery(dbcmd,com.common.DAL.ReturnType.DataSetType);  
			}
			catch(ApplicationException appException)
			{
				//	Logger.LogTraceError("ShipmentTrackingMgrDAL.cs","GetLatestDateFromShipment","ERR002","Error in Query String "+appException.Message);
				throw appException;
			}

				return dsDt;
		}
		

		public static String GetCutOffTime(String strAppID, String strEnterpriseID,int booking_No,String strConsig_No,String strZipCode)
		{	
			DbConnection dbCon= null;
			IDbCommand dbcmd=null;
			DataSet dsDt = null;

			dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if (dbCon==null)
			{
				//	Logger.LogTraceError("ShipmentTrackingMgrDAL.cs","GetLatestDateFromShipment","ERR001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}


			StringBuilder strBuilder = new StringBuilder();
			strBuilder.Append(" SELECT dbo.Zipcode.cutoff_time FROM dbo.Shipment LEFT OUTER JOIN ");
			strBuilder.Append(" dbo.Zipcode ON dbo.Shipment.sender_zipcode = dbo.Zipcode.zipcode AND dbo.Shipment.applicationid = dbo.Zipcode.applicationid AND ");
			strBuilder.Append(" dbo.Shipment.enterpriseid = dbo.Zipcode.enterpriseid ");

			strBuilder.Append(" WHERE dbo.Shipment.applicationid = '");
			strBuilder.Append(strAppID);
			strBuilder.Append("' AND dbo.Shipment.enterpriseid = '");
			strBuilder.Append(strEnterpriseID);
			strBuilder.Append("' AND dbo.Shipment.booking_no = ");
			strBuilder.Append(booking_No);
			strBuilder.Append(" AND dbo.Shipment.consignment_no = '");
			strBuilder.Append(strConsig_No);
			strBuilder.Append("'  AND dbo.Shipment.sender_zipcode = '");
			strBuilder.Append(strZipCode);
			strBuilder.Append("'");

			String strSQLQuery = strBuilder.ToString();
			dbcmd = dbCon.CreateCommand(strSQLQuery,CommandType.Text);
 
			try
			{
				dsDt = (DataSet)dbCon.ExecuteQuery(dbcmd,com.common.DAL.ReturnType.DataSetType);  
			}
			catch(ApplicationException appException)
			{
				//	Logger.LogTraceError("ShipmentTrackingMgrDAL.cs","GetLatestDateFromShipment","ERR002","Error in Query String "+appException.Message);
				throw appException;
			}

			if(dsDt.Tables[0].Rows.Count>0)
				return dsDt.Tables[0].Rows[0][0].ToString();
			else
				return "0";
		}
		

		public static String GetStateName(String strAppID, String strEnterpriseID,String strCountry,String strStateCode)
		{	
			DbConnection dbCon= null;
			IDbCommand dbcmd=null;
			DataSet dsDt = null;

			dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if (dbCon==null)
			{
				//	Logger.LogTraceError("ShipmentTrackingMgrDAL.cs","GetLatestDateFromShipment","ERR001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}


			StringBuilder strBuilder = new StringBuilder();
			strBuilder.Append(" SELECT state_name FROM dbo.State ");

			strBuilder.Append(" WHERE applicationid = '");
			strBuilder.Append(strAppID);
			strBuilder.Append("' AND enterpriseid = '");
			strBuilder.Append(strEnterpriseID);
			strBuilder.Append("' AND country = '");
			strBuilder.Append(strCountry);
			strBuilder.Append("' AND state_code = '");
			strBuilder.Append(strStateCode);
			strBuilder.Append("'");

			String strSQLQuery = strBuilder.ToString();
			dbcmd = dbCon.CreateCommand(strSQLQuery,CommandType.Text);
 
			try
			{
				dsDt = (DataSet)dbCon.ExecuteQuery(dbcmd,com.common.DAL.ReturnType.DataSetType);  
			}
			catch(ApplicationException appException)
			{
				//	Logger.LogTraceError("ShipmentTrackingMgrDAL.cs","GetLatestDateFromShipment","ERR002","Error in Query String "+appException.Message);
				throw appException;
			}

			if(dsDt.Tables[0].Rows.Count>0)
				return dsDt.Tables[0].Rows[0][0].ToString();
			else
				return "0";
		}
		

		public static String GetProvinceName(String strAppID, String strEnterpriseID,String strCountry,String strZipCode)
		{	
			DbConnection dbCon= null;
			IDbCommand dbcmd=null;
			DataSet dsDt = null;

			dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if (dbCon==null)
			{
				//	Logger.LogTraceError("ShipmentTrackingMgrDAL.cs","GetLatestDateFromShipment","ERR001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}


			StringBuilder strBuilder = new StringBuilder();

			strBuilder.Append(" SELECT DISTINCT dbo.State.state_name ");
			strBuilder.Append(" FROM dbo.Zipcode LEFT OUTER JOIN ");
			strBuilder.Append(" dbo.State ON dbo.Zipcode.applicationid = dbo.State.applicationid AND dbo.Zipcode.enterpriseid = dbo.State.enterpriseid AND ");
			strBuilder.Append(" dbo.Zipcode.country = dbo.State.country AND dbo.Zipcode.state_code = dbo.State.state_code ");
			strBuilder.Append(" WHERE Zipcode.applicationid  = '");
			strBuilder.Append(strAppID);
			strBuilder.Append("' AND Zipcode.enterpriseid = '");
			strBuilder.Append(strEnterpriseID);
			strBuilder.Append("' AND Zipcode.country = '");
			strBuilder.Append(strCountry);
			strBuilder.Append("' AND  Zipcode.zipcode = '");
			strBuilder.Append(strZipCode);
			strBuilder.Append("'");

			String strSQLQuery = strBuilder.ToString();
			dbcmd = dbCon.CreateCommand(strSQLQuery,CommandType.Text);
 
			try
			{
				dsDt = (DataSet)dbCon.ExecuteQuery(dbcmd,com.common.DAL.ReturnType.DataSetType);  
			}
			catch(ApplicationException appException)
			{
				//	Logger.LogTraceError("ShipmentTrackingMgrDAL.cs","GetLatestDateFromShipment","ERR002","Error in Query String "+appException.Message);
				throw appException;
			}

			if(dsDt.Tables[0].Rows.Count>0)
				return dsDt.Tables[0].Rows[0][0].ToString();
			else
				return "0";
		}
		
		//By Aoo 01/04/02008
		public static int GetUserRoleID(String strAppID, String strEnterpriseID,String strUserID)
		{
			DbConnection dbCon = null;
			IDbCommand dbcmd = null;
			DataSet dsDt = null;

			dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if (dbCon==null)
			{
				throw new ApplicationException("Connection to Database failed",null);
			}


			StringBuilder strBuilder = new StringBuilder();
			strBuilder.Append(" SELECT roleid FROM User_Role_Relation ");
			strBuilder.Append(" WHERE applicationid = '");
			strBuilder.Append(strAppID);
			strBuilder.Append("' AND enterpriseid = '");
			strBuilder.Append(strEnterpriseID);
			strBuilder.Append("' AND userid = '");
			strBuilder.Append(strUserID);
			strBuilder.Append("'");


			String strSQLQuery = strBuilder.ToString();
			dbcmd = dbCon.CreateCommand(strSQLQuery,CommandType.Text);
 
			try
			{
				dsDt = (DataSet)dbCon.ExecuteQuery(dbcmd,com.common.DAL.ReturnType.DataSetType);  
			}
			catch(ApplicationException appException)
			{
				throw appException;
			}
 
			return Convert.ToInt16(dsDt.Tables[0].Rows[0][0].ToString());
		}

		public static int GetUserRole(String strAppID, String strEnterpriseID,String strUserID,String strUserRole)
		{	
			DbConnection dbCon= null;
			IDbCommand dbcmd=null;
			DataSet dsDt = null;

			dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if (dbCon==null)
			{
				//	Logger.LogTraceError("ShipmentTrackingMgrDAL.cs","GetLatestDateFromShipment","ERR001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}


			StringBuilder strBuilder = new StringBuilder();
			strBuilder.Append(" SELECT COUNT(*) FROM  dbo.Role_Master RIGHT OUTER JOIN ");
			strBuilder.Append(" dbo.User_Role_Relation ON dbo.Role_Master.applicationid = dbo.User_Role_Relation.applicationid AND ");
			strBuilder.Append(" dbo.Role_Master.enterpriseid = dbo.User_Role_Relation.enterpriseid AND dbo.Role_Master.roleid = dbo.User_Role_Relation.roleid RIGHT OUTER JOIN ");
			strBuilder.Append(" dbo.User_Master ON dbo.User_Role_Relation.applicationid = dbo.User_Master.applicationid AND ");
			strBuilder.Append(" dbo.User_Role_Relation.enterpriseid = dbo.User_Master.enterpriseid AND dbo.User_Role_Relation.userid = dbo.User_Master.userid ");

			strBuilder.Append(" WHERE User_Master.applicationid = '");
			strBuilder.Append(strAppID);
			strBuilder.Append("' AND User_Master.enterpriseid = '");
			strBuilder.Append(strEnterpriseID);
			strBuilder.Append("' AND Role_Master.role_name = '");
			strBuilder.Append(strUserRole);
			strBuilder.Append("' AND User_Master.userid = '");
			strBuilder.Append(strUserID);
			strBuilder.Append("'");


			String strSQLQuery = strBuilder.ToString();
			dbcmd = dbCon.CreateCommand(strSQLQuery,CommandType.Text);
 
			try
			{
				dsDt = (DataSet)dbCon.ExecuteQuery(dbcmd,com.common.DAL.ReturnType.DataSetType);  
			}
			catch(ApplicationException appException)
			{
				//	Logger.LogTraceError("ShipmentTrackingMgrDAL.cs","GetLatestDateFromShipment","ERR002","Error in Query String "+appException.Message);
				throw appException;
			}
 
			return Convert.ToInt16(dsDt.Tables[0].Rows[0][0].ToString());
		}
		
		//by sittichai  14/03/2008
		public static bool CheckInvoiceDate(String strAppID,String strEnterpriseID,String strConsing_No)
		{
			DbConnection dbCon= null;
			IDbCommand dbcmd=null;
			DataSet dsDt = null;

			dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if (dbCon==null)
			{
				//	Logger.LogTraceError("ShipmentTrackingMgrDAL.cs","GetLatestDateFromShipment","ERR001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			StringBuilder strBuilder = new StringBuilder();

			strBuilder.Append(" select count(*) from shipment where shipment.invoice_date <> null or shipment.invoice_date <> '' ");
			strBuilder.Append(" and shipment.consignment_no ='" + strConsing_No+ "'");
			strBuilder.Append(" and shipment.enterpriseid ='"+strEnterpriseID+"'");
			strBuilder.Append(" and shipment.applicationid = '" +strAppID+ "'");

			String strSQLQuery = strBuilder.ToString();
			dbcmd = dbCon.CreateCommand(strSQLQuery,CommandType.Text);
 
			try
			{
				dsDt = (DataSet)dbCon.ExecuteQuery(dbcmd,com.common.DAL.ReturnType.DataSetType);  
			}
			catch(ApplicationException appException)
			{
				//	Logger.LogTraceError("ShipmentTrackingMgrDAL.cs","GetLatestDateFromShipment","ERR002","Error in Query String "+appException.Message);
				throw appException;
			}

			if(dsDt.Tables[0].Rows.Count>0)
			{
				if(!dsDt.Tables[0].Rows[0][0].GetType().Equals(System.Type.GetType("System.DBNull")))
					if(Int32.Parse(dsDt.Tables[0].Rows[0][0].ToString())>0)
						return false;
					else
						return true;
			}
			return true;

		}



//��������Ǩ��
		public static Double GetCODAmountVASXXX(String strAppID, String strEnterpriseID,int booking_No,String strConsig_No)
		{	
			DbConnection dbCon= null;
			IDbCommand dbcmd=null;
			DataSet dsDt = null;

			dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if (dbCon==null)
			{
				//	Logger.LogTraceError("ShipmentTrackingMgrDAL.cs","GetLatestDateFromShipment","ERR001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}


			StringBuilder strBuilder = new StringBuilder();
			strBuilder.Append("SELECT dbo.Shipment_VAS.vas_surcharge ");
			strBuilder.Append(" FROM dbo.Shipment INNER JOIN dbo.Shipment_VAS ON dbo.Shipment.applicationid = ");
			strBuilder.Append(" dbo.Shipment_VAS.applicationid AND dbo.Shipment.enterpriseid = ");
			strBuilder.Append(" dbo.Shipment_VAS.enterpriseid AND dbo.Shipment.booking_no = ");
			strBuilder.Append(" dbo.Shipment_VAS.booking_no AND dbo.Shipment.consignment_no = dbo.Shipment_VAS.consignment_no ");

			strBuilder.Append(" WHERE dbo.Shipment_VAS.vas_code = 'COD' AND dbo.Shipment.applicationid = '");
			strBuilder.Append(strAppID);
			strBuilder.Append("' AND dbo.Shipment.enterpriseid = '");
			strBuilder.Append(strEnterpriseID);
			strBuilder.Append("' AND dbo.Shipment.booking_no = ");
			strBuilder.Append(booking_No);
			strBuilder.Append(" AND dbo.Shipment.consignment_no = '");
			strBuilder.Append(strConsig_No);
			strBuilder.Append("'");

			String strSQLQuery = strBuilder.ToString();
			dbcmd = dbCon.CreateCommand(strSQLQuery,CommandType.Text);
 
			try
			{
				dsDt = (DataSet)dbCon.ExecuteQuery(dbcmd,com.common.DAL.ReturnType.DataSetType);  
			}
			catch(ApplicationException appException)
			{
				//	Logger.LogTraceError("ShipmentTrackingMgrDAL.cs","GetLatestDateFromShipment","ERR002","Error in Query String "+appException.Message);
				throw appException;
			}

			if(dsDt.Tables[0].Rows.Count>0)
				return (Double)dsDt.Tables[0].Rows[0][0];
			else
				return 0.00;
		}
		
//��������Ǩ��
		public static DataSet GetPostDeliveryCOD2(String strAppID, String strEnterpriseID)
		{	
			DbConnection dbCon= null;
			IDbCommand dbcmd=null;
			DataSet dsDt = null;

			dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
 
			if (dbCon==null)
			{
			//	Logger.LogTraceError("ShipmentTrackingMgrDAL.cs","GetLatestDateFromShipment","ERR001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}


			StringBuilder strBuilder = new StringBuilder();
			strBuilder.Append("select * from shipment where applicationid = '"+strAppID+"' and enterpriseid = '"+strEnterpriseID+"'");
			
			String strSQLQuery = strBuilder.ToString();
			dbcmd = dbCon.CreateCommand(strSQLQuery,CommandType.Text);
 
			try
			{
				dsDt = (DataSet)dbCon.ExecuteQuery(dbcmd,com.common.DAL.ReturnType.DataSetType);  
			}
			catch(ApplicationException appException)
			{
			//	Logger.LogTraceError("ShipmentTrackingMgrDAL.cs","GetLatestDateFromShipment","ERR002","Error in Query String "+appException.Message);
				throw appException;
			}
			return dsDt;		
		}
		

		public static SessionDS GetPostDeliveryCODDS(String strAppID, String strEnterpriseID, int iCurrent, int iDSRecSize,DataSet dsQueryParam)
		{

			SessionDS sessionDS = null;


			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("SysDataMgrDAL","PostDeliveryCOD","EDB101","DbConnection object is null!!");
				return sessionDS;
			}

			StringBuilder strBuilder = new StringBuilder();

			//strBuilder.Append("select * from shipment");
			strBuilder.Append(" SELECT Shipment.*, Shipment_Tracking.tracking_datetime FROM Shipment LEFT OUTER JOIN ");
			strBuilder.Append(" Shipment_Tracking ON Shipment.applicationid = Shipment_Tracking.applicationid AND  ");
			strBuilder.Append(" Shipment.enterpriseid = Shipment_Tracking.enterpriseid AND Shipment.booking_no = Shipment_Tracking.booking_no AND ");
			strBuilder.Append(" Shipment.consignment_no = Shipment_Tracking.consignment_no ");           
            
			strBuilder.Append(" where isnull(shipment_tracking.deleted,'N')<>'Y' and Shipment.applicationid = '");
			strBuilder.Append(strAppID);
			strBuilder.Append("' and Shipment.enterpriseid = '");
			strBuilder.Append(strEnterpriseID);
			strBuilder.Append("' and Shipment_Tracking.status_code = 'POD' ");
			strBuilder.Append(" and Shipment.cod_amount > 0 ");


			DataRow drEach = dsQueryParam.Tables[0].Rows[0];

			if(Utility.IsNotDBNull(drEach["consignment_no"]) && drEach["consignment_no"].ToString()!="")
			{
				String strConsig_no = (String) drEach["consignment_no"];
				strBuilder.Append(" and Shipment.consignment_no like ");
				strBuilder.Append("'%");
				strBuilder.Append(strConsig_no);
				strBuilder.Append("%' ");
			}

			if(Utility.IsNotDBNull(drEach["ref_no"]) && drEach["ref_no"].ToString()!="")
			{
				String strRefNo = (String) drEach["ref_no"];
				strBuilder.Append(" and Shipment.ref_no like ");
				strBuilder.Append("'%");
				strBuilder.Append(strRefNo);
				strBuilder.Append("%' ");
			}
			
			if(Utility.IsNotDBNull(drEach["payerid"]) && drEach["payerid"].ToString()!="")
			{
				String strCustID = (String) drEach["payerid"];
				strBuilder.Append(" and Shipment.payerid like ");
				//strBuilder.Append("N'%"); //To accept Unicode
				strBuilder.Append("'%");
				strBuilder.Append(strCustID);
				strBuilder.Append("%' ");
			}


			if(Utility.IsNotDBNull(drEach["route_code"]) && drEach["route_code"].ToString()!="")
			{
				String strRouteCode = (String) drEach["route_code"];
				strBuilder.Append(" and Shipment.route_code like ");
				strBuilder.Append("'%");
				strBuilder.Append(strRouteCode);
				strBuilder.Append("%' ");
			}

			if(Utility.IsNotDBNull(drEach["destination_station"]) && drEach["destination_station"].ToString()!="")
			{
				String strDestination_Station = (String) drEach["destination_station"];
				strBuilder.Append(" and Shipment.destination_station like ");
				strBuilder.Append("'%");
				strBuilder.Append(strDestination_Station);
				strBuilder.Append("%' ");
			}

			String sdtFrom="";			
			String sdtTo="";

			if(Utility.IsNotDBNull(drEach["actual_POD_From"]) && drEach["actual_POD_From"].ToString()!="")
			{
				DateTime dtFrom=System.Convert.ToDateTime(drEach["actual_POD_From"]);
				sdtFrom=Utility.DateFormat(strAppID,strEnterpriseID, dtFrom ,DTFormat.DateTime);
			}

			if(Utility.IsNotDBNull(drEach["actual_POD_To"]) && drEach["actual_POD_To"].ToString()!="")
			{
				DateTime dtTo=System.Convert.ToDateTime(drEach["actual_POD_To"]);
				sdtTo=Utility.DateFormat(strAppID,strEnterpriseID, dtTo ,DTFormat.DateTime);
			}
//			strBuilder.Append(" and Shipment_Tracking.tracking_datetime = " +sdtFrom+ " ");

			if (sdtFrom !="" && sdtTo !="")
				strBuilder.Append(" and Shipment.act_delivery_date between "+sdtFrom+ " and "+sdtTo+" ");

			if (sdtFrom =="" && sdtTo !="")
				strBuilder.Append(" and Shipment.act_delivery_date <= "+sdtTo+" ");

			if (sdtFrom !="" && sdtTo =="")
				strBuilder.Append(" and Shipment.act_delivery_date >= "+sdtFrom+ " ");

			strBuilder.Append(" order by Shipment.consignment_no ASC ");

			String strSQLQuery = strBuilder.ToString();
			
			sessionDS = dbCon.ExecuteQuery(strSQLQuery,iCurrent,iDSRecSize,"PostDeliveryCOD");

			return  sessionDS;
	
		}


//��������Ǩ��
		public static SessionDS GetPostDeliveryShipment(String strAppID, String strEnterpriseID)
		{
			SessionDS dsStatusCode = null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("SysDataMgrDAL","GetStatusCodeDS","EDB101","DbConnection object is null!!");
				return dsStatusCode;
			}

			String strSQLQuery = "select * from shipment";

/*			String strSQLQuery = "select status_code,status_description,Status_type,status_close,invoiceable,system_code from Status_Code where ";			
			strSQLQuery += "applicationid = '"+strAppID+"' and enterpriseid = '"+strEnterpriseID+"' and ";
 */		
			dsStatusCode = (SessionDS)dbCon.ExecuteQuery(strSQLQuery,ReturnType.DataSetType);
			return  dsStatusCode;
			//return (DataSet)dbCon.ExecuteQuery(strSQLQuery,ReturnType.DataSetType);
	
		}

		
		public static SessionDS GetEmptyShipmentDS(int iNumRows)
		{
			

			DataTable dtShipment = new DataTable();
 
			dtShipment.Columns.Add(new DataColumn("applicationid", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("enterpriseid", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("booking_no", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("consignment_no", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("ref_no", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("booking_datetime", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("payerid", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("payer_type", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("new_account", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("payer_name", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("payer_address1", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("payer_address2", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("payer_zipcode", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("payer_country", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("payer_telephone", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("payer_fax", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("payment_mode", typeof(string)));

			dtShipment.Columns.Add(new DataColumn("sender_name", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("sender_address1", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("sender_address2", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("sender_zipcode", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("sender_country", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("sender_telephone", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("sender_fax", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("sender_contact_person", typeof(string)));
			
			dtShipment.Columns.Add(new DataColumn("recipient_name", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("recipient_address1", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("recipient_address2", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("recipient_zipcode", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("recipient_country", typeof(string)));
			
			dtShipment.Columns.Add(new DataColumn("tot_wt", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("recipient_telephone", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("tot_dim_wt", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("recipient_fax", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("tot_pkg", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("service_code", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("recipient_contact_person", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("declare_value", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("chargeable_wt", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("insurance_surcharge", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("max_insurance_cover", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("act_pickup_datetime", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("est_delivery_datetime", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("percent_dv_additional", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("act_delivery_date", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("payment_type", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("return_pod_slip", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("shipment_type", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("origin_state_code", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("invoice_no", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("destination_state_code", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("invoice_amt", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("tot_freight_charge", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("invoice_date", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("tot_vas_surcharge", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("debit_amt", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("credit_amt", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("cash_collected", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("last_status_code", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("last_exception_code", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("last_status_datetime", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("shpt_manifest_datetime", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("delivery_manifested", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("route_code", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("quotation_no", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("quotation_version", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("commodity_code", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("esa_surcharge", typeof(string)));

			dtShipment.Columns.Add(new DataColumn("agent_pup_cost_consignment", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("agent_pup_cost_wt", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("agent_del_cost_consignment", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("agent_del_cost_wt", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("remark", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("mbg", typeof(string)));

			dtShipment.Columns.Add(new DataColumn("cod_amount", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("origin_station", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("destination_station", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("return_invoice_hc", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("est_hc_return_datetime", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("act_hc_return_datetime", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("cod_amt_collected_datetime", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("cod_amt_collected_cash", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("cod_amt_collected_check", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("cod_amt_collected_other", typeof(string)));
			
			dtShipment.Columns.Add(new DataColumn("wh_tax_amt", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("net_cod_amt", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("total_cod_amt", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("cod_amt_verified_datetime_cash", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("cod_amt_verified_datetime_check", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("cod_amt_verified_datetime_other", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("cod_remittance_to_customer_datetime_cash", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("cod_remittance_to_customer_datetime_check", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("cod_remittance_to_customer_datetime_other", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("cod_adv_to_customer", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("cod_remark", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("cod_verified_remark", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("cod_remittance_remark", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("cod_refused", typeof(string)));

			dtShipment.Columns.Add(new DataColumn("tracking_datetime", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("actual_POD_From", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("actual_POD_To", typeof(string)));
			



			for(int i=0; i < iNumRows; i++)
			{
				DataRow drEach = dtShipment.NewRow();
				dtShipment.Rows.Add(drEach);
			}

			DataSet dsShipment = new DataSet();
			dsShipment.Tables.Add(dtShipment);

			SessionDS sessionDS = new SessionDS();
			sessionDS.ds = dsShipment;
			sessionDS.DataSetRecSize = dsShipment.Tables[0].Rows.Count;
			sessionDS.QueryResultMaxSize = sessionDS.DataSetRecSize;

			return  sessionDS;
	
		}


		public static int UpdatePostDeliveryShipment(String strAppId,String strEnterpriseId,String strBooking_no,String strConsig_no,DataSet dsShipment,bool boolUpdateUserCODAmountCollected)
		{
			int iRowsEffected = 0;

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppId,strEnterpriseId);
   
			if(dbCon==null)
			{
				Logger.LogTraceError("SysDataMgrDAL","UpdateRouteCode","R001","DbConnection object is null!!");
				return iRowsEffected;
			}

			int iRows = dsShipment.Tables[0].Rows.Count;
			DataRow drEach = dsShipment.Tables[0].Rows[0];  
 
			try
			{

				StringBuilder strBuilder = new StringBuilder();

				strBuilder.Append("Update shipment set ");
				
					strBuilder.Append(" cod_refused='");
					strBuilder.Append(Utility.ReplaceSingleQuote(drEach["cod_refused"].ToString()));
					strBuilder.Append("'");


				if((!drEach["cod_amt_collected_datetime"].GetType().Equals(System.Type.GetType("System.DBNull")))&&boolUpdateUserCODAmountCollected)
				{

					if(!drEach["cod_amt_collected_datetime"].GetType().Equals(System.Type.GetType("System.DBNull")))
					{
						strBuilder.Append(" ,cod_amt_collected_datetime='");
						strBuilder.Append(Convert.ToDateTime(drEach["cod_amt_collected_datetime"]));
						strBuilder.Append("'");
					}
					else
						strBuilder.Append(" ,cod_amt_collected_datetime=null ");
				}

				Decimal cod_amt_collected_cash=0,cod_amt_collected_check=0,cod_amt_collected_other=0,
					wh_tax_amt=0,net_cod_amt=0,total_cod_amt=0;


				if((!drEach["cod_amt_collected_cash"].GetType().Equals(System.Type.GetType("System.DBNull")))&&boolUpdateUserCODAmountCollected)
				{
					strBuilder.Append(" ,cod_amt_collected_cash=");

					cod_amt_collected_cash = Convert.ToDecimal(Utility.ReplaceSingleQuote(drEach["cod_amt_collected_cash"].ToString()));

					strBuilder.Append(cod_amt_collected_cash);
				}


				if((!drEach["cod_amt_collected_check"].GetType().Equals(System.Type.GetType("System.DBNull")))&&boolUpdateUserCODAmountCollected)
				{
					strBuilder.Append(" ,cod_amt_collected_check=");
					cod_amt_collected_check = Convert.ToDecimal(Utility.ReplaceSingleQuote(drEach["cod_amt_collected_check"].ToString()));
					strBuilder.Append(cod_amt_collected_check);
				}				
				

				if((!drEach["cod_amt_collected_other"].GetType().Equals(System.Type.GetType("System.DBNull")))&&boolUpdateUserCODAmountCollected)
				{
					strBuilder.Append(" ,cod_amt_collected_other=");

						cod_amt_collected_other = Convert.ToDecimal(Utility.ReplaceSingleQuote(drEach["cod_amt_collected_other"].ToString()));
						strBuilder.Append(cod_amt_collected_other);
				}			


				if((!drEach["wh_tax_amt"].GetType().Equals(System.Type.GetType("System.DBNull")))&&boolUpdateUserCODAmountCollected)
				{
					strBuilder.Append(" ,wh_tax_amt=");

						wh_tax_amt = Convert.ToDecimal(Utility.ReplaceSingleQuote(drEach["wh_tax_amt"].ToString()));
						strBuilder.Append(wh_tax_amt);

				}			

//�ӹǳ���ͧ��ѧ
				if((!drEach["net_cod_amt"].GetType().Equals(System.Type.GetType("System.DBNull")))&&boolUpdateUserCODAmountCollected)
				{  

					strBuilder.Append(" ,net_cod_amt=");

						net_cod_amt = Convert.ToDecimal(Utility.ReplaceSingleQuote(drEach["net_cod_amt"].ToString()));
						strBuilder.Append(net_cod_amt);

				}

				if((!drEach["total_cod_amt"].GetType().Equals(System.Type.GetType("System.DBNull")))&&boolUpdateUserCODAmountCollected)
				{
					strBuilder.Append(" ,total_cod_amt=");

					total_cod_amt = Convert.ToDecimal(Utility.ReplaceSingleQuote(drEach["total_cod_amt"].ToString()));
					strBuilder.Append(total_cod_amt);
				}

				//VERIFIED
				if(!drEach["cod_amt_verified_datetime_cash"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					if(Convert.ToString(drEach["cod_amt_verified_datetime_cash"])=="1/1/1900 12:00:00 AM")
						strBuilder.Append(" ,cod_amt_verified_datetime_cash=null ");
					else
					{
						strBuilder.Append(" ,cod_amt_verified_datetime_cash='");
						strBuilder.Append(Convert.ToDateTime(drEach["cod_amt_verified_datetime_cash"]));
						strBuilder.Append("'");
					}
				}
				
				if(!drEach["cod_amt_verified_datetime_check"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					if(Convert.ToString(drEach["cod_amt_verified_datetime_check"])=="1/1/1900 12:00:00 AM")
						strBuilder.Append(" ,cod_amt_verified_datetime_check=null ");
					else
					{
						strBuilder.Append(" ,cod_amt_verified_datetime_check='");
						strBuilder.Append(Convert.ToDateTime(Utility.ReplaceSingleQuote(drEach["cod_amt_verified_datetime_check"].ToString())));
						strBuilder.Append("'");
					}
				}

				if(!drEach["cod_amt_verified_datetime_other"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					if(Convert.ToString(drEach["cod_amt_verified_datetime_other"])=="1/1/1900 12:00:00 AM")
						strBuilder.Append(" ,cod_amt_verified_datetime_other=null ");
					else
					{
						strBuilder.Append(" ,cod_amt_verified_datetime_other='");
						strBuilder.Append(Convert.ToDateTime(Utility.ReplaceSingleQuote(drEach["cod_amt_verified_datetime_other"].ToString())));
						strBuilder.Append("'");
					}
				}

				//REMITTANCE
				if(!drEach["cod_remittance_to_customer_datetime_cash"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					if(Convert.ToString(drEach["cod_remittance_to_customer_datetime_cash"])=="1/1/1900 12:00:00 AM")
						strBuilder.Append(" ,cod_remittance_to_customer_datetime_cash=null ");
					else
					{
						strBuilder.Append(" ,cod_remittance_to_customer_datetime_cash='");
						strBuilder.Append(Convert.ToDateTime(Utility.ReplaceSingleQuote(drEach["cod_remittance_to_customer_datetime_cash"].ToString())));
						strBuilder.Append("'");
					}
				}

				if(!drEach["cod_remittance_to_customer_datetime_check"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					if(Convert.ToString(drEach["cod_remittance_to_customer_datetime_check"])=="1/1/1900 12:00:00 AM")
						strBuilder.Append(" ,cod_remittance_to_customer_datetime_check=null ");
					else
					{
						strBuilder.Append(" ,cod_remittance_to_customer_datetime_check='");
						strBuilder.Append(Convert.ToDateTime(Utility.ReplaceSingleQuote(drEach["cod_remittance_to_customer_datetime_check"].ToString())));
						strBuilder.Append("'");
					}
				}

				if(!drEach["cod_remittance_to_customer_datetime_other"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					if(Convert.ToString(drEach["cod_remittance_to_customer_datetime_other"])=="1/1/1900 12:00:00 AM")
						strBuilder.Append(" ,cod_remittance_to_customer_datetime_other=null ");
					else
					{
						strBuilder.Append(" ,cod_remittance_to_customer_datetime_other='");
						strBuilder.Append(Convert.ToDateTime(Utility.ReplaceSingleQuote(drEach["cod_remittance_to_customer_datetime_other"].ToString())));
						strBuilder.Append("'");
					}
				}

				if(!drEach["cod_adv_to_customer"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					strBuilder.Append(" ,cod_adv_to_customer=");
					strBuilder.Append(Convert.ToDecimal(Utility.ReplaceSingleQuote(drEach["cod_adv_to_customer"].ToString())));
				}

				//REMARKS

				if(!drEach["cod_remark"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					strBuilder.Append(" ,cod_remark=N'");
					
					strBuilder.Append(Utility.ReplaceSingleQuote(drEach["cod_remark"].ToString()));
					strBuilder.Append("'");
				}

				if(!drEach["cod_verified_remark"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					strBuilder.Append(" ,cod_verified_remark=N'");
					
					strBuilder.Append(Utility.ReplaceSingleQuote(drEach["cod_verified_remark"].ToString()));
					strBuilder.Append("'");
				}

				if(!drEach["cod_remittance_remark"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					strBuilder.Append(" ,cod_remittance_remark=N'");
					
					strBuilder.Append(Utility.ReplaceSingleQuote(drEach["cod_remittance_remark"].ToString()));
					strBuilder.Append("'");
				}


				strBuilder.Append(" where  applicationid='");
				strBuilder.Append(strAppId);
				strBuilder.Append("' and enterpriseid='");
				strBuilder.Append(strEnterpriseId);
				strBuilder.Append("' and booking_no=");
				strBuilder.Append(strBooking_no);
				strBuilder.Append(" and consignment_no='");
				strBuilder.Append(strConsig_no);
				strBuilder.Append("'");
 
				String strSQLQuery = strBuilder.ToString();

				IDbCommand dbcmd = dbCon.CreateCommand(strSQLQuery,CommandType.Text);
				iRowsEffected = dbCon.ExecuteNonQuery(dbcmd);

			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("SysDataMgrDAL","UpdateRouteCode","R002","Error During Insert "+ appException.Message.ToString());
				//throw new ApplicationException("Error Updating Route Code ",appException);
			}
  
			return iRowsEffected; 
  
		}


		public static bool CheckCODfirstTime(String strAppID, String strEnterpriseID,String booking_No,String strConsig_No,String strFieldName)
		{
			DataSet dsIShip = new DataSet();
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("POSTDELIVERYCOD","POSTDELIVERYCOD","EDB101","DbConnection object is null!!");
			}

			StringBuilder strBd = new StringBuilder();

			strBd.Append(" select * from shipment ");
			strBd.Append(" where ("+strFieldName+"='' or "+strFieldName+" IS NULL) ");
			strBd.Append(" and enterpriseid ='"+strEnterpriseID+"'");
			strBd.Append(" and applicationid = '" +strAppID+ "'");
			strBd.Append(" and consignment_no = '"+ strConsig_No +"' ");
			strBd.Append(" and booking_no ="+ booking_No +" ");

			dsIShip.Clear();

			dsIShip = (DataSet)dbCon.ExecuteQuery(strBd.ToString(),ReturnType.DataSetType);

			if(dsIShip.Tables[0].Rows.Count>0)
			{
				return true; //if found NULL for that field
			}

			return false; 
		}


//		public static DateTime GetOriginCODDTx(String strAppID, String strEnterpriseID,String booking_No,String strConsig_No,String strFieldName)
//		{
//			DbConnection dbCon= null;
//			IDbCommand dbcmd=null;
//			IDbCommand dbcmd2=null;
//			int iRowsEffected = 0;
//
//			dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
//
//			if (dbCon==null)
//			{
//				throw new ApplicationException("Connection to Database failed",null);
//			}
//			StringBuilder strBuilder = new StringBuilder();
//			StringBuilder strBuilder2 = new StringBuilder(); //FOR OLD_COD_INFO
//
//			strBuilder2.Append("select top 1 new_cod_info from shipment_audit_trial where applicationid='");
//			strBuilder2.Append(strAppID);
//			//strBuilder2.Append("' and enterpriseid='"+strEnterpriseID+"' and booking_no="+booking_No+" and consignment_no='"+strConsig_No+"' and audit_type='"+strAudit_Type+"' order by last_updated DESC");
//
//			String strSQLQuery2 = strBuilder2.ToString();
//			dbcmd2 = dbCon.CreateCommand(strSQLQuery2,CommandType.Text);
//
//			DataSet dsDt = null;
//			String strOLDXML=null;
//
//			dsDt = (DataSet)dbCon.ExecuteQuery(dbcmd2,com.common.DAL.ReturnType.DataSetType);  
//
//			if(dsDt.Tables[0].Rows.Count>0)
//			{
//				if(!dsDt.Tables[0].Rows[0][0].GetType().Equals(System.Type.GetType("System.DBNull")))
//					strOLDXML = (String)dsDt.Tables[0].Rows[0][0];
//			}
//
//
//											
//			System.Xml.XmlDocument xdoc = new System.Xml.XmlDocument();
//
//			xdoc.LoadXml(strOLDXML); 
//			string myXmlData = xdoc.OuterXml;
//
//
//		}
//

		public static DateTime GetOriginCODDT(String strAppID, String strEnterpriseID,String booking_No,String strConsig_No,String strFieldName,String strOldNew)
		{

			DbConnection dbCon= null;
			IDbCommand dbcmd2=null;

			dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if (dbCon==null)
			{
				throw new ApplicationException("Connection to Database failed",null);
			}
			StringBuilder strBuilder = new StringBuilder();
			StringBuilder strBuilder2 = new StringBuilder(); //FOR OLD_COD_INFO
			
			if(strOldNew=="NEW")
				strBuilder2.Append("select top 1 new_cod_info from shipment_audit_trial where applicationid='");
			else
				strBuilder2.Append("select top 1 old_cod_info from shipment_audit_trial where applicationid='");

			strBuilder2.Append(strAppID);
			strBuilder2.Append("' and enterpriseid='"+strEnterpriseID+"' and booking_no="+booking_No+" and consignment_no='"+strConsig_No+"' order by last_updated DESC");

			String strSQLQuery2 = strBuilder2.ToString();
			dbcmd2 = dbCon.CreateCommand(strSQLQuery2,CommandType.Text);

			DataSet dsDt = null;
			String strOLDXML=null;

			dsDt = (DataSet)dbCon.ExecuteQuery(dbcmd2,com.common.DAL.ReturnType.DataSetType);  

			if(dsDt.Tables[0].Rows.Count>0)
			{
				if(!dsDt.Tables[0].Rows[0][0].GetType().Equals(System.Type.GetType("System.DBNull")))
					strOLDXML = (String)dsDt.Tables[0].Rows[0][0];
			}

			System.Xml.XmlDocument xdoc = new System.Xml.XmlDocument();

			xdoc.LoadXml(strOLDXML); 

			String xxx = xdoc.GetElementsByTagName(strFieldName)[0].ChildNodes[0].Value;

			if(xxx.Length>0)
			{
				DateTime dtXX = DateTime.Parse(xxx);
				return dtXX;
			}
			else
				return DateTime.Now;


		}



		public static int UpdateCODUpdateUser(String strAppID, String strEnterpriseID,String booking_No,String strConsig_No,String strAudit_Type, String strUserID,String strXml)
		{	
			DbConnection dbCon= null;
			IDbCommand dbcmd=null;
			IDbCommand dbcmd2=null;
			int iRowsEffected = 0;

			dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if (dbCon==null)
			{
				//	Logger.LogTraceError("ShipmentTrackingMgrDAL.cs","GetLatestDateFromShipment","ERR001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			StringBuilder strBuilder = new StringBuilder();
			StringBuilder strBuilder2 = new StringBuilder(); //FOR OLD_COD_INFO

//			strBuilder2.Append("update shipment_audit_trial");
//			strBuilder2.Append(" set old_cod_info=");
			strBuilder2.Append("select top 1 new_cod_info from shipment_audit_trial where applicationid='");
			strBuilder2.Append(strAppID);
			strBuilder2.Append("' and enterpriseid='"+strEnterpriseID+"' and booking_no="+booking_No+" and consignment_no='"+strConsig_No+"' and audit_type='"+strAudit_Type+"' order by last_updated DESC");

//			strBuilder2.Append(" where  applicationid='");
//			strBuilder2.Append(strAppID);
//			strBuilder2.Append("' and enterpriseid='");
//			strBuilder2.Append(strEnterpriseID);
//			strBuilder2.Append("' and booking_no=");
//			strBuilder2.Append(booking_No);
//			strBuilder2.Append(" and consignment_no='");
//			strBuilder2.Append(strConsig_No);
//			strBuilder2.Append("'");
//			strBuilder2.Append(" and audit_type='");
//			strBuilder2.Append(strAudit_Type);
//			strBuilder2.Append("'");


			String strSQLQuery2 = strBuilder2.ToString();
			dbcmd2 = dbCon.CreateCommand(strSQLQuery2,CommandType.Text);
			//iRowsEffected = dbCon.ExecuteScalar(dbcmd);

			DataSet dsDt = null;
			String strOLDXML=null;

			dsDt = (DataSet)dbCon.ExecuteQuery(dbcmd2,com.common.DAL.ReturnType.DataSetType);  

			if(dsDt.Tables[0].Rows.Count>0)
				{
					if(!dsDt.Tables[0].Rows[0][0].GetType().Equals(System.Type.GetType("System.DBNull")))
						strOLDXML = (String)dsDt.Tables[0].Rows[0][0];
				}


											
			System.Xml.XmlDocument xdoc = new System.Xml.XmlDocument();

			xdoc.LoadXml(strXml); 
			string myXmlData = xdoc.OuterXml;

			strBuilder.Append(" insert  Shipment_Audit_Trial ");
			strBuilder.Append(" (applicationid,enterpriseid,consignment_no,booking_no, ");
			strBuilder.Append(" audit_type,last_userid,last_updated,new_cod_info,old_cod_info) VALUES( ");
			strBuilder.Append("'" + strAppID + "','"+strEnterpriseID+"','"+strConsig_No+"',");	
			strBuilder.Append(booking_No+ ",'" + strAudit_Type +"','"+strUserID+"','"+DateTime.Now+"','" + myXmlData + "','"+strOLDXML+"')");	

			String strSQLQuery = strBuilder.ToString();
			dbcmd = dbCon.CreateCommand(strSQLQuery,CommandType.Text);

			
 
			try
			{
				iRowsEffected = dbCon.ExecuteNonQuery(dbcmd);
			}
			catch(ApplicationException appException)
			{
				//	Logger.LogTraceError("ShipmentTrackingMgrDAL.cs","GetLatestDateFromShipment","ERR002","Error in Query String "+appException.Message);
				throw appException;
			}

			return iRowsEffected;
		}
		

		public static int DeleteCODVAS(String strAppID, String strEnterpriseID,String booking_No,String strConsig_No,Double dblVasAmt)
		{	
			DbConnection dbCon= null;
			IDbCommand dbcmd=null,dbcmd2=null;
			int iRowsEffected = 0;


			dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if (dbCon==null)
			{
				//	Logger.LogTraceError("ShipmentTrackingMgrDAL.cs","GetLatestDateFromShipment","ERR001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			//ź���� COD ������������
			StringBuilder strBuilder = new StringBuilder();
			strBuilder.Append(" DELETE FROM dbo.Shipment_VAS ");
			strBuilder.Append(" WHERE dbo.Shipment_VAS.applicationid = '");
			strBuilder.Append(strAppID);
			strBuilder.Append("' AND dbo.Shipment_VAS.enterpriseid = '");
			strBuilder.Append(strEnterpriseID);
			strBuilder.Append("' AND dbo.Shipment_VAS.booking_no = ");
			strBuilder.Append(booking_No);
			strBuilder.Append(" AND dbo.Shipment_VAS.consignment_no = '");
			strBuilder.Append(strConsig_No);
			strBuilder.Append("' AND dbo.Shipment_VAS.VAS_CODE='COD' ");

//��������� ������ѡ�ҡ Total VAS SURCHARGE, ��� TOTAL AMOUNT ���
			
			StringBuilder strBuilder2 = new StringBuilder();
			strBuilder2.Append(" Update dbo.Shipment ");
			strBuilder2.Append(" set tot_vas_surcharge=tot_vas_surcharge-");
			strBuilder2.Append(dblVasAmt);
//			strBuilder2.Append(" , total_cod_amt= ");
//			strBuilder2.Append();

			 
			strBuilder2.Append(" WHERE dbo.Shipment.applicationid = '");
			strBuilder2.Append(strAppID);
			strBuilder2.Append("' AND dbo.Shipment.enterpriseid = '");
			strBuilder2.Append(strEnterpriseID);
			strBuilder2.Append("' AND dbo.Shipment.booking_no = ");
			strBuilder2.Append(booking_No);
			strBuilder2.Append(" AND dbo.Shipment.consignment_no = '");
			strBuilder2.Append(strConsig_No);
			strBuilder2.Append("'");

			String strSQLQuery = strBuilder.ToString();
			dbcmd = dbCon.CreateCommand(strSQLQuery,CommandType.Text);

			String strSQLQuery2 = strBuilder2.ToString();
			dbcmd2 = dbCon.CreateCommand(strSQLQuery2,CommandType.Text);

 
			try
			{
//				dsDt = (DataSet)dbCon.ExecuteQuery(dbcmd,com.common.DAL.ReturnType.DataSetType);  
				iRowsEffected = dbCon.ExecuteNonQuery(dbcmd);
				iRowsEffected = dbCon.ExecuteNonQuery(dbcmd2);

			}
			catch(ApplicationException appException)
			{
				//	Logger.LogTraceError("ShipmentTrackingMgrDAL.cs","GetLatestDateFromShipment","ERR002","Error in Query String "+appException.Message);
				throw appException;
			}

			return iRowsEffected;

		}
		

		public static void UpdateCODHistory(String strAppId,String strEnterpriseId,String strBooking_no,String strConsig_no,DataRow drEach,Utility ut)
		{
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppId,strEnterpriseId);
   
			if(dbCon==null)
			{
				Logger.LogTraceError("SysDataMgrDAL","UpdateRouteCode","R001","DbConnection object is null!!");
			}

			IDbConnection conApp=null;
			conApp = dbCon.GetConnection();		
			conApp.Open();
			//dbCon.OpenConnection(ref conApp);
			IDbCommand dbCmd2 = dbCon.CreateCommand(" select 1 ",CommandType.Text);
			dbCmd2.Connection = conApp;
			Decimal net_cod_amt = 0;
			if(!drEach["net_cod_amt"].GetType().Equals(System.Type.GetType("System.DBNull")))
				net_cod_amt = Decimal.Parse(drEach["net_cod_amt"].ToString());
			//COD Amount Collected D/T AUTOCONSIG HISTORY BY X APR 02 08
			//CHECK IT HAS BEEN CREATED FOR FIRST TIME?
			if(CheckCODfirstTime(strAppId,strEnterpriseId,strBooking_no,strConsig_no,"cod_amt_collected_datetime"))
			{
				if(!drEach["cod_amt_collected_datetime"].GetType().Equals(System.Type.GetType("System.DBNull")))
					if(DateTime.Parse(drEach["cod_amt_collected_datetime"].ToString())!=DateTime.MinValue)
						Utility.CreateAutoConsignmentHistory(strAppId,strEnterpriseId,8,"Net COD Collected: "+net_cod_amt.ToString(),Int32.Parse(strBooking_no),strConsig_no,DateTime.Parse(drEach["cod_amt_collected_datetime"].ToString()).AddSeconds(9),ut.GetUserID(),ref dbCmd2,ref dbCon);
			}
			else
			{
				DateTime dtOriginCODAMTCollectedDT = DateTime.Now;
				DateTime dtOriginCODAMTCollectedDTOLD = DateTime.Now;

				//�óշ������ź��� COD AMT DT �͡ ������¹�ŧ������
				if(!drEach["cod_amt_collected_datetime"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					if(DateTime.Parse(drEach["cod_amt_collected_datetime"].ToString())!=DateTime.MinValue)
					{
						dtOriginCODAMTCollectedDT = GetOriginCODDT(strAppId,strEnterpriseId,strBooking_no,strConsig_no,"COD_Amount_Collected_DateTime","NEW"); //�Ҥ�����Ԩ��ഷ�����͹
						//dtOriginCODAMTCollectedDTOLD = GetOriginCODDT(strAppId,strEnterpriseId,strBooking_no,strConsig_no,"COD_Amount_Collected_DT","OLD"); //�Ҥ�����Ԩ��ഷ�����͹
						Utility.DeleteAutoConsignmentHistory2(strAppId,strEnterpriseId,8,"",Int32.Parse(strBooking_no),strConsig_no,9,"CODC",ut.GetUserID().ToString(),ref dbCmd2,ref dbCon);
						Utility.CreateAutoConsignmentHistory(strAppId,strEnterpriseId,8,"Net COD Collected: "+net_cod_amt.ToString(),Int32.Parse(strBooking_no),strConsig_no,dtOriginCODAMTCollectedDT.AddSeconds(9),ut.GetUserID(),ref dbCmd2,ref dbCon); //(DateTime)drEach["cod_amt_collected_datetime"]
					}
				}
				else //�óշ���ա��ź����͡���
				{
					//dtOriginCODAMTCollectedDTOLD = GetOriginCODDT(strAppId,strEnterpriseId,strBooking_no,strConsig_no,"COD_Amount_Collected_DT","OLD"); //�Ҥ�����Ԩ��ഷ�����͹
					Utility.DeleteAutoConsignmentHistory2(strAppId,strEnterpriseId,8,"",Int32.Parse(strBooking_no),strConsig_no,9,"CODC",ut.GetUserID().ToString(),ref dbCmd2,ref dbCon);
				}
			}
			//END COD Amount Collected D/T AUTOCONSIG HISTORY BY X APR 02 08
//VERIFIED DT
			if(CheckCODfirstTime(strAppId,strEnterpriseId,strBooking_no,strConsig_no,"cod_amt_verified_datetime_cash"))
			{
				if(!drEach["cod_amt_verified_datetime_cash"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
						if(DateTime.Parse(drEach["cod_amt_verified_datetime_cash"].ToString())!=DateTime.MinValue)
						Utility.CreateAutoConsignmentHistory(strAppId,strEnterpriseId,9,"COD Verified (Cash): "+drEach["cod_amt_collected_cash"].ToString(),Int32.Parse(strBooking_no),strConsig_no,DateTime.Parse(drEach["cod_amt_verified_datetime_cash"].ToString()).AddSeconds(1),ut.GetUserID(),ref dbCmd2,ref dbCon);
				}
			}
			else
			{
				DateTime cod_amt_verified_datetime_cash = DateTime.Now;
				DateTime cod_amt_verified_datetime_cashOLD = DateTime.Now;

				if(!drEach["cod_amt_verified_datetime_cash"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					if(DateTime.Parse(drEach["cod_amt_verified_datetime_cash"].ToString())!=DateTime.MinValue)
					{
						cod_amt_verified_datetime_cash = GetOriginCODDT(strAppId,strEnterpriseId,strBooking_no,strConsig_no,"COD_Amount_Verified_DateTime_Cash","NEW"); //�Ҥ�����Ԩ��ഷ�����͹
						//cod_amt_verified_datetime_cashOLD = GetOriginCODDT(strAppId,strEnterpriseId,strBooking_no,strConsig_no,"COD_Amount_Verified_DateTime_Cash","OLD"); //�Ҥ�����Ԩ��ഷ�����͹
						Utility.DeleteAutoConsignmentHistory2(strAppId,strEnterpriseId,9,"",Int32.Parse(strBooking_no),strConsig_no,1,"CODV",ut.GetUserID().ToString(),ref dbCmd2,ref dbCon);
						Utility.CreateAutoConsignmentHistory(strAppId,strEnterpriseId,9,"COD Verified (Cash): "+drEach["cod_amt_collected_cash"].ToString(),Int32.Parse(strBooking_no),strConsig_no,cod_amt_verified_datetime_cash.AddSeconds(1),ut.GetUserID(),ref dbCmd2,ref dbCon); //(DateTime)drEach["cod_amt_collected_datetime"]
					}
				}
				else
				{
					//cod_amt_verified_datetime_cashOLD = GetOriginCODDT(strAppId,strEnterpriseId,strBooking_no,strConsig_no,"COD_Amount_Verified_DateTime_Cash","OLD"); //�Ҥ�����Ԩ��ഷ�����͹
					Utility.DeleteAutoConsignmentHistory2(strAppId,strEnterpriseId,9,"",Int32.Parse(strBooking_no),strConsig_no,1,"CODV",ut.GetUserID().ToString(),ref dbCmd2,ref dbCon);
				}
			}

			if(CheckCODfirstTime(strAppId,strEnterpriseId,strBooking_no,strConsig_no,"cod_amt_verified_datetime_check"))
			{
				if(!drEach["cod_amt_verified_datetime_check"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
						if(DateTime.Parse(drEach["cod_amt_verified_datetime_check"].ToString())!=DateTime.MinValue)
						Utility.CreateAutoConsignmentHistory(strAppId,strEnterpriseId,9,"COD Verified (Check): "+drEach["cod_amt_collected_check"].ToString(),Int32.Parse(strBooking_no),strConsig_no,DateTime.Parse(drEach["cod_amt_verified_datetime_check"].ToString()).AddSeconds(2),ut.GetUserID(),ref dbCmd2,ref dbCon);
				}
			}
			else
			{
				DateTime cod_amt_verified_datetime_check = DateTime.Now;
				DateTime cod_amt_verified_datetime_checkOLD = DateTime.Now;

				if(!drEach["cod_amt_verified_datetime_check"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					if(DateTime.Parse(drEach["cod_amt_verified_datetime_check"].ToString())!=DateTime.MinValue)
					{
						cod_amt_verified_datetime_check = GetOriginCODDT(strAppId,strEnterpriseId,strBooking_no,strConsig_no,"COD_Amount_Verified_DateTime_Check","NEW"); //�Ҥ�����Ԩ��ഷ�����͹
						//d_amt_verified_datetime_checkOLD = GetOriginCODDT(strAppId,strEnterpriseId,strBooking_no,strConsig_no,"COD_Amount_Verified_DateTime_Check","OLD"); //�Ҥ�����Ԩ��ഷ�����͹
						Utility.DeleteAutoConsignmentHistory2(strAppId,strEnterpriseId,9,"",Int32.Parse(strBooking_no),strConsig_no,2,"CODV",ut.GetUserID().ToString(),ref dbCmd2,ref dbCon);
						Utility.CreateAutoConsignmentHistory(strAppId,strEnterpriseId,9,"COD Verified (Check): "+drEach["cod_amt_collected_check"].ToString(),Int32.Parse(strBooking_no),strConsig_no,cod_amt_verified_datetime_check.AddSeconds(2),ut.GetUserID(),ref dbCmd2,ref dbCon); //(DateTime)drEach["cod_amt_collected_datetime"]
					}
				}
				else
				{
					//cod_amt_verified_datetime_checkOLD = GetOriginCODDT(strAppId,strEnterpriseId,strBooking_no,strConsig_no,"COD_Amount_Verified_DateTime_Check","OLD"); //�Ҥ�����Ԩ��ഷ�����͹
					Utility.DeleteAutoConsignmentHistory2(strAppId,strEnterpriseId,9,"",Int32.Parse(strBooking_no),strConsig_no,2,"CODV",ut.GetUserID().ToString(),ref dbCmd2,ref dbCon);
				}
			}

			if(CheckCODfirstTime(strAppId,strEnterpriseId,strBooking_no,strConsig_no,"cod_amt_verified_datetime_other"))
			{
				if(!drEach["cod_amt_verified_datetime_other"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
						if(DateTime.Parse(drEach["cod_amt_verified_datetime_other"].ToString())!=DateTime.MinValue)
						Utility.CreateAutoConsignmentHistory(strAppId,strEnterpriseId,9,"COD Verified (Other): "+drEach["cod_amt_collected_other"].ToString(),Int32.Parse(strBooking_no),strConsig_no,DateTime.Parse(drEach["cod_amt_verified_datetime_other"].ToString()).AddSeconds(3),ut.GetUserID(),ref dbCmd2,ref dbCon);
				}
			}
			else
			{
				DateTime cod_amt_verified_datetime_other = DateTime.Now;
				DateTime cod_amt_verified_datetime_otherOLD = DateTime.Now;

				if(!drEach["cod_amt_verified_datetime_other"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					if(DateTime.Parse(drEach["cod_amt_verified_datetime_other"].ToString())!=DateTime.MinValue)
					{
						cod_amt_verified_datetime_other = GetOriginCODDT(strAppId,strEnterpriseId,strBooking_no,strConsig_no,"COD_Amount_Verified_DateTime_Other","NEW"); //�Ҥ�����Ԩ��ഷ�����͹
						//cod_amt_verified_datetime_otherOLD = GetOriginCODDT(strAppId,strEnterpriseId,strBooking_no,strConsig_no,"COD_Amount_Verified_DateTime_Other","OLD"); //�Ҥ�����Ԩ��ഷ�����͹
						Utility.DeleteAutoConsignmentHistory2(strAppId,strEnterpriseId,9,"",Int32.Parse(strBooking_no),strConsig_no,3,"CODV",ut.GetUserID().ToString(),ref dbCmd2,ref dbCon);
						Utility.CreateAutoConsignmentHistory(strAppId,strEnterpriseId,9,"COD Verified (Other): "+drEach["cod_amt_collected_other"].ToString(),Int32.Parse(strBooking_no),strConsig_no,cod_amt_verified_datetime_other.AddSeconds(3),ut.GetUserID(),ref dbCmd2,ref dbCon); //(DateTime)drEach["cod_amt_collected_datetime"]
					}
				}
				else
				{
					//cod_amt_verified_datetime_otherOLD = GetOriginCODDT(strAppId,strEnterpriseId,strBooking_no,strConsig_no,"COD_Amount_Verified_DateTime_Other","OLD"); //�Ҥ�����Ԩ��ഷ�����͹
					Utility.DeleteAutoConsignmentHistory2(strAppId,strEnterpriseId,9,"",Int32.Parse(strBooking_no),strConsig_no,3,"CODV",ut.GetUserID().ToString(),ref dbCmd2,ref dbCon);
				}
			}

//END VERIFIED DT

//REMITTANCE DT
			if(CheckCODfirstTime(strAppId,strEnterpriseId,strBooking_no,strConsig_no,"cod_remittance_to_customer_datetime_cash"))
			{
				if(!drEach["cod_remittance_to_customer_datetime_cash"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					if(DateTime.Parse(drEach["cod_remittance_to_customer_datetime_cash"].ToString())!=DateTime.MinValue)
					 Utility.CreateAutoConsignmentHistory(strAppId,strEnterpriseId,10,"COD Remitted to Customer (Cash): "+drEach["cod_amt_collected_cash"].ToString(),Int32.Parse(strBooking_no),strConsig_no,DateTime.Parse(drEach["cod_remittance_to_customer_datetime_cash"].ToString()).AddSeconds(4),ut.GetUserID(),ref dbCmd2,ref dbCon);
				}
			}
			else
			{
				DateTime cod_remittance_to_customer_datetime_cash = DateTime.Now;
				DateTime cod_remittance_to_customer_datetime_cashOLD = DateTime.Now;

				if(!drEach["cod_remittance_to_customer_datetime_cash"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					if(DateTime.Parse(drEach["cod_remittance_to_customer_datetime_cash"].ToString())!=DateTime.MinValue)
					{
						cod_remittance_to_customer_datetime_cash = GetOriginCODDT(strAppId,strEnterpriseId,strBooking_no,strConsig_no,"COD_Remittance_to_Customer_DateTime_Cash","NEW"); //�Ҥ�����Ԩ��ഷ�����͹
						//cod_remittance_to_customer_datetime_cashOLD = GetOriginCODDT(strAppId,strEnterpriseId,strBooking_no,strConsig_no,"COD_Remittance_to_Customer_DateTime_Cash","OLD"); //�Ҥ�����Ԩ��ഷ�����͹
						Utility.DeleteAutoConsignmentHistory2(strAppId,strEnterpriseId,10,"",Int32.Parse(strBooking_no),strConsig_no,4,"CODR",ut.GetUserID().ToString(),ref dbCmd2,ref dbCon);
						Utility.CreateAutoConsignmentHistory(strAppId,strEnterpriseId,10,"COD Remitted to Customer (Cash): "+drEach["cod_amt_collected_cash"].ToString(),Int32.Parse(strBooking_no),strConsig_no,cod_remittance_to_customer_datetime_cash.AddSeconds(4),ut.GetUserID(),ref dbCmd2,ref dbCon); //(DateTime)drEach["cod_amt_collected_datetime"]
					}
				}
				else
				{
					//cod_remittance_to_customer_datetime_cashOLD = GetOriginCODDT(strAppId,strEnterpriseId,strBooking_no,strConsig_no,"COD_Remittance_to_Customer_DateTime_Cash","OLD"); //�Ҥ�����Ԩ��ഷ�����͹
					Utility.DeleteAutoConsignmentHistory2(strAppId,strEnterpriseId,10,"",Int32.Parse(strBooking_no),strConsig_no,4,"CODR",ut.GetUserID().ToString(),ref dbCmd2,ref dbCon);
				}
			}

			if(CheckCODfirstTime(strAppId,strEnterpriseId,strBooking_no,strConsig_no,"cod_remittance_to_customer_datetime_check"))
			{
				if(!drEach["cod_remittance_to_customer_datetime_check"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					if(DateTime.Parse(drEach["cod_remittance_to_customer_datetime_check"].ToString())!=DateTime.MinValue)
						Utility.CreateAutoConsignmentHistory(strAppId,strEnterpriseId,10,"COD Remitted to Customer (Check): "+drEach["cod_amt_collected_check"].ToString(),Int32.Parse(strBooking_no),strConsig_no,DateTime.Parse(drEach["cod_remittance_to_customer_datetime_check"].ToString()).AddSeconds(5),ut.GetUserID(),ref dbCmd2,ref dbCon);
				}
			}
			else
			{
				DateTime cod_remittance_to_customer_datetime_check = DateTime.Now;
				DateTime cod_remittance_to_customer_datetime_checkOLD = DateTime.Now;

				if(!drEach["cod_remittance_to_customer_datetime_check"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					if(DateTime.Parse(drEach["cod_remittance_to_customer_datetime_check"].ToString())!=DateTime.MinValue)
					{
						cod_remittance_to_customer_datetime_check = GetOriginCODDT(strAppId,strEnterpriseId,strBooking_no,strConsig_no,"COD_Remittance_to_Customer_DateTime_Check","NEW"); //�Ҥ�����Ԩ��ഷ�����͹
						//cod_remittance_to_customer_datetime_checkOLD = GetOriginCODDT(strAppId,strEnterpriseId,strBooking_no,strConsig_no,"COD_Remittance_to_Customer_DateTime_Check","OLD"); //�Ҥ�����Ԩ��ഷ�����͹
						Utility.DeleteAutoConsignmentHistory2(strAppId,strEnterpriseId,10,"",Int32.Parse(strBooking_no),strConsig_no,5,"CODR",ut.GetUserID().ToString(),ref dbCmd2,ref dbCon);
						Utility.CreateAutoConsignmentHistory(strAppId,strEnterpriseId,10,"COD Remitted to Customer (Check): "+drEach["cod_amt_collected_check"].ToString(),Int32.Parse(strBooking_no),strConsig_no,cod_remittance_to_customer_datetime_check.AddSeconds(5),ut.GetUserID(),ref dbCmd2,ref dbCon); //(DateTime)drEach["cod_amt_collected_datetime"]
					}
				}
				else
				{
					//cod_remittance_to_customer_datetime_checkOLD = GetOriginCODDT(strAppId,strEnterpriseId,strBooking_no,strConsig_no,"COD_Remittance_to_Customer_DateTime_Check","OLD"); //�Ҥ�����Ԩ��ഷ�����͹
					Utility.DeleteAutoConsignmentHistory2(strAppId,strEnterpriseId,10,"",Int32.Parse(strBooking_no),strConsig_no,5,"CODR",ut.GetUserID().ToString(),ref dbCmd2,ref dbCon);
				}
			}

			if(CheckCODfirstTime(strAppId,strEnterpriseId,strBooking_no,strConsig_no,"cod_remittance_to_customer_datetime_other"))
			{
				if(!drEach["cod_remittance_to_customer_datetime_other"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					if(DateTime.Parse(drEach["cod_remittance_to_customer_datetime_other"].ToString())!=DateTime.MinValue)
						Utility.CreateAutoConsignmentHistory(strAppId,strEnterpriseId,10,"COD Remitted to Customer (Other): "+drEach["cod_amt_collected_other"].ToString(),Int32.Parse(strBooking_no),strConsig_no,DateTime.Parse(drEach["cod_remittance_to_customer_datetime_other"].ToString()).AddSeconds(6),ut.GetUserID(),ref dbCmd2,ref dbCon);
				}
			}
			else
			{
				DateTime cod_remittance_to_customer_datetime_other = DateTime.Now;
				DateTime cod_remittance_to_customer_datetime_otherOLD = DateTime.Now;

				if(!drEach["cod_remittance_to_customer_datetime_other"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					if(DateTime.Parse(drEach["cod_remittance_to_customer_datetime_other"].ToString())!=DateTime.MinValue)
					{
						cod_remittance_to_customer_datetime_other = GetOriginCODDT(strAppId,strEnterpriseId,strBooking_no,strConsig_no,"COD_Remittance_to_Customer_DateTime_Other","NEW"); //�Ҥ�����Ԩ��ഷ�����͹
						//cod_remittance_to_customer_datetime_otherOLD = GetOriginCODDT(strAppId,strEnterpriseId,strBooking_no,strConsig_no,"COD_Remittance_to_Customer_DateTime_Other","OLD"); //�Ҥ�����Ԩ��ഷ�����͹
						Utility.DeleteAutoConsignmentHistory2(strAppId,strEnterpriseId,10,"",Int32.Parse(strBooking_no),strConsig_no,6,"CODR",ut.GetUserID().ToString(),ref dbCmd2,ref dbCon);
						Utility.CreateAutoConsignmentHistory(strAppId,strEnterpriseId,10,"COD Remitted to Customer (Other): "+drEach["cod_amt_collected_other"].ToString(),Int32.Parse(strBooking_no),strConsig_no,cod_remittance_to_customer_datetime_other.AddSeconds(6),ut.GetUserID(),ref dbCmd2,ref dbCon); //(DateTime)drEach["cod_amt_collected_datetime"]
					}
				}
				else
				{
					//cod_remittance_to_customer_datetime_otherOLD = GetOriginCODDT(strAppId,strEnterpriseId,strBooking_no,strConsig_no,"COD_Remittance_to_Customer_DateTime_Other","OLD"); //�Ҥ�����Ԩ��ഷ�����͹
					Utility.DeleteAutoConsignmentHistory2(strAppId,strEnterpriseId,10,"",Int32.Parse(strBooking_no),strConsig_no,6,"CODR",ut.GetUserID().ToString(),ref dbCmd2,ref dbCon);
				}
			}

//END REMITTANCE DT



		}

	}
}
