using System;
using System.Data;
using com.common.classes;
using com.common.DAL;
using com.common.util;

namespace TIESDAL
{
	/// <summary>
	/// Summary description for ManualRatingOverrideDAL.
	/// </summary>
	public class ManualRatingOverrideDAL
	{
		public ManualRatingOverrideDAL()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		public static SessionDS Get(Utility util, SessionDS dsQueryParam, int iCurrent, int iRecSize, bool schemaOnly)
		{	
			string AppID = util.GetAppID(), EnterpriseID=util.GetEnterpriseID();

			IDbCommand dbCmd = null;
			DbConnection dbCon= DbConnectionManager.GetInstance().GetDbConnection(AppID,EnterpriseID);
			if (dbCon==null)
			{
				throw new ApplicationException("Connection to Database failed",null);
			}
			
			SessionDS sessDS = null; 
			try
			{
				String strSQL = 
					@"  SELECT  cast(null as datetime) as actual_pod_datefrom,
								cast(null as datetime) as actual_pod_dateto, 
								ST.tracking_datetime, 
								SM.applicationid, SM.enterpriseid, SM.payerid, SM.booking_no,
								SM.consignment_no, SM.ref_no, SM.route_code, SM.destination_station, 
								SM.invoice_date, SM.manual_override, SM.manual_over_user, SM.manual_over_datetime,
								
								SM.original_rated_freight, SM.tot_freight_charge,
								SM.original_rated_ins, SM.insurance_surcharge,
								SM.original_rated_other, SM.other_surch_amount,
								SM.tot_vas_surcharge, 
								SM.original_rated_esa, SM.esa_surcharge,
								SM.original_rated_total, SM.total_rated_amount 
						FROM	Shipment SM 
					--inner join
						left join
								(select max(tracking_datetime) as tracking_datetime, applicationid, enterpriseid, consignment_no, booking_no
								from	Shipment_Tracking 
								where  isnull(deleted, 'N') <> 'Y' and  status_code = 'POD'
								group by applicationid, enterpriseid, consignment_no, booking_no
								) AS ST ON  SM.applicationid = ST.applicationid AND SM.enterpriseid = ST.enterpriseid AND 
											SM.consignment_no = ST.consignment_no AND SM.booking_no = ST.booking_no 
						WHERE	(1="+(schemaOnly?0:1)+@") and (SM.applicationid = '"+AppID+@"') and (SM.enterpriseid = '"+EnterpriseID+@"') ";

				//Search
				if(!schemaOnly)
				{
					DataRow drS = dsQueryParam.ds.Tables[0].Rows[0];
					if(Utility.IsNotDBNull(drS["consignment_no"]))					
						strSQL += " and (SM.consignment_no like '%"+drS["consignment_no"]+"%') ";					

					if(Utility.IsNotDBNull(drS["ref_no"]))
						strSQL += " and (SM.ref_no like '%"+drS["ref_no"]+"%') ";
								
					if(Utility.IsNotDBNull(drS["payerid"]))
						strSQL += " and (SM.payerid like '%"+drS["payerid"]+"%') ";
					
					if(Utility.IsNotDBNull(drS["route_code"]))
						strSQL += " and (SM.route_code like '%"+drS["route_code"]+"%') ";					

					if(Utility.IsNotDBNull(drS["destination_station"]))
						strSQL += " and (SM.destination_station like '%"+drS["destination_station"]+"%') ";
					
					if(Utility.IsNotDBNull(drS["actual_pod_datefrom"]))
					{
						DateTime dtFrom=System.Convert.ToDateTime(drS["actual_pod_datefrom"]);
						string sdtFrom=Utility.DateFormat(AppID,EnterpriseID, dtFrom ,DTFormat.DateTime);
						strSQL += " and (ST.tracking_datetime >= "+sdtFrom+") ";
					}

					if(Utility.IsNotDBNull(drS["actual_pod_dateto"]))
					{
						DateTime dtTo=System.Convert.ToDateTime(drS["actual_pod_dateto"]);
						string sdtTo=Utility.DateFormat(AppID,EnterpriseID, dtTo ,DTFormat.DateTime);
						strSQL += " and (ST.tracking_datetime <= "+sdtTo+") ";
					}
				}
				strSQL += " order by SM.consignment_no ASC ";

				//sessDS = dbCon.ExecuteQuery(strSQL, iCurrent, iRecSize, "ManualRatingOverride");
				dbCmd = dbCon.CreateCommand(strSQL, CommandType.Text);
				dbCmd.CommandTimeout = 300;  //Add Command TimeOut for case of serveral records: 300 seconds (5 minutes)
				sessDS = dbCon.ExecuteQuery(dbCmd, iCurrent, iRecSize, "ManualRatingOverride");

				if(schemaOnly)
				{
					DataRow row = sessDS.ds.Tables[0].NewRow();
					sessDS.ds.Tables[0].Rows.Add(row);					
				}		
				
			}
			catch(ApplicationException ex)
			{
				throw ex;
			}

			return sessDS;
		}

		public static int Update(Utility util, ref DataSet dsUpdate)
		{			
			if(util == null) throw new ApplicationException("Utility isn't define", null);			
			string AppID = util.GetAppID(), EnterpriseID=util.GetEnterpriseID();

			int iRowsEffected = 0;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(AppID,EnterpriseID);
   
			if(dbCon==null)
			{
				throw new Exception("DbConnection object is null!!");
			}
			
			try
			{
				DataTable gcTb = dsUpdate.Tables[0].GetChanges(); if(gcTb==null) return iRowsEffected;
				gcTb = gcTb.GetChanges(DataRowState.Modified); if(gcTb==null) return iRowsEffected;				
				DataRow r = gcTb.Rows[0];

				String strSQL = 
					@"UPDATE Shipment 
					  SET	 original_rated_freight="+ NullDB(r["original_rated_freight"])+@"
							 ,tot_freight_charge="+ NullDB(r["tot_freight_charge"])+@"
							 ,original_rated_ins="+ NullDB(r["original_rated_ins"])+@"
							 ,insurance_surcharge="+ NullDB(r["insurance_surcharge"]) +@"
							 ,original_rated_other="+ NullDB(r["original_rated_other"])+@"
							 ,other_surch_amount="+ NullDB(r["other_surch_amount"]) +@"
							 ,original_rated_esa="+ NullDB(r["original_rated_esa"])+@"
							 ,esa_surcharge="+ NullDB(r["esa_surcharge"]) +@"
							 ,original_rated_total="+ NullDB(r["original_rated_total"])+@"
							 ,total_rated_amount="+ NullDB(r["total_rated_amount"]) +@"
							 ,manual_over_user='"+util.GetUserID()+@"'
							 ,manual_over_datetime=getdate()
							 ,manual_override='"+r["manual_override"]+@"'
					 WHERE  applicationid='"+r["applicationid"]+@"' and 
							 enterpriseid='"+r["enterpriseid"]+@"' and 
							 booking_no='"+r["booking_no"]+@"' and 
							 consignment_no='"+r["consignment_no"]+"' ";

				IDbCommand dbcmd = dbCon.CreateCommand(strSQL,CommandType.Text);
				iRowsEffected = dbCon.ExecuteNonQuery(dbcmd);
			}
			catch(ApplicationException ex)
			{
				//Logger.LogTraceError("SysDataMgrDAL","UpdateRouteCode","R002","Error During Insert "+ appException.Message.ToString());
				throw new ApplicationException("Error Updating Surcharge ",ex);
			}

			return iRowsEffected;
		}

		private static string NullDB(object obj)
		{
			if(obj==DBNull.Value) return "NULL";
			else return obj.ToString();
		}
	}
}
