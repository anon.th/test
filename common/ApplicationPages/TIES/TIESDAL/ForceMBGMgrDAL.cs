using System;
using System.Data;
using com.common.classes;
using com.common.DAL;
using com.common.util;

namespace TIESDAL
{
	/// <summary>
	/// Summary description for ManualRatingOverrideDAL.
	/// </summary>
	public class ForceMBGMgrDAL
	{
		public ForceMBGMgrDAL()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		public static SessionDS Get(Utility util, string strConsignment_No, int iCurrent, int iRecSize, bool schemaOnly)
		{	
			string AppID = util.GetAppID(), EnterpriseID=util.GetEnterpriseID();

			IDbCommand dbCmd = null;
			DbConnection dbCon= DbConnectionManager.GetInstance().GetDbConnection(AppID,EnterpriseID);
			if (dbCon==null)
			{
				throw new ApplicationException("Connection to Database failed",null);
			}
			
			SessionDS sessDS = null; 
			try
			{
				String strSQL ="  SELECT distinct Shipment.consignment_no,Invoice.invoice_no,Shipment.payerid, ";
						strSQL+=" Invoice.invoice_status,Shipment.last_exception_code,Shipment.tot_freight_charge, ";
						strSQL+=" Shipment.tot_vas_surcharge,Shipment.insurance_surcharge,Shipment.esa_surcharge, ";
						strSQL+=" Shipment.other_surch_amount,Shipment.total_rated_amount,Shipment.mbg_amount,Invoice_Detail.mbg_amount AS Invoice_Detail_mbg_amount,Customer.MBG AS  Customer_MBG, ";
						strSQL+=" Shipment.MBG,Shipment.forcembg_amount ";
						strSQL+=" FROM Shipment ";
						strSQL+=" left join Invoice_Detail on ";
						strSQL+=" Shipment.applicationid=Invoice_Detail.applicationid ";
						strSQL+=" and Shipment.enterpriseid=Invoice_Detail.enterpriseid ";
						strSQL+=" and Shipment.consignment_no=Invoice_Detail.consignment_no";
						strSQL+=" left join  Invoice on ";
						strSQL+=" Invoice_Detail.applicationid=Invoice.applicationid " ;
						strSQL+=" and Invoice_Detail.enterpriseid=Invoice.enterpriseid " ;
						strSQL+=" and Invoice_Detail.invoice_no=Invoice.invoice_no ";
						strSQL+=" inner join  Customer on ";
						strSQL+=" Shipment.payerid = Customer.custid ";
						strSQL+=" and Shipment.applicationid=Customer.applicationid ";
						strSQL+=" and Shipment.enterpriseid=Customer.enterpriseid ";
						strSQL+=" WHERE(1="+(schemaOnly?0:1)+@") and (Shipment.applicationid = '"+AppID+"') and (Shipment.enterpriseid = '"+EnterpriseID+"') and (Shipment.consignment_no = '"+strConsignment_No+"') ";
						strSQL+=" order by Invoice.invoice_no Desc";
				
				dbCmd = dbCon.CreateCommand(strSQL, CommandType.Text);
				dbCmd.CommandTimeout = 300;  //Add Command TimeOut for case of serveral records: 300 seconds (5 minutes)
				sessDS = dbCon.ExecuteQuery(dbCmd, iCurrent, iRecSize, "ForceMBG");

				if(schemaOnly)
				{
					DataRow row = sessDS.ds.Tables[0].NewRow();
					sessDS.ds.Tables[0].Rows.Add(row);					
				}		
				
			}
			catch(ApplicationException ex)
			{
				throw ex;
			}

			return sessDS;
		}

		public static int ApplyMBG(Utility util,string strConsignment_no,string strInvoice_no, decimal dblMBG_Amount,string strMBG_Flag,string strRemark)
		{		
			SessionDS sessDS = Get(util, strConsignment_no, 0, 1, false);
			if (sessDS.ds.Tables[0].Rows.Count==0)
			{
				throw new Exception("No found invoice for Update.");
			}
			else
			{
				if (sessDS.ds.Tables[0].Rows[0]["invoice_status"]!=DBNull.Value)
				{
					if(!sessDS.ds.Tables[0].Rows[0]["invoice_status"].ToString().Trim().Equals("N")&& !sessDS.ds.Tables[0].Rows[0]["invoice_status"].ToString().Trim().Equals("C"))
					{
						throw new Exception("Invoice status is wrong for force MBG.");
					}
				}
			}
		
			IDbCommand dbCmd = null;
			if(util == null) throw new ApplicationException("Utility isn't define", null);			
			string AppID = util.GetAppID(), EnterpriseID=util.GetEnterpriseID();

			int iRowsEffected = 0;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(AppID,EnterpriseID);
   
			if(dbCon==null)
			{
				throw new Exception("DbConnection object is null!!");
			}
		    IDbConnection conApp = dbCon.GetConnection();
			if(conApp == null)
			{
				Logger.LogTraceError("ForceMBGMgrDAL","ApplyMBG","ERR003","conApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			try
			{
				dbCon.OpenConnection(ref conApp);
			}
			catch(ApplicationException appException)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("ForceMBGMgrDAL","ApplyMBG","ERR004","Error opening connection : "+ appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ",appException);
			}
			catch(Exception exception)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("ForceMBGMgrDAL","ApplyMBG","ERR005","Error opening connection : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}
			IDbTransaction transactionApp = dbCon.BeginTransaction(conApp,IsolationLevel.ReadCommitted);
			try
			{
				dbCmd = dbCon.CreateCommand("", CommandType.Text);
				dbCmd.Connection = conApp;
				dbCmd.Transaction = transactionApp;
				String strSQL=""; 
				strSQL+=" UPDATE Shipment SET  mbg='"+ strMBG_Flag +"',mbg_amount= "+ dblMBG_Amount +",forcembg_amount= "+ dblMBG_Amount +",invoice_amt=tot_freight_charge + insurance_surcharge + other_surch_amount + tot_vas_surcharge + esa_surcharge + total_excp_charges + invoice_adjustment + " + dblMBG_Amount;
				strSQL+=" WHERE  applicationid='"+ util.GetAppID() +"'";
				strSQL+=" and enterpriseid='"+util.GetEnterpriseID()+"'";
				strSQL+=" and consignment_no='"+ strConsignment_no +"'";
				dbCmd = dbCon.CreateCommand(strSQL,CommandType.Text);
				dbCmd.Connection = conApp;
				dbCmd.Transaction = transactionApp;
				iRowsEffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);

				if (!strInvoice_no.Equals(""))
				{
					if (sessDS.ds.Tables[0].Rows[0]["invoice_status"]!=DBNull.Value)
					{
						if(sessDS.ds.Tables[0].Rows[0]["invoice_status"].ToString().Trim().Equals("N"))
						{
							strSQL=""; 
							strSQL+=" UPDATE Invoice_Detail SET  mbg='"+ strMBG_Flag +"',mbg_amount= "+ dblMBG_Amount +",invoice_amt=tot_freight_charge + insurance_amt + other_surcharge + tot_vas_surcharge + esa_surcharge + total_exception + invoice_adj_amount + " + dblMBG_Amount ;
							strSQL+=" WHERE  applicationid='"+ util.GetAppID() +"'";
							strSQL+=" and enterpriseid='"+util.GetEnterpriseID()+"'";
							strSQL+=" and consignment_no='"+ strConsignment_no +"'";
							strSQL+=" and invoice_no='"+ strInvoice_no +"'";
							dbCmd.CommandText = strSQL.ToString();
							iRowsEffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);

							strSQL=""; 
							strSQL+=" UPDATE inv ";
							strSQL+=" SET inv.mbg_amount = ind.mbg_amount, inv.invoice_amt= ind.invoice_amt ";
							strSQL+=" FROM ( SELECT applicationid,enterpriseid,invoice_no,SUM(invoice_amt)AS invoice_amt, SUM(mbg_amount) AS mbg_amount ";
							strSQL+=" FROM Invoice_Detail ";
							strSQL+=" WHERE (invoice_no = '"+ strInvoice_no +"' and applicationid='"+ util.GetAppID() +"' and enterpriseid='"+util.GetEnterpriseID()+"' and  mbg_amount is not null) ";
							strSQL+=" group by invoice_no,applicationid,enterpriseid) ind ";
							strSQL+=" Inner join Invoice inv on inv.invoice_no=ind.invoice_no ";
							strSQL+=" and inv.enterpriseid=ind.enterpriseid";
							strSQL+=" and inv.applicationid=ind.applicationid";

							dbCmd.CommandText = strSQL.ToString();
							iRowsEffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
						}
					}
				}
				strSQL=""; 
				strSQL+=" Update shipment_tracking set Remark='" +strRemark+ "' ";
				strSQL+=" WHERE  applicationid='"+ util.GetAppID() +"'";
				strSQL+=" and enterpriseid='"+util.GetEnterpriseID()+"'";
				strSQL+=" and consignment_no='"+ strConsignment_no +"'";	
				strSQL+=" and tracking_datetime=(select Max(tracking_datetime) from shipment_tracking ";
				strSQL+=" WHERE  applicationid='"+ util.GetAppID() +"'";
				strSQL+=" and enterpriseid='"+util.GetEnterpriseID()+"'";
				strSQL+=" and consignment_no='"+ strConsignment_no +"')";
				dbCmd.CommandText = strSQL.ToString();
				iRowsEffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);

				transactionApp.Commit();
			}
			catch(ApplicationException ex)
			{
				try
				{
					transactionApp.Rollback();
				}
				catch(Exception rollbackException)
				{
					throw new ApplicationException("Error Updating MBG ",ex);
				}
				throw new ApplicationException("Error Updating MBG ",ex);
			}
			finally
			{
				if(conApp != null)
				{
					conApp.Close();
				}
			}

			return iRowsEffected;
		}
		public static int RevertMBG(Utility util,string strConsignment_no,string strInvoice_no, decimal dblMBG_Amount,string strMBG_Flag)
		{		
			SessionDS sessDS = Get(util, strConsignment_no, 0, 1, false);
			if (sessDS.ds.Tables[0].Rows.Count==0)
			{
				throw new Exception("No found invoice for Update.");
			}
			else
			{
				if (sessDS.ds.Tables[0].Rows[0]["invoice_status"]!=DBNull.Value)
				{
					if(!sessDS.ds.Tables[0].Rows[0]["invoice_status"].ToString().Trim().Equals("N")&& !sessDS.ds.Tables[0].Rows[0]["invoice_status"].ToString().Trim().Equals("C"))
					{
						throw new Exception("Invoice status is wrong for force MBG.");
					}
				}
			}

			IDbCommand dbCmd = null;
			if(util == null) throw new ApplicationException("Utility isn't define", null);			
			string AppID = util.GetAppID(), EnterpriseID=util.GetEnterpriseID();

			int iRowsEffected = 0;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(AppID,EnterpriseID);
   
			if(dbCon==null)
			{
				throw new Exception("DbConnection object is null!!");
			}
			IDbConnection conApp = dbCon.GetConnection();
			if(conApp == null)
			{
				Logger.LogTraceError("ForceMBGMgrDAL","RevertMBG","ERR003","conApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			try
			{
				dbCon.OpenConnection(ref conApp);
			}
			catch(ApplicationException appException)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("ForceMBGMgrDAL","RevertMBG","ERR004","Error opening connection : "+ appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ",appException);
			}
			catch(Exception exception)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("ForceMBGMgrDAL","RevertMBG","ERR005","Error opening connection : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}
			IDbTransaction transactionApp = dbCon.BeginTransaction(conApp,IsolationLevel.ReadCommitted);
			try
			{
				dbCmd = dbCon.CreateCommand("", CommandType.Text);
				dbCmd.Connection = conApp;
				dbCmd.Transaction = transactionApp;
				String strSQL=""; 
				strSQL+=" UPDATE Shipment SET  mbg='"+ strMBG_Flag +"',mbg_amount= 0.00,forcembg_amount=0.00,invoice_amt=tot_freight_charge + insurance_surcharge + other_surch_amount + tot_vas_surcharge + esa_surcharge + total_excp_charges + invoice_adjustment + 0.00 ";
				strSQL+=" WHERE  applicationid='"+ util.GetAppID() +"'";
				strSQL+=" and enterpriseid='"+util.GetEnterpriseID()+"'";
				strSQL+=" and consignment_no='"+ strConsignment_no +"'";
				dbCmd = dbCon.CreateCommand(strSQL,CommandType.Text);
				dbCmd.Connection = conApp;
				dbCmd.Transaction = transactionApp;
				iRowsEffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);

				if (!strInvoice_no.Equals(""))
				{
					if (sessDS.ds.Tables[0].Rows[0]["invoice_status"]!=DBNull.Value)
					{
						if(sessDS.ds.Tables[0].Rows[0]["invoice_status"].ToString().Trim().Equals("N"))
						{
							strSQL=""; 
							strSQL+=" UPDATE Invoice_Detail SET  mbg='"+ strMBG_Flag +"',mbg_amount= 0.00,invoice_amt=tot_freight_charge + insurance_amt + other_surcharge + tot_vas_surcharge + esa_surcharge + total_exception + invoice_adj_amount + 0.00 " ;
							strSQL+=" WHERE  applicationid='"+ util.GetAppID() +"'";
							strSQL+=" and enterpriseid='"+util.GetEnterpriseID()+"'";
							strSQL+=" and consignment_no='"+ strConsignment_no +"'";
							strSQL+=" and invoice_no='"+ strInvoice_no +"'";
							dbCmd.CommandText = strSQL.ToString();
							iRowsEffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);

					
							strSQL=""; 
							strSQL+=" UPDATE inv ";
							strSQL+=" SET inv.mbg_amount = ind.mbg_amount, inv.invoice_amt= ind.invoice_amt ";
							strSQL+=" FROM ( SELECT applicationid,enterpriseid,invoice_no,SUM(invoice_amt)AS invoice_amt, SUM(mbg_amount)-("+ dblMBG_Amount +") AS mbg_amount ";
							strSQL+=" FROM Invoice_Detail ";
							strSQL+=" WHERE (invoice_no = '"+ strInvoice_no +"' and applicationid='"+ util.GetAppID() +"' and enterpriseid='"+util.GetEnterpriseID()+"' and  mbg_amount is not null) ";
							strSQL+=" group by invoice_no,applicationid,enterpriseid) ind ";
							strSQL+=" Inner join Invoice inv on inv.invoice_no=ind.invoice_no ";
							strSQL+=" and inv.enterpriseid=ind.enterpriseid";
							strSQL+=" and inv.applicationid=ind.applicationid";

							dbCmd.CommandText = strSQL.ToString();
							iRowsEffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
						}
					}
				}
				strSQL=""; 
				strSQL+=" Update shipment_tracking set Remark=null ";
				strSQL+=" WHERE  applicationid='"+ util.GetAppID() +"'";
				strSQL+=" and enterpriseid='"+util.GetEnterpriseID()+"'";
				strSQL+=" and consignment_no='"+ strConsignment_no +"'";	
				strSQL+=" and tracking_datetime=(select Max(tracking_datetime) from shipment_tracking ";
				strSQL+=" WHERE  applicationid='"+ util.GetAppID() +"'";
				strSQL+=" and enterpriseid='"+util.GetEnterpriseID()+"'";
				strSQL+=" and consignment_no='"+ strConsignment_no +"')";
				dbCmd.CommandText = strSQL.ToString();
				iRowsEffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);

				transactionApp.Commit();
			}
			catch(ApplicationException ex)
			{
				try
				{
					transactionApp.Rollback();
				}
				catch(Exception rollbackException)
				{
					throw new ApplicationException("Error Updating MBG ",ex);
				}
				throw new ApplicationException("Error Updating MBG ",ex);
			}
			finally
			{
				if(conApp != null)
				{
					conApp.Close();
				}
			}

			return iRowsEffected;
		}



		private static string NullDB(object obj)
		{
			if(obj==DBNull.Value) return "NULL";
			else return obj.ToString();
		}
	}
}
