using System;
using System.Data;
using System.Collections;
using System.Text;
using com.common.DAL;
using com.common.util;
using com.common.classes;
using com.ties.classes;

namespace com.ties.DAL
{
	public struct PODDetails
	{
		public String PODDateTime;
		public String PODException;
		public String PersonIncharge;
		public String PODRemark;
	}

	/// <summary>
	/// Summary description for ShipmentTrackingMgrDAL.
	/// </summary>
	public class ShipmentTrackingMgrDAL
	{
		public ShipmentTrackingMgrDAL()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		public static SessionDS QueryShipmentTracking(DataSet dsShipTrack, String strAppID, String strEnterpriseID, int recStart, int recSize)
		{
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			DataRow dr = dsShipTrack.Tables[0].Rows[0];

			String strSQLWhere = null;
			String strDateColName = "";
			String strTranDate = dr[0].ToString();
			String strShowHistory = dr[16].ToString();
			String strStartDate = null;
			String strEndDate = null;
			if(strTranDate != "")
			{
				if (strTranDate == "B")
				{
					//strSQLWhere += "and latest_code like '"+strTranDate+"' ";
					strDateColName = "s.booking_datetime";
				}
				else if (strTranDate == "P")
				{
					strDateColName = "s.act_pickup_datetime";
				}
				else
				{
					strDateColName = "s.last_status_datetime";
				}

				if(dr[1] != System.DBNull.Value)
				{
					DateTime dateStartDate = (DateTime) dr[1];
					strStartDate = Utility.DateFormat(strAppID, strEnterpriseID, dateStartDate, DTFormat.DateTime);
					if(strShowHistory == "N")
					{
						strSQLWhere += "and "+strDateColName+" >= "+strStartDate+" ";
					}
					else
					{
						if(strTranDate != "S")
						{
							strSQLWhere += "and "+strDateColName+" >= "+strStartDate+" ";
						}
					}
				}
				if(dr[2] != System.DBNull.Value)
				{
					DateTime dateEndDate = (DateTime) dr[2];
					strEndDate = Utility.DateFormat(strAppID, strEnterpriseID, dateEndDate, DTFormat.DateTime);
					if(strShowHistory == "N")
					{
						strSQLWhere += "and "+strDateColName+" <= "+strEndDate+" ";
					}
					else
					{
						if(strTranDate != "S")
						{
							strSQLWhere += "and "+strDateColName+" <= "+strEndDate+" ";
						}
					}
				}
			}
			String strPayerType = dr[3].ToString();	
			String strPayerId = dr[4].ToString();

			if(strPayerType != "")
			{
				if(strPayerId != "")
				{
					strSQLWhere += "and s.payerid like '"+strPayerId+"' ";
				  //strSQLWhere += "and s.payer_type = '"+strPayerType+"' "; //Thosapol.y Comment (25/06/2013)
					strSQLWhere += "and c.payer_type = '"+strPayerType+"' "; //Thosapol.y Modify (25/06/2013)
				}
			}
			String strRouteType = dr[5].ToString();
			String strRouteCode = dr[6].ToString();
			if(strRouteCode != "")
			{
				strSQLWhere += "and s.route_code like '"+strRouteCode+"' ";
			}
			String strZipCode = dr[7].ToString();
			if(strZipCode != "")
			{
				strSQLWhere += "and s.recipient_zipcode like '"+strZipCode+"' ";
			}

			String strStateCode = dr[8].ToString();
			if(strStateCode != "")
			{
				strSQLWhere += "and z1.state_code like '"+strStateCode+"' ";
			}

			String strBookingType = dr[9].ToString();
			if((strBookingType.Trim() != "") && (strBookingType.Trim() !=  "0"))
			{
				strSQLWhere += "and isnull(pkr.booking_type, 'C') like '"+strBookingType+"' ";
			}

			String strConsgNo = dr[10].ToString();
			if(strConsgNo != "")
			{
				strSQLWhere += "and s.consignment_no like '"+strConsgNo+"' ";
			}
			String strBookingNo = dr[11].ToString();
			if(strBookingNo != "")
			{
				strSQLWhere += "and s.booking_no like '"+strBookingNo+"' ";
			}
			String strStatusCode = dr[12].ToString();
			if(strStatusCode != "")
			{
				strSQLWhere += "and stf.status_code like '"+strStatusCode+"' ";
			}
			String strExcepCode = dr[13].ToString();
			if(strExcepCode != "")
			{
				strSQLWhere += "and isnull(stf.exception_code, '') like '"+strExcepCode+"' ";
			}
			String strShipClosed = dr[14].ToString();
			
			//#Start:PSA 2009_029 By Gwang on 13Mar09#
			String strRefNo = dr["ref_no"].ToString();
			if(strRefNo != "")
			{
				strSQLWhere += "and isnull(s.ref_no, '') = '"+strRefNo+"' ";
			}
			//#End:PSA 2009_029
			if(dr["UserLocation"] != DBNull.Value && dr["UserLocation"].ToString().Trim() != "")
			{
				String strUserLocation = dr["UserLocation"].ToString();
				strSQLWhere += "and ( s.destination_station = '"+strUserLocation+"' ";
				strSQLWhere += "or  s.origin_station = '"+strUserLocation+"' ) ";
			}
			

			String strQuery = "select s.booking_no, s.consignment_no, s.booking_datetime, ";
			strQuery += "s.act_pickup_datetime, s.payerid, s.payer_name, ";
			strQuery += "s.ref_no, s.sender_country, s.recipient_country, ";
			strQuery += "stf.last_updated last_status_datetime, ";
			strQuery += "stf.status_code last_status_code, isnull(stf.exception_code, '') last_exception_code, ";
			strQuery += "s.route_code, s.invoice_date, 'No' as Status_Close, ";
			strQuery += "z1.state_code as rec_state_code, ";
			strQuery += "z2.state_code as sen_state_code, ";
			//Modify By Tom 21/10/2009
			strQuery += "z2.pickup_route + '<br>' + s.route_code as Pup_DEL, isnull(pkr.booking_type, 'C') booking_type ";
			//strQuery += "z2.pickup_route + '<br>' + z1.delivery_route as Pup_DEL, isnull(pkr.booking_type, 'C') booking_type ";
			//End Modify By Tom 21/10/2009
			strQuery += "from shipment s with(nolock) ";
			strQuery += "inner join zipcode z1 with(nolock) ";
			strQuery += "on (s.applicationid = z1.applicationid ";
			strQuery += "and s.enterpriseid = z1.enterpriseid ";
			strQuery += "and s.recipient_zipcode = z1.zipcode) ";
			strQuery += "left outer join zipcode z2 with(nolock) ";
			strQuery += "on (s.applicationid = z2.applicationid ";
			strQuery += "and s.enterpriseid = z2.enterpriseid ";
			strQuery += "and s.sender_zipcode = z2.zipcode) ";
			strQuery += "inner join (select st2.applicationid, st2.enterpriseid, ";
			strQuery += "st2.consignment_no, st2.status_code, st2.exception_code, ";
			strQuery += "st1.tracking_datetime,st2.last_updated ";
			strQuery += "from ( select st0.applicationid, st0.enterpriseid, st0.consignment_no, ";
			if(strShowHistory == "Y" && strStatusCode != "")
			{
				strQuery += "st0.tracking_datetime tracking_datetime ";
				strQuery += "from ( select applicationid, enterpriseid, tracking_datetime, consignment_no ";
				strQuery += "from shipment_tracking with(nolock) ";
				strQuery += "where isnull(deleted, 'N') <> 'Y' ";
				strQuery += "and applicationid = '"+strAppID+"' ";
				strQuery += "and enterpriseid = '"+strEnterpriseID+"' "; 
				if (strTranDate == "S")
				{
					strQuery += "and tracking_datetime >= "+strStartDate+" and tracking_datetime <= "+strEndDate+" ";
				}
				if(strStatusCode != "")
				{
					strQuery += "and status_code='"+strStatusCode+"' ";
				}
				strQuery += ") st0 ";
				if(strConsgNo != "")
				{
					strQuery += "where consignment_no = '"+strConsgNo+"' ";
				}
				strQuery += ") st1 ";
			}
			else
			{
			strQuery += "max(st0.last_updated) tracking_datetime ";
				strQuery += "from ( select * ";
				strQuery += "from shipment_tracking with(nolock) ";
				strQuery += "where isnull(deleted, 'N') <> 'Y' ";
				strQuery += "and applicationid = '"+strAppID+"' ";
				strQuery += "and enterpriseid = '"+strEnterpriseID+"' ) st0 ";
				if(strConsgNo != "")
				{
					strQuery += "where consignment_no = '"+strConsgNo+"' ";
				}
				strQuery += "group by st0.applicationid, st0.enterpriseid, st0.consignment_no) st1 ";
			}
			strQuery += "inner join shipment_tracking st2 with(nolock) ";
			strQuery += "on (st1.applicationid = st2.applicationid ";
			strQuery += "and st1.enterpriseid = st2.enterpriseid ";
			strQuery += "and st1.consignment_no = st2.consignment_no ";
			strQuery += "and st1.tracking_datetime = st2.last_updated)) stf ";
			strQuery += "on (s.applicationid = stf.applicationid and s.enterpriseid = stf.enterpriseid and s.consignment_no = stf.consignment_no) ";
			strQuery += "left outer join Pickup_Request pkr on (s.applicationid = pkr.applicationid ";
			strQuery += "and s.enterpriseid = pkr.enterpriseid ";
			strQuery += "and s.booking_no = pkr.booking_no) ";
			strQuery += " INNER JOIN Customer AS c WITH (nolock) ON s.payerid = c.custid "; //Thosapol.y Add Line Code (25/06/2013)
			strQuery += "where s.applicationid = '"+strAppID+"' "; 
			strQuery += "and s.enterpriseid = '"+strEnterpriseID+"' ";

			if (strSQLWhere!=null && strSQLWhere!="")
			{
				strQuery += strSQLWhere;
			}

			strQuery += "ORDER BY s.booking_no DESC";
			IDbCommand idbCom = dbCon.CreateCommand(strQuery, CommandType.Text);
			//SessionDS sessionds = dbCon.ExecuteQuery(idbCom, recStart, recSize,"shipment");
			SessionDS sessionds = dbCon.ExecuteQuery(strQuery, recStart, recSize,"shipment");
			return sessionds;
		}

		public static SessionDS QueryShipmentTracking_New(DataSet dsShipTrack, String strAppID, String strEnterpriseID, int recStart, int recSize)
		{
			SessionDS sessionds = new SessionDS();
			IDbCommand dbCmd = null;

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("module1","DailyLodgmentsMgrDAL","QueryDailyLodgments","ERR001","DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null",null);
			}
			try
			{
				System.Collections.ArrayList storedParams = new System.Collections.ArrayList();
				DataRow dr = dsShipTrack.Tables[0].Rows[0];
				
				//Enterprise
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@enterpriseid",dr[0].ToString()));
				//UserId
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@userid",dr[1].ToString()));
				//Start Date
				if(dr[2].ToString() == "")
				{
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@Startdate",DBNull.Value));
				}
				else
				{
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@Startdate",dr[2].ToString()));
				}				
				//End Date
				if(dr[3].ToString() == "")
				{
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@Enddate",DBNull.Value));				
				}
				else
				{
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@Enddate",dr[3].ToString()));	
				}
				//Date Type
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@DateType",dr[4]));
				// PayerId
				if(dr[5].ToString() == "")
				{
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@PayerID",DBNull.Value));
				}
				else
				{
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@PayerID",dr[5].ToString()));
				}
				// Payer Type
				if(dr[6].ToString() == "")
				{
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@PayerType",DBNull.Value));
				}
				else
				{
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@PayerType",dr[6].ToString()));
				}
				// MasterAccount
				if(dr[7].ToString() == "")
				{
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@master_account",DBNull.Value));
				}
				else
				{
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@master_account",dr[7].ToString()));
				}
				// Route Type
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@RouteType",dr[8]));
				// RouteCode
				if(dr[9].ToString() == "")
				{
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@RouteCode",DBNull.Value));
				}
				else
				{
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@RouteCode",dr[9].ToString()));
				}
				// ZipCode
				if(dr[10].ToString() == "")
				{
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@Recipient_zipcode",DBNull.Value));
				}
				else
				{
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@Recipient_zipcode",dr[10].ToString()));
				}
				// StateCode
				if(dr[11].ToString() == "")
				{
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@destination_state_code",DBNull.Value));
				}
				else
				{
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@destination_state_code",dr[11].ToString()));
				}
				// Consignment No
				if(dr[12].ToString() == "")
				{
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@consignment_no",DBNull.Value));
				}
				else
				{
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@consignment_no",dr[12].ToString()));
				}
				// booking No
				if(dr[13].ToString() == "")
				{
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@booking_no",DBNull.Value));
				}
				else
				{
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@booking_no",dr[13].ToString()));
				}
				// ref No
				if(dr[14].ToString() == "")
				{
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@ref_no",DBNull.Value));
				}
				else
				{
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@ref_no",dr[14].ToString()));
				}
				// Booking Type
				if(dr[15].ToString() == "")
				{
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@Booking_type",DBNull.Value));
				}
				else
				{
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@Booking_type",dr[15].ToString()));
				}
				// Status Code
				if(dr[16].ToString() == "")
				{
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@status_code",DBNull.Value));
				}
				else
				{
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@status_code",dr[16].ToString()));
				}
				// exceptionCode 
				if(dr[17].ToString() == "")
				{
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@exception_code",DBNull.Value));
				}
				else
				{
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@exception_code",dr[17].ToString()));
				}
				// Status type 
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@status_type",dr[18]));
				// Invoiced 
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@invoiced",dr[19]));
				// Closed
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@closed",dr[20]));			
				// Service Type
				if(dr[21].ToString() == "")
				{
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@service_type",DBNull.Value));
				}
				else
				{
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@service_type",dr[21].ToString()));
				}

				dbCmd = dbCon.CreateCommand("QueryShipmentTracking1",storedParams, CommandType.StoredProcedure);
				DataSet ds = (DataSet)dbCon.ExecuteQuery(dbCmd, com.common.DAL.ReturnType.DataSetType);
				sessionds.ds = ds;
				sessionds.DataSetRecSize = 50;
				sessionds.QueryResultMaxSize = ds.Tables[0].Rows.Count;
				}
		catch(Exception ex)
			{
				throw ex;
			}		
			return sessionds;
		}

		public static SessionDS ShipmentForDeletion(String strAppID, String strEnterpriseID, String strBookNo, String strCongNo, int recStart, int recSize)
		{
			
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon==null)
			{
				Logger.LogTraceError("ShipmentTrackingMgrDAL","ShipmentForDeletion","SDM002","conApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

//			String strQry = "select s.booking_no, s.booking_datetime, s.act_pickup_datetime, s.payerid, s.payer_name, s.consignment_no, s.ref_no, ";
//			strQry += "s.shipment_type, s.sender_country, s.recipient_country, st.deleted, st.delete_remark, st.tracking_datetime, st.status_code, st.exception_code, st.person_incharge, st.remark ";
//			strQry += "from shipment s, shipment_tracking st where s.applicationid = st.applicationid and s.enterpriseid = st.enterpriseid and s.booking_no = st.booking_no ";
//			strQry += "and st.applicationid = '"+strAppID+"' and st.enterpriseid = '"+strEnterpriseID+"' ";
//			strQry += "and st.booking_no = '"+strBookNo+"' ";
//			//strQry += "group by s.booking_no, s.booking_datetime, s.act_pickup_datetime, s.payerid, s.payer_name, s.consignment_no, s.ref_no, ";
//			//strQry += "s.shipment_type, s.sender_country, s.recipient_country, st.deleted, st.delete_remark, st.tracking_datetime, st.status_code, st.exception_code, st.person_incharge, st.remark ";
//			strQry += "ORDER BY st.tracking_datetime DESC";

			String strQry = "select s.booking_no, s.booking_datetime, ";
			strQry += "s.act_pickup_datetime, s.payerid, s.payer_name, s.consignment_no, ";
			strQry += "s.ref_no, s.shipment_type, s.sender_country, s.recipient_country, ";
			strQry += "st.deleted, st.delete_remark, st.tracking_datetime, st.status_code, ";
			strQry += "isnull(st.exception_code, '') exception_code, upper(st.person_incharge) as person_incharge, st.remark, upper(st.location) as location , "; 
			//strQry += " upper(st.last_userid) as last_userid ";
			strQry += " CASE WHEN CHARINDEX(' ', REPLACE(REPLACE(user_name, 'SWB ', ''), ' ', ' ')) = 0 ";
			strQry += " THEN REPLACE(user_name, 'SWB ', '')";
			strQry += " ELSE SUBSTRING(REPLACE(REPLACE(user_name, 'SWB ', ''), ' ', ' '), 1, CHARINDEX(' ', REPLACE(REPLACE(user_name, 'SWB ', ''), ' ', ' '))-1)";
			strQry += " + '.' + SUBSTRING(REPLACE(REPLACE(user_name, 'SWB ', ''), ' ', ' '), 1+CHARINDEX(' ', REPLACE(REPLACE(user_name, 'SWB ', ''), ' ', ' ')),1)";
			strQry += " END as last_userid";

			strQry += ",st.last_updated, st.consignee_name, upper(z1.state_code) as sen_state_code,";
			strQry += "z2.state_code as rec_state_code, s.est_delivery_datetime, s.service_code, ";
			strQry += "sc.status_description, ex.exception_description ";
			strQry += "from (	select * ";
			strQry += "from shipment with (nolock) ";
			strQry += "where applicationid = '"+strAppID+"' ";
			strQry += "and enterpriseid = '"+strEnterpriseID+"' ";
			strQry += "and booking_no = '"+strBookNo+"' ";
			strQry += "and consignment_no = '"+strCongNo+"') as s ";
			strQry += "left outer join ( ";
			strQry += "select * ";
			strQry += "from shipment_tracking with (nolock) ";
			strQry += "where applicationid = '"+strAppID+"' ";
			strQry += "and enterpriseid = '"+strEnterpriseID+"' ";
			strQry += "and booking_no = '"+strBookNo+"' ";
			strQry += "and consignment_no = '"+strCongNo+"') as st ";
			strQry += "on (s.booking_no = st.booking_no and s.consignment_no = st.consignment_no) ";
			strQry += "left outer join ( ";
			strQry += "select * ";
			strQry += "from zipcode with (nolock) ";
			strQry += "where applicationid = '"+strAppID+"' ";
			strQry += "and enterpriseid = '"+strEnterpriseID+"') as z1 ";
			strQry += "on (s.sender_zipcode = z1.zipcode) ";
			strQry += "inner join ( ";
			strQry += "select * ";
			strQry += "from zipcode ";
			strQry += "where applicationid = '"+strAppID+"' ";
			strQry += "and enterpriseid = '"+strEnterpriseID+"') as z2 ";
			strQry += "on (s.recipient_zipcode = z2.zipcode) ";
			strQry += "left join exception_code ex ";
			strQry += "on(ex.applicationid=s.applicationid and ex.enterpriseid =s.enterpriseid and st.exception_code = ex.exception_code) ";
			strQry += "join (select distinct * from status_code where applicationid = '"+strAppID+"' and enterpriseid = '"+strEnterpriseID+"') sc ";
			strQry += "on(sc.applicationid=s.applicationid and sc.enterpriseid =s.enterpriseid and st.status_code = sc.status_code ) ";
			strQry += "left join user_master um on um.userid = st.last_userid";
			strQry += " ORDER BY st.tracking_datetime DESC";


			IDbCommand idbCom = dbCon.CreateCommand(strQry, CommandType.Text);
			//SessionDS sessionds = dbCon.ExecuteQuery(idbCom, recStart, recSize,"shipment");
			SessionDS sessionds = dbCon.ExecuteQuery(strQry, recStart, recSize,"shipment");
			return sessionds;
		}

		//Added by GwanG on 18Feb08
		public static SessionDS ShipmentForDeletion(String strAppID, String strEnterpriseID, String strBookNo, String strCongNo)
		{
			
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon==null)
			{
				Logger.LogTraceError("ShipmentTrackingMgrDAL","ShipmentForDeletion","SDM002","conApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			//			String strQry = "select s.booking_no, s.booking_datetime, s.act_pickup_datetime, s.payerid, s.payer_name, s.consignment_no, s.ref_no, ";
			//			strQry += "s.shipment_type, s.sender_country, s.recipient_country, st.deleted, st.delete_remark, st.tracking_datetime, st.status_code, st.exception_code, st.person_incharge, st.remark ";
			//			strQry += "from shipment s, shipment_tracking st where s.applicationid = st.applicationid and s.enterpriseid = st.enterpriseid and s.booking_no = st.booking_no ";
			//			strQry += "and st.applicationid = '"+strAppID+"' and st.enterpriseid = '"+strEnterpriseID+"' ";
			//			strQry += "and st.booking_no = '"+strBookNo+"' ";
			//			//strQry += "group by s.booking_no, s.booking_datetime, s.act_pickup_datetime, s.payerid, s.payer_name, s.consignment_no, s.ref_no, ";
			//			//strQry += "s.shipment_type, s.sender_country, s.recipient_country, st.deleted, st.delete_remark, st.tracking_datetime, st.status_code, st.exception_code, st.person_incharge, st.remark ";
			//			strQry += "ORDER BY st.tracking_datetime DESC";

			String strQry = "select s.booking_no, s.booking_datetime, ";
			strQry += "s.act_pickup_datetime, s.payerid, s.payer_name, s.consignment_no, ";
			strQry += "s.ref_no, s.shipment_type, s.sender_country, s.recipient_country, ";
			strQry += "st.deleted, st.delete_remark, st.tracking_datetime, st.status_code, ";
			strQry += "isnull(st.exception_code, '') exception_code, upper(st.person_incharge) as person_incharge, st.remark, upper(st.location) as location, "; 
			//strQry += " upper(st.last_userid) as last_userid";
			//strQry += " upper(st.last_userid) as last_userid ";
			strQry += " CASE WHEN CHARINDEX(' ', REPLACE(REPLACE(user_name, 'SWB ', ''), ' ', ' ')) = 0 ";
			strQry += " THEN REPLACE(user_name, 'SWB ', '')";
			strQry += " ELSE SUBSTRING(REPLACE(REPLACE(user_name, 'SWB ', ''), ' ', ' '), 1, CHARINDEX(' ', REPLACE(REPLACE(user_name, 'SWB ', ''), ' ', ' '))-1)";
			strQry += " + '.' + SUBSTRING(REPLACE(REPLACE(user_name, 'SWB ', ''), ' ', ' '), 1+CHARINDEX(' ', REPLACE(REPLACE(user_name, 'SWB ', ''), ' ', ' ')),1)";
			strQry += " END as last_userid";
			
			strQry += ", st.last_updated, st.consignee_name, z1.state_code as sen_state_code,";
			strQry += "z2.state_code as rec_state_code, s.est_delivery_datetime, s.service_code, ";
			strQry += "sc.status_description, ex.exception_description ";
			strQry += "from (	select * ";
			strQry += "from shipment ";
			strQry += "where applicationid = '"+strAppID+"' ";
			strQry += "and enterpriseid = '"+strEnterpriseID+"' ";
			if (strBookNo!=null)
			{
				strQry += "and booking_no = '"+strBookNo+"' ";
			}
			strQry += "and consignment_no = '"+strCongNo+"') as s ";
			strQry += "left outer join ( ";
			strQry += "select * ";
			strQry += "from shipment_tracking with (nolock) ";
			strQry += "where applicationid = '"+strAppID+"' ";
			strQry += "and enterpriseid = '"+strEnterpriseID+"' ";
			if (strBookNo!=null)
			{
				strQry += "and booking_no = '"+strBookNo+"' ";
			}
			strQry += "and consignment_no = '"+strCongNo+"') as st ";
			strQry += "on (s.booking_no = st.booking_no and s.consignment_no = st.consignment_no) ";
			strQry += "left outer join ( ";
			strQry += "select * ";
			strQry += "from zipcode with (nolock) ";
			strQry += "where applicationid = '"+strAppID+"' ";
			strQry += "and enterpriseid = '"+strEnterpriseID+"') as z1 ";
			strQry += "on (s.sender_zipcode = z1.zipcode) ";
			strQry += "inner join ( ";
			strQry += "select * ";
			strQry += "from zipcode with (nolock) ";
			strQry += "where applicationid = '"+strAppID+"' ";
			strQry += "and enterpriseid = '"+strEnterpriseID+"') as z2 ";
			strQry += "on (s.recipient_zipcode = z2.zipcode) ";
			strQry += "left join exception_code ex ";
			strQry += "on(ex.applicationid=s.applicationid and ex.enterpriseid =s.enterpriseid and st.exception_code = ex.exception_code) ";
			strQry += "join (select distinct * from status_code where applicationid = '"+strAppID+"' and enterpriseid = '"+strEnterpriseID+"') sc ";
			strQry += "on(sc.applicationid=s.applicationid and sc.enterpriseid =s.enterpriseid and st.status_code = sc.status_code ) ";
			strQry += "left join user_master um on um.userid = st.last_userid ";
			strQry += "ORDER BY st.tracking_datetime DESC";


			IDbCommand idbCom = dbCon.CreateCommand(strQry, CommandType.Text);
			//dbCon.ExecuteQuery(idbCom, recStart, recSize,"shipment");
			DataSet ds = new DataSet();
			ds = (DataSet)dbCon.ExecuteQuery(idbCom,ReturnType.DataSetType);
			//dbCon.ExecuteQuery(strQry, recStart, recSize,"shipment");
			SessionDS sessionds = new SessionDS();
			sessionds.ds = ds;
			return sessionds;
		}


		public static SessionDS ShipmentForDeletion_New(String strAppID, String strEnterpriseID, String strBookNo, String strCongNo, int intReturnPackage)
		{
			SessionDS sessionds = new SessionDS();
			IDbCommand dbCmd = null;

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon==null)
			{
				Logger.LogTraceError("ShipmentTrackingMgrDAL","ShipmentForDeletion","SDM002","conApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			System.Collections.ArrayList storedParams = new System.Collections.ArrayList();
				
			//Enterprise
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@enterpriseid",strEnterpriseID));
			//Consignment No
			if(strCongNo == "")
			{
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@consignment_no",DBNull.Value));
			}
			else
			{
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@consignment_no",strCongNo));
			}	
			//Booking No
			if(strBookNo == "")
			{
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@booking_no",DBNull.Value));
			}
			else
			{
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@booking_no",strBookNo));
			}
			//ReturnPackage
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@ReturnPackages",intReturnPackage));
			//Payerid
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@payerid",DBNull.Value));
			//RestrictToPayer
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@RestrictToPayer",0));
			//AllowBeforeMDE
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@AllowBeforeMDE",1));

			dbCmd = dbCon.CreateCommand("QueryTrackandTrace",storedParams, CommandType.StoredProcedure);
			DataSet ds = (DataSet)dbCon.ExecuteQuery(dbCmd, com.common.DAL.ReturnType.DataSetType);
			sessionds.ds = ds;
			sessionds.DataSetRecSize = 50;
			sessionds.QueryResultMaxSize = ds.Tables[0].Rows.Count;
			return sessionds;
		}

/// <summary>
/// This method USED "AS ONE TRANSACTION" in place of 
/// RevertStatus, 
/// UpdateDeleteHistory(String strAppID, String strEnterpriseID, String strBookNo, String strTrackDate, bool delete, String strDeleteRemark)
/// UpdateDeleteHistory(String strAppID, String strEnterpriseID, String strBookNo, String strExceptionToRevert, String strStatusToRevert, String strLastDateToRevert)
/// UpdateShipmentPODStatus(String strAppID, String strEnterpriseID, String strBookNo, String strTrackDate)
/// </summary>
/// <param name="strAppID"></param>
/// <param name="strEnterpriseID"></param>
/// <param name="strBookNum"></param>
/// <param name="dtLastDate"></param>
/// <param name="dtTrackDate"></param>
/// <param name="bDelete"></param>
/// <param name="strDeleteRemark"></param>
/// <param name="strStatusCode"></param>
/// <param name="strPODStatusCodes">All POD Status codes returned as Comma separated values</param>
/// <returns></returns>
		public static int UpdateShipmentStatus(String strAppID,String strEnterpriseID, String strBookNum, DateTime dtLastDate, DateTime dtTrackDate, Boolean bDelete, String strDeleteRemark, String strStatusCode, String strPODStatusCodes,String strActDelvryDate, String ConsignmentNo)
		{
			int iRows = 0;		
			DbConnection dbConApp = null;
			IDbCommand dbCommandApp = null;
			IDbTransaction transactionApp = null;
			IDbConnection conApp = null;
			String strExceptionMBGStatus=null;
			
			//Get App Database Connection
			dbConApp = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbConApp == null)
			{
				Logger.LogTraceError("ShipmentTrackingMgrDAL","UpdateShipmentStatus","ERR0001","dbConApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}			
			
			conApp = dbConApp.GetConnection();			
			if(conApp == null)
			{
				Logger.LogTraceError("ShipmentTrackingMgrDAL","UpdateShipmentStatus","ERR0002","conApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			try
			{
				dbConApp.OpenConnection(ref conApp);
			}
			catch(ApplicationException appException)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("ShipmentTrackingMgrDAL","UpdateShipmentStatus","ERR0003","Error opening connection : "+ appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ",appException);
			}
			catch(Exception exception)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("ShipmentTrackingMgrDAL","UpdateShipmentStatus","ERR0004","Error opening connection : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}
			
			String strQry = "";
			//String strActDelvryDate=null;

			//Begin App Transaction
			transactionApp = dbConApp.BeginTransaction(conApp,IsolationLevel.ReadCommitted);
			if(transactionApp == null)
			{
				Logger.LogTraceError("ShipmentTrackingMgrDAL","UpdateShipmentStatus","ERR0005","transactionApp is null");
				throw new ApplicationException("Failed Begining a App Transaction..",null);
			}
			
			String strLastDate=Utility.DateFormat(strAppID, strEnterpriseID, dtLastDate, DTFormat.DateVeryLongTime);
			String strTrackDate=Utility.DateFormat(strAppID, strEnterpriseID, dtTrackDate, DTFormat.DateVeryLongTime);

			//String strLastDate=Utility.DateFormat(strAppID, strEnterpriseID, dtLastDate, DTFormat.DateTime);
			//String strTrackDate=Utility.DateFormat(strAppID, strEnterpriseID, dtTrackDate, DTFormat.DateTime);

			try
			{
				//if(dtTrackDate >= dtLastDate)
				if(dtTrackDate >= dtLastDate)
				{	
					strQry = "select tracking_datetime, status_code, exception_code ";
					strQry += "from shipment_tracking with(nolock) ";
					strQry += "where (enterpriseid = '"+strEnterpriseID+"') ";
					strQry += "and (applicationid = '"+strAppID+"') ";
					strQry += "and (booking_no = '"+strBookNum+"') ";
					strQry += "and (deleted NOT IN ('y','Y') OR deleted IS NULL) ";
					strQry += "AND (tracking_datetime = (SELECT MAX(tracking_datetime) ";
					strQry += "FROM shipment_tracking with(nolock) ";
					strQry += "WHERE (enterpriseid = '"+strEnterpriseID+"') ";
					strQry += "and (applicationid = '"+strAppID+"') ";
					strQry += "and (booking_no = '"+strBookNum+"') ";
					// Jiab 15/11/53
					strQry += "and (consignment_no = '"+ ConsignmentNo +"') ";
					// Jiab 15/11/53 End
					strQry += "and (tracking_datetime <"+strLastDate+") ";
					strQry += "AND (deleted NOT IN ('y','Y') OR deleted IS NULL)))";
			
					SessionDS sessionds;
					try
					{
						sessionds = dbConApp.ExecuteQuery(strQry,0,5,"revertEalierStatus");
						if(sessionds.ds.Tables[0].Rows.Count>0)
						{
							DataRow dr = sessionds.ds.Tables[0].Rows[0];
							String strStatusCodeToRevert = dr["status_code"].ToString();
							String strExceptCodeToRevert = dr["exception_code"].ToString();
							DateTime dtDateToRevert = (DateTime)dr["tracking_datetime"];
							//String strDateToRevert = Utility.DateFormat(strAppID, strEnterpriseID, dtTrackDate, DTFormat.DateTime);				
							String strDateToRevert = Utility.DateFormat(strAppID, strEnterpriseID, dtDateToRevert, DTFormat.DateTime);				

							strQry= " Update Shipment with(rowlock) set last_exception_code = '"+strExceptCodeToRevert+"'";
							strQry+=" ,last_status_datetime = "+strDateToRevert+",";
							strQry+=" last_status_code = '"+strStatusCodeToRevert+"' ";
							strQry+=" where applicationid = '"+strAppID+"' ";
							strQry+=" and enterpriseid = '"+strEnterpriseID+"'";
							strQry+=" and booking_no = "+strBookNum;
							strQry+=" and consignment_no = '"+ConsignmentNo + "'";
					
							dbCommandApp = dbConApp.CreateCommand(strQry,CommandType.Text);
							dbCommandApp.Connection = conApp;
							dbCommandApp.Transaction = transactionApp;
							iRows = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);
							Logger.LogTraceInfo("ShipmentTrackingMgrDAL","UpdateShipmentStatus","ERR0012",iRows + " rows updated into Shipment table");							
						}
						
					}
					catch(Exception exp)
					{
						Logger.LogTraceInfo("ShipmentTrackingMrgDAL","RevertStatus","SDM002",exp.Message);
						throw new ApplicationException("Unable to Revert to earlier date.",null);
					}
					
				}

				//HC Return Task
				StringBuilder strBuilderTMP = new StringBuilder();
				if (strStatusCode == "HCR")
				{
					DataSet tmpShip = new DataSet();
					DataSet tmpHasPOD = new DataSet();

					DateTime EstPODDt = new DateTime();
					DateTime ActPODDt = new DateTime();
					DateTime EstHCDt = new DateTime();

					//check is contained POD status
					strBuilderTMP = new StringBuilder();
					strBuilderTMP.Append("SELECT * ");
					strBuilderTMP.Append("FROM shipment_tracking with(nolock) ");
					strBuilderTMP.Append("WHERE applicationid = '"+strAppID+"' ");
					strBuilderTMP.Append("AND enterpriseid = '"+strEnterpriseID+"' ");
					strBuilderTMP.Append("AND consignment_no = '"+ConsignmentNo+"' ");
					strBuilderTMP.Append("AND booking_no ="+ strBookNum + " ");
					strBuilderTMP.Append("AND (ltrim(rtrim(status_code)) = 'POD' and upper(rtrim(ltrim(isnull(deleted, 'N')))) <> 'Y')");

					dbCommandApp = dbConApp.CreateCommand(strBuilderTMP.ToString(),CommandType.Text);
					dbCommandApp.Connection = conApp;
					dbCommandApp.Transaction = transactionApp;
					tmpHasPOD = (DataSet)dbConApp.ExecuteQueryInTransaction(dbCommandApp, ReturnType.DataSetType);

					//get shipment data
					strBuilderTMP = new StringBuilder();
					strBuilderTMP.Append("SELECT * ");
					strBuilderTMP.Append("FROM shipment with(nolock) ");
					strBuilderTMP.Append("WHERE applicationid = '"+strAppID+"' ");
					strBuilderTMP.Append("AND enterpriseid = '"+strEnterpriseID+"' ");
					strBuilderTMP.Append("AND consignment_no = '"+ConsignmentNo+"' ");
					strBuilderTMP.Append("AND booking_no ="+ strBookNum + " ");

					dbCommandApp = dbConApp.CreateCommand(strBuilderTMP.ToString(),CommandType.Text);
					dbCommandApp.Connection = conApp;
					dbCommandApp.Transaction = transactionApp;
					tmpShip = (DataSet)dbConApp.ExecuteQueryInTransaction(dbCommandApp, ReturnType.DataSetType);

					if (tmpHasPOD.Tables[0].Rows.Count > 0)
					{
						// if shipment contain POD Status
						ActPODDt = (DateTime)tmpHasPOD.Tables[0].Rows[0]["tracking_datetime"];
						EstHCDt = (DateTime)DomesticShipmentMgrDAL.calEstHCDateTime(ActPODDt, tmpShip.Tables[0].Rows[0]["destination_state_code"].ToString(),
							ConsignmentNo.Trim(), strBookNum.Trim(), strAppID, strEnterpriseID,dbConApp,dbCommandApp);

						strBuilderTMP = new StringBuilder();
						strBuilderTMP.Append("UPDATE shipment with(rowlock) ");
						strBuilderTMP.Append("SET est_hc_return_datetime ="+Utility.DateFormat(strAppID,strEnterpriseID,EstHCDt,DTFormat.DateTime)+", "); 
						strBuilderTMP.Append("act_hc_return_datetime = null ");
						strBuilderTMP.Append("WHERE applicationid ='"+strAppID+"' ");
						strBuilderTMP.Append("AND enterpriseid = '"+strEnterpriseID+"' "); 
						strBuilderTMP.Append("AND booking_no ="+strBookNum + " ");
						strBuilderTMP.Append("AND consignment_no = '"+ConsignmentNo + "'");

						dbCommandApp = dbConApp.CreateCommand(strBuilderTMP.ToString(),CommandType.Text);
						dbCommandApp.Connection = conApp;
						dbCommandApp.Transaction = transactionApp;
						iRows = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);	
					}
					else
					{
						// if shipment doesn't contain POD Status
						EstPODDt = (DateTime)tmpShip.Tables[0].Rows[0]["est_delivery_datetime"];
						EstHCDt = (DateTime)DomesticShipmentMgrDAL.calEstHCDateTime(EstPODDt, tmpShip.Tables[0].Rows[0]["destination_state_code"].ToString(),
							ConsignmentNo.Trim(), strBookNum.Trim(), strAppID, strEnterpriseID);

						strBuilderTMP = new StringBuilder();
						strBuilderTMP.Append("UPDATE shipment with(rowlock) ");
						strBuilderTMP.Append("SET est_hc_return_datetime ="+Utility.DateFormat(strAppID,strEnterpriseID,EstHCDt,DTFormat.DateTime)+", "); 
						strBuilderTMP.Append("act_hc_return_datetime = null ");
						strBuilderTMP.Append("WHERE applicationid ='"+strAppID+"' ");
						strBuilderTMP.Append("AND enterpriseid = '"+strEnterpriseID+"' "); 
						strBuilderTMP.Append("AND booking_no ="+strBookNum + " ");
						strBuilderTMP.Append("AND consignment_no = '"+ConsignmentNo + "'");

						dbCommandApp = dbConApp.CreateCommand(strBuilderTMP.ToString(),CommandType.Text);
						dbCommandApp.Connection = conApp;
						dbCommandApp.Transaction = transactionApp;
						iRows = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);	
						
					}
				}
				//HC Return Task

				String strDelete = null;
				if(bDelete)
				{
					strDelete = "Y";
				}
				else if(!bDelete)
				{
					strDelete = "N";
				}
				strQry= " Update shipment_tracking with(rowlock) set deleted = '"+strDelete+"'";
				strQry+=" , delete_remark = N'"+strDeleteRemark +"'";
				strQry+=" where applicationid = '"+strAppID+"' ";
				strQry+=" and enterpriseid = '"+strEnterpriseID+"'";
				strQry+=" and booking_no = "+strBookNum+ " ";
				strQry+=" and tracking_datetime = "+strTrackDate;
				strQry+=" and consignment_no = '"+ConsignmentNo + "'";

				dbCommandApp = dbConApp.CreateCommand(strQry,CommandType.Text);
				dbCommandApp.Connection = conApp;
				dbCommandApp.Transaction = transactionApp;
				iRows = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);
				
				Logger.LogTraceInfo("ShipmentTrackingMgrDAL","UpdateShipmentStatus","ERR0012",iRows + " rows updated into shipment_tracking table");								

		
				if (strActDelvryDate.Trim()=="")
				{
					strActDelvryDate="null";
				}
				char comma=',';
				String[] strPODs=strPODStatusCodes.Split(comma);
				for (int i=0;i< strPODs.Length;i++)
				{
					if(strStatusCode==strPODs[i].ToString())
					{
						strQry= " Update shipment with(rowlock) set Act_Delivery_Date ="+strActDelvryDate+ " ";
						strQry+=" where applicationid ='"+strAppID+"'";
						strQry+=" and enterpriseid = '"+strEnterpriseID+"' ";
						strQry+=" and booking_no ="+strBookNum;
						strQry+=" and consignment_no = '"+ConsignmentNo + "'";
						dbCommandApp = dbConApp.CreateCommand(strQry,CommandType.Text);
						dbCommandApp.Connection = conApp;
						dbCommandApp.Transaction = transactionApp;
						iRows = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);
						Logger.LogTraceInfo("ShipmentTrackingMgrDAL","UpdateShipmentStatus","ERR0012",iRows + " rows updated into shipment_tracking table");							
					}
				}
				//finding MBG				
				if (strBookNum !=null)
				{
					strExceptionMBGStatus=getMBGStatus(strAppID,strEnterpriseID,Int32.Parse(strBookNum),dtTrackDate, ConsignmentNo,dbConApp,dbCommandApp);
				}
				// ends finding MBG
				//updating mbg in shipment
				if(strExceptionMBGStatus !=null)
				{
					strQry= " Update shipment with(rowlock) set mbg ='"+strExceptionMBGStatus+"' ";
					strQry+=" where applicationid ='"+strAppID+"'";
					strQry+=" and enterpriseid = '"+strEnterpriseID+"' ";
					strQry+=" and booking_no ="+strBookNum;
					strQry+=" and consignment_no = '"+ConsignmentNo + "'";
					dbCommandApp = dbConApp.CreateCommand(strQry,CommandType.Text);
					dbCommandApp.Connection = conApp;
					dbCommandApp.Transaction = transactionApp;
					iRows = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);
					Logger.LogTraceInfo("ShipmentTrackingMgrDAL","UpdateShipmentStatus","ERR0013",iRows + " rows updated into shipment_tracking table");							
				}


				//commit the transaction
				transactionApp.Commit();
				Logger.LogTraceInfo("ShipmentTrackingMgrDAL","UpdateShipmentStatus","ERR0014","App db Transaction committed.");
			}

			catch(ApplicationException appException)
			{				
				try
				{
					transactionApp.Rollback();
					Logger.LogTraceInfo("ShipmentTrackingMgrDAL","UpdateShipmentStatus","ERR0006","App db delete transaction rolled back.");
				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("ShipmentTrackingMgrDAL","UpdateShipmentStatus","ERR0007","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}

				Logger.LogTraceError("ShipmentTrackingMgrDAL","UpdateShipmentStatus","ERR0008","Transaction Failed : "+ appException.Message.ToString());
				throw new ApplicationException("Error  "+appException.Message,appException);
			}
			catch(Exception exception)
			{
				try
				{
					transactionApp.Rollback();
					Logger.LogTraceInfo("ShipmentTrackingMgrDAL","UpdateShipmentStatus","ERR0009","App db delete transaction rolled back.");
				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("ShipmentTrackingMgrDAL","UpdateShipmentStatus","ERR0010","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}

				Logger.LogTraceError("ShipmentTrackingMgrDAL","UpdateShipmentStatus","ERR0011","Transaction Failed : "+ exception.Message.ToString());
				throw new ApplicationException("Error "+exception.Message,exception);
			}
			finally
			{
				if(conApp != null)
				{
					conApp.Close();
				}
			}
			
			return iRows;

		}
		

		//Added By Tom 16/12/2009
		public static int UpdateDomesticShipment(String strAppID,String strEnterpriseID, String ConsignmentNo)
		{
			int iRows = 0;		
			DbConnection dbConApp = null;
			IDbCommand dbCommandApp = null;
			IDbTransaction transactionApp = null;
			IDbConnection conApp = null;
			// ends finding MBG
			
			//Get App Database Connection
			dbConApp = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbConApp == null)
			{
				Logger.LogTraceError("ShipmentTrackingMgrDAL","UpdateShipmentStatus","ERR0001","dbConApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}			
			
			conApp = dbConApp.GetConnection();			
			if(conApp == null)
			{
				Logger.LogTraceError("ShipmentTrackingMgrDAL","UpdateShipmentStatus","ERR0002","conApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			try
			{
				dbConApp.OpenConnection(ref conApp);
			}
			catch(ApplicationException appException)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("ShipmentTrackingMgrDAL","UpdateShipmentStatus","ERR0003","Error opening connection : "+ appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ",appException);
			}
			catch(Exception exception)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("ShipmentTrackingMgrDAL","UpdateShipmentStatus","ERR0004","Error opening connection : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}
			
			String strQry = "";
			//String strActDelvryDate=null;

			//Begin App Transaction
			transactionApp = dbConApp.BeginTransaction(conApp,IsolationLevel.ReadCommitted);
			if(transactionApp == null)
			{
				Logger.LogTraceError("ShipmentTrackingMgrDAL","UpdateShipmentStatus","ERR0005","transactionApp is null");
				throw new ApplicationException("Failed Begining a App Transaction..",null);
			}

			try
			{
				strQry = "select top 1 status_code, exception_code ";
				strQry += "from shipment_tracking with(nolock) ";
				strQry += "where(enterpriseid = '"+strEnterpriseID+"') ";
				strQry += "and (applicationid = '"+strAppID+"') ";
				strQry += "AND (ISNULL(deleted, 'N') <> 'Y')  ";
				strQry += "and consignment_no = '"+ConsignmentNo+"' ";
				strQry += "ORDER BY tracking_datetime DESC";
			
				SessionDS sessionds;
				try
				{
					sessionds = dbConApp.ExecuteQuery(strQry,0,5,"revertEalierStatus");
					if(sessionds.ds.Tables[0].Rows.Count>0)
					{
						DataRow dr = sessionds.ds.Tables[0].Rows[0];
						String strStatusCodeToRevert = dr["status_code"].ToString();
						String strExceptCodeToRevert = dr["exception_code"].ToString();				

						strQry= " Update Shipment with(rowlock) set last_exception_code = '"+strExceptCodeToRevert+"',";
						strQry+=" last_status_code = '"+strStatusCodeToRevert+"' ";
						strQry+=" where applicationid = '"+strAppID+"' ";
						strQry+=" and enterpriseid = '"+strEnterpriseID+"'";
						strQry+=" and consignment_no = '"+ConsignmentNo + "'";
					
						dbCommandApp = dbConApp.CreateCommand(strQry,CommandType.Text);
						dbCommandApp.Connection = conApp;
						dbCommandApp.Transaction = transactionApp;
						iRows = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);
						Logger.LogTraceInfo("ShipmentTrackingMgrDAL","UpdateShipmentStatus","ERR0012",iRows + " rows updated into Shipment table");							
					}
						
				}
				catch(Exception exp)
				{
					Logger.LogTraceInfo("ShipmentTrackingMrgDAL","RevertStatus","SDM002",exp.Message);
					throw new ApplicationException("Unable to Revert to earlier date.",null);
				}

				//commit the transaction
				transactionApp.Commit();
				Logger.LogTraceInfo("ShipmentTrackingMgrDAL","UpdateShipmentStatus","ERR0014","App db Transaction committed.");
			}

			catch(ApplicationException appException)
			{				
				try
				{
					transactionApp.Rollback();
					Logger.LogTraceInfo("ShipmentTrackingMgrDAL","UpdateShipmentStatus","ERR0006","App db delete transaction rolled back.");
				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("ShipmentTrackingMgrDAL","UpdateShipmentStatus","ERR0007","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}

				Logger.LogTraceError("ShipmentTrackingMgrDAL","UpdateShipmentStatus","ERR0008","Transaction Failed : "+ appException.Message.ToString());
				throw new ApplicationException("Error  "+appException.Message,appException);
			}
			catch(Exception exception)
			{
				try
				{
					transactionApp.Rollback();
					Logger.LogTraceInfo("ShipmentTrackingMgrDAL","UpdateShipmentStatus","ERR0009","App db delete transaction rolled back.");
				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("ShipmentTrackingMgrDAL","UpdateShipmentStatus","ERR0010","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}

				Logger.LogTraceError("ShipmentTrackingMgrDAL","UpdateShipmentStatus","ERR0011","Transaction Failed : "+ exception.Message.ToString());
				throw new ApplicationException("Error "+exception.Message,exception);
			}
			finally
			{
				if(conApp != null)
				{
					conApp.Close();
				}
			}
			
			return iRows;

		}
		
		//End Added By Tom 16/12/2009

		//Added by GwanG 
		public static int UpdateShipmentStatus(String strAppID,String strEnterpriseID, String strBookNum, DateTime dtLastDate, DateTime dtTrackDate, Boolean bDelete, String strDeleteRemark, String strStatusCode, String strPODStatusCodes,String strActDelvryDate, String ConsignmentNo, String strUser)
		{
			int iRows = 0;		
			DbConnection dbConApp = null;
			IDbCommand dbCommandApp = null;
			IDbTransaction transactionApp = null;
			IDbConnection conApp = null;
			String strExceptionMBGStatus=null;
			
			
			//Get App Database Connection
			dbConApp = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbConApp == null)
			{
				Logger.LogTraceError("ShipmentTrackingMgrDAL","UpdateShipmentStatus","ERR0001","dbConApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}			
			
			conApp = dbConApp.GetConnection();			
			if(conApp == null)
			{
				Logger.LogTraceError("ShipmentTrackingMgrDAL","UpdateShipmentStatus","ERR0002","conApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			try
			{
				dbConApp.OpenConnection(ref conApp);
			}
			catch(ApplicationException appException)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("ShipmentTrackingMgrDAL","UpdateShipmentStatus","ERR0003","Error opening connection : "+ appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ",appException);
			}
			catch(Exception exception)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("ShipmentTrackingMgrDAL","UpdateShipmentStatus","ERR0004","Error opening connection : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}
			
			String strQry = "";
			//String strActDelvryDate=null;

			//Begin App Transaction
			transactionApp = dbConApp.BeginTransaction(conApp,IsolationLevel.ReadCommitted);
			if(transactionApp == null)
			{
				Logger.LogTraceError("ShipmentTrackingMgrDAL","UpdateShipmentStatus","ERR0005","transactionApp is null");
				throw new ApplicationException("Failed Begining a App Transaction..",null);
			}
			
			//Comment By GwanG on 27Mar09 
			//String strTrackDate=Utility.DateFormat(strAppID, strEnterpriseID, dtTrackDate, DTFormat.DateTime);
			//String strLastDate=Utility.DateFormat(strAppID, strEnterpriseID, dtLastDate, DTFormat.DateTime);
			
			String strTrackDate=Utility.DateFormat(strAppID, strEnterpriseID, dtTrackDate, DTFormat.DateVeryLongTime);
			String strLastDate=Utility.DateFormat(strAppID, strEnterpriseID, dtLastDate, DTFormat.DateVeryLongTime);

			try
			{
				//if(dtTrackDate >= dtLastDate)
				if(dtTrackDate >= dtLastDate)
				{	
					strQry = "select tracking_datetime, status_code, exception_code ";
					strQry += "from shipment_tracking with(nolock) ";
					strQry += "where (enterpriseid = '"+strEnterpriseID+"') ";
					strQry += "and (applicationid = '"+strAppID+"') ";
					strQry += "and (booking_no = '"+strBookNum+"') ";
					strQry += "and (consignment_no = '"+ConsignmentNo+"') ";
					strQry += "and (deleted NOT IN ('y','Y') OR deleted IS NULL) ";
					strQry += "AND (tracking_datetime = (SELECT MAX(tracking_datetime) ";
					strQry += "FROM shipment_tracking with(nolock) ";
					strQry += "WHERE (enterpriseid = '"+strEnterpriseID+"') ";
					strQry += "and (applicationid = '"+strAppID+"') ";
					strQry += "and (booking_no = '"+strBookNum+"') ";
					strQry += "and (consignment_no = '"+ConsignmentNo+"') ";
					strQry += "and (tracking_datetime <"+strLastDate+") ";
					strQry += "AND (deleted NOT IN ('y','Y') OR deleted IS NULL)))";
			
					SessionDS sessionds;
					try
					{
						sessionds = dbConApp.ExecuteQuery(strQry,0,5,"revertEalierStatus");
						if(sessionds.ds.Tables[0].Rows.Count>0)
						{
							DataRow dr = sessionds.ds.Tables[0].Rows[0];
							String strStatusCodeToRevert = dr["status_code"].ToString();
							String strExceptCodeToRevert = dr["exception_code"].ToString();
							DateTime dtDateToRevert = (DateTime)dr["tracking_datetime"];
							//String strDateToRevert = Utility.DateFormat(strAppID, strEnterpriseID, dtTrackDate, DTFormat.DateTime);				
							String strDateToRevert = Utility.DateFormat(strAppID, strEnterpriseID, dtDateToRevert, DTFormat.DateTime);				

							strQry= " Update Shipment with(rowlock) set last_exception_code = '"+strExceptCodeToRevert+"'";
							strQry+=" ,last_status_datetime = "+strDateToRevert+",";
							strQry+=" last_status_code = '"+strStatusCodeToRevert+"' ";
							strQry+=" where applicationid = '"+strAppID+"' ";
							strQry+=" and enterpriseid = '"+strEnterpriseID+"'";
							strQry+=" and booking_no = "+strBookNum;
							strQry+=" and consignment_no = '"+ConsignmentNo + "'";
					
							dbCommandApp = dbConApp.CreateCommand(strQry,CommandType.Text);
							dbCommandApp.Connection = conApp;
							dbCommandApp.Transaction = transactionApp;
							iRows = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);
							Logger.LogTraceInfo("ShipmentTrackingMgrDAL","UpdateShipmentStatus","ERR0012",iRows + " rows updated into Shipment table");							
						}
						
					}
					catch(Exception exp)
					{
						Logger.LogTraceInfo("ShipmentTrackingMrgDAL","RevertStatus","SDM002",exp.Message);
						throw new ApplicationException("Unable to Revert to earlier date.",null);
					}
					
				}

				//HC Return Task
				StringBuilder strBuilderTMP = new StringBuilder();
				if (strStatusCode == "HCR")
				{
					DataSet tmpShip = new DataSet();
					DataSet tmpHasPOD = new DataSet();

					DateTime EstPODDt = new DateTime();
					DateTime ActPODDt = new DateTime();
					DateTime EstHCDt = new DateTime();

					//check is contained POD status
					strBuilderTMP = new StringBuilder();
					strBuilderTMP.Append("SELECT * ");
					strBuilderTMP.Append("FROM shipment_tracking with(nolock) ");
					strBuilderTMP.Append("WHERE applicationid = '"+strAppID+"' ");
					strBuilderTMP.Append("AND enterpriseid = '"+strEnterpriseID+"' ");
					strBuilderTMP.Append("AND consignment_no = '"+ConsignmentNo+"' ");
					strBuilderTMP.Append("AND booking_no ="+ strBookNum + " ");
					strBuilderTMP.Append("AND (ltrim(rtrim(status_code)) = 'POD' and upper(rtrim(ltrim(isnull(deleted, 'N')))) <> 'Y')");

					dbCommandApp = dbConApp.CreateCommand(strBuilderTMP.ToString(),CommandType.Text);
					dbCommandApp.Connection = conApp;
					dbCommandApp.Transaction = transactionApp;
					tmpHasPOD = (DataSet)dbConApp.ExecuteQueryInTransaction(dbCommandApp, ReturnType.DataSetType);

					//get shipment data
					strBuilderTMP = new StringBuilder();
					strBuilderTMP.Append("SELECT * ");
					strBuilderTMP.Append("FROM shipment with(nolock) ");
					strBuilderTMP.Append("WHERE applicationid = '"+strAppID+"' ");
					strBuilderTMP.Append("AND enterpriseid = '"+strEnterpriseID+"' ");
					strBuilderTMP.Append("AND consignment_no = '"+ConsignmentNo+"' ");
					strBuilderTMP.Append("AND booking_no ="+ strBookNum + " ");

					dbCommandApp = dbConApp.CreateCommand(strBuilderTMP.ToString(),CommandType.Text);
					dbCommandApp.Connection = conApp;
					dbCommandApp.Transaction = transactionApp;
					tmpShip = (DataSet)dbConApp.ExecuteQueryInTransaction(dbCommandApp, ReturnType.DataSetType);

					if (tmpHasPOD.Tables[0].Rows.Count > 0)
					{
						// if shipment contain POD Status
						ActPODDt = (DateTime)tmpHasPOD.Tables[0].Rows[0]["tracking_datetime"];
						EstHCDt = (DateTime)DomesticShipmentMgrDAL.calEstHCDateTime(ActPODDt, tmpShip.Tables[0].Rows[0]["destination_state_code"].ToString(),
							ConsignmentNo.Trim(), strBookNum.Trim(), strAppID, strEnterpriseID);

						strBuilderTMP = new StringBuilder();
						strBuilderTMP.Append("UPDATE shipment with(rowlock) ");
						strBuilderTMP.Append("SET est_hc_return_datetime ="+Utility.DateFormat(strAppID,strEnterpriseID,EstHCDt,DTFormat.DateTime)+", "); 
						strBuilderTMP.Append("act_hc_return_datetime = null ");
						strBuilderTMP.Append("WHERE applicationid ='"+strAppID+"' ");
						strBuilderTMP.Append("AND enterpriseid = '"+strEnterpriseID+"' "); 
						strBuilderTMP.Append("AND booking_no ="+strBookNum + " ");
						strBuilderTMP.Append("AND consignment_no = '"+ConsignmentNo + "'");

						dbCommandApp = dbConApp.CreateCommand(strBuilderTMP.ToString(),CommandType.Text);
						dbCommandApp.Connection = conApp;
						dbCommandApp.Transaction = transactionApp;
						iRows = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);	
					}
					else
					{
						// if shipment doesn't contain POD Status
						EstPODDt = (DateTime)tmpShip.Tables[0].Rows[0]["est_delivery_datetime"];
						EstHCDt = (DateTime)DomesticShipmentMgrDAL.calEstHCDateTime(EstPODDt, tmpShip.Tables[0].Rows[0]["destination_state_code"].ToString(),
							ConsignmentNo.Trim(), strBookNum.Trim(), strAppID, strEnterpriseID,dbConApp,dbCommandApp);

						strBuilderTMP = new StringBuilder();
						strBuilderTMP.Append("UPDATE shipment with(rowlock) ");
						strBuilderTMP.Append("SET est_hc_return_datetime ="+Utility.DateFormat(strAppID,strEnterpriseID,EstHCDt,DTFormat.DateTime)+", "); 
						strBuilderTMP.Append("act_hc_return_datetime = null ");
						strBuilderTMP.Append("WHERE applicationid ='"+strAppID+"' ");
						strBuilderTMP.Append("AND enterpriseid = '"+strEnterpriseID+"' "); 
						strBuilderTMP.Append("AND booking_no ="+strBookNum + " ");
						strBuilderTMP.Append("AND consignment_no = '"+ConsignmentNo + "'");

						dbCommandApp = dbConApp.CreateCommand(strBuilderTMP.ToString(),CommandType.Text);
						dbCommandApp.Connection = conApp;
						dbCommandApp.Transaction = transactionApp;
						iRows = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);	
						
					}
				}
				//HC Return Task

				String strDelete = null;
				if(bDelete)
				{
					strDelete = "Y";
				}
				else if(!bDelete)
				{
					strDelete = "N";
				}
				 
				strQry= " Update shipment_tracking with(rowlock) set deleted = '"+strDelete+"'";
				strQry+=" , delete_remark = N'"+strDeleteRemark +"' ,";
				//Gwang on 15Feb08
				strQry+=" last_userid = '"+strUser+"' ,last_updated = ";
				strQry+= Utility.DateFormat(strAppID,strEnterpriseID,DateTime.Now,DTFormat.DateTime);
				strQry+=" where applicationid = '"+strAppID+"' ";
				strQry+=" and enterpriseid = '"+strEnterpriseID+"'";				
				strQry+=" and booking_no = "+strBookNum+ " ";
				strQry+=" and tracking_datetime = "+strTrackDate;
				strQry+=" and consignment_no = '"+ConsignmentNo + "' ";
				strQry+=" and status_code = '"+ strStatusCode  +"'";

				dbCommandApp = dbConApp.CreateCommand(strQry,CommandType.Text);
				dbCommandApp.Connection = conApp;
				dbCommandApp.Transaction = transactionApp;
				iRows = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);
				
				Logger.LogTraceInfo("ShipmentTrackingMgrDAL","UpdateShipmentStatus","ERR0012",iRows + " rows updated into shipment_tracking table");								

		
				if (strActDelvryDate.Trim()=="")
				{
					strActDelvryDate="null";
				}
				char comma=',';
				String[] strPODs=strPODStatusCodes.Split(comma);
				for (int i=0;i< strPODs.Length;i++)
				{
					if(strStatusCode==strPODs[i].ToString())
					{
						strQry= " Update shipment with(rowlock) set Act_Delivery_Date ="+strActDelvryDate+ " ";
						strQry+=" where applicationid ='"+strAppID+"'";
						strQry+=" and enterpriseid = '"+strEnterpriseID+"' ";
						strQry+=" and booking_no ="+strBookNum;
						strQry+=" and consignment_no = '"+ConsignmentNo + "'";
						dbCommandApp = dbConApp.CreateCommand(strQry,CommandType.Text);
						dbCommandApp.Connection = conApp;
						dbCommandApp.Transaction = transactionApp;
						iRows = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);
						Logger.LogTraceInfo("ShipmentTrackingMgrDAL","UpdateShipmentStatus","ERR0012",iRows + " rows updated into shipment_tracking table");							
					}
				}
				//finding MBG			
				if (strBookNum !=null)
				{
					strExceptionMBGStatus=getMBGStatus(strAppID,strEnterpriseID,Int32.Parse(strBookNum),dtTrackDate, ConsignmentNo,dbConApp,dbCommandApp);
				}
				// ends finding MBG
				//updating mbg in shipment
				if(strExceptionMBGStatus !=null)
				{
					strQry= " Update shipment with(rowlock) set mbg ='"+strExceptionMBGStatus+"' ";
					strQry+=" where applicationid ='"+strAppID+"'";
					strQry+=" and enterpriseid = '"+strEnterpriseID+"' ";
					strQry+=" and booking_no ="+strBookNum;
					strQry+=" and consignment_no = '"+ConsignmentNo + "'";
					dbCommandApp = dbConApp.CreateCommand(strQry,CommandType.Text);
					dbCommandApp.Connection = conApp;
					dbCommandApp.Transaction = transactionApp;
					iRows = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);
					Logger.LogTraceInfo("ShipmentTrackingMgrDAL","UpdateShipmentStatus","ERR0013",iRows + " rows updated into shipment_tracking table");							
				}


				//commit the transaction
				transactionApp.Commit();
				Logger.LogTraceInfo("ShipmentTrackingMgrDAL","UpdateShipmentStatus","ERR0014","App db Transaction committed.");
			}

			catch(ApplicationException appException)
			{				
				try
				{
					transactionApp.Rollback();
					Logger.LogTraceInfo("ShipmentTrackingMgrDAL","UpdateShipmentStatus","ERR0006","App db delete transaction rolled back.");
				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("ShipmentTrackingMgrDAL","UpdateShipmentStatus","ERR0007","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}

				Logger.LogTraceError("ShipmentTrackingMgrDAL","UpdateShipmentStatus","ERR0008","Transaction Failed : "+ appException.Message.ToString());
				throw new ApplicationException("Error  "+appException.Message,appException);
			}
			catch(Exception exception)
			{
				try
				{
					transactionApp.Rollback();
					Logger.LogTraceInfo("ShipmentTrackingMgrDAL","UpdateShipmentStatus","ERR0009","App db delete transaction rolled back.");
				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("ShipmentTrackingMgrDAL","UpdateShipmentStatus","ERR0010","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}

				Logger.LogTraceError("ShipmentTrackingMgrDAL","UpdateShipmentStatus","ERR0011","Transaction Failed : "+ exception.Message.ToString());
				throw new ApplicationException("Error "+exception.Message,exception);
			}
			finally
			{
				if(conApp != null)
				{
					conApp.Close();
				}
			}
			
			return iRows;

		}
		public static int UpdateShipmentStatus(String strAppID, String strEnterpriseID, int intBkg_No,DateTime dtTrackDate, String consignment_no, String strStatusCode, String strRemark, String strUserID)
		{
			int iRows = 0;		
			DbConnection dbConApp = null;
			IDbCommand dbCommandApp = null;
			IDbTransaction transactionApp = null;
			IDbConnection conApp = null;
			
			//Get App Database Connection
			dbConApp = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbConApp == null)
			{
				Logger.LogTraceError("ShipmentTrackingMgrDAL","UpdateShipmentStatus","ERR0001","dbConApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}			
			
			conApp = dbConApp.GetConnection();			
			if(conApp == null)
			{
				Logger.LogTraceError("ShipmentTrackingMgrDAL","UpdateShipmentStatus","ERR0002","conApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			try
			{
				dbConApp.OpenConnection(ref conApp);
			}
			catch(ApplicationException appException)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("ShipmentTrackingMgrDAL","UpdateShipmentStatus","ERR0003","Error opening connection : "+ appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ",appException);
			}
			catch(Exception exception)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("ShipmentTrackingMgrDAL","UpdateShipmentStatus","ERR0004","Error opening connection : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}
			
			String strQry = "";
			//String strActDelvryDate=null;

			//Begin App Transaction
			transactionApp = dbConApp.BeginTransaction(conApp,IsolationLevel.ReadCommitted);
			if(transactionApp == null)
			{
				Logger.LogTraceError("ShipmentTrackingMgrDAL","UpdateShipmentStatus","ERR0005","transactionApp is null");
				throw new ApplicationException("Failed Begining a App Transaction..",null);
			}
			try
			{
				if(strStatusCode == "CNC" || strStatusCode == "DNC")
				{
					String tmpStatusCode = null;
					if(strStatusCode == "CNC")
						tmpStatusCode = "CNA";
					else
						tmpStatusCode = "DNA";

					strQry = "Update Shipment_Tracking with(rowlock) Set deleted='Y'";
					strQry+= " , last_userid='"+strUserID+"', last_updated='"+dtTrackDate+"'";
					strQry+= " Where applicationid='"+strAppID+"' ";
					strQry+= " and enterpriseid='"+strEnterpriseID+"' ";
					strQry+= " and booking_no="+intBkg_No+" ";
					strQry+= " and consignment_no='"+consignment_no+"'";
					strQry+= " and status_code='"+tmpStatusCode+"'";

					dbCommandApp = dbConApp.CreateCommand(strQry,CommandType.Text);
					dbCommandApp.Connection = conApp;
					dbCommandApp.Transaction = transactionApp;
					iRows = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);
					Logger.LogTraceInfo("ShipmentTrackingMgrDAL","UpdateShipmentStatus","ERR0013",iRows + " rows updated into shipment_tracking table");
				}
//				else
//				{
				strQry = "Insert into Shipment_Tracking with(rowlock)  (applicationid,enterpriseid";
				strQry+= " ,consignment_no,tracking_datetime,booking_no,";
				strQry+= " status_code,remark,last_userid,last_updated)";
				strQry+= " Values('"+strAppID+"','"+strEnterpriseID+"','"+consignment_no+"',";
				strQry+= " '"+dtTrackDate+"',"+intBkg_No+",'"+strStatusCode+"',";
				strQry+= " '"+strRemark+"','"+strUserID+"','"+DateTime.Now+"')";
//				}
				dbCommandApp = dbConApp.CreateCommand(strQry,CommandType.Text);
				dbCommandApp.Connection = conApp;
				dbCommandApp.Transaction = transactionApp;
				iRows = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);
				Logger.LogTraceInfo("ShipmentTrackingMgrDAL","UpdateShipmentStatus","ERR0013",iRows + " rows updated into shipment_tracking table");
			}
			catch(Exception ex)
			{
				try
				{
					transactionApp.Rollback();
					Logger.LogTraceInfo("ShipmentTrackingMgrDAL","UpdateShipmentStatus","ERR0006","App db delete transaction rolled back.");
				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("ShipmentTrackingMgrDAL","UpdateShipmentStatus","ERR0007","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}

				Logger.LogTraceError("ShipmentTrackingMgrDAL","UpdateShipmentStatus","ERR0008","Transaction Failed");
				throw new ApplicationException(ex.Message);
			}
			try
			{
				if(strStatusCode == "CNC" || strStatusCode == "DNC")
				{				
					strQry = "Update Shipment with(rowlock) Set last_status_code=";
					strQry+= " (Select Top 1 status_code from Shipment_tracking  with(nolock) ";
					strQry+= " Where applicationid='"+strAppID+"' and enterpriseid='"+strEnterpriseID+"'";
					strQry+= " and booking_no="+intBkg_No+" and consignment_no='"+consignment_no+"'";
					strQry+= " and (deleted<>'Y' or deleted is null) and status_code <> 'CNC' ";
					strQry+= " and status_code <> 'DNC' order by tracking_datetime Desc),";
					strQry+= " last_status_datetime=";
					strQry+= " (Select Top 1 last_updated from Shipment_tracking with(nolock)";
					strQry+= " Where applicationid='"+strAppID+"' and enterpriseid='"+strEnterpriseID+"'";
					strQry+= " and booking_no="+intBkg_No+" and consignment_no='"+consignment_no+"'";
					strQry+= " and (deleted<>'Y' or deleted is null) and status_code <> 'CNC' and status_code <> 'DNC' order by tracking_datetime Desc)";
					strQry+= " Where applicationid='"+strAppID+"' and enterpriseid='"+strEnterpriseID+"'";
					strQry+= " and booking_no="+intBkg_No+" and consignment_no='"+consignment_no+"'";
				}
				else
				{
					strQry = "Update Shipment with(rowlock) set last_status_code='"+strStatusCode+"'";
					strQry+= " ,last_status_datetime = '"+dtTrackDate+"'";
					strQry+= " Where applicationid='"+strAppID+"' ";
					strQry+= " and enterpriseid='"+strEnterpriseID+"' ";
					strQry+= " and booking_no="+intBkg_No+" ";
					strQry+= " and consignment_no='"+consignment_no+"'";
				}
				dbCommandApp = dbConApp.CreateCommand(strQry,CommandType.Text);
				dbCommandApp.Connection = conApp;
				dbCommandApp.Transaction = transactionApp;
				iRows = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);
				Logger.LogTraceInfo("ShipmentTrackingMgrDAL","UpdateShipmentStatus","ERR0013",iRows + " rows updated into shipment_tracking table");
				transactionApp.Commit();
			}
			catch(Exception ex)
			{
				try
				{
					transactionApp.Rollback();
					Logger.LogTraceInfo("ShipmentTrackingMgrDAL","UpdateShipmentStatus","ERR0006","App db delete transaction rolled back.");
				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("ShipmentTrackingMgrDAL","UpdateShipmentStatus","ERR0007","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}

				Logger.LogTraceError("ShipmentTrackingMgrDAL","UpdateShipmentStatus","ERR0008","Transaction Failed");
				throw new ApplicationException(ex.Message);
			}
			
			finally
			{
				if(conApp != null)
				{
					conApp.Close();
				}
			}
			return iRows;
		}

		public static void UpdateDeleteHistory(String strAppID, String strEnterpriseID, String strBookNo, String strExceptionToRevert, String strStatusToRevert, String strLastDateToRevert)
		{
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("ShipmentTrackingMgrDAL","UpdateRevertedDate","SDM002","dbConApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
		
			StringBuilder strBuilder = new StringBuilder();
			
			strBuilder.Append("Update shipment set last_exception_code = '"+strExceptionToRevert+"', ");
			strBuilder.Append("last_status_datetime = "+strLastDateToRevert+", last_status_code = '"+strStatusToRevert+"' where ");
			strBuilder.Append(" applicationid = '");
			strBuilder.Append(strAppID);
			strBuilder.Append("' and enterpriseid = '");
			strBuilder.Append(strEnterpriseID);
			strBuilder.Append("' and booking_no = ");
			strBuilder.Append(strBookNo);
			strBuilder.Append("");

			String strSQLQuery = strBuilder.ToString();
			try
			{
				IDbCommand idbCom = dbCon.CreateCommand(strSQLQuery, CommandType.Text);
				dbCon.ExecuteNonQuery(idbCom);
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceInfo("ShipmentTrackingMgrDAL","UpdateRevertedDate","SDM002","unable to update Reverted date.");
				throw new ApplicationException("Update reverted datetime fail",null);
			}
		}

		public static String GetMinTrackingDate(String strAppID, String strEnterpriseID, String strBookNo, String strTrackDate, String strPODStatusCodes,string strConsignmentNo)
		{
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("ShipmentTrackingMgrDAL","UpdateShipmentPODStatus","SDM002","dbConApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			String strSQLQuery=null;
			String strActDelvryDate="";
			
			try
			{
				strSQLQuery ="  Select MIN(Tracking_DateTime) as MinTrackingDate ";
				strSQLQuery +=" From Shipment_Tracking with(nolock) ";
				strSQLQuery +=" where Status_Code IN ("+strPODStatusCodes+") and ";
				strSQLQuery +=" applicationid='"+strAppID+"' ";
				strSQLQuery +=" and enterpriseid='"+strEnterpriseID+"' and ";
				strSQLQuery +=" Booking_No="+strBookNo+" ";
				// jiab 15/11/53
				strSQLQuery +=" and Consignment_No='"+strConsignmentNo+"' ";
				// jiab 15/11/53  end
				strSQLQuery +=" and (DELETED NOT IN ('y','Y') OR DELETED IS NULL) ";
				strSQLQuery +=" and Tracking_Datetime <> "+strTrackDate;
				
				IDbCommand idbCom = dbCon.CreateCommand(strSQLQuery, CommandType.Text);
				DataSet dsQuery=(DataSet) dbCon.ExecuteQuery(strSQLQuery,ReturnType.DataSetType);
				if (dsQuery.Tables[0].Rows.Count>0)
				{
					DataRow dr= dsQuery.Tables[0].Rows[0];
					if ((dr["MinTrackingDate"] != null) && (!dr["MinTrackingDate"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						DateTime dtActDelvryDate=(DateTime) dr["MinTrackingDate"];
						 strActDelvryDate = Utility.DateFormat(strAppID, strEnterpriseID, dtActDelvryDate, DTFormat.DateTime);
					}					
				}
			}
			catch(Exception exp)
			{
				Logger.LogTraceInfo("ShipmentTrackingMgrDAL","UpdateShipmentPODStatus","SDM002","unable to update Actual Delivery Datetime.");
				throw new ApplicationException("Update Actual Delivery Datetime failed",null);
			}
			
			return strActDelvryDate;
						
//			try
//			{	
//				strSQLQuery="  Update shipment set Act_Delivery_Date ="+strActDelvryDate+ " where applicationid ='";
//				strSQLQuery+=  strAppID+"'  and enterpriseid = '"+strEnterpriseID+"' and booking_no ="+strBookNo;
//				IDbCommand idbCom = dbCon.CreateCommand(strSQLQuery, CommandType.Text);
//				dbCon.ExecuteNonQuery(idbCom);
//			}
//			catch(ApplicationException appException)
//			{
//				Logger.LogTraceInfo("ShipmentTrackingMgrDAL","UpdateShipmentPODStatus","SDM002","unable to update Actual Delivery Datetime.");
//				throw new ApplicationException("Update Actual Delivery Datetime failed",null);
//			}
		}

		public static void UpdateDeleteHistory(String strAppID, String strEnterpriseID, String strBookNo, String strTrackDate, bool delete, String strDeleteRemark)
		{
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("ShipmentTrackingMgrDAL","UpdateDeleteHistory","SDM002","dbConApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			//DateTime dt = GetLatestDateFromShipment(dbCon, strAppID, strEnterpriseID, strBookNo);
			//String strLatestDateTime = Utility.DateFormat(strAppID, strEnterpriseID, dt,DTFormat.DateTime);
			//if(strTrackDate == strLatestDateTime)
			//{
			//	RevertStatus(dbCon,strAppID,strEnterpriseID,strBookNo,strLatestDateTime);
			//}

			StringBuilder strBuilder = new StringBuilder();
			String strDelete = null;
			if(delete)
			{
				strDelete = "y";
			}
			else if(!delete)
			{
				strDelete = "n";
			}
			strBuilder.Append("Update shipment_tracking set deleted = '"+strDelete+"', delete_remark = N'"+strDeleteRemark +"' where ");
			strBuilder.Append(" applicationid = '");
			strBuilder.Append(strAppID);
			strBuilder.Append("' and enterpriseid = '");
			strBuilder.Append(strEnterpriseID);
			strBuilder.Append("' and booking_no = ");
			strBuilder.Append(strBookNo);
			strBuilder.Append(" and tracking_datetime = ");
			strBuilder.Append(strTrackDate);

			strBuilder.Append("");

			String strSQLQuery = strBuilder.ToString();
			try
			{
				IDbCommand idbCom = dbCon.CreateCommand(strSQLQuery, CommandType.Text);
				dbCon.ExecuteNonQuery(idbCom);
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceInfo("ShipmentTrackingMgrDAL","UpdateDeleteHistory","SDM002","unable to update Reverted status and exception.");
				throw new ApplicationException("Update reverted status and exception fail",null);
			}
		}

		public static void RevertStatus(String strAppID, String strEnterpriseID, String strBookNo, String strLatestDateTime)
		{
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("ShipmentTrackingMgrDAL","UpdateRevertedDate","SDM002","dbConApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			String strQuery = "select tracking_datetime, status_code, exception_code from shipment_tracking where ";
			strQuery += "enterpriseid = '"+strEnterpriseID+"' and applicationid = '"+strAppID+"' and booking_no = '"+strBookNo+"' ";
			strQuery += "and (deleted NOT IN ('y') OR deleted <> 'Y' OR deleted IS NULL) AND tracking_datetime = (SELECT MAX(tracking_datetime) ";
			strQuery += "FROM shipment_tracking WHERE enterpriseid = '"+strEnterpriseID+"' and applicationid = '"+strAppID+"' and booking_no = '"+strBookNo+"' ";
			strQuery += "and (tracking_datetime < "+strLatestDateTime+"))";
			
			SessionDS sessionds;
			try
			{
				sessionds = dbCon.ExecuteQuery(strQuery,0,5,"revertEalierStatus");
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceInfo("ShipmentTrackingMrgDAL","RevertStatus","SDM002","unable to Reverted to earlier date.");
				throw new ApplicationException("unable to Reverted to earlier date.",null);
			}

			DataRow dr = sessionds.ds.Tables[0].Rows[0];
			String strStatusCodeToRevert = dr["status_code"].ToString();
			String strExceptCodeToRevert = dr["exception_code"].ToString();
			DateTime dtTrackDate = (DateTime)dr["tracking_datetime"];
			String strTrackDate = Utility.DateFormat(strAppID, strEnterpriseID, dtTrackDate, DTFormat.DateTime);
			UpdateDeleteHistory(strAppID,strEnterpriseID,strBookNo,strExceptCodeToRevert,strStatusCodeToRevert,strTrackDate);
		}

		public static DataSet GetLatestDateFromShipment(String strAppID, String strEnterpriseID, int iBookNo)
		{	
			DbConnection dbCon= null;
			IDbCommand dbcmd=null;
			DataSet dsDt = null;

			dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if (dbCon==null)
			{
				Logger.LogTraceError("ShipmentTrackingMgrDAL.cs","GetLatestDateFromShipment","ERR001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}


			StringBuilder strBuilder = new StringBuilder();
			strBuilder.Append("select last_status_datetime from shipment where applicationid = '"+strAppID+"' and enterpriseid = '"+strEnterpriseID+"' and booking_no ="+ iBookNo);
			
			String strSQLQuery = strBuilder.ToString();
			dbcmd = dbCon.CreateCommand(strSQLQuery,CommandType.Text);
 
			try
			{
				dsDt = (DataSet)dbCon.ExecuteQuery(dbcmd,com.common.DAL.ReturnType.DataSetType);  
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("ShipmentTrackingMgrDAL.cs","GetLatestDateFromShipment","ERR002","Error in Query String "+appException.Message);
				throw appException;
			}
			return dsDt;		
		}
		
		public static DataSet GetListServiceType(String strAppID, String strEnterpriseID)
		{	
			DbConnection dbCon= null;
			IDbCommand dbcmd=null;
			DataSet dsDt = null;

			dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if (dbCon==null)
			{
				Logger.LogTraceError("ShipmentTrackingMgrDAL.cs","GetListServiceType","ERR001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}


			StringBuilder strBuilder = new StringBuilder();
			strBuilder.Append("SELECT service_code FROM [Service] WHERE applicationid = 'TIES' AND enterpriseid = 'PNGAF' ORDER BY service_code");
			
			String strSQLQuery = strBuilder.ToString();
			dbcmd = dbCon.CreateCommand(strSQLQuery,CommandType.Text);
 
			try
			{
				dsDt = (DataSet)dbCon.ExecuteQuery(dbcmd,com.common.DAL.ReturnType.DataSetType);  
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("ShipmentTrackingMgrDAL.cs","GetListServiceType","ERR002","Error in Query String "+appException.Message);
				throw appException;
			}
			return dsDt;		
		}
		
		public static DataSet GetRouteCode(String strAppID, String strEnterpriseID)
		{	
			DbConnection dbCon= null;
			IDbCommand dbcmd=null;
			DataSet dsDt = null;

			dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if (dbCon==null)
			{
				Logger.LogTraceError("ShipmentTrackingMgrDAL.cs","GetListServiceType","ERR001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			StringBuilder strBuilder = new StringBuilder();
			strBuilder.Append(" SELECT ComboText FROM (SELECT '' AS ComboText UNION SELECT path_code AS ComboText FROM delivery_path WHERE applicationID='"+strAppID+"' and EnterpriseID='"+strEnterpriseID+"' and delivery_type in('S','W')) TMP ORDER BY ComboText ASC ");
			
			String strSQLQuery = strBuilder.ToString();
			dbcmd = dbCon.CreateCommand(strSQLQuery,CommandType.Text);
 
			try
			{
				dsDt = (DataSet)dbCon.ExecuteQuery(dbcmd,com.common.DAL.ReturnType.DataSetType);  
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("ShipmentTrackingMgrDAL.cs","GetRouteCode","ERR002","Error in Query String "+appException.Message);
				throw appException;
			}
			return dsDt;		
		}
		
		// to be deleted
		private static DateTime GetLatestDateFromShipment(DbConnection dbCon, String strAppID, String strEnterpriseID, String strBookNo)
		{	
			StringBuilder strBuilder = new StringBuilder();
			strBuilder.Append("select last_status_datetime from shipment where applicationid = '"+strAppID+"' and enterpriseid = '"+strEnterpriseID+"' and booking_no = '"+strBookNo+"' ");
			//strBuilder.Append("and deleted not in ('y','Y') ");
			String strSQLQuery = strBuilder.ToString();
			IDbCommand idbCom = dbCon.CreateCommand(strSQLQuery, CommandType.Text);
			SessionDS sessionds = dbCon.ExecuteQuery(idbCom, 0, 5, "shipment");

			DataRow dr = sessionds.ds.Tables[0].Rows[0];
			DateTime dt = new DateTime(1,1,1);
			if(dr["last_status_datetime"]!=System.DBNull.Value)
			{
				dt = (DateTime)dr["last_status_datetime"];
				return dt;
			}
			
			return dt;
		}

		public static PODDetails GetEarliestPodDetails(String strAppID, String strEnterpriseID, String strConsgNo, int intBookNo)
		{
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("ShipmentTrackingMgrDAL","GetEarliestPodDetails","SDM002","dbConApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			PODDetails PODDetails = new PODDetails();
			StringBuilder strBuilder = new StringBuilder();
			String strSQLQuery=null, strPODStatusCodes=null, strPODs=null;
			String[] strPODStatusCode =TIESUtility.getPODStatus(strAppID, strEnterpriseID);

			for(int i=0;i<strPODStatusCode.Length;i++)
			{
				if (i==0 || i==(strPODStatusCode.Length -1))
				{
					strPODStatusCodes+="'"+strPODStatusCode[i]+"'";
					strPODs+=strPODStatusCode[i];
				}
				else
				{
					strPODStatusCodes+="'"+strPODStatusCode[i]+"',";
					strPODs+=strPODStatusCode[i]+",";
				}
			}

			try
			{
				strBuilder.Append("select tracking_datetime, exception_code, person_incharge, remark from shipment_tracking where applicationid = '");
				strBuilder.Append(strAppID);
				strBuilder.Append("' and enterpriseid = '"); 
				strBuilder.Append(strEnterpriseID);
				strBuilder.Append("' and consignment_no = '");
				strBuilder.Append(strConsgNo);
				strBuilder.Append("' and booking_no = ");
				strBuilder.Append(intBookNo);
				strBuilder.Append(" and tracking_datetime = (select min(tracking_datetime) from shipment_tracking where applicationid = '");
					strBuilder.Append(strAppID);
					strBuilder.Append("' and enterpriseid = '"); 
					strBuilder.Append(strEnterpriseID);
					strBuilder.Append("' and consignment_no = '");
					strBuilder.Append(strConsgNo);
					strBuilder.Append("' and booking_no = ");
					strBuilder.Append(intBookNo);
					strBuilder.Append(" and status_code in (");
					strBuilder.Append(strPODStatusCodes);
					strBuilder.Append(") and (deleted not in ('y','Y') or deleted is null)) ");
				strBuilder.Append(" and status_code in (");
				strBuilder.Append(strPODStatusCodes);
				strBuilder.Append(") and (deleted not in ('y','Y') or deleted is null) ");
				
				strSQLQuery = strBuilder.ToString();
				IDbCommand idbCom = dbCon.CreateCommand(strSQLQuery, CommandType.Text);
				DataSet dsQuery=(DataSet) dbCon.ExecuteQuery(strSQLQuery,ReturnType.DataSetType);
				if (dsQuery.Tables[0].Rows.Count>0)
				{
					DataRow dr= dsQuery.Tables[0].Rows[0];

					if (Utility.IsNotDBNull(dr["tracking_datetime"]))
					{
						DateTime dtPODDate = (DateTime) dr["tracking_datetime"];
						PODDetails.PODDateTime = Utility.DateFormat(strAppID, strEnterpriseID, dtPODDate, DTFormat.DateTime);
					}					
					if(dr["exception_code"].ToString() != "")
					{
						PODDetails.PODException = dr["exception_code"].ToString();
					}
					if(dr["person_incharge"].ToString() != "")
					{
						PODDetails.PersonIncharge = dr["person_incharge"].ToString();
					}
					if(dr["remark"].ToString() != "")
					{
						PODDetails.PODRemark = dr["remark"].ToString();
					}
				}
			}
			catch(Exception exp)
			{
				Logger.LogTraceInfo("ShipmentTrackingMgrDAL","GetEarliestPodDetails","SDM002","unable to update Actual Delivery Datetime.");
				throw new ApplicationException("Failed retrieving Earliest POD details",null);
			}
			
			return PODDetails;
						
		}
		public static string getMBGStatus(String strAppID,String strEnterpriseID,int intBkg_No,DateTime dtTrackDate, String consignment_no)
		{
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("ShipmentTrackingMgrDAL","getMBGStatus","SDM002","dbConApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			return getMBGStatus( strAppID, strEnterpriseID, intBkg_No, dtTrackDate,  consignment_no, dbCon);
		}
		public static string getMBGStatus(String strAppID,String strEnterpriseID,int intBkg_No,DateTime dtTrackDate, String consignment_no,DbConnection dbCon)
		{
			IDbConnection conApp = dbCon.GetConnection();
			if(conApp == null)
			{
				Logger.LogTraceError("ShipmentTrackingMgrDAL","getMBGStatus","ERR003","conApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			try
			{
				dbCon.OpenConnection(ref conApp);
			}
			catch(ApplicationException appException)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("ShipmentTrackingMgrDAL","getMBGStatus","ERR004","Error opening connection : "+ appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ",appException);
			}
			catch(Exception exception)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("ShipmentTrackingMgrDAL","getMBGStatus","ERR005","Error opening connection : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}
			
			IDbCommand dbCmd = null;		
			dbCmd = dbCon.CreateCommand("",CommandType.Text);
			dbCmd.Connection = conApp;	
			//return getMBGStatus( strAppID, strEnterpriseID, intBkg_No, dtTrackDate,  consignment_no, dbCon, dbCmd);
			string result ;	
			try
			{
				result = getMBGStatus( strAppID, strEnterpriseID, intBkg_No, dtTrackDate,  consignment_no, dbCon, dbCmd);
			}
			catch(ApplicationException appExpection)
			{
				throw appExpection;
			}
			finally
			{
				if(conApp != null)
				{
					conApp.Close();
				}
			}
			return result;
		}
		public static string getMBGStatus(String strAppID,String strEnterpriseID,int intBkg_No,DateTime dtTrackDate, String consignment_no,DbConnection dbCon,IDbCommand dbCmd)
		{
			DataSet dsExcepStatus = null;
			DataSet dsExcepStatusCA = null;			
			String strExceptionMBGStatus=null;			
			try
			{
				
				
				//string strQ="SELECT mbg FROM Exception_Code WHERE (exception_code =(SELECT  exception_code  FROM  Shipment_Tracking  WHERE  (booking_no ="+intBkg_No+") AND (tracking_datetime ="+Utility.DateFormat(strAppID,strEnterpriseID,dtTrackDate,DTFormat.DateTime)+") AND (enterpriseid ='"+strEnterpriseID+"') AND (applicationid ='"+strAppID+"'))) AND (enterpriseid ='"+strEnterpriseID+"') AND (applicationid ='"+strAppID+"')";
				string strQ="SELECT mbg  FROM  Shipment  with(nolock) ";
				strQ += " WHERE  (mbg =(SELECT     mbg  FROM   Exception_Code  with(nolock)  WHERE  (exception_code =";
				//Added by GwanG 14Jul08
				strQ += " (SELECT distinct isnull(exception_code,'') FROM Shipment_Tracking  with(nolock)  ";
				strQ += " WHERE     (booking_no = "+intBkg_No+") AND (consignment_no = '"+consignment_no+"') ";
				strQ += " AND (tracking_datetime ="+Utility.DateFormat(strAppID,strEnterpriseID,dtTrackDate,DTFormat.DateTime)+") ";
				strQ += " AND (enterpriseid ='"+strEnterpriseID+"') AND (applicationid = 'TIES'))) AND (enterpriseid ='"+strEnterpriseID+"') ";
				strQ += " AND (applicationid = 'TIES'))) AND (booking_no = "+intBkg_No+") AND (consignment_no = '"+consignment_no+"')";
				//End Added
				//dbCmd = dbCon.CreateCommand(strQ,CommandType.Text);
				//dsExcepStatusCA =(DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
				dbCmd.CommandText = strQ;
				dsExcepStatusCA =(DataSet)dbCon.ExecuteQueryInTransaction(dbCmd,com.common.DAL.ReturnType.DataSetType);
				if (dsExcepStatusCA.Tables[0].Rows.Count > 0)
				{
					DataRow drExcepStatusCA = dsExcepStatusCA.Tables[0].Rows[0];
					if((drExcepStatusCA["mbg"]!= null) && (!drExcepStatusCA["mbg"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						strExceptionMBGStatus = (String)(drExcepStatusCA["mbg"]);
						if(strExceptionMBGStatus.ToUpper().Equals("Y"))
						{
							//strExceptionMBGStatus="N";
							strQ="SELECT mbg FROM Exception_Code  with(nolock)  ";
							strQ += " WHERE (exception_code IN ";
							strQ += " (SELECT DISTINCT exception_code FROM Shipment_Tracking  with(nolock)  ";
							strQ += " WHERE (applicationid ='"+strAppID+"') AND (enterpriseid ='"+strEnterpriseID+"') ";
							strQ += " AND (booking_no = "+intBkg_No+") AND (consignment_no = '"+consignment_no+"') AND (deleted NOT IN ('y', 'Y') OR deleted IS NULL) ";
							strQ += " AND (tracking_datetime <>"+Utility.DateFormat(strAppID,strEnterpriseID,dtTrackDate,DTFormat.DateTime)+"))) ";
							strQ += " AND (mbg IN ('y', 'Y'))";
							dbCmd = dbCon.CreateCommand(strQ,CommandType.Text);
							dsExcepStatus =(DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
							if (dsExcepStatus.Tables[0].Rows.Count > 0)
							{
								DataRow drExcepStatus = dsExcepStatus.Tables[0].Rows[0];
								if((drExcepStatus["mbg"]!= null) && (!drExcepStatus["mbg"].GetType().Equals(System.Type.GetType("System.DBNull"))))
								{
									strExceptionMBGStatus = (String)(drExcepStatus["mbg"]);
									if(strExceptionMBGStatus.ToUpper().Equals("Y"))
									{
										strExceptionMBGStatus=null;
									}
								}
							}
							else
							{
								strExceptionMBGStatus="N";
							}
						}						
					}
				}

							
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("ShipmentUpdateMgrDAL","getMBGStatus","ERR002","Error : "+ appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ",appException);
			}
			catch(Exception exception)
			{
				Logger.LogTraceError("ShipmentUpdateMgrDAL","getMBGStatus","ERR003","Error : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}
			return strExceptionMBGStatus;
		}
		public static bool chkPODStatusHasPODEX(string strAppID,string strEnterpriseID,string  strBookNum,string strConsignmentNo,string strStatusCode)
		{
			string PODId = "128";
			string PODEXId= "129";
			string strPODStatus;
			string strPODEXStatus;
			string strSQLQuery;
			bool result =false;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("ShipmentTrackingMgrDAL","chkPODStatusHasPODEX","SDM002","dbConApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			try
			{

			strSQLQuery ="select status_code from status_code ";
			strSQLQuery +=" where auto_process ='" +PODId +"'";

			IDbCommand idbCom = dbCon.CreateCommand(strSQLQuery, CommandType.Text);
			strPODStatus=(String) dbCon.ExecuteScalar(strSQLQuery);
			if(strStatusCode!=strPODStatus)
			{
				return result;
			}

			strSQLQuery ="select status_code from status_code ";
			strSQLQuery +=" where auto_process ='" +PODEXId +"'";

			idbCom = dbCon.CreateCommand(strSQLQuery, CommandType.Text);
			strPODEXStatus=(String) dbCon.ExecuteScalar(strSQLQuery);
			
			strSQLQuery =" Select * From Shipment_tracking ";
			strSQLQuery +=" Where applicationid='"+strAppID +"'";
			strSQLQuery +=" and enterpriseid ='"+strEnterpriseID +"'";
			strSQLQuery +=" and booking_no ='"+strBookNum +"'";
			strSQLQuery +=" and consignment_no ='"+ strConsignmentNo +"'";
			strSQLQuery +=" and status_code ='"+strPODStatus +"'";

			idbCom = dbCon.CreateCommand(strSQLQuery, CommandType.Text);
			DataSet ds=(DataSet) dbCon.ExecuteQuery(strSQLQuery,ReturnType.DataSetType);
			if(ds.Tables[0].Rows.Count>0)
			{
				strSQLQuery =" Select * From Shipment_tracking ";
				strSQLQuery +=" Where applicationid='"+strAppID +"'";
				strSQLQuery +=" and enterpriseid ='"+strEnterpriseID +"'";
				strSQLQuery +=" and booking_no ='"+strBookNum +"'";
				strSQLQuery +=" and consignment_no ='"+ strConsignmentNo +"'";
				strSQLQuery +=" and status_code ='"+strPODEXStatus +"'";
				strSQLQuery +=" and ISNULL(deleted,'') <>'Y'";

				idbCom = dbCon.CreateCommand(strSQLQuery, CommandType.Text);
				ds=(DataSet) dbCon.ExecuteQuery(strSQLQuery,ReturnType.DataSetType);
				if(ds.Tables[0].Rows.Count>0)
				{
					result=true;
				}

			}

			return result;
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("ShipmentUpdateMgrDAL","chkPODStatusHasPODEX","ERR002","Error : "+ appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ",appException);
			}
			catch(Exception exception)
			{
				Logger.LogTraceError("ShipmentUpdateMgrDAL","chkPODStatusHasPODEX","ERR003","Error : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}
		}


		public static int GetEnterpriseConfigurationsReturnPackages(string strAppID,string strEnterpriseID)
		{
			int returnPackage;
			string strQry = null;
			IDbCommand dbCmd = null;
			DataSet dsConfig = null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("EnterpriseConfigMgrDAL","QueryShipmentTracking","EDSM102","DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null",null);
			}
			strQry = "select * from dbo.EnterpriseConfigurations('PNGAF','TrackandTrace')";

			dbCmd = dbCon.CreateCommand(strQry,CommandType.Text);
			try
			{
				dsConfig = (DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
				returnPackage = Convert.ToInt32(dsConfig.Tables[0].Rows[0]["value"]);
			}
			catch(System.ApplicationException appException)
			{
				Logger.LogTraceError("QueryShipmentTrackingManager","GetConfig","","Error ");
				throw appException;
			}
			return returnPackage;
		}


		public static bool IsExistCustomer(string strAppID,string strEnterpriseID, string strCusID)
		{
			bool rData = false;
			string strQry = null;
			IDbCommand dbCmd = null;
			DataSet ds = null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("EnterpriseConfigMgrDAL","QueryShipmentTracking","EDSM102","DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null",null);
			}
			strQry = "select * from customer where applicationid='TIES' and enterpriseid = 'PNGAF' and custid = '" + strCusID + "' and status_active = 'Y'";

			dbCmd = dbCon.CreateCommand(strQry,CommandType.Text);
			try
			{
				ds = (DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
				
				if(strCusID.Trim() == "")
				{ 
					rData = true;
				}
				else
				{					
					if(ds.Tables[0].Rows.Count > 0)
					{
						rData = true;
					}
					else
					{
						rData = false;
					}
				}
			}
			catch(System.ApplicationException appException)
			{
				Logger.LogTraceError("QueryShipmentTrackingManager","IsExistCustomer","","Error ");
				throw appException;
			}
			return rData;
		}

		public static bool IsExistZipCode(string strAppID,string strEnterpriseID, string strZipcode)
		{
			bool rData = false;
			string strQry = null;
			IDbCommand dbCmd = null;
			DataSet ds = null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("EnterpriseConfigMgrDAL","QueryShipmentTracking","EDSM102","DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null",null);
			}
			strQry = "select * from Zipcode z, State s where z.applicationid='TIES' and z.enterpriseid = 'PNGAF' and z.applicationid = s.applicationid and z.enterpriseid = s.enterpriseid and z.country = s.country and z.state_code = s.state_code  and z.zipcode = '" + strZipcode + "' ";

			dbCmd = dbCon.CreateCommand(strQry,CommandType.Text);
			try
			{
				ds = (DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);

				if(strZipcode.Trim() == "")
				{ 
					rData = true;
				}
				else
				{
					if(ds.Tables[0].Rows.Count > 0)
					{
						rData = true;
					}
					else
					{
						rData = false;
					}
				}
				
			}
			catch(System.ApplicationException appException)
			{
				Logger.LogTraceError("QueryShipmentTrackingManager","IsExistZipCode","","Error ");
				throw appException;
			}
			return rData;
		}

		public static bool IsExistState(string strAppID,string strEnterpriseID, string strStateCode)
		{
			bool rData = false;
			string strQry = null;
			IDbCommand dbCmd = null;
			DataSet ds = null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("EnterpriseConfigMgrDAL","QueryShipmentTracking","EDSM102","DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null",null);
			}
			strQry = " select * from state where applicationid='TIES' and enterpriseid = 'PNGAF' and state_code = '" + strStateCode + "'";

			dbCmd = dbCon.CreateCommand(strQry,CommandType.Text);
			try
			{
				ds = (DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);

				if(strStateCode.Trim() == "")
				{ 
					rData = true;
				}
				else
				{
					if(ds.Tables[0].Rows.Count > 0)
					{
						rData = true;
					}
					else
					{
						rData = false;
					}
				}
				
			}
			catch(System.ApplicationException appException)
			{
				Logger.LogTraceError("QueryShipmentTrackingManager","IsExistState","","Error ");
				throw appException;
			}
			return rData;
		}

		public static bool IsExistStatus(string strAppID,string strEnterpriseID, string strStatusCode, string strUserid)
		{
			bool rData = false;
			string strQry = null;
			IDbCommand dbCmd = null;
			DataSet ds = null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("EnterpriseConfigMgrDAL","QueryShipmentTracking","EDSM102","DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null",null);
			}
			strQry = "select * from (select distinct status_code,status_description,Status_type,status_close,invoiceable,system_code,tt_display_status,tt_display_remarks,auto_process,auto_location,auto_remark,tt_display_location, scanning_type ,AllowRoles, isnull(AllowInSWB_Emu,'N') as AllowInSWB_Emu, ";
			strQry = strQry + " isnull(AllowUPD_ByBatch,'N') as AllowUPD_ByBatch from Status_Code  inner join  ( SELECT   distinct  Role_Master.role_name fROM  User_Role_Relation INNER JOIN Role_Master ON User_Role_Relation.applicationid = Role_Master.applicationid AND User_Role_Relation.enterpriseid = Role_Master.enterpriseid AND  User_Role_Relation.roleid = Role_Master.roleid ";
			strQry = strQry + " WHERE  (User_Role_Relation.userid = '" + strUserid + "')) a on   status_code.allowRoles like '%' + a.role_name + '%'  where status_code.applicationid = 'TIES' and status_code.enterpriseid = 'PNGAF'  union  select status_code,status_description,Status_type,status_close,invoiceable,  system_code,tt_display_status,tt_display_remarks,auto_process,auto_location,auto_remark, ";  
			strQry = strQry + "	tt_display_location, scanning_type ,AllowRoles, isnull(AllowInSWB_Emu,'N') as AllowInSWB_Emu,  isnull(AllowUPD_ByBatch,'N') as AllowUPD_ByBatch from Status_Code where (AllowRoles is null or Rtrim(Ltrim(AllowRoles))='') ) a where 1=1 and status_code = '" + strStatusCode + "'";

			dbCmd = dbCon.CreateCommand(strQry,CommandType.Text);
			try
			{
				ds = (DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);

				if(strStatusCode.Trim() == "")
				{ 
					rData = true;
				}
				else
				{
					if(ds.Tables[0].Rows.Count > 0)
					{
						rData = true;
					}
					else
					{
						rData = false;
					}
				}
			}
			catch(System.ApplicationException appException)
			{
				Logger.LogTraceError("QueryShipmentTrackingManager","IsExistStatus","","Error ");
				throw appException;
			}
			return rData;
		}

		public static bool IsExistException(string strAppID,string strEnterpriseID, string strExceptionCode)
		{
			bool rData = false;
			string strQry = null;
			IDbCommand dbCmd = null;
			DataSet ds = null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("EnterpriseConfigMgrDAL","QueryShipmentTracking","EDSM102","DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null",null);
			}
			strQry = " select * from Exception_Code e,status_code s where e.applicationid='TIES' and e.enterpriseid = 'PNGAF' and e.active_st = 'Y' and e.status_code like '" + strExceptionCode + "' and s.applicationid = e.applicationid and s.enterpriseid = e.enterpriseid and s.status_code = e.status_code";

			dbCmd = dbCon.CreateCommand(strQry,CommandType.Text);
			try
			{
				ds = (DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);

				if(strExceptionCode.Trim() == "")
				{ 
					rData = true;
				}
				else
				{
					if(ds.Tables[0].Rows.Count > 0)
					{
						rData = true;
					}
					else
					{
						rData = false;
					}
				}
			}
			catch(System.ApplicationException appException)
			{
				Logger.LogTraceError("QueryShipmentTrackingManager","IsExistException","","Error ");
				throw appException;
			}
			return rData;
		}

	}
}
