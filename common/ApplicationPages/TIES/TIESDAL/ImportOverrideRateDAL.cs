using System;
using System.Data;
using com.common.classes;
using com.common.DAL;
using com.common.util;
using System.Text;
using com.ties.classes;
using System.Collections;
using System.Text.RegularExpressions;

namespace com.ties.DAL
{
	/// <summary>
	/// Summary description for ImportOverrideRateDAL.
	/// </summary>
	public class ImportOverrideRateDAL
	{
		public ImportOverrideRateDAL()
		{
			//
			// TODO: Add constructor logic here
			//
		}


		private static bool IsNumeric(string str) 
		{
			try 
			{
				if (str.Trim() == "") 
				{
					return true;
				}
				else 
				{
					Convert.ToDecimal(str);
					return true;
				}
			}
			catch(FormatException) 
			{
				return false;
			}
		}


		public static DataSet GetEmptyImport()
		{
			DataTable dtImport =  new DataTable();
			
			dtImport.Columns.Add(new DataColumn("recordid",typeof(string)));
			dtImport.Columns.Add(new DataColumn("consignment_no",typeof(string)));
			dtImport.Columns.Add(new DataColumn("booking_no",typeof(int)));
			dtImport.Columns.Add(new DataColumn("isvalid_consignment_no",typeof(int)));
			dtImport.Columns.Add(new DataColumn("ref_no",typeof(string))); 
			dtImport.Columns.Add(new DataColumn("destination_station",typeof(string))); 
			dtImport.Columns.Add(new DataColumn("route_code",typeof(string))); 
			dtImport.Columns.Add(new DataColumn("payerid",typeof(string))); 
			dtImport.Columns.Add(new DataColumn("act_delivery_date",typeof(DateTime)));  

			dtImport.Columns.Add(new DataColumn("tot_freight_charge",typeof(decimal)));
			dtImport.Columns.Add(new DataColumn("insurance_surcharge",typeof(decimal)));
			dtImport.Columns.Add(new DataColumn("other_surch_amount",typeof(decimal)));
			dtImport.Columns.Add(new DataColumn("tot_vas_surcharge",typeof(decimal))); 
			dtImport.Columns.Add(new DataColumn("esa_surcharge",typeof(decimal)));
			dtImport.Columns.Add(new DataColumn("total_rated_amount",typeof(decimal)));

			dtImport.Columns.Add(new DataColumn("override_rated_freight",typeof(string))); 
			dtImport.Columns.Add(new DataColumn("isvalid_override_rated_freight",typeof(int))); 

			dtImport.Columns.Add(new DataColumn("override_rated_ins",typeof(string))); 
			dtImport.Columns.Add(new DataColumn("isvalid_override_rated_ins",typeof(int))); 

			dtImport.Columns.Add(new DataColumn("override_rated_other",typeof(string))); 
			dtImport.Columns.Add(new DataColumn("isvalid_override_rated_other",typeof(int)));

			dtImport.Columns.Add(new DataColumn("override_rated_esa",typeof(string)));
			dtImport.Columns.Add(new DataColumn("isvalid_override_rated_esa",typeof(int)));

			dtImport.Columns.Add(new DataColumn("override_rated_total",typeof(string)));

			dtImport.Columns.Add(new DataColumn("excluded_reason",typeof(string)));

			DataRow drEach = dtImport.NewRow();

			dtImport.Rows.Add(drEach);
			DataSet dsImport = new DataSet();
			dsImport.Tables.Add(dtImport);
			return  dsImport;	
		}


		public static DataSet GetEmptyValidImport()
		{
			DataTable dtImport =  new DataTable();
			
			dtImport.Columns.Add(new DataColumn("recordid",typeof(string)));
			dtImport.Columns.Add(new DataColumn("consignment_no",typeof(string)));
			dtImport.Columns.Add(new DataColumn("isvalid_consignment_no",typeof(int)));
			dtImport.Columns.Add(new DataColumn("ref_no",typeof(string))); 
			dtImport.Columns.Add(new DataColumn("destination_station",typeof(string))); 
			dtImport.Columns.Add(new DataColumn("route_code",typeof(string))); 
			dtImport.Columns.Add(new DataColumn("payerid",typeof(string))); 
			dtImport.Columns.Add(new DataColumn("act_delivery_date",typeof(DateTime)));  

			dtImport.Columns.Add(new DataColumn("tot_freight_charge",typeof(decimal)));
			dtImport.Columns.Add(new DataColumn("insurance_surcharge",typeof(decimal)));
			dtImport.Columns.Add(new DataColumn("other_surch_amount",typeof(decimal)));
			dtImport.Columns.Add(new DataColumn("tot_vas_surcharge",typeof(decimal))); 
			dtImport.Columns.Add(new DataColumn("esa_surcharge",typeof(decimal)));
			dtImport.Columns.Add(new DataColumn("total_rated_amount",typeof(decimal)));

			dtImport.Columns.Add(new DataColumn("override_rated_freight",typeof(decimal))); 
			dtImport.Columns.Add(new DataColumn("isvalid_override_rated_freight",typeof(int))); 

			dtImport.Columns.Add(new DataColumn("override_rated_ins",typeof(decimal))); 
			dtImport.Columns.Add(new DataColumn("isvalid_override_rated_ins",typeof(int))); 

			dtImport.Columns.Add(new DataColumn("override_rated_other",typeof(decimal))); 
			dtImport.Columns.Add(new DataColumn("isvalid_override_rated_other",typeof(int)));

			dtImport.Columns.Add(new DataColumn("override_rated_esa",typeof(decimal)));
			dtImport.Columns.Add(new DataColumn("isvalid_override_rated_esa",typeof(int)));

			dtImport.Columns.Add(new DataColumn("override_rated_total",typeof(decimal)));

			dtImport.Columns.Add(new DataColumn("excluded_reason",typeof(string)));

			DataRow drEach = dtImport.NewRow();

			dtImport.Rows.Add(drEach);
			DataSet dsImport = new DataSet();
			dsImport.Tables.Add(dtImport);
			return  dsImport;	
		}


		public static DataSet GetEmptyInvalidImport()
		{
			DataTable dtImport =  new DataTable();
			
			dtImport.Columns.Add(new DataColumn("recordid",typeof(string)));
			dtImport.Columns.Add(new DataColumn("consignment_no",typeof(string)));
			dtImport.Columns.Add(new DataColumn("isvalid_consignment_no",typeof(int)));
			dtImport.Columns.Add(new DataColumn("ref_no",typeof(string))); 
			dtImport.Columns.Add(new DataColumn("destination_station",typeof(string))); 
			dtImport.Columns.Add(new DataColumn("route_code",typeof(string))); 
			dtImport.Columns.Add(new DataColumn("payerid",typeof(string))); 
			dtImport.Columns.Add(new DataColumn("act_delivery_date",typeof(string)));  

			dtImport.Columns.Add(new DataColumn("tot_freight_charge",typeof(string)));
			dtImport.Columns.Add(new DataColumn("insurance_surcharge",typeof(string)));
			dtImport.Columns.Add(new DataColumn("other_surch_amount",typeof(string)));
			dtImport.Columns.Add(new DataColumn("tot_vas_surcharge",typeof(string))); 
			dtImport.Columns.Add(new DataColumn("esa_surcharge",typeof(string)));
			dtImport.Columns.Add(new DataColumn("total_rated_amount",typeof(string)));

			dtImport.Columns.Add(new DataColumn("override_rated_freight",typeof(string))); 
			dtImport.Columns.Add(new DataColumn("isvalid_override_rated_freight",typeof(int))); 

			dtImport.Columns.Add(new DataColumn("override_rated_ins",typeof(string))); 
			dtImport.Columns.Add(new DataColumn("isvalid_override_rated_ins",typeof(int))); 

			dtImport.Columns.Add(new DataColumn("override_rated_other",typeof(string))); 
			dtImport.Columns.Add(new DataColumn("isvalid_override_rated_other",typeof(int)));

			dtImport.Columns.Add(new DataColumn("override_rated_esa",typeof(string)));
			dtImport.Columns.Add(new DataColumn("isvalid_override_rated_esa",typeof(int)));

			dtImport.Columns.Add(new DataColumn("override_rated_total",typeof(string)));

			dtImport.Columns.Add(new DataColumn("excluded_reason",typeof(string)));

			DataRow drEach = dtImport.NewRow();

			dtImport.Rows.Add(drEach);
			DataSet dsImport = new DataSet();
			dsImport.Tables.Add(dtImport);
			return  dsImport;	
		}


		public static int ClearTempDataByUserId(String strAppID, String strEnterpriseID, String userId)
		{
			IDbCommand dbCmd = null;
			int iRowsAffected = 0;
			StringBuilder strBuild = null;
			
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("ImportOverrideRateDAL", "ClearTempDataByUserId", "ClearTempDataByUserId", "DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null",null);
			}

			IDbConnection conApp = dbCon.GetConnection();
			if(conApp == null)
			{
				Logger.LogTraceError("ImportOverrideRateDAL", "ClearTempDataByUserId", "ClearTempDataByUserId", "conApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			try
			{
				dbCon.OpenConnection(ref conApp);
			}
			catch(ApplicationException appException)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("ImportOverrideRateDAL", "ClearTempDataByUserId", "ClearTempDataByUserId", "Error opening connection : "+ appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ",appException);
			}
			catch(Exception exception)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("ImportOverrideRateDAL", "ClearTempDataByUserId", "ClearTempDataByUserId", "Error opening connection : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}
			IDbTransaction transactionApp = dbCon.BeginTransaction(conApp,IsolationLevel.ReadCommitted);

			if(transactionApp == null)
			{
				Logger.LogTraceError("ImportOverrideRateDAL", "ClearTempDataByUserId", "ClearTempDataByUserId", "transactionApp is null");
				throw new ApplicationException("Failed Begining a App Transaction..",null);
			}
			try
			{

				//delete from the Consignment table
				strBuild = new StringBuilder();
				strBuild.Append("DELETE " );
				strBuild.Append("FROM ImportOverride ");
				strBuild.Append("WHERE userid = '" + userId + "' ");

				dbCmd = dbCon.CreateCommand(strBuild.ToString(), CommandType.Text);
				dbCmd.Connection = conApp;
				dbCmd.Transaction = transactionApp;
				iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);

				transactionApp.Commit();
			}
			catch(ApplicationException appException)
			{
				try
				{
					transactionApp.Rollback();
					Logger.LogDebugInfo("ImportOverrideRateDAL", "ClearTempDataByUserId", "ClearTempDataByUserId","App db insert transaction rolled back.");

				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("ImportOverrideRateDAL", "ClearTempDataByUserId", "ClearTempDataByUserId","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}


				throw new ApplicationException("Error deleting audit data ",appException);
			}
			catch(Exception exception)
			{
				try
				{
					transactionApp.Rollback();
					Logger.LogDebugInfo("ImportOverrideRateDAL", "ClearTempDataByUserId", "ClearTempDataByUserId","App db insert transaction rolled back.");

				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("ImportOverrideRateDAL", "ClearTempDataByUserId", "ClearTempDataByUserId","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}

				throw new ApplicationException("Error deleting audit data ",exception);
			}
			finally
			{
				if(conApp != null)
				{
					conApp.Close();
				}
			}
			return iRowsAffected;

		}


		public static String GetUserCulture(String strAppID, String strEnterpriseID, String userId)
		{
			SessionDS sessionDS = null;			
			StringBuilder strBuilder = new StringBuilder();

			strBuilder.Append("SELECT * ");
			strBuilder.Append("From User_Master ");
			strBuilder.Append("where applicationid = '");
			strBuilder.Append(strAppID+"'");
			strBuilder.Append(" and enterpriseid = '");
			strBuilder.Append(strEnterpriseID+"'");
			strBuilder.Append(" and userid = '");
			strBuilder.Append(userId+"'");

	
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("ImportOverrideRateDAL", "GetUserCulture", "GetUserCulture", "DbConnection object is null!!");
			}
			
			sessionDS = dbCon.ExecuteQuery(strBuilder.ToString(), 0, 0, "audit1001");
			if(sessionDS.ds.Tables[0].Rows.Count > 0)
				return sessionDS.ds.Tables[0].Rows[0]["user_culture"].ToString();
			else
				return "";
		}


		public static DataSet GetShipmentByConList(String strAppID, String strEnterpriseID, String conList) 
		{
			SessionDS sessionDS = null;			
			String strSQL = "";

			strSQL = "SELECT * FROM Shipment WHERE consignment_no in (" + conList + ") ";  
			
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("ImportOverrideRateDAL","GetShipmentDataByConSeq","GetShipmentDataByConSeq","DbConnection object is null!!");
				return sessionDS.ds;
			}
			
			sessionDS = dbCon.ExecuteQuery(strSQL, 0, 0, "Shipment");
			return  sessionDS.ds;
		}

		
		public static string[] ValidateImportData(DataRow drExcel, DataRow[] drDB)
		{
			string invalidStr = "";
			string invalidIndx = "";

			string consignment_no = "";
			string bookingExcel = "";
			string bookingDB ="";
			string[] arrOfInvalid = {"", ""};
			int OverrideVal = 0;

			consignment_no = drExcel["consignment_no"].ToString();			

			if (drDB.Length == 0)
			{
				invalidStr = "Specified consignment is not found in the system.";
				invalidIndx = "0";
			}
			else
			{				
					invalidIndx = invalidIndx + "1";
			}

			//override_rated_freight
			if(!IsNumeric(drExcel["override_rated_freight"].ToString()))
			{
				invalidStr = invalidStr + "Freight charge must be numeric.";
				invalidIndx = invalidIndx + "0";
			}
			else
			{
				OverrideVal =  drExcel["override_rated_freight"].ToString().Length;
				if (OverrideVal > 8)
				{
					invalidStr = invalidStr + "Freight charge was exceed  length.";
					invalidIndx = invalidIndx + "0";
				}
				else
				{
					invalidIndx = invalidIndx + "1";
				}
			}

			//override_rated_ins
			if(!IsNumeric(drExcel["override_rated_ins"].ToString()))
			{
				invalidStr = invalidStr + "Insurance surcharge must be numeric.";
				invalidIndx = invalidIndx + "0";
			}
			else
			{
				OverrideVal =  drExcel["override_rated_ins"].ToString().Length;
				if (OverrideVal > 8)
				{
					invalidStr = invalidStr + "Insurance surcharge was exceed  length.";
					invalidIndx = invalidIndx + "0";
				}
				else
				{
					invalidIndx = invalidIndx + "1";
				}
			}

			//override_rated_other
			if(!IsNumeric(drExcel["override_rated_other"].ToString()))
			{
				invalidStr = invalidStr + "Other surcharge must be numeric.";
				invalidIndx = invalidIndx + "0";
			}
			else
			{
				OverrideVal =  drExcel["override_rated_other"].ToString().Length;
				if (OverrideVal > 8)
				{
					invalidStr = invalidStr + "Other surcharge was exceed  length.";
					invalidIndx = invalidIndx + "0";
				}
				else
				{
					invalidIndx = invalidIndx + "1";
				}
			}

			//override_rated_esa
			if(!IsNumeric(drExcel["override_rated_esa"].ToString()))
			{
				invalidStr = invalidStr + "ESA surcharge must be numeric.";
				invalidIndx = invalidIndx + "0";
			}
			else
			{
				OverrideVal =  drExcel["override_rated_esa"].ToString().Length;
				if (OverrideVal > 8)
				{
					invalidStr = invalidStr + "ESA surcharge was exceed  length.";
					invalidIndx = invalidIndx + "0";
				}
				else
				{
					invalidIndx = invalidIndx + "1";
				}
			}

			arrOfInvalid[0] = invalidStr;
			arrOfInvalid[1] = invalidIndx;
		
			return arrOfInvalid;
		}


		public static void ImportToTempTables(String strAppID, String strEnterpriseID, DataSet dsImportData,  String userId)
		{
			int iRowsAffected = 0;
			IDbCommand dbCmdCons = null;
			
			#region Check DataSet Is Not NULL

			//Check import data
			if (dsImportData == null)
			{
				Logger.LogTraceError("ImportOverrideRateDAL", "ImportToTempTables", "ImportToTempTables", "DataSet is null!!");
				throw new ApplicationException("The Import DataSet is null",null);
			}

			#endregion

			#region Initiate DB Connection

			//Create DB Connection
			DbConnection dbConCons = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);
			if(dbConCons == null)
			{
				Logger.LogTraceError("ImportOverrideRateDAL", "ImportToTempTables", "ImportToTempTables", "DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null",null);
			}

			//Create DB Interface Connection
			IDbConnection conAppCons = dbConCons.GetConnection();
			if(conAppCons == null)
			{
				Logger.LogTraceError("ImportOverrideRateDAL", "ImportToTempTables", "ImportToTempTables", "conApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			try
			{
				dbConCons.OpenConnection(ref conAppCons);
			}
			catch(ApplicationException appException)
			{
				if(conAppCons != null)
				{
					conAppCons.Close();
				}
				Logger.LogTraceError("ImportOverrideRateDAL", "ImportToTempTables", "ImportToTempTables", "Error opening connection : "+ appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ",appException);
			}
			catch(Exception exception)
			{
				if(conAppCons != null)
				{
					conAppCons.Close();
				}
				Logger.LogTraceError("ImportOverrideRateDAL", "ImportToTempTables", "ImportToTempTables", "Error opening connection : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}

			#endregion

			#region Create  & Activate Transaction

			//Create DB Transaction
			IDbTransaction transactionAppCons = dbConCons.BeginTransaction(conAppCons, IsolationLevel.ReadCommitted);

			if(transactionAppCons == null)
			{
				Logger.LogTraceError("ImportOverrideRateDAL", "ImportToTempTables", "ImportToTempTables", "transactionApp is null");
				throw new ApplicationException("Failed Begining a App Transaction..",null);
			}

			#endregion

			int i = 0;

			StringBuilder strBuilder = new StringBuilder();

			foreach(DataRow drEach in dsImportData.Tables[0].Rows)
			{
				try
				{

					#region Execute the first transaction

					if (i == 0)
					{
						strBuilder = new StringBuilder();
						strBuilder.Append("select count(*) from Core_Enterprise");
						dbCmdCons = dbConCons.CreateCommand(strBuilder.ToString() , CommandType.Text);
						dbCmdCons.Connection = conAppCons;
						dbCmdCons.Transaction = transactionAppCons;
						iRowsAffected = dbConCons.ExecuteNonQueryInTransaction(dbCmdCons);
					}
					i++;

					#endregion
		
				
					#region Insert To Temporary Table

					strBuilder = new StringBuilder();
					strBuilder.Append("INSERT INTO ImportOverride([recordid], [userid], [consignment_no], [ref_no], ");
					strBuilder.Append("[destination_station], [payerid], [route_code], [act_delivery_date], ");
					strBuilder.Append("[tot_freight_charge], [insurance_surcharge], [other_surch_amount], [tot_vas_surcharge], [esa_surcharge], [total_rated_amount], ");
					strBuilder.Append("[override_rated_freight], [override_rated_ins], [override_rated_other], [override_rated_esa], [override_rated_total], ");
					strBuilder.Append("[excluded_reason], [recordType], [isvalid_consignment_no], [isvalid_override_rated_freight], [isvalid_override_rated_ins], ");
					strBuilder.Append("[isvalid_override_rated_other], [isvalid_override_rated_esa],[applicationid], [enterpriseid] )");
					strBuilder.Append("VALUES(");

					#region recordid

					if((drEach["recordid"]!= null) && (!drEach["recordid"].GetType().Equals(System.Type.GetType("System.DBNull"))) && (drEach["recordid"].ToString().Trim() != ""))
					{
						String recordid = Utility.ReplaceSingleQuote(drEach["recordid"].ToString());
						strBuilder.Append("'");
						strBuilder.Append(recordid);
						strBuilder.Append("'");
					}
					else
					{
						strBuilder.Append("null");
					}
					strBuilder.Append(",");

					#endregion

					#region userid

					if((userId != null) && (userId.Trim() != ""))
					{
						strBuilder.Append("'");
						strBuilder.Append(userId);
						strBuilder.Append("'");	
					}
					else
					{
						strBuilder.Append("null");
					}
					strBuilder.Append(",");

					#endregion

					#region consignment_no

					if((drEach["consignment_no"]!= null) && (!drEach["consignment_no"].GetType().Equals(System.Type.GetType("System.DBNull"))) && (drEach["consignment_no"].ToString().Trim() != ""))
					{
						String consignment_no = Utility.ReplaceSingleQuote(drEach["consignment_no"].ToString());
						strBuilder.Append("'");
						strBuilder.Append(consignment_no);
						strBuilder.Append("'");
					}
					else
					{
						strBuilder.Append("null");
					}
					strBuilder.Append(",");

					#endregion

					#region ref_no

					if((drEach["ref_no"]!= null) && (!drEach["ref_no"].GetType().Equals(System.Type.GetType("System.DBNull")))&& (drEach["ref_no"].ToString().Trim() != ""))
					{
						String ref_no = Utility.ReplaceSingleQuote(drEach["ref_no"].ToString());
						strBuilder.Append("'");
						strBuilder.Append(ref_no);
						strBuilder.Append("'");
					}
					else
					{
						strBuilder.Append("null");
					}
					strBuilder.Append(",");

					#endregion

					#region destination_station

					if((drEach["destination_station"]!= null) && (!drEach["destination_station"].GetType().Equals(System.Type.GetType("System.DBNull"))) && (drEach["destination_station"].ToString().Trim() != ""))
					{
						String destination_station = Utility.ReplaceSingleQuote(drEach["destination_station"].ToString());
						strBuilder.Append("'");
						strBuilder.Append(destination_station);
						strBuilder.Append("'");
					}
					else
					{
						strBuilder.Append("null");
					}
					strBuilder.Append(",");

					#endregion

					#region payerid

					if((drEach["payerid"]!= null) && (!drEach["payerid"].GetType().Equals(System.Type.GetType("System.DBNull"))) && (drEach["payerid"].ToString().Trim() != ""))
					{
						String payerid = Utility.ReplaceSingleQuote(drEach["payerid"].ToString());
						strBuilder.Append("'");
						strBuilder.Append(payerid);
						strBuilder.Append("'");
					}
					else
					{
						strBuilder.Append("null");
					}
					strBuilder.Append(",");

					#endregion

					#region route_code

					if((drEach["route_code"]!= null) && (!drEach["route_code"].GetType().Equals(System.Type.GetType("System.DBNull"))) && (drEach["route_code"].ToString().Trim() != ""))
					{
						String route_code = Utility.ReplaceSingleQuote(drEach["route_code"].ToString());
						strBuilder.Append("'");
						strBuilder.Append(route_code);
						strBuilder.Append("'");
					}
					else
					{
						strBuilder.Append("null");
					}
					strBuilder.Append(",");

					#endregion

					#region act_delivery_date

					if((drEach["act_delivery_date"]!= null) && (!drEach["act_delivery_date"].GetType().Equals(System.Type.GetType("System.DBNull")))&& (drEach["act_delivery_date"].ToString().Trim() != ""))
					{
						String act_delivery_date = drEach["act_delivery_date"].ToString();
						strBuilder.Append("'");
						strBuilder.Append(act_delivery_date);
						strBuilder.Append("'");	
					}
					else
					{
						strBuilder.Append("null");
					}
					strBuilder.Append(",");

					#endregion

					#region tot_freight_charge

					if((drEach["tot_freight_charge"]!= null) && (!drEach["tot_freight_charge"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						String tot_freight_charge = Utility.ReplaceSingleQuote(drEach["tot_freight_charge"].ToString());
						strBuilder.Append(tot_freight_charge);
					}
					else
					{
						strBuilder.Append("null");
					}
					strBuilder.Append(",");

					#endregion

					#region insurance_surcharge

					if((drEach["insurance_surcharge"]!= null) && (!drEach["insurance_surcharge"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						String insurance_surcharge = Utility.ReplaceSingleQuote(drEach["insurance_surcharge"].ToString());
						strBuilder.Append(insurance_surcharge);
					}
					else
					{
						strBuilder.Append("null");
					}
					strBuilder.Append(",");

					#endregion

					#region other_surch_amount

					if((drEach["other_surch_amount"]!= null) && (!drEach["other_surch_amount"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						String other_surch_amount = Utility.ReplaceSingleQuote(drEach["other_surch_amount"].ToString());
						strBuilder.Append(other_surch_amount);
					}
					else
					{
						strBuilder.Append("null");
					}
					strBuilder.Append(",");

					#endregion

					#region tot_vas_surcharge

					if((drEach["tot_vas_surcharge"]!= null) && (!drEach["tot_vas_surcharge"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						String tot_vas_surcharge = Utility.ReplaceSingleQuote(drEach["tot_vas_surcharge"].ToString());
						strBuilder.Append(tot_vas_surcharge);
					}
					else
					{
						strBuilder.Append("null");
					}
					strBuilder.Append(",");

					#endregion

					#region esa_surcharge

					if((drEach["esa_surcharge"]!= null) && (!drEach["esa_surcharge"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						String esa_surcharge = Utility.ReplaceSingleQuote(drEach["esa_surcharge"].ToString());
						strBuilder.Append(esa_surcharge);
					}
					else
					{
						strBuilder.Append("null");
					}
					strBuilder.Append(",");

					#endregion

					#region total_rated_amount

					if((drEach["total_rated_amount"]!= null) && (!drEach["total_rated_amount"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						String total_rated_amount = Utility.ReplaceSingleQuote(drEach["total_rated_amount"].ToString());
						strBuilder.Append(total_rated_amount);
					}
					else
					{
						strBuilder.Append("null");
					}
					strBuilder.Append(",");

					#endregion

					#region override_rated_freight

					if((drEach["override_rated_freight"]!= null) && (!drEach["override_rated_freight"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						String override_rated_freight = Utility.ReplaceSingleQuote(drEach["override_rated_freight"].ToString());
						strBuilder.Append("'" + override_rated_freight + "'");
					}
					else
					{
						strBuilder.Append("null");
					}
					strBuilder.Append(",");

					#endregion

					#region override_rated_ins

					if((drEach["override_rated_ins"]!= null) && (!drEach["override_rated_ins"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						String override_rated_ins = Utility.ReplaceSingleQuote(drEach["override_rated_ins"].ToString());
						strBuilder.Append("'" + override_rated_ins + "'");
					}
					else
					{
						strBuilder.Append("null");
					}
					strBuilder.Append(",");

					#endregion

					#region override_rated_other

					if((drEach["override_rated_other"]!= null) && (!drEach["override_rated_other"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						String override_rated_other = Utility.ReplaceSingleQuote(drEach["override_rated_other"].ToString());
						strBuilder.Append("'" + override_rated_other + "'");
					}
					else
					{
						strBuilder.Append("null");
					}
					strBuilder.Append(",");

					#endregion

					#region override_rated_esa

					if((drEach["override_rated_esa"]!= null) && (!drEach["override_rated_esa"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						String override_rated_esa = Utility.ReplaceSingleQuote(drEach["override_rated_esa"].ToString());
						strBuilder.Append("'" + override_rated_esa + "'");
					}
					else
					{
						strBuilder.Append("null");
					}
					strBuilder.Append(",");

					#endregion

					#region override_rated_total

					if((drEach["override_rated_total"]!= null) && (!drEach["override_rated_total"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						String override_rated_total = Utility.ReplaceSingleQuote(drEach["override_rated_total"].ToString());
						strBuilder.Append("'" + override_rated_total + "'");
					}
					else
					{
						strBuilder.Append("null");
					}
					strBuilder.Append(",");

					#endregion

					#region excluded_reason

					if((drEach["excluded_reason"]!= null) && (!drEach["excluded_reason"].GetType().Equals(System.Type.GetType("System.DBNull"))) && (drEach["excluded_reason"].ToString().Trim() != ""))
					{
						String excluded_reason = Utility.ReplaceSingleQuote(drEach["excluded_reason"].ToString());
						strBuilder.Append("'");
						strBuilder.Append(excluded_reason);
						strBuilder.Append("'");
					}
					else
					{
						strBuilder.Append("null");
					}
					strBuilder.Append(",");

					#endregion

					#region recordType

					if (drEach["excluded_reason"].ToString().Trim() == "")
						strBuilder.Append("'valid'");							//Valid Record
					else
						strBuilder.Append("'invalid'");							//Invalid Record

					strBuilder.Append(",");
					
					#endregion

					#region isvalid_consignment_no

					if((drEach["isvalid_consignment_no"]!= null) && (!drEach["isvalid_consignment_no"].GetType().Equals(System.Type.GetType("System.DBNull"))) && (drEach["isvalid_consignment_no"].ToString().Trim() != ""))
					{
						int isvalid_consignment_no = Convert.ToInt16(drEach["isvalid_consignment_no"]);
						strBuilder.Append(isvalid_consignment_no);
					}
					else
					{
						strBuilder.Append("null");
					}
					strBuilder.Append(",");

					#endregion

					#region isvalid_override_rated_freight

					if((drEach["isvalid_override_rated_freight"]!= null) && (!drEach["isvalid_override_rated_freight"].GetType().Equals(System.Type.GetType("System.DBNull"))) && (drEach["isvalid_override_rated_freight"].ToString().Trim() != ""))
					{
						int isvalid_override_rated_freight = Convert.ToInt16(drEach["isvalid_override_rated_freight"]);
						strBuilder.Append(isvalid_override_rated_freight);
					}
					else
					{
						strBuilder.Append("null");
					}
					strBuilder.Append(",");

					#endregion

					#region isvalid_override_rated_ins

					if((drEach["isvalid_override_rated_ins"]!= null) && (!drEach["isvalid_override_rated_ins"].GetType().Equals(System.Type.GetType("System.DBNull"))) && (drEach["isvalid_override_rated_ins"].ToString().Trim() != ""))
					{
						int isvalid_override_rated_ins = Convert.ToInt16(drEach["isvalid_override_rated_ins"]);
						strBuilder.Append(isvalid_override_rated_ins);
					}
					else
					{
						strBuilder.Append("null");
					}
					strBuilder.Append(",");

					#endregion

					#region isvalid_override_rated_other

					if((drEach["isvalid_override_rated_other"]!= null) && (!drEach["isvalid_override_rated_other"].GetType().Equals(System.Type.GetType("System.DBNull"))) && (drEach["isvalid_override_rated_other"].ToString().Trim() != ""))
					{
						int isvalid_override_rated_other = Convert.ToInt16(drEach["isvalid_override_rated_other"]);
						strBuilder.Append(isvalid_override_rated_other);
					}
					else
					{
						strBuilder.Append("null");
					}
					strBuilder.Append(",");

					#endregion

					#region isvalid_override_rated_esa

					if((drEach["isvalid_override_rated_esa"]!= null) && (!drEach["isvalid_override_rated_esa"].GetType().Equals(System.Type.GetType("System.DBNull"))) && (drEach["isvalid_override_rated_esa"].ToString().Trim() != ""))
					{
						int isvalid_override_rated_esa = Convert.ToInt16(drEach["isvalid_override_rated_esa"]);
						strBuilder.Append(isvalid_override_rated_esa);
					}
					else
					{
						strBuilder.Append("null");
					}
					strBuilder.Append(",");
					#endregion

					#region applicationid
						if((strAppID != null) && (strAppID.Trim() != ""))
						{
							strBuilder.Append("'");
							strBuilder.Append(strAppID);
							strBuilder.Append("'");	
						}
						else
						{
							strBuilder.Append("null");
						}
						strBuilder.Append(",");
					#endregion

					#region aenterpriseid
					if((strEnterpriseID != null) && (strEnterpriseID.Trim() != ""))
					{
						strBuilder.Append("'");
						strBuilder.Append(strEnterpriseID);
						strBuilder.Append("'");	
					}
					else
					{
						strBuilder.Append("null");
					}
					
					#endregion

					strBuilder.Append(")");

					dbCmdCons.CommandText = strBuilder.ToString();
					iRowsAffected = dbConCons.ExecuteNonQueryInTransaction(dbCmdCons);

					#endregion
				
				}
				catch(ApplicationException appException)
				{
					try
					{
						transactionAppCons.Rollback();
						Logger.LogDebugInfo("ImportConsignmentsDAL","ImportToTempTables","ImportToTempTables","App db insert transaction rolled back.");

					}
					catch(Exception rollbackException)
					{
						Logger.LogTraceError("ImportConsignmentsDAL","ImportToTempTables","ImportToTempTables","Error during app transanction roll back "+ rollbackException.Message.ToString());
					}

					Logger.LogTraceError("ImportConsignmentsDAL","ImportToTempTables","ImportToTempTables","Insert Transaction Failed : "+ appException.Message.ToString());

					throw new ApplicationException("Appliction Exception Error adding Temp Table",appException); //boon
				}
				catch(Exception exception)
				{
					try
					{
						transactionAppCons.Rollback();
						Logger.LogDebugInfo("ImportConsignmentsDAL","ImportToTempTables","ImportToTempTables","App db insert transaction rolled back.");

					}
					catch(Exception rollbackException)
					{
						Logger.LogTraceError("ImportConsignmentsDAL","ImportToTempTables","ImportToTempTables", "Error during app transanction roll back "+ rollbackException.Message.ToString());
					}

					Logger.LogTraceError("ImportConsignmentsDAL","ImportToTempTables","ImportToTempTables","Insert Transaction Failed : "+ exception.Message.ToString());
					
					throw new ApplicationException("Error adding Temp Table ",exception);
				}
			}
			transactionAppCons.Commit();	//Commit after end For Loop, by boon

			if(conAppCons != null)
			{
				conAppCons.Close();
			}
		}


		public static DataSet GetImportDataByRecordType(String strAppID, String strEnterpriseID, int iCurRow, int iDSRowSize, String userId, String recordType)
		{
			SessionDS sessionDS = null;			
			StringBuilder strBuilder = new StringBuilder();

			strBuilder.Append("SELECT recordid, userid, consignment_no, isvalid_consignment_no, ref_no, destination_station, route_code, payerid, act_delivery_date, ");
			strBuilder.Append("tot_freight_charge, insurance_surcharge, other_surch_amount, tot_vas_surcharge, esa_surcharge, total_rated_amount, ");
			strBuilder.Append("override_rated_freight, isvalid_override_rated_freight, override_rated_ins, isvalid_override_rated_ins, override_rated_other, isvalid_override_rated_other, override_rated_esa, isvalid_override_rated_esa, override_rated_total, ");
			strBuilder.Append("excluded_reason, recordType ");
			strBuilder.Append("FROM	ImportOverride ");
			strBuilder.Append("WHERE userid = '" + userId + "' ");
			strBuilder.Append("AND recordType = '" + recordType + "' ");
	
			
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("ImportOverrideRateDAL", "GetImportDataByRecordType", "GetImportDataByRecordType","DbConnection object is null!!");
				return sessionDS.ds;
			}
			
			if(recordType == "valid")
				sessionDS = dbCon.ExecuteQuery(strBuilder.ToString(), iCurRow, iDSRowSize, "ValidImportData");
			else if (recordType == "invalid")
				sessionDS = dbCon.ExecuteQuery(strBuilder.ToString(), iCurRow, iDSRowSize, "InvalidImportData");

			strBuilder = null;

			return  sessionDS.ds;
		}

		
		public static int InsertOverrideRate(String strAppID, String strEnterpriseID, String userID)
		{
			//Create DbConnection
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("InsertOverrideRate", "ImportOverrideRateDAL", "InsertOverrideRate","ERR001","DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null",null);
			}

			//Create IDbConnection
			IDbConnection conApp = dbCon.GetConnection();
			if(conApp == null)
			{
				Logger.LogTraceError("InsertOverrideRate", "ImportOverrideRateDAL", "InsertOverrideRate", "ERR004", "conApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			//Open Connection
			try
			{
				dbCon.OpenConnection(ref conApp);
			}
			catch(ApplicationException appException)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("InsertOverrideRate","ImportOverrideRateDAL","InsertOverrideRate","ERR005","Error opening connection : "+ appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ",appException);
			}
			catch(Exception exception)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("InsertOverrideRate","ImportOverrideRateDAL","InsertOverrideRate","ERR006","Error opening connection : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}

			//Create Transaction
			IDbTransaction transactionApp = dbCon.BeginTransaction(conApp,IsolationLevel.ReadCommitted);
			if(transactionApp == null)
			{
				Logger.LogTraceError("InsertOverrideRate","ImportOverrideRateDAL","InsertOverrideRate","ERR007","transactionApp is null");
				throw new ApplicationException("Failed Begining a App Transaction..",null);
			}

			IDbCommand dbCmd = null;
			int iRowsAffected = 0;

			try
			{
				dbCmd = dbCon.CreateCommand("", CommandType.Text);
				dbCmd.Connection = conApp;
				dbCmd.Transaction = transactionApp;

				StringBuilder strBuilder = new StringBuilder();
				strBuilder.Append (@"UPDATE  	Shipment
						SET original_rated_freight=
								CASE   
								WHEN ISNULL(Imp.override_rated_freight,'Y')  = 'Y'  THEN Shp.original_rated_freight   
								ELSE   Shp.tot_freight_charge   END
						, tot_freight_charge=
								CASE 
								WHEN ISNULL(Imp.override_rated_freight,'Y')  = 'Y'    THEN Shp.tot_freight_charge 
								ELSE   Imp.override_rated_freight  END
						, original_rated_ins=
								CASE 
								WHEN ISNULL(Imp.override_rated_ins,'Y')  = 'Y'  THEN Shp.original_rated_ins 
								ELSE Shp.insurance_surcharge END
						, insurance_surcharge=
								CASE 
								WHEN ISNULL(Imp.override_rated_ins,'Y')  = 'Y'  THEN Shp.insurance_surcharge 
								ELSE  Imp.override_rated_ins END
						, original_rated_other=
								CASE 
								WHEN  ISNULL(Imp.override_rated_other,'Y')  = 'Y'  THEN Shp.original_rated_other 
								ELSE  Shp.other_surch_amount END
						, other_surch_amount=
								CASE 
								WHEN ISNULL(Imp.override_rated_other,'Y')  = 'Y'  THEN Shp.other_surch_amount 
								ELSE Imp.override_rated_other END
						, original_rated_esa=
								CASE 
								WHEN ISNULL(Imp.override_rated_esa,'Y')  = 'Y'  THEN Shp.original_rated_esa 
								ELSE Shp.esa_surcharge END
						, esa_surcharge=
								CASE 
								WHEN ISNULL(Imp.override_rated_esa,'Y')  = 'Y'  THEN Shp.esa_surcharge 
								ELSE Imp.override_rated_esa END
						, original_rated_total=Shp.total_rated_amount
						, total_rated_amount=
									(CASE 
										WHEN ISNULL(Imp.override_rated_freight,'Y')  = 'Y'    THEN Shp.tot_freight_charge 
										ELSE   Imp.override_rated_freight  END) 
								+ (CASE 
										WHEN ISNULL(Imp.override_rated_ins,'Y')  = 'Y'  THEN Shp.insurance_surcharge 
										ELSE  Imp.override_rated_ins END) 
								+ (CASE 
										WHEN ISNULL(Imp.override_rated_other,'Y')  = 'Y'  THEN Shp.other_surch_amount 
										ELSE Imp.override_rated_other END) 
								+ Shp.tot_vas_surcharge 
								+  (CASE 
										WHEN ISNULL(Imp.override_rated_esa,'Y')  = 'Y'  THEN Shp.esa_surcharge 
										ELSE Imp.override_rated_esa END)
						,manual_over_user		=	'" + userID + @"'
						,manual_over_datetime	=	getdate()
						,manual_override			=	'Y' 
				FROM	ImportOverride  Imp   	INNER JOIN Shipment    Shp
				ON		(Imp.consignment_no	= Shp.consignment_no  AND  Imp.applicationid = Shp.applicationid 
								AND  Imp.enterpriseid = Shp.enterpriseid   )
				WHERE	Imp.userid	= '" + userID + @"'    AND  Imp.recordType = 'valid' ");			
				
				String strQuery = strBuilder.ToString();
				dbCmd.CommandText = strQuery;

				Logger.LogDebugInfo("InsertOverrideRate","ImportOverrideRateDAL","InsertOverrideRate","INF001","QUERY IS : "+strQuery);
				iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
				Logger.LogDebugInfo("InsertOverrideRate","ImportOverrideRateDAL","InsertOverrideRate","INF002",iRowsAffected + " rows inserted into Shipment table ....");

				transactionApp.Commit();

				Logger.LogDebugInfo("InsertOverrideRate","ImportOverrideRateDAL","InsertOverrideRate","INF009","App db insert transaction committed.");
			}
			catch(ApplicationException appException)
			{
				try
				{
					transactionApp.Rollback();
					Logger.LogDebugInfo("InsertOverrideRate","ImportOverrideRateDAL","InsertOverrideRate","INF010","App db insert transaction rolled back.");

				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("InsertOverrideRate","ImportOverrideRateDAL","InsertOverrideRate","ERR008","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}


				Logger.LogTraceError("InsertOverrideRate","ImportOverrideRateDAL","InsertOverrideRate","ERR009","Insert Transaction Failed : "+ appException.Message.ToString());
				throw new ApplicationException("Error adding Shipment ",appException);
			}
			catch(Exception exception)
			{
				try
				{
					transactionApp.Rollback();
					Logger.LogDebugInfo("InsertOverrideRate","ImportOverrideRateDAL","InsertOverrideRate","INF011","App db insert transaction rolled back.");

				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("InsertOverrideRate","ImportOverrideRateDAL","InsertOverrideRate","ERR010","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}

				Logger.LogTraceError("InsertOverrideRate","ImportOverrideRateDAL","InsertOverrideRate","ERR011","Insert Transaction Failed : "+ exception.Message.ToString());
				throw new ApplicationException("Error adding Shipment ",exception);
			}
			finally
			{
				if(conApp != null)
				{
					conApp.Close();
				}
			}
			return iRowsAffected;
		}
	}
}
