using System;
using System.Data;
using com.common.classes;
using com.common.DAL;
using com.common.util;
using com.ties.classes;

namespace TIESDAL
{

	public class EnterpriseConfigMgrDAL
	{
		public EnterpriseConfigMgrDAL()
		{

		}

		public static DomesticShipmentConfigurations GetDomesticShipmentConfigurations(string appID,string enterpriseID)
		{
			string strQry = null;
			IDbCommand dbCmd = null;
			DataSet dsConfig = null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(appID,enterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("EnterpriseConfigMgrDAL","DomesticShipment","EDSM102","DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null",null);
			}
			strQry = "select * from dbo.EnterpriseConfigurations('PNGAF','DomesticShipment')";

			dbCmd = dbCon.CreateCommand(strQry,CommandType.Text);
			try
			{
				dsConfig = (DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
			}
			catch(System.ApplicationException appException)
			{
				Logger.LogTraceError("DomesticShipmentManager","GetFromPickUp","","Error ");
				throw appException;
			}
			return EnterpriseConfigurations.DomesticShipment(dsConfig);
		}

		public static CustomerProfileConfigurations GetCustomerProfileConfigurations(string appID,string enterpriseID)
		{
			string strQry = null;
			IDbCommand dbCmd = null;
			DataSet dsConfig = null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(appID,enterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("EnterpriseConfigMgrDAL","DomesticShipment","EDSM102","DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null",null);
			}
			strQry = "select * from dbo.EnterpriseConfigurations('PNGAF','CustomerProfile')";

			dbCmd = dbCon.CreateCommand(strQry,CommandType.Text);
			try
			{
				dsConfig = (DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
			}
			catch(System.ApplicationException appException)
			{
				Logger.LogTraceError("DomesticShipmentManager","GetFromPickUp","","Error ");
				throw appException;
			}
			return EnterpriseConfigurations.CustomerProfile(dsConfig);
		}

		public static ManifestFormsConfigurations GetManifestFormsConfigurations(string appID,string enterpriseID)
		{
			string strQry = null;
			IDbCommand dbCmd = null;
			DataSet dsConfig = null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(appID,enterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("EnterpriseConfigMgrDAL","DomesticShipment","EDSM102","DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null",null);
			}
			strQry = "select * from dbo.EnterpriseConfigurations('PNGAF','ManifestForms')";

			dbCmd = dbCon.CreateCommand(strQry,CommandType.Text);
			try
			{
				dsConfig = (DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
			}
			catch(System.ApplicationException appException)
			{
				Logger.LogTraceError("DomesticShipmentManager","GetFromPickUp","","Error ");
				throw appException;
			}
			return EnterpriseConfigurations.ManifestForms(dsConfig);
		}
		public static PackageWtDimConfigurations GetPackageWtDimConfigurations(string appID,string enterpriseID)
		{
			string strQry = null;
			IDbCommand dbCmd = null;
			DataSet dsConfig = null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(appID,enterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("EnterpriseConfigMgrDAL","PackageWtDim","EDSM102","DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null",null);
			}
			strQry = "select * from dbo.EnterpriseConfigurations('"+enterpriseID+"','SWB_PkgDims')";

			dbCmd = dbCon.CreateCommand(strQry,CommandType.Text);
			try
			{
				dsConfig = (DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
			}
			catch(System.ApplicationException appException)
			{
				Logger.LogTraceError("EnterpriseConfigMgrDAL","GetPackageWtDimConfigurations","","Error ");
				throw appException;
			}
			return EnterpriseConfigurations.PackageWtDimForms(dsConfig);
		}
		public static QueryShipmentTrackingConfigurations GetQueryShipmentTrackingConfigurations(string appID,string enterpriseID)
		{
			string strQry = null;
			IDbCommand dbCmd = null;
			DataSet dsConfig = null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(appID,enterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("EnterpriseConfigMgrDAL","QueryShipmentTracking","EDSM102","DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null",null);
			}
			strQry = "select * from dbo.EnterpriseConfigurations('PNGAF','QueryShipmentTracking')";

			dbCmd = dbCon.CreateCommand(strQry,CommandType.Text);
			try
			{
				dsConfig = (DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
			}
			catch(System.ApplicationException appException)
			{
				Logger.LogTraceError("QueryShipmentTrackingManager","GetConfig","","Error ");
				throw appException;
			}
			return EnterpriseConfigurations.QueryShipmentTracking(dsConfig);
		}

	}
}
