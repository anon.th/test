using System;
using System.Data;
using System.Text;
using com.common.DAL;
using com.common.classes;
using com.ties.classes;
using com.common.util;
using System.Data.SqlClient;
//using com.common.util;

namespace com.ties.DAL
{
	/// <summary>
	/// Summary description for ShipmentUpdateMgrDAL.
	/// </summary>
	/// 
	
	public struct getConsignmentParam
	{
		public string strDeliveryPathCode;
		public DateTime dtDeliveryDatetime;
		public string strFltVehNo;
		public DateTime dtDepartDatetime;
		public string strRouteCode;
	}

	public class ShipmentUpdateMgrDAL
	{
		public ShipmentUpdateMgrDAL()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		/// <summary>
		/// This method gets the empty dataset for the Shipment update by route.
		/// </summary>
		/// <returns>Dataset</returns>
		/// 
		public static DataSet GetEmptyshipupdDS()
		{
			DataTable dtShpmntupd = new DataTable("ShpUpdate");
			dtShpmntupd.Columns.Add(new DataColumn("booking_no", typeof(int)));
			dtShpmntupd.Columns.Add(new DataColumn("consignment_no",typeof(string)));
			dtShpmntupd.Columns.Add(new DataColumn("tracking_datetime",typeof(DateTime)));
			dtShpmntupd.Columns.Add(new DataColumn("status_code",typeof(string)));
			dtShpmntupd.Columns.Add(new DataColumn("exception_code",typeof(string)));
			dtShpmntupd.Columns.Add(new DataColumn("person_incharge",typeof(string)));
			dtShpmntupd.Columns.Add(new DataColumn("remarks",typeof(string)));
			dtShpmntupd.Columns.Add(new DataColumn("last_userid",typeof(string)));
			dtShpmntupd.Columns.Add(new DataColumn("last_updated",typeof(string)));
			dtShpmntupd.Columns.Add(new DataColumn("CnsgBkgNo",typeof(string)));
			//HC Return Task
			dtShpmntupd.Columns.Add(new DataColumn("ZipCode",typeof(string)));
			//HC Return Task
			//BY X JAN 21 08
			dtShpmntupd.Columns.Add(new DataColumn("location",typeof(string)));
			dtShpmntupd.Columns.Add(new DataColumn("consignee_name",typeof(string)));
			//END BY X JAN 21 08
			// BY TU 16/02/2011
			dtShpmntupd.Columns.Add(new DataColumn("pkg_no",typeof(string)));



			DataColumn[] PrimKey = new DataColumn[3];
			PrimKey[0] = dtShpmntupd.Columns["CnsgBkgNo"];
			PrimKey[1] = dtShpmntupd.Columns["tracking_datetime"];
			PrimKey[2] = dtShpmntupd.Columns["pkg_no"];
			dtShpmntupd.PrimaryKey = PrimKey;

			DataSet dsShpmntup = new DataSet();
			dsShpmntup.Tables.Add(dtShpmntupd);

			dsShpmntup.Tables["ShpUpdate"].Columns["consignment_no"].Unique = true; //Checks duplicate records..
			dsShpmntup.Tables["ShpUpdate"].Columns["consignment_no"].AllowDBNull = false;
			
			return  dsShpmntup;
		}
		

		/// <summary>
		/// This method creates an empty dataset  with the value of only tracking_datetime.
		/// </summary>
		/// <param name="dsShpmntup">Dataset</param>
		/// <param name="scanDateTime">The tracking date time to be updated in the dataset</param>
		public static void AddNewRowInShpmntUpdt(ref DataSet dsShpmntup,String scanDateTime,String strLocation)
		{
			DataRow drNew = dsShpmntup.Tables[0].NewRow();

			int vDate = Convert.ToInt32(scanDateTime.Substring(0, 2));
			int vMonth = Convert.ToInt32(scanDateTime.Substring(3, 2));
			int vYear = Convert.ToInt32(scanDateTime.Substring(6, 4));
			int vHour = 0;
			int vMin = 0;
			if(scanDateTime.Length>11)
			{
				vHour = Convert.ToInt32(scanDateTime.Substring(11, 2));
			}
			if(scanDateTime.Length>14)
			{
				vMin = Convert.ToInt32(scanDateTime.Substring(14, 2));
			}
			
			drNew[0] = 0;
			drNew[1] = "";
			drNew["tracking_datetime"] = new DateTime(vYear,vMonth,vDate,vHour,vMin, 0);
			
			drNew["location"] = strLocation;
			drNew[3] = "";
			drNew[4] = "";
			drNew[5] = "";
			drNew[6] = "";
			drNew[7] = "";
			drNew[8] = "";
			drNew[9] = "";

			try
			{
				dsShpmntup.Tables[0].Rows.Add(drNew);
			}
			catch(Exception ex)
			{
				Logger.LogTraceError("shipmentUpdateMgrDAL","AddNewRowInShpmntUpdt","R005",ex.Message.ToString());
			}
			
		}


		/// <summary>
		/// This method updates table shipment, inserts into shipment_tracking table when it is Domestic.
		/// and when it is Pickup it updates pickup_request table and inserts into dispatch_tracking table.
		/// </summary>
		/// <param name="strAppID">Application ID</param>
		/// <param name="strEnterpriseID">Enterprise ID</param>
		/// <param name="dsShpmtUpdate">Dataset</param>
		/// <param name="strStatusCode">Status Code</param>
		/// <param name="strExceptionCode">Exception Code</param>
		/// <param name="strDmstPickup">Type which contains value "Domestic" or "Pickup"</param>
		/// <param name="userID">The logged in user ID</param>
		/// <param name="scanDate"></param>
		public static void AddRecordShipmntTracking(String strAppID,String strEnterpriseID,DataSet dsShpmtUpdate,String strStatusCode,String strExceptionCode,String strDmstPickup,String userID,String strBatch_no)
		{	
			IDbCommand dbCmd = null;
			int iRowsAffected = 0;
			int cnt = 0;
			int i = 0;
			int iBookingNo = 0;
			StringBuilder strBuilder = null;
			DateTime dtCurrent = DateTime.Now;
			String strExceptionMBGStatus=null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("shipmentUpdateMgrDAL","AddRecordShipmntTracking","ERR001","DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null",null);
			}
			if (dsShpmtUpdate == null)
			{
				Logger.LogTraceError("shipmentUpdateMgrDAL","AddRecordShipmntTracking","ERR002","DataSet is null!!");
				throw new ApplicationException("The Shipment Tracking DataSet is null",null);
			}

			IDbConnection conApp = dbCon.GetConnection();
			
			if(conApp == null)
			{
				Logger.LogTraceError("ShipmentUpdateMgrDAL","AddRecordShipmntTracking","ERR003","conApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			try
			{
				dbCon.OpenConnection(ref conApp);
			}
			catch(ApplicationException appException)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("ShipmentUpdateMgrDAL","AddRecordShipmntTracking","ERR004","Error opening connection : "+ appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ",appException);
			}
			catch(Exception exception)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("ShipmentUpdateMgrDAL","AddRecordShipmntTracking","ERR005","Error opening connection : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}


			IDbTransaction transactionApp = dbCon.BeginTransaction(conApp,IsolationLevel.ReadCommitted);

			if(transactionApp == null)
			{
				Logger.LogTraceError("ShipmentUpdateMgrDAL","AddRecordShipmntTracking","ERR006","transactionApp is null");
				throw new ApplicationException("Failed Begining a App Transaction..",null);
			}
			try
			{
				dbCmd = dbCon.CreateCommand("select count(*) from Core_Enterprise with(nolock)" ,CommandType.Text);
				dbCmd.Connection = conApp;
				dbCmd.Transaction = transactionApp;
				iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);

				cnt = dsShpmtUpdate.Tables[0].Rows.Count;
				DataRow drEach = null;
				i = 0;
				if(strDmstPickup.Equals("Domestic"))
				{
					#region Domestic
					for(i=0;i<cnt;i++)
					{
                        iBookingNo = 0;
						drEach = dsShpmtUpdate.Tables[0].Rows[i];	
						String strConsgtNo = (String)drEach["CnsgBkgNo"];

						//updating MBG
						if (Utility.IsNotDBNull(drEach["booking_no"]))
						{
							iBookingNo = (int)drEach["booking_no"];
//							strExceptionMBGStatus=getMBGStatus(strAppID,strEnterpriseID,strStatusCode,strExceptionCode,iBookingNo, strConsgtNo,dbCon,dbCmd);
						}
						#region  Comment by Aoo 26-04-2010 Old code
						strBuilder = new StringBuilder();	
						//Inserted by Bernard Phua on 31-Dec-2002
						// Update Act_Delivery_Datetime on shipment Table.
						// ---> Start
						//updating shipment		
						iBookingNo = (int)drEach["booking_no"];
						String[] strPOD = TIESUtility.getPODStatus(strAppID,strEnterpriseID);
						DateTime dtTrackingDtTimeInput=DateTime.Now;

						for(int j=0; j<strPOD.Length; j++)
						{
							if(strStatusCode == strPOD[j])
							{							
//								String strActDeliveryDateTime = getActualDeliveryDateTime(strAppID,strEnterpriseID,strConsgtNo,iBookingNo,strPOD,dbCon,dbCmd);
//							
//								if (strActDeliveryDateTime != null)
//								{
//									dtTrackingDtTimeInput = DateTime.ParseExact(strActDeliveryDateTime, "dd/MM/yyyy HH:mm", null);
//								}
								strBuilder = new StringBuilder();
								strBuilder.Append("Update Shipment with(rowlock) Set act_delivery_date = ");
								
							//if ((strActDeliveryDateTime != null && DateTime.ParseExact(strActDeliveryDateTime,"dd/MM/yyyy HH:mm",null) > dtTrackingDtTimeInput))
//								if (strActDeliveryDateTime != null)
//								{
//									DateTime dtTrckingDtTimedr=(DateTime)drEach["tracking_datetime"];
//									if(dtTrackingDtTimeInput.CompareTo(dtTrckingDtTimedr)> 0)
//									{
//										dtTrackingDtTimeInput=dtTrckingDtTimedr;
//									}
//									strBuilder.Append(Utility.DateFormat(strAppID,strEnterpriseID,dtTrackingDtTimeInput,DTFormat.DateTime));
//								}
//								else
//								{
									dtTrackingDtTimeInput = (DateTime) drEach["tracking_datetime"];
									strBuilder.Append(Utility.DateFormat(strAppID,strEnterpriseID,dtTrackingDtTimeInput,DTFormat.DateTime));
//								}
							
								strBuilder.Append(" where applicationid = '");
								strBuilder.Append(strAppID);
								strBuilder.Append("' and enterpriseid = '");
								strBuilder.Append(strEnterpriseID + "'");
								strBuilder.Append(" and consignment_no = ");
								
								if(Utility.IsNotDBNull(drEach["CnsgBkgNo"]))
								{
									String strConsgmntNo = (String)drEach["CnsgBkgNo"];
									strBuilder.Append("'");
									strBuilder.Append(strConsgmntNo);
									strBuilder.Append("'");
								}

								if(Utility.IsNotDBNull(drEach["booking_no"]))
								{
									iBookingNo = (int)drEach["booking_no"];
									strBuilder.Append(" and booking_no = " + iBookingNo);
								}
								else
								{
									Logger.LogTraceError("ShipmentUpdateMgrDAL","AddRecordShipmntTracking","ERR010","Update Shipment table on Actual Delivery failed: Booking_No is null.");
									throw new ApplicationException("Failed Begining a App Transaction..",null);
								}


								dbCmd.CommandText = strBuilder.ToString();
								iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
								//Logger.LogTraceInfo("ShipmentUpdateMgrDAL","AddRecordShipmntTracking","INF012",iRowsAffected + " rows updated into shipment table on Act_Delivery_Datetime");
								break;	
							}
						}
					//Update the shipment table status code
					//get status code 
						DataSet dtSC=null;						
						String strStatusCodeSC=null;
						String strExceptionCodeSC=null;
						DateTime dtTrackingDtTime;
						DateTime dtTrackingDtTimeSC=System.DateTime.Now;
						if (Utility.IsNotDBNull(drEach["booking_no"]))
						{
							iBookingNo = (int)drEach["booking_no"];
						}
						dtSC = getLatestStatusCode(strAppID,strEnterpriseID,iBookingNo, strConsgtNo,dbCon,dbCmd);
						if (dtSC.Tables[0].Rows.Count > 0)
						{
							DataRow	drEachSC = dtSC.Tables[0].Rows[0];
							if(Utility.IsNotDBNull(drEachSC["tracking_datetime"]))
							{
								dtTrackingDtTimeSC = (DateTime) drEachSC["tracking_datetime"];
								if(Utility.IsNotDBNull(drEach["tracking_datetime"]))
								{
									dtTrackingDtTime = (DateTime) drEach["tracking_datetime"];
									if(dtTrackingDtTimeSC.CompareTo(dtTrackingDtTime) > 0)
									{
										if(Utility.IsNotDBNull(drEachSC["status_code"]))
										{
											strStatusCodeSC=(String)drEachSC["status_code"];
										}
										
										if(Utility.IsNotDBNull(drEachSC["exception_code"]))
										{
											strExceptionCodeSC=(String)drEachSC["exception_code"];
										}
										
									}
									else
									{
										dtTrackingDtTimeSC = (DateTime) drEach["tracking_datetime"];
										strStatusCodeSC=strStatusCode;
										strExceptionCodeSC=strExceptionCode;
									}
								}
								
							}
						}
						else
						{
							dtTrackingDtTimeSC = (DateTime) drEach["tracking_datetime"];
							strStatusCodeSC=strStatusCode;
							strExceptionCodeSC=strExceptionCode;
						}
						strBuilder=new StringBuilder();
						strBuilder.Append(" update shipment with(rowlock) set last_status_code = ");
					
						if (strStatusCodeSC != null)
						{
							strBuilder.Append("'");
							strBuilder.Append(strStatusCodeSC);
							strBuilder.Append("'");
						}	
						else
						{
							strBuilder.Append("null");
						}
						strBuilder.Append(",");

						strBuilder.Append(" last_exception_code = ");
						if(strExceptionCodeSC != null)
						{
							strBuilder.Append("'");
							strBuilder.Append(strExceptionCodeSC);
							strBuilder.Append("'");
						}
						else
						{
							strBuilder.Append("null");
						}
						strBuilder.Append(",");
						strBuilder.Append(" last_status_datetime = ");
						strBuilder.Append(Utility.DateFormat(strAppID,strEnterpriseID,dtTrackingDtTimeSC,DTFormat.DateTime));
					//updating mbg
						if(strExceptionMBGStatus !=null && strExceptionMBGStatus.ToUpper() !="N")
						{
							strBuilder.Append(",");
							strBuilder.Append(" mbg = ");
							strBuilder.Append("'");
							strBuilder.Append(strExceptionMBGStatus);
							strBuilder.Append("'");
						}
						strBuilder.Append(" where applicationid = '");
						strBuilder.Append(strAppID);
						strBuilder.Append("' and enterpriseid = '");
						strBuilder.Append(strEnterpriseID+"'");
						strBuilder.Append(" and consignment_no = ");
						if(Utility.IsNotDBNull(drEach["CnsgBkgNo"]))
						{
							String strConsgmntNo = (String)drEach["CnsgBkgNo"];
							strBuilder.Append("'");
							strBuilder.Append(strConsgmntNo);
							strBuilder.Append("'");
						}
						if (Utility.IsNotDBNull(drEach["booking_no"]))
						{
							iBookingNo = (int)drEach["booking_no"];
							strBuilder.Append(" and booking_no = "+iBookingNo);
						}
						
						//strBuilder.Append(" and ((last_status_datetime is null) or (last_status_datetime <= ");
	//					strBuilder.Append(" and ((last_status_datetime is not null) or(last_status_datetime <= ");
//						
//						if(Utility.IsNotDBNull(drEach["tracking_datetime"])) 
//						{
//							dtTrackingDtTime = (DateTime) drEach["tracking_datetime"];
//							strBuilder.Append(Utility.DateFormat(strAppID,strEnterpriseID,dtTrackingDtTime,DTFormat.DateTime)+"))");
//						}
//						else
//						{
//							strBuilder.Append("null");
//						}

						
						dbCmd.CommandText = strBuilder.ToString();
						iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
						Logger.LogTraceInfo("ShipmentUpdateMgrDAL","AddRecordShipmentTracking","INF007", iRowsAffected.ToString() + " rows updated in shipment table");
				
						
				
						//----> End

						//Modified by Elango dt.23/12/2002
						//Remark: To include surcharge amount in shipment_tracking table from Customer/Agent Profile
						//----Start
						string strInvoiceable=null,	strConsgNo=null;
						decimal	decSurcharge=0;

						if(Utility.IsNotDBNull(drEach["CnsgBkgNo"]))
							strConsgNo = (String)drEach["CnsgBkgNo"];
						
						//get surcharge details from Agent/Customer table
						decSurcharge= GetPayerSurcharge(strAppID,strEnterpriseID,strConsgNo,iBookingNo,strStatusCode,strExceptionCode,dbCmd,dbCon);

//						if (decSurcharge > 0) strInvoiceable = "Y";
//						else strInvoiceable = "N";

						//Status and Exception Codes 
						//both have an Invoiceable flag - either Y or N.  
						if (strExceptionCode!=null && strExceptionCode!="")
						{
							//Exception_Code.invoiceable means the invoiceable field of the record 
							//in the Exception_Code table that corresponds to the Status code entered on the form.
//							DataSet dsExceptionInvoiceable = TIESUtility.GetInvoiceableByExceptionCode(strAppID, strEnterpriseID,strStatusCode,strExceptionCode);
//							if(dsExceptionInvoiceable.Tables[0].Rows.Count > 0)
//								strInvoiceable = (String)dsExceptionInvoiceable.Tables[0].Rows[0]["invoiceable"];
                            
							//4.7.3 If no POD slip return is required
							strInvoiceable = getInvableExcepCode(dbCon, dbCmd,strAppID, strEnterpriseID, strConsgNo, iBookingNo, strStatusCode,strExceptionCode);

							//4.7.3 If the entered exception code exists for the Customer in Customer_Exception_Charge.exception_code
                            //decSurcharge
							SetSurchOfCust(dbCon, dbCmd,strAppID, strEnterpriseID, strConsgNo, iBookingNo, strStatusCode,strExceptionCode,ref decSurcharge);
						}
						//else
						//{
						//	//Status_Code.invoiceable means the invoiceable field of the record 
						//	//in the Status_Code table that corresponds to the Status code entered on the form.
						//	DataSet dsStatusCodeInvoiceable = TIESUtility.GetInvoiceableByStatusCode(strAppID, strEnterpriseID,strStatusCode);
						
						//	if(dsStatusCodeInvoiceable.Tables[0].Rows.Count > 0)
						//		strInvoiceable = (String)dsStatusCodeInvoiceable.Tables[0].Rows[0]["invoiceable"];
						//}
						//----End

						#region POD
						//HC Return Task
						if(strStatusCode.Trim() == "POD") 
						{
							DataRow drEachHC_POD = dsShpmtUpdate.Tables[0].Rows[i];							
							String strZipCodeHC_POD = (String)drEachHC_POD["ZipCode"];							
							DateTime dtNewEstHC_POD;

							if( drEachHC_POD["tracking_datetime"]!=null && drEachHC_POD["tracking_datetime"]!=DBNull.Value)
							{
								Zipcode zipCodeHC_POD = new Zipcode();
								zipCodeHC_POD.Populate(strAppID, strEnterpriseID, strZipCodeHC_POD,dbCon,dbCmd);
								String strStateCodeHC_POD = zipCodeHC_POD.StateCode;

								if (strStateCodeHC_POD.Trim() != "")
								{
									String strTmpConsgmntNo = (String)drEach["CnsgBkgNo"];

									object tmpReHCDateTime_POD = DomesticShipmentMgrDAL.calEstHCDateTime((DateTime)drEachHC_POD["tracking_datetime"], strStateCodeHC_POD, 
										strTmpConsgmntNo.Trim(), iBookingNo.ToString().Trim(),
										strAppID, strEnterpriseID,dbCon,dbCmd);

									if( tmpReHCDateTime_POD!=null && tmpReHCDateTime_POD != DBNull.Value)
									{
										dtNewEstHC_POD = (DateTime)tmpReHCDateTime_POD;
										if((!dtNewEstHC_POD.GetType().Equals(System.Type.GetType("System.DBNull"))))
										{
											strBuilder = new StringBuilder();
											strBuilder.Append("update shipment with(rowlock) set ");
											strBuilder.Append("est_hc_return_datetime = ");
											strBuilder.Append(Utility.DateFormat(strAppID, strEnterpriseID, dtNewEstHC_POD, DTFormat.DateTime));
											strBuilder.Append(" where applicationid = '");
											strBuilder.Append(strAppID);
											strBuilder.Append("' and enterpriseid = '");
											strBuilder.Append(strEnterpriseID+"'");
											strBuilder.Append(" and consignment_no = ");

											if(Utility.IsNotDBNull(drEachHC_POD["CnsgBkgNo"]))
											{
												String strConsgmntNo = (String)drEachHC_POD["CnsgBkgNo"];
												strBuilder.Append("'");
												strBuilder.Append(strConsgmntNo);
												strBuilder.Append("'");
											}

											if (Utility.IsNotDBNull(drEachHC_POD["booking_no"]))
											{
												iBookingNo = (int)drEachHC_POD["booking_no"];
												strBuilder.Append(" and booking_no = "+iBookingNo);
											}

											strBuilder.Append(" and (isnull(return_pod_slip, 'N') <> 'N' or isnull(return_invoice_hc, 'N') <> 'N')");

											dbCmd.CommandText = strBuilder.ToString();
											iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
											Logger.LogTraceInfo("ShipmentUpdateMgrDAL","AddRecordShipmntTracking","INF008",iRowsAffected + " rows inserted into shipment_tracking table");
										}	
									}

									// 4.7.2 - Upon entry of a POD record:
									strInvoiceable = getInvablePODByCompareDate(dbCon, dbCmd,strAppID, strEnterpriseID, strConsgNo, iBookingNo, Convert.ToDateTime(drEachHC_POD["tracking_datetime"]));
									
								}
							}
						}	
						#endregion

						#region HCR
						else if(strStatusCode.Trim() == "HCR") 
						{
							DataRow drEachHC_HCR = dsShpmtUpdate.Tables[0].Rows[i];

							if(drEachHC_HCR["tracking_datetime"]!=null && drEachHC_HCR["tracking_datetime"]!=DBNull.Value)
							{
								strBuilder = new StringBuilder();
								strBuilder.Append("update shipment with(rowlock) set ");
								strBuilder.Append("act_hc_return_datetime = ");
								strBuilder.Append(Utility.DateFormat(strAppID, strEnterpriseID, (DateTime)drEachHC_HCR["tracking_datetime"], DTFormat.DateTime));
								strBuilder.Append(" where applicationid = '");
								strBuilder.Append(strAppID);
								strBuilder.Append("' and enterpriseid = '");
								strBuilder.Append(strEnterpriseID+"'");
								strBuilder.Append(" and consignment_no = ");

								if(Utility.IsNotDBNull(drEachHC_HCR["CnsgBkgNo"]))
								{
									String strConsgmntNo = (String)drEachHC_HCR["CnsgBkgNo"];
									strBuilder.Append("'");
									strBuilder.Append(strConsgmntNo);
									strBuilder.Append("'");
								}

								if (Utility.IsNotDBNull(drEachHC_HCR["booking_no"]))
								{
									iBookingNo = (int)drEachHC_HCR["booking_no"];
									strBuilder.Append(" and booking_no = "+iBookingNo);
								}

								dbCmd.CommandText = strBuilder.ToString();
								iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
								Logger.LogTraceInfo("ShipmentUpdateMgrDAL","AddRecordShipmntTracking","INF008",iRowsAffected + " rows inserted into shipment_tracking table");


								//4.7.4 Upon entry of any HCR Status code:
								//Shipment_Tracking.invoiceable <-- Status_Code.invoiceable
								dbCmd.CommandText = @"select invoiceable from Status_Code with(nolock) where  applicationid='"+strAppID+"' and enterpriseid='"+strEnterpriseID+"' and status_code='"+strStatusCode+"' ";
								object obj_inv_able = dbCon.ExecuteScalarInTransaction(dbCmd);
								if(obj_inv_able!=null) strInvoiceable=obj_inv_able.ToString();
							}
						}	
						//HC Return Task
						#endregion


						//insert into MF_Shipment_Tracking_Staging  BY TU 21/02/2010.
						drEach = dsShpmtUpdate.Tables[0].Rows[i];
						strBuilder = new StringBuilder();
						strBuilder.Append ("insert into MF_Shipment_Tracking_Staging with(rowlock) (applicationid,enterpriseid,");
						strBuilder.Append("consignment_no,booking_no,tracking_datetime,status_code,exception_code,person_incharge,");
						strBuilder.Append("location,consignee_Name,remark,last_userid,last_updated,invoiceable,surcharge,pkg_no,batch_no) values('");
						strBuilder.Append(strAppID);
						strBuilder.Append("','"+strEnterpriseID+"',");

						if(Utility.IsNotDBNull(drEach["CnsgBkgNo"]))
						{
							String strConsgmntNo = (String)drEach["CnsgBkgNo"];
							strBuilder.Append("'");
							strBuilder.Append(strConsgmntNo);
							strBuilder.Append("'");	
						}
						else
						{
							strBuilder.Append("null");
						}
						strBuilder.Append(",");
						strBuilder.Append(0);//iBookingNo);
						strBuilder.Append(",");
						string PkgNo="";
						if(Utility.IsNotDBNull(drEach["pkg_no"]))
						{
							if(drEach["pkg_no"].ToString()!="0")
							{
								PkgNo = (String)drEach["pkg_no"];
								//								strRemarks += "Pkg#" + Utility.ReplaceSingleQuote(strPkgNo) +" ";
								//								if(strBatch_no!="")
								//								{
								//									DataSet ds = new DataSet();
								//									ds= SWB_Emulator.SWB_Batch_Manifest_Read(strAppID,strEnterpriseID,strBatch_no);
								//									if(ds.Tables[0].Rows.Count>0)
								//										strRemarks += " B#" + strBatch_no + " R:"+ds.Tables[0].Rows[0]["Batch_ID"].ToString();
								//								}
							}
						}
						else
						{
							PkgNo = "0";
						}
						if(Utility.IsNotDBNull(drEach["tracking_datetime"]))
						{
							dtTrackingDtTime = (DateTime) drEach["tracking_datetime"];
							String strDate="";
							if(Utility.IsNotDBNull(drEach["pkg_no"]))
							{
								if(drEach["pkg_no"].ToString()!="0")
								{
									strDate = Utility.DateFormat(strAppID,strEnterpriseID,dtTrackingDtTime.AddSeconds(Convert.ToInt32(PkgNo)) ,DTFormat.DateLongTime);
							
								}
								else
								{
									strDate = Utility.DateFormat(strAppID,strEnterpriseID,dtTrackingDtTime.AddSeconds(Convert.ToInt32(0)) ,DTFormat.DateLongTime);

								}
							}
							else
							{
								strDate = Utility.DateFormat(strAppID,strEnterpriseID,dtTrackingDtTime.AddSeconds(Convert.ToInt32(0)) ,DTFormat.DateLongTime);

							}
							
							strBuilder.Append(strDate);
						}
						else
						{
							strBuilder.Append("null");
						}
						strBuilder.Append(",");
						if(strStatusCode != null && strStatusCode != "")
						{
							strBuilder.Append("upper('");
							strBuilder.Append(strStatusCode);
							strBuilder.Append("')");
						}
						else
						{
							strBuilder.Append("null");
						}
						strBuilder.Append(",");

						if(strExceptionCode != null && strExceptionCode != "")
						{
							strBuilder.Append("upper('");
							strBuilder.Append(strExceptionCode);
							strBuilder.Append("')");
						}
						else
						{
							strBuilder.Append("null");
						}
						strBuilder.Append(",");

						if(Utility.IsNotDBNull(drEach["person_incharge"]))
						{
							String strIncharge = (String)drEach["person_incharge"];
							strBuilder.Append("N'");
							strBuilder.Append(Utility.ReplaceSingleQuote(strIncharge));
							strBuilder.Append("'");
						}
						else
						{
							strBuilder.Append("null");
						}
						strBuilder.Append(",");

//BY X JAN 21 08
						
						if(Utility.IsNotDBNull(drEach["location"]))
						{
							String strLocation = (String)drEach["location"];
							strBuilder.Append("N'");
							strBuilder.Append(Utility.ReplaceSingleQuote(strLocation));
							strBuilder.Append("'");
						}
						else
						{
							strBuilder.Append("null");
						}
						strBuilder.Append(",");

						if(Utility.IsNotDBNull(drEach["consignee_Name"]))
						{
							String strConsignee_Name = (String)drEach["consignee_Name"];
							strBuilder.Append("N'");
							strBuilder.Append(Utility.ReplaceSingleQuote(strConsignee_Name));
							strBuilder.Append("'");
						}
						else
						{
							strBuilder.Append("null");
						}
						strBuilder.Append(",");
//END BY X JAN 21 08
						String strRemarks="";

						if(Utility.IsNotDBNull(drEach["pkg_no"]))
						{
							if(drEach["pkg_no"].ToString()!="0")
							{
								String strPkgNo = (String)drEach["pkg_no"];
								strRemarks += "Pkg#" + Utility.ReplaceSingleQuote(strPkgNo) +" ";
								if(strBatch_no!="")
								{
									DataSet ds = new DataSet();
									ds= SWB_Emulator.SWB_Batch_Manifest_Read(strAppID,strEnterpriseID,strBatch_no);
									if(ds.Tables[0].Rows.Count>0)
										strRemarks += " B#" + strBatch_no + " R:"+ds.Tables[0].Rows[0]["Batch_ID"].ToString();
								}
							}
						}

						if(Utility.IsNotDBNull(drEach["remarks"]))
						{
							strRemarks += (String)drEach["remarks"];
							strBuilder.Append("N'");
							strBuilder.Append(Utility.ReplaceSingleQuote(strRemarks));
							strBuilder.Append("'");
						}
						else
						{
							strBuilder.Append("null");
						}
						strBuilder.Append(",");

						if(userID != null)
						{
							strBuilder.Append("upper('");
							strBuilder.Append(userID);
							strBuilder.Append("')");
						}
						else
						{
							strBuilder.Append("null");
						}
						

						strBuilder.Append(",");
						String strLastUpdateDate="";
						if(Utility.IsNotDBNull(drEach["pkg_no"]))
						{
							if(drEach["pkg_no"].ToString()!="0")
							{
								strLastUpdateDate = Utility.DateFormat(strAppID,strEnterpriseID,dtCurrent.AddSeconds(Convert.ToInt32(PkgNo)) ,DTFormat.DateLongTime);
							}
							else
							{
								strLastUpdateDate = Utility.DateFormat(strAppID,strEnterpriseID,dtCurrent.AddSeconds(Convert.ToInt32(0)) ,DTFormat.DateLongTime);
							}
						}
						else
						{
							strLastUpdateDate = Utility.DateFormat(strAppID,strEnterpriseID,dtCurrent.AddSeconds(Convert.ToInt32(0)) ,DTFormat.DateLongTime);
						}
						strBuilder.Append(strLastUpdateDate);

						if(strInvoiceable == null)
							strBuilder.Append(",'N'"+","+decSurcharge);
						else
							strBuilder.Append(",'"+strInvoiceable+"'"+","+decSurcharge);
							
						strBuilder.Append(",");

						if(Utility.IsNotDBNull(drEach["pkg_no"]))
						{
							String strPkgNo = (String)drEach["pkg_no"];
							strBuilder.Append(Utility.ReplaceSingleQuote(strPkgNo));
						}
						else
						{
							strBuilder.Append("0");
						}
						if(strBatch_no!="")
						{
							strBuilder.Append(","+ strBatch_no);
						}
						else
						{
							strBuilder.Append(",null");
						}
						strBuilder.Append(")");

						dbCmd.CommandText = strBuilder.ToString();
						iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
						Logger.LogTraceInfo("ShipmentUpdateMgrDAL","AddRecordShipmntTracking","INF008",iRowsAffected + " rows inserted into shipment_tracking table");

						String strscanData = drEach["CnsgBkgNo"].ToString().Trim() + " " + drEach["pkg_no"].ToString().Trim();
//						if (strStatusCode.Trim()=="SIP"||strStatusCode.Trim()=="SOP" || strStatusCode.Trim()=="UNSIP" || strStatusCode.Trim()=="UNSOP"|| strStatusCode.Trim()=="CLS"||strStatusCode.Trim()=="UNCLS")
//						{
//							String strSQL ="SELECT * FROM MF_Raw_Scans	";
//							strSQL +=" WHERE scannedData = '" +strscanData+ "' and status_code='"+ strStatusCode +"' and location='"+ drEach["location"].ToString().Trim() +"' ";
//							strSQL +=" and Batch_no='" + strBatch_no + "'";
//							strSQL +=" and applicationid='" + strAppID + "'";
//							strSQL +=" and enterpriseid='" + strEnterpriseID + "'";
//							dbCmd.CommandText =strSQL;
//							DataSet DS = (DataSet)dbCon.ExecuteQueryInTransaction(dbCmd,com.common.DAL.ReturnType.DataSetType);
//							if(DS.Tables[0].Rows.Count==0)
//							{
//								strSQL="";
//								strSQL +=" INSERT INTO MF_Raw_Scans ";
//								strSQL +=" (applicationid ";
//								strSQL +=" ,enterpriseid";
//								strSQL +=" ,scannedData";
//								strSQL +=" ,status_code";
//								strSQL +=" ,Location";
//								strSQL +=" ,Batch_No";
//								strSQL +=" ,ScannedDT";
//								strSQL +=" ,UserID";
//								strSQL +=" )VALUES(";
//								strSQL +="'"+ strAppID+"'";
//								strSQL +=",'"+ strEnterpriseID+"'";
//								strSQL +=",'"+ strscanData+"'";
//								strSQL +=",'"+ strStatusCode+"'";
//								strSQL +=",'"+ drEach["location"].ToString().Trim() +"'";
//								strSQL +=",'"+ strBatch_no +"'";
//								strSQL +=","+Utility.DateFormat(strAppID,strEnterpriseID,dtTrackingDtTimeInput,DTFormat.DateTime);
//								strSQL +=",'"+ userID+"')";
//								dbCmd.CommandText = strSQL.ToString();
//								iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
//							}
//							else
//							{
//								strSQL +=" INSERT MF_Raw_Scans_Dup ";
//								strSQL +=" (applicationid ";
//								strSQL +=" ,enterpriseid";
//								strSQL +=" ,scannedData";
//								strSQL +=" ,status_code";
//								strSQL +=" ,Location";
//								strSQL +=" ,Batch_No";
//								strSQL +=" ,ScannedDT";
//								strSQL +=" ,UserID";
//								strSQL +=" )VALUES(";
//								strSQL +="'"+ strAppID+"'";
//								strSQL +=",'"+ strEnterpriseID+"'";
//								strSQL +=",'"+ strscanData+"'";
//								strSQL +=",'"+ strStatusCode+"'";
//								strSQL +=",'"+ drEach["location"].ToString().Trim() +"'";
//								strSQL +=",'"+ strBatch_no +"'";
//								strSQL +=","+Utility.DateFormat(strAppID,strEnterpriseID,dtTrackingDtTimeInput,DTFormat.DateTime);
//								strSQL +=",'"+ userID+"')";
//								dbCmd.CommandText = strSQL.ToString();
//								iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
//							}
//
//						}
						#endregion

						#region New code by Aoo 26-04-2010
						
//						StringBuilder stbSql = new StringBuilder();
//						stbSql.Append(" EXEC AddRecordShipmentTracking ");
//						stbSql.Append(" '"+strDmstPickup+"', "); // @DmsPickup VARCHAR(50) = NULL,
//						stbSql.Append(" '"+strAppID+"', "); // @ApplicationID VARCHAR(10) = NULL,
//						stbSql.Append(" '"+strEnterpriseID+"', "); // @EnterpriseID VARCHAR(10) = NULL,
//						stbSql.Append(" '"+iBookingNo+"', "); // @Booking_NO VARCHAR(20) = NULL,
//						stbSql.Append(" '"+strConsgtNo+"', "); // @Consignment_NO VARCHAR(100) = NULL,
//						stbSql.Append(" '"+strStatusCode+"', "); // @StatusCode VARCHAR(50) = NULL,
//						stbSql.Append(Utility.DateFormat(strAppID,strEnterpriseID,(DateTime)drEach["tracking_datetime"],DTFormat.DateTime)+ ", "); // @Tracking_datetime DATETIME = NULL,
//						stbSql.Append(" '"+strExceptionCode+"', "); // @Exception_code VARCHAR(50) = NULL,
//						stbSql.Append(" N'"+drEach["person_incharge"].ToString()+"', "); // @Person_incharge VARCHAR(100) = NULL,
//						stbSql.Append(" N'"+drEach["remarks"].ToString()+"', "); // @Remarks VARCHAR(200) = NULL,
//						stbSql.Append(" N'"+drEach["last_userid"].ToString()+"', "); // @Last_userid VARCHAR(40) = NULL,
//						stbSql.Append(" '"+drEach["last_updated"].ToString()+"', "); // @Last_updated DATETIME = NULL,
//						stbSql.Append(" '"+drEach["ZipCode"].ToString()+"', "); // @ZipCode VARCHAR(5) = NULL,
//						stbSql.Append(" '"+drEach["location"].ToString()+"', "); // @Location VARCHAR(10) = NULL,
//						stbSql.Append(" N'"+drEach["consignee_Name"].ToString()+"', "); // @Consignee_name VARCHAR(100) = NULL,
//						stbSql.Append(" '"+userID+"', "); // @UserID  VARCHAR(30) = NULL,
//						stbSql.Append(Utility.DateFormat(strAppID,strEnterpriseID,dtCurrent,DTFormat.DateTime));  // @UpdateDate DATETIME = NULL
						//dbCmd = dbCon.CreateCommand(stbSql.ToString() ,CommandType.Text);

						
//						dbCmd.Connection = conApp;
//						dbCmd.Transaction = transactionApp;
//						iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
//						Logger.LogTraceInfo("ShipmentUpdateMgrDAL","AddRecordShipmntTracking","INF008",iRowsAffected + " rows inserted into shipment_tracking table");
				#endregion
					}
					#endregion

					// Insert to MF_Shipment_Tracking_Staging 

				}
				else if(strDmstPickup.Equals("Pickup"))
				{
					#region Pickup
					cnt = dsShpmtUpdate.Tables[0].Rows.Count;
					i = 0;
					drEach = null;					
					DateTime dtTrackingDtTime;
					String strPUP = TIESUtility.getActualPickupStatusCode(strAppID,strEnterpriseID);
					for(i=0;i<cnt;i++)
					{
						drEach = dsShpmtUpdate.Tables[0].Rows[i];
						//insert into dispatch tracking
						drEach = dsShpmtUpdate.Tables[0].Rows[i];
						strBuilder = new StringBuilder();
						strBuilder.Append ("insert into dispatch_tracking with(rowlock) (applicationid,enterpriseid,");
						strBuilder.Append("booking_no,tracking_datetime,status_code,exception_code,person_incharge,");
						strBuilder.Append("location,consignee_Name,remark,last_userid,last_updated) values('");
						strBuilder.Append(strAppID);
						strBuilder.Append("','"+strEnterpriseID+"',");


						if(Utility.IsNotDBNull(drEach["CnsgBkgNo"]))
						{
							iBookingNo = Convert.ToInt32(drEach["CnsgBkgNo"]);
							strBuilder.Append(iBookingNo);
						}
						else
						{
							strBuilder.Append("null");
						}
						strBuilder.Append(",");

						if(Utility.IsNotDBNull(drEach["tracking_datetime"]))
						{
							dtTrackingDtTime = (DateTime) drEach["tracking_datetime"];
							strBuilder.Append(Utility.DateFormat(strAppID,strEnterpriseID,dtTrackingDtTime,DTFormat.DateTime));
						}
						else
						{
							strBuilder.Append("null");
						}
						strBuilder.Append(",");
						if(strStatusCode != null)
						{
							strBuilder.Append("upper('");
							strBuilder.Append(strStatusCode);
							strBuilder.Append("')");
						}
						else
						{
							strBuilder.Append("null");
						}
						strBuilder.Append(",");

						if(strExceptionCode != null)
						{
							strBuilder.Append("upper('");
							strBuilder.Append(strExceptionCode);
							strBuilder.Append("')");
						}
						else
						{
							strBuilder.Append("null");
						}
						strBuilder.Append(",");

						if(Utility.IsNotDBNull(drEach["person_incharge"]))
						{
							String strIncharge = (String)drEach["person_incharge"];
							strBuilder.Append("N'");
							strBuilder.Append(Utility.ReplaceSingleQuote(strIncharge));
							strBuilder.Append("'");
						}
						else
						{
							strBuilder.Append("null");
						}


						strBuilder.Append(",");


//BY X JAN 21 08
						if(Utility.IsNotDBNull(drEach["location"]))
						{
							String strLocation = (String)drEach["location"];
							strBuilder.Append("N'");
							strBuilder.Append(Utility.ReplaceSingleQuote(strLocation));
							strBuilder.Append("'");
						}
						else
						{
							strBuilder.Append("null");
						}
						strBuilder.Append(",");

						if(Utility.IsNotDBNull(drEach["consignee_Name"]))
						{
							String strConsignee_Name = (String)drEach["consignee_Name"];
							strBuilder.Append("N'");
							strBuilder.Append(Utility.ReplaceSingleQuote(strConsignee_Name));
							strBuilder.Append("'");
						}
						else
						{
							strBuilder.Append("null");
						}
						strBuilder.Append(",");
//END BY X JAN 21 08

						if(Utility.IsNotDBNull(drEach["remarks"]))
						{
							String strRemarks = (String)drEach["remarks"];
							strBuilder.Append("N'");
							strBuilder.Append(Utility.ReplaceSingleQuote(strRemarks));
							strBuilder.Append("'");
						}
						else
						{
							strBuilder.Append("null");
						}
						strBuilder.Append(",");

						if(userID != null)
						{
							strBuilder.Append("upper('");
							strBuilder.Append(userID);
							strBuilder.Append("')");
						}
						else
						{
							strBuilder.Append("null");
						}
						strBuilder.Append(",");
						strBuilder.Append(Utility.DateFormat(strAppID,strEnterpriseID,dtCurrent,DTFormat.DateTime));
						strBuilder.Append(")");

							
						dbCmd.CommandText = strBuilder.ToString();
						iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
						Logger.LogTraceInfo("ShipmentUpdateMgrDAL","AddRecordShipmntTracking-dispatch tracking","INF010",iRowsAffected + " rows inserted into dispatch_tracking table");

					//Update the pickup request table status code
					//get status code 
						DataSet dtSC=null;
						String strStatusCodeSC=null;
						String strExceptionCodeSC=null;
						DateTime dtTrackingDtTimeSC=System.DateTime.Now;
						if (Utility.IsNotDBNull(drEach["CnsgBkgNo"]))
						{
							iBookingNo = Convert.ToInt32(drEach["CnsgBkgNo"]);
						}
						dtSC=getLatestStatusCodePickup(strAppID,strEnterpriseID,iBookingNo,dbCmd,dbCon);
						if(dtSC.Tables[0].Rows.Count>0)
						{
							DataRow	drEachSC = dtSC.Tables[0].Rows[0];
							
							if(Utility.IsNotDBNull(drEachSC["tracking_datetime"]))
							{
								dtTrackingDtTimeSC = (DateTime) drEachSC["tracking_datetime"];
								if(Utility.IsNotDBNull(drEach["tracking_datetime"]))
								{
									dtTrackingDtTime = (DateTime) drEach["tracking_datetime"];
									if(dtTrackingDtTimeSC.CompareTo(dtTrackingDtTime) > 0)
									{
										if(Utility.IsNotDBNull(drEachSC["status_code"]))
										{
											strStatusCodeSC=(String)drEachSC["status_code"];
										}										
										if(Utility.IsNotDBNull(drEachSC["exception_code"]))
										{
											strExceptionCodeSC=(String)drEachSC["exception_code"];
										}
										
									}
									else
									{
										dtTrackingDtTimeSC = (DateTime) drEach["tracking_datetime"];
										strStatusCodeSC=strStatusCode;
										strExceptionCodeSC=strExceptionCode;
									}
								}
								
							}
						}
						else
						{
							dtTrackingDtTimeSC = (DateTime) drEach["tracking_datetime"];
							strStatusCodeSC=strStatusCode;
							strExceptionCodeSC=strExceptionCode;
						}
					//ends get status code


						strBuilder = new StringBuilder();
						strBuilder.Append(" update  pickup_request with(rowlock) set latest_status_code = ");
						if(strStatusCodeSC != null)
						{
							strBuilder.Append("'");
							strBuilder.Append(strStatusCodeSC);
							strBuilder.Append("'");
						}
						else
						{
							strBuilder.Append("null");
						}
						strBuilder.Append(",");

						strBuilder.Append("latest_exception_code = ");
						if(strExceptionCodeSC != null)
						{
							strBuilder.Append("'");
							strBuilder.Append(strExceptionCodeSC);
							strBuilder.Append("'");
						}
						else
						{
							strBuilder.Append("null");
						}
						strBuilder.Append(",");

						strBuilder.Append(" latest_status_datetime = ");
						
//						if(Utility.IsNotDBNull(drEach["tracking_datetime"]))
//						{
//							DateTime dtTrackingDtTime = (DateTime) drEach["tracking_datetime"];
							strBuilder.Append(Utility.DateFormat(strAppID,strEnterpriseID,dtTrackingDtTimeSC,DTFormat.DateTime));
//						}
//						else
//						{
//							strBuilder.Append("null");
//						}	

						strBuilder.Append(" where applicationid = '");
						strBuilder.Append(strAppID);
						strBuilder.Append("' and enterpriseid = '");
						strBuilder.Append(strEnterpriseID+"'");
			
						strBuilder.Append(" and booking_no = ");
						if(Utility.IsNotDBNull(drEach["CnsgBkgNo"]))
						{
							int ibookingNo = Convert.ToInt32(drEach["CnsgBkgNo"]);
							strBuilder.Append(ibookingNo);
						}
						

					//	strBuilder.Append(" and ((latest_status_datetime is null) or (latest_status_datetime <= ");
							strBuilder.Append(" and ((latest_status_datetime is not null) or(latest_status_datetime <= ");
						
						if(Utility.IsNotDBNull(drEach["tracking_datetime"]))
						{
							dtTrackingDtTime = (DateTime) drEach["tracking_datetime"];
							strBuilder.Append(Utility.DateFormat(strAppID,strEnterpriseID,dtTrackingDtTime,DTFormat.DateTime)+"))");
						}
						else
						{
							strBuilder.Append("null");
						}


						dbCmd.CommandText = strBuilder.ToString();
						iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
						Logger.LogTraceInfo("ShipmentUpdateMgrDAL","AddRecordShipmntTracking","INF009",iRowsAffected + " rows updated into pickuprequest table");
						
		
					
			//update actual delivery date
						StringBuilder strBldr = new StringBuilder();	
					
	
						if(Utility.IsNotDBNull(drEach["CnsgBkgNo"]))
						{
							int ibookingNo = Convert.ToInt32(drEach["CnsgBkgNo"]);//tracking_datetime
							//String strActDeliveryDateTime = InsertActualPickupDateTime(strAppID,strEnterpriseID,iBookingNo,strPUP);
							String strActDeliveryDateTime = getActualPickupDateTime(strAppID,strEnterpriseID,ibookingNo,strPUP,dbCmd,dbCon);
							if(strActDeliveryDateTime != null)
							{
								DateTime dt = DateTime.ParseExact(strActDeliveryDateTime,"dd/MM/yyyy HH:mm",null);
										
								strBldr.Append("Update Pickup_Request with(rowlock) set act_pickup_datetime = "+Utility.DateFormat(strAppID,strEnterpriseID,dt,DTFormat.DateTime)+" where ");
								strBldr.Append(" applicationid = '");
								strBldr.Append(strAppID);
								strBldr.Append("' and enterpriseid = '");
								strBldr.Append(strEnterpriseID);
								strBldr.Append("' and booking_no = ");
								strBldr.Append(ibookingNo);	
							}
							else
							{
								DateTime dt=(DateTime)drEach["tracking_datetime"];
								strBldr.Append("Update Pickup_Request with(rowlock) set act_pickup_datetime = "+Utility.DateFormat(strAppID,strEnterpriseID,dt,DTFormat.DateTime)+" where ");
								strBldr.Append(" applicationid = '");
								strBldr.Append(strAppID);
								strBldr.Append("' and enterpriseid = '");
								strBldr.Append(strEnterpriseID);
								strBldr.Append("' and booking_no = ");
								strBldr.Append(ibookingNo);
							}
						}

						dbCmd.CommandText = strBldr.ToString();
						iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);	
						Logger.LogTraceInfo("ShipmentUpdateMgrDAL","AddRecordShipmntTracking-actual pickup","INF010",iRowsAffected + " rows inserted into dispatch_tracking table");
						//ends update actual delivery date
					
					}
					#endregion
				}
				transactionApp.Commit();
				Logger.LogTraceInfo("ShipmentUpdateMgrDAL","AddRecordShipmntTracking","INF011","App db insert transaction committed.");

			}
			catch(ApplicationException appException)
			{
				try
				{
					transactionApp.Rollback();
					Logger.LogTraceInfo("ShipmentUpdateMgrDAL","AddRecordShipmntTracking","ERR012","App db insert transaction rolled back.");

				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("ShipmentUpdateMgrDAL","AddRecordShipmntTracking","ERR007","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}


				Logger.LogTraceError("ShipmentUpdateMgrDAL","AddRecordShipmntTracking","ERR008","Insert Transaction Failed : "+ appException.Message.ToString());
				throw new ApplicationException("Error "+appException.Message,appException);
			}
			catch(Exception exception)
			{
				try
				{
					transactionApp.Rollback();
					Logger.LogTraceInfo("ShipmentUpdateMgrDAL","AddRecordShipmntTracking","ERR015","App db insert transaction rolled back.");

				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("ShipmentUpdateMgrDAL","AddRecordShipmntTracking","ERR009","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}

				Logger.LogTraceError("ShipmentUpdateMgrDAL","AddRecordShipmntTracking","ERR010","Insert Transaction Failed : "+ exception.Message.ToString());
				throw new ApplicationException("Error "+exception.Message,exception);
			}
			finally
			{
				if(conApp != null)
				{
					conApp.Close();
					conApp.Dispose();
				}
			}

		}

		/// <summary>
		/// This method gets booking no. for the particular consignment from shipment table.
		/// </summary>
		/// <param name="strAppID">Application ID</param>
		/// <param name="strEnterpriseID">Enterprise ID</param>
		/// <param name="ConsgmntNo">Consignment no.</param>
		/// <returns>int (booking number)</returns>
		public static int GetShipmentBookingNo(String strAppID,String strEnterpriseID,String ConsgmntNo)
		{
			IDbCommand dbCmd = null;
			StringBuilder strBuild = null;
			int iBookingNo = 0;

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("ShipmentUpdateMgrDAL","GetShipmentBookingNo","ERR001","DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null",null);
			}

			strBuild =  new StringBuilder();
			strBuild.Append("select * from shipment with(nolock) where applicationid = '");
			strBuild.Append(strAppID+"'");
			strBuild.Append(" and enterpriseid = '");
			strBuild.Append(strEnterpriseID+"'");
			strBuild.Append(" and consignment_no = '");
			strBuild.Append(ConsgmntNo+"'");
			
			try
			{
				dbCmd = dbCon.CreateCommand(strBuild.ToString(),CommandType.Text);
				DataSet dsShipmnt =(DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
				if (dsShipmnt.Tables[0].Rows.Count > 0)
				{
					DataRow drEach = dsShipmnt.Tables[0].Rows[0];
					if(Utility.IsNotDBNull(drEach["booking_no"]))
					{
						iBookingNo = Convert.ToInt32(drEach["booking_no"]);
					}
				}
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("ShipmentUpdateMgrDAL","GetShipmentBookingNo","ERR002","Error : "+ appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ",appException);
			}
			catch(Exception exception)
			{
				Logger.LogTraceError("ShipmentUpdateMgrDAL","GetShipmentBookingNo","ERR003","Error : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}
			return iBookingNo;
		}

		public static bool IsDupStatusPODEX(String strAppID,String strEnterpriseID,String ConsgmntNo,String status_code)
		{
			IDbCommand dbCmd = null;
			StringBuilder strBuild = null;
			
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("ShipmentUpdateMgrDAL","GetShipmentBookingNo","ERR001","DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null",null);
			}

			strBuild =  new StringBuilder();
			strBuild.Append(" select consignment_no from Shipment_Tracking with(nolock) ");
			strBuild.Append(" where status_code = '" + status_code + "' ");
			strBuild.Append(" and enterpriseid = '" + strEnterpriseID + "' ");
			strBuild.Append(" and applicationid = '" + strAppID + "' ");
			strBuild.Append(" and consignment_no = '" + ConsgmntNo + "' ");
			strBuild.Append(" and isnull(deleted,'') <> 'Y'");
			
			try
			{
				dbCmd = dbCon.CreateCommand(strBuild.ToString(),CommandType.Text);
				DataSet dsShipmnt =(DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
				if (dsShipmnt.Tables[0].Rows.Count > 0)
				{
					return false;
				}

				strBuild =  new StringBuilder();
				strBuild.Append(" select consignment_no from MF_Shipment_Tracking_Staging with(nolock) ");
				strBuild.Append(" where status_code = '" + status_code + "' ");
				strBuild.Append(" and enterpriseid = '" + strEnterpriseID + "' ");
				strBuild.Append(" and applicationid = '" + strAppID + "' ");
				strBuild.Append(" and consignment_no = '" + ConsgmntNo + "' AND isnull(Process_Flag,'N') = 'N' ");

				dbCmd = dbCon.CreateCommand(strBuild.ToString(),CommandType.Text);
				dsShipmnt =(DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
				if (dsShipmnt.Tables[0].Rows.Count > 0)
				{
					return false;
				}

				strBuild =  new StringBuilder();
				strBuild.Append(" select consignment_no from MF_Shipment_Tracking_Staging with(nolock) ");
				strBuild.Append(" where status_code = '" + status_code + "' ");
				strBuild.Append(" and enterpriseid = '" + strEnterpriseID + "' ");
				strBuild.Append(" and applicationid = '" + strAppID + "' ");
				strBuild.Append(" and consignment_no = '" + ConsgmntNo + "' ");

				dbCmd = dbCon.CreateCommand(strBuild.ToString(),CommandType.Text);
				DataSet dsShipmnt2 =(DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
				
				strBuild =  new StringBuilder();
				strBuild.Append(" select consignment_no from Shipment_Tracking with(nolock) ");
				strBuild.Append(" where status_code = '" + status_code + "' ");
				strBuild.Append(" and enterpriseid = '" + strEnterpriseID + "' ");
				strBuild.Append(" and applicationid = '" + strAppID + "' ");
				strBuild.Append(" and consignment_no = '" + ConsgmntNo + "' ");
				strBuild.Append(" and isnull(deleted,'N') = 'Y'");

				dbCmd = dbCon.CreateCommand(strBuild.ToString(),CommandType.Text);
				dsShipmnt =(DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);

				if (dsShipmnt.Tables[0].Rows.Count < dsShipmnt2.Tables[0].Rows.Count)
				{
					return false;
				}
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("ShipmentUpdateMgrDAL","GetShipmentBookingNo","ERR002","Error : "+ appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ",appException);
			}
			catch(Exception exception)
			{
				Logger.LogTraceError("ShipmentUpdateMgrDAL","GetShipmentBookingNo","ERR003","Error : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}
			return true;
		}
		public static DateTime GetShipmentEstDeliveryDate(String strAppID,String strEnterpriseID,String ConsgmntNo)
		{
			IDbCommand dbCmd = null;
			StringBuilder strBuild = null;
			DateTime dtEstDeliveryDate=new DateTime(1111,11,11);

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("ShipmentUpdateMgrDAL","GetShipmentBookingNo","ERR001","DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null",null);
			}

			strBuild =  new StringBuilder();
			strBuild.Append("select * from shipment with(nolock) where applicationid = '");
			strBuild.Append(strAppID+"'");
			strBuild.Append(" and enterpriseid = '");
			strBuild.Append(strEnterpriseID+"'");
			strBuild.Append(" and consignment_no = '");
			strBuild.Append(ConsgmntNo+"'");
			
			try
			{
				dbCmd = dbCon.CreateCommand(strBuild.ToString(),CommandType.Text);
				DataSet dsShipmnt =(DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
				if (dsShipmnt.Tables[0].Rows.Count > 0)
				{
					DataRow drEach = dsShipmnt.Tables[0].Rows[0];
					if(Utility.IsNotDBNull(drEach["est_delivery_datetime"]))
					{
						dtEstDeliveryDate =(DateTime)drEach["est_delivery_datetime"];
					}
				}
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("ShipmentUpdateMgrDAL","GetShipmentBookingNo","ERR002","Error : "+ appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ",appException);
			}
			catch(Exception exception)
			{
				Logger.LogTraceError("ShipmentUpdateMgrDAL","GetShipmentBookingNo","ERR003","Error : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}
			return dtEstDeliveryDate;
		}

		public static String GetShipmentLatest_Status_Code(String strAppID,String strEnterpriseID,String ConsgmntNo)
		{
			IDbCommand dbCmd = null;
			StringBuilder strBuild = null;
			String LastStatusCode="";
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("ShipmentUpdateMgrDAL","GetShipmentBookingNo","ERR001","DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null",null);
			}

			strBuild =  new StringBuilder();
			strBuild.Append("select status_code from shipment_tracking with(nolock) where applicationid = '");
			strBuild.Append(strAppID+"'");
			strBuild.Append(" and enterpriseid = '");
			strBuild.Append(strEnterpriseID+"'");
			strBuild.Append(" and consignment_no = '");
			strBuild.Append(ConsgmntNo+"' order by Tracking_dateTime DESC");
			
			try
			{
				dbCmd = dbCon.CreateCommand(strBuild.ToString(),CommandType.Text);
				DataSet dsShipmnt =(DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
				if (dsShipmnt.Tables[0].Rows.Count > 0)
				{
					DataRow drEach = dsShipmnt.Tables[0].Rows[0];
					if(Utility.IsNotDBNull(drEach["status_code"]))
					{
						LastStatusCode =(String)drEach["status_code"];
					}
				}
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("ShipmentUpdateMgrDAL","GetShipmentBookingNo","ERR002","Error : "+ appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ",appException);
			}
			catch(Exception exception)
			{
				Logger.LogTraceError("ShipmentUpdateMgrDAL","GetShipmentBookingNo","ERR003","Error : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}
			return LastStatusCode.Trim();
		}

		public static bool CheckPODStatus(String strAppID,String strEnterpriseID,String ConsgmntNo)
		{
			IDbCommand dbCmd = null;
			StringBuilder strBuild = null;
			bool chkStatus = false;

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("ShipmentUpdateMgrDAL","CheckPODStatus","ERR001","DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null",null);
			}

			strBuild =  new StringBuilder();
			strBuild.Append("select status_code from shipment_tracking with(nolock) where applicationid = '");
			strBuild.Append(strAppID+"'");
			strBuild.Append(" and enterpriseid = '");
			strBuild.Append(strEnterpriseID+"'");
			strBuild.Append(" and consignment_no = '");
			strBuild.Append(ConsgmntNo+"' and ISNULL(deleted, 'N') <> 'Y' order by Tracking_dateTime DESC");
			
			try
			{
				dbCmd = dbCon.CreateCommand(strBuild.ToString(),CommandType.Text);
				DataSet dsShipmnt =(DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
				if (dsShipmnt.Tables[0].Rows.Count > 0)
				{
					foreach(DataRow drEach in dsShipmnt.Tables[0].Rows)
					{
						if(drEach["status_code"].ToString().Trim() == "POD")
						{
							chkStatus = true;
						}
					}
				}
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("ShipmentUpdateMgrDAL","GetShipmentBookingNo","ERR002","Error : "+ appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ",appException);
			}
			catch(Exception exception)
			{
				Logger.LogTraceError("ShipmentUpdateMgrDAL","GetShipmentBookingNo","ERR003","Error : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}
			return chkStatus;
		}

		public static DateTime GetShipmentBookingDateTime(String strAppID,String strEnterpriseID,String ConsgmntNo)
		{
			IDbCommand dbCmd = null;
			StringBuilder strBuild = null;
			DateTime dtBookingDateTime=new DateTime(1111,11,11);

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("ShipmentUpdateMgrDAL","GetShipmentBookingNo","ERR001","DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null",null);
			}

			strBuild =  new StringBuilder();
			strBuild.Append("select * from shipment with(nolock) where applicationid = '");
			strBuild.Append(strAppID+"'");
			strBuild.Append(" and enterpriseid = '");
			strBuild.Append(strEnterpriseID+"'");
			strBuild.Append(" and consignment_no = '");
			strBuild.Append(ConsgmntNo+"'");
			
			try
			{
				dbCmd = dbCon.CreateCommand(strBuild.ToString(),CommandType.Text);
				DataSet dsShipmnt =(DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
				if (dsShipmnt.Tables[0].Rows.Count > 0)
				{
					DataRow drEach = dsShipmnt.Tables[0].Rows[0];
					if(Utility.IsNotDBNull(drEach["booking_datetime"]))
					{
						dtBookingDateTime =(DateTime)drEach["booking_datetime"];
					}
				}
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("ShipmentUpdateMgrDAL","GetShipmentBookingNo","ERR002","Error : "+ appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ",appException);
			}
			catch(Exception exception)
			{
				Logger.LogTraceError("ShipmentUpdateMgrDAL","GetShipmentBookingNo","ERR003","Error : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}
			return dtBookingDateTime;
		}


		/// <summary>
		/// This method gets an empty dataset for the Shipment Update by route method
		/// </summary>
		/// <returns>An empty dataset is returned</returns>
		public static DataSet GetEmptyShipUpdRouteDS()
		{
			DataTable dtShipmntRoute = new DataTable();
			dtShipmntRoute.Columns.Add(new DataColumn("consignment_no",typeof(string)));
			dtShipmntRoute.Columns.Add(new DataColumn("route_code",typeof(string)));
			dtShipmntRoute.Columns.Add(new DataColumn("origin_state_code",typeof(string)));
			dtShipmntRoute.Columns.Add(new DataColumn("destination_state_code",typeof(string)));
			DataSet dsDhpmntRoute = new DataSet();
			dsDhpmntRoute.Tables.Add(dtShipmntRoute);
			return dsDhpmntRoute;
		}
		
		/// <summary>
		/// This method inserts a row in the dataset returned by the method GetEmptyShipUpdRouteDS().
		/// </summary>
		/// <param name="dsShipUpd">Dataset</param>
		public static void AddRowInShipUpdRouteDS(DataSet dsShipUpd)
		{
			DataRow drNew = dsShipUpd.Tables[0].NewRow();
			dsShipUpd.Tables[0].Rows.Add(drNew);
		}

		/// <summary>
		/// This method returns empty dataset for the 
		/// </summary>
		/// <returns></returns>
		public static DataSet GetEmptyRouteDetailDS()
		{
			DataTable dtShipmntRoute = new DataTable();
			dtShipmntRoute.Columns.Add(new DataColumn("route_code",typeof(string)));
			dtShipmntRoute.Columns.Add(new DataColumn("route_description",typeof(string)));
			dtShipmntRoute.Columns.Add(new DataColumn("departure_date",typeof(DateTime)));
			dtShipmntRoute.Columns.Add(new DataColumn("flight_vehicle_no",typeof(string)));
			dtShipmntRoute.Columns.Add(new DataColumn("awb_driver_name",typeof(string)));
			dtShipmntRoute.Columns.Add(new DataColumn("origin_state_code",typeof(string)));
			dtShipmntRoute.Columns.Add(new DataColumn("destination_state_code",typeof(string)));
			DataSet dsDhpmntRoute = new DataSet();
			dsDhpmntRoute.Tables.Add(dtShipmntRoute);
			return dsDhpmntRoute;
		}
		/// <summary>
		/// This method adds a record to the dataset returned by the method GetEmptyRouteDetailDS().
		/// </summary>
		/// <param name="dsRouteDetail">Dataset</param>
		public static void AddNewRowInRouteDetailDS(DataSet dsRouteDetail)
		{
			DataRow drNew = dsRouteDetail.Tables[0].NewRow();
			dsRouteDetail.Tables[0].Rows.Add(drNew);
		}
		/// <summary>
		/// This method gets details from delivery_manifest_detail a,route_code b,delivery_manifest tables.
		/// </summary>
		/// <param name="strAppID">Application ID</param>
		/// <param name="strEnterpriseID">Enterprise ID</param>
		/// <param name="dtDeparture">Date of Departure</param>
		/// <param name="strRouteCode">Route Code</param>
		/// <param name="strPathCode">Path Code</param>
		/// <returns></returns>
		public static DataSet GetDlvryManifstDtl(String strAppID,String strEnterpriseID,DateTime dtDeparture,String strRouteCode,String strPathCode)
		{
			IDbCommand dbCmd = null;
			StringBuilder strBuild = null;
			DataSet dsDlvryDetail = null;

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("ShipmentUpdateMgrDAL","GetDlvryManifstDtl","ERR001","DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null",null);
			}

			strBuild =  new StringBuilder();
			strBuild.Append("select a.route_code,b.route_description,a.delivery_manifest_datetime,a.flight_vehicle_no,c.awb_driver_name,a.origin_state_code,a.destination_state_code from delivery_manifest_detail a with(nolock),route_code b with(nolock),delivery_manifest c with(nolock) where a.applicationid = '");
			strBuild.Append(strAppID+"'");
			strBuild.Append(" and a.enterpriseid = '");
			strBuild.Append(strEnterpriseID+"'");
			if((strPathCode != null) && (strPathCode != ""))
			{
				strBuild.Append(" and a.path_code like '%");
				strBuild.Append(Utility.ReplaceSingleQuote(strPathCode));
				strBuild.Append("%'");
			}
			if(dtDeparture.ToString("yyyy") != "1111")
			{
				strBuild.Append(" and a.delivery_manifest_datetime <= ");
				String strDate = Utility.DateFormat(strAppID,strEnterpriseID,dtDeparture,DTFormat.Date);
				strBuild.Append(strDate.Substring(0,strDate.Length-1));
				strBuild.Append(" "+"23:59'");
			}

			if((strRouteCode != null) && (strRouteCode != ""))
			{
				strBuild.Append(" and a.route_code like '%");
				strBuild.Append(Utility.ReplaceSingleQuote(strRouteCode));
				strBuild.Append("%'");
			}
			strBuild.Append(" and b.applicationid ='");
			strBuild.Append(strAppID+"'");
			strBuild.Append(" and b.enterpriseid = '");
			strBuild.Append(strEnterpriseID+"'");
			strBuild.Append(" and b.route_code = a.route_code");
			strBuild.Append(" and b.path_code = a.path_code");

			strBuild.Append(" and c.applicationid ='");
			strBuild.Append(strAppID+"'");
			strBuild.Append(" and c.enterpriseid = '");
			strBuild.Append(strEnterpriseID+"'");
			strBuild.Append(" and c.path_code = b.path_code");
			strBuild.Append(" and c.flight_vehicle_no =  a.flight_vehicle_no ");
			strBuild.Append(" and c.delivery_manifest_datetime =  a.delivery_manifest_datetime ");

			try
			{
				dbCmd = dbCon.CreateCommand(strBuild.ToString(),CommandType.Text);
				dsDlvryDetail =(DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("ShipmentUpdateMgrDAL","GetDlvryManifstDtl","ERR002","Error : "+ appException.Message.ToString());
				throw new ApplicationException("Error "+appException.Message,appException);
			}
			catch(Exception exception)
			{
				Logger.LogTraceError("ShipmentUpdateMgrDAL","GetDlvryManifstDtl","ERR003","Error : "+ exception.Message.ToString());
				throw new ApplicationException("Error  "+exception.Message,exception);
			}
			return dsDlvryDetail;

		}


		/// <summary>
		/// This method returns all the Domestic Shipments Consignment for which invoicing is not done.
		/// </summary>
		/// <param name="a_strAppID">Application ID</param>
		/// <param name="a_strEnterpriseID">Enterprise ID</param>
		/// <returns>Dataset</returns>
				    
		public static DataSet GetAllDomesticCongn(String strAppID,String strEnterpriseID, getConsignmentParam CongnParam)
		{
			DataSet dsCongnmnt = null;
			IDbCommand dbCmd = null;
			
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("ShipmentUpdateMgrDAL","GetAllDomesticCongn","ERR001","DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null",null);
			}

			StringBuilder strBuild =  new StringBuilder();
			strBuild.Append("select A.consignment_no AS consignment_no, A.origin_state_code AS origin_state_code, A.destination_state_code AS destination_state_code, A.route_code AS route_code");
			strBuild.Append(" FROM delivery_manifest_detail A with(nolock), shipment B with(nolock) where A.applicationid = '");
			strBuild.Append(strAppID+"'");
			strBuild.Append(" and A.enterpriseid = '");
			strBuild.Append(strEnterpriseID+"'");
			strBuild.Append(" and A.applicationid = B.applicationid");
			strBuild.Append(" and A.enterpriseid = B.enterpriseid");
			strBuild.Append(" and A.consignment_no = B.consignment_no ");
			strBuild.Append(" and (B.invoice_no is null or B.invoice_no = '')");
			strBuild.Append(" and A.path_code = '");
			strBuild.Append(CongnParam.strDeliveryPathCode);
			strBuild.Append("' and A.flight_vehicle_no = '");
			strBuild.Append(CongnParam.strFltVehNo);
			strBuild.Append("' and A.delivery_manifest_datetime = ");
			strBuild.Append(Utility.DateFormat(strAppID,strEnterpriseID,CongnParam.dtDeliveryDatetime,DTFormat.DateTime));
		
			if (CongnParam.strRouteCode.Length > 0)
			{
				strBuild.Append(" and A.route_code = '");
				strBuild.Append(CongnParam.strRouteCode);
				strBuild.Append("'");
			}
			//Added By Tom 24/8/09
			strBuild.Append(" order by consignment_no");
			//End added By Tom 24/8/09
			
			
			try
			{
				dbCmd = dbCon.CreateCommand(strBuild.ToString(),CommandType.Text);
				dbCmd.CommandTimeout = 300; 
				dsCongnmnt =(DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("ShipmentUpdateMgrDAL","GetAllDomesticCongn","ERR002","Error : "+ appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ",appException);
			}
			catch(Exception exception)
			{
				Logger.LogTraceError("ShipmentUpdateMgrDAL","GetAllDomesticCongn","ERR003","Error : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}
			return dsCongnmnt;
		}
		
		///<summary>
		///This method gets MBG status
		///</summary>
		public static string getMBGStatus(String strAppID,String strEnterpriseID,String strStatusCode,String strExceptionCode,int intBkg_No, String consignmentNo)
		{
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("shipmentUpdateMgrDAL","AddRecordShipmntTracking","ERR001","DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null",null);
			}	
			return getMBGStatus(strAppID,strEnterpriseID,strStatusCode,strExceptionCode,intBkg_No,consignmentNo,dbCon);
		}


		/// <summary>
		/// Overload Method 
		/// Created by Manit W. on 10 Mar 2010
		/// Modified by Sompote T. on 25 Mar 2010 
		/// </summary>
		/// <param name="strAppID"></param>
		/// <param name="strEnterpriseID"></param>
		/// <param name="strStatusCode"></param>
		/// <param name="strExceptionCode"></param>
		/// <param name="intBkg_No"></param>
		/// <param name="consignmentNo"></param>
		/// <param name="dbCon"></param>
		/// <returns></returns>
		public static string getMBGStatus(String strAppID,String strEnterpriseID,String strStatusCode,String strExceptionCode,int intBkg_No, String consignmentNo,DbConnection dbCon)
		{	
			IDbConnection conApp = dbCon.GetConnection();
			if(conApp == null)
			{
				Logger.LogTraceError("ShipmentUpdateMgrDAL","AddRecordShipmntTracking","ERR003","conApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			try
			{
				dbCon.OpenConnection(ref conApp);
			}
			catch(ApplicationException appException)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("ShipmentUpdateMgrDAL","AddRecordShipmntTracking","ERR004","Error opening connection : "+ appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ",appException);
			}
			catch(Exception exception)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("ShipmentUpdateMgrDAL","AddRecordShipmntTracking","ERR005","Error opening connection : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}
			
			IDbCommand dbCmd = null;		
			dbCmd = dbCon.CreateCommand("",CommandType.Text);
			dbCmd.Connection = conApp;	
			//return getMBGStatus(strAppID,strEnterpriseID,strStatusCode,strExceptionCode,intBkg_No,consignmentNo,dbCon,dbCmd);
			string result ;	
			try
			{
				result = getMBGStatus(strAppID,strEnterpriseID,strStatusCode,strExceptionCode,intBkg_No,consignmentNo,dbCon,dbCmd);
			}
			catch(ApplicationException appExpection)
			{
				throw appExpection;
			}
			finally
			{
				if(conApp != null)
				{
					conApp.Close();
				}
			}

			return result;
		}
/// <summary>
/// Overload Method 
/// Created by Sompote T. on 25 Mar 2010 
/// </summary>
/// <param name="strAppID"></param>
/// <param name="strEnterpriseID"></param>
/// <param name="strStatusCode"></param>
/// <param name="strExceptionCode"></param>
/// <param name="intBkg_No"></param>
/// <param name="consignmentNo"></param>
/// <param name="dbCon"></param>
/// <param name="dbCmd"></param>
/// <returns></returns>
		public static string getMBGStatus(String strAppID,String strEnterpriseID,String strStatusCode,String strExceptionCode,int intBkg_No, String consignmentNo,DbConnection dbCon,IDbCommand dbCmd)
		{		
			DataSet dsExcepStatus = null;
			DataSet dsExcepStatusCA = null;		
			String strExceptionMBGStatus=null;
			
			StringBuilder strBuild =  new StringBuilder();
			strBuild.Append("SELECT DISTINCT e.mbg, s.payer_type FROM Exception_Code e with(nolock) INNER JOIN Shipment s with(nolock) ON e.applicationid = s.applicationid AND e.enterpriseid = s.enterpriseid WHERE (e.exception_code ='"+strExceptionCode+"') AND (s.booking_no ="+intBkg_No+")  AND (s.consignment_no = '"+consignmentNo+"') AND (e.applicationid ='"+strAppID+"') AND (s.applicationid ='"+strAppID+"') AND (e.enterpriseid ='"+strEnterpriseID+"')   AND (s.enterpriseid ='"+strEnterpriseID+"')");
			try
			{			
				dbCmd.CommandText = strBuild.ToString();
				dsExcepStatus =(DataSet)dbCon.ExecuteQueryInTransaction(dbCmd,com.common.DAL.ReturnType.DataSetType);
				if (dsExcepStatus.Tables[0].Rows.Count > 0)
				{
					DataRow drExcepStatus = dsExcepStatus.Tables[0].Rows[0];
					if((drExcepStatus["mbg"]!= null) && (!drExcepStatus["mbg"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						strExceptionMBGStatus = (String)(drExcepStatus["mbg"]);
						if(strExceptionMBGStatus.ToUpper().Equals("Y"))
						{
							if((drExcepStatus["payer_type"]!= null) && (!drExcepStatus["payer_type"].GetType().Equals(System.Type.GetType("System.DBNull"))))
							{
								String strPayerType=null;
								strPayerType = (String)(drExcepStatus["payer_type"]);
								if(strPayerType !=null)
								{
									string strQ="";
									if(strPayerType.ToUpper().Equals("C"))
									{
										strQ="SELECT mbg FROM  Customer with(nolock) WHERE (custid = (SELECT payerid FROM   Shipment with(nolock) WHERE (booking_no ="+intBkg_No+") AND (consignment_no = '"+consignmentNo+"') AND (applicationid ='"+strAppID+"') AND (enterpriseid ='"+strEnterpriseID+"')))";
									}
									else
									{
										strQ="SELECT mbg FROM Agent with(nolock) WHERE (agentid =(SELECT payerid FROM  Shipment with(nolock) WHERE (booking_no ="+intBkg_No+") AND (consignment_no = '"+consignmentNo+"') AND (applicationid ='"+strAppID+"') AND (enterpriseid ='"+strEnterpriseID+"')))";
									}
								
									dbCmd.CommandText = strQ;
									dsExcepStatusCA =(DataSet)dbCon.ExecuteQueryInTransaction(dbCmd,com.common.DAL.ReturnType.DataSetType);
									if (dsExcepStatusCA.Tables[0].Rows.Count > 0)
									{
										DataRow drExcepStatusCA = dsExcepStatusCA.Tables[0].Rows[0];
										if((drExcepStatusCA["mbg"]!= null) && (!drExcepStatusCA["mbg"].GetType().Equals(System.Type.GetType("System.DBNull"))))
										{
											strExceptionMBGStatus = (String)(drExcepStatusCA["mbg"]);
											if(strExceptionMBGStatus.ToUpper().Equals("N"))
											{
												strExceptionMBGStatus=null;
											}
										}
									}

								}
							}
								
						}
						else
							strExceptionMBGStatus=null;
					}
				}
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("ShipmentUpdateMgrDAL","getMBGStatus","ERR002","Error : "+ appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ",appException);
			}
			catch(Exception exception)
			{
				Logger.LogTraceError("ShipmentUpdateMgrDAL","getMBGStatus","ERR003","Error : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}
			return strExceptionMBGStatus;
		}

















		/// <summary>
		/// This method gets payer type for the particular consignment from shipment table.
		/// </summary>
		/// <param name="strAppID">Application ID</param>
		/// <param name="strEnterpriseID">Enterprise ID</param>
		/// <param name="ConsgmntNo">Consignment no.</param>
		/// <returns>string (payer type)</returns>
		public static decimal GetPayerSurcharge(String strAppID,String strEnterpriseID,String ConsgmntNo,int strBknNo,String strStatusCode,String strExceptionCode,IDbCommand dbCmd,DbConnection dbCon)
		{
			StringBuilder strBuild = null;
			DataSet dsShipmnt = null, dsSurcharge = null;
			string strPayerType=null, strPayerID=null;
			decimal decSur=0;
			bool bException=false;

			strBuild =  new StringBuilder();
			strBuild.Append("select payer_type, payerid, new_account from shipment with(nolock) where applicationid = '");
			strBuild.Append(strAppID+"'");
			strBuild.Append(" and enterpriseid = '");
			strBuild.Append(strEnterpriseID+"'");
			strBuild.Append(" and consignment_no = '");
			strBuild.Append(ConsgmntNo+"'");
			
			try
			{
				dbCmd.CommandText = strBuild.ToString();
				dsShipmnt =(DataSet)dbCon.ExecuteQueryInTransaction(dbCmd,com.common.DAL.ReturnType.DataSetType);

			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("ShipmentUpdateMgrDAL","GetShipmentPayerType","ERR002","Error : "+ appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ",appException);
			}
			catch(Exception exception)
			{
				Logger.LogTraceError("ShipmentUpdateMgrDAL","GetShipmentPayerType","ERR003","Error : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}
	
			if (dsShipmnt.Tables[0].Rows.Count > 0)
			{
				//get Payer type and Payer ID
				DataRow drEach = dsShipmnt.Tables[0].Rows[0];
				if((drEach["payer_type"]!= null) && (!drEach["payer_type"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					strPayerType = (string)drEach["payer_type"];
				else
					return decSur;
				if((drEach["payerid"]!= null) && (!drEach["payerid"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					strPayerID = (string)drEach["payerid"];
				else
					return decSur;

				if (strExceptionCode!=null && strExceptionCode!="")
					bException = true;
				else
					bException = false;

				strBuild =  new StringBuilder();
				strBuild.Append("select surcharge from ");
				if (strPayerType == "C")
					if (bException == true)
						strBuild.Append("customer_exception_charge ");
					else
						strBuild.Append("customer_status_charge ");
				else 
					if (bException == true)
						strBuild.Append("agent_exception_charge ");
					else
						strBuild.Append("agent_status_charge ");

				strBuild.Append(" with(nolock) where applicationid = '"+strAppID+"' and enterpriseid = '"+strEnterpriseID+"' and ");
				if (strPayerType == "C")
					strBuild.Append("custid = ");
				else
					strBuild.Append("agentid = ");
				strBuild.Append("'"+strPayerID+"' and status_code = '"+strStatusCode+"' ");

				if (bException == true)
					strBuild.Append("and exception_code = '"+strExceptionCode+"' ");
			
				try
				{
					dbCmd.CommandText = strBuild.ToString();
					dsSurcharge =(DataSet)dbCon.ExecuteQueryInTransaction(dbCmd,com.common.DAL.ReturnType.DataSetType);

					if (dsSurcharge.Tables[0].Rows.Count > 0)
					{
						//get Payer type and Payer ID
						DataRow drSur = dsSurcharge.Tables[0].Rows[0];
						if((drSur["surcharge"]!= null) && (!drSur["surcharge"].GetType().Equals(System.Type.GetType("System.DBNull"))))
							decSur = Convert.ToDecimal(drSur["surcharge"]);
					}
				}
				catch(ApplicationException appException)
				{
					Logger.LogTraceError("ShipmentUpdateMgrDAL","GetShipmentPayerType","ERR002","Error : "+ appException.Message.ToString());
					throw new ApplicationException("Error opening database connection ",appException);
				}
				catch(Exception exception)
				{
					Logger.LogTraceError("ShipmentUpdateMgrDAL","GetShipmentPayerType","ERR003","Error : "+ exception.Message.ToString());
					throw new ApplicationException("Error opening database connection ",exception);
				}
			}
			else
			{
				return decSur;
			}

			return decSur;
		}


		//Comment by Sompote 2010-03-29
		/*private static String getActualDeliveryDateTime(String strAppID,String strEnterpriseID,String strConsignment, int iBookingNo,String[]strPOD)
		{

			IDbCommand dbCmd = null;
			int i = 0;
			DateTime dtCurrent = DateTime.Now;
			

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("shipmentUpdateMgrDAL","AddRecordShipmntTracking","ERR001","DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null",null);
			}
			

			IDbConnection conApp = dbCon.GetConnection();
			
			if(conApp == null)
			{
				Logger.LogTraceError("ShipmentUpdateMgrDAL","AddRecordShipmntTracking","ERR003","conApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			try
			{
				dbCon.OpenConnection(ref conApp);
			}
			catch(ApplicationException appException)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("ShipmentUpdateMgrDAL","AddRecordShipmntTracking","ERR004","Error opening connection : "+ appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ",appException);
			}
			catch(Exception exception)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("ShipmentUpdateMgrDAL","AddRecordShipmntTracking","ERR005","Error opening connection : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}


			IDbTransaction transactionApp = dbCon.BeginTransaction(conApp,IsolationLevel.ReadCommitted);

			if(transactionApp == null)
			{
				Logger.LogTraceError("ShipmentUpdateMgrDAL","AddRecordShipmntTracking","ERR006","transactionApp is null");
				throw new ApplicationException("Failed Begining a App Transaction..",null);
			}
			String strBuilder = null;			
			String strActDelvryDate = null; 
			if(dbCon == null)
			{
				Logger.LogTraceError("ShipmentUpdateMgrDAL","getActualDeliveryDateTime","ERR001","DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null",null);
			}
			
			String str1=null;
			for(i=0; i<strPOD.Length;i++)
			{
				if (i==0 || i==(strPOD.Length-1))
				{
					str1+="'"+strPOD[i]+"'";
				}
				else
					str1+= ",'" + strPOD[i] + "',";				
			}		
			strBuilder="SELECT MIN(tracking_datetime) AS tracking_datetime FROM Shipment_Tracking  where (applicationid = '";
			strBuilder+=strAppID+"' and enterpriseid = '"+strEnterpriseID+"' and consignment_no = '";
			strBuilder+=strConsignment+"') and (Booking_No = "+iBookingNo+ ") and (Status_Code in ("+ str1 +")) and (deleted NOT IN ('Y', 'y') OR deleted IS NULL)";
							
			DateTime dtActDeliveryDatetime;
			try
			{	
				
				dbCmd = dbCon.CreateCommand(strBuilder.ToString(),CommandType.Text);
				dbCmd.Connection = conApp;
				dbCmd.Transaction = transactionApp;
			
				DataSet dsShipmnt =(DataSet)dbCon.ExecuteQueryInTransaction(dbCmd,com.common.DAL.ReturnType.DataSetType);
				
				if (dsShipmnt.Tables[0].Rows.Count > 0)
				{
					DataRow drEach = dsShipmnt.Tables[0].Rows[0];

					if((drEach["tracking_datetime"]!= null) && (!drEach["tracking_datetime"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						dtActDeliveryDatetime = (DateTime)drEach["tracking_datetime"];
						strActDelvryDate =dtActDeliveryDatetime.ToString("dd/MM/yyyy HH:mm");
					}
					else
					{
						strActDelvryDate = null;
					}
				}
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("ShipmentUpdateMgrDAL","getActualDeliveryDateTime","ERR001","Error : "+ appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ",appException);
			}
			catch(Exception exception)
			{
				Logger.LogTraceError("ShipmentUpdateMgrDAL","getActualDeliveryDateTime","ERR002","Error : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}
			return strActDelvryDate;

		}
*/
/// <summary>
/// Add by Sompote 2010-03-29
/// </summary>
/// <param name="strAppID"></param>
/// <param name="strEnterpriseID"></param>
/// <param name="strConsignment"></param>
/// <param name="iBookingNo"></param>
/// <param name="strPOD"></param>
/// <returns></returns>
		private static String getActualDeliveryDateTime(String strAppID,String strEnterpriseID,String strConsignment, int iBookingNo,String[]strPOD)
		{					
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);				
			if(dbCon == null)
			{
				Logger.LogTraceError("shipmentUpdateMgrDAL","AddRecordShipmntTracking","ERR001","DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null",null);
			}		
			return getActualDeliveryDateTime( strAppID, strEnterpriseID, strConsignment,  iBookingNo,strPOD, dbCon);
		}

		/// <summary>
		/// This method gets ActulaDeliveryDateTime for the particular consignment from shipment Tracking table.
		/// </summary>
		/// <param name="strAppID">Application ID</param>
		/// <param name="strEnterpriseID">Enterprise ID</param>
		/// <param name="ConsgmntNo">Consignment no.</param>
		/// <returns>DateTime (actDeliverydatetime Type )</returns>

		private static String getActualDeliveryDateTime(String strAppID,String strEnterpriseID,String strConsignment, int iBookingNo,String[]strPOD,DbConnection dbCon)
		{		
			IDbConnection conApp = dbCon.GetConnection();			
			if(conApp == null)
			{
				Logger.LogTraceError("ShipmentUpdateMgrDAL","AddRecordShipmntTracking","ERR003","conApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			try
			{
				dbCon.OpenConnection(ref conApp);
			}
			catch(ApplicationException appException)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("ShipmentUpdateMgrDAL","AddRecordShipmntTracking","ERR004","Error opening connection : "+ appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ",appException);
			}
			catch(Exception exception)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("ShipmentUpdateMgrDAL","AddRecordShipmntTracking","ERR005","Error opening connection : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}	
			IDbCommand dbCmd = null;	
			dbCmd = dbCon.CreateCommand("",CommandType.Text);
			dbCmd.Connection = conApp;		
			//return getActualDeliveryDateTime( strAppID, strEnterpriseID, strConsignment,  iBookingNo,strPOD, dbCon, dbCmd);
			string result  ;	
			try
			{
				result = getActualDeliveryDateTime( strAppID, strEnterpriseID, strConsignment,  iBookingNo,strPOD, dbCon, dbCmd);
			}
			catch(ApplicationException appExpection)
			{
				throw appExpection;
			}
			finally
			{
				if(conApp != null)
				{
					conApp.Close();
				}
			}
			return result;
		}

		/// <summary>
		/// Overload Method by Sompote T. on 25 Mar 2010
		/// </summary>
		/// <param name="strAppID"></param>
		/// <param name="strEnterpriseID"></param>
		/// <param name="strConsignment"></param>
		/// <param name="iBookingNo"></param>
		/// <param name="strPOD"></param>
		/// <param name="dbCon"></param>
		/// <param name="dbCmd"></param>
		/// <returns></returns>
		private static String getActualDeliveryDateTime(String strAppID,String strEnterpriseID,String strConsignment, int iBookingNo,String[]strPOD,DbConnection dbCon,IDbCommand dbCmd)
		{						
			int i = 0;
			DateTime dtCurrent = DateTime.Now;	
			String strBuilder = null;			
			String strActDelvryDate = null; 
			String str1=null;
			for(i=0; i<strPOD.Length;i++)
			{
				if (i==0 || i==(strPOD.Length-1))
				{
					str1+="'"+strPOD[i]+"'";
				}
				else
					str1+= ",'" + strPOD[i] + "',";				
			}		
			strBuilder="SELECT MIN(tracking_datetime) AS tracking_datetime FROM Shipment_Tracking with(nolock) where (applicationid = '";
			strBuilder+=strAppID+"' and enterpriseid = '"+strEnterpriseID+"' and consignment_no = '";
			strBuilder+=strConsignment+"') and (Booking_No = "+iBookingNo+ ") and (Status_Code in ("+ str1 +")) and (deleted NOT IN ('Y', 'y') OR deleted IS NULL)";
							
			DateTime dtActDeliveryDatetime;
			try
			{	
				DataSet dsShipmnt = new DataSet();
				
				dbCmd.CommandText = strBuilder.ToString();
				dsShipmnt =(DataSet)dbCon.ExecuteQueryInTransaction(dbCmd,com.common.DAL.ReturnType.DataSetType);
				

				if (dsShipmnt.Tables[0].Rows.Count > 0)
				{
					DataRow drEach = dsShipmnt.Tables[0].Rows[0];

					if((drEach["tracking_datetime"]!= null) && (!drEach["tracking_datetime"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						dtActDeliveryDatetime = (DateTime)drEach["tracking_datetime"];
						strActDelvryDate =dtActDeliveryDatetime.ToString("dd/MM/yyyy HH:mm");
					}
					else
					{
						strActDelvryDate = null;
					}
				}
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("ShipmentUpdateMgrDAL","getActualDeliveryDateTime","ERR001","Error : "+ appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ",appException);
			}
			catch(Exception exception)
			{
				Logger.LogTraceError("ShipmentUpdateMgrDAL","getActualDeliveryDateTime","ERR002","Error : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}
			return strActDelvryDate;
		}


		private static String getActualPickupDateTime(String strAppID,String strEnterpriseID, int iBookingNo,String strPUP,IDbCommand dbCmd,DbConnection dbCon)
		{
			DataSet dsShipAcDelDt=null;
			String strActPickupDateTime = null; 
			if(dbCon == null)
			{
				Logger.LogTraceError("ShipmentUpdateMgrDAL","getActualPickupDateTime","ERR001","DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null",null);
			}
			
				string strqry="SELECT MAX(tracking_datetime) As tracking_datetime FROM  Dispatch_Tracking with(nolock) WHERE     (applicationid = '"+strAppID+"') AND (enterpriseid = '"+strEnterpriseID+"') AND (booking_no = "+iBookingNo+") AND (status_code = '"+strPUP+"') AND (deleted <> 'Y' OR deleted IS NULL)";
			
							
			DateTime dtActDeliveryDatetime;
			try
			{	
				dbCmd.CommandText = strqry;
				dsShipAcDelDt =(DataSet)dbCon.ExecuteQueryInTransaction(dbCmd,com.common.DAL.ReturnType.DataSetType);
				if (dsShipAcDelDt.Tables[0].Rows.Count > 0)
				{
					DataRow drEach = dsShipAcDelDt.Tables[0].Rows[0];

					if((drEach["tracking_datetime"]!= null) && (!drEach["tracking_datetime"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						dtActDeliveryDatetime = (DateTime)drEach["tracking_datetime"];
						strActPickupDateTime =dtActDeliveryDatetime.ToString("dd/MM/yyyy HH:mm");
					}
					else
					{
						strActPickupDateTime = null;
					}
				}
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("ShipmentUpdateMgrDAL","getActualDeliveryDateTime","ERR001","Error : "+ appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ",appException);
			}
			catch(Exception exception)
			{
				Logger.LogTraceError("ShipmentUpdateMgrDAL","getActualDeliveryDateTime","ERR002","Error : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}
			return strActPickupDateTime;

		}
		private static string InsertActualPickupDateTime(String strAppID, String strEnterpriseID, int strBookNo, String strActStatusPickup)
		{
			string strActualPickDateTime=null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			string strqry="SELECT MAX(tracking_datetime) As tracking_datetime FROM  Dispatch_Tracking with(nolock) WHERE     (applicationid = '"+strAppID+"') AND (enterpriseid = '"+strEnterpriseID+"') AND (booking_no = "+strBookNo+") AND (status_code = '"+strActStatusPickup+"') AND (deleted <> 'Y')";
		
			try
			{
				strActualPickDateTime=(string)dbCon.ExecuteScalar(strqry);
				return strActualPickDateTime;

			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceInfo("DispatchTrackingMrgDAL","UpdateActualPickupDateTime","SDM002","unable to update actual pickup date time.");
				throw new ApplicationException("unable to update actual pickup date time.",null);
			}
			//return strActualPickDateTime;
		}
		/// <summary>
		/// Comment By Sompote 2010-03-29
		/// </summary>
		/// <param name="strAppID"></param>
		/// <param name="strEnterpriseID"></param>
		/// <param name="iBookingNo"></param>
		/// <param name="ConsignNo"></param>
		/// <returns></returns>
		/*private static DataSet getLatestStatusCode(String strAppID,String strEnterpriseID, int iBookingNo, String ConsignNo)
		{
			DataSet dsLatestStatusCode=null;
			IDbCommand dbCmd = null;
			DateTime dtCurrent = DateTime.Now;
			

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("shipmentUpdateMgrDAL","getLatestStatusCode","ERR001","DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null",null);
			}
			

			IDbConnection conApp = dbCon.GetConnection();
			
			if(conApp == null)
			{
				Logger.LogTraceError("ShipmentUpdateMgrDAL","getLatestStatusCode","ERR003","conApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			try
			{
				dbCon.OpenConnection(ref conApp);
			}
			catch(ApplicationException appException)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("ShipmentUpdateMgrDAL","getLatestStatusCode","ERR004","Error opening connection : "+ appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ",appException);
			}
			catch(Exception exception)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("ShipmentUpdateMgrDAL","getLatestStatusCode","ERR005","Error opening connection : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}
			IDbTransaction transactionApp = dbCon.BeginTransaction(conApp,IsolationLevel.ReadCommitted);

			if(transactionApp == null)
			{
				Logger.LogTraceError("ShipmentUpdateMgrDAL","getLatestStatusCode","ERR006","transactionApp is null");
				throw new ApplicationException("Failed Begining a App Transaction..",null);
			}
			if(dbCon == null)
			{
				Logger.LogTraceError("ShipmentUpdateMgrDAL","getActualDeliveryDateTime","ERR001","DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null",null);
			}
				string strqry="SELECT status_code, exception_code, tracking_datetime ";
				strqry += "FROM   Shipment_Tracking ";
				strqry += "WHERE (applicationid = '"+strAppID+"') ";
				strqry += "AND (enterpriseid = '"+strEnterpriseID+"') ";
				strqry += "AND (booking_no = "+iBookingNo+") ";
				strqry += "AND (consignment_no = '"+ConsignNo+"') ";
				strqry += "AND (tracking_datetime = ";
				strqry += "(SELECT MAX(tracking_datetime) AS tracking_datetime ";
				strqry += "FROM Shipment_Tracking ";
				strqry += "WHERE (applicationid = '"+strAppID+"') ";
				strqry += "AND (enterpriseid = '"+strEnterpriseID+"') ";
				strqry += "AND (booking_no = "+iBookingNo+") ";
				strqry += "AND (consignment_no = '"+ConsignNo+"') ";
				strqry += "AND (deleted IS NULL OR deleted NOT IN ('y', 'Y'))))";
			
			try
			{	
				dbCmd = dbCon.CreateCommand(strqry,CommandType.Text);
				dbCmd.Connection = conApp;
				dbCmd.Transaction = transactionApp;
				dsLatestStatusCode =(DataSet)dbCon.ExecuteQueryInTransaction(dbCmd,com.common.DAL.ReturnType.DataSetType);
				
			}
			
			catch(Exception exception)
			{
				Logger.LogTraceError("ShipmentUpdateMgrDAL","getLatestStatusCode","ERR002","Error : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}
			return dsLatestStatusCode;

		}*/
		private static DataSet getLatestStatusCode(String strAppID,String strEnterpriseID, int iBookingNo, String ConsignNo)
		{
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("shipmentUpdateMgrDAL","getLatestStatusCode","ERR001","DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null",null);
			}		
			return getLatestStatusCode( strAppID, strEnterpriseID,  iBookingNo,  ConsignNo, dbCon);

		}	
		/// <summary>
		/// Add by Sompote 2010-03-29
		/// </summary>
		/// <param name="strAppID"></param>
		/// <param name="strEnterpriseID"></param>
		/// <param name="iBookingNo"></param>
		/// <param name="ConsignNo"></param>
		/// <param name="dbCon"></param>
		/// <returns></returns>
		private static DataSet getLatestStatusCode(String strAppID,String strEnterpriseID, int iBookingNo, String ConsignNo, DbConnection dbCon)
		{					
			IDbConnection conApp = dbCon.GetConnection();
			
			if(conApp == null)
			{
				Logger.LogTraceError("ShipmentUpdateMgrDAL","getLatestStatusCode","ERR003","conApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			try
			{
				dbCon.OpenConnection(ref conApp);
			}
			catch(ApplicationException appException)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("ShipmentUpdateMgrDAL","getLatestStatusCode","ERR004","Error opening connection : "+ appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ",appException);
			}
			catch(Exception exception)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("ShipmentUpdateMgrDAL","getLatestStatusCode","ERR005","Error opening connection : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}				
			IDbCommand dbCmd = null;		
			dbCmd = dbCon.CreateCommand("",CommandType.Text);
			dbCmd.Connection = conApp;	
			//return getLatestStatusCode( strAppID, strEnterpriseID,  iBookingNo,  ConsignNo, dbCon, dbCmd);
			DataSet ds ;	
			try
			{
				ds = getLatestStatusCode( strAppID, strEnterpriseID,  iBookingNo,  ConsignNo, dbCon, dbCmd);
			}
			catch(ApplicationException appExpection)
			{
				throw appExpection;
			}
			finally
			{
				if(conApp != null)
				{
					conApp.Close();
				}
			}
			return ds;

		}
		/// <summary>
		/// Add by Sompote 2010-03-29
		/// </summary>
		/// <param name="strAppID"></param>
		/// <param name="strEnterpriseID"></param>
		/// <param name="iBookingNo"></param>
		/// <param name="ConsignNo"></param>
		/// <param name="dbCon"></param>
		/// <param name="dbCmd"></param>
		/// <returns></returns>
		private static DataSet getLatestStatusCode(String strAppID,String strEnterpriseID, int iBookingNo, String ConsignNo,DbConnection dbCon,IDbCommand dbCmd)
		{			
			DataSet dsLatestStatusCode=null;			
			DateTime dtCurrent = DateTime.Now;
			string strqry="SELECT status_code, exception_code, tracking_datetime ";
			strqry += "FROM   Shipment_Tracking with(nolock) ";
			strqry += "WHERE (applicationid = '"+strAppID+"') ";
			strqry += "AND (enterpriseid = '"+strEnterpriseID+"') ";
			strqry += "AND (booking_no = "+iBookingNo+") ";
			strqry += "AND (consignment_no = '"+ConsignNo+"') ";
			strqry += "AND (tracking_datetime = ";
			strqry += "(SELECT MAX(tracking_datetime) AS tracking_datetime ";
			strqry += "FROM Shipment_Tracking with(nolock) ";
			strqry += "WHERE (applicationid = '"+strAppID+"') ";
			strqry += "AND (enterpriseid = '"+strEnterpriseID+"') ";
			strqry += "AND (booking_no = "+iBookingNo+") ";
			strqry += "AND (consignment_no = '"+ConsignNo+"') ";
			strqry += "AND (deleted IS NULL OR deleted NOT IN ('y', 'Y'))))";
			
			try
			{					
				dbCmd.CommandText = strqry;
				dsLatestStatusCode =(DataSet)dbCon.ExecuteQueryInTransaction(dbCmd,com.common.DAL.ReturnType.DataSetType);
			}			
			catch(Exception exception)
			{
				Logger.LogTraceError("ShipmentUpdateMgrDAL","getLatestStatusCode","ERR002","Error : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}
			return dsLatestStatusCode;

		}

		private static DataSet getLatestStatusCodePickup(String strAppID,String strEnterpriseID, int iBookingNo,IDbCommand dbCmd,DbConnection dbCon)
		{
			DataSet dsLatestStatusCode=null;
			if(dbCon == null)
			{
				Logger.LogTraceError("ShipmentUpdateMgrDAL","getActualPickupDateTime","ERR001","DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null",null);
			}
			
			//string strqry="SELECT MAX(tracking_datetime) As tracking_datetime FROM  Dispatch_Tracking  WHERE     (applicationid = '"+strAppID+"') AND (enterpriseid = '"+strEnterpriseID+"') AND (booking_no = "+iBookingNo+") AND (status_code = '"+strPUP+"') AND (deleted <> 'Y' OR deleted IS NULL)";
			
			string strqry="SELECT status_code, exception_code, tracking_datetime FROM Dispatch_Tracking with(nolock) WHERE (applicationid = '"+strAppID+"') AND (enterpriseid = '"+strEnterpriseID+"') AND (booking_no = "+iBookingNo+") AND (tracking_datetime = (SELECT MAX(tracking_datetime) AS tracking_datetime FROM Dispatch_Tracking with(nolock) WHERE (applicationid = '"+strAppID+"') AND (enterpriseid = '"+strEnterpriseID+"') AND (booking_no = "+iBookingNo+") AND (deleted IS NULL OR deleted NOT IN ('y', 'Y'))))";
			
			try
			{	
				dbCmd.CommandText = strqry;
				dsLatestStatusCode =(DataSet)dbCon.ExecuteQueryInTransaction(dbCmd,com.common.DAL.ReturnType.DataSetType);
				
			}
			
			catch(Exception exception)
			{
				Logger.LogTraceError("ShipmentUpdateMgrDAL","getLatestStatusCode","ERR002","Error : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}
			
			return dsLatestStatusCode;

		}
/*
 SessionDS sessionDS ;	
try
			{
				sessionDS = QueryDispatchForShipmentTracking( strAppID,  strEnterpriseID, strBookNo,  recStart,  recSize, dbCon, dbCmd);
			}
			catch(ApplicationException appExpection)
			{
				throw appExpection;
			}
			finally
			{
				if(conApp != null)
				{
					conApp.Close();
				}
			}

 * */

		public static DataSet getRouteDetails(String strAppID,String strEnterpriseID, String pathCode)
		{

			IDbCommand dbCmd = null;
			StringBuilder strBuild = null;
			DataSet dsManifest = null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("ShipmentUpdateMgrDAL","getRouteDetails","ERR001","DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null",null);
			}
			
			strBuild =  new StringBuilder();
			strBuild.Append("select * ");
			strBuild.Append("from delivery_path with(nolock) ");
			strBuild.Append("where applicationid = '");
			strBuild.Append(strAppID+"'");
			strBuild.Append(" and enterpriseid = '");
			strBuild.Append(strEnterpriseID+"'");
			strBuild.Append(" and path_code = '" + pathCode + "' ");
			
			try
			{
				dbCmd = dbCon.CreateCommand(strBuild.ToString(),CommandType.Text);
				dsManifest =(DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("ShipmentUpdateMgrDAL","getRouteDetails","ERR002","Error : "+ appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ",appException);
			}
			catch(Exception exception)
			{
				Logger.LogTraceError("ShipmentUpdateMgrDAL","getRouteDetails","ERR003","Error : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}
			return dsManifest;

		}

		//HC Return Task
		public static DataSet getZipCodeByCon(String strAppID,String strEnterpriseID, String consignmentNo, String bookingNo)
		{

			IDbCommand dbCmd = null;
			StringBuilder strBuild = null;
			DataSet dsZipCode = null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("ShipmentUpdateMgrDAL","getZipCodeByCon","ERR001","DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null",null);
			}
			
			strBuild =  new StringBuilder();
			strBuild.Append("select recipient_zipcode ");
			strBuild.Append("from shipment with(nolock) ");
			strBuild.Append("where applicationid = '");
			strBuild.Append(strAppID+"'");
			strBuild.Append(" and enterpriseid = '");
			strBuild.Append(strEnterpriseID+"'");
			strBuild.Append(" and consignment_no = '" + consignmentNo + "' ");
			strBuild.Append(" and booking_no = '" + bookingNo + "' ");
			
			try
			{
				dbCmd = dbCon.CreateCommand(strBuild.ToString(),CommandType.Text);
				dsZipCode =(DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("ShipmentUpdateMgrDAL","getZipCodeByCon","ERR002","Error : "+ appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ",appException);
			}
			catch(Exception exception)
			{
				Logger.LogTraceError("ShipmentUpdateMgrDAL","getZipCodeByCon","ERR003","Error : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}
			return dsZipCode;

		}

		public static DataSet getTrackingRecords(String strAppID,String strEnterpriseID, int iBookingNo, String ConsignNo)
		{
			DataSet dsTrackingRecord = null;
			IDbCommand dbCmd = null;
			
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("shipmentUpdateMgrDAL","getTrackingRecords","ERR001","DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null",null);
			}

			IDbConnection conApp = dbCon.GetConnection();
			if(conApp == null)
			{
				Logger.LogTraceError("ShipmentUpdateMgrDAL","getTrackingRecords","ERR003","conApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			try
			{
				dbCon.OpenConnection(ref conApp);
			}
			catch(ApplicationException appException)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("ShipmentUpdateMgrDAL","getTrackingRecords","ERR004","Error opening connection : "+ appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ",appException);
			}
			catch(Exception exception)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("ShipmentUpdateMgrDAL","getTrackingRecords","ERR005","Error opening connection : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}
			IDbTransaction transactionApp = dbCon.BeginTransaction(conApp,IsolationLevel.ReadCommitted);

			if(transactionApp == null)
			{
				Logger.LogTraceError("ShipmentUpdateMgrDAL","getTrackingRecords","ERR006","transactionApp is null");
				throw new ApplicationException("Failed Begining a App Transaction..",null);
			}
			if(dbCon == null)
			{
				Logger.LogTraceError("ShipmentUpdateMgrDAL","getTrackingRecords","ERR001","DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null",null);
			}

			string strqry="SELECT * ";
			strqry += "FROM   Shipment_Tracking with(nolock) ";
			strqry += "WHERE (applicationid = '"+strAppID+"') ";
			strqry += "AND (enterpriseid = '"+strEnterpriseID+"') ";
			strqry += "AND (booking_no = "+iBookingNo+") ";
			strqry += "AND (consignment_no = '"+ConsignNo+"') ";

			
			try
			{	
				dbCmd = dbCon.CreateCommand(strqry,CommandType.Text);
				dbCmd.Connection = conApp;
				dbCmd.Transaction = transactionApp;
				dsTrackingRecord =(DataSet)dbCon.ExecuteQueryInTransaction(dbCmd,com.common.DAL.ReturnType.DataSetType);
				
			}
			
			catch(Exception exception)
			{
				Logger.LogTraceError("ShipmentUpdateMgrDAL","getTrackingRecords","ERR002","Error : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}
			return dsTrackingRecord;

		}

		/// <summary>
		// 4.7.2
		//	Upon entry of a POD record:
		//	If the actual delivery date / time (status date / time of the POD) is less than or equal to the estimated delivery date
		//	/ time and no POD slip return is required:
		//	THEN Shipment_Tracking.invoiceable <-- Status_Code.invoiceable (of POD status)
		//	ELSE Shipment_Tracking.invoiceable <-- N
		/// </summary>
		private static string getInvablePODByCompareDate(DbConnection dbCon, IDbCommand dbCmd, string applicationid, string enterpriseid, 
														 string consignment_no, int booking_no, DateTime tracking_datetime)
		{
			dbCmd.CommandText = @"
				--parameter in
				declare  @applicationid varchar(40)
						,@enterpriseid varchar(40)
						,@consignment_no varchar(30)
						,@booking_no int
						,@tracking_datetime	 datetime 
						,@invoiceable char(1)

				--set param
				set @applicationid='"+applicationid+@"'
				set @enterpriseid='"+enterpriseid+@"'
				set @consignment_no='"+consignment_no+@"'
				set @booking_no="+booking_no+@"
				set @tracking_datetime='"+tracking_datetime.ToString("MM/dd/yyyy hh:mm:ss")+@"'
				set @invoiceable='N'

				--########## Compare tracking_datetime vs @est_delivery_datetime  && return invoiceable ##########
				select  @invoiceable=(case when @tracking_datetime<=SM.est_delivery_datetime 
							  then ( select invoiceable from Status_Code with(nolock) where (applicationid=@applicationid) and (enterpriseid=@enterpriseid) and (status_code='POD'))
							  else 'N' end)
				from    Shipment SM with(nolock) 
				where	(SM.applicationid=@applicationid) and (SM.enterpriseid=@enterpriseid) and 
						(SM.consignment_no=@consignment_no) and (SM.booking_no=@booking_no) and 
						isnull(SM.return_pod_slip, 'N')='N' 
				
				select @invoiceable ";

			object invoiceable="N"; //Default
			try
			{	
				invoiceable = dbCon.ExecuteScalarInTransaction(dbCmd);
			}			
			catch(Exception exception)
			{
				Logger.LogTraceError("ShipmentUpdateMgrDAL","getTrackingRecords","ERR002","Error : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}

			return invoiceable.ToString();
		}

		/// <summary>
		/// 4.7.3
		// Upon entry of any Exception code:
		//	If no POD slip return is required
		//	THEN Shipment_Tracking.invoiceable <-- Exception_Code.invoiceable
		//	ELSE Shipment_Tracking.invoiceable <-- N
		/// </summary>
		private static string getInvableExcepCode(DbConnection dbCon, IDbCommand dbCmd, string applicationid, string enterpriseid, 
												  string consignment_no, int booking_no, string status_code, string exception_code)
		{
			dbCmd.CommandText = @"
				--parameter in
				declare  @applicationid varchar(40)
						,@enterpriseid varchar(40)
						,@consignment_no varchar(30)
						,@booking_no int
						,@status_code varchar(12)
						,@exception_code varchar(12)
					
				--set param
				set @applicationid='"+applicationid+@"'
				set @enterpriseid='"+enterpriseid+@"'
				set @consignment_no='"+consignment_no+@"'
				set @booking_no="+booking_no+@"
				set @status_code='"+status_code+@"'
				set @exception_code='"+exception_code+@"'

				--########## no POD slip return & return invoiceable ##########
				select  (case when isnull(SM.return_pod_slip, 'N')='N' 
							  then (select invoiceable from Exception_Code with(nolock) 
									where (applicationid=@applicationid) and (enterpriseid=@enterpriseid) and 
										  (status_code=@status_code) and (exception_code=@exception_code) )
							  else 'N' end) 
				from    Shipment SM with(nolock) 
				where	(SM.applicationid=@applicationid) and (SM.enterpriseid=@enterpriseid) and 
						(SM.consignment_no=@consignment_no) and (SM.booking_no=@booking_no)  ";

			object invoiceable="N"; //Default
			try
			{	
				invoiceable = dbCon.ExecuteScalarInTransaction(dbCmd);
			}			
			catch(Exception exception)
			{
				Logger.LogTraceError("ShipmentUpdateMgrDAL","getTrackingRecords","ERR002","Error : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}

			return invoiceable.ToString();
		}

		private static void SetSurchOfCust(DbConnection dbCon, IDbCommand dbCmd, string applicationid, string enterpriseid, 
											string consignment_no, int booking_no, string status_code, string exception_code, ref decimal surchargeRef)
		{
			dbCmd.CommandText = @"
				--parameter in
				declare  @applicationid varchar(40)
						,@enterpriseid varchar(40)
						,@consignment_no varchar(30)
						,@booking_no int
						,@status_code varchar(12)
						,@exception_code varchar(12)

				--set param 
				set @applicationid='"+applicationid+@"'
				set @enterpriseid='"+enterpriseid+@"'
				set @consignment_no='"+consignment_no+@"'
				set @booking_no="+booking_no+@"
				set @status_code='"+status_code+@"'
				set @exception_code='"+exception_code+@"' 

				--select * from Shipment where consignment_no=@consignment_no
				--select invoiceable,* from Shipment_Tracking where consignment_no=@consignment_no

				--########## Select surcharge ##########
				select  surcharge
				from    Customer_Exception_Charge CEC  with(nolock) left join 
						Shipment S  with(nolock) on CEC.custid=S.payerid and 
									CEC.applicationid=S.applicationid and 
									CEC.enterpriseid=S.enterpriseid and 
									CEC.custid=S.payerid 
				where   S.applicationid=@applicationid and S.enterpriseid=@enterpriseid and 
						S.consignment_no=@consignment_no and S.booking_no=@booking_no and 
						CEC.exception_code in (@exception_code)   ";

			object surcharge=null; //Default
			try
			{	
				surcharge = dbCon.ExecuteScalarInTransaction(dbCmd);
				if(surcharge!=null && surcharge!=DBNull.Value) surchargeRef=Convert.ToDecimal(surcharge);
			}			
			catch(Exception exception)
			{
				Logger.LogTraceError("ShipmentUpdateMgrDAL","getTrackingRecords","ERR002","Error : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}
		}
		public static DataSet GetCoreSysetmCodeNumVal(String strAppID, String strEnterpriseID,
			String strCodeID, int iCurRow, int iDSRowSize)
		{
			SessionDS sessionDS = null;			
			String strSQL = "";

			strSQL = "SELECT code_num_value "; 	
			strSQL += "FROM core_system_code with(nolock) ";
			strSQL += "WHERE applicationid = '" + strAppID + "' ";
			strSQL += "AND codeid = '" + strCodeID + "' ";
			strSQL += "ORDER BY sequence asc ";
	
			
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("ImportConsignmentsDAL","GetCoreSysetmCodeNumVal","GetCoreSysetmCodeNumVal","DbConnection object is null!!");
				return sessionDS.ds;
			}
			
			
			sessionDS = dbCon.ExecuteQuery(strSQL, iCurRow, iDSRowSize, "core_system_code_num_val");
			return  sessionDS.ds;
		}

		public static DataSet getHCFromShipment(String strAppID,String strEnterpriseID, int iBookingNo, String ConsignNo)
		{
			DataSet dsTrackingRecord = null;
			IDbCommand dbCmd = null;
			
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("shipmentUpdateMgrDAL","getHCFromShipment","ERR001","DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null",null);
			}

			IDbConnection conApp = dbCon.GetConnection();
			if(conApp == null)
			{
				Logger.LogTraceError("ShipmentUpdateMgrDAL","getHCFromShipment","ERR003","conApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			try
			{
				dbCon.OpenConnection(ref conApp);
			}
			catch(ApplicationException appException)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("ShipmentUpdateMgrDAL","getHCFromShipment","ERR004","Error opening connection : "+ appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ",appException);
			}
			catch(Exception exception)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("ShipmentUpdateMgrDAL","getHCFromShipment","ERR005","Error opening connection : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}
			IDbTransaction transactionApp = dbCon.BeginTransaction(conApp,IsolationLevel.ReadCommitted);

			if(transactionApp == null)
			{
				Logger.LogTraceError("ShipmentUpdateMgrDAL","getHCFromShipment","ERR006","transactionApp is null");
				throw new ApplicationException("Failed Begining a App Transaction..",null);
			}
			if(dbCon == null)
			{
				Logger.LogTraceError("ShipmentUpdateMgrDAL","getHCFromShipment","ERR001","DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null",null);
			}

			string strqry="SELECT * ";
			strqry += "FROM   Shipment with(nolock) ";
			strqry += "WHERE (applicationid = '"+strAppID+"') ";
			strqry += "AND (enterpriseid = '"+strEnterpriseID+"') ";
			strqry += "AND (booking_no = "+iBookingNo+") ";
			strqry += "AND (consignment_no = '"+ConsignNo+"') ";

			
			try
			{	
				dbCmd = dbCon.CreateCommand(strqry,CommandType.Text);
				dbCmd.Connection = conApp;
				dbCmd.Transaction = transactionApp;
				dsTrackingRecord =(DataSet)dbCon.ExecuteQueryInTransaction(dbCmd,com.common.DAL.ReturnType.DataSetType);
				
			}
			
			catch(Exception exception)
			{
				Logger.LogTraceError("ShipmentUpdateMgrDAL","getHCFromShipment","ERR002","Error : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}
			return dsTrackingRecord;

		}

		//HC Return Task

//		public static DataSet getRouteDetails(String strAppID,String strEnterpriseID,string pathCode, string flightVehicleNo, DateTime dtDelManifestDateTime)
//		{
//
//			IDbCommand dbCmd = null;
//			StringBuilder strBuild = null;
//			DataSet dsManifest = null;
//			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
//			if(dbCon == null)
//			{
//				Logger.LogTraceError("ShipmentUpdateMgrDAL","getRouteDetails","ERR001","DbConnection object is null!!");
//				throw new ApplicationException("DbConnection object is null",null);
//			}
//
//			String strDelManifestDateTime = Utility.DateFormat(strAppID, strEnterpriseID,dtDelManifestDateTime,DTFormat.DateTime);
//			
//			strBuild =  new StringBuilder();
//			strBuild.Append("select * ");
//			strBuild.Append("from delivery_manifest_detail ");
//			strBuild.Append("where path_code = '" + pathCode + "' ");
//			strBuild.Append("and flight_vehicle_no = '" + flightVehicleNo + "' ");
//			strBuild.Append("and delivery_manifest_datetime = " + strDelManifestDateTime);
//			
//			try
//			{
//				dbCmd = dbCon.CreateCommand(strBuild.ToString(),CommandType.Text);
//				dsManifest =(DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
//			}
//			catch(ApplicationException appException)
//			{
//				Logger.LogTraceError("ShipmentUpdateMgrDAL","getRouteDetails","ERR002","Error : "+ appException.Message.ToString());
//				throw new ApplicationException("Error opening database connection ",appException);
//			}
//			catch(Exception exception)
//			{
//				Logger.LogTraceError("ShipmentUpdateMgrDAL","getRouteDetails","ERR003","Error : "+ exception.Message.ToString());
//				throw new ApplicationException("Error opening database connection ",exception);
//			}
//			return dsManifest;
//
//		}

		public static bool IsDupTrackDT_SHP(String strAppID,String strEnterpriseID,DateTime status_dt,String ConsignNo,String BookingNo)
		{

			DataSet dsTrack = null;
			IDbCommand dbCmd = null;
			StringBuilder strBuild = null;
			int count = 0;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("ShipmentUpdateMgrDAL","getRouteDetails","ERR001","DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null",null);
			}
	
			strBuild = new StringBuilder();

			try
			{
				strBuild.Append("select * from shipment_tracking with(nolock) ");
				strBuild.Append("WHERE ");
				strBuild.Append(" tracking_datetime = "+Utility.DateFormat(strAppID, strEnterpriseID, status_dt, DTFormat.DateLongTime));
				if(common.util.Utility.IsNotDBNull(ConsignNo) && ConsignNo != "")
				{
					strBuild.Append(" and consignment_no = '"+ConsignNo+"' ");
				}

				if(common.util.Utility.IsNotDBNull(BookingNo))
				{
					strBuild.Append(" and booking_no = " +Convert.ToInt32(BookingNo) );
				}


				dbCmd = dbCon.CreateCommand(strBuild.ToString(),CommandType.Text);
				dsTrack =(DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
			}
			
			catch(Exception exception)
			{
				Logger.LogTraceError("ShipmentUpdateMgrDAL","getHCFromShipment","ERR002","Error : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}
			
			count = dsTrack.Tables[0].Rows.Count;

			if(count>0)	
			{
				return true;
			}
			else 
			{
				return false;
			}
		}

		public static bool IsDupTrackDT_SHP_AndStaging(String strAppID,String strEnterpriseID,DateTime status_dt,String ConsignNo,String BookingNo)
		{

			DataSet dsTrack = null;
			IDbCommand dbCmd = null;
			StringBuilder strBuild = null;
			int count = 0;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("ShipmentUpdateMgrDAL","getRouteDetails","ERR001","DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null",null);
			}
	
			strBuild = new StringBuilder();

			try
			{
				strBuild.Append("select consignment_no from MF_Shipment_Tracking_Staging with(nolock) ");
				strBuild.Append("WHERE ");
				strBuild.Append(" tracking_datetime = "+Utility.DateFormat(strAppID, strEnterpriseID, status_dt, DTFormat.DateLongTime));
				if(common.util.Utility.IsNotDBNull(ConsignNo) && ConsignNo != "")
				{
					strBuild.Append(" and consignment_no = '"+ConsignNo+"' ");
				}

				if(common.util.Utility.IsNotDBNull(BookingNo))
				{
					strBuild.Append(" and booking_no = " +Convert.ToInt32(BookingNo) );
				}

				strBuild.Append(" union select consignment_no from shipment_tracking with(nolock) ");
				strBuild.Append("WHERE ");
				strBuild.Append(" tracking_datetime = "+Utility.DateFormat(strAppID, strEnterpriseID, status_dt, DTFormat.DateLongTime));
				if(common.util.Utility.IsNotDBNull(ConsignNo) && ConsignNo != "")
				{
					strBuild.Append(" and consignment_no = '"+ConsignNo+"' ");
				}

				if(common.util.Utility.IsNotDBNull(BookingNo))
				{
					strBuild.Append(" and booking_no = " +Convert.ToInt32(BookingNo) );
				}


				dbCmd = dbCon.CreateCommand(strBuild.ToString(),CommandType.Text);
				dsTrack =(DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
			}
			
			catch(Exception exception)
			{
				Logger.LogTraceError("ShipmentUpdateMgrDAL","getHCFromShipment","ERR002","Error : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}
			
			count = dsTrack.Tables[0].Rows.Count;

			if(count>0)	
			{
				return true;
			}
			else 
			{
				return false;
			}
		}
	
	
/*
		public static bool IsDupTrackDT_DISP(String strAppID,String strEnterpriseID,DateTime status_dt,long BookNo)
		{

			DataSet dsTrack = null;
			IDbCommand dbCmd = null;
			StringBuilder strBuild = null;
			int count = 0;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("ShipmentUpdateMgrDAL","getRouteDetails","ERR001","DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null",null);
			}
	
			strBuild = new StringBuilder();

			try
			{
				strBuild.Append("select * from Dispatch_tracking ");
				strBuild.Append("WHERE booking_no = "+BookNo.ToString() );
				strBuild.Append(" and tracking_datetime = "+Utility.DateFormat(strAppID, strEnterpriseID, status_dt, DTFormat.DateLongTime));

				
				dbCmd = dbCon.CreateCommand(strBuild.ToString(),CommandType.Text);
				dsTrack =(DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
			}
			
			catch(Exception exception)
			{
				Logger.LogTraceError("ShipmentUpdateMgrDAL","getHCFromShipment","ERR002","Error : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}
			
			count = dsTrack.Tables[0].Rows.Count;

			if(count>0)	
			{
				return true;
			}
			else 
			{
				return false;
			}
		}
		
		*/


		/// <summary>
		/// Modified by Sompote 2010-04-05
		/// </summary>
		/// <param name="strAppID"></param>
		/// <param name="strEnterpriseID"></param>
		/// <param name="status_dt"></param>
		/// <param name="BookNo"></param>
		/// <returns></returns>
		public static bool IsDupTrackDT_DISP(String strAppID,String strEnterpriseID,DateTime status_dt,long BookNo)
		{
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("ShipmentUpdateMgrDAL","getRouteDetails","ERR001","DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null",null);
			}
			return IsDupTrackDT_DISP( strAppID, strEnterpriseID, status_dt, BookNo, dbCon);
		}
		
		/// <summary>
		/// Modified by Sompote 2010-04-05
		/// </summary>
		/// <param name="strAppID"></param>
		/// <param name="strEnterpriseID"></param>
		/// <param name="status_dt"></param>
		/// <param name="BookNo"></param>
		/// <returns></returns>
		public static bool IsDupTrackDT_DISP(String strAppID,String strEnterpriseID,DateTime status_dt,long BookNo,DbConnection dbCon)
		{
			IDbConnection conApp = dbCon.GetConnection();
			if(conApp == null)
			{
				Logger.LogTraceError("ShipmentUpdateMgrDAL","getRouteDetails","ERR003","conApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			try
			{
				dbCon.OpenConnection(ref conApp);
			}
			catch(ApplicationException appException)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("ShipmentUpdateMgrDAL","getRouteDetails","ERR004","Error opening connection : "+ appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ",appException);
			}
			catch(Exception exception)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("ShipmentUpdateMgrDAL","getRouteDetails","ERR005","Error opening connection : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}
			
			IDbCommand dbCmd = null;		
			dbCmd = dbCon.CreateCommand("",CommandType.Text);
			dbCmd.Connection = conApp;	
			//return IsDupTrackDT_DISP( strAppID, strEnterpriseID, status_dt, BookNo, dbCon, dbCmd);
			
			//SessionDS sessionDS ;	
			bool result = false; 
			try
			{
				result = IsDupTrackDT_DISP( strAppID, strEnterpriseID, status_dt, BookNo, dbCon, dbCmd);
			}
			catch(ApplicationException appExpection)
			{
				throw appExpection;
			}
			finally
			{
				if(conApp != null)
				{
					conApp.Close();
				}
			}
			return result;
		}
		
		/// <summary>
		/// Add by Sompote 2010-04-05
		/// </summary>
		/// <param name="strAppID"></param>
		/// <param name="strEnterpriseID"></param>
		/// <param name="status_dt"></param>
		/// <param name="BookNo"></param>
		/// <param name="dbCon"></param>
		/// <param name="dbCmd"></param>
		/// <returns></returns>
		public static bool IsDupTrackDT_DISP(String strAppID,String strEnterpriseID,DateTime status_dt,long BookNo,DbConnection dbCon,IDbCommand dbCmd)
		{
			DataSet dsTrack = null;
			//IDbCommand dbCmd = null;
			StringBuilder strBuild = null;
			int count = 0;
			/*DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("ShipmentUpdateMgrDAL","getRouteDetails","ERR001","DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null",null);
			}*/
	
			strBuild = new StringBuilder();

			try
			{
				strBuild.Append("select * from Dispatch_tracking with(nolock) ");
				strBuild.Append("WHERE booking_no = "+BookNo.ToString() );
				strBuild.Append(" and tracking_datetime = "+Utility.DateFormat(strAppID, strEnterpriseID, status_dt, DTFormat.DateLongTime));

				
				//dbCmd = dbCon.CreateCommand(strBuild.ToString(),CommandType.Text);
				dbCmd.CommandText = strBuild.ToString();
				dsTrack =(DataSet)dbCon.ExecuteQueryInTransaction(dbCmd,com.common.DAL.ReturnType.DataSetType);
			}
			
			catch(Exception exception)
			{
				Logger.LogTraceError("ShipmentUpdateMgrDAL","getHCFromShipment","ERR002","Error : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}
			
			count = dsTrack.Tables[0].Rows.Count;

			if(count>0)	
			{
				return true;
			}
			else 
			{
				return false;
			}
		}
	
		public static int MF_Shipment_CheckDupPOD(String strAppID, String strEnterpriseID, String strConsignment_no)
		{
			IDbCommand dbCmd = null;
			DbConnection dbCon= DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if (dbCon==null)
			{
				throw new ApplicationException("Connection to Database failed",null);
			}
			int iRowsEffected = 0;
			String strSQL="";
			try
			{
				strSQL="";
				strSQL+=" select consignment_no from MF_Shipment_Tracking_Staging with(nolock) ";
				strSQL+=" WHERE (applicationid = '"+strAppID+"') ";
				strSQL+=" and (enterpriseid = '"+strEnterpriseID+"') ";
				strSQL+=" and (consignment_no = '"+strConsignment_no+"')";
				strSQL+=" and (status_code ='POD')";
				strSQL+=" and (isnull(deleted,'') <> 'Y' and isnull(Process_flag,'')<>'Y')";
				strSQL+=" Union ";
				strSQL+=" select consignment_no from Shipment_Tracking with(nolock) ";
				strSQL+=" WHERE (applicationid = '"+strAppID+"') ";
				strSQL+=" and (enterpriseid = '"+strEnterpriseID+"') ";
				strSQL+=" and (consignment_no = '" + strConsignment_no + "')";
				strSQL+=" and (status_code ='POD')";
				strSQL+=" and (isnull(deleted,'') <> 'Y')";

				dbCmd = dbCon.CreateCommand(strSQL,CommandType.Text);
				DataSet dsHas =(DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
				if(dsHas!=null)
				{
					if(dsHas.Tables[0]!=null)
					{
						if(dsHas.Tables[0].Rows.Count>0)
						{
							return -1;
						}
					}
				}
			}
			catch(ApplicationException ex)
			{
				throw ex;
			}
			return iRowsEffected;
		}

		public static int MF_Shipment_Update(String strAppID, String strEnterpriseID, String strBatchNo, String strStatusCode, DateTime StatusDT, String strRemarks, String strUserID, String strLocation,String strPersonIncharge,String strConsingnee)
		{
			IDbCommand dbCmd = null;
			DbConnection dbCon= DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if (dbCon==null)
			{
				throw new ApplicationException("Connection to Database failed",null);
			}
			int iRowsEffected = 0;
			try
			{
				String strSQL="";
				strSQL +=" select Distinct '"+strAppID+"',";
				strSQL +=" '" + strEnterpriseID +"',";
				strSQL +=" MF_Shipment_Tracking_Staging.consignment_no,";
				strSQL +=" '"+ StatusDT +"',";
				strSQL +=" 0,"; //MF_Shipment_Tracking_Staging.booking_no,";
				strSQL +=" '"+strStatusCode+"',";
				strSQL +=" 0,";
				strSQL +=" N'"+strPersonIncharge+"',";
				strSQL +=" N'"+strRemarks+"',";
				strSQL +=" '"+strUserID+"',";
				strSQL +=" getdate(),";
				strSQL +=" (select invoiceable from status_code with(nolock) where status_code = '"+strStatusCode+"'),";
				strSQL +=" '"+strLocation+"',";
				strSQL +=" 'N',";
				strSQL +=" '"+ strBatchNo +"'";
				if(strStatusCode=="POD")
				{
					strSQL +=",'"+ strConsingnee +"'";
				}
				strSQL +=" FROM  MF_Shipment_Tracking_Staging with(nolock) INNER JOIN ";
				strSQL +=" MF_Batch_Manifest_Detail with(nolock) ON MF_Shipment_Tracking_Staging.applicationid = MF_Batch_Manifest_Detail.applicationID AND ";
				strSQL +=" MF_Shipment_Tracking_Staging.consignment_no = MF_Batch_Manifest_Detail.Consignment_No AND ";
				strSQL +=" MF_Shipment_Tracking_Staging.enterpriseid = MF_Batch_Manifest_Detail.enterpriseID ";
				strSQL+=" WHERE (MF_Shipment_Tracking_Staging.applicationid = '"+strAppID+"') and (MF_Shipment_Tracking_Staging.enterpriseid = '"+strEnterpriseID+"') and (MF_Batch_Manifest_Detail.Batch_No = '"+strBatchNo+"') and  (MF_Batch_Manifest_Detail.Shipped_Pkg_Qty > MF_Batch_Manifest_Detail.Recv_Pkg_Qty)";

				dbCmd = dbCon.CreateCommand(strSQL,CommandType.Text);
				DataSet ds =(DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);

				if(ds!=null)
				{
					if(ds.Tables[0]!=null)
					{
						if(ds.Tables[0].Rows.Count==0)
						{
							return 0;
						}
					}
					else
					{
						return 0;
					}
				}
				else
				{
					return 0;
				}

				if(strStatusCode=="POD")
				{
					strSQL="";
					strSQL+=" select consignment_no from MF_Shipment_Tracking_Staging with(nolock) ";
					strSQL+=" WHERE (applicationid = '"+strAppID+"') ";
					strSQL+=" and (enterpriseid = '"+strEnterpriseID+"') ";
					strSQL+=" and (Batch_No = '"+strBatchNo+"')";
					strSQL+=" and (status_code ='POD')";
					strSQL+=" and (isnull(deleted,'') <> 'Y' and isnull(Process_flag,'')<>'Y')";
					strSQL+=" Union ";
					strSQL+=" select consignment_no from Shipment_Tracking with(nolock) ";
					strSQL+=" WHERE (applicationid = '"+strAppID+"') ";
					strSQL+=" and (enterpriseid = '"+strEnterpriseID+"') ";
					strSQL+=" and (consignment_no in (select consignment_no from MF_Shipment_Tracking_Staging with(nolock) WHERE (Batch_No = '" + strBatchNo + "')))";
					strSQL+=" and (status_code ='POD')";
					strSQL+=" and (isnull(deleted,'') <> 'Y')";
					dbCmd = dbCon.CreateCommand(strSQL,CommandType.Text);
					DataSet dsHas =(DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
					if(dsHas!=null)
					{
						if(dsHas.Tables[0]!=null)
						{
							if(dsHas.Tables[0].Rows.Count>0)
							{
								return -1;
							}
						}
					}

				}

				strSQL="";
				strSQL =" insert into MF_Shipment_Tracking_Staging with(rowlock) (applicationid,";
				strSQL +=" enterpriseid,";
				strSQL +=" consignment_no,";
				strSQL +=" tracking_datetime,";
				strSQL +=" booking_no,";
				strSQL +=" status_code,";
                strSQL +=" pkg_no,";
				strSQL +=" person_incharge,";
				strSQL +=" remark,";
				strSQL +=" last_userid,";
				strSQL +=" last_updated,";
				strSQL +=" invoiceable,";
				strSQL +=" location,";
				strSQL +=" Process_Flag,";
				strSQL +=" batch_no";
				if(strStatusCode=="POD")
				{
						strSQL +=",consignee_name";
				}
				strSQL +=")";
				strSQL +=" select Distinct '"+strAppID+"',";
				strSQL +=" '" + strEnterpriseID +"',";
				strSQL +=" MF_Shipment_Tracking_Staging.consignment_no,";
				strSQL +=" '"+ StatusDT +"',";
				strSQL +=" 0,"; //MF_Shipment_Tracking_Staging.booking_no,";
				strSQL +=" '"+strStatusCode+"',";
				strSQL +=" 0,";
				strSQL +=" N'"+strPersonIncharge+"',";
				strSQL +=" N'"+strRemarks+"',";
				strSQL +=" '"+strUserID+"',";
				strSQL +=" getdate(),";
				strSQL +=" (select invoiceable from status_code with(nolock) where status_code = '"+strStatusCode+"'),";
				strSQL +=" '"+strLocation+"',";
				strSQL +=" 'N',";
				strSQL +=" '"+ strBatchNo +"'";
				if(strStatusCode=="POD")
				{
					strSQL +=",'"+ strConsingnee +"'";
				}
				strSQL +=" FROM  MF_Shipment_Tracking_Staging with(nolock) INNER JOIN ";
                strSQL +=" MF_Batch_Manifest_Detail with(nolock) ON MF_Shipment_Tracking_Staging.applicationid = MF_Batch_Manifest_Detail.applicationID AND ";
                strSQL +=" MF_Shipment_Tracking_Staging.consignment_no = MF_Batch_Manifest_Detail.Consignment_No AND ";
                strSQL +=" MF_Shipment_Tracking_Staging.enterpriseid = MF_Batch_Manifest_Detail.enterpriseID ";
				strSQL+=" WHERE (MF_Shipment_Tracking_Staging.applicationid = '"+strAppID+"') and (MF_Shipment_Tracking_Staging.enterpriseid = '"+strEnterpriseID+"') and (MF_Batch_Manifest_Detail.Batch_No = '"+strBatchNo+"') and  (MF_Batch_Manifest_Detail.Shipped_Pkg_Qty > MF_Batch_Manifest_Detail.Recv_Pkg_Qty)";
				dbCmd = dbCon.CreateCommand(strSQL, CommandType.Text);
				dbCmd.CommandTimeout = 300;  //Add Command TimeOut for case of serveral records: 300 seconds (5 minutes)
				iRowsEffected = dbCon.ExecuteNonQuery(dbCmd);
				
			}
			catch(ApplicationException ex)
			{
				throw ex;
			}
			return iRowsEffected;
		}

		public static int MF_Shipment_Update_by_Batch(String strAppID, String strEnterpriseID, String strBatchNo, String strStatusCode, DateTime StatusDT, String strRemarks, String strUserID, String strLocation,String strPersonIncharge,String strConsingnee)
		{
			int iRowsEffected = 0;
			IDbCommand dbCmd = null;
			DbConnection dbCon= DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if (dbCon==null)
			{
				throw new ApplicationException("Connection to Database failed",null);
			}
			try
			{
				string strSQL = "";
				strSQL +=" SELECT consignment_no ";
				strSQL +=" , tracking_datetime = '"+ StatusDT + "' ";
				strSQL +=" , status_code = '" + strStatusCode + "' ";
				strSQL +=" , person_incharge = '" + strPersonIncharge + "' ";
				strSQL +=" , remark =  '" + strRemarks + "' ";
				strSQL +=" , consignee_name =  '" + strConsingnee + "' ";
				strSQL +=" , batch_no =  '" + strBatchNo + "' ";
				strSQL +=" FROM  dbo.MF_Batch_Manifest_Detail WITH(NOLOCK) ";
				strSQL +=" WHERE applicationid = '" + strAppID + "' ";
				strSQL +=" AND enterpriseid = '" + strEnterpriseID + "' ";
				strSQL +=" AND batch_no =  '" + strBatchNo + "' ";
				strSQL +=" AND Shipped_Pkg_Qty > Recv_Pkg_Qty ";
				dbCmd = dbCon.CreateCommand(strSQL,CommandType.Text);
				DataSet ds =(DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
				if(ds != null && ds.Tables.Count>0 && ds.Tables[0].Rows.Count>0)
				{
					string xmlData = ConvertDataSetToXML(ds);
					DataSet result = InsertStaging(strAppID,strEnterpriseID,strUserID,xmlData,0,1,0,strLocation);
					int tbResult=-1;
					int tbCount = -1;
					for(int i=0;i < result.Tables.Count;i++)
					{
						if(result.Tables[i].Columns["booking_no"] != null)
						{
							tbResult = i;
						}
						if(result.Tables[i].Columns["Status_Inserted_Count"] != null)
						{
							tbCount=i;
						}
					}

					if(result.Tables[tbResult].Rows.Count <= 0 || (result.Tables[tbResult].Rows.Count>0 
						&& result.Tables[tbResult].Rows[0]["error"].ToString().Equals("21")==true))
					{
						result = InsertStaging(strAppID,strEnterpriseID,strUserID,xmlData,1,1,0,strLocation);
						for(int i=0;i < result.Tables.Count;i++)
						{
							if(result.Tables[i].Columns["booking_no"] != null)
							{
								tbResult = i;
							}
							if(result.Tables[i].Columns["Status_Inserted_Count"] != null)
							{
								tbCount=i;
							}
						}
						
						if(result.Tables[tbResult].Rows.Count > 0)
						{
							iRowsEffected=-1;
						}
						else
						{
							iRowsEffected=int.Parse(result.Tables[tbCount].Rows[0]["Status_Inserted_Count"].ToString());
						}
					}
					else
					{
						iRowsEffected= -1;
					}
				}
				else
				{
					iRowsEffected=-2;
				}
			}
			catch(ApplicationException ex)
			{
				throw ex;
			}
			return iRowsEffected;
		}
		public static System.Data.DataSet InsertStaging(string strAppID, string strEnterpriseID, string strUserID,string pathFn,string local)
		{
			System.Data.DataSet result = new DataSet();
			DbConnection dbCon= DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			IDbCommand dbcmd = null;
			
			if (dbCon==null)
			{
				throw new ApplicationException("Connection to Database failed",null);
			}
			try
			{
				string xmlDoc = "<st><r filename=\""+pathFn+"\" sheetname=\"Sheet1\" /></st>";
				
				System.Collections.ArrayList storedParams = new System.Collections.ArrayList();
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@userid",strUserID));
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@stXML",xmlDoc));
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@mode",1));
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@return",1));
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@xmlmode",1));
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@user_loc1",local));
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@enterpriseid",strEnterpriseID));
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@applicationid",strAppID));
				dbcmd = dbCon.CreateCommand("InsertStaging", storedParams, CommandType.StoredProcedure);

//				IDbCommand arithabortCommand = dbCon.CreateCommand("SET ARITHABORT ON",CommandType.Text);
//				arithabortCommand.ExecuteNonQuery();

				result = (DataSet)dbCon.ExecuteQuery(dbcmd, com.common.DAL.ReturnType.DataSetType);  
			}
			catch(ApplicationException ex)
			{
				throw ex;
			}
			return result;
		}

		public static System.Data.DataSet InsertStaging(string strAppID, string strEnterpriseID, string strUserID,string xmlDoc 
				,int mode, int returnmode, int xmlmode,string local)
		{
			System.Data.DataSet result = new DataSet();
			DbConnection dbCon= DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			IDbCommand dbcmd = null;
			
			if (dbCon==null)
			{
				throw new ApplicationException("Connection to Database failed",null);
			}
			try
			{

				System.Collections.ArrayList storedParams = new System.Collections.ArrayList();
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@userid",strUserID));
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@stXML",xmlDoc));
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@mode",mode));
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@return",returnmode));
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@xmlmode",xmlmode));
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@user_loc1",local));
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@enterpriseid",strEnterpriseID));
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@applicationid",strAppID));
				dbcmd = dbCon.CreateCommand("InsertStaging", storedParams, CommandType.StoredProcedure);

				//				IDbCommand arithabortCommand = dbCon.CreateCommand("SET ARITHABORT ON",CommandType.Text);
				//				arithabortCommand.ExecuteNonQuery();

				result = (DataSet)dbCon.ExecuteQuery(dbcmd, com.common.DAL.ReturnType.DataSetType);  
			}
			catch(ApplicationException ex)
			{
				throw ex;
			}
			return result;
		}
		// Function to convert passed dataset to XML data
		public static string ConvertDataSetToXML(DataSet xmlDS)
		{
			System.IO.MemoryStream stream = null;
			System.Xml.XmlTextWriter writer = null;
			try
			{
				xmlDS.DataSetName="st";
				xmlDS.AcceptChanges();

				for(int i=0;i<xmlDS.Tables[0].Columns.Count;i++)
				{
					xmlDS.Tables[0].Columns[i].ColumnMapping=MappingType.Attribute;
				}
				xmlDS.Tables[0].TableName="r";
				xmlDS.Tables[0].AcceptChanges();
				stream = new System.IO.MemoryStream();
				// Load the XmlTextReader from the stream
				writer = new System.Xml.XmlTextWriter(stream, Encoding.Unicode);
				// Write to the file with the WriteXml method.
				xmlDS.WriteXml(writer,XmlWriteMode.IgnoreSchema);
				int count = (int) stream.Length;
				byte[] arr = new byte[count];
				stream.Seek(0, System.IO.SeekOrigin.Begin);
				stream.Read(arr, 0, count);
				UnicodeEncoding utf = new UnicodeEncoding();
				return utf.GetString(arr).Trim();
			}
			catch
			{
				return String.Empty ;
			}
			finally
			{
				if(writer != null) writer.Close();
			}
		}

	}
}
