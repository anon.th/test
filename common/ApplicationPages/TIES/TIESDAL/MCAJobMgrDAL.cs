using System;
using System.Collections;
using System.Data;
using com.common.DAL;
using com.common.util;
using com.common.classes;
using com.ties.classes;
using System.Text;

namespace com.ties.DAL
{
	/// <summary>
	/// Summary description for MCAJobMgrDAL.
	/// </summary>
	public class MCAJobMgrDAL
	{
		public MCAJobMgrDAL()
		{
			//
			// TODO: Add constructor logic here
			//
		}


		public static SessionDS GetEmptyMCAJobDS(int iNumRows)
		{
			

			DataTable dtMCAJob = new DataTable();
 
			dtMCAJob.Columns.Add(new DataColumn("jobid", typeof(long)));
			dtMCAJob.Columns.Add(new DataColumn("account_year", typeof(int)));
			dtMCAJob.Columns.Add(new DataColumn("account_month", typeof(short)));
			dtMCAJob.Columns.Add(new DataColumn("job_status",typeof(string)));
			dtMCAJob.Columns.Add(new DataColumn("start_datetime",typeof(DateTime)));
			dtMCAJob.Columns.Add(new DataColumn("end_datetime",typeof(DateTime)));
			dtMCAJob.Columns.Add(new DataColumn("remark",typeof(string)));


			for(int i=0; i < iNumRows; i++)
			{
				DataRow drEach = dtMCAJob.NewRow();

				dtMCAJob.Rows.Add(drEach);
			}

			DataSet dsMCAJob = new DataSet();
			dsMCAJob.Tables.Add(dtMCAJob);

			SessionDS sessionDS = new SessionDS();
			sessionDS.ds = dsMCAJob;
			sessionDS.DataSetRecSize = dsMCAJob.Tables[0].Rows.Count;
			sessionDS.QueryResultMaxSize = sessionDS.DataSetRecSize;

			return  sessionDS;
	
		}

		public static void AddNewRowInMCAJobDS(SessionDS sessionDS)
		{
			DataRow drNew = sessionDS.ds.Tables[0].NewRow();

			drNew[0] = "";
			drNew[1] = "";

			sessionDS.ds.Tables[0].Rows.Add(drNew);
			sessionDS.QueryResultMaxSize++;


		}


		public static SessionDS GetMCAJobDS(String strAppID, String strEnterpriseID, int iCurrent, int iDSRecSize,DataSet dsQueryParam)
		{

			SessionDS sessionDS = null;


			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("MCAJobMgrDAL","GetMCAJobDS","EDB101","DbConnection object is null!!");
				return sessionDS;
			}

			StringBuilder strBuilder = new StringBuilder();
			strBuilder.Append("select jobid, account_year, account_month, job_status, start_datetime,end_datetime,remark from Cost_Allocation_Task where applicationid = '");
			strBuilder.Append(strAppID);
			strBuilder.Append("' and enterpriseid = '");
			strBuilder.Append(strEnterpriseID);
			strBuilder.Append("' ");

			DataRow drEach = dsQueryParam.Tables[0].Rows[0];


			if(Utility.IsNotDBNull(drEach["jobid"]))
			{
				decimal iJobID = Convert.ToDecimal(drEach["jobid"]);
				strBuilder.Append(" and jobid = ");
				strBuilder.Append(iJobID);
				strBuilder.Append(" ");
			}

			if((drEach["account_year"] != null) && (!drEach["account_year"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				int iAccountYear = Convert.ToInt32(drEach["account_year"]);
				strBuilder.Append(" and account_year = ");
				strBuilder.Append(iAccountYear);
				strBuilder.Append(" ");

			}

			if((drEach["account_month"] != null) && (!drEach["account_month"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				int iAccountMonth = (short) drEach["account_month"];
				strBuilder.Append(" and account_month = ");
				strBuilder.Append(iAccountMonth);
				strBuilder.Append(" ");

			}

			if((drEach["job_status"] != null) && (!drEach["job_status"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				String strJobStatus = (String) drEach["job_status"];
				strBuilder.Append(" and job_status = '");
				strBuilder.Append(strJobStatus);
				strBuilder.Append("' ");
			}

			if((drEach["start_datetime"] != null) && (!drEach["start_datetime"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				DateTime dtStartDateTime = (DateTime) drEach["start_datetime"];
				strBuilder.Append(" and start_datetime = ");
				String strStartDateTime = Utility.DateFormat(strAppID,strEnterpriseID,dtStartDateTime,DTFormat.DateTime);
				strBuilder.Append(strStartDateTime);
				strBuilder.Append(" ");
			}

			if((drEach["end_datetime"] != null) && (!drEach["end_datetime"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				DateTime dtEndDateTime = (DateTime) drEach["end_datetime"];
				strBuilder.Append(" and end_datetime = ");
				String strEndDateTime = Utility.DateFormat(strAppID,strEnterpriseID,dtEndDateTime,DTFormat.DateTime);
				strBuilder.Append(strEndDateTime);
				strBuilder.Append(" ");
			}

			if((drEach["remark"] != null) && (!drEach["remark"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				String strRemark = (String) drEach["remark"];
				strBuilder.Append(" and remark like N'%");
				strBuilder.Append(Utility.ReplaceSingleQuote(strRemark));
				strBuilder.Append("%' ");
			}

			strBuilder.Append("order by start_datetime desc");
			
			String strSQLQuery = strBuilder.ToString();
			
			sessionDS = dbCon.ExecuteQuery(strSQLQuery,iCurrent,iDSRecSize,"MCAJobTable");

			return  sessionDS;
	
		}



		public static int DeleteMCAJobDS(String strAppID, String strEnterpriseID, decimal iJobID)
		{
			int iRowsDeleted = 0;

			DbConnection dbConApp = null;
			IDbCommand dbCommandApp = null;

			IDbTransaction transactionApp = null;
			IDbConnection conApp = null;

	
			//Get App dbConnection

			dbConApp = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbConApp == null)
			{
				Logger.LogTraceError("MCAJobMgrDAL","DeleteMCAJobDS","SDM002","dbConApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			//Begin Core and App Transaction

			conApp = dbConApp.GetConnection();
			
			if(conApp == null)
			{
				Logger.LogTraceError("MCAJobMgrDAL","DeleteMCAJobDS","SDM002","conApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			try
			{
				dbConApp.OpenConnection(ref conApp);
			}
			catch(ApplicationException appException)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("MCAJobMgrDAL","DeleteMCAJobDS","SDM003","Error opening connection : "+ appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ",appException);
			}
			catch(Exception exception)
			{
				if(conApp != null)
				{
					conApp.Close();
				}

				Logger.LogTraceError("MCAJobMgrDAL","DeleteMCAJobDS","SDM003","Error opening connection : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}


			transactionApp = dbConApp.BeginTransaction(conApp,IsolationLevel.ReadCommitted);

			if(transactionApp == null)
			{
				Logger.LogTraceError("MCAJobMgrDAL","DeleteMCAJobDS","SDM002","transactionApp is null");
				throw new ApplicationException("Failed Begining a App Transaction..",null);
			}


			try
			{

				//Update record into core_enterprise Table

				StringBuilder strBuilder = new StringBuilder();
			
				strBuilder.Append("Delete from Shipment_Cost where ");
				strBuilder.Append(" applicationid = '");
				strBuilder.Append(strAppID);
				strBuilder.Append("' and enterpriseid = '");
				strBuilder.Append(strEnterpriseID);
				strBuilder.Append("' and jobid = ");
				strBuilder.Append(iJobID);


				String strSQLQuery = strBuilder.ToString();
				dbCommandApp = dbConApp.CreateCommand(strSQLQuery,CommandType.Text); 
				dbCommandApp.Connection = conApp;
				dbCommandApp.Transaction = transactionApp;

				iRowsDeleted = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);
				Logger.LogTraceInfo("MCAJobMgrDAL","DeleteMCAJobDS","INF001",iRowsDeleted + " rows deleted from Exception_Code table");

				strBuilder = new StringBuilder();
			
				strBuilder.Append("update Monthly_Cost_Account set allocated = 'N', jobid = null");
				strBuilder.Append(" where applicationid = '");
				strBuilder.Append(strAppID);
				strBuilder.Append("' and enterpriseid = '");
				strBuilder.Append(strEnterpriseID);
				strBuilder.Append("' and jobid = ");
				strBuilder.Append(iJobID);


				strSQLQuery = strBuilder.ToString();

				dbCommandApp.CommandText = strSQLQuery;


				iRowsDeleted = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);
				Logger.LogTraceInfo("MCAJobMgrDAL","DeleteMCAJobDS","INF001",iRowsDeleted + " rows deleted from Status_Code table");

				strBuilder = new StringBuilder();
			
				strBuilder.Append("Delete from Cost_Allocation_Task where ");
				strBuilder.Append(" applicationid = '");
				strBuilder.Append(strAppID);
				strBuilder.Append("' and enterpriseid = '");
				strBuilder.Append(strEnterpriseID);
				strBuilder.Append("' and jobid = ");
				strBuilder.Append(iJobID);

				strSQLQuery = strBuilder.ToString();

				dbCommandApp.CommandText = strSQLQuery;


				iRowsDeleted = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);
				Logger.LogTraceInfo("MCAJobMgrDAL","DeleteMCAJobDS","INF001",iRowsDeleted + " rows deleted from Status_Code table");
			
				//Commit application and core transaction.

				transactionApp.Commit();
				Logger.LogTraceInfo("MCAJobMgrDAL","DeleteMCAJobDS","SDM003","App db delete transaction committed.");

			}
			catch(ApplicationException appException)
			{
				
				try
				{
					transactionApp.Rollback();
					Logger.LogTraceInfo("MCAJobMgrDAL","DeleteMCAJobDS","SDM002","App db delete transaction rolled back.");

				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("MCAJobMgrDAL","DeleteMCAJobDS","SDM002","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}


				Logger.LogTraceError("MCAJobMgrDAL","DeleteMCAJobDS","SDM002","Delete Transaction Failed : "+ appException.Message.ToString());
				throw new ApplicationException("Error deleting Status Code "+appException.Message,appException);
			}
			catch(Exception exception)
			{
				try
				{
					transactionApp.Rollback();
					Logger.LogTraceInfo("MCAJobMgrDAL","DeleteMCAJobDS","SDM002","App db delete transaction rolled back.");

				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("MCAJobMgrDAL","DeleteMCAJobDS","SDM002","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}

				Logger.LogTraceError("MCAJobMgrDAL","DeleteMCAJobDS","SDM004","delete Transaction Failed : "+ exception.Message.ToString());
				throw new ApplicationException("Error deleting Status Code "+exception.Message,exception);
			}
			finally
			{
				if(conApp != null)
				{
					conApp.Close();
				}
			}
			
			return iRowsDeleted;
		}


		public static DataSet GetMCAJobStatusDS(DbConnection dbCon, IDbTransaction dbTransaction, IDbConnection con, String strAppID, String strEnterpriseID)
		{
			DataProviderType dbProvider = DbConnectionManager.GetInstance().GetDbProviderType(strAppID,strEnterpriseID);
			StringBuilder strBuilder = new StringBuilder();

			switch (dbProvider)
			{
				case DataProviderType.Sql :
				
					//Form the select query with table lock on Cost_Allocation_Task table and read the job_status 
					
					strBuilder.Append("select jobid, account_year, account_month, start_datetime, end_datetime, job_status, remark from Cost_Allocation_Task with (tablockx) where applicationid = '");
					strBuilder.Append(strAppID);
					strBuilder.Append("' and enterpriseid = '");
					strBuilder.Append(strEnterpriseID);
					strBuilder.Append("' and job_status = 'I'");
					break;
				
				case DataProviderType.Oracle:
					//Lock the Table
					IDbCommand dbCmdLock=dbCon.CreateCommand("Lock Table Cost_Allocation_Task in Exclusive mode NOWAIT",CommandType.Text);
					dbCmdLock.Connection=con;
					dbCmdLock.Transaction=dbTransaction;
					dbCon.ExecuteNonQueryInTransaction(dbCmdLock);
				
					strBuilder.Append("select jobid, account_year, account_month, start_datetime, end_datetime, job_status, remark from Cost_Allocation_Task where applicationid = '");
					strBuilder.Append(strAppID);
					strBuilder.Append("' and enterpriseid = '");
					strBuilder.Append(strEnterpriseID);
					strBuilder.Append("' and job_status = 'I'");

					break;
				
			}
				
			String strSQLQuery = strBuilder.ToString();	
			IDbCommand dbCommand = dbCon.CreateCommand(strSQLQuery,CommandType.Text);
			dbCommand.Connection = con;
			dbCommand.Transaction = dbTransaction;

			return (DataSet)dbCon.ExecuteQueryInTransaction(dbCommand,ReturnType.DataSetType);
		}

		public static decimal GetCostAllocationTaskJobID(String strAppID, String strEnterpriseID,  int iAccountYear, short iAccountMonth)
		{
			decimal iCostAllocationTaskJobID = -1;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("MonthlyCostAccountMgrDAL","GetCostCodeDS","EDB101","DbConnection object is null!!");
				return iCostAllocationTaskJobID;
			}

			StringBuilder strBuilder = new StringBuilder();

			strBuilder.Append("select distinct jobid from Cost_Allocation_Task");
			strBuilder.Append(" where applicationid = '");
			strBuilder.Append(strAppID);
			strBuilder.Append("' and enterpriseid = '");
			strBuilder.Append(strEnterpriseID);
			strBuilder.Append("' and account_year = ");
			strBuilder.Append(iAccountYear);
			strBuilder.Append(" and account_month = ");
			strBuilder.Append(iAccountMonth);

			String strSQLQuery = strBuilder.ToString();
			Object objCostAllocationTaskJobID = dbCon.ExecuteScalar(strSQLQuery);

			if(Utility.IsNotDBNull(objCostAllocationTaskJobID))
			{
				iCostAllocationTaskJobID  = Convert.ToDecimal(objCostAllocationTaskJobID);
			}

			return iCostAllocationTaskJobID;			
		}

		public static JobStatusInfo AddMCAJob(DbConnection dbCon, IDbTransaction dbTransaction, IDbConnection con, String strAppID, String strEnterpriseID, int iAccountYear, int iAccountMonth, decimal iJobID)
		{
			JobStatusInfo jobStatusInfo = new JobStatusInfo();
			StringBuilder strBuilder = new StringBuilder();

			if(iJobID == -1)
			{
				jobStatusInfo.iJobID = Counter.GetNext(strAppID,strEnterpriseID,"jobid");
				strBuilder.Append("insert into Cost_Allocation_Task (applicationid, enterpriseid, jobid, account_year, account_month, start_datetime, end_datetime, job_status, remark) values ('");
				strBuilder.Append(strAppID);
				strBuilder.Append("','");
				strBuilder.Append(strEnterpriseID);
				strBuilder.Append("',");
				strBuilder.Append(jobStatusInfo.iJobID);
				strBuilder.Append(",");
				strBuilder.Append(iAccountYear);
				strBuilder.Append(",");
				strBuilder.Append(iAccountMonth);
				strBuilder.Append(",");
				jobStatusInfo.dtStartDateTime = DateTime.Now;
				String strStartDateTime = Utility.DateFormat(strAppID,strEnterpriseID,jobStatusInfo.dtStartDateTime,DTFormat.DateTime);
				strBuilder.Append(strStartDateTime);
				jobStatusInfo.strRemark = "New Job";
				jobStatusInfo.strJobStatus = "In Progress";
				strBuilder.Append(",null,'I','New Job')");
			}
			else
			{
				jobStatusInfo.dtStartDateTime = DateTime.Now;
				String strStartDateTime = Utility.DateFormat(strAppID,strEnterpriseID,jobStatusInfo.dtStartDateTime,DTFormat.DateTime);
				jobStatusInfo.iJobID  = iJobID;
				jobStatusInfo.strRemark = "Reallocated";
				jobStatusInfo.strJobStatus = "In Progress";

				strBuilder.Append("update Cost_Allocation_Task set start_datetime = ");
				strBuilder.Append(strStartDateTime);
				strBuilder.Append(", end_datetime = null, job_status = 'I', remark = 'Reallocated' where applicationid = '");
				strBuilder.Append(strAppID);
				strBuilder.Append("' and enterpriseid = '");
				strBuilder.Append(strEnterpriseID);
				strBuilder.Append("' and jobid = ");
				strBuilder.Append(jobStatusInfo.iJobID);
			}

			String strInsertQuery = strBuilder.ToString();

			IDbCommand dbCommand = dbCon.CreateCommand(strInsertQuery,CommandType.Text);
			dbCommand.Connection = con;
			dbCommand.Transaction = dbTransaction;

			int iRowsInserted = dbCon.ExecuteNonQueryInTransaction(dbCommand);

			return jobStatusInfo;
		}

		public static int UpdateMCAJobStatus(String strAppID, String strEnterpriseID,decimal iJobID, DateTime dtEndDateTime, String strJobStatus, String strRemark)
		{
			int iRowsAffected = 0;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("MCAJobMgrDAL","ModifyMCAJob","EDB101","DbConnection object is null!!");
				return iRowsAffected;
			}


			StringBuilder strBuilder = new StringBuilder();
			
			strBuilder.Append("update Cost_Allocation_Task set end_datetime = ");

			String strEndDate = Utility.DateFormat(strAppID,strEnterpriseID,dtEndDateTime,DTFormat.DateTime);
			strBuilder.Append(strEndDate);
			strBuilder.Append(",job_status = '");
			strBuilder.Append(strJobStatus);
			strBuilder.Append("',remark = N'");
			strBuilder.Append(Utility.ReplaceSingleQuote(strRemark));
			strBuilder.Append("' where applicationid = '");
			strBuilder.Append(strAppID);
			strBuilder.Append("' and enterpriseid = '");
			strBuilder.Append(strEnterpriseID);
			strBuilder.Append("' and jobid = ");
			strBuilder.Append(iJobID);

			String strSQLQuery = strBuilder.ToString();

			IDbCommand dbCmd = dbCon.CreateCommand(strSQLQuery,CommandType.Text);

			try
			{
				iRowsAffected = dbCon.ExecuteNonQuery(dbCmd);
				Logger.LogTraceInfo("MCAJobMgrDAL","ModifyMCAJob","SDM001",iRowsAffected+" rows inserted in to Cost_Code table");
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("MCAJobMgrDAL","ModifyMCAJob","SDM001","Error During update "+ appException.Message.ToString());
				throw new ApplicationException("Error updating MCAJob Status",appException);
			}
			return iRowsAffected;
			
		}

		public static int DeleteShipmentCostRecords(String strAppID, String strEnterpriseID,decimal iJobID)
		{
			int iRowsAffected = 0;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("MCAJobMgrDAL","DeleteShipmentCostRecords","EDB101","DbConnection object is null!!");
				return iRowsAffected;
			}

			StringBuilder strBuilder = new StringBuilder();
			strBuilder.Append("delete from shipment_cost where applicationid = '");
			strBuilder.Append(strAppID);
			strBuilder.Append("' and enterpriseid = '");
			strBuilder.Append(strEnterpriseID);
			strBuilder.Append("' and jobid = ");
			strBuilder.Append(iJobID);

			String strSQLQuery = strBuilder.ToString();

			IDbCommand dbCmd = dbCon.CreateCommand(strSQLQuery,CommandType.Text);

			try
			{
				iRowsAffected = dbCon.ExecuteNonQuery(dbCmd);
				Logger.LogTraceInfo("MCAJobMgrDAL","DeleteShipmentCostRecords","SDM001",iRowsAffected+" rows deleted from Shipment_Cost table");
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("MCAJobMgrDAL","DeleteShipmentCostRecords","SDM001","Error During delete "+ appException.Message.ToString());
				throw new ApplicationException("Error deleting shipment cost ",appException);
			}

			return iRowsAffected;
		}

		public static int GetShipmentRecordCount(String strAppID, String strEnterpriseID,  int iAccountYear, short iAccountMonth)
		{
			int iShipmentRecordCount = -1;

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("MonthlyCostAccountMgrDAL","GetCostCodeDS","EDB101","DbConnection object is null!!");
				return iShipmentRecordCount;
			}

			String strFromDate = "01/";
			if(iAccountMonth <10)
			{
				strFromDate += "0";
			}
				
			strFromDate += iAccountMonth+"/"+iAccountYear;
			DateTime dtFrom = DateTime.ParseExact (strFromDate, "dd/MM/yyyy", null);
			DateTime dtTo = dtFrom.AddMonths(1);
				
			String strDateFrom = Utility.DateFormat(strAppID,strEnterpriseID,dtFrom,DTFormat.DateTime);
			String strDateTo = Utility.DateFormat(strAppID,strEnterpriseID,dtTo,DTFormat.DateTime);

			String strCountQuery = "select count(*) as cnt  from Shipment ";
			StringBuilder strBuilderWhereClause = new StringBuilder();
			strBuilderWhereClause.Append("where applicationid = '");
			strBuilderWhereClause.Append(strAppID);
			strBuilderWhereClause.Append("' and enterpriseid = '");
			strBuilderWhereClause.Append(strEnterpriseID);
			strBuilderWhereClause.Append("' and booking_datetime >= ");
			strBuilderWhereClause.Append(strDateFrom);
			strBuilderWhereClause.Append("and booking_datetime < ");
			strBuilderWhereClause.Append(strDateTo);

			String strWhereClause = strBuilderWhereClause.ToString();
			String strSQLQuery = strCountQuery + strWhereClause;
			Object objShipmentRecordCount = dbCon.ExecuteScalar(strSQLQuery);

			if(Utility.IsNotDBNull(objShipmentRecordCount) && objShipmentRecordCount!=null)
			{
				iShipmentRecordCount  = System.Convert.ToInt32(objShipmentRecordCount);
			}

			return iShipmentRecordCount;			
		}


	}
}
