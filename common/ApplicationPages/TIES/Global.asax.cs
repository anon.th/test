using System;
using System.Reflection;
using System.Collections;
using System.Threading;
using System.ComponentModel;
using System.Web;
using System.Web.SessionState;
using com.common.RBAC;
using System.Web.Security;
using System.Diagnostics;
using System.IO;
using System.Globalization;
using System.Resources;
using com.common.util;
using com.common.classes;


namespace com.ties 
{
	/// <summary>
	/// Summary description for Global.
	/// </summary>
	/// 
	

	public class Global : System.Web.HttpApplication
	{
		public Global()
		{
			InitializeComponent();
		}	
		
		protected void Application_Start(Object sender, EventArgs e)
		{
			Logger.LogTraceInfo("Global","Application Start","INFO",DateTime.Now.ToString("dd MMM yyyy, HH:mm"));
			
			// This method is not required anymore. Listner is now configure through 'Web.config' file.
			//addEventLogListner();
			//String jsDir = System.Configuration.ConfigurationSettings.AppSettings["jsDir"];
			//String resourcePath = System.Configuration.ConfigurationSettings.AppSettings["resourcePath"];
			//System.Configuration.ConfigurationSettings.AppSettings["jsDir"] = Context.Server.MapPath(".") + Path.DirectorySeparatorChar + "Scripts";
			//String path = Context.Server.MapPath(".");
			initializeResourceManager();
			Hashtable appThreads  = new Hashtable();
			Application["APP_THREADS"] = appThreads;
		}
 
		protected void Session_Start(Object sender, EventArgs e)
		{

		}

		protected void Application_BeginRequest(Object sender, EventArgs e)
		{

		}

		protected void Application_EndRequest(Object sender, EventArgs e)
		{

		}

		protected void Application_AuthenticateRequest(Object sender, EventArgs e)
		{

			if (Request.IsAuthenticated == true)
			{
				String strCookieName = System.Configuration.ConfigurationSettings.AppSettings["appcookiename"];

				if (Request.Cookies[strCookieName] != null && Context.Request.Cookies["formcookie"].Value != "")
				{
					//Get roles from roles cookie
					
					FormsAuthenticationTicket formsAuthenticationTicket = FormsAuthentication.Decrypt(Context.Request.Cookies["formcookie"].Value);

					//convert the string representation of the role data into a string
					ArrayList userRoles = new ArrayList();
					
					foreach (String strRoletmp in formsAuthenticationTicket.UserData.Split(new char[]{';'}))
					{
						userRoles.Add(strRoletmp);
						
					}
					String[] strUserRole = null;
					strUserRole =(String[])userRoles.ToArray(typeof(String));
					Context.User = new System.Security.Principal.GenericPrincipal(Context.User.Identity, strUserRole);
				}
			}
		}

		protected void Application_Error(Object sender, EventArgs e)
		{
		}

		protected void Session_End(Object sender, EventArgs e)
		{

		}

		protected void Application_End(Object sender, EventArgs e)
		{
			stopAllAppThreads();
			Logger.LogTraceInfo("Global","Application End","INFO",DateTime.Now.ToString("dd MMM yyyy, HH:mm"));
		}

		//		private void addEventLogListner()
		//		{
		//			if (!EventLog.SourceExists("ties"))
		//			{
		//				EventLog.CreateEventSource("ties", "Application");
		//			}
		//
		//			EventLog appEventLog = new EventLog();
		//			appEventLog.Source = "ties";
		//				
		//			EventLogTraceListener eventLogListener = new EventLogTraceListener(appEventLog);
		//			System.Diagnostics.Trace.Listeners.Add(eventLogListener);
		//		}


		private void stopAllAppThreads()
		{
			Hashtable appThreads = (Hashtable)this.Context.Application["APP_THREADS"];


			//Stop all running threads.

			if(appThreads != null)
			{
				IDictionaryEnumerator threadEnumerator = appThreads.GetEnumerator();
				while(threadEnumerator.MoveNext())
				{
					if(threadEnumerator.Value != null)
					{
						Thread eachThread = (Thread)threadEnumerator.Value;
						if(eachThread.IsAlive)
						{
							eachThread.Abort();
						}
					}
				}
			}


		}

		private void initializeResourceManager()
		{
			//			string str = Server.MapPath("resources");
			//			Logger.LogTraceInfo("Global","InitializeResourceManager","INFO",str);

			//			Utility.MessagesResourceManager = ResourceManager.CreateFileBasedResourceManager("usermessages", System.Configuration.ConfigurationSettings.AppSettings["resourcePath"], null);
			//			Utility.ModulesResourceManager = ResourceManager.CreateFileBasedResourceManager("modulename", System.Configuration.ConfigurationSettings.AppSettings["resourcePath"], null);
			//			Utility.ScreenLabelsResourceManager = ResourceManager.CreateFileBasedResourceManager("ScreenLabel", System.Configuration.ConfigurationSettings.AppSettings["resourcePath"], null);

			//Assembly tiesAssembly = Assembly.Load("TIES");			
			Utility.ModulesResourceManager = new ResourceManager("TIES.resources.modulename",Assembly.GetExecutingAssembly());
			Utility.MessagesResourceManager = new ResourceManager("TIES.resources.usermessages",Assembly.GetExecutingAssembly());
			Utility.ScreenLabelsResourceManager = new ResourceManager("TIES.resources.ScreenLabel",Assembly.GetExecutingAssembly());
		}

			
		#region Web Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
		}
		#endregion
	}
}