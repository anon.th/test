using System;

namespace com.common.AppExtension
{
	/// <summary>
	/// Summary description for FreightCharge.
	/// </summary>
	public class FreightCharge
	{
		/// <summary>
		/// local variables
		/// </summary>
		private String  m_strApplicationID	= null;
		private String  m_strEnterpriseID	= null;		
		private String  m_strPayerID		= null;
		private String  m_strPayerName		= null;
		private String  m_strPayerType		= null;		
		private String  m_OriginZone		= null;
		private String  m_DestinationZone	= null;
		private Decimal m_FChargeableWt		= 0;		
		private String  m_strServiceCode	= null;
		private Decimal m_CommitTime		= 0;
		private Decimal m_TransitDay		= 0;
		private Decimal m_TransitHour		= 0;
		private String  m_strBandID			= null;
		private Decimal m_Discount			= 0;
		private String  m_strVASCode		= null;
		private Decimal m_Surcharge			= 0;
		private Decimal m_Lenght			= 0;
		private Decimal m_Height			= 0;
		private Decimal m_Breadth			= 0;
		private Decimal m_Weight			= 0;
		private Decimal m_Quantity			= 0;

		public FreightCharge()
		{			
		}
		#region properties
		/// <summary>
		/// Application id
		/// </summary>
		public String ApplicationID
		{
			set
			{
				m_strApplicationID = value;
			}
			get
			{
				return m_strApplicationID;
			}
			
		}
		/// <summary>
		/// Enterprise id
		/// </summary>
		public String EnterpriseID
		 {
			 set
			 {
				 m_strEnterpriseID = value;
			 }
			 get
			 {
				 return m_strEnterpriseID;
			 }
			
		 }
		/// <summary>
		/// Payer id
		/// </summary>
		public String PayerID
		  {
			  set
			  {
				  m_strPayerID = value;
			  }
			  get
			  {
				  return m_strPayerID;
			  }
			
		  }
		/// <summary>
		/// Payer name
		/// </summary>
		public String PayerName
		   {
			   set
			   {
				   m_strPayerName = value;
			   }
			   get
			   {
				   return m_strPayerName;
			   }
			
		   }
		/// <summary>
		/// Payer type
		/// </summary>
		public String PayerType
			{
				set
				{
					m_strPayerType = value;
				}
				get
				{
					return m_strPayerType;
				}
			
			}
		/// <summary>
		/// Origin Zone
		/// </summary>
		public String OriginZone
			 {
				 set
				 {
					 m_OriginZone = value;
				 }
				 get
				 {
					 return m_OriginZone;
				 }
			
			 }
		/// <summary>
		/// Destination Zone
		/// </summary>
		public String DestinationZone
			  {
				  set
				  {
					  m_DestinationZone = value;
				  }
				  get
				  {
					  return m_DestinationZone;
				  }
			
			  }
		/// <summary>
		/// FChargeable Wt
		/// </summary>
		public Decimal FChargeableWt
			   {
				   set
				   {
					   m_FChargeableWt = value;
				   }
				   get
				   {
					   return m_FChargeableWt;
				   }
			
			   }
		/// <summary>
		///Service Code
		/// </summary>
		public String ServiceCode
				{
					set
					{
						m_strServiceCode = value;
					}
					get
					{
						return m_strServiceCode;
					}
			
				}
		/// <summary>
		///CommitTime
		/// </summary>
		public Decimal CommitTime
				 {
					 set
					 {
						 m_CommitTime = value;
					 }
					 get
					 {
						 return m_CommitTime;
					 }
			
				 }
		/// <summary>
		/// TransitDay
		/// </summary>
		public Decimal TransitDay
				  {
					  set
					  {
						  m_TransitDay = value;
					  }
					  get
					  {
						  return m_TransitDay;
					  }
			
				  }
		/// <summary>
		/// TransitHour
		/// </summary>
		public Decimal TransitHour
				   {
					   set
					   {
						   m_TransitHour = value;
					   }
					   get
					   {
						   return m_TransitHour;
					   }
			
				   }

		/// <summary>
		/// BandID
		/// </summary>
		public String BandID
		{
			set
			{
				m_strBandID = value;
			}
			get
			{
				return m_strBandID;
			}
			
		}
		/// <summary>
		///Discount
		/// </summary>
		public Decimal Discount
		{
			set
			{
				m_Discount = value;
			}
			get
			{
				return m_Discount;
			}
			
		}
		/// <summary>
		/// VAS Code
		/// </summary>
		public String VASCode
		{
			set
			{
				m_strVASCode = value;
			}
			get
			{
				return m_strVASCode;
			}
			
		}
		/// <summary>
		/// Surcharge 
		/// </summary>
		public Decimal Surcharge 
		{
			set
			{
				m_Surcharge  = value;
			}
			get
			{
				return m_Surcharge ;
			}
			
		}
		/// <summary>
		/// Lenght
		/// </summary>
		public Decimal Lenght
		{
			set
			{
				m_Lenght = value;
			}
			get
			{
				return m_Lenght;
			}
			
		}
		/// <summary>
		///CommitTime
		/// </summary>
		public Decimal Height
		{
			set
			{
				m_Height = value;
			}
			get
			{
				return m_Height;
			}			
		}
		/// <summary>
		/// Breadth
		/// </summary>
		public Decimal Breadth
		{
			set
			{
				m_Breadth = value;
			}
			get
			{
				return m_Breadth;
			}			
		}
		/// <summary>
		/// Weight
		/// </summary>
		public Decimal Weight
		{
			set
			{
				m_Weight = value;
			}
			get
			{
				return m_Weight;
			}
			
		}
		/// <summary>
		/// Quantity
		/// </summary>
		public Decimal Quantity
		{
			set
			{
				m_Quantity = value;
			}
			get
			{
				return m_Quantity;
			}
			
		}		
		#endregion
	}
}
