using System;

namespace com.common.AppExtension
{
	/// <summary>
	/// Summary description for IFreightCharge.
	/// </summary>
	public interface IFreightCharge
	{
		Object CoreEnterprise();
		Object Logger(String strApplicationID,String strEnterpriseID);
		Object DatabaseConnection(String strApplicationID,String strEnterpriseID);
		Object User(String strApplicationID,String strEnterpriseID);
		Object SystemCode(String strApplicationID,String strEnterpriseID);		
	}
}
