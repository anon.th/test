using System;

namespace com.ties.classes
{
	public struct JobStatusInfo
	{
		public decimal iJobID;
		public DateTime dtStartDateTime;
		public DateTime dtEndDateTime;
		public String strJobStatus;
		public String strRemark;
		public String strJobStatusMessageID;
	}
}
