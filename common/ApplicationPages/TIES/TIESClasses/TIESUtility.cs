using System;
using System.Collections;
using System.Data;
using System.Text;
using com.common.DAL;
using com.common.classes;
using com.common.util;
using com.common.AppExtension;

namespace com.ties.classes
{
	/// <summary>
	/// TIESUtility contains the methods which is used commonly among all modules.
	/// </summary>
	/// 

	public struct QuotationData
	{
		public String strQuotationNo;
		public int iQuotationVersion;
	}

	public struct ExtendedData
	{
		public String strAssemblyid;
	}

	public struct ServiceData 
	{
		public String strVASCode;
		public bool isServiceAvail;
	}

	public struct VASSurcharge
	{
		public decimal decSurcharge;
		public String strVASCode;
		public String strVASDesc;
	}

	public struct StatusCodeDesc
	{
		public String strStatusCode;
		public String strStatusDesc;
		public String strAuto_Location;
		public String Allow_in_SWB;
	}

	public struct ExceptionCodeDesc
	{
		public String strExceptionCode;
		public String strExceptionDesc;
	}

	public struct TransitDayNServiceCode
	{
		public decimal iTransitDay;
		public String strServiceCode;
		public bool isLess;
	}

	public struct AgentConsgCost
	{
		public String strAgentID;
		public decimal decCost;
	}

	public struct AgentWtCost
	{
		public String strAgentID;
		public decimal decCost;
	}

	public class TIESUtility
	{
		public TIESUtility()
		{
			//
			// TODO: Add constructor logic here
			//
		}
		

		/// <summary>
		/// This method will perform the business logic to calculate the freight charge.
		/// </summary>
		/// <param name="a_strAppID"> Application ID</param>
		/// <param name="a_strEnterpriseID"> Enterprise ID</param>
		/// <param name="a_strPayerID"> Customer or Agent ID</param>
		/// <param name="a_strPayerType"> Payer type: 'A' for Agent and 'C' for Customer</param>
		/// <param name="a_strOrigZoneCode"> Origin Zone Code</param>
		/// <param name="a_strDestZoneCode"> Destination Zone Code</param>
		/// <param name="a_fChargeableWt"> Shipment Charageable Weight</param>
		/// <param name="a_strServiceCode"> Service Code</param>
		/// <returns>Return the computed freight charge as Float data type.</returns>
		public static float ComputeFreightCharge(string a_strAppID, string a_strEnterpriseID, 
			string a_strPayerID, string a_strPayerType, string a_strOrigZoneCode, 
			string a_strDestZoneCode, float a_fChargeableWt, string a_strServiceCode,
			string a_strOrgZipCode, string a_strDestZipCode)
		{
			// TODO: Write logic to compute freight charge here.

			String strQuotation			= null;
			int iQuotationVer			= 0;
			float fFreightCharge		= 0;
			String strInvokeMethod		= null;
			String strAssemblyid			= null;
			QuotationData structQtnData;
			ExtendedData structExtData;			
			// extended business logic for frieght charge begins
				try
				{
					structExtData	= getExtendedMethod(a_strAppID,a_strEnterpriseID,a_strPayerID,a_strPayerType);
					strAssemblyid	= structExtData.strAssemblyid;
					strInvokeMethod = "ComputeFreightCharge";
				}
				catch(ApplicationException appException)
				{
					Logger.LogTraceError("TIESUtility.cs","ComputeFreightCharge","ERR000","  "+appException.Message);
					throw new ApplicationException("ComputeFreightCharge Method (CustomerQtnActive)..",appException);
				}
			if(strAssemblyid != null)
			{
				FreightCharge objFC		=	new FreightCharge();
				objFC.ApplicationID		=	a_strAppID;
				objFC.EnterpriseID		=	a_strEnterpriseID;
				objFC.FChargeableWt		=	Convert.ToDecimal(a_fChargeableWt);
				objFC.DestinationZone	=	a_strDestZoneCode;
				objFC.OriginZone		=	a_strOrigZoneCode;
				objFC.ServiceCode		=	a_strServiceCode;
				AssemblyLoader objALoad	=	new AssemblyLoader();
				fFreightCharge=float.Parse(objALoad.LoadAssembly(a_strAppID,a_strEnterpriseID,strAssemblyid,strInvokeMethod,objFC).ToString());
				return fFreightCharge;							
			}
			// extended business logic for frieght charge ends	
			else
			{
				if(! a_strPayerType.Equals("A"))
				{				
					//The payer is a customer.
					try
					{
						structQtnData = CustomerQtnActive(a_strAppID,a_strEnterpriseID,a_strPayerID);
					}
					catch(ApplicationException appException)
					{
						Logger.LogTraceError("TIESUtility.cs","ComputeFreightCharge","ERR001","  "+appException.Message);
						throw new ApplicationException("ComputeFreightCharge Method (CustomerQtnActive)..",appException);
					}
					strQuotation = structQtnData.strQuotationNo;
					iQuotationVer = structQtnData.iQuotationVersion;
				}
				else if(a_strPayerType.Equals("A")) 
				{
					//The payer is an Agent.
					try
					{
						structQtnData = AgentQtnActive(a_strAppID,a_strEnterpriseID,a_strPayerID);
					}
					catch(ApplicationException appException)
					{
						Logger.LogTraceError("TIESUtility.cs","ComputeFreightCharge","ERR002","  "+appException.Message);
						throw new ApplicationException("ComputeFreightCharge Method.(AgentQtnActive).",appException);
					}
					strQuotation = structQtnData.strQuotationNo;
					iQuotationVer = structQtnData.iQuotationVersion;
				}

				//Quotaion exists
				if(strQuotation != null)
				{
					//check for the bandCode
					String strBandCode = null;
					try
					{
						strBandCode = GetBandCode(a_strAppID,a_strEnterpriseID,a_strPayerType,a_strPayerID,strQuotation,iQuotationVer);
					}
					catch(ApplicationException appException)
					{
						Logger.LogTraceError("TIESUtility.cs","ComputeFreightCharge","ERR003","  "+appException.Message);
						throw new ApplicationException("ComputeFreightCharge Method.(GetBandCode).",appException);
					}
				
					if(strBandCode != null)
					{
						//Calculate the Freight Charge
						fFreightCharge = ComputeFC(a_strAppID, a_strEnterpriseID,a_strOrigZoneCode,
							a_strDestZoneCode,a_fChargeableWt, a_strOrgZipCode, a_strDestZipCode,
							a_strPayerID, a_strServiceCode);
					
						decimal decSrvcDisc = 0;
						//Get the service discount & apply on the calculated Freight Charge.
						try
						{
							decSrvcDisc = GetQuotSrvCodeDiscount(a_strAppID,a_strEnterpriseID,a_strServiceCode,strQuotation,iQuotationVer,a_strPayerType,a_strPayerID);	
						}
						catch(ApplicationException appException)
						{
							Logger.LogTraceError("TIESUtility.cs","ComputeFreightCharge","ERR004","  "+appException.Message);
							throw new ApplicationException("ComputeFreightCharge Method.(GetServiceCodeDiscount).",appException);
						}
						if(decSrvcDisc != 0)
						{
							fFreightCharge = (fFreightCharge + (fFreightCharge*((float)decSrvcDisc/100)));
							//}

							decimal decBandDisc = 0;
							//Get banddiscount and  apply on the above calculated Freight Charge.
							try
							{
								decBandDisc = GetBandDiscount(a_strAppID,a_strEnterpriseID,strBandCode);
							}
							catch(ApplicationException appException)
							{
								Logger.LogTraceError("TIESUtility.cs","ComputeFreightCharge","ERR005","  "+appException.Message);
								throw new ApplicationException("ComputeFreightCharge Method.(GetBandDiscount).",appException);
							}

							if(decBandDisc > 0)
							{
								fFreightCharge = (fFreightCharge - (fFreightCharge*((float)decBandDisc/100)));
							}
						}
						else
						{
							decimal decServiceDisc = 0;
							//Get the service discount from base table & apply on the calculated Freight Charge.
							try
							{
								decServiceDisc = GetServiceCodeDiscount(a_strAppID,a_strEnterpriseID,a_strServiceCode);
							}
							catch(ApplicationException appException)
							{
								Logger.LogTraceError("TIESUtility.cs","ComputeFreightCharge","ERR008","  "+appException.Message);
								throw new ApplicationException("ComputeFreightCharge Method.(GetServiceCodeDiscount).",appException);
							}
							if(decServiceDisc != 0)
							{
								fFreightCharge = (fFreightCharge + (fFreightCharge*((float)decServiceDisc/100)));
							}
						}
					}
					else
					{
						//Get the Free Form Rates
						try
						{
							fFreightCharge = (float)GetQtnFFRates(a_strAppID,a_strEnterpriseID,a_strServiceCode,a_strPayerID,strQuotation,iQuotationVer,a_strOrigZoneCode,a_strDestZoneCode,a_strPayerType,a_fChargeableWt);
						}
						catch(ApplicationException appException)
						{
							Logger.LogTraceError("TIESUtility.cs","ComputeFreightCharge","ERR006","  "+appException.Message);
							throw new ApplicationException("ComputeFreightCharge Method.(GetQtnFFRates).",appException);
						}
					}

				}
				else //No active Quotation
				{
					//calculate Freight Charge
					try
					{
						fFreightCharge = ComputeFC(a_strAppID, a_strEnterpriseID,a_strOrigZoneCode,
							a_strDestZoneCode,a_fChargeableWt,a_strOrgZipCode, a_strDestZipCode,
							a_strPayerID, a_strServiceCode);
					}
					catch(ApplicationException appException)
					{
						Logger.LogTraceError("TIESUtility.cs","ComputeFreightCharge","ERR007","  "+appException.Message);
						throw new ApplicationException("ComputeFreightCharge Method.(ComputeFC).",appException);
					}


					decimal decServiceDisc = 0;
					//Get the service discount from base table & apply on the calculated Freight Charge.
					try
					{
						decServiceDisc = GetServiceCodeDiscount(a_strAppID,a_strEnterpriseID,a_strServiceCode);
					}
					catch(ApplicationException appException)
					{
						Logger.LogTraceError("TIESUtility.cs","ComputeFreightCharge","ERR008","  "+appException.Message);
						throw new ApplicationException("ComputeFreightCharge Method.(GetServiceCodeDiscount).",appException);
					}
					if(decServiceDisc != 0)
					{
						fFreightCharge = (fFreightCharge + (fFreightCharge*((float)decServiceDisc/100)));
					}
				}
			}
			return (float)Convert.ToDecimal(fFreightCharge.ToString("#.00"));    	
			
		}

		/// <summary>
		/// This method calculates the freight charge.
		/// </summary>
		/// <param name="a_strAppID"> Application ID</param>
		/// <param name="a_strEnterpriseID"> Enterprise ID</param>
		/// <param name="a_strOrigZoneCode"> Origin Zone Code</param>
		/// <param name="a_strDestZoneCode"> Destination Zone Code</param>
		/// <param name="a_fChargeableWt"> Shipment Charageable Weight</param>
		/// <returns>Return the computed freight charge as Float data type.</returns>
		private static float ComputeFC(string a_strAppID, string a_strEnterpriseID,string a_strOrigZoneCode, string a_strDestZoneCode,float a_fChargeableWt,
			String strOrgZipCode, String strDestZipCode,
			String strCustID, String serviceCode)
		{
			BaseZoneRates basezonerates = new BaseZoneRates();
			basezonerates.PopulateFR(a_strAppID, a_strEnterpriseID, a_strOrigZoneCode,a_strDestZoneCode,a_fChargeableWt,strOrgZipCode,strDestZipCode,strCustID,serviceCode);
			if(basezonerates.Rounded == true)
			{
				float fChargeableWt = 0;
				//Get the method to be used for round off from the enterprise table.
				Enterprise enterpriseRoundOff = new Enterprise();
				enterpriseRoundOff.Populate(a_strAppID,a_strEnterpriseID);
				decimal decRoundMethod = enterpriseRoundOff.RoundingMethod;
				decimal decIncrWeight = enterpriseRoundOff.IncrementWt;

				//Round the chargeable weight here & then find the range
				decimal decChargeableWeight = (decimal)a_fChargeableWt;
				if(decRoundMethod == 0)
				{
					fChargeableWt = (float)Math.Round(decChargeableWeight,2);
				}
				else if(decRoundMethod == 1)
				{
					//decChargeableWeight = Math.Round(decChargeableWeight,2);		
					fChargeableWt = (float)Math.Ceiling((double)decChargeableWeight);
				}
				else if(decRoundMethod == 2)
				{
					//decChargeableWeight = Math.Round(decChargeableWeight,2);
					fChargeableWt = (float)Math.Floor((double)decChargeableWeight);
				}

				basezonerates.PopulateFR(a_strAppID,a_strEnterpriseID,a_strOrigZoneCode,a_strDestZoneCode,a_fChargeableWt,strOrgZipCode,strDestZipCode,strCustID,serviceCode);
			}
			decimal decStartPrice = basezonerates.StartPrice;
			decimal decIncremPrice = basezonerates.IncrementPrice;
			decimal decStartWt = basezonerates.StartWeight;
			
			Enterprise enterprise = new Enterprise();
			enterprise.Populate(a_strAppID,a_strEnterpriseID);
			decimal decIncrmntWt = enterprise.IncrementWt;

			decimal fChrgWt = (decimal)a_fChargeableWt - decStartWt;
			float fFreightCharge = 0;
			//Formula for calculating Freight Charge
			if (fChrgWt < 0)
			{
				//fChrgWt = decStartWt;
				fFreightCharge=(float)decStartPrice;

			}
			else
			{
				try
				{
					fFreightCharge = (float)(((fChrgWt/decIncrmntWt)*decIncremPrice) + decStartPrice);
				}
				catch(ApplicationException appException)
				{
					Logger.LogTraceError("TIESUtility.cs","ComputeFC","ERR001","  "+appException.Message);
					throw new ApplicationException("ComputeFC",appException);
				}
			}
			return (float)Convert.ToDecimal(fFreightCharge.ToString("#0.00")); 
		}

		/// <summary>
		/// This method will determine whether a particular Service is available in the destination zipcode.
		/// </summary>
		/// <param name="a_strAppID">Application ID</param>
		/// <param name="a_strEnterpriseID">Enterprise ID</param>
		/// <param name="a_strServiceCode">Service Code</param>
		/// <param name="a_str_ZipCode">Zipcode</param>
		/// <returns>True is zip service available and False is not available.</returns>
		public static bool IsZipCodeServiceAvailable(string a_strAppID, string a_strEnterpriseID, string a_strServiceCode, string a_str_ZipCode)
		{
			DbConnection dbCon = null;
			DataSet dsZipService = null;
			IDbCommand dbCmd = null;
			bool isAvail = true;

			dbCon = DbConnectionManager.GetInstance().GetDbConnection(a_strAppID,a_strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("TIESUtility.cs","IsZipCodeServiceAvailable","ERR001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			
			StringBuilder strQry = new StringBuilder();
			strQry = strQry.Append("select * from zipcode_service_excluded  ");
			strQry.Append(" where applicationid = '");
			strQry.Append(a_strAppID);
			strQry.Append("' and enterpriseid = '");
			strQry.Append(a_strEnterpriseID);
			strQry.Append("' and service_code = '");
			strQry.Append(Utility.ReplaceSingleQuote(a_strServiceCode));
			strQry.Append("' and zipcode = '");
			strQry.Append(Utility.ReplaceSingleQuote(a_str_ZipCode));
			strQry.Append("'");
		
			dbCmd = dbCon.CreateCommand(strQry.ToString(),CommandType.Text);

			try
			{
				dsZipService =(DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
			}
			catch(ApplicationException appExpection)
			{
				Logger.LogTraceError("TIESUtility.cs","IsZipCodeServiceAvailable","ERR002","Error in  Execute Query "+appExpection.Message);
				throw appExpection;
			}

			if(dsZipService.Tables[0].Rows.Count >0)
			{
				isAvail = false;
			}
			
			return isAvail;
		}

		/// <summary>
		/// This method will get a list of outstanding Status Code for Pickup Request or Dispatch operation.
		/// </summary>
		/// <param name="a_strAppID"> Application ID</param>
		/// <param name="a_strEnterpriseID"> Enterprise ID</param>
		/// <returns>Return an array of string containing the individual status code.</returns>
		public static String[] getOutstandingDispatchStatus(string a_strAppID, string a_strEnterpriseID)
		{
			String[] StatusCodeList = {"DISP", "PIN"};

			return StatusCodeList;
		}
		/// <summary>
		/// This method will get the acutal status code to determine if the 
		/// actual pickup datetime be updated to Pickup_Request database table
		/// </summary>
		/// <param name="a_strAppID">Application ID</param>
		/// <param name="a_strEnterpriseID">Enterprise ID</param>
		/// <returns>Returns a String data type</returns>
		public static String getActualPickupStatusCode(string a_strAppID, string a_strEnterpriseID)
		{
			return "PUP";
		}
		/// <summary>
		/// This method will return the initial dispatch status code.
		/// </summary>
		/// <param name="a_strAppID"> Application ID</param>
		/// <param name="a_strEnterpriseID"> Enterprise ID</param>
		/// <returns>Return initial dispatch status code as string data type.</returns>
		public static string getInitialDispatchStatus(string a_strAppID, string a_strEnterpriseID)
		{
			return "DISP";
		}

		/// <summary>
		/// This method will return the initial dispatch status code.
		/// </summary>
		/// <param name="a_strAppID"> Application ID</param>
		/// <param name="a_strEnterpriseID"> Enterprise ID</param>
		/// <returns>Return initial dispatch status code as string data type.</returns>
		public static string getInitialShipmentStatusCode(string a_strAppID, string a_strEnterpriseID)
		{
			return "MDE";
		}

		
		/// <summary>
		/// This method will get a list of Status Code for deleting domestic shipment.
		/// </summary>
		/// <param name="a_strAppID"> Application ID</param>
		/// <param name="a_strEnterpriseID"> Enterprise ID</param>
		/// <returns>Return an array of string containing the status code which can be deleted.</returns>
		public static String[] getDeleteAllowStatus(string a_strAppID, string a_strEnterpriseID)
		{
			String[] StatusCodeDeleteList = {"MDE"};

			return StatusCodeDeleteList;
		}

		/// <summary>
		/// This method will return the initial Shipment Delivery Manifest.
		/// </summary>
		/// <param name="a_strAppID"> Application ID</param>
		/// <param name="a_strEnterpriseID"> Enterprise ID</param>
		/// <returns>Return initial Shipment Delivery Manifest as string data type.</returns>
		public static string getInitialDlvrymanifest(string a_strAppID, string a_strEnterpriseID)
		{
			return "No";
		}
		/// <summary>
		/// This method will perform the business logic to calculate the Dimension Weight.
		/// </summary>
		/// <param name="a_strAppID"> Application ID</param>
		/// <param name="a_strEnterpriseID"> Enterprise ID</param>
		/// <param name="a_fVolume"> Package Volume</param>
		/// <returns>Return the computed Dimensional Weight as Float data type.</returns>
		public static float ComputeDimWt(string a_strAppID, string a_strEnterpriseID,float a_fVolume)
		{
			float fDimWt = 0;	
			
			Enterprise enterprise = new Enterprise();
			enterprise.Populate(a_strAppID,a_strEnterpriseID);
			float fDensityFactor = (float)enterprise.DensityFactor;
			try
			{
				fDimWt = (a_fVolume/fDensityFactor);
			}
			catch(System.ApplicationException appException)
			{
				Logger.LogTraceError("TIESUtility","ComputeDimWt","ERR001","Error while calculating the dimwt ");
				throw appException;
			}

			return (float)Convert.ToDecimal(fDimWt.ToString("#0.00")); 
		}	

		/// <summary>
		/// This method will return the quotation no for a customer if it exists.
		/// </summary>
		/// <param name="a_strAppID"> Application ID</param>
		/// <param name="a_strEnterpriseID"> Enterprise ID</param>
		/// <param name="a_strCustID"> Customer ID</param>
		/// <returns>Return the Quotation number as String data type or null if no quotations exists.</returns>
		public static QuotationData CustomerQtnActive(String a_strAppID,String a_strEnterpriseID,String a_strCustID )
		{
			DbConnection dbConApp = null;
			IDbCommand dbCmd = null;
			DataSet dsCustomer = null;
			String  strQuotationNo = null;
			int iQuotationVer = 0;
			QuotationData structQuotation = new QuotationData();
		
			dbConApp = DbConnectionManager.GetInstance().GetDbConnection(a_strAppID,a_strEnterpriseID);

			if(dbConApp == null)
			{
				Logger.LogTraceError("TIESUtility","CustomerQtnActive","ERR001","dbConApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			StringBuilder strQry = new StringBuilder();
			strQry.Append("select * from customer_quotation where applicationid='");
			strQry.Append(a_strAppID);
			strQry.Append("' and enterpriseid = '");
			strQry.Append(a_strEnterpriseID);
			strQry.Append("' and custid = '");
			strQry.Append(Utility.ReplaceSingleQuote(a_strCustID)+"'");
			strQry.Append(" and quotation_status = 'Q'");

			dbCmd = dbConApp.CreateCommand(strQry.ToString(),CommandType.Text);
			try
			{
				dsCustomer =(DataSet)dbConApp.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
			}
			catch(ApplicationException appExpection)
			{
				Logger.LogTraceError("TIESUtility","CustomerQtnActive","ERR002","Error in the query String "+appExpection.Message);
				throw appExpection;
			}
			if(dsCustomer.Tables[0].Rows.Count > 0)
			{
				DataRow drEach = dsCustomer.Tables[0].Rows[0];
				if(!drEach["quotation_no"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					strQuotationNo = (String)drEach["quotation_no"];					
				}
				if(!drEach["quotation_version"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					iQuotationVer = Convert.ToInt32(drEach["quotation_version"]);					
				}
			}
			
			structQuotation.strQuotationNo = strQuotationNo;
			structQuotation.iQuotationVersion = iQuotationVer;

			return structQuotation;

		}
		public static ExtendedData getExtendedMethod(String a_strAppID,String a_strEnterpriseID,String a_strCustID,String a_strPayerType)
		{
			DbConnection dbConApp		= null;
			IDbCommand dbCmd			= null;
			DataSet dsCustomer			= null;
			String  strAssemblyid		= null;
			ExtendedData structextData	= new ExtendedData();
		
			dbConApp = DbConnectionManager.GetInstance().GetDbConnection(a_strAppID,a_strEnterpriseID);

			if(dbConApp == null)
			{
				Logger.LogTraceError("TIESUtility","CustomerQtnActive","ERR001","dbConApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			StringBuilder strQry = new StringBuilder();
			if(a_strPayerType.Equals("C"))
			{
				strQry.Append("	select * from customer_assembly where applicationid='"+ a_strAppID +"' and enterpriseid = '"+a_strEnterpriseID+"' and custid = '"+Utility.ReplaceSingleQuote(a_strCustID)+"' and customized_assembly_code = '1'");

//				strQry.Append(" select * from assembly a,customer_assembly ca where a.applicationid='"+ a_strAppID +"' and a.enterpriseid = '"+enterpriseid+"'  and ca.custid='"+Utility.ReplaceSingleQuote(a_strCustID)+"' and ");
//				strQry.Append(" a.applicationid = ca.applicationid and a.enterpriseid = ca.enterpriseid and a.assemblyid=ca.assemblyid and ca.customized_assembly_code='"+1+"' ");
			}
			else
			{
				strQry.Append("	select * from agent_assembly where applicationid='"+ a_strAppID +"' and enterpriseid = '"+a_strEnterpriseID+"' and agentid = '"+Utility.ReplaceSingleQuote(a_strCustID)+"' and customized_assembly_code = '2'");
			}
			dbCmd = dbConApp.CreateCommand(strQry.ToString(),CommandType.Text);
			try
			{
				dsCustomer =(DataSet)dbConApp.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
			}
			catch(ApplicationException appExpection)
			{
				Logger.LogTraceError("TIESUtility","CustomerQtnActive","ERR002","Error in the query String "+appExpection.Message);
				throw appExpection;
			}
			if(dsCustomer.Tables[0].Rows.Count > 0)
			{
				DataRow drEach = dsCustomer.Tables[0].Rows[0];
				
				if(!drEach["assemblyid"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					strAssemblyid = (String)drEach["assemblyid"];					
				}				
			}
			structextData.strAssemblyid	= strAssemblyid;
			return structextData;
		}	
		/// <summary>
		/// This method will return the quotation no for a Agent if it exists.
		/// </summary>
		/// <param name="a_strAppID"> Application ID</param>
		/// <param name="a_strEnterpriseID"> Enterprise ID</param>
		/// <param name="a_strAgentID"> Agent ID</param>
		/// <returns>Return the Quotation number as String data type or null if no quotations exists.</returns>
		public static QuotationData AgentQtnActive(String a_strAppID,String a_strEnterpriseID,String a_strAgentID)
		{
			String strAgentQtnNo = null;
			int iQuotationVer = 0;
			DbConnection dbConApp = null;
			IDbCommand dbCmd = null;
			DataSet dsCustomer = null;
			QuotationData structQuotation = new QuotationData();
			
			dbConApp = DbConnectionManager.GetInstance().GetDbConnection(a_strAppID,a_strEnterpriseID);

			if(dbConApp == null)
			{
				Logger.LogTraceError("TIESUtility","AgentQtnActive","ERR001","dbConApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			StringBuilder strQry = new StringBuilder();
			strQry.Append("select * from agent_quotation where applicationid='");
			strQry.Append(a_strAppID);
			strQry.Append("' and enterpriseid = '");
			strQry.Append(a_strEnterpriseID);
			strQry.Append("' and agentid = '");
			strQry.Append(Utility.ReplaceSingleQuote(a_strAgentID)+"'");
			strQry.Append(" and quotation_status = 'Q'");

			dbCmd = dbConApp.CreateCommand(strQry.ToString(),CommandType.Text);
			try
			{
				dsCustomer =(DataSet)dbConApp.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
			}
			catch(ApplicationException appExpection)
			{
				Logger.LogTraceError("TIESUtility","AgentQtnActive","ERR002","Error in the query String");
				throw appExpection;
			}
			if(dsCustomer.Tables[0].Rows.Count > 0)
			{
				DataRow drEach = dsCustomer.Tables[0].Rows[0];
				if(!drEach["quotation_no"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					strAgentQtnNo = (String)drEach["quotation_no"];					
				}
				if(!drEach["quotation_version"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					iQuotationVer = Convert.ToInt32(drEach["quotation_version"]);					
				}
			}

			structQuotation.strQuotationNo = strAgentQtnNo;
			structQuotation.iQuotationVersion = iQuotationVer;

			return structQuotation;
		}

		/// <summary>
		/// This method will return the Band Code for a Agent/Customer if it exists.
		/// </summary>
		/// <param name="a_strAppID"> Application ID</param>
		/// <param name="a_strEnterpriseID"> Enterprise ID</param>
		/// /// <param name="a_strPayerType"> Payer type: 'A' for Agent and 'C' for Customer</param>
		/// <param name="a_strPayerID"> Customer or Agent ID</param>
		/// <param name="a_strQuotationNo"> Quotation Number</param>
		/// <param name="a_iQuotationVer"> Quotation Version</param>
		/// <returns>Return the Band Code as String data type or null if no quotations exists.</returns>
		public static String GetBandCode(String a_strAppID,String a_strEnterpriseID,String a_strPayerType,String a_strPayerID,String a_strQuotationNo,int a_iQuotationVer)
		{
			String strBandCode = null;
			DbConnection dbConApp = null;
			IDbCommand dbCmd = null;
			DataSet dsQuotation = null;

			dbConApp = DbConnectionManager.GetInstance().GetDbConnection(a_strAppID,a_strEnterpriseID);

			if(dbConApp == null)
			{
				Logger.LogTraceError("TIESUtility","GetBandCode","ERR001","dbConApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			StringBuilder strQry = new StringBuilder();
			
			if(a_strPayerType.Equals("C"))
			{
				strQry.Append("select * from customer_quotation where applicationid='");
				strQry.Append(a_strAppID);
				strQry.Append("' and enterpriseid = '");
				strQry.Append(a_strEnterpriseID);
				strQry.Append("' and custid = '");
				strQry.Append(Utility.ReplaceSingleQuote(a_strPayerID));
				strQry.Append("' and quotation_no = '");
				strQry.Append(a_strQuotationNo);
				strQry.Append("' and quotation_version = ");
				strQry.Append(a_iQuotationVer);
			}
			else if(a_strPayerType.Equals("A"))
			{
				strQry.Append("select * from agent_quotation where applicationid='");
				strQry.Append(a_strAppID);
				strQry.Append("' and enterpriseid = '");
				strQry.Append(a_strEnterpriseID);
				strQry.Append("' and agentid = '");
				strQry.Append(Utility.ReplaceSingleQuote(a_strPayerID));
				strQry.Append("' and quotation_no = '");
				strQry.Append(a_strQuotationNo);
				strQry.Append("' and quotation_version = ");
				strQry.Append(a_iQuotationVer);
			}

			dbCmd = dbConApp.CreateCommand(strQry.ToString(),CommandType.Text);
			try
			{
				dsQuotation =(DataSet)dbConApp.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
			}
			catch(ApplicationException appExpection)
			{
				Logger.LogTraceError("TIESUtility","GetBandCode","ERR002","Error in the query String");
				throw appExpection;
			}
			if(dsQuotation.Tables[0].Rows.Count > 0)
			{
				DataRow drEach = dsQuotation.Tables[0].Rows[0];
				if(!drEach["band_code"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					strBandCode = (String)drEach["band_code"];					
				}
			}
			return strBandCode;
		}

		/// <summary>
		/// This method will return the Band Code for a Agent/Customer if it exists.
		/// </summary>
		/// <param name="a_strAppID"> Application ID</param>
		/// <param name="a_strEnterpriseID"> Enterprise ID</param>
		/// <param name="a_strBandCode"> Band Code</param>
		/// <returns>Return the Band Discount as decimal data type </returns>
		public static decimal GetBandDiscount(String a_strAppID,String a_strEnterpriseID,String a_strBandCode)
		{
			decimal decBandDiscount = 0;
			DbConnection dbConApp = null;
			IDbCommand dbCmd = null;
			DataSet dsQuotation = null;

			dbConApp = DbConnectionManager.GetInstance().GetDbConnection(a_strAppID,a_strEnterpriseID);

			if(dbConApp == null)
			{
				Logger.LogTraceError("TIESUtility","GetBandDiscount","ERR001","dbConApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			StringBuilder strQry = new StringBuilder();
			
			strQry.Append("select * from band_discount where applicationid='");
			strQry.Append(a_strAppID);
			strQry.Append("' and enterpriseid = '");
			strQry.Append(a_strEnterpriseID);
			strQry.Append("' and band_code = '");
			strQry.Append(a_strBandCode);
			strQry.Append("'");
		
			dbCmd = dbConApp.CreateCommand(strQry.ToString(),CommandType.Text);
			try
			{
				dsQuotation =(DataSet)dbConApp.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
			}
			catch(ApplicationException appExpection)
			{
				Logger.LogTraceError("TIESUtility","GetBandCode","ERR002","Error in the query String");
				throw appExpection;
			}
			if(dsQuotation.Tables[0].Rows.Count > 0)
			{
				DataRow drEach = dsQuotation.Tables[0].Rows[0];
				if(!drEach["percent_discount"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					decBandDiscount = (decimal)drEach["percent_discount"];					
				}
			}
			return decBandDiscount;
		}
		
		/// <summary>
		/// This method will return the ServiceCode discount.
		/// </summary>
		/// <param name="a_strAppID"> Application ID</param>
		/// <param name="a_strEnterpriseID"> Enterprise ID</param>
		/// <param name="a_strServiceCode"> Service Code</param>
		/// <returns>Return the Service Discount as decimal data type </returns>
		public static decimal GetServiceCodeDiscount(String a_strAppID,String a_strEnterpriseID,String a_strServiceCode)
		{
			decimal decSrvCodeDiscount = 0;
			DbConnection dbConApp = null;
			IDbCommand dbCmd = null;
			DataSet dsSrvcCode = null;

			dbConApp = DbConnectionManager.GetInstance().GetDbConnection(a_strAppID,a_strEnterpriseID);

			if(dbConApp == null)
			{
				Logger.LogTraceError("TIESUtility","GetServiceCodeDiscount","ERR001","dbConApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			StringBuilder strQry = new StringBuilder();
			
			strQry.Append("select * from service where applicationid='");
			strQry.Append(a_strAppID);
			strQry.Append("' and enterpriseid = '");
			strQry.Append(a_strEnterpriseID);
			strQry.Append("' and service_code = '");
			strQry.Append(a_strServiceCode);
			strQry.Append("'");
		
			dbCmd = dbConApp.CreateCommand(strQry.ToString(),CommandType.Text);
			try
			{
				dsSrvcCode =(DataSet)dbConApp.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
			}
			catch(ApplicationException appExpection)
			{
				Logger.LogTraceError("TIESUtility","GetServiceCodeDiscount","ERR002","Error in the query String");
				throw appExpection;
			}
			if(dsSrvcCode.Tables[0].Rows.Count > 0)
			{
				DataRow drEach = dsSrvcCode.Tables[0].Rows[0];
				if(!drEach["service_charge_percent"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					decSrvCodeDiscount = (decimal)drEach["service_charge_percent"];					
				}
			}
			return decSrvCodeDiscount;
		}

		/// <summary>
		/// This method will return the ServiceCode discount for the quotation.
		/// </summary>
		/// <param name="a_strAppID"> Application ID</param>
		/// <param name="a_strEnterpriseID"> Enterprise ID</param>
		/// <param name="a_strServiceCode"> Service Code</param>
		/// <returns>Return the Service Discount as decimal data type </returns>
		public static decimal GetQuotSrvCodeDiscount(String a_strAppID,String a_strEnterpriseID,String a_strServiceCode,String strQuotation,int iQuotVer, String strPayerType,String strPayerID)
		{
			decimal decSrvCodeDiscount = 0;
			DbConnection dbConApp = null;
			IDbCommand dbCmd = null;
			DataSet dsSrvcCode = null;

			dbConApp = DbConnectionManager.GetInstance().GetDbConnection(a_strAppID,a_strEnterpriseID);

			if(dbConApp == null)
			{
				Logger.LogTraceError("TIESUtility","GetQuotSrvCodeDiscount","ERR001","dbConApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			StringBuilder strQry = new StringBuilder();
			
			strQry.Append("select * from ");
			if(strPayerType.Equals("C"))
			{
				strQry.Append("  customer_quotation_service ");
			}
			else if(strPayerType.Equals("A"))
			{
				strQry.Append("  Agent_quotation_service ");
			}

			strQry.Append(" where applicationid = '");
			strQry.Append(a_strAppID);
			strQry.Append("' and enterpriseid = '");
			strQry.Append(a_strEnterpriseID);
			strQry.Append("' and service_code = '");
			strQry.Append(a_strServiceCode);
			strQry.Append("'");

			if(strPayerType.Equals("C"))
			{
				strQry.Append(" and custid = '");
				strQry.Append(strPayerID);
				strQry.Append("'");
			}
			else if(strPayerType.Equals("A"))
			{
				strQry.Append(" and agentid = '");
				strQry.Append(Utility.ReplaceSingleQuote(strPayerID));
				strQry.Append("'");
			}
			strQry.Append(" and quotation_no = '");
			strQry.Append(strQuotation);
			strQry.Append("'");
			strQry.Append(" and quotation_version = ");
			strQry.Append(iQuotVer);
		
			dbCmd = dbConApp.CreateCommand(strQry.ToString(),CommandType.Text);
			try
			{
				dsSrvcCode =(DataSet)dbConApp.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
			}
			catch(ApplicationException appExpection)
			{
				Logger.LogTraceError("TIESUtility","GetQuotSrvCodeDiscount","ERR002","Error in the query String");
				throw appExpection;
			}
			if(dsSrvcCode.Tables[0].Rows.Count > 0)
			{
				DataRow drEach = dsSrvcCode.Tables[0].Rows[0];
				if(!drEach["service_charge_percent"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					decSrvCodeDiscount = (decimal)drEach["service_charge_percent"];					
				}
			}
			return decSrvCodeDiscount;
		}

		/// <summary>
		/// This method will return the ServiceCode discount.
		/// </summary>
		/// <param name="a_strAppID"> Application ID</param>
		/// <param name="a_strEnterpriseID"> Enterprise ID</param>
		/// <param name="a_strServiceCode"> Service Code</param>
		/// <param name="a_strCustID"> Customer ID</param>
		/// <param name="a_strQuotationNo"> Quotation No</param>
		/// <param name="a_iQuotationVer"> Quotation Version</param>
		/// <param name="a_strOrgZone"> Origin Zone</param>
		/// <param name="a_strDstnZone"> Destination zone</param>
		/// <param name="a_strPayerType"> Payer type: 'A' for Agent and 'C' for Customer</param>
		/// <param name="a_fChargeableWt">Chargeable Weight</param>
		/// <returns>Return the Freight charge as decimal data type </returns>
		public static decimal GetQtnFFRates(String a_strAppID,String a_strEnterpriseID,String a_strServiceCode,String a_strCustID,String a_strQuotationNo,int a_iQuotationVer,String a_strOrgZone,String a_strDstnZone,String a_strPayerType,float a_fChargeableWt)
		{
			decimal decWt_Charge = 0;
			DbConnection dbConApp = null;
			IDbCommand dbCmd = null;
			DataSet dsFFRates = null;

			dbConApp = DbConnectionManager.GetInstance().GetDbConnection(a_strAppID,a_strEnterpriseID);

			if(dbConApp == null)
			{
				Logger.LogTraceError("TIESUtility","GetQtnFFRates","ERR001","dbConApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			StringBuilder strQry = new StringBuilder();
			
			if(a_strPayerType.Equals("C"))
			{
				strQry.Append("select * from customer_quotation_ff_Rates where applicationid='");
				strQry.Append(a_strAppID);
				strQry.Append("' and enterpriseid = '");
				strQry.Append(a_strEnterpriseID);
				strQry.Append("' and custid = '");
				strQry.Append(Utility.ReplaceSingleQuote(a_strCustID));
				strQry.Append("'");
				strQry.Append(" and quotation_no = '");
				strQry.Append(a_strQuotationNo);
				strQry.Append("' and quotation_version = ");
				strQry.Append(a_iQuotationVer);
				strQry.Append(" and origin_zone_code = '");
				strQry.Append(a_strOrgZone);
				strQry.Append("' and destination_zone_code = '");
				strQry.Append(a_strDstnZone);
				strQry.Append("' and service_code = '");
				strQry.Append(a_strServiceCode);
				strQry.Append("' and wt = ");
				strQry.Append(a_fChargeableWt);
			}
			else if(a_strPayerType.Equals("A"))
			{
				strQry.Append("select * from agent_quotation_ff_Rates where applicationid='");
				strQry.Append(a_strAppID);
				strQry.Append("' and enterpriseid = '");
				strQry.Append(a_strEnterpriseID);
				strQry.Append("' and agentid = '");
				strQry.Append(Utility.ReplaceSingleQuote(a_strCustID));
				strQry.Append("'");
				strQry.Append(" and quotation_no = '");
				strQry.Append(a_strQuotationNo);
				strQry.Append("' and quotation_version = ");
				strQry.Append(a_iQuotationVer);
				strQry.Append(" and origin_zone_code = '");
				strQry.Append(a_strOrgZone);
				strQry.Append("' and destination_zone_code = '");
				strQry.Append(a_strDstnZone);
				strQry.Append("' and service_code = '");
				strQry.Append(a_strServiceCode);
				strQry.Append("' and wt = ");
				strQry.Append(a_fChargeableWt);
			}

			dbCmd = dbConApp.CreateCommand(strQry.ToString(),CommandType.Text);
			try
			{
				dsFFRates =(DataSet)dbConApp.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
			}
			catch(ApplicationException appExpection)
			{
				Logger.LogTraceError("TIESUtility","GetQtnFFRates","ERR002","Error in the query String");
				throw appExpection;
			}
			if(dsFFRates.Tables[0].Rows.Count > 0)
			{
				DataRow drEach = dsFFRates.Tables[0].Rows[0];
				if(!drEach["wt_charge"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					decWt_Charge = (decimal)drEach["wt_charge"];					
				}
			}
			return decWt_Charge;
		}
		
		/// <summary>
		/// This method will return whether the path is in a Route_Code Table 
		/// </summary>
		/// <param name="a_strAppID"> Application ID</param>
		/// <param name="a_strEnterpriseID"> Enterprise ID</param>
		/// <param name="a_path"> Selected Path </param>
		/// <returns>Return true if it is in a Route_Code Table </returns>
		
		//By Aoo.
		public static string GetDeliveryType(string strAppID,string EntID,string strPath)
		{
			DbConnection conn = null;
			IDbCommand cmd = null;
			DataSet dsDT = null;
			string getDT = "";

			conn = DbConnectionManager.GetInstance().GetDbConnection(strAppID,EntID);

			if(conn == null)
			{
				Logger.LogTraceError("TIESUtility","GetDeliveryType","ERR001","dbConApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			StringBuilder strQry = new StringBuilder();
			strQry.Append("SELECT * FROM Delivery_Path WHERE path_code = '"); 
			strQry.Append(strPath);
			strQry.Append("' AND applicationid = '");
			strQry.Append(strAppID);
			strQry.Append("' AND enterpriseid = '");
			strQry.Append(EntID);
			strQry.Append("'");

			cmd = conn.CreateCommand(strQry.ToString(),CommandType.Text);
			try
			{
				dsDT =(DataSet)conn.ExecuteQuery(cmd,com.common.DAL.ReturnType.DataSetType);
			}
			catch(ApplicationException appExpection)
			{
				Logger.LogTraceError("TIESUtility","GetDeliveryType","ERR002","Error in the query String");
				throw appExpection;
			}
			if(dsDT.Tables[0].Rows.Count > 0)
			{
				DataRow drDT = dsDT.Tables[0].Rows[0];
				if (drDT["delivery_type"] != null && drDT["delivery_type"].ToString() != "")
				{
					getDT = (string)drDT["delivery_type"];
				}
			}
			return getDT;
		}

		public static bool IsInRouteCode(String a_strAppID,String a_strEnterpriseID,String a_path)
		{
			DbConnection dbConApp = null;
			IDbCommand dbCmd = null;
			DataSet dsRouteCode = null;
			bool isRouteCode = false;

			dbConApp = DbConnectionManager.GetInstance().GetDbConnection(a_strAppID,a_strEnterpriseID);

			if(dbConApp == null)
			{
				Logger.LogTraceError("TIESUtility","IsInRoutePath","ERR001","dbConApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			StringBuilder strQry = new StringBuilder();
			strQry.Append("Select * From Route_Code Where applicationid = '"); 
			strQry.Append(a_strAppID);
			strQry.Append("' and enterpriseid = '");
			strQry.Append(a_strEnterpriseID);
			strQry.Append("' and path_code = '");
			strQry.Append(a_path);
			strQry.Append("'");

			dbCmd = dbConApp.CreateCommand(strQry.ToString(),CommandType.Text);
			try
			{
				dsRouteCode =(DataSet)dbConApp.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
			}
			catch(ApplicationException appExpection)
			{
				Logger.LogTraceError("TIESUtility","IsInRouteCode","ERR002","Error in the query String");
				throw appExpection;
			}
			if(dsRouteCode.Tables[0].Rows.Count > 0)
			{
				isRouteCode = true;
			}
			return isRouteCode;
		}
		
		/*Comment by Sompote 2010-04-05
		/// <summary>
		/// This method will return whether the day is a holiday for the enterprise
		/// </summary>
		/// <param name="a_strAppID"> Application ID</param>
		/// <param name="a_strEnterpriseID"> Enterprise ID</param>
		/// <param name="a_stEstmDlvryDate"> Estimated Delivery Date</param>
		/// <returns>Return true if it is a holiday or else false as bool data type </returns>
		public static bool IsDayHoliday(String a_strAppID,String a_strEnterpriseID,DateTime a_stEstmDlvryDate)
		{
			DbConnection dbConApp = null;
			IDbCommand dbCmd = null;
			DataSet dsHoliday = null;
			bool isHoliday = false;

			dbConApp = DbConnectionManager.GetInstance().GetDbConnection(a_strAppID,a_strEnterpriseID);

			if(dbConApp == null)
			{
				Logger.LogTraceError("TIESUtility","IsDayHoliday","ERR001","dbConApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			StringBuilder strQry = new StringBuilder();
			strQry.Append("select * from enterprise_holiday where applicationid = '");
			strQry.Append(a_strAppID);
			strQry.Append("' and enterpriseid = '");
			strQry.Append(a_strEnterpriseID);
			strQry.Append("' and datefrom <= ");
			strQry.Append(Utility.DateFormat(a_strAppID,a_strEnterpriseID,a_stEstmDlvryDate,DTFormat.Date));
			strQry.Append(" and dateto >= ");
			strQry.Append(Utility.DateFormat(a_strAppID,a_strEnterpriseID,a_stEstmDlvryDate,DTFormat.Date));
			//String strDate = Utility.DateFormat(a_strAppID,a_strEnterpriseID,a_stEstmDlvryDate,DTFormat.Date);
			//strQry.Append(strDate.Substring(0,strDate.Length-1));
			///strQry.Append(" "+"23:59'");
			
			
			dbCmd = dbConApp.CreateCommand(strQry.ToString(),CommandType.Text);
			try
			{
				dsHoliday =(DataSet)dbConApp.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
			}
			catch(ApplicationException appExpection)
			{
				Logger.LogTraceError("TIESUtility","IsDayHoliday","ERR002","Error in the query String");
				throw appExpection;
			}
			if(dsHoliday.Tables[0].Rows.Count > 0)
			{
				isHoliday = true;
			}
			return isHoliday;
		}
		
		*/

		public static bool IsDayHoliday(String a_strAppID,String a_strEnterpriseID,DateTime a_stEstmDlvryDate)
		{
			DbConnection dbConApp = null;
			dbConApp = DbConnectionManager.GetInstance().GetDbConnection(a_strAppID,a_strEnterpriseID);

			if(dbConApp == null)
			{
				Logger.LogTraceError("TIESUtility","IsDayHoliday","ERR001","dbConApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			return IsDayHoliday( a_strAppID, a_strEnterpriseID, a_stEstmDlvryDate, dbConApp);
		}
		public static bool IsDayHoliday(String a_strAppID,String a_strEnterpriseID,DateTime a_stEstmDlvryDate,DbConnection dbCon)
		{
			IDbConnection conApp = dbCon.GetConnection();
			if(conApp == null)
			{
				Logger.LogTraceError("TIESUtility","IsDayHoliday","ERR003","conApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			try
			{
				dbCon.OpenConnection(ref conApp);
			}
			catch(ApplicationException appException)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("TIESUtility","IsDayHoliday","ERR004","Error opening connection : "+ appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ",appException);
			}
			catch(Exception exception)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("TIESUtility","IsDayHoliday","ERR005","Error opening connection : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}
			
			IDbCommand dbCmd = null;		
			dbCmd = dbCon.CreateCommand("",CommandType.Text);
			dbCmd.Connection = conApp;	

			//return IsDayHoliday( a_strAppID, a_strEnterpriseID, a_stEstmDlvryDate, dbCon, dbCmd);
		
			bool result = false ;	
			try
			{
				result = IsDayHoliday( a_strAppID, a_strEnterpriseID, a_stEstmDlvryDate, dbCon, dbCmd);
			}
			catch(ApplicationException appExpection)
			{
				throw appExpection;
			}
			finally
			{
				if(conApp != null)
				{
					conApp.Close();
				}
			}
			return result;

		}
		public static bool IsDayHoliday(String a_strAppID,String a_strEnterpriseID,DateTime a_stEstmDlvryDate,DbConnection dbCon,IDbCommand dbCmd)
		{
			//DbConnection dbConApp = null;
			//IDbCommand dbCmd = null;
			DataSet dsHoliday = null;
			bool isHoliday = false;

			/*dbConApp = DbConnectionManager.GetInstance().GetDbConnection(a_strAppID,a_strEnterpriseID);

			if(dbConApp == null)
			{
				Logger.LogTraceError("TIESUtility","IsDayHoliday","ERR001","dbConApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}*/
			StringBuilder strQry = new StringBuilder();
			strQry.Append("select * from enterprise_holiday where applicationid = '");
			strQry.Append(a_strAppID);
			strQry.Append("' and enterpriseid = '");
			strQry.Append(a_strEnterpriseID);
			strQry.Append("' and datefrom <= ");
			strQry.Append(Utility.DateFormat(a_strAppID,a_strEnterpriseID,a_stEstmDlvryDate,DTFormat.Date));
			strQry.Append(" and dateto >= ");
			strQry.Append(Utility.DateFormat(a_strAppID,a_strEnterpriseID,a_stEstmDlvryDate,DTFormat.Date));
			//String strDate = Utility.DateFormat(a_strAppID,a_strEnterpriseID,a_stEstmDlvryDate,DTFormat.Date);
			//strQry.Append(strDate.Substring(0,strDate.Length-1));
			///strQry.Append(" "+"23:59'");
			
			
			//dbCmd = dbConApp.CreateCommand(strQry.ToString(),CommandType.Text);
			dbCmd.CommandText = strQry.ToString();
			try
			{
				//dsHoliday =(DataSet)dbConApp.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
				dsHoliday =(DataSet)dbCon.ExecuteQueryInTransaction(dbCmd,com.common.DAL.ReturnType.DataSetType);
			}
			catch(ApplicationException appExpection)
			{
				Logger.LogTraceError("TIESUtility","IsDayHoliday","ERR002","Error in the query String");
				throw appExpection;
			}
			if(dsHoliday.Tables[0].Rows.Count > 0)
			{
				isHoliday = true;
			}
			return isHoliday;
		}
		

		/// <summary>
		/// This method will return whether there is service for Saturday and Sunday
		/// </summary>
		/// <param name="a_strAppID"> Application ID</param>
		/// <param name="a_strEnterpriseID"> Enterprise ID</param>
		/// <param name="a_strZipCode"> Sender Zip Code</param>
		/// <param name="a_strWeekDay">Week Day</param>
		/// <param name="a_strSrvcCode">Service Code</param>
		/// <returns>Returns struct(ServiceData) datatype</returns>
		public static ServiceData IsServiceAvailable(String a_strAppID,String a_strEnterpriseID,String a_strZipCode, String a_strWeekDay,String a_strDayType)
		{
			DbConnection dbConApp = null;
			IDbCommand dbCmd = null;
			DataSet dsService = null;
			ServiceData serviceData = new ServiceData();

			dbConApp = DbConnectionManager.GetInstance().GetDbConnection(a_strAppID,a_strEnterpriseID);

			if(dbConApp == null)
			{
				Logger.LogTraceError("TIESUtility","IsServiceAvailable","ERR001","dbConApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			StringBuilder strQry = new StringBuilder();
			strQry.Append("select * from enterprise where applicationid = '");
			strQry.Append(a_strAppID);
			strQry.Append("' and enterpriseid = '");
			strQry.Append(a_strEnterpriseID+"'");
			//strQry.Append("' and zipcode ='");
			//strQry.Append(a_strZipCode);
			//strQry.Append("'");
			
			if(a_strDayType.Equals("W"))
			{
				if(a_strWeekDay.Equals("Saturday"))
				{
					strQry.Append(" and sat_delivery_avail = 'Y'");
				}
				else if(a_strWeekDay.Equals("Sunday"))
				{
					strQry.Append(" and sun_delivery_avail = 'Y'");
				}
			}
			else if(a_strDayType.Equals("H"))
			{
				strQry.Append(" and pub_delivery_avail = 'Y'");
			}
			
			dbCmd = dbConApp.CreateCommand(strQry.ToString(),CommandType.Text);
			try
			{
				dsService =(DataSet)dbConApp.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
			}
			catch(ApplicationException appExpection)
			{
				Logger.LogTraceError("TIESUtility","IsServiceAvailable","ERR002","Error in the query String "+appExpection.Message);
				throw appExpection;
			}
			if(dsService.Tables[0].Rows.Count > 0)
			{
				DataRow drEach = dsService.Tables[0].Rows[0];

				String strDlvry = "N";
				String strVASCode = null;

				if(a_strDayType.Equals("W"))
				{
					if(a_strWeekDay.Equals("Saturday"))
					{
						if(!drEach["sat_delivery_avail"].GetType().Equals(System.Type.GetType("System.DBNull")))
						{
							strDlvry = (String)drEach["sat_delivery_avail"].ToString();
							strVASCode = (String)drEach["sat_delivery_vas_code"].ToString();
						}
					}
					else if(a_strWeekDay.Equals("Sunday"))
					{
						if(!drEach["sun_delivery_avail"].GetType().Equals(System.Type.GetType("System.DBNull")))
						{
							strDlvry = (String)drEach["sun_delivery_avail"].ToString();
							strVASCode = (String)drEach["sun_delivery_vas_code"].ToString();
						}
					}
				}
				else if (a_strDayType.Equals("H"))
				{
					if(!drEach["pub_delivery_avail"].GetType().Equals(System.Type.GetType("System.DBNull")))
					{
						strDlvry = (String)drEach["pub_delivery_avail"].ToString();
						strVASCode = (String)drEach["pub_delivery_vas_code"].ToString();
					}

				}
				
				if(strDlvry.Equals("Y"))
				{
					serviceData.isServiceAvail = true;
					serviceData.strVASCode = strVASCode;
				}
				
			}
			return serviceData;
		}

		/// <summary>
		/// This method will return the details of cash customer
		/// </summary>
		/// <param name="a_strAppID"> Application ID</param>
		/// <param name="a_strEnterpriseID"> Enterprise ID</param>
		/// <returns>Returns DataSet </returns>
		public static DataSet GetCashCustomer(String a_strAppID,String a_strEnterpriseID)
		{
			DbConnection dbCon= null;
			DataSet dsCustomer =null;
			IDbCommand dbcmd=null;

			dbCon = DbConnectionManager.GetInstance().GetDbConnection(a_strAppID,a_strEnterpriseID);

			if (dbCon==null)
			{
				Logger.LogTraceError("TIESUtility.cs","GetCashCustomer","ERR001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			StringBuilder strQry =  new StringBuilder(); 

			strQry = strQry.Append("select * from customer a");
			strQry.Append(" where a.applicationid = '");
			strQry.Append(a_strAppID);
			strQry.Append ("' and a.enterpriseid = '");
			strQry.Append (a_strEnterpriseID);
			strQry.Append ("' and a.custid ='CASHCUSTOMER'");

			dbcmd = dbCon.CreateCommand(strQry.ToString(),CommandType.Text);
 
			try
			{
				dsCustomer = (DataSet)dbCon.ExecuteQuery(dbcmd,com.common.DAL.ReturnType.DataSetType);  
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("TIESUtility.cs","GetCashCustomer","ERR002","Error in Query String");
				throw appException;
			}
			return dsCustomer;
		}

		/// <summary>
		/// This method will return the VAS Code & Surcharge for a Agent/Customer if it exists.
		/// </summary>
		/// <param name="a_strAppID"> Application ID</param>
		/// <param name="a_strEnterpriseID"> Enterprise ID</param>
		/// /// <param name="a_strPayerType"> Payer type: 'A' for Agent and 'C' for Customer</param>
		/// <param name="a_strPayerID"> Customer or Agent ID</param>
		/// <param name="a_strQuotationNo"> Quotation Number</param>
		/// <param name="a_iQuotationVer"> Quotation Version</param>
		/// <param name="a_strVASCode"> VAS Code</param>
		/// <returns>Return the Surcharge and VAS Code as struct data type </returns>
		public static VASSurcharge GetSurchrgVASCode(String a_strAppID,String a_strEnterpriseID,String a_strPayerType,String a_strPayerID,String a_strQuotationNo,int a_iQuotationVer,String a_strVASCode)
		{
			String strVASCode = null;
			decimal decSurcharge = 0;
			String strVASDesc = null;
			DbConnection dbConApp = null;
			IDbCommand dbCmd = null;
			DataSet dsVASSurchrg = null;
			VASSurcharge vasSurcharge = new VASSurcharge();

			dbConApp = DbConnectionManager.GetInstance().GetDbConnection(a_strAppID,a_strEnterpriseID);

			if(dbConApp == null)
			{
				Logger.LogTraceError("TIESUtility","GetSurchrgVASCode","ERR001","dbConApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			StringBuilder strQry = new StringBuilder();
			
			if(a_strPayerType.Equals("C"))
			{
				strQry.Append("select a.vas_code,a.surcharge,b.vas_description from customer_quotation_vas a,vas b where a.applicationid='");
				strQry.Append(a_strAppID);
				strQry.Append("' and a.enterpriseid = '");
				strQry.Append(a_strEnterpriseID);
				strQry.Append("' and a.custid = '");
				strQry.Append(Utility.ReplaceSingleQuote(a_strPayerID));
				strQry.Append("' and a.quotation_no = '");
				strQry.Append(a_strQuotationNo);
				strQry.Append("' and a.quotation_version = ");
				strQry.Append(a_iQuotationVer);
				strQry.Append(" and a.vas_code = '");
				strQry.Append(a_strVASCode);
				strQry.Append("' and b.applicationid = '");
				strQry.Append(a_strAppID);
				strQry.Append("' and b.enterpriseid = '");
				strQry.Append(a_strEnterpriseID);
				strQry.Append("' and b.vas_code = a.vas_code ");
			}
			else if(a_strPayerType.Equals("A"))
			{
				strQry.Append("select a.vas_code,a.surcharge,b.vas_description from agent_quotation_vas a,vas b where a.applicationid='");
				strQry.Append(a_strAppID);
				strQry.Append("' and a.enterpriseid = '");
				strQry.Append(a_strEnterpriseID);
				strQry.Append("' and a.agentid = '");
				strQry.Append(a_strPayerID);
				strQry.Append("' and a.quotation_no = '");
				strQry.Append(a_strQuotationNo);
				strQry.Append("' and a.quotation_version = ");
				strQry.Append(a_iQuotationVer);
				strQry.Append(" and a.vas_code = '");
				strQry.Append(a_strVASCode);
				strQry.Append("' and b.applicationid = '");
				strQry.Append(a_strAppID);
				strQry.Append("' and b.enterpriseid = '");
				strQry.Append(a_strEnterpriseID);
				strQry.Append("' and b.vas_code = a.vas_code ");
			}

			dbCmd = dbConApp.CreateCommand(strQry.ToString(),CommandType.Text);
			try
			{
				dsVASSurchrg =(DataSet)dbConApp.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
			}
			catch(ApplicationException appExpection)
			{
				Logger.LogTraceError("TIESUtility","GetSurchrgVASCode","ERR002","Error in the query String");
				throw appExpection;
			}
			if(dsVASSurchrg.Tables[0].Rows.Count > 0)
			{
				DataRow drEach = dsVASSurchrg.Tables[0].Rows[0];

				if(!drEach["vas_code"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					strVASCode = (String)drEach["vas_code"];					
				}
				if(!drEach["surcharge"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					decSurcharge = (decimal)drEach["surcharge"];					
				}
				if(!drEach["vas_description"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					strVASDesc = (String)drEach["vas_description"];
				}
			}
			vasSurcharge.decSurcharge = decSurcharge;
			vasSurcharge.strVASCode = strVASCode;
			vasSurcharge.strVASDesc = strVASDesc;
			return vasSurcharge;
		}

		/// <summary>
		/// This method returns a struct which contains the Exception Code & description.
		/// </summary>
		/// <param name="a_strAppID">Application ID</param>
		/// <param name="a_strEnterpriseID">Enterprise ID</param>
		/// <param name="strExcepCode">Exception Code</param>
		/// <param name="strStatusCode">Exception Description</param>
		/// <returns>Returns struct(ExceptionCodeDesc) data type</returns>
		public static ExceptionCodeDesc GetExceptionCodeDesc(String a_strAppID,String a_strEnterpriseID,String strExcepCode,String strStatusCode)
		{
			DbConnection dbConApp = null;
			IDbCommand dbCmd = null;
			DataSet dsExcepCode = null;
			ExceptionCodeDesc exceptionCodeDesc = new ExceptionCodeDesc();
			String strExceptionCd = null;
			String strExceptionDesc = null;

			dbConApp = DbConnectionManager.GetInstance().GetDbConnection(a_strAppID,a_strEnterpriseID);

			if(dbConApp == null)
			{
				Logger.LogTraceError("TIESUtility","GetExceptionCodeDesc","ERR001","dbConApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			StringBuilder strQry = new StringBuilder();
			strQry.Append(" Select * from exception_code where applicationid = '");
			strQry.Append(a_strAppID);
			strQry.Append("' and enterpriseid = '");
			strQry.Append(a_strEnterpriseID);
			strQry.Append("' ");
			strQry.Append(" and active_st = 'Y'");	//Jeab 22 Mar 2012
			//Modify by Gwang on 4-Feb-08
			if(strStatusCode != null && strStatusCode.Trim() != "")
			{
				strQry.Append(" and status_code = '");
				strQry.Append(strStatusCode+"'");
			}
			if(strExcepCode != "")  //Jeab 22 Mar 2012
			{
				strQry.Append(" and exception_code = '");
				strQry.Append(strExcepCode+"'");
			}
			else
			{
				strQry.Append(" and exception_code = '");
				strQry.Append("'");
			}			
			
			dbCmd = dbConApp.CreateCommand(strQry.ToString(),CommandType.Text);
			try
			{
				dsExcepCode =(DataSet)dbConApp.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
			}
			catch(ApplicationException appExpection)
			{
				Logger.LogTraceError("TIESUtility","GetExceptionCodeDesc","ERR002","Error in the query String");
				throw appExpection;
			}
			if(dsExcepCode.Tables[0].Rows.Count > 0)
			{
				DataRow drEach = dsExcepCode.Tables[0].Rows[0];

				if((drEach["exception_code"] != null) && (!drEach["exception_code"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					strExceptionCd = (String)drEach["exception_code"];					
				}
				if((drEach["exception_description"] != null) && (!drEach["exception_description"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					strExceptionDesc = (String)drEach["exception_description"];					
				}
			}

			exceptionCodeDesc.strExceptionCode = strExceptionCd;
			exceptionCodeDesc.strExceptionDesc = strExceptionDesc;
			return exceptionCodeDesc;
		}
		/// <summary>
		/// This method returns a struct of status Code and description.
		/// </summary>
		/// <param name="a_strAppID">Application ID</param>
		/// <param name="a_strEnterpriseID">Enterprise ID</param>
		/// <param name="strStatusCode">Status Code</param>
		/// <param name="strType">Status Description</param>
		/// <returns>Returns the struct(StatusCodeDesc) data type</returns>
		/// 
		public static StatusCodeDesc GetStatusCodeInfo(String a_strAppID,String a_strEnterpriseID,String strStatusCode)
		{
			DbConnection dbConApp = null;
			IDbCommand dbCmd = null;
			DataSet dsStatusCode = null;
			StatusCodeDesc statusCodeDesc = new StatusCodeDesc();
			String strStatusCd = null;
			String strStatusDesc = null;
			String strAuto_Location = null;

			dbConApp = DbConnectionManager.GetInstance().GetDbConnection(a_strAppID,a_strEnterpriseID);

			if(dbConApp == null)
			{
				Logger.LogTraceError("TIESUtility","GetStatusCodeDesc","ERR001","dbConApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			StringBuilder strQry = new StringBuilder();
			strQry.Append(" Select * from status_code where applicationid = '");
			strQry.Append(a_strAppID);
			strQry.Append("' and enterpriseid = '");
			strQry.Append(a_strEnterpriseID);
			strQry.Append("' and status_code = '");
			strQry.Append(strStatusCode+"'");
			strQry.Append(" ");
			
			dbCmd = dbConApp.CreateCommand(strQry.ToString(),CommandType.Text);
			try
			{
				dsStatusCode =(DataSet)dbConApp.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
			}
			catch(ApplicationException appExpection)
			{
				Logger.LogTraceError("TIESUtility","GetExceptionCodeDesc","ERR002","Error in the query String");
				throw appExpection;
			}
			if(dsStatusCode.Tables[0].Rows.Count > 0)
			{
				DataRow drEach = dsStatusCode.Tables[0].Rows[0];

				if((drEach["status_code"] != null) && (!drEach["status_code"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					strStatusCd = (String)drEach["status_code"];					
				}
				if((drEach["status_description"] != null) && (!drEach["status_description"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					strStatusDesc = (String)drEach["status_description"];					
				}
				//by X FEB 06 08
				if((drEach["auto_location"] != null) && (!drEach["auto_location"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					strAuto_Location = (String)drEach["auto_location"];					
				}
			}

			statusCodeDesc.strStatusCode = strStatusCd;
			statusCodeDesc.strStatusDesc = strStatusDesc;
			statusCodeDesc.strAuto_Location = strAuto_Location;
			return statusCodeDesc;
		}


		public static StatusCodeDesc GetStatusCodeDesc(String a_strAppID,String a_strEnterpriseID,String strStatusCode,String strType)
		{
			DbConnection dbConApp = null;
			IDbCommand dbCmd = null;
			DataSet dsStatusCode = null;
			StatusCodeDesc statusCodeDesc = new StatusCodeDesc();
			String strStatusCd = null;
			String strStatusDesc = null;
			String strAuto_Location = null;
			String Allow_in_SWB= null; 

			dbConApp = DbConnectionManager.GetInstance().GetDbConnection(a_strAppID,a_strEnterpriseID);

			if(dbConApp == null)
			{
				Logger.LogTraceError("TIESUtility","GetStatusCodeDesc","ERR001","dbConApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			StringBuilder strQry = new StringBuilder();
			strQry.Append(" Select * from status_code where applicationid = '");
			strQry.Append(a_strAppID);
			strQry.Append("' and enterpriseid = '");
			strQry.Append(a_strEnterpriseID);
			strQry.Append("' and status_code = '");
			strQry.Append(strStatusCode+"'");
			strQry.Append(" and (status_type = 'B' ");
			strQry.Append(" or status_type = '");
			strQry.Append(strType+"')");
			
			
			dbCmd = dbConApp.CreateCommand(strQry.ToString(),CommandType.Text);
			try
			{
				dsStatusCode =(DataSet)dbConApp.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
			}
			catch(ApplicationException appExpection)
			{
				Logger.LogTraceError("TIESUtility","GetExceptionCodeDesc","ERR002","Error in the query String");
				throw appExpection;
			}
			if(dsStatusCode.Tables[0].Rows.Count > 0)
			{
				DataRow drEach = dsStatusCode.Tables[0].Rows[0];

				if((drEach["status_code"] != null) && (!drEach["status_code"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					strStatusCd = (String)drEach["status_code"];					
				}
				if((drEach["status_description"] != null) && (!drEach["status_description"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					strStatusDesc = (String)drEach["status_description"];					
				}
				//by X FEB 06 08
				if((drEach["auto_location"] != null) && (!drEach["auto_location"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					strAuto_Location = (String)drEach["auto_location"];					
				}
				if((drEach["allowinSWB_Emu"] != null) && (!drEach["allowinSWB_Emu"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					Allow_in_SWB = (String)drEach["allowinSWB_Emu"];					
				}
			}

			statusCodeDesc.strStatusCode = strStatusCd;
			statusCodeDesc.strStatusDesc = strStatusDesc;
			statusCodeDesc.strAuto_Location = strAuto_Location;
			statusCodeDesc.Allow_in_SWB = Allow_in_SWB;
			return statusCodeDesc;
		}

		public static DataSet GetStatusAllowInSWB(String a_strAppID,String a_strEnterpriseID)
		{
			DbConnection dbConApp = null;
			IDbCommand dbCmd = null;
			DataSet dsStatusCode = null;


			dbConApp = DbConnectionManager.GetInstance().GetDbConnection(a_strAppID,a_strEnterpriseID);

			if(dbConApp == null)
			{
				Logger.LogTraceError("TIESUtility","GetStatusCodeDesc","ERR001","dbConApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			StringBuilder strQry = new StringBuilder();
			strQry.Append(" Select * from status_code where applicationid = '");
			strQry.Append(a_strAppID);
			strQry.Append("' and enterpriseid = '");
			strQry.Append(a_strEnterpriseID);
			strQry.Append("' and (AllowInSWB_Emu='Y')");

			
			
			dbCmd = dbConApp.CreateCommand(strQry.ToString(),CommandType.Text);
			try
			{
				dsStatusCode =(DataSet)dbConApp.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
			}
			catch(ApplicationException appExpection)
			{
				Logger.LogTraceError("TIESUtility","GetExceptionCodeDesc","ERR002","Error in the query String");
				throw appExpection;
			}


			return dsStatusCode;
		}


		/// <summary>
		/// This method checks whether the booking number exists in PickUprequest table or not & returns a boolean.
		/// Returns true if it exists else returns false.
		/// </summary>
		/// <param name="a_strAppID">Application ID</param>
		/// <param name="a_strEnterpriseID">Enterprise ID</param>
		/// <param name="booking_no">Booking Number</param>
		/// <returns>returns boolean</returns>
		public static bool IsPickUpBookingExist(String a_strAppID,String a_strEnterpriseID,int booking_no)
		{
			IDbCommand dbCmd = null;
			StringBuilder strBuild = null;
			bool isExists = false;

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(a_strAppID,a_strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("TIESUtility","IsPickUpBookingExist","ERR001","DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null",null);
			}

			strBuild =  new StringBuilder();
			strBuild.Append("select * from pickup_request where applicationid = '");
			strBuild.Append(a_strAppID+"'");
			strBuild.Append(" and enterpriseid = '");
			strBuild.Append(a_strEnterpriseID+"'");
			strBuild.Append(" and booking_no = ");
			strBuild.Append(booking_no);
			
			try
			{
				dbCmd = dbCon.CreateCommand(strBuild.ToString(),CommandType.Text);
				DataSet dsPickup =(DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
				if (dsPickup.Tables[0].Rows.Count > 0)
				{
					isExists = true;
				}

			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("TIESUtility","IsPickUpBookingExist","ERR002","Error : "+ appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ",appException);
			}
			catch(Exception exception)
			{
				Logger.LogTraceError("TIESUtility","IsPickUpBookingExist","ERR003","Error : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}
			return isExists;
		}
		/// <summary>
		/// This method checks whether the Shipment is invoiced.
		/// Returns true if it is invoiced else returns false.
		/// </summary>
		/// <param name="a_strAppID"></param>
		/// <param name="a_strEnterpriseID"></param>
		/// <param name="ibookingNo"></param>
		/// <returns>returns boolean</returns>
		public static bool IsInvoiced(String a_strAppID,String a_strEnterpriseID,int ibookingNo,String ConsignNo)
		{
			bool isInvoiced = false;
			IDbCommand dbCmd = null;
			StringBuilder strBuild = null;
			
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(a_strAppID,a_strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("TIESUtility","IsInvoiceable","ERR001","DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null",null);
			}

			strBuild =  new StringBuilder();
			strBuild.Append("select * from shipment where applicationid = '");
			strBuild.Append(a_strAppID+"'");
			strBuild.Append(" and enterpriseid = '");
			strBuild.Append(a_strEnterpriseID+"'");
			strBuild.Append(" and booking_no = ");
			strBuild.Append(ibookingNo);
			strBuild.Append(" and consignment_no = '");
			strBuild.Append(ConsignNo);
			strBuild.Append("' and invoice_no is not null and invoice_no != ''");
			
			try
			{
				dbCmd = dbCon.CreateCommand(strBuild.ToString(),CommandType.Text);
				DataSet dsPickup =(DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
				if (dsPickup.Tables[0].Rows.Count > 0)
				{
					isInvoiced = true;
				}

			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("TIESUtility","IsInvoiceable","ERR002","Error : "+ appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ",appException);
			}
			catch(Exception exception)
			{
				Logger.LogTraceError("TIESUtility","IsInvoiceable","ERR003","Error : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}
			return isInvoiced;

		}
		/// <summary>
		/// The method sets the boolean value depending on whether service is available for the 
		/// given VAS Code & ZipCode 
		/// </summary>
		/// <param name="a_strAppID">Application ID</param>
		/// <param name="a_strEnterpriseID">Enterprise ID</param>
		/// <param name="strVASCode">VAS Code</param>
		/// <param name="strDestZipCode">Destination ZipCode</param>
		/// <returns>returns boolean</returns>
		public static bool IsVASExcluded(String a_strAppID,String a_strEnterpriseID,String strVASCode,String strDestZipCode)
		{
			bool isExcluded = true;
			IDbCommand dbCmd = null;
			
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(a_strAppID,a_strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("TIESUtility","IsVASExcluded","ERR001","DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null",null);
			}

			StringBuilder strQry = new StringBuilder();
			strQry.Append("select * from VAS a where a.applicationid = '");
			strQry.Append(a_strAppID);
			strQry.Append("' and a.enterpriseid = '");
			strQry.Append(a_strEnterpriseID);
			strQry.Append("'");
			strQry.Append(" and a.vas_code = '");
			strQry.Append(Utility.ReplaceSingleQuote(strVASCode));
			strQry.Append("'");
			strQry.Append(" and a.vas_code not in (select b.vas_code from zipcode_vas_excluded b where b.applicationid = '");
			strQry.Append(a_strAppID);
			strQry.Append("' and b.enterpriseid = '");
			strQry.Append(a_strEnterpriseID+"'");
			strQry.Append(" and b.zipcode = '");
			strQry.Append(Utility.ReplaceSingleQuote(strDestZipCode));
			strQry.Append("'");		
			strQry.Append(")");
			
			try
			{
				dbCmd = dbCon.CreateCommand(strQry.ToString(),CommandType.Text);
				DataSet dsVASExcluded =(DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
				if (dsVASExcluded.Tables[0].Rows.Count > 0)
				{
					isExcluded = false;
				}

			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("TIESUtility","IsVASExcluded","ERR002","Error : "+ appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ",appException);
			}
			catch(Exception exception)
			{
				Logger.LogTraceError("TIESUtility","IsVASExcluded","ERR003","Error : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}
			return isExcluded;

		}

		/// <summary>
		/// This method is used for checking whether the delivery date is less than the minimum required days for a particular service.
		/// </summary>
		/// <param name="a_strAppID">Application ID</param>
		/// <param name="a_strEnterpriseID">Enterprise ID</param>
		/// <param name="strServiceCode">Service Code</param>
		/// <param name="dtDelivery">Deliver date time</param>
		/// <param name="dtPickup">Pickupdatetime</param>
		/// <returns>Struct is returned</returns>
		public static TransitDayNServiceCode IsDelvryDtLess(String a_strAppID,String a_strEnterpriseID,String strServiceCode,DateTime dtDelivery,DateTime dtPickup)
		{
			TransitDayNServiceCode DayNService = new TransitDayNServiceCode();
			DayNService.iTransitDay = 0;
			DayNService.strServiceCode = null;
			DayNService.isLess = false;

			Service service = new Service();
			service.Populate(a_strAppID,a_strEnterpriseID,strServiceCode);
			double iTransitDay = Convert.ToDouble(service.TransitDay);		
			if(iTransitDay > 0)
			{
				DayNService.iTransitDay = Convert.ToDecimal(iTransitDay);
				DayNService.strServiceCode = strServiceCode;
				DateTime dtPickupDateTime =  dtPickup.AddDays(iTransitDay);
				String strPickUpDate = dtPickupDateTime.ToString("dd/MM/yyyy");
				dtPickupDateTime = DateTime.ParseExact(strPickUpDate,"dd/MM/yyyy",null);

				String strDlvryDate = dtDelivery.ToString("dd/MM/yyyy");
				dtDelivery = DateTime.ParseExact(strDlvryDate,"dd/MM/yyyy",null);

				int iCompare = dtDelivery.CompareTo(dtPickupDateTime);
				
				if(iCompare < 0)
				{
					DayNService.isLess = true;
				}
			}
			return DayNService;
		}

		/// <summary>
		/// This method gets the state name for the given state code.
		/// </summary>
		/// <param name="a_strAppID">Application ID</param>
		/// <param name="a_strEnterpriseID">Enterprise ID</param>
		/// <param name="strStatecode">State Code</param>
		/// <returns>String</returns>
		public static String GetStateDesc(String a_strAppID,String a_strEnterpriseID,String strStatecode)
		{
			DbConnection dbConApp = null;
			IDbCommand dbCmd = null;
			DataSet dsState = null;
			String strStateDesc = null;
			dbConApp = DbConnectionManager.GetInstance().GetDbConnection(a_strAppID,a_strEnterpriseID);

			if(dbConApp == null)
			{
				Logger.LogTraceError("TIESUtility","GetStateDesc","ERR001","dbConApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			StringBuilder strQry = new StringBuilder();
			strQry.Append(" Select * from state where applicationid = '");
			strQry.Append(a_strAppID);
			strQry.Append("' and enterpriseid = '");
			strQry.Append(a_strEnterpriseID);
			strQry.Append("' and state_code = '");
			strQry.Append(strStatecode+"'");

			dbCmd = dbConApp.CreateCommand(strQry.ToString(),CommandType.Text);
			try
			{
				dsState =(DataSet)dbConApp.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
			}
			catch(ApplicationException appExpection)
			{
				Logger.LogTraceError("TIESUtility","GetStateDesc","ERR002","Error in the query String");
				throw appExpection;
			}
			if(dsState.Tables[0].Rows.Count > 0)
			{
				DataRow drEach = dsState.Tables[0].Rows[0];

				if((drEach["state_name"] != null) && (!drEach["state_name"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					strStateDesc = (String)drEach["state_name"];					
				}
			}

			return strStateDesc;
		}

		/// <summary>
		/// This method gets the description of the given Path Code.
		/// </summary>
		/// <param name="a_strAppID">Application ID</param>
		/// <param name="a_strEnterpriseID">Enterprise ID</param>
		/// <param name="strPathCode">Path Code</param>
		/// <returns></returns>
		public static String GetPathCodeDesc(String a_strAppID,String a_strEnterpriseID,String strPathCode)
		{
			DbConnection dbConApp = null;
			IDbCommand dbCmd = null;
			DataSet dsPath = null;
			String strPathDesc = null;
			dbConApp = DbConnectionManager.GetInstance().GetDbConnection(a_strAppID,a_strEnterpriseID);

			if(dbConApp == null)
			{
				Logger.LogTraceError("TIESUtility","GetPathCodeDesc","ERR001","dbConApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			StringBuilder strQry = new StringBuilder();
			strQry.Append(" Select * from delivery_path where applicationid = '");
			strQry.Append(a_strAppID);
			strQry.Append("' and enterpriseid = '");
			strQry.Append(a_strEnterpriseID);
			strQry.Append("' and path_code = '");
			strQry.Append(Utility.ReplaceSingleQuote(strPathCode)+"'");

			dbCmd = dbConApp.CreateCommand(strQry.ToString(),CommandType.Text);
			try
			{
				dsPath =(DataSet)dbConApp.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
			}
			catch(ApplicationException appExpection)
			{
				Logger.LogTraceError("TIESUtility","GetPathCodeDesc","ERR002","Error in the query String");
				throw appExpection;
			}
			if(dsPath.Tables[0].Rows.Count > 0)
			{
				DataRow drEach = dsPath.Tables[0].Rows[0];

				if((drEach["path_description"] != null) && (!drEach["path_description"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					strPathDesc = (String)drEach["path_description"];					
				}
			}

			return strPathDesc;
		}

		/// <summary>
		/// This method gets the description of the given Path Code.
		/// </summary>
		/// <param name="a_strAppID">Application ID</param>
		/// <param name="a_strEnterpriseID">Enterprise ID</param>
		/// <param name="strPathCode">Path Code</param>
		/// <returns></returns>

		public static String GetPathDesc_Type(String a_strAppID,String a_strEnterpriseID,String strPathCode)
		{
			DbConnection dbConApp = null;
			IDbCommand dbCmd = null;
			DataSet dsPath = null;
			String strPathDesc_Type = null;
			dbConApp = DbConnectionManager.GetInstance().GetDbConnection(a_strAppID,a_strEnterpriseID);

			if(dbConApp == null)
			{
				Logger.LogTraceError("TIESUtility","GetPathCodeDesc","ERR001","dbConApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			StringBuilder strQry = new StringBuilder();
			strQry.Append(" Select * from delivery_path where applicationid = '");
			strQry.Append(a_strAppID);
			strQry.Append("' and enterpriseid = '");
			strQry.Append(a_strEnterpriseID);
			strQry.Append("' and path_code = '");
			strQry.Append(Utility.ReplaceSingleQuote(strPathCode)+"'");

			dbCmd = dbConApp.CreateCommand(strQry.ToString(),CommandType.Text);
			try
			{
				dsPath =(DataSet)dbConApp.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
			}
			catch(ApplicationException appExpection)
			{
				Logger.LogTraceError("TIESUtility","GetPathCodeDesc","ERR002","Error in the query String");
				throw appExpection;
			}
			if(dsPath.Tables[0].Rows.Count > 0)
			{
				DataRow drEach = dsPath.Tables[0].Rows[0];

				if((drEach["Delivery_Type"] != null) && (!drEach["Delivery_Type"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					strPathDesc_Type = (String)drEach["Delivery_Type"];					
				}

				if((drEach["path_description"] != null) && (!drEach["path_description"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					strPathDesc_Type = strPathDesc_Type+","+(String)drEach["path_description"];					
				}
			}

			return strPathDesc_Type;
		}

		/// This method gets the Route Code By Path Code.
		/// </summary>
		/// <param name="a_strAppID">Application ID</param>
		/// <param name="a_strEnterpriseID">Enterprise ID</param>
		/// <param name="strPathCode">Path Code</param>
		/// <returns></returns>

		public static String GetRouteCodeByPathCode(String a_strAppID,String a_strEnterpriseID,String strPathCode)
		{
			DbConnection dbConApp = null;
			IDbCommand dbCmd = null;
			DataSet dsRouteCode = null;
			String strRouteCode = null;
			dbConApp = DbConnectionManager.GetInstance().GetDbConnection(a_strAppID,a_strEnterpriseID);

			if(dbConApp == null)
			{
				Logger.LogTraceError("TIESUtility","GetRouteCodeByPathCode","ERR001","dbConApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			StringBuilder strQry = new StringBuilder();
			strQry.Append(" Select * from Route_Code where applicationid = '");
			strQry.Append(a_strAppID);
			strQry.Append("' and enterpriseid = '");
			strQry.Append(a_strEnterpriseID);
			strQry.Append("' and path_code = '");
			strQry.Append(Utility.ReplaceSingleQuote(strPathCode)+"'");

			dbCmd = dbConApp.CreateCommand(strQry.ToString(),CommandType.Text);
			try
			{
				dsRouteCode =(DataSet)dbConApp.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
			}
			catch(ApplicationException appExpection)
			{
				Logger.LogTraceError("TIESUtility","GetRouteCodeByPathCode","ERR002","Error in the query String");
				throw appExpection;
			}
			if(dsRouteCode.Tables[0].Rows.Count > 0)
			{
				DataRow drEach = dsRouteCode.Tables[0].Rows[0];

				if((drEach["route_code"] != null) && (!drEach["route_code"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					strRouteCode = (String)drEach["route_Code"];					
				}
			}

			return strRouteCode;
		}
		
		/// <summary>
		/// This method gets the Agent's pickup cost per consignment from the Agent_zipcode table.
		/// </summary>
		/// <param name="a_strAppID">Application ID</param>
		/// <param name="a_strEnterpriseID">Enterprise ID</param>
		/// <param name="strAgentID">Agent ID</param>
		/// <param name="strSndZipCode">Sender Zip Code</param>
		/// <returns>decimal</returns>
		public static AgentConsgCost GetAgentConsignmentCost(String a_strAppID,String a_strEnterpriseID,String strZipCode)
		{
			DbConnection dbConApp = null;
			IDbCommand dbCmd = null;
			DataSet ds = null;
			
			AgentConsgCost agentCost = new AgentConsgCost();
			agentCost.strAgentID = null;
			agentCost.decCost = 0;

			dbConApp = DbConnectionManager.GetInstance().GetDbConnection(a_strAppID,a_strEnterpriseID);

			if(dbConApp == null)
			{
				Logger.LogTraceError("TIESUtility","GetAgentPickUpCnsgCost","ERR001","dbConApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			StringBuilder strQry = new StringBuilder();
			strQry.Append(" Select * from Agent_Zipcode where applicationid = '");
			strQry.Append(a_strAppID);
			strQry.Append("' and enterpriseid = '");
			strQry.Append(a_strEnterpriseID);
			strQry.Append("' and zipcode =");
			strQry.Append("'");
			strQry.Append(Utility.ReplaceSingleQuote(strZipCode));
			strQry.Append("'");

			dbCmd = dbConApp.CreateCommand(strQry.ToString(),CommandType.Text);
			try
			{
				ds =(DataSet)dbConApp.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
			}
			catch(ApplicationException appExpection)
			{
				Logger.LogTraceError("TIESUtility","GetAgentPickUpCnsgCost","ERR002","Error in the query String");
				throw appExpection;
			}
			if(ds.Tables[0].Rows.Count > 0)
			{
				DataRow drEach = ds.Tables[0].Rows[0];

				if((drEach["pickup_cost_per_consignment"] != null) && (!drEach["pickup_cost_per_consignment"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					agentCost.decCost = (decimal)drEach["pickup_cost_per_consignment"];					
				}

				if((drEach["agentid"] != null) && (!drEach["agentid"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					agentCost.strAgentID = (String)drEach["agentid"];					
				}
			}

			return agentCost;
		}
		/// <summary>
		/// This method gets the pickup weight cost, if not found it is logged.
		/// </summary>
		/// <param name="a_strAppID">Application ID</param>
		/// <param name="a_strEnterpriseID">Enterprise ID</param>
		/// <param name="strAgentID">Agent ID</param>
		/// <param name="strSndZipCode">Sender Zip Code</param>
		/// <param name="decTotWt">Total Weight</param>
		/// <returns>decimal</returns>
		public static AgentWtCost GetAgentWtCost(String a_strAppID,String a_strEnterpriseID,String strZipCode,decimal decTotWt)
		{
			DbConnection dbConApp = null;
			IDbCommand dbCmd = null;
			DataSet ds = null;
			
			AgentWtCost agentWtCost = new AgentWtCost();
			agentWtCost.strAgentID = null;
			agentWtCost.decCost = 0;

			dbConApp = DbConnectionManager.GetInstance().GetDbConnection(a_strAppID,a_strEnterpriseID);

			if(dbConApp == null)
			{
				Logger.LogTraceError("TIESUtility","GetAgentPickUpWtCost","ERR001","dbConApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			StringBuilder strQry = new StringBuilder();
			strQry.Append(" Select * from Agent_Cost where applicationid = '");
			strQry.Append(a_strAppID);
			strQry.Append("' and enterpriseid = '");
			strQry.Append(a_strEnterpriseID);
			strQry.Append("'");
			strQry.Append(" and zipcode =");
			strQry.Append("'");
			strQry.Append(Utility.ReplaceSingleQuote(strZipCode));
			strQry.Append("'");
			strQry.Append(" and start_wt > = ");
			strQry.Append(decTotWt);
			strQry.Append(" and end_wt <= ");
			strQry.Append(decTotWt);

			dbCmd = dbConApp.CreateCommand(strQry.ToString(),CommandType.Text);
			try
			{
				ds =(DataSet)dbConApp.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
			}
			catch(ApplicationException appExpection)
			{
				Logger.LogTraceError("TIESUtility","GetAgentPickUpWtCost","ERR002","Error in the query String");
				throw appExpection;
			}
			if(ds.Tables[0].Rows.Count > 0)
			{
				DataRow drEach = ds.Tables[0].Rows[0];

				if((drEach["agent_pup_cost_wt"] != null) && (!drEach["agent_pup_cost_wt"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					agentWtCost.decCost = (decimal)drEach["agent_pup_cost_wt"];					
				}

				if((drEach["agentid"] != null) && (!drEach["agentid"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					agentWtCost.strAgentID = (String)drEach["agentid"];	
				}
			}
			else
			{
				Logger.LogTraceError("TIESUtility","GetAgentPickUpWtCost","ERR003","EnterpriseID: "+a_strEnterpriseID+" ZipCode: "+strZipCode);
			}

			return agentWtCost;
		}
		/// <summary>
		/// This method checks whether the Shipment number exists in pickup_shipment table or not & returns a boolean.
		/// Returns true if it exists else returns false.
		/// </summary>
		/// <param name="strAppID"></param>
		/// <param name="strEnterpriseID"></param>
		/// <param name="iBookingNo"></param>
		/// <returns></returns>
		public static bool IsPickupShipmentExist(String strAppID, String strEnterpriseID,int iBookingNo)
		{
			IDbCommand dbCmd = null;
			StringBuilder strBuild = null;
			bool ispickupshipmentexist = false;

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("TIESUtility","IsPickupShipmentExist","ERR001","DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null",null);
			}
			strBuild =  new StringBuilder();
			strBuild.Append("select * from pickup_shipment where applicationid = '");
			strBuild.Append(strAppID+"'");
			strBuild.Append(" and enterpriseid = '");
			strBuild.Append(strEnterpriseID+"'");
			strBuild.Append(" and booking_no = ");
			strBuild.Append(iBookingNo);
			
			try
			{
				dbCmd = dbCon.CreateCommand(strBuild.ToString(),CommandType.Text);
				DataSet dsShipmnt =(DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
				if (dsShipmnt.Tables[0].Rows.Count > 0)
				{
					ispickupshipmentexist = true;
				}

			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("TIESUtility","IsPickupShipmentExist","ERR002","Error : "+ appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ",appException);
			}
			catch(Exception exception)
			{
				Logger.LogTraceError("TIESUtility","IsPickupShipmentExist","ERR003","Error : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}
			return ispickupshipmentexist;

		}
		/// <summary>
		/// This method will get a list of  POD Status Codes.
		/// </summary>
		/// <param name="a_strAppID"> Application ID</param>
		/// <param name="a_strEnterpriseID"> Enterprise ID</param>
		/// <returns>Return an array of string containing the individual status code.</returns>
		public static String[] getPODStatus(string a_strAppID, string a_strEnterpriseID)
		{
			String[] StatusPODList = {"POD"};

			return StatusPODList;
		}
		/// <summary>
		/// This method will use to round up the decimal number according to appId and entPId.
		/// It gets value wr_round_method from enterprise table.
		/// </summary>
		public static string Round_Enterprise_Currency(string strAppID, string strEnterpriseID,decimal fltNumber)
		{
			com.ties.classes.Enterprise objEnt = new com.ties.classes.Enterprise();	
			objEnt.Populate(strAppID,strEnterpriseID);
			decimal fltNo=0;
			string strCurrencyValue="";
			fltNo=System.Math.Round(fltNumber,objEnt.CurrencyDecimal);
			if(objEnt.CurrencyDecimal==1)
			strCurrencyValue=fltNo.ToString("#.0");
			else if(objEnt.CurrencyDecimal==2)
			strCurrencyValue=fltNo.ToString("#.00");
			else if(objEnt.CurrencyDecimal==3)
			strCurrencyValue=fltNo.ToString("#.000");
			else
			strCurrencyValue=fltNo.ToString();
			return strCurrencyValue;
					
		}

		public static DataSet GetInvoiceableByStatusCode(String a_strAppID,String a_strEnterpriseID,String strStatusCode)
		{
			DbConnection dbConApp = null;
			IDbCommand dbCmd = null;
			DataSet dsStatusCode = null;
			StatusCodeDesc statusCodeDesc = new StatusCodeDesc();

			dbConApp = DbConnectionManager.GetInstance().GetDbConnection(a_strAppID,a_strEnterpriseID);

			if(dbConApp == null)
			{
				Logger.LogTraceError("TIESUtility","GetInvoiceableByStatusCode","ERR001","dbConApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			StringBuilder strQry = new StringBuilder();
			strQry.Append(" Select * from status_code where applicationid = '");
			strQry.Append(a_strAppID);
			strQry.Append("' and enterpriseid = '");
			strQry.Append(a_strEnterpriseID);
			strQry.Append("' and status_code = '");
			strQry.Append(strStatusCode+"'");
			
			
			dbCmd = dbConApp.CreateCommand(strQry.ToString(),CommandType.Text);
			try
			{
				dsStatusCode =(DataSet)dbConApp.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
			}
			catch(ApplicationException appExpection)
			{
				Logger.LogTraceError("TIESUtility","GetInvoiceableByStatusCode","ERR002","Error in the query String");
				throw appExpection;
			}
			
			return dsStatusCode;
		}
		
		
		public static DataSet GetInvoiceableByExceptionCode(String a_strAppID,String a_strEnterpriseID,String strStatusCode,String strExceptionCode)
		{
			DbConnection dbConApp = null;
			IDbCommand dbCmd = null;
			DataSet dsStatusCode = null;
			StatusCodeDesc statusCodeDesc = new StatusCodeDesc();

			dbConApp = DbConnectionManager.GetInstance().GetDbConnection(a_strAppID,a_strEnterpriseID);

			if(dbConApp == null)
			{
				Logger.LogTraceError("TIESUtility","GetInvoiceableByExceptionCode","ERR001","dbConApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			StringBuilder strQry = new StringBuilder();
			strQry.Append(" Select * from Exception_Code where applicationid = '");
			strQry.Append(a_strAppID);
			strQry.Append("' and enterpriseid = '");
			strQry.Append(a_strEnterpriseID);
			strQry.Append("' and status_code = '");
			strQry.Append(strStatusCode);
			strQry.Append("' and exception_code = '");
			strQry.Append(strExceptionCode+"'");
			
			
			dbCmd = dbConApp.CreateCommand(strQry.ToString(),CommandType.Text);
			try
			{
				dsStatusCode =(DataSet)dbConApp.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
			}
			catch(ApplicationException appExpection)
			{
				Logger.LogTraceError("TIESUtility","GetInvoiceableByExceptionCode","ERR002","Error in the query String");
				throw appExpection;
			}
			
			return dsStatusCode;
		}
		
		
		public static decimal EnterpriseRounding(decimal beforeRound,int wt_rounding_method,decimal wt_increment_amt)
		{
			decimal reVal = beforeRound;
			decimal BeforDot = 0;
			decimal AfterDot = 0;
			decimal tmp05 = Convert.ToDecimal(0.5);
			string strBeforeRound = beforeRound.ToString();
			int dotIndex = Convert.ToInt32(strBeforeRound.IndexOf(".", 0));

			if(dotIndex < 0) 
			{
				BeforDot = Convert.ToDecimal(strBeforeRound);
				AfterDot = Convert.ToDecimal("0.00");
			}
			else 
			{ 
				BeforDot = Convert.ToDecimal(strBeforeRound.Substring(0, dotIndex));
				AfterDot = Convert.ToDecimal("0." + strBeforeRound.Substring(dotIndex + 1, strBeforeRound.Length - (dotIndex + 1)));
			}

			if(wt_increment_amt == tmp05)
			{
				if(AfterDot == tmp05) 
				{
					reVal = beforeRound;
				}
				else if (AfterDot > tmp05)
				{
					if(wt_rounding_method == 0)
						reVal = BeforDot + 1;
					else if (wt_rounding_method == 1)
						reVal = BeforDot + 1;
					else if (wt_rounding_method == 2)
						reVal = BeforDot + tmp05;
				}
				else if (AfterDot < tmp05)
				{
					if(wt_rounding_method == 0)
						reVal = BeforDot + tmp05;
					else if (wt_rounding_method == 1)
						reVal = BeforDot + tmp05;
					else if (wt_rounding_method == 2)
						reVal = BeforDot;
				}
			}
			else if(wt_increment_amt == 1)
			{
				if(AfterDot == tmp05) 
				{
					if(wt_rounding_method == 0)
						reVal = BeforDot + 1;
					else if (wt_rounding_method == 1)
						reVal = BeforDot + 1;
					else if (wt_rounding_method == 2)
						reVal = BeforDot;
				}
				else if (AfterDot > tmp05)
				{
					if(wt_rounding_method == 0)
						reVal = BeforDot + 1;
					else if (wt_rounding_method == 1)
						reVal = BeforDot + 1;
					else if (wt_rounding_method == 2)
						reVal = BeforDot;
				}
				else if (AfterDot < tmp05)
				{
					if(wt_rounding_method == 0)
						reVal = BeforDot;
					else if (wt_rounding_method == 1)
						reVal = BeforDot + 1;
					else if (wt_rounding_method == 2)
						reVal = BeforDot;
				}
			}

			return reVal;
		}


		/// <summary>
		/// This method checks whether the status_code already exists in status_code.
		/// returns true if it already exists else false.
		/// </summary>
		/// <param name="strAppID">Application ID</param>
		/// <param name="strEnterpriseID">Enterprise ID</param>
		/// <param name="status_code">Status Code</param>
		/// <returns>boolean</returns>
		/// Created by Gwang
		public static bool IsStatusCodeExistBYRole(String strAppID, String strEnterpriseID,String status_code,String UserID)
		{
			IDbCommand dbCmd = null;
			StringBuilder strBuild = null;
			bool isExists = false;

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("TIESUtility","IsStatusCodeExist","ERR001","DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null",null);
			}

			strBuild =  new StringBuilder();
			strBuild.Append("select distinct status_code,status_description,Status_type,status_close,invoiceable,system_code,tt_display_status,tt_display_remarks,auto_process,auto_location,auto_remark,tt_display_location, scanning_type ,AllowRoles, isnull(AllowInSWB_Emu,'N') as AllowInSWB_Emu, isnull(AllowUPD_ByBatch,'N') as AllowUPD_ByBatch from Status_Code ");
			strBuild.Append(" inner join  ( SELECT   distinct  Role_Master.role_name fROM  User_Role_Relation INNER JOIN Role_Master ON User_Role_Relation.applicationid = Role_Master.applicationid AND User_Role_Relation.enterpriseid = Role_Master.enterpriseid AND ");
			strBuild.Append(" User_Role_Relation.roleid = Role_Master.roleid WHERE  (User_Role_Relation.userid = '"+ UserID +"')) a on   status_code.allowRoles like '%' + a.role_name + '%' where applicationid='");
			strBuild.Append(strAppID+"'");
			strBuild.Append(" and enterpriseid = '");
			strBuild.Append(strEnterpriseID+"'");
			strBuild.Append(" and status_code = '");
			strBuild.Append(Utility.ReplaceSingleQuote(status_code)+"'");
			strBuild.Append( " and status_code.AllowInSWB_Emu = 'Y'");
			strBuild.Append( " union ");
			strBuild.Append( " select status_code,status_description,Status_type,status_close,invoiceable,");
			strBuild.Append( " system_code,tt_display_status,tt_display_remarks,auto_process,auto_location,auto_remark,");
			strBuild.Append( " tt_display_location, scanning_type ,AllowRoles, isnull(AllowInSWB_Emu,'N') as AllowInSWB_Emu,");
			strBuild.Append( " isnull(AllowUPD_ByBatch,'N') as AllowUPD_ByBatch from Status_Code where (AllowRoles is null or Rtrim(Ltrim(AllowRoles))='')");
			strBuild.Append( " and (status_code.AllowInSWB_Emu = 'Y')");

			
			try
			{
				dbCmd = dbCon.CreateCommand(strBuild.ToString(),CommandType.Text);
				DataSet dgStatusCode =(DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
				if (dgStatusCode.Tables[0].Rows.Count > 0)
				{
					for(int i=0;i<=dgStatusCode.Tables[0].Rows.Count-1;i++)
					{
						if(dgStatusCode.Tables[0].Rows[i]["status_code"].ToString().Trim()==status_code)
						{
							isExists = true;
							break;
						}
					}
				}

			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("TIESUtility","IsStatusCodeExist","ERR002","Error : "+ appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ",appException);
			}
			catch(Exception exception)
			{
				Logger.LogTraceError("TIESUtility","IsStatusCodeExist","ERR003","Error : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}
			return isExists;
		}


		public static bool IsStatusCodeExist(String strAppID, String strEnterpriseID,String status_code,String strUserID)
		{
			IDbCommand dbCmd = null;
			bool isExists = false;

			//Jeab 12 Apr 2011
//			if (status_code == "CGIN" || status_code == "CGOUT" || status_code == "CLS" || status_code == "DLY02" || status_code == "DLY13"
//				|| status_code == "STS42" || status_code == "STS70" || status_code == "STS77")
//			{
//				isExists = false;
//				return isExists;
//			}
			//Jeab 12 Apr 2011  =========> End

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("TIESUtility","IsStatusCodeExist","ERR001","DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null",null);
			}

//			strBuild =  new StringBuilder();
//			strBuild.Append("select * from status_code where applicationid = '");
//			strBuild.Append(strAppID+"'");
//			strBuild.Append(" and enterpriseid = '");
//			strBuild.Append(strEnterpriseID+"'");
//			strBuild.Append(" and status_code = '");
//			strBuild.Append(Utility.ReplaceSingleQuote(status_code)+"'");
			//Jeab 22 Mar 2012
			String strSQLQuery =" select * from ( ";
			strSQLQuery += "select distinct status_code,status_description,Status_type,status_close,invoiceable,system_code,tt_display_status,tt_display_remarks,auto_process,auto_location,auto_remark,tt_display_location, scanning_type ,AllowRoles, isnull(AllowInSWB_Emu,'N') as AllowInSWB_Emu, isnull(AllowUPD_ByBatch,'N') as AllowUPD_ByBatch from Status_Code ";
			strSQLQuery +=" inner join  ( SELECT   distinct  Role_Master.role_name fROM  User_Role_Relation INNER JOIN Role_Master ON User_Role_Relation.applicationid = Role_Master.applicationid AND User_Role_Relation.enterpriseid = Role_Master.enterpriseid AND ";
			strSQLQuery +=" User_Role_Relation.roleid = Role_Master.roleid WHERE  (User_Role_Relation.userid = '"+ strUserID +"')) a on   status_code.allowRoles like '%' + a.role_name + '%' ";
			strSQLQuery += " where status_code.applicationid = '"+strAppID+"' and status_code.enterpriseid = '"+strEnterpriseID+"' ";
			strSQLQuery += " union ";
			strSQLQuery += " select status_code,status_description,Status_type,status_close,invoiceable, ";
			strSQLQuery += " system_code,tt_display_status,tt_display_remarks,auto_process,auto_location,auto_remark, ";
			strSQLQuery += " tt_display_location, scanning_type ,AllowRoles, isnull(AllowInSWB_Emu,'N') as AllowInSWB_Emu, ";
			strSQLQuery += " isnull(AllowUPD_ByBatch,'N') as AllowUPD_ByBatch from Status_Code where (AllowRoles is null or Rtrim(Ltrim(AllowRoles))='') ";
			strSQLQuery += ") a where ";
			strSQLQuery += " status_code = '";
			strSQLQuery += Utility.ReplaceSingleQuote(status_code)+ "'";
			//Jeab 22 Mar 2012  =========> End
			try
			{
				dbCmd = dbCon.CreateCommand(strSQLQuery.ToString(),CommandType.Text);
				DataSet dgStatusCode =(DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
				if (dgStatusCode.Tables[0].Rows.Count > 0)
				{
					isExists = true;
				}

			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("TIESUtility","IsStatusCodeExist","ERR002","Error : "+ appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ",appException);
			}
			catch(Exception exception)
			{
				Logger.LogTraceError("TIESUtility","IsStatusCodeExist","ERR003","Error : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}
			return isExists;
		}

		public static bool IsStatusMDE(String strAppID, String strEnterpriseID,String consignment)
		{
			IDbCommand dbCmd = null;
			StringBuilder strBuild = null;
			bool isMDE = false;

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("TIESUtility","IsStatusMDE","ERR001","DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null",null);
			}

			strBuild =  new StringBuilder();
			strBuild.Append("select * from shipment_tracking where applicationid = '");
			strBuild.Append(strAppID+"'");
			strBuild.Append(" and enterpriseid = '");
			strBuild.Append(strEnterpriseID+"'");
			strBuild.Append(" and status_code = 'MDE'");
			strBuild.Append(" and consignment_no = '");
			strBuild.Append(Utility.ReplaceSingleQuote(consignment)+"'");

			try
			{
				dbCmd = dbCon.CreateCommand(strBuild.ToString(),CommandType.Text);
				DataSet dgStatusCode =(DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
				if (dgStatusCode.Tables[0].Rows.Count > 0)
				{
					isMDE = true;
				}

			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("TIESUtility","IsStatusCodeExist","ERR002","Error : "+ appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ",appException);
			}
			catch(Exception exception)
			{
				Logger.LogTraceError("TIESUtility","IsStatusCodeExist","ERR003","Error : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}
			return isMDE;
		}
		public static bool isDestinationStation(String strAppID, String strEnterpriseID,String consignment,String Dest_Station)
		{
			IDbCommand dbCmd = null;
			StringBuilder strBuild = null;
			bool isDest = false;

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("TIESUtility","IsStatusMDE","ERR001","DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null",null);
			}

			strBuild =  new StringBuilder();
			strBuild.Append("select * from shipment where applicationid = '");
			strBuild.Append(strAppID+"'");
			strBuild.Append(" and enterpriseid = '");
			strBuild.Append(strEnterpriseID+"'");
			strBuild.Append(" and destination_station = '"+ Dest_Station +"'");
			strBuild.Append(" and consignment_no = '");
			strBuild.Append(Utility.ReplaceSingleQuote(consignment)+"'");

			try
			{
				dbCmd = dbCon.CreateCommand(strBuild.ToString(),CommandType.Text);
				DataSet dgStatusCode =(DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
				if (dgStatusCode.Tables[0].Rows.Count > 0)
				{
					isDest = true;
				}

			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("TIESUtility","IsStatusCodeExist","ERR002","Error : "+ appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ",appException);
			}
			catch(Exception exception)
			{
				Logger.LogTraceError("TIESUtility","IsStatusCodeExist","ERR003","Error : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}
			return isDest;
		}
		public static DataSet GetStatusCodeByExceptCode(String a_strAppID,String a_strEnterpriseID,String strExceptionCode)
		{
			DbConnection dbConApp = null;
			IDbCommand dbCmd = null;
			DataSet dsStatusCode = null;

			dbConApp = DbConnectionManager.GetInstance().GetDbConnection(a_strAppID,a_strEnterpriseID);

			if(dbConApp == null)
			{
				Logger.LogTraceError("TIESUtility","GetStatusCodeByExceptCode","ERR001","dbConApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			StringBuilder strQry = new StringBuilder();
			
				strQry.Append(" Select * from Exception_Code where applicationid = '");
				strQry.Append(a_strAppID);
				strQry.Append("' and enterpriseid = '");
				strQry.Append(a_strEnterpriseID);
				strQry.Append("' and active_st = 'Y ");	//Jeab 22 Mar 2012
			if(strExceptionCode != "")  //Jeab 22 Mar 2012
			{
				strQry.Append("' and exception_code = '");
				strQry.Append(strExceptionCode+"'");
			}
			else
			{
				strQry.Append("' and exception_code = '");
				strQry.Append("'");
			}
			
			dbCmd = dbConApp.CreateCommand(strQry.ToString(),CommandType.Text);
			try
			{
				dsStatusCode =(DataSet)dbConApp.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
			}
			catch(ApplicationException appExpection)
			{
				Logger.LogTraceError("TIESUtility","GetStatusCodeByExceptCode","ERR002","Error in the query String");
				throw appExpection;
			}
			
			return dsStatusCode;
		}
		
		//Added By GwanG for PickupVAS
		public static ServiceData IsServicePUPAvail(String a_strAppID,String a_strEnterpriseID, String a_strWeekDay,String a_strDayType)
		{
			DbConnection dbConApp = null;
			IDbCommand dbCmd = null;
			DataSet dsService = null;
			ServiceData serviceData = new ServiceData();

			dbConApp = DbConnectionManager.GetInstance().GetDbConnection(a_strAppID,a_strEnterpriseID);

			if(dbConApp == null)
			{
				Logger.LogTraceError("TIESUtility","IsServiceAvailable","ERR001","dbConApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			StringBuilder strQry = new StringBuilder();
			strQry.Append("select * from enterprise where applicationid = '");
			strQry.Append(a_strAppID);
			strQry.Append("' and enterpriseid = '");
			strQry.Append(a_strEnterpriseID+"'");

			
			if(a_strDayType.Equals("W"))
			{
				if(a_strWeekDay.Equals("Saturday"))
				{
					strQry.Append(" and sat_pickup_avail = 'Y'");
				}
				else if(a_strWeekDay.Equals("Sunday"))
				{
					strQry.Append(" and sun_pickup_avail = 'Y'");
				}
			}
			else if(a_strDayType.Equals("H"))
			{
				strQry.Append(" and pub_pickup_avail = 'Y'");
			}
			
			dbCmd = dbConApp.CreateCommand(strQry.ToString(),CommandType.Text);
			try
			{
				dsService =(DataSet)dbConApp.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
			}
			catch(ApplicationException appExpection)
			{
				Logger.LogTraceError("TIESUtility","IsServiceAvailable","ERR002","Error in the query String "+appExpection.Message);
				throw appExpection;
			}
			if(dsService.Tables[0].Rows.Count > 0)
			{
				DataRow drEach = dsService.Tables[0].Rows[0];

				String strDlvry = "N";
				String strVASCode = null;

				if(a_strDayType.Equals("W"))
				{
					if(a_strWeekDay.Equals("Saturday"))
					{
						if(!drEach["sat_pickup_avail"].GetType().Equals(System.Type.GetType("System.DBNull")))
						{
							strDlvry = (String)drEach["sat_pickup_avail"];
							strVASCode = (String)drEach["sat_pickup_vas_code"];
						}
					}
					else if(a_strWeekDay.Equals("Sunday"))
					{
						if(!drEach["sun_pickup_avail"].GetType().Equals(System.Type.GetType("System.DBNull")))
						{
							strDlvry = (String)drEach["sun_pickup_avail"];
							strVASCode = (String)drEach["sun_pickup_vas_code"];
						}
					}
				}
				else if (a_strDayType.Equals("H"))
				{
					if(!drEach["pub_pickup_avail"].GetType().Equals(System.Type.GetType("System.DBNull")))
					{
						strDlvry = (String)drEach["pub_pickup_avail"];
						strVASCode = (String)drEach["pub_pickup_vas_code"];
					}

				}
				
				if(strDlvry.Equals("Y"))
				{
					serviceData.isServiceAvail = true;
					serviceData.strVASCode = strVASCode;
				}
				
			}
			return serviceData;
		}
	}

	

}
