using System;
using System.Data;
using System.Text;
using com.common.DAL;
using com.common.classes;
using com.common.util;

namespace com.ties.classes
{
	/// <summary>
	/// Summary description for Zipcode.
	/// </summary>
	public class Zipcode
	{
		public Zipcode()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		
		private String m_strAppID = null;
		private String m_strEnterpriseID = null;
		private String m_strCountry = null;
		private String m_strStateCode = null;
		private String m_strZoneCode = null;
		private String m_strZipCode = null;
		private DateTime m_dtCuttOff ;
		private String m_strPickUpRoute = null;
		private String m_strArea = null;
		private String m_strRegion = null;
		private String m_strStateName = null;
		private String m_strDeliveryRouteCode = null;
		private decimal m_decESASurcharge;
		
		/// <summary>
		/// This method populates values from zipcode base table.
		/// Modified by Sompote 2010-03-29 
		/// </summary>
		/// <param name="appID"></param>
		/// <param name="enterpriseID"></param>
		/// <param name="zipCode"></param>
		public void Populate(String appID,String enterpriseID,String zipCode)
		{
			DbConnection dbCon = null;
			DataSet dsLocation = null;
			IDbCommand dbCmd = null;

			dbCon = DbConnectionManager.GetInstance().GetDbConnection(appID,enterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("ZipCode.cs","PopulateValues","ERR001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}		
			Populate( appID, enterpriseID, zipCode, dbCon);
		}
		/// <summary>
		/// Add by Sompote 2010-03-29
		/// </summary>
		/// <param name="appID"></param>
		/// <param name="enterpriseID"></param>
		/// <param name="zipCode"></param>
		/// <param name="dbCon"></param>
		public void Populate(String appID,String enterpriseID,String zipCode,DbConnection dbCon)
		{	
			IDbConnection conApp = dbCon.GetConnection();
			if(conApp == null)
			{
				Logger.LogTraceError("ZipCode.cs","PopulateValues","ERR003","conApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			try
			{
				dbCon.OpenConnection(ref conApp);
			}
			catch(ApplicationException appException)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("ZipCode.cs","PopulateValues","ERR004","Error opening connection : "+ appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ",appException);
			}
			catch(Exception exception)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("ZipCode.cs","PopulateValues","ERR005","Error opening connection : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}
			
			IDbCommand dbCmd = null;		
			dbCmd = dbCon.CreateCommand("",CommandType.Text);
			dbCmd.Connection = conApp;	

			//Populate( appID, enterpriseID, zipCode, dbCon, dbCmd);
		
			SessionDS sessionDS ;	
			try
			{
				Populate( appID, enterpriseID, zipCode, dbCon, dbCmd);
			}
			catch(ApplicationException appExpection)
			{
				throw appExpection;
			}
			finally
			{
				if(conApp != null)
				{
					conApp.Close();
				}
			}

		}

		/// <summary>
		/// Add by Sompote 2010-03-29
		/// </summary>
		/// <param name="appID"></param>
		/// <param name="enterpriseID"></param>
		/// <param name="zipCode"></param>
		/// <param name="dbCon"></param>
		/// <param name="dbCmd"></param>
		public void Populate(String appID,String enterpriseID,String zipCode,DbConnection dbCon,IDbCommand dbCmd )
		{			
		
			DataSet dsLocation = null;

			StringBuilder strQry = new StringBuilder();
			strQry = strQry.Append("select * ");
			strQry.Append(" from zipcode  ");
			strQry.Append(" where applicationid = '");
			strQry.Append(appID);
			strQry.Append("' and enterpriseid = '");
			strQry.Append(enterpriseID);
			strQry.Append("' and zipcode = '");
			strQry.Append(Utility.ReplaceSingleQuote(zipCode));
			strQry.Append("'");

			dbCmd.CommandText = strQry.ToString();

			try
			{
				dsLocation =(DataSet)dbCon.ExecuteQueryInTransaction(dbCmd,com.common.DAL.ReturnType.DataSetType);
			}
			catch(ApplicationException appExpection)
			{
				Logger.LogTraceError("Zipcode.cs","PopulateValues","ERR002","Error in PopulateValues Execute Query ");
				throw appExpection;
			}
			
			if(dsLocation.Tables[0].Rows.Count >0)
			{
				DataRow drEach = dsLocation.Tables[0].Rows[0];
				m_strZoneCode = (String)drEach["zone_code"];
				
				if(!drEach["zone_code"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					m_strZoneCode = (String)drEach["zone_code"];
				}
				if(!drEach["zipcode"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					m_strZipCode = (String)drEach["zipcode"];
				}
				if(!drEach["delivery_route"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					m_strDeliveryRouteCode = (String)drEach["delivery_route"];
				}
				if(!drEach["cutoff_time"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					m_dtCuttOff = (DateTime)drEach["cutoff_time"];
				}
				if(!drEach["esa_surcharge"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					m_decESASurcharge = (decimal)drEach["esa_surcharge"];
				}
				if(!drEach["country"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					m_strCountry = (String)drEach["country"];
				}
				if(!drEach["state_code"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					m_strStateCode = (String)drEach["state_code"];
				}
				if(!drEach["pickup_route"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					m_strPickUpRoute = (String)drEach["pickup_route"];
				}
			}
		}

		/// <summary>
		/// This method populates values from zipcode & state base tables.
		/// </summary>
		/// <param name="appID">Application ID</param>
		/// <param name="enterpriseID">Enterprise ID</param>
		/// <param name="countryName">Country Name</param>
		/// <param name="zipCode">Zip Code</param>
		public void Populate(String appID,String enterpriseID,String countryName,String zipCode)
		{
			DbConnection dbCon = null;
			DataSet dsLocation = null;
			IDbCommand dbCmd = null;

			dbCon = DbConnectionManager.GetInstance().GetDbConnection(appID,enterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("ZipCode.cs","PopulateValues","Zip001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			
			StringBuilder strQry = new StringBuilder();
			strQry = strQry.Append("select a.applicationid,a.enterpriseid,a.country,a.state_code,a.zone_code,a.zipcode,");
			strQry.Append(" a.cutoff_time,a.delivery_route,a.pickup_route,a.area,a.region,a.esa_surcharge,b.state_name from zipcode a,state b ");
			strQry.Append(" where a.applicationid = '");
			strQry.Append(appID);
			strQry.Append("' and a.enterpriseid = '");
			strQry.Append(enterpriseID);
			strQry.Append("' ");
			if(countryName != null && countryName.Trim().Length>0)
			{
				strQry.Append(" and a.country = '");
				strQry.Append(Utility.ReplaceSingleQuote(countryName));
				strQry.Append("' ");
			}
			strQry.Append(" and a.zipcode = '");
			strQry.Append(Utility.ReplaceSingleQuote(zipCode));
			strQry.Append("'");
			strQry.Append(" and b.applicationid = a.applicationid ");
			strQry.Append(" and b.enterpriseid = a.enterpriseid ");
			strQry.Append(" and b.country = a.country ");
			strQry.Append(" and b.state_code = a.state_code ");
			
			dbCmd = dbCon.CreateCommand(strQry.ToString(),CommandType.Text);

			try
			{
				dsLocation =(DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
			}
			catch(ApplicationException appExpection)
			{
				Logger.LogTraceError("Zipcode.cs","PopulateValues","ZE0001","Error in PopulateValues Execute Query ");
				throw appExpection;
			}
			
			if(dsLocation.Tables[0].Rows.Count >0)
			{
				DataRow drEach = dsLocation.Tables[0].Rows[0];
				if(!drEach["state_name"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					m_strStateName = (String)drEach["state_name"];
				}
				if(!drEach["Country"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					m_strCountry=(String)drEach["Country"];
				}
				if(!drEach["cutoff_time"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					m_dtCuttOff = (DateTime)drEach["cutoff_time"];
				}
				if(!drEach["delivery_route"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					m_strDeliveryRouteCode = (String)drEach["delivery_route"];
				}
				if(!drEach["esa_surcharge"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					m_decESASurcharge = (decimal)drEach["esa_surcharge"];
				}
				if(!drEach["pickup_route"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					m_strPickUpRoute = (String)drEach["pickup_route"];
				}
			}
		}

		public  DataSet ckZipcodeInSystem(String appID,String enterpriseID,String zipCode)
		{
			DbConnection dbCon = null;
			DataSet dsZipcode = new DataSet();
			IDbCommand dbCmd = null;

			dbCon = DbConnectionManager.GetInstance().GetDbConnection(appID,enterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("ZipCode.cs","PopulateValues","Zip001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			
			StringBuilder strQry = new StringBuilder();
			strQry = strQry.Append("select * from zipcode ");
			strQry.Append(" where applicationid = '");
			strQry.Append(appID);
			strQry.Append("' and enterpriseid = '");
			strQry.Append(enterpriseID);
			strQry.Append("' and zipcode = '");
			strQry.Append(Utility.ReplaceSingleQuote(zipCode));
			strQry.Append("'");
			
			dbCmd = dbCon.CreateCommand(strQry.ToString(),CommandType.Text);

			try
			{
				dsZipcode =(DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
			}
			catch(ApplicationException appExpection)
			{
				Logger.LogTraceError("Zipcode.cs","PopulateValues","ZE0001","Error in PopulateValues Execute Query ");
				throw appExpection;
			}

			return dsZipcode;
		}
		

		/// <summary>
		/// This method gets and sets the value of the Country.
		/// </summary>
		public String Country
		{
			set
			{
				m_strCountry = value;
			}
			get
			{
				return m_strCountry;
			}
		}

		/// <summary>
		/// This method gets & sets the value of the state code.
		/// </summary>
		public String StateCode
		{
			set
			{
				m_strStateCode = value;
			}
			get
			{
				return m_strStateCode;
			}
		}

		/// <summary>
		/// This method gets and sets the value of the Zone Code.
		/// </summary>
		public String ZoneCode
		{
			set
			{
				m_strZoneCode = value;
			}
			get
			{
				return m_strZoneCode;
			}
		}

		/// <summary>
		/// This method gets and sets the value of the zip code.
		/// </summary>
		public String ZipCode
		{
			set
			{
				m_strZipCode = value;
			}
			get
			{
				return m_strZipCode;
			}
		}

		/// <summary>
		/// This method gets and sets the value of cutt off time
		/// </summary>
		public DateTime CuttOffTime
		{
			set
			{
				m_dtCuttOff = value;
			}
			get
			{
				return m_dtCuttOff;
			}
		}
		
		/// <summary>
		/// This method gets & sets the value of the Pick Up Route.
		/// </summary>
		public String PickUpRoute
		{
			set
			{
				m_strPickUpRoute = value;
			}
			get
			{
				return m_strPickUpRoute;
			}
		}

		/// <summary>
		/// The method gets and sets the value of the Area.
		/// </summary>
		public String Area
		{
			set
			{
				m_strArea = value;
			}
			get
			{
				return m_strArea;
			}
		}

		/// <summary>
		/// This method gets and sets the value of the region.
		/// </summary>
		public String Region
		{
			set
			{
				m_strRegion = value;
			}
			get
			{
				return m_strRegion;
			}
		}

		/// <summary>
		/// This method gets and sets the value of the state name.
		/// </summary>
		public String StateName
		{
			set
			{
				m_strStateName = value;
			}
			get
			{
				return m_strStateName;
			}
		}

		/// <summary>
		/// This method gets and sets the value of the Delivery Route.
		/// </summary>
		public String DeliveryRoute
		{
			set
			{
				m_strDeliveryRouteCode = value;
			}
			get
			{
				return m_strDeliveryRouteCode;
			}
		}

		/// <summary>
		/// This method gets and sets the value of the Extented Service Area Surcharge.
		/// </summary>
		public decimal EASSurcharge
		{
			set
			{
				m_decESASurcharge =  value;
			}
			get
			{
				return m_decESASurcharge;
			}
		}
	}
	}

