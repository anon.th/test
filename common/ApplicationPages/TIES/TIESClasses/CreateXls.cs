using System;
using System.Data;
using Excel;
using System.Runtime.InteropServices;
using System.Net;
using com.common.applicationpages;
using System.Web;
using System.IO;
using System.Web.UI;
using System.Threading;
using System.Data;

namespace TIESClasses
{
	/// <summary>
	/// Summary description for CreateXls.
	/// </summary>
	public class CreateXls : BasePage
	{
		public string createXls(DataSet ds,  string filePath, string[] columnsType)
		{
			System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
			int RowCountDS = ds.Tables[0].Rows.Count;
			int ColumnCountDS = ds.Tables[0].Columns.Count;

			try
			{
				ApplicationClass xlsApp = new ApplicationClass();
				Range xlsRange = null;
				object oMissing = System.Reflection.Missing.Value;

				xlsApp.Visible = false;

				Workbook xlsWorkBook = xlsApp.Workbooks.Add(oMissing);
				Worksheet xlsWorkSheet = (Worksheet)xlsWorkBook.ActiveSheet;
				xlsRange = xlsWorkSheet.UsedRange;
				
				int startRowsXls = 1;
				int startColumnXls = 1;

				if (ds.Tables[0].Rows.Count > 0)
				{
					for (int row = 0; row < RowCountDS+startRowsXls; row++)
					{
						for (int col = 0; col < ColumnCountDS; col++)
						{
							//Header
							if (row == 0)
							{
								Range xlsCell = (Range)xlsRange.Cells[row+startRowsXls, col+startColumnXls];
								xlsCell.NumberFormat = TypeText;

								xlsWorkSheet.Cells[row+startRowsXls, col+startColumnXls] = ds.Tables[0].Columns[col].ColumnName.ToString().Replace("_"," ").ToUpper();
							}
							else
							{
								int newRowsDetails = startRowsXls;
								//Details
								Range xlsCell = (Range)xlsRange.Cells[row+startRowsXls, col+startColumnXls];
                           
								if (columnsType[col] != null)
									xlsCell.NumberFormat = columnsType[col ];    //use index
								else
									xlsCell.NumberFormat = TypeText;

								xlsWorkSheet.Cells[row+startRowsXls, col+startColumnXls] = ds.Tables[0].Rows[row-1][col].ToString();
							}
						}
					}
				}

				if (System.IO.File.Exists(filePath))
				{
					System.IO.File.Delete(filePath);
				}

				xlsWorkBook.SaveAs(filePath,oMissing,oMissing,oMissing,oMissing,oMissing,XlSaveAsAccessMode.xlNoChange,oMissing,oMissing,oMissing,oMissing,oMissing);
				xlsWorkBook.Close(false, oMissing,oMissing);
				xlsApp.Quit();
			
				return "0";
			}
			catch (Exception ex)
			{
				return ex.Message.ToString();
			}
		}

		public void  KillProcesses(string processesName)
		{
			if (processesName != "")
			{
				System.Diagnostics.Process[] proCesses = System.Diagnostics.Process.GetProcessesByName(processesName);

				foreach (System.Diagnostics.Process eachProcess in proCesses)
				{
					try
					{
						if (eachProcess.MainWindowTitle.Length == 0)
							eachProcess.Kill();
					}
					catch {}
				}
			}
		}

		private string _typeText = "@";
		private string _typeDate = "dd/MM/yyyy HH:mm";
		private string _typeDecimal = "#,##0.00";

		private Range rgFound;
		private Application xlApp;
		private Workbook wb;
		private Worksheet ws;

		public string TypeText
		{
			get
			{
				return _typeText;
			}
			set
			{
				_typeText = value;
			}
		}

		public string TypeDate
		{
			get
			{
				return _typeDate;
			}
			set
			{
				_typeDate = value;
			}
		}

		public string TypeDecimal
		{
			get
			{
				return _typeDecimal;
			}
			set
			{
				_typeDecimal = value;
			}
		}


		public string createXlsByGwang(System.Data.DataTable dt,  string strFilePath, string strInvoiceNumber, string[] columnsType, System.Web.UI.Page pPage)
		{
			System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");

			ApplicationClass xlsApp = null;
			Workbook xlsWorkBook = null;
			Worksheet xlsWorkSheet = null;
			Range xlsRange = null;
			Range xlsCell = null;

			try
			{
				int recCount = 0;
				int fileCount = 1;

				object oMissing = System.Reflection.Missing.Value;

				int RowCountDS = dt.Rows.Count;
				int ColumnCountDS = dt.Columns.Count;

				xlsApp = new ApplicationClass();
				xlsApp.Visible = false;

				for (int row = 0; row < RowCountDS; row++)
				{
					recCount = recCount + 1;

					if (recCount == 1)
					{
						xlsWorkBook = xlsApp.Workbooks.Add(oMissing);	

						xlsWorkSheet = (Worksheet)xlsWorkBook.Worksheets[1];
						xlsWorkSheet.Name = strInvoiceNumber + "_" + Convert.ToString(fileCount);
						xlsWorkSheet.Activate();
					}

					if (row == 0 && recCount == 1)
					{
						//Header Kerry
						xlsWorkSheet.get_Range("L1","S1").Font.Bold = true;
						xlsWorkSheet.get_Range("L1","S1").Font.Name = "Tahoma";
						xlsWorkSheet.get_Range("L1","S1").Font.Size = "14";
						xlsWorkSheet.get_Range("L1","S1").MergeCells = true;
						xlsWorkSheet.get_Range("L1","S1").Value2 = "Kerry Distribution (Thailand) Limited";
						xlsWorkSheet.get_Range("L1","S1").HorizontalAlignment = Excel.Constants.xlCenter;

						//Header Kerry's Address
						xlsWorkSheet.get_Range("K2","T2").Font.Name = "Tahoma";
						xlsWorkSheet.get_Range("K2","T2").Font.Size = "10";
						xlsWorkSheet.get_Range("K2","T2").MergeCells = true;
						xlsWorkSheet.get_Range("K2","T2").Value2 = "803 8/F Chao Phya Tower 89 Soi Wat Suan Plu, New Road, Bangrak, THAILAND 10500";
						xlsWorkSheet.get_Range("K2","T2").HorizontalAlignment = Excel.Constants.xlCenter;
					}

					if (recCount == 1)
					{
						xlsRange = xlsWorkSheet.UsedRange;

						xlsWorkSheet.get_Range("A3","T5").Font.Name = "Tahoma";
						xlsWorkSheet.get_Range("A3","T5").Font.Size = "10";

						//Header Left Header
						xlsWorkSheet.get_Range("A3","D3").MergeCells = true;
						xlsWorkSheet.get_Range("A3","D3").Value2  = "Invoice Number:";
						xlsWorkSheet.get_Range("A3","D3").HorizontalAlignment = Excel.Constants.xlRight;
						
						xlsWorkSheet.get_Range("A4","D4").MergeCells = true;
						xlsWorkSheet.get_Range("A4","D4").Value2  = "Customer-ID:";
						xlsWorkSheet.get_Range("A4","D4").HorizontalAlignment = Excel.Constants.xlRight;
						
						xlsWorkSheet.get_Range("A5","D5").MergeCells = true;
						xlsWorkSheet.get_Range("A5","D5").Value2  = "Printed by:";
						xlsWorkSheet.get_Range("A5","D5").HorizontalAlignment = Excel.Constants.xlRight;


						//Content Left Header
						xlsWorkSheet.get_Range("E3","G3").MergeCells = true;
						xlsWorkSheet.get_Range("E3","G3").Value2  = "";
						xlsWorkSheet.get_Range("E3","G3").HorizontalAlignment = Excel.Constants.xlLeft;

						
						xlsWorkSheet.get_Range("E4","G4").Value2  = "";
						xlsWorkSheet.get_Range("E4","G4").HorizontalAlignment = Excel.Constants.xlLeft;
						xlsWorkSheet.get_Range("E4","G4").MergeCells = true;

						xlsWorkSheet.get_Range("E5","G5").MergeCells = true;
						xlsWorkSheet.get_Range("E5","G5").Value2  = "";
						xlsWorkSheet.get_Range("E5","G5").HorizontalAlignment = Excel.Constants.xlLeft;


						//Header Right Header
						xlsWorkSheet.get_Range("H4","I4").MergeCells = true;
						xlsWorkSheet.get_Range("H4","I4").Value2  = "Customer Name:";
						xlsWorkSheet.get_Range("H4","I4").HorizontalAlignment = Excel.Constants.xlRight;

						xlsWorkSheet.get_Range("H5","I5").MergeCells = true;
						xlsWorkSheet.get_Range("H5","I5").Value2 = "On:";
						xlsWorkSheet.get_Range("H5","I5").HorizontalAlignment = Excel.Constants.xlRight;


						//Content Right Header
						xlsWorkSheet.get_Range("J4","T4").MergeCells = true;
						xlsWorkSheet.get_Range("J4","T4").Value2  = "";
						xlsWorkSheet.get_Range("J4","T4").HorizontalAlignment = Excel.Constants.xlLeft;

						xlsWorkSheet.get_Range("J5","T5").MergeCells = true;
						xlsWorkSheet.get_Range("J5","T5").Value2  = "";
						xlsWorkSheet.get_Range("J5","T5").HorizontalAlignment = Excel.Constants.xlLeft;
					}

					//#################### Set Excel Columns ####################

					//Construct Column(2) Consignment# / Cust. Ref.#
					
					xlsCell = (Range)xlsRange.Cells[6, 2];
					xlsCell.NumberFormat = TypeText;
					xlsWorkSheet.Cells[6, 2] = "Consignment#";

					xlsCell = (Range)xlsRange.Cells[7, 2];
					xlsCell.NumberFormat = TypeText;
					xlsWorkSheet.Cells[7, 2] = "Cust. Ref.#";
					//Construct Column(2) Consignment# / Cust. Ref.#

					//Construct Column(3) Pickup Date
					xlsCell = (Range)xlsRange.Cells[6, 3];
					xlsCell.NumberFormat = TypeText;
					xlsWorkSheet.Cells[6, 3] = "Pickup Date";
					//Construct Column(3) Pickup Date

					//Construct Column(4) Delivery Date
					xlsCell = (Range)xlsRange.Cells[6, 4];
					xlsCell.NumberFormat = TypeText;
					xlsWorkSheet.Cells[6, 4] = "Delivery Date";
					//Construct Column(4) Delivery Date

					//Construct Column(5) Recipient Name
					xlsCell = (Range)xlsRange.Cells[6, 5];
					xlsCell.NumberFormat = TypeText;
					xlsWorkSheet.Cells[6, 5] = "Recipient Name";
					//Construct Column(5) Recipient Name

					//Construct Column(6) Status
					xlsCell = (Range)xlsRange.Cells[6, 6];
					xlsCell.NumberFormat = TypeText;
					xlsWorkSheet.Cells[6, 6] = "Status";
					//Construct Column(6) Status

					//Construct Column(7) WEIGHT / Act. / Dim. /Chg.
					xlsCell = (Range)xlsRange.Cells[6, 7];
					xlsCell.NumberFormat = TypeText;
					xlsWorkSheet.Cells[6, 7] = "WEIGHT";

					xlsCell = (Range)xlsRange.Cells[7, 7];
					xlsCell.NumberFormat = TypeText;
					xlsWorkSheet.Cells[7, 7] = "Act.";
					//Construct Column(7) WEIGHT / Act. / Dim. /Chg.

					//#################### Set Excel Columns ####################
					
					int startRowsXls = 7;
					int startColumnXls = 1;

					for (int col = 0; col < ColumnCountDS; col++)
					{
						int newRowsDetails = startRowsXls;
	
						xlsCell = (Range)xlsRange.Cells[row+startRowsXls, col+startColumnXls];
												                           
						if (columnsType[col] != null)
							xlsCell.NumberFormat = columnsType[col ];    
						else
							xlsCell.NumberFormat = TypeText;

						if(dt.Rows.Count > 0)
						{
							xlsWorkSheet.Cells[row+startRowsXls, col+startColumnXls] = dt.Rows[row][col].ToString();
						}
					}

					if(recCount == 50)
					{
						string strActualFile = strFilePath + strInvoiceNumber + "_" + Convert.ToString(fileCount) + ".xls";
						
						if (System.IO.File.Exists(strActualFile))
						{
							System.IO.File.Delete(strActualFile);
						}

						xlsWorkBook.SaveAs(strActualFile,oMissing,oMissing,oMissing,oMissing,oMissing,XlSaveAsAccessMode.xlNoChange,oMissing,oMissing,oMissing,oMissing,oMissing);
						xlsWorkBook.Close(false, oMissing,oMissing);
						

						GetDownloadFile(strActualFile, strInvoiceNumber + "_" + Convert.ToString(fileCount) + ".xls", pPage);

						recCount = 0;
						fileCount = fileCount + 1;
					}
				}
			}
			catch (Exception ex)
			{
				return ex.Message.ToString();
			}
			finally
			{
				xlsApp.Quit();
			}
						
			return "0";
		}

		private void GetDownloadFile(string strActualFile, string strFileName, System.Web.UI.Page pPage)
		{
			pPage.Response.ContentType="application/ms-excel";
			pPage.Response.AddHeader("content-disposition","attachment; filename="+strFileName);
			FileStream fs = new FileStream(strActualFile, FileMode.Open);
			long FileSize;
			FileSize = fs.Length;
			byte[] getContent = new byte[(int)FileSize];
			fs.Read(getContent, 0, (int)fs.Length);
			fs.Close();

			pPage.Response.BinaryWrite(getContent);
		}

		#region Create Excel file for Export ConsignmentNote
			public string createXlsConsignment(DataSet dsCon, DataSet dsPackage, string filePath, string[] columnsType)
			{
				System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
				int RowCountDScon = dsCon.Tables[0].Rows.Count;
				int ColumnCountDScon = dsCon.Tables[0].Columns.Count;
				int RowCountDSpackage = dsPackage.Tables[0].Rows.Count;
				int ColumnCountDSpackage = dsPackage.Tables[0].Columns.Count;
			
				try
				{
					ApplicationClass xlsApp = new ApplicationClass();
					Range xlsRange = null;
					object oMissing = System.Reflection.Missing.Value;

					xlsApp.Visible = false;

					Workbook xlsWorkBook = xlsApp.Workbooks.Add(oMissing);
					//Worksheet xlsWorkSheet = (Worksheet)xlsWorkBook.ActiveSheet;
					Worksheet xlsWorkSheet1 = (Worksheet)xlsWorkBook.Sheets[1];
					xlsWorkSheet1.Name = "Consignments";
					xlsRange = xlsWorkSheet1.UsedRange;
				
					int startRowsXls1 = 1;
					int startColumnXls1 = 1;

					if (dsCon.Tables[0].Rows.Count > 0)
					{
						for (int row1 = 0; row1 < RowCountDScon+startRowsXls1; row1++)
						{
							for (int col1 = 0; col1 < ColumnCountDScon; col1++)
							{
								//Header
								if (row1 == 0)
								{
									Range xlsCell1 = (Range)xlsRange.Cells[row1+startRowsXls1, col1+startColumnXls1];
									xlsCell1.NumberFormat = TypeText;

									xlsWorkSheet1.Cells[row1+startRowsXls1, col1+startColumnXls1] = dsCon.Tables[0].Columns[col1].ColumnName.ToString();
								}
								else
								{
									int newRowsDetails1 = startRowsXls1;
									//Details
									Range xlsCell1 = (Range)xlsRange.Cells[row1+startRowsXls1, col1+startColumnXls1];
                           
									if (columnsType[col1] != null)
										xlsCell1.NumberFormat = columnsType[col1];    //use index
									else
										xlsCell1.NumberFormat = TypeText;

									xlsWorkSheet1.Cells[row1+startRowsXls1, col1+startColumnXls1] = dsCon.Tables[0].Rows[row1-1][col1].ToString();
								}
							}
						}
					}



					//Sheet Package
					Worksheet xlsWorkSheet2 = (Worksheet)xlsWorkBook.Sheets[2];
					xlsWorkSheet2.Name = "Packages";
					xlsRange = xlsWorkSheet2.UsedRange;

					int startRowsXls2 = 1;
					int startColumnXls2 = 1;

					if (dsPackage.Tables[0].Rows.Count > 0)
					{
						for (int row2 = 0; row2 < RowCountDSpackage+startRowsXls2; row2++)
						{
							for (int col2 = 0; col2 < ColumnCountDSpackage; col2++)
							{
								//Header
								if (row2 == 0)
								{
									Range xlsCell2 = (Range)xlsRange.Cells[row2+startRowsXls2, col2+startColumnXls2];
									xlsCell2.NumberFormat = TypeText;

									xlsWorkSheet2.Cells[row2+startRowsXls2, col2+startColumnXls2] = dsPackage.Tables[0].Columns[col2].ColumnName.ToString();
								}
								else
								{
									int newRowsDetails2 = startRowsXls2;
									//Details
									Range xlsCell2 = (Range)xlsRange.Cells[row2+startRowsXls2, col2+startColumnXls2];
                           
									if (columnsType[col2] != null)
										xlsCell2.NumberFormat = columnsType[col2];    //use index
									else
										xlsCell2.NumberFormat = TypeText;

									xlsWorkSheet2.Cells[row2+startRowsXls2, col2+startColumnXls2] = dsPackage.Tables[0].Rows[row2-1][col2].ToString();
								}
							}
						}
					}





					if (System.IO.File.Exists(filePath))
					{
						System.IO.File.Delete(filePath);
					}

					xlsWorkBook.SaveAs(filePath,oMissing,oMissing,oMissing,oMissing,oMissing,XlSaveAsAccessMode.xlNoChange,oMissing,oMissing,oMissing,oMissing,oMissing);
					xlsWorkBook.Close(false, oMissing,oMissing);
					xlsApp.Quit();

					return "0";
				}
				catch (Exception ex)
				{
					return ex.Message.ToString();
				}
			}
		#endregion

		public string createXlsConsign(System.Data.DataTable dtCon, string filePath, string[] columnsType)
		{
			System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
			int RowCountDScon = dtCon.Rows.Count;
			int ColumnCountDScon = dtCon.Columns.Count;
			
			try
			{
				ApplicationClass xlsApp = new ApplicationClass();
				Range xlsRange = null;
				object oMissing = System.Reflection.Missing.Value;

				xlsApp.Visible = false;

				Workbook xlsWorkBook = xlsApp.Workbooks.Add(oMissing);
				Worksheet xlsWorkSheet1 = (Worksheet)xlsWorkBook.Sheets[1];
				xlsWorkSheet1.Name = "Sheet1";
				xlsRange = xlsWorkSheet1.UsedRange;
				
				int startRowsXls1 = 1;
				int startColumnXls1 = 1;

				if (dtCon.Rows.Count > 0)
				{
					for (int row1 = 0; row1 < RowCountDScon+startRowsXls1; row1++)
					{
						for (int col1 = 0; col1 < ColumnCountDScon; col1++)
						{
							//Header
							if (row1 == 0)
							{
								Range xlsCell1 = (Range)xlsRange.Cells[row1+startRowsXls1, col1+startColumnXls1];
								xlsCell1.NumberFormat = TypeText;

								xlsWorkSheet1.Cells[row1+startRowsXls1, col1+startColumnXls1] = dtCon.Columns[col1].ColumnName.ToString();
							}
							else
							{
								int newRowsDetails1 = startRowsXls1;
								//Details
								Range xlsCell1 = (Range)xlsRange.Cells[row1+startRowsXls1, col1+startColumnXls1];
                           
								if (columnsType[col1] != null)
									xlsCell1.NumberFormat = columnsType[col1];    //use index
								else
									xlsCell1.NumberFormat = TypeText;

								xlsWorkSheet1.Cells[row1+startRowsXls1, col1+startColumnXls1] = dtCon.Rows[row1-1][col1].ToString();
							}
						}
					}
				}

				if (System.IO.File.Exists(filePath))
				{
					System.IO.File.Delete(filePath);
				}

				xlsWorkBook.SaveAs(filePath,oMissing,oMissing,oMissing,oMissing,oMissing,XlSaveAsAccessMode.xlNoChange,oMissing,oMissing,oMissing,oMissing,oMissing);
				xlsWorkBook.Close(false, oMissing,oMissing);
				xlsApp.Quit();

				return "0";
			}
			catch (Exception ex)
			{
				return ex.Message.ToString();
			}
		}


		public string createXlsOverride(System.Data.DataTable dtInvalidImport, string filePath)
		{
			System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
			int RowCountDScon = dtInvalidImport.Rows.Count;
			
			try
			{
				ApplicationClass xlsApp = new ApplicationClass();
				Range xlsRange = null;
				object oMissing = System.Reflection.Missing.Value;

				xlsApp.Visible = false;

				Workbook xlsWorkBook = xlsApp.Workbooks.Add(oMissing);
				Worksheet xlsWorkSheet1 = (Worksheet)xlsWorkBook.Sheets[1];
				xlsWorkSheet1.Name = "OverrideRate";
				xlsRange = xlsWorkSheet1.UsedRange;
				
				int startRowsXls1 = 1;
	
				if (dtInvalidImport.Rows.Count > 0)
				{
					for (int row1 = 0; row1 < RowCountDScon+startRowsXls1; row1++)
					{
						if (row1 == 0)
						{
							xlsWorkSheet1.Cells[row1+startRowsXls1, 1] = "consignment_no";
							xlsWorkSheet1.Cells[row1+startRowsXls1, 2] = "override_rated_freight";
							xlsWorkSheet1.Cells[row1+startRowsXls1, 3] = "override_rated_ins";
							xlsWorkSheet1.Cells[row1+startRowsXls1, 4] = "override_rated_other";
							xlsWorkSheet1.Cells[row1+startRowsXls1, 5] = "override_rated_esa";
						}
						else
						{
							xlsWorkSheet1.Cells[row1+startRowsXls1, 1] = dtInvalidImport.Rows[row1-1]["consignment_no"].ToString();
							xlsWorkSheet1.Cells[row1+startRowsXls1, 2] = dtInvalidImport.Rows[row1-1]["override_rated_freight"].ToString();
							xlsWorkSheet1.Cells[row1+startRowsXls1, 3] = dtInvalidImport.Rows[row1-1]["override_rated_ins"].ToString();
							xlsWorkSheet1.Cells[row1+startRowsXls1, 4] = dtInvalidImport.Rows[row1-1]["override_rated_other"].ToString();
							xlsWorkSheet1.Cells[row1+startRowsXls1, 5] = dtInvalidImport.Rows[row1-1]["override_rated_esa"].ToString();
						}
					}
				}

				if (System.IO.File.Exists(filePath))
				{
					System.IO.File.Delete(filePath);
				}

				xlsWorkBook.SaveAs(filePath,oMissing,oMissing,oMissing,oMissing,oMissing,XlSaveAsAccessMode.xlNoChange,oMissing,oMissing,oMissing,oMissing,oMissing);
				xlsWorkBook.Close(false, oMissing,oMissing);
				xlsApp.Quit();

				return "0";
			}
			catch (Exception ex)
			{
				return ex.Message.ToString();
			}
		}


		public string createXlsVariances(System.Data.DataTable dtVariances, string filePath)
		{
			System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
			int RowCountDScon = dtVariances.Rows.Count;
			
			try
			{
				ApplicationClass xlsApp = new ApplicationClass();
				Range xlsRange = null;
				object oMissing = System.Reflection.Missing.Value;

				xlsApp.Visible = false;

				Workbook xlsWorkBook = xlsApp.Workbooks.Add(oMissing);
				Worksheet xlsWorkSheet1 = (Worksheet)xlsWorkBook.Sheets[1];
				xlsWorkSheet1.Name = "Batch Variances";
				xlsRange = xlsWorkSheet1.UsedRange;
				
				int startRowsXls1 = 1;
	
				if (dtVariances.Rows.Count > 0)
				{
					for (int row1 = 0; row1 < RowCountDScon+startRowsXls1; row1++)
					{
						if (row1 == 0)
						{
							xlsWorkSheet1.Cells[row1+startRowsXls1, 1] = "Sonsignment Number";
							xlsWorkSheet1.Cells[row1+startRowsXls1, 2] = "Origin DC";
							xlsWorkSheet1.Cells[row1+startRowsXls1, 3] = "Destination DC";
							xlsWorkSheet1.Cells[row1+startRowsXls1, 4] = "#Pkgs";
							xlsWorkSheet1.Cells[row1+startRowsXls1, 5] = "Weight";
							xlsWorkSheet1.Cells[row1+startRowsXls1, 6] = "SOP Scanned #Pkgs";
							xlsWorkSheet1.Cells[row1+startRowsXls1, 7] = "SOP Scanned Var";
							xlsWorkSheet1.Cells[row1+startRowsXls1, 8] = "SIP Scanned #Pkgs";
							xlsWorkSheet1.Cells[row1+startRowsXls1, 9] = "SIP Scanned Var";
							xlsWorkSheet1.Cells[row1+startRowsXls1, 10] = "Status";
							xlsWorkSheet1.Cells[row1+startRowsXls1, 11] = "Last Status Code";
						}
						else
						{
							xlsWorkSheet1.Cells[row1+startRowsXls1, 1] = dtVariances.Rows[row1-1]["Consignment_No"].ToString();
							xlsWorkSheet1.Cells[row1+startRowsXls1, 2] = dtVariances.Rows[row1-1]["Origin_DC"].ToString();
							xlsWorkSheet1.Cells[row1+startRowsXls1, 3] = dtVariances.Rows[row1-1]["Dest_DC"].ToString();
							xlsWorkSheet1.Cells[row1+startRowsXls1, 4] = dtVariances.Rows[row1-1]["Cons_Tot_Pkgs"].ToString();
							xlsWorkSheet1.Cells[row1+startRowsXls1, 5] = dtVariances.Rows[row1-1]["Cons_Tot_Wt"].ToString();
							xlsWorkSheet1.Cells[row1+startRowsXls1, 6] = dtVariances.Rows[row1-1]["SOP_PKG"].ToString();
							xlsWorkSheet1.Cells[row1+startRowsXls1, 7] = dtVariances.Rows[row1-1]["SOP_VAR"].ToString();
							xlsWorkSheet1.Cells[row1+startRowsXls1, 8] = dtVariances.Rows[row1-1]["SIP_PKG"].ToString();
							xlsWorkSheet1.Cells[row1+startRowsXls1, 9] = dtVariances.Rows[row1-1]["SIP_Var"].ToString();
							xlsWorkSheet1.Cells[row1+startRowsXls1, 10] = dtVariances.Rows[row1-1]["xStatus"].ToString();
							xlsWorkSheet1.Cells[row1+startRowsXls1, 11] = dtVariances.Rows[row1-1]["Last_Status_code"].ToString();
						}
					}
				}

				if (System.IO.File.Exists(filePath))
				{
					System.IO.File.Delete(filePath);
				}

				xlsWorkBook.SaveAs(filePath,oMissing,oMissing,oMissing,oMissing,oMissing,XlSaveAsAccessMode.xlNoChange,oMissing,oMissing,oMissing,oMissing,oMissing);
				xlsWorkBook.Close(false, oMissing,oMissing);
				xlsApp.Quit();

				return "0";
			}
			catch (Exception ex)
			{
				return ex.Message.ToString();
			}
		}


		public string createXlsDailyLodgments(System.Data.DataTable dtLodgments, string filePath)
		{
			//System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
			int RowCountDScon = dtLodgments.Rows.Count;
			
			try
			{
				ApplicationClass xlsApp = new ApplicationClass();
				Range xlsRange = null;
				object oMissing = System.Reflection.Missing.Value;

				xlsApp.Visible = false;

				Workbook xlsWorkBook = xlsApp.Workbooks.Add(oMissing);
				Worksheet xlsWorkSheet1 = (Worksheet)xlsWorkBook.Sheets[1];
				xlsWorkSheet1.Name = "Daily Lodgments";
				xlsRange = xlsWorkSheet1.UsedRange;
				
				int startRowsXls1 = 1;
				if (dtLodgments.Rows.Count > 0)
				{
					for (int row1 = 0; row1 < RowCountDScon+startRowsXls1; row1++)
					{
						if (row1 == 0)
						{
							xlsWorkSheet1.Cells[row1+startRowsXls1, 1] = "Airline";
							xlsWorkSheet1.Cells[row1+startRowsXls1, 2] = "AWB";
							xlsWorkSheet1.Cells[row1+startRowsXls1, 3] = "Flight";
							xlsWorkSheet1.Cells[row1+startRowsXls1, 4] = "Date";
							xlsWorkSheet1.Cells[row1+startRowsXls1, 5] = "Origin";
							xlsWorkSheet1.Cells[row1+startRowsXls1, 6] = "Destination";
							xlsWorkSheet1.Cells[row1+startRowsXls1, 7] = "Pieces";
							xlsWorkSheet1.Cells[row1+startRowsXls1, 8] = "Weight";
							xlsWorkSheet1.Cells[row1+startRowsXls1, 9] = "Service";
							xlsWorkSheet1.Cells[row1+startRowsXls1, 10] = "BatchNumber";
						}
						else
						{
							if(dtLodgments.Rows[row1-1]["AWB_Number"] != DBNull.Value
								&& dtLodgments.Rows[row1-1]["AWB_Number"].ToString().Trim() != ""
								&& dtLodgments.Rows[row1-1]["AWB_Number"].ToString().Trim().Length>3)
							{
								xlsWorkSheet1.Cells[row1+startRowsXls1, 1] = dtLodgments.Rows[row1-1]["AWB_Number"].ToString().Substring(0,2);
								xlsWorkSheet1.Cells[row1+startRowsXls1, 2] = "'" + dtLodgments.Rows[row1-1]["AWB_Number"].ToString().Substring(2);
							}
							else
							{
								xlsWorkSheet1.Cells[row1+startRowsXls1, 1] = dtLodgments.Rows[row1-1]["AWB_Number"].ToString();
								xlsWorkSheet1.Cells[row1+startRowsXls1, 2] = "'" +dtLodgments.Rows[row1-1]["AWB_Number"].ToString();
							}
							string strBatchDate=DateTime.Parse(dtLodgments.Rows[row1-1]["Batch_Date"].ToString()).ToString("dd/MM/yyyy");

							xlsWorkSheet1.Cells[row1+startRowsXls1, 3] = dtLodgments.Rows[row1-1]["Flight_Number"].ToString();
							xlsWorkSheet1.Cells[row1+startRowsXls1, 4] = "'"+strBatchDate;
							xlsWorkSheet1.Cells[row1+startRowsXls1, 5] = dtLodgments.Rows[row1-1]["DepartureDC"].ToString();
							xlsWorkSheet1.Cells[row1+startRowsXls1, 6] = dtLodgments.Rows[row1-1]["ArrivalDC"].ToString();
							xlsWorkSheet1.Cells[row1+startRowsXls1, 7] = dtLodgments.Rows[row1-1]["Scanned_Pkg_Count"].ToString();
							xlsWorkSheet1.Cells[row1+startRowsXls1, 8] = dtLodgments.Rows[row1-1]["Cons_Tot_Wt"].ToString();
							xlsWorkSheet1.Cells[row1+startRowsXls1, 9] = dtLodgments.Rows[row1-1]["ServiceType"].ToString();
							xlsWorkSheet1.Cells[row1+startRowsXls1, 10] = dtLodgments.Rows[row1-1]["BatchNumber"].ToString();
						}
					}
				}

				if (System.IO.File.Exists(filePath))
				{
					System.IO.File.Delete(filePath);
				}

				xlsWorkBook.SaveAs(filePath,oMissing,oMissing,oMissing,oMissing,oMissing,XlSaveAsAccessMode.xlNoChange,oMissing,oMissing,oMissing,oMissing,oMissing);
				xlsWorkBook.Close(false, oMissing,oMissing);
				xlsApp.Quit();

				return "0";
			}
			catch (Exception ex)
			{
				return ex.Message.ToString();
			}
		}


		private void releaseObject(object obj, ref string err_msg)
		{
			try
			{
				System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
				obj = null;
			}
			catch (Exception ex)
			{
				obj = null;
				err_msg = "Unable to release the Object " + ex.ToString() + ".";

			}
			finally
			{
				GC.WaitForPendingFinalizers();
				GC.Collect();

				GC.WaitForPendingFinalizers();
				GC.Collect();
			}
		}


		public bool delEmptyRowAndColumn(string path, string file_name, string file_name_new, string firstColName,ref string err_msg)
		{
			bool blReturn = false;

			string[] xlCol = { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L" };
			int[] xlRow = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
           
			object missing = System.Type.Missing;

			string strFirstFoundAddress;

			try
			{
				System.Threading.Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.CreateSpecificCulture("en-US");

				xlApp = new ApplicationClass();

				wb = xlApp.Workbooks.Open(path + file_name, 
					0, 
					true, 
					5, 
					"", 
					"", 
					true, 
					XlPlatform.xlWindows, 
					"\t", 
					false, 
					false, 
					0, 
					true, 
					1, 
					0);

				ws = (Worksheet)wb.Worksheets.get_Item(1);

				// Find's parameters are "sticky". If you don't specify them
				// they'll default to the last used values - including parameters
				// set via Excel's user interface
				rgFound = ws.Cells.Find(firstColName,
					missing,
					XlFindLookIn.xlValues,
					XlLookAt.xlPart,
					missing,
					XlSearchDirection.xlNext,
					false,
					missing,
					missing);


				// If Find doesn't find anything, rgFound will be null
				if (rgFound != null)
				{
					// Save the address of the first found item - 
					// it will be used in a loop terminating condition.
					strFirstFoundAddress = rgFound.get_Address(true, true, XlReferenceStyle.xlA1, missing, missing);


					string[] strTmp = strFirstFoundAddress.Split('$');
					string curCol = strTmp[1];
					string curRow = strTmp[2];

					if (curCol != "A")
					{
						//Delete column
						for (int i = 0; i < xlCol.Length; i++)
						{
							if (xlCol[i].Equals(curCol))
							{
								break;
							}
							((Range)ws.Cells[1,1]).EntireColumn.Delete(missing);
						}
					}
					if (curRow != "1")
					{
						//Delete row
						for (int i = 0; i < xlRow.Length; i++)
						{
							if (xlRow[i].ToString().Equals(curRow))
							{
								break;
							}
							((Range)ws.Cells[1,1]).EntireRow.Delete(missing);
						}
					}
					if (System.IO.File.Exists(path + file_name_new))
						System.IO.File.Delete(path + file_name_new);

					wb.SaveAs(path + file_name_new, missing,
						missing, missing, missing, missing, XlSaveAsAccessMode.xlExclusive,
						missing, missing, missing, missing, missing);
					wb.Close(false, null, null);
					xlApp.Quit();
					blReturn = true;
				}
			}
			catch (Exception ex)
			{  
				err_msg = "Can not search in worksheet![" + ex.Message + "]";
				blReturn = false;
			}

			releaseObject(xlApp,ref err_msg);
			releaseObject(ws,ref err_msg);
			releaseObject(wb,ref err_msg);
			releaseObject(rgFound,ref err_msg);


			return blReturn;
		}

	}
}
