using System;
using System.Data;
using System.Text;
using com.common.DAL;
using com.common.classes;
using com.common.util;
namespace com.ties.classes
{
	/// <summary>
	/// Summary description for Customer.
	/// </summary>
	public class Customer
	{
		public Customer()
		{
			
		}
		
		private string m_strName=null;
		private string m_straddress1=null;
		private string m_straddress2=null;
		private string m_strtelephone=null;
		private string m_strfax=null;
		private string m_strZipCode = null;
		private string m_strApplyDimWt = null;
		private string m_strPODSlip = null;
		private string m_strESASurcharge = null;
		private string m_strESASurchargeDel = null;
		private string m_strContactPerson = null;
		private object m_insurance_percent_surcharge = null;
		private object m_free_insurance_amt = null;
		private object m_mbg = null;
		private object m_cod_surcharge_amt = null;
		private object m_cod_surcharge_percent = null;
		private object m_insurance_maximum_amt = null;
		private object m_density_factor = null;
		private object m_payer_type = null;				//HC Return Task
		private object m_hc_invoice_required = null;	//HC Return Task
		private object m_saleman_id = null;				//Added By Tom 22/7/09
		private string m_strSpecialInstruction = null;	//End Added By Tom 22/7/09
		private string m_strCustID = null;				//Add By Lin 28/4/2010
		private string m_strCustName = null;			//Add By Lin 28/4/2010
		private string m_strRemark2 = null;				//Add By Lin 28/4/2010
		private string m_strSenderZipcode = null;		//Add By Lin 28/4/2010
		private string m_strDim_By_tot = null;		//Jeab 28 Dec 10


		#region Declare By Aoo 14/02/2008
		//Declare By Aoo 14/02/2008
		private string strPaymentMode = null;
		private string strMin_insurance_surcharge = null;
		private string strMax_insurance_surcharge = null;
		private string strOther_surcharge_amount = null;
		private string strOther_surcharge_percentage = null;
		private string strOther_surcharge_min = null;
		private string strOther_surcharge_max = null;
		private string strOther_surcharge_desc = null;
		private string strDiscount_band = null;
		private string strMinimum_box = null;
		private string isCustomerBox = null;
		//End Declare By Aoo
		#endregion


		/// <summary>
		/// This method gets the details from the customer table.
		/// </summary>
		/// <param name="appid">Application ID</param>
		/// <param name="enterpriseid">Enterprise ID</param>
		/// <param name="custid">Customer ID</param>
		public void Populate(string appid,string enterpriseid,string custid)
		{
			DbConnection dbCon= null;
			DataSet dsCustomer =null;
			IDbCommand dbcmd=null;

			dbCon = DbConnectionManager.GetInstance().GetDbConnection(appid,enterpriseid);

			if (dbCon==null)
			{
				Logger.LogTraceError("Customer.cs","Populate","Cust001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			StringBuilder strQry =  new StringBuilder(); 

			strQry = strQry.Append("select * from customer a");
			strQry.Append(" where a.applicationid = '");
			strQry.Append(appid);
			strQry.Append ("' and a.enterpriseid = '");
			strQry.Append (enterpriseid);
			strQry.Append ("' and a.custid ='");
			strQry.Append (Utility.ReplaceSingleQuote(custid));
			strQry.Append ("'");

			dbcmd = dbCon.CreateCommand(strQry.ToString(),CommandType.Text);
 
			try
			{
				dsCustomer = (DataSet)dbCon.ExecuteQuery(dbcmd,com.common.DAL.ReturnType.DataSetType);  
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("Customer.cs","Populate","C001","dbcon is null");
				throw appException;
			}

			if (dsCustomer.Tables[0].Rows.Count  > 0) 
			{
				DataRow drEach = dsCustomer.Tables[0].Rows[0];
				if(!drEach["cust_name"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					m_strName = (string)drEach["cust_name"];
				}
				
				if(!drEach["address1"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					m_straddress1=(string)drEach["address1"];
				}
				
				if(!drEach["address2"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					m_straddress2=(string)drEach["address2"];
				}
				

				if(!drEach["telephone"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					m_strtelephone=(string)drEach["telephone"];
				}
				
				if(!drEach["fax"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					m_strfax=(string)drEach["fax"];
				}
				
				if(!drEach["zipcode"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					m_strZipCode = (string)drEach["zipcode"];
				}
				if(!drEach["apply_dim_wt"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					m_strApplyDimWt = (string)drEach["apply_dim_wt"];
				}
				if(!drEach["pod_slip_required"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					m_strPODSlip = (string)drEach["pod_slip_required"];
				}	
				if(!drEach["apply_esa_surcharge"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					m_strESASurcharge = (string)drEach["apply_esa_surcharge"];
				}
				if(!drEach["apply_esa_surcharge_rep"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					m_strESASurchargeDel = (string)drEach["apply_esa_surcharge_rep"];
				}
				if(!drEach["contact_person"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					m_strContactPerson = (string)drEach["contact_person"];
				}

				m_insurance_percent_surcharge = drEach["insurance_percent_surcharge"];
				m_free_insurance_amt = drEach["free_insurance_amt"];
				m_mbg = drEach["mbg"];
				m_cod_surcharge_amt = drEach["cod_surcharge_amt"];
				m_cod_surcharge_percent = drEach["cod_surcharge_percent"];
				m_insurance_maximum_amt = drEach["insurance_maximum_amt"];
				m_density_factor = drEach["density_factor"];
				m_payer_type = drEach["payer_type"];
				//HC Return Task
				if(!drEach["hc_invoice_required"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					m_hc_invoice_required = (string)drEach["hc_invoice_required"];
				}
				//HC Return Task

				
				if(!drEach["salesmanid"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					m_saleman_id = (string)drEach["salesmanid"];
				}
				

				//By Aoo 14/02/2008
				if(!drEach["Payment_mode"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					this.strPaymentMode = drEach["Payment_mode"].ToString();
				}
				else
				{
					 this.strPaymentMode = "";
					
				}
				if(!drEach["min_insurance_surcharge"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					this.strMin_insurance_surcharge = drEach["min_insurance_surcharge"].ToString();
				}
				else
				{
					this.strMin_insurance_surcharge = "";
				}
				if(!drEach["max_insurance_surcharge"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					this.strMax_insurance_surcharge = drEach["max_insurance_surcharge"].ToString();
				}
				else
				{
					this.strMax_insurance_surcharge = "";
				}
				if(!drEach["other_surcharge_amount"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					this.strOther_surcharge_amount = drEach["other_surcharge_amount"].ToString();
				}
				else
				{
					this.strOther_surcharge_amount = "";
				}
				if(!drEach["other_surcharge_percentage"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					this.strOther_surcharge_percentage = drEach["other_surcharge_percentage"].ToString();
				}
				else
				{
					this.strOther_surcharge_percentage = "";
				}
				if(!drEach["other_surcharge_min"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					this.strOther_surcharge_min = drEach["other_surcharge_min"].ToString();
				}
				else
				{
					this.strOther_surcharge_min = "";
				}
				if(!drEach["other_surcharge_max"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					this.strOther_surcharge_max = drEach["other_surcharge_max"].ToString();
				}
				else
				{
					this.strOther_surcharge_max = "";
				}
				if(!drEach["other_surcharge_desc"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					this.strOther_surcharge_desc = drEach["other_surcharge_desc"].ToString();
				}
				else
				{
					this.strOther_surcharge_desc = "";
				}
				if(!drEach["discount_band"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					this.strDiscount_band = drEach["discount_band"].ToString();
				}
				else
				{
					this.strDiscount_band = "";
				}
				//End By Aoo
				//Added By Tom 22/7/09
				if(!drEach["remark2"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					this.m_strSpecialInstruction = drEach["remark2"].ToString();
				}
				else
				{
					this.m_strSpecialInstruction = "";
				}
				//End Added By Tom 22/7/09
				if(!drEach["Minimum_Box"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					this.strMinimum_box = drEach["Minimum_Box"].ToString();
				}
				else
				{
					this.strMinimum_box = "0";
				}
				if(!drEach["CustomerBox"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					this.isCustomerBox = drEach["CustomerBox"].ToString();
				}
				else
				{
					this.isCustomerBox = "N";
				}
		//Jeab 28 Dec 10
				if(!drEach["dim_by_tot"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					m_strDim_By_tot = (string)drEach["dim_by_tot"];
				}
		//Jeab 28 Dec 10 =========> End
			}
		}
		
		public bool IsBoxRateAvailable(string strAppID, string strEnterpriseId,string strOriginZoneCode, string strDestZoneCode, string strServiceType)
		{
			if(this.IsCustomer_Box == "Y")
			{
				DbConnection dbCon= null;
				DataSet dsBoxRate =null;
				IDbCommand dbcmd=null;

				dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseId);

				if (dbCon==null)
				{
					Logger.LogTraceError("Customer.cs","Populate","Cust001","dbCon is null");
					throw new ApplicationException("Connection to Database failed",null);
				}

				StringBuilder strQry =  new StringBuilder(); 

				strQry = strQry.Append("select * from Base_Zone_Rates a");
				strQry.Append(" where a.applicationid = '");
				strQry.Append(strAppID);
				strQry.Append ("' and a.enterpriseid = '");
				strQry.Append (strEnterpriseId);
				strQry.Append ("' and a.origin_zone_code ='");
				strQry.Append (strOriginZoneCode);
				strQry.Append ("' and a.destination_zone_code = '");
				strQry.Append (strDestZoneCode);
				strQry.Append ("' and a.service_code ='");
				strQry.Append (strServiceType);
				strQry.Append ("' ");

				dbcmd = dbCon.CreateCommand(strQry.ToString(),CommandType.Text);
 
				try
				{
					dsBoxRate = (DataSet)dbCon.ExecuteQuery(dbcmd,com.common.DAL.ReturnType.DataSetType);  
				}
				catch(ApplicationException appException)
				{
					Logger.LogTraceError("Customer.cs","Populate","C001","dbcon is null");
					throw appException;
				}
				if(dsBoxRate.Tables[0].Rows.Count > 0)
				{
					return true;
				}
				else
				{
					return false;
				}
			}else
				return true;
		}
		/// <summary>
		/// This method gets & sets the Customer Name
		/// </summary>
		public string CustomerName
		{
			set
			{
				m_strName=value;
			}
			get
			{
				return m_strName;
			}
		}

		/// <summary>
		/// This method gets or sets the customer Address1.
		/// </summary>
		public string CustomerAddress1
		{
			set
			{
				m_straddress1=value;
			}
			get
			{
				return m_straddress1;
			}
		}

		/// <summary>
		/// This method gets or sets the customer Address2.
		/// </summary>
		public string CustomerAddress2
		{
			set
			{
				m_straddress2=value;
			}
			get
			{
				return m_straddress2;
			}
		}

		/// <summary>
		/// This method gets or sets the customer telephone.
		/// </summary>
		public string CustomerTelephone
		{
			set
			{
				m_strtelephone = value;
			}
			get
			{
				return m_strtelephone;
			}
		}

		/// <summary>
		/// This method gets or sets the customer fax.
		/// </summary>
		public string CustomerFax
		{
			set
			{
				m_strfax=value;
			}
			get
			{
				return m_strfax;
			}

		}
		
		/// <summary>
		/// This method gets or sets the customer zip code.
		/// </summary>
		public string ZipCode
		{
			set
			{
				m_strZipCode = value;
			}
			get
			{
				return m_strZipCode;
			}
		}

		/// <summary>
		/// This method gets & sets the value of Dimensional Weight which  is Y (YES) or N (No).
		/// </summary>
		public string ApplyDimWt
		{
			set
			{
				m_strApplyDimWt = value;
			}
			get
			{
				return m_strApplyDimWt;
			}
		}

		/// <summary>
		/// This method gets & sets the value of POD (Proof of Delivery) which  is Y (YES) or N (No).
		/// </summary>
		public String PODSlipRequired
		{
			set
			{
				m_strPODSlip = value;
			}
			get
			{
				return m_strPODSlip;
			}
		}

		/// <summary>
		/// This method gets & sets the value of ESA (Extended Service Area) Surcharge which  is Y (YES) or N (No).
		/// </summary>
		public String ESASurcharge
		{
			set
			{
				m_strESASurcharge = value;
			}
			get
			{
				return m_strESASurcharge;
			}
		}
		
		/// <summary>
		/// This method gets & sets the value of ESA (Extended Service Area) Surcharge which  is Y (YES) or N (No).
		/// </summary>
		public String ESASurchargeDel
		{
			set
			{
				m_strESASurchargeDel = value;
			}
			get
			{
				return m_strESASurchargeDel;
			}
		}

	//Jeab 28 Dec 10
		public String Dim_By_tot
		{
			set
			{
				m_strDim_By_tot = value;
			}
			get
			{
				return m_strDim_By_tot;
			}
		}
	//Jeab 28 Dec 10 =========> End
		/// <summary>
		/// This method gets & sets the value of Contact Person.
		/// </summary>
		public String ContactPerson
		{
			set
			{
				m_strContactPerson = value;
			}
			get
			{
				return m_strContactPerson;
			}

		}

		public object insurance_percent_surcharge
		{
			set
			{
				m_insurance_percent_surcharge = value;
			}
			get
			{
				return m_insurance_percent_surcharge;
			}

		}

		public object free_insurance_amt
		{
			set
			{
				m_free_insurance_amt = value;
			}
			get
			{
				return m_free_insurance_amt;
			}

		}

		public object mbg
		{
			set
			{
				m_mbg = value;
			}
			get
			{
				return m_mbg;
			}

		}

		public object cod_surcharge_amt
		{
			set
			{
				m_cod_surcharge_amt = value;
			}
			get
			{
				return m_cod_surcharge_amt;
			}

		}

		public object cod_surcharge_percent
		{
			set
			{
				m_cod_surcharge_percent = value;
			}
			get
			{
				return m_cod_surcharge_percent;
			}

		}

		public object insurance_maximum_amt
		{
			set
			{
				m_insurance_maximum_amt = value;
			}
			get
			{
				return m_insurance_maximum_amt;
			}

		}

		public object density_factor
		{
			set
			{
				m_density_factor = value;
			}
			get
			{
				return m_density_factor;
			}

		}

		public object payer_type
		{
			set
			{
				m_payer_type = value;
			}
			get
			{
				return m_payer_type;
			}

		}

		//HC Return Task
		public object hc_invoice_required
		{
			set
			{
				m_hc_invoice_required = value;
			}
			get
			{
				return m_hc_invoice_required;
			}

		}
		//HC Return Task

		public object saleman_id
		{
			set
			{
				m_saleman_id = value;
			}
			get
			{
				return m_saleman_id;
			}
		}
		//Added By Tom 22/7/09
		public string special_instruction
		{
			set
			{
				m_strSpecialInstruction = value;
			}
			get
			{
				return m_strSpecialInstruction;
			}
		}
		//End Added By Tom 22/7/09

		public string Minimum_Box
		{
			set
			{
				strMinimum_box = value;
			}
			get
			{
				return strMinimum_box;
			}
		}

		public string IsCustomer_Box
		{
			set
			{
				isCustomerBox = value;
			}
			get
			{
				return isCustomerBox;
			}
		}

		#region  Create Properties By Aoo 14/02/2008
	
		public string PaymentMode
		{
			get
			{
				return strPaymentMode;
			}
			set
			{
				strPaymentMode = value;
			}
		}
		public string MinInsuranceSurcharge
		{
			get
			{
				return this.strMin_insurance_surcharge;
			}
			set
			{
				this.strMin_insurance_surcharge = value;
			}
		}
		public string MaxInsuranceSurcharge
		{
			get
			{
				return this.strMax_insurance_surcharge;
			}
			set
			{
				this.strMax_insurance_surcharge = value;
			}
		}
		public string OtherSurchargeAmount
		{
			get
			{
				return strOther_surcharge_amount;
			}
			set
			{
				strOther_surcharge_amount = value;
			}
		}
		public string OtherSurchargeAmountPercentage
		{
			get
			{
				return strOther_surcharge_percentage;
			}
			set
			{
				strOther_surcharge_percentage = value;
			}
		}
		public string OtherSurchargeMin
		{
			get
			{
				return strOther_surcharge_min;
			}
			set
			{
				strOther_surcharge_min = value;
			}
		}
		public string OtherSurchargeMax
		{
			get
			{
				return strOther_surcharge_max;
			}
			set
			{
				strOther_surcharge_max = value;
			}
		}
		public string OtherSurchargeDesc
		{
			get
			{
				return strOther_surcharge_desc;
			}
			set
			{
				strOther_surcharge_desc = value;
			}
		}
		public string DiscountBand
		{
			get
			{
				return strDiscount_band;
			}
			set
			{
				strDiscount_band = value;
			}
		}

		#endregion End Create Properties By Aoo


		#region Add By Lin 28/4/2010
			public String CustID  //custid
			{ 
				set
				{
					m_strCustID = value;
				}
				get
				{
					return m_strCustID;
				}
			}

			public String CustName  //cust_name
			{
				set
				{
					m_strCustName = value;
				}
				get
				{
					return m_strCustName;
				}
			}

			public String Remark2  //Remark2
			{
				set
				{
					m_strRemark2 = value;
				}
				get
				{
					return m_strRemark2;
				}
			}

			public String SenderZipcode   //zipcode
			{
				set
				{
					m_strSenderZipcode = value;
				}
				get
				{
					return m_strSenderZipcode;
				}
			}
		#endregion End Add By Lin 28/4/2010
	}
	
}
