using System;
using System.Data;
using System.Text;
using com.common.DAL;
using com.common.classes;
using com.common.util;

namespace com.ties.classes
{
	/// <summary>
	/// Summary description for DeliveryPath.
	/// </summary>
	public class DeliveryPath
	{
		public DeliveryPath()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		private String m_strAppID = null;
		private String m_strEnterpriseID = null;
		private String m_path_code = null;
		private String m_path_description = null;
		private String m_delivery_type = null;
		private decimal m_line_haul_cost = 0;
		private String m_origin_station = null;
		private String m_destination_station = null;
		private String m_flight_vehicle_no = null;
		private String m_awb_driver_name = null;
		private DateTime m_departure_time;

		/*Comment by sompote 2010-04-05
		public void Populate(String appID,String enterpriseID,String pathCode)
		{
			DbConnection dbCon = null;
			DataSet dsLocation = null;
			IDbCommand dbCmd = null;

			dbCon = DbConnectionManager.GetInstance().GetDbConnection(appID,enterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("DeliveryPath.cs","PopulateValues","ERR001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			
			StringBuilder strQry = new StringBuilder();
			strQry = strQry.Append("select * ");
			strQry.Append(" from Delivery_Path  ");
			strQry.Append(" where applicationid = '");
			strQry.Append(appID);
			strQry.Append("' and enterpriseid = '");
			strQry.Append(enterpriseID);
			strQry.Append("' and path_code = '");
			strQry.Append(Utility.ReplaceSingleQuote(pathCode));
			strQry.Append("'");

			dbCmd = dbCon.CreateCommand(strQry.ToString(),CommandType.Text);

			try
			{
				dsLocation =(DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
			}
			catch(ApplicationException appExpection)
			{
				Logger.LogTraceError("DeliveryPath.cs","PopulateValues","ERR002","Error in PopulateValues Execute Query ");
				throw appExpection;
			}
			
			if(dsLocation.Tables[0].Rows.Count >0)
			{
				DataRow drEach = dsLocation.Tables[0].Rows[0];
				
				if(!drEach["path_code"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					m_path_code = (String)drEach["path_code"];
				}
				if(!drEach["path_description"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					m_path_description = (String)drEach["path_description"];
				}
				if(!drEach["delivery_type"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					m_delivery_type = (String)drEach["delivery_type"];
				}
				if(!drEach["line_haul_cost"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					m_line_haul_cost = (decimal)drEach["line_haul_cost"];
				}
				if(!drEach["origin_station"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					m_origin_station = (String)drEach["origin_station"];
				}
				if(!drEach["destination_station"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					m_destination_station = (String)drEach["destination_station"];
				}
				if(!drEach["flight_vehicle_no"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					m_flight_vehicle_no = (String)drEach["flight_vehicle_no"];
				}
				if(!drEach["awb_driver_name"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					m_awb_driver_name = (String)drEach["awb_driver_name"];
				}
				if(!drEach["departure_time"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					m_departure_time = (DateTime)drEach["departure_time"];
				}
			}
		}

		*/
		/// <summary>
		/// Add by Sompote 2010-04-07
		/// </summary>
		/// <param name="appID"></param>
		/// <param name="enterpriseID"></param>
		/// <param name="pathCode"></param>
		public void Populate(String appID,String enterpriseID,String pathCode)
		{
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(appID,enterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("DeliveryPath.cs","PopulateValues","ERR001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			Populate( appID, enterpriseID, pathCode, dbCon);
		}
		/// <summary>
		/// Add by Sompote 2010-04-07
		/// </summary>
		/// <param name="appID"></param>
		/// <param name="enterpriseID"></param>
		/// <param name="pathCode"></param>
		/// <param name="dbCon"></param>
		public void Populate(String appID,String enterpriseID,String pathCode,DbConnection dbCon)
		{
			IDbConnection conApp = dbCon.GetConnection();
			if(conApp == null)
			{
				Logger.LogTraceError("DeliveryPath.cs","PopulateValues","ERR003","conApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			try
			{
				dbCon.OpenConnection(ref conApp);
			}
			catch(ApplicationException appException)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("DeliveryPath.cs","PopulateValues","ERR004","Error opening connection : "+ appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ",appException);
			}
			catch(Exception exception)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("DeliveryPath.cs","PopulateValues","ERR005","Error opening connection : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}
			
			IDbCommand dbCmd = null;		
			dbCmd = dbCon.CreateCommand("",CommandType.Text);
			dbCmd.Connection = conApp;			
			
			try
			{
				Populate( appID, enterpriseID, pathCode, dbCon, dbCmd);
			}
			catch(ApplicationException appExpection)
			{
				throw appExpection;
			}
			finally
			{
				if(conApp != null)
				{
					conApp.Close();
				}
			}

		}

		/// <summary>
		/// Add by Sompote 2010-04-05
		/// </summary>
		/// <param name="appID"></param>
		/// <param name="enterpriseID"></param>
		/// <param name="pathCode"></param>
		/// <param name="dbCon"></param>
		/// <param name="dbCmd"></param>
		public void Populate(String appID,String enterpriseID,String pathCode,DbConnection dbCon,IDbCommand dbCmd)
		{
			//DbConnection dbCon = null;
			DataSet dsLocation = null;
			//IDbCommand dbCmd = null;

			/*dbCon = DbConnectionManager.GetInstance().GetDbConnection(appID,enterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("DeliveryPath.cs","PopulateValues","ERR001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}*/
			
			StringBuilder strQry = new StringBuilder();
			strQry = strQry.Append("select * ");
			strQry.Append(" from Delivery_Path  ");
			strQry.Append(" where applicationid = '");
			strQry.Append(appID);
			strQry.Append("' and enterpriseid = '");
			strQry.Append(enterpriseID);
			strQry.Append("' and path_code = '");
			strQry.Append(Utility.ReplaceSingleQuote(pathCode));
			strQry.Append("'");

			//dbCmd = dbCon.CreateCommand(strQry.ToString(),CommandType.Text);
			dbCmd.CommandText = strQry.ToString();
			try
			{
				dsLocation =(DataSet)dbCon.ExecuteQueryInTransaction(dbCmd,com.common.DAL.ReturnType.DataSetType);
			}
			catch(ApplicationException appExpection)
			{
				Logger.LogTraceError("DeliveryPath.cs","PopulateValues","ERR002","Error in PopulateValues Execute Query ");
				throw appExpection;
			}
			
			if(dsLocation.Tables[0].Rows.Count >0)
			{
				DataRow drEach = dsLocation.Tables[0].Rows[0];
				
				if(!drEach["path_code"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					m_path_code = (String)drEach["path_code"];
				}
				if(!drEach["path_description"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					m_path_description = (String)drEach["path_description"];
				}
				if(!drEach["delivery_type"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					m_delivery_type = (String)drEach["delivery_type"];
				}
				if(!drEach["line_haul_cost"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					m_line_haul_cost = (decimal)drEach["line_haul_cost"];
				}
				if(!drEach["origin_station"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					m_origin_station = (String)drEach["origin_station"];
				}
				if(!drEach["destination_station"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					m_destination_station = (String)drEach["destination_station"];
				}
				if(!drEach["flight_vehicle_no"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					m_flight_vehicle_no = (String)drEach["flight_vehicle_no"];
				}
				if(!drEach["awb_driver_name"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					m_awb_driver_name = (String)drEach["awb_driver_name"];
				}
				if(!drEach["departure_time"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					m_departure_time = (DateTime)drEach["departure_time"];
				}
			}
		}

		public String PathCode
		{
			set
			{
				m_path_code = value;
			}
			get
			{
				return m_path_code;
			}
		}


		public String PathDescription
		{
			set
			{
				m_path_description = value;
			}
			get
			{
				return m_path_description;
			}
		}


		public String DeliveryType
		{
			set
			{
				m_delivery_type = value;
			}
			get
			{
				return m_delivery_type;
			}
		}


		public decimal LineHaulCost
		{
			set
			{
				m_line_haul_cost = value;
			}
			get
			{
				return m_line_haul_cost;
			}
		}


		public String OriginStation
		{
			set
			{
				m_origin_station = value;
			}
			get
			{
				return m_origin_station;
			}
		}


		public String DestinationStation
		{
			set
			{
				m_destination_station = value;
			}
			get
			{
				return m_destination_station;
			}
		}


		public String FlightVehicleNo
		{
			set
			{
				m_flight_vehicle_no = value;
			}
			get
			{
				return m_flight_vehicle_no;
			}
		}


		public String AWBDriverName
		{
			set
			{
				m_awb_driver_name = value;
			}
			get
			{
				return m_awb_driver_name;
			}
		}


		public DateTime DepartureDatetime
		{
			set
			{
				m_departure_time = value;
			}
			get
			{
				return m_departure_time;
			}
		}

	}
}
