using System;
using System.Text;
using Cambro.Web.DbCombo;
using System.Data;
using System.Collections;
using com.common.util;
using com.common.DAL;
using com.common.classes;


namespace com.ties.classes
{
	/// <summary>
	/// Summary description for DbComboDAL.
	/// </summary>
	public class DbComboDAL
	{
//		protected static ConfigSettings configSettings = new ConfigSettings();

		public DbComboDAL()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		public static DataSet OriStateQuery(String strAppID,String strEnterpriseID,Cambro.Web.DbCombo.ServerMethodArgs args,String strWhereClause)
		{
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if (dbCon == null)
			{
				//Logger.LogTraceError("DeliveryManifestMgrDAL","DeliveryManifestMgrDAL","GetAssignedConsignmentList","EDB101","DbConnection object is null!!");
				//System.Diagnostics.Trace.WriteLineIf(Logger.AppSwitch.TraceError,"[DeliveryManifestMgrDAL::GetAssignedConsignmentList()] DbConnection is null","ERROR");
				return null;
			}			
			ArrayList queryParams = new ArrayList();
			String strSQLQuery = null;
			strSQLQuery =  " Select distinct S.Origin_State_Code AS DbComboText, S.Origin_State_Code AS DbComboValue";
			strSQLQuery += " From  Shipment S,Route_Code RC, Delivery_Path DP ";
			strSQLQuery += " where  S.Route_Code = RC.Route_Code and ";
			strSQLQuery += " S.applicationid=RC.applicationid and S.EnterpriseID=RC.EnterpriseID and ";
			strSQLQuery += " RC.Path_Code=DP.Path_Code  and ";
			strSQLQuery += " RC.applicationid=DP.applicationid and RC.EnterpriseID=DP.EnterpriseID and ";
			strSQLQuery += " S.delivery_manifested='N' and ";
			strSQLQuery += " S.applicationid = '"+strAppID+"' and S.EnterpriseID = '"+strEnterpriseID+"' ";
			strSQLQuery += strWhereClause;
			strSQLQuery += " order by S.Origin_State_Code";
			DataSet dsQryData = (DataSet)dbCon.ExecuteQuery(strSQLQuery, queryParams, ReturnType.DataSetType);
			return dsQryData;
		}

		public static DataSet PathDescQuery(String strAppID,String strEnterpriseID,Cambro.Web.DbCombo.ServerMethodArgs args,String strWhereClause)
		{
			String sTempWhere=null;
			sTempWhere= " and path_description like '%"+Utility.ReplaceSingleQuote(args.Query)+"%'";
			sTempWhere+=strWhereClause;
			DataProviderType dbProvider = DbConnectionManager.GetInstance().GetDbProviderType(strAppID,strEnterpriseID);
			ArrayList queryParams = new ArrayList();
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
//			IDbDataParameter dataParam = dbCon.MakeParam("@Query",DbType.String,200,ParameterDirection.Input,"%"+Utility.ReplaceSingleQuote(args.Query)+"%");
//			queryParams.Add(dataParam);
			
			String strQry=null;
			switch (dbProvider)
			{
				case DataProviderType.Sql :
					strQry= "SELECT TOP "+args.Top+" path_description AS DbComboText, path_description AS DbComboValue FROM delivery_path ";
					strQry+= " WHERE applicationID='"+strAppID+"' and EnterpriseID='"+strEnterpriseID+"' "+sTempWhere +" ORDER BY path_code";
					break;
				
				case DataProviderType.Oracle:
					strQry = "SELECT path_description AS DbComboText, path_description AS DbComboValue FROM delivery_path  WHERE rownum <= "+args.Top;
					strQry+= " and applicationID='"+strAppID+"' and EnterpriseID='"+strEnterpriseID+"' "+sTempWhere +" ORDER BY path_code";								
					break;
				
				case DataProviderType.Oledb:
					break;
			}			
			DataSet dsQryData = (DataSet)dbCon.ExecuteQuery(strQry,queryParams,ReturnType.DataSetType);
			return dsQryData;
		}

		public static DataSet PathCodeQuery(String strAppID,String strEnterpriseID,Cambro.Web.DbCombo.ServerMethodArgs args)
		{
			String sTempWhere=null;
			DataProviderType dbProvider = DbConnectionManager.GetInstance().GetDbProviderType(strAppID,strEnterpriseID);
			ArrayList queryParams = new ArrayList();
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			sTempWhere= " and Path_Code like '%"+Utility.ReplaceSingleQuote(args.Query)+"%'";
//			IDbDataParameter dataParam = dbCon.MakeParam("@Query",DbType.String,12,ParameterDirection.Input,"'%"+Utility.ReplaceSingleQuote(args.Query)+"%'");
//			queryParams.Add(dataParam);

			String strQry=null;
			switch (dbProvider)
			{
				case DataProviderType.Sql :
					strQry = " SELECT TOP "+args.Top+" path_code AS DbComboText, path_code AS DbComboValue FROM delivery_path ";
					strQry+= " WHERE applicationID='"+strAppID+"' and EnterpriseID='"+strEnterpriseID+"' "+sTempWhere + " ORDER BY path_code";
					break;	
			
				case DataProviderType.Oracle:
					strQry = " SELECT path_code AS DbComboText, path_code AS DbComboValue FROM delivery_path where rownum <= " + args.Top;
					strQry+= " and applicationID='"+strAppID+"' and EnterpriseID='"+strEnterpriseID+"' "+sTempWhere + " ORDER BY path_code";					
					break;

				case DataProviderType.Oledb:
					break;
			}			


			DataSet dsQryData = (DataSet)dbCon.ExecuteQuery(strQry,queryParams,ReturnType.DataSetType);
			return dsQryData;
		}

		public static DataSet PathCodeWithOutWQuery(String strAppID,String strEnterpriseID,Cambro.Web.DbCombo.ServerMethodArgs args)
		{
			String sTempWhere=null;
			DataProviderType dbProvider = DbConnectionManager.GetInstance().GetDbProviderType(strAppID,strEnterpriseID);
			ArrayList queryParams = new ArrayList();
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			sTempWhere= " and Path_Code like '%"+Utility.ReplaceSingleQuote(args.Query)+"%'";
			//			IDbDataParameter dataParam = dbCon.MakeParam("@Query",DbType.String,12,ParameterDirection.Input,"'%"+Utility.ReplaceSingleQuote(args.Query)+"%'");
			//			queryParams.Add(dataParam);

			String strQry=null;
			switch (dbProvider)
			{
				case DataProviderType.Sql :
					strQry = " SELECT TOP "+args.Top+" path_code AS DbComboText, path_code AS DbComboValue FROM delivery_path ";
					strQry+= " WHERE applicationID='"+strAppID+"' and EnterpriseID='"+strEnterpriseID+"' and delivery_type <> 'W' "+sTempWhere + " ORDER BY path_code";
					break;	
			
				case DataProviderType.Oracle:
					strQry = " SELECT path_code AS DbComboText, path_code AS DbComboValue FROM delivery_path where rownum <= " + args.Top;
					strQry+= " and applicationID='"+strAppID+"' and EnterpriseID='"+strEnterpriseID+"' and delivery_type <> 'W' "+sTempWhere + " ORDER BY path_code";					
					break;

				case DataProviderType.Oledb:
					break;
			}			


			DataSet dsQryData = (DataSet)dbCon.ExecuteQuery(strQry,queryParams,ReturnType.DataSetType);
			return dsQryData;
		}

		public static DataSet PathCodeQuery(String strAppID,String strEnterpriseID,Cambro.Web.DbCombo.ServerMethodArgs args, String strWhereClause)
		{
			String sTempWhere=null;
			sTempWhere= " and Path_Code like '%"+Utility.ReplaceSingleQuote(args.Query)+"%'";
			sTempWhere+=strWhereClause;
			DataProviderType dbProvider = DbConnectionManager.GetInstance().GetDbProviderType(strAppID,strEnterpriseID);
			ArrayList queryParams = new ArrayList();
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
//			IDbDataParameter dataParam = dbCon.MakeParam("@Query",DbType.String,12,ParameterDirection.Input,"%"+Utility.ReplaceSingleQuote(args.Query)+"%");
//			queryParams.Add(dataParam);
			
			String strQry=null;
			switch (dbProvider)
			{
				case DataProviderType.Sql :
					strQry = " SELECT TOP "+args.Top+" path_code AS DbComboText, path_code AS DbComboValue FROM delivery_path ";
					strQry+= " WHERE applicationID='"+strAppID+"' and EnterpriseID='"+strEnterpriseID+"' "+ sTempWhere +" ORDER BY path_code";
					break;	
			
				case DataProviderType.Oracle:
					strQry = " SELECT path_code AS DbComboText, path_code AS DbComboValue FROM delivery_path where rownum <= " + args.Top;
					strQry+= " and applicationID='"+strAppID+"' and EnterpriseID='"+strEnterpriseID+"' "+ sTempWhere + " ORDER BY path_code";					
					break;

				case DataProviderType.Oledb:
					break;
			}		
			
			DataSet dsQryData = (DataSet)dbCon.ExecuteQuery(strQry,queryParams,ReturnType.DataSetType);
			return dsQryData;
		}
		
		public static DataSet DeliveryDatetime(String strAppID, String strEnterpriseID, Cambro.Web.DbCombo.ServerMethodArgs args)
		{
			String sTempWhere=null;
			sTempWhere= " and delivery_manifest_datetime like '%"+Utility.ReplaceSingleQuote(args.Query)+"%'";
			DataProviderType dbProvider = DbConnectionManager.GetInstance().GetDbProviderType(strAppID,strEnterpriseID);
			ArrayList queryParams = new ArrayList();
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
//			IDbDataParameter dataParam = dbCon.MakeParam("@Query",DbType.String,16,ParameterDirection.Input,"%"+args.Query+"%");
//			queryParams.Add(dataParam);			

			String strQry=null;

			switch (dbProvider)
			{
				case DataProviderType.Sql :
					strQry = " SELECT DISTINCT TOP " + args.Top + " dbo.DateToString(delivery_manifest_datetime) AS DbComboText, dbo.DateToString(delivery_manifest_datetime) AS DbComboVAlue From delivery_manifest ";
					strQry += " Where applicationID='"+strAppID+"' and EnterpriseID='"+strEnterpriseID+"' "+sTempWhere+" ORDER BY DbComboText";
					break;	
			
				case DataProviderType.Oracle:
					strQry = " SELECT DISTINCT TO_CHAR(delivery_manifest_datetime,'dd/MM/yyyy HH24:mi') AS DbComboText, TO_CHAR(delivery_manifest_datetime,'dd/MM/yyyy HH24:mi') AS DbComboVAlue From delivery_manifest where rownum < = " + args.Top ;
					strQry += " and applicationID='"+strAppID+"' and EnterpriseID='"+strEnterpriseID+"' "+sTempWhere+" ORDER BY DbComboText";
					break;

				case DataProviderType.Oledb:
					break;
			}		
			
			DataSet dsQryData = (DataSet) dbCon.ExecuteQuery(strQry,queryParams,ReturnType.DataSetType);
			return dsQryData;
		}

		public static DataSet DeliveryDatetime(String strAppID, String strEnterpriseID, Cambro.Web.DbCombo.ServerMethodArgs args, String strWhereClause)
		{
			String sTempWhere=null;
			sTempWhere= " and delivery_manifest_datetime like '%"+Utility.ReplaceSingleQuote(args.Query)+"%'";
			sTempWhere+=strWhereClause;
			DataProviderType dbProvider = DbConnectionManager.GetInstance().GetDbProviderType(strAppID,strEnterpriseID);
			ArrayList queryParams = new ArrayList();
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
//			IDbDataParameter dataParam = dbCon.MakeParam("@Query",DbType.String,16,ParameterDirection.Input,"%"+args.Query+"%");
//			queryParams.Add(dataParam);

			String strQry=null;

			switch (dbProvider)
			{
				case DataProviderType.Sql :
					strQry = " SELECT DISTINCT TOP " + args.Top + " dbo.DateToString(delivery_manifest_datetime) AS DbComboText, dbo.DateToString(delivery_manifest_datetime) AS DbComboVAlue From delivery_manifest ";
					strQry += " Where applicationID='"+strAppID+"' and EnterpriseID='"+strEnterpriseID+"' "+sTempWhere+" ORDER BY DbComboText";
					break;	
			
				case DataProviderType.Oracle:
					strQry = " SELECT DISTINCT TO_CHAR(delivery_manifest_datetime,'dd/MM/yyyy HH24:mi') AS DbComboText, TO_CHAR(delivery_manifest_datetime,'dd/MM/yyyy HH24:mi') AS DbComboVAlue From delivery_manifest where rownum < = " + args.Top ;
					strQry += " and applicationID='"+strAppID+"' and EnterpriseID='"+strEnterpriseID+"' "+sTempWhere+" ORDER BY DbComboText";
					break;

				case DataProviderType.Oledb:
					break;
			}		
			
			DataSet dsQryData = (DataSet) dbCon.ExecuteQuery(strQry,queryParams,ReturnType.DataSetType);
			return dsQryData;
		}
	
		//Added by GwanG on 16/06/08
		public static DataSet DepartDatetime(String strAppID, String strEnterpriseID, Cambro.Web.DbCombo.ServerMethodArgs args)
		{
			String sTempWhere=null;
			sTempWhere= " and Departure_datetime like '%"+Utility.ReplaceSingleQuote(args.Query)+"%'";
			DataProviderType dbProvider = DbConnectionManager.GetInstance().GetDbProviderType(strAppID,strEnterpriseID);
			ArrayList queryParams = new ArrayList();
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
//			IDbDataParameter dataParam = dbCon.MakeParam("@Query",DbType.String,16,ParameterDirection.Input,"%"+args.Query+"%");
//			queryParams.Add(dataParam);			

			String strQry=null;

			switch (dbProvider)
			{
				case DataProviderType.Sql :
					strQry = " SELECT DISTINCT TOP " + args.Top + " dbo.DateToString(Departure_datetime) AS DbComboText, dbo.DateToString(Departure_datetime) AS DbComboVAlue From delivery_manifest ";
					strQry += " Where applicationID='"+strAppID+"' and EnterpriseID='"+strEnterpriseID+"' "+sTempWhere+" ORDER BY DbComboText";
					break;	
			
				case DataProviderType.Oracle:
					strQry = " SELECT DISTINCT TO_CHAR(Departure_datetime,'dd/MM/yyyy HH24:mi') AS DbComboText, TO_CHAR(Departure_datetime,'dd/MM/yyyy HH24:mi') AS DbComboVAlue From delivery_manifest where rownum < = " + args.Top ;
					strQry += " and applicationID='"+strAppID+"' and EnterpriseID='"+strEnterpriseID+"' "+sTempWhere+" ORDER BY DbComboText";
					break;

				case DataProviderType.Oledb:
					break;
			}		
			
			DataSet dsQryData = (DataSet) dbCon.ExecuteQuery(strQry,queryParams,ReturnType.DataSetType);
			return dsQryData;
		}

		public static DataSet DepartDatetime(String strAppID, String strEnterpriseID, Cambro.Web.DbCombo.ServerMethodArgs args, String strWhereClause)
		{
			String sTempWhere=null;
			sTempWhere= " and departure_datetime like '%"+Utility.ReplaceSingleQuote(args.Query)+"%'";
			sTempWhere+=strWhereClause;
			DataProviderType dbProvider = DbConnectionManager.GetInstance().GetDbProviderType(strAppID,strEnterpriseID);
			ArrayList queryParams = new ArrayList();
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
//			IDbDataParameter dataParam = dbCon.MakeParam("@Query",DbType.String,16,ParameterDirection.Input,"%"+args.Query+"%");
//			queryParams.Add(dataParam);

			String strQry=null;

			switch (dbProvider)
			{
				case DataProviderType.Sql :
					strQry = " SELECT DISTINCT TOP " + args.Top + " dbo.DateToString(departure_datetime) AS DbComboText, dbo.DateToString(departure_datetime) AS DbComboVAlue From delivery_manifest ";
					strQry += " Where applicationID='"+strAppID+"' and EnterpriseID='"+strEnterpriseID+"' "+sTempWhere+" ORDER BY DbComboText";
					break;	
			
				case DataProviderType.Oracle:
					strQry = " SELECT DISTINCT TO_CHAR(departure_datetime,'dd/MM/yyyy HH24:mi') AS DbComboText, TO_CHAR(departure_datetime,'dd/MM/yyyy HH24:mi') AS DbComboVAlue From delivery_manifest where rownum < = " + args.Top ;
					strQry += " and applicationID='"+strAppID+"' and EnterpriseID='"+strEnterpriseID+"' "+sTempWhere+" ORDER BY DbComboText";
					break;

				case DataProviderType.Oledb:
					break;
			}		
			
			DataSet dsQryData = (DataSet) dbCon.ExecuteQuery(strQry,queryParams,ReturnType.DataSetType);
			return dsQryData;
		}
	
		// End Added

		public static DataSet FlightVehicleQuery(String strAppID, String strEnterpriseID, Cambro.Web.DbCombo.ServerMethodArgs args, String strWhereClause)
		{
			String sTempWhere=null;
			sTempWhere= " and Flight_Vehicle_No like '%"+Utility.ReplaceSingleQuote(args.Query)+"%'";
			sTempWhere+=strWhereClause;
			DataProviderType dbProvider = DbConnectionManager.GetInstance().GetDbProviderType(strAppID,strEnterpriseID);
			ArrayList queryParams = new ArrayList();
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
//			IDbDataParameter dataParam = dbCon.MakeParam("@Query",DbType.String,12,ParameterDirection.Input,"%"+args.Query+"%");
//			queryParams.Add(dataParam);			

			String strQry=null;
			switch (dbProvider)
			{
				case DataProviderType.Sql :
					strQry = " SELECT DISTINCT TOP " + args.Top + " Flight_Vehicle_No AS DbComboText, Flight_Vehicle_No AS DbComboVAlue From delivery_manifest ";
					strQry += " Where applicationID='"+strAppID+"' and EnterpriseID='"+strEnterpriseID+"' "+sTempWhere+" ORDER BY Flight_Vehicle_No ";
					break;	
			
				case DataProviderType.Oracle:
					strQry = " SELECT DISTINCT Flight_Vehicle_No AS DbComboText, Flight_Vehicle_No AS DbComboVAlue From delivery_manifest WHERE rownum <=" + args.Top;
					strQry += " and applicationID='"+strAppID+"' and EnterpriseID='"+strEnterpriseID+"' "+sTempWhere+" ORDER BY Flight_Vehicle_No ";
					break;

				case DataProviderType.Oledb:
					break;
			}		
			
			DataSet dsQryData = (DataSet) dbCon.ExecuteQuery(strQry,queryParams,ReturnType.DataSetType);
			return dsQryData;
		}

		public static DataSet CostCodeQuery(String strAppID,String strEnterpriseID, int iAccountYear, int iAccountMonth, Cambro.Web.DbCombo.ServerMethodArgs args)
		{
			String sTempWhere=null;
			sTempWhere= " and cost_code like '%"+Utility.ReplaceSingleQuote(args.Query)+"%'";
			DataProviderType dbProvider = DbConnectionManager.GetInstance().GetDbProviderType(strAppID,strEnterpriseID);
			ArrayList queryParams = new ArrayList();
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
//			IDbDataParameter dataParam = dbCon.MakeParam("@Query",DbType.String,12,ParameterDirection.Input,"%"+args.Query+"%");
//			queryParams.Add(dataParam);			

			StringBuilder strBuilder = new StringBuilder();

			switch (dbProvider)
			{
				case DataProviderType.Sql :
					strBuilder.Append("SELECT TOP "+args.Top+" Cost_Code AS DbComboText, Cost_Code AS DbComboValue FROM Cost_Code WHERE applicationid = '");
					strBuilder.Append(strAppID + "' and enterpriseid = '" + strEnterpriseID + "' "+sTempWhere);	
					
					if(iAccountYear != -1 && iAccountMonth != -1)
					{
						strBuilder.Append(" and cost_code not in (select distinct cost_code from Monthly_Cost_Account where applicationid = '");
						strBuilder.Append(strAppID + "' and enterpriseid = '"+strEnterpriseID+"' and account_year = ");
						strBuilder.Append(iAccountYear + " and account_month = "+iAccountMonth+ " )");
					}
					break;	
			
				case DataProviderType.Oracle:
					strBuilder.Append("SELECT Cost_Code AS DbComboText, Cost_Code AS DbComboValue FROM Cost_Code WHERE rownum < = "+ args.Top );
					strBuilder.Append(" and applicationid = '"+ strAppID + "' and enterpriseid = '" + strEnterpriseID + "' "+sTempWhere);
					if(iAccountYear != -1 && iAccountMonth != -1)
					{
						strBuilder.Append(" and cost_code not in (select distinct cost_code from Monthly_Cost_Account where applicationid = '");
						strBuilder.Append(strAppID + "' and enterpriseid = '"+strEnterpriseID+"' and account_year = ");
						strBuilder.Append(iAccountYear + " and account_month = "+iAccountMonth+ " )");
					}
					break;

				case DataProviderType.Oledb:
					break;
			}		

			strBuilder.Append(" ORDER BY cost_code");

			String strQry = strBuilder.ToString();//"SELECT TOP "+args.Top+" cost_code AS DbComboText, cost_code AS DbComboValue FROM Cost_Code WHERE cost_code LIKE @Query and cost_code not in (select distinct cost_code from Monthly_Cost_Account where applicationid = ) ORDER BY cost_code";
			DataSet dsQryData = (DataSet)dbCon.ExecuteQuery(strQry,queryParams,ReturnType.DataSetType);
			return dsQryData;
		}
		
		public static DataSet PayerIDQueryInvoiceGeneration(String strAppID,String strEnterpriseID,Cambro.Web.DbCombo.ServerMethodArgs args, String strPayerType, String strPayerName)
		{
			DataProviderType dbProvider = DbConnectionManager.GetInstance().GetDbProviderType(strAppID,strEnterpriseID);
			ArrayList queryParams = new ArrayList();
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			//			IDbDataParameter dataParam = dbCon.MakeParam("@Query",DbType.String,20,ParameterDirection.Input,"%"+Utility.ReplaceSingleQuote(args.Query)+"%");
			//			queryParams.Add(dataParam);			
			
			String strQry = null;
			strQry = " SELECT DISTINCT TOP "+args.Top+" UPPER(s.payerid) AS DbComboText, UPPER(s.payerid) AS DbComboValue";
			strQry += " FROM dbo.Shipment s WITH(NOLOCK)";
			strQry += " INNER JOIN dbo.Customer c WITH(NOLOCK)";
			strQry += " ON c.applicationid = s.applicationid AND c.enterpriseid = s.enterpriseid AND c.custid = s.payerid";
			strQry += " INNER JOIN dbo.Shipment_Tracking st WITH(NOLOCK)";
			strQry += " ON st.applicationid = s.applicationid AND st.enterpriseid = s.enterpriseid AND";
			strQry += " st.booking_no = s.booking_no AND st.consignment_no = s.consignment_no AND"; 
			strQry += " ISNULL(st.deleted, 'N') = 'N' AND";
			strQry += " ((st.status_code = 'POD' and s.est_delivery_datetime >= st.tracking_datetime) or";
			strQry += " (st.status_code = 'PODEX' and s.est_delivery_datetime < st.tracking_datetime))";
			strQry += " WHERE s.applicationid = '"+strAppID+"' AND s.enterpriseid = '"+strEnterpriseID+"' AND";
			strQry += " s.act_delivery_date IS NOT NULL and s.invoice_date IS NULL AND";
			strQry += " s.payerid LIKE '%"+Utility.ReplaceSingleQuote(args.Query)+"%'";
			strQry += " AND c.Cust_Name LIKE '%"+Utility.ReplaceSingleQuote(strPayerName)+"%'";

			switch (dbProvider)
			{
				case DataProviderType.Sql :
					if (strPayerType=="R") //for credit consignment
					{
						strQry += " AND s.payment_mode = 'R' AND c.payment_mode = 'R'";         
					}
					else if (strPayerType=="CASH") //FOR cash consignment
					{
						strQry += " AND s.payment_mode = 'C' AND c.payment_mode IN ('C','R')";
					}
					else if (strPayerType=="BC")// All Customer By Aoo 27/03/2008
					{
						strQry = " SELECT TOP "+args.Top+" CustID AS DbComboText, CustID AS DbComboValue FROM Customer  ";
						strQry+= " WHERE applicationID='"+strAppID+"' and EnterpriseID='"+strEnterpriseID+"' and CustID LIKE '%"+Utility.ReplaceSingleQuote(args.Query)+"%'";
						strQry+= " and Cust_Name LIKE '%"+Utility.ReplaceSingleQuote(strPayerName)+"%' ORDER BY CustID ";
					}
					break;

				case DataProviderType.Oracle:
					break;
				
				case DataProviderType.Oledb:
					break;
			}		
			
			strQry += " ORDER BY UPPER(s.payerid)";

			DataSet dsQryData = (DataSet)dbCon.ExecuteQuery(strQry,queryParams,ReturnType.DataSetType);
			return dsQryData;
		}

		public static DataSet PayerIDQuery(String strAppID,String strEnterpriseID,Cambro.Web.DbCombo.ServerMethodArgs args, String strPayerType, String strPayerName)
		{
			DataProviderType dbProvider = DbConnectionManager.GetInstance().GetDbProviderType(strAppID,strEnterpriseID);
			ArrayList queryParams = new ArrayList();
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
//			IDbDataParameter dataParam = dbCon.MakeParam("@Query",DbType.String,20,ParameterDirection.Input,"%"+Utility.ReplaceSingleQuote(args.Query)+"%");
//			queryParams.Add(dataParam);			
			
			String strQry=null;

			switch (dbProvider)
			{
				case DataProviderType.Sql :
					if (strPayerType=="A")
					{
						strQry = " SELECT TOP "+args.Top+" AgentID AS DbComboText, AgentID AS DbComboValue FROM Agent  ";
						strQry+= " WHERE applicationID='"+strAppID+"' and EnterpriseID='"+strEnterpriseID+"' and AgentID LIKE '%"+Utility.ReplaceSingleQuote(args.Query)+"%'";
						strQry+= " and Agent_Name LIKE '%"+Utility.ReplaceSingleQuote(strPayerName)+"%' ORDER BY AgentID ";
					}
					else if (strPayerType=="B")
					{
						strQry =  " SELECT TOP "+args.Top+" AgentID AS DbComboText, AgentID AS DbComboValue FROM Agent WHERE applicationID='"+strAppID+"' and EnterpriseID='"+strEnterpriseID+"'"; 
						strQry += " UNION ";
						strQry += " SELECT TOP "+args.Top+" CustID AS DbComboText, CustID AS DbComboValue FROM Customer  WHERE applicationID='"+strAppID+"' and EnterpriseID='"+strEnterpriseID+"'"; 
					}
					else if (strPayerType=="C")
					{
						strQry = " SELECT TOP "+args.Top+" CustID AS DbComboText, CustID AS DbComboValue FROM Customer  ";
						strQry+= " WHERE applicationID='"+strAppID+"' and EnterpriseID='"+strEnterpriseID+"' and CustID LIKE '%"+Utility.ReplaceSingleQuote(args.Query)+"%'";
						strQry+= " and Cust_Name LIKE '%"+Utility.ReplaceSingleQuote(strPayerName)+"%' ORDER BY CustID ";
					}
					else if (strPayerType=="R") //FOR CREDIT CUSTOMER BY X FEB 08 08
					{
						strQry = " SELECT TOP "+args.Top+" CustID AS DbComboText, CustID AS DbComboValue FROM Customer  ";
						strQry+= " WHERE applicationID='"+strAppID+"' and EnterpriseID='"+strEnterpriseID+"' and CustID LIKE '%"+Utility.ReplaceSingleQuote(args.Query)+"%'";
						strQry+= " and Cust_Name LIKE '%"+Utility.ReplaceSingleQuote(strPayerName)+"%' and payment_mode='R' ORDER BY CustID ";
					}
						//Jeab  29 Nov 2011
					else if (strPayerType=="CASH") //FOR CASH CUSTOMER
					{
						strQry = " SELECT TOP "+args.Top+" CustID AS DbComboText, CustID AS DbComboValue FROM Customer  ";
						strQry+= " WHERE applicationID='"+strAppID+"' and EnterpriseID='"+strEnterpriseID+"' and CustID LIKE '%"+Utility.ReplaceSingleQuote(args.Query)+"%'";
						strQry+= " and Cust_Name LIKE '%"+Utility.ReplaceSingleQuote(strPayerName)+"%' ORDER BY CustID ";  //and payment_mode='C' 
					}
						//Jeab  29 Nov 2011  =========> End
					else if (strPayerType=="BC")// All Customer By Aoo 27/03/2008
					{
						strQry = " SELECT TOP "+args.Top+" CustID AS DbComboText, CustID AS DbComboValue FROM Customer  ";
						strQry+= " WHERE applicationID='"+strAppID+"' and EnterpriseID='"+strEnterpriseID+"' and CustID LIKE '%"+Utility.ReplaceSingleQuote(args.Query)+"%'";
						strQry+= " and Cust_Name LIKE '%"+Utility.ReplaceSingleQuote(strPayerName)+"%' ORDER BY CustID ";
					}
					break;

				case DataProviderType.Oracle:
					if (strPayerType=="A")
					{
						strQry = " SELECT AgentID AS DbComboText, AgentID AS DbComboValue FROM Agent  where rownum <=" + args.Top;
						strQry+= " and applicationID='"+strAppID+"' and EnterpriseID='"+strEnterpriseID+"' and AgentID LIKE '%"+Utility.ReplaceSingleQuote(args.Query)+"%'";
						strQry+= " and Agent_Name LIKE '%"+Utility.ReplaceSingleQuote(strPayerName)+"%' ORDER BY AgentID ";
					}
					else if (strPayerType=="B")
					{
						strQry =  " SELECT AgentID AS DbComboText, AgentID AS DbComboValue FROM Agent WHERE applicationID='"+strAppID+"' and EnterpriseID='"+strEnterpriseID+"' and rownum <=" + args.Top;
						strQry += " UNION ";
						strQry += " SELECT CustID AS DbComboText, CustID AS DbComboValue FROM Customer  WHERE applicationID='"+strAppID+"' and EnterpriseID='"+strEnterpriseID+"' and rownum <=" + args.Top;
					}
					else if (strPayerType=="C")
					{
						strQry = " SELECT CustID AS DbComboText, CustID AS DbComboValue FROM Customer where Rownum <="+args.Top;
						strQry+= " and applicationID='"+strAppID+"' and EnterpriseID='"+strEnterpriseID+"' and CustID LIKE '%"+Utility.ReplaceSingleQuote(args.Query)+"%'";
						strQry+= " and Cust_Name LIKE '%"+Utility.ReplaceSingleQuote(strPayerName)+"%' ORDER BY CustID ";
					}
					else if (strPayerType=="R") //FOR CREDIT CUSTOMER BY X FEB 08 08
					{
						strQry = " SELECT TOP "+args.Top+" CustID AS DbComboText, CustID AS DbComboValue FROM Customer  ";
						strQry+= " WHERE applicationID='"+strAppID+"' and EnterpriseID='"+strEnterpriseID+"' and CustID LIKE '%"+Utility.ReplaceSingleQuote(args.Query)+"%'";
						strQry+= " and Cust_Name LIKE '%"+Utility.ReplaceSingleQuote(strPayerName)+"%' and payment_mode='R' ORDER BY CustID ";
					}
					break;
				
				case DataProviderType.Oledb:
					break;
			}		

			DataSet dsQryData = (DataSet)dbCon.ExecuteQuery(strQry,queryParams,ReturnType.DataSetType);
			return dsQryData;
		}

		public static DataSet PayerNameQuery(String strAppID,String strEnterpriseID,Cambro.Web.DbCombo.ServerMethodArgs args,String strPayerType, String strPayerID)
		{
			DataProviderType dbProvider = DbConnectionManager.GetInstance().GetDbProviderType(strAppID,strEnterpriseID);
			ArrayList queryParams = new ArrayList();
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
//			IDbDataParameter dataParam = dbCon.MakeParam("@Query",DbType.String,100,ParameterDirection.Input,"%"+Utility.ReplaceSingleQuote(args.Query)+"%");
//			queryParams.Add(dataParam);
			
			String strQry=null;

			switch (dbProvider)
			{
				case DataProviderType.Sql :
					if (strPayerType=="A")
					{
						strQry = " SELECT TOP "+args.Top+" Agent_Name AS DbComboText, Agent_Name AS DbComboValue FROM Agent  ";
						strQry+= " WHERE applicationID='"+strAppID+"' and EnterpriseID='"+strEnterpriseID+"' ";				
						strQry+= " and AgentID LIKE '%"+Utility.ReplaceSingleQuote(strPayerID)+"%' and Agent_Name LIKE '%"+Utility.ReplaceSingleQuote(args.Query)+"%' ORDER BY Agent_Name ";				
					}
					else if (strPayerType=="B")
					{
						strQry =  " SELECT TOP "+args.Top+" Agent_Name AS DbComboText, Agent_Name AS DbComboValue FROM Agent WHERE applicationID='"+strAppID+"' and EnterpriseID='"+strEnterpriseID+"'"+" and AgentID LIKE '%"+strPayerID+"%' and Agent_Name like '%"+Utility.ReplaceSingleQuote(args.Query)+"%' ";
						strQry += " UNION ";
						strQry += " SELECT TOP "+args.Top+" Cust_Name AS DbComboText, Cust_Name AS DbComboValue FROM Customer  WHERE applicationID='"+strAppID+"' and EnterpriseID='"+strEnterpriseID+"'" +" and CustID LIKE '%"+strPayerID+"%' and Cust_Name like '%"+Utility.ReplaceSingleQuote(args.Query)+"%'";
					}
					else if (strPayerType=="C")
					{
						strQry = " SELECT TOP "+args.Top+" Cust_Name AS DbComboText, Cust_Name AS DbComboValue FROM Customer  ";
						strQry+= " WHERE applicationID='"+strAppID+"' and EnterpriseID='"+strEnterpriseID+"' ";
						strQry+= " and CustID LIKE '%"+Utility.ReplaceSingleQuote(strPayerID)+"%' and Cust_Name LIKE '%"+Utility.ReplaceSingleQuote(args.Query)+"%' ORDER BY Cust_Name ";				
					}		
					break;

				case DataProviderType.Oracle:
					if (strPayerType=="A")
					{
						strQry = " SELECT Agent_Name AS DbComboText, Agent_Name AS DbComboValue FROM Agent where rownum <="+args.Top;
						strQry+= " and applicationID='"+strAppID+"' and EnterpriseID='"+strEnterpriseID+"' ";				
						strQry+= " and AgentID LIKE '%"+Utility.ReplaceSingleQuote(strPayerID)+"%' and Agent_Name LIKE '%"+Utility.ReplaceSingleQuote(args.Query)+"%' ORDER BY Agent_Name ";				
					}
					else if (strPayerType=="B")
					{
						strQry =  " SELECT Agent_Name AS DbComboText, Agent_Name AS DbComboValue FROM Agent WHERE applicationID='"+strAppID+"' and EnterpriseID='"+strEnterpriseID+"' and AgentID LIKE '%"+strPayerID+"%' and Agent_Name LIKE '%"+Utility.ReplaceSingleQuote(args.Query)+"%' and rownum <="+args.Top;
						strQry += " UNION ";
						strQry += " SELECT Cust_Name AS DbComboText, Cust_Name AS DbComboValue FROM Customer  WHERE applicationID='"+strAppID+"' and EnterpriseID='"+strEnterpriseID+"' and CustID LIKE '%"+strPayerID+"%' and Cust_Name LIKE '%"+Utility.ReplaceSingleQuote(args.Query)+"%' and rownum <="+args.Top;
					}
					else if (strPayerType=="C")
					{
						strQry = " SELECT Cust_Name AS DbComboText, Cust_Name AS DbComboValue FROM Customer  where rownum <="+args.Top;
						strQry+= " and applicationID='"+strAppID+"' and EnterpriseID='"+strEnterpriseID+"' ";
						strQry+= " and CustID LIKE '%"+Utility.ReplaceSingleQuote(strPayerID)+"%' and Cust_Name LIKE '%"+Utility.ReplaceSingleQuote(args.Query)+"%' ORDER BY Cust_Name ";
					}		
					break;
				
				case DataProviderType.Oledb:
					break;
			}		
			DataSet dsQryData = (DataSet)dbCon.ExecuteQuery(strQry,queryParams,ReturnType.DataSetType);
			return dsQryData;
		}

		public static DataSet ServiceCodeQuery(String strAppID,String strEnterpriseID,Cambro.Web.DbCombo.ServerMethodArgs args)
		{

			String sTempWhere=null;
			sTempWhere= " and service_code like '%"+Utility.ReplaceSingleQuote(args.Query)+"%'";

			DataProviderType dbProvider = DbConnectionManager.GetInstance().GetDbProviderType(strAppID,strEnterpriseID);
			ArrayList queryParams = new ArrayList();
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
//			IDbDataParameter dataParam = dbCon.MakeParam("@Query",DbType.String,12,ParameterDirection.Input,"%"+args.Query+"%");
//			queryParams.Add(dataParam);
			
			String strQry=null;

			switch (dbProvider)
			{
				case DataProviderType.Sql:
					strQry = " SELECT TOP "+args.Top+" service_code AS DbComboText, service_code AS DbComboValue FROM service ";
					strQry+= " WHERE applicationID='"+strAppID+"' and EnterpriseID='"+strEnterpriseID+"' "+sTempWhere+" ORDER BY service_code";				
					break;

				case DataProviderType.Oracle:
					strQry = " SELECT Service_code AS DbComboText, service_code AS DbComboValue FROM service where rownum <= " + args.Top;
					strQry+= " and applicationID='"+strAppID+"' and EnterpriseID='"+strEnterpriseID+"' "+sTempWhere+" ORDER BY service_code";				
					break;
				
				case DataProviderType.Oledb:
					break;
			}		
			DataSet dsQryData = (DataSet)dbCon.ExecuteQuery(strQry,queryParams,ReturnType.DataSetType);
			return dsQryData;
		}

		public static DataSet GetAllServiceCode(String strAppID,String strEnterpriseID)
		{

			DataProviderType dbProvider = DbConnectionManager.GetInstance().GetDbProviderType(strAppID,strEnterpriseID);
			ArrayList queryParams = new ArrayList();
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			//			IDbDataParameter dataParam = dbCon.MakeParam("@Query",DbType.String,12,ParameterDirection.Input,"%"+args.Query+"%");
			//			queryParams.Add(dataParam);
			
			String strQry=null;

			switch (dbProvider)
			{
				case DataProviderType.Sql:
					strQry = " SELECT service_code AS DbComboText, service_code AS DbComboValue FROM service ";
					strQry+= " WHERE applicationID='"+strAppID+"' and EnterpriseID='"+strEnterpriseID+"' ORDER BY service_code";				
					break;

				case DataProviderType.Oracle:
					strQry = " SELECT Service_code AS DbComboText, service_code AS DbComboValue FROM service where ";
					strQry+= " applicationID='"+strAppID+"' and EnterpriseID='"+strEnterpriseID+"' ORDER BY service_code";				
					break;
				
				case DataProviderType.Oledb:
					break;
			}		
			DataSet dsQryData = (DataSet)dbCon.ExecuteQuery(strQry,queryParams,ReturnType.DataSetType);
			return dsQryData;
		}

		//Jeab 20 Jun 2011
		public static DataSet ExceptionTypeQuery(String strAppID,String strEnterpriseID,Cambro.Web.DbCombo.ServerMethodArgs args)
		{

			String sTempWhere=null;
			sTempWhere= " and exception_type like '%"+Utility.ReplaceSingleQuote(args.Query)+"%'  and status_code = 'PODEX'  ";

			DataProviderType dbProvider = DbConnectionManager.GetInstance().GetDbProviderType(strAppID,strEnterpriseID);
			ArrayList queryParams = new ArrayList();
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			
			String strQry=null;

			switch (dbProvider)
			{
				case DataProviderType.Sql:
					strQry = " SELECT  distinct  exception_type AS DbComboText, exception_type AS DbComboValue FROM exception_code ";
					strQry+= " WHERE applicationID='"+strAppID+"' and EnterpriseID='"+strEnterpriseID+"' "+sTempWhere+" ORDER BY exception_type";				
					break;

				case DataProviderType.Oracle:
					strQry = " SELECT  distinct  exception_type AS DbComboText, exception_type AS DbComboValue FROM exception_code where rownum <= " + args.Top;
					strQry+= " and applicationID='"+strAppID+"' and EnterpriseID='"+strEnterpriseID+"' "+sTempWhere+" ORDER BY exception_type";				
					break;
				
				case DataProviderType.Oledb:
					break;
			}		
			DataSet dsQryData = (DataSet)dbCon.ExecuteQuery(strQry,queryParams,ReturnType.DataSetType);
			return dsQryData;
		}
		//Jeab 20 Jun 2011  =========> End

		public static DataSet OriginZoneCodeQuery(String strAppID,String strEnterpriseID,Cambro.Web.DbCombo.ServerMethodArgs args)
		{
			String sTempWhere=null;
			sTempWhere= " and zone_code like '%"+Utility.ReplaceSingleQuote(args.Query)+"%'";

			DataProviderType dbProvider = DbConnectionManager.GetInstance().GetDbProviderType(strAppID,strEnterpriseID);
			ArrayList queryParams = new ArrayList();
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
//			IDbDataParameter dataParam = dbCon.MakeParam("@Query",DbType.String,12,ParameterDirection.Input,"%"+Utility.ReplaceSingleQuote(args.Query)+"%");
//			queryParams.Add(dataParam);			
			
			String strQry=null;
			
			switch (dbProvider)
			{
				case DataProviderType.Sql:
					strQry = " SELECT DISTINCT TOP "+args.Top+" zone_code AS DbComboText, zone_code AS DbComboValue FROM zone ";
					strQry+= " WHERE applicationID='"+strAppID+"' and EnterpriseID='"+strEnterpriseID+"' "+sTempWhere+" ORDER BY zone_code";
					break;
				
				case DataProviderType.Oracle:
					strQry = " SELECT DISTINCT Zone_code AS DbComboText, zone_code AS DbComboValue FROM zone where rownum <= " + args.Top;
					strQry+= " and applicationID='"+strAppID+"' and EnterpriseID='"+strEnterpriseID+"' "+sTempWhere+" ORDER BY zone_code";
					break;
				
				case DataProviderType.Oledb:
					break;
			}		
			DataSet dsQryData = (DataSet)dbCon.ExecuteQuery(strQry,queryParams,ReturnType.DataSetType);
			return dsQryData; 
		}

		public static DataSet DestZoneCodeQuery(String strAppID,String strEnterpriseID,Cambro.Web.DbCombo.ServerMethodArgs args)
		{
			String sTempWhere=null;
			sTempWhere= " and destination_zone_code like '%"+Utility.ReplaceSingleQuote(args.Query)+"%'";

			DataProviderType dbProvider = DbConnectionManager.GetInstance().GetDbProviderType(strAppID,strEnterpriseID);
			ArrayList queryParams = new ArrayList();
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
//			IDbDataParameter dataParam = dbCon.MakeParam("@Query",DbType.String,12,ParameterDirection.Input,"%"+Utility.ReplaceSingleQuote(args.Query)+"%");
//			queryParams.Add(dataParam);
			
			String strQry=null;
			
			switch (dbProvider)
			{
				case DataProviderType.Sql:
					strQry = " SELECT DISTINCT TOP "+args.Top+" destination_zone_code AS DbComboText, destination_zone_code AS DbComboValue FROM base_zone_rates ";
					strQry+= " WHERE applicationID='"+strAppID+"' and EnterpriseID='"+strEnterpriseID+"' "+sTempWhere+" ORDER BY destination_zone_code";					
					break;

				case DataProviderType.Oracle:
					strQry = " SELECT DISTINCT destination_zone_code AS DbComboText, destination_zone_code AS DbComboValue FROM base_zone_rates where rownum <= "+ args.Top ;
					strQry+= " and applicationID='"+strAppID+"' and EnterpriseID='"+strEnterpriseID+"' "+sTempWhere+" ORDER BY destination_zone_code";
					break;

				case DataProviderType.Oledb:
					break;
			}

			DataSet dsQryData = (DataSet)dbCon.ExecuteQuery(strQry,queryParams,ReturnType.DataSetType);
			return dsQryData;
		}

		public static DataSet SenderNameQuery(String strAppID,String strEnterpriseID,Cambro.Web.DbCombo.ServerMethodArgs args , String strPayerID)
		{
			String sTempWhere=null;
			sTempWhere= " and snd_rec_name like '%"+Utility.ReplaceSingleQuote(args.Query)+"%'";

			DataProviderType dbProvider = DbConnectionManager.GetInstance().GetDbProviderType(strAppID,strEnterpriseID);
			ArrayList queryParams = new ArrayList();
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
//			IDbDataParameter dataParam = dbCon.MakeParam("@Query",DbType.String,100,ParameterDirection.Input,"%"+Utility.ReplaceSingleQuote(args.Query)+"%");
//			queryParams.Add(dataParam);
			
			String strQry=null;
			
			switch (dbProvider)
			{
				case DataProviderType.Sql:
					strQry = " SELECT TOP "+args.Top+" snd_rec_name AS DbComboText, snd_rec_name AS DbComboValue FROM Customer_Snd_Rec ";
					strQry+= " WHERE applicationID='"+strAppID+"' and EnterpriseID='"+strEnterpriseID+"' ";
					strQry+= " and custid LIKE '%"+Utility.ReplaceSingleQuote(strPayerID)+"%' and snd_rec_type='S' or  snd_rec_type='B' "+sTempWhere+" ORDER BY snd_rec_name ";			
					break;

				case DataProviderType.Oracle:
					strQry = " SELECT snd_rec_name AS DbComboText, snd_rec_name AS DbComboValue FROM Customer_Snd_Rec ";
					strQry+= " WHERE applicationID='"+strAppID+"' and EnterpriseID='"+strEnterpriseID+"' and rownum <= "+ args.Top;
					strQry+= " and custid LIKE '%"+Utility.ReplaceSingleQuote(strPayerID)+"%' and snd_rec_type='S' or  snd_rec_type='B' "+sTempWhere+" ORDER BY snd_rec_name ";			
					break;
				
				case DataProviderType.Oledb:
					break;
			}

			DataSet dsQryData = (DataSet)dbCon.ExecuteQuery(strQry,queryParams,ReturnType.DataSetType);
			return dsQryData;
		}

		public static DataSet BookingNoCodeQuery(String strAppID,String strEnterpriseID,Cambro.Web.DbCombo.ServerMethodArgs args)
		{
			String sTempWhere=null;
			sTempWhere= " and booking_no like '%"+Utility.ReplaceSingleQuote(args.Query)+"%'";

			DataProviderType dbProvider = DbConnectionManager.GetInstance().GetDbProviderType(strAppID,strEnterpriseID);
			ArrayList queryParams = new ArrayList();
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
//			IDbDataParameter dataParam = dbCon.MakeParam("@Query",DbType.String,12,ParameterDirection.Input,"%"+Utility.ReplaceSingleQuote(args.Query)+"%");
//			queryParams.Add(dataParam);
			
			String strQry=null;
			
			switch (dbProvider)
			{
				case DataProviderType.Sql:
					strQry = " SELECT DISTINCT TOP "+args.Top+"booking_no AS DbComboText, booking_no AS DbComboValue FROM dispatch_tracking";
					strQry+= " WHERE applicationID='"+strAppID+"' and EnterpriseID='"+strEnterpriseID+"' "+sTempWhere+" ORDER BY booking_no Desc";
					break;

				case DataProviderType.Oracle:
					strQry = " SELECT DISTINCT booking_no AS DbComboText, booking_no AS DbComboValue FROM dispatch_tracking where Rownum <=" + args.Top;
					strQry+= " and applicationID='"+strAppID+"' and EnterpriseID='"+strEnterpriseID+"' "+sTempWhere+" ORDER BY booking_no Desc";
					break;
				
				case DataProviderType.Oledb:
					break;
			}

			DataSet dsQryData = (DataSet)dbCon.ExecuteQuery(strQry,queryParams,ReturnType.DataSetType);
			return dsQryData; 
		}

		public static DataSet StatusCodeQuery(String strAppID,String strEnterpriseID,Cambro.Web.DbCombo.ServerMethodArgs args)
		{
			String sTempWhere=null;
			sTempWhere= " and status_code like '%"+Utility.ReplaceSingleQuote(args.Query)+"%'";

			DataProviderType dbProvider = DbConnectionManager.GetInstance().GetDbProviderType(strAppID,strEnterpriseID);
			ArrayList queryParams = new ArrayList();
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
//			IDbDataParameter dataParam = dbCon.MakeParam("@Query",DbType.String,12,ParameterDirection.Input,"%"+Utility.ReplaceSingleQuote(args.Query)+"%");
//			queryParams.Add(dataParam);			
			
			String strQry=null;
			
			switch (dbProvider)
			{
				case DataProviderType.Sql:
					strQry = " SELECT DISTINCT TOP "+args.Top+"status_code AS DbComboText, status_code AS DbComboValue FROM status_code";
					strQry+= " WHERE applicationID='"+strAppID+"' and EnterpriseID='"+strEnterpriseID+"' "+sTempWhere+" ORDER BY status_code";
					break;

				case DataProviderType.Oracle:
					strQry = " SELECT DISTINCT status_code AS DbComboText, status_code AS DbComboValue FROM status_code where Rownum<="+args.Top;
					strQry+= " WHERE applicationID='"+strAppID+"' and EnterpriseID='"+strEnterpriseID+"' "+sTempWhere+" ORDER BY status_code";
					break;

				case DataProviderType.Oledb:
					break;
			}

			DataSet dsQryData = (DataSet)dbCon.ExecuteQuery(strQry,queryParams,ReturnType.DataSetType);
			return dsQryData; 
		}

		public static DataSet ExceptioCodeQuery(String strAppID,String strEnterpriseID,Cambro.Web.DbCombo.ServerMethodArgs args , String strStatusCode)
		{
			String sTempWhere=null;
			sTempWhere= " and exception_code like '%"+Utility.ReplaceSingleQuote(args.Query)+"%'";

			DataProviderType dbProvider = DbConnectionManager.GetInstance().GetDbProviderType(strAppID,strEnterpriseID);
			ArrayList queryParams = new ArrayList();
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
//			IDbDataParameter dataParam = dbCon.MakeParam("@Query",DbType.String,100,ParameterDirection.Input,"%"+Utility.ReplaceSingleQuote(args.Query)+"%");
//			queryParams.Add(dataParam);			
			
			String strQry=null;
			
			switch (dbProvider)
			{
				case DataProviderType.Sql:
					strQry = " SELECT TOP "+args.Top+" exception_code AS DbComboText, exception_code AS DbComboValue FROM exception_code ";
					strQry+= " WHERE applicationID='"+strAppID+"' and EnterpriseID='"+strEnterpriseID+"' ";
					strQry+= " and status_code LIKE '%"+Utility.ReplaceSingleQuote(strStatusCode)+"%' "+sTempWhere+" ORDER BY exception_code ";			
					break;

				case DataProviderType.Oracle:
					strQry = " SELECT exception_code AS DbComboText, exception_code AS DbComboValue FROM exception_code where Rownum <="+args.Top;
					strQry+= " and applicationID='"+strAppID+"' and EnterpriseID='"+strEnterpriseID+"' ";
					strQry+= " and status_code LIKE '%"+Utility.ReplaceSingleQuote(strStatusCode)+"%' "+sTempWhere+" ORDER BY exception_code ";							
					break;

				case DataProviderType.Oledb:
					break;
			}

			DataSet dsQryData = (DataSet)dbCon.ExecuteQuery(strQry,queryParams,ReturnType.DataSetType);
			return dsQryData;
		}
		
		public static DataSet RouteCodeQuery(String strAppID,String strEnterpriseID,Cambro.Web.DbCombo.ServerMethodArgs args , String strDeliveryPathCode)
		{
			String sTempWhere=null;
			sTempWhere= " and route_code like '%"+Utility.ReplaceSingleQuote(args.Query)+"%'";

			DataProviderType dbProvider = DbConnectionManager.GetInstance().GetDbProviderType(strAppID,strEnterpriseID);
			ArrayList queryParams = new ArrayList();
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
//			IDbDataParameter dataParam = dbCon.MakeParam("@Query",DbType.String,20,ParameterDirection.Input,"%"+Utility.ReplaceSingleQuote(args.Query)+"%");
//			queryParams.Add(dataParam);			
			
			String strQry=null;
			
			switch (dbProvider)
			{
				case DataProviderType.Sql:			
					strQry = " SELECT TOP "+args.Top+" route_code AS DbComboText, route_code AS DbComboValue FROM route_code  ";
					strQry+= " WHERE applicationID='"+strAppID+"' and EnterpriseID='"+strEnterpriseID+"' "+sTempWhere;
					strQry+= " and path_code LIKE '%"+Utility.ReplaceSingleQuote(strDeliveryPathCode)+"%' ORDER BY route_code  ";
					break;

				case DataProviderType.Oracle:
					strQry = " SELECT route_code AS DbComboText, route_code AS DbComboValue FROM route_code  where rownum <=" + args.Top;
					strQry+= " and applicationID='"+strAppID+"' and EnterpriseID='"+strEnterpriseID+"' "+sTempWhere;
					strQry+= " and path_code LIKE '%"+Utility.ReplaceSingleQuote(strDeliveryPathCode)+"%' ORDER BY route_code  ";
					break;

				case DataProviderType.Oledb:
					break;
			}

			DataSet dsQryData = (DataSet)dbCon.ExecuteQuery(strQry,queryParams,ReturnType.DataSetType);
			return dsQryData;
		}

		public static DataSet RecpNameQuery(String strAppID,String strEnterpriseID,Cambro.Web.DbCombo.ServerMethodArgs args)
		{
			String sTempWhere=null;
			sTempWhere= " and snd_rec_name like '%"+Utility.ReplaceSingleQuote(args.Query)+"%'";

			DataProviderType dbProvider = DbConnectionManager.GetInstance().GetDbProviderType(strAppID,strEnterpriseID);
			ArrayList queryParams = new ArrayList();
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
//			IDbDataParameter dataParam = dbCon.MakeParam("@Query",DbType.String,100,ParameterDirection.Input,"%"+Utility.ReplaceSingleQuote(args.Query)+"%");
//			queryParams.Add(dataParam);
			
			String strQry=null;
			
			switch (dbProvider)
			{
				case DataProviderType.Sql:	
					strQry = " SELECT TOP "+args.Top+" snd_rec_name AS DbComboText, snd_rec_name AS DbComboValue FROM Customer_Snd_Rec ";
					strQry+= " WHERE applicationID='"+strAppID+"' and EnterpriseID='"+strEnterpriseID+"' ";
					strQry+= " and snd_rec_type = 'R'  or  snd_rec_type='B' "+sTempWhere+" ORDER BY snd_rec_name ";			
					break;

				case DataProviderType.Oracle:
					strQry = " SELECT snd_rec_name AS DbComboText, snd_rec_name AS DbComboValue FROM Customer_Snd_Rec where rownum <= " + args.Top;
					strQry+= " and applicationID='"+strAppID+"' and EnterpriseID='"+strEnterpriseID+"' ";
					strQry+= " and snd_rec_type = 'R'  or  snd_rec_type='B' "+sTempWhere+" ORDER BY snd_rec_name ";			
					break;
				
				case DataProviderType.Oledb:
					break;
			}

			DataSet dsQryData = (DataSet)dbCon.ExecuteQuery(strQry,queryParams,ReturnType.DataSetType);
			return dsQryData;
		}

		public static DataSet PickupStatusCodeQuery(String strAppID,String strEnterpriseID,Cambro.Web.DbCombo.ServerMethodArgs args)
		{
			String sTempWhere=null;
			sTempWhere= " and status_code like '%"+Utility.ReplaceSingleQuote(args.Query)+"%'";

			DataProviderType dbProvider = DbConnectionManager.GetInstance().GetDbProviderType(strAppID,strEnterpriseID);
			ArrayList queryParams = new ArrayList();
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
//			IDbDataParameter dataParam = dbCon.MakeParam("@Query",DbType.String,12,ParameterDirection.Input,"%"+Utility.ReplaceSingleQuote(args.Query)+"%");
//			queryParams.Add(dataParam);			
//			
			String strQry=null;
			
			switch (dbProvider)
			{
				case DataProviderType.Sql:	
					strQry = " SELECT DISTINCT TOP "+args.Top+"status_code AS DbComboText, status_code AS DbComboValue FROM status_code";
					strQry+= " WHERE applicationID='"+strAppID+"' and EnterpriseID='"+strEnterpriseID+"'and status_type IN ('P','B') "+sTempWhere+" ORDER BY status_code";
					break;

				case DataProviderType.Oracle:
					strQry = " SELECT DISTINCT status_code AS DbComboText, status_code AS DbComboValue FROM status_code where rownum <= "+args.Top ;
					strQry+= " and applicationID='"+strAppID+"' and EnterpriseID='"+strEnterpriseID+"'and status_type IN ('P','B')' "+sTempWhere+" ORDER BY status_code";
					break;

				case DataProviderType.Oledb:
					break;
			}

			DataSet dsQryData = (DataSet)dbCon.ExecuteQuery(strQry,queryParams,ReturnType.DataSetType);
			return dsQryData; 
		}


		public static DataSet VersusStatusCodeQuery(String strAppID,String strEnterpriseID,Cambro.Web.DbCombo.ServerMethodArgs args,String typef,String cri)
		{
			String sTempWhere=null;
			sTempWhere= " and status_code like '%"+Utility.ReplaceSingleQuote(args.Query)+"%'";

			DataProviderType dbProvider = DbConnectionManager.GetInstance().GetDbProviderType(strAppID,strEnterpriseID);
			ArrayList queryParams = new ArrayList();
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
		
			String strQry=null;
			
			switch (dbProvider)
			{
				case DataProviderType.Sql:	
					strQry = " SELECT DISTINCT TOP "+args.Top+"status_code AS DbComboText, status_code AS DbComboValue FROM status_code";
					strQry+= " WHERE applicationID='"+strAppID+"' and EnterpriseID='"+strEnterpriseID+"' "+sTempWhere;
					if(typef != "D")
					{
						strQry+= " and status_type in ('P','D') " ;
					}
					if(cri != "")
					{
						strQry+= " and status_code ='" + cri +"'";					
					}
					
					strQry+= " ORDER BY status_code";
					break;

				case DataProviderType.Oracle:
					strQry = " SELECT DISTINCT status_code AS DbComboText, status_code AS DbComboValue FROM status_code where rownum <= "+args.Top ;
					strQry+= " and applicationID='"+strAppID+"' and EnterpriseID='"+strEnterpriseID+"'and status_type='P' "+sTempWhere+" ORDER BY status_code";
					break;

				case DataProviderType.Oledb:
					break;
			}

			DataSet dsQryData = (DataSet)dbCon.ExecuteQuery(strQry,queryParams,ReturnType.DataSetType);
			return dsQryData; 
		}

		public static DataSet VersusStatusCodeQuery(String strAppID,String strEnterpriseID,String typef,String cri)
		{
			String sTempWhere=null;
//			sTempWhere= " and status_code like '%"+Utility.ReplaceSingleQuote(args.Query)+"%'";

			DataProviderType dbProvider = DbConnectionManager.GetInstance().GetDbProviderType(strAppID,strEnterpriseID);
			ArrayList queryParams = new ArrayList();
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
		
			String strQry=null;
			
			switch (dbProvider)
			{
				case DataProviderType.Sql:	
					strQry = " SELECT DISTINCT status_code , status_description  FROM status_code";
					strQry+= " WHERE applicationID='"+strAppID+"' and EnterpriseID='"+strEnterpriseID+"' ";//+sTempWhere;
					if(typef != "D")
					{
						strQry+= " and status_type in ('P','D') " ;
					}
					if(cri != "")
					{
						strQry+= " and status_code ='" + cri +"'";					
					}
					
					strQry+= " ORDER BY status_code";
					break;

				case DataProviderType.Oracle:
//					strQry = " SELECT DISTINCT status_code AS DbComboText, status_code AS DbComboValue FROM status_code where rownum <= "+args.Top ;
//					strQry+= " and applicationID='"+strAppID+"' and EnterpriseID='"+strEnterpriseID+"'and status_type='P' "+sTempWhere+" ORDER BY status_code";
					break;

				case DataProviderType.Oledb:
					break;
			}

			DataSet dsQryData = (DataSet)dbCon.ExecuteQuery(strQry,queryParams,ReturnType.DataSetType);
			return dsQryData; 
		}



		public static DataSet StateCodeQuery(String strAppID,String strEnterpriseID,Cambro.Web.DbCombo.ServerMethodArgs args,String strCountry)
		{
			String sTempWhere=null;
			sTempWhere= " and state_code like '%"+Utility.ReplaceSingleQuote(args.Query)+"%'";

			DataProviderType dbProvider = DbConnectionManager.GetInstance().GetDbProviderType(strAppID,strEnterpriseID);
			ArrayList queryParams = new ArrayList();
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
//			IDbDataParameter dataParam = dbCon.MakeParam("@Query",DbType.String,12,ParameterDirection.Input,"%"+Utility.ReplaceSingleQuote(args.Query)+"%");
//			queryParams.Add(dataParam);
			
			String strQry=null;
			
			switch (dbProvider)
			{
				case DataProviderType.Sql:	
					strQry = " SELECT DISTINCT TOP "+args.Top+" state_code AS DbComboText, state_code AS DbComboValue FROM state ";
					strQry+= " WHERE applicationID='"+strAppID+"' and EnterpriseID='"+strEnterpriseID+"' and country like '%"+Utility.ReplaceSingleQuote(strCountry)+"%' "+sTempWhere+" ORDER BY state_code";
					break;

				case DataProviderType.Oracle:
					strQry = " SELECT DISTINCT state_code AS DbComboText, state_code AS DbComboValue FROM state where Rownum<="+args.Top;
					strQry+= " and applicationID='"+strAppID+"' and EnterpriseID='"+strEnterpriseID+"' and country like '%"+Utility.ReplaceSingleQuote(strCountry)+"%' "+sTempWhere+" ORDER BY state_code";
					break;
			
				case DataProviderType.Oledb:
					break;
			}

			DataSet dsQryData = (DataSet)dbCon.ExecuteQuery(strQry,queryParams,ReturnType.DataSetType);
			return dsQryData; 
		}

		public static DataSet ZipCodeQuery(String strAppID,String strEnterpriseID,Cambro.Web.DbCombo.ServerMethodArgs args,String strCountry,String strState)
		{
			String sTempWhere=null;
			sTempWhere= " and zipcode like '%"+Utility.ReplaceSingleQuote(args.Query)+"%'";

			DataProviderType dbProvider = DbConnectionManager.GetInstance().GetDbProviderType(strAppID,strEnterpriseID);
			ArrayList queryParams = new ArrayList();
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
//			IDbDataParameter dataParam = dbCon.MakeParam("@Query",DbType.String,12,ParameterDirection.Input,"%"+Utility.ReplaceSingleQuote(args.Query)+"%");
//			queryParams.Add(dataParam);			
			
			String strQry=null;
			
			switch (dbProvider)
			{
				case DataProviderType.Sql:	
					strQry = " SELECT DISTINCT TOP "+args.Top+" zipcode AS DbComboText, zipcode AS DbComboValue FROM zipcode ";
					strQry+= " WHERE applicationID='"+strAppID+"' and EnterpriseID='"+strEnterpriseID+"' and country like '%"+Utility.ReplaceSingleQuote(strCountry)+"%'";
					strQry+= " and state_code like '%"+Utility.ReplaceSingleQuote(strState)+"%' "+sTempWhere+" ORDER BY zipcode";
					break;

				case DataProviderType.Oracle:
					strQry = " SELECT DISTINCT zipcode AS DbComboText, zipcode AS DbComboValue FROM zipcode where rownum<="+args.Top;
					strQry+= " and applicationID='"+strAppID+"' and EnterpriseID='"+strEnterpriseID+"' and country like '%"+Utility.ReplaceSingleQuote(strCountry)+"%'";
					strQry+= " and state_code like '%"+Utility.ReplaceSingleQuote(strState)+"%' "+sTempWhere+" ORDER BY zipcode";
					break;
			
				case DataProviderType.Oledb:
					break;
			}

			DataSet dsQryData = (DataSet)dbCon.ExecuteQuery(strQry,queryParams,ReturnType.DataSetType);
			return dsQryData; 
		}

		public static DataSet CountryQuery(String strAppID,String strEnterpriseID,Cambro.Web.DbCombo.ServerMethodArgs args)
		{
			String sTempWhere=null;
			sTempWhere= " and country like '%"+Utility.ReplaceSingleQuote(args.Query)+"%'";

			DataProviderType dbProvider = DbConnectionManager.GetInstance().GetDbProviderType(strAppID,strEnterpriseID);
			ArrayList queryParams = new ArrayList();
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
//			IDbDataParameter dataParam = dbCon.MakeParam("@Query",DbType.String,100,ParameterDirection.Input,"%"+Utility.ReplaceSingleQuote(args.Query)+"%");
//			queryParams.Add(dataParam);			
			
			String strQry=null;
			
			switch (dbProvider)
			{
				case DataProviderType.Sql:	
					strQry = " SELECT distinct TOP "+args.Top+" country AS DbComboText, country AS DbComboValue FROM state ";
					strQry+= " WHERE applicationID='"+strAppID+"' and EnterpriseID='"+strEnterpriseID+"' ";
					strQry+=  sTempWhere+" ORDER BY country ";			
					break;

				case DataProviderType.Oracle:
					strQry = " SELECT distinct country AS DbComboText, country AS DbComboValue FROM state ";
					strQry+= " WHERE applicationID='"+strAppID+"' and EnterpriseID='"+strEnterpriseID+"' ";
					strQry+=  sTempWhere+" ORDER BY country ";	
					break;
			
				case DataProviderType.Oledb:
					break;
			}

			DataSet dsQryData = (DataSet)dbCon.ExecuteQuery(strQry,queryParams,ReturnType.DataSetType);
			return dsQryData;
		}

		public static DataSet CreditDebitQuery(String strAppID,String strEnterpriseID,Cambro.Web.DbCombo.ServerMethodArgs args,String strSearchType, String strInvoiceNo)
		{
//			String sTempWhere=null;
//			sTempWhere= " and Credit_No like '%"+Utility.ReplaceSingleQuote(args.Query)+"%'";

			DataProviderType dbProvider = DbConnectionManager.GetInstance().GetDbProviderType(strAppID,strEnterpriseID);
			ArrayList queryParams = new ArrayList();
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
//			IDbDataParameter dataParam = dbCon.MakeParam("@Query",DbType.String,100,ParameterDirection.Input,"%"+Utility.ReplaceSingleQuote(args.Query)+"%");
//			queryParams.Add(dataParam);
			
			String strQry=null;
			
			switch (dbProvider)
			{
				case DataProviderType.Sql:	

					if(strSearchType =="C")			// CREDIT NOTE
					{
						strQry = " SELECT  TOP "+args.Top+" Credit_No AS DbComboText, Credit_No AS DbComboValue FROM Credit_Note ";
						strQry+= " WHERE applicationID='"+strAppID+"' and EnterpriseID='"+strEnterpriseID+"' ";
						strQry+= " and Credit_No LIKE '%"+Utility.ReplaceSingleQuote(args.Query)+"%' and Invoice_No like '%"+ Utility.ReplaceSingleQuote(strInvoiceNo) +"%' ORDER BY Credit_No ";			
					}
					else if (strSearchType=="D")	// DEBIT NOTE
					{
						strQry = " SELECT  TOP "+args.Top+" Debit_No AS DbComboText, Debit_No AS DbComboValue FROM Debit_Note ";
						strQry+= " WHERE applicationID='"+strAppID+"' and EnterpriseID='"+strEnterpriseID+"' ";
						strQry+= " and Debit_No LIKE '%"+Utility.ReplaceSingleQuote(args.Query)+"%' and Invoice_No like '%"+Utility.ReplaceSingleQuote(strInvoiceNo)+"%' ORDER BY Debit_No ";			
					}	
					break;

				case DataProviderType.Oracle:
					if(strSearchType =="C")			// CREDIT NOTE
					{
						strQry = " SELECT Credit_No AS DbComboText, Credit_No AS DbComboValue FROM Credit_Note where rownum <="+args.Top;
						strQry+= " and applicationID='"+strAppID+"' and EnterpriseID='"+strEnterpriseID+"' ";
						strQry+= " and Credit_No LIKE '%"+Utility.ReplaceSingleQuote(args.Query)+"%' and Invoice_No like '%"+ Utility.ReplaceSingleQuote(strInvoiceNo) +"%' ORDER BY Credit_No ";			
					}
					else if (strSearchType=="D")	// DEBIT NOTE
					{
						strQry = " SELECT Debit_No AS DbComboText, Debit_No AS DbComboValue FROM Debit_Note where rownum <="+args.Top;
						strQry+= " and applicationID='"+strAppID+"' and EnterpriseID='"+strEnterpriseID+"' ";
						strQry+= " and Debit_No LIKE '%"+Utility.ReplaceSingleQuote(args.Query)+"%' and Invoice_No like '%"+Utility.ReplaceSingleQuote(strInvoiceNo)+"%' ORDER BY Debit_No ";			
					}	
					break;
				
				case DataProviderType.Oledb:
					break;
			}
			
			DataSet dsQryData = (DataSet)dbCon.ExecuteQuery(strQry,queryParams,ReturnType.DataSetType);
			return dsQryData;
		}

		public static DataSet InvoiceQuery(String strAppID,String strEnterpriseID,Cambro.Web.DbCombo.ServerMethodArgs args,String strSearchType, String strCreditDebitNo)
		{
			String sTempWhere=null;
			sTempWhere= " and Invoice_No like '%"+Utility.ReplaceSingleQuote(args.Query)+"%'";

			DataProviderType dbProvider = DbConnectionManager.GetInstance().GetDbProviderType(strAppID,strEnterpriseID);
			ArrayList queryParams = new ArrayList();
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
//			IDbDataParameter dataParam = dbCon.MakeParam("@Query",DbType.String,100,ParameterDirection.Input,"%"+Utility.ReplaceSingleQuote(args.Query)+"%");
//			queryParams.Add(dataParam);			
			
			String strQry=null;
			
			switch (dbProvider)
			{
				case DataProviderType.Sql:	

					if(strSearchType =="C")
					{
						strQry = " SELECT distinct TOP "+args.Top+" Invoice_No AS DbComboText, Invoice_No AS DbComboValue FROM Credit_Note ";
						strQry+= " WHERE applicationID='"+strAppID+"' and EnterpriseID='"+strEnterpriseID+"' ";
						strQry+= sTempWhere+" and Credit_No LIKE '%"+ Utility.ReplaceSingleQuote(strCreditDebitNo) +"%' ORDER BY Invoice_No ";			
					}
					else if(strSearchType =="D")
					{
						strQry = " SELECT distinct TOP "+args.Top+" Invoice_No AS DbComboText, Invoice_No AS DbComboValue FROM Debit_Note ";
						strQry+= " WHERE applicationID='"+strAppID+"' and EnterpriseID='"+strEnterpriseID+"' ";
						strQry+= sTempWhere+" and Debit_No LIKE '%"+ Utility.ReplaceSingleQuote(strCreditDebitNo) +"%' ORDER BY Invoice_No ";			
					}
					break;

				case DataProviderType.Oracle:
					
					if(strSearchType =="C")
					{
						strQry = " SELECT distinct Invoice_No AS DbComboText, Invoice_No AS DbComboValue FROM Credit_Note where rownum <="+args.Top;
						strQry+= " and applicationID='"+strAppID+"' and EnterpriseID='"+strEnterpriseID+"' ";
						strQry+= sTempWhere+" and Credit_No LIKE '%"+ Utility.ReplaceSingleQuote(strCreditDebitNo) +"%' ORDER BY Invoice_No ";			
					}
					else if(strSearchType =="D")
					{
						strQry = " SELECT distinct Invoice_No AS DbComboText, Invoice_No AS DbComboValue FROM Debit_Note where rownum <="+args.Top;
						strQry+= " and applicationID='"+strAppID+"' and EnterpriseID='"+strEnterpriseID+"' ";
						strQry+= sTempWhere+" and Debit_No LIKE '%"+ Utility.ReplaceSingleQuote(strCreditDebitNo) +"%' ORDER BY Invoice_No ";			
					}
					break;
				
				case DataProviderType.Oledb:
					break;
			}
			
			DataSet dsQryData = (DataSet)dbCon.ExecuteQuery(strQry,queryParams,ReturnType.DataSetType);
			return dsQryData;
		}

		public static DataSet InvoiceQuery(String strAppID,String strEnterpriseID,Cambro.Web.DbCombo.ServerMethodArgs args)
		{
			String sTempWhere=null;
			sTempWhere= " and Invoice_No like '%"+Utility.ReplaceSingleQuote(args.Query)+"%'";

			DataProviderType dbProvider = DbConnectionManager.GetInstance().GetDbProviderType(strAppID,strEnterpriseID);
			ArrayList queryParams = new ArrayList();
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
//			IDbDataParameter dataParam = dbCon.MakeParam("@Query",DbType.String,100,ParameterDirection.Input,"%"+args.Query+"%");
//			queryParams.Add(dataParam);			
			
			String strQry=null;

			switch (dbProvider)
			{
				case DataProviderType.Sql:	
					strQry = " SELECT TOP "+args.Top+" Invoice_No AS DbComboText, Invoice_No AS DbComboValue FROM Invoice ";
					strQry+= " WHERE applicationID='"+strAppID+"' and EnterpriseID='"+strEnterpriseID+"' "+sTempWhere+" ORDER BY Invoice_No";
					break;

				case DataProviderType.Oracle:
					strQry = " SELECT Invoice_No AS DbComboText, Invoice_No AS DbComboValue FROM Invoice where Rownum <="+args.Top;
					strQry+= " and applicationID='"+strAppID+"' and EnterpriseID='"+strEnterpriseID+"' "+sTempWhere+" ORDER BY Invoice_No";
					break;
				
				case DataProviderType.Oledb:
					break;
			}
			DataSet dsQryData = (DataSet)dbCon.ExecuteQuery(strQry,queryParams,ReturnType.DataSetType);
			return dsQryData;
		}

		public static DataSet InvoiceQuery(String strAppID,String strEnterpriseID,String custID,Cambro.Web.DbCombo.ServerMethodArgs args)
		{
			String sTempWhere=null;
			sTempWhere= " and s.Invoice_No like '%"+Utility.ReplaceSingleQuote(args.Query)+"%' and s.payerid = '"+custID+"' and i.invoice_status in ('A','L') ";

			DataProviderType dbProvider = DbConnectionManager.GetInstance().GetDbProviderType(strAppID,strEnterpriseID);
			ArrayList queryParams = new ArrayList();
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			//			IDbDataParameter dataParam = dbCon.MakeParam("@Query",DbType.String,100,ParameterDirection.Input,"%"+args.Query+"%");
			//			queryParams.Add(dataParam);			
			
			String strQry=null;

			switch (dbProvider)
			{
				case DataProviderType.Sql:	
					strQry = " SELECT distinct TOP "+args.Top+" i.Invoice_No AS DbComboText, i.Invoice_No AS DbComboValue FROM Invoice i";// edit by ching
					strQry+= " INNER JOIN SHIPMENT s ON s.applicationID=i.applicationID and s.EnterpriseID=i.EnterpriseID and s.Invoice_No=i.Invoice_No";
					strQry+= " WHERE s.applicationID='"+strAppID+"' and s.EnterpriseID='"+strEnterpriseID+"' "+sTempWhere+" ORDER BY i.Invoice_No";
					break;

				case DataProviderType.Oracle:
					strQry = " SELECT Invoice_No AS DbComboText, Invoice_No AS DbComboValue FROM Invoice where Rownum <="+args.Top;
					strQry+= " and applicationID='"+strAppID+"' and EnterpriseID='"+strEnterpriseID+"' "+sTempWhere+" ORDER BY Invoice_No";
					break;
				
				case DataProviderType.Oledb:
					break;
			}
			DataSet dsQryData = (DataSet)dbCon.ExecuteQuery(strQry,queryParams,ReturnType.DataSetType);
			return dsQryData;
		}

		public static DataSet CustomerIDQuery(String strAppID,String strEnterpriseID,Cambro.Web.DbCombo.ServerMethodArgs args)
		{
			String sTempWhere=null;
			sTempWhere= " and custid like '%"+Utility.ReplaceSingleQuote(args.Query)+"%'";

			DataProviderType dbProvider = DbConnectionManager.GetInstance().GetDbProviderType(strAppID,strEnterpriseID);
			ArrayList queryParams = new ArrayList();
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);	
			
			String strQry=null;

			switch (dbProvider)
			{
				case DataProviderType.Sql:	
					strQry = " SELECT TOP "+args.Top+" custid AS DbComboText, custid AS DbComboValue FROM Customer ";
					strQry+= " WHERE applicationID='"+strAppID+"' and EnterpriseID='"+strEnterpriseID+"' "+sTempWhere+" ORDER BY custid";
					break;

				case DataProviderType.Oracle:
					strQry = " SELECT custid AS DbComboText, custid AS DbComboValue FROM Customer where Rownum <="+args.Top;
					strQry+= " and applicationID='"+strAppID+"' and EnterpriseID='"+strEnterpriseID+"' "+sTempWhere+" ORDER BY custid";
					break;
				
				case DataProviderType.Oledb:
					break;
			}
			DataSet dsQryData = (DataSet)dbCon.ExecuteQuery(strQry,queryParams,ReturnType.DataSetType);
			return dsQryData;
		}

		public static DataSet StatusQuery(String strAppID,String strEnterpriseID,String code_id,String culture,Cambro.Web.DbCombo.ServerMethodArgs args)
		{
			String sTempWhere=null;
			sTempWhere= " and code_text like '%"+Utility.ReplaceSingleQuote(args.Query)+"%' and codeid ='"+code_id+"' and culture = '"+culture+"' and code_str_value <>'P'";

			DataProviderType dbProvider = DbConnectionManager.GetInstance().GetDbProviderType(strAppID,strEnterpriseID);
			ArrayList queryParams = new ArrayList();
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);	
			
			String strQry=null;

			switch (dbProvider)
			{
				case DataProviderType.Sql:	
					strQry = " SELECT TOP "+args.Top+" code_text AS DbComboText, code_str_value AS DbComboValue FROM Core_System_Code ";
					strQry+= " WHERE applicationID='"+strAppID+"' "+sTempWhere+" ORDER BY codeid";
					break;

				case DataProviderType.Oracle:
					strQry = " SELECT code_text AS DbComboText, code_str_value AS DbComboValue FROM Core_System_Code where Rownum <="+args.Top;
					strQry+= " and applicationID='"+strAppID+"' "+sTempWhere+" ORDER BY codeid";
					break;
				
				case DataProviderType.Oledb:
					break;
			}
			DataSet dsQryData = (DataSet)dbCon.ExecuteQuery(strQry,queryParams,ReturnType.DataSetType);
			return dsQryData;
		}

		public static DataSet ChargeReasonCodeQuery(String strAppID,String strEnterpriseID,String code_id,String culture,Cambro.Web.DbCombo.ServerMethodArgs args)
		{
			String sTempWhere=null;
			culture = "en-US";
			sTempWhere= " and code_str_value like '%"+Utility.ReplaceSingleQuote(args.Query)+"%' and codeid ='"+code_id+"' and culture = '"+culture+"' ";

			DataProviderType dbProvider = DbConnectionManager.GetInstance().GetDbProviderType(strAppID,strEnterpriseID);
			ArrayList queryParams = new ArrayList();
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);	
			
			String strQry=null;

			switch (dbProvider)
			{
				case DataProviderType.Sql:	
					strQry = " SELECT TOP "+args.Top+" code_str_value AS DbComboText, code_num_value AS DbComboValue FROM Core_System_Code ";
					strQry+= " WHERE applicationID='"+strAppID+"' "+sTempWhere+" ORDER BY codeid";
					break;

				case DataProviderType.Oracle:
					strQry = " SELECT code_str_value AS DbComboText, code_num_value AS DbComboValue FROM Core_System_Code where Rownum <="+args.Top;
					strQry+= " and applicationID='"+strAppID+"' "+sTempWhere+" ORDER BY codeid";
					break;
				
				case DataProviderType.Oledb:
					break;
			}
			DataSet dsQryData = (DataSet)dbCon.ExecuteQuery(strQry,queryParams,ReturnType.DataSetType);
			return dsQryData;
		}

		public static DataSet ChargeReasonCodeQuery(String strAppID,String strEnterpriseID,String code_id,String culture)
		{
			String sTempWhere=null;
			culture = "en-US";
			sTempWhere= " and codeid ='"+code_id+"' and culture = '"+culture+"' ";

			DataProviderType dbProvider = DbConnectionManager.GetInstance().GetDbProviderType(strAppID,strEnterpriseID);
			ArrayList queryParams = new ArrayList();
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);	
			
			String strQry=null;

			switch (dbProvider)
			{
				case DataProviderType.Sql:	
					strQry = " SELECT code_str_value AS DbComboText, code_num_value AS DbComboValue FROM Core_System_Code ";
					strQry+= " WHERE applicationID='"+strAppID+"' "+sTempWhere+" ORDER BY codeid";
					break;

				case DataProviderType.Oracle:
					strQry = " SELECT code_str_value AS DbComboText, code_num_value AS DbComboValue FROM Core_System_Code where ";
					strQry+= " applicationID='"+strAppID+"' "+sTempWhere+" ORDER BY codeid";
					break;
				
				case DataProviderType.Oledb:
					break;
			}
			DataSet dsQryData = (DataSet)dbCon.ExecuteQuery(strQry,queryParams,ReturnType.DataSetType);
			return dsQryData;
		}

		public static DataSet ConsignmentQuery(String strAppID,String strEnterpriseID,String strInvoiceNo, Cambro.Web.DbCombo.ServerMethodArgs args)
		{
			String sTempWhere=null;
			sTempWhere= " and Consignment_No like '"+Utility.ReplaceSingleQuote(args.Query)+"%'";

			DataProviderType dbProvider = DbConnectionManager.GetInstance().GetDbProviderType(strAppID,strEnterpriseID);
			ArrayList queryParams = new ArrayList();
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
//			IDbDataParameter dataParam = dbCon.MakeParam("@Query",DbType.String,100,ParameterDirection.Input,"%"+Utility.ReplaceSingleQuote(args.Query)+"%");
//			queryParams.Add(dataParam);			
			
			String strQry=null;

			switch (dbProvider)
			{
				case DataProviderType.Sql:	
					strQry = " SELECT Distinct TOP "+args.Top+" Consignment_No AS DbComboText, Consignment_No AS DbComboValue FROM shipment ";//edit by ching		invoice_detail >> shipment			
					strQry+= " WHERE applicationID='"+strAppID+"' and EnterpriseID='"+strEnterpriseID+"' "+sTempWhere+" and Invoice_No = '"+Utility.ReplaceSingleQuote(strInvoiceNo)+"' ORDER BY Consignment_No";
					break;

				case DataProviderType.Oracle:
					strQry = " SELECT Distinct Consignment_No AS DbComboText, Consignment_No AS DbComboValue FROM shipment where rownum <="+args.Top;
					strQry+= " and applicationID='"+strAppID+"' and EnterpriseID='"+strEnterpriseID+"' "+sTempWhere+" and Invoice_No = '"+Utility.ReplaceSingleQuote(strInvoiceNo)+"' ORDER BY Consignment_No";
					break;
				
				case DataProviderType.Oledb:
					break;
			}
			DataSet dsQryData = (DataSet)dbCon.ExecuteQuery(strQry,queryParams,ReturnType.DataSetType);
			return dsQryData;
		}

		public static DataSet CommodityCodeQuery(String strAppID,String strEnterpriseID, Cambro.Web.DbCombo.ServerMethodArgs args)
		{
			String sTempWhere=null;
			sTempWhere= " and commodity_code like '%"+Utility.ReplaceSingleQuote(args.Query)+"%'";

			DataProviderType dbProvider = DbConnectionManager.GetInstance().GetDbProviderType(strAppID,strEnterpriseID);
			ArrayList queryParams = new ArrayList();
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
//			IDbDataParameter dataParam = dbCon.MakeParam("@Query",DbType.String,12,ParameterDirection.Input,"%"+Utility.ReplaceSingleQuote(args.Query)+"%");
//			queryParams.Add(dataParam);			
			
			String strQry=null;

			switch (dbProvider)
			{
				case DataProviderType.Sql:	
					strQry = " SELECT DISTINCT TOP "+args.Top+"commodity_code AS DbComboText, commodity_code AS DbComboValue FROM commodity";
					strQry+= " WHERE applicationID='"+strAppID+"' and EnterpriseID='"+strEnterpriseID+"' "+sTempWhere +" ORDER BY commodity_code";
					break;

				case DataProviderType.Oracle:
					strQry = " SELECT DISTINCT commodity_code AS DbComboText, commodity_code AS DbComboValue FROM commodity where rownum <="+args.Top;
					strQry+= " and applicationID='"+strAppID+"' and EnterpriseID='"+strEnterpriseID+"' "+sTempWhere +" ORDER BY commodity_code";
					break;
				
				case DataProviderType.Oledb:
					break;
			}

			DataSet dsQryData = (DataSet)dbCon.ExecuteQuery(strQry,queryParams,ReturnType.DataSetType);
			return dsQryData; 
		}

		public static DataSet CommodityTypeQuery(String strAppID,String strEnterpriseID, Cambro.Web.DbCombo.ServerMethodArgs args)
		{
			String sTempWhere=null;
			sTempWhere= " and commodity_Type like '%"+Utility.ReplaceSingleQuote(args.Query)+"%'";

			DataProviderType dbProvider = DbConnectionManager.GetInstance().GetDbProviderType(strAppID,strEnterpriseID);
			ArrayList queryParams = new ArrayList();
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
//			IDbDataParameter dataParam = dbCon.MakeParam("@Query",DbType.String,12,ParameterDirection.Input,"%"+Utility.ReplaceSingleQuote(args.Query)+"%");
//			queryParams.Add(dataParam);			
			
			String strQry=null;

			switch (dbProvider)
			{
				case DataProviderType.Sql:	
					strQry = " SELECT DISTINCT TOP "+args.Top+"commodity_Type AS DbComboText, commodity_Type AS DbComboValue FROM commodity";
					strQry+= " WHERE applicationID='"+strAppID+"' and EnterpriseID='"+strEnterpriseID+"' "+sTempWhere +"  ORDER BY commodity_Type";
					break;

				case DataProviderType.Oracle:
					strQry = " SELECT distinct commodity_Type AS DbComboText, commodity_Type AS DbComboValue FROM commodity where rownum <="+args.Top;
					strQry+= " and applicationID='"+strAppID+"' and EnterpriseID='"+strEnterpriseID+"' "+sTempWhere +" ORDER BY commodity_Type";
					break;
				
				case DataProviderType.Oledb:
					break;
			}
			
			DataSet dsQryData = (DataSet)dbCon.ExecuteQuery(strQry,queryParams,ReturnType.DataSetType);
			return dsQryData; 
		}

		public static DataSet IndustrialCodeQuery(String strAppID,String strEnterpriseID, Cambro.Web.DbCombo.ServerMethodArgs args)
		{
			String sTempWhere=null;
			sTempWhere= " and industrial_sector_code like '%"+Utility.ReplaceSingleQuote(args.Query)+"%'";

			DataProviderType dbProvider = DbConnectionManager.GetInstance().GetDbProviderType(strAppID,strEnterpriseID);
			ArrayList queryParams = new ArrayList();
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
//			IDbDataParameter dataParam = dbCon.MakeParam("@Query",DbType.String,12,ParameterDirection.Input,"%"+Utility.ReplaceSingleQuote(args.Query)+"%");
//			queryParams.Add(dataParam);

			String strQry=null;

			switch (dbProvider)
			{
				case DataProviderType.Sql:	
					strQry = " SELECT DISTINCT TOP "+args.Top+"industrial_sector_code AS DbComboText, industrial_sector_code AS DbComboValue FROM industrial_sector";
					strQry+= " WHERE applicationID='"+strAppID+"' and EnterpriseID='"+strEnterpriseID+"' "+sTempWhere+" ORDER BY industrial_sector_code";
					break;

				case DataProviderType.Oracle:
					strQry = " SELECT  industrial_sector_code AS DbComboText, industrial_sector_code AS DbComboValue FROM industrial_sector where rownum <="+args.Top;
					strQry+= " and applicationID='"+strAppID+"' and EnterpriseID='"+strEnterpriseID+"' "+sTempWhere+" ORDER BY industrial_sector_code";
					break;
				
				case DataProviderType.Oledb:
					break;
			}
			
			DataSet dsQryData = (DataSet)dbCon.ExecuteQuery(strQry,queryParams,ReturnType.DataSetType);
			return dsQryData; 
		}

		public static DataSet VasCodeQuery(String strAppID,String strEnterpriseID, Cambro.Web.DbCombo.ServerMethodArgs args)
		{
			String sTempWhere=null;
			sTempWhere= " and vas_code like '%"+Utility.ReplaceSingleQuote(args.Query)+"%'";

			DataProviderType dbProvider = DbConnectionManager.GetInstance().GetDbProviderType(strAppID,strEnterpriseID);
			ArrayList queryParams = new ArrayList();
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
//			IDbDataParameter dataParam = dbCon.MakeParam("@Query",DbType.String,12,ParameterDirection.Input,"%"+Utility.ReplaceSingleQuote(args.Query)+"%");
//			queryParams.Add(dataParam);
			
			String strQry=null;

			switch (dbProvider)
			{
				case DataProviderType.Sql:	
					strQry = " SELECT DISTINCT TOP "+args.Top+" vas_code AS DbComboText, vas_code AS DbComboValue FROM vas";
					strQry+= " WHERE applicationID='"+strAppID+"' and EnterpriseID='"+strEnterpriseID+"' "+sTempWhere+" ORDER BY vas_code";
					break;

				case DataProviderType.Oracle:
					strQry = " SELECT DISTINCT vas_code AS DbComboText, vas_code AS DbComboValue FROM vas where Rownum <="+args.Top;
					strQry+= " and applicationID='"+strAppID+"' and EnterpriseID='"+strEnterpriseID+"' "+sTempWhere+" ORDER BY vas_code";					
					break;		

				case DataProviderType.Oledb:
					break;
			}
			
			DataSet dsQryData = (DataSet)dbCon.ExecuteQuery(strQry,queryParams,ReturnType.DataSetType);
			return dsQryData; 
		}

		public static DataSet ZoneCodeQuery(String strAppID,String strEnterpriseID, Cambro.Web.DbCombo.ServerMethodArgs args)
		{
			String sTempWhere=null;
			sTempWhere= " and zone_code like '%"+Utility.ReplaceSingleQuote(args.Query)+"%'";

			DataProviderType dbProvider = DbConnectionManager.GetInstance().GetDbProviderType(strAppID,strEnterpriseID);
			ArrayList queryParams = new ArrayList();
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
//			IDbDataParameter dataParam = dbCon.MakeParam("@Query",DbType.String,12,ParameterDirection.Input,"%"+Utility.ReplaceSingleQuote(args.Query)+"%");
//			queryParams.Add(dataParam);
			
			String strQry=null;

			switch (dbProvider)
			{
				case DataProviderType.Sql:	
					strQry = " SELECT DISTINCT TOP "+args.Top+" zone_code AS DbComboText, zone_code AS DbComboValue FROM zipcode";
					strQry+= " WHERE applicationID='"+strAppID+"' and EnterpriseID='"+strEnterpriseID+"' "+sTempWhere+" ORDER BY zone_code";
					break;

				case DataProviderType.Oracle:
					strQry = " SELECT DISTINCT zone_code AS DbComboText, zone_code AS DbComboValue FROM zipcode where rownum <="+args.Top;
					strQry+= " and applicationID='"+strAppID+"' and EnterpriseID='"+strEnterpriseID+"' "+sTempWhere+" ORDER BY zone_code";				
					break;

				case DataProviderType.Oledb:
					break;
			}
			
			DataSet dsQryData = (DataSet)dbCon.ExecuteQuery(strQry,queryParams,ReturnType.DataSetType);
			return dsQryData; 
		}
			
		public static DataSet StateNameQuery(String strAppID,String strEnterpriseID,Cambro.Web.DbCombo.ServerMethodArgs args,String strStateName)
		{
			String sTempWhere=null;
			sTempWhere= " and state_name like '%"+Utility.ReplaceSingleQuote(args.Query)+"%'";

			DataProviderType dbProvider = DbConnectionManager.GetInstance().GetDbProviderType(strAppID,strEnterpriseID);
			ArrayList queryParams = new ArrayList();
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
//			IDbDataParameter dataParam = dbCon.MakeParam("@Query",DbType.String,12,ParameterDirection.Input,"%"+Utility.ReplaceSingleQuote(args.Query)+"%");
//			queryParams.Add(dataParam);
			
			String strQry=null;

			switch (dbProvider)
			{
				case DataProviderType.Sql:	
					strQry = " SELECT DISTINCT TOP "+args.Top+" state_name AS DbComboText, state_name AS DbComboValue FROM state ";
					strQry+= " WHERE applicationID='"+strAppID+"' and EnterpriseID='"+strEnterpriseID+"' and country like '%"+Utility.ReplaceSingleQuote(strStateName)+"%' "+sTempWhere+" ORDER BY state_name";
					break;

				case DataProviderType.Oracle:
					strQry = " SELECT DISTINCT state_name AS DbComboText, state_name AS DbComboValue FROM state where rownum <="+ args.Top;
					strQry+= " and applicationID='"+strAppID+"' and EnterpriseID='"+strEnterpriseID+"' and country like '%"+Utility.ReplaceSingleQuote(strStateName)+"%' "+sTempWhere+" ORDER BY state_name";
					break;

				case DataProviderType.Oledb:
					break;
			}
			
			DataSet dsQryData = (DataSet)dbCon.ExecuteQuery(strQry,queryParams,ReturnType.DataSetType);
			return dsQryData; 
		}

		public static DataSet CustomerProfilePayerIDQuery(String strAppID,String strEnterpriseID,Cambro.Web.DbCombo.ServerMethodArgs args, String strPayerType, String strPayerName)
		{
			DataProviderType dbProvider = DbConnectionManager.GetInstance().GetDbProviderType(strAppID,strEnterpriseID);
			ArrayList queryParams = new ArrayList();
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			//			IDbDataParameter dataParam = dbCon.MakeParam("@Query",DbType.String,20,ParameterDirection.Input,"%"+Utility.ReplaceSingleQuote(args.Query)+"%");
			//			queryParams.Add(dataParam);			
			
			String strQry=null;

			switch (dbProvider)
			{
				case DataProviderType.Sql :
					if (strPayerType=="A")
					{
						strQry = " SELECT distinct TOP "+args.Top+" AgentID AS DbComboText, AgentID AS DbComboValue FROM v_AgentProfile  ";
						strQry+= " WHERE applicationID='"+strAppID+"' and EnterpriseID='"+strEnterpriseID+"' and AgentID LIKE '%"+Utility.ReplaceSingleQuote(args.Query)+"%'";
						strQry+= " and  quotation_status = 'Q' or quotation_status = 'D' and Agent_Name LIKE '%"+Utility.ReplaceSingleQuote(strPayerName)+"%' ORDER BY AgentID ";
					}
					else if (strPayerType=="C")
					{
						strQry = " SELECT distinct TOP "+args.Top+" CustID AS DbComboText, CustID AS DbComboValue FROM v_CustomerProfile ";
						strQry+= " WHERE applicationID='"+strAppID+"' and EnterpriseID='"+strEnterpriseID+"' and CustID LIKE '%"+Utility.ReplaceSingleQuote(args.Query)+"%'";
						strQry+= " and quotation_status = 'Q' or quotation_status = 'D' and Cust_Name LIKE '%"+Utility.ReplaceSingleQuote(strPayerName)+"%' ORDER BY CustID ";
					}
					break;

				case DataProviderType.Oracle:
					if (strPayerType=="A")
					{
						strQry = " SELECT distinct AgentID AS DbComboText, AgentID AS DbComboValue FROM v_AgentProfile  where rownum <=" + args.Top;
						strQry+= " and applicationID='"+strAppID+"' and EnterpriseID='"+strEnterpriseID+"' and AgentID LIKE '%"+Utility.ReplaceSingleQuote(args.Query)+"%'";
						strQry+= " and quotation_status = 'Q' or quotation_status = 'D'and Agent_Name LIKE '%"+Utility.ReplaceSingleQuote(strPayerName)+"%' ORDER BY AgentID ";
					}
					else if (strPayerType=="C")
					{
						strQry = " SELECT distinct CustID AS DbComboText, CustID AS DbComboValue FROM v_CustomerProfile where Rownum <="+args.Top;
						strQry+= " and applicationID='"+strAppID+"' and EnterpriseID='"+strEnterpriseID+"' and CustID LIKE '%"+Utility.ReplaceSingleQuote(args.Query)+"%'";
						strQry+= " and quotation_status = 'Q' or quotation_status = 'D'and Cust_Name LIKE '%"+Utility.ReplaceSingleQuote(strPayerName)+"%' ORDER BY CustID ";
					}
					break;
				
				case DataProviderType.Oledb:
					break;
			}		

			DataSet dsQryData = (DataSet)dbCon.ExecuteQuery(strQry,queryParams,ReturnType.DataSetType);
			return dsQryData;
		}

		public static DataSet ConveyanceCodeQuery(String strAppID,String strEnterpriseID, Cambro.Web.DbCombo.ServerMethodArgs args)
		{
			String sTempWhere=null;
			sTempWhere= " and conveyance_code like '%"+Utility.ReplaceSingleQuote(args.Query)+"%'"; 
			DataProviderType dbProvider = DbConnectionManager.GetInstance().GetDbProviderType(strAppID,strEnterpriseID);
			ArrayList queryParams = new ArrayList();
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			
			StringBuilder strBuilder = new StringBuilder();

			switch (dbProvider)
			{
				case DataProviderType.Sql :
					strBuilder.Append("SELECT TOP "+args.Top+" Conveyance_code AS DbComboText, conveyance_code AS DbComboValue FROM Conveyance WHERE applicationid = '");
					strBuilder.Append(strAppID + "' and enterpriseid = '" + strEnterpriseID + "' "+sTempWhere);	
									
					break;	
			
				case DataProviderType.Oracle:
					strBuilder.Append("SELECT Conveyance_code AS DbComboText, Conveyance_code AS DbComboValue FROM Conveyance WHERE rownum < = "+ args.Top );
					strBuilder.Append(" and applicationid = '"+ strAppID + "' and enterpriseid = '" + strEnterpriseID + "' "+sTempWhere);
					
					break;

				case DataProviderType.Oledb:
					break;
			}		

			strBuilder.Append(" ORDER BY Conveyance_code");

			String strQry = strBuilder.ToString();
			DataSet dsQryData = (DataSet)dbCon.ExecuteQuery(strQry,queryParams,ReturnType.DataSetType);
			return dsQryData;
		}

		public static DataSet AgentConveyanceQuery(String strAppID,String strEnterpriseID, Cambro.Web.DbCombo.ServerMethodArgs args, String strAgentID)
		{
			String sTempWhere=null;
			sTempWhere= " and conveyance_code like '%"+Utility.ReplaceSingleQuote(args.Query)+"%' and AgentId like '%"+strAgentID+"%'";
			DataProviderType dbProvider = DbConnectionManager.GetInstance().GetDbProviderType(strAppID,strEnterpriseID);
			ArrayList queryParams = new ArrayList();
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			
			StringBuilder strBuilder = new StringBuilder();

			switch (dbProvider)
			{
				case DataProviderType.Sql :
					strBuilder.Append("SELECT TOP "+args.Top+" Conveyance_code AS DbComboText, conveyance_code AS DbComboValue FROM Agent_Conveyance WHERE applicationid = '");
					strBuilder.Append(strAppID + "' and enterpriseid = '" + strEnterpriseID + "' "+sTempWhere);	
									
					break;	
			
				case DataProviderType.Oracle:
					strBuilder.Append("SELECT Conveyance_code AS DbComboText, Conveyance_code AS DbComboValue FROM Agent_Conveyance WHERE rownum < = "+ args.Top );
					strBuilder.Append(" and applicationid = '"+ strAppID + "' and enterpriseid = '" + strEnterpriseID + "' "+sTempWhere);
					
					break;

				case DataProviderType.Oledb:
					break;
			}		

			strBuilder.Append(" ORDER BY Conveyance_code");

			String strQry = strBuilder.ToString();
			DataSet dsQryData = (DataSet)dbCon.ExecuteQuery(strQry,queryParams,ReturnType.DataSetType);
			return dsQryData;
		}

		public static DataSet OriginZoneQuery(String strAppID,String strEnterpriseID,Cambro.Web.DbCombo.ServerMethodArgs args, String strAgentId)
		{
			String sTempWhere=null;
			sTempWhere= " and zone_code like '%"+Utility.ReplaceSingleQuote(args.Query)+"%' and AgentId like '%"+strAgentId+"%'";

			DataProviderType dbProvider = DbConnectionManager.GetInstance().GetDbProviderType(strAppID,strEnterpriseID);
			ArrayList queryParams = new ArrayList();
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			
			String strQry=null;			
			switch (dbProvider)
			{
				case DataProviderType.Sql:
					strQry = " SELECT DISTINCT TOP "+args.Top+" zone_code AS DbComboText, zone_code AS DbComboValue FROM Agent_zone ";
					strQry+= " WHERE applicationID='"+strAppID+"' and EnterpriseID='"+strEnterpriseID+"' "+sTempWhere+" ORDER BY zone_code";
					break;
				
				case DataProviderType.Oracle:
					strQry = " SELECT DISTINCT Zone_code AS DbComboText, zone_code AS DbComboValue FROM Agent_zone where rownum <= " + args.Top;
					strQry+= " and applicationID='"+strAppID+"' and EnterpriseID='"+strEnterpriseID+"' "+sTempWhere+" ORDER BY zone_code";
					break;
				
				case DataProviderType.Oledb:
					break;
			}		
			DataSet dsQryData = (DataSet)dbCon.ExecuteQuery(strQry,queryParams,ReturnType.DataSetType);
			return dsQryData; 
		}

		public static DataSet DestinationZoneQuery(String strAppID,String strEnterpriseID,Cambro.Web.DbCombo.ServerMethodArgs args, String strAgentId)
		{
			String sTempWhere=null;
			sTempWhere= " and Zone_Code like '%"+Utility.ReplaceSingleQuote(args.Query)+"%' and AgentId like '%"+strAgentId+"%'";

			DataProviderType dbProvider = DbConnectionManager.GetInstance().GetDbProviderType(strAppID,strEnterpriseID);
			ArrayList queryParams = new ArrayList();
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
		
			String strQry=null;			
			switch (dbProvider)
			{
				case DataProviderType.Sql:
					strQry = " SELECT DISTINCT TOP "+args.Top+" zone_code AS DbComboText, zone_code AS DbComboValue FROM Agent_Zone ";
					strQry+= " WHERE applicationID='"+strAppID+"' and EnterpriseID='"+strEnterpriseID+"' "+sTempWhere+" ORDER BY Zone_code";					
					break;

				case DataProviderType.Oracle:
					strQry = " SELECT DISTINCT Zone_code AS DbComboText, zone_code AS DbComboValue FROM Agent_Zone where rownum <= "+ args.Top ;
					strQry+= " and applicationID='"+strAppID+"' and EnterpriseID='"+strEnterpriseID+"' "+sTempWhere+" ORDER BY zone_code";
					break;

				case DataProviderType.Oledb:
					break;
			}

			DataSet dsQryData = (DataSet)dbCon.ExecuteQuery(strQry,queryParams,ReturnType.DataSetType);
			return dsQryData;
		}


		public static DataSet DistributionCenterQuery(String strAppID,String strEnterpriseID, Cambro.Web.DbCombo.ServerMethodArgs args)
		{
			DataProviderType dbProvider = DbConnectionManager.GetInstance().GetDbProviderType(strAppID, strEnterpriseID);
			ArrayList queryParams = new ArrayList();
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);

			String strQry=null;
			switch (dbProvider)
			{
				case DataProviderType.Sql :
					strQry = " SELECT TOP "+args.Top+" origin_code AS DbComboText, origin_code AS DbComboValue FROM Distribution_center ";
					strQry+= " WHERE applicationID='"+strAppID+"' and EnterpriseID='"+strEnterpriseID+"' " + " ORDER BY origin_code";
					break;	
			
				case DataProviderType.Oracle:
					strQry = " SELECT origin_code AS DbComboText, origin_code AS DbComboValue FROM Distribution_center where rownum <= " + args.Top;
					strQry+= " and applicationID='"+strAppID+"' and EnterpriseID='"+strEnterpriseID+"' " + " ORDER BY origin_code";					
					break;

				case DataProviderType.Oledb:
					break;
			}			


			DataSet dsQryData = (DataSet)dbCon.ExecuteQuery(strQry,queryParams,ReturnType.DataSetType);
			return dsQryData;
		}

		public static DataSet DistributionCenterQuery(String strAppID,String strEnterpriseID, String strUserLocationID, Cambro.Web.DbCombo.ServerMethodArgs args)
		{
			DataProviderType dbProvider = DbConnectionManager.GetInstance().GetDbProviderType(strAppID, strEnterpriseID);
			ArrayList queryParams = new ArrayList();
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);

			String strQry=null;
			switch (dbProvider)
			{
				case DataProviderType.Sql :
					strQry = " SELECT TOP "+args.Top+" origin_code AS DbComboText, origin_code AS DbComboValue FROM Distribution_center ";
					strQry+= " WHERE applicationID='"+strAppID+"' and EnterpriseID='"+strEnterpriseID+"' ";
					strQry+= " and origin_code = '"+strUserLocationID+"' ORDER BY origin_code";
					break;	
			
				case DataProviderType.Oracle:
					strQry = " SELECT origin_code AS DbComboText, origin_code AS DbComboValue FROM Distribution_center where rownum <= " + args.Top;
					strQry+= " and applicationID='"+strAppID+"' and EnterpriseID='"+strEnterpriseID+"' " + " ORDER BY origin_code";					
					break;

				case DataProviderType.Oledb:
					break;
			}			


			DataSet dsQryData = (DataSet)dbCon.ExecuteQuery(strQry,queryParams,ReturnType.DataSetType);
			return dsQryData;
		}

		public static DataSet MasterAccountQuery(String strAppID,String strEnterpriseID, Cambro.Web.DbCombo.ServerMethodArgs args)
		{
			DataProviderType dbProvider = DbConnectionManager.GetInstance().GetDbProviderType(strAppID, strEnterpriseID);
			ArrayList queryParams = new ArrayList();
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);

			String strQry=null;
			String strTempWhere= " and code_text like '%"+Utility.ReplaceSingleQuote(args.Query)+"%'";

			switch (dbProvider)
			{
				case DataProviderType.Sql :
					strQry = " SELECT DISTINCT TOP "+args.Top+" code_Text AS DbComboText, code_str_value AS DbComboValue FROM core_system_code ";
					strQry+= " WHERE applicationID='"+strAppID+"' AND codeid='master_account' "+strTempWhere+" ORDER BY code_text";
					break;	
			
				case DataProviderType.Oracle:
					strQry = " SELECT code_Text AS DbComboText, code_str_value AS DbComboValue FROM Customer where rownum <= " + args.Top;
					strQry+= " and applicationID='"+strAppID+"' AND codeid='master_account' "+strTempWhere+" ORDER BY code_text";
					break;

				case DataProviderType.Oledb:
					break;
			}			


			DataSet dsQryData = (DataSet)dbCon.ExecuteQuery(strQry,queryParams,ReturnType.DataSetType);
			return dsQryData;
		}



		public static DataSet DiscountBandQuery(String strAppID,String strEnterpriseID, Cambro.Web.DbCombo.ServerMethodArgs args)
		{
			DataProviderType dbProvider = DbConnectionManager.GetInstance().GetDbProviderType(strAppID, strEnterpriseID);
			ArrayList queryParams = new ArrayList();
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);

			String strQry=null;
			switch (dbProvider)
			{
				case DataProviderType.Sql :
					strQry = " SELECT DISTINCT TOP "+args.Top+" discount_band AS DbComboText, discount_band AS DbComboValue FROM Customer ";
					strQry+= " WHERE applicationID='"+strAppID+"' and EnterpriseID='"+strEnterpriseID+"' " + " AND discount_band is not null ORDER BY discount_band";
					break;	
			
				case DataProviderType.Oracle:
					strQry = " SELECT discount_band AS DbComboText, discount_band AS DbComboValue FROM Customer where rownum <= " + args.Top;
					strQry+= " and applicationID='"+strAppID+"' and EnterpriseID='"+strEnterpriseID+"' " + " ORDER BY discount_band";					
					break;

				case DataProviderType.Oledb:
					break;
			}			


			DataSet dsQryData = (DataSet)dbCon.ExecuteQuery(strQry,queryParams,ReturnType.DataSetType);
			return dsQryData;
		}

		//by sittichai
		public static DataSet UserLocationQuery(String strAppID,String strEnterpriseID )
		{
			DataProviderType dbProvider = DbConnectionManager.GetInstance().GetDbProviderType(strAppID, strEnterpriseID);
			ArrayList queryParams = new ArrayList();
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);

			String strQry=null;
			switch (dbProvider)
			{
				case DataProviderType.Sql :
					strQry = " SELECT origin_code FROM Distribution_center ";
					strQry+= " WHERE applicationID='"+strAppID+"' and EnterpriseID='"+strEnterpriseID+"' " + " ORDER BY origin_code";
					break;	
			
				case DataProviderType.Oracle:
					strQry = " SELECT origin_code  FROM Distribution_center ";
					strQry+= " and applicationID='"+strAppID+"' and EnterpriseID='"+strEnterpriseID+"' " + " ORDER BY origin_code";					
					break;

				case DataProviderType.Oledb:
					break;
			}			


			DataSet dsQryData = (DataSet)dbCon.ExecuteQuery(strQry,queryParams,ReturnType.DataSetType);
			return dsQryData;
		}

		//by sittichai
		public static DataSet CustomerAccountQuery(String strAppID,String strEnterpriseID )
		{
			DataProviderType dbProvider = DbConnectionManager.GetInstance().GetDbProviderType(strAppID, strEnterpriseID);
			ArrayList queryParams = new ArrayList();
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);

			String strQry=null;
			switch (dbProvider)
			{
				case DataProviderType.Sql :
					strQry = " SELECT custid FROM Customer ";
					strQry+= " WHERE applicationID='"+strAppID+"' and EnterpriseID='"+strEnterpriseID+"' " + " ORDER BY custid";
					break;	
			
				case DataProviderType.Oracle:
					strQry = " SELECT custid  FROM Customer ";
					strQry+= " and applicationID='"+strAppID+"' and EnterpriseID='"+strEnterpriseID+"' " + " ORDER BY custid";					
					break;

				case DataProviderType.Oledb:
					break;
			}			


			DataSet dsQryData = (DataSet)dbCon.ExecuteQuery(strQry,queryParams,ReturnType.DataSetType);
			return dsQryData;
		}

		public static DataSet SalesmanQuery(String strAppID,String strEnterpriseID, Cambro.Web.DbCombo.ServerMethodArgs args)
		{
			DataProviderType dbProvider = DbConnectionManager.GetInstance().GetDbProviderType(strAppID, strEnterpriseID);
			ArrayList queryParams = new ArrayList();
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);

			String strQry=null;
			switch (dbProvider)
			{
				case DataProviderType.Sql :
					strQry = " SELECT TOP "+args.Top+" salesmanid AS DbComboText, salesmanid AS DbComboValue FROM Salesman ";
					strQry+= " WHERE applicationID='"+strAppID+"' and EnterpriseID='"+strEnterpriseID+"' " + " ORDER BY salesmanid";
					break;	
			
				case DataProviderType.Oracle:
					strQry = " SELECT salesmanid AS DbComboText, salesmanid AS DbComboValue FROM Salesman where rownum <= " + args.Top;
					strQry+= " and applicationID='"+strAppID+"' and EnterpriseID='"+strEnterpriseID+"' " + " ORDER BY salesmanid";					
					break;

				case DataProviderType.Oledb:
					break;
			}			


			DataSet dsQryData = (DataSet)dbCon.ExecuteQuery(strQry,queryParams,ReturnType.DataSetType);
			return dsQryData;
		}

		public static DataSet VersusStatusQuery(String strAppID,String strEnterpriseID, Cambro.Web.DbCombo.ServerMethodArgs args)
		{
			DataProviderType dbProvider = DbConnectionManager.GetInstance().GetDbProviderType(strAppID, strEnterpriseID);
			ArrayList queryParams = new ArrayList();
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);

			String strQry=null;
			switch (dbProvider)
			{
				case DataProviderType.Sql :
					strQry = " SELECT distinct TOP "+args.Top+" status_code AS DbComboText, status_code AS DbComboValue FROM Shipment_Tracking ";
					strQry+= " WHERE applicationID='"+strAppID+"' and EnterpriseID='"+strEnterpriseID+"' " + " ORDER BY status_code";
					break;	

				case DataProviderType.Oracle:
					strQry = " SELECT distinct status_code AS DbComboText, status_code AS DbComboValue FROM Shipment_Tracking where rownum <= " + args.Top;
					strQry+= " and applicationID='"+strAppID+"' and EnterpriseID='"+strEnterpriseID+"' " + " ORDER BY status_code";					
					break;

				case DataProviderType.Oledb:
					break;
			}			


			DataSet dsQryData = (DataSet)dbCon.ExecuteQuery(strQry,queryParams,ReturnType.DataSetType);
			return dsQryData;
		}
		//Nualjan 20-01-2011
		public static DataSet BatchIDQuery(String strAppID,String strEnterpriseID )
		{
			DataProviderType dbProvider = DbConnectionManager.GetInstance().GetDbProviderType(strAppID, strEnterpriseID);
			ArrayList queryParams = new ArrayList();
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);

			String strQry=null;
			switch (dbProvider)
			{
				case DataProviderType.Sql :
					strQry = " SELECT path_code,delivery_type,delivery_type+'|'+path_code as value FROM Delivery_Path ";
					strQry+= " WHERE applicationID='"+strAppID+"' and EnterpriseID='"+strEnterpriseID+"' " ;
					strQry+= " and path_description not like '%PUP%'";
					strQry+= " and path_description  not like '%Pickup%'";
					strQry+= " and path_description  not like '%Pick up%'";
					strQry+= " and delivery_type in('S','L','W','A')";
					strQry+= " ORDER BY path_code";

					break;	
			
				case DataProviderType.Oracle:
					strQry = " SELECT path_code,delivery_type,delivery_type+'|'+path_code as value  FROM Delivery_Path ";
					strQry+= " and applicationID='"+strAppID+"' and EnterpriseID='"+strEnterpriseID+"' ";
					strQry+= " and path_description not like '%PUP%'";
					strQry+= " and path_description  not like '%Pickup%'";
					strQry+= " and path_description  not like '%Pick up%'";
					strQry+= " and delivery_type in('S','L','W')";
					strQry+= " ORDER BY path_code";					
					break;

				case DataProviderType.Oledb:
					break;
			}			


			DataSet dsQryData = (DataSet)dbCon.ExecuteQuery(strQry,queryParams,ReturnType.DataSetType);
			return dsQryData;
		}
		public static DataSet BatchIDQuery(String strAppID,String strEnterpriseID,String location,String statusCode )
		{
			DataProviderType dbProvider = DbConnectionManager.GetInstance().GetDbProviderType(strAppID, strEnterpriseID);
			ArrayList queryParams = new ArrayList();
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);

			String strQry=null;
			switch (dbProvider)
			{
				case DataProviderType.Sql :
					strQry = " SELECT path_code,delivery_type,delivery_type+'|'+path_code as value FROM Delivery_Path ";
					strQry+= " WHERE applicationID='"+strAppID+"' and EnterpriseID='"+strEnterpriseID+"' " ;
					strQry+= " and path_description not like '%PUP%'";
					strQry+= " and path_description  not like '%Pickup%'";
					strQry+= " and path_description  not like '%Pick up%'";
					if(statusCode=="CLS")
					{
					strQry+= " and delivery_type in('W')";
					}
					else
					{
					strQry+= " and delivery_type in('S','L','A')";
					}
					strQry+= " and origin_station ='"+ location +"'";
					strQry+= " ORDER BY path_code";

					break;	
			
				case DataProviderType.Oracle:
					strQry = " SELECT path_code,delivery_type,delivery_type+'|'+path_code as value  FROM Delivery_Path ";
					strQry+= " and applicationID='"+strAppID+"' and EnterpriseID='"+strEnterpriseID+"' ";
					strQry+= " and path_description not like '%PUP%'";
					strQry+= " and path_description  not like '%Pickup%'";
					strQry+= " and path_description  not like '%Pick up%'";
					strQry+= " and delivery_type in('S','L','W')";
					strQry+= " and origin_station ='"+ location +"'";
					strQry+= " ORDER BY path_code";					
					break;

				case DataProviderType.Oledb:
					break;
			}			


			DataSet dsQryData = (DataSet)dbCon.ExecuteQuery(strQry,queryParams,ReturnType.DataSetType);
			return dsQryData;
		}

		public static DataSet TruckIDQuery(String strAppID,String strEnterpriseID,String BMCP,String Batch_ID,int Batch_No)
		{
			DataSet dsCons = null;

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			System.Collections.ArrayList storedParams = new System.Collections.ArrayList();
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@applicationid",strAppID));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@enterpriseid",strEnterpriseID));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@BMCP",BMCP));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@Batch_ID",Batch_ID));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@Batch_No",Batch_No));
			dsCons =(DataSet)dbCon.ExecuteProcedure ("MF_TrucksList",storedParams,common.DAL.ReturnType.DataSetType);

			return dsCons ;
		}


		public static DataSet BatchTypeQuery(String strAppID,String strEnterpriseID,String strBatchID)
		{
			DataProviderType dbProvider = DbConnectionManager.GetInstance().GetDbProviderType(strAppID, strEnterpriseID);
			ArrayList queryParams = new ArrayList();
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);

			String strQry=null;
			switch (dbProvider)
			{
				case DataProviderType.Sql :
					strQry = " SELECT delivery_type FROM Delivery_Path ";
					strQry+= " WHERE applicationID='"+strAppID+"' and EnterpriseID='"+strEnterpriseID+"' and path_code='"+strBatchID+"' " + " ORDER BY delivery_type";
					break;	
			
				case DataProviderType.Oracle:
					strQry = " SELECT delivery_type  FROM Delivery_Path ";
					strQry+= " and applicationID='"+strAppID+"' and EnterpriseID='"+strEnterpriseID+"' and path_code='"+strBatchID+"' " + " ORDER BY delivery_type";					
					break;

				case DataProviderType.Oledb:
					break;
			}			


			DataSet dsQryData = (DataSet)dbCon.ExecuteQuery(strQry,queryParams,ReturnType.DataSetType);
			return dsQryData;
		}

//		public static DataSet DriverIDQuery(String strAppID,String strEnterpriseID)
//		{
//			DataProviderType dbProvider = DbConnectionManager.GetInstance().GetDbProviderType(strAppID, strEnterpriseID);
//			ArrayList queryParams = new ArrayList();
//			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);
//
//			String strQry=null;
//			switch (dbProvider)
//			{
//				case DataProviderType.Sql :
//					strQry = " SELECT Emp_ID, Emp_Name,Emp_ID+' '+Emp_Name Emp_Display FROM mf_staff ";
//					strQry+= " WHERE applicationID='"+strAppID+"' and EnterpriseID='"+strEnterpriseID+"' and Emp_Type ='D' and ((convert(varchar(8),getdate(),112) between convert(varchar(8),isnull(Initial_in_service_date,''),112) and convert(varchar(8),isnull(Separation_Date,getdate()),112) and Out_Of_Service_Date is null and In_Service_Date is null) or (convert(varchar(8),getdate(),112) between convert(varchar(8),isnull(Initial_in_service_date,''),112) and convert(varchar(8),isnull(Out_Of_Service_Date,''),112) and Out_Of_Service_Date is not null and In_Service_Date is null) or (convert(varchar(8),getdate(),112) between convert(varchar(8),isnull(In_Service_Date,''),112) and convert(varchar(8),isnull(Separation_Date,getdate()),112) and Out_Of_Service_Date is not null and In_Service_Date is not null )) " + " ORDER BY Emp_Name";
//					break;	
//			
//				case DataProviderType.Oracle:
//					strQry = " SELECT Emp_ID, Emp_Name,Emp_ID+' '+Emp_Name Emp_Display  FROM mf_staff ";
//					strQry+= " and applicationID='"+strAppID+"' and EnterpriseID='"+strEnterpriseID+"' and Emp_Type ='D' and ((convert(varchar(8),getdate(),112) between convert(varchar(8),isnull(Initial_in_service_date,''),112) and convert(varchar(8),isnull(Separation_Date,getdate()),112) and Out_Of_Service_Date is null and In_Service_Date is null) or (convert(varchar(8),getdate(),112) between convert(varchar(8),isnull(Initial_in_service_date,''),112) and convert(varchar(8),isnull(Out_Of_Service_Date,''),112) and Out_Of_Service_Date is not null and In_Service_Date is null) or (convert(varchar(8),getdate(),112) between convert(varchar(8),isnull(In_Service_Date,''),112) and convert(varchar(8),isnull(Separation_Date,getdate()),112) and Out_Of_Service_Date is not null and In_Service_Date is not null )) " + " ORDER BY Emp_Name";					
//					break;
//
//				case DataProviderType.Oledb:
//					break;
//			}			
//
//
//			DataSet dsQryData = (DataSet)dbCon.ExecuteQuery(strQry,queryParams,ReturnType.DataSetType);
//			return dsQryData;
//		}

		public static DataSet DriverIDQuery(String strAppID,String strEnterpriseID,String BMCP,int Batch_No,String location,String On_Arrival )
		{
			DataSet dsCons = null;

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			System.Collections.ArrayList storedParams = new System.Collections.ArrayList();
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@applicationid",strAppID));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@enterpriseid",strEnterpriseID));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@BMCP",BMCP));			
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@Batch_No",Batch_No));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@Current_Location",location));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@On_Arrival",On_Arrival));

			dsCons =(DataSet)dbCon.ExecuteProcedure ("MF_DriversList",storedParams,common.DAL.ReturnType.DataSetType);

			return dsCons ;
		}

//		public static DataSet DriverIDforFilterQuery(String strAppID,String strEnterpriseID,String base_location)
//		{
//			DataProviderType dbProvider = DbConnectionManager.GetInstance().GetDbProviderType(strAppID, strEnterpriseID);
//			ArrayList queryParams = new ArrayList();
//			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);
//
//			String strQry=null;
//			switch (dbProvider)
//			{
//				case DataProviderType.Sql :
//					strQry = " SELECT Emp_ID, Emp_Name,Emp_ID+' '+Emp_Name Emp_Display FROM mf_staff ";
//					strQry+= " WHERE applicationID='"+strAppID+"' and EnterpriseID='"+strEnterpriseID+"' and Emp_Type ='D' and base_location = '"+base_location+"' and ((convert(varchar(8),getdate(),112) between convert(varchar(8),isnull(Initial_in_service_date,''),112) and convert(varchar(8),isnull(Separation_Date,getdate()),112) and Out_Of_Service_Date is null and In_Service_Date is null) or (convert(varchar(8),getdate(),112) between convert(varchar(8),isnull(Initial_in_service_date,''),112) and convert(varchar(8),isnull(Out_Of_Service_Date,''),112) and Out_Of_Service_Date is not null and In_Service_Date is null) or (convert(varchar(8),getdate(),112) between convert(varchar(8),isnull(In_Service_Date,''),112) and convert(varchar(8),isnull(Separation_Date,getdate()),112) and Out_Of_Service_Date is not null and In_Service_Date is not null )) " + " ORDER BY Emp_Name";
//					break;	
//			
//				case DataProviderType.Oracle:
//					strQry = " SELECT Emp_ID, Emp_Name,Emp_ID+' '+Emp_Name Emp_Display  FROM mf_staff ";
//					strQry+= " and applicationID='"+strAppID+"' and EnterpriseID='"+strEnterpriseID+"' and Emp_Type ='D' and base_location = '"+base_location+"' and ((convert(varchar(8),getdate(),112) between convert(varchar(8),isnull(Initial_in_service_date,''),112) and convert(varchar(8),isnull(Separation_Date,getdate()),112) and Out_Of_Service_Date is null and In_Service_Date is null) or (convert(varchar(8),getdate(),112) between convert(varchar(8),isnull(Initial_in_service_date,''),112) and convert(varchar(8),isnull(Out_Of_Service_Date,''),112) and Out_Of_Service_Date is not null and In_Service_Date is null) or (convert(varchar(8),getdate(),112) between convert(varchar(8),isnull(In_Service_Date,''),112) and convert(varchar(8),isnull(Separation_Date,getdate()),112) and Out_Of_Service_Date is not null and In_Service_Date is not null )) " + " ORDER BY Emp_Name";					
//					break;
//
//				case DataProviderType.Oledb:
//					break;
//			}			
//
//			DataSet dsQryData = (DataSet)dbCon.ExecuteQuery(strQry,queryParams,ReturnType.DataSetType);
//			return dsQryData;
//		}

		

		public static DataSet DriverIDBMCP(String strAppID,String strEnterpriseID)
		{
			DataProviderType dbProvider = DbConnectionManager.GetInstance().GetDbProviderType(strAppID, strEnterpriseID);
			ArrayList queryParams = new ArrayList();
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);

			String strQry=null;
			switch (dbProvider)
			{
				case DataProviderType.Sql :
					strQry = " select distinct Dep_Driver_ID emp_id,Emp_Name,Dep_Driver_ID+' '+Emp_Name Emp_Display ";
					strQry+= " from dbo.MF_Batch_Manifest_DepArr a WITH (NOLOCK) ";
					strQry+= " left outer join mf_staff b on a.Dep_Driver_ID=b.emp_id ";
					strQry+= " where isnull(Dep_Driver_ID,'') !='' ";
					break;	
			
//				case DataProviderType.Oracle:
//					strQry = " SELECT Emp_ID, Emp_Name,Emp_ID+' '+Emp_Name Emp_Display  FROM mf_staff ";
//					strQry+= " and applicationID='"+strAppID+"' and EnterpriseID='"+strEnterpriseID+"' and Emp_Type ='D' and ((convert(varchar(8),getdate(),112) between convert(varchar(8),isnull(Initial_in_service_date,''),112) and convert(varchar(8),isnull(Separation_Date,getdate()),112) and Out_Of_Service_Date is null and In_Service_Date is null) or (convert(varchar(8),getdate(),112) between convert(varchar(8),isnull(Initial_in_service_date,''),112) and convert(varchar(8),isnull(Out_Of_Service_Date,''),112) and Out_Of_Service_Date is not null and In_Service_Date is null) or (convert(varchar(8),getdate(),112) between convert(varchar(8),isnull(In_Service_Date,''),112) and convert(varchar(8),isnull(Separation_Date,getdate()),112) and Out_Of_Service_Date is not null and In_Service_Date is not null )) " + " ORDER BY Emp_Name";					
//					break;

				case DataProviderType.Oledb:
					break;
			}			


			DataSet dsQryData = (DataSet)dbCon.ExecuteQuery(strQry,queryParams,ReturnType.DataSetType);
			return dsQryData;
		}
		// End

		
		// boon 2011-09-14 -- start
		public static DataSet StaffIdQuery(String strAppID,String strEnterpriseID,Cambro.Web.DbCombo.ServerMethodArgs args)
		{
			String sTempWhere=null;
			sTempWhere= " and emp_id like '%" + Utility.ReplaceSingleQuote(args.Query) + "%'";

			DataProviderType dbProvider = DbConnectionManager.GetInstance().GetDbProviderType(strAppID,strEnterpriseID);
			ArrayList queryParams = new ArrayList();
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			//			IDbDataParameter dataParam = dbCon.MakeParam("@Query",DbType.String,12,ParameterDirection.Input,"%"+args.Query+"%");
			//			queryParams.Add(dataParam);
			
			String strQry = null;

			switch (dbProvider)
			{
				case DataProviderType.Sql:
					strQry = " SELECT TOP " + args.Top + " emp_id AS DbComboText, emp_id AS DbComboValue FROM mf_staff ";
					strQry+= " WHERE applicationID='" + strAppID + "' and EnterpriseID='" + strEnterpriseID + "' " + sTempWhere + " ORDER BY emp_id";				
					break;

				case DataProviderType.Oracle:
					strQry = " SELECT emp_id AS DbComboText, emp_id AS DbComboValue FROM mf_staff where rownum <= " + args.Top;
					strQry+= " and applicationID='"+strAppID+"' and EnterpriseID='"+strEnterpriseID+"' "+sTempWhere+" ORDER BY emp_id";				
					break;
				
				case DataProviderType.Oledb:
					break;
			}		
			DataSet dsQryData = (DataSet)dbCon.ExecuteQuery(strQry,queryParams,ReturnType.DataSetType);
			return dsQryData;
		}

		public static DataSet VehicleIdQuery(String strAppID,String strEnterpriseID,Cambro.Web.DbCombo.ServerMethodArgs args)
		{

			String sTempWhere=null;
			sTempWhere= " and truckid like '%" + Utility.ReplaceSingleQuote(args.Query) + "%'";

			DataProviderType dbProvider = DbConnectionManager.GetInstance().GetDbProviderType(strAppID,strEnterpriseID);
			ArrayList queryParams = new ArrayList();
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			//			IDbDataParameter dataParam = dbCon.MakeParam("@Query",DbType.String,12,ParameterDirection.Input,"%"+args.Query+"%");
			//			queryParams.Add(dataParam);
			
			String strQry = null;

			switch (dbProvider)
			{
				case DataProviderType.Sql:
					strQry = " SELECT TOP " + args.Top + " truckid AS DbComboText, truckid AS DbComboValue FROM mf_vehicles ";
					strQry += " WHERE applicationID='" + strAppID + "' and EnterpriseID='" + strEnterpriseID + "' " + sTempWhere + " ORDER BY truckid";				
					break;

				case DataProviderType.Oracle:
					strQry = " SELECT truckid AS DbComboText, truckid AS DbComboValue FROM mf_vehicles where rownum <= " + args.Top;
					strQry += " and applicationID='" + strAppID + "' and EnterpriseID='" + strEnterpriseID + "' " + sTempWhere + " ORDER BY truckid";				
					break;
				
				case DataProviderType.Oledb:
					break;
			}		
			DataSet dsQryData = (DataSet)dbCon.ExecuteQuery(strQry,queryParams,ReturnType.DataSetType);
			return dsQryData;
		}
		// boon 2011-09-14 -- end

	} 
}