using System;
using System.Data;
using com.common.DAL;
using System.Text;
using com.common.classes;
using com.common.util;

namespace com.ties.classes
{
	/// <summary>
	/// Summary description for Commodity.
	/// </summary>
	public class CommodityDetails
	{
		public CommodityDetails()
		{
			//
			// TODO: Add constructor logic here
			//
		}
		String m_strCommodityDesc = null;
		String m_strCommodityCode = null;

		public String CommodityDescription
		{
			set
			{
				m_strCommodityDesc = value;
			}
			get
			{
				return m_strCommodityDesc;
			}
		}

		public String CommodityCode
		{
			set
			{
				m_strCommodityCode = value;
			}
			get
			{
				return m_strCommodityCode;
			}
		}
		public void Populate(String appID, String enterpriseID,String commodityCode)
		{
			DbConnection dbCon = null;
			DataSet dsCommodity = null;
			IDbCommand dbCmd = null;

			dbCon = DbConnectionManager.GetInstance().GetDbConnection(appID,enterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("Service.cs","Populate","ERR001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			
			StringBuilder strQry = new StringBuilder();
			strQry = strQry.Append("select * from commodity  ");
			strQry.Append(" where applicationid = '");
			strQry.Append(appID);
			strQry.Append("' and enterpriseid = '");
			strQry.Append(enterpriseID);
			strQry.Append("' and commodity_code = '");
			strQry.Append(Utility.ReplaceSingleQuote(commodityCode));
			strQry.Append("'");

			dbCmd = dbCon.CreateCommand(strQry.ToString(),CommandType.Text);

			try
			{
				dsCommodity =(DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
			}
			catch(ApplicationException appExpection)
			{
				Logger.LogTraceError("Commodity.cs","Populate","ERR002","Error in  Execute Query ");
				throw appExpection;
			}

			if(dsCommodity.Tables[0].Rows.Count >0)
			{
				DataRow drEach = dsCommodity.Tables[0].Rows[0];
				
				if(!drEach["commodity_description"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					m_strCommodityDesc = (String)drEach["commodity_description"];					
				}
				if(!drEach["commodity_code"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					m_strCommodityCode = (String)drEach["commodity_code"];					
				}

			}
		}
	}
}
