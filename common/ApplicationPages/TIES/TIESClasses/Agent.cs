using System;
using System.Data;
using System.Text;
using com.common.DAL;
using com.common.classes;


namespace com.ties.classes
{
	/// <summary>
	/// Summary description for Agent.
	/// </summary>
	public class Agent
	{
		public Agent()
		{
			//
			// TODO: Add constructor logic here
			//
		}
		private string m_strAppID=null;
		private string m_strEnterpriseID=null;
		private string m_strName=null;
		private string m_straddress1=null;
		private string m_straddress2=null;
		private string m_strtelephone=null;
		private string m_strfax=null;
		private string m_strZipCode = null;
		private string m_strApplyDimWt = null;
		private string m_strPODSlip = null;
		private string m_strESASurcharge = null;

		/// <summary>
		/// This method populates the agent details from the Agent table.
		/// </summary>
		/// <param name="appid">Application ID</param>
		/// <param name="enterpriseid">Enterprise ID</param>
		/// <param name="agentid">Agent ID</param>
		public void Populate(string appid,string enterpriseid,string agentid)
		{
			DbConnection dbCon= null;
			DataSet dsAgent =null;
			IDbCommand dbcmd=null;

			dbCon = DbConnectionManager.GetInstance().GetDbConnection(appid,enterpriseid);

			if (dbCon==null)
			{
				Logger.LogTraceError("Agent.cs","PopulateAgentData","Agent001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			StringBuilder strQry =  new StringBuilder(); 

			strQry = strQry.Append("select * from Agent a");
			strQry.Append(" where a.applicationid = '");
			strQry.Append(appid);
			strQry.Append ("' and a.enterpriseid = '");
			strQry.Append (enterpriseid);
			strQry.Append ("' and a.agentid ='");
			strQry.Append (agentid);
			strQry.Append ("'");

			dbcmd = dbCon.CreateCommand(strQry.ToString(),CommandType.Text);
 
			try
			{
				dsAgent = (DataSet)dbCon.ExecuteQuery(dbcmd,com.common.DAL.ReturnType.DataSetType);  
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("Agent.cs","PopulateAgentData","A001","dbcon is null");
				throw appException;
			}

			if (dsAgent.Tables[0].Rows.Count  > 0) 
			{
				DataRow drEach = dsAgent.Tables[0].Rows[0];
				if(!drEach["agent_name"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					m_strName = (string)drEach["agent_name"];
				}
				
				if(!drEach["agent_address1"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					m_straddress1=(string)drEach["agent_address1"];
				}
				
				if(!drEach["agent_address2"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					m_straddress2=(string)drEach["agent_address2"];
				}
				

				if(!drEach["telephone"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					m_strtelephone=(string)drEach["telephone"];
				}
				
				if(!drEach["fax"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					m_strfax=(string)drEach["fax"];
				}
				
				if(!drEach["zipcode"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					m_strZipCode = (string)drEach["zipcode"];
				}
				if(!drEach["apply_dim_wt"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					m_strApplyDimWt = (string)drEach["apply_dim_wt"];
				}
				if(!drEach["pod_slip_required"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					m_strPODSlip = (string)drEach["pod_slip_required"];
				}	
				if(!drEach["apply_esa_surcharge"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					m_strESASurcharge = (string)drEach["apply_esa_surcharge"];
				}
			}
		}


		/// <summary>
		/// This method gets & sets the value of Application ID.
		/// </summary>
		public String AppID
			{
				set
				{
					m_strAppID = value;
				}
				get
				{
					return m_strAppID;
				}
			}
		/// <summary>
		/// This method gets & sets the value of Enterprise ID.
		/// </summary>
		public string EnterpriseID
		{
			set
			{
				m_strEnterpriseID=value;
			}
			get
			{
				return m_strEnterpriseID;
			}
		}

		/// <summary>
		/// This method gets & sets the value of Agent Name.
		/// </summary>
		public string AgentName
		{
			set
			{
				m_strName=value;
			}
			get
			{
				return m_strName;
			}
		}

		/// <summary>
		/// This method gets & sets the value of Agent Address1.
		/// </summary>
		public string AgentAddress1
		{
			set
			{
				m_straddress1=value;
			}
			get
			{
				return m_straddress1;
			}
		}

		/// <summary>
		/// This method gets & sets the value of Agent Address2.
		/// </summary>
		public string AgentAddress2
		{
			set
			{
				m_straddress2=value;
			}
			get
			{
				return m_straddress2;
			}
		}

		/// <summary>
		/// This method gets & sets the value of Agent Telephone.
		/// </summary>
		public string AgentTelephone
		{
			set
			{
				m_strtelephone = value;
			}
			get
			{
				return m_strtelephone;
			}
		}

		/// <summary>
		/// This method gets & sets the value of Agent Fax.
		/// </summary>
		public string AgentFax
		{
			set
			{
				m_strfax=value;
			}
			get
			{
				return m_strfax;
			}

		}
		

		/// <summary>
		/// This method gets & sets the value of ZipCode.
		/// </summary>
		public string ZipCode
		{
			set
			{
				m_strZipCode = value;
			}
			get
			{
				return m_strZipCode;
			}
		}

		/// <summary>
		/// This method gets & sets the value of Dimensional Weight which  is Y (YES) or N (No).
		/// </summary>
		public string ApplyDimWt
		{
			set
			{
				m_strApplyDimWt = value;
			}
			get
			{
				return m_strApplyDimWt;
			}
		}

		/// <summary>
		/// This method gets & sets the value of POD (Proof of Delivery) which  is Y (YES) or N (No).
		/// </summary>
		public String PODSlipRequired
		{
			set
			{
				m_strPODSlip = value;
			}
			get
			{
				return m_strPODSlip;
			}
		}


		/// <summary>
		/// This method gets & sets the value of ESA (Extended Service Area) Surcharge which  is Y (YES) or N (No).
		/// </summary>
		public String ESASurcharge
		{
			set
			{
				m_strESASurcharge = value;
			}
			get
			{
				return m_strESASurcharge;
			}
		}
	}
}
