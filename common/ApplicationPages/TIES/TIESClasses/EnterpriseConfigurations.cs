using System;
using System.Text;
using System.Data;
using System.Runtime.Serialization;

namespace com.ties.classes
{
	public class EnterpriseConfigurations
	{
		public static DomesticShipmentConfigurations  DomesticShipment(DataSet ds)
		{
			DomesticShipmentConfigurations conf = new DomesticShipmentConfigurations();
			DataConfigurations dataConf= new DataConfigurations(ds);
			conf.DaysUntilBookingExpires=dataConf.DaysUntilBookingExpires;
			conf.HCSupportedbyEnterprise=dataConf.HCSupportedbyEnterprise;
			conf.CODSupportedbyEnterprise=dataConf.CODSupportedbyEnterprise;
			conf.InsSupportedbyEnterprise=dataConf.InsSupportedbyEnterprise;
			conf.CommCodeOnShipment=dataConf.CommCodeOnShipment;
			return conf;
		}

		public static CustomerProfileConfigurations  CustomerProfile(DataSet ds)
		{
			CustomerProfileConfigurations conf = new CustomerProfileConfigurations();
			DataConfigurations dataConf= new DataConfigurations(ds);
			conf.HCSupportedbyEnterprise=dataConf.HCSupportedbyEnterprise;
			conf.CODSupportedbyEnterprise=dataConf.CODSupportedbyEnterprise;
			conf.InsSupportedbyEnterprise=dataConf.InsSupportedbyEnterprise;
			return conf;
		}

		public static ManifestFormsConfigurations  ManifestForms(DataSet ds)
		{
			ManifestFormsConfigurations conf = new ManifestFormsConfigurations();
			DataConfigurations dataConf= new DataConfigurations(ds);
			conf.DaysUntilBookingExpires=dataConf.DaysUntilBookingExpires;
			return conf;
		}

		public static PackageWtDimConfigurations  PackageWtDimForms(DataSet ds)
		{
			PackageWtDimConfigurations conf = new PackageWtDimConfigurations();
			foreach(DataRow dr in ds.Tables[0].Rows)
			{
				if(dr["key"].ToString() =="DisplayOnly")
				{
					if(dr["value"].ToString() == "MPS Number")
					{
						conf.Mpsnumber=false;
					}
					else if(dr["value"].ToString() == "Quantity")
					{
						conf.Quantity=false;
					}
					else if(dr["value"].ToString() == "Weight")
					{
						conf.Weight=false;
					}
					else if(dr["value"].ToString() == "Length")
					{
						conf.Length=false;
					}
					else if(dr["value"].ToString() == "Breadth")
					{
						conf.Breadth=false;
					}
					else if(dr["value"].ToString() == "Height")
					{
						conf.Height=false;
					}
				}
				else if (dr["key"].ToString() == "DefaultFocus")
				{
					if(dr["value"].ToString() == "MPS Number")
					{
						conf.DefaultFocus = "MPS Number";
					}
					else if(dr["value"].ToString() == "Quantity")
					{
						conf.DefaultFocus = "Quantity";
					}
					else if(dr["value"].ToString() == "Weight")
					{
						conf.DefaultFocus = "Weight";
					}
					else if(dr["value"].ToString() == "Length")
					{
						conf.DefaultFocus = "Length";
					}
					else if(dr["value"].ToString() == "Breadth")
					{
						conf.DefaultFocus = "Breadth";
					}
					else if(dr["value"].ToString() == "Height")
					{
						conf.DefaultFocus = "Height";
					}
				}
			}
			return conf;
		}

		public static QueryShipmentTrackingConfigurations  QueryShipmentTracking(DataSet ds)
		{
			QueryShipmentTrackingConfigurations conf = new QueryShipmentTrackingConfigurations();
			
			foreach(DataRow dr in ds.Tables[0].Rows)
			{
				if(dr["key"].ToString()== "CreditControlInScope")
				{
					if(dr["value"].ToString() == "N")
					{
						conf.CreditControlInScope= false;
					} 
					else 
					{
						conf.CreditControlInScope= true;
					}
				}
				else if(dr["key"].ToString() == "InvoicingInScope" )
				{
					if(dr["value"].ToString() == "N")
					{
						conf.InvoicingInScope= false;
					} 
					else 
					{
						conf.InvoicingInScope= true;
					}
				}						
			}
			return conf;
		}

	}

	public class DataConfigurations  
	{
		private int _DaysUntilBookingExpires=5;
		private bool _HCSupportedbyEnterprise=false;
		private bool _CODSupportedbyEnterprise=false;
		private bool _InsSupportedbyEnterprise=false;
		private bool _CommCodeOnShipment=false;

		public DataConfigurations(DataSet ds)
		{
			if(ds.Tables.Count > 0 && (ds.Tables[0].Columns["key"] != null && ds.Tables[0].Columns["value"] != null))
			{
				foreach(DataRow dr in ds.Tables[0].Rows)
				{
					string str_value = dr["value"].ToString();
					switch(dr["key"].ToString())
					{
						case "DaysUntilBookingExpires":
							try
							{
								_DaysUntilBookingExpires = int.Parse(str_value);
							}
							catch
							{

							}
							break;

						case "HCSupportedbyEnterprise":
							if(str_value =="Y")
							{
								_HCSupportedbyEnterprise = true;
							}
							break;
						case "CODSupportedbyEnterprise":
							if(str_value =="Y")
							{
								_CODSupportedbyEnterprise = true;
							}
							break;
						case "InsSupportedbyEnterprise":
							if(str_value =="Y")
							{
								_InsSupportedbyEnterprise = true;
							}
							break;
						case "CommCodeOnShipment":
							if(str_value =="Y")
							{
								_CommCodeOnShipment = true;
							}
							break;
						default:
							break;
					}
				}
			}

		}

		public int DaysUntilBookingExpires
		{
			get{return _DaysUntilBookingExpires;}
			set{_DaysUntilBookingExpires=value;}
		}

		public bool HCSupportedbyEnterprise
		{
			get{return _HCSupportedbyEnterprise;}
			set{_HCSupportedbyEnterprise=value;}
		}
		public bool CODSupportedbyEnterprise
		{
			get{return _CODSupportedbyEnterprise;}
			set{_CODSupportedbyEnterprise=value;}
		}
		public bool InsSupportedbyEnterprise
		{
			get{return _InsSupportedbyEnterprise;}
			set{_InsSupportedbyEnterprise=value;}
		}
		public bool CommCodeOnShipment
		{
			get{return _CommCodeOnShipment;}
			set{_CommCodeOnShipment=value;}
		}
	}

	[Serializable()]
	public class DomesticShipmentConfigurations  
	{
		private int _DaysUntilBookingExpires=5;
		private bool _HCSupportedbyEnterprise=false;
		private bool _CODSupportedbyEnterprise=false;
		private bool _InsSupportedbyEnterprise=false;
		private bool _CommCodeOnShipment=false;

		public DomesticShipmentConfigurations()
		{

		}

		public int DaysUntilBookingExpires
		{
			get{return _DaysUntilBookingExpires;}
			set{_DaysUntilBookingExpires=value;}
		}

		public bool HCSupportedbyEnterprise
		{
			get{return _HCSupportedbyEnterprise;}
			set{_HCSupportedbyEnterprise=value;}
		}
		public bool CODSupportedbyEnterprise
		{
			get{return _CODSupportedbyEnterprise;}
			set{_CODSupportedbyEnterprise=value;}
		}
		public bool InsSupportedbyEnterprise
		{
			get{return _InsSupportedbyEnterprise;}
			set{_InsSupportedbyEnterprise=value;}
		}
		public bool CommCodeOnShipment
		{
			get{return _CommCodeOnShipment;}
			set{_CommCodeOnShipment=value;}
		}
	}

	public class PackageWtDimConfigurations  
	{
		private bool _Mpsnumber=true;
		private bool _Quantity=true;
		private bool _Weight=true;
		private bool _Length=true;
		private bool _Breadth=true;
		private bool _Height=true;

		private string _DefaultFocus = "";

		public PackageWtDimConfigurations()
		{

		}

		public bool Quantity
		{
			get{return _Quantity;}
			set{_Quantity=value;}
		}

		public bool Mpsnumber
		{
			get{return _Mpsnumber;}
			set{_Mpsnumber=value;}
		}

		public bool Weight
		{
			get{return _Weight;}
			set{_Weight=value;}
		}

		public bool Length
		{
			get{return _Length;}
			set{_Length=value;}
		}

		public bool Breadth
		{
			get{return _Breadth;}
			set{_Breadth=value;}
		}

		public bool Height
		{
			get{return _Height;}
			set{_Height=value;}
		}

		public string DefaultFocus
		{
			get{return _DefaultFocus;}
			set{_DefaultFocus=value;}
		}
	}

	[Serializable()]
	public class CustomerProfileConfigurations 
	{
		private bool _HCSupportedbyEnterprise=false;
		private bool _CODSupportedbyEnterprise=false;
		private bool _InsSupportedbyEnterprise=false;

		public CustomerProfileConfigurations()
		{

		}

		public bool HCSupportedbyEnterprise
		{
			get{return _HCSupportedbyEnterprise;}
			set{_HCSupportedbyEnterprise=value;}
		}
		public bool CODSupportedbyEnterprise
		{
			get{return _CODSupportedbyEnterprise;}
			set{_CODSupportedbyEnterprise=value;}
		}
		public bool InsSupportedbyEnterprise
		{
			get{return _InsSupportedbyEnterprise;}
			set{_InsSupportedbyEnterprise=value;}
		}
	}


	[Serializable()]
	public class ManifestFormsConfigurations
	{
		private int _DaysUntilBookingExpires=5;

		public ManifestFormsConfigurations()
		{

		}

		public int DaysUntilBookingExpires
		{
			get{return _DaysUntilBookingExpires;}
			set{_DaysUntilBookingExpires=value;}
		}
	}


	[Serializable()]
	public class QueryShipmentTrackingConfigurations 
	{
		private bool _CreditControlInScope=false;
		private bool _InvoicingInScope=false;

		public QueryShipmentTrackingConfigurations()
		{

		}

		public bool CreditControlInScope
		{
			get{return _CreditControlInScope;}
			set{_CreditControlInScope=value;}
		}

		public bool InvoicingInScope
		{
			get{return _InvoicingInScope;}
			set{_InvoicingInScope=value;}
		}
	}

}