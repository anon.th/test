using System;
using System.Data;
using com.ties.classes;
using com.ties.DAL;
using com.ties.BAL;
using TIESDAL;
namespace TIESBAL
{
	/// <summary>
	/// Summary description for RetSWBPcksBatchBAL.
	/// </summary>
	public class RetSWBPcksBatchBAL
	{
		public RetSWBPcksBatchBAL()
		{
			//
			// TODO: Add constructor logic here
			//
		}
		public static bool isConsignmentHasInv(String strAppID, String strEnterpriseID,String consignmentNo)
		{
			DataSet dsShipment = new DataSet();
			bool result = false;
			dsShipment = RetSWBPcksBatchDAL.getShipmentData(strAppID,strEnterpriseID,consignmentNo);
			if(dsShipment.Tables[0].Rows.Count>0 && dsShipment.Tables[0].Rows[0]["Invoice_No"].ToString()!="")
			{
				result =true;
			}
			return result;

		}
		public static bool isPreshipment(String strAppID, String strEnterpriseID,String consignmentNo)
		{
			DataSet dsShipment = new DataSet();
			bool result = false;
			dsShipment = RetSWBPcksBatchDAL.getShipmentData(strAppID,strEnterpriseID,consignmentNo);
			if(dsShipment.Tables[0].Rows.Count>0 && dsShipment.Tables[0].Rows[0]["payerid"]==null)
			{
				result =true;
			}
			return result;

		}
		public static bool isManual_Rating_Override(String strAppID, String strEnterpriseID,String consignmentNo)
		{
			DataSet dsShipment = new DataSet();
			bool result = false;
			dsShipment = RetSWBPcksBatchDAL.getShipmentData(strAppID,strEnterpriseID,consignmentNo);
			if(dsShipment.Tables[0].Rows.Count>0 && dsShipment.Tables[0].Rows[0]["Manual_Override"].ToString()!="")
			{
				result =true;
			}
			return result;

		}
		public static bool isExistInSWB(String strAppID, String strEnterpriseID,String consignmentNo)
		{
			DataSet dsPackage = new DataSet();
			bool result = false;
			dsPackage = RetSWBPcksBatchDAL.getPackageData(strAppID,strEnterpriseID,consignmentNo);
			if(dsPackage.Tables[0].Rows.Count>0 )
			{
				result =true;
			}
			return result;

		}
		public static bool isIdenticalPackage(String strAppID, String strEnterpriseID, String Booking_no,String consignmentNo)
		{
			//DataSet dsIdenticalPkg = new DataSet();
			int nInden = 0;
			bool result = false;
			nInden = RetSWBPcksBatchDAL.GetIdenticalPackage(strAppID,strEnterpriseID,Booking_no,consignmentNo);
			//if(dsIdenticalPkg.Tables[0].Rows.Count>0 )
			if(nInden == 1)
			{
				result =true;
			}
			return result;
		}
		public static bool IsBoxRateAvailable(String strAppID, String strEnterpriseID, String PayerId,String sender_zipcode,String recip_zipcode,String service_code)
		{
			DataSet tmpCustZone = null;
			string sendCustZone = null;
			string recipCustZone = null;
			tmpCustZone = SysDataMgrDAL.GetCustZoneData(strAppID, strEnterpriseID,
				0, 0, PayerId.Trim(), sender_zipcode.Trim()).ds;
			if(tmpCustZone.Tables[0].Rows.Count > 0)
			{
				foreach(DataRow dr in tmpCustZone.Tables[0].Rows)
				{
					sendCustZone = dr["zone_code"].ToString().Trim();
				}
			}
			tmpCustZone = SysDataMgrDAL.GetCustZoneData(strAppID, strEnterpriseID,
				0, 0, PayerId.Trim(), recip_zipcode.Trim()).ds;
			if(tmpCustZone.Tables[0].Rows.Count > 0)
			{
				foreach(DataRow dr in tmpCustZone.Tables[0].Rows)
				{
					recipCustZone = dr["zone_code"].ToString().Trim();
				}
			}
			Customer customer = new Customer();
			customer.Populate(strAppID,strEnterpriseID, PayerId);
			return customer.IsBoxRateAvailable(strAppID,strEnterpriseID,sendCustZone,recipCustZone,service_code.Trim());
			
		}
		public static decimal calInsuranceSurcharge(string strAppId,string strEnterpriseId , string PayerID,decimal declare_value)
		{
				
			Customer customer = new Customer();
			customer.Populate(strAppId,strEnterpriseId, PayerID);
			DomesticShipmentMgrBAL domesticBAL = new DomesticShipmentMgrBAL();
			return domesticBAL.ComputeInsSurchargeByCust(strAppId, strEnterpriseId, Convert.ToDecimal(declare_value), PayerID, customer);
		}
	}
}
