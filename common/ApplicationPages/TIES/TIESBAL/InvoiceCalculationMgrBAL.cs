using System;
using System.Data;
using com.ties.classes;
using com.ties.DAL;
using com.common.util;
using System.Text;

namespace com.ties.BAL
{
	/// <summary>
	/// Summary description for InvoiceGenerationMgrBAL.
	/// </summary>
	public class InvoiceCalculationMgrBAL
	{
		public InvoiceCalculationMgrBAL()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		public int GenerateInvoices(string strAppID, string strEnterpriseID, DataSet dsInvoiceCriteria, ref int intCustomerCnt, ref int intAWBCnt)
		{
			DataSet dsInvShip = null;
			int intRet = 0;

			dsInvShip = InvoiceGenerationMgrDAL.GetInvoiceableShipments(strAppID, strEnterpriseID, dsInvoiceCriteria);
			intRet = CalculateCharges(strAppID, strEnterpriseID, dsInvShip, ref intCustomerCnt, ref intAWBCnt);

			return intRet;
		}

		public int CalculateCharges(string strAppID, string strEnterpriseID, DataSet dsIShip, ref int intCustomerCnt, ref int intAWBCnt)
		{
			int i=0, intCnt=0;
			intCnt = dsIShip.Tables[0].Rows.Count;
			//intCnt = 10;
			if (intCnt < 1)
				return intCnt;

			//invoice header variables
			string strPrevPayType=null, strPrevPayID=null, strInvoiceNo=null, strInvoiceStatus=null;
			string strInvoiceDate=null, strDueDate=null;
			string strPayerId=null, strPayerType=null, strPayerName=null, strPayerAdd1=null, strPayerAdd2=null;
			string strPayerZip=null, strPayerCountry=null, strPayerTel=null, strPayerFax=null, strPayMode=null;
			int intCreditTerm=0, intRowsInserted=0, intInvDetInserted=0;
			DateTime dtInvoiceDate=DateTime.Now, dtDueDate=DateTime.Now;
			StringBuilder strBuilder = null;

			//invoice detail variables
			string strConsgNo=null, strRefNo=null, strOriState=null, strDesState=null, strRecName=null, strRecAdd1=null;
			string strRecAdd2=null, strRecZip=null, strRecCountry=null, strSenName=null, strSenAdd1=null, strSenAdd2=null;
			string strSenZip=null, strSenCountry=null, strMBG=null, strServiceCode=null; //, strPODExcep=null, strPerInCharge=null;
			string strBookingDate=null;//, strPODRemark=null;
			int intTotPkg=0, intBookingNo=0;
			decimal decTotWt=0, decTotDimWt=0, decCharWt=0, decInsAmt=0, decTotFrgCharge=0, decTotVasSurcharge=0, decEsaSurcharge=0;
			decimal decInvoiceAmt=0, decInvSurchargeAmt=0;
			DateTime dtBookingDate;//, dtPODDate;

			//dataset for invoice summary 
			DataSet dsInvSumm=null;
			CreateInvSumDS(ref dsInvSumm);
			//UpdateInvSumDS(ref dsInvSumm,"S","INS",5,0);
			//UpdateInvSumDS(ref dsInvSumm,"I","INS",0,30);
			//return 0;

			for(i=0;i<intCnt;i++)
			{
				DataRow drEachShip = dsIShip.Tables[0].Rows[i];
				strPayerId = (string)drEachShip["payerid"];
				strPayerType = (string)drEachShip["payer_type"];
				if (strPrevPayType==null || strPrevPayType!=strPayerType || strPrevPayID==null || strPrevPayID!=strPayerId)
				{
					if (intRowsInserted > 0)
						//call the method to run sql statements for invoice_summary table
						UpdateInvSumTab(ref dsInvSumm,strAppID,strEnterpriseID,strInvoiceNo);

					strPrevPayID = strPayerId;
					strPrevPayType = strPayerType;
					strInvoiceNo = ""; strInvoiceDate = ""; strDueDate = ""; strInvoiceStatus = ""; strPayerName = ""; strPayerAdd1 = ""; strPayerAdd2 = "";
					strPayerZip = ""; strPayerCountry = ""; strPayerTel = ""; strPayerFax = ""; strPayMode = "";
					intCreditTerm = 0; intInvDetInserted=0;
					dtInvoiceDate=DateTime.Now; dtDueDate=DateTime.Now;

					//generate a new invoice no and insert into invoice header table
					strInvoiceNo = (string)Counter.GetNext(strAppID,strEnterpriseID,"invoice_number","INV");
					if (strPayerType == "C")
					{
						strPayMode = (string)drEachShip["customer_payment_mode"];
						if ((drEachShip["customer_credit_term"]!=null) && (!drEachShip["customer_credit_term"].GetType().Equals(System.Type.GetType("System.DBNull"))))
							intCreditTerm = Convert.ToInt32(drEachShip["customer_credit_term"]);
					}
					else
					{
						strPayMode = (string)drEachShip["agent_payment_mode"];
						if ((drEachShip["agent_credit_term"]!=null) && (!drEachShip["agent_credit_term"].GetType().Equals(System.Type.GetType("System.DBNull"))))
							intCreditTerm = Convert.ToInt32(drEachShip["agent_credit_term"]);
					}
					strInvoiceDate = Utility.DateFormat(strAppID, strEnterpriseID, dtInvoiceDate, DTFormat.DateTime);
					if (strPayMode == "R")
						dtDueDate = dtDueDate.AddDays(intCreditTerm);
					strDueDate = Utility.DateFormat(strAppID, strEnterpriseID, dtDueDate, DTFormat.DateTime);

					strBuilder = new StringBuilder();
					strBuilder.Append("insert into invoice (applicationid, enterpriseid, invoice_no, invoice_date, due_date, payerid, payer_type, payer_name, ");
					strBuilder.Append("payer_address1, payer_address2, payer_zipcode, payer_country, payer_telephone, payer_fax, payment_mode, invoice_status) values (");
					strBuilder.Append("'"+strAppID+"', '"+strEnterpriseID+"', '"+strInvoiceNo+"', "+strInvoiceDate+", "+strDueDate+", '"+strPayerId+"', '"+strPayerType+"', ");

					if ((drEachShip["payer_name"]!=null) && (!drEachShip["payer_name"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						strPayerName = (string)drEachShip["payer_name"];
						strBuilder.Append("N'"+strPayerName+"', ");
					}
					else
						strBuilder.Append("null, ");

					if ((drEachShip["payer_address1"]!=null) && (!drEachShip["payer_address1"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						strPayerAdd1 = (string)drEachShip["payer_address1"];
						strBuilder.Append("N'"+strPayerAdd1+"', ");
					}
					else
						strBuilder.Append("null, ");

					if ((drEachShip["payer_address2"]!=null) && (!drEachShip["payer_address2"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						strPayerAdd2 = (string)drEachShip["payer_address2"];
						strBuilder.Append("N'"+strPayerAdd2+"', ");
					}
					else
						strBuilder.Append("null, ");

					if ((drEachShip["payer_zipcode"]!=null) && (!drEachShip["payer_zipcode"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						strPayerZip = (string)drEachShip["payer_zipcode"];
						strBuilder.Append("'"+strPayerZip+"', ");
					}
					else
						strBuilder.Append("null, ");

					if ((drEachShip["payer_country"]!=null) && (!drEachShip["payer_country"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						strPayerCountry = (string)drEachShip["payer_country"];
						strBuilder.Append("'"+strPayerCountry+"', ");
					}
					else
						strBuilder.Append("null, ");

					if ((drEachShip["payer_telephone"]!=null) && (!drEachShip["payer_telephone"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						strPayerTel = (string)drEachShip["payer_telephone"];
						strBuilder.Append("'"+strPayerTel+"', ");
					}
					else
						strBuilder.Append("null, ");

					if ((drEachShip["payer_fax"]!=null) && (!drEachShip["payer_fax"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						strPayerFax = (string)drEachShip["payer_fax"];
						strBuilder.Append("'"+strPayerFax+"', ");
					}
					else
						strBuilder.Append("null, ");

					strInvoiceStatus = "U";
					strBuilder.Append("'"+strPayMode+"', '"+strInvoiceStatus+"')");

					//insert into invoice header table
					try
					{
						intRowsInserted += InvoiceGenerationMgrDAL.InsertInvoiceHeader(strAppID, strEnterpriseID, strBuilder.ToString());
						intCustomerCnt++;
					}
					catch(ApplicationException appException)
					{
						throw new ApplicationException(appException.Message,appException);
					}
				}

				//insert into invoice detail tables
				strConsgNo=null; strRefNo=null; strOriState=null; strDesState=null; strRecName=null; strRecAdd1=null;
				strRecAdd2=null; strRecZip=null; strRecCountry=null; strSenName=null; strSenAdd1=null; strSenAdd2=null;
				strSenZip=null; strSenCountry=null; strMBG=null; strServiceCode=null; //strPODExcep=null; strPerInCharge=null;
				strBookingDate=null; //strPODRemark=null;
				intTotPkg=0; decTotWt=0; decTotDimWt=0; decCharWt=0; decInsAmt=0; decTotFrgCharge=0; decTotVasSurcharge=0; 
				intBookingNo=0; decEsaSurcharge=0; decInvoiceAmt=0; decInvSurchargeAmt=0;//dtBookingDate; dtPODDate;

				//StringBuilder strBuilder = new StringBuilder();
				strBuilder = new StringBuilder();
				strBuilder.Append("insert into invoice_detail (applicationid, enterpriseid, invoice_no, consignment_no, booking_datetime, ");
				strBuilder.Append("ref_no, origin_state_code, destination_state_code, tot_pkg, tot_wt, tot_dim_wt, chargeable_wt, recipient_name, ");
				strBuilder.Append("recipient_address1, recipient_address2, recipient_zipcode, recipient_country, sender_name, sender_address1, ");
				strBuilder.Append("sender_address2, sender_zipcode, sender_country, mbg, service_code, pod_datetime, pod_exception, person_incharge, ");
				strBuilder.Append("pod_remark, insurance_amt, tot_freight_charge, tot_vas_surcharge, esa_surcharge, invoice_amt) values (");
				strBuilder.Append("'"+strAppID+"', '"+strEnterpriseID+"', '"+strInvoiceNo+"', ");

				strConsgNo = (string)drEachShip["consignment_no"];
				strBuilder.Append("'"+strConsgNo+"', ");

				//get booking no to join shipment_tracking table
				intBookingNo = Convert.ToInt32(drEachShip["booking_no"]);
				dtBookingDate = (DateTime)drEachShip["booking_datetime"];
				strBookingDate = Utility.DateFormat(strAppID, strEnterpriseID, dtBookingDate, DTFormat.DateTime);
				strBuilder.Append(strBookingDate+", ");

				if ((drEachShip["ref_no"]!=null) && (!drEachShip["ref_no"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					strRefNo = (string)drEachShip["ref_no"];
					strBuilder.Append("'"+strRefNo+"', ");
				}
				else
					strBuilder.Append("null, ");

				if ((drEachShip["origin_state_code"]!=null) && (!drEachShip["origin_state_code"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					strOriState = (string)drEachShip["origin_state_code"];
					strBuilder.Append("'"+strOriState+"', ");
				}
				else
					strBuilder.Append("null, ");

				if ((drEachShip["destination_state_code"]!=null) && (!drEachShip["destination_state_code"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					strDesState = (string)drEachShip["destination_state_code"];
					strBuilder.Append("'"+strDesState+"', ");
				}
				else
					strBuilder.Append("null, ");

				if ((drEachShip["tot_pkg"]!=null) && (!drEachShip["tot_pkg"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					intTotPkg = Convert.ToInt32(drEachShip["tot_pkg"]);
				strBuilder.Append(intTotPkg+", ");

				if ((drEachShip["tot_wt"]!=null) && (!drEachShip["tot_wt"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					decTotWt = Convert.ToDecimal(drEachShip["tot_wt"]);
				strBuilder.Append(decTotWt+", ");

				if ((drEachShip["tot_dim_wt"]!=null) && (!drEachShip["tot_dim_wt"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					decTotDimWt = Convert.ToDecimal(drEachShip["tot_dim_wt"]);
				strBuilder.Append(decTotDimWt+", ");

				if ((drEachShip["chargeable_wt"]!=null) && (!drEachShip["chargeable_wt"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					decCharWt = Convert.ToDecimal(drEachShip["chargeable_wt"]);
				strBuilder.Append(decCharWt+", ");

				if ((drEachShip["recipient_name"]!=null) && (!drEachShip["recipient_name"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					strRecName = (string)drEachShip["recipient_name"];
					strBuilder.Append("N'"+strRecName+"', ");
				}
				else
					strBuilder.Append("null, ");

				if ((drEachShip["recipient_address1"]!=null) && (!drEachShip["recipient_address1"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					strRecAdd1 = (string)drEachShip["recipient_address1"];
					strBuilder.Append("N'"+strRecAdd1+"', ");
				}
				else
					strBuilder.Append("null, ");

				if ((drEachShip["recipient_address2"]!=null) && (!drEachShip["recipient_address2"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					strRecAdd2 = (string)drEachShip["recipient_address2"];
					strBuilder.Append("N'"+strRecAdd2+"', ");
				}
				else
					strBuilder.Append("null, ");

				if ((drEachShip["recipient_zipcode"]!=null) && (!drEachShip["recipient_zipcode"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					strRecZip = (string)drEachShip["recipient_zipcode"];
					strBuilder.Append("'"+strRecZip+"', ");
				}
				else
					strBuilder.Append("null, ");

				if ((drEachShip["recipient_country"]!=null) && (!drEachShip["recipient_country"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					strRecCountry = (string)drEachShip["recipient_country"];
					strBuilder.Append("'"+strRecCountry+"', ");
				}
				else
					strBuilder.Append("null, ");

				if ((drEachShip["sender_name"]!=null) && (!drEachShip["sender_name"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					strSenName = (string)drEachShip["sender_name"];
					strBuilder.Append("N'"+strSenName+"', ");
				}
				else
					strBuilder.Append("null, ");

				if ((drEachShip["sender_address1"]!=null) && (!drEachShip["sender_address1"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					strSenAdd1 = (string)drEachShip["sender_address1"];
					strBuilder.Append("N'"+strSenAdd1+"', ");
				}
				else
					strBuilder.Append("null, ");

				if ((drEachShip["sender_address2"]!=null) && (!drEachShip["sender_address2"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					strSenAdd2 = (string)drEachShip["sender_address2"];
					strBuilder.Append("N'"+strSenAdd2+"', ");
				}
				else
					strBuilder.Append("null, ");

				if ((drEachShip["sender_zipcode"]!=null) && (!drEachShip["sender_zipcode"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					strSenZip = (string)drEachShip["sender_zipcode"];
					strBuilder.Append("'"+strSenZip+"', ");
				}
				else
					strBuilder.Append("null, ");

				if ((drEachShip["sender_country"]!=null) && (!drEachShip["sender_country"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					strSenCountry = (string)drEachShip["sender_country"];
					strBuilder.Append("'"+strSenCountry+"', ");
				}
				else
					strBuilder.Append("null, ");

				if (strPayerType == "C")
				{
					if ((drEachShip["customer_mbg"]!=null) && (!drEachShip["customer_mbg"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						strMBG = (string)drEachShip["customer_mbg"];
						strBuilder.Append("'"+strMBG+"', ");
					}
					else
						strBuilder.Append("null, ");
				}
				else
				{
					if ((drEachShip["agent_mbg"]!=null) && (!drEachShip["agent_mbg"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						strMBG = (string)drEachShip["agent_mbg"];
						strBuilder.Append("'"+strMBG+"', ");
					}
					else
						strBuilder.Append("null, ");
				}

				if ((drEachShip["service_code"]!=null) && (!drEachShip["service_code"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					strServiceCode = (string)drEachShip["service_code"];
					strBuilder.Append("'"+strServiceCode+"', ");
				}
				else
					strBuilder.Append("null, ");

				//get "POD_datetime", "POD_exception", "person_incharge" and "POD_remark" from shipment tracking table and
				//insert into respective columns of invoice_detail table 
				strBuilder.Append("null, null, null, null, ");

				if ((drEachShip["insurance_surcharge"]!=null) && (!drEachShip["insurance_surcharge"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					decInsAmt = Convert.ToDecimal(drEachShip["insurance_surcharge"]);
				strBuilder.Append(decInsAmt+", ");

				if ((drEachShip["tot_freight_charge"]!=null) && (!drEachShip["tot_freight_charge"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					decTotFrgCharge = Convert.ToDecimal(drEachShip["tot_freight_charge"]);
				strBuilder.Append(decTotFrgCharge+", ");

				if ((drEachShip["tot_vas_surcharge"]!=null) && (!drEachShip["tot_vas_surcharge"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					decTotVasSurcharge = Convert.ToDecimal(drEachShip["tot_vas_surcharge"]);
				strBuilder.Append(decTotVasSurcharge+", ");

				if ((drEachShip["esa_surcharge"]!=null) && (!drEachShip["esa_surcharge"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					decEsaSurcharge = Convert.ToDecimal(drEachShip["esa_surcharge"]);
				strBuilder.Append(decEsaSurcharge+", ");

				//calculate invoice_amt
				strBuilder.Append(decInvoiceAmt+")");
				//update the invoice_amt into shipment table 
				//

				try
				{
					//insert into invoice detail table
					intInvDetInserted += InvoiceGenerationMgrDAL.InsertInvoiceHeader(strAppID, strEnterpriseID, strBuilder.ToString());

					//calculate invoice_status_exception surcharges
					decInvSurchargeAmt=CalculateSurcharge(strAppID, strEnterpriseID, strInvoiceNo, intBookingNo, strConsgNo, strPayerType);
					decInvoiceAmt += decInvSurchargeAmt;
					intAWBCnt++;

					//generate/update invoice summary dataset
					UpdateInvSumDS(ref dsInvSumm,"S",strServiceCode,decCharWt,decTotFrgCharge);
					UpdateInvSumDS(ref dsInvSumm,"I","INS",0,decInsAmt);
					UpdateInvSumDS(ref dsInvSumm,"E","ESA",0,decEsaSurcharge);
					UpdateInvSumDS(ref dsInvSumm,"V","VAS",0,decTotVasSurcharge);
					UpdateInvSumDS(ref dsInvSumm,"X","S&E",0,decInvSurchargeAmt);

					//update shipment table
					InvoiceGenerationMgrDAL.UpdateShipment(strAppID, strEnterpriseID, intBookingNo, strConsgNo, strInvoiceNo, strInvoiceDate, decInvoiceAmt);
				}
				catch(ApplicationException appException)
				{
					throw new ApplicationException(appException.Message,appException);
				}

			}
			return intRowsInserted;
		}

		public decimal CalculateSurcharge(string strAppID, string strEnterpriseID, string strInvoiceNo, int intBookNo, string strConsignmentNo, string strPayerType)
		{
			DataSet dsInvSurcharge=null;
			int intSurCnt=0, i=0, intRowsIns=0;
			string strStatusCode=null, strExceptionCode=null;
			decimal decSurcharge=0, decTotSur=0;
			dsInvSurcharge = InvoiceGenerationMgrDAL.GetInvoiceableSurcharges(strAppID, strEnterpriseID, intBookNo, strConsignmentNo);
			intSurCnt = dsInvSurcharge.Tables[0].Rows.Count;
			for(i=0;i<intSurCnt;i++)
			{
				strStatusCode=null; strExceptionCode=null; decSurcharge=0; decTotSur=0;
				DataRow drEachShip = dsInvSurcharge.Tables[0].Rows[i];

				if ((drEachShip["surcharge"]!=null) && (!drEachShip["surcharge"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					decSurcharge = Convert.ToDecimal(drEachShip["surcharge"]);

				if (decSurcharge > 0)
				{
					decTotSur += decSurcharge;
					strStatusCode = (string)drEachShip["status_code"];
					if ((drEachShip["exception_code"]!=null) && (!drEachShip["exception_code"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						strExceptionCode = (string)drEachShip["exception_code"];
					else
						strExceptionCode = " ";

					StringBuilder strBuilder = new StringBuilder();
					strBuilder.Append("insert into invoice_status_exception (applicationid, enterpriseid, invoice_no, consignment_no, status_code, ");
					strBuilder.Append("exception_code, surcharge) values (");
					strBuilder.Append("'"+strAppID+"', '"+strEnterpriseID+"', '"+strInvoiceNo+"', '"+strConsignmentNo+"', '"+strStatusCode+"', ");
					strBuilder.Append("'"+strExceptionCode+"', "+decSurcharge+")");
					try
					{
						intRowsIns += InvoiceGenerationMgrDAL.InsertInvoiceHeader(strAppID, strEnterpriseID, strBuilder.ToString());
					}
					catch(ApplicationException appException)
					{
						throw new ApplicationException(appException.Message,appException);
					}
				}
			}
			return decTotSur;
		}

		private void CreateInvSumDS(ref DataSet dsInvSumm)
		{
			DataTable dtInvSumm = new DataTable();
			dtInvSumm.Columns.Add(new DataColumn("summary_type", typeof(string)));
			dtInvSumm.Columns.Add(new DataColumn("summary_code", typeof(string)));
			dtInvSumm.Columns.Add(new DataColumn("tot_consignment", typeof(int)));
			dtInvSumm.Columns.Add(new DataColumn("tot_chargeable_wt", typeof(decimal)));
			dtInvSumm.Columns.Add(new DataColumn("invoice_amt", typeof(decimal)));

			/*DataRow drEach = dtInvSumm.NewRow();
			drEach["summary_type"] = "S";
			drEach["summary_code"] = "SVC";
			drEach["tot_consignment"] = 1;
			drEach["tot_chargeable_wt"] = 10;
			drEach["invoice_amt"] = 100;
			dtInvSumm.Rows.Add(drEach);*/

			dsInvSumm = new DataSet();
			dsInvSumm.Tables.Add(dtInvSumm);
		}

		private void UpdateInvSumDS(ref DataSet dsInvSumm, string strSummType, string strSummCode, decimal decCharWT, decimal decInvAmt)
		{
			int intDSCnt = dsInvSumm.Tables[0].Rows.Count;
			bool bFound=false;
			int intRowIndex=0, intTotConsg=0;
			decimal decChargeableWt=0, decInvoiceAmt=0;
			if (decInvAmt<1)
				return;

			for (int j=0;j<intDSCnt;j++)
			{
				DataRow drEachSumm = dsInvSumm.Tables[0].Rows[j];
				string strSummaryType = (string)drEachSumm["summary_type"];
				if (strSummaryType.Equals(strSummType))
				{
					string strSummaryCode = (string)drEachSumm["summary_code"];
					if (strSummaryCode.Equals(strSummCode))
					{
						bFound=true;
						intRowIndex=j;
						break;
					}
				}
			}
			if (bFound==true)
			{
				//update dataset
				DataRow dr = dsInvSumm.Tables[0].Rows[intRowIndex];
				intTotConsg = (Convert.ToInt32(dr["tot_consignment"])) + 1;
				dr["tot_consignment"] = intTotConsg;
				if (decCharWT > 0)
				{
					decChargeableWt = (Convert.ToDecimal(dr["tot_chargeable_wt"])) + decCharWT;
					dr["tot_chargeable_wt"] = decChargeableWt;
				}
				if (decInvAmt > 0)
				{
					decInvoiceAmt = (Convert.ToDecimal(dr["invoice_amt"])) + decInvAmt;
					dr["invoice_amt"] = decInvoiceAmt;
				}
				dsInvSumm.Tables[0].Rows[intRowIndex].AcceptChanges();
			}
			else
			{
				//insert dataset
				DataRow dr = dsInvSumm.Tables[0].NewRow();
				dr["summary_type"] = strSummType;
				dr["summary_code"] = strSummCode;
				dr["tot_consignment"] = 1;
				dr["tot_chargeable_wt"] = decCharWT;
				dr["invoice_amt"] = decInvAmt;
				dsInvSumm.Tables[0].Rows.Add(dr);
			}
			int intChk = dsInvSumm.Tables[0].Rows.Count;
			return;
		}

		private void UpdateInvSumTab(ref DataSet dsInvSumm, string strAppID, string strEnterpriseID, string strInvoiceNo)
		{
			int intRowCnt=dsInvSumm.Tables[0].Rows.Count;
			if (intRowCnt<1)
				return;

			for (int k=0;k<intRowCnt;k++)
			{
				DataRow dr = dsInvSumm.Tables[0].Rows[k];
				string strSummType = (string)dr["summary_type"];
				string strSummCode = (string)dr["summary_code"];
				int intTotConsg = Convert.ToInt32(dr["tot_consignment"]);
				decimal decCharWt = Convert.ToDecimal(dr["tot_chargeable_wt"]);
				decimal decInvAmt = Convert.ToDecimal(dr["invoice_amt"]);
				StringBuilder strBuilder = new StringBuilder();
				strBuilder.Append("insert into invoice_summary (applicationid, enterpriseid, invoice_no, summary_code_type, summary_code, ");
				strBuilder.Append("tot_consignment, tot_chargeable_wt, invoice_amt,sequence) values ('"+strAppID+"', '"+strEnterpriseID+"', '"+strInvoiceNo+"', ");
				strBuilder.Append("'"+strSummType+"', '"+strSummCode+"', "+intTotConsg+", "+decCharWt+", "+decInvAmt+",null)");
				try
				{
					InvoiceGenerationMgrDAL.InsertInvoiceHeader(strAppID, strEnterpriseID, strBuilder.ToString());
				}
				catch(ApplicationException appException)
				{
					throw new ApplicationException(appException.Message,appException);
				}
			}
			dsInvSumm.Tables[0].Rows.Clear();
			return;
		}

	}
}
