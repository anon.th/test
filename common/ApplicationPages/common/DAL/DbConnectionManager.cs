using System;
using System.Data;
using System.Collections;
using com.common.classes;


namespace com.common.DAL
{

	public struct ConnectionDetails
	{
		public String ServerName;
		public String DatabaseName; //Not applicable for Oracle Database
		public String UserID;
		public String Password;
	}

	/// <summary>
	/// This class is created as singleton class, which stores exactly one
	/// instance of type DbConnectionManager for the life of the application
	/// </summary>
	public class DbConnectionManager
	{
		private static DbConnectionManager dbConManager = null;
		private static Hashtable applicationSettings = null;

		/// <summary>
		/// Private constructor of this class. The constructor initializes applicationSettings hastable which 
		/// stores database related settings for the core database as well as settings for various enterprise specific
		/// database for that applications.<br>
		/// </summary>
		private DbConnectionManager()
		{

			String strAppID = System.Configuration.ConfigurationSettings.AppSettings["applicationID"];

			AppConfig applicationConfig = new AppConfig();
			applicationConfig.AppConfigSettings.ApplicationID = strAppID;
			applicationConfig.AppConfigSettings.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["coreDSN"];

			applicationConfig.AppConfigSettings.EnterpriseID = "";
			applicationConfig.AppConfigSettings.DBConnectionType = DAL.ConnectionType.Core;

			String coreDbProvider = System.Configuration.ConfigurationSettings.AppSettings["coreProvider"];

			if(coreDbProvider == "SQL")
			{
				applicationConfig.AppConfigSettings.ProviderType = DAL.DataProviderType.Sql;
			}
			else if(coreDbProvider == "OLEDB")
			{
				applicationConfig.AppConfigSettings.ProviderType = DAL.DataProviderType.Oledb;
			}
			else if(coreDbProvider == "ORACLE")
			{
				applicationConfig.AppConfigSettings.ProviderType = DAL.DataProviderType.Oracle;
			}

			

			applicationSettings = new Hashtable();
			applicationSettings[strAppID] = applicationConfig;


			ArrayList enterpriseList = GetCoreEnterprises(strAppID);

			foreach(CoreEnterprise coreEnterprise in enterpriseList)
			{
				ConfigSettings configSettings = new ConfigSettings();
				configSettings.ApplicationID = strAppID;
				configSettings.EnterpriseID = coreEnterprise.EnterpriseID;
				String dbProvider = coreEnterprise.DBProvider;

				if(dbProvider == "SQL")
				{
					configSettings.ProviderType = DAL.DataProviderType.Sql;
				}
				else if(dbProvider == "OLEDB")
				{
					configSettings.ProviderType = DAL.DataProviderType.Oledb;
				}
				else if(dbProvider == "ORACLE")
				{
					configSettings.ProviderType = DAL.DataProviderType.Oracle;
				}
				
				configSettings.DBConnectionType = DAL.ConnectionType.App;
				configSettings.ConnectionString = coreEnterprise.EnterpriseDSN;

				applicationConfig.setEnterpriseConfiguration(coreEnterprise.EnterpriseID,configSettings);
			}

			applicationSettings[strAppID] = applicationConfig;
			
		}

		/// <summary>
		/// Public static method to get one and only one instance of this class. When this method is called for 
		/// the first time the new instance of this class is created and returned. On successive calls of this method
		/// same instance is returned. Its a typical singleton implementation.
		/// </summary>
		/// <returns>instance of DbConnectionManager class</returns>
		public static DbConnectionManager GetInstance()
		{
			if(dbConManager == null)
			{
				dbConManager = new DbConnectionManager();
			}
			
			return dbConManager;
		}

		/// <summary>
		/// Get the DbConnection object for the database identified by the specified Provider and connection strings.
		/// </summary>
		/// <param name="strProvider">Database Provider (SQL/ORACLE/OLEDB)</param>
		/// <param name="strConString">Database connection string (URL) for the database</param>
		/// <returns>An  instance of type DbConnection. Returns null if not found or any error</returns>
		public DbConnection GetDirectDbConnection(String strProvider, String strConString)
		{	
			DbConnection dbCon = null;

			ConfigSettings configSettings = new ConfigSettings();
			configSettings.ConnectionString = strConString;
			
			if(strProvider == "SQL")
			{
				configSettings.ProviderType = DAL.DataProviderType.Sql;
			}
			else if(strProvider == "OLEDB")
			{
				configSettings.ProviderType = DAL.DataProviderType.Oledb;
			}
			else if(strProvider == "ORACLE")
			{
				configSettings.ProviderType = DAL.DataProviderType.Oracle;
			}


			switch (configSettings.ProviderType)
			{
				case DAL.DataProviderType.Sql :
					dbCon = new DbConSQLServer(configSettings);
					break;
				case DAL.DataProviderType.Oracle:
					dbCon = new DbConOracleServer(configSettings);
					break;
				case DAL.DataProviderType.Oledb:
					break;
			}
			return dbCon;
		}


		/// <summary>
		/// Get the DbConnection object for the  (Core) database for the specified applicationid
		/// </summary>
		/// <param name="strAppID">ApplicationID for the current application</param>
		/// <returns>An instance of type DbConnection. Returns null in case of any error.</returns>
		public DbConnection GetDbConnection(String strAppID)
		{	
			DbConnection dbCon = null;

			AppConfig appConfig = (AppConfig)applicationSettings[strAppID];

			if(appConfig == null)
			{
				Console.WriteLine("Application ID not found.. Please check the Web.config file");
				return dbCon;
			}
			switch (appConfig.AppConfigSettings.ProviderType)
			{
				case DAL.DataProviderType.Sql :
					dbCon = new DbConSQLServer(appConfig.AppConfigSettings);
					break;
				case DAL.DataProviderType.Oracle:
					dbCon = new DbConOracleServer(appConfig.AppConfigSettings);
					break;
				case DAL.DataProviderType.Oledb:
					break;
			}
			return dbCon;
		}


		/// <summary>
		/// Get the DbConnection object for the Enterprise specific database identified by the Applicationid and Enterpriseid
		/// from the in memory hashtable maintained within the singleton instance of this class.<br>
		/// If the entry for the configsettings not found in the hashtable, the method trys to read it from the database, if found
		/// updates the hashtable. Returns null if the connection not found
		/// </summary>
		/// <param name="strAppID">Application id for the current application</param>
		/// <param name="strEnterpriseID">Enterprise id for the current enterprise</param>
		/// <returns>An instance of type DbConnection. Returns null in case of any error.</returns>
		public DbConnection GetDbConnection(String strAppID, String strEnterpriseID)
		{
			DbConnection dbCon = null;

			AppConfig appConfig = (AppConfig)applicationSettings[strAppID];

			if(appConfig == null)
			{
				Console.WriteLine("Application ID not found.. Please check the Web.config file");
				return dbCon;
			}

			ConfigSettings enterpriseConfig = appConfig.getEnterpriseConfiguration(strEnterpriseID);

			if(enterpriseConfig == null)
			{
				enterpriseConfig = GetConfigSettingsForEnterprise(strAppID,strEnterpriseID);

				if(enterpriseConfig == null)
				{
					Console.WriteLine("Enterprise not found.. Please Consult System Administrator ");
					return dbCon;		
				}

				appConfig.setEnterpriseConfiguration(strEnterpriseID,enterpriseConfig);
			}

			

			switch (enterpriseConfig.ProviderType)
			{
				case DAL.DataProviderType.Sql :
					dbCon = new DbConSQLServer(enterpriseConfig);
					break;
				case DAL.DataProviderType.Oracle:
					dbCon = new DbConOracleServer(enterpriseConfig);
					break;
				case DAL.DataProviderType.Oledb:
					break;
			}
			return dbCon;
		}
		

		/// <summary>
		/// Get the type DataProviderType for the Database used for a particular enterprise identified by
		/// its applicationid and the enterpriseid
		/// </summary>
		/// <param name="strAppID">ApplicationID for the current application</param>
		/// <param name="strEnterpriseID">Enterprise id for the current enterprise</param>
		/// <returns>One of values from the enum DataProviderType</returns>
		public DataProviderType GetDbProviderType(String strAppID, String strEnterpriseID)
		{
			AppConfig appConfig = (AppConfig)applicationSettings[strAppID];
			ConfigSettings enterpriseConfig = appConfig.getEnterpriseConfiguration(strEnterpriseID);
			return enterpriseConfig.ProviderType;
		}


		/// <summary>
		/// Private method used inside the constructor of this class to get the details of all enterprises for the specified
		/// applicationid
		/// </summary>
		/// <param name="strAppID">ApplicationID for the current application</param>
		/// <returns>ArrayList containing objects of type CoreEnterprise</returns>
		private  ArrayList GetCoreEnterprises(String appID)
		{
			com.common.DAL.DbConnection dbCon = null;
			String strQry = null;
			IDbCommand dbCmd = null;
			ArrayList coreEnterpriseList = new ArrayList();
			DataSet coreDataSet = null;
			int i = 0, cnt = 0;


			dbCon = GetDbConnection(appID);
			strQry = "select enterpriseid,enterprise_name,db_connection,db_provider from core_enterprise where applicationid = "+"'"+appID+"'";
			dbCmd = dbCon.CreateCommand(strQry,CommandType.Text);
			coreDataSet = (DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
			cnt = coreDataSet.Tables[0].Rows.Count;

			
			for(i=0;i<cnt;i++)
			{
				DataRow drEach = coreDataSet.Tables[0].Rows[i];

				CoreEnterprise coreEnterprise = new CoreEnterprise();
				
				coreEnterprise.EnterpriseID = (String)drEach["enterpriseid"];
				coreEnterprise.EnterpriseName = (String)drEach["enterprise_name"];
				coreEnterprise.EnterpriseDSN = (String)drEach["db_connection"];
				coreEnterprise.DBProvider = (String)drEach["db_provider"];

				coreEnterpriseList.Add(coreEnterprise);
			}

			return coreEnterpriseList;
		}


		/// <summary>
		/// Private method for Getting the ConfigSettigns from the database for the database for the specified applicationid and enterpriseid
		/// </summary>
		/// <param name="strAppID">Application id for the current database</param>
		/// <param name="strEnterpriseID">Enterpriseid for the current database</param>
		/// <returns>An object of type ConfigSettings</returns>
		private ConfigSettings GetConfigSettingsForEnterprise(String strAppID, String strEnterpriseID)
		{
			ConfigSettings enterpriseConfigSettings = null;

			com.common.DAL.DbConnection dbCon = null;
			String strQry = null;
			IDbCommand dbCmd = null;

			

			DataSet coreDataSet = null;
			int  cnt = 0;


			dbCon = GetDbConnection(strAppID);

			if(dbCon == null)
			{
				Console.WriteLine("Core connection not found...");
				return enterpriseConfigSettings;
			}

			strQry = "select db_connection,db_provider from core_enterprise where applicationid = '"+strAppID+"' and enterpriseid = '"+strEnterpriseID+"'";
			
			dbCmd = dbCon.CreateCommand(strQry,CommandType.Text);
			
			coreDataSet = (DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
			
			cnt = coreDataSet.Tables[0].Rows.Count;

			if(cnt != 0)
			{
				DataRow drEach = coreDataSet.Tables[0].Rows[0];

				enterpriseConfigSettings = new ConfigSettings();

				enterpriseConfigSettings.ApplicationID = strAppID;
				enterpriseConfigSettings.EnterpriseID = strEnterpriseID;
				
				enterpriseConfigSettings.ConnectionString = (String)drEach["db_connection"];
				String dbProvider = (String)drEach["db_provider"];

				if(dbProvider == "SQL")
				{
					enterpriseConfigSettings.ProviderType = DAL.DataProviderType.Sql;
				}
				else if(dbProvider == "OLEDB")
				{
					enterpriseConfigSettings.ProviderType = DAL.DataProviderType.Oledb;
				}
				else if(dbProvider == "ORACLE")
				{
					enterpriseConfigSettings.ProviderType = DAL.DataProviderType.Oracle;
				}
				
				enterpriseConfigSettings.DBConnectionType = DAL.ConnectionType.App;
				
			}

			return enterpriseConfigSettings;
		}


		public static ConnectionDetails GetConnectionDetails(String strAppID, String strEnterpriseID)
		{
			ConnectionDetails conDetails  = new ConnectionDetails();

			AppConfig appConfig = (AppConfig)applicationSettings[strAppID];
			ConfigSettings enterpriseConfig =  appConfig.getEnterpriseConfiguration(strEnterpriseID);
			DataProviderType dbProvider = enterpriseConfig.ProviderType;
			String strConnStr = enterpriseConfig.ConnectionString;
			String strFindString=null;

			switch(dbProvider)
			{
				case DataProviderType.Sql:
					
					//strFindString = FindConnectionString(strConnStr, "data source=");
					strFindString = System.Configuration.ConfigurationSettings.AppSettings["crSQLDSN"];
					conDetails.ServerName=strFindString;

					strFindString = FindConnectionString(strConnStr, "initial catalog=");
					conDetails.DatabaseName=strFindString;

					strFindString = FindConnectionString(strConnStr, "user id=");
					conDetails.UserID=strFindString;

					strFindString = FindConnectionString(strConnStr, "password=");
					conDetails.Password=strFindString;
					/*
					strFindString = System.Configuration.ConfigurationSettings.AppSettings["crOracleDSN"];
					conDetails.ServerName=strFindString;
					strFindString = "APPDB";
					conDetails.UserID=strFindString;
					strFindString = "APPDB";
					conDetails.Password=strFindString;
					*/
					break;

				case DataProviderType.Oracle:
					//strFindString = FindConnectionString(strConnStr, "data source=");
					strFindString = System.Configuration.ConfigurationSettings.AppSettings["crOracleDSN"];
					conDetails.ServerName=strFindString;

					strFindString = FindConnectionString(strConnStr, "user id=");
					conDetails.UserID=strFindString;

					strFindString = FindConnectionString(strConnStr, "password=");
					conDetails.Password=strFindString;
					break;
				case DataProviderType.Oledb:
					break;
			}

			return conDetails;
		}
		

		public static ConnectionDetails GetConnectionReportPublisher(String strAppID, String strEnterpriseID)
		{
			ConnectionDetails conDetails  = new ConnectionDetails();
			AppConfig appConfig = (AppConfig)applicationSettings[strAppID];
			ConfigSettings enterpriseConfig =  appConfig.getEnterpriseConfiguration(strEnterpriseID);
			DataProviderType dbProvider = enterpriseConfig.ProviderType;
			String strConnStr = enterpriseConfig.ConnectionString;
			String strFindString=null;
			switch(dbProvider)
			{
				case DataProviderType.Sql:
					strFindString = System.Configuration.ConfigurationSettings.AppSettings["crIReportPublisherDSN"];
					conDetails.ServerName=strFindString;

					strFindString = FindConnectionString(strConnStr, "initial catalog=");
					conDetails.DatabaseName=strFindString;

					strFindString = FindConnectionString(strConnStr, "user id=");
					conDetails.UserID=strFindString;

					strFindString = FindConnectionString(strConnStr, "password=");
					conDetails.Password=strFindString;
					break;

				case DataProviderType.Oracle:
					break;
				case DataProviderType.Oledb:
					break;
			}

			return conDetails;
		}
		
		private static String FindConnectionString(String strConnectionString, String strIndexString)
		{
			int intIndexStart=0, intIndexEnd=0;
			String strStringFound=null, strConStr=null;

			strConStr = strConnectionString.ToUpper();
			
			intIndexStart = strConStr.IndexOf(strIndexString.ToUpper());
			if (intIndexStart >= 0)
			{
				intIndexEnd = strConnectionString.IndexOf(";",intIndexStart);
				if (intIndexEnd >= 0)
				{
					strStringFound = strConnectionString.Substring((intIndexStart+strIndexString.Length),(intIndexEnd-(intIndexStart+strIndexString.Length)));
				}
			}
			return strStringFound;
		}

	}
}
