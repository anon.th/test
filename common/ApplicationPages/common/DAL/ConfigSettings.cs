using System;

namespace com.common.DAL
{
	/// <summary>
	/// Summary description for ConfigSettings. This class is used to store various
	/// database related settings as ConfigSettings object - properties. 
	/// </summary>
	public class ConfigSettings
	{
		protected String m_strApplicationID = null;
		protected String m_strEnterpriseID = null;
		protected DataProviderType m_enumDbProvider;
		protected ConnectionType m_enumConnType;
		protected String m_strConString = null;

		public ConfigSettings()
		{


		}

		public ConfigSettings(String strApplicationID,String strEnterpriseID,DataProviderType enumDbProvider,ConnectionType enumConnType )
		{
			m_strApplicationID=strApplicationID;
			m_strEnterpriseID=strEnterpriseID;
			m_enumDbProvider=enumDbProvider;
			m_enumConnType=enumConnType;
		}
		

		public string ApplicationID
		{
			set
			{
				m_strApplicationID=value;
			}
			get
			{
				return m_strApplicationID;
			}
		}

		public string EnterpriseID
		{
			set
			{
				m_strEnterpriseID=value;
			}
			get
			{
				return m_strEnterpriseID;
			}
		}
		public DataProviderType ProviderType
		{
			set
			{
				m_enumDbProvider=value;
			}
			get
			{
				return m_enumDbProvider;
			}
		}
		public ConnectionType DBConnectionType
		{
			set
			{
				m_enumConnType=value;
			}
			get
			{
				return m_enumConnType;
			}
		}
		public String ConnectionString
		{
			set
			{
				m_strConString=value;
			}
			get
			{
				return m_strConString;
			}
		}
	}
}
