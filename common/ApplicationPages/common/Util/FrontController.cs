using System;
using System.IO;
using System.Xml;
using System.Web;

namespace com.common.util
{
	/// <summary>
	/// 
	///  This class encapsulates typical FrontController responsibilities like 
	///  Redirecting to the next page, Showing a popup window, Transfering the control to the
	///  next URL, returning the next URL. 
	///  
	///  The class is implemented as Singleton. The one and only one instance of the class is
	///  maintained across the application and is accissible using static method GetInstance().
	///  
	///  The instance of the class can not be created using new operator as constructor is private.
	///  
	///  When the instance is created for the first time, it is initialized from an XML document. The path of
	///  that XML document should be specified in the Web.Config file under <b>appSettings section</b> with a 
	///  key name <b>fcDataFile</b>. For details about format and content of XML document, refer to the inline
	///  coments in the sample XML file <i>fcDataFile.xml</i>.
	///
	/// </summary>
	public class FrontController
	{

		/// <summary>
		/// static variable to hold the single instance of this class to support singleton implementation.
		/// </summary>
		private static FrontController m_FrontController = null;

		/// <summary>
		/// Member variable to hold the xml document in memory during the life of this class.
		/// </summary>
		private XmlDocument m_xmlFCData = null;


		/// <summary>
		/// private constructor. Hence instance of this class can not be created from within methods of any other class.
		/// </summary>
		private FrontController()
		{
			//Read the path of the xml document from the config file.
			String strFilePath = System.Configuration.ConfigurationSettings.AppSettings["fcDataFile"];

			//Create the xml document and load it.
			m_xmlFCData = new XmlDocument();
			m_xmlFCData.Load(strFilePath);


		}

		/// <summary>
		/// Public static method to get one and only one instance of this class. When this method is called for 
		/// the first time the new instance of this class is created and returned. On successive calls of this method
		/// same instance is returned. Its a typical singleton   implementation.
		/// </summary>
		/// <returns></returns>
		public static FrontController GetInstance()
		{
			if(m_FrontController == null)
			{
				m_FrontController = new FrontController();
			}

			return m_FrontController;
		}

		/// <summary>
		/// Transfer the page control to the next page for the specified button on the specified page.<br><br>
		///	If the <b>throwFCException</b> flag in the web.config file is set to true then the method
		///	throws ApplicationException with appropriate message in case of any error. <br>
		///	If this flag is set to false, then the control will not be transferred to the next page
		///	in case of any error.
		/// </summary>
		/// <param name="strBtn">The string id representing the button being clicked.</param>
		/// <param name="strCurrentPage">Name of the page containing the button being clicked. e.g. Page1.aspx</param>
		/// <param name="context">Page.Context object for the current page</param>
		public void PageTransfer(String strBtn, String strCurrentPage, HttpContext context)
		{

			String strNextURL = null;

			strNextURL = NextURL(strBtn,strCurrentPage);


			if(strNextURL != null && strNextURL.Length > 0)
			{
				context.Server.Transfer(strNextURL);
			}
		}
		

		/// <summary>
		/// Redirect to the next page for the specified button on the specified page.<br><br>
		///	If the <b>throwFCException</b> flag in the web.config file is set to true then the method
		///	throws ApplicationException with appropriate message in case of any error. <br>
		///	If this flag is set to false, then the page will not be redirected to the next page
		///	in case of any error.
		/// </summary>
		/// <param name="strBtn">The string id representing the button being clicked.</param>
		/// <param name="strCurrentPage">Name of the page containing the button being clicked. e.g. Page1.aspx</param>
		/// <param name="context">Page.Context object for the current page</param>
		public void PageRedirect(String strBtn, String strCurrentPage,HttpContext context)
		{
			String strNextURL = null;

			strNextURL = NextURL(strBtn,strCurrentPage);


			if(strNextURL != null && strNextURL.Length > 0)
			{
				context.Response.Redirect(strNextURL);
			}
		}


		/// <summary>
		/// Transfer the page control to the error page for the specified page.<br><br>
		///	If the <b>throwFCException</b> flag in the web.config file is set to true then the method
		///	throws ApplicationException with appropriate message in case of any error. <br>
		///	If this flag is set to false, then the control will not be transferred to the error page
		///	in case of any error.
		/// </summary>
		/// <param name="strCurrentPage">Name of the page e.g. Page1.aspx</param>
		/// <param name="context">Page.Context object for the current page</param>
		public void ErrorTransfer(String strCurrentPage,HttpContext context)
		{
			String strNextURL = null;

			strNextURL = GetErrorURL(strCurrentPage);
			
			if(strNextURL != null && strNextURL.Length > 0)
			{
				context.Server.Transfer(strNextURL);
			}
		}


		/// <summary>
		/// Redirect to the error page for the specified page.<br><br>
		///	If the <b>throwFCException</b> flag in the web.config file is set to true then the method
		///	throws ApplicationException with appropriate message in case of any error. <br>
		///	If this flag is set to false, then the control will not be redirected to the error page
		///	in case of any error.
		/// </summary>
		/// <param name="strCurrentPage">Name of the current page e.g. Page1.aspx</param>
		/// <param name="context">Page.Context object for the current page</param>
		public void ErrorRedirect(String strCurrentPage,HttpContext context)
		{
			String strNextURL = null;

			strNextURL = GetErrorURL(strCurrentPage);
			
			if(strNextURL != null && strNextURL.Length > 0)
			{
				context.Response.Redirect(strNextURL);
			}
		}

		/// <summary>
		/// Get the Next Page URL for the specified button on the specified page.
		/// </summary>
		/// <param name="strBtn">The string id representing the button being clicked.</param>
		/// <param name="strCurrentPage">Name of the page containing the button being clicked. e.g. Page1.aspx</param>
		/// <returns>The next page URL as string value<br><br>
		///			 If the <b>throwFCException</b> flag in the web.config file is set to true then the method
		///			 throws ApplicationException with appropriate message in case of any error.<br>
		///			 If this flag is set to false, then the method will return  <i>null</i> in case of any error.
		/// </returns>
		public String NextURL(String strBtn, String strCurrentPage)
		{

			String strNextURL = null;
			String strIsThrowFCException = System.Configuration.ConfigurationSettings.AppSettings["throwFCException"];
			if(strIsThrowFCException != null)
			{
				strIsThrowFCException = strIsThrowFCException.ToUpper();
			}

			XmlNode root = m_xmlFCData.DocumentElement;
			XmlNode pageNode = root.SelectSingleNode("//page[@name='"+strCurrentPage+"']");
			if(pageNode != null)
			{
				XmlNode buttonNode = pageNode.SelectSingleNode("button[@name='"+strBtn+"']");

				if(buttonNode != null && buttonNode.InnerText != null && buttonNode.InnerText.Length > 0)
				{
					strNextURL = buttonNode.InnerText;
				}
				else
				{
					strNextURL = GetDefaultURL(pageNode);

					if(strNextURL == null && strIsThrowFCException == "TRUE")
					{
						throw new ApplicationException("FCException. Default Next URL for the Page :"+strCurrentPage+": is not defined in the xml document.");
					}
				}
			}
			else
			{
				if(strIsThrowFCException == "TRUE")
				{
					throw new ApplicationException("FCException. Page section for the Page :"+strCurrentPage+": is not defined in the xml document.");
				}
			}

			return strNextURL;
		}



		/// <summary>
		/// Get the Error Page URL for the specified page.
		/// </summary>
		/// <param name="strCurrentPage">Name of the current page e.g. Page1.aspx</param>
		/// <returns>The Error Page URL as string value<br><br>
		///			 If the <b>throwFCException</b> flag in the web.config file is set to true then the method
		///			 throws ApplicationException with appropriate message in case of any error.<br>If this flag is set to false, then the method will return
		///			 <i>null</i> in case of any error.
		/// </returns>
		public String GetErrorURL(String strCurrentPage)
		{
			String strErrorURL = null;

			XmlNode root = m_xmlFCData.DocumentElement;
			XmlNode pageNode = root.SelectSingleNode("//page[@name='"+strCurrentPage+"']");

			String strIsThrowFCException = System.Configuration.ConfigurationSettings.AppSettings["throwFCException"];
			if(strIsThrowFCException != null)
			{
				strIsThrowFCException = strIsThrowFCException.ToUpper();
			}


			if(pageNode != null)
			{
				XmlNode errorNode = pageNode.SelectSingleNode("error");

				if(errorNode != null && errorNode.InnerText != null && errorNode.InnerText.Length > 0)
				{
					strErrorURL = errorNode.InnerText;
				}
				else
				{
					if(strIsThrowFCException == "TRUE")
					{
						throw new ApplicationException("FCException. Error section for the Page :"+strCurrentPage+": is not defined in the xml document.");
					}
				}
			}
			else
			{
				if(strIsThrowFCException == "TRUE")
				{
					throw new ApplicationException("FCException. Page section for the Page :"+strCurrentPage+": is not defined in the xml document.");
				}
			}

			return strErrorURL;
		}


		/// <summary>
		/// Show next page for the specified button on the specified page in the popup window.<br><br>
		///	If the <b>throwFCException</b> flag in the web.config file is set to true then the method
		///	throws ApplicationException with appropriate message in case of any error. <br>
		///	If this flag is set to false, then the popup will not be shown
		///	in case of any error.
		/// </summary>
		/// <param name="strBtn">The string id representing the button being clicked.</param>
		/// <param name="strCurrentPage">Name of the page containing the button being clicked. e.g. Page1.aspx</param>
		/// <param name="context">Page.Context object for the current page</param>
		/// <param name="strQueryString">QueryString to be passed to the Popup page.<br>
		/// This string value will be concatenated to the popup URL as PopupPage.aspx?QUERY_STRING<br>
		/// </param>
		public void ShowPopup(String strBtn, String strCurrentPage,HttpContext context,String strQueryString)
		{
			String strNextURL = null;

			XmlNode root = m_xmlFCData.DocumentElement;
			XmlNode pageNode = root.SelectSingleNode("//page[@name='"+strCurrentPage+"']");

			String strIsThrowFCException = System.Configuration.ConfigurationSettings.AppSettings["throwFCException"];
			if(strIsThrowFCException != null)
			{
				strIsThrowFCException = strIsThrowFCException.ToUpper();
			}

			
			if(pageNode != null)
			{
				XmlNode buttonNode = pageNode.SelectSingleNode("button[@name='"+strBtn+"']");

				if(buttonNode != null && buttonNode.InnerText != null && buttonNode.InnerText.Length > 0)
				{
					strNextURL = buttonNode.InnerText;
					XmlAttribute atribFeatures = buttonNode.Attributes["features"];
					String strFeatures = "";

					if(atribFeatures != null)
					{
						strFeatures = atribFeatures.InnerText;
					}
					else
					{
						strFeatures = "";
					}

					XmlAttribute atribTarget = buttonNode.Attributes["target"];
					String strTarget = "";

					if(atribTarget != null)
					{
						strTarget = atribTarget.InnerText;
					}
					else
					{
						strTarget = "";
					}

					XmlAttribute atribReplace = buttonNode.Attributes["replace"];
					String strIsReplace = "";

					if(atribReplace != null)
					{
						strIsReplace = atribReplace.InnerText;
					}
					else
					{
						strIsReplace = "true";
					}

					String strUrl = strNextURL;
				
					if(strQueryString != null && strQueryString.Length > 0)
					{						
						strUrl += "?" + strQueryString;
					}
				
					String strScript ="";

					strScript += "<script language=javascript>";
					strScript += "window.open('" + strUrl + "','"+strTarget+"','" + strFeatures + "',"+strIsReplace+");";
					strScript += "</script>";
				
					context.Response.Write(strScript);
				}
				else
				{
					if(strIsThrowFCException == "TRUE")
					{
						throw new ApplicationException("FCException. button section :"+strBtn+": for the Page :"+strCurrentPage+": is not defined properly in the xml document.");
					}

				}
			}
			else
			{
				if(strIsThrowFCException == "TRUE")
				{
					throw new ApplicationException("FCException. Page section for the Page :"+strCurrentPage+": is not defined in the xml document.");
				}

			}
		}



		/// <summary>
		/// Private method used to Get the Default Page URL for the specified page node.
		/// </summary>
		/// <param name="pageNode">page node as XmlNode object.</param>
		/// <returns>String value representing the default page URL. null if the default section is not defined in the page section.</returns>
		private String GetDefaultURL(XmlNode pageNode)
		{
			String strDefaultURL = null;

			XmlNode defaultNode = pageNode.SelectSingleNode("default");

			if(defaultNode != null && defaultNode.InnerText != null && defaultNode.InnerText.Length > 0)
			{
				strDefaultURL = defaultNode.InnerText;
			}

			return strDefaultURL;
		}


	}



}