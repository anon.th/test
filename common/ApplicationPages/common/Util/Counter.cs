using System;
using System.Data;
using com.common.DAL;



namespace com.common.util
{
	/// <summary>
	/// The class has various static methods to interface with the appdb.Counter table.
	/// </summary>
	public class Counter
	{

		/// <summary>
		/// Private constructor which does nothing.
		/// </summary>
		private Counter()
		{
		}

		/// <summary>
		/// Get the next count for the specified CounterCode for specified applicationid and enterpriseid.
		/// Reading the current value of the count from the Counter table and its incrementing operation takes
		/// place in a transaction. Hence the returned count value will always be ensured tobe unique when multiple users are trying
		/// to get next count for the same counter code.
		/// </summary>
		/// <param name="strAppID">ApplicationID for the current application</param>
		/// <param name="strEnterpriseID">EnterpriseID for the current enterprise</param>
		/// <param name="strCounterCode">one of valid counter code in the counter table</param>
		/// <returns>Next count value as long</returns>
		public static decimal GetNext(String strAppID, String strEnterpriseID, String strCounterCode)
		{

			decimal currentValue = 0;
			
			DbConnection dbCon1 = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			IDbConnection con1 = dbCon1.GetConnection();
			dbCon1.OpenConnection(ref con1);			
			IDbTransaction transaction = dbCon1.BeginTransaction(con1,IsolationLevel.ReadCommitted);
			String strSQLQuery = null;
			//String strSQLQuery = "select * from counter";
			DataProviderType dbProvider = DbConnectionManager.GetInstance().GetDbProviderType(strAppID,strEnterpriseID);
			//IDbCommand cmd = dbCon1.CreateCommand(strSQLQuery,CommandType.Text);

			#region comment
			switch(dbProvider)
			{

				case DataProviderType.Oracle:
					strSQLQuery = "select * from counter where code_name = '"+strCounterCode+"' and applicationid = '"+strAppID+"' and enterpriseid = '"+strEnterpriseID+"' for update";
					break;

				case DataProviderType.Oledb:
					break;

				case DataProviderType.Sql:
				default:

					strSQLQuery = "select * from counter with (updlock) where code_name = '"+strCounterCode+"' and applicationid = '"+strAppID+"' and enterpriseid = '"+strEnterpriseID+"'";
					break;
			}


			IDbCommand cmd = dbCon1.CreateCommand(strSQLQuery,CommandType.Text);
			cmd.Connection = con1;
			cmd.Transaction = transaction;
			DataSet dsCounter = (DataSet)dbCon1.ExecuteQueryInTransaction(cmd,ReturnType.DataSetType);
			int cnt = dsCounter.Tables[0].Rows.Count;

			//GetNext(trAppID, strEnterpriseID, strCounterCode, dbCmd, dbCon, conApp, tranApp);

			if(cnt > 0)
			{
				DataRow drFirst = dsCounter.Tables[0].Rows[0];
				currentValue =Convert.ToDecimal((drFirst["current_value"]));
				//currentValue =long.Parse(drFirst["current_value"].ToString());

				//Increment current counter value by one

				currentValue++;

				strSQLQuery = "Update Counter set current_value = "+currentValue+" where code_name = '"+strCounterCode+"' ";
				strSQLQuery += "and applicationid = '"+strAppID+"' and enterpriseid = '"+strEnterpriseID+"'";

				cmd.CommandText = strSQLQuery;

				
				dbCon1.ExecuteNonQueryInTransaction(cmd);
				transaction.Commit();
				con1.Close();
			}
			else
			{
				transaction.Rollback();
				con1.Close();
			}
			#endregion

			//currentValue = GetNext(strAppID, strEnterpriseID, strCounterCode,ref cmd, ref dbCon1,ref con1,ref transaction);
			//Add by Sompote
//			if(con1 != null)
//			{
//				con1.Close();
//			}
			return currentValue;
		}

		/// <summary>
		/// Get the next count for the specified CounterCode with a prefix (as specified by the argument)for specified applicationid and enterpriseid.
		/// Reading the current value of the count from the Counter table and its incrementing operation takes
		/// place in a transaction. Hence the returned count value will always be ensured tobe unique when multiple users are trying
		/// to get next count for the same counter code.
		/// </summary>
		/// <param name="strAppID"></param>
		/// <param name="strEnterpriseID"></param>
		/// <param name="strCounterCode"></param>
		/// <param name="dbCmd"></param>
		/// <param name="dbCon"></param>
		/// <param name="conApp"></param>
		/// <param name="tranApp"></param>
		/// <returns></returns>
		/// Create by Aoo 26/03/2010
		public static decimal GetNext(String strAppID, String strEnterpriseID, String strCounterCode,ref IDbCommand dbCmd, ref DbConnection dbCon, ref IDbConnection conApp, ref IDbTransaction tranApp)
		{

			decimal currentValue = 0;
		
			//dbCon.OpenConnection(ref conApp);
			//tranApp = dbCon.BeginTransaction(conApp, IsolationLevel.ReadCommitted);

			//-------------------------------------------------------------------------------------
			String strSQLQuery = null;
			
			DataProviderType dbProvider = DbConnectionManager.GetInstance().GetDbProviderType(strAppID,strEnterpriseID);

			switch(dbProvider)
			{

				case DataProviderType.Oracle:
					strSQLQuery = "select * from counter where code_name = '"+strCounterCode+"' and applicationid = '"+strAppID+"' and enterpriseid = '"+strEnterpriseID+"' for update";
					break;

				case DataProviderType.Oledb:
					break;

				case DataProviderType.Sql:
				default:

					strSQLQuery = "select * from counter with (updlock) where code_name = '"+strCounterCode+"' and applicationid = '"+strAppID+"' and enterpriseid = '"+strEnterpriseID+"'";
					break;
			}

			dbCmd = dbCon.CreateCommand(strSQLQuery,CommandType.Text);
			dbCmd.Connection = conApp;
			dbCmd.Transaction = tranApp;
			DataSet dsCounter = (DataSet)dbCon.ExecuteQueryInTransaction(dbCmd,ReturnType.DataSetType);
			int cnt = dsCounter.Tables[0].Rows.Count;


			if(cnt > 0)
			{
				int intRowEffect = 0;
				DataRow drFirst = dsCounter.Tables[0].Rows[0];
				currentValue =Convert.ToDecimal((drFirst["current_value"]));
				//currentValue =long.Parse(drFirst["current_value"].ToString());

				//Increment current counter value by one

				currentValue++;

				strSQLQuery = "Update Counter set current_value = "+currentValue+" where code_name = '"+strCounterCode+"' ";
				strSQLQuery += "and applicationid = '"+strAppID+"' and enterpriseid = '"+strEnterpriseID+"'";

				//dbCmd.CommandText = strSQLQuery;
				dbCmd = dbCon.CreateCommand(strSQLQuery,CommandType.Text);
				dbCmd.Connection = conApp;
				dbCmd.Transaction = tranApp;
				intRowEffect = dbCon.ExecuteNonQueryInTransaction(dbCmd);
				//tranApp.Commit();


			}
			else
			{
				//tranApp.Rollback();
			}		
			return currentValue;
		}


		/// <summary>
		/// Get the next count for the specified CounterCode with a prefix (as specified by the argument)for specified applicationid and enterpriseid.
		/// Reading the current value of the count from the Counter table and its incrementing operation takes
		/// place in a transaction. Hence the returned count value will always be ensured tobe unique when multiple users are trying
		/// to get next count for the same counter code.
		/// </summary>
		/// <param name="strAppID">ApplicationID for the current application</param>
		/// <param name="strEnterpriseID">EnterpriseID for the current enterprise</param>
		/// <param name="strCounterCode">one of valid counter code in the counter table</param>
		/// <param name="strPrefix">Prefix string to be prefixed to the new count value</param>
		/// <returns>Next prefixed count as string value</returns>
		public static String GetNext(String strAppID, String strEnterpriseID, String strCounterCode, String strPrefix)
		{
			String strNextCount = "" + strPrefix;
			strNextCount += GetNext(strAppID,strEnterpriseID,strCounterCode);

			return strNextCount;
		}
		public static String GetNext(String strAppID, String strEnterpriseID, String strCounterCode, String strPrefix,DbConnection dbCon,  IDbCommand dbCmd)
		{
			String strNextCount = "" + strPrefix;
			strNextCount += GetNext(strAppID,strEnterpriseID,strCounterCode, dbCon, dbCmd);

			return strNextCount;
		}
		public static decimal GetNext(String strAppID, String strEnterpriseID, String strCounterCode,  DbConnection dbCon, IDbCommand dbCmd)
		{

			decimal currentValue = 0;
		
			//dbCon.OpenConnection(ref conApp);
			//tranApp = dbCon.BeginTransaction(conApp, IsolationLevel.ReadCommitted);

			//-------------------------------------------------------------------------------------
			String strSQLQuery = null;
			DataProviderType dbProvider = DbConnectionManager.GetInstance().GetDbProviderType(strAppID,strEnterpriseID);

			switch(dbProvider)
			{

				case DataProviderType.Oracle:
					strSQLQuery = "select * from counter where code_name = '"+strCounterCode+"' and applicationid = '"+strAppID+"' and enterpriseid = '"+strEnterpriseID+"' for update";
					break;

				case DataProviderType.Oledb:
					break;

				case DataProviderType.Sql:
				default:

					strSQLQuery = "select * from counter where code_name = '"+strCounterCode+"' and applicationid = '"+strAppID+"' and enterpriseid = '"+strEnterpriseID+"'";
					break;
			}

			//dbCmd = dbCon.CreateCommand(strSQLQuery,CommandType.Text);
			//dbCmd.Connection = conApp;
			//dbCmd.Transaction = tranApp;
			dbCmd.CommandText = strSQLQuery;
			DataSet dsCounter = (DataSet)dbCon.ExecuteQueryInTransaction(dbCmd,ReturnType.DataSetType);
			int cnt = dsCounter.Tables[0].Rows.Count;

			if(cnt > 0)
			{
				DataRow drFirst = dsCounter.Tables[0].Rows[0];
				currentValue =Convert.ToDecimal((drFirst["current_value"]));
				//currentValue =long.Parse(drFirst["current_value"].ToString());

				//Increment current counter value by one

				currentValue++;

				strSQLQuery = "Update Counter set current_value = "+currentValue+" where code_name = '"+strCounterCode+"' ";
				strSQLQuery += "and applicationid = '"+strAppID+"' and enterpriseid = '"+strEnterpriseID+"'";

				dbCmd.CommandText = strSQLQuery;
				dbCon.ExecuteNonQueryInTransaction(dbCmd);
				//tranApp.Commit();


			}
			else
			{
				//tranApp.Rollback();
			}

			return currentValue;
		}

		/// <summary>
		/// Increaments the count for the specified counter code by 1 in a transaction. Resets the count before a new
		/// month starts. For this ResetEveryMonth logic to work the counter should be initialized as below: <br>
		/// 1MM0000 - where <br>
		/// MM - represents the current month e.g. for January use 01, for November use 11.
		/// Leading 1 is used to prevent trimming of leading zeros when number is converted in to string.
		/// the max value of the count range from 0000 to 9999.
		/// </summary>
		/// <param name="strAppID">ApplicationID for the current application</param>
		/// <param name="strEnterpriseID">EnterpriseID for the current enterprise</param>
		/// <param name="strCounterCode">one of valid counter code in the counter table</param>
		/// <returns>Returns the 4 digit count value as string</returns>
		public static String GetNextResetEveryMonth(String strAppID, String strEnterpriseID, String strCounterCode)
		{
			String strCurrentValue = null;

			decimal currentValue = 0;
			decimal maxValue = 0;
			
			DbConnection dbCon1 = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			IDbConnection con1 = dbCon1.GetConnection();
			dbCon1.OpenConnection(ref con1);			
			IDbTransaction transaction = dbCon1.BeginTransaction(con1,IsolationLevel.ReadCommitted);
			String strSQLQuery = null;
			
			DataProviderType dbProvider = DbConnectionManager.GetInstance().GetDbProviderType(strAppID,strEnterpriseID);

			switch(dbProvider)
			{

				case DataProviderType.Oracle:
					strSQLQuery = "select * from counter  where code_name = '"+strCounterCode+"' and applicationid = '"+strAppID+"' and enterpriseid = '"+strEnterpriseID+"' for update";
					break;

				case DataProviderType.Oledb:
					break;

				case DataProviderType.Sql:
				default:

					strSQLQuery = "select * from counter with (updlock) where code_name = '"+strCounterCode+"' and applicationid = '"+strAppID+"' and enterpriseid = '"+strEnterpriseID+"'";
					break;
			}


			IDbCommand cmd = dbCon1.CreateCommand(strSQLQuery,CommandType.Text);
			cmd.Connection = con1;
			cmd.Transaction = transaction;
			DataSet dsCounter = (DataSet)dbCon1.ExecuteQueryInTransaction(cmd,ReturnType.DataSetType);
			int cnt = dsCounter.Tables[0].Rows.Count;


			if(cnt > 0)
			{
				DataRow drFirst = dsCounter.Tables[0].Rows[0];
				currentValue = Convert.ToDecimal(drFirst["current_value"]);
				maxValue = Convert.ToDecimal(drFirst["max_value"]);

				if(currentValue <= maxValue)
				{

					//Increment current counter value by one

					//check month changed or not. If changed then reset counter as YYMM0000

					strCurrentValue = Convert.ToString(currentValue);
					String strMonth = strCurrentValue.Substring(1,2);

					DateTime dtNow = DateTime.Now;
					String strMonthNow = dtNow.ToString("MM");
					if(strMonthNow != strMonth)
					{
						currentValue = Convert.ToInt64("1" + strMonthNow + "0001");
						maxValue = Convert.ToInt64("1"+ strMonthNow + "9999");
					}
					else
					{
						currentValue++;
					}





					strSQLQuery = "Update Counter set current_value = "+currentValue+", max_value = "+ maxValue+" where code_name = '"+strCounterCode+"' ";
					strSQLQuery += "and applicationid = '"+strAppID+"' and enterpriseid = '"+strEnterpriseID+"'";

					cmd.CommandText = strSQLQuery;

				
					dbCon1.ExecuteNonQueryInTransaction(cmd);
					transaction.Commit();
					con1.Close();
				}
				else
				{
					transaction.Rollback();
					con1.Close();
					strCurrentValue = null;
				}
			}
			else
			{
				transaction.Rollback();
				con1.Close();
			}

			if(strCurrentValue != null)
			{
				strCurrentValue = Convert.ToString(currentValue);
				strCurrentValue = strCurrentValue.Substring(3,4);
			}

			return strCurrentValue;
		}



		/// <summary>
		/// 
		/// </summary>
		/// <param name="strAppID">ApplicationID for the current application</param>
		/// <param name="strEnterpriseID">EnterpriseID for the current enterprise</param>
		/// <param name="strCounterCode">one of valid counter code in the counter table</param>
		/// <param name="strPrefix">Prefix string to be prefixed to the new count value</param>
		/// <returns>Returns the prefixed 4 digit count value as string </returns>
		public static String GetNextResetEveryMonth(String strAppID, String strEnterpriseID, String strCounterCode, String strPrefix)
		{
			String strNextCount = "" + strPrefix;
			strNextCount += GetNextResetEveryMonth(strAppID,strEnterpriseID,strCounterCode);

			return strNextCount;
		}


		/// <summary>
		/// This method simply returns the current value of the count for the specified counter code.
		/// </summary>
		/// <param name="strAppID">ApplicationID for the current application</param>
		/// <param name="strEnterpriseID">EnterpriseID for the current enterprise</param>
		/// <param name="strCounterCode">one of valid counter code in the counter table</param>
		/// <returns>Returns the current count value</returns>
		public static decimal GetCurrent(String strAppID, String strEnterpriseID, String strCounterCode)
		{
			
			decimal currentValue = 0;
			
			DbConnection dbCon1 = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			String strSQLQuery = null;
			strSQLQuery = "select current_value from counter where code_name = '"+strCounterCode+"' and applicationid = '"+strAppID+"' and enterpriseid = '"+strEnterpriseID+"'";

			DataSet dsCounter = (DataSet)dbCon1.ExecuteQuery(strSQLQuery,ReturnType.DataSetType);
			int cnt = dsCounter.Tables[0].Rows.Count;


			if(cnt > 0)
			{
				DataRow drFirst = dsCounter.Tables[0].Rows[0];
				currentValue = Convert.ToDecimal(drFirst["current_value"]);
			}

			return currentValue;
		}
	}
}
