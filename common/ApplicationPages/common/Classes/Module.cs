using System;

namespace com.common.classes
{
	/// <summary>
	/// Summary description for Module.
	/// </summary>
	public class Module
	{
		private String m_strModuleID;
		private String m_strModuleName;
		private String m_strIconImage;
		private int m_iDisplayStyle;
		private String m_strLinkURL;
		private String m_strModuleRights;
		public Module()
		{
			//
			// TODO: Add constructor logic here
			//
		}
		public String ModuleId
		{
			set
			{
				m_strModuleID = value;
			}
			get
			{
				return m_strModuleID;
			}
			
		}
		
		public String ModuleName
		{
			set
			{
				m_strModuleName = value;
			}
			get
			{
				return m_strModuleName;
			}
			
		}
		
		public String ModuleIconImage
		{
			set
			{
				m_strIconImage = value;
			}
			get
			{
				return m_strIconImage;
			}
			
		}
		
		public int ModuleNameStyle
		{
			set
			{
				m_iDisplayStyle = value;
			}
			get
			{
				return m_iDisplayStyle;
			}
		}

		
		public String ModuleLinkURL
		{
			set
			{
				m_strLinkURL = value;
				
			}
			get
			{
				return m_strLinkURL;
			}
			
		}

		public String ModuleRights
		{
			set
			{
				m_strModuleRights = value;
			}
			get
			{
				return m_strModuleRights;
			}
		}
		
	}
}
