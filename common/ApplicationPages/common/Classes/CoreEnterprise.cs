using System;

namespace com.common.classes
{
	/// <summary>
	/// Summary description for Enterprise.
	/// </summary>
	public class CoreEnterprise
	{
		private String m_strEnterpriseID = null;
		private String m_strEnterprise_Name = null;
		private String m_strEnterprise_Type=null;
		private String m_strContact_Person = null;
		private String m_strLogo_URL = null;
		private String m_strStyleSheet_URL = null;
		private String m_strPhone_Number = null;
		private String m_strEnterprise_DSN = null;
		private String m_strDBProvider = null;
		private String m_strContentBgImageURL = null;
		private String m_strNavigationImageURL = null;
		private String m_strBannerBgImageURL = null;
		private String m_strWelcomeURL = null;
		private String m_strEnterpriseWebsite = null;
		private String m_strAdminCulture = null;
		private String m_strAdminPswd = null;

		public CoreEnterprise()
		{
			
		}

		public String AdminCulture
		{
			set
			{
				m_strAdminCulture = value;
			}
			get
			{
				return m_strAdminCulture;
			}
		}

		public String AdminPswd
		{
			set
			{
				m_strAdminPswd = value;
			}
			get
			{
				return m_strAdminPswd;
			}
		}

		public String EnterpriseID
		{
			set
			{
				m_strEnterpriseID = value;
			}
			get
			{
				return m_strEnterpriseID;
			}
		}

		public String EnterpriseName
		{
			set
			{
				m_strEnterprise_Name = value;
			}
			get
			{
				return m_strEnterprise_Name;
			}
		}

		public String EnterpriseType
		{
			set
			{
				m_strEnterprise_Type = value;
			}
			get
			{
				return m_strEnterprise_Type;
			}
		}

		public String ContactPerson
		{
			set
			{
				m_strContact_Person = value;
			}
			get
			{
				return m_strContact_Person;
			}
		}

		public String LogoURL
		{
			set
			{
				m_strLogo_URL = value;
			}
			get
			{
				return m_strLogo_URL;
			}
		}

		public String StyleSheetURL
		{
			set
			{
				m_strStyleSheet_URL = value;
			}
			get
			{
				return m_strStyleSheet_URL;
			}
		}

		public String PhoneNumber
		{
			set
			{
				m_strPhone_Number = value;
			}
			get
			{
				return m_strPhone_Number;
			}
		}

		public String EnterpriseDSN
		{
			set
			{
				m_strEnterprise_DSN = value;
			}
			get
			{
				return m_strEnterprise_DSN;
			}
		}

		public String DBProvider
		{
			set
			{
				m_strDBProvider = value;
			}
			get
			{
				return m_strDBProvider;
			}
		}

		public String ContentBgImageURL
		{
			set
			{
				m_strContentBgImageURL = value;
			}
			get
			{
				return m_strContentBgImageURL;
			}
		}

		public String NavigationBgImageURL
		{
			set
			{
				m_strNavigationImageURL = value;
			}
			get
			{
				return m_strNavigationImageURL;
			}
		}

		public String  BannerBgImageURL
		{
			set
			{
				m_strBannerBgImageURL = value;
			}
			get
			{
				return m_strBannerBgImageURL;
			}
		}

		public String WelcomeURL
		{
			set
			{
				m_strWelcomeURL = value;
			}
			get
			{
				return m_strWelcomeURL;
			}
		}

		public String EnterpriseWebsite
		{
			set
			{
				m_strEnterpriseWebsite = value;
			}
			get
			{
				return m_strEnterpriseWebsite;
			}
		}
	}
}
