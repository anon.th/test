using System;

namespace com.common.classes
{
	/// <summary>
	/// Summary description for SystemCode.
	/// </summary>
	public class SystemCode
	{

		private String    m_strText = null;
		private float       m_iNumValue = 0;
		private String    m_StringValue = null;


		public SystemCode()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		public String Text
		{
			get
			{
				return m_strText;
			}

			set
			{
				m_strText=value;
			}
		}

		public float NumValue
		{
			get
			{
				return m_iNumValue;
			}

			set
			{
				m_iNumValue=value;
			}
		}

		public String StringValue
		{
			get
			{
				return m_StringValue;
			}

			set
			{
				m_StringValue=value;
			}
		}



	}
}
