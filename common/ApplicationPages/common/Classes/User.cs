using System;

namespace com.common.classes
{
	/// <summary>
	/// Summary description for User
	/// </summary>
	public class User
	{
		private String m_strUserID = null;
		private String m_strUserName = null;
		private String m_strUserPswd = null;
		private String m_strCulture = null;
		private String m_strUsrDept = null;
		private String m_strUserEmail = null;
		private String m_UserType = null;
		private String m_strUserLocation = null;
		private String m_strCustomerAccount = null;
		private String m_strPayerId = null;

		public User()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		public String UserID
		{
			set
			{
				m_strUserID = value;
			}
			get
			{
				return m_strUserID;
			}
		}

		public String UserName
		{
			set
			{
				m_strUserName = value;
			}
			get
			{
				return m_strUserName;
			}
		}

		public String UserPswd
		{
			set
			{
				m_strUserPswd = value;
			}
			get
			{
				return m_strUserPswd;
			}
		}

		public String UserCulture
		{
			set
			{
				m_strCulture = value;
			}
			get
			{
				return m_strCulture;
			}
		}

		public String UserEmail
		{
			set
			{
				m_strUserEmail = value;
			}
			get
			{
				return m_strUserEmail;
			}
		}

		public String UserDepartment
		{
			set
			{
				m_strUsrDept = value;
			}
			get
			{
				return m_strUsrDept;
			}
		}

		public String UserType
		{
			set
			{
				m_UserType = value;
			}
			get
			{
				return m_UserType;
			}
		}
		public String UserLocation
		{
			set 
			{
				m_strUserLocation = value;
			}
			get 
			{
				return m_strUserLocation;
			}
		}
		public String CustomerAccount
		{
			set 
			{
				m_strCustomerAccount = value;
			}
			get 
			{
				return m_strCustomerAccount;
			}
		}

		public String PayerID
		{
			set
			{
				m_strPayerId = value;
			}
			get
			{
				return m_strPayerId;
			}
		}

	}
}
