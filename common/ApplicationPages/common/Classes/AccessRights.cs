using System;
using System.Runtime.Serialization;

namespace com.common.classes
{
	/// <summary>
	/// The instance of this class is returned by  RBAC.GetModuleAccessRights() method. It represents Module access rights,
	/// Insert, Update and Delete access rights for a particular module for the current user.
	/// </summary>
	[Serializable()]
	public class AccessRights
	{
		private bool m_bInsert;
		private bool m_bUpdate;
		private bool m_bDelete;
		private bool m_bModule;

		/// <summary>
		/// Inistializes all rights to be false. 
		/// </summary>
		public AccessRights()
		{
			m_bInsert = false;
			m_bUpdate = false;
			m_bDelete = false;
			m_bModule = false;
		}

		/// <summary>
		/// Property Insert right. True means user has insert rights in a particular module.
		/// </summary>
		public bool Insert
		{
			set
			{
				m_bInsert = value;
			}
			get
			{
				return m_bInsert;
			}
			
		}
		
		/// <summary>
		/// Property Update right. True means user has update rights in a particular module.
		/// </summary>
		public bool Update
		{
			set
			{
				m_bUpdate = value;
			}
			get
			{
				return m_bUpdate;
			}
			
		}
		
		/// <summary>
		/// Property Delete right. True means user has delete rights in a particular module.
		/// </summary>
		public bool Delete
		{
			set
			{
				m_bDelete = value;
			}
			get
			{
				return m_bDelete;
			}
			
		}
		
		/// <summary>
		/// Property Module right. True means user has module access rights for a particular module.
		/// </summary>
		public bool Module
		{
			set
			{
				m_bModule = value;
			}
			get
			{
				return m_bModule;
			}
		}

	}
}
