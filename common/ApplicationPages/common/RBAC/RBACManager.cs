using System.Data;
using System.Collections;
using System.Data.SqlClient;
using System;
using com.common.DAL;
using com.common.classes;
using com.common.util;



namespace com.common.RBAC
{
	/// <summary>
	/// Summary description for RBACManager.
	/// </summary>
	
	public enum OwnerType
	{
		User,
		Role
	}


	public class RBACManager
	{
		public RBACManager()
		{
		}
		

		/****** Core Enterprises related methods used mostly by Host Admin Module *******/

		/// <summary>
		/// Get all Core Enterprises from the core_enterprise table for the specified applicationid
		/// </summary>
		/// <param name="appID">ApplicationID for the current application</param>
		/// <returns>ArrayList containing objects of type CoreEnterprise</returns>
		public static ArrayList GetCoreEnterprises(String appID)
		{
			com.common.DAL.DbConnection dbCon = null;
			String strQry = null;
			IDbCommand dbCmd = null;
			ArrayList coreEnterpriseList = new ArrayList();
			DataSet coreDataSet = null;
			int i = 0, cnt = 0;
			CoreEnterprise coreEnterprise = null;

			dbCon = com.common.DAL.DbConnectionManager.GetInstance().GetDbConnection(appID);
			strQry = "select * from core_enterprise where applicationid = '"+appID+"'";
			dbCmd = dbCon.CreateCommand(strQry,CommandType.Text);
			coreDataSet = (DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
			cnt = coreDataSet.Tables[0].Rows.Count;

			
			for(i=0;i<cnt;i++)
			{
				DataRow drEach = coreDataSet.Tables[0].Rows[i];
				String strEnterpriseID = (String)drEach["enterpriseid"];
				String strEnterprise_Name = (String)drEach["enterprise_name"];
				String strEnterprise_Type=(String)drEach["enterprise_type"];
				String strContactPerson = (String)drEach["contact_person"];
				String strlogo_URL = (String)drEach["logo_url"];
				String strStyleSheet_URL = (String)drEach["stylesheet_url"];
				String strPhone_number = "";
				if(!drEach["contact_telephone"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					strPhone_number = (String)drEach["contact_telephone"];
				}
				String strEnterprise_DSN = (String)drEach["db_connection"];
				String strDBProvider = (String)drEach["db_provider"];
				String strContentImageURL = (String)drEach["content_bg_image_url"];
				String strNavigationImageURL = (String)drEach["navigation_bg_image_url"];
				String strBannerImageURL = (String)drEach["banner_bg_image_url"];
				String strWelcomeURL = (String)drEach["welcome_url"];
				String strEnterpriseWebsite = "";
				if(!drEach["enterprise_website"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					strEnterpriseWebsite = (String)drEach["enterprise_website"];
				}
				coreEnterprise = new CoreEnterprise();
				coreEnterprise.EnterpriseID = strEnterpriseID;
				coreEnterprise.EnterpriseName = strEnterprise_Name;
				coreEnterprise.EnterpriseType=strEnterprise_Type;
				coreEnterprise.ContactPerson = strContactPerson;
				coreEnterprise.LogoURL = strlogo_URL;
				coreEnterprise.StyleSheetURL = strStyleSheet_URL;
				coreEnterprise.PhoneNumber = strPhone_number;
				coreEnterprise.EnterpriseDSN = strEnterprise_DSN;
				coreEnterprise.DBProvider = strDBProvider;
				coreEnterprise.ContentBgImageURL = strContentImageURL;
				coreEnterprise.NavigationBgImageURL = strNavigationImageURL;
				coreEnterprise.BannerBgImageURL = strBannerImageURL;
				coreEnterprise.WelcomeURL = strWelcomeURL;
				coreEnterprise.EnterpriseWebsite = strEnterpriseWebsite;
				coreEnterpriseList.Add(coreEnterprise);
			}

			return coreEnterpriseList;
		}


		/// <summary>
		/// Get all Core enterprises from the core_enterprise table for the specified applicationid and 
		/// various search criteria from the same table.
		/// </summary>
		/// <param name="appID">ApplicationID for the current application</param>
		/// <param name="coreEnterprise">An object of type CoreEnerprise to store various search criteria</param>
		/// <returns>ArrayList containing objects of type CoreEnterprise</returns>
		public static ArrayList GetCoreEnterprises(String appID, CoreEnterprise coreEnterprise)
		{
			String strQry = "select * from core_enterprise where applicationid = ";
			strQry = strQry+"'"+appID+"'";
			ArrayList coreEnterpriseList = new ArrayList();
			int i = 0, cnt = 0;
			DbConnection dbCon = null;
			DataSet coreDataSet = null;
			IDbCommand dbCmd = null;
			String strUserCulture = null;

			if  ((coreEnterprise.EnterpriseID != null) && (coreEnterprise.EnterpriseID != ""))
			{
				strQry = strQry+ " and EnterpriseID = '"+coreEnterprise.EnterpriseID+"'";
			}
				
			if ((coreEnterprise.EnterpriseName != null) && (coreEnterprise.EnterpriseName != ""))
			{
				strQry = strQry+" and Enterprise_Name like '%"+coreEnterprise.EnterpriseName+"%'";
			}
			
			if ((coreEnterprise.EnterpriseType != null) && (coreEnterprise.EnterpriseType != ""))
			{
				strQry = strQry+" and Enterprise_Type like '%"+coreEnterprise.EnterpriseType +"%'";
			}

			if ((coreEnterprise.ContactPerson != null) && (coreEnterprise.ContactPerson != ""))
			{
				strQry = strQry+" and Contact_person like '%"+coreEnterprise.ContactPerson+"%'";
			}

			if ((coreEnterprise.LogoURL != null) && (coreEnterprise.LogoURL != ""))
			{
				strQry = strQry+" and logo_url = '"+coreEnterprise.LogoURL+"'";
			}

			if ((coreEnterprise.StyleSheetURL != null) && (coreEnterprise.StyleSheetURL != ""))
			{
				strQry = strQry+" and stylesheet_url = '"+coreEnterprise.StyleSheetURL+"'";
			}
				
			if ((coreEnterprise.PhoneNumber != null) && (coreEnterprise.PhoneNumber != ""))
			{
				strQry = strQry+" and contact_telephone = '"+coreEnterprise.PhoneNumber+"'";
			}

			if ((coreEnterprise.EnterpriseDSN != null) && (coreEnterprise.EnterpriseDSN != ""))
			{
				strQry = strQry+" and db_connection = '"+coreEnterprise.EnterpriseDSN+"'";
			}

			if ((coreEnterprise.DBProvider != null) && (coreEnterprise.DBProvider != ""))
			{
				strQry = strQry+" and db_provider = '"+coreEnterprise.DBProvider+"'";
			}

			if ((coreEnterprise.ContentBgImageURL != null) && (coreEnterprise.ContentBgImageURL != ""))
			{
				strQry = strQry+" and content_bg_image_url = '"+coreEnterprise.ContentBgImageURL+"'";
			}

			if ((coreEnterprise.NavigationBgImageURL != null) && (coreEnterprise.NavigationBgImageURL != ""))
			{
				strQry = strQry+" and navigation_bg_image_url = '"+coreEnterprise.NavigationBgImageURL+"'";
			}

			if ((coreEnterprise.BannerBgImageURL != null) && (coreEnterprise.BannerBgImageURL != ""))
			{
				strQry = strQry+" and banner_bg_image_url = '"+coreEnterprise.BannerBgImageURL+"'";
			}

			if ((coreEnterprise.WelcomeURL != null) && (coreEnterprise.WelcomeURL != ""))
			{
				strQry = strQry+" and welcome_url = '"+coreEnterprise.WelcomeURL+"'";
			}

			if ((coreEnterprise.EnterpriseWebsite != null) && (coreEnterprise.EnterpriseWebsite != ""))
			{
				strQry = strQry+" and enterprise_website = '"+coreEnterprise.EnterpriseWebsite+"'";
			}
			
			
			dbCon = DbConnectionManager.GetInstance().GetDbConnection(appID);
			dbCmd = dbCon.CreateCommand(strQry,CommandType.Text);
			coreDataSet = (DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
			cnt = coreDataSet.Tables[0].Rows.Count;

			
			for(i=0;i<cnt;i++)
			{
				DataRow drEach = coreDataSet.Tables[0].Rows[i];
				String strEnterpriseID = (String)drEach["enterpriseid"];
				String strEnterprise_Name = (String)drEach["enterprise_name"];
				String strEnterprise_Type= (String)drEach["enterprise_type"];
				String strContactPerson = (String)drEach["contact_person"];
				String strlogo_URL = (String)drEach["logo_url"];
				String strStyleSheet_URL = (String)drEach["stylesheet_url"];
				String strPhone_number = "";
				if(!drEach["contact_telephone"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					strPhone_number = (String)drEach["contact_telephone"];
				}
				String strEnterprise_DSN = (String)drEach["db_connection"];
				String strDBProvider = (String)drEach["db_provider"];
				String strContentImageURL = (String)drEach["content_bg_image_url"];
				String strNavigationImageURL = (String)drEach["navigation_bg_image_url"];
				String strBannerImageURL = (String)drEach["banner_bg_image_url"];
				String strWelcomeURL = (String)drEach["welcome_url"];
				String strEnterpriseWebsite = "";
				if(!drEach["enterprise_website"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					strEnterpriseWebsite = (String)drEach["enterprise_website"];
				}
				coreEnterprise = new CoreEnterprise();
				coreEnterprise.EnterpriseID = strEnterpriseID;
				coreEnterprise.EnterpriseName = strEnterprise_Name;
				coreEnterprise.EnterpriseType=strEnterprise_Type;
				coreEnterprise.ContactPerson = strContactPerson;
				coreEnterprise.LogoURL = strlogo_URL;
				coreEnterprise.StyleSheetURL = strStyleSheet_URL;
				coreEnterprise.PhoneNumber = strPhone_number;
				coreEnterprise.EnterpriseDSN = strEnterprise_DSN;
				coreEnterprise.DBProvider = strDBProvider;
				coreEnterprise.ContentBgImageURL = strContentImageURL;
				coreEnterprise.NavigationBgImageURL = strNavigationImageURL;
				coreEnterprise.BannerBgImageURL = strBannerImageURL;
				coreEnterprise.WelcomeURL = strWelcomeURL;
				coreEnterprise.EnterpriseWebsite = strEnterpriseWebsite;


				strUserCulture = GetCulture(appID,strEnterpriseID,strDBProvider,strEnterprise_DSN);
				

				coreEnterprise.AdminCulture = strUserCulture;
				coreEnterpriseList.Add(coreEnterprise);
			}			
			return coreEnterpriseList;
		}


		/// <summary>
		/// The method is used by the host admin module when a new enterprise is created. <br>
		/// It adds records in the following tables in that sequence in a single transaction.<br><br>
		/// core_enterprise -	Core Enterprise details added into this table <br>
		/// User_Master		-	An administrator user is created in the User_Master table for the enterprise <br>
		/// enterprise		-	Enterprise details are added into appdb.enterprise table <br>
		/// Role_Master		-	A system role is created for this enterprise <br>
		/// module_tree_role_relation	-	A set of all modules are fetched from the Core_Module_Tree_Master table <br>
		///									specified applicationid and inserted in to this table with the specified enterpriseid and IDU permissions <br>
		/// user_role_relation	-	System role is associated with the administrator user for this enteprise <br>
		/// permission_role_relation	-	System role is associated with the Permissions <br>
		///									(Not yet used so far - only few dummy permisisons are inserted)<br>
		/// Counter			-	All entries from the Core_Counter table for the specified applicationid are fetched <br>
		///						and inserted in to the appdb.Counter table<br>
		/// Status_Code	-	Specific to TIES application. Records from the Core_Status_Code table for the applicationid are fetched and <br>
		///					inserted in to the appdb.Status_Code table<br>
		/// Exception_Code	-	Specific to the TIES application. Records from the Core_Exception_Code table for the applicationid are fetched and<br>
		///						inserted in to the appdb.Exception_Code table<br>
		/// </summary>
		/// <param name="appID">ApplicationID for the current application</param>
		/// <param name="enterpriseID">EnterpriseID for the current enterprise</param>
		/// <param name="coreEnterprise">An object of type CoreEnterprise to be added in to database</param>
		public static void AddCoreEnterprise(String appID,String enterpriseID, CoreEnterprise coreEnterprise)
		{
			int iRowsAffected = 0;

			DbConnection dbConApp = null;
			DbConnection dbConCore = null;

			IDbCommand dbCommandCore = null;
			IDbCommand dbCommandApp = null;

			IDbTransaction transactionCore = null;
			IDbConnection conCore = null;

			IDbTransaction transactionApp = null;
			IDbConnection conApp = null;

			String strQry = null;
			
			DataSet coreModuleTree,corePermission = null;
			
			int cnt = 0, i = 0;
			

	
			//Get Core dbConnection
			dbConCore = DbConnectionManager.GetInstance().GetDbConnection(appID);

			if(dbConCore == null)
			{
				Logger.LogTraceError("RBACManager","AddCoreEnterprise","RBAC002","dbConCore is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			//Get App dbConnection

			dbConApp = DbConnectionManager.GetInstance().GetDirectDbConnection(coreEnterprise.DBProvider,coreEnterprise.EnterpriseDSN);

			if(dbConApp == null)
			{
				Logger.LogTraceError("RBACManager","AddCoreEnterprise","RBAC002","dbConApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			//Begin Core and App Transaction
			conCore = dbConCore.GetConnection();
			
			if(conCore == null)
			{
				Logger.LogTraceError("RBACManager","AddCoreEnterprise","RBAC002","conCore is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			conApp = dbConApp.GetConnection();
			
			if(conApp == null)
			{
				Logger.LogTraceError("RBACManager","AddCoreEnterprise","RBAC002","conApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			try
			{
				dbConCore.OpenConnection(ref conCore);
				dbConApp.OpenConnection(ref conApp);
			}
			catch(ApplicationException appException)
			{
				if(conApp != null)
				{
					conApp.Close();
				}

				if(conCore != null)
				{
					conCore.Close();
				}

				Logger.LogTraceError("RBACManager","AddCoreEnterprise","RBAC003","Error opening connection : "+ appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ",appException);
			}
			catch(Exception exception)
			{
				if(conApp != null)
				{
					conApp.Close();
				}

				if(conCore != null)
				{
					conCore.Close();
				}

				Logger.LogTraceError("RBACManager","AddCoreEnterprise","RBAC003","Error opening connection : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}

			transactionCore = dbConCore.BeginTransaction(conCore,IsolationLevel.ReadCommitted);
			
			if(transactionCore == null)
			{
				Logger.LogTraceError("RBACManager","AddCoreEnterprise","RBAC002","transactionCore is null");
				throw new ApplicationException("Failed Begining a Core Transaction..",null);
			}

			transactionApp = dbConApp.BeginTransaction(conApp,IsolationLevel.ReadCommitted);

			if(transactionApp == null)
			{
				Logger.LogTraceError("RBACManager","AddCoreEnterprise","RBAC002","transactionApp is null");
				throw new ApplicationException("Failed Begining a App Transaction..",null);
			}


			try
			{

				//Insert record into core_enterprise Table

				strQry = "insert into core_enterprise(applicationid,enterpriseid,enterprise_name,contact_person,";
				strQry = strQry + "contact_telephone,enterprise_website,logo_url,stylesheet_url,welcome_url,banner_bg_image_url,";
				strQry = strQry + "navigation_bg_image_url,content_bg_image_url,db_provider,db_connection,enterprise_type)";
				strQry = strQry + "values('"+appID+"','"+coreEnterprise.EnterpriseID+"','"+coreEnterprise.EnterpriseName;
				strQry = strQry + "','"+coreEnterprise.ContactPerson+"','"+coreEnterprise.PhoneNumber+"','"+coreEnterprise.EnterpriseWebsite;
				strQry = strQry + "','"+coreEnterprise.LogoURL+"','"+coreEnterprise.StyleSheetURL+"','"+coreEnterprise.WelcomeURL;
				strQry = strQry + "','"+coreEnterprise.BannerBgImageURL+"','"+coreEnterprise.NavigationBgImageURL;
				strQry = strQry + "','"+coreEnterprise.ContentBgImageURL+"','"+coreEnterprise.DBProvider+"','"+coreEnterprise.EnterpriseDSN+"','"+coreEnterprise.EnterpriseType+"')";

				dbCommandCore = dbConCore.CreateCommand(strQry,CommandType.Text); //Creating the command first time
				dbCommandCore.Connection = conCore;
				dbCommandCore.Transaction = transactionCore;

				iRowsAffected = dbConCore.ExecuteNonQueryInTransaction(dbCommandCore);
				Logger.LogTraceInfo("RBACManager","AddCoreEnterprise","INF001",iRowsAffected + " rows inserted into core_enterprise table");

				//Add user record into user_master table

				String strUserID = "administrator";
				String strUserName = "administrator";
				String strUserPswd = coreEnterprise.AdminPswd;
				String strUserCulture = coreEnterprise.AdminCulture;
				String strUsrDept = "";
				String strUsrEmail = "";
				String strUserType = "A";


				strQry = "insert into User_Master(enterpriseid,userid,applicationid,user_name,user_password,user_culture,email,department_name,user_type) ";
				strQry = strQry + " values('"+enterpriseID+"','"+strUserID+"','"+appID+"','";
				strQry = strQry + strUserName+"','"+strUserPswd+"','"+strUserCulture+"','"+Utility.ReplaceSingleQuote(strUsrEmail)+"','"+strUsrDept+"','"+strUserType+"')";
				
				dbCommandApp = dbConApp.CreateCommand(strQry,CommandType.Text); //Creating command object first time
				dbCommandApp.Connection = conApp;
				dbCommandApp.Transaction = transactionApp;
				
				iRowsAffected = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);
				Logger.LogTraceInfo("RBACManager","AddCoreEnterprise","INF001",iRowsAffected + " rows inserted into User_Master table");
			

				//Add record into Enterprise table
				strQry = " insert into enterprise (applicationid,enterpriseid,enterprise_name,contact_person,contact_telephone,enterprise_type, free_insurance_amt, max_insurance_amt)";
				strQry = strQry + "values('"+appID+"','"+enterpriseID+"','"+coreEnterprise.EnterpriseName+"','"+coreEnterprise.ContactPerson+"','"+coreEnterprise.PhoneNumber+"','"+coreEnterprise.EnterpriseType+"',0,0)";

				dbCommandApp.CommandText = strQry;
				iRowsAffected = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);
				Logger.LogTraceInfo("RBACManager","AddCoreEnterprise","INF001",iRowsAffected + " rows inserted into enterprise table");

				String strRoleName = "system";

				long iRoleID = 0;

				strQry = "insert into Role_Master(applicationid,enterpriseid,roleid,role_name,role_description) values('"+appID+"','"+enterpriseID+"'"+","+iRoleID+",'"+strRoleName+"','')";

				dbCommandApp.CommandText = strQry;
				iRowsAffected = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);
				Logger.LogTraceInfo("RBACManager","AddCoreEnterprise","INF001",iRowsAffected + " rows inserted into Role_Master table");

				//Adding into Module_Tree_Role_Relation from core_module_tree_master

				strQry = "select * from core_module_tree_master where applicationid = '"+appID+"'";

				dbCommandCore.CommandText = strQry;

				coreModuleTree = (DataSet)dbConCore.ExecuteQueryInTransaction(dbCommandCore,com.common.DAL.ReturnType.DataSetType);
				cnt = coreModuleTree.Tables[0].Rows.Count;
				Logger.LogTraceInfo("RBACManager","AddCoreEnterprise","INF001",cnt + " rows read from core_module_tree_master table");


				for(i=0;i<cnt;i++)
				{
					DataRow drEach = coreModuleTree.Tables[0].Rows[i];
					String strChildID = (String)drEach["child_id"];
					String strChildName = (String)drEach["child_display_name"];
					String strParentID = (String)drEach["Parent_id"];
					String strChildIconImage = "";
					if(!drEach["child_icon_image"].GetType().Equals(System.Type.GetType("System.DBNull")))
					{
						strChildIconImage = (String)drEach["child_icon_image"];
					}
					decimal iDisplayStyle = 0;
					if(!drEach["display_style"].GetType().Equals(System.Type.GetType("System.DBNull")))
					{
						if (coreModuleTree.Tables[0].Columns["display_style"].DataType == System.Type.GetType("System.Decimal"))
							iDisplayStyle = (decimal)drEach["display_style"];
						else
							iDisplayStyle = (byte) drEach["display_style"];
					}
					String strChildURL = "";
					if(!drEach["child_url"].GetType().Equals(System.Type.GetType("System.DBNull")))
					{
						strChildURL = (String)drEach["child_url"];
					}

					strQry = " insert into module_tree_role_relation(roleid,enterpriseid,parentid,child_id,";
					strQry = strQry + " applicationid,child_name,child_icon_image,display_style,child_url)values (";
					strQry = strQry + iRoleID+",'"+enterpriseID+"','"+strParentID+"','"+strChildID+"',";
					strQry = strQry + "'"+appID+"','"+strChildName+"','"+strChildIconImage+"','"+iDisplayStyle+"',";
					strQry = strQry +"'"+strChildURL+"')";

					dbCommandApp.CommandText = strQry;
					iRowsAffected = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);
					Logger.LogTraceInfo("RBACManager","AddCoreEnterprise","INF001",iRowsAffected + " rows inserted into module_tree_role_relation table");
				}
			
				//Add record into table user_role_relation

				strQry = "insert into user_role_relation(roleid,userid,enterpriseid,applicationid)values(";
				strQry = strQry +iRoleID+",'"+strUserID+"','"+enterpriseID+"','"+appID+"')";

				dbCommandApp.CommandText = strQry;
				iRowsAffected = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);
				Logger.LogTraceInfo("RBACManager","AddCoreEnterprise","INF001",iRowsAffected + " rows inserted into user_role_relation table");

				//Add record into permission role relation
				strQry = "select * from core_permission_master where applicationid = '"+appID+"'";
				dbCommandCore.CommandText = strQry;

				corePermission = (DataSet)dbConCore.ExecuteQueryInTransaction(dbCommandCore,com.common.DAL.ReturnType.DataSetType);
				cnt = corePermission.Tables[0].Rows.Count;
				Logger.LogTraceInfo("RBACManager","AddCoreEnterprise","INF001",cnt + " rows read from core_permission_master table");

			
				for(i=0;i<cnt;i++)
				{
					DataRow drEach = corePermission.Tables[0].Rows[i];
					String strPermissionID = (String)drEach["permissionid"];

					strQry = "insert into permission_role_relation(roleid,permissionid,enterpriseid,applicationid)values(";
					strQry = strQry +iRoleID+",'"+strPermissionID+"','"+enterpriseID+"','"+appID+"')"; 

					dbCommandApp.CommandText = strQry;
					iRowsAffected = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);
					Logger.LogTraceInfo("RBACManager","AddCoreEnterprise","INF001",iRowsAffected + " rows inserted into permission_role_relation table");
				}


				//Adding record from Core_Counter to Counter table in App db

				strQry = "select * from Core_Counter where applicationid = '"+appID+"'";
				dbCommandCore.CommandText = strQry;

				DataSet dsCounter = (DataSet)dbConCore.ExecuteQueryInTransaction(dbCommandCore,com.common.DAL.ReturnType.DataSetType);
				cnt = dsCounter.Tables[0].Rows.Count;
				Logger.LogTraceInfo("RBACManager","AddCoreEnterprise","INF001",cnt + " rows read from Core_Counter table");

				for(i=0;i<cnt;i++)
				{
				
					DataRow drEach = dsCounter.Tables[0].Rows[i];
					String codeName = (String)drEach["code_name"];
			
					String currentValue = drEach["current_value"].ToString();

					String maxValue = drEach["max_value"].ToString();
					String minValue = drEach["min_value"].ToString();
					String recycle = "";
					if(!drEach["recycle"].GetType().Equals(System.Type.GetType("System.DBNull")))
					{
						recycle = (String)drEach["recycle"];
					}

					strQry = "insert into Counter(applicationid,enterpriseid,code_name,current_value,max_value,min_value,recycle) values(";
					strQry += "'"+ appID+"','"+enterpriseID+"','"+codeName+"',"+currentValue+","+maxValue+","+minValue+",'"+recycle+"')"; 

					dbCommandApp.CommandText = strQry;
					iRowsAffected = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);
					Logger.LogTraceInfo("RBACManager","AddCoreEnterprise","INF001",iRowsAffected + " rows inserted into Counter table");
				}


				//Adding record from Core_Status_Code to Status_Code table in App db

				strQry = "select * from Core_Status_Code where applicationid = '"+appID+"'";
				dbCommandCore.CommandText = strQry;

				DataSet dsStatusCode = (DataSet)dbConCore.ExecuteQueryInTransaction(dbCommandCore,com.common.DAL.ReturnType.DataSetType);
				cnt = dsStatusCode.Tables[0].Rows.Count;
				Logger.LogTraceInfo("RBACManager","AddCoreEnterprise","INF001",cnt + " rows read from Core_Status_Code table");

				for(i=0;i<cnt;i++)
				{
				
					DataRow drEach = dsStatusCode.Tables[0].Rows[i];
					String statCode = (String)drEach["status_code"];
			
					String statDescription = drEach["status_description"].ToString();

					String statClose = drEach["status_close"].ToString();
					String invoice = drEach["invoiceable"].ToString();
					String systemCode = "Y";

					strQry = "insert into Status_Code(applicationid,enterpriseid,status_code,status_description,status_close,invoiceable, system_code) values(";
					strQry += "'"+ appID+"','"+enterpriseID+"','"+statCode+"','"+statDescription+"','"+statClose+"','"+invoice+"','"+systemCode+"')"; 
					
					dbCommandApp.CommandText = strQry;
					iRowsAffected = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);
					Logger.LogTraceInfo("RBACManager","AddCoreEnterprise","INF001",iRowsAffected + " rows inserted into Status_Code table");
				}

				//Adding record from Core_Exception_Code to Exception_Code table in App db
				strQry = "select * from Core_Exception_Code where applicationid = '"+appID+"'";
				dbCommandCore.CommandText = strQry;

				DataSet dsExceptionCode = (DataSet)dbConCore.ExecuteQueryInTransaction(dbCommandCore,com.common.DAL.ReturnType.DataSetType);
				cnt = dsExceptionCode.Tables[0].Rows.Count;
				Logger.LogTraceInfo("RBACManager","AddCoreEnterprise","INF001",cnt + " rows read from Core_Exception_Code table");


				for(i=0;i<cnt;i++)
				{
				
					DataRow drEach = dsExceptionCode.Tables[0].Rows[i];
					String statCode = (String)drEach["status_code"];
			
					String execeptCode = drEach["exception_code"].ToString();

					String exceptDescription = drEach["exception_description"].ToString();
					String mbg = drEach["mbg"].ToString();
					String statClose = drEach["status_close"].ToString();
					String invoiceable = drEach["invoiceable"].ToString();
					String system_code = "Y";

					strQry = "insert into Exception_Code(applicationid,enterpriseid,status_code,exception_code,exception_description,mbg,status_close,invoiceable,system_code) values(";
					strQry += "'"+ appID+"','"+enterpriseID+"','"+statCode+"','"+execeptCode+"','"+exceptDescription+"','"+mbg+"','"+statClose+"','"+invoiceable+"','"+system_code+"')"; 

					dbCommandApp.CommandText = strQry;
					iRowsAffected = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);
					Logger.LogTraceInfo("RBACManager","AddCoreEnterprise","INF001",iRowsAffected + " rows inserted into Exception_Code table");
				}

				//Commit application and core transaction.

				transactionCore.Commit();
				transactionApp.Commit();
				Logger.LogTraceInfo("RBACManager","AddCoreEnterprise","RBAC003","Core db and App db insert transaction committed.");

			}
			catch(ApplicationException appException)
			{

				try
				{
					transactionCore.Rollback();
					Logger.LogTraceInfo("RBACManager","AddCoreEnterprise","RBAC003","Core db insert transaction rolled back.");

				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("RBACManager","AddCoreEnterprise","RBAC003","Error during core transanction roll back "+ rollbackException.Message.ToString());
				}

				
				try
				{
					transactionApp.Rollback();
					Logger.LogTraceInfo("RBACManager","AddCoreEnterprise","RBAC003","App db insert transaction rolled back.");

				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("RBACManager","AddCoreEnterprise","RBAC003","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}


				Logger.LogTraceError("RBACManager","AddCoreEnterprise","RBAC003","Insert Transaction Failed : "+ appException.Message.ToString());
				throw new ApplicationException("Error adding core enterprise ",appException);
			}
			catch(Exception exception)
			{
				try
				{
					transactionCore.Rollback();
					Logger.LogTraceInfo("RBACManager","AddCoreEnterprise","RBAC003","Core db insert transaction rolled back.");

				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("RBACManager","AddCoreEnterprise","RBAC003","Error during core transanction roll back "+ rollbackException.Message.ToString());
				}

				
				try
				{
					transactionApp.Rollback();
					Logger.LogTraceInfo("RBACManager","AddCoreEnterprise","RBAC003","App db insert transaction rolled back.");

				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("RBACManager","AddCoreEnterprise","RBAC003","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}

				Logger.LogTraceError("RBACManager","AddCoreEnterprise","RBAC004","Insert Transaction Failed : "+ exception.Message.ToString());
				throw new ApplicationException("Error adding core enterprise ",exception);
			}
			finally
			{
				if(conApp != null)
				{
					conApp.Close();
				}

				if(conCore != null)
				{
					conCore.Close();
				}
			}
		}


		/// <summary>
		/// Method is used to modifiy record for a particular CoreEnterprise in the core_enterprise table for the 
		/// specified applicationid. The method also updates the password of the administrator user in the user_master table for this enterprise
		/// </summary>
		/// <param name="appID">ApplicationID for the current application</param>
		/// <param name="coreEnterprise">An object of type CoreEnterprise to be modified</param>
		public static void ModifyCoreEnterprise(String appID,CoreEnterprise coreEnterprise)
		{

			int iRowsAffected = 0;

			DbConnection dbConApp = null;
			DbConnection dbConCore = null;

			IDbCommand dbCommandCore = null;
			IDbCommand dbCommandApp = null;

			IDbTransaction transactionCore = null;
			IDbConnection conCore = null;

			IDbTransaction transactionApp = null;
			IDbConnection conApp = null;

			String strQry = null;
	
	
			//Get Core dbConnection
			dbConCore = DbConnectionManager.GetInstance().GetDbConnection(appID);

			if(dbConCore == null)
			{
				Logger.LogTraceError("RBACManager","ModifyCoreEnterprise","RBAC002","dbConCore is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			//Get App dbConnection

			dbConApp = DbConnectionManager.GetInstance().GetDirectDbConnection(coreEnterprise.DBProvider,coreEnterprise.EnterpriseDSN);

			if(dbConApp == null)
			{
				Logger.LogTraceError("RBACManager","ModifyCoreEnterprise","RBAC002","dbConApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			//Begin Core and App Transaction
			conCore = dbConCore.GetConnection();
			
			if(conCore == null)
			{
				Logger.LogTraceError("RBACManager","ModifyCoreEnterprise","RBAC002","conCore is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			conApp = dbConApp.GetConnection();
			
			if(conApp == null)
			{
				Logger.LogTraceError("RBACManager","ModifyCoreEnterprise","RBAC002","conApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			try
			{
				dbConCore.OpenConnection(ref conCore);
				dbConApp.OpenConnection(ref conApp);
			}
			catch(ApplicationException appException)
			{
				if(conApp != null)
				{
					conApp.Close();
				}

				if(conCore != null)
				{
					conCore.Close();
				}

				Logger.LogTraceError("RBACManager","ModifyCoreEnterprise","RBAC003","Error opening connection : "+ appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ",appException);
			}
			catch(Exception exception)
			{
				if(conApp != null)
				{
					conApp.Close();
				}

				if(conCore != null)
				{
					conCore.Close();
				}

				Logger.LogTraceError("RBACManager","ModifyCoreEnterprise","RBAC003","Error opening connection : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}

			transactionCore = dbConCore.BeginTransaction(conCore,IsolationLevel.ReadCommitted);
			
			if(transactionCore == null)
			{
				Logger.LogTraceError("RBACManager","ModifyCoreEnterprise","RBAC002","transactionCore is null");
				throw new ApplicationException("Failed Begining a Core Transaction..",null);
			}

			transactionApp = dbConApp.BeginTransaction(conApp,IsolationLevel.ReadCommitted);

			if(transactionApp == null)
			{
				Logger.LogTraceError("RBACManager","ModifyCoreEnterprise","RBAC002","transactionApp is null");
				throw new ApplicationException("Failed Begining a App Transaction..",null);
			}


			try
			{

				//Update record into core_enterprise Table

				strQry = "update core_enterprise set enterprise_name='"+coreEnterprise.EnterpriseName+"',";
				strQry = strQry + " contact_person ='"+coreEnterprise.ContactPerson+"',";
				strQry = strQry + " logo_url = '"+coreEnterprise.LogoURL+"',";
				strQry = strQry + " stylesheet_url = '"+coreEnterprise.StyleSheetURL+"',";
				strQry = strQry + " contact_telephone = '"+coreEnterprise.PhoneNumber+"',";
				strQry = strQry + " db_connection = '"+coreEnterprise.EnterpriseDSN+"',";
				strQry = strQry + " db_provider = '"+coreEnterprise.DBProvider+"',";
				strQry = strQry + " content_bg_image_url = '"+coreEnterprise.ContentBgImageURL+"',";
				strQry = strQry + " navigation_bg_image_url = '"+coreEnterprise.NavigationBgImageURL+"',";
				strQry = strQry + " banner_bg_image_url = '"+coreEnterprise.BannerBgImageURL+"',";
				strQry = strQry + " welcome_url = '"+coreEnterprise.WelcomeURL+"',";
				strQry = strQry + " enterprise_website = '"+coreEnterprise.EnterpriseWebsite+"',";
				strQry = strQry + " enterprise_type = '"+coreEnterprise.EnterpriseType+"'";
				strQry = strQry + " where applicationid = '"+appID+"'";
				strQry = strQry + " and   enterpriseid  = '"+coreEnterprise.EnterpriseID+"'"; 

				dbCommandCore = dbConCore.CreateCommand(strQry,CommandType.Text); //Creating the command first time
				dbCommandCore.Connection = conCore;
				dbCommandCore.Transaction = transactionCore;

				iRowsAffected = dbConCore.ExecuteNonQueryInTransaction(dbCommandCore);
				Logger.LogTraceInfo("RBACManager","ModifyCoreEnterprise","INF001",iRowsAffected + " rows updated into Core_enterprise table");

				//Update user password into user_master table

				String strUserPswd = coreEnterprise.AdminPswd;

				if ((strUserPswd != null) && (strUserPswd != ""))
				{
					strQry = "update User_Master set user_password = '"+strUserPswd+"'"+" where enterpriseid = '"+coreEnterprise.EnterpriseID+"'";
					strQry = strQry +" and applicationid = '"+appID+"'"+" and userid = "+"'administrator'";
				
					dbCommandApp = dbConApp.CreateCommand(strQry,CommandType.Text); //Creating command object first time
					dbCommandApp.Connection = conApp;
					dbCommandApp.Transaction = transactionApp;
				
					iRowsAffected = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);
					Logger.LogTraceInfo("RBACManager","ModifyCoreEnterprise","INF001",iRowsAffected + " row(s) updated into User_Master table");
				}
			
				//Commit application and core transaction.

				transactionCore.Commit();
				transactionApp.Commit();
				Logger.LogTraceInfo("RBACManager","ModifyCoreEnterprise","RBAC003","Core db and App db update transaction committed.");

			}
			catch(ApplicationException appException)
			{

				try
				{
					transactionCore.Rollback();
					Logger.LogTraceInfo("RBACManager","ModifyCoreEnterprise","RBAC003","Core db update transaction rolled back.");

				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("RBACManager","ModifyCoreEnterprise","RBAC003","Error during core transanction roll back "+ rollbackException.Message.ToString());
				}

				
				try
				{
					transactionApp.Rollback();
					Logger.LogTraceInfo("RBACManager","ModifyCoreEnterprise","RBAC003","App db update transaction rolled back.");

				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("RBACManager","ModifyCoreEnterprise","RBAC003","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}


				Logger.LogTraceError("RBACManager","ModifyCoreEnterprise","RBAC003","Update Transaction Failed : "+ appException.Message.ToString());
				throw new ApplicationException("Error modifying core enterprise ",appException);
			}
			catch(Exception exception)
			{
				try
				{
					transactionCore.Rollback();
					Logger.LogTraceInfo("RBACManager","ModifyCoreEnterprise","RBAC003","Core db update transaction rolled back.");

				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("RBACManager","ModifyCoreEnterprise","RBAC003","Error during core transanction roll back "+ rollbackException.Message.ToString());
				}

				
				try
				{
					transactionApp.Rollback();
					Logger.LogTraceInfo("RBACManager","ModifyCoreEnterprise","RBAC003","App db update transaction rolled back.");

				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("RBACManager","ModifyCoreEnterprise","RBAC003","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}

				Logger.LogTraceError("RBACManager","ModifyCoreEnterprise","RBAC004","Update Transaction Failed : "+ exception.Message.ToString());
				throw new ApplicationException("Error modifying core enterprise ",exception);
			}
			finally
			{
				if(conApp != null)
				{
					conApp.Close();
				}

				if(conCore != null)
				{
					conCore.Close();
				}
			}


			/////////////////////////////////////////////////////////////////
			//			try
			//			{
			//				DbConnection  dbCore = null, dbCon = null;
			//				IDbCommand dbCom = null;
			//				String strQry = null;
			//
			//
			//				String strUserPswd = null;
			//				strUserPswd = coreEnterprise.AdminPswd;
			//
			//				if ((strUserPswd != null) && (strUserPswd != ""))
			//				{
			//					dbCon = DbConnectionManager.GetInstance().GetDbConnection(appID,coreEnterprise.EnterpriseID);
			//					strQry = "update User_Master set user_password = '"+strUserPswd+"'"+" where enterpriseid = '"+coreEnterprise.EnterpriseID+"'";
			//					strQry = strQry +" and applicationid = '"+appID+"'"+" and userid = "+"'administrator'";
			//					dbCom = dbCon.CreateCommand(strQry,CommandType.Text);
			//					dbCon.ExecuteNonQuery(dbCom);
			//				}
			//
			//
			//				
			//
			//				dbCore = DbConnectionManager.GetInstance().GetDbConnection(appID);
			//				strQry = "update Core_enterprise set enterprise_name='"+coreEnterprise.EnterpriseName+"',";
			//				strQry = strQry + " contact_person ='"+coreEnterprise.ContactPerson+"',";
			//				strQry = strQry + " logo_url = '"+coreEnterprise.LogoURL+"',";
			//				strQry = strQry + " stylesheet_url = '"+coreEnterprise.StyleSheetURL+"',";
			//				strQry = strQry + " contact_telephone = '"+coreEnterprise.PhoneNumber+"',";
			//				strQry = strQry + " db_connection = '"+coreEnterprise.EnterpriseDSN+"',";
			//				strQry = strQry + " db_provider = '"+coreEnterprise.DBProvider+"',";
			//				strQry = strQry + " content_bg_image_url = '"+coreEnterprise.ContentBgImageURL+"',";
			//				strQry = strQry + " navigation_bg_image_url = '"+coreEnterprise.NavigationBgImageURL+"',";
			//				strQry = strQry + " banner_bg_image_url = '"+coreEnterprise.BannerBgImageURL+"',";
			//				strQry = strQry + " welcome_url = '"+coreEnterprise.WelcomeURL+"',";
			//				strQry = strQry + " enterprise_website = '"+coreEnterprise.EnterpriseWebsite+"'";
			//				strQry = strQry + " where applicationid = '"+appID+"'";
			//				strQry = strQry + " and   enterpriseid  = '"+coreEnterprise.EnterpriseID+"'"; 
			//				dbCom = dbCore.CreateCommand(strQry,CommandType.Text);
			//				dbCore.ExecuteNonQuery(dbCom);
			//			}
			//			catch(Exception modifyEnterpException)
			//			{
			//				Console.WriteLine("Exception ModifyCoreEnterprise	: "+modifyEnterpException.Message.ToString());
			//			}

		}


		/// <summary>
		/// Deletes the record from the core_enterprise table for the specified applicationid and enterpriseid
		/// This method is not implemented fully. It should delete records from all dependent tables in a single transaction.
		/// </summary>
		/// <param name="appID">ApplicationID for the current application</param>
		/// <param name="enterpriseID">EnterpriseID for the current enterprise</param>
		public static void DeleteCoreEnterprise(String appID,String enterpriseID)
		{
			try
			{
				DbConnection dbCon = null;
				IDbCommand dbDeleteQuery = null;
				String strQry = null;
			
				dbCon = DbConnectionManager.GetInstance().GetDbConnection(appID);
				strQry = "delete from core_enterprise where  enterpriseid = '"+enterpriseID+"'"+" and applicationid = '"+appID+"'";
				dbDeleteQuery = dbCon.CreateCommand(strQry,CommandType.Text);
				dbCon.ExecuteNonQuery(dbDeleteQuery);

			}
			catch(Exception deleteUserException)
			{
				Console.WriteLine("Exception delete Core Enterprise	: "+deleteUserException.Message.ToString());
			}
		}


		/// <summary>
		/// Get the user_culture of the administrator user from the user_master table for the specified applicationid and enterpriseid.
		/// </summary>
		/// <param name="strAppID">ApplicationID for the current application</param>
		/// <param name="strEnterprise">EnterpriseID for the current enterprise</param>
		/// <param name="strProvider">DbProvider (SQL/ORACLE/OLEDB) used to get a DbConnection object</param>
		/// <param name="strConn">Connection String for the enterprise database</param>
		/// <returns>User culture as a string value</returns>
		private static String GetCulture(String strAppID, String strEnterprise, String strProvider, String strConn)
		{
			String strUserCulture=null;
			IDbCommand dbCmd = null;
			DataSet usrDataSet=null;
			//Getting the culture for the administrator user
			DbConnection dbConnection = null;
			dbConnection = DbConnectionManager.GetInstance().GetDirectDbConnection(strProvider,strConn);
			
			String strQry = ""; 
			strQry = "select user_culture from user_master where applicationid = '"+strAppID+"'"+ " and enterpriseid = ";
			strQry = strQry +"'"+strEnterprise+"'"+ " and userid = "+"'administrator'";
			dbCmd = dbConnection.CreateCommand(strQry,CommandType.Text);

			try
			{
				usrDataSet = (DataSet)dbConnection.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
			}
			catch(Exception err)
			{
				String dd = err.Message;
			}

			if ((usrDataSet != null) && (usrDataSet.Tables[0].Rows.Count > 0))
			{
				DataRow drUsrEach = usrDataSet.Tables[0].Rows[0];
				strUserCulture = (String)drUsrEach["user_culture"];
			}
			return strUserCulture;
		}



		/****** Module Management Methods *******/

		/// <summary>
		/// Get all modules from module_tree_role_relation table for the specified applicationid,
		/// enterpriseid, roleid as containied in the Role object, and the parent_module_id
		/// </summary>
		/// <param name="strAppID">ApplicationID for the current application</param>
		/// <param name="strEnterpriseID">EnterpriseID for the current enterprise</param>
		/// <param name="role">An oject of type Role</param>
		/// <param name="moduleID">Module ID for the parent whose child modules are to be fetched</param>
		/// <returns>ArrayList containing objects of type Module</returns>
		public  static ArrayList GetAllModules(String strAppID, String strEnterpriseID, Role role, String moduleID)
		{
			ArrayList childList = new ArrayList();
			DbConnection dbCon = null;
			IDbCommand dbCmd = null;
			String strQry = null;
			int cnt = 0, i = 0;
			DataSet modDataSet = null;
			Module module;
			
			dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			strQry = "SELECT child_id,child_name,child_icon_image,display_style,child_url "+ 
			"FROM Module_Tree_Role_Relation "+
			"WHERE roleid = "+role.RoleID+" and parentid = '"+moduleID+"'"+" and enterpriseid = '"+strEnterpriseID+"'"+" and applicationid = '"+strAppID+"'";
			dbCmd = dbCon.CreateCommand(strQry,CommandType.Text);
			modDataSet =(DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
			
			cnt = modDataSet.Tables[0].Rows.Count;

			for(i=0;i<cnt;i++)
			{
				DataRow drEach = modDataSet.Tables[0].Rows[i];

				String childid = (String)drEach["child_id"];
				String childName = (String)drEach["child_name"];			
				String imageURL = (String)drEach["child_icon_image"];
				decimal iDisplayStyle;
				if (modDataSet.Tables[0].Columns["display_style"].DataType == System.Type.GetType("System.Decimal"))
					iDisplayStyle = (decimal)drEach["display_style"];
				else
					iDisplayStyle = (byte) drEach["display_style"];
			
				String linkURL = "";
				if(!drEach["child_url"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					linkURL = (String)drEach["child_url"];					
				}
				module = new Module();
				module.ModuleId = childid;
				module.ModuleName = childName;
				module.ModuleNameStyle = System.Convert.ToInt32(iDisplayStyle);
				module.ModuleLinkURL = linkURL;
				module.ModuleIconImage = imageURL;

				childList.Add(module);				
			}
			return childList;
		}


		/// <summary>
		/// Get all distinct modules from module_tree_role_relation and module_tree_user_relation tables for the 
		/// specified applicationid, enterpriseid, userid (as contained in the user parameter) and the parent_module_id.
		/// </summary>
		/// <param name="appID">ApplicationID for the current application</param>
		/// <param name="enterpriseID">EnterpriseID for the current enterprise</param>
		/// <param name="user">An object of type User. </param>
		/// <param name="moduleid">Module ID for the parent whose child modules are to be fetched</param>
		/// <returns>ArrayList containing objects of type Module</returns>
		public static ArrayList GetAllModules(String appID, String enterpriseID, User user, String moduleid)
		{
			ArrayList moluleArray = new ArrayList();
			String strQry = null;
			int cnt = 0,i = 0;
			Module module = null;
			DbConnection dbCon = null;
			DataSet modDataSet = null;
			IDbCommand dbCmd = null;

			strQry = "(select distinct c.child_id,c.child_name,c.child_icon_image,c.display_style,c.child_url from module_tree_user_relation c where c.applicationid = '"+appID+"'";
			strQry = strQry+ " and c.enterpriseID = '"+enterpriseID+"'"+" and c.userid = '"+user.UserID+"'" +" and c.parent_id = '"+moduleid+"')";
			strQry = strQry+ " union ";
			strQry = strQry+ " (select distinct a.child_id,a.child_name,a.child_icon_image,a.display_style,a.child_url from module_tree_role_relation a where a.applicationid = '"+appID+"'";
			strQry = strQry+ " and a.enterpriseID = '"+enterpriseID+"'"+" and parentid = '"+moduleid+"'"+" and roleid in ";
			strQry = strQry+ " (select distinct b.roleid from user_role_relation b where b.applicationid = '"+appID+"'";
			strQry = strQry+ " and b.enterpriseID = '"+enterpriseID+"'"+" and b.userid = '"+user.UserID+"'"+" )";
			strQry = strQry+")";
			
			dbCon = DbConnectionManager.GetInstance().GetDbConnection(appID,enterpriseID);
			dbCmd = dbCon.CreateCommand(strQry,CommandType.Text);
			try
			{
				modDataSet =(DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
			}
			catch(ApplicationException appExpection)
			{
				Logger.LogTraceError("RBACManager","GetAllModules","RBAC001","Error in building tree");
				throw appExpection;
			}
			cnt = modDataSet.Tables[0].Rows.Count;

			for(i=0;i<cnt;i++)
			{
				DataRow drEach = modDataSet.Tables[0].Rows[i];

				String childid = (String)drEach["child_id"];
				String childName = (String)drEach["child_name"];			
				String imageURL = (String)drEach["child_icon_image"];
				decimal iDisplayStyle;	

				if (modDataSet.Tables[0].Columns["display_style"].DataType == System.Type.GetType("System.Decimal"))
					iDisplayStyle = (decimal)drEach["display_style"];
				else
					iDisplayStyle = (byte) drEach["display_style"];

				String linkURL = "";
				if(!drEach["child_url"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					linkURL = (String)drEach["child_url"];					
				}
				module = new Module();
				module.ModuleId = childid;
				module.ModuleName = childName;
				module.ModuleNameStyle = System.Convert.ToInt32(iDisplayStyle);
				module.ModuleLinkURL = linkURL;
				module.ModuleIconImage = imageURL;

				moluleArray.Add(module);				
			}
				return moluleArray;
		}


		/// <summary>
		/// Get the module (usually only one) from module_tree_role_relation table for the specified applicationid,
		/// enterpriseid, parent_module_id, child_module_id and role_id.
		/// </summary>
		/// <param name="appID">ApplicationID for the current application</param>
		/// <param name="enterpriseID">EnterpriseID for the current enterprise</param>
		/// <param name="parentID">Module ID for the parent whose child module are to be fetched</param>
		/// <param name="childID">Module ID for the child module</param>
		/// <param name="roleID">Role Id for which the module is required</param>
		/// <returns>ArrayList containing object of type Module</returns>
		public static ArrayList GetAllModules(String appID,String enterpriseID,String parentID, String childID,decimal roleID)
		{
			DbConnection dbCon = null;
			IDbCommand dbCom = null;
			String strQry = null;
			DataSet moduleDataSet = null;
			ArrayList moduleList = new ArrayList();
			Module module = null;
			int cnt = 0, i = 0;

			dbCon = DbConnectionManager.GetInstance().GetDbConnection(appID,enterpriseID);
			strQry = "select * from module_tree_role_relation ";
			strQry = strQry +" where applicationid = '"+appID+"'";
			strQry = strQry +" and enterpriseid = '"+enterpriseID+"'";
			strQry = strQry +" and parentid = '"+parentID+"'";
			strQry = strQry +" and child_id = '"+childID+"'";
			strQry = strQry +" and roleid = "+roleID;
			dbCom = dbCon.CreateCommand(strQry,CommandType.Text);
			moduleDataSet = (DataSet)dbCon.ExecuteQuery(dbCom,com.common.DAL.ReturnType.DataSetType);
			cnt = moduleDataSet.Tables[0].Rows.Count;
			
			for(i=0;i<cnt;i++)
			{
				DataRow drEach = moduleDataSet.Tables[0].Rows[i];
				String strChildID = (String)drEach["child_id"];
				String strChildName = (String)drEach["child_name"];
				String strModuleRights = (String)drEach["module_rights"];
				String strChildIconImage = "";
				if(!drEach["child_icon_image"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					strChildIconImage = (String)drEach["child_icon_image"];
				}
				
				String strChildURL = "";
				if(!drEach["child_url"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					strChildURL = (String)drEach["child_url"];
				}

				module = new Module();
				module.ModuleId = strChildID;
				module.ModuleIconImage = strChildIconImage;
				module.ModuleLinkURL = strChildURL;
				module.ModuleName = strChildName;
				module.ModuleRights = strModuleRights;
				moduleList.Add(module);
			}
			return moduleList;
		}


		/// <summary>
		/// Get the module (usually only one) from the module_tree_user_relation table for the specified applicationid, 
		/// enterpriseid, parent_module_id, child_module_id and userid
		/// </summary>
		/// <param name="appID">ApplicationID for the current application</param>
		/// <param name="enterpriseID">EnterpriseID for the current enterprise</param>
		/// <param name="parentID">Module ID for the parent whose child module are to be fetched</param>
		/// <param name="childID">Module ID for the child module</param>
		/// <param name="userID">User ID for which the module is required</param>
		/// <returns>ArrayList containing object of type Module</returns>
		public static ArrayList GetAllModules(String appID,String enterpriseID,String parentID, String childID,String userID)
		{
			DbConnection dbCon = null;
			IDbCommand dbCom = null;
			String strQry = null;
			DataSet moduleDataSet = null;
			ArrayList moduleList = new ArrayList();
			Module module = null;
			int cnt = 0, i = 0;

			dbCon = DbConnectionManager.GetInstance().GetDbConnection(appID,enterpriseID);
			strQry = "select * from module_tree_user_relation ";
			strQry = strQry +" where applicationid = '"+appID+"'";
			strQry = strQry +" and enterpriseid = '"+enterpriseID+"'";
			strQry = strQry +" and parent_id = '"+parentID+"'";
			strQry = strQry +" and child_id = '"+childID+"'";
			strQry = strQry +" and userid = '"+userID+"'";
			dbCom = dbCon.CreateCommand(strQry,CommandType.Text);
			moduleDataSet = (DataSet)dbCon.ExecuteQuery(dbCom,com.common.DAL.ReturnType.DataSetType);
			cnt = moduleDataSet.Tables[0].Rows.Count;
			
			for(i=0;i<cnt;i++)
			{
				DataRow drEach = moduleDataSet.Tables[0].Rows[i];
				String strChildID = (String)drEach["child_id"];
				String strChildName = (String)drEach["child_name"];
				String strModuleRights = (String)drEach["module_rights"];
				String strChildIconImage = "";
				if(!drEach["child_icon_image"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					strChildIconImage = (String)drEach["child_icon_image"];
				}
			
				String strChildURL = "";
				if(!drEach["child_url"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					strChildURL = (String)drEach["child_url"];
				}

				module = new Module();
				module.ModuleId = strChildID;
				module.ModuleIconImage = strChildIconImage;
				module.ModuleLinkURL = strChildURL;
				module.ModuleName = strChildName;
				module.ModuleRights = strModuleRights;
				moduleList.Add(module);
			}
			return moduleList;
		}


		/// <summary>
		/// Get all modules for specified parent_module_id and applicationid from the core_module_tree_master table
		/// </summary>
		/// <param name="appID">ApplicationID for the current application</param>
		/// <param name="moduleID">Parent module id</param>
		/// <returns>ArrayList containing objects of type Module</returns>
		public static ArrayList GetCoreModules(String appID,String moduleID)
		{
			DbConnection dbCore = null;
			IDbCommand dbCom = null;
			String strQry = null;
			DataSet moduleDataSet = null;
			ArrayList moduleList = new ArrayList();
			Module module = null;
			int cnt = 0, i = 0;

			dbCore = DbConnectionManager.GetInstance().GetDbConnection(appID);
			strQry = "select * from core_module_tree_master ";
			strQry = strQry +" where applicationid = '"+appID+"'"+" and parent_id = '"+moduleID+"'";
			dbCom = dbCore.CreateCommand(strQry,CommandType.Text);
			moduleDataSet = (DataSet)dbCore.ExecuteQuery(dbCom,com.common.DAL.ReturnType.DataSetType);
			cnt = moduleDataSet.Tables[0].Rows.Count;
			
			for(i=0;i<cnt;i++)
			{
				DataRow drEach = moduleDataSet.Tables[0].Rows[i];
				String strChildID = (String)drEach["child_id"];
				String strChildName = (String)drEach["child_display_name"];
				
				String strChildIconImage = "";
				if(!drEach["child_icon_image"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					strChildIconImage = (String)drEach["child_icon_image"];
				}
				
				decimal iDisplayStyle = 0;
				if(!drEach["display_style"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					if (moduleDataSet.Tables[0].Columns["display_style"].DataType == System.Type.GetType("System.Decimal"))
						iDisplayStyle = (decimal)drEach["display_style"];
					else
						iDisplayStyle = (byte) drEach["display_style"];
				}
				
				String strChildURL = "";
				if(!drEach["child_url"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					strChildURL = (String)drEach["child_url"];
				}

				module = new Module();
				module.ModuleId = strChildID;
				module.ModuleIconImage = strChildIconImage;
				module.ModuleLinkURL = strChildURL;
				module.ModuleName = strChildName;
				module.ModuleNameStyle = System.Convert.ToInt32(iDisplayStyle);
				moduleList.Add(module);
			}
			return moduleList;
		}


		/// <summary>
		/// Insert modules for specified appid, enterpriseid, roleid and parent_module_id in the module_tree_role_relation table<br>
		/// These modules are fetched from core_module_tree_master table for specified applicationid and parentmoduleid.
		/// </summary>
		/// <param name="appID">ApplicationID for the current application</param>
		/// <param name="enterpriseID">EnterpriseID for the current enterprise</param>
		/// <param name="role">Role for which modules are to be added</param>
		/// <param name="parentModuleID">Parent Module id</param>
		/// <param name="modules">ArrayList containing objects of type Module</param>
		public static void AddModules(String appID, String enterpriseID,Role role,String parentModuleID,ArrayList modules)
		{
			DbConnection dbCon,dbCore = null;
			String strQry = null;
			DataSet moduleDataSet = null;
			IDbCommand dbCom = null;

			dbCon = DbConnectionManager.GetInstance().GetDbConnection(appID,enterpriseID);
			dbCore = DbConnectionManager.GetInstance().GetDbConnection(appID);
			for (int i=0;i<modules.Count;i++)
			{
				strQry = "select * from core_module_tree_master where applicationid = '"+appID+"'";
				strQry = strQry + " and parent_id = '"+parentModuleID+"'";
				strQry = strQry + " and child_id = '"+modules[i]+"'";
				dbCom = dbCore.CreateCommand(strQry,CommandType.Text);
				moduleDataSet = (DataSet)dbCore.ExecuteQuery(dbCom,com.common.DAL.ReturnType.DataSetType);
				if (moduleDataSet.Tables[0].Rows.Count >0)
				{
					DataRow drEach = moduleDataSet.Tables[0].Rows[i];
					String strChildID = (String)drEach["child_id"];
					String strChildName = (String)drEach["child_display_name"];
					String strParentID = (String)drEach["Parent_id"];
					String strChildIconImage = "";
					if(!drEach["child_icon_image"].GetType().Equals(System.Type.GetType("System.DBNull")))
					{
						strChildIconImage = (String)drEach["child_icon_image"];
					}
					
					decimal iDisplayStyle = 0;
					
					if(!drEach["display_style"].GetType().Equals(System.Type.GetType("System.DBNull")))
					{
						if (moduleDataSet.Tables[0].Columns["display_style"].DataType == System.Type.GetType("System.Decimal"))
							iDisplayStyle = (decimal)drEach["display_style"];
						else
							iDisplayStyle = (byte)drEach["display_style"];
					}
					String strChildURL = "";
					if(!drEach["child_url"].GetType().Equals(System.Type.GetType("System.DBNull")))
					{
						strChildURL = (String)drEach["child_url"];
					}
					strQry = " insert into module_tree_role_relation(roleid,enterpriseid,parentid,child_id,";
					strQry = strQry + " applicationid,child_name,child_icon_image,display_style,child_url)values (";
					strQry = strQry + role.RoleID+",'"+enterpriseID+"','"+strParentID+"','"+strChildID+"',";
					strQry = strQry + "'"+appID+"','"+strChildName+"','"+strChildIconImage+"','"+iDisplayStyle+"',";
					strQry = strQry + "'"+strChildURL+"')";
					dbCom = dbCon.CreateCommand(strQry,CommandType.Text);
					dbCon.ExecuteNonQuery(dbCom);

				}

			}
				
		}


		/// <summary>
		/// Insert single module along with specified IDU module rights into module_tree_role_relation table for the specified applicaitonid, 
		/// enterpriseid, roleid, parent_module_id and child_module_id<br>
		/// At present this method is not used in the UserMgt module instead its logic is now taken care in some other method which
		/// implements requirement specific database operations in a single transaction.
		/// </summary>
		/// <param name="appID">ApplicationID for the current application</param>
		/// <param name="enterpriseID">EnterpriseID for the current enterprise</param>
		/// <param name="role">Role object containing roleid</param>
		/// <param name="parentModuleID">parent module id as string value</param>
		/// <param name="module">Moduleid as string value</param>
		/// <param name="modRights">IDU Module rights for the specified role for this module</param>
		public static void AddModules(String appID, String enterpriseID,Role role,String parentModuleID,String module,String modRights)
		{
			DbConnection dbCon,dbCore = null;
			String strQry = null;
			DataSet moduleDataSet = null;
			IDbCommand dbCom = null;
			int cnt = 0; int i = 0;

			dbCon = DbConnectionManager.GetInstance().GetDbConnection(appID,enterpriseID);
			dbCore = DbConnectionManager.GetInstance().GetDbConnection(appID);
			strQry = "select * from core_module_tree_master where applicationid = '"+appID+"'";
			strQry = strQry + " and parent_id = '"+parentModuleID+"'";
			strQry = strQry + " and child_id = '"+module+"'";
			dbCom = dbCore.CreateCommand(strQry,CommandType.Text);
			moduleDataSet = (DataSet)dbCore.ExecuteQuery(dbCom,com.common.DAL.ReturnType.DataSetType);

			cnt = moduleDataSet.Tables[0].Rows.Count;
			
			for(i=0;i<cnt;i++)
			{
				DataRow drEach = moduleDataSet.Tables[0].Rows[i];
				String strChildID = (String)drEach["child_id"];
				String strChildName = (String)drEach["child_display_name"];
				String strParentID = (String)drEach["Parent_id"];
				String strChildIconImage = "";
				if(!drEach["child_icon_image"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					strChildIconImage = (String)drEach["child_icon_image"];
				}
				decimal iDisplayStyle = 0;
				if(!drEach["display_style"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					if (moduleDataSet.Tables[0].Columns["display_style"].DataType == System.Type.GetType("System.Decimal"))
						iDisplayStyle = (decimal)drEach["display_style"];
					else
						iDisplayStyle = (byte) drEach["display_style"];

				}
				String strChildURL = "";
				if(!drEach["child_url"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					strChildURL = (String)drEach["child_url"];
				}
				strQry = " insert into module_tree_role_relation(roleid,enterpriseid,parentid,child_id,";
				strQry = strQry + " applicationid,child_name,child_icon_image,display_style,child_url,module_rights)values (";
				strQry = strQry + role.RoleID+",'"+enterpriseID+"','"+strParentID+"','"+strChildID+"',";
				strQry = strQry + "'"+appID+"','"+strChildName+"','"+strChildIconImage+"','"+iDisplayStyle+"',";
				strQry = strQry + "'"+strChildURL+"','"+modRights+"')";
				dbCom = dbCon.CreateCommand(strQry,CommandType.Text);
				dbCon.ExecuteNonQuery(dbCom);

			}				
		}


		/// <summary>
		/// Insert single module along with specified IDU module rights into module_tree_user_relation table for the specified applicaitonid, 
		/// enterpriseid, userid, parent_module_id and child_module_id<br>
		/// At present this method is not used in the UserMgt module instead its logic is now taken care in some other method which
		/// implements requirement specific database operations in a single transaction.
		/// </summary>
		/// <param name="appID">ApplicationID for the current application</param>
		/// <param name="enterpriseID">EnterpriseID for the current enterprise</param>
		/// <param name="user">An object of type User</param>
		/// <param name="parentModuleID">Parent Module id</param>
		/// <param name="module">Child Module id</param>
		/// <param name="modRights">IDU accessrights to be given to the user</param>
		public static void AddModules(String appID, String enterpriseID,User user,String parentModuleID,String module,String modRights)
		{
			DbConnection dbCon,dbCore = null;
			String strQry = null;
			DataSet moduleDataSet = null;
			IDbCommand dbCom = null;
			int cnt = 0; int i = 0;

			dbCon = DbConnectionManager.GetInstance().GetDbConnection(appID,enterpriseID);
			dbCore = DbConnectionManager.GetInstance().GetDbConnection(appID);
			strQry = "select * from core_module_tree_master where applicationid = '"+appID+"'";
			strQry = strQry + " and parent_id = '"+parentModuleID+"'";
			strQry = strQry + " and child_id = '"+module+"'";
			dbCom = dbCore.CreateCommand(strQry,CommandType.Text);
			moduleDataSet = (DataSet)dbCore.ExecuteQuery(dbCom,com.common.DAL.ReturnType.DataSetType);

			cnt = moduleDataSet.Tables[0].Rows.Count;
			
			for(i=0;i<cnt;i++)
			{
				DataRow drEach = moduleDataSet.Tables[0].Rows[i];
				String strChildID = (String)drEach["child_id"];
				String strChildName = (String)drEach["child_display_name"];
				String strParentID = (String)drEach["Parent_id"];
				String strChildIconImage = "";
				if(!drEach["child_icon_image"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					strChildIconImage = (String)drEach["child_icon_image"];
				}
				decimal iDisplayStyle = 0;
				if(!drEach["display_style"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					if (moduleDataSet.Tables[0].Columns["display_style"].DataType == System.Type.GetType("System.Decimal"))
						iDisplayStyle = (decimal)drEach["display_style"];
					else
						iDisplayStyle = (byte) drEach["display_style"];
				}
				String strChildURL = "";
				if(!drEach["child_url"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					strChildURL = (String)drEach["child_url"];
				}
				strQry = " insert into module_tree_user_relation(userid,enterpriseid,parent_id,child_id,";
				strQry = strQry + " applicationid,child_name,child_icon_image,display_style,child_url,module_rights)values ('";
				strQry = strQry + user.UserID+"','"+enterpriseID+"','"+strParentID+"','"+strChildID+"',";
				strQry = strQry + "'"+appID+"','"+strChildName+"','"+strChildIconImage+"','"+iDisplayStyle+"',";
				strQry = strQry + "'"+strChildURL+"','"+modRights+"')";
				dbCom = dbCon.CreateCommand(strQry,CommandType.Text);
				dbCon.ExecuteNonQuery(dbCom);

			}				
		}


		/// <summary>
		/// Delete record(s) from the module_tree_role_relation table for the specified applicationid, enterpriseid and roleid.<br>
		/// At present this method is not used in the UserMgt module instead its logic is now taken care in some other method which
		/// implements requirement specific database operations in a single transaction.
		/// </summary>
		/// <param name="appID">ApplicationID for the current application</param>
		/// <param name="enterpriseID">EnterpriseID for the current enterprise</param>
		/// <param name="roleID">Role id of the role whose modules are tobe deleted</param>
		/// <returns>Returns true if deleted successfully else returns false</returns>
		public static bool DeleteModules(String appID, String enterpriseID,long roleID)
		{
			bool isDeleted = false;
			String strQry = null;
			try
			{
				DbConnection dbCon = null;
				IDbCommand dbCom = null;
				
				dbCon = DbConnectionManager.GetInstance().GetDbConnection(appID,enterpriseID);
				strQry = "delete from module_tree_role_relation where  enterpriseid = '"+enterpriseID+"'"+" and applicationid = '"+appID+"'"+"and roleid = "+roleID;
				dbCom = dbCon.CreateCommand(strQry,CommandType.Text);
				dbCon.ExecuteNonQuery(dbCom);
				isDeleted = true;
			}
			catch(Exception deleteUserException)
			{
				Console.WriteLine("Exception deleteUser	: "+deleteUserException.Message.ToString());
			}
			return isDeleted;
		}


		/// <summary>
		/// Delete record(s) from the module_tree_user_relation table for the specified applicationid, enterpriseid and userid.<br>
		/// At present this method is not used in the UserMgt module instead its logic is now taken care in some other method which
		/// implements requirement specific database operations in a single transaction.
		/// </summary>
		/// <param name="appID">ApplicationID for the current application</param>
		/// <param name="enterpriseID">EnterpriseID for the current enterprise</param>
		/// <param name="userID"></param>
		/// <returns></returns>
		public static bool DeleteModules(String appID, String enterpriseID,String userID)
		{
			bool isDeleted = false;
			String strQry = null;
			try
			{
				DbConnection dbCon = null;
				IDbCommand dbCom = null;
				
				dbCon = DbConnectionManager.GetInstance().GetDbConnection(appID,enterpriseID);
				strQry = "delete from module_tree_user_relation where  enterpriseid = '"+enterpriseID+"'"+" and applicationid = '"+appID+"'";
				strQry = strQry+" and userid = '"+userID+"'";
				dbCom = dbCon.CreateCommand(strQry,CommandType.Text);
				dbCon.ExecuteNonQuery(dbCom);
				isDeleted = true;
			}
			catch(Exception deleteUserException)
			{
				Console.WriteLine("Exception deleteUser	: "+deleteUserException.Message.ToString());
			}
			return isDeleted;
		}

	

		/// <summary>
		/// Get IDU module access rights for the specified applicationid, enterpriseid, userid, parent and child moduleid <br>
		/// from module_tree_role_relation table and module_tree_user relation table as distinct records.
		/// </summary>
		/// <param name="appID">ApplicationID for the current application</param>
		/// <param name="enterpriseID">EnterpriseID for the current enterprise</param>
		/// <param name="userid">User Id of the user whose access rights are required</param>
		/// <param name="moduleid">Child module id</param>
		/// <param name="parentid">Parent module id</param>
		/// <returns>AccessRights object which has Module, Insert, Update and Delete rights property initialized</returns>
		public static AccessRights GetModuleAccessRights(String appID, String enterpriseID, String userid, String moduleid, String parentid)
		{
			AccessRights accessRights = new AccessRights();

			
			

			String strQry = null;
			int cnt = 0,i = 0;
			DbConnection dbCon = null;
			DataSet modDataSet = null;
			IDbCommand dbCmd = null;

			strQry = "(select distinct c.module_rights as module_rights from module_tree_user_relation c where c.applicationid = '"+appID+"'";
			strQry = strQry+ " and c.enterpriseID = '"+enterpriseID+"'"+" and c.userid = '"+userid+"'" +" and c.parent_id = '"+parentid+"' and c.child_id = '"+moduleid+"')";
			strQry = strQry+ " union ";
			strQry = strQry+ " (select distinct a.module_rights as module_rights from module_tree_role_relation a where a.applicationid = '"+appID+"'";
			strQry = strQry+ " and a.enterpriseID = '"+enterpriseID+"'"+" and a.parentid = '"+parentid+"' and a.child_id = '"+moduleid+"' and roleid in ";
			strQry = strQry+ " (select distinct b.roleid from user_role_relation b where b.applicationid = '"+appID+"'";
			strQry = strQry+ " and b.enterpriseID = '"+enterpriseID+"'"+" and b.userid = '"+userid+"'"+" )";
			strQry = strQry+")";
			
			dbCon = DbConnectionManager.GetInstance().GetDbConnection(appID,enterpriseID);
			dbCmd = dbCon.CreateCommand(strQry,CommandType.Text);
			try
			{
				modDataSet =(DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
			}
			catch(ApplicationException appExpection)
			{
				Logger.LogTraceError("RBACManager","GetAllModules","RBAC001","Error in building tree");
				throw appExpection;
			}
			cnt = modDataSet.Tables[0].Rows.Count;

			if(cnt > 0)
			{
				DataRow drEach = modDataSet.Tables[0].Rows[i];

				String moduleRights = (String)drEach["module_rights"];

				accessRights.Module = true;

				if(moduleRights.IndexOf("I") != -1)
				{
					accessRights.Insert = true;
				}

				if(moduleRights.IndexOf("U") != -1)
				{
					accessRights.Update = true;
				}

				if(moduleRights.IndexOf("D") != -1)
				{
					accessRights.Delete = true;
				}
			}
			return accessRights;
		}


		/**** User related methods *************/

		/// <summary>
		/// Add record for a new user in user_master table for the specified applicationid and enterpriseid
		/// </summary>
		/// <param name="appID">ApplicationID for the current application</param>
		/// <param name="enterpriseID">EnterpriseID for the current enterprise</param>
		/// <param name="user">An object of type user</param>
		public static void AddUser(String appID, String enterpriseID,User user)
		{
			try
			{
				DbConnection dbCon = null;
				IDbCommand dbInsertQuery = null;
				String strUserID = user.UserID;
				String strUserName = user.UserName;
				String strUserPswd = user.UserPswd;
				String strUserCulture = user.UserCulture;
				String strUsrDept = user.UserDepartment;
				String strUsrEmail = user.UserEmail;
				String strUserType = user.UserType;
				String strQry = null;

				dbCon = DbConnectionManager.GetInstance().GetDbConnection(appID,enterpriseID);
				strQry = "insert into User_Master(enterpriseid,userid,applicationid,user_name,user_password,user_culture,email,department_name,user_type) ";
				strQry = strQry + " values('"+enterpriseID+"','"+strUserID+"','"+appID+"','";
				strQry = strQry + strUserName+"','"+strUserPswd+"','"+strUserCulture+"','"+Utility.ReplaceSingleQuote(strUsrEmail)+"','"+strUsrDept+"','"+strUserType+"')";
				dbInsertQuery = dbCon.CreateCommand(strQry,CommandType.Text);
				dbCon.ExecuteNonQuery(dbInsertQuery);
			}
			catch(Exception addUserException)
			{
				Console.WriteLine("Exception addUser	: "+addUserException.Message.ToString());
			}
		}


		/// <summary>
		/// Update record for the specified user in user_master table for the specified applicationid and enterpriseid
		/// </summary>
		/// <param name="appID">ApplicationID for the current application</param>
		/// <param name="enterpriseID">EnterpriseID for the current enterprise</param>
		/// <param name="user">An object of type user</param>
		public static void ModifyUser(String appID, String enterpriseID,User user)
		{
			try
			{
				DbConnection dbCon = null;
				IDbCommand dbUpdateQuery = null;
				String strUserID = user.UserID;
				String strUserName = user.UserName;
				String strUserPswd = user.UserPswd;
				String strUsrDept = user.UserDepartment;
				String strUsrEmail = user.UserEmail;
				String strUsrCulture = user.UserCulture;
				//String strUserType = user.UserType;
				String strQry = null;

				dbCon = DbConnectionManager.GetInstance().GetDbConnection(appID,enterpriseID);
				strQry = "Update User_Master set User_name='"+strUserName+"',";

				if ((strUserPswd != null) && (strUserPswd != ""))
				{
					strQry = strQry+" user_password='"+strUserPswd+"',";
				}
				strQry += " email ='"+Utility.ReplaceSingleQuote(strUsrEmail)+"',";
				strQry += " department_name ='"+strUsrDept+"',";
				strQry += " user_culture ='"+strUsrCulture+"'";
				strQry += " where enterpriseid = '"+enterpriseID+"' and applicationid ='"+appID+"' and userid = '"+strUserID+"'";
				dbUpdateQuery = dbCon.CreateCommand(strQry,CommandType.Text);
				dbCon.ExecuteNonQuery(dbUpdateQuery);
			}
			catch(Exception modifyUserException)
			{
				Console.WriteLine("Exception modifyUser	: "+modifyUserException.Message.ToString());
			}
		}


		/// <summary>
		/// Delete record for the specified userid from module_tree_user_relation, user_role_relation and User_Master tables in that 
		/// sequence in a single transaction.
		/// </summary>
		/// <param name="appID">ApplicationID for the current application</param>
		/// <param name="enterpriseID">EnterpriseID for the current enterprise</param>
		/// <param name="userID">User records to be deleted</param>
		/// <returns>Returns number of rows affected by the last non-query</returns>
		public static int DeleteUser(String appID, String enterpriseID,String userID)
		{
			IDbTransaction transaction = null;
			IDbConnection con = null;


			int iRowsAffected = -1;

			try
			{
				DbConnection dbCon = null;
				IDbCommand dbDeleteQuery = null;
				String strUserID = userID;
				String strQry = null;
		
				dbCon = DbConnectionManager.GetInstance().GetDbConnection(appID,enterpriseID);
				if(dbCon == null)
				{
					Logger.LogTraceError("RBACManager","DeleteUser","RBAC002","dbCon is null");
					throw new ApplicationException("Connection to Database failed",null);
				}

				con = dbCon.GetConnection();
				dbCon.OpenConnection(ref con);
				transaction = dbCon.BeginTransaction(con,IsolationLevel.ReadCommitted);


				strQry = "delete from module_tree_user_relation where applicationid = ";
				strQry = strQry + "'"+appID+"'"+" and enterpriseid = '"+enterpriseID+"'";
				strQry = strQry + " and userid = '"+userID+"'";

				dbDeleteQuery = dbCon.CreateCommand(strQry,CommandType.Text);
				dbDeleteQuery.Connection = con;
				dbDeleteQuery.Transaction = transaction;

			
				iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbDeleteQuery);

				strQry = "delete from user_role_relation where applicationid = ";
				strQry = strQry + "'"+appID+"'"+" and enterpriseid = '"+enterpriseID+"'";
				strQry = strQry + " and userid = '"+userID+"'";
				
				dbDeleteQuery.CommandText = strQry;
				iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbDeleteQuery);

				strQry = "delete from User_Master where  enterpriseid = '"+enterpriseID+"'"+" and applicationid = '"+appID+"'"+"and userid = '"+userID+"'";
				
				dbDeleteQuery.CommandText = strQry;
				iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbDeleteQuery);
				transaction.Commit();
			}
			catch(ApplicationException appException)
			{
				transaction.Rollback();
				Logger.LogTraceError("RBACManager","DeleteUser","RBAC003",appException.Message.ToString());
				throw new ApplicationException("Delete operation failed",appException);
			}
			finally
			{
				con.Close();
			}
			return iRowsAffected;
		}


		/// <summary>
		/// Delete record for the specified user from module_tree_user_relation, user_role_relation and User_Master tables in that 
		/// sequence in a single transaction.
		/// </summary>
		/// <param name="appID">ApplicationID for the current application</param>
		/// <param name="enterpriseID">EnterpriseID for the current enterprise</param>
		/// <param name="user">An object of type user</param>
		public static void DeleteUser(String appID, String enterpriseID,User user)
		{
			try
			{
				String strUserID = user.UserID;
				DeleteUser(appID,enterpriseID,strUserID);
			}
			catch(Exception deleteUserException)
			{
				Console.WriteLine("Exception deleteUser	: "+deleteUserException.Message.ToString());
			}
		}



		/// <summary>
		/// Get user record from the user_master table for the specified applicationid, enterpriseid and userid
		/// </summary>
		/// <param name="appID">ApplicationID for the current application</param>
		/// <param name="enterpriseID">EnterpriseID for the current enterprise</param>
		/// <param name="userID">User id whose details need to be fetched</param>
		/// <returns>User object which has various properties initialized</returns>
		public static User GetUser(String appID, String enterpriseID,String userID)
		{
			User user = null;
			try
			{
				DbConnection dbCon = null;
				IDbCommand dbCmd = null;
				String strQry = null;

				dbCon = DbConnectionManager.GetInstance().GetDbConnection(appID,enterpriseID);
				strQry = "select userid, user_name, user_password, user_culture,email,department_name, User_Type, payerid, Location from user_master where  enterpriseid ='"+enterpriseID+"'" + " and applicationid = '"+appID+"'" + " and userid = "+ "'"+userID+"'";
				dbCmd = dbCon.CreateCommand(strQry,CommandType.Text);
				DataSet usrDataSet =(DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
				int cnt = usrDataSet.Tables[0].Rows.Count;
				
				if (cnt>0)
				{
					DataRow drEach = usrDataSet.Tables[0].Rows[0];
					String strUserID = (String)drEach["userid"];
					String strUserName = (String)drEach["user_name"];
					String strUserCulture = (String)drEach["user_culture"];
					String strEmail = "";
					String strUserLocation = "";

					if(!drEach["email"].GetType().Equals(System.Type.GetType("System.DBNull")))
					{
						strEmail = (String)drEach["email"];
					}
					if(!drEach["Location"].GetType().Equals(System.Type.GetType("System.DBNull")))
					{
						strUserLocation = (String)drEach["Location"];
					}
					String strUserDept = "";
					if(!drEach["department_name"].GetType().Equals(System.Type.GetType("System.DBNull")))
					{
						strUserDept = (String)drEach["department_name"];
					}
					String strUserType = "";
					if(!drEach["User_Type"].GetType().Equals(System.Type.GetType("System.DBNull")))
					{
						strUserType = (String)drEach["User_Type"];
					}		
	
					String strPayer = "";
					if(!drEach["PayerID"].GetType().Equals(System.Type.GetType("System.DBNull")))
					{
						strPayer = (String)drEach["PayerID"];
					}	
					
					
					user = new User();
					user.UserID = strUserID;
					user.UserName = strUserName;
					user.UserCulture = strUserCulture;
					user.UserDepartment = strUserDept;					
					user.UserType=strUserType;
					user.UserEmail = strEmail;
					user.UserLocation = strUserLocation;
					user.PayerID = strPayer;
					
				}
			}
			catch(Exception getUserException)
			{
				Console.WriteLine("Exception getUser	: "+getUserException.Message.ToString());
				return user;
			}
			return user;
		}


		/// <summary>
		/// Get all users from the user_master table for the specified applicationid, enterpriseid and various
		/// search criteria in the same table data
		/// </summary>
		/// <param name="appID">ApplicationID for the current application</param>
		/// <param name="enterpriseID">EnterpriseID for the current enterprise</param>
		/// <param name="user">An object of type user which stores various search criteria</param>
		/// <returns>ArrayList containing objects of type User</returns>
		public static ArrayList GetAllUsers(String appID, String enterpriseID,User user)
		{
			ArrayList userArrayList = new ArrayList();
			DbConnection dbCon = null;
			IDbCommand dbCmd = null;
			
			String strQry = "select * from user_master where applicationid = ";
			strQry = strQry+"'"+appID+"'"+" and enterpriseid = '"+enterpriseID+"' and userid != 'administrator' ";
			
			if  ((user.UserID != null)&& (user.UserID != ""))
			{
				//strQry = strQry+ " and userid = '"+user.UserID+"'";
				strQry = strQry+ " and userid like '%"+user.UserID+"%'";
			}
				
			if ((user.UserName != null) && (user.UserName != ""))
			{
				strQry = strQry+" and user_name like '%"+user.UserName+"%'";
			}

			if ((user.UserDepartment != null) && (user.UserDepartment != ""))
			{
				strQry = strQry+" and department_name like '%"+user.UserDepartment+"%'";
			}

			if ((user.UserEmail != null) && (user.UserEmail != ""))
			{
				strQry = strQry+" and email like '%"+Utility.ReplaceSingleQuote(user.UserEmail)+"%'";
			}

			if (user.UserType != "0")
			{
				strQry = strQry+" and user_type = '"+user.UserType+"'";
			}

			if (user.UserCulture != "")
			{
				strQry = strQry+" and user_culture ='"+user.UserCulture+"'";
			}
			if (user.UserLocation !="")
			{
				strQry = strQry+" and location ='"+user.UserLocation+"'";
			}
			if (user.CustomerAccount !="")
			{
				strQry = strQry+" and payerid ='"+user.CustomerAccount+"'";
			}

			dbCon = DbConnectionManager.GetInstance().GetDbConnection(appID,enterpriseID);
			dbCmd = dbCon.CreateCommand(strQry,CommandType.Text);
			DataSet  userData = (DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
			
			int cnt = userData.Tables[0].Rows.Count;
			int i = 0;
			for(i = 0;i<cnt;i++)
			{
				DataRow drEach = userData.Tables[0].Rows[i];
				String strUserID = (String)drEach["userid"];
				String strUserName = (String)drEach["user_name"];
				String strUserCulture = (String)drEach["user_culture"];
				String strUserType = (String)drEach["user_type"];
				String strUserPswd = (String)drEach["user_password"];
				String strEmail = "";
				String strUserLocation = "";
				String strCustomerAccount = "";
				if(!drEach["Location"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					strUserLocation = (String)drEach["Location"];
				}

				if(!drEach["payerid"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					strCustomerAccount = (String)drEach["payerid"];
				}
				if(!drEach["email"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					strEmail = (String)drEach["email"];
				}
				String strUserDept = "";
				if(!drEach["department_name"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					strUserDept = (String)drEach["department_name"];
				}
					
				user = new User();
				user.UserID = strUserID;
				user.UserName = strUserName;
				user.UserCulture = strUserCulture;
				user.UserDepartment = strUserDept;
				user.UserEmail = strEmail;
				user.UserType = strUserType;
				user.UserPswd = strUserPswd;
				user.UserLocation = strUserLocation;
				user.CustomerAccount= strCustomerAccount;
				userArrayList.Add(user);
			}


			return userArrayList;

		}

		/// <summary>
		/// Get records for one ore more users from user_master table for the specified applicationid, enterpriseid and optionally the userid
		/// </summary>
		/// <param name="appID">ApplicationID for the current application</param>
		/// <param name="enterpriseID">EnterpriseID for the current enterprise</param>
		/// <param name="strUserid">User id of the user whose profile is required</param>
		/// <returns>ArrayList containing objects of User type</returns>
		public static ArrayList GetUserProfile(String appID, String enterpriseID,String strUserid)
		{
			ArrayList userArrayList = new ArrayList();
			DbConnection dbCon = null;
			IDbCommand dbCmd = null;
			
			String strQry = "select * from user_master where applicationid = ";
			strQry = strQry+"'"+appID+"'"+" and enterpriseid = '"+enterpriseID+"'";
			
			if  ((strUserid != null)&& (strUserid != ""))
			{
				strQry = strQry+ " and userid = '"+strUserid+"'";
			}
				
			dbCon = DbConnectionManager.GetInstance().GetDbConnection(appID,enterpriseID);
			dbCmd = dbCon.CreateCommand(strQry,CommandType.Text);
			DataSet  userData = (DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
			
			int cnt = userData.Tables[0].Rows.Count;
			int i = 0;
			for(i = 0;i<cnt;i++)
			{
				DataRow drEach = userData.Tables[0].Rows[i];
				String strUserID = (String)drEach["userid"];
				String strUserName = (String)drEach["user_name"];
				String strUserCulture = (String)drEach["user_culture"];
				String strUserType = (String)drEach["user_type"];
				String strUserPswd = (String)drEach["user_password"];
				String strEmail = "";
				if(!drEach["email"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					strEmail = (String)drEach["email"];
				}
				String strUserDept = "";
				if(!drEach["department_name"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					strUserDept = (String)drEach["department_name"];
				}
					

				User user = new User();
				user.UserID = strUserID;
				user.UserName = strUserName;
				user.UserCulture = strUserCulture;
				user.UserDepartment = strUserDept;
				user.UserEmail = strEmail;
				user.UserType = strUserType;
				user.UserPswd = strUserPswd;
				userArrayList.Add(user);
			}


			return userArrayList;

		}

		/// <summary>
		/// Verify whether specified userid for the applicationid and enterpriseid has the specified password. This verification
		/// is done from user_master table
		/// </summary>
		/// <param name="appID">ApplicationID for the current application</param>
		/// <param name="enterpriseID">EnterpriseID for the current enterprise</param>
		/// <param name="userID">ID of the user to be validated</param>
		/// <param name="userPswd">Password of the user</param>
		/// <returns>If authenticated returns true. If not authenticated returns false</returns>
		public static bool AuthenticateUser(String appID, String enterpriseID,String userID,String userPswd)
		{
			bool isValid = false;
			DbConnection dbCon = null;
			String strQry = null;
			IDbCommand dbCmd = null;
			DataSet authenDataSet = null;

			dbCon = DbConnectionManager.GetInstance().GetDbConnection(appID,enterpriseID);
			if(dbCon == null)
			{
				Console.WriteLine("Connection to the Enterprise Failed. Contact System Administrator");
				return false;
			}


			strQry = "select * from user_master where applicationid = '"+appID+"'";
			/*Comment by GwanG on 20June08*/
			//strQry = strQry+" and enterpriseid='"+enterpriseID+"'"+ " and userid ='"+userID+"'"+" and user_password ='"+userPswd+"'";
			//strQry = strQry+" and enterpriseid='"+enterpriseID+"'"+ " and userid ='"+userID+"'"+" and user_password =(select dbo.BlowFishEncode('"+userPswd+"',dbo.BlowFishKey()))";
			strQry = strQry+" and enterpriseid='"+enterpriseID+"'"+ " and userid ='"+userID+"'"+" and  '"+userPswd+"' =(select dbo.BlowFishDecode(user_password,dbo.BlowFishKey()))";
			//
			dbCmd = dbCon.CreateCommand(strQry,CommandType.Text);
			authenDataSet = (DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
			if (authenDataSet.Tables[0].Rows.Count >0)
				isValid = true;
			else
				isValid = false;

			return isValid;

		}


		/***** User-Role Relation methods ******/


		/// <summary>
		/// Assign one or more role(s) to the specified user for the specified applicationid and enterpriseid.
		/// This is done by adding records into user_role_relation table
		/// </summary>
		/// <param name="appID">ApplicationID for the current application</param>
		/// <param name="enterpriseID">EnterpriseID for the current enterprise</param>
		/// <param name="user">An object of type user</param>
		/// <param name="roleList">ArrayList containing list of Roles to be assigned to the user</param>
		/// <returns>returns 1 if the last row is added successfully</returns>
		public static int AssignRolesToUser(String appID, String enterpriseID,User user,ArrayList roleList)
		{
			DbConnection dbCon = null;
			IDbCommand dbCmd = null;
			String strQry = null;
			int rowsEffected = 0;
			int iRoleID = 0;

			dbCon = DbConnectionManager.GetInstance().GetDbConnection(appID,enterpriseID);
			foreach(Role role in roleList)
			{
				iRoleID = (int)role.RoleID;

				strQry = "insert into user_role_relation(roleid,userid,enterpriseid,applicationid)values(";
				strQry = strQry +role.RoleID+",'"+user.UserID+"','"+enterpriseID+"','"+appID+"')";
				dbCmd = dbCon.CreateCommand(strQry,CommandType.Text);
				rowsEffected = dbCon.ExecuteNonQuery(dbCmd);
			}
			return rowsEffected;
		}
	

		/// <summary>
		/// Delete user-role relation records from the user_role_relation table for the specified applicatonid, enterpriseid and the user
		/// </summary>
		/// <param name="appID">ApplicationID for the current application</param>
		/// <param name="enterpriseID">EnterpriseID for the current enterprise</param>
		/// <param name="user">An object of type user whose user-role relation to be revoked</param>
		/// <returns>Returns number of rows deleted successfully</returns>
		public static int RevokeRolesFromUser(String appID, String enterpriseID,User user)
		{
			DbConnection dbCon = null;
			IDbCommand dbCmd = null;
			String strQry = null;
			int rowsEffected = 0;
			
			dbCon = DbConnectionManager.GetInstance().GetDbConnection(appID,enterpriseID);
			
			strQry = "delete from user_role_relation where applicationid = '"+appID+"'";
			strQry = strQry + " and enterpriseid = '"+enterpriseID+"' and userid = '"+user.UserID+"'";
			dbCmd = dbCon.CreateCommand(strQry,CommandType.Text);
			rowsEffected = dbCon.ExecuteNonQuery(dbCmd);
			
			return rowsEffected;
		}


		/// <summary>
		/// Get all users for the specified role for the specified applicationid, enterpriseid from user_role_relation.
		/// </summary>
		/// <param name="appID">ApplicationID for the current application</param>
		/// <param name="enterpriseID">EnterpriseID for the current enterprise</param>
		/// <param name="role">An object of type Role whose members (Users) are to be fetched</param>
		/// <returns>ArrayList containing objects of type User</returns>
		public static ArrayList GetAllRoleMemebers(String appID, String enterpriseID,Role role)
		{
			ArrayList usrList = new ArrayList();
			DbConnection dbCon = null;
			String strQry = null;
			IDbCommand dbCmd = null;
			DataSet usrDataSet = null;
			int cnt = 0,i = 0;
			User user = null;

			dbCon = DbConnectionManager.GetInstance().GetDbConnection(appID,enterpriseID);
			strQry = "select a.userid,b.user_name, b.user_culture from user_role_relation a,user_master b where a.roleid = "+role.RoleID+" and b.userid = a.userid ";
			dbCmd = dbCon.CreateCommand(strQry,CommandType.Text);
			usrDataSet =(DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
			cnt = usrDataSet.Tables[0].Rows.Count;

			for(i=0;i<cnt;i++)
			{
				DataRow drEach = usrDataSet.Tables[0].Rows[i];
				String strUserID = (String)drEach["userid"];
				String strUserName = (String)drEach["user_name"];
				String strUserCulture = (String)drEach["user_culture"];
				user = new User();
				user.UserID = strUserID;
				user.UserName = strUserName;
				user.UserCulture = strUserCulture;
				usrList.Add(user);				
			}
			return	usrList;
		}




		/***** Role related methods   *************/


		/// <summary>
		/// Add record for a new role in Role_Master table for the specified applicationid and enterpriseid
		/// </summary>
		/// <param name="appID">ApplicationID for the current application</param>
		/// <param name="enterpriseID">EnterpriseID for the current enterprise</param>
		/// <param name="role">Role to be added in the database</param>
		/// <returns></returns>
		public static decimal AddRole(String appID, String enterpriseID,Role role)
		{
			//int iRoleID = 0;
			decimal iRoleID = 0;
			try
			{
				DbConnection dbCon = null;
				IDbCommand dbCom = null;
				String strQry = null;
				String strRoleName = role.RoleName;
				iRoleID = Counter.GetNext(appID,enterpriseID,"roleid");
				dbCon = DbConnectionManager.GetInstance().GetDbConnection(appID,enterpriseID);
				strQry = "insert into Role_Master(applicationid,enterpriseid,roleid,role_name,role_description) values('"+appID+"','"+enterpriseID+"'"+","+iRoleID+",'"+strRoleName+"','"+role.RoleDescription+"'"+")";
				dbCom = dbCon.CreateCommand(strQry,CommandType.Text);
				dbCon.ExecuteNonQuery(dbCom);
			}
			catch(Exception addRoleException)
			{
				Console.WriteLine("Exception addRole	: "+addRoleException.Message.ToString());
			}
			return iRoleID;
		}

		/// <summary>
		/// Update record for the specified role in role_master table for the specified applicationid and enterpriseid
		/// </summary>
		/// <param name="appID">ApplicationID for the current application</param>
		/// <param name="enterpriseID">EnterpriseID for the current enterprise</param>
		/// <param name="role">Role to be modified</param>
		public static void ModifyRole(String appID, String enterpriseID,Role role)
		{
			try
			{
				DbConnection dbCon = null;
				IDbCommand dbUpdateQuery = null;
				decimal iRoleID = role.RoleID;
				String strRoleName = role.RoleName;
				String strQry = null;

				dbCon = DbConnectionManager.GetInstance().GetDbConnection(appID,enterpriseID);
				strQry = "update Role_Master set Role_name = '"+strRoleName+"',"+"role_description = '"+role.RoleDescription+"'"+" where enterpriseid = '"+enterpriseID+"'"+" and applicationid = '"+appID+"'" + " and roleid ="+iRoleID;
				dbUpdateQuery = dbCon.CreateCommand(strQry,CommandType.Text);
				dbCon.ExecuteNonQuery(dbUpdateQuery);
			}
			catch(Exception modifyRoleException)
			{
				Console.WriteLine("Exception modifyRole	: "+modifyRoleException.Message.ToString());
			}
		}


		/// <summary>
		/// Delete records from the role_master table for the specified applicationid, enterpriseid and roleid
		/// </summary>
		/// <param name="appID">ApplicationID for the current application</param>
		/// <param name="enterpriseID">EnterpriseID for the current enterprise</param>
		/// <param name="role">Role to be deleted</param>
		public static void DeleteRole(String appID, String enterpriseID,Role role)
		{
			try
			{
				decimal iRoleID = role.RoleID;
				DeleteRole(appID,enterpriseID,iRoleID);
			}
			catch(Exception deleteRoleException)
			{
				Console.WriteLine("Exception deleteRole (1)	: "+deleteRoleException.Message.ToString());
			}
		}

		/// <summary>
		/// Delete records from the role_master table for the specified applicationid, enterpriseid and roleid
		/// </summary>
		/// <param name="appID">ApplicationID for the current application</param>
		/// <param name="enterpriseID">EnterpriseID for the current enterprise</param>
		/// <param name="roleID">Roleid of the role to be deleted</param>
		public static void DeleteRole(String appID, String enterpriseID,decimal roleID)
		{
			try
			{
				DbConnection dbCon = null;
				IDbCommand dbDeleteQuery = null;
				decimal iRoleID = roleID;
				String strQry = null;
			
				dbCon = DbConnectionManager.GetInstance().GetDbConnection(appID,enterpriseID);
				strQry = "delete from Role_Master where  enterpriseid = '"+enterpriseID+"'"+" and applicationid = '"+appID+"'"+" and roleid = "+roleID;
				strQry = strQry + " and role_name != 'system'";
				dbDeleteQuery = dbCon.CreateCommand(strQry,CommandType.Text);
				dbCon.ExecuteNonQuery(dbDeleteQuery);
			}
			catch(Exception deleteRoleException)
			{
				Console.WriteLine("Exception deleteRole	(2): "+deleteRoleException.Message.ToString());
			}
		}


		/// <summary>
		/// Get the role details from the role_master table for the speicifed applicationid, enterpriseid and roleid
		/// </summary>
		/// <param name="appID">ApplicationID for the current application</param>
		/// <param name="enterpriseID">EnterpriseID for the current enterprise</param>
		/// <param name="roleID">id of the role which needs to be fetched</param>
		/// <returns>Role details as Role object</returns>
		public static Role GetRole(String appID, String enterpriseID,int roleID)
		{
			Role role = null;
			try
			{
				DbConnection dbCon = null;
				IDbCommand dbCmd = null;
				String strQry = null;

				dbCon = DbConnectionManager.GetInstance().GetDbConnection(appID,enterpriseID);
				strQry = "select roleid, role_name from role_master where  enterpriseid ='"+enterpriseID+"'" + " and applicationid = '"+appID+"'" + " and roleid = "+ roleID;
				dbCmd = dbCon.CreateCommand(strQry,CommandType.Text);
				DataSet roleDataSet =(DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);

				DataRow drEach = roleDataSet.Tables[0].Rows[0];
				int iRoleID = (int)drEach["roleid"];
				String strRoleName = (String)drEach["role_name"];
				role = new Role();
				role.RoleName = strRoleName;
				role.RoleID = iRoleID;
			}
			catch(Exception getRoleException)
			{
				Console.WriteLine("Exception getRole: "+getRoleException.Message.ToString());
			}
			return role;
		}


		/// <summary>
		/// Get all roles excluding the system role. If roleid is 0, get all roles from the role_master table 
		/// for the specified applicationid and enterpriseid
		/// </summary>
		/// <param name="appID">ApplicationID for the current application</param>
		/// <param name="enterpriseID">EnterpriseID for the current enterprise</param>
		/// <param name="role">An object of type role to specify various search criteria</param>
		/// <returns>ArrayList containing objects of type Role</returns>
		public static ArrayList GetAllRoles(String appID, String enterpriseID,Role role)
		{
			ArrayList roleArrayList = new ArrayList();
			int i = 0;
			int cnt = 0;
			DbConnection dbCon = null;
			IDbCommand dbCmd = null;
			
			String strQry = "select role_description,roleid,role_name from role_master where applicationid = ";
			strQry = strQry+"'"+appID+"'"+" and enterpriseid = '"+enterpriseID+"'";
			strQry = strQry+" and role_name != 'system'";
			
			if  ((role.RoleID > 0))
			{
				strQry = strQry+ " and roleid = "+role.RoleID;
			}
				
			if ((role.RoleName != null) && (role.RoleName != ""))
			{
				strQry = strQry+" and role_name like '%"+role.RoleName+"%'";
			}

			if ((role.RoleDescription != null) && (role.RoleDescription != ""))
			{
				strQry = strQry+" and role_description like '%"+role.RoleDescription+"%'";
			}
			dbCon = DbConnectionManager.GetInstance().GetDbConnection(appID,enterpriseID);
			dbCmd = dbCon.CreateCommand(strQry,CommandType.Text);
			DataSet  roleData = (DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
			cnt = roleData.Tables[0].Rows.Count;
			for(i=0;i<cnt;i++)
			{
				DataRow drEach = roleData.Tables[0].Rows[i];
				decimal iRoleID = System.Convert.ToDecimal(drEach["roleid"]);
				String strRoleName = (String)drEach["role_name"];
				String strRoleDesc = "";
				if(!drEach["role_description"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					strRoleDesc = (String)drEach["role_description"];
				}
				role = new Role();
				role.RoleID = iRoleID;
				role.RoleName = strRoleName;
				role.RoleDescription = strRoleDesc;
				roleArrayList.Add(role);
			}
			return roleArrayList;

		}


		/// <summary>
		/// Get all roles for the specified user. 
		/// </summary>
		/// <param name="appID">ApplicationID for the current application</param>
		/// <param name="enterpriseID">EnterpriseID for the current enterprise</param>
		/// <param name="user">An object of type user whose all associated roles need to be fetched</param>
		/// <returns>ArrayList containing objects of type Role</returns>
		public static ArrayList GetAllRoles(String appID, String enterpriseID,User user)
		{
			ArrayList roleList = new ArrayList();
			DbConnection dbCon = null;
			String strQry = null;
			IDbCommand dbCmd = null;
			DataSet roleDataSet = null;
			int cnt = 0,i = 0;
			Role role = null;

			dbCon = DbConnectionManager.GetInstance().GetDbConnection(appID,enterpriseID);
			strQry = "select distinct a.roleid,b.role_name from user_role_relation a,role_master b ";
			strQry = strQry + " where a.applicationid = '"+appID+"'";
			strQry = strQry + " and a.enterpriseid = '"+enterpriseID+"'";
			strQry = strQry + "	and a.userid = '"+user.UserID+"'";
			strQry = strQry + " and b.roleid = a.roleid ";
			strQry = strQry + " and b.role_name != 'system'";
			strQry = strQry + " and b.applicationid = a.applicationid";
			strQry = strQry + " and b.enterpriseid = a.enterpriseid";

			dbCmd = dbCon.CreateCommand(strQry,CommandType.Text);
			roleDataSet =(DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
			
			cnt = roleDataSet.Tables[0].Rows.Count;

			for(i=0;i<cnt;i++)
			{
				DataRow drEach = roleDataSet.Tables[0].Rows[i];
				decimal iRoleID = System.Convert.ToDecimal(drEach["roleid"]);
				String strRoleName = (String)drEach["role_name"];
				role = new Role();
				role.RoleName = strRoleName;
				role.RoleID = iRoleID;
				roleList.Add(role);				
			}

			return roleList;
		}

	

		/******* Permission related methods */

		/// <summary>
		/// Get all permissions from the permission_role_relation and  core_permission_master table for the specified applicationid, enterpriseid and roleid
		/// </summary>
		/// <param name="appID">ApplicationID for the current application</param>
		/// <param name="enterpriseID">EnterpriseID for the current enterprise</param>
		/// <param name="role">Role whose permissions need to be fetched</param>
		/// <returns>ArrayList containing objects of type Permission</returns>
		public static ArrayList GetAllPermissions(String appID, String enterpriseID,Role role)
		{
			ArrayList permissionList = new ArrayList();
			DbConnection dbCon, dbCoreCon = null;
			String strQry = null;
			IDbCommand dbCmd = null;
			DataSet permDataSet,coreDataSet = null;
			int cnt = 0,i = 0;
			Permission permission = null;
			
			dbCon = DbConnectionManager.GetInstance().GetDbConnection(appID,enterpriseID);
			strQry = "select a.permissionid from permission_role_relation a where applicationid='"+appID+"'"+" and enterpriseid='"+enterpriseID+"'"+ " and a.roleid = "+role.RoleID;
			dbCmd = dbCon.CreateCommand(strQry,CommandType.Text);
			permDataSet =(DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
			cnt = permDataSet.Tables[0].Rows.Count;

			dbCoreCon = DbConnectionManager.GetInstance().GetDbConnection(appID);
			for(i=0;i<cnt;i++)
			{
				DataRow drEach = permDataSet.Tables[0].Rows[i];
				String iPermissionID = (String)drEach["permissionid"];
				permission = new Permission();
				permission.PermissionID = iPermissionID;
				strQry = "select a.permission_description,a.allow_data_restriction from core_permission_master a where a.applicationid ="+ "'"+appID+"'"+" and a.permissionid = '"+iPermissionID+"'";
				dbCmd = dbCoreCon.CreateCommand(strQry,CommandType.Text);
				coreDataSet = (DataSet)dbCoreCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
				DataRow drCorePerm = coreDataSet.Tables[0].Rows[0];
				String strPermissionDesc = (String)drCorePerm["permission_description"];
				bool IsAllowDataRestriction = (bool)drCorePerm["allow_data_restriction"];
				permission.setAllowDataRestriction(IsAllowDataRestriction);
				permission.PermissionDesc = strPermissionDesc;
				permissionList.Add(permission);				
			}

			return permissionList;
		}


		/// <summary>
		/// Get all distinct permissions from the permission_User_relation, permission_role_relation and  core_permission_master table for the specified applicationid, enterpriseid and roleid
		/// </summary>
		/// <param name="appID">ApplicationID for the current application</param>
		/// <param name="enterpriseID">EnterpriseID for the current enterprise</param>
		/// <param name="user">User whose permissions need to be fetched</param>
		/// <returns>ArrayList containing objects of type Permission</returns>
		public static ArrayList GetAllPermissions(String appID, String enterpriseID,User user)
		{
			ArrayList permissionList = new ArrayList();
			DbConnection dbCon, dbCoreCon = null;
			String strQry = null;
			IDbCommand dbCmd = null;
			DataSet permDataSet,coreDataSet = null;
			int cnt = 0,i = 0;
			Permission permission = null;
			
			dbCon = DbConnectionManager.GetInstance().GetDbConnection(appID,enterpriseID);
			strQry = "(select distinct a.permissionid from permission_User_relation a where a.applicationid='"+appID+"'"+" and a.enterpriseid='"+enterpriseID+"'"+ " and a.userid = '"+user.UserID+"')";
			strQry = strQry+" union (select distinct permissionid from permission_role_relation where applicationid ='"+appID+"'"+" and enterpriseid = '"+enterpriseID+"'"+" and roleid in (select distinct roleid from user_role_relation ";
			strQry = strQry+" where applicationid='"+appID+"'"+" and enterpriseid='"+enterpriseID+"'"+" and userid ='"+user.UserID+"'"+" ))";
			dbCmd = dbCon.CreateCommand(strQry,CommandType.Text);
			permDataSet =(DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
			cnt = permDataSet.Tables[0].Rows.Count;

			dbCoreCon = DbConnectionManager.GetInstance().GetDbConnection(appID);
			for(i=0;i<cnt;i++)
			{
				DataRow drEach = permDataSet.Tables[0].Rows[i];
				String iPermissionID = (String)drEach["permissionid"];
				permission = new Permission();
				permission.PermissionID = iPermissionID;
				strQry = "select a.permission_description,a.allow_data_restriction from core_permission_master a where a.applicationid ="+ "'"+appID+"'"+" and a.permissionid = '"+iPermissionID+"'";
				dbCmd = dbCoreCon.CreateCommand(strQry,CommandType.Text);
				coreDataSet = (DataSet)dbCoreCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
				DataRow drCorePerm = coreDataSet.Tables[0].Rows[0];
				String strPermissionDesc = (String)drCorePerm["permission_description"];
				bool IsAllowDataRestriction = (bool)drCorePerm["allow_data_restriction"];
				permission.setAllowDataRestriction(IsAllowDataRestriction);
				permission.PermissionDesc = strPermissionDesc;
				permissionList.Add(permission);				
			}

			return permissionList;
		}


		/// <summary>
		/// Get permission owners (Role or User(s)) for the specified applicationid, enterpriseid and permission. 
		/// </summary>
		/// <param name="appID">ApplicationID for the current application</param>
		/// <param name="enterpriseID">EnterpriseID for the current enterprise</param>
		/// <param name="permission">Permissions whose owner is required</param>
		/// <param name="ownerType">One of enum OwnerType value (User or Role)</param>
		/// <returns>If the owner type is OwnerType.User then returns ArrayList containing objects of type User <br>
		///			 and if the owner type is OwnerType.Role then returns ArrayList containing objects of type Role.
		/// </returns>
		public static ArrayList GetAllPermissionOwners(String appID, String enterpriseID,Permission permission,OwnerType ownerType)
		{
			ArrayList ownerList = new ArrayList();
			DbConnection dbCon = null;
			String strQry = null;
			IDbCommand dbCmd = null;
			DataSet roleDataSet = null;
			int cnt = 0, i = 0;
			User user = null;
			Role role = null;

			dbCon = DbConnectionManager.GetInstance().GetDbConnection(appID,enterpriseID);

			if (ownerType == com.common.RBAC.OwnerType.User)
			{
				strQry = "(select distinct a.userid,b.user_name,b.user_culture from permission_user_relation a,user_master b ";
				strQry = strQry+ " where a.applicationid='"+appID+"'"+" and a.enterpriseid='"+enterpriseID+"'" + " and a.permissionid='"+permission.PermissionID+"'";
				strQry = strQry+ " and b.applicationid = a.applicationid and a.enterpriseid = b.enterpriseid and b.userid = a.userid )";
				strQry = strQry+ " union ";
				strQry = strQry+ " (select distinct c.userid,d.user_name,d.user_culture from user_role_relation c,user_master d ";
				strQry = strQry+ " where c.applicationid ="+ "'"+appID+"'"+" and c.enterpriseid =" +"'"+enterpriseID+"'"+" and d.applicationid = c.applicationid ";
				strQry = strQry+ " and d.enterpriseid = c.enterpriseid and d.userid = c.userid and c.roleid in (select e.roleid from permission_role_relation e where ";
				strQry = strQry+ " e.applicationid = '"+appID+"'"+" and e.enterpriseid = '"+enterpriseID+"'"+" and permissionid = '"+permission.PermissionID+"')";
				strQry = strQry+ ")";

			}
			else if (ownerType == com.common.RBAC.OwnerType.Role)
			{
				strQry = "select a.roleid, b.role_name from permission_role_relation a,role_master b ";
				strQry = strQry+ " where a.applicationid = '"+appID+"'"+" and a.enterpriseid = '"+enterpriseID+"'";
				strQry = strQry+ " and a.permissionid = '"+permission.PermissionID+"'" ;
				strQry = strQry+ " and b.applicationid = a.applicationid ";
				strQry = strQry+ " and b.enterpriseid  = a.enterpriseid ";
				strQry = strQry+ " and b.roleid		   = a.roleid ";	
			}
			dbCon = DbConnectionManager.GetInstance().GetDbConnection(appID,enterpriseID);
			dbCmd = dbCon.CreateCommand(strQry,CommandType.Text);
			roleDataSet =(DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);			
			cnt = roleDataSet.Tables[0].Rows.Count;
			
			for(i=0;i<cnt;i++)
			{
				DataRow drEach = roleDataSet.Tables[0].Rows[i];
				
				if (ownerType == com.common.RBAC.OwnerType.Role)
				{
					int iRoleID = (int)drEach["roleid"];
					String strRoleName = (String)drEach["role_name"];
					role = new Role();
					role.RoleName = strRoleName;
					role.RoleID = iRoleID;
					ownerList.Add(role);				
				}
				else if (ownerType == com.common.RBAC.OwnerType.User)
				{
					String strUserID = (String)drEach["userid"];
					String strUserName = (String)drEach["user_name"];
					String strUserCulture = (String)drEach["user_culture"];
					user = new User();
					user.UserID = strUserID;
					user.UserName = strUserName;
					user.UserCulture = strUserCulture;
					ownerList.Add(user);			
				}
			}

			return ownerList;
		}


		/// <summary>
		/// Checks whether specified user is given specified permission.
		/// </summary>
		/// <param name="appID">ApplicationID for the current application</param>
		/// <param name="enterpriseID">EnterpriseID for the current enterprise</param>
		/// <param name="permission">Permission object </param>
		/// <param name="user">User object</param>
		/// <returns>True if permitted. False if not permitted</returns>
		public static bool IsPermitted(String appID, String enterpriseID,Permission permission,User user)
		{
			bool isPermitted=false;
			DbConnection dbCon = null;
			String strQry = null;
			IDbCommand dbCmd = null;
			DataSet permDataSet = null;

			dbCon = DbConnectionManager.GetInstance().GetDbConnection(appID,enterpriseID);
			strQry = "select * from permission_user_relation where applicationid = '"+appID+"'";
			strQry = strQry+" and enterpriseid='"+enterpriseID+"'"+ " and userid ='"+user.UserID+"'"+" and permissionid ='"+permission.PermissionID+"'";
			dbCmd = dbCon.CreateCommand(strQry,CommandType.Text);
			permDataSet = (DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
			if (permDataSet.Tables[0].Rows.Count >0)
				isPermitted = true;
			else
				isPermitted = false;

			return isPermitted;
		}



		/// <summary>
		/// This method is used in UserMgmt Module. It addes specified user in the user_master table and relates it to the list
		/// of roles in user_role_relation table. Both of these operation are handled in a single transaction.
		/// </summary>
		/// <param name="appID">ApplicationID for the current application</param>
		/// <param name="enterpriseID">EnterpriseID for the current enterprise</param>
		/// <param name="user">User object</param>
		/// <param name="roleList">Roles tobe related to the specified user</param>
		/// <returns>Returns number of rows affected by the last query inside the method.</returns>
		public static int CreateUserNRoles(String strAppID,String strEnterpriseID,User user,ArrayList roleList)
		{
			int iRows = 0;
			int iRoleID = 0;

			DbConnection dbConApp = null;
			IDbCommand dbCommandApp = null;

			IDbTransaction transactionApp = null;
			IDbConnection conApp = null;

			//Get App dbConnection
			dbConApp = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbConApp == null)
			{
				Logger.LogTraceError("RBAC","CreateUserNRoles","ERR0001","dbConApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			//Begin App Transaction

			conApp = dbConApp.GetConnection();
			
			if(conApp == null)
			{
				Logger.LogTraceError("RBAC","CreateUserNRoles","ERR0002","conApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			try
			{
				dbConApp.OpenConnection(ref conApp);
			}
			catch(ApplicationException appException)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("RBAC","CreateUserNRoles","ERR0003","Error opening connection : "+ appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ",appException);
			}
			catch(Exception exception)
			{
				if(conApp != null)
				{
					conApp.Close();
				}

				Logger.LogTraceError("RBAC","CreateUserNRoles","ERR0004","Error opening connection : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}


			transactionApp = dbConApp.BeginTransaction(conApp,IsolationLevel.ReadCommitted);

			if(transactionApp == null)
			{
				Logger.LogTraceError("RBAC","CreateUserNRoles","ERR0005","transactionApp is null");
				throw new ApplicationException("Failed Begining a App Transaction..",null);
			}


			try
			{
				//Insert record in user_master table.

				String strUserID = user.UserID;
				String strUserName = user.UserName;
				String strUserPswd = user.UserPswd;
				String strUserCulture = user.UserCulture;
				String strUsrDept = user.UserDepartment;
				String strUsrEmail = user.UserEmail;
				String strUserType = user.UserType;
				String strUserLocation = user.UserLocation;
				String strCustomerAcc = user.CustomerAccount;
				String strQry = null;

				//dbCon = DbConnectionManager.GetInstance().GetDbConnection(appID,enterpriseID);
				strQry = "insert into User_Master(enterpriseid,userid,applicationid,user_name,user_password,user_culture,email,department_name,user_type, location, payerid) ";
				strQry = strQry + " values('"+strEnterpriseID+"','"+strUserID+"','"+strAppID+"','";
				strQry = strQry + strUserName+"','"+strUserPswd+"','"+strUserCulture+"','"+strUsrEmail+"','"+strUsrDept+"','"+strUserType+"','"+strUserLocation+"','"+strCustomerAcc+"')";
				
				dbCommandApp = dbConApp.CreateCommand(strQry,CommandType.Text);
				dbCommandApp.Connection = conApp;
				dbCommandApp.Transaction = transactionApp;

				iRows = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);
				Logger.LogTraceInfo("RBAC","CreateUserNRoles","ERR0012",iRows + " rows inserted into User_Master table");

				//insert into user_role_relation
				foreach(Role role in roleList)
				{
					iRoleID = (int)role.RoleID;

					strQry = "insert into user_role_relation(roleid,userid,enterpriseid,applicationid)values(";
					strQry = strQry +role.RoleID+",'"+user.UserID+"','"+strEnterpriseID+"','"+strAppID+"')";
					dbCommandApp.CommandText = strQry;

					iRows = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);
					Logger.LogTraceInfo("RBAC","CreateUserNRoles","ERR0013",iRows + " rows inserted into user_role_relation table");
					
				}

				//commit the transaction
				transactionApp.Commit();
				Logger.LogTraceInfo("RBAC","CreateUserNRoles","ERR0014","App db Transaction committed.");

			}
			catch(ApplicationException appException)
			{
				
				try
				{
					transactionApp.Rollback();
					Logger.LogTraceInfo("RBAC","CreateUserNRoles","ERR0006","App db delete transaction rolled back.");

				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("RBAC","CreateUserNRoles","ERR0007","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}


				Logger.LogTraceError("RBAC","CreateUserNRoles","ERR0008","Transaction Failed : "+ appException.Message.ToString());
				throw new ApplicationException("Error  "+appException.Message,appException);
			}
			catch(Exception exception)
			{
				try
				{
					transactionApp.Rollback();
					Logger.LogTraceInfo("RBAC","CreateUserNRoles","ERR0009","App db delete transaction rolled back.");

				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("RBAC","CreateUserNRoles","ERR0010","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}

				Logger.LogTraceError("RBAC","CreateUserNRoles","ERR0011","Transaction Failed : "+ exception.Message.ToString());
				throw new ApplicationException("Error "+exception.Message,exception);
			}
			finally
			{
				if(conApp != null)
				{
					conApp.Close();
				}
			}
			
			return iRows;
		}


		/// <summary>
		/// This method is used in the UserMgmt module. It does following operation in a single transaction. <br>
		/// delete record(s) from module_tree_user_relation for the specified appid, enterpriseid and userid <br>
		/// delete record(s) from user_role_relation for the specified appid, enterpriseid and userid <br>
		/// update record in the user_master table for the specified appid, enterpriseid and userid <br>
		/// insert records in the user_role_relation table for the specified appid, enterpriseid, userid  <br>
		/// </summary>
		/// <param name="strAppID">ApplicationID for the current application</param>
		/// <param name="strEnterpriseID">EnterpriseID for the current enterprise</param>
		/// <param name="strUserID"></param>
		/// <param name="user"></param>
		/// <param name="roleList"></param>
		/// <returns></returns>
		public static int ModifyUserNRoles(String strAppID,String strEnterpriseID,String strUserID,User user,ArrayList roleList)
		{
			int iRows = 0;
			int iRoleID = 0;

			DbConnection dbConApp = null;
			IDbCommand dbCommandApp = null;

			IDbTransaction transactionApp = null;
			IDbConnection conApp = null;

			//Get App dbConnection
			dbConApp = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbConApp == null)
			{
				Logger.LogTraceError("RBAC","ModifyUserNRoles","ERR0001","dbConApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			//Begin App Transaction

			conApp = dbConApp.GetConnection();
			
			if(conApp == null)
			{
				Logger.LogTraceError("RBAC","ModifyUserNRoles","ERR0002","conApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			try
			{
				dbConApp.OpenConnection(ref conApp);
			}
			catch(ApplicationException appException)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("RBAC","ModifyUserNRoles","ERR0003","Error opening connection : "+ appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ",appException);
			}
			catch(Exception exception)
			{
				if(conApp != null)
				{
					conApp.Close();
				}

				Logger.LogTraceError("RBAC","ModifyUserNRoles","ERR0004","Error opening connection : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}


			transactionApp = dbConApp.BeginTransaction(conApp,IsolationLevel.ReadCommitted);

			if(transactionApp == null)
			{
				Logger.LogTraceError("RBAC","ModifyUserNRoles","ERR0005","transactionApp is null");
				throw new ApplicationException("Failed Begining a App Transaction..",null);
			}


			try
			{
				//delete from table module_tree_user_relation
				String strQry = null;
				strQry = "delete from module_tree_user_relation where  enterpriseid = '"+strEnterpriseID+"'"+" and applicationid = '"+strAppID+"'";
				strQry = strQry+" and userid = '"+strUserID+"'";

				dbCommandApp = dbConApp.CreateCommand(strQry,CommandType.Text);
				dbCommandApp.Connection = conApp;
				dbCommandApp.Transaction = transactionApp;
//				dbCommandApp.CommandTimeout = 10000;

				iRows = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);
				Logger.LogTraceInfo("RBAC","ModifyUserNRoles","ERR0012",iRows + " rows deleted from module_tree_user_relation table");

				//delete from table user_role_relation
				strQry = null;
				strQry = "delete from user_role_relation where applicationid = '"+strAppID+"'";
				strQry = strQry + " and enterpriseid = '"+strEnterpriseID+"' and userid = '"+user.UserID+"'";
				
				dbCommandApp.CommandText = strQry;				
				iRows = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);
				System.Threading.Thread.Sleep(100);
				Logger.LogTraceInfo("RBAC","ModifyUserNRoles","ERR0013",iRows + " rows deleted from user_role_relation table");

				//Update the user_master table
				//String strUserID = user.UserID;
				String strUserName = user.UserName;
				String strUserPswd = user.UserPswd;
				String strUsrDept = user.UserDepartment;
				String strUsrEmail = user.UserEmail;
				String strUserType = user.UserType;
				String strUserCulture = user.UserCulture;
				String strUserLocation = user.UserLocation;    //by sittichai
				String strCustomerAccount = user.CustomerAccount;  //by sittichai
				strQry = null;

				strQry = "update User_Master ";
				strQry = strQry+ " set user_name = '"+strUserName+"'"+",";

				if ((strUserPswd != null) && (strUserPswd != ""))
				{
					strQry = strQry+" user_password= '"+strUserPswd +"',";
				}

				strQry = strQry+" email ='"+strUsrEmail+"',";
				strQry = strQry+" department_name ='"+strUsrDept+"',";
				strQry = strQry+" user_type ='"+strUserType+"',";
				strQry = strQry+" user_culture ='"+strUserCulture+"',";
				strQry = strQry+" location ='"+strUserLocation+"',";  //by sittichai
				strQry = strQry+" payerid ='"+strCustomerAccount+"'";   //by sittichai
				strQry = strQry + " where enterpriseid = '"+strEnterpriseID+"'"+" and applicationid = '"+strAppID+"'"+" and userid = '"+strUserID+"'";

				dbCommandApp.CommandText = strQry;
				iRows = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);
				Logger.LogTraceInfo("RBAC","ModifyUserNRoles","MSG0014",iRows + " rows updated in user_master table");

				//insert into user_role_relation
				foreach(Role role in roleList)
				{
					iRoleID = (int)role.RoleID;

					strQry = "insert into user_role_relation(roleid,userid,enterpriseid,applicationid)values(";
					strQry = strQry +role.RoleID+",'"+user.UserID+"','"+strEnterpriseID+"','"+strAppID+"')";
					
					dbCommandApp.CommandText = strQry;
					iRows = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);
					Logger.LogTraceInfo("RBAC","ModifyUserNRoles","MSG0015",iRows + " rows inserted into user_role_relation table");
					
				}

				//commit the transaction
				transactionApp.Commit();
				Logger.LogTraceInfo("RBAC","ModifyUserNRoles","MSG0016","App db Transaction committed.");
			}
			catch(ApplicationException appException)
			{
				
				try
				{
					transactionApp.Rollback();
					Logger.LogTraceInfo("RBAC","CreateUserNRoles","ERR0006","App db delete transaction rolled back.");

				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("RBAC","CreateUserNRoles","ERR0007","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}


				Logger.LogTraceError("RBAC","CreateUserNRoles","ERR0008","Transaction Failed : "+ appException.Message.ToString());
				throw new ApplicationException("Error  "+appException.Message,appException);
			}
			catch(Exception exception)
			{
				try
				{
					transactionApp.Rollback();
					Logger.LogTraceInfo("RBAC","CreateUserNRoles","ERR0009","App db delete transaction rolled back.");

				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("RBAC","CreateUserNRoles","ERR0010","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}

				Logger.LogTraceError("RBAC","CreateUserNRoles","ERR0011","Transaction Failed : "+ exception.Message.ToString());
				throw new ApplicationException("Error "+exception.Message,exception);
			}
			finally
			{
				if(conApp != null)
				{
					conApp.Close();
				}
			}
			
			return iRows;
		}


		/// <summary>
		/// This method is used in the AdminRoleInfo module. It does following operation in a single transaction. <br>
		/// delete record(s) from module_tree_role_relation for the specified appid, enterpriseid and roleid <br>
		/// delete record(s) from Role_Master for the specified appid, enterpriseid and roleid <br>
		/// </summary>
		/// <param name="strAppID">ApplicationID for the current application</param>
		/// <param name="strEnterpriseID">EnterpriseID for the current enterprise</param>
		/// <param name="lRoleID"></param>
		/// <returns>Returns records affected by the last non-query.</returns>
		public static int DeleteModuleNRole(String strAppID,String strEnterpriseID,long lRoleID)
		{
			int iRows = 0;

			DbConnection dbConApp = null;
			IDbCommand dbCommandApp = null;

			IDbTransaction transactionApp = null;
			IDbConnection conApp = null;
			
			//Get App dbConnection
			dbConApp = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbConApp == null)
			{
				Logger.LogTraceError("RBAC","DeleteModuleNRole","ERR0001","dbConApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			//Begin App Transaction

			conApp = dbConApp.GetConnection();
			
			if(conApp == null)
			{
				Logger.LogTraceError("RBAC","DeleteModuleNRole","ERR0002","conApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			try
			{
				dbConApp.OpenConnection(ref conApp);
			}
			catch(ApplicationException appException)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("RBAC","DeleteModuleNRole","ERR0003","Error opening connection : "+ appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ",appException);
			}
			catch(Exception exception)
			{
				if(conApp != null)
				{
					conApp.Close();
				}

				Logger.LogTraceError("RBAC","DeleteModuleNRole","ERR0004","Error opening connection : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}


			transactionApp = dbConApp.BeginTransaction(conApp,IsolationLevel.ReadCommitted);

			if(transactionApp == null)
			{
				Logger.LogTraceError("RBAC","DeleteModuleNRole","ERR0005","transactionApp is null");
				throw new ApplicationException("Failed Begining a App Transaction..",null);
			}


			try
			{
				
				String strQry = null;
				//delete from module_tree_role_relation
				strQry = "delete from module_tree_role_relation where  enterpriseid = '"+strEnterpriseID+"'"+" and applicationid = '"+strAppID+"'"+"and roleid = "+lRoleID;
				dbCommandApp = dbConApp.CreateCommand(strQry,CommandType.Text);
				dbCommandApp.Connection = conApp;
				dbCommandApp.Transaction = transactionApp;
				iRows = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);
				Logger.LogTraceInfo("RBAC","DeleteModuleNRole","ERR0012",iRows + " rows deleted from module_tree_role_relation table");
				
				strQry = null;
				
				//delete from role_master
				strQry = "delete from Role_Master where  enterpriseid = '"+strEnterpriseID+"'"+" and applicationid = '"+strAppID+"'"+" and roleid = "+lRoleID;
				dbCommandApp.CommandText = strQry;
				iRows = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);
				Logger.LogTraceInfo("RBAC","DeleteModuleNRole","ERR0013",iRows + " rows deleted from role_master table");

				//commit the transaction
				transactionApp.Commit();
				Logger.LogTraceInfo("RBAC","DeleteModuleNRole","ERR0014","App db Transaction committed.");
					
			}
			catch(ApplicationException appException)
			{
				
				try
				{
					transactionApp.Rollback();
					Logger.LogTraceInfo("RBAC","DeleteModuleNRole","ERR0006","App db delete transaction rolled back.");

				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("RBAC","DeleteModuleNRole","ERR0007","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}


				Logger.LogTraceError("RBAC","DeleteModuleNRole","ERR0008","Transaction Failed : "+ appException.Message.ToString());
				throw new ApplicationException("Error  "+appException.Message,appException);
			}
			catch(Exception exception)
			{
				try
				{
					transactionApp.Rollback();
					Logger.LogTraceInfo("RBAC","DeleteModuleNRole","ERR0009","App db delete transaction rolled back.");

				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("RBAC","DeleteModuleNRole","ERR0010","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}

				Logger.LogTraceError("RBAC","DeleteModuleNRole","ERR0011","Transaction Failed : "+ exception.Message.ToString());
				throw new ApplicationException("Error "+exception.Message,exception);
			}
			finally
			{
				if(conApp != null)
				{
					conApp.Close();
				}
			}
			
			return iRows;
		}


		/// <summary>
		/// This method prepares query for following db operation and addes in to the array list passed by reference. <br>
		/// Adding new role in to the role_master table<br>
		/// Adding modules for that role in to module_tree_role_relation table
		/// 
		/// </summary>
		/// <param name="strAppID">ApplicationID for the current application</param>
		/// <param name="strEnterpriseID">EnterpriseID for the current enterprise</param>
		/// <param name="role"></param>
		/// <param name="strParentModuleID"></param>
		/// <param name="strModule"></param>
		/// <param name="strModRights"></param>
		/// <param name="QueryList"></param>
		/// <returns>Returns the role id.. </returns>
		public static decimal GetQryAddModuleNRole(String strAppID,String strEnterpriseID,Role role,String strParentModuleID,String strModule,String strModRights,ref ArrayList QueryList)
		{
			decimal lRoleID = 0;
			
			DbConnection dbCore = null;
			IDbCommand dbCom = null;

			String strQry = null;
			
			//Get the roleid and Query for insert into Role_Master
			String strRoleName = role.RoleName;
			lRoleID = Counter.GetNext(strAppID,strEnterpriseID,"roleid");
			role.RoleID = lRoleID;
			
			strQry = "insert into Role_Master(applicationid,enterpriseid,roleid,role_name,role_description) values('"+strAppID+"','"+strEnterpriseID+"'"+","+lRoleID+",'"+strRoleName+"','"+role.RoleDescription+"'"+")";
			if(QueryList != null)
				QueryList.Add(strQry);
			
			
			strQry = null;
			
			//query for insert into module_tree_user_relation table.
			dbCore = DbConnectionManager.GetInstance().GetDbConnection(strAppID);
			if(dbCore == null)
			{
				lRoleID = 0;
				Logger.LogTraceError("RBAC","GetQryAddModuleNRole","ERR0001","dbConApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			DataSet moduleDataSet = null;

			strQry = "select * from core_module_tree_master where applicationid = '"+strAppID+"'";
			strQry = strQry + " and parent_id = '"+strParentModuleID+"'";
			strQry = strQry + " and child_id = '"+strModule+"'";
			
			dbCom = dbCore.CreateCommand(strQry,CommandType.Text);
			moduleDataSet = (DataSet)dbCore.ExecuteQuery(dbCom,com.common.DAL.ReturnType.DataSetType);

			int cnt = moduleDataSet.Tables[0].Rows.Count;
		
			if(cnt >0)
			{
				DataRow drEach = moduleDataSet.Tables[0].Rows[0];
				String strChildID = (String)drEach["child_id"];
				String strChildName = (String)drEach["child_display_name"];
				String strParentID = (String)drEach["Parent_id"];
				String strChildIconImage = "";
				if(!drEach["child_icon_image"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					strChildIconImage = (String)drEach["child_icon_image"];
				}
				decimal iDisplayStyle = 0;
				if(!drEach["display_style"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					if (moduleDataSet.Tables[0].Columns["display_style"].DataType == System.Type.GetType("System.Decimal"))
						iDisplayStyle = (decimal)drEach["display_style"];
					else
						iDisplayStyle = (byte) drEach["display_style"];
				}
				String strChildURL = "";
				if(!drEach["child_url"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					strChildURL = (String)drEach["child_url"];
				}
				strQry = null;
				strQry = " insert into module_tree_role_relation(roleid,enterpriseid,parentid,child_id,";
				strQry = strQry + " applicationid,child_name,child_icon_image,display_style,child_url,module_rights)values (";
				strQry = strQry + role.RoleID+",'"+strEnterpriseID+"','"+strParentID+"','"+strChildID+"',";
				strQry = strQry + "'"+strAppID+"','"+strChildName+"','"+strChildIconImage+"','"+iDisplayStyle+"',";
				strQry = strQry + "'"+strChildURL+"','"+strModRights+"')";
				if(QueryList != null)
					QueryList.Add(strQry);	
			}
			return lRoleID;
		}



		/// <summary>
		/// This method prepares query for following db operation and addes in to the array list passed by reference. <br>
		/// Adding modules for the specified role, parent moduleid and child module id in to module_tree_role_relation table
		/// </summary>
		/// <param name="appID">ApplicationID for the current application</param>
		/// <param name="enterpriseID">EnterpriseID for the current enterprise</param>
		/// <param name="role"></param>
		/// <param name="parentModuleID"></param>
		/// <param name="module"></param>
		/// <param name="modRights"></param>
		/// <param name="QueryList"></param>
		public static void GetModulesRoleInsertQry(String appID, String enterpriseID,Role role,String parentModuleID,String module,String modRights,ref ArrayList QueryList)
		{
			DbConnection dbCon,dbCore = null;
			String strQry = null;
			DataSet moduleDataSet = null;
			IDbCommand dbCom = null;
			int cnt = 0; int i = 0;

			dbCon = DbConnectionManager.GetInstance().GetDbConnection(appID,enterpriseID);
			dbCore = DbConnectionManager.GetInstance().GetDbConnection(appID);
			strQry = "select * from core_module_tree_master where applicationid = '"+appID+"'";
			strQry = strQry + " and parent_id = '"+parentModuleID+"'";
			strQry = strQry + " and child_id = '"+module+"'";
			dbCom = dbCore.CreateCommand(strQry,CommandType.Text);
			moduleDataSet = (DataSet)dbCore.ExecuteQuery(dbCom,com.common.DAL.ReturnType.DataSetType);

			cnt = moduleDataSet.Tables[0].Rows.Count;
			
			if (cnt>0)
			{
				DataRow drEach = moduleDataSet.Tables[0].Rows[i];
				String strChildID = (String)drEach["child_id"];
				String strChildName = (String)drEach["child_display_name"];
				String strParentID = (String)drEach["Parent_id"];
				String strChildIconImage = "";
				if(!drEach["child_icon_image"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					strChildIconImage = (String)drEach["child_icon_image"];
				}
				decimal iDisplayStyle = 0;
				if(!drEach["display_style"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					if (moduleDataSet.Tables[0].Columns["display_style"].DataType == System.Type.GetType("System.Decimal"))
						iDisplayStyle = (decimal)drEach["display_style"];
					else
						iDisplayStyle = (byte) drEach["display_style"];
				}
				String strChildURL = "";
				if(!drEach["child_url"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					strChildURL = (String)drEach["child_url"];
				}
				strQry = " insert into module_tree_role_relation(roleid,enterpriseid,parentid,child_id,";
				strQry = strQry + " applicationid,child_name,child_icon_image,display_style,child_url,module_rights)values (";
				strQry = strQry + role.RoleID+",'"+enterpriseID+"','"+strParentID+"','"+strChildID+"',";
				strQry = strQry + "'"+appID+"','"+strChildName+"','"+strChildIconImage+"','"+iDisplayStyle+"',";
				strQry = strQry + "'"+strChildURL+"','"+modRights+"')";
				
				if(QueryList != null)
				{
					QueryList.Add(strQry);
				}
			}				
		}


		/// <summary>
		/// This method prepares query for following db operation and addes in to the array list passed by reference. <br>
		/// delete record(s) from module_tree_role_relation for the specified applicationid, enterpriseid and roleid<br>
		/// update record in the Role_Master for the specified applicationid, enterpriseid and roleid<br>
		/// insert record(s) in to module_tree_role_relation
		/// </summary>
		/// <param name="strAppID">ApplicationID for the current application</param>
		/// <param name="strEnterpriseID">EnterpriseID for the current enterprise</param>
		/// <param name="lRoleID"></param>
		/// <param name="role"></param>
		/// <param name="strParentModuleID"></param>
		/// <param name="strModule"></param>
		/// <param name="strModRights"></param>
		/// <param name="QueryList"></param>
		public static void GetQryModifyModuleNRole(String strAppID,String strEnterpriseID,decimal lRoleID,Role role,String strParentModuleID,String strModule,String strModRights,ref ArrayList QueryList)
		{

			DbConnection dbCore = null;
			IDbCommand dbCom = null;
	
			String strQry = null;
			//Get query for delete from module_tree_role_relation table.
			strQry = "delete from module_tree_role_relation where  enterpriseid = '"+strEnterpriseID+"'"+" and applicationid = '"+strAppID+"'"+"and roleid = "+lRoleID;
			if(QueryList != null)
				QueryList.Add(strQry);

			//Get query update role_master table
			decimal iRoleID = role.RoleID;
			String strRoleName = role.RoleName;
			strQry = null;
			strQry = "update Role_Master set Role_name = '"+strRoleName+"',"+"role_description = '"+role.RoleDescription+"'"+" where enterpriseid = '"+strEnterpriseID+"'"+" and applicationid = '"+strAppID+"'" + " and roleid ="+lRoleID;
			if(QueryList != null)
				QueryList.Add(strQry);


			//Get query insert into module_tree_user_relation table.
			dbCore = DbConnectionManager.GetInstance().GetDbConnection(strAppID);
			if(dbCore == null)
			{
				Logger.LogTraceError("RBAC","AddModuleNRole","ERR00013","dbConApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			DataSet moduleDataSet = null;
			strQry = "select * from core_module_tree_master where applicationid = '"+strAppID+"'";
			strQry = strQry + " and parent_id = '"+strParentModuleID+"'";
			strQry = strQry + " and child_id = '"+strModule+"'";
			
			dbCom = dbCore.CreateCommand(strQry,CommandType.Text);
			moduleDataSet = (DataSet)dbCore.ExecuteQuery(dbCom,com.common.DAL.ReturnType.DataSetType);

			int cnt = moduleDataSet.Tables[0].Rows.Count;
		
			if(cnt >0)
			{
				DataRow drEach = moduleDataSet.Tables[0].Rows[0];
				String strChildID = (String)drEach["child_id"];
				String strChildName = (String)drEach["child_display_name"];
				String strParentID = (String)drEach["Parent_id"];
				String strChildIconImage = "";
				if(!drEach["child_icon_image"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					strChildIconImage = (String)drEach["child_icon_image"];
				}
				decimal iDisplayStyle = 0;
				if(!drEach["display_style"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					if (moduleDataSet.Tables[0].Columns["display_style"].DataType == System.Type.GetType("System.Decimal"))
						iDisplayStyle = (decimal)drEach["display_style"];
					else
						iDisplayStyle = (byte) drEach["display_style"];
				}
				String strChildURL = "";
				if(!drEach["child_url"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					strChildURL = (String)drEach["child_url"];
				}
				strQry = null;
				strQry = " insert into module_tree_role_relation(roleid,enterpriseid,parentid,child_id,";
				strQry = strQry + " applicationid,child_name,child_icon_image,display_style,child_url,module_rights)values (";
				strQry = strQry + role.RoleID+",'"+strEnterpriseID+"','"+strParentID+"','"+strChildID+"',";
				strQry = strQry + "'"+strAppID+"','"+strChildName+"','"+strChildIconImage+"','"+iDisplayStyle+"',";
				strQry = strQry + "'"+strChildURL+"','"+strModRights+"')";
	
				if(QueryList != null)
					QueryList.Add(strQry);
			}
		}

		/// <summary>
		/// This method prepares query for following db operations and addes in to the array list passed by reference. <br>
		/// insert record in to user_master table for the specified applicationid, enterpriseid, user details.
		/// insert record(s) in to module_tree_user_relation
		/// </summary>
		/// <param name="strAppID">ApplicationID for the current application</param>
		/// <param name="strEnterpriseID">EnterpriseID for the current enterprise</param>
		/// <param name="user"></param>
		/// <param name="strParentModuleID"></param>
		/// <param name="strModule"></param>
		/// <param name="strModRights"></param>
		/// <param name="QueryList"></param>
		public static void GetQryAddUserNModules(String strAppID,String strEnterpriseID,User user,String strParentModuleID,String strModule,String strModRights,ref ArrayList QueryList)
		{
			IDbCommand dbCom = null;
			DbConnection dbCore = null;
			
			//Get query for Insert record in user_master table.

			String strUserID = user.UserID;
			String strUserName = user.UserName;
			String strUserPswd = user.UserPswd;
			String strUserCulture = user.UserCulture;
			String strUsrDept = user.UserDepartment;
			String strUsrEmail = user.UserEmail;
			String strUserType = user.UserType;
			String strQry = null;

			strQry = "insert into User_Master(enterpriseid,userid,applicationid,user_name,user_password,user_culture,email,department_name,user_type) ";
			strQry = strQry + " values('"+strEnterpriseID+"','"+strUserID+"','"+strAppID+"','";
			strQry = strQry + strUserName+"','"+strUserPswd+"','"+strUserCulture+"','"+strUsrEmail+"','"+strUsrDept+"','"+strUserType+"')";
			if(QueryList != null)
				QueryList.Add(strQry);


			//Get query for insert into module_tree_user_relation table.
			DataSet moduleDataSet = null;
			dbCore = DbConnectionManager.GetInstance().GetDbConnection(strAppID);
			if(dbCore == null)
			{
				Logger.LogTraceError("RBAC","GetQryAddUserNModules","ERR0001","dbConApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			strQry = null;
			strQry = "select * from core_module_tree_master where applicationid = '"+strAppID+"'";
			strQry = strQry + " and parent_id = '"+strParentModuleID+"'";
			strQry = strQry + " and child_id = '"+strModule+"'";

			dbCom = dbCore.CreateCommand(strQry,CommandType.Text);
			moduleDataSet = (DataSet)dbCore.ExecuteQuery(dbCom,com.common.DAL.ReturnType.DataSetType);

			int cnt = moduleDataSet.Tables[0].Rows.Count;
		
			if(cnt > 0)
			{
				DataRow drEach = moduleDataSet.Tables[0].Rows[0];
				String strChildID = (String)drEach["child_id"];
				String strChildName = (String)drEach["child_display_name"];
				String strParentID = (String)drEach["Parent_id"];
				String strChildIconImage = "";
				if(!drEach["child_icon_image"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					strChildIconImage = (String)drEach["child_icon_image"];
				}
				decimal iDisplayStyle = 0;
				if(!drEach["display_style"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					if (moduleDataSet.Tables[0].Columns["display_style"].DataType == System.Type.GetType("System.Decimal"))
						iDisplayStyle = (decimal)drEach["display_style"];
					else
						iDisplayStyle = (byte) drEach["display_style"];
				}
				String strChildURL = "";
				if(!drEach["child_url"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					strChildURL = (String)drEach["child_url"];
				}
				strQry = null;
				strQry = " insert into module_tree_user_relation(userid,enterpriseid,parent_id,child_id,";
				strQry = strQry + " applicationid,child_name,child_icon_image,display_style,child_url,module_rights)values ('";
				strQry = strQry + user.UserID+"','"+strEnterpriseID+"','"+strParentID+"','"+strChildID+"',";
				strQry = strQry + "'"+strAppID+"','"+strChildName+"','"+strChildIconImage+"','"+iDisplayStyle+"',";
				if(strChildURL == "defaultWelcome.aspx")
				{
					strQry = strQry + "'"+strChildURL+"','IDU')";
				}
				else
				{
					strQry = strQry + "'"+strChildURL+"','"+strModRights+"')";
				}
				

				if(QueryList != null)
					QueryList.Add(strQry);
			}
		}



		/// <summary>
		/// This method prepares query for following db operations and addes in to the array list passed by reference. <br>
		/// delete record(s) from user_role_relation table for specified applicationid, enterpriseid and userid .<br>
		/// delte record(s) from module_tree_user_relation table for the applicationid, enterpriseid and userid <br>
		/// update record in to user_master table for the specified applicationid, enterpriseid, user details.
		/// insert record(s) in to module_tree_user_relation for that user
		/// </summary>
		/// <param name="strAppID">ApplicationID for the current application</param>
		/// <param name="strEnterpriseID">EnterpriseID for the current enterprise</param>
		/// <param name="user"></param>
		/// <param name="strUserID"></param>
		/// <param name="strParentModuleID"></param>
		/// <param name="strModule"></param>
		/// <param name="strModRights"></param>
		/// <param name="QueryList"></param>
		public static void GetQryModifyUserNModules(String strAppID,String strEnterpriseID,User user,String strUserID,String strParentModuleID,String strModule,String strModRights,ref ArrayList QueryList)
		{
			DbConnection dbCore = null;
			IDbCommand dbCom = null;
				
			//Get query for delete from user_role_relation
			String strQry = null;
			strQry = "delete from user_role_relation where enterpriseid = '"+strEnterpriseID+"'"+" and applicationid = '"+strAppID+"'";
			strQry = strQry+" and userid = '"+strUserID+"'";
			if(QueryList != null)
				QueryList.Add(strQry);
			
			//Get query for delete records from module_tree_user_relation table.
			strQry = null;
			strQry = "delete from module_tree_user_relation where  enterpriseid = '"+strEnterpriseID+"'"+" and applicationid = '"+strAppID+"'";
			strQry = strQry+" and userid = '"+strUserID+"'";
			if(QueryList != null)
				QueryList.Add(strQry);

			//Get query for Update the user_master table
			String strUserName = user.UserName;
			String strUserPswd = user.UserPswd;
			String strUsrDept = user.UserDepartment;
			String strUsrEmail = user.UserEmail;
			String strUserType = user.UserType;
			
			strQry = null;
			strQry = "update User_Master ";
			strQry = strQry+ " set user_name = '"+strUserName+"'"+",";
			if ((strUserPswd != null) && (strUserPswd != ""))
			{
				strQry = strQry+" user_password= '"+strUserPswd +"',";
			}

			strQry = strQry+" email ='"+Utility.ReplaceSingleQuote(strUsrEmail)+"',";
			strQry = strQry+" department_name ='"+strUsrDept+"',";
			strQry = strQry+" user_type ='"+strUserType+"'";
			strQry = strQry + " where enterpriseid = '"+strEnterpriseID+"'"+" and applicationid = '"+strAppID+"'"+" and userid = '"+strUserID+"'";

			if(QueryList != null)
				QueryList.Add(strQry);

			//Get query for insert into module_tree_user_relation table.
			dbCore = DbConnectionManager.GetInstance().GetDbConnection(strAppID);
			if(dbCore == null)
			{
				Logger.LogTraceError("RBAC","AddUserNModules","ERR00017","dbConApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			DataSet moduleDataSet = null;
			strQry = "select * from core_module_tree_master where applicationid = '"+strAppID+"'";
			strQry = strQry + " and parent_id = '"+strParentModuleID+"'";
			strQry = strQry + " and child_id = '"+strModule+"'";
			
			dbCom = dbCore.CreateCommand(strQry,CommandType.Text);
			moduleDataSet = (DataSet)dbCore.ExecuteQuery(dbCom,com.common.DAL.ReturnType.DataSetType);

			int cnt = moduleDataSet.Tables[0].Rows.Count;
		
			if(cnt >0)
			{
				DataRow drEach = moduleDataSet.Tables[0].Rows[0];
				String strChildID = (String)drEach["child_id"];
				String strChildName = (String)drEach["child_display_name"];
				String strParentID = (String)drEach["Parent_id"];
				String strChildIconImage = "";
				if(!drEach["child_icon_image"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					strChildIconImage = (String)drEach["child_icon_image"];
				}
				decimal iDisplayStyle = 0;
				if(!drEach["display_style"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					if (moduleDataSet.Tables[0].Columns["display_style"].DataType == System.Type.GetType("System.Decimal"))
						iDisplayStyle = (decimal)drEach["display_style"];
					else
						iDisplayStyle = (byte) drEach["display_style"];
				}
				String strChildURL = "";
				if(!drEach["child_url"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					strChildURL = (String)drEach["child_url"];
				}
				strQry = null;
				strQry = " insert into module_tree_user_relation(userid,enterpriseid,parent_id,child_id,";
				strQry = strQry + " applicationid,child_name,child_icon_image,display_style,child_url,module_rights)values ('";
				strQry = strQry + user.UserID+"','"+strEnterpriseID+"','"+strParentID+"','"+strChildID+"',";
				strQry = strQry + "'"+strAppID+"','"+strChildName+"','"+strChildIconImage+"','"+iDisplayStyle+"',";
				if(strChildURL =="defaultWelcome.aspx")
				{
					strQry = strQry + "'"+strChildURL+"','IDU')";
				}
				else
				{
					strQry = strQry + "'"+strChildURL+"','"+strModRights+"')";
				}
				if(QueryList != null)
					QueryList.Add(strQry);
			}	
		}


		/// <summary>
		/// This method prepares query for following db operations and addes in to the array list passed by reference. <br>
		/// insert record(s) in to module_tree_user_relation for that user<br>
		/// </summary>
		/// <param name="appID"></param>
		/// <param name="enterpriseID"></param>
		/// <param name="user"></param>
		/// <param name="parentModuleID"></param>
		/// <param name="module"></param>
		/// <param name="modRights"></param>
		/// <param name="QueryList"></param>
		public static void GetModulesUserInsertQry(String appID, String enterpriseID,User user,String parentModuleID,String module,String modRights,ref ArrayList QueryList)
		{
			DbConnection dbCon,dbCore = null;
			String strQry = null;
			DataSet moduleDataSet = null;
			IDbCommand dbCom = null;
			int cnt = 0; int i = 0;

			dbCon = DbConnectionManager.GetInstance().GetDbConnection(appID,enterpriseID);
			dbCore = DbConnectionManager.GetInstance().GetDbConnection(appID);
			strQry = "select * from core_module_tree_master where applicationid = '"+appID+"'";
			strQry = strQry + " and parent_id = '"+parentModuleID+"'";
			strQry = strQry + " and child_id = '"+module+"'";
			dbCom = dbCore.CreateCommand(strQry,CommandType.Text);
			moduleDataSet = (DataSet)dbCore.ExecuteQuery(dbCom,com.common.DAL.ReturnType.DataSetType);

			cnt = moduleDataSet.Tables[0].Rows.Count;
			
			if (cnt>0)
			{
				DataRow drEach = moduleDataSet.Tables[0].Rows[i];
				String strChildID = (String)drEach["child_id"];
				String strChildName = (String)drEach["child_display_name"];
				String strParentID = (String)drEach["Parent_id"];
				String strChildIconImage = "";
				if(!drEach["child_icon_image"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					strChildIconImage = (String)drEach["child_icon_image"];
				}
				decimal iDisplayStyle = 0;
				if(!drEach["display_style"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					if (moduleDataSet.Tables[0].Columns["display_style"].DataType == System.Type.GetType("System.Decimal"))
						iDisplayStyle = (decimal)drEach["display_style"];
					else
						iDisplayStyle = (byte) drEach["display_style"];
				}
				String strChildURL = "";
				if(!drEach["child_url"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					strChildURL = (String)drEach["child_url"];
				}
				strQry = " insert into module_tree_user_relation(userid,enterpriseid,parent_id,child_id,";
				strQry = strQry + " applicationid,child_name,child_icon_image,display_style,child_url,module_rights)values ('";
				strQry = strQry + user.UserID+"','"+enterpriseID+"','"+strParentID+"','"+strChildID+"',";
				strQry = strQry + "'"+appID+"','"+strChildName+"','"+strChildIconImage+"','"+iDisplayStyle+"',";
				strQry = strQry + "'"+strChildURL+"','"+modRights+"')";
				
				if(QueryList != null)
				{
					QueryList.Add(strQry);
				}
			}				
		}



		/// <summary>
		/// This method is used to execute all non-queries in a batch in single transaction passed 
		/// as an arraylist of string objects.
		/// </summary>
		/// <param name="strAppID">ApplicationID for the current application</param>
		/// <param name="strEnterpriseID">EnterpriseID for the current enterprise</param>
		/// <param name="QueryList">ArrayList containing the non-query as string values.</param>
		/// <returns>returns the number of rows affected by the execution of last non-query in the list</returns>
		public static int SaveRecordsInBatch(String strAppID,String strEnterpriseID,ref ArrayList QueryList)
		{
			int iRows = 0;
			DbConnection dbConApp = null;
			
			//Get App dbConnection
			dbConApp = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbConApp == null)
			{
				Logger.LogTraceError("RBAC","SaveModuleNRole","ERR0001","dbConApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			try
			{
				if(QueryList != null)
				{
					iRows = dbConApp.ExecuteBatchSQLinTranscation(QueryList);	
				}
				
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("RBAC","SaveRecordInBatch","ERR0002","Transaction Failed during batch execution : "+ appException.Message.ToString());
				throw new ApplicationException("Error during batch execution "+appException.Message,appException);
			}
			
			return iRows;
		}
	}
}
