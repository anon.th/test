using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Web.Security;
using com.common.classes;
using com.common.util;
using com.common.RBAC;

namespace com.common.applicationpages
{
	/// <summary>
	/// Summary description for BasePage.
	/// </summary>
	public class BasePage : System.Web.UI.Page
	{
		protected Utility utility = null;
		protected AccessRights m_moduleAccessRights = null;
		protected string strScrollPosition;
		public BasePage()
		{
		}

		protected override void OnInit(EventArgs e)
		{
			base.OnInit(e);
			this.Load += new System.EventHandler(this.BasePage_Load);			
		}

		//For Set RegistrationKey property in aspx
		public String DbComboRegKey
		{
			get{	return System.Configuration.ConfigurationSettings.AppSettings["DbComboRegKey"];}
		}

		private void BasePage_Load(object sender, System.EventArgs e)
		{
			utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings,Page.Session);
			getSessionTimeOut();
			settingScrollPosition();
			if(!utility.GetUserCulture().ToUpper().Equals("EN-US"))
			{
				getPageControls(Page);		
			}
			if(!Page.IsPostBack)
			{
				SecurityCheck();
			}
			else
			{
				if(ViewState["MODULE_RIGHTS"] != null)
				{
					m_moduleAccessRights  = (AccessRights)ViewState["MODULE_RIGHTS"];
				}
			}
		}

		protected void SecurityCheck()
		{
			String strParentModuleID = Request.Params["PARENTMODID"];
			String strModuleID = Request.Params["MODID"]; 

			m_moduleAccessRights = RBACManager.GetModuleAccessRights(utility.GetAppID(),utility.GetEnterpriseID(),utility.GetUserID(),strModuleID,strParentModuleID);
			ViewState["MODULE_RIGHTS"] = m_moduleAccessRights;

			if(m_moduleAccessRights.Module == false)
			{
				Response.Redirect("AccessDeniedPage.aspx",true);
			}
		}
		protected void settingScrollPosition()
		{
			if(Request["ScrollPosition"] != null)
			{
				strScrollPosition = Request["ScrollPosition"].ToString();
			}
		}
		
		protected void getSessionTimeOut()
		{
			if(utility.GetUserID()==null)
			{
				FormsAuthentication.SignOut();
				Session.Abandon();
				Response.Write("<script>window.top.location.href='EnterpriseLogon.aspx?errMsg=TIMEOUT&culture="+utility.GetUserCulture()+"';</script>");
				Response.End();
			}
		}
		protected void getSessionTimeOut(bool isInfolder)
		{
			utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings,Page.Session);
			if(utility.GetUserID()==null)
			{
				if(isInfolder)
				{
					FormsAuthentication.SignOut();
					Session.Abandon();
					Response.Write("<script>window.top.location.href='../EnterpriseLogon.aspx?errMsg=TIMEOUT&culture="+utility.GetUserCulture()+"';</script>");
					Response.End();
				}
			}
		}
		protected void getPageControls(Control c)
		{
			for(int i = 0;i < c.Controls.Count;i++)
			{
			//Button
				if(c.Controls[i].GetType().ToString() == "System.Web.UI.WebControls.Button")
				{
					System.Web.UI.WebControls.Button btn=(System.Web.UI.WebControls.Button)c.Controls[i];
					if(c.Controls[i].ID !=null)
					{
						btn.Text=Utility.GetLanguageText(ResourceType.ScreenLabel,btn.Text,utility.GetUserCulture());
					}
				}
			
				String str = null;
//			//Label
				if(c.Controls[i].GetType().ToString() == "System.Web.UI.WebControls.Label" && c.Controls[i].GetType().ToString() != "Cambro.Web.DbCombo.DbCombo")
				{
					System.Web.UI.WebControls.Label lbl=(System.Web.UI.WebControls.Label)c.Controls[i];
					if(c.Controls[i].ID !=null && lbl.Text != "" && lbl.Text != null)
					{
						lbl.Text=Utility.GetLanguageText(ResourceType.ScreenLabel,lbl.Text,utility.GetUserCulture());
						str = lbl.Text;
					}
				}
			
				//DataGridLabel
				/*if(c.Controls[i].GetType().ToString() == "System.Web.UI.WebControls.DataGridLabel")
				{
					System.Web.UI.WebControls.Label lbl = (System.Web.UI.WebControls.Label) c.Controls[i];
					if(!lbl.Text.Trim().Equals(""))
					{
						lbl.Text = Utility.GetLanguageText(ResourceType.ScreenLabel, lbl.Text, utility.GetUserCulture());
					} 
				}*/
				//DataGridItem
				/*if (c.Controls[i].GetType().ToString().Equals("System.Web.UI.WebControls.DataGrid"))
				{
					System.Web.UI.WebControls.DataGrid dg = (System.Web.UI.WebControls.DataGrid) c.Controls[i];
					foreach (DataGridItem item in dg.Items)
					{
						int cellCount = item.Cells.Count;
						for (int cellIndex = 0; cellIndex < cellCount; cellIndex++)
						{
							String tmpStr = item.Cells[cellIndex].Text;
							tmpStr = tmpStr + "";
						}
					}
				}*/
				

			//HyperLink
				if(c.Controls[i].GetType().ToString() == "System.Web.UI.WebControls.HyperLink")
				{
					System.Web.UI.WebControls.HyperLink urls=(System.Web.UI.WebControls.HyperLink)c.Controls[i];
					if(c.Controls[i].ID !=null)
					{
						urls.Text=Utility.GetLanguageText(ResourceType.ScreenLabel,urls.Text,utility.GetUserCulture());
					}
				}
			//Checkbox
				if(c.Controls[i].GetType().ToString() == "System.Web.UI.WebControls.CheckBox")
				{
					System.Web.UI.WebControls.CheckBox chk=(System.Web.UI.WebControls.CheckBox)c.Controls[i];
					if(c.Controls[i].ID !=null)
					{	
						chk.Text=Utility.GetLanguageText(ResourceType.ScreenLabel,chk.Text,utility.GetUserCulture());
					}
				}
			//Radio Button
				if(c.Controls[i].GetType().ToString() == "System.Web.UI.WebControls.RadioButton")
				{
					System.Web.UI.WebControls.RadioButton rdo=(System.Web.UI.WebControls.RadioButton)c.Controls[i];
					if(c.Controls[i].ID !=null)
					{	
						rdo.Text=Utility.GetLanguageText(ResourceType.ScreenLabel,rdo.Text,utility.GetUserCulture());
					}
				}

				//Radio Button List
				if(c.Controls[i].GetType().ToString() == "System.Web.UI.WebControls.RadioButtonList")
				{
					System.Web.UI.WebControls.RadioButtonList rdoButLst = (System.Web.UI.WebControls.RadioButtonList) c.Controls[i];
					for (int lstCount = 0; lstCount < rdoButLst.Items.Count; lstCount++)
					{
						System.Web.UI.WebControls.ListItem lstItem = rdoButLst.Items[lstCount];
						if (!lstItem.Text.Trim().Equals(""))
						{
							lstItem.Text = Utility.GetLanguageText(ResourceType.ScreenLabel, lstItem.Text, utility.GetUserCulture()); 
						}
					}
				}

				//Check Box List
				if(c.Controls[i].GetType().ToString() == "System.Web.UI.WebControls.CheckBoxList")
				{
					System.Web.UI.WebControls.CheckBoxList chkBoxLst = (System.Web.UI.WebControls.CheckBoxList) c.Controls[i];
					for (int lstCount = 0; lstCount < chkBoxLst.Items.Count; lstCount++)
					{
						System.Web.UI.WebControls.ListItem lstItem = chkBoxLst.Items[lstCount];
						if (!lstItem.Text.Trim().Equals(""))
						{
							lstItem.Text = Utility.GetLanguageText(ResourceType.ScreenLabel, lstItem.Text, utility.GetUserCulture()); 
						}
					}
				}

			//datagrid
				if(c.Controls[i].GetType().ToString() == "System.Web.UI.WebControls.DataGrid")
				{
					System.Web.UI.WebControls.DataGrid dg=(System.Web.UI.WebControls.DataGrid)c.Controls[i];
					if(c.Controls[i].ID !=null)
					{
						int j=0,k=0;
						j=dg.Columns.Count;
						for(k=0;k<j;k++)
						{
							try
							{
								//String strColName = dg.Columns[k].HeaderText;
								dg.Columns[k].HeaderText=Utility.GetLanguageText(ResourceType.ScreenLabel,dg.Columns[k].HeaderText,utility.GetUserCulture());
							}
							catch(Exception ex)
							{
								Console.Write(ex.Message);
							}
						}
						try
						{
							dg.PagerStyle.NextPageText=Utility.GetLanguageText(ResourceType.ScreenLabel,dg.PagerStyle.NextPageText,utility.GetUserCulture());
							dg.PagerStyle.PrevPageText=Utility.GetLanguageText(ResourceType.ScreenLabel,dg.PagerStyle.PrevPageText,utility.GetUserCulture());
						}
						catch(Exception ex)
						{
							Console.Write(ex.Message);
						}
					}
				}
			//tabbed strip  
				if(c.Controls[i].GetType().ToString() == "Microsoft.Web.UI.WebControls.TabStrip")
				{
					Microsoft.Web.UI.WebControls.TabStrip ts=(Microsoft.Web.UI.WebControls.TabStrip)c.Controls[i];
					foreach(Microsoft.Web.UI.WebControls.Tab tb in ts.Items)
					{
						tb.Text=Utility.GetLanguageText(ResourceType.ScreenLabel,tb.Text,utility.GetUserCulture());						
					}
						
				}
			//validation for required field validator
				if(c.Controls[i].GetType().ToString() == "System.Web.UI.WebControls.RequiredFieldValidator")
				{
					System.Web.UI.WebControls.RequiredFieldValidator rqv=(System.Web.UI.WebControls.RequiredFieldValidator)c.Controls[i];
				
						try
						{		
							rqv.ErrorMessage=Utility.GetLanguageText(ResourceType.ScreenLabel,rqv.ErrorMessage,utility.GetUserCulture());						
						}
						catch(Exception ex)
						{
							Console.Write(ex.Message);
						}
				}
			//validation for RegularExpressionValidator
				if(c.Controls[i].GetType().ToString() == "System.Web.UI.WebControls.RegularExpressionValidator")
				{
					System.Web.UI.WebControls.RegularExpressionValidator rge=(System.Web.UI.WebControls.RegularExpressionValidator)c.Controls[i];
				
					try
					{		
						rge.ErrorMessage=Utility.GetLanguageText(ResourceType.ScreenLabel,rge.ErrorMessage,utility.GetUserCulture());						
					}
					catch(Exception ex)
					{
						Console.Write(ex.Message);
					}
				}
			//validation for CompareValidator
				if(c.Controls[i].GetType().ToString() == "System.Web.UI.WebControls.CompareValidator")
				{
					System.Web.UI.WebControls.CompareValidator cmv=(System.Web.UI.WebControls.CompareValidator)c.Controls[i];
				
					try
					{		
						
						cmv.ErrorMessage=Utility.GetLanguageText(ResourceType.ScreenLabel,cmv.ErrorMessage,utility.GetUserCulture());						
					}
					catch(Exception ex)
					{
						Console.Write(ex.Message);
					}
				}
			//validation for RangeValidator
			if(c.Controls[i].GetType().ToString() == "System.Web.UI.WebControls.RangeValidator")
			{
				System.Web.UI.WebControls.RangeValidator rnv=(System.Web.UI.WebControls.RangeValidator)c.Controls[i];
			
				try
				{		
					rnv.ErrorMessage=Utility.GetLanguageText(ResourceType.ScreenLabel,rnv.ErrorMessage,utility.GetUserCulture());						
				}
				catch(Exception ex)
				{
					Console.Write(ex.Message);
				}
			}
		//validation for summary
			if(c.Controls[i].GetType().ToString() == "System.Web.UI.WebControls.ValidationSummary")
			{
				System.Web.UI.WebControls.ValidationSummary vds=(System.Web.UI.WebControls.ValidationSummary)c.Controls[i];
			
				try
				{	
					
					vds.HeaderText=Utility.GetLanguageText(ResourceType.ScreenLabel,vds.HeaderText,utility.GetUserCulture());						
				}
				catch(Exception ex)
				{
					Console.Write(ex.Message);
				}
			}
		//html tables which are running server side
			if(c.Controls[i].GetType().ToString() == "System.Web.UI.HtmlControls.HtmlTable")
			{
				System.Web.UI.HtmlControls.HtmlTable tb=(System.Web.UI.HtmlControls.HtmlTable)c.Controls[i];
				foreach(System.Web.UI.HtmlControls.HtmlTableRow tr in tb.Rows)
				{
					foreach(System.Web.UI.HtmlControls.HtmlTableCell tc in tr.Cells)
					{
						try
						{
							tc.InnerText=Utility.GetLanguageText(ResourceType.ScreenLabel,tc.InnerText,utility.GetUserCulture());
						}
						catch(Exception ex)
						{
							Console.Write(ex.Message);
						}	
					}						
				}					
			}
		//html button 
				if(c.Controls[i].GetType().ToString() == "System.Web.UI.HtmlControls.HtmlInputButton")
				{
					System.Web.UI.HtmlControls.HtmlInputButton ltc=(System.Web.UI.HtmlControls.HtmlInputButton)c.Controls[i];
					try
					{							
						ltc.Value=Utility.GetLanguageText(ResourceType.ScreenLabel,ltc.Value,utility.GetUserCulture());						
					}
					catch(Exception ex)
					{
						Console.Write(ex.Message);
					}
						
				}
			if(c.Controls[i].HasControls())
				getPageControls(c.Controls[i]);
			}//outer for loop
		}

		private void getAllLabels(string str)
		{
			string FilePath=System.Configuration.ConfigurationSettings.AppSettings["controlName"];
			StreamWriter FileWriter; 			
			if(File.Exists(FilePath))
			{
				try
				{
					FileWriter = File.AppendText(FilePath);
					FileWriter.WriteLine(str.Trim()+"=");
					FileWriter.Close();
				}
				catch(System.IO.IOException e)
				{
					Console.WriteLine(e.Message);
					//Response.Write("click again because It is used by another processor");
				}
			}
			else
			{
				Response.Write("file not found");
			}
			
			
		}
	}
}
