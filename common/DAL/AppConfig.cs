using System;
using System.Collections;


namespace com.common.DAL
{
	/// <summary>
	/// Summary description for AppConfig. Has properties to hold ConfigSettings for the core db and hashtable which
	/// holds the ConfigSettings for the enterprise specific tables.
	/// </summary>
	public class AppConfig
	{
		ConfigSettings m_appConfiguration = null;
		Hashtable m_enterpriseSettings = null;

		public AppConfig()
		{
			m_enterpriseSettings = new Hashtable();
			m_appConfiguration = new ConfigSettings();
		}

		public ConfigSettings AppConfigSettings
		{
			set
			{
				m_appConfiguration=value;
			}
			get
			{
				return m_appConfiguration;
			}
		}

		public ConfigSettings getEnterpriseConfiguration(String strEnterpriseID)
		{
			ConfigSettings configuration = (ConfigSettings)m_enterpriseSettings[strEnterpriseID];
			return configuration;
		}

		public void setEnterpriseConfiguration(String strEnterpriseID, ConfigSettings configuration)
		{
			m_enterpriseSettings[strEnterpriseID] = configuration;
		}
		

	}
}
