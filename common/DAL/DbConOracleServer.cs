using System;
using System.Data;
using System.Data.OracleClient;
using System.Collections;
using com.common.classes;

namespace com.common.DAL
{
	/// <summary>
	/// Summary description for DbConOracleServer.
	/// </summary>
	public class DbConOracleServer : AbstractDbConnection
	{
		public DbConOracleServer(ConfigSettings configuration)
		{
			m_configuration = configuration; 
		}

		/// <summary>
		/// Overloaded constructor taking parameters like Applicationid, enterpriseid, Dataprovider type and ConnectionType
		/// </summary>
		/// <param name="strAppID"></param>
		/// <param name="strEnterpriseID"></param>
		/// <param name="dbProvider"></param>
		/// <param name="dbConType"></param>
		public DbConOracleServer(String strAppID, String strEnterpriseID, DataProviderType dbProvider, ConnectionType dbConType)
		{
			m_configuration = new ConfigSettings();
			m_configuration.ApplicationID = strAppID;
			m_configuration.EnterpriseID = strEnterpriseID;
			m_configuration.ProviderType = dbProvider;
			m_configuration.DBConnectionType = dbConType;
		}

		/// <summary>
		/// Overridden method. Gets the inner IDbConnection object for this DbConnection object
		/// </summary>
		/// <returns>An object of type IDbConnection</returns>
		public override IDbConnection GetConnection()
		{
			OracleConnection oracleConnection = null;

			if(m_configuration.ConnectionString != null)
			{
				oracleConnection = new OracleConnection(m_configuration.ConnectionString);
			}
			return oracleConnection;
		}

		/// <summary>
		/// Begins a transaction for the specified IDbConnection object and returns the IDbTransaction object
		/// </summary>
		/// <param name="connection"></param>
		/// <param name="isolationLevel"></param>
		/// <returns></returns>
		public override IDbTransaction BeginTransaction(IDbConnection connection, IsolationLevel isolationLevel)
		{
			OracleTransaction  oracleTransaction = null;

			if(connection != null)
			{
				OracleConnection oracleCon = (OracleConnection)connection ;
				oracleTransaction = oracleCon.BeginTransaction(isolationLevel);
			}
			return oracleTransaction;
		}

		/// <summary>
		/// Create an OracleDataAdapter object and returned as IDbDataAdapter. That object is initialized using IDbCommand object
		/// </summary>
		/// <param name="dbCmd">An object of type IDbCommand</param>
		/// <returns>Returns IDbDataAdapter objects</returns>
		public override IDbDataAdapter CreateAdapter(IDbCommand dbCmd)
		{
			if(dbCmd == null)
			{
				throw new ApplicationException("IDbCommand object is null.",null);
			}

			OracleDataAdapter  oracleAdapter = new OracleDataAdapter((OracleCommand)dbCmd);
			return (IDbDataAdapter)oracleAdapter;
		}

		/// <summary>
		/// Create an OracleCommand object and return as IDbCommand object
		/// </summary>
		/// <param name="strCmd">Sql Query string</param></param>
		/// <param name="cmdParams">ArrayList containing IDbDataParameter objects</param>
		/// <param name="cmdType">one of values of enum CommandType</param>
		/// <returns>Returns IDbCommand object</returns>
		public override IDbCommand CreateCommand(String strCmd, ArrayList cmdParams, CommandType cmdType)
		{
			OracleCommand dbCommand = new OracleCommand(strCmd);
			if(cmdParams != null) 
			{
				foreach (IDbDataParameter parameter in cmdParams)
					dbCommand.Parameters.Add((OracleParameter)parameter);
			}
			dbCommand.CommandType = cmdType;
			return dbCommand;
		}

		/// <summary>
		/// Creates an OracleCommand object from Sql query and command type and return as IDbCommand Object
		/// </summary>
		/// <param name="strCmd">SQL query string</param>
		/// <param name="cmdType">CommandType enum value</param>
		/// <returns>An instance of IDbCommand type</returns>
		public override IDbCommand CreateCommand(String strCmd, CommandType cmdType)
		{
			OracleCommand dbCommand = new OracleCommand(strCmd);
			dbCommand.Connection = (OracleConnection)m_connection;
			dbCommand.CommandType = cmdType;

			return dbCommand;
		}

		/// <summary>
		/// Create an OracleParameter object and return as IDbDataParameter object from the specified input values
		/// </summary>
		/// <param name="strParamName">Name of parameter</param>
		/// <param name="dbType">DbType of the parameter</param>
		/// <param name="iSize">Size of the parameter</param>
		/// <param name="iParamDirection">one of enum value ParameterDirection</param>
		/// <param name="objValue">Value of the parameter</param>
		/// <returns>An object of type IDbDataParameter</returns>
		public override IDbDataParameter MakeParam(String strParamName, DbType dbType, int iSize, ParameterDirection iParamDirection, Object objValue)
		{
			OracleParameter param = null;

			try
			{
				param = new OracleParameter();
				param.ParameterName = strParamName;
				param.DbType = dbType;
				param.Direction = iParamDirection;
				
				if (iSize > 0)
				{
					param.Size = iSize;
				}

				if (iParamDirection == ParameterDirection.Input && objValue != null)
				{
					param.Value = objValue;
				}
				
			}
			catch(OracleException makeParamException)
			{
				Logger.LogTraceError("DbConSQLServer","MakeParam","EDBCSQL01","Error creating parameters.."+ makeParamException.Message.ToString());
				throw new ApplicationException("Error creating parameters..",makeParamException);
			}
			
			return param;
		}

		/// <summary>
		/// This method is used by ExecuteQuery Methods of AbstractDbConnection class 
		/// </summary>
		/// <param name="dbAdapter">An object of type IDbDataAdapter</param>
		/// <param name="dsToFill">DataSet to fill passed by reference</param>
		/// <param name="iCurrent">Current index from where to fetch records</param>
		/// <param name="iRecSize">Number of records to be fetched</param>
		/// <param name="strTableName">Table name that will be given to the returned dataset</param>
		/// <returns>Returns value returned from OracleAdapter.fill() method</returns>
		protected override int FillDataSet(IDbDataAdapter dbAdapter,ref DataSet dsToFill,int iCurrent, int iRecSize,String strTableName)
		{
			if(dbAdapter == null)
			{
				throw new ApplicationException("IDbDataAdapter object is null.",null);
			}
			if(dsToFill == null)
			{
				throw new ApplicationException("DataSet object is null.",null);
			}
			OracleDataAdapter oracleDbAdapter = (OracleDataAdapter)dbAdapter;
			return oracleDbAdapter.Fill(dsToFill,iCurrent,iRecSize,strTableName);
		}
	}
}
