using System;
using System.Data;
using System.Collections;
using com.common.classes;

namespace com.common.DAL
{
	/// <summary>
	/// Summary description for DbConnection.
	/// </summary>
	public interface DbConnection
	{
		Object ExecuteQuery(String strSqlQuery,ReturnType returnType);
		Object ExecuteQuery(String strSqlQuery,ArrayList queryParams, ReturnType returnType);
		Object ExecuteQuery(IDbCommand dbCmd, ReturnType returnType);

		SessionDS ExecuteQuery(String strSqlQuery,int iCurrent, int iRecSize,String strTableName);
		SessionDS ExecuteQuery(IDbCommand dbCmd, int iCurrent, int iRecSize,String strTableName);

		Object ExecuteScalar(String strSqlQuery);
		Object ExecuteQueryInTransaction(IDbCommand dbCmd, ReturnType returnType);

		int ExecuteNonQuery(IDbCommand dbCmd);
		int ExecuteNonQueryInTransaction(IDbCommand dbCmd);

		Object ExecuteScalarInTransaction(IDbCommand dbCmd);

		int ExecuteBatchSQLinTranscation(ArrayList sqlList);

		IDbDataParameter MakeParam(String strParamName, DbType dbType, int iSize, ParameterDirection iParamDirection, Object objValue);

		int ExecuteProcedure(String strProcName,ArrayList procParams);
		Object ExecuteProcedure(String strProcName,ArrayList procParams,ReturnType returnType);

		int ExecuteProcedureScalar(String strProcName,ArrayList procParams);

		IDbConnection GetConnection();
		IDbTransaction BeginTransaction(IDbConnection connection, IsolationLevel isolationLevel);
		int OpenConnection(ref IDbConnection connection);
		IDbCommand CreateCommand(String strCmd, ArrayList cmdParams, CommandType cmdType);
		IDbCommand CreateCommand(String strCmd, CommandType cmdType);
		IDbDataAdapter CreateAdapter(IDbCommand dbCmd);

	}
}
