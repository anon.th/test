using System;
using System.Data;
using System.Collections;
using com.common.classes;


namespace com.common.DAL
{
	/// <summary>
	/// Summary description for AbstractDbConnection. This is an abstract class. All abstract methods of this class must be overridden by 
	/// some database specific class to implement Polymorphism design Pattern.
	/// </summary>
	/// 
	public abstract class AbstractDbConnection : DbConnection
	{
		protected IDbConnection		m_connection = null;
		protected IDbDataAdapter	m_adapter = null;
		protected DataSet			m_dsQueryReturn = null;
		protected ConfigSettings	m_configuration = null;


		public AbstractDbConnection()
		{
		}

		/// <summary>
		/// Executes the query passed as String. Used for Select Queryies.
		/// </summary>
		/// <param name="strSqlQuery">Query to be executed as String value</param>
		/// <param name="returnType">One of enum values of ReturnType Enum i.e. DataReaderType, XmlDocumentType, DataSetType</param>
		/// <returns>Returns DataSet as Object type when the ReturnType is DataSetType.
		///	                 null when the ReturnType is XmlDocumentType
		///	                 Throws ApplicationException when ReturnType is DataReaderType
		///	                 Throws ApplicationException on any error.</returns>
		public Object ExecuteQuery(String strSqlQuery,ReturnType returnType)
		{
			try
			{
				switch (returnType)
				{
					case ReturnType.DataReaderType:
						throw new ApplicationException("Connection Object Required. Please use other overloaded method.",null);

					case ReturnType.XmlDocumentType:
						return null;

					case ReturnType.DataSetType:
					default:
						m_connection = GetConnection();
						OpenConnection();
						IDbCommand dbCmd = CreateCommand(strSqlQuery,CommandType.Text);
						dbCmd.Connection = m_connection;
						m_adapter = CreateAdapter(dbCmd);
						m_dsQueryReturn = new DataSet();
						m_adapter.Fill(m_dsQueryReturn);
						if(m_connection != null)
						{
							m_connection.Close();
							m_connection.Dispose();
							m_connection=null;
						}
						return m_dsQueryReturn;
				}
				
			}
			catch(Exception exeException)
			{	
				Logger.LogTraceError("AbstractDbConnection","ExecuteQuery","EADBC01","Error executing Query"+ exeException.Message);
				throw new ApplicationException("Error executing Query : "+exeException.Message,exeException);
			}
		}



		/// <summary>
		/// Executes the query passed as String. Used for Select Queryies.
		/// </summary>
		/// <param name="strSqlQuery">Query to be executed as String value</param>
		/// <param name="returnType">One of enum values of ReturnType Enum i.e. DataReaderType, XmlDocumentType, DataSetType</param>
		/// <returns>Returns DataSet as Object type when the ReturnType is DataSetType.
		///	                 null when the ReturnType is XmlDocumentType
		///	                 Throws ApplicationException when ReturnType is DataReaderType
		///	                 Throws ApplicationException on any error.</returns>
		public Object ExecuteQuery(String strSqlQuery,ArrayList queryParams, ReturnType returnType)
		{
			try
			{
				switch (returnType)
				{
					case ReturnType.DataReaderType:
						throw new ApplicationException("Connection Object Required. Please use other overloaded method.",null);

					case ReturnType.XmlDocumentType:
						return null;

					case ReturnType.DataSetType:
					default:
						m_connection = GetConnection();
						OpenConnection();
						IDbCommand dbCmd = CreateCommand(strSqlQuery,queryParams,CommandType.Text);
						dbCmd.Connection = m_connection;
						m_adapter = CreateAdapter(dbCmd);
						m_dsQueryReturn = new DataSet();
						m_adapter.Fill(m_dsQueryReturn);
						if(m_connection != null)
						{
							m_connection.Close();
							m_connection.Dispose();
							m_connection=null;
						}
						return m_dsQueryReturn;
				}
				
			}
			catch(Exception exeException)
			{	
				Logger.LogTraceError("AbstractDbConnection","ExecuteQuery","EADBC01","Error executing Query"+ exeException.Message);
				throw new ApplicationException("Error executing Query : "+exeException.Message,exeException);
			}
		}


		/// <summary>
		/// Executes the query passed as IDbCommand type object. Used for Select Queryies.
		/// </summary>
		/// <param name="dbCmd">IDbCommand object with CommandText property initialized to the query to be executed. CommandType property initialized to CommandType.Text enum value.</param>
		/// <param name="returnType">One of enum values of ReturnType Enum i.e. DataReaderType, XmlDocumentType, DataSetType</param>
		/// <returns>Returns DataSet as Object type when the ReturnType is DataSetType and IDataReader when the ReturnType is DataReaderType
		///	                 null when the ReturnType is XmlDocumentType
		///	                 Throws ApplicationException on any error.</returns>
		public Object ExecuteQuery(IDbCommand dbCmd, ReturnType returnType)
		{

			if(dbCmd == null)
			{
				throw new ApplicationException("IDbCommand object is null.",null);
			}

			try
			{

				switch (returnType)
				{
					case ReturnType.DataReaderType:
						IDataReader dataReader = (IDataReader)dbCmd.ExecuteReader();
						return dataReader;

					case ReturnType.XmlDocumentType:
						return null;

					case ReturnType.DataSetType:
					default:

						m_connection = GetConnection();
						OpenConnection();
						dbCmd.Connection = m_connection;
						m_adapter = CreateAdapter(dbCmd);
						m_dsQueryReturn = new DataSet();
						m_adapter.Fill(m_dsQueryReturn);
						if(m_connection != null)
						{
							m_connection.Close();
							m_connection.Dispose();
							m_connection=null;
						}
						return m_dsQueryReturn;	
				}
		
			}
			catch(Exception exeException)
			{
				if(m_connection != null)
				{
					m_connection.Close();
					m_connection.Dispose();
					m_connection=null;
				}
				Logger.LogTraceError("AbstractDbConnection","ExecuteQuery","EADBC03","Error executing Query"+ exeException.Message);
				throw new ApplicationException("Error executing Query : "+exeException.Message,exeException);
			}
			
		}

		/// <summary>
		/// Executes query passed as String and returns object of type SessionDS which has DataSet propery initialized upon return. This method is used to return fixed number of records from specific index.
		/// </summary>
		/// <param name="strSqlQuery">Query to be executed as String value</param>
		/// <param name="iCurrent">Next index from which records to be fetched.</param>
		/// <param name="iRecSize">Number of records required in fetched DataSet.</param>
		/// <param name="strTableName">Name of table which will be assigned to the return DataSet. Can be any string value.</param>
		/// <returns>Returns object of type SessionDS.</returns>
		public SessionDS ExecuteQuery(String strSqlQuery,int iCurrent, int iRecSize,String strTableName)
		{

			SessionDS sessionDS = null;
			try
			{
				m_connection = GetConnection();
				
				OpenConnection();

				IDbCommand dbCmd = CreateCommand(strSqlQuery,CommandType.Text);
				
				dbCmd.Connection = m_connection;
				m_adapter = CreateAdapter(dbCmd);
				m_dsQueryReturn = new DataSet();
				sessionDS = new SessionDS();

				FillDataSet(m_adapter,ref m_dsQueryReturn,iCurrent, iRecSize,strTableName);

				sessionDS.ds = m_dsQueryReturn;
				sessionDS.DataSetRecSize = m_dsQueryReturn.Tables[0].Rows.Count;
 
				//Find the count of query result
	
//strSqlQuery
				string str2 = strSqlQuery;

				if(strSqlQuery.ToUpper().IndexOf("#TEMP",0) != -1)
				{
					dbCmd.CommandText = genStrforSpecialcase(strSqlQuery);
				}
				else
				{
					dbCmd.CommandText = MakeCountQuery(strSqlQuery);
				}
				sessionDS.QueryResultMaxSize =Convert.ToDecimal(dbCmd.ExecuteScalar());//sessionDS.ds.Tables[0].Rows.Count; // m_dsQueryReturn.Tables[0].Rows.Count;
					
	
				if(m_connection != null)
				{
					m_connection.Close();
					m_connection.Dispose();
					m_connection=null;
				}
			}
			catch(Exception exeException)
			{	
				Logger.LogTraceError("AbstractDbConnection","ExecuteQuery","EADBC01","Error executing Query"+ exeException.Message);
				throw new ApplicationException("Error executing Query : "+exeException.Message,exeException);
			}
			return sessionDS;
			
		}

		private string genStrforSpecialcase(string strBuilder)
		{
			string strSpecial = "";
//			if(strBuilder.ToUpper().IndexOf("ORDER",0) != -1)
//			{
//				strBuilder = strBuilder.ToUpper();
//				int iIndexOfTemp = strBuilder.IndexOf("ORDER",0);
//				strSpecial = strBuilder.Substring(0,iIndexOfTemp - 1);
//			}
//			else
//			{
//				strSpecial = strBuilder;
//			}
//
//			int iIndexOfFrom  = strBuilder.IndexOf("FROM #TEMP ",0);
//			int iIndexOfDrop  = strBuilder.IndexOf(" DROP ",0);
//
//			//strSpecial += strBuilder;
//			strSpecial += " select count(*) from #TEMP ";
//			string str2 = strBuilder.Substring(iIndexOfFrom,iIndexOfDrop);
//			strSpecial += str2;
//			strSpecial += " drop table #temp ";

			strSpecial = strBuilder.Replace("*","count(*)");


			return strSpecial;
		
		}

		/// <summary>
		/// Executes query passed as String and returns object of type SessionDS which has DataSet propery initialized upon return. This method is used to return fixed number of records from specific index.
		/// </summary>
		/// <param name="dbCmd">IDbCommand object with CommandText property initialized to the query to be executed. CommandType property initialized to CommandType.Text enum value.</param>
		/// <param name="iCurrent">Next index from which records to be fetched.</param>
		/// <param name="iRecSize">Number of records required in fetched DataSet.</param>
		/// <param name="strTableName">Name of table which will be assigned to the return DataSet. Can be any string value.</param>
		/// <returns>Returns object of type SessionDS.</returns>
		public SessionDS ExecuteQuery(IDbCommand dbCmd, int iCurrent, int iRecSize,String strTableName)
		{
			SessionDS sessionDS = null;

			if(dbCmd == null)
			{
				throw new ApplicationException("IDbCommand object is null.",null);
			}

			try
			{
				m_connection = GetConnection();
				OpenConnection();
				dbCmd.Connection = m_connection;
				m_adapter = CreateAdapter(dbCmd);
				m_dsQueryReturn = new DataSet();
				sessionDS = new SessionDS();

				FillDataSet(m_adapter,ref m_dsQueryReturn,iCurrent, iRecSize,strTableName);
				sessionDS.ds = m_dsQueryReturn;

				dbCmd.CommandText = MakeCountQuery(dbCmd.CommandText);
				sessionDS.QueryResultMaxSize = Convert.ToDecimal(dbCmd.ExecuteScalar());
				sessionDS.DataSetRecSize = m_dsQueryReturn.Tables[0].Rows.Count;



				if(m_connection != null)
				{
					m_connection.Close();
					m_connection.Dispose();
					m_connection=null;
				}
		
			}
			catch(Exception exeException)
			{
				if(m_connection != null)
				{
					m_connection.Close();
					m_connection.Dispose();
					m_connection=null;
				}
				Logger.LogTraceError("AbstractDbConnection","ExecuteQuery","EADBC03","Error executing Query"+ exeException.Message);
				throw new ApplicationException("Error executing Query : "+exeException.Message,exeException);
			}
			return sessionDS;
		}


		/// <summary>
		/// Executes the scalar for the sql query passed as parameter and return the first value for the first column
		/// </summary>
		/// <param name="strSqlQuery">Query to be executed</param>
		/// <returns>Returns the first value for the first column as object</returns>
		public Object ExecuteScalar(String strSqlQuery)
		{
			Object objToReturn = null;

			try
			{
				m_connection = GetConnection();
				OpenConnection();
				IDbCommand dbCmd = CreateCommand(strSqlQuery,CommandType.Text);
				dbCmd.Connection = m_connection;
				objToReturn = dbCmd.ExecuteScalar();
			}
			catch(Exception exeException)
			{	
				Logger.LogTraceError("AbstractDbConnection","ExecuteScalar","EADBC01","Error executing scalar"+ exeException.Message);
				throw new ApplicationException("Error executing scalar : "+exeException.Message,exeException);
			}
			finally
			{
				if(m_connection != null)
				{
					m_connection.Close();
					m_connection.Dispose();
					m_connection=null;
				}
			}
			return objToReturn;
		}
		
		/// <summary>
		/// Executes the query passed as IDbCommand type object in side a transaction. Used for Select Queryies.
		/// </summary>
		/// <param name="dbCmd">IDbCommand object with CommandText property initialized to the query to be executed. 
		///						CommandType property initialized to CommandType.Text enum value. 
		///						Connection property initialized with IDbConnection type object
		///						Transaction property initialized with IDbTransaction type object.</param>
		/// <param name="returnType">One of enum values of ReturnType Enum i.e. DataReaderType, XmlDocumentType, DataSetType</param>
		/// <returns>Returns DataSet as Object type when the ReturnType is DataSetType and IDataReader when the ReturnType is DataReaderType
		///	                 null when the ReturnType is XmlDocumentType
		///	                 Throws ApplicationException on any error and the transaction is rolled bacak.</returns>
		public Object ExecuteQueryInTransaction(IDbCommand dbCmd, ReturnType returnType)
		{
			if(dbCmd == null)
			{
				throw new ApplicationException("IDbCommand object is null.",null);
			}

			try
			{
				switch (returnType)
				{
					case ReturnType.DataReaderType:
						IDataReader dataReader = (IDataReader)dbCmd.ExecuteReader();
						return dataReader;

					case ReturnType.XmlDocumentType:
						return null;

					case ReturnType.DataSetType:
					default:
						m_adapter = CreateAdapter(dbCmd);
						m_dsQueryReturn = new DataSet();
						m_adapter.Fill(m_dsQueryReturn);
						
						return m_dsQueryReturn;	
				}
		
			}
			catch(Exception exeException)
			{
				Logger.LogTraceError("AbstractDbConnection","ExecuteQueryInTransaction","EADBC04","Error executing Query in Transaction "+ exeException.Message);
				throw new ApplicationException("Error in NonQuery : "+exeException.Message,exeException);
			}
		}


		/// <summary>
		/// Executes the query passed as IDbCommand type object in side a transaction as scalar.
		/// </summary>
		/// <param name="dbCmd">IDbCommand object with CommandText property initialized to the query to be executed. 
		///						CommandType property initialized to CommandType.Text enum value. 
		///						Connection property initialized with IDbConnection type object
		///						Transaction property initialized with IDbTransaction type object.</param>
		/// <returns>Returns Object as Object type </returns>
		public Object ExecuteScalarInTransaction(IDbCommand dbCmd)
		{
			Object objToReturn = null;

			if(dbCmd == null)
			{
				throw new ApplicationException("IDbCommand object is null.",null);
			}

			try
			{
				objToReturn = dbCmd.ExecuteScalar();
			}
			catch(Exception exeException)
			{
				Logger.LogTraceError("AbstractDbConnection","ExecuteScalarInTransaction","EADBC04","Error executing scalar in Transaction "+ exeException.Message);
				throw new ApplicationException("Error in NonQuery : "+exeException.Message,exeException);
			}

			return objToReturn;

		}


		/// <summary>
		/// Executes query passed as IDbCommand Object. Used for queries like Insert, Update and Delete.
		/// </summary>
		/// <param name="dbCmd">IDbCommand object which has CommandText property initialized.</param>
		/// <returns>Returns number of records affected for the particular query. E.g. Query deletes 2 records when executed. It will return 2.
		///			 Throws ApplicationException on error.</returns>
		public int ExecuteNonQuery(IDbCommand dbCmd)
		{
			int iRowsAffected = 0;

			if(dbCmd == null)
			{
				throw new ApplicationException("IDbCommand object is null.",null);
			}

			try
			{
				m_connection = GetConnection();
				OpenConnection();
				dbCmd.Connection = m_connection;
				iRowsAffected = dbCmd.ExecuteNonQuery();

			}
			catch(Exception nonQueryException)
			{
				Logger.LogTraceError("AbstractDbConnection","ExecuteNonQuery","EADBC05","Error executing NonQuery "+ nonQueryException.Message);
				throw new ApplicationException("Error in NonQuery : "+nonQueryException.Message,nonQueryException);
			}
			finally
			{
				if(m_connection != null)
				{
					m_connection.Close();
					m_connection.Dispose();
					m_connection=null;
				}
			}
			return iRowsAffected;
		}
		/// <summary>
		/// Executes non-Query passed as IDbCommand Object in transaction. Used for queries like Insert, Update and Delete.
		/// </summary>
		/// <param name="dbCmd">IDbCommand object with CommandText property initialized to the query to be executed. 
		///						CommandType property initialized to CommandType.Text enum value. 
		///						Connection property initialized with IDbConnection type object
		///						Transaction property initialized with IDbTransaction type object.</param>
		///<returns>Returns number of records affected for the particular query. E.g. Query deletes 2 records when executed. It will return 2.
		///			 Throws ApplicationException on error.</returns>
		public int ExecuteNonQueryInTransaction(IDbCommand dbCmd)
		{
			int iRowsAffected = 0;

			if(dbCmd == null)
			{
				throw new ApplicationException("IDbCommand object is null.",null);
			}

			try
			{
				iRowsAffected = dbCmd.ExecuteNonQuery();
			}
			catch(Exception nonQueryException)
			{
				Logger.LogTraceError("AbstractDbConnection","ExecuteNonQueryInTransaction","EDB006","Error executing NonQuery in Transaction"+ nonQueryException.Message);
				throw new ApplicationException("Error in NonQuery : "+nonQueryException.Message,nonQueryException);
				
			}
			return iRowsAffected;

		}


		/// <summary>
		/// Executes a list of non-queries inside a transaction. The transaction will be commited upon successful execution of all queries. If a single query execution fails, the entire transaction will be rolled back.
		/// </summary>
		/// <param name="sqlList">ArrayList object which contains list of Sql non-queries as string objects</param>
		/// <returns>Returns 1 if the transaction is comitted successfully. 
		///			 Throws ApplicationException on error and transaction is rolled back.</returns>
		public int ExecuteBatchSQLinTranscation(ArrayList sqlList)
		{

			int iReturn = 0;
			IDbTransaction dbTransaction = null;


			try
			{
				m_connection = GetConnection();
				OpenConnection();
				dbTransaction = m_connection.BeginTransaction(IsolationLevel.ReadCommitted);
				IDbCommand dbBatchCmd = CreateCommand("",CommandType.Text);
				dbBatchCmd.Connection = m_connection;
				dbBatchCmd.Transaction = dbTransaction;

				foreach(string strSql in sqlList)
				{
					dbBatchCmd.CommandText = strSql;
					dbBatchCmd.ExecuteNonQuery();
				}

				dbTransaction.Commit();

				iReturn = 1;
			}
			catch(Exception exception)
			{
				try
				{
					dbTransaction.Rollback();
					Logger.LogTraceInfo("AbstractDbConnection","ExecuteBatchSQLinTranscation","EDB006","Transaction roll backed.");

				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("AbstractDbConnection","ExecuteBatchSQLinTranscation","EDB006","Transaction roll backed. "+ rollbackException.Message.ToString());
				}


				Logger.LogTraceError("AbstractDbConnection","ExecuteBatchSQLinTranscation","EDB006","Error in executing Batch SQL"+ exception.Message);
				throw new ApplicationException("Error in executing Batch SQL : "+ exception.Message,exception);
			}

			finally
			{
				if(m_connection != null)
				{
					m_connection.Close();
					m_connection.Dispose();
					m_connection=null;
				}
			}
			
			return iReturn;
		}
		
		/// <summary>
		/// Executes Sql Procedure.
		/// </summary>
		/// <param name="strProcName">Name of the procedure tobe executed.</param>
		/// <param name="procParams">List of parameter as ArrayList containing IDbDataParameter type of objects.</param>
		/// <returns>Returns 0 on failure and 1 for success.</returns>
		public int ExecuteProcedure(String strProcName,ArrayList procParams)
		{
			int iReturn = 0;

			try
			{
				m_connection = GetConnection();
				OpenConnection();
				IDbCommand dbCmd = CreateCommand(strProcName,procParams,CommandType.StoredProcedure);
				dbCmd.Connection = m_connection;
				dbCmd.ExecuteNonQuery();
				iReturn = 1;
			}
			catch(Exception execProcedure)
			{
				Logger.LogTraceError("AbstractDbConnection","ExecuteProcedure","EADBC05","Error executing procedure "+ execProcedure.Message);
				throw new ApplicationException("Error in execute Procedure : "+execProcedure.Message,execProcedure);
			}
			finally
			{
				if(m_connection != null)
				{
					m_connection.Close();
					m_connection.Dispose();
					m_connection=null;
				}
			}
			return iReturn;
		}
		public int ExecuteProcedureScalar(String strProcName,ArrayList procParams)
		{
			int iReturn = 0;

			try
			{
				m_connection = GetConnection();
				OpenConnection();
				IDbCommand dbCmd = CreateCommand(strProcName,procParams,CommandType.StoredProcedure);
				dbCmd.Connection = m_connection;
				iReturn= (int)dbCmd.ExecuteScalar();
				//iReturn = 1;
			}
			catch(Exception execProcedure)
			{
				Logger.LogTraceError("AbstractDbConnection","ExecuteProcedure","EADBC05","Error executing procedure "+ execProcedure.Message);
				throw new ApplicationException("Error in execute Procedure : "+execProcedure.Message,execProcedure);
			}
			finally
			{
				if(m_connection != null)
				{
					m_connection.Close();
					m_connection.Dispose();
					m_connection=null;
				}
			}
			return iReturn;
		}

		/// <summary>
		/// Executes Sql Procedure and return DataSet as result of execution.
		/// </summary>
		/// <param name="strProcName">Name of the procedure.</param>
		/// <param name="procParams">List of parameter as ArrayList containing IDbDataParameter type of objects.</param>
		/// <param name="returnType"></param>
		/// <returns>Returns DataSet as Object type when the ReturnType is DataSetType.
		///	                 null when the ReturnType is XmlDocumentType
		///	                 Throws ApplicationException when ReturnType is DataReaderType
		///	                 Throws ApplicationException on any error.</returns>
		public Object ExecuteProcedure(String strProcName,ArrayList procParams,ReturnType returnType)
		{

			try
			{
				switch (returnType)
				{
					case ReturnType.DataReaderType:
						throw new ApplicationException("DataReader can not be returned with procedure. Please use ExecuteQuery method.",null);

					case ReturnType.XmlDocumentType:
						return null;

					case ReturnType.DataSetType:
					default:
						m_connection = GetConnection();
						OpenConnection();
						IDbCommand dbCmd = CreateCommand(strProcName,procParams,CommandType.StoredProcedure);
						dbCmd.Connection = m_connection;
						m_adapter = CreateAdapter(dbCmd);
						m_dsQueryReturn = new DataSet();
						m_adapter.Fill(m_dsQueryReturn);
						if(m_connection != null)
						{
							m_connection.Close();
							m_connection.Dispose();
							m_connection=null;
						}
						return m_dsQueryReturn;
				}
				
			}
			catch(Exception exeException)
			{	
				Logger.LogTraceError("AbstractDbConnection","ExecuteProcedure","EADBC01","Error executing procedure"+ exeException.Message);
				throw new ApplicationException("Error executing procedure : "+exeException.Message,exeException);
			}
		}


		/// <summary>
		/// Used to open IDbConnection object which is passed by reference to this function.
		/// </summary>
		/// <param name="connection">IDbConnection object passed by reference.</param>
		/// <returns>Returns 0 on success and -1 on failure. Throws ApplicationException on error.</returns>
		public int OpenConnection(ref IDbConnection connection)
		{
			if(connection == null)
			{
				throw new ApplicationException("IDbConnection object is null.",null);
			}
			
			int iReturn = 0;
			try
			{
				if (connection.State==System.Data.ConnectionState.Closed)
				{
					connection.Open();		
				}
			}
			catch(Exception ConException)
			{
				iReturn = -1;
				Logger.LogTraceError("AbstractDbConnection","OpenConnection","EADBC06","Error Opening Connection "+ ConException.Message);
				throw new ApplicationException("Error Opening Connection : "+ConException.Message,ConException);
			}
			return iReturn;
		}



		/// <summary>
		/// Gets IDbConnection object. The connection state will be closed. i.e. not open state. Tobe overridden.
		/// </summary>
		/// <returns>Returns IDbConnection object.</returns>
		public abstract IDbConnection GetConnection();

		/// <summary>
		/// Begins transaction for an IDbConnection object for the specified IsolationLevel. Tobe overridden.
		/// </summary>
		/// <param name="connection">IDbConnection object</param>
		/// <param name="isolationLevel">IsolationLevel used to create the Transaction</param>
		/// <returns>IDbTransaction object.</returns>
		public abstract IDbTransaction BeginTransaction(IDbConnection connection, IsolationLevel isolationLevel);

		/// <summary>
		/// The function is used to create the parameter to be used for ExecuteProcedure functions. Tobe overridden.
		/// </summary>
		/// <param name="strParamName">Name of the parameter as defined in the sql procedure as String object</param>
		/// <param name="dbType">one of DbType enum values.</param>
		/// <param name="iSize">Size of the parameter as integer.</param>
		/// <param name="iParamDirection">ParameterDirection as one of ParameterDirection enum values</param>
		/// <param name="objValue">Value of this parameter.</param>
		/// <returns>Returns IDbDataParameter type object</returns>
		public abstract IDbDataParameter MakeParam(String strParamName, DbType dbType, int iSize, ParameterDirection iParamDirection, Object objValue);

		/// <summary>
		/// Creates the command object and returned as IDbCommand object. Tobe overridden.
		/// </summary>
		/// <param name="strCmd">Sql query for the command object.</param>
		/// <param name="cmdParams">ArrayList of IDbDataParameter type of objects used for the command.</param>
		/// <param name="cmdType">one of CommandType enum values</param>
		/// <returns>Returns IDbCommand Type object</returns>
		public abstract IDbCommand CreateCommand(String strCmd, ArrayList cmdParams, CommandType cmdType);

		/// <summary>
		/// Creates the command object and returned as IDbCommand object. Tobe overridden.
		/// </summary>
		/// <param name="strCmd">Sql query for the command object.</param>
		/// <param name="cmdType">one of CommandType enum values</param>
		/// <returns>Returns IDbCommand Type object</returns>
		public abstract IDbCommand CreateCommand(String strCmd, CommandType cmdType);

		/// <summary>
		/// Creates an Adapter object from the command object. Tobe overridden.
		/// </summary>
		/// <param name="dbCmd">IDbCommand object</param>
		/// <returns>Returns IDbDataAdapter object.</returns>
		public abstract IDbDataAdapter CreateAdapter(IDbCommand dbCmd);

		/// <summary>
		/// This method is used by ExecuteQuery Methods of this class 
		/// </summary>
		/// <param name="dbAdapter">An object of type IDbDataAdapter</param>
		/// <param name="dsToFill">DataSet to fill passed by reference</param>
		/// <param name="iCurrent">Current index from where to fetch records</param>
		/// <param name="iRecSize">Number of records to be fetched</param>
		/// <param name="strTableName">Table name that will be given to the returned dataset</param>
		/// <returns>Returns value returned from SqlAdapter.fill() method</returns>
		protected abstract int FillDataSet(IDbDataAdapter dbAdapter,ref DataSet dsToFill,int iCurrent, int iRecSize,String strTableName);


		/// <summary>
		/// Private method used by methods of this class to open the db connection.
		/// </summary>
		/// <returns>-1 if some error. 0 for success.</returns>
		private int OpenConnection()
		{
			int iReturn = 0;
			try
			{				
				if (m_connection != null && m_connection.State==System.Data.ConnectionState.Closed)
				{
					m_connection.Open();		
				}
			}
			catch(Exception ConException)
			{
				iReturn = -1;
				Logger.LogTraceError("AbstractDbConnection","OpenConnection","EADBC06","Error Opening Connection "+ ConException.Message);
				throw new ApplicationException("Error Opening Connection : "+ConException.Message,ConException);				
			}
			return iReturn;
		}



		/// <summary>
		/// Private method used by methods of this class to prepare a intermediate query.
		/// </summary>
		/// <param name="strSqlQuery">Actual query</param>
		/// <returns>Intermediate Count query</returns>
		private String MakeCountQuery(String strSqlQuery)
		{

			String strSqlQueryWithoutOrder = "";

			if(strSqlQuery.ToUpper().IndexOf("ORDER",0) != -1)
			{
				strSqlQuery = strSqlQuery.ToUpper();
				int iIndexOfOrder = strSqlQuery.IndexOf("ORDER",0);
				strSqlQueryWithoutOrder = strSqlQuery.Substring(0,iIndexOfOrder - 1);
			}
			else
			{
				strSqlQueryWithoutOrder = strSqlQuery;
			}

			String strSelectCountQuery = "SELECT COUNT(*) from ("+ strSqlQueryWithoutOrder +") a";
			return strSelectCountQuery;
		}

	}
}
