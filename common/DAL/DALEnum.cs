using System;

namespace com.common.DAL
{
	/// <summary>
	/// Summary description for various enum used by the DAL layer
	/// </summary>
	public enum ReturnType
	{
		DataSetType,
		DataReaderType,
		XmlDocumentType
	}

	public enum DataProviderType
	{
		Sql,
		Oledb,
		Oracle
	}

	public enum ConnectionType
	{
		Core,
		App
	}

	
}