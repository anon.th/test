﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _Default : Page
{
    protected MultiView agentProfileMultiPage;
    protected Button btnTab1;
    protected Button btnTab2;
    protected Button btnTab3;

    protected System.Web.UI.WebControls.Menu tabMenu1;

    protected void Page_Load(Object sender, EventArgs e)
    {
        agentProfileMultiPage = (MultiView)FindControl("MultiView1");

        btnTab1 = (Button)FindControl("Button1");
        btnTab2 = (Button)FindControl("Button2");
        btnTab3 = (Button)FindControl("Button3");

        if (!Page.IsPostBack)
        {
            //  Make the first tab selected
            agentProfileMultiPage.ActiveViewIndex = 0;
        }

    }

    protected void Tab1_Click(Object sender, EventArgs e)
    {
        btnTab1.CssClass = "Clicked";
        btnTab2.CssClass = "Initial";
        btnTab3.CssClass = "Initial";

        agentProfileMultiPage.ActiveViewIndex = 0;

        ShowLabels();
    }

    protected void Tab2_Click(Object sender, EventArgs e)
    {
        btnTab1.CssClass = "Initial";
        btnTab2.CssClass = "Clicked";
        btnTab3.CssClass = "Initial";

        agentProfileMultiPage.ActiveViewIndex = 1;

        ShowLabels();
    }

    protected void Tab3_Click(Object sender, EventArgs e)
    {
        btnTab1.CssClass = "Initial";
        btnTab2.CssClass = "Initial";
        btnTab3.CssClass = "Clicked";

        agentProfileMultiPage.ActiveViewIndex = 2;

        ShowLabels();
    }

    protected void ShowLabels()
    { 
        System.Web.UI.WebControls.Label lbl1;
        System.Web.UI.WebControls.Label lbl2;
        System.Web.UI.WebControls.Label lbl3;

        lbl1 = (Label)FindControlInView("Label1");
        lbl2 = (Label)FindControlInView("Label2");
        lbl3 = (Label)FindControlInView("Label3");

        lbl1.Text = "Agent Information - Tab1";
        lbl2.Text = "Agent Information - Tab2";
        lbl3.Text = "Agent Information - Tab3";

        //Message.Text = "Tab " + e.Item.Text + " clicked";
    }

    protected Control FindControlInView(string id)
    {
        Control ctl = null;

        foreach (View v in agentProfileMultiPage.Views)
        {
            ctl = v.FindControl(id);
            if (null != ctl)
            {
                break;
            }
        }

        return ctl;
    }

}
