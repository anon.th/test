﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(TabControl.Startup))]
namespace TabControl
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
