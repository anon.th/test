﻿<%@ Page Title="Home Page" Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
    <HEAD>
        <title>AgentProfile</title>
        <style type="text/css">
        .Initial
        {
            display: block;
            padding: 4px 18px 4px 18px;
            float: left;
            background: url("../Images/InitialImage.png") no-repeat right top;
            color: Black;
            font-weight: bold;
        }
        .Initial:hover
        {
            color: White;
            background: url("../Images/SelectedButton.png") no-repeat right top;
        }
        .Clicked
        {
            float: left;
            display: block;
            background: url("../Images/SelectedButton.png") no-repeat right top;
            padding: 4px 18px 4px 18px;
            color: Black;
            font-weight: bold;
            color: White;
        }
    </style>
    </HEAD>
    <BODY>
	    <FORM id="AgentProfile" method="post" runat="server">
            <table width="80%" align="center">
                <tr>
                    <td>
                        <asp:Button Text="Tab 1" BorderStyle="None" ID="Button1" CssClass="Initial" runat="server" OnClick="Tab1_Click" />
                        <asp:Button Text="Tab 2" BorderStyle="None" ID="Button2" CssClass="Initial" runat="server" OnClick="Tab2_Click" />
                        <asp:Button Text="Tab 3" BorderStyle="None" ID="Button3" CssClass="Initial" runat="server" OnClick="Tab3_Click" />
                    </td>
                </tr>
            </table>
            <div>
                <asp:MultiView 
            ID="MultiView1"
            runat="server"
            ActiveViewIndex="0"  >
            <asp:View ID="Tab1" runat="server"  >
                <table width="600" height="400" cellpadding=0 cellspacing=0>
                    <tr valign="top">
                        <td class="TabArea" style="width: 600px">
                            <br />
                            <br />
                            Tab View 1
                            <asp:Label id="Label1" runat="server"></asp:Label>
                        </td>
                    </tr>
                </table>
                </asp:View>
            <asp:View ID="Tab2" runat="server">
                <table width="600px" height="400px" cellpadding=0 cellspacing=0>
                    <tr valign="top">
                        <td class="TabArea" style="width: 600px">
                        <br />
                        <br />
                            Tab View 2
                            <asp:Label id="Label2" runat="server"></asp:Label>
                        </td>
                    </tr>
                </table>
            </asp:View>
            <asp:View ID="Tab3" runat="server">
                <table width="600px" height="400px" cellpadding=0 cellspacing=0>
                    <tr valign="top">
                        <td class="TabArea" style="width: 600px">
                        <br />
                        <br />
                            Tab View 3
                            <asp:Label id="Label3" runat="server"></asp:Label>
                        </td>
                    </tr>
                </table>
            </asp:View>
        </asp:MultiView>
            </div>
            <asp:label id="Message" runat="server"/>
        </FORM>
    </BODY>
</HTML>
