﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Web.UI.WebControls;

namespace Aware.Web.UI.WebControls
{
    class TreeNode : System.Web.UI.WebControls.TreeNode
    {

        public bool CheckBox
        {
            get
            {
                return (bool)this.ShowCheckBox;
            }
            set
            {
                this.ShowCheckBox = value;
            }
        }

        public TreeNode GetNodeFromIndex(string strIndex)
        {
            int index = Int32.Parse(strIndex);

            return null; // this.ChildNodes[index];
        }

        public string NodeData
        {
            get
            {
                return this.Text;
            }
            set
            {
                this.Text = value;
            }


        }

    }

}
