﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DbFactory.cs" company="Aware Corporation Limited">
//   Copyright (c) 2014-2015.  All rights reserved.
// </copyright>
// <summary>
//   Defines the DbFactory type.
//   Reviewed on 28 Jul 2016.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using PNG.DataAccess.Interfaces;
using System.Data;
using System.Data.SqlClient;

namespace PNG.DataAccess.Database
{
    /// <summary>
    /// The db factory.
    /// </summary>
    public class DbFactory : IDbFactory
    {
        /// <summary>
        /// The _connection.
        /// </summary>
        private IDbConnection _connection;

        /// <summary>
        /// Gets the connection.
        /// </summary>
        public IDbConnection Connection
        {
            get { return this._connection ?? (this._connection = this.CreateClientConnection()); }
        }

        /// <summary>
        /// The create client connection.
        /// </summary>
        /// <returns>
        /// The <see cref="IDbConnection"/>.
        /// </returns>
        public IDbConnection CreateClientConnection()
        {
            const string connectionStringName = "DMS_R3_PNGAFConnectionString";
            return CreateConnection(connectionStringName);
        }

        /// <summary>
        /// The create connection.
        /// </summary>
        /// <param name="connectionStringName">
        /// The name.
        /// </param>
        /// <returns>
        /// The <see cref="IDbConnection"/>.
        /// </returns>
        private static IDbConnection CreateConnection(string connectionStringName)
        {
            if (string.IsNullOrEmpty(connectionStringName))
                throw new System.ArgumentNullException("connectionStringName");

            var connectionString = Common.Helpers.ConfigurationHelper.GetConnectString(connectionStringName);

            if (string.IsNullOrEmpty(connectionString))
                throw new System.NullReferenceException("The 'database connection string' argument cannot be null or empty.");

            IDbConnection result = new SqlConnection(connectionString);
            return result;
        }
    }
}