﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SearchQueryHelper.cs" company="Aware Corporation Limited">
//   Copyright (c) 2014-2015.  All rights reserved.
// </copyright>
// <summary>
//   Defines the SearchQueryHelper type.
//   Reviewed on 15 Jan 2016.
// </summary>
// -------------------------------------------------------------------------------------------------------------------

using Dapper;
using Newtonsoft.Json;
using PNG.Common.Enums;
using PNG.Common.Helpers;
using PNG.Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PNG.DataAccess
{
    /// <summary>
    /// The search query helper.
    /// </summary>
    public static class SearchQueryHelper
    {
        /// <summary>
        /// Create Command
        /// </summary>
        /// <param name="sqlFields"></param>
        /// <param name="sqlFrom"></param>
        /// <param name="sqlCondition"></param>
        /// <returns></returns>
        public static StringBuilder CreateCommand(string sqlFields, string sqlFrom, string sqlCondition)
        {
            return CreateCommand(sqlFields, sqlFrom, sqlCondition, false);
        }

        /// <summary>
        /// Create Command
        /// </summary>
        /// <param name="sqlFields"></param>
        /// <param name="sqlFrom"></param>
        /// <param name="sqlCondition"></param>
        /// <param name="isReadUnCommited"></param>
        /// <returns></returns>
        public static StringBuilder CreateCommand(string sqlFields, string sqlFrom, string sqlCondition, bool isReadUnCommited)
        {
            if (string.IsNullOrEmpty(sqlFields))
            {
                throw new ArgumentNullException("sqlFields");
            }

            if (string.IsNullOrEmpty(sqlFrom))
            {
                throw new ArgumentNullException("sqlFrom");
            }

            if (string.IsNullOrEmpty(sqlCondition))
            {
                throw new ArgumentNullException("sqlCondition");
            }

            var sqlBuilder = new StringBuilder();

            if (isReadUnCommited)
            {
                sqlBuilder.AppendFormat(@"SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED; ");
            }

            // Sanitize the SQL command
            //sqlFields = SearchQuerySanitizer.SanitizeSQL(sqlFields, true);
            //sqlFrom = SearchQuerySanitizer.SanitizeSQL(sqlFrom, true);
            //sqlCondition = SearchQuerySanitizer.SanitizeSQL(sqlCondition, false);

            sqlBuilder.AppendFormat(@"SELECT {0} FROM {1} WHERE {2} ", sqlFields, sqlFrom, sqlCondition);

            return sqlBuilder;
        }

        /// <summary>
        /// Create sql count command
        /// </summary>
        /// <param name="sqlFrom">
        /// </param>
        /// <param name="sqlCondition">
        /// </param>
        /// <param name="isColse">
        /// </param>
        /// <param name="sqlFieldCount"></param>
        /// <returns>
        /// The <see cref="StringBuilder"/>.
        /// </returns>
        public static StringBuilder CreateCommandCount(string sqlFrom, string sqlCondition, bool isColse = true, string sqlFieldCount = null)
        {
            if (string.IsNullOrEmpty(sqlFrom))
            {
                throw new ArgumentNullException("sqlFrom");
            }

            if (string.IsNullOrEmpty(sqlCondition))
            {
                throw new ArgumentNullException("sqlCondition");
            }

            var sqlBuilder = new StringBuilder();

            // Sanitize the SQL command
            //sqlFieldCount = SearchQuerySanitizer.SanitizeSQL(sqlFieldCount, true);
            //sqlFrom = SearchQuerySanitizer.SanitizeSQL(sqlFrom, true);
            //sqlCondition = SearchQuerySanitizer.SanitizeSQL(sqlCondition, false);

            if (!string.IsNullOrEmpty(sqlFieldCount))
            {
                sqlBuilder.AppendFormat(@"SELECT COUNT(DISTINCT {2}) AS TotalRecord FROM {0} WHERE {1} ", sqlFrom, sqlCondition, sqlFieldCount);
            }
            else
            {
                sqlBuilder.AppendFormat(@"SELECT COUNT(1) AS TotalRecord FROM {0} WHERE {1} ", sqlFrom, sqlCondition);
            }

            if (isColse)
            {
                sqlBuilder.Append(";");
            }

            return sqlBuilder;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sqlFrom"></param>
        /// <param name="sqlCondition"></param>
        /// <param name="isColse"></param>
        /// <param name="sqlFieldCount"></param>
        /// <returns></returns>
        public static StringBuilder CreateCommandCustom(string sqlFrom, string sqlCondition, bool isColse = true)
        {
            if (string.IsNullOrEmpty(sqlFrom))
            {
                throw new ArgumentNullException("sqlFrom");
            }

            if (string.IsNullOrEmpty(sqlCondition))
            {
                throw new ArgumentNullException("sqlCondition");
            }

            var sqlBuilder = new StringBuilder();

            // Sanitize the SQL command
            //sqlFieldCount = SearchQuerySanitizer.SanitizeSQL(sqlFieldCount, true);
            //sqlFrom = SearchQuerySanitizer.SanitizeSQL(sqlFrom, true);
            //sqlCondition = SearchQuerySanitizer.SanitizeSQL(sqlCondition, false);

            
            sqlBuilder.AppendFormat(@" {0} WHERE {1} ", sqlFrom, sqlCondition);
          

            if (isColse)
            {
                sqlBuilder.Append(";");
            }

            return sqlBuilder;
        }

        /// <summary>
        /// Create sql offset command
        /// </summary>
        /// <param name="itemPerPage">
        /// </param>
        /// <param name="pageIndex">
        /// </param>
        /// <returns>
        /// The <see cref="StringBuilder"/>.
        /// </returns>
        public static StringBuilder CreateCommandOffset(int? itemPerPage, int? pageIndex)
        {
            var sqlBuilder = new StringBuilder();

            if (itemPerPage.HasValue && pageIndex.HasValue)
            {
                sqlBuilder.AppendFormat(@"OFFSET {0}*({1}-1) ROWS FETCH NEXT {0} ROWS ONLY; ", itemPerPage, pageIndex);
            }

            return sqlBuilder;
        }

        /// <summary>
        /// Create sql command with order by
        /// </summary>
        /// <param name="sqlFields">
        /// </param>
        /// <param name="sqlFrom">
        /// </param>
        /// <param name="sqlCondition">
        /// </param>
        /// <param name="sqlFieldOrderBy">
        /// </param>
        /// <param name="sortAscending">
        /// </param>
        /// <returns>
        /// The <see cref="StringBuilder"/>.
        /// </returns>
        public static StringBuilder CreateCommandWithOrderBy(string sqlFields, string sqlFrom, string sqlCondition, string sqlFieldOrderBy)
        {
            return CreateCommandWithOrderBy(sqlFields, sqlFrom, sqlCondition, sqlFieldOrderBy, null, false);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sqlFields"></param>
        /// <param name="sqlFrom"></param>
        /// <param name="sqlCondition"></param>
        /// <param name="sqlFieldOrderBy"></param>
        /// <returns></returns>
        public static StringBuilder CreateCommandWithOrderByCustom(string sqlFrom, string sqlCondition, string sqlFieldOrderBy)
        {
            return CreateCommandWithOrderByCustom(sqlFrom, sqlCondition, sqlFieldOrderBy, true);
        }

        /// <summary>
        /// CreateCommandWithOrderBy
        /// </summary>
        /// <param name="sqlFields"></param>
        /// <param name="sqlFrom"></param>
        /// <param name="sqlCondition"></param>
        /// <param name="sqlFieldOrderBy"></param>
        /// <param name="sortAscending"></param>
        /// <returns></returns>
        public static StringBuilder CreateCommandWithOrderBy(string sqlFields, string sqlFrom, string sqlCondition, string sqlFieldOrderBy, bool? sortAscending)
        {
            return CreateCommandWithOrderBy(sqlFields, sqlFrom, sqlCondition, sqlFieldOrderBy, sortAscending, false);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sqlFields"></param>
        /// <param name="sqlFrom"></param>
        /// <param name="sqlCondition"></param>
        /// <param name="sqlFieldOrderBy"></param>
        /// <param name="sortAscending"></param>
        /// <param name="isReadUnCommited"></param>
        /// <returns></returns>
        public static StringBuilder CreateCommandWithOrderBy(string sqlFields, string sqlFrom, string sqlCondition, string sqlFieldOrderBy, bool? sortAscending, bool isReadUnCommited)
        {
            if (string.IsNullOrEmpty(sqlFields))
            {
                throw new ArgumentNullException("sqlFields");
            }

            if (string.IsNullOrEmpty(sqlFrom))
            {
                throw new ArgumentNullException("sqlFrom");
            }

            if (string.IsNullOrEmpty(sqlCondition))
            {
                throw new ArgumentNullException("sqlCondition");
            }

            if (string.IsNullOrEmpty(sqlFieldOrderBy))
            {
                throw new ArgumentNullException("sqlFieldOrderBy");
            }

            var sqlBuilder = new StringBuilder();
            var sqlSort = sortAscending.HasValue ? (sortAscending.Value ? "ASC" : "DESC") : string.Empty;

            if (isReadUnCommited)
            {
                sqlBuilder.AppendFormat(@"SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED; ");
            }

            // Sanitize the SQL command
            //sqlFields = SearchQuerySanitizer.SanitizeSQL(sqlFields, true);
            //sqlFrom = SearchQuerySanitizer.SanitizeSQL(sqlFrom, true);
            //sqlCondition = SearchQuerySanitizer.SanitizeSQL(sqlCondition, false);
            //sqlFieldOrderBy = SearchQuerySanitizer.SanitizeSQL(sqlFieldOrderBy, false);
            //sqlSort = SearchQuerySanitizer.SanitizeSQL(sqlSort, false);

            sqlBuilder.AppendFormat(@"SELECT {0} FROM {1} WHERE {2} ORDER BY {3} {4} ", sqlFields, sqlFrom, sqlCondition, sqlFieldOrderBy, sqlSort);

            return sqlBuilder;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sqlFields"></param>
        /// <param name="sqlFrom"></param>
        /// <param name="sqlCondition"></param>
        /// <param name="sqlFieldOrderBy"></param>
        /// <param name="sortAscending"></param>
        /// <param name="isReadUnCommited"></param>
        /// <returns></returns>
        public static StringBuilder CreateCommandWithOrderByCustom(string sqlFrom, string sqlCondition, string sqlFieldOrderBy, bool? sortAscending)
        {     

            if (string.IsNullOrEmpty(sqlFrom))
            {
                throw new ArgumentNullException("sqlFrom");
            }

            if (string.IsNullOrEmpty(sqlCondition))
            {
                throw new ArgumentNullException("sqlCondition");
            }

            if (string.IsNullOrEmpty(sqlFieldOrderBy))
            {
                throw new ArgumentNullException("sqlFieldOrderBy");
            }

            var sqlBuilder = new StringBuilder();
            var sqlSort = sortAscending.HasValue ? (sortAscending.Value ? "ASC" : "DESC") : string.Empty;


            // Sanitize the SQL command
            //sqlFields = SearchQuerySanitizer.SanitizeSQL(sqlFields, true);
            //sqlFrom = SearchQuerySanitizer.SanitizeSQL(sqlFrom, true);
            //sqlCondition = SearchQuerySanitizer.SanitizeSQL(sqlCondition, false);
            //sqlFieldOrderBy = SearchQuerySanitizer.SanitizeSQL(sqlFieldOrderBy, false);
            //sqlSort = SearchQuerySanitizer.SanitizeSQL(sqlSort, false);

            sqlBuilder.AppendFormat(@"{0} WHERE {1} ORDER BY {2} {3} ", sqlFrom, sqlCondition, sqlFieldOrderBy, sqlSort);

            return sqlBuilder;
        }

        /// <summary>
        /// Create sql condition command
        /// </summary>
        /// <param name="filterList">
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public static string CreateCondition(this IEnumerable<SearchFilterView> filterList)
        {
            if (filterList == null) return " (1=1) ";

            var sqlBuilder = new StringBuilder();
            var searchFilterViews = filterList as SearchFilterView[] ?? filterList.ToArray();

            if (!searchFilterViews.Any())
            {
                return " (1=1) ";
            }

            const string collate = " COLLATE Thai_BIN";
            const string arg4 = @" {0} {1} {2} @{3} ";
            const string arg5 = @" {0} {1}.{2} {3} @{4} ";
            const string argDirect4 = @" {0} {1} {2} {3} ";
            const string argDirect5 = @" {0} {1}.{2} {3} {4} ";

            searchFilterViews.ForEach(p =>
            {
                if (p.ListSearchFilterViews == null)
                {
                    if (string.IsNullOrEmpty(p.TableName))
                    {
                        if (p.OperatorType == SearchOperatorType.Is)
                        {
                            sqlBuilder.AppendFormat(argDirect4, sqlBuilder.Length == 0 ? string.Empty : p.ConditionType.GetAttributeCode(), p.FieldName, p.OperatorType.GetAttributeCode(), p.Value);
                        }
                        else
                        {
                            sqlBuilder.AppendFormat(arg4, sqlBuilder.Length == 0 ? string.Empty : p.ConditionType.GetAttributeCode(), p.FieldName, p.OperatorType.GetAttributeCode(), p.ParameterName);
                        }
                    }
                    else
                    {
                        if (p.OperatorType == SearchOperatorType.Is)
                        {
                            sqlBuilder.AppendFormat(argDirect5, sqlBuilder.Length == 0 ? string.Empty : p.ConditionType.GetAttributeCode(), p.TableName, p.FieldName, p.OperatorType.GetAttributeCode(), p.Value);
                        }
                        else
                        {
                            if (p.Value != null)
                            {
                                if (p.Value.GetType().ToString() == "System.String" && p.OperatorType == SearchOperatorType.Like)
                                {
                                    sqlBuilder.AppendFormat(arg5, sqlBuilder.Length == 0 ? string.Empty : p.ConditionType.GetAttributeCode(), "Lower(" + p.TableName, p.FieldName + ")", p.OperatorType.GetAttributeCode(), p.ParameterName + collate);
                                }
                                else
                                {
                                    sqlBuilder.AppendFormat(arg5, sqlBuilder.Length == 0 ? string.Empty : p.ConditionType.GetAttributeCode(), p.TableName, p.FieldName, p.OperatorType.GetAttributeCode(), p.ParameterName);
                                }
                            }
                            else
                            {
                                sqlBuilder.AppendFormat(arg5, sqlBuilder.Length == 0 ? string.Empty : p.ConditionType.GetAttributeCode(), p.TableName, p.FieldName, p.OperatorType.GetAttributeCode(), p.ParameterName);
                            }
                        }
                    }
                }
                else
                {
                    var sqlSubBuilder = new StringBuilder();

                    // ListSearchFilterViews (Level 1)
                    p.ListSearchFilterViews.ForEach(lf =>
                    {
                        if (lf.ListSearchFilterViews == null)
                        {
                            if (string.IsNullOrEmpty(lf.TableName))
                            {
                                if (lf.OperatorType == SearchOperatorType.Is)
                                {
                                    sqlSubBuilder.AppendFormat(argDirect4, sqlSubBuilder.Length == 0 ? string.Empty : lf.ConditionType.GetAttributeCode(), lf.FieldName, lf.OperatorType.GetAttributeCode(), lf.Value);
                                }
                                else
                                {
                                    sqlSubBuilder.AppendFormat(arg4, sqlSubBuilder.Length == 0 ? string.Empty : lf.ConditionType.GetAttributeCode(), lf.FieldName, lf.OperatorType.GetAttributeCode(), lf.ParameterName);
                                }
                            }
                            else
                            {
                                if (lf.OperatorType == SearchOperatorType.Is)
                                {
                                    sqlSubBuilder.AppendFormat(argDirect5, sqlSubBuilder.Length == 0 ? string.Empty : lf.ConditionType.GetAttributeCode(), lf.TableName, lf.FieldName, lf.OperatorType.GetAttributeCode(), lf.Value);
                                }
                                else
                                {
                                    sqlSubBuilder.AppendFormat(arg5, sqlSubBuilder.Length == 0 ? string.Empty : lf.ConditionType.GetAttributeCode(), lf.TableName, lf.FieldName, lf.OperatorType.GetAttributeCode(), lf.ParameterName);
                                }
                            }
                        }
                        else
                        {
                            var sqlSubBuilder2 = new StringBuilder();

                            // ListSearchFilterViews (Level 2)
                            lf.ListSearchFilterViews.ForEach(lv2 =>
                            {
                                if (string.IsNullOrEmpty(lv2.TableName))
                                {
                                    if (lv2.OperatorType == SearchOperatorType.Is)
                                    {
                                        sqlSubBuilder2.AppendFormat(argDirect4, sqlSubBuilder2.Length == 0 ? string.Empty : lv2.ConditionType.GetAttributeCode(), lv2.FieldName, lv2.OperatorType.GetAttributeCode(), lv2.Value);
                                    }
                                    else
                                    {
                                        sqlSubBuilder2.AppendFormat(arg4, sqlSubBuilder2.Length == 0 ? string.Empty : lv2.ConditionType.GetAttributeCode(), lv2.FieldName, lv2.OperatorType.GetAttributeCode(), lv2.ParameterName);
                                    }
                                }
                                else
                                {
                                    if (lv2.OperatorType == SearchOperatorType.Is)
                                    {
                                        sqlSubBuilder2.AppendFormat(argDirect5, sqlSubBuilder2.Length == 0 ? string.Empty : lv2.ConditionType.GetAttributeCode(), lv2.TableName, lv2.FieldName, lv2.OperatorType.GetAttributeCode(), lv2.Value);
                                    }
                                    else
                                    {
                                        sqlSubBuilder2.AppendFormat(arg5, sqlSubBuilder2.Length == 0 ? string.Empty : lv2.ConditionType.GetAttributeCode(), lv2.TableName, lv2.FieldName, lv2.OperatorType.GetAttributeCode(), lv2.ParameterName);
                                    }
                                }
                            });
                            sqlSubBuilder.AppendFormat(" {0} ({1})", sqlSubBuilder.Length == 0 ? string.Empty : lf.ConditionType.GetAttributeCode(), sqlSubBuilder2);
                        }
                    });

                    sqlBuilder.AppendFormat(" {0} ({1})", sqlBuilder.Length == 0 ? string.Empty : p.ConditionType.GetAttributeCode(), sqlSubBuilder);
                }
            });

            return sqlBuilder.ToString();
        }

        /// <summary>
        /// Create sql command with filter list
        /// </summary>
        /// <param name="filterList">
        /// </param>
        /// <returns>
        /// The <see cref="DynamicParameters"/>.
        /// </returns>
        public static DynamicParameters CreateConditionParams(this IEnumerable<SearchFilterView> filterList)
        {
            var parameters = new DynamicParameters();

            if (filterList == null) return parameters;

            filterList.ForEach(p =>
            {
                if (p.ListSearchFilterViews == null)
                {
                    switch (p.OperatorType)
                    {
                        case SearchOperatorType.Like:
                            switch (p.LikePosition)
                            {
                                case SearchLikePosition.BeginWords:
                                    parameters.Add(p.ParameterName, "%" + p.Value.ToString().ToLower());
                                    break;
                                case SearchLikePosition.EndWords:
                                    parameters.Add(p.ParameterName, p.Value.ToString().ToLower() + "%");
                                    break;
                                default:
                                    parameters.Add(p.ParameterName, "%" + p.Value.ToString().ToLower() + "%");
                                    break;
                            }
                            break;
                        case SearchOperatorType.NotIn:
                        case SearchOperatorType.In:
                            parameters.Add(p.ParameterName, p.ListValues);
                            break;
                        default:
                            if ((p.Value != null)
                                // ReSharper disable once OperatorIsCanBeUsed
                                && (p.Value.GetType() == typeof(DateTime)
                                // ReSharper disable once OperatorIsCanBeUsed
                                || p.Value.GetType() == typeof(DateTime?)))
                            {
                                var dateConvert = DateTimeHelper.ConvertDateParameterToString(p.Value);

                                // Append time 23:59:59 to date string when operator is <=
                                switch (p.OperatorType)
                                {
                                    case SearchOperatorType.LessEqual:
                                        dateConvert = string.Format("{0} 23:59:59", dateConvert);
                                        parameters.Add(p.ParameterName, dateConvert);
                                        break;
                                    case SearchOperatorType.LessOrEqualWithOwnTime:
                                        parameters.Add(p.ParameterName, p.Value);
                                        break;
                                    default:
                                        parameters.Add(p.ParameterName, dateConvert);
                                        break;
                                }
                            }
                            else if ((p.Value != null)
                                && p.Value.ToString().Equals("(bool)NULL", StringComparison.InvariantCultureIgnoreCase))
                            {
                                p.Value = "NULL";
                            }
                            else if ((p.Value != null)
                                && p.Value.ToString().Equals("(datetime)NULL", StringComparison.InvariantCultureIgnoreCase))
                            {
                                p.Value = "NULL";
                            }
                            else if ((p.Value != null)
                                && p.Value.ToString().Equals("(int)NULL", StringComparison.InvariantCultureIgnoreCase))
                            {
                                p.Value = "NULL";
                            }
                            else if ((p.Value != null)
                                && p.Value.ToString().Equals("(bool)NOTNULL", StringComparison.InvariantCultureIgnoreCase))
                            {
                                p.Value = "NOT NULL";
                            }
                            else if ((p.Value != null)
                                && p.Value.ToString().Equals("(datetime)NOTNULL", StringComparison.InvariantCultureIgnoreCase))
                            {
                                p.Value = "NOT NULL";
                            }
                            else if ((p.Value != null)
                                && p.Value.ToString().Equals("(int)NOTNULL", StringComparison.InvariantCultureIgnoreCase))
                            {
                                p.Value = "NOT NULL";
                            }
                            else
                            {
                                parameters.Add(p.ParameterName, p.Value);
                            }
                            break;
                    }
                }
                else
                {
                    parameters.AddDynamicParams(p.ListSearchFilterViews.CreateConditionParams());
                }
            });

            return parameters;
        }

        /// <summary>
        /// The serialize to json.
        /// </summary>
        /// <param name="list">
        /// The list.
        /// </param>
        /// <typeparam name="T">
        /// </typeparam>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public static string SerializeToJson<T>(List<T> list)
        {
            return JsonConvert.SerializeObject(list);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="data"></param>
        /// <returns></returns>
        public static string SerializeToJson<T>(T data)
        {
            return JsonConvert.SerializeObject(data);
        }
    }
}