﻿using System;
using System.Collections.Generic;
using System.Linq;
using PNG.DataAccess.Database;
using PNG.DataAccess.Interfaces;
using PNG.Model;
using PNG.Dapper.Extensions;
using PNG.Model.Models;
using Dapper;

namespace PNG.DataAccess.Services
{
    /// <summary>
    /// 
    /// </summary>
    public class MenuServiceDAL : DapperRepository<TestConnection>, IMenuService
    {
        /// <summary>
        ///     The _db factory.
        /// </summary>
        protected readonly IDbFactory DbFactory;


        public MenuServiceDAL() : this(new DbFactory()) { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dbFactory"></param>
        public MenuServiceDAL(IDbFactory dbFactory)
            : base(dbFactory.Connection)
        {
            DbFactory = dbFactory;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="appID"></param>
        /// <param name="enterpriseId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public List<Module_Tree_Role_Relation> GetAllModules(string appID, string enterpriseID, string userid, string moduleId)
        {
            List<Module_Tree_Role_Relation> result  = new List<Module_Tree_Role_Relation>();
            try
            {
                var param = new DynamicParameters();
                param.Add("@appId", appID);
                param.Add("@enterpriseId", enterpriseID);
                //param.Add("@userId", user.userid);
                param.Add("@userId", userid);
                param.Add("@moduleId", moduleId);
                result =  this.Exec<Module_Tree_Role_Relation>("GetAllModules", param).ToList();                
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return result;
        }

    }
}
