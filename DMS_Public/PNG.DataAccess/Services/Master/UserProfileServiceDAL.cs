﻿using System;
using System.Linq;
using PNG.DataAccess.Database;
using PNG.DataAccess.Interfaces;
using PNG.Model;
using PNG.Dapper.Extensions;
using PNG.Model.Models;
using System.Text;
using Dapper;
using System.Data;
using System.Globalization;
using PNG.Common.Services.Tracing;
using PNG.Common.Services.Logging;
using System.Collections.Generic;

namespace PNG.DataAccess.Services
{
    /// <summary>
    /// 
    /// </summary>
    public class UserProfileServiceDAL : DapperRepository<User_Master>, IUserProfileService
    {
        /// <summary>
        ///     The _db factory.
        /// </summary>
        protected readonly IDbFactory DbFactory;


        public UserProfileServiceDAL() : this(new DbFactory()) { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dbFactory"></param>
        public UserProfileServiceDAL(IDbFactory dbFactory)
            : base(dbFactory.Connection)
        {
            DbFactory = dbFactory;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public bool Edit(User_Master entity)
        {
            if (null == entity)
            {
                throw new ArgumentNullException("entity");
            }

            try
            {
                Tracer.TraceIn();
                return base.Update(entity);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                Tracer.TraceError(ex);
                throw;
            }
            finally
            {
                Tracer.TraceOut();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="fieldToUpdate"></param>
        /// <returns></returns>
        public bool Edit(User_Master entity, List<string> fieldToUpdate)
        {
            bool result = false;
            Tracer.TraceIn();
            try
            {
                result = this.Update(entity, fieldToUpdate);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                Tracer.TraceError(ex);
                throw;
            }
            finally
            {
                Tracer.TraceOut(result);
            }
            return result;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="userName"></param>
        /// <returns></returns>
        public User_Master GetUser(string userId)
        {
            try
            {
                Tracer.TraceIn();
                var sqlCommand = new StringBuilder(@"SELECT * FROM dbo.User_Master WHERE userId = @userId");

                var param = new { userId = userId };
                return this.DbFactory.Connection.Query<User_Master>(sqlCommand.ToString(), param).FirstOrDefault();
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                Tracer.TraceError(ex);
                throw;
            }
            finally
            {
                Tracer.TraceOut();
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public List<User_Master> GetUsers(string userName)
        {
            try
            {
                Tracer.TraceIn();
                var sqlCommand = new StringBuilder(@"SELECT * FROM dbo.User_Master WHERE user_name = @user_name");

                var param = new { user_name = userName };
                return this.DbFactory.Connection.Query<User_Master>(sqlCommand.ToString(), param).ToList();
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                Tracer.TraceError(ex);
                throw;
            }
            finally
            {
                Tracer.TraceOut();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public bool UpdateProfile(User_Master user)
        {
            try
            {
                Tracer.TraceIn();
                var sqlCommand = new StringBuilder(@"Update dbo.User_Master SET email = @email, department_name = @department_name, user_name = @user_name Where userid = @userId");

                var param = new { userId = user.userid, email = user.email, department_name = user.department_name, user_name = user.user_name };
                return this.DbFactory.Connection.Query<bool>(sqlCommand.ToString(), param).FirstOrDefault();
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                Tracer.TraceError(ex);
                throw;
            }
            finally
            {
                Tracer.TraceOut();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public bool UpdatePassword(User_Master user)
        {
            try
            {
                Tracer.TraceIn();
                var sqlCommand = new StringBuilder(@"Update dbo.User_Master SET user_password = @user_password Where userid = @userId");

                var param = new { userId = user.userid, user_password = user.user_password };
                return this.DbFactory.Connection.Query<bool>(sqlCommand.ToString(), param).FirstOrDefault();
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                Tracer.TraceError(ex);
                throw;
            }
            finally
            {
                Tracer.TraceOut();
            }
        }


    }
}
