﻿using Dapper;
using PNG.Dapper.Extensions;
using PNG.DataAccess.Database;
using PNG.DataAccess.Interfaces;
using PNG.Model;
using PNG.Model.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PNG.DataAccess.Services
{
    public class CreateConsignmentServiceDAL : DapperRepository<TestConnection>, ICreateConsignmentService
    {
        protected readonly IDbFactory DbFactory;

        public CreateConsignmentServiceDAL() : this(new DbFactory()) { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dbFactory"></param>
        public CreateConsignmentServiceDAL(IDbFactory dbFactory)
            : base(dbFactory.Connection)
        {
            DbFactory = dbFactory;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="EnterpriseID"></param>
        /// <param name="UserID"></param>
        /// <param name="ConsgnNo"></param>
        /// <param name="Action"></param>
        /// <param name="PayerID"></param>
        /// <returns></returns>
        public CreateConsignmentResult GetCreateConsignmentStoreProcedure(string enterpriseID, CreateConsignmentParam createconsignParam)
        {
            var result = new CreateConsignmentResult();

            try
            {
                var param = new DynamicParameters();

                param.Add("@action", createconsignParam.action);
                param.Add("@ForceDelete", createconsignParam.ForceDelete);
                param.Add("@enterpriseId", enterpriseID); /* createconsignParam.enterpriseid */
                param.Add("@userloggedin", createconsignParam.userloggedin); 
                param.Add("@OverrideSenderName", createconsignParam.OverrideSenderName);
                param.Add("@consignment_no", createconsignParam.consignment_no);
                param.Add("@payerid", createconsignParam.payerid);
                param.Add("@service_code", createconsignParam.service_code);
                param.Add("@ref_no", createconsignParam.ref_no);
                param.Add("@sender_name", createconsignParam.sender_name);
                param.Add("@sender_address1", createconsignParam.sender_address1);
                param.Add("@sender_address2", createconsignParam.sender_address2);
                param.Add("@sender_zipcode", createconsignParam.sender_zipcode);
                param.Add("@sender_telephone", createconsignParam.sender_telephone);
                param.Add("@sender_fax", createconsignParam.sender_fax);
                param.Add("@sender_contact_person", createconsignParam.sender_contact_person);
                param.Add("@sender_email", createconsignParam.sender_email);
                param.Add("@recipient_telephone", createconsignParam.recipient_telephone);
                param.Add("@recipient_name", createconsignParam.recipient_name);
                param.Add("@recipient_address1", createconsignParam.recipient_address1);
                param.Add("@recipient_address2", createconsignParam.recipient_address2);
                param.Add("@recipient_zipcode", createconsignParam.recipient_zipcode);
                param.Add("@recipient_fax", createconsignParam.recipient_fax);
                param.Add("@recipient_contact_person", createconsignParam.recipient_contact_person);
                param.Add("@declare_value", createconsignParam.declare_value);
                param.Add("@cod_amount", createconsignParam.cod_amount);
                param.Add("@remark", createconsignParam.remark);
                param.Add("@return_pod_slip", createconsignParam.return_pod_slip);
                param.Add("@return_invoice_hc", createconsignParam.return_invoice_hc);
                param.Add("@DangerousGoods", createconsignParam.DangerousGoods);
                param.Add("@ContractAccepted", createconsignParam.ContractAccepted);
                param.Add("@PackageDetails", createconsignParam.PackageDetails);  /*createconsignParam.PackageDetails*/
                param.Add("@GoodsDescription", createconsignParam.GoodsDescription);

                var reader = this.DbFactory.Connection.QueryMultiple("CSS_Consignment", param: new
                {
                    action = createconsignParam.action,
                    ForceDelete = createconsignParam.ForceDelete,
                    enterpriseid = enterpriseID, /* createconsignParam.enterpriseid */
                userloggedin = createconsignParam.userloggedin, 
                    OverrideSenderName = createconsignParam.OverrideSenderName,
                    consignment_no = createconsignParam.consignment_no,
                    payerid = createconsignParam.payerid,
                    service_code = createconsignParam.service_code,
                    ref_no = createconsignParam.ref_no,
                    sender_name = createconsignParam.sender_name,
                    sender_address1 = createconsignParam.sender_address1,
                    sender_address2 = createconsignParam.sender_address2,
                    sender_zipcode = createconsignParam.sender_zipcode,
                    sender_telephone = createconsignParam.sender_telephone,
                    sender_fax = createconsignParam.sender_fax,
                    sender_contact_person = createconsignParam.sender_contact_person,
                    sender_email = createconsignParam.sender_email,
                    recipient_telephone = createconsignParam.recipient_telephone,
                    recipient_name = createconsignParam.recipient_name,
                    recipient_address1 = createconsignParam.recipient_address1,
                    recipient_address2 = createconsignParam.recipient_address2,
                    recipient_zipcode = createconsignParam.recipient_zipcode,
                    recipient_fax = createconsignParam.recipient_fax,
                    recipient_contact_person = createconsignParam.recipient_contact_person,
                    declare_value = createconsignParam.declare_value,
                    cod_amount = createconsignParam.cod_amount,
                    remark = createconsignParam.remark,
                    return_pod_slip = createconsignParam.return_pod_slip,
                    return_invoice_hc = createconsignParam.return_invoice_hc,
                    DangerousGoods = createconsignParam.DangerousGoods,
                    ContractAccepted = createconsignParam.ContractAccepted,
                    PackageDetails = createconsignParam.PackageDetails,
                    GoodsDescription = createconsignParam.GoodsDescription
                }, commandType: CommandType.StoredProcedure);

                var createConsignmentError = reader.Read<CreateConsignmentError>();
                var createConsignmentSenderDetail = reader.Read<CreateConsignmentSenderDetail>().ToList();
                var createConsignmentPackageDetail = reader.Read<CreateConsignmentPackageDetail>();

                result.CreateConsignmentError = createConsignmentError;
                result.CreateConsignmentSenderDetail = createConsignmentSenderDetail;
                result.CreateConsignmentPackageDetail = createConsignmentPackageDetail;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return result;
        }

        public CreateConsignmentUpdateReference UpdateReferenceStoreProcedure(string applicationid, string enterpriseID, CreateConsignmentParam createconsignParam)
        {
            var result = new CreateConsignmentUpdateReference();

            try
            {
                var param = new DynamicParameters();
                
                param.Add("@applicationid", applicationid);
                param.Add("@enterpriseId", enterpriseID); /* createconsignParam.enterpriseid */
                param.Add("@userloggedin", createconsignParam.userloggedin);
                param.Add("@telephone", createconsignParam.recipient_telephone);
                param.Add("@reference_name", createconsignParam.recipient_name);
                param.Add("@address1", createconsignParam.recipient_address1);
                param.Add("@address2", createconsignParam.recipient_address2);
                param.Add("@zipcode", createconsignParam.recipient_zipcode);
                param.Add("@fax", createconsignParam.recipient_fax);
                param.Add("@contactperson", createconsignParam.recipient_contact_person);
                param.Add("@cost_centre", createconsignParam.cost_centre);
               

                var reader = this.DbFactory.Connection.QueryMultiple("updateReferences", param: new
                {
                    applicationid = applicationid,
                    enterpriseId = enterpriseID,
                    userloggedin = createconsignParam.userloggedin,
                    telephone = createconsignParam.recipient_telephone,
                    reference_name = createconsignParam.recipient_name,
                    address1 = createconsignParam.recipient_address1,
                    address2 = createconsignParam.recipient_address2,
                    zipcode = createconsignParam.recipient_zipcode,
                    fax = createconsignParam.recipient_fax,
                    contactperson = createconsignParam.recipient_contact_person,
                    cost_centre = createconsignParam.cost_centre
                }, commandType: CommandType.StoredProcedure);

                var updateData = reader.Read<CreateConsignmentUpdateReference>();
              

                result = updateData.FirstOrDefault();
                
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return result;
        }

        public string QueryPlayerId(string appID, string enterpriseId, string userId)
        {
            string result;
            try
            {
                //var sqlCommand = new StringBuilder(@"	select [custid] from dbo.CSS_EnterpriseUserAccounts
                //           where [enterpriseid] = @enterpriseId and [userid] = @userId");

                var sqlCommand = new StringBuilder(@"SELECT payerid FROM dbo.CustomerAccount2(@enterpriseId, @userId)");

                var param = new { userId = userId, enterpriseId = enterpriseId };
                result = this.DbFactory.Connection.Query<string>(sqlCommand.ToString(), param).FirstOrDefault();

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return result;
        }

        public List<CreateConsignmentQuerySndRecName> QuerySndRecName(string enterpriseid, string userloggedin, string payerid)
        {
            var result = new List<CreateConsignmentQuerySndRecName>();
            try
            {
                var sqlCommand = new StringBuilder(@"SELECT snd_rec_name FROM dbo.GetSenderNames(@enterpriseid, @userloggedin,@payerid )");

                var param = new { enterpriseid = enterpriseid, userloggedin = userloggedin, payerid = payerid };
                result = this.DbFactory.Connection.Query<CreateConsignmentQuerySndRecName>(sqlCommand.ToString(), param).ToList();

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return result;
        }

        public string QuerySndRecNameTwo(string enterpriseid, string userloggedin, string payerid)
        {
            string result;
            try
            {
                var sqlCommand = new StringBuilder(@"SELECT snd_rec_name FROM dbo.GetSenderNames(@enterpriseid, @userloggedin,@payerid ) Order by snd_rec_name");

                var param = new { enterpriseid = enterpriseid, userloggedin = userloggedin, payerid = payerid };
                result = this.DbFactory.Connection.Query<string>(sqlCommand.ToString(), param).FirstOrDefault();

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return result;
        }


        public CreateConsignmentBindSenderDetail GetBindSenderDetails(string applicationId, string enterpriseId, string payerId, string sndRecName)
        {
            var result = new CreateConsignmentBindSenderDetail();
            try
            {
                var sqlCommand = new StringBuilder(@"SELECT snd_rec_name,snd_rec_type,contact_person,email,address1,address2,country,zipcode,telephone,fax 
                FROM dbo.Customer_Snd_Rec WHERE applicationid = @applicationid AND enterpriseid = @enterpriseid AND custid = @custid AND snd_rec_name = @snd_rec_name
	            AND snd_rec_type IN ('S', 'B')  Order by snd_rec_name");

                var param = new { applicationid = applicationId, enterpriseid = enterpriseId, custid = payerId, snd_rec_name = sndRecName };

                result = this.DbFactory.Connection.Query<CreateConsignmentBindSenderDetail>(sqlCommand.ToString(), param).FirstOrDefault();

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return result;
        }

        public CreateConsignmentCustomerAccount GetCustomerAccount(string enterpriseId, string userloggedin)
        {
            var result = new CreateConsignmentCustomerAccount();
            try
            {
                var sqlCommand = new StringBuilder(@"SELECT * FROM dbo.CustomerAccount2(@enterpriseid, @userloggedin)");

                var param = new { enterpriseid = enterpriseId, userloggedin = userloggedin };

                result = this.DbFactory.Connection.Query<CreateConsignmentCustomerAccount>(sqlCommand.ToString(), param).FirstOrDefault();

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return result;
        }

        public bool GetUserAssignedRoles(string strEnterpriseID, string userId, string roleId)
        {
            bool result = false;
            var value = new CreateConsignmentUserAssignRole();
            try
            {
                var sqlCommand = new StringBuilder(@"SELECT [user_name], IsAssigned = MAX(IsAssigned)
	                                                FROM dbo.UserAssignedRoles(@enterpriseid, @userId , @roleId )
	                                                GROUP BY [user_name]  ");

                var param = new { enterpriseid = strEnterpriseID, userId = userId, roleId = roleId };
                value = this.DbFactory.Connection.Query<CreateConsignmentUserAssignRole>(sqlCommand.ToString(), param).FirstOrDefault();
                if (value.IsAssigned == 1)
                {
                    result = true;
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return result;
        }

        public List<CreateConsignmentServiceCode> GetServiceCode(string applicationId, string enterpriseId)
        {
            var result = new List<CreateConsignmentServiceCode>();
            try
            {
                var sqlCommand = new StringBuilder(@"SELECT service_code FROM dbo.[Service] WHERE applicationid = @applicationid AND  enterpriseid = @enterpriseid  ORDER BY service_charge_amt");

                var param = new { applicationid = applicationId, enterpriseid = enterpriseId };

                result = this.DbFactory.Connection.Query<CreateConsignmentServiceCode>(sqlCommand.ToString(), param).ToList();

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return result;
        }

        public CreateConsignmentCustomsWarehousesName GetCustomsWarehousesName(string applicationId, string enterpriseId, string selectedValue)
        {
            var result = new CreateConsignmentCustomsWarehousesName();
            try
            {
                var sqlCommand = new StringBuilder(@"SELECT applicationid, enterpriseid, warehouse_name, contact_person, email ,
	                                                address1, address2, country, state_code, zipcode, telephone, fax 
	                                                FROM dbo.Customs_warehouses 
	                                                WHERE applicationid = @applicationId AND enterpriseid = @enterpriseid 
	                                                AND warehouse_name = @selectedValue
	                                                Order by warehouse_name");

                var param = new { applicationId = applicationId, enterpriseid = enterpriseId, selectedValue = selectedValue };

                result = this.DbFactory.Connection.Query<CreateConsignmentCustomsWarehousesName>(sqlCommand.ToString(), param).FirstOrDefault();

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return result;
        }

        public CreateConsignmentStateName GetStateName(string applicationId, string enterpriseId, string zipcode)
        {
            var result = new CreateConsignmentStateName();
            try
            {
                var sqlCommand = new StringBuilder(@"SELECT a.zipcode, s.state_name,b.Country FROM dbo.Zipcode a 
	                                JOIN dbo.Enterprise b ON a.applicationid = b.applicationid AND a.enterpriseid = b.enterpriseid AND a.Country = b.Country 
	                                Join dbo.State  s ON a.state_code = s.state_code 
	                                WHERE a.applicationid = @applicationId AND a.enterpriseid =  @enterpriseid AND a.zipcode = @zipcode ");

                var param = new { applicationId = applicationId, enterpriseid = enterpriseId, zipcode = zipcode };

                result = this.DbFactory.Connection.Query<CreateConsignmentStateName>(sqlCommand.ToString(), param).FirstOrDefault();

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return result;
        }

        public CreateConsignmentBindRecipienDetail getRecipientDetailByTelephone(string applicationId, string enterpriseId, string telephone)
        {
            var result = new CreateConsignmentBindRecipienDetail();
           
            try
            {
                var sqlCommand = new StringBuilder(@"SELECT telephone, reference_name, address1, address2, zipcode, fax, contactperson, cost_centre 
	                                                FROM [References] 
	                                                WHERE applicationid= @applicationId
	                                                AND enterpriseid = @enterpriseId
	                                                AND telephone = @telephone");

                var param = new { applicationId = applicationId, enterpriseid = enterpriseId, telephone = telephone };

                result = this.DbFactory.Connection.Query<CreateConsignmentBindRecipienDetail>(sqlCommand.ToString(), param).FirstOrDefault();

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return result;
        }

        public CreateConsignmentBindRecipienDetail getRecipientDetailByCostCentre(string applicationId, string enterpriseId, string costcentre)
        {
            var result = new CreateConsignmentBindRecipienDetail();

            try
            {
                var sqlCommand = new StringBuilder(@"SELECT telephone, reference_name, address1, address2, zipcode, fax, contactperson, cost_centre 
	                                                FROM [References] 
	                                                WHERE applicationid= @applicationId
	                                                AND enterpriseid = @enterpriseId
	                                                AND cost_centre = @costcentre");

                var param = new { applicationId = applicationId, enterpriseid = enterpriseId, costcentre = costcentre };

                result = this.DbFactory.Connection.Query<CreateConsignmentBindRecipienDetail>(sqlCommand.ToString(), param).FirstOrDefault();

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return result;
        }

        public Customer getCustomerAcc2(string applicationId, string enterpriseId, string payerId)
        {
            var result = new Customer();

            try
            {
                var sqlCommand = new StringBuilder(@"SELECT * FROM customer a
	                                                WHERE a.applicationid = @applicationId
	                                                AND a.enterpriseid = @enterpriseid
	                                                AND a.custid = @payerId ");

                var param = new { applicationId = applicationId, enterpriseid = enterpriseId, payerId = payerId };

                result = this.DbFactory.Connection.Query<Customer>(sqlCommand.ToString(), param).FirstOrDefault();

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return result;
        }

        public List<CreateConsignmentPackageLimit> getPackageLimit(string applicationId, string enterpriseId, string payerId)
        {
            var result = new List<CreateConsignmentPackageLimit>();

            try
            {
                var sqlCommand = new StringBuilder(@"select [key], [value] from dbo.EnterpriseConfigurations(@enterpriseid,'PackageDetailsLimits') 
	                                                UNION ALL 
	                                                SELECT 'DensityFactor', ISNULL(b.density_factor, a.density_factor)  
	                                                FROM dbo.Enterprise a  JOIN dbo.Customer b  ON a.applicationid = b.applicationid AND a.enterpriseid = b.enterpriseid  
	                                                WHERE a.applicationid = @applicationId AND a.enterpriseid = @enterpriseid AND  b.custid = @payerId  ");

                var param = new { applicationId = applicationId, enterpriseid = enterpriseId, payerId = payerId };

                result = this.DbFactory.Connection.Query<CreateConsignmentPackageLimit>(sqlCommand.ToString(), param).ToList();

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return result;
        }


        public List<CreateConsignmentConfigurations> getConfigurations(string applicationId, string enterpriseId, string payerId)
        {
            var result = new List<CreateConsignmentConfigurations>();

            try
            {
                var sqlCommand = new StringBuilder(@"WITH EnterpriseConfigs AS(    
	            SELECT [key], [value]    
	            FROM dbo.EnterpriseConfigurations(@enterpriseid, 'CreateUpdateConsignment'))	 
	            SELECT [key], [value] FROM EnterpriseConfigs 
	            UNION ALL SELECT a.[key] ,[value]=CASE WHEN b.[Value] = 'N' THEN '0' ELSE a.[Value] END 
	            FROM (    SELECT [key]='MaxDeclaredValue',[value]=CAST(ISNULL(b.insurance_maximum_amt-b.free_insurance_amt ,a.max_insurance_amt-a.free_insurance_amt) 
                    AS VARCHAR(10)), [key2]='InsSupportedbyEnterprise'    
	            FROM dbo.Enterprise a    LEFT JOIN dbo.Customer b        
	            ON a.applicationid = b.applicationid AND a.enterpriseid = b.enterpriseid AND custid = @payerId   
	            WHERE a.applicationid = @applicationId AND a.enterpriseid = @enterpriseid
                UNION ALL 
	            SELECT 'MaxCODAmount', '999999', 'CODSupportedbyEnterprise') a JOIN EnterpriseConfigs b ON [key2] = b.[key];");

                var param = new { applicationId = applicationId, enterpriseid = enterpriseId, payerId = payerId };

                result = this.DbFactory.Connection.Query<CreateConsignmentConfigurations>(sqlCommand.ToString(), param).ToList();

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return result;
        }

        public CreateConsignmentEnterpriseContract getEnterpriseContract(string enterpriseId, string payerId, string userId)
        {
            var result = new CreateConsignmentEnterpriseContract();

            try
            {
                var sqlCommand = new StringBuilder(@"SELECT AcceptedCurrentContract,ContractText FROM dbo.EnterpriseContract(@enterpriseid, @payerId , @userId)");

                var param = new { enterpriseid = enterpriseId, payerId = payerId, userId = userId };

                result = this.DbFactory.Connection.Query<CreateConsignmentEnterpriseContract>(sqlCommand.ToString(), param).FirstOrDefault();

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return result;
        }

        public CreateConsignmentDefaultServiceCode getDefaultServiceCode(string applicationId, string enterpriseId, string payerId)
        {
            var result = new CreateConsignmentDefaultServiceCode();

            try
            {
                var sqlCommand = new StringBuilder(@"SELECT Default_Service_Code = ISNULL(Default_Service_Code, '') 
	                                                FROM dbo.Customer
	                                                WHERE applicationid = @applicationId
	                                                AND enterpriseid = @enterpriseid
	                                                AND custid = @payerId");

                var param = new { applicationId = applicationId, enterpriseid = enterpriseId, payerId = payerId};

                result = this.DbFactory.Connection.Query<CreateConsignmentDefaultServiceCode>(sqlCommand.ToString(), param).FirstOrDefault();

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return result;
        }







    }
}
