﻿using Dapper;
using PNG.Common.Services.Logging;
using PNG.Common.Services.Tracing;
using PNG.Dapper.Extensions;
using PNG.DataAccess.Database;
using PNG.DataAccess.Interfaces;
using PNG.Model;
using PNG.Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PNG.DataAccess.Services
{
    public class ShipmentTrackingServiceDAL : DapperRepository<TestConnection>, IShipmentTrackingService
    {

        protected readonly IDbFactory DbFactory;

        public ShipmentTrackingServiceDAL() : this(new DbFactory()) { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dbFactory"></param>
        public ShipmentTrackingServiceDAL(IDbFactory dbFactory)
            : base(dbFactory.Connection)
        {
            DbFactory = dbFactory;
        }

        public ShipmentTrackingUserDetail getUserDetail(string applicationId, string enterpriseId, string userId)
        {
            var result = new ShipmentTrackingUserDetail();
            try
            {
                var sqlCommand = new StringBuilder(@"SELECT userid, user_name, user_culture,email,department_name, User_Type, payerid, Location FROM User_Master 
                                                    WHERE enterpriseid = @enterpriseId AND applicationid = @applicationId AND userid = @userId");

                var param = new { enterpriseId = enterpriseId, applicationId = applicationId, userid = userId };
                result = this.DbFactory.Connection.Query<ShipmentTrackingUserDetail>(sqlCommand.ToString(), param).FirstOrDefault();

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return result;
        }

        public List<ShipmentTrackingCodeValue> getCodeValues(string applicationid, ShipmentTrackingParam shipmentParam)
        {
            var result = new List<ShipmentTrackingCodeValue>();
            try
            {
                var sqlCommand = new StringBuilder(@"SELECT sequence, code_text, code_str_value, code_num_value 
                                                   FROM Core_System_Code 
                                                   WHERE applicationid = @applicationid AND culture = @culture AND codeid = @codeid ORDER BY sequence");

                var param = new { applicationid = applicationid, culture = shipmentParam.user_culture, codeid = shipmentParam.codeid };
                result = this.DbFactory.Connection.Query<ShipmentTrackingCodeValue>(sqlCommand.ToString(), param).ToList();

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return result;
        }

        public List<ShipmentTrackingAllRoles> getAllRoles(string applicationId, string enterpriseId, string userId)
        {
            var result = new List<ShipmentTrackingAllRoles>();
            try
            {
                var sqlCommand = new StringBuilder(@"  SELECT DISTINCT a.roleid,b.role_name FROM user_role_relation a, role_master b  
                                              WHERE a.applicationid = @applicationId AND a.enterpriseid = @enterpriseId	AND a.userid = @userId 
                                              AND b.roleid = a.roleid  AND b.role_name != 'system' 
                                              AND b.applicationid = a.applicationid AND b.enterpriseid = a.enterpriseid");

                var param = new { applicationId = applicationId, enterpriseId = enterpriseId, userId = userId };
                result = this.DbFactory.Connection.Query<ShipmentTrackingAllRoles>(sqlCommand.ToString(), param).ToList();

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return result;
        }
        public List<ShipmentTrackingPathCodeQuery> getPathCodeQuery(string applicationId, string enterpriseId)
        {
            var result = new List<ShipmentTrackingPathCodeQuery>();
            try
            {
                var sqlCommand = new StringBuilder(@"SELECT path_code AS DbComboText, path_code AS DbComboValue FROM dbo.Route_Code
                                                    WHERE applicationID = @applicationId AND EnterpriseID = @enterpriseId   ORDER BY path_code");

                var param = new { applicationId = applicationId, enterpriseId = enterpriseId };
                result = this.DbFactory.Connection.Query<ShipmentTrackingPathCodeQuery>(sqlCommand.ToString(), param).ToList();

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return result;
        }

        public List<ShipmentTrackingPathCodeQuery> getDistributionCenterQuery(string applicationId, string enterpriseId)
        {
            var result = new List<ShipmentTrackingPathCodeQuery>();
            try
            {
                var sqlCommand = new StringBuilder(@"SELECT origin_code AS DbComboText, origin_code AS DbComboValue 
                                                   FROM Distribution_center  
                                                   WHERE applicationID = @applicationId and EnterpriseID = @enterpriseId  ORDER BY origin_code");

                var param = new { applicationId = applicationId, enterpriseId = enterpriseId };
                result = this.DbFactory.Connection.Query<ShipmentTrackingPathCodeQuery>(sqlCommand.ToString(), param).ToList();

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return result;
        }

        public List<ShipmentTrackingPathCodeQuery> getDistributionCenterQuery(string applicationId, string enterpriseId, string userLocationId)
        {
            var result = new List<ShipmentTrackingPathCodeQuery>();
            try
            {
                var sqlCommand = new StringBuilder(@"SELECT origin_code AS DbComboText, origin_code AS DbComboValue 
                                                   FROM Distribution_center WHERE applicationID = @applicationId 
                                                   and EnterpriseID = @enterpriseId and origin_code = @userLocationId  ORDER BY origin_code");

                var param = new { applicationId = applicationId, enterpriseId = enterpriseId, userLocationId = userLocationId };
                result = this.DbFactory.Connection.Query<ShipmentTrackingPathCodeQuery>(sqlCommand.ToString(), param).ToList();

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return result;
        }

        public SearchResultView<LovState> SearchWithCriteria(SearchQueryParameters parameters)
        {
            Tracer.TraceIn();
            var result = new SearchResultView<LovState>();

            try
            {
                var sqlCommand = new StringBuilder();

                string sqlFields = @"State.*";

                string sqlFrom = @"State";

                DynamicParameters sqlParam = parameters.FilterList.CreateConditionParams();

                string sqlCondition = parameters.FilterList.CreateCondition();
                
                var commandCount = SearchQueryHelper.CreateCommandCount(sqlFrom, sqlCondition, true, null);

                // Append command with all fields and order by
                if (!string.IsNullOrEmpty(parameters.SortColumn))
                {
                    sqlCommand.Append(SearchQueryHelper.CreateCommandWithOrderBy(sqlFields, sqlFrom, sqlCondition,
                        parameters.SortColumn, parameters.SortAscending));
                }
                else
                {
                    sqlCommand.Append(SearchQueryHelper.CreateCommand(sqlFields, sqlFrom, sqlCondition));
                }

                // Append command offset
                sqlCommand.Append(SearchQueryHelper.CreateCommandOffset(parameters.ItemsPerPage, parameters.Page));

                var list = this.DbFactory.Connection.Query<LovState>(sqlCommand.ToString(), sqlParam).Select(c => c);
                var count = this.DbFactory.Connection.Query<int>(commandCount.ToString(), sqlParam).FirstOrDefault();

                result.SearchResults = list.AsQueryable();
                result.TotalRecord = count;
                result.RecordCount = count;
        
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                Tracer.TraceError(ex);
                throw;
            }
            finally
            {
                Tracer.TraceOut(result);
            }

            return result;
        }


        public SearchResultView<LovStatusCode> SearchWithStatusCode(SearchQueryParameters parameters, string applicationId, string enterpriseId)
        {
            Tracer.TraceIn();
            var result = new SearchResultView<LovStatusCode>();

            try
            {

                var sqlCommand = new StringBuilder();

                string sqlFields = @"status_code,status_description,Status_type,status_close,invoiceable";

                string sqlFrom = @"(
                  select distinct status_code,status_description,Status_type,status_close,invoiceable,system_code,tt_display_status,tt_display_remarks,auto_process,auto_location,auto_remark,tt_display_location, scanning_type ,AllowRoles, isnull(AllowInSWB_Emu,'N') as AllowInSWB_Emu,
                    isnull(AllowUPD_ByBatch,'N') as AllowUPD_ByBatch from Status_Code
                  inner join  ( SELECT   distinct  Role_Master.role_name fROM  User_Role_Relation INNER JOIN Role_Master ON User_Role_Relation.applicationid = Role_Master.applicationid AND User_Role_Relation.enterpriseid = Role_Master.enterpriseid AND
                  User_Role_Relation.roleid = Role_Master.roleid WHERE  (User_Role_Relation.userid = @userId )) a on   status_code.allowRoles like @role 
                  where status_code.applicationid = @applicationId and status_code.enterpriseid = @enterpriseId
                  union
                  select status_code,status_description,Status_type,status_close,invoiceable,
                  system_code,tt_display_status,tt_display_remarks,auto_process,auto_location,auto_remark,
                  tt_display_location, scanning_type ,AllowRoles, isnull(AllowInSWB_Emu,'N') as AllowInSWB_Emu,
                  isnull(AllowUPD_ByBatch,'N') as AllowUPD_ByBatch from Status_Code where (AllowRoles is null or Rtrim(Ltrim(AllowRoles))='') 
                  ) StatusCode";


                var useridParam = parameters.FilterList.Where(w => w.ParameterName == "puserid").FirstOrDefault().Value;
                var allowRolesParam = parameters.FilterList.Where(w => w.ParameterName == "pallowRoles").FirstOrDefault().Value;

                object statusCodeParam = null;
                object statusCodeDesParam = null;
                object statusCloseParam = null;
                object statusTypeParam = null;
                object invoiceableParam = null;

                if (useridParam != null)
                {
                    parameters.FilterList = parameters.FilterList.Where(w => w.ParameterName != "puserid");
                    //tradeDate = parameters.FilterList.FirstOrDefault(w => w.ParameterName == "puserid").Value;
                }
                if (allowRolesParam != null)
                {
                    parameters.FilterList = parameters.FilterList.Where(w => w.ParameterName != "pallowRoles");
                }


                if (parameters.FilterList.Any(w => w.ParameterName == "pstatus_code"))
                {
                    statusCodeParam = parameters.FilterList.Where(w => w.ParameterName == "pstatus_code").FirstOrDefault().Value;
                }

                if (parameters.FilterList.Any(w => w.ParameterName == "pstatus_description"))
                {
                    statusCodeDesParam = parameters.FilterList.Where(w => w.ParameterName == "pstatus_description").FirstOrDefault().Value;
                }

                if (parameters.FilterList.Any(w => w.ParameterName == "pstatus_close"))
                {
                    statusCloseParam = parameters.FilterList.Where(w => w.ParameterName == "pstatus_close").FirstOrDefault().Value;
                }

                if (parameters.FilterList.Any(w => w.ParameterName == "pstatus_type"))
                {
                    statusTypeParam = parameters.FilterList.Where(w => w.ParameterName == "pstatus_type").FirstOrDefault().ListValues;
                }

                if (parameters.FilterList.Any(w => w.ParameterName == "pinvoiceable"))
                {
                    invoiceableParam = parameters.FilterList.Where(w => w.ParameterName == "pinvoiceable").FirstOrDefault().Value;
                }
                
                DynamicParameters sqlParam = parameters.FilterList.CreateConditionParams();

                string sqlCondition = parameters.FilterList.CreateCondition();

                var commandCount = SearchQueryHelper.CreateCommandCount(sqlFrom, sqlCondition, true, null);

                // Append command with all fields and order by
                if (!string.IsNullOrEmpty(parameters.SortColumn))
                {
                    sqlCommand.Append(SearchQueryHelper.CreateCommandWithOrderBy(sqlFields, sqlFrom, sqlCondition,
                        parameters.SortColumn, parameters.SortAscending));
                }
                else
                {
                    sqlCommand.Append(SearchQueryHelper.CreateCommand(sqlFields, sqlFrom, sqlCondition));
                }

                // Append command offset
                sqlCommand.Append(SearchQueryHelper.CreateCommandOffset(parameters.ItemsPerPage, parameters.Page));

                //var param = new DynamicParameters();
                // param = new { applicationId = applicationId, enterpriseId = enterpriseId, userId = useridParam, role = allowRolesParam };
                sqlParam.Add("@applicationId", applicationId);
                sqlParam.Add("@enterpriseId", enterpriseId);
                sqlParam.Add("@userId", useridParam);
                sqlParam.Add("@role", allowRolesParam);
                //sqlParam.Add("@pstatus_code", statusCodeParam);

                var list = this.DbFactory.Connection.Query<LovStatusCode>(sqlCommand.ToString(), sqlParam).ToList();
                var count = this.DbFactory.Connection.Query<int>(commandCount.ToString(), sqlParam).FirstOrDefault();

                result.SearchResults = list.AsQueryable();
                result.TotalRecord = count;
                result.RecordCount = count;
                
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                Tracer.TraceError(ex);
                throw;
            }
            finally
            {
                Tracer.TraceOut(result);
            }

            return result;
        }

        public SearchResultView<LovExeptionCode> SearchWithExceptionCode(SearchQueryParameters parameters, string applicationId, string enterpriseId)
        {
            Tracer.TraceIn();
            var result = new SearchResultView<LovExeptionCode>();

            try
            {
                var sqlCommand = new StringBuilder();

                string sqlFields = @"Exception_Code.status_code, Exception_Code.exception_code, Exception_Code.exception_description, Exception_Code.mbg,
                                        Exception_Code.status_close, Exception_Code.invoiceable, status_code.status_description";

                string sqlFrom = @" Exception_Code inner join status_code  ON status_code.applicationid = Exception_Code.applicationid
                                    and status_code.enterpriseid = Exception_Code.enterpriseid
                                    and status_code.status_code = Exception_Code.status_code
                                    and Exception_Code.applicationid= @applicationId
                                    and Exception_Code.enterpriseid = @enterpriseId and Exception_Code.active_st = 'Y'";


                //var useridParam = parameters.FilterList.Where(w => w.ParameterName == "puserid").FirstOrDefault().Value;
                //var allowRolesParam = parameters.FilterList.Where(w => w.ParameterName == "pallowRoles").FirstOrDefault().Value;

                object statusCodeParam = null;
                object exception_code = null;
                object mbg = null;
                object status_close = null;
                object invoiceable = null;

                //if (useridParam != null)
                //{
                //    parameters.FilterList = parameters.FilterList.Where(w => w.ParameterName != "puserid");
                //    //tradeDate = parameters.FilterList.FirstOrDefault(w => w.ParameterName == "puserid").Value;
                //}
                //if (allowRolesParam != null)
                //{
                //    parameters.FilterList = parameters.FilterList.Where(w => w.ParameterName != "pallowRoles");
                //}


                if (parameters.FilterList.Any(w => w.ParameterName == "pstatus_code"))
                {
                    statusCodeParam = parameters.FilterList.Where(w => w.ParameterName == "pstatus_code").FirstOrDefault().Value;
                }

                if (parameters.FilterList.Any(w => w.ParameterName == "pexception_code"))
                {
                    exception_code = parameters.FilterList.Where(w => w.ParameterName == "pexception_code").FirstOrDefault().Value;
                }

                if (parameters.FilterList.Any(w => w.ParameterName == "pmbg"))
                {
                    mbg = parameters.FilterList.Where(w => w.ParameterName == "pmbg").FirstOrDefault().Value;
                }

                if (parameters.FilterList.Any(w => w.ParameterName == "pstatus_close"))
                {
                    status_close = parameters.FilterList.Where(w => w.ParameterName == "pstatus_close").FirstOrDefault().ListValues;
                }

                if (parameters.FilterList.Any(w => w.ParameterName == "pinvoiceable"))
                {
                    invoiceable = parameters.FilterList.Where(w => w.ParameterName == "pinvoiceable").FirstOrDefault().Value;
                }

                DynamicParameters sqlParam = parameters.FilterList.CreateConditionParams();

                string sqlCondition = parameters.FilterList.CreateCondition();

                var commandCount = SearchQueryHelper.CreateCommandCount(sqlFrom, sqlCondition, true, null);

                // Append command with all fields and order by
                if (!string.IsNullOrEmpty(parameters.SortColumn))
                {
                    sqlCommand.Append(SearchQueryHelper.CreateCommandWithOrderBy(sqlFields, sqlFrom, sqlCondition,
                        parameters.SortColumn, parameters.SortAscending));
                }
                else
                {
                    sqlCommand.Append(SearchQueryHelper.CreateCommand(sqlFields, sqlFrom, sqlCondition));
                }

                // Append command offset
                sqlCommand.Append(SearchQueryHelper.CreateCommandOffset(parameters.ItemsPerPage, parameters.Page));
                
                sqlParam.Add("@applicationId", applicationId);
                sqlParam.Add("@enterpriseId", enterpriseId);

                var list = this.DbFactory.Connection.Query<LovExeptionCode>(sqlCommand.ToString(), sqlParam).ToList();
                var count = this.DbFactory.Connection.Query<int>(commandCount.ToString(), sqlParam).FirstOrDefault();

                result.SearchResults = list.AsQueryable();
                result.TotalRecord = count;
                result.RecordCount = count;

            }
            catch (Exception ex)
            {
                Log.Error(ex);
                Tracer.TraceError(ex);
                throw;
            }
            finally
            {
                Tracer.TraceOut(result);
            }

            return result;
        }

        public ShipmentTrackingDeliveryPath getDeliveryPath(string applicationId, string enterpriseId, ShipmentTrackingDeliveryPathParam paramPath)
        {
            var result = new ShipmentTrackingDeliveryPath();
            try
            {
                var sqlCommand = new StringBuilder(@"select * from Delivery_Path where applicationid = @applicationId and enterpriseid = @enterpriseId and path_code = @pathCode");

                var param = new { enterpriseId = enterpriseId, applicationId = applicationId, pathCode = paramPath.path_code };
                result = this.DbFactory.Connection.Query<ShipmentTrackingDeliveryPath>(sqlCommand.ToString(), param).FirstOrDefault();

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return result;
        }
    }
}
