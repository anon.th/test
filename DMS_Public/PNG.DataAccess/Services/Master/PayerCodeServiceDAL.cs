﻿using Dapper;
using PNG.Common.Services.Logging;
using PNG.Common.Services.Tracing;
using PNG.Dapper.Extensions;
using PNG.DataAccess.Database;
using PNG.DataAccess.Interfaces;
using PNG.Model;
using PNG.Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PNG.DataAccess.Services
{
    public class PayerCodeServiceDAL : DapperRepository<Customer>, IPayerCodeService
    {
        protected readonly IDbFactory DbFactory;


        public PayerCodeServiceDAL() : this(new DbFactory()) { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dbFactory"></param>
        public PayerCodeServiceDAL(IDbFactory dbFactory)
            : base(dbFactory.Connection)
        {
            DbFactory = dbFactory;
        }

        public SearchResultView<Customer> SearchWithCriteria(SearchQueryParameters parameters)
        {
            Tracer.TraceIn();
            var result = new SearchResultView<Customer>();

            try
            {
                var sqlCommand = new StringBuilder();

                string sqlFields = @"customer.*";

                string sqlFrom = @"customer";

                DynamicParameters sqlParam = parameters.FilterList.CreateConditionParams();

                string sqlCondition = parameters.FilterList.CreateCondition();

                // Append command count
                //if (parameters.ItemsPerPage.HasValue)
                //{
                //   var commandCount = SearchQueryHelper.CreateCommandCount(sqlFrom, sqlCondition, true, "customer.cust_name");
                //}
                var commandCount = SearchQueryHelper.CreateCommandCount(sqlFrom, sqlCondition, true, "customer.cust_name");

                // Append command with all fields and order by
                if (!string.IsNullOrEmpty(parameters.SortColumn))
                {
                    sqlCommand.Append(SearchQueryHelper.CreateCommandWithOrderBy(sqlFields, sqlFrom, sqlCondition,
                        parameters.SortColumn, parameters.SortAscending));
                }
                else
                {
                    sqlCommand.Append(SearchQueryHelper.CreateCommand(sqlFields, sqlFrom, sqlCondition));
                }

                // Append command offset
                sqlCommand.Append(SearchQueryHelper.CreateCommandOffset(parameters.ItemsPerPage, parameters.Page));

                var list = this.DbFactory.Connection.Query<Customer>(sqlCommand.ToString(), sqlParam).Select(c => c);
                var count = this.DbFactory.Connection.Query<int>(commandCount.ToString(), sqlParam).FirstOrDefault();

                result.SearchResults = list.AsQueryable();
                result.TotalRecord = count;
                result.RecordCount = count;
                //using (SqlMapper.GridReader multi = DbFactory.Connection.QueryMultiple(sqlCommand.ToString(), sqlParam))
                //{
                //    int count = 0;
                //    if (parameters.ItemsPerPage.HasValue)
                //    {
                //        count = multi.Read<int>().FirstOrDefault();
                //    }
                //    IEnumerable<Customer> resultList =
                //        multi.Read<Customer, Enterprise, State, string, Customer>(
                //            (Customer, enterprise, state, state_name) =>
                //            {
                //                //if (state != null)
                //                //{
                //                //    Customer.State = state;
                //                //}
                //                //if (state_name != null)
                //                //{
                //                //    Customer.state_name = state_name;
                //                //}

                //                //if (lockFund != null && lockFund.LockResign != null)
                //                //    fund.IsLockFund = lockFund.LockResign.Value;
                //                //else fund.IsLockFund = false;

                //                return Customer;
                //            },
                //            "EnterpriseId,StateId, StateName");

                //    result.SearchResults = resultList.AsQueryable();
                //    result.TotalRecord = count;
                //    result.RecordCount = resultList.Count();
                //}
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                Tracer.TraceError(ex);
                throw;
            }
            finally
            {
                Tracer.TraceOut(result);
            }

            return result;
        }
    }
}
