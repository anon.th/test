﻿using Dapper;
using PNG.Dapper.Extensions;
using PNG.DataAccess.Database;
using PNG.DataAccess.Interfaces;
using PNG.Model;
using PNG.Model.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PNG.DataAccess.Services
{
    public class SearchConsignmentServiceDAL : DapperRepository<TestConnection>, ISearchConsignmentService
    {
        /// </summary>
        protected readonly IDbFactory DbFactory;


        public SearchConsignmentServiceDAL() : this(new DbFactory()) { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dbFactory"></param>
        public SearchConsignmentServiceDAL(IDbFactory dbFactory)
            : base(dbFactory.Connection)
        {
            DbFactory = dbFactory;
        }

        public SearchConsignmentStatusPrinting getStatusPrinting(string enterpriseId)
        {
            var result = new SearchConsignmentStatusPrinting();
            try
            {
                var sqlCommand = new StringBuilder(@"SELECT * from dbo.EnterpriseConfigurations(@enterpriseid,'ConsignmentStatusPrinting')");

                var param = new { enterpriseid = enterpriseId};

                result = this.DbFactory.Connection.Query<SearchConsignmentStatusPrinting>(sqlCommand.ToString(), param).FirstOrDefault();

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return result;
        }

        public List<CreateConsignmentQuerySndRecName> QuerySndRecName(string enterpriseid, string userloggedin, string payerid)
        {
            var result = new List<CreateConsignmentQuerySndRecName>();
            try
            {
                var sqlCommand = new StringBuilder(@"SELECT snd_rec_name FROM dbo.GetSenderNames(@enterpriseid, @userloggedin,@payerid )  Order by snd_rec_name");

                var param = new { enterpriseid = enterpriseid, userloggedin = userloggedin, payerid = payerid };
                result = this.DbFactory.Connection.Query<CreateConsignmentQuerySndRecName>(sqlCommand.ToString(), param).ToList();

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return result;
        }

        public string getEnterpriseUserAccountsDataSet(string appID, string enterpriseId, string userId)
        {
            string result;
            try
            {
                //var sqlCommand = new StringBuilder(@"	select [custid] from dbo.CSS_EnterpriseUserAccounts
                //           where [enterpriseid] = @enterpriseId and [userid] = @userId");
                var sqlCommand = new StringBuilder(@"SELECT payerid FROM dbo.CustomerAccount2(@enterpriseId, @userId)");

                var param = new { userId = userId , enterpriseId  = enterpriseId };
                result = this.DbFactory.Connection.Query<string>(sqlCommand.ToString(), param).FirstOrDefault();

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return result;
        }

        public List<CreateConsignmentCustomsWarehousesName> getCustomsWarehousesName(string applicationId, string enterpriseId, string selectedValue)
        {
            var result = new List<CreateConsignmentCustomsWarehousesName>();
            try
            {
                var sqlCommand = new StringBuilder(@"");
                if (selectedValue != null && selectedValue!= "")
                {
                    sqlCommand = new StringBuilder(@"SELECT applicationid, enterpriseid, warehouse_name, contact_person, email ,
	                                                address1, address2, country, state_code, zipcode, telephone, fax 
	                                                FROM dbo.Customs_warehouses 
	                                                WHERE applicationid = @applicationId AND enterpriseid = @enterpriseid 
	                                                AND warehouse_name = @selectedValue
	                                                Order by warehouse_name");
                }
                else
                {      
                    sqlCommand = new StringBuilder(@"SELECT applicationid, enterpriseid, warehouse_name, contact_person, email ,
	                                                address1, address2, country, state_code, zipcode, telephone, fax 
	                                                FROM dbo.Customs_warehouses 
	                                                WHERE applicationid = @applicationId AND enterpriseid = @enterpriseid 	                                               
	                                                Order by warehouse_name");
                }
                

                var param = new { applicationId = applicationId, enterpriseid = enterpriseId, selectedValue = selectedValue };

                result = this.DbFactory.Connection.Query<CreateConsignmentCustomsWarehousesName>(sqlCommand.ToString(), param).ToList();

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return result;
        }

        public string getEnterpriseUserAccounts(string enterpriseId, string userId)
        {
            string result;
            try
            {
                //var sqlCommand = new StringBuilder(@"select [custid] from dbo.CSS_EnterpriseUserAccounts
                //                                 where [enterpriseid] = @enterpriseId and [userid] = @userId");
                var sqlCommand = new StringBuilder(@"SELECT payerid FROM dbo.CustomerAccount2(@enterpriseId, @userId)");

                var param = new { enterpriseId = enterpriseId, userId = userId };
                result = this.DbFactory.Connection.Query<string>(sqlCommand.ToString(), param).FirstOrDefault();

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return result;
        }

        public List<SearchConsignmentCustomStatus> getCustomStatus(string applicationId, string enterpriseId)
        {
            var result = new List<SearchConsignmentCustomStatus>();
            try
            {
                var sqlCommand = new StringBuilder(@"SELECT DisplayStatus=code_num_value, StatusID=sequence
                                                    FROM dbo.Enterprise_Configurations
                                                    WHERE applicationid = @applicationId AND enterpriseid =  @enterpriseid
                                                    AND codeid IN ('CustomerStatus', 'CustomsStatus') AND sequence NOT IN (192, 193)
                                                    ORDER BY CASE sequence WHEN 0 THEN 999 ELSE sequence END");               

                var param = new { applicationId = applicationId, enterpriseid = enterpriseId};

                result = this.DbFactory.Connection.Query<SearchConsignmentCustomStatus>(sqlCommand.ToString(), param).ToList();

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return result;
        }

        public List<SearchConsignmentCustomStatus> getCustomerStatus(string applicationId, string enterpriseId)
        {
            var result = new List<SearchConsignmentCustomStatus>();
            try
            {
                var sqlCommand = new StringBuilder(@"SELECT DisplayStatus=code_num_value, StatusID=sequence
                                                    FROM dbo.Enterprise_Configurations 
                                                    WHERE applicationid = @applicationId AND enterpriseid =  @enterpriseid
                                                   AND codeid = 'CustomerStatus' ORDER BY CASE sequence WHEN 0 THEN 999 ELSE sequence END");

                var param = new { applicationId = applicationId, enterpriseid = enterpriseId };

                result = this.DbFactory.Connection.Query<SearchConsignmentCustomStatus>(sqlCommand.ToString(), param).ToList();

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return result;
        }

        public SearchConsignmentResult CSS_ConsignmentStatus(string enterpriseID, SearchConsignmentParam searchconsignParam)
        {
            var result = new SearchConsignmentResult();

            try
            {
                var param = new DynamicParameters();
               
                param.Add("@enterpriseid", enterpriseID);
                param.Add("@userloggedin", searchconsignParam.userloggedin); /* searchconsignParam.enterpriseid */
                if (searchconsignParam.SenderName != null)
                {
                    param.Add("@SenderName", searchconsignParam.SenderName);
                }
                if (searchconsignParam.payerid != null)
                {
                    param.Add("@payerid", searchconsignParam.payerid);
                }
                if (searchconsignParam.MasterAWBNumber != null)
                {
                    param.Add("@MasterAWBNumber", searchconsignParam.MasterAWBNumber);
                }
                if (searchconsignParam.JobEntryNo != null)
                {
                    param.Add("@JobEntryNo", searchconsignParam.JobEntryNo);
                }
                if (searchconsignParam.consignment_no != null)
                {
                    param.Add("@consignment_no", searchconsignParam.consignment_no);
                }
                if (searchconsignParam.status_id != null)
                {
                    param.Add("@status_id", searchconsignParam.status_id);
                }
                if (searchconsignParam.ShippingList_No != null)
                {
                    param.Add("@ShippingList_No", searchconsignParam.ShippingList_No);
                }
                if (searchconsignParam.recipient_telephone != null)
                {
                    param.Add("@recipient_telephone", searchconsignParam.recipient_telephone);
                }
                if (searchconsignParam.ref_no != null)
                {
                    param.Add("@ref_no", searchconsignParam.ref_no);
                }
                if (searchconsignParam.recipient_zipcode != null)
                {
                    param.Add("@recipient_zipcode", searchconsignParam.recipient_zipcode);
                }
                if (searchconsignParam.service_code != null)
                {
                    param.Add("@service_code", searchconsignParam.service_code);
                }
                if (searchconsignParam.datefrom != null)
                {
                    param.Add("@datefrom", searchconsignParam.datefrom);
                }
                if (searchconsignParam.dateto != null)
                {
                    param.Add("@dateto", searchconsignParam.dateto);
                }
                if (searchconsignParam.pageCount != null)
                {
                    param.Add("@pageCount", searchconsignParam.pageCount);
                }
                if (searchconsignParam.pageNumber != null)
                {
                    param.Add("@pageNumber", searchconsignParam.pageNumber);
                }

                //param.Add("@status_id", searchconsignParam.status_id != null ? searchconsignParam.status_id : null);                
                //param.Add("@ShippingList_No", searchconsignParam.ShippingList_No);
                //param.Add("@recipient_telephone", searchconsignParam.recipient_telephone);
                //param.Add("@ref_no", searchconsignParam.ref_no);
                //param.Add("@recipient_zipcode", searchconsignParam.recipient_zipcode);
                //param.Add("@service_code", searchconsignParam.service_code);
                //param.Add("@datefrom", searchconsignParam.datefrom != null ? searchconsignParam.datefrom : null);
                //param.Add("@dateto", searchconsignParam.dateto != null ? searchconsignParam.dateto : null);
                //param.Add("@pageCount", searchconsignParam.pageCount);
               // param.Add("@pageNumber", searchconsignParam.pageNumber);


                var reader = this.DbFactory.Connection.QueryMultiple("CSS_ConsignmentStatusSearch", param: new
                {
                    enterpriseid = enterpriseID,
                    userloggedin = searchconsignParam.userloggedin,
                    SenderName = searchconsignParam.SenderName,
                    payerid = searchconsignParam.payerid,
                    MasterAWBNumber = searchconsignParam.MasterAWBNumber,
                    JobEntryNo = searchconsignParam.JobEntryNo,
                    consignment_no = searchconsignParam.consignment_no,
                    status_id = searchconsignParam.status_id,
                    ShippingList_No = searchconsignParam.ShippingList_No,
                    recipient_telephone = searchconsignParam.recipient_telephone,
                    ref_no = searchconsignParam.ref_no,
                    recipient_zipcode = searchconsignParam.recipient_zipcode,
                    service_code = searchconsignParam.service_code,
                    datefrom = searchconsignParam.datefrom,
                    dateto = searchconsignParam.dateto,
                    pageCount = searchconsignParam.pageCount,
                    pageNumber = searchconsignParam.pageNumber
                }, commandType: CommandType.StoredProcedure);

                var searchConsignmentError = reader.Read<SearchConsignmentError>();
                var searchConsignmentDetail = reader.Read<SearchConsignmentDetail>().ToList();
                var searchConsignmentCount = reader.Read<int>().FirstOrDefault();

                result.SearchConsignmentError = searchConsignmentError;
                result.SearchConsignmentDetail = searchConsignmentDetail;
                result.CountData = searchConsignmentCount;

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return result;
        }


        public SearchConsignmentReprintConsNoteResult CSS_ReprintConsNote(string enterpriseID, ReprintConsNoteParam reprintConsNoteParam)
        {
            var result = new SearchConsignmentReprintConsNoteResult();

            try
            {
                var param = new DynamicParameters();

                param.Add("@enterpriseid", enterpriseID);
                param.Add("@userloggedin", reprintConsNoteParam.userloggedin);
                param.Add("@ConsignmentsList", reprintConsNoteParam.consignmentsList);
                param.Add("@DataSource", reprintConsNoteParam.dataSource);
                if (reprintConsNoteParam.bookingNo > 0)
                {
                    param.Add("@BookingNo", reprintConsNoteParam.bookingNo);
                }           
                
                var reader = this.DbFactory.Connection.QueryMultiple("CSS_ReprintConsNote", param: new
                {
                    enterpriseid = enterpriseID,
                    userloggedin = reprintConsNoteParam.userloggedin,
                    ConsignmentsList = reprintConsNoteParam.consignmentsList,
                    DataSource = reprintConsNoteParam.dataSource,
                    BookingNo = reprintConsNoteParam.bookingNo,

                }, commandType: CommandType.StoredProcedure);

                var reprintConsNoteError = reader.Read<ReprintConsNoteError>();
                var reprintConsNoteDetail = reader.Read<ReprintConsNoteDetail>().ToList();
                //var searchConsignmentCount = reader.Read<int>().FirstOrDefault();

                result.SearchConsignmentReprintConsNoteError = reprintConsNoteError;
                result.SearchConsignmentReprintConsNoteDetail = reprintConsNoteDetail;

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return result;
        }

        public SearchConsignmentPrintShippingListResult CSS_PrintShippingList(string enterpriseID, PrintShippingListParam printShippingListParam)
        {
            var result = new SearchConsignmentPrintShippingListResult();

            try
            {
                var param = new DynamicParameters();

                param.Add("@enterpriseid", enterpriseID);
                param.Add("@userloggedin", printShippingListParam.userloggedin);
                param.Add("@ConsignmentsList", printShippingListParam.consignmentsList);

                var reader = this.DbFactory.Connection.QueryMultiple("CSS_PrintShippingList", param: new
                {
                    enterpriseid = enterpriseID,
                    userloggedin = printShippingListParam.userloggedin,
                    ConsignmentsList = printShippingListParam.consignmentsList

                }, commandType: CommandType.StoredProcedure);

                var reprintShippingListError = reader.Read<PrintShippingListError>();
                var reprintShippingListDetail = reader.Read<PrintShippingListDetail>().ToList();
                //var searchConsignmentCount = reader.Read<int>().FirstOrDefault();

                result.SearchConsignmentPrintShippingListError = reprintShippingListError.FirstOrDefault();
                result.SearchConsignmentPrintShippingListDetail = reprintShippingListDetail;

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return result;
        }

        public SearchConsignmentPrintConsNoteResult CSS_PrintConsNote(string enterpriseID, PrintShippingListParam printConsNoteParam)
        {
            var result = new SearchConsignmentPrintConsNoteResult();

            try
            {
                var param = new DynamicParameters();

                param.Add("@enterpriseid", enterpriseID);
                param.Add("@userloggedin", printConsNoteParam.userloggedin);
                param.Add("@ConsignmentsList", printConsNoteParam.consignmentsList);

                var reader = this.DbFactory.Connection.QueryMultiple("CSS_PrintConsNotes", param: new
                {
                    enterpriseid = enterpriseID,
                    userloggedin = printConsNoteParam.userloggedin,
                    ConsignmentsList = printConsNoteParam.consignmentsList

                }, commandType: CommandType.StoredProcedure);

                var printConsNoteError = reader.Read<PrintConsNoteError>();
                var printConsNoteDetail = reader.Read<PrintConsNoteDetail>().ToList();

                result.SearchConsignmentPrintConsNoteError = printConsNoteError;
                result.SearchConsignmentPrintConsNoteDetail = printConsNoteDetail;

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return result;
        }

        public string getConnectionString(string appID, string enterpriseId)
        {
            string result;
            try
            {
                var sqlCommand = new StringBuilder(@"SELECT db_connection FROM Core_Enterprise WHERE applicationid = @appID and enterpriseid = @enterpriseId");

                var param = new { appID = appID, enterpriseId = enterpriseId };
                result = this.DbFactory.Connection.Query<string>(sqlCommand.ToString(), param).FirstOrDefault();

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return result;
        }
    }
}
