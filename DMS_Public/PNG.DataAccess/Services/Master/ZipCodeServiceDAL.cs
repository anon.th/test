﻿using System;
using System.Collections.Generic;
using System.Linq;
using PNG.DataAccess.Database;
using PNG.DataAccess.Interfaces;
using PNG.Model;
using PNG.Dapper.Extensions;
using PNG.Model.Models;
using Dapper;
using PNG.Common.Services.Tracing;
using System.Text;
using PNG.Common.Services.Logging;

namespace PNG.DataAccess.Services
{
    /// <summary>
    /// 
    /// </summary>
    public class ZipCodeServiceDAL : DapperRepository<Zipcode>, IZipCodeService
    {
        /// <summary>
        ///     The _db factory.
        /// </summary>
        protected readonly IDbFactory DbFactory;


        public ZipCodeServiceDAL() : this(new DbFactory()) { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dbFactory"></param>
        public ZipCodeServiceDAL(IDbFactory dbFactory)
            : base(dbFactory.Connection)
        {
            DbFactory = dbFactory;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public SearchResultView<Zipcode> SearchWithCriteria(SearchQueryParameters parameters)
        {
            Tracer.TraceIn();
            var result = new SearchResultView<Zipcode>();

            try
            {
                var sqlCommand = new StringBuilder();

                string sqlFields = @" Zipcode.*,
                                Enterprise.ZipCode AS EnterpriseId ,Enterprise.*,
                                State.Country AS StateId , State.*, State.state_name AS StateName";

                string sqlFrom = @"Zipcode
	                                INNER JOIN Enterprise ON Zipcode.applicationid = Enterprise.applicationid AND Zipcode.enterpriseid = Enterprise.enterpriseid AND ZipCode.Country = Enterprise.Country
		                            JOIN State ON Zipcode.applicationid = State.applicationid AND Zipcode.enterpriseid = State.enterpriseid AND Zipcode.Country = State.Country AND Zipcode.state_code = State.state_code   ";

                DynamicParameters sqlParam = parameters.FilterList.CreateConditionParams();

                string sqlCondition = parameters.FilterList.CreateCondition();

                // Append command count
                if (parameters.ItemsPerPage.HasValue)
                {
                    sqlCommand.Append(SearchQueryHelper.CreateCommandCount(sqlFrom, sqlCondition, true, "Zipcode.zipcode"));
                }

                // Append command with all fields and order by
                if (!string.IsNullOrEmpty(parameters.SortColumn))
                {
                    sqlCommand.Append(SearchQueryHelper.CreateCommandWithOrderBy(sqlFields, sqlFrom, sqlCondition,
                        parameters.SortColumn, parameters.SortAscending));
                }
                else
                {
                    sqlCommand.Append(SearchQueryHelper.CreateCommand(sqlFields, sqlFrom, sqlCondition));
                }

                // Append command offset
                sqlCommand.Append(SearchQueryHelper.CreateCommandOffset(parameters.ItemsPerPage, parameters.Page));

                using (SqlMapper.GridReader multi = DbFactory.Connection.QueryMultiple(sqlCommand.ToString(), sqlParam))
                {
                    int count = 0;
                    if (parameters.ItemsPerPage.HasValue)
                    {
                        count = multi.Read<int>().FirstOrDefault();
                    }
                    IEnumerable<Zipcode> resultList =
                        multi.Read<Zipcode, Enterprise, State, string , Zipcode>(
                            (zipcode, enterprise, state, state_name) =>
                            {
                                if (state != null)
                                {
                                    zipcode.State = state;
                                }
                                if (state_name != null)
                                {
                                    zipcode.state_name = state_name;
                                }

                                //if (lockFund != null && lockFund.LockResign != null)
                                //    fund.IsLockFund = lockFund.LockResign.Value;
                                //else fund.IsLockFund = false;

                                return zipcode;
                            },
                            "EnterpriseId,StateId, StateName");

                    result.SearchResults = resultList.AsQueryable();
                    result.TotalRecord = count;
                    result.RecordCount = resultList.Count();
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                Tracer.TraceError(ex);
                throw;
            }
            finally
            {
                Tracer.TraceOut(result);
            }

            return result;
        }

    }
}
