﻿using System;
using System.Collections.Generic;
using System.Linq;
using PNG.DataAccess.Database;
using PNG.DataAccess.Interfaces;
using PNG.Model;
using PNG.Dapper.Extensions;
using PNG.Model.Models;
using Dapper;
using PNG.Common.Services.Tracing;
using System.Text;
using PNG.Common.Services.Logging;

namespace PNG.DataAccess.Services
{
    /// <summary>
    /// 
    /// </summary>
    public class ReferencesServiceDAL : DapperRepository<Reference>, IReferenceService
    {
        /// <summary>
        ///     The _db factory.
        /// </summary>
        protected readonly IDbFactory DbFactory;


        public ReferencesServiceDAL() : this(new DbFactory()) { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dbFactory"></param>
        public ReferencesServiceDAL(IDbFactory dbFactory)
            : base(dbFactory.Connection)
        {
            DbFactory = dbFactory;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public SearchResultView<Reference> SearchWithCriteria(SearchQueryParameters parameters)
        {
            Tracer.TraceIn();
            var result = new SearchResultView<Reference>();

            try
            {
                var sqlCommand = new StringBuilder();

                string sqlFields = @" [References].*";

                string sqlFrom = @"[References] ";

                DynamicParameters sqlParam = parameters.FilterList.CreateConditionParams();

                string sqlCondition = parameters.FilterList.CreateCondition();

                // Append command count
                if (parameters.ItemsPerPage.HasValue)
                {
                    sqlCommand.Append(SearchQueryHelper.CreateCommandCount(sqlFrom, sqlCondition, true, "[References].telephone"));
                }

                // Append command with all fields and order by
                if (!string.IsNullOrEmpty(parameters.SortColumn))
                {
                    sqlCommand.Append(SearchQueryHelper.CreateCommandWithOrderBy(sqlFields, sqlFrom, sqlCondition,
                        parameters.SortColumn, parameters.SortAscending));
                }
                else
                {
                    sqlCommand.Append(SearchQueryHelper.CreateCommand(sqlFields, sqlFrom, sqlCondition));
                }

                // Append command offset
                sqlCommand.Append(SearchQueryHelper.CreateCommandOffset(parameters.ItemsPerPage, parameters.Page));

                using (SqlMapper.GridReader multi = DbFactory.Connection.QueryMultiple(sqlCommand.ToString(), sqlParam))
                {
                    int count = 0;
                    if (parameters.ItemsPerPage.HasValue)
                    {
                        count = multi.Read<int>().FirstOrDefault();
                    }
                    IEnumerable<Reference> resultList = multi.Read<Reference>().ToList();
                           

                    result.SearchResults = resultList.AsQueryable();
                    result.TotalRecord = count;
                    result.RecordCount = resultList.Count();
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                Tracer.TraceError(ex);
                throw;
            }
            finally
            {
                Tracer.TraceOut(result);
            }

            return result;
        }

    }
}
