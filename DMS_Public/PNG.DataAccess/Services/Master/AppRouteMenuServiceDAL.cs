﻿using Dapper;
using PNG.Common.Services.Logging;
using PNG.Common.Services.Tracing;
using PNG.Dapper.Extensions;
using PNG.DataAccess.Database;
using PNG.DataAccess.Interfaces;
using PNG.Model;
using PNG.Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PNG.DataAccess.Services
{
    public class AppRouteMenuServiceDAL : DapperRepository<AppRouteMenu>, IAppRouteMenuService
    {
        /// <summary>
        /// The _dbFactoory.
        /// </summary>
        protected readonly IDbFactory _dbFactory;

        /// <summary>
        /// 
        /// </summary>
        public AppRouteMenuServiceDAL() : this(new DbFactory()) { }

        /// <summary>
        /// Initializes a new instance of the <see cref="ExternalAppRouteMenuServiceDAL"/> class.
        /// </summary>
        /// <param name="dbFactory">The db factory.</param>
        public AppRouteMenuServiceDAL(IDbFactory dbFactory)
            : base(dbFactory.Connection)
        {
            this._dbFactory = dbFactory;
        }

        /// <summary>
        /// The search with criteria.
        /// </summary>
        /// <param name="parameters">The parameters.</param>
        /// <returns>The <see cref="SearchResultView"/>.</returns>
        public SearchResultView<AppRouteMenu> SearchWithCriteria(SearchQueryParameters parameters)
        {
            Tracer.TraceIn();
            var result = new SearchResultView<AppRouteMenu>();

            try
            {
                var sqlCommand = new StringBuilder();
                var sqlFields = @" AppRouteMenu.* ";
                //var sqlFrom = @"AppRouteMenu
                //            INNER JOIN Core_Module_Tree_Master Core_Module_Tree_Master ON (AppRouteMenu.MenuId = Core_Module_Tree_Master.applicationid) ";

                var sqlFrom = @"AppRouteMenu
                             ";
                var sqlParam = parameters.FilterList.CreateConditionParams();
                var sqlCondition = parameters.FilterList.CreateCondition();

                if (parameters.ItemsPerPage.HasValue)
                {
                    // Append command count
                    sqlCommand.Append(SearchQueryHelper.CreateCommandCount(sqlFrom, sqlCondition));
                }

                if (!string.IsNullOrEmpty(parameters.SortColumn))
                {
                    // Append command with all fields and order by
                    sqlCommand.Append(SearchQueryHelper.CreateCommandWithOrderBy(sqlFields, sqlFrom, sqlCondition,
                        parameters.SortColumn, parameters.SortAscending));
                }
                else
                {
                    sqlCommand.Append(SearchQueryHelper.CreateCommand(sqlFields, sqlFrom, sqlCondition));
                }

                // Append command offset
                sqlCommand.Append(SearchQueryHelper.CreateCommandOffset(parameters.ItemsPerPage, parameters.Page));
                using (var multi = this._dbFactory.Connection.QueryMultiple(sqlCommand.ToString(), sqlParam))
                {
                    int count = 0;
                    if (parameters.ItemsPerPage.HasValue)
                    {
                        count = multi.Read<int>().FirstOrDefault();
                    }

                    var resultList =
                        multi.Read<AppRouteMenu>();

                    result.SearchResults = resultList;
                    result.TotalRecord = count;
                    result.RecordCount = resultList.Count();
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                Tracer.TraceError(ex);
                throw;
            }
            finally
            {
                Tracer.TraceOut(result);
            }

            return result;
        }
    }
}
