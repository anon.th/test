﻿using System;
using System.Linq;
using PNG.DataAccess.Database;
using PNG.DataAccess.Interfaces;
using PNG.Model;
using PNG.Dapper.Extensions;
using System.Text;
using Dapper;
using PNG.Common.Services.Logging;
using System.Collections;
using System.Collections.Generic;

namespace PNG.DataAccess.Services
{
    /// <summary>
    /// 
    /// </summary>
    public class LoginServiceDAL : DapperRepository<TestConnection>, ILoginService
    {
        /// <summary>
        ///     The _db factory.
        /// </summary>
        protected readonly IDbFactory DbFactory;


        public LoginServiceDAL() : this(new DbFactory()) { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dbFactory"></param>
        public LoginServiceDAL(IDbFactory dbFactory)
            : base(dbFactory.Connection)
        {
            DbFactory = dbFactory;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="appID"></param>
        /// <param name="enterpriseId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public bool AuthenticateUser(string appID, string enterpriseID, string userID, string userPswd)
        {
            bool result = false;
            User_Master resultList = new User_Master();
            try
            {
                var sqlCommand = new StringBuilder(@"select * from user_master where applicationid = @appID And enterpriseid = @enterpriseID AND userid =  @userID AND  @userPswd = (select dbo.BlowFishDecode(user_password,dbo.BlowFishKey()))");

                var param = new { appID = appID, enterpriseID = enterpriseID, userID = userID, userPswd = userPswd };
                resultList = this.DbFactory.Connection.Query<User_Master>(sqlCommand.ToString(), param).FirstOrDefault();

                if (resultList != null)
                {
                    result = true;
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="appID"></param>
        /// <param name="enterpriseID"></param>
        /// <param name="userID"></param>
        /// <returns></returns>
        public User_Master GetUser(string appID, string enterpriseID, string userID)
        {
            try
            {
                User_Master user = null;

                var sqlCommand = new StringBuilder(
                    @"select * from user_master where  enterpriseid = @enterpriseID And applicationid = @appID And userid = @userID");

                var param = new { enterpriseID = enterpriseID, appID = appID, userID = userID };
                return user = this.DbFactory.Connection.Query<User_Master>(sqlCommand.ToString(), param)
                    .FirstOrDefault();
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="appID"></param>
        /// <param name="enterpriseID"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public List<Role_Master> GetAllRoles(string appID, string enterpriseID, User_Master user)
        {
            List<Role_Master> roleList = new List<Role_Master>();
           
            var sqlCommand = new StringBuilder(
                @"select distinct a.roleid,b.role_name from user_role_relation a,role_master b Where a.applicationid = @appID And a.enterpriseid = @enterpriseID And a.userid = @userId and b.roleid = a.roleid and b.role_name != 'system' and b.applicationid = a.applicationid and b.enterpriseid = a.enterpriseid");


            var param = new { appID = appID, enterpriseID = enterpriseID , userId = user.userid};
            roleList =  this.DbFactory.Connection.Query<Role_Master>(sqlCommand.ToString(), param).ToList();

            return roleList;
        }

    }
}
