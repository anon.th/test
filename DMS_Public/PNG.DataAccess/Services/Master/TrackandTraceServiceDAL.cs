﻿using System;
using System.Collections.Generic;
using System.Linq;
using PNG.DataAccess.Database;
using PNG.DataAccess.Interfaces;
using PNG.Model;
using PNG.Dapper.Extensions;
using PNG.Model.Models;
using System.Text;
using Dapper;
using System.Data;
using System.Data.SqlClient;
using PNG.Common.Helpers;
using System.Globalization;

namespace PNG.DataAccess.Services
{
    /// <summary>
    /// 
    /// </summary>
    public class TrackandTraceServiceDAL : DapperRepository<TestConnection>, ITrackandTraceService
    {
        /// <summary>
        ///     The _db factory.
        /// </summary>
        protected readonly IDbFactory DbFactory;


        public TrackandTraceServiceDAL() : this(new DbFactory()) { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dbFactory"></param>
        public TrackandTraceServiceDAL(IDbFactory dbFactory)
            : base(dbFactory.Connection)
        {
            DbFactory = dbFactory;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="AppID"></param>
        /// <param name="EnterpriseID"></param>
        /// <param name="ConsgnNo"></param>
        /// <param name="refNo"></param>
        /// <param name="PayerId"></param>
        /// <param name="ExpandPackageDetail"></param>
        /// <returns></returns>
        public TrackAndTraceResult SearchTrackandTrace(string AppID, string EnterpriseID, string ConsgnNo, string refNo, string PayerId, string ExpandPackageDetail)
        {
            var result = new TrackAndTraceResult();

            try
            {


                var param = new DynamicParameters();
                var a = DataConvertHelper.ToStr(ConsgnNo);
                var b = DataConvertHelper.ToStr(refNo);

                //if (ConsgnNo != "null")
                //{
                    param.Add("@enterpriseId", EnterpriseID);
                    param.Add("@payerid", PayerId);
                    param.Add("@consignment_no", ConsgnNo == null ? (object)DBNull.Value : ConsgnNo);
                    param.Add("@ref_no", refNo == null ? (object)DBNull.Value : refNo);
                    param.Add("@ReturnPackages", ExpandPackageDetail);
                //}

                //if (refNo != "null")
                //{
                //    param.Add("@enterpriseId", EnterpriseID);
                //    param.Add("@payerid", PayerId);
                //    param.Add("@ref_no", refNo);
                //    param.Add("@ReturnPackages", ExpandPackageDetail);
                //}

                var reader = this.DbFactory.Connection.QueryMultiple("QueryTrackandTrace", param: new { enterpriseid = EnterpriseID, consignment_no = ConsgnNo, ref_no = refNo, payerid = PayerId, ReturnPackages = ExpandPackageDetail }, commandType: CommandType.StoredProcedure);

                var trackandTraceHeader = reader.Read<TrackandTraceHeader>();
                var trackandTraceDetail = reader.Read<TrackandTraceDetail>().ToList();


                foreach (var item in trackandTraceDetail)
                {
                    if (item.tracking_datetime != null)
                    {
                        //item.tracking_datetime.Substring(0, 16);
                        if (item.tracking_datetime.Length <= 16)
                        {
                            item.tracking_datetime2 = DateTime.ParseExact(item.tracking_datetime, "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture);
                        }
                        else
                        {
                            // do nothing
                        }
                        //var trackdate = item.tracking_datetime.Substring(0,16);
                        //item.tracking_datetime2 = DateTime.ParseExact(trackdate, "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture);
                    }
                    //if (item.tracking_datetime != null)
                    //{
                    //    item.tracking_datetime2 = DateTime.ParseExact(item.tracking_datetime, "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture);
                    //}
                }

                result.TrackAndTraceHeader = trackandTraceHeader;
                result.TrackAndTraceDetail = trackandTraceDetail;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="appID"></param>
        /// <param name="enterpriseId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public string QueryPlayerIdTrackandTrace(string appID, string enterpriseId, string userId)
        {
            string result;
            try
            {
                // var sqlCommand = new StringBuilder(@"SELECT payerid FROM dbo.User_Master WHERE userId = @userId");
                var sqlCommand = new StringBuilder(@"SELECT payerid FROM dbo.CustomerAccount2(@enterpriseId, @userId)");

                var param = new { userId = userId, enterpriseId = enterpriseId };
                result = this.DbFactory.Connection.Query<string>(sqlCommand.ToString(), param).FirstOrDefault();

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return result;
        }

    }
}
