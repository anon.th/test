﻿using Dapper;
using PNG.Dapper.Extensions;
using PNG.DataAccess.Database;
using PNG.DataAccess.Interfaces;
using PNG.Model;
using PNG.Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PNG.DataAccess.Services
{
    public class ReportServiceDAL : DapperRepository<object>, IReportService
    {
        /// <summary>
        /// The DbFactory.
        /// </summary>
        protected readonly IDbFactory DbFactory;
        
        public ReportServiceDAL()
       : this(new DbFactory())
        {
        }

        public ReportServiceDAL(IDbFactory dbFactory)
            : base(dbFactory.Connection)
        {
            DbFactory = dbFactory;
        }

        public string getConnectionString(string appID, string enterpriseId)
        {
            string result;
            try
            {
                var sqlCommand = new StringBuilder(@"SELECT db_connection FROM Core_Enterprise WHERE applicationid = @appID and enterpriseid = @enterpriseId");

                var param = new { appID = appID, enterpriseId = enterpriseId };
                result = this.DbFactory.Connection.Query<string>(sqlCommand.ToString(), param).FirstOrDefault();

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return result;
        }


    }
}
