﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="UserAuthServiceDAL.cs" company="Aware Corporation Limited">
//   Copyright (c) 2014-2015.  All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

using Dapper;
using Microsoft.AspNet.Identity;
using PNG.AspNetIdentity.Dapper;
using PNG.Common.Services.Logging;
using PNG.Common.Services.Tracing;
using PNG.Dapper.Extensions;
using PNG.DataAccess.AspNetIdentities;
using PNG.DataAccess.Database;
using PNG.DataAccess.Interfaces;
using PNG.Model;
using System;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PNG.DataAccess.Services
{
    /// <summary>
    /// The UserAuthServiceDAL.
    /// </summary>
    public class UserAuthServiceDAL : DapperRepository<Object>, IUserAuthService
    {
        /// <summary>
        /// The DbFactory.
        /// </summary>
        protected readonly IDbFactory DbFactory;

        /// <summary>
        /// Initializes a new instance of the <see cref="UserAuthServiceDAL"/> class.
        /// </summary>
        public UserAuthServiceDAL()
            : this(new DbFactory())
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="UserAuthServiceDAL"/> class.
        /// </summary>
        /// <param name="dbFactory">
        /// The db factory.
        /// </param>
        /// <param name="transaction">
        /// The transaction.
        /// </param>
        public UserAuthServiceDAL(IDbFactory dbFactory, IDbTransaction transaction = null)
            : base(dbFactory.Connection, transaction)
        {
            DbFactory = dbFactory;
        }

        /// <summary>
        /// get user when login
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="password"></param>
        /// <returns>
        /// ApplicationUser
        /// </returns>
        public async Task<ApplicationUser> GetUser(string userName, string password)
        {
            IUserStore<ApplicationUser> userStore = null;
            UserManager<ApplicationUser> userManager = null;

            Tracer.TraceIn();
            ApplicationUser result = null;

            try
            {
                userStore = new UserStore<ApplicationUser, ApplicationRole>(new ApplicationDbContext(this.DbFactory.Connection));
                userManager = new ApplicationUserManager(userStore);

                result = await userManager.FindAsync(userName, password);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                Tracer.TraceError(ex);
                throw;
            }
            finally
            {
                Tracer.TraceOut(result);
                //if (userStore != null) userStore.Dispose();
                //if (userManager != null) userManager.Dispose();
            }

            return result;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="appID"></param>
        /// <param name="enterpriseID"></param>
        /// <param name="userID"></param>
        /// <param name="userPswd"></param>
        /// <returns></returns>
        public Model.User_Master AuthenticateUser(string appID, string enterpriseID, string userID, string userPswd)
        {
            bool result = false;
            Model.User_Master resultList = new Model.User_Master();
            try
            {
                var sqlCommand = new StringBuilder(@"select * from user_master where applicationid = @appID And enterpriseid = @enterpriseID AND userid =  @userID AND  @userPswd = (select dbo.BlowFishDecode(user_password,dbo.BlowFishKey()))");

                var param = new { appID = appID, enterpriseID = enterpriseID, userID = userID, userPswd = userPswd };
                resultList = this.DbFactory.Connection.Query<Model.User_Master>(sqlCommand.ToString(), param).FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return resultList;
        }
    }
}