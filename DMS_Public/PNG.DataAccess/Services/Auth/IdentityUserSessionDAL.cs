﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IdentityUserSessionDAL.cs" company="Aware Corporation Limited">
//   Copyright (c) 2014-2015.  All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

using Dapper;
using PNG.Common.Services;
using PNG.Common.Services.Logging;
using PNG.Common.Services.Tracing;
using PNG.Dapper.Extensions;
using PNG.DataAccess.Database;
using PNG.DataAccess.Interfaces;
using PNG.Model;
using System;
using System.Collections.Generic;
using System.Linq;

namespace PNG.DataAccess.Services
{
    /// <summary>
    /// The IdentityUserSessionDAL.
    /// </summary>
    public class IdentityUserSessionDAL : DapperRepository<IdentityUserSessions>, IIdentityUserSession
    {
        /// <summary>
        /// The DbFactory.
        /// </summary>
        protected readonly IDbFactory DbFactory;

        /// <summary>
        /// Initializes a new instance of the <see cref="IdentityUserSessionDAL"/> class.
        /// </summary>
        public IdentityUserSessionDAL()
            : this(new DbFactory())
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="IdentityUserSessionDAL"/> class.
        /// </summary>
        /// <param name="dbFactory">
        /// The db Factory.
        /// </param>
        public IdentityUserSessionDAL(IDbFactory dbFactory)
            : base(dbFactory.Connection)
        {
            this.DbFactory = dbFactory;
        }

        /// <summary>
        /// The create identity user session.
        /// </summary>
        /// <param name="userSession">
        /// The user session.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool CreateIdentityUserSession(IdentityUserSessions userSession)
        {
            Tracer.TraceIn();
            var result = false;

            try
            {
                result = this.Insert(userSession) != -1;
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                Tracer.TraceError(ex);
                throw;
            }
            finally
            {
                Tracer.TraceOut(result);
            }

            return result;
        }

        /// <summary>
        /// The invalidate user session.
        /// </summary>
        /// <param name="userName">
        /// The user id.
        /// </param>
        /// <param name="authToken">
        /// The auth token.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool InvalidateUserSession(string userName, string authToken)
        {
            Tracer.TraceIn();
            var result = false;
            const string sqlCommand = @" DELETE IdentityUserSessions Where OwnerUsername = @userName and AuthToken = @authToken ";

            try
            {
                var param = new { userName = userName, authToken = authToken };
                result = this.DbFactory.Connection.Execute(sqlCommand, param) != -1;
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                Tracer.TraceError(ex);
                throw;
            }
            finally
            {
                Tracer.TraceOut(result);
            }

            return result;
        }

        /// <summary>
        /// The re validate user session.
        /// </summary>
        /// <param name="userName">
        /// The user id.
        /// </param>
        /// <param name="authToken">
        /// The auth token.
        /// </param>
        /// <param name="sessionTimeout">
        /// The session timeout.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool ReValidateUserSession(string userName, string authToken, int sessionTimeout)
        {
            Tracer.TraceIn();
            var result = false;
            const string sqlCommand = @"SELECT * FROM IdentityUserSessions Where OwnerUsername = @userName and AuthToken = @authToken ";

            try
            {
                var param = new { userName = userName, authToken = authToken };
                var sessions = this.Query<IdentityUserSessions>(sqlCommand, param);

                if (!sessions.Any())
                {
                    return result;
                }

                var session = sessions.FirstOrDefault();

                if (session != null)
                {
                    if (session.ExpirationDate < DateTime.Now)
                    {
                        // User's session is expired --> invalid session
                        return result;
                    }

                    session.ExpirationDate = session.ExpirationDate.AddMinutes(sessionTimeout);
                    session.ModifiedDate = DateTime.Now;
                    this.Update(session, new List<string>() { "ExpirationDate", "ModifiedDate" });
                    result = true;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                Tracer.TraceError(ex);
                throw;
            }
            finally
            {
                Tracer.TraceOut(result);
            }

            return result;
        }

        /// <summary>
        /// The delete expired user sessions.
        /// </summary>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool DeleteExpiredUserSessions()
        {
            Tracer.TraceIn();
            var result = false;
            const string sqlCommand = @" DELETE IdentityUserSessions Where ExpirationDate < @ExpirationDate ";

            try
            {
                var param = new { ExpirationDate = DateTime.Now };
                result = this.DbFactory.Connection.Execute(sqlCommand, param) != -1;
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                Tracer.TraceError(ex);
                throw;
            }
            finally
            {
                Tracer.TraceOut(result);
            }

            return result;
        }
    }
}