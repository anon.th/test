﻿using System;
using System.Collections.Generic;
using System.Linq;
using PNG.DataAccess.Database;
using PNG.DataAccess.Interfaces;
using PNG.Model;
using PNG.Dapper.Extensions;
using PNG.DataAccess.Interfaces;

namespace PNG.DataAccess.Services
{
    /// <summary>
    /// 
    /// </summary>
    public class TestConnectionServiceDAL : DapperRepository<TestConnection>, ITestConnectionService
    {
        /// <summary>
        ///     The _db factory.
        /// </summary>
        protected readonly IDbFactory DbFactory;

        /// <summary>
        ///     Initializes a new instance of the <see cref="BalanceConfirmAllStockPlaceServiceDAL" /> class.
        /// </summary>
        public TestConnectionServiceDAL() : this(new DbFactory()) { }

        /// <summary>
        ///     Initializes a new instance of the <see cref="BalanceConfirmAllStockPlaceServiceDAL" /> class.
        /// </summary>
        /// <param name="dbFactory"></param>
        /// 
        public TestConnectionServiceDAL(IDbFactory dbFactory)
            : base(dbFactory.Connection)
        {
            DbFactory = dbFactory;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public List<TestConnection> SelectAll()
        {
            var result = new List<TestConnection>();

            try
            {
                result = Query<TestConnection>(@"SELECT Core_Module_Tree_Master.* FROM Core_Module_Tree_Master", null).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return result;
        }
    }
}
