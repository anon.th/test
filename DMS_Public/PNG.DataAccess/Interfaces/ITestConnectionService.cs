﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PNG.DataAccess.Interfaces
{
    public interface ITestConnectionService
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        List<PNG.Model.TestConnection> SelectAll();

        /// <summary>
        /// The dispose.
        /// </summary>
        void Dispose();
    }
}
