﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IDbFactory.cs" company="Aware Technology Solutions Corporation Ltd">
//   Copyright (c) 2016-2017.  All rights reserved.
// </copyright>
// <summary>
//   Defines the IDbFactory type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.Data;

namespace PNG.DataAccess.Interfaces
{
    /// <summary>
    /// The DbFactory interface.
    /// </summary>
    public interface IDbFactory
    {
        /// <summary>
        /// Gets the connection.
        /// </summary>
        IDbConnection Connection { get; }
    }
}