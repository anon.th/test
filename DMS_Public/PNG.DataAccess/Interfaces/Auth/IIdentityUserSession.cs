﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IIdentityUserSession.cs" company="Aware Corporation Limited">
//   Copyright (c) 2014-2015.  All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------


using PNG.DataAccess.Interfaces;
using PNG.Model;

namespace PNG.DataAccess.Interfaces
{
    /// <summary>
    /// The IdentityUserSessionDAL interface.
    /// </summary>
    public interface IIdentityUserSession
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="userSession"></param>
        /// <returns></returns>
        bool CreateIdentityUserSession(IdentityUserSessions userSession);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="authToken"></param>
        /// <returns></returns>
        bool InvalidateUserSession(string userName, string authToken);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="authToken"></param>
        /// <param name="sessionTimeout"></param>
        /// <returns></returns>
        bool ReValidateUserSession(string userName, string authToken, int sessionTimeout);

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        bool DeleteExpiredUserSessions();

        /// <summary>
        /// The dispose.
        /// </summary>
        void Dispose();
    }
}