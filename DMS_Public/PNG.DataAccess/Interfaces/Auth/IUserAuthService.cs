﻿
using PNG.DataAccess.AspNetIdentities;
using PNG.Model;
using System.Threading.Tasks;

namespace PNG.DataAccess.Interfaces
{
    /// <summary>
    /// The UserAuthServiceDAL interface.
    /// </summary>
    public interface IUserAuthService
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        Task<ApplicationUser> GetUser(string userName, string password);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="appID"></param>
        /// <param name="enterpriseID"></param>
        /// <param name="userID"></param>
        /// <param name="userPswd"></param>
        /// <returns></returns>
        User_Master AuthenticateUser(string appID, string enterpriseID, string userID, string userPswd);

        ///// <summary>
        ///// 
        ///// </summary>
        ///// <param name="userName"></param>
        ///// <param name="userObj"></param>
        ///// <param name="maxErrorLogin"></param>
        ///// <returns></returns>
        //List<ResultLogin> OptimizeLogin(string userName, bool userObj, int maxErrorLogin);
        /// <summary>
        /// The dispose.
        /// </summary>
        void Dispose();
    }
}