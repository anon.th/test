﻿using PNG.Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PNG.DataAccess.Interfaces
{
    public interface IShipmentTrackingService
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        ShipmentTrackingUserDetail getUserDetail(string applicationId, string enterpriseId, string userId);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="applicationid"></param>
        /// <param name="culture"></param>
        /// <param name="codeid"></param>
        /// <returns></returns>
        List<ShipmentTrackingCodeValue> getCodeValues(string applicationid, ShipmentTrackingParam shipmentParam);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="applicationId"></param>
        /// <param name="enterpriseId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        List<ShipmentTrackingAllRoles> getAllRoles(string applicationId, string enterpriseId, string userId);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="applicationId"></param>
        /// <param name="enterpriseId"></param>
        /// <returns></returns>
        List<ShipmentTrackingPathCodeQuery> getPathCodeQuery(string applicationId, string enterpriseId);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="applicationId"></param>
        /// <param name="enterpriseId"></param>
        /// <returns></returns>
        List<ShipmentTrackingPathCodeQuery> getDistributionCenterQuery(string applicationId, string enterpriseId);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="applicationId"></param>
        /// <param name="enterpriseId"></param>
        /// <param name="userLocationId"></param>
        /// <returns></returns>
        List<ShipmentTrackingPathCodeQuery> getDistributionCenterQuery(string applicationId, string enterpriseId, string userLocationId);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        SearchResultView<LovState> SearchWithCriteria(SearchQueryParameters parameters);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="parameters"></param>
        /// <param name="applicationId"></param>
        /// <param name="enterpriseId"></param>
        /// <returns></returns>
        SearchResultView<LovStatusCode> SearchWithStatusCode(SearchQueryParameters parameters, string applicationId, string enterpriseId);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="parameters"></param>
        /// <param name="applicationId"></param>
        /// <param name="enterpriseId"></param>
        /// <returns></returns>
        SearchResultView<LovExeptionCode> SearchWithExceptionCode(SearchQueryParameters parameters, string applicationId, string enterpriseId);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="applicationId"></param>
        /// <param name="enterpriseId"></param>
        /// <param name="pathCode"></param>
        /// <returns></returns>
        ShipmentTrackingDeliveryPath getDeliveryPath(string applicationId, string enterpriseId, ShipmentTrackingDeliveryPathParam paramPath);

        void Dispose();
    }
}
