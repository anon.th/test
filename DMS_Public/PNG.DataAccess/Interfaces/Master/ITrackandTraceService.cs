﻿using PNG.Model;
using PNG.Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PNG.DataAccess.Interfaces
{
    public interface ITrackandTraceService
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        TrackAndTraceResult SearchTrackandTrace(string AppID, string EnterpriseID, string ConsgnNo,
            string refNo, string PayerId, string ExpandPackageDetail);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="appID"></param>
        /// <param name="enterpriseId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        string QueryPlayerIdTrackandTrace(string appID, string enterpriseId, string userId);

        /// <summary>
        /// The dispose.
        /// </summary>
        void Dispose();
    }
}
