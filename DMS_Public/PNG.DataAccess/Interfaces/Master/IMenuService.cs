﻿using PNG.Model;
using PNG.Model.Models;
using System.Collections.Generic;

namespace PNG.DataAccess.Interfaces
{
    public interface IMenuService
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="appID"></param>
        /// <param name="enterpriseID"></param>
        /// <param name="user"></param>
        /// <param name="moduleid"></param>
        /// <returns></returns>
        List<Module_Tree_Role_Relation> GetAllModules(string appID, string enterpriseID, string userid, string moduleId);

        /// <summary>
        /// The Dispose.
        /// </summary>
        void Dispose();
    }
}
