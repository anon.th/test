﻿using PNG.Model;
using PNG.Model.Models;
using System;
using System.Collections.Generic;

namespace PNG.DataAccess.Interfaces
{
    public interface ISearchConsignmentService
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="enterpriseId"></param>
        /// <returns></returns>
        SearchConsignmentStatusPrinting getStatusPrinting(string enterpriseId);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="enterpriseid"></param>
        /// <param name="userloggedin"></param>
        /// <param name="payerid"></param>
        /// <returns></returns>
        List<CreateConsignmentQuerySndRecName> QuerySndRecName(string enterpriseid, string userloggedin, string payerid);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="applicationId"></param>
        /// <param name="enterpriseId"></param>
        /// <param name="selectedValue"></param>
        /// <returns></returns>
        List<CreateConsignmentCustomsWarehousesName> getCustomsWarehousesName(string applicationId, string enterpriseId, string selectedValue);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="appID"></param>
        /// <param name="enterpriseId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        string getEnterpriseUserAccountsDataSet(string appID, string enterpriseId, string userId);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="enterpriseId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        string getEnterpriseUserAccounts(string enterpriseId, string userId);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="applicationId"></param>
        /// <param name="enterpriseId"></param>
        /// <returns></returns>
        List<SearchConsignmentCustomStatus> getCustomStatus(string applicationId, string enterpriseId);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="applicationId"></param>
        /// <param name="enterpriseId"></param>
        /// <returns></returns>
        List<SearchConsignmentCustomStatus> getCustomerStatus(string applicationId, string enterpriseId);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="enterpriseID"></param>
        /// <param name="searchconsignParam"></param>
        /// <returns></returns>
        SearchConsignmentResult CSS_ConsignmentStatus(string enterpriseID, SearchConsignmentParam searchconsignParam);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="enterpriseID"></param>
        /// <param name="reprintConsNoteParam"></param>
        /// <returns></returns>
        SearchConsignmentReprintConsNoteResult CSS_ReprintConsNote(string enterpriseID, ReprintConsNoteParam reprintConsNoteParam);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="enterpriseID"></param>
        /// <param name="reprintShippingListParam"></param>
        /// <returns></returns>
        SearchConsignmentPrintShippingListResult CSS_PrintShippingList(string enterpriseID, PrintShippingListParam printShippingListParam);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="enterpriseID"></param>
        /// <param name="reprintShippingListParam"></param>
        /// <returns></returns>
        SearchConsignmentPrintConsNoteResult CSS_PrintConsNote(string enterpriseID, PrintShippingListParam printConsNoteParam);


        string getConnectionString(string appID, string enterpriseId);

        void Dispose();
    }
}
