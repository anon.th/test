﻿using PNG.Model;
using PNG.Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PNG.DataAccess.Interfaces
{
    public interface ICreateConsignmentService
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="EnterpriseID"></param>
        /// <param name="UserID"></param>
        /// <param name="ConsgnNo"></param>
        /// <param name="Action"></param>
        /// <param name="PayerID"></param>
        /// <returns></returns>
        CreateConsignmentResult GetCreateConsignmentStoreProcedure(string enterpriseID, CreateConsignmentParam createconsignParam);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="applicationid"></param>
        /// <param name="enterpriseID"></param>
        /// <param name="updateParam"></param>
        /// <returns></returns>
        CreateConsignmentUpdateReference UpdateReferenceStoreProcedure(string applicationid, string enterpriseID, CreateConsignmentParam createconsignParam);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="appID"></param>
        /// <param name="enterpriseId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        string QueryPlayerId(string appID, string enterpriseId, string userId);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="enterpriseId"></param>
        /// <param name="userId"></param>
        /// <param name="payerId"></param>
        /// <returns></returns>
        List<CreateConsignmentQuerySndRecName> QuerySndRecName(string enterpriseId, string userId, string payerId);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="enterpriseId"></param>
        /// <param name="userId"></param>
        /// <param name="payerId"></param>
        /// <returns></returns>
        string QuerySndRecNameTwo(string enterpriseId, string userId, string payerId);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="appID"></param>
        /// <param name="enterpriseId"></param>
        /// <param name="payerId"></param>
        /// <param name="sndRecName"></param>
        /// <returns></returns>
        CreateConsignmentBindSenderDetail GetBindSenderDetails(string appID, string enterpriseId, string payerId, string sndRecName);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="enterpriseId"></param>
        /// <param name="userloggedin"></param>
        /// <returns></returns>
        CreateConsignmentCustomerAccount GetCustomerAccount(string enterpriseId, string userloggedin);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="strEnterpriseID"></param>
        /// <param name="userId"></param>
        /// <param name="roleId"></param>
        /// <returns></returns>
        bool GetUserAssignedRoles(string strEnterpriseID, string userId, string roleId);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="applicationId"></param>
        /// <param name="enterpriseId"></param>
        /// <returns></returns>
        List<CreateConsignmentServiceCode> GetServiceCode(string applicationId, string enterpriseId);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="applicationId"></param>
        /// <param name="enterpriseId"></param>
        /// <param name="selectedValue"></param>
        /// <returns></returns>
        CreateConsignmentCustomsWarehousesName GetCustomsWarehousesName(string applicationId, string enterpriseId, string selectedValue);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="applicationId"></param>
        /// <param name="enterpriseId"></param>
        /// <param name="zipcode"></param>
        /// <returns></returns>
        CreateConsignmentStateName GetStateName(string applicationId, string enterpriseId, string zipcode);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="applicationId"></param>
        /// <param name="enterpriseId"></param>
        /// <param name="telephone"></param>
        /// <returns></returns>
        CreateConsignmentBindRecipienDetail getRecipientDetailByTelephone(string applicationId, string enterpriseId, string telephone);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="applicationId"></param>
        /// <param name="enterpriseId"></param>
        /// <param name="costcentre"></param>
        /// <returns></returns>
        CreateConsignmentBindRecipienDetail getRecipientDetailByCostCentre(string applicationId, string enterpriseId, string costcentre);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="applicationId"></param>
        /// <param name="enterpriseId"></param>
        /// <param name="payerId"></param>
        /// <returns></returns>
        Customer getCustomerAcc2(string applicationId, string enterpriseId, string payerId);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="applicationId"></param>
        /// <param name="enterpriseId"></param>
        /// <param name="payerId"></param>
        /// <returns></returns>
        List<CreateConsignmentPackageLimit> getPackageLimit(string applicationId, string enterpriseId, string payerId);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="applicationId"></param>
        /// <param name="enterpriseId"></param>
        /// <param name="payerId"></param>
        /// <returns></returns>
        List<CreateConsignmentConfigurations> getConfigurations(string applicationId, string enterpriseId, string payerId);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="enterpriseId"></param>
        /// <param name="payerId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        CreateConsignmentEnterpriseContract getEnterpriseContract(string enterpriseId, string payerId, string userId);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="applicationId"></param>
        /// <param name="enterpriseId"></param>
        /// <param name="payerId"></param>
        /// <returns></returns>
        CreateConsignmentDefaultServiceCode getDefaultServiceCode(string applicationId, string enterpriseId, string payerId);

        void Dispose();
    }
}
