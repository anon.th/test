﻿using PNG.Model;
using System.Collections.Generic;

namespace PNG.DataAccess.Interfaces
{
    public interface ILoginService
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        bool AuthenticateUser(string appID, string enterpriseID, string userID, string userPswd);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="appID"></param>
        /// <param name="enterpriseID"></param>
        /// <param name="userID"></param>
        /// <returns></returns>
        User_Master GetUser(string appID, string enterpriseID, string userID);


        /// <summary>
        /// 
        /// </summary>
        /// <param name="appID"></param>
        /// <param name="enterpriseID"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        List<Role_Master> GetAllRoles(string appID, string enterpriseID, User_Master user);

        /// <summary>
        /// The dispose.
        /// </summary>
        void Dispose();
    }
}
