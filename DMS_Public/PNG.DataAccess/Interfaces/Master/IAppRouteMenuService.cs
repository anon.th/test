﻿using PNG.Model;
using PNG.Model.Models;

namespace PNG.DataAccess.Interfaces
{
    public interface IAppRouteMenuService
    {
        /// <summary>
        /// The search with criteria.
        /// </summary>
        /// <param name="queryParameters">
        /// The query parameters.
        /// </param>
        /// <returns>
        /// The <see cref="SearchResultView"/>.
        /// </returns>
        SearchResultView<AppRouteMenu> SearchWithCriteria(SearchQueryParameters queryParameters);

        /// <summary>
        /// The Dispose.
        /// </summary>
        void Dispose();
    }
}
