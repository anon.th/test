﻿using PNG.Model;
using PNG.Model.Models;
using System.Collections.Generic;

namespace PNG.DataAccess.Interfaces
{
    public interface IReferenceService
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="appID"></param>
        /// <param name="enterpriseID"></param>
        /// <param name="user"></param>
        /// <param name="moduleid"></param>
        /// <returns></returns>
        SearchResultView<Reference> SearchWithCriteria(SearchQueryParameters parameters);

        /// <summary>
        /// The Dispose.
        /// </summary>
        void Dispose();
    }
}
