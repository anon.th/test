﻿using PNG.Model;
using PNG.Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PNG.DataAccess.Interfaces
{
    public interface IPayerCodeService
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        SearchResultView<Customer> SearchWithCriteria(SearchQueryParameters parameters);

        void Dispose();
    }
}
