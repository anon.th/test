﻿using PNG.Model;
using System.Collections.Generic;

namespace PNG.DataAccess.Interfaces
{
    public interface IUserProfileService
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="appID"></param>
        /// <param name="enterpriseId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        bool Edit(User_Master entity);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="fieldToUpdate"></param>
        /// <returns></returns>
        bool Edit(User_Master entity, List<string> fieldToUpdate);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        User_Master GetUser(string userId);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        List<User_Master> GetUsers(string userId);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        bool UpdateProfile(User_Master user);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        bool UpdatePassword(User_Master user);

        /// <summary>
        /// The dispose.
        /// </summary>
        void Dispose();
    }
}
