﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ApplicationUser.cs" company="Aware Corporation Limited">
//   Copyright (c) 2014-2015.  All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

using Microsoft.AspNet.Identity;
using PNG.AspNetIdentity.Dapper;
using System.Security.Claims;
using System.Threading.Tasks;

namespace PNG.DataAccess.AspNetIdentities
{
    /// You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    /// <summary>
    /// The application user.
    /// </summary>
    [Dapper.Extensions.Utils.Table("User_Master")]
    public class ApplicationUser : User_Master
    {
        /// <summary>
        /// Gets or sets the first name.
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// Gets or sets the last name.
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// The generate user identity async.
        /// </summary>
        /// <param name="manager">
        /// The manager.
        /// </param>
        /// <param name="authenticationType">
        /// The authentication type.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager, string authenticationType)
        {
            return await Task.Run(() =>
            {
                var userIdentity = manager.CreateIdentity(this, authenticationType);
                return userIdentity;
            });
        }
    }
}