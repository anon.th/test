﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ApplicationDbContext.cs" company="Aware Corporation Limited">
//   Copyright (c) 2014-2015.  All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

using PNG.AspNetIdentity.Dapper;
using System.Data;
using System.Data.SqlClient;

namespace PNG.DataAccess.AspNetIdentities
{
    /// <summary>
    /// The application db context.
    /// </summary>
    public class ApplicationDbContext : DapperIdentityDbContext<ApplicationUser, ApplicationRole>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ApplicationDbContext"/> class.
        /// </summary>
        /// <param name="connection">
        /// The connection.
        /// </param>
        public ApplicationDbContext(IDbConnection connection)
            : base(connection)
        {
        }

        /// <summary>
        /// The create.
        /// </summary>
        /// <returns>
        /// The <see cref="ApplicationDbContext"/>.
        /// </returns>
        public static ApplicationDbContext Create()
        {
            var connString = System.Configuration.ConfigurationManager.ConnectionStrings["DMS_R3_PNGAFConnectionString"].ConnectionString;
            var conn = new SqlConnection(connString);
            return new ApplicationDbContext(conn);
        }
    }
}