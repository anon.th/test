﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ApplicationRole.cs" company="Aware Corporation Limited">
//   Copyright (c) 2014-2015.  All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

using PNG.AspNetIdentity.Dapper;

namespace PNG.DataAccess.AspNetIdentities
{
    /// <summary>
    /// The application role.
    /// </summary>
    [Dapper.Extensions.Utils.Table("IdentityRoles")]
    public class ApplicationRole : IdentityRole
    {
    }
}