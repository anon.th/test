﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IConstructorSelector.cs" company="Aware Technology Solutions Corporation Ltd">
//   Copyright (c) 2016-2017.  All rights reserved.
// </copyright>
// <summary>
//   Defines the IConstructorSelector type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Reflection;

namespace PNG.SimpleInjector.Extensions
{
    /// <summary>
    /// 
    /// </summary>
    public interface IConstructorSelector
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        ConstructorInfo GetConstructor(Type type);
    }
}