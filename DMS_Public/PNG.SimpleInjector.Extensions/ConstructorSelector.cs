﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ConstructorSelector.cs" company="Aware Technology Solutions Corporation Ltd">
//   Copyright (c) 2016-2017.  All rights reserved.
// </copyright>
// <summary>
//   Defines the ConstructorSelector type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Linq;
using System.Reflection;

namespace PNG.SimpleInjector.Extensions
{
    /// <summary>
    /// 
    /// </summary>
    public sealed class ConstructorSelector : IConstructorSelector
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly Func<Type, ConstructorInfo> _selector;

        /// <summary>
        /// 
        /// </summary>
        public static readonly IConstructorSelector MostParameters = new ConstructorSelector(type => type.GetConstructors()
            .OrderByDescending(count => count.GetParameters().Length)
            .FirstOrDefault());

        /// <summary>
        /// 
        /// </summary>
        public static readonly IConstructorSelector LeastParameters = new ConstructorSelector(type => type.GetConstructors()
            .OrderBy(count => count.GetParameters().Length)
            .FirstOrDefault());

        /// <summary>
        /// 
        /// </summary>
        /// <param name="selector"></param>
        public ConstructorSelector(Func<Type, ConstructorInfo> selector)
        {
            this._selector = selector;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public ConstructorInfo GetConstructor(Type type)
        {
            return this._selector(type);
        }
    }
}