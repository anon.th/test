﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PNG.Model.Models
{
    public class CreateConsignmentSenderDetail
    {
        public string applicationid { get; set; }
        public string enterpriseid { get; set; }
        public string consignment_no { get; set; }
        public string payerid { get; set; }
        public string ShippingList_no { get; set; }
        public string service_code { get; set; }
        public string ref_no { get; set; }
        public string last_status { get; set; }
        public DateTime? last_status_DT { get; set; }
        public string sender_name { get; set; }
        public string sender_address1 { get; set; }
        public string sender_address2 { get; set; }
        public string sender_zipcode { get; set; }
        public string sender_state_name { get; set; }
        public string sender_telephone { get; set; }
        public string sender_fax { get; set; }
        public string sender_contact_person { get; set; }
        public string sender_email { get; set; }
        public string recipient_telephone { get; set; }
        public string recipient_name { get; set; }
        public string recipient_address1 { get; set; }
        public string recipient_address2 { get; set; }
        public string recipient_zipcode { get; set; }
        public string recipient_state_name { get; set; }
        public string recipient_fax { get; set; }
        public string recipient_contact_person { get; set; }

        public string declare_value { get; set; }
        public string cod_amount { get; set; }
        public string remark { get; set; }
        public string return_pod_slip { get; set; }
        public string return_invoice_hc { get; set; }
        public int DangerousGoods { get; set; }
        public string Created_By { get; set; }
        public DateTime? Created_DT { get; set; }
        public string Updated_By { get; set; }
        public DateTime? Updated_DT { get; set; }
        public int ContractAccepted { get; set; }
        public string GoodsDescription { get; set; }

    }
}
