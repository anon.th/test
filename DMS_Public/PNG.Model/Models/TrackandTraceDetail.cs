﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PNG.Model.Models
{
    public class TrackandTraceDetail
    {
        public string tracking_datetime { get; set; }

        public string status_code { get; set; }

        public string status_description { get; set; }

        public string exception_code { get; set; }

        public string exception_description { get; set; }

        public string location { get; set; }

        public string remarks { get; set; }


        public string consignee_name { get; set; }

        public string deleted { get; set; }


        public string delete_remark { get; set; }

        public string person_incharge { get; set; }

        public DateTime? last_updated { get; set; }

        public string last_userid { get; set; }

        public DateTime? tracking_datetime2 { get; set; }

        public string scc { get; set; }
    }
}
