﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PNG.Model.Models
{
    public class CreateConsignmentStateName
    {
        public string zipcode { get; set; }
        public string state_name { get; set; }
        public string Country { get; set; }
    }

}
