﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PNG.Model.Models
{
    public class SearchResultView<T> where T : class
    {
        /// <summary>
        /// Gets or sets the search results.
        /// </summary>
        public IEnumerable<T> SearchResults { get; set; }

        /// <summary>
        /// Gets or sets the total record.
        /// </summary>
        public int TotalRecord { get; set; }

        /// <summary>
        /// Gets or sets the record count.
        /// </summary>
        public int RecordCount { get; set; }
    }
}
