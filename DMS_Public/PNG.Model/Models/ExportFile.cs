﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ExportFile.cs" company="Aware Technology Solutions Corporation Ltd">
//   Copyright (c) 2016-2017.  All rights reserved.
// </copyright>
// <summary>
//   Defines the ExportFile, ExportInfo and FileResult type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
using PNG.Common.Enums;
using System.Collections.Generic;
using System.Data;

namespace PNG.Model.Models
{
    /// <summary>
    /// File result return path and info string data, content type and file name
    /// </summary>
    public class ExportFile
    {
        /// <summary>
        /// 
        /// </summary>
        public string ExportType
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        public string Url
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        public bool Enabled
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        public string TranslationText
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        public string PhysicalFullFileName
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        public string FileName
        {
            get;
            set;
        }
    }

    /// <summary>
    /// File result return Base64 string data, content type and file name
    /// </summary>
    public class FileResult
    {
        /// <summary>
        /// 
        /// </summary>
        public string Base64StringData
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        public string ContentType
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        public string FileName
        {
            get;
            set;
        }
    }

    /// <summary>
    /// Export info is a input data of export module
    /// </summary>
    public class ExportInfo<T>
        where T : class
    {
        /// <summary>
        /// 
        /// </summary>
        public IEnumerable<T> SourceAsEnumerable
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        public DataTable SourceAsTable
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        public FileFormat FileType
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        public string FilePath
        {
            get;
            set;
        }
    }
}