﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PNG.Model.Models
{
    public class CreateConsignmentBindSenderDetail
    {
        // SENDER DETAIL
        public string snd_rec_name { get; set; }
        public string snd_rec_type { get; set; }
        public string contact_person { get; set; }
        public string email { get; set; }
        public string address1 { get; set; }
        public string address2 { get; set; }
        public string country { get; set; }
        public string zipcode { get; set; }
        public string telephone { get; set; }
        public string fax { get; set; }
    }
    public class CreateConsignmentBindRecipienDetail
    {
        public string telephone { get; set; }
        public string reference_name { get; set; }
        public string address1 { get; set; }
        public string address2 { get; set; }
        public string zipcode { get; set; }
        public string fax { get; set; }
        public string contactperson { get; set; }
        public string cost_centre { get; set; }
    }
}
