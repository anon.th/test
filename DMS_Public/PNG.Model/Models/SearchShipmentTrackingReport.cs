﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PNG.Model.Models
{
    public class SearchShipmentTrackingReport
    {
        public DateTime PeriodFrom { get; set; }
        public DateTime PeriodTo { get; set; }
        public string delPathOriginDC { get; set; }
        public string delPathDestinationDC { get; set; }
        public string month { get; set; }
        public string year { get; set; }
        public string tran_date { get; set; }
        public string payer_code { get; set; }
        public List<string> payer_type { get; set; }
        public string route_type { get; set; }
        public string route_code { get; set; }
        public string origin_dc { get; set; }
        public string destination_dc { get; set; }
        public string zip_code { get; set; }
        public string state_code { get; set; }
        public string booking_type { get; set; }
        public string consignment_no { get; set; }
        public string booking_no { get; set; }
        public string status_code { get; set; }
        public string exception_code { get; set; }
        public string shipment_closed { get; set; }
        public string shipment_invoiced { get; set; }
        public string track_all { get; set; }
        public string RptType { get; set; }
        public string userId { get; set; }
        public DateTime PeriodFromFormat { get; set; }
        public DateTime PeriodToFormat { get; set; }
        public string reportName { get; set; }
    }
    
}
