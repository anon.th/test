﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PNG.Model.Models
{
    public class CreateConsignmentResult
    {
        public IEnumerable<CreateConsignmentError> CreateConsignmentError { get; set; }
        public List<CreateConsignmentSenderDetail> CreateConsignmentSenderDetail { get; set; }
        public IEnumerable<CreateConsignmentPackageDetail> CreateConsignmentPackageDetail { get; set; }

    }
    public class CreateConsignmentError
    {
        public int? ErrorCode { get; set; }

        public int? RequestedAction { get; set; }

        public string ErrorMessage { get; set; }

        public int? EnableDeleteButton { get; set; }


    }
    public class CreateConsignmentUpdateReference
    {
        public string msgStatus { get; set; }

        public string msgMessage { get; set; }

    }
}
