﻿using System.ComponentModel.DataAnnotations;

namespace PNG.Model
{
    public partial class UserLogin
    {
        [Required]
        [Display(Name = "Username")]
        public string Username { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Required]
        [Display(Name = "IP Address")]
        public string IpAddress { get; set; }

        // [Required]
        [Display(Name = "Client Connection Id")]
        public string ClientConnectionId { get; set; }


        [Display(Name = "enterpriseID")]
        public string EnterpriseID { get; set; }

        [Display(Name = "userTypeId")]
        public string UserTypeId { get; set; }

    }
}
