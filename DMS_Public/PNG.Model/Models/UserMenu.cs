﻿using PNG.Dapper.Extensions.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PNG.Model
{   
        public partial class Module_Tree_Role_Relation
        {
            [Ignored]
            public string new_child_name { get; set; }
        }

        public partial class Zipcode
        {
            [Ignored]
            public string state_name { get; set; }
        }
}
