﻿using PNG.Common.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PNG.Model.Models
{
    public class SearchFilterView
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SearchFilterView"/> class.
        /// </summary>
        public SearchFilterView()
        {
            this.LikePosition = SearchLikePosition.AllWords;
        }

        /// <summary>
        /// Gets or sets the table name.
        /// </summary>
        public string TableName { get; set; }

        /// <summary>
        /// Gets or sets the field name.
        /// </summary>
        public string FieldName { get; set; }

        /// <summary>
        /// Gets or sets the parameter name.
        /// </summary>
        public string ParameterName { get; set; }

        /// <summary>
        /// Gets or sets the value.
        /// </summary>
        public object Value { get; set; }

        /// <summary>
        /// Gets or sets the list of value.
        /// </summary>
        public List<object> ListValues { get; set; }

        /// <summary>
        /// Gets or sets the operator type.
        /// </summary>
        public SearchOperatorType OperatorType { get; set; }

        /// <summary>
        /// Gets or sets the condition type.
        /// </summary>
        public SearchConditionType ConditionType { get; set; }

        /// <summary>
        /// Gets or sets the like position.
        /// </summary>
        public SearchLikePosition LikePosition { get; set; }

        /// <summary>
        /// Gets or sets the ListSearchFilterViews
        /// </summary>
        public List<SearchFilterView> ListSearchFilterViews { get; set; }
    }
}
