﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PNG.Model.Models
{
    public class CreateConsignmentCustomerAccount
    {
        public string currency { get; set; }
        public decimal currency_decimal { get; set; }
        public string cust_name { get; set; }
        public string DefaultSender { get; set; }
        public bool IsCustomerUser { get; set; }
        public bool IsCustomsUser { get; set; }
        public bool IsEnterpriseUser { get; set; }
        public bool IsMasterCustomerUser { get; set; }
        public string payerid { get; set; }
    }
}
