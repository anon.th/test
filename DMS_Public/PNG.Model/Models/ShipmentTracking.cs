﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PNG.Model.Models
{
    public class ShipmentTrackingCodeValue
    {
        public double? sequence { get; set; }
        public string code_text { get; set; }
        public string code_str_value { get; set; }
        public string code_num_value { get; set; }
    }

    public class ShipmentTrackingUserDetail
    {
        public string userid { get; set; }
        public string user_name { get; set; }
        public string user_culture { get; set; }
        public string email { get; set; }
        public string department_name { get; set; }
        public string User_Type { get; set; }
        public string payerid { get; set; }
        public string Location { get; set; }
    }

    public class ShipmentTrackingParam
    {
        public string userid { get; set; }
        public string user_culture { get; set; }
        public string codeid { get; set; }
    }

    public class ShipmentTrackingAllRoles
    {
        public int? roleid { get; set; }
        public string role_name { get; set; }
    }
    public class ShipmentTrackingPathCodeQuery
    {
        public string DbComboText { get; set; }
        public string DbComboValue { get; set; }
    }

    public class LovState
    {
        public string country { get; set; }
        public string state_code { get; set; }
        public string state_name { get; set; }
    }
    public class LovStatusCode
    {
        public string status_code { get; set; }
        public string status_description { get; set; }
        public string Status_type { get; set; }
        public string status_close { get; set; }
        public string invoiceable { get; set; }
    }

    public class LovExeptionCode
    {
        public string status_code { get; set; }
        public string exception_code { get; set; }
        public string exception_description { get; set; }
        public string mbg { get; set; }
        public string status_close { get; set; }
        public string invoiceable { get; set; }
        public string status_description { get; set; }
    }
    public class ShipmentTrackingDeliveryPath
    {
        public string path_code { get; set; }
        public string applicationid { get; set; }
        public string enterpriseid { get; set; }
        public string path_description { get; set; }
        public string delivery_type { get; set; }
        public double? line_haul_cost { get; set; }
        public string origin_station { get; set; }
        public string destination_station { get; set; }
        public string flight_vehicle_no { get; set; }
        public string awb_driver_name { get; set; }
        public DateTime? departure_time { get; set; }
    }
    public class ShipmentTrackingDeliveryPathParam
    {
        public string path_code { get; set; }
    }

}
