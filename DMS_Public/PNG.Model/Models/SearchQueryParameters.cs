﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PNG.Model.Models
{
    public class SearchQueryParameters
    {

        public void ClearFilterList()
        {
            FilterList = new List<SearchFilterView>();
        }
        /// <summary>
        /// Gets or sets the filter list.
        /// </summary>
        public IEnumerable<SearchFilterView> FilterList { get; set; }

        /// <summary>
        /// Gets or sets the sort column.
        /// </summary>
        public string SortColumn { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether sort ascending.
        /// </summary>
        public bool SortAscending { get; set; }

        /// <summary>
        /// Gets or sets the page.
        /// </summary>
        public int? Page { get; set; }

        /// <summary>
        /// Gets or sets the items per page.
        /// </summary>
        public int? ItemsPerPage { get; set; }
    }
}
