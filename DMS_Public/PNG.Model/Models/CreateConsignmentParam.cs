﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PNG.Model.Models
{
    public class CreateConsignmentParam
    {
        public int action { get; set; }
        public int ForceDelete { get; set; }
        public string enterpriseid { get; set; }
        public string userloggedin { get; set; }
        public string OverrideSenderName { get; set; }
        public string consignment_no { get; set; }
        public string payerid { get; set; }
        public string service_code { get; set; }
        public string ref_no { get; set; }
        public string sender_name { get; set; }
        public string sender_address1 { get; set; }
        public string sender_address2 { get; set; }
        public string sender_zipcode { get; set; }
        public string sender_telephone { get; set; }
        public string sender_fax { get; set; }
        public string sender_contact_person { get; set; }
        public string sender_email { get; set; }
        public string recipient_telephone { get; set; }
        public string recipient_name { get; set; }
        public string recipient_address1 { get; set; }
        public string recipient_address2 { get; set; }
        public string recipient_zipcode { get; set; }
        public string recipient_fax { get; set; }
        public string recipient_contact_person { get; set; }
        public decimal declare_value { get; set; }
        public decimal cod_amount { get; set; }
        public string remark { get; set; }
        public string return_pod_slip { get; set; }
        public string return_invoice_hc { get; set; }
        public int DangerousGoods { get; set; }
        public int ContractAccepted { get; set; }
        public string PackageDetails { get; set; }
        public string GoodsDescription { get; set; }
        public string cost_centre { get; set; }

    }
    public class CreateConsignUpdateParam
    {
        public string userloggedin { get; set; }
        public string recipient_telephone { get; set; }
        public string recipient_reference_name { get; set; }
        public string recipient_address1 { get; set; }
        public string recipient_address2 { get; set; }
        public string recipient_zipcode { get; set; }
        public string recipient_fax { get; set; }
        public string recipient_contactperson { get; set; }
        public string recipient_cost_centre { get; set; }
    }
}
