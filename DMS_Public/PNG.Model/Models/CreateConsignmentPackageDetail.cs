﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PNG.Model.Models
{
    public class CreateConsignmentPackageDetail
    {
        public string applicationid { get; set; }
        public string enterpriseid { get; set; }
        public string consignment_no { get; set; }
        public string payerid { get; set; }
        public int? mps_code { get; set; }
        public int? pkg_qty { get; set; }
        public double? pkg_wt { get; set; }
        public double? tot_wt { get; set; }
        public double? pkg_length { get; set; }
        public double? pkg_breadth { get; set; }
        public double? pkg_height { get; set; }
    }
    public class CreateConsignmentPackageLimit
    {
        public string key { get; set; }
        public string value { get; set; }
    }

    public class CreateConsignmentConfigurations
    {
        public string key { get; set; }
        public string value { get; set; }
    }

    public class CreateConsignmentEnterpriseContract
    {
        public string AcceptedCurrentContract { get; set; }
        public string ContractText { get; set; }
    }
    public class CreateConsignmentDefaultServiceCode
    {
        public string Default_Service_Code { get; set; }
    }
}
