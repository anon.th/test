﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PNG.Model.Models
{
    public class CreateConsignmentUserAssignRole
    {
        public string user_name { get; set; }
        public int IsAssigned { get; set; }

    }
}
