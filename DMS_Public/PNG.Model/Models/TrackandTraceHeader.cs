﻿using System;
using System.Collections.Generic;

namespace PNG.Model.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class TrackAndTraceResult
    {
        public IEnumerable<TrackandTraceHeader> TrackAndTraceHeader { get; set; }


        public List<TrackandTraceDetail> TrackAndTraceDetail { get; set; }

    }

    public class TrackandTraceHeader
    {
        public string enterpriseid { get; set; }

        public int booking_no { get; set; }

        public string consignment_no { get; set; }

        public string ref_no { get; set; }

        public DateTime? booking_datetime { get; set; }

        public DateTime? est_delivery_datetime { get; set; }

        public string service_code { get; set; }

        public string origin_state_code { get; set; }

        public string destination_state_code { get; set; }

        public string payerid { get; set; }

        public string payer_name { get; set; }

        public string status_description { get; set; }

    }

    public class TrackandTraceParam
    {
        public string consignmentNumber { get; set; }
        public string customerRef { get; set; }
        public string userId { get; set; }
        public Boolean returnPackage { get; set; }
    }
}
