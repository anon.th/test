﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PNG.Model.Models
{
    public class CreateConsignmentCustomsWarehousesName
    {
        public string applicationid { get; set; }
        public string enterpriseid { get; set; }
        public string warehouse_name { get; set; }
        public string contact_person { get; set; }
        public string email { get; set; }
        public string address1 { get; set; }
        public string address2 { get; set; }
        public string country { get; set; }
        public string state_code { get; set; }
        public string zipcode { get; set; }
        public string telephone { get; set; }
        public string fax { get; set; }
    }
}
