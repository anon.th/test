﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PNG.Model.Models
{
    public class ReportDataSource
    {
        /// <summary>
        /// Gets or sets the sub report name.
        /// </summary>
        public string SubReportName { get; set; }

        /// <summary>
        /// Gets or sets the data source.
        /// </summary>
        public IEnumerable<object> DataSource { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DataTable DataSourceAsTable { get; set; }

        public string TableName { get; set; }
    }
}
