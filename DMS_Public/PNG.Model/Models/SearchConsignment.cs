﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PNG.Model.Models
{
    public class SearchConsignment
    {
        public string key { get; set; }
        public string value { get; set; }
    }
    public class SearchConsignmentStatusPrinting
    {
        public string key { get; set; }
        public string value { get; set; }
    }

    public class SearchConsignmentCustomStatus
    {
        public string DisplayStatus { get; set; }
        public int? StatusID  { get; set; }
    }

    public class SearchConsignmentResult
    {
        public IEnumerable<SearchConsignmentError> SearchConsignmentError { get; set; }
        public List<SearchConsignmentDetail> SearchConsignmentDetail { get; set; }
        public int? CountData { get; set; }
    }

    public class SearchConsignmentError
    {
        public int? ErrorCode { get; set; }
        public string ErrorMessage { get; set; }
    }
    public class SearchConsignmentDetail
    {        
        public string applicationid { get; set; }
        public string enterpriseid { get; set; }
        public string consignment_no { get; set; }
        public string sender_name { get; set; }
        public string last_status { get; set; }
        public string ShippingList_no { get; set; }
        public string recipient_telephone { get; set; }
        public string recipient_name { get; set; }
        public string recipient_zipcode { get; set; }
        public string service_code { get; set; }
        public int? pkg_qty { get; set; }
        public string ref_no { get; set; }
        public int? ProcessStep { get; set; }
        public int? booking_no { get; set; }
        public int? JobEntryNo { get; set; }
        public string MAWBNumber { get; set; }
        public int? SeqNo { get; set; }
    }

    public class SearchConsignmentParam
    {
        public string enterpriseid { get; set; }
        public string userloggedin { get; set; }
        public string SenderName { get; set; }
        public string consignment_no { get; set; }
        public int? status_id { get; set; }
        public string ShippingList_No { get; set; }
        public string recipient_telephone { get; set; }
        public string ref_no { get; set; }
        public string recipient_zipcode { get; set; }
        public string service_code { get; set; }
        public DateTime? datefrom { get; set; }
        public DateTime? dateto { get; set; }
        public string payerid { get; set; }
        public string MasterAWBNumber { get; set; }
        public string JobEntryNo { get; set; }
        public int? pageNumber { get; set; }
        public int? pageCount { get; set; }
    }

    public class ReprintConsNoteParam
    {
        public string userloggedin { get; set; }
        public string consignmentsList { get; set; }
        public int? dataSource { get; set; }
        public int? bookingNo { get; set; }

    }

    public class PrintShippingListParam
    {
        public string userloggedin { get; set; }
        public string consignmentsList { get; set; }
    }

    public class ReprintConsNoteError
    {
        public int? ErrorCode { get; set; }
        public string ErrorMessage { get; set; }
        public string ReportTemplate { get; set; }

    }
    public class ReprintConsNoteDetail
    {        
        public string consignment_no { get; set; }
        public string payerid { get; set; }
        public string service_code { get; set; }
        public string ref_no { get; set; }
        public string sender_name { get; set; }
        public string sender_address1 { get; set; }
        public string sender_address2 { get; set; }

        public string sender_zipcode { get; set; }
        public string sender_state { get; set; }
        public string sender_telephone { get; set; }

        public string sender_fax { get; set; }
        public string sender_contact_person { get; set; }
        public string sender_email { get; set; }
        public string recipient_telephone { get; set; }
        public string recipient_name { get; set; }
        public string recipient_address1 { get; set; }
        public string recipient_address2 { get; set; }
        public string recipient_zipcode { get; set; }
        public string recipient_state { get; set; }
        public string recipient_fax { get; set; }
        public string recipient_contact_person { get; set; }
        public Nullable<double> declare_value { get; set; }
        public Nullable<double> cod_amount { get; set; }  
        public string remark { get; set; }
        public string return_pod_slip { get; set; }
        public string return_invoice_hc { get; set; }
        public Nullable<int> DangerousGoods { get; set; }
        public string MoreInfo { get; set; }
        public string MorePkgs { get; set; }
        public Nullable<int> Qty1 { get; set; }
        public Nullable<double> Wgt1 { get; set; }
        public Nullable<int> Qty2 { get; set; }
        public Nullable<double> Wgt2 { get; set; }
        public Nullable<int> Qty3 { get; set; }
        public Nullable<double> Wgt3 { get; set; }
        public Nullable<int> Qty4 { get; set; }
        public Nullable<double> Wgt4 { get; set; }
        public Nullable<int> Qty5 { get; set; }
        public Nullable<double> Wgt5 { get; set; }
        public Nullable<int> Qty6 { get; set; }
        public Nullable<double> Wgt6 { get; set; }
        public Nullable<int> Qty7 { get; set; }
        public Nullable<double> Wgt7 { get; set; }
        public Nullable<int> Qty8 { get; set; }
        public Nullable<double> Wgt8 { get; set; }
        public Nullable<int> Qty { get; set; }
        public Nullable<double> Wgt { get; set; }
        public string CopyName { get; set; }
        public string sender_country { get; set; }
        public string recipient_country { get; set; }
        public string GoodsDescription { get; set; }
        public Nullable<double> total_rated_amount { get; set; }
        public Nullable<double> tax_on_rated_amount { get; set; }
        public Nullable<double> total_amount { get; set; }
        public byte[] imgBarcode { get; set; }
        public Nullable<int> Rtg1 { get; set; }
        public Nullable<int> Rtg1_toea { get; set; }
        public Nullable<int> Rtg2 { get; set; }
        public Nullable<int> Rtg2_toea { get; set; }
        public Nullable<int> Rtg3 { get; set; }
        public Nullable<int> Rtg3_toea { get; set; }
        public Nullable<int> other_surch_amount { get; set; }
        public Nullable<int> other_surch_amount_toea { get; set; }
        public Nullable<int> basic_charge { get; set; }
        public Nullable<int> basic_charge_toea { get; set; }
        public string ConsignmentDescription { get; set; }

    }

    public class SearchConsignmentReprintConsNoteResult
    {
        public IEnumerable<ReprintConsNoteError> SearchConsignmentReprintConsNoteError { get; set; }
        public List<ReprintConsNoteDetail> SearchConsignmentReprintConsNoteDetail { get; set; }
    }

    public class PrintShippingListError
    {
        public int? ErrorCode { get; set; }
        public string ErrorMessage { get; set; }

    }
    public class PrintShippingListDetail
    {
        public string enterprise_name { get; set; }
        public string enterprise_address { get; set; }
        public string enterprise_telephone { get; set; }
        public string custid { get; set; }
        public string customer_address { get; set; }
        public string customer_zipcode { get; set; }
        public string customer_telephone { get; set; }
        public string sender_name { get; set; }
        public string sender_address { get; set; }
        public string sender_zipcode { get; set; }
        public string sender_telephone { get; set; }
        public string consignment_no { get; set; }
        public int? total_packages { get; set; }
        public int? total_wt { get; set; }
        public string ref_no { get; set; }
        public string service_code { get; set; }
        public string recipient_telephone { get; set; }
        public string recipient_name { get; set; }
        public string recipient_address { get; set; }
        public string recipient_zipcode { get; set; }
        public string state_name { get; set; }
        public string ShippingList_No { get; set; }
        public string cust_name { get; set; }
        public DateTime? Printed_Date_Time { get; set; }
        public string GoodsDescription { get; set; }
        public string cost_centre { get; set; }
        public byte[] imgBarcode { get; set; }

    }
    public class SearchConsignmentPrintShippingListResult
    {
        public PrintShippingListError SearchConsignmentPrintShippingListError { get; set; }
        public List<PrintShippingListDetail> SearchConsignmentPrintShippingListDetail { get; set; }
    }
    public class PrintConsNoteError
    {
        public int? ErrorCode { get; set; }
        public string ErrorMessage { get; set; }
        public string ReportTemplate { get; set; }

    }
    public class PrintConsNoteDetail
    {
        public string consignment_no { get; set; }
        public string payerid { get; set; }
        public string service_code { get; set; }
        public string ref_no { get; set; }
        public string sender_name { get; set; }
        public string sender_address1 { get; set; }
        public string sender_address2 { get; set; }

        public string sender_zipcode { get; set; }
        public string sender_state { get; set; }
        public string sender_telephone { get; set; }

        public string sender_fax { get; set; }
        public string sender_contact_person { get; set; }
        public string sender_email { get; set; }
        public string recipient_telephone { get; set; }
        public string recipient_name { get; set; }
        public string recipient_address1 { get; set; }
        public string recipient_address2 { get; set; }
        public string recipient_zipcode { get; set; }
        public string recipient_state { get; set; }
        public string recipient_fax { get; set; }
        public string recipient_contact_person { get; set; }
        public double? declare_value { get; set; }
        public double? cod_amount { get; set; }
        public string remark { get; set; }
        public string return_pod_slip { get; set; }
        public string return_invoice_hc { get; set; }
        public int? DangerousGoods { get; set; }
        public string MoreInfo { get; set; }
        public string MorePkgs { get; set; }
        public int? Qty1 { get; set; }
        public double? Wgt1 { get; set; }
        public int? Qty2 { get; set; }
        public double? Wgt2 { get; set; }
        public int? Qty3 { get; set; }
        public double? Wgt3 { get; set; }
        public int? Qty4 { get; set; }
        public double? Wgt4 { get; set; }
        public int? Qty5 { get; set; }
        public double? Wgt5 { get; set; }
        public int? Qty6 { get; set; }
        public double? Wgt6 { get; set; }
        public int? Qty7 { get; set; }
        public double? Wgt7 { get; set; }
        public int? Qty8 { get; set; }
        public double? Wgt8 { get; set; }
        public int? Qty { get; set; }
        public double? Wgt { get; set; }
        public string CopyName { get; set; }
        public string sender_country { get; set; }
        public string recipient_country { get; set; }
        public string GoodsDescription { get; set; }
        public byte[] imgBarcode { get; set; }

    }
    public class SearchConsignmentPrintConsNoteResult
    {
        public IEnumerable<PrintConsNoteError> SearchConsignmentPrintConsNoteError { get; set; }
        public List<PrintConsNoteDetail> SearchConsignmentPrintConsNoteDetail { get; set; }
    }
    public class SearchConsignmentConnectionstring
    {
        public string datasource { get; set; }
        public string initialCatalog { get; set; }
        public string persistSecurity { get; set; }
        public string userId { get; set; }
        public string password { get; set; }
        public string packetSize { get; set; }
    }

    public class PrintConsNoteParam
    {        
        public string enterpriseId { get; set; }
        public string userloggedIn { get; set; }
        public string consignmentList { get; set; }

    }

    public class PrintConsNoteResult
    {
        public PrintConsNoteParam paramFilter { get; set; }
        public List<PrintConsNoteDetail> paramDetail { get; set; }
    }

    public class ReprintConsNoteResult
    {
        public List<ReprintConsNoteDetail> paramDetail { get; set; }
    }

}
