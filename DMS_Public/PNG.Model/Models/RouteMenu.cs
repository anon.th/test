﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PNG.Model.Models
{
    public class RouteMenu
    {
        public string RouteName { get; set; }
        public string TemplateUrl { get; set; }
        public int? ResolverId { get; set; }
    }
}
