﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PNG.Model.Models
{
    public class Module
    {
        public string ModuleId { get; set; }
        public string ModuleName { get; set; }
        public string ModuleIconImage { get; set; }
        public string ModuleNameStyle { get; set; }
        public string ModuleLinkURL { get; set; }
        public string ModuleRights { get; set; }
    }    
}
