//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PNG.Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class Role_Master
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Role_Master()
        {
            this.User_Role_Relation = new HashSet<User_Role_Relation>();
            this.Module_Tree_Role_Relation = new HashSet<Module_Tree_Role_Relation>();
        }
    
        public string applicationid { get; set; }
        public string enterpriseid { get; set; }
        public int roleid { get; set; }
        public string role_name { get; set; }
        public string role_description { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<User_Role_Relation> User_Role_Relation { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Module_Tree_Role_Relation> Module_Tree_Role_Relation { get; set; }
        public virtual Enterprise Enterprise { get; set; }
    }
}
