﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SqlMapperExtensions.cs" company="Aware Corporation Limited">
//   Copyright (c) 2014-2015.  All rights reserved.
// </copyright>
// <summary>
//   Defines the SqlMapperExtensions type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using Dapper;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

// XML comments
#pragma warning disable 1573, 1591
namespace PNG.Dapper.Extensions.Utils
{
    /// <summary>
    /// The sql mapper extensions.
    /// </summary>
    public static partial class SqlMapperExtensions
    {
        /// <summary>
        /// The Proxy interface.
        /// </summary>
        public interface IProxy
        {
            /// <summary>
            /// Gets or sets a value indicating whether is dirty.
            /// </summary>
            bool IsDirty { get; set; }
        }

        /// <summary>
        /// The key properties.
        /// </summary>
        public static ConcurrentDictionary<RuntimeTypeHandle, IEnumerable<PropertyInfo>> KeyProperties =
            new ConcurrentDictionary<RuntimeTypeHandle, IEnumerable<PropertyInfo>>();

        /// <summary>
        /// The type properties.
        /// </summary>
        public static ConcurrentDictionary<RuntimeTypeHandle, IEnumerable<PropertyInfo>> TypeProperties =
            new ConcurrentDictionary<RuntimeTypeHandle, IEnumerable<PropertyInfo>>();

        /// <summary>
        /// The computed properties.
        /// </summary>
        public static ConcurrentDictionary<RuntimeTypeHandle, IEnumerable<PropertyInfo>> ComputedProperties =
            new ConcurrentDictionary<RuntimeTypeHandle, IEnumerable<PropertyInfo>>();

        /// <summary>
        /// The auto increment properties.
        /// </summary>
        public static ConcurrentDictionary<RuntimeTypeHandle, IEnumerable<PropertyInfo>> AutoIncrementProperties =
            new ConcurrentDictionary<RuntimeTypeHandle, IEnumerable<PropertyInfo>>();

        /// <summary>
        /// The ignored properties.
        /// </summary>
        public static ConcurrentDictionary<RuntimeTypeHandle, IEnumerable<PropertyInfo>> IgnoredProperties =
            new ConcurrentDictionary<RuntimeTypeHandle, IEnumerable<PropertyInfo>>();

        /// <summary>
        /// The get queries.
        /// </summary>
        public static ConcurrentDictionary<RuntimeTypeHandle, string> GetQueries =
            new ConcurrentDictionary<RuntimeTypeHandle, string>();

        /// <summary>
        /// The type table name.
        /// </summary>
        public static ConcurrentDictionary<RuntimeTypeHandle, string> TypeTableName =
            new ConcurrentDictionary<RuntimeTypeHandle, string>();

        /// <summary>
        /// The adapter dictionary.
        /// </summary>
        private static readonly Dictionary<string, ISqlAdapter> AdapterDictionary =
            new Dictionary<string, ISqlAdapter>
            {
                { "sqlconnection", new SqlServerAdapter()},
                { "npgsqlconnection", new PostgresAdapter()},
                { "sqliteconnection", new SqLiteAdapter()}
            };

        /// <summary>
        /// The computed properties cache.
        /// </summary>
        /// <param name="type">
        /// The type.
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>IEnumerable</cref>
        ///     </see>
        ///     .
        /// </returns>
        private static IEnumerable<PropertyInfo> ComputedPropertiesCache(Type type)
        {
            IEnumerable<PropertyInfo> pi;
            if (ComputedProperties.TryGetValue(type.TypeHandle, out pi))
            {
                return pi;
            }

            var computedProperties = TypePropertiesCache(type).Where(p => p.GetCustomAttributes(true).Any(a => a is ComputedAttribute)).ToList();

            ComputedProperties[type.TypeHandle] = computedProperties;
            return computedProperties;
        }

        /// <summary>
        /// The key properties cache.
        /// </summary>
        /// <param name="type">
        /// The type.
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>IEnumerable</cref>
        ///     </see>
        ///     .
        /// </returns>
        private static IEnumerable<PropertyInfo> KeyPropertiesCache(Type type)
        {
            IEnumerable<PropertyInfo> pi;

            if (KeyProperties.TryGetValue(type.TypeHandle, out pi))
            {
                return pi;
            }

            var allProperties = TypePropertiesCache(type);
            var propertyInfos = allProperties as PropertyInfo[] ?? allProperties.ToArray();
            var keyProperties = propertyInfos.Where(p => p.GetCustomAttributes(true).Any(a => a is KeyAttribute)).ToList();

            if (keyProperties.Count == 0)
            {
                var idProp = propertyInfos.FirstOrDefault(p => p.Name.ToLower() == "id");

                if (idProp != null)
                {
                    keyProperties.Add(idProp);
                }
            }

            KeyProperties[type.TypeHandle] = keyProperties;
            return keyProperties;
        }

        /// <summary>
        /// The type properties cache.
        /// </summary>
        /// <param name="type">
        /// The type.
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>IEnumerable</cref>
        ///     </see>
        ///     .
        /// </returns>
        private static IEnumerable<PropertyInfo> TypePropertiesCache(Type type)
        {
            IEnumerable<PropertyInfo> pis;
            if (TypeProperties.TryGetValue(type.TypeHandle, out pis))
            {
                return pis;
            }

            var properties = type.GetProperties().Where(IsWriteable).ToArray();
            TypeProperties[type.TypeHandle] = properties;
            return properties;
        }

        /// <summary>
        /// The auto increment properties cache.
        /// </summary>
        /// <param name="type">
        /// The type.
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>IEnumerable</cref>
        ///     </see>
        ///     .
        /// </returns>
        private static IEnumerable<PropertyInfo> AutoIncrementPropertiesCache(Type type)
        {
            IEnumerable<PropertyInfo> pi;
            if (AutoIncrementProperties.TryGetValue(type.TypeHandle, out pi))
            {
                return pi;
            }

            var autoIncrementProperties = TypePropertiesCache(type).Where(p => p.GetCustomAttributes(true).Any(a => a is AutoIncrementAttribute)).ToList();

            AutoIncrementProperties[type.TypeHandle] = autoIncrementProperties;
            return autoIncrementProperties;
        }

        /// <summary>
        /// The ignored properties cache.
        /// </summary>
        /// <param name="type">
        /// The type.
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>IEnumerable</cref>
        ///     </see>
        ///     .
        /// </returns>
        private static IEnumerable<PropertyInfo> IgnoredPropertiesCache(Type type)
        {
            IEnumerable<PropertyInfo> pi;
            if (IgnoredProperties.TryGetValue(type.TypeHandle, out pi))
            {
                return pi;
            }

            var ignoredProperties = TypePropertiesCache(type).Where(p => p.GetCustomAttributes(true).Any(a => a is IgnoredAttribute)).ToList();

            IgnoredProperties[type.TypeHandle] = ignoredProperties;
            return ignoredProperties;
        }

        /// <summary>
        /// The is writeable.
        /// </summary>
        /// <param name="pi">
        /// The pi.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public static bool IsWriteable(PropertyInfo pi)
        {
            var attributes = pi.GetCustomAttributes(typeof(WriteAttribute), false);

            if (attributes.Length != 1)
                return true;

            var write = (WriteAttribute)attributes[0];
            return write.Write;
        }

        /// <summary>
        /// Returns all entities from table "Ts". T must be of interface type. 
        /// Created entity is tracked/intercepted for changes and used by the Update() extension. 
        /// </summary>
        /// <typeparam name="T">
        /// Interface type to create and populate
        /// </typeparam>
        /// <param name="connection">
        /// Open SqlConnection
        /// </param>
        /// <param name="transaction">
        /// The transaction.
        /// </param>
        /// <param name="commandTimeout">
        /// The command Timeout.
        /// </param>
        /// <returns>
        /// Entity of T
        /// </returns>
        public static IEnumerable<T> GetAll<T>(this IDbConnection connection, IDbTransaction transaction = null, int? commandTimeout = null)
            where T : class
        {
            var type = typeof(T);
            string sql;
            if (!GetQueries.TryGetValue(type.TypeHandle, out sql))
            {
                var keys = KeyPropertiesCache(type);
                var propertyInfos = keys as PropertyInfo[] ?? keys.ToArray();

                if (propertyInfos.Count() > 1)
                    throw new DataException("Get<T> only supports an entity with a single [Key] property");
                if (!propertyInfos.Any())
                    throw new DataException("Get<T> only supports en entity with a [Key] property");

                var name = GetTableName(type);

                // pluralizer 
                // query information schema and only select fields that are both in information schema and underlying class / interface 
                sql = "SELECT * FROM " + name + " WITH(NOLOCK) ";
                GetQueries[type.TypeHandle] = sql;
            }

            IEnumerable<T> obj;

            if (type.IsInterface)
            {
                var res = connection.Query(sql).FirstOrDefault() as IDictionary<string, object>;

                if (res == null)
                    return null;

                obj = ProxyGenerator.GetInterfaceProxy<IEnumerable<T>>();

                foreach (var property in TypePropertiesCache(type))
                {
                    var val = res[property.Name];
                    property.SetValue(obj, val, null);
                }

                // reset change tracking and return
                // ReSharper disable once SuspiciousTypeConversion.Global
                ((IProxy)obj).IsDirty = false;
            }
            else
            {
                obj = connection.Query<T>(sql, transaction: transaction, commandTimeout: commandTimeout);
            }

            return obj;
        }

        /// <summary>
        /// Returns a single entity by a single id from table "Ts". T must be of interface type. 
        /// Id must be marked with [Key] attribute.
        /// Created entity is tracked/intercepted for changes and used by the Update() extension. 
        /// </summary>
        /// <typeparam name="T">
        /// Interface type to create and populate
        /// </typeparam>
        /// <param name="connection">
        /// Open SqlConnection
        /// </param>
        /// <param name="id">
        /// Id of the entity to get, must be marked with [Key] attribute
        /// </param>
        /// <param name="transaction">
        /// The transaction.
        /// </param>
        /// <param name="commandTimeout">
        /// The command Timeout.
        /// </param>
        /// <returns>
        /// Entity of T
        /// </returns>
        public static T Get<T>(this IDbConnection connection, dynamic id, IDbTransaction transaction = null, int? commandTimeout = null)
            where T : class
        {
            var type = typeof(T);
            string sql;
            if (!GetQueries.TryGetValue(type.TypeHandle, out sql))
            {
                var keys = KeyPropertiesCache(type);
                var propertyInfos = keys as PropertyInfo[] ?? keys.ToArray();

                if (propertyInfos.Count() > 1)
                    throw new DataException("Get<T> only supports an entity with a single [Key] property");
                if (!propertyInfos.Any())
                    throw new DataException("Get<T> only supports en entity with a [Key] property");

                var onlyKey = propertyInfos.First();

                var name = GetTableName(type);

                // pluralizer 
                // query information schema and only select fields that are both in information schema and underlying class / interface 
                sql = "SELECT * FROM " + name + " WITH(NOLOCK) " + " WHERE " + onlyKey.Name + " = @id";
                GetQueries[type.TypeHandle] = sql;
            }

            var dynParms = new DynamicParameters();
            dynParms.Add("@id", id);

            T obj;

            if (type.IsInterface)
            {
                var res = connection.Query(sql, dynParms).FirstOrDefault() as IDictionary<string, object>;

                if (res == null)
                    return null;

                obj = ProxyGenerator.GetInterfaceProxy<T>();

                foreach (var property in TypePropertiesCache(type))
                {
                    var val = res[property.Name];
                    property.SetValue(obj, val, null);
                }

                // reset change tracking and return
                ((IProxy)obj).IsDirty = false;
            }
            else
            {
                obj = connection.Query<T>(sql, dynParms, transaction, commandTimeout: commandTimeout).FirstOrDefault();
            }

            return obj;
        }

        /// <summary>
        /// Returns a single entity by a single id from table "Ts" asynchronously using .NET 4.5 Task. T must be of interface type. 
        /// Id must be marked with [Key] attribute.
        /// Created entity is tracked/intercepted for changes and used by the Update() extension. 
        /// </summary>
        /// <typeparam name="T">
        /// Interface type to create and populate
        /// </typeparam>
        /// <param name="connection">
        /// Open SqlConnection
        /// </param>
        /// <param name="id">
        /// Id of the entity to get, must be marked with [Key] attribute
        /// </param>
        /// <param name="transaction">
        /// The transaction.
        /// </param>
        /// <param name="commandTimeout">
        /// The command Timeout.
        /// </param>
        /// <returns>
        /// Entity of T
        /// </returns>
        public static async Task<T> GetAsync<T>(this IDbConnection connection, dynamic id, IDbTransaction transaction = null, int? commandTimeout = null)
            where T : class
        {
            var type = typeof(T);
            string sql;
            if (!GetQueries.TryGetValue(type.TypeHandle, out sql))
            {
                var keys = KeyPropertiesCache(type);
                var propertyInfos = keys as PropertyInfo[] ?? keys.ToArray();

                if (propertyInfos.Count() > 1)
                    throw new DataException("Get<T> only supports an entity with a single [Key] property");
                if (!propertyInfos.Any())
                    throw new DataException("Get<T> only supports en entity with a [Key] property");

                var onlyKey = propertyInfos.First();

                var name = GetTableName(type);

                // pluralizer 
                // query information schema and only select fields that are both in information schema and underlying class / interface 
                sql = "SELECT * FROM " + name + " WITH(NOLOCK) " + " WHERE " + onlyKey.Name + " = @id";
                GetQueries[type.TypeHandle] = sql;
            }

            var dynParms = new DynamicParameters();
            dynParms.Add("@id", id);

            T obj;

            if (type.IsInterface)
            {
                var res = (await connection.QueryAsync<dynamic>(sql, dynParms).ConfigureAwait(false)).FirstOrDefault() as IDictionary<string, object>;

                if (res == null)
                    return null;

                obj = ProxyGenerator.GetInterfaceProxy<T>();

                foreach (var property in TypePropertiesCache(type))
                {
                    var val = res[property.Name];
                    property.SetValue(obj, val, null);
                }

                // reset change tracking and return
                ((IProxy)obj).IsDirty = false;
            }
            else
            {
                obj = (await connection.QueryAsync<T>(sql, dynParms, transaction: transaction, commandTimeout: commandTimeout).ConfigureAwait(false)).FirstOrDefault();
            }

            return obj;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="connection"></param>
        /// <param name="queryString"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public static DataSet GetDataSet(this IDbConnection connection, string queryString, SqlParameter[] parameters = null)
        {
            var dataSet = new DataSet();
            var sqlConnection = (SqlConnection)connection;

            if (sqlConnection == null)
                return dataSet;

            var adapter = new SqlDataAdapter();
            var command = new SqlCommand(queryString, sqlConnection);

            if ((parameters != null) && (parameters.Length > 0))
            {
                command.Parameters.AddRange(parameters);
            }

            command.CommandType = CommandType.StoredProcedure;
            adapter.SelectCommand = command;
            adapter.Fill(dataSet);

            return dataSet;
        }

        /// <summary>
        /// The get table name.
        /// </summary>
        /// <param name="type">
        /// The type.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        private static string GetTableName(Type type)
        {
            string name;

            if (TypeTableName.TryGetValue(type.TypeHandle, out name))
                return name;

            name = type.Name.Contains("`") ? type.Name.Substring(0, type.Name.IndexOf("`", StringComparison.OrdinalIgnoreCase)) : type.Name;
            name = string.Format("{0}s", name);

            if (type.IsInterface && name.StartsWith("I", StringComparison.OrdinalIgnoreCase))
            {
                name = name.Substring(1);
            }

            // NOTE: This as dynamic trick should be able to handle both our own Table-attribute as well as the one in EntityFramework 
            var tableAttr = type.GetCustomAttributes(false).SingleOrDefault(attr => attr.GetType().Name == "TableAttribute") as dynamic;

            if (tableAttr != null)
                name = tableAttr.Name;

            TypeTableName[type.TypeHandle] = name;

            return name;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="tableName"></param>
        /// <param name="keyProperties"></param>
        /// <param name="hasAutoIncrement"></param>
        /// <param name="columnList"></param>
        /// <param name="parameterList"></param>
        private static void InsertParamsPreparing<T>(out string tableName, out IEnumerable<PropertyInfo> keyProperties, out bool hasAutoIncrement, out string columnList, out string parameterList)
            where T : class
        {
            var type = typeof(T);
            tableName = GetTableName(type);

            var allProperties = TypePropertiesCache(type);
            keyProperties = KeyPropertiesCache(type);

            var computedProperties = ComputedPropertiesCache(type);
            var propertyInfos = allProperties as PropertyInfo[] ?? allProperties.ToArray();
            var properties = computedProperties as PropertyInfo[] ?? computedProperties.ToArray();
            var allPropertiesExceptKeyAndComputed = propertyInfos.Except(properties);
            var autoIncrementProperties = AutoIncrementPropertiesCache(type);

            hasAutoIncrement = false;

            var incrementProperties = autoIncrementProperties as PropertyInfo[] ?? autoIncrementProperties.ToArray();
            var enumerable = keyProperties as PropertyInfo[] ?? keyProperties.ToArray();

            if (incrementProperties.Any() && enumerable.Any())
            {
                hasAutoIncrement = enumerable.Contains(incrementProperties.First());
            }

            if (hasAutoIncrement)
            {
                allPropertiesExceptKeyAndComputed = propertyInfos.Except(enumerable.Union(properties));
            }

            // Except Ignored properties
            var ignoredProperties = IgnoredPropertiesCache(type);
            var second = ignoredProperties as PropertyInfo[] ?? ignoredProperties.ToArray();

            if (second.Any())
            {
                allPropertiesExceptKeyAndComputed = allPropertiesExceptKeyAndComputed.Except(second);
            }

            var sbColumnList = new StringBuilder(null);
            var sbParameterList = new StringBuilder(null);
            var propertiesExceptKeyAndComputed = allPropertiesExceptKeyAndComputed as PropertyInfo[] ?? allPropertiesExceptKeyAndComputed.ToArray();

            for (var i = 0; i < propertiesExceptKeyAndComputed.Count(); i++)
            {
                var property = propertiesExceptKeyAndComputed.ElementAt(i);

                sbColumnList.AppendFormat("[{0}]", property.Name);
                sbColumnList.Append(", ");

                sbParameterList.AppendFormat("@{0}", property.Name);
                sbParameterList.Append(", ");
            }

            columnList = sbColumnList.ToString();

            if (columnList.Length > 0)
                columnList = columnList.Substring(0, sbColumnList.Length - 2);

            parameterList = sbParameterList.ToString();

            if (parameterList.Length > 0)
                parameterList = parameterList.Substring(0, parameterList.Length - 2);
        }

        /// <summary>
        /// Inserts an entity into table "Ts" and returns identity id.
        /// </summary>
        /// <typeparam name="T">
        /// </typeparam>
        /// <param name="connection">
        /// Open SqlConnection
        /// </param>
        /// <param name="entityToInsert">
        /// Entity to insert
        /// </param>
        /// <param name="transaction">
        /// The transaction.
        /// </param>
        /// <param name="commandTimeout">
        /// The command Timeout.
        /// </param>
        /// <returns>
        /// Identity of inserted entity
        /// </returns>
        public static long Insert<T>(this IDbConnection connection, T entityToInsert, IDbTransaction transaction = null, int? commandTimeout = null)
            where T : class
        {
            IEnumerable<PropertyInfo> keyProperties;
            string tableName, columnList, parameterList;
            bool hasAutoIncrement;

            // insert all parameters preparing
            InsertParamsPreparing<T>(out tableName, out keyProperties, out hasAutoIncrement, out columnList, out parameterList);

            var adapter = GetFormatter(connection);
            var id = adapter.Insert(connection, transaction, commandTimeout, tableName,
                columnList, parameterList, keyProperties, entityToInsert, hasAutoIncrement);

            return id;
        }

        /// <summary>
        /// Inserts an entity into table "Ts" asynchronously using .NET 4.5 Task and returns identity id.
        /// </summary>
        /// <param name="connection">
        /// Open SqlConnection
        /// </param>
        /// <param name="entityToInsert">
        /// Entity to insert
        /// </param>
        /// <param name="transaction">
        /// The transaction.
        /// </param>
        /// <param name="commandTimeout">
        /// The command Timeout.
        /// </param>
        /// <returns>
        /// Identity of inserted entity
        /// </returns>
        public static Task<long> InsertAsync<T>(this IDbConnection connection, T entityToInsert, IDbTransaction transaction = null, int? commandTimeout = null)
            where T : class
        {
            IEnumerable<PropertyInfo> keyProperties;
            string tableName, columnList, parameterList;
            bool hasAutoIncrement;

            // insert all parameters preparing
            InsertParamsPreparing<T>(out tableName, out keyProperties, out hasAutoIncrement, out columnList, out parameterList);

            var adapter = GetFormatter(connection);
            return adapter.InsertAsync(connection, transaction, commandTimeout, tableName,
                columnList, parameterList, keyProperties, entityToInsert, hasAutoIncrement);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="connection"></param>
        /// <param name="entitiesToInsert"></param>
        /// <param name="transaction"></param>
        /// <param name="commandTimeout"></param>
        /// <returns></returns>
        public static bool BulkInsert<T>(this IDbConnection connection, IEnumerable<T> entitiesToInsert, IDbTransaction transaction = null, int? commandTimeout = null)
            where T : class
        {
            // insert table and statement preparing
            var tableName = GetTableName(typeof(T));
            const bool result = false;

            // create sql bulk copy and options setting
            return entitiesToInsert.BulkCopy(connection, tableName, result, commandTimeout);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="connection"></param>
        /// <param name="entitiesToInsert"></param>
        /// <param name="transaction"></param>
        /// <param name="commandTimeout"></param>
        /// <returns></returns>
        public static Task<bool> BulkInsertAsync<T>(this IDbConnection connection, IEnumerable<T> entitiesToInsert, IDbTransaction transaction = null, int? commandTimeout = null)
            where T : class
        {
            IEnumerable<PropertyInfo> keyProperties;
            string tableName, columnList, parameterList;
            bool hasAutoIncrement;

            // insert all parameters preparing
            InsertParamsPreparing<T>(out tableName, out keyProperties, out hasAutoIncrement, out columnList, out parameterList);

            var adapter = GetFormatter(connection);
            var result = adapter.BulkInsertAsync(connection, transaction, commandTimeout, tableName,
                columnList, parameterList, keyProperties, entitiesToInsert, hasAutoIncrement);

            return result;
        }

        /// <summary>
        /// Updates entity in table "Ts", checks if the entity is modified if the entity is tracked by the Get() extension.
        /// </summary>
        /// <typeparam name="T">
        /// Type to be updated
        /// </typeparam>
        /// <param name="connection">
        /// Open SqlConnection
        /// </param>
        /// <param name="entityToUpdate">
        /// Entity to be updated
        /// </param>
        /// <param name="transaction">
        /// The transaction.
        /// </param>
        /// <param name="commandTimeout">
        /// The command Timeout.
        /// </param>
        /// <returns>
        /// true if updated, false if not found or not modified (tracked entities)
        /// </returns>
        public static bool Update<T>(this IDbConnection connection, T entityToUpdate, IDbTransaction transaction = null, int? commandTimeout = null)
            where T : class
        {
            var proxy = entityToUpdate as IProxy;

            if (proxy != null)
            {
                if (!proxy.IsDirty) return false;
            }

            var type = typeof(T);
            var keyProperties = KeyPropertiesCache(type);
            var propertyInfos = keyProperties as PropertyInfo[] ?? keyProperties.ToArray();

            if (!propertyInfos.Any())
                throw new ArgumentException("Entity must have at least one [Key] property");

            var name = GetTableName(type);
            var sbUpdate = new StringBuilder();
            sbUpdate.AppendFormat("UPDATE {0} SET ", name);

            var allProperties = TypePropertiesCache(type);
            var computedProperties = ComputedPropertiesCache(type);
            var nonIdProps = allProperties.Except(propertyInfos.Union(computedProperties));

            // Except Ignored properties
            var ignoredProperties = IgnoredPropertiesCache(type);
            var properties = ignoredProperties as PropertyInfo[] ?? ignoredProperties.ToArray();

            if (properties.Any())
            {
                nonIdProps = nonIdProps.Except(properties);
            }

            var idProps = nonIdProps as PropertyInfo[] ?? nonIdProps.ToArray();

            for (var i = 0; i < idProps.Count(); i++)
            {
                var property = idProps.ElementAt(i);

                sbUpdate.AppendFormat("{0} = @{1}", property.Name, property.Name);
                sbUpdate.Append(", ");
            }

            var strSql = sbUpdate.ToString();

            if (strSql.Length > 0)
                strSql = strSql.Substring(0, strSql.Length - 2);

            var sbWhere = new StringBuilder();
            sbWhere.Append(" WHERE ");

            for (var i = 0; i < propertyInfos.Count(); i++)
            {
                var property = propertyInfos.ElementAt(i);

                sbWhere.AppendFormat("{0} = @{1}", property.Name, property.Name);

                if (i < propertyInfos.Count() - 1)
                    sbWhere.AppendFormat(" AND ");
            }

            strSql += sbWhere.ToString();

            var updated = connection.Execute(strSql, entityToUpdate, commandTimeout: commandTimeout, transaction: transaction);
            return updated > 0;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="connection"></param>
        /// <param name="entityToUpdate"></param>
        /// <param name="fieldToUpdate"></param>
        /// <param name="transaction"></param>
        /// <param name="commandTimeout"></param>
        /// <returns></returns>
        public static bool Update<T>(this IDbConnection connection, T entityToUpdate, List<string> fieldToUpdate, IDbTransaction transaction = null, int? commandTimeout = null)
            where T : class
        {
            var proxy = entityToUpdate as IProxy;

            if (proxy != null)
            {
                if (!proxy.IsDirty) return false;
            }

            var type = typeof(T);
            var keyProperties = KeyPropertiesCache(type);
            var propertyInfos = keyProperties as PropertyInfo[] ?? keyProperties.ToArray();

            if (!propertyInfos.Any())
                throw new ArgumentException("Entity must have at least one [Key] property");

            var name = GetTableName(type);
            var sbUpdate = new StringBuilder();

            sbUpdate.AppendFormat("UPDATE {0} SET ", name);

            var allProperties = TypePropertiesCache(type);
            var computedProperties = ComputedPropertiesCache(type);
            var nonIdProps = allProperties.Except(propertyInfos.Union(computedProperties));

            // Except Ignored properties
            var ignoredProperties = IgnoredPropertiesCache(type);
            var properties = ignoredProperties as PropertyInfo[] ?? ignoredProperties.ToArray();

            if (properties.Any())
            {
                nonIdProps = nonIdProps.Except(properties);
            }

            var updatedCount = 0;
            var idProps = nonIdProps as PropertyInfo[] ?? nonIdProps.ToArray();

            for (var i = 0; i < idProps.Count(); i++)
            {
                var property = idProps.ElementAt(i);

                if (!fieldToUpdate.Contains(property.Name))
                    continue;

                updatedCount++;
                sbUpdate.AppendFormat("{0} = @{1}", property.Name, property.Name);
                sbUpdate.Append(", ");
            }

            if (fieldToUpdate.Count() != updatedCount)
            {
                throw new ArgumentException("Update field name doesn't match");
            }

            var strSql = sbUpdate.ToString();

            if (strSql.Length > 0)
                strSql = strSql.Substring(0, strSql.Length - 2);

            var sbWhere = new StringBuilder();
            sbWhere.Append(" WHERE ");

            for (var i = 0; i < propertyInfos.Count(); i++)
            {
                var property = propertyInfos.ElementAt(i);

                sbWhere.AppendFormat("{0} = @{1}", property.Name, property.Name);

                if (i < propertyInfos.Count() - 1)
                    sbWhere.AppendFormat(" AND ");
            }

            strSql += sbWhere.ToString();

            var updated = connection.Execute(strSql, entityToUpdate, commandTimeout: commandTimeout, transaction: transaction);
            return updated > 0;
        }

        /// <summary>
        /// Updates entity in table "Ts" asynchronously using .NET 4.5 Task, checks if the entity is modified if the entity is tracked by the Get() extension.
        /// </summary>
        /// <typeparam name="T">
        /// Type to be updated
        /// </typeparam>
        /// <param name="connection">
        /// Open SqlConnection
        /// </param>
        /// <param name="entityToUpdate">
        /// Entity to be updated
        /// </param>
        /// <param name="transaction">
        /// The transaction.
        /// </param>
        /// <param name="commandTimeout">
        /// The command Timeout.
        /// </param>
        /// <returns>
        /// true if updated, false if not found or not modified (tracked entities)
        /// </returns>
        public static async Task<bool> UpdateAsync<T>(this IDbConnection connection, T entityToUpdate, IDbTransaction transaction = null, int? commandTimeout = null)
            where T : class
        {
            var proxy = entityToUpdate as IProxy;

            if (proxy != null)
            {
                if (!proxy.IsDirty) return false;
            }

            var type = typeof(T);
            var keyProperties = KeyPropertiesCache(type);
            var propertyInfos = keyProperties as PropertyInfo[] ?? keyProperties.ToArray();

            if (!propertyInfos.Any())
                throw new ArgumentException("Entity must have at least one [Key] property");

            var name = GetTableName(type);
            var sbUpdate = new StringBuilder();

            sbUpdate.AppendFormat("UPDATE {0} SET ", name);

            var allProperties = TypePropertiesCache(type);
            var computedProperties = ComputedPropertiesCache(type);
            var nonIdProps = allProperties.Except(propertyInfos.Union(computedProperties));

            // Except Ignored properties
            var ignoredProperties = IgnoredPropertiesCache(type);
            var properties = ignoredProperties as PropertyInfo[] ?? ignoredProperties.ToArray();

            if (properties.Any())
            {
                nonIdProps = nonIdProps.Except(properties);
            }

            var idProps = nonIdProps as PropertyInfo[] ?? nonIdProps.ToArray();

            for (var i = 0; i < idProps.Count(); i++)
            {
                var property = idProps.ElementAt(i);

                sbUpdate.AppendFormat("{0} = @{1}", property.Name, property.Name);
                sbUpdate.Append(", ");
            }

            var strSql = sbUpdate.ToString();

            if (strSql.Length > 0)
                strSql = strSql.Substring(0, strSql.Length - 2);

            var sbWhere = new StringBuilder();
            sbWhere.Append(" WHERE ");

            for (var i = 0; i < propertyInfos.Count(); i++)
            {
                var property = propertyInfos.ElementAt(i);

                sbWhere.AppendFormat("{0} = @{1}", property.Name, property.Name);

                if (i < propertyInfos.Count() - 1)
                    sbWhere.AppendFormat(" AND ");
            }

            strSql += sbWhere.ToString();

            var updated = await connection.ExecuteAsync(strSql, entityToUpdate, commandTimeout: commandTimeout, transaction: transaction).ConfigureAwait(false);
            return updated > 0;
        }

        /// <summary>
        /// Delete entity in table "Ts".
        /// </summary>
        /// <typeparam name="T">
        /// Type of entity
        /// </typeparam>
        /// <param name="connection">
        /// Open SqlConnection
        /// </param>
        /// <param name="entityToDelete">
        /// Entity to delete
        /// </param>
        /// <param name="transaction">
        /// The transaction.
        /// </param>
        /// <param name="commandTimeout">
        /// The command Timeout.
        /// </param>
        /// <returns>
        /// true if deleted, false if not found
        /// </returns>
        public static bool Delete<T>(this IDbConnection connection, T entityToDelete, IDbTransaction transaction = null, int? commandTimeout = null)
            where T : class
        {
            if (entityToDelete == null)
                throw new ArgumentException("Cannot Delete null Object", "entityToDelete");

            var type = typeof(T);
            var keyProperties = KeyPropertiesCache(type);
            var propertyInfos = keyProperties as PropertyInfo[] ?? keyProperties.ToArray();

            if (!propertyInfos.Any())
                throw new ArgumentException("Entity must have at least one [Key] property");

            var name = GetTableName(type);
            var sb = new StringBuilder();

            sb.AppendFormat("DELETE FROM {0} WHERE ", name);

            for (var i = 0; i < propertyInfos.Count(); i++)
            {
                var property = propertyInfos.ElementAt(i);

                sb.AppendFormat("{0} = @{1}", property.Name, property.Name);

                if (i < propertyInfos.Count() - 1)
                    sb.AppendFormat(" AND ");
            }

            var deleted = connection.Execute(sb.ToString(), entityToDelete, transaction, commandTimeout);
            return deleted > 0;
        }

        /// <summary>
        /// Delete entity in table "Ts" asynchronously using .NET 4.5 Task.
        /// </summary>
        /// <typeparam name="T">
        /// Type of entity
        /// </typeparam>
        /// <param name="connection">
        /// Open SqlConnection
        /// </param>
        /// <param name="entityToDelete">
        /// Entity to delete
        /// </param>
        /// <param name="transaction">
        /// The transaction.
        /// </param>
        /// <param name="commandTimeout">
        /// The command Timeout.
        /// </param>
        /// <returns>
        /// true if deleted, false if not found
        /// </returns>
        public static async Task<bool> DeleteAsync<T>(this IDbConnection connection, T entityToDelete, IDbTransaction transaction = null, int? commandTimeout = null)
            where T : class
        {
            if (entityToDelete == null)
                throw new ArgumentException("Cannot Delete null Object", "entityToDelete");

            var type = typeof(T);
            var keyProperties = KeyPropertiesCache(type);
            var propertyInfos = keyProperties as PropertyInfo[] ?? keyProperties.ToArray();

            if (!propertyInfos.Any())
                throw new ArgumentException("Entity must have at least one [Key] property");

            var name = GetTableName(type);
            var sb = new StringBuilder();

            sb.AppendFormat("DELETE FROM {0} WHERE ", name);

            for (var i = 0; i < propertyInfos.Count(); i++)
            {
                var property = propertyInfos.ElementAt(i);

                sb.AppendFormat("{0} = @{1}", property.Name, property.Name);

                if (i < propertyInfos.Count() - 1)
                    sb.AppendFormat(" AND ");
            }

            var deleted = await connection.ExecuteAsync(sb.ToString(), entityToDelete, transaction, commandTimeout).ConfigureAwait(false);
            return deleted > 0;
        }

        /// <summary>
        /// Delete all entities in the table related to the type T.
        /// </summary>
        /// <typeparam name="T">
        /// Type of entity
        /// </typeparam>
        /// <param name="connection">
        /// Open SqlConnection
        /// </param>
        /// <param name="transaction">
        /// The transaction.
        /// </param>
        /// <param name="commandTimeout">
        /// The command Timeout.
        /// </param>
        /// <returns>
        /// true if deleted, false if none found
        /// </returns>
        public static bool DeleteAll<T>(this IDbConnection connection, IDbTransaction transaction = null, int? commandTimeout = null)
            where T : class
        {
            var type = typeof(T);
            var name = GetTableName(type);
            var statement = String.Format("DELETE FROM {0}", name);
            var deleted = connection.Execute(statement, null, transaction, commandTimeout);

            return deleted > 0;
        }

        /// <summary>
        /// Delete all entities in the table related to the type T asynchronously using .NET 4.5 Task.
        /// </summary>
        /// <typeparam name="T">
        /// Type of entity
        /// </typeparam>
        /// <param name="connection">
        /// Open SqlConnection
        /// </param>
        /// <param name="transaction">
        /// The transaction.
        /// </param>
        /// <param name="commandTimeout">
        /// The command Timeout.
        /// </param>
        /// <returns>
        /// true if deleted, false if none found
        /// </returns>
        public static async Task<bool> DeleteAllAsync<T>(this IDbConnection connection, IDbTransaction transaction = null, int? commandTimeout = null)
            where T : class
        {
            var type = typeof(T);
            var name = GetTableName(type);
            var statement = String.Format("DELETE FROM {0}", name);
            var deleted = await connection.ExecuteAsync(statement, null, transaction, commandTimeout).ConfigureAwait(false);
            return deleted > 0;
        }

        /// <summary>
        /// The get formatter.
        /// </summary>
        /// <param name="connection">
        /// The connection.
        /// </param>
        /// <returns>
        /// The <see cref="ISqlAdapter"/>.
        /// </returns>
        public static ISqlAdapter GetFormatter(IDbConnection connection)
        {
            string name = connection.GetType().Name.ToLower();
            if (!AdapterDictionary.ContainsKey(name))
                return new SqlServerAdapter();
            return AdapterDictionary[name];
        }

        /// <summary>
        /// The proxy generator.
        /// </summary>
        internal class ProxyGenerator
        {
            /// <summary>
            /// The type cache.
            /// </summary>
            private static readonly Dictionary<Type, object> TypeCache = new Dictionary<Type, object>();

            /// <summary>
            /// The get asm builder.
            /// </summary>
            /// <param name="name">
            /// The name.
            /// </param>
            /// <returns>
            /// The <see cref="AssemblyBuilder"/>.
            /// </returns>
            private static AssemblyBuilder GetAsmBuilder(string name)
            {
                var assemblyBuilder = Thread.GetDomain().DefineDynamicAssembly(new AssemblyName { Name = name },
                    AssemblyBuilderAccess.Run);       // NOTE: to save, use RunAndSave

                return assemblyBuilder;
            }

            /// <summary>
            /// The get class proxy.
            /// </summary>
            /// <typeparam name="T">
            /// </typeparam>
            /// <returns>
            /// The <see cref="T"/>.
            /// </returns>
            /// <exception cref="NotImplementedException">
            /// </exception>
            public static T GetClassProxy<T>()
            {
                // A class proxy could be implemented if all properties are virtual
                // otherwise there is a pretty dangerous case where internal actions will not update dirty tracking
                throw new NotImplementedException();
            }

            /// <summary>
            /// The get interface proxy.
            /// </summary>
            /// <typeparam name="T">
            /// </typeparam>
            /// <returns>
            /// The <see cref="T"/>.
            /// </returns>
            public static T GetInterfaceProxy<T>()
            {
                var typeOfT = typeof(T);
                object k;

                if (TypeCache.TryGetValue(typeOfT, out k))
                {
                    return (T)k;
                }

                var assemblyBuilder = GetAsmBuilder(typeOfT.Name);
                var moduleBuilder = assemblyBuilder.DefineDynamicModule("SqlMapperExtensions." + typeOfT.Name); //NOTE: to save, add "asdasd.dll" parameter
                var interfaceType = typeof(IProxy);
                var typeBuilder = moduleBuilder.DefineType(typeOfT.Name + "_" + Guid.NewGuid(),
                    TypeAttributes.Public | TypeAttributes.Class);

                typeBuilder.AddInterfaceImplementation(typeOfT);
                typeBuilder.AddInterfaceImplementation(interfaceType);

                // create our _isDirty field, which implements IProxy
                var setIsDirtyMethod = CreateIsDirtyProperty(typeBuilder);

                // Generate a field for each property, which implements the T
                foreach (var property in typeof(T).GetProperties())
                {
                    var isId = property.GetCustomAttributes(true).Any(a => a is KeyAttribute);
                    CreateProperty<T>(typeBuilder, property.Name, property.PropertyType, setIsDirtyMethod, isId);
                }

                var generatedType = typeBuilder.CreateType();

                // NOTE: to save, uncomment
                //assemblyBuilder.Save(name + ".dll");  

                var generatedObject = Activator.CreateInstance(generatedType);

                TypeCache.Add(typeOfT, generatedObject);
                return (T)generatedObject;
            }

            /// <summary>
            /// The create is dirty property.
            /// </summary>
            /// <param name="typeBuilder">
            /// The type builder.
            /// </param>
            /// <returns>
            /// The <see cref="MethodInfo"/>.
            /// </returns>
            private static MethodInfo CreateIsDirtyProperty(TypeBuilder typeBuilder)
            {
                var propType = typeof(bool);
                var field = typeBuilder.DefineField("_" + "IsDirty", propType, FieldAttributes.Private);
                var property = typeBuilder.DefineProperty("IsDirty",
                                               System.Reflection.PropertyAttributes.None,
                                               propType,
                                               new[] { propType });

                const MethodAttributes getSetAttr = MethodAttributes.Public | MethodAttributes.NewSlot | MethodAttributes.SpecialName |
                                                    MethodAttributes.Final | MethodAttributes.Virtual | MethodAttributes.HideBySig;

                // Define the "get" and "set" accessor methods
                var currGetPropMthdBldr = typeBuilder.DefineMethod("get_" + "IsDirty",
                                             getSetAttr,
                                             propType,
                                             Type.EmptyTypes);
                var currGetIl = currGetPropMthdBldr.GetILGenerator();
                currGetIl.Emit(OpCodes.Ldarg_0);
                currGetIl.Emit(OpCodes.Ldfld, field);
                currGetIl.Emit(OpCodes.Ret);
                var currSetPropMthdBldr = typeBuilder.DefineMethod("set_" + "IsDirty",
                                             getSetAttr,
                                             null,
                                             new[] { propType });
                var currSetIl = currSetPropMthdBldr.GetILGenerator();
                currSetIl.Emit(OpCodes.Ldarg_0);
                currSetIl.Emit(OpCodes.Ldarg_1);
                currSetIl.Emit(OpCodes.Stfld, field);
                currSetIl.Emit(OpCodes.Ret);

                property.SetGetMethod(currGetPropMthdBldr);
                property.SetSetMethod(currSetPropMthdBldr);
                var getMethod = typeof(IProxy).GetMethod("get_" + "IsDirty");
                var setMethod = typeof(IProxy).GetMethod("set_" + "IsDirty");
                typeBuilder.DefineMethodOverride(currGetPropMthdBldr, getMethod);
                typeBuilder.DefineMethodOverride(currSetPropMthdBldr, setMethod);

                return currSetPropMthdBldr;
            }

            /// <summary>
            /// 
            /// </summary>
            /// <typeparam name="T"></typeparam>
            /// <param name="typeBuilder"></param>
            /// <param name="propertyName"></param>
            /// <param name="propType"></param>
            /// <param name="setIsDirtyMethod"></param>
            /// <param name="isIdentity"></param>
            private static void CreateProperty<T>(TypeBuilder typeBuilder, string propertyName, Type propType, MethodInfo setIsDirtyMethod, bool isIdentity)
            {
                //Define the field and the property 
                var field = typeBuilder.DefineField("_" + propertyName, propType, FieldAttributes.Private);
                var property = typeBuilder.DefineProperty(propertyName,
                                               System.Reflection.PropertyAttributes.None,
                                               propType,
                                               new[] { propType });

                const MethodAttributes getSetAttr = MethodAttributes.Public | MethodAttributes.Virtual |
                                                    MethodAttributes.HideBySig;

                // Define the "get" and "set" accessor methods
                var currGetPropMthdBldr = typeBuilder.DefineMethod("get_" + propertyName,
                                             getSetAttr,
                                             propType,
                                             Type.EmptyTypes);

                var currGetIl = currGetPropMthdBldr.GetILGenerator();
                currGetIl.Emit(OpCodes.Ldarg_0);
                currGetIl.Emit(OpCodes.Ldfld, field);
                currGetIl.Emit(OpCodes.Ret);

                var currSetPropMthdBldr = typeBuilder.DefineMethod("set_" + propertyName,
                                             getSetAttr,
                                             null,
                                             new[] { propType });

                //store value in private field and set the isdirty flag
                var currSetIl = currSetPropMthdBldr.GetILGenerator();
                currSetIl.Emit(OpCodes.Ldarg_0);
                currSetIl.Emit(OpCodes.Ldarg_1);
                currSetIl.Emit(OpCodes.Stfld, field);
                currSetIl.Emit(OpCodes.Ldarg_0);
                currSetIl.Emit(OpCodes.Ldc_I4_1);
                currSetIl.Emit(OpCodes.Call, setIsDirtyMethod);
                currSetIl.Emit(OpCodes.Ret);

                // Should copy all attributes defined by the interface?
                if (isIdentity)
                {
                    var keyAttribute = typeof(KeyAttribute);
                    var myConstructorInfo = keyAttribute.GetConstructor(new Type[] { });

                    if (myConstructorInfo != null)
                    {
                        var attributeBuilder = new CustomAttributeBuilder(myConstructorInfo, new object[] { });
                        property.SetCustomAttribute(attributeBuilder);
                    }
                }

                property.SetGetMethod(currGetPropMthdBldr);
                property.SetSetMethod(currSetPropMthdBldr);
                var getMethod = typeof(T).GetMethod("get_" + propertyName);
                var setMethod = typeof(T).GetMethod("set_" + propertyName);
                typeBuilder.DefineMethodOverride(currGetPropMthdBldr, getMethod);
                typeBuilder.DefineMethodOverride(currSetPropMthdBldr, setMethod);
            }
        }
    }

    /// <summary>
    /// The table attribute.
    /// </summary>
    [AttributeUsage(AttributeTargets.Class)]
    public class TableAttribute : Attribute
    {
        public TableAttribute(string tableName)
        {
            Name = tableName;
        }
        public string Name { get; private set; }
    }

    /// <summary>
    /// The key attribute.
    /// do not want to depend on data annotations that is not in client profile
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class KeyAttribute : Attribute
    {
    }

    /// <summary>
    /// The write attribute.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class WriteAttribute : Attribute
    {
        public WriteAttribute(bool write)
        {
            Write = write;
        }
        public bool Write { get; private set; }
    }

    /// <summary>
    /// The computed attribute.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class ComputedAttribute : Attribute
    {
    }

    /// <summary>
    /// The auto increment attribute.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class AutoIncrementAttribute : Attribute
    {
    }

    /// <summary>
    /// The ignored attribute.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class IgnoredAttribute : Attribute
    {
    }

    /// <summary>
    /// Send DataTable as dapper parameter
    /// </summary>
    public class DapperTableParameter : SqlMapper.ICustomQueryParameter
    {
        /// <summary>
        /// 
        /// </summary>
        protected DataTable Table = null;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="table"></param>
        public DapperTableParameter(DataTable table)
        {
            Table = table;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="command"></param>
        /// <param name="name"></param>
        public void AddParameter(IDbCommand command, string name)
        {
            // This is SqlConnection specific
            ((SqlCommand)command).Parameters.Add("@" + name, SqlDbType.Structured).Value = Table;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class DapperTvp<T> : DapperTableParameter
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="list"></param>
        public DapperTvp(IEnumerable<T> list)
            : base(new DataTable())
        {
            var t = typeof(T);
            var propertyByName = new Dictionary<string, PropertyInfo>();

            foreach (var p in t.GetProperties())
            {
                propertyByName.Add(p.Name, p);
                Table.Columns.Add(p.Name, Nullable.GetUnderlyingType(p.PropertyType) ?? p.PropertyType);
            }

            foreach (var i in list)
            {
                var row = Table.NewRow();
                foreach (var p in propertyByName)
                {
                    if (p.Value.GetValue(i) != null)
                    {
                        row[p.Key] = p.Value.GetValue(i, null);
                    }
                    else
                    {
                        row[p.Key] = DBNull.Value;
                    }
                }

                Table.Rows.Add(row);
            }
        }
    }
}

/// <summary>
/// The SqlAdapter interface.
/// </summary>
public interface ISqlAdapter
{
    /// <summary>
    /// The insert.
    /// </summary>
    /// <param name="connection">
    /// The connection.
    /// </param>
    /// <param name="transaction">
    /// The transaction.
    /// </param>
    /// <param name="commandTimeout">
    /// The command timeout.
    /// </param>
    /// <param name="tableName">
    /// The table name.
    /// </param>
    /// <param name="columnList">
    /// The column list.
    /// </param>
    /// <param name="parameterList">
    /// The parameter list.
    /// </param>
    /// <param name="keyProperties">
    /// The key properties.
    /// </param>
    /// <param name="entityToInsert">
    /// The entity to insert.
    /// </param>
    /// <param name="autoIncrement">
    /// The auto increment.
    /// </param>
    /// <returns>
    /// The <see cref="int"/>.
    /// </returns>
    long Insert(IDbConnection connection, IDbTransaction transaction, int? commandTimeout, String tableName, string columnList, string parameterList, IEnumerable<PropertyInfo> keyProperties, object entityToInsert, bool autoIncrement);

    /// <summary>
    /// The insert async.
    /// </summary>
    /// <param name="connection">
    /// The connection.
    /// </param>
    /// <param name="transaction">
    /// The transaction.
    /// </param>
    /// <param name="commandTimeout">
    /// The command timeout.
    /// </param>
    /// <param name="tableName">
    /// The table name.
    /// </param>
    /// <param name="columnList">
    /// The column list.
    /// </param>
    /// <param name="parameterList">
    /// The parameter list.
    /// </param>
    /// <param name="keyProperties">
    /// The key properties.
    /// </param>
    /// <param name="entityToInsert">
    /// The entity to insert.
    /// </param>
    /// <param name="autoIncrement">
    /// The auto increment.
    /// </param>
    /// <returns>
    /// The <see cref="Task"/>.
    /// </returns>
    Task<long> InsertAsync(IDbConnection connection, IDbTransaction transaction, int? commandTimeout, String tableName, string columnList, string parameterList, IEnumerable<PropertyInfo> keyProperties, object entityToInsert, bool autoIncrement);

    /// <summary>
    /// 
    /// </summary>
    /// <param name="connection"></param>
    /// <param name="transaction"></param>
    /// <param name="commandTimeout"></param>
    /// <param name="tableName"></param>
    /// <param name="columnList"></param>
    /// <param name="parameterList"></param>
    /// <param name="keyProperties"></param>
    /// <param name="entitiesToInsert"></param>
    /// <param name="autoIncrement"></param>
    bool BulkInsert(IDbConnection connection, IDbTransaction transaction, int? commandTimeout, String tableName,
        string columnList, string parameterList, IEnumerable<PropertyInfo> keyProperties, IEnumerable<object> entitiesToInsert, bool autoIncrement);

    /// <summary>
    /// 
    /// </summary>
    /// <param name="connection"></param>
    /// <param name="transaction"></param>
    /// <param name="commandTimeout"></param>
    /// <param name="tableName"></param>
    /// <param name="columnList"></param>
    /// <param name="parameterList"></param>
    /// <param name="keyProperties"></param>
    /// <param name="entitiesToInsert"></param>
    /// <param name="autoIncrement"></param>
    /// <returns></returns>
    Task<bool> BulkInsertAsync(IDbConnection connection, IDbTransaction transaction, int? commandTimeout, String tableName,
        string columnList, string parameterList, IEnumerable<PropertyInfo> keyProperties, IEnumerable<object> entitiesToInsert, bool autoIncrement);
}

/// <summary>
/// The sql server adapter.
/// </summary>
public class SqlServerAdapter : ISqlAdapter
{
    /// <summary>
    /// The insert.
    /// </summary>
    /// <param name="connection">
    /// The connection.
    /// </param>
    /// <param name="transaction">
    /// The transaction.
    /// </param>
    /// <param name="commandTimeout">
    /// The command timeout.
    /// </param>
    /// <param name="tableName">
    /// The table name.
    /// </param>
    /// <param name="columnList">
    /// The column list.
    /// </param>
    /// <param name="parameterList">
    /// The parameter list.
    /// </param>
    /// <param name="keyProperties">
    /// The key properties.
    /// </param>
    /// <param name="entityToInsert">
    /// The entity to insert.
    /// </param>
    /// <param name="autoIncrement">
    /// The auto increment.
    /// </param>
    /// <returns>
    /// The <see cref="int"/>.
    /// </returns>
    public long Insert(IDbConnection connection, IDbTransaction transaction, int? commandTimeout, string tableName, string columnList, string parameterList, IEnumerable<PropertyInfo> keyProperties, object entityToInsert, bool autoIncrement)
    {
        var cmd = string.Format("INSERT INTO {0} ({1}) VALUES ({2})", tableName, columnList, parameterList);
        connection.Execute(cmd, entityToInsert, transaction, commandTimeout);

        if (!autoIncrement)
            return 0;

        // NOTE: would prefer to use IDENT_CURRENT('tablename') or IDENT_SCOPE but these are not available on SQLCE
        // var rowId = connection.Query("select @@IDENTITY id", transaction: transaction, commandTimeout: commandTimeout);
        var ident = connection.Query(string.Format(@"SELECT IDENT_CURRENT('{0}') id", tableName), transaction: transaction, commandTimeout: commandTimeout);
        var id = (long)ident.First().id;
        var propertyInfos = keyProperties as PropertyInfo[] ?? keyProperties.ToArray();

        if (!propertyInfos.Any())
            return id;

        var key = propertyInfos.First();

        if (key.PropertyType == typeof(int))
        {
            propertyInfos.First().SetValue(entityToInsert, (int)id, null);
        }
        else if (key.PropertyType == typeof(short))
        {
            propertyInfos.First().SetValue(entityToInsert, (short)id, null);
        }
        else
        {
            propertyInfos.First().SetValue(entityToInsert, id, null);
        }

        return id;
    }

    /// <summary>
    /// The insert async.
    /// </summary>
    /// <param name="connection">
    /// The connection.
    /// </param>
    /// <param name="transaction">
    /// The transaction.
    /// </param>
    /// <param name="commandTimeout">
    /// The command timeout.
    /// </param>
    /// <param name="tableName">
    /// The table name.
    /// </param>
    /// <param name="columnList">
    /// The column list.
    /// </param>
    /// <param name="parameterList">
    /// The parameter list.
    /// </param>
    /// <param name="keyProperties">
    /// The key properties.
    /// </param>
    /// <param name="entityToInsert">
    /// The entity to insert.
    /// </param>
    /// <param name="autoIncrement">
    /// The auto increment.
    /// </param>
    /// <returns>
    /// The <see cref="Task"/>.
    /// </returns>
    public async Task<long> InsertAsync(IDbConnection connection, IDbTransaction transaction, int? commandTimeout, string tableName, string columnList, string parameterList, IEnumerable<PropertyInfo> keyProperties, object entityToInsert, bool autoIncrement)
    {
        var cmd = string.Format("INSERT INTO {0} ({1}) VALUES ({2})", tableName, columnList, parameterList);
        await connection.ExecuteAsync(cmd, entityToInsert, transaction, commandTimeout).ConfigureAwait(false);

        if (!autoIncrement)
            return 0;

        // NOTE: would prefer to use IDENT_CURRENT('tablename') or IDENT_SCOPE but these are not available on SQLCE
        var r = await connection.QueryAsync<dynamic>(string.Format(@"SELECT IDENT_CURRENT('{0}') id", tableName), transaction: transaction, commandTimeout: commandTimeout).ConfigureAwait(false);
        var id = (long)r.First().id;
        var propertyInfos = keyProperties as PropertyInfo[] ?? keyProperties.ToArray();

        if (!propertyInfos.Any())
            return id;

        var key = propertyInfos.First();

        if (key.PropertyType == typeof(int))
        {
            propertyInfos.First().SetValue(entityToInsert, (int)id, null);
        }
        else if (key.PropertyType == typeof(short))
        {
            propertyInfos.First().SetValue(entityToInsert, (short)id, null);
        }
        else
        {
            propertyInfos.First().SetValue(entityToInsert, id, null);
        }

        return id;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="connection"></param>
    /// <param name="transaction"></param>
    /// <param name="commandTimeout"></param>
    /// <param name="tableName"></param>
    /// <param name="columnList"></param>
    /// <param name="parameterList"></param>
    /// <param name="keyProperties"></param>
    /// <param name="entitiesToInsert"></param>
    /// <param name="autoIncrement"></param>
    /// <returns></returns>
    public bool BulkInsert(IDbConnection connection, IDbTransaction transaction, int? commandTimeout, string tableName, string columnList, string parameterList, IEnumerable<PropertyInfo> keyProperties, IEnumerable<object> entitiesToInsert, bool autoIncrement)
    {
        try
        {
            var cmd = new StringBuilder();
            cmd.AppendFormat(" INSERT INTO {0} ", tableName);
            cmd.AppendFormat(" ({0}) ", columnList);
            cmd.AppendFormat(" SELECT {0} ", parameterList);

            var result = connection.Execute(cmd.ToString(), entitiesToInsert, transaction);
            return (result > 0);
        }
        catch (Exception err)
        {
            throw new Exception(err.Message, err);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="connection"></param>
    /// <param name="transaction"></param>
    /// <param name="commandTimeout"></param>
    /// <param name="tableName"></param>
    /// <param name="columnList"></param>
    /// <param name="parameterList"></param>
    /// <param name="keyProperties"></param>
    /// <param name="entitiesToInsert"></param>
    /// <param name="autoIncrement"></param>
    /// <returns></returns>
    public async Task<bool> BulkInsertAsync(IDbConnection connection, IDbTransaction transaction, int? commandTimeout, string tableName, string columnList, string parameterList, IEnumerable<PropertyInfo> keyProperties, IEnumerable<object> entitiesToInsert, bool autoIncrement)
    {
        try
        {
            var cmd = new StringBuilder();
            cmd.AppendFormat(" INSERT INTO {0} ", tableName);
            cmd.AppendFormat(" ({0}) ", columnList);
            cmd.AppendFormat(" SELECT {0} ", parameterList);

            var result = await connection.ExecuteAsync(cmd.ToString(), entitiesToInsert).ConfigureAwait(false);
            return (result > 0);
        }
        catch (Exception err)
        {
            throw new Exception(err.Message, err);
        }
    }
}

/// <summary>
/// The postgres adapter.
/// </summary>
public class PostgresAdapter : ISqlAdapter
{
    /// <summary>
    /// The insert.
    /// </summary>
    /// <param name="connection">
    /// The connection.
    /// </param>
    /// <param name="transaction">
    /// The transaction.
    /// </param>
    /// <param name="commandTimeout">
    /// The command timeout.
    /// </param>
    /// <param name="tableName">
    /// The table name.
    /// </param>
    /// <param name="columnList">
    /// The column list.
    /// </param>
    /// <param name="parameterList">
    /// The parameter list.
    /// </param>
    /// <param name="keyProperties">
    /// The key properties.
    /// </param>
    /// <param name="entityToInsert">
    /// The entity to insert.
    /// </param>
    /// <param name="autoIncrement">
    /// The auto increment.
    /// </param>
    /// <returns>
    /// The <see cref="long"/>.
    /// </returns>
    public long Insert(IDbConnection connection, IDbTransaction transaction, int? commandTimeout, String tableName, string columnList, string parameterList, IEnumerable<PropertyInfo> keyProperties, object entityToInsert, bool autoIncrement)
    {
        var sb = new StringBuilder();
        sb.AppendFormat("INSERT INTO {0} ({1}) VALUES ({2})", tableName, columnList, parameterList);

        // If no primary key then safe to assume a join table with not too much data to return
        var propertyInfos = keyProperties as PropertyInfo[] ?? keyProperties.ToArray();

        if (!propertyInfos.Any() || !autoIncrement)
            sb.Append(" RETURNING *");
        else
        {
            sb.Append(" RETURNING ");
            var first = true;

            foreach (var property in propertyInfos)
            {
                if (!first)
                    sb.Append(", ");
                first = false;
                sb.Append(property.Name);
            }
        }

        var results = connection.Query(sb.ToString(), entityToInsert, transaction, commandTimeout: commandTimeout);
        var enumerable = results as dynamic[] ?? results.ToArray();

        // Return the key by assinging the corresponding property in the object - by product is that it supports compound primary keys
        long id = 0;

        if (!autoIncrement)
            return id;

        foreach (var p in propertyInfos)
        {
            var value = ((IDictionary<string, object>)enumerable.First())[p.Name.ToLower()];

            p.SetValue(entityToInsert, value, null);

            if (id == 0)
                id = (long)(value);
        }

        return id;
    }

    /// <summary>
    /// The insert async.
    /// </summary>
    /// <param name="connection">
    /// The connection.
    /// </param>
    /// <param name="transaction">
    /// The transaction.
    /// </param>
    /// <param name="commandTimeout">
    /// The command timeout.
    /// </param>
    /// <param name="tableName">
    /// The table name.
    /// </param>
    /// <param name="columnList">
    /// The column list.
    /// </param>
    /// <param name="parameterList">
    /// The parameter list.
    /// </param>
    /// <param name="keyProperties">
    /// The key properties.
    /// </param>
    /// <param name="entityToInsert">
    /// The entity to insert.
    /// </param>
    /// <param name="autoIncrement">
    /// The auto increment.
    /// </param>
    /// <returns>
    /// The <see cref="Task"/>.
    /// </returns>
    public async Task<long> InsertAsync(IDbConnection connection, IDbTransaction transaction, int? commandTimeout, String tableName, string columnList, string parameterList, IEnumerable<PropertyInfo> keyProperties, object entityToInsert, bool autoIncrement)
    {
        var sb = new StringBuilder();
        sb.AppendFormat("INSERT INTO {0} ({1}) VALUES ({2})", tableName, columnList, parameterList);

        // If no primary key then safe to assume a join table with not too much data to return
        var propertyInfos = keyProperties as PropertyInfo[] ?? keyProperties.ToArray();
        if (!propertyInfos.Any() || !autoIncrement)
            sb.Append(" RETURNING *");
        else
        {
            sb.Append(" RETURNING ");
            var first = true;
            foreach (var property in propertyInfos)
            {
                if (!first)
                    sb.Append(", ");
                first = false;
                sb.Append(property.Name);
            }
        }

        var results = await connection.QueryAsync<dynamic>(sb.ToString(), entityToInsert, transaction, commandTimeout).ConfigureAwait(false);
        var enumerable = results as dynamic[] ?? results.ToArray();

        // Return the key by assinging the corresponding property in the object - by product is that it supports compound primary keys
        long id = 0;
        if (!autoIncrement) return id;

        foreach (var p in propertyInfos)
        {
            var value = ((IDictionary<string, object>)enumerable.First())[p.Name.ToLower()];
            p.SetValue(entityToInsert, value, null);
            if (id == 0)
                id = (long)(value);
        }

        return id;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="connection"></param>
    /// <param name="transaction"></param>
    /// <param name="commandTimeout"></param>
    /// <param name="tableName"></param>
    /// <param name="columnList"></param>
    /// <param name="parameterList"></param>
    /// <param name="keyProperties"></param>
    /// <param name="entitiesToInsert"></param>
    /// <param name="autoIncrement"></param>
    /// <returns></returns>
    public bool BulkInsert(IDbConnection connection, IDbTransaction transaction, int? commandTimeout, string tableName, string columnList, string parameterList, IEnumerable<PropertyInfo> keyProperties, IEnumerable<object> entitiesToInsert, bool autoIncrement)
    {
        throw new NotImplementedException();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="connection"></param>
    /// <param name="transaction"></param>
    /// <param name="commandTimeout"></param>
    /// <param name="tableName"></param>
    /// <param name="columnList"></param>
    /// <param name="parameterList"></param>
    /// <param name="keyProperties"></param>
    /// <param name="entitiesToInsert"></param>
    /// <param name="autoIncrement"></param>
    /// <returns></returns>
    public Task<bool> BulkInsertAsync(IDbConnection connection, IDbTransaction transaction, int? commandTimeout, string tableName, string columnList, string parameterList, IEnumerable<PropertyInfo> keyProperties, IEnumerable<object> entitiesToInsert, bool autoIncrement)
    {
        throw new NotImplementedException();
    }
}

/// <summary>
/// The sq lite adapter.
/// </summary>
public class SqLiteAdapter : ISqlAdapter
{
    /// <summary>
    /// The insert.
    /// </summary>
    /// <param name="connection">
    /// The connection.
    /// </param>
    /// <param name="transaction">
    /// The transaction.
    /// </param>
    /// <param name="commandTimeout">
    /// The command timeout.
    /// </param>
    /// <param name="tableName">
    /// The table name.
    /// </param>
    /// <param name="columnList">
    /// The column list.
    /// </param>
    /// <param name="parameterList">
    /// The parameter list.
    /// </param>
    /// <param name="keyProperties">
    /// The key properties.
    /// </param>
    /// <param name="entityToInsert">
    /// The entity to insert.
    /// </param>
    /// <param name="autoIncrement">
    /// The auto increment.
    /// </param>
    /// <returns>
    /// The <see cref="int"/>.
    /// </returns>
    public long Insert(IDbConnection connection, IDbTransaction transaction, int? commandTimeout, String tableName, string columnList, string parameterList, IEnumerable<PropertyInfo> keyProperties, object entityToInsert, bool autoIncrement)
    {
        var cmd = string.Format("INSERT INTO {0} ({1}) VALUES ({2})", tableName, columnList, parameterList);
        connection.Execute(cmd, entityToInsert, transaction, commandTimeout);

        if (!autoIncrement)
            return 0;

        var r = connection.Query("SELECT LAST_INSERT_ROWID() ID", transaction: transaction, commandTimeout: commandTimeout);
        object id = r.First().id;
        var propertyInfos = keyProperties as PropertyInfo[] ?? keyProperties.ToArray();

        if (!propertyInfos.Any())
            return (long)id;

        propertyInfos.First().SetValue(entityToInsert, id, null);

        return (long)id;
    }

    /// <summary>
    /// The insert async.
    /// </summary>
    /// <param name="connection">
    /// The connection.
    /// </param>
    /// <param name="transaction">
    /// The transaction.
    /// </param>
    /// <param name="commandTimeout">
    /// The command timeout.
    /// </param>
    /// <param name="tableName">
    /// The table name.
    /// </param>
    /// <param name="columnList">
    /// The column list.
    /// </param>
    /// <param name="parameterList">
    /// The parameter list.
    /// </param>
    /// <param name="keyProperties">
    /// The key properties.
    /// </param>
    /// <param name="entityToInsert">
    /// The entity to insert.
    /// </param>
    /// <param name="autoIncrement">
    /// The auto increment.
    /// </param>
    /// <returns>
    /// The <see cref="Task"/>.
    /// </returns>
    public async Task<long> InsertAsync(IDbConnection connection, IDbTransaction transaction, int? commandTimeout, String tableName, string columnList, string parameterList, IEnumerable<PropertyInfo> keyProperties, object entityToInsert, bool autoIncrement)
    {
        var cmd = string.Format("INSERT INTO {0} ({1}) VALUES ({2})", tableName, columnList, parameterList);
        await connection.ExecuteAsync(cmd, entityToInsert, transaction, commandTimeout).ConfigureAwait(false);

        if (!autoIncrement)
            return 0;

        var r = connection.Query("SELECT LAST_INSERT_ROWID() ID", transaction: transaction, commandTimeout: commandTimeout);
        object id = r.First().id;
        var propertyInfos = keyProperties as PropertyInfo[] ?? keyProperties.ToArray();

        if (!propertyInfos.Any())
            return (long)id;

        propertyInfos.First().SetValue(entityToInsert, id, null);

        return (long)id;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="connection"></param>
    /// <param name="transaction"></param>
    /// <param name="commandTimeout"></param>
    /// <param name="tableName"></param>
    /// <param name="columnList"></param>
    /// <param name="parameterList"></param>
    /// <param name="keyProperties"></param>
    /// <param name="entitiesToInsert"></param>
    /// <param name="autoIncrement"></param>
    /// <returns></returns>
    public bool BulkInsert(IDbConnection connection, IDbTransaction transaction, int? commandTimeout, string tableName, string columnList, string parameterList, IEnumerable<PropertyInfo> keyProperties, IEnumerable<object> entitiesToInsert, bool autoIncrement)
    {
        throw new NotImplementedException();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="connection"></param>
    /// <param name="transaction"></param>
    /// <param name="commandTimeout"></param>
    /// <param name="tableName"></param>
    /// <param name="columnList"></param>
    /// <param name="parameterList"></param>
    /// <param name="keyProperties"></param>
    /// <param name="entitiesToInsert"></param>
    /// <param name="autoIncrement"></param>
    /// <returns></returns>
    public Task<bool> BulkInsertAsync(IDbConnection connection, IDbTransaction transaction, int? commandTimeout, string tableName, string columnList, string parameterList, IEnumerable<PropertyInfo> keyProperties, IEnumerable<object> entitiesToInsert, bool autoIncrement)
    {
        throw new NotImplementedException();
    }
}