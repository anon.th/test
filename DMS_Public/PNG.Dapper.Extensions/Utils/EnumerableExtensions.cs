﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="EnumerableExtensions.cs" company="Aware Corporation Limited">
//   Copyright (c) 2014-2015.  All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;

namespace PNG.Dapper.Extensions.Utils
{
    /// <summary>
    /// 
    /// </summary>
    public static class EnumerableExtensions
    {
        /// <summary>
        /// 
        /// </summary>
        private static readonly ConcurrentDictionary<RuntimeTypeHandle, IEnumerable<PropertyInfo>> IgnoredProperties =
            new ConcurrentDictionary<RuntimeTypeHandle, IEnumerable<PropertyInfo>>();

        /// <summary>
        /// 
        /// </summary>
        private static readonly ConcurrentDictionary<RuntimeTypeHandle, IEnumerable<PropertyInfo>> TypeProperties =
            new ConcurrentDictionary<RuntimeTypeHandle, IEnumerable<PropertyInfo>>();

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="iterators"></param>
        /// <returns></returns>
        public static DataTable AsDataTable<T>(this IEnumerable<T> iterators)
            where T : class
        {
            if (iterators == null)
                throw new ArgumentNullException("iterators");

            var type = typeof(T);

            return AsDataTable(iterators, type);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="iterators"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public static DataTable AsDataTable(this IEnumerable iterators, Type type)
        {
            if (iterators == null)
                throw new ArgumentNullException("iterators");

            if (type == null)
                throw new ArgumentNullException("type");

            var table = new DataTable();

            try
            {
                // get writeable properties of entity
                var propertyKeys = TypePropertiesCache(type);
                var ignoredKeys = IgnoredPropertiesCache(type);
                var writeableKeys = propertyKeys.Except(ignoredKeys);

                // created table columns
                var properties = writeableKeys as PropertyInfo[] ?? writeableKeys.ToArray();

                foreach (var writeableKey in properties)
                    table.Columns.Add(writeableKey.Name, Nullable.GetUnderlyingType(writeableKey.PropertyType) ?? writeableKey.PropertyType);

                // created table rows
                foreach (var iterator in iterators)
                    table.Rows.Add(properties.Select(property => GetPropertyValue(property.GetValue(iterator, null))).ToArray());
            }
            catch (Exception err)
            {
                throw new Exception(err.Message, err);
            }

            return table;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="iterators"></param>
        /// <param name="connection"></param>
        /// <param name="tableName"></param>
        /// <param name="result"></param>
        /// <param name="commandTimeout"></param>
        /// <returns></returns>
        public static bool BulkCopy<T>(this IEnumerable<T> iterators, IDbConnection connection, string tableName, bool result, int? commandTimeout = null)
            where T : class
        {
            // create sql connection
            var sqlConnection = connection as SqlConnection;

            try
            {
                // check "NULL" value
                if (sqlConnection == null)
                    return false;

                // open sql connection
                if (sqlConnection.State != ConnectionState.Open)
                    sqlConnection.Open();

                var sqlBulkCopy = new SqlBulkCopy(sqlConnection);

                // options setting
                sqlBulkCopy.DestinationTableName = tableName;
                sqlBulkCopy.BatchSize = 5000;
                if (commandTimeout.HasValue && commandTimeout > 0)
                    sqlBulkCopy.BulkCopyTimeout = commandTimeout.Value;

                try
                {
                    // create table to write to server
                    var table = iterators.AsDataTable();

                    if ((table != null)
                        && (table.Rows.Count > 0))
                    {
                        // write to server
                        sqlBulkCopy.WriteToServer(table);
                        result = true;
                    }
                }
                catch (Exception err)
                {
                    throw new Exception(err.Message, err);
                }
                finally
                {
                    // close SqlBulkCopy connection
                    sqlBulkCopy.Close();
                }
            }
            catch (Exception err)
            {
                throw new Exception(err.Message, err);
            }
            finally
            {
                if ((sqlConnection != null)
                    && (sqlConnection.State != ConnectionState.Closed))
                {
                    sqlConnection.Close();
                }
            }

            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        private static IEnumerable<PropertyInfo> IgnoredPropertiesCache(Type type)
        {
            IEnumerable<PropertyInfo> propertyInfo;

            if (IgnoredProperties.TryGetValue(type.TypeHandle, out propertyInfo))
                return propertyInfo;

            var ignoredProperties = TypePropertiesCache(type).Where(p => p.GetCustomAttributes(true).Any(a => a is IgnoredAttribute)).ToList();

            IgnoredProperties[type.TypeHandle] = ignoredProperties;
            return ignoredProperties;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        private static IEnumerable<PropertyInfo> TypePropertiesCache(Type type)
        {
            IEnumerable<PropertyInfo> propertyInfos;

            if (TypeProperties.TryGetValue(type.TypeHandle, out propertyInfos))
                return propertyInfos;

            var properties = type.GetProperties().Where(IsWritable).ToArray();

            TypeProperties[type.TypeHandle] = properties;
            return properties;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="propertyInfo"></param>
        /// <returns></returns>
        private static bool IsWritable(PropertyInfo propertyInfo)
        {
            var attributes = propertyInfo.GetCustomAttributes(typeof(WriteAttribute), false);

            if (attributes.Length != 1) return true;

            var write = (WriteAttribute)attributes[0];
            return write.Write;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        private static object GetPropertyValue(object value)
        {
            try
            {
                return value ?? DBNull.Value;
            }
            catch
            {
                return DBNull.Value;
            }
        }
    }
}