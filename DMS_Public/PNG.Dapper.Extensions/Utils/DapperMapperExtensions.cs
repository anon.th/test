﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SqlMapperExtensions.cs" company="Aware Technology Solutions Corporation Ltd">
//   Copyright (c) 2016-2017.  All rights reserved.
// </copyright>
// <summary>
//   Defines the SqlMapperExtensions type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Dynamic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace PNG.Dapper.Extensions.Utils
{
    /// <summary>
    /// The sql mapper extensions.
    /// </summary>
    public static partial class SqlMapperExtensions
    {
        /// <summary>
        /// The find.
        /// </summary>
        /// <param name="connection">
        /// The connection.
        /// </param>
        /// <param name="expression">
        /// The expression.
        /// </param>
        /// <param name="transaction">
        /// The transaction.
        /// </param>
        /// <param name="commandTimeout">
        /// The command timeout.
        /// </param>
        /// <typeparam name="T">
        /// The generic type of T
        /// </typeparam>
        /// <returns>
        /// The <see>
        ///         <cref>IEnumerable</cref>
        ///     </see>
        ///     .
        /// </returns>
        public static IEnumerable<T> Find<T>(this IDbConnection connection, Expression<Func<T, bool>> expression, IDbTransaction transaction = null, int? commandTimeout = null)
        {
            var result = GetDynamicQuery(expression);
            return connection.Query<T>(result.Sql, (object)result.Param, transaction);
        }

        /// <summary>
        /// The find async.
        /// </summary>
        /// <param name="connection">
        /// The connection.
        /// </param>
        /// <param name="expression">
        /// The expression.
        /// </param>
        /// <param name="transaction">
        /// The transaction.
        /// </param>
        /// <param name="commandTimeout">
        /// The command timeout.
        /// </param>
        /// <typeparam name="T">
        /// The generic type of T
        /// </typeparam>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public static Task<IEnumerable<T>> FindAsync<T>(this IDbConnection connection, Expression<Func<T, bool>> expression, IDbTransaction transaction = null, int? commandTimeout = null)
        {
            var result = GetDynamicQuery(expression);
            return connection.QueryAsync<T>(result.Sql, (object)result.Param, transaction);
        }

        /// <summary>
        /// The remove.
        /// </summary>
        /// <param name="connection">
        /// The connection.
        /// </param>
        /// <param name="expression">
        /// The expression.
        /// </param>
        /// <param name="transaction">
        /// The transaction.
        /// </param>
        /// <param name="commandTimeout">
        /// The command timeout.
        /// </param>
        /// <typeparam name="T">
        /// The generic type of T
        /// </typeparam>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        public static int Remove<T>(this IDbConnection connection, Expression<Func<T, bool>> expression, IDbTransaction transaction = null, int? commandTimeout = null)
        {
            var result = GetDynamicRemove(expression);
            return connection.Execute(result.Sql, (object)result.Param, transaction);
        }

        /// <summary>
        /// The remove async.
        /// </summary>
        /// <param name="connection">
        /// The connection.
        /// </param>
        /// <param name="expression">
        /// The expression.
        /// </param>
        /// <param name="transaction">
        /// The transaction.
        /// </param>
        /// <param name="commandTimeout">
        /// The command timeout.
        /// </param>
        /// <typeparam name="T">
        /// The generic type of T
        /// </typeparam>
        /// <returns>
        /// The<see cref="Task"/>.
        /// </returns>
        public static Task<int> RemoveAsync<T>(this IDbConnection connection, Expression<Func<T, bool>> expression, IDbTransaction transaction = null, int? commandTimeout = null)
        {
            var result = GetDynamicRemove(expression);
            return connection.ExecuteAsync(result.Sql, (object)result.Param, transaction);
        }

        /// <summary>
        /// The get table name.
        /// </summary>
        /// <param name="connection">
        /// The connection.
        /// </param>
        /// <typeparam name="T">
        /// The generic type of T
        /// </typeparam>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public static string GetTableName<T>(this IDbConnection connection)
        {
            var type = typeof(T);
            return GetTableName(type);
        }

        /// <summary>
        /// The get dynamic query.
        /// </summary>
        /// <param name="expression">
        /// The expression.
        /// </param>
        /// <typeparam name="T">
        /// The generic type of T
        /// </typeparam>
        /// <returns>
        /// The<see cref="LinqToSqlResult"/>.
        /// </returns>
        private static LinqToSqlResult GetDynamicQuery<T>(Expression<Func<T, bool>> expression)
        {
            const string headSql = "SELECT * FROM";
            return GetDynamicSql(expression, headSql);
        }

        /// <summary>
        /// The get dynamic remove.
        /// </summary>
        /// <param name="expression">
        /// The expression.
        /// </param>
        /// <typeparam name="T">
        /// The generic type of T
        /// </typeparam>
        /// <returns>
        /// The <see cref="LinqToSqlResult"/>.
        /// </returns>
        private static LinqToSqlResult GetDynamicRemove<T>(Expression<Func<T, bool>> expression)
        {
            const string headSql = "DELETE FROM";
            return GetDynamicSql(expression, headSql);
        }

        /// <summary>
        /// The get dynamic sql.
        /// </summary>
        /// <param name="expression">
        /// The expression.
        /// </param>
        /// <param name="headSql">
        /// The head sql.
        /// </param>
        /// <typeparam name="T">
        /// The generic type of T
        /// </typeparam>
        /// <returns>
        /// The <see cref="LinqToSqlResult"/>.
        /// </returns>
        private static LinqToSqlResult GetDynamicSql<T>(Expression<Func<T, bool>> expression, string headSql)
        {
            IDictionary<string, Object> expando = new ExpandoObject();
            var tableName = GetTableName(typeof(T));
            var queryProperties = new List<QueryParameter>();
            var body = (BinaryExpression)expression.Body;
            var builder = new StringBuilder();

            // walk the tree and build up a list of query parameter objects
            // from the left and right branches of the expression tree
            WalkTree(body, ExpressionType.Default, ref queryProperties);

            // convert the query parms into a SQL string and dynamic property object
            builder.Append(headSql + " ");
            builder.Append(tableName);
            builder.Append(" WHERE ");

            for (var i = 0; i < queryProperties.Count(); i++)
            {
                var item = queryProperties[i];

                if (!string.IsNullOrEmpty(item.LinkingOperator) && i > 0)
                {
                    builder.Append(string.Format("{0} {1} {2} :{1} ", item.LinkingOperator, item.PropertyName, item.QueryOperator));
                }
                else
                {
                    builder.Append(string.Format("{0} {1} :{0} ", item.PropertyName, item.QueryOperator));
                }

                expando[item.PropertyName] = item.PropertyValue;
            }

            return new LinqToSqlResult(builder.ToString().TrimEnd(), expando);
        }

        /// <summary>
        /// Walks the tree.
        /// </summary>
        /// <param name="body">The body.</param>
        /// <param name="linkingType">Type of the linking.</param>
        /// <param name="queryProperties">The query properties.</param>
        private static void WalkTree(BinaryExpression body, ExpressionType linkingType, ref List<QueryParameter> queryProperties)
        {
            while (true)
            {
                if (body.NodeType != ExpressionType.AndAlso && body.NodeType != ExpressionType.OrElse)
                {
                    var propertyName = GetPropertyName(body);
                    dynamic propertyValue = body.Right;
                    var opr = GetOperator(body.NodeType);
                    var link = GetOperator(linkingType);

                    queryProperties.Add(new QueryParameter(link, propertyName, GetValue(propertyValue), opr));
                }
                else
                {
                    WalkTree((BinaryExpression)body.Left, body.NodeType, ref queryProperties);
                    var tmp = body;

                    body = (BinaryExpression)body.Right;
                    linkingType = tmp.NodeType;

                    continue;
                }

                break;
            }
        }

        /// <summary>
        /// The get value.
        /// </summary>
        /// <param name="propertyValue">
        /// The property value.
        /// </param>
        /// <returns>
        /// The <see cref="object"/>.
        /// </returns>
        private static object GetValue(dynamic propertyValue)
        {
            if (propertyValue.NodeType == ExpressionType.Constant) return propertyValue.Value;

            var member = (MemberExpression)propertyValue;
            var objectMember = Expression.Convert(member, typeof(object));
            var getterLambda = Expression.Lambda<Func<object>>(objectMember);
            var getter = getterLambda.Compile();

            return getter();
        }

        /// <summary>
        /// Gets the name of the property.
        /// </summary>
        /// <param name="body">The body.</param>
        /// <returns>The property name for the property expression.</returns>
        private static string GetPropertyName(BinaryExpression body)
        {
            var propertyName = body.Left.ToString().Split(new[] { '.' })[1];

            if (body.Left.NodeType == ExpressionType.Convert)
            {
                // hack to remove the trailing ) when convering.
                propertyName = propertyName.Replace(")", string.Empty);
            }

            return propertyName;
        }

        /// <summary>
        /// Gets the operator.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <returns>
        /// The expression types SQL server equivalent operator.
        /// </returns>
        /// <exception cref="System.NotImplementedException"></exception>
        private static string GetOperator(ExpressionType type)
        {
            switch (type)
            {
                case ExpressionType.Equal:
                    return "=";
                case ExpressionType.NotEqual:
                    return "!=";
                case ExpressionType.LessThan:
                    return "<";
                case ExpressionType.GreaterThan:
                    return ">";
                case ExpressionType.AndAlso:
                case ExpressionType.And:
                    return "AND";
                case ExpressionType.Or:
                case ExpressionType.OrElse:
                    return "OR";
                case ExpressionType.Default:
                    return string.Empty;
                default:
                    throw new NotImplementedException();
            }
        }
    }
}