﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PagingExtensions.cs" company="Aware Technology Solutions Corporation Ltd">
//   Copyright (c) 2016-2017.  All rights reserved.
// </copyright>
// <summary>
//   Defines the PagingExtensions type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.Collections.Generic;
using System.Linq;

namespace PNG.Dapper.Extensions.Utils
{
    /// <summary>
    /// 
    /// </summary>
    public static class PagingExtensions
    {
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="entity"></param>
        /// <param name="pageSize"></param>
        /// <param name="page"></param>
        /// <returns></returns>
        public static IEnumerable<T> Page<T>(this IEnumerable<T> entity, int pageSize, int page)
            where T : class
        {
            return page <= 0 ? entity.Skip(0 * pageSize).Take(pageSize) : entity.Skip((page - 1) * pageSize).Take(pageSize);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="entity"></param>
        /// <param name="pageSize"></param>
        /// <param name="page"></param>
        /// <returns></returns>
        public static IQueryable<T> Page<T>(this IQueryable<T> entity, int pageSize, int page)
            where T : class
        {
            return page <= 0 ? entity.Skip(0 * pageSize).Take(pageSize) : entity.Skip((page - 1) * pageSize).Take(pageSize);
        }
    }
}
