﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IRepository.cs" company="Aware Corporation Limited">
//   Copyright (c) 2014-2015.  All rights reserved.
// </copyright>
// <summary>
//   Defines the IRepository type.
//   Reviewed on 25 Jul 2016.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using Dapper;
using PNG.Dapper.Extensions.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Threading.Tasks;

namespace PNG.Dapper.Extensions
{
    /// <summary>
    /// The Repository interface.
    /// </summary>
    /// <typeparam name="T">
    /// The generic type of T
    /// </typeparam>
    public interface IRepository<T> : IRepository<T, object>
        where T : class
    {
    }

    /// <summary>
    /// The Repository interface.
    /// </summary>
    /// <typeparam name="T">
    /// he generic type of T
    /// </typeparam>
    /// <typeparam name="TKey">
    /// The generic type of Tkey
    /// </typeparam>
    public interface IRepository<T, in TKey>
        where T : class
    {
        /// <summary>
        /// The insert.
        /// </summary>
        /// <param name="item">
        /// The item.
        /// </param>
        /// <returns>
        /// The <see cref="long"/>.
        /// </returns>
        long Insert(T item);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="items"></param>
        /// <param name="isIdTypeInt"></param>
        /// <returns></returns>
        IEnumerable<T> InsertRange(IEnumerable<T> items, bool isIdTypeInt = false);

        /// <summary>
        /// The insert async.
        /// </summary>
        /// <param name="item">
        /// The item.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<long> InsertAsync(T item);

        /// <summary>
        /// The remove.
        /// </summary>
        /// <param name="item">
        /// The item.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        bool Remove(T item);

        /// <summary>
        /// The remove async.
        /// </summary>
        /// <param name="item">
        /// The item.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<bool> RemoveAsync(T item);

        /// <summary>
        /// The remove all.
        /// </summary>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        bool RemoveAll();

        /// <summary>
        /// The remove all async.
        /// </summary>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<bool> RemoveAllAsync();

        /// <summary>
        /// The remove.
        /// </summary>
        /// <param name="predicate">
        /// The predicate.
        /// </param>
        void Remove(Expression<Func<T, bool>> predicate);

        /// <summary>
        /// The remove async.
        /// </summary>
        /// <param name="predicate">
        /// The predicate.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task RemoveAsync(Expression<Func<T, bool>> predicate);

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="item">
        /// The item.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        bool Update(T item);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="items"></param>
        /// <param name="fieldToUpdate"></param>
        /// <returns></returns>
        bool UpdateRange(IEnumerable<T> items, List<string> fieldToUpdate);

        /// <summary>
        /// The update async.
        /// </summary>
        /// <param name="item">
        /// The item.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<bool> UpdateAsync(T item);

        /// <summary>
        /// The get all.
        /// </summary>
        /// <returns>
        /// The <see>
        ///         <cref>IEnumerable</cref>
        ///     </see>
        ///     .
        /// </returns>
        IEnumerable<T> GetAll();

        /// <summary>
        /// The get all async.
        /// </summary>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<IEnumerable<T>> GetAllAsync();

        /// <summary>
        /// The get.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="T"/>.
        /// </returns>
        T Get(TKey id);

        /// <summary>
        /// The get async.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<T> GetAsync(TKey id);

        /// <summary>
        /// The find.
        /// </summary>
        /// <param name="predicate">
        /// The predicate.
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>IEnumerable</cref>
        ///     </see>
        ///     .
        /// </returns>
        IEnumerable<T> Find(Expression<Func<T, bool>> predicate);

        /// <summary>
        /// The find async.
        /// </summary>
        /// <param name="predicate">
        /// The predicate.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<IEnumerable<T>> FindAsync(Expression<Func<T, bool>> predicate);

        /// <summary>
        /// The query.
        /// </summary>
        /// <param name="sql">
        /// The sql.
        /// </param>
        /// <param name="param">
        /// The param.
        /// </param>
        /// <typeparam name="TS">
        /// </typeparam>
        /// <returns>
        /// The <see>
        ///         <cref>IEnumerable</cref>
        ///     </see>
        ///     .
        /// </returns>
        IEnumerable<TS> Query<TS>(string sql, object param);

        /// <summary>
        /// The query async.
        /// </summary>
        /// <param name="sql">
        /// The sql.
        /// </param>
        /// <param name="param">
        /// The param.
        /// </param>
        /// <typeparam name="TS">
        /// </typeparam>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<IEnumerable<TS>> QueryAsync<TS>(string sql, object param);

        /// <summary>
        /// The get table name.
        /// </summary>
        /// <typeparam name="TModel">
        /// </typeparam>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        string GetTableName<TModel>();

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TSp"></typeparam>
        /// <param name="sql"></param>
        /// <param name="param"></param>
        /// <param name="commandTimeout"></param>
        /// <returns></returns>
        IEnumerable<TSp> Exec<TSp>(string sql, DynamicParameters param = null, int? commandTimeout = null);

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TSp"></typeparam>
        /// <param name="sql"></param>
        /// <param name="param"></param>
        /// <param name="commandTimeout"></param>
        /// <returns></returns>
        Task<IEnumerable<TSp>> ExecAsync<TSp>(string sql, DynamicParameters param = null, int? commandTimeout = null);
    }

    /// <summary>
    /// The dapper repository.
    /// </summary>
    /// <typeparam name="T">
    /// The generic type of T
    /// </typeparam>
    public class DapperRepository<T> : DapperRepository<T, object>, IRepository<T> where T : class
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DapperRepository{T}"/> class.
        /// </summary>
        /// <param name="connection">
        /// The connection.
        /// </param>
        public DapperRepository(IDbConnection connection)
            // ReSharper disable once RedundantArgumentDefaultValue
            : this(connection, null)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DapperRepository{T}"/> class.
        /// </summary>
        /// <param name="connection">
        /// The connection.
        /// </param>
        /// <param name="transaction">
        /// The transaction.
        /// </param>
        public DapperRepository(IDbConnection connection, IDbTransaction transaction = null)
            : base(connection, transaction)
        {
        }
    }

    /// <summary>
    /// The dapper repository.
    /// </summary>
    /// <typeparam name="T">
    /// The generic type of T
    /// </typeparam>
    /// <typeparam name="TKey">
    /// The generic type of TKey
    /// </typeparam>
    public class DapperRepository<T, TKey> : IRepository<T, TKey>, IDisposable where T : class
    {
        /// <summary>
        /// 
        /// </summary>
        private const string DapperExecutionTimeout = "DapperExecutionTimeout";

        /// <summary>
        /// The _connection.
        /// </summary>
        private readonly IDbConnection _connection;

        /// <summary>
        /// The _transaction.
        /// </summary>
        private readonly IDbTransaction _transaction;

        /// <summary>
        /// Exception Message
        /// </summary>
        public string ExceptionMessage
        {
            get;
            set;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DapperRepository{T,TKey}"/> class.
        /// </summary>
        /// <param name="connection">
        /// The connection.
        /// </param>
        public DapperRepository(IDbConnection connection)
            // ReSharper disable once RedundantArgumentDefaultValue
            : this(connection, null)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DapperRepository{T,TKey}"/> class.
        /// </summary>
        /// <param name="connection">
        /// The connection.
        /// </param>
        /// <param name="transaction">
        /// The transaction.
        /// </param>
        public DapperRepository(IDbConnection connection, IDbTransaction transaction = null)
        {
            _connection = connection;
            _transaction = transaction;

            // Set the command timeout for execution to 60 minutes, if have no app setting for DapperExecutionTimeout.
            var dapperTimeout = 3600;

            // Load value of DapperCommandTimeout from appSettings.
            if (ConfigurationManager.AppSettings.AllKeys.Contains(DapperExecutionTimeout))
            {
                dapperTimeout = GetAppSettingValue<int>(DapperExecutionTimeout);
            }

            // Specifies the default Command Timeout for all Queries. 
            SqlMapper.Settings.CommandTimeout = dapperTimeout;
        }

        /// <summary>
        /// The insert.
        /// </summary>
        /// <param name="item">
        /// The item.
        /// </param>
        /// <returns>
        /// The <see cref="long"/>.
        /// </returns>
        public long Insert(T item)
        {
            try
            {
                EnsureConnectionOpen();
                return _connection.Insert(item, _transaction);
            }
            finally
            {
                EnsureConnectionClosed();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="items"></param>
        /// <param name="isIdTypeInt"></param>
        /// <returns></returns>
        public IEnumerable<T> InsertRange(IEnumerable<T> items, bool isIdTypeInt = false)
        {
            try
            {
                EnsureConnectionOpen();
                var type = typeof(T);
                var props = type.GetProperties(BindingFlags.Public | BindingFlags.Instance);
                var prop = props.FirstOrDefault(x => x.Name.ToLower() == "id");
                foreach (var item in items)
                {
                    var id = _connection.Insert(item, _transaction);
                    if (prop != null)
                    {
                        if (isIdTypeInt)
                        {
                            prop.SetValue(item, (int)id, null);
                        }
                        else
                        {
                            prop.SetValue(item, id, null);
                        }
                    }
                }
                return items;
            }
            finally
            {
                EnsureConnectionClosed();
            }
        }

        /// <summary>
        /// The insert async.
        /// </summary>
        /// <param name="item">
        /// The item.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<long> InsertAsync(T item)
        {
            try
            {
                EnsureConnectionOpen();
                return await _connection.InsertAsync(item, _transaction);
            }
            finally
            {
                EnsureConnectionClosed();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="items"></param>
        /// <param name="commandTimeout"></param>
        /// <returns></returns>
        public bool BulkInsert(IEnumerable<T> items, int? commandTimeout = null)
        {
            try
            {
                EnsureConnectionOpen();
                return _connection.BulkInsert(items, _transaction, commandTimeout);
            }
            finally
            {
                EnsureConnectionClosed();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="items"></param>
        /// <returns></returns>
        public async Task<bool> BulkInsertAsync(IEnumerable<T> items)
        {
            try
            {
                EnsureConnectionOpen();
                return await _connection.BulkInsertAsync(items, _transaction);
            }
            finally
            {
                EnsureConnectionClosed();
            }
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="item">
        /// The item.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool Update(T item)
        {
            try
            {
                EnsureConnectionOpen();
                return _connection.Update(item, _transaction);
            }
            finally
            {
                EnsureConnectionClosed();
            }
        }

        /// <summary>
        /// Update by fields
        /// </summary>
        /// <param name="item"></param>
        /// <param name="fieldToUpdate"></param>
        /// <returns></returns>
        public bool Update(T item, List<string> fieldToUpdate)
        {
            try
            {
                EnsureConnectionOpen();
                return _connection.Update(item, fieldToUpdate, _transaction);
            }
            finally
            {
                EnsureConnectionClosed();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="items"></param>
        /// <param name="fieldToUpdate"></param>
        /// <returns></returns>
        public bool UpdateRange(IEnumerable<T> items, List<string> fieldToUpdate)
        {
            try
            {
                EnsureConnectionOpen();
                foreach (var item in items)
                {
                    _connection.Update(item, fieldToUpdate, _transaction);
                }
                return true;
            }
            finally
            {
                EnsureConnectionClosed();
            }
        }

        /// <summary>
        /// The update async.
        /// </summary>
        /// <param name="item">
        /// The item.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<bool> UpdateAsync(T item)
        {
            try
            {
                EnsureConnectionOpen();
                return await _connection.UpdateAsync(item, _transaction);
            }
            finally
            {
                EnsureConnectionClosed();
            }
        }

        /// <summary>
        /// The remove.
        /// </summary>
        /// <param name="item">
        /// The item.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool Remove(T item)
        {
            try
            {
                EnsureConnectionOpen();
                return _connection.Delete(item, _transaction);
            }
            finally
            {
                EnsureConnectionClosed();
            }
        }

        /// <summary>
        /// The remove async.
        /// </summary>
        /// <param name="item">
        /// The item.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<bool> RemoveAsync(T item)
        {
            try
            {
                EnsureConnectionOpen();
                return await _connection.DeleteAsync(item, _transaction);
            }
            finally
            {
                EnsureConnectionClosed();
            }
        }

        /// <summary>
        /// The remove all.
        /// </summary>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool RemoveAll()
        {
            try
            {
                EnsureConnectionOpen();
                return _connection.DeleteAll<T>(_transaction);
            }
            finally
            {
                EnsureConnectionClosed();
            }
        }

        /// <summary>
        /// The remove all async.
        /// </summary>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<bool> RemoveAllAsync()
        {
            try
            {
                EnsureConnectionOpen();
                return await _connection.DeleteAllAsync<T>(_transaction);
            }
            finally
            {
                EnsureConnectionClosed();
            }
        }

        /// <summary>
        /// The get.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="T"/>.
        /// </returns>
        public T Get(TKey id)
        {
            try
            {
                EnsureConnectionOpen();
                return _connection.Get<T>(id, _transaction);
            }
            finally
            {
                EnsureConnectionClosed();
            }
        }

        /// <summary>
        /// The get async.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<T> GetAsync(TKey id)
        {
            try
            {
                EnsureConnectionOpen();
                return await _connection.GetAsync<T>(id, _transaction);
            }
            finally
            {
                EnsureConnectionClosed();
            }
        }

        /// <summary>
        /// The get all.
        /// </summary>
        /// <returns>
        /// The <see>
        ///         <cref>IEnumerable</cref>
        ///     </see>
        ///     .
        /// </returns>
        public IEnumerable<T> GetAll()
        {
            try
            {
                EnsureConnectionOpen();
                return _connection.GetAll<T>(_transaction);
            }
            finally
            {
                EnsureConnectionClosed();
            }
        }

        /// <summary>
        /// The get all async.
        /// </summary>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<IEnumerable<T>> GetAllAsync()
        {
            try
            {
                EnsureConnectionOpen();
                return await _connection.GetAsync<IEnumerable<T>>(_transaction);
            }
            finally
            {
                EnsureConnectionClosed();
            }
        }

        /// <summary>
        /// Get DataSet
        /// </summary>
        /// <param name="queryString"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public DataSet GetDataSet(string queryString, SqlParameter[] parameters)
        {
            try
            {
                EnsureConnectionOpen();
                return _connection.GetDataSet(queryString, parameters);
            }
            finally
            {
                EnsureConnectionClosed();
            }
        }

        /// <summary>
        /// The ensure connection closed.
        /// </summary>
        public void EnsureConnectionClosed()
        {
            try
            {
                //Code backup
                //if ((_transaction == null)
                //    && (_connection.State == ConnectionState.Open))
                //{
                //    _connection.Close();
                //}

                if (_connection.State != ConnectionState.Closed)
                {
                    _connection.Close();
                }
            }
            catch (Exception ex)
            {
                ExceptionMessage = ex.Message;
                throw;
            }
        }

        #region IDisposable Support
        /// <summary>
        /// The disposed value.
        /// </summary>
        private bool _disposedValue; // To detect redundant calls
        /// <summary>
        /// The dispose.
        /// </summary>
        /// <param name="disposing">
        /// The disposing.
        /// </param>
        protected virtual void Dispose(bool disposing)
        {
            if (_disposedValue) return;
            if (disposing)
            {
                if (_connection != null && _connection.State != ConnectionState.Closed)
                {
                    _connection.Close();
                }
            }

            // free unmanaged resources (unmanaged objects) and override a finalizer below.
            // set large fields to null.
            _disposedValue = true;
        }

        /// <summary>
        /// The dispose.
        /// </summary>
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);

            // uncomment the following line if the finalizer is overridden above.
            GC.SuppressFinalize(this);
        }

        #endregion

        /// <summary>
        /// The remove.
        /// </summary>
        /// <param name="predicate">
        /// The predicate.
        /// </param>
        public void Remove(Expression<Func<T, bool>> predicate)
        {
            try
            {
                EnsureConnectionOpen();
                _connection.Remove(predicate, _transaction);
            }
            finally
            {
                EnsureConnectionClosed();
            }
        }

        /// <summary>
        /// The remove async.
        /// </summary>
        /// <param name="predicate">
        /// The predicate.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task RemoveAsync(Expression<Func<T, bool>> predicate)
        {
            try
            {
                EnsureConnectionOpen();
                await _connection.RemoveAsync(predicate, _transaction);
            }
            finally
            {
                EnsureConnectionClosed();
            }
        }

        /// <summary>
        /// The find.
        /// </summary>
        /// <param name="predicate">
        /// The predicate.
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>IEnumerable</cref>
        ///     </see>
        ///     .
        /// </returns>
        public IEnumerable<T> Find(Expression<Func<T, bool>> predicate)
        {
            try
            {
                EnsureConnectionOpen();
                return _connection.Find(predicate, _transaction);
            }
            finally
            {
                EnsureConnectionClosed();
            }
        }

        /// <summary>
        /// The find async.
        /// </summary>
        /// <param name="predicate">
        /// The predicate.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<IEnumerable<T>> FindAsync(Expression<Func<T, bool>> predicate)
        {
            try
            {
                EnsureConnectionOpen();
                return await _connection.FindAsync(predicate, _transaction);
            }
            finally
            {
                EnsureConnectionClosed();
            }
        }

        /// <summary>
        /// The query.
        /// </summary>
        /// <param name="sql">
        /// The sql.
        /// </param>
        /// <param name="param">
        /// The param.
        /// </param>
        /// <typeparam name="TS">
        /// </typeparam>
        /// <returns>
        /// The <see>
        ///         <cref>IEnumerable</cref>
        ///     </see>
        ///     .
        /// </returns>
        public IEnumerable<TS> Query<TS>(string sql, object param)
        {
            try
            {
                EnsureConnectionOpen();
                return _connection.Query<TS>(sql, param, _transaction);
            }
            finally
            {
                EnsureConnectionClosed();
            }
        }

        /// <summary>
        /// The query async.
        /// </summary>
        /// <param name="sql">
        /// The sql.
        /// </param>
        /// <param name="param">
        /// The param.
        /// </param>
        /// <typeparam name="TS">
        /// The generic type of TS
        /// </typeparam>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<IEnumerable<TS>> QueryAsync<TS>(string sql, object param)
        {
            try
            {
                EnsureConnectionOpen();
                return await _connection.QueryAsync<TS>(sql, param, _transaction);
            }
            finally
            {
                EnsureConnectionClosed();
            }
        }

        /// <summary>
        /// The get table name.
        /// </summary>
        /// <typeparam name="TModel">
        /// The generic type of TModel
        /// </typeparam>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string GetTableName<TModel>()
        {
            return _connection.GetTableName<TModel>();
        }

        /// <summary>
        /// Store Procedure Execution
        /// </summary>
        /// <typeparam name="TSp"></typeparam>
        /// <param name="sql"></param>
        /// <param name="param"></param>
        /// <param name="commandTimeout"></param>
        /// <returns></returns>
        public IEnumerable<TSp> Exec<TSp>(string sql, DynamicParameters param = null, int? commandTimeout = null)
        {
            try
            {
                EnsureConnectionOpen();
                return _connection.Query<TSp>(sql, param, commandType: CommandType.StoredProcedure, transaction: _transaction, commandTimeout: commandTimeout);
            }
            finally
            {
                EnsureConnectionClosed();
            }
        }

        /// <summary>
        /// Store Procedure Execution Async.
        /// </summary>
        /// <typeparam name="TSp"></typeparam>
        /// <param name="sql"></param>
        /// <param name="param"></param>
        /// <param name="commandTimeout"></param>
        /// <returns></returns>
        public async Task<IEnumerable<TSp>> ExecAsync<TSp>(string sql, DynamicParameters param = null, int? commandTimeout = null)
        {
            try
            {
                EnsureConnectionOpen();
                return await _connection.QueryAsync<TSp>(sql, param, commandType: CommandType.StoredProcedure, transaction: _transaction, commandTimeout: commandTimeout).ConfigureAwait(false);
            }
            finally
            {
                EnsureConnectionClosed();
            }
        }

        /// <summary>
        /// The ensure connection open.
        /// </summary>
        private void EnsureConnectionOpen()
        {
            var retries = 3;

            if (_connection.State == ConnectionState.Open)
                return;

            while ((retries >= 0)
                && (_connection.State != ConnectionState.Open))
            {
                try
                {
                    _connection.Open();
                    retries--;
                }
                catch (Exception ex)
                {
                    ExceptionMessage = ex.Message;
                    throw;
                }
            }
        }

        /// <summary>
        /// The get AppSettings by name.
        /// </summary>
        /// <param name="settingName"></param>
        /// <returns></returns>
        private string GetAppSetting(string settingName)
        {
            string result = string.Empty;

            try
            {
                if (ConfigurationManager.AppSettings.AllKeys.Contains(settingName))
                {
                    if (ConfigurationManager.AppSettings[settingName] != null)
                    {
                        result = ConfigurationManager.AppSettings[settingName];
                    }
                }
            }
            catch (Exception)
            {
                result = string.Empty;
            }

            return result;
        }

        /// <summary>
        /// The get value.
        /// </summary>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="settingName"></param>
        /// <returns></returns>
        private TValue GetAppSettingValue<TValue>(string settingName)
        {
            try
            {
                var appSettingValue = GetAppSetting(settingName);
                var converter = TypeDescriptor.GetConverter(typeof(TValue));

                if (!string.IsNullOrEmpty(appSettingValue))
                    return (TValue)converter.ConvertFromInvariantString(appSettingValue);

                return default(TValue);
            }
            catch (Exception)
            {
                return default(TValue);
            }
        }
    }
}