﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IdentityRole.cs" company="Aware Corporation Limited">
//   Copyright (c) 2014-2015.  All rights reserved.
// </copyright>
// <summary>
//   Defines the IdentityRole type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using Microsoft.AspNet.Identity;
using System;

namespace PNG.AspNetIdentity.Dapper
{
    /// <summary>
    /// The identity role.
    /// </summary>
    public class IdentityRole : IdentityRole<string>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="IdentityRole"/> class.
        /// </summary>
        public IdentityRole()
        {
            Id = Guid.NewGuid().ToString();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="IdentityRole"/> class.
        /// </summary>
        /// <param name="name">
        /// The name.
        /// </param>
        public IdentityRole(string name)
            : base(Guid.NewGuid().ToString(), name)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="IdentityRole"/> class.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <param name="name">
        /// The name.
        /// </param>
        public IdentityRole(string id, string name)
            : base(id, name)
        {
        }
    }

    /// <summary>
    /// The identity role.
    /// </summary>
    /// <typeparam name="TKey">
    /// The generic type of TKey
    /// </typeparam>
    public class IdentityRole<TKey> : IRole<TKey>
    {
        /// <summary>
        /// Role ID
        /// </summary>
        public TKey Id { get; set; }

        /// <summary>
        /// Role name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Status
        /// </summary>
        public bool IsActive { get; set; }

        /// <summary>
        /// Created By
        /// </summary>
        public virtual string CreatedBy { get; set; }

        /// <summary>
        /// Created Date
        /// </summary>
        public virtual DateTime? CreatedDate { get; set; }

        /// <summary>
        /// Created By
        /// </summary>
        public virtual string ModifiedBy { get; set; }

        /// <summary>
        /// Modified Date
        /// </summary>
        public virtual DateTime? ModifiedDate { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="IdentityRole{TKey}"/> class. 
        /// Default constructor for Role 
        /// </summary>
        public IdentityRole()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="IdentityRole{TKey}"/> class. 
        /// Constructor that takes names as argument 
        /// </summary>
        /// <param name="name">
        /// </param>
        public IdentityRole(string name)
            : this()
        {
            Name = name;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="IdentityRole{TKey}"/> class.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <param name="name">
        /// The name.
        /// </param>
        public IdentityRole(TKey id, string name)
            : this(name)
        {
            Id = id;
        }
    }
}