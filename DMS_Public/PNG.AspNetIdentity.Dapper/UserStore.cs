﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="UserStore.cs" company="Aware Corporation Limited">
//   Copyright (c) 2014-2015.  All rights reserved.
// </copyright>
// <summary>
//   Defines the UserStore type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Dapper;

namespace PNG.AspNetIdentity.Dapper
{
    /// <summary>
    /// The user store.
    /// </summary>
    /// <typeparam name="TUser">
    /// The generic type of TUser
    /// </typeparam>
    /// <typeparam name="TRole">
    /// The generic type of TRole
    /// </typeparam>
    public class UserStore<TUser, TRole> : UserStore<TUser, TRole, string, string>, IUserStore<TUser>
        where TUser : User_Master
        where TRole : IdentityRole
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="UserStore{TUser,TRole}"/> class.
        /// </summary>
        /// <param name="dbContext">
        /// The db context.
        /// </param>
        public UserStore(IdentityDbContext<TUser, TRole, string, string> dbContext)
            : base(dbContext)
        {
        }

        /// <summary>
        /// The get logins async.
        /// </summary>
        /// <param name="user">
        /// The user.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// </exception>
        public override async Task<IList<UserLoginInfo>> GetLoginsAsync(TUser user)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }

            var result = await DbContent.UserLoginRepository.FindAsync(c => c.UserId == user.Id);
            var identityUserLogins = result as IdentityUserLogin<string>[] ?? result.ToArray();
            var rs = new List<UserLoginInfo>();

            if (result == null || !identityUserLogins.Any())
                return rs;

            rs.AddRange(identityUserLogins.Select(login =>
                new UserLoginInfo(login.LoginProvider, login.ProviderKey)));

            return rs;
        }

        /// <summary>
        /// The get claims async.
        /// </summary>
        /// <param name="user">
        /// The user.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// </exception>
        public override async Task<IList<System.Security.Claims.Claim>> GetClaimsAsync(TUser user)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }

            var result = await DbContent.UserClaimRepository.FindAsync(c => c.UserId == user.Id);
            var identityUserClaims = result as IdentityUserClaim<string>[] ?? result.ToArray();
            var rs = new List<System.Security.Claims.Claim>();

            if (result == null || !identityUserClaims.Any())
                return rs;

            rs.AddRange(identityUserClaims.Select(login =>
                new System.Security.Claims.Claim(login.ClaimType, login.ClaimValue)));

            return rs;
        }

        /// <summary>
        /// The remove claim async.
        /// </summary>
        /// <param name="user">
        /// The user.
        /// </param>
        /// <param name="claim">
        /// The claim.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// </exception>
        public override async Task RemoveClaimAsync(TUser user, System.Security.Claims.Claim claim)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }

            if (claim == null)
            {
                throw new ArgumentNullException("claim");
            }

            await DbContent.UserClaimRepository.RemoveAsync(c => c.UserId == user.Id
                && c.ClaimValue == claim.Value
                && c.ClaimType == claim.ValueType);
        }

        /// <summary>
        /// The remove from role async.
        /// </summary>
        /// <param name="user">
        /// The user.
        /// </param>
        /// <param name="roleName">
        /// The role name.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// </exception>
        public override async Task RemoveFromRoleAsync(TUser user, string roleName)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }
            if (string.IsNullOrEmpty(roleName))
            {
                throw new ArgumentNullException("roleName");
            }

            var roles = await DbContent.RoleRepository.FindAsync(c => c.Name == roleName);
            var enumerable = roles as TRole[] ?? roles.ToArray();

            if (roles != null && enumerable.Any())
            {
                var role = enumerable.First();
                await DbContent.UserRoleRepository.RemoveAsync(c => c.RoleId == role.Id && c.UserId == user.Id);
            }
        }

        /// <summary>
        /// The remove login async.
        /// </summary>
        /// <param name="user">
        /// The user.
        /// </param>
        /// <param name="login">
        /// The login.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// </exception>
        public override async Task RemoveLoginAsync(TUser user, UserLoginInfo login)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }

            if (login == null)
            {
                throw new ArgumentNullException("login");
            }

            await DbContent.UserLoginRepository.RemoveAsync(c => c.UserId == user.Id
                && c.ProviderKey == login.ProviderKey
                && c.LoginProvider == login.LoginProvider);
        }
        
        #region IDisposable Support
        /// <summary>
        /// The disposed value.
        /// </summary>
        private bool disposedValue = false; // To detect redundant calls

        /// <summary>
        /// The dispose.
        /// </summary>
        /// <param name="disposing">
        /// The disposing.
        /// </param>
        protected override void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                }

                // free unmanaged resources (unmanaged objects) and override a finalizer below.
                // set large fields to null.
                disposedValue = true;
                base.Dispose();
            }
        }
        #endregion
    }

    /// <summary>
    /// The user store.
    /// </summary>
    /// <typeparam name="TUser">
    /// The generic type of TUser
    /// </typeparam>
    /// <typeparam name="TRole">
    /// The generic type of TRole
    /// </typeparam>
    /// <typeparam name="TKey">
    /// The generic type of TKey
    /// </typeparam>
    /// <typeparam name="TRoleKey">
    /// The generic type of TRoleKey
    /// </typeparam>
    public class UserStore<TUser, TRole, TKey, TRoleKey> : IUserLoginStore<TUser, TKey>,
        IUserClaimStore<TUser, TKey>,
        IUserRoleStore<TUser, TKey>,
        IUserPasswordStore<TUser, TKey>,
        IUserSecurityStampStore<TUser, TKey>,
        IQueryableUserStore<TUser, TKey>,
        IUserEmailStore<TUser, TKey>,
        IUserTwoFactorStore<TUser, TKey>,
        IUserLockoutStore<TUser, TKey>
        where TUser : User_Master<TKey>
        where TRole : IdentityRole<TRoleKey>
    {
        /// <summary>
        /// The _db content.
        /// </summary>
        private readonly IdentityDbContext<TUser, TRole, TKey, TRoleKey> _dbContent;

        /// <summary>
        /// Gets the db content.
        /// </summary>
        public IdentityDbContext<TUser, TRole, TKey, TRoleKey> DbContent
        {
            get { return _dbContent; }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="UserStore{TUser,TRole,TKey,TRoleKey}"/> class.
        /// </summary>
        /// <param name="dbContent">
        /// The db content.
        /// </param>
        /// <exception cref="ArgumentNullException">
        /// </exception>
        public UserStore(IdentityDbContext<TUser, TRole, TKey, TRoleKey> dbContent)
        {
            if (dbContent == null)
            {
                throw new ArgumentNullException("dbContent");
            }

            _dbContent = dbContent;
        }

        /// <summary>
        /// The add login async.
        /// </summary>
        /// <param name="user">
        /// The user.
        /// </param>
        /// <param name="login">
        /// The login.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// </exception>
        public virtual async Task AddLoginAsync(TUser user, UserLoginInfo login)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }

            if (login == null)
            {
                throw new ArgumentNullException("login");
            }

            var userLogin = new IdentityUserLogin<TKey>
            {
                UserId = user.Id,
                ProviderKey = login.ProviderKey,
                LoginProvider = login.LoginProvider
            };

            await DbContent.UserLoginRepository.InsertAsync(userLogin);
        }

        /// <summary>
        /// The find async.
        /// </summary>
        /// <param name="login">
        /// The login.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public virtual async Task<TUser> FindAsync(UserLoginInfo login)
        {
            var task = DbContent.UserLoginRepository.FindAsync(c => c.LoginProvider == login.LoginProvider && c.ProviderKey == login.ProviderKey);

            task.Wait();

            var logines = task.Result;
            var identityUserLogins = logines as IdentityUserLogin<TKey>[] ?? logines.ToArray();

            if (logines == null || !identityUserLogins.Any())
                return null;

            var userId = identityUserLogins.First().UserId;
            return await DbContent.UserRepository.GetAsync(userId);
        }

        /// <summary>
        /// The get logins async.
        /// </summary>
        /// <param name="user">
        /// The user.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// </exception>
        public virtual async Task<IList<UserLoginInfo>> GetLoginsAsync(TUser user)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }

            var result = await DbContent.UserLoginRepository.FindAsync(c => (object)c.UserId == (object)user.Id);
            var rs = new List<UserLoginInfo>();
            var identityUserLogins = result as IdentityUserLogin<TKey>[] ?? result.ToArray();

            if (result == null || !identityUserLogins.Any())
                return rs;

            rs.AddRange(identityUserLogins.Select(login =>
                new UserLoginInfo(login.LoginProvider, login.ProviderKey)));

            return rs;
        }

        /// <summary>
        /// The remove login async.
        /// </summary>
        /// <param name="user">
        /// The user.
        /// </param>
        /// <param name="login">
        /// The login.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// </exception>
        public virtual async Task RemoveLoginAsync(TUser user, UserLoginInfo login)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }

            if (login == null)
            {
                throw new ArgumentNullException("login");
            }

            await DbContent.UserLoginRepository.RemoveAsync(c =>
                (object)c.UserId == (object)user.Id && c.ProviderKey == login.ProviderKey && c.LoginProvider == login.LoginProvider);
        }

        /// <summary>
        /// The create async.
        /// </summary>
        /// <param name="user">
        /// The user.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// </exception>
        public virtual async Task CreateAsync(TUser user)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }

            await DbContent.UserRepository.InsertAsync(user);
        }

        /// <summary>
        /// The delete async.
        /// </summary>
        /// <param name="user">
        /// The user.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// </exception>
        public virtual async Task DeleteAsync(TUser user)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }

            await DbContent.UserRepository.RemoveAsync(user);
        }

        /// <summary>
        /// The find by id async.
        /// </summary>
        /// <param name="userId">
        /// The user id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// </exception>
        public virtual async Task<TUser> FindByIdAsync(TKey userId)
        {
            // ReSharper disable once CompareNonConstrainedGenericWithNull
            if (userId == null)
            {
                throw new ArgumentNullException("userId");
            }

            return await DbContent.UserRepository.GetAsync(userId);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userName"></param>
        /// <returns></returns>
        public virtual async Task<TUser> FindByNameAsync(string userName)
        {
            if (string.IsNullOrEmpty(userName))
            {
                throw new ArgumentNullException("userName");
            }

            var users = await DbContent.UserRepository.FindAsync(c => c.UserId == userName);
            return users.FirstOrDefault();
        }

        /// <summary>
        /// The update async.
        /// </summary>
        /// <param name="user">
        /// The user.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// </exception>
        public virtual async Task UpdateAsync(TUser user)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }

            await DbContent.UserRepository.UpdateAsync(user);
        }

        /// <summary>
        /// The add claim async.
        /// </summary>
        /// <param name="user">
        /// The user.
        /// </param>
        /// <param name="claim">
        /// The claim.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// </exception>
        public virtual async Task AddClaimAsync(TUser user, System.Security.Claims.Claim claim)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }

            if (claim == null)
            {
                throw new ArgumentNullException("claim");
            }

            var userClaim = new IdentityUserClaim<TKey>
            {
                UserId = user.Id,
                ClaimType = claim.ValueType,
                ClaimValue = claim.Value
            };

            await DbContent.UserClaimRepository.InsertAsync(userClaim);
        }

        /// <summary>
        /// The get claims async.
        /// </summary>
        /// <param name="user">
        /// The user.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// </exception>
        public virtual async Task<IList<System.Security.Claims.Claim>> GetClaimsAsync(TUser user)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }

            var result = await DbContent.UserClaimRepository.FindAsync(c => (object)c.UserId == (object)user.Id);
            var rs = new List<System.Security.Claims.Claim>();
            var identityUserClaims = result as IdentityUserClaim<TKey>[] ?? result.ToArray();

            if (result == null || !identityUserClaims.Any())
                return rs;

            rs.AddRange(identityUserClaims.Select(login =>
                new System.Security.Claims.Claim(login.ClaimType, login.ClaimValue)));

            return rs;
        }

        /// <summary>
        /// The remove claim async.
        /// </summary>
        /// <param name="user">
        /// The user.
        /// </param>
        /// <param name="claim">
        /// The claim.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// </exception>
        public virtual async Task RemoveClaimAsync(TUser user, System.Security.Claims.Claim claim)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }

            if (claim == null)
            {
                throw new ArgumentNullException("claim");
            }

            await DbContent.UserClaimRepository.RemoveAsync(c =>
                (object)c.UserId == (object)user.Id && c.ClaimValue == claim.Value && c.ClaimType == claim.ValueType);
        }

        /// <summary>
        /// The add to role async.
        /// </summary>
        /// <param name="user">
        /// The user.
        /// </param>
        /// <param name="roleName">
        /// The role name.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// </exception>
        /// <exception cref="ArgumentException">
        /// </exception>
        public virtual async Task AddToRoleAsync(TUser user, string roleName)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }

            if (string.IsNullOrEmpty(roleName))
            {
                throw new ArgumentException("Argument cannot be null or empty: roleName.");
            }

            var roles = await DbContent.RoleRepository.FindAsync(c => c.Name == roleName);
            var enumerable = roles as TRole[] ?? roles.ToArray();

            if (roles != null && enumerable.Any())
            {
                var roleId = enumerable.First().Id;
                var userRole = new IdentityUserRole<TKey, TRoleKey>
                {
                    RoleId = roleId,
                    UserId = user.Id,
                };

                await DbContent.UserRoleRepository.InsertAsync(userRole);
            }
        }

        /// <summary>
        /// The get roles async.
        /// </summary>
        /// <param name="user">
        /// The user.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// </exception>
        public virtual async Task<IList<string>> GetRolesAsync(TUser user)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }

            var sql = GetRoleNamesSqlString();
            var roles = await DbContent.RoleRepository.QueryAsync<string>(sql, user);
            var rs = new List<string>();

            if (roles == null)
                return rs;

            rs.AddRange(roles);

            return rs;
        }

        /// <summary>
        /// The get role names sql string.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        private string GetRoleNamesSqlString()
        {
            var roletableName = DbContent.RoleRepository.GetTableName<TRole>();
            var userRoleTableName = DbContent.UserRoleRepository.GetTableName<IdentityUserRole<TKey, TRoleKey>>();
            return string.Format("select name from {0} a inner join {1} b on a.Id=b.RoleId where b.UserId=@Id", roletableName, userRoleTableName);
        }

        /// <summary>
        /// The get is in role sql string.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        private string GetIsInRoleSqlString()
        {
            var roletableName = DbContent.RoleRepository.GetTableName<TRole>();
            var userRoleTableName = DbContent.UserRoleRepository.GetTableName<IdentityUserRole<TKey, TRoleKey>>();
            return string.Format("select name from {0} a inner join {1} b on a.Id=b.RoleId where b.UserId=@UserId and a.Name=@Name", roletableName, userRoleTableName);

        }

        /// <summary>
        /// The is in role async.
        /// </summary>
        /// <param name="user">
        /// The user.
        /// </param>
        /// <param name="roleName">
        /// The role name.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// </exception>
        public virtual async Task<bool> IsInRoleAsync(TUser user, string roleName)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }
            if (string.IsNullOrEmpty(roleName))
            {
                throw new ArgumentNullException("roleName");
            }

            var sql = GetIsInRoleSqlString();
            var param = new DynamicParameters();
            param.Add("UserId", user.Id);
            param.Add("Name", roleName);
            var roles = await DbContent.RoleRepository.QueryAsync<string>(sql, param);
            if (roles != null && roles.Any())
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// The remove from role async.
        /// </summary>
        /// <param name="user">
        /// The user.
        /// </param>
        /// <param name="roleName">
        /// The role name.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// </exception>
        public virtual async Task RemoveFromRoleAsync(TUser user, string roleName)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }

            if (string.IsNullOrEmpty(roleName))
            {
                throw new ArgumentNullException("roleName");
            }

            var roles = await DbContent.RoleRepository.FindAsync(c => c.Name == roleName);
            var enumerable = roles as TRole[] ?? roles.ToArray();

            if (roles != null && enumerable.Any())
            {
                var role = enumerable.First();

                await DbContent.UserRoleRepository.RemoveAsync(c =>
                    (object)c.RoleId == (object)role.Id && (object)c.UserId == (object)user.Id);
            }
        }

        /// <summary>
        /// The get password hash async.
        /// </summary>
        /// <param name="user">
        /// The user.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// </exception>
        public virtual async Task<string> GetPasswordHashAsync(TUser user)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }

            return await Task.FromResult("d+RH5IYMuEJ1n/GixDnRbifwDJ1+c5dBLrol/hjChHdj8H5mdbohyPwo0o09VWirgnXbJN6RPINkuSlcnBxGC1LFxlKATfnstoCptJF6OBI=");
        }

        /// <summary>
        /// The has password async.
        /// </summary>
        /// <param name="user">
        /// The user.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public virtual async Task<bool> HasPasswordAsync(TUser user)
        {
            var passwordHash = await GetPasswordHashAsync(user);
            return string.IsNullOrEmpty(passwordHash);
        }

        /// <summary>
        /// The set password hash async.
        /// </summary>
        /// <param name="user">
        /// The user.
        /// </param>
        /// <param name="passwordHash">
        /// The password hash.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// </exception>
        public virtual async Task SetPasswordHashAsync(TUser user, string passwordHash)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }
            user.PasswordHash = passwordHash;
            await DbContent.UserRepository.UpdateAsync(user);
        }

        /// <summary>
        /// The get security stamp async.
        /// </summary>
        /// <param name="user">
        /// The user.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// </exception>
        public virtual async Task<string> GetSecurityStampAsync(TUser user)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }
            return await Task.FromResult(user.SecurityStamp);
        }

        /// <summary>
        /// The set security stamp async.
        /// </summary>
        /// <param name="user">
        /// The user.
        /// </param>
        /// <param name="stamp">
        /// The stamp.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// </exception>
        public virtual async Task SetSecurityStampAsync(TUser user, string stamp)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }
            user.SecurityStamp = stamp;
            await DbContent.UserRepository.UpdateAsync(user);
        }

        /// <summary>
        /// Gets the users.
        /// </summary>
        public virtual IQueryable<TUser> Users
        {
            get
            {
                var users = GetAllUsers();
                return users.AsQueryable();
            }
        }

        /// <summary>
        /// The get all users.
        /// </summary>
        /// <returns>
        /// The <see>
        ///         <cref>IEnumerable</cref>
        ///     </see>
        ///     .
        /// </returns>
        private IEnumerable<TUser> GetAllUsers()
        {
            var users = DbContent.UserRepository.Find(c => c.UserId != " ");
            return users;
        }

        /// <summary>
        /// The find by email async.
        /// </summary>
        /// <param name="email">
        /// The email.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public virtual async Task<TUser> FindByEmailAsync(string email)
        {
            var users = await DbContent.UserRepository.FindAsync(c => c.Email == email);
            var enumerable = users as TUser[] ?? users.ToArray();

            if (users != null && enumerable.Any())
                return enumerable.First();

            return null;
        }

        /// <summary>
        /// The get email async.
        /// </summary>
        /// <param name="user">
        /// The user.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// </exception>
        public virtual async Task<string> GetEmailAsync(TUser user)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }
            return await Task.FromResult(user.Email);
        }

        /// <summary>
        /// The get email confirmed async.
        /// </summary>
        /// <param name="user">
        /// The user.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// </exception>
        public virtual async Task<bool> GetEmailConfirmedAsync(TUser user)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }
            return await Task.FromResult(user.EmailConfirmed);
        }

        /// <summary>
        /// The set email async.
        /// </summary>
        /// <param name="user">
        /// The user.
        /// </param>
        /// <param name="email">
        /// The email.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// </exception>
        public virtual async Task SetEmailAsync(TUser user, string email)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }
            user.Email = email;
            await DbContent.UserRepository.UpdateAsync(user);
        }

        /// <summary>
        /// The set email confirmed async.
        /// </summary>
        /// <param name="user">
        /// The user.
        /// </param>
        /// <param name="confirmed">
        /// The confirmed.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// </exception>
        public virtual async Task SetEmailConfirmedAsync(TUser user, bool confirmed)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }
            user.EmailConfirmed = confirmed;
            await DbContent.UserRepository.UpdateAsync(user);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public virtual async Task<bool> GetTwoFactorEnabledAsync(TUser user)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }

            return await Task.FromResult(user.TwoFactorEnabled);
        }

        /// <summary>
        /// The set two factor enabled async.
        /// </summary>
        /// <param name="user">
        /// The user.
        /// </param>
        /// <param name="enabled">
        /// The enabled.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// </exception>
        public virtual async Task SetTwoFactorEnabledAsync(TUser user, bool enabled)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }
            user.TwoFactorEnabled = enabled;
            await DbContent.UserRepository.UpdateAsync(user);
        }

        /// <summary>
        /// The get access failed count async.
        /// </summary>
        /// <param name="user">
        /// The user.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// </exception>
        public virtual async Task<int> GetAccessFailedCountAsync(TUser user)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }
            return await Task.FromResult(user.AccessFailedCount);
        }

        /// <summary>
        /// The get lockout enabled async.
        /// </summary>
        /// <param name="user">
        /// The user.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// </exception>
        public virtual async Task<bool> GetLockoutEnabledAsync(TUser user)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }

            return await Task.FromResult(user.LockoutEnabled);
        }

        /// <summary>
        /// The get lockout end date async.
        /// </summary>
        /// <param name="user">
        /// The user.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// </exception>
        public virtual async Task<DateTimeOffset> GetLockoutEndDateAsync(TUser user)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }

            return await Task.FromResult(user.LockoutEndDateUtc.HasValue
                    ? new DateTimeOffset(DateTime.SpecifyKind(user.LockoutEndDateUtc.Value, DateTimeKind.Utc))
                    : new DateTimeOffset());
        }

        /// <summary>
        /// The increment access failed count async.
        /// </summary>
        /// <param name="user">
        /// The user.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// </exception>
        public virtual async Task<int> IncrementAccessFailedCountAsync(TUser user)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }

            user.AccessFailedCount++;
            await DbContent.UserRepository.UpdateAsync(user);
            return user.AccessFailedCount;
        }

        /// <summary>
        /// The reset access failed count async.
        /// </summary>
        /// <param name="user">
        /// The user.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// </exception>
        public virtual async Task ResetAccessFailedCountAsync(TUser user)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }
            user.AccessFailedCount = 0;
            await DbContent.UserRepository.UpdateAsync(user);
        }

        /// <summary>
        /// The set lockout enabled async.
        /// </summary>
        /// <param name="user">
        /// The user.
        /// </param>
        /// <param name="enabled">
        /// The enabled.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// </exception>
        public virtual async Task SetLockoutEnabledAsync(TUser user, bool enabled)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }

            user.LockoutEnabled = enabled;
            await DbContent.UserRepository.UpdateAsync(user);
        }

        /// <summary>
        /// The set lockout end date async.
        /// </summary>
        /// <param name="user">
        /// The user.
        /// </param>
        /// <param name="lockoutEnd">
        /// The lockout end.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// </exception>
        public virtual async Task SetLockoutEndDateAsync(TUser user, DateTimeOffset lockoutEnd)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }

            user.LockoutEndDateUtc = lockoutEnd.UtcDateTime;
            await DbContent.UserRepository.UpdateAsync(user);
        }
        
        #region IDisposable Support
        /// <summary>
        /// The disposed value.
        /// </summary>
        private bool disposedValue = false; // To detect redundant calls

        /// <summary>
        /// The dispose.
        /// </summary>
        /// <param name="disposing">
        /// The disposing.
        /// </param>
        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                }

                // free unmanaged resources (unmanaged objects) and override a finalizer below.
                // set large fields to null.
                disposedValue = true;
            }
        }

        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);

            // uncomment the following line if the finalizer is overridden above.
            GC.SuppressFinalize(this);
        }
        #endregion
    }
}