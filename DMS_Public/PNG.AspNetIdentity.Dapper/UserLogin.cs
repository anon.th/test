﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="UserLogin.cs" company="Aware Corporation Limited">
//   Copyright (c) 2014-2015.  All rights reserved.
// </copyright>
// <summary>
//   Defines the IdentityUserLogin type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;

namespace PNG.AspNetIdentity.Dapper
{
    public class IdentityUserLogin<TKey>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="IdentityUserLogin{TKey}"/> class.
        /// </summary>
        public IdentityUserLogin()
        {
            Id = Guid.NewGuid().ToString();
        }

        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Gets or sets the user id.
        /// </summary>
        public TKey UserId { get; set; }

        /// <summary>
        /// Gets or sets the login provider.
        /// </summary>
        public string LoginProvider { get; set; }

        /// <summary>
        /// Gets or sets the provider key.
        /// </summary>
        public string ProviderKey { get; set; }
    }
}