﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="UserRole.cs" company="Aware Corporation Limited">
//   Copyright (c) 2014-2015.  All rights reserved.
// </copyright>
// <summary>
//   Defines the IdentityUserRole type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;

namespace PNG.AspNetIdentity.Dapper
{
    /// <summary>
    /// The identity user role.
    /// </summary>
    /// <typeparam name="TKey">
    /// The generic type of TKey
    /// </typeparam>
    /// <typeparam name="TRoleKey">
    /// The generic type of TRoleKey
    /// </typeparam>
    public class IdentityUserRole<TKey, TRoleKey>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="IdentityUserRole{TKey,TRoleKey}"/> class.
        /// </summary>
        public IdentityUserRole()
        {
            Id = Guid.NewGuid().ToString();
        }

        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Gets or sets the user id.
        /// </summary>
        public TKey UserId { get; set; }

        /// <summary>
        /// Gets or sets the role id.
        /// </summary>
        public TRoleKey RoleId { get; set; }
    }
}