﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IdentityUser.cs" company="Aware Corporation Limited">
//   Copyright (c) 2014-2015.  All rights reserved.
// </copyright>
// <summary>
//   Defines the IdentityUser type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using Microsoft.AspNet.Identity;
using PNG.Dapper.Extensions.Utils;
using System;

namespace PNG.AspNetIdentity.Dapper
{
    /// <summary>
    /// The identity user.
    /// </summary>
    public class User_Master : User_Master<string>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="IdentityUser"/> class.
        /// </summary>
        public User_Master()
        {
            Id = Guid.NewGuid().ToString();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="IdentityUser"/> class.
        /// </summary>
        /// <param name="name">
        /// The name.
        /// </param>
        public User_Master(string name)
            : base(name)
        {
            Id = Guid.NewGuid().ToString();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="name"></param>
        public User_Master(string id, string name)
            : base(id, name)
        {
        }
    }

    /// <summary>
    /// The identity user.
    /// </summary>
    /// <typeparam name="TKey">
    /// </typeparam>
    public class User_Master<TKey> : IUser<TKey>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="IdentityUser{TKey}"/> class.
        /// Default constructor
        /// </summary>
        public User_Master()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="IdentityUser{TKey}"/> class.
        /// Constructor that takes user name as argument
        /// </summary>
        /// <param name="userName">
        /// </param>
        public User_Master(string userName)
            : this()
        {
            UserId = userName;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="IdentityUser{TKey}"/> class.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <param name="userName">
        /// The user name.
        /// </param>
        public User_Master(TKey id, string userName)
            : this(userName)
        {
            Id = id;
        }

        /// <summary>
        /// User ID
        /// </summary>
        public TKey Id { get; set; }

        /// <summary>
        /// User's name
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// User's name
        /// </summary>
        public string UserId { get; set; }


        public string User_Password { get; set; }


        public string Department_Name { get; set; }


        public string PayerId { get; set; }

        /// <summary>
        /// Email
        /// </summary>
        public virtual string Email { get; set; }

        /// <summary>
        /// True if the email is confirmed, default is false
        /// </summary>
        public virtual bool EmailConfirmed { get; set; }

        /// <summary>
        /// The salted/hashed form of the user password
        /// </summary>
        public virtual string PasswordHash { get; set; }

        /// <summary>
        /// A random value that should change whenever a users credentials have changed (password changed, login removed)
        /// </summary>
        public virtual string SecurityStamp { get; set; }

        /// <summary>
        /// Is two factor enabled for the user
        /// </summary>
        public virtual bool TwoFactorEnabled { get; set; }

        /// <summary>
        /// DateTime in UTC when lockout ends, any time in the past is considered not locked out.
        /// </summary>
        public virtual DateTime? LockoutEndDateUtc { get; set; }

        /// <summary>
        /// Is lockout enabled for this user
        /// </summary>
        public virtual bool LockoutEnabled { get; set; }

        /// <summary>
        /// Used to record failures for the purposes of lockout
        /// </summary>
        public virtual int AccessFailedCount { get; set; }

        /// <summary>
        /// Is Active
        /// </summary>
        public virtual bool? IsActive { get; set; }

        /// <summary>
        /// Created By
        /// </summary>
        public virtual string CreatedBy { get; set; }

        /// <summary>
        /// Created Date
        /// </summary>
        public virtual DateTime? CreatedDate { get; set; }

        /// <summary>
        /// Modified By
        /// </summary>
        public virtual string ModifiedBy { get; set; }

        /// <summary>
        /// Modified Date
        /// </summary>
        public virtual DateTime? ModifiedDate { get; set; }

        /// <summary>
        /// Guid for request reset password
        /// </summary>
        public virtual string RequestResetPasswordGuid { get; set; }

        /// <summary>
        /// DateTime in UTC when request reset password (timestamp)
        /// </summary>
        public virtual DateTime? RequestResetPasswordDate { get; set; }

        /// <summary>
        /// Effective Date.
        /// </summary>
        public virtual DateTime? EffectiveDate { get; set; }

        /// <summary>
        /// Expiry Date.
        /// </summary>
        public virtual DateTime? ExpiryDate { get; set; }

        /// <summary>
        /// DefaultRole
        /// </summary>
        public virtual TKey DefaultRoleId { get; set; }

        /// <summary>
        /// Signature File
        /// </summary>
        public virtual int? SignatureFileId { get; set; }

        /// <summary>
        /// PasswordChangedDate
        /// </summary>
        public virtual DateTime? PasswordChangedDate { get; set; }

        /// <summary>
        /// Department Id
        /// </summary>
        public virtual int? DepartmentId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public virtual string UserTypeId { get; set; }

        /// <summary>
        /// AuditUserId
        /// </summary>
        [Ignored]
        public virtual int AuditUserId { get; set; }
    }
}