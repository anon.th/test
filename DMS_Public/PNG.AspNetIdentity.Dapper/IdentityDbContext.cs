﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IdentityDbContext.cs" company="Aware Corporation Limited">
//   Copyright (c) 2014-2015.  All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

using PNG.Dapper.Extensions;
using System;
using System.Data;

namespace PNG.AspNetIdentity.Dapper
{
    /// <summary>
    /// The dapper identity db context.
    /// </summary>
    /// <typeparam name="TUser">
    /// The generic type of TUser
    /// </typeparam>
    /// <typeparam name="TRole">
    /// The generic type of TRole
    /// </typeparam>
    public class DapperIdentityDbContext<TUser, TRole> : IdentityDbContext<TUser, TRole, string, string>
        where TUser : User_Master<string>
        where TRole : IdentityRole<string>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DapperIdentityDbContext{TUser,TRole}"/> class.
        /// </summary>
        /// <param name="connection">
        /// The connection.
        /// </param>
        public DapperIdentityDbContext(IDbConnection connection)
            : base(connection, null)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DapperIdentityDbContext{TUser,TRole}"/> class.
        /// </summary>
        /// <param name="connection">
        /// The connection.
        /// </param>
        /// <param name="transaction">
        /// The transaction.
        /// </param>
        public DapperIdentityDbContext(IDbConnection connection, IDbTransaction transaction)
            : base(connection, transaction)
        {
        }
    }

    /// <summary>
    /// The identity db context.
    /// </summary>
    /// <typeparam name="TUser">
    /// The generic type of TUser
    /// </typeparam>
    /// <typeparam name="TRole">
    /// The generic type of TRole
    /// </typeparam>
    /// <typeparam name="TKey">
    /// The generic type of TKey
    /// </typeparam>
    /// <typeparam name="TRoleKey">
    /// The generic type of TRoleKey
    /// </typeparam>
    public class IdentityDbContext<TUser, TRole, TKey, TRoleKey> : IDisposable
        where TUser : User_Master<TKey>
        where TRole : IdentityRole<TRoleKey>
    {
        /// <summary>
        /// The _conn.
        /// </summary>
        private readonly IDbConnection _conn;

        /// <summary>
        /// The _transaction.
        /// </summary>
        private readonly IDbTransaction _transaction;

        /// <summary>
        /// The _user repository.
        /// </summary>
        private IRepository<TUser, TKey> _userRepository;

        /// <summary>
        /// The _user login repository.
        /// </summary>
        private IRepository<IdentityUserLogin<TKey>> _userLoginRepository;

        /// <summary>
        /// The _user claim repository.
        /// </summary>
        private IRepository<IdentityUserClaim<TKey>> _userClaimRepository;

        /// <summary>
        /// The _role repository.
        /// </summary>
        private IRepository<TRole, TRoleKey> _roleRepository;

        /// <summary>
        /// The _user role repository.
        /// </summary>
        private IRepository<IdentityUserRole<TKey, TRoleKey>> _userRoleRepository;

        /// <summary>
        /// Gets the user repository.
        /// </summary>
        public IRepository<TUser, TKey> UserRepository
        {
            get
            {
                return _userRepository ?? (_userRepository = new DapperRepository<TUser, TKey>(DbConnection, DbTransaction));
            }
        }

        /// <summary>
        /// Gets the role repository.
        /// </summary>
        public IRepository<TRole, TRoleKey> RoleRepository
        {
            get
            {
                return _roleRepository ?? (_roleRepository = new DapperRepository<TRole, TRoleKey>(DbConnection, DbTransaction));
            }
        }

        /// <summary>
        /// Gets the user role repository.
        /// </summary>
        public IRepository<IdentityUserRole<TKey, TRoleKey>> UserRoleRepository
        {
            get
            {
                return _userRoleRepository ?? (_userRoleRepository = new DapperRepository<IdentityUserRole<TKey, TRoleKey>>(DbConnection, DbTransaction));
            }
        }

        /// <summary>
        /// Gets the user login repository.
        /// </summary>
        public IRepository<IdentityUserLogin<TKey>> UserLoginRepository
        {
            get
            {
                return _userLoginRepository ?? (_userLoginRepository = new DapperRepository<IdentityUserLogin<TKey>>(DbConnection, DbTransaction));
            }
        }

        /// <summary>
        /// Gets the user claim repository.
        /// </summary>
        public IRepository<IdentityUserClaim<TKey>> UserClaimRepository
        {
            get
            {
                return _userClaimRepository ?? (_userClaimRepository = new DapperRepository<IdentityUserClaim<TKey>>(DbConnection, DbTransaction));
            }
        }

        /// <summary>
        /// Gets the db connection.
        /// </summary>
        public IDbConnection DbConnection
        {
            get
            {
                return _conn;
            }
        }

        /// <summary>
        /// Gets the db transaction.
        /// </summary>
        public IDbTransaction DbTransaction
        {
            get
            {
                return _transaction;
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="IdentityDbContext{TUser,TRole,TKey,TRoleKey}"/> class.
        /// </summary>
        /// <param name="connection">
        /// The connection.
        /// </param>
        public IdentityDbContext(IDbConnection connection)
            : this(connection, null)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="IdentityDbContext{TUser,TRole,TKey,TRoleKey}"/> class.
        /// </summary>
        /// <param name="connection">
        /// The connection.
        /// </param>
        /// <param name="transaction">
        /// The transaction.
        /// </param>
        /// <exception cref="ArgumentNullException">
        /// </exception>
        public IdentityDbContext(IDbConnection connection, IDbTransaction transaction)
        {
            if (connection == null)
            {
                throw new ArgumentNullException("connection");
            }
            _conn = connection;
            _transaction = transaction;
        }

        #region IDisposable Support
        /// <summary>
        /// The disposed value.
        /// </summary>
        private bool disposedValue = false; // To detect redundant calls

        /// <summary>
        /// The dispose.
        /// </summary>
        /// <param name="disposing">
        /// The disposing.
        /// </param>
        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    if (_conn.State != ConnectionState.Closed)
                    {
                        _conn.Close();
                    }
                }

                // free unmanaged resources (unmanaged objects) and override a finalizer below.
                // set large fields to null.
                disposedValue = true;
            }
        }

        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);

            // uncomment the following line if the finalizer is overridden above.
            GC.SuppressFinalize(this);
        }
        #endregion
    }
}