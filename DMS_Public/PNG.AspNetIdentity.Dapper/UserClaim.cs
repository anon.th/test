﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="UserClaim.cs" company="Aware Corporation Limited">
//   Copyright (c) 2014-2015.  All rights reserved.
// </copyright>
// <summary>
//   Defines the IdentityUserClaim type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;

namespace PNG.AspNetIdentity.Dapper
{
    /// <summary>
    /// The identity user claim.
    /// </summary>
    /// <typeparam name="TKey">
    /// </typeparam>
    public class IdentityUserClaim<TKey>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="IdentityUserClaim{TKey}"/> class.
        /// </summary>
        public IdentityUserClaim()
        {
            Id = Guid.NewGuid().ToString();
        }

        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Gets or sets the user id.
        /// </summary>
        public TKey UserId { get; set; }

        /// <summary>
        /// Gets or sets the claim value.
        /// </summary>
        public string ClaimValue { get; set; }

        /// <summary>
        /// Gets or sets the claim type.
        /// </summary>
        public string ClaimType { get; set; }
    }
}