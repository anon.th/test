﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="RoleStore.cs" company="Aware Corporation Limited">
//   Copyright (c) 2014-2015.  All rights reserved.
// </copyright>
// <summary>
//   Defines the RoleStore type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using Microsoft.AspNet.Identity;
using PNG.AspNetIdentity.Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PNG.AspnetIdentity.Dapper
{
    /// <summary>
    /// The role store.
    /// </summary>
    /// <typeparam name="TUser">
    /// </typeparam>
    /// <typeparam name="TRole">
    /// </typeparam>
    public class RoleStore<TUser, TRole> : RoleStore<TUser, TRole, string, string>
        where TUser : User_Master
        where TRole : IdentityRole
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RoleStore{TUser,TRole}"/> class.
        /// </summary>
        /// <param name="dbContext">
        /// The db context.
        /// </param>
        public RoleStore(DapperIdentityDbContext<TUser, TRole> dbContext)
            : base(dbContext)
        {
        }
    }

    /// <summary>
    /// The role store.
    /// </summary>
    /// <typeparam name="TUser">
    /// The generic type of TUser
    /// </typeparam>
    /// <typeparam name="TRole">
    /// The generic type of TRole
    /// </typeparam>
    /// <typeparam name="TKey">
    /// The generic type of TKey
    /// </typeparam>
    /// <typeparam name="TRoleKey">
    /// The generic type of TRoleKey
    /// </typeparam>
    public class RoleStore<TUser, TRole, TKey, TRoleKey> : IQueryableRoleStore<TRole, TRoleKey>
        where TUser : User_Master<TKey>
        where TRole : IdentityRole<TRoleKey>
    {
        /// <summary>
        /// The _db content.
        /// </summary>
        private readonly IdentityDbContext<TUser, TRole, TKey, TRoleKey> _dbContent;

        /// <summary>
        /// Gets the db content.
        /// </summary>
        public IdentityDbContext<TUser, TRole, TKey, TRoleKey> DbContent
        {
            get { return _dbContent; }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="RoleStore{TUser,TRole,TKey,TRoleKey}"/> class.
        /// </summary>
        /// <param name="dbContent">
        /// The db content.
        /// </param>
        /// <exception cref="ArgumentNullException">
        /// </exception>
        public RoleStore(IdentityDbContext<TUser, TRole, TKey, TRoleKey> dbContent)
        {
            if (dbContent == null)
            {
                throw new ArgumentNullException("dbContent");
            }

            _dbContent = dbContent;
        }

        /// <summary>
        /// Gets the roles.
        /// </summary>
        public virtual IQueryable<TRole> Roles
        {
            get
            {
                var roles = GetAllRoles();
                return roles.AsQueryable();
            }
        }

        /// <summary>
        /// The get all roles.
        /// </summary>
        /// <returns>
        /// The <see>
        ///         <cref>IEnumerable</cref>
        ///     </see>
        ///     .
        /// </returns>
        /// <exception cref="Exception">
        /// </exception>
        private IEnumerable<TRole> GetAllRoles()
        {
            try
            {
                var tableName = DbContent.RoleRepository.GetTableName<TRole>();
                var sql = string.Format("SELECT * FROM {0}", tableName);
                var roles = DbContent.RoleRepository.Query<TRole>(sql, null);

                return roles;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }
        }

        /// <summary>
        /// The create async.
        /// </summary>
        /// <param name="role">
        /// The role.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// </exception>
        public virtual async Task CreateAsync(TRole role)
        {
            if (role == null)
            {
                throw new ArgumentNullException("role");
            }
            await DbContent.RoleRepository.InsertAsync(role);
        }

        /// <summary>
        /// The delete async.
        /// </summary>
        /// <param name="role">
        /// The role.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// </exception>
        public virtual async Task DeleteAsync(TRole role)
        {
            if (role == null)
            {
                throw new ArgumentNullException("role");
            }
            await DbContent.RoleRepository.RemoveAsync(role);
        }

        /// <summary>
        /// The find by id async.
        /// </summary>
        /// <param name="roleId">
        /// The role id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public virtual async Task<TRole> FindByIdAsync(TRoleKey roleId)
        {
            return await DbContent.RoleRepository.GetAsync(roleId);
        }

        /// <summary>
        /// The find by name async.
        /// </summary>
        /// <param name="roleName">
        /// The role name.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public virtual async Task<TRole> FindByNameAsync(string roleName)
        {
            var roles = await DbContent.RoleRepository.FindAsync(c => c.Name == roleName);
            return roles.FirstOrDefault();
        }

        /// <summary>
        /// The update async.
        /// </summary>
        /// <param name="role">
        /// The role.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// </exception>
        public virtual async Task UpdateAsync(TRole role)
        {
            if (role == null)
            {
                throw new ArgumentNullException("role");
            }
            await DbContent.RoleRepository.UpdateAsync(role);
        }
        
        #region IDisposable Support
        /// <summary>
        /// The disposed value.
        /// </summary>
        private bool disposedValue = false; // To detect redundant calls

        /// <summary>
        /// The dispose.
        /// </summary>
        /// <param name="disposing">
        /// The disposing.
        /// </param>
        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                }

                // free unmanaged resources (unmanaged objects) and override a finalizer below.
                // set large fields to null.
                disposedValue = true;
            }
        }

        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);

            // uncomment the following line if the finalizer is overridden above.
            GC.SuppressFinalize(this);
        }
        #endregion
    }
}