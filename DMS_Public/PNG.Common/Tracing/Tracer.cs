﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Tracer.cs" company="Aware Technology Solutions Corporation Ltd">
//   Copyright (c) 2016-2017.  All rights reserved.
// </copyright>
// <summary>
//   Defines the Tracer type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace PNG.Common.Services.Tracing
{
    public static class Tracer
    {
        private const string DateFormat = "yyyy-MM-dd HH:mm:ss.ffffff";

        /// <summary>
        /// 
        /// </summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void TraceIn()
        {
#if TRACE
            var method = new StackFrame(1, false).GetMethod();
            var type = method.DeclaringType;

            if (type != null)
            {
                var message = DateTime.Now.ToString(DateFormat) + " " + type.Name + "." + method.Name + " {";
                Trace.WriteLine(message);
            }

            Trace.Indent();
#endif
        }

        /// <summary>
        /// 
        /// </summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void TraceOut()
        {
#if TRACE
            var method = new StackFrame(1, false).GetMethod();
            var type = method.DeclaringType;

            if (type == null)
                return;

            var message = DateTime.Now.ToString(DateFormat) + " " + type.Name + "." + method.Name + " }";

            Trace.Unindent();
            Trace.WriteLine(message);
#endif
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void TraceOut(object value)
        {
#if TRACE
            Trace.WriteLine("Returning: " + value);

            var method = new StackFrame(1, false).GetMethod();
            var type = method.DeclaringType;

            if (type == null)
                return;

            var message = DateTime.Now.ToString(DateFormat) + " " + type.Name + "." + method.Name + " }";

            Trace.Unindent();
            Trace.WriteLine(message);
#endif
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void WriteLine(string message)
        {
            Trace.WriteLine(DateTime.Now.ToString(DateFormat) + " " + message);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void TraceInfo(string message)
        {
            Trace.TraceInformation(DateTime.Now.ToString(DateFormat) + " " + message);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ex"></param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void TraceError(Exception ex)
        {
            TraceError(ex.ToString());
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void TraceError(string message)
        {
            Trace.TraceInformation(DateTime.Now.ToString(DateFormat) + " " + message);
        }

        /// <summary>
        /// 
        /// </summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void Indent()
        {
            Trace.Indent();
        }

        /// <summary>
        /// 
        /// </summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void Unindent()
        {
            Trace.Unindent();
        }
    }
}