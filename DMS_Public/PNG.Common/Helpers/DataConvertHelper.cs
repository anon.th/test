﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DataConvertHelper.cs" company="Aware Technology Solutions Corporation Ltd">
//   Copyright (c) 2016-2017.  All rights reserved.
// </copyright>
// <summary>
//   Defines the DataConvertHelper type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;

namespace PNG.Common.Helpers
{
    /// <summary>
    ///
    /// </summary>
    public static class DataConvertHelper
    {
        /// <summary>
        ///
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        public static bool IsNumeric(this object expression)
        {
            double retNum;

            var isNum = Double.TryParse(Convert.ToString(expression), NumberStyles.Any, NumberFormatInfo.InvariantInfo, out retNum);
            return isNum;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="value"></param>
        /// <param name="parameterFormat"></param>
        /// <returns></returns>
        public static string GetDateValue(this string value, string parameterFormat)
        {
            string result;

            try
            {
                var dateStringArray = parameterFormat.Split(':');
                DateTime dateValue;
                result = DateTime.TryParseExact(value, dateStringArray[0],
                    new CultureInfo(dateStringArray[1]), DateTimeStyles.None, out dateValue)
                    ? dateValue.ToString(DateTimeHelper.DateStringFormat, ConfigurationHelper.DefaultCultureInfo)
                    : null;
            }
            catch
            {
                result = value;
            }

            return result;
        }

        #region ToStr

        /// <summary>
        ///
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string ToStr(object value)
        {
            return ToStr(value, "", "");
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="value"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public static string ToStr(object value, string defaultValue)
        {
            return ToStr(value, defaultValue, "");
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="value"></param>
        /// <param name="defaultValue"></param>
        /// <param name="format"></param>
        /// <returns></returns>
        public static string ToStr(object value, string defaultValue, string format)
        {
            try
            {
                if (string.IsNullOrEmpty(value.ToString()))
                    return !string.IsNullOrEmpty(defaultValue) ? defaultValue : string.Empty;

                if ((value is DateTime) && !string.IsNullOrEmpty(format))
                    return ((DateTime)value).ToString(format);

                return value.ToString();
            }
            catch
            {
                return !string.IsNullOrEmpty(defaultValue) ? defaultValue : string.Empty;
            }
        }

        public static string convertMinToStr(string totalTime)
        {
            try
            {
                var time = Int32.Parse(totalTime);

                int tmp = 0;
                int days = 0;
                int hours = 0;
                int minutes = 0;
                if (time >= 60)
                {
                    days = time/480;
                    tmp = time%480;
                    hours = tmp/60;
                    minutes = tmp%60;
                }
                else
                {
                    minutes = time;
                    hours = 0;
                    days = 0;
                }

                var hrmin_str = "";
                if (hours > 0 && minutes > 0)
                {
                    hrmin_str = hours.ToString();
                    if (minutes == 30)
                    {
                        hrmin_str += ".5 Hours";
                    }
                    else
                    {
                        hrmin_str += " Hours";
                    }
                }

                if (hours == 0 && minutes > 0)
                {
                    hrmin_str = "0.5 Hours";
                }

                if (hours > 0 && minutes == 0)
                {
                    if (hours == 1)
                    {
                        hrmin_str = hours + " Hour";
                    }
                    else
                    {
                        hrmin_str = hours + " Hours";
                    }

                }

                var days_str = "";
                if (days > 0)
                {
                    if (days > 1)
                    {
                        days_str = days + " Days ";
                    }
                    else
                    {
                        days_str = days + " Day ";
                    }
                }

                return days_str + hrmin_str;
            }
            catch
            {
                return "";
            }
        }

        #endregion ToStr

        #region ToDate

        /// <summary>
        ///
        /// </summary>
        /// <param name="value"></param>
        /// <param name="parameterFormat"></param>
        /// <param name="defaultDate"></param>
        /// <returns></returns>
        public static DateTime? ToDate(object value, string parameterFormat, DateTime? defaultDate = null)
        {
            return ToDateFormat(value, parameterFormat, false, defaultDate);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="value"></param>
        /// <param name="parameterFormat"></param>
        /// <param name="usingDateFormat"></param>
        /// <param name="defaultDate"></param>
        /// <returns></returns>
        public static DateTime? ToDateFormat(object value, string parameterFormat, bool usingDateFormat, DateTime? defaultDate = null)
        {
            var dateValue = new DateTime();
            string[] dateStringArray = null;

            try
            {
                if (IsNumeric(value) && ToDbl(value) > 0)
                {
                    dateValue = DateTime.FromOADate(ToDbl(value));
                }
                else
                {
                    if (!string.IsNullOrEmpty(parameterFormat))
                        dateStringArray = parameterFormat.Split(':');

                    if (usingDateFormat)
                    {
                        // TODO: enhanced to support BKK
                        if (dateStringArray != null && DateTime.TryParseExact(ToStr(value, string.Empty, dateStringArray[0]), dateStringArray[0],
                            new CultureInfo(dateStringArray[1]), DateTimeStyles.None, out dateValue))
                        {
                            return dateValue;
                        }
                    }
                    else
                    {
                        // TODO: enhanced to support CNX
                        if (DateTime.TryParseExact(ToStr(value, string.Empty, DateTimeHelper.DateStringFormat), DateTimeHelper.DateStringFormat,
                            ConfigurationHelper.DefaultCultureInfo, DateTimeStyles.None, out dateValue))
                        {
                            return dateValue;
                        }

                        return null;
                    }
                }
            }
            catch
            {
                if (defaultDate.HasValue)
                {
                    return defaultDate.Value;
                }

                return null;
            }

            return dateValue;
        }

        #endregion ToDate

        #region ToShort

        /// <summary>
        ///
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static Int16 ToShort(object value)
        {
            try
            {
                return Int16.Parse(value.ToString());
            }
            catch
            {
                return 0;
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="value"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public static Int16 ToShort(object value, Int16 defaultValue)
        {
            try
            {
                return Int16.Parse(value.ToString());
            }
            catch
            {
                return defaultValue;
            }
        }

        #endregion ToShort

        #region ToInt

        /// <summary>
        ///
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static int ToInt(object value)
        {
            try
            {
                return int.Parse(value.ToString());
            }
            catch
            {
                return 0;
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="value"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public static int ToInt(object value, int defaultValue)
        {
            try
            {
                return int.Parse(value.ToString());
            }
            catch
            {
                return defaultValue;
            }
        }

        #endregion ToInt

        #region ToLong

        /// <summary>
        ///
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static long ToLong(object value)
        {
            try
            {
                return long.Parse(value.ToString());
            }
            catch
            {
                return 0;
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="value"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public static long ToLong(object value, int defaultValue)
        {
            try
            {
                return long.Parse(value.ToString());
            }
            catch
            {
                return defaultValue;
            }
        }

        #endregion ToLong

        #region ToDec

        /// <summary>
        ///
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static decimal ToDec(object value)
        {
            try
            {
                return decimal.Parse(value.ToString());
            }
            catch
            {
                return 0;
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="value"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public static decimal ToDec(object value, decimal defaultValue)
        {
            try
            {
                return decimal.Parse(value.ToString());
            }
            catch
            {
                return defaultValue;
            }
        }

        #endregion ToDec

        #region ToBoolean

        /// <summary>
        ///
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static bool ToBool(object value)
        {
            try
            {
                return bool.Parse(value.ToString());
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="value"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public static bool ToBool(object value, bool defaultValue)
        {
            try
            {
                return bool.Parse(value.ToString());
            }
            catch
            {
                return defaultValue;
            }
        }

        #endregion ToBoolean

        #region ToDbl

        /// <summary>
        ///
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static double ToDbl(object value)
        {
            try
            {
                return double.Parse(value.ToString());
            }
            catch
            {
                return 0;
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="value"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public static double ToDbl(object value, double defaultValue)
        {
            try
            {
                return double.Parse(value.ToString());
            }
            catch
            {
                return defaultValue;
            }
        }

        #endregion ToDbl

        #region ToFloat

        /// <summary>
        ///
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static float ToFlt(object value)
        {
            try
            {
                return float.Parse(value.ToString());
            }
            catch
            {
                return 0;
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="value"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public static float ToFlt(object value, float defaultValue)
        {
            try
            {
                return float.Parse(value.ToString());
            }
            catch
            {
                return defaultValue;
            }
        }

        #endregion ToFloat

        #region Copy

        /// <summary>
        ///
        /// </summary>
        /// <param name="value"></param>
        /// <param name="startPosition"></param>
        /// <param name="endPosition"></param>
        /// <returns></returns>
        public static string Copy(string value, int startPosition, int endPosition)
        {
            return value.Substring(startPosition - 1, (endPosition - startPosition) + 1);
        }

        #endregion Copy

        #region FixedLength

        /// <summary>
        /// Fixed Length
        /// </summary>
        /// <param name="value"></param>
        /// <param name="startPosition"></param>
        /// <param name="endPosition"></param>
        /// <returns></returns>
        public static string FixedLength(string value, int startPosition, int endPosition)
        {
            var stringArray = value.ToCharArray();
            var index = startPosition - 1;
            var charCount = endPosition - index;

            if (startPosition > stringArray.Count() || charCount < 1)
                return string.Empty;

            //ตรวจสอบ array count เกินตำแหน่ง array จะใช้ตัวสุดท้ายแทน
            charCount = (index + charCount) < stringArray.Count() - 1
                ? charCount : stringArray.Count() - index;

            var stringBuilder = new StringBuilder();
            return stringBuilder.Append(stringArray, index, charCount).ToString();
        }

        #endregion FixedLength

        #region RemoveSpace

        /// <summary>
        ///
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string RemoveSpace(object value)
        {
            return value != null ? value.ToString().Replace(" ", "") : null;
        }

        #endregion RemoveSpace

        /// <summary>
        /// Indicates whether a specified DataTable is null, has zero columns, or (optionally) zero rows.
        /// </summary>
        /// <param name="dataTable">DataTable to check.</param>
        /// <param name="ignoreZeroRows">When set to true, the function will return true even if the table's row count is equal to zero.</param>
        /// <returns>False if the specified DataTable null, has zero columns, or zero rows, otherwise true.</returns>
        public static bool IsValidDatatable(DataTable dataTable, bool ignoreZeroRows = false)
        {
            if (dataTable == null)
                return false;
            if (dataTable.Columns.Count == 0)
                return false;
            if (ignoreZeroRows)
                return true;

            return dataTable.Rows.Count != 0;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="property"></param>
        /// <param name="columnNames"></param>
        /// <param name="row"></param>
        /// <returns></returns>
        public static bool IsValidObjectData(PropertyInfo property, List<string> columnNames, DataRow row)
        {
            return property != null && property.CanWrite && columnNames.Contains(property.Name) && row[property.Name] != DBNull.Value;
        }

        /// <summary>
        ///
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="dataTable"></param>
        /// <returns></returns>
        public static IList<T> ToClassInstanceCollection<T>(DataTable dataTable) where T : class, new()
        {
            if (!IsValidDatatable(dataTable))
                return new List<T>();

            var classType = typeof(T);
            IList<PropertyInfo> propertyList = classType.GetProperties();

            // Parameter class has no public properties.
            if (propertyList.Count == 0)
                return new List<T>();

            var columnNames = dataTable.Columns.Cast<DataColumn>().Select(column => column.ColumnName).ToList();
            var result = new List<T>();

            try
            {
                foreach (DataRow row in dataTable.Rows)
                {
                    var classObject = new T();

                    foreach (var property in propertyList)
                    {
                        if (!IsValidObjectData(property, columnNames, row))
                            continue;



                        var propertyValue = ChangeType(
                                row[property.Name],
                                property.PropertyType
                            );

                        property.SetValue(classObject, propertyValue, null);
                    }

                    result.Add(classObject);
                }
                return result;
            }
            catch (Exception ex)
            {
                return new List<T>();
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="value"></param>
        /// <param name="conversion"></param>
        /// <returns></returns>
        public static object ChangeType(object value, Type conversion)
        {
            var type = conversion;

            if (!type.IsGenericType || !(type.GetGenericTypeDefinition() == typeof(Nullable<>)))
                return Convert.ChangeType(value, type);

            if (value == null)
            {
                return null;
            }

            type = Nullable.GetUnderlyingType(type);
            return Convert.ChangeType(value, type);
        }

        /// <summary>
        /// The serialize to json.
        /// </summary>
        /// <param name="list">
        /// The list.
        /// </param>
        /// <typeparam name="T">
        /// </typeparam>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public static string SerializeToJson<T>(List<T> list)
        {
            return JsonConvert.SerializeObject(list, Formatting.None, new IsoDateTimeConverter { DateTimeFormat = "yyyy-MM-ddTHH:mm:ss.fff" });
        }

        /// <summary>
        ///
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="data"></param>
        /// <returns></returns>
        public static string SerializeToJson<T>(T data)
        {
            return JsonConvert.SerializeObject(data, Formatting.None, new IsoDateTimeConverter { DateTimeFormat = "yyyy-MM-ddTHH:mm:ss.fff" });
            //return JsonConvert.SerializeObject(data);
        }

        /// <summary>
        ///
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="data"></param>
        /// <returns></returns>
        public static List<T> DeserializeListFromJson<T>(string data)
        {
            return JsonConvert.DeserializeObject<List<T>>(data);
        }

        /// <summary>
        ///
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="data"></param>
        /// <returns></returns>
        public static T DeserializeFromJson<T>(string data)
        {
            return JsonConvert.DeserializeObject<T>(data);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="columnName"></param>
        /// <returns></returns>
        public static Dictionary<int, int> ExcelColumnToNumber(string columnName)
        {
            var columnNumber = string.Join("", columnName.ToCharArray().Where(Char.IsDigit));
            var name = columnName.Replace(columnNumber, "");
            var number = 0;
            var pow = 1;

            for (var i = name.Length - 1; i >= 0; i--)
            {
                number += (name[i] - 'A' + 1) * pow;
                pow *= 26;
            }

            var result = new Dictionary<int, int> { { number, ToInt(columnNumber) } };
            return result;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static bool IsCardId(string value)
        {
            if (!IsNumeric(value))
                return false;

            if (value.Length != 13)
                return false;

            int i;
            float sum;

            for (i = 0, sum = 0; i < 12; i++)
                sum += ToFlt(value[i]) * (13 - i);

            return Math.Abs(((11 - sum % 11) % 10) - ToFlt(value[12])) < 1F;
        }

        /// <summary>
        /// Build range with size and return to list of generic list T
        /// ex. [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18] and size is 5
        /// return {[1, 2, 3, 4, 5], [6, 7, 8, 9, 10], [11, 12, 13, 14, 15], [16, 17, 18]}
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sources"></param>
        /// <param name="size"></param>
        /// <returns></returns>
        public static List<List<T>> BuildRangeWithSize<T>(this IEnumerable<T> sources, int size)
        {
            var results = sources.Select((request, index) => new { request, index })
             .GroupBy(g => g.index / size, t => t.request)
             .Select(g => g.ToList());

            return results.ToList();
        }
    }
}