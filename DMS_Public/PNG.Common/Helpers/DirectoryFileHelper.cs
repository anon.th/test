﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DirectoryFileHelper.cs" company="Aware Technology Solutions Corporation Ltd">
//   Copyright (c) 2016-2017.  All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace PNG.Common.Helpers
{
    using Enums;
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Text.RegularExpressions;
    using System.Web.Hosting;
    using System.Web.Hosting;

    /// <summary>
    ///
    /// </summary>
    public static class DirectoryFileHelper
    {
        private const string _fileSeparatorChar = "_";

        /// <summary>
        /// Get fulll file name (name+path)
        /// </summary>
        /// <param name="rootDirectory"></param>
        /// <param name="fileName"></param>
        /// <param name="subDirectory"></param>
        /// <returns></returns>
        public static string GetFullFileName(string rootDirectory, string fileName, string subDirectory = null)
        {
            try
            {
                var defaultDirectoryName = !string.IsNullOrEmpty(subDirectory) ? (Path.DirectorySeparatorChar + subDirectory) : string.Empty;

                // TODO: ExportReport should send with parameter
                var defaultRootDirectory = GetFullPath(rootDirectory);

                // set full directory name
                var defaultFullDirectory = defaultRootDirectory + defaultDirectoryName;

                // set full directory name + full file name
                var fullFileName = defaultFullDirectory + Path.DirectorySeparatorChar + fileName;

                return fullFileName;
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
          

            
        }

        public static string GetFullPath(string path)
        {
            try
            {
                return HostingEnvironment.MapPath(path);
            }
            catch (Exception ex)
            {
                return path;
            }
           
        }

        public static string JoinWithUnderscore(string[] values)
        {
            return string.Join(_fileSeparatorChar, values);
        }

        public static string ReplaceSpecialCharacter(string value)
        {
            return Regex.Replace(value, @"[\/:*?<>#@|!?$\\//""]", "_");
        }

        /// <summary>
        /// Combine multiple text file
        /// </summary>
        /// <param name="fileNames"></param>
        /// <param name="destFileName"></param>
        /// <returns></returns>
        public static bool CombineMultipleTextFile(List<string> fileNames, string destFileName)
        {
            var result = false;
            using (var output = File.Create(destFileName))
            {
                foreach (var file in fileNames)
                {
                    if (File.Exists(file))
                    {
                        using (var input = File.OpenRead(file))
                        {
                            input.CopyTo(output);
                        }
                    }
                    else
                    {
                        throw new FileNotFoundException(file);
                    }
                }
                result = true;
            }
            return result;
        }
    }
}