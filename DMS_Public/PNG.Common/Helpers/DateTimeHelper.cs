﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DateTimeHelper.cs" company="Aware Technology Solutions Corporation Ltd">
//   Copyright (c) 2016-2017.  All rights reserved.
// </copyright>
// <summary>
//   Defines the DateTimeHelper type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Globalization;

namespace PNG.Common.Helpers
{
    /// <summary>
    ///
    /// </summary>
    public static class DateTimeHelper
    {
        /// <summary>
        ///
        /// </summary>
        public static string DateParameterFormat { get { return "{0:MM/dd/yyyy}"; } }
        
        /// <summary>
        ///
        /// </summary>
        public static string DateStringStore { get { return "yyyyMMddHHmm"; } }
        
        /// <summary>
        ///
        /// </summary>
        public static string DateStringFormat { get { return "MM/dd/yyyy"; } }

        /// <summary>
        ///
        /// </summary>
        public static string DateReportStringFormat { get { return "dd/MM/yyyy"; } }

        /// <summary>
        ///
        /// </summary>
        public static string DateParameterFormats { get { return "{0:yyyy-MM-dd}"; } }

        /// <summary>
        ///
        /// </summary>
        public static string DateTimeStringFormat { get { return "MM/dd/yyyy hh:mm:ss tt"; } }

        /// <summary>
        ///
        /// </summary>
        public static string DateTimeStringFormatTh { get { return "dd/MM/yyyy hh:mm:ss tt"; } }

        /// <summary>
        ///
        /// </summary>
        public static string DateReportStringFormatTh { get { return "dd MMMM yyyy"; } }

        /// <summary>
        /// 
        /// </summary>
        public static string DateConvertFormatTh { get { return "dd/MM/yyyy:th-TH"; } }

        /// <summary>
        /// 
        /// </summary>
        public static string DateReportFormatMonthYear { get { return "MM/yyyy"; } }

        /// <summary>
        ///
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string ConvertDateParameterToString(object value)
        {
            return Convert.ToDateTime(value).ToString(DateStringFormat, ConfigurationHelper.DefaultCultureInfo);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="value"></param>
        /// <param name="cultureInfo"></param>
        /// <returns></returns>
        public static string ConvertDateParameterToString(object value, CultureInfo cultureInfo)
        {
            return Convert.ToDateTime(value).ToString(DateReportStringFormat, cultureInfo);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string ConvertDateTimeToString(object value)
        {
            return Convert.ToDateTime(value).ToString(DateTimeStringFormat, ConfigurationHelper.DefaultCultureInfo);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string ConvertStoreStringDataTime(string value)
        {
            try
            {
                var date = DateTime.ParseExact(value, DateStringStore, ConfigurationHelper.DefaultCultureInfo);

                string suffix;

                if (date.Day%10 == 1)
                {
                    suffix = "st";
                }
                else if (date.Day%10 == 2)
                {
                    suffix = "nd";
                }
                else if (date.Day%10 == 3)
                {
                    suffix = "rd";
                }
                else
                {
                    suffix = "th";
                }

                return date.ToString("dddd d") + suffix + " of " + date.ToString("MMMM yyyy hh:mm tt");
            }
            catch
            {
                return "";
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="value"></param>
        /// <param name="cultureInfo"></param>
        /// <returns></returns>
        public static string ConvertDateTimeToString(object value, CultureInfo cultureInfo)
        {
            return Convert.ToDateTime(value).ToString(DateTimeStringFormat, cultureInfo);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        // ReSharper disable once InconsistentNaming
        public static string ConvertDateTimeToStringTH(object value)
        {
            return Convert.ToDateTime(value).ToString(DateTimeStringFormatTh, ConfigurationHelper.ReportCultureInfo);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="value"></param>
        /// <param name="cultureInfo"></param>
        /// <returns></returns>
        // ReSharper disable once InconsistentNaming
        public static string ConvertDateTimeToStringTH(object value, CultureInfo cultureInfo)
        {
            return Convert.ToDateTime(value).ToString(DateTimeStringFormatTh, cultureInfo);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <param name="formatDateTime"></param>
        /// <returns></returns>
        public static string ConvertDateTimeToStringWithFormat(object value, string formatDateTime)
        {
            return Convert.ToDateTime(value).ToString(formatDateTime);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        // ReSharper disable once InconsistentNaming
        public static string GetFormatDateTH(DateTime value)
        {
            return value.ToString(DateReportStringFormat, ConfigurationHelper.ReportCultureInfo);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        // ReSharper disable once InconsistentNaming
        public static string GetMonthNameTH(DateTime value)
        {
            return value.ToString("MMMM", ConfigurationHelper.ReportCultureInfo);
        }

        ///  <summary>
        /// 
        ///  </summary>
        ///  <param name="value"></param>
        /// <param name="cultureInfo"></param>
        /// <returns></returns>
        // ReSharper disable once InconsistentNaming
        public static string GetReportFormatDateTH(object value, CultureInfo cultureInfo)
        {
            return Convert.ToDateTime(value).ToString(DateReportStringFormatTh, cultureInfo);
        }

        ///  <summary>
        /// 
        ///  </summary>
        ///  <param name="value"></param>
        /// <param name="cultureInfo"></param>
        /// <returns></returns>
        // ReSharper disable once InconsistentNaming
        public static string GetReportFormatDateMonthYear(object value, CultureInfo cultureInfo)
        {
            return Convert.ToDateTime(value).ToString(DateReportFormatMonthYear, cultureInfo);
        }
    }

    /// <summary>
    ///
    /// </summary>
    public struct DateTimeSpan
    {
        private readonly int _years;
        private readonly int _months;
        private readonly int _days;
        private readonly int _hours;
        private readonly int _minutes;
        private readonly int _seconds;
        private readonly int _milliseconds;

        /// <summary>
        ///
        /// </summary>
        /// <param name="years"></param>
        /// <param name="months"></param>
        /// <param name="days"></param>
        /// <param name="hours"></param>
        /// <param name="minutes"></param>
        /// <param name="seconds"></param>
        /// <param name="milliseconds"></param>
        public DateTimeSpan(int years, int months, int days, int hours, int minutes, int seconds, int milliseconds)
        {
            this._years = years;
            this._months = months;
            this._days = days;
            this._hours = hours;
            this._minutes = minutes;
            this._seconds = seconds;
            this._milliseconds = milliseconds;
        }

        /// <summary>
        ///
        /// </summary>
        public int Years { get { return _years; } }

        /// <summary>
        ///
        /// </summary>
        public int Months { get { return _months; } }

        /// <summary>
        ///
        /// </summary>
        public int Days { get { return _days; } }

        /// <summary>
        ///
        /// </summary>
        public int Hours { get { return _hours; } }

        /// <summary>
        ///
        /// </summary>
        public int Minutes { get { return _minutes; } }

        /// <summary>
        ///
        /// </summary>
        public int Seconds { get { return _seconds; } }

        /// <summary>
        ///
        /// </summary>
        public int Milliseconds { get { return _milliseconds; } }

        /// <summary>
        ///
        /// </summary>
        private enum Phase
        {
            Years,
            Months,
            Days,
            Done
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="date1"></param>
        /// <param name="date2"></param>
        /// <returns></returns>
        public static DateTimeSpan CompareDates(DateTime date1, DateTime date2)
        {
            if (date2 < date1)
            {
                var sub = date1;
                date1 = date2;
                date2 = sub;
            }

            var current = date1;
            var years = 0;
            var months = 0;
            var days = 0;
            var phase = Phase.Years;
            var span = new DateTimeSpan();

            while (phase != Phase.Done)
            {
                switch (phase)
                {
                    case Phase.Years:
                        if (current.AddYears(years + 1) > date2)
                        {
                            phase = Phase.Months;
                            current = current.AddYears(years);
                        }
                        else
                        {
                            years++;
                        }
                        break;

                    case Phase.Months:
                        if (current.AddMonths(months + 1) > date2)
                        {
                            phase = Phase.Days;
                            current = current.AddMonths(months);
                        }
                        else
                        {
                            months++;
                        }
                        break;

                    case Phase.Days:
                        if (current.AddDays(days + 1) > date2)
                        {
                            current = current.AddDays(days);
                            var timespan = date2 - current;
                            span = new DateTimeSpan(years, months, days, timespan.Hours, timespan.Minutes, timespan.Seconds, timespan.Milliseconds);
                            phase = Phase.Done;
                        }
                        else
                        {
                            days++;
                        }
                        break;
                }
            }

            return span;
        }
    }
}