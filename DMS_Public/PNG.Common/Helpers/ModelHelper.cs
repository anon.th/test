﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ModelHelper.cs" company="Aware Technology Solutions Corporation Ltd">
//   Copyright (c) 2016-2017.  All rights reserved.
// </copyright>
// <summary>
//   Defines the ModelHelper type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

//using PNG.Dapper.Extensions.Utils;
using PNG.Dapper.Extensions.Utils;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Xml;

namespace PNG.Common.Helpers
{
    public static class ModelHelper
    {
        /// <summary>
        /// The ignored properties.
        /// </summary>
        private static readonly ConcurrentDictionary<RuntimeTypeHandle, IEnumerable<PropertyInfo>> IgnoredProperties =
            new ConcurrentDictionary<RuntimeTypeHandle, IEnumerable<PropertyInfo>>();

        /// <summary>
        /// 
        /// </summary>
        private static readonly ConcurrentDictionary<RuntimeTypeHandle, IEnumerable<PropertyInfo>> TypeProperties =
            new ConcurrentDictionary<RuntimeTypeHandle, IEnumerable<PropertyInfo>>();

        /// <summary>
        /// The auto increment properties.
        /// </summary>
        private static readonly ConcurrentDictionary<RuntimeTypeHandle, IEnumerable<PropertyInfo>> AutoIncrementProperties =
            new ConcurrentDictionary<RuntimeTypeHandle, IEnumerable<PropertyInfo>>();

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static IEnumerable<PropertyInfo> IgnoredPropertiesCache<T>()
        {
            var type = typeof(T);
            IEnumerable<PropertyInfo> propertyInfos;

            if (IgnoredProperties.TryGetValue(type.TypeHandle, out propertyInfos))
                return propertyInfos;

            var ignoredProperties = type.GetProperties().Where(p => p.GetCustomAttributes(true).Any(a => a is IgnoredAttribute)).ToList();

            IgnoredProperties[type.TypeHandle] = ignoredProperties;
            return ignoredProperties;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static IEnumerable<PropertyInfo> AutoIncrementPropertiesCache<T>()
        {
            var type = typeof(T);

            IEnumerable<PropertyInfo> propertyInfos;
            if (AutoIncrementProperties.TryGetValue(type.TypeHandle, out propertyInfos))
                return propertyInfos;

            var autoIncrementProperties = type.GetProperties().Where(p => p.GetCustomAttributes(true).Any(a => a is AutoIncrementAttribute)).ToList();

            AutoIncrementProperties[type.TypeHandle] = autoIncrementProperties;
            return autoIncrementProperties;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static IEnumerable<PropertyInfo> TypePropertiesCache<T>()
        {
            var type = typeof(T);

            IEnumerable<PropertyInfo> propertyInfos;
            if (TypeProperties.TryGetValue(type.TypeHandle, out propertyInfos))
                return propertyInfos;

            var properties = type.GetProperties().Where(IsWritable).ToArray();

            TypeProperties[type.TypeHandle] = properties;
            return properties;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="propertyInfo"></param>
        /// <returns></returns>
        public static bool IsWritable(PropertyInfo propertyInfo)
        {
            var attributes = propertyInfo.GetCustomAttributes(typeof(WriteAttribute), false);

            if (attributes.Length != 1) return true;

            var write = (WriteAttribute)attributes[0];
            return write.Write;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static object GetPropertyValue(object value)
        {
            try
            {
                return value ?? DBNull.Value;
            }
            catch
            {
                return DBNull.Value;
            }
        }

        /// <summary>
        /// Perform a deep Copy of an object that is marked with '[Serializable]' or '[DataContract]'
        /// </summary>
        /// <typeparam name="T">The type of object being copied.</typeparam>
        /// <param name="source">The object instance to copy.</param>
        /// <returns>The copied object.</returns>
        public static T Clone<T>(T source)
        {
            //if (typeof(T).IsSerializable == true)
            //{
            //    return CloneUsingSerializable<T>(source);
            //}

            //if (IsDataContract(typeof(T)) == true)
            //{
            //    return CloneUsingDataContracts<T>(source);
            //}

            //throw new ArgumentException("The type must be Serializable or use DataContracts.", "source");
            var serialized = DataConvertHelper.SerializeToJson(source);
            return DataConvertHelper.DeserializeFromJson<T>(serialized);
        }

        /// <summary>
        /// Perform a deep Copy of an object that is marked with '[Serializable]'
        /// </summary>
        /// <remarks>
        /// Found on http://stackoverflow.com/questions/78536/cloning-objects-in-c-sharp
        /// Uses code found on CodeProject, which allows free use in third party apps
        /// - http://www.codeproject.com/KB/tips/SerializedObjectCloner.aspx
        /// </remarks>
        /// <typeparam name="T">The type of object being copied.</typeparam>
        /// <param name="source">The object instance to copy.</param>
        /// <returns>The copied object.</returns>
        public static T CloneUsingSerializable<T>(T source)
        {
            if (!typeof(T).IsSerializable)
            {
                throw new ArgumentException("The type must be serializable.", "source");
            }

            // Don't serialize a null object, simply return the default for that object
            if (ReferenceEquals(source, null))
            {
                return default(T);
            }

            IFormatter formatter = new BinaryFormatter();
            Stream stream = new MemoryStream();
            using (stream)
            {
                formatter.Serialize(stream, source);
                stream.Seek(0, SeekOrigin.Begin);
                return (T)formatter.Deserialize(stream);
            }
        }

        /// <summary>
        /// Perform a deep Copy of an object that is marked with '[DataContract]'
        /// </summary>
        /// <typeparam name="T">The type of object being copied.</typeparam>
        /// <param name="source">The object instance to copy.</param>
        /// <returns>The copied object.</returns>
        //public static T CloneUsingDataContracts<T>(T source)
        //{
        //    if (IsDataContract(typeof(T)) == false)
        //    {
        //        throw new ArgumentException("The type must be a data contract.", "source");
        //    }

        //    // ** Don't serialize a null object, simply return the default for that object
        //    if (ReferenceEquals(source, null))
        //    {
        //        return default(T);
        //    }

        //    var dcs = new DataContractSerializer(typeof(T));

        //    using (Stream stream = new MemoryStream())
        //    {
        //        using (var writer = XmlDictionaryWriter.CreateBinaryWriter(stream))
        //        {
        //            dcs.WriteObject(writer, source);
        //            writer.Flush();
        //            stream.Seek(0, SeekOrigin.Begin);

        //            using (var reader = XmlDictionaryReader.CreateBinaryReader(stream, XmlDictionaryReaderQuotas.Max))
        //            {
        //                return (T)dcs.ReadObject(reader);
        //            }
        //        }
        //    }
        //}

        /// <summary>
        /// Helper function to check if a class is a [DataContract]
        /// </summary>
        /// <param name="type">The type of the object to check.</param>
        /// <returns>Boolean flag indicating if the class is a DataContract (true) or not (false) </returns>
        public static bool IsDataContract(Type type)
        {
            var attributes = type.GetCustomAttributes(typeof(DataContractAttribute), false);
            return attributes.Length == 1;
        }
    }
}
