﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ConvertHelper.cs" company="Aware Technology Solutions Corporation Ltd">
//   Copyright (c) 2016-2017.  All rights reserved.
// </copyright>
// <summary>
//   Defines the ConvertHelper type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Globalization;
using System.Text.RegularExpressions;

namespace PNG.Common.Helpers
{
    /// <summary>
    ///
    /// </summary>
    public class ConvertHelper
    {
        /// <summary>
        ///
        /// </summary>
        /// <param name="format"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string ExportFieldDateFormat(string format, string value)
        {
            var result = value;
            if (!string.IsNullOrEmpty(value) && value.Trim() != "")
            {
                var dateValue = Convert.ToDateTime(value);
                var dateStringArray = format.Split(':');
                var cultureInfo = new CultureInfo(dateStringArray[1]);
                result = dateValue.ToString(dateStringArray[0], cultureInfo.DateTimeFormat);
            }
            return result;
        }

        /// <summary>
        /// Export with string format
        /// Example
        /// string.Format("{0:0,0.0000;(0,0.0000);zero}", -22334455.43456) // (22,334,455.4345)
        /// string.Format("{0:0,0.0000;(0,0.0000);zero}", 22334455.43456)  // 22,334,455.4345
        /// </summary>
        /// <param name="format"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string ExportFieldStringFormat(string format, object value)
        {
            string result;

            try
            {
                result = string.Format(format, value);
            }
            // ReSharper disable once RedundantCatchClause
            catch
            {
                throw;
            }

            return result;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="maxlength"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string ExportFieldWithMaxLength(short? maxlength, string value)
        {
            if (maxlength == null)
                return value;

            return value.ToString(CultureInfo.InvariantCulture).Length > maxlength ? value.Substring(0, (int)maxlength) : value;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="startPosition"></param>
        /// <param name="endPosition"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string ExportFieldWithPosition(int? startPosition, int? endPosition, string value)
        {
            if (startPosition != null && endPosition != null)
                return value.Substring((int)startPosition, (int)endPosition - (int)startPosition);

            if (startPosition != null)
                return value.Substring((int)startPosition);

            return endPosition != null ? value.Substring(0, (int)endPosition) : value;
        }

        /// <summary>
        /// convert column special word to display.
        /// </summary>
        /// <param name="column"></param>
        /// <returns></returns>
        public static string SpecialWord(string column)
        {
            var regex1 = new Regex("&");
            var regex2 = new Regex("<");
            var regex3 = new Regex(">");
            var result = string.Empty;

            try
            {
                if (column != null)
                {
                    result = regex1.Replace(column, "&amp;");
                    result = regex2.Replace(result, "&lt;");
                    result = regex3.Replace(result, "&gt;");
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return result;
        }
    }
}