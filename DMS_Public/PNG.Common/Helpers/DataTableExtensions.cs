﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DataTableExtensions.cs" company="Aware Technology Solutions Corporation Ltd">
//   Copyright (c) 2016-2017.  All rights reserved.
// </copyright>
// <summary>
//   Defines the DataTableExtensions type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;

namespace PNG.Common.Helpers
{
    /// <summary>
    /// 
    /// </summary>
    public static class DataTableExtensions
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="dataTable"></param>
        /// <param name="columnNames"></param>
        /// <returns></returns>
        public static DataTable SetColumnsOrder(this DataTable dataTable, params string[] columnNames)
        {
            try
            {
                var view = new DataView(dataTable);
                var table = view.ToTable(dataTable.TableName, false, columnNames);

                return table;
            }
            catch (Exception ex)
            {
                return dataTable;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="dataTable"></param>
        /// <param name="conversion"></param>
        public static void ThDateConverter<T>(this DataTable dataTable, Func<object, T> conversion)
        {
            var culture = System.Threading.Thread.CurrentThread.CurrentCulture;

            // TODO: set "th-TH" culture to current thread to convert date to Thai
            System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("th-TH");

            // TODO: declare pattern for matching to column name and date format 
            const string indexOf = "Date";
            const string pattern = @"^((([0-9])|([0-2][0-9])|([3][0-1]))\s+(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)\s+\d{4})|(T\+[0-9]+)$";

            try
            {
                if ((dataTable == null) || (dataTable.Rows.Count <= 0))
                    return;

                foreach (DataRow row in dataTable.Rows)
                {
                    if (row == null)
                        continue;

                    // TODO: copy object to local space
                    var tmp = row;

                    if ((dataTable.Columns.Count <= 0))
                        continue;

                    // TODO: compare pattern for matching to column name and date format 
                    var list = dataTable.Columns.Cast<DataColumn>()
                        .Where(column => (column.ColumnName.IndexOf(indexOf, StringComparison.OrdinalIgnoreCase) > -1)
                                         && tmp[column] != null
                                         && tmp[column] != DBNull.Value
                                         && !string.IsNullOrEmpty(tmp[column].ToString())
                                         && Regex.IsMatch(tmp[column].ToString(), pattern, RegexOptions.IgnoreCase));

                    var dataColumns = list as DataColumn[] ?? list.ToArray();

                    if (!dataColumns.Any())
                        continue;

                    foreach (var column in dataColumns)
                    {
                        try
                        {
                            row[column] = conversion(row[column]);
                        }
                        // ReSharper disable once EmptyGeneralCatchClause
                        catch (Exception ex)
                        { }
                    }
                }
            }
            // ReSharper disable once RedundantCatchClause
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                // TODO: set default current culture back
                System.Threading.Thread.CurrentThread.CurrentCulture = culture;
            }
        }
    }
}