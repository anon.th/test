﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="NetworkSecurity.cs" company="Aware Technology Solutions Corporation Ltd">
//   Copyright (c) 2016-2017.  All rights reserved.
// </copyright>
// <summary>
//   Defines the NetworkSecurity type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Runtime.InteropServices;
using System.Security.Principal;

namespace PNG.Common.Helpers
{
    /// <summary>
    /// 
    /// </summary>
    public enum LogonType
    {
        Logon32LogonInteractive = 2,
        Logon32LogonNetwork = 3,
        Logon32LogonBatch = 4,
        Logon32LogonService = 5,
        Logon32LogonUnlock = 7,
        Logon32LogonNetworkCleartext = 8,    // Only for Win2K or higher
        Logon32LogonNewCredentials = 9       // Only for Win2K or higher
    };

    /// <summary>
    /// 
    /// </summary>
    public enum LogonProvider
    {
        Logon32ProviderDefault = 0,
        Logon32ProviderWinnt35 = 1,
        Logon32ProviderWinnt40 = 2,
        Logon32ProviderWinnt50 = 3
    };

    /// <summary>
    /// 
    /// </summary>
    static class SecurityUtil32
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="lpszUsername"></param>
        /// <param name="lpszDomain"></param>
        /// <param name="lpszPassword"></param>
        /// <param name="dwLogonType"></param>
        /// <param name="dwLogonProvider"></param>
        /// <param name="tokenHandle"></param>
        /// <returns></returns>
        [DllImport("advapi32.dll", SetLastError = true)]
        public static extern bool LogonUser(String lpszUsername, String lpszDomain, String lpszPassword,
            int dwLogonType, int dwLogonProvider, ref IntPtr tokenHandle);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="handle"></param>
        /// <returns></returns>
        [DllImport("kernel32.dll", CharSet = CharSet.Auto)]
        public extern static bool CloseHandle(IntPtr handle);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="existingTokenHandle"></param>
        /// <param name="securityImpersonationLevel"></param>
        /// <param name="duplicateTokenHandle"></param>
        /// <returns></returns>
        [DllImport("advapi32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        public extern static bool DuplicateToken(IntPtr existingTokenHandle,
            int securityImpersonationLevel, ref IntPtr duplicateTokenHandle);
    }

    /// <summary>
    /// 
    /// </summary>
    public class NetworkSecurity : IDisposable
    {
        private bool _disposed;
        public const int Logon32LogonInteractive = 2;
        public const int Logon32ProviderDefault = 0;
        private WindowsImpersonationContext _impersonationContext;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="domain"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public bool ImpersonateUser(String userName, String domain, String password)
        {
            var result = false;
            var tokenHandle = IntPtr.Zero;
            var tokenDuplicate = IntPtr.Zero;

            try
            {
                if (SecurityUtil32.LogonUser(userName,
                                        domain,
                                        password,
                                        Logon32LogonInteractive,
                                        Logon32ProviderDefault,
                                        ref tokenHandle))
                {
                    if (SecurityUtil32.DuplicateToken(tokenHandle, 2, ref tokenDuplicate))
                    {
                        var tempWindowsIdentity = new WindowsIdentity(tokenDuplicate);
                        _impersonationContext = tempWindowsIdentity.Impersonate();

                        result = (_impersonationContext != null);
                    }
                    else
                    {
                        LastError = (UInt32)Marshal.GetLastWin32Error();
                    }
                }
                else
                {
                    LastError = (UInt32)Marshal.GetLastWin32Error();
                }
            }
            finally
            {
                if (tokenHandle != IntPtr.Zero)
                {
                    SecurityUtil32.CloseHandle(tokenHandle);
                }
                if (tokenDuplicate != IntPtr.Zero)
                {
                    SecurityUtil32.CloseHandle(tokenDuplicate);
                }

            }

            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        public void UndoImpersonation()
        {
            _impersonationContext.Undo();
        }

        /// <summary>
        /// 
        /// </summary>
        public uint LastError { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="disposing"></param>
        protected virtual void Dispose(bool disposing)
        {
            if (_disposed) return;

            if (disposing)
            {
                if (null != _impersonationContext)
                {
                    UndoImpersonation();
                }
            }

            _disposed = true;
        }
    }
}