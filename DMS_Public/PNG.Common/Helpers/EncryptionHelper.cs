﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="EncryptionHelper.cs" company="Aware Technology Solutions Corporation Ltd">
//   Copyright (c) 2016-2017.  All rights reserved.
// </copyright>
// <summary>
//   Defines the EncryptionHelper type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace PNG.Common.Helpers
{
    /// <summary>
    /// 
    /// </summary>
    public class EncryptionHelper
    {
        /// <summary>
        /// This constant string is used as a "salt" value for the PasswordDeriveBytes function calls.
        /// This size of the IV (in bytes) must = (keysize / 8).  Default keysize is 256, so the IV must be
        /// 32 bytes long.  Using a 16 character string here gives us 32 bytes when converted to a byte array.
        /// </summary>
        private const string InitVector = "tu89geji340t89u2";

        /// <summary>
        /// This constant is used to determine the keysize of the encryption algorithm.
        /// </summary>
        private const int Keysize = 256;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="plainText"></param>
        /// <param name="passPhrase"></param>
        /// <returns></returns>
        public static string Encrypt(string plainText, string passPhrase)
        {
            if (plainText == null)
                return null;

            var initVectorBytes = Encoding.UTF8.GetBytes(InitVector);
            var plainTextBytes = Encoding.UTF8.GetBytes(plainText);
            var password = new PasswordDeriveBytes(passPhrase, null);
#pragma warning disable 618
            var keyBytes = password.GetBytes(Keysize / 8);
#pragma warning restore 618
            var symmetricKey = new RijndaelManaged { Mode = CipherMode.CBC };
            var encryptor = symmetricKey.CreateEncryptor(keyBytes, initVectorBytes);

            CryptoStream cryptoStream = null;
            byte[] cipherTextBytes;

            try
            {
                var memoryStream = new MemoryStream();
                cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write);

                cryptoStream.Write(plainTextBytes, 0, plainTextBytes.Length);
                cryptoStream.FlushFinalBlock();

                cipherTextBytes = memoryStream.ToArray();
            }
            finally
            {
                if (cryptoStream != null)
                    cryptoStream.Dispose();
            }

            return Convert.ToBase64String(cipherTextBytes);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="cipherText"></param>
        /// <param name="passPhrase"></param>
        /// <returns></returns>
        public static string Decrypt(string cipherText, string passPhrase)
        {
            if (cipherText == null)
                return null;

            var initVectorBytes = Encoding.ASCII.GetBytes(InitVector);
            var cipherTextBytes = Convert.FromBase64String(cipherText);
            var password = new PasswordDeriveBytes(passPhrase, null);
#pragma warning disable 618
            var keyBytes = password.GetBytes(Keysize / 8);
#pragma warning restore 618
            var symmetricKey = new RijndaelManaged { Mode = CipherMode.CBC };
            var decryptor = symmetricKey.CreateDecryptor(keyBytes, initVectorBytes);
            var memoryStream = new MemoryStream(cipherTextBytes);
            var cryptoStream = new CryptoStream(memoryStream, decryptor, CryptoStreamMode.Read);
            var plainTextBytes = new byte[cipherTextBytes.Length];
            var decryptedByteCount = cryptoStream.Read(plainTextBytes, 0, plainTextBytes.Length);

            cryptoStream.Close();

            return Encoding.UTF8.GetString(plainTextBytes, 0, decryptedByteCount);
        }

        /// <summary>
        /// QueryString encryption
        /// </summary>
        /// <param name="inputText"></param>
        /// <param name="key"></param>
        /// <param name="salt"></param>
        /// <returns></returns>
//        public static string EncryptQueryString(string inputText, string key, string salt)
//        {
//            try
//            {
//                var plainText = Encoding.UTF8.GetBytes(inputText);

//                using (var rijndaelCipher = new RijndaelManaged())
//                {
//                    var secretKey = new PasswordDeriveBytes(Encoding.ASCII.GetBytes(key), Encoding.ASCII.GetBytes(salt));
//#pragma warning disable 618
//                    using (var encryptor = rijndaelCipher.CreateEncryptor(secretKey.GetBytes(32), secretKey.GetBytes(16)))
//#pragma warning restore 618
//                    {
//                        using (var memoryStream = new MemoryStream())
//                        {
//                            using (var cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write))
//                            {
//                                cryptoStream.Write(plainText, 0, plainText.Length);
//                                cryptoStream.FlushFinalBlock();

//                                // Generate a string that won't get screwed up when passed as a query string.
//                                return HttpServerUtility.UrlTokenEncode(memoryStream.ToArray());
//                            }
//                        }
//                    }
//                }
//            }
//            catch
//            {
//                return inputText;
//            }
//        }

        /// <summary>
        /// QueryString decryption
        /// </summary>
        /// <param name="inputText"></param>
        /// <param name="key"></param>
        /// <param name="salt"></param>
        /// <returns></returns>
//        public static string DecryptQueryString(string inputText, string key, string salt)
//        {
//            try
//            {
//                var encryptedData = HttpServerUtility.UrlTokenDecode(inputText);

//                if (encryptedData == null)
//                    return string.Empty;

//                var secretKey = new PasswordDeriveBytes(Encoding.ASCII.GetBytes(key), Encoding.ASCII.GetBytes(salt));

//                using (var rijndaelCipher = new RijndaelManaged())
//                {
//#pragma warning disable 618
//                    using (var decryptor = rijndaelCipher.CreateDecryptor(secretKey.GetBytes(32), secretKey.GetBytes(16)))
//#pragma warning restore 618
//                    {
//                        using (var memoryStream = new MemoryStream(encryptedData))
//                        {
//                            using (var cryptoStream = new CryptoStream(memoryStream, decryptor, CryptoStreamMode.Read))
//                            {
//                                var plainText = new byte[encryptedData.Length];
//                                cryptoStream.Read(plainText, 0, plainText.Length);
//                                var utf8 = Encoding.UTF8.GetString(plainText);
//                                return utf8;
//                            }
//                        }
//                    }
//                }

//            }
//            catch
//            {
//                return inputText;
//            }
//        }

        /// <summary>
        /// Encrypts specified plaintext using Rijndael symmetric key algorithm and returns a
        /// base64-encoded result.
        /// </summary>
        /// <param name="plainText">Plaintext value to be encrypted.</param>
        /// <param name="passPhrase">
        /// Passphrase from which a pseudo-random password will be derived. The derived password will
        /// be used to generate the encryption key. Passphrase can be any string. In this example we
        /// assume that this passphrase is an ASCII string.
        /// </param>
        /// <param name="saltValue">
        /// Salt value used along with passphrase to generate password. Salt can be any string. In
        /// this example we assume that salt is an ASCII string.
        /// </param>
        /// <param name="hashAlgorithm">
        /// Hash algorithm used to generate password. Allowed values are: "MD5" and "SHA1". SHA1
        /// hashes are a bit slower, but more secure than MD5 hashes.
        /// </param>
        /// <param name="passwordIterations">
        /// Number of iterations used to generate password. One or two iterations should be enough.
        /// </param>
        /// <param name="initVector">
        /// Initialization vector (or IV). This value is required to encrypt the first block of
        /// plaintext data. For RijndaelManaged class IV must be exactly 16 ASCII characters long.
        /// </param>
        /// <param name="keySize">
        /// Size of encryption key in bits. Allowed values are: 128, 192, and 256. Longer keys are
        /// more secure than shorter keys.
        /// </param>
        /// <returns>Encrypted value formatted as a base64-encoded string.</returns>
        public static string Encrypt(string plainText, string passPhrase, string saltValue, string hashAlgorithm, int passwordIterations, string initVector, int keySize)
        {
            // Convert strings into byte arrays.
            // Let us assume that strings only contain ASCII codes.
            // If strings include Unicode characters, use Unicode, UTF7, or UTF8 
            // encoding.
            var initVectorBytes = Encoding.ASCII.GetBytes(initVector);
            var saltValueBytes = Encoding.ASCII.GetBytes(saltValue);

            // Convert our plaintext into a byte array.
            // Let us assume that plaintext contains UTF8-encoded characters.
            var plainTextBytes = Encoding.UTF8.GetBytes(plainText);

            // First, we must create a password, from which the key will be derived.
            // This password will be generated from the specified passphrase and 
            // salt value. The password will be created using the specified hash 
            // algorithm. Password creation can be done in several iterations.
            var password = new PasswordDeriveBytes
            (
                passPhrase,
                saltValueBytes,
                hashAlgorithm,
                passwordIterations
            );

            // Use the password to generate pseudo-random bytes for the encryption
            // key. Specify the size of the key in bytes (instead of bits).
#pragma warning disable 618
            var keyBytes = password.GetBytes(keySize / 8);
#pragma warning restore 618

            // Create uninitialized Rijndael encryption object.
            var symmetricKey = new RijndaelManaged
            {
                Mode = CipherMode.CBC
            };

            // It is reasonable to set encryption mode to Cipher Block Chaining
            // (CBC). Use default options for other symmetric key parameters.

            // Generate encryptor from the existing key bytes and initialization 
            // vector. Key size will be defined based on the number of the key 
            // bytes.
            var encryptor = symmetricKey.CreateEncryptor
            (
                keyBytes,
                initVectorBytes
            );

            // Define memory stream which will be used to hold encrypted data.
            var memoryStream = new MemoryStream();

            // Define cryptographic stream (always use Write mode for encryption).
            var cryptoStream = new CryptoStream
            (
                memoryStream,
                encryptor,
                CryptoStreamMode.Write
            );

            // Start encrypting.
            cryptoStream.Write(plainTextBytes, 0, plainTextBytes.Length);

            // Finish encrypting.
            cryptoStream.FlushFinalBlock();

            // Convert our encrypted data from a memory stream into a byte array.
            var cipherTextBytes = memoryStream.ToArray();

            // Close both streams.
            memoryStream.Close();
            cryptoStream.Close();

            // Convert encrypted data into a base64-encoded string.
            var cipherText = Convert.ToBase64String(cipherTextBytes);

            // Return encrypted string.
            return cipherText;
        }

        /// <summary>
        /// Decrypts specified ciphertext using Rijndael symmetric key algorithm.
        /// </summary>
        /// <param name="cipherText">
        /// Base64-formatted ciphertext value.
        /// </param>
        /// <param name="passPhrase">
        /// Passphrase from which a pseudo-random password will be derived. The
        /// derived password will be used to generate the encryption key.
        /// Passphrase can be any string. In this example we assume that this
        /// passphrase is an ASCII string.
        /// </param>
        /// <param name="saltValue">
        /// Salt value used along with passphrase to generate password. Salt can
        /// be any string. In this example we assume that salt is an ASCII string.
        /// </param>
        /// <param name="hashAlgorithm">
        /// Hash algorithm used to generate password. Allowed values are: "MD5" and
        /// "SHA1". SHA1 hashes are a bit slower, but more secure than MD5 hashes.
        /// </param>
        /// <param name="passwordIterations">
        /// Number of iterations used to generate password. One or two iterations
        /// should be enough.
        /// </param>
        /// <param name="initVector">
        /// Initialization vector (or IV). This value is required to encrypt the
        /// first block of plaintext data. For RijndaelManaged class IV must be
        /// exactly 16 ASCII characters long.
        /// </param>
        /// <param name="keySize">
        /// Size of encryption key in bits. Allowed values are: 128, 192, and 256.
        /// Longer keys are more secure than shorter keys.
        /// </param>
        /// <returns>
        /// Decrypted string value.
        /// </returns>
        /// <remarks>
        /// Most of the logic in this function is similar to the Encrypt
        /// logic. In order for decryption to work, all parameters of this function
        /// - except cipherText value - must match the corresponding parameters of
        /// the Encrypt function which was called to generate the
        /// ciphertext.
        /// </remarks>
        public static string Decrypt(string cipherText, string passPhrase, string saltValue, string hashAlgorithm, int passwordIterations, string initVector, int keySize)
        {
            // Convert strings defining encryption key characteristics into byte
            // arrays. Let us assume that strings only contain ASCII codes.
            // If strings include Unicode characters, use Unicode, UTF7, or UTF8
            // encoding.
            var initVectorBytes = Encoding.ASCII.GetBytes(initVector);
            var saltValueBytes = Encoding.ASCII.GetBytes(saltValue);

            // Convert our ciphertext into a byte array.
            var cipherTextBytes = Convert.FromBase64String(cipherText);

            // First, we must create a password, from which the key will be 
            // derived. This password will be generated from the specified 
            // passphrase and salt value. The password will be created using
            // the specified hash algorithm. Password creation can be done in
            // several iterations.
            var password = new PasswordDeriveBytes
            (
                passPhrase,
                saltValueBytes,
                hashAlgorithm,
                passwordIterations
            );

            // Use the password to generate pseudo-random bytes for the encryption
            // key. Specify the size of the key in bytes (instead of bits).
#pragma warning disable 618
            var keyBytes = password.GetBytes(keySize / 8);
#pragma warning restore 618

            // Create uninitialized Rijndael encryption object.
            var symmetricKey = new RijndaelManaged
            {
                Mode = CipherMode.CBC
            };

            // It is reasonable to set encryption mode to Cipher Block Chaining
            // (CBC). Use default options for other symmetric key parameters.

            // Generate decryptor from the existing key bytes and initialization 
            // vector. Key size will be defined based on the number of the key 
            // bytes.
            var decryptor = symmetricKey.CreateDecryptor
            (
                keyBytes,
                initVectorBytes
            );

            // Define memory stream which will be used to hold encrypted data.
            var memoryStream = new MemoryStream(cipherTextBytes);

            // Define cryptographic stream (always use Read mode for encryption).
            var cryptoStream = new CryptoStream
            (
                memoryStream,
                decryptor,
                CryptoStreamMode.Read
            );

            // Since at this point we don't know what the size of decrypted data
            // will be, allocate the buffer long enough to hold ciphertext;
            // plaintext is never longer than ciphertext.
            var plainTextBytes = new byte[cipherTextBytes.Length];

            // Start decrypting.
            var decryptedByteCount = cryptoStream.Read
            (
                plainTextBytes,
                0,
                plainTextBytes.Length
            );

            // Close both streams.
            memoryStream.Close();
            cryptoStream.Close();

            // Convert decrypted data into a string. 
            // Let us assume that the original plaintext string was UTF8-encoded.
            var plainText = Encoding.UTF8.GetString
            (
                plainTextBytes,
                0,
                decryptedByteCount
            );

            // Return decrypted string.   
            return plainText;
        }
    }
}

/*
***
// original plain text
string plainText = "Hello, World!";

// can be any string
string passPhrase = "Pas5pr@se";

// can be any string
string saltValue = "s@1tValue";

// can be "MD5"
string hashAlgorithm = "SHA1";

// can be any number
int passwordIterations = 2;

// must be 16 bytes
string initVector = "@1B2c3D4e5F6g7H8";

// can be 192 or 128
int keySize = 256;
***
*/
