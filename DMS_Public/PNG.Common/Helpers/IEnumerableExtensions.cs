﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IEnumerableExtensions.cs" company="Aware Technology Solutions Corporation Ltd">
//   Copyright (c) 2016-2017.  All rights reserved.
// </copyright>
// <summary>
//   Defines the IEnumerableExtensions type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;

namespace PNG.Common.Helpers
{
    // ReSharper disable once InconsistentNaming
    public static class IEnumerableExtensions
    {
        /// <summary>
        /// Executes Action on each member of the IEnumerable. Returns IEnumerable, but can be safely cast back into original type
        /// </summary>
        /// <typeparam name="TSource"></typeparam>
        /// <param name="sources"></param>
        /// <param name="action"></param>
        /// <returns></returns>
        public static IEnumerable<TSource> ForEach<TSource>(this IEnumerable<TSource> sources, Action<TSource> action)
        {
            if (sources == null)
                throw new ArgumentNullException("sources");

            if (action == null)
                throw new ArgumentNullException("action");

            var source = sources as TSource[] ?? sources.ToArray();

            foreach (var item in source)
            {
                action(item);
            }

            return source;
        }
    }
}