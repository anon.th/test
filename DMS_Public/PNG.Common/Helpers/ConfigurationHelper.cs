﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ConfigurationHelper.cs" company="Aware Technology Solutions Corporation Ltd">
//   Copyright (c) 2016-2017.  All rights reserved.
// </copyright>
// <summary>
//   Defines the ConfigurationHelper type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using PNG.Common.Services.Logging;
using System;
using System.ComponentModel;
using System.Configuration;
using System.Globalization;

namespace PNG.Common.Helpers
{
    /// <summary>
    /// The configuration helper.
    /// </summary>
    public class ConfigurationHelper
    {
        /// <summary>
        /// The default culture info.
        /// </summary>
        public static readonly CultureInfo DefaultCultureInfo = new CultureInfo("en-US");
        public static readonly CultureInfo ReportCultureInfo = new CultureInfo("th-TH");
        public static readonly string NotificationUserName = "notification";

        /// <summary>
        /// The get connect string.
        /// </summary>
        /// <param name="name">
        /// The name.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public static string GetConnectString(string name)
        {
            string result;

            try
            {
                result = ConfigurationManager.ConnectionStrings[name].ConnectionString;
            }
            catch (Exception ex)
            {
                Log.Error(ex);

                // Try to get from AppSettings
                try
                {
                    result = GetAppSettingValue<string>(name);
                }
                catch
                {
                    result = string.Format("ConnectionString {0} cannot be found, {1} ", name, ex.Message);
                }
            }

            return result;
        }

        /// <summary>
        /// The get connect provider.
        /// </summary>
        /// <param name="name">
        /// The name.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public static string GetConnectProvider(string name)
        {
            string result;

            try
            {
                result = ConfigurationManager.ConnectionStrings[name].ProviderName;
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                result = string.Format("ConnectionProvider {0} cannot be found, {1}", name, ex.Message);
            }

            return result;
        }

        /// <summary>
        /// The get app setting.
        /// </summary>
        /// <param name="name">
        /// The name.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public static string GetAppSetting(string name)
        {
            string result;

            try
            {
                result = ConfigurationManager.AppSettings[name];
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                result = string.Format("AppSetting {0} cannot be found, {1}", name, ex.Message);
            }

            return result;
        }

        /// <summary>
        /// The get value.
        /// </summary>
        /// <param name="name">
        /// The name.
        /// </param>
        /// <typeparam name="T">
        /// </typeparam>
        /// <returns>
        /// The <see cref="T"/>.
        /// </returns>
        public static T GetAppSettingValue<T>(string name)
        {
            try
            {
                var appSettingValue = GetAppSetting(name);
                var converter = TypeDescriptor.GetConverter(typeof(T));

                if (!string.IsNullOrEmpty(appSettingValue))
                    return (T)converter.ConvertFromInvariantString(appSettingValue);

                return default(T);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return default(T);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static int GetScopeDatabaseTimeout()
        {
            int result;
            try
            {
                var value = GetAppSettingApp(ApplicationConstance.WEB_CFG_SCOPE_DB_TIMEOUT);
                return DataConvertHelper.ToInt(value);
            }
            catch (Exception)
            {
                result = 3;
            }
            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public static string GetAppSettingApp(string name)
        {
            string result;
            try
            {
                var cfgtype = ConfigurationManager.AppSettings[ApplicationConstance.WEB_CFG_CONFIG_APP];
                result = ConfigurationManager.AppSettings[string.Format("{0}_{1}", name, cfgtype)];
            }
            catch (Exception ex)
            {
                result = string.Format("App setting {0} not found {1} ", name, ex.Message);
            }

            return result;
        }
    }
}
