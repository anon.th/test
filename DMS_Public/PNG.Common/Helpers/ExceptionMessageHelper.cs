﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ExceptionMessageHelper.cs" company="Aware Technology Solutions Corporation Ltd">
//   Copyright (c) 2016-2017.  All rights reserved.
// </copyright>
// <summary>
//   Defines the ExceptionMessageHelper type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using PNG.Common.Models;

namespace PNG.Common.Helpers
{
    /// <summary>
    /// 
    /// </summary>
    public static class ExceptionMessageHelper
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static ExceptionMessage GetSuccess()
        {
            return new ExceptionMessage { Status = "success" };
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        /// <param name="fieldName"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public static ExceptionMessage GetError(string message, string fieldName, string data = null)
        {
            return new ExceptionMessage
            {
                Status = "error",
                Message = message,
                FieldName = fieldName,
                Data = data
            };
        }
    }
}