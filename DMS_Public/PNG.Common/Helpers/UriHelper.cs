﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="UriHelper.cs" company="Aware Technology Solutions Corporation Ltd">
//   Copyright (c) 2016-2017.  All rights reserved.
// </copyright>
// <summary>
//   Defines the UriHelper type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Web;

namespace PNG.Common.Helpers
{
    /// <summary>
    /// 
    /// </summary>
    public static class UriHelper
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="uri"></param>
        /// <param name="name"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        //public static Uri AddQuery(this Uri uri, string name, string value)
        //{
        //    var ub = new UriBuilder(uri);
        //    var queryString = HttpUtility.ParseQueryString(uri.Query);

        //    queryString.Add(name, value);
        //    ub.Query = queryString.ToString();

        //    return ub.Uri;
        //}

        /// <summary>
        /// 
        /// </summary>
        /// <param name="uri"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        //public static IList<string> GetQueryValues(this Uri uri, string name)
        //{
        //    var queryString = HttpUtility.ParseQueryString(uri.Query);
        //    var value = queryString.Get(name);

        //    // if parameter not found, return empty list
        //    if (value == null)
        //        return new List<string>();

        //    var values = value.Split(',');

        //    return values;
        //}

        /// <summary>
        /// 
        /// </summary>
        /// <param name="uri"></param>
        /// <returns></returns>
        public static string GetBaseUri(this Uri uri)
        {
            var queryString = uri.PathAndQuery;
            var values = uri.OriginalString.Replace(queryString, string.Empty).TerminateWith("/");

            return values;
        }
    }
}