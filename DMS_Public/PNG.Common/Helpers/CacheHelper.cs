﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CacheExtensions.cs" company="Aware Technology Solutions Corporation Ltd">
//   Copyright (c) 2016-2017.  All rights reserved.
// </copyright>
// <summary>
//   Defines the CacheExtensions type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using Newtonsoft.Json;
using System;
using System.Text;

namespace PNG.Common.Helpers
{
    public static class CacheExtensions
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static byte[] ObjectToByteArray(Object obj)
        {
            if (null == obj)
            {
                return null;
            }
            var item = JsonConvert.SerializeObject(obj, new JsonSerializerSettings
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                PreserveReferencesHandling = PreserveReferencesHandling.Objects
            });
            return Encoding.UTF8.GetBytes(item);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="arrBytes"></param>
        /// <returns></returns>
        public static T ByteArrayToObject<T>(byte[] arrBytes)
        {
            if (null == arrBytes)
            {
                return default(T);
            }
            var item = Encoding.UTF8.GetString(arrBytes);

            return JsonConvert.DeserializeObject<T>(item, new JsonSerializerSettings
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                PreserveReferencesHandling = PreserveReferencesHandling.Objects
            });
        }
    }
}