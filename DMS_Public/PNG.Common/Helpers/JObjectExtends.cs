﻿using System;
using Newtonsoft.Json.Linq;

namespace PNG.Common.Helpers
{
    public static class JObjectExtends
    {
        public static T JsonObjectConverter<T>(this object entry)
        {
            try
            {
                var obj = entry as JObject;

                if (obj != null)
                {
                    return obj.ToObject<T>();
                }

                return ((T)entry);
            }
            catch (Exception)
            {
                return default(T);
            }
        }
    }
}
