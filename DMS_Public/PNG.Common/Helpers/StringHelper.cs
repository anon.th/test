﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="StringHelper.cs" company="Aware Technology Solutions Corporation Ltd">
//   Copyright (c) 2016-2017.  All rights reserved.
// </copyright>
// <summary>
//   Defines the StringHelper type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace PNG.Common.Helpers
{
    /// <summary>
    /// 
    /// </summary>
    public static class StringHelper
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static string Base64Decode(this string str)
        {
            var encodedDataAsBytes = System.Convert.FromBase64String(str);
            return System.Text.Encoding.UTF8.GetString(encodedDataAsBytes);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static string Base64Encode(this string str)
        {
            var toEncodeAsBytes = System.Text.Encoding.UTF8.GetBytes(str);
            return System.Convert.ToBase64String(toEncodeAsBytes);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="str"></param>
        /// <param name="term"></param>
        /// <returns></returns>
        public static string TerminateWith(this string str, string term)
        {
            return str.EndsWith(term)
                ? str
                : string.Concat(str, term);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static string DecimalFormat23And12()
        {
            return "##########0.0###########";
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static string DecimalFormat15And2()
        {
            return "############0.0#";
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static string StringStar()
        {
            return "************";
        }
    }
}