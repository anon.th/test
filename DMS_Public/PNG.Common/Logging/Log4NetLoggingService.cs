﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Log4NetLoggingService.cs" company="Aware Technology Solutions Corporation Ltd">
//   Copyright (c) 2016-2017.  All rights reserved.
// </copyright>
// <summary>
//   Defines the Log4NetLoggingService type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.IO;
using System.Reflection;
using PNG.Common.Interfaces.Logging;
using log4net;
using log4net.Config;

namespace PNG.Common.Services.Logging
{
    /// <summary>
    /// The log 4 net logging service.
    /// </summary>
    public class Log4NetLoggingService : ILoggingService
    {
        /// <summary>
        /// The log.
        /// </summary>
        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// The configure.
        /// </summary>
        public void Configure()
        {
            XmlConfigurator.Configure(LogManager.GetRepository(Assembly.GetCallingAssembly()));
        }

        /// <summary>
        /// The configure.
        /// </summary>
        /// <param name="configFile">
        /// The config file.
        /// </param>
        public void Configure(string configFile)
        {
            XmlConfigurator.Configure(new FileInfo(configFile));
        }

        /// <summary>
        /// The debug.
        /// </summary>
        /// <param name="message">
        /// The message.
        /// </param>
        public void Debug(string message)
        {
            Log.Debug(message);
        }

        /// <summary>
        /// The error.
        /// </summary>
        /// <param name="ex">
        /// The ex.
        /// </param>
        public void Error(Exception ex)
        {
            Log.Error(ex.Message, ex);
        }

        /// <summary>
        /// The error.
        /// </summary>
        /// <param name="message">
        /// The message.
        /// </param>
        /// <param name="ex">
        /// The ex.
        /// </param>
        public void Error(string message, Exception ex)
        {
            Log.Error(message, ex);
        }

        /// <summary>
        /// The error.
        /// </summary>
        /// <param name="message">
        /// The message.
        /// </param>
        public void Error(string message)
        {
            Log.Error(message);
        }

        /// <summary>
        /// The fatal.
        /// </summary>
        /// <param name="message">
        /// The message.
        /// </param>
        public void Fatal(string message)
        {
            Log.Fatal(message);
        }

        /// <summary>
        /// The info.
        /// </summary>
        /// <param name="message">
        /// The message.
        /// </param>
        public void Info(string message)
        {
            Log.Info(message);
        }

        /// <summary>
        /// The warn.
        /// </summary>
        /// <param name="message">
        /// The message.
        /// </param>
        public void Warn(string message)
        {
            Log.Warn(message);
        }
    }
}
