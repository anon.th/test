﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Log.cs" company="Aware Technology Solutions Corporation Ltd">
//   Copyright (c) 2016-2017.  All rights reserved.
// </copyright>
// <summary>
//   Defines the Log type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using PNG.Common.Interfaces.Logging;

namespace PNG.Common.Services.Logging
{
    /// <summary>
    /// The log.
    /// </summary>
    public static class Log
    {
        /// <summary>
        /// The _logging service.
        /// </summary>
        private static ILoggingService _loggingService;

        /// <summary>
        /// The set logger.
        /// </summary>
        /// <param name="logger">
        /// The logger.
        /// </param>
        public static void SetLogger(ILoggingService logger)
        {
            if (logger != null)
                _loggingService = logger;
        }

        /// <summary>
        /// The debug.
        /// </summary>
        /// <param name="message">
        /// The message.
        /// </param>
        public static void Debug(string message)
        {
            if (_loggingService != null)
                _loggingService.Debug(message);
        }

        /// <summary>
        /// The error.
        /// </summary>
        /// <param name="ex">
        /// The ex.
        /// </param>
        public static void Error(Exception ex)
        {
            if (_loggingService != null)
                _loggingService.Error(ex);
        }

        /// <summary>
        /// The error.
        /// </summary>
        /// <param name="message">
        /// The message.
        /// </param>
        /// <param name="ex">
        /// The ex.
        /// </param>
        public static void Error(string message, Exception ex)
        {
            if (_loggingService != null)
                _loggingService.Error(message, ex);
        }

        /// <summary>
        /// The error.
        /// </summary>
        /// <param name="message">
        /// The message.
        /// </param>
        public static void Error(string message)
        {
            if (_loggingService != null)
                _loggingService.Error(message);
        }

        /// <summary>
        /// The Fatal.
        /// </summary>
        /// <param name="message">
        /// The message.
        /// </param>
        public static void Fatal(string message)
        {
            if (_loggingService != null)
                _loggingService.Fatal(message);
        }

        /// <summary>
        /// The info.
        /// </summary>
        /// <param name="message">
        /// The message.
        /// </param>
        public static void Info(string message)
        {
            if (_loggingService != null)
                _loggingService.Info(message);
        }

        /// <summary>
        /// The warn.
        /// </summary>
        /// <param name="message">
        /// The message.
        /// </param>
        public static void Warn(string message)
        {
            if (_loggingService != null)
                _loggingService.Warn(message);
        }
    }
}
