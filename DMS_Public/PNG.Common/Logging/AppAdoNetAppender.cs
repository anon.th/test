﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AppAdoNetAppender.cs" company="Aware Technology Solutions Corporation Ltd">
//   Copyright (c) 2016-2017.  All rights reserved.
// </copyright>
// <summary>
//   Defines the AppAdoNetAppender type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using PNG.Common.Enums;
using PNG.Common.Enums.Common;
using log4net.Appender;
using System;

namespace PNG.Common.Services.Logging
{
    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    // ReSharper disable once InconsistentNaming
    public class AppAdoNetAppender : AdoNetAppender
    {
        /// <summary>
        /// 
        /// </summary>
        public override void ActivateOptions()
        {
            PopulateConnectionString();
            base.ActivateOptions();
        }

        /// <summary>
        /// 
        /// </summary>
        private void PopulateConnectionString()
        {
            ConnectionString = ConnectionString;
        }

        /// <summary>
        /// 
        /// </summary>
        public new string ConnectionString
        {
            get
            {
                return base.ConnectionString;
            }
            set
            {
                if (value == null)
                    throw new ArgumentNullException("value");

                var connectionStringName = AppConfigurationCode.ApplicationConnectionString.GetAttributeCode();
                base.ConnectionString = Helpers.ConfigurationHelper.GetConnectString(connectionStringName);
            }
        }
    }
}
