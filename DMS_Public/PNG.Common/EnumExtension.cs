﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="EnumAttribute.cs" company="Aware Technology Solutions Corporation Ltd">
//   Copyright (c) 2016-2017.  All rights reserved.
// </copyright>
// <summary>
//   Defines the EnumAttribute type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Linq;
using System.Reflection;

namespace PNG.Common.Enums
{
    /// <summary>
    /// The enum attrribute.
    /// </summary>
    [AttributeUsage(AttributeTargets.Field, AllowMultiple = false, Inherited = true)]
    public class EnumAttribute : Attribute
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="EnumAttribute"/> class.
        /// </summary>
        protected EnumAttribute()
        {
        }
    }

    /// <summary>
    /// The enum extension.
    /// </summary>
    public static class EnumExtension
    {
        /// <summary>
        /// The get attribute.
        /// </summary>
        /// <param name="value">
        /// The value.
        /// </param>
        /// <returns>
        /// The <see cref="EnumAttribute"/>.
        /// </returns>
        public static EnumAttribute GetAttribute(this Enum value)
        {
            var type = value.GetType();
            var fieldInfo = type.GetField(value.ToString());
            var atts = (EnumAttribute[])fieldInfo.GetCustomAttributes(typeof(EnumAttribute), false);

            return atts.Length > 0 ? atts[0] : null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string GetAttributeCode(this Enum value)
        {
            return ((PNGAttribute)value.GetAttribute()).Code;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string GetAttributeDescription(this Enum value)
        {
            return ((PNGAttribute)value.GetAttribute()).Description;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static int GetAttributeId(this Enum value)
        {
            return ((PNGAttribute)value.GetAttribute()).Id;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string GetAttributeMessage(this Enum value)
        {
            return ((PNGAttribute)value.GetAttribute()).Message;
        }
    }

    /// <summary>
    /// The enum extension.
    /// </summary>
    /// <typeparam name="T">
    /// </typeparam>
    public static class EnumExtension<T>
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public static T GetEnumByName(string name)
        {
            var enumType = typeof(T);
            var items = enumType.GetMembers(BindingFlags.Public | BindingFlags.Static).Where(e => e.Name == name);

            var firstOrDefault = items.FirstOrDefault();
            if (firstOrDefault != null)
                return (T)Enum.Parse(typeof(T), firstOrDefault.Name, true);

            return default(T);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        public static T GetEnumByCode(string code)
        {
            var enumType = typeof(T);
            var items = enumType.GetMembers(BindingFlags.Public | BindingFlags.Static);
            var result = (from item in items let check = item.GetCustomAttributes().FirstOrDefault(i => ((PNGAttribute)i).Code.ToUpper() == code.ToUpper()) where check != null select item).FirstOrDefault();

            if (result == null)
            {
                throw new ArgumentException("Argument incorect.");
            }

            return (T)Enum.Parse(typeof(T), result.Name, true);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static T GetEnumById(int id)
        {
            var enumType = typeof(T);
            var items = enumType.GetMembers(BindingFlags.Public | BindingFlags.Static);
            var result = (from item in items let check = item.GetCustomAttributes().FirstOrDefault(i => ((PNGAttribute)i).Id == id) where check != null select item).FirstOrDefault();

            if (result == null)
            {
                throw new ArgumentException("Argument incorect.");
            }

            return (T)Enum.Parse(typeof(T), result.Name, true);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="description"></param>
        /// <returns></returns>
        public static T GetEnumByDescription(string description)
        {
            var enumType = typeof(T);
            var items = enumType.GetMembers(BindingFlags.Public | BindingFlags.Static);
            var result = (from item in items let check = item.GetCustomAttributes().FirstOrDefault(i => ((PNGAttribute)i).Description == description) where check != null select item).FirstOrDefault();

            if (result == null)
            {
                return default(T);
            }

            return (T)Enum.Parse(typeof(T), result.Name, true);
        }
    }

    /// <summary>
    /// The pvd attribute.
    /// </summary>
    // ReSharper disable once InconsistentNaming
    public class PNGAttribute : EnumAttribute
    {
        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the code.
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the Message.
        /// </summary>
        public string Message { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public PNGAttribute()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PNGAttribute"/> class.
        /// </summary>
        /// <param name="id">
        /// The Id.
        /// </param>
        public PNGAttribute(int id)
        {
            Id = id;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PNGAttribute"/> class.
        /// </summary>
        /// <param name="code">
        /// The code.
        /// </param>
        public PNGAttribute(string code)
        {
            Code = code;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PNGAttribute"/> class.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <param name="code">
        /// The code.
        /// </param>
        public PNGAttribute(int id, string code)
        {
            Id = id;
            Code = code;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PNGAttribute"/> class.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <param name="code">
        /// The code.
        /// </param>
        /// <param name="description">
        /// The description.
        /// </param>
        public PNGAttribute(int id, string code, string description)
        {
            Id = id;
            Code = code;
            Description = description;
        }
    }
}