﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ILoggingService.cs" company="Aware Technology Solutions Corporation Ltd">
//   Copyright (c) 2016-2017.  All rights reserved.
// </copyright>
// <summary>
//   The LoggingService interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;

namespace PNG.Common.Interfaces.Logging
{
    /// <summary>
    /// The LoggingService interface.
    /// </summary>
    public interface ILoggingService
    {
        /// <summary>
        /// Automatically configures the log4net system based on the 
        /// application's configuration settings.
        /// </summary>
        void Configure();

        /// <summary>
        /// Configures log4net using the specified configuration file.
        /// </summary>
        /// <param name="configFile">
        /// The config file.
        /// </param>
        void Configure(string configFile);

        /// <summary>
        /// The debug.
        /// </summary>
        /// <param name="message">
        /// The message.
        /// </param>
        void Debug(string message);

        /// <summary>
        /// The error.
        /// </summary>
        /// <param name="ex">
        /// The ex.
        /// </param>
        void Error(Exception ex);

        /// <summary>
        /// The error.
        /// </summary>
        /// <param name="message">
        /// The message.
        /// </param>
        /// <param name="ex">
        /// The ex.
        /// </param>
        void Error(string message, Exception ex);

        /// <summary>
        /// The error.
        /// </summary>
        /// <param name="message">
        /// The message.
        /// </param>
        void Error(string message);

        /// <summary>
        /// The fatal.
        /// </summary>
        /// <param name="message">
        /// The message.
        /// </param>
        void Fatal(string message);

        /// <summary>
        /// The info.
        /// </summary>
        /// <param name="message">
        /// The message.
        /// </param>
        void Info(string message);

        /// <summary>
        /// The warn.
        /// </summary>
        /// <param name="message">
        /// The message.
        /// </param>
        void Warn(string message);
    }
}
