﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ILoggingManager.cs" company="Aware Technology Solutions Corporation Ltd">
//   Copyright (c) 2016-2017.  All rights reserved.
// </copyright>
// <summary>
//   Defines the ILoggingManager type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using log4net;
using System;

namespace PNG.Common.Interfaces.Logging
{
    /// <summary>
    /// The LoggingManager interface.
    /// </summary>
    public interface ILoggingManager
    {
        /// <summary>
        /// The get logger.
        /// </summary>
        /// <param name="type">
        /// The type.
        /// </param>
        /// <returns>
        /// The <see cref="ILog"/>.
        /// </returns>
        ILog GetLogger(Type type);
    }
}
