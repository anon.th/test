﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ExceptionMessage.cs" company="Aware Technology Solutions Corporation Ltd">
//   Copyright (c) 2016-2017.  All rights reserved.
// </copyright>
// <summary>
//   Defines the ExceptionMessage type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.Collections.Generic;

namespace PNG.Common.Models
{
    public class ExceptionMessage
    {
        /// <summary>
        /// Get or Set status
        /// </summary>
        public string Status { get; set; }

        /// <summary>
        /// /// Get or Set Message
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// /// Get or Set FieldName
        /// </summary>
        public string FieldName { get; set; }

        /// <summary>
        /// /// Get or Set Data
        /// </summary>
        public string Data { get; set; }

        /// <summary>
        /// The translation message should be extract and set translation 
        /// </summary>
        public List<string> TranslationMessage { get; set; }
    }
}
