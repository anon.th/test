﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PNG.Common.Models
{
    public class MessageResource
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string Code { get; set; }
        public string Message { get; set; }
    }
}
