﻿namespace PNG.Common.Enums
{
    public enum WebApiCache
    {
        [PNG(Code = "AddressViewCache", Description = "Address view cache name")]
        AddressViewCache,

        [PNG(Code = "GeneralCache", Description = "General cache cache name")]
        GeneralCache,

        [PNG(Code = "DocumentStatusCache", Description = "DocumentStatus cache name")]
        DocumentStatusCache
    }

    public enum WebSiteCache
    {
        [PNG(Code = "AddressViewCache", Description = "Address view cache name")]
        AddressViewCache,

        [PNG(Code = "CultureResourceCache", Description = "Culture resource cache name")]
        CultureResourceCache,

        [PNG(Code = "GeneralCache", Description = "General cache name")]
        GeneralCache,

        [PNG(Code = "GeneralCacheTable", Description = "General table cache name")]
        GeneralCacheTable,

        [PNG(Code = "DocumentStatusCache", Description = "DocumentStatus cache name")]
        DocumentStatusCache,

        [PNG(Code = "DocumentStatusCacheTable", Description = "DocumentStatus tablr cache name")]
        DocumentStatusCacheTable
    }
}

