﻿namespace PNG.Common.Enums
{
    public enum TokenKey
    {
        [PNG(Code = "__RequestVerificationToken")]
        RequestVerificationToken,

        [PNG(Code = "__CookieToken")]
        CookieToken
    }
}