﻿namespace PNG.Common.Enums
{
    public enum UserClaim
    {
        [PNG(Code = "Id", Description = "Application User Id")]
        Id,

        [PNG(Code = "UserName", Description = "Application User Name")]
        UserName,

        [PNG(Code = "FirstName", Description = "Application FirstName")]
        FirstName,

        [PNG(Code = "LastName", Description = "Application LastName")]
        LastName,

        [PNG(Code = "Email", Description = "Application User Email")]
        Email,

        [PNG(Code = "DefaultRoleId", Description = "Default Role Id")]
        DefaultRoleId,

        [PNG(Code = "IpAddress", Description = "IP Address")]
        IpAddress,

        [PNG(Code = "CurrentGuid", Description = "Current Guid completed login")]
        CurrentGuid,

        [PNG(Code = "SuccessMessage", Description = "Id after Login")]
        SuccessMessage
    }
}
