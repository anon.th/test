﻿namespace PNG.Common.Enums
{
    public enum SearchOperatorType
    {
        [PNG(Code = "Like", Description = "Like Operator Condition")]
        Like,

        [PNG(Code = "=", Description = "Equal Operator Condition")]
        Equal,

        [PNG(Code = "<>", Description = "Not Equal Operator Condition")]
        NotEqual,

        [PNG(Code = ">=", Description = "Greater and Equal Operator Condition")]
        GreaterEqual,

        [PNG(Code = "<=", Description = "Less and Equal Operator Condition (if date time concat 23:59:59)")]
        LessEqual,

        [PNG(Code = "<=", Description = "Less than or Equal Operator Condition with own time")]
        LessOrEqualWithOwnTime,

        [PNG(Code = " IN ", Description = "In Condition")]
        In,

        [PNG(Code = "NOT IN ", Description = "Not In Condition")]
        NotIn,

        [PNG(Code = "IS ", Description = "Is null or not null")]
        Is,

        [PNG(Code = ">", Description = "Greater Than Operator Condition")]
        GreaterThan,

        [PNG(Code = "<", Description = "Less Than Operator Condition")]
        LessThan
    }
}
