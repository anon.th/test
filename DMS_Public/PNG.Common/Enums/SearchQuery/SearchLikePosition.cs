﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PNG.Common.Enums
{
    public enum SearchLikePosition
    {
        [PNG(Code = " BW", Description = "Begin Words")]
        BeginWords,

        [PNG(Code = "EW", Description = "End Words")]
        EndWords,

        [PNG(Code = "AW", Description = "All Words")]
        AllWords,
    }
}
