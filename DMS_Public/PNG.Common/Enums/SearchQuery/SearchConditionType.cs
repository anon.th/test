﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PNG.Common.Enums
{
    public enum SearchConditionType
    {
        [PNG(Code = " And ", Description = "And Condition")]
        And,

        [PNG(Code = " Or ", Description = "Or Condition")]
        Or,

        [PNG(Code = " NOT ", Description = "Not Condition")]
        Not
    }
}
