﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomMessageError.cs" company="Aware Technology Solutions Corporation Ltd">
//   Copyright (c) 2016-2017.  All rights reserved.
// </copyright>
// <summary>
//   Defines the CustomMessageError type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace PNG.Common.Enums.Message
{
    /// <summary>
    /// The CustomMessageError
    /// </summary>
    public enum Message
    {
        [PNG(Code = "MSG001", Description = "ลบสำเร็จ", Message = "Delete Successfully.")]
        MessageDeleteSuccess,
        [PNG(Code = "MSG002", Description = "บันทึกสำเร็จ", Message = "Save Successfully.")]
        MessageSaveSuccess,
        [PNG(Code = "MSG003", Description = "สำเร็จ", Message = "Successfully.")]
        MessageSuccess,
        [PNG(Code = "MSG004", Description = "Username หรือ Password ผิด", Message = "Username or password incorrect. Please try again.")]
        MessageinvailUsernameAndPassword,
        [PNG(Code = "MSG005", Description = "มี Error ให้ไปติดต่อ Admin", Message = "Contact technical support.")]
        MessageContactAdmin,
        [PNG(Code = "MSG006", Description = "Data ซ้ำ", Message = "Data duplicate !")]
        MessageDataduplicate,
        [PNG(Code = "MSG007", Description = "Duplicate สำเร็จ", Message = "Duplicate is success.")]
        MessageDataduplicateSuccess,
        [PNG(Code = "MSG008", Description = "บันทึกผิดพลาด", Message = "Save failed.")]
        MessageSaveFail,
        [PNG(Code = "MSG009", Description = "confirm deny หลาย Rows", Message = "Please confirm that you wish to batch deny the {0} requests you have selected")]
        MessageConfirmDenylistdayoff,
        [PNG(Code = "MSG010", Description = "confirm approve หลาย Rows", Message = "Please confirm that you wish to batch approve the {0} requests you have selected")]
        MessageConfirmApprovelistdayoff,
        [PNG(Code = "MSG011", Description = "confirm approve", Message = "Are you sure you wish to approve this day off request?")]
        MessageConfirmApprovedayoff,
        [PNG(Code = "MSG012", Description = "confirm deny", Message = "Are you sure you wish to deny this day off request?")]
        MessageConfirmDenydayoff,
        [PNG(Code = "MSG013", Description = "ระบุจำนวนไม่เกินที่กำหนด {0}=Field,จำนวน{1}-{2}", Message = "Length of {0} must be between {1} and {2}.")]
        MessageOutofLength,
        [PNG(Code = "MSG014", Description = "ต้องระบุ require field", Message = "Please enter {0}.")]
        MessageRequire,
        [PNG(Code = "MSG015", Description = "ต้องระบุ form to ใน calendar เป็นในปีเดียวกัน", Message = "Year not Duplicate")]
        MessageYearNotDup,
        [PNG(Code = "MSG016", Description = "รับพารามิตเตอร์ไม่ครบ", Message = "The parameters sent were invalid")]
        MessageParametersInvalid,
        [PNG(Code = "MSG017", Description = "Invalid Credentials", Message = "Invalid Credentials")]
        MessageInvalidCredentials,
        [PNG(Code = "MSG018", Description = "The staff code sent is invalid", Message = "The staff code sent is invalid")]
        MessageStaffCodeInvalid,
        [PNG(Code = "MSG019", Description = "Confirm Duplicate Year", Message = "Owerwrite data that exists within this year ?")]
        MessageConfirmDup,
        [PNG(Code = "MSG020", Description = "Confirm Delete holiday", Message = "Are you sure you want to delete the selected holiday from the calendar?")]
        MessageConfirmDeleteHholiday,
        [PNG(Code = "MSG021", Description = "Confirm Add Data", Message = "Are you sure you want to save?")]
        MessageConfirmAdd,
        [PNG(Code = "MSG022", Description = "Confirm Edit Data", Message = "Are you sure you want to edit?")]
        MessageConfirmEdit,
        [PNG(Code = "MSG023", Description = "Confirm Delete Data", Message = "Are you sure you want to delete?")]
        MessageConfirmDelete,
        [PNG(Code = "MSG024", Description = "import one file at a time.", Message = "You can only import {0} file at a time.")]
        MessageImportLimitFiles,
        [PNG(Code = "MSG025", Description = "Invalid file type", Message = "Invalid file type: {0} Import file must be Microsoft Excel only")]
        MessageInvalidFileType,
        [PNG(Code = "MSG026", Description = "Confirm Import data", Message = "Please confirm if you wish to proceed to import the following data.")]
        MessageConfirmImport,
        [PNG(Code = "MSG027", Description = "This information is required", Message = "This information is required")]
        MessageThisInformationIsRequired,
        [PNG(Code = "MSG028", Description = "Invalid file or file size has exceeded it max limit of 2MB.", Message = "Invalid file or file size has exceeded it max limit of 2MB.")]
        MessageInvalidFileSize
    }
}
