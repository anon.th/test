﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PNG.Common.Enums
{
    public enum FileFormat
    {
        Csv,
        Excel,
        Text
    }
}
