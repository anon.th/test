﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ParamerterType.cs" company="Aware Technology Solutions Corporation Ltd">
//   Copyright (c) 2016-2017.  All rights reserved.
// </copyright>
// <summary>
//   Defines the ParamerterType type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace PNG.Common.Enums.Common
{
    /// <summary>
    /// The paramerter type.
    /// </summary>
    public enum ParamerterType
    {
        [PNG(Code = "Str", Description = "PtStr")]
        PtStr,
        [PNG(Code = "Int", Description = "PtInt")]
        PtInt,
        [PNG(Code = "Dec", Description = "PtDec")]
        PtDec,
        [PNG(Code = "Date", Description = "PtDate")]
        PtDate
    }
}
