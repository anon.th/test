﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ParameterOperator.cs" company="Aware Technology Solutions Corporation Ltd">
//   Copyright (c) 2016-2017.  All rights reserved.
// </copyright>
// <summary>
//   Defines the ParameterOperator type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace PNG.Common.Enums.Common
{
    /// <summary>
    /// The parameter operator.
    /// </summary>
    public enum ParameterOperator
    {
        [PNG(Code = "=", Description = "OtEqual")]
        OtEqual,
        [PNG(Code = ">=", Description = "OtGreaterEqual")]
        OtGreaterEqual,
        [PNG(Code = "<=", Description = "OtLessEqual")]
        OtLessEqual,
        [PNG(Code = "<>", Description = "NotOtEqual")]
        NotOtEqual
    }

    /// <summary>
    /// The Parameter Operator Helper
    /// </summary>
    public static class ParameterOperatorHelper
    {
        /// <summary>
        /// The to parameter operator.
        /// </summary>
        /// <param name="value">
        /// The value.
        /// </param>
        /// <returns>
        /// The <see cref="ParameterOperator"/>.
        /// </returns>
        public static ParameterOperator ToParameterOperator(this string value)
        {
            ParameterOperator result;

            switch (value)
            {
                case "=":
                    result = ParameterOperator.OtEqual;
                    break;
                case ">=":
                    result = ParameterOperator.OtGreaterEqual;
                    break;
                case "<=":
                    result = ParameterOperator.OtLessEqual;
                    break;
                default:
                    result = ParameterOperator.NotOtEqual;
                    break;
            }

            return result;
        }
    }
}
