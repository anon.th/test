﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AppConfiguration.cs" company="Aware Technology Solutions Corporation Ltd">
//   Copyright (c) 2016-2017.  All rights reserved.
// </copyright>
// <summary>
//   Defines the AppConfigurationCode type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace PNG.Common.Enums.Common
{
    /// <summary>
    /// 
    /// </summary>
    public enum AppConfigurationCode
    {
        [PNG(Code = "ApplicationConnectionString", Description = "Database Connection String")]
        ApplicationConnectionString,

        [PNG(Code = "baseApi", Description = "Application base api url")]
        baseApi,

        [PNG(Code = "applicationID", Description = "applicationID")]
        applicationID,

        [PNG(Code = "enterpriseID", Description = "enterpriseID")]
        enterpriseID,

        // AppConfiguration table
        [PNG(Code = "MaxTimeoutExpired", Description = "Maximum owin context timeout expired in minutes")]
        MaxTimeoutExpired,

        [PNG(Code = "MaxUserSessionTimeoutExpired", Description = "Maximum identity user session timeout expired in minutes")]
        MaxUserSessionTimeoutExpired,


        [PNG(Code = "MaxReValidateUserSession", Description = "Maximum re validate identity user session in minutes")]
        MaxReValidateUserSession,

    }
}
