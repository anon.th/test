﻿using PNG.Common.Enums;
using PNG.Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PNG.BusinessLogic.Interfaces
{
    public interface IReportService
    {
        Task<List<FileResult>> GetShipmentTrackingReport(SearchShipmentTrackingReport parameters);

        Task<List<FileResult>> GetCSS_PrintConsNoteDetail(PrintConsNoteResult printConsNoteDetail);
        Task<List<FileResult>> GetCSS_PrintShippingListDetail(PrintShippingListDetail[] printShippingListDetail);
        Task<List<FileResult>> GetCSS_ReprintConsNoteDetail(ReprintConsNoteResult reprintConsNoteDetail);
        Task<List<FileResult>> GetCSS_ReprintConsNoteLabelDetail(ReprintConsNoteResult reprintConsNoteLabelDetail);

        string TestReport();
    }
}
