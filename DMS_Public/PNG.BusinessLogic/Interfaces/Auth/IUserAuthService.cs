﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IUserAuthService.cs" company="Aware Corporation Limited">
//   Copyright (c) 2014-2015.  All rights reserved.
// </copyright>
// <summary>
//   Defines the IUserAuthService type.
//   Reviewed on 23 Jan 2017.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.Threading.Tasks;
using PNG.DataAccess.AspNetIdentities;
using PNG.Common.Models;
using System.Collections.Generic;
using PNG.Model;

// ReSharper disable once CheckNamespace
namespace PNG.BusinessLogic.Interfaces
{
    public interface IUserAuthService
    {
        /// <summary>
        /// get user login
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="password"></param>
        /// <param name="clientIpAddress"></param>
        /// <returns></returns>
        Task<ApplicationUser> GetUser(string userName, string password, string clientIpAddress, string enterpriseID);

    }
}