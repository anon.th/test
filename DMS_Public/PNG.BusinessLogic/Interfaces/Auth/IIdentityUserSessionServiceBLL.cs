﻿
using PNG.Model;

// ReSharper disable once CheckNamespace
namespace PNG.BusinessLogic.Interfaces
{
    /// <summary>
    /// The IdentityUserSessionService interface.
    /// </summary>
    public interface IIdentityUserSessionService
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="userSession"></param>
        /// <returns></returns>
        bool CreateIdentityUserSession(IdentityUserSessions userSession);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="authToken"></param>
        /// <returns></returns>
        bool InvalidateUserSession(string userName, string authToken);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="authToken"></param>
        /// <param name="sessionTimeout"></param>
        /// <returns></returns>
        bool ReValidateUserSession(string userName, string authToken, int sessionTimeout);

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        bool DeleteExpiredUserSessions();

        /// <summary>
        /// The dispose.
        /// </summary>
        void Dispose();
    }
}