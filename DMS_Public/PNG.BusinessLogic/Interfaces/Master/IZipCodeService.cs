﻿using PNG.Model;
using PNG.Model.Models;

namespace PNG.BusinessLogic.Interfaces
{
    public interface IZipCodeService
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        SearchResultView<Zipcode> SearchWithCriteria(SearchQueryParameters parameters);

    }
}
