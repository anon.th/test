﻿using PNG.Model;

namespace PNG.BusinessLogic.Interfaces
{
    public interface IUserProfileService
    {

        /// <summary>
        /// 
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        bool Edit(User_Master user);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        User_Master GetUser(string userId);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        bool UpdatePassword(User_Master entity);


        /// <summary>
        /// 
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        string ValidateUserProfile(User_Master user);

    }
}
