﻿using PNG.Model;
using PNG.Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PNG.BusinessLogic.Interfaces
{
    public interface IAppRouteMenuService
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="queryParameters"></param>
        /// <returns></returns>
        SearchResultView<AppRouteMenu> SearchWithCriteria(SearchQueryParameters queryParameters);

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        IEnumerable<RouteMenu> GetAppRouteMenus();

    }
}
