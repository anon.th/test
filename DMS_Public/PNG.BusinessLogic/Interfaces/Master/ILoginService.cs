﻿using PNG.Model;
using System.Collections.Generic;

namespace PNG.BusinessLogic.Interfaces
{
    /// <summary>
    /// 
    /// </summary>
    public interface ILoginService
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="appID"></param>
        /// <param name="enterpriseID"></param>
        /// <param name="userID"></param>
        /// <param name="userPswd"></param>
        /// <returns></returns>
        List<TestConnection> Login(string appID, string enterpriseID, string userID, string userPswd);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="appID"></param>
        /// <param name="enterpriseID"></param>
        /// <param name="userID"></param>
        /// <param name="userPswd"></param>
        /// <returns></returns>
        bool AuthenticateUser(string appID, string enterpriseID, string userID, string userPswd);
    }
}
