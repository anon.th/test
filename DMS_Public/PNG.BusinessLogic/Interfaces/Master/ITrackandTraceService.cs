﻿using PNG.Model;
using PNG.Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PNG.BusinessLogic.Interfaces
{
    public interface ITrackandTraceService
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="ConsignmentNo"></param>
        /// <param name="refNo"></param>
        /// <param name="userId"></param>
        /// <param name="queryParameters"></param>
        /// <returns></returns>
        //TrackAndTraceResult SearchTrackandTrace(string ConsignmentNo, string refNo, string userId, bool expandPackageDetails);

        TrackAndTraceResult SearchTrackandTrace(TrackandTraceParam param);

    }
}
