﻿using PNG.Model;
using PNG.Model.Models;
using System;
using System.Collections.Generic;

namespace PNG.BusinessLogic.Interfaces
{
    public interface ISearchConsignmentService
    {
        SearchConsignmentStatusPrinting getStatusPrinting();

        List<CreateConsignmentQuerySndRecName> QuerySndRecName(string userloggedin);

        List<CreateConsignmentCustomsWarehousesName> getCustomsWarehousesName(string selectedValue);

        string getEnterpriseUserAccounts(string userId);

        List<SearchConsignmentCustomStatus> getCustomStatus();

        List<SearchConsignmentCustomStatus> getCustomerStatus();

        SearchConsignmentResult CSS_ConsignmentStatus(SearchConsignmentParam searchconsignParam);

        SearchConsignmentReprintConsNoteResult CSS_ReprintConsNote(ReprintConsNoteParam reprintConsNoteParam);

        SearchConsignmentPrintShippingListResult CSS_PrintShippingList(PrintShippingListParam printShippingListParam);

        SearchConsignmentPrintConsNoteResult CSS_PrintConsNote(PrintShippingListParam printConsNoteParam);

        string getConnectionString();
    }
}
