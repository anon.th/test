﻿using PNG.Model;
using PNG.Model.Models;

namespace PNG.BusinessLogic.Interfaces
{
    public interface IReferencesService
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        SearchResultView<Reference> SearchWithCriteria(SearchQueryParameters parameters);

    }
}
