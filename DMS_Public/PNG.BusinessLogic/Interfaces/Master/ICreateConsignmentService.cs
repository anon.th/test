﻿using PNG.Model;
using PNG.Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PNG.BusinessLogic.Interfaces
{
    public interface ICreateConsignmentService
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="EnterpriseID"></param>
        /// <param name="UserID"></param>
        /// <param name="ConsgnNo"></param>
        /// <param name="ActionType"></param>
        /// <returns></returns>
        CreateConsignmentResult GetCreateConsignmentStoreProcedure(CreateConsignmentParam createconsignParam);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="updateParam"></param>
        /// <returns></returns>
        CreateConsignmentUpdateReference UpdateReferenceStoreProcedure(CreateConsignmentParam createconsignParam);

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        CreateConsignmentBindSenderDetail GetBindSenderDetails(string userId);

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        CreateConsignmentCustomerAccount GetCustomerAccount(string userId);

        bool GetUserAssignedRoles(string userId);

        List<CreateConsignmentQuerySndRecName> GetQuerySndRecName(string userId);

        List<CreateConsignmentServiceCode> GetServiceCode();

        CreateConsignmentCustomsWarehousesName GetCustomsWarehousesName(string selectedValue);

        CreateConsignmentStateName GetStateName(string zipcode);

        CreateConsignmentBindRecipienDetail getRecipientDetailByTelephone(string telephone);

        CreateConsignmentBindRecipienDetail getRecipientDetailByCostCentre(string costcentre);

        Customer getCustomerAcc2(string payerId);

        List<CreateConsignmentPackageLimit> getPackageLimit(string userId);

        List<CreateConsignmentConfigurations> getConfigurations(string userId);

        CreateConsignmentEnterpriseContract getEnterpriseContract(string userId);

        CreateConsignmentDefaultServiceCode getDefaultServiceCode(string userId);


    }
}
