﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PNG.BusinessLogic.Interfaces
{
    public interface ITestConnectionService
    {
        List<Model.TestConnection> GetList();
    }
}
