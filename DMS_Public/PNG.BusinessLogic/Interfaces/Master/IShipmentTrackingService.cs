﻿using PNG.Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PNG.BusinessLogic.Interfaces
{
    public interface IShipmentTrackingService
    {

        ShipmentTrackingUserDetail getUserDetail(string userId);
        List<ShipmentTrackingCodeValue> getCodeValues(ShipmentTrackingParam shipmentParam);
        List<ShipmentTrackingAllRoles> getAllRoles(string userId);
        List<ShipmentTrackingPathCodeQuery> getPathCodeQuery();
        List<ShipmentTrackingPathCodeQuery> getDistributionCenterQuery();
        List<ShipmentTrackingPathCodeQuery> getDistributionCenterQuery(string userLocationId);
        SearchResultView<LovState> SearchWithCriteria(SearchQueryParameters parameters);
        SearchResultView<LovStatusCode> SearchWithStatusCode(SearchQueryParameters parameters);
        SearchResultView<LovExeptionCode> SearchWithExceptionCode(SearchQueryParameters parameters);
        ShipmentTrackingDeliveryPath getDeliveryPath(ShipmentTrackingDeliveryPathParam paramPath);

    }
}
