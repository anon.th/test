﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PNG.Model;
using PNG.Model.Models;

namespace PNG.BusinessLogic.Interfaces
{
    public interface IMenuService
    {

        //List<Module_Tree_User_Relation> GetMenu(string userId);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="appID"></param>
        /// <param name="enterpriseID"></param>
        /// <param name="user"></param>
        /// <param name="moduleid"></param>
        /// <returns></returns>
        List<Module_Tree_Role_Relation> GetAllModules(string userid, string moduleid); //User_Master user
    }
}
