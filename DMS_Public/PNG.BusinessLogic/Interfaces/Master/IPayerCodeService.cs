﻿using PNG.Model;
using PNG.Model.Models;


namespace PNG.BusinessLogic.Interfaces
{
    public interface IPayerCodeService
    {
        SearchResultView<Customer> SearchWithCriteria(SearchQueryParameters parameters);

    }
}
