﻿using System.Collections.Generic;
using PNG.BusinessLogic.Interfaces;
using PNG.Model;
using PNG.DataAccess.Services;
using PNG.Model.Models;
using PNG.Common.Helpers;
using PNG.Common.Enums.Common;
using PNG.Common.Enums;
using PNG.Common.Services.Tracing;
using System;
using PNG.Common.Services.Logging;
using System.Linq;

namespace PNG.BusinessLogic.Services
{
    public class UserProfileServiceBLL : IUserProfileService
    {
        /// <summary>
        /// 
        /// </summary>
        protected readonly DataAccess.Interfaces.IUserProfileService _repository = null;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="testConnectionService"></param>
        public UserProfileServiceBLL()
        {
            this._repository = new UserProfileServiceDAL();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public bool Edit(User_Master entity)
        {
            try
            {
                Tracer.TraceIn();
                return this._repository.UpdateProfile(entity);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                Tracer.TraceError(ex);
                throw;
            }
            finally
            {
                Tracer.TraceOut();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public User_Master GetUser(string userId)
        {
            try
            {
                Tracer.TraceIn();
                return this._repository.GetUser(userId);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                Tracer.TraceError(ex);
                throw;
            }
            finally
            {
                Tracer.TraceOut();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public bool UpdatePassword(User_Master entity)
        {
            try
            {
                Tracer.TraceIn();
                return this._repository.UpdatePassword(entity);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                Tracer.TraceError(ex);
                throw;
            }
            finally
            {
                Tracer.TraceOut();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public string ValidateUserProfile(User_Master user)
        {
            try
            {
                var result = string.Empty;
                Tracer.TraceIn();
                var userProfileList = this._repository.GetUsers(user.user_name);

                var queryUserProfile = from users in userProfileList
                                       where users.user_name == user.user_name && users.userid != user.userid
                                       select users;

                if (queryUserProfile.Count() > 0)
                {
                    result = "Username is duplicate";
                }

                return result;
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                Tracer.TraceError(ex);
                throw;
            }
            finally
            {
                Tracer.TraceOut();
            }
        }

    }
}
