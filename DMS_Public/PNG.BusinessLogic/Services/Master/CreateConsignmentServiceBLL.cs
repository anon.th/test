﻿using PNG.BusinessLogic.Interfaces;
using System.Collections.Generic;
using PNG.Model;
using PNG.DataAccess.Services;
using PNG.Model.Models;
using PNG.Common.Helpers;
using PNG.Common.Enums.Common;
using PNG.Common.Enums;

namespace PNG.BusinessLogic.Services
{
    public class CreateConsignmentServiceBLL : ICreateConsignmentService
    {
        /// <summary>
        /// 
        /// </summary>
        protected readonly DataAccess.Interfaces.ICreateConsignmentService _repository = null;

        /// <summary>
        /// 
        /// </summary>
        public CreateConsignmentServiceBLL()
        {
            this._repository = new CreateConsignmentServiceDAL();
        }

        public CreateConsignmentResult GetCreateConsignmentStoreProcedure(CreateConsignmentParam createconsignParam)
        {

            //GetEnterpriseID
            var applicationID = ConfigurationHelper.GetAppSetting(AppConfigurationCode.applicationID.GetAttributeCode());
            var enterpriseID = ConfigurationHelper.GetAppSetting(AppConfigurationCode.enterpriseID.GetAttributeCode());

            return this._repository.GetCreateConsignmentStoreProcedure(enterpriseID, createconsignParam);
        }
        public CreateConsignmentUpdateReference UpdateReferenceStoreProcedure(CreateConsignmentParam createconsignParam)
        {
            var applicationID = ConfigurationHelper.GetAppSetting(AppConfigurationCode.applicationID.GetAttributeCode());
            var enterpriseID = ConfigurationHelper.GetAppSetting(AppConfigurationCode.enterpriseID.GetAttributeCode());
            return this._repository.UpdateReferenceStoreProcedure(applicationID, enterpriseID, createconsignParam);
        }

        public CreateConsignmentBindSenderDetail GetBindSenderDetails(string userId)
        {
            var applicationID = ConfigurationHelper.GetAppSetting(AppConfigurationCode.applicationID.GetAttributeCode());
            var enterpriseID = ConfigurationHelper.GetAppSetting(AppConfigurationCode.enterpriseID.GetAttributeCode());

            var payerId = this._repository.QueryPlayerId(applicationID, enterpriseID, userId);
            var sndRecName = this._repository.QuerySndRecNameTwo(enterpriseID, userId, payerId);
            return this._repository.GetBindSenderDetails(applicationID, enterpriseID, payerId, sndRecName);
        }
        public CreateConsignmentCustomerAccount GetCustomerAccount(string userId)
        {
            var applicationID = ConfigurationHelper.GetAppSetting(AppConfigurationCode.applicationID.GetAttributeCode());
            var enterpriseID = ConfigurationHelper.GetAppSetting(AppConfigurationCode.enterpriseID.GetAttributeCode());

            return this._repository.GetCustomerAccount(enterpriseID, userId);
        }

        public bool GetUserAssignedRoles(string userId)
        {
            var enterpriseID = ConfigurationHelper.GetAppSetting(AppConfigurationCode.enterpriseID.GetAttributeCode());
            var roleId = "CUSTOMS;CUSTOMSOPS";
            return this._repository.GetUserAssignedRoles(enterpriseID, userId, roleId);
        }

        public List<CreateConsignmentQuerySndRecName> GetQuerySndRecName(string userId)
        {
            var applicationID = ConfigurationHelper.GetAppSetting(AppConfigurationCode.applicationID.GetAttributeCode());
            var enterpriseID = ConfigurationHelper.GetAppSetting(AppConfigurationCode.enterpriseID.GetAttributeCode());
            var payerId = this._repository.QueryPlayerId(applicationID, enterpriseID, userId);
            return this._repository.QuerySndRecName(enterpriseID, userId, payerId);
        }

        public List<CreateConsignmentServiceCode> GetServiceCode()
        {
            var applicationID = ConfigurationHelper.GetAppSetting(AppConfigurationCode.applicationID.GetAttributeCode());
            var enterpriseID = ConfigurationHelper.GetAppSetting(AppConfigurationCode.enterpriseID.GetAttributeCode());
            return this._repository.GetServiceCode(applicationID, enterpriseID);
        }

        public CreateConsignmentCustomsWarehousesName GetCustomsWarehousesName(string selectedValue)
        {
            var applicationID = ConfigurationHelper.GetAppSetting(AppConfigurationCode.applicationID.GetAttributeCode());
            var enterpriseID = ConfigurationHelper.GetAppSetting(AppConfigurationCode.enterpriseID.GetAttributeCode());
            return this._repository.GetCustomsWarehousesName(applicationID, enterpriseID, selectedValue);
        }

        public CreateConsignmentStateName GetStateName(string zipcode)
        {
            var applicationID = ConfigurationHelper.GetAppSetting(AppConfigurationCode.applicationID.GetAttributeCode());
            var enterpriseID = ConfigurationHelper.GetAppSetting(AppConfigurationCode.enterpriseID.GetAttributeCode());
            return this._repository.GetStateName(applicationID, enterpriseID, zipcode);
        }

        public CreateConsignmentBindRecipienDetail getRecipientDetailByTelephone(string telephone)
        {
            var applicationID = ConfigurationHelper.GetAppSetting(AppConfigurationCode.applicationID.GetAttributeCode());
            var enterpriseID = ConfigurationHelper.GetAppSetting(AppConfigurationCode.enterpriseID.GetAttributeCode());
            return this._repository.getRecipientDetailByTelephone(applicationID, enterpriseID, telephone);
        }


        public CreateConsignmentBindRecipienDetail getRecipientDetailByCostCentre(string costcentre)
        {
            var applicationID = ConfigurationHelper.GetAppSetting(AppConfigurationCode.applicationID.GetAttributeCode());
            var enterpriseID = ConfigurationHelper.GetAppSetting(AppConfigurationCode.enterpriseID.GetAttributeCode());
            return this._repository.getRecipientDetailByCostCentre(applicationID, enterpriseID, costcentre);
        }

        public Customer getCustomerAcc2(string payerId)
        {
            var applicationID = ConfigurationHelper.GetAppSetting(AppConfigurationCode.applicationID.GetAttributeCode());
            var enterpriseID = ConfigurationHelper.GetAppSetting(AppConfigurationCode.enterpriseID.GetAttributeCode());
            return this._repository.getCustomerAcc2(applicationID, enterpriseID, payerId);
        }

        public List<CreateConsignmentPackageLimit> getPackageLimit(string userId)
        {
            var applicationID = ConfigurationHelper.GetAppSetting(AppConfigurationCode.applicationID.GetAttributeCode());
            var enterpriseID = ConfigurationHelper.GetAppSetting(AppConfigurationCode.enterpriseID.GetAttributeCode());
            var payerId = this._repository.QueryPlayerId(applicationID, enterpriseID, userId);
            return this._repository.getPackageLimit(applicationID, enterpriseID, payerId);
        }

        public List<CreateConsignmentConfigurations> getConfigurations(string userId)
        {
            var applicationID = ConfigurationHelper.GetAppSetting(AppConfigurationCode.applicationID.GetAttributeCode());
            var enterpriseID = ConfigurationHelper.GetAppSetting(AppConfigurationCode.enterpriseID.GetAttributeCode());
            var payerId = this._repository.QueryPlayerId(applicationID, enterpriseID, userId);
            return this._repository.getConfigurations(applicationID, enterpriseID, payerId);
        }

        public CreateConsignmentEnterpriseContract getEnterpriseContract(string userId)
        {
            var applicationID = ConfigurationHelper.GetAppSetting(AppConfigurationCode.applicationID.GetAttributeCode());
            var enterpriseID = ConfigurationHelper.GetAppSetting(AppConfigurationCode.enterpriseID.GetAttributeCode());
            var payerId = this._repository.QueryPlayerId(applicationID, enterpriseID, userId);
            return this._repository.getEnterpriseContract(enterpriseID, payerId, userId);
        }

        public CreateConsignmentDefaultServiceCode getDefaultServiceCode(string userId)
        {
            var applicationID = ConfigurationHelper.GetAppSetting(AppConfigurationCode.applicationID.GetAttributeCode());
            var enterpriseID = ConfigurationHelper.GetAppSetting(AppConfigurationCode.enterpriseID.GetAttributeCode());
            var payerId = this._repository.QueryPlayerId(applicationID, enterpriseID, userId);
            return this._repository.getDefaultServiceCode(applicationID, enterpriseID, payerId);
        }
    }
}
