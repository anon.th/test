﻿using PNG.BusinessLogic.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PNG.Model;
using PNG.DataAccess.Services;

namespace PNG.BusinessLogic.Services
{
    public class LoginServiceBLL : ILoginService
    {
        /// <summary>
        /// 
        /// </summary>
        protected readonly DataAccess.Interfaces.ILoginService _repository = null;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="testConnectionService"></param>
        public LoginServiceBLL()
        {
            this._repository = new LoginServiceDAL();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="appID"></param>
        /// <param name="enterpriseID"></param>
        /// <param name="userID"></param>
        /// <param name="userPswd"></param>
        /// <returns></returns>
        public bool AuthenticateUser(string appID, string enterpriseID, string userID, string userPswd)
        {
            return this._repository.AuthenticateUser(appID, enterpriseID, userID, userPswd);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public List<TestConnection> Login(string appID, string enterpriseID, string userID, string userPswd)
        {
            bool isValid = AuthenticateUser(appID, enterpriseID, userID, userPswd);

            if (isValid)
            {
                User_Master user = this._repository.GetUser(appID, enterpriseID, userID);
                user.userid = userID.Trim();



            }
            throw new NotImplementedException();
        }
    }
}
