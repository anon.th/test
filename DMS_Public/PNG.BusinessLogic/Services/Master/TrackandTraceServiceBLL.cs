﻿using System.Collections.Generic;
using PNG.BusinessLogic.Interfaces;
using PNG.Model;
using PNG.DataAccess.Services;
using PNG.Model.Models;
using PNG.Common.Helpers;
using PNG.Common.Enums.Common;
using PNG.Common.Enums;

namespace PNG.BusinessLogic.Services
{
    public class TrackandTraceServiceBLL : ITrackandTraceService
    {
        /// <summary>
        /// 
        /// </summary>
        protected readonly DataAccess.Interfaces.ITrackandTraceService _repository = null;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="testConnectionService"></param>
        public TrackandTraceServiceBLL()
        {
            this._repository = new TrackandTraceServiceDAL();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        //public TrackAndTraceResult SearchTrackandTrace(string consignmentNo, string refNo, string userId, bool expandPackageDetails)
        //{
        //    //GetAppId
        //    var applicationID = ConfigurationHelper.GetAppSetting(AppConfigurationCode.applicationID.GetAttributeCode());
        //    var enterpriseID = ConfigurationHelper.GetAppSetting(AppConfigurationCode.enterpriseID.GetAttributeCode());

        //    //QueryPlayerIdTrackandTrace
        //    var payerId = this._repository.QueryPlayerIdTrackandTrace(applicationID, enterpriseID, userId);
        //    var expandPackageDetailsParam = expandPackageDetails == false ? "0":"1";


        //    if (consignmentNo == "null")
        //    {
        //        consignmentNo = null;
        //    }

        //    if (refNo == "null")
        //    {
        //        refNo = null;
        //    }

        //    return this._repository.SearchTrackandTrace(applicationID, enterpriseID, consignmentNo, refNo, payerId, expandPackageDetailsParam);
        //}
        public TrackAndTraceResult SearchTrackandTrace(TrackandTraceParam param)
        {
            //GetAppId
            var applicationID = ConfigurationHelper.GetAppSetting(AppConfigurationCode.applicationID.GetAttributeCode());
            var enterpriseID = ConfigurationHelper.GetAppSetting(AppConfigurationCode.enterpriseID.GetAttributeCode());
            

            //QueryPlayerIdTrackandTrace
            var payerId = this._repository.QueryPlayerIdTrackandTrace(applicationID, enterpriseID, param.userId);
            var expandPackageDetailsParam = param.returnPackage == false ? "0" : "1";


            if (param.consignmentNumber == "null")
            {
                param.consignmentNumber = null;
            }

            if (param.customerRef == "null")
            {
                param.customerRef = null;
            }

            return this._repository.SearchTrackandTrace(applicationID, enterpriseID, param.consignmentNumber, param.customerRef, payerId, expandPackageDetailsParam);
        }
    }
}
