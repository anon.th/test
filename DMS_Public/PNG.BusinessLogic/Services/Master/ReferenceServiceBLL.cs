﻿using PNG.BusinessLogic.Interfaces;
using PNG.DataAccess.Services;
using PNG.Model.Models;
using PNG.Model;
using PNG.Common.Services.Tracing;
using System;
using PNG.Common.Services.Logging;

namespace PNG.BusinessLogic.Services
{
    public class ReferencesServiceBLL : IReferencesService
    {
        /// <summary>
        /// 
        /// </summary>
        protected readonly DataAccess.Interfaces.IReferenceService _repository = null;

        /// <summary>
        /// 
        /// </summary>
        public ReferencesServiceBLL()
        {
            this._repository = new ReferencesServiceDAL();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public SearchResultView<Reference> SearchWithCriteria(SearchQueryParameters parameters)
        {
            SearchResultView<Reference> result = null;
            Tracer.TraceIn();

            try
            {
                result = this._repository.SearchWithCriteria(parameters);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                Tracer.TraceError(ex);
                throw;
            }
            finally
            {
                Tracer.TraceOut(result);

                if (this._repository != null)
                {
                    this._repository.Dispose();
                }
            }

            return result;
        }

    }
}
