﻿using System.Collections.Generic;
using PNG.BusinessLogic.Interfaces;
using PNG.DataAccess.Services;
using PNG.Model.Models;
using PNG.Common.Helpers;
using PNG.Common.Enums.Common;
using PNG.Common.Enums;
using PNG.Model;

namespace PNG.BusinessLogic.Services
{
    public class MenuServiceBLL : IMenuService
    {
        /// <summary>
        /// 
        /// </summary>
        protected readonly DataAccess.Interfaces.IMenuService _repository = null;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="testConnectionService"></param>
        public MenuServiceBLL()
        {
            this._repository = new MenuServiceDAL();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public List<Module_Tree_Role_Relation> GetAllModules(string userid, string moduleid)
        {
            //GetAppId
            var applicationID = ConfigurationHelper.GetAppSetting(AppConfigurationCode.applicationID.GetAttributeCode());
            var enterpriseID = ConfigurationHelper.GetAppSetting(AppConfigurationCode.enterpriseID.GetAttributeCode());
            //user.userid = "bishops";

            return this._repository.GetAllModules(applicationID, enterpriseID, userid, moduleid);
        }
    }
}
