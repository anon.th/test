﻿using PNG.BusinessLogic.Interfaces;
using PNG.DataAccess.Services;
using PNG.Model.Models;
using PNG.Model;
using PNG.Common.Services.Tracing;
using System;
using PNG.Common.Services.Logging;

namespace PNG.BusinessLogic.Services
{
    public class ZipCodeServiceBLL : IZipCodeService
    {
        /// <summary>
        /// 
        /// </summary>
        protected readonly DataAccess.Interfaces.IZipCodeService _repository = null;

        /// <summary>
        /// 
        /// </summary>
        public ZipCodeServiceBLL()
        {
            this._repository = new ZipCodeServiceDAL();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public SearchResultView<Zipcode> SearchWithCriteria(SearchQueryParameters parameters)
        {
            SearchResultView<Zipcode> result = null;
            Tracer.TraceIn();

            try
            {
                result = this._repository.SearchWithCriteria(parameters);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                Tracer.TraceError(ex);
                throw;
            }
            finally
            {
                Tracer.TraceOut(result);

                if (this._repository != null)
                {
                    this._repository.Dispose();
                }
            }

            return result;
        }

    }
}
