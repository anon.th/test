﻿using PNG.BusinessLogic.Interfaces;
using System.Collections.Generic;
using PNG.Model;
using PNG.DataAccess.Services;
using PNG.Model.Models;
using PNG.Common.Helpers;
using PNG.Common.Enums.Common;
using PNG.Common.Enums;

namespace PNG.BusinessLogic.Services
{
    public class SearchConsignmentServiceBLL : ISearchConsignmentService
    {
        protected readonly DataAccess.Interfaces.ISearchConsignmentService _repository = null;

        /// <summary>
        /// 
        /// </summary>
        public SearchConsignmentServiceBLL()
        {
            this._repository = new SearchConsignmentServiceDAL();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="enterpriseId"></param>
        /// <returns></returns>
        public SearchConsignmentStatusPrinting getStatusPrinting()
        {
            var applicationID = ConfigurationHelper.GetAppSetting(AppConfigurationCode.applicationID.GetAttributeCode());
            var enterpriseID = ConfigurationHelper.GetAppSetting(AppConfigurationCode.enterpriseID.GetAttributeCode());
            return this._repository.getStatusPrinting(enterpriseID);
        }

        public List<CreateConsignmentQuerySndRecName> QuerySndRecName(string userloggedin)
        {
            var applicationID = ConfigurationHelper.GetAppSetting(AppConfigurationCode.applicationID.GetAttributeCode());
            var enterpriseID = ConfigurationHelper.GetAppSetting(AppConfigurationCode.enterpriseID.GetAttributeCode());
            var payerId = this._repository.getEnterpriseUserAccountsDataSet(applicationID, enterpriseID, userloggedin);
            return this._repository.QuerySndRecName(enterpriseID, userloggedin, payerId);
        }

        public List<CreateConsignmentCustomsWarehousesName> getCustomsWarehousesName(string selectedValue)
        {
            var applicationID = ConfigurationHelper.GetAppSetting(AppConfigurationCode.applicationID.GetAttributeCode());
            var enterpriseID = ConfigurationHelper.GetAppSetting(AppConfigurationCode.enterpriseID.GetAttributeCode());
            return this._repository.getCustomsWarehousesName(applicationID, enterpriseID, selectedValue);
        }

        public string getEnterpriseUserAccounts(string userId)
        {
            var enterpriseID = ConfigurationHelper.GetAppSetting(AppConfigurationCode.enterpriseID.GetAttributeCode());
            return this._repository.getEnterpriseUserAccounts(enterpriseID, userId);
        }

        public List<SearchConsignmentCustomStatus> getCustomStatus()
        {
            var applicationID = ConfigurationHelper.GetAppSetting(AppConfigurationCode.applicationID.GetAttributeCode());
            var enterpriseID = ConfigurationHelper.GetAppSetting(AppConfigurationCode.enterpriseID.GetAttributeCode());
            return this._repository.getCustomStatus(applicationID, enterpriseID);
        }


        public List<SearchConsignmentCustomStatus> getCustomerStatus()
        {
            var applicationID = ConfigurationHelper.GetAppSetting(AppConfigurationCode.applicationID.GetAttributeCode());
            var enterpriseID = ConfigurationHelper.GetAppSetting(AppConfigurationCode.enterpriseID.GetAttributeCode());
            return this._repository.getCustomerStatus(applicationID, enterpriseID);
        }

        public SearchConsignmentResult CSS_ConsignmentStatus(SearchConsignmentParam searchconsignParam)
        {
            var enterpriseID = ConfigurationHelper.GetAppSetting(AppConfigurationCode.enterpriseID.GetAttributeCode());
            return this._repository.CSS_ConsignmentStatus(enterpriseID, searchconsignParam);
        }

        public SearchConsignmentReprintConsNoteResult CSS_ReprintConsNote(ReprintConsNoteParam reprintConsNoteParam)
        {
            var enterpriseID = ConfigurationHelper.GetAppSetting(AppConfigurationCode.enterpriseID.GetAttributeCode());
            return this._repository.CSS_ReprintConsNote(enterpriseID, reprintConsNoteParam);
        }

        public SearchConsignmentPrintShippingListResult CSS_PrintShippingList(PrintShippingListParam printShippingListParam)
        {
            var enterpriseID = ConfigurationHelper.GetAppSetting(AppConfigurationCode.enterpriseID.GetAttributeCode());
            return this._repository.CSS_PrintShippingList(enterpriseID, printShippingListParam);
        }

        public SearchConsignmentPrintConsNoteResult CSS_PrintConsNote(PrintShippingListParam printConsNoteParam)
        {
            var enterpriseID = ConfigurationHelper.GetAppSetting(AppConfigurationCode.enterpriseID.GetAttributeCode());
            return this._repository.CSS_PrintConsNote(enterpriseID, printConsNoteParam);
        }

        public string getConnectionString()
        {
            var applicationID = ConfigurationHelper.GetAppSetting(AppConfigurationCode.applicationID.GetAttributeCode());
            var enterpriseID = ConfigurationHelper.GetAppSetting(AppConfigurationCode.enterpriseID.GetAttributeCode());
            return this._repository.getConnectionString(applicationID, enterpriseID);
        }
    }
}
