﻿using PNG.BusinessLogic.Interfaces;
using PNG.Common.Enums;
using PNG.Common.Enums.Common;
using PNG.Common.Helpers;
using PNG.Common.Services.Logging;
using PNG.Common.Services.Tracing;
using PNG.DataAccess.Services;
using PNG.Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PNG.BusinessLogic.Services
{
    public class ShipmentTrackingServiceBLL : IShipmentTrackingService
    {
        protected readonly DataAccess.Interfaces.IShipmentTrackingService _repository = null;

        /// <summary>
        /// 
        /// </summary>
        public ShipmentTrackingServiceBLL()
        {
            this._repository = new ShipmentTrackingServiceDAL();
        }

        public ShipmentTrackingUserDetail getUserDetail(string userId)
        {
            var applicationID = ConfigurationHelper.GetAppSetting(AppConfigurationCode.applicationID.GetAttributeCode());
            var enterpriseID = ConfigurationHelper.GetAppSetting(AppConfigurationCode.enterpriseID.GetAttributeCode());
            return this._repository.getUserDetail(applicationID, enterpriseID, userId);
        }

        public List<ShipmentTrackingCodeValue> getCodeValues(ShipmentTrackingParam shipmentParam)
        {
            var applicationID = ConfigurationHelper.GetAppSetting(AppConfigurationCode.applicationID.GetAttributeCode());
            var enterpriseID = ConfigurationHelper.GetAppSetting(AppConfigurationCode.enterpriseID.GetAttributeCode());
            
            return this._repository.getCodeValues(applicationID, shipmentParam);
        }

        public List<ShipmentTrackingAllRoles> getAllRoles(string userId)
        {
            var applicationID = ConfigurationHelper.GetAppSetting(AppConfigurationCode.applicationID.GetAttributeCode());
            var enterpriseID = ConfigurationHelper.GetAppSetting(AppConfigurationCode.enterpriseID.GetAttributeCode());

            return this._repository.getAllRoles(applicationID, enterpriseID, userId);
        }

        public List<ShipmentTrackingPathCodeQuery> getPathCodeQuery()
        {
            var applicationID = ConfigurationHelper.GetAppSetting(AppConfigurationCode.applicationID.GetAttributeCode());
            var enterpriseID = ConfigurationHelper.GetAppSetting(AppConfigurationCode.enterpriseID.GetAttributeCode());

            return this._repository.getPathCodeQuery(applicationID, enterpriseID);
        }

        public List<ShipmentTrackingPathCodeQuery> getDistributionCenterQuery()
        {
            var applicationID = ConfigurationHelper.GetAppSetting(AppConfigurationCode.applicationID.GetAttributeCode());
            var enterpriseID = ConfigurationHelper.GetAppSetting(AppConfigurationCode.enterpriseID.GetAttributeCode());

            return this._repository.getDistributionCenterQuery(applicationID, enterpriseID);
        }

        public List<ShipmentTrackingPathCodeQuery> getDistributionCenterQuery(string userLocationId)
        {
            var applicationID = ConfigurationHelper.GetAppSetting(AppConfigurationCode.applicationID.GetAttributeCode());
            var enterpriseID = ConfigurationHelper.GetAppSetting(AppConfigurationCode.enterpriseID.GetAttributeCode());

            return this._repository.getDistributionCenterQuery(applicationID, enterpriseID, userLocationId);
        }

        public SearchResultView<LovState> SearchWithCriteria(SearchQueryParameters parameters)
        {
            SearchResultView<LovState> result = null;
            Tracer.TraceIn();

            try
            {
                result = this._repository.SearchWithCriteria(parameters);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                Tracer.TraceError(ex);
                throw;
            }
            finally
            {
                Tracer.TraceOut(result);

                if (this._repository != null)
                {
                    this._repository.Dispose();
                }
            }

            return result;
        }

        public SearchResultView<LovStatusCode> SearchWithStatusCode(SearchQueryParameters parameters)
        {
            SearchResultView<LovStatusCode> result = null;
            Tracer.TraceIn();
            var applicationID = ConfigurationHelper.GetAppSetting(AppConfigurationCode.applicationID.GetAttributeCode());
            var enterpriseID = ConfigurationHelper.GetAppSetting(AppConfigurationCode.enterpriseID.GetAttributeCode());
            try
            {
                result = this._repository.SearchWithStatusCode(parameters, applicationID, enterpriseID);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                Tracer.TraceError(ex);
                throw;
            }
            finally
            {
                Tracer.TraceOut(result);

                if (this._repository != null)
                {
                    this._repository.Dispose();
                }
            }

            return result;
        }

        public SearchResultView<LovExeptionCode> SearchWithExceptionCode(SearchQueryParameters parameters)
        {
            SearchResultView<LovExeptionCode> result = null;
            Tracer.TraceIn();
            var applicationID = ConfigurationHelper.GetAppSetting(AppConfigurationCode.applicationID.GetAttributeCode());
            var enterpriseID = ConfigurationHelper.GetAppSetting(AppConfigurationCode.enterpriseID.GetAttributeCode());
            try
            {

                result = this._repository.SearchWithExceptionCode(parameters, applicationID, enterpriseID);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                Tracer.TraceError(ex);
                throw;
            }
            finally
            {
                Tracer.TraceOut(result);

                if (this._repository != null)
                {
                    this._repository.Dispose();
                }
            }

            return result;
        }

        public ShipmentTrackingDeliveryPath getDeliveryPath(ShipmentTrackingDeliveryPathParam paramPath)
        {
            var applicationID = ConfigurationHelper.GetAppSetting(AppConfigurationCode.applicationID.GetAttributeCode());
            var enterpriseID = ConfigurationHelper.GetAppSetting(AppConfigurationCode.enterpriseID.GetAttributeCode());

            return this._repository.getDeliveryPath(applicationID, enterpriseID, paramPath);
        }

    }
}
