﻿using System.Collections.Generic;
using PNG.BusinessLogic.Interfaces;
using PNG.Model;
using PNG.DataAccess.Services;

namespace PNG.BusinessLogic.Services
{
    public class TestConnectionServiceBLL : ITestConnectionService
    {
        /// <summary>
        /// 
        /// </summary>
        protected readonly DataAccess.Interfaces.ITestConnectionService _repository = null;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="testConnectionService"></param>
        public TestConnectionServiceBLL()
        {
            this._repository = new TestConnectionServiceDAL();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public List<TestConnection> GetList()
        {
            return this._repository.SelectAll();
        }
    }
}
