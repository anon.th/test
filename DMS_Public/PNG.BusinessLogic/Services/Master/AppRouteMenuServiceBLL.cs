﻿using System;
using System.Collections.Generic;
using System.Linq;
using PNG.Common.Services.Logging;
using PNG.Common.Services.Tracing;
using PNG.Model;
using PNG.Model.Models;
using PNG.DataAccess.Interfaces;
using PNG.DataAccess.Services;

namespace PNG.BusinessLogic.Services
{
    /// <inheritdoc />
    /// <summary>
    /// The ExternalAppRouteMenu.
    /// </summary>
    public class AppRouteMenuServiceBLL : Interfaces.IAppRouteMenuService
    {
        /// <summary>
        /// The _repository.
        /// </summary>
        protected readonly IAppRouteMenuService _repository;

        /// <summary>
        /// The disposed value.
        /// </summary>
        private bool disposedValue = false; // To detect redundant calls

        public AppRouteMenuServiceBLL()
        {
            this._repository = new AppRouteMenuServiceDAL();
        }

        /// <summary>
        /// The search with criteria.
        /// </summary>
        /// <param name="queryParameters">
        /// The query parameters.
        /// </param>
        /// <returns>
        /// The <see cref="SearchResultView"/>.
        /// </returns>
        public SearchResultView<AppRouteMenu> SearchWithCriteria(SearchQueryParameters queryParameters)
        {
            SearchResultView<AppRouteMenu> result = null;
            Tracer.TraceIn();

            try
            {
                result = _repository.SearchWithCriteria(queryParameters);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                Tracer.TraceError(ex);
                throw;
            }
            finally
            {
                if (this._repository != null)
                {
                    this._repository.Dispose();
                }

                Tracer.TraceOut(result);
            }

            return result;
        }

        /// <summary>
        /// The get application route menus.
        /// </summary>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        public IEnumerable<RouteMenu> GetAppRouteMenus()
        {
            var appRoutes = new List<RouteMenu>();

            // Initialize application routes with no resolver
            #region

            var noResolverRoutes = new List<RouteMenu>()
            {
                new RouteMenu()
                {
                    RouteName = "/",
                    TemplateUrl = "/TrackAndTrace/Index"
                },
                new RouteMenu()
                {
                    //ResolverId = 0,
                    RouteName = "/trackandtrace",
                    TemplateUrl = "/TrackAndTrace/Index"
                },
                new RouteMenu()
                {
                    //ResolverId = 0,
                    RouteName = "/welcome",
                    TemplateUrl = "/Home/Welcome"
                },
                new RouteMenu()
                {
                    RouteName = "/home",
                    TemplateUrl = "/Home/Default"
                },
                new RouteMenu()
                {
                    RouteName = "/login",
                    TemplateUrl = "/Home/Login"
                },
                new RouteMenu()
                {
                    RouteName = "/userprofile",
                    TemplateUrl = "/Home/UserProfile"
                },
                new RouteMenu()
                {
                    RouteName = "/resetpassword",
                    TemplateUrl = "/Home/ResetPassword"
                },
                new RouteMenu()
                {
                    RouteName = "/error",
                    TemplateUrl = "/Error/Index"
                },
                 new RouteMenu()
                {
                    RouteName = "/createconsignment",
                    TemplateUrl = "/CreateConsignment/Index"
                },
                 new RouteMenu()
                {
                    RouteName = "/updateconsignment/consignmentno/:consignment?",
                    TemplateUrl = "/CreateConsignment/Update"
                },
                new RouteMenu()
                {
                    RouteName = "/searchconsignment",
                    TemplateUrl = "/SearchConsignment/Index"
                }
                ,
                new RouteMenu()
                {
                    RouteName = "/shipmenttracking",
                    TemplateUrl = "/ShipmentTracking/Index"
                }
            };
            #endregion

            appRoutes.AddRange(noResolverRoutes);

            Tracer.TraceIn();

            try
            {
                // Find menu ParentId are null and IsSubTitle are false from vwMenu
                #region
                var parameters = new SearchQueryParameters()
                {
                    FilterList = new List<SearchFilterView>
                {
                  
                    //new SearchFilterView()
                    //{
                    //        TableName = string.Empty,
                    //        FieldName = "Core_Module_Tree_Master.child_url",
                    //        ParameterName = "child_url",
                    //        ConditionType = SearchConditionType.And,
                    //        OperatorType = SearchOperatorType.Is,
                    //        Value = " NOT NULL "
                    //},
                    //new SearchFilterView()
                    //{
                    //        TableName = string.Empty,
                    //        FieldName = "Core_Module_Tree_Master.child_url2",
                    //        ParameterName = "child_url2",
                    //        ConditionType = SearchConditionType.And,
                    //        OperatorType = SearchOperatorType.NotEqual,
                    //        Value = ""
                    //}
                },
                    SortAscending = true,
                    SortColumn = "AppRouteMenu.id"
                };
                #endregion

                var menus = _repository.SearchWithCriteria(parameters);

                var requireResolverRoutes = menus.SearchResults.Select(menu => new RouteMenu()
                {
                    RouteName = menu.RouteName,
                    TemplateUrl = menu.TemplateUrl,
                    // TODO: check "IsResolver" property has value or not?, if not null the value must be "True"
                    ResolverId = (menu.IsResolver.HasValue && menu.IsResolver.Value) ? menu.MenuId : 0
                }).ToList();

                appRoutes.AddRange(requireResolverRoutes);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                Tracer.TraceError(ex);
                throw;
            }
            finally
            {
                if (this._repository != null)
                {
                    this._repository.Dispose();
                }

                Tracer.TraceOut(appRoutes);
            }

            return appRoutes;
        }
    }
}
