﻿using PNG.BusinessLogic.Interfaces;
using PNG.DataAccess.Services;
using PNG.Model.Models;
using PNG.Model;
using PNG.Common.Services.Tracing;
using System;
using PNG.Common.Services.Logging;

namespace PNG.BusinessLogic.Services
{
    public class PayerCodeServiceBLL : IPayerCodeService
    {
        /// </summary>
        protected readonly DataAccess.Interfaces.IPayerCodeService _repository = null;

        /// <summary>
        /// 
        /// </summary>
        public PayerCodeServiceBLL()
        {
            this._repository = new PayerCodeServiceDAL();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public SearchResultView<Customer> SearchWithCriteria(SearchQueryParameters parameters)
        {
            SearchResultView<Customer> result = null;
            Tracer.TraceIn();

            try
            {
                result = this._repository.SearchWithCriteria(parameters);

            }
            catch (Exception ex)
            {
                Log.Error(ex);
                Tracer.TraceError(ex);
                throw;
            }
            finally
            {
                Tracer.TraceOut(result);

                if (this._repository != null)
                {
                    this._repository.Dispose();
                }
            }

            return result;
        }

    }
}
