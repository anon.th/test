﻿
using PNG.BusinessLogic.Interfaces;
using PNG.Common.Services.Logging;
using PNG.Common.Services.Tracing;
using PNG.DataAccess.Interfaces;
using PNG.DataAccess.Services;
using PNG.Model;
using System;

// ReSharper disable once CheckNamespace
namespace PNG.BusinessLogic.Services
{
    // ReSharper disable once InconsistentNaming
    public class IdentityUserSessionServiceBLL : IIdentityUserSessionService, IDisposable
    {
        /// <summary>
        /// The _repository.
        /// </summary>
        protected readonly IIdentityUserSession Repository;

        /// <summary>
        /// 
        /// </summary>
        public IdentityUserSessionServiceBLL()
        {
            this.Repository = new IdentityUserSessionDAL();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userSession"></param>
        /// <returns></returns>
        public bool CreateIdentityUserSession(IdentityUserSessions userSession)
        {
            var result = false;

            try
            {
                Tracer.TraceIn();
                result = this.Repository.CreateIdentityUserSession(userSession);

            }
            catch (Exception ex)
            {
                Log.Error(ex);
                Tracer.TraceError(ex);
                throw;
            }
            finally
            {
                Tracer.TraceOut(result);
            }

            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="authToken"></param>
        /// <returns></returns>
        public bool InvalidateUserSession(string userName, string authToken)
        {
            var result = false;

            try
            {
                Tracer.TraceIn();
                result = this.Repository.InvalidateUserSession(userName, authToken);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                Tracer.TraceError(ex);
                throw;
            }
            finally
            {
                Tracer.TraceOut(result);
            }

            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="authToken"></param>
        /// <param name="sessionTimeout"></param>
        /// <returns></returns>
        public bool ReValidateUserSession(string userName, string authToken, int sessionTimeout)
        {
            var result = false;

            try
            {
                Tracer.TraceIn();
                result = this.Repository.ReValidateUserSession(userName, authToken, sessionTimeout);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                Tracer.TraceError(ex);
                throw;
            }
            finally
            {
                Tracer.TraceOut(result);
            }

            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public bool DeleteExpiredUserSessions()
        {
            var result = false;

            try
            {
                Tracer.TraceIn();
                result = this.Repository.DeleteExpiredUserSessions();
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                Tracer.TraceError(ex);
                throw;
            }
            finally
            {
                Tracer.TraceOut(result);
            }

            return result;
        }

        #region IDisposable Support

        /// <summary>
        /// The disposed value.
        /// </summary>
        private bool _disposedValue; // To detect redundant calls

        // This code added to correctly implement the disposable pattern.
        /// <summary>
        /// The dispose.
        /// </summary>
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);

            // TODO: uncomment the following line if the finalizer is overridden above.
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// The dispose.
        /// </summary>
        /// <param name="disposing">
        /// The disposing.
        /// </param>
        protected virtual void Dispose(bool disposing)
        {
            if (_disposedValue)
                return;

            if (disposing)
            {
                if (Repository != null)
                {
                    Repository.Dispose();
                }
            }

            // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
            // TODO: set large fields to null.
            _disposedValue = true;
        }
        #endregion IDisposable Support
    }
}
