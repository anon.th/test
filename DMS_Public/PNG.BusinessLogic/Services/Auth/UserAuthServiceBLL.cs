﻿using PNG.Common.Enums;
using PNG.Common.Enums.Common;
using PNG.Common.Helpers;
using PNG.Common.Services.Logging;
using PNG.Common.Services.Tracing;
using PNG.DataAccess.AspNetIdentities;
using PNG.DataAccess.Interfaces;
using PNG.DataAccess.Services;
using PNG.Model;
using System;
using System.Threading.Tasks;

// ReSharper disable once CheckNamespace
namespace PNG.BusinessLogic.Services
{
    // ReSharper disable once InconsistentNaming
    public class UserAuthServiceBLL : Interfaces.IUserAuthService
    {
        /// <summary>
        /// The _repository.
        /// </summary>
        protected readonly IUserAuthService Repository;


        /// <summary>
        ///
        /// </summary>
        public UserAuthServiceBLL()
        {
            this.Repository = new UserAuthServiceDAL();
        }

        /// <summary>
        /// get user login
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="password"></param>
        /// <param name="clientIpAddress"></param>
        /// <returns>
        /// ApplicationUser
        /// </returns>
        public async Task<ApplicationUser> GetUser(string userName, string password, string clientIpAddress, string enterpriseID)
        {
            ApplicationUser user = new ApplicationUser();
            bool isSuccessLogin = false;
            User_Master result = new User_Master();

            try
            {
                Tracer.TraceIn();
                var applicationID = ConfigurationHelper.GetAppSetting(AppConfigurationCode.applicationID.GetAttributeCode());
                //var enterpriseID = ConfigurationHelper.GetAppSetting(AppConfigurationCode.enterpriseID.GetAttributeCode());

                //user = await this.Repository.GetUser(userName, password);
                result = this.Repository.AuthenticateUser(applicationID, enterpriseID, userName, password);

                if (result != null)
                {
                    user.UserId = result.userid;
                    user.UserName = result.user_name;
                    user.Email = result.email;
                    user.PayerId = result.payerid;
                    user.Department_Name = result.department_name;
                    user.UserTypeId = result.user_type;
                }              
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                Tracer.TraceError(ex);
                throw;
            }
            finally
            {
                Tracer.TraceOut(user);
            }

            return user;
        }
    }
}