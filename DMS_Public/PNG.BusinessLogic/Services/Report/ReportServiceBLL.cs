﻿using PNG.BusinessLogic.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PNG.Common.Enums;
using PNG.Model.Models;
using PNG.Common.Helpers;
using System.IO;
using PNG.Common.Services.Logging;
using PNG.Common.Services.Tracing;
using PNG.DataAccess.Services;
using System.Web;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System.Globalization;
using PNG.Model;
using PNG.Dapper.Extensions.Utils;
using PNG.Common.Enums.Common;
using System.Collections;
using BarcodeLib;
using System.Drawing;

namespace PNG.BusinessLogic.Services
{
    public class ReportServiceBLL : IReportService
    {
        protected readonly DataAccess.Interfaces.IReportService _repository = null;
        protected readonly DataAccess.Interfaces.ISearchConsignmentService _SearchConsignment = null;

        public ReportServiceBLL()
        {
            this._repository = new ReportServiceDAL();
        }
        public async Task<List<FileResult>> GetShipmentTrackingReport(SearchShipmentTrackingReport parameters)
        {
            Tracer.TraceIn();
            List<FileResult> exportFiles = new List<FileResult>();
            string fileName = string.Empty;
            string reportdirectory = string.Empty;
            string reportpath = string.Empty;
            string reportfile = string.Empty;

            System.Globalization.CultureInfo culture = new System.Globalization.CultureInfo("en-US");

            try
            {
                var datasources = new List<ReportDataSource>();

                //Get data in table
                var customers = new List<Customer>();
                var enterprise = new List<Enterprise>();
                var pickupRequest = new List<Pickup_Request>();
                var shipment = new List<Shipment>();
                var zipcode = new List<Zipcode>();
                var vPersonCharge = new List<V_PersonCharge>();

                //Set crystal report data
                datasources.Add(new ReportDataSource()
                {
                    SubReportName = string.Empty,
                    TableName = "Customer",
                    DataSource = customers,
                    DataSourceAsTable = customers.AsDataTable()
                });

                datasources.Add(new ReportDataSource()
                {
                    SubReportName = string.Empty,
                    TableName = "Enterprise",
                    DataSource = enterprise,
                    DataSourceAsTable = enterprise.AsDataTable()
                });

                datasources.Add(new ReportDataSource()
                {
                    SubReportName = string.Empty,
                    TableName = "Pickup_Request",
                    DataSource = pickupRequest,
                    DataSourceAsTable = pickupRequest.AsDataTable()
                });

                datasources.Add(new ReportDataSource()
                {
                    SubReportName = string.Empty,
                    TableName = "Shipment",
                    DataSource = shipment,
                    DataSourceAsTable = shipment.AsDataTable()
                });

                datasources.Add(new ReportDataSource()
                {
                    SubReportName = string.Empty,
                    TableName = "Zipcode",
                    DataSource = zipcode,
                    DataSourceAsTable = zipcode.AsDataTable()
                });

                datasources.Add(new ReportDataSource()
                {
                    SubReportName = string.Empty,
                    TableName = "V_PersonCharge",
                    DataSource = vPersonCharge,
                    DataSourceAsTable = vPersonCharge.AsDataTable()
                });

                var DocumentName = "ShipmentTracking";

                // ReportFileName
                reportpath = ConfigurationHelper.GetAppSetting("reportFileLocation");
                //reportfile = "ShipmentTrackingReport3PA.rpt";
                reportfile = parameters.reportName.Trim();
                reportdirectory = DirectoryFileHelper.GetFullFileName(reportpath, reportfile);

                //ReportPath
                var reportFileName = string.Format("~/FileReports/{0}_{1}.pdf", DocumentName, DateTime.Now.ToString("yyyyMMddHHmm", culture));

                var fullFilePath = DirectoryFileHelper.GetFullPath(reportFileName);
                exportFiles = await ExportReportResult(reportdirectory, datasources, parameters, ReportFormat.PDF, ReportModule.ShipmentTracking, fullFilePath);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                Tracer.TraceError(ex);
                throw;
            }
            finally
            {
                Tracer.TraceOut(exportFiles);
            }

            return exportFiles;
        }

        private void SetLogonInfo(ref ReportDocument reportDocument)
        {
            //create a structure object to store connection details
            //ConnectionDetails conDet = new ConnectionDetails();

            string strAppID = ConfigurationHelper.GetAppSetting(AppConfigurationCode.applicationID.GetAttributeCode());
            string strEnterpriseID = ConfigurationHelper.GetAppSetting(AppConfigurationCode.enterpriseID.GetAttributeCode());
            string connectionString = this._repository.getConnectionString(strAppID, strEnterpriseID);
            //string connectionString = this._searchconsignment.getConnectionString();
            string[] connectionStringArray = connectionString.Split(';');
            // string[] connectionStringArrayTemp = new string[connectionStringArray.Length];

            SearchConsignmentConnectionstring connection = new SearchConsignmentConnectionstring();


            for (int j = 0; j < connectionStringArray.Length; j++)
            {
                string[] connectionStringArrayTemp = connectionStringArray[j].Split('=');
               
                switch (connectionStringArrayTemp[0].ToLower())
                {
                    case "data source": connection.datasource = connectionStringArrayTemp[1]; break;
                    case "initial catalog": connection.initialCatalog = connectionStringArrayTemp[1]; break;
                    case "persist security info": connection.persistSecurity = connectionStringArrayTemp[1]; break;
                    case "user id": connection.userId = connectionStringArrayTemp[1]; break;
                    case "password": connection.password = connectionStringArrayTemp[1]; break;
                    case "packet size": connection.packetSize = connectionStringArrayTemp[1]; break;
                }
            }

            //get connection details from dbcon manager
            //conDet = DbConnectionManager.GetConnectionDetails(strAppID, strEnterpriseID);

            TableLogOnInfo MyLogin;
            //get the no of tables in the main report and 
            //assign connection details to all
            int intTabCnt = reportDocument.Database.Tables.Count, i = 0;
            for (i = 0; i < intTabCnt; i++)
            {
                MyLogin = reportDocument.Database.Tables[i].LogOnInfo;
                if (!string.IsNullOrEmpty(connection.initialCatalog))
                {
                    MyLogin.ConnectionInfo.DatabaseName = connection.initialCatalog;
                    MyLogin.ConnectionInfo.ServerName = connection.datasource;
                    MyLogin.ConnectionInfo.UserID = connection.userId;
                    MyLogin.ConnectionInfo.Password = connection.password;
                    reportDocument.Database.Tables[i].ApplyLogOnInfo(MyLogin);
                }
                //if (!string.IsNullOrEmpty("DMS-R3-PNGAF-LIVE"))
                //{
                //    MyLogin.ConnectionInfo.DatabaseName = "DMS-R3-PNGAF-LIVE";
                //    MyLogin.ConnectionInfo.ServerName = "172.16.48.34";
                //    MyLogin.ConnectionInfo.UserID = "sa";
                //    MyLogin.ConnectionInfo.Password = "Aware@2019";
                //    reportDocument.Database.Tables[i].ApplyLogOnInfo(MyLogin);
                //}

                reportDocument.Database.Tables[i].Location = reportDocument.Database.Tables[i].Name.ToUpper();
                //reportDocument.Database.Tables[i].Location = "CSS_PrintConsNotes";
            }
        }

        private int LastDayOfMonth(String strMonth, String strYear)
        {
            int intLastDay = 0;
            switch (strMonth)
            {
                case "01":
                case "03":
                case "05":
                case "07":
                case "08":
                case "10":
                case "12":
                    intLastDay = 31;
                    break;
                case "04":
                case "06":
                case "09":
                case "11":
                    intLastDay = 30;
                    break;
                case "02":
                    //intLastDay = 28;
                    //by ching 19/02/2008
                    DateTime dt = System.DateTime.ParseExact("01/02/" + strYear, "dd/MM/yyyy", null);
                    DateTime firstDayOfThisMonth = dt.Subtract(TimeSpan.FromDays(dt.Day - 1));
                    DateTime firstDayOfNextMonth = firstDayOfThisMonth.AddMonths(1);
                    DateTime lastDayOfThisMonth = firstDayOfNextMonth.Subtract(TimeSpan.FromDays(1));

                    intLastDay = Int32.Parse(lastDayOfThisMonth.Day.ToString());
                    break;
            }
            return intLastDay;
        }

        private void SetShipTrackParameter(ref ReportDocument reportDocument, SearchShipmentTrackingReport parameters)
        {
            //declare instance for parameterfields class
            ParameterValues paramvals = new ParameterValues();
            ParameterFieldDefinitions paramFlds;
            ParameterDiscreteValue paramDiscVal = new ParameterDiscreteValue();
            ParameterRangeValue paramRangeVal = new ParameterRangeValue();

            //getting parameter field values
            string strPayerId = "*", strPayerType = "";
            string strDateType = "";
            string strRouteCode = "*", strRouteType = "";
            string strZipCode = "*", strStateCode = "*";
            string strorigin_dc = "*", strdestination_dc = "*";
            string strdelPathorigin_dc = "*", strdelPathdestination_dc = "*";
            string strBookingType = "*";
            string strConsgNo = "*", strStatusCode = "*", strExcepCode = "*", strShipInvoiced = "*";
            long lBookingNo = 0;
            DateTime dtStartDate = parameters.PeriodFromFormat;
            DateTime dtEndDate = parameters.PeriodToFormat;

            if (parameters.month != null && parameters.year != null)
            {
                if (parameters.month.Length == 1)
                {
                    parameters.month = "0" + parameters.month;
                }
                dtStartDate = DateTime.ParseExact("01/" + parameters.month + "/" + parameters.year, "dd/MM/yyyy", null);
                int intLastDay = 0;
                intLastDay = LastDayOfMonth(parameters.month, parameters.year);
                dtEndDate = DateTime.ParseExact(intLastDay.ToString() + "/" + parameters.month + "/" + parameters.year + " 23:59", "dd/MM/yyyy HH:mm", null);

            }

            var payerType = "";
            var bookingNO = "";
            if (parameters.payer_type != null)
            {
                payerType = String.Join(",", parameters.payer_type);
            }

            //if (parameters.booking_no != null)
            //{
            //    bookingNO = parameters.booking_no;
            //}
            //else
            //{
            //    bookingNO = "";
            //}




            string strAppID = ConfigurationHelper.GetAppSetting(AppConfigurationCode.applicationID.GetAttributeCode());//utility.GetAppID();
            string strEnterpriseID = ConfigurationHelper.GetAppSetting(AppConfigurationCode.enterpriseID.GetAttributeCode());//utility.GetEnterpriseID();
            string strUserId = parameters.userId;//utility.GetUserID();

            //reportDocument.SetParameterValue("@param_name", the_value);

            //get all parameter fields
            paramFlds = reportDocument.DataDefinition.ParameterFields;

            //passing parameter for parameter fields
            foreach (ParameterFieldDefinition paramFld in paramFlds)
            {
                switch (paramFld.ParameterFieldName)
                {
                    case "p_appid":
                        paramDiscVal.Value = strAppID;
                        break;
                    case "p_enterprise":
                        paramDiscVal.Value = strEnterpriseID;
                        break;
                    case "p_consignment_no":
                        //paramDiscVal.Value = parameters.consignment_no != null ? parameters.consignment_no: strConsgNo;
                        paramDiscVal.Value = "";
                        break;
                    case "p_booking_no":
                        paramDiscVal.Value = parameters.booking_no != null ? Int32.Parse(parameters.booking_no) : lBookingNo;
                       // paramDiscVal.Value = bookingNO;
                        break;
                    case "p_payer_type":
                        paramDiscVal.Value = parameters.payer_type != null ? "(" + payerType + ")" : strPayerType;
                        break;
                    case "p_payerid":
                        paramDiscVal.Value = parameters.payer_code != null ? parameters.payer_code : "";
                        break;
                    case "p_date_type":
                        paramDiscVal.Value = parameters.tran_date != null ? parameters.tran_date : "";
                        break;
                    case "p_date_range":
                        paramRangeVal.StartValue = dtStartDate;
                        paramRangeVal.EndValue = dtEndDate;
                        break;
                    case "p_route_code":
                        paramDiscVal.Value = parameters.route_code != null ? parameters.route_code : "";
                        break;
                    case "p_zipcode":
                        paramDiscVal.Value = parameters.zip_code != null ? parameters.zip_code : "";
                        break;
                    case "p_state_code":
                        paramDiscVal.Value = parameters.state_code != null ? parameters.state_code : "";
                        break;
                    case "p_last_status_code":
                        paramDiscVal.Value = parameters.status_code != null ? parameters.status_code : "";
                        break;
                    case "p_last_exception_code":
                        paramDiscVal.Value = parameters.exception_code != null ? parameters.exception_code : "";
                        break;
                    case "p_invoice_flag":
                        paramDiscVal.Value = parameters.shipment_invoiced != null ? parameters.shipment_invoiced : "";
                        break;
                    case "p_userid":
                        paramDiscVal.Value = parameters.userId != null ? parameters.userId : strUserId;
                        break;
                    case "p_origin_dc":
                        paramDiscVal.Value = parameters.origin_dc != null ? parameters.origin_dc : "";
                        break;
                    case "p_destination_dc":
                        paramDiscVal.Value = parameters.destination_dc != null ? parameters.destination_dc : "";
                        break;
                    case "p_route_type":
                        paramDiscVal.Value = parameters.route_type != null ? parameters.route_type : strRouteType;
                        break;
                    case "p_delPath_origin_dc":
                        paramDiscVal.Value = parameters.delPathOriginDC != null ? parameters.delPathOriginDC : "";
                        break;
                    case "p_delPath_destination_dc":
                        paramDiscVal.Value = parameters.delPathDestinationDC != null ? parameters.delPathDestinationDC : "";
                        break;
                    case "p_bookingtype":
                        paramDiscVal.Value = parameters.booking_type != null ? parameters.booking_type : "*";
                        break;
                    default:
                        continue;
                }
                paramvals = paramFld.CurrentValues;
                if (paramFld.DiscreteOrRangeKind == DiscreteOrRangeKind.RangeValue)
                    paramvals.Add(paramRangeVal);
                else
                    paramvals.Add(paramDiscVal);
                paramFld.ApplyCurrentValues(paramvals);
            }
        }

        public async Task<List<FileResult>> GetCSS_PrintConsNoteDetail(PrintConsNoteResult printConsNoteDetail)
        {
            Tracer.TraceIn();
            List<FileResult> exportFiles = new List<FileResult>();
            string fileName = string.Empty;
            string reportdirectory = string.Empty;
            string reportpath = string.Empty;
            string reportfile = string.Empty;
            string appId = ConfigurationHelper.GetAppSetting(AppConfigurationCode.applicationID.GetAttributeCode());
            string enterpriseId = ConfigurationHelper.GetAppSetting(AppConfigurationCode.enterpriseID.GetAttributeCode());

            System.Globalization.CultureInfo culture = new System.Globalization.CultureInfo("en-US");
            Log.Info("printConsNoteDetail.paramDetail.Count: " + printConsNoteDetail.paramDetail.Count);
            for (int i = 0; i < printConsNoteDetail.paramDetail.Count; i++)
            {
                string Barcodes = "*" + printConsNoteDetail.paramDetail[i].consignment_no.ToString().ToUpper() + "*";
                Log.Info("Barcodes: " + Barcodes);
                Barcode bcd = new Barcode();
                bcd.Alignment = BarcodeLib.AlignmentPositions.CENTER;
                bcd.IncludeLabel = false;
                bcd.RotateFlipType = RotateFlipType.RotateNoneFlipNone;
                bcd.Encode(TYPE.CODE39, Barcodes, System.Drawing.Color.Black, System.Drawing.Color.White, 240, 65);
                Log.Info("Barcodes: " + Barcodes);
                MemoryStream ms = new MemoryStream();
                bcd.SaveImage(ms, BarcodeLib.SaveTypes.PNG);

                Byte[] oByte = ms.ToArray();
                printConsNoteDetail.paramDetail[i].imgBarcode = oByte;
            }

           

            try
            {
                IEnumerable<PrintConsNoteDetail> result = printConsNoteDetail.paramDetail;
                var datasources = new List<ReportDataSource>();
                var datasource = result;

                PrintConsNoteParam param = new PrintConsNoteParam();
                param.enterpriseId = enterpriseId;
                param.userloggedIn = printConsNoteDetail.paramFilter.userloggedIn;
                param.consignmentList = printConsNoteDetail.paramFilter.consignmentList;



                if (datasource.Any())
                {
                    var subSubDataTable = datasource.AsDataTable();
                    datasources.Add(new ReportDataSource()
                    {
                        SubReportName = string.Empty,
                        DataSource = datasource,
                        TableName = "CSS_PrintConsNotes",
                        DataSourceAsTable = subSubDataTable
                    });

                    var DocumentName = "PrintConsignmentNote";

                    // ReportFileName
                    reportpath = ConfigurationHelper.GetAppSetting("reportFileLocation");
                    reportfile = "PreprintConsignmentTNT2.rpt"; 
                    //reportfile = "CrystalReport1.rpt";
                    reportdirectory = DirectoryFileHelper.GetFullFileName(reportpath, reportfile);

                    //ReportPath
                    var reportFileName = string.Format("~/FileReports/{0}_{1}.pdf", DocumentName, DateTime.Now.ToString("yyyyMMddHHmm", culture));

                    var fullFilePath = DirectoryFileHelper.GetFullPath(reportFileName);
                    exportFiles = await ExportReportResultConsNote(reportdirectory, datasources, param, ReportFormat.PDF, ReportModule.ConsignmentNote, fullFilePath);
                }
                                
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                Tracer.TraceError(ex);
                throw;
            }
            finally
            {
                Tracer.TraceOut(exportFiles);
            }

            return exportFiles;
        }

        public async Task<List<FileResult>> GetCSS_PrintShippingListDetail(PrintShippingListDetail[] printShippingListDetail)
        {
            Log.Info("Testtttt");
            Tracer.TraceIn();
            List<FileResult> exportFiles = new List<FileResult>();
            string fileName = string.Empty;
            string reportdirectory = string.Empty;
            string reportpath = string.Empty;
            string reportfile = string.Empty;
            string appId = ConfigurationHelper.GetAppSetting(AppConfigurationCode.applicationID.GetAttributeCode());
            string enterpriseId = ConfigurationHelper.GetAppSetting(AppConfigurationCode.enterpriseID.GetAttributeCode());

            System.Globalization.CultureInfo culture = new System.Globalization.CultureInfo("en-US");

            string Barcodes = "*" + printShippingListDetail[0].ShippingList_No.ToString().ToUpper() + "*";
            Log.Info("Barcodes: " + Barcodes);
            Barcode bcd = new Barcode();
            bcd.Alignment = BarcodeLib.AlignmentPositions.LEFT;
            bcd.IncludeLabel = false;
            bcd.RotateFlipType = RotateFlipType.RotateNoneFlipNone;
            bcd.Encode(TYPE.CODE39, Barcodes, System.Drawing.Color.Black, System.Drawing.Color.White, 265, 65);
            Log.Info("Barcodes: " + Barcodes);
            MemoryStream ms = new MemoryStream();
            bcd.SaveImage(ms, BarcodeLib.SaveTypes.PNG);

            Byte[] oByte = ms.ToArray();
            printShippingListDetail[0].imgBarcode = oByte;

            try
            {
                
                IEnumerable<PrintShippingListDetail> result = printShippingListDetail;
                var datasources = new List<ReportDataSource>();
                var datasource = result;

                Log.Info("printShippingListDetail: " + printShippingListDetail.ToString());

                if (datasource.Any())
                {
                    
                    var subSubDataTable = datasource.AsDataTable();
                    datasources.Add(new ReportDataSource()
                    {
                        SubReportName = string.Empty,
                        DataSource = datasource,
                        TableName = "CSS_PrintShippingList",
                        DataSourceAsTable = subSubDataTable
                    });

                    var DocumentName = "PrintShipping";

                    // ReportFileName
                    reportpath = ConfigurationHelper.GetAppSetting("reportFileLocation");
                    reportfile = "PreprintShippingTNT.rpt";
                    //reportfile = "CrystalReport1.rpt";
                    reportdirectory = DirectoryFileHelper.GetFullFileName(reportpath, reportfile);

                    Log.Info("reportdirectory: " + reportdirectory.ToString());

                    //ReportPath
                    var reportFileName = string.Format("~/FileReports/{0}_{1}.pdf", DocumentName, DateTime.Now.ToString("yyyyMMddHHmm", culture));
                    Log.Info("reportFileName: " + reportFileName.ToString());

                    var fullFilePath = DirectoryFileHelper.GetFullPath(reportFileName);
                    Log.Info("fullFilePath: " + fullFilePath.ToString());

                    exportFiles = await ExportReportResultConsNote(reportdirectory, datasources, null, ReportFormat.PDF, ReportModule.ShippingList, fullFilePath);
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex);
                Tracer.TraceError(ex);
                throw;
            }
            finally
            {
                Tracer.TraceOut(exportFiles);
            }

            return exportFiles;
        }

        public async Task<List<FileResult>> GetCSS_ReprintConsNoteDetail(ReprintConsNoteResult reprintConsNoteDetail)
        {
            Tracer.TraceIn();
            List<FileResult> exportFiles = new List<FileResult>();
            string fileName = string.Empty;
            string reportdirectory = string.Empty;
            string reportpath = string.Empty;
            string reportfile = string.Empty;
            string appId = ConfigurationHelper.GetAppSetting(AppConfigurationCode.applicationID.GetAttributeCode());
            string enterpriseId = ConfigurationHelper.GetAppSetting(AppConfigurationCode.enterpriseID.GetAttributeCode());

            System.Globalization.CultureInfo culture = new System.Globalization.CultureInfo("en-US");

            try
            {

                IEnumerable<ReprintConsNoteDetail> result = reprintConsNoteDetail.paramDetail;
                var datasources = new List<ReportDataSource>();
                var datasource = result;
                //IEnumerable< ReprintConsNoteDetail> datasource = result as IEnumerable<ReprintConsNoteDetail>;

                string Barcodes = "*" + reprintConsNoteDetail.paramDetail[0].consignment_no.ToString().ToUpper() + "*";
                Log.Info("Barcodes: "+ Barcodes);
                Barcode bcd = new Barcode();
                bcd.Alignment = BarcodeLib.AlignmentPositions.CENTER;
                bcd.IncludeLabel = false;
                bcd.RotateFlipType = RotateFlipType.RotateNoneFlipNone;
                bcd.Encode(TYPE.CODE39, Barcodes, System.Drawing.Color.Black, System.Drawing.Color.White, 240, 65);
                Log.Info("Barcodes: " + Barcodes);
                MemoryStream ms = new MemoryStream();
                bcd.SaveImage(ms, BarcodeLib.SaveTypes.PNG);

                Byte[] oByte = ms.ToArray();
                reprintConsNoteDetail.paramDetail[0].imgBarcode = oByte;

                if (datasource.Any())
                {
                    var subSubDataTable = datasource.AsDataTable();
                    datasources.Add(new ReportDataSource()
                    {
                        SubReportName = string.Empty,
                        DataSource = datasource,
                        TableName = "CSS_PrintConsNotes",
                        DataSourceAsTable = subSubDataTable
                    });
                   
                    Log.Info(datasources.ToString());
                    var DocumentName = "PrintConsignmentNote";


                    // ReportFileName
                    reportpath = ConfigurationHelper.GetAppSetting("reportFileLocation");
                    reportfile = "PreprintConsignmentTNT2.rpt";  /*PreprintConsignmentTNT2*/
                     //reportfile = "CrystalReport1.rpt";
                     reportdirectory = DirectoryFileHelper.GetFullFileName(reportpath, reportfile);

                    //ReportPath
                    var reportFileName = string.Format("~/FileReports/{0}_{1}.pdf", DocumentName, DateTime.Now.ToString("yyyyMMddHHmm", culture));

                    var fullFilePath = DirectoryFileHelper.GetFullPath(reportFileName);
                    exportFiles = await ExportReportResultConsNote(reportdirectory, datasources, null, ReportFormat.PDF, ReportModule.ReprintConsNote, fullFilePath);
                }
                Log.Info("test");
                Log.Info(reprintConsNoteDetail.paramDetail.ToString());
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                Tracer.TraceError(ex);
                throw;
            }
            finally
            {
                Tracer.TraceOut(exportFiles);
            }

            return exportFiles;
        }

        public string TestReport()
        {
            Tracer.TraceIn();
            List<FileResult> exportFiles = new List<FileResult>();
            string fileName = string.Empty;
            string reportdirectory = string.Empty;
            string testt = string.Empty;
            string reportpath = string.Empty;
            string reportfile = string.Empty;
            string appId = ConfigurationHelper.GetAppSetting(AppConfigurationCode.applicationID.GetAttributeCode());
            string enterpriseId = ConfigurationHelper.GetAppSetting(AppConfigurationCode.enterpriseID.GetAttributeCode());

            System.Globalization.CultureInfo culture = new System.Globalization.CultureInfo("en-US");

            try
            {

 
                var datasources = new List<ReportDataSource>();


                var DocumentName = "PrintConsignmentNote";

                    // ReportFileName
                   // reportpath = ConfigurationHelper.GetAppSetting("reportFileLocation");
                    reportfile = "CrystalReport1.rpt";  /*PreprintConsignmentTNT2*/
                                                        //reportfile = "CrystalReport1.rpt";
                //var name2 = "C:\\inetpub\\wwwroot\\DMS_Public.Api\\Reports";
                ////reportdirectory = DirectoryFileHelper.GetFullFileName(reportpath, reportfile
                //reportdirectory = DirectoryFileHelper.GetFullFileName(name2, reportfile);
                //var name = "D:\\workspace\\PNG.GIT\\PNG.WebApi\\Reports\\";

                ReportDocument rpt = new ReportDocument();
                rpt.Load(Path.Combine(HttpContext.Current.Server.MapPath("~/Reports"), "CrystalReport1.rpt")); 


                //ReportPath
                var reportFileName = string.Format("~/FileReports/{0}_{1}.pdf", DocumentName, DateTime.Now.ToString("yyyyMMddHHmm", culture));

                var fullFilePath = DirectoryFileHelper.GetFullPath(reportFileName);
                var reportDocument = new ReportDocument();
                testt = DirectoryFileHelper.GetFullPath(reportFileName);
                //// Load Report
                //reportDocument.Load(reportdirectory, OpenReportMethod.OpenReportByTempCopy);
                //reportDocument.Refresh();

                var pdfFile = ExportPDFFileResult(reportDocument, fullFilePath);
                var fileResults = new List<FileResult>();
                fileResults.Add(pdfFile);

                //exportFiles = ExportReportResultConsNote(reportdirectory, datasources, null, ReportFormat.PDF, ReportModule.ReprintConsNote, fullFilePath);


                Log.Info("test");
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                Tracer.TraceError(ex);
                return ex.ToString();
            }
            finally
            {
                Tracer.TraceOut(exportFiles);
            }

            return testt;
        }

        public async Task<List<FileResult>> GetCSS_ReprintConsNoteLabelDetail(ReprintConsNoteResult reprintConsNoteParamlabel)
        {
            Tracer.TraceIn();
            List<FileResult> exportFiles = new List<FileResult>();
            string fileName = string.Empty;
            string reportdirectory = string.Empty;
            string reportpath = string.Empty;
            string reportfile = string.Empty;
            string appId = ConfigurationHelper.GetAppSetting(AppConfigurationCode.applicationID.GetAttributeCode());
            string enterpriseId = ConfigurationHelper.GetAppSetting(AppConfigurationCode.enterpriseID.GetAttributeCode());

            System.Globalization.CultureInfo culture = new System.Globalization.CultureInfo("en-US");

            string Barcodes = "*" + reprintConsNoteParamlabel.paramDetail[0].consignment_no.ToString().ToUpper() + "*";
            Log.Info("Barcodes: " + Barcodes);
            Barcode bcd = new Barcode();
            bcd.Alignment = BarcodeLib.AlignmentPositions.CENTER;
            bcd.IncludeLabel = false;
            bcd.RotateFlipType = RotateFlipType.RotateNoneFlipNone;
            bcd.Encode(TYPE.CODE39, Barcodes, System.Drawing.Color.Black, System.Drawing.Color.White, 240, 65);
            Log.Info("Barcodes: " + Barcodes);
            MemoryStream ms = new MemoryStream();
            bcd.SaveImage(ms, BarcodeLib.SaveTypes.PNG);

            Byte[] oByte = ms.ToArray();
            reprintConsNoteParamlabel.paramDetail[0].imgBarcode = oByte;

            try
            {

                IEnumerable<ReprintConsNoteDetail> result = reprintConsNoteParamlabel.paramDetail;
                var datasources = new List<ReportDataSource>();
                var datasource = result;
                //IEnumerable< ReprintConsNoteDetail> datasource = result as IEnumerable<ReprintConsNoteDetail>;


                if (datasource.Any())
                {
                    var subSubDataTable = datasource.AsDataTable();
                    datasources.Add(new ReportDataSource()
                    {
                        SubReportName = string.Empty,
                        DataSource = datasource,
                        TableName = "CSS_PrintConsNotes",
                        DataSourceAsTable = subSubDataTable
                    });

                    var DocumentName = "PrintConsignmentNote";

                    // ReportFileName
                    reportpath = ConfigurationHelper.GetAppSetting("reportFileLocation");
                    reportfile = "PreprintConsignmentTNT3.rpt";  /*PreprintConsignmentTNT4*/
                                                                 //reportfile = "CrystalReport1.rpt";
                    reportdirectory = DirectoryFileHelper.GetFullFileName(reportpath, reportfile);

                    //ReportPath
                    var reportFileName = string.Format("~/FileReports/{0}_{1}.pdf", DocumentName, DateTime.Now.ToString("yyyyMMddHHmm", culture));

                    var fullFilePath = DirectoryFileHelper.GetFullPath(reportFileName);
                    exportFiles = await ExportReportResultConsNote(reportdirectory, datasources, null, ReportFormat.PDF, ReportModule.ReprintConsNoteLabel, fullFilePath);
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex);
                Tracer.TraceError(ex);
                throw;
            }
            finally
            {
                Tracer.TraceOut(exportFiles);
            }

            return exportFiles;
        }

        //Share Function
        private Task<List<FileResult>> ExportReportResult(string reportDocumentPath, IEnumerable<ReportDataSource> reportDataSource, SearchShipmentTrackingReport parameters, ReportFormat reportFormat, ReportModule reportModule, string reportPath = null)
        {
            var fileResults = new List<FileResult>();

            if (string.IsNullOrEmpty(reportDocumentPath))
            {
                throw new ArgumentNullException("reportDocumentPath");
            }

            if (!reportDataSource.Any())
            {
                throw new ArgumentNullException("reportDataSource");
            }

            return Task.Run(() =>
            {
                var reportDocument = new ReportDocument();

                // Load Report
                reportDocument.Load(reportDocumentPath, OpenReportMethod.OpenReportByTempCopy);
                reportDocument.Refresh();

                // Set Report Data Source
                reportDataSource.ForEach(source =>
                {
                    if (string.IsNullOrEmpty(source.SubReportName))
                    {
                        //reportDocument.Database.Tables[source.TableName].SetDataSource(source.DataSourceAsTable);
                    }
                    else
                    {
                        reportDocument.OpenSubreport(source.SubReportName).SetDataSource(source.DataSourceAsTable);
                    }
                });

                //Set parameter value
                switch (reportModule)
                {
                    case ReportModule.ShipmentTracking:
                        SetLogonInfo(ref reportDocument);
                        SetShipTrackParameter(ref reportDocument, parameters);
                        break;
                }

                var pdfFile = ExportPDFFileResult(reportDocument, reportPath);
                fileResults.Add(pdfFile);

                return fileResults;
            });
        }

        private Task<List<FileResult>> ExportReportResultConsNote(string reportDocumentPath, IEnumerable<ReportDataSource> reportDataSource, PrintConsNoteParam parameters, ReportFormat reportFormat, ReportModule reportModule, string reportPath = null)
        {
            var fileResults = new List<FileResult>();

            if (string.IsNullOrEmpty(reportDocumentPath))
            {
                throw new ArgumentNullException("reportDocumentPath");
            }

            if (!reportDataSource.Any())
            {
                throw new ArgumentNullException("reportDataSource");
            }

            return Task.Run(() =>
            {
                var reportDocument = new ReportDocument();

                // Load Report
                reportDocument.Load(reportDocumentPath, OpenReportMethod.OpenReportByTempCopy);
                reportDocument.Refresh();

                // Set Report Data Source
                reportDataSource.ForEach(source =>
                {
                    if (string.IsNullOrEmpty(source.SubReportName))
                    {
                        // reportDocument.Database.Tables[source.TableName].SetDataSource(source.DataSource);
                        //if (source.TableName == "CSS_ReprintConsNote")
                        //{
                        //    reportDocument.SetDataSource(source.DataSource);
                        //}
                        //else
                        //{
                        //    reportDocument.SetDataSource(source.DataSourceAsTable);
                        //}
                        //if (source.DataSourceAsTable.TableName == "CSS_PrintConsNotes1")
                        //{
                        //    source.DataSourceAsTable.TableName = "CSS_PrintConsNotes1";
                        //    reportDocument.SetDataSource(source.DataSourceAsTable);
                        //}
                        //else
                        //{
                        //    reportDocument.SetDataSource(source.DataSourceAsTable);
                        //}
                        //source.DataSourceAsTable.TableName = "CSS_PrintConsNotes1";
                        reportDocument.SetDataSource(source.DataSourceAsTable);

                    }
                    else
                    {
                        reportDocument.OpenSubreport(source.SubReportName).SetDataSource(source.DataSourceAsTable);
                    }
                });

                //Set parameter value
                switch (reportModule)
                {
                    case ReportModule.ConsignmentNote:
                       // SetLogonInfo(ref reportDocument);
                        //setConnoteReport(ref reportDocument, parameters);
                        break;
                    case ReportModule.ShippingList:
                        //SetLogonInfo(ref reportDocument);
                        //setConnoteReport(ref reportDocument, parameters);
                        break; 
                    case ReportModule.ReprintConsNote:
                        //SetLogonInfo(ref reportDocument);
                        //setConnoteReport(ref reportDocument, parameters);
                        break;
                    case ReportModule.ReprintConsNoteLabel:
                       // SetLogonInfo(ref reportDocument);
                        //setConnoteReport(ref reportDocument, parameters);
                        break;

                }

                var pdfFile = ExportPDFFileResult(reportDocument, reportPath);
                fileResults.Add(pdfFile);

                return fileResults;
            });
        }

        private void setConnoteReport(ref ReportDocument reportDocument, PrintConsNoteParam parameters)
        {
            reportDocument.SetParameterValue("@enterpriseid", parameters.enterpriseId);
            reportDocument.SetParameterValue("@userloggedin", parameters.userloggedIn);
            reportDocument.SetParameterValue("@ConsignmentsList", parameters.consignmentList);
        }

        private FileResult ExportPDFFileResult(ReportDocument reportDocument, string fullFilePath)
        {
            if (string.IsNullOrEmpty(fullFilePath))
            {
                var fileName = string.Format("~/FileReports/PDF_EXP_{0}.pdf", DateTime.Now.ToString("yyyyMMddHHmmss",
                    CultureInfo.InvariantCulture));

                fullFilePath = DirectoryFileHelper.GetFullPath(fileName);
            }
            Log.Info("fullFilePath: " + fullFilePath.ToString());

            reportDocument.ExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
            reportDocument.ExportToDisk(ExportFormatType.PortableDocFormat, fullFilePath);
            //reportDocument.ExportToDisk(ExportFormatType.PortableDocFormat, "D:\\test.pdf");
            reportDocument.Dispose();
            reportDocument.Close();

            var file = new FileInfo(fullFilePath);

            if (file.Exists)
            {
                var fileStream = File.ReadAllBytes(file.FullName);

                var pdfFile = new FileResult()
                {
                    Base64StringData = Convert.ToBase64String(fileStream),
                    ContentType = "application/octet-stream",
                    FileName = Path.GetFileName(file.FullName)
                };

                return pdfFile;
            }

            return null;
        }


    }
}
