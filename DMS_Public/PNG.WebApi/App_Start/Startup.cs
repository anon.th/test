﻿using System.Linq;
using System.Net.Http.Formatting;
using System.Web;
using System.Web.Http;
using log4net.Config;
using Microsoft.Owin;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Owin;
using PNG.Common.Interfaces.Logging;
using PNG.Common.Services.Logging;
using PNG.WebApi.Handlers;
using SimpleInjector;
using SimpleInjector.Integration.WebApi;
using SimpleInjector.Lifestyles;
using PNG.DataAccess.Database;
using PNG.BusinessLogic.Interfaces;
using PNG.BusinessLogic.Services;
using System.Web.Mvc;
using PNG.WebApi.Models;
using System.Net.Http.Headers;
using Microsoft.Owin.Security.OAuth;
using System;
using PNG.WebApi.Providers;
using PNG.DataAccess.AspNetIdentities;
using PNG.Common.Enums.Common;
using PNG.Common.Enums;
using System.Configuration;
using Microsoft.Owin.Security.DataHandler.Encoder;
using Microsoft.Owin.Security.Jwt;
using Microsoft.Owin.Security;
using ApplicationDbContext = PNG.DataAccess.AspNetIdentities.ApplicationDbContext;

[assembly: OwinStartup(typeof(PNG.WebApi.App_Start.Startup))]

namespace PNG.WebApi.App_Start
{
    public class Startup
    {
        /// <summary>
        /// 
        /// </summary>
        protected readonly string BaseUrl = Common.Helpers.ConfigurationHelper.GetAppSetting(AppConfigurationCode.baseApi.GetAttributeCode());


        /// <summary>
        /// The MaxTimeoutExpired
        /// </summary>
        protected readonly int MaxTimeoutExpired = Common.Helpers.ConfigurationHelper.GetAppSettingValue<int>(AppConfigurationCode.MaxTimeoutExpired.GetAttributeCode());

        // For more information on configuring authentication, please visit http://go.microsoft.com/fwlink/?LinkId=301864
        public void Configuration(IAppBuilder app)
        {
            var httpConfig = GlobalConfiguration.Configuration;
            httpConfig.IncludeErrorDetailPolicy = IncludeErrorDetailPolicy.Always;

            // Simple Injector configure
            var container = new Container();

            ConfigureOAuthTokenGeneration(app, container);

            ConfigureOAuthTokenConsumption(app);
            // Async Scoped (async / await)
            // There will be only one instance of a given service type within a certain(explicitly defined) scope and that instance will be disposed when the scope ends.
            // This scope will automatically flow with the logical flow of control of asynchronous methods.
            container.Options.DefaultScopedLifestyle = new AsyncScopedLifestyle();

            // Register all services to Simple Injector container
            ConfigureSimpleInjectorInitializeContainer(container);

            // Register logging
            ConfigureLogging(container);

            // Verify Simple Injector
            container.Verify();

            // Simple Injector Resolver
            httpConfig.DependencyResolver = new SimpleInjectorWebApiDependencyResolver(container);

            // Configure Trace helper delegate handler
            httpConfig.MessageHandlers.Add(new TraceHelperHandler());

            ConfigureWebApi(httpConfig);

            app.UseWebApi(httpConfig);

            // To remove X-AspNetMvc-Version
            MvcHandler.DisableMvcResponseHeader = true;

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="container"></param>
        private static void ConfigureLogging(Container container)
        {
            XmlConfigurator.Configure();
            container.Register<ILoggingService>(() => new Log4NetLoggingService());

            var loggingService = container.GetInstance<ILoggingService>();
            loggingService.Configure(HttpContext.Current.Server.MapPath("~/log4net.config"));
            Log.SetLogger(loggingService);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="config"></param>
        private static void ConfigureWebApi(HttpConfiguration config)
        {
            config.MapHttpAttributeRoutes();

            AreaRegistration.RegisterAllAreas();
            var jsonFormatter = config.Formatters.OfType<JsonMediaTypeFormatter>().First();
            jsonFormatter.SerializerSettings.ContractResolver = new DefaultContractResolver();

            GlobalConfiguration.Configuration.Formatters.JsonFormatter.SupportedMediaTypes.Add(new MediaTypeHeaderValue("text/html"));
            GlobalConfiguration.Configuration.Formatters.JsonFormatter.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
            GlobalConfiguration.Configuration.EnsureInitialized();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="container"></param>
        private static void ConfigureSimpleInjectorInitializeContainer(Container container)
        {
            // TODO: Change all Business logic classes remove contructor with IDbFactory
            container.Register<DataAccess.Interfaces.IDbFactory>(() => new DbFactory(), Lifestyle.Transient);

            // Register services
            container.Register<ITestConnectionService, TestConnectionServiceBLL>();
            container.Register<IAppRouteMenuService, AppRouteMenuServiceBLL>();
            container.Register<ITrackandTraceService, TrackandTraceServiceBLL>();
            container.Register<IUserProfileService, UserProfileServiceBLL>();
            container.Register<ILoginService, LoginServiceBLL>();
            container.Register<IUserAuthService, UserAuthServiceBLL>();
            container.Register<IMenuService, MenuServiceBLL>();
            container.Register<ICreateConsignmentService, CreateConsignmentServiceBLL>();
            container.Register<IZipCodeService, ZipCodeServiceBLL>();
            container.Register<IReferencesService, ReferencesServiceBLL>();
            container.Register<ISearchConsignmentService, SearchConsignmentServiceBLL>();
            container.Register<IReportService, ReportServiceBLL>();
            container.Register<IShipmentTrackingService, ShipmentTrackingServiceBLL>();
            container.Register<IPayerCodeService, PayerCodeServiceBLL>();
            //container.Register<IIdentityUserSessionService, IdentityUserSessionServiceBLL>();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="app"></param>
        /// <param name="container"></param>
        private void ConfigureOAuthTokenGeneration(IAppBuilder app, Container container)
        {
            // Configure the db context and user manager to use a single instance per request
            app.CreatePerOwinContext(ApplicationDbContext.Create);
            app.CreatePerOwinContext<ApplicationUserManager>(ApplicationUserManager.Create);
            app.CreatePerOwinContext<ApplicationRoleManager>(ApplicationRoleManager.Create);

            var oAuthServerOptions = new OAuthAuthorizationServerOptions
            {
                //For Dev enviroment only (on production should be AllowInsecureHttp = false)
                AllowInsecureHttp = true,
                TokenEndpointPath = new PathString("/oauth/token"),
                AccessTokenExpireTimeSpan = TimeSpan.FromMinutes(MaxTimeoutExpired),
                Provider = new CustomOAuthProvider(container),
                //Provider = new MyAuthorizationServerProvider(),
                AccessTokenFormat = new CustomJwtFormat(BaseUrl)
            };

            // OAuth 2.0 Bearer Access Token Generation
            app.UseOAuthAuthorizationServer(oAuthServerOptions);
            app.UseOAuthBearerAuthentication(new OAuthBearerAuthenticationOptions());
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="app"></param>
        private void ConfigureOAuthTokenConsumption(IAppBuilder app)
        {
            var issuer = BaseUrl;
            var audienceId = ConfigurationManager.AppSettings["as:AudienceId"];
            var audienceSecret = TextEncodings.Base64Url.Decode(ConfigurationManager.AppSettings["as:AudienceSecret"]);

            // Api controllers with an [Authorize] attribute will be validated with JWT
            app.UseJwtBearerAuthentication(new JwtBearerAuthenticationOptions
            {
                AuthenticationMode = AuthenticationMode.Active,
                AllowedAudiences = new[] { audienceId },
                IssuerSecurityTokenProviders = new IIssuerSecurityTokenProvider[]
                {
                    new SymmetricKeyIssuerSecurityTokenProvider(issuer, audienceSecret)
                }
            });
        }


        //private static void ConfigureCaching(Container container)
        //{
        //    IDistributedCache cache = null;

        //    var enabledDistributedCache = Common.Helpers.ConfigurationHelper.GetAppSettingValue<bool>(AppConfigurationCode.EnabledDistributedCache.GetAttributeCode());

        //    if (enabledDistributedCache == false)
        //    {
        //        cache = new MemoryDistributedCache(new IMemoryCache(new MemoryCacheOptions()));
        //    }
        //    else
        //    {
        //        cache = new SqlServerCache(new SqlServerCacheOptions
        //        {
        //            ConnectionString = Common.Helpers.ConfigurationHelper.GetConnectString(AppConfigurationCode.DistributedCacheConnection.GetAttributeCode()),
        //            SchemaName = Common.Helpers.ConfigurationHelper.GetAppSettingValue<string>(AppConfigurationCode.DistributedCacheSchemaName.GetAttributeCode()),
        //            TableName = Common.Helpers.ConfigurationHelper.GetAppSettingValue<string>(AppConfigurationCode.DistributedCacheTableName.GetAttributeCode())
        //        });
        //    }

        //    container.Register(() => cache, Lifestyle.Singleton);

        //    // Set cache identity user manager in base api controller
        //    container.RegisterInitializer<BaseApiController>(instance =>
        //    {
        //        instance.CacheIdentityUserManager = cache;
        //        instance.CacheAddressViewManager = cache;
        //        instance.CacheUserAccessRightManager = cache;
        //        instance.CacheGeneralManager = cache;
        //        instance.CacheDocumentManager = cache;
        //    });
        //}
    }
}
