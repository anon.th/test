﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PvdAuthorizeAttribute.cs" company="Aware Corporation Limited">
//   Copyright (c) 2014-2015.  All rights reserved.
// </copyright>
// <summary>
//   Defines the PvdAuthorizeAttribute type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using Microsoft.AspNet.Identity;
using Microsoft.Owin;
using PNG.BusinessLogic.Interfaces;
using PNG.Common.Enums;
using PNG.Common.Enums.Common;
using PNG.Common.Services.Logging;
using PNG.BusinessLogic.Services;
using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;

namespace PNG.WebApi.Handlers
{
    /// <summary>
    /// 
    /// </summary>
    public class PNGAuthorizeAttribute : AuthorizeAttribute
    {
        protected readonly int MaxTimeoutExpired = Common.Helpers.ConfigurationHelper.GetAppSettingValue<int>(AppConfigurationCode.MaxTimeoutExpired.GetAttributeCode());
        protected readonly int MaxUserSessionTimeoutExpired = Common.Helpers.ConfigurationHelper.GetAppSettingValue<int>(AppConfigurationCode.MaxUserSessionTimeoutExpired.GetAttributeCode());
        protected readonly int MaxReValidateUserSession = Common.Helpers.ConfigurationHelper.GetAppSettingValue<int>(AppConfigurationCode.MaxReValidateUserSession.GetAttributeCode());

        /// <summary>
        /// 
        /// </summary>
        private static IOwinContext CurrentRequest
        {
            get
            {
                return HttpContext.Current.GetOwinContext();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="actionContext"></param>
        public override void OnAuthorization(HttpActionContext actionContext)
        {
            if (SkipAuthorization(actionContext))
            {
                return;
            }
            //HttpContext.Current.Request.Params[s]
            //if (!HttpContext.Current.User.Identity.IsAuthenticated)
            //{
            //    actionContext.Response = actionContext.ControllerContext.Request.CreateErrorResponse(HttpStatusCode.Unauthorized,
            //        "Session token expired or not valid.",
            //        new Exception("Session token expired or not valid."));
            //    Log.Info("Session token expired or not valid with Identity.IsAuthenticated is false.");
            //    return;
            //}

            IIdentityUserSessionService userSessionService = null;

            try
            {
                //var cacheIdentityUserManager = GlobalConfiguration.Configuration.DependencyResolver.GetService(typeof(IDistributedCache)) as IDistributedCache;
                //var currentGuid = Infrastructures.ExtendedClaimsProvider.GetClaimValue<string>(UserClaim.CurrentGuid.GetAttributeCode());
                //var identityUserCache = CacheExtensions.ByteArrayToObject<IdentityUserCache>(cacheIdentityUserManager.Get(currentGuid));

                //if (identityUserCache == null)
                //{
                //    actionContext.Response = actionContext.ControllerContext.Request.CreateErrorResponse(HttpStatusCode.Unauthorized,
                //        "Session token expired or not valid.",
                //        new Exception("Session token expired or not valid."));

                //    Log.Info("Session token expired or not valid with identityUserCache is null.");
                //    return;
                //}

                //var timeSpan = DateTime.Now.Subtract(identityUserCache.LastRequest);

                //if (timeSpan.TotalMinutes > this.MaxReValidateUserSession)
                //{
                //    // update last request and update to identity user cache
                //    identityUserCache.LastRequest = DateTime.Now;
                //    cacheIdentityUserManager.Set(currentGuid, CacheExtensions.ObjectToByteArray(identityUserCache),
                //        new DistributedCacheEntryOptions().SetSlidingExpiration(TimeSpan.FromMinutes(MaxTimeoutExpired)));
                //}
                //else if (timeSpan.TotalMinutes < 0)
                //{
                //    // update last request and update to identity user cache
                //    identityUserCache.LastRequest = DateTime.Now;
                //    cacheIdentityUserManager.Set(currentGuid, CacheExtensions.ObjectToByteArray(identityUserCache),
                //        new DistributedCacheEntryOptions().SetSlidingExpiration(TimeSpan.FromMinutes(MaxTimeoutExpired)));

                //    return;
                //}
                //else
                //{
                //    return;
                //}
                //var aaaa = HttpContext.Current.Request.Headers["Authorization"];


                //var ownerUsername = GetCurrentUserName();
                //var authToken = GetCurrentBearerAuthrorizationToken();
                var authToken = HttpContext.Current.Request.Headers["token"];
                byte[] data = Convert.FromBase64String(authToken);
                authToken = System.Text.Encoding.UTF8.GetString(data);
                var ownerUsername = HttpContext.Current.Request.Headers["username"];
                //JToken tok = JsonConvert.DeserializeObject<JSToken>(jsonContent);
                if (string.IsNullOrWhiteSpace(ownerUsername) || string.IsNullOrWhiteSpace(authToken))
                {
                    return;
                }

                userSessionService = new IdentityUserSessionServiceBLL();

                var valid = userSessionService.ReValidateUserSession(ownerUsername, authToken, this.MaxUserSessionTimeoutExpired);

                if (valid)
                {
                    //base.OnAuthorization(actionContext);
                    return;
                }
                else
                {
                    actionContext.Response = actionContext.ControllerContext.Request.CreateErrorResponse(
                        HttpStatusCode.Unauthorized, "Session token expired or not valid.", new Exception("Session token expired or not valid."));
                    Log.Info("Session token expired or not valid with ReValidateUserSession.");
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                throw;
            }
            finally
            {
                if (userSessionService != null)
                {
                    userSessionService.Dispose();
                }
            }
        }


        //public override void OnAuthorization(HttpActionContext actionContext)
        //{
        //    if (Authorize(actionContext))
        //    {
        //        return;
        //    }

        //    HandleUnauthorizedRequest(actionContext);
        //}

        //protected bool Authorize(HttpActionContext httpContext)
        //{
        //    bool isTokenAuthorized = HasValidToken();
        //    if (isTokenAuthorized) return true;

        //    return false;
        //}


        //protected bool HasValidToken()
        //{
        //    string token = string.Empty;
        //    token = HttpContext.Current.Request.Headers["token"];

        //    IIdentityUserSessionService userSessionService = null;
        //    userSessionService = new IdentityUserSessionServiceBLL();

        //    var valid = userSessionService.ReValidateUserSession(ownerUsername, token, this.MaxUserSessionTimeoutExpired);

        //    if (valid)
        //    {
        //        base.OnAuthorization(actionContext);
        //    }
        //    else
        //    {
        //        actionContext.Response = actionContext.ControllerContext.Request.CreateErrorResponse(
        //            HttpStatusCode.Unauthorized, "Session token expired or not valid.", new Exception("Session token expired or not valid."));
        //        Log.Info("Session token expired or not valid with ReValidateUserSession.");
        //    }
        //    //_connectionString = WebConfigurationManager.ConnectionStrings["SqlConnectionString"].ConnectionString;
        //    //SqlTransaction txn = null;
        //    //using (SqlConnection conn = new SqlConnection(_connectionString))
        //    //{
        //    //    conn.Open();
        //    //    txn = conn.BeginTransaction();
        //    //    List<SqlParameter> parameters = new List<SqlParameter>();
        //    //    SqlParameter parameter = new SqlParameter();
        //    //    parameters.Add(new SqlParameter("@token", token));

        //    //    parameter = new SqlParameter("@return_ops", 0);
        //    //    parameter.Direction = ParameterDirection.Output;
        //    //    parameters.Add(parameter);

        //    //    SqlHelper.ExecuteNonQuery(txn, CommandType.StoredProcedure, "[master_LoggedInUsers]", parameters.ToArray());

        //    //    int result = Convert.ToInt32(parameters[1].Value);

        //    //    if (result <= 0)
        //    //    {
        //    //        return false;
        //    //    }
        //    //    else return true;
        //    //}
        //    return true;
        //}

        /// <summary>
        /// 
        /// </summary>
        /// <param name="actionContext"></param>
        /// <returns></returns>
        private static bool SkipAuthorization(HttpActionContext actionContext)
        {
            return actionContext.ActionDescriptor.GetCustomAttributes<AllowAnonymousAttribute>().Any()
                   || actionContext.ControllerContext.ControllerDescriptor.GetCustomAttributes<AllowAnonymousAttribute>().Any();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private static string GetCurrentBearerAuthrorizationToken()
        {
            string authToken = null;

            if (CurrentRequest == null || CurrentRequest.Request.Headers.Get("Authorization") == null)
                return null;

            var headerValue = CurrentRequest.Request.Headers.GetValues("Authorization");
            var bearerValue = headerValue[0].Split(' ');

            if (bearerValue.Length == 2)
            {
                authToken = bearerValue[1];
            }

            return authToken;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private static string GetCurrentUserName()
        {
            if (HttpContext.Current.User == null)
            {
                return null;
            }

            var userId = HttpContext.Current.User.Identity.GetUserName();
            return userId;
        }
    }
}
