﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TraceHelperHandler.cs" company="Aware Technology Solutions Corporation Ltd">
//   Copyright (c) 2016-2017.  All rights reserved.
// </copyright>
// <summary>
//   Defines the TraceHelperHandler type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Routing;

namespace PNG.WebApi.Handlers
{
    public class TraceHelperHandler : DelegatingHandler
    {
        protected const string TraceBeginRequestFormat = @"{0} controller: {1}, method: {2}, return type: {3} {{ ";
        protected const string TraceExceptionRequestFormat = @"{0} exception: {1}{2}}}";
        protected const string TraceEndRequestFormat = @"{0} response: {1} {2}}}";

        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            return await base.SendAsync(request, cancellationToken).ContinueWith(
                task =>
                {
                    try
                    {
                        // Find information about the controller and action related to this request
                        var attributedRouteData = request.GetConfiguration().Routes.GetRouteData(request);

                        if (attributedRouteData != null)
                        {
                            var subRouteData = attributedRouteData.GetSubRoutes().FirstOrDefault();

                            if (subRouteData != null)
                            {
                                object actions;
                                var isActions = subRouteData.Route.DataTokens.TryGetValue("actions", out actions);

                                if (isActions)
                                {
                                    var action = ((HttpActionDescriptor[])actions)[0];

                                    if (action == null)
                                        throw new ArgumentNullException();

                                    Trace.WriteLine(string.Format(TraceBeginRequestFormat, DateTime.Now, action.ControllerDescriptor.ControllerType.Name, action.ActionName, action.ReturnType.Name));
                                    Trace.Indent();
                                }
                            }

                            var isValidResponse = ResponseIsValid(task.Result);


                            object responseObject;
                            task.Result.TryGetContentValue(out responseObject);

                            if (isValidResponse)
                            {
                                Trace.WriteLine(string.Format(TraceEndRequestFormat, DateTime.Now, responseObject,
                                    Environment.NewLine));
                            }
                            else
                            {
                                var errorBuilder = new StringBuilder();

                                foreach (var error in (HttpError)responseObject)
                                {
                                    errorBuilder.Append(string.Format("{0}{1}", error, Environment.NewLine));
                                }

                                Trace.TraceError(TraceExceptionRequestFormat, DateTime.Now, errorBuilder.ToString(), Environment.NewLine);
                            }

                        }
                    }
                    catch (Exception ex)
                    {
                        Trace.TraceError(TraceExceptionRequestFormat, DateTime.Now, ex, Environment.NewLine);
                    }
                    finally
                    {
                        Trace.Unindent();
                    }


                    return task.Result;
                }, cancellationToken);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="response"></param>
        /// <returns></returns>
        private static bool ResponseIsValid(HttpResponseMessage response)
        {
            if (response == null)
                return false;

            return (response.StatusCode == HttpStatusCode.OK) ||
                   !(response.Content is ObjectContent);
        }
    }
}