﻿using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security.OAuth;
using PNG.BusinessLogic.Interfaces;
using PNG.BusinessLogic.Services;
using PNG.Common.Models;
using PNG.DataAccess.AspNetIdentities;
using PNG.WebApi.Helpers;
using PNG.WebApi.Infrastructures;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace PNG.WebApi.Providers
{
    public class MyAuthorizationServerProvider : OAuthAuthorizationServerProvider
    {
        public override async Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            context.Validated();
        }
        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            //using (UserMasterRepository _repo = new UserMasterRepository())
            //{
            //    var user = _repo.ValidateUser(context.UserName, context.Password);
            //    if (user == null)
            //    {
            //        context.SetError("invalid_grant", "Provided username and password is incorrect");
            //        return;
            //    }
            //    var identity = new ClaimsIdentity(context.Options.AuthenticationType);
            //    identity.AddClaim(new Claim(ClaimTypes.Role, user.UserRoles));
            //    identity.AddClaim(new Claim(ClaimTypes.Name, user.UserName));
            //    identity.AddClaim(new Claim("Email", user.UserEmailID));
            //    context.Validated(identity);
            //}

            const string allowedOrigin = "*";
            context.OwinContext.Response.Headers.Add("Access-Control-Allow-Origin", new[] { allowedOrigin });

            var userManager = context.OwinContext.GetUserManager<ApplicationUserManager>();
            IUserAuthService userAuth = new UserAuthServiceBLL();

            var clientIpAddress = IdentityUserExtension.GetClientIpAddress(context);
            var clientConnectionId = IdentityUserExtension.GetClientConnectionId(context);
            var forceChangePassword = false;
            var exceptionMessages = new List<ExceptionMessage>();

            var user = await userAuth.GetUser(context.UserName, context.Password, clientIpAddress, exceptionMessages);

            if (user == null)
            {
                if (exceptionMessages.Any())
                {
                    context.SetError("invalid_userexpired", exceptionMessages.FirstOrDefault().Message);
                }
                else
                {
                    context.SetError("invalid_grant", "The user name or password is incorrect.");
                }
                return;
            }

            ClaimsIdentity claimsIdentity = new ClaimsIdentity(context.Options.AuthenticationType);
            //claimsIdentity.AddClaim(new Claim("sub", context.UserName));
            //claimsIdentity.AddClaim(new Claim("role", "user"));

            //ExtendedClaimsProvider.GetClaims(user, clientIpAddress, clientConnectionId));
            claimsIdentity.AddClaims(ExtendedClaimsProvider.GetClaims(user, clientIpAddress, clientConnectionId));
            claimsIdentity.AddClaim(new Claim("role", "user"));
            context.Validated(claimsIdentity);

            //var oAuthIdentity = await user.GenerateUserIdentityAsync(userManager, "JWT");

            //oAuthIdentity.AddClaims(ExtendedClaimsProvider.GetClaims(user, clientIpAddress, clientConnectionId));
            //oAuthIdentity.AddClaims(RolesFromClaims.CreateRolesBasedOnClaims(oAuthIdentity));


            //// Append client connection id and force change password to the authentication ticket
            //var properties = CreateProperties(clientConnectionId, forceChangePassword);

            //var ticket = new AuthenticationTicket(oAuthIdentity, properties);
            context.Validated();
        }
    }
}