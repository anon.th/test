﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomOAuthProvider.cs" company="Aware Corporation Limited">
//   Copyright (c) 2014-2015.  All rights reserved.
// </copyright>
// <summary>
//   Defines the CustomOAuthProvider type.
//   Reviewed on 17 Jan 2016.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.OAuth;
using PNG.BusinessLogic.Interfaces;
using PNG.BusinessLogic.Services;
using PNG.Common.Enums;
using PNG.Common.Enums.Common;
using PNG.Common.Helpers;
using PNG.Common.Models;
using PNG.DataAccess.AspNetIdentities;
using PNG.Model.Models;
using PNG.WebApi.Helpers;
using PNG.WebApi.Infrastructures;
using SimpleInjector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace PNG.WebApi.Providers
{
    /// <summary>
    /// 
    /// </summary>
    public class CustomOAuthProvider : OAuthAuthorizationServerProvider
    {
        protected readonly int MaxTimeoutExpired = PNG.Common.Helpers.ConfigurationHelper.GetAppSettingValue<int>(AppConfigurationCode.MaxTimeoutExpired.GetAttributeCode());

        private readonly Container _container;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="container"></param>
        public CustomOAuthProvider(Container container)
        {
            this._container = container;
        }

        /// <summary>
        /// Called when a request to the Token endpoint arrives with a "grant_type" of "password"
        /// This occurs when the user has provided name and password credentials directly into the client application's user interface, 
        /// and the client application is using those to acquire an "access_token" and optional "refresh_token"
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            const string allowedOrigin = "*";
            context.OwinContext.Response.Headers.Add("Access-Control-Allow-Origin", new[] { allowedOrigin });

            var userManager = context.OwinContext.GetUserManager<ApplicationUserManager>();
            IUserAuthService userAuth = new UserAuthServiceBLL();

            var clientIpAddress = IdentityUserExtension.GetClientIpAddress(context);
            var clientConnectionId = IdentityUserExtension.GetClientConnectionId(context);
            var enterpriseId = IdentityUserExtension.GetEnterpriseId(context);
            var userType = IdentityUserExtension.GetUserTypeId(context);

            var exceptionMessages = new List<ExceptionMessage>();
            //var enterpriseID = ConfigurationHelper.GetAppSetting(AppConfigurationCode.enterpriseID.GetAttributeCode());

            var user = await userAuth.GetUser(context.UserName, context.Password, clientIpAddress, enterpriseId);

            if (user == null)
            {
                if (exceptionMessages.Any())
                {
                    context.SetError("invalid_userexpired", exceptionMessages.FirstOrDefault().Message);
                }
                else
                {
                    context.SetError("invalid_grant", "The user name or password is incorrect.");
                }
                return;
            }

            ClaimsIdentity claimsIdentity = new ClaimsIdentity(context.Options.AuthenticationType);
            //claimsIdentity.AddClaim(new Claim("sub", context.UserName));
            //claimsIdentity.AddClaim(new Claim("role", "user"));

            //ExtendedClaimsProvider.GetClaims(user, clientIpAddress, clientConnectionId));
            claimsIdentity.AddClaims(ExtendedClaimsProvider.GetClaims(user, clientIpAddress, clientConnectionId));
            context.Validated(claimsIdentity);

            //var oAuthIdentity = await user.GenerateUserIdentityAsync(userManager, "JWT");

            //oAuthIdentity.AddClaims(ExtendedClaimsProvider.GetClaims(user, clientIpAddress, clientConnectionId));
            //oAuthIdentity.AddClaims(RolesFromClaims.CreateRolesBasedOnClaims(oAuthIdentity));


            //// Append client connection id and force change password to the authentication ticket
            //var properties = CreateProperties(clientConnectionId, forceChangePassword);

            //var ticket = new AuthenticationTicket(oAuthIdentity, properties);
            context.Validated();

            // Set IdentityUserCache with expired match appConfig MaxTimeoutExpired
            //var cacheIdentityUserManager = this._container.GetInstance<IDistributedCache>();
            //var currentGuidType = UserClaim.CurrentGuid.GetAttributeCode();
            //var foundClaim = claimsIdentity.Claims.FirstOrDefault(c => c.Type == currentGuidType);

            //if (foundClaim != null)
            //{
            //    var currentGuid = foundClaim.Value;
            //    var userId = user.Id;

            //    var identityUserCache = new IdentityUserCache
            //    {
            //        CurrentGuid = currentGuid,
            //        UserId = userId,
            //        CurrentRoleId = user.DefaultRoleId,
            //        LastRequest = DateTime.Now,
            //        ForceChangePassword = forceChangePassword
            //    };

            //    if (cacheIdentityUserManager != null)
            //    {
            //        cacheIdentityUserManager.Set(currentGuid, PNG.Common.Helpers.CacheExtensions.ObjectToByteArray(identityUserCache),
            //            new DistributedCacheEntryOptions().SetSlidingExpiration(TimeSpan.FromMinutes(MaxTimeoutExpired)));
            //    }
            //}
        }

        /// <summary>
        /// Called to validate that the origin of the request is a registered "client_id", and that the correct credentials for that client are present on the request.
        /// </summary>
        /// <param name="context">
        /// The context.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public override Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            context.Validated();
            return Task.FromResult<object>(null);
        }

        public static AuthenticationProperties CreateProperties(string clientConnectionId, bool forceChangePassword)
        {
            IDictionary<string, string> data = new Dictionary<string, string>
            {
                { "ClientConnectionId", clientConnectionId },
                { "ForceChangePassword", forceChangePassword.ToString() }
            };
            return new AuthenticationProperties(data);
        }

        public override Task TokenEndpoint(OAuthTokenEndpointContext context)
        {
            foreach (KeyValuePair<string, string> property in context.Properties.Dictionary)
            {
                context.AdditionalResponseParameters.Add(property.Key, property.Value);
            }

            return Task.FromResult<object>(null);
        }
    }
}
