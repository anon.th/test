﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="BaseApiController.cs" company="Aware Technology Solutions Corporation Ltd">
//   Copyright (c) 2016-2017.  All rights reserved.
// </copyright>
// <summary>
//   Defines the BaseApiController type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using Microsoft.AspNet.Identity;
using PNG.Common.Enums;
using PNG.Common.Enums.Common;
using System.Web.Http;

namespace PNG.WebApi.Controllers
{
    //[PNGAuthorize]
    public class BaseApiController : ApiController
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="result"></param>
        /// <returns></returns>
        protected IHttpActionResult GetErrorResult(IdentityResult result)
        {
            if (result == null)
            {
                return InternalServerError();
            }

            if (result.Succeeded)
                return null;

            if (result.Errors != null)
            {
                foreach (var error in result.Errors)
                {
                    ModelState.AddModelError("", error);
                }
            }

            if (ModelState.IsValid)
            {
                // No ModelState errors are available to send, so just return an empty BadRequest.
                return BadRequest();
            }

            return BadRequest(ModelState);
        }

        public string ApiBaseUri
        {
            get { return Common.Helpers.ConfigurationHelper.GetAppSetting(AppConfigurationCode.baseApi.GetAttributeCode()); }
        }

        /// <summary>
        /// 
        /// </summary>
        public int MaxTimeoutExpired
        {
            get { return Common.Helpers.ConfigurationHelper.GetAppSettingValue<int>(AppConfigurationCode.MaxTimeoutExpired.GetAttributeCode()); }
        }
    }
}
