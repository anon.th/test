﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AppRouteMenuController.cs" company="Aware Corporation Limited">
//   Copyright (c) 2014-2015.  All rights reserved.
// </copyright>
// <summary>
//   Defines the ExternalAppRouteMenuController type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using PNG.BusinessLogic.Interfaces;
using PNG.WebApi.Controllers;
using PNG.WebApi.Handlers;
using PNG.Model.Models;
using System.Collections.Generic;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using PNG.Model;

namespace PNG.WebApi.Areas.Master.Controllers
{
    [RoutePrefix("api/master/trackandtrace")]
    //[PNGAuthorize]
    public class TrackandTraceController : ApiController
    {
        /// <summary>
        /// The _app route menu.
        /// </summary>
        protected readonly ITrackandTraceService _trackandtrace;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="appRouteMenu"></param>
        public TrackandTraceController(ITrackandTraceService trackandTrace)
        {
            this._trackandtrace = trackandTrace;
        }

        //[Route("consignmentNo/{consignmentNo}/refno/{refno}/userId/{userId}/expandPackageDetails/{expandPackageDetails}")]
        //[HttpGet]
        //public TrackAndTraceResult SearchTrackandTrace(string consignmentNo, string refno,string userId, bool expandPackageDetails)
        //{
        //    return this._trackandtrace.SearchTrackandTrace(consignmentNo, refno, userId, expandPackageDetails);
        //}

        [Route("consignmentNo")]
        [HttpPost]
        public TrackAndTraceResult SearchTrackandTrace(TrackandTraceParam param)
        {
            return this._trackandtrace.SearchTrackandTrace(param);
        }
    }
}