﻿using PNG.BusinessLogic.Interfaces;
using PNG.Model;
using PNG.Model.Models;
using PNG.WebApi.Handlers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace PNG.WebApi.Areas.Master.Controllers
{
    [RoutePrefix("api/master/createconsignment")]
    [PNGAuthorize]
    public class CreateConsignmentController : ApiController
    {
        /// <summary>
        /// 
        /// </summary>
        protected readonly ICreateConsignmentService _createconsignment;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="createconsignment"></param>
        public CreateConsignmentController(ICreateConsignmentService createconsignment)
        {
            this._createconsignment = createconsignment;
        }


        [Route("getcreconstore")]
        [HttpPost]
        public CreateConsignmentResult GetCreateConsignmentStoreProcedure(CreateConsignmentParam createconsignParam)
        {
            return this._createconsignment.GetCreateConsignmentStoreProcedure(createconsignParam);
        }

        [Route("updatereference")]
        [HttpPost]
        public CreateConsignmentUpdateReference UpdateConsignmentStoreProcedure(CreateConsignmentParam createconsignParam)
        {
            return this._createconsignment.UpdateReferenceStoreProcedure(createconsignParam);
        }

        [Route("getbindsenderdetail/userId/{userId}")]
        [HttpGet]
        public CreateConsignmentBindSenderDetail GetBindSenderDetails(string userId)
        {
            return this._createconsignment.GetBindSenderDetails(userId);
        }

        [Route("getcustomeraccount/userId/{userId}")]
        [HttpGet]
        public CreateConsignmentCustomerAccount GetCustomerAccount(string userId)
        {
            return this._createconsignment.GetCustomerAccount(userId);
        }

        [Route("getuserassignedrole/userId/{userId}")]
        [HttpGet]
        public bool GetUserAssignedRoles(string userId)
        {
            return this._createconsignment.GetUserAssignedRoles(userId);
        }

        [Route("getquerysndrecname/userId/{userId}")]
        [HttpGet]
        public List<CreateConsignmentQuerySndRecName> GetQuerySndRecName(string userId)
        {
            return this._createconsignment.GetQuerySndRecName(userId);
        }

        [Route("getservicecode")]
        [HttpGet]
        public List<CreateConsignmentServiceCode> GetServiceCode()
        {
            return this._createconsignment.GetServiceCode();
        }

        [Route("getwarehousename/selectValue/{selectValue}")]
        [HttpGet]
        public CreateConsignmentCustomsWarehousesName GetCustomsWarehousesName(string selectValue)
        {
            return this._createconsignment.GetCustomsWarehousesName(selectValue);
        }

        [Route("getstatename/zipcode/{zipcode}")]
        [HttpGet]
        public CreateConsignmentStateName GetStateName(string zipcode)
        {
            return this._createconsignment.GetStateName(zipcode);
        }

        [Route("getbytelephone/telephone/{telephone}")]
        [HttpGet]
        public CreateConsignmentBindRecipienDetail GetRecipientDetailByTelephone(string telephone)
        {
            return this._createconsignment.getRecipientDetailByTelephone(telephone);
        }

        [Route("getbycostcentre/costcentre/{costcentre}")]
        [HttpGet]
        public CreateConsignmentBindRecipienDetail GetRecipientDetailByCostCentre(string costcentre)
        {
            return this._createconsignment.getRecipientDetailByCostCentre(costcentre);
        }

        [Route("getcustomeracc2/payerId/{payerId}")]
        [HttpGet]
        public Customer GetCustomerAcc2(string payerId)
        {
            return this._createconsignment.getCustomerAcc2(payerId);
        }

        [Route("getpackagelimit/userId/{userId}")]
        [HttpGet]
        public List<CreateConsignmentPackageLimit> GetPackageLimit(string userId)
        {
            return this._createconsignment.getPackageLimit(userId);
        }

        [Route("getconfiguration/userId/{userId}")]
        [HttpGet]
        public List<CreateConsignmentConfigurations> GetConfigurations(string userId)
        {
            return this._createconsignment.getConfigurations(userId);
        }

        [Route("getenterprisecontract/userId/{userId}")]
        [HttpGet]
        public CreateConsignmentEnterpriseContract GetEnterpriseContract(string userId)
        {
            return this._createconsignment.getEnterpriseContract(userId);
        }

        [Route("getdefaultservicecode/userId/{userId}")]
        [HttpGet]
        public CreateConsignmentDefaultServiceCode GetDefaultServiceCode(string userId)
        {
            return this._createconsignment.getDefaultServiceCode(userId);
        }



    }
}
