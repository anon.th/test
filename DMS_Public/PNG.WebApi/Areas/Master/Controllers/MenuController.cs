﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AppRouteMenuController.cs" company="Aware Corporation Limited">
//   Copyright (c) 2014-2015.  All rights reserved.
// </copyright>
// <summary>
//   Defines the ExternalAppRouteMenuController type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using PNG.BusinessLogic.Interfaces;
using PNG.Model;
using PNG.Model.Models;
using System.Collections.Generic;
using System.Web.Http;

namespace PNG.WebApi.Areas.Master.Controllers
{
    [RoutePrefix("api/master/menu")]
    public class MenuController : ApiController
    {
        /// <summary>
        /// The _app route menu.
        /// </summary>
        protected readonly IMenuService _menu;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="appRouteMenu"></param>
        public MenuController(IMenuService menu)
        {
            this._menu = menu;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="userid"></param>
        /// <param name="moduleid"></param>
        /// <returns></returns>
        [Route("userid/{userid}/moduleid/{moduleid}")]
        [HttpGet]
        [AllowAnonymous]
        public List<Module_Tree_Role_Relation> GetMenus(string userid, string moduleid)
        {
            return this._menu.GetAllModules(userid, moduleid);
        }
    }
}