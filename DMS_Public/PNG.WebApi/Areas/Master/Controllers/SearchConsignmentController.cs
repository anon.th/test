﻿using PNG.BusinessLogic.Interfaces;
using PNG.Model;
using PNG.Model.Models;
using PNG.WebApi.Handlers;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace PNG.WebApi.Areas.Master.Controllers
{
    [RoutePrefix("api/master/searchconsignment")]
    [PNGAuthorize]
    public class SearchConsignmentController : ApiController
    {
        protected readonly ISearchConsignmentService _searchconsignment;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="searchconsignment"></param>
        public SearchConsignmentController(ISearchConsignmentService searchconsignment)
        {
            this._searchconsignment = searchconsignment;
        }

        [Route("getstatusprintingconfig")]
        [HttpGet]
        public SearchConsignmentStatusPrinting GetStatusPrinting()
        {
            return this._searchconsignment.getStatusPrinting();
        }

        [Route("getreferenceds/userId/{userId}")]
        [HttpGet]
        public List<CreateConsignmentQuerySndRecName> GetReferenceDS(string userId)
        {
            return this._searchconsignment.QuerySndRecName(userId);
        }

        [Route("getcustomswarehousesname/selectValue/{selectValue?}")]
        [HttpGet]
        public List<CreateConsignmentCustomsWarehousesName> GetCustomsWarehousesName(string selectValue)
        {
            return this._searchconsignment.getCustomsWarehousesName(selectValue);
        }

        [Route("getenterpriseuseraccounts/userId/{userId}")]
        [HttpGet]
        public string GetEnterpriseUserAccounts(string userId)
        {
            return this._searchconsignment.getEnterpriseUserAccounts(userId);
        }

        [Route("getcustomstatus")]
        [HttpGet]
        public List<SearchConsignmentCustomStatus> GetCustomStatus()
        {
            return this._searchconsignment.getCustomStatus();
        }

        [Route("getcustomerstatus")]
        [HttpGet]
        public List<SearchConsignmentCustomStatus> GetCustomerStatus()
        {
            return this._searchconsignment.getCustomerStatus();
        }

        [Route("getcssconsignmentstatus")]
        [HttpPost]
        public SearchConsignmentResult CSS_ConsignmentStatus(SearchConsignmentParam param)
        {

            //var filter = new SearchConsignmentParam
            //{
            //    enterpriseid = "PNGAF"  ,
            //    userloggedin = "AWARE"  ,
            //    SenderName = ""  ,
            //    payerid = "20799"  ,
            //    MasterAWBNumber = ""  ,
            //    JobEntryNo = "",
            //    consignment_no = "",
            //    status_id = null,
            //    ShippingList_No = "",
            //    recipient_telephone = "",
            //    ref_no = "",
            //    recipient_zipcode = "",
            //    service_code = "",
            //    datefrom = new DateTime(2001, 01, 01),  
            //    dateto = new DateTime(2001, 01, 01)
            //};
            //var json = Newtonsoft.Json.JsonConvert.SerializeObject(filter);
            //var x = param.datefrom.ToString().Substring(0,8);
           
            //DateTime val = DateTime.ParseExact(x, "MM/dd/yyyy HH:mm", CultureInfo.InvariantCulture);
            //param.datefrom = val;

            return this._searchconsignment.CSS_ConsignmentStatus(param);
        }

        [Route("getcssreprintconsnote")]
        [HttpPost]
        public SearchConsignmentReprintConsNoteResult CSS_ReprintConsNote(ReprintConsNoteParam reprintConsNoteParam)
        {
            return this._searchconsignment.CSS_ReprintConsNote(reprintConsNoteParam);
        }

        [Route("getcssprintshippinglist")]
        [HttpPost]
        public SearchConsignmentPrintShippingListResult CSS_PrintShippingList(PrintShippingListParam printShippingListParam)
        {
            return this._searchconsignment.CSS_PrintShippingList(printShippingListParam);
        }

        [Route("getcssprintconsnote")]
        [HttpPost]
        public SearchConsignmentPrintConsNoteResult CSS_PrintConsNote(PrintShippingListParam printConsNoteParam)
        {
            return this._searchconsignment.CSS_PrintConsNote(printConsNoteParam);
        }


        [Route("getconnectionString")]
        [HttpGet]
        [AllowAnonymous]
        public string getConnectionString()
        {
            string connectionString = this._searchconsignment.getConnectionString();
            string[] connectionStringArray = connectionString.Split(';');
           // string[] connectionStringArrayTemp = new string[connectionStringArray.Length];

            SearchConsignmentConnectionstring connection = new SearchConsignmentConnectionstring();


            for (int i = 0; i < connectionStringArray.Length; i++)
            {
                string[] connectionStringArrayTemp = connectionStringArray[i].Split('=');             

                switch (connectionStringArrayTemp[0].ToLower())
                {
                    case "data source": connection.datasource = connectionStringArrayTemp[1];break;
                    case "initial catalog": connection.initialCatalog = connectionStringArrayTemp[1]; break;
                    case "persist security info": connection.persistSecurity = connectionStringArrayTemp[1]; break;
                    case "user id": connection.userId = connectionStringArrayTemp[1]; break;
                    case "password": connection.password = connectionStringArrayTemp[1]; break;
                    case "packet size": connection.packetSize = connectionStringArrayTemp[1]; break;
                }
            }
            return connectionString;
        }

      






    }
}
