﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AppRouteMenuController.cs" company="Aware Corporation Limited">
//   Copyright (c) 2014-2015.  All rights reserved.
// </copyright>
// <summary>
//   Defines the ExternalAppRouteMenuController type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using PNG.BusinessLogic.Interfaces;
using PNG.WebApi.Controllers;
using PNG.WebApi.Handlers;
using PNG.Model.Models;
using System.Collections.Generic;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace PNG.WebApi.Areas.Master.Controllers
{
    [RoutePrefix("api/master/approutemenu")]
    public class AppRouteMenuController : ApiController
    {
        /// <summary>
        /// The _app route menu.
        /// </summary>
        protected readonly IAppRouteMenuService _appRouteMenu;

        /// <summary>
        /// Initializes a new instance of the <see cref="AppRouteMenuController"/> class. 
        /// AppRouteMenuController constructor
        /// </summary>
        /// <param name="appRouteMenu">
        /// The appRouteMenu.
        /// </param>
        public AppRouteMenuController(IAppRouteMenuService appRouteMenu)
        {
            this._appRouteMenu = appRouteMenu;
        }

        [Route("")]
        [AllowAnonymous]
        public IEnumerable<RouteMenu> GetApplicationRouteMenus()
        {
            return this._appRouteMenu.GetAppRouteMenus();
        }
    }
}