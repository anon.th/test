﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="UserProfileController.cs" company="Aware Corporation Limited">
//   Copyright (c) 2014-2015.  All rights reserved.
// </copyright>
// <summary>
//   Defines the ExternalAppRouteMenuController type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using PNG.BusinessLogic.Interfaces;
using System.Web.Http;
using PNG.Model;
using PNG.WebApi.Handlers;

namespace PNG.WebApi.Areas.Master.Controllers
{
    [RoutePrefix("api/master/userprofile")]
    [PNGAuthorize]
    public class UserProfileController : ApiController
    {
        /// <summary>
        /// 
        /// </summary>
        protected readonly IUserProfileService _userprofile;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userprofile"></param>
        public UserProfileController(IUserProfileService userprofile)
        {
            this._userprofile = userprofile;
        }

        [Route("")]
        [HttpPost]
        public bool Edit(User_Master user)
        {
            return this._userprofile.Edit(user);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        [Route("userId/{userId}")]
        [HttpGet]
        public User_Master GetUser(string userId)
        {
            return this._userprofile.GetUser(userId);
        }


        [Route("resetPassword")]
        [HttpPost]
        public bool UpdatePassword(User_Master user)
        {
            return this._userprofile.UpdatePassword(user);
        }

        [Route("validateUserProfile")]
        [HttpPost]
        public string ValidateUserProfile(User_Master user)
        {
            return this._userprofile.ValidateUserProfile(user);
        }
    }
}