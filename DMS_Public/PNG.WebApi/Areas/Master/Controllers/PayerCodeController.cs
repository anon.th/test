﻿using PNG.BusinessLogic.Interfaces;
using PNG.Model;
using PNG.Model.Models;
using PNG.WebApi.Handlers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace PNG.WebApi.Areas.Master.Controllers
{
    [RoutePrefix("api/master/payercode")]
    [PNGAuthorize]
    public class PayerCodeController : ApiController
    {
        protected readonly IPayerCodeService _payercode;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userprofile"></param>
        public PayerCodeController(IPayerCodeService payercode)
        {
            this._payercode = payercode;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        [Route("")]
        [HttpPost]
        public SearchResultView<Customer> Search(SearchQueryParameters parameters)
        {
            return this._payercode.SearchWithCriteria(parameters);
        }
    }
}
