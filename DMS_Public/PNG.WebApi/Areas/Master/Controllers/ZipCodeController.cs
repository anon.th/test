﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="UserProfileController.cs" company="Aware Corporation Limited">
//   Copyright (c) 2014-2015.  All rights reserved.
// </copyright>
// <summary>
//   Defines the ExternalAppRouteMenuController type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using PNG.BusinessLogic.Interfaces;
using System.Web.Http;
using PNG.Model;
using PNG.WebApi.Handlers;
using PNG.Model.Models;

namespace PNG.WebApi.Areas.Master.Controllers
{
    [RoutePrefix("api/master/zipcode")]
    [PNGAuthorize]
    public class ZipCodeController : ApiController
    {
        /// <summary>
        /// 
        /// </summary>
        protected readonly IZipCodeService _zipcode;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userprofile"></param>
        public ZipCodeController(IZipCodeService zipcode)
        {
            this._zipcode = zipcode;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        [Route("")]
        [HttpPost]
        public SearchResultView<Zipcode> Search(SearchQueryParameters parameters)
        {
            return this._zipcode.SearchWithCriteria(parameters);
        }

    }
}