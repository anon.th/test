﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="UserProfileController.cs" company="Aware Corporation Limited">
//   Copyright (c) 2014-2015.  All rights reserved.
// </copyright>
// <summary>
//   Defines the ExternalAppRouteMenuController type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using PNG.BusinessLogic.Interfaces;
using System.Web.Http;
using PNG.Model;
using System;
using PNG.WebApi.Helpers;
using System.Net;
using System.Threading.Tasks;
using PNG.WebApi.Controllers;
using Newtonsoft.Json.Linq;
using PNG.BusinessLogic.Services;

namespace PNG.WebApi.Areas.Master.Controllers
{
    [RoutePrefix("api/master")]
    //[PNGAuthorize]
    public class LoginController : BaseApiController
    {
        /// <summary>
        /// 
        /// </summary>
        protected readonly ILoginService _login;

        /// <summary>
        /// 
        /// </summary>
        protected readonly IUserAuthService _userAuth;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userprofile"></param>
        /// <param name="login"></param>
        public LoginController(ILoginService login, IUserAuthService userAuth)
        {
            this._login = login;
            this._userAuth = userAuth;
        }


        //[Route("")]
        //[HttpPost]
        //[AllowAnonymous]
        //public async Task<IHttpActionResult> Login(UserLogin user)
        //{

        //}

        [Route("login")]
        [HttpPost]
        [AllowAnonymous]
        public async Task<IHttpActionResult> LoginUser(UserLogin user)
        {
            if (user == null)
            {
                return this.BadRequest("Invalid user data");
            }

            // Decode password base64 string data from  btoa(encodeURIComponent(loginData.password))
            byte[] data = Convert.FromBase64String(user.Password);
            user.Password = System.Text.Encoding.UTF8.GetString(data);
            user.Password = System.Web.HttpUtility.UrlDecode(user.Password);

            var resultUser = _userAuth.GetUser(user.Username, user.Password, null, user.EnterpriseID);


            //Create Client connection id
            user.ClientConnectionId = Guid.NewGuid().ToString();
            var response = await IdentityUserExtension.GetApiTokenResponse(user, base.ApiBaseUri);

            if (response.StatusCode != HttpStatusCode.OK)
                return this.ResponseMessage(response);

            // Get access token from response body
            var responseJson = await response.Content.ReadAsStringAsync();
            var jObject = JObject.Parse(responseJson);
            var authToken = jObject.GetValue("access_token").ToString();
            var userSessionService = new IdentityUserSessionServiceBLL();

            userSessionService.CreateIdentityUserSession(new IdentityUserSessions
            {
                OwnerUsername = user.Username,
                AuthToken = authToken,
                ExpirationDate = DateTime.Now.AddMinutes(base.MaxTimeoutExpired),
                ModifiedDate = DateTime.Now
            });

            // Delete all expired user sessions
            userSessionService.DeleteExpiredUserSessions();
            userSessionService.Dispose();

            //var userIdentityCache = CacheExtensions.ByteArrayToObject<IdentityUserCache>(base.CacheIdentityUserManager.Get(user.ClientConnectionId));

            //if (userIdentityCache != null)
            //{
            //    // Add response header with Client connection id when valid login
            //    response.Headers.Add("ClientConnectionId", userIdentityCache.CurrentGuid);

            //    // Add response header with force user to change password
            //    response.Headers.Add(CustomMessageError.ForceChangePassword.GetAttributeCode(), userIdentityCache.ForceChangePassword.ToString());
            //}

            return this.ResponseMessage(response);
        }
    }
}