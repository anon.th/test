﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="UserProfileController.cs" company="Aware Corporation Limited">
//   Copyright (c) 2014-2015.  All rights reserved.
// </copyright>
// <summary>
//   Defines the ExternalAppRouteMenuController type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using PNG.BusinessLogic.Interfaces;
using System.Web.Http;
using PNG.Model;
using PNG.WebApi.Handlers;
using PNG.Model.Models;

namespace PNG.WebApi.Areas.Master.Controllers
{
    [RoutePrefix("api/master/references")]
    [PNGAuthorize]
    public class ReferencesController : ApiController
    {
        /// <summary>
        /// 
        /// </summary>
        protected readonly IReferencesService _references;

       /// <summary>
       /// 
       /// </summary>
       /// <param name="references"></param>
        public ReferencesController(IReferencesService references)
        {
            this._references = references;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        [Route("")]
        [HttpPost]
        public SearchResultView<Reference> Search(SearchQueryParameters parameters)
        {
            return this._references.SearchWithCriteria(parameters);
        }

    }
}