﻿using PNG.BusinessLogic.Interfaces;
using PNG.Model.Models;
using PNG.WebApi.Handlers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace PNG.WebApi.Areas.Master.Controllers
{
    [RoutePrefix("api/master/shipmenttracking")]
    [PNGAuthorize]
    public class ShipmentTrackingController : ApiController
    {
        protected readonly IShipmentTrackingService _shipmenttracking;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="shipmenttracking"></param>
        public ShipmentTrackingController(IShipmentTrackingService shipmenttracking)
        {
            this._shipmenttracking = shipmenttracking;
        }

        [Route("getuserdetail/userId/{userId}")]
        [HttpGet]
        public ShipmentTrackingUserDetail GetUserCulture(string userId)
        {
            return this._shipmenttracking.getUserDetail(userId);
        }

        [Route("getcodevalues")]
        [HttpPost]
        public List<ShipmentTrackingCodeValue> GetCodeValues(ShipmentTrackingParam shipmentParam)
        {
            return this._shipmenttracking.getCodeValues(shipmentParam);
        }

        [Route("getallroles/userId/{userId}")]
        [HttpGet]
        public List<ShipmentTrackingAllRoles> GetAllRoles(string userId)
        {
            return this._shipmenttracking.getAllRoles(userId);
        }

        [Route("getpathcodequery")]
        [HttpGet]
        public List<ShipmentTrackingPathCodeQuery> GetPathCodeQuery()
        {
            return this._shipmenttracking.getPathCodeQuery();
        }

        [Route("getdistributioncenterquery")]
        [HttpGet]
        public List<ShipmentTrackingPathCodeQuery> GetDistributionCenterQuery()
        {
            return this._shipmenttracking.getDistributionCenterQuery();
        }

        [Route("getdistributioncenterquerylocation/userLocationId/{userLocationId}")]
        [HttpGet]
        public List<ShipmentTrackingPathCodeQuery> getDistributionCenterQuery(string userLocationId)
        {
            return this._shipmenttracking.getDistributionCenterQuery(userLocationId);
        }

        [Route("getLovState")]
        [HttpPost]
        public SearchResultView<LovState> Search(SearchQueryParameters parameters)
        {
            return this._shipmenttracking.SearchWithCriteria(parameters);
        }

        [Route("getLovStatusCode")]
        [HttpPost]
        public SearchResultView<LovStatusCode> SearchWithStatusCode(SearchQueryParameters parameters)
        {
            return this._shipmenttracking.SearchWithStatusCode(parameters);
        }

        [Route("getLovExeptionCode")]
        [HttpPost]
        public SearchResultView<LovExeptionCode> SearchWithExceptionCode(SearchQueryParameters parameters)
        {
            return this._shipmenttracking.SearchWithExceptionCode(parameters);
        }

        [Route("getDeliveryPath")]
        [HttpPost]
        public ShipmentTrackingDeliveryPath getDeliveryPath(ShipmentTrackingDeliveryPathParam paramPath)
        {
            return this._shipmenttracking.getDeliveryPath(paramPath);
        }
    }
}
