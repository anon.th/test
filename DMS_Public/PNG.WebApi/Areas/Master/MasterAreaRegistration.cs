﻿using System.Web.Mvc;

namespace PNG.WebApi.Areas.Master
{
    public class MasterAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "Master";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "Master_Api",
                "Master/{controller}/{action}/{id}",
                new { id = UrlParameter.Optional }
            );
        }
    }
}