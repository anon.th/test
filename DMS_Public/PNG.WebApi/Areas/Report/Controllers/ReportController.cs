﻿using PNG.BusinessLogic.Interfaces;
using PNG.Model.Models;
using PNG.WebApi.Handlers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace PNG.WebApi.Areas.Report.Controllers
{
    [RoutePrefix("api/report")]
    [PNGAuthorize]
    public class ReportController : ApiController
    {
        protected readonly IReportService ReportService;
        
        public ReportController(IReportService reportService)
        {
            this.ReportService = reportService;
        }

        [HttpPost]
        [Route("shipmentTracking")]
        public async Task<List<FileResult>> GetShipmentTrackingReport(SearchShipmentTrackingReport queryParameters)
        {
            try
            {
                //List<FileResult> listFile = new List<FileResult>();
                var listFile = await this.ReportService.GetShipmentTrackingReport(queryParameters);

                return listFile;
            }
            catch (Exception)
            {

                return null;
            }
        }

        [HttpPost]
        [Route("loadcssprintconsnote")]
        public async Task<List<FileResult>> CSS_PrintConsNoteDetail(PrintConsNoteResult printConsNoteParam)
        {
            try
            {
                var listFile = await this.ReportService.GetCSS_PrintConsNoteDetail(printConsNoteParam);

                return listFile;
            }
            catch (Exception)
            {
                return null;
            }
        }

        [HttpPost]
        [Route("loadcssprintshippinglist")]
        public async Task<List<FileResult>> CSS_PrintShippingListDetail(PrintShippingListDetail[] printShippingListParam)
        {
            try
            {
                var listFile = await this.ReportService.GetCSS_PrintShippingListDetail(printShippingListParam);

                return listFile;
            }
            catch (Exception)
            {
                return null;
            }
        }

        [HttpPost]
        [Route("loadcssreprintconsnote")]
        public async Task<List<FileResult>> CSS_ReprintConsNoteDetail(ReprintConsNoteResult reprintConsNoteParam)
        {
            try
            {
                var listFile = await this.ReportService.GetCSS_ReprintConsNoteDetail(reprintConsNoteParam);

                return listFile;
            }
            catch (Exception)
            {
                return null;
            }
        }

        [HttpPost]
        [Route("loadcssreprintconsnotelabel")]
        public async Task<List<FileResult>> CSS_ReprintConsNoteLabelDetail(ReprintConsNoteResult reprintConsNoteParamlabel)
        {
            try
            {
                var listFile = await this.ReportService.GetCSS_ReprintConsNoteLabelDetail(reprintConsNoteParamlabel);

                return listFile;
            }
            catch (Exception)
            {
                return null;
            }
        }

        [HttpGet]
        [Route("testreport")]
        [AllowAnonymous]
        public string TestReport()
        {
            try
            {
                var listFile = this.ReportService.TestReport();

                return listFile;
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}