﻿using System.Web.Http;
using PNG.WebApi.Controllers;

namespace PNG.WebApi.Areas.Testing.Controllers
{
    [RoutePrefix("api/testing/testing")]
    [AllowAnonymous]
    public class TestingController : ApiController
    {
        protected readonly BusinessLogic.Interfaces.ITestConnectionService TestConnectionService = null;

        public TestingController(BusinessLogic.Interfaces.ITestConnectionService testConnectionService)
        {
            this.TestConnectionService = testConnectionService;
        }

        [Route("")]
        [HttpGet]
        public IHttpActionResult SearchTesting()
        {
            return this.Ok(this.TestConnectionService.GetList());
        }
    }
}
