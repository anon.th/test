﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ExtendedClaimsProvider.cs" company="Aware Corporation Limited">
//   Copyright (c) 2014-2015.  All rights reserved.
// </copyright>
// <summary>
//   Defines the ExtendedClaimsProvider type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using Microsoft.Owin.Security;
using PNG.Common.Enums;
using PNG.DataAccess.AspNetIdentities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Security.Claims;
using System.Threading;
using System.Web;

namespace PNG.WebApi.Infrastructures
{
    /// <summary>
    /// The extended claims provider.
    /// </summary>
    public static class ExtendedClaimsProvider
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="user"></param>
        /// <param name="ipAddress"></param>
        /// <returns></returns>
        public static IEnumerable<Claim> GetClaims(ApplicationUser user, string ipAddress, string clientConnectionId)
        {
            var claims = new List<Claim>
            {
                CreateClaim(UserClaim.Id.GetAttributeCode(), user.Id),
                CreateClaim(UserClaim.UserName.GetAttributeCode(), user.UserName),
                //CreateClaim(UserClaim.FirstName.GetAttributeCode(), string.IsNullOrEmpty(user.FirstName) ? string.Empty : user.FirstName),
                //CreateClaim(UserClaim.LastName.GetAttributeCode(), string.IsNullOrEmpty(user.LastName) ? string.Empty : user.LastName),
                CreateClaim(UserClaim.Email.GetAttributeCode(), user.Email),
                //CreateClaim(UserClaim.DefaultRoleId.GetAttributeCode(), user.DefaultRoleId),
                CreateClaim(UserClaim.IpAddress.GetAttributeCode(), ipAddress),
                CreateClaim(UserClaim.CurrentGuid.GetAttributeCode(), clientConnectionId) // This value from IdentityUserExtension.GetApiTokenResponse
            };

            return claims;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="type"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static Claim CreateClaim(string type, string value)
        {
            return new Claim(type, value, ClaimValueTypes.String);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public static void AddOrUpdateClaim(string key, string value)
        {
            var currentPrincipal = Thread.CurrentPrincipal as ClaimsPrincipal;

            if (currentPrincipal == null) return;

            var identity = currentPrincipal.Identity as ClaimsIdentity;
            if (identity == null)
                return;

            // check for existing claim and remove it
            var existingClaim = identity.FindFirst(key);
            if (existingClaim != null)
                identity.RemoveClaim(existingClaim);

            // add new claim
            identity.AddClaim(new Claim(key, value));
            var authenticationManager = HttpContext.Current.GetOwinContext().Authentication;
            authenticationManager.AuthenticationResponseGrant = new AuthenticationResponseGrant(new ClaimsPrincipal(identity),
                new AuthenticationProperties { IsPersistent = true });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="claimType"></param>
        /// <returns></returns>
        public static string GetClaimValue(string claimType)
        {
            var currentClaimsPrincipal = Thread.CurrentPrincipal as ClaimsPrincipal;
            if (currentClaimsPrincipal == null)
                return null;

            var claim = currentClaimsPrincipal.FindFirst(claimType);
            return (claim != null) ? claim.Value : null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="claimType"></param>
        /// <returns></returns>
        public static T GetClaimValue<T>(string claimType)
        {
            var currentClaimsPrincipal = Thread.CurrentPrincipal as ClaimsPrincipal;
            if (currentClaimsPrincipal == null)
                return default(T);

            var claim = currentClaimsPrincipal.FindFirst(claimType);
            var value = (claim != null) ? claim.Value : null;
            if (value == null)
                return default(T);

            var converter = TypeDescriptor.GetConverter(typeof(T));
            return (T)converter.ConvertFromInvariantString(value);
        }
    }
}