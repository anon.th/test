﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ClaimsAuthorizationAttribute.cs" company="Aware Corporation Limited">
//   Copyright (c) 2014-2015.  All rights reserved.
// </copyright>
// <summary>
//   Defines the ClaimsAuthorizationAttribute type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace PNG.WebApi.Infrastructures
{
    /// <summary>
    /// 
    /// </summary>
    public class ClaimsAuthorizationAttribute : AuthorizationFilterAttribute
    {
        /// <summary>
        /// 
        /// </summary>
        public string ClaimType { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string ClaimValue { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="actionContext"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public override Task OnAuthorizationAsync(HttpActionContext actionContext, System.Threading.CancellationToken cancellationToken)
        {
            var principal = actionContext.RequestContext.Principal as ClaimsPrincipal;

            if (principal != null && !principal.Identity.IsAuthenticated)
            {
                actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.Unauthorized);
                return Task.FromResult<object>(null);
            }

            if (principal == null || principal.HasClaim(x => x.Type == ClaimType && x.Value == ClaimValue))
                return Task.FromResult<object>(null);

            actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.Unauthorized);
            return Task.FromResult<object>(null);

            //User is Authorized, complete execution
        }
    }
}