﻿using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security.OAuth;
using PNG.Model;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;

namespace PNG.WebApi.Helpers
{
    /// <summary>
    /// 
    /// </summary>
    public static class IdentityUserExtension
    {
        /// <summary>
        /// The get api token response.
        /// </summary>
        /// <param name="loginModel"></param>
        /// <param name="apiBaseUri">
        /// The api base uri.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public static async Task<HttpResponseMessage> GetApiTokenResponse(UserLogin loginModel, string apiBaseUri)
        {
            using (var client = new HttpClient())
            {
                // setup client
                client.BaseAddress = new Uri(apiBaseUri);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                // setup login data
                var formContent = new FormUrlEncodedContent(new[]
                {
                    new KeyValuePair<string, string>("grant_type", "password"),
                    new KeyValuePair<string, string>("username", loginModel.Username),
                    new KeyValuePair<string, string>("password", loginModel.Password),
                    new KeyValuePair<string, string>("ipaddress", loginModel.IpAddress),
                    new KeyValuePair<string, string>("clientconnectionid", loginModel.ClientConnectionId),
                    new KeyValuePair<string, string>("enterpriseid", loginModel.EnterpriseID),
                    new KeyValuePair<string, string>("usertypeid", loginModel.UserTypeId)
                });

                // send request
                var responseMessage = await client.PostAsync("/oauth/token", formContent);
                return responseMessage;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="token"></param>
        /// <param name="apiBaseUri"></param>
        /// <param name="requestPath"></param>
        /// <returns></returns>
        // ReSharper disable once UnusedMember.Local
        private static async Task<string> GetRequest(string token, string apiBaseUri, string requestPath)
        {
            using (var client = new HttpClient())
            {
                //setup client
                client.BaseAddress = new Uri(apiBaseUri);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Add("Authorization", "Bearer " + token);

                //make request
                var response = await client.GetAsync(requestPath);
                var responseString = await response.Content.ReadAsStringAsync();
                return responseString;
            }
        }

        /// <summary>
        /// get client ip address
        /// </summary>
        /// <param name="context"></param>
        /// <returns>
        /// string ipaddress
        /// </returns>
        public static string GetClientIpAddress(OAuthGrantResourceOwnerCredentialsContext context)
        {
            var clientIpAppress = context.Request.ReadFormAsync().Result["ipaddress"] ?? "undefined";
            return clientIpAppress;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public static string GetClientConnectionId(OAuthGrantResourceOwnerCredentialsContext context)
        {
            var clientConnectionId = context.Request.ReadFormAsync().Result["clientconnectionid"] ?? Guid.NewGuid().ToString();
            return clientConnectionId;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public static string GetEnterpriseId(OAuthGrantResourceOwnerCredentialsContext context)
        {
            var clientConnectionId = context.Request.ReadFormAsync().Result["enterpriseid"] ?? Guid.NewGuid().ToString();
            return clientConnectionId;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public static string GetUserTypeId(OAuthGrantResourceOwnerCredentialsContext context)
        {
            var clientConnectionId = context.Request.ReadFormAsync().Result["usertypeId"] ?? Guid.NewGuid().ToString();
            return clientConnectionId;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static string GetCurrentUserName()
        {
            if (HttpContext.Current.User == null)
            {
                return null;
            }

            var userId = HttpContext.Current.User.Identity.GetUserName();
            return userId;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static string GetCurrentBearerAuthrorizationToken()
        {
            string authToken = null;
            var ctx = HttpContext.Current.GetOwinContext();

            if (ctx == null || ctx.Request.Headers.Get("Authorization") == null)
                return null;

            var headerValue = ctx.Request.Headers.GetValues("Authorization");
            var bearerValue = headerValue[0].Split(' ');

            if (bearerValue.Length == 2)
            {
                authToken = bearerValue[1];
            }

            return authToken;
        }
    }
}