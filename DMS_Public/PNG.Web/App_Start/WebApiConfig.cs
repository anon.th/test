﻿using System.Web;
using System.Web.Http;
using log4net.Config;
using SimpleInjector.Integration.Web.Mvc;
using System.Web.Mvc;
using System.Reflection;
using SimpleInjector;
using PNG.Common.Interfaces.Logging;
using PNG.Common.Services.Logging;
using System.Net.Http;

namespace PNG.Web
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Simple Injector configure
            var container = new Container();

            // Don't use this if set forword url with api proxy handler
            // config.MapHttpAttributeRoutes();

            // Set forword url with api proxy handler
            config.Routes.MapHttpRoute(
                name: "ApiProxy",
                routeTemplate: "api/{*pathinfo}",
                handler: HttpClientFactory.CreatePipeline
                (
                    innerHandler: new HttpClientHandler(), // Will never get here
                    handlers: new DelegatingHandler[]
                    {
                        new ApiProxyHandler(container)
                    }
                ),
                defaults: new { id = RouteParameter.Optional },
                constraints: null);

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            // Logging and Tracing
            XmlConfigurator.Configure();
            container.Register<ILoggingService>(() => new Log4NetLoggingService());

            // This is an extension method from the integration package.
            container.RegisterMvcControllers(Assembly.GetExecutingAssembly());

            // This is an extension method from the integration package as well.
            container.RegisterMvcIntegratedFilterProvider();

            container.Verify();

            var loggingService = container.GetInstance<ILoggingService>();
            loggingService.Configure(HttpContext.Current.Server.MapPath("~/log4net.config"));
            Log.SetLogger(loggingService);

            DependencyResolver.SetResolver(new SimpleInjectorDependencyResolver(container));
        }
    }
}
