﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Optimization;

namespace PNG.Web
{
    public class AngularAreaBundler : IBundleOrderer
    {
        /// <summary>
        ///
        /// </summary>
        private readonly string _area;

        /// <summary>
        ///
        /// </summary>
        /// <param name="area"></param>
        public AngularAreaBundler(string area)
        {
            this._area = area;
            BringToTop = new List<string>();
        }

        /// <summary>
        ///
        /// </summary>
        public IList<string> BringToTop
        {
            get;
            private set;
        }

        /// <inheritdoc />
        ///  <summary>
        ///  </summary>
        ///  <param name="context"></param>
        ///  <param name="files"></param>
        ///  <returns></returns>
        public IEnumerable<BundleFile> OrderFiles(BundleContext context, IEnumerable<BundleFile> files)
        {
            var allFiles = files.ToList();
            var filteredFiles = allFiles
                .Where(f => f.VirtualFile.Name != null
                    && (!BringToTop.Contains(f.VirtualFile.Name, StringComparer.InvariantCultureIgnoreCase)
                    && f.VirtualFile.VirtualPath.Contains($"/{_area}/")));
            return allFiles
                .Where(f => f.VirtualFile.Name != null
                    && BringToTop.Contains(f.VirtualFile.Name, StringComparer.InvariantCultureIgnoreCase))
                .Concat(filteredFiles);
        }
    }

    /// <summary>
    ///
    /// </summary>
    public class CssRewriteUrlTransformWrapper : IItemTransform
    {
        /// <summary>
        ///
        /// </summary>
        /// <param name="includedVirtualPath"></param>
        /// <param name="input"></param>
        /// <returns></returns>
        public string Process(string includedVirtualPath, string input)
        {
            return new CssRewriteUrlTransform().Process("~" + VirtualPathUtility.ToAbsolute(includedVirtualPath), input);
        }
    }

    /// <summary>
    ///
    /// </summary>
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles, bool minify)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                "~/Scripts/jquery-{version}.js",
                "~/Scripts/toastr.js",
                "~/Scripts/angular-file-upload1.2.8.js",
                "~/Scripts/jquery.signalR-2.2.0.min.js"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                "~/Scripts/modernizr-2.6.2"));

            bundles.Add(new ScriptBundle("~/bundles/angular-ui").Include(
                "~/Scripts/angular-block-ui.js",
                "~/Scripts/angular-ui/ui-bootstrap.js",
                "~/Scripts/angular-ui/ui-bootstrap-tpls.js",
                "~/Scripts/lodash.js",
                "~/Scripts/scrolling-tabs.js"
                ));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                "~/Scripts/bootstrap.js",
                "~/Scripts/bootstrap-datepicker.js",
                "~/Scripts/bootstrap-datepicker-thai.js",
                "~/Scripts/locales/bootstrap-datepicker.th.js",
                "~/Scripts/respond.js",
                "~/Scripts/bootstrap-timepicker.js"));

            bundles.Add(new ScriptBundle("~/bundles/angular-core").Include(
                "~/Scripts/angular.js",
                "~/Scripts/angular-block-ui.js",
                "~/Scripts/angular-animate.js",
                "~/Scripts/angular-cookies.js",
                "~/Scripts/angular-file-upload.js",
                "~/Scripts/angular-local-storage.min.js",
                "~/Scripts/angular-resource.js",
                "~/Scripts/angular-route.js",
                "~/Scripts/angular-messages.js",
                "~/Scripts/angular-sanitize.js"));

            bundles.Add(new StyleBundle("~/Content/mainCss").Include(
                "~/Content/angular-block-ui.css",
                "~/Content/mainstyle.css",
                "~/Content/bootstrap.css",
                "~/Content/datepicker.css",
                "~/Content/frontend-mainstyle.css",
                "~/Content/media.css",
                "~/Content/scrolling-tabs.css",
                "~/Content/site.css",
                "~/Content/font-awesome.css",
                "~/Content/style.css",
                "~/Content/toastr.css",
                "~/Content/timepicker.css",
                "~/Content/dialogStyle.css"));

            var select2CssBundle = new StyleBundle("~/Content/select2Css");
            select2CssBundle.Include(
                "~/Content/css/select2.css", new CssRewriteUrlTransformWrapper());
            bundles.Add(select2CssBundle);

            // I have disabled this section because tinymce hardcode to load default theme lightgray. put skin.min.css and content.min.css to ~/bundles/skins/lightgray directory
            //var tinyMceCssBundle = new StyleBundle("~/Content/tinymce");
            //tinyMceCssBundle.Include("~/Scripts/tinymce/plugins/visualblocks/css/visualblocks.min.css", new CssRewriteUrlTransformWrapper());
            //tinyMceCssBundle.Include("~/Scripts/tinymce/skins/lightgray/skin.min.css", new CssRewriteUrlTransformWrapper());
            //tinyMceCssBundle.Include("~/Scripts/tinymce/skins/lightgray/content.min.css", new CssRewriteUrlTransformWrapper());
            //bundles.Add(tinyMceCssBundle);

            bundles.Add(new Bundle("~/bundles/angular-app").Include("~/App/App.js"));

            var toastrBundle = minify
              ? new Bundle("~/bundles/toastr", "//cdnjs.cloudflare.com/ajax/libs/toastr.js/2.0.2/js/toastr.min.js")
              : new Bundle("~/bundles/toastr", "//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.js");
            toastrBundle.Include("~/Scripts/toastr.js");
            bundles.Add(toastrBundle);

            var utilityBundle = new Bundle("~/bundles/utility");
            utilityBundle.Include(
                "~/App/shared/services/angularLinq.js",
                "~/Scripts/FileSaver.min.js");
            bundles.Add(utilityBundle);

            var tinyMceBundle = new Bundle("~/bundles/tinymce");
            tinyMceBundle.Include(
                "~/Scripts/tinymce/tinymce.min.js",
                "~/Scripts/tinymce/plugins/visualblocks/plugin.min.js",
                "~/Scripts/tinymce/themes/modern/theme.min.js ",
                "~/Scripts/tinymce/plugins/textcolor/plugin.min.js",
                "~/Scripts/tinymce/plugins/preview/plugin.min.js");
            bundles.Add(tinyMceBundle);


            Bundle angularAppHelperBundle = new Bundle("~/bundles/angular-app-helper");
            angularAppHelperBundle.Include(
                "~/App/shared/helpers/DateTimeHelper.js",
                "~/App/shared/helpers/FileHelper.js",
                "~/App/shared/helpers/ScopeHelper.js",
                "~/App/shared/helpers/StringHelper.js",
                "~/App/shared/helpers/UtilityHelper.js",
                "~/App/Shared/Validation/Validation.js");
            bundles.Add(angularAppHelperBundle);

            //var recaptchaApi = minify
            //    ? new Bundle("~/bundles/recaptcha-api")
            //    : new Bundle("~/bundles/recaptcha-api");
            //recaptchaApi.Include(
            //    "~/Scripts/recaptcha/api.js");
            //bundles.Add(recaptchaApi);

            var pluginBundle = new Bundle("~/bundles/plugin");
            pluginBundle.Include(
                "~/Scripts/select2.js",
                "~/Scripts/flow/flow.js",
                "~/Scripts/ng-flow/ng-flow.js",
                "~/Scripts/angular-ui-select2.js",
                "~/Scripts/jquery.floatThead.js",
                "~/Scripts/angular-floatThead.js",
                "~/Scripts/maskedPasswordElement.js");
            //"~/Scripts/angular-recaptcha.js");
            bundles.Add(pluginBundle);

            Bundle angularAppDirectivesBundle = minify
               ? new Bundle("~/bundles/angular-app-directives")
               : new Bundle("~/bundles/angular-app-directives");
            angularAppDirectivesBundle.IncludeDirectory("~/App", "*.js", true);
            angularAppDirectivesBundle.IncludeDirectory("~/App", "*.html", true);
            angularAppDirectivesBundle.Orderer = new AngularAreaBundler("directives");
            bundles.Add(angularAppDirectivesBundle);

            Bundle angularAppServicesBundle = new Bundle("~/bundles/angular-app-services");
            angularAppServicesBundle.Orderer = new AngularAreaBundler("services");
            angularAppServicesBundle.IncludeDirectory("~/App", "*.js", true);
            bundles.Add(angularAppServicesBundle);

            Bundle angularAppEnumsBundle = new Bundle("~/bundles/angular-app-enums");
            angularAppEnumsBundle.Orderer = new AngularAreaBundler("enums");
            angularAppEnumsBundle.IncludeDirectory("~/App", "*.js", true);
            bundles.Add(angularAppEnumsBundle);

            Bundle angularAppFiltersBundle = new Bundle("~/bundles/angular-app-filters");
            angularAppFiltersBundle.Include(
                "~/App/shared/filters/buddhaDateFilter.js",
                "~/App/shared/filters/decimalFilter.js",
                "~/App/shared/filters/uniqueFilter.js",
                "~/App/shared/filters/ssnFilter.js");
            angularAppFiltersBundle.Orderer = new AngularAreaBundler("filters");
            bundles.Add(angularAppFiltersBundle);

            // Controller js files
            bundles.Add(new Bundle("~/bundles/angular-app-controllers").Include(
                // Lov
                "~/App/lov/controllers/listOfViewZipCodeController.js",
                "~/App/lov/controllers/listOfViewCostCentreController.js",
                "~/App/lov/controllers/listOfViewPayerCodeController.js",
                "~/App/lov/controllers/listOfViewStateCodeController.js",
                "~/App/lov/controllers/listOfViewStatusCodeController.js",
                "~/App/lov/controllers/listOfViewExceptionCodeController.js",

                // Public
                "~/App/public/controllers/errorController.js",
                "~/App/public/controllers/homeController.js",
                "~/App/public/controllers/loginController.js",
                "~/App/public/controllers/mainWindowController.js",
                "~/App/public/controllers/showMessageController.js",
                "~/App/public/controllers/welcomeController.js",
                "~/App/public/controllers/trackandtraceController.js",
                "~/App/public/controllers/userProfileController.js",
                "~/App/public/controllers/createConsignmentController.js",
                 "~/App/public/controllers/updateConsignmentController.js",
                 "~/App/public/controllers/searchconsignmentController.js",
                 "~/App/public/controllers/shipmentTrackingController.js"
            ));

            // Set EnableOptimizations to false for debugging. For more information,
            // visit http://go.microsoft.com/fwlink/?LinkId=301862
            BundleTable.EnableOptimizations = minify;
        }
    }
}