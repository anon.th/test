var Lov;
(function (Lov) {
    var LovHelperService = (function () {
        function LovHelperService($cookies, $q, createDialog, $window) {
            this.$cookies = $cookies;
            this.$q = $q;
            this.createDialog = createDialog;
            this.$window = $window;
            this.DialogProperties = {
                'width': '900px',
                'overflow': 'auto',
                'max-width': '100%'
            };
            this.DialogPropertiesCosCentre = {
                'width': '1000px',
                'overflow': 'auto',
                'max-width': '100%'
            };
            this.DialogPropertiesPayerCode = {
                'width': '1300px',
                'overflow': 'auto',
                'max-width': '100%'
            };
            this.DialogBodyCss = {
                'max-height': '630px',
                'width': '100%',
                'overflow': 'auto'
            };
            if (!this.CurrentLovParameter) {
                this.ClearLovParameter();
            }
        }
        LovHelperService.Factory = function ($cookies, $q, createDialog, $window) {
            return new LovHelperService($cookies, $q, createDialog, $window);
        };
        LovHelperService.prototype.SetLovParameter = function (lovParameter) {
            this.CurrentLovParameter = lovParameter;
        };
        LovHelperService.prototype.GetLovParameter = function () {
            return this.CurrentLovParameter;
        };
        LovHelperService.prototype.ClearLovParameter = function () {
            this.CurrentLovParameter = {};
        };
        // List of View Company
        LovHelperService.prototype.CreateZipCodeLov = function (lovId) {
            var templateUrl = "/CreateConsignment/LovZipCode/";
            this.createDialog(templateUrl, 'dialog', {
                id: lovId,
                title: "Search postal code",
                showFooter: false,
                css: this.DialogProperties,
                bodycss: this.DialogBodyCss
            });
        };
        // List of View Company
        LovHelperService.prototype.CreateCostCentreLov = function (lovId) {
            var templateUrl = "/CreateConsignment/LovCostCentre/";
            this.createDialog(templateUrl, 'dialog', {
                id: lovId,
                title: "Search cost Centre",
                showFooter: false,
                css: this.DialogPropertiesCosCentre,
                bodycss: this.DialogBodyCss
            });
        };
        LovHelperService.prototype.CreateTelephoneLov = function (lovId) {
            var templateUrl = "/CreateConsignment/LovCostCentre/";
            this.createDialog(templateUrl, 'dialog', {
                id: lovId,
                title: "Search Telephone",
                showFooter: false,
                css: this.DialogPropertiesCosCentre,
                bodycss: this.DialogBodyCss
            });
        };
        LovHelperService.prototype.CreatePayerCodeLov = function (lovId) {
            var templateUrl = "/ShipmentTracking/LovPayerCode/";
            this.createDialog(templateUrl, 'dialog', {
                id: lovId,
                title: "Search payer Code",
                showFooter: false,
                css: this.DialogPropertiesPayerCode,
                bodycss: this.DialogBodyCss
            });
        };
        LovHelperService.prototype.CreateStateCodeLov = function (lovId) {
            var templateUrl = "/ShipmentTracking/LovStateCode/";
            this.createDialog(templateUrl, 'dialog', {
                id: lovId,
                title: "Search state Code",
                showFooter: false,
                css: this.DialogPropertiesCosCentre,
                bodycss: this.DialogBodyCss
            });
        };
        // List of View Company
        LovHelperService.prototype.CreateShipmentZipCodeLov = function (lovId) {
            var templateUrl = "/ShipmentTracking/LovZipCode/";
            this.createDialog(templateUrl, 'dialog', {
                id: lovId,
                title: "Search postal code",
                showFooter: false,
                css: this.DialogProperties,
                bodycss: this.DialogBodyCss
            });
        };
        LovHelperService.prototype.CreateStatusCodeLov = function (lovId) {
            var templateUrl = "/ShipmentTracking/LovStatusCode/";
            this.createDialog(templateUrl, 'dialog', {
                id: lovId,
                title: "Search status Code",
                showFooter: false,
                css: this.DialogPropertiesCosCentre,
                bodycss: this.DialogBodyCss
            });
        };
        LovHelperService.prototype.CreateExceptionCodeLov = function (lovId) {
            var templateUrl = "/ShipmentTracking/LovExceptionCode/";
            this.createDialog(templateUrl, 'dialog', {
                id: lovId,
                title: "Search exception Code",
                showFooter: false,
                css: this.DialogPropertiesCosCentre,
                bodycss: this.DialogBodyCss
            });
        };
        //////Add -------------------------------------------------------------------
        LovHelperService.prototype.SetLovTradeDateParameter = function (lovParameter) {
            this.CurrentLovTradaDateParameter = lovParameter;
        };
        ////////Add TransferMember-------------------------------------------------------------------
        //public SetLovAddTransferMemberParameter(lovParameter: Interfaces.ITransferMember): void
        //{
        //    this.CurrentLovAddTransferMemberParameter = lovParameter;
        //}
        LovHelperService.prototype.GetLovTradeDateParameter = function () {
            return this.CurrentLovTradaDateParameter;
        };
        LovHelperService.prototype.ClearLovTradeDateParameter = function () {
            this.CurrentLovParameter = {};
        };
        return LovHelperService;
    }());
    Lov.LovHelperService = LovHelperService;
    Main.App.Services.factory("lovHelperService", ["$cookies", "$q", "createDialog", "$window", LovHelperService.Factory]);
})(Lov || (Lov = {}));
//# sourceMappingURL=lovHelperService.js.map