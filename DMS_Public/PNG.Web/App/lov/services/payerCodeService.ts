﻿module PayerCode {
    export class PayerCodeService {
        public static Factory(
            $q: ng.IQService,
            webRequestService: Util.WebRequestService,
            configSettings: Interfaces.IConfigSettings,
            $http: any) {
            return new PayerCodeService($q, webRequestService, configSettings, $http);
        }

        constructor(
            public $q: ng.IQService,
            private serviceProxy: Util.WebRequestService,
            private configSettings: Interfaces.IConfigSettings,
            private $http: any
        ) {

        }

        public GetPayerCodeByPagination(queryParameter: Interfaces.ISearchQueryParameter): ng.IPromise<Interfaces.IPayerCode> {
            var uri: string = this.configSettings.apiServiceBaseUri + "api/master/payercode";

            return this.serviceProxy.Post(uri, queryParameter);
        }

    }

    Main.App.Services.factory("payerCodeService", ["$q", "webRequestService", "configSettings", PayerCodeService.Factory]);
}