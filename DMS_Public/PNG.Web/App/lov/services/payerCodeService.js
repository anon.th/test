var PayerCode;
(function (PayerCode) {
    var PayerCodeService = (function () {
        function PayerCodeService($q, serviceProxy, configSettings, $http) {
            this.$q = $q;
            this.serviceProxy = serviceProxy;
            this.configSettings = configSettings;
            this.$http = $http;
        }
        PayerCodeService.Factory = function ($q, webRequestService, configSettings, $http) {
            return new PayerCodeService($q, webRequestService, configSettings, $http);
        };
        PayerCodeService.prototype.GetPayerCodeByPagination = function (queryParameter) {
            var uri = this.configSettings.apiServiceBaseUri + "api/master/payercode";
            return this.serviceProxy.Post(uri, queryParameter);
        };
        return PayerCodeService;
    }());
    PayerCode.PayerCodeService = PayerCodeService;
    Main.App.Services.factory("payerCodeService", ["$q", "webRequestService", "configSettings", PayerCodeService.Factory]);
})(PayerCode || (PayerCode = {}));
//# sourceMappingURL=payerCodeService.js.map