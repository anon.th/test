﻿module StateCode {
    export class StateCodeService {
        public static Factory(
            $q: ng.IQService,
            webRequestService: Util.WebRequestService,
            configSettings: Interfaces.IConfigSettings,
            $http: any) {
            return new StateCodeService($q, webRequestService, configSettings, $http);
        }

        constructor(
            public $q: ng.IQService,
            private serviceProxy: Util.WebRequestService,
            private configSettings: Interfaces.IConfigSettings,
            private $http: any
        ) {

        }

        public GetStateCodeByPagination(queryParameter: Interfaces.ISearchQueryParameter): ng.IPromise<Interfaces.IStateCode> {
            var uri: string = this.configSettings.apiServiceBaseUri + "api/master/shipmenttracking/getLovState";

            return this.serviceProxy.Post(uri, queryParameter);
        }

    }

    Main.App.Services.factory("stateCodeService", ["$q", "webRequestService", "configSettings", StateCodeService.Factory]);
}