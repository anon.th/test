﻿module StatusCode {
    export class StatusCodeService {
        public static Factory(
            $q: ng.IQService,
            webRequestService: Util.WebRequestService,
            configSettings: Interfaces.IConfigSettings,
            $http: any) {
            return new StatusCodeService($q, webRequestService, configSettings, $http);
        }

        constructor(
            public $q: ng.IQService,
            private serviceProxy: Util.WebRequestService,
            private configSettings: Interfaces.IConfigSettings,
            private $http: any
        ) {

        }

        public GetStatusCodeByPagination(queryParameter: Interfaces.ISearchQueryParameter): ng.IPromise<Interfaces.IStatusCode> {
            var uri: string = this.configSettings.apiServiceBaseUri + "api/master/shipmenttracking/getLovStatusCode";

            return this.serviceProxy.Post(uri, queryParameter);
        }

        public GetCodevalue(param: Interfaces.ISearchShipmentTrackingParam): ng.IPromise<Interfaces.ISearchShipmentTrackingGetCodeValue[]> {
            var uri: string = this.configSettings.apiServiceBaseUri + "api/master/shipmenttracking/getcodevalues";
            return this.serviceProxy.Post(uri, param);
        }

        public GetCodevalue2(param: Interfaces.ISearchShipmentTrackingParam): ng.IPromise<Interfaces.ISearchShipmentTrackingGetCodeValue[]> 
        {
            param.codeid = "status_type";
            param.userid = "AWARE";
            param.user_culture = "en-US";
            var uri: string = this.configSettings.apiServiceBaseUri + "api/master/shipmenttracking/getcodevalues";
            return this.serviceProxy.Post(uri, param);
        }

    }

    Main.App.Services.factory("statusCodeService", ["$q", "webRequestService", "configSettings", StatusCodeService.Factory]);
}