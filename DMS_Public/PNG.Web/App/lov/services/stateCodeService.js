var StateCode;
(function (StateCode) {
    var StateCodeService = (function () {
        function StateCodeService($q, serviceProxy, configSettings, $http) {
            this.$q = $q;
            this.serviceProxy = serviceProxy;
            this.configSettings = configSettings;
            this.$http = $http;
        }
        StateCodeService.Factory = function ($q, webRequestService, configSettings, $http) {
            return new StateCodeService($q, webRequestService, configSettings, $http);
        };
        StateCodeService.prototype.GetStateCodeByPagination = function (queryParameter) {
            var uri = this.configSettings.apiServiceBaseUri + "api/master/shipmenttracking/getLovState";
            return this.serviceProxy.Post(uri, queryParameter);
        };
        return StateCodeService;
    }());
    StateCode.StateCodeService = StateCodeService;
    Main.App.Services.factory("stateCodeService", ["$q", "webRequestService", "configSettings", StateCodeService.Factory]);
})(StateCode || (StateCode = {}));
//# sourceMappingURL=stateCodeService.js.map