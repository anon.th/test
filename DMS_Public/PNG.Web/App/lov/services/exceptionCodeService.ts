﻿module ExceptionCode {
    export class ExceptionCodeService {
        public static Factory(
            $q: ng.IQService,
            webRequestService: Util.WebRequestService,
            configSettings: Interfaces.IConfigSettings,
            $http: any) {
            return new ExceptionCodeService($q, webRequestService, configSettings, $http);
        }

        constructor(
            public $q: ng.IQService,
            private serviceProxy: Util.WebRequestService,
            private configSettings: Interfaces.IConfigSettings,
            private $http: any
        ) {

        }

        public GetExceptionCodeByPagination(queryParameter: Interfaces.ISearchQueryParameter): ng.IPromise<Interfaces.IStatusCode> {
            var uri: string = this.configSettings.apiServiceBaseUri + "api/master/shipmenttracking/getLovExeptionCode";

            return this.serviceProxy.Post(uri, queryParameter);
        }

        public GetCodevalue(param: Interfaces.ISearchShipmentTrackingParam): ng.IPromise<Interfaces.ISearchShipmentTrackingGetCodeValue[]> {
            var uri: string = this.configSettings.apiServiceBaseUri + "api/master/shipmenttracking/getcodevalues";
            return this.serviceProxy.Post(uri, param);
        }



    }

    Main.App.Services.factory("exceptionCodeService", ["$q", "webRequestService", "configSettings", ExceptionCodeService.Factory]);
}