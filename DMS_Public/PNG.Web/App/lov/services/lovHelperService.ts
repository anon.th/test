﻿module Lov
{
    export class LovHelperService
    {
        private CurrentLovTradaDateParameter: Interfaces.ILovTradeDate;
        private CurrentLovParameter: Interfaces.IListOfViewParameter;
        private DialogProperties: any;
        private DialogPropertiesCosCentre: any;
        private DialogPropertiesPayerCode: any;
        private DialogBodyCss: any;
        public static Factory(
            $cookies: Interfaces.ICookies,
            $q: ng.IQService,
            createDialog: any,
            $window: any
        )
        {
            return new LovHelperService($cookies, $q, createDialog, $window);
        }

        constructor(
            private $cookies: Interfaces.ICookies,
            private $q: ng.IQService,
            private createDialog: any,
            private $window: any)
        {
            this.DialogProperties = {
                'width': '900px',
                'overflow': 'auto',
                'max-width': '100%'
            };

            this.DialogPropertiesCosCentre = {
                'width': '1000px',
                'overflow': 'auto',
                'max-width': '100%'
            };

            this.DialogPropertiesPayerCode = {
                'width': '1300px',
                'overflow': 'auto',
                'max-width': '100%'
            };

            this.DialogBodyCss = {
                'max-height': '630px',
                'width': '100%',
                'overflow': 'auto'
            }

            if (!this.CurrentLovParameter)
            {
                this.ClearLovParameter();
            }
        }

        public SetLovParameter(lovParameter: Interfaces.IListOfViewParameter): void
        {
            this.CurrentLovParameter = lovParameter;
        }

        public GetLovParameter(): Interfaces.IListOfViewParameter
        {
            return this.CurrentLovParameter;
        }

        public ClearLovParameter(): void
        {
            this.CurrentLovParameter = {};
        }

        // List of View Company
        public CreateZipCodeLov(lovId: string): void
        {
            var templateUrl = "/CreateConsignment/LovZipCode/";
            this.createDialog(templateUrl, 'dialog',
                {
                    id: lovId,
                    title: "Search postal code",
                    showFooter: false,
                    css: this.DialogProperties,
                    bodycss: this.DialogBodyCss
                });
        }

        // List of View Company
        public CreateCostCentreLov(lovId: string): void
        {
            var templateUrl = "/CreateConsignment/LovCostCentre/";
            this.createDialog(templateUrl, 'dialog',
                {
                    id: lovId,
                    title: "Search cost Centre",
                    showFooter: false,
                    css: this.DialogPropertiesCosCentre,
                    bodycss: this.DialogBodyCss
                });
        }

        public CreateTelephoneLov(lovId: string): void {
            var templateUrl = "/CreateConsignment/LovCostCentre/";
            this.createDialog(templateUrl, 'dialog',
                {
                    id: lovId,
                    title: "Search Telephone",
                    showFooter: false,
                    css: this.DialogPropertiesCosCentre,
                    bodycss: this.DialogBodyCss
                });
        }

        public CreatePayerCodeLov(lovId: string): void {
            var templateUrl = "/ShipmentTracking/LovPayerCode/";
            this.createDialog(templateUrl, 'dialog',
                {
                    id: lovId,
                    title: "Search payer Code",
                    showFooter: false,
                    css: this.DialogPropertiesPayerCode,
                    bodycss: this.DialogBodyCss
                });
        }

        public CreateStateCodeLov(lovId: string): void {
            var templateUrl = "/ShipmentTracking/LovStateCode/";
            this.createDialog(templateUrl, 'dialog',
                {
                    id: lovId,
                    title: "Search state Code",
                    showFooter: false,
                    css: this.DialogPropertiesCosCentre,
                    bodycss: this.DialogBodyCss
                });
        }

        // List of View Company
        public CreateShipmentZipCodeLov(lovId: string): void {
            var templateUrl = "/ShipmentTracking/LovZipCode/";
            this.createDialog(templateUrl, 'dialog',
                {
                    id: lovId,
                    title: "Search postal code",
                    showFooter: false,
                    css: this.DialogProperties,
                    bodycss: this.DialogBodyCss
                });
        }

        public CreateStatusCodeLov(lovId: string): void {
            var templateUrl = "/ShipmentTracking/LovStatusCode/";
            this.createDialog(templateUrl, 'dialog',
                {
                    id: lovId,
                    title: "Search status Code",
                    showFooter: false,
                    css: this.DialogPropertiesCosCentre,
                    bodycss: this.DialogBodyCss
                });
        }

        public CreateExceptionCodeLov(lovId: string): void {
            var templateUrl = "/ShipmentTracking/LovExceptionCode/";
            this.createDialog(templateUrl, 'dialog',
                {
                    id: lovId,
                    title: "Search exception Code",
                    showFooter: false,
                    css: this.DialogPropertiesCosCentre,
                    bodycss: this.DialogBodyCss
                });
        }


        //////Add -------------------------------------------------------------------
        public SetLovTradeDateParameter(lovParameter: Interfaces.ILovTradeDate): void
        {
            this.CurrentLovTradaDateParameter = lovParameter;
        }

        ////////Add TransferMember-------------------------------------------------------------------
        //public SetLovAddTransferMemberParameter(lovParameter: Interfaces.ITransferMember): void
        //{
        //    this.CurrentLovAddTransferMemberParameter = lovParameter;
        //}
        public GetLovTradeDateParameter(): Interfaces.ILovTradeDate
        {
            return this.CurrentLovTradaDateParameter;
        }

        public ClearLovTradeDateParameter(): void
        {
            this.CurrentLovParameter = {};
        }



        ////open lov CompanyBoard
        //public OpenCompanyBoard(lovId: number): void
        //{
        //    var templateUrl = "/Master/ExternalIdentityUsers/LovCompanyBoard/";
        //    this.createDialog(templateUrl, 'dialog',
        //        {
        //            id: lovId,
        //            title: this.$translate.instant('ExternalIdentityUsers.CompanyBoardLabel'),
        //            showFooter: false,
        //            css: this.DialogProperties,
        //            bodycss: this.DialogBodyCss
        //        });
        //}
    }

    Main.App.Services.factory("lovHelperService", ["$cookies", "$q", "createDialog", "$window", LovHelperService.Factory]);
}  
