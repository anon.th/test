var ExceptionCode;
(function (ExceptionCode) {
    var ExceptionCodeService = (function () {
        function ExceptionCodeService($q, serviceProxy, configSettings, $http) {
            this.$q = $q;
            this.serviceProxy = serviceProxy;
            this.configSettings = configSettings;
            this.$http = $http;
        }
        ExceptionCodeService.Factory = function ($q, webRequestService, configSettings, $http) {
            return new ExceptionCodeService($q, webRequestService, configSettings, $http);
        };
        ExceptionCodeService.prototype.GetExceptionCodeByPagination = function (queryParameter) {
            var uri = this.configSettings.apiServiceBaseUri + "api/master/shipmenttracking/getLovExeptionCode";
            return this.serviceProxy.Post(uri, queryParameter);
        };
        ExceptionCodeService.prototype.GetCodevalue = function (param) {
            var uri = this.configSettings.apiServiceBaseUri + "api/master/shipmenttracking/getcodevalues";
            return this.serviceProxy.Post(uri, param);
        };
        return ExceptionCodeService;
    }());
    ExceptionCode.ExceptionCodeService = ExceptionCodeService;
    Main.App.Services.factory("exceptionCodeService", ["$q", "webRequestService", "configSettings", ExceptionCodeService.Factory]);
})(ExceptionCode || (ExceptionCode = {}));
//# sourceMappingURL=exceptionCodeService.js.map