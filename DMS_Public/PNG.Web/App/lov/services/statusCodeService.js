var StatusCode;
(function (StatusCode) {
    var StatusCodeService = (function () {
        function StatusCodeService($q, serviceProxy, configSettings, $http) {
            this.$q = $q;
            this.serviceProxy = serviceProxy;
            this.configSettings = configSettings;
            this.$http = $http;
        }
        StatusCodeService.Factory = function ($q, webRequestService, configSettings, $http) {
            return new StatusCodeService($q, webRequestService, configSettings, $http);
        };
        StatusCodeService.prototype.GetStatusCodeByPagination = function (queryParameter) {
            var uri = this.configSettings.apiServiceBaseUri + "api/master/shipmenttracking/getLovStatusCode";
            return this.serviceProxy.Post(uri, queryParameter);
        };
        StatusCodeService.prototype.GetCodevalue = function (param) {
            var uri = this.configSettings.apiServiceBaseUri + "api/master/shipmenttracking/getcodevalues";
            return this.serviceProxy.Post(uri, param);
        };
        StatusCodeService.prototype.GetCodevalue2 = function (param) {
            param.codeid = "status_type";
            param.userid = "AWARE";
            param.user_culture = "en-US";
            var uri = this.configSettings.apiServiceBaseUri + "api/master/shipmenttracking/getcodevalues";
            return this.serviceProxy.Post(uri, param);
        };
        return StatusCodeService;
    }());
    StatusCode.StatusCodeService = StatusCodeService;
    Main.App.Services.factory("statusCodeService", ["$q", "webRequestService", "configSettings", StatusCodeService.Factory]);
})(StatusCode || (StatusCode = {}));
//# sourceMappingURL=statusCodeService.js.map