﻿module Interfaces
{
    export interface ICostCentre
    {
        IsSelected?: boolean;
    }
}
module CostCentre
{
    export interface IListOfViewCostCentreController extends ng.IScope
    {
        vm: ListOfViewCostCentreController;
        ListDisplayCostCentre: Interfaces.ISearchResultView<Interfaces.ICostCentre>;
        LovParamCostCentre: Interfaces.ILovCostCentre;
        ListSelectedReferences: Interfaces.ICostCentre[];
        IsMultipleSelected: boolean;
        IsHideSearchStatus: boolean;
        IsShowLock: boolean;

        SearchParameter: Interfaces.ISearchQueryParameter;
        SelectedPageLink: Interfaces.IPageLink;
    }

    export class ListOfViewCostCentreController
    {
        public static $inject = ["$scope", "costCentreService", "$rootScope", "lovHelperService", "errorHandlerService"];
        public WatchEvents: Function;
        private broadcastName: string = "ListOfViewReferences";
        private isCheckedAll: boolean = false;
        private status: Number = 1;
        constructor(private $scope: IListOfViewCostCentreController,
            private costCentreService: CostCentre.CostCentreService,
            private $rootScope: ng.IScope,
            private lovHelperService: Lov.LovHelperService,
            private errorHandlerService: Shared.ErrorHandlerService)
        {
            $scope.vm = this;
            this.$scope.ListSelectedReferences = [];

            //Initial method
            this.InitializeListOfViewZipCode();
            this.AddHandlerSorting();
            this.InitializeSearchParameter();
            this.LoadData();

            this.AddHandlerPageLink();
        }

        private InitializeListOfViewZipCode(): void
        {
            //set parameter Lov company
            var lovParameter = this.lovHelperService.GetLovParameter();

            if (lovParameter)
            {
                //copy parameter to this Lov
                this.$scope.LovParamCostCentre = angular.copy(lovParameter.SearchFilter);

                if (lovParameter.BroadcastName)
                {
                    this.broadcastName = lovParameter.BroadcastName;
                }

                if (this.$scope.LovParamCostCentre.ListSelected)
                {
                    this.$scope.ListSelectedReferences = this.$scope.LovParamCostCentre.ListSelected;
                }

                if (lovParameter.IsHideSearchStatus)
                {
                    this.$scope.IsHideSearchStatus = lovParameter.IsHideSearchStatus;
                    if (lovParameter.Active >= -1 && lovParameter.Active <= 1)
                    {
                        //-1 = All, 1 = Active, 0 = Inactive
                        this.$scope.vm.status = lovParameter.Active;
                    }
                    else
                    {
                        this.$scope.vm.status = 1;
                    }
                }

                this.$scope.IsMultipleSelected = lovParameter.IsMultipleSelected;

                if (lovParameter.IsShowLock)
                    this.$scope.IsShowLock = lovParameter.IsShowLock;
                else this.$scope.IsShowLock = false;
            }
            else
            {
                //clear parameter this Lov
                this.$scope.LovParamCostCentre = null;
            }
        }

        private LoadData(): ng.IPromise<void>
        {
            //if (this.$scope.IsShowLock === true && this.$scope.LovParamCostCentre.FundId != null)
            //{
            //    return this.zipCodeService.GetZipCodeByPagination(this.$scope.SearchParameter, this.$scope.LovParamCostCentre.FundId)
            //        .then((companyResult: Interfaces.ICompany[]) =>
            //        {
            //            this.SetDataToDisplay(companyResult);
            //        },
            //        ((error) =>
            //        {
            //            this.errorHandlerService.HandleException(error, "GetZipCodeByPagination");
            //        }));
            //}
            //else
            //{
            return this.costCentreService.GetCostCentreByPagination(this.$scope.SearchParameter)
                .then((costCentreResult: Interfaces.ICostCentre[]) =>
                {
                    this.SetDataToDisplay(costCentreResult);
                },
                ((error) =>
                {
                    this.errorHandlerService.HandleException(error, "GetcostCentreResultByPagination");
                }));
            //}
        }

        private SetDataToDisplay(costcentreResult: Interfaces.ICostCentre[])
        {
            this.$scope.ListDisplayCostCentre = costcentreResult;
            //this.$rootScope.$broadcast(this.broadcastName, [true, zipcode]);
            //เช็คผลค้นหาที่ได้ตรงกับที่ค้นหา
            if (this.$scope.ListDisplayCostCentre.TotalRecord == 1)
            {
                if (this.$scope.IsMultipleSelected == false)
                {
                    var zipcode: Interfaces.IZipCode = this.$scope.ListDisplayCostCentre.SearchResults[0];

                    //if (this.$scope.IsShowLock === false
                    //    || (this.$scope.IsShowLock === true && company.IsLockCompany === false))
                    //{
                    this.$rootScope.$broadcast(this.broadcastName, [true, zipcode]);
                    return;
                    //}
                }
                //else if (this.$scope.IsMultipleSelected == true)
                //{
                //    var search: Interfaces.ICompany[] = this.$scope.ListSelectedCompany.filter(c => c.Id == this.$scope.ListDisplayCompany.SearchResults[0].Id);
                //    if (search.length == 0)
                //    {
                //        this.$scope.ListSelectedCompany.push(this.$scope.ListDisplayCompany.SearchResults[0]);
                //    }

                //    this.$rootScope.$broadcast(this.broadcastName, [true, this.$scope.ListSelectedCompany]);
                //}
            }

            //this.SetCheckboxOfListSelected();
        }

        // เซต display checkbox จาก list selected (ที่เคยเลือก)
        private SetCheckboxOfListSelected(): void
        {
            this.$scope.vm.isCheckedAll = false;

            this.$scope.ListDisplayCostCentre.SearchResults.forEach(costcentre =>
            {
                var search: Interfaces.ICostCentre[] = this.$scope.ListSelectedReferences.filter(c => c.telephone == costcentre.telephone);
                if (search.length > 0)
                {
                    costcentre.IsSelected = true;
                }
            });
        }

        private AddHandlerSorting(): void
        {
            //On event from sorting
            this.WatchEvents = this.$scope.$watch(Shared.PvdConstant.ColumnSorting, (current, old) =>
            {
                if (!current || (current == old)) return;
                this.$scope.SearchParameter.Page = 1;
                this.LoadData();
            });
        }

        public SortingDisplayIcon(): string
        {
            return this.$scope.SearchParameter.SortAscending ? Shared.ColumnIcon.ArrowUp : Shared.ColumnIcon.ArrowDown;
        }

        public SortingColumn(columnName: string): void
        {
            if (this.$scope.SearchParameter.SortColumn == columnName)
            {
                this.$scope.SearchParameter.SortAscending = !this.$scope.SearchParameter.SortAscending;
            } else
            {
                this.$scope.SearchParameter.SortColumn = columnName;
                this.$scope.SearchParameter.SortAscending = true;
            }
        }

        private AddHandlerPageLink(): void
        {
            //On event from page link changed
            this.WatchEvents = this.$scope.$watch(Shared.PvdConstant.SelectedPageLink, (current, old) =>
            {
                if (!current || (current == old)) return;
                this.$scope.SearchParameter.Page = this.$scope.SelectedPageLink.Page;
                this.LoadData();
            });
        }

        public InitializeSearchParameter(): void
        {
            this.WatchEvents();
            this.$scope.SearchParameter = {
                FilterList: this.GenerateFilterList(),
                ItemsPerPage: Shared.ValueHelper.RecordsPerPageLov,
                Page: this.$scope.SearchParameter != undefined ? this.$scope.SearchParameter.Page : 1,
                SortAscending: true,
                SortColumn: "[References].telephone"
            };
            this.AddHandlerSorting();
        }

        private GenerateFilterList(): Interfaces.ISearchFilterView[]
        {
            var result: Interfaces.ISearchFilterView[] = [];
            var condition: Interfaces.ISearchFilterView;

            if (this.$scope.LovParamCostCentre)
            {
                if (this.$scope.LovParamCostCentre.telephone)
                {
                    condition = {
                        ConditionType: Enums.SearchConditionType.And,
                        OperatorType: Enums.SearchOperatorType.Like,
                        TableName: "[References]",
                        FieldName: "telephone",
                        ParameterName: "ptelephone",
                        Value: this.$scope.LovParamCostCentre.telephone
                    };
                    result.push(condition);
                }

                if (this.$scope.LovParamCostCentre.cost_centre)
                {
                    condition = {
                        ConditionType: Enums.SearchConditionType.And,
                        OperatorType: Enums.SearchOperatorType.Like,
                        TableName: "[References]",
                        FieldName: "cost_centre",
                        ParameterName: "pcost_centre",
                        Value: this.$scope.LovParamCostCentre.cost_centre
                    };
                    result.push(condition);
                }

                if (this.$scope.LovParamCostCentre.reference_name)
                {
                    condition = {
                        ConditionType: Enums.SearchConditionType.And,
                        OperatorType: Enums.SearchOperatorType.Like,
                        TableName: "[References]",
                        FieldName: "reference_name",
                        ParameterName: "preference_name",
                        Value: this.$scope.LovParamCostCentre.reference_name
                    };
                    result.push(condition);
                }

            }
            return result;
        }

        public SearchData()
        {
            //On search
            this.$scope.SearchParameter.Page = 1;
            this.AddHandlerSorting();
            this.InitializeSearchParameter();
            this.LoadData();
        }

        public SelectedAll(): void
        {
            this.$scope.ListDisplayCostCentre.SearchResults.forEach(zipcode =>
            {
                zipcode.IsSelected = this.$scope.vm.isCheckedAll;
                this.AddOrRemoveRow(zipcode);
            });
        }

        public SelectRow(zipcode: Interfaces.IZipCode): void
        {
            if (!this.$scope.IsMultipleSelected)
            {
                if (this.$scope.IsShowLock === false
                    || (this.$scope.IsShowLock === true))
                {
                    this.$rootScope.$broadcast(this.broadcastName, [true, zipcode]);
                    return;
                }
            }
            zipcode.IsSelected = (zipcode.IsSelected) ? false : true;

            this.AddOrRemoveRow(zipcode);
        }

        public AddOrRemoveRow(zipcode: Interfaces.IZipCode): void
        {
            if (zipcode.IsSelected)
            {
                //ถ้า chk เป็น true จะแอดเข้า list
                var search: Interfaces.IZipCode[] = this.$scope.ListSelectedReferences.filter(c => c.zipcode == zipcode.zipcode);
                if (search.length == 0)
                {
                    this.$scope.ListSelectedReferences.push(zipcode);
                }
            }
            else
            {
                var index = 0;
                this.$scope.ListSelectedReferences.forEach(c =>
                {
                    if (c.zipcode == zipcode.zipcode)
                    {
                        this.$scope.ListSelectedReferences.splice(index, 1);
                    }
                    index++;
                });
            }
        }

        public ListOfViewCancel(): void
        {
            //On cancel
            if (!this.$scope.IsHideSearchStatus)
            {
                this.$scope.vm.status = 1;
            }
            if (this.$scope.LovParamCostCentre)
            {
                this.$scope.LovParamCostCentre.telephone = null;
                this.$scope.LovParamCostCentre.cost_centre = null;
                this.$scope.LovParamCostCentre.reference_name = null;
            }
            this.$scope.ListSelectedReferences = [];
            this.$scope.SearchParameter.Page = 1;
            this.AddHandlerSorting();
            this.InitializeSearchParameter();
            this.LoadData();
        }

        public ListOfViewSubmit(): void
        {
            this.$rootScope.$broadcast(this.broadcastName, [true, this.$scope.ListSelectedReferences]);
        }
    }

    Main.App.Controllers.controller("listOfViewCostCentreController", ListOfViewCostCentreController);
}