var ZipCode;
(function (ZipCode) {
    var ListOfViewZipCodeController = (function () {
        function ListOfViewZipCodeController($scope, zipCodeService, $rootScope, lovHelperService, errorHandlerService) {
            this.$scope = $scope;
            this.zipCodeService = zipCodeService;
            this.$rootScope = $rootScope;
            this.lovHelperService = lovHelperService;
            this.errorHandlerService = errorHandlerService;
            this.broadcastName = "ListOfViewZipCode";
            this.isCheckedAll = false;
            this.status = 1;
            $scope.vm = this;
            this.$scope.ListSelectedZipCode = [];
            //Initial method
            this.InitializeListOfViewZipCode();
            this.AddHandlerSorting();
            this.InitializeSearchParameter();
            this.LoadData();
            this.AddHandlerPageLink();
        }
        ListOfViewZipCodeController.prototype.InitializeListOfViewZipCode = function () {
            //set parameter Lov company
            var lovParameter = this.lovHelperService.GetLovParameter();
            if (lovParameter) {
                //copy parameter to this Lov
                this.$scope.LovParamZipCode = angular.copy(lovParameter.SearchFilter);
                if (lovParameter.BroadcastName) {
                    this.broadcastName = lovParameter.BroadcastName;
                }
                if (this.$scope.LovParamZipCode.ListSelected) {
                    this.$scope.ListSelectedZipCode = this.$scope.LovParamZipCode.ListSelected;
                }
                if (lovParameter.IsHideSearchStatus) {
                    this.$scope.IsHideSearchStatus = lovParameter.IsHideSearchStatus;
                    if (lovParameter.Active >= -1 && lovParameter.Active <= 1) {
                        //-1 = All, 1 = Active, 0 = Inactive
                        this.$scope.vm.status = lovParameter.Active;
                    }
                    else {
                        this.$scope.vm.status = 1;
                    }
                }
                this.$scope.IsMultipleSelected = lovParameter.IsMultipleSelected;
                if (lovParameter.IsShowLock)
                    this.$scope.IsShowLock = lovParameter.IsShowLock;
                else
                    this.$scope.IsShowLock = false;
            }
            else {
                //clear parameter this Lov
                this.$scope.LovParamZipCode = {};
            }
        };
        ListOfViewZipCodeController.prototype.LoadData = function () {
            var _this = this;
            //if (this.$scope.IsShowLock === true && this.$scope.LovParamZipCode.FundId != null)
            //{
            //    return this.zipCodeService.GetZipCodeByPagination(this.$scope.SearchParameter, this.$scope.LovParamZipCode.FundId)
            //        .then((companyResult: Interfaces.ICompany[]) =>
            //        {
            //            this.SetDataToDisplay(companyResult);
            //        },
            //        ((error) =>
            //        {
            //            this.errorHandlerService.HandleException(error, "GetZipCodeByPagination");
            //        }));
            //}
            //else
            //{
            return this.zipCodeService.GetReferencesByPagination(this.$scope.SearchParameter)
                .then(function (zipCodeResult) {
                _this.SetDataToDisplay(zipCodeResult);
            }, (function (error) {
                _this.errorHandlerService.HandleException(error, "GetZipCodeByPagination");
            }));
            //}
        };
        ListOfViewZipCodeController.prototype.SetDataToDisplay = function (zipCodeResult) {
            this.$scope.ListDisplayZipCode = zipCodeResult;
            //this.$rootScope.$broadcast(this.broadcastName, [true, zipcode]);
            //เช็คผลค้นหาที่ได้ตรงกับที่ค้นหา
            if (this.$scope.ListDisplayZipCode.TotalRecord == 1) {
                if (this.$scope.IsMultipleSelected == false) {
                    var zipcode = this.$scope.ListDisplayZipCode.SearchResults[0];
                    //if (this.$scope.IsShowLock === false
                    //    || (this.$scope.IsShowLock === true && company.IsLockCompany === false))
                    //{
                    this.$rootScope.$broadcast(this.broadcastName, [true, zipcode]);
                    return;
                }
            }
            //this.SetCheckboxOfListSelected();
        };
        // เซต display checkbox จาก list selected (ที่เคยเลือก)
        ListOfViewZipCodeController.prototype.SetCheckboxOfListSelected = function () {
            var _this = this;
            this.$scope.vm.isCheckedAll = false;
            this.$scope.ListDisplayZipCode.SearchResults.forEach(function (zipcode) {
                var search = _this.$scope.ListSelectedZipCode.filter(function (c) { return c.zipcode == zipcode.zipcode; });
                if (search.length > 0) {
                    zipcode.IsSelected = true;
                }
            });
        };
        ListOfViewZipCodeController.prototype.AddHandlerSorting = function () {
            var _this = this;
            //On event from sorting
            this.WatchEvents = this.$scope.$watch(Shared.PvdConstant.ColumnSorting, function (current, old) {
                if (!current || (current == old))
                    return;
                _this.$scope.SearchParameter.Page = 1;
                _this.LoadData();
            });
        };
        ListOfViewZipCodeController.prototype.SortingDisplayIcon = function () {
            return this.$scope.SearchParameter.SortAscending ? Shared.ColumnIcon.ArrowUp : Shared.ColumnIcon.ArrowDown;
        };
        ListOfViewZipCodeController.prototype.SortingColumn = function (columnName) {
            if (this.$scope.SearchParameter.SortColumn == columnName) {
                this.$scope.SearchParameter.SortAscending = !this.$scope.SearchParameter.SortAscending;
            }
            else {
                this.$scope.SearchParameter.SortColumn = columnName;
                this.$scope.SearchParameter.SortAscending = true;
            }
        };
        ListOfViewZipCodeController.prototype.AddHandlerPageLink = function () {
            var _this = this;
            //On event from page link changed
            this.WatchEvents = this.$scope.$watch(Shared.PvdConstant.SelectedPageLink, function (current, old) {
                if (!current || (current == old))
                    return;
                _this.$scope.SearchParameter.Page = _this.$scope.SelectedPageLink.Page;
                _this.LoadData();
            });
        };
        ListOfViewZipCodeController.prototype.InitializeSearchParameter = function () {
            this.WatchEvents();
            this.$scope.SearchParameter = {
                FilterList: this.GenerateFilterList(),
                ItemsPerPage: Shared.ValueHelper.RecordsPerPageLov,
                Page: this.$scope.SearchParameter != undefined ? this.$scope.SearchParameter.Page : 1,
                SortAscending: true,
                SortColumn: "zipcode.zipcode"
            };
            this.AddHandlerSorting();
        };
        ListOfViewZipCodeController.prototype.GenerateFilterList = function () {
            var result = [];
            var condition;
            if (this.$scope.LovParamZipCode) {
                condition = {
                    ConditionType: Enums.SearchConditionType.And,
                    OperatorType: Enums.SearchOperatorType.Equal,
                    TableName: "ZipCode",
                    FieldName: "applicationid",
                    ParameterName: "applicationid",
                    Value: "TIES"
                };
                result.push(condition);
                condition = {
                    ConditionType: Enums.SearchConditionType.And,
                    OperatorType: Enums.SearchOperatorType.Equal,
                    TableName: "ZipCode",
                    FieldName: "enterpriseid",
                    ParameterName: "enterpriseid",
                    Value: "PNGAF"
                };
                result.push(condition);
                if (this.$scope.LovParamZipCode.zipcode) {
                    condition = {
                        ConditionType: Enums.SearchConditionType.And,
                        OperatorType: Enums.SearchOperatorType.Like,
                        TableName: "zipcode",
                        FieldName: "zipcode",
                        ParameterName: "pzipcode",
                        Value: this.$scope.LovParamZipCode.zipcode
                    };
                    result.push(condition);
                }
                if (this.$scope.LovParamZipCode.Country) {
                    condition = {
                        ConditionType: Enums.SearchConditionType.And,
                        OperatorType: Enums.SearchOperatorType.Equal,
                        TableName: "zipcode",
                        FieldName: "country",
                        ParameterName: "country",
                        Value: this.$scope.LovParamZipCode.Country
                    };
                    result.push(condition);
                }
            }
            return result;
        };
        ListOfViewZipCodeController.prototype.SearchData = function () {
            //On search
            this.$scope.SearchParameter.Page = 1;
            this.AddHandlerSorting();
            this.InitializeSearchParameter();
            this.LoadData();
        };
        ListOfViewZipCodeController.prototype.SelectedAll = function () {
            var _this = this;
            this.$scope.ListDisplayZipCode.SearchResults.forEach(function (zipcode) {
                zipcode.IsSelected = _this.$scope.vm.isCheckedAll;
                _this.AddOrRemoveRow(zipcode);
            });
        };
        ListOfViewZipCodeController.prototype.SelectRow = function (zipcode) {
            if (!this.$scope.IsMultipleSelected) {
                if (this.$scope.IsShowLock === false
                    || (this.$scope.IsShowLock === true)) {
                    this.$rootScope.$broadcast(this.broadcastName, [true, zipcode]);
                    return;
                }
            }
            zipcode.IsSelected = (zipcode.IsSelected) ? false : true;
            this.AddOrRemoveRow(zipcode);
        };
        ListOfViewZipCodeController.prototype.AddOrRemoveRow = function (zipcode) {
            var _this = this;
            if (zipcode.IsSelected) {
                //ถ้า chk เป็น true จะแอดเข้า list
                var search = this.$scope.ListSelectedZipCode.filter(function (c) { return c.zipcode == zipcode.zipcode; });
                if (search.length == 0) {
                    this.$scope.ListSelectedZipCode.push(zipcode);
                }
            }
            else {
                var index = 0;
                this.$scope.ListSelectedZipCode.forEach(function (c) {
                    if (c.zipcode == zipcode.zipcode) {
                        _this.$scope.ListSelectedZipCode.splice(index, 1);
                    }
                    index++;
                });
            }
        };
        ListOfViewZipCodeController.prototype.ListOfViewCancel = function () {
            //On cancel
            if (!this.$scope.IsHideSearchStatus) {
                this.$scope.vm.status = 1;
            }
            if (this.$scope.LovParamZipCode) {
                this.$scope.LovParamZipCode.zipcode = null;
                this.$scope.LovParamZipCode.Name = null;
            }
            this.$scope.ListSelectedZipCode = [];
            this.$scope.SearchParameter.Page = 1;
            this.AddHandlerSorting();
            this.InitializeSearchParameter();
            this.LoadData();
        };
        ListOfViewZipCodeController.prototype.ListOfViewSubmit = function () {
            this.$rootScope.$broadcast(this.broadcastName, [true, this.$scope.ListSelectedZipCode]);
        };
        ListOfViewZipCodeController.$inject = ["$scope", "zipCodeService", "$rootScope", "lovHelperService", "errorHandlerService"];
        return ListOfViewZipCodeController;
    }());
    ZipCode.ListOfViewZipCodeController = ListOfViewZipCodeController;
    Main.App.Controllers.controller("listOfViewZipCodeController", ListOfViewZipCodeController);
})(ZipCode || (ZipCode = {}));
//# sourceMappingURL=listOfViewZipCodeController.js.map