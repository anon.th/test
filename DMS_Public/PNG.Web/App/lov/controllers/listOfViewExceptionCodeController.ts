﻿module Interfaces {
    export interface IExceptionCode {
        IsSelected?: boolean;
    }
}
module ExceptionCode {
    export interface IListOfViewExceptionCodeController extends ng.IScope {
        vm: ListOfViewExceptionCodeController;
        ListDisplayExceptionCode: Interfaces.ISearchResultView<Interfaces.IExceptionCode>;
        LovParamExceptionCode: Interfaces.ILovExceptionCode;
        ListSelectedReferences: Interfaces.IExceptionCode[];
        IsMultipleSelected: boolean;
        IsHideSearchStatus: boolean;
        IsShowLock: boolean;
        paramShipment2: Interfaces.ISearchShipmentTrackingParam;
        mbgList: Interfaces.ISearchShipmentTrackingGetCodeValue[];
        statusCloseList: Interfaces.ISearchShipmentTrackingGetCodeValue[];
        invoiceList: Interfaces.ISearchShipmentTrackingGetCodeValue[];

        SearchParameter: Interfaces.ISearchQueryParameter;
        SearchParameterCustom: Interfaces.ISearchQueryParameterCustom;
        SelectedPageLink: Interfaces.IPageLink;
    }

    export class ListOfViewExceptionCodeController {
        public static $inject = ["$scope", "exceptionCodeService", "$rootScope", "lovHelperService", "errorHandlerService", "shipmentTrackingService"];
        public WatchEvents: Function;
        private broadcastName: string = "ExceptionCodePopup";
        private isCheckedAll: boolean = false;
        private status: Number = 1;
        public selctedMbg: string;
        public selctedStatusClose: string;
        public selctedInvoiceable: string;
        constructor(private $scope: IListOfViewExceptionCodeController,
            private exceptionCodeService: ExceptionCode.ExceptionCodeService,
            private $rootScope: ng.IScope,
            private lovHelperService: Lov.LovHelperService,
            private errorHandlerService: Shared.ErrorHandlerService,
            private shipmentTrackingService: ShipmentTracking.ShipmentTrackingService) {
            $scope.vm = this;
            this.$scope.paramShipment2 = {};
            this.$scope.ListSelectedReferences = [];
            this.$scope.mbgList = [];
            this.$scope.statusCloseList = [];
            this.$scope.invoiceList = [];

            //Initial method
            this.InitializeListOfViewExceptionCode();
            this.AddHandlerSorting();
            this.InitializeSearchParameter();

            this.loadMbg().then(() => {
                this.loadStatusClose().then(() => {
                    this.loadInvoice();
                });
            });


            this.LoadData();

            this.AddHandlerPageLink();
        }

        private InitializeListOfViewExceptionCode(): void {
            //set parameter Lov company
            var lovParameter = this.lovHelperService.GetLovParameter();

            if (lovParameter) {
                //copy parameter to this Lov
                this.$scope.LovParamExceptionCode = angular.copy(lovParameter.SearchFilter);
                this.$scope.paramShipment2.userid = this.$scope.LovParamExceptionCode.userid;
                this.$scope.paramShipment2.user_culture = this.$scope.LovParamExceptionCode.culture;

                if (lovParameter.BroadcastName) {
                    this.broadcastName = lovParameter.BroadcastName;
                }

                if (this.$scope.LovParamExceptionCode.ListSelected) {
                    this.$scope.ListSelectedReferences = this.$scope.LovParamExceptionCode.ListSelected;
                }

                if (lovParameter.IsHideSearchStatus) {
                    this.$scope.IsHideSearchStatus = lovParameter.IsHideSearchStatus;
                    if (lovParameter.Active >= -1 && lovParameter.Active <= 1) {
                        //-1 = All, 1 = Active, 0 = Inactive
                        this.$scope.vm.status = lovParameter.Active;
                    }
                    else {
                        this.$scope.vm.status = 1;
                    }
                }

                this.$scope.IsMultipleSelected = lovParameter.IsMultipleSelected;

                if (lovParameter.IsShowLock)
                    this.$scope.IsShowLock = lovParameter.IsShowLock;
                else this.$scope.IsShowLock = false;
            }
            else {
                //clear parameter this Lov
                this.$scope.LovParamExceptionCode = null;
            }
        }

        private LoadData(): ng.IPromise<void> {
            return this.exceptionCodeService.GetExceptionCodeByPagination(this.$scope.SearchParameter)
                .then((Result: Interfaces.IStatusCode[]) => {
                    this.SetDataToDisplay(Result);
                },
                ((error) => {
                    this.errorHandlerService.HandleException(error, "GetStatusCodeByPagination");
                }));

        }

        private loadMbg(): ng.IPromise<void> {
            this.$scope.paramShipment2.codeid = "mbg";
            return this.shipmentTrackingService.GetCodevalue(this.$scope.paramShipment2).then((result: Interfaces.ISearchShipmentTrackingGetCodeValue[]) => {
                if (result != null && result.length > 0) {
                    this.$scope.mbgList = result;
                }

            }, (error) => {
                this.errorHandlerService.HandleException(error, "");
            });
        }
        private loadStatusClose(): ng.IPromise<void> {

            this.$scope.paramShipment2.codeid = "close_status";

            return this.exceptionCodeService.GetCodevalue(this.$scope.paramShipment2).then((result: Interfaces.ISearchShipmentTrackingGetCodeValue[]) => {
                if (result != null && result.length > 0) {
                    this.$scope.statusCloseList = result;
                }

            }, (error) => {
                this.errorHandlerService.HandleException(error, "");
            });
        }
        private loadInvoice(): ng.IPromise<void> {

            this.$scope.paramShipment2.codeid = "invoiceable";
            return this.exceptionCodeService.GetCodevalue(this.$scope.paramShipment2).then((result: Interfaces.ISearchShipmentTrackingGetCodeValue[]) => {
                if (result != null && result.length > 0) {
                    this.$scope.invoiceList = result;
                }

            }, (error) => {
                this.errorHandlerService.HandleException(error, "");
            });
        }

        private SetDataToDisplay(Result: Interfaces.IStatusCode[]) {
            this.$scope.ListDisplayExceptionCode = Result;
            //this.$rootScope.$broadcast(this.broadcastName, [true, zipcode]);
            //เช็คผลค้นหาที่ได้ตรงกับที่ค้นหา
            if (this.$scope.ListDisplayExceptionCode.TotalRecord == 1) {
                if (this.$scope.IsMultipleSelected == false) {
                    var statecode: Interfaces.IStatusCode = this.$scope.ListDisplayExceptionCode.SearchResults[0];

                    //if (this.$scope.IsShowLock === false
                    //    || (this.$scope.IsShowLock === true && company.IsLockCompany === false))
                    //{
                    this.$rootScope.$broadcast(this.broadcastName, [true, statecode]);
                    return;
                    //}
                }
            }

            //this.SetCheckboxOfListSelected();
        }

        // เซต display checkbox จาก list selected (ที่เคยเลือก)
        private SetCheckboxOfListSelected(): void {
            this.$scope.vm.isCheckedAll = false;

            this.$scope.ListDisplayExceptionCode.SearchResults.forEach(statecode => {
                var search: Interfaces.IStatusCode[] = this.$scope.ListSelectedReferences.filter(c => c.status_code == statecode.status_code);
                if (search.length > 0) {
                    statecode.IsSelected = true;
                }
            });
        }

        private AddHandlerSorting(): void {
            //On event from sorting
            this.WatchEvents = this.$scope.$watch(Shared.PvdConstant.ColumnSorting, (current, old) => {
                if (!current || (current == old)) return;
                this.$scope.SearchParameter.Page = 1;
                this.LoadData();
            });
        }

        public SortingDisplayIcon(): string {
            return this.$scope.SearchParameter.SortAscending ? Shared.ColumnIcon.ArrowUp : Shared.ColumnIcon.ArrowDown;
        }

        public SortingColumn(columnName: string): void {
            if (this.$scope.SearchParameter.SortColumn == columnName) {
                this.$scope.SearchParameter.SortAscending = !this.$scope.SearchParameter.SortAscending;
            } else {
                this.$scope.SearchParameter.SortColumn = columnName;
                this.$scope.SearchParameter.SortAscending = true;
            }
        }

        private AddHandlerPageLink(): void {
            //On event from page link changed
            this.WatchEvents = this.$scope.$watch(Shared.PvdConstant.SelectedPageLink, (current, old) => {
                if (!current || (current == old)) return;
                this.$scope.SearchParameter.Page = this.$scope.SelectedPageLink.Page;
                this.LoadData();
            });
        }

        public InitializeSearchParameter(): void {
            this.WatchEvents();
            this.$scope.SearchParameter = {
                FilterList: this.GenerateFilterList(),
                ItemsPerPage: Shared.ValueHelper.RecordsPerPageDetail,
                Page: this.$scope.SearchParameter != undefined ? this.$scope.SearchParameter.Page : 1,
                SortAscending: true,
                SortColumn: "[Exception_Code].status_code"
            };
            this.AddHandlerSorting();
        }

        private GenerateFilterList(): Interfaces.ISearchFilterView[] {
            var result: Interfaces.ISearchFilterView[] = [];
            var condition: Interfaces.ISearchFilterView;
            if (this.$scope.LovParamExceptionCode) {


                if (this.$scope.LovParamExceptionCode.status_code) {
                    condition = {
                        ConditionType: Enums.SearchConditionType.And,
                        OperatorType: Enums.SearchOperatorType.Like,
                        TableName: "[Exception_Code]",
                        FieldName: "status_code",
                        ParameterName: "pstatus_code",
                        Value: this.$scope.LovParamExceptionCode.status_code
                    };
                    result.push(condition);
                }
                if (this.$scope.LovParamExceptionCode.exception_code) {
                    condition = {
                        ConditionType: Enums.SearchConditionType.And,
                        OperatorType: Enums.SearchOperatorType.Like,
                        TableName: "[Exception_Code]",
                        FieldName: "exception_code",
                        ParameterName: "pexception_code",
                        Value: this.$scope.LovParamExceptionCode.exception_code
                    };
                    result.push(condition);
                }
                if (this.selctedMbg) {
                    condition = {
                        ConditionType: Enums.SearchConditionType.And,
                        OperatorType: Enums.SearchOperatorType.Like,
                        TableName: "[Exception_Code]",
                        FieldName: "mbg",
                        ParameterName: "pmbg",
                        Value: this.selctedMbg
                    };
                    result.push(condition);
                }
                if (this.selctedStatusClose) {
                    condition = {
                        ConditionType: Enums.SearchConditionType.And,
                        OperatorType: Enums.SearchOperatorType.Like,
                        TableName: "[Exception_Code]",
                        FieldName: "status_close",
                        ParameterName: "pstatus_close",
                        Value: this.selctedStatusClose
                    };
                    result.push(condition);
                }
                if (this.selctedInvoiceable) {
                    condition = {
                        ConditionType: Enums.SearchConditionType.And,
                        OperatorType: Enums.SearchOperatorType.Like,
                        TableName: "[Exception_Code]",
                        FieldName: "invoiceable",
                        ParameterName: "pinvoiceable",
                        Value: this.selctedInvoiceable
                    };
                    result.push(condition);
                }


            }
            return result;
        }

        public SearchData() {
            //On search
            this.$scope.SearchParameter.Page = 1;
            //this.$scope.SearchParameterCustom.Page = 1;



            this.AddHandlerSorting();
            this.InitializeSearchParameter();
            this.LoadData();
        }

        public SelectedAll(): void {
            this.$scope.ListDisplayExceptionCode.SearchResults.forEach(zipcode => {
                zipcode.IsSelected = this.$scope.vm.isCheckedAll;
                this.AddOrRemoveRow(zipcode);
            });
        }

        public SelectRow(statecode: Interfaces.IStatusCode): void {
            if (!this.$scope.IsMultipleSelected) {
                if (this.$scope.IsShowLock === false
                    || (this.$scope.IsShowLock === true)) {
                    this.$rootScope.$broadcast(this.broadcastName, [true, statecode]);
                    return;
                }
            }
            statecode.IsSelected = (statecode.IsSelected) ? false : true;

            this.AddOrRemoveRow(statecode);
        }

        public AddOrRemoveRow(statecode: Interfaces.IStatusCode): void {
            if (statecode.IsSelected) {
                //ถ้า chk เป็น true จะแอดเข้า list
                var search: Interfaces.IStatusCode[] = this.$scope.ListSelectedReferences.filter(c => c.status_code == statecode.status_code);
                if (search.length == 0) {
                    this.$scope.ListSelectedReferences.push(statecode);
                }
            }
            else {
                var index = 0;
                this.$scope.ListSelectedReferences.forEach(c => {
                    if (c.status_code == statecode.status_code) {
                        this.$scope.ListSelectedReferences.splice(index, 1);
                    }
                    index++;
                });
            }
        }

        public ListOfViewCancel(): void {
            //On cancel
            if (!this.$scope.IsHideSearchStatus) {
                this.$scope.vm.status = 1;
            }
            if (this.$scope.LovParamExceptionCode) {
                this.$scope.LovParamExceptionCode.status_code = null;
                this.$scope.LovParamExceptionCode.status_description = null;
            }
            if (this.selctedMbg) {
                this.selctedMbg = null;
                this.$scope.mbgList = [];
                this.loadMbg();
            }
            if (this.selctedStatusClose) {
                this.selctedStatusClose = null;
                this.$scope.statusCloseList = [];
                this.loadStatusClose();
            }
            if (this.selctedInvoiceable) {
                this.selctedInvoiceable = null;
                this.$scope.invoiceList = [];
                this.loadInvoice();
            }

            this.$scope.ListSelectedReferences = [];
            this.$scope.SearchParameter.Page = 1;
            this.AddHandlerSorting();
            this.InitializeSearchParameter();
            this.LoadData();
        }

        public ListOfViewSubmit(): void {
            this.$rootScope.$broadcast(this.broadcastName, [true, this.$scope.ListSelectedReferences]);
        }
    }

    Main.App.Controllers.controller("listOfViewExceptionCodeController", ListOfViewExceptionCodeController);
}