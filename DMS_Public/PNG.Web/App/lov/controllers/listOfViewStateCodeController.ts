﻿module Interfaces {
    export interface IStateCode {
        IsSelected?: boolean;
    }
}
module StateCode {
    export interface IListOfViewStateCodeController extends ng.IScope {
        vm: ListOfViewStateCodeController;
        ListDisplayStateCode: Interfaces.ISearchResultView<Interfaces.IStateCode>;
        LovParamStateCode: Interfaces.ILovStateCode;
        ListSelectedReferences: Interfaces.IStateCode[];
        IsMultipleSelected: boolean;
        IsHideSearchStatus: boolean;
        IsShowLock: boolean;

        SearchParameter: Interfaces.ISearchQueryParameter;
        SelectedPageLink: Interfaces.IPageLink;
    }

    export class ListOfViewStateCodeController {
        public static $inject = ["$scope", "stateCodeService", "$rootScope", "lovHelperService", "errorHandlerService"];
        public WatchEvents: Function;
        private broadcastName: string = "StateCodePopup";
        private isCheckedAll: boolean = false;
        private status: Number = 1;
        constructor(private $scope: IListOfViewStateCodeController,
            private stateCodeService: StateCode.StateCodeService,
            private $rootScope: ng.IScope,
            private lovHelperService: Lov.LovHelperService,
            private errorHandlerService: Shared.ErrorHandlerService) {
            $scope.vm = this;
            this.$scope.ListSelectedReferences = [];
            //Initial method
            this.InitializeListOfViewStateCode();
            this.AddHandlerSorting();
            this.InitializeSearchParameter();
            this.LoadData();

            this.AddHandlerPageLink();
        }

        private InitializeListOfViewStateCode(): void {
            //set parameter Lov company
            var lovParameter = this.lovHelperService.GetLovParameter();

            if (lovParameter) {
                //copy parameter to this Lov
                this.$scope.LovParamStateCode = angular.copy(lovParameter.SearchFilter);

                if (lovParameter.BroadcastName) {
                    this.broadcastName = lovParameter.BroadcastName;
                }

                if (this.$scope.LovParamStateCode.ListSelected) {
                    this.$scope.ListSelectedReferences = this.$scope.LovParamStateCode.ListSelected;
                }

                if (lovParameter.IsHideSearchStatus) {
                    this.$scope.IsHideSearchStatus = lovParameter.IsHideSearchStatus;
                    if (lovParameter.Active >= -1 && lovParameter.Active <= 1) {
                        //-1 = All, 1 = Active, 0 = Inactive
                        this.$scope.vm.status = lovParameter.Active;
                    }
                    else {
                        this.$scope.vm.status = 1;
                    }
                }

                this.$scope.IsMultipleSelected = lovParameter.IsMultipleSelected;

                if (lovParameter.IsShowLock)
                    this.$scope.IsShowLock = lovParameter.IsShowLock;
                else this.$scope.IsShowLock = false;
            }
            else {
                //clear parameter this Lov
                this.$scope.LovParamStateCode = null;
            }
        }

        private LoadData(): ng.IPromise<void> {
            //if (this.$scope.IsShowLock === true && this.$scope.LovParamCostCentre.FundId != null)
            //{
            //    return this.zipCodeService.GetZipCodeByPagination(this.$scope.SearchParameter, this.$scope.LovParamCostCentre.FundId)
            //        .then((companyResult: Interfaces.ICompany[]) =>
            //        {
            //            this.SetDataToDisplay(companyResult);
            //        },
            //        ((error) =>
            //        {
            //            this.errorHandlerService.HandleException(error, "GetZipCodeByPagination");
            //        }));
            //}
            //else
            //{
            return this.stateCodeService.GetStateCodeByPagination(this.$scope.SearchParameter)
                .then((Result: Interfaces.IStateCode[]) => {
                    this.SetDataToDisplay(Result);
                },
                ((error) => {
                    this.errorHandlerService.HandleException(error, "GetStateCodeByPagination");
                }));
            //}
        }

        private SetDataToDisplay(Result: Interfaces.IStateCode[]) {
            this.$scope.ListDisplayStateCode = Result;
            //this.$rootScope.$broadcast(this.broadcastName, [true, zipcode]);
            //เช็คผลค้นหาที่ได้ตรงกับที่ค้นหา
            if (this.$scope.ListDisplayStateCode.TotalRecord == 1) {
                if (this.$scope.IsMultipleSelected == false) {
                    var statecode: Interfaces.IStateCode = this.$scope.ListDisplayStateCode.SearchResults[0];

                    //if (this.$scope.IsShowLock === false
                    //    || (this.$scope.IsShowLock === true && company.IsLockCompany === false))
                    //{
                    this.$rootScope.$broadcast(this.broadcastName, [true, statecode]);
                    return;
                    //}
                }
                //else if (this.$scope.IsMultipleSelected == true)
                //{
                //    var search: Interfaces.ICompany[] = this.$scope.ListSelectedCompany.filter(c => c.Id == this.$scope.ListDisplayCompany.SearchResults[0].Id);
                //    if (search.length == 0)
                //    {
                //        this.$scope.ListSelectedCompany.push(this.$scope.ListDisplayCompany.SearchResults[0]);
                //    }

                //    this.$rootScope.$broadcast(this.broadcastName, [true, this.$scope.ListSelectedCompany]);
                //}
            }

            //this.SetCheckboxOfListSelected();
        }

        // เซต display checkbox จาก list selected (ที่เคยเลือก)
        private SetCheckboxOfListSelected(): void {
            this.$scope.vm.isCheckedAll = false;

            this.$scope.ListDisplayStateCode.SearchResults.forEach(statecode => {
                var search: Interfaces.IStateCode[] = this.$scope.ListSelectedReferences.filter(c => c.state_code == statecode.state_code);
                if (search.length > 0) {
                    statecode.IsSelected = true;
                }
            });
        }

        private AddHandlerSorting(): void {
            //On event from sorting
            this.WatchEvents = this.$scope.$watch(Shared.PvdConstant.ColumnSorting, (current, old) => {
                if (!current || (current == old)) return;
                this.$scope.SearchParameter.Page = 1;
                this.LoadData();
            });
        }

        public SortingDisplayIcon(): string {
            return this.$scope.SearchParameter.SortAscending ? Shared.ColumnIcon.ArrowUp : Shared.ColumnIcon.ArrowDown;
        }

        public SortingColumn(columnName: string): void {
            if (this.$scope.SearchParameter.SortColumn == columnName) {
                this.$scope.SearchParameter.SortAscending = !this.$scope.SearchParameter.SortAscending;
            } else {
                this.$scope.SearchParameter.SortColumn = columnName;
                this.$scope.SearchParameter.SortAscending = true;
            }
        }

        private AddHandlerPageLink(): void {
            //On event from page link changed
            this.WatchEvents = this.$scope.$watch(Shared.PvdConstant.SelectedPageLink, (current, old) => {
                if (!current || (current == old)) return;
                this.$scope.SearchParameter.Page = this.$scope.SelectedPageLink.Page;
                this.LoadData();
            });
        }

        public InitializeSearchParameter(): void {
            this.WatchEvents();
            this.$scope.SearchParameter = {
                FilterList: this.GenerateFilterList(),
                ItemsPerPage: Shared.ValueHelper.RecordsPerPageDetail,
                Page: this.$scope.SearchParameter != undefined ? this.$scope.SearchParameter.Page : 1,
                SortAscending: true,
                SortColumn: "[State].country"
            };
            this.AddHandlerSorting();
        }

        private GenerateFilterList(): Interfaces.ISearchFilterView[] {
            var result: Interfaces.ISearchFilterView[] = [];
            var condition: Interfaces.ISearchFilterView;
            if (this.$scope.LovParamStateCode) {
                if (this.$scope.LovParamStateCode.applicationid) {
                    condition = {
                        ConditionType: Enums.SearchConditionType.And,
                        OperatorType: Enums.SearchOperatorType.Equal,
                        TableName: "[State]",
                        FieldName: "applicationid",
                        ParameterName: "papplicationid",
                        Value: this.$scope.LovParamStateCode.applicationid
                    };
                    result.push(condition);
                }

                if (this.$scope.LovParamStateCode.enterpriseid) {
                    condition = {
                        ConditionType: Enums.SearchConditionType.And,
                        OperatorType: Enums.SearchOperatorType.Equal,
                        TableName: "[State]",
                        FieldName: "enterpriseid",
                        ParameterName: "penterpriseid",
                        Value: this.$scope.LovParamStateCode.enterpriseid
                    };
                    result.push(condition);
                }

                if (this.$scope.LovParamStateCode.country) {
                    condition = {
                        ConditionType: Enums.SearchConditionType.And,
                        OperatorType: Enums.SearchOperatorType.Like,
                        TableName: "[State]",
                        FieldName: "country",
                        ParameterName: "pcountry",
                        Value: this.$scope.LovParamStateCode.country
                    };
                    result.push(condition);
                }
                if (this.$scope.LovParamStateCode.state_code) {
                    condition = {
                        ConditionType: Enums.SearchConditionType.And,
                        OperatorType: Enums.SearchOperatorType.Like,
                        TableName: "[State]",
                        FieldName: "state_code",
                        ParameterName: "pstate_code",
                        Value: this.$scope.LovParamStateCode.state_code
                    };
                    result.push(condition);
                }
                

            }
            return result;
        }

        public SearchData() {
            //On search
            this.$scope.SearchParameter.Page = 1;
            this.AddHandlerSorting();
            this.InitializeSearchParameter();
            this.LoadData();
        }

        public SelectedAll(): void {
            this.$scope.ListDisplayStateCode.SearchResults.forEach(zipcode => {
                zipcode.IsSelected = this.$scope.vm.isCheckedAll;
                this.AddOrRemoveRow(zipcode);
            });
        }

        public SelectRow(statecode: Interfaces.IStateCode): void {
            if (!this.$scope.IsMultipleSelected) {
                if (this.$scope.IsShowLock === false
                    || (this.$scope.IsShowLock === true)) {
                    this.$rootScope.$broadcast(this.broadcastName, [true, statecode]);
                    return;
                }
            }
            statecode.IsSelected = (statecode.IsSelected) ? false : true;

            this.AddOrRemoveRow(statecode);
        }

        public AddOrRemoveRow(statecode: Interfaces.IStateCode): void {
            if (statecode.IsSelected) {
                //ถ้า chk เป็น true จะแอดเข้า list
                var search: Interfaces.IStateCode[] = this.$scope.ListSelectedReferences.filter(c => c.state_code == statecode.state_code);
                if (search.length == 0) {
                    this.$scope.ListSelectedReferences.push(statecode);
                }
            }
            else {
                var index = 0;
                this.$scope.ListSelectedReferences.forEach(c => {
                    if (c.state_code == statecode.state_code) {
                        this.$scope.ListSelectedReferences.splice(index, 1);
                    }
                    index++;
                });
            }
        }

        public ListOfViewCancel(): void {
            //On cancel
            if (!this.$scope.IsHideSearchStatus) {
                this.$scope.vm.status = 1;
            }
            if (this.$scope.LovParamStateCode) {
                this.$scope.LovParamStateCode.country = null;
                this.$scope.LovParamStateCode.state_code = null;
            }
            this.$scope.ListSelectedReferences = [];
            this.$scope.SearchParameter.Page = 1;
            this.AddHandlerSorting();
            this.InitializeSearchParameter();
            this.LoadData();
        }

        public ListOfViewSubmit(): void {
            this.$rootScope.$broadcast(this.broadcastName, [true, this.$scope.ListSelectedReferences]);
        }
    }

    Main.App.Controllers.controller("listOfViewStateCodeController", ListOfViewStateCodeController);
}