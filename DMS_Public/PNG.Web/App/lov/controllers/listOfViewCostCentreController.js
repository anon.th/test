var CostCentre;
(function (CostCentre) {
    var ListOfViewCostCentreController = (function () {
        function ListOfViewCostCentreController($scope, costCentreService, $rootScope, lovHelperService, errorHandlerService) {
            this.$scope = $scope;
            this.costCentreService = costCentreService;
            this.$rootScope = $rootScope;
            this.lovHelperService = lovHelperService;
            this.errorHandlerService = errorHandlerService;
            this.broadcastName = "ListOfViewReferences";
            this.isCheckedAll = false;
            this.status = 1;
            $scope.vm = this;
            this.$scope.ListSelectedReferences = [];
            //Initial method
            this.InitializeListOfViewZipCode();
            this.AddHandlerSorting();
            this.InitializeSearchParameter();
            this.LoadData();
            this.AddHandlerPageLink();
        }
        ListOfViewCostCentreController.prototype.InitializeListOfViewZipCode = function () {
            //set parameter Lov company
            var lovParameter = this.lovHelperService.GetLovParameter();
            if (lovParameter) {
                //copy parameter to this Lov
                this.$scope.LovParamCostCentre = angular.copy(lovParameter.SearchFilter);
                if (lovParameter.BroadcastName) {
                    this.broadcastName = lovParameter.BroadcastName;
                }
                if (this.$scope.LovParamCostCentre.ListSelected) {
                    this.$scope.ListSelectedReferences = this.$scope.LovParamCostCentre.ListSelected;
                }
                if (lovParameter.IsHideSearchStatus) {
                    this.$scope.IsHideSearchStatus = lovParameter.IsHideSearchStatus;
                    if (lovParameter.Active >= -1 && lovParameter.Active <= 1) {
                        //-1 = All, 1 = Active, 0 = Inactive
                        this.$scope.vm.status = lovParameter.Active;
                    }
                    else {
                        this.$scope.vm.status = 1;
                    }
                }
                this.$scope.IsMultipleSelected = lovParameter.IsMultipleSelected;
                if (lovParameter.IsShowLock)
                    this.$scope.IsShowLock = lovParameter.IsShowLock;
                else
                    this.$scope.IsShowLock = false;
            }
            else {
                //clear parameter this Lov
                this.$scope.LovParamCostCentre = null;
            }
        };
        ListOfViewCostCentreController.prototype.LoadData = function () {
            var _this = this;
            //if (this.$scope.IsShowLock === true && this.$scope.LovParamCostCentre.FundId != null)
            //{
            //    return this.zipCodeService.GetZipCodeByPagination(this.$scope.SearchParameter, this.$scope.LovParamCostCentre.FundId)
            //        .then((companyResult: Interfaces.ICompany[]) =>
            //        {
            //            this.SetDataToDisplay(companyResult);
            //        },
            //        ((error) =>
            //        {
            //            this.errorHandlerService.HandleException(error, "GetZipCodeByPagination");
            //        }));
            //}
            //else
            //{
            return this.costCentreService.GetCostCentreByPagination(this.$scope.SearchParameter)
                .then(function (costCentreResult) {
                _this.SetDataToDisplay(costCentreResult);
            }, (function (error) {
                _this.errorHandlerService.HandleException(error, "GetcostCentreResultByPagination");
            }));
            //}
        };
        ListOfViewCostCentreController.prototype.SetDataToDisplay = function (costcentreResult) {
            this.$scope.ListDisplayCostCentre = costcentreResult;
            //this.$rootScope.$broadcast(this.broadcastName, [true, zipcode]);
            //เช็คผลค้นหาที่ได้ตรงกับที่ค้นหา
            if (this.$scope.ListDisplayCostCentre.TotalRecord == 1) {
                if (this.$scope.IsMultipleSelected == false) {
                    var zipcode = this.$scope.ListDisplayCostCentre.SearchResults[0];
                    //if (this.$scope.IsShowLock === false
                    //    || (this.$scope.IsShowLock === true && company.IsLockCompany === false))
                    //{
                    this.$rootScope.$broadcast(this.broadcastName, [true, zipcode]);
                    return;
                }
            }
            //this.SetCheckboxOfListSelected();
        };
        // เซต display checkbox จาก list selected (ที่เคยเลือก)
        ListOfViewCostCentreController.prototype.SetCheckboxOfListSelected = function () {
            var _this = this;
            this.$scope.vm.isCheckedAll = false;
            this.$scope.ListDisplayCostCentre.SearchResults.forEach(function (costcentre) {
                var search = _this.$scope.ListSelectedReferences.filter(function (c) { return c.telephone == costcentre.telephone; });
                if (search.length > 0) {
                    costcentre.IsSelected = true;
                }
            });
        };
        ListOfViewCostCentreController.prototype.AddHandlerSorting = function () {
            var _this = this;
            //On event from sorting
            this.WatchEvents = this.$scope.$watch(Shared.PvdConstant.ColumnSorting, function (current, old) {
                if (!current || (current == old))
                    return;
                _this.$scope.SearchParameter.Page = 1;
                _this.LoadData();
            });
        };
        ListOfViewCostCentreController.prototype.SortingDisplayIcon = function () {
            return this.$scope.SearchParameter.SortAscending ? Shared.ColumnIcon.ArrowUp : Shared.ColumnIcon.ArrowDown;
        };
        ListOfViewCostCentreController.prototype.SortingColumn = function (columnName) {
            if (this.$scope.SearchParameter.SortColumn == columnName) {
                this.$scope.SearchParameter.SortAscending = !this.$scope.SearchParameter.SortAscending;
            }
            else {
                this.$scope.SearchParameter.SortColumn = columnName;
                this.$scope.SearchParameter.SortAscending = true;
            }
        };
        ListOfViewCostCentreController.prototype.AddHandlerPageLink = function () {
            var _this = this;
            //On event from page link changed
            this.WatchEvents = this.$scope.$watch(Shared.PvdConstant.SelectedPageLink, function (current, old) {
                if (!current || (current == old))
                    return;
                _this.$scope.SearchParameter.Page = _this.$scope.SelectedPageLink.Page;
                _this.LoadData();
            });
        };
        ListOfViewCostCentreController.prototype.InitializeSearchParameter = function () {
            this.WatchEvents();
            this.$scope.SearchParameter = {
                FilterList: this.GenerateFilterList(),
                ItemsPerPage: Shared.ValueHelper.RecordsPerPageLov,
                Page: this.$scope.SearchParameter != undefined ? this.$scope.SearchParameter.Page : 1,
                SortAscending: true,
                SortColumn: "[References].telephone"
            };
            this.AddHandlerSorting();
        };
        ListOfViewCostCentreController.prototype.GenerateFilterList = function () {
            var result = [];
            var condition;
            if (this.$scope.LovParamCostCentre) {
                if (this.$scope.LovParamCostCentre.telephone) {
                    condition = {
                        ConditionType: Enums.SearchConditionType.And,
                        OperatorType: Enums.SearchOperatorType.Like,
                        TableName: "[References]",
                        FieldName: "telephone",
                        ParameterName: "ptelephone",
                        Value: this.$scope.LovParamCostCentre.telephone
                    };
                    result.push(condition);
                }
                if (this.$scope.LovParamCostCentre.cost_centre) {
                    condition = {
                        ConditionType: Enums.SearchConditionType.And,
                        OperatorType: Enums.SearchOperatorType.Like,
                        TableName: "[References]",
                        FieldName: "cost_centre",
                        ParameterName: "pcost_centre",
                        Value: this.$scope.LovParamCostCentre.cost_centre
                    };
                    result.push(condition);
                }
                if (this.$scope.LovParamCostCentre.reference_name) {
                    condition = {
                        ConditionType: Enums.SearchConditionType.And,
                        OperatorType: Enums.SearchOperatorType.Like,
                        TableName: "[References]",
                        FieldName: "reference_name",
                        ParameterName: "preference_name",
                        Value: this.$scope.LovParamCostCentre.reference_name
                    };
                    result.push(condition);
                }
            }
            return result;
        };
        ListOfViewCostCentreController.prototype.SearchData = function () {
            //On search
            this.$scope.SearchParameter.Page = 1;
            this.AddHandlerSorting();
            this.InitializeSearchParameter();
            this.LoadData();
        };
        ListOfViewCostCentreController.prototype.SelectedAll = function () {
            var _this = this;
            this.$scope.ListDisplayCostCentre.SearchResults.forEach(function (zipcode) {
                zipcode.IsSelected = _this.$scope.vm.isCheckedAll;
                _this.AddOrRemoveRow(zipcode);
            });
        };
        ListOfViewCostCentreController.prototype.SelectRow = function (zipcode) {
            if (!this.$scope.IsMultipleSelected) {
                if (this.$scope.IsShowLock === false
                    || (this.$scope.IsShowLock === true)) {
                    this.$rootScope.$broadcast(this.broadcastName, [true, zipcode]);
                    return;
                }
            }
            zipcode.IsSelected = (zipcode.IsSelected) ? false : true;
            this.AddOrRemoveRow(zipcode);
        };
        ListOfViewCostCentreController.prototype.AddOrRemoveRow = function (zipcode) {
            var _this = this;
            if (zipcode.IsSelected) {
                //ถ้า chk เป็น true จะแอดเข้า list
                var search = this.$scope.ListSelectedReferences.filter(function (c) { return c.zipcode == zipcode.zipcode; });
                if (search.length == 0) {
                    this.$scope.ListSelectedReferences.push(zipcode);
                }
            }
            else {
                var index = 0;
                this.$scope.ListSelectedReferences.forEach(function (c) {
                    if (c.zipcode == zipcode.zipcode) {
                        _this.$scope.ListSelectedReferences.splice(index, 1);
                    }
                    index++;
                });
            }
        };
        ListOfViewCostCentreController.prototype.ListOfViewCancel = function () {
            //On cancel
            if (!this.$scope.IsHideSearchStatus) {
                this.$scope.vm.status = 1;
            }
            if (this.$scope.LovParamCostCentre) {
                this.$scope.LovParamCostCentre.telephone = null;
                this.$scope.LovParamCostCentre.cost_centre = null;
                this.$scope.LovParamCostCentre.reference_name = null;
            }
            this.$scope.ListSelectedReferences = [];
            this.$scope.SearchParameter.Page = 1;
            this.AddHandlerSorting();
            this.InitializeSearchParameter();
            this.LoadData();
        };
        ListOfViewCostCentreController.prototype.ListOfViewSubmit = function () {
            this.$rootScope.$broadcast(this.broadcastName, [true, this.$scope.ListSelectedReferences]);
        };
        ListOfViewCostCentreController.$inject = ["$scope", "costCentreService", "$rootScope", "lovHelperService", "errorHandlerService"];
        return ListOfViewCostCentreController;
    }());
    CostCentre.ListOfViewCostCentreController = ListOfViewCostCentreController;
    Main.App.Controllers.controller("listOfViewCostCentreController", ListOfViewCostCentreController);
})(CostCentre || (CostCentre = {}));
//# sourceMappingURL=listOfViewCostCentreController.js.map