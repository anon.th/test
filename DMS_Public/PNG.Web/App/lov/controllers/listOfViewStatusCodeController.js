var StatusCode;
(function (StatusCode) {
    var ListOfViewStatusCodeController = (function () {
        function ListOfViewStatusCodeController($scope, statusCodeService, $rootScope, lovHelperService, errorHandlerService, shipmentTrackingService) {
            var _this = this;
            this.$scope = $scope;
            this.statusCodeService = statusCodeService;
            this.$rootScope = $rootScope;
            this.lovHelperService = lovHelperService;
            this.errorHandlerService = errorHandlerService;
            this.shipmentTrackingService = shipmentTrackingService;
            this.broadcastName = "StatusCodePopup";
            this.isCheckedAll = false;
            this.status = 1;
            $scope.vm = this;
            this.$scope.paramShipment2 = {};
            this.$scope.ListSelectedReferences = [];
            this.$scope.statusTypeList = [];
            this.$scope.statusCloseList = [];
            this.$scope.invoiceList = [];
            //Initial method
            this.InitializeListOfViewStatusCode();
            this.AddHandlerSorting();
            this.InitializeSearchParameter();
            this.loadStatusType().then(function () {
                _this.loadStatusClose().then(function () {
                    _this.loadInvoice();
                });
            });
            this.LoadData();
            this.AddHandlerPageLink();
        }
        ListOfViewStatusCodeController.prototype.InitializeListOfViewStatusCode = function () {
            //set parameter Lov company
            var lovParameter = this.lovHelperService.GetLovParameter();
            if (lovParameter) {
                //copy parameter to this Lov
                this.$scope.LovParamStatusCode = angular.copy(lovParameter.SearchFilter);
                this.$scope.paramShipment2.userid = this.$scope.LovParamStatusCode.userid;
                this.$scope.paramShipment2.user_culture = this.$scope.LovParamStatusCode.culture;
                if (lovParameter.BroadcastName) {
                    this.broadcastName = lovParameter.BroadcastName;
                }
                if (this.$scope.LovParamStatusCode.ListSelected) {
                    this.$scope.ListSelectedReferences = this.$scope.LovParamStatusCode.ListSelected;
                }
                if (lovParameter.IsHideSearchStatus) {
                    this.$scope.IsHideSearchStatus = lovParameter.IsHideSearchStatus;
                    if (lovParameter.Active >= -1 && lovParameter.Active <= 1) {
                        //-1 = All, 1 = Active, 0 = Inactive
                        this.$scope.vm.status = lovParameter.Active;
                    }
                    else {
                        this.$scope.vm.status = 1;
                    }
                }
                this.$scope.IsMultipleSelected = lovParameter.IsMultipleSelected;
                if (lovParameter.IsShowLock)
                    this.$scope.IsShowLock = lovParameter.IsShowLock;
                else
                    this.$scope.IsShowLock = false;
            }
            else {
                //clear parameter this Lov
                this.$scope.LovParamStatusCode = null;
            }
        };
        ListOfViewStatusCodeController.prototype.LoadData = function () {
            var _this = this;
            return this.statusCodeService.GetStatusCodeByPagination(this.$scope.SearchParameter)
                .then(function (Result) {
                _this.SetDataToDisplay(Result);
            }, (function (error) {
                _this.errorHandlerService.HandleException(error, "GetStatusCodeByPagination");
            }));
        };
        ListOfViewStatusCodeController.prototype.loadStatusType = function () {
            var _this = this;
            this.$scope.paramShipment2.codeid = "status_type";
            return this.shipmentTrackingService.GetCodevalue(this.$scope.paramShipment2).then(function (result) {
                if (result != null && result.length > 0) {
                    _this.$scope.statusTypeList = result;
                }
            }, function (error) {
                _this.errorHandlerService.HandleException(error, "");
            });
        };
        ListOfViewStatusCodeController.prototype.loadStatusClose = function () {
            var _this = this;
            this.$scope.paramShipment2.codeid = "close_status";
            return this.statusCodeService.GetCodevalue(this.$scope.paramShipment2).then(function (result) {
                if (result != null && result.length > 0) {
                    _this.$scope.statusCloseList = result;
                }
            }, function (error) {
                _this.errorHandlerService.HandleException(error, "");
            });
        };
        ListOfViewStatusCodeController.prototype.loadInvoice = function () {
            var _this = this;
            this.$scope.paramShipment2.codeid = "invoiceable";
            return this.statusCodeService.GetCodevalue(this.$scope.paramShipment2).then(function (result) {
                if (result != null && result.length > 0) {
                    _this.$scope.invoiceList = result;
                }
            }, function (error) {
                _this.errorHandlerService.HandleException(error, "");
            });
        };
        ListOfViewStatusCodeController.prototype.SetDataToDisplay = function (Result) {
            this.$scope.ListDisplayStatusCode = Result;
            //this.$rootScope.$broadcast(this.broadcastName, [true, zipcode]);
            //เช็คผลค้นหาที่ได้ตรงกับที่ค้นหา
            if (this.$scope.ListDisplayStatusCode.TotalRecord == 1) {
                if (this.$scope.IsMultipleSelected == false) {
                    var statecode = this.$scope.ListDisplayStatusCode.SearchResults[0];
                    //if (this.$scope.IsShowLock === false
                    //    || (this.$scope.IsShowLock === true && company.IsLockCompany === false))
                    //{
                    this.$rootScope.$broadcast(this.broadcastName, [true, statecode]);
                    return;
                }
            }
            //this.SetCheckboxOfListSelected();
        };
        // เซต display checkbox จาก list selected (ที่เคยเลือก)
        ListOfViewStatusCodeController.prototype.SetCheckboxOfListSelected = function () {
            var _this = this;
            this.$scope.vm.isCheckedAll = false;
            this.$scope.ListDisplayStatusCode.SearchResults.forEach(function (statecode) {
                var search = _this.$scope.ListSelectedReferences.filter(function (c) { return c.status_code == statecode.status_code; });
                if (search.length > 0) {
                    statecode.IsSelected = true;
                }
            });
        };
        ListOfViewStatusCodeController.prototype.AddHandlerSorting = function () {
            var _this = this;
            //On event from sorting
            this.WatchEvents = this.$scope.$watch(Shared.PvdConstant.ColumnSorting, function (current, old) {
                if (!current || (current == old))
                    return;
                _this.$scope.SearchParameter.Page = 1;
                _this.LoadData();
            });
        };
        ListOfViewStatusCodeController.prototype.SortingDisplayIcon = function () {
            return this.$scope.SearchParameter.SortAscending ? Shared.ColumnIcon.ArrowUp : Shared.ColumnIcon.ArrowDown;
        };
        ListOfViewStatusCodeController.prototype.SortingColumn = function (columnName) {
            if (this.$scope.SearchParameter.SortColumn == columnName) {
                this.$scope.SearchParameter.SortAscending = !this.$scope.SearchParameter.SortAscending;
            }
            else {
                this.$scope.SearchParameter.SortColumn = columnName;
                this.$scope.SearchParameter.SortAscending = true;
            }
        };
        ListOfViewStatusCodeController.prototype.AddHandlerPageLink = function () {
            var _this = this;
            //On event from page link changed
            this.WatchEvents = this.$scope.$watch(Shared.PvdConstant.SelectedPageLink, function (current, old) {
                if (!current || (current == old))
                    return;
                _this.$scope.SearchParameter.Page = _this.$scope.SelectedPageLink.Page;
                _this.LoadData();
            });
        };
        ListOfViewStatusCodeController.prototype.InitializeSearchParameter = function () {
            this.WatchEvents();
            this.$scope.SearchParameter = {
                FilterList: this.GenerateFilterList(),
                ItemsPerPage: Shared.ValueHelper.RecordsPerPageDetail,
                Page: this.$scope.SearchParameter != undefined ? this.$scope.SearchParameter.Page : 1,
                SortAscending: true,
                SortColumn: "[StatusCode].status_code"
            };
            this.AddHandlerSorting();
        };
        ListOfViewStatusCodeController.prototype.GenerateFilterList = function () {
            var result = [];
            var condition;
            if (this.$scope.LovParamStatusCode) {
                if (this.$scope.LovParamStatusCode.status_code) {
                    condition = {
                        ConditionType: Enums.SearchConditionType.And,
                        OperatorType: Enums.SearchOperatorType.Like,
                        TableName: "[StatusCode]",
                        FieldName: "status_code",
                        ParameterName: "pstatus_code",
                        Value: this.$scope.LovParamStatusCode.status_code
                    };
                    result.push(condition);
                }
                if (this.$scope.LovParamStatusCode.status_description) {
                    condition = {
                        ConditionType: Enums.SearchConditionType.And,
                        OperatorType: Enums.SearchOperatorType.Like,
                        TableName: "[StatusCode]",
                        FieldName: "status_description",
                        ParameterName: "pstatus_description",
                        Value: this.$scope.LovParamStatusCode.status_description
                    };
                    result.push(condition);
                }
                if (this.selctedStatusType) {
                    condition = {
                        ConditionType: Enums.SearchConditionType.And,
                        OperatorType: Enums.SearchOperatorType.In,
                        TableName: "[StatusCode]",
                        FieldName: "status_type",
                        ParameterName: "pstatus_type",
                        ListValues: [this.selctedStatusType, "B"]
                    };
                    result.push(condition);
                }
                if (this.selctedStatusClose) {
                    condition = {
                        ConditionType: Enums.SearchConditionType.And,
                        OperatorType: Enums.SearchOperatorType.Like,
                        TableName: "[StatusCode]",
                        FieldName: "status_close",
                        ParameterName: "pstatus_close",
                        Value: this.selctedStatusClose
                    };
                    result.push(condition);
                }
                if (this.selctedInvoiceable) {
                    condition = {
                        ConditionType: Enums.SearchConditionType.And,
                        OperatorType: Enums.SearchOperatorType.Like,
                        TableName: "[StatusCode]",
                        FieldName: "invoiceable",
                        ParameterName: "pinvoiceable",
                        Value: this.selctedInvoiceable
                    };
                    result.push(condition);
                }
                if (this.$scope.LovParamStatusCode.userid) {
                    condition = {
                        ConditionType: Enums.SearchConditionType.And,
                        OperatorType: Enums.SearchOperatorType.Equal,
                        TableName: "[StatusCode]",
                        FieldName: "userid",
                        ParameterName: "puserid",
                        Value: this.$scope.LovParamStatusCode.userid
                    };
                    result.push(condition);
                }
                if (this.$scope.LovParamStatusCode.allowRoles) {
                    condition = {
                        ConditionType: Enums.SearchConditionType.And,
                        OperatorType: Enums.SearchOperatorType.Like,
                        TableName: "[StatusCode]",
                        FieldName: "allowRoles",
                        ParameterName: "pallowRoles",
                        Value: "%" + this.$scope.LovParamStatusCode.allowRoles + "%"
                    };
                    result.push(condition);
                }
            }
            return result;
        };
        ListOfViewStatusCodeController.prototype.SearchData = function () {
            //On search
            this.$scope.SearchParameter.Page = 1;
            //this.$scope.SearchParameterCustom.Page = 1;
            this.AddHandlerSorting();
            this.InitializeSearchParameter();
            this.LoadData();
        };
        ListOfViewStatusCodeController.prototype.SelectedAll = function () {
            var _this = this;
            this.$scope.ListDisplayStatusCode.SearchResults.forEach(function (zipcode) {
                zipcode.IsSelected = _this.$scope.vm.isCheckedAll;
                _this.AddOrRemoveRow(zipcode);
            });
        };
        ListOfViewStatusCodeController.prototype.SelectRow = function (statecode) {
            if (!this.$scope.IsMultipleSelected) {
                if (this.$scope.IsShowLock === false
                    || (this.$scope.IsShowLock === true)) {
                    this.$rootScope.$broadcast(this.broadcastName, [true, statecode]);
                    return;
                }
            }
            statecode.IsSelected = (statecode.IsSelected) ? false : true;
            this.AddOrRemoveRow(statecode);
        };
        ListOfViewStatusCodeController.prototype.AddOrRemoveRow = function (statecode) {
            var _this = this;
            if (statecode.IsSelected) {
                //ถ้า chk เป็น true จะแอดเข้า list
                var search = this.$scope.ListSelectedReferences.filter(function (c) { return c.status_code == statecode.status_code; });
                if (search.length == 0) {
                    this.$scope.ListSelectedReferences.push(statecode);
                }
            }
            else {
                var index = 0;
                this.$scope.ListSelectedReferences.forEach(function (c) {
                    if (c.status_code == statecode.status_code) {
                        _this.$scope.ListSelectedReferences.splice(index, 1);
                    }
                    index++;
                });
            }
        };
        ListOfViewStatusCodeController.prototype.ListOfViewCancel = function () {
            //On cancel
            if (!this.$scope.IsHideSearchStatus) {
                this.$scope.vm.status = 1;
            }
            if (this.$scope.LovParamStatusCode) {
                this.$scope.LovParamStatusCode.status_code = null;
                this.$scope.LovParamStatusCode.status_description = null;
            }
            if (this.selctedStatusType) {
                this.selctedStatusType = null;
                this.$scope.statusTypeList = [];
                this.loadStatusType();
            }
            if (this.selctedStatusClose) {
                this.selctedStatusClose = null;
                this.$scope.statusCloseList = [];
                this.loadStatusClose();
            }
            if (this.selctedInvoiceable) {
                this.selctedInvoiceable = null;
                this.$scope.invoiceList = [];
                this.loadInvoice();
            }
            this.$scope.ListSelectedReferences = [];
            this.$scope.SearchParameter.Page = 1;
            this.AddHandlerSorting();
            this.InitializeSearchParameter();
            this.LoadData();
        };
        ListOfViewStatusCodeController.prototype.ListOfViewSubmit = function () {
            this.$rootScope.$broadcast(this.broadcastName, [true, this.$scope.ListSelectedReferences]);
        };
        ListOfViewStatusCodeController.$inject = ["$scope", "statusCodeService", "$rootScope", "lovHelperService", "errorHandlerService", "shipmentTrackingService"];
        return ListOfViewStatusCodeController;
    }());
    StatusCode.ListOfViewStatusCodeController = ListOfViewStatusCodeController;
    Main.App.Controllers.controller("listOfViewStatusCodeController", ListOfViewStatusCodeController);
})(StatusCode || (StatusCode = {}));
//# sourceMappingURL=listOfViewStatusCodeController.js.map