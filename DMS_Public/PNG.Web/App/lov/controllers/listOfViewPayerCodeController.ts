﻿module Interfaces {
    export interface IPayerCode {
        IsSelected?: boolean;
    }
}
module PayerCode {
    export interface IListOfViewPayerCodeController extends ng.IScope {
        vm: ListOfViewPayerCodeController;
        ListDisplayPayerCode: Interfaces.ISearchResultView<Interfaces.IPayerCode>;
        LovParamPayerCode: Interfaces.ILovPayerCode;
        ListSelectedReferences: Interfaces.IPayerCode[];
        IsMultipleSelected: boolean;
        IsHideSearchStatus: boolean;
        IsShowLock: boolean;

        SearchParameter: Interfaces.ISearchQueryParameter;
        SelectedPageLink: Interfaces.IPageLink;
    }

    export class ListOfViewPayerCodeController {
        public static $inject = ["$scope", "payerCodeService", "$rootScope", "lovHelperService", "errorHandlerService"];
        public WatchEvents: Function;
        private broadcastName: string = "PayerCodePopup";
        private isCheckedAll: boolean = false;
        private status: Number = 1;
        constructor(private $scope: IListOfViewPayerCodeController,
            private payerCodeService: PayerCode.PayerCodeService,
            private $rootScope: ng.IScope,
            private lovHelperService: Lov.LovHelperService,
            private errorHandlerService: Shared.ErrorHandlerService) {
            $scope.vm = this;
            this.$scope.ListSelectedReferences = [];
            //Initial method
            this.InitializeListOfViewPayerCode();
            this.AddHandlerSorting();
            this.InitializeSearchParameter();
            this.LoadData();

            this.AddHandlerPageLink();
        }

        private InitializeListOfViewPayerCode(): void {
            //set parameter Lov company
            var lovParameter = this.lovHelperService.GetLovParameter();

            if (lovParameter) {
                //copy parameter to this Lov
                this.$scope.LovParamPayerCode = angular.copy(lovParameter.SearchFilter);

                if (lovParameter.BroadcastName) {
                    this.broadcastName = lovParameter.BroadcastName;
                }

                if (this.$scope.LovParamPayerCode.ListSelected) {
                    this.$scope.ListSelectedReferences = this.$scope.LovParamPayerCode.ListSelected;
                }

                if (lovParameter.IsHideSearchStatus) {
                    this.$scope.IsHideSearchStatus = lovParameter.IsHideSearchStatus;
                    if (lovParameter.Active >= -1 && lovParameter.Active <= 1) {
                        //-1 = All, 1 = Active, 0 = Inactive
                        this.$scope.vm.status = lovParameter.Active;
                    }
                    else {
                        this.$scope.vm.status = 1;
                    }
                }

                this.$scope.IsMultipleSelected = lovParameter.IsMultipleSelected;

                if (lovParameter.IsShowLock)
                    this.$scope.IsShowLock = lovParameter.IsShowLock;
                else this.$scope.IsShowLock = false;
            }
            else {
                //clear parameter this Lov
                this.$scope.LovParamPayerCode = null;
            }
        }

        private LoadData(): ng.IPromise<void> {
            return this.payerCodeService.GetPayerCodeByPagination(this.$scope.SearchParameter)
                .then((Result: Interfaces.IPayerCode[]) => {
                    this.SetDataToDisplay(Result);
                },
                ((error) => {
                    this.errorHandlerService.HandleException(error, "GetpayerCodeResultByPagination");
                }));
            //}
        }

        private SetDataToDisplay(Result: Interfaces.IPayerCode[]) {
            this.$scope.ListDisplayPayerCode = Result;
            //this.$rootScope.$broadcast(this.broadcastName, [true, zipcode]);
            //เช็คผลค้นหาที่ได้ตรงกับที่ค้นหา
            if (this.$scope.ListDisplayPayerCode.TotalRecord == 1) {
                if (this.$scope.IsMultipleSelected == false) {
                    var payercode: Interfaces.IPayerCode = this.$scope.ListDisplayPayerCode.SearchResults[0];

                    //if (this.$scope.IsShowLock === false
                    //    || (this.$scope.IsShowLock === true && company.IsLockCompany === false))
                    //{
                    this.$rootScope.$broadcast(this.broadcastName, [true, payercode]);
                    return;
                    //}
                }
            }
            
        }

      

        // เซต display checkbox จาก list selected (ที่เคยเลือก)
        private SetCheckboxOfListSelected(): void {
            this.$scope.vm.isCheckedAll = false;

            this.$scope.ListDisplayPayerCode.SearchResults.forEach(payercode => {
                var search: Interfaces.IPayerCode[] = this.$scope.ListSelectedReferences.filter(c => c.custid == payercode.custid);
                if (search.length > 0) {
                    payercode.IsSelected = true;
                }
            });
        }

        private AddHandlerSorting(): void {
            //On event from sorting
            this.WatchEvents = this.$scope.$watch(Shared.PvdConstant.ColumnSorting, (current, old) => {
                if (!current || (current == old)) return;
                this.$scope.SearchParameter.Page = 1;
                this.LoadData();
            });
        }

        public SortingDisplayIcon(): string {
            return this.$scope.SearchParameter.SortAscending ? Shared.ColumnIcon.ArrowUp : Shared.ColumnIcon.ArrowDown;
        }

        public SortingColumn(columnName: string): void {
            if (this.$scope.SearchParameter.SortColumn == columnName) {
                this.$scope.SearchParameter.SortAscending = !this.$scope.SearchParameter.SortAscending;
            } else {
                this.$scope.SearchParameter.SortColumn = columnName;
                this.$scope.SearchParameter.SortAscending = true;
            }
        }

        private AddHandlerPageLink(): void {
            //On event from page link changed
            this.WatchEvents = this.$scope.$watch(Shared.PvdConstant.SelectedPageLink, (current, old) => {
                if (!current || (current == old)) return;
                this.$scope.SearchParameter.Page = this.$scope.SelectedPageLink.Page;
                this.LoadData();
            });
        }

        public InitializeSearchParameter(): void {
            this.WatchEvents();
            this.$scope.SearchParameter = {
                FilterList: this.GenerateFilterList(),
                ItemsPerPage: Shared.ValueHelper.RecordsPerPageDetail,
                Page: this.$scope.SearchParameter != undefined ? this.$scope.SearchParameter.Page : 1,
                SortAscending: true,
                SortColumn: "[Customer].custid"
            };
            this.AddHandlerSorting();
        }

        private GenerateFilterList(): Interfaces.ISearchFilterView[] {
            var result: Interfaces.ISearchFilterView[] = [];
            var condition: Interfaces.ISearchFilterView;
            if (this.$scope.LovParamPayerCode) {
                if (this.$scope.LovParamPayerCode.applicationid) {
                    condition = {
                        ConditionType: Enums.SearchConditionType.And,
                        OperatorType: Enums.SearchOperatorType.Equal,
                        TableName: "[customer]",
                        FieldName: "applicationid",
                        ParameterName: "papplicationid",
                        Value: this.$scope.LovParamPayerCode.applicationid
                    };
                    result.push(condition);
                }

                if (this.$scope.LovParamPayerCode.enterpriseid) {
                    condition = {
                        ConditionType: Enums.SearchConditionType.And,
                        OperatorType: Enums.SearchOperatorType.Equal,
                        TableName: "[customer]",
                        FieldName: "enterpriseid",
                        ParameterName: "penterpriseid",
                        Value: this.$scope.LovParamPayerCode.enterpriseid
                    };
                    result.push(condition);
                }

                if (this.$scope.LovParamPayerCode.custid) {
                    condition = {
                        ConditionType: Enums.SearchConditionType.And,
                        OperatorType: Enums.SearchOperatorType.Like,
                        TableName: "[customer]",
                        FieldName: "custid",
                        ParameterName: "pcustid",
                        Value: this.$scope.LovParamPayerCode.custid
                    };
                    result.push(condition);
                }
                if (this.$scope.LovParamPayerCode.cust_name) {
                    condition = {
                        ConditionType: Enums.SearchConditionType.And,
                        OperatorType: Enums.SearchOperatorType.Like,
                        TableName: "[customer]",
                        FieldName: "cust_name",
                        ParameterName: "pcust_name",
                        Value: this.$scope.LovParamPayerCode.cust_name
                    };
                    result.push(condition);
                }
                if (this.$scope.LovParamPayerCode.zipcode) {
                    condition = {
                        ConditionType: Enums.SearchConditionType.And,
                        OperatorType: Enums.SearchOperatorType.Like,
                        TableName: "[customer]",
                        FieldName: "zipcode",
                        ParameterName: "pzipcode",
                        Value: this.$scope.LovParamPayerCode.zipcode
                    };
                    result.push(condition);
                }

                if (this.$scope.LovParamPayerCode.payer_type.length > 0) {
                    condition = {
                        ConditionType: Enums.SearchConditionType.And,
                        OperatorType: Enums.SearchOperatorType.In,
                        TableName: "[customer]",
                        FieldName: "payer_type",
                        ParameterName: "ppayer_type",
                        ListValues: this.$scope.LovParamPayerCode.payer_type
                    };
                    result.push(condition);
                }
                if (this.$scope.LovParamPayerCode.status_active) {
                    condition = {
                        ConditionType: Enums.SearchConditionType.And,
                        OperatorType: Enums.SearchOperatorType.Equal,
                        TableName: "[customer]",
                        FieldName: "status_active",
                        ParameterName: "pstatus_active",
                        Value: this.$scope.LovParamPayerCode.status_active
                    };
                    result.push(condition);
                }

            }
            return result;
        }

        public SearchData() {
            //On search
            this.$scope.SearchParameter.Page = 1;
            this.AddHandlerSorting();
            this.InitializeSearchParameter();
            this.LoadData();
        }

        public SelectedAll(): void {
            this.$scope.ListDisplayPayerCode.SearchResults.forEach(zipcode => {
                zipcode.IsSelected = this.$scope.vm.isCheckedAll;
                this.AddOrRemoveRow(zipcode);
            });
        }

        public SelectRow(payercode: Interfaces.IPayerCode): void {
            if (!this.$scope.IsMultipleSelected) {
                if (this.$scope.IsShowLock === false
                    || (this.$scope.IsShowLock === true)) {
                    this.$rootScope.$broadcast(this.broadcastName, [true, payercode]);
                    return;
                }
            }
            payercode.IsSelected = (payercode.IsSelected) ? false : true;

            this.AddOrRemoveRow(payercode);
        }

        public AddOrRemoveRow(payercode: Interfaces.IPayerCode): void {
            if (payercode.IsSelected) {
                //ถ้า chk เป็น true จะแอดเข้า list
                var search: Interfaces.IPayerCode[] = this.$scope.ListSelectedReferences.filter(c => c.zipcode == payercode.custid);
                if (search.length == 0) {
                    this.$scope.ListSelectedReferences.push(payercode);
                }
            }
            else {
                var index = 0;
                this.$scope.ListSelectedReferences.forEach(c => {
                    if (c.custid == payercode.custid) {
                        this.$scope.ListSelectedReferences.splice(index, 1);
                    }
                    index++;
                });
            }
        }

        public ListOfViewCancel(): void {
            //On cancel
            if (!this.$scope.IsHideSearchStatus) {
                this.$scope.vm.status = 1;
            }
            if (this.$scope.LovParamPayerCode) {
                this.$scope.LovParamPayerCode.custid = null;
                this.$scope.LovParamPayerCode.cust_name = null;
                this.$scope.LovParamPayerCode.zipcode = null;
            }
            this.$scope.ListSelectedReferences = [];
            this.$scope.SearchParameter.Page = 1;
            this.AddHandlerSorting();
            this.InitializeSearchParameter();
            this.LoadData();
        }

        public ListOfViewSubmit(): void {
            this.$rootScope.$broadcast(this.broadcastName, [true, this.$scope.ListSelectedReferences]);
        }
    }

    Main.App.Controllers.controller("listOfViewPayerCodeController", ListOfViewPayerCodeController);
}