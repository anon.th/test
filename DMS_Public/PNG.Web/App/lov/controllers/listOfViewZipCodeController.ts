﻿module Interfaces
{
    export interface IZipCode
    {
        IsSelected?: boolean;
    }
}
module ZipCode
{
    export interface IListOfViewZipCodeController extends ng.IScope
    {
        vm: ListOfViewZipCodeController;
        ListDisplayZipCode: Interfaces.ISearchResultView<Interfaces.IZipCode>;
        LovParamZipCode: Interfaces.ILovZipCode;
        ListSelectedZipCode: Interfaces.IZipCode[];
        IsMultipleSelected: boolean;
        IsHideSearchStatus: boolean;
        IsShowLock: boolean;

        SearchParameter: Interfaces.ISearchQueryParameter;
        SelectedPageLink: Interfaces.IPageLink;
    }

    export class ListOfViewZipCodeController
    {
        public static $inject = ["$scope", "zipCodeService", "$rootScope", "lovHelperService", "errorHandlerService"];
        public WatchEvents: Function;
        private broadcastName: string = "ListOfViewZipCode";
        private isCheckedAll: boolean = false;
        private status: Number = 1;
        constructor(private $scope: IListOfViewZipCodeController,
            private zipCodeService: ZipCode.ZipCodeService,
            private $rootScope: ng.IScope,
            private lovHelperService: Lov.LovHelperService,
            private errorHandlerService: Shared.ErrorHandlerService)
        {
            $scope.vm = this;
            this.$scope.ListSelectedZipCode = [];

            //Initial method
            this.InitializeListOfViewZipCode();
            this.AddHandlerSorting();
            this.InitializeSearchParameter();
            this.LoadData();

            this.AddHandlerPageLink();
        }

        private InitializeListOfViewZipCode(): void
        {
            //set parameter Lov company
            var lovParameter = this.lovHelperService.GetLovParameter();

            if (lovParameter)
            {
                //copy parameter to this Lov
                this.$scope.LovParamZipCode = angular.copy(lovParameter.SearchFilter);

                if (lovParameter.BroadcastName)
                {
                    this.broadcastName = lovParameter.BroadcastName;
                }

                if (this.$scope.LovParamZipCode.ListSelected)
                {
                    this.$scope.ListSelectedZipCode = this.$scope.LovParamZipCode.ListSelected;
                }

                if (lovParameter.IsHideSearchStatus)
                {
                    this.$scope.IsHideSearchStatus = lovParameter.IsHideSearchStatus;
                    if (lovParameter.Active >= -1 && lovParameter.Active <= 1)
                    {
                        //-1 = All, 1 = Active, 0 = Inactive
                        this.$scope.vm.status = lovParameter.Active;
                    }
                    else
                    {
                        this.$scope.vm.status = 1;
                    }
                }

                this.$scope.IsMultipleSelected = lovParameter.IsMultipleSelected;

                if (lovParameter.IsShowLock)
                    this.$scope.IsShowLock = lovParameter.IsShowLock;
                else this.$scope.IsShowLock = false;
            }
            else
            {
                //clear parameter this Lov
                this.$scope.LovParamZipCode = {};
            }
        }

        private LoadData(): ng.IPromise<void>
        {
            //if (this.$scope.IsShowLock === true && this.$scope.LovParamZipCode.FundId != null)
            //{
            //    return this.zipCodeService.GetZipCodeByPagination(this.$scope.SearchParameter, this.$scope.LovParamZipCode.FundId)
            //        .then((companyResult: Interfaces.ICompany[]) =>
            //        {
            //            this.SetDataToDisplay(companyResult);
            //        },
            //        ((error) =>
            //        {
            //            this.errorHandlerService.HandleException(error, "GetZipCodeByPagination");
            //        }));
            //}
            //else
            //{
                return this.zipCodeService.GetReferencesByPagination(this.$scope.SearchParameter)
                    .then((zipCodeResult: Interfaces.IZipCode[]) =>
                    {
                        this.SetDataToDisplay(zipCodeResult);
                    },
                    ((error) =>
                    {
                        this.errorHandlerService.HandleException(error, "GetZipCodeByPagination");
                    }));
            //}
        }

        private SetDataToDisplay(zipCodeResult: Interfaces.IZipCode[])
        {
            this.$scope.ListDisplayZipCode = zipCodeResult;
            //this.$rootScope.$broadcast(this.broadcastName, [true, zipcode]);
            //เช็คผลค้นหาที่ได้ตรงกับที่ค้นหา
            if (this.$scope.ListDisplayZipCode.TotalRecord == 1)
            {
                if (this.$scope.IsMultipleSelected == false)
                {
                    var zipcode: Interfaces.IZipCode = this.$scope.ListDisplayZipCode.SearchResults[0];

                    //if (this.$scope.IsShowLock === false
                    //    || (this.$scope.IsShowLock === true && company.IsLockCompany === false))
                    //{
                    this.$rootScope.$broadcast(this.broadcastName, [true, zipcode]);
                    return;
                    //}
                }
                //else if (this.$scope.IsMultipleSelected == true)
                //{
                //    var search: Interfaces.ICompany[] = this.$scope.ListSelectedCompany.filter(c => c.Id == this.$scope.ListDisplayCompany.SearchResults[0].Id);
                //    if (search.length == 0)
                //    {
                //        this.$scope.ListSelectedCompany.push(this.$scope.ListDisplayCompany.SearchResults[0]);
                //    }

                //    this.$rootScope.$broadcast(this.broadcastName, [true, this.$scope.ListSelectedCompany]);
                //}
            }

            //this.SetCheckboxOfListSelected();
        }

        // เซต display checkbox จาก list selected (ที่เคยเลือก)
        private SetCheckboxOfListSelected(): void
        {
            this.$scope.vm.isCheckedAll = false;

            this.$scope.ListDisplayZipCode.SearchResults.forEach(zipcode =>
            {
                var search: Interfaces.IZipCode[] = this.$scope.ListSelectedZipCode.filter(c => c.zipcode == zipcode.zipcode);
                if (search.length > 0)
                {
                    zipcode.IsSelected = true;
                }
            });
        }

        private AddHandlerSorting(): void
        {
            //On event from sorting
            this.WatchEvents = this.$scope.$watch(Shared.PvdConstant.ColumnSorting, (current, old) =>
            {
                if (!current || (current == old)) return;
                this.$scope.SearchParameter.Page = 1;
                this.LoadData();
            });
        }

        public SortingDisplayIcon(): string
        {
            return this.$scope.SearchParameter.SortAscending ? Shared.ColumnIcon.ArrowUp : Shared.ColumnIcon.ArrowDown;
        }

        public SortingColumn(columnName: string): void
        {
            if (this.$scope.SearchParameter.SortColumn == columnName)
            {
                this.$scope.SearchParameter.SortAscending = !this.$scope.SearchParameter.SortAscending;
            } else
            {
                this.$scope.SearchParameter.SortColumn = columnName;
                this.$scope.SearchParameter.SortAscending = true;
            }
        }

        private AddHandlerPageLink(): void
        {
            //On event from page link changed
            this.WatchEvents = this.$scope.$watch(Shared.PvdConstant.SelectedPageLink, (current, old) =>
            {
                if (!current || (current == old)) return;
                this.$scope.SearchParameter.Page = this.$scope.SelectedPageLink.Page;
                this.LoadData();
            });
        }

        public InitializeSearchParameter(): void
        {
            this.WatchEvents();
            this.$scope.SearchParameter = {
                FilterList: this.GenerateFilterList(),
                ItemsPerPage: Shared.ValueHelper.RecordsPerPageLov,
                Page: this.$scope.SearchParameter != undefined ? this.$scope.SearchParameter.Page : 1,
                SortAscending: true,
                SortColumn: "zipcode.zipcode"
            };
            this.AddHandlerSorting();
        }

        private GenerateFilterList(): Interfaces.ISearchFilterView[]
        {
            var result: Interfaces.ISearchFilterView[] = [];
            var condition: Interfaces.ISearchFilterView;

            if (this.$scope.LovParamZipCode)
            {

                condition = {
                    ConditionType: Enums.SearchConditionType.And,
                    OperatorType: Enums.SearchOperatorType.Equal,
                    TableName: "ZipCode",
                    FieldName: "applicationid",
                    ParameterName: "applicationid",
                    Value: "TIES"
                };
                result.push(condition);

                condition = {
                    ConditionType: Enums.SearchConditionType.And,
                    OperatorType: Enums.SearchOperatorType.Equal,
                    TableName: "ZipCode",
                    FieldName: "enterpriseid",
                    ParameterName: "enterpriseid",
                    Value: "PNGAF"
                };
                result.push(condition);

                if (this.$scope.LovParamZipCode.zipcode)
                {
                    condition = {
                        ConditionType: Enums.SearchConditionType.And,
                        OperatorType: Enums.SearchOperatorType.Like,
                        TableName: "zipcode",
                        FieldName: "zipcode",
                        ParameterName: "pzipcode",
                        Value: this.$scope.LovParamZipCode.zipcode
                    };
                    result.push(condition);
                }

                if (this.$scope.LovParamZipCode.Country)
                {
                    condition = {
                        ConditionType: Enums.SearchConditionType.And,
                        OperatorType: Enums.SearchOperatorType.Equal,
                        TableName: "zipcode",
                        FieldName: "country",
                        ParameterName: "country",
                        Value: this.$scope.LovParamZipCode.Country
                    };
                    result.push(condition);
                }

            }
            return result;
        }

        public SearchData()
        {
            //On search
            this.$scope.SearchParameter.Page = 1;
            this.AddHandlerSorting();
            this.InitializeSearchParameter();
            this.LoadData();
        }

        public SelectedAll(): void
        {
            this.$scope.ListDisplayZipCode.SearchResults.forEach(zipcode =>
            {
                zipcode.IsSelected = this.$scope.vm.isCheckedAll;
                this.AddOrRemoveRow(zipcode);
            });
        }

        public SelectRow(zipcode: Interfaces.IZipCode): void
        {
            if (!this.$scope.IsMultipleSelected)
            {
                if (this.$scope.IsShowLock === false
                    || (this.$scope.IsShowLock === true))
                {
                    this.$rootScope.$broadcast(this.broadcastName, [true, zipcode]);
                    return;
                }
            }
            zipcode.IsSelected = (zipcode.IsSelected) ? false : true;

            this.AddOrRemoveRow(zipcode);
        }

        public AddOrRemoveRow(zipcode: Interfaces.IZipCode): void
        {
            if (zipcode.IsSelected)
            {
                //ถ้า chk เป็น true จะแอดเข้า list
                var search: Interfaces.IZipCode[] = this.$scope.ListSelectedZipCode.filter(c => c.zipcode == zipcode.zipcode);
                if (search.length == 0)
                {
                    this.$scope.ListSelectedZipCode.push(zipcode);
                }
            }
            else
            {
                var index = 0;
                this.$scope.ListSelectedZipCode.forEach(c =>
                {
                    if (c.zipcode == zipcode.zipcode)
                    {
                        this.$scope.ListSelectedZipCode.splice(index, 1);
                    }
                    index++;
                });
            }
        }

        public ListOfViewCancel(): void
        {
            //On cancel
            if (!this.$scope.IsHideSearchStatus)
            {
                this.$scope.vm.status = 1;
            }
            if (this.$scope.LovParamZipCode)
            {
                this.$scope.LovParamZipCode.zipcode = null;
                this.$scope.LovParamZipCode.Name = null;
            }
            this.$scope.ListSelectedZipCode = [];
            this.$scope.SearchParameter.Page = 1;
            this.AddHandlerSorting();
            this.InitializeSearchParameter();
            this.LoadData();
        }

        public ListOfViewSubmit(): void
        {
            this.$rootScope.$broadcast(this.broadcastName, [true, this.$scope.ListSelectedZipCode]);
        }
    }

    Main.App.Controllers.controller("listOfViewZipCodeController", ListOfViewZipCodeController);
}