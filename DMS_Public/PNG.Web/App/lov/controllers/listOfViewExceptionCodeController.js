var ExceptionCode;
(function (ExceptionCode) {
    var ListOfViewExceptionCodeController = (function () {
        function ListOfViewExceptionCodeController($scope, exceptionCodeService, $rootScope, lovHelperService, errorHandlerService, shipmentTrackingService) {
            var _this = this;
            this.$scope = $scope;
            this.exceptionCodeService = exceptionCodeService;
            this.$rootScope = $rootScope;
            this.lovHelperService = lovHelperService;
            this.errorHandlerService = errorHandlerService;
            this.shipmentTrackingService = shipmentTrackingService;
            this.broadcastName = "ExceptionCodePopup";
            this.isCheckedAll = false;
            this.status = 1;
            $scope.vm = this;
            this.$scope.paramShipment2 = {};
            this.$scope.ListSelectedReferences = [];
            this.$scope.mbgList = [];
            this.$scope.statusCloseList = [];
            this.$scope.invoiceList = [];
            //Initial method
            this.InitializeListOfViewExceptionCode();
            this.AddHandlerSorting();
            this.InitializeSearchParameter();
            this.loadMbg().then(function () {
                _this.loadStatusClose().then(function () {
                    _this.loadInvoice();
                });
            });
            this.LoadData();
            this.AddHandlerPageLink();
        }
        ListOfViewExceptionCodeController.prototype.InitializeListOfViewExceptionCode = function () {
            //set parameter Lov company
            var lovParameter = this.lovHelperService.GetLovParameter();
            if (lovParameter) {
                //copy parameter to this Lov
                this.$scope.LovParamExceptionCode = angular.copy(lovParameter.SearchFilter);
                this.$scope.paramShipment2.userid = this.$scope.LovParamExceptionCode.userid;
                this.$scope.paramShipment2.user_culture = this.$scope.LovParamExceptionCode.culture;
                if (lovParameter.BroadcastName) {
                    this.broadcastName = lovParameter.BroadcastName;
                }
                if (this.$scope.LovParamExceptionCode.ListSelected) {
                    this.$scope.ListSelectedReferences = this.$scope.LovParamExceptionCode.ListSelected;
                }
                if (lovParameter.IsHideSearchStatus) {
                    this.$scope.IsHideSearchStatus = lovParameter.IsHideSearchStatus;
                    if (lovParameter.Active >= -1 && lovParameter.Active <= 1) {
                        //-1 = All, 1 = Active, 0 = Inactive
                        this.$scope.vm.status = lovParameter.Active;
                    }
                    else {
                        this.$scope.vm.status = 1;
                    }
                }
                this.$scope.IsMultipleSelected = lovParameter.IsMultipleSelected;
                if (lovParameter.IsShowLock)
                    this.$scope.IsShowLock = lovParameter.IsShowLock;
                else
                    this.$scope.IsShowLock = false;
            }
            else {
                //clear parameter this Lov
                this.$scope.LovParamExceptionCode = null;
            }
        };
        ListOfViewExceptionCodeController.prototype.LoadData = function () {
            var _this = this;
            return this.exceptionCodeService.GetExceptionCodeByPagination(this.$scope.SearchParameter)
                .then(function (Result) {
                _this.SetDataToDisplay(Result);
            }, (function (error) {
                _this.errorHandlerService.HandleException(error, "GetStatusCodeByPagination");
            }));
        };
        ListOfViewExceptionCodeController.prototype.loadMbg = function () {
            var _this = this;
            this.$scope.paramShipment2.codeid = "mbg";
            return this.shipmentTrackingService.GetCodevalue(this.$scope.paramShipment2).then(function (result) {
                if (result != null && result.length > 0) {
                    _this.$scope.mbgList = result;
                }
            }, function (error) {
                _this.errorHandlerService.HandleException(error, "");
            });
        };
        ListOfViewExceptionCodeController.prototype.loadStatusClose = function () {
            var _this = this;
            this.$scope.paramShipment2.codeid = "close_status";
            return this.exceptionCodeService.GetCodevalue(this.$scope.paramShipment2).then(function (result) {
                if (result != null && result.length > 0) {
                    _this.$scope.statusCloseList = result;
                }
            }, function (error) {
                _this.errorHandlerService.HandleException(error, "");
            });
        };
        ListOfViewExceptionCodeController.prototype.loadInvoice = function () {
            var _this = this;
            this.$scope.paramShipment2.codeid = "invoiceable";
            return this.exceptionCodeService.GetCodevalue(this.$scope.paramShipment2).then(function (result) {
                if (result != null && result.length > 0) {
                    _this.$scope.invoiceList = result;
                }
            }, function (error) {
                _this.errorHandlerService.HandleException(error, "");
            });
        };
        ListOfViewExceptionCodeController.prototype.SetDataToDisplay = function (Result) {
            this.$scope.ListDisplayExceptionCode = Result;
            //this.$rootScope.$broadcast(this.broadcastName, [true, zipcode]);
            //เช็คผลค้นหาที่ได้ตรงกับที่ค้นหา
            if (this.$scope.ListDisplayExceptionCode.TotalRecord == 1) {
                if (this.$scope.IsMultipleSelected == false) {
                    var statecode = this.$scope.ListDisplayExceptionCode.SearchResults[0];
                    //if (this.$scope.IsShowLock === false
                    //    || (this.$scope.IsShowLock === true && company.IsLockCompany === false))
                    //{
                    this.$rootScope.$broadcast(this.broadcastName, [true, statecode]);
                    return;
                }
            }
            //this.SetCheckboxOfListSelected();
        };
        // เซต display checkbox จาก list selected (ที่เคยเลือก)
        ListOfViewExceptionCodeController.prototype.SetCheckboxOfListSelected = function () {
            var _this = this;
            this.$scope.vm.isCheckedAll = false;
            this.$scope.ListDisplayExceptionCode.SearchResults.forEach(function (statecode) {
                var search = _this.$scope.ListSelectedReferences.filter(function (c) { return c.status_code == statecode.status_code; });
                if (search.length > 0) {
                    statecode.IsSelected = true;
                }
            });
        };
        ListOfViewExceptionCodeController.prototype.AddHandlerSorting = function () {
            var _this = this;
            //On event from sorting
            this.WatchEvents = this.$scope.$watch(Shared.PvdConstant.ColumnSorting, function (current, old) {
                if (!current || (current == old))
                    return;
                _this.$scope.SearchParameter.Page = 1;
                _this.LoadData();
            });
        };
        ListOfViewExceptionCodeController.prototype.SortingDisplayIcon = function () {
            return this.$scope.SearchParameter.SortAscending ? Shared.ColumnIcon.ArrowUp : Shared.ColumnIcon.ArrowDown;
        };
        ListOfViewExceptionCodeController.prototype.SortingColumn = function (columnName) {
            if (this.$scope.SearchParameter.SortColumn == columnName) {
                this.$scope.SearchParameter.SortAscending = !this.$scope.SearchParameter.SortAscending;
            }
            else {
                this.$scope.SearchParameter.SortColumn = columnName;
                this.$scope.SearchParameter.SortAscending = true;
            }
        };
        ListOfViewExceptionCodeController.prototype.AddHandlerPageLink = function () {
            var _this = this;
            //On event from page link changed
            this.WatchEvents = this.$scope.$watch(Shared.PvdConstant.SelectedPageLink, function (current, old) {
                if (!current || (current == old))
                    return;
                _this.$scope.SearchParameter.Page = _this.$scope.SelectedPageLink.Page;
                _this.LoadData();
            });
        };
        ListOfViewExceptionCodeController.prototype.InitializeSearchParameter = function () {
            this.WatchEvents();
            this.$scope.SearchParameter = {
                FilterList: this.GenerateFilterList(),
                ItemsPerPage: Shared.ValueHelper.RecordsPerPageDetail,
                Page: this.$scope.SearchParameter != undefined ? this.$scope.SearchParameter.Page : 1,
                SortAscending: true,
                SortColumn: "[Exception_Code].status_code"
            };
            this.AddHandlerSorting();
        };
        ListOfViewExceptionCodeController.prototype.GenerateFilterList = function () {
            var result = [];
            var condition;
            if (this.$scope.LovParamExceptionCode) {
                if (this.$scope.LovParamExceptionCode.status_code) {
                    condition = {
                        ConditionType: Enums.SearchConditionType.And,
                        OperatorType: Enums.SearchOperatorType.Like,
                        TableName: "[Exception_Code]",
                        FieldName: "status_code",
                        ParameterName: "pstatus_code",
                        Value: this.$scope.LovParamExceptionCode.status_code
                    };
                    result.push(condition);
                }
                if (this.$scope.LovParamExceptionCode.exception_code) {
                    condition = {
                        ConditionType: Enums.SearchConditionType.And,
                        OperatorType: Enums.SearchOperatorType.Like,
                        TableName: "[Exception_Code]",
                        FieldName: "exception_code",
                        ParameterName: "pexception_code",
                        Value: this.$scope.LovParamExceptionCode.exception_code
                    };
                    result.push(condition);
                }
                if (this.selctedMbg) {
                    condition = {
                        ConditionType: Enums.SearchConditionType.And,
                        OperatorType: Enums.SearchOperatorType.Like,
                        TableName: "[Exception_Code]",
                        FieldName: "mbg",
                        ParameterName: "pmbg",
                        Value: this.selctedMbg
                    };
                    result.push(condition);
                }
                if (this.selctedStatusClose) {
                    condition = {
                        ConditionType: Enums.SearchConditionType.And,
                        OperatorType: Enums.SearchOperatorType.Like,
                        TableName: "[Exception_Code]",
                        FieldName: "status_close",
                        ParameterName: "pstatus_close",
                        Value: this.selctedStatusClose
                    };
                    result.push(condition);
                }
                if (this.selctedInvoiceable) {
                    condition = {
                        ConditionType: Enums.SearchConditionType.And,
                        OperatorType: Enums.SearchOperatorType.Like,
                        TableName: "[Exception_Code]",
                        FieldName: "invoiceable",
                        ParameterName: "pinvoiceable",
                        Value: this.selctedInvoiceable
                    };
                    result.push(condition);
                }
            }
            return result;
        };
        ListOfViewExceptionCodeController.prototype.SearchData = function () {
            //On search
            this.$scope.SearchParameter.Page = 1;
            //this.$scope.SearchParameterCustom.Page = 1;
            this.AddHandlerSorting();
            this.InitializeSearchParameter();
            this.LoadData();
        };
        ListOfViewExceptionCodeController.prototype.SelectedAll = function () {
            var _this = this;
            this.$scope.ListDisplayExceptionCode.SearchResults.forEach(function (zipcode) {
                zipcode.IsSelected = _this.$scope.vm.isCheckedAll;
                _this.AddOrRemoveRow(zipcode);
            });
        };
        ListOfViewExceptionCodeController.prototype.SelectRow = function (statecode) {
            if (!this.$scope.IsMultipleSelected) {
                if (this.$scope.IsShowLock === false
                    || (this.$scope.IsShowLock === true)) {
                    this.$rootScope.$broadcast(this.broadcastName, [true, statecode]);
                    return;
                }
            }
            statecode.IsSelected = (statecode.IsSelected) ? false : true;
            this.AddOrRemoveRow(statecode);
        };
        ListOfViewExceptionCodeController.prototype.AddOrRemoveRow = function (statecode) {
            var _this = this;
            if (statecode.IsSelected) {
                //ถ้า chk เป็น true จะแอดเข้า list
                var search = this.$scope.ListSelectedReferences.filter(function (c) { return c.status_code == statecode.status_code; });
                if (search.length == 0) {
                    this.$scope.ListSelectedReferences.push(statecode);
                }
            }
            else {
                var index = 0;
                this.$scope.ListSelectedReferences.forEach(function (c) {
                    if (c.status_code == statecode.status_code) {
                        _this.$scope.ListSelectedReferences.splice(index, 1);
                    }
                    index++;
                });
            }
        };
        ListOfViewExceptionCodeController.prototype.ListOfViewCancel = function () {
            //On cancel
            if (!this.$scope.IsHideSearchStatus) {
                this.$scope.vm.status = 1;
            }
            if (this.$scope.LovParamExceptionCode) {
                this.$scope.LovParamExceptionCode.status_code = null;
                this.$scope.LovParamExceptionCode.status_description = null;
            }
            if (this.selctedMbg) {
                this.selctedMbg = null;
                this.$scope.mbgList = [];
                this.loadMbg();
            }
            if (this.selctedStatusClose) {
                this.selctedStatusClose = null;
                this.$scope.statusCloseList = [];
                this.loadStatusClose();
            }
            if (this.selctedInvoiceable) {
                this.selctedInvoiceable = null;
                this.$scope.invoiceList = [];
                this.loadInvoice();
            }
            this.$scope.ListSelectedReferences = [];
            this.$scope.SearchParameter.Page = 1;
            this.AddHandlerSorting();
            this.InitializeSearchParameter();
            this.LoadData();
        };
        ListOfViewExceptionCodeController.prototype.ListOfViewSubmit = function () {
            this.$rootScope.$broadcast(this.broadcastName, [true, this.$scope.ListSelectedReferences]);
        };
        ListOfViewExceptionCodeController.$inject = ["$scope", "exceptionCodeService", "$rootScope", "lovHelperService", "errorHandlerService", "shipmentTrackingService"];
        return ListOfViewExceptionCodeController;
    }());
    ExceptionCode.ListOfViewExceptionCodeController = ListOfViewExceptionCodeController;
    Main.App.Controllers.controller("listOfViewExceptionCodeController", ListOfViewExceptionCodeController);
})(ExceptionCode || (ExceptionCode = {}));
//# sourceMappingURL=listOfViewExceptionCodeController.js.map