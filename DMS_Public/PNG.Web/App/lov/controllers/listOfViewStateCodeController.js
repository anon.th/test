var StateCode;
(function (StateCode) {
    var ListOfViewStateCodeController = (function () {
        function ListOfViewStateCodeController($scope, stateCodeService, $rootScope, lovHelperService, errorHandlerService) {
            this.$scope = $scope;
            this.stateCodeService = stateCodeService;
            this.$rootScope = $rootScope;
            this.lovHelperService = lovHelperService;
            this.errorHandlerService = errorHandlerService;
            this.broadcastName = "StateCodePopup";
            this.isCheckedAll = false;
            this.status = 1;
            $scope.vm = this;
            this.$scope.ListSelectedReferences = [];
            //Initial method
            this.InitializeListOfViewStateCode();
            this.AddHandlerSorting();
            this.InitializeSearchParameter();
            this.LoadData();
            this.AddHandlerPageLink();
        }
        ListOfViewStateCodeController.prototype.InitializeListOfViewStateCode = function () {
            //set parameter Lov company
            var lovParameter = this.lovHelperService.GetLovParameter();
            if (lovParameter) {
                //copy parameter to this Lov
                this.$scope.LovParamStateCode = angular.copy(lovParameter.SearchFilter);
                if (lovParameter.BroadcastName) {
                    this.broadcastName = lovParameter.BroadcastName;
                }
                if (this.$scope.LovParamStateCode.ListSelected) {
                    this.$scope.ListSelectedReferences = this.$scope.LovParamStateCode.ListSelected;
                }
                if (lovParameter.IsHideSearchStatus) {
                    this.$scope.IsHideSearchStatus = lovParameter.IsHideSearchStatus;
                    if (lovParameter.Active >= -1 && lovParameter.Active <= 1) {
                        //-1 = All, 1 = Active, 0 = Inactive
                        this.$scope.vm.status = lovParameter.Active;
                    }
                    else {
                        this.$scope.vm.status = 1;
                    }
                }
                this.$scope.IsMultipleSelected = lovParameter.IsMultipleSelected;
                if (lovParameter.IsShowLock)
                    this.$scope.IsShowLock = lovParameter.IsShowLock;
                else
                    this.$scope.IsShowLock = false;
            }
            else {
                //clear parameter this Lov
                this.$scope.LovParamStateCode = null;
            }
        };
        ListOfViewStateCodeController.prototype.LoadData = function () {
            var _this = this;
            //if (this.$scope.IsShowLock === true && this.$scope.LovParamCostCentre.FundId != null)
            //{
            //    return this.zipCodeService.GetZipCodeByPagination(this.$scope.SearchParameter, this.$scope.LovParamCostCentre.FundId)
            //        .then((companyResult: Interfaces.ICompany[]) =>
            //        {
            //            this.SetDataToDisplay(companyResult);
            //        },
            //        ((error) =>
            //        {
            //            this.errorHandlerService.HandleException(error, "GetZipCodeByPagination");
            //        }));
            //}
            //else
            //{
            return this.stateCodeService.GetStateCodeByPagination(this.$scope.SearchParameter)
                .then(function (Result) {
                _this.SetDataToDisplay(Result);
            }, (function (error) {
                _this.errorHandlerService.HandleException(error, "GetStateCodeByPagination");
            }));
            //}
        };
        ListOfViewStateCodeController.prototype.SetDataToDisplay = function (Result) {
            this.$scope.ListDisplayStateCode = Result;
            //this.$rootScope.$broadcast(this.broadcastName, [true, zipcode]);
            //เช็คผลค้นหาที่ได้ตรงกับที่ค้นหา
            if (this.$scope.ListDisplayStateCode.TotalRecord == 1) {
                if (this.$scope.IsMultipleSelected == false) {
                    var statecode = this.$scope.ListDisplayStateCode.SearchResults[0];
                    //if (this.$scope.IsShowLock === false
                    //    || (this.$scope.IsShowLock === true && company.IsLockCompany === false))
                    //{
                    this.$rootScope.$broadcast(this.broadcastName, [true, statecode]);
                    return;
                }
            }
            //this.SetCheckboxOfListSelected();
        };
        // เซต display checkbox จาก list selected (ที่เคยเลือก)
        ListOfViewStateCodeController.prototype.SetCheckboxOfListSelected = function () {
            var _this = this;
            this.$scope.vm.isCheckedAll = false;
            this.$scope.ListDisplayStateCode.SearchResults.forEach(function (statecode) {
                var search = _this.$scope.ListSelectedReferences.filter(function (c) { return c.state_code == statecode.state_code; });
                if (search.length > 0) {
                    statecode.IsSelected = true;
                }
            });
        };
        ListOfViewStateCodeController.prototype.AddHandlerSorting = function () {
            var _this = this;
            //On event from sorting
            this.WatchEvents = this.$scope.$watch(Shared.PvdConstant.ColumnSorting, function (current, old) {
                if (!current || (current == old))
                    return;
                _this.$scope.SearchParameter.Page = 1;
                _this.LoadData();
            });
        };
        ListOfViewStateCodeController.prototype.SortingDisplayIcon = function () {
            return this.$scope.SearchParameter.SortAscending ? Shared.ColumnIcon.ArrowUp : Shared.ColumnIcon.ArrowDown;
        };
        ListOfViewStateCodeController.prototype.SortingColumn = function (columnName) {
            if (this.$scope.SearchParameter.SortColumn == columnName) {
                this.$scope.SearchParameter.SortAscending = !this.$scope.SearchParameter.SortAscending;
            }
            else {
                this.$scope.SearchParameter.SortColumn = columnName;
                this.$scope.SearchParameter.SortAscending = true;
            }
        };
        ListOfViewStateCodeController.prototype.AddHandlerPageLink = function () {
            var _this = this;
            //On event from page link changed
            this.WatchEvents = this.$scope.$watch(Shared.PvdConstant.SelectedPageLink, function (current, old) {
                if (!current || (current == old))
                    return;
                _this.$scope.SearchParameter.Page = _this.$scope.SelectedPageLink.Page;
                _this.LoadData();
            });
        };
        ListOfViewStateCodeController.prototype.InitializeSearchParameter = function () {
            this.WatchEvents();
            this.$scope.SearchParameter = {
                FilterList: this.GenerateFilterList(),
                ItemsPerPage: Shared.ValueHelper.RecordsPerPageDetail,
                Page: this.$scope.SearchParameter != undefined ? this.$scope.SearchParameter.Page : 1,
                SortAscending: true,
                SortColumn: "[State].country"
            };
            this.AddHandlerSorting();
        };
        ListOfViewStateCodeController.prototype.GenerateFilterList = function () {
            var result = [];
            var condition;
            if (this.$scope.LovParamStateCode) {
                if (this.$scope.LovParamStateCode.applicationid) {
                    condition = {
                        ConditionType: Enums.SearchConditionType.And,
                        OperatorType: Enums.SearchOperatorType.Equal,
                        TableName: "[State]",
                        FieldName: "applicationid",
                        ParameterName: "papplicationid",
                        Value: this.$scope.LovParamStateCode.applicationid
                    };
                    result.push(condition);
                }
                if (this.$scope.LovParamStateCode.enterpriseid) {
                    condition = {
                        ConditionType: Enums.SearchConditionType.And,
                        OperatorType: Enums.SearchOperatorType.Equal,
                        TableName: "[State]",
                        FieldName: "enterpriseid",
                        ParameterName: "penterpriseid",
                        Value: this.$scope.LovParamStateCode.enterpriseid
                    };
                    result.push(condition);
                }
                if (this.$scope.LovParamStateCode.country) {
                    condition = {
                        ConditionType: Enums.SearchConditionType.And,
                        OperatorType: Enums.SearchOperatorType.Like,
                        TableName: "[State]",
                        FieldName: "country",
                        ParameterName: "pcountry",
                        Value: this.$scope.LovParamStateCode.country
                    };
                    result.push(condition);
                }
                if (this.$scope.LovParamStateCode.state_code) {
                    condition = {
                        ConditionType: Enums.SearchConditionType.And,
                        OperatorType: Enums.SearchOperatorType.Like,
                        TableName: "[State]",
                        FieldName: "state_code",
                        ParameterName: "pstate_code",
                        Value: this.$scope.LovParamStateCode.state_code
                    };
                    result.push(condition);
                }
            }
            return result;
        };
        ListOfViewStateCodeController.prototype.SearchData = function () {
            //On search
            this.$scope.SearchParameter.Page = 1;
            this.AddHandlerSorting();
            this.InitializeSearchParameter();
            this.LoadData();
        };
        ListOfViewStateCodeController.prototype.SelectedAll = function () {
            var _this = this;
            this.$scope.ListDisplayStateCode.SearchResults.forEach(function (zipcode) {
                zipcode.IsSelected = _this.$scope.vm.isCheckedAll;
                _this.AddOrRemoveRow(zipcode);
            });
        };
        ListOfViewStateCodeController.prototype.SelectRow = function (statecode) {
            if (!this.$scope.IsMultipleSelected) {
                if (this.$scope.IsShowLock === false
                    || (this.$scope.IsShowLock === true)) {
                    this.$rootScope.$broadcast(this.broadcastName, [true, statecode]);
                    return;
                }
            }
            statecode.IsSelected = (statecode.IsSelected) ? false : true;
            this.AddOrRemoveRow(statecode);
        };
        ListOfViewStateCodeController.prototype.AddOrRemoveRow = function (statecode) {
            var _this = this;
            if (statecode.IsSelected) {
                //ถ้า chk เป็น true จะแอดเข้า list
                var search = this.$scope.ListSelectedReferences.filter(function (c) { return c.state_code == statecode.state_code; });
                if (search.length == 0) {
                    this.$scope.ListSelectedReferences.push(statecode);
                }
            }
            else {
                var index = 0;
                this.$scope.ListSelectedReferences.forEach(function (c) {
                    if (c.state_code == statecode.state_code) {
                        _this.$scope.ListSelectedReferences.splice(index, 1);
                    }
                    index++;
                });
            }
        };
        ListOfViewStateCodeController.prototype.ListOfViewCancel = function () {
            //On cancel
            if (!this.$scope.IsHideSearchStatus) {
                this.$scope.vm.status = 1;
            }
            if (this.$scope.LovParamStateCode) {
                this.$scope.LovParamStateCode.country = null;
                this.$scope.LovParamStateCode.state_code = null;
            }
            this.$scope.ListSelectedReferences = [];
            this.$scope.SearchParameter.Page = 1;
            this.AddHandlerSorting();
            this.InitializeSearchParameter();
            this.LoadData();
        };
        ListOfViewStateCodeController.prototype.ListOfViewSubmit = function () {
            this.$rootScope.$broadcast(this.broadcastName, [true, this.$scope.ListSelectedReferences]);
        };
        ListOfViewStateCodeController.$inject = ["$scope", "stateCodeService", "$rootScope", "lovHelperService", "errorHandlerService"];
        return ListOfViewStateCodeController;
    }());
    StateCode.ListOfViewStateCodeController = ListOfViewStateCodeController;
    Main.App.Controllers.controller("listOfViewStateCodeController", ListOfViewStateCodeController);
})(StateCode || (StateCode = {}));
//# sourceMappingURL=listOfViewStateCodeController.js.map