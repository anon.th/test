var PayerCode;
(function (PayerCode) {
    var ListOfViewPayerCodeController = (function () {
        function ListOfViewPayerCodeController($scope, payerCodeService, $rootScope, lovHelperService, errorHandlerService) {
            this.$scope = $scope;
            this.payerCodeService = payerCodeService;
            this.$rootScope = $rootScope;
            this.lovHelperService = lovHelperService;
            this.errorHandlerService = errorHandlerService;
            this.broadcastName = "PayerCodePopup";
            this.isCheckedAll = false;
            this.status = 1;
            $scope.vm = this;
            this.$scope.ListSelectedReferences = [];
            //Initial method
            this.InitializeListOfViewPayerCode();
            this.AddHandlerSorting();
            this.InitializeSearchParameter();
            this.LoadData();
            this.AddHandlerPageLink();
        }
        ListOfViewPayerCodeController.prototype.InitializeListOfViewPayerCode = function () {
            //set parameter Lov company
            var lovParameter = this.lovHelperService.GetLovParameter();
            if (lovParameter) {
                //copy parameter to this Lov
                this.$scope.LovParamPayerCode = angular.copy(lovParameter.SearchFilter);
                if (lovParameter.BroadcastName) {
                    this.broadcastName = lovParameter.BroadcastName;
                }
                if (this.$scope.LovParamPayerCode.ListSelected) {
                    this.$scope.ListSelectedReferences = this.$scope.LovParamPayerCode.ListSelected;
                }
                if (lovParameter.IsHideSearchStatus) {
                    this.$scope.IsHideSearchStatus = lovParameter.IsHideSearchStatus;
                    if (lovParameter.Active >= -1 && lovParameter.Active <= 1) {
                        //-1 = All, 1 = Active, 0 = Inactive
                        this.$scope.vm.status = lovParameter.Active;
                    }
                    else {
                        this.$scope.vm.status = 1;
                    }
                }
                this.$scope.IsMultipleSelected = lovParameter.IsMultipleSelected;
                if (lovParameter.IsShowLock)
                    this.$scope.IsShowLock = lovParameter.IsShowLock;
                else
                    this.$scope.IsShowLock = false;
            }
            else {
                //clear parameter this Lov
                this.$scope.LovParamPayerCode = null;
            }
        };
        ListOfViewPayerCodeController.prototype.LoadData = function () {
            var _this = this;
            return this.payerCodeService.GetPayerCodeByPagination(this.$scope.SearchParameter)
                .then(function (Result) {
                _this.SetDataToDisplay(Result);
            }, (function (error) {
                _this.errorHandlerService.HandleException(error, "GetpayerCodeResultByPagination");
            }));
            //}
        };
        ListOfViewPayerCodeController.prototype.SetDataToDisplay = function (Result) {
            this.$scope.ListDisplayPayerCode = Result;
            //this.$rootScope.$broadcast(this.broadcastName, [true, zipcode]);
            //เช็คผลค้นหาที่ได้ตรงกับที่ค้นหา
            if (this.$scope.ListDisplayPayerCode.TotalRecord == 1) {
                if (this.$scope.IsMultipleSelected == false) {
                    var payercode = this.$scope.ListDisplayPayerCode.SearchResults[0];
                    //if (this.$scope.IsShowLock === false
                    //    || (this.$scope.IsShowLock === true && company.IsLockCompany === false))
                    //{
                    this.$rootScope.$broadcast(this.broadcastName, [true, payercode]);
                    return;
                }
            }
        };
        // เซต display checkbox จาก list selected (ที่เคยเลือก)
        ListOfViewPayerCodeController.prototype.SetCheckboxOfListSelected = function () {
            var _this = this;
            this.$scope.vm.isCheckedAll = false;
            this.$scope.ListDisplayPayerCode.SearchResults.forEach(function (payercode) {
                var search = _this.$scope.ListSelectedReferences.filter(function (c) { return c.custid == payercode.custid; });
                if (search.length > 0) {
                    payercode.IsSelected = true;
                }
            });
        };
        ListOfViewPayerCodeController.prototype.AddHandlerSorting = function () {
            var _this = this;
            //On event from sorting
            this.WatchEvents = this.$scope.$watch(Shared.PvdConstant.ColumnSorting, function (current, old) {
                if (!current || (current == old))
                    return;
                _this.$scope.SearchParameter.Page = 1;
                _this.LoadData();
            });
        };
        ListOfViewPayerCodeController.prototype.SortingDisplayIcon = function () {
            return this.$scope.SearchParameter.SortAscending ? Shared.ColumnIcon.ArrowUp : Shared.ColumnIcon.ArrowDown;
        };
        ListOfViewPayerCodeController.prototype.SortingColumn = function (columnName) {
            if (this.$scope.SearchParameter.SortColumn == columnName) {
                this.$scope.SearchParameter.SortAscending = !this.$scope.SearchParameter.SortAscending;
            }
            else {
                this.$scope.SearchParameter.SortColumn = columnName;
                this.$scope.SearchParameter.SortAscending = true;
            }
        };
        ListOfViewPayerCodeController.prototype.AddHandlerPageLink = function () {
            var _this = this;
            //On event from page link changed
            this.WatchEvents = this.$scope.$watch(Shared.PvdConstant.SelectedPageLink, function (current, old) {
                if (!current || (current == old))
                    return;
                _this.$scope.SearchParameter.Page = _this.$scope.SelectedPageLink.Page;
                _this.LoadData();
            });
        };
        ListOfViewPayerCodeController.prototype.InitializeSearchParameter = function () {
            this.WatchEvents();
            this.$scope.SearchParameter = {
                FilterList: this.GenerateFilterList(),
                ItemsPerPage: Shared.ValueHelper.RecordsPerPageDetail,
                Page: this.$scope.SearchParameter != undefined ? this.$scope.SearchParameter.Page : 1,
                SortAscending: true,
                SortColumn: "[Customer].custid"
            };
            this.AddHandlerSorting();
        };
        ListOfViewPayerCodeController.prototype.GenerateFilterList = function () {
            var result = [];
            var condition;
            if (this.$scope.LovParamPayerCode) {
                if (this.$scope.LovParamPayerCode.applicationid) {
                    condition = {
                        ConditionType: Enums.SearchConditionType.And,
                        OperatorType: Enums.SearchOperatorType.Equal,
                        TableName: "[customer]",
                        FieldName: "applicationid",
                        ParameterName: "papplicationid",
                        Value: this.$scope.LovParamPayerCode.applicationid
                    };
                    result.push(condition);
                }
                if (this.$scope.LovParamPayerCode.enterpriseid) {
                    condition = {
                        ConditionType: Enums.SearchConditionType.And,
                        OperatorType: Enums.SearchOperatorType.Equal,
                        TableName: "[customer]",
                        FieldName: "enterpriseid",
                        ParameterName: "penterpriseid",
                        Value: this.$scope.LovParamPayerCode.enterpriseid
                    };
                    result.push(condition);
                }
                if (this.$scope.LovParamPayerCode.custid) {
                    condition = {
                        ConditionType: Enums.SearchConditionType.And,
                        OperatorType: Enums.SearchOperatorType.Like,
                        TableName: "[customer]",
                        FieldName: "custid",
                        ParameterName: "pcustid",
                        Value: this.$scope.LovParamPayerCode.custid
                    };
                    result.push(condition);
                }
                if (this.$scope.LovParamPayerCode.cust_name) {
                    condition = {
                        ConditionType: Enums.SearchConditionType.And,
                        OperatorType: Enums.SearchOperatorType.Like,
                        TableName: "[customer]",
                        FieldName: "cust_name",
                        ParameterName: "pcust_name",
                        Value: this.$scope.LovParamPayerCode.cust_name
                    };
                    result.push(condition);
                }
                if (this.$scope.LovParamPayerCode.zipcode) {
                    condition = {
                        ConditionType: Enums.SearchConditionType.And,
                        OperatorType: Enums.SearchOperatorType.Like,
                        TableName: "[customer]",
                        FieldName: "zipcode",
                        ParameterName: "pzipcode",
                        Value: this.$scope.LovParamPayerCode.zipcode
                    };
                    result.push(condition);
                }
                if (this.$scope.LovParamPayerCode.payer_type.length > 0) {
                    condition = {
                        ConditionType: Enums.SearchConditionType.And,
                        OperatorType: Enums.SearchOperatorType.In,
                        TableName: "[customer]",
                        FieldName: "payer_type",
                        ParameterName: "ppayer_type",
                        ListValues: this.$scope.LovParamPayerCode.payer_type
                    };
                    result.push(condition);
                }
                if (this.$scope.LovParamPayerCode.status_active) {
                    condition = {
                        ConditionType: Enums.SearchConditionType.And,
                        OperatorType: Enums.SearchOperatorType.Equal,
                        TableName: "[customer]",
                        FieldName: "status_active",
                        ParameterName: "pstatus_active",
                        Value: this.$scope.LovParamPayerCode.status_active
                    };
                    result.push(condition);
                }
            }
            return result;
        };
        ListOfViewPayerCodeController.prototype.SearchData = function () {
            //On search
            this.$scope.SearchParameter.Page = 1;
            this.AddHandlerSorting();
            this.InitializeSearchParameter();
            this.LoadData();
        };
        ListOfViewPayerCodeController.prototype.SelectedAll = function () {
            var _this = this;
            this.$scope.ListDisplayPayerCode.SearchResults.forEach(function (zipcode) {
                zipcode.IsSelected = _this.$scope.vm.isCheckedAll;
                _this.AddOrRemoveRow(zipcode);
            });
        };
        ListOfViewPayerCodeController.prototype.SelectRow = function (payercode) {
            if (!this.$scope.IsMultipleSelected) {
                if (this.$scope.IsShowLock === false
                    || (this.$scope.IsShowLock === true)) {
                    this.$rootScope.$broadcast(this.broadcastName, [true, payercode]);
                    return;
                }
            }
            payercode.IsSelected = (payercode.IsSelected) ? false : true;
            this.AddOrRemoveRow(payercode);
        };
        ListOfViewPayerCodeController.prototype.AddOrRemoveRow = function (payercode) {
            var _this = this;
            if (payercode.IsSelected) {
                //ถ้า chk เป็น true จะแอดเข้า list
                var search = this.$scope.ListSelectedReferences.filter(function (c) { return c.zipcode == payercode.custid; });
                if (search.length == 0) {
                    this.$scope.ListSelectedReferences.push(payercode);
                }
            }
            else {
                var index = 0;
                this.$scope.ListSelectedReferences.forEach(function (c) {
                    if (c.custid == payercode.custid) {
                        _this.$scope.ListSelectedReferences.splice(index, 1);
                    }
                    index++;
                });
            }
        };
        ListOfViewPayerCodeController.prototype.ListOfViewCancel = function () {
            //On cancel
            if (!this.$scope.IsHideSearchStatus) {
                this.$scope.vm.status = 1;
            }
            if (this.$scope.LovParamPayerCode) {
                this.$scope.LovParamPayerCode.custid = null;
                this.$scope.LovParamPayerCode.cust_name = null;
                this.$scope.LovParamPayerCode.zipcode = null;
            }
            this.$scope.ListSelectedReferences = [];
            this.$scope.SearchParameter.Page = 1;
            this.AddHandlerSorting();
            this.InitializeSearchParameter();
            this.LoadData();
        };
        ListOfViewPayerCodeController.prototype.ListOfViewSubmit = function () {
            this.$rootScope.$broadcast(this.broadcastName, [true, this.$scope.ListSelectedReferences]);
        };
        ListOfViewPayerCodeController.$inject = ["$scope", "payerCodeService", "$rootScope", "lovHelperService", "errorHandlerService"];
        return ListOfViewPayerCodeController;
    }());
    PayerCode.ListOfViewPayerCodeController = ListOfViewPayerCodeController;
    Main.App.Controllers.controller("listOfViewPayerCodeController", ListOfViewPayerCodeController);
})(PayerCode || (PayerCode = {}));
//# sourceMappingURL=listOfViewPayerCodeController.js.map