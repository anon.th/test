var Main;
(function (Main) {
    var App = (function () {
        function App() {
            // TODO: remove when unused.
            //console.log("angular constructor loaded.");
        }
        App.Filters = angular.module("app.filters", ["ngCookies"]);
        App.Directives = angular.module("app.directives", ["ui.bootstrap", "ui.select2"]);
        App.Services = angular.module("app.services", ["createDialogService", "ngStorage", "LocalStorageModule", "blockUI"]);
        App.Controllers = angular.module("app.controllers", ["app.services"]);
        App.Module = angular.module("app", ["app.filters", "app.directives", "app.services", "app.controllers", "ngResource",
            "ngRoute", "ngSanitize", "LocalStorageModule", "angularFileUpload", "app.settings", "mj.scrollingTabs", "ngMessages"]);
        return App;
    }());
    Main.App = App;
    var $routeProviderReference;
    App.Module.run(["$rootScope", "$cookies", "$location", "$log", "authService",
        function ($rootScope, $cookies, $location, $log, authService) {
            // Authorization Data
            //authService.FillAuthData();
            $rootScope.$on("webRequestTiming", function (event, data) {
                var logMessage;
                if (data.error) {
                    var errorMessage = "error message not supplied";
                    if (data.error.data) {
                        errorMessage = data.error.data.Message;
                        if (data.error.data.ExceptionType)
                            errorMessage += "\n ExceptionType : " + data.error.data.ExceptionType;
                        if (data.error.data.ExceptionMessage)
                            errorMessage += "\n ExceptionMessage : " + data.error.data.ExceptionMessage;
                    }
                    logMessage = "[{0} ms] {1} {2} {3} ({4})".format(data.time, data.method, data.uri, data.status, errorMessage);
                    $log.error(logMessage);
                    $("#log").prepend("<li style='color:#a94442'>{0}</li>".format(logMessage));
                }
                else {
                    logMessage = "[{0} ms] {1} {2} {3} {4}".format(data.time, data.method, data.uri, JSON.stringify(data), "success");
                    $log.info(logMessage);
                    $("#log").prepend("<li style='color:#3c763d'>{0}</li>".format(logMessage));
                }
            });
        }
    ]);
    App.Module.run(["$route", "$rootScope", "$location", "$anchorScroll", "$http", "configSettings", "$sessionStorage", "localStorageService",
        function ($route, $rootScope, $location, $anchorScroll, $http, configSettings, $sessionStorage, localStorageService) {
            var original = $location.path;
            $location.path = function (path, reload) {
                if (reload === false) {
                    var lastRoute = $route.current;
                    var un = $rootScope.$on("$locationChangeSuccess", function () {
                        $route.current = lastRoute;
                        un();
                    });
                }
                return original.apply($location, [path]);
            };
            $rootScope.$on("$routeChangeStart", function (event, currentState, previousState) {
                if (localStorageService.get('authorizationData') == undefined) {
                    var withOutAuth = [
                        Shared.RouteNames.TrackAndTrace, Shared.RouteNames.Login,
                    ];
                    var checkPath = withOutAuth.filter(function (f) { return $location.path().indexOf(f) != -1; }).length;
                    if (checkPath == 0 && $location.path().length > 1) {
                        $location.path('/login');
                    }
                }
                ////การตรวจสอบ Session เมื่อมีการเปลียน URL โดยจะยกเว้น 2 ลิงค์หลัก
                //var withOutLink = ['/Error/Index', '/Error/TimeOut'];
                //if (withOutLink.indexOf(currentState.templateUrl) == -1 && currentState.templateUrl != null)
                //{
                //    //check session timeout
                //    //$http.get(configSettings.apiServiceBaseUri + "home/CheckSessionTimeout").success((isTimeOut: any) =>
                //    //{
                //    //    if (isTimeOut == "True" && $sessionStorage.CFZSession != null)
                //    //    {
                //    //        $location.path('/timeout', true);
                //    //    }
                //    //    else if (isTimeOut == "True")
                //    //    {
                //    //        $location.path('/error', true);
                //    //    }
                //    //});
                //    angular.element(".block_navbar-nav").show();
                //}
                //else
                //{
                //    angular.element(".block_navbar-nav").hide();
                //}
            });
            $rootScope.$on("$routeChangeSuccess", function (event, currentState, previousState) {
                $anchorScroll();
            });
            var resolver = function (menuId) {
                return {
                    load: ["authService", "$route", "$rootScope", "$location", "$timeout", "$q",
                        function (authService, $route, $rootScope, $location, $timeout, $q) {
                            authService.FillAuthData()
                                .then(function () {
                                if (authService.userInfo) {
                                    var menu = null;
                                    if (authService.userInfo.ListUserMenu) {
                                        menu = authService.userInfo.ListUserMenu.filter(function (m) { return m.MenuId == menuId && m.IsCanView == true; });
                                    }
                                    if (menu != null) {
                                        setTimeout(function () {
                                            $rootScope.$apply(function () {
                                                $rootScope.$broadcast("CheckAccessRight", [true, menu]);
                                            });
                                        }, 100);
                                    }
                                    else {
                                        var withOutAuth = [Shared.RouteNames.WelCome, Shared.RouteNames.Home, Shared.RouteNames.Login];
                                        var currentPath = $location.path();
                                        if (withOutAuth.filter(function (f) { return currentPath == f; }).length == 0 && currentPath.length > 1) {
                                            var loginPattern = /login$/i;
                                            var loginResult = loginPattern.test(currentPath);
                                            var welcomePattern = /welcome$/i;
                                            var welcomeResult = welcomePattern.test(currentPath);
                                            // TODO: the current user have no permission for login page
                                            if (loginResult === false)
                                                $location.url("/error");
                                            // TODO: the current user have no permission for welcome page
                                            if (welcomeResult === false)
                                                $location.url("/error");
                                        }
                                    }
                                }
                            });
                        }
                    ]
                };
            };
            //$http.get(configSettings.apiServiceBaseUri + "home/getlistmenu").success((listMenuCode: string[]) =>
            //{
            //$sessionStorage.CFZSession = listMenuCode[0] == "null" ? false : true;
            // TODO: Move this section to service
            $http.get(configSettings.apiServiceBaseUri + "api/master/approutemenu").then(function (data) {
                var routes = data.data;
                for (var loop = 0; loop < routes.length; loop++) {
                    var route = routes[loop];
                    $routeProviderReference.when(route.RouteName, {
                        templateUrl: route.TemplateUrl,
                        //to do permission menu
                        resolve: true
                    });
                }
                $routeProviderReference.otherwise({ redirectTo: "/error" });
                $route.reload();
            });
            //});
            // Disable backspace key for browser navigation
            $(document).on("keydown", function (e) {
                var key = e.which || e.keyCode || 0;
                if (key == 8) {
                    if (!$(e.target).is("input, textarea")) {
                        e.preventDefault();
                    }
                }
            });
            // Disable autocomplete for form elements
            $(document).on("focus click", "form", function (e) {
                $(this).attr("autocomplete", "off");
            });
            // Disable autocomplete for input elements
            $(document).on("focus click", "input", function (e) {
                $(this).attr("autocomplete", "off");
            });
            // Disable autocomplete for textarea elements
            $(document).on("focus click", "textarea", function (e) {
                $(this).attr("autocomplete", "off");
            });
        }]);
    // TODO: using typescript enums and constants also in AngularJS view pages.
    App.Module.run(["$rootScope", function ($rootScope) {
            $rootScope.PageType = Enums.PageType;
        }]);
    App.Module.config(["$provide", "$httpProvider", "$routeProvider", "configSettings", "$locationProvider", "$qProvider",
        function ($provide, $httpProvider, $routeProvider, configSettings, $locationProvider, $qProvider) {
            $qProvider.errorOnUnhandledRejections(false);
            //$routeProvider.when("/trackandtrace",
            //        {
            //            templateUrl: "/TrackandTrace/Index",
            //            controller: "TrackandTraceController" //Load a HTML template
            //        })
            //    .when("/welcome",
            //        {
            //            templateUrl: "/Home/Welcome", //Redirect to action  
            //            controller: "HomeController"
            //        });
            $locationProvider.hashPrefix('');
            //$locationProvider.html5Mode(true);
            //var webRequestServiceTimings = ($delegate: Util.WebRequestService, $rootScope: ng.IScope, $q: ng.IQService) =>
            //{
            //    var caller = $delegate;
            //    var success = (scope: ng.IScope, data: any, method: string, uri: string, timeTaken: number, deferred: ng.IDeferred<any>) =>
            //    {
            //        scope.$broadcast("webRequestTiming", <Interfaces.ITimings>{
            //            method: method,
            //            uri: uri,
            //            time: timeTaken
            //        });
            //        deferred.resolve(data);
            //    };
            //    var fail = (scope: ng.IScope, data: ng.IHttpPromiseCallbackArg<any>, method: string, uri: string, timeTaken: number, deferred: ng.IDeferred<any>) =>
            //    {
            //        scope.$broadcast("webRequestTiming", <Interfaces.ITimings>{
            //            method: method,
            //            status: data.status,
            //            uri: uri,
            //            time: timeTaken,
            //            error: data
            //        });
            //        deferred.reject(data);
            //    };
            //    var getFn = caller.Get;
            //    $delegate.Get = (uri: string): ng.IPromise<any> =>
            //    {
            //        var startTime = new Date();
            //        var promise = getFn.apply(caller, [uri]);
            //        var defer = $q.defer<any>();
            //        promise.then(
            //            (data) => success($rootScope, data, "GET", uri, new Date().valueOf() - startTime.valueOf(), defer),
            //            (arg: ng.IHttpPromiseCallbackArg<any>) => fail($rootScope, arg, "GET", uri, new Date().valueOf() - startTime.valueOf(), defer)
            //        );
            //        return defer.promise;
            //    };
            //    var postFn = caller.Post;
            //    $delegate.Post = (uri: string, data: any) =>
            //    {
            //        var startTime = new Date();
            //        var promise = postFn.apply(caller, [uri, data]);
            //        var defer = $q.defer<any>();
            //        promise.then(
            //            (arg) => success($rootScope, arg, "POST", uri, new Date().valueOf() - startTime.valueOf(), defer),
            //            (arg: ng.IHttpPromiseCallbackArg<any>) => fail($rootScope, arg, "POST", uri, new Date().valueOf() - startTime.valueOf(), defer)
            //        );
            //        return defer.promise;
            //    };
            //    var putFn = caller.Put;
            //    $delegate.Put = (uri: string, data: any) =>
            //    {
            //        var startTime = new Date();
            //        var promise = putFn.apply(caller, [uri, data]);
            //        var defer = $q.defer<any>();
            //        promise.then(
            //            (arg) => success($rootScope, arg, "PUT", uri, new Date().valueOf() - startTime.valueOf(), defer),
            //            (arg: ng.IHttpPromiseCallbackArg<any>) => fail($rootScope, arg, "PUT", uri, new Date().valueOf() - startTime.valueOf(), defer)
            //        );
            //        return defer.promise;
            //    };
            //    var deleteFn = caller.Delete;
            //    $delegate.Delete = (uri: string) =>
            //    {
            //        var startTime = new Date();
            //        var promise = deleteFn.apply(caller, [uri]);
            //        var defer = $q.defer<any>();
            //        promise.then(
            //            (arg) => success($rootScope, arg, "DELETE", uri, new Date().valueOf() - startTime.valueOf(), defer),
            //            (arg: ng.IHttpPromiseCallbackArg<any>) => fail($rootScope, arg, "DELETE", uri, new Date().valueOf() - startTime.valueOf(), defer)
            //        );
            //        return defer.promise;
            //    };
            //    return $delegate;
            //};
            //$provide.decorator("webRequestService", ["$delegate", "$rootScope", "$q", webRequestServiceTimings]);
            //$httpProvider.interceptors.push('authInterceptorService');
            if (!$httpProvider.defaults.headers.common) {
                $httpProvider.defaults.headers.common = {};
            }
            //disable IE ajax request caching
            $httpProvider.defaults.headers.common["If-Modified-Since"] = "Mon, 26 Jul 1997 05:00:00 GMT";
            // extra
            $httpProvider.defaults.headers.common["Cache-Control"] = "no-cache";
            $httpProvider.defaults.headers.common["Pragma"] = "no-cache";
            //$translateProvider.useLoader('$translateUrlLoader', {
            //    url: configSettings.apiServiceBaseUri + 'api/translations',
            //    queryParameter: 'lang'
            //});
            // TODO: enabled client side translation with this statement $translateProvider.useUrlLoader('Resources/CultureResourceClientSide.json');
            $routeProviderReference = $routeProvider;
        }
    ]);
    App.Module.run(["$rootScope", "$timeout", "$document", "authService", "$route", "$location", "localStorageService",
        function ($rootScope, $timeout, $document, authService, $route, $location, localStorageService) {
            //App.Module.run(function ($rootScope, $timeout, $document)
            //{
            //console.log('starting run');
            // Timeout timer value
            //3600000 1 ชั่วโมง
            var TimeOutTimerValue = 3600000;
            //var TimeOutTimerValue = 5000;
            // Start a timeout
            var TimeOut_Thread = $timeout(function () { LogoutByTimer(); }, TimeOutTimerValue);
            var bodyElement = angular.element($document);
            /// Keyboard Events
            bodyElement.bind('keydown', function (e) { TimeOut_Resetter(e); });
            bodyElement.bind('keyup', function (e) { TimeOut_Resetter(e); });
            /// Mouse Events    
            bodyElement.bind('click', function (e) { TimeOut_Resetter(e); });
            bodyElement.bind('mousemove', function (e) { TimeOut_Resetter(e); });
            bodyElement.bind('DOMMouseScroll', function (e) { TimeOut_Resetter(e); });
            bodyElement.bind('mousewheel', function (e) { TimeOut_Resetter(e); });
            bodyElement.bind('mousedown', function (e) { TimeOut_Resetter(e); });
            /// Touch Events
            bodyElement.bind('touchstart', function (e) { TimeOut_Resetter(e); });
            bodyElement.bind('touchmove', function (e) { TimeOut_Resetter(e); });
            /// Common Events
            bodyElement.bind('scroll', function (e) { TimeOut_Resetter(e); });
            bodyElement.bind('focus', function (e) { TimeOut_Resetter(e); });
            function LogoutByTimer() {
                //console.log('Logout');
                //still login
                if (localStorageService.get("authorizationData") != null) {
                    authService.LogOut();
                    $location.url("/trackandtrace");
                    $rootScope.$broadcast("Logout");
                }
                ///////////////////////////////////////////////////
                /// redirect to another page(eg. Login.html) here
                ///////////////////////////////////////////////////
            }
            function TimeOut_Resetter(e) {
                //console.log('' + e);
                /// Stop the pending timeout
                $timeout.cancel(TimeOut_Thread);
                /// Reset the timeout
                TimeOut_Thread = $timeout(function () { LogoutByTimer(); }, TimeOutTimerValue);
            }
        }]);
    App.Module.config(["blockUIConfig", function (blockUIConfig) {
            blockUIConfig.autoBlock = true;
            blockUIConfig.autoInjectBodyBlock = true;
            blockUIConfig.blockBrowserNavigation = true;
            blockUIConfig.delay = 0;
            // Tell the blockUI service to ignore certain requests
            blockUIConfig.requestFilter = function (config) {
                //Perform a global, case-insensitive search on the request url for 'api' ...
                if (config.url.match(/^\/api\/($|\/).*/)) {
                    return false; // ... don't block it.
                }
            };
            // Change the displayed message based on the http verbs being used.    
            blockUIConfig.requestFilter = function (config) {
                var message;
                switch (config.method) {
                    case 'GET':
                        message = 'Loading ...';
                        break;
                    case 'POST':
                        message = 'Loading ...';
                        break;
                    case 'DELETE':
                        message = 'Loading ...';
                        break;
                    case 'PUT':
                        message = 'Loading ...';
                        break;
                }
                ;
                return message;
            };
        }]);
})(Main || (Main = {}));
//# sourceMappingURL=App.js.map