var ShipmentTracking;
(function (ShipmentTracking) {
    var ShipmentTrackingService = (function () {
        function ShipmentTrackingService($q, serviceProxy, configSettings) {
            this.$q = $q;
            this.serviceProxy = serviceProxy;
            this.configSettings = configSettings;
        }
        ShipmentTrackingService.Factory = function ($q, webRequestService, configSettings) {
            return new ShipmentTrackingService($q, webRequestService, configSettings);
        };
        ShipmentTrackingService.prototype.GetReport = function (param) {
            var uri = this.configSettings.apiServiceBaseUri + "api/report/shipmentTracking";
            return this.serviceProxy.Post(uri, param);
        };
        ShipmentTrackingService.prototype.GetUserDetail = function (userID) {
            var uri = this.configSettings.apiServiceBaseUri + "api/master/shipmenttracking/getuserdetail/userId/" + userID;
            return this.serviceProxy.Get(uri);
        };
        ShipmentTrackingService.prototype.GetCodevalue = function (param) {
            var uri = this.configSettings.apiServiceBaseUri + "api/master/shipmenttracking/getcodevalues";
            return this.serviceProxy.Post(uri, param);
        };
        ShipmentTrackingService.prototype.GetAllRoles = function (userID) {
            var uri = this.configSettings.apiServiceBaseUri + "api/master/shipmenttracking/getallroles/userId/" + userID;
            return this.serviceProxy.Get(uri);
        };
        ShipmentTrackingService.prototype.GetPathCodeQuery = function () {
            var uri = this.configSettings.apiServiceBaseUri + "api/master/shipmenttracking/getpathcodequery";
            return this.serviceProxy.Get(uri);
        };
        ShipmentTrackingService.prototype.GetDistributionCenterQuery = function () {
            var uri = this.configSettings.apiServiceBaseUri + "api/master/shipmenttracking/getdistributioncenterquery";
            return this.serviceProxy.Get(uri);
        };
        ShipmentTrackingService.prototype.GetDistributionCenterQueryWithLocation = function (location) {
            var uri = this.configSettings.apiServiceBaseUri + "api/master/shipmenttracking/getdistributioncenterquerylocation/userLocationId/" + location;
            return this.serviceProxy.Get(uri);
        };
        ShipmentTrackingService.prototype.GetDeliveryPath = function (queryParameter) {
            var uri = this.configSettings.apiServiceBaseUri + "api/master/shipmenttracking/getDeliveryPath";
            return this.serviceProxy.Post(uri, queryParameter);
        };
        return ShipmentTrackingService;
    }());
    ShipmentTracking.ShipmentTrackingService = ShipmentTrackingService;
    Main.App.Services.factory("shipmentTrackingService", ShipmentTrackingService.Factory);
})(ShipmentTracking || (ShipmentTracking = {}));
//# sourceMappingURL=shipmentTrackingService.js.map