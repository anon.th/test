﻿module CostCentre
{
    export class CostCentreService
    {
        public static Factory(
            $q: ng.IQService,
            webRequestService: Util.WebRequestService,
            configSettings: Interfaces.IConfigSettings,
            $http: any)
        {
            return new CostCentreService($q, webRequestService, configSettings, $http);
        }

        constructor(
            public $q: ng.IQService,
            private serviceProxy: Util.WebRequestService,
            private configSettings: Interfaces.IConfigSettings,
            private $http: any
        )
        {

        }

        public GetCostCentreByPagination(queryParameter: Interfaces.ISearchQueryParameter): ng.IPromise<Interfaces.ICostCentre>
        {
            var uri: string = this.configSettings.apiServiceBaseUri + "api/master/references";

            return this.serviceProxy.Post(uri, queryParameter);
        }

    }

    Main.App.Services.factory("costCentreService", ["$q", "webRequestService", "configSettings", CostCentreService.Factory]);
}