var TrackandTrace;
(function (TrackandTrace) {
    var TrackandTraceService = (function () {
        function TrackandTraceService($q, serviceProxy, configSettings, $http) {
            this.$q = $q;
            this.serviceProxy = serviceProxy;
            this.configSettings = configSettings;
            this.$http = $http;
        }
        TrackandTraceService.Factory = function ($q, webRequestService, configSettings, $http) {
            return new TrackandTraceService($q, webRequestService, configSettings, $http);
        };
        //public checkItemStatus(consignmentNo: string, refno: string, userId: string, expandPackageDetails : boolean): ng.IPromise<Interfaces.ITrackAndTraceResult>
        //{
        //    var uri: string = this.configSettings.apiServiceBaseUri + "api/master/trackandtrace/consignmentNo/" + consignmentNo + "/refno/" + refno + "/userId/" + userId + "/expandPackageDetails/" + expandPackageDetails;
        //    return this.serviceProxy.Get(uri);
        //}
        TrackandTraceService.prototype.checkItemStatus = function (paramTrack) {
            var uri = this.configSettings.apiServiceBaseUri + "api/master/trackandtrace/consignmentNo";
            return this.serviceProxy.Post(uri, paramTrack);
        };
        return TrackandTraceService;
    }());
    TrackandTrace.TrackandTraceService = TrackandTraceService;
    Main.App.Services.factory("trackandtraceService", ["$q", "webRequestService", "configSettings", TrackandTraceService.Factory]);
})(TrackandTrace || (TrackandTrace = {}));
//# sourceMappingURL=trackandtraceService.js.map