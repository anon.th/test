var ZipCode;
(function (ZipCode) {
    var ZipCodeService = (function () {
        function ZipCodeService($q, serviceProxy, configSettings, $http) {
            this.$q = $q;
            this.serviceProxy = serviceProxy;
            this.configSettings = configSettings;
            this.$http = $http;
        }
        ZipCodeService.Factory = function ($q, webRequestService, configSettings, $http) {
            return new ZipCodeService($q, webRequestService, configSettings, $http);
        };
        ZipCodeService.prototype.update = function (user) {
            var uri = this.configSettings.apiServiceBaseUri + "api/master/userprofile/";
            return this.serviceProxy.Post(uri, user);
        };
        ZipCodeService.prototype.getUserId = function (userId) {
            var uri = this.configSettings.apiServiceBaseUri + "api/master/userprofile/userId/" + userId;
            return this.serviceProxy.Get(uri);
        };
        ZipCodeService.prototype.resetPassword = function (user) {
            var uri = this.configSettings.apiServiceBaseUri + "api/master/userprofile/resetPassword";
            return this.serviceProxy.Post(uri, user);
        };
        ZipCodeService.prototype.validate = function (user) {
            var uri = this.configSettings.apiServiceBaseUri + "api/master/userprofile/validateUserProfile";
            return this.serviceProxy.Post(uri, user);
        };
        ZipCodeService.prototype.GetReferencesByPagination = function (queryParameter) {
            var uri = this.configSettings.apiServiceBaseUri + "api/master/zipcode";
            return this.serviceProxy.Post(uri, queryParameter);
        };
        return ZipCodeService;
    }());
    ZipCode.ZipCodeService = ZipCodeService;
    Main.App.Services.factory("zipCodeService", ["$q", "webRequestService", "configSettings", ZipCodeService.Factory]);
})(ZipCode || (ZipCode = {}));
//# sourceMappingURL=zipCodeService.js.map