﻿module TrackandTrace
{
    export class TrackandTraceService
    {
        public static Factory(
            $q: ng.IQService,
            webRequestService: Util.WebRequestService,
            configSettings: Interfaces.IConfigSettings,
            $http: any)
        {
            return new TrackandTraceService($q, webRequestService, configSettings,$http);
        }

        constructor(
            public $q: ng.IQService,
            private serviceProxy: Util.WebRequestService,
            private configSettings: Interfaces.IConfigSettings,
            private $http: any
        )
        {

        }

        //public checkItemStatus(consignmentNo: string, refno: string, userId: string, expandPackageDetails : boolean): ng.IPromise<Interfaces.ITrackAndTraceResult>
        //{
        //    var uri: string = this.configSettings.apiServiceBaseUri + "api/master/trackandtrace/consignmentNo/" + consignmentNo + "/refno/" + refno + "/userId/" + userId + "/expandPackageDetails/" + expandPackageDetails;

        //    return this.serviceProxy.Get(uri);
        //}
        public checkItemStatus(paramTrack: Interfaces.ITrackAndTraceParam): ng.IPromise<Interfaces.ITrackAndTraceResult> {
          
            var uri: string = this.configSettings.apiServiceBaseUri + "api/master/trackandtrace/consignmentNo";
            return this.serviceProxy.Post(uri, paramTrack);
        }
    }

    Main.App.Services.factory("trackandtraceService", ["$q", "webRequestService", "configSettings", TrackandTraceService.Factory]);
}