﻿module Home {
    export class HomeService {
        public static Factory(
            $q: ng.IQService,
            webRequestService: Util.WebRequestService,
            configSettings: Interfaces.IConfigSettings,
            $http: any) {
            return new HomeService($q, webRequestService, configSettings, $http);
        }

        constructor(
            public $q: ng.IQService,
            private serviceProxy: Util.WebRequestService,
            private configSettings: Interfaces.IConfigSettings,
            private $http: any
        ) {

        }

        public checkItemStatus(consignmentNo: string, refno: string, userId: string): ng.IPromise<Interfaces.ITrackAndTraceResult> {
            var uri: string = this.configSettings.apiServiceBaseUri + "api/master/trackandtrace/consignmentNo/" + consignmentNo + "/refno/" + refno + "/userId/" + userId;

            return this.serviceProxy.Get(uri);
        }

        public getAllmodules(userid: string, moduleid: string): ng.IPromise<Interfaces.IMenu> {
            var uri: string = this.configSettings.apiServiceBaseUri + "api/master/menu/userid/" + userid + "/moduleid/" + moduleid;

            return this.serviceProxy.Get(uri);
        }
    }

    Main.App.Services.factory("homeService", ["$q", "webRequestService", "configSettings", HomeService.Factory]);
}