﻿module CreateConsignment
{
    export class CreateConsignmentService
    {
        public static Factory(
            $q: ng.IQService,
            webRequestService: Util.WebRequestService,
            configSettings: Interfaces.IConfigSettings,
            $http: any)
        {
            return new CreateConsignmentService($q, webRequestService, configSettings, $http);
        }

        constructor(
            public $q: ng.IQService,
            private serviceProxy: Util.WebRequestService,
            private configSettings: Interfaces.IConfigSettings,
            private $http: any
        )
        {

        }

        public getSpCSSConsignmentDetail(param: Interfaces.ICreateConsignmentParam): ng.IPromise<Interfaces.ICreateConsignmentResult>
        {
            var uri: string = this.configSettings.apiServiceBaseUri + "api/master/createconsignment/getcreconstore";

            return this.serviceProxy.Post(uri, param);
        }
        public updateReferenceStoreProcedure(param: Interfaces.ICreateConsignmentParam): ng.IPromise<Interfaces.IUpdateConsignment> {
            var uri: string = this.configSettings.apiServiceBaseUri + "api/master/createconsignment/updatereference";

            return this.serviceProxy.Post(uri, param);
        }

        public getBindSenderDetail(userId: string): ng.IPromise<Interfaces.ICreateConsignmentBindSenderDetail>
        {
            var uri: string = this.configSettings.apiServiceBaseUri + "api/master/createconsignment/getbindsenderdetail/userId/" + userId;

            return this.serviceProxy.Get(uri);
        }
        public getCustomerAccount(userId: string): ng.IPromise<Interfaces.ICreateConsignmentCustomerAccount>
        {
            var uri: string = this.configSettings.apiServiceBaseUri + "api/master/createconsignment/getcustomeraccount/userId/" + userId;

            return this.serviceProxy.Get(uri);
        }

        public getUserAssignedRole(userId: string): ng.IPromise<boolean>
        {
            var uri: string = this.configSettings.apiServiceBaseUri + "api/master/createconsignment/getuserassignedrole/userId/" + userId;

            return this.serviceProxy.Get(uri);
        }

        public getCustomsWarehousesNameDefault(userId: string): ng.IPromise<Interfaces.ICreateConsignmentCustomsWarehousesNameDefault[]>
        {
            var uri: string = this.configSettings.apiServiceBaseUri + "api/master/createconsignment/getquerysndrecname/userId/" + userId;

            return this.serviceProxy.Get(uri);
        }

        public getServiceCode(): ng.IPromise<Interfaces.ICreateConsignmentServiceCode>
        {
            var uri: string = this.configSettings.apiServiceBaseUri + "api/master/createconsignment/getservicecode";

            return this.serviceProxy.Get(uri);
        }
        public getCustomsWarehousesName(selectedValue: string): ng.IPromise<Interfaces.ICreateConsignmentCustomsWarehousesName>
        {
            var uri: string = this.configSettings.apiServiceBaseUri + "api/master/createconsignment/getwarehousename/selectValue/" + selectedValue;

            return this.serviceProxy.Get(uri);
        }
        public getStateName(zipcode: string): ng.IPromise<Interfaces.ICreateConsignmentGetStateName>
        {
            var uri: string = this.configSettings.apiServiceBaseUri + "api/master/createconsignment/getstatename/zipcode/" + zipcode;

            return this.serviceProxy.Get(uri);
        }
        public getRecipientDetailByTelephone(telephone: string): ng.IPromise<Interfaces.ICreateConsignmentBindRecipientDetail> {
            var uri: string = this.configSettings.apiServiceBaseUri + "api/master/createconsignment/getbytelephone/telephone/" + telephone;

            return this.serviceProxy.Get(uri);
        }

        public getRecipientDetailByCostCentre(costcentre: string): ng.IPromise<Interfaces.ICreateConsignmentBindRecipientDetail> {
            var uri: string = this.configSettings.apiServiceBaseUri + "api/master/createconsignment/getbycostcentre/costcentre/" + costcentre;

            return this.serviceProxy.Get(uri);
        }
        public getCustomerAccount2(payerId: string): ng.IPromise<Interfaces.ICreateConsignmentCustomerAccount2> {
            var uri: string = this.configSettings.apiServiceBaseUri + "api/master/createconsignment/getcustomeracc2/payerId/" + payerId;

            return this.serviceProxy.Get(uri);
        }
        public getPackageLimit(userId: string): ng.IPromise<Interfaces.ICreateConsignmentGetPackageLimit[]> {
            var uri: string = this.configSettings.apiServiceBaseUri + "api/master/createconsignment/getpackagelimit/userId/" + userId;

            return this.serviceProxy.Get(uri);
        }

        public getConfiguration(userId: string): ng.IPromise<Interfaces.ICreateConsignmentGetConfiguration[]> {
            var uri: string = this.configSettings.apiServiceBaseUri + "api/master/createconsignment/getconfiguration/userId/" + userId;

            return this.serviceProxy.Get(uri);
        }

        public getEnterpriseContract(userId: string): ng.IPromise<Interfaces.ICreateConsignmentGetEnterpriseContract> {
            var uri: string = this.configSettings.apiServiceBaseUri + "api/master/createconsignment/getenterprisecontract/userId/" + userId;

            return this.serviceProxy.Get(uri);
        }

        public getDefaultServiceCode(userId: string): ng.IPromise<Interfaces.ICreateConsignmentDefaultServiceCode> {
            var uri: string = this.configSettings.apiServiceBaseUri + "api/master/createconsignment/getdefaultservicecode/userId/" + userId;

            return this.serviceProxy.Get(uri);
        }
    }

    Main.App.Services.factory("createConsignmentService", ["$q", "webRequestService", "configSettings", CreateConsignmentService.Factory]);
}