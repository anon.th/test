﻿module ShipmentTracking {
    export class ShipmentTrackingService {
        public static Factory(
            $q: ng.IQService,
            webRequestService: Util.WebRequestService,
            configSettings: Interfaces.IConfigSettings) {
            return new ShipmentTrackingService($q, webRequestService, configSettings);
        }

        constructor(
            private $q: ng.IQService,
            private serviceProxy: Util.WebRequestService,
            private configSettings: Interfaces.IConfigSettings
        ) {

        }

        public GetReport(param: Interfaces.ISearchShipmentTrackingReport): ng.IPromise<Interfaces.FileResult[]> {
            var uri: string = this.configSettings.apiServiceBaseUri + "api/report/shipmentTracking";
            return this.serviceProxy.Post(uri, param);
        }

        public GetUserDetail(userID: string): ng.IPromise<Interfaces.IserachShipmentTrackingUserDetail> {
            var uri: string = this.configSettings.apiServiceBaseUri + "api/master/shipmenttracking/getuserdetail/userId/" + userID ;
            return this.serviceProxy.Get(uri);
        }

        public GetCodevalue(param: Interfaces.ISearchShipmentTrackingParam): ng.IPromise<Interfaces.ISearchShipmentTrackingGetCodeValue[]> {
            var uri: string = this.configSettings.apiServiceBaseUri + "api/master/shipmenttracking/getcodevalues";
            return this.serviceProxy.Post(uri, param);
        }

        public GetAllRoles(userID: string): ng.IPromise<Interfaces.ISearchShipmentTrackingAllRoles[]> {
            var uri: string = this.configSettings.apiServiceBaseUri + "api/master/shipmenttracking/getallroles/userId/" + userID;
            return this.serviceProxy.Get(uri);
        }

        public GetPathCodeQuery(): ng.IPromise<Interfaces.ISearchShipmentTrackingPathCodeQuery[]> {
            var uri: string = this.configSettings.apiServiceBaseUri + "api/master/shipmenttracking/getpathcodequery" ;
            return this.serviceProxy.Get(uri);
        }

        public GetDistributionCenterQuery(): ng.IPromise<Interfaces.ISearchShipmentTrackingPathCodeQuery[]> {
            var uri: string = this.configSettings.apiServiceBaseUri + "api/master/shipmenttracking/getdistributioncenterquery";
            return this.serviceProxy.Get(uri);
        }

        public GetDistributionCenterQueryWithLocation(location: string): ng.IPromise<Interfaces.ISearchShipmentTrackingPathCodeQuery[]> {
            var uri: string = this.configSettings.apiServiceBaseUri + "api/master/shipmenttracking/getdistributioncenterquerylocation/userLocationId/" + location;
            return this.serviceProxy.Get(uri);
        }

        public GetDeliveryPath(queryParameter: Interfaces.IserachShipmentTrackingDeliveryPathParam): ng.IPromise<Interfaces.IserachShipmentTrackingDeliveryPath> {
            var uri: string = this.configSettings.apiServiceBaseUri + "api/master/shipmenttracking/getDeliveryPath";
            return this.serviceProxy.Post(uri, queryParameter);
        }
    }

    Main.App.Services.factory("shipmentTrackingService", ShipmentTrackingService.Factory);
}