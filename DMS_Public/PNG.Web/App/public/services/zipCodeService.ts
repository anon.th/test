﻿module ZipCode
{
    export class ZipCodeService
    {
        public static Factory(
            $q: ng.IQService,
            webRequestService: Util.WebRequestService,
            configSettings: Interfaces.IConfigSettings,
            $http: any)
        {
            return new ZipCodeService($q, webRequestService, configSettings, $http);
        }

        constructor(
            public $q: ng.IQService,
            private serviceProxy: Util.WebRequestService,
            private configSettings: Interfaces.IConfigSettings,
            private $http: any
        )
        {

        }

        public update(user: Interfaces.IUserMaster): ng.IPromise<boolean>
        {
            var uri: string = this.configSettings.apiServiceBaseUri + "api/master/userprofile/";

            return this.serviceProxy.Post(uri, user);
        }

        public getUserId(userId: string): ng.IPromise<Interfaces.IUserMaster>
        {
            var uri: string = this.configSettings.apiServiceBaseUri + "api/master/userprofile/userId/" + userId;
            return this.serviceProxy.Get(uri);
        }

        public resetPassword(user: Interfaces.IUserMaster): ng.IPromise<boolean>
        {
            var uri: string = this.configSettings.apiServiceBaseUri + "api/master/userprofile/resetPassword";

            return this.serviceProxy.Post(uri, user);
        }

        public validate(user: Interfaces.IUserMaster): ng.IPromise<string>
        {
            var uri: string = this.configSettings.apiServiceBaseUri + "api/master/userprofile/validateUserProfile";

            return this.serviceProxy.Post(uri, user);
        }

        public GetReferencesByPagination(queryParameter: Interfaces.ISearchQueryParameter): ng.IPromise<Interfaces.IZipCode>
        {
            var uri: string = this.configSettings.apiServiceBaseUri + "api/master/zipcode";

            return this.serviceProxy.Post(uri, queryParameter);
        }

    }

    Main.App.Services.factory("zipCodeService", ["$q", "webRequestService", "configSettings", ZipCodeService.Factory]);
}