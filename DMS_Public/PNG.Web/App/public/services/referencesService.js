var CostCentre;
(function (CostCentre) {
    var CostCentreService = (function () {
        function CostCentreService($q, serviceProxy, configSettings, $http) {
            this.$q = $q;
            this.serviceProxy = serviceProxy;
            this.configSettings = configSettings;
            this.$http = $http;
        }
        CostCentreService.Factory = function ($q, webRequestService, configSettings, $http) {
            return new CostCentreService($q, webRequestService, configSettings, $http);
        };
        CostCentreService.prototype.GetCostCentreByPagination = function (queryParameter) {
            var uri = this.configSettings.apiServiceBaseUri + "api/master/references";
            return this.serviceProxy.Post(uri, queryParameter);
        };
        return CostCentreService;
    }());
    CostCentre.CostCentreService = CostCentreService;
    Main.App.Services.factory("costCentreService", ["$q", "webRequestService", "configSettings", CostCentreService.Factory]);
})(CostCentre || (CostCentre = {}));
//# sourceMappingURL=referencesService.js.map