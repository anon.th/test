var Home;
(function (Home) {
    var HomeService = (function () {
        function HomeService($q, serviceProxy, configSettings, $http) {
            this.$q = $q;
            this.serviceProxy = serviceProxy;
            this.configSettings = configSettings;
            this.$http = $http;
        }
        HomeService.Factory = function ($q, webRequestService, configSettings, $http) {
            return new HomeService($q, webRequestService, configSettings, $http);
        };
        HomeService.prototype.checkItemStatus = function (consignmentNo, refno, userId) {
            var uri = this.configSettings.apiServiceBaseUri + "api/master/trackandtrace/consignmentNo/" + consignmentNo + "/refno/" + refno + "/userId/" + userId;
            return this.serviceProxy.Get(uri);
        };
        HomeService.prototype.getAllmodules = function (userid, moduleid) {
            var uri = this.configSettings.apiServiceBaseUri + "api/master/menu/userid/" + userid + "/moduleid/" + moduleid;
            return this.serviceProxy.Get(uri);
        };
        return HomeService;
    }());
    Home.HomeService = HomeService;
    Main.App.Services.factory("homeService", ["$q", "webRequestService", "configSettings", HomeService.Factory]);
})(Home || (Home = {}));
//# sourceMappingURL=homeService.js.map