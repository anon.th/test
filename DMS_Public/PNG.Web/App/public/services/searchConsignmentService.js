var SearchConsignment;
(function (SearchConsignment) {
    var SearchConsignmentService = (function () {
        function SearchConsignmentService($q, serviceProxy, configSettings, $http) {
            this.$q = $q;
            this.serviceProxy = serviceProxy;
            this.configSettings = configSettings;
            this.$http = $http;
        }
        SearchConsignmentService.Factory = function ($q, webRequestService, configSettings, $http) {
            return new SearchConsignmentService($q, webRequestService, configSettings, $http);
        };
        SearchConsignmentService.prototype.getStatusPrinting = function () {
            var uri = this.configSettings.apiServiceBaseUri + "api/master/searchconsignment/getstatusprintingconfig";
            return this.serviceProxy.Get(uri);
        };
        SearchConsignmentService.prototype.getReferenceDS = function (userId) {
            var uri = this.configSettings.apiServiceBaseUri + "api/master/searchconsignment/getreferenceds/userId/" + userId;
            return this.serviceProxy.Get(uri);
        };
        SearchConsignmentService.prototype.getCustomsWarehousesName = function (selectValue) {
            var uri = this.configSettings.apiServiceBaseUri + "api/master/searchconsignment/getcustomswarehousesname/selectValue/" + selectValue;
            return this.serviceProxy.Get(uri);
        };
        SearchConsignmentService.prototype.getEnterpriseUserAccounts = function (userId) {
            var uri = this.configSettings.apiServiceBaseUri + "api/master/searchconsignment/getenterpriseuseraccounts/userId/" + userId;
            return this.serviceProxy.Get(uri);
        };
        SearchConsignmentService.prototype.getCustomStatus = function () {
            var uri = this.configSettings.apiServiceBaseUri + "api/master/searchconsignment/getcustomstatus";
            return this.serviceProxy.Get(uri);
        };
        SearchConsignmentService.prototype.getCustomerStatus = function () {
            var uri = this.configSettings.apiServiceBaseUri + "api/master/searchconsignment/getcustomerstatus";
            return this.serviceProxy.Get(uri);
        };
        SearchConsignmentService.prototype.getSpCSS_ConsignmentStatus = function (param) {
            var uri = this.configSettings.apiServiceBaseUri + "api/master/searchconsignment/getcssconsignmentstatus";
            return this.serviceProxy.Post(uri, param);
        };
        SearchConsignmentService.prototype.getSpCSS_ReprintConsNote = function (param) {
            var uri = this.configSettings.apiServiceBaseUri + "api/master/searchconsignment/getcssreprintconsnote";
            return this.serviceProxy.Post(uri, param);
        };
        SearchConsignmentService.prototype.getSpCSS_PrintShippingList = function (param) {
            var uri = this.configSettings.apiServiceBaseUri + "api/master/searchconsignment/getcssprintshippinglist";
            return this.serviceProxy.Post(uri, param);
        };
        SearchConsignmentService.prototype.getSpCSS_PrintConsNote = function (param) {
            var uri = this.configSettings.apiServiceBaseUri + "api/master/searchconsignment/getcssprintconsnote";
            return this.serviceProxy.Post(uri, param);
        };
        SearchConsignmentService.prototype.load_PrintConsNote = function (param) {
            var uri = this.configSettings.apiServiceBaseUri + "api/report/loadcssprintconsnote";
            return this.serviceProxy.Post(uri, param);
        };
        SearchConsignmentService.prototype.load_PrintShippingList = function (param) {
            var uri = this.configSettings.apiServiceBaseUri + "api/report/loadcssprintshippinglist";
            return this.serviceProxy.Post(uri, param);
        };
        SearchConsignmentService.prototype.load_ReprintConsNote = function (param) {
            var uri = this.configSettings.apiServiceBaseUri + "api/report/loadcssreprintconsnote";
            return this.serviceProxy.Post(uri, param);
        };
        SearchConsignmentService.prototype.load_ReprintConsNoteLabel = function (param) {
            var uri = this.configSettings.apiServiceBaseUri + "api/report/loadcssreprintconsnotelabel";
            return this.serviceProxy.Post(uri, param);
        };
        return SearchConsignmentService;
    }());
    SearchConsignment.SearchConsignmentService = SearchConsignmentService;
    Main.App.Services.factory("searchConsignmentService", ["$q", "webRequestService", "configSettings", SearchConsignmentService.Factory]);
})(SearchConsignment || (SearchConsignment = {}));
//# sourceMappingURL=searchConsignmentService.js.map