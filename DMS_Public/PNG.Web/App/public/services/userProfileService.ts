﻿module UserProfile
{
    export class UserProfileService
    {
        public static Factory(
            $q: ng.IQService,
            webRequestService: Util.WebRequestService,
            configSettings: Interfaces.IConfigSettings,
            $http: any)
        {
            return new UserProfileService($q, webRequestService, configSettings, $http);
        }

        constructor(
            public $q: ng.IQService,
            private serviceProxy: Util.WebRequestService,
            private configSettings: Interfaces.IConfigSettings,
            private $http: any
        )
        {

        }

        public update(user: Interfaces.IUserMaster): ng.IPromise<boolean>
        {
            var uri: string = this.configSettings.apiServiceBaseUri + "api/master/userprofile/";

            return this.serviceProxy.Post(uri, user);
        }

        public getUserId(userId: string): ng.IPromise<Interfaces.IUserMaster>
        {
            var uri: string = this.configSettings.apiServiceBaseUri + "api/master/userprofile/userId/" + userId;
            return this.serviceProxy.Get(uri);
        }

        public resetPassword(user: Interfaces.IUserMaster): ng.IPromise<boolean>
        {
            var uri: string = this.configSettings.apiServiceBaseUri + "api/master/userprofile/resetPassword";

            return this.serviceProxy.Post(uri, user);
        }

        public validate(user: Interfaces.IUserMaster): ng.IPromise<string>
        {
            var uri: string = this.configSettings.apiServiceBaseUri + "api/master/userprofile/validateUserProfile";

            return this.serviceProxy.Post(uri, user);
        }

    }

    Main.App.Services.factory("userProfileService", ["$q", "webRequestService", "configSettings", UserProfileService.Factory]);
}