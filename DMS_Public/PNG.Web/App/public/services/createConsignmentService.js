var CreateConsignment;
(function (CreateConsignment) {
    var CreateConsignmentService = (function () {
        function CreateConsignmentService($q, serviceProxy, configSettings, $http) {
            this.$q = $q;
            this.serviceProxy = serviceProxy;
            this.configSettings = configSettings;
            this.$http = $http;
        }
        CreateConsignmentService.Factory = function ($q, webRequestService, configSettings, $http) {
            return new CreateConsignmentService($q, webRequestService, configSettings, $http);
        };
        CreateConsignmentService.prototype.getSpCSSConsignmentDetail = function (param) {
            var uri = this.configSettings.apiServiceBaseUri + "api/master/createconsignment/getcreconstore";
            return this.serviceProxy.Post(uri, param);
        };
        CreateConsignmentService.prototype.updateReferenceStoreProcedure = function (param) {
            var uri = this.configSettings.apiServiceBaseUri + "api/master/createconsignment/updatereference";
            return this.serviceProxy.Post(uri, param);
        };
        CreateConsignmentService.prototype.getBindSenderDetail = function (userId) {
            var uri = this.configSettings.apiServiceBaseUri + "api/master/createconsignment/getbindsenderdetail/userId/" + userId;
            return this.serviceProxy.Get(uri);
        };
        CreateConsignmentService.prototype.getCustomerAccount = function (userId) {
            var uri = this.configSettings.apiServiceBaseUri + "api/master/createconsignment/getcustomeraccount/userId/" + userId;
            return this.serviceProxy.Get(uri);
        };
        CreateConsignmentService.prototype.getUserAssignedRole = function (userId) {
            var uri = this.configSettings.apiServiceBaseUri + "api/master/createconsignment/getuserassignedrole/userId/" + userId;
            return this.serviceProxy.Get(uri);
        };
        CreateConsignmentService.prototype.getCustomsWarehousesNameDefault = function (userId) {
            var uri = this.configSettings.apiServiceBaseUri + "api/master/createconsignment/getquerysndrecname/userId/" + userId;
            return this.serviceProxy.Get(uri);
        };
        CreateConsignmentService.prototype.getServiceCode = function () {
            var uri = this.configSettings.apiServiceBaseUri + "api/master/createconsignment/getservicecode";
            return this.serviceProxy.Get(uri);
        };
        CreateConsignmentService.prototype.getCustomsWarehousesName = function (selectedValue) {
            var uri = this.configSettings.apiServiceBaseUri + "api/master/createconsignment/getwarehousename/selectValue/" + selectedValue;
            return this.serviceProxy.Get(uri);
        };
        CreateConsignmentService.prototype.getStateName = function (zipcode) {
            var uri = this.configSettings.apiServiceBaseUri + "api/master/createconsignment/getstatename/zipcode/" + zipcode;
            return this.serviceProxy.Get(uri);
        };
        CreateConsignmentService.prototype.getRecipientDetailByTelephone = function (telephone) {
            var uri = this.configSettings.apiServiceBaseUri + "api/master/createconsignment/getbytelephone/telephone/" + telephone;
            return this.serviceProxy.Get(uri);
        };
        CreateConsignmentService.prototype.getRecipientDetailByCostCentre = function (costcentre) {
            var uri = this.configSettings.apiServiceBaseUri + "api/master/createconsignment/getbycostcentre/costcentre/" + costcentre;
            return this.serviceProxy.Get(uri);
        };
        CreateConsignmentService.prototype.getCustomerAccount2 = function (payerId) {
            var uri = this.configSettings.apiServiceBaseUri + "api/master/createconsignment/getcustomeracc2/payerId/" + payerId;
            return this.serviceProxy.Get(uri);
        };
        CreateConsignmentService.prototype.getPackageLimit = function (userId) {
            var uri = this.configSettings.apiServiceBaseUri + "api/master/createconsignment/getpackagelimit/userId/" + userId;
            return this.serviceProxy.Get(uri);
        };
        CreateConsignmentService.prototype.getConfiguration = function (userId) {
            var uri = this.configSettings.apiServiceBaseUri + "api/master/createconsignment/getconfiguration/userId/" + userId;
            return this.serviceProxy.Get(uri);
        };
        CreateConsignmentService.prototype.getEnterpriseContract = function (userId) {
            var uri = this.configSettings.apiServiceBaseUri + "api/master/createconsignment/getenterprisecontract/userId/" + userId;
            return this.serviceProxy.Get(uri);
        };
        CreateConsignmentService.prototype.getDefaultServiceCode = function (userId) {
            var uri = this.configSettings.apiServiceBaseUri + "api/master/createconsignment/getdefaultservicecode/userId/" + userId;
            return this.serviceProxy.Get(uri);
        };
        return CreateConsignmentService;
    }());
    CreateConsignment.CreateConsignmentService = CreateConsignmentService;
    Main.App.Services.factory("createConsignmentService", ["$q", "webRequestService", "configSettings", CreateConsignmentService.Factory]);
})(CreateConsignment || (CreateConsignment = {}));
//# sourceMappingURL=createConsignmentService.js.map