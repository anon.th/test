var UserProfile;
(function (UserProfile) {
    var UserProfileService = (function () {
        function UserProfileService($q, serviceProxy, configSettings, $http) {
            this.$q = $q;
            this.serviceProxy = serviceProxy;
            this.configSettings = configSettings;
            this.$http = $http;
        }
        UserProfileService.Factory = function ($q, webRequestService, configSettings, $http) {
            return new UserProfileService($q, webRequestService, configSettings, $http);
        };
        UserProfileService.prototype.update = function (user) {
            var uri = this.configSettings.apiServiceBaseUri + "api/master/userprofile/";
            return this.serviceProxy.Post(uri, user);
        };
        UserProfileService.prototype.getUserId = function (userId) {
            var uri = this.configSettings.apiServiceBaseUri + "api/master/userprofile/userId/" + userId;
            return this.serviceProxy.Get(uri);
        };
        UserProfileService.prototype.resetPassword = function (user) {
            var uri = this.configSettings.apiServiceBaseUri + "api/master/userprofile/resetPassword";
            return this.serviceProxy.Post(uri, user);
        };
        UserProfileService.prototype.validate = function (user) {
            var uri = this.configSettings.apiServiceBaseUri + "api/master/userprofile/validateUserProfile";
            return this.serviceProxy.Post(uri, user);
        };
        return UserProfileService;
    }());
    UserProfile.UserProfileService = UserProfileService;
    Main.App.Services.factory("userProfileService", ["$q", "webRequestService", "configSettings", UserProfileService.Factory]);
})(UserProfile || (UserProfile = {}));
//# sourceMappingURL=userProfileService.js.map