﻿module SearchConsignment {
    export class SearchConsignmentService {
        public static Factory(
            $q: ng.IQService,
            webRequestService: Util.WebRequestService,
            configSettings: Interfaces.IConfigSettings,
            $http: any) {
            return new SearchConsignmentService($q, webRequestService, configSettings, $http);
        }

        constructor(
            public $q: ng.IQService,
            private serviceProxy: Util.WebRequestService,
            private configSettings: Interfaces.IConfigSettings,
            private $http: any
        ) {

        }

        public getStatusPrinting(): ng.IPromise<Interfaces.ISearchConsignmentGetstatusprintingconfig> {
            var uri: string = this.configSettings.apiServiceBaseUri + "api/master/searchconsignment/getstatusprintingconfig";

            return this.serviceProxy.Get(uri);
        }

        public getReferenceDS(userId: string): ng.IPromise<Interfaces.ICreateConsignmentCustomsWarehousesNameDefault[]> {
            var uri: string = this.configSettings.apiServiceBaseUri + "api/master/searchconsignment/getreferenceds/userId/" + userId;

            return this.serviceProxy.Get(uri);
        }

        public getCustomsWarehousesName(selectValue: string): ng.IPromise<Interfaces.ICreateConsignmentCustomsWarehousesName[]> {
            var uri: string = this.configSettings.apiServiceBaseUri + "api/master/searchconsignment/getcustomswarehousesname/selectValue/" + selectValue;

            return this.serviceProxy.Get(uri);
        }

        public getEnterpriseUserAccounts(userId: string): ng.IPromise<string> {
            var uri: string = this.configSettings.apiServiceBaseUri + "api/master/searchconsignment/getenterpriseuseraccounts/userId/" + userId;

            return this.serviceProxy.Get(uri);
        }

        public getCustomStatus(): ng.IPromise<Interfaces.ISearchConsignmentGetCustomStatus[]> {
            var uri: string = this.configSettings.apiServiceBaseUri + "api/master/searchconsignment/getcustomstatus";

            return this.serviceProxy.Get(uri);
        }

        public getCustomerStatus(): ng.IPromise<Interfaces.ISearchConsignmentGetCustomStatus[]> {
            var uri: string = this.configSettings.apiServiceBaseUri + "api/master/searchconsignment/getcustomerstatus";

            return this.serviceProxy.Get(uri);
        }

        public getSpCSS_ConsignmentStatus(param: Interfaces.ISearchConsignmentParam): ng.IPromise<Interfaces.ISearchConsignmentResult> {
            var uri: string = this.configSettings.apiServiceBaseUri + "api/master/searchconsignment/getcssconsignmentstatus";

            return this.serviceProxy.Post(uri, param);
        }

        public getSpCSS_ReprintConsNote(param: Interfaces.ISearchConsignmentReprintConsNoteParam): ng.IPromise<Interfaces.ISearchConsignmentReprintConsNoteResult> {
            var uri: string = this.configSettings.apiServiceBaseUri + "api/master/searchconsignment/getcssreprintconsnote";

            return this.serviceProxy.Post(uri, param);
        }

        public getSpCSS_PrintShippingList(param: Interfaces.ISearchConsignmentPrintShippingListParam): ng.IPromise<Interfaces.ISearchConsignmentReprintShippingListResult> {
            var uri: string = this.configSettings.apiServiceBaseUri + "api/master/searchconsignment/getcssprintshippinglist";

            return this.serviceProxy.Post(uri, param);
        }

        public getSpCSS_PrintConsNote(param: Interfaces.ISearchConsignmentPrintShippingListParam): ng.IPromise<Interfaces.ISearchConsignmentPrintConsNoteResult> {
            var uri: string = this.configSettings.apiServiceBaseUri + "api/master/searchconsignment/getcssprintconsnote";

            return this.serviceProxy.Post(uri, param);
        }

        public load_PrintConsNote(param: Interfaces.IPrintConsNote): ng.IPromise<Interfaces.FileResult[]> {
            var uri: string = this.configSettings.apiServiceBaseUri + "api/report/loadcssprintconsnote";
            return this.serviceProxy.Post(uri, param);
        }

        public load_PrintShippingList(param: Interfaces.ISearchConsignmentPrintShippingListDetail[]): ng.IPromise<Interfaces.FileResult[]> {
            var uri: string = this.configSettings.apiServiceBaseUri + "api/report/loadcssprintshippinglist";
            return this.serviceProxy.Post(uri, param);
        }

        public load_ReprintConsNote(param: Interfaces.IReprintConsNote): ng.IPromise<Interfaces.FileResult[]> {
            var uri: string = this.configSettings.apiServiceBaseUri + "api/report/loadcssreprintconsnote";
            return this.serviceProxy.Post(uri, param);
        }

        public load_ReprintConsNoteLabel(param: Interfaces.IReprintConsNote): ng.IPromise<Interfaces.FileResult[]> {
            var uri: string = this.configSettings.apiServiceBaseUri + "api/report/loadcssreprintconsnotelabel";
            return this.serviceProxy.Post(uri, param);
        }

       
    }

    Main.App.Services.factory("searchConsignmentService", ["$q", "webRequestService", "configSettings", SearchConsignmentService.Factory]);
}