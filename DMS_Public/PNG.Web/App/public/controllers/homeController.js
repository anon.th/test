var Home;
(function (Home) {
    var HomeController = (function () {
        function HomeController($rootScope, $scope, $location, authService, $cookies, configSettings, localStorageService, $sessionStorage, clientConnectionService, trackandtraceService, userProfileService, homeService, $route) {
            var _this = this;
            this.$rootScope = $rootScope;
            this.$scope = $scope;
            this.$location = $location;
            this.authService = authService;
            this.$cookies = $cookies;
            this.configSettings = configSettings;
            this.localStorageService = localStorageService;
            this.$sessionStorage = $sessionStorage;
            this.clientConnectionService = clientConnectionService;
            this.trackandtraceService = trackandtraceService;
            this.userProfileService = userProfileService;
            this.homeService = homeService;
            this.$route = $route;
            $scope.vm = this;
            //this.InitHome();
            $scope.authentication = authService.authentication;
            this.$scope.authentication = this.localStorageService.get('authorizationData');
            //this.$scope.currentPath = this.$location.absUrl;
            this.$scope.currentPath = $location.path();
            //console.log(window.location.pathname);
            //console.log(this.$scope.currentPath);
            //get usermenu from localstorage to UserInfo
            authService.FillAuthData()
                .then(function () {
                _this.$scope.UserInfo = _this.authService.userInfo;
                _this.$sessionStorage.UserName = _this.$scope.UserInfo.UserName;
            }).then(function () {
                if (_this.localStorageService.get('authorizationData') != null) {
                    _this.getMenu(_this.localStorageService.get('authorizationData').userName)
                        .then(function () {
                        _this.$scope.authentication = _this.localStorageService.get('authorizationData');
                    });
                }
            });
            //if (this.$scope.authentication != null)
            //{
            //this.$scope.userId = this.$scope.authentication.userName;
            //}
            //get usermenu from database
            this.$scope.$on("UserInfo", function (event) {
                var data = [];
                for (var _i = 1; _i < arguments.length; _i++) {
                    data[_i - 1] = arguments[_i];
                }
                //this.getMenu();
                if (_this.localStorageService.get('authorizationData') != null) {
                    _this.$scope.authentication = _this.localStorageService.get('authorizationData');
                    _this.getUserInfo(_this.localStorageService.get('authorizationData').userName).then(function () {
                        _this.getMenu(_this.localStorageService.get('authorizationData').userName);
                    });
                }
            });
            //get usermenu from database
            this.$scope.$on("Logout", function (event) {
                var data = [];
                for (var _i = 1; _i < arguments.length; _i++) {
                    data[_i - 1] = arguments[_i];
                }
                _this.logOutbyTimeOut();
            });
        }
        HomeController.prototype.clearSearch = function () {
            this.$scope.trackAndTraceHeader = null;
            this.$scope.trackAndTraceDetailTable = [];
            this.$scope.trackingNumberArray = [];
        };
        HomeController.prototype.getMenu = function (userName) {
            var _this = this;
            var parentId = "06Customer Self Service";
            //this.$scope.userId = "puma";
            return this.homeService.getAllmodules(userName, parentId)
                .then(function (result) {
                if (result != null) {
                    _this.$scope.navTopMenu = result;
                }
            }, (function (error) {
                console.log("error => ", error);
            }));
        };
        HomeController.prototype.logOutbyTimeOut = function () {
            this.authService.LogOut();
            this.$scope.authentication = null;
            this.$location.url('/trackandtrace');
            this.$scope.navTopMenu = null;
            this.localStorageService.remove("RecentNumber");
            this.$route.reload();
        };
        HomeController.prototype.logOut = function () {
            this.authService.LogOut();
            this.$scope.authentication = null;
            this.$location.url('/login');
            this.$scope.navTopMenu = null;
            this.localStorageService.remove("RecentNumber");
        };
        HomeController.prototype.getUserInfo = function (userId) {
            var _this = this;
            return this.userProfileService.getUserId(userId)
                .then(function (result) {
                _this.$scope.user = result;
                _this.localStorageService.set("UserInfo", _this.$scope.user);
            }, (function (error) {
                //this.authService.LogOut();
                console.log("error", error);
                //this.errorHandlerService.HandleException(error, "loadStatus");
            }));
        };
        HomeController.$inject = ["$rootScope", "$scope", "$location", "authService",
            "$cookies", "configSettings",
            "localStorageService",
            "$sessionStorage", "clientConnectionService", "trackandtraceService", "userProfileService", "homeService", "$route"];
        return HomeController;
    }());
    Home.HomeController = HomeController;
    Main.App.Controllers.controller("homeController", HomeController);
})(Home || (Home = {}));
//# sourceMappingURL=homeController.js.map