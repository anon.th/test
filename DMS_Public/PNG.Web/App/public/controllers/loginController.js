var Login;
(function (Login) {
    var LoginController = (function () {
        function LoginController($scope, $rootScope, $location, authService, configSettings, errorHandlerService, localStorageService, dialogHelperService, clientConnectionService, $q, $route, $sessionStorage, homeService) {
            this.$scope = $scope;
            this.$rootScope = $rootScope;
            this.$location = $location;
            this.authService = authService;
            this.configSettings = configSettings;
            this.errorHandlerService = errorHandlerService;
            this.localStorageService = localStorageService;
            this.dialogHelperService = dialogHelperService;
            this.clientConnectionService = clientConnectionService;
            this.$q = $q;
            this.$route = $route;
            this.$sessionStorage = $sessionStorage;
            this.homeService = homeService;
            this.isDownLoad = false;
            this.downloadTxt = '';
            $scope.vm = this;
            $scope.loginData = {
                userName: "",
                password: "",
                companyId: "",
                useRefreshTokens: false,
                email: "",
                enterpriseid: "",
                Validator: Validation.Validator.For(Validation.DefaultValidationRules.LoginData)
            };
            this.$scope.loginData.enterpriseid = "PNGAF";
            if (this.localStorageService.get('authorizationData') != undefined) {
                this.$location.url("/trackandtrace");
            }
            //  if auth equal true redirect welcome.
        }
        LoginController.prototype.Reset = function () {
            this.$scope.loginData.password = "";
            angular.element(document.querySelector('#hide_txtpass')).val("");
        };
        LoginController.prototype.login = function () {
            var _this = this;
            this.localStorageService.remove("RecentNumber");
            this.authService.Login(this.$scope.loginData)
                .then(function (response) {
                if (!response.error) {
                    var authData = _this.localStorageService.get('authorizationData');
                    _this.$location.url("/trackandtrace");
                    setTimeout(function () {
                        _this.$rootScope.$broadcast("UserInfo");
                    }, 100);
                    _this.$route.reload();
                }
                else {
                    _this.Reset();
                    _this.$scope.messageError = "Username or password is incorrect.";
                }
            }, (function (error) {
                // TODO: Disable autocomplete for password input
                //this.Reset();
                _this.$scope.messageError = "Username or password is incorrect.";
                //this.errorHandlerService.HandleException(error, "login", this.$scope.loginData.userName);
            }));
        };
        LoginController.$inject = ["$scope", "$rootScope", "$location", "authService", "configSettings", "errorHandlerService",
            "localStorageService", "dialogHelperService", "clientConnectionService", "$q", "$route", "$sessionStorage", "homeService"];
        return LoginController;
    }());
    Login.LoginController = LoginController;
    Main.App.Controllers.controller("loginController", LoginController);
})(Login || (Login = {}));
//# sourceMappingURL=loginController.js.map