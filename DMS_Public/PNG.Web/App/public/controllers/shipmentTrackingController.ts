﻿module ShipmentTracking {
    export interface IShipmentTrackingControllerScope extends ng.IScope {
        vm: ShipmentTrackingController;
        authentication: any;
        SearchParameterReport: Interfaces.ISearchQueryParameter;
        searchParmeter: Interfaces.ISearchShipmentTrackingReport;
        userDetail: Interfaces.IserachShipmentTrackingUserDetail;
        paramShipment: Interfaces.ISearchShipmentTrackingParam;
        monthList: Interfaces.ISearchShipmentTrackingGetCodeValue[];
        bookingTypeList: Interfaces.ISearchShipmentTrackingGetCodeValue[];
        payerTypeList: Interfaces.ISearchShipmentTrackingGetCodeValue[];
        allRoles: Interfaces.ISearchShipmentTrackingAllRoles[];
        pathCode: Interfaces.ISearchShipmentTrackingPathCodeQuery[];
        dsOrigin: Interfaces.ISearchShipmentTrackingPathCodeQuery[];
        dsDestination: Interfaces.ISearchShipmentTrackingPathCodeQuery[];
        //chkMatchStatusCode: boolean;
        //chkInvoiceIssue: boolean;
        //chkShowPackage: boolean;
        //chkShipmentClose: boolean;
        txtConsignmentNo: string;
        txtYear: string;
        txtBookingNo: string;
        visibleMonth: boolean;
        visibleDatePicker: boolean;
        txtPayerCode: string;
        txtPayerName: string;
        txtDestinationPostalCode: string;
        txtDestinationPostalDescription: string;
        txtDestinationStateProvince: string;
        txtDestinationStateProvinceDescription: string;
        txtStatusCode: string;
        txtStatusDescription: string;
        txtExceptionCode: string;
        txtExceptionDescription: string;
        errorMessage: string;
        tranDate: any[];
        routeType: any[];
        deliveryPathParam: Interfaces.IserachShipmentTrackingDeliveryPathParam;
        checkUser: boolean;
        chkPackageName: string;
        
    }

    export class ShipmentTrackingController {
        public static $inject = ['$scope', '$location', '$route', 'authService',
            '$cookies', "$q", "$sessionStorage", "localStorageService", "shipmentTrackingService",
            "errorHandlerService", "lovHelperService", "dialogHelperService", "notificationService", "dateTimeHelperService", "$filter"];

        public currentPath: string;
        public IsShowMenu: boolean;
        public selectedMonth: string;
        public selectedBookingType: string;
        public selectedPayerType: string[];
        public selectedPathCode: string;
        public selectedOrigin: string;
        public selectedDestination: string;
        public selectedTranDate: string;
        public selectedRouteType: string;
        public currentYear: number;
        public chkMatchStatusCode: boolean;
        public chkInvoiceIssue: boolean;
        public chkShowPackage: boolean;
        public chkShipmentClose: boolean;

        constructor(private $scope: IShipmentTrackingControllerScope,
            private $location: ng.ILocationService,
            private $route: any,
            private authService: Util.AuthService,
            private $cookies: ng.cookies.ICookiesService,
            private $q: ng.IQService,
            private $sessionStorage: any,
            private localStorageService: any,
            private shipmentTrackingService: ShipmentTracking.ShipmentTrackingService,
            private errorHandlerService: Shared.ErrorHandlerService,
            private lovHelperService: Lov.LovHelperService,
            private dialogHelperService: Shared.DialogHelperService,
            private notificationService: Shared.NotificationService,
            private dateTimeHelperService: Shared.DateTimeHelper,
            private $filter: any
        ) {
            $scope.vm = this;
            //$scope.authentication = authService.authentication;
            this.$scope.authentication = this.localStorageService.get('authorizationData');
            //console.log("this.$scope.authentication", this.$scope.authentication);

            this.currentPath = $location.path();

            var withOutAuth: string[] = [Shared.RouteNames.WelCome, Shared.RouteNames.EmailAccess, Shared.RouteNames.Reset, Shared.RouteNames.Unlock,
                Shared.RouteNames.Request, Shared.RouteNames.ShowMessage, Shared.RouteNames.Register, Shared.RouteNames.Login, Shared.RouteNames.RegisterUnlock];

            this.prepareData();
            this.prepareParam();
            this.initShipmentTracking();
            this.prepareValue();
        }

        public prepareData(): void
        {
            this.$scope.userDetail = {};
            this.$scope.paramShipment = {};
            this.$scope.searchParmeter = {};
            this.$scope.monthList = [];
            this.$scope.bookingTypeList = [];
            this.$scope.payerTypeList = [];
            this.$scope.allRoles = [];
            this.$scope.pathCode = [];
            this.$scope.dsOrigin = [];
            this.$scope.dsDestination = [];
            this.$scope.tranDate = [];
            this.$scope.routeType = [];
            this.$scope.deliveryPathParam = {};
            this.$scope.txtConsignmentNo = null;
            this.$scope.txtYear = null;
            this.$scope.txtBookingNo = null;
            this.$scope.txtPayerCode = null;
            this.$scope.errorMessage = null;
            this.$scope.chkPackageName = null;
            this.chkMatchStatusCode = false;
            this.chkInvoiceIssue = false;
            this.chkShowPackage = false;
            this.chkShipmentClose = false;
            this.initOnDialog();
            this.currentYear = new Date().getFullYear();
        }

        public prepareParam(): void
        {
            this.$scope.paramShipment.userid = this.$scope.authentication.userName;
        }

        public prepareValue(): void
        {
            this.$scope.tranDate =
                [
                    {
                        "value": "B",
                        "name": "Booking Date"
                    },
                    {
                        "value": "P",
                        "name": "Pickup Date"
                    },
                    {
                        "value": "S",
                        "name": "Status Date"
                    }
                ];
            this.selectedTranDate = "B";

            this.$scope.routeType =
                [
                    {
                        "value": "L",
                        "name": "Linehaul"
                    },
                    {
                        "value": "S",
                        "name": "Delivery/CL"
                    },
                    {
                        "value": "A",
                        "name": "Air Route"
                    } 
                ];
            this.selectedRouteType = "L";
        }


        public initShipmentTracking(): void
        {
            this.getUserDetail().then(() =>
            {
                if (this.$scope.userDetail.User_Type != "C")
                {
                    this.$scope.checkUser = false;
                }
                else
                {
                    this.$scope.checkUser = true;
                }

                this.getMonth().then(() =>
                {
                    this.getBookingType().then(() =>
                    {
                        this.getPayerType().then(() =>
                        {
                            this.getAllRoles().then(() =>
                            {
                                this.getPathCodeQuery().then(() =>
                                {
                                    this.getDistributionCenterQueryOrigin().then(() =>
                                    {
                                        if (this.CheckUserRoleLocation())
                                        {
                                            this.getDistributionCenterQueryDestination();
                                        }
                                        else
                                        {
                                            this.getDistributionCenterQueryWithLocation();
                                        }  
                                    });                                        
                                });                                  
                            });
                        });
                    });
                });
            });
        }

        public getUserDetail(): ng.IPromise<void>
        {
            return this.shipmentTrackingService.GetUserDetail(this.$scope.authentication.userName).then((result: Interfaces.IserachShipmentTrackingUserDetail) =>
            {
                if (result)
                {
                    this.$scope.userDetail = result;
                    //console.log("this.$scope.userDetail", this.$scope.userDetail);
                    this.$scope.paramShipment.user_culture = this.$scope.userDetail.user_culture;
                }

            }, (error) =>
            {
                this.errorHandlerService.HandleException(error, "");
            });
        }

        public getMonth(): ng.IPromise<void>
        {

            this.$scope.paramShipment.codeid = "months";

            return this.shipmentTrackingService.GetCodevalue(this.$scope.paramShipment).then((result: Interfaces.ISearchShipmentTrackingGetCodeValue[]) => {
                if (result != null && result.length > 0)
                {
                    this.$scope.monthList = result;
                    this.localStorageService.set("localMonthList", this.$scope.monthList);
                    //console.log("this.$scope.monthList", this.$scope.monthList);
                }

            }, (error) =>
                {
                    this.errorHandlerService.HandleException(error, "");
                });
        }


        public getBookingType(): ng.IPromise<void> {

            this.$scope.paramShipment.codeid = "booking_type";

            return this.shipmentTrackingService.GetCodevalue(this.$scope.paramShipment).then((result: Interfaces.ISearchShipmentTrackingGetCodeValue[]) => {
                if (result != null && result.length > 0)
                {
                    this.$scope.bookingTypeList = result;
                    //console.log("this.$scope.bookingTypeList", this.$scope.bookingTypeList);
                }

            }, (error) => {
                    this.errorHandlerService.HandleException(error, "");
                });
        }

        public getPayerType(): ng.IPromise<void> {

            this.$scope.paramShipment.codeid = "customer_type";

            return this.shipmentTrackingService.GetCodevalue(this.$scope.paramShipment).then((result: Interfaces.ISearchShipmentTrackingGetCodeValue[]) => {
                if (result != null && result.length > 0)
                {
                    this.$scope.payerTypeList = result;
                    //console.log("this.$scope.payerTypeList", this.$scope.payerTypeList);
                }

            }, (error) => {
                this.errorHandlerService.HandleException(error, "");
            });
        }

        public getAllRoles(): ng.IPromise<void> {           

            return this.shipmentTrackingService.GetAllRoles(this.$scope.authentication.userName).then((result: Interfaces.ISearchShipmentTrackingAllRoles[]) => {
                if (result != null && result.length > 0) {
                    this.$scope.allRoles = result;
                    //console.log("this.$scope.allRoles", this.$scope.allRoles);
                }

            }, (error) => {
                this.errorHandlerService.HandleException(error, "");
            });
        }

        public getPathCodeQuery(): ng.IPromise<void> {

            return this.shipmentTrackingService.GetPathCodeQuery().then((result: Interfaces.ISearchShipmentTrackingPathCodeQuery[]) => {
                if (result != null && result.length > 0) {
                    this.$scope.pathCode = result;
                    //console.log("this.$scope.pathCode", this.$scope.pathCode);
                }

            }, (error) => {
                this.errorHandlerService.HandleException(error, "");
            });
        }

        public getDistributionCenterQueryOrigin(): ng.IPromise<void> {

            return this.shipmentTrackingService.GetDistributionCenterQuery().then((result: Interfaces.ISearchShipmentTrackingPathCodeQuery[]) => {
                if (result != null && result.length > 0) {
                    this.$scope.dsOrigin = result;
                    //console.log("this.$scope.dsOrigin", this.$scope.dsOrigin);
                }

            }, (error) => {
                this.errorHandlerService.HandleException(error, "");
            });
        }

        public getDistributionCenterQueryDestination(): ng.IPromise<void> {

            return this.shipmentTrackingService.GetDistributionCenterQuery().then((result: Interfaces.ISearchShipmentTrackingPathCodeQuery[]) => {
                if (result != null && result.length > 0) {
                    this.$scope.dsDestination = result;
                    //console.log("this.$scope.dsDestination", this.$scope.dsDestination);
                }

            }, (error) => {
                this.errorHandlerService.HandleException(error, "");
            });
        }

        public getDistributionCenterQueryWithLocation(): ng.IPromise<void> {

            return this.shipmentTrackingService.GetDistributionCenterQueryWithLocation(this.$scope.userDetail.Location).then((result: Interfaces.ISearchShipmentTrackingPathCodeQuery[]) => {
                if (result != null && result.length > 0) {
                    this.$scope.dsDestination = result;
                    //console.log("this.$scope.dsDestination", this.$scope.dsDestination);
                }

            }, (error) => {
                this.errorHandlerService.HandleException(error, "");
            });
        }

        public CheckUserRoleLocation(): boolean
        {
            var result: boolean = false;
            for (var i in this.$scope.allRoles)
            {
                if (this.$scope.allRoles[i].role_name == "OPSSU")
                {
                    result = true;
                    break;
                }
                else if (this.$scope.allRoles[i].role_name == "CSSU")
                {
                    result = true;
                    break;
                }                
            }
            //console.log("result", result);
            return result;
        }

        //public checkMatchStatusCOde(statusCode: boolean): void
        //{
        //    console.log("statusCode", statusCode);
        //    console.log("this.$scope.chkMatchStatusCode", this.$scope.chkMatchStatusCode);
        //}

        public loadDeliveryPath(): ng.IPromise<void>
        {
            if (this.selectedPathCode != null && this.selectedPathCode != "")
            {
                this.$scope.deliveryPathParam.path_code = this.selectedPathCode;
                return this.shipmentTrackingService.GetDeliveryPath(this.$scope.deliveryPathParam).then((result: Interfaces.IserachShipmentTrackingDeliveryPath) =>
                {
                    if (result)
                    {
                        this.$scope.searchParmeter.delPathOriginDC = result.origin_station;
                        this.$scope.searchParmeter.delPathDestinationDC = result.destination_station;
                    }
                },
                (error) => {
                    this.errorHandlerService.HandleException(error, "GetDeliveryPath");
                });
            }
            
        }

        public getDate(date: Date): Date
        {
            if (typeof date === "Date") {
                if (date.getFullYear() > 2500)
                    date = new Date(date.getFullYear() - 543, date.getMonth(), date.getDate());
                else date = new Date(date.getFullYear(), date.getMonth(), date.getDate());
            }
            else if (typeof date === "string") {
                var arr: string[] = date.toString().split("/");
                if (parseInt(arr[2]) > 2500)
                    date = new Date(Date.UTC(parseInt(arr[2]) - 543, parseInt(arr[1]) - 1, parseInt(arr[0])));
                else date = new Date(Date.UTC(parseInt(arr[2]), parseInt(arr[1]) - 1, parseInt(arr[0])));
            }
            return date;
        }

        public checkDate(): void
        {
            
            if (this.$scope.searchParmeter.PeriodFrom || this.$scope.searchParmeter.PeriodTo) {
                this.$scope.visibleMonth = true;
                this.selectedMonth = null;
                this.$scope.txtYear = null;
                this.$scope.monthList = [];
              

                //this.getMonth();
                if (this.localStorageService.get("localMonthList") != null) {
                    this.$scope.monthList = this.localStorageService.get("localMonthList");
                }
            }
            else {
                this.$scope.visibleMonth = false;
            }  
            
            

            if (this.selectedMonth || this.$scope.txtYear)
            {
                this.$scope.visibleDatePicker = true;
            }
            else
            {
                this.$scope.visibleDatePicker = false;
            }
        }

        public validate(): boolean
        {
            var check: boolean = false;
            if (!this.$scope.searchParmeter.PeriodFrom && !this.$scope.searchParmeter.PeriodTo && (this.selectedMonth == null || this.selectedMonth == ""))
            {
                this.$scope.errorMessage = "Please enter a date range";
                check = true;
            }
            else if (!this.$scope.searchParmeter.PeriodFrom && this.$scope.searchParmeter.PeriodTo)
            {
                this.$scope.errorMessage = "Please enter Start Date";
                check = true;
            }
            else if (this.$scope.searchParmeter.PeriodFrom && !this.$scope.searchParmeter.PeriodTo)
            {
                this.$scope.errorMessage = "Please enter End Date";
                check = true;
            }
            else if (this.selectedMonth && (this.$scope.txtYear == null || this.$scope.txtYear == ""))
            {
                this.$scope.errorMessage = "Month and Year are required. Please enter values.";
                check = true;
            }
            else if (!this.selectedMonth && (this.$scope.txtYear != null && this.$scope.txtYear != ""))
            {
                this.$scope.errorMessage = "Month and Year are required. Please enter values.";
                check = true;
            }
            else if (this.$scope.searchParmeter.PeriodTo < this.$scope.searchParmeter.PeriodFrom)
            {
                this.$scope.errorMessage = "To Date should be greater than or equal to From Date.";
                check = true;
            }
            else
            {
                this.$scope.errorMessage = null;
                check = false;
            }
            
            if (this.$scope.errorMessage != null)
            {
                this.notifyMessagePopupErrMessage(this.$scope.errorMessage, true);
            }
            return check;
        }

        private notifyMessagePopupErrMessage(msg: string, isError: boolean): void {
            if (isError) {
                this.dialogHelperService.ShowMessage("Warning", msg);
            }
            else {
                this.notificationService.Success(msg);
            }
        }


        public clearText(): void
        {
            if (this.$scope.txtPayerCode == null || this.$scope.txtPayerCode == "")
            {
                this.$scope.txtPayerName = null;
            }

            if (this.$scope.txtDestinationPostalCode == null || this.$scope.txtDestinationPostalCode == "")
            {
                this.$scope.txtDestinationPostalDescription = null;
            }

            if (this.$scope.txtDestinationStateProvince == null || this.$scope.txtDestinationStateProvince == "")
            {
                this.$scope.txtDestinationStateProvinceDescription = null;
            }

            if (this.$scope.txtStatusCode == null || this.$scope.txtStatusCode == "")
            {
                this.$scope.txtStatusDescription = null;
            }

            if (this.$scope.txtExceptionCode == null || this.$scope.txtExceptionCode == "")
            {
                this.$scope.txtExceptionDescription = null;
            }
        }

        //public getSelectedValues(): void {
        //    var selectedVal = $("#multiselect").val();
        //    for (var i = 0; i < selectedVal.length; i++) {
        //        this.innerFunc(i);
        //    }
        //}
        //public innerFunc(i: number): void {
        //    var selectedVal = $("#multiselect").val();
        //    setTimeout(function () {
        //        location.href = selectedVal[i];
        //    }, i * 5000);
        //}

        public GenerateReport(): void {
            //console.log("this.selectedMonth", this.selectedMonth);
            //console.log("this.$scope.searchParmeter.PeriodFrom", this.$scope.searchParmeter.PeriodFrom);
            //console.log("this.$scope.searchParmeter.PeriodTo", this.$scope.searchParmeter.PeriodTo);
            //console.log("this.selectedBookingType", this.selectedBookingType);
            //console.log("this.selectedPayerType", this.selectedPayerType);
            //console.log("this.selectedTranDate", this.selectedTranDate);
            //console.log("this.selectedRouteType", this.selectedRouteType);
            //console.log("this.selectedPathCode", this.selectedPathCode);
            //console.log("this.selectedOrigin", this.selectedOrigin);
            //console.log("this.selectedDestination", this.selectedDestination);
            //console.log("this.chkInvoiceIssue", this.$scope.chkInvoiceIssue);
            //console.log("this.chkMatchStatusCode", this.$scope.chkMatchStatusCode);
            //console.log("this.chkShowPackage", this.$scope.chkShowPackage);
            //console.log("txtYear", this.$scope.txtYear);
            //console.log("this.chkShipmentClose", this.$scope.chkShipmentClose);
            var result: boolean = false;
            result = this.validate();
            if (result != true)
            {
                if (this.selectedRouteType == "L" || this.selectedRouteType == "A") {
                    this.loadDeliveryPath();
                    //console.log("searchParmeter", this.$scope.searchParmeter);
                }
                else
                {
                    this.$scope.searchParmeter.delPathOriginDC = "";
                    this.$scope.searchParmeter.delPathDestinationDC = "";
                }
                this.prapereSearchParam();
                //console.log("this.$scope.searchParmeter", this.$scope.searchParmeter);
                this.loadReport();
            }
            else
            {
                
            }
            
            
            //var result: Interfaces.ISearchFilterView[] = [];
            //var condition: Interfaces.ISearchFilterView;

            //this.shipmentTrackingService.GetReport(this.$scope.searchParmeter)
            //    .then(result => {
            //        if (result) {
            //            var blob = Shared.FileHelper.Base64ToBlob(result[0].Base64StringData, result[0].ContentType);
            //            saveAs(blob, result[0].FileName);
            //        }
            //    }, (error) => {
            //        this.errorHandlerService.HandleException(error, "");
            //    });
        }

        public loadReport(): ng.IPromise<void>
        {
            var result: Interfaces.ISearchFilterView[] = [];
            var condition: Interfaces.ISearchFilterView;
            //console.log("IN load this.$scope.searchParmeter", this.$scope.searchParmeter);          

            return this.shipmentTrackingService.GetReport(this.$scope.searchParmeter)
                .then(result => {
                    if (result) {
                        var blob = Shared.FileHelper.Base64ToBlob(result[0].Base64StringData, result[0].ContentType);
                        saveAs(blob, result[0].FileName);
                    }
                }, (error) => {
                    this.errorHandlerService.HandleException(error, "");
                });
        }

        public prapereSearchParam(): void
        {
            this.$scope.searchParmeter.userId = this.$scope.authentication.userName;

            if (this.selectedMonth)
            {
                this.$scope.searchParmeter.month = this.selectedMonth;
                if (this.$scope.txtYear)
                {
                    this.$scope.searchParmeter.year = this.$scope.txtYear.toString().trim();
                }
               
            }
            //console.log("this.$scope.searchParmeter.PeriodFrom", this.$scope.searchParmeter.PeriodFrom);
            //console.log("this.$scope.searchParmeter.PeriodTo", this.$scope.searchParmeter.PeriodTo);
        

            if (this.$scope.searchParmeter.PeriodFrom || this.$scope.searchParmeter.PeriodTo)
            {
                var startDate = this.$scope.searchParmeter.PeriodFrom != null ? this.$scope.searchParmeter.PeriodFrom : this.$scope.searchParmeter.PeriodTo;
                var endDate = this.$scope.searchParmeter.PeriodTo != null ? this.$scope.searchParmeter.PeriodTo : startDate;
                var stDate: Date = this.getDate(startDate);
                var enDate: Date = this.getDate(endDate);
                var dateStart: string = this.dateTimeHelperService.GetStartTimeOfDate(stDate);
                var dateEnd: string = this.dateTimeHelperService.GetEndTimeOfDate(enDate);
                var sDate: Date = this.$filter("date")(dateStart, "dd/MM/yyyy HH:mm");
                var eDate: Date = this.$filter("date")(dateEnd, "dd/MM/yyyy HH:mm");
                this.$scope.searchParmeter.PeriodFromFormat = sDate;
                this.$scope.searchParmeter.PeriodToFormat = eDate;
                //console.log(this.$scope.searchParmeter.PeriodFromFormat);
                //console.log(this.$scope.searchParmeter.PeriodToFormat);
            }



            //if (this.$scope.searchParmeter.PeriodFrom && this.$scope.searchParmeter.PeriodTo) {

                
            //    var stDate: Date = this.getDate(this.$scope.searchParmeter.PeriodFrom);
            //    var enDate: Date = this.getDate(this.$scope.searchParmeter.PeriodTo);
            //    var dateStart: string = this.dateTimeHelperService.GetStartTimeOfDate(stDate);
            //    var dateEnd: string = this.dateTimeHelperService.GetEndTimeOfDate(enDate);
            //    var sDate: Date = this.$filter("date")(dateStart, "dd/MM/yyyy HH:mm");
            //    var eDate: Date = this.$filter("date")(dateEnd, "dd/MM/yyyy HH:mm");
            //    this.$scope.searchParmeter.PeriodFromFormat = sDate;
            //    this.$scope.searchParmeter.PeriodToFormat = eDate;
            //    console.log(this.$scope.searchParmeter.PeriodFromFormat);
            //    console.log(this.$scope.searchParmeter.PeriodToFormat);
            //}
            //else
            //{
            //    if (this.$scope.searchParmeter.PeriodFrom)
            //    {
            //        var stDate: Date = this.getDate(this.$scope.searchParmeter.PeriodFrom);
            //        var enDate: Date = this.getDate(this.$scope.searchParmeter.PeriodFrom);
            //        var dateStart: string = this.dateTimeHelperService.GetStartTimeOfDate(stDate);
            //        var dateEnd: string = this.dateTimeHelperService.GetEndTimeOfDate(enDate);
            //        var sDate: Date = this.$filter("date")(dateStart, "dd/MM/yyyy HH:mm");
            //        var eDate: Date = this.$filter("date")(dateEnd, "dd/MM/yyyy HH:mm");
            //        this.$scope.searchParmeter.PeriodFromFormat = sDate;
            //        this.$scope.searchParmeter.PeriodToFormat = eDate;
            //        console.log(this.$scope.searchParmeter.PeriodFromFormat);
            //        console.log(this.$scope.searchParmeter.PeriodToFormat);
            //    }
            //    else
            //    {
            //        var stDate: Date = this.getDate(this.$scope.searchParmeter.PeriodTo);
            //        var enDate: Date = this.getDate(this.$scope.searchParmeter.PeriodTo);
            //        var dateStart: string = this.dateTimeHelperService.GetStartTimeOfDate(stDate);
            //        var dateEnd: string = this.dateTimeHelperService.GetEndTimeOfDate(enDate);
            //        var sDate: Date = this.$filter("date")(dateStart, "dd/MM/yyyy HH:mm");
            //        var eDate: Date = this.$filter("date")(dateEnd, "dd/MM/yyyy HH:mm");
            //        this.$scope.searchParmeter.PeriodFromFormat = sDate;
            //        this.$scope.searchParmeter.PeriodToFormat = eDate;
            //        console.log(this.$scope.searchParmeter.PeriodFromFormat);
            //        console.log(this.$scope.searchParmeter.PeriodToFormat);
            //    }
            //}


            if (this.selectedTranDate)
            {
                this.$scope.searchParmeter.tran_date = this.selectedTranDate;
            }
            if (this.$scope.userDetail.User_Type == "C")
            {
                this.$scope.searchParmeter.payer_code = this.$scope.userDetail.payerid;
            }
            else
            {
                if (this.$scope.txtPayerCode)
                {
                    this.$scope.searchParmeter.payer_code = this.$scope.txtPayerCode.trim();
                }                
            }
            if (this.selectedPayerType && this.selectedPayerType.length > 0)
            {
                this.$scope.searchParmeter.payer_type = this.selectedPayerType;
            }

            if (this.$scope.userDetail.User_Type != "C")
            {
                if (this.selectedRouteType)
                {
                    this.$scope.searchParmeter.route_type = this.selectedRouteType;
                }
            }
            else
            {
                if (this.selectedRouteType)
                {
                    this.$scope.searchParmeter.route_type = this.selectedRouteType;
                }
                if (this.selectedPathCode)
                {
                    this.$scope.searchParmeter.route_code = this.selectedPathCode;
                }
                if (this.selectedOrigin)
                {
                    this.$scope.searchParmeter.origin_dc = this.selectedOrigin;
                }
                if (this.selectedDestination)
                {
                    this.$scope.searchParmeter.destination_dc = this.selectedDestination;
                }
            }


           
            
            if (this.$scope.txtDestinationPostalCode)
            {
                this.$scope.searchParmeter.zip_code = this.$scope.txtDestinationPostalCode.trim();
            }
            if (this.$scope.txtDestinationStateProvince)
            {
                this.$scope.searchParmeter.state_code = this.$scope.txtDestinationStateProvince.trim();
            }
            if (this.selectedBookingType)
            {
                this.$scope.searchParmeter.booking_type = this.selectedBookingType;
            }
            if (this.$scope.txtConsignmentNo)
            {
                this.$scope.searchParmeter.consignment_no = this.$scope.txtConsignmentNo.trim();
            }
            if (this.$scope.txtBookingNo)
            {
                this.$scope.searchParmeter.booking_no = this.$scope.txtBookingNo.trim();
            }
            if (this.$scope.txtStatusCode)
            {
                this.$scope.searchParmeter.status_code = this.$scope.txtStatusCode.trim();
            }
            if (this.$scope.txtExceptionCode)
            {
                this.$scope.searchParmeter.exception_code = this.$scope.txtExceptionCode.trim();
            }
            
            if (this.chkShipmentClose == true)
            {
                this.$scope.searchParmeter.shipment_closed = "Y";
            }
            else
            {
                this.$scope.searchParmeter.shipment_closed = "N";
            }

            if (this.chkInvoiceIssue == true)
            {
                this.$scope.searchParmeter.shipment_invoiced = "Y";
            }
            else
            {
                this.$scope.searchParmeter.shipment_invoiced = "N";
            }

            if (this.chkMatchStatusCode == true)
            {
                
                if (this.$scope.txtStatusCode == "" || this.$scope.txtStatusCode == null)
                {
                    this.$scope.searchParmeter.track_all = "N";

                    if (this.chkShowPackage == true)
                    {
                        this.$scope.chkPackageName = "ShipmentTrackingQueryRpt";
                    }
                    else
                    {                       
                        this.$scope.chkPackageName = "ShipmentTrackingQueryRpt_NoDT";
                    }
                }
                else
                {
                    this.$scope.searchParmeter.track_all = "Y";

                    if (this.chkShowPackage == true)
                    {
                        this.$scope.chkPackageName = "ShipmentTrackingQueryRptAll";
                    }
                    else
                    {
                        this.$scope.chkPackageName = "ShipmentTrackingQueryRptAll_NoDT";
                    }
                }

                
            }
            else
            {
                this.$scope.searchParmeter.track_all = "N";

                if (this.chkShowPackage == true)
                {
                    this.$scope.chkPackageName = "ShipmentTrackingQueryRpt";
                }
                else
                {
                    this.$scope.chkPackageName = "ShipmentTrackingQueryRpt_NoDT";
                }
            }

            if (this.$scope.userDetail.User_Type != "C")
            {
                this.$scope.searchParmeter.RptType = "N";
            }
            else
            {
                this.$scope.searchParmeter.RptType = "C";
            }

            //set report name
            if (this.$scope.chkPackageName.trim() == "ShipmentTrackingQueryRpt")
            {
                if (this.$scope.searchParmeter.RptType.trim() == "C")
                {
                    this.$scope.searchParmeter.reportName = "ShipmentTrackingReport3PA.rpt";
                }
                else
                {
                    this.$scope.searchParmeter.reportName = "ShipmentTrackingReport.rpt";
                }
            }
            else if (this.$scope.chkPackageName.trim() == "ShipmentTrackingQueryRptAll")
            {
                if (this.$scope.searchParmeter.RptType.trim() == "C")
                {
                    this.$scope.searchParmeter.reportName = "ShipmentTrackingReport3PA.rpt";
                }
                else
                {
                    this.$scope.searchParmeter.reportName = "ShipmentTrackingReportAll_DT.rpt";
                }
            }
            else if (this.$scope.chkPackageName.trim() == "ShipmentTrackingQueryRpt_NoDT")
            {
                if (this.$scope.searchParmeter.RptType.trim() == "C")
                {
                    this.$scope.searchParmeter.reportName = "ShipmentTrackingReport3PA.rpt";
                }
                else
                {
                    this.$scope.searchParmeter.reportName = "ShipmentTrackingReport_NoDT.rpt";
                }
            }
            else if (this.$scope.chkPackageName.trim() == "ShipmentTrackingQueryRptAll_NoDT")
            {
                if (this.$scope.searchParmeter.RptType.trim() == "C")
                {
                    this.$scope.searchParmeter.reportName = "ShipmentTrackingReport3PA.rpt";
                }
                else
                {
                    this.$scope.searchParmeter.reportName = "ShipmentTrackingReportAll_NoDT.rpt";
                }
            }
           

        }


        private initOnDialog(): void {           

            // DIALOG: PayerCodePopup
            this.$scope.$on("PayerCodePopup", (event, ...data: any[]) => {
                //this.$scope.bindRecipientDetail.lovZipCode = [];
                var selectedItem: Interfaces.IPayerCode = data[0][1]
                this.$scope.txtPayerCode = selectedItem.custid;
                this.$scope.txtPayerName = selectedItem.cust_name;
                //this.$scope.bindRecipientDetail.cost_centre = selectedItem.cost_centre;
                //this.$scope.bindRecipientDetail.reference_name = selectedItem.reference_name;
                //this.getBindRecipientDetailByCostCentre(this.$scope.bindRecipientDetail.cost_centre);
               // this.setSessionStorage();

                // Hide Dialog
                var dialog: any = angular.element(document.querySelector("#" + "PayerCodePopup"));
                dialog.modal("hide");
                this.lovHelperService.ClearLovParameter();
            });
            // DIALOG: StateCodePopup
            this.$scope.$on("StateCodePopup", (event, ...data: any[]) => {
                var selectedItem: Interfaces.IStateCode = data[0][1]
                this.$scope.txtDestinationStateProvince = selectedItem.state_code;
                this.$scope.txtDestinationStateProvinceDescription = selectedItem.state_name;

                // Hide Dialog
                var dialog: any = angular.element(document.querySelector("#" + "StateCodePopup"));
                dialog.modal("hide");
                this.lovHelperService.ClearLovParameter();
            });

            // DIALOG: ZipCodePopup
            this.$scope.$on("ZipCodePopupSender", (event, ...data: any[]) => {
                var selectedItem: Interfaces.IZipCode = data[0][1]
                this.$scope.txtDestinationPostalCode = selectedItem.zipcode;
                this.$scope.txtDestinationPostalDescription = selectedItem.state_name;

                // Hide Dialog
                var dialog: any = angular.element(document.querySelector("#" + "ZipCodePopupSender"));
                dialog.modal("hide");
                this.lovHelperService.ClearLovParameter();
            });

            // DIALOG: StatusCodePopup
            this.$scope.$on("StatusCodePopup", (event, ...data: any[]) => {
                var selectedItem: Interfaces.IStatusCode = data[0][1]
                this.$scope.txtStatusCode = selectedItem.status_code;
                this.$scope.txtStatusDescription = selectedItem.status_description;

                // Hide Dialog
                var dialog: any = angular.element(document.querySelector("#" + "StatusCodePopup"));
                dialog.modal("hide");
                this.lovHelperService.ClearLovParameter();
            });

            // DIALOG: ExceptionCodePopup
            this.$scope.$on("ExceptionCodePopup", (event, ...data: any[]) => {
                var selectedItem: Interfaces.IExceptionCode = data[0][1]
                this.$scope.txtExceptionCode = selectedItem.exception_code;
                this.$scope.txtExceptionDescription = selectedItem.exception_description;
                this.$scope.txtStatusCode = selectedItem.status_code;
                this.$scope.txtStatusDescription = selectedItem.status_description;

                // Hide Dialog
                var dialog: any = angular.element(document.querySelector("#" + "ExceptionCodePopup"));
                dialog.modal("hide");
                this.lovHelperService.ClearLovParameter();
            });
        }

        public searchPayerCode(payerCode: string): void
        {
            this.lovHelperService.CreatePayerCodeLov("PayerCodePopup");
            //console.log("this.selectedPayerType", this.selectedPayerType);
            this.lovHelperService.SetLovParameter({
                BroadcastName: "PayerCodePopup",
                SearchFilter: {
                    custid: payerCode ? payerCode : "",
                    cust_name: "" ,
                    zipcode: "",
                    applicationid: "TIES",
                    enterpriseid: "PNGAF",
                    status_active: "Y",
                    payer_type: this.selectedPayerType ? this.selectedPayerType : ""
                },
                IsMultipleSelected: false,
                IsHideSearchStatus: false,
                IsShowLock: false,
                Active: 1 // All:-1, Active:1, Inactive:0
            });

        }


        public searchZipCodeSender(zipcode: string): void {
            this.lovHelperService.CreateShipmentZipCodeLov("ZipCodePopupSender");

            this.lovHelperService.SetLovParameter({
                BroadcastName: "ZipCodePopupSender",
                SearchFilter: {
                    zipcode: zipcode ? zipcode : "",
                    country: ""
                },
                IsMultipleSelected: false,
                IsHideSearchStatus: false,
                IsShowLock: false,
                Active: 1 // All:-1, Active:1, Inactive:0
            });

        }

        public searchStateCode(stateCode: string): void {
            this.lovHelperService.CreateStateCodeLov("StateCodePopup");

            this.lovHelperService.SetLovParameter({
                BroadcastName: "StateCodePopup",
                SearchFilter: {
                    applicationid: "TIES",
                    enterpriseid: "PNGAF",
                    state_code: stateCode ? stateCode : "",
                    country: "",
                },
                IsMultipleSelected: false,
                IsHideSearchStatus: false,
                IsShowLock: false,
                Active: 1 // All:-1, Active:1, Inactive:0
            });

        }

        public searchStatusCode(statusCode: string): void {
            this.lovHelperService.CreateStatusCodeLov("StatusCodePopup");

            this.lovHelperService.SetLovParameter({
                BroadcastName: "StatusCodePopup",
                SearchFilter: {
                    applicationid: "TIES",
                    enterpriseid: "PNGAF",
                    status_code: statusCode ? statusCode : "",
                    counstatus_descriptiontry: "",
                    culture: this.$scope.userDetail.user_culture,
                    userid: this.$scope.userDetail.userid,
                    allowRoles: this.$scope.userDetail.User_Type
                },
                IsMultipleSelected: false,
                IsHideSearchStatus: false,
                IsShowLock: false,
                Active: 1 // All:-1, Active:1, Inactive:0
            });

        }

        public searchExceptionCode(exceptionCode: string): void {
            this.lovHelperService.CreateExceptionCodeLov("ExceptionCodePopup");

            this.lovHelperService.SetLovParameter({
                BroadcastName: "ExceptionCodePopup",
                SearchFilter: {
                    exception_code: exceptionCode ? exceptionCode : "",
                    status_code: this.$scope.txtStatusCode ? this.$scope.txtStatusCode : "",
                    culture: this.$scope.userDetail.user_culture,
                    userid: this.$scope.userDetail.userid
                },
                IsMultipleSelected: false,
                IsHideSearchStatus: false,
                IsShowLock: false,
                Active: 1 // All:-1, Active:1, Inactive:0
            });

        }

       

        


        public Reset() {
            this.$location.url(this.currentPath);
            this.$route.reload();
        }


    }

    Main.App.Controllers.controller("shipmentTrackingController", ShipmentTrackingController);
}