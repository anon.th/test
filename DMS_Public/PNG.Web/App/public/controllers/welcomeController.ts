﻿module WelCome
{
    export interface IWelComeControllerScope extends ng.IScope
    {
        vm: WelComeController;
        authentication: any;
    }

    export class WelComeController
    {
        public static $inject = ['$scope', '$location', 'authService',
            '$cookies', "$q", "$sessionStorage"];

        public currentPath: string;
        public IsShowMenu: boolean;

        constructor(private $scope: IWelComeControllerScope,
            private $location: ng.ILocationService,
            private authService: Util.AuthService,
            private $cookies: ng.cookies.ICookiesService,
            private $q: ng.IQService,
            private $sessionStorage: any)
        {
            $scope.vm = this;
            $scope.authentication = authService.authentication;

            this.currentPath = $location.path();

            var withOutAuth: string[] = [Shared.RouteNames.WelCome, Shared.RouteNames.EmailAccess, Shared.RouteNames.Reset, Shared.RouteNames.Unlock,
                Shared.RouteNames.Request, Shared.RouteNames.ShowMessage, Shared.RouteNames.Register, Shared.RouteNames.Login, Shared.RouteNames.RegisterUnlock];

            //if ($scope.authentication.isAuth == false)
            //{
            //    let checkPath = withOutAuth.filter(f => $location.path() == f).length;
            //    if (checkPath == 0 && this.currentPath.length > 1)
            //    {
            //        $scope.authentication.isAuth = true;
            //    }
            //    else
            //    {
            //        if (!$scope.authentication.isAuth)
            //        {
            //            this.setTimeOut(1000).then((result: boolean) =>
            //            {
            //                if (result)
            //                {
            //                    $location.url("/login");
            //                }
            //            });
            //        }
            //    }
            //} 

            this.initWelcome();
        }

        public initWelcome() 
        {
            console.log("Welcome");

        }

        public setTimeOut(milliSecond: number): ng.IPromise<boolean>
        {
            var result = false;
            var defer = this.$q.defer<boolean>();

            setTimeout(function ()
            {
                result = true;
                defer.resolve(result);
            }, milliSecond);

            return defer.promise;
        }
    }

    Main.App.Controllers.controller("welcomeController", WelComeController);
}