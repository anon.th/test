﻿module SearchConsignment {
    export interface ISearchConsignmentControllerScope extends ng.IScope {
        vm: SearchConsignmentController;
        authentication: any;
        UserInfo: Interfaces.IUserInfo;
        ParentSubMenus: Interfaces.IUserMenu[];
        MenuSub: Interfaces.IUserMenu[];
        Menu: Interfaces.IUserMenu[];
        //MainMenu: Interfaces.IExternalMenu[];
        Roles: Interfaces.IExternalIdentityUserRoles[];
        IsCollapsed: boolean;
        CurrentMenuId: number;
        MenuUrl: string;
        WithOutLoginPage: boolean;
        IsAlert: boolean;
        userCustomerAccount: Interfaces.ICreateConsignmentCustomerAccount;
        enterpriseConfig: Interfaces.ISearchConsignmentGetstatusprintingconfig;
        referenceDS: any[];
        customerIdVisible: boolean;
        serviceCode: Interfaces.ICreateConsignmentServiceCode;
        status: Interfaces.ISearchConsignmentGetCustomStatus[];
        senderNameVisible: boolean;
        searchParam: Interfaces.ISearchConsignmentParam;
        searchParamLocalStorage: Interfaces.ISearchConsignmentParam;
        errorResult: Interfaces.ISearchConsignmentError;
        detailResult: Interfaces.ISearchConsignmentDetail[];
        detailReport: Interfaces.ISearchConsignmentDetail[];
        customerId: string;
        payerId: string;
        payerIdparam: string;
        consignmentNo: string;
        errorText: string;
        showErrorText: boolean;
        SearchParameter: Interfaces.ISearchQueryParameter;
        SelectedPageLink: Interfaces.IPageLink;
        contData: number;
        //masterAWBNo: string;
        //jobEntryNo: string;
        recipientPhoneNo: string;
        RecipientPostalCode: string;
        ReferenceNo: string;
        filter: number;
        errorMessage: string;
        disabledBtnPrint: boolean;
        disabledBtnMainPrint: boolean;
        printConsNoteParam: Interfaces.ISearchConsignmentPrintShippingListParam;
        printShippingListParam: Interfaces.ISearchConsignmentPrintShippingListParam;
        reprintConsNoteParam: Interfaces.ISearchConsignmentReprintConsNoteParam;
        printConsNoteDetailParam: Interfaces.ISearchConsignmentPrintConsNoteDetail[];
        printShippingListDetailParam: Interfaces.ISearchConsignmentPrintShippingListDetail[];
        reprintConsNoteDetailParam: Interfaces.ISearchConsignmentReprintConsNoteDetail[];
        paramConsNote: Interfaces.ISearchConsignmentPrintConsNoteParam;
        paramConsNoteResult: Interfaces.IPrintConsNote;
        paramReprintConsNoteResult: Interfaces.IReprintConsNote;

        

    }


    export class SearchConsignmentController {
        public static $inject = ["$rootScope", "$scope", "$location", "authService",
            "$cookies", "configSettings",
            "localStorageService",
            "$sessionStorage", "clientConnectionService", "$filter", "lovHelperService", "notificationService",
            "dialogHelperService", "searchConsignmentService", "createConsignmentService", "$route", "$window", "$routeParams", "errorHandlerService"];
        private isCheckedAll: boolean = false;
        public WatchEvents: Function;
        public startDate: Date = null;
        public endDate: Date = null;
        private selectedServiceCode: string;
        private selectedSenderName: string;
        private payerCusID: string;
        private selectedStatus: number;
        private masterAWBNo: string;
        private jobEntryNo: string;
        private filter: number;
        private defaultUrlPrefix = "/searchconsignment";

        constructor(private $rootScope: ng.IScope,
            private $scope: ISearchConsignmentControllerScope,
            private $location: ng.ILocationService,
            private authService: Util.AuthService,
            private $cookies: ng.cookies.ICookiesService,
            private configSettings: Interfaces.IConfigSettings,
            private localStorageService: any,
            private $sessionStorage: any,
            private clientConnectionService: Util.ClientConnectionService,
            private $filter: any,
            private lovHelperService: Lov.LovHelperService,
            private notificationService: Shared.NotificationService,
            private dialogHelperService: Shared.DialogHelperService,
            private searchConsignmentService: SearchConsignment.SearchConsignmentService,
            private createConsignmentService: CreateConsignment.CreateConsignmentService,
            private $route: any,
            private $window: any,
            private $routeParams: any,
            private errorHandlerService: Shared.ErrorHandlerService) {

            $scope.vm = this;
            this.$scope.authentication = this.localStorageService.get('authorizationData');
            //console.log(this.$scope.authentication);
            //console.log(this.$scope.authentication.userName);


            this.filter = $routeParams.filter != undefined ? $routeParams.filter : null;
            //console.log("this.filter", this.filter);

            if (this.filter) {
                this.$scope.filter = this.filter;
            }


            this.Init();
            this.InitSearchConsignment();

        }
        public Init(): void
        {
            this.$scope.disabledBtnMainPrint = true;
            this.$sessionStorage.ListItem = null;
            this.$sessionStorage.ListItem = [];
            this.$scope.userCustomerAccount = {};
            this.$scope.enterpriseConfig = [];
            this.$scope.referenceDS = [];
            this.$scope.serviceCode = [];
            this.$scope.status = [];
            this.$scope.searchParam = {};
            this.$scope.searchParamLocalStorage = {};
            this.$scope.errorResult = [];
            this.$scope.detailResult = [];
            this.$scope.detailReport = []
            this.$scope.paramConsNoteResult = {};
            this.$scope.paramReprintConsNoteResult = {};
            this.$scope.printConsNoteParam = {};
            this.$scope.printShippingListParam = {};
            this.$scope.reprintConsNoteParam = {};
            this.$scope.paramConsNote = {};
            this.$scope.printConsNoteDetailParam = [];
            this.$scope.printShippingListDetailParam = [];
            this.$scope.reprintConsNoteDetailParam = [];
            this.$scope.errorMessage = null;
            this.$scope.showErrorText = true;
            this.$scope.consignmentNo = null;
            this.masterAWBNo = null;
            this.jobEntryNo = null;
            this.$scope.recipientPhoneNo = null;
            this.$scope.RecipientPostalCode = null;
            this.$scope.ReferenceNo = null;
            this.$scope.payerIdparam = null;
            this.selectedSenderName = null;
            this.$scope.SearchParameter = [];
            this.$scope.contData = 0;
            this.$scope.SearchParameter.ItemsPerPage = 10;
            this.$scope.SearchParameter.Page = 1;
            this.AddHandlerPageLink();
            
        }

        public InitSearchConsignment(): void
        {
            this.getCustomerAccount().then(() => {
                this.getConsignmentStatusPrintingConfigurations();
                if (this.$scope.userCustomerAccount.IsEnterpriseUser == false) {
                    this.getReferenceDS();
                }
                //IsEnterpriseUser
                if (this.$scope.userCustomerAccount.IsEnterpriseUser) {
                    this.$scope.disabledBtnPrint = true;
                    this.getReferenceDS();
                    this.$scope.senderNameVisible = false;
                    this.$scope.customerIdVisible = true;
                    if (this.$scope.userCustomerAccount.IsCustomsUser) {
                        this.getCustomsWarehousesName();
                    }
                    else {
                        this.getEnterpriseUserAccounts().then(() => {
                            if (this.$scope.customerId != null && this.$scope.customerId != "") {
                                this.getReferenceDS();
                            }
                        });
                    }
                }
                else//IsCustomerUser
                {
                    this.$scope.disabledBtnPrint = false;
                    //this.$scope.senderNameVisible = true;
                    this.$scope.customerIdVisible = false;
                    if (this.$scope.userCustomerAccount.IsCustomerUser == true && this.$scope.userCustomerAccount.IsMasterCustomerUser == true) {

                    }
                    if (this.$scope.userCustomerAccount.IsCustomerUser == true && this.$scope.userCustomerAccount.IsMasterCustomerUser == false) {

                    }
                }

                this.bindControl();
            }).then(
                () =>
                {
                    if (this.$scope.filter != null)
                    {
                        if (this.localStorageService.get("SearchParam") != null)
                        {
                            this.$scope.searchParamLocalStorage = this.localStorageService.get("SearchParam");
                            //console.log(" this.$scope.searchParamLocalStorage", this.$scope.searchParamLocalStorage);
                            this.getSpCSS_ConsignmentStatusByLocalStorage().then(
                                () =>
                                {
                                    if (this.$scope.errorResult.ErrorCode != 0)
                                    {
                                        this.$scope.errorText = this.$scope.errorResult.ErrorMessage;
                                        this.$scope.showErrorText = true;
                                    }
                                    else {
                                        this.$scope.showErrorText = false;
                                    }
                                });
                        }
                    }
                });
        }
        public getCustomerAccount(): ng.IPromise<void>
        {
            return this.createConsignmentService.getCustomerAccount(this.$scope.authentication.userName).then((result: Interfaces.ICreateConsignmentCustomerAccount) =>
            {
                if (result != null && result != "")
                {
                    this.$scope.userCustomerAccount = result;
                    //console.log(" this.$scope.userCustomerAccount", this.$scope.userCustomerAccount);
                }
            }, ((error) => {
                console.error(error);
            }));
        }

        public getConsignmentStatusPrintingConfigurations(): ng.IPromise<void> {
            return this.searchConsignmentService.getStatusPrinting().then((result: Interfaces.ISearchConsignmentGetstatusprintingconfig) => {
                if (result != null && result != "")
                {
                    this.$scope.enterpriseConfig = result;
                   // console.log("this.$scope.enterpriseConfig", this.$scope.enterpriseConfig);
                    this.$scope.SearchParameter.ItemsPerPage = parseInt(this.$scope.enterpriseConfig.value); 
                    //console.log("this.$scope.SearchParameter.ItemsPerPage", this.$scope.SearchParameter.ItemsPerPage);
                }
            }, ((error) => {
                console.error(error);
            }));
        }

        public getReferenceDS(): ng.IPromise<void> {
            return this.searchConsignmentService.getReferenceDS(this.$scope.authentication.userName).then((result: Interfaces.ICreateConsignmentCustomsWarehousesNameDefault[]) => {
                if (result != null && result.length > 1) {
                    this.$scope.senderNameVisible = false;
                    this.$scope.referenceDS = result;
                    if (this.$scope.userCustomerAccount.IsCustomerUser)
                    {
                        this.selectedSenderName = result[0].snd_rec_name;
                    }
                    //console.log(" this.$scope.referenceDS", this.$scope.referenceDS);
                }
                else
                {
                    this.$scope.senderNameVisible = true;
                    this.$scope.referenceDS = result;
                    this.selectedSenderName = result[0].snd_rec_name;
                }
            }, ((error) => {
                console.error(error);
            }));
        }
        public getCustomsWarehousesName(): ng.IPromise<void> {
            return this.searchConsignmentService.getCustomsWarehousesName(this.selectedSenderName).then((result: Interfaces.ICreateConsignmentCustomsWarehousesName[]) => {
                if (result != null && result.length > 0)
                {
                    this.$scope.referenceDS = result;
                    //console.log(" this.$scope.referenceDS", this.$scope.referenceDS);
                }
            }, ((error) => {
                console.error(error);
            }));
        }
        public getEnterpriseUserAccounts(): ng.IPromise<void> {
            return this.searchConsignmentService.getEnterpriseUserAccounts(this.$scope.authentication.userName).then((result: string) => {
                if (result != null && result.length > 0)
                {
                    this.$scope.customerId = result;
                    //console.log(" this.$scope.customerId", this.$scope.customerId);
                    //this.$scope.customerIdVisible = false;
                }
                //else { this.$scope.customerIdVisible = true; }

            }, ((error) => {
              //  console.error(error);
            }));
        }

        public getServiceCode(): ng.IPromise<void> {
            return this.createConsignmentService.getServiceCode().then((result: Interfaces.ICreateConsignmentServiceCode[]) => {
                if (result != null && result.length > 0)
                {
                    this.$scope.serviceCode = result;
                    //console.log(" this.$scope.serviceCode", this.$scope.serviceCode);
                }

            }, ((error) => {
                console.error(error);
            }));
        }
        public bindControl(): void {

            this.getServiceCode().then(() =>
            {
                if (this.$scope.userCustomerAccount.IsCustomsUser)
                {
                    this.getCustomStatus();
                }
                else
                {
                    this.getCustomerStatus();
                }
               
            });
        }
        public getCustomStatus(): ng.IPromise<void> {
            return this.searchConsignmentService.getCustomStatus().then((result: Interfaces.ISearchConsignmentGetCustomStatus[]) => {
                if (result != null && result.length > 0)
                {
                    this.$scope.status = result;
                    //console.log(" this.$scope.status", this.$scope.status);
                }

            }, ((error) => {
                console.error(error);
            }));
        }
        public getCustomerStatus(): ng.IPromise<void> {
            return this.searchConsignmentService.getCustomerStatus().then((result: Interfaces.ISearchConsignmentGetCustomStatus[]) => {
                if (result != null && result.length > 0) {
                    this.$scope.status = result;
                    //console.log(" this.$scope.status", this.$scope.status);
                }

            }, ((error) => {
                console.error(error);
            }));
        }

        public getSpCSS_ConsignmentStatus(): ng.IPromise<void> {
            return this.searchConsignmentService.getSpCSS_ConsignmentStatus(this.$scope.searchParam).then((result: Interfaces.ISearchConsignmentResult) => {
                if (result != null)
                {
                    if (result.SearchConsignmentError.length > 0)
                    {
                        this.$scope.errorResult = result.SearchConsignmentError[0];
                        //console.log("this.$scope.errorResult", this.$scope.errorResult);
                    }
                    if (result.SearchConsignmentDetail.length > 0)
                    {
                        this.$scope.detailResult = result.SearchConsignmentDetail;
                       // console.log("this.$scope.detailResult", this.$scope.detailResult);
                    }

                    this.$scope.contData = result.CountData;                   
                    //console.log("this.$scope.contData", this.$scope.contData);
                }

            }, ((error) => {
                console.error(error);
            }));
        }

        public prepareParam(): void
        {
            if (this.payerCusID != null && this.payerCusID != "")
            {
                this.$scope.searchParam.payerid = this.payerCusID;
            }
            this.$scope.searchParam.userloggedin = this.$scope.authentication.userName;
            //this.$scope.searchParam.payerid = this.$scope.payerIdparam;
            if (this.$scope.consignmentNo != null && this.$scope.consignmentNo != "")
            {
                this.$scope.searchParam.consignment_no = this.$scope.consignmentNo;
            }
            if (this.selectedSenderName != null && this.selectedSenderName != "")
            {
                this.$scope.searchParam.SenderName = this.selectedSenderName;
            }
            if (this.endDate && !this.startDate)
            {
                this.$scope.errorMessage = "Please enter From Date if To Date entered.";
                
            }
            else
            {
                if (this.startDate)
                {
                    var stdate: Date = this.getDate(this.startDate);
                    this.$scope.searchParam.datefrom = stdate;
                }
               
            }
            if (this.startDate && this.endDate)
            {
                if (this.endDate < this.startDate)
                {
                    this.$scope.errorMessage = "To Date should be greater than or equal to From Date.";
                }
                else
                {
                    var CurrentDate: Date = this.$filter("date")(Date.now(), "dd/MM/yyyy");
                    //console.log("CurrentDate", CurrentDate);

                    if (this.endDate > CurrentDate)
                    {
                        this.$scope.errorMessage = "Dates should not be in the future.";
                    }
                    else
                    {
                        var endate: Date = this.getDate(this.endDate);
                        this.$scope.searchParam.dateto = endate;
                        this.$scope.errorMessage = null;
                    }

                    if (this.startDate > CurrentDate)
                    {
                        this.$scope.errorMessage = "Dates should not be in the future.";
                    }
                    else
                    {
                        var stdate: Date = this.getDate(this.startDate);
                        this.$scope.searchParam.datefrom = stdate;
                        this.$scope.errorMessage = null;
                    }
                }
            }
            
            //if (this.endDate)
            //{
            //    var endate: Date = this.getDate(this.endDate);
            //    console.log("endate", endate);
            //    console.log("this.endDate", this.endDate);
            //    this.$scope.searchParam.dateto = endate;
            //}
            if (this.selectedStatus != null)
            {
                this.$scope.searchParam.status_id = this.selectedStatus;
            }
            if (this.$scope.enterpriseConfig.value != null && this.$scope.enterpriseConfig.value != "")
            {
                this.$scope.searchParam.pageCount = parseInt(this.$scope.enterpriseConfig.value);
            }
            else
            {
                this.$scope.searchParam.pageCount = 10;
            }
            if (this.$scope.SearchParameter.Page != null && this.$scope.SearchParameter.Page != 0)
            {
                this.$scope.searchParam.pageNumber = this.$scope.SearchParameter.Page;
            }
            else
            {
                this.$scope.searchParam.pageNumber = 1;
            }
            if (this.selectedServiceCode)
            {
                this.$scope.searchParam.service_code = this.selectedServiceCode;
            }
            if (this.masterAWBNo != null && this.masterAWBNo != "")
            {
                this.$scope.searchParam.MasterAWBNumber = this.masterAWBNo;
            }
            if (this.jobEntryNo != null && this.jobEntryNo != "")
            {
                this.$scope.searchParam.JobEntryNo = this.jobEntryNo;
            }
            if (this.$scope.recipientPhoneNo != null)
            {
                this.$scope.searchParam.recipient_telephone = this.$scope.recipientPhoneNo;
            }
            if (this.$scope.RecipientPostalCode != null)
            {
                this.$scope.searchParam.recipient_zipcode = this.$scope.RecipientPostalCode;
            }
            if (this.$scope.ReferenceNo != null)
            {
                this.$scope.searchParam.ref_no = this.$scope.ReferenceNo;
            }
                       
            this.localStorageService.set("SearchParam", this.$scope.searchParam);
            
        }

        private notifyMessagePopupErrMessage(msg: string, isError: boolean): void
        {
            if (isError) {
                this.dialogHelperService.ShowMessage("Warning", msg);
            }
            else {
                this.notificationService.Success(msg);
            }
        }

        private notifyMessagePopupErrMessageReport(msg: string, isError: boolean): void {
            if (isError) {
                this.dialogHelperService.ShowMessage("Warning", msg);
            }
            else {
                this.notificationService.Success(msg);
            }
        }

        public onClickSearch(): void
        {
            this.$scope.searchParam = {};
            this.prepareParam();
            this.$scope.disabledBtnMainPrint = true;
            this.$sessionStorage.ListItem = null;
            this.$sessionStorage.ListItem = [];
            if (this.$scope.errorMessage != null)
            {
                this.notifyMessagePopupErrMessage(this.$scope.errorMessage, true);
            }
            else
            {
                //console.log(" this.$scope.searchParam", this.$scope.searchParam);
                this.getSpCSS_ConsignmentStatus().then(
                    () => {
                        if (this.$scope.errorResult.ErrorCode != 0) {
                            this.$scope.errorText = this.$scope.errorResult.ErrorMessage;
                            this.$scope.showErrorText = true;
                        }
                        else {
                            this.$scope.showErrorText = false;
                        }
                    });
            }
            //console.log("this.$scope.SearchParameter.ItemsPerPage", this.$scope.SearchParameter.ItemsPerPage);
            
        }

        //public reloadByLocalstorage(): void
        //{
        //    if (this.localStorageService.get("SearchParam") != null)
        //    {
        //        this.$scope.searchParamLocalStorage = this.localStorageService.get("SearchParam");
        //    }
        //}

        public getSpCSS_ConsignmentStatusByLocalStorage(): ng.IPromise<void> {
            return this.searchConsignmentService.getSpCSS_ConsignmentStatus(this.$scope.searchParamLocalStorage).then((result: Interfaces.ISearchConsignmentResult) => {
                if (result != null) {
                    if (result.SearchConsignmentError.length > 0) {
                        this.$scope.errorResult = result.SearchConsignmentError[0];
                        //console.log("this.$scope.errorResult", this.$scope.errorResult);
                    }
                    if (result.SearchConsignmentDetail.length > 0) {
                        this.$scope.detailResult = result.SearchConsignmentDetail;
                        //console.log("this.$scope.detailResult", this.$scope.detailResult);
                    }

                    this.$scope.contData = result.CountData;
                    //console.log("this.$scope.contData", this.$scope.contData);
                }

            }, ((error) => {
                console.error(error);
            }));
        }

        public onClickClear(): void
        {
            this.$scope.disabledBtnMainPrint = true;
            this.$sessionStorage.ListItem = null;
            this.$sessionStorage.ListItem = [];
            this.$location.url("#/searchconsignment");
            //this.$window.location.href = "#/searchconsignment";
            this.$route.reload();
        }
        public getPayerIdParam(cusId: string): void
        {
            //console.log("cusId", cusId);
            if (cusId != null && cusId != "")
            {
                this.$scope.searchParam.payerid = cusId;
            }           
        }

        public updateConsignment(consignmentNo: string): void
        {
            //console.log("consignmentNo =>", consignmentNo);
            //this.$location.url("#/updateconsignment/consignmentno/" + consignmentNo);
            //this.$location.path("#/updateconsignment/consignmentno/" + consignmentNo);
            this.$window.location.href = "#/updateconsignment/consignmentno/" + consignmentNo;
        }

        public getDate(date: Date): Date
        {
            if (typeof date === "Date") {
                if (date.getFullYear() > 2500)
                    date = new Date(date.getFullYear() - 543, date.getMonth(), date.getDate());
                else date = new Date(date.getFullYear(), date.getMonth(), date.getDate());
            }
            else if (typeof date === "string") {
                var arr: string[] = date.toString().split("/");
                if (parseInt(arr[2]) > 2500)
                    date = new Date(Date.UTC(parseInt(arr[2]) - 543, parseInt(arr[1]) - 1, parseInt(arr[0])));
                else date = new Date(Date.UTC(parseInt(arr[2]), parseInt(arr[1]) - 1, parseInt(arr[0])));
            }
            return date;
        }

        public printConNotes(consignmentNo: string): ng.IPromise<void>
        {
            var condition: Interfaces.ISearchFilterView;
            this.$scope.printConsNoteParam = {};
            //console.log("consignmentNo", consignmentNo);
            //if (consignmentNo)
            //{
            //    this.$scope.printConsNoteParam.consignmentsList = consignmentNo;
            //}
            this.$scope.detailReport = [];
            this.$scope.detailReport = angular.copy(this.$sessionStorage.ListItem);
            var consList = "";
            var cons = "";

            this.$scope.detailReport.forEach(item => {
                if (item.IsSelected == true) {
                    var id = item.consignment_no.trim();
                    if (cons.length > 0) {
                        cons += ";";
                    }
                    cons += id;
                }
            });

            consList = cons;
           // console.log('consList', consList);
            this.$scope.printConsNoteParam.consignmentsList = consList;
           
            this.$scope.printConsNoteParam.userloggedin = this.$scope.authentication.userName;
            //console.log("printConsNoteParam", this.$scope.printConsNoteParam);

            return this.searchConsignmentService.getSpCSS_PrintConsNote(this.$scope.printConsNoteParam)
                .then((result: Interfaces.ISearchConsignmentPrintConsNoteResult) =>
            {
                if (result.SearchConsignmentPrintConsNoteError[0].ErrorCode > 0)
                {
                    this.notifyMessagePopupErrMessageReport(result.SearchConsignmentPrintConsNoteError[0].ErrorMessage, true);
                }
                else if (result.SearchConsignmentPrintConsNoteError[0].ReportTemplate == "")
                {
                    
                    this.notifyMessagePopupErrMessageReport("Template file not found", true);
                }
                else
                {
                    this.$scope.printConsNoteDetailParam = result.SearchConsignmentPrintConsNoteDetail;
                    //console.log("this.$scope.printConsNoteDetailParam", this.$scope.printConsNoteDetailParam);
                    this.printConNotesDetail().then(() =>
                    {
                        this.$sessionStorage.ListItem = null;
                        this.$sessionStorage.ListItem = [];
                        this.onClickSearch();
                    });
                }
                }, (error) =>
                {
                this.errorHandlerService.HandleException(error, "");
            });
            
        }
        public printConNotesDetail(): ng.IPromise<void>
        {
            this.$scope.paramConsNote = {};
            this.$scope.paramConsNote.enterpriseId = null;
            this.$scope.paramConsNote.userloggedIn = this.$scope.authentication.userName;
            this.$scope.paramConsNote.consignmentList = this.$scope.printConsNoteParam.consignmentsList;
            this.$scope.paramConsNoteResult.paramDetail = this.$scope.printConsNoteDetailParam;
            this.$scope.paramConsNoteResult.paramFilter = this.$scope.paramConsNote;
            //console.log(this.$scope.paramConsNoteResult);
            return this.searchConsignmentService.load_PrintConsNote(this.$scope.paramConsNoteResult).then(result =>
            {
                if (result)
                {
                    var blob = Shared.FileHelper.Base64ToBlob(result[0].Base64StringData, result[0].ContentType);
                    saveAs(blob, result[0].FileName);
                }
            }, (error) =>
                {
                this.errorHandlerService.HandleException(error, "");
            });
        }

        public printShippingList(consignmentNo: string): ng.IPromise<void>
        {
            var condition: Interfaces.ISearchFilterView;
            this.$scope.printShippingListParam = {};
            this.$scope.detailReport = [];
            //if (consignmentNo)
            //{
            //    this.$scope.printShippingListParam.consignmentsList = consignmentNo;
            //}
            this.$scope.detailReport = angular.copy(this.$sessionStorage.ListItem);
            var consList = "";
            var cons = "";

            this.$scope.detailReport.forEach(item => {
                if (item.IsSelected == true) {
                    var id = item.consignment_no.trim();
                    if (cons.length > 0) {
                        cons += ";";
                    }                   
                    cons += id;
                }
            });
           
            consList = cons;
           // console.log('consList', consList);


            this.$scope.printShippingListParam.consignmentsList = consList;
            this.$scope.printShippingListParam.userloggedin = this.$scope.authentication.userName;
           // console.log("printShippingListParam", this.$scope.printShippingListParam);

            return this.searchConsignmentService.getSpCSS_PrintShippingList(this.$scope.printShippingListParam)
                .then(result =>
                {
                  
                    if (result.SearchConsignmentPrintShippingListError.ErrorCode > 0) {
                        this.notifyMessagePopupErrMessageReport(result.SearchConsignmentPrintShippingListError.ErrorMessage, true);
                    }
                    else
                    {
                        //this.$scope.printShippingListDetailParam = [];
                        this.$scope.printShippingListDetailParam = result.SearchConsignmentPrintShippingListDetail;
                       
                        this.printShippingListDetail().then(() =>
                        {
                            this.$sessionStorage.ListItem = null;
                            this.$sessionStorage.ListItem = [];
                            this.onClickSearch();
                        });
                    }
                }, (error) => {
                    this.errorHandlerService.HandleException(error, "");
                });
           
        }

        public printShippingListDetail(): ng.IPromise<void> {
            // var items: Interfaces.ISearchConsignmentPrintShippingListDetail[] = [];
            // items.push(this.$scope.printShippingListDetailParam);
            // items.push(this.$scope.printShippingListDetailParam);
           // console.log("this.$scope.printShippingListDetailParam", this.$scope.printShippingListDetailParam);
            return this.searchConsignmentService.load_PrintShippingList(this.$scope.printShippingListDetailParam).then(result =>
            {
                if (result)
                {
                    var blob = Shared.FileHelper.Base64ToBlob(result[0].Base64StringData, result[0].ContentType);
                    saveAs(blob, result[0].FileName);
                }
            }, (error) =>
                {
                this.errorHandlerService.HandleException(error, "");
            });
        }

        public reprintConsNote(consignmentNo: string): ng.IPromise<void>
        {
            //var result: Interfaces.ISearchConsignmentPrintConsNoteResult = {};
            var condition: Interfaces.ISearchFilterView;
            this.$scope.reprintConsNoteParam = {};

            //console.log("consignmentNo", consignmentNo);
            //if (consignmentNo)
            //{
            //    this.$scope.reprintConsNoteParam.consignmentsList = consignmentNo;
            //}
            this.$scope.detailReport = [];
            this.$scope.detailReport = angular.copy(this.$sessionStorage.ListItem);
            var consList = "";
            var cons = "";

            this.$scope.detailReport.forEach(item => {
                if (item.IsSelected == true) {
                    var id = item.consignment_no.trim();
                    if (cons.length > 0) {
                        cons += ";";
                    }
                    cons += id;
                }
            });

            consList = cons;
            //console.log('consList', consList);
            this.$scope.reprintConsNoteParam.consignmentsList = consList;
            this.prepareReprint();
           
            //console.log("reprintConsNoteParam", this.$scope.reprintConsNoteParam);

            return this.searchConsignmentService.getSpCSS_ReprintConsNote(this.$scope.reprintConsNoteParam)
                .then((result: Interfaces.ISearchConsignmentReprintConsNoteResult) =>
                {
                    if (result.SearchConsignmentReprintConsNoteError[0].ErrorCode > 0)
                    {
                        this.notifyMessagePopupErrMessageReport(result.SearchConsignmentReprintConsNoteError[0].ErrorMessage, true);
                    }
                    else if (result.SearchConsignmentReprintConsNoteError[0].ReportTemplate == "")
                    {

                        this.notifyMessagePopupErrMessageReport("Template file not found", true);
                    }
                    else {
                        this.$scope.reprintConsNoteDetailParam = [];
                        this.$scope.reprintConsNoteDetailParam = result.SearchConsignmentReprintConsNoteDetail;
                        //console.log("this.$scope.reprintConsNoteDetailParam", this.$scope.reprintConsNoteDetailParam);
                        this.reprintConsNoteDetail().then(() =>
                        {
                            this.$sessionStorage.ListItem = null;
                            this.$sessionStorage.ListItem = [];
                            this.onClickSearch();
                        });
                    }
                }, (error) => {
                    this.errorHandlerService.HandleException(error, "");
                });
        }

        public reprintConsNoteDetail(): ng.IPromise<void> {
            this.$scope.paramReprintConsNoteResult = {}
            this.$scope.reprintConsNoteDetailParam[0].ConsignmentDescription = this.$scope.reprintConsNoteDetailParam[0].GoodsDescription;
            this.$scope.paramReprintConsNoteResult.paramDetail = this.$scope.reprintConsNoteDetailParam;
            return this.searchConsignmentService.load_ReprintConsNote(this.$scope.paramReprintConsNoteResult).then(result =>
            {
                if (result)
                {
                    var blob = Shared.FileHelper.Base64ToBlob(result[0].Base64StringData, result[0].ContentType);
                    saveAs(blob, result[0].FileName);
                }
            }, (error) =>
                {
                this.errorHandlerService.HandleException(error, "");
            });
        }

        public reprintConsNoteLabel(consignmentNo: string): ng.IPromise<void>
        {
            //var result: Interfaces.ISearchFilterView[] = [];
            var condition: Interfaces.ISearchFilterView;
            this.$scope.reprintConsNoteParam = {};

            //console.log("consignmentNo", consignmentNo);
            //if (consignmentNo)
            //{
            //    this.$scope.reprintConsNoteParam.consignmentsList = consignmentNo;
            //}
            this.$scope.detailReport = [];
            this.$scope.detailReport = angular.copy(this.$sessionStorage.ListItem);
            var consList = "";
            var cons = "";

            this.$scope.detailReport.forEach(item => {
                if (item.IsSelected == true) {
                    var id = item.consignment_no.trim();
                    if (cons.length > 0) {
                        cons += ";";
                    }
                    cons += id;
                }
            });

            consList = cons;
            //console.log('consList', consList);
            this.$scope.reprintConsNoteParam.consignmentsList = consList;
            this.prepareReprint();

            //console.log("reprintConsNoteParam", this.$scope.reprintConsNoteParam);

            return this.searchConsignmentService.getSpCSS_ReprintConsNote(this.$scope.reprintConsNoteParam)
                .then((result: Interfaces.ISearchConsignmentReprintConsNoteResult) =>
                {
                    if (result.SearchConsignmentReprintConsNoteError[0].ErrorCode > 0)
                    {
                        this.notifyMessagePopupErrMessageReport(result.SearchConsignmentReprintConsNoteError[0].ErrorMessage, true);
                    }
                    else if (result.SearchConsignmentReprintConsNoteError[0].ReportTemplate == "")
                    {

                        this.notifyMessagePopupErrMessageReport("Template file not found", true);
                    }
                    else
                    {
                        this.$scope.reprintConsNoteDetailParam = [];
                        this.$scope.reprintConsNoteDetailParam = result.SearchConsignmentReprintConsNoteDetail;
                        //console.log("this.$scope.reprintConsNoteDetailParam", this.$scope.reprintConsNoteDetailParam);
                        this.reprintConsNoteLabelDetail().then(() =>
                        {
                            this.$sessionStorage.ListItem = null;
                            this.$sessionStorage.ListItem = [];
                            this.onClickSearch();
                        });
                    }
                }, (error) =>
                {
                    this.errorHandlerService.HandleException(error, "");
                });
        }

        public reprintConsNoteLabelDetail(): ng.IPromise<void>
        {
            this.$scope.paramReprintConsNoteResult = {}
            this.$scope.paramReprintConsNoteResult.paramDetail = this.$scope.reprintConsNoteDetailParam;
            return this.searchConsignmentService.load_ReprintConsNoteLabel(this.$scope.paramReprintConsNoteResult).then(result =>
            {
                if (result)
                {
                    var blob = Shared.FileHelper.Base64ToBlob(result[0].Base64StringData, result[0].ContentType);
                    saveAs(blob, result[0].FileName);
                }
            }, (error) =>
                {
                this.errorHandlerService.HandleException(error, "");
            });
        }

        public prepareReprint(): void
        {           
            this.$scope.reprintConsNoteParam.userloggedin = this.$scope.authentication.userName;
            this.$scope.reprintConsNoteParam.dataSource = 0;
            this.$scope.reprintConsNoteParam.bookingNo = -1;
        }
     

        private AddHandlerPageLink(): void {
            //On event from page link changed
            this.WatchEvents = this.$scope.$watch(Shared.PvdConstant.SelectedPageLink, (current, old) => {
                if (!current || (current == old)) return;
                this.$scope.SearchParameter.Page = this.$scope.SelectedPageLink.Page == null ? 1 : this.$scope.SelectedPageLink.Page;
                this.onClickSearch();
            });
        }

        public initItemDetail() {
            this.$scope.detailResult.forEach(item => {
                item.IsEdit = false;
                item.IsSelected = false;
            });
        }

        public SelectRow(item: Interfaces.ICreateConsignmentPackageDetail): void {
            if (angular.isUndefined(this.$sessionStorage.ListItem) || this.$sessionStorage.ListItem == null) {
                this.$sessionStorage.ListItem = [];
            }

            var valueChk = this.$sessionStorage.ListItem.filter(Item => Item == item).length;


            // checked value in list ListItem session.
           // console.log({ valueChk });
          
            if (valueChk == 0) {
                this.$sessionStorage.ListItem.push(item);
            }
            else {
                // if unchecked will remove id from list ListItem session.
                if (!item.IsSelected) {
                    var index = this.$sessionStorage.ListItem.indexOf(item, 0);
                    if (index != undefined) {
                        this.$sessionStorage.ListItem.splice(index, 1);
                    }
                }
            }
            if (this.$sessionStorage.ListItem.length > 0) {
                this.$scope.disabledBtnMainPrint = false;
            } else {
                this.$scope.disabledBtnMainPrint = true;
            }
            //console.log('this.$sessionStorage.ListItem', this.$sessionStorage.ListItem)

            // this.CheckSelectedItemAll();
        }

        public CheckSelectedItemAll(): void {
            this.isCheckedAll = this.$sessionStorage.CheckAll;
            if (this.$sessionStorage.ListItem) {
                if (this.$sessionStorage.ListItem.length != this.$scope.detailResult.length) {
                    this.isCheckedAll = false;
                    this.$sessionStorage.CheckAll = this.isCheckedAll;
                }
                else {
                    this.$sessionStorage.CheckAll = true;
                    this.isCheckedAll = this.$sessionStorage.CheckAll;
                }
            }
        }

        

    }

    Main.App.Controllers.controller("searchConsignmentController", SearchConsignmentController);
}