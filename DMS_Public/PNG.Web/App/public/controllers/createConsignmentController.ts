﻿module CreateConsignment {
    export interface ICreateConsignmentControllerScope extends ng.IScope {
        vm: CreateConsignmentController;
        authentication: any;
        UserInfo: Interfaces.IUserInfo;
        ParentSubMenus: Interfaces.IUserMenu[];
        MenuSub: Interfaces.IUserMenu[];
        Menu: Interfaces.IUserMenu[];
        //MainMenu: Interfaces.IExternalMenu[];
        Roles: Interfaces.IExternalIdentityUserRoles[];
        IsCollapsed: boolean;
        CurrentMenuId: number;
        MenuUrl: string;
        WithOutLoginPage: boolean;

        IsAlert: boolean;
        senderParam: Interfaces.ICreateConsignmentParam;
        saveParam: Interfaces.ICreateConsignmentParam;
        updateParam: Interfaces.IUpdateConsignmentParam;
        bindSenderDetail: Interfaces.ICreateConsignmentBindSenderDetail;
        bindRecipientDetail: Interfaces.ICreateConsignmentBindRecipientDetail;
        userCustomerAccount: Interfaces.ICreateConsignmentCustomerAccount;
        IsUserCustoms: boolean;
        senderName: any[];
        serviceCode: Interfaces.ICreateConsignmentServiceCode;
        packageDetail: Interfaces.ICreateConsignmentPackageDetail[];
        senderCSSDetail: Interfaces.ICreateConsignmentSenderDetail;
        errorCSSDetail: Interfaces.ICreateConsignmentError;
        bindStatus: Interfaces.ICreateConsignmentStatus;
        packageLimit: Interfaces.ICreateConsignmentGetPackageLimit[];
        configuration: Interfaces.ICreateConsignmentGetConfiguration[];
        enterpriseContract: Interfaces.ICreateConsignmentGetEnterpriseContract;
        defaultServicecode: Interfaces.ICreateConsignmentDefaultServiceCode;
        updateRef: Interfaces.IUpdateConsignment;
        packageDetailArray: number[];
        selectedValue: string;
        visible: boolean;
        IsAllChecked: boolean;
        tooltipAdd: string;
        tooltipRemove: string;
        tooltipEdit: string;
        tooltipSave: string;
        tooltipCancel: string;
        wt: number;
        pkg_wt: number;
        oldServiceCode: string;
        visibleBtSave: boolean;
        dangerousGoods: number;
        showDangerousGoods: boolean;
        showContractAccepted: boolean;
        defaultServiceCodeFag: boolean;
        dgText: string;
        dgTextClass: boolean;
        consignNoText: string;
        consignNoShow: boolean;
        hideSavebt: boolean;
        consignmentNo: string;
        cusNo: string;
        cusName: string;
        visibleFieldset: boolean;
        visibleConsignmentNo: boolean;
        packageText: string;
        adminModeHeader: boolean;
        adminModeSender: boolean;
        adminModeRecipient: boolean;
        adminModePackage: boolean;

        validateMessgge: string;
        validateMessggeArray: string[];
        dgmessage: boolean;
        pod_slip: string;
        invoiceHc: string;
        checkEdit: boolean;
        checkDGText: boolean;       

    }



    export class CreateConsignmentController {
        public static $inject = ["$rootScope", "$scope", "$location", "authService",
            "$cookies", "configSettings",
            "localStorageService",
            "$sessionStorage", "clientConnectionService", "createConsignmentService", "$filter", "lovHelperService", "notificationService", "dialogHelperService", "$route"];
        private isCheckedAll: boolean = false;
        private selectedItem: string;
        private selectedServiceCode: string;
        private defaultUrlPrefix = "/createconsignment/";

        constructor(private $rootScope: ng.IScope,
            private $scope: ICreateConsignmentControllerScope,
            private $location: ng.ILocationService,
            private authService: Util.AuthService,
            private $cookies: ng.cookies.ICookiesService,
            private configSettings: Interfaces.IConfigSettings,
            private localStorageService: any,
            private $sessionStorage: any,
            private clientConnectionService: Util.ClientConnectionService,
            private createConsignmentService: CreateConsignment.CreateConsignmentService,
            private $filter: any,
            private lovHelperService: Lov.LovHelperService,
            private notificationService: Shared.NotificationService,
            private dialogHelperService: Shared.DialogHelperService,
            private $route: any) {

            $scope.vm = this;
            this.$scope.authentication = this.localStorageService.get('authorizationData');
            this.$scope.senderParam = {};
            this.$scope.saveParam = {};
            this.$scope.updateParam = {};
            this.$scope.defaultServicecode = {};
            this.$scope.bindSenderDetail = {};
            this.$scope.bindRecipientDetail = {};
            this.$scope.senderName = [];
            this.$scope.serviceCode = [];
            this.$scope.packageDetail = [];
            this.$scope.packageDetailArray = [];
            this.$scope.senderCSSDetail = [];
            this.$scope.errorCSSDetail = [];
            this.$scope.bindStatus = {};
            this.$scope.updateRef = {};
            this.$scope.packageLimit = [];
            this.$sessionStorage.ListItem = null;
            this.$sessionStorage.ListItem = [];
            this.$scope.enterpriseContract = {};
            this.$scope.validateMessggeArray = [];
            this.InitCreateConsignment();
            this.initMenu();
            this.initTooltip();
            this.initOnDialog();
            this.$scope.oldServiceCode = "";
            this.$scope.consignmentNo = null;
            this.$scope.dangerousGoods = 0;
            this.$scope.showDangerousGoods = false;
            this.$scope.showContractAccepted = false;
            this.$scope.dgText = "";
            this.$scope.dgTextClass = false;
            this.$scope.checkDGText = false;
            this.$scope.consignNoText = "";
            this.$scope.consignNoShow = false;
            this.$scope.hideSavebt = false;
            this.$scope.visibleFieldset = true;
            this.$scope.cusName = null;
            this.$scope.visibleConsignmentNo = false;
            this.$scope.packageText = "";
            this.$scope.pod_slip = "";
            this.$scope.invoiceHc = "";
            //this.$scope.errorCSSDetail.ErrorCode = -1;



        }

        public InitCreateConsignment() {

            this.createConsignmentService.getUserAssignedRole(this.$scope.authentication.userName).then((result: boolean) => {
                this.$scope.IsUserCustoms = result;
            }, ((error) => {
                    console.error(error);
            }));



            this.getCustomerAccount().then(() => {
                this.IsPermission();

                if (this.$scope.userCustomerAccount.IsEnterpriseUser) {
                    this.$scope.visible = false;
                    this.getReferenceDSonLoad();
                }
                else {
                    this.$scope.visible = false;
                    this.$scope.visibleConsignmentNo = true;


                    this.getSpCssConsignment()
                        .then(() => {
                            this.initData();
                            if (this.$scope.errorCSSDetail.ErrorCode == 0) {

                                if (this.$scope.userCustomerAccount.IsEnterpriseUser) {
                                    //this.createConsignmentService.getCustomsWarehousesNameDefault(this.$scope.authentication.userName).then((result: Interfaces.ICreateConsignmentCustomsWarehousesNameDefault[]) => {
                                    //    // this.$scope.visible = false;
                                    //    this.$scope.senderName = result;

                                    //}, ((error) => {
                                    //    console.error('Cannot load getCustomsWarehousesNameDefault...');
                                    //}));
                                    //this.getBindService();
                                    //this.bindAllsenderAndrecipientInfo();
                                }
                                else {
                                    //this.$scope.visible = true;
                                    this.getReferenceDS().then(() => {
                                        this.getBindSender();
                                        this.getBindService();
                                    });
                                }

                                if (this.$scope.senderCSSDetail != null) {
                                    if (this.$scope.senderCSSDetail.DangerousGoods == null || this.$scope.senderCSSDetail.DangerousGoods == 1) {
                                        this.$scope.dangerousGoods = -1;
                                        this.$scope.showDangerousGoods = true;
                                        //this.ShowDangerousGoods = true;
                                        //this.DangerousGoods = -1;
                                    }
                                    else {
                                        //this.ShowDangerousGoods = false;
                                        this.$scope.showDangerousGoods = false;
                                    }
                                    if (this.$scope.senderCSSDetail.ContractAccepted == null || this.$scope.senderCSSDetail.DangerousGoods == 0) {

                                        this.$scope.showContractAccepted = true;
                                        //this.ShowDangerousGoods = true;
                                        //this.DangerousGoods = -1;
                                    }
                                    this.getDefaultServiceCode().then(() => {
                                        if (this.$scope.defaultServicecode.Default_Service_Code != "" && this.$scope.defaultServicecode.Default_Service_Code != null) {
                                            this.selectedServiceCode = this.$scope.defaultServicecode.Default_Service_Code;
                                            this.$scope.defaultServiceCodeFag = true;
                                        }
                                        else {
                                            this.selectedServiceCode = this.$scope.senderCSSDetail.service_code;
                                            this.$scope.defaultServiceCodeFag = false;
                                        }
                                    });
                                }
                            }
                        });
                }

            });
            this.getPackageLimit();

        }

        public initMenu() {

        }
        public onChangeConsignmentNo(consignNo: string): void {
            this.$scope.consignmentNo = consignNo;
            //this.$scope.dgmessage = false;
            this.$scope.dgText = "";
            this.$scope.checkDGText = false;
            if (consignNo != null && consignNo != "" && this.$scope.cusNo != null && this.$scope.cusNo != "") {
                                
                this.getSpCssConsignment().then(() => {
                    if (this.$scope.errorCSSDetail.ErrorCode == 29 || this.$scope.errorCSSDetail.ErrorCode != 0) {
                        this.IsPermission();
                        this.bindConsignmentText(true, this.$scope.errorCSSDetail.ErrorMessage);
                        this.$scope.visibleFieldset = true;
                        this.$scope.visibleConsignmentNo = false;
                    }
                    else if (this.$scope.errorCSSDetail.ErrorCode == 0) {
                        this.IsPermission();
                        this.$scope.visibleFieldset = false;
                        this.$scope.visibleConsignmentNo = true;
                        this.$scope.consignNoShow = false;
                       
                        //this.getReferenceDS().then(() => {
                        this.getConfiguration().then(() => {
                            this.getBindService().then(() => {
                                // this.selectedServiceCode = this.$scope.serviceCode[0].service_code;
                                this.bindAllsenderAndrecipientInfo();
                            });
                        });
                        //});
                    }



                });
            }
        }

       
        public bindConsignmentText(isError: boolean, msg: string): void {
            if (isError == true) {
                this.$scope.consignNoShow = true;
                this.$scope.consignNoText = msg;
            }
        }

        public getSpCssConsignment(): ng.IPromise<void> {
            this.prepareData();
            if (this.$scope.senderParam != null) {
                //console.log("this.$scope.senderParam", this.$scope.senderParam);
                return this.createConsignmentService.getSpCSSConsignmentDetail(this.$scope.senderParam)
                    .then((result: Interfaces.ICreateConsignmentResult) =>
                    {
                        //console.log("result", result);
                       
                        if (result.CreateConsignmentSenderDetail.length > 0)
                        {
                            this.$scope.senderCSSDetail = result.CreateConsignmentSenderDetail[0];
                            this.$scope.pod_slip = this.$scope.senderCSSDetail.return_pod_slip;
                            this.$scope.invoiceHc = this.$scope.senderCSSDetail.return_invoice_hc; 
                        }
                        if (result.CreateConsignmentPackageDetail.length > 0) {
                            this.$scope.packageDetail = result.CreateConsignmentPackageDetail;
                        }
                                                     
                            this.$scope.errorCSSDetail = result.CreateConsignmentError[0];
                           
                           // console.log("senderCSSDetail", this.$scope.senderCSSDetail);
                           // console.log("errorCSSDetail", this.$scope.errorCSSDetail);
                        
                    }, ((error) => {
                        console.error(error);
                    }));
            }
        }

        public prepareData(): void {
            var actionType = 1;
            if (this.$scope.userCustomerAccount.IsEnterpriseUser) {
                this.$scope.senderParam.action = actionType;
                this.$scope.senderParam.userloggedin = this.$scope.authentication.userName;
                this.$scope.senderParam.payerid = this.$scope.cusNo;
                this.$scope.senderParam.consignment_no = this.$scope.consignmentNo;
                //console.log("in if prepare");
               
            }
            else {
                this.$scope.senderParam.action = actionType;
                this.$scope.senderParam.userloggedin = this.$scope.authentication.userName;
                this.$scope.senderParam.payerid = this.$scope.userCustomerAccount.payerid;
                //console.log("in else prepare");
            }


        }

        public getCustomerAccount(): ng.IPromise<void> {
            return this.createConsignmentService.getCustomerAccount(this.$scope.authentication.userName).then((result: Interfaces.ICreateConsignmentCustomerAccount) => {
                this.$scope.userCustomerAccount = result;
                this.$scope.cusNo = this.$scope.userCustomerAccount.payerid;
                this.$scope.cusName = this.$scope.userCustomerAccount.cust_name;
            }, ((error) => {
                    console.error(error);
            }));
        }

        public getCustomerAccountOnTextChanged(payerid: string): ng.IPromise<void> {

            if (payerid != null && payerid != "") {
                return this.createConsignmentService.getCustomerAccount2(payerid).then((result: Interfaces.ICreateConsignmentCustomerAccount2) => {
                    if (result != null) {
                        this.$scope.cusNo = result.custid;
                        this.$scope.cusName = result.cust_name;
                    }
                }, ((error) => {
                        console.error(error);
                }));
            }
        }

        public getBindSender() {
            if (this.$scope.IsUserCustoms) {
                this.getCustomsWarehousesName();
            }
            else {
                this.getBindSenderDetail();

            }
        }

        public getBindSenderDetail(): ng.IPromise<void> {
            return this.createConsignmentService.getBindSenderDetail(this.$scope.authentication.userName).then((result: Interfaces.ICreateConsignmentBindSenderDetail) => {
                if (result != null) {

                    this.$scope.bindSenderDetail = result;
                    this.getSenderStateName(this.$scope.bindSenderDetail.zipcode);
                }

            }, ((error) => {
                    console.error(error);
            }));
        }

        public getCustomsWarehousesName(): ng.IPromise<void> {
            return this.createConsignmentService.getCustomsWarehousesName(this.$scope.selectedValue).then((result: Interfaces.ICreateConsignmentCustomsWarehousesName) => {
                if (result != null) {
                    this.$scope.bindSenderDetail = result;
                }
            }, ((error) => {
                    console.error(error);
            }));
        }

        public getReferenceDS(): ng.IPromise<void> {
            return this.createConsignmentService.getCustomsWarehousesNameDefault(this.$scope.authentication.userName).then((result: Interfaces.ICreateConsignmentCustomsWarehousesNameDefault[]) => {
                this.$scope.senderName = result;
            }, ((error) => {
                    console.error(error);
            }));
        }

        public getReferenceDSonLoad(): ng.IPromise<void> {
            return this.createConsignmentService.getCustomsWarehousesNameDefault(this.$scope.authentication.userName).then((result: Interfaces.ICreateConsignmentCustomsWarehousesNameDefault[]) => {
                this.$scope.senderName = result;
                this.selectedItem = this.$scope.senderName[0];
                //console.log("this.$scope.senderName", this.$scope.senderName);
                //console.log("this.selectedItem", this.selectedItem);
            }, ((error) => {
                console.error(error);
            }));
        }
        public getBindService(): ng.IPromise<void> {
            return this.createConsignmentService.getServiceCode().then((result: Interfaces.ICreateConsignmentServiceCode[]) => {
                if (result) {
                    this.$scope.serviceCode = result;
                    this.selectedServiceCode = this.$scope.serviceCode[0].service_code;


                    //this.$scope.serviceCode = this.$scope.serviceCode[0].service_code;
                };
            }, ((error) => {
                    console.error(error);
            }));
        }

        public getSenderStateName(zipcode: string): ng.IPromise<void> {
            return this.createConsignmentService.getStateName(zipcode).then((resultStateName: Interfaces.ICreateConsignmentGetStateName) => {
                if (resultStateName != null) {
                    this.$scope.bindSenderDetail.state_name = resultStateName.state_name;
                }
            }, ((error) => {
                    console.error(error);
            }));
        }

        public getBindRecipientDetailByTelephone(telephone: string): ng.IPromise<void> {
            //this.$scope.bindRecipientDetail = {};
            //this.$scope.bindRecipientDetail.cost_centre = null;
            //this.$scope.bindRecipientDetail.zipcode = null;

            if (telephone != null && telephone != "") {
                return this.createConsignmentService.getRecipientDetailByTelephone(telephone).then((result: Interfaces.ICreateConsignmentBindRecipientDetail) => {
                    if (result.telephone != null && result.telephone != "") {
                        if (this.$scope.bindRecipientDetail.cost_centre != null && this.$scope.bindRecipientDetail.cost_centre != "") {
                            this.$scope.bindRecipientDetail.telephone = result.telephone;
                        }
                        else
                        {
                            this.$scope.bindRecipientDetail = result;
                            this.getRecipientStateName(this.$scope.bindRecipientDetail.zipcode);
                        }
                        
                    }
                }, ((error) => {
                        //console.error(error);
                }));
            }
        }

        public getRecipientStateName(zipcode: string): ng.IPromise<void> {

            return this.createConsignmentService.getStateName(zipcode).then((resultStateName: Interfaces.ICreateConsignmentGetStateName) => {
                if (resultStateName != null) {
                    this.$scope.bindRecipientDetail.state_name = resultStateName.state_name;
                }
            }, ((error) => {
                    console.error(error);
            }));

        }
        public initData() {
            this.$scope.packageDetail.forEach(item => {
                item.IsEdit = false;
                item.IsSelected = false;
            });
        }

        public AddRow(): void {
            var newItem: Interfaces.ICreateConsignmentPackageDetail;
            newItem = {};
            newItem.pkg_qty;
            newItem.pkg_wt;
            newItem.tot_wt;
            newItem.pkg_length;
            newItem.pkg_breadth;
            newItem.pkg_height;
            newItem.IsEdit = true;
            newItem.IsSelected = false;
            newItem.IsAdd = true;
            newItem.IsActive = true;
            newItem.TempPackage = [];

            this.$scope.packageDetail.push(newItem);
            this.$scope.checkEdit = true;

        }

        public SelectRow(packageItem: Interfaces.ICreateConsignmentPackageDetail): void {
            if (angular.isUndefined(this.$sessionStorage.ListItem) || this.$sessionStorage.ListItem == null) {
                this.$sessionStorage.ListItem = [];
            }

            var valueChk = this.$sessionStorage.ListItem.filter(Item => Item == packageItem).length;


            // checked value in list ListItem session.
            if (valueChk == 0) {
                this.$sessionStorage.ListItem.push(packageItem);
            }
            else {
                // if unchecked will remove id from list ListItem session.
                if (!packageItem.IsSelected) {
                    var index = this.$sessionStorage.ListItem.indexOf(packageItem, 0);
                    if (index != undefined) {
                        this.$sessionStorage.ListItem.splice(index, 1);
                    }
                }
            }

            this.CheckSelectedItemAll();
        }
        public SelectAll(): void {
            var isCheck = false;
            this.$scope.packageDetail.forEach(item => {
                item.IsSelected = this.isCheckedAll;
                if (this.isCheckedAll == true) {
                    isCheck = true;
                    this.$sessionStorage.CheckAll = this.isCheckedAll;
                }
            });
            for (var i = 0; i < this.$scope.packageDetail.length; i++) {
                this.$scope.packageDetail[i].IsSelected = this.isCheckedAll;
                if (this.isCheckedAll == true) {
                    var index = this.$sessionStorage.ListItem.indexOf(this.$scope.packageDetail[i], 0);
                    if (index == -1) //not found
                    {
                        this.$sessionStorage.ListItem.push(this.$scope.packageDetail[i]);
                        isCheck = true;
                        this.$sessionStorage.CheckAll = this.isCheckedAll;
                    }
                }
            }
            if (!isCheck) {
                this.$sessionStorage.ListItem = [];
            }

            this.CheckSelectedItemAll();
        }

        public CheckSelectedItemAll(): void {
            this.isCheckedAll = this.$sessionStorage.CheckAll;
            if (this.$sessionStorage.ListItem) {
                if (this.$sessionStorage.ListItem.length != this.$scope.packageDetail.length) {
                    this.isCheckedAll = false;
                    this.$sessionStorage.CheckAll = this.isCheckedAll;
                }
                else {
                    this.$sessionStorage.CheckAll = true;
                    this.isCheckedAll = this.$sessionStorage.CheckAll;
                }
            }
        }

        public CheckDisableDeleteButton(): string {
            var resultCssClass: string = "icon-disable";

            if (this.$sessionStorage.ListItem) {
                this.$sessionStorage.ListItem.forEach(item => {
                    if (item) {
                        resultCssClass = "cursor-pointer";
                    }
                });
            }
            return resultCssClass;
        }

        public initTooltip(): void {
            this.$scope.tooltipAdd = "Add";
            this.$scope.tooltipRemove = "Remove";
            this.$scope.tooltipEdit = "Edit";
            this.$scope.tooltipSave = "Save";
            this.$scope.tooltipCancel = "Cancel";
        }
        public EditRowData(packageItem: Interfaces.ICreateConsignmentPackageDetail): void {
            //this.LoadPackageChequeLayout(packageItem);
            packageItem.TempPackage = angular.copy(packageItem);
            packageItem.IsEdit = true;
            this.$scope.checkEdit = true;
        }

        public CancelRowData(packageItem: Interfaces.ICreateConsignmentPackageDetail, index: number): void {
            this.$scope.validateMessggeArray = [];
            if (packageItem.IsAdd == true) {
                this.$scope.packageDetail.splice(index, 1);
            } else {
                this.$scope.packageDetail[index] = angular.copy(packageItem.TempPackage);
                //this.$scope.ListBankChequeLayout = angular.copy(this.$scope.BankChequeLayoutTemp);
                packageItem.IsEdit = false;
                packageItem.IsAdd = false;
            }
            this.$scope.checkEdit = false;
        }

        public RemoveData(): void {
            this.$scope.validateMessggeArray = [];
            for (var i = this.$scope.packageDetail.length - 1; i >= 0; i--) {
                if (this.$scope.packageDetail[i].IsSelected == true) {
                    //console.log(" index ", this.$scope.packageDetail.indexOf(this.$scope.packageDetail[i]));
                    this.$scope.packageDetail.splice(this.$scope.packageDetail.indexOf(this.$scope.packageDetail[i]), 1);
                }
            }
        }
        public SaveRowData(packageItem: Interfaces.ICreateConsignmentPackageDetail, index: number): void {

            this.$scope.validateMessgge = "";
            this.$scope.validateMessggeArray = [];
            if (!packageItem) {
                console.log("packageItem is null");
            }
            else {
                this.$scope.checkEdit = false;
                var VolumeLimit = this.$scope.packageLimit.filter(f => f.key == "VolumeLimit");
                var WeightLimit = this.$scope.packageLimit.filter(f => f.key == "PkgWtLimit");
                var QuantityLimit = this.$scope.packageLimit.filter(f => f.key == "PkgQtyLimit");
                var RowLimit = this.$scope.packageLimit.filter(f => f.key == "PkgRowWtLimit");
                var DensityFactor = this.$scope.packageLimit.filter(f => f.key == "DensityFactor");

                //weight
                var wt = (packageItem.pkg_wt) ? packageItem.pkg_wt.toString().trim().replace(",", "") : null;
                if (parseFloat(wt) > parseFloat(WeightLimit[0].value)) {
                    this.$scope.validateMessgge = "Individual Package Weight exceeds the Enterprise limit.";
                    this.$scope.validateMessggeArray.push(this.$scope.validateMessgge);
                    this.$scope.packageDetail[index].pkg_wt = null;
                    this.$scope.packageDetail[index].IsEdit = true;
                }
                else if (parseFloat(wt) <= 0) {
                    this.$scope.packageDetail[index].pkg_wt = null;
                }
                else {
                    this.$scope.packageDetail[index].pkg_wt = parseFloat(wt);
                    this.$scope.packageDetail[index].IsEdit = false;
                }
                //quantity
                var qt = packageItem.pkg_qty ? packageItem.pkg_qty.toString().trim().replace(",", "") : null;

                if (parseInt(qt) > parseInt(QuantityLimit[0].value)) {

                    if (this.$scope.validateMessgge.length > 0) {
                       // this.$scope.validateMessgge += "<br/>";
                    }
                    this.$scope.validateMessgge = "Total number of packages exceeds the Enterprise Limit.";
                    this.$scope.validateMessggeArray.push(this.$scope.validateMessgge);
                    this.$scope.packageDetail[index].pkg_qty = parseInt(qt);
                    this.$scope.packageDetail[index].IsEdit = true;
                }
                else if (parseInt(qt) <= 0 || qt == null) {
                    this.$scope.packageDetail[index].pkg_qty = null;
                }
                else {
                    this.$scope.packageDetail[index].pkg_qty = parseInt(qt);
                    this.$scope.packageDetail[index].IsEdit = false;
                }
                //total weight
                var ttwaight = (parseFloat(wt) * parseInt(qt));
                if (ttwaight > parseFloat(RowLimit[0].value)) {

                    if (this.$scope.validateMessgge.length > 0) {
                        //this.$scope.validateMessgge += "<br/>";
                    }
                    this.$scope.validateMessgge = "Package Weight x Quantity exceeds the limit that can be saved.";
                    this.$scope.validateMessggeArray.push(this.$scope.validateMessgge);
                    this.$scope.packageDetail[index].tot_wt = ttwaight;
                    ////console.log("ttwaight", ttwaight);
                    this.$scope.packageDetail[index].IsEdit = true;
                }
                else if (ttwaight <= 0) {
                    this.$scope.packageDetail[index].tot_wt = null;
                }
                else {
                    this.$scope.packageDetail[index].tot_wt = ttwaight;
                    this.$scope.packageDetail[index].IsEdit = false;
                }
                //width
                var lengtH = (packageItem.pkg_length) ? packageItem.pkg_length.toString().trim().replace(",", "") : null;
                if (parseFloat(lengtH) != null && parseFloat(lengtH) > 0) {
                    this.$scope.packageDetail[index].pkg_length = parseFloat(lengtH);
                }
                else {
                    this.$scope.packageDetail[index].pkg_length = null;
                }
                //breadth
                var breadtH = (packageItem.pkg_breadth) ? packageItem.pkg_breadth.toString().trim().replace(",", "") : null;
                if (parseFloat(breadtH) != null && parseFloat(breadtH) > 0) {
                    this.$scope.packageDetail[index].pkg_breadth = parseFloat(breadtH);
                }
                else {
                    this.$scope.packageDetail[index].pkg_breadth = null;
                }
                //height
                var heighT = (packageItem.pkg_height) ? packageItem.pkg_height.toString().trim().replace(",", "") : null;
                if (parseFloat(heighT) != null && parseFloat(heighT) > 0) {
                    this.$scope.packageDetail[index].pkg_height = parseFloat(heighT);
                }
                else {
                    this.$scope.packageDetail[index].pkg_height = null;
                }

                if (((parseFloat(lengtH) * parseFloat(breadtH) * parseFloat(heighT)) * parseInt(qt)) > parseFloat(VolumeLimit[0].value)) {

                    if (this.$scope.validateMessgge.length > 0) {
                    }
                    this.$scope.validateMessgge = "Volume of package exceeds the Enterprise Limit.";
                    this.$scope.validateMessggeArray.push(this.$scope.validateMessgge);
                    this.$scope.packageDetail[index].IsEdit = true;
                }
                else
                {
                    this.$scope.packageDetail[index].IsEdit = false;
                }

                if ((((parseFloat(lengtH) * parseFloat(breadtH) * parseFloat(heighT)) * parseInt(qt)) / parseFloat(DensityFactor[0].value)) > parseFloat(RowLimit[0].value)) {

                    if (this.$scope.validateMessgge.length > 0) {
                    }
                    this.$scope.validateMessgge = "Package Weight x Quantity exceeds the limit that can be saved.";
                    this.$scope.validateMessggeArray.push(this.$scope.validateMessgge);
                    this.$scope.packageDetail[index].IsEdit = true;
                }
                else
                {
                    this.$scope.packageDetail[index].IsEdit = false;
                }


                if (parseFloat(wt) > parseFloat(WeightLimit[0].value) || ((parseFloat(lengtH) * parseFloat(breadtH) * parseFloat(heighT)) * parseInt(qt)) > parseFloat(VolumeLimit[0].value) || (((parseFloat(lengtH) * parseFloat(breadtH) * parseFloat(heighT)) * parseInt(qt)) / parseFloat(DensityFactor[0].value)) > parseFloat(RowLimit[0].value)) {
                    this.$scope.packageDetail[index].IsEdit = true;
                }
                else if (parseFloat(wt) <= 0) {
                    this.$scope.packageDetail[index].pkg_wt = null;
                }
                else {
                    this.$scope.packageDetail[index].IsEdit = false;
                }
                //quantity

                if (parseInt(qt) > parseInt(QuantityLimit[0].value) || ((parseFloat(lengtH) * parseFloat(breadtH) * parseFloat(heighT)) * parseInt(qt)) > parseFloat(VolumeLimit[0].value) || (((parseFloat(lengtH) * parseFloat(breadtH) * parseFloat(heighT)) * parseInt(qt)) / parseFloat(DensityFactor[0].value)) > parseFloat(RowLimit[0].value)) {
                    this.$scope.packageDetail[index].IsEdit = true;
                }
                else if (parseInt(qt) <= 0 || qt == null) {
                    this.$scope.packageDetail[index].pkg_qty = null;
                }
                else {
                    this.$scope.packageDetail[index].IsEdit = false;
                }
                
                this.$scope.packageDetail[index].IsSelected = false;
                this.$scope.packageDetail[index].IsActive = false;



            }

        }

        public getPackageLimit(): ng.IPromise<void> {
            return this.createConsignmentService.getPackageLimit(this.$scope.authentication.userName).then((result: Interfaces.ICreateConsignmentGetPackageLimit[]) => {
                this.$scope.packageLimit = result;
            }, ((error) => {
                    console.error(error);
            }));
        }
        public CreateConsignment(): void {
            this.$scope.checkDGText = true;
            this.getConfiguration().then(() => {

                this.getEnterpriseConTract().then(() => {
                    //if (this.$scope.enterpriseContract != null)
                    //{
                    var DGGoodService = this.$scope.configuration.filter(f => f.key == "DangerousGoodsService");
                    this.dialogHelperService.ConfirmMessageHideCLoseBtn("Confirm", "Does this consignment contain dangerous goods?",
                        () => {
                            //Todo Confirm Set Value DG toSelect Box
                            this.selectedServiceCode = DGGoodService[0].value;
                           
                            this.$scope.dangerousGoods = 1;
                            this.$scope.dgText = "";
                           // this.$scope.dgmessage = true;
                            //this.$scope.oldServiceCode = DGGoodService[0].value;
                            if (this.$scope.enterpriseContract.AcceptedCurrentContract != null && this.$scope.enterpriseContract.AcceptedCurrentContract == "1") {
                                this.SaveData();
                            }
                            else {
                                if (this.$scope.userCustomerAccount.IsEnterpriseUser) {
                                    this.SaveData();
                                }
                                else {
                                    //alert('Contract ')

                                    this.SaveData();
                                }
                            }
                        },
                        () => {
                            //Todo Cancel Donothing
                            //alert('cancel');
                            this.$scope.saveParam.DangerousGoods = 0;
                            this.$scope.dangerousGoods = 0;
                            this.$scope.dgText = "";
                            if (this.$scope.enterpriseContract.AcceptedCurrentContract != null && this.$scope.enterpriseContract.AcceptedCurrentContract == "1") {
                                this.SaveData();
                            }
                            else {
                                if (this.$scope.userCustomerAccount.IsEnterpriseUser) {
                                    this.SaveData();
                                }
                                else {
                                    //alert('Contract ')

                                    this.SaveData();
                                }
                            }
                        });

                    //}
                });

            });
        }

        // NOTIFY MESSAGE
        private notifyMessage(msg: string, isError: boolean): void {
            if (isError) {
                this.dialogHelperService.ShowMessage("Error", msg);
            }
            else {
                this.notificationService.Success(msg);
            }
        }
        public getConfiguration(): ng.IPromise<void> {
            this.$scope.configuration = [];
            return this.createConsignmentService.getConfiguration(this.$scope.authentication.userName).then((result: Interfaces.ICreateConsignmentGetConfiguration[]) => {
                this.$scope.configuration = result;
            }, ((error) => {
                    console.error(error);
            }));
        }
        public getEnterpriseConTract(): ng.IPromise<void> {
            this.$scope.enterpriseContract = {}
            return this.createConsignmentService.getEnterpriseContract(this.$scope.authentication.userName).then((result: Interfaces.ICreateConsignmentGetEnterpriseContract) => {
                if (result != null) {
                    this.$scope.enterpriseContract = result;
                }
            }, ((error) => {
                //console.error('Cannot load getEnterpriseContract');
                    console.log(error);
            }));
        }
        public onchangeServiceCode() {
            

        }
        public SaveData(): void {
           
            var actionType = 2;
            this.$scope.saveParam = {};
            this.$scope.saveParam.action = actionType;
            this.$scope.saveParam.userloggedin = this.$scope.authentication.userName;
            if (this.$scope.userCustomerAccount.IsEnterpriseUser) {
                this.$scope.saveParam.payerid = this.$scope.cusNo; 
                this.$scope.saveParam.consignment_no = this.$scope.consignmentNo;
               
            }

            this.$scope.saveParam.ContractAccepted = this.$scope.senderCSSDetail.ContractAccepted;


            var pkg = "";
            for (var i = 0; i < this.$scope.packageDetail.length; i++) {
                var line = "";

                if (this.$scope.packageDetail[i].pkg_qty != null && this.$scope.packageDetail[i].pkg_qty > 0) {
                    line += "" + this.$scope.packageDetail[i].pkg_qty.toString();
                }
                else {
                    line += "0";
                }
                var wt = (this.$scope.packageDetail[i].pkg_wt) ? this.$scope.packageDetail[i].pkg_wt.toString().trim().replace(",", "") : null;
                if (parseFloat(wt) != null && parseFloat(wt) > 0) {
                    line += "," + parseFloat(wt);
                }
                else {
                    line += ",0";
                }

                var length = (this.$scope.packageDetail[i].pkg_length) ? this.$scope.packageDetail[i].pkg_length.toString().trim().replace(",", "") : null;
                if (parseFloat(length) != null && parseFloat(length) > 0) {
                    line += "," + parseFloat(length);
                }
                var breadth = (this.$scope.packageDetail[i].pkg_breadth) ? this.$scope.packageDetail[i].pkg_breadth.toString().trim().replace(",", "") : null;
                if (parseFloat(breadth) != null && parseFloat(breadth) > 0) {
                    line += "," + parseFloat(breadth);
                }
                var height = (this.$scope.packageDetail[i].pkg_height) ? this.$scope.packageDetail[i].pkg_height.toString().trim().replace(",", "") : null;
                if (parseFloat(height) != null && parseFloat(height) > 0) {
                    line += "," + parseFloat(height);
                }
                if (pkg.length > 0) {
                    pkg = pkg + ";";
                }
                pkg += line;
            }


            
            this.$scope.saveParam.service_code = this.selectedServiceCode;
            this.$scope.saveParam.sender_name = this.selectedItem;
            this.$scope.saveParam.sender_address1 = this.$scope.bindSenderDetail.address1;
            this.$scope.saveParam.sender_address2 = this.$scope.bindSenderDetail.address2;
            this.$scope.saveParam.sender_zipcode = this.$scope.bindSenderDetail.zipcode;
            this.$scope.saveParam.sender_telephone = this.$scope.bindSenderDetail.telephone;
            this.$scope.saveParam.sender_fax = this.$scope.bindSenderDetail.fax;
            this.$scope.saveParam.sender_contact_person = this.$scope.bindSenderDetail.contact_person;
            this.$scope.saveParam.sender_email = this.$scope.bindSenderDetail.email;
            this.$scope.saveParam.recipient_telephone = this.$scope.bindRecipientDetail.telephone;
            this.$scope.saveParam.recipient_name = this.$scope.bindRecipientDetail.reference_name;
            this.$scope.saveParam.recipient_address1 = this.$scope.bindRecipientDetail.address1;
            this.$scope.saveParam.recipient_address2 = this.$scope.bindRecipientDetail.address2;
            this.$scope.saveParam.recipient_zipcode = this.$scope.bindRecipientDetail.zipcode;
            this.$scope.saveParam.recipient_fax = this.$scope.bindRecipientDetail.fax;
            this.$scope.saveParam.recipient_contact_person = this.$scope.bindRecipientDetail.contactperson;
            this.$scope.saveParam.return_pod_slip = this.$scope.pod_slip;
            this.$scope.saveParam.return_invoice_hc = this.$scope.invoiceHc;
            this.$scope.saveParam.PackageDetails = pkg;
            this.$scope.saveParam.ref_no = this.$scope.bindRecipientDetail.ref_no;
            this.$scope.saveParam.remark = this.$scope.bindRecipientDetail.remark;
            this.$scope.saveParam.GoodsDescription = this.$scope.bindRecipientDetail.GoodsDescription;
            this.$scope.saveParam.DangerousGoods = this.$scope.dangerousGoods;

            this.sendParamtoSave().then(() => {

                if (this.$scope.errorCSSDetail.ErrorCode == 0) {

                    //this.$scope.dangerousGoods = -1;
                    this.updateReferenceCreateConsignment().then(() => {

                        this.$scope.hideSavebt = true;
                        this.$scope.showDangerousGoods = false;
                        var message = "Consignment saved"
                        this.notifyMessage(message, false);
                        this.$scope.adminModeHeader = true;
                        this.$scope.adminModeSender = true;
                        this.$scope.adminModeRecipient = true;
                        //this.bindAllsenderAndrecipientInfo();        create will use reload page instead bindall recipient || bind will use in update consignment
                      
                        //var message = "Consignment saved"
                        //this.notifyMessage(message, false);
                        //this.$scope.adminModeHeader = true
                        //this.$scope.adminModeSender = true;
                        //this.$scope.adminModeRecipient = true;
                    }).then(() => {
                        //this.backToIndex();  senderCSSDetail.consignment_no
                      //  console.log("consignment_no", this.$scope.senderCSSDetail.consignment_no);
                        var myMessage = "Your consignment no is " + this.$scope.senderCSSDetail.consignment_no
                       // console.log("myMessage", myMessage);
                        this.notifyMessagePopupConsignment(myMessage, true);
                        });

                }
                else
                {
                    this.notifyMessage(this.$scope.errorCSSDetail.ErrorMessage, true);
                    this.$scope.adminModeHeader = false;
                    this.$scope.adminModeSender = false;
                    this.$scope.adminModeRecipient = false;
                }
            });


        }

        private notifyMessagePopupConsignment(msg: string, isError: boolean): void {
            if (isError) {
                this.dialogHelperService.ShowMessage2("Consignment No", msg,
                    () => {
                        //Todo ok 
                        this.backToIndex();
                    });
            }
            else {
                this.notificationService.Success(msg);
            }
        }
        public sendParamtoSave(): ng.IPromise<void> {
            //this.$scope.errorCSSDetail = {};
            //this.$scope.senderCSSDetail = {};
            return this.createConsignmentService.getSpCSSConsignmentDetail(this.$scope.saveParam)
                .then((result: Interfaces.ICreateConsignmentResult) => {
                    if (result.CreateConsignmentError != null) {
                        if (result.CreateConsignmentPackageDetail.length > 0)
                        {
                            this.$scope.packageDetail = result.CreateConsignmentPackageDetail;
                        }
                        if (result.CreateConsignmentSenderDetail.length > 0)
                        {
                            this.$scope.senderCSSDetail = result.CreateConsignmentSenderDetail[0];
                        }
                        if (result.CreateConsignmentError.length > 0)
                        {
                            this.$scope.errorCSSDetail = result.CreateConsignmentError[0];
                        }
                        
                    }
                }, ((error) => {
                    console.error(error);
                }));
        }
        public updateReferenceCreateConsignment(): ng.IPromise<void> {
            this.$scope.saveParam.cost_centre = this.$scope.bindRecipientDetail.cost_centre;
            return this.createConsignmentService.updateReferenceStoreProcedure(this.$scope.saveParam).then((result: Interfaces.IUpdateConsignment) => {
                this.$scope.updateRef = result;
            }, ((error) => {
                    console.error(error);
            }));
        }

        public getDefaultServiceCode(): ng.IPromise<void> {
            return this.createConsignmentService.getDefaultServiceCode(this.$scope.authentication.userName).then((result: Interfaces.ICreateConsignmentDefaultServiceCode) => {
                if (result != null) {
                    this.$scope.defaultServicecode = result[0];
                }
            }, ((error) => {
                //console.error('Cannot load getDefaultServiceCode');
            }));
        }

        public bindAllsenderAndrecipientInfo(): void {
            if (this.$scope.userCustomerAccount.IsEnterpriseUser) {
                this.getDefaultServiceCode().then(() => {
                    if (this.$scope.defaultServicecode.Default_Service_Code != "" && this.$scope.defaultServicecode.Default_Service_Code != null) {
                        this.selectedServiceCode = this.$scope.defaultServicecode.Default_Service_Code;
                    }
                    else {
                        this.selectedServiceCode = this.$scope.senderCSSDetail.service_code;
                    }
                });
            }

            
            this.$scope.bindStatus.Created_By = this.$scope.senderCSSDetail.Created_By;
            this.$scope.bindStatus.Created_DT = this.$scope.senderCSSDetail.Created_DT;
            this.$scope.bindStatus.Updated_By = this.$scope.senderCSSDetail.Updated_By;
            this.$scope.bindStatus.Updated_DT = this.$scope.senderCSSDetail.Updated_DT;
            this.$scope.bindStatus.last_status = this.$scope.senderCSSDetail.last_status;
            this.$scope.bindStatus.last_status_DT = this.$scope.senderCSSDetail.last_status_DT;
            this.$scope.userCustomerAccount.shippinglist = this.$scope.senderCSSDetail.ShippingList_no;
            //this.$scope.senderName = this.$scope.senderCSSDetail.sender_name;
            //sender
            this.$scope.bindSenderDetail.address1 = this.$scope.senderCSSDetail.sender_address1;
            this.$scope.bindSenderDetail.address2 = this.$scope.senderCSSDetail.sender_address2;
            this.$scope.bindSenderDetail.zipcode = this.$scope.senderCSSDetail.sender_zipcode;
            this.$scope.bindSenderDetail.state_name = this.$scope.senderCSSDetail.sender_state_name;
            this.$scope.bindSenderDetail.telephone = this.$scope.senderCSSDetail.sender_telephone;
            this.$scope.bindSenderDetail.fax = this.$scope.senderCSSDetail.sender_fax;
            this.$scope.bindSenderDetail.contact_person = this.$scope.senderCSSDetail.sender_contact_person;
            this.$scope.bindSenderDetail.email = this.$scope.senderCSSDetail.sender_email;
            //recipient
            this.$scope.bindRecipientDetail.telephone = this.$scope.senderCSSDetail.recipient_telephone;
            //this.$scope.bindRecipientDetail.cost_centre = this.$scope.senderCSSDetail.c;
            this.$scope.bindRecipientDetail.reference_name = this.$scope.senderCSSDetail.recipient_name;
            this.$scope.bindRecipientDetail.address1 = this.$scope.senderCSSDetail.recipient_address1;
            this.$scope.bindRecipientDetail.address2 = this.$scope.senderCSSDetail.recipient_address2;
            this.$scope.bindRecipientDetail.zipcode = this.$scope.senderCSSDetail.recipient_zipcode;
            this.$scope.bindRecipientDetail.state_name = this.$scope.senderCSSDetail.recipient_state_name;
            this.$scope.bindRecipientDetail.fax = this.$scope.senderCSSDetail.recipient_fax;
            this.$scope.bindRecipientDetail.contactperson = this.$scope.senderCSSDetail.recipient_contact_person;
            this.$scope.bindRecipientDetail.ref_no = this.$scope.senderCSSDetail.ref_no;
            this.$scope.bindRecipientDetail.remark = this.$scope.senderCSSDetail.remark;
            this.$scope.bindRecipientDetail.GoodsDescription = this.$scope.senderCSSDetail.GoodsDescription;

            var DGGoodService = this.$scope.configuration.filter(f => f.key == "DangerousGoodsService");
      
            if (this.$scope.senderCSSDetail.service_code == DGGoodService[0].value) {
                this.selectedServiceCode = DGGoodService[0].value;
            }
            else
            {
                this.selectedServiceCode = this.$scope.senderCSSDetail.service_code;
            }

            if (this.$scope.checkDGText == true)
            {
                if (this.$scope.senderCSSDetail.DangerousGoods == 1)
                {
                    this.bindDGText(true);
                }
                else
                {
                    this.bindDGText(false);
                }
            }          

            
            //if (this.$scope.senderName.filter(f => f.sender_name == this.$scope.senderCSSDetail.sender_name) != null)
            //{
            //    this.selectedItem = this.$scope.senderCSSDetail.sender_name;
            //}
            //else
            //{
            //    var tempArray: Interfaces.ICreateConsignmentCustomsWarehousesNameDefault[] = [];
            //    var tempArray2: any[] = this.$scope.senderName;
            //    tempArray.push(this.$scope.senderCSSDetail.sender_name);

            //    this.$scope.senderName = [];
            //    for (var i = 0; i < tempArray2.length; i++) {
            //        tempArray.push(tempArray2[i]);
            //    }
            //    //console.log("tempArray for", tempArray);
            //    this.$scope.senderName = tempArray;

            //}

         
           



        }
        public bindDGText(isDG: boolean): void {
            if (isDG == true) {
                this.$scope.dgTextClass = true;
                this.$scope.dgText = "Dangerous Goods Declaration: Shipper has declared that this consignment contains dangerous goods.";
            }
            else {
                this.$scope.dgTextClass = false;
                this.$scope.dgText = "Dangerous Goods Declaration: Shipper has declared that this consignment does not contain dangerous goods.";
            }
        }

        private initOnDialog(): void {
            // DIALOG: ZipCodePopup
            this.$scope.$on("ZipCodePopup", (event, ...data: any[]) => {
                var selectedItem: Interfaces.IZipCode = data[0][1]
                this.$scope.bindRecipientDetail.zipcode = selectedItem.zipcode;
                this.getRecipientStateName(this.$scope.bindRecipientDetail.zipcode);
                //this.setSessionStorage();

                // Hide Dialog
                var dialog: any = angular.element(document.querySelector("#" + "ZipCodePopup"));
                dialog.modal("hide");
                this.lovHelperService.ClearLovParameter();
            });

            // DIALOG: ZipCodePopup
            this.$scope.$on("ZipCodePopupSender", (event, ...data: any[]) => {
                var selectedItem: Interfaces.IZipCode = data[0][1]
                this.$scope.bindSenderDetail.zipcode = selectedItem.zipcode;
                this.$scope.bindSenderDetail.state_name = selectedItem.state_code;
                this.getSenderStateName(this.$scope.bindSenderDetail.zipcode);
                //this.setSessionStorage();

                // Hide Dialog
                var dialog: any = angular.element(document.querySelector("#" + "ZipCodePopupSender"));
                dialog.modal("hide");
                this.lovHelperService.ClearLovParameter();
            });


            // DIALOG: CostCentrePopup
            this.$scope.$on("CostCentrePopup", (event, ...data: any[]) => {
                this.$scope.bindRecipientDetail.lovZipCode = [];
                var selectedItem: Interfaces.ICostCentre = data[0][1]
                this.$scope.bindRecipientDetail.cost_centre = selectedItem.cost_centre;
                this.$scope.bindRecipientDetail.reference_name = selectedItem.reference_name;
                this.getBindRecipientDetailByCostCentre(this.$scope.bindRecipientDetail.cost_centre);
                //this.setSessionStorage();

                // Hide Dialog
                var dialog: any = angular.element(document.querySelector("#" + "CostCentrePopup"));
                dialog.modal("hide");
                this.lovHelperService.ClearLovParameter();
            });

            // DIALOG: CostCentrePopup
            this.$scope.$on("TelephonePopup", (event, ...data: any[]) => {
                this.$scope.bindRecipientDetail.lovZipCode = [];
                var selectedItem: Interfaces.ICostCentre = data[0][1]
                this.$scope.bindRecipientDetail.telephone = selectedItem.telephone;
                this.$scope.bindRecipientDetail.reference_name = selectedItem.reference_name;
                this.getBindRecipientDetailByTelephone(this.$scope.bindRecipientDetail.telephone);
                //this.setSessionStorage();

                // Hide Dialog
                var dialog: any = angular.element(document.querySelector("#" + "TelephonePopup"));
                dialog.modal("hide");
                this.lovHelperService.ClearLovParameter();
            });
        }
        public getBindRecipientDetailByCostCentre(costcentre: string): ng.IPromise<void> {
            //this.$scope.bindRecipientDetail = {};
            this.$scope.bindRecipientDetail.cost_centre = null;
            //this.$scope.bindRecipientDetail.zipcode = null;

            if (costcentre != null && costcentre != "") {
                return this.createConsignmentService.getRecipientDetailByCostCentre(costcentre).then((result: Interfaces.ICreateConsignmentBindRecipientDetail) => {
                    if (result.cost_centre != null && result.cost_centre != "") {
                        if (this.$scope.bindRecipientDetail.telephone != null && this.$scope.bindRecipientDetail.telephone != "") {
                            this.$scope.bindRecipientDetail.cost_centre = result.cost_centre;
                        }
                        else
                        {
                            this.$scope.bindRecipientDetail = result;
                            this.getRecipientStateName(this.$scope.bindRecipientDetail.zipcode);
                        }
                        
                        
                    }
                }, ((error) => {
                        console.error(error);
                }));
            }
        }


        // Search ListOfView
        public searchZipCode(zipcode: string): void {
            this.lovHelperService.CreateZipCodeLov("ZipCodePopup");

            this.lovHelperService.SetLovParameter({
                BroadcastName: "ZipCodePopup",
                SearchFilter: {
                    zipcode: zipcode ? zipcode : "",
                    country: "PAPUA NEW GUINEA"
                },
                IsMultipleSelected: false,
                IsHideSearchStatus: false,
                IsShowLock: false,
                Active: 1 // All:-1, Active:1, Inactive:0
            });

        }

        // Search ListOfView
        public searchZipCodeSender(zipcode: string): void {
            this.lovHelperService.CreateZipCodeLov("ZipCodePopupSender");

            this.lovHelperService.SetLovParameter({
                BroadcastName: "ZipCodePopupSender",
                SearchFilter: {
                    zipcode: zipcode ? zipcode : "",
                    country: "PAPUA NEW GUINEA"
                },
                IsMultipleSelected: false,
                IsHideSearchStatus: false,
                IsShowLock: false,
                Active: 1 // All:-1, Active:1, Inactive:0
            });

        }

        //Search ListOfView
        public searchCostCentre(costCentre: string): void {
            this.lovHelperService.CreateCostCentreLov("CostCentrePopup");

            this.lovHelperService.SetLovParameter({
                BroadcastName: "CostCentrePopup",
                SearchFilter: {
                    telephone: "",
                    cost_centre: costCentre ? costCentre : "",
                    reference_name: "",
                },
                IsMultipleSelected: false,
                IsHideSearchStatus: false,
                IsShowLock: false,
                Active: 1 // All:-1, Active:1, Inactive:0
            });

        }

        //Search ListOfView
        public searchTelephone(telephone: string): void {
            this.lovHelperService.CreateTelephoneLov("TelephonePopup");

            this.lovHelperService.SetLovParameter({
                BroadcastName: "TelephonePopup",
                SearchFilter: {
                    telephone: telephone ? telephone : "",
                    cost_centre: "",
                    reference_name: "",
                },
                IsMultipleSelected: false,
                IsHideSearchStatus: false,
                IsShowLock: false,
                Active: 1 // All:-1, Active:1, Inactive:0
            });

        }



        public IsPermission(): void {
            if (this.$scope.userCustomerAccount.IsEnterpriseUser) // admin
            {
                this.$scope.adminModeHeader = false;
                this.$scope.adminModeSender = true;
                this.$scope.adminModeRecipient = true;
                this.$scope.adminModePackage = true;
                if (this.$scope.errorCSSDetail.ErrorCode == 0) {
                    this.$scope.adminModeHeader = true;
                    this.$scope.adminModeSender = false;
                    this.$scope.adminModeRecipient = false;
                    this.$scope.adminModePackage = true;
                }

                if (this.$scope.errorCSSDetail.ErrorCode == 29) {
                    this.$scope.adminModeHeader = false;
                    this.$scope.adminModeSender = true;
                    this.$scope.adminModeRecipient = true;
                    this.$scope.adminModePackage = true;
                }
            }
            else {
                this.$scope.adminModeHeader = true;
                this.$scope.adminModeSender = false;
                this.$scope.adminModeRecipient = false;
                this.$scope.adminModePackage = false;
            }

        }

        public backToIndex() {
            //this.$location.path("/");
            this.$location.url(/*this.defaultUrlPrefix*/"#/createconsignment/");
            this.$route.reload();
        }



        public getInvalid(item: any) {
            //console.log(item);
        }


    }

    Main.App.Controllers.controller("createConsignmentController", CreateConsignmentController);
}