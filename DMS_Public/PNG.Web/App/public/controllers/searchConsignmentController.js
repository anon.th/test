var SearchConsignment;
(function (SearchConsignment) {
    var SearchConsignmentController = (function () {
        function SearchConsignmentController($rootScope, $scope, $location, authService, $cookies, configSettings, localStorageService, $sessionStorage, clientConnectionService, $filter, lovHelperService, notificationService, dialogHelperService, searchConsignmentService, createConsignmentService, $route, $window, $routeParams, errorHandlerService) {
            this.$rootScope = $rootScope;
            this.$scope = $scope;
            this.$location = $location;
            this.authService = authService;
            this.$cookies = $cookies;
            this.configSettings = configSettings;
            this.localStorageService = localStorageService;
            this.$sessionStorage = $sessionStorage;
            this.clientConnectionService = clientConnectionService;
            this.$filter = $filter;
            this.lovHelperService = lovHelperService;
            this.notificationService = notificationService;
            this.dialogHelperService = dialogHelperService;
            this.searchConsignmentService = searchConsignmentService;
            this.createConsignmentService = createConsignmentService;
            this.$route = $route;
            this.$window = $window;
            this.$routeParams = $routeParams;
            this.errorHandlerService = errorHandlerService;
            this.isCheckedAll = false;
            this.startDate = null;
            this.endDate = null;
            this.defaultUrlPrefix = "/searchconsignment";
            $scope.vm = this;
            this.$scope.authentication = this.localStorageService.get('authorizationData');
            //console.log(this.$scope.authentication);
            //console.log(this.$scope.authentication.userName);
            this.filter = $routeParams.filter != undefined ? $routeParams.filter : null;
            //console.log("this.filter", this.filter);
            if (this.filter) {
                this.$scope.filter = this.filter;
            }
            this.Init();
            this.InitSearchConsignment();
        }
        SearchConsignmentController.prototype.Init = function () {
            this.$scope.disabledBtnMainPrint = true;
            this.$sessionStorage.ListItem = null;
            this.$sessionStorage.ListItem = [];
            this.$scope.userCustomerAccount = {};
            this.$scope.enterpriseConfig = [];
            this.$scope.referenceDS = [];
            this.$scope.serviceCode = [];
            this.$scope.status = [];
            this.$scope.searchParam = {};
            this.$scope.searchParamLocalStorage = {};
            this.$scope.errorResult = [];
            this.$scope.detailResult = [];
            this.$scope.detailReport = [];
            this.$scope.paramConsNoteResult = {};
            this.$scope.paramReprintConsNoteResult = {};
            this.$scope.printConsNoteParam = {};
            this.$scope.printShippingListParam = {};
            this.$scope.reprintConsNoteParam = {};
            this.$scope.paramConsNote = {};
            this.$scope.printConsNoteDetailParam = [];
            this.$scope.printShippingListDetailParam = [];
            this.$scope.reprintConsNoteDetailParam = [];
            this.$scope.errorMessage = null;
            this.$scope.showErrorText = true;
            this.$scope.consignmentNo = null;
            this.masterAWBNo = null;
            this.jobEntryNo = null;
            this.$scope.recipientPhoneNo = null;
            this.$scope.RecipientPostalCode = null;
            this.$scope.ReferenceNo = null;
            this.$scope.payerIdparam = null;
            this.selectedSenderName = null;
            this.$scope.SearchParameter = [];
            this.$scope.contData = 0;
            this.$scope.SearchParameter.ItemsPerPage = 10;
            this.$scope.SearchParameter.Page = 1;
            this.AddHandlerPageLink();
        };
        SearchConsignmentController.prototype.InitSearchConsignment = function () {
            var _this = this;
            this.getCustomerAccount().then(function () {
                _this.getConsignmentStatusPrintingConfigurations();
                if (_this.$scope.userCustomerAccount.IsEnterpriseUser == false) {
                    _this.getReferenceDS();
                }
                //IsEnterpriseUser
                if (_this.$scope.userCustomerAccount.IsEnterpriseUser) {
                    _this.$scope.disabledBtnPrint = true;
                    _this.getReferenceDS();
                    _this.$scope.senderNameVisible = false;
                    _this.$scope.customerIdVisible = true;
                    if (_this.$scope.userCustomerAccount.IsCustomsUser) {
                        _this.getCustomsWarehousesName();
                    }
                    else {
                        _this.getEnterpriseUserAccounts().then(function () {
                            if (_this.$scope.customerId != null && _this.$scope.customerId != "") {
                                _this.getReferenceDS();
                            }
                        });
                    }
                }
                else {
                    _this.$scope.disabledBtnPrint = false;
                    //this.$scope.senderNameVisible = true;
                    _this.$scope.customerIdVisible = false;
                    if (_this.$scope.userCustomerAccount.IsCustomerUser == true && _this.$scope.userCustomerAccount.IsMasterCustomerUser == true) {
                    }
                    if (_this.$scope.userCustomerAccount.IsCustomerUser == true && _this.$scope.userCustomerAccount.IsMasterCustomerUser == false) {
                    }
                }
                _this.bindControl();
            }).then(function () {
                if (_this.$scope.filter != null) {
                    if (_this.localStorageService.get("SearchParam") != null) {
                        _this.$scope.searchParamLocalStorage = _this.localStorageService.get("SearchParam");
                        //console.log(" this.$scope.searchParamLocalStorage", this.$scope.searchParamLocalStorage);
                        _this.getSpCSS_ConsignmentStatusByLocalStorage().then(function () {
                            if (_this.$scope.errorResult.ErrorCode != 0) {
                                _this.$scope.errorText = _this.$scope.errorResult.ErrorMessage;
                                _this.$scope.showErrorText = true;
                            }
                            else {
                                _this.$scope.showErrorText = false;
                            }
                        });
                    }
                }
            });
        };
        SearchConsignmentController.prototype.getCustomerAccount = function () {
            var _this = this;
            return this.createConsignmentService.getCustomerAccount(this.$scope.authentication.userName).then(function (result) {
                if (result != null && result != "") {
                    _this.$scope.userCustomerAccount = result;
                }
            }, (function (error) {
                console.error(error);
            }));
        };
        SearchConsignmentController.prototype.getConsignmentStatusPrintingConfigurations = function () {
            var _this = this;
            return this.searchConsignmentService.getStatusPrinting().then(function (result) {
                if (result != null && result != "") {
                    _this.$scope.enterpriseConfig = result;
                    // console.log("this.$scope.enterpriseConfig", this.$scope.enterpriseConfig);
                    _this.$scope.SearchParameter.ItemsPerPage = parseInt(_this.$scope.enterpriseConfig.value);
                }
            }, (function (error) {
                console.error(error);
            }));
        };
        SearchConsignmentController.prototype.getReferenceDS = function () {
            var _this = this;
            return this.searchConsignmentService.getReferenceDS(this.$scope.authentication.userName).then(function (result) {
                if (result != null && result.length > 1) {
                    _this.$scope.senderNameVisible = false;
                    _this.$scope.referenceDS = result;
                    if (_this.$scope.userCustomerAccount.IsCustomerUser) {
                        _this.selectedSenderName = result[0].snd_rec_name;
                    }
                }
                else {
                    _this.$scope.senderNameVisible = true;
                    _this.$scope.referenceDS = result;
                    _this.selectedSenderName = result[0].snd_rec_name;
                }
            }, (function (error) {
                console.error(error);
            }));
        };
        SearchConsignmentController.prototype.getCustomsWarehousesName = function () {
            var _this = this;
            return this.searchConsignmentService.getCustomsWarehousesName(this.selectedSenderName).then(function (result) {
                if (result != null && result.length > 0) {
                    _this.$scope.referenceDS = result;
                }
            }, (function (error) {
                console.error(error);
            }));
        };
        SearchConsignmentController.prototype.getEnterpriseUserAccounts = function () {
            var _this = this;
            return this.searchConsignmentService.getEnterpriseUserAccounts(this.$scope.authentication.userName).then(function (result) {
                if (result != null && result.length > 0) {
                    _this.$scope.customerId = result;
                }
                //else { this.$scope.customerIdVisible = true; }
            }, (function (error) {
                //  console.error(error);
            }));
        };
        SearchConsignmentController.prototype.getServiceCode = function () {
            var _this = this;
            return this.createConsignmentService.getServiceCode().then(function (result) {
                if (result != null && result.length > 0) {
                    _this.$scope.serviceCode = result;
                }
            }, (function (error) {
                console.error(error);
            }));
        };
        SearchConsignmentController.prototype.bindControl = function () {
            var _this = this;
            this.getServiceCode().then(function () {
                if (_this.$scope.userCustomerAccount.IsCustomsUser) {
                    _this.getCustomStatus();
                }
                else {
                    _this.getCustomerStatus();
                }
            });
        };
        SearchConsignmentController.prototype.getCustomStatus = function () {
            var _this = this;
            return this.searchConsignmentService.getCustomStatus().then(function (result) {
                if (result != null && result.length > 0) {
                    _this.$scope.status = result;
                }
            }, (function (error) {
                console.error(error);
            }));
        };
        SearchConsignmentController.prototype.getCustomerStatus = function () {
            var _this = this;
            return this.searchConsignmentService.getCustomerStatus().then(function (result) {
                if (result != null && result.length > 0) {
                    _this.$scope.status = result;
                }
            }, (function (error) {
                console.error(error);
            }));
        };
        SearchConsignmentController.prototype.getSpCSS_ConsignmentStatus = function () {
            var _this = this;
            return this.searchConsignmentService.getSpCSS_ConsignmentStatus(this.$scope.searchParam).then(function (result) {
                if (result != null) {
                    if (result.SearchConsignmentError.length > 0) {
                        _this.$scope.errorResult = result.SearchConsignmentError[0];
                    }
                    if (result.SearchConsignmentDetail.length > 0) {
                        _this.$scope.detailResult = result.SearchConsignmentDetail;
                    }
                    _this.$scope.contData = result.CountData;
                }
            }, (function (error) {
                console.error(error);
            }));
        };
        SearchConsignmentController.prototype.prepareParam = function () {
            if (this.payerCusID != null && this.payerCusID != "") {
                this.$scope.searchParam.payerid = this.payerCusID;
            }
            this.$scope.searchParam.userloggedin = this.$scope.authentication.userName;
            //this.$scope.searchParam.payerid = this.$scope.payerIdparam;
            if (this.$scope.consignmentNo != null && this.$scope.consignmentNo != "") {
                this.$scope.searchParam.consignment_no = this.$scope.consignmentNo;
            }
            if (this.selectedSenderName != null && this.selectedSenderName != "") {
                this.$scope.searchParam.SenderName = this.selectedSenderName;
            }
            if (this.endDate && !this.startDate) {
                this.$scope.errorMessage = "Please enter From Date if To Date entered.";
            }
            else {
                if (this.startDate) {
                    var stdate = this.getDate(this.startDate);
                    this.$scope.searchParam.datefrom = stdate;
                }
            }
            if (this.startDate && this.endDate) {
                if (this.endDate < this.startDate) {
                    this.$scope.errorMessage = "To Date should be greater than or equal to From Date.";
                }
                else {
                    var CurrentDate = this.$filter("date")(Date.now(), "dd/MM/yyyy");
                    //console.log("CurrentDate", CurrentDate);
                    if (this.endDate > CurrentDate) {
                        this.$scope.errorMessage = "Dates should not be in the future.";
                    }
                    else {
                        var endate = this.getDate(this.endDate);
                        this.$scope.searchParam.dateto = endate;
                        this.$scope.errorMessage = null;
                    }
                    if (this.startDate > CurrentDate) {
                        this.$scope.errorMessage = "Dates should not be in the future.";
                    }
                    else {
                        var stdate = this.getDate(this.startDate);
                        this.$scope.searchParam.datefrom = stdate;
                        this.$scope.errorMessage = null;
                    }
                }
            }
            //if (this.endDate)
            //{
            //    var endate: Date = this.getDate(this.endDate);
            //    console.log("endate", endate);
            //    console.log("this.endDate", this.endDate);
            //    this.$scope.searchParam.dateto = endate;
            //}
            if (this.selectedStatus != null) {
                this.$scope.searchParam.status_id = this.selectedStatus;
            }
            if (this.$scope.enterpriseConfig.value != null && this.$scope.enterpriseConfig.value != "") {
                this.$scope.searchParam.pageCount = parseInt(this.$scope.enterpriseConfig.value);
            }
            else {
                this.$scope.searchParam.pageCount = 10;
            }
            if (this.$scope.SearchParameter.Page != null && this.$scope.SearchParameter.Page != 0) {
                this.$scope.searchParam.pageNumber = this.$scope.SearchParameter.Page;
            }
            else {
                this.$scope.searchParam.pageNumber = 1;
            }
            if (this.selectedServiceCode) {
                this.$scope.searchParam.service_code = this.selectedServiceCode;
            }
            if (this.masterAWBNo != null && this.masterAWBNo != "") {
                this.$scope.searchParam.MasterAWBNumber = this.masterAWBNo;
            }
            if (this.jobEntryNo != null && this.jobEntryNo != "") {
                this.$scope.searchParam.JobEntryNo = this.jobEntryNo;
            }
            if (this.$scope.recipientPhoneNo != null) {
                this.$scope.searchParam.recipient_telephone = this.$scope.recipientPhoneNo;
            }
            if (this.$scope.RecipientPostalCode != null) {
                this.$scope.searchParam.recipient_zipcode = this.$scope.RecipientPostalCode;
            }
            if (this.$scope.ReferenceNo != null) {
                this.$scope.searchParam.ref_no = this.$scope.ReferenceNo;
            }
            this.localStorageService.set("SearchParam", this.$scope.searchParam);
        };
        SearchConsignmentController.prototype.notifyMessagePopupErrMessage = function (msg, isError) {
            if (isError) {
                this.dialogHelperService.ShowMessage("Warning", msg);
            }
            else {
                this.notificationService.Success(msg);
            }
        };
        SearchConsignmentController.prototype.notifyMessagePopupErrMessageReport = function (msg, isError) {
            if (isError) {
                this.dialogHelperService.ShowMessage("Warning", msg);
            }
            else {
                this.notificationService.Success(msg);
            }
        };
        SearchConsignmentController.prototype.onClickSearch = function () {
            var _this = this;
            this.$scope.searchParam = {};
            this.prepareParam();
            this.$scope.disabledBtnMainPrint = true;
            this.$sessionStorage.ListItem = null;
            this.$sessionStorage.ListItem = [];
            if (this.$scope.errorMessage != null) {
                this.notifyMessagePopupErrMessage(this.$scope.errorMessage, true);
            }
            else {
                //console.log(" this.$scope.searchParam", this.$scope.searchParam);
                this.getSpCSS_ConsignmentStatus().then(function () {
                    if (_this.$scope.errorResult.ErrorCode != 0) {
                        _this.$scope.errorText = _this.$scope.errorResult.ErrorMessage;
                        _this.$scope.showErrorText = true;
                    }
                    else {
                        _this.$scope.showErrorText = false;
                    }
                });
            }
            //console.log("this.$scope.SearchParameter.ItemsPerPage", this.$scope.SearchParameter.ItemsPerPage);
        };
        //public reloadByLocalstorage(): void
        //{
        //    if (this.localStorageService.get("SearchParam") != null)
        //    {
        //        this.$scope.searchParamLocalStorage = this.localStorageService.get("SearchParam");
        //    }
        //}
        SearchConsignmentController.prototype.getSpCSS_ConsignmentStatusByLocalStorage = function () {
            var _this = this;
            return this.searchConsignmentService.getSpCSS_ConsignmentStatus(this.$scope.searchParamLocalStorage).then(function (result) {
                if (result != null) {
                    if (result.SearchConsignmentError.length > 0) {
                        _this.$scope.errorResult = result.SearchConsignmentError[0];
                    }
                    if (result.SearchConsignmentDetail.length > 0) {
                        _this.$scope.detailResult = result.SearchConsignmentDetail;
                    }
                    _this.$scope.contData = result.CountData;
                }
            }, (function (error) {
                console.error(error);
            }));
        };
        SearchConsignmentController.prototype.onClickClear = function () {
            this.$scope.disabledBtnMainPrint = true;
            this.$sessionStorage.ListItem = null;
            this.$sessionStorage.ListItem = [];
            this.$location.url("#/searchconsignment");
            //this.$window.location.href = "#/searchconsignment";
            this.$route.reload();
        };
        SearchConsignmentController.prototype.getPayerIdParam = function (cusId) {
            //console.log("cusId", cusId);
            if (cusId != null && cusId != "") {
                this.$scope.searchParam.payerid = cusId;
            }
        };
        SearchConsignmentController.prototype.updateConsignment = function (consignmentNo) {
            //console.log("consignmentNo =>", consignmentNo);
            //this.$location.url("#/updateconsignment/consignmentno/" + consignmentNo);
            //this.$location.path("#/updateconsignment/consignmentno/" + consignmentNo);
            this.$window.location.href = "#/updateconsignment/consignmentno/" + consignmentNo;
        };
        SearchConsignmentController.prototype.getDate = function (date) {
            if (typeof date === "Date") {
                if (date.getFullYear() > 2500)
                    date = new Date(date.getFullYear() - 543, date.getMonth(), date.getDate());
                else
                    date = new Date(date.getFullYear(), date.getMonth(), date.getDate());
            }
            else if (typeof date === "string") {
                var arr = date.toString().split("/");
                if (parseInt(arr[2]) > 2500)
                    date = new Date(Date.UTC(parseInt(arr[2]) - 543, parseInt(arr[1]) - 1, parseInt(arr[0])));
                else
                    date = new Date(Date.UTC(parseInt(arr[2]), parseInt(arr[1]) - 1, parseInt(arr[0])));
            }
            return date;
        };
        SearchConsignmentController.prototype.printConNotes = function (consignmentNo) {
            var _this = this;
            var condition;
            this.$scope.printConsNoteParam = {};
            //console.log("consignmentNo", consignmentNo);
            //if (consignmentNo)
            //{
            //    this.$scope.printConsNoteParam.consignmentsList = consignmentNo;
            //}
            this.$scope.detailReport = [];
            this.$scope.detailReport = angular.copy(this.$sessionStorage.ListItem);
            var consList = "";
            var cons = "";
            this.$scope.detailReport.forEach(function (item) {
                if (item.IsSelected == true) {
                    var id = item.consignment_no.trim();
                    if (cons.length > 0) {
                        cons += ";";
                    }
                    cons += id;
                }
            });
            consList = cons;
            // console.log('consList', consList);
            this.$scope.printConsNoteParam.consignmentsList = consList;
            this.$scope.printConsNoteParam.userloggedin = this.$scope.authentication.userName;
            //console.log("printConsNoteParam", this.$scope.printConsNoteParam);
            return this.searchConsignmentService.getSpCSS_PrintConsNote(this.$scope.printConsNoteParam)
                .then(function (result) {
                if (result.SearchConsignmentPrintConsNoteError[0].ErrorCode > 0) {
                    _this.notifyMessagePopupErrMessageReport(result.SearchConsignmentPrintConsNoteError[0].ErrorMessage, true);
                }
                else if (result.SearchConsignmentPrintConsNoteError[0].ReportTemplate == "") {
                    _this.notifyMessagePopupErrMessageReport("Template file not found", true);
                }
                else {
                    _this.$scope.printConsNoteDetailParam = result.SearchConsignmentPrintConsNoteDetail;
                    //console.log("this.$scope.printConsNoteDetailParam", this.$scope.printConsNoteDetailParam);
                    _this.printConNotesDetail().then(function () {
                        _this.$sessionStorage.ListItem = null;
                        _this.$sessionStorage.ListItem = [];
                        _this.onClickSearch();
                    });
                }
            }, function (error) {
                _this.errorHandlerService.HandleException(error, "");
            });
        };
        SearchConsignmentController.prototype.printConNotesDetail = function () {
            var _this = this;
            this.$scope.paramConsNote = {};
            this.$scope.paramConsNote.enterpriseId = null;
            this.$scope.paramConsNote.userloggedIn = this.$scope.authentication.userName;
            this.$scope.paramConsNote.consignmentList = this.$scope.printConsNoteParam.consignmentsList;
            this.$scope.paramConsNoteResult.paramDetail = this.$scope.printConsNoteDetailParam;
            this.$scope.paramConsNoteResult.paramFilter = this.$scope.paramConsNote;
            //console.log(this.$scope.paramConsNoteResult);
            return this.searchConsignmentService.load_PrintConsNote(this.$scope.paramConsNoteResult).then(function (result) {
                if (result) {
                    var blob = Shared.FileHelper.Base64ToBlob(result[0].Base64StringData, result[0].ContentType);
                    saveAs(blob, result[0].FileName);
                }
            }, function (error) {
                _this.errorHandlerService.HandleException(error, "");
            });
        };
        SearchConsignmentController.prototype.printShippingList = function (consignmentNo) {
            var _this = this;
            var condition;
            this.$scope.printShippingListParam = {};
            this.$scope.detailReport = [];
            //if (consignmentNo)
            //{
            //    this.$scope.printShippingListParam.consignmentsList = consignmentNo;
            //}
            this.$scope.detailReport = angular.copy(this.$sessionStorage.ListItem);
            var consList = "";
            var cons = "";
            this.$scope.detailReport.forEach(function (item) {
                if (item.IsSelected == true) {
                    var id = item.consignment_no.trim();
                    if (cons.length > 0) {
                        cons += ";";
                    }
                    cons += id;
                }
            });
            consList = cons;
            // console.log('consList', consList);
            this.$scope.printShippingListParam.consignmentsList = consList;
            this.$scope.printShippingListParam.userloggedin = this.$scope.authentication.userName;
            // console.log("printShippingListParam", this.$scope.printShippingListParam);
            return this.searchConsignmentService.getSpCSS_PrintShippingList(this.$scope.printShippingListParam)
                .then(function (result) {
                if (result.SearchConsignmentPrintShippingListError.ErrorCode > 0) {
                    _this.notifyMessagePopupErrMessageReport(result.SearchConsignmentPrintShippingListError.ErrorMessage, true);
                }
                else {
                    //this.$scope.printShippingListDetailParam = [];
                    _this.$scope.printShippingListDetailParam = result.SearchConsignmentPrintShippingListDetail;
                    _this.printShippingListDetail().then(function () {
                        _this.$sessionStorage.ListItem = null;
                        _this.$sessionStorage.ListItem = [];
                        _this.onClickSearch();
                    });
                }
            }, function (error) {
                _this.errorHandlerService.HandleException(error, "");
            });
        };
        SearchConsignmentController.prototype.printShippingListDetail = function () {
            var _this = this;
            // var items: Interfaces.ISearchConsignmentPrintShippingListDetail[] = [];
            // items.push(this.$scope.printShippingListDetailParam);
            // items.push(this.$scope.printShippingListDetailParam);
            // console.log("this.$scope.printShippingListDetailParam", this.$scope.printShippingListDetailParam);
            return this.searchConsignmentService.load_PrintShippingList(this.$scope.printShippingListDetailParam).then(function (result) {
                if (result) {
                    var blob = Shared.FileHelper.Base64ToBlob(result[0].Base64StringData, result[0].ContentType);
                    saveAs(blob, result[0].FileName);
                }
            }, function (error) {
                _this.errorHandlerService.HandleException(error, "");
            });
        };
        SearchConsignmentController.prototype.reprintConsNote = function (consignmentNo) {
            var _this = this;
            //var result: Interfaces.ISearchConsignmentPrintConsNoteResult = {};
            var condition;
            this.$scope.reprintConsNoteParam = {};
            //console.log("consignmentNo", consignmentNo);
            //if (consignmentNo)
            //{
            //    this.$scope.reprintConsNoteParam.consignmentsList = consignmentNo;
            //}
            this.$scope.detailReport = [];
            this.$scope.detailReport = angular.copy(this.$sessionStorage.ListItem);
            var consList = "";
            var cons = "";
            this.$scope.detailReport.forEach(function (item) {
                if (item.IsSelected == true) {
                    var id = item.consignment_no.trim();
                    if (cons.length > 0) {
                        cons += ";";
                    }
                    cons += id;
                }
            });
            consList = cons;
            //console.log('consList', consList);
            this.$scope.reprintConsNoteParam.consignmentsList = consList;
            this.prepareReprint();
            //console.log("reprintConsNoteParam", this.$scope.reprintConsNoteParam);
            return this.searchConsignmentService.getSpCSS_ReprintConsNote(this.$scope.reprintConsNoteParam)
                .then(function (result) {
                if (result.SearchConsignmentReprintConsNoteError[0].ErrorCode > 0) {
                    _this.notifyMessagePopupErrMessageReport(result.SearchConsignmentReprintConsNoteError[0].ErrorMessage, true);
                }
                else if (result.SearchConsignmentReprintConsNoteError[0].ReportTemplate == "") {
                    _this.notifyMessagePopupErrMessageReport("Template file not found", true);
                }
                else {
                    _this.$scope.reprintConsNoteDetailParam = [];
                    _this.$scope.reprintConsNoteDetailParam = result.SearchConsignmentReprintConsNoteDetail;
                    //console.log("this.$scope.reprintConsNoteDetailParam", this.$scope.reprintConsNoteDetailParam);
                    _this.reprintConsNoteDetail().then(function () {
                        _this.$sessionStorage.ListItem = null;
                        _this.$sessionStorage.ListItem = [];
                        _this.onClickSearch();
                    });
                }
            }, function (error) {
                _this.errorHandlerService.HandleException(error, "");
            });
        };
        SearchConsignmentController.prototype.reprintConsNoteDetail = function () {
            var _this = this;
            this.$scope.paramReprintConsNoteResult = {};
            this.$scope.reprintConsNoteDetailParam[0].ConsignmentDescription = this.$scope.reprintConsNoteDetailParam[0].GoodsDescription;
            this.$scope.paramReprintConsNoteResult.paramDetail = this.$scope.reprintConsNoteDetailParam;
            return this.searchConsignmentService.load_ReprintConsNote(this.$scope.paramReprintConsNoteResult).then(function (result) {
                if (result) {
                    var blob = Shared.FileHelper.Base64ToBlob(result[0].Base64StringData, result[0].ContentType);
                    saveAs(blob, result[0].FileName);
                }
            }, function (error) {
                _this.errorHandlerService.HandleException(error, "");
            });
        };
        SearchConsignmentController.prototype.reprintConsNoteLabel = function (consignmentNo) {
            var _this = this;
            //var result: Interfaces.ISearchFilterView[] = [];
            var condition;
            this.$scope.reprintConsNoteParam = {};
            //console.log("consignmentNo", consignmentNo);
            //if (consignmentNo)
            //{
            //    this.$scope.reprintConsNoteParam.consignmentsList = consignmentNo;
            //}
            this.$scope.detailReport = [];
            this.$scope.detailReport = angular.copy(this.$sessionStorage.ListItem);
            var consList = "";
            var cons = "";
            this.$scope.detailReport.forEach(function (item) {
                if (item.IsSelected == true) {
                    var id = item.consignment_no.trim();
                    if (cons.length > 0) {
                        cons += ";";
                    }
                    cons += id;
                }
            });
            consList = cons;
            //console.log('consList', consList);
            this.$scope.reprintConsNoteParam.consignmentsList = consList;
            this.prepareReprint();
            //console.log("reprintConsNoteParam", this.$scope.reprintConsNoteParam);
            return this.searchConsignmentService.getSpCSS_ReprintConsNote(this.$scope.reprintConsNoteParam)
                .then(function (result) {
                if (result.SearchConsignmentReprintConsNoteError[0].ErrorCode > 0) {
                    _this.notifyMessagePopupErrMessageReport(result.SearchConsignmentReprintConsNoteError[0].ErrorMessage, true);
                }
                else if (result.SearchConsignmentReprintConsNoteError[0].ReportTemplate == "") {
                    _this.notifyMessagePopupErrMessageReport("Template file not found", true);
                }
                else {
                    _this.$scope.reprintConsNoteDetailParam = [];
                    _this.$scope.reprintConsNoteDetailParam = result.SearchConsignmentReprintConsNoteDetail;
                    //console.log("this.$scope.reprintConsNoteDetailParam", this.$scope.reprintConsNoteDetailParam);
                    _this.reprintConsNoteLabelDetail().then(function () {
                        _this.$sessionStorage.ListItem = null;
                        _this.$sessionStorage.ListItem = [];
                        _this.onClickSearch();
                    });
                }
            }, function (error) {
                _this.errorHandlerService.HandleException(error, "");
            });
        };
        SearchConsignmentController.prototype.reprintConsNoteLabelDetail = function () {
            var _this = this;
            this.$scope.paramReprintConsNoteResult = {};
            this.$scope.paramReprintConsNoteResult.paramDetail = this.$scope.reprintConsNoteDetailParam;
            return this.searchConsignmentService.load_ReprintConsNoteLabel(this.$scope.paramReprintConsNoteResult).then(function (result) {
                if (result) {
                    var blob = Shared.FileHelper.Base64ToBlob(result[0].Base64StringData, result[0].ContentType);
                    saveAs(blob, result[0].FileName);
                }
            }, function (error) {
                _this.errorHandlerService.HandleException(error, "");
            });
        };
        SearchConsignmentController.prototype.prepareReprint = function () {
            this.$scope.reprintConsNoteParam.userloggedin = this.$scope.authentication.userName;
            this.$scope.reprintConsNoteParam.dataSource = 0;
            this.$scope.reprintConsNoteParam.bookingNo = -1;
        };
        SearchConsignmentController.prototype.AddHandlerPageLink = function () {
            var _this = this;
            //On event from page link changed
            this.WatchEvents = this.$scope.$watch(Shared.PvdConstant.SelectedPageLink, function (current, old) {
                if (!current || (current == old))
                    return;
                _this.$scope.SearchParameter.Page = _this.$scope.SelectedPageLink.Page == null ? 1 : _this.$scope.SelectedPageLink.Page;
                _this.onClickSearch();
            });
        };
        SearchConsignmentController.prototype.initItemDetail = function () {
            this.$scope.detailResult.forEach(function (item) {
                item.IsEdit = false;
                item.IsSelected = false;
            });
        };
        SearchConsignmentController.prototype.SelectRow = function (item) {
            if (angular.isUndefined(this.$sessionStorage.ListItem) || this.$sessionStorage.ListItem == null) {
                this.$sessionStorage.ListItem = [];
            }
            var valueChk = this.$sessionStorage.ListItem.filter(function (Item) { return Item == item; }).length;
            // checked value in list ListItem session.
            // console.log({ valueChk });
            if (valueChk == 0) {
                this.$sessionStorage.ListItem.push(item);
            }
            else {
                // if unchecked will remove id from list ListItem session.
                if (!item.IsSelected) {
                    var index = this.$sessionStorage.ListItem.indexOf(item, 0);
                    if (index != undefined) {
                        this.$sessionStorage.ListItem.splice(index, 1);
                    }
                }
            }
            if (this.$sessionStorage.ListItem.length > 0) {
                this.$scope.disabledBtnMainPrint = false;
            }
            else {
                this.$scope.disabledBtnMainPrint = true;
            }
            //console.log('this.$sessionStorage.ListItem', this.$sessionStorage.ListItem)
            // this.CheckSelectedItemAll();
        };
        SearchConsignmentController.prototype.CheckSelectedItemAll = function () {
            this.isCheckedAll = this.$sessionStorage.CheckAll;
            if (this.$sessionStorage.ListItem) {
                if (this.$sessionStorage.ListItem.length != this.$scope.detailResult.length) {
                    this.isCheckedAll = false;
                    this.$sessionStorage.CheckAll = this.isCheckedAll;
                }
                else {
                    this.$sessionStorage.CheckAll = true;
                    this.isCheckedAll = this.$sessionStorage.CheckAll;
                }
            }
        };
        SearchConsignmentController.$inject = ["$rootScope", "$scope", "$location", "authService",
            "$cookies", "configSettings",
            "localStorageService",
            "$sessionStorage", "clientConnectionService", "$filter", "lovHelperService", "notificationService",
            "dialogHelperService", "searchConsignmentService", "createConsignmentService", "$route", "$window", "$routeParams", "errorHandlerService"];
        return SearchConsignmentController;
    }());
    SearchConsignment.SearchConsignmentController = SearchConsignmentController;
    Main.App.Controllers.controller("searchConsignmentController", SearchConsignmentController);
})(SearchConsignment || (SearchConsignment = {}));
//# sourceMappingURL=searchConsignmentController.js.map