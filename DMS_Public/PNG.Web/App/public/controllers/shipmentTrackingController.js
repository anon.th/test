var ShipmentTracking;
(function (ShipmentTracking) {
    var ShipmentTrackingController = (function () {
        function ShipmentTrackingController($scope, $location, $route, authService, $cookies, $q, $sessionStorage, localStorageService, shipmentTrackingService, errorHandlerService, lovHelperService, dialogHelperService, notificationService, dateTimeHelperService, $filter) {
            this.$scope = $scope;
            this.$location = $location;
            this.$route = $route;
            this.authService = authService;
            this.$cookies = $cookies;
            this.$q = $q;
            this.$sessionStorage = $sessionStorage;
            this.localStorageService = localStorageService;
            this.shipmentTrackingService = shipmentTrackingService;
            this.errorHandlerService = errorHandlerService;
            this.lovHelperService = lovHelperService;
            this.dialogHelperService = dialogHelperService;
            this.notificationService = notificationService;
            this.dateTimeHelperService = dateTimeHelperService;
            this.$filter = $filter;
            $scope.vm = this;
            //$scope.authentication = authService.authentication;
            this.$scope.authentication = this.localStorageService.get('authorizationData');
            //console.log("this.$scope.authentication", this.$scope.authentication);
            this.currentPath = $location.path();
            var withOutAuth = [Shared.RouteNames.WelCome, Shared.RouteNames.EmailAccess, Shared.RouteNames.Reset, Shared.RouteNames.Unlock,
                Shared.RouteNames.Request, Shared.RouteNames.ShowMessage, Shared.RouteNames.Register, Shared.RouteNames.Login, Shared.RouteNames.RegisterUnlock];
            this.prepareData();
            this.prepareParam();
            this.initShipmentTracking();
            this.prepareValue();
        }
        ShipmentTrackingController.prototype.prepareData = function () {
            this.$scope.userDetail = {};
            this.$scope.paramShipment = {};
            this.$scope.searchParmeter = {};
            this.$scope.monthList = [];
            this.$scope.bookingTypeList = [];
            this.$scope.payerTypeList = [];
            this.$scope.allRoles = [];
            this.$scope.pathCode = [];
            this.$scope.dsOrigin = [];
            this.$scope.dsDestination = [];
            this.$scope.tranDate = [];
            this.$scope.routeType = [];
            this.$scope.deliveryPathParam = {};
            this.$scope.txtConsignmentNo = null;
            this.$scope.txtYear = null;
            this.$scope.txtBookingNo = null;
            this.$scope.txtPayerCode = null;
            this.$scope.errorMessage = null;
            this.$scope.chkPackageName = null;
            this.chkMatchStatusCode = false;
            this.chkInvoiceIssue = false;
            this.chkShowPackage = false;
            this.chkShipmentClose = false;
            this.initOnDialog();
            this.currentYear = new Date().getFullYear();
        };
        ShipmentTrackingController.prototype.prepareParam = function () {
            this.$scope.paramShipment.userid = this.$scope.authentication.userName;
        };
        ShipmentTrackingController.prototype.prepareValue = function () {
            this.$scope.tranDate =
                [
                    {
                        "value": "B",
                        "name": "Booking Date"
                    },
                    {
                        "value": "P",
                        "name": "Pickup Date"
                    },
                    {
                        "value": "S",
                        "name": "Status Date"
                    }
                ];
            this.selectedTranDate = "B";
            this.$scope.routeType =
                [
                    {
                        "value": "L",
                        "name": "Linehaul"
                    },
                    {
                        "value": "S",
                        "name": "Delivery/CL"
                    },
                    {
                        "value": "A",
                        "name": "Air Route"
                    }
                ];
            this.selectedRouteType = "L";
        };
        ShipmentTrackingController.prototype.initShipmentTracking = function () {
            var _this = this;
            this.getUserDetail().then(function () {
                if (_this.$scope.userDetail.User_Type != "C") {
                    _this.$scope.checkUser = false;
                }
                else {
                    _this.$scope.checkUser = true;
                }
                _this.getMonth().then(function () {
                    _this.getBookingType().then(function () {
                        _this.getPayerType().then(function () {
                            _this.getAllRoles().then(function () {
                                _this.getPathCodeQuery().then(function () {
                                    _this.getDistributionCenterQueryOrigin().then(function () {
                                        if (_this.CheckUserRoleLocation()) {
                                            _this.getDistributionCenterQueryDestination();
                                        }
                                        else {
                                            _this.getDistributionCenterQueryWithLocation();
                                        }
                                    });
                                });
                            });
                        });
                    });
                });
            });
        };
        ShipmentTrackingController.prototype.getUserDetail = function () {
            var _this = this;
            return this.shipmentTrackingService.GetUserDetail(this.$scope.authentication.userName).then(function (result) {
                if (result) {
                    _this.$scope.userDetail = result;
                    //console.log("this.$scope.userDetail", this.$scope.userDetail);
                    _this.$scope.paramShipment.user_culture = _this.$scope.userDetail.user_culture;
                }
            }, function (error) {
                _this.errorHandlerService.HandleException(error, "");
            });
        };
        ShipmentTrackingController.prototype.getMonth = function () {
            var _this = this;
            this.$scope.paramShipment.codeid = "months";
            return this.shipmentTrackingService.GetCodevalue(this.$scope.paramShipment).then(function (result) {
                if (result != null && result.length > 0) {
                    _this.$scope.monthList = result;
                    _this.localStorageService.set("localMonthList", _this.$scope.monthList);
                }
            }, function (error) {
                _this.errorHandlerService.HandleException(error, "");
            });
        };
        ShipmentTrackingController.prototype.getBookingType = function () {
            var _this = this;
            this.$scope.paramShipment.codeid = "booking_type";
            return this.shipmentTrackingService.GetCodevalue(this.$scope.paramShipment).then(function (result) {
                if (result != null && result.length > 0) {
                    _this.$scope.bookingTypeList = result;
                }
            }, function (error) {
                _this.errorHandlerService.HandleException(error, "");
            });
        };
        ShipmentTrackingController.prototype.getPayerType = function () {
            var _this = this;
            this.$scope.paramShipment.codeid = "customer_type";
            return this.shipmentTrackingService.GetCodevalue(this.$scope.paramShipment).then(function (result) {
                if (result != null && result.length > 0) {
                    _this.$scope.payerTypeList = result;
                }
            }, function (error) {
                _this.errorHandlerService.HandleException(error, "");
            });
        };
        ShipmentTrackingController.prototype.getAllRoles = function () {
            var _this = this;
            return this.shipmentTrackingService.GetAllRoles(this.$scope.authentication.userName).then(function (result) {
                if (result != null && result.length > 0) {
                    _this.$scope.allRoles = result;
                }
            }, function (error) {
                _this.errorHandlerService.HandleException(error, "");
            });
        };
        ShipmentTrackingController.prototype.getPathCodeQuery = function () {
            var _this = this;
            return this.shipmentTrackingService.GetPathCodeQuery().then(function (result) {
                if (result != null && result.length > 0) {
                    _this.$scope.pathCode = result;
                }
            }, function (error) {
                _this.errorHandlerService.HandleException(error, "");
            });
        };
        ShipmentTrackingController.prototype.getDistributionCenterQueryOrigin = function () {
            var _this = this;
            return this.shipmentTrackingService.GetDistributionCenterQuery().then(function (result) {
                if (result != null && result.length > 0) {
                    _this.$scope.dsOrigin = result;
                }
            }, function (error) {
                _this.errorHandlerService.HandleException(error, "");
            });
        };
        ShipmentTrackingController.prototype.getDistributionCenterQueryDestination = function () {
            var _this = this;
            return this.shipmentTrackingService.GetDistributionCenterQuery().then(function (result) {
                if (result != null && result.length > 0) {
                    _this.$scope.dsDestination = result;
                }
            }, function (error) {
                _this.errorHandlerService.HandleException(error, "");
            });
        };
        ShipmentTrackingController.prototype.getDistributionCenterQueryWithLocation = function () {
            var _this = this;
            return this.shipmentTrackingService.GetDistributionCenterQueryWithLocation(this.$scope.userDetail.Location).then(function (result) {
                if (result != null && result.length > 0) {
                    _this.$scope.dsDestination = result;
                }
            }, function (error) {
                _this.errorHandlerService.HandleException(error, "");
            });
        };
        ShipmentTrackingController.prototype.CheckUserRoleLocation = function () {
            var result = false;
            for (var i in this.$scope.allRoles) {
                if (this.$scope.allRoles[i].role_name == "OPSSU") {
                    result = true;
                    break;
                }
                else if (this.$scope.allRoles[i].role_name == "CSSU") {
                    result = true;
                    break;
                }
            }
            //console.log("result", result);
            return result;
        };
        //public checkMatchStatusCOde(statusCode: boolean): void
        //{
        //    console.log("statusCode", statusCode);
        //    console.log("this.$scope.chkMatchStatusCode", this.$scope.chkMatchStatusCode);
        //}
        ShipmentTrackingController.prototype.loadDeliveryPath = function () {
            var _this = this;
            if (this.selectedPathCode != null && this.selectedPathCode != "") {
                this.$scope.deliveryPathParam.path_code = this.selectedPathCode;
                return this.shipmentTrackingService.GetDeliveryPath(this.$scope.deliveryPathParam).then(function (result) {
                    if (result) {
                        _this.$scope.searchParmeter.delPathOriginDC = result.origin_station;
                        _this.$scope.searchParmeter.delPathDestinationDC = result.destination_station;
                    }
                }, function (error) {
                    _this.errorHandlerService.HandleException(error, "GetDeliveryPath");
                });
            }
        };
        ShipmentTrackingController.prototype.getDate = function (date) {
            if (typeof date === "Date") {
                if (date.getFullYear() > 2500)
                    date = new Date(date.getFullYear() - 543, date.getMonth(), date.getDate());
                else
                    date = new Date(date.getFullYear(), date.getMonth(), date.getDate());
            }
            else if (typeof date === "string") {
                var arr = date.toString().split("/");
                if (parseInt(arr[2]) > 2500)
                    date = new Date(Date.UTC(parseInt(arr[2]) - 543, parseInt(arr[1]) - 1, parseInt(arr[0])));
                else
                    date = new Date(Date.UTC(parseInt(arr[2]), parseInt(arr[1]) - 1, parseInt(arr[0])));
            }
            return date;
        };
        ShipmentTrackingController.prototype.checkDate = function () {
            if (this.$scope.searchParmeter.PeriodFrom || this.$scope.searchParmeter.PeriodTo) {
                this.$scope.visibleMonth = true;
                this.selectedMonth = null;
                this.$scope.txtYear = null;
                this.$scope.monthList = [];
                //this.getMonth();
                if (this.localStorageService.get("localMonthList") != null) {
                    this.$scope.monthList = this.localStorageService.get("localMonthList");
                }
            }
            else {
                this.$scope.visibleMonth = false;
            }
            if (this.selectedMonth || this.$scope.txtYear) {
                this.$scope.visibleDatePicker = true;
            }
            else {
                this.$scope.visibleDatePicker = false;
            }
        };
        ShipmentTrackingController.prototype.validate = function () {
            var check = false;
            if (!this.$scope.searchParmeter.PeriodFrom && !this.$scope.searchParmeter.PeriodTo && (this.selectedMonth == null || this.selectedMonth == "")) {
                this.$scope.errorMessage = "Please enter a date range";
                check = true;
            }
            else if (!this.$scope.searchParmeter.PeriodFrom && this.$scope.searchParmeter.PeriodTo) {
                this.$scope.errorMessage = "Please enter Start Date";
                check = true;
            }
            else if (this.$scope.searchParmeter.PeriodFrom && !this.$scope.searchParmeter.PeriodTo) {
                this.$scope.errorMessage = "Please enter End Date";
                check = true;
            }
            else if (this.selectedMonth && (this.$scope.txtYear == null || this.$scope.txtYear == "")) {
                this.$scope.errorMessage = "Month and Year are required. Please enter values.";
                check = true;
            }
            else if (!this.selectedMonth && (this.$scope.txtYear != null && this.$scope.txtYear != "")) {
                this.$scope.errorMessage = "Month and Year are required. Please enter values.";
                check = true;
            }
            else if (this.$scope.searchParmeter.PeriodTo < this.$scope.searchParmeter.PeriodFrom) {
                this.$scope.errorMessage = "To Date should be greater than or equal to From Date.";
                check = true;
            }
            else {
                this.$scope.errorMessage = null;
                check = false;
            }
            if (this.$scope.errorMessage != null) {
                this.notifyMessagePopupErrMessage(this.$scope.errorMessage, true);
            }
            return check;
        };
        ShipmentTrackingController.prototype.notifyMessagePopupErrMessage = function (msg, isError) {
            if (isError) {
                this.dialogHelperService.ShowMessage("Warning", msg);
            }
            else {
                this.notificationService.Success(msg);
            }
        };
        ShipmentTrackingController.prototype.clearText = function () {
            if (this.$scope.txtPayerCode == null || this.$scope.txtPayerCode == "") {
                this.$scope.txtPayerName = null;
            }
            if (this.$scope.txtDestinationPostalCode == null || this.$scope.txtDestinationPostalCode == "") {
                this.$scope.txtDestinationPostalDescription = null;
            }
            if (this.$scope.txtDestinationStateProvince == null || this.$scope.txtDestinationStateProvince == "") {
                this.$scope.txtDestinationStateProvinceDescription = null;
            }
            if (this.$scope.txtStatusCode == null || this.$scope.txtStatusCode == "") {
                this.$scope.txtStatusDescription = null;
            }
            if (this.$scope.txtExceptionCode == null || this.$scope.txtExceptionCode == "") {
                this.$scope.txtExceptionDescription = null;
            }
        };
        //public getSelectedValues(): void {
        //    var selectedVal = $("#multiselect").val();
        //    for (var i = 0; i < selectedVal.length; i++) {
        //        this.innerFunc(i);
        //    }
        //}
        //public innerFunc(i: number): void {
        //    var selectedVal = $("#multiselect").val();
        //    setTimeout(function () {
        //        location.href = selectedVal[i];
        //    }, i * 5000);
        //}
        ShipmentTrackingController.prototype.GenerateReport = function () {
            //console.log("this.selectedMonth", this.selectedMonth);
            //console.log("this.$scope.searchParmeter.PeriodFrom", this.$scope.searchParmeter.PeriodFrom);
            //console.log("this.$scope.searchParmeter.PeriodTo", this.$scope.searchParmeter.PeriodTo);
            //console.log("this.selectedBookingType", this.selectedBookingType);
            //console.log("this.selectedPayerType", this.selectedPayerType);
            //console.log("this.selectedTranDate", this.selectedTranDate);
            //console.log("this.selectedRouteType", this.selectedRouteType);
            //console.log("this.selectedPathCode", this.selectedPathCode);
            //console.log("this.selectedOrigin", this.selectedOrigin);
            //console.log("this.selectedDestination", this.selectedDestination);
            //console.log("this.chkInvoiceIssue", this.$scope.chkInvoiceIssue);
            //console.log("this.chkMatchStatusCode", this.$scope.chkMatchStatusCode);
            //console.log("this.chkShowPackage", this.$scope.chkShowPackage);
            //console.log("txtYear", this.$scope.txtYear);
            //console.log("this.chkShipmentClose", this.$scope.chkShipmentClose);
            var result = false;
            result = this.validate();
            if (result != true) {
                if (this.selectedRouteType == "L" || this.selectedRouteType == "A") {
                    this.loadDeliveryPath();
                }
                else {
                    this.$scope.searchParmeter.delPathOriginDC = "";
                    this.$scope.searchParmeter.delPathDestinationDC = "";
                }
                this.prapereSearchParam();
                //console.log("this.$scope.searchParmeter", this.$scope.searchParmeter);
                this.loadReport();
            }
            else {
            }
            //var result: Interfaces.ISearchFilterView[] = [];
            //var condition: Interfaces.ISearchFilterView;
            //this.shipmentTrackingService.GetReport(this.$scope.searchParmeter)
            //    .then(result => {
            //        if (result) {
            //            var blob = Shared.FileHelper.Base64ToBlob(result[0].Base64StringData, result[0].ContentType);
            //            saveAs(blob, result[0].FileName);
            //        }
            //    }, (error) => {
            //        this.errorHandlerService.HandleException(error, "");
            //    });
        };
        ShipmentTrackingController.prototype.loadReport = function () {
            var _this = this;
            var result = [];
            var condition;
            //console.log("IN load this.$scope.searchParmeter", this.$scope.searchParmeter);          
            return this.shipmentTrackingService.GetReport(this.$scope.searchParmeter)
                .then(function (result) {
                if (result) {
                    var blob = Shared.FileHelper.Base64ToBlob(result[0].Base64StringData, result[0].ContentType);
                    saveAs(blob, result[0].FileName);
                }
            }, function (error) {
                _this.errorHandlerService.HandleException(error, "");
            });
        };
        ShipmentTrackingController.prototype.prapereSearchParam = function () {
            this.$scope.searchParmeter.userId = this.$scope.authentication.userName;
            if (this.selectedMonth) {
                this.$scope.searchParmeter.month = this.selectedMonth;
                if (this.$scope.txtYear) {
                    this.$scope.searchParmeter.year = this.$scope.txtYear.toString().trim();
                }
            }
            //console.log("this.$scope.searchParmeter.PeriodFrom", this.$scope.searchParmeter.PeriodFrom);
            //console.log("this.$scope.searchParmeter.PeriodTo", this.$scope.searchParmeter.PeriodTo);
            if (this.$scope.searchParmeter.PeriodFrom || this.$scope.searchParmeter.PeriodTo) {
                var startDate = this.$scope.searchParmeter.PeriodFrom != null ? this.$scope.searchParmeter.PeriodFrom : this.$scope.searchParmeter.PeriodTo;
                var endDate = this.$scope.searchParmeter.PeriodTo != null ? this.$scope.searchParmeter.PeriodTo : startDate;
                var stDate = this.getDate(startDate);
                var enDate = this.getDate(endDate);
                var dateStart = this.dateTimeHelperService.GetStartTimeOfDate(stDate);
                var dateEnd = this.dateTimeHelperService.GetEndTimeOfDate(enDate);
                var sDate = this.$filter("date")(dateStart, "dd/MM/yyyy HH:mm");
                var eDate = this.$filter("date")(dateEnd, "dd/MM/yyyy HH:mm");
                this.$scope.searchParmeter.PeriodFromFormat = sDate;
                this.$scope.searchParmeter.PeriodToFormat = eDate;
            }
            //if (this.$scope.searchParmeter.PeriodFrom && this.$scope.searchParmeter.PeriodTo) {
            //    var stDate: Date = this.getDate(this.$scope.searchParmeter.PeriodFrom);
            //    var enDate: Date = this.getDate(this.$scope.searchParmeter.PeriodTo);
            //    var dateStart: string = this.dateTimeHelperService.GetStartTimeOfDate(stDate);
            //    var dateEnd: string = this.dateTimeHelperService.GetEndTimeOfDate(enDate);
            //    var sDate: Date = this.$filter("date")(dateStart, "dd/MM/yyyy HH:mm");
            //    var eDate: Date = this.$filter("date")(dateEnd, "dd/MM/yyyy HH:mm");
            //    this.$scope.searchParmeter.PeriodFromFormat = sDate;
            //    this.$scope.searchParmeter.PeriodToFormat = eDate;
            //    console.log(this.$scope.searchParmeter.PeriodFromFormat);
            //    console.log(this.$scope.searchParmeter.PeriodToFormat);
            //}
            //else
            //{
            //    if (this.$scope.searchParmeter.PeriodFrom)
            //    {
            //        var stDate: Date = this.getDate(this.$scope.searchParmeter.PeriodFrom);
            //        var enDate: Date = this.getDate(this.$scope.searchParmeter.PeriodFrom);
            //        var dateStart: string = this.dateTimeHelperService.GetStartTimeOfDate(stDate);
            //        var dateEnd: string = this.dateTimeHelperService.GetEndTimeOfDate(enDate);
            //        var sDate: Date = this.$filter("date")(dateStart, "dd/MM/yyyy HH:mm");
            //        var eDate: Date = this.$filter("date")(dateEnd, "dd/MM/yyyy HH:mm");
            //        this.$scope.searchParmeter.PeriodFromFormat = sDate;
            //        this.$scope.searchParmeter.PeriodToFormat = eDate;
            //        console.log(this.$scope.searchParmeter.PeriodFromFormat);
            //        console.log(this.$scope.searchParmeter.PeriodToFormat);
            //    }
            //    else
            //    {
            //        var stDate: Date = this.getDate(this.$scope.searchParmeter.PeriodTo);
            //        var enDate: Date = this.getDate(this.$scope.searchParmeter.PeriodTo);
            //        var dateStart: string = this.dateTimeHelperService.GetStartTimeOfDate(stDate);
            //        var dateEnd: string = this.dateTimeHelperService.GetEndTimeOfDate(enDate);
            //        var sDate: Date = this.$filter("date")(dateStart, "dd/MM/yyyy HH:mm");
            //        var eDate: Date = this.$filter("date")(dateEnd, "dd/MM/yyyy HH:mm");
            //        this.$scope.searchParmeter.PeriodFromFormat = sDate;
            //        this.$scope.searchParmeter.PeriodToFormat = eDate;
            //        console.log(this.$scope.searchParmeter.PeriodFromFormat);
            //        console.log(this.$scope.searchParmeter.PeriodToFormat);
            //    }
            //}
            if (this.selectedTranDate) {
                this.$scope.searchParmeter.tran_date = this.selectedTranDate;
            }
            if (this.$scope.userDetail.User_Type == "C") {
                this.$scope.searchParmeter.payer_code = this.$scope.userDetail.payerid;
            }
            else {
                if (this.$scope.txtPayerCode) {
                    this.$scope.searchParmeter.payer_code = this.$scope.txtPayerCode.trim();
                }
            }
            if (this.selectedPayerType && this.selectedPayerType.length > 0) {
                this.$scope.searchParmeter.payer_type = this.selectedPayerType;
            }
            if (this.$scope.userDetail.User_Type != "C") {
                if (this.selectedRouteType) {
                    this.$scope.searchParmeter.route_type = this.selectedRouteType;
                }
            }
            else {
                if (this.selectedRouteType) {
                    this.$scope.searchParmeter.route_type = this.selectedRouteType;
                }
                if (this.selectedPathCode) {
                    this.$scope.searchParmeter.route_code = this.selectedPathCode;
                }
                if (this.selectedOrigin) {
                    this.$scope.searchParmeter.origin_dc = this.selectedOrigin;
                }
                if (this.selectedDestination) {
                    this.$scope.searchParmeter.destination_dc = this.selectedDestination;
                }
            }
            if (this.$scope.txtDestinationPostalCode) {
                this.$scope.searchParmeter.zip_code = this.$scope.txtDestinationPostalCode.trim();
            }
            if (this.$scope.txtDestinationStateProvince) {
                this.$scope.searchParmeter.state_code = this.$scope.txtDestinationStateProvince.trim();
            }
            if (this.selectedBookingType) {
                this.$scope.searchParmeter.booking_type = this.selectedBookingType;
            }
            if (this.$scope.txtConsignmentNo) {
                this.$scope.searchParmeter.consignment_no = this.$scope.txtConsignmentNo.trim();
            }
            if (this.$scope.txtBookingNo) {
                this.$scope.searchParmeter.booking_no = this.$scope.txtBookingNo.trim();
            }
            if (this.$scope.txtStatusCode) {
                this.$scope.searchParmeter.status_code = this.$scope.txtStatusCode.trim();
            }
            if (this.$scope.txtExceptionCode) {
                this.$scope.searchParmeter.exception_code = this.$scope.txtExceptionCode.trim();
            }
            if (this.chkShipmentClose == true) {
                this.$scope.searchParmeter.shipment_closed = "Y";
            }
            else {
                this.$scope.searchParmeter.shipment_closed = "N";
            }
            if (this.chkInvoiceIssue == true) {
                this.$scope.searchParmeter.shipment_invoiced = "Y";
            }
            else {
                this.$scope.searchParmeter.shipment_invoiced = "N";
            }
            if (this.chkMatchStatusCode == true) {
                if (this.$scope.txtStatusCode == "" || this.$scope.txtStatusCode == null) {
                    this.$scope.searchParmeter.track_all = "N";
                    if (this.chkShowPackage == true) {
                        this.$scope.chkPackageName = "ShipmentTrackingQueryRpt";
                    }
                    else {
                        this.$scope.chkPackageName = "ShipmentTrackingQueryRpt_NoDT";
                    }
                }
                else {
                    this.$scope.searchParmeter.track_all = "Y";
                    if (this.chkShowPackage == true) {
                        this.$scope.chkPackageName = "ShipmentTrackingQueryRptAll";
                    }
                    else {
                        this.$scope.chkPackageName = "ShipmentTrackingQueryRptAll_NoDT";
                    }
                }
            }
            else {
                this.$scope.searchParmeter.track_all = "N";
                if (this.chkShowPackage == true) {
                    this.$scope.chkPackageName = "ShipmentTrackingQueryRpt";
                }
                else {
                    this.$scope.chkPackageName = "ShipmentTrackingQueryRpt_NoDT";
                }
            }
            if (this.$scope.userDetail.User_Type != "C") {
                this.$scope.searchParmeter.RptType = "N";
            }
            else {
                this.$scope.searchParmeter.RptType = "C";
            }
            //set report name
            if (this.$scope.chkPackageName.trim() == "ShipmentTrackingQueryRpt") {
                if (this.$scope.searchParmeter.RptType.trim() == "C") {
                    this.$scope.searchParmeter.reportName = "ShipmentTrackingReport3PA.rpt";
                }
                else {
                    this.$scope.searchParmeter.reportName = "ShipmentTrackingReport.rpt";
                }
            }
            else if (this.$scope.chkPackageName.trim() == "ShipmentTrackingQueryRptAll") {
                if (this.$scope.searchParmeter.RptType.trim() == "C") {
                    this.$scope.searchParmeter.reportName = "ShipmentTrackingReport3PA.rpt";
                }
                else {
                    this.$scope.searchParmeter.reportName = "ShipmentTrackingReportAll_DT.rpt";
                }
            }
            else if (this.$scope.chkPackageName.trim() == "ShipmentTrackingQueryRpt_NoDT") {
                if (this.$scope.searchParmeter.RptType.trim() == "C") {
                    this.$scope.searchParmeter.reportName = "ShipmentTrackingReport3PA.rpt";
                }
                else {
                    this.$scope.searchParmeter.reportName = "ShipmentTrackingReport_NoDT.rpt";
                }
            }
            else if (this.$scope.chkPackageName.trim() == "ShipmentTrackingQueryRptAll_NoDT") {
                if (this.$scope.searchParmeter.RptType.trim() == "C") {
                    this.$scope.searchParmeter.reportName = "ShipmentTrackingReport3PA.rpt";
                }
                else {
                    this.$scope.searchParmeter.reportName = "ShipmentTrackingReportAll_NoDT.rpt";
                }
            }
        };
        ShipmentTrackingController.prototype.initOnDialog = function () {
            var _this = this;
            // DIALOG: PayerCodePopup
            this.$scope.$on("PayerCodePopup", function (event) {
                var data = [];
                for (var _i = 1; _i < arguments.length; _i++) {
                    data[_i - 1] = arguments[_i];
                }
                //this.$scope.bindRecipientDetail.lovZipCode = [];
                var selectedItem = data[0][1];
                _this.$scope.txtPayerCode = selectedItem.custid;
                _this.$scope.txtPayerName = selectedItem.cust_name;
                //this.$scope.bindRecipientDetail.cost_centre = selectedItem.cost_centre;
                //this.$scope.bindRecipientDetail.reference_name = selectedItem.reference_name;
                //this.getBindRecipientDetailByCostCentre(this.$scope.bindRecipientDetail.cost_centre);
                // this.setSessionStorage();
                // Hide Dialog
                var dialog = angular.element(document.querySelector("#" + "PayerCodePopup"));
                dialog.modal("hide");
                _this.lovHelperService.ClearLovParameter();
            });
            // DIALOG: StateCodePopup
            this.$scope.$on("StateCodePopup", function (event) {
                var data = [];
                for (var _i = 1; _i < arguments.length; _i++) {
                    data[_i - 1] = arguments[_i];
                }
                var selectedItem = data[0][1];
                _this.$scope.txtDestinationStateProvince = selectedItem.state_code;
                _this.$scope.txtDestinationStateProvinceDescription = selectedItem.state_name;
                // Hide Dialog
                var dialog = angular.element(document.querySelector("#" + "StateCodePopup"));
                dialog.modal("hide");
                _this.lovHelperService.ClearLovParameter();
            });
            // DIALOG: ZipCodePopup
            this.$scope.$on("ZipCodePopupSender", function (event) {
                var data = [];
                for (var _i = 1; _i < arguments.length; _i++) {
                    data[_i - 1] = arguments[_i];
                }
                var selectedItem = data[0][1];
                _this.$scope.txtDestinationPostalCode = selectedItem.zipcode;
                _this.$scope.txtDestinationPostalDescription = selectedItem.state_name;
                // Hide Dialog
                var dialog = angular.element(document.querySelector("#" + "ZipCodePopupSender"));
                dialog.modal("hide");
                _this.lovHelperService.ClearLovParameter();
            });
            // DIALOG: StatusCodePopup
            this.$scope.$on("StatusCodePopup", function (event) {
                var data = [];
                for (var _i = 1; _i < arguments.length; _i++) {
                    data[_i - 1] = arguments[_i];
                }
                var selectedItem = data[0][1];
                _this.$scope.txtStatusCode = selectedItem.status_code;
                _this.$scope.txtStatusDescription = selectedItem.status_description;
                // Hide Dialog
                var dialog = angular.element(document.querySelector("#" + "StatusCodePopup"));
                dialog.modal("hide");
                _this.lovHelperService.ClearLovParameter();
            });
            // DIALOG: ExceptionCodePopup
            this.$scope.$on("ExceptionCodePopup", function (event) {
                var data = [];
                for (var _i = 1; _i < arguments.length; _i++) {
                    data[_i - 1] = arguments[_i];
                }
                var selectedItem = data[0][1];
                _this.$scope.txtExceptionCode = selectedItem.exception_code;
                _this.$scope.txtExceptionDescription = selectedItem.exception_description;
                _this.$scope.txtStatusCode = selectedItem.status_code;
                _this.$scope.txtStatusDescription = selectedItem.status_description;
                // Hide Dialog
                var dialog = angular.element(document.querySelector("#" + "ExceptionCodePopup"));
                dialog.modal("hide");
                _this.lovHelperService.ClearLovParameter();
            });
        };
        ShipmentTrackingController.prototype.searchPayerCode = function (payerCode) {
            this.lovHelperService.CreatePayerCodeLov("PayerCodePopup");
            //console.log("this.selectedPayerType", this.selectedPayerType);
            this.lovHelperService.SetLovParameter({
                BroadcastName: "PayerCodePopup",
                SearchFilter: {
                    custid: payerCode ? payerCode : "",
                    cust_name: "",
                    zipcode: "",
                    applicationid: "TIES",
                    enterpriseid: "PNGAF",
                    status_active: "Y",
                    payer_type: this.selectedPayerType ? this.selectedPayerType : ""
                },
                IsMultipleSelected: false,
                IsHideSearchStatus: false,
                IsShowLock: false,
                Active: 1 // All:-1, Active:1, Inactive:0
            });
        };
        ShipmentTrackingController.prototype.searchZipCodeSender = function (zipcode) {
            this.lovHelperService.CreateShipmentZipCodeLov("ZipCodePopupSender");
            this.lovHelperService.SetLovParameter({
                BroadcastName: "ZipCodePopupSender",
                SearchFilter: {
                    zipcode: zipcode ? zipcode : "",
                    country: ""
                },
                IsMultipleSelected: false,
                IsHideSearchStatus: false,
                IsShowLock: false,
                Active: 1 // All:-1, Active:1, Inactive:0
            });
        };
        ShipmentTrackingController.prototype.searchStateCode = function (stateCode) {
            this.lovHelperService.CreateStateCodeLov("StateCodePopup");
            this.lovHelperService.SetLovParameter({
                BroadcastName: "StateCodePopup",
                SearchFilter: {
                    applicationid: "TIES",
                    enterpriseid: "PNGAF",
                    state_code: stateCode ? stateCode : "",
                    country: "",
                },
                IsMultipleSelected: false,
                IsHideSearchStatus: false,
                IsShowLock: false,
                Active: 1 // All:-1, Active:1, Inactive:0
            });
        };
        ShipmentTrackingController.prototype.searchStatusCode = function (statusCode) {
            this.lovHelperService.CreateStatusCodeLov("StatusCodePopup");
            this.lovHelperService.SetLovParameter({
                BroadcastName: "StatusCodePopup",
                SearchFilter: {
                    applicationid: "TIES",
                    enterpriseid: "PNGAF",
                    status_code: statusCode ? statusCode : "",
                    counstatus_descriptiontry: "",
                    culture: this.$scope.userDetail.user_culture,
                    userid: this.$scope.userDetail.userid,
                    allowRoles: this.$scope.userDetail.User_Type
                },
                IsMultipleSelected: false,
                IsHideSearchStatus: false,
                IsShowLock: false,
                Active: 1 // All:-1, Active:1, Inactive:0
            });
        };
        ShipmentTrackingController.prototype.searchExceptionCode = function (exceptionCode) {
            this.lovHelperService.CreateExceptionCodeLov("ExceptionCodePopup");
            this.lovHelperService.SetLovParameter({
                BroadcastName: "ExceptionCodePopup",
                SearchFilter: {
                    exception_code: exceptionCode ? exceptionCode : "",
                    status_code: this.$scope.txtStatusCode ? this.$scope.txtStatusCode : "",
                    culture: this.$scope.userDetail.user_culture,
                    userid: this.$scope.userDetail.userid
                },
                IsMultipleSelected: false,
                IsHideSearchStatus: false,
                IsShowLock: false,
                Active: 1 // All:-1, Active:1, Inactive:0
            });
        };
        ShipmentTrackingController.prototype.Reset = function () {
            this.$location.url(this.currentPath);
            this.$route.reload();
        };
        ShipmentTrackingController.$inject = ['$scope', '$location', '$route', 'authService',
            '$cookies', "$q", "$sessionStorage", "localStorageService", "shipmentTrackingService",
            "errorHandlerService", "lovHelperService", "dialogHelperService", "notificationService", "dateTimeHelperService", "$filter"];
        return ShipmentTrackingController;
    }());
    ShipmentTracking.ShipmentTrackingController = ShipmentTrackingController;
    Main.App.Controllers.controller("shipmentTrackingController", ShipmentTrackingController);
})(ShipmentTracking || (ShipmentTracking = {}));
//# sourceMappingURL=shipmentTrackingController.js.map