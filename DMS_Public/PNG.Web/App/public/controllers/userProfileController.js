var UserProfile;
(function (UserProfile) {
    var UserProfileController = (function () {
        function UserProfileController($scope, $location, authService, $cookies, $q, $sessionStorage, userProfileService, notificationService, dialogHelperService, localStorageService, $rootScope) {
            this.$scope = $scope;
            this.$location = $location;
            this.authService = authService;
            this.$cookies = $cookies;
            this.$q = $q;
            this.$sessionStorage = $sessionStorage;
            this.userProfileService = userProfileService;
            this.notificationService = notificationService;
            this.dialogHelperService = dialogHelperService;
            this.localStorageService = localStorageService;
            this.$rootScope = $rootScope;
            $scope.vm2 = this;
            this.$scope.user = {};
            //this.$scope.user.department_name = "Warehouse"
            this.$scope.newPassword = null;
            this.$scope.confirmPassword = null;
            this.$scope.authentication = this.localStorageService.get('authorizationData');
            if (this.$scope.authentication != null) {
                this.$scope.userId = this.$scope.authentication.userName;
            }
            var withOutAuth = [Shared.RouteNames.WelCome, Shared.RouteNames.EmailAccess, Shared.RouteNames.Reset, Shared.RouteNames.Unlock,
                Shared.RouteNames.Request, Shared.RouteNames.ShowMessage, Shared.RouteNames.Register, Shared.RouteNames.Login, Shared.RouteNames.RegisterUnlock];
            //if ($scope.authentication.isAuth == false)
            //{
            //    let checkPath = withOutAuth.filter(f => $location.path() == f).length;
            //    if (checkPath == 0 && this.currentPath.length > 1)
            //    {
            //        $scope.authentication.isAuth = true;
            //    }
            //    else
            //    {
            //        if (!$scope.authentication.isAuth)
            //        {
            //            this.setTimeOut(1000).then((result: boolean) =>
            //            {
            //                if (result)
            //                {
            //                    $location.url("/login");
            //                }
            //            });
            //        }
            //    }
            //} 
            this.initWelcome();
            this.GetUser();
        }
        UserProfileController.prototype.initWelcome = function () {
            console.log("Userprofile");
        };
        UserProfileController.prototype.GetUser = function () {
            var _this = this;
            //var userId = "bishops";
            //this.$scope.userId = "bishops";
            return this.userProfileService.getUserId(this.$scope.userId)
                .then(function (result) {
                _this.$scope.user = result;
            }, (function (error) {
                //console.log(error);
                //this.errorHandlerService.HandleException(error, "loadStatus");
            }));
        };
        UserProfileController.prototype.update = function () {
            var _this = this;
            //this.validate()
            //    .then((dialogMessage: string) =>
            //    {
            //        
            //        if (dialogMessage != null && dialogMessage)
            //        {
            //            console.log(dialogMessage);
            //        }
            //        else
            //        {
            return this.userProfileService.update(this.$scope.user)
                .then(function (result) {
                if (result) {
                    _this.notifyMessage("Save successfully", false);
                    setTimeout(function () {
                        _this.$rootScope.$broadcast("UserInfo");
                    }, 100);
                }
            }, (function (error) {
                //console.log(error);
                //this.errorHandlerService.HandleException(error, "loadStatus");
            }));
            //}
            //});
        };
        UserProfileController.prototype.validate = function () {
            var defer = this.$q.defer();
            var resultMessage = null;
            this.userProfileService.validate(this.$scope.user)
                .then(function (result) {
                if (result != null) {
                    resultMessage = result;
                    defer.resolve(result);
                }
            });
            return defer.promise;
        };
        UserProfileController.prototype.ResetPassword = function () {
            var _this = this;
            //this.$scope.user.user_name = "bishops";
            this.$scope.user.user_password = this.$scope.newPassword;
            return this.userProfileService.resetPassword(this.$scope.user)
                .then(function (result) {
                if (result) {
                    _this.notifyMessage("Save successfully", false);
                    _this.backToUserProfile();
                }
            }, (function (error) {
                //console.log(error);
                //this.errorHandlerService.HandleException(error, "loadStatus");
            }));
        };
        // NOTIFY MESSAGE
        UserProfileController.prototype.notifyMessage = function (msg, isError) {
            if (isError) {
                this.dialogHelperService.ShowMessage("Error", msg);
            }
            else {
                this.notificationService.Success(msg);
            }
        };
        UserProfileController.prototype.setTimeOut = function (milliSecond) {
            var result = false;
            var defer = this.$q.defer();
            setTimeout(function () {
                result = true;
                defer.resolve(result);
            }, milliSecond);
            return defer.promise;
        };
        UserProfileController.prototype.backToIndex = function () {
            this.$location.path("/");
        };
        UserProfileController.prototype.backToUserProfile = function () {
            this.$location.path("/userprofile");
        };
        UserProfileController.prototype.goToResetPassword = function () {
            this.$location.path("/resetpassword");
        };
        UserProfileController.$inject = ['$scope', '$location', 'authService',
            '$cookies', "$q", "$sessionStorage", "userProfileService", "notificationService", "dialogHelperService", "localStorageService", "$rootScope"];
        return UserProfileController;
    }());
    UserProfile.UserProfileController = UserProfileController;
    Main.App.Controllers.controller("userProfileController", UserProfileController);
})(UserProfile || (UserProfile = {}));
//# sourceMappingURL=userProfileController.js.map