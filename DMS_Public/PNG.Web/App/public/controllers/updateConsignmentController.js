var UpdateConsignment;
(function (UpdateConsignment) {
    var UpdateConsignmentController = (function () {
        function UpdateConsignmentController($rootScope, $scope, $location, authService, $cookies, configSettings, localStorageService, $sessionStorage, clientConnectionService, createConsignmentService, $filter, lovHelperService, notificationService, dialogHelperService, $routeParams, $route, $window) {
            this.$rootScope = $rootScope;
            this.$scope = $scope;
            this.$location = $location;
            this.authService = authService;
            this.$cookies = $cookies;
            this.configSettings = configSettings;
            this.localStorageService = localStorageService;
            this.$sessionStorage = $sessionStorage;
            this.clientConnectionService = clientConnectionService;
            this.createConsignmentService = createConsignmentService;
            this.$filter = $filter;
            this.lovHelperService = lovHelperService;
            this.notificationService = notificationService;
            this.dialogHelperService = dialogHelperService;
            this.$routeParams = $routeParams;
            this.$route = $route;
            this.$window = $window;
            this.isCheckedAll = false;
            this.defaultUrlPrefix = "/updateconsignment";
            this.consignmentno = "";
            $scope.vm = this;
            this.$scope.authentication = this.localStorageService.get('authorizationData');
            this.$scope.prepareParam = {};
            this.$scope.saveParam = {};
            this.$scope.deleteParam = {};
            this.$scope.updateParam = {};
            this.$scope.defaultServicecode = {};
            this.$scope.bindSenderDetail = {};
            this.$scope.bindRecipientDetail = {};
            this.$scope.senderName = [];
            this.$scope.serviceCode = [];
            this.$scope.packageDetail = [];
            this.$scope.packageDetailArray = [];
            this.$scope.senderCSSDetail = {};
            this.$scope.errorCSSDetail = {};
            this.$scope.bindStatus = {};
            this.$scope.updateRef = {};
            this.$scope.packageLimit = [];
            this.$sessionStorage.ListItem = null;
            this.$sessionStorage.ListItem = [];
            this.$scope.enterpriseContract = {};
            this.InitUpdateConsignment();
            this.initTooltip();
            this.initOnDialog();
            this.$scope.oldServiceCode = "";
            this.$scope.consignmentNo = null;
            this.$scope.dangerousGoods = 0;
            this.$scope.showDangerousGoods = false;
            this.$scope.showContractAccepted = false;
            this.$scope.dgText = "";
            this.$scope.dgTextClass = false;
            this.$scope.consignNoText = "";
            this.$scope.consignNoShow = false;
            this.$scope.hideSavebt = false;
            this.$scope.visibleFieldset = true;
            this.$scope.cusName = null;
            this.$scope.visibleConsignmentNo = false;
            this.$scope.packageText = "";
            this.$scope.pod_slip = "";
            this.$scope.invoiceHc = "";
            this.consignmentno = $routeParams.consignment != undefined ? $routeParams.consignment : null;
            //console.log("this.consignmentno",this.consignmentno);
            if (this.consignmentno) {
                this.$scope.consignmentNo = this.consignmentno;
            }
            //console.log("this.$scope.authentication", this.$scope.authentication);
            //console.log("this.$scope.authentication.userName", this.$scope.authentication.userName);
        }
        UpdateConsignmentController.prototype.InitUpdateConsignment = function () {
            var _this = this;
            this.getCustomerAccount().then(function () {
                _this.IsPermission();
                if (_this.$scope.userCustomerAccount.IsEnterpriseUser) {
                    _this.$scope.visible = false;
                }
                else {
                    _this.$scope.visible = false;
                    _this.$scope.visibleConsignmentNo = true;
                }
                _this.getSpCssConsignment().then(function () {
                    _this.IsPermission();
                    _this.getCustomerAcc2(_this.$scope.senderCSSDetail.payerid).then(function () {
                        //this.getBindService().then(
                        //() =>
                        //{
                        //    this.bindAllsenderAndrecipientInfo();
                        //});
                        _this.getBindService()
                            .then(function () { return _this.getReferenceDS()
                            .then(function () {
                            _this.bindAllsenderAndrecipientInfo();
                        }); });
                    });
                });
            });
            this.getPackageLimit();
        };
        UpdateConsignmentController.prototype.bindConsignmentText = function (isError, msg) {
            if (isError == true) {
                this.$scope.consignNoShow = true;
                this.$scope.consignNoText = msg;
            }
        };
        UpdateConsignmentController.prototype.prepareData = function () {
            var actionType = 0;
            if (this.$scope.userCustomerAccount.IsEnterpriseUser) {
                this.$scope.prepareParam.action = actionType;
                this.$scope.prepareParam.userloggedin = this.$scope.authentication.userName;
                this.$scope.prepareParam.consignment_no = this.$scope.consignmentNo;
            }
            else {
                this.$scope.prepareParam.action = actionType;
                this.$scope.prepareParam.userloggedin = this.$scope.authentication.userName;
                this.$scope.prepareParam.consignment_no = this.$scope.consignmentNo;
            }
            //console.log("this.$scope.prepareParam", this.$scope.prepareParam);
        };
        UpdateConsignmentController.prototype.getSpCssConsignment = function () {
            var _this = this;
            this.prepareData();
            if (this.$scope.prepareParam != null) {
                return this.createConsignmentService.getSpCSSConsignmentDetail(this.$scope.prepareParam)
                    .then(function (result) {
                    if (result.CreateConsignmentError != null) {
                        if (result.CreateConsignmentPackageDetail.length > 0) {
                            _this.$scope.packageDetail = result.CreateConsignmentPackageDetail;
                        }
                        if (result.CreateConsignmentSenderDetail.length > 0) {
                            _this.$scope.senderCSSDetail = result.CreateConsignmentSenderDetail[0];
                            _this.$scope.pod_slip = _this.$scope.senderCSSDetail.return_pod_slip;
                            _this.$scope.invoiceHc = _this.$scope.senderCSSDetail.return_invoice_hc;
                        }
                        if (result.CreateConsignmentError.length > 0) {
                            _this.$scope.errorCSSDetail = result.CreateConsignmentError[0];
                        }
                    }
                }, (function (error) {
                    console.error(error);
                }));
            }
        };
        UpdateConsignmentController.prototype.getCustomerAccount = function () {
            var _this = this;
            return this.createConsignmentService.getCustomerAccount(this.$scope.authentication.userName).then(function (result) {
                _this.$scope.userCustomerAccount = result;
                _this.$scope.cusNo = _this.$scope.userCustomerAccount.payerid;
                _this.$scope.cusName = _this.$scope.userCustomerAccount.cust_name;
            }, (function (error) {
                console.error(error);
            }));
        };
        UpdateConsignmentController.prototype.getCustomerAccountOnTextChanged = function (payerid) {
            var _this = this;
            if (payerid != null && payerid != "") {
                return this.createConsignmentService.getCustomerAccount2(payerid).then(function (result) {
                    if (result != null) {
                        _this.$scope.cusNo = result.custid;
                        _this.$scope.cusName = result.cust_name;
                    }
                }, (function (error) {
                    console.error(error);
                }));
            }
        };
        UpdateConsignmentController.prototype.getCustomerAcc2 = function (payerid) {
            var _this = this;
            if (payerid != null && payerid != "") {
                return this.createConsignmentService.getCustomerAccount2(payerid).then(function (result) {
                    if (result != null) {
                        //this.$scope.cusNo = result.custid;
                        _this.$scope.cusName = result.cust_name;
                    }
                }, (function (error) {
                    console.error(error);
                }));
            }
        };
        UpdateConsignmentController.prototype.getBindSender = function () {
            if (this.$scope.IsUserCustoms) {
                this.getCustomsWarehousesName();
            }
            else {
                this.getBindSenderDetail();
            }
        };
        UpdateConsignmentController.prototype.getBindSenderDetail = function () {
            var _this = this;
            return this.createConsignmentService.getBindSenderDetail(this.$scope.authentication.userName).then(function (result) {
                if (result != null) {
                    _this.$scope.bindSenderDetail = result;
                    _this.getSenderStateName(_this.$scope.bindSenderDetail.zipcode);
                }
            }, (function (error) {
                console.error(error);
            }));
        };
        UpdateConsignmentController.prototype.getCustomsWarehousesName = function () {
            var _this = this;
            return this.createConsignmentService.getCustomsWarehousesName(this.$scope.selectedValue).then(function (result) {
                if (result != null) {
                    _this.$scope.bindSenderDetail = result;
                }
            }, (function (error) {
                console.error(error);
            }));
        };
        UpdateConsignmentController.prototype.getReferenceDS = function () {
            var _this = this;
            return this.createConsignmentService.getCustomsWarehousesNameDefault(this.$scope.authentication.userName).then(function (result) {
                _this.$scope.senderName = result;
                //console.log(" this.$scope.senderName", this.$scope.senderName);
            }, (function (error) {
                console.error(error);
            }));
        };
        UpdateConsignmentController.prototype.getBindService = function () {
            var _this = this;
            return this.createConsignmentService.getServiceCode().then(function (result) {
                if (result) {
                    _this.$scope.serviceCode = result;
                }
                ;
            }, (function (error) {
                console.error(error);
            }));
        };
        UpdateConsignmentController.prototype.getSenderStateName = function (zipcode) {
            var _this = this;
            return this.createConsignmentService.getStateName(zipcode).then(function (resultStateName) {
                if (resultStateName != null) {
                    _this.$scope.bindSenderDetail.state_name = resultStateName.state_name;
                }
            }, (function (error) {
                console.error(error);
            }));
        };
        UpdateConsignmentController.prototype.getBindRecipientDetailByTelephone = function (telephone) {
            //this.$scope.bindRecipientDetail = {};
            //this.$scope.bindRecipientDetail.cost_centre = null;
            //this.$scope.bindRecipientDetail.zipcode = null;
            var _this = this;
            if (telephone != null && telephone != "") {
                return this.createConsignmentService.getRecipientDetailByTelephone(telephone).then(function (result) {
                    if (result.telephone != null && result.telephone != "") {
                        if (_this.$scope.bindRecipientDetail.cost_centre != null && _this.$scope.bindRecipientDetail.cost_centre != "") {
                            _this.$scope.bindRecipientDetail.telephone = result.telephone;
                        }
                        else {
                            _this.$scope.bindRecipientDetail = result;
                            _this.getRecipientStateName(_this.$scope.bindRecipientDetail.zipcode);
                        }
                    }
                }, (function (error) {
                    //console.error(error);
                }));
            }
        };
        UpdateConsignmentController.prototype.getRecipientStateName = function (zipcode) {
            var _this = this;
            return this.createConsignmentService.getStateName(zipcode).then(function (resultStateName) {
                if (resultStateName != null) {
                    _this.$scope.bindRecipientDetail.state_name = resultStateName.state_name;
                }
            }, (function (error) {
                console.error(error);
            }));
        };
        UpdateConsignmentController.prototype.initData = function () {
            this.$scope.packageDetail.forEach(function (item) {
                item.IsEdit = false;
                item.IsSelected = false;
            });
        };
        UpdateConsignmentController.prototype.AddRow = function () {
            var newItem;
            newItem = {};
            newItem.pkg_qty;
            newItem.pkg_wt;
            newItem.tot_wt;
            newItem.pkg_length;
            newItem.pkg_breadth;
            newItem.pkg_height;
            newItem.IsEdit = true;
            newItem.IsSelected = false;
            newItem.IsAdd = true;
            newItem.IsActive = true;
            newItem.TempPackage = [];
            this.$scope.packageDetail.push(newItem);
            this.$scope.checkEdit = true;
        };
        UpdateConsignmentController.prototype.SelectRow = function (packageItem) {
            if (angular.isUndefined(this.$sessionStorage.ListItem) || this.$sessionStorage.ListItem == null) {
                this.$sessionStorage.ListItem = [];
            }
            var valueChk = this.$sessionStorage.ListItem.filter(function (Item) { return Item == packageItem; }).length;
            // checked value in list ListItem session.
            if (valueChk == 0) {
                this.$sessionStorage.ListItem.push(packageItem);
            }
            else {
                // if unchecked will remove id from list ListItem session.
                if (!packageItem.IsSelected) {
                    var index = this.$sessionStorage.ListItem.indexOf(packageItem, 0);
                    if (index != undefined) {
                        this.$sessionStorage.ListItem.splice(index, 1);
                    }
                }
            }
            this.CheckSelectedItemAll();
        };
        UpdateConsignmentController.prototype.SelectAll = function () {
            var _this = this;
            var isCheck = false;
            this.$scope.packageDetail.forEach(function (item) {
                item.IsSelected = _this.isCheckedAll;
                if (_this.isCheckedAll == true) {
                    isCheck = true;
                    _this.$sessionStorage.CheckAll = _this.isCheckedAll;
                }
            });
            for (var i = 0; i < this.$scope.packageDetail.length; i++) {
                this.$scope.packageDetail[i].IsSelected = this.isCheckedAll;
                if (this.isCheckedAll == true) {
                    var index = this.$sessionStorage.ListItem.indexOf(this.$scope.packageDetail[i], 0);
                    if (index == -1) {
                        this.$sessionStorage.ListItem.push(this.$scope.packageDetail[i]);
                        isCheck = true;
                        this.$sessionStorage.CheckAll = this.isCheckedAll;
                    }
                }
            }
            if (!isCheck) {
                this.$sessionStorage.ListItem = [];
            }
            this.CheckSelectedItemAll();
        };
        UpdateConsignmentController.prototype.CheckSelectedItemAll = function () {
            this.isCheckedAll = this.$sessionStorage.CheckAll;
            if (this.$sessionStorage.ListItem) {
                if (this.$sessionStorage.ListItem.length != this.$scope.packageDetail.length) {
                    this.isCheckedAll = false;
                    this.$sessionStorage.CheckAll = this.isCheckedAll;
                }
                else {
                    this.$sessionStorage.CheckAll = true;
                    this.isCheckedAll = this.$sessionStorage.CheckAll;
                }
            }
        };
        UpdateConsignmentController.prototype.CheckDisableDeleteButton = function () {
            var resultCssClass = "icon-disable";
            if (this.$sessionStorage.ListItem) {
                this.$sessionStorage.ListItem.forEach(function (item) {
                    if (item) {
                        resultCssClass = "cursor-pointer";
                    }
                });
            }
            return resultCssClass;
        };
        UpdateConsignmentController.prototype.initTooltip = function () {
            this.$scope.tooltipAdd = "Add";
            this.$scope.tooltipRemove = "Remove";
            this.$scope.tooltipEdit = "Edit";
            this.$scope.tooltipSave = "Save";
            this.$scope.tooltipCancel = "Cancel";
        };
        UpdateConsignmentController.prototype.EditRowData = function (packageItem) {
            //this.LoadPackageChequeLayout(packageItem);
            packageItem.TempPackage = angular.copy(packageItem);
            packageItem.IsEdit = true;
            this.$scope.checkEdit = true;
        };
        UpdateConsignmentController.prototype.CancelRowData = function (packageItem, index) {
            this.$scope.validateMessggeArray = [];
            if (packageItem.IsAdd == true) {
                this.$scope.packageDetail.splice(index, 1);
            }
            else {
                this.$scope.packageDetail[index] = angular.copy(packageItem.TempPackage);
                //this.$scope.ListBankChequeLayout = angular.copy(this.$scope.BankChequeLayoutTemp);
                packageItem.IsEdit = false;
                packageItem.IsAdd = false;
            }
            this.$scope.checkEdit = false;
        };
        UpdateConsignmentController.prototype.RemoveData = function () {
            this.$scope.validateMessggeArray = [];
            for (var i = this.$scope.packageDetail.length - 1; i >= 0; i--) {
                if (this.$scope.packageDetail[i].IsSelected == true) {
                    //console.log(" index ", this.$scope.packageDetail.indexOf(this.$scope.packageDetail[i]));
                    this.$scope.packageDetail.splice(this.$scope.packageDetail.indexOf(this.$scope.packageDetail[i]), 1);
                }
            }
        };
        UpdateConsignmentController.prototype.SaveRowData = function (packageItem, index) {
            this.$scope.validateMessgge = "";
            this.$scope.validateMessggeArray = [];
            if (!packageItem) {
                console.log("packageItem is null");
            }
            else {
                this.$scope.checkEdit = false;
                var VolumeLimit = this.$scope.packageLimit.filter(function (f) { return f.key == "VolumeLimit"; });
                var WeightLimit = this.$scope.packageLimit.filter(function (f) { return f.key == "PkgWtLimit"; });
                var QuantityLimit = this.$scope.packageLimit.filter(function (f) { return f.key == "PkgQtyLimit"; });
                var RowLimit = this.$scope.packageLimit.filter(function (f) { return f.key == "PkgRowWtLimit"; });
                var DensityFactor = this.$scope.packageLimit.filter(function (f) { return f.key == "DensityFactor"; });
                //weight
                var wt = (packageItem.pkg_wt) ? packageItem.pkg_wt.toString().trim().replace(",", "") : null;
                if (parseFloat(wt) > parseFloat(WeightLimit[0].value)) {
                    this.$scope.validateMessgge = "Individual Package Weight exceeds the Enterprise limit.";
                    this.$scope.validateMessggeArray.push(this.$scope.validateMessgge);
                    this.$scope.packageDetail[index].pkg_wt = null;
                    this.$scope.packageDetail[index].IsEdit = true;
                }
                else if (parseFloat(wt) <= 0) {
                    this.$scope.packageDetail[index].pkg_wt = null;
                }
                else {
                    this.$scope.packageDetail[index].pkg_wt = parseFloat(wt);
                    this.$scope.packageDetail[index].IsEdit = false;
                }
                //quantity
                var qt = packageItem.pkg_qty ? packageItem.pkg_qty.toString().trim().replace(",", "") : null;
                if (parseInt(qt) > parseInt(QuantityLimit[0].value)) {
                    if (this.$scope.validateMessgge.length > 0) {
                    }
                    this.$scope.validateMessgge = "Total number of packages exceeds the Enterprise Limit.";
                    this.$scope.validateMessggeArray.push(this.$scope.validateMessgge);
                    this.$scope.packageDetail[index].pkg_qty = parseInt(qt);
                    this.$scope.packageDetail[index].IsEdit = true;
                }
                else if (parseInt(qt) <= 0 || qt == null) {
                    this.$scope.packageDetail[index].pkg_qty = null;
                }
                else {
                    this.$scope.packageDetail[index].pkg_qty = parseInt(qt);
                    this.$scope.packageDetail[index].IsEdit = false;
                }
                //total weight
                var ttwaight = (parseFloat(wt) * parseInt(qt));
                if (ttwaight > parseFloat(RowLimit[0].value)) {
                    if (this.$scope.validateMessgge.length > 0) {
                    }
                    this.$scope.validateMessgge = "Package Weight x Quantity exceeds the limit that can be saved.";
                    this.$scope.validateMessggeArray.push(this.$scope.validateMessgge);
                    this.$scope.packageDetail[index].tot_wt = ttwaight;
                    ////console.log("ttwaight", ttwaight);
                    this.$scope.packageDetail[index].IsEdit = true;
                }
                else if (ttwaight <= 0) {
                    this.$scope.packageDetail[index].tot_wt = null;
                }
                else {
                    this.$scope.packageDetail[index].tot_wt = ttwaight;
                    this.$scope.packageDetail[index].IsEdit = false;
                }
                //width
                var lengtH = (packageItem.pkg_length) ? packageItem.pkg_length.toString().trim().replace(",", "") : null;
                if (parseFloat(lengtH) != null && parseFloat(lengtH) > 0) {
                    this.$scope.packageDetail[index].pkg_length = parseFloat(lengtH);
                }
                else {
                    this.$scope.packageDetail[index].pkg_length = null;
                }
                //breadth
                var breadtH = (packageItem.pkg_breadth) ? packageItem.pkg_breadth.toString().trim().replace(",", "") : null;
                if (parseFloat(breadtH) != null && parseFloat(breadtH) > 0) {
                    this.$scope.packageDetail[index].pkg_breadth = parseFloat(breadtH);
                }
                else {
                    this.$scope.packageDetail[index].pkg_breadth = null;
                }
                //height
                var heighT = (packageItem.pkg_height) ? packageItem.pkg_height.toString().trim().replace(",", "") : null;
                if (parseFloat(heighT) != null && parseFloat(heighT) > 0) {
                    this.$scope.packageDetail[index].pkg_height = parseFloat(heighT);
                }
                else {
                    this.$scope.packageDetail[index].pkg_height = null;
                }
                if (((parseFloat(lengtH) * parseFloat(breadtH) * parseFloat(heighT)) * parseInt(qt)) > parseFloat(VolumeLimit[0].value)) {
                    if (this.$scope.validateMessgge.length > 0) {
                    }
                    this.$scope.validateMessgge = "Volume of package exceeds the Enterprise Limit.";
                    this.$scope.validateMessggeArray.push(this.$scope.validateMessgge);
                    this.$scope.packageDetail[index].IsEdit = true;
                }
                else {
                    this.$scope.packageDetail[index].IsEdit = false;
                }
                if ((((parseFloat(lengtH) * parseFloat(breadtH) * parseFloat(heighT)) * parseInt(qt)) / parseFloat(DensityFactor[0].value)) > parseFloat(RowLimit[0].value)) {
                    if (this.$scope.validateMessgge.length > 0) {
                    }
                    this.$scope.validateMessgge = "Package Weight x Quantity exceeds the limit that can be saved.";
                    this.$scope.validateMessggeArray.push(this.$scope.validateMessgge);
                    this.$scope.packageDetail[index].IsEdit = true;
                }
                else {
                    this.$scope.packageDetail[index].IsEdit = false;
                }
                if (parseFloat(wt) > parseFloat(WeightLimit[0].value) || ((parseFloat(lengtH) * parseFloat(breadtH) * parseFloat(heighT)) * parseInt(qt)) > parseFloat(VolumeLimit[0].value) || (((parseFloat(lengtH) * parseFloat(breadtH) * parseFloat(heighT)) * parseInt(qt)) / parseFloat(DensityFactor[0].value)) > parseFloat(RowLimit[0].value)) {
                    this.$scope.packageDetail[index].IsEdit = true;
                }
                else if (parseFloat(wt) <= 0) {
                    this.$scope.packageDetail[index].pkg_wt = null;
                }
                else {
                    this.$scope.packageDetail[index].IsEdit = false;
                }
                //quantity
                if (parseInt(qt) > parseInt(QuantityLimit[0].value) || ((parseFloat(lengtH) * parseFloat(breadtH) * parseFloat(heighT)) * parseInt(qt)) > parseFloat(VolumeLimit[0].value) || (((parseFloat(lengtH) * parseFloat(breadtH) * parseFloat(heighT)) * parseInt(qt)) / parseFloat(DensityFactor[0].value)) > parseFloat(RowLimit[0].value)) {
                    this.$scope.packageDetail[index].IsEdit = true;
                }
                else if (parseInt(qt) <= 0 || qt == null) {
                    this.$scope.packageDetail[index].pkg_qty = null;
                }
                else {
                    this.$scope.packageDetail[index].IsEdit = false;
                }
                //this.$scope.packageDetail[index].IsActive = true;
                this.$scope.packageDetail[index].IsSelected = false;
                //this.$scope.packageDetail[index].IsEdit = false;
                this.$scope.packageDetail[index].IsActive = false;
            }
        };
        UpdateConsignmentController.prototype.getPackageLimit = function () {
            var _this = this;
            return this.createConsignmentService.getPackageLimit(this.$scope.authentication.userName).then(function (result) {
                _this.$scope.packageLimit = result;
            }, (function (error) {
                console.error(error);
            }));
        };
        UpdateConsignmentController.prototype.SaveConsignment = function () {
            var _this = this;
            this.getConfiguration().then(function () {
                _this.getEnterpriseConTract().then(function () {
                    var DGGoodService = _this.$scope.configuration.filter(function (f) { return f.key == "DangerousGoodsService"; });
                    //this.dialogHelperService.ConfirmMessage("Warning", "Does this consignment contain dangerous goods?",
                    //    () => {
                    //        this.selectedServiceCode = DGGoodService[0].value;
                    //        this.$scope.dangerousGoods = 1;
                    //        this.$scope.dgText = "";
                    //        if (this.$scope.enterpriseContract.AcceptedCurrentContract != null && this.$scope.enterpriseContract.AcceptedCurrentContract == "1") {
                    //            this.SaveData();
                    //        }
                    //        else {
                    //            if (this.$scope.userCustomerAccount.IsEnterpriseUser) {
                    //                this.SaveData();
                    //            }
                    //            else {
                    //                this.SaveData();
                    //            }
                    //        }
                    //    },
                    //    () => {
                    //        this.$scope.saveParam.DangerousGoods = 0;
                    //        this.$scope.dangerousGoods = 0;
                    //        this.$scope.dgText = "";
                    //        if (this.$scope.enterpriseContract.AcceptedCurrentContract != null && this.$scope.enterpriseContract.AcceptedCurrentContract == "1") {
                    //            this.SaveData();
                    //        }
                    //        else {
                    //            if (this.$scope.userCustomerAccount.IsEnterpriseUser) {
                    //                this.SaveData();
                    //            }
                    //            else {
                    //                this.SaveData();
                    //            }
                    //        }
                    //    });
                    if (_this.$scope.enterpriseContract.AcceptedCurrentContract != null && _this.$scope.enterpriseContract.AcceptedCurrentContract == "1") {
                        _this.SaveData();
                    }
                    else {
                        if (_this.$scope.userCustomerAccount.IsEnterpriseUser) {
                            _this.SaveData();
                        }
                        else {
                            _this.SaveData();
                        }
                    }
                });
            });
        };
        // NOTIFY MESSAGE
        UpdateConsignmentController.prototype.notifyMessage = function (msg, isError) {
            if (isError) {
                this.dialogHelperService.ShowMessage("Error", msg);
            }
            else {
                this.notificationService.Success(msg);
            }
        };
        UpdateConsignmentController.prototype.getConfiguration = function () {
            var _this = this;
            this.$scope.configuration = [];
            return this.createConsignmentService.getConfiguration(this.$scope.authentication.userName).then(function (result) {
                _this.$scope.configuration = result;
            }, (function (error) {
                console.error(error);
            }));
        };
        UpdateConsignmentController.prototype.getEnterpriseConTract = function () {
            var _this = this;
            this.$scope.enterpriseContract = {};
            return this.createConsignmentService.getEnterpriseContract(this.$scope.authentication.userName).then(function (result) {
                if (result != null) {
                    _this.$scope.enterpriseContract = result;
                }
            }, (function (error) {
                //console.error('Cannot load getEnterpriseContract');
                console.log(error);
            }));
        };
        UpdateConsignmentController.prototype.onchangeServiceCode = function () {
        };
        UpdateConsignmentController.prototype.SaveData = function () {
            var _this = this;
            var actionType = 2;
            this.$scope.saveParam = {};
            this.$scope.saveParam.action = actionType;
            this.$scope.saveParam.userloggedin = this.$scope.authentication.userName;
            //if (this.$scope.userCustomerAccount.IsEnterpriseUser) {
            //    this.$scope.saveParam.payerid = this.$scope.cusNo;
            //    this.$scope.saveParam.consignment_no = this.$scope.consignmentNo;
            //}
            this.$scope.saveParam.payerid = this.$scope.cusNo;
            this.$scope.saveParam.consignment_no = this.$scope.consignmentNo;
            //if (this.$scope.showContractAccepted) {
            //    this.$scope.saveParam.ContractAccepted; /* when Click Accepted */
            //}
            //else {
            //    this.$scope.saveParam.ContractAccepted = null;
            //}
            this.$scope.saveParam.ContractAccepted = this.$scope.senderCSSDetail.ContractAccepted;
            var pkg = "";
            for (var i = 0; i < this.$scope.packageDetail.length; i++) {
                var line = "";
                if (this.$scope.packageDetail[i].pkg_qty != null && this.$scope.packageDetail[i].pkg_qty > 0) {
                    line += "" + this.$scope.packageDetail[i].pkg_qty.toString();
                }
                else {
                    line += "0";
                }
                var wt = (this.$scope.packageDetail[i].pkg_wt) ? this.$scope.packageDetail[i].pkg_wt.toString().trim().replace(",", "") : null;
                if (parseFloat(wt) != null && parseFloat(wt) > 0) {
                    line += "," + parseFloat(wt);
                }
                else {
                    line += ",0";
                }
                var length = (this.$scope.packageDetail[i].pkg_length) ? this.$scope.packageDetail[i].pkg_length.toString().trim().replace(",", "") : null;
                if (parseFloat(length) != null && parseFloat(length) > 0) {
                    line += "," + parseFloat(length);
                }
                var breadth = (this.$scope.packageDetail[i].pkg_breadth) ? this.$scope.packageDetail[i].pkg_breadth.toString().trim().replace(",", "") : null;
                if (parseFloat(breadth) != null && parseFloat(breadth) > 0) {
                    line += "," + parseFloat(breadth);
                }
                var height = (this.$scope.packageDetail[i].pkg_height) ? this.$scope.packageDetail[i].pkg_height.toString().trim().replace(",", "") : null;
                if (parseFloat(height) != null && parseFloat(height) > 0) {
                    line += "," + parseFloat(height);
                }
                if (pkg.length > 0) {
                    pkg = pkg + ";";
                }
                pkg += line;
            }
            this.$scope.saveParam.service_code = this.selectedServiceCode;
            this.$scope.saveParam.sender_name = this.selectedItem;
            this.$scope.saveParam.sender_address1 = this.$scope.bindSenderDetail.address1;
            this.$scope.saveParam.sender_address2 = this.$scope.bindSenderDetail.address2;
            this.$scope.saveParam.sender_zipcode = this.$scope.bindSenderDetail.zipcode;
            this.$scope.saveParam.sender_telephone = this.$scope.bindSenderDetail.telephone;
            this.$scope.saveParam.sender_fax = this.$scope.bindSenderDetail.fax;
            this.$scope.saveParam.sender_contact_person = this.$scope.bindSenderDetail.contact_person;
            this.$scope.saveParam.sender_email = this.$scope.bindSenderDetail.email;
            this.$scope.saveParam.recipient_telephone = this.$scope.bindRecipientDetail.telephone;
            this.$scope.saveParam.recipient_name = this.$scope.bindRecipientDetail.reference_name;
            this.$scope.saveParam.recipient_address1 = this.$scope.bindRecipientDetail.address1;
            this.$scope.saveParam.recipient_address2 = this.$scope.bindRecipientDetail.address2;
            this.$scope.saveParam.recipient_zipcode = this.$scope.bindRecipientDetail.zipcode;
            this.$scope.saveParam.recipient_fax = this.$scope.bindRecipientDetail.fax;
            this.$scope.saveParam.recipient_contact_person = this.$scope.bindRecipientDetail.contactperson;
            this.$scope.saveParam.return_pod_slip = this.$scope.pod_slip;
            this.$scope.saveParam.return_invoice_hc = this.$scope.invoiceHc;
            this.$scope.saveParam.PackageDetails = pkg;
            this.$scope.saveParam.ref_no = this.$scope.bindRecipientDetail.ref_no;
            this.$scope.saveParam.remark = this.$scope.bindRecipientDetail.remark;
            this.$scope.saveParam.GoodsDescription = this.$scope.bindRecipientDetail.GoodsDescription;
            this.$scope.saveParam.DangerousGoods = this.$scope.dangerousGoods;
            this.sendParamtoSave().then(function () {
                if (_this.$scope.errorCSSDetail.ErrorCode == 0) {
                    //this.$scope.dangerousGoods = -1;
                    _this.updateReferenceCreateConsignment().then(function () {
                        _this.$scope.hideSavebt = false;
                        var message = "Consignment saved";
                        _this.notifyMessage(message, false);
                        //this.$scope.adminModeHeader = true;
                        //this.$scope.adminModeSender = true;
                        //this.$scope.adminModeRecipient = true;
                        _this.bindAllsenderAndrecipientInfo(); //create will use reload page instead bindall recipient || bindAll will use in update consignment                        
                    });
                }
                else {
                    _this.notifyMessage(_this.$scope.errorCSSDetail.ErrorMessage, true);
                    _this.$scope.adminModeHeader = false;
                    //this.$scope.adminModeSender = true;
                    //this.$scope.adminModeRecipient = true;
                    _this.$scope.adminModeSender = false;
                    _this.$scope.adminModeRecipient = false;
                }
            });
        };
        UpdateConsignmentController.prototype.notifyMessagePopupConsignment = function (msg, isError) {
            var _this = this;
            if (isError) {
                this.dialogHelperService.ShowMessage2("Consignment No", msg, function () {
                    //Todo ok 
                    _this.backToIndex();
                });
            }
            else {
                this.notificationService.Success(msg);
            }
        };
        UpdateConsignmentController.prototype.sendParamtoSave = function () {
            var _this = this;
            //this.$scope.errorCSSDetail = {};
            //this.$scope.senderCSSDetail = {};
            return this.createConsignmentService.getSpCSSConsignmentDetail(this.$scope.saveParam)
                .then(function (result) {
                if (result.CreateConsignmentError != null) {
                    if (result.CreateConsignmentPackageDetail.length > 0) {
                        _this.$scope.packageDetail = result.CreateConsignmentPackageDetail;
                    }
                    if (result.CreateConsignmentSenderDetail.length > 0) {
                        _this.$scope.senderCSSDetail = result.CreateConsignmentSenderDetail[0];
                    }
                    if (result.CreateConsignmentError.length > 0) {
                        _this.$scope.errorCSSDetail = result.CreateConsignmentError[0];
                    }
                }
            }, (function (error) {
                console.error(error);
            }));
        };
        UpdateConsignmentController.prototype.updateReferenceCreateConsignment = function () {
            var _this = this;
            this.$scope.saveParam.cost_centre = this.$scope.bindRecipientDetail.cost_centre;
            return this.createConsignmentService.updateReferenceStoreProcedure(this.$scope.saveParam).then(function (result) {
                _this.$scope.updateRef = result;
            }, (function (error) {
                console.error(error);
            }));
        };
        UpdateConsignmentController.prototype.getDefaultServiceCode = function () {
            var _this = this;
            return this.createConsignmentService.getDefaultServiceCode(this.$scope.authentication.userName).then(function (result) {
                if (result != null) {
                    _this.$scope.defaultServicecode = result[0];
                }
            }, (function (error) {
                //console.error('Cannot load getDefaultServiceCode');
            }));
        };
        UpdateConsignmentController.prototype.bindAllsenderAndrecipientInfo = function () {
            this.$scope.cusNo = this.$scope.senderCSSDetail.payerid;
            this.$scope.bindStatus.Created_By = this.$scope.senderCSSDetail.Created_By;
            this.$scope.bindStatus.Created_DT = this.$scope.senderCSSDetail.Created_DT;
            this.$scope.bindStatus.Updated_By = this.$scope.senderCSSDetail.Updated_By;
            this.$scope.bindStatus.Updated_DT = this.$scope.senderCSSDetail.Updated_DT;
            this.$scope.bindStatus.last_status = this.$scope.senderCSSDetail.last_status;
            this.$scope.bindStatus.last_status_DT = this.$scope.senderCSSDetail.last_status_DT;
            this.$scope.userCustomerAccount.shippinglist = this.$scope.senderCSSDetail.ShippingList_no;
            //this.$scope.senderName = this.$scope.senderCSSDetail.sender_name;
            //sender
            this.$scope.bindSenderDetail.address1 = this.$scope.senderCSSDetail.sender_address1;
            this.$scope.bindSenderDetail.address2 = this.$scope.senderCSSDetail.sender_address2;
            this.$scope.bindSenderDetail.zipcode = this.$scope.senderCSSDetail.sender_zipcode;
            this.$scope.bindSenderDetail.state_name = this.$scope.senderCSSDetail.sender_state_name;
            this.$scope.bindSenderDetail.telephone = this.$scope.senderCSSDetail.sender_telephone;
            this.$scope.bindSenderDetail.fax = this.$scope.senderCSSDetail.sender_fax;
            this.$scope.bindSenderDetail.contact_person = this.$scope.senderCSSDetail.sender_contact_person;
            this.$scope.bindSenderDetail.email = this.$scope.senderCSSDetail.sender_email;
            //recipient
            this.$scope.bindRecipientDetail.telephone = this.$scope.senderCSSDetail.recipient_telephone;
            //this.$scope.bindRecipientDetail.cost_centre = this.$scope.senderCSSDetail.c;
            this.$scope.bindRecipientDetail.reference_name = this.$scope.senderCSSDetail.recipient_name;
            this.$scope.bindRecipientDetail.address1 = this.$scope.senderCSSDetail.recipient_address1;
            this.$scope.bindRecipientDetail.address2 = this.$scope.senderCSSDetail.recipient_address2;
            this.$scope.bindRecipientDetail.zipcode = this.$scope.senderCSSDetail.recipient_zipcode;
            this.$scope.bindRecipientDetail.state_name = this.$scope.senderCSSDetail.recipient_state_name;
            this.$scope.bindRecipientDetail.fax = this.$scope.senderCSSDetail.recipient_fax;
            this.$scope.bindRecipientDetail.contactperson = this.$scope.senderCSSDetail.recipient_contact_person;
            this.$scope.bindRecipientDetail.ref_no = this.$scope.senderCSSDetail.ref_no;
            this.$scope.bindRecipientDetail.remark = this.$scope.senderCSSDetail.remark;
            this.$scope.bindRecipientDetail.GoodsDescription = this.$scope.senderCSSDetail.GoodsDescription;
            this.selectedServiceCode = this.$scope.senderCSSDetail.service_code;
            this.selectedItem = this.$scope.senderCSSDetail.sender_name;
        };
        UpdateConsignmentController.prototype.bindDGText = function (isDG) {
            if (isDG == true) {
                this.$scope.dgTextClass = true;
                this.$scope.dgText = "Dangerous Goods Declaration: Shipper has declared that this consignment contains dangerous goods.";
            }
            else {
                this.$scope.dgTextClass = false;
                this.$scope.dgText = "Dangerous Goods Declaration: Shipper has declared that this consignment does not contain dangerous goods.";
            }
        };
        UpdateConsignmentController.prototype.delteConsignment = function () {
            var _this = this;
            if (this.$scope.userCustomerAccount.IsCustomsUser) {
                var actionType = 3;
                this.$scope.deleteParam = {};
                this.$scope.deleteParam.action = actionType;
                this.$scope.deleteParam.userloggedin = this.$scope.authentication.userName;
                this.$scope.deleteParam.ForceDelete = 0;
                this.$scope.deleteParam.consignment_no = this.$scope.consignmentNo;
                if (this.$scope.userCustomerAccount.IsEnterpriseUser) {
                    this.$scope.deleteParam.payerid = this.$scope.cusNo.toString().trim();
                }
                this.sendParamtoDelete().then(function () {
                    //console.log("this.$scope.errorCSSDetail", this.$scope.errorCSSDetail);
                    _this.dialogHelperService.ConfirmMessageDelete("Warning", _this.$scope.errorCSSDetail.ErrorMessage, function () {
                        var actionType = 3;
                        _this.$scope.deleteParam = {};
                        _this.$scope.deleteParam.action = actionType;
                        _this.$scope.deleteParam.userloggedin = _this.$scope.authentication.userName;
                        _this.$scope.deleteParam.ForceDelete = 1;
                        _this.$scope.deleteParam.consignment_no = _this.$scope.consignmentNo;
                        if (_this.$scope.userCustomerAccount.IsEnterpriseUser) {
                            _this.$scope.deleteParam.payerid = _this.$scope.cusNo.toString().trim();
                        }
                        _this.sendParamtoDelete().then(function () {
                            if (_this.$scope.errorCSSDetail.ErrorCode == 0) {
                                var message = "Consignment deleted";
                                _this.notifyMessage(message, false);
                                setTimeout(function () {
                                    _this.backToIndexNoFilter();
                                }, 3000);
                            }
                            else {
                                _this.notifyMessage(_this.$scope.errorCSSDetail.ErrorMessage, true);
                            }
                        });
                    }, function () {
                        // do nothing
                    });
                });
            }
            else {
                this.dialogHelperService.ConfirmMessageDelete("Warning", "Are you sure you want to delete this consignment?", function () {
                    var actionType = 3;
                    _this.$scope.deleteParam = {};
                    _this.$scope.deleteParam.action = actionType;
                    _this.$scope.deleteParam.userloggedin = _this.$scope.authentication.userName;
                    _this.$scope.deleteParam.ForceDelete = 1;
                    _this.$scope.deleteParam.consignment_no = _this.$scope.consignmentNo;
                    if (_this.$scope.userCustomerAccount.IsEnterpriseUser) {
                        _this.$scope.deleteParam.payerid = _this.$scope.cusNo.toString().trim();
                    }
                    _this.sendParamtoDelete().then(function () {
                        if (_this.$scope.errorCSSDetail.ErrorCode == 0) {
                            var message = "Consignment deleted";
                            _this.notifyMessage(message, false);
                            setTimeout(function () {
                                _this.backToIndexNoFilter();
                            }, 3000);
                        }
                        else {
                            _this.notifyMessage(_this.$scope.errorCSSDetail.ErrorMessage, true);
                        }
                    });
                }, function () {
                    // do nothing
                });
            }
        };
        UpdateConsignmentController.prototype.sendParamtoDelete = function () {
            var _this = this;
            //console.log(" this.$scope.deleteParam", this.$scope.deleteParam);
            return this.createConsignmentService.getSpCSSConsignmentDetail(this.$scope.deleteParam)
                .then(function (result) {
                if (result.CreateConsignmentError != null) {
                    if (result.CreateConsignmentPackageDetail.length > 0) {
                        _this.$scope.packageDetail = result.CreateConsignmentPackageDetail;
                    }
                    if (result.CreateConsignmentSenderDetail.length > 0) {
                        _this.$scope.senderCSSDetail = result.CreateConsignmentSenderDetail[0];
                    }
                    if (result.CreateConsignmentError.length > 0) {
                        _this.$scope.errorCSSDetail = result.CreateConsignmentError[0];
                    }
                }
            }, (function (error) {
                console.error(error);
            }));
        };
        UpdateConsignmentController.prototype.initOnDialog = function () {
            var _this = this;
            // DIALOG: ZipCodePopup
            this.$scope.$on("ZipCodePopup", function (event) {
                var data = [];
                for (var _i = 1; _i < arguments.length; _i++) {
                    data[_i - 1] = arguments[_i];
                }
                var selectedItem = data[0][1];
                _this.$scope.bindRecipientDetail.zipcode = selectedItem.zipcode;
                _this.getRecipientStateName(_this.$scope.bindRecipientDetail.zipcode);
                //this.setSessionStorage();
                // Hide Dialog
                var dialog = angular.element(document.querySelector("#" + "ZipCodePopup"));
                dialog.modal("hide");
                _this.lovHelperService.ClearLovParameter();
            });
            // DIALOG: ZipCodePopup
            this.$scope.$on("ZipCodePopupSender", function (event) {
                var data = [];
                for (var _i = 1; _i < arguments.length; _i++) {
                    data[_i - 1] = arguments[_i];
                }
                var selectedItem = data[0][1];
                _this.$scope.bindSenderDetail.zipcode = selectedItem.zipcode;
                _this.$scope.bindSenderDetail.state_name = selectedItem.state_code;
                _this.getSenderStateName(_this.$scope.bindSenderDetail.zipcode);
                //this.setSessionStorage();
                // Hide Dialog
                var dialog = angular.element(document.querySelector("#" + "ZipCodePopupSender"));
                dialog.modal("hide");
                _this.lovHelperService.ClearLovParameter();
            });
            // DIALOG: CostCentrePopup
            this.$scope.$on("CostCentrePopup", function (event) {
                var data = [];
                for (var _i = 1; _i < arguments.length; _i++) {
                    data[_i - 1] = arguments[_i];
                }
                _this.$scope.bindRecipientDetail.lovZipCode = [];
                var selectedItem = data[0][1];
                _this.$scope.bindRecipientDetail.cost_centre = selectedItem.cost_centre;
                _this.$scope.bindRecipientDetail.reference_name = selectedItem.reference_name;
                _this.getBindRecipientDetailByCostCentre(_this.$scope.bindRecipientDetail.cost_centre);
                //this.setSessionStorage();
                // Hide Dialog
                var dialog = angular.element(document.querySelector("#" + "CostCentrePopup"));
                dialog.modal("hide");
                _this.lovHelperService.ClearLovParameter();
            });
        };
        UpdateConsignmentController.prototype.getBindRecipientDetailByCostCentre = function (costcentre) {
            var _this = this;
            this.$scope.bindRecipientDetail = {};
            this.$scope.bindRecipientDetail.cost_centre = null;
            this.$scope.bindRecipientDetail.zipcode = null;
            if (costcentre != null && costcentre != "") {
                return this.createConsignmentService.getRecipientDetailByCostCentre(costcentre).then(function (result) {
                    if (result != null) {
                        _this.$scope.bindRecipientDetail = result;
                        _this.getRecipientStateName(_this.$scope.bindRecipientDetail.zipcode);
                    }
                }, (function (error) {
                    console.error(error);
                }));
            }
        };
        // Search ListOfView
        UpdateConsignmentController.prototype.searchZipCode = function (zipcode) {
            this.lovHelperService.CreateZipCodeLov("ZipCodePopup");
            this.lovHelperService.SetLovParameter({
                BroadcastName: "ZipCodePopup",
                SearchFilter: {
                    zipcode: zipcode ? zipcode : "",
                    country: "PAPUA NEW GUINEA"
                },
                IsMultipleSelected: false,
                IsHideSearchStatus: false,
                IsShowLock: false,
                Active: 1 // All:-1, Active:1, Inactive:0
            });
        };
        // Search ListOfView
        UpdateConsignmentController.prototype.searchZipCodeSender = function (zipcode) {
            this.lovHelperService.CreateZipCodeLov("ZipCodePopupSender");
            this.lovHelperService.SetLovParameter({
                BroadcastName: "ZipCodePopupSender",
                SearchFilter: {
                    zipcode: zipcode ? zipcode : "",
                    country: "PAPUA NEW GUINEA"
                },
                IsMultipleSelected: false,
                IsHideSearchStatus: false,
                IsShowLock: false,
                Active: 1 // All:-1, Active:1, Inactive:0
            });
        };
        //Search ListOfView
        UpdateConsignmentController.prototype.searchCostCentre = function (costCentre) {
            this.lovHelperService.CreateCostCentreLov("CostCentrePopup");
            this.lovHelperService.SetLovParameter({
                BroadcastName: "CostCentrePopup",
                SearchFilter: {
                    telephone: "",
                    cost_centre: costCentre ? costCentre : "",
                    reference_name: "",
                },
                IsMultipleSelected: false,
                IsHideSearchStatus: false,
                IsShowLock: false,
                Active: 1 // All:-1, Active:1, Inactive:0
            });
        };
        UpdateConsignmentController.prototype.IsPermission = function () {
            if (this.$scope.userCustomerAccount.IsEnterpriseUser) {
                if (this.$scope.errorCSSDetail.ErrorCode == 0) {
                    this.$scope.adminModeHeader = true;
                    this.$scope.adminModeSender = false;
                    this.$scope.adminModeRecipient = false;
                    this.$scope.adminModePackage = true;
                }
                if (this.$scope.errorCSSDetail.ErrorCode == 29) {
                    this.$scope.adminModeHeader = false;
                    this.$scope.adminModeSender = true;
                    this.$scope.adminModeRecipient = true;
                    this.$scope.adminModePackage = true;
                }
            }
            else {
                this.$scope.adminModeHeader = true;
                this.$scope.adminModeSender = false;
                this.$scope.adminModeRecipient = false;
                this.$scope.adminModePackage = false;
            }
        };
        UpdateConsignmentController.prototype.backToIndex = function () {
            //this.$location.url("#/searchconsignment");
            this.$window.location.href = "#/searchconsignment?filter=1";
            //this.$route.reload();
        };
        UpdateConsignmentController.prototype.backToIndexNoFilter = function () {
            //this.$location.url("#/searchconsignment");
            this.$window.location.href = "#/searchconsignment";
            //this.$route.reload();
        };
        UpdateConsignmentController.prototype.getInvalid = function (item) {
            console.log(item);
        };
        UpdateConsignmentController.$inject = ["$rootScope", "$scope", "$location", "authService",
            "$cookies", "configSettings",
            "localStorageService",
            "$sessionStorage", "clientConnectionService", "createConsignmentService", "$filter", "lovHelperService", "notificationService", "dialogHelperService", "$routeParams", "$route", "$window"];
        return UpdateConsignmentController;
    }());
    UpdateConsignment.UpdateConsignmentController = UpdateConsignmentController;
    Main.App.Controllers.controller("updateConsignmentController", UpdateConsignmentController);
})(UpdateConsignment || (UpdateConsignment = {}));
//# sourceMappingURL=updateConsignmentController.js.map