﻿module Login
{
    export interface ILoginControllerScope extends ng.IScope
    {
        vm: LoginController;
        loginData: Interfaces.ILoginData;
        myHTML: any;
        IsAlert: boolean;
        IsUserFail: boolean;
        IsPassWordFail: boolean;
        authentication: any;
        messageError: string;
        navTopMenu: Interfaces.IMenu;
    }

    export class LoginController
    {
        public static $inject = ["$scope", "$rootScope", "$location", "authService", "configSettings", "errorHandlerService",
            "localStorageService", "dialogHelperService", "clientConnectionService", "$q", "$route", "$sessionStorage", "homeService"];

        private userFail: string;
        private passwordFail: string;
        private isDownLoad: boolean = false;
        private downloadTxt: string = '';
        private isAuth: boolean;
        constructor(private $scope: ILoginControllerScope,
            private $rootScope: ng.IScope,
            private $location: ng.ILocationService,
            private authService: Util.AuthService,
            private configSettings: Interfaces.IConfigSettings,
            private errorHandlerService: Shared.ErrorHandlerService,
            private localStorageService: ng.localStorage.ILocalStorageService,
            private dialogHelperService: Shared.DialogHelperService,
            private clientConnectionService: Util.ClientConnectionService,
            private $q: ng.IQService,
            private $route: any,
            private $sessionStorage: any,
            private homeService: Home.HomeService
        )
        {
           
            $scope.vm = this;
            $scope.loginData = {
                userName: "",
                password: "",
                companyId: "",
                useRefreshTokens: false,
                email: "",
                enterpriseid: "",
                Validator: Validation.Validator.For<Interfaces.ILoginData>(Validation.DefaultValidationRules.LoginData)
            };
            this.$scope.loginData.enterpriseid = "PNGAF"
            if (this.localStorageService.get('authorizationData') != undefined)
            {
                this.$location.url("/trackandtrace");
            }
            //  if auth equal true redirect welcome.
        }

        private Reset(): void
        {
            this.$scope.loginData.password = "";
            angular.element(document.querySelector('#hide_txtpass')).val("");
        }

        public login()
        {
            this.localStorageService.remove("RecentNumber");
            this.authService.Login(this.$scope.loginData)
                .then((response: any) =>
                {
                    if (!response.error)
                    {
                        var authData = this.localStorageService.get('authorizationData');
                        this.$location.url("/trackandtrace");
                        setTimeout(() =>
                        {
                            this.$rootScope.$broadcast("UserInfo");
                        }, 100);

                        this.$route.reload();
                    }
                    else
                    {
                        this.Reset();
                        this.$scope.messageError = "Username or password is incorrect.";
                        //this.dialogHelperService.ShowMessage("MessageErrorExplan", "MessageLoginError");
                    }
                }, ((error) =>
                {
                    // TODO: Disable autocomplete for password input
                    //this.Reset();
                    this.$scope.messageError = "Username or password is incorrect.";
                    //this.errorHandlerService.HandleException(error, "login", this.$scope.loginData.userName);
                }));
        }

    }

    Main.App.Controllers.controller("loginController", LoginController);
}