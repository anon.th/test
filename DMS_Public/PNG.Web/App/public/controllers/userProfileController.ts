﻿module UserProfile
{
    export interface IUserProfileControllerScope extends ng.IScope
    {
        vm2: UserProfileController;
        authentication: any;
        user: Interfaces.IUserMaster;
        newPassword: string;
        confirmPassword: string;
        comparePassword: boolean;
        resultPassword: string;
        userId: string;
    }

    export class UserProfileController
    {
        public static $inject = ['$scope', '$location', 'authService',
            '$cookies', "$q", "$sessionStorage", "userProfileService", "notificationService", "dialogHelperService", "localStorageService", "$rootScope"];

        public currentPath: string;
        public IsShowMenu: boolean;

        constructor(private $scope: IUserProfileControllerScope,
            private $location: ng.ILocationService,
            private authService: Util.AuthService,
            private $cookies: ng.cookies.ICookiesService,
            private $q: ng.IQService,
            private $sessionStorage: any,
            private userProfileService: UserProfile.UserProfileService,
            private notificationService: Shared.NotificationService,
            private dialogHelperService: Shared.DialogHelperService,
            private localStorageService: any,
            private $rootScope: ng.IScope)
        {
            $scope.vm2 = this;
            this.$scope.user = {};
            //this.$scope.user.department_name = "Warehouse"
            this.$scope.newPassword = null;
            this.$scope.confirmPassword = null;

            this.$scope.authentication = this.localStorageService.get('authorizationData');
            if (this.$scope.authentication != null)
            {
                this.$scope.userId = this.$scope.authentication.userName;
            }


            var withOutAuth: string[] = [Shared.RouteNames.WelCome, Shared.RouteNames.EmailAccess, Shared.RouteNames.Reset, Shared.RouteNames.Unlock,
                Shared.RouteNames.Request, Shared.RouteNames.ShowMessage, Shared.RouteNames.Register, Shared.RouteNames.Login, Shared.RouteNames.RegisterUnlock];

            //if ($scope.authentication.isAuth == false)
            //{
            //    let checkPath = withOutAuth.filter(f => $location.path() == f).length;
            //    if (checkPath == 0 && this.currentPath.length > 1)
            //    {
            //        $scope.authentication.isAuth = true;
            //    }
            //    else
            //    {
            //        if (!$scope.authentication.isAuth)
            //        {
            //            this.setTimeOut(1000).then((result: boolean) =>
            //            {
            //                if (result)
            //                {
            //                    $location.url("/login");
            //                }
            //            });
            //        }
            //    }
            //} 

            this.initWelcome();
            this.GetUser();
        }

        public initWelcome() 
        {
            console.log("Userprofile");

        }

        public GetUser() 
        {
            //var userId = "bishops";
            //this.$scope.userId = "bishops";
            return this.userProfileService.getUserId(this.$scope.userId)
                .then((result: Interfaces.IUserMaster) =>
                {
                    this.$scope.user = result;
                }, ((error) =>
                {
                    //console.log(error);
                    //this.errorHandlerService.HandleException(error, "loadStatus");
                }));
        }

        public update()
        {
            //this.validate()
            //    .then((dialogMessage: string) =>
            //    {
            //        
            //        if (dialogMessage != null && dialogMessage)
            //        {
            //            console.log(dialogMessage);
            //        }
            //        else
            //        {
            return this.userProfileService.update(this.$scope.user)
                .then((result: boolean) =>
                {
                    if (result)
                    {
                        this.notifyMessage("Save successfully", false);
                        setTimeout(() =>
                        {
                            this.$rootScope.$broadcast("UserInfo");
                        }, 100);
                    }
                }
                , ((error) =>
                {
                    //console.log(error);
                    //this.errorHandlerService.HandleException(error, "loadStatus");
                }));
            //}
            //});
        }

        public validate(): ng.IPromise<string>
        {
            var defer = this.$q.defer<string>();
            var resultMessage: string = null;
            this.userProfileService.validate(this.$scope.user)
                .then((result: string) =>
                {
                    if (result != null)
                    {
                        resultMessage = result;
                        defer.resolve(result);
                    }
                });

            return defer.promise;
        }

        public ResetPassword()
        {
            //this.$scope.user.user_name = "bishops";
            this.$scope.user.user_password = this.$scope.newPassword;
            return this.userProfileService.resetPassword(this.$scope.user)
                .then((result: boolean) =>
                {
                    if (result)
                    {
                        this.notifyMessage("Save successfully", false);
                        this.backToUserProfile();
                    }
                }
                , ((error) =>
                {
                    //console.log(error);
                    //this.errorHandlerService.HandleException(error, "loadStatus");
                }));
        }

        // NOTIFY MESSAGE
        private notifyMessage(msg: string, isError: boolean): void
        {
            if (isError)
            {
                this.dialogHelperService.ShowMessage("Error", msg);
            }
            else
            {
                this.notificationService.Success(msg);
            }
        }


        public setTimeOut(milliSecond: number): ng.IPromise<boolean>
        {
            var result = false;
            var defer = this.$q.defer<boolean>();

            setTimeout(function ()
            {
                result = true;
                defer.resolve(result);
            }, milliSecond);

            return defer.promise;
        }

        public backToIndex()
        {
            this.$location.path("/");
        }

        public backToUserProfile()
        {
            this.$location.path("/userprofile");
        }
        public goToResetPassword()
        {
            this.$location.path("/resetpassword");
        }
    }

    Main.App.Controllers.controller("userProfileController", UserProfileController);
}