﻿module Home 
{
    export interface IHomeControllerScope extends ng.IScope 
    {
        vm: HomeController;
        authentication: any;
        UserInfo: Interfaces.IUserInfo;
        ParentSubMenus: Interfaces.IUserMenu[];
        MenuSub: Interfaces.IUserMenu[];
        Menu: Interfaces.IUserMenu[];
        Roles: Interfaces.IExternalIdentityUserRoles[];
        IsCollapsed: boolean;
        CurrentMenuId: number;
        MenuUrl: string;
        currentPath: string;

        WithOutLoginPage: boolean;
        IsAlert: boolean;

        //in export interface
        navleft: any
        navright: any[];
        navlast: any[];
        status: any;
        RecentNumber: string[];
        RecentNumberArray: string[];
        trackingNumber: string;
        trackingNumberRecent: string;
        trackingNumberArray: string[];
        trackAndTraceHeader: Interfaces.ITrackAndTraceHeader;
        trackAndTraceDetailTimeline: Interfaces.ITrackAndTraceDetail[];
        trackAndTraceDetailTable: Interfaces.ITrackAndTraceDetail[];
        userId: string;
        user: Interfaces.IUserMaster;
        navTopMenu: Interfaces.IMenu;
    }



    export class HomeController 
    {
        public static $inject = ["$rootScope", "$scope", "$location", "authService",
            "$cookies", "configSettings",
            "localStorageService",
            "$sessionStorage", "clientConnectionService", "trackandtraceService", "userProfileService", "homeService","$route"];

        constructor(private $rootScope: ng.IScope,
            private $scope: IHomeControllerScope,
            private $location: ng.ILocationService,
            private authService: Util.AuthService,
            private $cookies: ng.cookies.ICookiesService,
            private configSettings: Interfaces.IConfigSettings,
            private localStorageService: any,
            private $sessionStorage: any,
            private clientConnectionService: Util.ClientConnectionService,
            private trackandtraceService: TrackandTrace.TrackandTraceService,
            private userProfileService: UserProfile.UserProfileService,
            private homeService: Home.HomeService,
            private $route: any)
        {
            $scope.vm = this;
            //this.InitHome();
            $scope.authentication = authService.authentication;
            this.$scope.authentication = this.localStorageService.get('authorizationData');
            //this.$scope.currentPath = this.$location.absUrl;
            this.$scope.currentPath = $location.path();
            //console.log(window.location.pathname);
            //console.log(this.$scope.currentPath);
            //get usermenu from localstorage to UserInfo
            authService.FillAuthData()
                .then(() =>
                {
                    this.$scope.UserInfo = this.authService.userInfo;
                    this.$sessionStorage.UserName = this.$scope.UserInfo.UserName;
                }).then(() =>
                {
                    if (this.localStorageService.get('authorizationData') != null)
                    {
                        this.getMenu(this.localStorageService.get('authorizationData').userName)
                            .then(() =>
                            {
                                this.$scope.authentication = this.localStorageService.get('authorizationData');
                            });
                    }
                });

            //if (this.$scope.authentication != null)
            //{
            //this.$scope.userId = this.$scope.authentication.userName;
          
            //}

            //get usermenu from database
            this.$scope.$on("UserInfo", (event, ...data: any[]) =>
            {

                //this.getMenu();
                if (this.localStorageService.get('authorizationData') != null)
                {
                    this.$scope.authentication = this.localStorageService.get('authorizationData');
                    this.getUserInfo(this.localStorageService.get('authorizationData').userName).then(() =>
                    {

                        this.getMenu(this.localStorageService.get('authorizationData').userName);
                    });
                }



            });

            //get usermenu from database
            this.$scope.$on("Logout", (event, ...data: any[]) =>
            {
                this.logOutbyTimeOut();
            });

        }

        public clearSearch() 
        {
            this.$scope.trackAndTraceHeader = null;
            this.$scope.trackAndTraceDetailTable = [];
            this.$scope.trackingNumberArray = [];
        }

        public getMenu(userName: string): ng.IPromise<void>
        {
            var parentId = "06Customer Self Service";
            //this.$scope.userId = "puma";
            return this.homeService.getAllmodules(userName, parentId)
                .then((result: Interfaces.IMenu) =>
                {
                    if (result != null)
                    {
                        this.$scope.navTopMenu = result;
                        //console.log(" this.$scope.navTopMenu", this.$scope.navTopMenu);
                        //for (var i in this.$scope.navTopMenu)
                        //{
                        //    if (this.$scope.navTopMenu[i].child_name == "Create/Update Consignment")
                        //    {
                        //        this.$scope.navTopMenu[1].child_name = "Create Consignment";
                        //    }
                        //}
                           
                       // this.$scope.navTopMenu[1].child_name = "Create Consignment";
                           
                        
                    }
                }
                , ((error) =>
                {
                    console.log("error => ", error);
                }));
        }

        public logOutbyTimeOut()
        {
            this.authService.LogOut();
            this.$scope.authentication = null;
            this.$location.url('/trackandtrace');
            this.$scope.navTopMenu = null;
            this.localStorageService.remove("RecentNumber");
            this.$route.reload();
        }


        public logOut()
        {
            this.authService.LogOut();
            this.$scope.authentication = null;
            this.$location.url('/login');
            this.$scope.navTopMenu = null;
            this.localStorageService.remove("RecentNumber");
        }

        public getUserInfo(userId: string) 
        {
            return this.userProfileService.getUserId(userId)
                .then((result: Interfaces.IUserMaster) =>
                {
                    this.$scope.user = result;
                    this.localStorageService.set("UserInfo", this.$scope.user);

                }, ((error) =>
                {
                    //this.authService.LogOut();
                    console.log("error", error);
                    //this.errorHandlerService.HandleException(error, "loadStatus");
                }));
        }
    }

    Main.App.Controllers.controller("homeController", HomeController);
}