var TrackAndTrace;
(function (TrackAndTrace) {
    var TrackAndTraceController = (function () {
        function TrackAndTraceController($rootScope, $scope, $location, authService, $cookies, configSettings, localStorageService, $sessionStorage, clientConnectionService, trackandtraceService, $q, $q2) {
            this.$rootScope = $rootScope;
            this.$scope = $scope;
            this.$location = $location;
            this.authService = authService;
            this.$cookies = $cookies;
            this.configSettings = configSettings;
            this.localStorageService = localStorageService;
            this.$sessionStorage = $sessionStorage;
            this.clientConnectionService = clientConnectionService;
            this.trackandtraceService = trackandtraceService;
            this.$q = $q;
            this.$q2 = $q2;
            $scope.vm = this;
            this.$scope.userId = null;
            this.$scope.expandPackageDetails = false;
            this.$scope.authentication = this.localStorageService.get('authorizationData');
            if (this.$scope.authentication != null) {
                this.$scope.userId = this.$scope.authentication.userName;
            }
            this.$scope.visibleCusRefTextArea = false;
            this.$scope.visibleTrackNumberTextArea = false;
            this.$scope.RecentNumbersArray = [];
            this.$scope.trackandtraceParam = {};
            this.$scope.trackAndTraceDetailTimeline = [];
            this.Initial();
        }
        TrackAndTraceController.prototype.Initial = function () {
            this.$scope.RecentNumbers = [];
            if (this.localStorageService.get("RecentNumber") != null) {
                this.$scope.RecentNumbersArray = this.localStorageService.get("RecentNumber");
            }
            this.$scope.RecentNumberArray = [];
            this.$scope.trackAndTraceDetailTable = [];
        };
        TrackAndTraceController.prototype.onChangeTracknumber = function () {
            if (this.$scope.trackingNumber != null && this.$scope.trackingNumber != "") {
                this.$scope.visibleCusRefTextArea = true;
            }
            else {
                this.$scope.visibleCusRefTextArea = false;
            }
            if (this.$scope.customerRefNumber != null && this.$scope.customerRefNumber != "") {
                this.$scope.visibleTrackNumberTextArea = true;
            }
            else {
                this.$scope.visibleTrackNumberTextArea = false;
            }
        };
        TrackAndTraceController.prototype.clearSearch = function () {
            this.$scope.trackAndTraceHeader = null;
            this.$scope.trackAndTraceDetailTimeline = [];
            this.$scope.trackAndTraceDetailTable = [];
            this.$scope.trackingNumberArray = [];
            this.$scope.customerRefNumberArray = [];
            this.$scope.trackandtraceParam = {};
        };
        TrackAndTraceController.prototype.validate = function () {
            var result = true;
            return result;
        };
        TrackAndTraceController.prototype.checkItemStatus = function () {
            var _this = this;
            this.$scope.IsShowAll = false;
            this.clearSearch();
            if (this.validate() == false) {
                return false;
            }
            if (this.$scope.trackingNumber != null && this.$scope.trackingNumber != "" && this.$scope.trackingNumber.length > 0) {
                this.$scope.RecentNumberArray = this.$scope.trackingNumber.split("\n");
                this.$scope.RecentNumberArray.forEach(function (value) {
                    var itemAdd = {
                        item: value.trim(),
                        type: "trackingNumber"
                    };
                    _this.$scope.RecentNumbersArray.push(itemAdd);
                });
                this.$scope.trackingNumberArray = this.$scope.trackingNumber.split("\n");
            }
            else {
                this.$scope.RecentNumberArray = this.$scope.customerRefNumber.split("\n");
                this.$scope.customerRefNumberArray = this.$scope.customerRefNumber.split("\n");
                this.$scope.RecentNumberArray.forEach(function (value) {
                    var itemAdd = {
                        item: value.trim(),
                        type: "customerRefNumber"
                    };
                    _this.$scope.RecentNumbersArray.push(itemAdd);
                });
            }
            if (this.$scope.RecentNumberArray != null) {
                this.localStorageService.set("RecentNumber", this.$scope.RecentNumbersArray);
            }
            if (this.$scope.trackingNumberArray.length > 0 /*&& this.$scope.trackingNumberArray.length <= 10*/) {
                var chain = this.$q.when();
                var checkResult = false;
                this.$scope.trackingNumberArray.forEach(function (trackNumber) {
                    chain = chain.then(function () {
                        //this.prepareParam(trackNumber, "null");
                        _this.$scope.trackandtraceParam.consignmentNumber = trackNumber.trim();
                        _this.$scope.trackandtraceParam.customerRef = "null";
                        _this.$scope.trackandtraceParam.userId = _this.$scope.userId;
                        _this.$scope.trackandtraceParam.returnPackage = _this.$scope.expandPackageDetails;
                        //return this.trackandtraceService.checkItemStatus(trackNumber, "null", this.$scope.userId, this.$scope.expandPackageDetails)
                        return _this.trackandtraceService.checkItemStatus(_this.$scope.trackandtraceParam)
                            .then(function (result) {
                            if (result.TrackAndTraceDetail != null && result.TrackAndTraceDetail.length > 0) {
                                _this.$scope.trackAndTraceHeader = result.TrackAndTraceHeader[0];
                                //console.log(this.$scope.trackAndTraceHeader);
                                _this.$scope.trackAndTraceDetailTimeline = result.TrackAndTraceDetail;
                                if (result.TrackAndTraceDetail.length > 0 && result.TrackAndTraceHeader[0] != null) {
                                    result.TrackAndTraceDetail[0].consignment_no = result.TrackAndTraceHeader[0].consignment_no; //table orange in array[0]
                                    result.TrackAndTraceDetail[0].ref_no = result.TrackAndTraceHeader[0].ref_no;
                                    result.TrackAndTraceHeader[0].status_description = result.TrackAndTraceDetail[0].status_description;
                                    _this.$scope.trackAndTraceDetailTable.push(result.TrackAndTraceDetail[0]);
                                    checkResult = true;
                                    _this.$scope.IsNoDataTrackingNumber = false;
                                }
                                else {
                                    if (checkResult == false) {
                                        _this.$scope.IsNoDataTrackingNumber = true;
                                    }
                                }
                            }
                            else {
                                if (checkResult == false) {
                                    _this.$scope.IsNoDataTrackingNumber = true;
                                }
                            }
                        }, (function (error) {
                            console.log(error);
                        })).then(function () {
                            _this.setTimelineCollapse();
                        });
                    });
                });
            }
            if (this.$scope.customerRefNumberArray.length > 0) {
                var chain = this.$q.when();
                var checkResult = false;
                this.$scope.customerRefNumberArray.forEach(function (customerRef) {
                    chain = chain.then(function () {
                        //console.log("customerRef", customerRef);
                        //console.log(btoa(encodeURIComponent(customerRef)));
                        //this.prepareParam("null", customerRef);
                        _this.$scope.trackandtraceParam.consignmentNumber = "null";
                        _this.$scope.trackandtraceParam.customerRef = customerRef.trim();
                        _this.$scope.trackandtraceParam.userId = _this.$scope.userId;
                        _this.$scope.trackandtraceParam.returnPackage = _this.$scope.expandPackageDetails;
                        //return this.trackandtraceService.checkItemStatus("null", customerRef, this.$scope.userId, this.$scope.expandPackageDetails)
                        return _this.trackandtraceService.checkItemStatus(_this.$scope.trackandtraceParam)
                            .then(function (result) {
                            if (result != null && result.TrackAndTraceDetail.length > 0) {
                                _this.$scope.trackAndTraceHeader = result.TrackAndTraceHeader[0];
                                _this.$scope.trackAndTraceDetailTimeline = result.TrackAndTraceDetail;
                                if (result.TrackAndTraceDetail.length > 0 && result.TrackAndTraceHeader[0] != null) {
                                    result.TrackAndTraceDetail[0].consignment_no = result.TrackAndTraceHeader[0].consignment_no; //table orange in array[0]
                                    result.TrackAndTraceDetail[0].ref_no = result.TrackAndTraceHeader[0].ref_no;
                                    result.TrackAndTraceHeader[0].status_description = result.TrackAndTraceDetail[0].status_description;
                                    _this.$scope.trackAndTraceDetailTable.push(result.TrackAndTraceDetail[0]);
                                    checkResult = true;
                                    _this.$scope.IsNoDataTrackingNumber = false;
                                }
                                else {
                                    if (checkResult == false) {
                                        _this.$scope.IsNoDataTrackingNumber = true;
                                    }
                                }
                            }
                            else {
                                if (checkResult == false) {
                                    _this.$scope.IsNoDataTrackingNumber = true;
                                }
                            }
                        }, (function (error) {
                            console.log(error);
                        })).then(function () {
                            _this.setTimelineCollapse();
                        });
                    });
                });
            }
            if (this.$scope.RecentNumbersArray.length > 10) {
                for (var i = 0; i < this.$scope.RecentNumbersArray.length; i++) {
                    if (this.$scope.RecentNumbersArray.length > 10) {
                        this.$scope.RecentNumbersArray.shift();
                    }
                }
            }
        };
        TrackAndTraceController.prototype.checkItemStatusOnTable = function (itemTrack) {
            var _this = this;
            this.$scope.IsShowAll = false;
            //this.prepareParam(itemTrack, null);
            this.$scope.trackandtraceParam.consignmentNumber = itemTrack;
            this.$scope.trackandtraceParam.customerRef = null;
            this.$scope.trackandtraceParam.userId = this.$scope.userId;
            this.$scope.trackandtraceParam.returnPackage = this.$scope.expandPackageDetails;
            //return this.trackandtraceService.checkItemStatus(itemTrack, null, this.$scope.userId, this.$scope.expandPackageDetails)
            return this.trackandtraceService.checkItemStatus(this.$scope.trackandtraceParam)
                .then(function (result) {
                if (result.TrackAndTraceDetail != null && result.TrackAndTraceDetail.length > 0) {
                    _this.$scope.trackAndTraceHeader = result.TrackAndTraceHeader[0];
                    _this.$scope.trackAndTraceDetailTimeline = result.TrackAndTraceDetail;
                    if (result.TrackAndTraceDetail != null && result.TrackAndTraceHeader[0] != null) {
                        result.TrackAndTraceDetail[0].consignment_no = _this.$scope.trackAndTraceHeader.consignment_no;
                        result.TrackAndTraceHeader[0].status_description = _this.$scope.trackAndTraceDetailTimeline[0].status_description;
                        _this.$scope.IsNoDataTrackingNumber = false;
                    }
                    else {
                        _this.$scope.IsNoDataTrackingNumber = true;
                    }
                }
                else {
                    _this.$scope.IsNoDataTrackingNumber = true;
                }
            }, (function (error) {
                console.log(error);
            }))
                .then(function () {
                _this.setTimelineCollapse();
            });
        };
        TrackAndTraceController.prototype.checkItemRecent = function (itemRecent) {
            var _this = this;
            var trackingNumber = null;
            var customerRefNumber = null;
            this.$scope.IsShowAll = false;
            if (itemRecent.type == "trackingNumber") {
                trackingNumber = itemRecent.item;
                customerRefNumber = null;
            }
            if (itemRecent.type == "customerRefNumber") {
                trackingNumber = null;
                customerRefNumber = itemRecent.item;
            }
            //this.prepareParam(trackingNumber, customerRefNumber);
            this.$scope.trackandtraceParam.consignmentNumber = trackingNumber;
            this.$scope.trackandtraceParam.customerRef = customerRefNumber;
            this.$scope.trackandtraceParam.userId = this.$scope.userId;
            this.$scope.trackandtraceParam.returnPackage = this.$scope.expandPackageDetails;
            //return this.trackandtraceService.checkItemStatus(trackingNumber, customerRefNumber, this.$scope.userId, this.$scope.expandPackageDetails)
            return this.trackandtraceService.checkItemStatus(this.$scope.trackandtraceParam)
                .then(function (result) {
                _this.$scope.trackAndTraceHeader = result.TrackAndTraceHeader[0];
                _this.$scope.trackAndTraceDetailTimeline = result.TrackAndTraceDetail;
                if (result.TrackAndTraceDetail.length > 0 && result.TrackAndTraceHeader[0] != null) {
                    _this.$scope.trackAndTraceDetailTable = [];
                    result.TrackAndTraceDetail[0].consignment_no = result.TrackAndTraceHeader[0].consignment_no; //table orange in array[0]
                    result.TrackAndTraceHeader[0].status_description = result.TrackAndTraceDetail[0].status_description;
                    _this.$scope.trackAndTraceDetailTable.push(result.TrackAndTraceDetail[0]);
                    _this.$scope.IsNoDataTrackingNumber = false;
                }
                else {
                    _this.$scope.IsNoDataTrackingNumber = true;
                }
            }, (function (error) {
                console.log(error);
            }))
                .then(function () {
                _this.setTimelineCollapse();
            });
        };
        TrackAndTraceController.prototype.setTimelineCollapse = function () {
            var numShowItem = 4;
            this.$scope.trackAndTraceDetailTimelineFirst = [];
            this.$scope.trackAndTraceDetailTimelineLast = [];
            if (this.$scope.trackAndTraceDetailTimeline.length > numShowItem && this.$scope.trackAndTraceDetailTimeline.length > numShowItem + 1) {
                if (this.$scope.expandPackageDetails == true && this.$scope.IsShowAll == true) {
                    this.$scope.IsShowAll = true;
                    this.$scope.IsFlagCollap = true;
                    if (this.$scope.trackAndTraceDetailTimeline.length <= numShowItem + 1) {
                        this.$scope.IsFlagCollap = false;
                    }
                }
                else if (this.$scope.expandPackageDetails == true && this.$scope.IsShowAll == false) {
                    this.$scope.IsFlagCollap = true;
                    this.$scope.IsShowAll = false;
                    for (var i = 0; i < this.$scope.trackAndTraceDetailTimeline.length; i++) {
                        if (i < numShowItem) {
                            this.$scope.trackAndTraceDetailTimeline[i].isShow = true;
                            this.$scope.trackAndTraceDetailTimelineFirst.push(this.$scope.trackAndTraceDetailTimeline[i]);
                        }
                        else if (i == this.$scope.trackAndTraceDetailTimeline.length - 1) {
                            this.$scope.trackAndTraceDetailTimeline[i].isShow = true;
                            this.$scope.trackAndTraceDetailTimelineLast.push(this.$scope.trackAndTraceDetailTimeline[i]);
                        }
                        else {
                            this.$scope.trackAndTraceDetailTimeline[i].isShow = false;
                        }
                    }
                }
                else if (this.$scope.expandPackageDetails == false && this.$scope.IsShowAll == true) {
                    this.$scope.IsShowAll = true;
                    this.$scope.IsFlagCollap = true;
                    if (this.$scope.trackAndTraceDetailTimeline.length <= numShowItem + 1) {
                        this.$scope.IsFlagCollap = false;
                    }
                }
                else {
                    this.$scope.IsFlagCollap = true;
                    this.$scope.IsShowAll = false;
                    for (var i = 0; i < this.$scope.trackAndTraceDetailTimeline.length; i++) {
                        if (i < numShowItem) {
                            this.$scope.trackAndTraceDetailTimeline[i].isShow = true;
                            this.$scope.trackAndTraceDetailTimelineFirst.push(this.$scope.trackAndTraceDetailTimeline[i]);
                        }
                        else if (i == this.$scope.trackAndTraceDetailTimeline.length - 1) {
                            this.$scope.trackAndTraceDetailTimeline[i].isShow = true;
                            this.$scope.trackAndTraceDetailTimelineLast.push(this.$scope.trackAndTraceDetailTimeline[i]);
                        }
                        else {
                            this.$scope.trackAndTraceDetailTimeline[i].isShow = false;
                        }
                    }
                }
            }
            else {
                this.$scope.IsShowAll = false;
                this.$scope.IsFlagCollap = true;
                if (this.$scope.trackAndTraceDetailTimeline.length <= numShowItem + 1) {
                    this.$scope.IsFlagCollap = false;
                }
            }
            //this.$scope.IsFlagCollap = true;
            //this.$scope.IsShowAll = false;
            //for (var i = 0; i < this.$scope.trackAndTraceDetailTimeline.length; i++)
            //{
            //    if (i < numShowItem)
            //    {
            //        this.$scope.trackAndTraceDetailTimeline[i].isShow = true;
            //        this.$scope.trackAndTraceDetailTimelineFirst.push(this.$scope.trackAndTraceDetailTimeline[i]);
            //    }
            //    else if (i == this.$scope.trackAndTraceDetailTimeline.length - 1)// Last item
            //    {
            //        this.$scope.trackAndTraceDetailTimeline[i].isShow = true;
            //        this.$scope.trackAndTraceDetailTimelineLast.push(this.$scope.trackAndTraceDetailTimeline[i]);
            //    }
            //    else
            //    {
            //        this.$scope.trackAndTraceDetailTimeline[i].isShow = false;
            //    }
            //}
        };
        TrackAndTraceController.prototype.expand = function () {
            this.$scope.IsShowAll = true;
            this.expandPackageDetails(this.$scope.expandPackageDetails);
        };
        TrackAndTraceController.prototype.unExpand = function () {
            this.$scope.IsShowAll = false;
            this.expandPackageDetails(this.$scope.expandPackageDetails);
        };
        TrackAndTraceController.prototype.expandPackageDetails = function (expandPackageDetails) {
            var _this = this;
            this.$scope.expandPackageDetails = expandPackageDetails;
            if (expandPackageDetails == true) {
                //this.$scope.IsShowAll = expandPackageDetails;
                //this.prepareParam(this.$scope.trackAndTraceHeader.consignment_no, this.$scope.trackAndTraceHeader.ref_no);
                this.$scope.trackandtraceParam.consignmentNumber = this.$scope.trackAndTraceHeader.consignment_no;
                this.$scope.trackandtraceParam.customerRef = this.$scope.trackAndTraceHeader.ref_no;
                this.$scope.trackandtraceParam.userId = this.$scope.userId;
                this.$scope.trackandtraceParam.returnPackage = this.$scope.expandPackageDetails;
                //return this.trackandtraceService.checkItemStatus(this.$scope.trackAndTraceHeader.consignment_no, this.$scope.trackAndTraceHeader.ref_no, this.$scope.userId, expandPackageDetails)
                return this.trackandtraceService.checkItemStatus(this.$scope.trackandtraceParam)
                    .then(function (result) {
                    //this.$scope.trackAndTraceHeader = result.TrackAndTraceHeader[0];
                    _this.$scope.trackAndTraceDetailTimeline = [];
                    _this.$scope.trackAndTraceDetailTimeline = result.TrackAndTraceDetail;
                    //this.setDateTime(result.TrackAndTraceDetail[0].tracking_datetime);
                    if (result.TrackAndTraceDetail.length > 0 && result.TrackAndTraceHeader[0] != null) {
                        //this.$scope.trackAndTraceDetailTable = [];
                        result.TrackAndTraceDetail[0].consignment_no = result.TrackAndTraceHeader[0].consignment_no; //table orange in array[0]
                        result.TrackAndTraceDetail[0].ref_no = result.TrackAndTraceHeader[0].ref_no; //table orange in array[0]
                        result.TrackAndTraceHeader[0].status_description = result.TrackAndTraceDetail[0].status_description;
                    }
                }, (function (error) {
                    console.log(error);
                }))
                    .then(function () {
                    _this.setTimelineCollapse();
                });
            }
            else {
                //this.$scope.IsShowAll = expandPackageDetails;         
                //this.prepareParam(this.$scope.trackAndTraceHeader.consignment_no, this.$scope.trackAndTraceHeader.ref_no);
                this.$scope.trackandtraceParam.consignmentNumber = this.$scope.trackAndTraceHeader.consignment_no;
                this.$scope.trackandtraceParam.customerRef = this.$scope.trackAndTraceHeader.ref_no;
                this.$scope.trackandtraceParam.userId = this.$scope.userId;
                this.$scope.trackandtraceParam.returnPackage = this.$scope.expandPackageDetails;
                //return this.trackandtraceService.checkItemStatus(this.$scope.trackAndTraceHeader.consignment_no, this.$scope.trackAndTraceHeader.ref_no, this.$scope.userId, expandPackageDetails)
                return this.trackandtraceService.checkItemStatus(this.$scope.trackandtraceParam)
                    .then(function (result) {
                    //this.$scope.trackAndTraceHeader = result.TrackAndTraceHeader[0];
                    _this.$scope.trackAndTraceDetailTimeline = [];
                    _this.$scope.trackAndTraceDetailTimeline = result.TrackAndTraceDetail;
                    //this.setDateTime(result.TrackAndTraceDetail[0].tracking_datetime);
                    if (result.TrackAndTraceDetail.length > 0 && result.TrackAndTraceHeader[0] != null) {
                        //this.$scope.trackAndTraceDetailTable = [];
                        result.TrackAndTraceDetail[0].consignment_no = result.TrackAndTraceHeader[0].consignment_no; //table orange in array[0]
                        result.TrackAndTraceDetail[0].ref_no = result.TrackAndTraceHeader[0].ref_no; //table orange in array[0]
                        result.TrackAndTraceHeader[0].status_description = result.TrackAndTraceDetail[0].status_description;
                    }
                }, (function (error) {
                    console.log(error);
                }))
                    .then(function () {
                    _this.setTimelineCollapse();
                });
            }
        };
        TrackAndTraceController.$inject = ["$rootScope", "$scope", "$location", "authService",
            "$cookies", "configSettings",
            "localStorageService",
            "$sessionStorage", "clientConnectionService", "trackandtraceService", "$q", "$q"];
        return TrackAndTraceController;
    }());
    TrackAndTrace.TrackAndTraceController = TrackAndTraceController;
    Main.App.Controllers.controller("trackandtraceController", TrackAndTraceController);
})(TrackAndTrace || (TrackAndTrace = {}));
//# sourceMappingURL=trackandtraceController.js.map