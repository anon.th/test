var WelCome;
(function (WelCome) {
    var WelComeController = (function () {
        function WelComeController($scope, $location, authService, $cookies, $q, $sessionStorage) {
            this.$scope = $scope;
            this.$location = $location;
            this.authService = authService;
            this.$cookies = $cookies;
            this.$q = $q;
            this.$sessionStorage = $sessionStorage;
            $scope.vm = this;
            $scope.authentication = authService.authentication;
            this.currentPath = $location.path();
            var withOutAuth = [Shared.RouteNames.WelCome, Shared.RouteNames.EmailAccess, Shared.RouteNames.Reset, Shared.RouteNames.Unlock,
                Shared.RouteNames.Request, Shared.RouteNames.ShowMessage, Shared.RouteNames.Register, Shared.RouteNames.Login, Shared.RouteNames.RegisterUnlock];
            //if ($scope.authentication.isAuth == false)
            //{
            //    let checkPath = withOutAuth.filter(f => $location.path() == f).length;
            //    if (checkPath == 0 && this.currentPath.length > 1)
            //    {
            //        $scope.authentication.isAuth = true;
            //    }
            //    else
            //    {
            //        if (!$scope.authentication.isAuth)
            //        {
            //            this.setTimeOut(1000).then((result: boolean) =>
            //            {
            //                if (result)
            //                {
            //                    $location.url("/login");
            //                }
            //            });
            //        }
            //    }
            //} 
            this.initWelcome();
        }
        WelComeController.prototype.initWelcome = function () {
            console.log("Welcome");
        };
        WelComeController.prototype.setTimeOut = function (milliSecond) {
            var result = false;
            var defer = this.$q.defer();
            setTimeout(function () {
                result = true;
                defer.resolve(result);
            }, milliSecond);
            return defer.promise;
        };
        WelComeController.$inject = ['$scope', '$location', 'authService',
            '$cookies', "$q", "$sessionStorage"];
        return WelComeController;
    }());
    WelCome.WelComeController = WelComeController;
    Main.App.Controllers.controller("welcomeController", WelComeController);
})(WelCome || (WelCome = {}));
//# sourceMappingURL=welcomeController.js.map