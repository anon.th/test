﻿module TrackAndTrace 
{
    export interface IRecentNumber
    {
        item: string;
        type: string;
    }

    export interface ITrackAndTraceControllerScope extends ng.IScope 
    {
        vm: TrackAndTraceController;
        authentication: any;
        UserInfo: Interfaces.IUserInfo;
        ParentSubMenus: Interfaces.IUserMenu[];
        MenuSub: Interfaces.IUserMenu[];
        Menu: Interfaces.IUserMenu[];
        //MainMenu: Interfaces.IExternalMenu[];
        Roles: Interfaces.IExternalIdentityUserRoles[];
        IsCollapsed: boolean;
        CurrentMenuId: number;
        MenuUrl: string;
        userId: string;


        WithOutLoginPage: boolean;
        IsAlert: boolean;

        //in export interface
        navleft: any
        navright: any[];
        navlast: any[];
        states: any;
        RecentNumbers: IRecentNumber[];
        RecentNumberArray: string[];
        RecentNumbersArray: IRecentNumber[];
        trackingNumber: string;
        customerRefNumber: string;
        trackingNumberRecent: string;
        trackingNumberArray: string[];
        customerRefNumberArray: string[];
        trackAndTraceHeader: Interfaces.ITrackAndTraceHeader;
        trackAndTraceDetailTimeline: Interfaces.ITrackAndTraceDetail[];
        trackAndTraceDetailTable: Interfaces.ITrackAndTraceDetail[];
        visibleCusRefTextArea: boolean;
        visibleTrackNumberTextArea: boolean;
        trackandtraceParam: Interfaces.ITrackAndTraceParam;
        //For seeMore
        trackAndTraceDetailTimelineFirst: Interfaces.ITrackAndTraceDetail[];
        trackAndTraceDetailTimelineLast: Interfaces.ITrackAndTraceDetail[];
        IsShowAll: boolean;
        IsFlagCollap: boolean;

        expandPackageDetails: boolean;
        IsNoDataTrackingNumber: boolean;
        IsNoDataCustomerReference: boolean;

    }

    export class TrackAndTraceController 
    {
        public static $inject = ["$rootScope", "$scope", "$location", "authService",
            "$cookies", "configSettings",
            "localStorageService",
            "$sessionStorage", "clientConnectionService", "trackandtraceService", "$q", "$q"];

        constructor(private $rootScope: ng.IScope,
            private $scope: ITrackAndTraceControllerScope,
            private $location: ng.ILocationService,
            private authService: Util.AuthService,
            private $cookies: ng.cookies.ICookiesService,
            private configSettings: Interfaces.IConfigSettings,
            private localStorageService: any,
            private $sessionStorage: any,
            private clientConnectionService: Util.ClientConnectionService,
            private trackandtraceService: TrackandTrace.TrackandTraceService,
            private $q: ng.IQService,
            private $q2: ng.IQService)
        {
            $scope.vm = this;
            this.$scope.userId = null;
            this.$scope.expandPackageDetails = false;
            this.$scope.authentication = this.localStorageService.get('authorizationData');
            if (this.$scope.authentication != null)
            {
                this.$scope.userId = this.$scope.authentication.userName;
            }
            this.$scope.visibleCusRefTextArea = false;
            this.$scope.visibleTrackNumberTextArea = false;
            this.$scope.RecentNumbersArray = [];
            this.$scope.trackandtraceParam = {};
            this.$scope.trackAndTraceDetailTimeline = [];
            this.Initial();

        }

        public Initial()
        {
            this.$scope.RecentNumbers = [];
            if (this.localStorageService.get("RecentNumber") != null)
            {
                this.$scope.RecentNumbersArray = this.localStorageService.get("RecentNumber");
            }
            this.$scope.RecentNumberArray = [];
            this.$scope.trackAndTraceDetailTable = [];
        }
        public onChangeTracknumber(): void
        {
            if (this.$scope.trackingNumber != null && this.$scope.trackingNumber != "" )
            {
                this.$scope.visibleCusRefTextArea = true;
            }
            else
            {
                this.$scope.visibleCusRefTextArea = false;
            }

            if (this.$scope.customerRefNumber != null && this.$scope.customerRefNumber != "")
            {
                this.$scope.visibleTrackNumberTextArea = true;
            }
            else
            {
                this.$scope.visibleTrackNumberTextArea = false;
            }
        }

        public clearSearch(): void
        {
            this.$scope.trackAndTraceHeader = null;
            this.$scope.trackAndTraceDetailTimeline = [];
            this.$scope.trackAndTraceDetailTable = [];
            this.$scope.trackingNumberArray = [];
            this.$scope.customerRefNumberArray = [];
            this.$scope.trackandtraceParam = {};
        }

        public validate(): boolean
        {
            var result = true;


            return result;
        }

        public checkItemStatus() 
        {
            this.$scope.IsShowAll = false;
            this.clearSearch();
            if (this.validate() == false)
            {
                return false;
            }
            if (this.$scope.trackingNumber != null && this.$scope.trackingNumber != "" && this.$scope.trackingNumber.length > 0)
            {
                this.$scope.RecentNumberArray = this.$scope.trackingNumber.split("\n");
                this.$scope.RecentNumberArray.forEach(value =>
                {
                    var itemAdd: TrackAndTrace.IRecentNumber =
                        {
                            item: value.trim(),
                            type: "trackingNumber"
                        };
                    this.$scope.RecentNumbersArray.push(itemAdd);
                });
                this.$scope.trackingNumberArray = this.$scope.trackingNumber.split("\n");
            }
            else
            {
                this.$scope.RecentNumberArray = this.$scope.customerRefNumber.split("\n");
                this.$scope.customerRefNumberArray = this.$scope.customerRefNumber.split("\n");

                this.$scope.RecentNumberArray.forEach(value =>
                {
                    var itemAdd: TrackAndTrace.IRecentNumber =
                        {
                            item: value.trim(),
                            type: "customerRefNumber"
                        };
                    this.$scope.RecentNumbersArray.push(itemAdd);
                });
            }

            if (this.$scope.RecentNumberArray != null)
            {
                this.localStorageService.set("RecentNumber", this.$scope.RecentNumbersArray);
            }
            if (this.$scope.trackingNumberArray.length > 0 /*&& this.$scope.trackingNumberArray.length <= 10*/) {
                var chain = this.$q.when();
                var checkResult: boolean = false;
                this.$scope.trackingNumberArray.forEach(trackNumber => {
                    chain = chain.then(() => {

                        //this.prepareParam(trackNumber, "null");
                        this.$scope.trackandtraceParam.consignmentNumber = trackNumber.trim();
                        this.$scope.trackandtraceParam.customerRef = "null";
                        this.$scope.trackandtraceParam.userId = this.$scope.userId;
                        this.$scope.trackandtraceParam.returnPackage = this.$scope.expandPackageDetails;
                        //return this.trackandtraceService.checkItemStatus(trackNumber, "null", this.$scope.userId, this.$scope.expandPackageDetails)
                        return this.trackandtraceService.checkItemStatus(this.$scope.trackandtraceParam)
                            .then((result: Interfaces.ITrackAndTraceResult) => {
                                if (result.TrackAndTraceDetail != null && result.TrackAndTraceDetail.length > 0) {
                                    this.$scope.trackAndTraceHeader = result.TrackAndTraceHeader[0];
                                    //console.log(this.$scope.trackAndTraceHeader);

                                    this.$scope.trackAndTraceDetailTimeline = result.TrackAndTraceDetail;
                                    if (result.TrackAndTraceDetail.length > 0 && result.TrackAndTraceHeader[0] != null) {
                                        result.TrackAndTraceDetail[0].consignment_no = result.TrackAndTraceHeader[0].consignment_no; //table orange in array[0]
                                        result.TrackAndTraceDetail[0].ref_no = result.TrackAndTraceHeader[0].ref_no;
                                        result.TrackAndTraceHeader[0].status_description = result.TrackAndTraceDetail[0].status_description;

                                        this.$scope.trackAndTraceDetailTable.push(result.TrackAndTraceDetail[0]);
                                        checkResult = true;
                                        this.$scope.IsNoDataTrackingNumber = false;
                                    }
                                    else {
                                        if (checkResult == false)
                                        {
                                            this.$scope.IsNoDataTrackingNumber = true;
                                        }
                                    }
                                }
                                else
                                {
                                    if (checkResult == false)
                                    {
                                        this.$scope.IsNoDataTrackingNumber = true;
                                    }
                                }
                                
                            }
                            , ((error) => {
                                console.log(error);

                            })).then(() => {
                                this.setTimelineCollapse();
                            });
                    });
                });
            }
            if (this.$scope.customerRefNumberArray.length > 0) {
                var chain = this.$q.when();
                var checkResult: boolean = false;
                this.$scope.customerRefNumberArray.forEach(customerRef => {
                    chain = chain.then(() => {
                        //console.log("customerRef", customerRef);
                        //console.log(btoa(encodeURIComponent(customerRef)));
                        //this.prepareParam("null", customerRef);
                        this.$scope.trackandtraceParam.consignmentNumber = "null";
                        this.$scope.trackandtraceParam.customerRef = customerRef.trim();
                        this.$scope.trackandtraceParam.userId = this.$scope.userId;
                        this.$scope.trackandtraceParam.returnPackage = this.$scope.expandPackageDetails;

                        //return this.trackandtraceService.checkItemStatus("null", customerRef, this.$scope.userId, this.$scope.expandPackageDetails)
                        return this.trackandtraceService.checkItemStatus(this.$scope.trackandtraceParam)
                            .then((result: Interfaces.ITrackAndTraceResult) => {
                                if (result != null && result.TrackAndTraceDetail.length > 0) {
                                    this.$scope.trackAndTraceHeader = result.TrackAndTraceHeader[0];
                                    this.$scope.trackAndTraceDetailTimeline = result.TrackAndTraceDetail;
                                    if (result.TrackAndTraceDetail.length > 0 && result.TrackAndTraceHeader[0] != null) {
                                        result.TrackAndTraceDetail[0].consignment_no = result.TrackAndTraceHeader[0].consignment_no; //table orange in array[0]
                                        result.TrackAndTraceDetail[0].ref_no = result.TrackAndTraceHeader[0].ref_no;
                                        result.TrackAndTraceHeader[0].status_description = result.TrackAndTraceDetail[0].status_description;

                                        this.$scope.trackAndTraceDetailTable.push(result.TrackAndTraceDetail[0]);
                                        checkResult = true;
                                        this.$scope.IsNoDataTrackingNumber = false;
                                    }
                                    else {
                                        if (checkResult == false) {
                                            this.$scope.IsNoDataTrackingNumber = true;
                                        }
                                    }
                                }
                                else {
                                    if (checkResult == false) {
                                        this.$scope.IsNoDataTrackingNumber = true;
                                    }
                                }
                            }
                            , ((error) =>
                            {
                                console.log(error);
                            })).then(() => {
                                this.setTimelineCollapse();
                            });
                    });
                });
            }

            if (this.$scope.RecentNumbersArray.length > 10)  // remove value from RecentNumber if length > 10
            {
                for (var i = 0; i < this.$scope.RecentNumbersArray.length; i++)
                {

                    if (this.$scope.RecentNumbersArray.length > 10)
                    {
                        this.$scope.RecentNumbersArray.shift();
                    }
                }
            }
        }

        public checkItemStatusOnTable(itemTrack: string) 
        {
            this.$scope.IsShowAll = false;
            //this.prepareParam(itemTrack, null);
            this.$scope.trackandtraceParam.consignmentNumber = itemTrack;
            this.$scope.trackandtraceParam.customerRef = null;
            this.$scope.trackandtraceParam.userId = this.$scope.userId;
            this.$scope.trackandtraceParam.returnPackage = this.$scope.expandPackageDetails;
            //return this.trackandtraceService.checkItemStatus(itemTrack, null, this.$scope.userId, this.$scope.expandPackageDetails)
            return this.trackandtraceService.checkItemStatus(this.$scope.trackandtraceParam)
                .then((result: Interfaces.ITrackAndTraceResult) =>
                {
                    if (result.TrackAndTraceDetail != null && result.TrackAndTraceDetail.length > 0 )
                    {
                        this.$scope.trackAndTraceHeader = result.TrackAndTraceHeader[0];
                        this.$scope.trackAndTraceDetailTimeline = result.TrackAndTraceDetail;
                        if (result.TrackAndTraceDetail != null && result.TrackAndTraceHeader[0] != null) 
                        {
                            result.TrackAndTraceDetail[0].consignment_no = this.$scope.trackAndTraceHeader.consignment_no;
                            result.TrackAndTraceHeader[0].status_description = this.$scope.trackAndTraceDetailTimeline[0].status_description;
                            this.$scope.IsNoDataTrackingNumber = false;
                        }
                        else
                        {
                            this.$scope.IsNoDataTrackingNumber = true;
                        }
                    }
                    else { this.$scope.IsNoDataTrackingNumber = true; }
                }
                , ((error) =>
                {
                    console.log(error);
                }))
                .then(() =>
                {
                    this.setTimelineCollapse();
                });
        }

        public checkItemRecent(itemRecent: TrackAndTrace.IRecentNumber)
        {
            var trackingNumber = null;
            var customerRefNumber = null;
            this.$scope.IsShowAll = false;
            if (itemRecent.type == "trackingNumber")
            {
                trackingNumber = itemRecent.item;
                customerRefNumber = null;
            }

            if (itemRecent.type == "customerRefNumber")
            {
                trackingNumber = null;
                customerRefNumber = itemRecent.item;
            }

            //this.prepareParam(trackingNumber, customerRefNumber);
            this.$scope.trackandtraceParam.consignmentNumber = trackingNumber;
            this.$scope.trackandtraceParam.customerRef = customerRefNumber;
            this.$scope.trackandtraceParam.userId = this.$scope.userId;
            this.$scope.trackandtraceParam.returnPackage = this.$scope.expandPackageDetails;
            //return this.trackandtraceService.checkItemStatus(trackingNumber, customerRefNumber, this.$scope.userId, this.$scope.expandPackageDetails)
            return this.trackandtraceService.checkItemStatus(this.$scope.trackandtraceParam)
                .then((result: Interfaces.ITrackAndTraceResult) =>
                {
                    this.$scope.trackAndTraceHeader = result.TrackAndTraceHeader[0];
                   
                    this.$scope.trackAndTraceDetailTimeline = result.TrackAndTraceDetail;

                    if (result.TrackAndTraceDetail.length > 0 && result.TrackAndTraceHeader[0] != null) 
                    {
                        this.$scope.trackAndTraceDetailTable = [];
                        result.TrackAndTraceDetail[0].consignment_no = result.TrackAndTraceHeader[0].consignment_no; //table orange in array[0]
                        result.TrackAndTraceHeader[0].status_description = result.TrackAndTraceDetail[0].status_description;

                        this.$scope.trackAndTraceDetailTable.push(result.TrackAndTraceDetail[0]);
                        this.$scope.IsNoDataTrackingNumber = false;
                    }
                    else
                    {
                        this.$scope.IsNoDataTrackingNumber = true;
                    }

                }
                , ((error) =>
                {
                    console.log(error);
                }))
                .then(() =>
                {
                    this.setTimelineCollapse();
                });
        }

        public setTimelineCollapse(): void
        {
            var numShowItem = 4;
            this.$scope.trackAndTraceDetailTimelineFirst = [];
            this.$scope.trackAndTraceDetailTimelineLast = [];
            
            if (this.$scope.trackAndTraceDetailTimeline.length > numShowItem && this.$scope.trackAndTraceDetailTimeline.length > numShowItem + 1) 
            {
                if (this.$scope.expandPackageDetails == true && this.$scope.IsShowAll == true) {
                    this.$scope.IsShowAll = true;
                    this.$scope.IsFlagCollap = true;
                    if (this.$scope.trackAndTraceDetailTimeline.length <= numShowItem + 1) {
                        this.$scope.IsFlagCollap = false;
                    }
                }
                else if (this.$scope.expandPackageDetails == true && this.$scope.IsShowAll == false)
                {
                    this.$scope.IsFlagCollap = true;
                    this.$scope.IsShowAll = false;
                    for (var i = 0; i < this.$scope.trackAndTraceDetailTimeline.length; i++) {
                        if (i < numShowItem) {
                            this.$scope.trackAndTraceDetailTimeline[i].isShow = true;
                            this.$scope.trackAndTraceDetailTimelineFirst.push(this.$scope.trackAndTraceDetailTimeline[i]);
                        }
                        else if (i == this.$scope.trackAndTraceDetailTimeline.length - 1)// Last item
                        {
                            this.$scope.trackAndTraceDetailTimeline[i].isShow = true;
                            this.$scope.trackAndTraceDetailTimelineLast.push(this.$scope.trackAndTraceDetailTimeline[i]);
                        }
                        else {
                            this.$scope.trackAndTraceDetailTimeline[i].isShow = false;
                        }
                    }
                    
                }
                else if (this.$scope.expandPackageDetails == false && this.$scope.IsShowAll == true)
                {
                    this.$scope.IsShowAll = true;
                    this.$scope.IsFlagCollap = true;
                    if (this.$scope.trackAndTraceDetailTimeline.length <= numShowItem+1) {
                        this.$scope.IsFlagCollap = false;
                    }
                    
                }
                else
                {
                    this.$scope.IsFlagCollap = true;
                    this.$scope.IsShowAll = false;
                    for (var i = 0; i < this.$scope.trackAndTraceDetailTimeline.length; i++) {
                        if (i < numShowItem) {
                            this.$scope.trackAndTraceDetailTimeline[i].isShow = true;
                            this.$scope.trackAndTraceDetailTimelineFirst.push(this.$scope.trackAndTraceDetailTimeline[i]);
                        }
                        else if (i == this.$scope.trackAndTraceDetailTimeline.length - 1)// Last item
                        {
                            this.$scope.trackAndTraceDetailTimeline[i].isShow = true;
                            this.$scope.trackAndTraceDetailTimelineLast.push(this.$scope.trackAndTraceDetailTimeline[i]);
                        }
                        else {
                            this.$scope.trackAndTraceDetailTimeline[i].isShow = false;
                        }
                    }
                }
            }
            else
            {
                this.$scope.IsShowAll = false;
                this.$scope.IsFlagCollap = true;
                if (this.$scope.trackAndTraceDetailTimeline.length <= numShowItem+1)
                {
                    this.$scope.IsFlagCollap = false;
                }
            }

                //this.$scope.IsFlagCollap = true;
                //this.$scope.IsShowAll = false;
                //for (var i = 0; i < this.$scope.trackAndTraceDetailTimeline.length; i++)
                //{
                //    if (i < numShowItem)
                //    {
                //        this.$scope.trackAndTraceDetailTimeline[i].isShow = true;
                //        this.$scope.trackAndTraceDetailTimelineFirst.push(this.$scope.trackAndTraceDetailTimeline[i]);
                //    }
                //    else if (i == this.$scope.trackAndTraceDetailTimeline.length - 1)// Last item
                //    {
                //        this.$scope.trackAndTraceDetailTimeline[i].isShow = true;
                //        this.$scope.trackAndTraceDetailTimelineLast.push(this.$scope.trackAndTraceDetailTimeline[i]);
                //    }
                //    else
                //    {
                //        this.$scope.trackAndTraceDetailTimeline[i].isShow = false;
                //    }
                //}
        }

        public expand(): void
        {
            this.$scope.IsShowAll = true;
            this.expandPackageDetails(this.$scope.expandPackageDetails);
        }

        public unExpand(): void
        {
            this.$scope.IsShowAll = false;
            this.expandPackageDetails(this.$scope.expandPackageDetails);
        }

        public expandPackageDetails(expandPackageDetails: boolean): ng.IPromise<void>
        {
            this.$scope.expandPackageDetails = expandPackageDetails;
            if (expandPackageDetails == true)
            {
                //this.$scope.IsShowAll = expandPackageDetails;
                //this.prepareParam(this.$scope.trackAndTraceHeader.consignment_no, this.$scope.trackAndTraceHeader.ref_no);
                this.$scope.trackandtraceParam.consignmentNumber = this.$scope.trackAndTraceHeader.consignment_no;
                this.$scope.trackandtraceParam.customerRef = this.$scope.trackAndTraceHeader.ref_no;
                this.$scope.trackandtraceParam.userId = this.$scope.userId;
                this.$scope.trackandtraceParam.returnPackage = this.$scope.expandPackageDetails;
                //return this.trackandtraceService.checkItemStatus(this.$scope.trackAndTraceHeader.consignment_no, this.$scope.trackAndTraceHeader.ref_no, this.$scope.userId, expandPackageDetails)
                return this.trackandtraceService.checkItemStatus(this.$scope.trackandtraceParam)
                    .then((result: Interfaces.ITrackAndTraceResult) =>
                    {
                        //this.$scope.trackAndTraceHeader = result.TrackAndTraceHeader[0];
                        this.$scope.trackAndTraceDetailTimeline = [];
                        this.$scope.trackAndTraceDetailTimeline = result.TrackAndTraceDetail;
                        //this.setDateTime(result.TrackAndTraceDetail[0].tracking_datetime);
                        if (result.TrackAndTraceDetail.length > 0 && result.TrackAndTraceHeader[0] != null)
                        {
                            //this.$scope.trackAndTraceDetailTable = [];
                            result.TrackAndTraceDetail[0].consignment_no = result.TrackAndTraceHeader[0].consignment_no; //table orange in array[0]
                            result.TrackAndTraceDetail[0].ref_no = result.TrackAndTraceHeader[0].ref_no; //table orange in array[0]
                            result.TrackAndTraceHeader[0].status_description = result.TrackAndTraceDetail[0].status_description;

                            //this.$scope.trackAndTraceDetailTable.push(result.TrackAndTraceDetail[0]);
                        }

                    }
                    , ((error) =>
                    {
                        console.log(error);
                    }))
                    .then(() =>
                    {
                        this.setTimelineCollapse();
                    });
                
            }
            else
            {
                //this.$scope.IsShowAll = expandPackageDetails;         
                //this.prepareParam(this.$scope.trackAndTraceHeader.consignment_no, this.$scope.trackAndTraceHeader.ref_no);
                this.$scope.trackandtraceParam.consignmentNumber = this.$scope.trackAndTraceHeader.consignment_no;
                this.$scope.trackandtraceParam.customerRef = this.$scope.trackAndTraceHeader.ref_no;
                this.$scope.trackandtraceParam.userId = this.$scope.userId;
                this.$scope.trackandtraceParam.returnPackage = this.$scope.expandPackageDetails;
                //return this.trackandtraceService.checkItemStatus(this.$scope.trackAndTraceHeader.consignment_no, this.$scope.trackAndTraceHeader.ref_no, this.$scope.userId, expandPackageDetails)
                return this.trackandtraceService.checkItemStatus(this.$scope.trackandtraceParam)
                    .then((result: Interfaces.ITrackAndTraceResult) => {
                        //this.$scope.trackAndTraceHeader = result.TrackAndTraceHeader[0];
                        this.$scope.trackAndTraceDetailTimeline = [];
                        this.$scope.trackAndTraceDetailTimeline = result.TrackAndTraceDetail;
                        //this.setDateTime(result.TrackAndTraceDetail[0].tracking_datetime);
                        if (result.TrackAndTraceDetail.length > 0 && result.TrackAndTraceHeader[0] != null) {
                            //this.$scope.trackAndTraceDetailTable = [];
                            result.TrackAndTraceDetail[0].consignment_no = result.TrackAndTraceHeader[0].consignment_no; //table orange in array[0]
                            result.TrackAndTraceDetail[0].ref_no = result.TrackAndTraceHeader[0].ref_no; //table orange in array[0]
                            result.TrackAndTraceHeader[0].status_description = result.TrackAndTraceDetail[0].status_description;

                            //this.$scope.trackAndTraceDetailTable.push(result.TrackAndTraceDetail[0]);
                        }

                    }
                    , ((error) => {
                        console.log(error);
                    }))
                    .then(() => {
                        this.setTimelineCollapse();
                    });
            }
            
            
        }

        //public prepareParam(consignNumber: string, cusRef: string): void
        //{
        //    this.$scope.trackandtraceParam = null;
        //    this.$scope.trackandtraceParam.consignmentNumber = consignNumber;
        //    this.$scope.trackandtraceParam.customerRef = cusRef;
        //    this.$scope.trackandtraceParam.userId = this.$scope.userId;
        //    this.$scope.trackandtraceParam.returnPackage = this.$scope.expandPackageDetails;
        //    console.log(this.$scope.trackandtraceParam);
        //}
    }

    Main.App.Controllers.controller("trackandtraceController", TrackAndTraceController);
}