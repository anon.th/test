﻿/// <reference path="../../_references.ts" />
module Shared
{
    export interface IPaginiationLovDirectiveScope extends ng.IScope
    {
        PageLinks: Interfaces.IPageLink[];
        totalCount: number;
        recordsPerPage: number;
        urlPrefix: string;
        urlPostfix: string;
        currentPageId: number;
        skip: number;
        previousLabel: string;
        nextLabel: string;
        maxSize: number;
        SetLink(currentPageLink: Interfaces.IPageLink);
    }

    export class PaginationLovDirective
    {
        constructor()
        {
            var directive: ng.IDirective = {};
            directive.restrict = 'AE';
            directive.require = "^ngModel";

            //two way data bind to attributes
            directive.scope =
                {
                    totalCount: '=',
                    recordsPerPage: '=',
                    urlPrefix: '=',
                    urlPostfix: '=',
                    currentPageId: '=',
                    skip: '=',
                    previousLabel: '=',
                    nextLabel: '=',
                    maxSize: '='
                };

            directive.link = (($scope: IPaginiationLovDirectiveScope, $element: any, $attributes: any, ngModel: ng.INgModelController) =>
            {
                //add a watch onto totalCount to see when we need to create the page links
                //this should only happen if the total count is greated then 0
                $scope.$watch('[totalCount,currentPageId]', () =>
                {
                    //instantiate new page links
                    $scope.PageLinks = [];
                    if ($scope.totalCount > 0)
                    {
                        var totalPages: number = Math.ceil($scope.totalCount / $scope.recordsPerPage);

                        //create tne next and back skip intervals 
                        var nextSkipInterval: number = ($scope.skip + $scope.recordsPerPage);
                        var backSkipInterval: number = ($scope.skip - $scope.recordsPerPage);

                        //create the new page Ids for back and next links
                        var backPageId: number = $scope.currentPageId - 1;
                        var nextPageId: number = $scope.currentPageId + 1;
                        var i: number = 0;

                        //make sure we need to generate the links
                        if (!($scope.recordsPerPage >= $scope.totalCount))
                        {
                            //make back link
                            $scope.PageLinks.push({
                                Id: i,
                                Skip: backSkipInterval,
                                Page: backPageId,
                                Label: $attributes.previousLabel != undefined ? $attributes.previousLabel : "«",
                                Link: $scope.skip <= 0 ? "" : $scope.urlPrefix + "/page/" + backPageId + "/skip/" + backSkipInterval + "/" + $scope.urlPostfix,
                                Current: false,
                                Disabled: $scope.skip <= 0 ? true : false
                            });

                            i = i + 1;

                            //make numbered links between back and next
                            var individualSkip: number = 0;

                            try
                            {
                                // TODO: Add new links between back and next
                                var maxSize: number = $scope.maxSize != undefined ? $scope.maxSize : 10;
                                var startPage: number = 1;
                                var endPage: number = totalPages;
                                var isMaxSized = angular.isDefined(maxSize) && maxSize < totalPages;

                                // TODO: recompute if maxSize
                                if (isMaxSized)
                                {
                                    // TODO: Visible pages are paginated with maxSize
                                    startPage = ((Math.ceil($scope.currentPageId / maxSize) - 1) * maxSize) + 1;
                                    // TODO: Adjust last page if limit is exceeded
                                    endPage = Math.min(startPage + maxSize - 1, totalPages);
                                }

                                // TODO: Add links between back and next
                                for (i = startPage; i <= endPage; i++)
                                {
                                    individualSkip = (i - 1) * $scope.recordsPerPage;
                                    $scope.PageLinks.push({
                                        Id: i,
                                        Label: i.toString(),
                                        Skip: individualSkip,
                                        Page: i,
                                        Link: $scope.urlPrefix + "/page/" + i + "/skip/" + individualSkip + "/" + $scope.urlPostfix,
                                        Current: i == $scope.currentPageId ? true : false,
                                        Disabled: false
                                    });
                                }

                                // TODO: Add links to move between page sets
                                if (isMaxSized)
                                {
                                    if (startPage > 1)
                                    {
                                        individualSkip = (startPage - 2) * $scope.recordsPerPage;
                                        $scope.PageLinks.push({
                                            Id: (startPage - 1),
                                            Label: '...',
                                            Skip: individualSkip,
                                            Page: (startPage - 1),
                                            Link: $scope.urlPrefix + "/page/" + (startPage - 1) + "/skip/" + individualSkip + "/" + $scope.urlPostfix,
                                            Current: (startPage - 1) == $scope.currentPageId ? true : false,
                                            Disabled: false
                                        });
                                    }

                                    if (endPage < totalPages)
                                    {
                                        individualSkip = endPage * $scope.recordsPerPage;
                                        $scope.PageLinks.push({
                                            Id: (endPage + 1),
                                            Label: '...',
                                            Skip: individualSkip,
                                            Page: (endPage + 1),
                                            Link: $scope.urlPrefix + "/page/" + (endPage + 1) + "/skip/" + individualSkip + "/" + $scope.urlPostfix,
                                            Current: (endPage + 1) == $scope.currentPageId ? true : false,
                                            Disabled: false
                                        });
                                    }
                                }

                            } catch (err)
                            {
                                // TODO: Reset PageLinks object and make default pagination method
                                $scope.PageLinks = [];
                                i = 0;

                                $scope.PageLinks.push({
                                    Id: i,
                                    Skip: backSkipInterval,
                                    Page: backPageId,
                                    Label: $attributes.previousLabel != undefined ? $attributes.previousLabel : "«",
                                    Link: $scope.skip <= 0 ? "" : $scope.urlPrefix + "/page/" + backPageId + "/skip/" + backSkipInterval + "/" + $scope.urlPostfix,
                                    Current: false,
                                    Disabled: $scope.skip <= 0 ? true : false
                                });

                                i = i + 1;

                                while (i <= totalPages)
                                {
                                    $scope.PageLinks.push({
                                        Id: i,
                                        Label: i.toString(),
                                        Skip: individualSkip,
                                        Page: i,
                                        Link: $scope.urlPrefix + "/page/" + i + "/skip/" + individualSkip + "/" + $scope.urlPostfix,
                                        Current: i == $scope.currentPageId ? true : false,
                                        Disabled: false
                                    });

                                    individualSkip = individualSkip + $scope.recordsPerPage;
                                    i++;
                                }
                            }

                            //make next link
                            $scope.PageLinks.push({
                                Id: i,
                                Label: $attributes.nextLabel != undefined ? $attributes.nextLabel : "»",
                                Skip: nextSkipInterval,
                                Page: nextPageId,
                                Link: nextSkipInterval >= $scope.totalCount ? "" : $scope.urlPrefix + "/page/" + nextPageId + "/skip/" + nextSkipInterval + "/" + $scope.urlPostfix,
                                Current: false,
                                Disabled: nextSkipInterval >= $scope.totalCount ? true : false
                            });
                        }
                    }
                });

                $scope.SetLink = (currentPageLink: Interfaces.IPageLink) =>
                {
                    if (!currentPageLink.Disabled)
                        ngModel.$setViewValue(currentPageLink);
                };
            });

            directive.templateUrl = "App/template/paginationLovTemplate.html";
            return directive;
        }
    }

    Main.App.Directives.directive("dirPaginationLov", () => new PaginationLovDirective());
} 