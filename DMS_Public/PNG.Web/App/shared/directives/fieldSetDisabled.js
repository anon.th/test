var Shared;
(function (Shared) {
    var FieldsetDisabledDirective = (function () {
        function FieldsetDisabledDirective($timeout) {
            var directive = {};
            directive.restrict = "E";
            directive.scope = {
                isDisabled: '=ngDisabled'
            };
            directive.link = function ($scope, $element, $attrs) {
                $scope.$watch('isDisabled', function (disabled) {
                    if (disabled == "true" || disabled == true) {
                        $element.find('input, select, textarea').prop('disabled', disabled);
                    }
                });
            };
            return directive;
        }
        return FieldsetDisabledDirective;
    }());
    Shared.FieldsetDisabledDirective = FieldsetDisabledDirective;
    Main.App.Directives.directive("fieldset", ["$timeout", function ($timeout) { return new FieldsetDisabledDirective($timeout); }]);
})(Shared || (Shared = {}));
//# sourceMappingURL=fieldSetDisabled.js.map