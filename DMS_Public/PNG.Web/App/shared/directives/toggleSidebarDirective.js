var Shared;
(function (Shared) {
    var toggleSidebarDirective = (function () {
        function toggleSidebarDirective($document) {
            var directive = {};
            directive.restrict = "A";
            directive.link = (function ($scope, $element, $attrs) {
                $element.bind("click", function (e) {
                    e.preventDefault();
                    var hideClass = 'hide-menu';
                    var ele = $document.find('#containerBox');
                    if (ele[0].className.indexOf(hideClass) == -1) {
                        $document.find('#containerBox').addClass(hideClass);
                    }
                    else {
                        $document.find('#containerBox').removeClass(hideClass);
                    }
                });
            });
            return directive;
        }
        return toggleSidebarDirective;
    }());
    Shared.toggleSidebarDirective = toggleSidebarDirective;
    Main.App.Directives.directive("toggleSidebar", ["$document", function ($document) { return new toggleSidebarDirective($document); }]);
})(Shared || (Shared = {}));
//# sourceMappingURL=toggleSidebarDirective.js.map