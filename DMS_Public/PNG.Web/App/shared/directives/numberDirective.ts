﻿//** Modify By Nayfido 30.10.2558 **//
//** - กรณี out focus จะเติมจำนวนทศนิยมที่ขาดให้อัตโนมัติ
//** - จำกัดจำนวน Length ของ numeric โดยใช้ attr numeric-point=".." เพื่อให้สอดคล้องกับ decimal ในฐานข้อมูล
//** - เคลียร์ 0 ที่อยู่ด้านหน้าทั้งกรณีพิมพ์ และ copy & paste
//** - เติม comma ให้อัตโนมัติ
//** - สามารถกำหนด max ของ value ได้ โดยใช้ attr max=".." เพื่อผู้ใช้ป้อนเกิน จะถูกปรับค่าให้เท่ากับค่า max
//** - สามารถกำหนด allow null เมื่อต้องการให้กรอกค่าว่างได้ โดยใช้ attr allow-null="true"

module Shared
{
    export class NumberDirective
    {
        constructor($filter: any)
        {
            var directive: ng.IDirective = {};
            directive.restrict = "A";
            directive.require = "^ngModel";
            directive.link = (($scope: ng.IScope, $element: any, $attrs: any, ngModel: ng.INgModelController) =>
            {
                //บล๊อค keypress comma
                $element.bind("keydown keypress", function (event)
                {
                    if (event.which === 188)
                    {
                        event.preventDefault();
                    }
                });

                // ตัด comma ทิ้ง ตอน paste
                $element.bind("paste", function (event)
                {
                    var _this = this;
                    // Short pause to wait for paste to complete
                    setTimeout(function ()
                    {
                        var pastedData = $(_this).val();
                        pastedData = String(pastedData).replace(/[\, ]/g, '');
                        ngModel.$setViewValue(pastedData);
                        ngModel.$render();
                    }, 100);

                });

                //เติมทศนิยมตัวที่ขาดอัตโนมัติ เมื่อ on blur
                $element.bind('blur', function ()
                {
                    var transformedInput;
                    var transformedParse: number = 0;

                    if ($attrs.allowDecimal == "true")
                    {
                        transformedInput = ngModel.$modelValue;
                        if (transformedInput != undefined)
                        {
                            transformedParse = parseFloat(String(transformedInput).replace(/\,/g, ''));
                            if (transformedParse < $attrs.min)
                            {
                                transformedInput = $attrs.min;
                            }

                            if ($attrs.decimalPoint)
                            {
                                if ($attrs.allowNull && transformedInput == "")
                                {
                                    return false;
                                }

                                var numerics = String(transformedInput).split(".");
                                numerics[0] = numerics[0].length == 0 ? "0" : numerics[0];
                                var decimalValue: string = numerics[1];

                                if (numerics[1] == undefined || numerics[1].length < Number($attrs.decimalPoint))
                                {
                                    var decimalValueLength: number = 0;

                                    if (numerics[1])
                                    {
                                        decimalValue = numerics[1].slice(0, $attrs.decimalPoint);
                                        decimalValueLength = decimalValue.length;
                                    }
                                    else
                                    {
                                        decimalValue = "";
                                    }
                                    console.info($attrs.decimalPoint - decimalValueLength);
                                    //เติม 0 ให้ครบตามจำนวน decimalPoint ที่กำหนด
                                    for (var i = 0; i < $attrs.decimalPoint - decimalValueLength; i++)
                                    {
                                        decimalValue += "0";
                                    }
                                }


                                if (transformedInput)
                                {
                                    numerics[0] = $filter("number")(numerics[0], 0);
                                    var valueResult = [numerics[0], decimalValue].join(".");

                                    ngModel.$setViewValue(valueResult);
                                    ngModel.$render();
                                }
                            }
                        }
                    }
                    else
                    {
                        transformedInput = ngModel.$modelValue;

                        if (transformedInput)
                        {
                            var x = $filter("number")(transformedInput, 0);
                            ngModel.$setViewValue(x);
                            ngModel.$render();
                        }
                    }
                });

                $element.bind('focus', function ()
                {
                    if (ngModel.$modelValue != "0" && ngModel.$modelValue)
                    {
                        ngModel.$setViewValue(ngModel.$modelValue.toString().replace(/\,/g, ''));
                        ngModel.$render();
                    }
                });

                $scope.$watch($attrs.ngModel, (newValue, oldValue) =>
                {
                    //defualt value
                    if (newValue == undefined) return '';
                    if (newValue == null) return '';

                    newValue = String(newValue);
                    var spiltArray = newValue.split("");

                    if ($attrs.allowNegative == "false" || $attrs.allowNegative == undefined)
                    {
                        if (spiltArray[0] == '-')
                        {
                            newValue = String(newValue).replace("-", "");
                            ngModel.$setViewValue(newValue);
                            ngModel.$render();
                        }
                    }

                    var transformedInput;
                    var transformedParse: number = 0;
                    if ($attrs.allowDecimal == "true")
                    {
                        transformedInput = String(newValue).replace(/[^0-9\.\,]/g, '');
                        var count = (transformedInput.match(/\./g) || []).length;

                        //ถ้ามีจุดทศนิยมมากกว่า 1 จุด ให้กลับไปใช้ value เดิม
                        if (count > 1)
                        {
                            transformedInput = oldValue;
                        }

                        if ($attrs.decimalPoint)
                        {
                            var n = String(transformedInput).split(".");

                            //จำกัดจำนวน length กรณีมีทศนิยม และไม่ได้กำหนด ค่า max
                            if (String(n[0]).replace(/\,/g, '').length > $attrs.numericPoint && !$attrs.max)
                            {
                                transformedInput = oldValue;
                            }
                            //จำกัดจำนวน ให้ไม่เกินที่กำหนด
                            else if (parseFloat(String(transformedInput).replace(/\,/g, '')) > $attrs.max)
                            {
                                transformedInput = $attrs.max;
                            }
                            else if (parseFloat(String(transformedInput).replace(/\,/g, '')) < $attrs.min)
                            {

                            }
                            else
                            {
                                //ทำการเคลียร์ 0 ที่อยู่ข้างหน้า กรณีมีทศนิยม
                                if (n[0].length > 1 && n[0][0] == "0")
                                {
                                    n[0] = n[0].slice(1);
                                    transformedInput = transformedInput.slice(1);
                                }

                                if (n[1])
                                {
                                    var n2 = n[1].slice(0, $attrs.decimalPoint);
                                    transformedInput = [n[0], n2].join(".");
                                }

                                if (newValue == oldValue)
                                {
                                    if ($attrs.netassetValue == "true")
                                    {
                                        transformedInput = transformedInput;
                                    }
                                    else
                                    {
                                        transformedInput = $filter('number')(transformedInput.replace(/\,/g, ''), parseInt($attrs.decimalPoint));

                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        transformedInput = String(newValue).replace(/[^0-9\,]/g, '');

                        //ทำการเคลียร์ 0 ที่อยู่ข้างหน้า
                        if (transformedInput.length > 1 && transformedInput[0] == "0")
                        {
                            transformedParse = parseInt(transformedInput);
                            transformedInput = transformedParse.toString();
                        }

                        //จำกัดจำนวน length กรณีไม่มีทศนิยม
                        if (String(transformedInput).replace(/\,/g, '').length > $attrs.numericPoint)
                        {
                            transformedInput = oldValue;
                        }

                        //จำกัดจำนวน ให้ไม่เกินที่กำหนด
                        if (parseInt(String(transformedInput).replace(/\,/g, '')) > $attrs.max)
                        {
                            transformedInput = $attrs.max;
                        }
                        else if (parseFloat(String(transformedInput).replace(/\,/g, '')) < $attrs.min)
                        {
                            transformedInput = $attrs.min;
                        }
                    }

                    if ($attrs.allowNegative == "true")
                    {
                        if (spiltArray[0] == '-')
                        {
                            transformedInput = '-' + transformedInput;
                        }
                    }

                    if (transformedInput.length == 1 && transformedInput == "-")
                    {
                        ngModel.$setValidity($attrs.ngModel, false);
                    } else
                    {
                        ngModel.$setValidity($attrs.ngModel, true);
                    }

                    if ($attrs.max != undefined && ngModel.$valid)
                    {
                        ngModel.$setValidity($attrs.ngModel, !(parseFloat(transformedInput) > parseFloat($attrs.max)));
                    }

                    if (transformedInput != newValue)
                    {
                        ngModel.$setViewValue(transformedInput);
                        ngModel.$render();
                    }
                });

            });

            return directive;
        }
    }
    Main.App.Directives.directive("numberTexbox", ["$filter", ($filter) => new NumberDirective($filter)]);
}