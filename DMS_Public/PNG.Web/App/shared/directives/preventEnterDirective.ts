﻿module Shared
{
    export class PreventEnterDirective
    {
        constructor()
        {
            var directive: ng.IDirective = {};
            directive.restrict = "A";
            directive.require = "^ngModel";
            directive.link = (scope: ng.IScope, $element: any, $attrs: any, ngModel: ng.INgModelController) =>
            {
                $element.bind("keypress", function (event)
                {
                    var key = event.keyCode || event.which;
                    if (key === 13) // Enter
                    {
                        event.returnValue = false; // for IE
                        event.preventDefault(); // for any browser
                    }
                    event.stopPropagation(); // not goto another event
                });
            }

            return directive;
        }
    }

    Main.App.Directives.directive("preventEnter", () => new PreventEnterDirective());
}