﻿module SelectBox
{
    export class selectBoxDirective
    {

        constructor()
        {
            var directive: ng.IDirective = {};
            directive.restrict = "AE";
            directive.require = "^ngModel";
            directive.scope = {
                ngDisabled: "=",
                ngModel: "="
            };

            directive.link = ($scope: any, $element: any, $attributes: any, ngModel: ng.INgModelController, $timeout) =>
            {
                $scope.myScope = $scope;
                $attributes.$observe("selecttooltip", () =>
                {
                    if ($attributes.selecttooltip === undefined) return;
                    $scope.selecttooltip = $attributes.selecttooltip;

                });

                $scope.modelName = $attributes.ngModel;
                $scope.entityName = $scope.modelName.slice(0, $scope.modelName.lastIndexOf("."));
                $scope.fieldName = $scope.modelName.slice($scope.modelName.lastIndexOf(".") + 1);

                $attributes.$observe('ngModel', value =>
                {
                    $scope.$parent.$watch(value, (current, old) =>
                    {
                        if (!current) return;
                        if (current == old) return;
                        if (!angular.isObject(current))
                        {
                            try
                            {
                                //json can't parse HTML 
                                current = JSON.parse(current);
                            } catch (e)
                            {
                                current = current;
                            }
                        }

                        ngModel.$setViewValue(current);
                    });
                });


                $attributes.$observe('defaultText', value =>
                {
                    $scope.defaultText = value;
                });

                if ($attributes.value)
                {
                    $scope.value = $attributes.value;
                }

                if ($attributes.text)
                {
                    $scope.text = $attributes.text;
                }

                $scope.isModel = () =>
                {
                    return $scope.modelName.indexOf('.') > -1;
                };

                $scope.getText = (data: any) =>
                {
                    var text = Shared.ScopeHelper.GetObjectData(data, $attributes.text);
                    return text;

                };
                $scope.getValue = (data: any) =>
                {
                    var value = Shared.ScopeHelper.GetObjectData(data, $attributes.value);
                    return value;

                };
                $scope.getData = () =>
                {
                    return Shared.ScopeHelper.GetObjectData($scope.$parent, $attributes.data);
                };

            };
            directive.templateUrl = "App/template/selectBoxDirectiveTemplate.html";
            return directive;
        }

    }
    Main.App.Directives.directive("selectBox", () => new selectBoxDirective());
};