var Shared;
(function (Shared) {
    var MobilePhoneDirective = (function () {
        function MobilePhoneDirective($filter) {
            var directive = {};
            directive.restrict = "A";
            directive.require = "^ngModel";
            directive.link = (function ($scope, $element, $attrs, ngModel) {
                $scope.$watch($attrs.ngModel, function (newValue, oldValue) {
                    //defualt value
                    if (newValue == undefined)
                        return '';
                    if (newValue == null)
                        return '';
                    var input = newValue.toString();
                    var result = "";
                    input = input.replace(/[^0-9]/g, '');
                    if (input.length > 3) {
                        for (var i = 0; i < input.length; i++) {
                            if (i == 3) {
                                result = result.concat("-");
                            }
                            result = result.concat(input[i]);
                        }
                    }
                    else {
                        result = input;
                    }
                    if (result.length > 11) {
                        result = result.slice(0, 11);
                    }
                    ngModel.$setViewValue(result);
                    ngModel.$render();
                });
            });
            return directive;
        }
        return MobilePhoneDirective;
    }());
    Shared.MobilePhoneDirective = MobilePhoneDirective;
    Main.App.Directives.directive("mobilePhone", ["$filter", function ($filter) { return new MobilePhoneDirective($filter); }]);
})(Shared || (Shared = {}));
//# sourceMappingURL=mobilePhoneDirective.js.map