﻿module Shared
{
    export class EmailAccountDirective
    {
        constructor($filter: any)
        {
            var directive: ng.IDirective = {};
            directive.restrict = "A";
            directive.require = "^ngModel";
            directive.link = (($scope: ng.IScope, $element: any, $attrs: any, ngModel: ng.INgModelController) =>
            {
                $element.bind("keydown keypress", function (event)
                {
                    if (event.which === 32) // space
                    {
                        event.preventDefault();
                    }
                });

                // ตัด space ทิ้ง ตอน paste
                $element.bind("paste", function (event)
                {
                    var _this = this;
                    // Short pause to wait for paste to complete
                    setTimeout(function ()
                    {
                        var pastedData = $(_this).val();
                        pastedData = String(pastedData).replace(' ', '');
                        ngModel.$setViewValue(pastedData);
                        ngModel.$render();
                    }, 100);

                });

                $scope.$watch($attrs.ngModel, (newValue, oldValue) =>
                {
                    //defualt value
                    if (newValue == undefined) return '';
                    if (newValue == null) return '';

                    newValue = String(newValue);

                    var transformedInput;

                    transformedInput = newValue.replace(' ', '');

                    if (transformedInput != newValue)
                    {
                        ngModel.$setViewValue(transformedInput);
                        ngModel.$render();
                    }
                });
            });

            return directive;
        }
    }
    Main.App.Directives.directive("emailaccountTextbox", ["$filter", ($filter) => new EmailAccountDirective($filter)]);
}
