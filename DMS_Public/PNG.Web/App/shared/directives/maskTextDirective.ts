﻿module Shared
{
    export class MaskTextDirective
    {
        constructor($document: any)
        {
            var directive: ng.IDirective = {};
            directive.restrict = "A";
            directive.require = "^ngModel";
            directive.link = (($scope: ng.IScope, $element: any, $attrs: any, ngModel: ng.INgModelController) =>
            {
                var hideId: string;
                var tempValue: string;
                var hideElement = $($element, this);
                var showElement = hideElement;
                var paste: boolean = false;
                if ($attrs.id != "")
                {
                    hideId = 'hide_' + $attrs.id;
                }
                if (!document.querySelector('#' + hideId))
                {
                    hideElement = showElement.clone();
                    showElement.after(hideElement);
                    hideElement.css('display', 'none');
                    hideElement.removeAttr('ng-model');
                    hideElement.attr('id', hideId);
                }

                $element.bind('blur', () =>
                {
                    if ($attrs.showhide == "on")
                    {
                        hideElement.val(showElement.val());
                        var result = showElement.val().replace(/[\S/ /]/g, "\u25CF");
                        ngModel.$setViewValue(result);
                        ngModel.$render();
                    }
                });

                $element.bind('focus', (e) =>
                {
                    if ($attrs.showhide == "on")
                    {
                        ngModel.$setViewValue(hideElement.val());
                        ngModel.$render();
                    }
                });


                $element.bind('click', (e) =>
                {
                    if ($attrs.showhide == "off")
                    {
                        if (!(e.target.selectionEnd == e.target.value.length && e.target.selectionStart <= e.target.value.length))
                        {
                            e.target.selectionStart = e.target.value.length;
                            e.target.selectionEnd = e.target.value.length;
                        }
                    }
                });

                $scope.$watch($attrs.ngModel, function (newValue)
                {
                    if ($attrs.showhide == "off")
                    {
                        if (!paste)
                        {
                            var Input: string = newValue;
                            var mask: string = showElement.val();
                            Input = Input.substr(Input.length - 1, 1);
                            Input = hideElement.val() + Input;
                            Input = Input.slice(0, mask.length);
                            hideElement.val(Input);
                            var result = showElement.val().replace(/[\S/ /]/g, "\u25CF");
                            ngModel.$setViewValue(result);
                            ngModel.$render();

                        }
                        else
                        {
                            var Input: string = newValue;
                            var mask: string = showElement.val();
                            Input = Input.slice(0, mask.length);
                            hideElement.val(Input);
                            var result = showElement.val().replace(/[\S/ /]/g, "\u25CF");
                            ngModel.$setViewValue(result);
                            ngModel.$render();
                        }

                        paste = false;
                    }
                });
                $element.bind("paste", function (event)
                {
                    paste = true;
                });
            });

            return directive;
        }
    }
    Main.App.Directives.directive("maskText", ["$document", ($document) => new MaskTextDirective($document)]);

    export class RevertMaskTextDirective
    {
        constructor($document: any)
        {
            var directive: ng.IDirective = {};
            directive.restrict = "A";
            directive.require = "^ngModel";
            directive.link = (($scope: ng.IScope, $element: any, $attrs: any, ngModel: ng.INgModelController) =>
            {
                var hideId: string;
                var tempValue: string;
                var hideElement = $($element, this);
                var showElement = hideElement;

                if ($attrs.id != "")
                {
                    hideId = 'hide_' + $attrs.id;
                }

                if (!document.querySelector('#' + hideId))
                {
                    hideElement = showElement.clone();
                    showElement.after(hideElement);
                    hideElement.removeAttr('ng-model');
                    hideElement.attr('id', hideId);
                    hideElement.css('display', 'none');

                    $scope.$watch('ngModel', () =>
                    {
                        var result = ngModel.$viewValue.replace(/[\S]/g, "\u25CF");

                        hideElement.attr({ value: ngModel.$viewValue });
                        showElement.attr({ value: result });

                        ngModel.$setViewValue(result);
                        ngModel.$render();
                    });
                }

                $element.bind('blur', () =>
                {
                    angular.element(document).ready(() =>
                    {
                        hideElement.val(showElement.val());
                        var result = showElement.val().replace(/[\S]/g, "\u25CF");
                        ngModel.$setViewValue(result);
                        ngModel.$render();
                    });
                });

                $element.bind('focus', (e) =>
                {
                    if ($attrs.show)
                    {
                        if ($attrs.show == "true")
                        {
                            ngModel.$setViewValue(hideElement.val());
                            ngModel.$render();
                        }
                    }
                });
            });

            return directive;
        }
    }
    Main.App.Directives.directive("revertMaskText", ["$document", ($document) => new RevertMaskTextDirective($document)]);
}