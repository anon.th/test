var Shared;
(function (Shared) {
    var MaskTextDirective = (function () {
        function MaskTextDirective($document) {
            var _this = this;
            var directive = {};
            directive.restrict = "A";
            directive.require = "^ngModel";
            directive.link = (function ($scope, $element, $attrs, ngModel) {
                var hideId;
                var tempValue;
                var hideElement = $($element, _this);
                var showElement = hideElement;
                var paste = false;
                if ($attrs.id != "") {
                    hideId = 'hide_' + $attrs.id;
                }
                if (!document.querySelector('#' + hideId)) {
                    hideElement = showElement.clone();
                    showElement.after(hideElement);
                    hideElement.css('display', 'none');
                    hideElement.removeAttr('ng-model');
                    hideElement.attr('id', hideId);
                }
                $element.bind('blur', function () {
                    if ($attrs.showhide == "on") {
                        hideElement.val(showElement.val());
                        var result = showElement.val().replace(/[\S/ /]/g, "\u25CF");
                        ngModel.$setViewValue(result);
                        ngModel.$render();
                    }
                });
                $element.bind('focus', function (e) {
                    if ($attrs.showhide == "on") {
                        ngModel.$setViewValue(hideElement.val());
                        ngModel.$render();
                    }
                });
                $element.bind('click', function (e) {
                    if ($attrs.showhide == "off") {
                        if (!(e.target.selectionEnd == e.target.value.length && e.target.selectionStart <= e.target.value.length)) {
                            e.target.selectionStart = e.target.value.length;
                            e.target.selectionEnd = e.target.value.length;
                        }
                    }
                });
                $scope.$watch($attrs.ngModel, function (newValue) {
                    if ($attrs.showhide == "off") {
                        if (!paste) {
                            var Input = newValue;
                            var mask = showElement.val();
                            Input = Input.substr(Input.length - 1, 1);
                            Input = hideElement.val() + Input;
                            Input = Input.slice(0, mask.length);
                            hideElement.val(Input);
                            var result = showElement.val().replace(/[\S/ /]/g, "\u25CF");
                            ngModel.$setViewValue(result);
                            ngModel.$render();
                        }
                        else {
                            var Input = newValue;
                            var mask = showElement.val();
                            Input = Input.slice(0, mask.length);
                            hideElement.val(Input);
                            var result = showElement.val().replace(/[\S/ /]/g, "\u25CF");
                            ngModel.$setViewValue(result);
                            ngModel.$render();
                        }
                        paste = false;
                    }
                });
                $element.bind("paste", function (event) {
                    paste = true;
                });
            });
            return directive;
        }
        return MaskTextDirective;
    }());
    Shared.MaskTextDirective = MaskTextDirective;
    Main.App.Directives.directive("maskText", ["$document", function ($document) { return new MaskTextDirective($document); }]);
    var RevertMaskTextDirective = (function () {
        function RevertMaskTextDirective($document) {
            var _this = this;
            var directive = {};
            directive.restrict = "A";
            directive.require = "^ngModel";
            directive.link = (function ($scope, $element, $attrs, ngModel) {
                var hideId;
                var tempValue;
                var hideElement = $($element, _this);
                var showElement = hideElement;
                if ($attrs.id != "") {
                    hideId = 'hide_' + $attrs.id;
                }
                if (!document.querySelector('#' + hideId)) {
                    hideElement = showElement.clone();
                    showElement.after(hideElement);
                    hideElement.removeAttr('ng-model');
                    hideElement.attr('id', hideId);
                    hideElement.css('display', 'none');
                    $scope.$watch('ngModel', function () {
                        var result = ngModel.$viewValue.replace(/[\S]/g, "\u25CF");
                        hideElement.attr({ value: ngModel.$viewValue });
                        showElement.attr({ value: result });
                        ngModel.$setViewValue(result);
                        ngModel.$render();
                    });
                }
                $element.bind('blur', function () {
                    angular.element(document).ready(function () {
                        hideElement.val(showElement.val());
                        var result = showElement.val().replace(/[\S]/g, "\u25CF");
                        ngModel.$setViewValue(result);
                        ngModel.$render();
                    });
                });
                $element.bind('focus', function (e) {
                    if ($attrs.show) {
                        if ($attrs.show == "true") {
                            ngModel.$setViewValue(hideElement.val());
                            ngModel.$render();
                        }
                    }
                });
            });
            return directive;
        }
        return RevertMaskTextDirective;
    }());
    Shared.RevertMaskTextDirective = RevertMaskTextDirective;
    Main.App.Directives.directive("revertMaskText", ["$document", function ($document) { return new RevertMaskTextDirective($document); }]);
})(Shared || (Shared = {}));
//# sourceMappingURL=maskTextDirective.js.map