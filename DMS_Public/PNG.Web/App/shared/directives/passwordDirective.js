var Shared;
(function (Shared) {
    var PasswordDirective = (function () {
        function PasswordDirective($filter) {
            var directive = {};
            directive.restrict = "A";
            directive.require = "^ngModel";
            directive.link = (function ($scope, $element, $attrs, ngModel) {
                $scope.$watch($attrs.ngModel, function (newValue, oldValue) {
                    //defualt value
                    if (newValue == undefined)
                        return '';
                    if (newValue == null)
                        return '';
                    newValue = String(newValue);
                    var transformedInput;
                    transformedInput = newValue.trim(); //newValue.replace(/\s/g, '');
                    if (transformedInput != newValue) {
                        ngModel.$setViewValue(transformedInput);
                        ngModel.$render();
                    }
                });
            });
            return directive;
        }
        return PasswordDirective;
    }());
    Shared.PasswordDirective = PasswordDirective;
    Main.App.Directives.directive("passwordRule", ["$filter", function ($filter) { return new PasswordDirective($filter); }]);
})(Shared || (Shared = {}));
//# sourceMappingURL=passwordDirective.js.map