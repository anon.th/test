﻿module Shared
{
    export class AreaDisableDirective
    {
        constructor($timeout: any)
        {
            var directive: ng.IDirective = {};
            directive.restrict = "EA";
            directive.scope = {
                isDisabled: '=ngDisabled'
            };
            directive.link = ($scope: any, $element: any, $attrs: any) =>
            {
                $scope.$watch('isDisabled', (disabled) =>
                {
                    if (disabled == "true" || disabled == true)
                    {
                        $timeout(() =>
                        {
                            $element.find('input, select, textarea').prop('disabled', disabled);
                        }, 200);
                    }
                    else
                    {
                        $element.find('input, select, textarea').removeAttr('disabled');
                    }
                });
            };
            return directive;
        }
    }

    Main.App.Directives.directive("areaDisable", ["$timeout", ($timeout) => new AreaDisableDirective($timeout)]);
}