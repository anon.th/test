/// <reference path="../../_references.ts" />
var Shared;
(function (Shared) {
    var PaginationDirective = (function () {
        function PaginationDirective() {
            var directive = {};
            directive.restrict = 'AE';
            // TODO: two way data bind to attributes
            directive.scope =
                {
                    totalCount: '=',
                    recordsPerPage: '=',
                    urlPrefix: '=',
                    urlPostfix: '=',
                    currentPageId: '=',
                    skip: '=',
                    previousLabel: '=',
                    nextLabel: '=',
                    maxSize: '=',
                    pagingZone: '='
                };
            directive.link = (function ($scope, $element, $attributes) {
                // TODO: add a watch onto totalCount to see when we need to create the page links
                // TODO: this should only happen if the total count is greated then 0
                $scope.$watch('[totalCount,currentPageId]', function () {
                    // TODO: instantiate new page links
                    $scope.PageLinks = [];
                    if ($scope.totalCount > 0) {
                        // TODO: get paging zone from $scope
                        var pagingZone = $scope.pagingZone != undefined ? $scope.pagingZone : "";
                        // TODO: calculate total pages
                        var totalPages = Math.ceil($scope.totalCount / $scope.recordsPerPage);
                        // TODO: create tne next and back skip intervals 
                        var nextSkipInterval = ($scope.skip + $scope.recordsPerPage);
                        var backSkipInterval = ($scope.skip - $scope.recordsPerPage);
                        // TODO: create the new page Ids for back and next links
                        var backPageId = $scope.currentPageId - 1;
                        var nextPageId = $scope.currentPageId + 1;
                        var i = 0;
                        // TODO: Make sure we need to generate the links
                        if (!($scope.recordsPerPage >= $scope.totalCount)) {
                            // TODO: Make a back link
                            $scope.PageLinks.push({
                                Id: i,
                                Label: $attributes.previousLabel != undefined ? $attributes.previousLabel : "«",
                                Link: $scope.skip <= 0 ? null : $scope.urlPrefix + "page/" + backPageId + "/skip/" + backSkipInterval + "/" + $scope.urlPostfix + pagingZone,
                                Current: false,
                                Disabled: $scope.skip <= 0 ? true : false
                            });
                            i = i + 1;
                            // TODO: Make all links between back and next
                            var individualSkip = 0;
                            try {
                                // TODO: Add new links between back and next
                                var maxSize = $scope.maxSize != undefined ? $scope.maxSize : 10;
                                var startPage = 1;
                                var endPage = totalPages;
                                var isMaxSized = angular.isDefined(maxSize) && maxSize < totalPages;
                                // TODO: Recompute if maxSize
                                if (isMaxSized) {
                                    // TODO: Visible pages are paginated with maxSize
                                    startPage = ((Math.ceil($scope.currentPageId / maxSize) - 1) * maxSize) + 1;
                                    // TODO: Adjust last page if limit is exceeded
                                    endPage = Math.min(startPage + maxSize - 1, totalPages);
                                }
                                // TODO: Add links between back and next
                                for (i = startPage; i <= endPage; i++) {
                                    individualSkip = (i - 1) * $scope.recordsPerPage;
                                    $scope.PageLinks.push({
                                        Id: i,
                                        Label: i.toString(),
                                        Link: i == $scope.currentPageId ? null : $scope.urlPrefix + "page/" + i + "/skip/" + individualSkip + "/" + $scope.urlPostfix + pagingZone,
                                        Current: i == $scope.currentPageId ? true : false,
                                        Disabled: false
                                    });
                                }
                                // TODO: Add links to move between page sets
                                if (isMaxSized) {
                                    if (startPage > 1) {
                                        individualSkip = (startPage - 2) * $scope.recordsPerPage;
                                        $scope.PageLinks.unshift({
                                            Id: (startPage - 1),
                                            Label: '...',
                                            Link: $scope.urlPrefix + "page/" + (startPage - 1) + "/skip/" + individualSkip + "/" + $scope.urlPostfix + pagingZone,
                                            Current: i == $scope.currentPageId ? true : false,
                                            Disabled: false
                                        });
                                    }
                                    if (endPage < totalPages) {
                                        individualSkip = endPage * $scope.recordsPerPage;
                                        $scope.PageLinks.push({
                                            Id: (endPage + 1),
                                            Label: '...',
                                            Link: $scope.urlPrefix + "page/" + (endPage + 1) + "/skip/" + individualSkip + "/" + $scope.urlPostfix + pagingZone,
                                            Current: i == $scope.currentPageId ? true : false,
                                            Disabled: false
                                        });
                                    }
                                }
                            }
                            catch (err) {
                                // TODO: Reset PageLinks object and make default pagination method
                                $scope.PageLinks = [];
                                i = 0;
                                // TODO: Make a back link
                                $scope.PageLinks.push({
                                    Id: i,
                                    Label: $attributes.previousLabel != undefined ? $attributes.previousLabel : "«",
                                    Link: $scope.skip <= 0 ? null : $scope.urlPrefix + "page/" + backPageId + "/skip/" + backSkipInterval + "/" + $scope.urlPostfix + pagingZone,
                                    Current: false,
                                    Disabled: $scope.skip <= 0 ? true : false
                                });
                                i = i + 1;
                                while (i <= totalPages) {
                                    $scope.PageLinks.push({
                                        Id: i,
                                        Label: i.toString(),
                                        Link: i == $scope.currentPageId ? null : $scope.urlPrefix + "page/" + i + "/skip/" + individualSkip + "/" + $scope.urlPostfix + pagingZone,
                                        Current: i == $scope.currentPageId ? true : false,
                                        Disabled: false
                                    });
                                    individualSkip = individualSkip + $scope.recordsPerPage;
                                    i++;
                                }
                            }
                            // TODO: Make a next link
                            $scope.PageLinks.push({
                                Id: i,
                                Label: $attributes.nextLabel != undefined ? $attributes.nextLabel : "»",
                                Link: nextSkipInterval >= $scope.totalCount ? null : $scope.urlPrefix + "page/" + nextPageId + "/skip/" + nextSkipInterval + "/" + $scope.urlPostfix + pagingZone,
                                Current: false,
                                Disabled: nextSkipInterval >= $scope.totalCount ? true : false
                            });
                        }
                    }
                });
                // TODO: Prevent next object firing event
                $scope.GotoLink = function (event) {
                    // TODO: Return value to browser
                    event.returnValue = false;
                    event.preventDefault();
                    // TODO: Prevent firing event
                    event.stopPropagation();
                };
            });
            directive.templateUrl = "App/template/paginationTemplate.html";
            return directive;
        }
        return PaginationDirective;
    }());
    Shared.PaginationDirective = PaginationDirective;
    Main.App.Directives.directive("dirPagination", function () { return new PaginationDirective(); });
})(Shared || (Shared = {}));
//# sourceMappingURL=paginationDirective.js.map