﻿module Shared
{
    export class MobilePhoneDirective
    {
        constructor($filter: any)
        {
            var directive: ng.IDirective = {};
            directive.restrict = "A";
            directive.require = "^ngModel";
            directive.link = (($scope: ng.IScope, $element: any, $attrs: any, ngModel: ng.INgModelController) =>
            {
                $scope.$watch($attrs.ngModel, (newValue, oldValue) =>
                {
                    //defualt value
                    if (newValue == undefined) return '';
                    if (newValue == null) return '';

                    var input: string = newValue.toString();
                    var result: string = "";
                    input = input.replace(/[^0-9]/g, '');

                    if (input.length > 3)
                    {
                        for (var i = 0; i < input.length; i++)
                        {
                            if (i == 3) { result = result.concat("-"); }
                            result = result.concat(input[i]);
                        }
                    }
                    else
                    {
                        result = input;
                    }

                    if (result.length > 11) { result = result.slice(0, 11); }

                    ngModel.$setViewValue(result);
                    ngModel.$render();
                });
            });

            return directive;
        }
    }
    Main.App.Directives.directive("mobilePhone", ["$filter", ($filter) => new MobilePhoneDirective($filter)]);
}