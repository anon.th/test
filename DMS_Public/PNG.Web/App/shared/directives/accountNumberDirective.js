var Shared;
(function (Shared) {
    var AccountNumberDirective = (function () {
        function AccountNumberDirective($filter) {
            var directive = {};
            directive.restrict = "A";
            directive.require = "^ngModel";
            directive.link = (function ($scope, $element, $attrs, ngModel) {
                $element.bind("keydown keypress", function (event) {
                    if (event.which === 32) {
                        event.preventDefault();
                    }
                });
                $scope.$watch($attrs.ngModel, function (newValue, oldValue) {
                    //defualt value
                    if (newValue == undefined)
                        return '';
                    if (newValue == null)
                        return '';
                    newValue = String(newValue);
                    var transformedInput;
                    transformedInput = newValue.replace(/[^0-9]/g, '');
                    if ($attrs.customMaxlength != undefined) {
                        transformedInput = transformedInput.substring(0, parseInt($attrs.customMaxlength));
                    }
                    if (transformedInput != newValue) {
                        ngModel.$setViewValue(transformedInput);
                        ngModel.$render();
                    }
                });
            });
            return directive;
        }
        return AccountNumberDirective;
    }());
    Shared.AccountNumberDirective = AccountNumberDirective;
    Main.App.Directives.directive("accountnumberTextbox", ["$filter", function ($filter) { return new AccountNumberDirective($filter); }]);
})(Shared || (Shared = {}));
//# sourceMappingURL=accountNumberDirective.js.map