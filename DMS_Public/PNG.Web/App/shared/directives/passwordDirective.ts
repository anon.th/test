﻿module Shared
{
    export class PasswordDirective
    {
        constructor($filter: any)
        {
            var directive: ng.IDirective = {};
            directive.restrict = "A";
            directive.require = "^ngModel";
            directive.link = (($scope: ng.IScope, $element: any, $attrs: any, ngModel: ng.INgModelController) =>
            {

                $scope.$watch($attrs.ngModel, (newValue, oldValue) =>
                {
                    //defualt value
                    if (newValue == undefined) return '';
                    if (newValue == null) return '';

                    newValue = String(newValue);

                    var transformedInput;

                    transformedInput = newValue.trim(); //newValue.replace(/\s/g, '');

                    if (transformedInput != newValue)
                    {
                        ngModel.$setViewValue(transformedInput);
                        ngModel.$render();
                    }
                });


            });

            return directive;
        }
    }
    Main.App.Directives.directive("passwordRule", ["$filter", ($filter) => new PasswordDirective($filter)]);
}