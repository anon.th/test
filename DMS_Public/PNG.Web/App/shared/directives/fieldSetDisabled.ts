﻿module Shared
{
    export class FieldsetDisabledDirective
    {
        constructor($timeout: any)
        {
            var directive: ng.IDirective = {};
            directive.restrict = "E";
            directive.scope = {
                isDisabled: '=ngDisabled'
            };
            directive.link = ($scope: any, $element: any, $attrs: any) =>
            {
                $scope.$watch('isDisabled', (disabled) =>
                {
                    if (disabled == "true" || disabled == true)
                    {
                        $element.find('input, select, textarea').prop('disabled', disabled);
                    }
                });
            };
            return directive;
        }
    }

    Main.App.Directives.directive("fieldset", ["$timeout", ($timeout) => new FieldsetDisabledDirective($timeout)]);
}