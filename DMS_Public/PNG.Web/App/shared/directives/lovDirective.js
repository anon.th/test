var Lov;
(function (Lov) {
    var LovDirective = (function () {
        function LovDirective() {
            var directive = {};
            directive.restrict = "AE";
            directive.require = "^ngModel";
            directive.scope = {
                ngOpenClick: '&',
                ngClearClick: '&',
                //เพิ่ม ngBlur กรณีที่พิมพ์ข้อมูลลลง Lov แล้วเจอข้อมูล
                ngBlur: '&',
                ngDisabled: "=",
                selectedDisabled: "=",
                iconOpenDisabled: "=",
                iconClearDisabled: "=",
                iconOpenHidden: "=",
                iconClearHidden: "=",
                textDisabled: "=",
                textCodeDisabled: "="
            };
            directive.link = function ($scope, $element, $attributes, ngModel, $timeout) {
                $scope.modelName = $attributes.ngModel;
                $scope.entityName = $scope.modelName.slice(0, $scope.modelName.lastIndexOf("."));
                $scope.fieldName = $scope.modelName.slice($scope.modelName.lastIndexOf(".") + 1);
                $scope.isModel = function () {
                    return $scope.modelName.indexOf('.') > -1;
                };
                $attributes.$observe('ngModel', function (value) {
                    $scope.$parent.$watch(value, function (current, old) {
                        if (!current)
                            return;
                        if ($scope.isModel()) {
                            $scope.my = $scope.$parent[$scope.entityName];
                        }
                        else {
                            $scope.entityName = $scope.modelName;
                            $scope.my = $scope.$parent;
                        }
                    });
                });
                $scope.$watch('ngDisabled', function (current, old) {
                    var element = angular.element("#" + $attributes.id + " .lovText");
                    var element2 = angular.element("#" + $attributes.id + " .lovCode");
                    if (current == true) {
                        element.attr("placeholder", "");
                        element2.attr("placeholder", "");
                    }
                    else {
                        element.attr("placeholder", $scope.textDefault);
                        element2.attr("placeholder", $scope.textCodeDefault);
                    }
                });
                if ($attributes.textDefault) {
                    $scope.textDefault = $attributes.textDefault;
                }
                if ($attributes.textCodeDefault) {
                    $scope.textCodeDefault = $attributes.textCodeDefault;
                }
                if ($attributes.id) {
                    $scope.id = $attributes.id;
                }
                if ($attributes.value) {
                    $scope.value = $attributes.value;
                }
                else {
                    $scope.value = "Id";
                }
                if ($attributes.code) {
                    $scope.code = $attributes.code;
                }
                else {
                    $scope.code = "Code";
                }
                if ($attributes.text) {
                    $scope.text = $attributes.text;
                }
                else {
                    $scope.text = "Name";
                }
                if ($attributes.codeDisabled) {
                    $scope.codeDisabled = $attributes.codeDisabled;
                }
                if ($attributes.cssCode) {
                    $scope.cssCode = $attributes.cssCode;
                }
                else {
                    $scope.cssCode = "col-xs-3 no-offset";
                }
                if ($attributes.cssText) {
                    $scope.cssText = $attributes.cssText;
                }
                else {
                    $scope.cssText = "col-xs-9 alpha";
                }
                $scope.disabledWhenSelected = function () {
                    if (!$scope.selectedDisabled || !$scope.my)
                        return false;
                    if ($scope.codeDisabled) {
                        return Shared.ScopeHelper.GetObjectData($scope.my, $scope.codeDisabled);
                    }
                    else {
                        return Shared.ScopeHelper.GetObjectData($scope.my, $scope.fieldName + "." + $scope.value);
                    }
                };
            };
            directive.templateUrl = "App/template/lovDirectiveTemplate.html";
            return directive;
        }
        return LovDirective;
    }());
    Lov.LovDirective = LovDirective;
    Main.App.Directives.directive("lov", function () { return new LovDirective(); });
})(Lov || (Lov = {}));
;
//# sourceMappingURL=lovDirective.js.map