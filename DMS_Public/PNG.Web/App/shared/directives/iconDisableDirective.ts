﻿module Shared
{
    export class IconDisableDirective
    {
        constructor($filter: any)
        {
            var directive: ng.IDirective = {};
            directive.restrict = "C";
            directive.link = (($scope: ng.IScope, $element: any, $attrs: any, ngModel: ng.INgModelController) =>
            {
                setTimeout(function ()
                {
                    if ($element.attr("class").indexOf("icon-disable") != -1)
                    {
                        $element.off('click');
                        $element.unbind("mouseenter mouseleave");
                    }
                }, 100);
            });

            return directive;
        }
    }
    Main.App.Directives.directive("fixIe", ["$filter", ($filter) => new IconDisableDirective($filter)]);
}