var SelectBox;
(function (SelectBox) {
    var selectBoxDirective = (function () {
        function selectBoxDirective() {
            var directive = {};
            directive.restrict = "AE";
            directive.require = "^ngModel";
            directive.scope = {
                ngDisabled: "=",
                ngModel: "="
            };
            directive.link = function ($scope, $element, $attributes, ngModel, $timeout) {
                $scope.myScope = $scope;
                $attributes.$observe("selecttooltip", function () {
                    if ($attributes.selecttooltip === undefined)
                        return;
                    $scope.selecttooltip = $attributes.selecttooltip;
                });
                $scope.modelName = $attributes.ngModel;
                $scope.entityName = $scope.modelName.slice(0, $scope.modelName.lastIndexOf("."));
                $scope.fieldName = $scope.modelName.slice($scope.modelName.lastIndexOf(".") + 1);
                $attributes.$observe('ngModel', function (value) {
                    $scope.$parent.$watch(value, function (current, old) {
                        if (!current)
                            return;
                        if (current == old)
                            return;
                        if (!angular.isObject(current)) {
                            try {
                                //json can't parse HTML 
                                current = JSON.parse(current);
                            }
                            catch (e) {
                                current = current;
                            }
                        }
                        ngModel.$setViewValue(current);
                    });
                });
                $attributes.$observe('defaultText', function (value) {
                    $scope.defaultText = value;
                });
                if ($attributes.value) {
                    $scope.value = $attributes.value;
                }
                if ($attributes.text) {
                    $scope.text = $attributes.text;
                }
                $scope.isModel = function () {
                    return $scope.modelName.indexOf('.') > -1;
                };
                $scope.getText = function (data) {
                    var text = Shared.ScopeHelper.GetObjectData(data, $attributes.text);
                    return text;
                };
                $scope.getValue = function (data) {
                    var value = Shared.ScopeHelper.GetObjectData(data, $attributes.value);
                    return value;
                };
                $scope.getData = function () {
                    return Shared.ScopeHelper.GetObjectData($scope.$parent, $attributes.data);
                };
            };
            directive.templateUrl = "App/template/selectBoxDirectiveTemplate.html";
            return directive;
        }
        return selectBoxDirective;
    }());
    SelectBox.selectBoxDirective = selectBoxDirective;
    Main.App.Directives.directive("selectBox", function () { return new selectBoxDirective(); });
})(SelectBox || (SelectBox = {}));
;
//# sourceMappingURL=selectBoxDirective.js.map