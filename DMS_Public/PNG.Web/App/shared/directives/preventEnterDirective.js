var Shared;
(function (Shared) {
    var PreventEnterDirective = (function () {
        function PreventEnterDirective() {
            var directive = {};
            directive.restrict = "A";
            directive.require = "^ngModel";
            directive.link = function (scope, $element, $attrs, ngModel) {
                $element.bind("keypress", function (event) {
                    var key = event.keyCode || event.which;
                    if (key === 13) {
                        event.returnValue = false; // for IE
                        event.preventDefault(); // for any browser
                    }
                    event.stopPropagation(); // not goto another event
                });
            };
            return directive;
        }
        return PreventEnterDirective;
    }());
    Shared.PreventEnterDirective = PreventEnterDirective;
    Main.App.Directives.directive("preventEnter", function () { return new PreventEnterDirective(); });
})(Shared || (Shared = {}));
//# sourceMappingURL=preventEnterDirective.js.map