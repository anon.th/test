﻿//** Modify By Nayfido 15.12.2558 **//
// สามารถ compare กับฟิลที่ต้องการ และแสดงการแจ้งเตือนทั้ง 2 ฟิล
// ป้องกันการแจ้งเตือนซ้ำใน alert

module Validation
{
    export class modelValidationDirective
    {

        public static Get($parse: ng.IParseService)
        {
            var directive: ng.IDirective = {};

            (<any>directive).bbl = Math.random() * 100;

            directive.restrict = "A";
            directive.replace = true;
            directive.require = "^ngModel";

            directive.link = ($scope: ng.IScope, $element: any, $attributes: any, ngModel: ng.INgModelController) =>
            {
                var modelName = $attributes.ngModel;
                var count = 0;
                var countfirst = 0;
                var fieldName = modelName.slice(modelName.indexOf(".") + 1);
                var entityName = modelName.slice(0, modelName.indexOf("."));

                if ($element.context.localName == 'select-box' || $element.context.localName == 'lov')
                {
                    $scope.$watch("ngModel", () =>
                    {
                        validate(ngModel.$modelValue);
                    });
                }
                else
                {
                    $attributes.$observe('ngModel', value =>
                    {
                        $scope.$watch(value, (current, old) =>
                        {
                            if (current == old) return;
                            validate(ngModel.$modelValue);
                        });
                    });
                }

                var validate = (value) =>
                {
                    var entity = Shared.ScopeHelper.GetValue($scope, entityName);
                    if (!entity || !entity.Validator)
                    {
                        return undefined;
                    }

                    //เช็ค validate

                    var valid = entity.Validator.ValidateField(entity, angular.isUndefined(value) ? null : value, fieldName);

                    //กำหนด ng-invalid หรือ ng-valid (default = ng-invalid) ซึ่งส่งผลต่อสีที่แสดงรอบ input

                    ngModel.$setValidity(modelName, valid);

                    if ($attributes.comparewith)
                    {
                        //get ng-model จากฟิลที่ compare
                        var compareElementInput: ng.IAugmentedJQuery = angular.element("input[ng-model='" + $attributes.comparewith + "']");
                        var compareElementDiv: ng.IAugmentedJQuery = angular.element("div[ng-model='" + $attributes.comparewith + "']");
                        var compareElementLabel: ng.IAugmentedJQuery = angular.element("label[showerror='" + $attributes.comparewith + "']");

                        // ถ้าเป็น tag input
                        if (compareElementInput.length > 0)
                        {
                            var compareModel: any = compareElementInput.data('$ngModelController');
                        }
                        // ถ้าเป็น tag div
                        else if (compareElementDiv.length > 0)
                        {
                            var compareModel: any = compareElementDiv.data('$ngModelController');
                        }

                        compareModel.$setValidity($attributes.comparewith, valid);

                        //ถ้าฟิล ngModel ถูกสัมผัส ก็ให้ทำการ removeclass ng-pristine จากฟิลที่ compare
                        if (ngModel.$dirty)
                        {
                            // ถ้าเป็น tag input
                            if (compareElementInput.length > 0)
                            {
                                compareElementInput.removeClass('ng-pristine');
                            }
                            // ถ้าเป็น tag div
                            else if (compareElementDiv.length > 0)
                            {
                                compareElementDiv.removeClass('ng-pristine');
                            }
                        }
                    }

                    var currentError = entity.Validator.CurrentError(fieldName);
                    var showerror = currentError;
                    //กำหนดข้อความให้กับ tooltip
                    if (currentError)
                    {

                        setErrorMessage(currentError);
                    }
                    else
                    {

                        setErrorMessage(currentError);
                    }
                    return value;
                };
                var setErrorMessage = (errorMessage) =>
                {
                    if ($attributes.$attr.tooltip)
                    {
                        $attributes.$set('tooltip', errorMessage);
                    }
                    else if ($attributes.$attr.selecttooltip)
                    {
                        $attributes.$set('selecttooltip', errorMessage);
                    }
                    else if ($attributes.$attr.showerror)
                    {
                        //do not showerror firstload
                        $attributes.$set('showerror', errorMessage);

                        //แสดง  error จาก validate
                        if (ngModel.$modelValue != undefined)
                        {
                            var myContainer = <HTMLElement>document.querySelector("label[showerror='" + $attributes.showerror + "'][ng-model='" + modelName + "']");
                            myContainer.innerText = $attributes.showerror;
                        }
                    }

                }; //For the input validation element when focus and do not type anything and go to another input it does not show red border.
                //I have add bind onblur to the input validation after onblur it will check validation for show red border when invalid.
                switch ($element.context.localName)
                {
                    case 'input':


                    case 'div':
                        //เมื่อ blue จาก input แล้วทำการ removeclass ng-pristine ซึ่งส่งผลต่อสีที่แสดงรอบ input

                        $element.bind('blur', () =>
                        {
                            $element.removeClass('ng-pristine'); // HACK. replace with $setPristine(false), not sure how to get to it
                        });

                        break;
                }

                // if a watch expression is specified, add a watch
                if ($attributes.watch)
                {
                    $scope.$watch($parse($attributes.watch), validate);
                }

                // validate when the validator changes
                $scope.$watch($parse(entityName + ".Validator"), () =>
                {
                    validate(ngModel.$modelValue);
                });

                ngModel.$formatters.unshift(validate);
                ngModel.$parsers.unshift(validate);
            };

            return directive;
        }
    }

    Main.App.Directives.directive("modelValidation", ["$parse", "$translate", ($parse, $translate) => modelValidationDirective.Get($parse)]);
};
