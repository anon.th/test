﻿module Shared
{
    export class DatePickerDirective
    {
        constructor($filter: any)
        {
            var directive: ng.IDirective = {};
            directive.restrict = "AE";
            directive.require = "^ngModel";
            directive.scope = {
                ngModel: "=",
                ngDisabled: "=",
                ngChange: '&'
            };

            directive.link = (($scope: any, $element: any, $attrs: any, ngModel: ng.INgModelController) =>
            {
                var options = {
                    autoclose: true,
                    format: 'dd/mm/yyyy',
                    language: 'th-th', /*'th-th'*/
                    todayBtn: 'linked',
                    todayHighlight: true,
                    forceParse: false
                };

                $scope.modelName = $attrs.ngModel;
                $scope.entityName = $scope.modelName.slice(0, $scope.modelName.lastIndexOf("."));
                $scope.fieldName = $scope.modelName.slice($scope.modelName.lastIndexOf(".") + 1);
                $scope.isModel = () =>
                {
                    return $scope.modelName.indexOf('.') > -1;
                };

                $attrs.$observe('ngModel', value =>
                {
                    $scope.$parent.$watch(value, (current, old) =>
                    {
                        // Backup: if (!current) return;
                        // Backup: if (current == old) return;
                        $scope.displayValue = $filter('date')(current, 'dd/MM/yyyy');/*buddhaDate*/
                    });
                });

                $scope.isValidDate2 = (s: any): boolean =>
                {
                    if (!s) return false;
                    var bits = s.split('-');
                    var y = bits[0], m = bits[1], d = bits[2];
                    // Assume not leap year by default (note zero index for Jan)
                    var daysInMonth = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

                    // If evenly divisible by 4 and not evenly divisible by 100,
                    // or is evenly divisible by 400, then a leap year
                    if ((!(y % 4) && y % 100) || !(y % 400))
                    {
                        daysInMonth[1] = 29;
                    }
                    return d <= daysInMonth[--m]
                }

                $scope.getDatepickerValue = (dpDate: any): Date =>
                {
                    var result = null;
                    var isLeapCorrect = true;
                    var day, month, year;
                    if (dpDate && typeof dpDate === "string")
                    {
                        dpDate = dpDate.trim();

                        var stringDate: string[];
                        var splitKey = "/";
                        if (dpDate.toString().indexOf("/") > -1)
                        {
                            splitKey = "/";
                        }
                        else if (dpDate.toString().indexOf("-") > -1)
                        {
                            splitKey = "-";
                        }
                        stringDate = dpDate.toString().split(splitKey);
                        if (stringDate.length == 3)
                        {
                            year = parseInt(stringDate[2]);
                            if (year < 1000 || year > 9999)
                                return null;
                            if (year > 2200)
                            {
                                year = year - 543;
                            }
                            day = stringDate[0].length == 1 ? '0' + stringDate[0] : stringDate[0];
                           
                            month = stringDate[1].length == 1 ? '0' + stringDate[1] : stringDate[1];
                            result = year + '-' + month + '-' + day;
                            if (parseInt(month) == 2 && parseInt(day) == 29)
                            {
                                isLeapCorrect = new Date(year, 1, 29).getMonth() == 1;
                            }
                        }
                        else
                        {
                            return null;
                        }
                    }
                    if ($scope.isValidDate2(result) && isLeapCorrect)
                        return result;
                    else
                        return null;
                };

                if ($attrs.startDate)
                {
                    options = $.extend({}, options, { startDate: Shared.ScopeHelper.GetValue($scope, $attrs.startDate) });
                }

                if ($attrs.endDate)
                {
                    options = $.extend({}, options, { endDate: Shared.ScopeHelper.GetValue($scope, $attrs.endDate) });
                }

                $scope.showDatepickerValue = (): void =>
                {
                    $element.datepicker(options)
                        .on('changeDate', e =>
                        {
                            var dateValue = new Date(Date.UTC(e.date.getFullYear(), e.date.getMonth(), e.date.getDate(), 0, 0, 0, 0));
                            var dateReturnValue = $filter('date')(dateValue, 'dd/MM/yyyy');
                            
                            ngModel.$setViewValue(dateReturnValue);
                            $scope.$parent.$apply();
                        });
                }

                if ($attrs.class.indexOf('datepicker-disabled') == (-1))
                {
                    $scope.showDatepickerValue();
                }
                else
                {
                    $scope.disabledValue = true;
                }

                var input = $element.find("input[type='text']");

                input.bind('blur', function ()
                {
                    var dateValue = $scope.getDatepickerValue(input.val());
                    var dateReturnValue = $filter('date')(dateValue, 'dd/MM/yyyy') //'yyyy-MM-dd'

                    if (!dateReturnValue
                        || (dateReturnValue && dateReturnValue.indexOf('NaN') > (-1)))
                    {
                        dateReturnValue = null;
                        $scope.displayValue = null;
                    }
                    else
                    {
                        $scope.displayValue = $filter('date')(dateReturnValue); //buddhaDate

                        //if ($attrs.startDate)
                        //{
                        //    var startDate = Shared.ScopeHelper.GetValue($scope, $attrs.startDate);
                        //    startDate = $filter('date')(startDate, 'dd/MM/yyyy');

                        //    if (dateReturnValue < startDate)
                        //    {
                        //        dateReturnValue = null;
                        //        $scope.displayValue = null;
                        //    }
                        //}

                        //if ($attrs.endDate)
                        //{
                        //    var endDate = Shared.ScopeHelper.GetValue($scope, $attrs.endDate);
                        //    endDate = $filter('date')(endDate, 'dd/MM/yyyy');

                        //    if (dateReturnValue > endDate)
                        //    {
                        //        dateReturnValue = null;
                        //        $scope.displayValue = null;
                        //    }
                        //}
                    }

                    ngModel.$setViewValue(dateReturnValue);
                    $scope.$parent.$apply();
                });
            });

            directive.templateUrl = "App/template/datepickerDirectiveTemplate.html";
            return directive;
        }
    }
    Main.App.Directives.directive("datePicker", ["$filter", ($filter) => new DatePickerDirective($filter)]);
}