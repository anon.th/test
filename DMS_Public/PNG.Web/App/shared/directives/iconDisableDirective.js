var Shared;
(function (Shared) {
    var IconDisableDirective = (function () {
        function IconDisableDirective($filter) {
            var directive = {};
            directive.restrict = "C";
            directive.link = (function ($scope, $element, $attrs, ngModel) {
                setTimeout(function () {
                    if ($element.attr("class").indexOf("icon-disable") != -1) {
                        $element.off('click');
                        $element.unbind("mouseenter mouseleave");
                    }
                }, 100);
            });
            return directive;
        }
        return IconDisableDirective;
    }());
    Shared.IconDisableDirective = IconDisableDirective;
    Main.App.Directives.directive("fixIe", ["$filter", function ($filter) { return new IconDisableDirective($filter); }]);
})(Shared || (Shared = {}));
//# sourceMappingURL=iconDisableDirective.js.map