﻿module Shared
{
    export class AccountNumberDirective
    {
        constructor($filter: any)
        {
            var directive: ng.IDirective = {};
            directive.restrict = "A";
            directive.require = "^ngModel";
            directive.link = (($scope: ng.IScope, $element: any, $attrs: any, ngModel: ng.INgModelController) =>
            {
                $element.bind("keydown keypress", function (event)
                {
                    if (event.which === 32) // space
                    {
                        event.preventDefault();
                    }
                });

                $scope.$watch($attrs.ngModel, (newValue, oldValue) =>
                {
                    //defualt value
                    if (newValue == undefined) return '';
                    if (newValue == null) return '';

                    newValue = String(newValue);

                    var transformedInput;

                    transformedInput = newValue.replace(/[^0-9]/g, '');

                    if ($attrs.customMaxlength != undefined)
                    {
                        transformedInput = transformedInput.substring(0, parseInt($attrs.customMaxlength));
                    }

                    if (transformedInput != newValue)
                    {
                        ngModel.$setViewValue(transformedInput);
                        ngModel.$render();
                    }
                });
            });

            return directive;
        }
    }
    Main.App.Directives.directive("accountnumberTextbox", ["$filter", ($filter) => new AccountNumberDirective($filter)]);
}
