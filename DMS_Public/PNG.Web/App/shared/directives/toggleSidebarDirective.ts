﻿module Shared
{
    export class toggleSidebarDirective
    {
        constructor($document: any)
        {
            var directive: ng.IDirective = {};
            directive.restrict = "A";
            directive.link = (($scope: ng.IScope, $element: any, $attrs: any) =>
            {
                $element.bind("click", function (e)
                {
                    e.preventDefault();
                    var hideClass: string = 'hide-menu';
                    var ele: any = $document.find('#containerBox');
                    if (ele[0].className.indexOf(hideClass) == -1)
                    {
                        $document.find('#containerBox').addClass(hideClass);
                    }
                    else
                    {
                        $document.find('#containerBox').removeClass(hideClass);
                    }
                });
            });

            return directive;
        }
    }
    Main.App.Directives.directive("toggleSidebar", ["$document", ($document) => new toggleSidebarDirective($document)]);
}