var Shared;
(function (Shared) {
    var AreaDisableDirective = (function () {
        function AreaDisableDirective($timeout) {
            var directive = {};
            directive.restrict = "EA";
            directive.scope = {
                isDisabled: '=ngDisabled'
            };
            directive.link = function ($scope, $element, $attrs) {
                $scope.$watch('isDisabled', function (disabled) {
                    if (disabled == "true" || disabled == true) {
                        $timeout(function () {
                            $element.find('input, select, textarea').prop('disabled', disabled);
                        }, 200);
                    }
                    else {
                        $element.find('input, select, textarea').removeAttr('disabled');
                    }
                });
            };
            return directive;
        }
        return AreaDisableDirective;
    }());
    Shared.AreaDisableDirective = AreaDisableDirective;
    Main.App.Directives.directive("areaDisable", ["$timeout", function ($timeout) { return new AreaDisableDirective($timeout); }]);
})(Shared || (Shared = {}));
//# sourceMappingURL=areaDisableDirective.js.map