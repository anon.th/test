var Shared;
(function (Shared) {
    var EmailAccountDirective = (function () {
        function EmailAccountDirective($filter) {
            var directive = {};
            directive.restrict = "A";
            directive.require = "^ngModel";
            directive.link = (function ($scope, $element, $attrs, ngModel) {
                $element.bind("keydown keypress", function (event) {
                    if (event.which === 32) {
                        event.preventDefault();
                    }
                });
                // ตัด space ทิ้ง ตอน paste
                $element.bind("paste", function (event) {
                    var _this = this;
                    // Short pause to wait for paste to complete
                    setTimeout(function () {
                        var pastedData = $(_this).val();
                        pastedData = String(pastedData).replace(' ', '');
                        ngModel.$setViewValue(pastedData);
                        ngModel.$render();
                    }, 100);
                });
                $scope.$watch($attrs.ngModel, function (newValue, oldValue) {
                    //defualt value
                    if (newValue == undefined)
                        return '';
                    if (newValue == null)
                        return '';
                    newValue = String(newValue);
                    var transformedInput;
                    transformedInput = newValue.replace(' ', '');
                    if (transformedInput != newValue) {
                        ngModel.$setViewValue(transformedInput);
                        ngModel.$render();
                    }
                });
            });
            return directive;
        }
        return EmailAccountDirective;
    }());
    Shared.EmailAccountDirective = EmailAccountDirective;
    Main.App.Directives.directive("emailaccountTextbox", ["$filter", function ($filter) { return new EmailAccountDirective($filter); }]);
})(Shared || (Shared = {}));
//# sourceMappingURL=emailAccountDirective.js.map