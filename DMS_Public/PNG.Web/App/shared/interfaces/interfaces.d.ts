﻿/// <reference path="../../../Scripts/typings/angularjs/angular-cookies.d.ts" />
declare module Interfaces {
    //interfaces which are not derived from the model
    export interface ICookies extends ng.cookies.ICookiesService {
        AuthToken: string;
        IsAuthenticated: boolean;
        SelectedLanguageCode: string;
    }

    export interface IGeneral {
        Id?: number;
        Code?: string;
        TypeCode?: string;
        Description?: string;
        DescriptionEng?: string;
        OrderNo?: number;
        IsDefault?: boolean;
        IsActive?: boolean;
        CreatedBy?: string;
        CreatedDate?: Date;
        ModifiedBy?: string;
        ModifiedDate?: Date;
    }

    export interface ITimings {
        method: string;
        uri: string;
        time: number;
        status?: number;
        error?: any;
    }

    export interface IConfigSettings {
        apiServiceBaseUri: string;
        clientId: string;
        uploadFileSize: number; //byte unit
        backOfficeBaseApi: string;
    }

    export interface ILoginData {
        userName: string;
        password: string;
        useRefreshTokens: boolean;
        email: string;
        companyId: string;
        ExternalIdentityUserQuestionAnswerList?: IExternalIdentityUserQuestionAnswer[];
        IpAddress?: string;
        pageType?: string;
        userId?: string;
        enterpriseid: string;
    }

    export interface IUserLogin {
        Username?: string;
        Password?: string;
        IpAddress?: string;
        ClientId?: string;
        EnterpriseId?: string;
    }

    /// <summary>
    /// Interfaces for Search query
    /// </summary>
    export interface IMetadataResults<T> {
        TotalResults?: number;
        ReturnedResults?: number;
        Results?: T[];
        Timestamp?: Date;
        Status?: string;
    }

    export interface IPageLink {
        Id?: number;
        Label?: string;
        Link?: string;
        Current?: boolean;
        Disabled?: boolean;
        Skip?: number;
        Page?: number;
    }

    /// Interfaces for Dialog Parameter
    export interface IDialogParameter {
        BroadcastName?: string;
        SearchFilter?: any;
    }

    export interface ISearchFilterView {
        TableName?: string;
        FieldName?: string;
        ParameterName?: string;
        Value?: any;
        ListValues?: any[];
        OperatorType?: Enums.SearchOperatorType;
        ConditionType?: Enums.SearchConditionType;
        ListSearchFilterViews?: Interfaces.ISearchFilterView[];
    }

    export interface ISearchQueryParameter {
        FilterList?: Interfaces.ISearchFilterView[];
        SortColumn?: string;
        SortAscending?: boolean;
        Page?: number;
        ItemsPerPage?: number;
    }

    export interface ISearchResultView<T> {
        SearchResults?: T[];
        TotalRecord?: number;
        RecordCount?: number;
    }

    export interface ISearchVestedInterestFilterResult {
        ScalarResult?: any;
    }

    /// <summary>
    /// Interfaces for List of view
    /// </summary>
    export interface IListOfViewParameter {
        BroadcastName?: string;
        SearchFilter?: any;
        IsMultipleSelected?: boolean;
        IsHideSearchStatus?: boolean;
        Active?: number; //-1 = ทั้งหมด, 1 = ปกติ, 0 = ยกเลิก
        IsShowLock?: boolean; // Show key sign
        IncludeListData?: any;
        AllChoiceData?: any;
        ExceptData?: any;
    }

    export interface ILovParameter {
        BroadcastName?: string;
        LovSearchParameter?: any;
        LovCompany?: any;
        LovFund?: any;
        LovGeneral?: any;
        LovMember?: any;
        LovPolicy?: any;
        LovBank?: any;
        LovMenu?: any;
    }

    //Search for Member
    export interface SearchMemberFilterView {
        MemberId?: number;
        Code?: string;
        FirstName?: string;
        LastName?: string;
    }

    export interface MemberFilterResult {
        Result?: any[];
    }
    //

    export interface SearchTradeDate {
        ///LovTradeDate?: Interfaces.ILovTradeDate;
        LovTradeDate?: any;
    }

    export interface ILovTradeDate {
        Date?: Date;
        DateString?: string;
    }

    //Add for tradedate
    export interface ITradeDateResult {
        ResultDate?: Date;
        ResultSelect?: boolean;
    }

    export interface IExceptionMessage {
        Status?: string;
        Message?: string;
        FieldName?: string;
        Data?: string;
        TranslationMessage?: string[]
    }

    export interface IChangePasswordBindingModel {
        OldPassword?: string;
        NewPassword?: string;
        ConfirmPassword?: string;
    }

    export interface IResetPasswordViewModel {
        UserName?: string;
        Email?: string;
        Password?: string;
        ConfirmPassword?: string;
        IpAddress?: string;
        MinLowercase?: string;
        MinNumber?: string;
        MinPasswordLength?: string;
        MinSpecialCharacter?: string;
        MinUppercase?: string;
        OldPassword?: string;
    }

    export interface IForgotPasswordViewModel {
        UserName?: string;
        Email?: string;
        IpAddress?: string;
    }

    export interface IRequestResetPasswordViewModel {
        Guid?: string;
        IpAddress?: string;
    }

    export interface ILovExternalUser {
        Id?: number;
        UserName?: string;
        FirstName?: string;
        LastName?: string;
        Email?: string;
        IsActive?: boolean;
    }

    export interface IUserMenu {
        MenuId?: number;
        MenuName?: string;
        Url?: string;
        ParentId?: number;
        RelativeId?: number;
        ModuleNameId?: number;
        IsSubTitle?: boolean;
        OrderNo?: number;
        IsCanView?: boolean;
        IsCanSave?: boolean;
        IsCanApprove?: boolean;
        IsShowView?: boolean;
        IsShowSave?: boolean;
        IsShowApprove?: boolean;

        ParentSubId?: number;
        MenuIconClass?: string;
        UserMenuLv2?: IUserMenu[];
    }

    export interface IUserInfo {
        Id: string;
        UserName: string;
        FirstName: string;
        LastName: string;
        Email: string;
        RoleId?: string;
        RoleCode?: string;
        RoleName?: string;
        ListUserMenu?: Interfaces.IUserMenu[];
        UserId?: string;
        MemberId?: number;
        MemberCode?: string;
        CompanyId?: number;
        CompanyCode?: string;
        CompanyName?: string;
        Phone?: string;
        Mobile?: string;
        ExternalIdentityUserMemberId?: number;
        ExternalIdentityUserRolesId?: string;
        SuitabilityTestDate?: Date;
        ResultSuitability?: number;
    }

    export interface IUserCover {
        UserInfoHeader?: IUserInfo;
        UserInfoDetail?: IUserInfo;
        UserEncrypt?: string;
    }

    export interface IUserAccessRight {
        MenuID?: number;
        IsCanView?: boolean;
        IsCanSave?: boolean;
        IsCanApprove?: boolean;
        IsShowView?: boolean;
        IsShowSave?: boolean;
        IsShowApprove?: boolean;
    }

    export interface IIdentityRoles {
        IsSeleted?: boolean;
    }

    export interface IExternalIdentityRoles {
        IsSeleted?: boolean;
    }

    export interface IvwExternalMenu {
        IsSelectedIsCanView?: boolean;
        IsSelectedIsCanSave?: boolean;
        IsSelectedIsCanApprove?: boolean;
    }

    // TODO: create dictionary types as in C#
    export interface IDictionary {
        [Key: string]: Object;
    }

    export interface IExternalIdentityUserRoles {
        CompanyId?: string;
        ExternalIdentityRolesCode?: string;
        ExternalIdentityRolesId?: string;
        Id?: string;
        ExternalIdentityRolesName?: string;
    }

    export interface IUploadFileResult {
        Extension?: string;
        IsCompleted?: boolean;
        IsUploaded?: boolean;
        ErrorMessage?: string;
    }

    export interface ILogin {
        CountCompany?: number;
    }

    export interface IExternalUserRoleAndType {
        ExternalIdentityUserMemberId: number;
        MemberId?: number;
        CompanyId: number;
        CompanyCode: string;
        CompanyName: string;
        FirstName: string;
        LastName: string;
        TaxpayerIdentificationNumber: string;
        IsMember: boolean;
        IsCommittee: boolean;
        IsAdmin: boolean;
        IsRoleMember: boolean;
        IsRoleCommittee: boolean;
        IsRoleAdmin: boolean;
    }

    export interface IExternalUserRoleAndTypeForInsert {
        ExternalIdentityUserMembersId: number;
        IsMember: boolean;
        IsAdmin: boolean;
        CompanyId: number;
        ExternalIdentityUsersId?: string;
    }

    export interface IPeriodView {
        IndexRow?: number;
        FundId?: number;
        CompanyId?: number;
        PeriodText?: string;
        PeriodYearTh?: number;
        PeriodMonth?: number;
        PeriodNo?: any;
        DueDate?: Date;
    }

    export interface IExternalIdentityUserQuestionAnswer {
        QuestionBody?: string;
        ReplyOfUser?: string;
        IsExact?: boolean;
        FailMessage?: string;
        FailMessage2?: string;
        UserName?: string;
        IsPresent?: boolean;
        UserId?: string;
    }

    export interface IInfomationUserType {
        CompanyId: number;
        CompanyCode: string;
        CommpanyName: string, FirstName: string;
        Lastname: string;
        TaxpayerIdentificationNumbernvarchar: string;
        IsMember: boolean;
        IsCompanyBoard: boolean;
        IsCommittee?: boolean;
        IsAdmin: boolean;
        ExternalIdentityRolesId?: string;
    }
    export interface IResultLogin {
        TypeLogin?: string;
        ValueLogin?: string;
    }

    export interface FileResult {
        Base64StringData?: string;
        ContentType?: string;
        FileName?: string;
    }

    export interface GetFundNametoFrontEnd {
        Id?: number;
        Name?: string;
    }

    export interface IGeneralListFrontEnd {
        Id?: number;
        Name?: string;
    }

    export interface IExternalRoleTransactionViewModel {
        UserName?: string;
        CompanyId?: number;
        Type?: number;
        RoleTypeId?: number;
    }


    export interface ITrackAndTraceHeader {
        enterpriseid?: string;
        booking_no?: string;
        consignment_no?: string;
        ref_no?: string;
        booking_datetime?: Date;
        est_delivery_datetime?: Date;
        service_code: string;
        origin_state_code: string;
        destination_state_code: string;
        payerid: string;
        payer_name: string;
        status_description?: string;
    }

    export interface ITrackAndTraceDetail {
        consignment_no?: string;
        ref_no?: string;
        tracking_datetime?: string;
        status_code?: string;
        status_description?: string;
        exception_code?: string;
        exception_description?: string;
        location?: string;
        remarks?: string;
        consignee_name?: string;
        deleted?: string;
        delete_remark?: string;
        person_incharge?: string;
        last_updated?: string;
        last_userid?: string;
        tracking_datetime2?: Date;
        scc: string;
        date: string;
        time: string;
        isShow: boolean;
    }

    export interface ITrackAndTraceResult {
        TrackAndTraceHeader: Interfaces.ITrackAndTraceHeader;
        TrackAndTraceDetail: Interfaces.ITrackAndTraceDetail[];

    }

    export interface IUserMaster {
        user_name?: string;
        user_password?: string;
        user_culture?: string;
        email?: string;
        department_name?: string;
        user_type?: string;
        payerid?: string;
        location?: string;
    }

    export interface IMenu {
        applicationid?: string;
        parent_id?: string;
        child_id?: string;
        child_name?: string;
        child_icon_image?: string;
        display_style?: number;
        child_url?: string;
        child_url2?: string;
        Url_Public?: string;
        new_child_name?: string;
    }

    export interface ISearchConsignment {
        consignment_no?: string;
    }

    export interface ICreateConsignmentResult {
        CreateConsignmentError: Interfaces.ICreateConsignmentError[];
        CreateConsignmentSenderDetail: Interfaces.ICreateConsignmentSenderDetail[];
        CreateConsignmentPackageDetail: Interfaces.ICreateConsignmentPackageDetail[];

    }
    export interface ICreateConsignmentError {
        ErrorCode?: number;
        RequestedAction?: number;
        ErrorMessage?: string;
        EnableDeleteButton?: number;
    }
    export interface ICreateConsignmentSenderDetail {
        applicationid?: string;
        enterpriseid?: string;
        consignment_no?: string;
        payerid?: string;
        ShippingList_no?: string;
        service_code?: string;
        ref_no?: string;
        last_status?: string;
        last_status_DT?: Date;
        sender_name?: string;
        sender_address1?: string;
        sender_address2?: string;
        sender_zipcode?: string;
        sender_state_name?: string;
        sender_telephone?: string;
        sender_fax?: string;
        sender_contact_person?: string;
        sender_email?: string;
        recipient_telephone?: string;
        recipient_name?: string;
        recipient_address1?: string;
        recipient_address2?: string;
        recipient_zipcode?: string;
        recipient_state_name?: string;
        recipient_fax?: string;
        recipient_contact_person?: string;
        declare_value?: string;
        cod_amount?: string;
        remark?: string;
        return_pod_slip?: string;
        return_invoice_hc?: string;
        DangerousGoods?: number;
        Created_By?: string;
        Created_DT?: Date;
        Updated_By?: string;
        Updated_DT?: Date;
        ContractAccepted?: number;
        GoodsDescription?: string;
    }
    export interface ICreateConsignmentPackageDetail {
        applicationid?: string;
        enterpriseid?: string;
        consignment_no?: number;
        payerid?: number;
        mps_code?: number;
        pkg_qty?: number;
        pkg_wt?: number;
        tot_wt?: number;
        pkg_length?: number;
        pkg_breadth?: number;
        pkg_height?: number;
        IsEdit?: boolean;
        IsSelected?: boolean;
        TempPackage?: Interfaces.ICreateConsignmentPackageDetail;
        IsAdd?: boolean;
        IsActive?: boolean;
    }
    export interface ICreateConsignmentParam {
        action?: number;
        ForceDelete?: number;
        enterpriseid?: string;
        userloggedin?: string;
        OverrideSenderName?: string;
        consignment_no?: string;
        payerid?: string;
        service_code?: string;
        ref_no?: string;
        sender_name?: string;
        sender_address1?: string;
        sender_address2?: string;
        sender_zipcode?: string;
        sender_telephone?: string;
        sender_fax?: string;
        sender_contact_person?: string;
        sender_email?: string;
        recipient_telephone?: string;
        recipient_name?: string;
        recipient_address1?: string;
        recipient_address2?: string;
        recipient_zipcode?: string;
        recipient_fax?: string;
        recipient_contact_person?: string;
        declare_value?: number;
        cod_amount?: number;
        remark?: string;
        return_pod_slip?: string;
        return_invoice_hc?: string;
        DangerousGoods?: number;
        ContractAccepted?: number;
        PackageDetails?: string;
        GoodsDescription?: string;
        cost_centre?: string;
    }

    export interface ICreateConsignmentBindSenderDetail {
        snd_rec_name?: string;
        snd_rec_type?: string;
        contact_person?: string;
        email?: string;
        address1?: string;
        address2?: string;
        country?: string;
        zipcode?: string;
        telephone?: string;
        fax?: string;
        state_name?: string;
    }

    export interface ICreateConsignmentBindRecipientDetail {

        telephone?: string;
        reference_name?: string;
        address1?: string;
        address2?: string;
        zipcode?: string;
        fax?: string;
        contactperson?: string;
        cost_centre?: string;
        ref_no?: string;
        remark?: string;
        GoodsDescription?: string;
        state_name?: string;
        lovZipCode?: Interfaces.IZipCode;
    }

    export interface ICreateConsignmentCustomerAccount {
        currency?: string;
        currency_decimal?: number;
        cust_name?: string;
        DefaultSender?: string;
        IsCustomerUser?: boolean;
        IsCustomsUser?: boolean;
        IsEnterpriseUser?: boolean;
        IsMasterCustomerUser?: boolean;
        payerid?: string;
        shippinglist?: string;
    }

    export interface ICreateConsignmentCustomerAccount2 {
        custid?: string;
        cust_name?: string;
    }

    export interface ICreateConsignmentCustomsWarehousesNameDefault {
        snd_rec_name?: string;
    }
    export interface ICreateConsignmentCustomsWarehousesNameDefaultResult {
        QuerySndRecName: Interfaces.ICreateConsignmentCustomsWarehousesNameDefault[];
    }

    export interface ICreateConsignmentServiceCode {
        service_code?: string;
    }
    //export interface ICreateConsignmentServiceCodeResult
    //{
    //    ServiceCodeResult: Interfaces.ICreateConsignmentServiceCode[];
    //}

    export interface ICreateConsignmentCustomsWarehousesName {
        applicationid?: string;
        enterpriseid?: string;
        warehouse_name?: string;
        contact_person?: string;
        email?: string;
        address1?: string;
        address2?: string;
        country?: string;
        state_code?: string;
        zipcode?: string;
        telephone?: string;
        fax?: string;
    }
    export interface ICreateConsignmentGetStateName {
        zipcode?: string;
        state_name?: string;
        Country?: string;
    }
    export interface ICreateConsignmentGetPackageLimit {
        key?: string;
        value?: string;
    }
    export interface ICreateConsignmentGetConfiguration {
        key?: string;
        value?: string;
    }
    export interface ICreateConsignmentGetEnterpriseContract {
        AcceptedCurrentContract?: string;
        ContractText?: string;
    }
    export interface ICreateConsignmentDefaultServiceCode {
        Default_Service_Code?: string;
    }

    export interface ILovZipCode {
        Id?: number;
        zipcode?: string;
        Name?: string;
        FundId?: number;
        FundCode?: string;
        ListFundId?: number[];
        ListCompanyIdNotIn?: number[];
        ListSelected?: Interfaces.IZipCode[];
        IsActive?: boolean;
        IsHideStatus?: boolean;
        IsActiveCompanyFund?: boolean;
        IsTransferFund?: boolean;
        Country?: string;
        state_code?: string;
    }

    export interface IZipCode {
        zipcode?: string;
        state_code?: string;
        country?: string;
        esa_surcharge?: string;
        state_name?: string;
    }

    export interface IUpdateConsignmentParam {
        userloggedin?: string;
        recipient_telephone?: string;
        recipient_reference_name?: string;
        recipient_address1?: string;
        recipient_address2?: string;
        recipient_zipcode?: string;
        recipient_fax?: string;
        recipient_contactperson?: string;
        recipient_cost_centre?: string;
    }
    export interface IUpdateConsignment {

        msgStatus?: string;
        msgMessage?: string;
    }

    export interface ICreateConsignmentStatus {
        Created_By?: string;
        Created_DT?: Date;
        Updated_By?: string;
        Updated_DT?: Date;
        last_status?: string;
        last_status_DT?: Date;
    }

    export interface ICostCentre
    {
        applicationid?: string;
        enterpriseid?: string;
        telephone?: string;
        reference_name?: string;
        address1?: string;
        address2?: string;
        zipcode?: string;
        fax?: string;
        contactperson?: string;
        cost_centre?: string;
    }


    export interface ILovCostCentre
    {
        Id?: number;       
        ListSelected?: Interfaces.ICostCentre[];
        IsActive?: boolean;
        IsHideStatus?: boolean;
        cost_centre?: string;
        telephone?: string;
        reference_name?: string;
    }

    export interface ISearchQueryParameterCustom {
        FilterList?: Interfaces.ISearchFilterView[];
        SortColumn?: string;
        SortAscending?: boolean;
        Page?: number;
        ItemsPerPage?: number;
        status_code?: string;
        status_description?: string;
        Status_type?: string;
        status_close?: string;
        invoiceable?: string;
        culture?: string;
        userid?: string;
        allowRoles?: string;
    }

    export interface IExceptionCode {
        status_code?: string;
        exception_code?: string;
        exception_description?: string;
        mbg?: string;
        status_close?: string;
        invoiceable?: string;
        status_description?: string;
    }

    export interface ILovExceptionCode {
        Id?: number;
        ListSelected?: Interfaces.IExceptionCode[];
        IsActive?: boolean;
        IsHideStatus?: boolean;
        status_code?: string;
        exception_code?: string;
        exception_description?: string;
        mbg?: string;
        status_close?: string;
        invoiceable?: string;
        status_description?: string;
        culture?: string;
        userid?: string;
        allowRoles?: string;
    }

    export interface IStatusCode {
        status_code?: string;
        status_description?: string;
        Status_type?: string;
        status_close?: string;
        invoiceable?: string;
    }
    export interface ILovStatusCode {
        Id?: number;
        ListSelected?: Interfaces.IStatusCode[];
        IsActive?: boolean;
        IsHideStatus?: boolean;
        applicationid?: string;
        enterpriseid?: string;
        status_code?: string;
        status_description?: string;
        Status_type?: string;
        status_close?: string;
        invoiceable?: string;
        culture?: string;
        userid?: string;
        allowRoles?: string;
    }

    export interface IStateCode {
        country?: string;
        state_code?: string;
        state_name?: string;
    }
    export interface ILovStateCode {
        Id?: number;
        ListSelected?: Interfaces.IStateCode[];
        IsActive?: boolean;
        IsHideStatus?: boolean;
        applicationid?: string;
        enterpriseid?: string;
        country?: string;
        state_code?: string;
        state_name?: string;
    }
    export interface IPayerCode {
        applicationid?: string;
        enterpriseid?: string;
        custid?: string;
        ref_code?: string;
        cust_name?: string;
        contact_person?: string;
        email?: string;
        address1?: string;
        address2?: string;
        country?: string;
        state_code?: string;
        zipcode?: string;
        telephone?: string;
        mbg?: string;
        fax?: string;
        credit_term?: number;
        industrial_sector_code?: string;
        credit_limit?: number;
        active_quotation_no?: string;
        status_active?: string;
        credit_outstanding?: number;
        remark?: string;
        salesmanid?: string;
        prom_tot_wt?: number;
        payment_mode?: string;
        prom_tot_package?: number;
        prom_period?: number;
        free_insurance_amt?: number;
        insurance_percent_surcharge?: number;
        created_by?: string;
        apply_dim_wt?: string;
        pod_slip_required?: string;
        apply_esa_surcharge?: string;
        invoice_return_days?: number;
        payer_type?: string;
        cod_surcharge_amt?: number;
        cod_surcharge_percent?: number;
        insurance_maximum_amt?: number;        
        density_factor?: number;
        hc_invoice_required?: string;
        apply_esa_surcharge_rep?: string;
        max_cod_surcharge?: number;
        min_cod_surcharge?: number;
        max_insurance_surcharge?: number;
        other_surcharge_amount?: number;
        other_surcharge_percentage?: number;
        other_surcharge_min?: number;
        other_surcharge_max?: number;
        other_surcharge_desc?: string;
        Discount_band?: string;
        CustomerBox?: string;
        master_account?: string;
        remark2?: string;
        next_bill_placement_date?: Date;
        Minimum_Box?: number;
        dim_by_tot?: string;
        creditstatus?: string;
        creditthreshold?: number;
        credit_used?: number;
        CreditStatus_UpdatedDT?: Date;
        Customs_TaxID?: string;
        Customs_TaxExempt?: number;
        Customs_AgencyFee?: number;
        Customs_CounterClearance?: number;
        Customs_DAWBFee?: number;
        Customs_Cartage1?: number;
        Customs_Cartage2?: number;
        Customs_EntryFee_Informal?: number;
        Customs_EntryFee_Formal1?: number;
        Customs_EntryFee_Formal2?: number;
        Customs_CurrencyAdjFactor?: number;
        Customs_ExpressFCDiscount?: number;
        RelatedAccounts?: string;
        Credit_Term_AccPac?: string;
        Starting_Consignment_No?: string;
        Default_Service_Code?: string;

        
    }
    export interface ILovPayerCode {
        Id?: number;
        ListSelected?: Interfaces.IPayerCode[];
        IsActive?: boolean;
        IsHideStatus?: boolean;
        applicationid?: string;
        enterpriseid?: string;
        custid?: string;
        cust_name?: string;
        zipcode?: string;
        status_active?: string;
        payer_type?: string[];
    }
    export interface ITrackAndTraceParam {
        consignmentNumber?: string;
        customerRef?: string;
        userId?: string;
        returnPackage?: boolean;
    }
    export interface ISearchConsignmentGetstatusprintingconfig {
        key?: string;
        value?: string;
    }

    export interface ISearchConsignmentGetCustomStatus {
        DisplayStatus?: string;
        StatusID?: number;        
    }

    export interface ISearchConsignmentParam {
        enterpriseid?: string;
        userloggedin?: string;
        SenderName?: string;
        consignment_no?: string;
        status_id?: number;
        ShippingList_No?: string;
        recipient_telephone?: string;
        ref_no?: string;
        recipient_zipcode?: string;
        service_code?: string;
        datefrom?: Date;
        dateto?: Date;
        payerid?: string;
        MasterAWBNumber?: string;
        JobEntryNo?: string;
        pageNumber?: number;
        pageCount?: number;
    }
    export interface ISearchConsignmentError {
        ErrorCode?: number;
        ErrorMessage?: string;
    }
    export interface ISearchConsignmentDetail {
        applicationid?: string;
        enterpriseid?: string;
        consignment_no?: string;
        sender_name?: string;
        last_status?: string;
        ShippingList_no?: string;
        recipient_telephone?: string;
        recipient_name?: string;
        recipient_zipcode?: string;
        service_code?: string;
        pkg_qty?: number;
        ref_no?: string;
        ProcessStep?: number;
        booking_no?: number;
        JobEntryNo?: number;
        MAWBNumber?: string;
        SeqNo?: number;
        IsEdit?: boolean;
        IsSelected?: boolean;
    }  

    export interface ISearchConsignmentResult {
        SearchConsignmentError: Interfaces.ICreateConsignmentError[];
        SearchConsignmentDetail: Interfaces.ICreateConsignmentSenderDetail[];
        CountData?: number;

    }

    export interface ISearchConsignmentPrintShippingListParam {
        userloggedin?: string;
        consignmentsList?: string;
    }

    export interface ISearchConsignmentReprintConsNoteParam {
        userloggedin?: string;
        consignmentsList?: string;
        dataSource?: number;
        bookingNo?: number;        
    }
    export interface ISearchConsignmentReprintConsNoteError{
        ErrorCode?: number;    
        ErrorMessage?: string;
        ReportTemplate?: string;  
    }

    export interface ISearchConsignmentReprintConsNoteDetail {
        consignment_no?: string;
        payerid?: string;
        service_code?: string;
        ref_no?: string;
        sender_name?: string;
        sender_address1?: string;
        sender_address2?: string;
        sender_zipcode?: string;
        sender_state?: string;
        sender_telephone?: string;
        sender_fax?: string;
        sender_contact_person?: string;
        sender_email?: string;
        recipient_telephone?: string;
        recipient_name?: string;
        recipient_address1?: string;
        recipient_address2?: string;
        recipient_zipcode?: string;
        recipient_state?: string;
        recipient_fax?: string;
        recipient_contact_person?: string;
        declare_value?: number;
        cod_amount?: number;
        remark?: string;
        return_pod_slip?: string;
        return_invoice_hc?: string;
        DangerousGoods?: number;
        MoreInfo?: string;
        MorePkgs?: string;
        Qty1?: number;
        Wgt1?: number;
        Qty2?: number;
        Wgt2?: number;
        Qty3?: number;
        Wgt3?: number;
        Qty4?: number;
        Wgt4?: number;
        Qty5?: number;
        Wgt5?: number;
        Qty6?: number;
        Wgt6?: number;
        Qty7?: number;
        Wgt7?: number;
        Qty8?: number;
        Wgt8?: number;
        Qty?: number;        
        Wgt?: number;
        CopyName?: string
        sender_country?: string;
        recipient_country?: string;
        GoodsDescription?: string;
        total_rated_amount?: number;
        tax_on_rated_amount?: number;
        total_amount?: number;
        imgBarcode?: Blob[];
        Rtg1?: number;
        Rtg1_toea?: number;
        Rtg2?: number;
        Rtg2_toea?: number;
        Rtg3?: number;
        Rtg3_toea?: number;
        other_surch_amount?: number;
        other_surch_amount_toea?: number;
        basic_charge?: number;
        basic_charge_toea?: number;
        ConsignmentDescription?: string;
    } 

    export interface ISearchConsignmentReprintConsNoteResult {
        SearchConsignmentReprintConsNoteError: Interfaces.ISearchConsignmentReprintConsNoteError[];
        SearchConsignmentReprintConsNoteDetail: Interfaces.ISearchConsignmentReprintConsNoteDetail[];
    }

   
    export interface ISearchConsignmentPrintShippingListDetail {
        enterprise_name?: string;
        enterprise_address?: string;
        enterprise_telephone?: string;
        custid?: string;
        customer_address?: string;
        customer_zipcode?: string;
        customer_telephone?: string;
        sender_name?: string;
        sender_address?: string;
        sender_zipcode?: string;
        sender_telephone?: string;
        consignment_no?: string;
        total_packages?: number;
        total_wt?: number;
        ref_no?: string;
        service_code?: string;
        recipient_telephone?: string;
        recipient_name?: string;
        recipient_address?: string;
        recipient_zipcode?: string;
        state_name?: string;
        ShippingList_No?: string;
        cust_name?: string;
        Printed_Date_Time?: Date;
        GoodsDescription?: string;
        cost_centre?: string;
        imgBarcode?: Blob[];

    }

    //export interface ISearchConsignmentReprintShippingListResult {
    //    SearchConsignmentReprintShippingListError?: Interfaces.ISearchConsignmentPrintShippingListError;
    //    SearchConsignmentReprintShippingListDetail?: Interfaces.ISearchConsignmentPrintShippingListDetail[];
    //}

    //export interface ISearchConsignmentPrintShippingListError {
    //    ErrorCode?: number;
    //    ErrorMessage?: string;
    //}
    export interface ISearchConsignmentReprintShippingListResult {
        SearchConsignmentPrintShippingListError?: Interfaces.IPrintShippingListError;
        SearchConsignmentPrintShippingListDetail?: Interfaces.ISearchConsignmentPrintShippingListDetail[];

    }

    export interface IPrintShippingListError {
        ErrorCode?: number;
        ErrorMessage: string;
    }

    export interface ISearchConsignmentPrintConsNoteError {
        ErrorCode?: number;
        ErrorMessage?: string;
        ReportTemplate?: string;
    }

    export interface ISearchConsignmentPrintConsNoteDetail {
        consignment_no?: string;
        payerid?: string;
        service_code?: string;
        ref_no?: string;
        sender_name?: string;
        sender_address1?: string;
        sender_address2?: string;
        sender_zipcode?: string;
        sender_state?: string;
        sender_telephone?: string;
        sender_fax?: string;
        sender_contact_person?: string;
        sender_email?: string;
        recipient_telephone?: string;
        recipient_name?: string;
        recipient_address1?: string;
        recipient_address2?: string;
        recipient_zipcode?: string;
        recipient_state?: string;
        recipient_fax?: string;
        recipient_contact_person?: string;
        declare_value?: number;
        cod_amount?: number;
        remark?: string;
        return_pod_slip?: string;
        return_invoice_hc?: string;
        DangerousGoods?: number;
        MoreInfo?: string;
        MorePkgs?: string;
        Qty1?: number;
        Wgt1?: number;
        Qty2?: number;
        Wgt2?: number;
        Qty3?: number;
        Wgt3?: number;
        Qty4?: number;
        Wgt4?: number;
        Qty5?: number;
        Wgt5?: number;
        Qty6?: number;
        Wgt6?: number;
        Qty7?: number;
        Wgt7?: number;
        Qty8?: number;
        Wgt8?: number;
        Qty?: number;
        Wgt?: number;
        CopyName?: string
        sender_country?: string;
        recipient_country?: string;
        GoodsDescription?: string;
        imgBarcode?: Blob[];
    }

    export interface ISearchConsignmentPrintConsNoteResult {
        SearchConsignmentPrintConsNoteError: Interfaces.ISearchConsignmentPrintConsNoteError[];
        SearchConsignmentPrintConsNoteDetail: Interfaces.ISearchConsignmentPrintConsNoteDetail[];
    }

    export interface ISearchShipmentTrackingReport {
        PeriodFrom?: Date;
        PeriodTo?: Date;
        delPathOriginDC?: string;
        delPathDestinationDC?: string;
        month?: string;
        year?: string;
        tran_date?: string;
        payer_code?: string;
        payer_type?: string[];
        route_type?: string;
        route_code?: string;
        origin_dc?: string;
        destination_dc?: string;
        zip_code?: string;
        state_code?: string;
        booking_type?: string;
        consignment_no?: string;
        booking_no?: string;
        status_code?: string;
        exception_code?: string;
        shipment_closed?: string;
        shipment_invoiced?: string; 
        track_all?: string;
        RptType?: string;
        userId?: string;
        PeriodFromFormat?: Date;
        PeriodToFormat?: Date;
        reportName?: string;

    }
    export interface ISearchShipmentTrackingPayerType {        
        payer_type?: string;
    }

    export interface ISearchShipmentTrackingGetCodeValue {
        sequence?: number;
        code_text?: string;
        code_str_value?: string;
        code_num_value?: string;
    }

    export interface ISearchShipmentTrackingParam {
        userid?: string;
        user_culture?: string;
        codeid?: string;
    }

    export interface IserachShipmentTrackingUserDetail {
        userid?: string;
        user_name?: string;
        user_culture?: string;
        email?: string;
        department_name?: string;
        User_Type?: string;
        payerid?: string;
        Location?: string;
    }

    export interface ISearchShipmentTrackingAllRoles {
        roleid?: number;
        role_name?: string;
    }

    export interface ISearchShipmentTrackingPathCodeQuery {
        DbComboText?: string;
        DbComboValue?: string;
    }

    export interface IserachShipmentTrackingDeliveryPath {
        path_code?: string;
        applicationid?: string;
        enterpriseid?: string;
        path_description?: string;
        delivery_type?: string;
        line_haul_cost?: number;
        origin_station?: string;
        destination_station?: string;
        flight_vehicle_no?: string;
        awb_driver_name?: string;
        departure_time?: Date;
    }
    export interface IserachShipmentTrackingDeliveryPathParam {
        path_code?: string;
    }

    export interface ISearchConsignmentPrintConsNoteParam {
        enterpriseId?: string;
        userloggedIn?: string;
        consignmentList?: string;
         
    }

    export interface IPrintConsNote{
        paramDetail?: Interfaces.ISearchConsignmentPrintConsNoteDetail[];
        paramFilter?: Interfaces.ISearchConsignmentPrintConsNoteParam;
    }
    export interface IReprintConsNote {
        paramDetail?: Interfaces.ISearchConsignmentReprintConsNoteDetail[];
    }
   
    


}