var Util;
(function (Util) {
    function MonthTHFilter() {
        return function (monthNumber) {
            if (monthNumber == null) {
                return "";
            }
            var monthNames = ["มกราคม", "กุมภาพันธ์", "มีนาคม", "เมษายน", "พฤษภาคม", "มิถุนายน", "กรกฎาคม", "สิงหาคม", "กันยายน", "ตุลาคม", "พฤศจิกายน", "ธันวาคม"];
            return monthNames[monthNumber - 1];
        };
    }
    Util.MonthTHFilter = MonthTHFilter;
    Main.App.Filters.filter("monthTh", MonthTHFilter);
})(Util || (Util = {}));
//# sourceMappingURL=monthThFilter.js.map