var Util;
(function (Util) {
    function DecimalFilter($filter) {
        return function (input, fractionSize) {
            var result;
            if (isNaN(input))
                return input;
            //convert exponential number 
            var data = String(input).split(/[eE]/);
            if (data.length > 1)
                input = input.toFixed(fractionSize);
            var numerics = String(input).split(".");
            numerics[0] = numerics[0].length == 0 ? "0" : numerics[0];
            var decimalValue = (numerics[1] == undefined) ? '' : numerics[1];
            if (numerics[1] == undefined || numerics[1].length < fractionSize) {
                var decimalValueLength = 0;
                if (numerics[1]) {
                    decimalValue = numerics[1].slice(0, fractionSize);
                    decimalValueLength = decimalValue.length;
                }
                //เติม 0 ให้ครบตามจำนวน decimalPoint ที่กำหนด
                for (var i = 0; i < fractionSize - decimalValueLength; i++) {
                    decimalValue += "0";
                }
            }
            if (input) {
                numerics[0] = $filter("number")(numerics[0], 0);
                result = [numerics[0], decimalValue].join(".");
            }
            return result;
        };
    }
    Util.DecimalFilter = DecimalFilter;
    Main.App.Filters.filter("decimal", ["$filter", function ($filter) { return DecimalFilter($filter); }]);
})(Util || (Util = {}));
//# sourceMappingURL=decimalFilter.js.map