var Util;
(function (Util) {
    function AdDateTimeFilter() {
        return function (input) {
            if (input == null) {
                return "";
            }
            var d = new Date(input.toString());
            var date = d.getDate().toString();
            var month = (d.getMonth() + 1).toString();
            var year = d.getUTCFullYear();
            if (year >= 2500) {
                year -= 543;
            }
            var hh = d.getHours().toString();
            var mm = d.getMinutes().toString();
            var ss = d.getSeconds().toString();
            return "{0}/{1}/{2} {3}:{4}:{5}".format(date.padLeft(date, 2, "0"), month.padLeft(month, 2, "0"), year, hh.padLeft(hh, 2, "0"), mm.padLeft(mm, 2, "0"), ss.padLeft(ss, 2, "0"));
        };
    }
    Util.AdDateTimeFilter = AdDateTimeFilter;
    Main.App.Filters.filter("adDateTime", AdDateTimeFilter);
})(Util || (Util = {}));
//# sourceMappingURL=adDateTimeFilter.js.map