﻿module Util
{
    export function NormalizeDateFilter()
    {
        return (input: Date) =>
        {
            if (input == null) { return ""; }
            var dateInput = new Date(input.toString());

            var month_names: string[] = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
            var date: string = dateInput.getUTCDate().toString();
            var month: string = month_names[dateInput.getMonth()];
            var year: number = dateInput.getFullYear();

            var result = "{0} {1}, {2}".format(month, date, year);
            return result;
        };
    }

    Main.App.Filters.filter("normalizeDate", NormalizeDateFilter);
}

