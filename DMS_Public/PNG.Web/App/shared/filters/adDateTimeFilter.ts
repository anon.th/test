﻿module Util
{
    export function AdDateTimeFilter()
    {
        return (input: Date) =>
        {
            if (input == null) { return ""; }
            var d = new Date(input.toString());
            var date: string = d.getDate().toString();
            var month: string = (d.getMonth() + 1).toString();
            var year: number = d.getUTCFullYear();
            if (year >= 2500)
            {
                year -= 543;
            }
            var hh: string = d.getHours().toString();
            var mm: string = d.getMinutes().toString();
            var ss: string = d.getSeconds().toString();

            return "{0}/{1}/{2} {3}:{4}:{5}".format(date.padLeft(date, 2, "0"), month.padLeft(month, 2, "0"), year, hh.padLeft(hh, 2, "0"), mm.padLeft(mm, 2, "0"), ss.padLeft(ss, 2, "0"));
        };

    }

    Main.App.Filters.filter("adDateTime", AdDateTimeFilter);
}

