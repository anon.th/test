﻿module Util
{
    export function MonthTHFilter()
    {
        return function (monthNumber: number)
        {
            if (monthNumber == null) { return ""; }

            var monthNames = ["มกราคม", "กุมภาพันธ์", "มีนาคม", "เมษายน", "พฤษภาคม", "มิถุนายน", "กรกฎาคม", "สิงหาคม", "กันยายน", "ตุลาคม", "พฤศจิกายน", "ธันวาคม"];
            return monthNames[monthNumber - 1];
        }
    }

    Main.App.Filters.filter("monthTh", MonthTHFilter);
}