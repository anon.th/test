﻿module Util
{
    export function BuddhaDateFilter($filter)
    {
        return (input: Date) =>
        {
            if (input == null) { return ""; }
            var dateInput = new Date(input.toString());

            // Edit get date beacuse of if input date has a time it's will calulate time to timezone and +7 to date
            var day = $filter("date")(input, "dd");
            var month: string = (dateInput.getMonth() + 1).toString();
            var year: number = dateInput.getFullYear();

            if (year <= 2500)
            {
                year += 543;
            }
            var result = "{0}/{1}/{2}".format(day, month.padLeft(month, 2, "0"), year);

            return result;
        };
    }

    Main.App.Filters.filter("buddhaDate", ($filter) => BuddhaDateFilter($filter));
}

