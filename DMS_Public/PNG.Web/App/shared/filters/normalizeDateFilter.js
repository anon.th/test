var Util;
(function (Util) {
    function NormalizeDateFilter() {
        return function (input) {
            if (input == null) {
                return "";
            }
            var dateInput = new Date(input.toString());
            var month_names = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
            var date = dateInput.getUTCDate().toString();
            var month = month_names[dateInput.getMonth()];
            var year = dateInput.getFullYear();
            var result = "{0} {1}, {2}".format(month, date, year);
            return result;
        };
    }
    Util.NormalizeDateFilter = NormalizeDateFilter;
    Main.App.Filters.filter("normalizeDate", NormalizeDateFilter);
})(Util || (Util = {}));
//# sourceMappingURL=normalizeDateFilter.js.map