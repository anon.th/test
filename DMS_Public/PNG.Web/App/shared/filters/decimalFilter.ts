﻿module Util
{
    export function DecimalFilter($filter: any)
    {
        return (input: any, fractionSize) =>
        {
            var result: string;
            if (isNaN(input)) return input;

            //convert exponential number 
            var data = String(input).split(/[eE]/);
            if (data.length > 1)
                input = input.toFixed(fractionSize);

            var numerics = String(input).split(".");
            numerics[0] = numerics[0].length == 0 ? "0" : numerics[0];
            var decimalValue: string = (numerics[1] == undefined) ? '' : numerics[1];

            if (numerics[1] == undefined || numerics[1].length < fractionSize)
            {
                var decimalValueLength: number = 0;

                if (numerics[1])
                {
                    decimalValue = numerics[1].slice(0, fractionSize);
                    decimalValueLength = decimalValue.length;
                }

                //เติม 0 ให้ครบตามจำนวน decimalPoint ที่กำหนด
                for (var i = 0; i < fractionSize - decimalValueLength; i++)
                {
                    decimalValue += "0";
                }
            }


            if (input)
            {
                numerics[0] = $filter("number")(numerics[0], 0);
                result = [numerics[0], decimalValue].join(".");
            }
            return result;
        };
    }

    Main.App.Filters.filter("decimal", ["$filter", ($filter) => DecimalFilter($filter)]);
}

