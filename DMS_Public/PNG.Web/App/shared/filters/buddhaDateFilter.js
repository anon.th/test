var Util;
(function (Util) {
    function BuddhaDateFilter($filter) {
        return function (input) {
            if (input == null) {
                return "";
            }
            var dateInput = new Date(input.toString());
            // Edit get date beacuse of if input date has a time it's will calulate time to timezone and +7 to date
            var day = $filter("date")(input, "dd");
            var month = (dateInput.getMonth() + 1).toString();
            var year = dateInput.getFullYear();
            if (year <= 2500) {
                year += 543;
            }
            var result = "{0}/{1}/{2}".format(day, month.padLeft(month, 2, "0"), year);
            return result;
        };
    }
    Util.BuddhaDateFilter = BuddhaDateFilter;
    Main.App.Filters.filter("buddhaDate", function ($filter) { return BuddhaDateFilter($filter); });
})(Util || (Util = {}));
//# sourceMappingURL=buddhaDateFilter.js.map