﻿module Util
{
    export function LimitToFilter()
    {
        return function (input, limit, begin)
        {
            if (Math.abs(Number(limit)) === Infinity)
            {
                limit = Number(limit);
            } else
            {
                limit = parseInt(limit);
            }
            if (isNaN(limit)) return input;

            if (typeof input === 'number') input = input.toString();
            if (typeof input !== 'object' && typeof input !== 'string') return input;

            begin = (!begin || isNaN(begin)) ? 0 : parseInt(begin);
            begin = (begin < 0) ? Math.max(0, input.length + begin) : begin;

            if (limit >= 0)
            {
                return input.slice(begin, begin + limit);
            } else
            {
                if (begin === 0)
                {
                    return input.slice(limit, input.length);
                } else
                {
                    return input.slice(Math.max(0, begin + limit), begin);
                }
            }
        };
    }

    Main.App.Filters.filter("limitToNew", LimitToFilter);
}
