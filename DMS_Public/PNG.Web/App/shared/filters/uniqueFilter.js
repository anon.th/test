var Util;
(function (Util) {
    function UniqueFilter() {
        return function (arr, field) {
            if (arr == null)
                return [];
            var o = {}, i, l = arr.length, r = [];
            for (i = 0; i < l; i += 1) {
                o[arr[i][field]] = arr[i];
            }
            for (i in o) {
                r.push(o[i]);
            }
            return r;
        };
    }
    Util.UniqueFilter = UniqueFilter;
    Main.App.Filters.filter("unique", UniqueFilter);
})(Util || (Util = {}));
//# sourceMappingURL=uniqueFilter.js.map