var Shared;
(function (Shared) {
    var UtilityHelper = (function () {
        function UtilityHelper() {
        }
        UtilityHelper.IsSubmitRequestExport = function (submitCode) {
            submitCode = submitCode.toUpperCase();
            var listExportCode = ["EXP9915", "EXP9931RECCONT", "EXP9932ALLOCATECONT", "EXP9933OVERCONT", "EXP9934CHEQUE_EXPIRED", "EXP9946"];
            return listExportCode.filter(function (l) { return l == submitCode; }).length > 0;
        };
        return UtilityHelper;
    }());
    Shared.UtilityHelper = UtilityHelper;
})(Shared || (Shared = {}));
//# sourceMappingURL=UtilityHelper.js.map