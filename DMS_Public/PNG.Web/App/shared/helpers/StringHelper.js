if (!String.prototype.format) {
    String.prototype.format = function () {
        var args = arguments;
        return this.replace(/{(\d+)}/g, function (match, number) {
            return typeof args[number] != 'undefined' ? "" + args[number] : match;
        });
    };
}
if (!String.prototype.padLeft) {
    String.prototype.padLeft = function (source, length, charPad) {
        if (!source || !charPad || source.length >= length) {
            return source;
        }
        var max = (length - source.length) / charPad.length;
        for (var i = 0; i < max; i++) {
            source = charPad + source;
        }
        return source;
    };
}
if (!String.prototype.replaceAll) {
    String.prototype.replaceAll = function (search, replacement) {
        var target = this;
        return target.replace(new RegExp(search, 'g'), replacement);
    };
}
//# sourceMappingURL=StringHelper.js.map