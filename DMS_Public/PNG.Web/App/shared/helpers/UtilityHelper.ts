﻿module Shared
{
    export class UtilityHelper
    {
        public static IsSubmitRequestExport(submitCode: string): boolean
        {
            submitCode = submitCode.toUpperCase();
            var listExportCode: string[] = ["EXP9915", "EXP9931RECCONT", "EXP9932ALLOCATECONT", "EXP9933OVERCONT", "EXP9934CHEQUE_EXPIRED", "EXP9946"];
            return listExportCode.filter(l => l == submitCode).length > 0;
        }
    }
}