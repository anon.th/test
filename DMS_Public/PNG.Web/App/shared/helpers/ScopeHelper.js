var Shared;
(function (Shared) {
    var ScopeHelper = (function () {
        function ScopeHelper() {
        }
        ScopeHelper.GetValue = function (scope, path) {
            var ret;
            // for a simple path, just return the field value
            if (path.indexOf(".") == -1)
                ret = ScopeHelper.scopeRecurse(scope, path);
            else {
                ret = scope;
                // break up the path elements into separate array indices
                var pathElements = path.split(".");
                pathElements.forEach(function (e) {
                    if (ret)
                        ret = ScopeHelper.scopeRecurse(ret, e);
                });
            }
            return ret;
        };
        ScopeHelper.GetValueScope = function (scope, path) {
            var ret = scope;
            // for a simple path, just return the field value
            if (path.indexOf(".") != -1) {
                // break up the path elements into separate array indices
                var pathElements = path.split(".");
                for (var i = 0; i < pathElements.length - 1; i++) {
                    if (ret)
                        ret = ScopeHelper.scopeRecurseScope(ret, pathElements[i]);
                }
            }
            return ret;
        };
        ScopeHelper.scopeRecurse = function (scope, member) {
            // if we have the member, return it
            if (scope.hasOwnProperty(member))
                return scope[member];
            // if we have a parent - recurse
            if (scope.hasOwnProperty("$parent") && scope.$parent)
                return ScopeHelper.scopeRecurse(scope.$parent, member);
            else
                return undefined;
        };
        ScopeHelper.scopeRecurseScope = function (scope, member) {
            // if we have the member, return it
            if (scope.hasOwnProperty(member))
                return scope[member];
            else
                return scope;
        };
        ScopeHelper.CopyValue = function (objectFrom, objectTo) {
            for (var propertyName in objectFrom) {
                if (!angular.isUndefined(objectTo[propertyName])) {
                    objectTo[propertyName] = objectFrom[propertyName];
                }
            }
            return objectTo;
        };
        ScopeHelper.GetObjectData = function (data, dataName) {
            var arrDataName = dataName.split('.');
            if (arrDataName.length) {
                if (arrDataName.length == 1) {
                    return data[arrDataName[0]];
                }
                else if (arrDataName.length == 2) {
                    return data[arrDataName[0]] ? data[arrDataName[0]][arrDataName[1]] : null;
                }
                else if (arrDataName.length == 3) {
                    return data[arrDataName[0]] && data[arrDataName[0]][arrDataName[1]] ? data[arrDataName[0]][arrDataName[1]][arrDataName[2]] : null;
                }
            }
        };
        return ScopeHelper;
    }());
    Shared.ScopeHelper = ScopeHelper;
})(Shared || (Shared = {}));
//# sourceMappingURL=ScopeHelper.js.map