﻿module Shared
{
    export class DateTimeHelper
    {
        public static Factory(
            $filter: any)
        {
            return new DateTimeHelper($filter);
        }

        constructor(
            private $filter: any)
        {
        }

        public static DateNowString(): string
        {
            var now = new Date();
            return now.getFullYear() + "-" + (now.getMonth() + 1) + "-" + (now.getDate());
        }

        public GetStartTimeOfDate(date: Date): string
        {
            return this.$filter("date")(date, "yyyy-MM-dd 0:00:00"); /* "yyyy-MM-dd 0:00:00"*/
        }

        public GetEndTimeOfDate(date: Date): string
        {
            return this.$filter("date")(date, "yyyy-MM-dd 23:59:59");
        }

        public static GetOnlyDate(date: Date): any
        {
            return date.toString().substr(0, 10);
        }
    }

    Main.App.Services.factory('dateTimeHelperService', ["$filter", DateTimeHelper.Factory]);
}