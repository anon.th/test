﻿interface String
{
    format(...replacements: any[]): string;
    padLeft(source: string, length: number, charPad: string): string
    isDate(): boolean;
    replaceAll(search: string, replacement: string): string;
}

if (!String.prototype.format)
{
    String.prototype.format = function ()
    {
        var args = arguments;
        return this.replace(/{(\d+)}/g, (match, number) =>
            typeof args[number] != 'undefined' ? "" + args[number] : match);
    };
}

if (!String.prototype.padLeft)
{
    String.prototype.padLeft = (source: string, length: number, charPad: string) =>
    {
        if (!source || !charPad || source.length >= length)
        {
            return source;
        }
        var max = (length - source.length) / charPad.length;
        for (var i = 0; i < max; i++)
        {
            source = charPad + source;
        }
        return source;
    };
}


if (!String.prototype.replaceAll)
{
    String.prototype.replaceAll = function (search, replacement)
    {
        var target = this;
        return target.replace(new RegExp(search, 'g'), replacement);
    };

}

