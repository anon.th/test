﻿module Shared
{
    export class FileHelper
    {
        public static Base64ToBlob(base64Data: string, contentType: string): Blob
        {
            contentType = contentType || '';
            var sliceSize = 1024;
            var byteCharacters = atob(base64Data);
            var bytesLength = byteCharacters.length;
            var slicesCount = Math.ceil(bytesLength / sliceSize);
            var byteArrays = new Array(slicesCount);

            for (var sliceIndex = 0; sliceIndex < slicesCount; ++sliceIndex)
            {
                var begin = sliceIndex * sliceSize;
                var end = Math.min(begin + sliceSize, bytesLength);

                var bytes = new Array(end - begin);
                for (var offset = begin, i = 0; offset < end; ++i, ++offset)
                {
                    bytes[i] = byteCharacters[offset].charCodeAt(0);
                }
                byteArrays[sliceIndex] = new Uint8Array(bytes);
            }
            return new Blob(byteArrays, { type: contentType });
        }

        // Remove input type file 
        public static ClearInputFile(elementName: string)
        {
            var inputFile = document.getElementById(elementName);
            if (/MSIE/.test(navigator.userAgent))
            {
                try
                {
                    // for IE5 - IE10
                    var form = document.createElement("form");
                    var ref = inputFile.nextSibling;

                    form.appendChild(inputFile);
                    form.reset();
                    ref.parentNode.insertBefore(inputFile, ref);
                }
                catch (err) { }
            }
            else
            {
                // normal reset behavior for other sane browsers
                $('#' + elementName).val(null);
            }
        }
    }
}