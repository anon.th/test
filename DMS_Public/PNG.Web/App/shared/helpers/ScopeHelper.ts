﻿module Shared
{
    export class ScopeHelper
    {
        public static GetValue(scope: any, path: string): any
        {
            var ret;

            // for a simple path, just return the field value
            if (path.indexOf(".") == -1)
                ret = ScopeHelper.scopeRecurse(scope, path);
            else
            {
                ret = scope;
                // break up the path elements into separate array indices
                var pathElements = path.split(".");
                pathElements.forEach((e) =>
                {
                    if (ret)
                        ret = ScopeHelper.scopeRecurse(ret, e);
                });
            }

            return ret;
        }

        public static GetValueScope(scope: any, path: string): any
        {
            var ret = scope;

            // for a simple path, just return the field value
            if (path.indexOf(".") != -1)
            {
                // break up the path elements into separate array indices
                var pathElements = path.split(".");
                for (var i = 0; i < pathElements.length - 1; i++)
                {
                    if (ret)
                        ret = ScopeHelper.scopeRecurseScope(ret, pathElements[i]);
                }
            }

            return ret;
        }

        private static scopeRecurse(scope: any, member: string): any
        {
            // if we have the member, return it
            if (scope.hasOwnProperty(member))
                return scope[member];

            // if we have a parent - recurse
            if (scope.hasOwnProperty("$parent") && scope.$parent)
                return ScopeHelper.scopeRecurse(scope.$parent, member);
            else
                return undefined;
        }

        private static scopeRecurseScope(scope: any, member: string): any
        {
            // if we have the member, return it
            if (scope.hasOwnProperty(member))
                return scope[member];
            else
                return scope;
        }

        public static CopyValue(objectFrom: any, objectTo: any): any
        {
            for (var propertyName in objectFrom)
            {
                if (!angular.isUndefined(objectTo[propertyName]))
                {
                    objectTo[propertyName] = objectFrom[propertyName];
                }
            }
            return objectTo;
        }

        public static GetObjectData(data: any, dataName: string): any
        {

            var arrDataName = dataName.split('.');

            if (arrDataName.length)
            {
                if (arrDataName.length == 1)
                {
                    return data[arrDataName[0]];
                }
                else if (arrDataName.length == 2)
                {

                    return data[arrDataName[0]] ? data[arrDataName[0]][arrDataName[1]] : null;
                }
                else if (arrDataName.length == 3)
                {
                    return data[arrDataName[0]] && data[arrDataName[0]][arrDataName[1]] ? data[arrDataName[0]][arrDataName[1]][arrDataName[2]] : null;
                }
            }
        }
    }
}