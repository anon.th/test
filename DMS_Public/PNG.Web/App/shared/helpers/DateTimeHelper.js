var Shared;
(function (Shared) {
    var DateTimeHelper = (function () {
        function DateTimeHelper($filter) {
            this.$filter = $filter;
        }
        DateTimeHelper.Factory = function ($filter) {
            return new DateTimeHelper($filter);
        };
        DateTimeHelper.DateNowString = function () {
            var now = new Date();
            return now.getFullYear() + "-" + (now.getMonth() + 1) + "-" + (now.getDate());
        };
        DateTimeHelper.prototype.GetStartTimeOfDate = function (date) {
            return this.$filter("date")(date, "yyyy-MM-dd 0:00:00"); /* "yyyy-MM-dd 0:00:00"*/
        };
        DateTimeHelper.prototype.GetEndTimeOfDate = function (date) {
            return this.$filter("date")(date, "yyyy-MM-dd 23:59:59");
        };
        DateTimeHelper.GetOnlyDate = function (date) {
            return date.toString().substr(0, 10);
        };
        return DateTimeHelper;
    }());
    Shared.DateTimeHelper = DateTimeHelper;
    Main.App.Services.factory('dateTimeHelperService', ["$filter", DateTimeHelper.Factory]);
})(Shared || (Shared = {}));
//# sourceMappingURL=DateTimeHelper.js.map