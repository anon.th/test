﻿module Enums
{
    export enum ExportType
    {
        RA = 407, // Reallocation
        RB = 408, // Rebalance
        RR = 409, // Reallocation & Rebalance
        S2M = 410 // Convert to Single Fund to Master Fund
    }

    export enum ExportTemplateType
    {
        ExpTransferOutTemplate = 14,
        ExpTransferInTemplate = 16,
        ExpInitialMember = 23,
        ExportReallocation = 25,
        ExportRebalance = 26,
        ExportReallocationAndRebalance = 27,
        ExportSingleFund2MasterFund = 28,
        ExpTransferAMCtoAMCOutTemplate = 32,
        ExpResignTemplate = 58,
        ExpEmployeeOutTemplate = 59,
        ExpMemberBalanceTransaction = 65,
        ExpChequeTransferIn = 74,
        ExpTransferMemberInToFund = 76,
        ExpAdjustmentBalance = 20001
    }
}