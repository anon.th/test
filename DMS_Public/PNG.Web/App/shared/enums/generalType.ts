﻿module Enums
{
    export enum UploadFileTransactionType
    {
        FCO = 20080,
        FRB = 20081,
        FRA = 20082,
        FRR = 20083,
        FRS = 20084,
        FIO = 20085,
        FEO = 20086
    }

    export enum ReportType
    {
        M = 20124,  //รายงานทะเบียนสมาชิก
        AM = 20125, //รายงานการลงทุน รายเดือน
        S = 20126,  //รายงาน Statement รายสมาชิก
        T = 20127,  //Template File
        AY = 20128  //รายงานการลงทุน รายปี
    }

    export enum SuitabilityTestReminderType
    {
        NoSuitabilityTest = 1,   //ไม่เคยมีแบบประเมินความเสี่ยง
        LessThanTwoMonth = 2,    //แบบประเมินครบกำหนดภายใน 2 เดือน
        DueTwoYear = 3,           //แบบประเมินครบกำหนด 2 ปี
        GreterThanTwoMonth = 4   //แบบประเมินยังไม่ครบกำหนดมากกว่า 2 เดือน
    }

    export enum TransactionSourceType
    {
        BackEnd = 20097,    // Back End
        FrontEnd = 20098    // Front End
    }

    export enum ReportFrontEndId
    {
        Rpt0304 = 28,   // รายงานการปรับสัดส่วนการลงทุนรายสมาชิก
        Rpt9925 = 114    // รายงานการปรับสัดส่วนการลงทุนรายสมาชิก
    }
    export enum BackStepType
    {
        NoneStrum = 0,
        PendingStrum = 1,
        Strumed = 2
    }
    export enum EventType
    {
        BeingBackStep = 0,
        BeingBackStepFromWebReq = 1
    }

    export enum AccessType
    {
        UploadFile = 21003,
        CompanyEmType = 21004,
        FrontEndReport = 21005
    }

    export enum ErrorType
    {
        InternalServerError = <any>"Internal Server Error, an unhandled exception was thrown by the services. Please contact your Administrator."
    }

    export enum RoleType
    {
        Member = 21001,
        Admin = 21002
    }
} 