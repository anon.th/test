var Enums;
(function (Enums) {
    (function (LoginRoleDefault) {
        LoginRoleDefault[LoginRoleDefault["Member"] = 0] = "Member";
        LoginRoleDefault[LoginRoleDefault["Committee"] = 1] = "Committee";
        LoginRoleDefault[LoginRoleDefault["Admin"] = 2] = "Admin";
    })(Enums.LoginRoleDefault || (Enums.LoginRoleDefault = {}));
    var LoginRoleDefault = Enums.LoginRoleDefault;
})(Enums || (Enums = {}));
//# sourceMappingURL=externalIdentityUser.js.map