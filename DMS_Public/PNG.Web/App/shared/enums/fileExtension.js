var Enums;
(function (Enums) {
    (function (FileExtension) {
        FileExtension[FileExtension["Csv"] = 46] = "Csv";
        FileExtension[FileExtension["Txt"] = 47] = "Txt";
        FileExtension[FileExtension["Xlsx"] = 48] = "Xlsx";
    })(Enums.FileExtension || (Enums.FileExtension = {}));
    var FileExtension = Enums.FileExtension;
})(Enums || (Enums = {}));
//# sourceMappingURL=fileExtension.js.map