﻿module Enums
{
    export enum FileFormat
    {
        Csv = 49,
        Excel = 50,
        FieldSepator = 51,
        FixedLength = 52
    }
}