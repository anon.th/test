// Declare a TypeScript enum using unique string values
var Enums;
(function (Enums) {
    (function (AppConfigurationCodeType) {
        AppConfigurationCodeType[AppConfigurationCodeType["MinLowercase"] = 'MinLowercase'] = "MinLowercase";
        AppConfigurationCodeType[AppConfigurationCodeType["MinUppercase"] = 'MinUppercase'] = "MinUppercase";
        AppConfigurationCodeType[AppConfigurationCodeType["MinNumber"] = 'MinNumber'] = "MinNumber";
        AppConfigurationCodeType[AppConfigurationCodeType["MinPasswordLength"] = 'MinPasswordLength'] = "MinPasswordLength";
        AppConfigurationCodeType[AppConfigurationCodeType["MinSpecialCharacter"] = 'MinSpecialCharacter'] = "MinSpecialCharacter";
    })(Enums.AppConfigurationCodeType || (Enums.AppConfigurationCodeType = {}));
    var AppConfigurationCodeType = Enums.AppConfigurationCodeType;
})(Enums || (Enums = {}));
//# sourceMappingURL=appConfigurationType.js.map