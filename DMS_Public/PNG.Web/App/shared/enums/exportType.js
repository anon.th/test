var Enums;
(function (Enums) {
    (function (ExportType) {
        ExportType[ExportType["RA"] = 407] = "RA";
        ExportType[ExportType["RB"] = 408] = "RB";
        ExportType[ExportType["RR"] = 409] = "RR";
        ExportType[ExportType["S2M"] = 410] = "S2M"; // Convert to Single Fund to Master Fund
    })(Enums.ExportType || (Enums.ExportType = {}));
    var ExportType = Enums.ExportType;
    (function (ExportTemplateType) {
        ExportTemplateType[ExportTemplateType["ExpTransferOutTemplate"] = 14] = "ExpTransferOutTemplate";
        ExportTemplateType[ExportTemplateType["ExpTransferInTemplate"] = 16] = "ExpTransferInTemplate";
        ExportTemplateType[ExportTemplateType["ExpInitialMember"] = 23] = "ExpInitialMember";
        ExportTemplateType[ExportTemplateType["ExportReallocation"] = 25] = "ExportReallocation";
        ExportTemplateType[ExportTemplateType["ExportRebalance"] = 26] = "ExportRebalance";
        ExportTemplateType[ExportTemplateType["ExportReallocationAndRebalance"] = 27] = "ExportReallocationAndRebalance";
        ExportTemplateType[ExportTemplateType["ExportSingleFund2MasterFund"] = 28] = "ExportSingleFund2MasterFund";
        ExportTemplateType[ExportTemplateType["ExpTransferAMCtoAMCOutTemplate"] = 32] = "ExpTransferAMCtoAMCOutTemplate";
        ExportTemplateType[ExportTemplateType["ExpResignTemplate"] = 58] = "ExpResignTemplate";
        ExportTemplateType[ExportTemplateType["ExpEmployeeOutTemplate"] = 59] = "ExpEmployeeOutTemplate";
        ExportTemplateType[ExportTemplateType["ExpMemberBalanceTransaction"] = 65] = "ExpMemberBalanceTransaction";
        ExportTemplateType[ExportTemplateType["ExpChequeTransferIn"] = 74] = "ExpChequeTransferIn";
        ExportTemplateType[ExportTemplateType["ExpTransferMemberInToFund"] = 76] = "ExpTransferMemberInToFund";
        ExportTemplateType[ExportTemplateType["ExpAdjustmentBalance"] = 20001] = "ExpAdjustmentBalance";
    })(Enums.ExportTemplateType || (Enums.ExportTemplateType = {}));
    var ExportTemplateType = Enums.ExportTemplateType;
})(Enums || (Enums = {}));
//# sourceMappingURL=exportType.js.map