var Enums;
(function (Enums) {
    (function (FileFormat) {
        FileFormat[FileFormat["Csv"] = 49] = "Csv";
        FileFormat[FileFormat["Excel"] = 50] = "Excel";
        FileFormat[FileFormat["FieldSepator"] = 51] = "FieldSepator";
        FileFormat[FileFormat["FixedLength"] = 52] = "FixedLength";
    })(Enums.FileFormat || (Enums.FileFormat = {}));
    var FileFormat = Enums.FileFormat;
})(Enums || (Enums = {}));
//# sourceMappingURL=fileFormat.js.map