﻿module Enums
{
    export enum MemberStatus
    {
        R = 71, // เกษียณรับเงินงวด
        H = 72, // คงเงิน
        A = 73, // ปกติ
        I = 74, // พ้นจากสมาชิกภาพ
        C = 75, // ยกเลิก
        N = 324 // New
    }
}