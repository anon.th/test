﻿module Enums
{
    export enum FileExtension
    {
        Csv = 46,
        Txt = 47,
        Xlsx = 48
    }
}