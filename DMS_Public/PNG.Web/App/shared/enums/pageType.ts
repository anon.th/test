﻿module Enums
{
    export enum PageType
    {
        Add,
        Edit,
        List,
        View,
        KeyIn, // Edit by Key-in
        Upload, // Edit by Upload
        Detail,
        Unlock,
        Register
    }
}