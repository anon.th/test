﻿// Declare a TypeScript enum using unique string values

module Enums
{
    export enum AppConfigurationCodeType
    {
        MinLowercase = <any>'MinLowercase',
        MinUppercase = <any>'MinUppercase',
        MinNumber = <any>'MinNumber',
        MinPasswordLength = <any>'MinPasswordLength',
        MinSpecialCharacter = <any>'MinSpecialCharacter'
    }
}