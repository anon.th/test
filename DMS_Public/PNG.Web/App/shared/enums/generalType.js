var Enums;
(function (Enums) {
    (function (UploadFileTransactionType) {
        UploadFileTransactionType[UploadFileTransactionType["FCO"] = 20080] = "FCO";
        UploadFileTransactionType[UploadFileTransactionType["FRB"] = 20081] = "FRB";
        UploadFileTransactionType[UploadFileTransactionType["FRA"] = 20082] = "FRA";
        UploadFileTransactionType[UploadFileTransactionType["FRR"] = 20083] = "FRR";
        UploadFileTransactionType[UploadFileTransactionType["FRS"] = 20084] = "FRS";
        UploadFileTransactionType[UploadFileTransactionType["FIO"] = 20085] = "FIO";
        UploadFileTransactionType[UploadFileTransactionType["FEO"] = 20086] = "FEO";
    })(Enums.UploadFileTransactionType || (Enums.UploadFileTransactionType = {}));
    var UploadFileTransactionType = Enums.UploadFileTransactionType;
    (function (ReportType) {
        ReportType[ReportType["M"] = 20124] = "M";
        ReportType[ReportType["AM"] = 20125] = "AM";
        ReportType[ReportType["S"] = 20126] = "S";
        ReportType[ReportType["T"] = 20127] = "T";
        ReportType[ReportType["AY"] = 20128] = "AY"; //รายงานการลงทุน รายปี
    })(Enums.ReportType || (Enums.ReportType = {}));
    var ReportType = Enums.ReportType;
    (function (SuitabilityTestReminderType) {
        SuitabilityTestReminderType[SuitabilityTestReminderType["NoSuitabilityTest"] = 1] = "NoSuitabilityTest";
        SuitabilityTestReminderType[SuitabilityTestReminderType["LessThanTwoMonth"] = 2] = "LessThanTwoMonth";
        SuitabilityTestReminderType[SuitabilityTestReminderType["DueTwoYear"] = 3] = "DueTwoYear";
        SuitabilityTestReminderType[SuitabilityTestReminderType["GreterThanTwoMonth"] = 4] = "GreterThanTwoMonth"; //แบบประเมินยังไม่ครบกำหนดมากกว่า 2 เดือน
    })(Enums.SuitabilityTestReminderType || (Enums.SuitabilityTestReminderType = {}));
    var SuitabilityTestReminderType = Enums.SuitabilityTestReminderType;
    (function (TransactionSourceType) {
        TransactionSourceType[TransactionSourceType["BackEnd"] = 20097] = "BackEnd";
        TransactionSourceType[TransactionSourceType["FrontEnd"] = 20098] = "FrontEnd"; // Front End
    })(Enums.TransactionSourceType || (Enums.TransactionSourceType = {}));
    var TransactionSourceType = Enums.TransactionSourceType;
    (function (ReportFrontEndId) {
        ReportFrontEndId[ReportFrontEndId["Rpt0304"] = 28] = "Rpt0304";
        ReportFrontEndId[ReportFrontEndId["Rpt9925"] = 114] = "Rpt9925"; // รายงานการปรับสัดส่วนการลงทุนรายสมาชิก
    })(Enums.ReportFrontEndId || (Enums.ReportFrontEndId = {}));
    var ReportFrontEndId = Enums.ReportFrontEndId;
    (function (BackStepType) {
        BackStepType[BackStepType["NoneStrum"] = 0] = "NoneStrum";
        BackStepType[BackStepType["PendingStrum"] = 1] = "PendingStrum";
        BackStepType[BackStepType["Strumed"] = 2] = "Strumed";
    })(Enums.BackStepType || (Enums.BackStepType = {}));
    var BackStepType = Enums.BackStepType;
    (function (EventType) {
        EventType[EventType["BeingBackStep"] = 0] = "BeingBackStep";
        EventType[EventType["BeingBackStepFromWebReq"] = 1] = "BeingBackStepFromWebReq";
    })(Enums.EventType || (Enums.EventType = {}));
    var EventType = Enums.EventType;
    (function (AccessType) {
        AccessType[AccessType["UploadFile"] = 21003] = "UploadFile";
        AccessType[AccessType["CompanyEmType"] = 21004] = "CompanyEmType";
        AccessType[AccessType["FrontEndReport"] = 21005] = "FrontEndReport";
    })(Enums.AccessType || (Enums.AccessType = {}));
    var AccessType = Enums.AccessType;
    (function (ErrorType) {
        ErrorType[ErrorType["InternalServerError"] = "Internal Server Error, an unhandled exception was thrown by the services. Please contact your Administrator."] = "InternalServerError";
    })(Enums.ErrorType || (Enums.ErrorType = {}));
    var ErrorType = Enums.ErrorType;
    (function (RoleType) {
        RoleType[RoleType["Member"] = 21001] = "Member";
        RoleType[RoleType["Admin"] = 21002] = "Admin";
    })(Enums.RoleType || (Enums.RoleType = {}));
    var RoleType = Enums.RoleType;
})(Enums || (Enums = {}));
//# sourceMappingURL=generalType.js.map