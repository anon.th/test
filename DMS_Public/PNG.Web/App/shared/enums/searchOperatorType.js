var Enums;
(function (Enums) {
    (function (SearchOperatorType) {
        SearchOperatorType[SearchOperatorType["Like"] = 0] = "Like";
        SearchOperatorType[SearchOperatorType["Equal"] = 1] = "Equal";
        SearchOperatorType[SearchOperatorType["NotEqual"] = 2] = "NotEqual";
        SearchOperatorType[SearchOperatorType["GreaterEqual"] = 3] = "GreaterEqual";
        SearchOperatorType[SearchOperatorType["LessEqual"] = 4] = "LessEqual";
        SearchOperatorType[SearchOperatorType["LessOrEqualWithOwnTime"] = 5] = "LessOrEqualWithOwnTime";
        SearchOperatorType[SearchOperatorType["In"] = 6] = "In";
        SearchOperatorType[SearchOperatorType["NotIn"] = 7] = "NotIn";
        SearchOperatorType[SearchOperatorType["Is"] = 8] = "Is";
        SearchOperatorType[SearchOperatorType["GreaterThan"] = 9] = "GreaterThan";
        SearchOperatorType[SearchOperatorType["LessThan"] = 10] = "LessThan";
    })(Enums.SearchOperatorType || (Enums.SearchOperatorType = {}));
    var SearchOperatorType = Enums.SearchOperatorType;
})(Enums || (Enums = {}));
//# sourceMappingURL=searchOperatorType.js.map