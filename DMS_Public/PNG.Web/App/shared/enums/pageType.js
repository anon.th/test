var Enums;
(function (Enums) {
    (function (PageType) {
        PageType[PageType["Add"] = 0] = "Add";
        PageType[PageType["Edit"] = 1] = "Edit";
        PageType[PageType["List"] = 2] = "List";
        PageType[PageType["View"] = 3] = "View";
        PageType[PageType["KeyIn"] = 4] = "KeyIn";
        PageType[PageType["Upload"] = 5] = "Upload";
        PageType[PageType["Detail"] = 6] = "Detail";
        PageType[PageType["Unlock"] = 7] = "Unlock";
        PageType[PageType["Register"] = 8] = "Register";
    })(Enums.PageType || (Enums.PageType = {}));
    var PageType = Enums.PageType;
})(Enums || (Enums = {}));
//# sourceMappingURL=pageType.js.map