﻿module Enums
{
    export enum SearchConditionType
    {
        And,
        Or,
        Not
    }
}