﻿module Enums
{
    export enum UploadFileTransactionStatus
    {
        Save = 20023, //รอดำเนินการ
        Cancel = 20024, //ยกเลิกการวางไฟล์
        Approve = 20025 //ได้รับข้อมูลเรียบร้อยแล้ว
    }

    export enum ReallocationRebalanceStatus
    {
        /*
        56	S  	บันทึก (BackEnd), รอดำเนินการ (FrontEnd)
        57	WA 	รออนุมัติ
        58	RA 	เปลี่ยนแผนแล้ว
        59	WR 	รอเปลี่ยนแผน
        60	WF 	รอแก้ไข
        61	C  	ยกเลิก
        94	PA 	รอจัดสรร
        95	ARO	จัดสรร Rebalance Out
        96	ARI	จัดสรร Rebalance In
        318	CRO	ยืนยันจัดสรร Rebalance Out
        319	CRI	ยืนยันจัดสรร Rebalance In
        */
        Save = 56,
        WaitApprove = 57,
        ReallocationAlready = 58,
        WaitReallocation = 59,
        WaitFix = 60,
        Cancel = 61,
        PendingApprove = 94,
        ApproveReblanceOut = 95,
        ApproveReblanceIn = 96,
        ConfirmApproveReblanceOut = 318,
        ConfirmApproveReblanceIn = 319
    }
}