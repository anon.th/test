﻿module Enums
{
    export enum CalculationType
    {
        Reallocation = 352, // การเปลี่ยนสัดส่วนการลงทุนเงินนำส่ง
        Rebalance = 353     // การปรับสัดส่วนการลงทุน
    }
}