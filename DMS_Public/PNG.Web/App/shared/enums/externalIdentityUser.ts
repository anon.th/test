﻿module Enums
{
    export enum LoginRoleDefault
    {
        Member,
        Committee,
        Admin
    }
}