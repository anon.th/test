var Enums;
(function (Enums) {
    (function (MemberStatus) {
        MemberStatus[MemberStatus["R"] = 71] = "R";
        MemberStatus[MemberStatus["H"] = 72] = "H";
        MemberStatus[MemberStatus["A"] = 73] = "A";
        MemberStatus[MemberStatus["I"] = 74] = "I";
        MemberStatus[MemberStatus["C"] = 75] = "C";
        MemberStatus[MemberStatus["N"] = 324] = "N"; // New
    })(Enums.MemberStatus || (Enums.MemberStatus = {}));
    var MemberStatus = Enums.MemberStatus;
})(Enums || (Enums = {}));
//# sourceMappingURL=memberStatus.js.map