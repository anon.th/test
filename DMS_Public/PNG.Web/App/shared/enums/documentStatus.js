var Enums;
(function (Enums) {
    (function (UploadFileTransactionStatus) {
        UploadFileTransactionStatus[UploadFileTransactionStatus["Save"] = 20023] = "Save";
        UploadFileTransactionStatus[UploadFileTransactionStatus["Cancel"] = 20024] = "Cancel";
        UploadFileTransactionStatus[UploadFileTransactionStatus["Approve"] = 20025] = "Approve"; //ได้รับข้อมูลเรียบร้อยแล้ว
    })(Enums.UploadFileTransactionStatus || (Enums.UploadFileTransactionStatus = {}));
    var UploadFileTransactionStatus = Enums.UploadFileTransactionStatus;
    (function (ReallocationRebalanceStatus) {
        /*
        56	S  	บันทึก (BackEnd), รอดำเนินการ (FrontEnd)
        57	WA 	รออนุมัติ
        58	RA 	เปลี่ยนแผนแล้ว
        59	WR 	รอเปลี่ยนแผน
        60	WF 	รอแก้ไข
        61	C  	ยกเลิก
        94	PA 	รอจัดสรร
        95	ARO	จัดสรร Rebalance Out
        96	ARI	จัดสรร Rebalance In
        318	CRO	ยืนยันจัดสรร Rebalance Out
        319	CRI	ยืนยันจัดสรร Rebalance In
        */
        ReallocationRebalanceStatus[ReallocationRebalanceStatus["Save"] = 56] = "Save";
        ReallocationRebalanceStatus[ReallocationRebalanceStatus["WaitApprove"] = 57] = "WaitApprove";
        ReallocationRebalanceStatus[ReallocationRebalanceStatus["ReallocationAlready"] = 58] = "ReallocationAlready";
        ReallocationRebalanceStatus[ReallocationRebalanceStatus["WaitReallocation"] = 59] = "WaitReallocation";
        ReallocationRebalanceStatus[ReallocationRebalanceStatus["WaitFix"] = 60] = "WaitFix";
        ReallocationRebalanceStatus[ReallocationRebalanceStatus["Cancel"] = 61] = "Cancel";
        ReallocationRebalanceStatus[ReallocationRebalanceStatus["PendingApprove"] = 94] = "PendingApprove";
        ReallocationRebalanceStatus[ReallocationRebalanceStatus["ApproveReblanceOut"] = 95] = "ApproveReblanceOut";
        ReallocationRebalanceStatus[ReallocationRebalanceStatus["ApproveReblanceIn"] = 96] = "ApproveReblanceIn";
        ReallocationRebalanceStatus[ReallocationRebalanceStatus["ConfirmApproveReblanceOut"] = 318] = "ConfirmApproveReblanceOut";
        ReallocationRebalanceStatus[ReallocationRebalanceStatus["ConfirmApproveReblanceIn"] = 319] = "ConfirmApproveReblanceIn";
    })(Enums.ReallocationRebalanceStatus || (Enums.ReallocationRebalanceStatus = {}));
    var ReallocationRebalanceStatus = Enums.ReallocationRebalanceStatus;
})(Enums || (Enums = {}));
//# sourceMappingURL=documentStatus.js.map