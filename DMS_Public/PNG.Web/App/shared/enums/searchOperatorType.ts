﻿module Enums
{
    export enum SearchOperatorType
    {
        Like,
        Equal,
        NotEqual,
        GreaterEqual,
        LessEqual,
        LessOrEqualWithOwnTime,
        In,
        NotIn,
        Is,
        GreaterThan,
        LessThan
    }
}