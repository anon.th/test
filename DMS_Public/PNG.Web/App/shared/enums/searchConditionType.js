var Enums;
(function (Enums) {
    (function (SearchConditionType) {
        SearchConditionType[SearchConditionType["And"] = 0] = "And";
        SearchConditionType[SearchConditionType["Or"] = 1] = "Or";
        SearchConditionType[SearchConditionType["Not"] = 2] = "Not";
    })(Enums.SearchConditionType || (Enums.SearchConditionType = {}));
    var SearchConditionType = Enums.SearchConditionType;
})(Enums || (Enums = {}));
//# sourceMappingURL=searchConditionType.js.map