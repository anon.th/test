﻿//** Modify By Nayfido 15.12.2558 **//
// สามารถ compare กับฟิลที่ต้องการ และแสดงการแจ้งเตือนทั้ง 2 ฟิล
// ป้องกันการแจ้งเตือนซ้ำใน alert

module Validation
{
    export class ValidationRule<T>
    {
        ValidationExpression(object: T, value: any): boolean { return false; }

        ValidationMessage: string;
        ValidationField: string;
        IsShow: boolean; //กำหนดให้แสดง validate หรือไม่
        ErrorOrder: number;//ระบุตำแหน่งที่จะแสดง error

        constructor(validationExpression: (object: T, value: any) => boolean, message: string, field?: string, isShow: boolean = true, errorOrder: number = 0)
        {
            this.ValidationExpression = validationExpression;
            this.ValidationMessage = message;
            if (typeof field !== "undefined")
                this.ValidationField = field;

            if (typeof isShow !== "undefined")
                this.IsShow = isShow;

            if (typeof errorOrder !== "undefined" && errorOrder != 0)
                this.ErrorOrder = errorOrder;
        }
    }

    export interface ValidationError
    {
        Error: string;
        Field?: string;
        Entity?: string;
        Argument1?: any;
        Argument2?: any;
        ErrorOrder?: number; // จะทำการเช็คเฉพาะ [0] หากมีค่าจะทำการ sorting แล้วนำไปแสดง
    }

    export class Validator<T>
    {
        ValidationRules: ValidationRule<T>[];
        ValidationErrors: ValidationError[] = [];

        bbl = Math.random();

        private lock: boolean = false;

        CurrentError(field: string): string
        {
            var fieldErrors = Util.AngularLinq.Where(this.ValidationErrors, e => field === '' || e.Field === field);
            var errorCount = fieldErrors.length;
            return errorCount === 0 ? "" : fieldErrors[0].Error;
        }

        Validate(entity: T): boolean
        {
            if (!this.ValidationRules)
                return true;
            if (!this.lock)
            {
                this.lock = true;
                this.ValidationErrors.length = 0;
                // check all validation rules
                for (var i = 0; i < this.ValidationRules.length; i++)
                {
                    var r = this.ValidationRules[i];

                    //ถ้า !isShow คือไม่ต้องการให้แสดงใน validate alert
                    if (!r.IsShow)
                    {
                        continue;
                    }

                    var listValue = r.ValidationField.split('.');
                    var value: any;
                    if (listValue.length == 1)
                    {
                        if (angular.isUndefined(entity[listValue[0]]))
                            value = undefined;
                        else
                            value = entity[listValue[0]];
                    }
                    else if (listValue.length == 2)
                    {
                        value = entity[listValue[0]] && entity[listValue[0]][listValue[1]] ? entity[listValue[0]][listValue[1]] : undefined;
                    }
                    else if (listValue.length == 3)
                    {
                        value = entity[listValue[0]] && entity[listValue[0]][listValue[1]] && entity[listValue[0]][listValue[1]][listValue[2]] ? entity[listValue[0]][listValue[1]][listValue[2]] : undefined;
                    }
                    else if (listValue.length == 4)
                    {
                        value = entity[listValue[0]] && entity[listValue[0]][listValue[1]] && entity[listValue[0]][listValue[1]][listValue[2]] && entity[listValue[0]][listValue[1]][listValue[2]][listValue[3]] ? entity[listValue[0]][listValue[1]][listValue[2]][listValue[3]] : undefined;
                    }
                    Validator.ValidateRule(entity, value, r, this.ValidationErrors);
                }
                this.lock = false;
                return (this.ValidationErrors.length == 0);
            }
            return true;
        }

        ValidateField(entity: T, value: any, field: string): boolean
        {
            if (!this.lock)
            {
                this.lock = true;
                this.ValidationErrors.length = 0;
                // check all validation rules that match field name
                Util.AngularLinq.ForEach(Util.AngularLinq.Where(this.ValidationRules, r => r.ValidationField == field), r => { Validator.ValidateRule(entity, value, r, this.ValidationErrors); });
                this.lock = false;
                return (this.ValidationErrors.length == 0);
            }
            return true;
        }

        public static ValidateRule<T>(entity: T, value: any, rule: ValidationRule<T>, errors: ValidationError[]): boolean
        {
            if (!rule.ValidationExpression(entity, value))
            {
                errors.push({ Error: rule.ValidationMessage, Field: rule.ValidationField, ErrorOrder: rule.ErrorOrder });
                return false;
            }
            return true;
        }

        // factory method
        static For<T>(rules: ValidationRule<T>[]): Validator<T>
        {
            var val = new Validator<T>();
            val.ValidationRules = rules;
            return val;
        }
    }

    export class DefaultValidationRules
    {
        public serviceProxy: Util.WebRequestService;


        static LoginData: ValidationRule<Interfaces.ILoginData>[] =
        [
            new ValidationRule<Interfaces.ILoginData>((e, v) => ValueHelper.IsNotBlank(v), "ValidationFundTaxNo", "userName"),
            new ValidationRule<Interfaces.ILoginData>((e, v) => ValueHelper.IsTaxNumberValid(v), "ValidationLoginUsernameFormat", "userName"),
            new ValidationRule<Interfaces.ILoginData>((e, v) => ValueHelper.IsNotBlank(v), "ValidationPassword", "password")
        ]
        static LoginDataTax: ValidationRule<Interfaces.ILoginData>[] =
        [
            new ValidationRule<Interfaces.ILoginData>((e, v) => ValueHelper.IsTaxNumberValid(v), "ValidationLoginUsernameFormat", "userName")
        ]
        static LoginDataUnlockUser: ValidationRule<Interfaces.ILoginData>[] =
        [
            new ValidationRule<Interfaces.ILoginData>((e, v) => ValueHelper.IsNotBlank(v), "ValidationUserName", "userName"),
            new ValidationRule<Interfaces.ILoginData>((e, v) => ValueHelper.IsNotBlank(v), "ValidationEmail", "email")
        ]
        static LoginDataUnlockUserFormat: ValidationRule<Interfaces.ILoginData>[] =
        [
            new ValidationRule<Interfaces.ILoginData>((e, v) => ValueHelper.IsEmailValid(v), "ValidationEmailFormat", "email"),
            new ValidationRule<Interfaces.ILoginData>((e, v) => ValueHelper.IsTaxNumberValid(v), "ValidationLoginUsernameFormat", "userName")
        ]
        static ExternalIdentityUsers: ValidationRule<IBackOffices.IExternalIdentityUsers>[] =
        [
            new ValidationRule<IBackOffices.IExternalIdentityUsers>((e, v) => ValueHelper.IsNotBlank(v), "ValidationUserName", "UserName"),
            new ValidationRule<IBackOffices.IExternalIdentityUsers>((e, v) => !ValueHelper.IsNotBlank(v) || ValueHelper.IsUserNameValid(v), "ValidationUserNameReg", "UserName"),
            new ValidationRule<IBackOffices.IExternalIdentityUsers>((e, v) => ValueHelper.IsNotBlank(v), "ValidationEmail", "Email"),
            new ValidationRule<IBackOffices.IExternalIdentityUsers>((e, v) => !ValueHelper.IsNotBlank(v) || ValueHelper.IsEmailValid(v), "ValidationEmailFormat", "Email"),
            new ValidationRule<IBackOffices.IExternalIdentityUsers>((e, v) => ValueHelper.IsNotBlank(v) ? ValueHelper.IsPasswordValid(v) : true, "ValidationPasswordFormat", "PasswordHash"),
            //new ValidationRule<IBackOffices.IExternalIdentityUsers>((e, v) => ValueHelper.IsNotBlank(v) ? ValueHelper.IsPasswordValid(v) : true, "ValidationConfirmPasswordFormat", "ConfirmPassword"),
            //new ValidationRule<IBackOffices.IExternalIdentityUsers>((e, v) => ValueHelper.IsIdentityUsersSamePasswordValid(e), "ValidationPasswordSameConfirmPassword", "PasswordHash"),
            //new ValidationRule<IBackOffices.IExternalIdentityUsers>((e, v) => ValueHelper.IsIdentityUsersSamePasswordValid(e), "ValidationPasswordSameConfirmPassword", "ConfirmPassword", false)
        ];
        static NewExternalIdentityUsers: ValidationRule<IBackOffices.IExternalIdentityUsers>[] =
        [
            new ValidationRule<IBackOffices.IExternalIdentityUsers>((e, v) => ValueHelper.IsNotBlank(v), "ValidationUserName", "UserName"),
            new ValidationRule<IBackOffices.IExternalIdentityUsers>((e, v) => ValueHelper.IsNotBlank(v), "ValidationEmail", "Email"),
            new ValidationRule<IBackOffices.IExternalIdentityUsers>((e, v) => !ValueHelper.IsNotBlank(v) || ValueHelper.IsEmailValid(v), "ValidationEmailFormat", "Email"),
            new ValidationRule<IBackOffices.IExternalIdentityUsers>((e, v) => ValueHelper.IsNotBlank(v), "ValidationPasswordHash", "PasswordHash"),
            new ValidationRule<IBackOffices.IExternalIdentityUsers>((e, v) => ValueHelper.IsNotBlank(v), "ValidationConfirmPassword", "ConfirmPassword"),
            new ValidationRule<IBackOffices.IExternalIdentityUsers>((e, v) => !ValueHelper.IsNotBlank(v) || ValueHelper.IsPasswordValid(v), "ValidationPasswordFormat", "PasswordHash"),
            new ValidationRule<IBackOffices.IExternalIdentityUsers>((e, v) => !ValueHelper.IsNotBlank(v) || ValueHelper.IsPasswordValid(v), "ValidationConfirmPasswordFormat", "ConfirmPassword")
            //new ValidationRule<IBackOffices.IExternalIdentityUsers>((e, v) => !ValueHelper.IsNotBlank(v) || ValueHelper.IsIdentityUsersSamePasswordValid(e), "ValidationPasswordSameConfirmPassword", "PasswordHash"),
            //new ValidationRule<IBackOffices.IExternalIdentityUsers>((e, v) => !ValueHelper.IsNotBlank(v) || ValueHelper.IsIdentityUsersSamePasswordValid(e), "ValidationPasswordSameConfirmPassword", "ConfirmPassword", false)
        ];

        static NewExternalIdentityUsersRegisterSubmit: ValidationRule<Interfaces.IExternalIdentityUserRegister>[] =
        [
            new ValidationRule<Interfaces.IExternalIdentityUserRegister>((e, v) => ValueHelper.IsNotBlank(v), "ValidationRegisterUserRequireFieldRefCode", "RefCode") //ValidateRequestUserRequireField
        ];


        static NewExternalIdentityUsersSubmit: ValidationRule<IBackOffices.IExternalIdentityUsers>[] =
        [
            new ValidationRule<IBackOffices.IExternalIdentityUsers>((e, v) => ValueHelper.IsNotBlank(v), "ValidationRequestUserRequireFieldTaxpayerIdentificationNumber", "UserName"),
            new ValidationRule<IBackOffices.IExternalIdentityUsers>((e, v) => !ValueHelper.IsNotBlank(v) || ValueHelper.IsTaxpayerValid(v), "ValidationRequestUserIDCardFormat", "UserName"),
            new ValidationRule<IBackOffices.IExternalIdentityUsers>((e, v) => ValueHelper.IsNotBlank(v), "ValidationPassword", "PasswordHash"),
            new ValidationRule<IBackOffices.IExternalIdentityUsers>((e, v) => !ValueHelper.IsNotBlank(v) || ValueHelper.IsPasswordValidFrontEndUser(e, v), "ValidationPasswordFormat", "PasswordHash"),
            new ValidationRule<IBackOffices.IExternalIdentityUsers>((e, v) => ValueHelper.IsNotBlank(v), "ValidationConfirmPassword", "ConfirmPassword")
        ];


        static NewExternalIdentityUserRegister: ValidationRule<Interfaces.IExternalIdentityUserRegister>[] =
        [
            new ValidationRule<Interfaces.IExternalIdentityUserRegister>((e, v) => ValueHelper.IsNotBlank(v), "ValidationRequestUserRequireFieldFirstname", "Firstname"), //ValidateRequestUserRequireField
            new ValidationRule<Interfaces.IExternalIdentityUserRegister>((e, v) => ValueHelper.IsNotBlank(v), "ValidationRequestUserRequireFieldLastname", "Lastname"), //ValidateRequestUserRequireField
            new ValidationRule<Interfaces.IExternalIdentityUserRegister>((e, v) => ValueHelper.IsNotBlank(v), "ValidationRequestUserRequireFieldTaxpayerIdentificationNumber", "TaxpayerIdentificationNumber"),
            new ValidationRule<Interfaces.IExternalIdentityUserRegister>((e, v) => !ValueHelper.IsNotBlank(v) || ValueHelper.IsTaxpayerValid(v), "ValidationRequestUserIDCardFormat", "TaxpayerIdentificationNumber"),
            new ValidationRule<Interfaces.IExternalIdentityUserRegister>((e, v) => ValueHelper.IsNotBlank(v), "ValidationRequestUserRequireFieldEmail", "Email"),
            new ValidationRule<Interfaces.IExternalIdentityUserRegister>((e, v) => ValueHelper.IsNotBlank(v), "ValidationRequestUserRequireFieldEmailConfirm", "EmailConfirm"),
            new ValidationRule<Interfaces.IExternalIdentityUserRegister>((e, v) => !ValueHelper.IsNotBlank(v) || ValueHelper.IsEmailValid(v), "ValidationRequestUserEmailFormat", "Email"),
            new ValidationRule<Interfaces.IExternalIdentityUserRegister>((e, v) => !ValueHelper.IsNotBlank(v) || ValueHelper.IsEmailValid(v), "ValidationRequestUserEmailFormat", "EmailConfirm")
        ];

        static NewForgotPasswordViewModel: ValidationRule<Interfaces.IForgotPasswordViewModel>[] =
        [
            new ValidationRule<Interfaces.IForgotPasswordViewModel>((e, v) => ValueHelper.IsNotBlank(v), "ValidationEmail", "Email"),
            new ValidationRule<Interfaces.IForgotPasswordViewModel>((e, v) => ValueHelper.IsNotBlank(v), "ValidationTaxpayerIdentificationNumber", "UserName")
        ];

        static NewForgotPasswordEmailFormat: ValidationRule<Interfaces.IForgotPasswordViewModel>[] =
        [
            new ValidationRule<Interfaces.IForgotPasswordViewModel>((e, v) => !ValueHelper.IsNotBlank(v) || ValueHelper.IsEmailValid(v), "ValidationEmailFormat", "Email")
        ];

        static NewResetPasswordViewModel: ValidationRule<Interfaces.IResetPasswordViewModel>[] =
        [
            new ValidationRule<Interfaces.IResetPasswordViewModel>((e, v) => ValueHelper.IsNotBlank(v), "ValidationNewPassword", "Password"),
            new ValidationRule<Interfaces.IResetPasswordViewModel>((e, v) => ValueHelper.IsNotBlank(v), "ValidationNewConfirmPassword", "ConfirmPassword")
        ];

        static NewResetPasswordViewModelFormat: ValidationRule<Interfaces.IResetPasswordViewModel>[] =
        [
            new ValidationRule<Interfaces.IResetPasswordViewModel>((e, v) => !ValueHelper.IsNotBlank(v) || ValueHelper.IsResetPasswordValid(e), "ValidationPasswordSameConfirmPassword", "ConfirmPassword")
        ];

        static UserInformationCompanyBoard: ValidationRule<IBackOffices.ICompanyBoard>[] =
        [
            new ValidationRule<IBackOffices.ICompanyBoard>((e, v) => ValueHelper.IsNotBlank(v), "ValidationEmail", "Email"),
            new ValidationRule<IBackOffices.ICompanyBoard>((e, v) => !ValueHelper.IsNotBlank(v) || ValueHelper.IsEmailValid(v), "ValidationEmailFormat", "Email"),
            new ValidationRule<IBackOffices.ICompanyBoard>((e, v) => !ValueHelper.IsNotBlank(v) || ValueHelper.IsPhoneNumberValid(v), "ValidationPhoneFormat", "Phone"),
            new ValidationRule<IBackOffices.ICompanyBoard>((e, v) => !ValueHelper.IsNotBlank(v) || (v.replace("-", "").length == 10 && ValueHelper.IsPhoneNumber(v.replace("-", ""))), "ValidationCompanyContactMobileFormat", "Mobile")
        ];

        static UserInformationCompanyAdministrator: ValidationRule<IBackOffices.ICompanyAdministrator>[] =
        [
            new ValidationRule<IBackOffices.ICompanyAdministrator>((e, v) => ValueHelper.IsNotBlank(v), "ValidationEmail", "Email"),
            new ValidationRule<IBackOffices.ICompanyAdministrator>((e, v) => !ValueHelper.IsNotBlank(v) || ValueHelper.IsEmailValid(v), "ValidationEmailFormat", "Email"),
            new ValidationRule<IBackOffices.ICompanyAdministrator>((e, v) => !ValueHelper.IsNotBlank(v) || ValueHelper.IsPhoneNumberValid(v), "ValidationPhoneFormat", "Phone"),
            new ValidationRule<IBackOffices.ICompanyAdministrator>((e, v) => !ValueHelper.IsNotBlank(v) || (v.replace("-", "").length == 10 && ValueHelper.IsPhoneNumber(v.replace("-", ""))), "ValidationCompanyContactMobileFormat", "Mobile")
        ];

        static NewRegisterQuestionAndAnswer: ValidationRule<Interfaces.IExternalIdentityUserQuestionAnswer>[] =
        [
            new ValidationRule<Interfaces.IExternalIdentityUserQuestionAnswer>((e, v) => ValueHelper.IsNotBlank(v) && ValueHelper.IsNumeric(v), "ValidationSuitabilityTestQuestionTitle", "QuestionId"),
            new ValidationRule<Interfaces.IExternalIdentityUserQuestionAnswer>((e, v) => ValueHelper.IsNotBlank(v), "ValidationSuitabilityTestAnswerTitle", "Answer")
        ];

        static UserInformationMember: ValidationRule<IBackOffices.IMember>[] =
        [
            new ValidationRule<IBackOffices.IMember>((e, v) => ValueHelper.IsNotBlank(v), "ValidationEmail", "Email"),
            new ValidationRule<IBackOffices.IMember>((e, v) => !ValueHelper.IsNotBlank(v) || ValueHelper.IsEmailValid(v), "ValidationEmailFormat", "Email"),
            new ValidationRule<IBackOffices.IMember>((e, v) => !ValueHelper.IsNotBlank(v) || ValueHelper.IsPhoneNumberValid(v), "ValidationPhoneFormat", "Phone"),
            new ValidationRule<IBackOffices.IMember>((e, v) => !ValueHelper.IsNotBlank(v) || (v.replace("-", "").length == 10 && ValueHelper.IsPhoneNumber(v.replace("-", ""))), "ValidationCompanyContactMobileFormat", "Mobile")
        ];

        static UserInformationQuestionAndAnswer: ValidationRule<Interfaces.IExternalIdentityUserQuestionAnswer>[] =
        [
            new ValidationRule<Interfaces.IExternalIdentityUserQuestionAnswer>((e, v) => ValueHelper.IsNotBlank(v), "ValidationSuitabilityTestAnswerTitle", "Answer")
        ];

        static UserInformationSaveNewPassword: ValidationRule<Interfaces.IResetPasswordViewModel>[] =
        [
            new ValidationRule<Interfaces.IResetPasswordViewModel>((e, v) => !ValueHelper.IsNotBlank(v) || ValueHelper.IsPasswordValidFrontEndUser(e, v), "ValidationCurrentPasswordFormat", "OldPassword"),
            new ValidationRule<Interfaces.IResetPasswordViewModel>((e, v) => !ValueHelper.IsNotBlank(v) || ValueHelper.IsPasswordValidFrontEndUser(e, v), "ValidationNewPasswordFormat", "Password"),
            new ValidationRule<Interfaces.IResetPasswordViewModel>((e, v) => !ValueHelper.IsNotBlank(v) || ValueHelper.IsPasswordValidFrontEndUser(e, v), "ValidationNewConfirmPasswordFormat", "ConfirmPassword"),
            new ValidationRule<Interfaces.IResetPasswordViewModel>((e, v) => !ValueHelper.IsNotBlank(v) || ValueHelper.IsResetPasswordValid(e), "ValidationPasswordSameConfirmPassword", "ConfirmPassword")
        ];
    }

    export class ValueHelper
    {
        static IsNotMoreThanCurrentDate(value: any): boolean
        {
            return new Date(value) < new Date();
        }

        static IsNotBlank(value: any): boolean
        {
            return typeof value != "undefined" && (value != "" || value.length != 0) && value != null;
        }

        static IsNotZero(value: any)
        {
            return typeof value != "undefined" && value !== null && value != 0;
        }

        static IsNotBlankObject(value: any): boolean
        {
            return typeof value != "undefined" && value != null;
        }

        static IsNotOverHundred(value: any): boolean
        {
            return typeof value != "undefined" && value != null && value <= 100;
        }

        static IsAny(value: any[]): boolean
        {
            return value.length > 0;
        }

        static IsEmailValid(value: any): boolean
        {
            return new RegExp("^[A-Za-z0-9._%+-]+@[A-Za-z0-9-]+[\.][A-Za-z]{2,}[\.]{0,1}[A-Za-z]{0,2}$").test(value);
        }

        static IsCodeNotValid(value: any): boolean
        {
            return new RegExp("^((?!(:|<|>|\/|\\\\|\\*|\\?|\\|)).)*$").test(value);
        }

        static IsMultiEmailValid(value: any): boolean
        {
            var emailformat = "[A-Za-z0-9._%+-]{0,}@[A-Za-z0-9-]+[\.][A-Za-z]{2,}[\.]{0,1}([A-Za-z]{0,2})";
            var regex = new RegExp('^' + emailformat + '(;\\n*' + emailformat + ')*;?$');
            return regex.test(value);
        }

        static IsUserNameValid(value: any): boolean
        {
            return (new RegExp("^([a-zA-Z0-9_]{1,}(\\.{1}?[a-zA-Z0-9_]*)?){4,50}$").test(value));
        }

        static IsFullNameValid(value: any): boolean
        {
            return (new RegExp("^([a-zA-Z0-9ก-๙ _]{1,}(\\.{1}?[a-zA-Z0-9_]*)?){0,50}$").test(value));
        }

        static IsTaxpayerValid(value: any): boolean
        {
            return (value.length == 17 && new RegExp("^([0-9-_]{1,}(\\.{1}?[0-9_]*)?){0,50}$").test(value));
        }

        static IsTaxNumberValid(value: any): boolean
        {
            return (value.length == 13 && new RegExp("^([0-9-_]{1,}(\\.{1}?[0-9_]*)?){0,50}$").test(value));
        }

        static IsNumeric(value: any)
        {
            return !isNaN(value - 0) && value !== null && value !== "" && value !== false;
        }

        static IsPhoneNumber(value: any): boolean
        {
            // allow only digit and white spaces
            return (new RegExp("^[+]+[\\d\\s]+$").test(value)) || (new RegExp("^[\\d\\s]+$").test(value));
        }

        static IsPhoneNumberValid(value: any): boolean
        {
            // allow only digit , white spaces , sharp and comma
            return (new RegExp("^(?=[0-9])[- # ,()0-9]+$").test(value));
        }

        static IsControlFormat(value: any): boolean
        {
            return new RegExp("n+").test(value);
        }

        static IsPasswordValid(value: any): boolean
        {
            var regex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&]{8,}$/;
            return regex.test(value);
        }

        static IsPasswordValidFrontEndUser(entity: any, value: any): boolean
        {
            var minNumber = entity.MinNumber;
            var minSpeacial = entity.MinSpecialCharacter;
            var minLowerCase = entity.MinLowercase;
            var minUpperCase = entity.MinUppercase;
            var minLength = entity.MinPasswordLength;

            if (value.length >= minLength)
            {
                //[/,:<>!~@#$%^&*()+={};?()\"\'|!\\[]{" + minSpecialCharacter[0].Value + ",}
                // Accepted special charactors: !@#$ %^&*()_-+=\|[]{}:; "'/?.,<>~
                var resultMinSpeacial = new RegExp("[/,:<>!~@#$%^&*()+={};?()\"\'|!\\[]{" + minSpeacial + ",}").test(value);
                var resultMinLowerCase = new RegExp("[a-z]{" + minLowerCase + ",}").test(value);
                var resultMinNumber = new RegExp("[0-9]{" + minNumber + ",}").test(value);
                var resultMinUpperCase = new RegExp("[A-Z]{" + minUpperCase + ",}").test(value);
                return resultMinSpeacial && resultMinLowerCase && resultMinNumber && resultMinUpperCase;
            }
            else
            {
                return false;
            }
        }

        static IsChangePasswordBindingModelSamePasswordValid(entity: Interfaces.IChangePasswordBindingModel): boolean
        {
            return entity.NewPassword == entity.ConfirmPassword;
        }

        static IsResetPasswordValid(entity: Interfaces.IResetPasswordViewModel): boolean
        {
            return entity.Password == entity.ConfirmPassword;
        }

        //static IsIdentityUsersSamePasswordValid(entity: IBackOffices.IExternalIdentityUsers): boolean
        //{
        //    return entity.PasswordHash == entity.ConfirmPassword;
        //}

        //static IsEmailSameEmailConfirmValid(entity: Interfaces.IExternalIdentityUserRegister): boolean
        //{
        //    return entity.Email == entity.EmailConfirm;
        //}

        static IsStartEndDateValid(value: any, entity: any): boolean
        {
            return entity.StartDate <= entity.EndDate;
        }

        static IsGreaterThan(value: any): boolean
        {
            return typeof value != "undefined" && (value == ">" || value == ">=" || value == "=");
        }

        static IsLessThan(value: any): boolean
        {
            return typeof value != "undefined" && (value == "<" || value == "<=" || value == "=");
        }

        static IsValueGreater(value: any, value2: any)
        {
            return value > value2;
        }

        static IsValueGreaterThan(value: any, value2: any): boolean
        {
            if (this.IsNotBlank(value) && this.IsNotBlank(value2))
            {
                value = value.toString().split(',').join('');
                value2 = value2.toString().split(',').join('');
                return Number(value) >= Number(value2);
            }
            return false;
        }

        static IsTaxPayerNo(value: any): boolean
        {
            return typeof value != "undefined" && value != null && value != "" && value.length == 16 && new RegExp("(^\d{1}-?\d{4}-?\d{5}-?\d{2}-?\d{1}$|^X-XXXX-XXXXX-XX-X$)").test(value);
        }
    }
}

module Interfaces
{
    export interface ILoginData
    {
        Validator?: Validation.Validator<ILoginData>;
    }

    export interface IExternalIdentityRoles
    {
        Validator?: Validation.Validator<IExternalIdentityRoles>;
    }

    export interface IExternalIdentityUserRoles
    {
        Validator?: Validation.Validator<IExternalIdentityUserRoles>;
    }

    export interface IExternalMenu
    {
        Validator?: Validation.Validator<IExternalMenu>;
    }

    export interface IForgotPasswordViewModel
    {
        Validator?: Validation.Validator<IForgotPasswordViewModel>;
    }

    export interface IResetPasswordViewModel
    {
        Validator?: Validation.Validator<IResetPasswordViewModel>;
    }

    export interface IExternalIdentityUserRegister
    {
        Validator?: Validation.Validator<IExternalIdentityUserRegister>;
    }

    export interface IExternalIdentityUserQuestionAnswer
    {
        Validator?: Validation.Validator<Interfaces.IExternalIdentityUserQuestionAnswer>;
    }

}

module IBackOffices
{
    export interface ICompanyBoard
    {
        Validator?: Validation.Validator<IBackOffices.ICompanyBoard>;
    }

    export interface ICompanyAdministrator
    {
        Validator?: Validation.Validator<IBackOffices.ICompanyAdministrator>;
    }

    export interface IMember
    {
        Validator?: Validation.Validator<IBackOffices.IMember>;
    }

    export interface IExternalIdentityUsers
    {
        Validator?: Validation.Validator<IExternalIdentityUsers>;
    }

    export interface IExternalIdentityUserQuestionAnswer
    {
        Validator?: Validation.Validator<Interfaces.IExternalIdentityUserQuestionAnswer>;
    }
}
