//** Modify By Nayfido 15.12.2558 **//
// สามารถ compare กับฟิลที่ต้องการ และแสดงการแจ้งเตือนทั้ง 2 ฟิล
// ป้องกันการแจ้งเตือนซ้ำใน alert
var Validation;
(function (Validation) {
    var ValidationRule = (function () {
        function ValidationRule(validationExpression, message, field, isShow, errorOrder) {
            if (isShow === void 0) { isShow = true; }
            if (errorOrder === void 0) { errorOrder = 0; }
            this.ValidationExpression = validationExpression;
            this.ValidationMessage = message;
            if (typeof field !== "undefined")
                this.ValidationField = field;
            if (typeof isShow !== "undefined")
                this.IsShow = isShow;
            if (typeof errorOrder !== "undefined" && errorOrder != 0)
                this.ErrorOrder = errorOrder;
        }
        ValidationRule.prototype.ValidationExpression = function (object, value) { return false; };
        return ValidationRule;
    }());
    Validation.ValidationRule = ValidationRule;
    var Validator = (function () {
        function Validator() {
            this.ValidationErrors = [];
            this.bbl = Math.random();
            this.lock = false;
        }
        Validator.prototype.CurrentError = function (field) {
            var fieldErrors = Util.AngularLinq.Where(this.ValidationErrors, function (e) { return field === '' || e.Field === field; });
            var errorCount = fieldErrors.length;
            return errorCount === 0 ? "" : fieldErrors[0].Error;
        };
        Validator.prototype.Validate = function (entity) {
            if (!this.ValidationRules)
                return true;
            if (!this.lock) {
                this.lock = true;
                this.ValidationErrors.length = 0;
                // check all validation rules
                for (var i = 0; i < this.ValidationRules.length; i++) {
                    var r = this.ValidationRules[i];
                    //ถ้า !isShow คือไม่ต้องการให้แสดงใน validate alert
                    if (!r.IsShow) {
                        continue;
                    }
                    var listValue = r.ValidationField.split('.');
                    var value;
                    if (listValue.length == 1) {
                        if (angular.isUndefined(entity[listValue[0]]))
                            value = undefined;
                        else
                            value = entity[listValue[0]];
                    }
                    else if (listValue.length == 2) {
                        value = entity[listValue[0]] && entity[listValue[0]][listValue[1]] ? entity[listValue[0]][listValue[1]] : undefined;
                    }
                    else if (listValue.length == 3) {
                        value = entity[listValue[0]] && entity[listValue[0]][listValue[1]] && entity[listValue[0]][listValue[1]][listValue[2]] ? entity[listValue[0]][listValue[1]][listValue[2]] : undefined;
                    }
                    else if (listValue.length == 4) {
                        value = entity[listValue[0]] && entity[listValue[0]][listValue[1]] && entity[listValue[0]][listValue[1]][listValue[2]] && entity[listValue[0]][listValue[1]][listValue[2]][listValue[3]] ? entity[listValue[0]][listValue[1]][listValue[2]][listValue[3]] : undefined;
                    }
                    Validator.ValidateRule(entity, value, r, this.ValidationErrors);
                }
                this.lock = false;
                return (this.ValidationErrors.length == 0);
            }
            return true;
        };
        Validator.prototype.ValidateField = function (entity, value, field) {
            var _this = this;
            if (!this.lock) {
                this.lock = true;
                this.ValidationErrors.length = 0;
                // check all validation rules that match field name
                Util.AngularLinq.ForEach(Util.AngularLinq.Where(this.ValidationRules, function (r) { return r.ValidationField == field; }), function (r) { Validator.ValidateRule(entity, value, r, _this.ValidationErrors); });
                this.lock = false;
                return (this.ValidationErrors.length == 0);
            }
            return true;
        };
        Validator.ValidateRule = function (entity, value, rule, errors) {
            if (!rule.ValidationExpression(entity, value)) {
                errors.push({ Error: rule.ValidationMessage, Field: rule.ValidationField, ErrorOrder: rule.ErrorOrder });
                return false;
            }
            return true;
        };
        // factory method
        Validator.For = function (rules) {
            var val = new Validator();
            val.ValidationRules = rules;
            return val;
        };
        return Validator;
    }());
    Validation.Validator = Validator;
    var DefaultValidationRules = (function () {
        function DefaultValidationRules() {
        }
        DefaultValidationRules.LoginData = [
            new ValidationRule(function (e, v) { return ValueHelper.IsNotBlank(v); }, "ValidationFundTaxNo", "userName"),
            new ValidationRule(function (e, v) { return ValueHelper.IsTaxNumberValid(v); }, "ValidationLoginUsernameFormat", "userName"),
            new ValidationRule(function (e, v) { return ValueHelper.IsNotBlank(v); }, "ValidationPassword", "password")
        ];
        DefaultValidationRules.LoginDataTax = [
            new ValidationRule(function (e, v) { return ValueHelper.IsTaxNumberValid(v); }, "ValidationLoginUsernameFormat", "userName")
        ];
        DefaultValidationRules.LoginDataUnlockUser = [
            new ValidationRule(function (e, v) { return ValueHelper.IsNotBlank(v); }, "ValidationUserName", "userName"),
            new ValidationRule(function (e, v) { return ValueHelper.IsNotBlank(v); }, "ValidationEmail", "email")
        ];
        DefaultValidationRules.LoginDataUnlockUserFormat = [
            new ValidationRule(function (e, v) { return ValueHelper.IsEmailValid(v); }, "ValidationEmailFormat", "email"),
            new ValidationRule(function (e, v) { return ValueHelper.IsTaxNumberValid(v); }, "ValidationLoginUsernameFormat", "userName")
        ];
        DefaultValidationRules.ExternalIdentityUsers = [
            new ValidationRule(function (e, v) { return ValueHelper.IsNotBlank(v); }, "ValidationUserName", "UserName"),
            new ValidationRule(function (e, v) { return !ValueHelper.IsNotBlank(v) || ValueHelper.IsUserNameValid(v); }, "ValidationUserNameReg", "UserName"),
            new ValidationRule(function (e, v) { return ValueHelper.IsNotBlank(v); }, "ValidationEmail", "Email"),
            new ValidationRule(function (e, v) { return !ValueHelper.IsNotBlank(v) || ValueHelper.IsEmailValid(v); }, "ValidationEmailFormat", "Email"),
            new ValidationRule(function (e, v) { return ValueHelper.IsNotBlank(v) ? ValueHelper.IsPasswordValid(v) : true; }, "ValidationPasswordFormat", "PasswordHash"),
        ];
        DefaultValidationRules.NewExternalIdentityUsers = [
            new ValidationRule(function (e, v) { return ValueHelper.IsNotBlank(v); }, "ValidationUserName", "UserName"),
            new ValidationRule(function (e, v) { return ValueHelper.IsNotBlank(v); }, "ValidationEmail", "Email"),
            new ValidationRule(function (e, v) { return !ValueHelper.IsNotBlank(v) || ValueHelper.IsEmailValid(v); }, "ValidationEmailFormat", "Email"),
            new ValidationRule(function (e, v) { return ValueHelper.IsNotBlank(v); }, "ValidationPasswordHash", "PasswordHash"),
            new ValidationRule(function (e, v) { return ValueHelper.IsNotBlank(v); }, "ValidationConfirmPassword", "ConfirmPassword"),
            new ValidationRule(function (e, v) { return !ValueHelper.IsNotBlank(v) || ValueHelper.IsPasswordValid(v); }, "ValidationPasswordFormat", "PasswordHash"),
            new ValidationRule(function (e, v) { return !ValueHelper.IsNotBlank(v) || ValueHelper.IsPasswordValid(v); }, "ValidationConfirmPasswordFormat", "ConfirmPassword")
        ];
        DefaultValidationRules.NewExternalIdentityUsersRegisterSubmit = [
            new ValidationRule(function (e, v) { return ValueHelper.IsNotBlank(v); }, "ValidationRegisterUserRequireFieldRefCode", "RefCode") //ValidateRequestUserRequireField
        ];
        DefaultValidationRules.NewExternalIdentityUsersSubmit = [
            new ValidationRule(function (e, v) { return ValueHelper.IsNotBlank(v); }, "ValidationRequestUserRequireFieldTaxpayerIdentificationNumber", "UserName"),
            new ValidationRule(function (e, v) { return !ValueHelper.IsNotBlank(v) || ValueHelper.IsTaxpayerValid(v); }, "ValidationRequestUserIDCardFormat", "UserName"),
            new ValidationRule(function (e, v) { return ValueHelper.IsNotBlank(v); }, "ValidationPassword", "PasswordHash"),
            new ValidationRule(function (e, v) { return !ValueHelper.IsNotBlank(v) || ValueHelper.IsPasswordValidFrontEndUser(e, v); }, "ValidationPasswordFormat", "PasswordHash"),
            new ValidationRule(function (e, v) { return ValueHelper.IsNotBlank(v); }, "ValidationConfirmPassword", "ConfirmPassword")
        ];
        DefaultValidationRules.NewExternalIdentityUserRegister = [
            new ValidationRule(function (e, v) { return ValueHelper.IsNotBlank(v); }, "ValidationRequestUserRequireFieldFirstname", "Firstname"),
            new ValidationRule(function (e, v) { return ValueHelper.IsNotBlank(v); }, "ValidationRequestUserRequireFieldLastname", "Lastname"),
            new ValidationRule(function (e, v) { return ValueHelper.IsNotBlank(v); }, "ValidationRequestUserRequireFieldTaxpayerIdentificationNumber", "TaxpayerIdentificationNumber"),
            new ValidationRule(function (e, v) { return !ValueHelper.IsNotBlank(v) || ValueHelper.IsTaxpayerValid(v); }, "ValidationRequestUserIDCardFormat", "TaxpayerIdentificationNumber"),
            new ValidationRule(function (e, v) { return ValueHelper.IsNotBlank(v); }, "ValidationRequestUserRequireFieldEmail", "Email"),
            new ValidationRule(function (e, v) { return ValueHelper.IsNotBlank(v); }, "ValidationRequestUserRequireFieldEmailConfirm", "EmailConfirm"),
            new ValidationRule(function (e, v) { return !ValueHelper.IsNotBlank(v) || ValueHelper.IsEmailValid(v); }, "ValidationRequestUserEmailFormat", "Email"),
            new ValidationRule(function (e, v) { return !ValueHelper.IsNotBlank(v) || ValueHelper.IsEmailValid(v); }, "ValidationRequestUserEmailFormat", "EmailConfirm")
        ];
        DefaultValidationRules.NewForgotPasswordViewModel = [
            new ValidationRule(function (e, v) { return ValueHelper.IsNotBlank(v); }, "ValidationEmail", "Email"),
            new ValidationRule(function (e, v) { return ValueHelper.IsNotBlank(v); }, "ValidationTaxpayerIdentificationNumber", "UserName")
        ];
        DefaultValidationRules.NewForgotPasswordEmailFormat = [
            new ValidationRule(function (e, v) { return !ValueHelper.IsNotBlank(v) || ValueHelper.IsEmailValid(v); }, "ValidationEmailFormat", "Email")
        ];
        DefaultValidationRules.NewResetPasswordViewModel = [
            new ValidationRule(function (e, v) { return ValueHelper.IsNotBlank(v); }, "ValidationNewPassword", "Password"),
            new ValidationRule(function (e, v) { return ValueHelper.IsNotBlank(v); }, "ValidationNewConfirmPassword", "ConfirmPassword")
        ];
        DefaultValidationRules.NewResetPasswordViewModelFormat = [
            new ValidationRule(function (e, v) { return !ValueHelper.IsNotBlank(v) || ValueHelper.IsResetPasswordValid(e); }, "ValidationPasswordSameConfirmPassword", "ConfirmPassword")
        ];
        DefaultValidationRules.UserInformationCompanyBoard = [
            new ValidationRule(function (e, v) { return ValueHelper.IsNotBlank(v); }, "ValidationEmail", "Email"),
            new ValidationRule(function (e, v) { return !ValueHelper.IsNotBlank(v) || ValueHelper.IsEmailValid(v); }, "ValidationEmailFormat", "Email"),
            new ValidationRule(function (e, v) { return !ValueHelper.IsNotBlank(v) || ValueHelper.IsPhoneNumberValid(v); }, "ValidationPhoneFormat", "Phone"),
            new ValidationRule(function (e, v) { return !ValueHelper.IsNotBlank(v) || (v.replace("-", "").length == 10 && ValueHelper.IsPhoneNumber(v.replace("-", ""))); }, "ValidationCompanyContactMobileFormat", "Mobile")
        ];
        DefaultValidationRules.UserInformationCompanyAdministrator = [
            new ValidationRule(function (e, v) { return ValueHelper.IsNotBlank(v); }, "ValidationEmail", "Email"),
            new ValidationRule(function (e, v) { return !ValueHelper.IsNotBlank(v) || ValueHelper.IsEmailValid(v); }, "ValidationEmailFormat", "Email"),
            new ValidationRule(function (e, v) { return !ValueHelper.IsNotBlank(v) || ValueHelper.IsPhoneNumberValid(v); }, "ValidationPhoneFormat", "Phone"),
            new ValidationRule(function (e, v) { return !ValueHelper.IsNotBlank(v) || (v.replace("-", "").length == 10 && ValueHelper.IsPhoneNumber(v.replace("-", ""))); }, "ValidationCompanyContactMobileFormat", "Mobile")
        ];
        DefaultValidationRules.NewRegisterQuestionAndAnswer = [
            new ValidationRule(function (e, v) { return ValueHelper.IsNotBlank(v) && ValueHelper.IsNumeric(v); }, "ValidationSuitabilityTestQuestionTitle", "QuestionId"),
            new ValidationRule(function (e, v) { return ValueHelper.IsNotBlank(v); }, "ValidationSuitabilityTestAnswerTitle", "Answer")
        ];
        DefaultValidationRules.UserInformationMember = [
            new ValidationRule(function (e, v) { return ValueHelper.IsNotBlank(v); }, "ValidationEmail", "Email"),
            new ValidationRule(function (e, v) { return !ValueHelper.IsNotBlank(v) || ValueHelper.IsEmailValid(v); }, "ValidationEmailFormat", "Email"),
            new ValidationRule(function (e, v) { return !ValueHelper.IsNotBlank(v) || ValueHelper.IsPhoneNumberValid(v); }, "ValidationPhoneFormat", "Phone"),
            new ValidationRule(function (e, v) { return !ValueHelper.IsNotBlank(v) || (v.replace("-", "").length == 10 && ValueHelper.IsPhoneNumber(v.replace("-", ""))); }, "ValidationCompanyContactMobileFormat", "Mobile")
        ];
        DefaultValidationRules.UserInformationQuestionAndAnswer = [
            new ValidationRule(function (e, v) { return ValueHelper.IsNotBlank(v); }, "ValidationSuitabilityTestAnswerTitle", "Answer")
        ];
        DefaultValidationRules.UserInformationSaveNewPassword = [
            new ValidationRule(function (e, v) { return !ValueHelper.IsNotBlank(v) || ValueHelper.IsPasswordValidFrontEndUser(e, v); }, "ValidationCurrentPasswordFormat", "OldPassword"),
            new ValidationRule(function (e, v) { return !ValueHelper.IsNotBlank(v) || ValueHelper.IsPasswordValidFrontEndUser(e, v); }, "ValidationNewPasswordFormat", "Password"),
            new ValidationRule(function (e, v) { return !ValueHelper.IsNotBlank(v) || ValueHelper.IsPasswordValidFrontEndUser(e, v); }, "ValidationNewConfirmPasswordFormat", "ConfirmPassword"),
            new ValidationRule(function (e, v) { return !ValueHelper.IsNotBlank(v) || ValueHelper.IsResetPasswordValid(e); }, "ValidationPasswordSameConfirmPassword", "ConfirmPassword")
        ];
        return DefaultValidationRules;
    }());
    Validation.DefaultValidationRules = DefaultValidationRules;
    var ValueHelper = (function () {
        function ValueHelper() {
        }
        ValueHelper.IsNotMoreThanCurrentDate = function (value) {
            return new Date(value) < new Date();
        };
        ValueHelper.IsNotBlank = function (value) {
            return typeof value != "undefined" && (value != "" || value.length != 0) && value != null;
        };
        ValueHelper.IsNotZero = function (value) {
            return typeof value != "undefined" && value !== null && value != 0;
        };
        ValueHelper.IsNotBlankObject = function (value) {
            return typeof value != "undefined" && value != null;
        };
        ValueHelper.IsNotOverHundred = function (value) {
            return typeof value != "undefined" && value != null && value <= 100;
        };
        ValueHelper.IsAny = function (value) {
            return value.length > 0;
        };
        ValueHelper.IsEmailValid = function (value) {
            return new RegExp("^[A-Za-z0-9._%+-]+@[A-Za-z0-9-]+[\.][A-Za-z]{2,}[\.]{0,1}[A-Za-z]{0,2}$").test(value);
        };
        ValueHelper.IsCodeNotValid = function (value) {
            return new RegExp("^((?!(:|<|>|\/|\\\\|\\*|\\?|\\|)).)*$").test(value);
        };
        ValueHelper.IsMultiEmailValid = function (value) {
            var emailformat = "[A-Za-z0-9._%+-]{0,}@[A-Za-z0-9-]+[\.][A-Za-z]{2,}[\.]{0,1}([A-Za-z]{0,2})";
            var regex = new RegExp('^' + emailformat + '(;\\n*' + emailformat + ')*;?$');
            return regex.test(value);
        };
        ValueHelper.IsUserNameValid = function (value) {
            return (new RegExp("^([a-zA-Z0-9_]{1,}(\\.{1}?[a-zA-Z0-9_]*)?){4,50}$").test(value));
        };
        ValueHelper.IsFullNameValid = function (value) {
            return (new RegExp("^([a-zA-Z0-9ก-๙ _]{1,}(\\.{1}?[a-zA-Z0-9_]*)?){0,50}$").test(value));
        };
        ValueHelper.IsTaxpayerValid = function (value) {
            return (value.length == 17 && new RegExp("^([0-9-_]{1,}(\\.{1}?[0-9_]*)?){0,50}$").test(value));
        };
        ValueHelper.IsTaxNumberValid = function (value) {
            return (value.length == 13 && new RegExp("^([0-9-_]{1,}(\\.{1}?[0-9_]*)?){0,50}$").test(value));
        };
        ValueHelper.IsNumeric = function (value) {
            return !isNaN(value - 0) && value !== null && value !== "" && value !== false;
        };
        ValueHelper.IsPhoneNumber = function (value) {
            // allow only digit and white spaces
            return (new RegExp("^[+]+[\\d\\s]+$").test(value)) || (new RegExp("^[\\d\\s]+$").test(value));
        };
        ValueHelper.IsPhoneNumberValid = function (value) {
            // allow only digit , white spaces , sharp and comma
            return (new RegExp("^(?=[0-9])[- # ,()0-9]+$").test(value));
        };
        ValueHelper.IsControlFormat = function (value) {
            return new RegExp("n+").test(value);
        };
        ValueHelper.IsPasswordValid = function (value) {
            var regex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&]{8,}$/;
            return regex.test(value);
        };
        ValueHelper.IsPasswordValidFrontEndUser = function (entity, value) {
            var minNumber = entity.MinNumber;
            var minSpeacial = entity.MinSpecialCharacter;
            var minLowerCase = entity.MinLowercase;
            var minUpperCase = entity.MinUppercase;
            var minLength = entity.MinPasswordLength;
            if (value.length >= minLength) {
                //[/,:<>!~@#$%^&*()+={};?()\"\'|!\\[]{" + minSpecialCharacter[0].Value + ",}
                // Accepted special charactors: !@#$ %^&*()_-+=\|[]{}:; "'/?.,<>~
                var resultMinSpeacial = new RegExp("[/,:<>!~@#$%^&*()+={};?()\"\'|!\\[]{" + minSpeacial + ",}").test(value);
                var resultMinLowerCase = new RegExp("[a-z]{" + minLowerCase + ",}").test(value);
                var resultMinNumber = new RegExp("[0-9]{" + minNumber + ",}").test(value);
                var resultMinUpperCase = new RegExp("[A-Z]{" + minUpperCase + ",}").test(value);
                return resultMinSpeacial && resultMinLowerCase && resultMinNumber && resultMinUpperCase;
            }
            else {
                return false;
            }
        };
        ValueHelper.IsChangePasswordBindingModelSamePasswordValid = function (entity) {
            return entity.NewPassword == entity.ConfirmPassword;
        };
        ValueHelper.IsResetPasswordValid = function (entity) {
            return entity.Password == entity.ConfirmPassword;
        };
        //static IsIdentityUsersSamePasswordValid(entity: IBackOffices.IExternalIdentityUsers): boolean
        //{
        //    return entity.PasswordHash == entity.ConfirmPassword;
        //}
        //static IsEmailSameEmailConfirmValid(entity: Interfaces.IExternalIdentityUserRegister): boolean
        //{
        //    return entity.Email == entity.EmailConfirm;
        //}
        ValueHelper.IsStartEndDateValid = function (value, entity) {
            return entity.StartDate <= entity.EndDate;
        };
        ValueHelper.IsGreaterThan = function (value) {
            return typeof value != "undefined" && (value == ">" || value == ">=" || value == "=");
        };
        ValueHelper.IsLessThan = function (value) {
            return typeof value != "undefined" && (value == "<" || value == "<=" || value == "=");
        };
        ValueHelper.IsValueGreater = function (value, value2) {
            return value > value2;
        };
        ValueHelper.IsValueGreaterThan = function (value, value2) {
            if (this.IsNotBlank(value) && this.IsNotBlank(value2)) {
                value = value.toString().split(',').join('');
                value2 = value2.toString().split(',').join('');
                return Number(value) >= Number(value2);
            }
            return false;
        };
        ValueHelper.IsTaxPayerNo = function (value) {
            return typeof value != "undefined" && value != null && value != "" && value.length == 16 && new RegExp("(^\d{1}-?\d{4}-?\d{5}-?\d{2}-?\d{1}$|^X-XXXX-XXXXX-XX-X$)").test(value);
        };
        return ValueHelper;
    }());
    Validation.ValueHelper = ValueHelper;
})(Validation || (Validation = {}));
//# sourceMappingURL=validation.js.map