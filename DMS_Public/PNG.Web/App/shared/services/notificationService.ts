﻿module Shared
{
    export class NotificationService
    {
        public static Factory($log: ng.ILogService)
        {
            return new NotificationService($log);
        }

        constructor(
            private $log: ng.ILogService)
        {
            toastr.options.positionClass = "toast-bottom-full-width";
        }

        public Info(message: string, title?: string, overrides?: ToastrOptions): JQuery
        {
            return toastr.info(message, title, overrides);
        }

        public Success(message: string, title?: string, overrides?: ToastrOptions): JQuery
        {
            var options: ToastrOptions = {
                tapToDismiss: false,
                closeButton: true,
                timeOut: 5000,
                extendedTimeOut: 0
            };
            if (!overrides) overrides = options;
            return toastr.success(message, title, overrides);
        }

        public Warning(message: string, title?: string, overrides?: ToastrOptions): JQuery
        {
            return toastr.warning(message, title, overrides);
        }

        public Error(message: string, title?: string, overrides?: ToastrOptions): JQuery
        {
            var options: ToastrOptions = {
                tapToDismiss: false,
                closeButton: true,
                timeOut: 0,
                extendedTimeOut: 0
            };
            if (!overrides) overrides = options;
            return toastr.error(message, title, overrides);
        }

        public Clear(toast?: JQuery): void
        {
            toastr.clear(toast);
        }
    }

    Main.App.Services.factory("notificationService", ["$log", NotificationService.Factory]);
}