var Shared;
(function (Shared) {
    var NotificationService = (function () {
        function NotificationService($log) {
            this.$log = $log;
            toastr.options.positionClass = "toast-bottom-full-width";
        }
        NotificationService.Factory = function ($log) {
            return new NotificationService($log);
        };
        NotificationService.prototype.Info = function (message, title, overrides) {
            return toastr.info(message, title, overrides);
        };
        NotificationService.prototype.Success = function (message, title, overrides) {
            var options = {
                tapToDismiss: false,
                closeButton: true,
                timeOut: 5000,
                extendedTimeOut: 0
            };
            if (!overrides)
                overrides = options;
            return toastr.success(message, title, overrides);
        };
        NotificationService.prototype.Warning = function (message, title, overrides) {
            return toastr.warning(message, title, overrides);
        };
        NotificationService.prototype.Error = function (message, title, overrides) {
            var options = {
                tapToDismiss: false,
                closeButton: true,
                timeOut: 0,
                extendedTimeOut: 0
            };
            if (!overrides)
                overrides = options;
            return toastr.error(message, title, overrides);
        };
        NotificationService.prototype.Clear = function (toast) {
            toastr.clear(toast);
        };
        return NotificationService;
    }());
    Shared.NotificationService = NotificationService;
    Main.App.Services.factory("notificationService", ["$log", NotificationService.Factory]);
})(Shared || (Shared = {}));
//# sourceMappingURL=notificationService.js.map