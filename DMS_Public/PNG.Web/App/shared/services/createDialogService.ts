﻿angular.module('createDialogService', []).factory('createDialog', ["$document", "$compile", "$rootScope", "$controller", "$timeout", "$cookies", "$location",
    function ($document, $compile, $rootScope, $controller, $timeout, $cookies, $location)
    {
        var defaults = {
            id: null,
            template: null,
            templateUrl: null,
            title: 'Default Title',
            backdrop: 'static',
            keyboard: true,
            success: { visible: true, label: 'OK', fn: null },
            cancel: { visible: true, label: 'Cancel', fn: null },
            controller: null, //just like route controller declaration
            backdropClass: "modal-backdrop",
            footerTemplate: null,
            modalClass: "modal",
            css: {
                'margin-top': '100px'
            },
            showFooter: true,
            dialogInformation: null,  //Html display above dialog title
            hideCloseBtn: false
        };

        var body = $document.find('body');

        return function Dialog(templateUrl/*optional*/, optionalClass, options, passedInLocals)
        {
            this.DilaogParams = {};

            // Handle arguments if optional template isn't provided.
            if (angular.isObject(templateUrl))
            {
                passedInLocals = options;
                options = templateUrl;
            } else
            {
                // check querystring 
                var cookieLanguage = $cookies.SelectedLanguageCode;

                if (typeof cookieLanguage === "undefined")
                {
                    // language default
                    cookieLanguage = "th-TH";
                }
                var idx: number = templateUrl.indexOf("?");
                if (idx > -1)
                {
                    templateUrl = templateUrl + "&lang=" + cookieLanguage;
                } else
                {
                    templateUrl = templateUrl + "?lang=" + cookieLanguage;
                }
                options.templateUrl = templateUrl;
            }

            options = angular.extend({}, defaults, options); //options defined in constructor
            var key;
            var idAttr = options.id ? ' id="' + options.id + '" ' : '';
            var defaultFooter = ' ';
            if (options.cancel.visible)
                defaultFooter = '<button class="btn-dialog-cancel" ng-click="$modalCancel();">{{$modalCancelLabel}}</button>';
            if (options.success.visible)
                defaultFooter += '<button class="btn-dialog-confirm" ng-click="$modalSuccess()">{{$modalSuccessLabel}}</button>';

         

            var footerTemplate = '';

            if (options.showFooter)
            {
                footerTemplate = '<div class="modal-footer">' +
                    (options.footerTemplate || defaultFooter) +
                    '</div>';
            }


            var modalBodyStyle = "";

            for (key in options.bodycss)
            {
                modalBodyStyle += key + ":" + options.bodycss[key] + ";";
            }

            var modalBody = (function ()
            {
                if (options.template)
                {
                    if (angular.isString(options.template))
                    {
                        // Simple string template
                        return '<div class="modal-body" style="' + modalBodyStyle + '">' + options.template + '</div>';
                    } else
                    {
                        // jQuery/JQlite wrapped object
                        return '<div class="modal-body" style="' + modalBodyStyle + '">' + options.template.html() + '</div>';
                    }
                } else
                {
                    // Template url
                    return '<div class="modal-body" style="' + modalBodyStyle + '" ng-include="\'' + options.templateUrl + '\'"></div>';
                }
            })();


            var modalContent;

            //We don't have the scope we're gonna use yet, so just get a compile function for modal
            if (options.dialogInformation)
            {
                if (angular.isString(options.dialogInformation))
                {

                    modalContent = <any>angular.element
                        (
                        '  <div class="modal-dialog">' +
                        '    <div class="modal-content">' +
                        '      <div class="modal-header">' +
                        '        <button type="button" class="close" ng-click="$modalCancel();">&times;</button>' +
                        options.dialogInformation +
                        '      <div class="row-section-gap"></div>' +
                        '      <span class="popup-title">{{$title}}</span>' +
                        '      </div>' +
                        modalBody +
                        footerTemplate +
                        '    </div>' +
                        '  </div>'
                        );
                }
            }
            else
            {
                var element = '  <div class="modal-dialog">' +
                    '    <div class="modal-content">' +
                    '      <div class="modal-header">';
                // hide close button
                if (!options.hideCloseBtn)
                {
                    element += '        <button type="button" class="close" ng-click="$modalCancel();">&times;</button>';
                }
                modalContent = <any>angular.element
                    (
                    element +
                    '        <span class="popup-title">{{$title}}</span>' +
                    '      </div>' +
                    modalBody +
                    footerTemplate +
                    '    </div>' +
                    '  </div>'
                    );
            }

            for (key in options.css)
            {
                modalContent.css(key, options.css[key]);
            }

            //add user specified class, if supplied
            if (optionalClass)
                modalContent.addClass(optionalClass);

            var modalEl = <any>angular.element(
                '<div class="' + options.modalClass + ' fade"' + idAttr + '   tabindex="-1" >' +
                '</div>');

            modalEl.append(modalContent);

            modalEl.on('hidden.bs.modal', function ()
            {
                body.unbind('keydown', handleEscPressed);
                modalEl.remove();
                $rootScope.$broadcast('dialogClosed');
            });

            $(document).on('show.bs.modal', '.modal', function (event)
            {
                var zIndex = 1040 + (10 * $('.modal:visible').length);
                $(this).css('z-index', zIndex);
                setTimeout(function ()
                {
                    $('.modal-backdrop').not('.modal-stack').css('z-index', zIndex - 1).addClass('modal-stack');
                }, 0);
            });

            var handleEscPressed = function (event)
            {
                // TODO: enable/disable keyboard for Esc key
                if (options.keyboard === true)
                {
                    if (event.keyCode === 27)
                    {
                        scope.$modalCancel();
                    }
                }
                else
                {
                    // TODO: prevent default behaviors in the browser
                    event.returnValue = false;
                    event.preventDefault();

                    // TODO: the event will no longer call any listeners on nodes that it travels through on its way to the target and back to the document
                    event.stopPropagation();
                }
            };

            var closeFn = function (obj)
            {
                $timeout(function ()
                {
                    modalEl.modal('hide');
                }, 1);
            };

            body.bind('keydown', handleEscPressed);

            var ctrl, locals,
                scope = options.scope || $rootScope.$new();

            scope.$title = options.title;
            scope.$modalClose = closeFn;
            scope.$modalCancel = function ()
            {
                if (options.cancel.fn)
                {
                    options.cancel.fn(this);
                }
                scope.$modalClose(this);
            };

            scope.$modalSuccess = function ()
            {
                if (options.success.fn)
                {
                    options.success.fn(this);
                }
                scope.$modalClose(this);
            };

            scope.$modalSuccessLabel = options.success.label;
            scope.$modalCancelLabel = options.cancel.label;

            if (options.controller)
            {
                locals = angular.extend({ $scope: scope }, passedInLocals);
                ctrl = $controller(options.controller, locals);
                // Yes, ngControllerController is not a typo
                modalEl.contents().data('$ngControllerController', ctrl);
            }

            $compile(modalEl)(scope);
            body.append(modalEl);

            modalEl.modal({
                show: true,
                backdrop: options.backdrop,
                keyboard: options.keyboard
            });

            $timeout(function ()
            {
                modalEl.addClass('in');
            }, 200);
        };
    }]);