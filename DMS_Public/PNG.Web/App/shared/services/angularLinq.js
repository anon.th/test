var Util;
(function (Util) {
    var AngularLinq = (function () {
        function AngularLinq() {
        }
        AngularLinq.JoinArrays = function (a1, a2, comparer) {
            var result = [];
            AngularLinq.ForEach(a1, function (t3) {
                var found = false;
                AngularLinq.ForEach(a2, function (t4) {
                    if (comparer(t3, t4)) {
                        result.push(t4);
                        found = true;
                    }
                });
                if (found == false) {
                    result.push(t3);
                }
            });
            return result;
        };
        AngularLinq.ForEach = function (array, expression) {
            if (array) {
                for (var i = 0; i < array.length; i++)
                    expression(array[i]);
            }
            return array;
        };
        AngularLinq.Where = function (array, expression) {
            var result = [];
            AngularLinq.ForEach(array, function (t) {
                if (expression(t))
                    result.push(t);
            });
            return result;
        };
        return AngularLinq;
    }());
    Util.AngularLinq = AngularLinq;
})(Util || (Util = {}));
//# sourceMappingURL=angularLinq.js.map