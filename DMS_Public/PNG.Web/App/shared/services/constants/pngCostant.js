var Shared;
(function (Shared) {
    var PvdConstant = (function () {
        function PvdConstant() {
        }
        PvdConstant.Empty = "";
        PvdConstant.Blank = " ";
        PvdConstant.JoinLine = ", ";
        PvdConstant.NewLine = "<br/>";
        // Column sorting and pagination
        PvdConstant.ColumnSorting = "[searchParameter.SortColumn,searchParameter.SortAscending]";
        PvdConstant.SelectedPageLink = "SelectedPageLink";
        PvdConstant.AddPage = "manage";
        PvdConstant.ManagePage = "manage";
        PvdConstant.IndexPage = "index";
        PvdConstant.ViewPage = "view";
        PvdConstant.DetailPage = "detail";
        PvdConstant.ResetPasswordPage = "resetpassword";
        PvdConstant.UnlockPage = "unlock";
        PvdConstant.KeyInPage = "/keyin/";
        PvdConstant.UploadPage = "/upload/";
        PvdConstant.DateFormatYYYYMMDD = "yyyy-MM-dd";
        PvdConstant.DateFormatDD = "dd";
        return PvdConstant;
    }());
    Shared.PvdConstant = PvdConstant;
    var SectionConditionType = (function () {
        function SectionConditionType() {
        }
        SectionConditionType.RegEx = "RegEx";
        return SectionConditionType;
    }());
    Shared.SectionConditionType = SectionConditionType;
    var SectionConditionOperator = (function () {
        function SectionConditionOperator() {
        }
        SectionConditionOperator.CellPattern = "CellPattern";
        return SectionConditionOperator;
    }());
    Shared.SectionConditionOperator = SectionConditionOperator;
    var ColumnIcon = (function () {
        function ColumnIcon() {
        }
        ColumnIcon.ArrowUp = "fa fa-caret-up";
        ColumnIcon.ArrowDown = "fa fa-caret-down";
        return ColumnIcon;
    }());
    Shared.ColumnIcon = ColumnIcon;
    var StatusList = (function () {
        function StatusList() {
        }
        StatusList.getStatus = function (statusValue) {
            if (statusValue == "A")
                return "ปกติ";
            else if (statusValue == "S")
                return "เพิ่ม/แก้ไข";
            else if (statusValue == "W")
                return "รออนุมัติ";
            else if (statusValue == "R")
                return "ไม่อนุมัติ";
            else if (statusValue == "I")
                return "ยกเลิก";
            else
                return "";
        };
        StatusList.Approve = "A";
        StatusList.Reject = "R";
        StatusList.Save = "S";
        StatusList.Waiting = "W";
        StatusList.Inactive = "I";
        return StatusList;
    }());
    Shared.StatusList = StatusList;
    var ListOfView = (function () {
        function ListOfView() {
        }
        ListOfView.CompanySingleSelected = "lovCompanyFrontEnd";
        ListOfView.CompanyMultipleSelected = "lovListCompanyFrontEnd";
        ListOfView.MemberSingleSelected = "lovMemberFrontEnd";
        ListOfView.MemberMultipleSelected = "lovListMemberFrontEnd";
        return ListOfView;
    }());
    Shared.ListOfView = ListOfView;
    var DialogPage = (function () {
        function DialogPage() {
        }
        DialogPage.LoginCompany = "dialogLoginCompany";
        DialogPage.MemberSuitabilityTest = "dialogMemberSuitabilityTest";
        return DialogPage;
    }());
    Shared.DialogPage = DialogPage;
    var ValueHelper = (function () {
        function ValueHelper() {
        }
        ValueHelper.CreateNumberRange = function (start, max) {
            var limits = [];
            for (var i = start; i <= max; ++i)
                limits.push(i.toString());
            return limits;
        };
        ValueHelper.MaxSeachLimit = 4;
        ValueHelper.RecordsPerPage = 20; // for Search Page
        ValueHelper.RecordsPerPageLov = 8; // for PopUp Page
        ValueHelper.RecordsPerPageDetail = 10; // for Detail Page
        return ValueHelper;
    }());
    Shared.ValueHelper = ValueHelper;
    var FileExtension = (function () {
        function FileExtension() {
        }
        FileExtension.Text = ".txt";
        FileExtension.Csv = ".csv";
        FileExtension.Excel = ".xlsx";
        FileExtension.JPG = ".jpg";
        FileExtension.PNG = ".png";
        FileExtension.Excel2003 = ".xls";
        return FileExtension;
    }());
    Shared.FileExtension = FileExtension;
    var TypeCode = (function () {
        function TypeCode() {
        }
        TypeCode.BusinessType = "BusinessType";
        TypeCode.CompanyType = "CompanyType";
        TypeCode.ResponsibilityType = "ResponsibilityType";
        TypeCode.EmploymentType = "EmploymentType";
        TypeCode.PayrollDateCondition = "PayrollDateCondition";
        TypeCode.ContributionType = "ContributionType";
        TypeCode.CompanyVestingType = "CompanyVestingType";
        TypeCode.CompanyUnvestType = "CompanyUnvestType";
        TypeCode.ResignReason = "ResignReason";
        TypeCode.CalculationType = "CalculationType";
        TypeCode.CompanyContributionSuspensionReasonType = "CompanyContributionSuspensionReasonType";
        return TypeCode;
    }());
    Shared.TypeCode = TypeCode;
    var Uploader = (function () {
        function Uploader() {
        }
        Uploader.UploaderFilters = "|jpg|png|jpeg|bmp|gif|";
        return Uploader;
    }());
    Shared.Uploader = Uploader;
    var Operator = (function () {
        function Operator() {
        }
        Operator.GreaterThanOrEqual = ">=";
        Operator.GreaterThan = ">";
        Operator.LessThanOrEqual = "<=";
        Operator.LessThan = "<";
        Operator.Equal = "=";
        return Operator;
    }());
    Shared.Operator = Operator;
    // Download format type
    var DownloadFormatType = (function () {
        function DownloadFormatType() {
        }
        DownloadFormatType.Excel = "E";
        DownloadFormatType.Pdf = "P";
        DownloadFormatType.Upload = "U";
        return DownloadFormatType;
    }());
    Shared.DownloadFormatType = DownloadFormatType;
    var MimeTypeDictionary = (function () {
        function MimeTypeDictionary() {
        }
        MimeTypeDictionary.Excel = "xls";
        MimeTypeDictionary.Pdf = "pdf";
        MimeTypeDictionary.Upload = "upload";
        return MimeTypeDictionary;
    }());
    Shared.MimeTypeDictionary = MimeTypeDictionary;
    var FileStorageFileType = (function () {
        function FileStorageFileType() {
        }
        FileStorageFileType.Export = "Export";
        FileStorageFileType.Report = "Report";
        FileStorageFileType.Upload = "Upload";
        return FileStorageFileType;
    }());
    Shared.FileStorageFileType = FileStorageFileType;
    var ExternalIdentityUserLoginType = (function () {
        function ExternalIdentityUserLoginType() {
        }
        ExternalIdentityUserLoginType.UserIsNotActive = "1";
        ExternalIdentityUserLoginType.UserIsLockByQuestion = "2";
        ExternalIdentityUserLoginType.UserIsLockedByLogin = "3";
        ExternalIdentityUserLoginType.UserOrPassWordWasWrong = "4";
        ExternalIdentityUserLoginType.MemberIsNotActive = "5";
        ExternalIdentityUserLoginType.UserHasSingleCompany = "0";
        ExternalIdentityUserLoginType.UserHasManyCompany = "00";
        ExternalIdentityUserLoginType.NullValueFromServiceBroker = "6";
        ExternalIdentityUserLoginType.OtherError = "7";
        return ExternalIdentityUserLoginType;
    }());
    Shared.ExternalIdentityUserLoginType = ExternalIdentityUserLoginType;
    var CheckAnswerType = (function () {
        function CheckAnswerType() {
        }
        CheckAnswerType.AllFailed = "0";
        CheckAnswerType.AllCorrect = "1";
        CheckAnswerType.UserIsLock = "2";
        return CheckAnswerType;
    }());
    Shared.CheckAnswerType = CheckAnswerType;
    var RouteNames = (function () {
        function RouteNames() {
        }
        RouteNames.WelCome = "/welcome";
        RouteNames.EmailAccess = "/emailusernameaccessright";
        RouteNames.Reset = "/resetpassword";
        RouteNames.Unlock = "/unlockuser2";
        RouteNames.Request = "/request";
        RouteNames.ShowMessage = "/showmessage/index";
        RouteNames.RegisterUnlock = "/register/unlock";
        RouteNames.Login = "/login";
        RouteNames.Register = "/register";
        RouteNames.Suitabilitytestreminder = "/member/suitabilitytestreminder";
        RouteNames.Membersuitabilitytest = "/member/membersuitabilitytest";
        RouteNames.Logout = "/logout";
        RouteNames.Home = "/home";
        RouteNames.TrackAndTrace = "/trackandtrace";
        RouteNames.Error = "/error";
        return RouteNames;
    }());
    Shared.RouteNames = RouteNames;
    var RoleName = (function () {
        function RoleName() {
        }
        RoleName.Member = "Member";
        RoleName.Admin = "Admin";
        return RoleName;
    }());
    Shared.RoleName = RoleName;
})(Shared || (Shared = {}));
//# sourceMappingURL=pngCostant.js.map