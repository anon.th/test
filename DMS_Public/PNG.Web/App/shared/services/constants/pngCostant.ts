﻿module Shared
{
    export class PvdConstant
    {
        public static Empty: string = "";
        public static Blank: string = " ";
        public static JoinLine: string = ", ";
        public static NewLine: string = "<br/>";

        // Column sorting and pagination
        public static ColumnSorting: string = "[searchParameter.SortColumn,searchParameter.SortAscending]";
        public static SelectedPageLink: string = "SelectedPageLink";
        public static AddPage: string = "manage";
        public static ManagePage: string = "manage";
        public static IndexPage: string = "index";
        public static ViewPage: string = "view";
        public static DetailPage: string = "detail";
        public static ResetPasswordPage: string = "resetpassword";
        public static UnlockPage: string = "unlock";

        public static KeyInPage: string = "/keyin/";
        public static UploadPage: string = "/upload/";

        public static DateFormatYYYYMMDD: string = "yyyy-MM-dd";
        public static DateFormatDD: string = "dd";
    }

    export class SectionConditionType
    {
        public static RegEx: string = "RegEx";
    }

    export class SectionConditionOperator
    {
        public static CellPattern: string = "CellPattern";
    }

    export class ColumnIcon
    {
        public static ArrowUp: string = "fa fa-caret-up";
        public static ArrowDown: string = "fa fa-caret-down"
    }

    export class StatusList
    {
        public static Approve: string = "A";
        public static Reject: string = "R";
        public static Save: string = "S";
        public static Waiting: string = "W";
        public static Inactive: string = "I";

        public static getStatus(statusValue: string): string
        {
            if (statusValue == "A") return "ปกติ";
            else if (statusValue == "S") return "เพิ่ม/แก้ไข";
            else if (statusValue == "W") return "รออนุมัติ";
            else if (statusValue == "R") return "ไม่อนุมัติ";
            else if (statusValue == "I") return "ยกเลิก";
            else return "";
        }
    }

    export class ListOfView
    {
        public static CompanySingleSelected: string = "lovCompanyFrontEnd";
        public static CompanyMultipleSelected: string = "lovListCompanyFrontEnd";

        public static MemberSingleSelected: string = "lovMemberFrontEnd";
        public static MemberMultipleSelected: string = "lovListMemberFrontEnd";
    }

    export class DialogPage
    {
        public static LoginCompany: string = "dialogLoginCompany";
        public static MemberSuitabilityTest: string = "dialogMemberSuitabilityTest";
    }

    export class ValueHelper
    {
        public static MaxSeachLimit: number = 4;
        public static RecordsPerPage: number = 20; // for Search Page
        public static RecordsPerPageLov: number = 8; // for PopUp Page
        public static RecordsPerPageDetail: number = 10; // for Detail Page

        public static CreateNumberRange(start: number, max: number): string[]
        {
            var limits: string[] = [];
            for (var i: number = start; i <= max; ++i)
                limits.push(i.toString());
            return limits;
        }
    }

    export class FileExtension
    {
        public static Text = ".txt";
        public static Csv = ".csv";
        public static Excel = ".xlsx";
        public static JPG = ".jpg";
        public static PNG = ".png";
        public static Excel2003 = ".xls";
    }

    export class TypeCode
    {
        public static BusinessType = "BusinessType";
        public static CompanyType = "CompanyType";
        public static ResponsibilityType = "ResponsibilityType";
        public static EmploymentType = "EmploymentType";
        public static PayrollDateCondition = "PayrollDateCondition";
        public static ContributionType = "ContributionType";
        public static CompanyVestingType = "CompanyVestingType";
        public static CompanyUnvestType = "CompanyUnvestType";
        public static ResignReason = "ResignReason";
        public static CalculationType = "CalculationType";
        public static CompanyContributionSuspensionReasonType = "CompanyContributionSuspensionReasonType";
    }

    export class Uploader
    {
        public static UploaderFilters = "|jpg|png|jpeg|bmp|gif|";
    }

    export class Operator
    {
        public static GreaterThanOrEqual: string = ">=";
        public static GreaterThan: string = ">";
        public static LessThanOrEqual: string = "<=";
        public static LessThan: string = "<";
        public static Equal: string = "=";
    }

    // Download format type
    export class DownloadFormatType
    {
        public static Excel = "E";
        public static Pdf = "P";
        public static Upload = "U";
    }

    export class MimeTypeDictionary
    {
        public static Excel = "xls";
        public static Pdf = "pdf";
        public static Upload = "upload";
    }

    export class FileStorageFileType
    {
        public static Export = "Export";
        public static Report = "Report";
        public static Upload = "Upload";
    }

    export class ExternalIdentityUserLoginType
    {
        public static UserIsNotActive = "1"
        public static UserIsLockByQuestion = "2";
        public static UserIsLockedByLogin = "3";
        public static UserOrPassWordWasWrong = "4";
        public static MemberIsNotActive = "5";
        public static UserHasSingleCompany = "0";
        public static UserHasManyCompany = "00";
        public static NullValueFromServiceBroker = "6";
        public static OtherError = "7";
    }

    export class CheckAnswerType
    {
        public static AllFailed = "0"
        public static AllCorrect = "1";
        public static UserIsLock = "2";
    }

    export class RouteNames
    {
        public static WelCome = "/welcome";
        public static EmailAccess = "/emailusernameaccessright";
        public static Reset = "/resetpassword";
        public static Unlock = "/unlockuser2";
        public static Request = "/request";
        public static ShowMessage = "/showmessage/index";
        public static RegisterUnlock = "/register/unlock";
        public static Login = "/login";
        public static Register = "/register";
        public static Suitabilitytestreminder = "/member/suitabilitytestreminder";
        public static Membersuitabilitytest = "/member/membersuitabilitytest";
        public static Logout = "/logout";
        public static Home = "/home";
        public static TrackAndTrace = "/trackandtrace";
        public static Error = "/error";
    }

    export class RoleName
    {
        public static Member = "Member"
        public static Admin = "Admin";
    }
}