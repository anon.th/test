﻿module Util
{
    export class AngularLinq
    {
        public static JoinArrays<T>(a1: T[], a2: T[], comparer: (t1: T, t2: T) => boolean): T[]
        {
            var result: T[] = [];
            AngularLinq.ForEach<T>(a1, (t3) =>
            {
                var found: boolean = false;
                AngularLinq.ForEach<T>(a2, (t4) =>
                {
                    if (comparer(t3, t4))                        
                    {
                        result.push(t4);
                        found = true;
                    }
                });

                if (found == false)
                {
                    result.push(t3);
                }

            });
            return result;
        }

        public static ForEach<T>(array: T[], expression: (t: T) => void): T[]
        {
            if (array)
            {
                for (var i: number = 0; i < array.length; i++)
                    expression(array[i]);
            }
            return array;
        }

        public static Where<T>(array: T[], expression: (t: T) => boolean): T[]
        {
            var result: T[] = [];
            AngularLinq.ForEach(array, t => 
            {
                if (expression(t))
                    result.push(t);
            });
            return result;
        }
    }
}