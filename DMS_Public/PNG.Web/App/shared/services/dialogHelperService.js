var Shared;
(function (Shared) {
    // Class
    var DialogHelperService = (function () {
        function DialogHelperService($cookies, $q, createDialog, $window) {
            this.$cookies = $cookies;
            this.$q = $q;
            this.createDialog = createDialog;
            this.$window = $window;
            this.translations = {}; // dictionary of translations
            this.DialogProperties = {
                'width': '700px',
                'height': '630px',
                'overflow': 'auto',
                'max-width': '100%'
            };
            this.DialogBodyCss = {
                'max-height': '630px',
                'width': '100%',
                'overflow': 'auto'
            };
            if (!this.CurrentDialogParameter) {
                this.ClearDialogParameter();
            }
        }
        DialogHelperService.Factory = function ($cookies, $q, createDialog, $window) {
            return new DialogHelperService($cookies, $q, createDialog, $window);
        };
        //set parameter dialog
        DialogHelperService.prototype.SetDialogParameter = function (dialogParameter) {
            this.CurrentDialogParameter = dialogParameter;
        };
        DialogHelperService.prototype.GetDialogParameter = function () {
            return this.CurrentDialogParameter;
        };
        DialogHelperService.prototype.ClearDialogParameter = function () {
            this.CurrentDialogParameter = {};
        };
        //end set paraneter dialog
        DialogHelperService.prototype.SetLabelButton = function () {
            this.okLabel = "OK";
            this.yesLabel = "Yes";
            this.okEnLabel = "OK";
            this.cancelLabel = "No";
        };
        DialogHelperService.prototype.ConfirmMessage = function (title, message, successfn, cancelfn, backdrop, keyboard) {
            var _this = this;
            this.SetLabelButton();
            //get ready to translate the title and message
            //for now title is assumed to be only one token, whereas a message can have one or more
            var words = [];
            var messageWords = message.split(' ');
            words.push(title);
            messageWords.forEach(function (w) { return words.push(w); });
            title = this.translations[title] != null ? this.translations[title] : title;
            //reassemble the message replacing the tokens with the translated text
            messageWords.forEach(function (mw) { return message = message.replace(mw, _this.translations[mw] != null ? _this.translations[mw] : mw); });
            var modal = $(this);
            this.createDialog({
                id: 'confirm-dialog',
                backdrop: backdrop == null ? 'static' : backdrop,
                keyboard: keyboard == null ? false : keyboard,
                title: title,
                css: {
                    'margin-top': Math.max(0, ($(window).height() - 207) / 2.4) //ปรับให้ modal อยู่ตรงกลาง
                },
                template: "<p style='padding: 5px 0 0 15px;'>" + message + "</p>",
                success: {
                    visible: true, label: this.okLabel, fn: successfn
                },
                cancel: {
                    visible: true, label: this.cancelLabel, fn: cancelfn
                },
                isDialogThemeBlack: true
            });
        };
        DialogHelperService.prototype.ConfirmMessageDelete = function (title, message, successfn, cancelfn, backdrop, keyboard) {
            var _this = this;
            this.SetLabelButton();
            //get ready to translate the title and message
            //for now title is assumed to be only one token, whereas a message can have one or more
            var words = [];
            var messageWords = message.split(' ');
            words.push(title);
            messageWords.forEach(function (w) { return words.push(w); });
            title = this.translations[title] != null ? this.translations[title] : title;
            //reassemble the message replacing the tokens with the translated text
            messageWords.forEach(function (mw) { return message = message.replace(mw, _this.translations[mw] != null ? _this.translations[mw] : mw); });
            var modal = $(this);
            this.createDialog({
                id: 'confirm-dialog',
                backdrop: backdrop == null ? 'static' : backdrop,
                keyboard: keyboard == null ? false : keyboard,
                title: title,
                css: {
                    'margin-top': Math.max(0, ($(window).height() - 207) / 2.4) //ปรับให้ modal อยู่ตรงกลาง
                },
                template: "<p style='padding: 5px 0 0 15px;'>" + message + "</p>",
                success: {
                    visible: true, label: this.yesLabel, fn: successfn
                },
                cancel: {
                    visible: true, label: this.cancelLabel, fn: cancelfn
                },
                isDialogThemeBlack: true
            });
        };
        DialogHelperService.prototype.ConfirmMessageHideCLoseBtn = function (title, message, successfn, cancelfn, backdrop, keyboard, _hideCloseBtn) {
            var _this = this;
            if (_hideCloseBtn === void 0) { _hideCloseBtn = true; }
            this.SetLabelButton();
            //get ready to translate the title and message
            //for now title is assumed to be only one token, whereas a message can have one or more
            var words = [];
            var messageWords = message.split(' ');
            words.push(title);
            messageWords.forEach(function (w) { return words.push(w); });
            title = this.translations[title] != null ? this.translations[title] : title;
            //reassemble the message replacing the tokens with the translated text
            messageWords.forEach(function (mw) { return message = message.replace(mw, _this.translations[mw] != null ? _this.translations[mw] : mw); });
            var modal = $(this);
            this.createDialog({
                id: 'confirm-dialog',
                backdrop: backdrop == null ? 'static' : backdrop,
                keyboard: keyboard == null ? false : keyboard,
                title: title,
                css: {
                    'margin-top': Math.max(0, ($(window).height() - 207) / 2.4) //ปรับให้ modal อยู่ตรงกลาง
                },
                template: "<p style='padding: 5px 0 0 15px;'>" + message + "</p>",
                success: {
                    visible: true, label: this.yesLabel, fn: successfn
                },
                cancel: {
                    visible: true, label: this.cancelLabel, fn: cancelfn
                },
                hideCloseBtn: _hideCloseBtn,
                isDialogThemeBlack: false
            });
        };
        DialogHelperService.prototype.ShowMessage = function (title, message, successfn, cancelBtn) {
            var _this = this;
            if (cancelBtn === void 0) { cancelBtn = false; }
            this.SetLabelButton();
            //get ready to translate the title and message
            //for now title is assumed to be only one token, whereas a message can have one or more
            var words = [];
            var messageWords = message.split(' ');
            words.push(title);
            messageWords.forEach(function (w) { return words.push(w); });
            title = this.translations[title] != null ? this.translations[title] : title;
            //reassemble the message replacing the tokens with the translated text
            messageWords.forEach(function (mw) { return message = message.replace(mw, _this.translations[mw] != null ? _this.translations[mw] : mw); });
            this.createDialog({
                id: 'show-dialog',
                backdrop: true,
                title: title,
                css: {
                    'margin-top': Math.max(0, ($(window).height() - 300) / 2.4),
                    'width': '680px',
                    'z-index': '15000'
                },
                template: "<p style='padding: 5px 0 0 15px;'>" + message + "</p>",
                success: { visible: true, label: this.okLabel, fn: successfn },
                cancel: { visible: cancelBtn, label: this.cancelLabel },
                isDialogThemeBlack: true
            });
        };
        DialogHelperService.prototype.ShowMessage2 = function (title, message, successfn, cancelBtn) {
            var _this = this;
            if (cancelBtn === void 0) { cancelBtn = false; }
            this.SetLabelButton();
            //get ready to translate the title and message
            //for now title is assumed to be only one token, whereas a message can have one or more
            var words = [];
            var messageWords = message.split(' ');
            words.push(title);
            messageWords.forEach(function (w) { return words.push(w); });
            title = this.translations[title] != null ? this.translations[title] : title;
            //reassemble the message replacing the tokens with the translated text
            messageWords.forEach(function (mw) { return message = message.replace(mw, _this.translations[mw] != null ? _this.translations[mw] : mw); });
            this.createDialog({
                id: 'show-dialog2',
                backdrop: true,
                title: title,
                css: {
                    'margin-top': Math.max(0, ($(window).height() - 300) / 2.4),
                    'width': '680px',
                    'z-index': '15000'
                },
                template: "<p style='padding: 5px 0 0 15px;'>" + message + "</p>",
                success: { visible: true, label: this.okLabel, fn: successfn },
                cancel: { visible: cancelBtn, label: this.cancelLabel },
                isDialogThemeBlack: true
            });
        };
        DialogHelperService.prototype.ShowRemarkMessage = function (title, message, successfn, cancelBtn) {
            var _this = this;
            if (cancelBtn === void 0) { cancelBtn = false; }
            this.SetLabelButton();
            //get ready to translate the title and message
            //for now title is assumed to be only one token, whereas a message can have one or more
            var words = [];
            var messageWords = message.split(' ');
            words.push(title);
            messageWords.forEach(function (w) { return words.push(w); });
            title = this.translations[title] != null ? this.translations[title] : title;
            //reassemble the message replacing the tokens with the translated text
            messageWords.forEach(function (mw) { return message = message.replace(mw, _this.translations[mw] != null ? _this.translations[mw] : mw); });
            this.createDialog({
                id: 'show-remarkdialog',
                backdrop: true,
                title: title,
                css: {
                    'margin-top': Math.max(0, ($(window).height() - 300) / 2.4),
                    'width': '680px',
                    'z-index': '15000',
                    'word-wrap': 'break-word'
                },
                template: "<p style='padding: 5px 0 0 15px;'>" + message + "</p>",
                success: { visible: true, label: this.okLabel, fn: successfn },
                cancel: { visible: cancelBtn, label: this.cancelLabel }
            });
        };
        DialogHelperService.prototype.ShowTermAndCondition = function (title, message, successfn, cancelBtn) {
            var _this = this;
            if (cancelBtn === void 0) { cancelBtn = false; }
            this.SetLabelButton();
            //get ready to translate the title and message
            //for now title is assumed to be only one token, whereas a message can have one or more
            var words = [];
            var messageWords = message.split(' ');
            words.push(title);
            messageWords.forEach(function (w) { return words.push(w); });
            title = this.translations[title] != null ? this.translations[title] : title;
            //reassemble the message replacing the tokens with the translated text
            messageWords.forEach(function (mw) { return message = message.replace(mw, _this.translations[mw] != null ? _this.translations[mw] : mw); });
            this.createDialog({
                id: 'show-remarkdialog',
                backdrop: true,
                title: title,
                css: {
                    'margin-top': '2%',
                    'width': '900px',
                    'z-index': '15000',
                    'word-wrap': 'break-word'
                },
                template: "<iframe width='850px' height='450px' frameborder='0' src='" + message + "'></iframe>",
                headercss: { 'color': '#26a59b' },
                success: { visible: true, label: this.okLabel, fn: successfn },
                cancel: { visible: cancelBtn, label: this.cancelLabel }
            });
        };
        //Dialog of PolicyBankAccount
        DialogHelperService.prototype.CreatePolicyBankAccountDialog = function (dialogId) {
            var templateUrl = "Master/Policy/DialogPolicyBankAccount/";
            this.createDialog(templateUrl, 'dialog', {
                id: dialogId,
                title: 'PolicyBankAccount.Title',
                showFooter: false,
                css: this.DialogProperties,
                bodycss: this.DialogBodyCss
            });
        };
        //Dialog of RiskOrder
        DialogHelperService.prototype.CreateRiskOrderDialog = function (dialogId) {
            var templateUrl = "Master/Policy/DialogRiskOrder/";
            this.createDialog(templateUrl, 'dialog', {
                id: dialogId,
                title: 'Policy.RiskOrder',
                showFooter: false,
                css: this.DialogProperties,
                bodycss: this.DialogBodyCss
            });
        };
        //Dialog of FundBankAccount
        DialogHelperService.prototype.CreateFundBankAccountDialog = function (dialogId, title) {
            var templateUrl = "Master/Fund/DialogFundBankAccount/";
            this.createDialog(templateUrl, 'dialog', {
                id: dialogId,
                title: title,
                showFooter: false,
                css: this.DialogProperties,
                bodycss: this.DialogBodyCss
            });
        };
        //Dialog of EmailTemplate
        DialogHelperService.prototype.CreateEmailTemplateDialog = function (dialogId) {
            var templateUrl = "Master/EmailTemplate/DialogEmailTemplate/";
            this.createDialog(templateUrl, 'dialog', {
                id: dialogId,
                title: 'EmailTemplate.TestEmail',
                showFooter: false,
                css: this.DialogProperties,
                bodycss: this.DialogBodyCss
            });
        };
        DialogHelperService.prototype.ShowMessageCustom = function (title, message, successfn, cancelBtn, _hideCloseBtn, _keyboard) {
            var _this = this;
            if (cancelBtn === void 0) { cancelBtn = false; }
            this.SetLabelButton();
            //get ready to translate the title and message
            //for now title is assumed to be only one token, whereas a message can have one or more
            var words = [];
            var messageWords = message.split(' ');
            words.push(title);
            messageWords.forEach(function (w) { return words.push(w); });
            title = this.translations[title] != null ? this.translations[title] : title;
            //reassemble the message replacing the tokens with the translated text
            messageWords.forEach(function (mw) { return message = message.replace(mw, _this.translations[mw] != null ? _this.translations[mw] : mw); });
            this.createDialog({
                id: 'show-dialog',
                title: title,
                css: {
                    'margin-top': Math.max(0, ($(window).height() - 300) / 2.4),
                    'width': '680px',
                    'z-index': '15000'
                },
                template: "<p style='padding: 5px 0 0 15px;'>" + message + "</p>",
                success: { visible: true, label: this.okEnLabel, fn: successfn },
                cancel: { visible: cancelBtn, label: this.cancelLabel },
                hideCloseBtn: _hideCloseBtn,
                keyboard: _keyboard,
                isDialogThemeBlack: true
            });
        };
        DialogHelperService.prototype.ConfirmMessageCustomButton = function (title, message, successfn, oklabel, cancellabel, cancelfn, backdrop, keyboard) {
            var _this = this;
            // this.SetLabelButton();
            //get ready to translate the title and message
            //for now title is assumed to be only one token, whereas a message can have one or more
            var words = [];
            var messageWords = message.split(' ');
            words.push(title);
            messageWords.forEach(function (w) { return words.push(w); });
            title = this.translations[title] != null ? this.translations[title] : title;
            //reassemble the message replacing the tokens with the translated text
            messageWords.forEach(function (mw) { return message = message.replace(mw, _this.translations[mw] != null ? _this.translations[mw] : mw); });
            var modal = $(this);
            this.createDialog({
                id: 'confirm-dialog',
                backdrop: backdrop == null ? 'static' : backdrop,
                keyboard: keyboard == null ? false : keyboard,
                title: title,
                css: {
                    'margin-top': Math.max(0, ($(window).height() - 207) / 2.4) //ปรับให้ modal อยู่ตรงกลาง
                },
                template: "<p style='padding: 5px 0 0 15px;'>" + message + "</p>",
                success: {
                    visible: true, label: oklabel, fn: successfn
                },
                cancel: {
                    visible: true, label: cancellabel, fn: cancelfn
                },
                isDialogThemeBlack: true
            });
        };
        return DialogHelperService;
    }());
    Shared.DialogHelperService = DialogHelperService;
    Main.App.Services.factory("dialogHelperService", ["$cookies", "$q", "createDialog", "$window", DialogHelperService.Factory]);
})(Shared || (Shared = {}));
//# sourceMappingURL=dialogHelperService.js.map