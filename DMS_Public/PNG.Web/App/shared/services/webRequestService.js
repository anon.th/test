var Util;
(function (Util) {
    var WebRequestService = (function () {
        function WebRequestService($http, $q, $cookies, $location, localStorageService) {
            this.$http = $http;
            this.$q = $q;
            this.$cookies = $cookies;
            this.$location = $location;
            this.localStorageService = localStorageService;
        }
        WebRequestService.Factory = function ($http, $q, $cookies, $location, localStorageService) {
            return new WebRequestService($http, $q, $cookies, $location, localStorageService);
        };
        WebRequestService.StripValidators = function (data) {
            // is data an array?
            if (typeof data != "string" && data.hasOwnProperty("length")) {
                for (var d in data)
                    data[d] = WebRequestService.StripValidators(data[d]);
            }
            else
                data = WebRequestService.StripValidator(data);
            return data;
        };
        WebRequestService.StripValidator = function (data) {
            if (typeof data == "string")
                return data;
            var copy = {};
            for (var property in data) {
                if (property != "Validator")
                    copy[property] = data[property];
            }
            return copy;
        };
        WebRequestService.prototype.Get = function (uri) {
            var _this = this;
            var deferred = this.$q.defer();
            this.$http.get(this.uriParser(uri), { headers: this.buildRequestHeaders() })
                .then(function (arg) {
                var result = _this.resolveReferences(arg.data);
                if (!angular.isUndefined(result) && result != null && result.ExceptionMessage == undefined) {
                    deferred.resolve(result);
                }
                else {
                    deferred.reject(arg);
                }
            }, function (arg) {
                deferred.reject(arg);
            });
            return deferred.promise;
        };
        WebRequestService.prototype.Post = function (uri, data) {
            var _this = this;
            data = WebRequestService.StripValidators(data);
            var deferred = this.$q.defer();
            this.$http.post(this.uriParser(uri), data, { headers: this.buildRequestHeaders() })
                .then(function (arg) {
                var result = _this.resolveReferences(arg.data);
                if (!angular.isUndefined(result) && result != null && result.ExceptionMessage == undefined) {
                    deferred.resolve(result);
                }
                else {
                    deferred.reject(arg);
                }
            }, function (arg) {
                deferred.reject(arg);
            });
            return deferred.promise;
        };
        WebRequestService.prototype.Put = function (uri, data) {
            var _this = this;
            data = WebRequestService.StripValidators(data);
            var deferred = this.$q.defer();
            this.$http.put(this.uriParser(uri), data, { headers: this.buildRequestHeaders() })
                .then(function (arg) {
                var result = _this.resolveReferences(arg.data);
                if (!angular.isUndefined(result) && result != null && result.ExceptionMessage == undefined) {
                    deferred.resolve(result);
                }
                else {
                    deferred.reject(arg);
                }
            }, function (arg) {
                deferred.reject(arg);
            });
            return deferred.promise;
        };
        WebRequestService.prototype.Delete = function (uri) {
            var _this = this;
            var deferred = this.$q.defer();
            this.$http['delete'](this.uriParser(uri), { headers: this.buildRequestHeaders() })
                .then(function (arg) {
                var result = _this.resolveReferences(arg.data);
                if (!angular.isUndefined(result) && result != null && result.ExceptionMessage == undefined) {
                    deferred.resolve(result);
                }
                else {
                    deferred.reject(arg);
                }
            }, function (arg) {
                deferred.reject(arg);
            });
            return deferred.promise;
        };
        WebRequestService.prototype.resolveReferences = function (json) { return json; };
        WebRequestService.prototype.isUnauthorised = function (arg) {
            if (arg.status == 401 && arg.data.Message == "Authorization has been denied for this request.") {
                window.alert(arg.data.Message);
                return true;
            }
            return false;
        };
        WebRequestService.prototype.buildRequestHeaders = function () {
            var userInfoAuth = this.localStorageService.get("authorizationData");
            if (userInfoAuth != null) {
                return {
                    'Accept-Language': "en-US",
                    'CurrentMenu': this.$location.url(),
                    'username': userInfoAuth.userName,
                    'token': userInfoAuth.token
                };
            }
            else {
                return {
                    'Accept-Language': "en-US",
                    'CurrentMenu': this.$location.url(),
                    'username': null,
                    'token': null
                };
            }
            //return { 'Accept-Language': "en-US", 'CurrentMenu': this.$location.url() };
        };
        WebRequestService.prototype.uriParser = function (uri) {
            var result = "";
            try {
                var parser = document.createElement("a");
                parser.href = uri;
                result = parser.protocol + "//" + parser.host + "/" + this.ltrim(parser.pathname, "/") + parser.search;
            }
            catch (ex) {
                result = uri;
            }
            return result;
        };
        WebRequestService.prototype.ltrim = function (uri, chars) {
            chars = chars || "\\s*";
            return uri.replace(new RegExp("^[" + chars + "]+", "g"), "");
        };
        WebRequestService.prototype.rtrim = function (uri, chars) {
            chars = chars || "\\s*";
            return uri.replace(new RegExp("[" + chars + "]+$", "g"), "");
        };
        return WebRequestService;
    }());
    Util.WebRequestService = WebRequestService;
    Main.App.Services.factory("webRequestService", ["$http", "$q", "$cookies", "$location", "localStorageService", WebRequestService.Factory]);
})(Util || (Util = {}));
//# sourceMappingURL=webRequestService.js.map