﻿module Util
{
    export class WebRequestService
    {
        public static Factory($http: ng.IHttpService, $q: ng.IQService, $cookies: Interfaces.ICookies, $location: ng.ILocationService, localStorageService: ng.localStorage.ILocalStorageService)
        {
            return new WebRequestService($http, $q, $cookies, $location, localStorageService);
        }

        constructor(private $http: ng.IHttpService, private $q: ng.IQService, private $cookies: Interfaces.ICookies, private $location: ng.ILocationService, private localStorageService: ng.localStorage.ILocalStorageService)
        {
        }

        public static StripValidators<T>(data: any)
        public static StripValidators<T>(data: T[]): T[]
        {
            // is data an array?
            if (typeof data != "string" && data.hasOwnProperty("length"))
            {
                for (var d in data)
                    data[d] = WebRequestService.StripValidators(data[d]);
            } else
                data = WebRequestService.StripValidator(data);
            return data;
        }

        public static StripValidator<T>(data: T): T
        {
            if (typeof data == "string")
                return data;
            var copy: any = {};
            for (var property in data)
            {
                if (property != "Validator")
                    copy[property] = data[property];
            }
            return copy;
        }

        public Get<T>(uri: string): ng.IPromise<T>
        {
            var deferred = this.$q.defer<T>();

            this.$http.get(this.uriParser(uri), { headers: this.buildRequestHeaders() })
                .then((arg: ng.IHttpPromiseCallbackArg<T>) =>
                {
                    var result = this.resolveReferences(arg.data);
                    if (!angular.isUndefined(result) && result != null && result.ExceptionMessage == undefined)
                    {
                        deferred.resolve(result);
                    } else
                    {
                        deferred.reject(arg);
                    }
                }, (arg: ng.IHttpPromiseCallbackArg<any>) =>
                {
                    deferred.reject(arg);
                });

            return deferred.promise;
        }

        public Post<T1, T2>(uri: string, data: T1): ng.IPromise<T2>
        public Post<T1, T2>(uri: string, data: T1[]): ng.IPromise<T2>
        {
            data = WebRequestService.StripValidators(data);
            var deferred = this.$q.defer<T2>();

            this.$http.post(this.uriParser(uri), data, { headers: this.buildRequestHeaders() })
                .then((arg: ng.IHttpPromiseCallbackArg<T2>) =>
                {
                    var result = this.resolveReferences(arg.data);
                    if (!angular.isUndefined(result) && result != null && result.ExceptionMessage == undefined)
                    {
                        deferred.resolve(result);
                    } else
                    {
                        deferred.reject(arg);
                    }
                }, (arg: ng.IHttpPromiseCallbackArg<any>) =>
                {
                    deferred.reject(arg);
                });

            return deferred.promise;
        }

        public Put<T1, T2>(uri: string, data: T1): ng.IPromise<T2>
        public Put<T1, T2>(uri: string, data: T1[]): ng.IPromise<T2>
        {
            data = WebRequestService.StripValidators(data);
            var deferred = this.$q.defer<T2>();

            this.$http.put(this.uriParser(uri), data, { headers: this.buildRequestHeaders() })
                .then((arg: ng.IHttpPromiseCallbackArg<T2>) =>
                {
                    var result = this.resolveReferences(arg.data);
                    if (!angular.isUndefined(result) && result != null && result.ExceptionMessage == undefined)
                    {
                        deferred.resolve(result);
                    } else
                    {
                        deferred.reject(arg);
                    }
                }, (arg: ng.IHttpPromiseCallbackArg<any>) =>
                {
                    deferred.reject(arg);
                });

            return deferred.promise;
        }

        public Delete<T1, T2>(uri: string): ng.IPromise<T2>
        {
            var deferred = this.$q.defer<T2>();

            this.$http['delete'](this.uriParser(uri), { headers: this.buildRequestHeaders() })
                .then((arg: ng.IHttpPromiseCallbackArg<T2>) =>
                {
                    var result = this.resolveReferences(arg.data);
                    if (!angular.isUndefined(result) && result != null && result.ExceptionMessage == undefined)
                    {
                        deferred.resolve(result);
                    } else
                    {
                        deferred.reject(arg);
                    }
                }, (arg: ng.IHttpPromiseCallbackArg<any>) =>
                {
                    deferred.reject(arg);
                });

            return deferred.promise;
        }

        private resolveReferences(json) { return json; }

        private isUnauthorised(arg: ng.IHttpPromiseCallbackArg<any>): boolean
        {
            if (arg.status == 401 && arg.data.Message == "Authorization has been denied for this request.")
            {
                window.alert(arg.data.Message);
                return true;
            }
            return false;
        }

        private buildRequestHeaders()
        {
            var userInfoAuth = this.localStorageService.get("authorizationData");
            if (userInfoAuth != null) {
                return {
                    'Accept-Language': "en-US",
                    'CurrentMenu': this.$location.url(),
                    'username': userInfoAuth.userName,
                    'token': userInfoAuth.token
                };
            } else 
            {
                return {
                    'Accept-Language': "en-US",
                    'CurrentMenu': this.$location.url(),
                    'username': null,
                    'token': null
                };
            }
            //return { 'Accept-Language': "en-US", 'CurrentMenu': this.$location.url() };
        }

        private uriParser(uri: string): string
        {
            var result: string = "";

            try
            {
                var parser = document.createElement("a");
                parser.href = uri;

                result = parser.protocol + "//" + parser.host + "/" + this.ltrim(parser.pathname, "/") + parser.search;
            }
            catch (ex)
            {
                result = uri;
            }

            return result;
        }

        private ltrim(uri: string, chars: string): string
        {
            chars = chars || "\\s*";
            return uri.replace(new RegExp("^[" + chars + "]+", "g"), "");
        }

        private rtrim(uri: string, chars: string): string
        {
            chars = chars || "\\s*";
            return uri.replace(new RegExp("[" + chars + "]+$", "g"), "");
        }
    }

    Main.App.Services.factory("webRequestService", ["$http", "$q", "$cookies", "$location", "localStorageService", WebRequestService.Factory]);
}