var Util;
(function (Util) {
    var Pagination = (function () {
        function Pagination() {
        }
        Pagination.Factory = function () {
            return new Pagination();
        };
        Pagination.prototype.CreatePageLinks = function (totalRecordCount, recordsPerPage, urlPrefix, skip, currentPageId, urlPostFix) {
            if (urlPostFix === void 0) { urlPostFix = ""; }
            //instantiate new page links
            this.PageLinks = [];
            this.TotalCount = totalRecordCount;
            this.RecordsPerPage = recordsPerPage;
            this.TotalPages = Math.ceil(this.TotalCount / this.RecordsPerPage);
            this.UrlPrefix = urlPrefix;
            this.CurrentPageId = currentPageId;
            this.CurrentSkip = skip;
            //create tne next and back skip intervals 
            var nextSkipInterval = (this.CurrentSkip + this.RecordsPerPage);
            var backSkipInterval = (this.CurrentSkip - this.RecordsPerPage);
            //create the new page Ids for back and next links
            var backPageId = this.CurrentPageId - 1;
            var nextPageId = this.CurrentPageId + 1;
            var i = 0;
            //make sure we need to generate the links
            if (!(this.RecordsPerPage >= this.TotalCount)) {
                //make back link
                this.PageLinks.push({
                    Id: i,
                    Label: "<<",
                    Link: this.CurrentSkip <= 0 ? "" : this.UrlPrefix + "page/" + backPageId + "/skip/" + backSkipInterval + "/" + urlPostFix,
                    Current: false,
                    Disabled: this.CurrentSkip <= 0 ? true : false
                });
                i = i + 1;
                //make numbered links between back and next
                var individualSkip = 0;
                while (i <= this.TotalPages) {
                    this.PageLinks.push({
                        Id: i,
                        Label: i.toString(),
                        Link: this.UrlPrefix + "page/" + i + "/skip/" + individualSkip + "/" + urlPostFix,
                        Current: i == this.CurrentPageId ? true : false,
                        Disabled: false
                    });
                    individualSkip = individualSkip + this.RecordsPerPage;
                    i++;
                }
                //make next link
                this.PageLinks.push({
                    Id: i,
                    Label: ">>",
                    Link: nextSkipInterval >= this.TotalCount ? "" : this.UrlPrefix + "page/" + nextPageId + "/skip/" + nextSkipInterval + "/" + urlPostFix,
                    Current: false,
                    Disabled: nextSkipInterval >= this.TotalCount ? true : false
                });
            }
        };
        Pagination.prototype.CreateLinks = function (totalRecordCount, recordsPerPage, urlPrefix, skip, pageId, urlPostFix) {
            if (urlPostFix === void 0) { urlPostFix = ""; }
            return urlPrefix + "page/" + pageId + "/skip/" + skip + "/" + urlPostFix;
        };
        Pagination.prototype.Clear = function () {
            this.PageLinks = [];
            this.RecordsPerPage = null;
            this.TotalCount = null;
            this.TotalPages = null;
            this.CurrentPageId = null;
            this.CurrentSkip = null;
            this.UrlPrefix = null;
        };
        return Pagination;
    }());
    Util.Pagination = Pagination;
    Main.App.Services.factory("paginationService", [Pagination.Factory]);
})(Util || (Util = {}));
//# sourceMappingURL=paginationService.js.map