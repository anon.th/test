﻿module Shared
{
    // Class
    export class DialogHelperService
    {
        private CurrentDialogParameter: Interfaces.IDialogParameter;
        private okLabel: string;
        private yesLabel: string;
        private okEnLabel: string;
        private cancelLabel: string;
        private DialogProperties: any;
        private DialogBodyCss: any;
        private DialogChequeBodyCss: any;
        public translations: any = {}; // dictionary of translations
        public static Factory(
            $cookies: Interfaces.ICookies,
            $q: ng.IQService,
            createDialog: any,
            $window: any)
        {
            return new DialogHelperService($cookies, $q, createDialog, $window);
        }

        constructor(private $cookies: Interfaces.ICookies,
            private $q: ng.IQService,
            private createDialog: any,
            private $window: any)
        {
            this.DialogProperties = {
                'width': '700px',
                'height': '630px',
                'overflow': 'auto',
                'max-width': '100%'
            };

            this.DialogBodyCss = {
                'max-height': '630px',
                'width': '100%',
                'overflow': 'auto'
            };

            if (!this.CurrentDialogParameter)
            {
                this.ClearDialogParameter();
            }
        }

        //set parameter dialog
        public SetDialogParameter(dialogParameter: Interfaces.IDialogParameter): void
        {
            this.CurrentDialogParameter = dialogParameter;
        }

        public GetDialogParameter(): Interfaces.IDialogParameter
        {
            return this.CurrentDialogParameter;
        }

        public ClearDialogParameter(): void
        {
            this.CurrentDialogParameter = {};
        }
        //end set paraneter dialog

        private SetLabelButton(): void
        {
            this.okLabel = "OK";
            this.yesLabel = "Yes";
            this.okEnLabel = "OK";
            this.cancelLabel = "No";
        }

        public ConfirmMessage(title: string, message: string, successfn: any, cancelfn?: any, backdrop?: any, keyboard?: boolean): void {
            this.SetLabelButton();
            //get ready to translate the title and message
            //for now title is assumed to be only one token, whereas a message can have one or more
            var words: string[] = [];
            var messageWords = message.split(' ');
            words.push(title);
            messageWords.forEach(w => words.push(w));

            title = this.translations[title] != null ? this.translations[title] : title;

            //reassemble the message replacing the tokens with the translated text
            messageWords.forEach(mw => message = message.replace(mw, this.translations[mw] != null ? this.translations[mw] : mw));

            var modal = $(this);

            this.createDialog(
                {
                    id: 'confirm-dialog',
                    backdrop: backdrop == null ? 'static' : backdrop,
                    keyboard: keyboard == null ? false : keyboard,
                    title: title,
                    css:
                    {
                        'margin-top': Math.max(0, ($(window).height() - 207) / 2.4) //ปรับให้ modal อยู่ตรงกลาง
                    },
                    template: "<p style='padding: 5px 0 0 15px;'>" + message + "</p>",
                    success: {
                        visible: true, label: this.okLabel, fn: successfn
                    },
                    cancel: {
                        visible: true, label: this.cancelLabel, fn: cancelfn
                    },
                    isDialogThemeBlack: true
                });
        }

        public ConfirmMessageDelete(title: string, message: string, successfn: any, cancelfn?: any, backdrop?: any, keyboard?: boolean): void {
            this.SetLabelButton();
            //get ready to translate the title and message
            //for now title is assumed to be only one token, whereas a message can have one or more
            var words: string[] = [];
            var messageWords = message.split(' ');
            words.push(title);
            messageWords.forEach(w => words.push(w));

            title = this.translations[title] != null ? this.translations[title] : title;

            //reassemble the message replacing the tokens with the translated text
            messageWords.forEach(mw => message = message.replace(mw, this.translations[mw] != null ? this.translations[mw] : mw));

            var modal = $(this);

            this.createDialog(
                {
                    id: 'confirm-dialog',
                    backdrop: backdrop == null ? 'static' : backdrop,
                    keyboard: keyboard == null ? false : keyboard,
                    title: title,
                    css:
                    {
                        'margin-top': Math.max(0, ($(window).height() - 207) / 2.4) //ปรับให้ modal อยู่ตรงกลาง
                    },
                    template: "<p style='padding: 5px 0 0 15px;'>" + message + "</p>",
                    success: {
                        visible: true, label: this.yesLabel, fn: successfn
                    },
                    cancel: {
                        visible: true, label: this.cancelLabel, fn: cancelfn
                    },
                    isDialogThemeBlack: true
                });
        }

        public ConfirmMessageHideCLoseBtn(title: string, message: string, successfn: any, cancelfn?: any, backdrop?: any, keyboard?: boolean, _hideCloseBtn: boolean = true): void
        {
            this.SetLabelButton();
            //get ready to translate the title and message
            //for now title is assumed to be only one token, whereas a message can have one or more
            var words: string[] = [];
            var messageWords = message.split(' ');
            words.push(title);
            messageWords.forEach(w => words.push(w));

            title = this.translations[title] != null ? this.translations[title] : title;

            //reassemble the message replacing the tokens with the translated text
            messageWords.forEach(mw => message = message.replace(mw, this.translations[mw] != null ? this.translations[mw] : mw));

            var modal = $(this);
            this.createDialog(
                {
                    id: 'confirm-dialog',
                    backdrop: backdrop == null ? 'static' : backdrop,
                    keyboard: keyboard == null ? false : keyboard,
                    title: title,
                    css:
                    {
                        'margin-top': Math.max(0, ($(window).height() - 207) / 2.4) //ปรับให้ modal อยู่ตรงกลาง
                    },
                    template: "<p style='padding: 5px 0 0 15px;'>" + message + "</p>",
                    success: {
                        visible: true, label: this.yesLabel, fn: successfn
                    },
                    cancel: {
                        visible: true, label: this.cancelLabel, fn: cancelfn
                    },
                    hideCloseBtn: _hideCloseBtn,
                    isDialogThemeBlack: false
                });
        }

        public ShowMessage(title: string, message: string, successfn?: any, cancelBtn: boolean = false): void
        {
            this.SetLabelButton();
            //get ready to translate the title and message
            //for now title is assumed to be only one token, whereas a message can have one or more
            var words: string[] = [];
            var messageWords = message.split(' ');
            words.push(title);
            messageWords.forEach(w => words.push(w));

            title = this.translations[title] != null ? this.translations[title] : title;

            //reassemble the message replacing the tokens with the translated text
            messageWords.forEach(mw => message = message.replace(mw, this.translations[mw] != null ? this.translations[mw] : mw));
            this.createDialog(
                {
                    id: 'show-dialog',
                    backdrop: true,
                    title: title,
                    css:
                    {
                        'margin-top': Math.max(0, ($(window).height() - 300) / 2.4), //ปรับให้ modal อยู่ตรงกลาง
                        'width': '680px',
                        'z-index': '15000'
                    },
                    template: "<p style='padding: 5px 0 0 15px;'>" + message + "</p>",
                    success: { visible: true, label: this.okLabel, fn: successfn },
                    cancel: { visible: cancelBtn, label: this.cancelLabel },
                    isDialogThemeBlack: true
                });
        }

        public ShowMessage2(title: string, message: string, successfn?: any, cancelBtn: boolean = false): void {
            this.SetLabelButton();
            //get ready to translate the title and message
            //for now title is assumed to be only one token, whereas a message can have one or more
            var words: string[] = [];
            var messageWords = message.split(' ');
            words.push(title);
            messageWords.forEach(w => words.push(w));

            title = this.translations[title] != null ? this.translations[title] : title;

            //reassemble the message replacing the tokens with the translated text
            messageWords.forEach(mw => message = message.replace(mw, this.translations[mw] != null ? this.translations[mw] : mw));
            this.createDialog(
                {
                    id: 'show-dialog2',
                    backdrop: true,
                 
                    title: title,
                    css:
                    {
                        'margin-top': Math.max(0, ($(window).height() - 300) / 2.4), //ปรับให้ modal อยู่ตรงกลาง
                        'width': '680px',
                        'z-index': '15000'
                    },
                    template: "<p style='padding: 5px 0 0 15px;'>" + message + "</p>",
                    success: { visible: true, label: this.okLabel, fn: successfn},
                    cancel: { visible: cancelBtn, label: this.cancelLabel },
                    isDialogThemeBlack: true
                });
        }

       

        public ShowRemarkMessage(title: string, message: string, successfn?: any, cancelBtn: boolean = false): void
        {
            this.SetLabelButton();
            //get ready to translate the title and message
            //for now title is assumed to be only one token, whereas a message can have one or more
            var words: string[] = [];
            var messageWords = message.split(' ');
            words.push(title);
            messageWords.forEach(w => words.push(w));

            title = this.translations[title] != null ? this.translations[title] : title;

            //reassemble the message replacing the tokens with the translated text
            messageWords.forEach(mw => message = message.replace(mw, this.translations[mw] != null ? this.translations[mw] : mw));
            this.createDialog(
                {
                    id: 'show-remarkdialog',
                    backdrop: true,
                    title: title,
                    css:
                    {
                        'margin-top': Math.max(0, ($(window).height() - 300) / 2.4), //ปรับให้ modal อยู่ตรงกลาง
                        'width': '680px',
                        'z-index': '15000',
                        'word-wrap': 'break-word'
                    },
                    template: "<p style='padding: 5px 0 0 15px;'>" + message + "</p>",
                    success: { visible: true, label: this.okLabel, fn: successfn },
                    cancel: { visible: cancelBtn, label: this.cancelLabel }
                });
        }

        public ShowTermAndCondition(title: string, message: string, successfn?: any, cancelBtn: boolean = false): void
        {
            this.SetLabelButton();
            //get ready to translate the title and message
            //for now title is assumed to be only one token, whereas a message can have one or more
            var words: string[] = [];
            var messageWords = message.split(' ');
            words.push(title);
            messageWords.forEach(w => words.push(w));

            title = this.translations[title] != null ? this.translations[title] : title;

            //reassemble the message replacing the tokens with the translated text
            messageWords.forEach(mw => message = message.replace(mw, this.translations[mw] != null ? this.translations[mw] : mw));
            this.createDialog(
                {
                    id: 'show-remarkdialog',
                    backdrop: true,
                    title: title,
                    css:
                    {
                        'margin-top': '2%',
                        'width': '900px',
                        'z-index': '15000',
                        'word-wrap': 'break-word'
                    },
                    template: "<iframe width='850px' height='450px' frameborder='0' src='" + message + "'></iframe>",
                    headercss: { 'color': '#26a59b' },
                    success: { visible: true, label: this.okLabel, fn: successfn },
                    cancel: { visible: cancelBtn, label: this.cancelLabel }
                });
        }

        //Dialog of PolicyBankAccount
        public CreatePolicyBankAccountDialog(dialogId: string): void
        {
            var templateUrl = "Master/Policy/DialogPolicyBankAccount/";
            this.createDialog(templateUrl, 'dialog',
                {
                    id: dialogId,
                    title: 'PolicyBankAccount.Title',
                    showFooter: false,
                    css: this.DialogProperties,
                    bodycss: this.DialogBodyCss
                });
        }

        //Dialog of RiskOrder
        public CreateRiskOrderDialog(dialogId: string): void
        {
            var templateUrl = "Master/Policy/DialogRiskOrder/";
            this.createDialog(templateUrl, 'dialog',
                {
                    id: dialogId,
                    title: 'Policy.RiskOrder',
                    showFooter: false,
                    css: this.DialogProperties,
                    bodycss: this.DialogBodyCss
                });
        }

        //Dialog of FundBankAccount
        public CreateFundBankAccountDialog(dialogId: string, title: string): void
        {
            var templateUrl = "Master/Fund/DialogFundBankAccount/";
            this.createDialog(templateUrl, 'dialog',
                {
                    id: dialogId,
                    title: title,
                    showFooter: false,
                    css: this.DialogProperties,
                    bodycss: this.DialogBodyCss
                });
        }

       

        //Dialog of EmailTemplate
        public CreateEmailTemplateDialog(dialogId: string): void
        {
            var templateUrl = "Master/EmailTemplate/DialogEmailTemplate/";
            this.createDialog(templateUrl, 'dialog',
                {
                    id: dialogId,
                    title: 'EmailTemplate.TestEmail',
                    showFooter: false,
                    css: this.DialogProperties,
                    bodycss: this.DialogBodyCss
                });
        }

        




      

        public ShowMessageCustom(title: string, message: string, successfn?: any, cancelBtn: boolean = false, _hideCloseBtn?: boolean, _keyboard?: boolean): void
        {
            this.SetLabelButton();
            //get ready to translate the title and message
            //for now title is assumed to be only one token, whereas a message can have one or more
            var words: string[] = [];
            var messageWords = message.split(' ');
            words.push(title);
            messageWords.forEach(w => words.push(w));

            title = this.translations[title] != null ? this.translations[title] : title;

            //reassemble the message replacing the tokens with the translated text
            messageWords.forEach(mw => message = message.replace(mw, this.translations[mw] != null ? this.translations[mw] : mw));
            this.createDialog(
                {
                    id: 'show-dialog',
                    title: title,
                    css:
                    {
                        'margin-top': Math.max(0, ($(window).height() - 300) / 2.4), //ปรับให้ modal อยู่ตรงกลาง
                        'width': '680px',
                        'z-index': '15000'
                    },
                    template: "<p style='padding: 5px 0 0 15px;'>" + message + "</p>",
                    success: { visible: true, label: this.okEnLabel, fn: successfn },
                    cancel: { visible: cancelBtn, label: this.cancelLabel },
                    hideCloseBtn: _hideCloseBtn,

                    keyboard: _keyboard,
                    isDialogThemeBlack: true
                });
        }

        public ConfirmMessageCustomButton(title: string, message: string, successfn: any, oklabel: string, cancellabel: string, cancelfn?: any, backdrop?: any, keyboard?: boolean): void
        {
            // this.SetLabelButton();
            //get ready to translate the title and message
            //for now title is assumed to be only one token, whereas a message can have one or more
            var words: string[] = [];
            var messageWords = message.split(' ');
            words.push(title);
            messageWords.forEach(w => words.push(w));

            title = this.translations[title] != null ? this.translations[title] : title;

            //reassemble the message replacing the tokens with the translated text
            messageWords.forEach(mw => message = message.replace(mw, this.translations[mw] != null ? this.translations[mw] : mw));

            var modal = $(this);

            this.createDialog(
                {
                    id: 'confirm-dialog',
                    backdrop: backdrop == null ? 'static' : backdrop,
                    keyboard: keyboard == null ? false : keyboard,
                    title: title,
                    css:
                    {
                        'margin-top': Math.max(0, ($(window).height() - 207) / 2.4) //ปรับให้ modal อยู่ตรงกลาง
                    },
                    template: "<p style='padding: 5px 0 0 15px;'>" + message + "</p>",
                    success: {
                        visible: true, label: oklabel, fn: successfn
                    },
                    cancel: {
                        visible: true, label: cancellabel, fn: cancelfn
                    },
                    isDialogThemeBlack: true
                });
        }
    }

    Main.App.Services.factory("dialogHelperService", ["$cookies", "$q", "createDialog", "$window", DialogHelperService.Factory]);
}  