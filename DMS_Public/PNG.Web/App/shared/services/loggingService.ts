﻿module Shared
{
    var config: any;
    export class LoggingService
    {

        public static Factory($log: ng.ILogService, notify: Shared.NotificationService)
        {
            return new LoggingService($log, notify);
        }

        constructor(
            private $log: ng.ILogService,
            private notify: Shared.NotificationService
        )
        {

        }

        public LogErrorOnly(...args: any[])
        {
            //if (config.LogLevel >= 1)
            {
                // this trick will pass the args array on, with out hacking around
                this.$log.error.apply(this, args);
            }
        }
    }

    Main.App.Services.factory("loggingService", ["$log", "notificationService", LoggingService.Factory]);
}