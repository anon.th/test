var Shared;
(function (Shared) {
    var config;
    var LoggingService = (function () {
        function LoggingService($log, notify) {
            this.$log = $log;
            this.notify = notify;
        }
        LoggingService.Factory = function ($log, notify) {
            return new LoggingService($log, notify);
        };
        LoggingService.prototype.LogErrorOnly = function () {
            var args = [];
            for (var _i = 0; _i < arguments.length; _i++) {
                args[_i - 0] = arguments[_i];
            }
            //if (config.LogLevel >= 1)
            {
                // this trick will pass the args array on, with out hacking around
                this.$log.error.apply(this, args);
            }
        };
        return LoggingService;
    }());
    Shared.LoggingService = LoggingService;
    Main.App.Services.factory("loggingService", ["$log", "notificationService", LoggingService.Factory]);
})(Shared || (Shared = {}));
//# sourceMappingURL=loggingService.js.map