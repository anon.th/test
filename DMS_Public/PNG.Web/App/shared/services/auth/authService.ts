﻿/// <reference path="../../../../Scripts/typings/angularLocalStorage/angularLocalStorage.d.ts" />

"use strict";

module Util
{
    export class AuthService
    {
        private $http: ng.IHttpService;
        private $q: ng.IQService;
        private localStorageService: ng.localStorage.ILocalStorageService;
        private $sessionStorage: any
        private configSettings: Interfaces.IConfigSettings;
        private webRequestService: Util.WebRequestService;
        private $location: ng.ILocationService;
        private $rootScope: ng.IScope;

        authentication: any;
        externalAuthData: any;

        public userInfo: Interfaces.IUserInfo;

        public static Init($http: ng.IHttpService, $q: ng.IQService, localStorageService: ng.localStorage.ILocalStorageService, $sessionStorage: any, configSettings: Interfaces.IConfigSettings, webRequestService: Util.WebRequestService, $location: ng.ILocationService, $rootScope: ng.IScope)
        {
            return new AuthService($http, $q, localStorageService, $sessionStorage, configSettings, webRequestService, $location, $rootScope);
        }

        constructor($http: ng.IHttpService, $q: ng.IQService, localStorageService: ng.localStorage.ILocalStorageService, $sessionStorage: any, configSettings: Interfaces.IConfigSettings, webRequestService: Util.WebRequestService, $location: ng.ILocationService, $rootScope: ng.IScope)
        {
            this.$http = $http;
            this.$q = $q;
            this.localStorageService = localStorageService;
            this.$sessionStorage = $sessionStorage;
            this.configSettings = configSettings;
            this.webRequestService = webRequestService;
            this.$location = $location;
            this.$rootScope = $rootScope;
            this.authentication = {
                isAuth: false,
                userName: "",
                useRefreshTokens: false,
                email: ""
            };
            this.externalAuthData = {
                provider: "",
                userName: "",
                externalAccessToken: ""
            };
            this.userInfo = {
                Id: "",
                UserName: "",
                FirstName: "",
                LastName: "",
                Email: "",
                RoleId: "",
                RoleCode: "",
                ListUserMenu: [],
                UserId: ""
            }
        }

        SaveRegistration(registration)
        {
            var self = this;
            self.LogOut();

            return self.$http.post(self.configSettings.apiServiceBaseUri + "api/account/register", registration).then(function (response)
            {
                return response;
            });
        }

        public Login(loginData: Interfaces.ILoginData)
        {
            var self = this;

            var data: Interfaces.IUserLogin = {
                Username: loginData.userName,
                Password: btoa(encodeURIComponent(loginData.password)),
                IpAddress: loginData.IpAddress,
                EnterpriseId: loginData.enterpriseid
            };

            if (loginData.useRefreshTokens)
            {
                data.ClientId = self.configSettings.clientId;
            }

            var deferred = self.$q.defer();

            self.LoginMaster(data)
                .then((response: any) =>           
                {
                    if (loginData.useRefreshTokens)
                    {
                        self.localStorageService.set("authorizationData", {
                            token: btoa(encodeURIComponent(response.access_token)),
                            userName: loginData.userName,
                            clientConnectionId: response.ClientConnectionId,
                            forceChangePassword: response.forceChangePassword,
                            refreshToken: response.refresh_token,
                            useRefreshTokens: true,
                            userTypeId: response.UserTypeId
                        });
                    }
                    else
                    {
                        self.localStorageService.set("authorizationData", {
                            token: btoa(encodeURIComponent(response.access_token)),
                            userName: loginData.userName,
                            clientConnectionId: response.ClientConnectionId,
                            refreshToken: "",
                            useRefreshTokens: false,
                            userTypeId: response.UserTypeId
                        });
                    }

                    self.authentication.isAuth = true;
                    self.authentication.userName = loginData.userName;
                    self.authentication.useRefreshTokens = loginData.useRefreshTokens;
                   // console.log(self.localStorageService.get("authorizationData"));
                    deferred.resolve(response);

                }).catch((err) =>
                {
                    this.LogOut();
                    deferred.reject(err);
                });

            self.authentication.isAuth = true;
            self.authentication.userName = loginData.userName;
            self.authentication.useRefreshTokens = loginData.useRefreshTokens;
            //console.log(self.localStorageService.get("authorizationData"));

            return deferred.promise;
        }

        LoginMaster(loginData: Interfaces.IUserLogin): ng.IPromise<Interfaces.IUserInfo>
        {
            var uri: string = this.configSettings.apiServiceBaseUri + "api/master/login";
            return this.webRequestService.Post(uri, JSON.stringify(loginData));
        }

        GetUserInfo(loginData: Interfaces.IUserLogin): ng.IPromise<Interfaces.IUserInfo>
        {
            var uri: string = this.configSettings.apiServiceBaseUri + "api/auth/userinfo";
            return this.webRequestService.Post(uri, JSON.stringify(loginData));
        }

        DecryptUserInfo(userEncrypt: string): ng.IPromise<Interfaces.IUserInfo>
        {
            var uri: string = this.configSettings.apiServiceBaseUri + "api/auth/decryptuserinfo";
            return this.webRequestService.Post(uri, JSON.stringify(userEncrypt));
        }

        SwitchRole(loginData: Interfaces.IUserLogin)
        {
            var self = this;
            self.localStorageService.remove("userInfo");
            self.localStorageService.remove("MainUserInfo");

            self.GetUserInfo(loginData)
                .then((userCover: Interfaces.IUserCover) =>
                {
                    self.localStorageService.set("userInfo", userCover.UserEncrypt);
                    userCover.UserInfoHeader.ListUserMenu = userCover.UserInfoDetail.ListUserMenu;
                    self.localStorageService.set("MainUserInfo", userCover.UserInfoHeader);
                    self.userInfo = userCover.UserInfoHeader;

                    var json = JSON.stringify(self.userInfo);
                    var refreshIntervalId = setInterval(() =>
                    {
                        self.$rootScope.$emit("hdfUpdateAllTab", json);
                        self.$rootScope.$on("AllTabResponse", (event, data: any) =>
                        {
                            if (data)
                            {
                                clearInterval(refreshIntervalId);
                            }
                        });
                    }, 500);
                });
        }

        LogOut()
        {
            var self = this;
            self.localStorageService.remove("authorizationData");
            self.localStorageService.remove("userInfo");
            self.localStorageService.remove("MainUserInfo");
            self.authentication.isAuth = false;
            self.authentication.userName = "";
            self.authentication.useRefreshTokens = false;
            this.$sessionStorage.$reset();
        }

        LogOutMain(): ng.IPromise<void>
        {
            var self = this;
            var deferred = self.$q.defer<void>();
            this.$sessionStorage.$reset();
            self.localStorageService.remove("authorizationData");
            self.localStorageService.remove("userInfo");
            self.localStorageService.remove("MainUserInfo");
            self.authentication.isAuth = false;
            self.authentication.userName = "";
            self.authentication.useRefreshTokens = false;
            var refreshIntervalId = setInterval(() =>
            {
                if (this.$sessionStorage.IdentityUserRole == undefined)
                {
                    clearInterval(refreshIntervalId);
                    deferred.resolve();
                }
            }, 100);
            return deferred.promise;
        }


        FillAuthData(menuId?: number)
        {
            var self = this;
            var deferred = self.$q.defer();
            var authData = self.localStorageService.get("authorizationData");
            if (authData)
            {
                self.authentication.isAuth = true;
                self.authentication.userName = authData.userName;
                self.authentication.useRefreshTokens = authData.useRefreshTokens;
            }


            setTimeout(() =>
            {
                deferred.resolve(self.userInfo);
                self.$rootScope.$broadcast("UserInfo");
            }, 100);

            var stringUser = self.localStorageService.get("userInfo");
            if (stringUser)
            {
                if (self.localStorageService.get("MainUserInfo"))
                {
                    self.userInfo = self.localStorageService.get("MainUserInfo");

                    setTimeout(() =>
                    {
                        deferred.resolve(self.userInfo);
                        self.$rootScope.$broadcast("UserInfo");
                    }, 100);
                }
                else
                {
                    self.DecryptUserInfo(stringUser)
                        .then((user: Interfaces.IUserInfo) =>
                        {
                            self.localStorageService.set("MainUserInfo", user);
                            self.userInfo = user;
                            deferred.resolve(user);
                        });
                }
            }
            else// case not login
            {
                // แบ่งเป็น 2 กรณีคือ เข้ามายังหน้าที่ต้อง login ก่อน และหน้าที่ไม่ต้อง login
                if (menuId) // ถ้าเป็นหน้าที่ต้องการสิทธิ์
                {
                    self.$location.url("/login");
                }
            }
            return deferred.promise;
        }

        RefreshToken()
        {
            var self = this;
            var deferred = self.$q.defer();
            var authData = self.localStorageService.get("authorizationData");

            if (authData)
            {
                if (authData.useRefreshTokens)
                {
                    var data = "grant_type=refresh_token&refresh_token=" + authData.refreshToken + "&client_id=" + self.configSettings.clientId;

                    self.localStorageService.remove("authorizationData");
                    self.$http.post<any>(self.configSettings.apiServiceBaseUri + "token", data, { headers: { "Content-Type": "application/x-www-form-urlencoded" } }).success(function (response)
                    {
                        self.localStorageService.set("authorizationData", { token: response.access_token, userName: response.userName, refreshToken: response.refresh_token, useRefreshTokens: true });
                        deferred.resolve(response);
                    }).error((err, status) =>
                    {
                        this.LogOut();
                        deferred.reject(err);
                    });
                }
            }

            return deferred.promise;
        }

        ObtainAccessToken(externalData)
        {
            var self = this;
            var deferred = self.$q.defer();

            self.$http.get<any>(self.configSettings.apiServiceBaseUri + "api/account/ObtainLocalAccessToken", { params: { provider: externalData.provider, externalAccessToken: externalData.externalAccessToken } }).success(function (response)
            {
                self.localStorageService.set("authorizationData", { token: response.access_token, userName: response.userName, refreshToken: "", useRefreshTokens: false });
                self.authentication.isAuth = true;
                self.authentication.userName = response.userName;
                self.authentication.useRefreshTokens = false;
                deferred.resolve(response);
            }).error(function (err, status)
            {
                self.LogOut();
                deferred.reject(err);
            });

            return deferred.promise;
        }

        RegisterExternal(registerExternalData)
        {
            var self = this;
            var deferred = self.$q.defer();

            self.$http.post<any>(self.configSettings.apiServiceBaseUri + "api/account/registerexternal", registerExternalData).success(function (response)
            {
                self.localStorageService.set("authorizationData", { token: response.access_token, userName: response.userName, refreshToken: "", useRefreshTokens: false });
                self.authentication.isAuth = true;
                self.authentication.userName = response.userName;
                self.authentication.useRefreshTokens = false;
                deferred.resolve(response);
            }).error(function (err, status)
            {
                self.LogOut();
                deferred.reject(err);
            });

            return deferred.promise;
        }

        public CancelLockTable(): ng.IPromise<boolean>
        {
            var uri: string = this.configSettings.apiServiceBaseUri + "api/master/LockTableLog/cancelLockTable";
            return this.webRequestService.Get(uri);
        }


        public refreshUserInfo()
        {
            var self = this;
            var deferred = self.$q.defer();
            var authData = self.localStorageService.get("authorizationData");
            if (authData)
            {
                self.authentication.isAuth = true;
                self.authentication.userName = authData.userName;
                self.authentication.useRefreshTokens = authData.useRefreshTokens;
            }

            var stringUser = self.localStorageService.get("userInfo");
            if (stringUser)
            {
                if (self.localStorageService.get("MainUserInfo"))
                {
                    self.userInfo = self.localStorageService.get("MainUserInfo");

                    setTimeout(() =>
                    {
                        deferred.resolve(self.userInfo);
                        self.$rootScope.$broadcast("UserInfo");
                    }, 100);
                }
                else
                {
                    self.DecryptUserInfo(stringUser)
                        .then((user: Interfaces.IUserInfo) =>
                        {
                            self.localStorageService.set("MainUserInfo", user);
                            self.userInfo = user;
                            deferred.resolve(user);
                        });
                }
            }

            return deferred.promise;
        }



        public LogOutSessionStable()
        {
            var self = this;
            self.localStorageService.remove("authorizationData");
            self.localStorageService.remove("userInfo");
            self.localStorageService.remove("MainUserInfo");
            self.authentication.isAuth = false;
            self.authentication.userName = "";
            self.authentication.useRefreshTokens = false;
        }

    }

    Main.App.Services.factory("authService", ["$http", "$q", "localStorageService", "$sessionStorage", "configSettings", "webRequestService", "$location", "$rootScope", AuthService.Init]);
}