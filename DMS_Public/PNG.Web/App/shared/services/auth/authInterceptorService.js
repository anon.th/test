/// <reference path="../../../../Scripts/typings/angularLocalStorage/angularLocalStorage.d.ts" />
'use strict';
var Util;
(function (Util) {
    var AuthInterceptorService = (function () {
        function AuthInterceptorService($injector, $location, $q, localStorageService, blockUI) {
            var _this = this;
            this.request = function (config) {
                var self = _this;
                config.headers = config.headers || {};
                var authData = self.localStorageService.get('authorizationFrontData');
                if (authData) {
                    config.headers.Authorization = 'Bearer ' + authData.token;
                    config.headers.ClientConnectionId = authData.clientConnectionId;
                }
                return config;
            };
            this.responseError = function (rejection) {
                var self = _this;
                if (rejection.status === 401) {
                    var authService = self.$injector.get('authService');
                    var authData = self.localStorageService.get('authorizationFrontData');
                    if (authData) {
                        if (authData.useRefreshTokens) {
                            self.$location.path('/refresh');
                            return self.$q.reject(rejection);
                        }
                    }
                    authService.LogOut();
                    self.$location.path('/login');
                }
                return self.$q.reject(rejection);
            };
            this.response = function (response) {
                var self = _this;
                if (self.blockUI.isBlocking() === true) {
                    self.blockUI.stop();
                }
                return response || _this.$q.when(response);
            };
            this.$injector = $injector;
            this.$location = $location;
            this.$q = $q;
            this.localStorageService = localStorageService;
            this.blockUI = blockUI;
        }
        AuthInterceptorService.Init = function ($injector, $location, $q, localStorageService, blockUI) {
            return new AuthInterceptorService($injector, $location, $q, localStorageService, blockUI);
        };
        return AuthInterceptorService;
    }());
    Util.AuthInterceptorService = AuthInterceptorService;
    Main.App.Services.factory('authInterceptorService', ['$injector', '$location', '$q', 'localStorageService', 'blockUI', AuthInterceptorService.Init]);
})(Util || (Util = {}));
