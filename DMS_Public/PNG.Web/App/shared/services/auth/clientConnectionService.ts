﻿/// <reference path="../../../../scripts/typings/signalr/signalr.d.ts" />
interface SignalR
{
    clientConnectionHub: IClientConnectionHubProxy;
}

interface IClientConnectionHubProxy
{
    client: ChatClient;
    server: ChatServer;
}

interface ChatClient
{
    addNewMessageToPage: (name: string, message: string) => void;
    removeLocalStorage: () => void;
    updateRoleAndMenu: (userInfo: string) => void;
}

interface ChatServer
{
    send(name: string, message: string): JQueryPromise<void>;
    updateRoleAndMenuAllTab(userInfo: string): void;
}

module Util
{
    export class ClientConnectionService
    {
        public _connection: IClientConnectionHubProxy;

        private $rootScope: ng.IRootScopeService;
        private $injector: ng.auto.IInjectorService;
        private $location: ng.ILocationService;
        private $q: ng.IQService;
        private localStorageService: ng.localStorage.ILocalStorageService;
        private $sessionStorage: any;
        private authService: Util.AuthService;

        public static Init($rootScope: ng.IRootScopeService, $injector: ng.auto.IInjectorService, $location: ng.ILocationService, $q: ng.IQService,
            localStorageService: ng.localStorage.ILocalStorageService, $sessionStorage: any,
            authService: Util.AuthService)
        {
            return new ClientConnectionService($rootScope, $injector, $location, $q, localStorageService, $sessionStorage, authService);
        }

        constructor(
            $rootScope: ng.IRootScopeService,
            $injector: ng.auto.IInjectorService,
            $location: ng.ILocationService,
            $q: ng.IQService,
            localStorageService: ng.localStorage.ILocalStorageService,
            $sessionStorage: any,
            authService: Util.AuthService)
        {
            this.$rootScope = $rootScope;
            this.$injector = $injector;
            this.$location = $location;
            this.$q = $q;
            this.localStorageService = localStorageService;
            this.$sessionStorage = $sessionStorage;
            this.authService = authService;

            $.connection.hub.logging = false;
        }

        public CreateClientConnection(authenticate?: boolean): ng.IPromise<void>
        {
            var defer = this.$q.defer<void>();

            var self = this;
            var authData = self.localStorageService.get('authorizationData');
            $.connection.hub.qs = "clientConnectionId=" + authData.clientConnectionId;

            // when from authenticate or browser tab have session storage SignalRConnectionId set the querystring is authenticate=true
            if (authenticate == true)
            {
                $.connection.hub.qs = $.connection.hub.qs + "&isauthenticated=" + authenticate;
            }

            this._connection = $.connection.clientConnectionHub;
            

            //this._connection.client.updateRoleAndMenu = (userInfo) =>
            //{
            //    this.$rootScope.$broadcast("UserInfoAllTab", userInfo);
            //};

            var that = this;
            //$.connection.hub.start().done(function ()
            //{
            //    that.$sessionStorage.SignalRConnectionId = $.connection.hub.id;

            //    that.$rootScope.$on("hdfUpdateAllTab", (event, data: any) =>
            //    {
            //        that.$rootScope.$emit("AllTabResponse", true);
            //        that._connection.server.updateRoleAndMenuAllTab(data);
            //    });
            //});

            defer.resolve();
            return defer.promise;
        }

        public Send(): JQueryPromise<void>
        {
            var that = this;
            return this._connection.server.send('testing...', 'send message from server').done(function (data)
            {
                that.$rootScope.$apply();
            });
        }

        public Stop(): void
        {
            $.connection.hub.stop();
        }
    }

    Main.App.Services.factory('clientConnectionService', ['$rootScope', '$injector', '$location', '$q', 'localStorageService', '$sessionStorage', 'authService', ClientConnectionService.Init]);
}