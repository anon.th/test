/// <reference path="../../../../scripts/typings/signalr/signalr.d.ts" />
var Util;
(function (Util) {
    var ClientConnectionService = (function () {
        function ClientConnectionService($rootScope, $injector, $location, $q, localStorageService, $sessionStorage, authService) {
            this.$rootScope = $rootScope;
            this.$injector = $injector;
            this.$location = $location;
            this.$q = $q;
            this.localStorageService = localStorageService;
            this.$sessionStorage = $sessionStorage;
            this.authService = authService;
            $.connection.hub.logging = false;
        }
        ClientConnectionService.Init = function ($rootScope, $injector, $location, $q, localStorageService, $sessionStorage, authService) {
            return new ClientConnectionService($rootScope, $injector, $location, $q, localStorageService, $sessionStorage, authService);
        };
        ClientConnectionService.prototype.CreateClientConnection = function (authenticate) {
            var defer = this.$q.defer();
            var self = this;
            var authData = self.localStorageService.get('authorizationData');
            $.connection.hub.qs = "clientConnectionId=" + authData.clientConnectionId;
            // when from authenticate or browser tab have session storage SignalRConnectionId set the querystring is authenticate=true
            if (authenticate == true) {
                $.connection.hub.qs = $.connection.hub.qs + "&isauthenticated=" + authenticate;
            }
            this._connection = $.connection.clientConnectionHub;
            //this._connection.client.updateRoleAndMenu = (userInfo) =>
            //{
            //    this.$rootScope.$broadcast("UserInfoAllTab", userInfo);
            //};
            var that = this;
            //$.connection.hub.start().done(function ()
            //{
            //    that.$sessionStorage.SignalRConnectionId = $.connection.hub.id;
            //    that.$rootScope.$on("hdfUpdateAllTab", (event, data: any) =>
            //    {
            //        that.$rootScope.$emit("AllTabResponse", true);
            //        that._connection.server.updateRoleAndMenuAllTab(data);
            //    });
            //});
            defer.resolve();
            return defer.promise;
        };
        ClientConnectionService.prototype.Send = function () {
            var that = this;
            return this._connection.server.send('testing...', 'send message from server').done(function (data) {
                that.$rootScope.$apply();
            });
        };
        ClientConnectionService.prototype.Stop = function () {
            $.connection.hub.stop();
        };
        return ClientConnectionService;
    }());
    Util.ClientConnectionService = ClientConnectionService;
    Main.App.Services.factory('clientConnectionService', ['$rootScope', '$injector', '$location', '$q', 'localStorageService', '$sessionStorage', 'authService', ClientConnectionService.Init]);
})(Util || (Util = {}));
//# sourceMappingURL=clientConnectionService.js.map