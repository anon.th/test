/// <reference path="../../../../Scripts/typings/angularLocalStorage/angularLocalStorage.d.ts" />
"use strict";
var Util;
(function (Util) {
    var AuthService = (function () {
        function AuthService($http, $q, localStorageService, $sessionStorage, configSettings, webRequestService, $location, $rootScope) {
            this.$http = $http;
            this.$q = $q;
            this.localStorageService = localStorageService;
            this.$sessionStorage = $sessionStorage;
            this.configSettings = configSettings;
            this.webRequestService = webRequestService;
            this.$location = $location;
            this.$rootScope = $rootScope;
            this.authentication = {
                isAuth: false,
                userName: "",
                useRefreshTokens: false,
                email: ""
            };
            this.externalAuthData = {
                provider: "",
                userName: "",
                externalAccessToken: ""
            };
            this.userInfo = {
                Id: "",
                UserName: "",
                FirstName: "",
                LastName: "",
                Email: "",
                RoleId: "",
                RoleCode: "",
                ListUserMenu: [],
                UserId: ""
            };
        }
        AuthService.Init = function ($http, $q, localStorageService, $sessionStorage, configSettings, webRequestService, $location, $rootScope) {
            return new AuthService($http, $q, localStorageService, $sessionStorage, configSettings, webRequestService, $location, $rootScope);
        };
        AuthService.prototype.SaveRegistration = function (registration) {
            var self = this;
            self.LogOut();
            return self.$http.post(self.configSettings.apiServiceBaseUri + "api/account/register", registration).then(function (response) {
                return response;
            });
        };
        AuthService.prototype.Login = function (loginData) {
            var _this = this;
            var self = this;
            var data = {
                Username: loginData.userName,
                Password: btoa(encodeURIComponent(loginData.password)),
                IpAddress: loginData.IpAddress,
                EnterpriseId: loginData.enterpriseid
            };
            if (loginData.useRefreshTokens) {
                data.ClientId = self.configSettings.clientId;
            }
            var deferred = self.$q.defer();
            self.LoginMaster(data)
                .then(function (response) {
                if (loginData.useRefreshTokens) {
                    self.localStorageService.set("authorizationData", {
                        token: btoa(encodeURIComponent(response.access_token)),
                        userName: loginData.userName,
                        clientConnectionId: response.ClientConnectionId,
                        forceChangePassword: response.forceChangePassword,
                        refreshToken: response.refresh_token,
                        useRefreshTokens: true,
                        userTypeId: response.UserTypeId
                    });
                }
                else {
                    self.localStorageService.set("authorizationData", {
                        token: btoa(encodeURIComponent(response.access_token)),
                        userName: loginData.userName,
                        clientConnectionId: response.ClientConnectionId,
                        refreshToken: "",
                        useRefreshTokens: false,
                        userTypeId: response.UserTypeId
                    });
                }
                self.authentication.isAuth = true;
                self.authentication.userName = loginData.userName;
                self.authentication.useRefreshTokens = loginData.useRefreshTokens;
                // console.log(self.localStorageService.get("authorizationData"));
                deferred.resolve(response);
            }).catch(function (err) {
                _this.LogOut();
                deferred.reject(err);
            });
            self.authentication.isAuth = true;
            self.authentication.userName = loginData.userName;
            self.authentication.useRefreshTokens = loginData.useRefreshTokens;
            //console.log(self.localStorageService.get("authorizationData"));
            return deferred.promise;
        };
        AuthService.prototype.LoginMaster = function (loginData) {
            var uri = this.configSettings.apiServiceBaseUri + "api/master/login";
            return this.webRequestService.Post(uri, JSON.stringify(loginData));
        };
        AuthService.prototype.GetUserInfo = function (loginData) {
            var uri = this.configSettings.apiServiceBaseUri + "api/auth/userinfo";
            return this.webRequestService.Post(uri, JSON.stringify(loginData));
        };
        AuthService.prototype.DecryptUserInfo = function (userEncrypt) {
            var uri = this.configSettings.apiServiceBaseUri + "api/auth/decryptuserinfo";
            return this.webRequestService.Post(uri, JSON.stringify(userEncrypt));
        };
        AuthService.prototype.SwitchRole = function (loginData) {
            var self = this;
            self.localStorageService.remove("userInfo");
            self.localStorageService.remove("MainUserInfo");
            self.GetUserInfo(loginData)
                .then(function (userCover) {
                self.localStorageService.set("userInfo", userCover.UserEncrypt);
                userCover.UserInfoHeader.ListUserMenu = userCover.UserInfoDetail.ListUserMenu;
                self.localStorageService.set("MainUserInfo", userCover.UserInfoHeader);
                self.userInfo = userCover.UserInfoHeader;
                var json = JSON.stringify(self.userInfo);
                var refreshIntervalId = setInterval(function () {
                    self.$rootScope.$emit("hdfUpdateAllTab", json);
                    self.$rootScope.$on("AllTabResponse", function (event, data) {
                        if (data) {
                            clearInterval(refreshIntervalId);
                        }
                    });
                }, 500);
            });
        };
        AuthService.prototype.LogOut = function () {
            var self = this;
            self.localStorageService.remove("authorizationData");
            self.localStorageService.remove("userInfo");
            self.localStorageService.remove("MainUserInfo");
            self.authentication.isAuth = false;
            self.authentication.userName = "";
            self.authentication.useRefreshTokens = false;
            this.$sessionStorage.$reset();
        };
        AuthService.prototype.LogOutMain = function () {
            var _this = this;
            var self = this;
            var deferred = self.$q.defer();
            this.$sessionStorage.$reset();
            self.localStorageService.remove("authorizationData");
            self.localStorageService.remove("userInfo");
            self.localStorageService.remove("MainUserInfo");
            self.authentication.isAuth = false;
            self.authentication.userName = "";
            self.authentication.useRefreshTokens = false;
            var refreshIntervalId = setInterval(function () {
                if (_this.$sessionStorage.IdentityUserRole == undefined) {
                    clearInterval(refreshIntervalId);
                    deferred.resolve();
                }
            }, 100);
            return deferred.promise;
        };
        AuthService.prototype.FillAuthData = function (menuId) {
            var self = this;
            var deferred = self.$q.defer();
            var authData = self.localStorageService.get("authorizationData");
            if (authData) {
                self.authentication.isAuth = true;
                self.authentication.userName = authData.userName;
                self.authentication.useRefreshTokens = authData.useRefreshTokens;
            }
            setTimeout(function () {
                deferred.resolve(self.userInfo);
                self.$rootScope.$broadcast("UserInfo");
            }, 100);
            var stringUser = self.localStorageService.get("userInfo");
            if (stringUser) {
                if (self.localStorageService.get("MainUserInfo")) {
                    self.userInfo = self.localStorageService.get("MainUserInfo");
                    setTimeout(function () {
                        deferred.resolve(self.userInfo);
                        self.$rootScope.$broadcast("UserInfo");
                    }, 100);
                }
                else {
                    self.DecryptUserInfo(stringUser)
                        .then(function (user) {
                        self.localStorageService.set("MainUserInfo", user);
                        self.userInfo = user;
                        deferred.resolve(user);
                    });
                }
            }
            else {
                // แบ่งเป็น 2 กรณีคือ เข้ามายังหน้าที่ต้อง login ก่อน และหน้าที่ไม่ต้อง login
                if (menuId) {
                    self.$location.url("/login");
                }
            }
            return deferred.promise;
        };
        AuthService.prototype.RefreshToken = function () {
            var _this = this;
            var self = this;
            var deferred = self.$q.defer();
            var authData = self.localStorageService.get("authorizationData");
            if (authData) {
                if (authData.useRefreshTokens) {
                    var data = "grant_type=refresh_token&refresh_token=" + authData.refreshToken + "&client_id=" + self.configSettings.clientId;
                    self.localStorageService.remove("authorizationData");
                    self.$http.post(self.configSettings.apiServiceBaseUri + "token", data, { headers: { "Content-Type": "application/x-www-form-urlencoded" } }).success(function (response) {
                        self.localStorageService.set("authorizationData", { token: response.access_token, userName: response.userName, refreshToken: response.refresh_token, useRefreshTokens: true });
                        deferred.resolve(response);
                    }).error(function (err, status) {
                        _this.LogOut();
                        deferred.reject(err);
                    });
                }
            }
            return deferred.promise;
        };
        AuthService.prototype.ObtainAccessToken = function (externalData) {
            var self = this;
            var deferred = self.$q.defer();
            self.$http.get(self.configSettings.apiServiceBaseUri + "api/account/ObtainLocalAccessToken", { params: { provider: externalData.provider, externalAccessToken: externalData.externalAccessToken } }).success(function (response) {
                self.localStorageService.set("authorizationData", { token: response.access_token, userName: response.userName, refreshToken: "", useRefreshTokens: false });
                self.authentication.isAuth = true;
                self.authentication.userName = response.userName;
                self.authentication.useRefreshTokens = false;
                deferred.resolve(response);
            }).error(function (err, status) {
                self.LogOut();
                deferred.reject(err);
            });
            return deferred.promise;
        };
        AuthService.prototype.RegisterExternal = function (registerExternalData) {
            var self = this;
            var deferred = self.$q.defer();
            self.$http.post(self.configSettings.apiServiceBaseUri + "api/account/registerexternal", registerExternalData).success(function (response) {
                self.localStorageService.set("authorizationData", { token: response.access_token, userName: response.userName, refreshToken: "", useRefreshTokens: false });
                self.authentication.isAuth = true;
                self.authentication.userName = response.userName;
                self.authentication.useRefreshTokens = false;
                deferred.resolve(response);
            }).error(function (err, status) {
                self.LogOut();
                deferred.reject(err);
            });
            return deferred.promise;
        };
        AuthService.prototype.CancelLockTable = function () {
            var uri = this.configSettings.apiServiceBaseUri + "api/master/LockTableLog/cancelLockTable";
            return this.webRequestService.Get(uri);
        };
        AuthService.prototype.refreshUserInfo = function () {
            var self = this;
            var deferred = self.$q.defer();
            var authData = self.localStorageService.get("authorizationData");
            if (authData) {
                self.authentication.isAuth = true;
                self.authentication.userName = authData.userName;
                self.authentication.useRefreshTokens = authData.useRefreshTokens;
            }
            var stringUser = self.localStorageService.get("userInfo");
            if (stringUser) {
                if (self.localStorageService.get("MainUserInfo")) {
                    self.userInfo = self.localStorageService.get("MainUserInfo");
                    setTimeout(function () {
                        deferred.resolve(self.userInfo);
                        self.$rootScope.$broadcast("UserInfo");
                    }, 100);
                }
                else {
                    self.DecryptUserInfo(stringUser)
                        .then(function (user) {
                        self.localStorageService.set("MainUserInfo", user);
                        self.userInfo = user;
                        deferred.resolve(user);
                    });
                }
            }
            return deferred.promise;
        };
        AuthService.prototype.LogOutSessionStable = function () {
            var self = this;
            self.localStorageService.remove("authorizationData");
            self.localStorageService.remove("userInfo");
            self.localStorageService.remove("MainUserInfo");
            self.authentication.isAuth = false;
            self.authentication.userName = "";
            self.authentication.useRefreshTokens = false;
        };
        return AuthService;
    }());
    Util.AuthService = AuthService;
    Main.App.Services.factory("authService", ["$http", "$q", "localStorageService", "$sessionStorage", "configSettings", "webRequestService", "$location", "$rootScope", AuthService.Init]);
})(Util || (Util = {}));
//# sourceMappingURL=authService.js.map