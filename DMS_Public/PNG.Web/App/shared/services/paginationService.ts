﻿module Util
{
    export class Pagination
    {
        public static Factory()
        {
            return new Pagination();
        }

        public PageLinks: Interfaces.IPageLink[];
        public RecordsPerPage: number;
        public TotalCount: number;
        public TotalPages: number;
        public CurrentPageId: number;
        public CurrentSkip: number;
        public UrlPrefix: string;


        constructor()
        {
        }

        public CreatePageLinks(totalRecordCount: number, recordsPerPage: number, urlPrefix: string, skip: number, currentPageId: number, urlPostFix: string = ""): void
        {
            //instantiate new page links
            this.PageLinks = [];

            this.TotalCount = totalRecordCount;
            this.RecordsPerPage = recordsPerPage;
            this.TotalPages = Math.ceil(this.TotalCount / this.RecordsPerPage);
            this.UrlPrefix = urlPrefix;
            this.CurrentPageId = currentPageId;
            this.CurrentSkip = skip;

            //create tne next and back skip intervals 
            var nextSkipInterval: number = (this.CurrentSkip + this.RecordsPerPage);
            var backSkipInterval: number = (this.CurrentSkip - this.RecordsPerPage);

            //create the new page Ids for back and next links
            var backPageId: number = this.CurrentPageId - 1;
            var nextPageId: number = this.CurrentPageId + 1;

            var i: number = 0;

            //make sure we need to generate the links
            if (!(this.RecordsPerPage >= this.TotalCount))
            {
                //make back link
                this.PageLinks.push(
                    {
                        Id: i,
                        Label: "<<",
                        Link: this.CurrentSkip <= 0 ? "" : this.UrlPrefix + "page/" + backPageId + "/skip/" + backSkipInterval + "/" + urlPostFix,
                        Current: false,
                        Disabled: this.CurrentSkip <= 0 ? true : false
                    });

                i = i + 1;

                //make numbered links between back and next
                var individualSkip: number = 0;

                while (i <= this.TotalPages)
                {
                    this.PageLinks.push(
                        {
                            Id: i,
                            Label: i.toString(),
                            Link: this.UrlPrefix + "page/" + i + "/skip/" + individualSkip + "/" + urlPostFix,
                            Current: i == this.CurrentPageId ? true : false,
                            Disabled: false
                        });

                    individualSkip = individualSkip + this.RecordsPerPage;
                    i++;
                }

                //make next link
                this.PageLinks.push(
                    {
                        Id: i,
                        Label: ">>",
                        Link: nextSkipInterval >= this.TotalCount ? "" : this.UrlPrefix + "page/" + nextPageId + "/skip/" + nextSkipInterval + "/" + urlPostFix,
                        Current: false,
                        Disabled: nextSkipInterval >= this.TotalCount ? true : false
                    });
            }
        }


        public CreateLinks(totalRecordCount: number, recordsPerPage: number, urlPrefix: string, skip: number, pageId: number, urlPostFix: string = ""): string
        {
            return urlPrefix + "page/" + pageId + "/skip/" + skip + "/" + urlPostFix;
        }

        public Clear(): void
        {
            this.PageLinks = [];
            this.RecordsPerPage = null;
            this.TotalCount = null;
            this.TotalPages = null;
            this.CurrentPageId = null;
            this.CurrentSkip = null;
            this.UrlPrefix = null;
        }
    }

    Main.App.Services.factory("paginationService", [Pagination.Factory]);
}