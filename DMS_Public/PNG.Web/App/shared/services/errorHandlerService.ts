﻿module Shared
{
    export class ErrorHandlerService
    {
        public static Factory(
            $rootScope: ng.IScope,
            log: Shared.LoggingService,
            notify: Shared.NotificationService,
            $filter: any,
            dialogHelperService: Shared.DialogHelperService,
            $location: any)
        {
            return new ErrorHandlerService($rootScope, log, notify, $filter, dialogHelperService, $location);
        }

        constructor(
            private $rootScope: ng.IScope,
            private log: Shared.LoggingService,
            private notify: Shared.NotificationService,
            private $filter: any,
            private dialogHelperService: Shared.DialogHelperService,
            private $location: any)
        {
        }

        public HandleException(error: any, location: string, option?: any)
        {
            // remove config from the error instance, to avoid circular reference - TODO: find a  better way to do this
            error = error || <any>{ "data": "supplied error was null" };
            error.config = null;

            var options: ToastrOptions = {
                tapToDismiss: false,
                closeButton: true,
                timeOut: 0,
                extendedTimeOut: 0
            };

            var exceptionMessage: string = "";
            var summaryExteptionMessage: string = "";

            var listExceptionMessage: any;

            if (error.data)
            {
                if (error.data.ExceptionMessage)
                {
                    try
                    {
                        //case custom error 500 return object
                        listExceptionMessage = JSON.parse(error.data.ExceptionMessage);

                        //case custom error single
                        if (!listExceptionMessage[0].Status)
                        {
                            exceptionMessage = listExceptionMessage[0];
                        }
                    }
                    catch (e)
                    {
                        //case nature error 500 return string
                        exceptionMessage = error.data.ExceptionMessage;
                    }
                }
            }
            else if (error.error)
            {
                exceptionMessage = error.error_description.toString();
            }

            if (exceptionMessage.search("Session token expired or not valid") != -1)
            {
                this.$rootScope.$broadcast("Unauthorized");
                return;
            }

            var errorExplain = "";//this.$translate.instant('MessageerrorExplain') + " : "; //fixed bug msg err duplicate(3174)

            if (exceptionMessage)
            {
                if (exceptionMessage.toUpperCase().search("THE USER NAME OR PASSWORD IS INCORRECT.") != -1)
                {
                    summaryExteptionMessage = errorExplain + 'MessageLoginError';
                }

               

                this.log.LogErrorOnly(summaryExteptionMessage);  // Just incase it is configured to be noisy
                this.dialogHelperService.ShowMessage("MessageErrorExplan", summaryExteptionMessage);
            }
            else if (listExceptionMessage)
            {
                if (listExceptionMessage.length > 0)
                {
                    summaryExteptionMessage = "";
                    var errorCount: number = 0;
                    var successCount: number = 0;
                    var contributionErrorCount: number = 0;
                    var memberFundErrorCount: number = 0;
                    listExceptionMessage.forEach(item =>
                    {
                        if (item.Message && item.Message.toUpperCase().search("CONFLICTED WITH THE REFERENCE") > -1)
                        {
                            summaryExteptionMessage += "<br/>  - " + item.FieldName;
                            errorCount++;
                        }
                        else if (item.Message && item.Message.toUpperCase().search("CONFLICTED WITH THE SAME TABLE REFERENCE") > -1)
                        {
                            summaryExteptionMessage += "<br/>  - " + item.FieldName;
                            errorCount++;
                        }
                        else
                        {
                            successCount++;
                        }
                    });
                    this.log.LogErrorOnly(summaryExteptionMessage);  // Just incase it is configured to be noisy

                    if (successCount > 0)
                    {
                        summaryExteptionMessage = "Success: " + successCount + " " + 'Common.List' + "<br/>" + 'MessageDeleteListErrorReference' + summaryExteptionMessage;
                    }

                    this.dialogHelperService.ShowMessage("MessageErrorExplan", summaryExteptionMessage);
                }
            }
        }

        public ShowErrorValidate(validationErrors: Validation.ValidationError[], title?: string)
        {
            // ดึงค่าแรกมาว่ามี ErrorOrder หรือไม่
            // ถ้ามีให้ Sort ตาม ErrorOrder
            if (validationErrors[0].ErrorOrder && validationErrors[0].ErrorOrder > 0)
            {
                validationErrors = this.$filter('orderBy')(validationErrors, 'ErrorOrder');
            }

            var messageErrors: string = validationErrors.map(v => !v.Argument1 ? v.Error : v.Error.format(v.Argument1, v.Argument2)).join(Shared.PvdConstant.NewLine);
            this.dialogHelperService.ShowMessage((title && title.length) ? title : "MessageErrorValidate", messageErrors);
        }

        public ShowErrorValidateCustom(validationErrors: Validation.ValidationError[], title?: string)
        {
            // ดึงค่าแรกมาว่ามี ErrorOrder หรือไม่
            // ถ้ามีให้ Sort ตาม ErrorOrder
            if (validationErrors[0].ErrorOrder && validationErrors[0].ErrorOrder > 0)
            {
                validationErrors = this.$filter('orderBy')(validationErrors, 'ErrorOrder');
            }

            var messageErrors: string = validationErrors.map(v => !v.Argument1 ? " - " + v.Error : v.Error.format(v.Argument1, v.Argument2)).join(Shared.PvdConstant.NewLine);
            this.dialogHelperService.ShowMessageCustom((title && title.length) ? title : "MessageErrorValidate", messageErrors);
        }

        public ShowSingleErrorValidate(validationErrors: Validation.ValidationError)
        {
            var messageErrors: string = validationErrors.Error;
            this.dialogHelperService.ShowMessage("MessageErrorValidate", messageErrors);
        }


        public translateMessage(values: string[]): string
        {
            var message: string = PvdConstant.Empty;
            for (var index = 0; index < values.length; index++)
            {
                if (index == 0)
                    message = values[index];
                else
                    message += PvdConstant.Blank + values[index];
            }
            return message;
        }

        public translateExceptionMessageList(values: Interfaces.IExceptionMessage[]): string
        {
            var allMessage: string = PvdConstant.Empty;
            var message: string = PvdConstant.Empty;
            var firstError: number = 0;

            values.forEach(value =>
            {
                for (var index = 0; index < value.TranslationMessage.length; index++)
                {
                    if (index == 0)
                        message = value.TranslationMessage[index];
                    else
                        message += PvdConstant.Blank + value.TranslationMessage[index];
                }
                if (firstError == 0)
                {
                    allMessage = "</br> - " + message;
                    firstError++;
                }
                else
                {
                    allMessage += "</br> - " + message;
                }
            });
            return allMessage;
        }
    }

    Main.App.Services.factory("errorHandlerService", ["$rootScope", "loggingService", "notificationService", "$filter", "dialogHelperService", "$location", ErrorHandlerService.Factory]);
}