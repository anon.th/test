var Shared;
(function (Shared) {
    var ErrorHandlerService = (function () {
        function ErrorHandlerService($rootScope, log, notify, $filter, dialogHelperService, $location) {
            this.$rootScope = $rootScope;
            this.log = log;
            this.notify = notify;
            this.$filter = $filter;
            this.dialogHelperService = dialogHelperService;
            this.$location = $location;
        }
        ErrorHandlerService.Factory = function ($rootScope, log, notify, $filter, dialogHelperService, $location) {
            return new ErrorHandlerService($rootScope, log, notify, $filter, dialogHelperService, $location);
        };
        ErrorHandlerService.prototype.HandleException = function (error, location, option) {
            // remove config from the error instance, to avoid circular reference - TODO: find a  better way to do this
            error = error || { "data": "supplied error was null" };
            error.config = null;
            var options = {
                tapToDismiss: false,
                closeButton: true,
                timeOut: 0,
                extendedTimeOut: 0
            };
            var exceptionMessage = "";
            var summaryExteptionMessage = "";
            var listExceptionMessage;
            if (error.data) {
                if (error.data.ExceptionMessage) {
                    try {
                        //case custom error 500 return object
                        listExceptionMessage = JSON.parse(error.data.ExceptionMessage);
                        //case custom error single
                        if (!listExceptionMessage[0].Status) {
                            exceptionMessage = listExceptionMessage[0];
                        }
                    }
                    catch (e) {
                        //case nature error 500 return string
                        exceptionMessage = error.data.ExceptionMessage;
                    }
                }
            }
            else if (error.error) {
                exceptionMessage = error.error_description.toString();
            }
            if (exceptionMessage.search("Session token expired or not valid") != -1) {
                this.$rootScope.$broadcast("Unauthorized");
                return;
            }
            var errorExplain = ""; //this.$translate.instant('MessageerrorExplain') + " : "; //fixed bug msg err duplicate(3174)
            if (exceptionMessage) {
                if (exceptionMessage.toUpperCase().search("THE USER NAME OR PASSWORD IS INCORRECT.") != -1) {
                    summaryExteptionMessage = errorExplain + 'MessageLoginError';
                }
                this.log.LogErrorOnly(summaryExteptionMessage); // Just incase it is configured to be noisy
                this.dialogHelperService.ShowMessage("MessageErrorExplan", summaryExteptionMessage);
            }
            else if (listExceptionMessage) {
                if (listExceptionMessage.length > 0) {
                    summaryExteptionMessage = "";
                    var errorCount = 0;
                    var successCount = 0;
                    var contributionErrorCount = 0;
                    var memberFundErrorCount = 0;
                    listExceptionMessage.forEach(function (item) {
                        if (item.Message && item.Message.toUpperCase().search("CONFLICTED WITH THE REFERENCE") > -1) {
                            summaryExteptionMessage += "<br/>  - " + item.FieldName;
                            errorCount++;
                        }
                        else if (item.Message && item.Message.toUpperCase().search("CONFLICTED WITH THE SAME TABLE REFERENCE") > -1) {
                            summaryExteptionMessage += "<br/>  - " + item.FieldName;
                            errorCount++;
                        }
                        else {
                            successCount++;
                        }
                    });
                    this.log.LogErrorOnly(summaryExteptionMessage); // Just incase it is configured to be noisy
                    if (successCount > 0) {
                        summaryExteptionMessage = "Success: " + successCount + " " + 'Common.List' + "<br/>" + 'MessageDeleteListErrorReference' + summaryExteptionMessage;
                    }
                    this.dialogHelperService.ShowMessage("MessageErrorExplan", summaryExteptionMessage);
                }
            }
        };
        ErrorHandlerService.prototype.ShowErrorValidate = function (validationErrors, title) {
            // ดึงค่าแรกมาว่ามี ErrorOrder หรือไม่
            // ถ้ามีให้ Sort ตาม ErrorOrder
            if (validationErrors[0].ErrorOrder && validationErrors[0].ErrorOrder > 0) {
                validationErrors = this.$filter('orderBy')(validationErrors, 'ErrorOrder');
            }
            var messageErrors = validationErrors.map(function (v) { return !v.Argument1 ? v.Error : v.Error.format(v.Argument1, v.Argument2); }).join(Shared.PvdConstant.NewLine);
            this.dialogHelperService.ShowMessage((title && title.length) ? title : "MessageErrorValidate", messageErrors);
        };
        ErrorHandlerService.prototype.ShowErrorValidateCustom = function (validationErrors, title) {
            // ดึงค่าแรกมาว่ามี ErrorOrder หรือไม่
            // ถ้ามีให้ Sort ตาม ErrorOrder
            if (validationErrors[0].ErrorOrder && validationErrors[0].ErrorOrder > 0) {
                validationErrors = this.$filter('orderBy')(validationErrors, 'ErrorOrder');
            }
            var messageErrors = validationErrors.map(function (v) { return !v.Argument1 ? " - " + v.Error : v.Error.format(v.Argument1, v.Argument2); }).join(Shared.PvdConstant.NewLine);
            this.dialogHelperService.ShowMessageCustom((title && title.length) ? title : "MessageErrorValidate", messageErrors);
        };
        ErrorHandlerService.prototype.ShowSingleErrorValidate = function (validationErrors) {
            var messageErrors = validationErrors.Error;
            this.dialogHelperService.ShowMessage("MessageErrorValidate", messageErrors);
        };
        ErrorHandlerService.prototype.translateMessage = function (values) {
            var message = Shared.PvdConstant.Empty;
            for (var index = 0; index < values.length; index++) {
                if (index == 0)
                    message = values[index];
                else
                    message += Shared.PvdConstant.Blank + values[index];
            }
            return message;
        };
        ErrorHandlerService.prototype.translateExceptionMessageList = function (values) {
            var allMessage = Shared.PvdConstant.Empty;
            var message = Shared.PvdConstant.Empty;
            var firstError = 0;
            values.forEach(function (value) {
                for (var index = 0; index < value.TranslationMessage.length; index++) {
                    if (index == 0)
                        message = value.TranslationMessage[index];
                    else
                        message += Shared.PvdConstant.Blank + value.TranslationMessage[index];
                }
                if (firstError == 0) {
                    allMessage = "</br> - " + message;
                    firstError++;
                }
                else {
                    allMessage += "</br> - " + message;
                }
            });
            return allMessage;
        };
        return ErrorHandlerService;
    }());
    Shared.ErrorHandlerService = ErrorHandlerService;
    Main.App.Services.factory("errorHandlerService", ["$rootScope", "loggingService", "notificationService", "$filter", "dialogHelperService", "$location", ErrorHandlerService.Factory]);
})(Shared || (Shared = {}));
//# sourceMappingURL=errorHandlerService.js.map