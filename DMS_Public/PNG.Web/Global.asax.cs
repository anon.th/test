﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using PNG.Common.Services.Logging;

namespace PNG.Web
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
           

            Log.Info("PNG.Web Starting...");
            // JavaScript Minifier/Compressor will process (*.js and *.css), if value is "true"
            var minify = (HttpContext.Current != null) && HttpContext.Current.IsDebuggingEnabled;
            BundleConfig.RegisterBundles(BundleTable.Bundles, !minify);

            // To remove X-AspNetMvc-Version
            MvcHandler.DisableMvcResponseHeader = true;
        }
    }
}
