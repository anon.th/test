﻿using PNG.Common.Enums;
using PNG.Common.Enums.Common;
using PNG.Common.Helpers;
using SimpleInjector;
using System;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace PNG.Web
{
    public class ApiProxyHandler : DelegatingHandler
    {
        protected readonly string _baseApi;
        private readonly Container _container;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="container"></param>
        public ApiProxyHandler(Container container)
        {
            this._container = container;

            _baseApi = ConfigurationHelper.GetAppSettingValue<string>(AppConfigurationCode.baseApi.GetAttributeCode());
            _baseApi = _baseApi.TrimEnd(new[] { '/' });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            if (request != null)
            {
                //// Add CurrentClientConnectionId from FrontOffice.Web to validate at FrontOffice.API
                //if ((WebApiHelper.Connections != null) && (WebApiHelper.Connections.Count() > 0))
                //{
                //    var connection = WebApiHelper.Connections.FirstOrDefault();

                //    if ((connection != null) && !request.Headers.Contains("Client-Connection-Id"))
                //    {
                //        request.Headers.Add("Client-Connection-Id", connection.ClientConnectionId);
                //    }
                //}
            }

            var url = string.Empty;

            // Build new url when have more than 1 query string such as api/translations?lang=th-TH
            foreach (var parameter in request.GetQueryStrings())
            {
                url = url + string.Format("/{0}/{1}", parameter.Key, parameter.Value);
            }

            url = request.RequestUri.AbsolutePath + url;
            url = url.TrimStart(new[] { '/' });

            var forwardUrl = string.Format("{0}/{1}", _baseApi, url);
            var forwardUri = new UriBuilder(forwardUrl);

            // Explicity set Content to null - to avoid protocol violation
            if (request.Method == HttpMethod.Get)
            {
                request.Content = null;
            }

            // Send it on to the requested URL
            request.RequestUri = forwardUri.Uri;

            // Response message from Web API
            HttpResponseMessage response = null;

            try
            {
                response = await base.SendAsync(request, cancellationToken);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex.InnerException);
            }

            return response;
        }
    }
}
