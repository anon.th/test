﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="WebApiHelper.cs" company="Aware Corporation Limited">
//   Copyright (c) 2014-2015.  All rights reserved.
// </copyright>
// <summary>
//   Defines the WebApiHelper type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using Newtonsoft.Json;
using PNG.Common.Enums.Common;
using PNG.Common.Helpers;
using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace PNG.Web
{
    public static class WebApiHelper
    {

        /// <summary>
        /// 
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>

      

        /// <summary>
        /// 
        /// </summary>
        public static List<ClientConnection> Connections = new List<ClientConnection>();
 
    }

    public class ClientConnection
    {
        public string ClientConnectionId { get; set; }
        public List<SignalRConnection> SignalRConnections { get; set; }
    }

    public class SignalRConnection
    {
        public string SignalRConnectionId { get; set; }
    }
}