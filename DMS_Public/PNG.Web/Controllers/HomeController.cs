﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using PNG.Common.Helpers;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;

namespace PNG.Web.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Welcome()
        {
            return PartialView();
        }


        public ActionResult Login()
        {
            return PartialView();
        }

        public ActionResult Default()
        {
            return PartialView();
        }


        public ActionResult ResetPassword()
        {
            return PartialView();
        }

        public ActionResult UserProfile()
        {
            return PartialView();
        }

        public ActionResult SearchConsignment()
        {
            return PartialView();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ActionResult Settings()
        {
            // set version
            Assembly assembly = Assembly.GetExecutingAssembly();
            FileVersionInfo fvi = FileVersionInfo.GetVersionInfo(assembly.Location);
            string version = fvi.ProductVersion;

            var settings = new SettingsDto
            {
                apiServiceBaseUri = ConfigurationHelper.GetAppSetting("appServiceUrl"),
                uploadFileSize = ConfigurationHelper.GetAppSetting("uploadFileSize"),
                baseApi = ConfigurationHelper.GetAppSetting("baseApi")
            };

            var serializerSettings = new JsonSerializerSettings
            {
                ContractResolver = new DefaultContractResolver()
            };

            var settingsJson = JsonConvert.SerializeObject(settings, Formatting.Indented, serializerSettings);
            var script = "angular.module('app.settings', []).constant('configSettings', " + settingsJson + ");";
            return this.JavaScript(script);
        }

        public class SettingsDto
        {
            /// <summary>
            /// 
            /// </summary>
            public string baseApi { get; set; }

            /// <summary>
            /// 
            /// </summary>
            public string apiServiceBaseUri { get; set; }

            /// <summary>
            /// 
            /// </summary>
            public string uploadFileSize { get; set; }
        }
    }
}