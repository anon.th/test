﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PNG.Web.Controllers
{
    public class ShipmentTrackingController : Controller
    {
        // GET: CreateConsignment
        public ActionResult Index()
        {
            return PartialView();
        }
         

        public ActionResult LovPayerCode()
        {
            return PartialView();
        }

        public ActionResult LovStateCode()
        {
            return PartialView();
        }

        public ActionResult LovZipCode()
        {
            return PartialView();
        }

        public ActionResult LovStatusCode()
        {
            return PartialView();
        }

        public ActionResult LovExceptionCode()
        {
            return PartialView();
        }


    }
}