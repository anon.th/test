﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for DBConnection
/// </summary>
 public   class DBConnection
{
     private DB_DMS_OTRDataContext _SharedContext;

     public DB_DMS_OTRDataContext SharedContext
    {
        get { return _SharedContext; }
        set { _SharedContext = value; }
    }


    public void BeginTransaction()
    {
        System.Data.Common.DbTransaction trans;

        try
        {
            using (SharedContext)
            {
                if (SharedContext != null)
                {
                    if (SharedContext.Connection.State != ConnectionState.Closed)
                    {
                        SharedContext.Connection.Close();
                    }
                    SharedContext = null;
                }
            }
            SharedContext = new DB_DMS_OTRDataContext();
            SharedContext.Connection.Open();
            SharedContext.DeferredLoadingEnabled = false;
            trans = SharedContext.Connection.BeginTransaction();
            SharedContext.Transaction = trans;

        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }



    public void CloseDBContext()
    {
        try
        {
            using (SharedContext)
            {
                if (SharedContext != null)
                {
                    if (SharedContext.Connection.State != ConnectionState.Closed)
                    {

                        SharedContext.Connection.Close();
                    }
                }

                if (SharedContext.Transaction != null)
                {
                    SharedContext.Transaction.Dispose();

                }

                SharedContext = null;
            }

        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    public void SubmitChangeContext()
    {
        try
        {
            if (SharedContext != null)
            {
                SharedContext.SubmitChanges();
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);

        }
    }

    public void OpenConnection()
    {
        //System.Data.Common.DbTransaction trans;

        try
        {
            using (SharedContext)
            {
                if (SharedContext != null)
                {
                    if (SharedContext.Connection.State != ConnectionState.Closed)
                    {
                        SharedContext.Connection.Close();
                    }
                    SharedContext = null;
                }
            }
            SharedContext = new DB_DMS_OTRDataContext();
            SharedContext.Connection.Open();
            SharedContext.DeferredLoadingEnabled = false;
            //trans = SharedContext.Connection.BeginTransaction();
            //SharedContext.Transaction = trans;

        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    
    public void CommitTransaction()
    {
        try
        {
            if (SharedContext != null)
            {
                SharedContext.Transaction.Commit();
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }

    }

    public void RollbackTransaction()
    {
        try
        {
            if (SharedContext != null)
            {

                SharedContext.Transaction.Rollback();
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    } 
}