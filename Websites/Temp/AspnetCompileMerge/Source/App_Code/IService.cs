﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

// NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService" in both code and config file together.

[ServiceContract]
public interface IService
{

    [OperationContract]
    [WebInvoke(//UriTemplate = "ValidateLogin"
        UriTemplate = "ValidateLogin"
        , Method = "POST"
        , ResponseFormat = WebMessageFormat.Json
        , BodyStyle = WebMessageBodyStyle.WrappedRequest
        )]

    Status ValidateLogin(string enterpriseID, string phoneNo, string imei, string userID, string userPwd);

    [OperationContract]
    [WebInvoke(UriTemplate = "CapturePOD"
        , Method = "POST"
        , ResponseFormat = WebMessageFormat.Json
        , BodyStyle = WebMessageBodyStyle.WrappedRequest)]
    Status CapturePOD(string enterpriseID, string phoneNo, string imei, string userID, string userPwd, string statusDT, string consignmentNo, string consignee);

    [OperationContract]
    [WebInvoke(UriTemplate = "CapturePODWithSignature"
          , Method = "POST"
          , ResponseFormat = WebMessageFormat.Json
          , BodyStyle = WebMessageBodyStyle.WrappedRequest)]
    Status CapturePODWithSignature(string enterpriseID, string phoneNo, string imei, string userID, string userPwd, string statusDT, string consignmentNo, string consignee, string fileBytes);


    [OperationContract]
    [WebInvoke(UriTemplate = "CaptureOther"
            , Method = "POST"
            , ResponseFormat = WebMessageFormat.Json
            , BodyStyle = WebMessageBodyStyle.WrappedRequest)]
    Status CaptureOther(string enterpriseID, string phoneNo, string imei, string userID, string userPwd, string statusDT, string consignmentNo, string status_code, string remark);

    [OperationContract]
    [WebInvoke(UriTemplate = "StatusCode_Read"
        , Method = "POST"
        , ResponseFormat = WebMessageFormat.Json
        , BodyStyle = WebMessageBodyStyle.WrappedRequest)]
    MobileApp_StatusCodesList StatusCode_Read();

    [OperationContract]
    [WebInvoke(UriTemplate = "WriteLogOnRuntime"
        , Method = "POST"
        , ResponseFormat = WebMessageFormat.Json
        , BodyStyle = WebMessageBodyStyle.WrappedRequest)]
    Status WriteLogOnRuntime(string imei, string EventName, string MethodName, string enterpriseID, string phoneNo
        , string userID, string userPwd, string statusDT, string consignmentNo, string consignee, string status_code, string remark);

    [OperationContract]
    [WebInvoke(UriTemplate = "UninstallProcessLog"
        , Method = "POST"
        , ResponseFormat = WebMessageFormat.Json
        , BodyStyle = WebMessageBodyStyle.WrappedRequest)]
    Status UninstallProcessLog(string enterpriseID, string phoneNo, string imei
     , string userID, string userPwd, string statusDT
    , string consignmentNo, string consignee, string status_code, string remark, string module_name);

    [OperationContract]
    [WebInvoke(UriTemplate = "UploadToTempFolder"
        , Method = "POST"
        , ResponseFormat = WebMessageFormat.Json
        , BodyStyle = WebMessageBodyStyle.WrappedRequest)]
    bool UploadToTempFolder(byte[] pFileBytes, string pFileName);

    [OperationContract]
    [WebInvoke(UriTemplate = "ScanShippingList"
        , Method = "POST"
        , ResponseFormat = WebMessageFormat.Json
        , BodyStyle = WebMessageBodyStyle.WrappedRequest)]
    MobileApp_ConsignmentsList ScanShippingList(string enterpriseID, string phoneNo, string imei, string userID, string userPwd, string statusDT, string shippingListNo);
}

// Use a data contract as illustrated in the sample below to add composite types to service operations.
[DataContract]
public class ProcessResults
{
    int _errorCode = -1;
    string _errorMessage = null;

    [DataMember]
    public int errorCode
    {
        get { return _errorCode; }
        set { _errorCode = value; }
    }

    [DataMember]
    public string errorMessage
    {
        get { return _errorMessage; }
        set { _errorMessage = value; }
    }
}

[DataContract]
public class Status
{

    ProcessResults _status = new ProcessResults();

    [DataMember]
    public ProcessResults status
    {
        get { return _status; }
        set { _status = value; }
    }


}

[DataContract]
public class MobileApp_StatusCodes
{
    [DataMember]
    public string status_code { get; set; }
}

[DataContract]
public class MobileApp_Consignments
{
    [DataMember]
    public string enterprise_name { get; set; }
    [DataMember]
    public string enterprise_address { get; set; }
    [DataMember]
    public string enterprise_telephone { get; set; }
    [DataMember]
    public string custid { get; set; }
    [DataMember]
    public string customer_address { get; set; }
    [DataMember]
    public string customer_zipcode { get; set; }
    [DataMember]
    public string customer_telephone { get; set; }
    [DataMember]
    public string sender_name { get; set; }
    [DataMember]
    public string sender_address { get; set; }
    [DataMember]
    public string sender_zipcode { get; set; }
    [DataMember]
    public string sender_telephone { get; set; }
    [DataMember]
    public string consignment_no { get; set; }
    [DataMember]
    public decimal? total_packages { get; set; }
    [DataMember]
    public decimal? total_wt { get; set; }
    [DataMember]
    public string ref_no { get; set; }
    [DataMember]
    public string service_code { get; set; }
    [DataMember]
    public string recipient_telephone { get; set; }
    [DataMember]
    public string recipient_name { get; set; }
    [DataMember]
    public string recipient_address { get; set; }
    [DataMember]
    public string recipient_zipcode { get; set; }
    [DataMember]
    public string state_name { get; set; }
    [DataMember]
    public string ShippingList_No { get; set; }
    [DataMember]
    public string cust_name { get; set; }
    [DataMember]
    public string GoodsDescription { get; set; }
    [DataMember]
    public string cost_centre { get; set; }

}

[CollectionDataContract]
public class MobileApp_StatusCodesList : List<MobileApp_StatusCodes>
{
    public MobileApp_StatusCodesList() { }
    public MobileApp_StatusCodesList(List<MobileApp_StatusCodes> MobileApp_StatusCodesList) : base(MobileApp_StatusCodesList) { }
}

[CollectionDataContract]
public class MobileApp_ConsignmentsList : List<MobileApp_Consignments>
{
    public MobileApp_ConsignmentsList() { }
    public MobileApp_ConsignmentsList(List<MobileApp_Consignments> MobileApp_ConsignmentsList) : base(MobileApp_ConsignmentsList) { }
}