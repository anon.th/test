﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for ApplicationConstant
/// </summary>
///

    public static class ApplicationConstant
    {

        public static string FORMAT_FILE = "{0}_DDMMyyyy_HHmmss";
        public static string FORMAT_DATE = "ddMMyyyy_HHmmss";
        public static string FORMAT_EXCEL_XLSX_TYPE = ".xlsx";
        public static string FORMAT_TXT_TYPE = ".txt";
        public static string FORMAT_EXCEL_XLS_TYPE = ".xls";
        public static string FORMAT_DATE_TIME = "yyyy-MM-dd HH:mm:ss";
        public static string FORMAT_DATE_YYYY_MM_DD = "yyyy-MM-dd";
        public static string FORMAT_DATE_YYYYMMDD = "yyyyMMdd";
        

        //LOG
        public const string LOG_PREFIX_IN = "IN:{0}";
        public const string LOG_PREFIX_OUT = "OUT:{0}";
        public const string LOG_PREFIX_ACCESS = "ACCESS:{0}";

       
        public static string FLAG_YES = "Yes";
        public static string FLAG_NO = "No";
        public static string FLAG_YES_CODE = "Y";
        public static string FLAG_NO_CODE = "N";

       

    }
