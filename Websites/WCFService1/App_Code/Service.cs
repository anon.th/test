﻿using log4net;
using System;
using System.IO;
using System.Reflection;
using System.ServiceModel.Activation;

// NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service" in code, svc and config file together.


[AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
public class Service : IService
{

    private static string gloClassName = MethodBase.GetCurrentMethod().DeclaringType.Name;
    private static readonly ILog gloLog = LogManager.GetLogger(gloClassName);

    public ProcessResults Test(string id)
    {
        return new ProcessResults();
    }

    public Status ValidateLogin(string enterpriseID, string phoneNo, string imei, string userID, string userPwd)
    {
        gloLog.Info(string.Format(ApplicationConstant.LOG_PREFIX_IN, "ValidateLogin"));
        ProcessResults rtnResult = null;
      //  string rtnProcess = "";
        Status status = null;
        try
        {
            rtnResult = new ProcessResults();
            status = new Status();
            //Passwordis
            byte bytePassworsIs = 0;
            string strPasswordis = System.Configuration.ConfigurationManager.AppSettings["Passwordis"].ToString();
            gloLog.Info(string.Format(ApplicationConstant.LOG_PREFIX_ACCESS, "Config Passwordis =" + strPasswordis));
            if (!string.IsNullOrEmpty(strPasswordis) && strPasswordis.Equals("0"))
            {
                bytePassworsIs = 1;
            }

            //decrypt password

            string decryptPassword = "";
            try
            {
                bool boolHashing = false;
                string key = System.Configuration.ConfigurationManager.AppSettings["HashKey"].ToString();
                string UseHashing = System.Configuration.ConfigurationManager.AppSettings["UseHashing"].ToString();
                if (!string.IsNullOrEmpty(UseHashing) && UseHashing.Equals("1"))
                {
                    boolHashing = true;
                }
                decryptPassword = DMSDecrypt.DMSDecryptHelper.Decrypt(userPwd, key, boolHashing);
            }
            catch (Exception e)
            {
                gloLog.Error(e.StackTrace);
            }

            //rowMessage
            string format = System.Configuration.ConfigurationManager.AppSettings["Rawmessage-format"].ToString();
            string strRawmessage = string.Format(format, "enterpriseID", enterpriseID);
            strRawmessage += string.Format(format, "phoneNo", phoneNo);
            strRawmessage += string.Format(format, "imei", imei);
            strRawmessage += string.Format(format, "userID", userID);
            strRawmessage += string.Format(format, "userPwd", userPwd);

            if (!string.IsNullOrEmpty(strRawmessage))
                strRawmessage = strRawmessage.Substring(0, strRawmessage.Length - 1);

            gloLog.Info(string.Format(ApplicationConstant.LOG_PREFIX_ACCESS, "Rawmessage =" + strRawmessage));


            //param object
            ValidateDto param = new ValidateDto();
            param.Enterpriseid = enterpriseID;
            param.Phoneno = phoneNo;
            param.Imei = imei;
            param.Userid = userID;
            param.User_Password = decryptPassword;
            param.Rawmessage = strRawmessage;
            param.Passwordis = bytePassworsIs;

            MobileAppImpl mobileImpl = new MobileAppImpl();
            ReturnErrorsDto rcvValidate = mobileImpl.ValidateLogin(param);
            if (rcvValidate != null)
            {
                rtnResult.errorCode = rcvValidate.ErrorCode;
                rtnResult.errorMessage = rcvValidate.ErrorMessage;

                gloLog.Info(string.Format(ApplicationConstant.LOG_PREFIX_ACCESS, "errorCode =" + rtnResult.errorCode));
                gloLog.Info(string.Format(ApplicationConstant.LOG_PREFIX_ACCESS, "errorMessage =" + rtnResult.errorMessage));
            }


        }
        catch (Exception ex)
        {
            gloLog.Error(ex.Message);
            DMSService.WebUtils.WriteAppEventLog("IMEI : " + imei + " ||ValidateLogin||Exception : " + ex.Message, true);

        }
        finally
        {
            //Status status = new Status();
            status.status = rtnResult;
            gloLog.Info(string.Format(ApplicationConstant.LOG_PREFIX_OUT, "ValidateLogin"));
        }
        return status;
    }

    public Status CapturePOD(string enterpriseID, string phoneNo, string imei, string userID, string userPwd, string statusDT, string consignmentNo, string consignee)
    {
        gloLog.Info(string.Format(ApplicationConstant.LOG_PREFIX_IN, "CapturePOD"));
        ProcessResults rtnResult = null;
        Status status = null;
        try
        {
            rtnResult = new ProcessResults();
            status = new Status();

            //Passwordis
            byte bytePassworsIs = 0;
            string strPasswordis = System.Configuration.ConfigurationManager.AppSettings["Passwordis"].ToString();
            gloLog.Info(string.Format(ApplicationConstant.LOG_PREFIX_ACCESS, "Config Passwordis =" + strPasswordis));
            if (!string.IsNullOrEmpty(strPasswordis) && strPasswordis.Equals("0"))
            {
                bytePassworsIs = 1;
            }

            //decrypt password
            string decryptPassword = "";
            try
            {
                bool boolHashing = false;
                string key = System.Configuration.ConfigurationManager.AppSettings["HashKey"].ToString();
                string UseHashing = System.Configuration.ConfigurationManager.AppSettings["UseHashing"].ToString();
                if (!string.IsNullOrEmpty(UseHashing) && UseHashing.Equals("1"))
                {
                    boolHashing = true;
                }
                decryptPassword = DMSDecrypt.DMSDecryptHelper.Decrypt(userPwd, key, boolHashing);
            }
            catch (Exception e)
            {
                gloLog.Error(e.StackTrace);
            }


            //rowMessage
            string format = System.Configuration.ConfigurationManager.AppSettings["Rawmessage-format"].ToString();
            string strRawmessage = string.Format(format, "enterpriseID", enterpriseID);
            strRawmessage += string.Format(format, "phoneNo", phoneNo);
            strRawmessage += string.Format(format, "imei", imei);
            strRawmessage += string.Format(format, "userID", userID);
            strRawmessage += string.Format(format, "userPwd", userPwd);
            strRawmessage += string.Format(format, "statusDT", statusDT);
            strRawmessage += string.Format(format, "consignmentNo", consignmentNo);
            strRawmessage += string.Format(format, "consignee", consignee);

            if (!string.IsNullOrEmpty(strRawmessage))
                strRawmessage = strRawmessage.Substring(0, strRawmessage.Length - 1);

            gloLog.Info(string.Format(ApplicationConstant.LOG_PREFIX_ACCESS, "Rawmessage =" + strRawmessage));


            //param object
            ValidateDto param = new ValidateDto();
            param.Enterpriseid = enterpriseID;
            param.Phoneno = phoneNo;
            param.Imei = imei;
            param.Userid = userID;
            param.User_Password = decryptPassword;
            param.Rawmessage = strRawmessage;
            param.Passwordis = bytePassworsIs;

            //status Datetimes YYYYMMDDHHMI
            DateTime? statusDate = null;
            if (!string.IsNullOrEmpty(statusDT))
            {
                int hour = 0;
                int minute = 0;
                if (statusDT.Length >= 8)
                {
                    int year = Convert.ToInt32(statusDT.Substring(0, 4));
                    int month = Convert.ToInt32(statusDT.Substring(4, 2));
                    int day = Convert.ToInt32(statusDT.Substring(6, 2));

                    statusDate = new DateTime(year, month, day);
                }
                if (statusDT.Length >= 10)
                {
                    hour = Convert.ToInt32(statusDT.Substring(8, 2));
                }
                if (statusDT.Length >= 12)
                {
                    minute = Convert.ToInt32(statusDT.Substring(10, 2));
                }

                if (statusDate != null)
                {
                    statusDate = statusDate.Value.AddHours(hour).AddMinutes(minute);//.ToUniversalTime();
                    gloLog.Info(string.Format(ApplicationConstant.LOG_PREFIX_ACCESS, "statusDT =" + statusDT + " to " + statusDate.Value.ToShortDateString() + " " + statusDate.Value.ToShortTimeString()));
                }
            }


            MobileAppImpl mobileImpl = new MobileAppImpl();
            ReturnErrorsDto rcvValidate = mobileImpl.ValidateStatus(param, statusDate, consignmentNo, consignee);
            if (rcvValidate != null)
            {
                rtnResult.errorCode = rcvValidate.ErrorCode;
                rtnResult.errorMessage = rcvValidate.ErrorMessage;

                gloLog.Info(string.Format(ApplicationConstant.LOG_PREFIX_ACCESS, "errorCode =" + rtnResult.errorCode));
                gloLog.Info(string.Format(ApplicationConstant.LOG_PREFIX_ACCESS, "errorMessage =" + rtnResult.errorMessage));
            }


        }
        catch (Exception ex)
        {
            gloLog.Error(ex.StackTrace);
            DMSService.WebUtils.WriteAppEventLog("IMEI : " + imei + " ||CapturePOD||Exception : " + ex.Message, true);

        }
        finally
        {
            //Status status = new Status();
            status.status = rtnResult;
            gloLog.Info(string.Format(ApplicationConstant.LOG_PREFIX_OUT, "CapturePOD"));
        }
        return status;
    }


    public Status CaptureOther(string enterpriseID, string phoneNo, string imei
     , string userID, string userPwd, string statusDT
    , string consignmentNo, string status_code, string remark)
    {
        gloLog.Info(string.Format(ApplicationConstant.LOG_PREFIX_IN, "CaptureOther"));
        ProcessResults rtnResult = null;
      //  string rtnProcess = "";
        Status status = null;
        try
        {
            rtnResult = new ProcessResults();
            status = new Status();

            //Passwordis
            byte bytePassworsIs = 0;
            string strPasswordis = System.Configuration.ConfigurationManager.AppSettings["Passwordis"].ToString();
            gloLog.Info(string.Format(ApplicationConstant.LOG_PREFIX_ACCESS, "Config Passwordis =" + strPasswordis));
            if (!string.IsNullOrEmpty(strPasswordis) && strPasswordis.Equals("0"))
            {
                bytePassworsIs = 1;
            }

            //decrypt password
            string decryptPassword = "";
            try
            {
                bool boolHashing = false;
                string key = System.Configuration.ConfigurationManager.AppSettings["HashKey"].ToString();
                string UseHashing = System.Configuration.ConfigurationManager.AppSettings["UseHashing"].ToString();
                if (!string.IsNullOrEmpty(UseHashing) && UseHashing.Equals("1"))
                {
                    boolHashing = true;
                }
                decryptPassword = DMSDecrypt.DMSDecryptHelper.Decrypt(userPwd, key, boolHashing);
            }
            catch (Exception e)
            {
                gloLog.Error(e.StackTrace);
            }


            //rowMessage
            string format = System.Configuration.ConfigurationManager.AppSettings["Rawmessage-format"].ToString();
            string strRawmessage = string.Format(format, "enterpriseID", enterpriseID);
            strRawmessage += string.Format(format, "phoneNo", phoneNo);
            strRawmessage += string.Format(format, "imei", imei);
            strRawmessage += string.Format(format, "userID", userID);
            strRawmessage += string.Format(format, "userPwd", userPwd);
            strRawmessage += string.Format(format, "statusDT", statusDT);
            strRawmessage += string.Format(format, "consignmentNo", consignmentNo);
            strRawmessage += string.Format(format, "StatusCode", status_code);
            strRawmessage += string.Format(format, "Remarks", remark);

            if (!string.IsNullOrEmpty(strRawmessage))
                strRawmessage = strRawmessage.Substring(0, strRawmessage.Length - 1);

            gloLog.Info(string.Format(ApplicationConstant.LOG_PREFIX_ACCESS, "Rawmessage =" + strRawmessage));


            //param object
            ValidateDto param = new ValidateDto();
            param.Enterpriseid = enterpriseID;
            param.Phoneno = phoneNo;
            param.Imei = imei;
            param.Userid = userID;
            param.User_Password = decryptPassword;
            param.Rawmessage = strRawmessage;
            param.Passwordis = bytePassworsIs;

            //status Datetimes YYYYMMDDHHMI
            DateTime? statusDate = null;
            if (!string.IsNullOrEmpty(statusDT))
            {
                int hour = 0;
                int minute = 0;
                int second = 0;
                if (statusDT.Length >= 8)
                {
                    int year = Convert.ToInt32(statusDT.Substring(0, 4));
                    int month = Convert.ToInt32(statusDT.Substring(4, 2));
                    int day = Convert.ToInt32(statusDT.Substring(6, 2));

                    statusDate = new DateTime(year, month, day);
                }
                if (statusDT.Length >= 10)
                {
                    hour = Convert.ToInt32(statusDT.Substring(8, 2));
                }
                if (statusDT.Length >= 12)
                {
                    minute = Convert.ToInt32(statusDT.Substring(10, 2));

                    if (statusDT.Length > 12)
                    {
                        second = Convert.ToInt32(statusDT.Substring(12, 2));
                    }
                }

                if (statusDate != null)
                {
                    statusDate = statusDate.Value.AddHours(hour).AddMinutes(minute).AddSeconds(second);//.ToUniversalTime();
                    gloLog.Info(string.Format(ApplicationConstant.LOG_PREFIX_ACCESS, "statusDT =" + statusDT + " to " + statusDate.Value.ToShortDateString() + " " + statusDate.Value.ToShortTimeString()));
                }
            }


            MobileAppImpl mobileImpl = new MobileAppImpl();
            ReturnErrorsDto rcvValidate = mobileImpl.GeneralStatus(param, statusDate, consignmentNo, status_code, remark);
            if (rcvValidate != null)
            {
                rtnResult.errorCode = rcvValidate.ErrorCode;
                rtnResult.errorMessage = rcvValidate.ErrorMessage;

                gloLog.Info(string.Format(ApplicationConstant.LOG_PREFIX_ACCESS, "errorCode =" + rtnResult.errorCode));
                gloLog.Info(string.Format(ApplicationConstant.LOG_PREFIX_ACCESS, "errorMessage =" + rtnResult.errorMessage));
            }


        }
        catch (Exception ex)
        {
            gloLog.Error(ex.Message);
            DMSService.WebUtils.WriteAppEventLog("IMEI : " + imei + " ||CaptureOther||Exception : " + ex.Message, true);

        }
        finally
        {
            //Status status = new Status();
            status.status = rtnResult;
            gloLog.Info(string.Format(ApplicationConstant.LOG_PREFIX_OUT, "CaptureOther"));
        }
        return status;
    }

    public Status UninstallProcessLog(string enterpriseID, string phoneNo, string imei
     , string userID, string userPwd, string statusDT
    , string consignmentNo, string consignee, string status_code, string remark, string module_name)
    {
        gloLog.Info(string.Format(ApplicationConstant.LOG_PREFIX_IN, "UninstallProcessLog"));
        ProcessResults rtnResult = null;
        Status status = null;
        try
        {
            rtnResult = new ProcessResults();
            status = new Status();

            //rowMessage
            string format = System.Configuration.ConfigurationManager.AppSettings["Rawmessage-format"].ToString();
            string strRawmessage = string.Format(format, "enterpriseID", enterpriseID);
            strRawmessage += string.Format(format, "phoneNo", phoneNo);
            strRawmessage += string.Format(format, "imei", imei);
            strRawmessage += string.Format(format, "userID", userID);
            strRawmessage += string.Format(format, "userPwd", userPwd);
            strRawmessage += string.Format(format, "statusDT", statusDT);
            strRawmessage += string.Format(format, "consignmentNo", consignmentNo);
            strRawmessage += string.Format(format, "consignee", consignee);
            strRawmessage += string.Format(format, "StatusCode", status_code);
            strRawmessage += string.Format(format, "Remarks", remark);
            strRawmessage += string.Format(format, "module_name", module_name);


            if (!string.IsNullOrEmpty(strRawmessage))
                strRawmessage = strRawmessage.Substring(0, strRawmessage.Length - 1);

            gloLog.Info(string.Format(ApplicationConstant.LOG_PREFIX_ACCESS, "Rawmessage =" + strRawmessage));

            status.status.errorCode = 0;
            status.status.errorMessage = "Success";

            gloLog.Info(string.Format(ApplicationConstant.LOG_PREFIX_ACCESS, "errorCode =" + status.status.errorCode));
            gloLog.Info(string.Format(ApplicationConstant.LOG_PREFIX_ACCESS, "errorMessage =" + status.status.errorMessage));
        }
        catch (Exception ex)
        {
            gloLog.Error(ex.Message);

            status.status.errorCode = 999;
            status.status.errorMessage = ex.Message;

            gloLog.Info(string.Format(ApplicationConstant.LOG_PREFIX_ACCESS, "errorCode =" + status.status.errorCode));
            gloLog.Info(string.Format(ApplicationConstant.LOG_PREFIX_ACCESS, "errorMessage =" + status.status.errorMessage));
        }
        finally
        {

            gloLog.Info(string.Format(ApplicationConstant.LOG_PREFIX_OUT, "UninstallProcessLog"));
        }
        return status;
    }

    public Status WriteLogOnRuntime(string imei, string EventName, string MethodName,string enterpriseID, string phoneNo, string userID, string userPwd, string statusDT, string consignmentNo, string consignee, string status_code, string remark)
    {
        Status status = null;
        try
        {
            status = new Status();

            //rowMessage
            string format = System.Configuration.ConfigurationManager.AppSettings["Rawmessage-format"].ToString();
            string strRawmessage = "";

            if (!enterpriseID.Trim().Equals(""))
                strRawmessage += string.Format(format, "enterpriseID", enterpriseID);

            if (!phoneNo.Trim().Equals(""))
                strRawmessage += string.Format(format, "phoneNo", phoneNo);

            if (!imei.Trim().Equals(""))
                strRawmessage += string.Format(format, "imei", imei);

            if (!userID.Trim().Equals(""))
                strRawmessage += string.Format(format, "userID", userID);

            if (!userPwd.Trim().Equals(""))
                strRawmessage += string.Format(format, "userPwd", userPwd);

            if (!statusDT.Trim().Equals(""))
                strRawmessage += string.Format(format, "statusDT", statusDT);

            if (!consignmentNo.Trim().Equals(""))
                strRawmessage += string.Format(format, "consignmentNo", consignmentNo);

            if (!consignee.Trim().Equals(""))
                strRawmessage += string.Format(format, "consignee", consignee);

            if (!status_code.Trim().Equals(""))
                strRawmessage += string.Format(format, "StatusCode", status_code);

            if (!remark.Trim().Equals(""))
                strRawmessage += string.Format(format, "Remarks", remark);


            if (!string.IsNullOrEmpty(strRawmessage))
                strRawmessage = strRawmessage.Substring(0, strRawmessage.Length - 1);

            DMSService.WebUtils.WriteAppEventLog("IMEI : " + imei + " ||Event name : " + EventName, false);
            DMSService.WebUtils.WriteAppEventLog("IMEI : " + imei + " ||Method name : " + MethodName, false);
            DMSService.WebUtils.WriteAppEventLog("IMEI : " + imei + " ||Parameter : " + strRawmessage, false);
            gloLog.Info(string.Format(ApplicationConstant.LOG_PREFIX_ACCESS, "Parameter : " + strRawmessage));

            status.status.errorCode = 0;
            status.status.errorMessage = "Success";
        }
        catch (Exception ex)
        {
            status.status.errorCode = 999;
            status.status.errorMessage = ex.Message;

            DMSService.WebUtils.WriteAppEventLog("IMEI : " + imei + " ||Event name : " + EventName, true);
            DMSService.WebUtils.WriteAppEventLog("IMEI : " + imei + " ||Method name : " + MethodName, true);
            DMSService.WebUtils.WriteAppEventLog("IMEI : " + imei + " ||Exception : " + ex.Message, true);
        }
        finally
        {
        }
        return status;
    }

    public Status CapturePODWithSignature(string enterpriseID, string phoneNo, string imei, string userID, string userPwd, string statusDT, string consignmentNo, string consignee, string fileBytes)
    {
        gloLog.Info(string.Format(ApplicationConstant.LOG_PREFIX_IN, "CapturePODWithSignature"));
        ProcessResults rtnResult = null;
        Status status = null;

        try
        {
            rtnResult = new ProcessResults();
            status = new Status();

            //Passwordis
            byte bytePassworsIs = 0;
            string strPasswordis = System.Configuration.ConfigurationManager.AppSettings["Passwordis"].ToString();
            gloLog.Info(string.Format(ApplicationConstant.LOG_PREFIX_ACCESS, "Config Passwordis =" + strPasswordis));
            if (!string.IsNullOrEmpty(strPasswordis) && strPasswordis.Equals("0"))
            {
                bytePassworsIs = 1;
            }

            //decrypt password
            string decryptPassword = "";
            try
            {
                bool boolHashing = false;
                string key = System.Configuration.ConfigurationManager.AppSettings["HashKey"].ToString();
                string UseHashing = System.Configuration.ConfigurationManager.AppSettings["UseHashing"].ToString();
                if (!string.IsNullOrEmpty(UseHashing) && UseHashing.Equals("1"))
                {
                    boolHashing = true;
                }
                decryptPassword = DMSDecrypt.DMSDecryptHelper.Decrypt(userPwd, key, boolHashing);
            }
            catch (Exception e)
            {
                gloLog.Error(e.StackTrace);
            }


            //rowMessage
            string format = System.Configuration.ConfigurationManager.AppSettings["Rawmessage-format"].ToString();
            string strRawmessage = string.Format(format, "enterpriseID", enterpriseID);
            strRawmessage += string.Format(format, "phoneNo", phoneNo);
            strRawmessage += string.Format(format, "imei", imei);
            strRawmessage += string.Format(format, "userID", userID);
            strRawmessage += string.Format(format, "userPwd", userPwd);
            strRawmessage += string.Format(format, "statusDT", statusDT);
            strRawmessage += string.Format(format, "consignmentNo", consignmentNo);
            strRawmessage += string.Format(format, "consignee", consignee);
            strRawmessage += string.Format(format, "Image", fileBytes);
            if (!string.IsNullOrEmpty(strRawmessage))
                strRawmessage = strRawmessage.Substring(0, strRawmessage.Length - 1);

            gloLog.Info(string.Format(ApplicationConstant.LOG_PREFIX_ACCESS, "Rawmessage =" + strRawmessage));

            //param object
            ValidateDto param = new ValidateDto();
            param.Enterpriseid = enterpriseID;
            param.Phoneno = phoneNo;
            param.Imei = imei;
            param.Userid = userID;
            param.User_Password = decryptPassword;
            param.Rawmessage = strRawmessage;
            param.Passwordis = bytePassworsIs;
            //status Datetimes YYYYMMDDHHMI
            DateTime? statusDate = null;
            if (!string.IsNullOrEmpty(statusDT))
            {
                int hour = 0;
                int minute = 0;
                if (statusDT.Length >= 8)
                {
                    int year = Convert.ToInt32(statusDT.Substring(0, 4));
                    int month = Convert.ToInt32(statusDT.Substring(4, 2));
                    int day = Convert.ToInt32(statusDT.Substring(6, 2));

                    statusDate = new DateTime(year, month, day);
                }
                if (statusDT.Length >= 10)
                {
                    hour = Convert.ToInt32(statusDT.Substring(8, 2));
                }
                if (statusDT.Length >= 12)
                {
                    minute = Convert.ToInt32(statusDT.Substring(10, 2));
                }

                if (statusDate != null)
                {
                    statusDate = statusDate.Value.AddHours(hour).AddMinutes(minute);//.ToUniversalTime();
                    gloLog.Info(string.Format(ApplicationConstant.LOG_PREFIX_ACCESS, "statusDT =" + statusDT + " to " + statusDate.Value.ToShortDateString() + " " + statusDate.Value.ToShortTimeString()));
                }
            }

            MobileAppImpl mobileImpl = new MobileAppImpl();
            ReturnErrorsDto rcvValidate = mobileImpl.ValidateStatusWithImage(param, statusDate, consignmentNo, consignee, fileBytes );
            if (rcvValidate != null)
            {
                rtnResult.errorCode = rcvValidate.ErrorCode;
                rtnResult.errorMessage = rcvValidate.ErrorMessage;

                gloLog.Info(string.Format(ApplicationConstant.LOG_PREFIX_ACCESS, "errorCode =" + rtnResult.errorCode));
                gloLog.Info(string.Format(ApplicationConstant.LOG_PREFIX_ACCESS, "errorMessage =" + rtnResult.errorMessage));
            }


        }
        catch (Exception ex)
        {
            gloLog.Error(ex.StackTrace);
            DMSService.WebUtils.WriteAppEventLog("IMEI : " + imei + " ||CapturePODWithSignature||Exception : " + ex.Message, true);

        }
        finally
        {
            status.status = rtnResult;

            gloLog.Info(string.Format(ApplicationConstant.LOG_PREFIX_OUT, "CapturePODWithSignature"));
        }
        return status;
    }

    public bool UploadToTempFolder(byte[] pFileBytes, string pFileName)
    {
        bool isSuccess = false;
        FileStream fileStream = null;
        //Get the file upload path store in web services web.config file.  
        string strTempFolderPath = System.Configuration.ConfigurationManager.AppSettings.Get("FileUploadPath");
        try
        {

            if (!string.IsNullOrEmpty(strTempFolderPath))
            {
                if (!string.IsNullOrEmpty(pFileName))
                {
                    string strFileFullPath = strTempFolderPath + pFileName;
                    fileStream = new FileStream(strFileFullPath, FileMode.OpenOrCreate, FileAccess.ReadWrite);
                    // write file stream into the specified file  
                    using (System.IO.FileStream fs = fileStream)
                    {
                        fs.Write(pFileBytes, 0, pFileBytes.Length);
                        isSuccess = true;
                    }
                }
            }
        }
        catch (Exception ex)
        {

            throw ex;

        }

        return isSuccess;
    }

    public byte[] GetFileFromFolder(string filename)
    {
        byte[] filedetails = new byte[0];
        string strTempFolderPath = System.Configuration.ConfigurationManager.AppSettings.Get("FileUploadPath");
        if (File.Exists(strTempFolderPath + filename))
        {
            return File.ReadAllBytes(strTempFolderPath + filename);
        }
        else return filedetails;
    }

    public MobileApp_StatusCodesList StatusCode_Read()
    {
        MobileApp_StatusCodesList objList = new MobileApp_StatusCodesList();

        try
        {
            objList = new MobileAppImpl().StatusCode_Read();
        }
        catch (Exception ex)
        {
            gloLog.Error("StatusCode_Read|Exception " + ex.Message);
            DMSService.WebUtils.WriteAppEventLog("Service==>StatusCode_Read||Exception : " + ex.Message, true);
            throw ex;
        }
        finally
        {

        }
        gloLog.Info(string.Format(ApplicationConstant.LOG_PREFIX_OUT, "StatusCode_Read"));
        return objList;
    }

    public MobileApp_ConsignmentsList ScanShippingList(string enterpriseID, string phoneNo, string imei, string userID, string userPwd, string statusDT, string shippingListNo)
    {
        gloLog.Info(string.Format(ApplicationConstant.LOG_PREFIX_IN, "ScanShippingListNo"));
        ProcessResults rtnResult = null;
        Status status = null;
        MobileApp_ConsignmentsList objList = new MobileApp_ConsignmentsList();
        try
        {

            rtnResult = new ProcessResults();
            status = new Status();

            //Passwordis
            byte bytePassworsIs = 0;
            string strPasswordis = System.Configuration.ConfigurationManager.AppSettings["Passwordis"].ToString();
            gloLog.Info(string.Format(ApplicationConstant.LOG_PREFIX_ACCESS, "Config Passwordis =" + strPasswordis));
            if (!string.IsNullOrEmpty(strPasswordis) && strPasswordis.Equals("0"))
            {
                bytePassworsIs = 1;
            }

            //decrypt password
            string decryptPassword = "";
            try
            {
                bool boolHashing = false;
                string key = System.Configuration.ConfigurationManager.AppSettings["HashKey"].ToString();
                string UseHashing = System.Configuration.ConfigurationManager.AppSettings["UseHashing"].ToString();
                if (!string.IsNullOrEmpty(UseHashing) && UseHashing.Equals("1"))
                {
                    boolHashing = true;
                }
                decryptPassword = DMSDecrypt.DMSDecryptHelper.Decrypt(userPwd, key, boolHashing);
            }
            catch (Exception e)
            {
                gloLog.Error(e.StackTrace);
            }


            //rowMessage
            string format = System.Configuration.ConfigurationManager.AppSettings["Rawmessage-format"].ToString();
            string strRawmessage = string.Format(format, "enterpriseID", enterpriseID);
            strRawmessage += string.Format(format, "phoneNo", phoneNo);
            strRawmessage += string.Format(format, "imei", imei);
            strRawmessage += string.Format(format, "userID", userID);
            strRawmessage += string.Format(format, "userPwd", userPwd);
            strRawmessage += string.Format(format, "statusDT", statusDT);
            strRawmessage += string.Format(format, "shippingListNo", shippingListNo);

            if (!string.IsNullOrEmpty(strRawmessage))
                strRawmessage = strRawmessage.Substring(0, strRawmessage.Length - 1);

            gloLog.Info(string.Format(ApplicationConstant.LOG_PREFIX_ACCESS, "Rawmessage =" + strRawmessage));


            //param object
            ValidateDto param = new ValidateDto();
            param.Enterpriseid = enterpriseID;
            param.Phoneno = phoneNo;
            param.Imei = imei;
            param.Userid = userID;
            param.User_Password = decryptPassword;
            param.Rawmessage = strRawmessage;
            param.Passwordis = bytePassworsIs;

            //status Datetimes YYYYMMDDHHMI
            DateTime? statusDate = null;
            if (!string.IsNullOrEmpty(statusDT))
            {
                int hour = 0;
                int minute = 0;
                if (statusDT.Length >= 8)
                {
                    int year = Convert.ToInt32(statusDT.Substring(0, 4));
                    int month = Convert.ToInt32(statusDT.Substring(4, 2));
                    int day = Convert.ToInt32(statusDT.Substring(6, 2));

                    statusDate = new DateTime(year, month, day);
                }
                if (statusDT.Length >= 10)
                {
                    hour = Convert.ToInt32(statusDT.Substring(8, 2));
                }
                if (statusDT.Length >= 12)
                {
                    minute = Convert.ToInt32(statusDT.Substring(10, 2));
                }

                if (statusDate != null)
                {
                    statusDate = statusDate.Value.AddHours(hour).AddMinutes(minute);//.ToUniversalTime();
                    gloLog.Info(string.Format(ApplicationConstant.LOG_PREFIX_ACCESS, "statusDT =" + statusDT + " to " + statusDate.Value.ToShortDateString() + " " + statusDate.Value.ToShortTimeString()));
                }
            }

            MobileAppImpl mobileImpl = new MobileAppImpl();
            objList = mobileImpl.ShippingList_Read(enterpriseID, phoneNo, imei, userID, userPwd, statusDate, shippingListNo, strRawmessage);

            return objList;

        }
        catch (Exception ex)
        {
            gloLog.Error(ex.StackTrace);
            DMSService.WebUtils.WriteAppEventLog("IMEI : " + imei + " ||ScanShippingList||Exception : " + ex.Message, true);

        }
        finally
        {
        }
        return objList;
    }
}

