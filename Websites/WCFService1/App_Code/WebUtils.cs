﻿using log4net;
using log4net.Core;
using log4net.Util;
using log4net.Layout;
using log4net.Appender;
using log4net.Repository;
using log4net.Repository.Hierarchy;

using System;
using System.Collections.Generic;
using System.Data;
using System.Reflection;
using System.Configuration;
using System.Xml;

namespace DMSService
{
    public  class WebUtils
    {
        public static string getConnection
        {
            get { return System.Configuration.ConfigurationManager.ConnectionStrings["local_SystemConnectionString"].ToString(); }
        }
        public static DataTable ConvertLINQToDataTable<T>(IEnumerable<T> varlist)
        {
            DataTable dtReturn = new DataTable();
            try
            {
                // column names 
                PropertyInfo[] oProps = null;

                if (varlist == null) return dtReturn;

                foreach (T rec in varlist)
                {
                    // Use reflection to get property names, to create table, Only first time, others  will follow 
                    if (oProps == null)
                    {
                        oProps = ((Type)rec.GetType()).GetProperties();
                        foreach (PropertyInfo pi in oProps)
                        {
                            Type colType = pi.PropertyType;

                            if ((colType.IsGenericType) && (colType.GetGenericTypeDefinition() == typeof(Nullable<>)))
                            {
                                colType = colType.GetGenericArguments()[0];
                            }

                            dtReturn.Columns.Add(new DataColumn(pi.Name, colType));
                        }
                    }

                    DataRow dr = dtReturn.NewRow();

                    foreach (PropertyInfo pi in oProps)
                    {
                        dr[pi.Name] = pi.GetValue(rec, null) == null ? DBNull.Value : pi.GetValue
                        (rec, null);
                    }
                    dtReturn.Rows.Add(dr);
                }

            }
            catch (Exception ex)
            {
                //gloLog.Error(ex.StackTrace);
                throw ex;
            }
            return dtReturn;
        }

        private static IAppender CreateFileAppender(string name, string fileName)
        {
            PatternLayout patternLayout = new PatternLayout();
            patternLayout.ConversionPattern = ConfigurationManager.AppSettings["LogPatternLayout"];
            patternLayout.ActivateOptions();

            RollingFileAppender appender = new RollingFileAppender();
            appender.Name = name;
            appender.File = fileName;
            appender.AppendToFile = true;
            appender.MaxSizeRollBackups = 10;
            appender.RollingStyle = RollingFileAppender.RollingMode.Size;
            appender.MaximumFileSize = ConfigurationManager.AppSettings["LogMaximumFileSize"];
            appender.Layout = patternLayout;
            appender.LockingModel = new FileAppender.MinimalLock();
            appender.StaticLogFileName = true;
            appender.ActivateOptions();
            return appender;
        }

        public static void WriteAppEventLog (string message, bool isError)
        {
            var hierarchy = (Hierarchy)LogManager.GetRepository();
            hierarchy.Threshold = Level.Debug;

            // Configure LoggerA
            string logName =ConfigurationManager.AppSettings["LogName"];
            string fileName = string.Format(ConfigurationManager.AppSettings["LogPath"], "_" + System.DateTime.Now.ToString("yyyyMMdd"));

            var LogEvent = hierarchy.LoggerFactory.CreateLogger("LogEvent");
            LogEvent.Hierarchy = hierarchy;
            LogEvent.AddAppender(CreateFileAppender(logName, fileName));
            LogEvent.Repository.Configured = true;
            LogEvent.Level = Level.Debug;

            ILog logEvent = new LogImpl(LogEvent);

            if (isError)
                logEvent.Error(message);
            else
                logEvent.Info(message);
        }

        public string GetXMLAsString(XmlDocument myxml)
        {
            return myxml.OuterXml;
        }
    }
}