﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for ApplicationMessageConstant
/// </summary>

    public static class ApplicationMessage
    {
        public const string PROCESS_RESULT_SUCCESS = "PROCESS_RESULT_SUCCESS";
        public const string PROCESS_RESULT_FAIL = "PROCESS_RESULT_FAIL";
        public const string PROCESS_RESULT_PREFIX_ERROR = "@ERROR";



        public const string ERROR_INVALID_VALUE = "{0} Value not over {1} . (ค่าของ {0} ต้องไม่เกิน {1})";
        public const string ERROR_PASSWORD_VALIDATE_LESS_THAN = "Password should be more than {0} characters. (กรุณาระบุพาสเวิร์ดอย่างน้อย {0} ตัวอักษร)";

        //SAVE
        public const string RESULT_SAVE_SUCCESS = "The data has been saved successfully. (บันทึกรายการสำเร็จ)";
        public const string RESULT_SAVE_FAIL = "The data could not be saved. Please see more detail below. (ไม่สามารถบันทึกข้อมูลได้. กรุณาตรวจสอบรายละเอียดด้านล่าง)";
        public const string ERROR_INVALID_DUPLICATE_RECORD = "Duplicated data found. (พบข้อมูลซ้ำในระบบ ไม่สามารถทำรายการได้)";
        //CONFIRM
        public const string INFO_CONFIRM_DELETE = "Are you confirm to delete data? (คุณต้องการยืนยันที่จะลบข้อมูลหรือไม่)";

        //DELETE
        public const string RESULT_DELETE_NO_DATA_FOUND = "No data found ,can not delete all data. (ไม่พบข้อมูล ไม่สามารถลบข้อมูลทั้งหมดได้)";
        public const string RESULT_DELETE_ALL_FAIL = "The data could not be deleted. Please see more detail below. (ไม่สามารถลบข้อมูลได้ กรุณาดูรายละเอียดด้านล่าง)";
        public const string RESULT_DELETE_SUCCESS = "The data has been deleted successfully. (ข้อมูลถูกลบเรียบร้อยแล้ว)";
        public const string RESULT_DELETE_FAIL = "The data could not be deleted. Please see more detail below. (ไม่สามารถลบข้อมูลได้ กรุณาดูรายละเอียดด้านล่าง)";


        public const string RESULT_ERROR = "Found error. Please see more detail below. (พบข้อผิดพลาด. กรุณาตรวจสอบรายละเอียดด้านล่าง)";
        public const string ERROR_NO_DATA_SEARCH = "No data found. (ไม่พบข้อมูล)";

        //Profile
        public const string ERROR_MISMATCH_PASSWORD = "Repassword is mismatch. (Password และ Repassword ไม่ตรงกัน)";
        public const string ERROR_PASSWORD_INCORRECT = "Password you entered is incorrect. (Password ไม่ถูกต้อง)";
        public const string ERROR_SAME_OLD_PASSWORD = "The new password should not be the same. (ไม่สามารถใช้ Password ซ้ำเดิมได้)";

        //Import Error 
        //public const string ERROR_INVALID_NUMBER_DATA = "Please input {0} only number.";
        public const string ERROR_INVALID_NUMBER = "Please input only number.";
        public const string ERROR_INVALID_OUT_OF_DATA = "Please input {0} between {1} - {2}.";
        //public const string ERROR_DATE_FROM_LESS_THAN_DATE_TO = "Date From must be less than Date To. (Date From ต้องมีค่าน้อยกว่า Date To)";
        public const string ERROR_DUPLICATE_RECORD = "This row is dulicated.";
        public const string ERROR_REQUIRE_FIELD = "This column is not null or empty.";
        public const string ERROR_INVALID_FORMAT_DATE = "";
        public const string ERROR_NO_MAPPING_FOR_THIS_DIST_CODE = "Cannot mapping Dist Code with Location Next ({0}).";
        public const string ERROR_QTY_ISNOT_A_NUMBER = "The Qty is not a number, please verify";
        public const string ERROR_INVALID_OUT_OF_MIN_DATA = "Please input {0} less than {1}.";
        
        //ta
        public const string ERROR_INVALID_EXCEL_DATE_LENGTH = "{0} Length not equal {1} digit.";
        public const string ERROR_INVALID_EXCEL_DATE_FORMAT = " {0} must be only date format {1}. ";
        public const string ERROR_INVALID_LENGTH = "{0} Length not over {1} digit.";

    }

