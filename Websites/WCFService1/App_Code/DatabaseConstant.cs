﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for DatabaseConstant
/// </summary>
public static class DatabaseConstant
{
    #region User
    public const int VALIDATE_LENGTH_OF_USER_LOGIN = 50;
    public const int VALIDATE_LENGTH_OF_PASSWORD = 50;
    public const int VALIDATE_LENGTH_OF_FIRST_NAME = 200;
    public const int VALIDATE_LENGTH_OF_LAST_NAME = 200;
    public const int VALIDATE_PASSWORD_LESS_THAN = 6;

    public const string COLUMNS_USER_OF_USER_LOGIN = "User_Login";
    public const string COLUMNS_USER_OF_PASSWORD = "Password";
    public const string COLUMNS_USER_OF_FIRST_NAME = "First_Name";
    public const string COLUMNS_USER_OF_LAST_NAME = "Last_Name";
    public const string COLUMNS_USER_OF_ISLOCK = "IsLock";
    public const string COLUMNS_USER_OF_ISACTIVE = "IsActive";
    public const string COLUMNS_USER_OF_BAD_PASSWORD_COUNT = "BadPassword_Count";
    public const string COLUMNS_USER_OF_GROUP_USER = "Group_User";
    public const string COLUMNS_USER_OF_IS_FIRST_LOGON = "IsFirst_Logon";
    public const string COLUMNS_USER_OF_PASSWORD_EXPIRE_DATE = "Password_ExpireDate";
    public const string COLUMNS_USER_OF_PASSWORD_LAST_UPDATE = "Password_LastUpdate";
    public const string COLUMNS_USER_OF_CREATED_DATE = "Create_Date";
    public const string COLUMNS_USER_OF_UPDATEBY = "UpdateBy";
    public const string COLUMNS_USER_OF_UPDATEDATE = "UpdateDate";
    #endregion User

    #region SaleScrap
    public const double VALIDATE_VALUE_OF_SALESCRAP = 100.000;

    public const string COLUMNS_SALESCRAPMAINTENANCE_PACKING_MONTH = "Packing_Month";
    public const string COLUMNS_SALESCRAPMAINTENANCE_FACTORY_CODE = "Factory_Code";
    public const string COLUMNS_SALESCRAPMAINTENANCE_SALESCRAP = "SaleScrap";
    public const string COLUMNS_SALESCRAPMAINTENANCE_UPDATEBY = "UpdateBy";
    public const string COLUMNS_SALESCRAPMAINTENANCE_UPDATEDATE = "UpdateDate";
    #endregion SaleScrap

    #region Menu
    public const string COLUMNS_MENUREFERENCE_MENU_ID = "Menu_Id";
    public const string COLUMNS_MENUREFERENCE_REFERENCE_MENU_ID = "Reference_Menu_Id";
    public const string COLUMNS_MENUREFERENCE_LEVEL = "Level";
    public const string COLUMNS_MENUREFERENCE_MENU_NAME = "Menu_Name";
    public const string COLUMNS_MENUREFERENCE_SCREEN_ID = "Screen_Id";
    #endregion Menu

    #region Screen
    public const string COLUMNS_SCREEN_SCREEN_ID = "Screen_Id";
    public const string COLUMNS_SCREEN_MENU_URL = "Menu_Url";
    #endregion Screen

    #region CountryCode
    public const int VALIDATE_LENGTH_OF_COUNTRY_CODE = 3;
    public const int VALIDATE_LENGTH_OF_COUNTRY = 10;

    public const string COLUMNS_COUNTRYCODE_COUNTRY_CODE_ID = "Country_Code_Id";
    public const string COLUMNS_COUNTRYCODE_COUNTRY = "Country";
    public const string COLUMNS_COUNTRYCODE_COUNTRY_CODE = "Country_Code";
    public const string COLUMNS_COUNTRYCODE_FACTORY_CODE = "Factory_Code";
    public const string COLUMNS_COUNTRYCODE_UPDATEBY = "UpdateBy";
    public const string COLUMNS_COUNTRYCODE_UPDATEDATE = "UpdateDate";
    #endregion CountryCode

    #region UserLogonInformation

    public const string COLUMNS_USERLOGON_INFO_USER_LOGIN = "User_Login";
    public const string COLUMNS_USERLOGON_INFO_REGISER_DATE = "Create_Date";
    public const string COLUMNS_USERLOGON_INFO_PASSWORD_EXPIRE_DATE = "Password_ExpireDate";
    public const string COLUMNS_USERLOGON_INFO_PASSWORD_LAST_SET = "Password_LastUpdate";
    public const string COLUMNS_USERLOGON_INFO_TIME_LAST_LOGON = "Time_Login";
    public const string COLUMNS_USERLOGON_INFO_TIME_BAD_LOGON = "Time_Bad_Login";
    public const string COLUMNS_USERLOGON_INFO_GROUP_USER = "Group_User";
    public const string COLUMNS_USERLOGON_INFO_ISLOCK = "IsLock";
    public const string COLUMNS_USERLOGON_INFO_ISACTIVE = "IsActive";
    public const string COLUMNS_USERLOGON_INFO_BAD_PASSWORD_COUNT = "BadPassword_Count";

    #endregion UserLogonInformation

    #region MaintainPlantCode
    public const string COLUMNS_MAINTAIN_PLANTCODE_PLANT_CODE_ID = "Plant_Code_Id";
    public const string COLUMNS_MAINTAIN_PLANTCODE_COUNTRY = "Country";
    public const string COLUMNS_MAINTAIN_PLANTCODE_PLANT_CODE = "Plant_Code";
    public const string COLUMNS_MAINTAIN_PLANTCODE_FACTORY_CODE = "Factory_Code";
    public const string COLUMNS_MAINTAIN_PLANTCODE_FROM_GROUP = "From_Group";
    public const string COLUMNS_MAINTAIN_PLANTCODE_FROM_COUNTRY = "From_Country";
    public const string COLUMNS_MAINTAIN_PLANTCODE_PLANT_NAME = "Plant_Name";
    public const string COLUMNS_MAINTAIN_PLANTCODE_SUPPORT_DATA_ID = "Support_Data_Id";
    public const string COLUMNS_MAINTAIN_PLANTCODE_UPDATEBY = "UpdateBy";
    public const string COLUMNS_MAINTAIN_PLANTCODE_UPDATEDATE = "UpdateDate";

    #endregion MaintainPlantCode

    #region MappingSupportData

    public const string COLUMNS_MAPPING_SUPPORTDATA_SUPPORT_DATA_ID = "Support_Data_Id";
    public const string COLUMNS_MAPPING_SUPPORTDATA_SUPPORT_REPORT_NAME = "Support_Report_Name";
    public const string COLUMNS_MAPPING_SUPPORTDATA_SUPPORT_CODE = "Support_Code";
    public const string COLUMNS_MAPPING_SUPPORTDATA_MAPPING_AMOUNT_FIELD1 = "Mapping_Amount_Field1";
    public const string COLUMNS_MAPPING_SUPPORTDATA_MAPPING_AMOUNT_FIELD2 = "Mapping_Amount_Field2";
    public const string COLUMNS_MAPPING_SUPPORTDATA_MAPPING_AMOUNT_TEXT = "Mapping_Amount_Text";
    public const string COLUMNS_MAPPING_SUPPORTDATA_MAPPING_CODE_40DR = "Mapping_Code_40DR";
    public const string COLUMNS_MAPPING_SUPPORTDATA_MAPPING_CODE_50CR = "Mapping_Code_50CR";
    public const string COLUMNS_MAPPING_SUPPORTDATA_DESCRIPTION_TEXT = "Description_Text";

    #endregion MappingSupportData

    #region UserAuthorization
    public const string COLUMNS_USER_AUTHORIZE_OF_AUTHORIZE_ID = "Authorize_Id";
    public const string COLUMNS_USER_AUTHORIZE_OF_USER_LOGIN = "UserLogin";
    public const string COLUMNS_USER_AUTHORIZE_OF_SCREEN_ID = "Screen_Id";
    public const string COLUMNS_USER_AUTHORIZE_OF_UPDATEBY = "UpdateBy";
    public const string COLUMNS_USER_AUTHORIZE_OF_UPDATEDATE = "UpdateDate";

    #endregion UserAuthorization

    #region MenuReference

    public const string COLUMNS_MENU_REFERENCE_OF_MAINMENU = "MainMenu";
    public const string COLUMNS_MENU_REFERENCE_OF_MENU_LEVEL_1 = "Menu_Level_1";
    public const string COLUMNS_MENU_REFERENCE_OF_MENU_LEVEL_2 = "Menu_Level_2";
    public const string COLUMNS_MENU_REFERENCE_OF_MENU_LEVEL_3 = "Menu_Level_3";
    public const string COLUMNS_MENU_REFERENCE_OF_MENU_SC_ID = "Menu_ScreenID";

    #endregion MenuReference

    #region Manifest_Detail
    public const string COLUMNS_MANIFEST_DETAIL_OF_MANIFEST_DETAIL_ID = "Manifest_Detail_Id";
    public const string COLUMNS_MANIFEST_DETAIL_OF_MANIFEST_MAIN_ID = "Manifest_Main_Id";
    public const string COLUMNS_MANIFEST_DETAIL_OF_FILEID = "FileID";
    public const string COLUMNS_MANIFEST_DETAIL_OF_RECORDID = "RecordID";
    public const string COLUMNS_MANIFEST_DETAIL_OF_EXPORTER_COMPANY = "Exporter_Company";
    public const string COLUMNS_MANIFEST_DETAIL_OF_DIVISION_1 = "Division_1";
    public const string COLUMNS_MANIFEST_DETAIL_OF_IMPORTER_COMPANY = "Importer_Company";
    public const string COLUMNS_MANIFEST_DETAIL_OF_DIVISION_2 = "Division_2";
    public const string COLUMNS_MANIFEST_DETAIL_OF_PACKING_GROUP = "Packing_Group";
    public const string COLUMNS_MANIFEST_DETAIL_OF_CASE_NO = "Case_No";
    public const string COLUMNS_MANIFEST_DETAIL_OF_EC_PART_NO = "EC_Part_No";
    public const string COLUMNS_MANIFEST_DETAIL_OF_PART_COLOR = "Part_Color";
    public const string COLUMNS_MANIFEST_DETAIL_OF_PART_OCCURRENCE = "Part_Occurrence";
    public const string COLUMNS_MANIFEST_DETAIL_OF_ORDER_NO = "Order_No";
    public const string COLUMNS_MANIFEST_DETAIL_OF_SHIPMENT = "Shipment";
    public const string COLUMNS_MANIFEST_DETAIL_OF_HS_CODE = "HS_Code";
    public const string COLUMNS_MANIFEST_DETAIL_OF_EC_PART_NO_WITH_HYPHEN = "EC_Part_No_With_Hyphen";
    public const string COLUMNS_MANIFEST_DETAIL_OF_EC_PART_NAME = "EC_Part_Name";
    public const string COLUMNS_MANIFEST_DETAIL_OF_BASE_PART_NO = "Base_Part_No";
    public const string COLUMNS_MANIFEST_DETAIL_OF_BASE_PART_WITH_HYPHEN = "Base_Part_With_Hyphen";
    public const string COLUMNS_MANIFEST_DETAIL_OF_PACKING_NO = "Packing_No";
    public const string COLUMNS_MANIFEST_DETAIL_OF_VANNING_GROUP_CODE = "Vanning_Group_Code";
    public const string COLUMNS_MANIFEST_DETAIL_OF_REASEN_CODE_KNOWN_SHORTAGE = "Reasen_Code_Known_Shortage";
    public const string COLUMNS_MANIFEST_DETAIL_OF_PART_PRICE_NO = "Part_Price_No";
    public const string COLUMNS_MANIFEST_DETAIL_OF_PACKING_MTOC = "Packing_MTOC";
    public const string COLUMNS_MANIFEST_DETAIL_OF_ITEM_NO = "Item_No";
    public const string COLUMNS_MANIFEST_DETAIL_OF_EC_NO = "EC_No";
    public const string COLUMNS_MANIFEST_DETAIL_OF_EC_CONTENTS_CODE = "EC_Contents_Code";
    public const string COLUMNS_MANIFEST_DETAIL_OF_PART_WEIGHT = "Part_Weight";
    public const string COLUMNS_MANIFEST_DETAIL_OF_PART_PRICE = "Part_Price";
    public const string COLUMNS_MANIFEST_DETAIL_OF_PACKED_PART_QTY = "Packed_Part_Qty";
    public const string COLUMNS_MANIFEST_DETAIL_OF_CONTAINERNO = "ContainerNo";
    public const string COLUMNS_MANIFEST_DETAIL_OF_PRODUCTID = "ProductID";
    public const string COLUMNS_MANIFEST_DETAIL_OF_ORDERPACKINGGROUP = "OrderPackingGroup";
    public const string COLUMNS_MANIFEST_DETAIL_OF_ORDEREDPARTNO = "OrderedPartNo";
    public const string COLUMNS_MANIFEST_DETAIL_OF_ORDEREDPARTCOLOR = "OrderedPartColor";
    public const string COLUMNS_MANIFEST_DETAIL_OF_STRUCTUREOFPARTSPRICE = "StructureOfPartsPrice";
    public const string COLUMNS_MANIFEST_DETAIL_OF_LCL_CODE = "LCL_Code";
    public const string COLUMNS_MANIFEST_DETAIL_OF_EXCHANGERATE = "ExchangeRate";
    public const string COLUMNS_MANIFEST_DETAIL_OF_PARTPRICE_YEN = "PartPrice_Yen";
    public const string COLUMNS_MANIFEST_DETAIL_OF_BONDEDCODE = "BondedCode";
    public const string COLUMNS_MANIFEST_DETAIL_OF_ACTUALSHIPPINGDATE = "ActualShippingDate";
    public const string COLUMNS_MANIFEST_DETAIL_OF_PARTSQTY_IN_PACKINGSPEC = "PartsQTY_IN_PackingSpec";
    public const string COLUMNS_MANIFEST_DETAIL_OF_CARTON = "Carton";
    public const string COLUMNS_MANIFEST_DETAIL_OF_CARTON_QTY_IN_MODULE = "Carton_QTY_IN_Module";
    public const string COLUMNS_MANIFEST_DETAIL_OF_CARTON_SEQUENCE = "Carton_Sequence";
    public const string COLUMNS_MANIFEST_DETAIL_OF_CARTON_STANDARD = "Carton_Standard";
    public const string COLUMNS_MANIFEST_DETAIL_OF_CARTON_QTY_IN_CARTON = "Carton_QTY_IN_Carton";
    public const string COLUMNS_MANIFEST_DETAIL_OF_POLYBAG = "Polybag";
    public const string COLUMNS_MANIFEST_DETAIL_OF_POLYBAG_SEQUENCE = "Polybag_Sequence";
    public const string COLUMNS_MANIFEST_DETAIL_OF_POLYBAG_STANDARD = "Polybag_Standard";
    public const string COLUMNS_MANIFEST_DETAIL_OF_POLYBAG_QTY_IN_BAG = "Polybag_QTY_IN_Bag";
    public const string COLUMNS_MANIFEST_DETAIL_OF_ENGINE_MISSIONASSYSIGN = "Engine_MissionAssySign";
    public const string COLUMNS_MANIFEST_DETAIL_OF_PACKINGSEQUENCE_IN_MODULE = "PackingSequence_IN_Module";
    public const string COLUMNS_MANIFEST_DETAIL_OF_INITIALPARTSIGN = "InitialPartSign";
    public const string COLUMNS_MANIFEST_DETAIL_OF_INTERCHANGEABILITY = "InterChangeAbility";
    public const string COLUMNS_MANIFEST_DETAIL_OF_RETURNABLECODE = "ReturnableCode";
    public const string COLUMNS_MANIFEST_DETAIL_OF_ORIGINALPC_NO = "OriginalPC_No";
    public const string COLUMNS_MANIFEST_DETAIL_OF_ORIGINALCASENO = "OriginalCaseNo";
    public const string COLUMNS_MANIFEST_DETAIL_OF_ORIGINALJSPORDERLOTNO = "OriginalJspOrderLotNo";
    public const string COLUMNS_MANIFEST_DETAIL_OF_UPDATEBY = "UpdateBy";
    public const string COLUMNS_MANIFEST_DETAIL_OF_UPDATEDATE = "UpdateDate";

    #endregion Manifest_Detail

    #region Manifest_Main
    public const string COLUMNS_MANIFEST_MAIN_OF_MANIFEST_MAIN_ID = "Manifest_Main_Id";
    public const string COLUMNS_MANIFEST_MAIN_OF_FACTORY_CODE = "Factory_Code";
    public const string COLUMNS_MANIFEST_MAIN_OF_PC_NO = "PC_No";
    public const string COLUMNS_MANIFEST_MAIN_OF_PACKING_DATE = "Packing_Date";
    public const string COLUMNS_MANIFEST_MAIN_OF_UPDATEBY = "UpdateBy";
    public const string COLUMNS_MANIFEST_MAIN_OF_UPDATEDATE = "UpdateDate";

    public const int COLUMNS_MANIFEST_IMPORT_COUNT = 60;
    #endregion Manifest_Main

    #region SystemParameters

    public const string COLUMNS_SYSTEMPARAMETERS_OF_SYS_PARAM_NAME = "Sys_Param_Name";
    public const string COLUMNS_SYSTEMPARAMETERS_OF_SYS_PARAM_TYPE = "Sys_Param_Type";
    public const string COLUMNS_SYSTEMPARAMETERS_OF_SYS_PARAM_VALUE = "Sys_Param_Value";
    public const string COLUMNS_SYSTEMPARAMETERS_OF_DESCRIPTION = "Description";
    public const string COLUMNS_SYSTEMPARAMETERS_OF_UPDATEBY = "UpdateBy";
    public const string COLUMNS_SYSTEMPARAMETERS_OF_UPDATEDATE = "UpdateDate";

    public const string SYS_PARAM_ADMIN_CYCLE_DAY_OF_PASSWORD_EXPIRE = "ADMIN_CYCLE_DAY_OF_PASSWORD_EXPIRE";
    public const string SYS_PARAM_USER_CYCLE_DAY_OF_PASSWORD_EXPIRE = "USER_CYCLE_DAY_OF_PASSWORD_EXPIRE";
    public const string SYS_PARAM_DEFAULT_PASSWORD_USER = "DEFAULT_PASSWORD_USER";
    public const string SYS_PARAM_NO_OF_BAD_PASSWORD_BEFORE_LOCK = "NO_OF_BAD_PASSWORD_BEFORE_LOCK";
    public const string SYS_PARAM_PASSWORD_LEN_MAX = "PASSWORD_LEN_MAX";
    public const string SYS_PARAM_PASSWORD_LEN_MIN = "PASSWORD_LEN_MIN";
    public const string SYS_PARAM_USER_LOGIN_LEN_MAX = "USER_LOGIN_LEN_MAX";
    public const string SYS_PARAM_USER_LOGIN_LEN_MIN = "USER_LOGIN_LEN_MIN";

    #endregion SystemParameters

    #region TransCost
    public const int VALIDATE_LENGTH_OF_DIST_CODE = 3;
    //public const int VALIDATE_VALUE_OF_CONTAINER_SIZE = 000;
    public const double VALIDATE_VALUE_OF_CONTAINER_VOLUME = 999999.99;
    public const double VALIDATE_VALUE_OF_CONTAINER_PRICE = 999999.99;


    public const string COLUMNS_TRANSCOST_OF_TRANSPORTATION_ID = "Transportation_Id";
    public const string COLUMNS_TRANSCOST_OF_PACKING_MONTH = "Packing_Month";
    public const string COLUMNS_TRANSCOST_OF_DIST_CODE = "Dist_Code";
    public const string COLUMNS_TRANSCOST_OF_FACTORY_CODE = "Factory_Code";
    public const string COLUMNS_TRANSCOST_OF_CONTAINER_SIZE = "Container_Size";
    public const string COLUMNS_TRANSCOST_OF_CONTAINER_VOLUME = "Container_Volume";
    public const string COLUMNS_TRANSCOST_OF_CONTAINER_PRICE = "Container_Price";
    public const string COLUMNS_TRANSCOST_OF_UPDATEBY = "UpdateBy";
    public const string COLUMNS_TRANSCOST_OF_UPDATEDATE = "UpdateDate";
    #endregion TransCost

    #region InhousePart
    public const string COLUMNS_INHOUSE_OF_PACKING_MONTH = "Packing_Month";
    public const string COLUMNS_INHOUSE_OF_DC_PART_NO = "DC_Part_No";
    public const string COLUMNS_INHOUSE_OF_FACTORY_CODE = "Factory_Code";
    public const string COLUMNS_INHOUSE_OF_PART_NAME = "Part_Name";
    public const string COLUMNS_INHOUSE_OF_PART_COST = "Part_Cost";
    public const string COLUMNS_INHOUSE_OF_OTHER_COST = "Other_Cost";
    public const string COLUMNS_INHOUSE_OF_LOCATION_FROM = "Location_From";
    public const string COLUMNS_INHOUSE_OF_UPDATEBY = "UpdateBy";
    public const string COLUMNS_INHOUSE_OF_UPDATEDATE = "UpdateDate";
    #endregion InhousePart

    #region Policy
    public const double VALIDATE_VALUE_OF_ROYALTY = 100.000;
    public const double VALIDATE_VALUE_OF_SGA = 100.000;
    public const double VALIDATE_VALUE_OF_NET_PROFIT = 100.000;

    public static int VALIDATE_LENGTH_TWO_DECIMAL = 2;
    public static int VALIDATE_LENGTH_THREE_DECIMAL = 3;

    public const string COLUMNS_POLICY_OF_POLICY_ID = "Policy_Id";
    public const string COLUMNS_POLICY_OF_PACKING_MONTH = "Packing_Month";
    public const string COLUMNS_POLICY_OF_DIST_CODE = "Dist_Code";
    public const string COLUMNS_POLICY_OF_FACTORY_CODE = "Factory_Code";
    public const string COLUMNS_POLICY_OF_ROYALTY = "Royalty";
    public const string COLUMNS_POLICY_OF_SGA = "SGA";
    public const string COLUMNS_POLICY_OF_NET_PROFIT = "Net_Profit";
    public const string COLUMNS_POLICY_OF_UPDATEBY = "UpdateBy";
    public const string COLUMNS_POLICY_OF_UPDATEDATE = "UpdateDate";

    #endregion Policy

    #region ModelProcess
    public const string COLUMNS_MODEL_PROCESS_OF_MODEL_UPLOAD_ID = "Model_Upload_Id";
    public const string COLUMNS_MODEL_PROCESS_OF_QUOTATION_CONTROL_ID = "Quotaion_Control_Id";
    public const string COLUMNS_MODEL_PROCESS_OF_DIST_CODE = "Dist_Code";
    public const string COLUMNS_MODEL_PROCESS_OF_MODEL_CODE = "Model_Code";
    public const string COLUMNS_MODEL_PROCESS_OF_MODEL_CODE_COMPARE = "Model_Code_Compare";
    public const string COLUMNS_MODEL_PROCESS_OF_UPDATEBY = "UpdateBy";
    public const string COLUMNS_MODEL_PROCESS_OF_UPDATEDATE = "UpdateDate";
    #endregion ModelProcess

    #region QuoMainControl
    public const string COLUMNS_QUOTATION_MAINCOMTROL_OF_QUOTATION_CONTROL_ID = "Quotation_Control_Id";
    public const string COLUMNS_QUOTATION_MAINCOMTROL_OF_PACKING_MONTH = "Packing_Month";
    public const string COLUMNS_QUOTATION_MAINCOMTROL_OF_FACTORY_CODE = "Factory_Code";
    public const string COLUMNS_QUOTATION_MAINCOMTROL_OF_ISPROCESS_SALE_SCRAP = "IsProcess_Sale_Scrap";
    public const string COLUMNS_QUOTATION_MAINCOMTROL_OF_ISPROCESS_QUOTATION = "IsProcess_Quotation";
    public const string COLUMNS_QUOTATION_MAINCOMTROL_OF_CREATE_DATE = "Create_Date";
    public const string COLUMNS_QUOTATION_MAINCOMTROL_OF_PROCESS_QUOTATION_DATE = "Process_Quotation_Date";
    public const string COLUMNS_QUOTATION_MAINCOMTROL_OF_CREATEBY = "CreateBy";
    public const string COLUMNS_QUOTATION_MAINCOMTROL_OF_UPDATEBY = "UpdateBy";
    public const string COLUMNS_QUOTATION_MAINCOMTROL_OF_UPDATEDATE = "UpdateDate";
    #endregion QuoMainControl

    #region GroupDist
    public const string COLUMNS_GROUP_DIST_MAINTENANCE_OF_GROUP_DIST_ID = "Group_Dist_Id";
    public const string COLUMNS_GROUP_DIST_MAINTENANCE_OF_DIST_CODE = "Dist_Code";
    public const string COLUMNS_GROUP_DIST_MAINTENANCE_OF_DIST_NAME = "Dist_Name";
    public const string COLUMNS_GROUP_DIST_MAINTENANCE_OF_SUB_DIST_CODE = "Sub_Dist_Code";
    public const string COLUMNS_GROUP_DIST_MAINTENANCE_OF_FACTORY_CODE = "Factory_Code";
    public const string COLUMNS_GROUP_DIST_MAINTENANCE_OF_PLANT_CODE = "Plant_Code";
    public const string COLUMNS_GROUP_DIST_MAINTENANCE_OF_SUB_PLANT_CODE = "Sub_Plant_Code";
    public const string COLUMNS_GROUP_DIST_MAINTENANCE_OF_UPDATEBY = "UpdateBy";
    public const string COLUMNS_GROUP_DIST_MAINTENANCE_OF_UPDATEDATE = "UpdateDate";
    #endregion GroupDist

    #region Vanning
    public const string COLUMNS_VANNING_OF_VANNING_ID = "Vaning_Id";
    public const string COLUMNS_VANNING_OF_FACTORY_CODE = "Factory_Code";
    public const string COLUMNS_VANNING_OF_PC_NO = "PC_No";
    public const string COLUMNS_VANNING_OF_DC_PART_NO = "DC_Part_No";
    public const string COLUMNS_VANNING_OF_VANNING_DATE = "Vanning_Date";
    public const string COLUMNS_VANNING_OF_PART_NAME = "Part_Name";
    public const string COLUMNS_VANNING_OF_PACKING_PLAN_QTY = "Packing_Plan_Qty";
    public const string COLUMNS_VANNING_OF_PACKING_RESULT_QTY = "Packing_Result_Qty";
    public const string COLUMNS_VANNING_OF_UNIT_PRICE = "Unit_Price";
    public const string COLUMNS_VANNING_OF_TOTAL_AMOUNT = "Total_Amount";
    public const string COLUMNS_VANNING_OF_PACKING_UPATEBY = "UpdateBy";
    public const string COLUMNS_VANNING_OF_PACKING_UPDATE_DATE = "UpdateDate";


    public const int COLUMNS_VANNING_IMPORT_COUNT = 8;
    #endregion Vanning

    #region COPReport
    public const string COLUMNS_COPREPORT_OF_QUOTATION_CONTROL_ID = "Quotation_Control_Id";
    public const string COLUMNS_COPREPORT_OF_DIST_CODE = "Dist_Code";
    public const string COLUMNS_COPREPORT_OF_MODEL_CODE = "Model_Code";
    public const string COLUMNS_COPREPORT_OF_FACTORY_CODE = "Factory_Code";
    public const string COLUMNS_COPREPORT_OF_PACKING_MONTH = "Packing_Month";
    public const string COLUMNS_COPREPORT_OF_CHECK = "Chk";
    #endregion COPReport

    #region COPReasonCode
    public const int VALIDATE_COP_REASON_CODE_OF_REASON_CODE_MAX = 5;
    public const int VALIDATE_COP_REASON_CODE_OF_REASON_DESCRIPTION_MAX = 50;

    public const string COLUMNS_COP_REASON_CODE_OF_REASON_CODE_ID = "Reason_Code_Id";
    public const string COLUMNS_COP_REASON_CODE_OF_REASON_CODE = "Reason_Code1";
    public const string COLUMNS_COP_REASON_CODE_OF_REASON_DESCRIPTION = "Reason_Description";
    public const string COLUMNS_COP_REASON_CODE_OF_FACTORY_CODE = "Factory_Code";
    public const string COLUMNS_COP_REASON_CODE_OF_UPDATEBY = "UpdateBy";
    public const string COLUMNS_COP_REASON_CODE_OF_UPDATEDATE = "UpdateDate";
    #endregion COPReasonCode

    #region COPReasonMaintenance

    public const string COLUMNS_COP_REASON_MAINTENANCE_OF_BASIC_PART_ID = "BASIC_PART_ID";
    public const string COLUMNS_COP_REASON_MAINTENANCE_OF_BASIC_PART_NO = "BASIC_PART_NO";
    public const string COLUMNS_COP_REASON_MAINTENANCE_OF_DC_PART_NO = "DC_PART_NO";
    public const string COLUMNS_COP_REASON_MAINTENANCE_OF_PACKING_MONTH = "PACKING_MONTH";
    public const string COLUMNS_COP_REASON_MAINTENANCE_OF_LAST_MONTH = "LAST_MONTH";
    public const string COLUMNS_COP_REASON_MAINTENANCE_OF_DIFF = "DIFF";
    public const string COLUMNS_COP_REASON_MAINTENANCE_OF_COPDIFF = "COPDIFF";
    public const string COLUMNS_COP_REASON_MAINTENANCE_OF_MAIN_DIST = "MAIN_DIST";
    public const string COLUMNS_COP_REASON_MAINTENANCE_OF_DATE_PACKING_MONTH = "DATE_PACKING_MONTH";
    public const string COLUMNS_COP_REASON_MAINTENANCE_OF_DATE_LAST_MONTH = "DATE_LAST_MONTH";
    public const string COLUMNS_COP_REASON_MAINTENANCE_OF_FACTORY_CODE = "FACTORY_CODE";

    #endregion COPReasonMaintenance

    #region COPReasonMaintenanceView

    public const string COLUMNS_COP_REASON_MAINTENANCE_VIEW_OF_STRUCTURE_PART = "STRUCTURE_PART";
    public const string COLUMNS_COP_REASON_MAINTENANCE_VIEW_OF_MAKER_TO = "MAKER_TO";
    public const string COLUMNS_COP_REASON_MAINTENANCE_VIEW_OF_QTY = "QTY";
    public const string COLUMNS_COP_REASON_MAINTENANCE_VIEW_OF_REASON_DESCRIPTION = "REASON_DESCRIPTION";
    public const string COLUMNS_COP_REASON_MAINTENANCE_VIEW_OF_REASON_PURCHASE = "REASON_PURCHASE";
    public const string COLUMNS_COP_REASON_MAINTENANCE_VIEW_OF_REASON_PURCHASE_TOTAL = "REASON_PURCHASE_TOTAL";
    public const string COLUMNS_COP_REASON_MAINTENANCE_VIEW_OF_EDATE = "EDATE";
    public const string COLUMNS_COP_REASON_MAINTENANCE_VIEW_OF_PART_GROUP = "PART_GROUP";

    #endregion COPReasonMaintenanceView

    #region COPReasonMaintenanceEdit
    public const double VALIDATE_COP_REASON_MAINTENANCE_EDIT_OF_AMOUNT_ZERO = 0;
    public const double VALIDATE_COP_REASON_MAINTENANCE_EDIT_OF_AMOUNT_MIN = -999999.99;
    public const double VALIDATE_COP_REASON_MAINTENANCE_EDIT_OF_AMOUNT_MAX = 999999.99;

    public const string COLUMNS_COP_REASON_MAINTENANCE_EDIT_OF_REASON_ID = "Reason_Id";
    public const string COLUMNS_COP_REASON_MAINTENANCE_EDIT_OF_BASIC_PART_NO = "Basic_Part_Id";
    public const string COLUMNS_COP_REASON_MAINTENANCE_EDIT_OF_REASON_CODE_ID = "Reason_Code_Id";
    public const string COLUMNS_COP_REASON_MAINTENANCE_EDIT_OF_REASON_CODE = "Reason_Code";
    public const string COLUMNS_COP_REASON_MAINTENANCE_EDIT_OF_REASON_DESCRIPTION = "Reason_Description";
    public const string COLUMNS_COP_REASON_MAINTENANCE_EDIT_OF_AMOUNT_PER_UNIT = "Amount_Per_Unit";
    public const string COLUMNS_COP_REASON_MAINTENANCE_EDIT_OF_TOTAL_AMOUNT = "Total_Amount";
    public const string COLUMNS_COP_REASON_MAINTENANCE_EDIT_OF_PART_GROUP = "Part_Group";
    public const string COLUMNS_COP_REASON_MAINTENANCE_EDIT_OF_EFFECTIVE_DATE = "Effective_Date";
    public const string COLUMNS_COP_REASON_MAINTENANCE_EDIT_OF_UPDATE_BY = "UpdateBy";
    public const string COLUMNS_COP_REASON_MAINTENANCE_EDIT_OF_UPDATE_DATE = "UpdateDate";

    #endregion COPReasonMaintenanceEdit

    #region Model
    public const string COLUMNS_PART_LIST_REPORT_OF_MODEL_UPLOAD_ID = "Model_Upload_Id";
    public const string COLUMNS_PART_LIST_REPORT_OF_QUOTATION_CONTROL_ID = "Quotation_Control_Id";
    public const string COLUMNS_PART_LIST_REPORT_OF_DIST_CODE = "Dist_Code";
    public const string COLUMNS_PART_LIST_REPORT_OF_MODLE_CODE = "Model_Code";
    public const string COLUMNS_PART_LIST_REPORT_OF_MODEL_CODE_COMPARE = "Model_Code_Compare";
    public const string COLUMNS_PART_LIST_REPORT_OF_PACKING_MONTH = "Packing_Month";
    public const string COLUMNS_PART_LIST_REPORT_OF_CHECK = "Chk";
    #endregion

    #region PartList
    public const string COLUMNS_PART_LIST_OF_BASIC_PART_ID = "Basic_Part_Id";
    public const string COLUMNS_PART_LIST_OF_BASIC_PART_NO = "Basic_Part_No";
    public const string COLUMNS_PART_LIST_OF_FACTORY_CODE = "Factory_Code";
    public const string COLUMNS_PART_LIST_OF_PACKING_MONTH = "Packing_Month";
    public const string COLUMNS_PART_LIST_OF_PART_COLOR = "Part_Color";
    public const string COLUMNS_PART_LIST_OF_MODEL_CODE = "Model_Code";
    public const string COLUMNS_PART_LIST_OF_MAKER_CODE = "Maker_Code";
    public const string COLUMNS_PART_LIST_OF_DIST_CODE = "Dist_Code";
    public const string COLUMNS_PART_LIST_OF_DC_PART_NO = "DC_Part_No";
    public const string COLUMNS_PART_LIST_OF_COMPARE_STATUS = "Compare_Status";
    public const string COLUMNS_PART_LIST_OF_GEN_QUOTATION = "Gen_Quotation";
    public const string COLUMNS_PART_LIST_OF_PART_LEVEL = "Part_Level";
    public const string COLUMNS_PART_LIST_OF_MAIN_PART_ID = "Main_Part_Id";
    public const string COLUMNS_PART_LIST_OF_PART_GROUP = "Part_Group";
    public const string COLUMNS_PART_LIST_OF_PART_NAME = "Part_Name";
    public const string COLUMNS_PART_LIST_OF_MODEL_COLOR = "Model_Color";
    public const string COLUMNS_PART_LIST_OF_FRAME_2 = "Frame_2";
    public const string COLUMNS_PART_LIST_OF_TYPE = "Type";
    public const string COLUMNS_PART_LIST_OF_QTY = "Qty";
    public const string COLUMNS_PART_LIST_OF_LOCATION_FROM = "Location_From";
    public const string COLUMNS_PART_LIST_OF_LOCATION_TO = "Location_To";
    public const string COLUMNS_PART_LIST_OF_LOCATION_NEXT = "Location_Next";
    public const string COLUMNS_PART_LIST_OF_RUN_NO = "Run_No";
    public const string COLUMNS_PART_LIST_OF_UPDATEBY = "UpdateBy";
    public const string COLUMNS_PART_LIST_OF_UPDATEDATE = "UpdateDate";
    #endregion

    #region Inquiry
    public const string COLUMNS_INQUIRY_OF_DIST_CODE = "Dist_Code";
    public const string COLUMNS_INQUIRY_OF_DIST_NAME = "Dist_Name";
    public const string COLUMNS_INQUIRY_OF_PACKING_MONTH = "Packing_Month";
    public const string COLUMNS_INQUIRY_OF_CHK = "Chk";
    #endregion

    #region COPWSFOBInquiry
    public const string COLUMNS_COPWSFOBINQUIRY_OF_DIST_CODE = "Dist_Code";
    public const string COLUMNS_COPWSFOBINQUIRY_OF_DIST_NAME = "Dist_Name";
    public const string COLUMNS_COPWSFOBINQUIRY_OF_BASIC_PART_NO = "Basic_Part_No";
    public const string COLUMNS_COPWSFOBINQUIRY_OF_DC_PART_NO = "DC_Part_No";
    public const string COLUMNS_COPWSFOBINQUIRY_OF_COP = "COP";
    public const string COLUMNS_COPWSFOBINQUIRY_OF_WS_PRICE = "WS_Price";
    public const string COLUMNS_COPWSFOBINQUIRY_OF_ASH_FOB_BHT = "ASH_FOB_BHT";
    #endregion COPWSFOBInquiry

    #region UP_LOAD_FOB
    public const string COLUMNS_UP_LOAD_FOB_OF_FOB_Upload_Id = "FOB_Upload_Id";
    public const string COLUMNS_UP_LOAD_FOB_OF_Factory_Code = "Factory_Code";
    public const string COLUMNS_UP_LOAD_FOB_OF_Packing_Month = "Packing_Month";
    public const string COLUMNS_UP_LOAD_FOB_OF_Basic_Part_No = "Basic_Part_No";
    public const string COLUMNS_UP_LOAD_FOB_OF_DC_Part_No = "DC_Part_No";
    public const string COLUMNS_UP_LOAD_FOB_OF_Dist_Code = "Dist_Code";
    public const string COLUMNS_UP_LOAD_FOB_OF_Location_From = "Location_From";
    public const string COLUMNS_UP_LOAD_FOB_OF_Location_To = "Location_To";
    public const string COLUMNS_UP_LOAD_FOB_OF_Dist_Name = "Dist_Name";
    public const string COLUMNS_UP_LOAD_FOB_OF_ASH_FOB_BHT = "ASH_FOB_BHT";
    public const string COLUMNS_UP_LOAD_FOB_OF_ASH_FOB_USD = "ASH_FOB_US";
    public const string COLUMNS_UP_LOAD_FOB_OF_UpdateBy = "UpdateBy";
    public const string COLUMNS_UP_LOAD_FOB_OF_UpdateDate = "UpdateDate";
    #endregion UP_LOAD_FOB

    #region FOB_CSV
    public const string COLUMNS_FOB_CSV_OF_Plant_Code = "FOB_Upload_Id";
    public const string COLUMNS_FOB_CSV_OF_Part_Color = "Factory_Code";
    public const string COLUMNS_FOB_CSV_OF_DC_Part_No = "DC_Part_No";
    public const string COLUMNS_FOB_CSV_OF_ASH_FOB_BHT = "ASH_FOB_BHT";
    public const string COLUMNS_FOB_CSV_OF_UpdateTime = "UpdateTime";
    public const string COLUMNS_FOB_CSV_OF_UpdateDate = "UpdateDate";
    #endregion FOB_CSV

    #region Quotation_Info
    public const string COLUMNS_QUOTATION_INFO_BASIC_PART_ID = "Basic_Part_Id";
    public const string COLUMNS_QUOTATION_INFO_PACKING_COST = "Packing_Cost";
    public const string COLUMNS_QUOTATION_INFO_PACKING_CAPACITY = "Packing_Capacity";
    public const string COLUMNS_QUOTATION_INFO_LOCAL = "Local";
    public const string COLUMNS_QUOTATION_INFO_ROTATION = "Rotation";
    public const string COLUMNS_QUOTATION_INFO_INHOUSE_PROCESS_PRICE = "Inhouse_Process_Price";
    public const string COLUMNS_QUOTATION_INFO_INHOUSE_MATERIAL_COST = "Inhouse_Material_Cost";
    public const string COLUMNS_QUOTATION_INFO_DIE_COST = "Die_Cost";
    public const string COLUMNS_QUOTATION_INFO_IMPORT_PART_COST = "Import_Part_Cost";
    public const string COLUMNS_QUOTATION_INFO_IMPORT_EXPENSE = "Import_Expense";
    public const string COLUMNS_QUOTATION_INFO_COP = "COP";
    public const string COLUMNS_QUOTATION_INFO_SALE_SCRAP = "Sale_Scrap";
    public const string COLUMNS_QUOTATION_INFO_NET_COP = "Net_COP";
    public const string COLUMNS_QUOTATION_INFO_TRANSPORTATION_COST = "Transportation_Cost";
    public const string COLUMNS_QUOTATION_INFO_SGA = "SGA";
    public const string COLUMNS_QUOTATION_INFO_ROYALTY = "Royalty";
    public const string COLUMNS_QUOTATION_INFO_NET_PROFIT = "Net_Profit";
    public const string COLUMNS_QUOTATION_INFO_WS_PRICE = "WS_Price";
    public const string COLUMNS_QUOTATION_INFO_UPDATEBY = "UpdateBy";
    public const string COLUMNS_QUOTATION_INFO_UPDATEDATE = "UpdateDate";

    #endregion

    #region TranferDataReportPage
    public const string COLUMNS_TRANFER_DATA_OF_TOTAL_DIRECT_EXPENSE = "Total_Direct_Expense";
    public const string COLUMNS_TRANFER_DATA_OF_TOTAL_NET_COP = "Total_Net_COP";
    public const string COLUMNS_TRANFER_DATA_OF_DIRECT_EXPENSE_PERSEN = "Direct_Expense_persen";
    public const string COLUMNS_TRANFER_DATA_OF_ROYATY_PERSEN = "Royalty_persen";
    public const string COLUMNS_TRANFER_DATA_OF_ROYATY_PERSEN_FOB = "Royalty_persen_FOB";
    public const string COLUMNS_TRANFER_DATA_OF_SGA_PERSEN = "SGA_persen";
    public const string COLUMNS_TRANFER_DATA_OF_SGA = "SGA";
    public const string COLUMNS_TRANFER_DATA_OF_NET_PROFIT_PERSEN = "Net_Profit_persen";
    public const string COLUMNS_TRANFER_DATA_OF_NET_PROFIT_PERSEN_FOB = "Net_Profit_persen_FOB";
    public const string COLUMNS_TRANFER_DATA_OF_WHOLE_SALE_BAHT = "Whole_Sale_Baht";

    #endregion
}