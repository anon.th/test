﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Data.Linq;

public class MobileAppImpl
{
    private static string gloClassName = MethodBase.GetCurrentMethod().DeclaringType.Name;
    private static readonly ILog gloLog = LogManager.GetLogger(gloClassName);

    public ReturnErrorsDto ValidateLogin(ValidateDto ValidateDto)
    {

        gloLog.Info(string.Format(ApplicationConstant.LOG_PREFIX_IN, "ValidateLogin"));
        DBConnection fvDBConnection = new DBConnection();
        ReturnErrorsDto rtnError = new ReturnErrorsDto();
        try
        {

            fvDBConnection.BeginTransaction();

            string strDebug = System.Configuration.ConfigurationManager.AppSettings["Debug"].ToString();
            string Rawmessage = null;
            if (!string.IsNullOrEmpty(strDebug))
            {
                int intDebug = Convert.ToInt32(strDebug);
                if (intDebug == 1)
                {
                    Rawmessage = ValidateDto.Rawmessage;
                }
            }
            var fvResult = (from tblMobileApp in fvDBConnection.SharedContext
                                    .MobileApp_ValidateLogin(ValidateDto.Enterpriseid
                                    , ValidateDto.Phoneno, ValidateDto.Imei
                                    , ValidateDto.Userid
                                    , ValidateDto.User_Password
                                    , ValidateDto.Passwordis
                                    , Rawmessage)
                            select tblMobileApp
                                ).FirstOrDefault();

            if (fvResult != null)
            {
                rtnError = new ReturnErrorsDto();
                rtnError.ErrorCode = fvResult.ErrorCode;
                rtnError.ErrorMessage = fvResult.ErrorMessage;

                DMSService.WebUtils.WriteAppEventLog("IMEI : " + ValidateDto.Imei + " ||Method name : MobileAppImpl==>ValidateLogin", false);
                DMSService.WebUtils.WriteAppEventLog("IMEI : " + ValidateDto.Imei + " ||ErrorCode : " + rtnError.ErrorCode, false);
                DMSService.WebUtils.WriteAppEventLog("IMEI : " + ValidateDto.Imei + " ||ErrorMessage : " + rtnError.ErrorMessage, false);
            }

            fvDBConnection.CommitTransaction();
        }
        catch (Exception ex)
        {
            fvDBConnection.RollbackTransaction();
            gloLog.Error("MobileAppImpl|ValidateLogin|Exception : " + ex.Message);

            DMSService.WebUtils.WriteAppEventLog("IMEI : " + ValidateDto.Imei + " ||Method name : MobileAppImpl==>ValidateLogin", true);
            DMSService.WebUtils.WriteAppEventLog("IMEI : " + ValidateDto.Imei + " ||ErrorMessage : " + ex.Message, true);
            throw ex;
        }
        finally
        {
            if (fvDBConnection != null)
            {
                fvDBConnection.CloseDBContext();
            }

        }

        gloLog.Info(string.Format(ApplicationConstant.LOG_PREFIX_OUT, "GetFactoryInfo"));
        return rtnError;
    }

    public ReturnErrorsDto ValidateStatus(ValidateDto ValidateDto, DateTime? status_DT, string consignment_no, string consignee)
    {

        gloLog.Info(string.Format(ApplicationConstant.LOG_PREFIX_IN, "ValidateStatus"));
        DBConnection fvDBConnection = new DBConnection();
        ReturnErrorsDto rtnError = new ReturnErrorsDto();
        try
        {

            fvDBConnection.OpenConnection();

            string strDebug = System.Configuration.ConfigurationManager.AppSettings["Debug"].ToString();
            string Rawmessage = null;
            if (!string.IsNullOrEmpty(strDebug))
            {
                int intDebug = Convert.ToInt32(strDebug);
                if (intDebug == 1)
                {
                    Rawmessage = ValidateDto.Rawmessage;
                }
            }
            var fvResult = (from tblMobileApp in fvDBConnection.SharedContext
                                    .MobileApp_ValidateStatus(ValidateDto.Enterpriseid
                                    , ValidateDto.Phoneno
                                    , ValidateDto.Imei
                                    , ValidateDto.Userid
                                    , ValidateDto.User_Password
                                    , status_DT
                                    , consignment_no
                                    , consignee
                                    , ValidateDto.Passwordis
                                    , Rawmessage
                                    )
                            select tblMobileApp
                                ).FirstOrDefault();

            if (fvResult != null)
            {
                rtnError = new ReturnErrorsDto();
                rtnError.ErrorCode = fvResult.ErrorCode;
                rtnError.ErrorMessage = fvResult.ErrorMessage;

                DMSService.WebUtils.WriteAppEventLog("IMEI : " + ValidateDto.Imei + " ||Method name : MobileAppImpl==>ValidateStatus", false);
                DMSService.WebUtils.WriteAppEventLog("IMEI : " + ValidateDto.Imei + " ||ErrorCode : " + rtnError.ErrorCode, false);
                DMSService.WebUtils.WriteAppEventLog("IMEI : " + ValidateDto.Imei + " ||ErrorMessage : " + rtnError.ErrorMessage, false);
            }
        }
        catch (Exception ex)
        {
            gloLog.Error("MobileAppImpl|ValidateStatus|Exception : " + ex.Message);

            DMSService.WebUtils.WriteAppEventLog("IMEI : " + ValidateDto.Imei + " ||Method name : MobileAppImpl==>ValidateStatus", true);
            DMSService.WebUtils.WriteAppEventLog("IMEI : " + ValidateDto.Imei + " ||ErrorMessage : " + rtnError.ErrorMessage, true);
            throw ex;
        }
        finally
        {
            if (fvDBConnection != null)
            {
                fvDBConnection.CloseDBContext();
            }

        }
        gloLog.Info(string.Format(ApplicationConstant.LOG_PREFIX_OUT, "ValidateStatus"));
        return rtnError;
    }

    public ReturnErrorsDto GeneralStatus(ValidateDto ValidateDto, DateTime? status_DT, string consignment_no, string status_code, string remark)
    {

        gloLog.Info(string.Format(ApplicationConstant.LOG_PREFIX_IN, "GeneralStatus"));
        DBConnection fvDBConnection = new DBConnection();
        ReturnErrorsDto rtnError = new ReturnErrorsDto();
        try
        {

            fvDBConnection.OpenConnection();

            string strDebug = System.Configuration.ConfigurationManager.AppSettings["Debug"].ToString();
            string Rawmessage = null;
            if (!string.IsNullOrEmpty(strDebug))
            {
                int intDebug = Convert.ToInt32(strDebug);
                if (intDebug == 1)
                {
                    Rawmessage = ValidateDto.Rawmessage;
                }
            }

            if (remark.Length == 0)
            {
                remark = null;
            }

           var fvResult = (from tblMobileApp in fvDBConnection.SharedContext
                                    .MobileApp_GeneralStatus(ValidateDto.Enterpriseid
                                    , ValidateDto.Phoneno
                                    , ValidateDto.Imei
                                    , ValidateDto.Userid
                                    , ValidateDto.User_Password
                                    , status_DT
                                    , consignment_no
                                    , status_code
                                    , remark
                                    , ValidateDto.Passwordis
                                    , Rawmessage)
                            select tblMobileApp
                                ).FirstOrDefault();

            if (fvResult != null)
            {
                rtnError = new ReturnErrorsDto();
                rtnError.ErrorCode = fvResult.ErrorCode;
                rtnError.ErrorMessage = fvResult.ErrorMessage;

                DMSService.WebUtils.WriteAppEventLog("IMEI : " + ValidateDto.Imei + " ||Method name : MobileAppImpl==>GeneralStatus", false);
                DMSService.WebUtils.WriteAppEventLog("IMEI : " + ValidateDto.Imei + " ||ErrorCode : " + rtnError.ErrorCode, false);
                DMSService.WebUtils.WriteAppEventLog("IMEI : " + ValidateDto.Imei + " ||ErrorMessage : " + rtnError.ErrorMessage, false);
            }
        }
        catch (Exception ex)
        {
            gloLog.Error("MobileAppImpl|GeneralStatus|Exception : " + ex.Message);

            DMSService.WebUtils.WriteAppEventLog("IMEI : " + ValidateDto.Imei + " ||Method name : MobileAppImpl==>GeneralStatus", true);
            DMSService.WebUtils.WriteAppEventLog("IMEI : " + ValidateDto.Imei + " ||ErrorMessage : " + rtnError.ErrorMessage, true);
            throw ex;
        }
        finally
        {
            if (fvDBConnection != null)
            {
                fvDBConnection.CloseDBContext();
            }

        }
        gloLog.Info(string.Format(ApplicationConstant.LOG_PREFIX_OUT, "GeneralStatus"));
        return rtnError;
    }

    public ReturnErrorsDto ValidateStatusWithImage(ValidateDto ValidateDto, DateTime? status_DT, string consignment_no, string consignee, string image)
    {

        gloLog.Info(string.Format(ApplicationConstant.LOG_PREFIX_IN, "ValidateStatusWithImage"));
        DBConnection fvDBConnection = new DBConnection();
        ReturnErrorsDto rtnError = new ReturnErrorsDto();
        try
        {

            fvDBConnection.OpenConnection();

            string strDebug = System.Configuration.ConfigurationManager.AppSettings["Debug"].ToString();
            string Rawmessage = null;
            if (!string.IsNullOrEmpty(strDebug))
            {
                int intDebug = Convert.ToInt32(strDebug);
                if (intDebug == 1)
                {
                    Rawmessage = ValidateDto.Rawmessage;
                }
            }
            var fvResult = (from tblMobileApp in fvDBConnection.SharedContext
                                    .MobileApp_ValidateStatus_Image(ValidateDto.Enterpriseid
                                    , ValidateDto.Phoneno
                                    , ValidateDto.Imei
                                    , ValidateDto.Userid
                                    , ValidateDto.User_Password
                                    , status_DT
                                    , consignment_no
                                    , consignee
                                    , ValidateDto.Passwordis
                                    , Rawmessage
                                    , image)
                            select tblMobileApp
                                ).FirstOrDefault();

            if (fvResult != null)
            {
                rtnError = new ReturnErrorsDto();
                rtnError.ErrorCode = fvResult.ErrorCode;
                rtnError.ErrorMessage = fvResult.ErrorMessage;

                DMSService.WebUtils.WriteAppEventLog("IMEI : " + ValidateDto.Imei + " ||Method name : MobileAppImpl==> ValidateStatusWithImage", false);
                DMSService.WebUtils.WriteAppEventLog("IMEI : " + ValidateDto.Imei + " ||ErrorCode : " + rtnError.ErrorCode, false);
                DMSService.WebUtils.WriteAppEventLog("IMEI : " + ValidateDto.Imei + " ||ErrorMessage : " + rtnError.ErrorMessage, false);
            }
            else
            {


            }

        }
        catch (Exception ex)
        {
            gloLog.Error("MobileAppImpl|ValidateStatus|Exception : " + ex.Message);

            DMSService.WebUtils.WriteAppEventLog("IMEI : " + ValidateDto.Imei + " ||Method name : MobileAppImpl==> ValidateStatusWithImage ", true);
            DMSService.WebUtils.WriteAppEventLog("IMEI : " + ValidateDto.Imei + " ||ErrorMessage : " + rtnError.ErrorMessage, true);
            throw ex;
        }
        finally
        {
            if (fvDBConnection != null)
            {
                fvDBConnection.CloseDBContext();
            }

        }
        gloLog.Info(string.Format(ApplicationConstant.LOG_PREFIX_OUT, "ValidateStatusWithImage"));
        return rtnError;
    }

    public MobileApp_StatusCodesList StatusCode_Read()
    {
        MobileApp_StatusCodesList objList = new MobileApp_StatusCodesList();

        gloLog.Info(string.Format(ApplicationConstant.LOG_PREFIX_IN, "StatusCode_Read"));
        DBConnection fvDBConnection = new DBConnection();
        ReturnErrorsDto rtnError = new ReturnErrorsDto();
        try
        {

            fvDBConnection.OpenConnection();

            var vStatusCode = from stts in fvDBConnection.SharedContext.v_MobileApp_StatusCodes
                              where stts.enterpriseid == "PNGAF"
                              select stts;

            if (vStatusCode != null)
            {
                foreach (var objTmp in vStatusCode)
                {
                    MobileApp_StatusCodes objStts = new MobileApp_StatusCodes();
                    objStts.status_code = objTmp.status_code;
                    objList.Add(objStts);
                }

            }
        }
        catch (Exception ex)
        {
            gloLog.Error("MobileAppImpl|StatusCode_Read|Exception : " + ex.Message);
            DMSService.WebUtils.WriteAppEventLog("Method name : MobileAppImpl==>StatusCode_Read", true);
            throw ex;
        }
        finally
        {
            if (fvDBConnection != null)
            {
                fvDBConnection.CloseDBContext();
            }

        }
        gloLog.Info(string.Format(ApplicationConstant.LOG_PREFIX_OUT, "StatusCode_Read"));
        return objList;
    }

    public MobileApp_ConsignmentsList ShippingList_Read(string enterpriseID, string phoneNo, string imei, string userID, string userPwd, DateTime? statusDT, string shippingListNo, string rawMessage)
    {
        MobileApp_ConsignmentsList objList = new MobileApp_ConsignmentsList();

        gloLog.Info(string.Format(ApplicationConstant.LOG_PREFIX_IN, "ShippingList_Read"));
        DBConnection fvDBConnection = new DBConnection();
        ReturnErrorsDto rtnError = new ReturnErrorsDto();
        try
        {

            fvDBConnection.OpenConnection();
            ISingleResult<MobileApp_ScanShippingListResult1> result = fvDBConnection.SharedContext.MobileApp_ScanShippingList(enterpriseID, phoneNo, imei, userID, userPwd, statusDT, shippingListNo, 0, rawMessage);
            if (result != null)
            {
                foreach (var objTmp in result)
                {
                    MobileApp_Consignments objCons = new MobileApp_Consignments();
                    objCons.consignment_no = objTmp.consignment_no;
                    objCons.enterprise_name = objTmp.enterprise_name;
                    objCons.enterprise_address = objTmp.enterprise_address;
                    objCons.enterprise_telephone = objTmp.enterprise_telephone;
                    objCons.custid = objTmp.custid;
                    objCons.customer_address = objTmp.customer_address;
                    objCons.customer_zipcode = objTmp.customer_zipcode;
                    objCons.customer_telephone = objTmp.customer_telephone;
                    objCons.sender_name = objTmp.sender_name;
                    objCons.sender_address = objTmp.sender_address;
                    objCons.sender_zipcode = objTmp.sender_zipcode;
                    objCons.sender_telephone = objTmp.sender_telephone;                  
                    objCons.total_packages = objTmp.total_packages;
                    objCons.total_wt = objTmp.total_wt;
                    objCons.ref_no = objTmp.ref_no;
                    objCons.service_code = objTmp.service_code;
                    objCons.recipient_telephone = objTmp.recipient_telephone;
                    objCons.recipient_name = objTmp.recipient_name;
                    objCons.recipient_address = objTmp.recipient_address;
                    objCons.recipient_zipcode = objTmp.recipient_zipcode;
                    objCons.state_name = objTmp.state_name;
                    objCons.ShippingList_No = objTmp.ShippingList_No;
                    objCons.cust_name = objTmp.cust_name;
                    objCons.GoodsDescription = objTmp.GoodsDescription;
                    objCons.cost_centre = objTmp.cost_centre;
                    objList.Add(objCons);
                }

            }
        }
        catch (Exception ex)
        {
            gloLog.Error("MobileAppImpl|ShippingList_Read|Exception : " + ex.Message);
            DMSService.WebUtils.WriteAppEventLog("Method name : MobileAppImpl==>ShippingList_Read", true);
            throw ex;
        }
        finally
        {
            if (fvDBConnection != null)
            {
                fvDBConnection.CloseDBContext();
            }

        }
        gloLog.Info(string.Format(ApplicationConstant.LOG_PREFIX_OUT, "ShippingList_Read"));
        return objList;
    }
}
