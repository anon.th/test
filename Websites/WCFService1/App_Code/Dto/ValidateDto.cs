﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using System.Web;


[System.Serializable]
public class ValidateDto
{
    public string Enterpriseid { get; set; }
    public string Phoneno { get; set; }
    public string Imei { get; set; }
    public string Userid { get; set; }
    public string User_Password { get; set; }
    public byte Passwordis { get; set; }
    public string Rawmessage { get; set; }


}
