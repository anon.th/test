using System;
using System.Data;
using System.Text;
using com.common.DAL;
using com.common.classes;

namespace com.ties.classes
{
	/// <summary>
	/// Summary description for QuotationVAS.
	/// </summary>
	public class QuotationVAS
	{
		public QuotationVAS()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		public static DataSet GetQuotationVAS(String appID,String enterpriseID,String strCustID,String strQuotNo, int iQuotVer,String strVasList,String strType,String strDestZipCode)
		{
			DbConnection dbConApp = null;
			IDbCommand dbCmd = null;
			DataSet dsQtnVAS = null;
			String strQryLine = "";
			
			dbConApp = DbConnectionManager.GetInstance().GetDbConnection(appID,enterpriseID);

			if(dbConApp == null)
			{
				Logger.LogTraceError("TIESUtility","GetQuotationVAS","ERR001","dbConApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			if(strVasList.Length > 0)
			{
				int i = 0;
				
				String[] str = strVasList.Split(new char[]{':'});
				int iTotNo = str.Length;
				for(i=0;i<iTotNo;i++)
				{
					if(i == iTotNo - 1)
					{
						strQryLine = strQryLine+"'"+str[i]+"'";
					}
					else
					{
						strQryLine = strQryLine+"'"+str[i]+"'"+",";
					}
				}
			}

			StringBuilder strQry = new StringBuilder();
			strQry.Append("select a.vas_code,b.vas_description,a.surcharge");

			if(strType.Equals("C"))
			{
				strQry.Append(" from customer_quotation_vas a");
			}
			else if(strType.Equals("A"))
			{
				strQry.Append(" from agent_quotation_vas a");
			}

			strQry.Append(" ,vas b  where a.applicationid='");
			strQry.Append(appID);
			strQry.Append("' and a.enterpriseid = '");
			strQry.Append(enterpriseID);
			if(strType.Equals("C"))
			{
				strQry.Append("' and a.custid = '");
			}
			else if(strType.Equals("A"))
			{
				strQry.Append("' and a.agentid = '");
			}
			strQry.Append(strCustID+"'");
			strQry.Append(" and a.quotation_no = '");
			strQry.Append(strQuotNo);
			strQry.Append("' and a.quotation_version = ");
			strQry.Append(iQuotVer);
			if(strQryLine.Length > 0)
			{
				strQry.Append(" and a.vas_code not in ("+strQryLine+")");
			}
			strQry.Append(" and b.applicationid = '");
			strQry.Append(appID);
			strQry.Append("' and b.enterpriseid = '");
			strQry.Append(enterpriseID);
			strQry.Append("' and b.vas_code = a.vas_code");
			strQry.Append(" and a.vas_code not in (select vas_code from zipcode_vas_excluded c where c.applicationid = '");
			strQry.Append(appID);
			strQry.Append("' and c.enterpriseid = '");
			strQry.Append(enterpriseID);
			strQry.Append("'");
			
			if((strDestZipCode != "") && (strDestZipCode != null))
			{
				strQry.Append(" and zipcode = '");
				strQry.Append(strDestZipCode);
				strQry.Append("'");
			}
			strQry.Append(")");

			dbCmd = dbConApp.CreateCommand(strQry.ToString(),CommandType.Text);
			try
			{
				dsQtnVAS =(DataSet)dbConApp.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
			}
			catch(ApplicationException appExpection)
			{
				Logger.LogTraceError("TIESUtility","GetQuotationVAS","ERR002","Error in the query String");
				throw appExpection;
			}
			
			return dsQtnVAS;
		}
	}
}
