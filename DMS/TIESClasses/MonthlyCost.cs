using System;

namespace  com.ties.classes
{
	public struct MonthlyCostAccount
	{
		public decimal iJobID;
		public String strCostCode;
		public int iAccountYear;
		public short iAccountMonth;
		public decimal decCostAmt;
		public String strAllocated;
	}

	public struct ShipmentCost
	{
		public decimal iJobID;
		public int iBookingNo;
		public String strConsignmentNo;
		public String strCostCode;
		public decimal decAmtAllocByConsignment;
		public decimal decAmtAllocByWt;
		public decimal decAmtAllocByDimWt;
	}
	

}
