using System;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

namespace TIESClasses
{
	[Serializable()]
	public class CustomsExpFrCollectRates
	{
		private string action = string.Empty;
		private string enterpriseId = string.Empty;
		private string userLoggedin  = string.Empty;
		private string weightFrom  = string.Empty;
		private string weightTo  = string.Empty;
		private string newEffectiveDate  = string.Empty;
		private string zone  = string.Empty;
		private string effectiveDate  = string.Empty;
		private string maxWeight  = string.Empty;
		private string rateToHub  = string.Empty;
		private string rateToOthers  = string.Empty;
		private string debug  = string.Empty;

		public string Action { set { action = value; } get { return action; } }
		public string EnterpriseId { set { enterpriseId = value; } get { return enterpriseId; } }
		public string UserLoggedin { set { userLoggedin = value; } get { return userLoggedin; } }
		public string WeightFrom { set { weightFrom = value; } get { return weightFrom; } }
		public string WeightTo { set { weightTo = value; } get { return weightTo; } }
		public string NewEffectiveDate { set { newEffectiveDate = value; } get { return newEffectiveDate; } }
		public string Zone { set { zone = value; } get { return zone; } }
		public string EffectiveDate { set { effectiveDate = value; } get { return effectiveDate; } }
		public string MaxWeight { set { maxWeight = value; } get { return maxWeight; } }
		public string RateToHub { set { rateToHub = value; } get { return rateToHub; } }
		public string RateToOthers { set { rateToOthers = value; } get { return rateToOthers; } }
		public string Debug { set { debug = value; } get { return debug; } }

		public CustomsExpFrCollectRates()
		{
		}
	}
}
