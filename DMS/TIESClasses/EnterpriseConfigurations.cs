using System;
using System.Text;
using System.Data;
using System.Runtime.Serialization;

namespace com.ties.classes
{
	public class EnterpriseConfigurations
	{
		public static DomesticShipmentConfigurations  DomesticShipment(DataSet ds)
		{
			DomesticShipmentConfigurations conf = new DomesticShipmentConfigurations();
			DataConfigurations dataConf= new DataConfigurations(ds);
			conf.DaysUntilBookingExpires=dataConf.DaysUntilBookingExpires;
			conf.HCSupportedbyEnterprise=dataConf.HCSupportedbyEnterprise;
			conf.CODSupportedbyEnterprise=dataConf.CODSupportedbyEnterprise;
			conf.InsSupportedbyEnterprise=dataConf.InsSupportedbyEnterprise;
			conf.CommCodeOnShipment=dataConf.CommCodeOnShipment;
			conf.ESAupportedbyEnterprise = dataConf.ESAupportedbyEnterprise;
			conf.PodexSurcharge = dataConf.PodexSurcharge;
			conf.MbgAmount = dataConf.MbgAmount;
			conf.ShowAdvancedInvoing = dataConf.ShowAdvancedInvoing;
			return conf;
		}

		public static CustomerProfileConfigurations  CustomerProfile(DataSet ds)
		{
			CustomerProfileConfigurations conf = new CustomerProfileConfigurations();
			DataConfigurations dataConf= new DataConfigurations(ds);
			conf.HCSupportedbyEnterprise=dataConf.HCSupportedbyEnterprise;
			conf.CODSupportedbyEnterprise=dataConf.CODSupportedbyEnterprise;
			conf.InsSupportedbyEnterprise=dataConf.InsSupportedbyEnterprise;
			conf.ESAupportedbyEnterprise=dataConf.ESAupportedbyEnterprise;
			conf.CustomsFrame = dataConf.CustomsFrame;
			conf.CartageWeightBreak = dataConf.CartageWeightBreak;
			conf.CreditControlBPDFrame = dataConf.CreditControlBPDFrame;
			
			return conf;
		}

		public static InvoiceGenerationPreviewConfigurations  InvoiceGenerationPreview(DataSet ds)
		{
			InvoiceGenerationPreviewConfigurations conf = new InvoiceGenerationPreviewConfigurations();
			DataConfigurations dataConf= new DataConfigurations(ds);
			conf.InsSupportedbyEnterprise=dataConf.InsSupportedbyEnterprise;
			conf.ESAupportedbyEnterprise=dataConf.ESAupportedbyEnterprise;
			conf.GST=dataConf.PodexSurcharge;
			
			return conf;
		}

		public static InvoiceManagementListingConfigurations InvoiceManagementListing(DataSet ds)
		{
			InvoiceManagementListingConfigurations conf = new InvoiceManagementListingConfigurations();
			DataConfigurations dataConf= new DataConfigurations(ds);
			conf.ShowAdvancedInvoicing=dataConf.ShowAdvancedInvoing;
			conf.InvoiceManagementApproveSelected=dataConf.InvoiceManagementApproveSelected;
			conf.InvoiceManagementPrintHeaderSelected=dataConf.InvoiceManagementPrintHeaderSelected;
			conf.InvoiceManagementPrintDetailSelected=dataConf.InvoiceManagementPrintDetailSelected;
			conf.InvoiceManagementPrintDetailPkgSelected=dataConf.InvoiceManagementPrintDetailPkgSelected;
			
			return conf;
		}

		public static ManifestFormsConfigurations  ManifestForms(DataSet ds)
		{
			ManifestFormsConfigurations conf = new ManifestFormsConfigurations();
			DataConfigurations dataConf= new DataConfigurations(ds);
			conf.DaysUntilBookingExpires=dataConf.DaysUntilBookingExpires;
			return conf;
		}

		public static PackageWtDimConfigurations  PackageWtDimForms(DataSet ds)
		{
			PackageWtDimConfigurations conf = new PackageWtDimConfigurations();
			foreach(DataRow dr in ds.Tables[0].Rows)
			{
				if(dr["key"].ToString() =="DisplayOnly")
				{
					if(dr["value"].ToString() == "MPS Number")
					{
						conf.Mpsnumber=false;
					}
					else if(dr["value"].ToString() == "Quantity")
					{
						conf.Quantity=false;
					}
					else if(dr["value"].ToString() == "Weight")
					{
						conf.Weight=false;
					}
					else if(dr["value"].ToString() == "Length")
					{
						conf.Length=false;
					}
					else if(dr["value"].ToString() == "Breadth")
					{
						conf.Breadth=false;
					}
					else if(dr["value"].ToString() == "Height")
					{
						conf.Height=false;
					}
				}
				else if (dr["key"].ToString() == "DefaultFocus")
				{
					if(dr["value"].ToString() == "MPS Number")
					{
						conf.DefaultFocus = "MPS Number";
					}
					else if(dr["value"].ToString() == "Quantity")
					{
						conf.DefaultFocus = "Quantity";
					}
					else if(dr["value"].ToString() == "Weight")
					{
						conf.DefaultFocus = "Weight";
					}
					else if(dr["value"].ToString() == "Length")
					{
						conf.DefaultFocus = "Length";
					}
					else if(dr["value"].ToString() == "Breadth")
					{
						conf.DefaultFocus = "Breadth";
					}
					else if(dr["value"].ToString() == "Height")
					{
						conf.DefaultFocus = "Height";
					}
				}
			}
			return conf;
		}

		public static QueryShipmentTrackingConfigurations  QueryShipmentTracking(DataSet ds)
		{
			QueryShipmentTrackingConfigurations conf = new QueryShipmentTrackingConfigurations();
			
			foreach(DataRow dr in ds.Tables[0].Rows)
			{
				if(dr["key"].ToString()== "CreditControlInScope")
				{
					if(dr["value"].ToString() == "N")
					{
						conf.CreditControlInScope= false;
					} 
					else 
					{
						conf.CreditControlInScope= true;
					}
				}
				else if(dr["key"].ToString() == "InvoicingInScope" )
				{
					if(dr["value"].ToString() == "N")
					{
						conf.InvoicingInScope= false;
					} 
					else 
					{
						conf.InvoicingInScope= true;
					}
				}						
			}
			return conf;
		}

		public static CreateUpdateConsignmentConfigurations  CreateUpdateConsignmentPage(DataSet ds)
		{
			CreateUpdateConsignmentConfigurations conf = new CreateUpdateConsignmentConfigurations();
			
			foreach(DataRow dr in ds.Tables[0].Rows)
			{
				if(dr["key"].ToString()== "HCSupportedbyEnterprise")
				{
					if(dr["value"].ToString() == "N")
					{
						conf.HCSupportedbyEnterprise= false;
					} 
					else 
					{
						conf.HCSupportedbyEnterprise= true;
					}
				}
				else if(dr["key"].ToString() == "CODSupportedbyEnterprise" )
				{
					if(dr["value"].ToString() == "N")
					{
						conf.CODSupportedbyEnterprise= false;
					} 
					else 
					{
						conf.CODSupportedbyEnterprise= true;
					}
				}		
				else if(dr["key"].ToString() == "InsSupportedbyEnterprise" )
				{
					if(dr["value"].ToString() == "N")
					{
						conf.InsSupportedbyEnterprise= false;
					} 
					else 
					{
						conf.InsSupportedbyEnterprise= true;
					}
				}	
				else if(dr["key"].ToString() == "DangerousGoods" )
				{
					if(dr["value"].ToString() == "N")
					{
						conf.DangerousGoods= false;
					} 
					else 
					{
						conf.DangerousGoods= true;
					}
				}	
				else if(dr["key"].ToString() == "MaxCODAmount" )
				{
					try
					{
						conf.MaxCODAmount=Convert.ToDecimal(dr["value"].ToString());
					}
					catch
					{
						conf.MaxCODAmount=0;
					}
				}	
				else if(dr["key"].ToString() == "MaxDeclaredValue" )
				{
					try
					{
						conf.MaxDeclaredValue=Convert.ToDecimal(dr["value"].ToString());
					}
					catch
					{
						conf.MaxDeclaredValue=0;
					}
				}	
				else if(dr["key"].ToString() == "DangerousGoodsService" )
				{
					conf.DangerousGoodsService=dr["value"].ToString().Trim();
				}	
				else if(dr["key"].ToString() == "GoodsDescriptionRequired" )
				{
					if(dr["value"].ToString() == "1")
					{
						conf.GoodsDescriptionRequired= true;
					} 
					else 
					{
						conf.GoodsDescriptionRequired= false;
					}
				}	
			}
			return conf;
		}


		public static ConsignmentStatusPrintingConfigurations  CreateConsignmentStatusPrintingPage(DataSet ds)
		{
			ConsignmentStatusPrintingConfigurations conf = new ConsignmentStatusPrintingConfigurations();
			
			foreach(DataRow dr in ds.Tables[0].Rows)
			{
				if(dr["key"].ToString()== "ConsignmentStatusGrid")
				{
					try
					{
						conf.ConsignmentStatusGrid = Int32.Parse(dr["value"].ToString());
					}
					catch
					{
						conf.ConsignmentStatusGrid=10;
					}
				}
			}
			return conf;
		}


		public static PackageDetailsLimitsConfigurations  CreatePackageDetailsLimitsConfigurations(DataSet ds)
		{
			PackageDetailsLimitsConfigurations conf = new PackageDetailsLimitsConfigurations();
			
			foreach(DataRow dr in ds.Tables[0].Rows)
			{
				if(dr["key"].ToString()== "VolumeLimit")
				{
					try
					{
						conf.VolumeLimit=Convert.ToDecimal(dr["value"].ToString());
					}
					catch(Exception ex)
					{
						throw ex;
					}
				}
				else if(dr["key"].ToString() == "PkgWtLimit" )
				{
					try
					{
						conf.PkgWtLimit=Convert.ToDecimal(dr["value"].ToString());
					}
					catch(Exception ex)
					{
						throw ex;
					}
				}		
				else if(dr["key"].ToString() == "PkgQtyLimit" )
				{
					try
					{
						conf.PkgQtyLimit=Convert.ToDecimal(dr["value"].ToString());
					}
					catch(Exception ex)
					{
						throw ex;
					}
				}	
				else if(dr["key"].ToString() == "PkgRowWtLimit" )
				{
					try
					{
						conf.PkgRowWtLimit=Convert.ToDecimal(dr["value"].ToString());
					}
					catch(Exception ex)
					{
						throw ex;
					}
				}
				else if(dr["key"].ToString() == "DensityFactor" )
				{
					try
					{
						conf.DensityFactor=Convert.ToDecimal(dr["value"].ToString());
					}
					catch(Exception ex)
					{
						throw ex;
					}
				}	
			}
			return conf;
		}

	
		public static MaintainPhonesIMEIsConfigurations  MaintainPhonesIMEIs(DataSet ds)
		{
			MaintainPhonesIMEIsConfigurations conf = new MaintainPhonesIMEIsConfigurations();
			foreach(DataRow dr in ds.Tables[0].Rows)
			{
				if(dr["key"].ToString()== "GridRows")
				{
					try
					{
						conf.GridRows = Convert.ToInt32(dr["value"].ToString());
					}
					catch(Exception ex)
					{
						throw ex;
					}
				}
			}
			return conf;
		}
	}

	public class DataConfigurations  
	{
		private int _DaysUntilBookingExpires=5;
		private bool _HCSupportedbyEnterprise=false;
		private bool _CODSupportedbyEnterprise=false;
		private bool _InsSupportedbyEnterprise=false;
		private bool _CommCodeOnShipment=false;
		private bool _CustomsFrame=false;
		private bool _ESAupportedbyEnterprise=false;
		private string _CartageWeightBreak=string.Empty;
		private bool _CreditControlBPDFrame=false;
		private bool _PodexSurcharge=false;
		private bool _MbgAmount=false;
		private bool _ShowAdvancedInvoing=false;
		private bool _InvoiceManagementApproveSelected=false;
		private bool _InvoiceManagementPrintHeaderSelected=false;
		private bool _InvoiceManagementPrintDetailSelected=false;
		private bool _InvoiceManagementPrintDetailPkgSelected=false;

		public DataConfigurations(DataSet ds)
		{
			if(ds.Tables.Count > 0 && (ds.Tables[0].Columns["key"] != null && ds.Tables[0].Columns["value"] != null))
			{
				foreach(DataRow dr in ds.Tables[0].Rows)
				{
					string str_value = dr["value"].ToString();
					switch(dr["key"].ToString())
					{
						case "DaysUntilBookingExpires":
							try
							{
								_DaysUntilBookingExpires = int.Parse(str_value);
							}
							catch
							{

							}
							break;

						case "HCSupportedbyEnterprise":
							if(str_value =="Y")
							{
								_HCSupportedbyEnterprise = true;
							}
							break;
						case "ESASupportedbyEnterprise":
							if(str_value =="Y")
							{
								_ESAupportedbyEnterprise = true;
							}
							break;
						case "CODSupportedbyEnterprise":
							if(str_value =="Y")
							{
								_CODSupportedbyEnterprise = true;
							}
							break;
						case "InsSupportedbyEnterprise":
							if(str_value =="Y")
							{
								_InsSupportedbyEnterprise = true;
							}
							break;
						case "CommCodeOnShipment":
							if(str_value =="Y")
							{
								_CommCodeOnShipment = true;
							}
							break;
						case "CustomsFrame":
							if(str_value =="Y")
							{
								_CustomsFrame = true;
							}
							break;
						case "CartageWeightBreak":
							if(str_value != null)
							{
								_CartageWeightBreak = str_value;
							}
							break;
						case "CreditControlBPDFrame":
							if(str_value =="Y")
							{
								_CreditControlBPDFrame = true;
							}
							break;
						case "CreditControlModule":
							if(str_value =="Y")
							{
								_CreditControlBPDFrame = true;
							}
							break;
						case "PODEXReqdonServiceFailure":
							if(str_value =="Y")
							{
								_PodexSurcharge = true;
							}
							break;
						case "MBGPossibleonServiceFailure":
							if(str_value =="Y")
							{
								_MbgAmount = true;
							}
							break;
						case "ShowAdvancedInvoicing":
							if(str_value =="Y")
							{
								_ShowAdvancedInvoing = true;
							}
							break;
						case "InvoiceManagementApproveSelected":
							if(str_value =="Y")
							{
								_InvoiceManagementApproveSelected = true;
							}
							break;
						case "InvoiceManagementPrintHeaderSelected":
							if(str_value =="Y")
							{
								_InvoiceManagementPrintHeaderSelected = true;
							}
							break;
						case "InvoiceManagementPrintDetailSelected":
							if(str_value =="Y")
							{
								_InvoiceManagementPrintDetailSelected = true;
							}
							break;
						case "InvoiceManagementPrintDetailPkgSelected":
							if(str_value =="Y")
							{
								_InvoiceManagementPrintDetailPkgSelected = true;
							}
							break;

						default:
							break;
					}
				}
			}

		}


		public bool PodexSurcharge
		{
			get{return _PodexSurcharge; }
			set{_PodexSurcharge=value;}
		}

		public bool MbgAmount
		{
			get{return _MbgAmount; }
			set{_MbgAmount=value;}
		}

		public bool ShowAdvancedInvoing
		{
			get{return _ShowAdvancedInvoing; }
			set{_ShowAdvancedInvoing=value;}
		}

		public bool CreditControlBPDFrame
		{
			get{return _CreditControlBPDFrame; }
			set{_CreditControlBPDFrame=value;}
		}

		public int DaysUntilBookingExpires
		{
			get{return _DaysUntilBookingExpires;}
			set{_DaysUntilBookingExpires=value;}
		}

		public bool HCSupportedbyEnterprise
		{
			get{return _HCSupportedbyEnterprise;}
			set{_HCSupportedbyEnterprise=value;}
		}
		public bool ESAupportedbyEnterprise
		{
			get{return _ESAupportedbyEnterprise;}
			set{_ESAupportedbyEnterprise=value;}
		}
		public bool CODSupportedbyEnterprise
		{
			get{return _CODSupportedbyEnterprise;}
			set{_CODSupportedbyEnterprise=value;}
		}
		public bool InsSupportedbyEnterprise
		{
			get{return _InsSupportedbyEnterprise;}
			set{_InsSupportedbyEnterprise=value;}
		}
		public bool CommCodeOnShipment
		{
			get{return _CommCodeOnShipment;}
			set{_CommCodeOnShipment=value;}
		}

		public bool CustomsFrame
		{
			get{return _CustomsFrame;}
			set{_CustomsFrame=value;}
		}

		public string CartageWeightBreak
		{
			get{return _CartageWeightBreak;}
			set{_CartageWeightBreak=value;}
		}


		public bool InvoiceManagementApproveSelected
		{
			get{return _InvoiceManagementApproveSelected;}
			set{_InvoiceManagementApproveSelected=value;}
		}

		public bool InvoiceManagementPrintHeaderSelected
		{
			get{return _InvoiceManagementPrintHeaderSelected;}
			set{_InvoiceManagementPrintHeaderSelected=value;}
		}

		public bool InvoiceManagementPrintDetailSelected
		{
			get{return _InvoiceManagementPrintDetailSelected;}
			set{_InvoiceManagementPrintDetailSelected=value;}
		}

		public bool InvoiceManagementPrintDetailPkgSelected
		{
			get{return _InvoiceManagementPrintDetailPkgSelected;}
			set{_InvoiceManagementPrintDetailPkgSelected=value;}
		}
	}

	[Serializable()]
	public class DomesticShipmentConfigurations  
	{
		private int _DaysUntilBookingExpires=5;
		private bool _HCSupportedbyEnterprise=false;
		private bool _CODSupportedbyEnterprise=false;
		private bool _InsSupportedbyEnterprise=false;
		private bool _CommCodeOnShipment=false;
		private bool _ESAupportedbyEnterprise= false;
		private bool _CreditControlBPDFrame=false;
		private bool _PodexSurcharge=false;
		private bool _MbgAmount=false;
		private bool _ShowAdvancedInvoing=false;

		public DomesticShipmentConfigurations()
		{

		}
		public bool PodexSurcharge
		{
			get{return _PodexSurcharge;}
			set{_PodexSurcharge=value;}
		}
		public bool MbgAmount
		{
			get{return _MbgAmount;}
			set{_MbgAmount=value;}
		}
		public bool ShowAdvancedInvoing
		{
			get{return _ShowAdvancedInvoing;}
			set{_ShowAdvancedInvoing=value;}
		}
		public bool CreditControlBPDFrame
		{
			get{return _CreditControlBPDFrame;}
			set{_CreditControlBPDFrame=value;}
		}

		public int DaysUntilBookingExpires
		{
			get{return _DaysUntilBookingExpires;}
			set{_DaysUntilBookingExpires=value;}
		}

		public bool ESAupportedbyEnterprise
		{
			get{return _ESAupportedbyEnterprise;}
			set{_ESAupportedbyEnterprise=value;}
		}
		public bool HCSupportedbyEnterprise
		{
			get{return _HCSupportedbyEnterprise;}
			set{_HCSupportedbyEnterprise=value;}
		}
		public bool CODSupportedbyEnterprise
		{
			get{return _CODSupportedbyEnterprise;}
			set{_CODSupportedbyEnterprise=value;}
		}
		public bool InsSupportedbyEnterprise
		{
			get{return _InsSupportedbyEnterprise;}
			set{_InsSupportedbyEnterprise=value;}
		}
		public bool CommCodeOnShipment
		{
			get{return _CommCodeOnShipment;}
			set{_CommCodeOnShipment=value;}
		}
	}

	public class PackageWtDimConfigurations  
	{
		private bool _Mpsnumber=true;
		private bool _Quantity=true;
		private bool _Weight=true;
		private bool _Length=true;
		private bool _Breadth=true;
		private bool _Height=true;

		private string _DefaultFocus = "";

		public PackageWtDimConfigurations()
		{

		}

		public bool Quantity
		{
			get{return _Quantity;}
			set{_Quantity=value;}
		}

		public bool Mpsnumber
		{
			get{return _Mpsnumber;}
			set{_Mpsnumber=value;}
		}

		public bool Weight
		{
			get{return _Weight;}
			set{_Weight=value;}
		}

		public bool Length
		{
			get{return _Length;}
			set{_Length=value;}
		}

		public bool Breadth
		{
			get{return _Breadth;}
			set{_Breadth=value;}
		}

		public bool Height
		{
			get{return _Height;}
			set{_Height=value;}
		}

		public string DefaultFocus
		{
			get{return _DefaultFocus;}
			set{_DefaultFocus=value;}
		}
	}

	[Serializable()]
	public class CustomerProfileConfigurations 
	{
		private bool _HCSupportedbyEnterprise=false;
		private bool _CODSupportedbyEnterprise=false;
		private bool _InsSupportedbyEnterprise=false;
		private bool _ESAupportedbyEnterprise=false;
		private bool _CustomsFrame=false;
		private bool _CreditControlBPDFrame=false;
		private string _CartageWeightBreak= string.Empty;

		public CustomerProfileConfigurations()
		{

		}

		public bool CreditControlBPDFrame
		{
			get{return _CreditControlBPDFrame;}
			set{_CreditControlBPDFrame=value;}
		}
		public bool ESAupportedbyEnterprise
		{
			get{return _ESAupportedbyEnterprise;}
			set{_ESAupportedbyEnterprise=value;}
		}
		public bool HCSupportedbyEnterprise
		{
			get{return _HCSupportedbyEnterprise;}
			set{_HCSupportedbyEnterprise=value;}
		}
		public bool CODSupportedbyEnterprise
		{
			get{return _CODSupportedbyEnterprise;}
			set{_CODSupportedbyEnterprise=value;}
		}
		public bool InsSupportedbyEnterprise
		{
			get{return _InsSupportedbyEnterprise;}
			set{_InsSupportedbyEnterprise=value;}
		}

		public bool CustomsFrame
		{
			get{return _CustomsFrame;}
			set{_CustomsFrame = value;}
		}

		public string CartageWeightBreak
		{
			get{return _CartageWeightBreak;}
			set{_CartageWeightBreak = value;}
		}
	}

	[Serializable()]
	public class InvoiceGenerationPreviewConfigurations 
	{
		private bool _InsSupportedbyEnterprise=false;
		private bool _ESAupportedbyEnterprise=false;
		private bool _GST=false;

		public InvoiceGenerationPreviewConfigurations()
		{

		}

		public bool InsSupportedbyEnterprise
		{
			get{return _InsSupportedbyEnterprise;}
			set{_InsSupportedbyEnterprise=value;}
		}

		public bool ESAupportedbyEnterprise
		{
			get{return _ESAupportedbyEnterprise;}
			set{_ESAupportedbyEnterprise=value;}
		}

		public bool GST
		{
			get{return _GST;}
			set{_GST=value;}
		}

	}

	[Serializable()]
	public class InvoiceManagementListingConfigurations 
	{
		private bool _ShowAdvancedInvoicing=false;
		private bool _InvoiceManagementApproveSelected=false;
		private bool _InvoiceManagementPrintHeaderSelected=false;
		private bool _InvoiceManagementPrintDetailSelected=false;
		private bool _InvoiceManagementPrintDetailPkgSelected=false;

		public InvoiceManagementListingConfigurations()
		{

		}

		public bool ShowAdvancedInvoicing
		{
			get{return _ShowAdvancedInvoicing;}
			set{_ShowAdvancedInvoicing=value;}
		}

		public bool InvoiceManagementApproveSelected
		{
			get{return _InvoiceManagementApproveSelected;}
			set{_InvoiceManagementApproveSelected=value;}
		}

		public bool InvoiceManagementPrintHeaderSelected
		{
			get{return _InvoiceManagementPrintHeaderSelected;}
			set{_InvoiceManagementPrintHeaderSelected=value;}
		}

		public bool InvoiceManagementPrintDetailSelected
		{
			get{return _InvoiceManagementPrintDetailSelected;}
			set{_InvoiceManagementPrintDetailSelected=value;}
		}

		public bool InvoiceManagementPrintDetailPkgSelected
		{
			get{return _InvoiceManagementPrintDetailPkgSelected;}
			set{_InvoiceManagementPrintDetailPkgSelected=value;}
		}

	}


	[Serializable()]
	public class ManifestFormsConfigurations
	{
		private int _DaysUntilBookingExpires=5;

		public ManifestFormsConfigurations()
		{

		}

		public int DaysUntilBookingExpires
		{
			get{return _DaysUntilBookingExpires;}
			set{_DaysUntilBookingExpires=value;}
		}
	}


	[Serializable()]
	public class QueryShipmentTrackingConfigurations 
	{
		private bool _CreditControlInScope=false;
		private bool _InvoicingInScope=false;

		public QueryShipmentTrackingConfigurations()
		{

		}

		public bool CreditControlInScope
		{
			get{return _CreditControlInScope;}
			set{_CreditControlInScope=value;}
		}

		public bool InvoicingInScope
		{
			get{return _InvoicingInScope;}
			set{_InvoicingInScope=value;}
		}
	}

	[Serializable()]
	public class ConsignmentStatusPrintingConfigurations 
	{
		private int _ConsignmentStatusGrid=20;

		public ConsignmentStatusPrintingConfigurations()
		{

		}

		public int ConsignmentStatusGrid
		{
			get{return _ConsignmentStatusGrid;}
			set{_ConsignmentStatusGrid=value;}
		}
	}

	[Serializable()]
	public class CreateUpdateConsignmentConfigurations 
	{
		private bool _HCSupportedbyEnterprise=true;
		private bool _CODSupportedbyEnterprise=true;
		private bool _InsSupportedbyEnterprise=true;
		private bool _DangerousGoods=true;
		private decimal _MaxCODAmount=0;
		private decimal _MaxDeclaredValue=0;
		private string _DangerousGoodsService="DG";
		private bool _GoodsDescriptionRequired = true;

		public CreateUpdateConsignmentConfigurations()
		{

		}

		public bool HCSupportedbyEnterprise
		{
			get{return _HCSupportedbyEnterprise;}
			set{_HCSupportedbyEnterprise=value;}
		}

		public bool CODSupportedbyEnterprise
		{
			get{return _CODSupportedbyEnterprise;}
			set{_CODSupportedbyEnterprise=value;}
		}

		public bool InsSupportedbyEnterprise
		{
			get{return _InsSupportedbyEnterprise;}
			set{_InsSupportedbyEnterprise=value;}
		}

		public bool DangerousGoods
		{
			get{return _DangerousGoods;}
			set{_DangerousGoods=value;}
		}

		public string DangerousGoodsService
		{
			get{return _DangerousGoodsService;}
			set{_DangerousGoodsService=value;}
		}

		public decimal MaxCODAmount
		{
			get{return _MaxCODAmount;}
			set{_MaxCODAmount=value;}
		}

		public decimal MaxDeclaredValue
		{
			get{return _MaxDeclaredValue;}
			set{_MaxDeclaredValue=value;}
		}

		public bool GoodsDescriptionRequired
		{
			get{return _GoodsDescriptionRequired;}
			set{_GoodsDescriptionRequired=value;}
		}
	}

	[Serializable()]
	public class PackageDetailsLimitsConfigurations 
	{
		private decimal _VolumeLimit=0;
		private decimal _PkgWtLimit=0;
		private decimal _PkgQtyLimit=0;
		private decimal _PkgRowWtLimit=0;
		private decimal _DensityFactor=0;

		public PackageDetailsLimitsConfigurations()
		{

		}

		public decimal VolumeLimit
		{
			get{return _VolumeLimit;}
			set{_VolumeLimit=value;}
		}

		public decimal PkgWtLimit
		{
			get{return _PkgWtLimit;}
			set{_PkgWtLimit=value;}
		}

		public decimal PkgQtyLimit
		{
			get{return _PkgQtyLimit;}
			set{_PkgQtyLimit=value;}
		}

		public decimal PkgRowWtLimit
		{
			get{return _PkgRowWtLimit;}
			set{_PkgRowWtLimit=value;}
		}

		public decimal DensityFactor
		{
			get{return _DensityFactor;}
			set{_DensityFactor=value;}
		}
	}

	public class MaintainPhonesIMEIsConfigurations 
	{
		private int _GridRows=0;

		public MaintainPhonesIMEIsConfigurations()
		{

		}

		public int GridRows
		{
			get{return _GridRows;}
			set{_GridRows=value;}
		}


	}
}