using System;
using com.common.DAL;
using com.common.classes;
using System.Data;
using System.Text;
using com.common.util;

namespace com.ties.classes
{
	/// <summary>
	/// Summary description for Route.
	/// </summary>
	public class RouteCode
	{
		public RouteCode()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		 String m_strPathCode = null;
		 String m_strRouteCode = null;
		 String m_strRoutedescp = null;
		 String m_strOriginStation = null;
		 String m_strDestinationStation = null;
		
		public String Path_Code
		{
			set
			{
				m_strPathCode = value;
			}
			get
			{
				return m_strPathCode;
			}
		}
		public String Route_Code
		{
			set
			{
				m_strRouteCode = value;
			}
			get
			{
				return m_strRouteCode;
			}
		}
		public String RouteDescription
		{
			set
			{
				m_strRoutedescp = value;
			}
			get
			{
				return m_strRoutedescp;
			}
		}
		public String OriginStation
		{
			set
			{
				m_strOriginStation = value;
			}
			get
			{
				return m_strOriginStation;
			}
		}
		public String DestinationStation
		{
			set
			{
				m_strDestinationStation = value;
			}
			get
			{
				return m_strDestinationStation;
			}
		}

		public void Populate(String appID, String enterpriseID, String RouteCode)
		{
			DbConnection dbCon= null;
			DataSet dsRouteCode =null;
			IDbCommand dbcmd=null;

			dbCon = DbConnectionManager.GetInstance().GetDbConnection(appID,enterpriseID);

			if (dbCon==null)
			{
				Logger.LogTraceError("VAS.cs","Populate","ERR001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			StringBuilder strQry =  new StringBuilder(); 

			strQry = strQry.Append("select * from route_code ");
			strQry.Append(" where applicationid = '");
			strQry.Append(appID);
			strQry.Append ("' and enterpriseid = '");
			strQry.Append (enterpriseID);
			strQry.Append ("' and route_code ='");
			strQry.Append (Utility.ReplaceSingleQuote(RouteCode));
			strQry.Append ("'");

			dbcmd = dbCon.CreateCommand(strQry.ToString(),CommandType.Text);
 
			try
			{
				dsRouteCode = (DataSet)dbCon.ExecuteQuery(dbcmd,com.common.DAL.ReturnType.DataSetType);  
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("Routecode.cs","Populate","ERR002","dbcon is null");
				throw appException;
			}

			if (dsRouteCode.Tables[0].Rows.Count  > 0) 
			{
				DataRow drEach = dsRouteCode.Tables[0].Rows[0];

				if(!drEach["path_code"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					m_strPathCode = (string)drEach["path_code"];
				}
				if(!drEach["route_code"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					m_strRouteCode = (string)drEach["route_code"];
				}
				if(!drEach["route_description"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					m_strRoutedescp = (string)drEach["route_description"];
				}
			
				
			}
		}
	}
}
