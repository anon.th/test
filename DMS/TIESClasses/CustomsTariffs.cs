using System;

namespace TIESClasses
{
	[Serializable()]
	public class CustomsTariffs
	{
		private string action = string.Empty;
		private string enterpriseId = string.Empty;
		private string userLoggedin  = string.Empty;
		private string tariffCode = string.Empty;
		private string description = string.Empty;
		private string dutyRate = string.Empty;
		private string dutyCalc = string.Empty;
		private string exciseRate = string.Empty;
		private string exciseCalc = string.Empty;
		private string permitRequired = string.Empty;

		public CustomsTariffs() { }

		public CustomsTariffs(string _action,
			string _enterpriseId,
			string _userLoggedin,
			string _tariffCode,
			string _description,
			string _dutyRate,
			string _dutyCalc,
			string _exciseRate,
			string _exciseCalc,
			string _permitRequired) 
		{
			action = _action;
			enterpriseId = _enterpriseId;
			userLoggedin  = _userLoggedin;
			tariffCode = _tariffCode;
			description = _description;
			dutyRate = _dutyRate;
			dutyCalc = _dutyCalc;
			exciseRate = _exciseRate;
			exciseCalc = _exciseCalc;
			permitRequired = _permitRequired;		
		}

		public string Action { set { action = value; } get { return action; } }
		public string EnterpriseId { set { enterpriseId = value; } get { return enterpriseId; } }
		public string UserLoggedin { set { userLoggedin = value; } get { return userLoggedin; } }
		public string TariffCode { set { tariffCode = value; } get { return tariffCode; } }
		public string Description { set { description = value; } get { return description; } }
		public string DutyRate { set { dutyRate = value; } get { return dutyRate; } }
		public string DutyCalc { set { dutyCalc = value; } get { return dutyCalc; } }
		public string ExciseRate { set { exciseRate = value; } get { return exciseRate; } }
		public string ExciseCalc { set { exciseCalc = value; } get { return exciseCalc; } }
		public string PermitRequired { set { permitRequired = value; } get { return permitRequired; } }
	}
}
