using System;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

namespace TIESClasses
{
	[Serializable()]
	public class CustomerProcessingAccess
	{
		private string action = string.Empty;
		private string enterpriseId = string.Empty;
		private string userid = string.Empty;
		private string custid = string.Empty;

		public CustomerProcessingAccess() { }
		public CustomerProcessingAccess(
			string _action, 
			string _enterpriseId,
			string _userid,
			string _custid) 
		{
			action = _action;
			enterpriseId = _enterpriseId;
			userid = _userid;
			custid = _custid;
		}

		public string Action { set { action = value; } get { return action; } }
		public string EnterpriseId { set { enterpriseId = value; } get { return enterpriseId; } }
		public string UserId { set { userid = value; } get { return userid; } }
		public string CustId { set { custid = value; } get { return custid; } }
	}
}
