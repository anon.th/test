using System;

namespace TIESClasses
{
	[Serializable()]
	public class ReportCheckRequisitions
	{
		private string enterpriseid = string.Empty;
		private string startDate = string.Empty;
		private string endDate = string.Empty;
		private string dateType = string.Empty;
		private string chequeNo = string.Empty;
		private string chequeNoTo = string.Empty;
		private string requisitionNo = string.Empty;
		private string requisitionNoTo = string.Empty;
		private string chequeReqType = string.Empty;
		private string payeeName = string.Empty;
		private string receiptNumber = string.Empty;
		private string showCheqReqDetail = string.Empty;
		private string searchDetailsFor = string.Empty;
		private string listOfItems = string.Empty; 

		public ReportCheckRequisitions() {}

		public ReportCheckRequisitions(string _enterpriseid,
			string _startDate,
			string _endDate,
			string _dateType,
			string _chequeNo,
			string _chequeNoTo,
			string _requisitionNo,
			string _requisitionNoTo,
			string _chequeReqType,
			string _payeeName,
			string _receiptNumber,
			string _showCheqReqDetail,
			string _searchDetailsFor,
			string _listOfItems) 
		{
			enterpriseid = _enterpriseid;
			startDate = _startDate;
			endDate = _endDate;
			dateType = _dateType;
			chequeNo = _chequeNo;
			chequeNoTo = _chequeNoTo;
			requisitionNo = _requisitionNo;
			requisitionNoTo = _requisitionNoTo;
			chequeReqType = _chequeReqType;
			payeeName = _payeeName;
			receiptNumber = _receiptNumber;
			showCheqReqDetail = _showCheqReqDetail;
			searchDetailsFor=_searchDetailsFor;
			listOfItems=_listOfItems;
		}

		public string Enterpriseid { set { enterpriseid = value; } get { return enterpriseid; } }
		public string StartDate { set { startDate = value; } get { return startDate; } }
		public string EndDate { set { endDate = value; } get { return endDate; } }
		public string DateType { set { dateType = value; } get { return dateType; } }
		public string ChequeNo { set { chequeNo = value; } get { return chequeNo; } }
		public string ChequeNoTo { set { chequeNoTo = value; } get { return chequeNoTo; } }
		public string RequisitionNo { set { requisitionNo = value; } get { return requisitionNo; } }
		public string RequisitionNoTo { set { requisitionNoTo = value; } get { return requisitionNoTo; } }
		public string ChequeReqType { set { chequeReqType = value; } get { return chequeReqType; } }
		public string PayeeName { set { payeeName = value; } get { return payeeName; } }
		public string ReceiptNumber { set { receiptNumber = value; } get { return receiptNumber; } }
		public string ShowCheqReqDetail { set { showCheqReqDetail = value; } get { return showCheqReqDetail; } }
		public string SearchDetailsFor { set { searchDetailsFor = value; } get { return searchDetailsFor; } }
		public string ListOfItems { set { listOfItems = value; } get { return listOfItems; } }

	}
}
