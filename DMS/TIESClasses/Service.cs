using System;
using System.Text;
using com.common.DAL;
using com.common.classes;
using System.Data;
using com.common.util;

namespace com.ties.classes
{
	/// <summary>
	/// Summary description for Service.
	/// </summary>
	public class Service
	{
		public Service()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		//
		// TODO: Write all Member Variables.
		//
		DateTime m_dtCommitTime;
		decimal		 m_iTransitDay = 0;
		int		 m_iTransitHour = 0;
		String   m_strServiceDesc = null;
		String m_strServiceCode = null;
		//
		// TODO: Write all Class Properties here.
		//
		public int TransitHour
		{
			set
			{
				m_iTransitHour = value;
			}
			get
			{
				return m_iTransitHour;
			}
		}

		public DateTime CommitTime
		{
			set
			{
				m_dtCommitTime = value;
			}
			get
			{
				return m_dtCommitTime;
			}
		}

		public decimal TransitDay
		{
			set
			{
				m_iTransitDay = value;
			}
			get
			{
				return m_iTransitDay;
			}
		}

		public String ServiceDescription
		{
			set
			{
				m_strServiceDesc = value;
			}
			get
			{
				return m_strServiceDesc;
			}
		}

		public String ServiceCode
		{
			set
			{
				m_strServiceCode = value;
			}
			get
			{
				return m_strServiceCode;
			}
		}
		public static bool IsAvailableFromDC(string a_strAppID, string a_strEnterpriseID, string a_strServiceCode, string strOriginDC, string a_str_RecipZipCode)
		{
			DbConnection dbCon = null;
			DataSet dsZipService = null;
			IDbCommand dbCmd = null;
			bool isAvail = true;
			decimal transit_time = 0;
			String commit_time = null;
			Service dcService = new Service();
			dbCon = DbConnectionManager.GetInstance().GetDbConnection(a_strAppID,a_strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("TIESUtility.cs","IsZipCodeServiceAvailable","ERR001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			StringBuilder strQry = new StringBuilder();

			Service service = new Service();
			service.Populate(a_strAppID,a_strEnterpriseID,a_strServiceCode);

			strQry.Append(" Select Service.service_code, ISNULL(Service.transit_day * 24, 0) + ISNULL(Service.transit_hour, 0) AS transit_time, convert(varchar(5),Service.commit_time,108) as commit_time");
			strQry.Append(" From Service, Best_Service_Available");
			strQry.Append(" Where Service.applicationid='"+a_strAppID+"' AND Service.enterpriseid='"+a_strEnterpriseID+"' ");
			strQry.Append(" AND Best_Service_Available.origin_dc = '"+Utility.ReplaceSingleQuote(strOriginDC)+"'");
			strQry.Append(" AND Best_Service_Available.recipientZipCode ='"+Utility.ReplaceSingleQuote(a_str_RecipZipCode)+"'");
			strQry.Append(" AND Best_Service_Available.applicationid = Service.applicationid");
			strQry.Append(" AND Best_Service_Available.enterpriseid = Service.enterpriseid");
			strQry.Append(" AND Best_Service_Available.service_code = Service.service_code");
		
			dbCmd = dbCon.CreateCommand(strQry.ToString(),CommandType.Text);

			try
			{
				dsZipService =(DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
			}
			catch(ApplicationException appExpection)
			{
				Logger.LogTraceError("TIESUtility.cs","IsZipCodeServiceAvailable","ERR002","Error in  Execute Query "+appExpection.Message);
				throw appExpection;
			}
			//
			//			if(dsZipService.Tables[0].Rows.Count <=0)
			//			{
			//				isAvail = false;
			//			}
			if(dsZipService.Tables[0].Rows.Count >0)
			{
				DataRow drEach = dsZipService.Tables[0].Rows[0];
				if(!drEach["transit_time"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					transit_time = Convert.ToDecimal(drEach["transit_time"]);	
				}
				if(!drEach["commit_time"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					commit_time = Convert.ToString(drEach["commit_time"]);
				}
			}
			else
			{
				return false;
			}
			
			if(((service.m_iTransitDay*24)+service.m_iTransitHour) < transit_time)
			{
				isAvail = false;
			}
			else
			{
				if(service.m_dtCommitTime.TimeOfDay < TimeSpan.Parse(commit_time))
					isAvail = false;
			}
			
			return isAvail;
		}
		public static bool IsAvailable(string a_strAppID, string a_strEnterpriseID, string a_strServiceCode, string a_str_SendZipCode, string a_str_RecipZipCode)
		{
			DbConnection dbCon = null;
			DataSet dsZipService = null;
			IDbCommand dbCmd = null;
			bool isAvail = true;
			decimal transit_time = 0;
			String commit_time = null;
			Service dcService = new Service();
			DataSet tmpDC = dcService.GetDCToOrigin(a_strAppID, a_strEnterpriseID,
				0, 0, a_str_SendZipCode.Trim()).ds;

			String strOriginDC = tmpDC.Tables[0].Rows[0]["origin_station"].ToString();

			tmpDC.Dispose();
			tmpDC = null;

			dbCon = DbConnectionManager.GetInstance().GetDbConnection(a_strAppID,a_strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("TIESUtility.cs","IsZipCodeServiceAvailable","ERR001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			
			StringBuilder strQry = new StringBuilder();
//			strQry.Append(" Select distinct Service.service_code, ISNULL(Service.transit_day * 24, 0) + ISNULL(Service.transit_hour, 0) AS transit_time, ");
//			strQry.Append(" Service.service_description, Service.service_charge_percent, Service.service_charge_amt ");
//			strQry.Append(" FROM   Zipcode INNER JOIN   Delivery_Path ON Zipcode.applicationid = Delivery_Path.applicationid "); 
//			strQry.Append(" AND Zipcode.enterpriseid = Delivery_Path.enterpriseid AND Zipcode.pickup_route = Delivery_Path.path_code ");
//			strQry.Append(" INNER JOIN  Service ON Delivery_Path.applicationid = Service.applicationid AND Delivery_Path.enterpriseid = Service.enterpriseid ");
//			strQry.Append(" WHERE (Zipcode.zipcode = '"+Utility.ReplaceSingleQuote(a_str_SendZipCode)+"') and Service.Service_code not in (select distinct service_code ");
//			strQry.Append(" from Zipcode_Service_Excluded where Zipcode_Service_Excluded.zipcode ='"+Utility.ReplaceSingleQuote(a_str_RecipZipCode)+"') ");
//			strQry.Append(" AND Zipcode.applicationid='"+a_strAppID+"' AND Zipcode.enterpriseid='"+a_strEnterpriseID+"' ");
//			strQry.Append(" AND Service.service_code ='"+Utility.ReplaceSingleQuote(a_strServiceCode)+"' ");
//			strQry.Append(" ORDER BY transit_time");
			Service service = new Service();
			service.Populate(a_strAppID,a_strEnterpriseID,a_strServiceCode);

			strQry.Append(" Select Service.service_code, ISNULL(Service.transit_day * 24, 0) + ISNULL(Service.transit_hour, 0) AS transit_time, convert(varchar(5),Service.commit_time,108) as commit_time");
			strQry.Append(" From Service, Best_Service_Available");
			strQry.Append(" Where Service.applicationid='"+a_strAppID+"' AND Service.enterpriseid='"+a_strEnterpriseID+"' ");
			strQry.Append(" AND Best_Service_Available.origin_dc = '"+Utility.ReplaceSingleQuote(strOriginDC)+"'");
			strQry.Append(" AND Best_Service_Available.recipientZipCode ='"+Utility.ReplaceSingleQuote(a_str_RecipZipCode)+"'");
			strQry.Append(" AND Best_Service_Available.applicationid = Service.applicationid");
			strQry.Append(" AND Best_Service_Available.enterpriseid = Service.enterpriseid");
			strQry.Append(" AND Best_Service_Available.service_code = Service.service_code");
		
			dbCmd = dbCon.CreateCommand(strQry.ToString(),CommandType.Text);

			try
			{
				dsZipService =(DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
			}
			catch(ApplicationException appExpection)
			{
				Logger.LogTraceError("TIESUtility.cs","IsZipCodeServiceAvailable","ERR002","Error in  Execute Query "+appExpection.Message);
				throw appExpection;
			}
//
//			if(dsZipService.Tables[0].Rows.Count <=0)
//			{
//				isAvail = false;
//			}
			if(dsZipService.Tables[0].Rows.Count >0)
			{
				DataRow drEach = dsZipService.Tables[0].Rows[0];
				if(!drEach["transit_time"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					transit_time = Convert.ToDecimal(drEach["transit_time"]);	
				}
				if(!drEach["commit_time"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					commit_time = Convert.ToString(drEach["commit_time"]);
				}
			}
			else
			{
				return false;
			}
			
			if(((service.m_iTransitDay*24)+service.m_iTransitHour) < transit_time)
			{
				isAvail = false;
			}
			else
			{
				if(service.m_dtCommitTime.TimeOfDay < TimeSpan.Parse(commit_time))
					isAvail = false;
			}
			
			return isAvail;
		}
		public void Populate(String appID, String enterpriseID,String serviceCode)
		{
			DbConnection dbCon = null;
			DataSet dsService = null;
			IDbCommand dbCmd = null;

			dbCon = DbConnectionManager.GetInstance().GetDbConnection(appID,enterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("Service.cs","Populate","ERR001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			
			StringBuilder strQry = new StringBuilder();
			strQry = strQry.Append("select * from service  ");
			strQry.Append(" where applicationid = '");
			strQry.Append(appID);
			strQry.Append("' and enterpriseid = '");
			strQry.Append(enterpriseID);
			strQry.Append("' and service_code = '");
			strQry.Append(Utility.ReplaceSingleQuote(serviceCode));
			strQry.Append("'");

			dbCmd = dbCon.CreateCommand(strQry.ToString(),CommandType.Text);

			try
			{
				dsService =(DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
			}
			catch(ApplicationException appExpection)
			{
				Logger.LogTraceError("Service.cs","Populate","ERR002","Error in  Execute Query ");
				throw appExpection;
			}

			if(dsService.Tables[0].Rows.Count >0)
			{
				DataRow drEach = dsService.Tables[0].Rows[0];
				
				if(!drEach["commit_time"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					m_dtCommitTime = (DateTime)drEach["commit_time"];					
				}
				if(!drEach["transit_day"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					m_iTransitDay = Convert.ToDecimal(drEach["transit_day"]);	
				}
				if(!drEach["transit_hour"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					m_iTransitHour = Convert.ToInt32(drEach["transit_hour"]);	
				}
				if(!drEach["service_description"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					m_strServiceDesc = (String)drEach["service_description"];
				}
				if(!drEach["service_code"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					m_strServiceCode = (String)drEach["service_code"];
				}
			} 

		}
		private SessionDS GetDCToOrigin(String strAppID, String strEnterpriseID,
			int iCurrent, int iDSRecSize, String strSenderZipcode)
		{
			SessionDS sessionDS = null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("DomesticShipmentMgrDAL","getDCToOrigStation","EDB101","DbConnection object is null!!");
				return sessionDS;
			}

			StringBuilder strQry = new StringBuilder();
			strQry.Append("SELECT distinct b.origin_station ");
			strQry.Append("FROM zipcode a ");
			strQry.Append("INNER JOIN Delivery_Path b ");
			strQry.Append("ON (a.applicationid = b.applicationid ");
			strQry.Append("AND a.enterpriseid = b.enterpriseid ");
			strQry.Append("AND a.pickup_route = b.path_code) ");
			strQry.Append("WHERE a.applicationid = '" + strAppID + "' ");
			strQry.Append("AND a.enterpriseid = '" + strEnterpriseID + "' ");
			strQry.Append("AND a.zipcode = '" + strSenderZipcode + "'");
					
			sessionDS = dbCon.ExecuteQuery(strQry.ToString(),iCurrent, iDSRecSize,"DCOriginStation");
			return  sessionDS;	
		}
		public static DataSet ReadAll(String strAppID, String strEnterpriseID)
		{
			DataSet dsService=null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			IDbCommand dbCmd = null;
			if(dbCon == null)
			{
//				Logger.LogTraceError("DomesticShipmentMgrDAL","getDCToOrigStation","EDB101","DbConnection object is null!!");
				return dsService;
			}
			StringBuilder strQry = new StringBuilder();
			strQry = strQry.Append("select distinct * from service  ");
			strQry.Append(" where applicationid = '");
			strQry.Append(strAppID);
			strQry.Append("' and enterpriseid = '");
			strQry.Append(strEnterpriseID);
			strQry.Append("'");

			dbCmd = dbCon.CreateCommand(strQry.ToString(),CommandType.Text);
			dsService =(DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
			return  dsService;	
			

		}

		public static DataSet ReadBestService(String strAppID, String strEnterpriseID,String strOriginDC, String strDestZipCode)
		{
			DataSet dsService=null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			IDbCommand dbCmd = null;
			if(dbCon == null)
			{
				return dsService;
			}
			StringBuilder strQry = new StringBuilder();
			strQry.Append(" Select 0 as BSA,applicationid,enterpriseid,service_code, ISNULL(Service.transit_day * 24, 0) + ISNULL(Service.transit_hour, 0) AS transit_time");
			strQry.Append(",service_description,service_charge_percent,service_charge_amt, convert(varchar(5),Service.commit_time,108) as commited_time from Service");
			strQry.Append(" Where service_code in (select service_code from Best_Service_Available");
			strQry.Append(" where origin_dc = '"+Utility.ReplaceSingleQuote(strOriginDC)+"'");
			strQry.Append(" and recipientZipCode = '"+Utility.ReplaceSingleQuote(strDestZipCode)+"')");
			strQry.Append(" Union");
			strQry.Append(" select 1 as BSA,* from (Select applicationid,enterpriseid,service_code, ISNULL(Service.transit_day * 24, 0) + ISNULL(Service.transit_hour, 0) AS transit_time");
			strQry.Append(",service_description,service_charge_percent,service_charge_amt, convert(varchar(5),Service.commit_time,108) as commited_time from Service) SR");
			strQry.Append(" Where (((SR.commited_time >= (select convert(varchar(5),Service.commit_time,108) as commited_time");
			strQry.Append(" from Service where service_code in (select service_code from Best_Service_Available");
			strQry.Append(" where origin_dc = '"+Utility.ReplaceSingleQuote(strOriginDC)+"'");
			strQry.Append(" and recipientZipCode = '"+Utility.ReplaceSingleQuote(strDestZipCode)+"')))");
			strQry.Append(" AND (SR.transit_time = (select ISNULL(Service.transit_day * 24, 0) + ISNULL(Service.transit_hour, 0) AS transit_time");
			strQry.Append(" from Service where service_code in (select service_code from Best_Service_Available");
			strQry.Append(" where origin_dc = '"+Utility.ReplaceSingleQuote(strOriginDC)+"'");
			strQry.Append(" and recipientZipCode = '"+Utility.ReplaceSingleQuote(strDestZipCode)+"'))))");
			strQry.Append(" or (SR.transit_time > (select ISNULL(Service.transit_day * 24, 0) + ISNULL(Service.transit_hour, 0) AS transit_time");
			strQry.Append(" from Service where service_code in (select service_code from Best_Service_Available");
			strQry.Append(" where origin_dc = '"+Utility.ReplaceSingleQuote(strOriginDC)+"'");
			strQry.Append(" and recipientZipCode = '"+Utility.ReplaceSingleQuote(strDestZipCode)+"')))) ");
			strQry.Append(" and SR.service_code not in (select service_code from Best_Service_Available");
			strQry.Append(" where origin_dc = '"+Utility.ReplaceSingleQuote(strOriginDC)+"'");
			strQry.Append(" and recipientZipCode = '"+Utility.ReplaceSingleQuote(strDestZipCode)+"')");
			strQry.Append(" AND SR.applicationid = '"+strAppID+"' and SR.enterpriseid = '"+strEnterpriseID+"'");
			//edit by Tumz.
			strQry.Append(" ORDER BY transit_time");

			dbCmd = dbCon.CreateCommand(strQry.ToString(),CommandType.Text);
			dsService =(DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
			return  dsService;	
			
		}

	}
}
