using System;

namespace TIESClasses
{
	[Serializable()]
	public class ExportInvoicesCheques
	{
		private string enterpriseid = string.Empty;
		private string userloggedin = string.Empty;
		private string documentType = string.Empty;
		private string startNo = string.Empty;
		private string endNo = string.Empty;
		private string showOnlyNotExported = string.Empty;
		private string listOfDocuments = string.Empty;
		private string invoicesList = string.Empty;

		public ExportInvoicesCheques(string _enterpriseid,
			string _userloggedin,
			string _documentType,
			string _startNo,
			string _endNo,
			string _showOnlyNotExported,
			string _InvoicesList)
		{
			enterpriseid = _enterpriseid;
			userloggedin = _userloggedin;
			documentType = _documentType;
			startNo = _startNo;
			endNo = _endNo;
			showOnlyNotExported = _showOnlyNotExported;
			invoicesList=_InvoicesList;
		}

		public ExportInvoicesCheques(string _enterpriseid,
			string _userloggedin,
			string _documentType,
			string _listOfDocuments)
		{
			enterpriseid = _enterpriseid;
			userloggedin = _userloggedin;
			documentType = _documentType;
			listOfDocuments = _listOfDocuments;
		}

		public string Enterpriseid { set { enterpriseid = value; } get { return enterpriseid; } }
		public string Userloggedin { set { userloggedin = value; } get { return userloggedin; } }
		public string DocumentType { set { documentType = value; } get { return documentType; } }
		public string StartNo { set { startNo = value; } get { return startNo; } }
		public string EndNo { set { endNo = value; } get { return endNo; } }
		public string ShowOnlyNotExported { set { showOnlyNotExported = value; } get { return showOnlyNotExported; } }		
		public string ListOfDocuments { set { listOfDocuments = value; } get { return listOfDocuments; } }
		public string InvoicesList { set { invoicesList = value; } get { return invoicesList; } }
	}
}
