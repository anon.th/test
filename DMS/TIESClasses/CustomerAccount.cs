using System;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

namespace TIESClasses
{
	/// <summary>
	/// Summary description for CustomerAccount.
	/// </summary>
	/// 
	[Serializable()] 
	public class CustomerAccount
	{
		private bool _IsEnterpriseUser=false;
		private bool _IsCustomerUser=false;
		private bool _IsMasterCustomerUser=false;
		private bool _IsCustomsUser=false;
		private string _Payerid="";
		private string _DefaultSender="";
		private string _Cust_name="";
		private string _Currency="";
		private decimal _Currency_decimal=2;

		public CustomerAccount()
		{

		}

		public bool IsEnterpriseUser
		{
			set
			{
				_IsEnterpriseUser = value;
			}
			get
			{
				return _IsEnterpriseUser;
			}
		}

		public bool IsCustomerUser
		{
			set
			{
				_IsCustomerUser = value;
			}
			get
			{
				return _IsCustomerUser;
			}
		}

		public bool IsCustomsUser
		{
			set
			{
				_IsCustomsUser = value;
			}
			get
			{
				return _IsCustomsUser;
			}
		}

		public bool IsMasterCustomerUser
		{
			set
			{
				_IsMasterCustomerUser = value;
			}
			get
			{
				return _IsMasterCustomerUser;
			}
		}

		public string Payerid
		{
			set
			{
				_Payerid = value;
			}
			get
			{
				return _Payerid;
			}
		}

		public string DefaultSender
		{
			set
			{
				_DefaultSender = value;
			}
			get
			{
				return _DefaultSender;
			}
		}

		public string Cust_name
		{
			set
			{
				_Cust_name = value;
			}
			get
			{
				return _Cust_name;
			}
		}

		public string Currency
		{
			set
			{
				_Currency = value;
			}
			get
			{
				return _Currency;
			}
		}

		public decimal Currency_decimal
		{
			set
			{
				_Currency_decimal = value;
			}
			get
			{
				return _Currency_decimal;
			}
		}
	}
}
