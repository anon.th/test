using System;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

namespace TIESClasses
{
	[Serializable()]
	public class CurrencyExchangeRates
	{
		private int action = 0;
		private string enterpriseId = string.Empty;
		private string userLoggedin  = string.Empty;
		private string exchangeRateType  = string.Empty;
		private string currencyCode  = string.Empty;
		private string rateDate;
		private string rate;

		public CurrencyExchangeRates(int _action,
									 string _enterpriseId,
									 string _userLoggedin,
									 string _exchangeRateType,
									 string _currencyCode)
		{ 
			action = _action;
			enterpriseId = _enterpriseId;
			userLoggedin = _userLoggedin;
			exchangeRateType = _exchangeRateType;
			currencyCode = _currencyCode;
		}

		public CurrencyExchangeRates(int _action,
								     string _enterpriseId,
									 string _userLoggedin,
									 string _exchangeRateType,
								     string _currencyCode,
									 string _rateDate)
		{ 
			action = _action;
			enterpriseId = _enterpriseId;
			userLoggedin = _userLoggedin;
			exchangeRateType = _exchangeRateType;
			currencyCode = _currencyCode;
			rateDate = _rateDate;
		}

		public CurrencyExchangeRates(int _action,
									 string _enterpriseId,
									 string _userLoggedin,
									 string _exchangeRateType,
									 string _currencyCode,
									 string _rateDate,
									 string _rate)
		{ 
			action = _action;
			enterpriseId = _enterpriseId;
			userLoggedin = _userLoggedin;
			exchangeRateType = _exchangeRateType;
			currencyCode = _currencyCode;
			rateDate = _rateDate;
			rate = _rate;
		}

		public int Action { set { action = value; } get { return action; } }
		public string EnterpriseId { set { enterpriseId = value; } get { return enterpriseId; } }
		public string UserLoggedin { set { userLoggedin = value; } get { return userLoggedin; } }
		public string ExchangeRateType { set { exchangeRateType = value; } get { return exchangeRateType; } }
		public string CurrencyCode { set { currencyCode = value; } get { return currencyCode; } }
		public string RateDate { set { rateDate = value; } get { return rateDate; } }
		public string Rate { set { rate = value; } get { return rate; } }
	}
}
