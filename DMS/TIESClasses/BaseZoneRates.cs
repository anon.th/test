using System;
using System.Text;
using com.common.DAL;
using System.Data;
using com.common.classes;

namespace com.ties.classes
{
	/// <summary>
	/// Summary description for BaseZoneRates.
	/// </summary>
	public class BaseZoneRates
	{
		public BaseZoneRates()
		{
			//
			// TODO: Add constructor logic here
			//
		}
		private decimal m_decStartPrice = 0;
		private decimal m_decIncrementPrice = 0;
		private decimal m_decStartWt = 0;
		private bool m_IsToBeRounded;
		
		/// <summary>
		/// This method gets the sets the boolean value for Rounded.
		/// The value is set in the populate method, if the chargeable weight is less than or
		/// equal to the start weight the Rounded is set to false. If the chargeable weight is
		/// greater than equal to the start weight the Rounded is set to true.
		/// </summary>
		public bool Rounded
		{
			set
			{
				m_IsToBeRounded = value;
			}
			get
			{
				return m_IsToBeRounded;
			}

		}

		/// <summary>
		/// This method gets and sets the value of the start price.
		/// </summary>
		public decimal StartPrice
		{
			set
			{
				m_decStartPrice = value;
			}
			get
			{
				return m_decStartPrice;
			}
		}

		/// <summary>
		/// This method gets and sets the value of the Increment price.
		/// </summary>
		public decimal IncrementPrice
		{
			set
			{
				m_decIncrementPrice = value;
			}
			get
			{
				return m_decIncrementPrice;
			}
		}

		/// <summary>
		/// This method gets and sets the value of the start weight.
		/// </summary>
		public decimal StartWeight
		{
			set
			{
				m_decStartWt = value;
			}
			get
			{
				return m_decStartWt;
			}
		}

		/// <summary>
		/// This method decides where the given chargeable weight falls and accordingly sets the value of
		/// start price, increment price, start weight and sets the boolean m_IsToBeRounded to false 
		/// if the chargeable weight is less than or
		/// equal to the start weight. If the chargeable weight is
		/// greater than equal to the start weight the m_IsToBeRounded is set to true.
		/// </summary>
		/// <param name="appID">Application ID</param>
		/// <param name="enterpriseID">Enterprise ID</param>
		/// <param name="strOrgZone">Origin Zone</param>
		/// <param name="strDestnZone">Destination Zone</param>
		/// <param name="fChargeableWt">Chargeable weight</param>
		public void Populate(String appID, String enterpriseID,String strOrgZone,String strDestnZone,float fChargeableWt)
		{
			DbConnection dbCon = null;
			DataSet dsBaseZone = null;
			IDbCommand dbCmd = null;
			bool isFound = false;

			dbCon = DbConnectionManager.GetInstance().GetDbConnection(appID,enterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("BaseZoneRates.cs","PopulateValues","ERR001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			
			StringBuilder strQry = new StringBuilder();
			//strQry = strQry.Append("select * from base_zone_rates a ");
			strQry = strQry.Append("select applicationid, enterpriseid, origin_zone_code, destination_zone_code, service_code, start_wt,end_wt,start_price,increment_price,max(effective_date) as effective_date from base_zone_rates a ");
			strQry.Append(" where a.applicationid = '");
			strQry.Append(appID);
			strQry.Append("' and a.enterpriseid = '");
			strQry.Append(enterpriseID);
			strQry.Append("' and a.origin_zone_code = '");
			strQry.Append(strOrgZone);
			strQry.Append("' and a.destination_zone_code = '");
			strQry.Append(strDestnZone+"'");
			strQry.Append(" and a.start_wt <= ");
			strQry.Append(fChargeableWt);
			strQry.Append(" and a.end_wt >= ");
			strQry.Append(fChargeableWt);
			strQry.Append(" and  effective_date < getdate() group by  applicationid, enterpriseid,origin_zone_code, destination_zone_code, service_code, start_wt,end_wt,start_price,increment_price ");


			dbCmd = dbCon.CreateCommand(strQry.ToString(),CommandType.Text);

			try
			{
				dsBaseZone =(DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
			}
			catch(ApplicationException appExpection)
			{
				Logger.LogTraceError("BaseZoneRates.cs","Populate","ERR002","Error in Execute Query ");
				throw appExpection;
			}
			//if the records are zero the chargeable weight is either less than the minimum
			//or greater than the maximum weight
			
			if(dsBaseZone.Tables[0].Rows.Count == 0)
			{
				//If the chargeable weight is less than the minimum rates
				strQry = new StringBuilder();
				strQry.Append("select min(start_wt) as start_wt, max(effective_date) as effective_date from base_zone_rates where applicationid = '");
				strQry.Append(appID);
				strQry.Append("' and enterpriseid = '");
				strQry.Append(enterpriseID);
				strQry.Append("' and origin_zone_code = '");
				strQry.Append(strOrgZone);
				strQry.Append("' and destination_zone_code = '");
				strQry.Append(strDestnZone+"'");
				strQry.Append(" and  effective_date < getdate() group by  applicationid, enterpriseid,origin_zone_code, destination_zone_code, service_code, start_wt,end_wt,start_price,increment_price ");

				//strQry.Append(" and effective_date < getdate() order by effective_date DESC");

				
				dbCmd = dbCon.CreateCommand(strQry.ToString(),CommandType.Text);

				try
				{
					dsBaseZone =(DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
				}
				catch(ApplicationException appExpection)
				{
					Logger.LogTraceError("BaseZoneRates.cs","Populate","ERR003","Error in Execute Query ");
					throw appExpection;
				}

				if(dsBaseZone.Tables[0].Rows.Count >0)
				{
					DataRow drEach = dsBaseZone.Tables[0].Rows[0];

					if((drEach["start_wt"]!= null) && (!drEach["start_wt"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						m_decStartWt = (decimal)drEach["start_wt"];
					}
					else
					{
						m_decStartWt = 0;
					}
					
					if(m_decStartWt >=  (decimal)fChargeableWt)
					{
						strQry = new StringBuilder();
						//strQry.Append("select * from base_zone_rates where applicationid = '");
						strQry.Append("select applicationid, enterpriseid, origin_zone_code, destination_zone_code, service_code, start_wt,end_wt,start_price,increment_price,max(effective_date) as effective_date from base_zone_rates where applicationid = '");
						strQry.Append(appID);
						strQry.Append("' and enterpriseid = '");
						strQry.Append(enterpriseID);
						strQry.Append("'"+" and origin_zone_code = '");
						strQry.Append(strOrgZone);
						strQry.Append("' and destination_zone_code = '");
						strQry.Append(strDestnZone+"'");
						strQry.Append(" and start_wt = ");
						strQry.Append(m_decStartWt);
						strQry.Append(" and  effective_date < getdate() group by  applicationid, enterpriseid,origin_zone_code, destination_zone_code, service_code, start_wt,end_wt,start_price,increment_price ");


						isFound = true;
						m_IsToBeRounded = false;
						dbCmd = dbCon.CreateCommand(strQry.ToString(),CommandType.Text);
					
						try
						{
							dsBaseZone =(DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
						}
						catch(ApplicationException appExpection)
						{
							Logger.LogTraceError("BaseZoneRates.cs","Populate","ERR004","Error in Execute Query ");
							throw appExpection;
						}
					}
				}

				//if the chargeable weight is greater than the max weight
				if(isFound == false)
				{
					strQry = new StringBuilder();
					strQry.Append("select max(start_wt) as start_wt , max(effective_date) as effective_date from base_zone_rates a where a.applicationid = '");
					strQry.Append(appID);
					strQry.Append("' and a.enterpriseid = '");
					strQry.Append(enterpriseID);
					strQry.Append("'"+" and a.origin_zone_code = '");
					strQry.Append(strOrgZone);
					strQry.Append("' and a.destination_zone_code = '");
					strQry.Append(strDestnZone+"'");
					strQry.Append(" and a.start_wt = ");
					strQry.Append(m_decStartWt);
					strQry.Append(" and  effective_date < getdate() group by  applicationid, enterpriseid,origin_zone_code, destination_zone_code, service_code, start_wt,end_wt,start_price,increment_price ");


					dbCmd = dbCon.CreateCommand(strQry.ToString(),CommandType.Text);
					
					try
					{
						dsBaseZone =(DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
					}
					catch(ApplicationException appExpection)
					{
						Logger.LogTraceError("BaseZoneRates.cs","Populate","ERR005","Error in Execute Query ");
						throw appExpection;
					}

					if(dsBaseZone.Tables[0].Rows.Count >0)
					{
						DataRow drEach = dsBaseZone.Tables[0].Rows[0];
						if((drEach["start_wt"]!= null) && (!drEach["start_wt"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						{
							m_decStartWt = (decimal)drEach["start_wt"];
						}
						else
						{
							m_decStartWt = 0;
						}
						
						if(m_decStartWt <=  (decimal)fChargeableWt)
						{
							strQry = new StringBuilder();
							//strQry.Append("select * from base_zone_rates where applicationid = '");
							strQry.Append("select applicationid, enterpriseid, origin_zone_code, destination_zone_code, service_code, start_wt,end_wt,start_price,increment_price,max(effective_date) as effective_date from base_zone_rates where applicationid = '");
							strQry.Append(appID);
							strQry.Append("' and enterpriseid = '");
							strQry.Append(enterpriseID);
							strQry.Append("'"+" and origin_zone_code = '");
							strQry.Append(strOrgZone);
							strQry.Append("' and destination_zone_code = '");
							strQry.Append(strDestnZone+"'");
							strQry.Append(" and start_wt = ");
							strQry.Append(m_decStartWt);
							strQry.Append(" and  effective_date < getdate() group by  applicationid, enterpriseid,origin_zone_code, destination_zone_code, service_code, start_wt,end_wt,start_price,increment_price ");


							isFound = true;
							m_IsToBeRounded = true;
			
							dbCmd = dbCon.CreateCommand(strQry.ToString(),CommandType.Text);
					
							try
							{
								dsBaseZone =(DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
							}
							catch(ApplicationException appExpection)
							{
								Logger.LogTraceError("BaseZoneRates.cs","Populate","ERR006","Error in Execute Query ");
								throw appExpection;
							}
						}
					}

				}

			}
			else
			{
				m_IsToBeRounded = true;
			}
			
			if(dsBaseZone.Tables[0].Rows.Count >0)
			{
				DataRow drEach = dsBaseZone.Tables[0].Rows[0];
				m_decStartPrice = (decimal)drEach["start_price"];
				m_decIncrementPrice = (decimal)drEach["increment_price"];
				m_decStartWt = (decimal)drEach["start_wt"];
			}
		}


		public void PopulateFR(String appID, String enterpriseID, String strOrgZone,String strDestnZone,
								float fChargeableWt, String strOrgZipCode, String strDestZipCode,
								String strCustID, String serviceCode)
		{
			DbConnection dbCon = null;
			DataSet dsBaseZone = null;
			IDbCommand dbCmd = null;
			bool isFound = false;

			DataSet dsTmp = null;
			String NewOrgZone = "";
			String NewDestZone = "";
			bool isFXRate = true;

			dbCon = DbConnectionManager.GetInstance().GetDbConnection(appID,enterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("BaseZoneRates.cs","PopulateValues","ERR001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			
			//*********** identify sender zone by looking into Customer_Zones  ************
			StringBuilder strQry = new StringBuilder();
			strQry = strQry.Append("select * from customer_zones ");
			strQry.Append("where applicationid = '");
			strQry.Append(appID);
			strQry.Append("' and enterpriseid = '");
			strQry.Append(enterpriseID);
			strQry.Append("' and custid = '");
			strQry.Append(strCustID);
			strQry.Append("' and zipcode = '");
			strQry.Append(strOrgZipCode + "'");
			//add by x nov 21 08
			strQry.Append(" and effective_date < getdate() order by effective_date DESC ");


			dbCmd = dbCon.CreateCommand(strQry.ToString(),CommandType.Text);

			try
			{
				dsTmp =(DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
			}
			catch(ApplicationException appExpection)
			{
				Logger.LogTraceError("BaseZoneRates.cs","Populate","ERR002","Error in Execute Query ");
				throw appExpection;
			}

			if(dsTmp.Tables[0].Rows.Count > 0)
			{
				if((dsTmp.Tables[0].Rows[0]["zone_code"] != null) && 
					(!dsTmp.Tables[0].Rows[0]["zone_code"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
					(dsTmp.Tables[0].Rows[0]["zone_code"].ToString() != ""))
				{
					NewOrgZone = dsTmp.Tables[0].Rows[0]["zone_code"].ToString().Trim();
				}
				else
				{
					NewOrgZone = "";
					isFXRate = false;
				}
			}
			else
			{
				NewOrgZone = "";
				isFXRate = false;
			}
			//*********** identify sender zone by looking into Customer_Zones  ************

			if(isFXRate)
			{
				//*********** identify zecp zone by looking into Customer_Zones  ************
				strQry = new StringBuilder();
				strQry = strQry.Append("select * from customer_zones ");
				strQry.Append("where applicationid = '");
				strQry.Append(appID);
				strQry.Append("' and enterpriseid = '");
				strQry.Append(enterpriseID);
				strQry.Append("' and custid = '");
				strQry.Append(strCustID);
				strQry.Append("' and zipcode = '");
				strQry.Append(strDestZipCode + "'");
				//add by x nov 21 08
				strQry.Append(" and effective_date < getdate() order by effective_date DESC ");


				dbCmd = dbCon.CreateCommand(strQry.ToString(),CommandType.Text);

				try
				{
					dsTmp =(DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
				}
				catch(ApplicationException appExpection)
				{
					Logger.LogTraceError("BaseZoneRates.cs","Populate","ERR002","Error in Execute Query ");
					throw appExpection;
				}

				if(dsTmp.Tables[0].Rows.Count > 0)
				{
					if((dsTmp.Tables[0].Rows[0]["zone_code"] != null) && 
						(!dsTmp.Tables[0].Rows[0]["zone_code"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
						(dsTmp.Tables[0].Rows[0]["zone_code"].ToString() != ""))
					{
						NewDestZone = dsTmp.Tables[0].Rows[0]["zone_code"].ToString().Trim();
					}
					else
					{
						NewDestZone = "";
						isFXRate = false;
					}
				}
				else
				{
					NewDestZone = "";
					isFXRate = false;
				}
				//*********** identify zecp zone by looking into Customer_Zones  ************
			}
			
			if(isFXRate)
			{
				//*********** identify rate by looking into Base_Rate  ************
				strQry = new StringBuilder();
				//strQry = strQry.Append("select * from Base_Zone_Rates ");
				strQry = strQry.Append("select applicationid, enterpriseid, origin_zone_code, destination_zone_code, service_code, start_wt,end_wt,start_price,increment_price,max(effective_date) as effective_date from base_zone_rates ");
				strQry.Append("where applicationid = '");
				strQry.Append(appID);
				strQry.Append("' and enterpriseid = '");
				strQry.Append(enterpriseID);
				strQry.Append("' and origin_zone_code = '");
				strQry.Append(NewOrgZone);
				strQry.Append("' and destination_zone_code = '");
				strQry.Append(NewDestZone);
				strQry.Append("' and service_code = '");
				strQry.Append(serviceCode + "'");
				strQry.Append(" and  effective_date < getdate() group by  applicationid, enterpriseid,origin_zone_code, destination_zone_code, service_code, start_wt,end_wt,start_price,increment_price ");



				dbCmd = dbCon.CreateCommand(strQry.ToString(),CommandType.Text);

				try
				{
					dsTmp =(DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
				}
				catch(ApplicationException appExpection)
				{
					Logger.LogTraceError("BaseZoneRates.cs","Populate","ERR002","Error in Execute Query ");
					throw appExpection;
				}

				if(dsTmp.Tables[0].Rows.Count > 0)
				{
					isFXRate = true;
				}
				else
				{
					isFXRate = false;
				}
				//*********** identify rate by looking into Base_Rate  ************
			}

			strQry = new StringBuilder();
			if(isFXRate) 
			{
				//strQry = strQry.Append("select  * from base_zone_rates a ");
				strQry = strQry.Append("select applicationid, enterpriseid, origin_zone_code, destination_zone_code, service_code, start_wt,end_wt,start_price,increment_price,max(effective_date) as effective_date from base_zone_rates a ");
				strQry.Append(" where a.applicationid = '");
				strQry.Append(appID);
				strQry.Append("' and a.enterpriseid = '");
				strQry.Append(enterpriseID);
				strQry.Append("' and a.origin_zone_code = '");
				strQry.Append(NewOrgZone);
				strQry.Append("' and a.destination_zone_code = '");
				strQry.Append(NewDestZone+"'");
				strQry.Append(" and a.start_wt <= ");
				strQry.Append(fChargeableWt);
				strQry.Append(" and a.end_wt >= ");
				strQry.Append(fChargeableWt);
				strQry.Append(" and service_code = '");
				strQry.Append(serviceCode + "'");
				strQry.Append(" and  effective_date < getdate() group by  applicationid, enterpriseid,origin_zone_code, destination_zone_code, service_code, start_wt,end_wt,start_price,increment_price ");
				strQry.Append(" order by effective_date desc");


		
			}
			else
			{
				//strQry = strQry.Append("select * from base_zone_rates a ");
				strQry = strQry.Append("select applicationid, enterpriseid, origin_zone_code, destination_zone_code, service_code, start_wt,end_wt,start_price,increment_price,max(effective_date) as effective_date from base_zone_rates a ");
				strQry.Append(" where a.applicationid = '");
				strQry.Append(appID);
				strQry.Append("' and a.enterpriseid = '");
				strQry.Append(enterpriseID);
				strQry.Append("' and a.origin_zone_code = '");
				strQry.Append(strOrgZone);
				strQry.Append("' and a.destination_zone_code = '");
				strQry.Append(strDestnZone+"'");
				strQry.Append(" and a.start_wt <= ");
				strQry.Append(fChargeableWt);
				strQry.Append(" and a.end_wt >= ");
				strQry.Append(fChargeableWt);
				strQry.Append(" and service_code = '");
				strQry.Append(serviceCode + "'");
				strQry.Append(" and  effective_date < getdate() group by  applicationid, enterpriseid,origin_zone_code, destination_zone_code, service_code, start_wt,end_wt,start_price,increment_price ");
				strQry.Append(" order by effective_date desc");

			}

			dbCmd = dbCon.CreateCommand(strQry.ToString(),CommandType.Text);

			try
			{
				dsBaseZone =(DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
			}
			catch(ApplicationException appExpection)
			{
				Logger.LogTraceError("BaseZoneRates.cs","Populate","ERR002","Error in Execute Query ");
				throw appExpection;
			}
			//if the records are zero the chargeable weight is either less than the minimum
			//or greater than the maximum weight
			
			if(dsBaseZone.Tables[0].Rows.Count == 0)
			{
				//If the chargeable weight is less than the minimum rates
				strQry = new StringBuilder();
				if(isFXRate) 
				{
					strQry.Append("select min(start_wt) as start_wt , max(effective_date) as effective_date from base_zone_rates where applicationid = '");
					strQry.Append(appID);
					strQry.Append("' and enterpriseid = '");
					strQry.Append(enterpriseID);
					strQry.Append("' and origin_zone_code = '");
					strQry.Append(NewOrgZone);
					strQry.Append("' and destination_zone_code = '");
					strQry.Append(NewDestZone+"'");
					strQry.Append(" and service_code = '");
					strQry.Append(serviceCode + "'");
					strQry.Append(" and  effective_date < getdate() group by  applicationid, enterpriseid,origin_zone_code, destination_zone_code, service_code, start_wt,end_wt,start_price,increment_price ");
					strQry.Append(" order by effective_date desc");
				}
				else
				{
					strQry.Append("select min(start_wt) as start_wt , max(effective_date) as effective_date from base_zone_rates where applicationid = '");
					strQry.Append(appID);
					strQry.Append("' and enterpriseid = '");
					strQry.Append(enterpriseID);
					strQry.Append("' and origin_zone_code = '");
					strQry.Append(strOrgZone);
					strQry.Append("' and destination_zone_code = '");
					strQry.Append(strDestnZone+"'");
					strQry.Append(" and service_code = '");
					strQry.Append(serviceCode + "'");
					strQry.Append(" and  effective_date < getdate() group by  applicationid, enterpriseid,origin_zone_code, destination_zone_code, service_code, start_wt,end_wt,start_price,increment_price ");
					strQry.Append(" order by effective_date desc");
				}

				dbCmd = dbCon.CreateCommand(strQry.ToString(),CommandType.Text);

				try
				{
					dsBaseZone =(DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
				}
				catch(ApplicationException appExpection)
				{
					Logger.LogTraceError("BaseZoneRates.cs","Populate","ERR003","Error in Execute Query ");
					throw appExpection;
				}

				if(dsBaseZone.Tables[0].Rows.Count >0)
				{
					DataRow drEach = dsBaseZone.Tables[0].Rows[0];

					if((drEach["start_wt"]!= null) && (!drEach["start_wt"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						m_decStartWt = (decimal)drEach["start_wt"];
					}
					else
					{
						m_decStartWt = 0;
					}
					
					if(m_decStartWt >=  (decimal)fChargeableWt)
					{
						strQry = new StringBuilder();
						if(isFXRate) 
						{
							//strQry.Append("select * from base_zone_rates where applicationid = '");
							strQry.Append("select applicationid, enterpriseid, origin_zone_code, destination_zone_code, service_code, start_wt,end_wt,start_price,increment_price,max(effective_date) as effective_date from base_zone_rates where applicationid = '");
							strQry.Append(appID);
							strQry.Append("' and enterpriseid = '");
							strQry.Append(enterpriseID);
							strQry.Append("'"+" and origin_zone_code = '");
							strQry.Append(NewOrgZone);
							strQry.Append("' and destination_zone_code = '");
							strQry.Append(NewDestZone+"'");
							strQry.Append(" and start_wt = ");
							strQry.Append(m_decStartWt);
							strQry.Append(" and service_code = '");
							strQry.Append(serviceCode + "'");
							strQry.Append(" and  effective_date < getdate() group by  applicationid, enterpriseid,origin_zone_code, destination_zone_code, service_code, start_wt,end_wt,start_price,increment_price ");
							strQry.Append(" order by effective_date desc");

						}
						else
						{
							//strQry.Append("select * from base_zone_rates where applicationid = '");
							strQry.Append("select applicationid, enterpriseid, origin_zone_code, destination_zone_code, service_code, start_wt,end_wt,start_price,increment_price,max(effective_date) as effective_date from base_zone_rates where applicationid = '");
							strQry.Append(appID);
							strQry.Append("' and enterpriseid = '");
							strQry.Append(enterpriseID);
							strQry.Append("'"+" and origin_zone_code = '");
							strQry.Append(strOrgZone);
							strQry.Append("' and destination_zone_code = '");
							strQry.Append(strDestnZone+"'");
							strQry.Append(" and start_wt = ");
							strQry.Append(m_decStartWt);
							strQry.Append(" and service_code = '");
							strQry.Append(serviceCode + "'");
							strQry.Append(" and  effective_date < getdate() group by  applicationid, enterpriseid,origin_zone_code, destination_zone_code, service_code, start_wt,end_wt,start_price,increment_price ");
							strQry.Append(" order by effective_date desc");

						}
						
						isFound = true;
						m_IsToBeRounded = false;
						dbCmd = dbCon.CreateCommand(strQry.ToString(),CommandType.Text);
					
						try
						{
							dsBaseZone =(DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
						}
						catch(ApplicationException appExpection)
						{
							Logger.LogTraceError("BaseZoneRates.cs","Populate","ERR004","Error in Execute Query ");
							throw appExpection;
						}
					}
				}

				//if the chargeable weight is greater than the max weight
				if(isFound == false)
				{
					strQry = new StringBuilder();
					if(isFXRate) 
					{
						strQry.Append("select max(start_wt) as start_wt , max(effective_date) as effective_date from base_zone_rates a where a.applicationid = '");
						strQry.Append(appID);
						strQry.Append("' and a.enterpriseid = '");
						strQry.Append(enterpriseID);
						strQry.Append("'"+" and a.origin_zone_code = '");
						strQry.Append(NewOrgZone);
						strQry.Append("' and a.destination_zone_code = '");
						strQry.Append(NewDestZone+"'");
						strQry.Append(" and a.start_wt = ");
						strQry.Append(m_decStartWt);
						strQry.Append(" and service_code = '");
						strQry.Append(serviceCode + "'");
						strQry.Append(" and  effective_date < getdate() group by  applicationid, enterpriseid,origin_zone_code, destination_zone_code, service_code, start_wt,end_wt,start_price,increment_price ");
						strQry.Append(" order by effective_date desc");
					}
					else
					{
						strQry.Append("select max(start_wt) as start_wt , max(effective_date) as effective_date from base_zone_rates a where a.applicationid = '");
						strQry.Append(appID);
						strQry.Append("' and a.enterpriseid = '");
						strQry.Append(enterpriseID);
						strQry.Append("'"+" and a.origin_zone_code = '");
						strQry.Append(strOrgZone);
						strQry.Append("' and a.destination_zone_code = '");
						strQry.Append(strDestnZone+"'");
						strQry.Append(" and a.start_wt = ");
						strQry.Append(m_decStartWt);
						strQry.Append(" and service_code = '");
						strQry.Append(serviceCode + "'");
						strQry.Append(" and  effective_date < getdate() group by  applicationid, enterpriseid,origin_zone_code, destination_zone_code, service_code, start_wt,end_wt,start_price,increment_price ");
						strQry.Append(" order by effective_date desc");
					}
					
					dbCmd = dbCon.CreateCommand(strQry.ToString(),CommandType.Text);
					
					try
					{
						dsBaseZone =(DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
					}
					catch(ApplicationException appExpection)
					{
						Logger.LogTraceError("BaseZoneRates.cs","Populate","ERR005","Error in Execute Query ");
						throw appExpection;
					}

					if(dsBaseZone.Tables[0].Rows.Count >0)
					{
						DataRow drEach = dsBaseZone.Tables[0].Rows[0];
						if((drEach["start_wt"]!= null) && (!drEach["start_wt"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						{
							m_decStartWt = (decimal)drEach["start_wt"];
						}
						else
						{
							m_decStartWt = 0;
						}
						
						if(m_decStartWt <=  (decimal)fChargeableWt)
						{
							strQry = new StringBuilder();
							if(isFXRate) 
							{
								//strQry.Append("select * from base_zone_rates where applicationid = 
								strQry.Append("select applicationid, enterpriseid, origin_zone_code, destination_zone_code, service_code, start_wt,end_wt,start_price,increment_price,max(effective_date) as effective_date from base_zone_rates where applicationid = '");
								strQry.Append(appID);
								strQry.Append("' and enterpriseid = '");
								strQry.Append(enterpriseID);
								strQry.Append("'"+" and origin_zone_code = '");
								strQry.Append(NewOrgZone);
								strQry.Append("' and destination_zone_code = '");
								strQry.Append(NewDestZone+"'");
								strQry.Append(" and start_wt = ");
								strQry.Append(m_decStartWt);
								strQry.Append(" and service_code = '");
								strQry.Append(serviceCode + "'");
								strQry.Append(" and  effective_date < getdate() group by  applicationid, enterpriseid,origin_zone_code, destination_zone_code, service_code, start_wt,end_wt,start_price,increment_price ");
								strQry.Append(" order by effective_date desc");
							}
							else
							{
								//strQry.Append("select  * from base_zone_rates where applicationid = '");
								strQry.Append("select applicationid, enterpriseid, origin_zone_code, destination_zone_code, service_code, start_wt,end_wt,start_price,increment_price,max(effective_date) as effective_date from base_zone_rates where applicationid = '");
								strQry.Append(appID);
								strQry.Append("' and enterpriseid = '");
								strQry.Append(enterpriseID);
								strQry.Append("'"+" and origin_zone_code = '");
								strQry.Append(strOrgZone);
								strQry.Append("' and destination_zone_code = '");
								strQry.Append(strDestnZone+"'");
								strQry.Append(" and start_wt = ");
								strQry.Append(m_decStartWt);
								strQry.Append(" and service_code = '");
								strQry.Append(serviceCode + "'");
								strQry.Append(" and  effective_date < getdate() group by  applicationid, enterpriseid,origin_zone_code, destination_zone_code, service_code, start_wt,end_wt,start_price,increment_price ");
								strQry.Append(" order by effective_date desc");
							}
							
							isFound = true;
							m_IsToBeRounded = true;
			
							dbCmd = dbCon.CreateCommand(strQry.ToString(),CommandType.Text);
					
							try
							{
								dsBaseZone =(DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
							}
							catch(ApplicationException appExpection)
							{
								Logger.LogTraceError("BaseZoneRates.cs","Populate","ERR006","Error in Execute Query ");
								throw appExpection;
							}
						}
					}

				}

			}
			else
			{
				m_IsToBeRounded = true;
			}
			
			if(dsBaseZone.Tables[0].Rows.Count >0)
			{
				DataRow drEach = dsBaseZone.Tables[0].Rows[0];
				m_decStartPrice = (decimal)drEach["start_price"];
				m_decIncrementPrice = (decimal)drEach["increment_price"];
				m_decStartWt = (decimal)drEach["start_wt"];
			}
		}
	}
}
