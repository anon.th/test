using System;
using com.common.DAL;
using com.common.classes;
using System.Data;
using System.Text;
using com.common.util;

namespace com.ties.classes
{
	/// <summary>
	/// This Class populates the values from the VAS base table.
	/// </summary>
	public class VAS
	{
		public VAS()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		//
		// TODO: Write all Member Variables.
		//
		private String m_strVasCode = null;
		private decimal m_decSurcharge = 0;
		private String m_strVASDescription = null;
		//
		// TODO: Write all Class Properties here.
		//

		/// <summary>
		/// This method gets and sets the value of the VAS Code.
		/// </summary>
		public String VASCode
		{
			set
			{
				m_strVasCode = value;
			}
			get
			{
				return m_strVasCode;
			}
		}

		/// <summary>
		/// This method gets and sets the value of the Surcharge.
		/// </summary>
		public decimal Surcharge
		{
			set
			{
				m_decSurcharge = value;
			}
			get
			{
				return m_decSurcharge;
			}
		}

		/// <summary>
		/// This method gets and sets the value of the VAS Description.
		/// </summary>
		public String VasDescription
		{
			set
			{
				m_strVASDescription = value;
			}
			get
			{
				return m_strVASDescription;
			}
		}

		/// <summary>
		/// This method populates value from the VAS base table.
		/// </summary>
		/// <param name="appID">Application ID</param>
		/// <param name="enterpriseID">Enterprise ID</param>
		/// <param name="vasCode">VAS Code</param>
		public void Populate(String appID, String enterpriseID, String vasCode)
		{
			DbConnection dbCon= null;
			DataSet dsVAS =null;
			IDbCommand dbcmd=null;

			dbCon = DbConnectionManager.GetInstance().GetDbConnection(appID,enterpriseID);

			if (dbCon==null)
			{
				Logger.LogTraceError("VAS.cs","Populate","ERR001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			StringBuilder strQry =  new StringBuilder(); 

			strQry = strQry.Append("select * from vas ");
			strQry.Append(" where applicationid = '");
			strQry.Append(appID);
			strQry.Append ("' and enterpriseid = '");
			strQry.Append (enterpriseID);
			strQry.Append ("' and vas_code ='");
			strQry.Append (Utility.ReplaceSingleQuote(vasCode));
			strQry.Append ("'");

			dbcmd = dbCon.CreateCommand(strQry.ToString(),CommandType.Text);
 
			try
			{
				dsVAS = (DataSet)dbCon.ExecuteQuery(dbcmd,com.common.DAL.ReturnType.DataSetType);  
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("VAS.cs","Populate","ERR002","dbcon is null");
				throw appException;
			}

			if (dsVAS.Tables[0].Rows.Count  > 0) 
			{
				DataRow drEach = dsVAS.Tables[0].Rows[0];
				if(!drEach["vas_code"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					m_strVasCode = (string)drEach["vas_code"];
				}
				if(!drEach["surcharge"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					m_decSurcharge = (decimal)drEach["surcharge"];
				}
				if(!drEach["vas_description"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					m_strVASDescription = (string)drEach["vas_description"];
				}
			}
		}

		public void PopulateCustVas(String appID, String enterpriseID, String vasCode, String custID)
		{
			DbConnection dbCon= null;
			DataSet dsVAS =null;
			IDbCommand dbcmd=null;

			dbCon = DbConnectionManager.GetInstance().GetDbConnection(appID,enterpriseID);

			if (dbCon==null)
			{
				Logger.LogTraceError("VAS.cs","Populate","ERR001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			StringBuilder strQry =  new StringBuilder(); 

			strQry = strQry.Append("select * from Customer_Vas ");
			strQry.Append(" where applicationid = '");
			strQry.Append(appID);
			strQry.Append ("' and enterpriseid = '");
			strQry.Append (enterpriseID);
			strQry.Append ("' and custid ='");
			strQry.Append (Utility.ReplaceSingleQuote(custID));
			strQry.Append ("' and vas_code ='");
			strQry.Append (Utility.ReplaceSingleQuote(vasCode));
			strQry.Append ("'");

			dbcmd = dbCon.CreateCommand(strQry.ToString(),CommandType.Text);
 
			try
			{
				dsVAS = (DataSet)dbCon.ExecuteQuery(dbcmd,com.common.DAL.ReturnType.DataSetType);  
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("VAS.cs","Populate","ERR002","dbcon is null");
				throw appException;
			}

			if (dsVAS.Tables[0].Rows.Count  > 0) 
			{
				DataRow drEach = dsVAS.Tables[0].Rows[0];
				if(!drEach["vas_code"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					m_strVasCode = (string)drEach["vas_code"];
				}
				if(!drEach["surcharge"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					m_decSurcharge = (decimal)drEach["surcharge"];
				}
				if(!drEach["description"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					m_strVASDescription = (string)drEach["description"];
				}
			}
		}
	}
}
