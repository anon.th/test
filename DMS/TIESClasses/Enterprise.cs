using System;
using com.common.classes;
using com.common.DAL;
using System.Text;
using System.Data;

namespace com.ties.classes
{
	/// <summary>
	/// Summary description for Enterprise.
	/// </summary>
	public class Enterprise
	{
		public Enterprise()
		{
			//
			// TODO: Add constructor logic here
			//
		}
		private String m_strCountry = null;
		private decimal m_decDensityFactor = 0;
		private decimal m_decMaxInsAmt = 0;
		private decimal m_decInsPercSurchrg = 0;
		private decimal m_decIncremWt = 0;
		private decimal m_byteWtRoundingMethod;
		private decimal m_decFreeInsuranceAmt = 0;
		private int m_iCurrencyDecimal = 0;

		#region Declare By Aoo 15/02/2008

		private string strInsurancePercentSurcharge;
		private string strFreeInsuranceAmt;
		private string strMaxInsuranceAmt;

		#endregion

		#region Create Properties By Aoo 15/02/2008

		public string Insurance_Percent_Surcharge
		{
			get
			{
				return this.strInsurancePercentSurcharge;
			}
			set
			{
				this.strInsurancePercentSurcharge = value;
			}
		}
		public string Free_Insurance_Amt
		{
			get
			{
				return this.strFreeInsuranceAmt;
			}
			set
			{
				this.strFreeInsuranceAmt = value;
			}
		}
		public string Max_Insurance_Amt
		{
			get
			{
				return this.strMaxInsuranceAmt;
			}
			set
			{
				this.strMaxInsuranceAmt = value;
			}
		}

		#endregion

		public int CurrencyDecimal
		{
			set
			{
				m_iCurrencyDecimal = value;
			}

			get
			{
				return m_iCurrencyDecimal;
			}
		}

		/// <summary>
		/// This method gets and sets the value of Free Insurance Amount.
		/// </summary>
		public decimal FreeInsuranceAmount
		{
			set
			{
				m_decFreeInsuranceAmt = value;
			}
			get
			{
				return m_decFreeInsuranceAmt;
			}
		}

		/// <summary>
		/// This method gets and sets the Rounding Method.
		/// 0 : Round Off
		/// 1 : Ceiling
		/// 2 : Floor
		/// </summary>
		public decimal RoundingMethod
		{
			set
			{
				m_byteWtRoundingMethod = value;
			}
			get
			{
				return m_byteWtRoundingMethod;
			}
		}

		/// <summary>
		/// This method gets and sets the value of the Country.
		/// </summary>
		public String Country
		{
			set
			{
				m_strCountry = value;
			}
			get
			{
				return m_strCountry;
			}
		}

		/// <summary>
		/// This method gets and sets the value of Density Factor.
		/// </summary>
		public decimal DensityFactor
		{
			set
			{
				m_decDensityFactor = value;
			}
			get
			{
				return m_decDensityFactor;
			}
		}

		/// <summary>
		/// This method gets and sets the value of Maximum Insurance Amount.
		/// </summary>
		public decimal MaxInsuranceAmt
		{
			set
			{
				m_decMaxInsAmt = value;
			}
			get
			{
				return m_decMaxInsAmt;
			}
		}

		/// <summary>
		/// This method gets and sets the value of Insurance Percentage Surcharge.
		/// </summary>
		public decimal InsPercSurchrg
		{
			set
			{
				m_decInsPercSurchrg = value;
			}
			get
			{
				return m_decInsPercSurchrg;
			}
		}

		/// <summary>
		/// This method gets and sets the value of Increment weight.
		/// </summary>
		public decimal IncrementWt
		{
			set
			{
				m_decIncremWt = value;
			}
			get
			{
				return m_decIncremWt;
			}
		}

		/// <summary>
		/// This method populates values from the Enterprise Table.
		/// </summary>
		/// <param name="appID">Application ID</param>
		/// <param name="enterpriseID">Enterprise ID</param>
		public void Populate(String appID, String enterpriseID)
		{
			DbConnection dbCon = null;
			DataSet dsEnterprise = null;
			IDbCommand dbCmd = null;

			dbCon = DbConnectionManager.GetInstance().GetDbConnection(appID,enterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("Enterprise.cs","Populate","ERR001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			
			StringBuilder strQry = new StringBuilder();
			strQry = strQry.Append("select * from enterprise a ");
			strQry.Append(" where a.applicationid = '");
			strQry.Append(appID);
			strQry.Append("' and a.enterpriseid = '");
			strQry.Append(enterpriseID);
			strQry.Append("'");

			dbCmd = dbCon.CreateCommand(strQry.ToString(),CommandType.Text);

			try
			{
				dsEnterprise =(DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
			}
			catch(ApplicationException appExpection)
			{
				Logger.LogTraceError("Enterprise.cs","Populate","ERR002","Error in  Execute Query ");
				throw appExpection;
			}

			if(dsEnterprise.Tables[0].Rows.Count >0)
			{
				DataRow drEach = dsEnterprise.Tables[0].Rows[0];
				if(!drEach["country"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					m_strCountry = (String)drEach["country"];
				}
				if(!drEach["density_factor"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					m_decDensityFactor = (decimal)drEach["density_factor"];
				}
				if(!drEach["max_insurance_amt"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					m_decMaxInsAmt = (decimal)drEach["max_insurance_amt"];
				}

				if(!drEach["insurance_percent_surcharge"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					m_decInsPercSurchrg = (decimal)drEach["insurance_percent_surcharge"];
				}
			
				if(!drEach["wt_increment_amt"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					m_decIncremWt = (decimal)drEach["wt_increment_amt"];
				}
				if(!drEach["wt_rounding_method"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					m_byteWtRoundingMethod =  Convert.ToDecimal( drEach["wt_rounding_method"]);
				}
				if(!drEach["free_insurance_amt"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					m_decFreeInsuranceAmt =  (decimal)drEach["free_insurance_amt"];
				}
				if(!drEach["currency_decimal"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					m_iCurrencyDecimal = Convert.ToInt32((drEach["currency_decimal"]));
				}

				//Create By Aoo 15/02/2008
				
				if(!drEach["insurance_percent_surcharge"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					this.strInsurancePercentSurcharge = drEach["insurance_percent_surcharge"].ToString();
				}
				else
				{
					this.strInsurancePercentSurcharge = "";
				}

				if(!drEach["free_insurance_amt"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					this.strFreeInsuranceAmt = drEach["free_insurance_amt"].ToString();
				}
			
				else
				{
					this.strFreeInsuranceAmt = "";
				}

				if(!drEach["max_insurance_amt"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					this.strMaxInsuranceAmt = drEach["max_insurance_amt"].ToString();
				}
				else
				{
					this.strMaxInsuranceAmt = "";
				}
				
				//End Create By Aoo 15/02/2008


			}
		}
	}
}
