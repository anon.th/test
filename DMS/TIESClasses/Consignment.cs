using System;
using System.Data;
using com.common.DAL;
using System.Text;
using com.common.classes;

namespace com.ties.classes
{
	/// <summary>
	/// Summary description for Role.
	/// </summary>
	public class Consignment
	{
		private String m_No;
		private String m_Origin;
		private String m_Destination;
		private String m_Route;
		public Consignment()
		{
		}
		
		public String ConsignmentNo
		{
			set
			{
				m_No = value;
			}
			get
			{
				return m_No;
			}
		}

		public String Consignment_Origin
		{
			set
			{
				m_Origin = value;
			}
			get
			{
				return m_Origin;
			}
		}

		public String Consignment_Destination
		{
			set
			{
				m_Destination = value;
			}
			get
			{
				return m_Destination;
			}
		}
		public String Consignment_Route
		{
			set
			{
				m_Route = value;
			}
			get
			{
				return m_Route;
			}
		}
	}
}
