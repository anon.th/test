using System;

namespace com.ties.classes
{
	public struct InvoiceStatusInfo
	{
		public long iInvoiceCount;
		public long iCustomerCount;
		public long iWayBillCount;
	}

}
