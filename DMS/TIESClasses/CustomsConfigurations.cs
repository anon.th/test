using System;

namespace TIESClasses
{
	[Serializable()]
	public class CustomsConfigurations
	{
		private string action = "0";
		private string enterpriseId = string.Empty;
		private string codeID  = string.Empty;
		private string pk  = string.Empty;
		private string c1  = string.Empty;
		private string c2  = string.Empty;
		private string c3  = string.Empty;
		private string c4  = string.Empty;
		private string c5  = string.Empty;
		private string c6  = string.Empty;
		private string c7  = string.Empty;
		private string c8  = string.Empty;
		private string c9  = string.Empty;
		private string c10  = string.Empty;

		public string Action { set { action = value; } get { return action; } }
		public string EnterpriseId { set { enterpriseId = value; } get { return enterpriseId; } }
		public string CodeID { set { codeID = value; } get { return codeID; } }
		public string PK { set { pk = value; } get { return pk; } }
		public string C1 { set { c1 = value; } get { return c1; } }
		public string C2 { set { c2 = value; } get { return c2; } }
		public string C3 { set { c3 = value; } get { return c3; } }
		public string C4 { set { c4 = value; } get { return c4; } }
		public string C5 { set { c5 = value; } get { return c5; } }
		public string C6 { set { c6 = value; } get { return c6; } }
		public string C7 { set { c7 = value; } get { return c7; } }
		public string C8 { set { c8 = value; } get { return c8; } }
		public string C9 { set { c9 = value; } get { return c9; } }
		public string C10 { set { c10 = value; } get { return c10; } }

		public CustomsConfigurations() { }

		public CustomsConfigurations(string _action, string _enterpriseId) 
		{
			action = _action;
			enterpriseId = _enterpriseId;
		}

		public CustomsConfigurations(string _action, string _enterpriseId, string _codeID) 
		{
			action = _action;
			enterpriseId = _enterpriseId;
			codeID = _codeID;
		}
	}
}
