using System;
using System.Data;
using System.Text;
using com.common.DAL;
using com.common.classes;
using com.common.util;

namespace com.ties.classes
{
	/// <summary>
	/// Summary description for BandCode.
	/// </summary>
	public class BandDisc
	{
		public BandDisc()
		{
			
		}
		private string m_strAppID=null;
		private string m_strEnterpriseID=null;
		private string m_strBand_code=null;
		private string m_strBand_description=null;
		private decimal m_strPercent_discount=0;
	//	string strQry=null;

		public void Populate(string appid,string enterpriseid,string  band_code)
		{
			DbConnection dbCon= null;
			DataSet dsBandDisc =null;
			IDbCommand dbcmd=null;

			dbCon = DbConnectionManager.GetInstance().GetDbConnection(appid,enterpriseid);

			if (dbCon==null)
			{
				Logger.LogTraceError("BandDiscount.cs","Populate","BD001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			
			StringBuilder strQry =  new StringBuilder(); 

			strQry = strQry.Append("select * from Band_Discount a");
			strQry.Append(" where a.applicationid = '");
			strQry.Append(appid);
			strQry.Append ("' and a.enterpriseid = '");
			strQry.Append (enterpriseid);
			strQry.Append ("' and a.band_code ='");
			strQry.Append (Utility.ReplaceSingleQuote(band_code));
			strQry.Append ("'");

			dbcmd = dbCon.CreateCommand(strQry.ToString(),CommandType.Text);
 
			try
			{
				dsBandDisc = (DataSet)dbCon.ExecuteQuery(dbcmd,com.common.DAL.ReturnType.DataSetType);  
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("BandDiscount.cs","Populate","BD002","dbcon is null");
				throw appException;
			}
			if (dsBandDisc.Tables[0].Rows.Count  > 0) 
			{
				DataRow drEach = dsBandDisc.Tables[0].Rows[0];
				if(!drEach["band_code"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					m_strBand_code = (string)drEach["band_code"];
				}
				
				if(!drEach["band_description"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					m_strBand_description=(string)drEach["band_description"];
				}
				
				if(!drEach["percent_discount"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					m_strPercent_discount=(decimal)drEach["percent_discount"];
				}
			}
		}
		public String AppID
		{
			set
			{
				m_strAppID = value;
			}
			get
			{
				return m_strAppID;
			}
		}
		public string EnterpriseID
		{
			set
			{
				m_strEnterpriseID=value;
			}
			get
			{
				return m_strEnterpriseID;
			}
		}
		public string BandCode
		{
			set
			{
				m_strBand_code=value;
			}
			get
			{
				return m_strBand_code;
			}
		}
		public string BandDescription
		{
			set
			{
				m_strBand_description=value;
			}
			get
			{
				return m_strBand_description;
			}
		}
		public decimal PercentageDiscount
		{
			set
			{
				m_strPercent_discount=value ;
			}
			get
			{
				return m_strPercent_discount;
			}
		}	
		
	}

}



