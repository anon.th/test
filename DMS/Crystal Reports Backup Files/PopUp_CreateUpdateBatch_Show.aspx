<%@ Page language="c#" Codebehind="PopUp_CreateUpdateBatch_Show.aspx.cs" AutoEventWireup="false" Inherits="TIES.WebUI.Manifest.PopUp_CreateUpdateBatch_Show"  %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
  <HEAD>
		<title>Warning: Batch Exists!</title>
		<base target="_self">
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../css/Styles.css" type="text/css" rel="stylesheet">
		<!--#INCLUDE FILE="../msFormValidations.inc"-->
		<script language="javascript" src="../Scripts/settingScrollPosition.js"></script>
		<script language="javascript">
function ReadPassedData()
{
    var obj = window.dialogArguments;
    var tb1 = document.getElementById('txtBatchID');
    tb1.value = obj.data1; 
    var tb2 = document.getElementById('txtTruckID');
    tb2.value = obj.data2; 
    var tb3 = document.getElementById('txtBatchDate');
    tb3.value = obj.data3; 
    //var tb4 =document.getElementById('txtBatchNo');
    //tb4.value = obj.data4;
    //alert("xx");
}


function DoUnload()
{
	//alert(document.getElementById('txtBatchID').value);
    var obj = window.dialogArguments;
    obj.BatchNo = document.getElementById('txtBatchNo').value;
    obj.TruckID = document.getElementById('txtTruckID').value;
    obj.BatchDate = document.getElementById('txtBatchDate').value;
    obj.BatchType = document.getElementById('txtBatchType').value;
    obj.BatchID =  document.getElementById('txtBatchID').value;

    obj.Result = document.getElementById('txtResult').value;
	//alert(document.getElementById('txtBatchNo').value);
}

function setCancel()
{
document.getElementById('txtResult').value='Cancel';
}
		</script>
</HEAD>
	<body onload="ReadPassedData()" onunload="DoUnload()" MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<TABLE id="Table1" style="Z-INDEX: 101; LEFT: 8px; POSITION: absolute; TOP: 8px" cellSpacing="1"
				cellPadding="1" border="1">
				<TR>
					<TD><asp:label id="Label1" runat="server" Width="416px" CssClass="tableLabel">The batch you are requesting to create already  exists, based on the Truck ID, Batch Date and Batch ID you have entered.</asp:label></TD>
					<TD></TD>
					<TD><asp:button id="btnOk" runat="server" Width="101px" CssClass="queryButton" Text="Create"></asp:button></TD>
				</TR>
				<TR>
					<TD></TD>
					<TD></TD>
					<TD><asp:button id="btnCancel" runat="server" Width="101px" CssClass="queryButton" Text="Cancel"></asp:button></TD>
				</TR>
				<TR>
					<TD><asp:label id="Label2" runat="server" CssClass="tableLabel">Do you wish to update one of the existing batches?</asp:label></TD>
					<TD></TD>
					<TD></TD>
				</TR>
				<TR>
					<TD colSpan="3"><FONT face="Tahoma">
							<TABLE id="Table2" cellSpacing="0" cellPadding="0" width="100%" border="0">
								<TR>
									<TD><asp:radiobuttonlist id="rdoBatchList" runat="server" CssClass="tableLabel" AutoPostBack="True"></asp:radiobuttonlist></TD>
								</TR>
							</TABLE>
						</FONT>
					</TD>
				</TR>
				<TR>
					<TD><asp:label id="Label3" runat="server" CssClass="tableLabel">If you do not select one of the above batches a new batch will be created.</asp:label></TD>
					<TD></TD>
					<TD></TD>
				</TR>
			</TABLE>
			<asp:textbox id="txtResult" style="DISPLAY: none; Z-INDEX: 102; LEFT: 56px; POSITION: absolute; TOP: 240px"
				runat="server"></asp:textbox><asp:textbox id="txtBatchNo" style="DISPLAY: none; Z-INDEX: 102; LEFT: 56px; POSITION: absolute; TOP: 240px"
				runat="server"></asp:textbox><asp:textbox id="txtTruckID" style="DISPLAY: none; Z-INDEX: 102; LEFT: 56px; POSITION: absolute; TOP: 240px"
				runat="server"></asp:textbox><asp:textbox id="txtBatchDate" style="DISPLAY: none; Z-INDEX: 102; LEFT: 56px; POSITION: absolute; TOP: 240px"
				runat="server"></asp:textbox><asp:textbox id="txtBatchType" style="DISPLAY: none; Z-INDEX: 102; LEFT: 56px; POSITION: absolute; TOP: 240px"
				runat="server"></asp:textbox><asp:textbox id="txtBatchID" style="DISPLAY: none; Z-INDEX: 102; LEFT: 56px; POSITION: absolute; TOP: 240px"
				runat="server"></asp:textbox></form>
	</body>
</HTML>
