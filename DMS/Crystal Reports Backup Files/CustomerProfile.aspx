<%@ Page language="c#" Codebehind="CustomerProfile.aspx.cs" AutoEventWireup="false" Inherits="com.ties.CustomerProfile" ValidateRequest="false" %>
<%@ Register TagPrefix="iewc" Namespace="Microsoft.Web.UI.WebControls" Assembly="Microsoft.Web.UI.WebControls, Version=1.0.2.226, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="com.common.util" %>
<%@ Import Namespace="Cambro.Web.DbCombo" %>
<%@ Register TagPrefix="DbCombo" Namespace="Cambro.Web.DbCombo" Assembly="Cambro.Web.DbCombo" %>
<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>CustomerProfile</title>
		<meta name="vs_snapToGrid" content="True">
		<meta name="vs_showGrid" content="True">
		<LINK rel="stylesheet" type="text/css" href="css/Styles.css" name="stylesheet">
		<META name="GENERATOR" content="Microsoft Visual Studio 7.0">
		<META name="CODE_LANGUAGE" content="C#">
		<META name="vs_defaultClientScript" content="JavaScript">
		<META name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<!--#INCLUDE FILE="msFormValidations.inc"-->
		<script language="javascript" src="Scripts/settingScrollPosition.js"></script>
		<script language="javascript">
//by X oct 03 08
		function setValue()
		{
			var s = document.all['inFile'];
			document.all['txtFilePath'].innerText = s.value;
			if(s.value != '') 
				document.all['btnImport'].disabled = false;
			else
				document.all['btnImport'].disabled = true;
			
		}
		
		function upBrowse()
		{
			document.all['divBrowse'].style.backgroundImage='url(images/btn-browse-up.GIF)';
		}
		
		function downBrowse()
		{
			document.all['divBrowse'].style.backgroundImage='url(images/btn-browse-down.GIF)';
		}		
		
		function clearValue()
		{
			if(document.all['txtFilePath'].innerText == '') {
				document.location.reload();
			}
		}
		
		function setOnOff()
		{
			//if(document.all['btnImport'].visible==false)
			var btnImp = document.getElementById("<%= btnImport_Show.ClientID %>");
			if(btnImp.visible == true)
			document.all['divBrowse'].style.visibility = 'visible';
			else
			document.all['divBrowse'].style.visibility = 'hidden';
		}
	
		//by X oct 03 08
	
		function clientvalidate(source, args){
			var rdoCash = document.getElementById("<%= radioBtnCash.ClientID %>");
			var rdoCredit = document.getElementById("<%= radioBtnCredit.ClientID %>");
			
			if (!rdoCash.checked && !rdoCredit.checked){
				args.IsValid = false;
			}   else {
				args.IsValid = true;
			}
		}
		
		// by A 11/09/2014
		
		function RemoveBadPaseNumberwhithDot(strTemp, obj) {
			strTemp = strTemp.replace(/[^.0-9]/g, ''); //replace non-numeric
			obj.value = strTemp.replace(/\s/g, ''); // replace space
		}
		
		function AfterPasteNumberwhithDot(obj) {
			setTimeout(function () {
				RemoveBadPaseNumberwhithDot(obj.value, obj);
			}, 1); //or 4
		}
		
		</script>
	</HEAD>
	<BODY onunload="window.top.displayBanner.fnCloseAll(0);" onload="setOnOff();" MS_POSITIONING="GridLayout">
		<FORM id="CustomerProfile" encType="multipart/form-data" method="post" runat="server">
			<iewc:tabstrip style="Z-INDEX: 103; POSITION: absolute; TOP: 73px; LEFT: 29px" id="TabStrip1" tabIndex="10"
				runat="server" TargetID="customerProfileMultiPage" TabSelectedStyle="background-color:#0000ff;color:#000000;"
				TabHoverStyle="background-color:#777777" TabDefaultStyle="font-family:verdana;font-weight:bold;font-size:8pt;color:#0000ff;width:79;height:21;text-align:center;"
				Width="699px" Height="35px">
				<IEWC:TAB TargetID="CustomerPage" Text="Customer"></IEWC:TAB>
				<IEWC:TAB tabIndex="1" TargetID="SpecialInfoPage" Text="Special Information" ToolTip="Customer Special Information"></IEWC:TAB>
				<IEWC:TAB tabIndex="2" TargetID="ReferencePage" Text="Reference"></IEWC:TAB>
				<IEWC:TAB tabIndex="3" TargetID="StatusPage" Text="Status &amp; Exception"></IEWC:TAB>
				<IEWC:TAB tabIndex="4" TargetID="QuatationPage" Text="Quotation Status"></IEWC:TAB>
				<IEWC:TAB tabIndex="5" TargetID="Zones" Text="Zones"></IEWC:TAB>
				<IEWC:TAB tabIndex="6" TargetID="VAS" Text="VAS"></IEWC:TAB>
			</iewc:tabstrip><asp:label style="Z-INDEX: 116; POSITION: absolute; TOP: 8px; LEFT: 29px" id="lblMainTitle"
				runat="server" Width="477px" Height="26px" CssClass="mainTitleSize">Customer Profile</asp:label><asp:label style="Z-INDEX: 115; POSITION: absolute; TOP: 104px; LEFT: 460px" id="lblNumRec"
				runat="server" Width="274px" Height="19px" CssClass="RecMsg"></asp:label><asp:button style="Z-INDEX: 112; POSITION: absolute; TOP: 44px; LEFT: 27px" id="btnQry" runat="server"
				Width="62px" Height="22px" CssClass="queryButton" Text="Query" CausesValidation="False"></asp:button><asp:button style="Z-INDEX: 102; POSITION: absolute; TOP: 44px; LEFT: 89px" id="btnExecQry"
				tabIndex="1" runat="server" Width="130px" Height="22px" CssClass="queryButton" Text="Execute Query" CausesValidation="False"></asp:button><asp:button style="Z-INDEX: 110; POSITION: absolute; TOP: 44px; LEFT: 219px" id="btnInsert"
				tabIndex="2" runat="server" Width="62px" Height="22px" CssClass="queryButton" Text="Insert" CausesValidation="False"></asp:button><asp:button style="Z-INDEX: 101; POSITION: absolute; TOP: 44px; LEFT: 280px" id="btnSave" tabIndex="3"
				runat="server" Width="66px" Height="22px" CssClass="queryButton" Text="Save" CausesValidation="True"></asp:button><asp:button style="Z-INDEX: 111; POSITION: absolute; TOP: 45px; LEFT: 346px" id="btnDelete"
				tabIndex="4" runat="server" Width="62px" Height="21px" CssClass="queryButton" Text="Delete" CausesValidation="False" Visible="False"></asp:button><asp:button style="Z-INDEX: 109; POSITION: absolute; TOP: 43px; LEFT: 609px" id="btnGoToFirstPage"
				tabIndex="5" runat="server" Width="24px" CssClass="queryButton" Text="|<" CausesValidation="False"></asp:button><asp:button style="Z-INDEX: 105; POSITION: absolute; TOP: 43px; LEFT: 633px" id="btnPreviousPage"
				tabIndex="6" runat="server" Width="24px" CssClass="queryButton" Text="<" CausesValidation="False"></asp:button><asp:button style="Z-INDEX: 106; POSITION: absolute; TOP: 43px; LEFT: 679px" id="btnNextPage"
				tabIndex="8" runat="server" Width="24px" CssClass="queryButton" Text=">" CausesValidation="False"></asp:button><asp:button style="Z-INDEX: 107; POSITION: absolute; TOP: 43px; LEFT: 703px" id="btnGoToLastPage"
				tabIndex="9" runat="server" Width="24px" CssClass="queryButton" Text=">|" CausesValidation="False"></asp:button><asp:textbox style="Z-INDEX: 108; POSITION: absolute; TOP: 44px; LEFT: 657px" id="txtCountRec"
				tabIndex="7" runat="server" Width="24px"></asp:textbox><iewc:multipage style="Z-INDEX: 104; POSITION: absolute; TOP: 128px; LEFT: 32px" id="customerProfileMultiPage"
				runat="server" Width="830px" Height="800px" SelectedIndex="1">
				<IEWC:PAGEVIEW id="CustomerPage">
					<TABLE style="Z-INDEX: 112; WIDTH: 780px" id="CustomerInfoTable" border="0" width="730"
						runat="server">
						<TR width="100%">
							<TD width="100%">
								<FIELDSET><LEGEND>
										<asp:Label id="lbl" CssClass="tableHeadingFieldset" Runat="server">Customer Information</asp:Label></LEGEND>
									<TABLE id="tblCustInfo" border="0" width="100%" align="left" runat="server">
										<TR height="5">
											<TD width="2%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="8%"></TD>
										</TR>
										<TR height="25">
											<TD align="right">
												<ASP:REQUIREDFIELDVALIDATOR id="validateAccNo" Width="100%" Runat="server" ErrorMessage="Account No." Display="None"
													ControlToValidate="txtAccNo"></ASP:REQUIREDFIELDVALIDATOR>
												<asp:label id="Label1" runat="server" Height="22px" Width="100%" CssClass="errorMsgColor">&nbsp;*&nbsp;</asp:label></TD>
											<TD colSpan="3">
												<asp:label id="lblAccNo" runat="server" Height="22px" Width="100%" CssClass="tableLabel">Account No.</asp:label></TD>
											<TD colSpan="3">
												<CC1:MSTEXTBOX style="TEXT-TRANSFORM: uppercase" id="txtAccNo" tabIndex="11" runat="server" Width="100%"
													CssClass="textField" TextMaskType="msUpperAlfaNumericWithUnderscore" MaxLength="20" AutoPostBack="False"></CC1:MSTEXTBOX></TD>
											<TD></TD>
											<TD>
												<asp:label id="lblRefNo" runat="server" Height="22px" Width="100%" CssClass="tableLabel">Customer Ref No</asp:label></TD>
											<TD colSpan="3">
												<CC1:MSTEXTBOX style="TEXT-TRANSFORM: uppercase" id="txtRefNo" tabIndex="12" runat="server" Width="100%"
													CssClass="textField" TextMaskType="msUpperAlfaNumericWithUnderscore" MaxLength="12" Enabled="True"></CC1:MSTEXTBOX></TD>
											<TD colSpan="2" align="right">
												<asp:label id="lblStatus" runat="server" Height="22px" Width="100%" CssClass="tableLabel">Status</asp:label></TD>
											<TD colSpan="3">
												<ASP:DROPDOWNLIST id="ddlStatus" tabIndex="13" runat="server" Width="100%" CssClass="textField" Enabled="True"></ASP:DROPDOWNLIST></TD>
										</TR>
										<TR height="10">
											<TD colSpan="20"></TD>
										</TR>
										<TR height="25">
											<TD>
												<ASP:REQUIREDFIELDVALIDATOR id="reqfieldvalCustomerName" Width="100%" Runat="server" ErrorMessage="Customer Name"
													Display="None" ControlToValidate="txtCustomerName"></ASP:REQUIREDFIELDVALIDATOR>
												<asp:label id="lblreqCustomerName" runat="server" Height="22px" Width="100%" CssClass="errorMsgColor">&nbsp;*&nbsp;</asp:label></TD>
											<TD colSpan="3">
												<asp:label id="lblCustomerName" runat="server" Height="22px" Width="100%" CssClass="tableLabel">Customer Name</asp:label></TD>
											<TD colSpan="8">
												<asp:textbox style="TEXT-TRANSFORM: uppercase" id="txtCustomerName" tabIndex="14" runat="server"
													Width="285px" CssClass="textField" MaxLength="100"></asp:textbox></TD>
											<TD align="right"></TD>
											<TD colSpan="3">
												<asp:label id="lblContactPerson" runat="server" Height="22px" Width="100%" CssClass="tableLabel">Contact Person</asp:label></TD>
											<TD colSpan="4">
												<asp:textbox style="TEXT-TRANSFORM: uppercase" id="txtContactPerson" tabIndex="21" runat="server"
													Width="200px" CssClass="textField" MaxLength="100"></asp:textbox></TD>
										</TR>
										<TR height="25">
											<TD>
												<ASP:REQUIREDFIELDVALIDATOR id="reqfieldvalAddress1" Width="100%" Runat="server" ErrorMessage="Address1" Display="None"
													ControlToValidate="txtAddress1"></ASP:REQUIREDFIELDVALIDATOR>
												<asp:label id="lblreqAddress1" runat="server" Height="22px" Width="100%" CssClass="errorMsgColor">&nbsp;*&nbsp;</asp:label></TD>
											<TD colSpan="3">
												<asp:label id="lblAddress1" runat="server" Height="22px" Width="100%" CssClass="tableLabel">Address 1</asp:label></TD>
											<TD colSpan="8">
												<asp:textbox style="TEXT-TRANSFORM: uppercase" id="txtAddress1" tabIndex="15" runat="server"
													Width="285px" CssClass="textField" MaxLength="100"></asp:textbox></TD>
											<TD>
												<ASP:REQUIREDFIELDVALIDATOR id="reqfieldvalTelephone" Width="100%" Runat="server" ErrorMessage="Telephone" Display="None"
													ControlToValidate="txtTelephone"></ASP:REQUIREDFIELDVALIDATOR>
												<asp:label id="lblreqTelephone" runat="server" Height="22px" Width="100%" CssClass="errorMsgColor">&nbsp;*&nbsp;</asp:label></TD>
											<TD colSpan="3">
												<asp:label id="lblTelphone" runat="server" Height="22px" Width="100%" CssClass="tableLabel">Telephone</asp:label></TD>
											<TD colSpan="4">
												<asp:textbox id="txtTelephone" tabIndex="22" runat="server" Width="200px" CssClass="textField"
													MaxLength="20"></asp:textbox></TD>
										</TR>
										<TR height="25">
											<TD>&nbsp;</TD>
											<TD colSpan="3">
												<asp:label id="lblAddress2" runat="server" Height="22px" Width="100%" CssClass="tableLabel">Address 2</asp:label></TD>
											<TD colSpan="8">
												<asp:textbox style="TEXT-TRANSFORM: uppercase" id="txtAddress2" tabIndex="17" runat="server"
													Width="285px" CssClass="textField" MaxLength="100"></asp:textbox></TD>
											<TD>&nbsp;</TD>
											<TD colSpan="3">
												<asp:label id="lblFax" runat="server" Height="22px" Width="100%" CssClass="tableLabel">Fax</asp:label></TD>
											<TD colSpan="4">
												<asp:textbox id="txtFax" tabIndex="22" runat="server" Width="200px" CssClass="textField" MaxLength="20"></asp:textbox></TD>
										</TR>
										<TR height="25">
											<TD align="right">
												<ASP:REQUIREDFIELDVALIDATOR id="reqfieldvalZipCode" Width="100%" Runat="server" ErrorMessage="Postal Code" Display="None"
													ControlToValidate="txtZipCode"></ASP:REQUIREDFIELDVALIDATOR>
												<asp:label id="lblreqZipCode" runat="server" Height="22px" Width="100%" CssClass="errorMsgColor">&nbsp;*&nbsp;</asp:label></TD>
											<TD colSpan="3">
												<asp:label id="lblZip" runat="server" Height="22px" Width="100%" CssClass="tableLabel">Postal Code</asp:label></TD>
											<TD colSpan="2">
												<CC1:MSTEXTBOX id="txtZipCode" tabIndex="18" runat="server" Width="100%" CssClass="textField" TextMaskType="msUpperAlfaNumericWithUnderscore"
													MaxLength="12" AutoPostBack="False"></CC1:MSTEXTBOX></TD>
											<TD>
												<asp:button id="btnZipcodePopup" tabIndex="19" runat="server" Width="100%" CssClass="queryButton"
													CausesValidation="False" Text="..."></asp:button></TD>
											<TD>
												<asp:label id="lblState" runat="server" Height="22px" Width="100%" CssClass="tableLabel">State</asp:label></TD>
											<TD colSpan="3">
												<asp:textbox style="TEXT-TRANSFORM: uppercase" id="txtState" tabIndex="20" runat="server" Width="100%"
													CssClass="textField" ReadOnly="True"></asp:textbox></TD>
											<TD colSpan="2">&nbsp;</TD>
											<TD colSpan="3">
												<asp:label id="lblEmail" runat="server" Height="22px" Width="100%" CssClass="tableLabel">Email</asp:label></TD>
											<TD colSpan="4">
												<asp:textbox id="txtEmail" tabIndex="23" runat="server" Width="160px" CssClass="textField" MaxLength="50"></asp:textbox></TD>
										</TR>
										<TR height="25">
											<TD align="right"></TD>
											<TD colSpan="3">
												<asp:label id="lblCountry" runat="server" Height="22px" Width="100%" CssClass="tableLabel">Country</asp:label></TD>
											<TD colSpan="4">
												<asp:textbox style="TEXT-TRANSFORM: uppercase" id="txtCountry" tabIndex="20" runat="server" Width="100%"
													CssClass="textField" AutoPostBack="False" ReadOnly="True"></asp:textbox></TD>
											<TD colSpan="5">&nbsp;</TD>
											<TD colSpan="3">
												<asp:label id="lblCreatedBy" runat="server" Height="22px" Width="100%" CssClass="tableLabel">Created By</asp:label></TD>
											<TD colSpan="4">
												<asp:textbox style="TEXT-TRANSFORM: uppercase" id="txtCreatedBy" tabIndex="24" runat="server"
													Width="200px" CssClass="textField" AutoPostBack="False" ReadOnly="True"></asp:textbox></TD>
										</TR>
									</TABLE>
								</FIELDSET>
							</TD>
						</TR>
						<TR id="Tr1" runat="server" width="100%">
							<TD width="100%">
								<FIELDSET><LEGEND>
										<asp:Label id="lblBasicPayment" runat="server" CssClass="tableHeadingFieldset">Basic Payment Details</asp:Label></LEGEND></LEGEND>
									<TABLE id="Table10" border="0" width="100%" align="left" runat="server">
										<TR height="5">
											<TD width="2%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="8%"></TD>
										</TR>
										<TR>
											<TD>&nbsp;</TD>
											<TD colSpan="3">
												<asp:label id="Label30" runat="server" Width="100%" CssClass="tableLabel">Payment Mode</asp:label></TD>
											<TD colSpan="3" align="left">
												<TABLE border="0" cellSpacing="0" cellPadding="0" width="100%">
													<TR>
														<TD>
															<ASP:RADIOBUTTON id="radioBtnCash" tabIndex="25" CssClass="tableRadioButton" Text="Cash" Runat="server"
																GroupName="CashCredit" Font-Size="Smaller"></ASP:RADIOBUTTON></TD>
														<TD>
															<ASP:RADIOBUTTON id="radioBtnCredit" tabIndex="25" CssClass="tableRadioButton" Text="Credit" Runat="server"
																GroupName="CashCredit" Font-Size="Smaller"></ASP:RADIOBUTTON></TD>
													</TR>
												</TABLE>
											</TD>
											<TD>&nbsp;</TD>
											<TD colSpan="3">
												<asp:label id="Label31" runat="server" Width="100%" CssClass="tableLabel">Credit Term</asp:label></TD>
											<TD colSpan="2">
												<CC1:MSTEXTBOX id="txtCreditTerm" visible="false" tabIndex="30" runat="server" CssClass="textFieldRightAlign"
													Readonly="true"></CC1:MSTEXTBOX>
												<CC1:MSTEXTBOX id="txtCreditTermAccPac" enabled="false" tabIndex="30" runat="server" CssClass="textFieldRightAlign"
													Readonly="true"></CC1:MSTEXTBOX>
											</TD>
											<TD colSpan="3">
												<ASP:REQUIREDFIELDVALIDATOR id="Requiredfieldvalidator2" Runat="server" ErrorMessage="Tax Exempt." Display="None"
													ControlToValidate="ddlTextExempt"></ASP:REQUIREDFIELDVALIDATOR>
												<asp:label id="Label28" runat="server" CssClass="tableLabel">Tax Exempt</asp:label>
												<asp:label id="Label41" runat="server" CssClass="tableLabel" ForeColor="Red">*</asp:label></TD>
											<TD colSpan="2" align="left">
												<ASP:DROPDOWNLIST id="ddlTextExempt" tabIndex="42" runat="server" Width="100%" CssClass="textField"
													AutoPostBack="False"></ASP:DROPDOWNLIST></TD>
										</TR>
									</TABLE>
								</FIELDSET>
							</TD>
						</TR>
						<TR id="tbTrPaymentInfo" runat="server" width="100%">
							<TD width="100%">
								<ASP:PANEL id="pnCredit" Runat="server">
									<FIELDSET><LEGEND>
											<asp:Label id="lblPyDetails" runat="server" CssClass="tableHeadingFieldset">Credit Control and Bill Placement Details</asp:Label></LEGEND>
										<TABLE id="tblPaymentInfo" border="0" width="100%" align="left" runat="server">
											<TR height="5">
												<TD width="2%"></TD>
												<TD width="5%"></TD>
												<TD width="5%"></TD>
												<TD width="5%"></TD>
												<TD width="5%"></TD>
												<TD width="5%"></TD>
												<TD width="5%"></TD>
												<TD width="5%"></TD>
												<TD width="5%"></TD>
												<TD width="5%"></TD>
												<TD width="5%"></TD>
												<TD width="5%"></TD>
												<TD width="5%"></TD>
												<TD width="5%"></TD>
												<TD width="5%"></TD>
												<TD width="5%"></TD>
												<TD width="5%"></TD>
												<TD width="5%"></TD>
												<TD width="5%"></TD>
												<TD width="8%"></TD>
											</TR>
											<TR>
												<TD>&nbsp;</TD>
												<TD colSpan="4"><%--<asp:label id="lblPaymentMode" runat="server" Height="22px" Width="100%" CssClass="tableLabel">Payment Mode</asp:label>--%></TD>
												<TD colSpan="3" align="left"><%--
												<asp:radiobutton id="radioBtnCash" runat="server" tabIndex="27" Height="15px" Text="Cash" Font-Size="Smaller" 
 CssClass="tableRadioButton" onclick="event.returnValue=false;"></asp:radiobutton>
												<asp:radiobutton id="radioBtnCredit" runat="server" tabIndex="28" Height="15px" Text="Credit" Font-Size="Smaller" 
 CssClass="tableRadioButton" onclick="event.returnValue=false;"></asp:radiobutton>
											--%><%--Fixed by A 11/09/2014--%><%--<table width="100%" border="0" cellpadding="0" cellspacing="0">
													<tr>
														<td><asp:RadioButton id="radioBtnCash" Runat="server" Text="Cash" Font-Size="Smaller" CssClass="tableRadioButton"
													GroupName="CashCredit" tabIndex="25" ></asp:RadioButton></td>
														<td><asp:RadioButton id="radioBtnCredit" Runat="server" Text="Credit" Font-Size="Smaller"
													CssClass="tableRadioButton" GroupName="CashCredit" tabIndex="25" ></asp:RadioButton></td>
													</tr>
												</table>--%></TD>
												<TD colSpan="4" align="left"></TD>
												<TD><%--<asp:requiredfieldvalidator id="RequirePaymentMode" Width="100%" Runat="server" ControlToValidate="radioBtnCash" Display="None" ErrorMessage="Payment Mode"></asp:requiredfieldvalidator>--%>
													<ASP:CUSTOMVALIDATOR id="rdo" Runat="server" ErrorMessage="Payment Mode" Display="None" Enabled="False"
														EnableClientScript="True" ClientValidationFunction="clientvalidate"></ASP:CUSTOMVALIDATOR><!--<asp:label id="lblReqPaymentMode" runat="server" Height="22px" Width="100%" CssClass="errorMsgColor">&nbsp;*&nbsp;</asp:label>--></TD>
												<TD colSpan="3">
													<asp:label id="lblCreditOutstanding" runat="server" Height="22px" Width="100%" CssClass="tableLabel">Credit Available</asp:label></TD>
												<TD colSpan="4">
													<CC1:MSTEXTBOX id="txtCreditOutstanding" tabIndex="27" runat="server" CssClass="textFieldRightAlign"
														Visible="false" TextMaskType="msNumeric" MaxLength="9" NumberScale="2" NumberPrecision="8"
														NumberMinValue="0" NumberMaxValue="999999"></CC1:MSTEXTBOX>
													<CC1:MSTEXTBOX id="txtCreditAvailable" tabIndex="27" runat="server" CssClass="textFieldRightAlign"
														Readonly="true"></CC1:MSTEXTBOX></TD>
											</TR>
											<TR>
												<TD align="right"></TD>
												<TD colSpan="4">
													<asp:label id="lblCreditLimit" runat="server" Height="22px" Width="100%" CssClass="tableLabel">Credit Limit</asp:label></TD>
												<TD colSpan="3">
													<CC1:MSTEXTBOX id="txtCreditLimit" tabIndex="28" runat="server" CssClass="textFieldRightAlign"
														Readonly="true"></CC1:MSTEXTBOX></TD>
												<TD colSpan="5">&nbsp;</TD>
												<TD colSpan="3">
													<asp:label id="lblCreditUsed" runat="server" Height="22px" Width="100%" CssClass="tableLabel">Credit Used</asp:label></TD>
												<TD colSpan="4">
													<CC1:MSTEXTBOX id="txtCreditUsed" tabIndex="29" runat="server" CssClass="textFieldRightAlign" Readonly="true"></CC1:MSTEXTBOX></TD>
											</TR>
											<TR>
												<TD align="right"></TD>
												<TD colSpan="4"><%--<asp:label id="lblCreditTerm" runat="server" Height="22px" Width="100%" CssClass="tableLabel">Credit Term</asp:label>--%></TD>
												<TD colSpan="3"><%--<cc1:mstextbox id="txtCreditTerm" tabIndex="30" runat="server" CssClass="textFieldRightAlign" Readonly="true"></cc1:mstextbox>--%></TD>
												<TD colSpan="5">&nbsp;</TD>
												<TD colSpan="3">
													<asp:label id="Label23" runat="server" Height="22px" Width="100%" CssClass="tableLabel">Next Bill Placement Date</asp:label></TD>
												<TD colSpan="4">
													<CC1:MSTEXTBOX id="txtNextBillPlacementDate" tabIndex="31" runat="server" Width="100%" CssClass="textField"
														TextMaskType="msDate" MaxLength="12" AutoPostBack="true" TextMaskString="99/99/9999"></CC1:MSTEXTBOX></TD>
											</TR>
											<TR>
												<TD align="right"></TD>
												<TD colSpan="4">
													<asp:label id="Label22" runat="server" Height="22px" Width="100%" CssClass="tableLabel">Credit Status</asp:label></TD>
												<TD colSpan="3">
													<CC1:MSTEXTBOX id="txtCreditStatus" tabIndex="32" runat="server" CssClass="textFieldRightAlign"
														Readonly="true"></CC1:MSTEXTBOX></TD>
												<TD colSpan="5">
													<asp:button id="btnChangeStatus" tabIndex="33" runat="server" Height="22px" Width="150px" CssClass="queryButton"
														CausesValidation="False" Text="Change Status" Enabled="False"></asp:button></TD>
												<TD colSpan="3">
													<asp:button id="btnSetBillPlacementRules" tabIndex="34" runat="server" Height="22px" Width="170px"
														CssClass="queryButton" CausesValidation="False" Text="Set Bill Placement Rules"></asp:button></TD>
												<TD colSpan="4"></TD>
											</TR>
											<TR>
												<TD>&nbsp;</TD>
												<TD colSpan="4">
													<asp:label id="Label24" runat="server" Height="22px" Width="100%" CssClass="tableLabel">Credit Threshold(%)</asp:label></TD>
												<TD colSpan="3">
													<CC1:MSTEXTBOX id="txtCreditThredholds" tabIndex="35" runat="server" CssClass="textFieldRightAlign"
														Readonly="true"></CC1:MSTEXTBOX></TD>
												<TD colSpan="5">&nbsp;</TD>
												<TD colSpan="3">
													<asp:label id="lblActiveQuatationNo" runat="server" Height="22px" Width="100%" CssClass="tableLabel">Active Quotation</asp:label></TD>
												<TD colSpan="4">
													<CC1:MSTEXTBOX id="txtActiveQuatationNo" tabIndex="36" runat="server" Width="100%" CssClass="textField"
														TextMaskType="msUpperAlfaNumericWithUnderscore" MaxLength="12" ReadOnly="True"></CC1:MSTEXTBOX></TD>
											</TR>
											<TR height="25">
												<TD></TD>
												<TD colSpan="4">
													<asp:label id="lblMBG" runat="server" Height="22px" Width="100%" CssClass="tableLabel">MBG</asp:label></TD>
												<TD colSpan="3">
													<ASP:DROPDOWNLIST id="ddlMBG" tabIndex="37" runat="server" Width="100%" CssClass="textField"></ASP:DROPDOWNLIST></TD>
												<TD colSpan="5">&nbsp;</TD>
												<TD colSpan="7">
													<ASP:CHECKBOX id="chkBox" tabIndex="38" runat="server" AutoPostBack="True"></ASP:CHECKBOX>
													<asp:label id="Label20x" runat="server" Height="10px" Width="140px" CssClass="tableLabel">Customer BOX Minimum</asp:label>
													<CC1:MSTEXTBOX id="txtMinimumBox" tabIndex="39" runat="server" Width="30px" CssClass="textFieldRightAlign"
														TextMaskType="msNumeric" MaxLength="9" Enabled="True" ReadOnly="true" NumberScale="0" NumberPrecision="8"
														NumberMinValue="1" NumberMaxValue="99"></CC1:MSTEXTBOX></TD>
											</TR>
											<TR id="Remark" runat="server">
												<TD></TD>
												<TD colSpan="4"></TD>
												<TD colSpan="2"></TD>
												<TD colSpan="4">&nbsp;</TD>
												<TD vAlign="bottom" colSpan="3">
													<asp:label id="lblRemarkBp" runat="server" Height="22px" Width="100%" CssClass="tableLabelRight">BPD Remark</asp:label></TD>
												<TD colSpan="6">
													<CC1:MSTEXTBOX id="txtRemarkBp" tabIndex="40" runat="server" Width="100%" CssClass="textField"
														TextMaskType="msUpperAlfaNumericWithUnderscore" MaxLength="200" AutoPostBack="true" ReadOnly="True"></CC1:MSTEXTBOX></TD>
											</TR>
											<TR>
												<TD></TD>
												<TD colSpan="4"></TD>
												<TD colSpan="2"></TD>
												<TD colSpan="4">&nbsp;</TD>
												<TD colSpan="5"></TD>
												<TD colSpan="4"></TD>
											</TR>
											<TR height="25">
											</TR>
										</TABLE>
									</FIELDSET>
								</ASP:PANEL></TD>
						</TR>
						<TR>
							<TD width="100%">
								<ASP:PANEL id="panel_Customs" Runat="server">
									<FIELDSET><LEGEND>
											<asp:Label id="Label25" runat="server" CssClass="tableHeadingFieldset">Customs</asp:Label></LEGEND>
										<TABLE style="maring: 5px" id="Table9" border="0" width="100%" align="left" runat="server">
											<TR>
												<TD width="130">
													<asp:label id="Label27" runat="server" CssClass="tableLabel">Tax Code</asp:label></TD>
												<TD width="50" align="left">
													<asp:textbox id="txtTaxCode" tabIndex="41" runat="server" Width="100%" CssClass="textField" Enabled="True"></asp:textbox></TD>
												<TD vAlign="top" rowSpan="3" align="left">
													<FIELDSET style="MARGIN: 0px 5px 0px 0px"><LEGEND>
															<asp:Label id="Label36" runat="server" CssClass="tableHeadingFieldset">Other Fees</asp:Label></LEGEND>
														<TABLE border="0" width="100%" align="left">
															<TR>
																<TD width="80">
																	<asp:label id="Label37" runat="server" CssClass="tableLabel">Agency Fee:</asp:label></TD>
																<TD align="left">
																	<CC1:MSTEXTBOX id="txtAgencyFree" tabIndex="43" onpaste="AfterPasteNumberwhithDot(this)" runat="server"
																		Width="100px" CssClass="textFieldRightAlign" TextMaskType="msNumeric" NumberPrecision="19"
																		NumberMinValue="0" NumberMaxValue="99999999"></CC1:MSTEXTBOX></TD>
																<TD width="110">
																	<asp:label id="Label38" runat="server" CssClass="tableLabel">Counter Clearance:</asp:label></TD>
																<TD>
																	<CC1:MSTEXTBOX id="txtCounterClearance" tabIndex="44" onpaste="AfterPasteNumberwhithDot(this)"
																		runat="server" Width="100px" CssClass="textFieldRightAlign" TextMaskType="msNumeric" NumberPrecision="19"
																		NumberMinValue="0" NumberMaxValue="99999999"></CC1:MSTEXTBOX></TD>
															</TR>
															<TR>
																<TD>
																	<asp:label id="Label40" runat="server" CssClass="tableLabel">DAWB Fee:</asp:label></TD>
																<TD colSpan="3">
																	<CC1:MSTEXTBOX id="txtDawbFee" tabIndex="45" onpaste="AfterPasteNumberwhithDot(this)" runat="server"
																		Width="100px" CssClass="textFieldRightAlign" TextMaskType="msNumeric" NumberPrecision="19"
																		NumberMinValue="0" NumberMaxValue="99999999"></CC1:MSTEXTBOX></TD>
															</TR>
															<TR>
																<TD vAlign="top" colSpan="4">
																	<TABLE border="0" cellSpacing="0" cellPadding="0">
																		<TR>
																			<TD vAlign="top">
																				<asp:label id="Label43" runat="server" Width="200px" CssClass="tableLabel">Currency Adjustment Factor (%):</asp:label></TD>
																			<TD vAlign="top" width="75">
																				<CC1:MSTEXTBOX id="txtCurrencyAdjstmentFactor" tabIndex="46" onpaste="AfterPasteNumberwhithDot(this)"
																					runat="server" Width="65px" CssClass="textFieldRightAlign" TextMaskType="msNumeric" NumberScale="2"
																					NumberPrecision="19" NumberMinValue="0" NumberMaxValue="99999999"></CC1:MSTEXTBOX></TD>
																			<TD vAlign="top">
																				<asp:label id="Label39" runat="server" Width="120px" CssClass="tableLabel">Express Freight<br /> Collect Discount (%)</asp:label></TD>
																			<TD vAlign="top">
																				<CC1:MSTEXTBOX id="txtCollectDiscount" tabIndex="47" onpaste="AfterPasteNonNumber(this)" runat="server"
																					Width="60px" CssClass="textFieldRightAlign" TextMaskType="msNumericCOD" NumberScale="2" NumberPrecision="5"
																					NumberMinValueCOD="0" NumberMaxValueCOD="100"></CC1:MSTEXTBOX></TD>
																		</TR>
																	</TABLE>
																</TD>
															</TR>
														</TABLE>
													</FIELDSET>
												</TD>
											</TR>
											<TR>
												<TD><%--<asp:requiredfieldvalidator id="Requiredfieldvalidator2" Runat="server" ControlToValidate="ddlTextExempt"
														Display="None" ErrorMessage="Tax Exempt."></asp:requiredfieldvalidator>
													<asp:label id="Label28" runat="server" CssClass="tableLabel">Tax Exempt</asp:label>
													<asp:label id="Label41" runat="server" CssClass="tableLabel" ForeColor="Red">*</asp:label>--%></TD>
												<TD align="left"><%--<asp:DropDownList id="ddlTextExempt" runat="server" tabIndex="42" Width="100%" CssClass="textField"
														AutoPostBack="False"></asp:DropDownList>--%></TD>
											</TR>
											<TR>
												<TD height="50" colSpan="3"></TD>
											</TR>
											<TR>
												<TD colSpan="2">
													<FIELDSET style="MARGIN: 0px 5px 0px 0px"><LEGEND>
															<asp:Label id="Label29" runat="server" CssClass="tableHeadingFieldset">Cartage</asp:Label></LEGEND>
														<TABLE border="0" width="100%" align="left">
															<TR>
																<TD width="130">
																	<asp:label id="lblCartageUpToKG" runat="server" Width="100%" CssClass="tableLabel">Up to 300KG</asp:label></TD>
																<TD>
																	<CC1:MSTEXTBOX id="txtCartageUpToKG" tabIndex="48" onpaste="AfterPasteNumberwhithDot(this)" runat="server"
																		Width="70px" CssClass="textFieldRightAlign" TextMaskType="msNumeric" NumberPrecision="19" NumberMinValue="0"
																		NumberMaxValue="99999999"></CC1:MSTEXTBOX></TD>
															</TR>
															<TR>
																<TD>
																	<asp:label id="lblAddl" runat="server" CssClass="tableLabel">Addl > 300KG(per kg)</asp:label></TD>
																<TD>
																	<CC1:MSTEXTBOX id="txtAddl" tabIndex="49" onpaste="AfterPasteNumberwhithDot(this)" runat="server"
																		Width="70px" CssClass="textFieldRightAlign" TextMaskType="msNumeric" NumberPrecision="19" NumberMinValue="0"
																		NumberMaxValue="99999999"></CC1:MSTEXTBOX></TD>
															</TR>
														</TABLE>
													</FIELDSET>
												</TD>
												<TD>
													<FIELDSET style="MARGIN: 0px 5px 0px 0px"><LEGEND>
															<asp:Label id="Label32" runat="server" CssClass="tableHeadingFieldset">Entry Fees</asp:Label></LEGEND>
														<TABLE border="0" width="100%" align="left">
															<TR>
																<TD width="15%">
																	<asp:label id="Label33" runat="server" Width="100%" CssClass="tableLabel">Informal:</asp:label></TD>
																<TD>
																	<CC1:MSTEXTBOX id="txtInformal" tabIndex="50" onpaste="AfterPasteNumberwhithDot(this)" runat="server"
																		Width="100px" CssClass="textFieldRightAlign" TextMaskType="msNumeric" NumberPrecision="19"
																		NumberMinValue="0" NumberMaxValue="99999999"></CC1:MSTEXTBOX></TD>
																<TD width="30%">
																	<asp:label id="Label35" runat="server" Width="90%" CssClass="tableLabel">Formal (1st page):</asp:label></TD>
																<TD>
																	<CC1:MSTEXTBOX id="txtFormal" tabIndex="51" onpaste="AfterPasteNumberwhithDot(this)" runat="server"
																		Width="90px" CssClass="textFieldRightAlign" TextMaskType="msNumeric" NumberPrecision="19" NumberMinValue="0"
																		NumberMaxValue="99999999"></CC1:MSTEXTBOX></TD>
															</TR>
															<TR>
																<TD></TD>
																<TD></TD>
																<TD>
																	<asp:label id="Label34" runat="server" Width="100%" CssClass="tableLabel">Addl page (each):</asp:label></TD>
																<TD>
																	<CC1:MSTEXTBOX id="txtAddlPage" tabIndex="52" onpaste="AfterPasteNumberwhithDot(this)" runat="server"
																		Width="90px" CssClass="textFieldRightAlign" TextMaskType="msNumeric" NumberPrecision="19" NumberMinValue="0"
																		NumberMaxValue="99999999"></CC1:MSTEXTBOX></TD>
															</TR>
														</TABLE>
													</FIELDSET>
												</TD>
											</TR>
										</TABLE>
									</FIELDSET>
								</ASP:PANEL></TD>
						</TR>
						<TR>
							<TD width="100%">
								<ASP:PANEL id="panel_clientGateway" Runat="server">
									<FIELDSET>
										<LEGEND>
											<asp:Label id="lbClientGateway" runat="server" CssClass="tableHeadingFieldset">Client Gateway</asp:Label></LEGEND>
										<TABLE style="maring: 5px" id="Table15" border="0" width="100%" align="left" runat="server">
											<TR height="5">
												<TD width="2%"></TD>
												<TD width="5%"></TD>
												<TD width="5%"></TD>
												<TD width="5%"></TD>
												<TD width="5%"></TD>
												<TD width="5%"></TD>
												<TD width="5%"></TD>
												<TD width="5%"></TD>
												<TD width="5%"></TD>
												<TD width="5%"></TD>
												<TD width="5%"></TD>
												<TD width="5%"></TD>
												<TD width="5%"></TD>
												<TD width="5%"></TD>
												<TD width="5%"></TD>
												<TD width="5%"></TD>
												<TD width="5%"></TD>
												<TD width="5%"></TD>
												<TD width="5%"></TD>
												<TD width="8%"></TD>
											</TR>
											<TR>
												<TD colSpan="5">
													<asp:label id="lbStartConnote" runat="server" CssClass="tableLabel">Start Consignment No.</asp:label>
												</TD>
												<TD colSpan="3 ">
													<CC1:MSTEXTBOX id="txtStartConnote" tabIndex="52" onpaste="AfterPasteNumberwhithDot(this)" runat="server"
														Width="100px" CssClass="textFieldRightAlign" TextMaskType="msUpperAlfaNumericWithUnderscore"
														MaxLength="30"></CC1:MSTEXTBOX>
												</TD>
												<TD colSpan="4">
													<asp:label id="Label48" runat="server" CssClass="tableLabel">Default Service Type</asp:label>
												</TD>
												<TD colSpan="2">
													<CC1:MSTEXTBOX id="txtServiceCode" tabIndex="52" onpaste="AfterPasteNumberwhithDot(this)" runat="server"
														Width="90px" CssClass="textFieldRightAlign" TextMaskType="msNumeric" NumberPrecision="19" NumberMinValue="0"
														NumberMaxValue="99999999"></CC1:MSTEXTBOX>
												</TD>
												<TD>
													<asp:button id="btService" tabIndex="64" runat="server" CssClass="queryButton" CausesValidation="False"
														Text="..."></asp:button>
												</TD>
											</TR>
										</TABLE>
									</FIELDSET>
								</ASP:PANEL>
							</TD>
						</TR>
					</TABLE>
				</IEWC:PAGEVIEW>
				<IEWC:PAGEVIEW id="SpecialInfoPage">
					<TABLE style="Z-INDEX: 112; WIDTH: 730px" id="Table1" border="0" width="730" runat="server">
						<TR width="100%">
							<TD width="100%">
								<FIELDSET><LEGEND>
										<asp:Label id="Label3" runat="server" CssClass="tableHeadingFieldset">Special Information</asp:Label></LEGEND>
									<TABLE id="tblSpecialInfo" border="0" width="100%" align="left" runat="server">
										<TR height="5">
											<TD width="2%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="8%"></TD>
										</TR>
										<TR height="25">
											<TD>&nbsp;</TD>
											<TD colSpan="5">
												<asp:label id="lblCustAccDispTab2" runat="server" Height="22px" Width="110px" CssClass="tableLabel">Customer Account</asp:label></TD>
											<TD colSpan="4">
												<asp:textbox id="txtCustAccDispTab2" runat="server" Width="100%" CssClass="textField" Enabled="False"></asp:textbox></TD>
											<TD align="right"></TD>
											<TD colSpan="3">
												<asp:label id="lblCustNameDispTab2" runat="server" Height="22px" Width="100%" CssClass="tableLabel">Customer Name</asp:label></TD>
											<TD colSpan="6">
												<asp:textbox id="txtCustNameDispTab2" runat="server" Width="100%" CssClass="textField" Enabled="False"></asp:textbox></TD>
										</TR>
										<TR height="25">
											<TD>&nbsp;</TD>
											<TD colSpan="5">
												<asp:label id="lblRemark" runat="server" Height="22px" Width="100%" CssClass="tableLabel">Remarks</asp:label></TD>
											<TD colSpan="14">
												<asp:textbox id="txtRemark" tabIndex="42" runat="server" Width="100%" CssClass="textField"></asp:textbox></TD>
										</TR>
										<TR height="25">
											<TD>&nbsp;</TD>
											<TD colSpan="5">
												<asp:label id="lblSpecialInstruction" runat="server" Height="22px" Width="100%" CssClass="tableLabel">Special Instruction</asp:label></TD>
											<TD colSpan="14">
												<asp:textbox id="txtSpecialInstruction" tabIndex="42" runat="server" Width="100%" CssClass="textField"></asp:textbox></TD>
										</TR>
										<TR height="10">
											<TD colSpan="20"></TD>
										</TR>
										<TR height="25">
											<TD>&nbsp;</TD>
											<TD colSpan="5">
												<asp:label id="lblISectorCode" runat="server" Height="22px" Width="100%" CssClass="tableLabel">Industrial Sector Code</asp:label></TD>
											<TD colSpan="3">
												<CC1:MSTEXTBOX id="txtISectorCode" tabIndex="31" runat="server" Width="100%" CssClass="textField"
													TextMaskType="msUpperAlfaNumericWithUnderscore" MaxLength="12"></CC1:MSTEXTBOX></TD>
											<TD>
												<asp:button id="btnISectorSearch" tabIndex="19" runat="server" Width="100%" CssClass="queryButton"
													CausesValidation="False" Text="..."></asp:button></TD>
											<TD align="right"></TD>
											<TD colSpan="5">
												<asp:label id="Label15" runat="server" Height="22px" Width="100%" CssClass="tableLabel">Density Factor</asp:label><%--<asp:label id="lblSalesmanID" runat="server" Width="100%" CssClass="tableLabel" Height="22px">Salesman ID</asp:label>--%></TD>
											<TD colSpan="3">
												<CC1:MSTEXTBOX id="msDensityFactor" tabIndex="35" runat="server" Width="100%" CssClass="textFieldRightAlign"
													TextMaskType="msNumeric" MaxLength="9" AutoPostBack="False" NumberScale="0" NumberPrecision="10"
													NumberMinValue="1" NumberMaxValue="19999"></CC1:MSTEXTBOX><%--<asp:textbox id="txtSalesmanID" runat="server" tabIndex="33" Width="100%" CssClass="textField" ReadOnly="True"></asp:textbox>--%></TD>
											<TD><%--<asp:button id="btnSearchSalesman" runat="server" tabIndex="51" Width="30px" CssClass="queryButton"
													Height="22px" CausesValidation="False" Text="..." OnClick="btnSearchSalesman_Click"></asp:button>--%></TD>
										</TR>
										<TR height="25">
											<TD>&nbsp;</TD>
											<TD colSpan="5">
												<asp:label id="lblSalesmanID" runat="server" Height="22px" Width="100%" CssClass="tableLabel">Salesman ID</asp:label></TD>
											<TD colSpan="3">
												<asp:textbox id="txtSalesmanID" tabIndex="33" runat="server" Width="100%" CssClass="textField"
													ReadOnly="True"></asp:textbox></TD>
											<TD>
												<asp:button id="btnSearchSalesman" tabIndex="51" onclick="btnSearchSalesman_Click" runat="server"
													Height="22px" Width="30px" CssClass="queryButton" CausesValidation="False" Text="..."></asp:button></TD>
											<TD align="right"></TD>
											<TD colSpan="5">
												<asp:label id="lblreqDiscountBand" runat="server" Height="22px" Width="100%" CssClass="tableLabel">Discount Band</asp:label></TD>
											<TD colSpan="3">
												<CC1:MSTEXTBOX id="msDiscountBand" tabIndex="35" runat="server" Width="100%" CssClass="textField"
													MaxLength="6"></CC1:MSTEXTBOX></TD>
											<TD></TD>
										</TR>
										<TR height="25">
											<TD>&nbsp;</TD>
											<%-- Master Account --%>
											<TD colSpan="5">
												<asp:label id="lblPromisedTotWt" runat="server" Height="22px" Width="100%" CssClass="tableLabel">Promised Total Wt</asp:label></TD>
											<TD colSpan="2">
												<CC1:MSTEXTBOX id="txtPromisedTotWt" tabIndex="34" runat="server" Width="100%" CssClass="textFieldRightAlign"
													TextMaskType="msNumeric" MaxLength="6" NumberScale="2" NumberPrecision="8" NumberMinValue="0"
													NumberMaxValue="999999"></CC1:MSTEXTBOX></TD>
											<TD colSpan="3">&nbsp;</TD>
											<TD colSpan="5"><%--<asp:label id="lblInsurancePercentSurcharge" runat="server" Width="100%" CssClass="tableLabel"
													Height="22px">Insurance Surcharge Percentage</asp:label>--%>
												<asp:label id="Label21" runat="server" Height="22px" Width="95px" CssClass="tableLabel">Master Account</asp:label></TD>
											<TD colSpan="2"><%--<cc1:mstextbox id="txtInsurancePercentSurcharge" runat="server" tabIndex="37" Width="100%" CssClass="textFieldRightAlign"
													AutoPostBack="False" MaxLength="6" TextMaskType="msNumeric" NumberMaxValue="100" NumberMinValue="0"
													NumberPrecision="6" NumberScale="2"></cc1:mstextbox>--%>
												<DBCOMBO:DBCOMBO id="DbComboMasterAccount" tabIndex="1" runat="server" Width="80px" AutoPostBack="True"
													Debug="false" TextBoxColumns="4" TextUpLevelSearchButton="F" ShowDbComboLink="False" ServerMethod="MasterAccountServerMethod"></DBCOMBO:DBCOMBO></TD>
											<TD colSpan="2">&nbsp;</TD>
										</TR>
										<TR height="25">
											<TD align="right"></TD>
											<TD colSpan="5">
												<asp:label id="lblPromisedPeriod" runat="server" Height="22px" Width="100%" CssClass="tableLabel">Promised Period</asp:label></TD>
											<TD colSpan="2">
												<CC1:MSTEXTBOX id="txtPromisedPeriod" tabIndex="35" runat="server" Width="100%" CssClass="textFieldRightAlign"
													TextMaskType="msNumeric" MaxLength="4" AutoPostBack="False" NumberScale="0" NumberPrecision="4"
													NumberMinValue="0" NumberMaxValue="9999"></CC1:MSTEXTBOX></TD>
											<TD colSpan="3">&nbsp;</TD>
											<TD colSpan="5">
												<asp:label id="lblDimByTOT" runat="server" Height="22px" Width="100%" CssClass="tableLabel">Dim by total</asp:label><%--<asp:label id="lblFreeInsuranceAmt" runat="server" Width="100%" CssClass="tableLabel" Height="22px">Free Coverage</asp:label>--%></TD>
											<TD colSpan="2">
												<ASP:DROPDOWNLIST id="ddlDimByTOT" tabIndex="29" runat="server" Width="100%" CssClass="textField"></ASP:DROPDOWNLIST><%--<cc1:mstextbox id="txtFreeInsuranceAmt" AutoPostBack="True" runat="server" tabIndex="38" Width="100%"
													CssClass="textFieldRightAlign" MaxLength="9" TextMaskType="msNumeric" NumberMaxValue="999999"
													NumberMinValue="0" NumberPrecision="8" NumberScale="2"></cc1:mstextbox>--%></TD>
											<TD colSpan="2">&nbsp;</TD>
										</TR>
										<%--  HC Return Task --%>
										<TR height="25">
											<TD align="right"></TD>
											<TD colSpan="5">
												<asp:label id="lblPromisedTotPkg" runat="server" Height="22px" Width="100%" CssClass="tableLabel">Promised Total Pkgs</asp:label></TD>
											<TD colSpan="2">
												<CC1:MSTEXTBOX id="txtPromisedTotPkg" tabIndex="36" runat="server" Width="100%" CssClass="textFieldRightAlign"
													TextMaskType="msNumeric" MaxLength="4" NumberScale="0" NumberPrecision="4" NumberMinValue="0"
													NumberMaxValue="9999"></CC1:MSTEXTBOX></TD>
											<TD colSpan="2">&nbsp;</TD>
											<%---Aoo---%>
											<TD>
												<ASP:REQUIREDFIELDVALIDATOR id="Requiredfieldvalidator1" Width="100%" Runat="server" ErrorMessage="First ship date"
													Display="None" ControlToValidate="txtFirstShipDate"></ASP:REQUIREDFIELDVALIDATOR>
												<asp:label id="Label26" runat="server" Height="22px" Width="100%" CssClass="errorMsgColor">&nbsp;*&nbsp;</asp:label></TD>
											<TD colSpan="5">
												<asp:label id="lblFirstShpDate" runat="server" Height="22px" Width="100%" CssClass="tableLabel">First ship date</asp:label><%--<asp:label id="Label14" runat="server" Width="100%" CssClass="tableLabel" Height="22px">Maximum Coverage</asp:label>--%></TD>
											<TD colSpan="2">
												<CC1:MSTEXTBOX id="txtFirstShipDate" tabIndex="32" runat="server" Width="100%" CssClass="textField"
													TextMaskType="msDate" MaxLength="12" AutoPostBack="true" TextMaskString="99/99/9999"></CC1:MSTEXTBOX><%--<cc1:mstextbox id="txtMaxAmt" runat="server" AutoPostBack="True" CssClass="textFieldRightAlign"
													Width="104px" TextMaskType="msNumeric" NumberMaxValue="99999999" NumberMinValue="0" NumberPrecision="19"
													NumberScale="2"></cc1:mstextbox>--%></TD>
											<TD colSpan="2">&nbsp;</TD>
											<%---Aoo---%>
											<%--<td colspan="1">
												<asp:requiredfieldvalidator id="validatePodSlipRequired" Width="100%" Runat="server" ControlToValidate="ddlPodSlipRequired" Display="None" ErrorMessage="HC Pod Required"></asp:requiredfieldvalidator>
												<asp:label id="lblreqPodSlipRequired" runat="server" Height="22px" Width="100%" CssClass="errorMsgColor">&nbsp;*&nbsp;</asp:label></td>
											<TD colSpan="5">
												<asp:label id="lblPodSlipRequired" runat="server" Width="100%" CssClass="tableLabel" Height="22px">HC Pod Required</asp:label>
												<asp:label id="Label7" runat="server" Width="100%" CssClass="tableLabel" Height="22px" Visible="False">Invoice Return Within(Days)</asp:label></TD>
											<TD colSpan="2">
												<asp:DropDownList id="ddlPodSlipRequired" runat="server" tabIndex="40" Width="100%" CssClass="textField"></asp:DropDownList>
												<cc1:mstextbox id="txtInvoiceReturn" runat="server" tabIndex="38" Width="0%" CssClass="textFieldRightAlign" MaxLength="2" TextMaskType="msNumeric" NumberMaxValue="20" NumberMinValue="0" NumberPrecision="8" NumberScale="2"></cc1:mstextbox></TD>
											<TD colSpan="12"></TD>--%>
										</TR>
										<TR height="25">
											<TD align="right">
												<ASP:REQUIREDFIELDVALIDATOR id="validateApplyDimWt" Width="100%" Runat="server" ErrorMessage="Apply Dim Wt."
													Display="None" ControlToValidate="ddlApplyDimWt"></ASP:REQUIREDFIELDVALIDATOR>
												<asp:label id="lblreqApplyDimWt" runat="server" Height="22px" Width="100%" CssClass="errorMsgColor">&nbsp;*&nbsp;</asp:label></TD>
											<TD colSpan="5">
												<asp:label id="lblApplyDimWt" runat="server" Height="22px" Width="100%" CssClass="tableLabel">Apply Dim Wt</asp:label></TD>
											<TD colSpan="2">
												<ASP:DROPDOWNLIST id="ddlApplyDimWt" tabIndex="39" runat="server" Width="100%" CssClass="textField"
													AutoPostBack="False"></ASP:DROPDOWNLIST></TD>
											<TD colSpan="3">&nbsp;</TD>
											<%---Aoo---%>
											<TD colSpan="5">
												<asp:label id="lblCategory" runat="server" Height="22px" Width="100%" CssClass="tableLabel">Category</asp:label><%--<asp:label id="lblreqMinInsuranceSurcharge" runat="server" Width="100%" CssClass="tableLabel"
													Height="22px">Minimum Insurance Surcharge</asp:label>--%></TD>
											<TD colSpan="2">
												<asp:TextBox id="txtCategory" Height="22px" Width="100%" Runat="server" Enabled="False"></asp:TextBox><%--<cc1:mstextbox id="txtMinInsuranceSurcharge" runat="server" tabIndex="35" Width="100%" CssClass="textFieldRightAlign"
													AutoPostBack="True" MaxLength="9" TextMaskType="msNumeric" NumberMaxValue="100000" NumberMinValue="0"
													NumberPrecision="9" NumberScale="9"></cc1:mstextbox>--%></TD>
											<TD colSpan="2">&nbsp;</TD>
											<%---Aoo---%>
											<%--<td colspan="1">
												<asp:requiredfieldvalidator id="validateInvoiceRequired" Width="100%" Runat="server" ControlToValidate="ddlInvoiceRequried" Display="None" ErrorMessage="HC Invoice Required"></asp:requiredfieldvalidator>
												<asp:label id="lblreqInvoiceRequired" runat="server" Height="22px" Width="100%" CssClass="errorMsgColor">&nbsp;*&nbsp;</asp:label></td>
											<TD colSpan="5">
												<asp:label id="lblInvoiceRequired" runat="server" Width="100%" CssClass="tableLabel" Height="22px">HC Invoice Required</asp:label></TD>
											<TD colSpan="2">
												<asp:DropDownList id="ddlInvoiceRequried" runat="server" tabIndex="40" Width="100%" CssClass="textField"></asp:DropDownList></TD>
											<TD colSpan="2">&nbsp;</TD>--%>
										</TR>
										<%-- Customer Type --%>
										<TR height="25">
											<TD align="right">
												<asp:label id="Label11" runat="server" Width="100%" CssClass="errorMsgColor">&nbsp;*&nbsp;</asp:label>
												<ASP:REQUIREDFIELDVALIDATOR id="validateApplyCustomerType" Width="100%" Runat="server" ErrorMessage="Customer Type"
													Display="None" ControlToValidate="ddlCustomerType"></ASP:REQUIREDFIELDVALIDATOR></TD>
											<TD colSpan="5">
												<asp:label id="Label17" runat="server" Width="100%" CssClass="tableLabel">Customer Type</asp:label></TD>
											<TD colSpan="2">
												<ASP:DROPDOWNLIST id="ddlCustomerType" tabIndex="41" runat="server" Width="100%" CssClass="textField"
													AutoPostBack="True"></ASP:DROPDOWNLIST></TD>
											<TD colSpan="3">&nbsp;</TD>
											<%---Aoo---%>
											<TD colSpan="5">
												<asp:label id="Label47" runat="server" Height="22px" Width="100%" CssClass="tableLabel">Relaled Accounts</asp:label><%--<asp:label id="lblInsurancePercentSurcharge" runat="server" Width="100%" CssClass="tableLabel"
													Height="22px">Insurance Surcharge Percentage</asp:label>--%></TD>
											<TD colSpan="2">
												<asp:TextBox id="txtRelaledAccounts" Height="22px" Width="100%" Enabled="false" Runat="server"></asp:TextBox><%--<cc1:mstextbox id="txtInsurancePercentSurcharge" runat="server" tabIndex="37" Width="100%" CssClass="textFieldRightAlign"
													AutoPostBack="False" MaxLength="6" TextMaskType="msNumeric" NumberMaxValue="100" NumberMinValue="0"
													NumberPrecision="6" NumberScale="2"></cc1:mstextbox>--%></TD>
											<TD colSpan="2"></TD>
										</TR>
										<%-- ESA Surcharges --%>
										<TR height="25">
											<TD colSpan="10">
												<ASP:PANEL id="pnESASurcharges" runat="server">
													<FIELDSET><LEGEND>
															<asp:Label id="Label42" runat="server" CssClass="tableHeadingFieldset">ESA Surcharges</asp:Label></LEGEND></LEGEND>
														<TABLE id="Table11" border="0" width="100%" align="left" runat="server">
															<TR height="5">
																<TD width="2%"></TD>
																<TD width="5%"></TD>
																<TD width="5%"></TD>
																<TD width="5%"></TD>
																<TD width="5%"></TD>
																<TD width="5%"></TD>
																<TD width="5%"></TD>
																<TD width="5%"></TD>
																<TD width="5%"></TD>
																<TD width="5%"></TD>
																<TD width="5%"></TD>
																<TD width="5%"></TD>
																<TD width="5%"></TD>
																<TD width="5%"></TD>
																<TD width="5%"></TD>
																<TD width="5%"></TD>
																<TD width="5%"></TD>
																<TD width="5%"></TD>
																<TD width="5%"></TD>
																<TD width="8%"></TD>
															</TR>
															<TR>
																<TD>
																	<ASP:REQUIREDFIELDVALIDATOR id="validateApplyEsaSurcharge" Width="100%" Runat="server" ErrorMessage="Apply ESA Surcharge"
																		Display="None" ControlToValidate="ddlApplyEsaSurcharge"></ASP:REQUIREDFIELDVALIDATOR>
																	<asp:label id="lblreqApplyEsaSurcharge" runat="server" Height="22px" Width="100%" CssClass="errorMsgColor">&nbsp;*&nbsp;</asp:label></TD>
																<TD colSpan="15">
																	<asp:label id="lblApplyEsaSurcharge" runat="server" Height="22px" Width="100%" CssClass="tableLabel">Apply ESA Surcharge For Pickup</asp:label></TD>
																<TD colSpan="4">
																	<ASP:DROPDOWNLIST id="ddlApplyEsaSurcharge" tabIndex="41" runat="server" Width="100%" CssClass="textField"
																		AutoPostBack="False"></ASP:DROPDOWNLIST></TD>
															</TR>
															<TR>
																<TD>
																	<ASP:REQUIREDFIELDVALIDATOR id="validateApplyEsaSurchargeDel" Width="100%" Runat="server" ErrorMessage="Apply ESA Surcharge For Delivery"
																		Display="None" ControlToValidate="ddlApplyEsaSurchargeDel"></ASP:REQUIREDFIELDVALIDATOR>
																	<asp:label id="lblreqApplyEsaSurchargeDel" runat="server" Height="22px" Width="100%" CssClass="errorMsgColor">&nbsp;*&nbsp;</asp:label></TD>
																<TD colSpan="15">
																	<asp:label id="lblApplyEsaSurchargeDel" runat="server" Height="22px" Width="100%" CssClass="tableLabel">Apply ESA Surcharge For Delivery</asp:label></TD>
																<TD colSpan="4">
																	<ASP:DROPDOWNLIST id="ddlApplyEsaSurchargeDel" tabIndex="41" runat="server" Width="100%" CssClass="textField"
																		AutoPostBack="False"></ASP:DROPDOWNLIST></TD>
															</TR>
														</TABLE>
													</FIELDSET>
												</ASP:PANEL></TD>
											<%-- Hard Copy Return --%>
											<TD colSpan="9">
												<ASP:PANEL id="pnHardCopyReturn" runat="server">
													<FIELDSET><LEGEND>
															<asp:Label id="Label44" runat="server" CssClass="tableHeadingFieldset">Hard Copy Return</asp:Label></LEGEND></LEGEND>
														<TABLE id="Table12" border="0" width="100%" align="left" runat="server">
															<TR height="5">
																<TD width="2%"></TD>
																<TD width="5%"></TD>
																<TD width="5%"></TD>
																<TD width="5%"></TD>
																<TD width="5%"></TD>
																<TD width="5%"></TD>
																<TD width="5%"></TD>
																<TD width="5%"></TD>
																<TD width="5%"></TD>
																<TD width="5%"></TD>
																<TD width="5%"></TD>
																<TD width="5%"></TD>
																<TD width="5%"></TD>
																<TD width="5%"></TD>
																<TD width="5%"></TD>
																<TD width="5%"></TD>
																<TD width="5%"></TD>
																<TD width="5%"></TD>
																<TD width="5%"></TD>
																<TD width="8%"></TD>
															</TR>
															<TR>
																<TD>
																	<ASP:REQUIREDFIELDVALIDATOR id="validatePodSlipRequired" Width="100%" Runat="server" ErrorMessage="HC Pod Required"
																		Display="None" ControlToValidate="ddlPodSlipRequired"></ASP:REQUIREDFIELDVALIDATOR>
																	<asp:label id="lblreqPodSlipRequired" runat="server" Height="22px" Width="100%" CssClass="errorMsgColor">&nbsp;*&nbsp;</asp:label></TD>
																<TD colSpan="15">
																	<asp:label id="lblPodSlipRequired" runat="server" Height="22px" Width="100%" CssClass="tableLabel">HC Pod Required</asp:label>
																	<asp:label id="Label7" runat="server" Height="22px" Width="100%" CssClass="tableLabel" Visible="False">Invoice Return Within(Days)</asp:label></TD>
																<TD colSpan="4">
																	<ASP:DROPDOWNLIST id="ddlPodSlipRequired" tabIndex="40" runat="server" Width="100%" CssClass="textField"></ASP:DROPDOWNLIST></TD>
																<TD>
																	<CC1:MSTEXTBOX id="txtInvoiceReturn" tabIndex="38" runat="server" Width="0%" CssClass="textFieldRightAlign"
																		TextMaskType="msNumeric" MaxLength="2" NumberScale="2" NumberPrecision="8" NumberMinValue="0"
																		NumberMaxValue="20"></CC1:MSTEXTBOX></TD>
															</TR>
															<TR>
																<TD>
																	<ASP:REQUIREDFIELDVALIDATOR id="validateInvoiceRequired" Width="100%" Runat="server" ErrorMessage="HC Invoice Required"
																		Display="None" ControlToValidate="ddlInvoiceRequried"></ASP:REQUIREDFIELDVALIDATOR>
																	<asp:label id="lblreqInvoiceRequired" runat="server" Height="22px" Width="100%" CssClass="errorMsgColor">&nbsp;*&nbsp;</asp:label></TD>
																<TD colSpan="15">
																	<asp:label id="lblInvoiceRequired" runat="server" Height="22px" Width="100%" CssClass="tableLabel">HC Invoice Required</asp:label></TD>
																<TD colSpan="4">
																	<ASP:DROPDOWNLIST id="ddlInvoiceRequried" tabIndex="40" runat="server" Width="100%" CssClass="textField"></ASP:DROPDOWNLIST></TD>
															</TR>
														</TABLE>
													</FIELDSET>
												</ASP:PANEL></TD>
											<%--<TD colSpan="1">
											</TD>
											<TD colSpan="1">
											</TD>
											<TD colSpan="1">&nbsp;</TD>--%>
											<%---Aoo---%>
											<%--<TD colSpan="1">
											</TD>
											<TD colSpan="1">
											</TD>
											<TD colSpan="1">&nbsp;</TD>--%>
										</TR>
										<%-- Insuranc Surcharges --%>
										<TR height="25">
											<TD colSpan="10">
												<ASP:PANEL id="pnInsuranceSurcharges" runat="server">
													<FIELDSET><LEGEND>
															<asp:Label id="Label45" runat="server" CssClass="tableHeadingFieldset">Insurance Surcharges</asp:Label></LEGEND></LEGEND>
														<TABLE id="Table13" border="0" width="100%" align="left" runat="server">
															<TR height="5">
																<TD width="2%"></TD>
																<TD width="5%"></TD>
																<TD width="5%"></TD>
																<TD width="5%"></TD>
																<TD width="5%"></TD>
																<TD width="5%"></TD>
																<TD width="5%"></TD>
																<TD width="5%"></TD>
																<TD width="5%"></TD>
																<TD width="5%"></TD>
																<TD width="5%"></TD>
																<TD width="5%"></TD>
																<TD width="5%"></TD>
																<TD width="5%"></TD>
																<TD width="5%"></TD>
																<TD width="5%"></TD>
																<TD width="5%"></TD>
																<TD width="5%"></TD>
																<TD width="5%"></TD>
																<TD width="8%"></TD>
															</TR>
															<TR>
																<TD colSpan="15">
																	<asp:label id="lblInsurancePercentSurcharge" runat="server" Height="22px" Width="100%" CssClass="tableLabel">Insurance Surcharge Percentage</asp:label></TD>
																<TD colSpan="5">
																	<CC1:MSTEXTBOX id="txtInsurancePercentSurcharge" tabIndex="37" runat="server" Width="100%" CssClass="textFieldRightAlign"
																		TextMaskType="msNumeric" MaxLength="6" AutoPostBack="False" NumberScale="2" NumberPrecision="6"
																		NumberMinValue="0" NumberMaxValue="100"></CC1:MSTEXTBOX></TD>
															</TR>
															<TR>
																<TD colSpan="15">
																	<asp:label id="lblFreeInsuranceAmt" runat="server" Height="22px" Width="100%" CssClass="tableLabel">Free Coverage</asp:label></TD>
																<TD colSpan="5">
																	<CC1:MSTEXTBOX id="txtFreeInsuranceAmt" tabIndex="38" runat="server" Width="100%" CssClass="textFieldRightAlign"
																		TextMaskType="msNumeric" MaxLength="9" AutoPostBack="True" NumberScale="2" NumberPrecision="8"
																		NumberMinValue="0" NumberMaxValue="999999"></CC1:MSTEXTBOX></TD>
															</TR>
															<TR>
																<TD colSpan="15"><%--<asp:label id="lblMaxCODSurcharge" runat="server" Width="100%" CssClass="tableLabel" Height="22px">Maximum COD Surcharge</asp:label>--%></TD>
																<TD colSpan="5"><%--<cc1:mstextbox id="txtMaxCODSurcharge" runat="server" tabIndex="35" Width="100%" CssClass="textFieldRightAlign"
																		AutoPostBack="True" MaxLength="9" TextMaskType="msNumeric" NumberMaxValue="100000" NumberMinValue="1"
																		NumberPrecision="9" NumberScale="9"></cc1:mstextbox>--%></TD>
															</TR>
															<TR>
																<TD colSpan="15">
																	<asp:label id="lblreqMinInsuranceSurcharge" runat="server" Height="22px" Width="100%" CssClass="tableLabel">Minimum Insurance Surcharge</asp:label><%--<asp:label id="lblreqDiscountBand" runat="server" Width="100%" CssClass="tableLabel" Height="22px">Discount Band</asp:label>--%></TD>
																<TD colSpan="5">
																	<CC1:MSTEXTBOX id="txtMinInsuranceSurcharge" tabIndex="35" runat="server" Width="100%" CssClass="textFieldRightAlign"
																		TextMaskType="msNumeric" MaxLength="9" AutoPostBack="True" NumberScale="9" NumberPrecision="9"
																		NumberMinValue="0" NumberMaxValue="100000"></CC1:MSTEXTBOX><%--<cc1:mstextbox id="msDiscountBand" runat="server" tabIndex="35" Width="100%" CssClass="textField"
																		MaxLength="6"></cc1:mstextbox>--%></TD>
															</TR>
															<TR>
																<TD colSpan="15">
																	<asp:label id="lblregMaxInsuranceSurcharge" runat="server" Height="22px" Width="100%" CssClass="tableLabel">Maximum Insurance Surcharge</asp:label></TD>
																<TD colSpan="5">
																	<CC1:MSTEXTBOX id="txtMaxInsuranceSurcharge" tabIndex="35" runat="server" Width="100%" CssClass="textFieldRightAlign"
																		TextMaskType="msNumeric" MaxLength="9" AutoPostBack="True" NumberScale="9" NumberPrecision="9"
																		NumberMinValue="0" NumberMaxValue="100000"></CC1:MSTEXTBOX></TD>
															</TR>
														</TABLE>
													</FIELDSET>
												</ASP:PANEL></TD>
											<TD colSpan="9">
												<ASP:PANEL id="pnCODSurcharges" runat="server">
													<FIELDSET><LEGEND>
															<asp:Label id="Label46" runat="server" CssClass="tableHeadingFieldset">COD Surcharges</asp:Label></LEGEND></LEGEND>
														<TABLE id="Table14" border="0" width="100%" align="left" runat="server">
															<TR height="5">
																<TD width="2%"></TD>
																<TD width="5%"></TD>
																<TD width="5%"></TD>
																<TD width="5%"></TD>
																<TD width="5%"></TD>
																<TD width="5%"></TD>
																<TD width="5%"></TD>
																<TD width="5%"></TD>
																<TD width="5%"></TD>
																<TD width="5%"></TD>
																<TD width="5%"></TD>
																<TD width="5%"></TD>
																<TD width="5%"></TD>
																<TD width="5%"></TD>
																<TD width="5%"></TD>
																<TD width="5%"></TD>
																<TD width="5%"></TD>
																<TD width="5%"></TD>
																<TD width="5%"></TD>
																<TD width="8%"></TD>
															</TR>
															<TR>
																<TD colSpan="15">
																	<asp:label id="Label13" runat="server" Height="22px" Width="100%" CssClass="tableLabel">COD Surcharge Amount</asp:label></TD>
																<TD colSpan="5">
																	<CC1:MSTEXTBOX id="txtCODSurchargeAmt" tabIndex="35" runat="server" Width="100%" CssClass="textFieldRightAlign"
																		TextMaskType="msNumeric" MaxLength="9" AutoPostBack="True" NumberScale="9" NumberPrecision="9"
																		NumberMinValue="0" NumberMaxValue="100000"></CC1:MSTEXTBOX></TD>
															</TR>
															<TR>
																<TD colSpan="15">
																	<asp:label id="Label16" runat="server" Height="22px" Width="100%" CssClass="tableLabel">COD Surcharge Percentage</asp:label></TD>
																<TD colSpan="5">
																	<CC1:MSTEXTBOX id="txtCODSurchargePercent" tabIndex="35" runat="server" Width="100%" CssClass="textFieldRightAlign"
																		TextMaskType="msNumeric" MaxLength="6" AutoPostBack="False" NumberScale="2" NumberPrecision="6"
																		NumberMinValue="0" NumberMaxValue="100"></CC1:MSTEXTBOX></TD>
															</TR>
															<TR>
																<TD colSpan="15">
																	<asp:label id="lblMaxCODSurcharge" runat="server" Height="22px" Width="100%" CssClass="tableLabel">Maximum COD Surcharge</asp:label></TD>
																<TD colSpan="5">
																	<CC1:MSTEXTBOX id="txtMaxCODSurcharge" tabIndex="35" runat="server" Width="100%" CssClass="textFieldRightAlign"
																		TextMaskType="msNumeric" MaxLength="9" AutoPostBack="True" NumberScale="9" NumberPrecision="9"
																		NumberMinValue="1" NumberMaxValue="100000"></CC1:MSTEXTBOX></TD>
															</TR>
															<TR>
																<TD colSpan="15">
																	<asp:label id="lblMinCODSurcharge" runat="server" Height="22px" Width="100%" CssClass="tableLabel">Minimum COD Surcharge</asp:label></TD>
																<TD colSpan="5">
																	<CC1:MSTEXTBOX id="txtMinCODSurcharge" tabIndex="35" runat="server" Width="100%" CssClass="textFieldRightAlign"
																		TextMaskType="msNumeric" MaxLength="9" AutoPostBack="True" NumberScale="9" NumberPrecision="9"
																		NumberMinValue="1" NumberMaxValue="100000"></CC1:MSTEXTBOX></TD>
															</TR>
														</TABLE>
													</FIELDSET>
												</ASP:PANEL></TD>
										</TR>
										<%-- HC Return Task --%>
										<TR height="25">
											<TD align="right"><%--<asp:requiredfieldvalidator id="validateApplyEsaSurcharge" Width="100%" Runat="server" ControlToValidate="ddlApplyEsaSurcharge"
													Display="None" ErrorMessage="Apply ESA Surcharge"></asp:requiredfieldvalidator>
												<asp:label id="lblreqApplyEsaSurcharge" runat="server" Height="22px" Width="100%" CssClass="errorMsgColor">&nbsp;*&nbsp;</asp:label>--%></TD>
											<TD colSpan="5"><%--<asp:label id="lblApplyEsaSurcharge" runat="server" Width="100%" CssClass="tableLabel" Height="22px">Apply ESA Surcharge For Pickup</asp:label>--%></TD>
											<TD colSpan="2"><%--<asp:DropDownList id="ddlApplyEsaSurcharge" runat="server" tabIndex="41" Width="100%" CssClass="textField"
													AutoPostBack="False"></asp:DropDownList>--%></TD>
											<TD colSpan="3">&nbsp;</TD>
											<%---Aoo---%>
											<TD colSpan="5"><%--<asp:label id="lblregMaxInsuranceSurcharge" runat="server" Width="100%" CssClass="tableLabel"
													Height="22px">Maximum Insurance Surcharge</asp:label>--%></TD>
											<TD colSpan="2"><%--<cc1:mstextbox id="txtMaxInsuranceSurcharge" runat="server" tabIndex="35" Width="100%" CssClass="textFieldRightAlign"
													AutoPostBack="True" MaxLength="9" TextMaskType="msNumeric" NumberMaxValue="100000" NumberMinValue="0"
													NumberPrecision="9" NumberScale="9"></cc1:mstextbox>--%></TD>
											<TD colSpan="2">&nbsp;</TD>
											<%---Aoo---%>
											<%--<td colspan="1">&nbsp;</td>
											<TD colSpan="5">
												<asp:label id="Label14" runat="server" Width="100%" CssClass="tableLabel" Height="22px">Maximum Coverage</asp:label></TD>
											<TD colSpan="2">
												<cc1:mstextbox id="txtMaxAmt" runat="server" CssClass="textFieldRightAlign" Width="104px" TextMaskType="msNumeric" NumberMaxValue="99999999" NumberMinValue="0" NumberPrecision="19" NumberScale="2"></cc1:mstextbox></TD>
											<TD colSpan="2">&nbsp;</TD>--%>
										</TR>
										<TR height="25">
											<TD align="right"><%--<asp:requiredfieldvalidator id="validateApplyEsaSurchargeDel" Width="100%" Runat="server" ControlToValidate="ddlApplyEsaSurchargeDel"
													Display="None" ErrorMessage="Apply ESA Surcharge For Delivery"></asp:requiredfieldvalidator>
												<asp:label id="lblreqApplyEsaSurchargeDel" runat="server" Height="22px" Width="100%" CssClass="errorMsgColor">&nbsp;*&nbsp;</asp:label>--%></TD>
											<TD colSpan="5"><%--<asp:label id="lblApplyEsaSurchargeDel" runat="server" Width="100%" CssClass="tableLabel" Height="22px">Apply ESA Surcharge For Delivery</asp:label>--%></TD>
											<TD colSpan="2"><%--<asp:DropDownList id="ddlApplyEsaSurchargeDel" runat="server" tabIndex="41" Width="100%" CssClass="textField"
													AutoPostBack="False"></asp:DropDownList>--%></TD>
											<TD colSpan="2">&nbsp;</TD>
											<%--Aoo--%>
											<TD><%--<asp:requiredfieldvalidator id="validatePodSlipRequired" Width="100%" Runat="server" ControlToValidate="ddlPodSlipRequired"
													Display="None" ErrorMessage="HC Pod Required"></asp:requiredfieldvalidator>
												<asp:label id="lblreqPodSlipRequired" runat="server" Height="22px" Width="100%" CssClass="errorMsgColor">&nbsp;*&nbsp;</asp:label>--%></TD>
											<TD colSpan="5"><%--<asp:label id="lblPodSlipRequired" runat="server" Width="100%" CssClass="tableLabel" Height="22px">HC Pod Required</asp:label>
												<asp:label id="Label7" runat="server" Width="100%" CssClass="tableLabel" Height="22px" Visible="False">Invoice Return Within(Days)</asp:label>--%></TD>
											<TD colSpan="2"><%--<asp:DropDownList id="ddlPodSlipRequired" runat="server" tabIndex="40" Width="100%" CssClass="textField"></asp:DropDownList>
												<cc1:mstextbox id="txtInvoiceReturn" runat="server" tabIndex="38" Width="0%" CssClass="textFieldRightAlign"
													MaxLength="2" TextMaskType="msNumeric" NumberMaxValue="20" NumberMinValue="0" NumberPrecision="8"
													NumberScale="2"></cc1:mstextbox>--%></TD>
											<TD colSpan="12"></TD>
											<%--<td colspan="1">&nbsp;</td>
											<TD colSpan="5">
												<asp:label id="Label15" runat="server" Width="100%" CssClass="tableLabel" Height="22px">Density Factor</asp:label></TD>
											<TD colSpan="2">
												<asp:DropDownList id="ddlDensityFactor" runat="server" tabIndex="41" Width="100%" CssClass="textField" AutoPostBack="False"></asp:DropDownList></TD>
											<TD colSpan="2">&nbsp;</TD>--%>
										</TR>
										<TR height="25">
											<TD align="right"></TD>
											<TD colSpan="5"><%--<asp:label id="Label13" runat="server" Width="100%" CssClass="tableLabel" Height="22px">COD Surcharge Amount</asp:label>--%></TD>
											<TD colSpan="2"><%--<cc1:mstextbox id="txtCODSurchargeAmt" runat="server" tabIndex="35" Width="100%" CssClass="textFieldRightAlign"
													AutoPostBack="True" MaxLength="9" TextMaskType="msNumeric" NumberMaxValue="100000" NumberMinValue="0"
													NumberPrecision="9" NumberScale="9"></cc1:mstextbox>--%></TD>
											<TD colSpan="2">&nbsp;</TD>
											<%--Aoo--%>
											<TD><%--<asp:requiredfieldvalidator id="validateInvoiceRequired" Width="100%" Runat="server" ControlToValidate="ddlInvoiceRequried"
													Display="None" ErrorMessage="HC Invoice Required"></asp:requiredfieldvalidator>
												<asp:label id="lblreqInvoiceRequired" runat="server" Height="22px" Width="100%" CssClass="errorMsgColor">&nbsp;*&nbsp;</asp:label>--%></TD>
											<TD colSpan="5"><%--<asp:label id="lblInvoiceRequired" runat="server" Width="100%" CssClass="tableLabel" Height="22px">HC Invoice Required</asp:label>--%></TD>
											<TD colSpan="2"><%--<asp:DropDownList id="ddlInvoiceRequried" runat="server" tabIndex="40" Width="100%" CssClass="textField"></asp:DropDownList>--%></TD>
											<TD colSpan="2">&nbsp;</TD>
											<%--<td colspan="1">
												<asp:label id="Label11" runat="server" Height="22px" Width="100%" CssClass="errorMsgColor">&nbsp;*&nbsp;</asp:label>
												<asp:requiredfieldvalidator id="validateApplyCustomerType" Width="100%" Runat="server" ControlToValidate="ddlCustomerType" Display="None" ErrorMessage="Customer Type"></asp:requiredfieldvalidator></td>
											<TD colSpan="5">
												<asp:label id="Label17" runat="server" Width="100%" CssClass="tableLabel" Height="22px">Customer Type</asp:label></TD>
											<TD colSpan="2">
												<asp:DropDownList id="ddlCustomerType" runat="server" tabIndex="41" Width="100%" CssClass="textField" AutoPostBack="False"></asp:DropDownList></TD>
											<TD colSpan="2">&nbsp;</TD>--%>
										</TR>
										<TR height="25">
											<TD align="right"></TD>
											<TD colSpan="5"><%--<asp:label id="Label16" runat="server" Width="100%" CssClass="tableLabel" Height="22px">COD Surcharge Percentage</asp:label>--%></TD>
											<TD colSpan="2"><%--<cc1:mstextbox id="txtCODSurchargePercent" runat="server" tabIndex="35" Width="100%" CssClass="textFieldRightAlign"
													AutoPostBack="False" MaxLength="6" TextMaskType="msNumeric" NumberMaxValue="100" NumberMinValue="0"
													NumberPrecision="6" NumberScale="2"></cc1:mstextbox>--%></TD>
											<TD colSpan="2">&nbsp;</TD>
											<%--Aoo--%>
											<TD>&nbsp;</TD>
											<TD colSpan="5"><%--<asp:label id="lblFreeInsuranceAmt" runat="server" Width="100%" CssClass="tableLabel" Height="22px">Free Coverage</asp:label>--%><%--<asp:label id="Label15" runat="server" Width="100%" CssClass="tableLabel" Height="22px">Density Factor</asp:label>--%></TD>
											<TD colSpan="2"><%--<cc1:mstextbox id="txtFreeInsuranceAmt" AutoPostBack="True" runat="server" tabIndex="38" Width="100%"
													CssClass="textFieldRightAlign" MaxLength="9" TextMaskType="msNumeric" NumberMaxValue="999999"
													NumberMinValue="0" NumberPrecision="8" NumberScale="2"></cc1:mstextbox>--%><%--<asp:DropDownList id="ddlDensityFactor" runat="server" tabIndex="41" Width="100%" CssClass="textField" AutoPostBack="False"></asp:DropDownList>--%><%--<cc1:mstextbox id="msDensityFactor" runat="server" tabIndex="35" Width="100%" CssClass="textFieldRightAlign"
													AutoPostBack="False" MaxLength="9" TextMaskType="msNumeric" NumberMaxValue="19999" NumberMinValue="1"
													NumberPrecision="10" NumberScale="0"></cc1:mstextbox>--%></TD>
											<TD colSpan="2">&nbsp;</TD>
											<%--<TD colSpan="5">
											</TD>
											<TD colSpan="2">
											</TD>
											<TD colSpan="2">&nbsp;</TD>--%>
										</TR> <!--���� interface ��ǹ Maximum COD Surcharge �Ѻ Minimum COD Surcharge-->
										<TR height="25">
											<TD align="right"></TD>
											<TD colSpan="5"><%--<asp:label id="lblMaxCODSurcharge" runat="server" Width="100%" CssClass="tableLabel" Height="22px">Maximum COD Surcharge</asp:label>--%></TD>
											<TD colSpan="2"><%--<cc1:mstextbox id="txtMaxCODSurcharge" runat="server" tabIndex="35" Width="100%" CssClass="textFieldRightAlign"
													AutoPostBack="True" MaxLength="9" TextMaskType="msNumeric" NumberMaxValue="100000" NumberMinValue="1"
													NumberPrecision="9" NumberScale="9"></cc1:mstextbox>--%></TD>
											<TD colSpan="2">&nbsp;</TD>
											<%--Aoo--%>
											<TD><%--<asp:label id="Label11" runat="server" Height="22px" Width="100%" CssClass="errorMsgColor">&nbsp;*&nbsp;</asp:label>
												<asp:requiredfieldvalidator id="validateApplyCustomerType" Width="100%" Runat="server" ControlToValidate="ddlCustomerType"
													Display="None" ErrorMessage="Customer Type"></asp:requiredfieldvalidator>--%></TD>
											<TD colSpan="5">
												<asp:label id="Label14" runat="server" Height="22px" Width="100%" CssClass="tableLabel" Visible="False">Maximum Coverage</asp:label><%--<asp:label id="Label17" runat="server" Width="100%" CssClass="tableLabel" Height="22px">Customer Type</asp:label>--%></TD>
											<TD colSpan="2">
												<CC1:MSTEXTBOX id="txtMaxAmt" runat="server" Width="104px" CssClass="textFieldRightAlign" Visible="False"
													TextMaskType="msNumeric" AutoPostBack="True" NumberScale="2" NumberPrecision="19" NumberMinValue="0"
													NumberMaxValue="99999999"></CC1:MSTEXTBOX><%--<asp:DropDownList id="ddlCustomerType" runat="server" tabIndex="41" Width="100%" CssClass="textField"
													AutoPostBack="False"></asp:DropDownList>--%></TD>
											<TD colSpan="2">&nbsp;</TD>
											<%--<TD colSpan="5">
											</TD>
											<TD colSpan="2">
											</TD>
											<TD colSpan="2">&nbsp;</TD>--%>
										</TR>
										<TR height="25">
											<TD align="right"></TD>
											<TD colSpan="5"><%--<asp:label id="lblMinCODSurcharge" runat="server" Width="100%" CssClass="tableLabel" Height="22px">Minimum COD Surcharge</asp:label>--%></TD>
											<TD colSpan="2"><%--<cc1:mstextbox id="txtMinCODSurcharge" runat="server" tabIndex="35" Width="100%" CssClass="textFieldRightAlign"
													AutoPostBack="True" MaxLength="9" TextMaskType="msNumeric" NumberMaxValue="100000" NumberMinValue="1"
													NumberPrecision="9" NumberScale="9"></cc1:mstextbox>--%></TD>
											<TD colSpan="2">&nbsp;</TD>
											<TD>&nbsp;</TD>
											<%--Aoo--%>
											<TD colSpan="5"><%--<asp:label id="lblreqMinInsuranceSurcharge" runat="server" Width="100%" CssClass="tableLabel"
													Height="22px">Minimum Insurance Surcharge</asp:label>--%><%--<asp:label id="lblreqDiscountBand" runat="server" Width="100%" CssClass="tableLabel" Height="22px">Discount Band</asp:label>--%></TD>
											<TD colSpan="2"><%--<cc1:mstextbox id="txtMinInsuranceSurcharge" runat="server" tabIndex="35" Width="100%" CssClass="textFieldRightAlign"
													AutoPostBack="True" MaxLength="9" TextMaskType="msNumeric" NumberMaxValue="100000" NumberMinValue="0"
													NumberPrecision="9" NumberScale="9"></cc1:mstextbox>--%><%--<cc1:mstextbox id="msDiscountBand" runat="server" tabIndex="35" Width="100%" CssClass="textField"
													MaxLength="6"></cc1:mstextbox>--%></TD>
											<TD colSpan="2">&nbsp;</TD>
										</TR>
										<%--Other Surcharge--%>
										<TR height="25">
											<TD align="right"></TD>
											<TD colSpan="5">
												<asp:label id="lblreqOtherSurchargeAmount" runat="server" Height="22px" Width="100%" CssClass="tableLabel"
													Visible="False">Other Surcharge Amount</asp:label></TD>
											<TD colSpan="2">
												<CC1:MSTEXTBOX id="msOtherSurchargeAmount" tabIndex="35" runat="server" Width="100%" CssClass="textFieldRightAlign"
													Visible="False" TextMaskType="msNumeric" MaxLength="9" AutoPostBack="True" NumberScale="9"
													NumberPrecision="9" NumberMinValue="0" NumberMaxValue="100000"></CC1:MSTEXTBOX></TD>
											<TD colSpan="2">&nbsp;</TD>
											<TD>&nbsp;</TD>
											<TD colSpan="5"><%--<asp:label id="Label21" runat="server" Width="95px" CssClass="tableLabel" Height="22px">Master Account</asp:label>--%></TD>
											<TD colSpan="2"><%--<dbcombo:dbcombo id="DbComboMasterAccount" tabIndex="1" runat="server" Width="80px" AutoPostBack="True"
													ServerMethod="MasterAccountServerMethod" ShowDbComboLink="False" TextUpLevelSearchButton="F"
													TextBoxColumns="4" Debug="false"></dbcombo:dbcombo>--%></TD>
											<TD colSpan="2">&nbsp;</TD>
										</TR>
										<TR height="25">
											<TD align="right"></TD>
											<TD colSpan="5">
												<asp:label id="lblreqOtherSurchargePercentage" runat="server" Height="22px" Width="100%" CssClass="tableLabel"
													Visible="False">Other Surcharge Percentage</asp:label></TD>
											<TD colSpan="2">
												<CC1:MSTEXTBOX id="msOtherSurchargePercentage" tabIndex="35" runat="server" Width="100%" CssClass="textFieldRightAlign"
													Visible="False" TextMaskType="msNumeric" MaxLength="9" AutoPostBack="True" NumberScale="2"
													NumberPrecision="6" NumberMinValue="0" NumberMaxValue="100"></CC1:MSTEXTBOX></TD>
											<TD colSpan="2">&nbsp;</TD>
											<TD>&nbsp;</TD>
											<TD colSpan="5"><%--<asp:label id="lblDimByTOT" runat="server" Width="100%" CssClass="tableLabel" Height="22px">Dim by total</asp:label>--%></TD>
											<TD colSpan="2"><%--<asp:DropDownList id="ddlDimByTOT" runat="server" tabIndex="29" Width="100%" CssClass="textField"></asp:DropDownList>--%></TD>
											<TD colSpan="2">&nbsp;</TD>
										</TR>
										<TR height="25">
											<TD align="right"></TD>
											<TD colSpan="5">
												<asp:label id="lblreqOtherSurchargeMinimum" runat="server" Height="22px" Width="100%" CssClass="tableLabel"
													Visible="False">Other Surcharge Minimum</asp:label></TD>
											<TD colSpan="2">
												<CC1:MSTEXTBOX id="msOtherSurchargeMinimum" tabIndex="35" runat="server" Width="100%" CssClass="textFieldRightAlign"
													Visible="False" TextMaskType="msNumeric" MaxLength="9" AutoPostBack="True" NumberScale="9"
													NumberPrecision="9" NumberMinValue="0" NumberMaxValue="100000"></CC1:MSTEXTBOX></TD>
											<TD colSpan="2">&nbsp;</TD>
											<TD><%--<asp:requiredfieldvalidator id="Requiredfieldvalidator1" Width="100%" Runat="server" ControlToValidate="txtFirstShipDate"
													Display="None" ErrorMessage="First ship date"></asp:requiredfieldvalidator>
												<asp:label id="Label26" runat="server" Height="22px" Width="100%" CssClass="errorMsgColor">&nbsp;*&nbsp;</asp:label>--%></TD>
											<TD colSpan="5"><%--<asp:label id="lblFirstShpDate" runat="server" Width="100%" CssClass="tableLabel" Height="22px">First ship date</asp:label>--%></TD>
											<TD colSpan="2"><%--<cc1:mstextbox id="txtFirstShipDate" runat="server" tabIndex="32" Width="100%" CssClass="textField"
													AutoPostBack="true" MaxLength="12" TextMaskString="99/99/9999" TextMaskType="msDate"></cc1:mstextbox>--%></TD>
											<TD colSpan="2">&nbsp;</TD>
										</TR>
										<TR height="25">
											<TD align="right"></TD>
											<TD colSpan="5">
												<asp:label id="lblreqOtherSurchargeMaximum" runat="server" Height="22px" Width="100%" CssClass="tableLabel"
													Visible="False">Other Surcharge Maximum</asp:label></TD>
											<TD colSpan="2">
												<CC1:MSTEXTBOX id="msOtherSurchargeMaximum" tabIndex="35" runat="server" Width="100%" CssClass="textFieldRightAlign"
													Visible="False" TextMaskType="msNumeric" MaxLength="9" AutoPostBack="True" NumberScale="9"
													NumberPrecision="9" NumberMinValue="0" NumberMaxValue="100000"></CC1:MSTEXTBOX></TD>
											<TD colSpan="2">&nbsp;</TD>
											<TD>&nbsp;</TD>
											<TD colSpan="5"><%--<asp:label id="lblCategory" runat="server" Width="100%" CssClass="tableLabel" Height="22px">Category</asp:label>--%></TD>
											<TD colSpan="2"><%--<asp:TextBox id="txtCategory" Runat="server" Width="100%" Height="22px" Enabled="False"></asp:TextBox>--%></TD>
											<TD colSpan="2">&nbsp;</TD>
										</TR>
										<TR height="25">
											<TD align="right"></TD>
											<TD colSpan="5">
												<asp:label id="lblreqOtherSurchargeDescription" runat="server" Height="22px" Width="100%" CssClass="tableLabel"
													Visible="False">Other Surcharge Description(for Invoice)</asp:label></TD>
											<TD colSpan="10">
												<CC1:MSTEXTBOX id="msOtherSurchargeDescription" tabIndex="35" runat="server" Width="100%" CssClass="textField"
													Visible="False" MaxLength="15"></CC1:MSTEXTBOX></TD>
											<%--<TD colSpan="2">&nbsp;</TD>
											<td colspan="1">&nbsp;</td>
											<TD colSpan="5">&nbsp;</TD>--%>
											<TD colSpan="2">&nbsp;</TD>
											<TD colSpan="2">&nbsp;</TD>
										</TR>
										<%--Other Surcharge--%>
										<%--<tr height="25">
											<TD colSpan="1">&nbsp;</TD>
											<TD colSpan="5">
												<asp:label id="lblRemark" runat="server" Width="100%" CssClass="tableLabel" Height="22px">Remarks</asp:label></TD>
											<TD colSpan="14">
												<asp:textbox id="txtRemark" runat="server" Width="100%" tabIndex="42" CssClass="textField"></asp:textbox></TD>
										</tr>--%>
										<%--Added By Tom 22/7/09--%>
										<%--<tr height="25">
											<TD colSpan="1">&nbsp;</TD>
											<TD colSpan="5">
												<asp:label id="lblSpecialInstruction" runat="server" Width="100%" CssClass="tableLabel" Height="22px">Special Instruction</asp:label></TD>
											<TD colSpan="14">
												<asp:textbox id="txtSpecialInstruction" runat="server" Width="100%" tabIndex="42" CssClass="textField"></asp:textbox></TD>
										</tr>--%>
										<%--End Added By Tom 22/7/09--%>
									</TABLE>
								</FIELDSET>
							</TD>
						</TR>
					</TABLE>
				</IEWC:PAGEVIEW>
				<IEWC:PAGEVIEW id="ReferencePage">
					<TABLE style="Z-INDEX: 112; WIDTH: 730px" id="Table2" border="0" width="730" runat="server">
						<TR width="100%">
							<TD width="100%">
								<FIELDSET><LEGEND>
										<asp:Label id="Label4" runat="server" CssClass="tableHeadingFieldset">Reference</asp:Label></LEGEND>
									<TABLE id="tblReferences" border="0" width="100%" align="left" runat="server">
										<TR height="5">
											<TD width="2%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="8%"></TD>
										</TR>
										<TR height="25">
											<TD>&nbsp;</TD>
											<TD colSpan="5">
												<asp:label id="lblCustAccDispTab3" runat="server" Height="22px" Width="100%" CssClass="tableLabel">Customer Account</asp:label></TD>
											<TD colSpan="4">
												<asp:textbox id="txtCustAccDispTab3" runat="server" Width="100%" CssClass="textField" Enabled="False"></asp:textbox></TD>
											<TD align="right"></TD>
											<TD colSpan="3">
												<asp:label id="lblCustNameDispTab3" runat="server" Height="22px" Width="100%" CssClass="tableLabel">Customer Name</asp:label></TD>
											<TD colSpan="6">
												<asp:textbox id="txtCustNameDispTab3" runat="server" Width="100%" CssClass="textField" Enabled="False"></asp:textbox></TD>
										</TR>
										<TR height="10">
											<TD colSpan="20"></TD>
										</TR>
										<TR height="25">
											<TD style="HEIGHT: 26px">&nbsp;</TD>
											<TD colSpan="13" align="left">
												<asp:button id="btnRefViewAll" tabIndex="50" runat="server" Width="80px" CssClass="queryButton"
													CausesValidation="False" Text="View All"></asp:button>
												<asp:button id="btnRefInsert" tabIndex="51" runat="server" Width="50px" CssClass="queryButton"
													CausesValidation="False" Text="Insert"></asp:button>
												<asp:button id="btnRefSave" tabIndex="52" runat="server" Width="50px" CssClass="queryButton"
													Text="Save"></asp:button>
												<asp:button id="btnRefDelete" tabIndex="53" runat="server" Width="57px" CssClass="queryButton"
													CausesValidation="False" Text="Delete" Visible="True"></asp:button>
												<asp:button id="btnFirstRef" tabIndex="54" runat="server" Width="19px" CssClass="queryButton"
													CausesValidation="False" Text="|<"></asp:button>
												<asp:button id="btnPreviousRef" tabIndex="55" runat="server" Width="19px" CssClass="queryButton"
													CausesValidation="False" Text="<"></asp:button>
												<asp:textbox id="txtRefJumpCount" tabIndex="56" runat="server" Width="16px"></asp:textbox>
												<asp:button id="btnNextRef" tabIndex="57" runat="server" Width="19px" CssClass="queryButton"
													CausesValidation="False" Text=">"></asp:button>
												<asp:button id="btnLastRef" tabIndex="58" runat="server" Width="19px" CssClass="queryButton"
													CausesValidation="False" Text=">|"></asp:button></TD>
											<TD colSpan="6">
												<asp:label id="lblNumRefRec" runat="server" Height="22px" Width="100%" CssClass="RecMsg"></asp:label></TD>
										</TR>
										<TR height="10">
											<TD colSpan="20"></TD>
										</TR>
										<TR height="25">
											<TD align="right">
												<ASP:REQUIREDFIELDVALIDATOR id="validateRefSndRecName" Width="100%" Runat="server" ErrorMessage="Name " Display="None"
													ControlToValidate="supposrtValidator"></ASP:REQUIREDFIELDVALIDATOR>
												<ASP:REQUIREDFIELDVALIDATOR id="valContactPerson" Width="100%" Runat="server" ErrorMessage="Contact Person"
													Display="None" ControlToValidate="suppContactPerson"></ASP:REQUIREDFIELDVALIDATOR>
												<ASP:REQUIREDFIELDVALIDATOR id="valAddress1" Width="100%" Runat="server" ErrorMessage="Address1" Display="None"
													ControlToValidate="suppAddress1"></ASP:REQUIREDFIELDVALIDATOR>
												<ASP:REQUIREDFIELDVALIDATOR id="valTelephone" Width="100%" Runat="server" ErrorMessage="Telephone" Display="None"
													ControlToValidate="suppTelephone"></ASP:REQUIREDFIELDVALIDATOR>
												<ASP:REQUIREDFIELDVALIDATOR id="valZipcode" Width="100%" Runat="server" ErrorMessage="ZipCode" Display="None"
													ControlToValidate="suppZipcode"></ASP:REQUIREDFIELDVALIDATOR>
												<asp:label id="Label2" runat="server" Height="22px" Width="100%" CssClass="errorMsgColor">&nbsp;*&nbsp;</asp:label></TD>
											<TD colSpan="3">
												<asp:label id="lblRefSndRecName" runat="server" Height="22px" Width="100%" CssClass="tableLabel">Name</asp:label></TD>
											<TD colSpan="6">
												<asp:textbox id="txtRefSndRecName" tabIndex="21" runat="server" Width="100%" CssClass="textField"
													AutoPostBack="False"></asp:textbox>
												<asp:textbox id="supposrtValidator" runat="server" Width="0" Visible="False">Text</asp:textbox>
												<asp:textbox id="suppContactPerson" runat="server" Width="0" Visible="False">Text</asp:textbox>
												<asp:textbox id="suppAddress1" runat="server" Width="0" Visible="False">Text</asp:textbox>
												<asp:textbox id="suppTelephone" runat="server" Width="0" Visible="False">Text</asp:textbox>
												<asp:textbox id="suppZipcode" runat="server" Width="0" Visible="False">Text</asp:textbox></TD>
											<TD>
												<asp:label id="lblreqrefContactPerson" runat="server" Height="22px" Width="100%" CssClass="errorMsgColor">&nbsp;*&nbsp;</asp:label></TD>
											<TD colSpan="3">
												<asp:label id="lblRefContactPerson" runat="server" Height="22px" Width="100%" CssClass="tableLabel">Contact Person</asp:label></TD>
											<TD colSpan="6">
												<asp:textbox id="txtRefContactPerson" tabIndex="60" runat="server" Width="210px" CssClass="textField"
													AutoPostBack="False"></asp:textbox></TD>
										</TR>
										<TR height="25">
											<TD>
												<asp:label id="lblreqRefAddress1" runat="server" Height="22px" Width="100%" CssClass="errorMsgColor">&nbsp;*&nbsp;</asp:label></TD>
											<TD colSpan="3">
												<asp:label id="lblRefAddress1" runat="server" Height="22px" Width="100%" CssClass="tableLabel">Address 1</asp:label></TD>
											<TD colSpan="6">
												<asp:textbox id="txtRefAddress1" tabIndex="61" runat="server" Width="100%" CssClass="textField"></asp:textbox></TD>
											<TD>
												<asp:label id="lblreqReference" runat="server" Height="22px" Width="100%" CssClass="errorMsgColor">&nbsp;*&nbsp;</asp:label></TD>
											<TD colSpan="3">
												<asp:label id="lblRefType" runat="server" Height="22px" Width="100%" CssClass="tableLabel">Reference Type</asp:label></TD>
											<TD colSpan="2">
												<ASP:CHECKBOX id="chkSender" tabIndex="66" runat="server" Height="15px" Width="100%" CssClass="tableRadioButton"
													Text="Sender" AutoPostBack="False" Font-Size="Smaller"></ASP:CHECKBOX></TD>
											<TD colSpan="4">
												<ASP:CHECKBOX id="chkRecipient" tabIndex="67" runat="server" Height="15px" Width="100%" CssClass="tableRadioButton"
													Text="Recipient" AutoPostBack="False" Font-Size="Smaller"></ASP:CHECKBOX></TD>
										</TR>
										<TR height="25">
											<TD>&nbsp;</TD>
											<TD colSpan="3">
												<asp:label id="lblRefAddress2" runat="server" Height="22px" Width="100%" CssClass="tableLabel">Address 2</asp:label></TD>
											<TD colSpan="6">
												<asp:textbox id="txtRefAddress2" tabIndex="62" runat="server" Width="100%" CssClass="textField"></asp:textbox></TD>
											<TD>
												<asp:label id="lblreqRefTelephone" runat="server" Height="22px" Width="100%" CssClass="errorMsgColor">&nbsp;*&nbsp;</asp:label></TD>
											<TD colSpan="3">
												<asp:label id="lblRefTelephone" runat="server" Height="22px" Width="100%" CssClass="tableLabel">Telephone</asp:label></TD>
											<TD colSpan="3">
												<asp:textbox id="txtRefTelephone" tabIndex="68" runat="server" Width="100%" CssClass="textField"></asp:textbox></TD>
											<TD colSpan="3">&nbsp;</TD>
										</TR>
										<TR height="25">
											<TD align="right">
												<asp:label id="lblreqRefZipcode" runat="server" Height="22px" Width="100%" CssClass="errorMsgColor">&nbsp;*&nbsp;</asp:label></TD>
											<TD colSpan="3">
												<asp:label id="lblRefZipcode" runat="server" Height="22px" Width="100%" CssClass="tableLabel">Postal Code</asp:label></TD>
											<TD colSpan="2">
												<CC1:MSTEXTBOX id="txtRefZipcode" tabIndex="63" runat="server" Width="100%" CssClass="textField"
													TextMaskType="msUpperAlfaNumericWithUnderscore" MaxLength="12" AutoPostBack="False"></CC1:MSTEXTBOX></TD>
											<TD>
												<asp:button id="btnRefZipcodeSearch" tabIndex="64" runat="server" CssClass="queryButton" CausesValidation="False"
													Text="..."></asp:button></TD>
											<TD>
												<asp:label id="lblRefState" runat="server" Height="22px" Width="100%" CssClass="tableLabel">State</asp:label></TD>
											<TD colSpan="3">
												<asp:textbox id="txtRefState" runat="server" Width="100%" CssClass="textField" ReadOnly="True"></asp:textbox></TD>
											<TD colSpan="3">
												<asp:label id="lblRefFax" runat="server" Height="22px" Width="100%" CssClass="tableLabel">Fax</asp:label></TD>
											<TD colSpan="3">
												<asp:textbox id="txtRefFax" tabIndex="69" runat="server" Width="100%" CssClass="textField"></asp:textbox></TD>
											<TD colSpan="3">&nbsp;</TD>
										</TR>
										<TR height="25">
											<TD align="right"></TD>
											<TD colSpan="3">
												<asp:label id="lblRefCountry" runat="server" Height="22px" Width="100%" CssClass="tableLabel">Country</asp:label></TD>
											<TD colSpan="2">
												<asp:textbox id="txtRefCountry" tabIndex="65" runat="server" Width="100%" CssClass="textField"
													AutoPostBack="False" ReadOnly="True"></asp:textbox></TD>
											<TD colSpan="5">&nbsp;</TD>
											<TD colSpan="3">
												<asp:label id="lblRefEmail" runat="server" Height="22px" Width="120px" CssClass="tableLabel">Email</asp:label></TD>
											<TD colSpan="6">
												<asp:textbox id="txtRefEmail" tabIndex="120" runat="server" Width="210px" CssClass="textField"></asp:textbox></TD>
										</TR>
									</TABLE>
								</FIELDSET>
							</TD>
						</TR>
					</TABLE>
				</IEWC:PAGEVIEW>
				<IEWC:PAGEVIEW id="StatusPage">
					<TABLE style="Z-INDEX: 112; WIDTH: 730px" id="Table3" border="0" width="730" runat="server">
						<TR width="100%">
							<TD width="100%">
								<FIELDSET><LEGEND>
										<asp:Label id="Label5" runat="server" CssClass="tableHeadingFieldset">Status & Exception Surcharges</asp:Label></LEGEND>
									<TABLE id="tblStatus" border="0" width="100%" align="left" runat="server">
										<TR height="5">
											<TD width="2%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="8%"></TD>
										</TR>
										<TR height="25">
											<TD>&nbsp;</TD>
											<TD colSpan="5">
												<asp:label id="lblCustAccDispTab4" runat="server" Height="22px" Width="100%" CssClass="tableLabel">Customer Account</asp:label></TD>
											<TD colSpan="4">
												<asp:textbox id="txtCustAccDispTab4" runat="server" Width="100%" CssClass="textField" Enabled="False"></asp:textbox></TD>
											<TD align="right"></TD>
											<TD colSpan="3">
												<asp:label id="lblCustNameDispTab4" runat="server" Height="22px" Width="100%" CssClass="tableLabel">Customer Name</asp:label></TD>
											<TD colSpan="6">
												<asp:textbox id="txtCustNameDispTab4" runat="server" Width="100%" CssClass="textField" Enabled="False"></asp:textbox></TD>
										</TR>
										<TR height="5">
											<TD colSpan="20"></TD>
										</TR>
										<TR height="25">
											<TD style="HEIGHT: 26px">&nbsp;</TD>
											<TD colSpan="12" align="left">
												<asp:button id="btnSCViewAll" tabIndex="50" onclick="btnSCViewAll_Click" runat="server" Height="22px"
													Width="80px" CssClass="queryButton" CausesValidation="False" Text="View All"></asp:button>
												<asp:button id="btnSCInsert" tabIndex="51" onclick="btnSCInsert_Click" runat="server" Height="22px"
													Width="62px" CssClass="queryButton" CausesValidation="False" Text="Insert"></asp:button></TD>
											<TD colSpan="7">
												<asp:label id="lblSCRecCnt" runat="server" Height="22px" Width="100%" CssClass="RecMsg"></asp:label></TD>
										</TR>
										<TR height="25">
											<TD>&nbsp;</TD>
											<TD colSpan="19">
												<asp:label id="lblSCMessage" runat="server" Height="22px" Width="100%" CssClass="errorMsgColor">Status Error.....</asp:label></TD>
										</TR>
										<TR>
											<TD>&nbsp;</TD>
											<TD colSpan="17">
												<ASP:DATAGRID style="Z-INDEX: 100" id="dgStatusCodes" runat="server" Width="100%" OnSelectedIndexChanged="dgStatusCodes_SelectedIndexChanged"
													ItemStyle-Height="20" AutoGenerateColumns="False" OnPageIndexChanged="dgStatusCodes_PageChange"
													OnItemCommand="dgStatusCodes_Button" OnItemDataBound="dgStatusCodes_Bound" OnEditCommand="dgStatusCodes_Edit"
													OnCancelCommand="dgStatusCodes_Cancel" OnUpdateCommand="dgStatusCodes_Update" AllowPaging="True"
													PageSize="3" AllowCustomPaging="True" OnDeleteCommand="dgStatusCodes_Delete" SelectedItemStyle-CssClass="gridFieldSelected">
													<SELECTEDITEMSTYLE CssClass="gridFieldSelected"></SELECTEDITEMSTYLE>
													<ITEMSTYLE Height="20px" CssClass="gridField"></ITEMSTYLE>
													<COLUMNS>
														<ASP:BUTTONCOLUMN Text="<IMG SRC='images/butt-select.gif' border=0 title='Select' >" CommandName="Select"
															ButtonType="LinkButton">
															<HEADERSTYLE Width="3%" CssClass="gridHeading" HorizontalAlign="Center"></HEADERSTYLE>
															<ITEMSTYLE HorizontalAlign="Center"></ITEMSTYLE>
														</ASP:BUTTONCOLUMN>
														<ASP:EDITCOMMANDCOLUMN ButtonType="LinkButton" EditText="<IMG SRC='images/butt-edit.gif' border=0 title='Edit' >"
															CancelText="<IMG SRC='images/butt-cancel.gif' border=0 title='Cancel' >" UpdateText="<IMG SRC='images/butt-update.gif' border=0 title='Update' >">
															<HEADERSTYLE Width="5%" CssClass="gridHeading"></HEADERSTYLE>
															<ITEMSTYLE HorizontalAlign="Left"></ITEMSTYLE>
														</ASP:EDITCOMMANDCOLUMN>
														<ASP:BUTTONCOLUMN Text="<IMG SRC='images/butt-delete.gif' border=0 title='Delete' >" CommandName="Delete"
															ButtonType="LinkButton">
															<HEADERSTYLE Width="3%" CssClass="gridHeading" HorizontalAlign="Center"></HEADERSTYLE>
															<ITEMSTYLE HorizontalAlign="Center"></ITEMSTYLE>
														</ASP:BUTTONCOLUMN>
														<ASP:TEMPLATECOLUMN HeaderText="Status Code">
															<HEADERSTYLE Width="25%" CssClass="gridHeading" Font-Bold="True"></HEADERSTYLE>
															<ITEMSTYLE Wrap="False"></ITEMSTYLE>
															<ITEMTEMPLATE>
																<asp:Label id=lblStatusCode CssClass="gridLabel" Text='<%#DataBinder.Eval(Container.DataItem,"status_code")%>' Runat="server">
																</asp:Label>
															</ITEMTEMPLATE>
															<EDITITEMTEMPLATE>
																<CC1:MSTEXTBOX id=txtStatusCode CssClass="gridTextBox" Text='<%#DataBinder.Eval(Container.DataItem,"status_code")%>' Runat="server" TextMaskType="msUpperAlfaNumericWithUnderscore" MaxLength="12" Enabled="True">
																</CC1:MSTEXTBOX>
																<ASP:REQUIREDFIELDVALIDATOR id="scValidator" Runat="server" ErrorMessage="Status Code" Display="None" ControlToValidate="txtStatusCode"
																	BorderWidth="0"></ASP:REQUIREDFIELDVALIDATOR>
															</EDITITEMTEMPLATE>
														</ASP:TEMPLATECOLUMN>
														<ASP:BUTTONCOLUMN Text="<IMG SRC='images/butt-search.gif' border=0 title='Search' >" CommandName="search"
															ButtonType="LinkButton">
															<HEADERSTYLE Width="3%" CssClass="gridHeading" HorizontalAlign="Center"></HEADERSTYLE>
															<ITEMSTYLE CssClass="gridField" HorizontalAlign="Center"></ITEMSTYLE>
														</ASP:BUTTONCOLUMN>
														<ASP:TEMPLATECOLUMN HeaderText="Status Description">
															<HEADERSTYLE Width="35%" CssClass="gridHeading"></HEADERSTYLE>
															<ITEMTEMPLATE>
																<asp:Label id=lblStatusDescription CssClass="gridLabel" Text='<%#DataBinder.Eval(Container.DataItem,"status_description")%>' Runat="server" Enabled="True">
																</asp:Label>
															</ITEMTEMPLATE>
															<EDITITEMTEMPLATE>
																<asp:TextBox id=txtStatusDescription CssClass="gridTextBox" Text='<%#DataBinder.Eval(Container.DataItem,"status_description")%>' Runat="server" Enabled="True" ReadOnly="True">
																</asp:TextBox>
															</EDITITEMTEMPLATE>
														</ASP:TEMPLATECOLUMN>
														<ASP:TEMPLATECOLUMN HeaderText="Charge Amount">
															<HEADERSTYLE Width="25%" CssClass="gridHeading"></HEADERSTYLE>
															<ITEMTEMPLATE>
																<asp:Label id=lblSCChargeAmount CssClass="gridLabel" Text='<%#DataBinder.Eval(Container.DataItem,"surcharge","{0:n}")%>' Runat="server" Enabled="True">
																</asp:Label>
															</ITEMTEMPLATE>
															<EDITITEMTEMPLATE>
																<CC1:MSTEXTBOX id=txtSCChargeAmount CssClass="gridTextBox" Text='<%#DataBinder.Eval(Container.DataItem,"surcharge", "{0:n}")%>' Runat="server" TextMaskType="msNumeric" MaxLength="9" Enabled="True" NumberScale="2" NumberPrecision="8" NumberMinValue="0" NumberMaxValue="999999">
																</CC1:MSTEXTBOX>
															</EDITITEMTEMPLATE>
														</ASP:TEMPLATECOLUMN>
													</COLUMNS>
													<PAGERSTYLE CssClass="normalText" HorizontalAlign="Right" PrevPageText="Previous" NextPageText="Next"></PAGERSTYLE>
												</ASP:DATAGRID></TD>
											<TD colSpan="2">&nbsp;</TD>
										</TR>
										<TR height="5">
											<TD colSpan="20"></TD>
										</TR>
										<TR height="25">
											<TD style="HEIGHT: 26px">&nbsp;</TD>
											<TD colSpan="12" align="left">
												<asp:button id="btnExInsert" tabIndex="51" onclick="btnExInsert_Click" runat="server" Height="22px"
													Width="62px" CssClass="queryButton" CausesValidation="False" Text="Insert"></asp:button></TD>
											<TD colSpan="5">
												<asp:label id="lblExRecCnt" runat="server" Height="22px" Width="100%" CssClass="RecMsg"></asp:label></TD>
										</TR>
										<TR height="25">
											<TD>&nbsp;</TD>
											<TD colSpan="19">
												<asp:label id="lblEXMessage" runat="server" Height="22px" Width="100%" CssClass="errorMsgColor"></asp:label></TD>
										</TR>
										<TR height="25">
											<TD>&nbsp;</TD>
											<TD colSpan="17">
												<ASP:DATAGRID style="Z-INDEX: 101" id="dgExceptionCodes" runat="server" Width="100%" ItemStyle-Height="20"
													AutoGenerateColumns="False" OnPageIndexChanged="dgExceptionCodes_PageChange" OnItemCommand="dgExceptionCodes_Button"
													OnItemDataBound="dgExceptionCodes_Bound" OnEditCommand="dgExceptionCodes_Edit" OnCancelCommand="dgExceptionCodes_Cancel"
													OnUpdateCommand="dgExceptionCodes_Update" AllowPaging="True" PageSize="25" AllowCustomPaging="True"
													OnDeleteCommand="dgExceptionCodes_Delete">
													<ITEMSTYLE Height="20px" CssClass="gridField"></ITEMSTYLE>
													<COLUMNS>
														<ASP:EDITCOMMANDCOLUMN ButtonType="LinkButton" EditText="<IMG SRC='images/butt-edit.gif' border=0 title='Edit' >"
															CancelText="<IMG SRC='images/butt-cancel.gif' border=0 title='Cancel' >" UpdateText="<IMG SRC='images/butt-update.gif' border=0 title='Update' >">
															<HEADERSTYLE Width="5%" CssClass="gridHeading"></HEADERSTYLE>
															<ITEMSTYLE CssClass="gridField"></ITEMSTYLE>
														</ASP:EDITCOMMANDCOLUMN>
														<ASP:BUTTONCOLUMN Text="<IMG SRC='images/butt-delete.gif' border=0 title='Delete' >" CommandName="Delete"
															ButtonType="LinkButton">
															<HEADERSTYLE Width="3%" CssClass="gridHeading" HorizontalAlign="Center"></HEADERSTYLE>
															<ITEMSTYLE CssClass="gridField" HorizontalAlign="Center"></ITEMSTYLE>
														</ASP:BUTTONCOLUMN>
														<ASP:TEMPLATECOLUMN HeaderText="Exception Code">
															<HEADERSTYLE Width="25%" CssClass="gridHeading" Font-Bold="True"></HEADERSTYLE>
															<ITEMTEMPLATE>
																<asp:Label id=lblExceptionCode CssClass="gridLabel" Text='<%#DataBinder.Eval(Container.DataItem,"exception_code")%>' Runat="server">
																</asp:Label>
															</ITEMTEMPLATE>
															<EDITITEMTEMPLATE>
																<CC1:MSTEXTBOX id=txtExceptionCode CssClass="gridTextBox" Text='<%#DataBinder.Eval(Container.DataItem,"exception_code")%>' Runat="server" TextMaskType="msUpperAlfaNumericWithUnderscore" MaxLength="12" Enabled="True">
																</CC1:MSTEXTBOX>
																<ASP:REQUIREDFIELDVALIDATOR id="exValidator" Runat="server" ErrorMessage="Exception Code" Display="None" ControlToValidate="txtExceptionCode"
																	BorderWidth="0"></ASP:REQUIREDFIELDVALIDATOR>
															</EDITITEMTEMPLATE>
														</ASP:TEMPLATECOLUMN>
														<ASP:BUTTONCOLUMN Text="<IMG SRC='images/butt-search.gif' border=0 title='Search' >" CommandName="search"
															ButtonType="LinkButton">
															<HEADERSTYLE Width="3%" CssClass="gridHeading" HorizontalAlign="Center"></HEADERSTYLE>
															<ITEMSTYLE CssClass="gridField" HorizontalAlign="Center"></ITEMSTYLE>
														</ASP:BUTTONCOLUMN>
														<ASP:TEMPLATECOLUMN HeaderText="Exception Description">
															<HEADERSTYLE Width="49%" CssClass="gridHeading"></HEADERSTYLE>
															<ITEMTEMPLATE>
																<asp:Label id=lblExceptionDescription CssClass="gridLabel" Text='<%#DataBinder.Eval(Container.DataItem,"exception_description")%>' Runat="server" Enabled="True">
																</asp:Label>
															</ITEMTEMPLATE>
															<EDITITEMTEMPLATE>
																<asp:TextBox id=txtExceptionDescription CssClass="gridTextBox" Text='<%#DataBinder.Eval(Container.DataItem,"exception_description")%>' Runat="server" Enabled="True" ReadOnly="True">
																</asp:TextBox>
															</EDITITEMTEMPLATE>
														</ASP:TEMPLATECOLUMN>
														<ASP:TEMPLATECOLUMN HeaderText="Charge Amount">
															<HEADERSTYLE Width="25%" CssClass="gridHeading"></HEADERSTYLE>
															<ITEMTEMPLATE>
																<asp:Label id=lblEXChargeAmount CssClass="gridLabel" Text='<%#DataBinder.Eval(Container.DataItem,"surcharge","{0:n}")%>' Runat="server" Enabled="True">
																</asp:Label>
															</ITEMTEMPLATE>
															<EDITITEMTEMPLATE>
																<CC1:MSTEXTBOX id=txtEXChargeAmount CssClass="gridTextBox" Text='<%#DataBinder.Eval(Container.DataItem,"surcharge","{0:n}")%>' Runat="server" TextMaskType="msNumeric" MaxLength="9" NumberScale="2" NumberPrecision="8" NumberMinValue="0" NumberMaxValue="999999" Enable="True">
																</CC1:MSTEXTBOX>
															</EDITITEMTEMPLATE>
														</ASP:TEMPLATECOLUMN>
													</COLUMNS>
													<PAGERSTYLE CssClass="normalText" HorizontalAlign="Right" PrevPageText="Previous" NextPageText="Next"></PAGERSTYLE>
												</ASP:DATAGRID></TD>
											<TD colSpan="2">&nbsp;</TD>
										</TR>
									</TABLE>
								</FIELDSET>
							</TD>
						</TR>
					</TABLE>
				</IEWC:PAGEVIEW>
				<IEWC:PAGEVIEW id="QuatationPage">
					<TABLE style="Z-INDEX: 112; WIDTH: 730px" id="Table5" border="0" width="600" runat="server">
						<TR width="100%">
							<TD width="100%">
								<FIELDSET><LEGEND>
										<asp:Label id="Label6" runat="server" CssClass="tableHeadingFieldset">Quotation Status</asp:Label></LEGEND>
									<TABLE id="tblQuatationStatus" border="0" align="left" runat="server">
										<TR height="5">
											<TD width="2%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="8%"></TD>
										</TR>
										<TR height="25">
											<TD>&nbsp;</TD>
											<TD colSpan="5">
												<asp:label id="lblCustAccDispTab5" runat="server" Height="22px" Width="100%" CssClass="tableLabel">Customer Account</asp:label></TD>
											<TD colSpan="4">
												<asp:textbox id="txtCustAccDispTab5" runat="server" Width="100%" CssClass="textField" Enabled="False"></asp:textbox></TD>
											<TD align="right"></TD>
											<TD colSpan="3">
												<asp:label id="lblCustNameDispTab5" runat="server" Height="22px" Width="100%" CssClass="tableLabel">Customer Name</asp:label></TD>
											<TD colSpan="6">
												<asp:textbox id="txtCustNameDispTab5" runat="server" Width="100%" CssClass="textField" Enabled="False"></asp:textbox></TD>
										</TR>
										<TR height="5">
											<TD colSpan="20"></TD>
										</TR>
										<TR height="25">
											<TD style="HEIGHT: 26px">&nbsp;</TD>
											<TD colSpan="13" align="left">
												<asp:button id="btnQuoteViewAll" tabIndex="50" runat="server" Height="22px" Width="80px" CssClass="queryButton"
													CausesValidation="False" Text="View All"></asp:button></TD>
											<TD colSpan="6">
												<asp:label id="lblNumQuoteRec" runat="server" Height="22px" Width="100%" CssClass="RecMsg"></asp:label></TD>
										</TR>
										<TR>
											<TD colSpan="20">
												<ASP:DATAGRID style="Z-INDEX: 104; POSITION: absolute; TOP: 125px; LEFT: 18px" id="dgQuotation"
													runat="server" Width="580px" ItemStyle-Height="20" AutoGenerateColumns="False" OnPageIndexChanged="OnQuotation_PageChange"
													OnItemDataBound="OnItemBound_Quotation" AllowPaging="True" PageSize="7" AllowCustomPaging="True"
													PagerStyle-PageButtonCount="7">
													<COLUMNS>
														<ASP:TEMPLATECOLUMN HeaderText="Quotation No.">
															<HEADERSTYLE Width="40%" CssClass="gridHeading"></HEADERSTYLE>
															<ITEMSTYLE CssClass="gridField"></ITEMSTYLE>
															<ITEMTEMPLATE>
																<asp:Label id=lblQuoteNo CssClass="gridLabel" Text='<%#DataBinder.Eval(Container.DataItem,"Quotation_No")%>' Visible="True" Runat="server">
																</asp:Label>
															</ITEMTEMPLATE>
														</ASP:TEMPLATECOLUMN>
														<ASP:TEMPLATECOLUMN HeaderText="Version">
															<HEADERSTYLE Width="10%" CssClass="gridHeading"></HEADERSTYLE>
															<ITEMSTYLE CssClass="gridField"></ITEMSTYLE>
															<ITEMTEMPLATE>
																<asp:Label id=lblQuoteVersion CssClass="gridLabel" Text='<%#DataBinder.Eval(Container.DataItem,"Quotation_version")%>' Visible="True" Runat="server">
																</asp:Label>
															</ITEMTEMPLATE>
														</ASP:TEMPLATECOLUMN>
														<ASP:TEMPLATECOLUMN HeaderText="Date">
															<HEADERSTYLE Width="15%" CssClass="gridHeading"></HEADERSTYLE>
															<ITEMSTYLE CssClass="gridField"></ITEMSTYLE>
															<ITEMTEMPLATE>
																<asp:Label id=lblQuoteDate CssClass="gridLabel" Text='<%#DataBinder.Eval(Container.DataItem,"Quotation_Date","{0:dd/MM/yyyy}")%>' Visible="True" Runat="server">
																</asp:Label>
															</ITEMTEMPLATE>
														</ASP:TEMPLATECOLUMN>
														<ASP:TEMPLATECOLUMN HeaderText="Status">
															<HEADERSTYLE Width="25%" CssClass="gridHeading"></HEADERSTYLE>
															<ITEMSTYLE CssClass="gridField"></ITEMSTYLE>
															<ITEMTEMPLATE>
																<asp:Label id=lblQuoteStatus CssClass="gridLabel" Text='<%#DataBinder.Eval(Container.DataItem,"Quotation_Status")%>' Visible="True" Runat="server">
																</asp:Label>
															</ITEMTEMPLATE>
														</ASP:TEMPLATECOLUMN>
													</COLUMNS>
													<PAGERSTYLE CssClass="normalText" HorizontalAlign="Right" PrevPageText="Previous" NextPageText="Next"></PAGERSTYLE>
												</ASP:DATAGRID></TD>
										</TR>
									</TABLE>
								</FIELDSET>
							</TD>
						</TR>
					</TABLE>
				</IEWC:PAGEVIEW>
				<IEWC:PAGEVIEW id="Zones">
					<TABLE style="Z-INDEX: 112; POSITION: absolute; WIDTH: 730px; TOP: 10px" id="Table4" border="0"
						width="600" runat="server">
						<TR width="100%">
							<TD width="100%">
								<FIELDSET><LEGEND>
										<asp:label id="Label8" runat="server" CssClass="tableHeadingFieldset">Zones</asp:label></LEGEND>
									<TABLE id="Table6" border="0" align="left" runat="server">
										<TR height="5">
											<TD width="2%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="8%"></TD>
										</TR>
										<TR height="25">
											<TD>&nbsp;</TD>
											<TD colSpan="5">
												<asp:label id="Label9" runat="server" Height="22px" Width="100%" CssClass="tableLabel">Customer Account</asp:label></TD>
											<TD colSpan="4">
												<asp:textbox id="txtCustAccDispTab6" runat="server" Width="100%" CssClass="textField" Enabled="False"></asp:textbox></TD>
											<TD align="right"></TD>
											<TD colSpan="3">
												<asp:label id="Label10" runat="server" Height="22px" Width="100%" CssClass="tableLabel">Customer Name</asp:label></TD>
											<TD colSpan="6">
												<asp:textbox id="txtCustNameDispTab6" runat="server" Width="100%" CssClass="textField" Enabled="False"></asp:textbox></TD>
										</TR>
										<TR>
											<TD colSpan="20">&nbsp;</TD>
										</TR>
										<TR>
											<TD>&nbsp;</TD>
											<TD colSpan="17">
												<asp:button id="btnQueryFR" runat="server" Width="70px" CssClass="queryButton" CausesValidation="False"
													Text="Query"></asp:button>
												<asp:button id="btnExecuteQueryFR" runat="server" Width="120px" CssClass="queryButton" CausesValidation="False"
													Text="Execute Query"></asp:button>
												<asp:button id="btnInsertFR" runat="server" Width="70px" CssClass="queryButton" CausesValidation="False"
													Text="Insert"></asp:button>&nbsp;
												<asp:button id="btnImport_Show" runat="server" Width="62px" CssClass="queryButton" Text="Import"></asp:button></TD>
										</TR>
										<TR>
											<TD></TD>
											<TD colSpan="19"><FONT face="Tahoma">
													<asp:button id="btnImport_Cancel" runat="server" Width="62px" CssClass="queryButton" Text="Cancel"></asp:button>
													<DIV style="BACKGROUND-IMAGE: url(images/btn-browse-up.GIF); WIDTH: 70px; DISPLAY: inline; HEIGHT: 21px"
														id="divBrowse" onmouseup="upBrowse();" onmousedown="downBrowse();"><INPUT onblur="setValue();" style="BORDER-BOTTOM: #88a0c8 1px outset; FILTER: alpha(opacity: 0); BORDER-LEFT: #88a0c8 1px outset; BACKGROUND-COLOR: #e9edf0; WIDTH: 0px; FONT-FAMILY: Arial; COLOR: #003068; FONT-SIZE: 11px; BORDER-TOP: #88a0c8 1px outset; BORDER-RIGHT: #88a0c8 1px outset; TEXT-DECORATION: none"
															id="inFile" onfocus="setValue();" type="file" name="inFile" runat="server"></DIV>
													<asp:button id="btnImport" runat="server" Width="62px" CssClass="queryButton" Text="Import"
														Enabled="False"></asp:button>
													<asp:button id="btnImport_Save" runat="server" Width="66px" CssClass="queryButton" Text="Save"
														Enabled="False"></asp:button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
													<asp:textbox id="txtFilePath" runat="server" Width="240px" CssClass="textField"></asp:textbox></FONT></TD>
										</TR>
										<TR>
											<TD colSpan="20">&nbsp;</TD>
										</TR>
										<TR>
											<TD>&nbsp;</TD>
											<TD colSpan="19">
												<ASP:DATAGRID id="dgCustZones" runat="server" Width="528px" Visible="True" AutoGenerateColumns="False"
													OnPageIndexChanged="dgCustZones_PageChange" OnItemCommand="dgCustZones_Button" OnEditCommand="dgCustZones_Edit"
													OnCancelCommand="dgCustZones_Cancel" OnUpdateCommand="dgCustZones_Update" AllowPaging="True"
													PageSize="50" OnDeleteCommand="dgCustZones_Delete" HorizontalAlign="left">
													<COLUMNS>
														<ASP:TEMPLATECOLUMN>
															<HEADERSTYLE CssClass="gridHeading"></HEADERSTYLE>
															<ITEMSTYLE CssClass="gridField" Wrap="False"></ITEMSTYLE>
															<ITEMTEMPLATE>
																&nbsp;&nbsp;
																<ASP:IMAGEBUTTON id="imgSelect" runat="server" Enabled="False" CommandName="Select" ImageUrl="images/butt-select.gif"></ASP:IMAGEBUTTON>&nbsp;&nbsp;
															</ITEMTEMPLATE>
														</ASP:TEMPLATECOLUMN>
														<ASP:EDITCOMMANDCOLUMN ButtonType="LinkButton" EditText="<IMG SRC='images/butt-edit.gif' border=0 title='Edit' >"
															CancelText="<IMG SRC='images/butt-cancel.gif' border=0 title='Cancel' >" UpdateText="<IMG SRC='images/butt-update.gif' border=0 title='Update' >">
															<HEADERSTYLE Width="4%" CssClass="gridHeading"></HEADERSTYLE>
															<ITEMSTYLE CssClass="gridField" Wrap="False"></ITEMSTYLE>
														</ASP:EDITCOMMANDCOLUMN>
														<ASP:BUTTONCOLUMN Text="<IMG SRC='images/butt-delete.gif' border=0 title='Delete' >" CommandName="Delete">
															<HEADERSTYLE Width="2%" CssClass="gridHeading"></HEADERSTYLE>
															<ITEMSTYLE CssClass="gridField"></ITEMSTYLE>
														</ASP:BUTTONCOLUMN>
														<ASP:TEMPLATECOLUMN HeaderText="Postal Code">
															<HEADERSTYLE Width="30%" CssClass="gridHeading" Font-Bold="True"></HEADERSTYLE>
															<ITEMSTYLE CssClass="gridField" Wrap="False"></ITEMSTYLE>
															<ITEMTEMPLATE>
																<asp:Label id=lblzipcode CssClass="gridLabel" Text='<%#DataBinder.Eval(Container.DataItem,"zipcode")%>' Visible="True" Runat="server">
																</asp:Label>
															</ITEMTEMPLATE>
															<EDITITEMTEMPLATE>
																<asp:TextBox id=Textbox1 CssClass="gridTextBox" Text='<%#DataBinder.Eval(Container.DataItem,"zipcode")%>' Visible="True" Runat="server" MaxLength="12">
																</asp:TextBox>
															</EDITITEMTEMPLATE>
														</ASP:TEMPLATECOLUMN>
														<ASP:BUTTONCOLUMN Text="<IMG SRC='images/butt-search.gif' border=0 title='Search' >" CommandName="zipcodeSearch"
															ButtonType="LinkButton">
															<HEADERSTYLE Width="5%" CssClass="gridHeading"></HEADERSTYLE>
															<ITEMSTYLE CssClass="gridField" Wrap="False"></ITEMSTYLE>
														</ASP:BUTTONCOLUMN>
														<ASP:TEMPLATECOLUMN HeaderText="Zone">
															<HEADERSTYLE Width="30%" CssClass="gridHeading" Font-Bold="True"></HEADERSTYLE>
															<ITEMSTYLE CssClass="gridField" Wrap="False"></ITEMSTYLE>
															<ITEMTEMPLATE>
																<asp:Label id=lblzone CssClass="gridLabel" Text='<%#DataBinder.Eval(Container.DataItem,"zone_code")%>' Visible="True" Runat="server">
																</asp:Label>
															</ITEMTEMPLATE>
															<EDITITEMTEMPLATE>
																<asp:TextBox id=txtzone CssClass="gridTextBox" Text='<%#DataBinder.Eval(Container.DataItem,"zone_code")%>' Visible="True" Runat="server" MaxLength="200">
																</asp:TextBox>
															</EDITITEMTEMPLATE>
														</ASP:TEMPLATECOLUMN>
														<ASP:BUTTONCOLUMN Text="<IMG SRC='images/butt-search.gif' border=0 title='Search' >" CommandName="zoneSearch"
															ButtonType="LinkButton">
															<HEADERSTYLE Width="5%" CssClass="gridHeading"></HEADERSTYLE>
															<ITEMSTYLE CssClass="gridField" Wrap="False"></ITEMSTYLE>
														</ASP:BUTTONCOLUMN>
														<ASP:TEMPLATECOLUMN HeaderText="Effective Date">
															<HEADERSTYLE Width="30%" CssClass="gridHeading" Font-Bold="True"></HEADERSTYLE>
															<ITEMSTYLE CssClass="gridField" Wrap="False"></ITEMSTYLE>
															<ITEMTEMPLATE>
																<asp:Label id=lblEffectiveDate runat="server" CssClass="gridLabel" Text='<%#DataBinder.Eval(Container.DataItem,"effective_date","{0:dd/MM/yyyy}")%>'>
																</asp:Label>
															</ITEMTEMPLATE>
															<EDITITEMTEMPLATE>
																<CC1:MSTEXTBOX id=txtEffectiveDate Text='<%#DataBinder.Eval(Container.DataItem,"effective_date","{0:dd/MM/yyyy}")%>' Runat="server" TextMaskType="msDate" MaxLength="10" TextMaskString="99/99/9999">
																</CC1:MSTEXTBOX>
															</EDITITEMTEMPLATE>
														</ASP:TEMPLATECOLUMN>
													</COLUMNS>
													<PAGERSTYLE CssClass="normalText" HorizontalAlign="Right" PrevPageText="Previous" NextPageText="Next"></PAGERSTYLE>
												</ASP:DATAGRID></TD>
										</TR>
									</TABLE>
								</FIELDSET>
							</TD>
						</TR>
					</TABLE>
				</IEWC:PAGEVIEW>
				<IEWC:PAGEVIEW id="VAS">
					<TABLE style="Z-INDEX: 112; WIDTH: 730px" id="Table7" border="0" width="600" runat="server">
						<TR width="100%">
							<TD width="100%">
								<FIELDSET><LEGEND>
										<asp:Label id="Label12" runat="server" CssClass="tableHeadingFieldset">Value Added Services and Surcharges</asp:Label></LEGEND>
									<TABLE id="Table8" border="0" align="left" runat="server">
										<TR height="25">
											<TD>&nbsp;</TD>
											<TD colSpan="5">
												<asp:label id="Label18" runat="server" Height="22px" Width="100%" CssClass="tableLabel">Customer Account</asp:label></TD>
											<TD colSpan="4">
												<asp:textbox id="txtCustAccDispTab7" runat="server" Width="100%" CssClass="textField" Enabled="False"></asp:textbox></TD>
											<TD align="right"></TD>
											<TD colSpan="3">
												<asp:label id="Label19" runat="server" Height="22px" Width="100%" CssClass="tableLabel">Customer Name</asp:label></TD>
											<TD colSpan="6">
												<asp:textbox id="txtCustNameDispTab7" runat="server" Width="100%" CssClass="textField" Enabled="False"></asp:textbox></TD>
										</TR>
										<TR>
											<TD colSpan="20">&nbsp;</TD>
										</TR>
										<TR>
											<TD>&nbsp;</TD>
											<TD colSpan="17">
												<asp:button id="btnViewAllVAS" runat="server" Width="70px" CssClass="queryButton" CausesValidation="False"
													Text="View All"></asp:button>
												<asp:button id="btnInsertVAS" runat="server" Width="70px" CssClass="queryButton" CausesValidation="False"
													Text="Insert"></asp:button></TD>
										</TR>
										<TR>
											<TD colSpan="20">&nbsp;</TD>
										</TR>
										<TR>
											<TD>&nbsp;</TD>
											<TD colSpan="19">
												<ASP:DATAGRID id="dgVAS" runat="server" Width="100%" OnSelectedIndexChanged="dgVAS_SelectedIndexChanged"
													ItemStyle-Height="20" AutoGenerateColumns="False" OnPageIndexChanged="dgVAS_PageChange" OnItemDataBound="dgVAS_Bound"
													OnEditCommand="dgVAS_Edit" OnCancelCommand="dgVAS_Cancel" OnUpdateCommand="dgVAS_Update" AllowPaging="True"
													PageSize="5" AllowCustomPaging="True" OnDeleteCommand="dgVAS_Delete" SelectedItemStyle-CssClass="gridFieldSelected"
													HorizontalAlign="left">
													<SELECTEDITEMSTYLE CssClass="gridFieldSelected"></SELECTEDITEMSTYLE>
													<ITEMSTYLE Height="20px" CssClass="gridField"></ITEMSTYLE>
													<COLUMNS>
														<ASP:EDITCOMMANDCOLUMN ButtonType="LinkButton" EditText="<IMG SRC='images/butt-edit.gif' border=0 title='Edit' >"
															CancelText="<IMG SRC='images/butt-cancel.gif' border=0 title='Cancel' >" UpdateText="<IMG SRC='images/butt-update.gif' border=0 title='Update' >">
															<HEADERSTYLE Width="5%" CssClass="gridHeading"></HEADERSTYLE>
															<ITEMSTYLE HorizontalAlign="Left"></ITEMSTYLE>
														</ASP:EDITCOMMANDCOLUMN>
														<ASP:BUTTONCOLUMN Text="<IMG SRC='images/butt-delete.gif' border=0 title='Delete' >" CommandName="Delete">
															<HEADERSTYLE Width="5%" CssClass="gridHeading" HorizontalAlign="Center"></HEADERSTYLE>
															<ITEMSTYLE HorizontalAlign="Center"></ITEMSTYLE>
														</ASP:BUTTONCOLUMN>
														<ASP:TEMPLATECOLUMN HeaderText="VAS Code">
															<HEADERSTYLE Width="15%" CssClass="gridHeading" Font-Bold="True"></HEADERSTYLE>
															<ITEMSTYLE Wrap="False"></ITEMSTYLE>
															<ITEMTEMPLATE>
																<asp:Label id=lblVASCode CssClass="gridLabel" Text='<%#DataBinder.Eval(Container.DataItem,"vas_code")%>' Runat="server">
																</asp:Label>
															</ITEMTEMPLATE>
															<EDITITEMTEMPLATE>
																<ASP:DROPDOWNLIST id="ddlVASCode" Width="86px" CssClass="gridDropDown" Runat="server" OnSelectedIndexChanged="ddlVASCode_SelectedIndexChanged"
																	AutoPostBack="True"></ASP:DROPDOWNLIST>
															</EDITITEMTEMPLATE>
														</ASP:TEMPLATECOLUMN>
														<ASP:TEMPLATECOLUMN HeaderText="VAS Description">
															<HEADERSTYLE Width="35%" CssClass="gridHeading" Wrap="False"></HEADERSTYLE>
															<ITEMTEMPLATE>
																<asp:Label id=lblVASDesc CssClass="gridLabel" Text='<%#DataBinder.Eval(Container.DataItem,"description")%>' Runat="server">
																</asp:Label>
															</ITEMTEMPLATE>
															<EDITITEMTEMPLATE>
																<asp:TextBox id=txtVASDesc CssClass="gridTextBox" Text='<%#DataBinder.Eval(Container.DataItem,"description")%>' Runat="server" MaxLength="200">
																</asp:TextBox>
															</EDITITEMTEMPLATE>
														</ASP:TEMPLATECOLUMN>
														<ASP:TEMPLATECOLUMN HeaderText="Surcharge">
															<HEADERSTYLE Width="10%" CssClass="gridHeading"></HEADERSTYLE>
															<ITEMSTYLE Wrap="False"></ITEMSTYLE>
															<ITEMTEMPLATE>
																<asp:Label id=lblSurcharge CssClass="gridLabel" Text='<%#DataBinder.Eval(Container.DataItem,"surcharge",(String)ViewState["m_format"])%>' Runat="server">
																</asp:Label>
															</ITEMTEMPLATE>
															<EDITITEMTEMPLATE>
																<CC1:MSTEXTBOX id=txtSurcharge CssClass="gridTextBoxNumber" Text='<%#DataBinder.Eval(Container.DataItem,"surcharge",(String)ViewState["m_format"])%>' Runat="server" TextMaskType="msNumeric" MaxLength="9" Enabled="True" NumberScale="2" NumberPrecision="8" NumberMinValue="0" NumberMaxValue="10000">
																</CC1:MSTEXTBOX>
															</EDITITEMTEMPLATE>
														</ASP:TEMPLATECOLUMN>
														<ASP:TEMPLATECOLUMN HeaderText="Percent">
															<HEADERSTYLE Width="10%" CssClass="gridHeading"></HEADERSTYLE>
															<ITEMSTYLE Wrap="False"></ITEMSTYLE>
															<ITEMTEMPLATE>
																<asp:Label id=lblPercent CssClass="gridLabel" Text='<%#DataBinder.Eval(Container.DataItem,"percent","{0:n}")%>' Runat="server">
																</asp:Label>
															</ITEMTEMPLATE>
															<EDITITEMTEMPLATE>
																<CC1:MSTEXTBOX id=txtPercent CssClass="gridTextBoxNumber" Text='<%#DataBinder.Eval(Container.DataItem,"percent","{0:n}")%>' Runat="server" TextMaskType="msNumericCustVAS" MaxLength="9" Enabled="True" NumberScale="2" NumberPrecision="8" NumberMaxValue="300" NumberMinValueCustVAS="0.01">
																</CC1:MSTEXTBOX>
															</EDITITEMTEMPLATE>
														</ASP:TEMPLATECOLUMN>
														<ASP:TEMPLATECOLUMN HeaderText="Min Surch" HeaderStyle-Wrap="False">
															<HEADERSTYLE Width="10%" CssClass="gridHeading"></HEADERSTYLE>
															<ITEMSTYLE Wrap="False"></ITEMSTYLE>
															<ITEMTEMPLATE>
																<asp:Label id=lblMinSurch CssClass="gridLabel" Text='<%#DataBinder.Eval(Container.DataItem,"min_surcharge",(String)ViewState["m_format"])%>' Runat="server">
																</asp:Label>
															</ITEMTEMPLATE>
															<EDITITEMTEMPLATE>
																<CC1:MSTEXTBOX id=txtMinSurch CssClass="gridTextBoxNumber" Text='<%#DataBinder.Eval(Container.DataItem,"min_surcharge",(String)ViewState["m_format"])%>' Runat="server" TextMaskType="msNumeric" MaxLength="9" Enabled="True" NumberScale="2" NumberPrecision="8" NumberMinValue="0" NumberMaxValue="10000">
																</CC1:MSTEXTBOX>
															</EDITITEMTEMPLATE>
														</ASP:TEMPLATECOLUMN>
														<ASP:TEMPLATECOLUMN HeaderText="Max Surch" HeaderStyle-Wrap="False">
															<HEADERSTYLE Width="10%" CssClass="gridHeading"></HEADERSTYLE>
															<ITEMSTYLE Wrap="False"></ITEMSTYLE>
															<ITEMTEMPLATE>
																<asp:Label id=lblMaxSurch CssClass="gridLabel" Text='<%#DataBinder.Eval(Container.DataItem,"max_surcharge",(String)ViewState["m_format"])%>' Runat="server">
																</asp:Label>
															</ITEMTEMPLATE>
															<EDITITEMTEMPLATE>
																<CC1:MSTEXTBOX id=txtMaxSurch CssClass="gridTextBoxNumber" Text='<%#DataBinder.Eval(Container.DataItem,"max_surcharge",(String)ViewState["m_format"])%>' Runat="server" TextMaskType="msNumeric" MaxLength="9" Enabled="True" NumberScale="2" NumberPrecision="8" NumberMinValue="0" NumberMaxValue="10000">
																</CC1:MSTEXTBOX>
															</EDITITEMTEMPLATE>
														</ASP:TEMPLATECOLUMN>
													</COLUMNS>
													<PAGERSTYLE CssClass="normalText" HorizontalAlign="Right" PrevPageText="Previous" NextPageText="Next"></PAGERSTYLE>
												</ASP:DATAGRID></TD>
										</TR>
									</TABLE>
								</FIELDSET>
							</TD>
						</TR>
					</TABLE>
				</IEWC:PAGEVIEW>
			</iewc:multipage><asp:validationsummary style="Z-INDEX: 113; POSITION: absolute; TOP: 12px; LEFT: 303px" id="custValidationSummary"
				Width="346px" Height="36px" ShowSummary="False" DisplayMode="BulletList" ShowMessageBox="True" HeaderText="Please enter the missing fields."
				Runat="server"></asp:validationsummary><asp:label style="Z-INDEX: 114; POSITION: absolute; TOP: 104px; LEFT: 26px" id="lblCPErrorMessage"
				runat="server" Width="585px" Height="19px" CssClass="errorMsgColor"></asp:label>
			<!--/FORM--></FORM>
	</BODY>
</HTML>
