<%@ Page language="c#" Codebehind="SalesTerritoryReportQuery.aspx.cs" AutoEventWireup="false" Inherits="com.ties.SalesTerritoryReportQuery" %>
<%@ Register TagPrefix="DbCombo" Namespace="Cambro.Web.DbCombo" Assembly="Cambro.Web.DbCombo" %>
<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<%@ Import Namespace="Cambro.Web.DbCombo" %>
<%@ Import Namespace="com.common.util" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Service Quality Indicator Report</title>
		<LINK rel="stylesheet" type="text/css" href="css/Styles.css" name="stylesheet">
		<meta name="GENERATOR" content="Microsoft Visual Studio 7.0">
		<meta name="CODE_LANGUAGE" content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<!--#INCLUDE FILE="msFormValidations.inc"-->
		<script language="javascript" src="Scripts/settingScrollPosition.js"></script>
				<script language="javascript">
			function makeUppercase(objId)
			{
				document.getElementById(objId).value = document.getElementById(objId).value.toUpperCase();
			}
		</script>
	</HEAD>
	<body onclick="GetScrollPosition();" onunload="window.top.displayBanner.fnCloseAll(0);"
		onload="SetScrollPosition();" MS_POSITIONING="GridLayout">
		<form id="SalesTerritoryReportQuery" method="post" runat="server">
			<asp:button style="Z-INDEX: 100; POSITION: absolute; TOP: 40px; LEFT: 20px" id="btnQuery" runat="server"
				Width="64px" CssClass="queryButton" Text="Query"></asp:button>
			<TABLE style="Z-INDEX: 107; POSITION: absolute; WIDTH: 800px; TOP: 90px; LEFT: 20px" id="tblExternal"
				border="0" width="800" runat="server">
				<TR>
					<TD style="WIDTH: 401px; HEIGHT: 137px" vAlign="top">
						<FIELDSET style="WIDTH: 390px; HEIGHT: 129px"><LEGEND><asp:label id="Label23" runat="server" Width="20px" CssClass="tableHeadingFieldset" Font-Bold="True">Dates</asp:label></LEGEND>
							<TABLE style="WIDTH: 360px; HEIGHT: 91px" id="Table4" border="0" cellSpacing="0" cellPadding="0"
								align="left" runat="server">
								<TR>
									<TD style="HEIGHT: 22px"></TD>
									<TD style="HEIGHT: 22px" colSpan="3">&nbsp;
										<asp:radiobutton id="rbBookingDateCust" runat="server" Width="115px" CssClass="tableRadioButton"
											Text="Booking Date" Height="22px" AutoPostBack="True" GroupName="QueryByDate" Checked="True"></asp:radiobutton><asp:radiobutton id="rbPickUpDateCust" runat="server" Width="180px" CssClass="tableRadioButton" Text="Actual Pickup Date"
											Height="22px" AutoPostBack="True" GroupName="QueryByDate"></asp:radiobutton></TD>
									<TD style="HEIGHT: 22px"></TD>
								</TR>
								<TR>
									<TD></TD>
									<TD colSpan="3">&nbsp;
										<asp:radiobutton id="rbMonthCust" runat="server" Width="75px" CssClass="tableRadioButton" Text="Month"
											Height="21px" AutoPostBack="True" GroupName="EnterDate" Checked="True"></asp:radiobutton>&nbsp;
										<asp:dropdownlist id="ddMonthCust" runat="server" Width="88px" CssClass="textField" Height="19px"></asp:dropdownlist>&nbsp;
										<asp:label id="Label22" runat="server" Width="30px" CssClass="tableLabel" Height="22px">Year</asp:label>&nbsp;
										<cc1:mstextbox id="txtYearCust" runat="server" Width="83" CssClass="textField" MaxLength="5" TextMaskType="msNumeric"
											NumberMaxValue="2999" NumberMinValue="1" NumberPrecision="4"></cc1:mstextbox></TD>
									<TD></TD>
								</TR>
								<TR>
									<TD style="HEIGHT: 20px"></TD>
									<TD style="HEIGHT: 20px" colSpan="3">&nbsp;
										<asp:radiobutton id="rbPeriodCust" runat="server" Width="76px" CssClass="tableRadioButton" Text="Period"
											AutoPostBack="True" GroupName="EnterDate"></asp:radiobutton>&nbsp;
										<cc1:mstextbox id="txtPeriodCust" runat="server" Width="82" CssClass="textField" MaxLength="10"
											TextMaskType="msDate" TextMaskString="99/99/9999"></cc1:mstextbox>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
										&nbsp;
										<cc1:mstextbox id="txtToCust" runat="server" Width="83" CssClass="textField" MaxLength="10" TextMaskType="msDate"
											TextMaskString="99/99/9999"></cc1:mstextbox></TD>
									<TD style="HEIGHT: 20px"></TD>
								</TR>
								<TR>
									<TD></TD>
									<TD colSpan="3">&nbsp;
										<asp:radiobutton id="rbDateCust" runat="server" Width="69px" CssClass="tableRadioButton" Text="Date"
											Height="22px" AutoPostBack="True" GroupName="EnterDate"></asp:radiobutton>&nbsp;&nbsp;
										<cc1:mstextbox id="txtDateCust" runat="server" Width="82" CssClass="textField" MaxLength="10" TextMaskType="msDate"
											TextMaskString="99/99/9999"></cc1:mstextbox></TD>
									<TD></TD>
								</TR>
							</TABLE>
						</FIELDSET>
					</TD>
					<TD vAlign="top">
						<FIELDSET style="WIDTH: 320px; HEIGHT: 121px"><LEGEND><asp:label id="Label14" runat="server" Width="79px" CssClass="tableHeadingFieldset" Font-Bold="True">Destination</asp:label></LEGEND>
							<TABLE style="WIDTH: 300px; HEIGHT: 57px" id="Table7" border="0" cellSpacing="0" cellPadding="0"
								align="left" runat="server">
								<TR height="37">
									<TD style="HEIGHT: 33px"></TD>
									<TD style="WIDTH: 360px; HEIGHT: 33px" class="tableLabel" colSpan="3">&nbsp;
										<asp:label id="Label13" runat="server" Width="85px" CssClass="tableLabel" Height="22px">Postal Code</asp:label>&nbsp;&nbsp;&nbsp;
										<cc1:mstextbox id="txtZipCodeCust" runat="server" Width="139px" CssClass="textField" MaxLength="10"
											TextMaskType="msUpperAlfaNumericWithUnderscore"></cc1:mstextbox>&nbsp;&nbsp;
										<asp:button id="btnZipCodeCust" runat="server" CssClass="searchButton" Text="..." CausesValidation="False"></asp:button></TD>
									<TD style="HEIGHT: 33px"></TD>
								</TR>
								<TR>
									<TD style="HEIGHT: 33px"></TD>
									<TD style="WIDTH: 360px; HEIGHT: 33px" class="tableLabel" colSpan="3">&nbsp;
										<asp:label id="Label12" runat="server" Width="100px" CssClass="tableLabel" Height="22px"> Province</asp:label><cc1:mstextbox id="txtStateCodeCust" runat="server" Width="139" CssClass="textField" Height="22"
											MaxLength="10" TextMaskType="msUpperAlfaNumericWithUnderscore"></cc1:mstextbox>&nbsp;&nbsp;
										<asp:button id="btnStateCodeCust" runat="server" CssClass="searchButton" Text="..." CausesValidation="False"></asp:button></TD>
									<TD style="HEIGHT: 33px"></TD>
								</TR>
							</TABLE>
						</FIELDSET>
					</TD>
				</TR>
			</TABLE>
			<asp:label style="Z-INDEX: 105; POSITION: absolute; TOP: 69px; LEFT: 24px" id="lblErrorMessage"
				runat="server" Width="700px" CssClass="errorMsgColor" Height="17px">Place holder for err msg</asp:label>&nbsp;
			<asp:button style="Z-INDEX: 101; POSITION: absolute; TOP: 40px; LEFT: 90px" id="btnExecuteQuery"
				runat="server" Width="123px" CssClass="queryButton" Text="Execute Query"></asp:button>
			<TABLE style="Z-INDEX: 104; POSITION: absolute; WIDTH: 800px; TOP: 90px; LEFT: 21px" id="tblInternal"
				border="0" width="800" runat="server">
				<TR>
					<TD style="WIDTH: 401px; HEIGHT: 137px" vAlign="top">
						<fieldset style="WIDTH: 390px; HEIGHT: 129px"><legend><asp:label id="lblDates" runat="server" Width="20px" CssClass="tableHeadingFieldset" Font-Bold="True">Dates</asp:label></legend>
							<TABLE style="WIDTH: 360px; HEIGHT: 91px" id="tblDates" border="0" cellSpacing="0" cellPadding="0"
								align="left" runat="server">
								<TR>
									<td style="HEIGHT: 22px"></td>
									<TD style="HEIGHT: 22px" colSpan="3">&nbsp;
										<asp:radiobutton id="rbBookingDate" runat="server" Width="115px" CssClass="tableRadioButton" Text="Booking Date"
											Height="22px" AutoPostBack="True" GroupName="QueryByDate" Checked="True"></asp:radiobutton><asp:radiobutton id="rbPickUpDate" runat="server" Width="180px" CssClass="tableRadioButton" Text="Actual Pickup Date"
											Height="22px" AutoPostBack="True" GroupName="QueryByDate"></asp:radiobutton></TD>
									<td style="HEIGHT: 22px"></td>
								</TR>
								<TR>
									<td></td>
									<TD colSpan="3">&nbsp;
										<asp:radiobutton id="rbMonth" runat="server" Width="73px" CssClass="tableRadioButton" Text="Month"
											Height="21px" AutoPostBack="True" GroupName="EnterDate" Checked="True"></asp:radiobutton>&nbsp;
										<asp:dropdownlist id="ddMonth" runat="server" Width="88px" CssClass="textField" Height="19px"></asp:dropdownlist>&nbsp;
										<asp:label id="lblYear" runat="server" Width="30px" CssClass="tableLabel" Height="22px">Year</asp:label>&nbsp;
										<cc1:mstextbox id="txtYear" runat="server" Width="83" CssClass="textField" MaxLength="5" TextMaskType="msNumeric"
											NumberMaxValue="2999" NumberMinValue="1" NumberPrecision="4"></cc1:mstextbox></TD>
									<td></td>
								</TR>
								<TR>
									<td style="HEIGHT: 20px"></td>
									<TD style="HEIGHT: 20px" colSpan="3">&nbsp;
										<asp:radiobutton id="rbPeriod" runat="server" Width="62px" CssClass="tableRadioButton" Text="Period"
											AutoPostBack="True" GroupName="EnterDate"></asp:radiobutton>&nbsp;&nbsp;&nbsp;
										<cc1:mstextbox id="txtPeriod" runat="server" Width="82" CssClass="textField" MaxLength="10" TextMaskType="msDate"
											TextMaskString="99/99/9999"></cc1:mstextbox>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
										&nbsp;
										<cc1:mstextbox id="txtTo" runat="server" Width="83" CssClass="textField" MaxLength="10" TextMaskType="msDate"
											TextMaskString="99/99/9999"></cc1:mstextbox></TD>
									<td style="HEIGHT: 20px"></td>
								</TR>
								<TR>
									<td></td>
									<TD colSpan="3">&nbsp;
										<asp:radiobutton id="rbDate" runat="server" Width="74px" CssClass="tableRadioButton" Text="Date"
											Height="22px" AutoPostBack="True" GroupName="EnterDate"></asp:radiobutton>&nbsp;&nbsp;<cc1:mstextbox id="txtDate" runat="server" Width="82" CssClass="textField" MaxLength="10" TextMaskType="msDate"
											TextMaskString="99/99/9999"></cc1:mstextbox></TD>
									<td></td>
								</TR>
							</TABLE>
						</fieldset>
						<FIELDSET style="WIDTH: 390px; HEIGHT: 81px"><LEGEND><asp:label id="Label15" runat="server" Width="48px" CssClass="tableHeadingFieldset" Font-Bold="True">Pickup</asp:label></LEGEND>
							<TABLE style="WIDTH: 360px; HEIGHT: 95px" id="Table3" border="0" cellSpacing="0" cellPadding="0"
								align="left" runat="server">
								<TR>
									<TD class="tableLabel" width="20%" align="left">&nbsp;
										<asp:label id="Label11" runat="server" Width="111px" CssClass="tableLabel" Height="22px">Route Code</asp:label></TD>
									<TD><DBCOMBO:DBCOMBO id="dbComboRoutePath" tabIndex="14" Width="25px" CssClass="tableLabel" AutoPostBack="True"
											RegistrationKey=" " Runat="server" ServerMethod="DbComboPickupPathCodeSelect" ShowDbComboLink="False"
											TextUpLevelSearchButton="v" TextBoxColumns="18"></DBCOMBO:DBCOMBO></TD>
								</TR>
								<TR>
									<TD class="tableLabel" align="left">&nbsp;
										<asp:label id="Label10" runat="server" Width="84px" CssClass="tableLabel" Height="22px">Postal Code</asp:label></TD>
									<TD><cc1:mstextbox id="txtRouteZipcode" runat="server" Width="139px" CssClass="textField" MaxLength="10"
											TextMaskType="msUpperAlfaNumericWithUnderscore"></cc1:mstextbox>&nbsp;
										<asp:button id="btnRouteZipCode" runat="server" CssClass="searchButton" Text="..." CausesValidation="False"></asp:button></TD>
								</TR>
								<TR>
									<TD class="tableLabel" align="left">&nbsp;<asp:label id="Label16" runat="server" Width="100px" CssClass="tableLabel" Height="22px">State / Province</asp:label></TD>
									<TD><cc1:mstextbox id="txtRouteStateCode" runat="server" Width="139" CssClass="textField" MaxLength="10"
											TextMaskType="msUpperAlfaNumericWithUnderscore"></cc1:mstextbox>&nbsp;
										<asp:button id="btnRouteStateCode" runat="server" CssClass="searchButton" Text="..." CausesValidation="False"></asp:button></TD>
								</TR>
							</TABLE>
						</FIELDSET>
					</TD>
					<TD vAlign="top">
						<fieldset style="WIDTH: 320px; HEIGHT: 196px"><legend><asp:label id="lblPayerType" runat="server" Width="79px" CssClass="tableHeadingFieldset" Font-Bold="True">Payer Type</asp:label></legend>
							<TABLE style="WIDTH: 300px; HEIGHT: 73px" id="tblPayerType" border="0" cellSpacing="0"
								cellPadding="0" align="left" runat="server">
								<tr>
									<td></td>
									<td height="1" colSpan="2">&nbsp;</td>
									<td></td>
									<td></td>
								</tr>
								<TR>
									<td></td>
									<TD class="tableLabel" vAlign="top" colSpan="2">&nbsp;
										<asp:label id="Label4" runat="server" Width="79px" CssClass="tableLabel" Height="22px">Payer Type</asp:label></TD>
									<td><asp:listbox id="lsbCustType" runat="server" Width="137px" Height="61px" AutoPostBack="True"
											SelectionMode="Multiple"></asp:listbox></td>
									<td></td>
								</TR>
								<TR height="33">
									<td bgColor="blue"></td>
									<TD class="tableLabel" vAlign="middle" colSpan="2">&nbsp;
										<asp:label id="lblPayerCode" runat="server" Width="79px" CssClass="tableLabel" Height="22px">Payer Code</asp:label>&nbsp;&nbsp;</TD>
									<td><asp:textbox id="txtPayerCode" runat="server" Width="148px" CssClass="textField"></asp:textbox>&nbsp;&nbsp;
										<asp:button id="btnPayerCode" runat="server" CssClass="searchButton" Text="..." CausesValidation="False"></asp:button></td>
									<td></td>
								</TR>
								<TR height="33">
									<td bgColor="blue"></td>
									<TD class="tableLabel" vAlign="middle" colSpan="2">&nbsp;
										<asp:label id="Label7" runat="server" Width="86px" CssClass="tableLabel" Height="22px">Discount Band</asp:label>&nbsp;&nbsp;</TD>
									<td><dbcombo:dbcombo id="DbComboDiscountBand" tabIndex="1" runat="server" Width="80px" AutoPostBack="True"
											ServerMethod="DiscountBandServerMethod" ShowDbComboLink="False" TextUpLevelSearchButton="F" TextBoxColumns="4"
											Debug="false"></dbcombo:dbcombo></td>
									<td></td>
								</TR>
								<TR>
									<TD bgColor="#0000ff" vAlign="middle"></TD>
									<TD class="tableLabel" vAlign="top" colSpan="2"><FONT face="Tahoma">&nbsp;
											<asp:label id="Label9" runat="server" Width="95px" CssClass="tableLabel" Height="22px">Master Account</asp:label></FONT></TD>
									<TD><dbcombo:dbcombo id="DbComboMasterAccount" tabIndex="1" runat="server" Width="80px" AutoPostBack="True"
											ServerMethod="MasterAccountServerMethod" ShowDbComboLink="False" TextUpLevelSearchButton="F"
											TextBoxColumns="4" Debug="false"></dbcombo:dbcombo></TD>
									<TD></TD>
								</TR>
							</TABLE>
						</fieldset>
					</TD>
				</TR>
				<TR>
					<TD style="WIDTH: 401px" vAlign="top">
						<fieldset style="WIDTH: 390px; HEIGHT: 120px"><legend><asp:label id="lblRouteType" runat="server" Width="145px" CssClass="tableHeadingFieldset" Font-Bold="True">Route / DC Selection</asp:label></legend>
							<TABLE style="WIDTH: 360px; HEIGHT: 47px" id="tblRouteType" border="0" cellSpacing="0"
								cellPadding="0" align="left" runat="server">
								<TR>
									<TD class="tableLabel" colSpan="2">&nbsp;
										<asp:radiobutton id="rbLongRoute" runat="server" Width="106px" CssClass="tableRadioButton" Text="Linehaul"
											Height="22px" AutoPostBack="True" GroupName="QueryByRoute" Checked="True"></asp:radiobutton><asp:radiobutton id="rbShortRoute" runat="server" Width="122px" CssClass="tableRadioButton" Text="Delivery/CL"
											Height="22px" AutoPostBack="True" GroupName="QueryByRoute"></asp:radiobutton><asp:radiobutton id="rbAirRoute" runat="server" Width="120px" CssClass="tableRadioButton" Text="Air Route"
											Height="22px" AutoPostBack="True" GroupName="QueryByRoute"></asp:radiobutton></TD>
								</TR>
								<tr>
									<TD class="tableLabel" width="20%">&nbsp;<asp:label id="lblRouteCode" runat="server" Width="111px" CssClass="tableLabel" Height="22px">Route Code</asp:label></TD>
									<td><DBCOMBO:DBCOMBO id="DbComboPathCode" tabIndex="14" Width="25px" CssClass="tableLabel" AutoPostBack="True"
											RegistrationKey=" " Runat="server" ServerMethod="DbComboPathCodeSelect" ShowDbComboLink="False"
											TextUpLevelSearchButton="v" TextBoxColumns="18"></DBCOMBO:DBCOMBO></td>
								</tr>
								<TR>
									<TD class="tableLabel">&nbsp;<asp:label id="Label2" runat="server" Width="154px" CssClass="tableLabel" Height="22px">Origin Distribution Center</asp:label></TD>
									<TD><DBCOMBO:DBCOMBO id="DbComboOriginDC" tabIndex="14" Width="25px" CssClass="tableLabel" AutoPostBack="True"
											RegistrationKey=" " Runat="server" ServerMethod="DbComboDistributionCenterSelect" ShowDbComboLink="False"
											TextUpLevelSearchButton="v" TextBoxColumns="18"></DBCOMBO:DBCOMBO></TD>
								</TR>
								<TR>
									<TD class="tableLabel">&nbsp;<asp:label id="Label3" runat="server" Width="181px" CssClass="tableLabel" Height="22px">Destination Distribution Center</asp:label></TD>
									<TD><DBCOMBO:DBCOMBO id="DbComboDestinationDC" tabIndex="14" Width="25px" CssClass="tableLabel" AutoPostBack="True"
											RegistrationKey=" " Runat="server" ServerMethod="DbComboDistributionCenterSelect" ShowDbComboLink="False"
											TextUpLevelSearchButton="v" TextBoxColumns="18"></DBCOMBO:DBCOMBO></TD>
								</TR>
							</TABLE>
						</fieldset>
					</TD>
					<TD vAlign="top">
						<fieldset style="WIDTH: 320px; HEIGHT: 121px"><legend><asp:label id="lblDestination" runat="server" Width="79px" CssClass="tableHeadingFieldset"
									Font-Bold="True">Destination</asp:label></legend>
							<TABLE style="WIDTH: 300px; HEIGHT: 57px" id="tblDestination" border="0" cellSpacing="0"
								cellPadding="0" align="left" runat="server">
								<tr height="37">
									<td style="HEIGHT: 33px"></td>
									<TD style="WIDTH: 360px; HEIGHT: 33px" class="tableLabel" colSpan="3">&nbsp;
										<asp:label id="lblZipCode" runat="server" Width="84px" CssClass="tableLabel" Height="22px">Postal Code</asp:label>&nbsp;&nbsp;&nbsp;
										<cc1:mstextbox id="txtZipCode" runat="server" Width="139px" CssClass="textField" MaxLength="10"
											TextMaskType="msUpperAlfaNumericWithUnderscore"></cc1:mstextbox>&nbsp;&nbsp;
										<asp:button id="btnZipCode" runat="server" CssClass="searchButton" Text="..." CausesValidation="False"></asp:button></TD>
									<td style="HEIGHT: 33px"></td>
								</tr>
								<tr>
									<td style="HEIGHT: 33px"></td>
									<TD style="WIDTH: 360px; HEIGHT: 33px" class="tableLabel" colSpan="3">&nbsp;
										<asp:label id="lblStateCode" runat="server" Width="100px" CssClass="tableLabel" Height="22px">State / Province</asp:label><cc1:mstextbox id="txtStateCode" runat="server" Width="139" CssClass="textField" Height="22" MaxLength="10"
											TextMaskType="msUpperAlfaNumericWithUnderscore"></cc1:mstextbox>&nbsp;&nbsp;
										<asp:button id="btnStateCode" runat="server" CssClass="searchButton" Text="..." CausesValidation="False"></asp:button></TD>
									<td style="HEIGHT: 33px"></td>
								</tr>
							</TABLE>
						</fieldset>
					</TD>
				</TR>
				<TR>
					<TD style="WIDTH: 401px" vAlign="top">
						<fieldset style="WIDTH: 390px; HEIGHT: 50px"><legend><asp:label id="Label5" runat="server" Width="111px" CssClass="tableHeadingFieldset" Font-Bold="True">Salesman ID</asp:label></legend>
							<TABLE style="WIDTH: 347px" id="Table1" border="0" cellSpacing="0" cellPadding="0" align="left"
								runat="server">
								<tr>
									<td>&nbsp;
										<asp:label id="Label6" runat="server" Width="163px" CssClass="tableLabel">Salesman ID</asp:label></td>
									<TD style="WIDTH: 197px" class="tableLabel" colSpan="3">&nbsp;&nbsp;
										<DBCOMBO:DBCOMBO id="DbSalesman" tabIndex="14" Width="25px" CssClass="tableLabel" AutoPostBack="True"
											RegistrationKey=" " Runat="server" ServerMethod="SalesmanQuery" ShowDbComboLink="False" TextUpLevelSearchButton="v"
											TextBoxColumns="18"></DBCOMBO:DBCOMBO></TD>
								</tr>
							</TABLE>
						</fieldset>
					</TD>
					<TD style="WIDTH: 400px" vAlign="top">
						<FIELDSET style="WIDTH: 320px; HEIGHT: 50px"><LEGEND><asp:label id="Label8" runat="server" Width="111px" CssClass="tableHeadingFieldset" Font-Bold="True">Service Type</asp:label></LEGEND>
							<TABLE style="WIDTH: 312px" id="Table2" border="0" cellSpacing="0" cellPadding="0" align="left"
								runat="server">
								<TR>
									<TD>&nbsp;
										<asp:label id="lblServiceCode" runat="server" Width="100px" CssClass="tableLabel">Service Code</asp:label></TD>
									<TD style="WIDTH: 197px" class="tableLabel" colSpan="3"><dbcombo:dbcombo id="dbCmbServiceCode" tabIndex="1" runat="server" Width="80px" AutoPostBack="True"
											ServerMethod="ServiceCodeServerMethod" ShowDbComboLink="False" TextUpLevelSearchButton="v" TextBoxColumns="4"></dbcombo:dbcombo></TD>
								</TR>
							</TABLE>
						</FIELDSET>
					</TD>
				</TR>
			</TABLE>
			<asp:label style="Z-INDEX: 102; POSITION: absolute; TOP: 4px; LEFT: 20px" id="Label1" runat="server"
				Width="365px" CssClass="maintitleSize" Height="27px">Revenue Cost Report</asp:label></TR></TABLE></TR></TABLE><input 
style="Z-INDEX: 106; POSITION: absolute; TOP: 35px; LEFT: 222px" 
value="<%=strScrollPosition%>" type=hidden name=ScrollPosition>
		</form>
	</body>
</HTML>
