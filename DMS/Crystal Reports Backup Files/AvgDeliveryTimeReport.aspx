<%@ Page language="c#" Codebehind="AvgDeliveryTimeReport.aspx.cs" AutoEventWireup="false" Inherits="com.ties.AvgDeliveryTimeReport" %>
<%@ Register TagPrefix="dbCombo" Namespace="Cambro.Web.DbCombo" Assembly="Cambro.Web.DbCombo" %>
<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<%@ Import Namespace="Cambro.Web.DbCombo" %>
<%@ Import Namespace="com.common.util" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>AvgDeliveryTimeReport</title>
		<LINK href="css/Styles.css" type="text/css" rel="stylesheet" name="stylesheet">
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<!--#INCLUDE FILE="msFormValidations.inc"-->
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="AvgDeliveryTimeReport" method="post" runat="server">
			<asp:button id="btnQuery" style="Z-INDEX: 101; LEFT: 19px; POSITION: absolute; TOP: 54px" tabIndex="13" runat="server" Height="20" CssClass="queryButton" Width="64px" Text="Query"></asp:button><asp:label id="lblErrorMessage" style="Z-INDEX: 104; LEFT: 20px; POSITION: absolute; TOP: 79px" runat="server" Height="21px" CssClass="errorMsgColor" Width="528px">Place holder for err msg</asp:label>&nbsp;
			<asp:button id="btnGenerate" style="Z-INDEX: 102; LEFT: 83px; POSITION: absolute; TOP: 54px" tabIndex="12" runat="server" Height="20" CssClass="queryButton" Width="82px" Text="Generate"></asp:button><asp:label id="lblTitle" style="Z-INDEX: 103; LEFT: 20px; POSITION: absolute; TOP: 10px" runat="server" Height="27px" CssClass="maintitleSize" Width="529px">Average Delivery Time Report</asp:label>
			<TABLE id="tblCustAgent" style="Z-INDEX: 105; LEFT: 18px; WIDTH: 720px; POSITION: absolute; TOP: 114px; HEIGHT: 324px" width="720" align="left" border="0" runat="server">
				<TR>
					<TD style="WIDTH: 400px; HEIGHT: 137px" vAlign="top">
						<FIELDSET style="WIDTH: 390px; HEIGHT: 129px"><LEGEND><asp:label id="lblDates" runat="server" CssClass="tableHeadingFieldset" Width="80px" Font-Bold="True">Booking Date</asp:label></LEGEND>
							<TABLE id="Table1" style="WIDTH: 360px; HEIGHT: 91px" cellSpacing="0" cellPadding="0" align="left" border="0" runat="server">
								<TR>
									<TD style="HEIGHT: 18px"></TD>
									<TD style="HEIGHT: 18px" colSpan="3">&nbsp;
										<asp:radiobutton id="rbMonth" runat="server" Height="21px" CssClass="tableRadioButton" Width="73px" Text="Month" Checked="True" GroupName="EnterDate" AutoPostBack="True"></asp:radiobutton>&nbsp;
										<asp:dropdownlist id="ddMonth" runat="server" Height="19px" CssClass="textField" Width="88px"></asp:dropdownlist>&nbsp;
										<asp:label id="Label3" runat="server" Height="22px" CssClass="tableLabel" Width="30px">Year</asp:label>&nbsp;
										<cc1:mstextbox id="txtYear" runat="server" CssClass="textField" Width="83" NumberPrecision="4" NumberMinValue="1" NumberMaxValue="2999" TextMaskType="msNumeric" MaxLength="5"></cc1:mstextbox></TD>
									<TD style="HEIGHT: 18px"></TD>
								</TR>
								<TR>
									<TD style="HEIGHT: 14px"></TD>
									<TD style="HEIGHT: 14px" colSpan="3">&nbsp;
										<asp:radiobutton id="rbPeriod" runat="server" CssClass="tableRadioButton" Width="62px" Text="Period" GroupName="EnterDate" AutoPostBack="True"></asp:radiobutton>&nbsp;&nbsp;&nbsp;
										<cc1:mstextbox id="txtPeriod" runat="server" CssClass="textField" Width="82" TextMaskType="msDate" MaxLength="10" TextMaskString="99/99/9999"></cc1:mstextbox>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
										&nbsp;
										<cc1:mstextbox id="txtTo" runat="server" CssClass="textField" Width="83" TextMaskType="msDate" MaxLength="10" TextMaskString="99/99/9999"></cc1:mstextbox></TD>
									<TD style="HEIGHT: 14px"></TD>
								</TR>
								<TR>
									<TD style="HEIGHT: 23px"></TD>
									<TD style="HEIGHT: 23px" colSpan="3">&nbsp;
										<asp:radiobutton id="rbDate" runat="server" Height="22px" CssClass="tableRadioButton" Width="62px" Text="Date" GroupName="EnterDate" AutoPostBack="True"></asp:radiobutton>&nbsp;&nbsp;&nbsp;
										<cc1:mstextbox id="txtDate" runat="server" CssClass="textField" Width="82" TextMaskType="msDate" MaxLength="10" TextMaskString="99/99/9999"></cc1:mstextbox></TD>
									<TD style="HEIGHT: 23px"></TD>
								</TR>
							</TABLE>
						</FIELDSET>
					</TD>
					<TD style="HEIGHT: 137px" vAlign="top">
						<FIELDSET style="WIDTH: 320px; HEIGHT: 105px"><LEGEND><asp:label id="lblPayerType" runat="server" CssClass="tableHeadingFieldset" Width="79px" Font-Bold="True">Payer Type</asp:label></LEGEND>
							<TABLE id="tblPayerType" style="WIDTH: 300px; HEIGHT: 73px" cellSpacing="0" cellPadding="0" align="left" border="0" runat="server">
								<TR>
									<TD></TD>
									<TD colSpan="2" height="1">&nbsp;</TD>
									<TD></TD>
									<TD></TD>
								</TR>
								<TR>
									<TD></TD>
									<TD class="tableLabel" vAlign="top" colSpan="2">&nbsp;
										<asp:label id="Label4" runat="server" Height="22px" CssClass="tableLabel" Width="79px">Payer Type</asp:label></TD>
									<TD><asp:listbox id="lsbCustType" runat="server" Height="61px" Width="137px" SelectionMode="Multiple"></asp:listbox></TD>
									<TD></TD>
								</TR>
								<TR height="33">
									<TD bgColor="blue"></TD>
									<TD class="tableLabel" vAlign="top" colSpan="2">&nbsp;
										<asp:label id="lblPayerCode" runat="server" Height="22px" CssClass="tableLabel" Width="79px">Payer Code</asp:label>&nbsp;&nbsp;</TD>
									<TD><asp:TextBox id="txtPayerCode" runat="server" Width="148px" CssClass="textField"></asp:TextBox><FONT face="Tahoma">&nbsp;&nbsp;</FONT>
										<asp:button id="btnPayerCode" runat="server" CssClass="searchButton" Text="..." CausesValidation="False"></asp:button></TD>
									<TD></TD>
								</TR>
							</TABLE>
						</FIELDSET>
					</TD>
				</TR>
				<TR>
					<TD style="WIDTH: 400px; HEIGHT: 134px" vAlign="top">
						<FIELDSET style="WIDTH: 390px; HEIGHT: 120px"><LEGEND><asp:label id="lblRouteType" runat="server" CssClass="tableHeadingFieldset" Width="145px" Font-Bold="True">Route / DC Selection</asp:label></LEGEND>
							<TABLE id="tblRouteType" style="WIDTH: 360px; HEIGHT: 47px" cellSpacing="0" cellPadding="0" align="left" border="0" runat="server">
								<TR>
									<TD class="tableLabel" colSpan="2">&nbsp;
										<asp:radiobutton id="rbLongRoute" runat="server" Height="22px" CssClass="tableRadioButton" Width="106px" Text="Linehaul" Checked="True" GroupName="QueryByRoute" AutoPostBack="True"></asp:radiobutton><asp:radiobutton id="rbShortRoute" runat="server" Height="22px" CssClass="tableRadioButton" Width="131px" Text="Delivery Route" GroupName="QueryByRoute" AutoPostBack="True"></asp:radiobutton><asp:radiobutton id="rbAirRoute" runat="server" Height="22px" CssClass="tableRadioButton" Width="120px" Text="Air Route" GroupName="QueryByRoute" AutoPostBack="True"></asp:radiobutton></TD>
								</TR>
								<TR>
									<TD class="tableLabel" width="20%">&nbsp;
										<asp:label id="lblRouteCode" runat="server" Height="22px" CssClass="tableLabel" Width="111px">Route Code</asp:label></TD>
									<TD><DBCOMBO:DBCOMBO id="DbComboPathCode" tabIndex="14" CssClass="tableLabel" Width="25px" Runat="server" AutoPostBack="True" ShowDbComboLink="False" TextUpLevelSearchButton="v" ServerMethod="DbComboPathCodeSelect" TextBoxColumns="18" RegistrationKey=" "></DBCOMBO:DBCOMBO></TD>
								</TR>
								<TR>
									<TD class="tableLabel">&nbsp;
										<asp:label id="Label5" runat="server" Height="22px" CssClass="tableLabel" Width="154px">Origin Distribution Center</asp:label></TD>
									<TD><DBCOMBO:DBCOMBO id="DbComboOriginDC" tabIndex="14" CssClass="tableLabel" Width="25px" Runat="server" AutoPostBack="True" ShowDbComboLink="False" TextUpLevelSearchButton="v" ServerMethod="DbComboDistributionCenterSelect" TextBoxColumns="18" RegistrationKey=" "></DBCOMBO:DBCOMBO></TD>
								</TR>
								<TR>
									<TD class="tableLabel">&nbsp;
										<asp:label id="Label6" runat="server" Height="22px" CssClass="tableLabel" Width="181px">Destination Distribution Center</asp:label></TD>
									<TD><DBCOMBO:DBCOMBO id="DbComboDestinationDC" tabIndex="14" CssClass="tableLabel" Width="25px" Runat="server" AutoPostBack="True" ShowDbComboLink="False" TextUpLevelSearchButton="v" ServerMethod="DbComboDistributionCenterSelect" TextBoxColumns="18" RegistrationKey=" "></DBCOMBO:DBCOMBO></TD>
								</TR>
							</TABLE>
						</FIELDSET>
					</TD>
					<TD style="HEIGHT: 134px" vAlign="top">
						<FIELDSET style="WIDTH: 320px; HEIGHT: 89px"><LEGEND><asp:label id="lblDestination" runat="server" CssClass="tableHeadingFieldset" Width="79px" Font-Bold="True">Destination</asp:label></LEGEND>
							<TABLE id="tblDestination" style="WIDTH: 300px; HEIGHT: 89px" cellSpacing="0" cellPadding="0" align="left" border="0" runat="server">
								<TR height="37">
									<TD></TD>
									<TD class="tableLabel" style="WIDTH: 360px" colSpan="3">&nbsp;
										<asp:label id="lblZipCode" runat="server" Height="22px" CssClass="tableLabel" Width="84px">Postal Code</asp:label>&nbsp;&nbsp;&nbsp;
										<cc1:mstextbox id="txtZipCode" runat="server" CssClass="textField" Width="139px" TextMaskType="msUpperAlfaNumericWithUnderscore" MaxLength="10"></cc1:mstextbox>&nbsp;&nbsp;
										<asp:button id="btnZipCode" runat="server" CssClass="searchButton" Text="..." CausesValidation="False"></asp:button></TD>
									<TD></TD>
								</TR>
								<TR>
									<TD></TD>
									<TD class="tableLabel" style="WIDTH: 360px" colSpan="3">&nbsp;
										<asp:label id="Label7" runat="server" Height="22px" CssClass="tableLabel" Width="100px">State / Province</asp:label><cc1:mstextbox id="txtStateCode" runat="server" Height="22" CssClass="textField" Width="139" TextMaskType="msUpperAlfaNumericWithUnderscore" MaxLength="10"></cc1:mstextbox>&nbsp;&nbsp;
										<asp:button id="btnStateCode" runat="server" CssClass="searchButton" Text="..." CausesValidation="False"></asp:button></TD>
									<TD></TD>
								</TR>
							</TABLE>
						</FIELDSET>
					</TD>
				</TR>
				<TR id="SendRecpID" runat="server">
					<TD colSpan="2">
						<FIELDSET style="WIDTH: 715px; HEIGHT: 79px"><LEGEND><asp:label id="Label2" Runat="server">Selection Criteria</asp:label></LEGEND>
							<TABLE style="WIDTH: 480px; HEIGHT: 27px">
								<TR height="27">
									<TD colSpan="4"><asp:label id="lblServiceCode" runat="server" Height="22px" CssClass="tableLabel" Width="100%">Service Code</asp:label></TD>
									<TD colSpan="7"><dbcombo:dbcombo id="dbCmbServiceCode" tabIndex="8" runat="server" Height="17px" Width="100%" AutoPostBack="True" ShowDbComboLink="False" TextUpLevelSearchButton="v" ServerMethod="ServiceCodeServerMethod" TextBoxColumns="14"></dbcombo:dbcombo></TD>
									<TD colSpan="9"></TD>
								</TR>
								<TR height="27">
									<TD colSpan="4"><asp:label id="lblZoneCode" runat="server" Height="22px" CssClass="tableLabel" Width="100%">Zone Code</asp:label></TD>
									<TD colSpan="7"><dbcombo:dbcombo id="dbCmbZoneCode" tabIndex="11" runat="server" Height="17px" Width="100%" AutoPostBack="True" ShowDbComboLink="False" TextUpLevelSearchButton="v" ServerMethod="ZoneCodeServerMethod" TextBoxColumns="10"></dbcombo:dbcombo></TD>
									<TD colSpan="9"></TD>
								</TR>
							</TABLE>
						</FIELDSET>
					</TD>
				</TR>
			</TABLE>
		</form>
		</FORM>
	</body>
</HTML>
