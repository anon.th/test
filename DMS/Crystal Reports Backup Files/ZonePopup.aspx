<%@ Page language="c#" Codebehind="ZonePopup.aspx.cs" AutoEventWireup="false" Inherits="com.ties.ZonePopup" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>ZonePopup</title>
		<LINK href="css/Styles.css" type="text/css" rel="stylesheet" name="stylesheet">
		<meta name="GENERATOR" Content="Microsoft Visual Studio 7.0">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="ZonePopup" method="post" runat="server">
			<asp:datagrid id="dgZoneMaster" style="Z-INDEX: 102; LEFT: 45px; POSITION: absolute; TOP: 96px" runat="server" OnPageIndexChanged="Paging" OnSelectedIndexChanged="dgZoneMaster_SelectedIndexChanged" AllowPaging="True" Width="522px" AutoGenerateColumns="False" BorderColor="#CCCCCC" BackColor="White" CellPadding="3" BorderWidth="1px" BorderStyle="None" CssClass="gridHeading" AllowCustomPaging="True">
				<SelectedItemStyle Font-Bold="True" ForeColor="White" CssClass="drilldownFieldSelected"></SelectedItemStyle>
				<ItemStyle CssClass="drilldownField"></ItemStyle>
				<HeaderStyle Width="10%" CssClass="drilldownHeading"></HeaderStyle>
				<FooterStyle ForeColor="#000066" BackColor="White"></FooterStyle>
				<Columns>
					<asp:BoundColumn DataField="zone_code" HeaderText="Zone Code"></asp:BoundColumn>
					<asp:BoundColumn DataField="zone_name" HeaderText="Zone Name"></asp:BoundColumn>
					<asp:ButtonColumn Text="Select" CommandName="Select"></asp:ButtonColumn>
				</Columns>
				<PagerStyle NextPageText="Next" PrevPageText="Previous" HorizontalAlign="Right" ForeColor="#000066" BackColor="White" CssClass="normalText"></PagerStyle>
			</asp:datagrid>
			<asp:textbox id="txtZoneCode" style="Z-INDEX: 107; LEFT: 45px; POSITION: absolute; TOP: 63px" runat="server" CssClass="textField" Width="116px" Height="19px"></asp:textbox>
			<asp:label id="lblZoneCode" style="Z-INDEX: 106; LEFT: 45px; POSITION: absolute; TOP: 38px" runat="server" CssClass="tableLabel" Width="100px" Height="16px" Font-Bold="True">Zone Code</asp:label>
			<asp:button id="btnSearch" style="Z-INDEX: 105; LEFT: 166px; POSITION: absolute; TOP: 63px" runat="server" Width="78" CssClass="buttonProp" Height="21" Text="Search" CausesValidation="False" Font-Bold="True"></asp:button>
			<asp:button id="btnOk" style="Z-INDEX: 101; LEFT: 245px; POSITION: absolute; TOP: 63px" runat="server" Width="78px" CssClass="buttonProp" Height="21px" Text="Close" CausesValidation="False" Font-Bold="True"></asp:button>
		</form>
	</body>
</HTML>
