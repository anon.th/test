<%@ Page language="c#" Codebehind="InvoiceCustomerPopup.aspx.cs" AutoEventWireup="false" Inherits="TIES.WebUI.InvoiceCustomerPopup" %>
<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>InvoiceCustomerPopup</title>
		<LINK rel="stylesheet" type="text/css" href="css/Styles.css" name="stylesheet">
		<meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<script language="javascript" src="Scripts/settingScrollPosition.js"></script>
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<asp:label style="Z-INDEX: 103; POSITION:  LEFT: 16px" id="lblMainTitle"
			runat="server" CssClass="mainTitleSize" Width="477px" Height="26px">Exclude Customer</asp:label>
		<form id="Form1" method="post" runat="server">
			<asp:datagrid style="Z-INDEX: 102; TOP: 56px; LEFT: 8px" id="dgPreview" runat="server"
				AutoGenerateColumns="False" Width="250px" ItemStyle-Height="20" SelectedItemStyle-CssClass="gridFieldSelected"
				Height="95px">
				<SelectedItemStyle CssClass="gridFieldSelected"></SelectedItemStyle>
				<ItemStyle Height="10px" CssClass="gridField"></ItemStyle>
				<Columns>
					<asp:TemplateColumn>
						<HeaderStyle Width="20px" CssClass="gridHeading"></HeaderStyle>
						<ItemTemplate>
							<asp:CheckBox id="chkSelect" Runat="server" ></asp:CheckBox>
						</ItemTemplate>
					</asp:TemplateColumn>
					<asp:BoundColumn DataField="payerid" HeaderText="Customer ID">
						<HeaderStyle HorizontalAlign="Center" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle HorizontalAlign="Left" CssClass="gridField"></ItemStyle>
					</asp:BoundColumn>
				</Columns>
				<PagerStyle NextPageText="Next" PrevPageText="Previous" HorizontalAlign="Left" CssClass="normalText"></PagerStyle>
			</asp:datagrid>
			<br>
			<asp:button style="Z-INDEX: 104; " id="btnExport"
				runat="server" Height="21px" Width="84px" CssClass="buttonProp" Text="Export" CausesValidation="False"></asp:button>
			<asp:button style="Z-INDEX: 101; " id="btnOk" runat="server"
				Height="21px" Width="84px" CssClass="buttonProp" Text="Close" CausesValidation="False"></asp:button>
		</form>
	</body>
</HTML>
