<%@ Page language="c#" Codebehind="Mf_Vehicle.aspx.cs" AutoEventWireup="false" Inherits="com.ties.Mf_Vehicle" %>
<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<%@ Import Namespace="com.common.util" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Mf_Vehicle</title>
		<LINK href="../css/styles.css" type="text/css" rel="stylesheet">
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<!--#INCLUDE FILE="../msFormValidations.inc"-->
		<script language="javascript" src="../Scripts/settingScrollPosition.js"></script>
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="DeliveryPath" method="post" runat="server">
			<asp:label id="lblMainheading" style="Z-INDEX: 101; LEFT: 33px; POSITION: absolute; TOP: 11px"
				runat="server" CssClass="mainTitleSize" Height="25px" Width="201px"> Vehicles</asp:label><asp:label id="lblErrorMessage" style="Z-INDEX: 106; LEFT: 35px; POSITION: absolute; TOP: 78px"
				runat="server" CssClass="errorMsgColor" Width="669px"></asp:label><asp:button id="btnInsert" style="Z-INDEX: 105; LEFT: 229px; POSITION: absolute; TOP: 47px"
				runat="server" CssClass="queryButton" Width="51px" CausesValidation="False" Text="Insert"></asp:button><asp:button id="btnExecuteQuery" style="Z-INDEX: 104; LEFT: 99px; POSITION: absolute; TOP: 47px"
				runat="server" CssClass="queryButton" CausesValidation="False" Text="ExecuteQuery"></asp:button><asp:button id="btnQuery" style="Z-INDEX: 103; LEFT: 32px; POSITION: absolute; TOP: 47px" runat="server"
				CssClass="queryButton" Width="67px" CausesValidation="False" Text="Query"></asp:button><asp:datagrid id="dgVehicle" style="Z-INDEX: 102; LEFT: 32px; POSITION: absolute; TOP: 113px"
				runat="server" Width="650px" OnDeleteCommand="dgVehicle_Delete" OnPageIndexChanged="dgVehicle_PageChange" AutoGenerateColumns="False" AllowPaging="True" AllowCustomPaging="True" OnItemDataBound="dgVehicle_ItemDataBound"
				OnCancelCommand="dgVehicle_Cancel" OnUpdateCommand="dgVehicle_Update" OnEditCommand="dgVehicle_Edit">
				<ItemStyle Height="20px" CssClass="gridfield"></ItemStyle>
				<Columns>
					<asp:EditCommandColumn ButtonType="LinkButton" UpdateText="&lt;IMG SRC='../images/butt-update.gif' border=0 title='Update' &gt;"
						CancelText="&lt;IMG SRC='../images/butt-cancel.gif' border=0 title='Cancel' &gt;" EditText="&lt;IMG SRC='../images/butt-edit.gif' border=0 title='Edit' &gt;">
						<HeaderStyle CssClass="gridHeading"></HeaderStyle>
						<ItemStyle HorizontalAlign="Left"></ItemStyle>
					</asp:EditCommandColumn>
					<asp:ButtonColumn Text="&lt;IMG SRC='../images/butt-delete.gif' border=0 title='Delete' &gt;" CommandName="Delete">
						<HeaderStyle CssClass="gridHeading"></HeaderStyle>
						<ItemStyle HorizontalAlign="Left"></ItemStyle>
					</asp:ButtonColumn>
					<asp:TemplateColumn HeaderText="Truck ID">
						<HeaderStyle Font-Bold="True" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle CssClass="gridField"></ItemStyle>
						<ItemTemplate>
							<asp:Label ID="lblTruckId" CssClass="gridLabel" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"truckid")%>'>
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<cc1:mstextbox CssClass="gridTextBox" ID="txtTruckId" Text='<%#DataBinder.Eval(Container.DataItem,"truckid")%>' Runat="server" Enabled="True" TextMaskType="msUpperAlfaNumeric" MaxLength="12">
							</cc1:mstextbox>
							<asp:RequiredFieldValidator ID="rvTruckId" Runat="server" BorderWidth="0" Display="None" ControlToValidate="txtTruckId"
								ErrorMessage="Truck ID "></asp:RequiredFieldValidator>
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Description">
						<HeaderStyle Font-Bold="True" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle CssClass="gridField"></ItemStyle>
						<ItemTemplate>
							<asp:Label ID="lblDescription" CssClass="gridLabel" Text='<%#DataBinder.Eval(Container.DataItem,"description")%>' Runat=server >
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<asp:TextBox ID="txtDescription" CssClass="gridTextBox" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"description")%>' MaxLength="200">
							</asp:TextBox>
							<asp:RequiredFieldValidator ID="rvDescription" Runat="server" BorderWidth="0" Display="None" ControlToValidate="txtDescription"
								ErrorMessage="Description"></asp:RequiredFieldValidator>
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="License Plate">
						<HeaderStyle CssClass="gridHeading"></HeaderStyle>
						<ItemStyle CssClass="gridField"></ItemStyle>
						<ItemTemplate>
							<asp:Label ID="lblLicensePlate" CssClass="gridLabel" Text='<%#DataBinder.Eval(Container.DataItem,"licenseplate")%>' Runat=server >
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<asp:TextBox ID="txtLicensePlate" CssClass="gridTextBox" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"licenseplate")%>' MaxLength="50">
							</asp:TextBox>
							<asp:RequiredFieldValidator ID="rvLicensePlate" Runat="server" BorderWidth="0" Display="None" ControlToValidate="txtLicensePlate"
								ErrorMessage="License Plate"></asp:RequiredFieldValidator>
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Type">
						<HeaderStyle CssClass="gridHeading"></HeaderStyle>
						<ItemStyle CssClass="gridField"></ItemStyle>
						<ItemTemplate>
							<asp:Label ID="lblVehType" CssClass="gridLabel" Text='<%#DataBinder.Eval(Container.DataItem,"type")%>' Runat=server>
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<asp:DropDownList id="ddlVehType" Width="50px" CssClass="gridTextBox" OnSelectedIndexChanged="chkDdlVehType"
								Runat="server" AutoPostBack="True"></asp:DropDownList>
							<asp:RequiredFieldValidator ID="rvType" Runat="server" BorderWidth="0" Display="None" ControlToValidate="ddlVehType"
								ErrorMessage="Type"></asp:RequiredFieldValidator>
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Year">
						<HeaderStyle CssClass="gridHeading"></HeaderStyle>
						<ItemStyle CssClass="gridField"></ItemStyle>
						<ItemTemplate>
							<asp:Label ID="lblYear" CssClass="gridLabel" Text='<%#DataBinder.Eval(Container.DataItem,"year")%>' Runat=server>
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<asp:DropDownList id="ddlYear" Width="80" CssClass="gridTextBox" Runat="server" AutoPostBack="True"></asp:DropDownList>
							<asp:RequiredFieldValidator ID="rvYear" Runat="server" BorderWidth="0" Display="None" ControlToValidate="ddlYear"
								ErrorMessage="Year"></asp:RequiredFieldValidator>
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Brand">
						<HeaderStyle CssClass="gridheading"></HeaderStyle>
						<ItemStyle CssClass="gridField"></ItemStyle>
						<ItemTemplate>
							<asp:Label ID="lblBrand" CssClass="gridLabel" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"brand")%>'>
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<asp:TextBox ID="txtBrand" CssClass="gridTextBox" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"brand")%>' MaxLength="50">
							</asp:TextBox>
							<asp:RequiredFieldValidator ID="rvBrand" Runat="server" BorderWidth="0" Display="None" ControlToValidate="txtBrand"
								ErrorMessage="Brand"></asp:RequiredFieldValidator>
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Model">
						<HeaderStyle CssClass="gridheading"></HeaderStyle>
						<ItemStyle CssClass="gridField"></ItemStyle>
						<ItemTemplate>
							<asp:Label ID="lblModel" CssClass="gridLabel" Width="5%" Runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"model")%>'>
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<asp:TextBox ID="txtModel" CssClass="gridTextBox" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"model")%>' MaxLength="50">
							</asp:TextBox>
							<asp:RequiredFieldValidator ID="rvModel" Runat="server" BorderWidth="0" Display="None" ControlToValidate="txtModel"
								ErrorMessage="Model"></asp:RequiredFieldValidator>
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Color">
						<HeaderStyle CssClass="gridheading"></HeaderStyle>
						<ItemStyle CssClass="gridField"></ItemStyle>
						<ItemTemplate>
							<asp:Label ID="lblColor" CssClass="gridLabel" Width="5%" Runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"color")%>'>
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<asp:DropDownList id="ddlColor" Width="100px" CssClass="gridTextBox" OnSelectedIndexChanged="chkDdlColor"
								Runat="server" AutoPostBack="True"></asp:DropDownList>
							<asp:RequiredFieldValidator ID="rvColor" Runat="server" BorderWidth="0" Display="None" ControlToValidate="ddlColor"
								ErrorMessage="Color"></asp:RequiredFieldValidator>
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Owner">
						<HeaderStyle CssClass="gridheading"></HeaderStyle>
						<ItemStyle HorizontalAlign="Right" CssClass="gridField"></ItemStyle>
						<ItemTemplate>
							<asp:Label ID="lblOwner" CssClass="gridLabel" Width="5%" Runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"owner")%>'>
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<asp:TextBox ID="txtOwner" CssClass="gridTextBox" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"owner")%>' MaxLength="100">
							</asp:TextBox>
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Type Of Lease">
						<HeaderStyle CssClass="gridHeading"></HeaderStyle>
						<ItemStyle CssClass="gridField"></ItemStyle>
						<ItemTemplate>
							<asp:Label ID="lblTypeOfLease" CssClass="gridLabel" Text='<%#DataBinder.Eval(Container.DataItem,"Type_Of_Lease")%>' Runat=server >
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<asp:TextBox ID="txtTypeOfLease" CssClass="gridTextBox" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Type_Of_Lease")%>' MaxLength="200">
							</asp:TextBox>
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Lease Start Date">
						<HeaderStyle CssClass="gridHeading"></HeaderStyle>
						<ItemStyle CssClass="gridField"></ItemStyle>
						<ItemTemplate>
							<asp:Label ID="lblLeaseStartDate" CssClass="gridLabel" Text='<%#DataBinder.Eval(Container.DataItem,"lease_start_date","{0:dd/MM/yyyy}")%>' Runat=server >
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<cc1:mstextbox id="txtLeaseStartDate" Text='<%#DataBinder.Eval(Container.DataItem,"lease_start_date","{0:dd/MM/yyyy}")%>' CssClass="textField" Width="100px" Runat="server" TextMaskType="msDate" MaxLength="16" AutoPostBack="True" Enabled="True" TextMaskString="99/99/9999">
							</cc1:mstextbox>
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Lease End Date">
						<HeaderStyle CssClass="gridHeading"></HeaderStyle>
						<ItemStyle CssClass="gridField"></ItemStyle>
						<ItemTemplate>
							<asp:Label ID="lblLeaseEndDate" CssClass="gridLabel" Text='<%#DataBinder.Eval(Container.DataItem,"lease_end_date","{0:dd/MM/yyyy}")%>' Runat=server >
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<cc1:mstextbox id="txtLeaseEndDate" Text='<%#DataBinder.Eval(Container.DataItem,"lease_end_date","{0:dd/MM/yyyy}")%>' CssClass="textField" Width="100px" Runat="server" TextMaskType="msDate" MaxLength="16" AutoPostBack="True" Enabled="True" TextMaskString="99/99/9999">
							</cc1:mstextbox>
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Monthly Cost">
						<HeaderStyle CssClass="gridHeading"></HeaderStyle>
						<ItemStyle CssClass="gridField"></ItemStyle>
						<ItemTemplate>
							<asp:Label ID="lblMonthlyCost" CssClass="gridLabelNumber" Text='<%#DataBinder.Eval(Container.DataItem,"monthly_cost","{0:#,##0.00}")%>' Runat=server >
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<cc1:mstextbox id="txtMonthlyCost" CssClass="textField" style="TEXT-ALIGN: right" Text='<%#DataBinder.Eval(Container.DataItem,"monthly_cost")%>' tabIndex="0" runat="server" Width="100px" AutoPostBack="False" MaxLength="11" NumberScale="2" TextMaskType="msNumeric" NumberMaxValue="1000000" NumberPrecision="11">
							</cc1:mstextbox>
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Base Location">
						<HeaderStyle CssClass="gridHeading"></HeaderStyle>
						<ItemStyle CssClass="gridField"></ItemStyle>
						<ItemTemplate>
							<asp:Label ID="lblBaseLocation" CssClass="gridLabel" Text='<%#DataBinder.Eval(Container.DataItem,"base_location")%>' Runat=server >
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<asp:DropDownList id="ddlBaseLocation" Width="50" CssClass="gridTextBox" Runat="server" AutoPostBack="True"></asp:DropDownList>
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Tare Weight">
						<HeaderStyle CssClass="gridHeading"></HeaderStyle>
						<ItemStyle CssClass="gridField"></ItemStyle>
						<ItemTemplate>
							<asp:Label ID="lblTareWeight" CssClass="gridLabelNumber" Text='<%#DataBinder.Eval(Container.DataItem,"tare_weight","{0:#,##0}")%>' Runat=server >
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<cc1:mstextbox id="txtTareWeight" CssClass="textField" style="TEXT-ALIGN: right" Text='<%#DataBinder.Eval(Container.DataItem,"tare_weight")%>' tabIndex="0" runat="server" Width="100px" AutoPostBack="False" MaxLength="11" NumberScale="0" TextMaskType="msNumeric" NumberMaxValue="1000000" NumberPrecision="11">
							</cc1:mstextbox>
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Gross Weight">
						<HeaderStyle CssClass="gridHeading"></HeaderStyle>
						<ItemStyle CssClass="gridField"></ItemStyle>
						<ItemTemplate>
							<asp:Label ID="lblGrossWeight" CssClass="gridLabelNumber" Text='<%#DataBinder.Eval(Container.DataItem,"gross_weight","{0:#,##0}")%>' Runat=server >
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<cc1:mstextbox id="txtGrossWeight" CssClass="textField" style="TEXT-ALIGN: right" Text='<%#DataBinder.Eval(Container.DataItem,"gross_weight")%>' tabIndex="0" runat="server" Width="100px" AutoPostBack="False" MaxLength="11" NumberScale="0" TextMaskType="msNumeric" NumberMaxValue="1000000" NumberPrecision="11">
							</cc1:mstextbox>
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Max CBM">
						<HeaderStyle CssClass="gridHeading"></HeaderStyle>
						<ItemStyle CssClass="gridField"></ItemStyle>
						<ItemTemplate>
							<asp:Label ID="lblMaxCBM" CssClass="gridLabelNumber" Text='<%#DataBinder.Eval(Container.DataItem,"max_cbm","{0:#,##0}")%>' Runat=server >
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<cc1:mstextbox id="txtMaxCBM" CssClass="textField" style="TEXT-ALIGN: right" Text='<%#DataBinder.Eval(Container.DataItem,"max_cbm")%>' tabIndex="0" runat="server" Width="100px" AutoPostBack="False" MaxLength="11" NumberScale="0" TextMaskType="msNumeric" NumberMaxValue="1000000" NumberPrecision="11">
							</cc1:mstextbox>
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Remark">
						<HeaderStyle CssClass="gridHeading"></HeaderStyle>
						<ItemStyle CssClass="gridField"></ItemStyle>
						<ItemTemplate>
							<asp:Label ID="lblRemark" CssClass="gridLabel" Text='<%#DataBinder.Eval(Container.DataItem,"remark")%>' Runat=server >
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<asp:TextBox ID="txtRemark" CssClass="gridTextBox" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"remark")%>' MaxLength="200">
							</asp:TextBox>
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Registration Date">
						<HeaderStyle CssClass="gridHeading"></HeaderStyle>
						<ItemStyle CssClass="gridField"></ItemStyle>
						<ItemTemplate>
							<asp:Label ID="lblRegistrationDate" CssClass="gridLabel" Text='<%#DataBinder.Eval(Container.DataItem,"registration_date","{0:dd/MM/yyyy}")%>' Runat=server >
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<cc1:mstextbox id="txtRegistrationDate" Text='<%#DataBinder.Eval(Container.DataItem,"registration_date","{0:dd/MM/yyyy}")%>' CssClass="textField" Width="100px" Runat="server" TextMaskType="msDate" MaxLength="16" AutoPostBack="True" Enabled="True" TextMaskString="99/99/9999">
							</cc1:mstextbox>
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Initial In Service Date">
						<HeaderStyle CssClass="gridHeading"></HeaderStyle>
						<ItemStyle CssClass="gridField"></ItemStyle>
						<ItemTemplate>
							<asp:Label ID="lblInitInServiceDate" CssClass="gridLabel" Text='<%#DataBinder.Eval(Container.DataItem,"initial_in_service_date","{0:dd/MM/yyyy}")%>' Runat=server >
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<cc1:mstextbox id="txtInitInServiceDate" Text='<%#DataBinder.Eval(Container.DataItem,"initial_in_service_date","{0:dd/MM/yyyy}")%>' CssClass="textField" Width="100px" Runat="server" TextMaskType="msDate" MaxLength="16" AutoPostBack="True" Enabled="True" TextMaskString="99/99/9999">
							</cc1:mstextbox>
							<asp:RequiredFieldValidator ID="rvInintInServiceDate" Runat="server" BorderWidth="0" Display="None" ControlToValidate="txtInitInServiceDate"
								ErrorMessage="Initial In Service Date"></asp:RequiredFieldValidator>
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Disposal Date">
						<HeaderStyle CssClass="gridHeading"></HeaderStyle>
						<ItemStyle CssClass="gridField"></ItemStyle>
						<ItemTemplate>
							<asp:Label ID="lblDisposalDate" CssClass="gridLabel" Text='<%#DataBinder.Eval(Container.DataItem,"disposal_date","{0:dd/MM/yyyy}")%>' Runat=server >
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<cc1:mstextbox id="txtDisposalDate" Text='<%#DataBinder.Eval(Container.DataItem,"disposal_date","{0:dd/MM/yyyy}")%>' CssClass="textField" Width="100px" Runat="server" TextMaskType="msDate" MaxLength="16" AutoPostBack="True" Enabled="True" TextMaskString="99/99/9999">
							</cc1:mstextbox>
							<asp:RequiredFieldValidator ID="rvDisposalDate" Runat="server" BorderWidth="0" Display="None" ControlToValidate="txtDisposalDate"
								ErrorMessage="Disposal Date"></asp:RequiredFieldValidator>
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Date Out Of Service">
						<HeaderStyle CssClass="gridHeading"></HeaderStyle>
						<ItemStyle CssClass="gridField"></ItemStyle>
						<ItemTemplate>
							<asp:Label ID="lblDateOutOfService" CssClass="gridLabel" Text='<%#DataBinder.Eval(Container.DataItem,"date_out_of_service","{0:dd/MM/yyyy}")%>' Runat=server >
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<cc1:mstextbox id="txtDateOutOfService" Text='<%#DataBinder.Eval(Container.DataItem,"date_out_of_service","{0:dd/MM/yyyy}")%>' CssClass="textField" Width="100px" Runat="server" TextMaskType="msDate" MaxLength="16" AutoPostBack="True" Enabled="True" TextMaskString="99/99/9999">
							</cc1:mstextbox>
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Date In Service">
						<HeaderStyle CssClass="gridHeading"></HeaderStyle>
						<ItemStyle CssClass="gridField"></ItemStyle>
						<ItemTemplate>
							<asp:Label ID="lblDateInService" CssClass="gridLabel" Text='<%#DataBinder.Eval(Container.DataItem,"date_in_service","{0:dd/MM/yyyy}")%>' Runat=server >
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<cc1:mstextbox id="txtDateInService" Text='<%#DataBinder.Eval(Container.DataItem,"date_in_service","{0:dd/MM/yyyy}")%>' CssClass="textField" Width="100px" Runat="server" TextMaskType="msDate" MaxLength="16" AutoPostBack="True" Enabled="True" TextMaskString="99/99/9999">
							</cc1:mstextbox>
						</EditItemTemplate>
					</asp:TemplateColumn>
				</Columns>
				<PagerStyle NextPageText="Next" PrevPageText="Previous" HorizontalAlign="Right" CssClass="normalText"></PagerStyle>
			</asp:datagrid><asp:validationsummary id="ValidationSummary1" style="Z-INDEX: 111; LEFT: 512px; POSITION: absolute; TOP: 8px"
				Height="36px" Width="346px" Runat="server" HeaderText="The following field(s) are required:<br><br>" ShowMessageBox="True"
				DisplayMode="BulletList" ShowSummary="False"></asp:validationsummary></form>
	</body>
</HTML>
