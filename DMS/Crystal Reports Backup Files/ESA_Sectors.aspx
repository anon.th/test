<%@ Page language="c#" Codebehind="ESA_Sectors.aspx.cs" AutoEventWireup="false" Inherits="com.ties.ESA_Sectors" %>
<%@ Import Namespace="com.common.util" %>
<%@ Register TagPrefix="iewc" Namespace="Microsoft.Web.UI.WebControls" Assembly="Microsoft.Web.UI.WebControls, Version=1.0.2.226, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>ESA_Sectors</title>
		<LINK href="css/Styles.css" type="text/css" rel="stylesheet" name="stylesheet">
		<META content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<META content="C#" name="CODE_LANGUAGE">
		<META content="JavaScript" name="vs_defaultClientScript">
		<META content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<!--#INCLUDE FILE="msFormValidations.inc"-->
		<script language="javascript" src="Scripts/settingScrollPosition.js"></script>
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="ESA_Sectors" method="post" runat="server">
			<TABLE id="xxx" style="Z-INDEX: 103; LEFT: 37px; WIDTH: 686px; POSITION: absolute; TOP: 28px; HEIGHT: 58px" width="686" border="0" runat="server">
				<TR>
					<TD style="HEIGHT: 25px"><asp:label id="lblTitle" runat="server" Width="477px" Height="26px" CssClass="mainTitleSize"> ESA Sectors</asp:label></TD>
				</TR>
				<TR>
					<TD><asp:button id="btnQuery" runat="server" Width="60px" CssClass="queryButton" Text="Query" CausesValidation="False"></asp:button><asp:button id="btnExecuteQuery" runat="server" Width="139px" CssClass="queryButton" Text="Execute Query" CausesValidation="False"></asp:button><asp:button id="btnInsert" runat="server" Width="68px" CssClass="queryButton" Text="Insert" CausesValidation="False"></asp:button>&nbsp;
					</TD>
				</TR>
			</TABLE>
			<asp:validationsummary id="ValidationSummary1" style="Z-INDEX: 105; LEFT: 37px; POSITION: absolute; TOP: 102px" Width="346px" Height="36px" Runat="server" HeaderText="The following field(s) are required:" ShowMessageBox="True" DisplayMode="BulletList" ShowSummary="False"></asp:validationsummary><asp:label id="lblErrorMessage" style="Z-INDEX: 101; LEFT: 34px; POSITION: absolute; TOP: 86px" runat="server" Width="540px" Height="35px" CssClass="errorMsgColor">Place holder for err msg</asp:label><asp:datagrid id="dgESA_Sectors" style="Z-INDEX: 104; LEFT: 38px; POSITION: absolute; TOP: 124px" runat="server" Width="524px" Visible="True" PageSize="50" OnPageIndexChanged="dgESA_Sectors_PageChange" OnEditCommand="dgESA_Sectors_Edit" OnCancelCommand="dgESA_Sectors_Cancel" OnDeleteCommand="dgESA_Sectors_Delete" OnUpdateCommand="dgESA_Sectors_Update" OnItemCommand="dgESA_Sectors_Button" AllowPaging="True" AutoGenerateColumns="False" HorizontalAlign="left">
				<Columns>
					<asp:EditCommandColumn ButtonType="LinkButton" UpdateText="&lt;IMG SRC='images/butt-update.gif' border=0 title='Update' &gt;" CancelText="&lt;IMG SRC='images/butt-cancel.gif' border=0 title='Cancel' &gt;" EditText="&lt;IMG SRC='images/butt-edit.gif' border=0 title='Edit' &gt;">
						<HeaderStyle Width="4%" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle Wrap="False" CssClass="gridField"></ItemStyle>
					</asp:EditCommandColumn>
					<asp:ButtonColumn Text="&lt;IMG SRC='images/butt-delete.gif' border=0 title='Delete' &gt;" CommandName="Delete">
						<HeaderStyle Width="2%" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle CssClass="gridField"></ItemStyle>
					</asp:ButtonColumn>
					<asp:TemplateColumn HeaderText="Postal Code">
						<HeaderStyle Font-Bold="True" Width="30%" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle Wrap="False" CssClass="gridField"></ItemStyle>
						<ItemTemplate>
							<asp:Label id=lblzipcode Text='<%#DataBinder.Eval(Container.DataItem,"postal_code")%>' CssClass="gridLabel" Visible="True" Runat="server">
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<cc1:mstextbox CssClass="gridTextBox" ID="txtzipcode" Text='<%#DataBinder.Eval(Container.DataItem,"postal_code")%>' Runat="server" Enabled="True" TextMaskType="msUpperAlfaNumericWithUnderscore" MaxLength="10">
							</cc1:mstextbox>
							<asp:RequiredFieldValidator ID="zipcValidator" Runat="server" BorderWidth="0" Display="None" ControlToValidate="txtzipcode" ErrorMessage="Postal Code"></asp:RequiredFieldValidator>
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:ButtonColumn Text="&lt;IMG SRC='images/butt-search.gif' border=0 title='Search' &gt;" CommandName="zipcodeSearch">
						<HeaderStyle Width="5%" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle Wrap="False" CssClass="gridField"></ItemStyle>
					</asp:ButtonColumn>
					<asp:TemplateColumn HeaderText="ESA Sector Global">
						<HeaderStyle Font-Bold="True" Width="30%" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle Wrap="False" CssClass="gridField"></ItemStyle>
						<ItemTemplate>
							<asp:Label id="lblzone" Text='<%#DataBinder.Eval(Container.DataItem,"ESA_Sector")%>' CssClass="gridLabel" Runat="server" Visible="True">
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<cc1:mstextbox CssClass="gridTextBox" ID="txtzone" Text='<%#DataBinder.Eval(Container.DataItem,"ESA_Sector")%>' Runat="server" Enabled="True" TextMaskType="msUpperAlfaNumericWithUnderscore" MaxLength="12">
							</cc1:mstextbox>
							<asp:RequiredFieldValidator ID="Requiredfieldvalidator1" Runat="server" BorderWidth="0" Display="None" ControlToValidate="txtzone" ErrorMessage="Zone"></asp:RequiredFieldValidator>
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:ButtonColumn Text="&lt;IMG SRC='images/butt-search.gif' border=0 title='Search' &gt;" CommandName="zoneSearch">
						<HeaderStyle Width="5%" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle Wrap="False" CssClass="gridField"></ItemStyle>
					</asp:ButtonColumn>
					<asp:TemplateColumn HeaderText="Effective Date">
						<HeaderStyle Font-Bold="True" Width="30%" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle Wrap="False" CssClass="gridField"></ItemStyle>
						<ItemTemplate>
							<asp:Label ID="lblEffectiveDate" CssClass="gridLabel" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"effective_date","{0:dd/MM/yyyy}")%>'>
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<cc1:mstextbox ID="txtEffectiveDate" MaxLength="10" TextMaskString="99/99/9999" TextMaskType="msDate" Text='<%#DataBinder.Eval(Container.DataItem,"effective_date","{0:dd/MM/yyyy}")%>' Runat="server">
							</cc1:mstextbox>
							<asp:RequiredFieldValidator ID="Requiredfieldvalidator2" Runat="server" BorderWidth="0" Display="None" ControlToValidate="txtEffectiveDate" ErrorMessage="Effective Date"></asp:RequiredFieldValidator>
						</EditItemTemplate>
					</asp:TemplateColumn>
				</Columns>
				<PagerStyle NextPageText="Next" PrevPageText="Previous" HorizontalAlign="Right" CssClass="normalText"></PagerStyle>
			</asp:datagrid></form>
	</body>
</HTML>
