<%@ Page language="c#" Codebehind="PopUpConfirmDialog.aspx.cs" AutoEventWireup="false" Inherits="TIES.WebUI.Manifest.PopUpConfirmDialog" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>PopUpConfirmDialog</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<script>
			function CloseWindow()
			{
			//set return value of the dialog box or dialog result
			top.returnValue= '500';
			window.close();
			}
		</script>
		<LINK href="../css/Styles.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body MS_POSITIONING="GridLayout" style="WIDTH: 600px; HIGHT: 150px">
		<form id="Form1" style="WIDTH: 600px; HEIGHT: 150px" method="post" runat="server">
			<asp:button id="btnYes" style="Z-INDEX: 101; LEFT: 256px; POSITION: absolute; TOP: 80px" runat="server"
				Text="Yes" Width="64px" CssClass="queryButton"></asp:button><asp:button id="btnNo" style="Z-INDEX: 102; LEFT: 328px; POSITION: absolute; TOP: 80px" runat="server"
				Text="No" Width="64px" CssClass="queryButton"></asp:button>
			<asp:Label id="lbmsg" style="Z-INDEX: 104; LEFT: 24px; POSITION: absolute; TOP: 24px" runat="server">Label</asp:Label></form>
	</body>
</HTML>
