<%@ Page language="c#" Codebehind="CustomizedCodePopup.aspx.cs" AutoEventWireup="false" Inherits="TIES.WebUI.CustomizedCodePopup" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>ServiceCodePopup</title>
		<LINK href="css/Styles.css" type="text/css" rel="stylesheet" name="stylesheet">
		<meta name="GENERATOR" Content="Microsoft Visual Studio 7.0">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<script language="javascript" src="Scripts/settingScrollPosition.js"></script>
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="ServiceCodePopup" method="post" runat="server">
			<asp:datagrid id="dgServiceCode" style="Z-INDEX: 101; LEFT: 45px; POSITION: absolute; TOP: 135px" runat="server" CssClass="gridHeading" BorderStyle="None" BorderWidth="1px" CellPadding="3" BackColor="White" BorderColor="#CCCCCC" AutoGenerateColumns="False" Width="522px" AllowPaging="True" PageSize="10" OnPageIndexChanged="Paging">
				<SelectedItemStyle Font-Bold="True" ForeColor="White" BackColor="#669999"></SelectedItemStyle>
				<HeaderStyle Width="10%" CssClass="gridHeading"></HeaderStyle>
				<ItemStyle CssClass="popupGridField"></ItemStyle>
				<FooterStyle ForeColor="#000066" BackColor="White"></FooterStyle>
				<Columns>
					<asp:BoundColumn DataField="code_text" HeaderText="Description"></asp:BoundColumn>
					<asp:BoundColumn DataField="code_num_value" HeaderText="Costumized Code" Visible="False"></asp:BoundColumn>
					<asp:BoundColumn DataField="assemblyid" HeaderText="Assembly Code"></asp:BoundColumn>
					<asp:BoundColumn DataField="Assembly_description" HeaderText="Description"></asp:BoundColumn>
					<asp:ButtonColumn Text="Select" CommandName="Select"></asp:ButtonColumn>
				</Columns>
				<PagerStyle HorizontalAlign="Left" ForeColor="#000066" BackColor="White" Mode="NumericPages"></PagerStyle>
			</asp:datagrid>
			<asp:button id="btnClose" style="Z-INDEX: 110; LEFT: 483px; POSITION: absolute; TOP: 89px" runat="server" Width="84px" CssClass="buttonProp" Height="21px" Text="Close" CausesValidation="False"></asp:button>
			<asp:Label id="lblCustName" style="Z-INDEX: 109; LEFT: 198px; POSITION: absolute; TOP: 68px" runat="server" CssClass="tablelabel">Description</asp:Label>
			<asp:label id="lblErrorMsg" style="Z-INDEX: 107; LEFT: 44px; POSITION: absolute; TOP: 114px" runat="server" Width="566px" CssClass="errorMsgColor" Height="19px"></asp:label>
			<asp:button id="btnSearch" style="Z-INDEX: 104; LEFT: 396px; POSITION: absolute; TOP: 89px" runat="server" CssClass="buttonProp" Width="84px" CausesValidation="False" Text="Search" Height="21px"></asp:button><asp:textbox id="txtServiceCode" style="Z-INDEX: 103; LEFT: 49px; POSITION: absolute; TOP: 90px" runat="server" CssClass="textField" Width="116px" Height="19px"></asp:textbox><asp:label id="lblCustID" style="Z-INDEX: 100; LEFT: 52px; POSITION: absolute; TOP: 67px" runat="server" CssClass="tableLabel" Width="139px" Height="16px">Customized Code</asp:label><asp:button id="btnRefresh" style="DISPLAY: none; Z-INDEX: 106; LEFT: 569px; POSITION: absolute; TOP: 108px" runat="server" Width="67px" Text="HIDDEN"></asp:button><asp:textbox id="txtServiceDescription" style="Z-INDEX: 102; LEFT: 199px; POSITION: absolute; TOP: 90px" runat="server" CssClass="textField" Width="184px" Height="19px"></asp:textbox>
			<asp:Label id="lblServiceTitle" style="Z-INDEX: 108; LEFT: 20px; POSITION: absolute; TOP: 15px" runat="server" Width="585px" CssClass="mainTitleSize" Height="42px">Customized Code</asp:Label>
		</form>
	</body>
</HTML>
