<%@ Page language="c#" Codebehind="CustomsJobRegistration.aspx.cs" AutoEventWireup="false" Inherits="TIES.WebUI.CustomsJobRegistration" %>
<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<%@ Import Namespace="com.common.util" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>CustomsJobRegistration</title>
		<meta name="vs_snapToGrid" content="True">
		<meta name="vs_showGrid" content="True">
		<LINK rel="stylesheet" type="text/css" href="css/Styles.css" name="stylesheet">
		<meta name="GENERATOR" content="Microsoft Visual Studio 7.0">
		<meta name="CODE_LANGUAGE" content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<!--#INCLUDE FILE="msFormValidations.inc"-->
		<script language="javascript" src="Scripts/settingScrollPosition.js"></script>
		<script language="javascript" src="Scripts/JScript_TextControl.js"></script>
		<script language="javascript" type="text/javascript">
		function funcDisBack(){
			history.go(+1);
		}
				
        function RemoveBadNonNumber(strTemp, obj) {
            strTemp = strTemp.replace(/[^0-9]/g, ''); //replace non-numeric
            obj.value = strTemp.replace(/\s/g, ''); // replace space
        }
        function AfterPasteNonNumber(obj) {
            setTimeout(function () {
                RemoveBadNonNumber(obj.value, obj);
            }, 1); //or 4
        }
        
        function RemoveBadNonNumberWeight(strTemp, obj) {
			strTemp = strTemp.replace(/[^._0-9]/g, ''); //replace non-numeric
            strTemp = strTemp.replace(/\s/g, ''); // replace space
            
			var res = strTemp.split(".");     			
			for (var i=0; i<res.length-1; i++) {
				if(i==0)
				{
					if (res[i].length > 6) 
					{
						strTemp=res[i].substring(res[i].length - 6);			
					}else{
						strTemp=res[i];
					}
				}
				if(i==1)
				{
					strTemp=strTemp+"."+res[i].substring(0,1);									
				}
			}	
			if(res.length == 1)
			{
				strTemp=strTemp.substring(strTemp.length - 6);				
			}		
			obj.value=strTemp;
        }
        
        function AfterPasteNonNumberWeight(obj) {
            setTimeout(function () {
                RemoveBadNonNumberWeight(obj.value, obj);
            }, 1); //or 4
        }
        
        function validate(evt) {
			var theEvent = evt || window.event;
			var key = theEvent.keyCode || theEvent.which;
			key = String.fromCharCode( key );
			var regex = /[0-9]/;
			if( !regex.test(key) ) {
				theEvent.returnValue = false;
				if(theEvent.preventDefault) theEvent.preventDefault();
			}
		}
        
        function fnOnclose()
        {
			//window.opener.CreateUpdateConsignment.submit();
			window.opener.callback();                
        }        
        
        function clickSave()
        {         
			alert("xxx"); 
        }  
        
        function MaxWeightLengtth(obj)
        {
              
        }
		</script>
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="CustomsMasterAWBEntry" method="post" runat="server">
			<DIV style="Z-INDEX: 0; POSITION: relative; WIDTH: 760px; HEIGHT: 217px; TOP: 34px; LEFT: 22px"
				id="divCustomsJobEntry" MS_POSITIONING="GridLayout" runat="server">
				<TABLE style="Z-INDEX: 112" id="Table5" border="0" width="760" runat="server">
					<tr>
						<td colSpan="4" align="left"><asp:label id="Label65" runat="server" Width="421px" Height="27px" CssClass="mainTitleSize">
							Customs Job Registration</asp:label></td>
					</tr>
					<tr>
						<td style="HEIGHT: 26px" vAlign="top" colSpan="4" align="left"><asp:button id="btnSave" runat="server" CssClass="queryButton" Text="Save" CausesValidation="False"></asp:button><asp:button style="MARGIN-LEFT: 1px" id="Button3" runat="server" CssClass="queryButton" Text="Close"
								CausesValidation="False"></asp:button></td>
					</tr>
					<TR>
						<TD style="HEIGHT: 19px" colSpan="4">
							<asp:label id="lblError" runat="server" Font-Size="X-Small" Font-Bold="True" ForeColor="Red"></asp:label>
						</TD>
					</TR>
					<tr>
						<td width="133"><asp:label id="Label17" runat="server" CssClass="tableLabel" style="Z-INDEX: 0"> Job Entry</asp:label></td>
						<td width="217"><asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea" id="JobEntryNo"
								tabIndex="99" runat="server" Width="95%" CssClass="textField" ReadOnly="True" MaxLength="100" Enabled="False"></asp:textbox></td>
						<td width="170">
							<asp:label id="Label39" runat="server" CssClass="tableLabel" style="Z-INDEX: 0"> Consignee (Customer ID)</asp:label>
						</td>
						<td width="240">
							<asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea" id="CustomerID"
								tabIndex="99" runat="server" Width="170px" CssClass="textField" ReadOnly="True" MaxLength="100"
								Enabled="False"></asp:textbox>
						</td>
					</tr>
					<TR>
						<TD><asp:label id="Label38" runat="server" CssClass="tableLabel" style="Z-INDEX: 0"> House AWB Number</asp:label></TD>
						<TD><asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea" id="HouseAWBNo"
								tabIndex="99" runat="server" Width="95%" CssClass="textField" ReadOnly="True" MaxLength="100"
								Enabled="False"></asp:textbox></TD>
						<TD><asp:label id="Label40" runat="server" CssClass="tableLabel" style="Z-INDEX: 0"> Customer's Tax Code</asp:label></TD>
						<TD><asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea" id="CustomerTaxCode"
								tabIndex="99" runat="server" Width="170px" CssClass="textField" ReadOnly="True" MaxLength="100"
								Enabled="False"></asp:textbox></TD>
					</TR>
					<tr>
						<td>
							<asp:label style="Z-INDEX: 0" id="Label37" runat="server" CssClass="tableLabel"> Info Received Date</asp:label>
							<asp:label style="Z-INDEX: 0" id="Label2" runat="server" CssClass="tableLabel" ForeColor="Red">*</asp:label></td>
						<td>
							<cc1:mstextbox style="Z-INDEX: 0" id="InfoReceivedDate" tabIndex="1" runat="server" CssClass="textField"
								Width="100px" MaxLength="12" TextMaskType="msDate" TextMaskString="99/99/9999"></cc1:mstextbox></td>
						<td><asp:label id="Label59" runat="server" CssClass="tableLabel" style="Z-INDEX: 0"> Consignee's Details</asp:label></td>
						<td></td>
					</tr>
					<TR>
						<TD><asp:label id="Label57" runat="server" CssClass="tableLabel" style="Z-INDEX: 0"> Master AWB Number</asp:label>
							<asp:label style="Z-INDEX: 0" id="Label58" runat="server" CssClass="tableLabel" ForeColor="Red">*</asp:label></TD>
						<TD>
							<asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase" id="MasterAWBNumber" tabIndex="2"
								runat="server" CssClass="textField" Width="95%" MaxLength="100"></asp:textbox></TD>
						<TD colspan="2"><asp:textbox style="Z-INDEX: 0;  BACKGROUND-COLOR: #eaeaea" id="Consignee_name" tabIndex="99"
								runat="server" Width="95%" CssClass="textField" ReadOnly="True" MaxLength="100" Enabled="False"></asp:textbox></TD>
					</TR>
					<TR>
						<TD></TD>
						<TD>
							<asp:CheckBox style="Z-INDEX: 0" id="IgnoreMAWBValidation" runat="server" CssClass="tableRadioButton"
								Text="Ignore MAWB validation" tabIndex="2"></asp:CheckBox></TD>
						<TD colSpan="2">
							<asp:textbox style="Z-INDEX: 0; BACKGROUND-COLOR: #eaeaea" id="Consignee_address1" tabIndex="99"
								runat="server" CssClass="textField" Width="95%" Enabled="False" MaxLength="100" ReadOnly="True"></asp:textbox></TD>
					</TR>
					<TR>
						<TD>
							<asp:label style="Z-INDEX: 0" id="Label41" runat="server" CssClass="tableLabel"> Mode of Transport</asp:label>
							<asp:label style="Z-INDEX: 0" id="Label43" runat="server" CssClass="tableLabel" ForeColor="Red">*</asp:label></TD>
						<TD>
							<table border="0" cellpadding="0" cellspacing="0" width="95%">
								<tr>
									<td Width="55">
										<asp:dropdownlist style="Z-INDEX: 0" id="TransportMode" tabIndex="3" runat="server" Width="50px" AutoPostBack="True"></asp:dropdownlist>
									</td>
									<td>
										<asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea" id="txtModeOfTransport"
											tabIndex="99" runat="server" CssClass="textField" Width="100%" Enabled="False" MaxLength="100"
											ReadOnly="True"></asp:textbox>
									</td>
								</tr>
							</table>
						</TD>
						<TD colSpan="2">
							<asp:textbox style="Z-INDEX: 0; BACKGROUND-COLOR: #eaeaea" id="Consignee_address2" tabIndex="99"
								runat="server" CssClass="textField" Width="95%" Enabled="False" MaxLength="100" ReadOnly="True"></asp:textbox></TD>
					</TR>
					<TR>
						<TD>
							<asp:label style="Z-INDEX: 0" id="Label42" runat="server" CssClass="tableLabel"> Exporter</asp:label></TD>
						<TD></TD>
						<TD colSpan="2">
							<asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea" id="Consignee_zipcode"
								tabIndex="99" runat="server" Width="10%" CssClass="textField" ReadOnly="True" MaxLength="100"
								Enabled="False"></asp:textbox><asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea" id="Consignee_state"
								tabIndex="99" runat="server" Width="85%" CssClass="textField" ReadOnly="True" MaxLength="100" Enabled="False"></asp:textbox>
						</TD>
					</TR>
					<TR>
						<TD colSpan="2">
							<asp:label style="POSITION: absolute" id="Label44" runat="server" CssClass="tableLabel" ForeColor="Red">*</asp:label>
							<asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; MARGIN-LEFT: 10px" id="ExporterName"
								tabIndex="4" runat="server" CssClass="textField" Width="336px" MaxLength="100"></asp:textbox></TD>
						<TD colSpan="2">
							<asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea" id="Consignee_telephone"
								tabIndex="99" runat="server" CssClass="textField" Width="150px" Enabled="False" MaxLength="100"
								ReadOnly="True"></asp:textbox>
						</TD>
					</TR>
					<TR>
						<TD colSpan="2">
							<asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; MARGIN-LEFT: 10px" id="ExporterAddress1"
								tabIndex="5" runat="server" CssClass="textField" Width="336px" MaxLength="100"></asp:textbox></TD>
						<TD colSpan="2"></TD>
					</TR>
					<TR>
						<TD colSpan="2">
							<asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; MARGIN-LEFT: 10px" id="ExporterAddress2"
								tabIndex="6" runat="server" CssClass="textField" Width="336px" MaxLength="100"></asp:textbox></TD>
						<TD colSpan="2"></TD>
					</TR>
					<TR>
						<TD>
							<asp:label id="Label45" runat="server" CssClass="tableLabel" style="Z-INDEX: 0"> Country</asp:label>
							<asp:label style="Z-INDEX: 0" id="Label3" runat="server" CssClass="tableLabel" ForeColor="Red">*</asp:label>
							<asp:dropdownlist style="Z-INDEX: 0" id="ExporterCountry" tabIndex="7" runat="server" Width="55px"
								AutoPostBack="True"></asp:dropdownlist></TD>
						<TD>
							<asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea" id="txtCounty"
								tabIndex="14" runat="server" CssClass="textField" Width="150px" Enabled="False" MaxLength="100"
								ReadOnly="True"></asp:textbox></TD>
						<TD colSpan="2">
							<asp:CheckBox style="Z-INDEX: 0" id="cbkDoorToDoor" tabIndex="13" runat="server" CssClass="tableRadioButton"
								Text="Express door-to-door" AutoPostBack="True"></asp:CheckBox>
							<asp:label style="Z-INDEX: 0; MARGIN-LEFT: 5px" id="lblOriginCity" runat="server" CssClass="tableLabel">Origin City</asp:label>
							<asp:dropdownlist style="Z-INDEX: 0" id="ddlOriginCity" tabIndex="14" runat="server" Width="60px"
								AutoPostBack="True"></asp:dropdownlist></TD>
					</TR>
					<TR>
						<TD>
							<asp:label style="Z-INDEX: 0" id="Label47" runat="server" CssClass="tableLabel"> Loading Port (City)</asp:label>
							<asp:label style="Z-INDEX: 0" id="Label4" runat="server" CssClass="tableLabel" ForeColor="Red">*</asp:label></TD>
						<TD>
							<asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase" id="LoadingPortCity" tabIndex="8"
								runat="server" CssClass="textField" Width="55px" MaxLength="3"></asp:textbox></TD>
						<TD colspan="2">
							<asp:label style="Z-INDEX: 0" id="Label12" runat="server" CssClass="tableLabel"> Pkgs</asp:label>
							<asp:label style="Z-INDEX: 0" id="Label13" runat="server" CssClass="tableLabel" ForeColor="Red">*</asp:label>
							<asp:textbox style="Z-INDEX: 0;MARGIN-LEFT: 2px;MARGIN-RIGHT: 2px" id="Total_pkgs" tabIndex="15"
								onkeypress="validate(event);" onpaste="AfterPasteNonNumber(this)" runat="server" CssClass="textFieldRightAlign"
								Width="50px" MaxLength="6"></asp:textbox>
							<asp:label style="Z-INDEX: 0" id="Label14" runat="server" CssClass="tableLabel"> Weight (kg)</asp:label>
							<asp:label style="Z-INDEX: 0" id="Label15" runat="server" CssClass="tableLabel" ForeColor="Red">*</asp:label>
							<cc1:mstextbox style="Z-INDEX: 0;MARGIN-LEFT: 2px;MARGIN-RIGHT: 2px" id="Total_weight" tabIndex="16"
								onpaste="AfterPasteNonNumberWeight(this);" runat="server" CssClass="textFieldRightAlign" Width="70px"
								TextMaskType="msNumericCOD" NumberPrecision="8" NumberMaxValueCOD="999999.9" NumberScale="1"></cc1:mstextbox>
							<asp:label style="Z-INDEX: 0" id="Label16" runat="server" CssClass="tableLabel"> Chg Wt</asp:label>
							<cc1:mstextbox style="Z-INDEX: 0; MARGIN-LEFT: 2px; MARGIN-RIGHT: 2px" id="ChargeableWeight" tabIndex="16"
								onpaste="AfterPasteNonNumberWeight(this);" runat="server" CssClass="textFieldRightAlign" Width="70px"
								MaxLength="8" TextMaskType="msNumericCOD" NumberScale="1" NumberMaxValueCOD="999999.9" NumberPrecision="8"></cc1:mstextbox>
						</TD>
					</TR>
					<TR>
						<TD>
							<asp:label style="Z-INDEX: 0" id="Label48" runat="server" CssClass="tableLabel"> Country</asp:label>
							<asp:label style="Z-INDEX: 0" id="Label5" runat="server" CssClass="tableLabel" ForeColor="Red">*</asp:label></TD>
						<TD>
							<table border="0" cellpadding="0" cellspacing="0" width="95%">
								<tr>
									<td width="60">
										<asp:dropdownlist style="Z-INDEX: 0" id="LoadingPortCountry" tabIndex="9" runat="server" Width="55px"
											AutoPostBack="True"></asp:dropdownlist>
									</td>
									<td>
										<asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea" id="txtPortCountry"
											tabIndex="99" runat="server" CssClass="textField" Width="100%" Enabled="False" MaxLength="100"
											ReadOnly="True"></asp:textbox>
									</td>
								</tr>
							</table>
						</TD>
						<TD>
							<asp:label style="Z-INDEX: 0" id="Label62" runat="server" CssClass="tableLabel"> Expected Arrival Date</asp:label>
							<asp:label style="Z-INDEX: 0" id="Label6" runat="server" CssClass="tableLabel" ForeColor="Red">*</asp:label></TD>
						<TD>
							<cc1:mstextbox style="Z-INDEX: 0" id="ExpectedArrivalDate" tabIndex="17" runat="server" CssClass="textField"
								Width="100px" MaxLength="12" TextMaskType="msDate" TextMaskString="99/99/9999"></cc1:mstextbox></TD>
					</TR>
					<TR>
						<TD><asp:label id="Label50" runat="server" CssClass="tableLabel" style="Z-INDEX: 0"> Currency Name</asp:label>
							<asp:label style="Z-INDEX: 0" id="Label7" runat="server" CssClass="tableLabel" ForeColor="Red">*</asp:label></TD>
						<TD>
							<table border="0" cellpadding="0" cellspacing="0" width="95%">
								<tr>
									<td>
										<asp:dropdownlist style="Z-INDEX: 0" id="CurrencyCode" tabIndex="10" runat="server" Width="55px" AutoPostBack="True"></asp:dropdownlist>
									</td>
									<td align="right">
										<asp:label id="Label55" runat="server" CssClass="tableLabel" style="Z-INDEX: 0;MARGIN-RIGHT: 5px"> Rate</asp:label>
										<asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea" id="exchange_rate"
											tabIndex="99" runat="server" CssClass="textField" Width="100px" Enabled="False" MaxLength="100"
											ReadOnly="True"></asp:textbox>
									</td>
								</tr>
							</table>
						</TD>
						<TD><FONT face="Tahoma">
								<asp:label style="Z-INDEX: 0" id="Label64" runat="server" CssClass="tableLabel"> Actual Goods Arrival Date</asp:label></FONT></TD>
						<TD>
							<cc1:mstextbox style="Z-INDEX: 0" id="ActualArrivalDate" tabIndex="18" runat="server" CssClass="textField"
								Width="100px" MaxLength="12" TextMaskType="msDate" TextMaskString="99/99/9999"></cc1:mstextbox></TD>
					</TR>
					<TR>
						<TD>
							<asp:label style="Z-INDEX: 0" id="lblShipFlightNo" runat="server" CssClass="tableLabel"> Ship/Flight No</asp:label>
							<asp:label style="Z-INDEX: 0" id="Label8" runat="server" CssClass="tableLabel" ForeColor="Red">*</asp:label></TD>
						<TD>
							<asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase" id="ShipFlightNo" tabIndex="11" runat="server"
								CssClass="textField" Width="95%" MaxLength="100"></asp:textbox></TD>
						<TD>
							<asp:label style="Z-INDEX: 0" id="Label66" runat="server" CssClass="tableLabel"> Entry Type</asp:label>
							<asp:label style="Z-INDEX: 0" id="Label11" runat="server" CssClass="tableLabel" ForeColor="Red">*</asp:label></TD>
						<TD>
							<asp:dropdownlist style="Z-INDEX: 0" id="EntryType" tabIndex="19" runat="server" Width="95%"></asp:dropdownlist></TD>
					</TR>
					<TR>
						<TD>
							<asp:label style="Z-INDEX: 0" id="Label49" runat="server" CssClass="tableLabel"> Discharge Port</asp:label>
							<asp:label style="Z-INDEX: 0" id="Label9" runat="server" CssClass="tableLabel" ForeColor="Red">*</asp:label></TD>
						<TD>
							<asp:dropdownlist style="Z-INDEX: 0" id="DischargePort" tabIndex="12" runat="server" Width="95%"></asp:dropdownlist></TD>
						<TD>
							<asp:label style="Z-INDEX: 0" id="Label68" runat="server" CssClass="tableLabel"> Delivery Terms</asp:label>
							<asp:label style="Z-INDEX: 0" id="Label10" runat="server" CssClass="tableLabel" ForeColor="Red">*</asp:label></TD>
						<TD>
							<table border="0" cellpadding="0" cellspacing="0" width="95%">
								<tr>
									<td width="55">
										<asp:dropdownlist style="Z-INDEX: 0" id="DeliveryTerms" tabIndex="20" runat="server" Width="50px"
											AutoPostBack="True"></asp:dropdownlist>
									</td>
									<td>
										<asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea" id="txtDeliveryTerms"
											tabIndex="99" runat="server" CssClass="textField" Width="100%" Enabled="False" MaxLength="100"
											ReadOnly="True"></asp:textbox>
									</td>
								</tr>
							</table>
						</TD>
					</TR>
					<TR width="100%">
						<TD colspan="2"><FONT face="Tahoma"></FONT>
						</TD>
						<TD colSpan="2"><FONT face="Tahoma"></FONT>
						</TD>
					</TR>
				</TABLE>
			</DIV>
		</form>
	</body>
</HTML>
