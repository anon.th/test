<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<%@ Page language="c#" Codebehind="InvoicedConsigmentPrinting.aspx.cs" AutoEventWireup="false" Inherits="TIES.WebUI.InvoicedConsigmentPrinting" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Consignment Status Printing</title>
		<meta name="vs_snapToGrid" content="True">
		<meta name="vs_showGrid" content="True">
		<LINK rel="stylesheet" type="text/css" href="css/Styles.css" name="stylesheet">
		<meta name="GENERATOR" content="Microsoft Visual Studio 7.0">
		<meta name="CODE_LANGUAGE" content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<!--#INCLUDE FILE="msFormValidations.inc"-->
		<script language="javascript" src="Scripts/settingScrollPosition.js"></script>
		<script language="javascript" src="Scripts/JScript_TextControl.js"></script>
		<script language="javascript" type="text/javascript">
		function funcDisBack()
		{
			history.go(+1);
		}

        function RemoveBadMasterAWBNumber(strTemp, obj) {
            strTemp = strTemp.replace(/[^-_/_a-zA-Z0-9]/g, ''); //replace non-numeric
            obj.value = strTemp.replace(/\s/g, ''); // replace space
        }
        function AfterPasteMasterAWBNumber(obj) {
            setTimeout(function () {
                RemoveBadMasterAWBNumber(obj.value, obj);
            }, 1); //or 4
        }
        
        function ValidateMasterAWBNumber(evt) {
			var theEvent = evt || window.event;
			var key = theEvent.keyCode || theEvent.which;
			key = String.fromCharCode( key );
			var regex = /[-_/_a-zA-Z0-9]/;
			if( !regex.test(key) ) {
				theEvent.returnValue = false;
				if(theEvent.preventDefault) theEvent.preventDefault();
			}
		}
		
		function RemoveBadNonNumber(strTemp, obj) {
            strTemp = strTemp.replace(/[^0-9]/g, ''); //replace non-numeric
            obj.value = strTemp.replace(/\s/g, ''); // replace space
        }
        
        function AfterPasteNonNumber(obj) {
            setTimeout(function () {
                RemoveBadNonNumber(obj.value, obj);
            }, 1); //or 4
        }
        
        function validate(evt) {
			var theEvent = evt || window.event;
			var key = theEvent.keyCode || theEvent.which;
			key = String.fromCharCode( key );
			var regex = /[0-9]/;
			if( !regex.test(key) ) {
				theEvent.returnValue = false;
				if(theEvent.preventDefault) theEvent.preventDefault();
			}
		}		
		</script>
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="InvoicedConsigmentPrinting" method="post" runat="server">
			<INPUT style="Z-INDEX: 101; POSITION: absolute; TOP: 680px; LEFT: 16px" type="hidden" name="ScrollPosition">
			<asp:validationsummary style="Z-INDEX: 105; POSITION: absolute; COLOR: red; TOP: 672px; LEFT: 240px" id="Validationsummary1"
				ShowSummary="False" HeaderText="Please enter the missing  fields." ShowMessageBox="True" DisplayMode="BulletList"
				Runat="server"></asp:validationsummary>
			<div style="Z-INDEX: 102; POSITION: relative; HEIGHT: 1248px; TOP: 32px; LEFT: 24px"
				id="divMain" MS_POSITIONING="GridLayout" runat="server">
				<TABLE style="Z-INDEX: 101; POSITION: absolute; TOP: 8px; LEFT: 0px" id="MainTable" border="0"
					width="100%" runat="server">
					<tr>
						<td colSpan="2" align="left"><asp:label id="Label1" runat="server" Width="421px" Height="27px" CssClass="mainTitleSize">
							Invoiced Consignment Printing</asp:label></td>
					</tr>
					<tr>
						<td style="HEIGHT: 24px" vAlign="top" colSpan="2" align="left">
							<asp:button id="btnQry" runat="server" Width="61px" CssClass="queryButton" Text="Query" CausesValidation="False"
								style="Z-INDEX: 0"></asp:button>
							<asp:button id="btnExecQry" runat="server" Width="130px" CssClass="queryButton" Text="Execute Query"
								CausesValidation="False"></asp:button>
							<asp:button id="btnReprintConsNote" runat="server" Width="150px" CssClass="queryButton" Text="Reprint Con Note"
								CausesValidation="False" Enabled="False"></asp:button>
						</td>
					</tr>
					<tr>
						<td vAlign="top" colSpan="2" align="left">
							<TABLE style="Z-INDEX: 112; WIDTH: 740px" id="Table1" border="0" width="730" runat="server">
								<TR width="100%">
									<TD style="Z-INDEX: 0" width="100%">
										<fieldset style="WIDTH: 824px"><legend><asp:label id="lblSearch" runat="server" CssClass="tableHeadingFieldset">Search Criteria</asp:label></legend>
											<table border="0" cellSpacing="1" cellPadding="1">
												<TBODY>
													<TR id="TrNonCustoms" runat="server">
														<TD style="WIDTH: 143px; HEIGHT: 14px"><asp:label style="Z-INDEX: 0" id="Label2" runat="server" Width="128px" CssClass="tableLabel"
																Height="12px"> Consignment No</asp:label></TD>
														<TD width="266" style="WIDTH: 266px; HEIGHT: 14px"><FONT face="Tahoma">
																<asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase" id="txtConsignNo" tabIndex="1" onkeypress="ValidateMasterAWBNumber(event)"
																	onpaste="AfterPasteMasterAWBNumber(this)" runat="server" CssClass="textField" Width="114px"
																	MaxLength="30"></asp:textbox></FONT></TD>
														<td style="HEIGHT: 14px"><asp:label style="Z-INDEX: 0" id="Label5" runat="server" Width="130" CssClass="tableLabel"> Customer ID</asp:label></td>
														<td style="HEIGHT: 14px"><asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase" id="txtCustomerID" tabIndex="2" onkeypress="ValidateMasterAWBNumber(event)"
																onpaste="AfterPasteMasterAWBNumber(this)" runat="server" Width="114px" CssClass="textField" MaxLength="30"></asp:textbox></td>
													</TR>
													<TR id="TrCustoms" runat="server">
														<TD style="WIDTH: 143px; HEIGHT: 14px"><asp:label style="Z-INDEX: 0" id="Label3" runat="server" Width="128px" CssClass="tableLabel"
																Height="12px">Manifested Date From</asp:label></TD>
														<TD width="266" style="WIDTH: 266px; HEIGHT: 14px"><FONT face="Tahoma">
																<cc1:mstextbox id="txtManifestedDataFrom" tabIndex="3" runat="server" Width="114px" CssClass="textField"
																	MaxLength="12" TextMaskType="msDate" TextMaskString="99/99/9999" style="Z-INDEX: 0"></cc1:mstextbox>&nbsp;<asp:label id="lblTo" runat="server" CssClass="tableLabel">To</asp:label>&nbsp;
																<cc1:mstextbox id="txtManifestedDatato" tabIndex="4" runat="server" Width="114px" CssClass="textField"
																	MaxLength="12" TextMaskType="msDate" TextMaskString="99/99/9999"></cc1:mstextbox></FONT></TD>
														<td style="HEIGHT: 14px"><asp:label style="Z-INDEX: 0" id="Label4" runat="server" Width="130" CssClass="tableLabel">Invoiced Date From</asp:label></td>
														<td style="HEIGHT: 14px">
															<cc1:mstextbox id="txtInvoiceDateFrom" tabIndex="5" runat="server" Width="114px" CssClass="textField"
																MaxLength="12" TextMaskType="msDate" TextMaskString="99/99/9999"></cc1:mstextbox>&nbsp;<asp:label id="Label7" runat="server" CssClass="tableLabel">To</asp:label>&nbsp;
															<cc1:mstextbox id="txtInvoiceDateto" tabIndex="6" runat="server" Width="114px" CssClass="textField"
																MaxLength="12" TextMaskType="msDate" TextMaskString="99/99/9999"></cc1:mstextbox></td>
													</TR>
												</TBODY>
											</table>
										</fieldset>
									</TD>
								</TR>
								<tr>
									<td colSpan="2"><asp:label style="Z-INDEX: 0" id="lblErrorMsg" runat="server" Width="566px" Height="19px" Font-Size="X-Small"
											Font-Bold="True" ForeColor="Red"></asp:label></td>
								</tr>
								<tr>
									<td colSpan="2" style="WIDTH: 903px"><asp:datagrid style="Z-INDEX: 0" id="dgConsignmentStatus" runat="server" SelectedItemStyle-CssClass="gridFieldSelected"
											PageSize="25" Width="824px" AutoGenerateColumns="False" HorizontalAlign="Left" AllowPaging="True">
											<SelectedItemStyle CssClass="gridFieldSelected"></SelectedItemStyle>
											<ItemStyle Height="15px" CssClass="gridField"></ItemStyle>
											<HeaderStyle Font-Bold="True" HorizontalAlign="Center" Height="40px" Width="2%" CssClass="gridHeading"></HeaderStyle>
											<Columns>
												<asp:TemplateColumn>
													<ItemStyle HorizontalAlign="Center" Height="25px" Width="30px"></ItemStyle>
													<ItemTemplate>
														<asp:CheckBox id="chkSelect" Runat="server" CrsID='<%#DataBinder.Eval(Container.DataItem,"consignment_no")%>' AutoPostBack="True" Checked='<%# DataBinder.Eval(Container.DataItem,"Check").ToString().Equals("true") %>'>
														</asp:CheckBox>
														</asp:CheckBox>
													</ItemTemplate>
													<EditItemTemplate>
														<asp:Label></asp:Label>
													</EditItemTemplate>
												</asp:TemplateColumn>
												<asp:BoundColumn DataField="consignment_no" HeaderText="Consignment No">
													<HeaderStyle Width="130px"></HeaderStyle>
													<ItemStyle HorizontalAlign="Center"></ItemStyle>
												</asp:BoundColumn>
												<asp:BoundColumn DataField="payerid" HeaderText="Customer ID">
													<HeaderStyle Width="130px"></HeaderStyle>
													<ItemStyle HorizontalAlign="Center"></ItemStyle>
												</asp:BoundColumn>
												<asp:BoundColumn DataField="manifest_date" HeaderText="Manifested Date">
													<HeaderStyle Width="130px"></HeaderStyle>
													<ItemStyle HorizontalAlign="Center"></ItemStyle>
												</asp:BoundColumn>
												<asp:BoundColumn DataField="invoice_date" HeaderText="Invoiced Date">
													<HeaderStyle Width="130px"></HeaderStyle>
													<ItemStyle HorizontalAlign="Center"></ItemStyle>
												</asp:BoundColumn>
												<asp:BoundColumn DataField="service_code" HeaderText="Service">
													<ItemStyle HorizontalAlign="Center"></ItemStyle>
												</asp:BoundColumn>
											</Columns>
											<PagerStyle NextPageText="Next" PrevPageText="Previous" HorizontalAlign="Left" CssClass="normalText"></PagerStyle>
										</asp:datagrid></td>
								</tr>
							</TABLE>
						</td>
					</tr>
				</TABLE>
			</div>
		</form>
	</body>
</HTML>
