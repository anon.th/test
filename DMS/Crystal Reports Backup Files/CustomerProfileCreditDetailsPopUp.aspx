<%@ Page language="c#" Codebehind="CustomerProfileCreditDetailsPopUp.aspx.cs" AutoEventWireup="false" Inherits="TIES.WebUI.CustomerProfileCreditDetailsPopUp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>CustomerProfileCreditDetailsPopUp</title>
		<LINK href="css/Styles.css" type="text/css" rel="stylesheet" name="stylesheet">
		<meta name="GENERATOR" Content="Microsoft Visual Studio 7.0">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<asp:button id="btnQry" style="Z-INDEX: 101; LEFT: 556px; POSITION: absolute; TOP: 42px" runat="server"
				CausesValidation="False" Width="82px" Text="OK" CssClass="queryButton"></asp:button>
			<asp:datagrid id="dgUpdateHistory" style="Z-INDEX: 105; LEFT: 32px; POSITION: absolute; TOP: 257px"
				runat="server" Width="601px" CssClass="gridHeading" AllowPaging="True" AutoGenerateColumns="False"
				BorderColor="#CCCCCC" BackColor="White" CellPadding="3" BorderWidth="1px" BorderStyle="None"
				AllowCustomPaging="True">
				<FooterStyle ForeColor="#000066" BackColor="White"></FooterStyle>
				<SelectedItemStyle Font-Bold="True" ForeColor="White" CssClass="drilldownFieldSelected"></SelectedItemStyle>
				<ItemStyle CssClass="drilldownField"></ItemStyle>
				<HeaderStyle Width="10%" CssClass="drilldownHeading"></HeaderStyle>
				<Columns>
					<asp:BoundColumn HeaderText="Credit Limit">
						<HeaderStyle HorizontalAlign="Center" Width="120px"></HeaderStyle>
						<ItemStyle HorizontalAlign="Right"></ItemStyle>
					</asp:BoundColumn>
					<asp:BoundColumn HeaderText="Credit Terms">
						<HeaderStyle HorizontalAlign="Center"></HeaderStyle>
						<ItemStyle HorizontalAlign="Center"></ItemStyle>
					</asp:BoundColumn>
					<asp:BoundColumn HeaderText="Credit Status">
						<HeaderStyle HorizontalAlign="Center"></HeaderStyle>
						<ItemStyle HorizontalAlign="Center"></ItemStyle>
					</asp:BoundColumn>
					<asp:BoundColumn HeaderText="Credit Threshold (%)">
						<HeaderStyle HorizontalAlign="Center" Width="50px"></HeaderStyle>
						<ItemStyle HorizontalAlign="Center"></ItemStyle>
					</asp:BoundColumn>
					<asp:BoundColumn HeaderText="Reason">
						<HeaderStyle HorizontalAlign="Center"></HeaderStyle>
						<ItemStyle HorizontalAlign="Center"></ItemStyle>
					</asp:BoundColumn>
					<asp:BoundColumn HeaderText="Updated By">
						<HeaderStyle HorizontalAlign="Center"></HeaderStyle>
						<ItemStyle HorizontalAlign="Center"></ItemStyle>
					</asp:BoundColumn>
					<asp:BoundColumn HeaderText="Updated Date">
						<HeaderStyle HorizontalAlign="Center"></HeaderStyle>
						<ItemStyle HorizontalAlign="Center"></ItemStyle>
					</asp:BoundColumn>
				</Columns>
				<PagerStyle NextPageText="Next" PrevPageText="Previous" HorizontalAlign="Right" ForeColor="#000066"
					BackColor="White" CssClass="normalText"></PagerStyle>
			</asp:datagrid>
			<FIELDSET style="Z-INDEX: 104; LEFT: 18px; WIDTH: 626px; POSITION: absolute; TOP: 238px; HEIGHT: 295px"
				align="absMiddle"><LEGEND>
					<asp:label id="Label6" CssClass="tableHeadingFieldset" Runat="server">Update History</asp:label></LEGEND><FONT face="Tahoma"><BR>
					&nbsp;&nbsp;</FONT><FONT face="Verdana">&nbsp;&nbsp;&nbsp;</FONT>&nbsp;</FIELDSET>
			<TABLE id="Table3" style="Z-INDEX: 103; LEFT: 17px; WIDTH: 584px; POSITION: absolute; TOP: 37px; HEIGHT: 188px; TEXT-ALIGN: left"
				cellSpacing="0" width="584" align="left" border="0" runat="server">
				<TR height="25">
					<TD style="WIDTH: 138px; HEIGHT: 30px" align="left" width="138"><FONT face="Tahoma">
							<asp:label id="Label52" CssClass="tableLabel" Runat="server"> Customer ID:</asp:label></FONT></TD>
					<TD style="WIDTH: 130px; HEIGHT: 30px" width="130">
						<asp:textbox id="Textbox1" style="TEXT-TRANSFORM: uppercase" tabIndex="4" runat="server" Width="98px"
							CssClass="textField" MaxLength="20"></asp:textbox></TD>
					<TD style="HEIGHT: 30px" width="724">
						<asp:textbox id="Textbox4" style="TEXT-TRANSFORM: uppercase" tabIndex="4" runat="server" Width="270px"
							CssClass="textField" MaxLength="20"></asp:textbox></TD>
				</TR>
				<TR>
					<TD style="WIDTH: 138px; HEIGHT: 30px" align="left" width="138">
						<asp:RadioButton id="RadioButton18" runat="server" Width="92px" Text="Cash" CssClass="tableLabel"></asp:RadioButton>
						<asp:RadioButton id="RadioButton17" runat="server" Width="106px" Text="Credit" CssClass="tableLabel"></asp:RadioButton></TD>
					<TD style="WIDTH: 130px; HEIGHT: 30px" width="130"><FONT face="Tahoma"></FONT></TD>
					<TD style="HEIGHT: 30px" width="724"><FONT face="Tahoma"></FONT></TD>
				</TR>
				<TR>
					<TD style="WIDTH: 138px; HEIGHT: 30px" align="left" width="138">
						<asp:label id="Label1" CssClass="tableLabel" Runat="server">Due Date:</asp:label></TD>
					<TD style="WIDTH: 130px; HEIGHT: 30px" width="130">
						<asp:textbox id="Textbox5" style="TEXT-TRANSFORM: uppercase" tabIndex="4" runat="server" Width="104px"
							CssClass="textField" MaxLength="20"></asp:textbox></TD>
					<TD style="HEIGHT: 30px" width="724"></TD>
				</TR>
				<TR>
					<TD style="WIDTH: 138px; HEIGHT: 30px" align="left" width="138">
						<asp:label id="Label2" CssClass="tableLabel" Runat="server">Actual BPD:</asp:label></TD>
					<TD style="WIDTH: 130px; HEIGHT: 30px" width="130">
						<asp:dropdownlist id="Dropdownlist2" runat="server" Width="104px" AutoPostBack="True"></asp:dropdownlist></TD>
					<TD style="HEIGHT: 30px" width="724"></TD>
				</TR>
				<TR>
					<TD style="WIDTH: 138px; HEIGHT: 30px" align="left" width="138">
						<asp:label id="Label3" CssClass="tableLabel" Runat="server">Promised Payment Date:</asp:label></TD>
					<TD style="WIDTH: 130px; HEIGHT: 30px" width="130">
						<asp:dropdownlist id="Dropdownlist1" runat="server" Width="104px" AutoPostBack="True"></asp:dropdownlist></TD>
					<TD style="HEIGHT: 30px" width="724"></TD>
				</TR>
				<TR height="25">
					<TD style="WIDTH: 138px; HEIGHT: 26px" noWrap align="left" width="138"><FONT face="Tahoma">
							<asp:label id="Label4" CssClass="tableLabel" Runat="server">Current Amount Paid:</asp:label></FONT></TD>
					<TD style="WIDTH: 130px; HEIGHT: 26px" width="130"></TD>
					<TD style="WIDTH: 724px; HEIGHT: 26px" width="724">
						<asp:label id="Label5" CssClass="tableLabel" Runat="server">Nam</asp:label><FONT face="Tahoma">&nbsp;
							<asp:dropdownlist id="Dropdownlist3" runat="server" Width="104px" AutoPostBack="True"></asp:dropdownlist></FONT></TD>
				</TR>
			</TABLE>
			<asp:button id="Button1" style="Z-INDEX: 100; LEFT: 556px; POSITION: absolute; TOP: 84px" runat="server"
				CausesValidation="False" Width="83px" Text="Cancel" CssClass="queryButton"></asp:button>
		</form>
	</body>
</HTML>
