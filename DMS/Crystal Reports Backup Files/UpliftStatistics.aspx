<%@ Page language="c#" Codebehind="UpliftStatistics.aspx.cs" AutoEventWireup="false" Inherits="TIES.WebUI.UpliftStatistics" %>
<%@ Import Namespace="com.common.util" %>
<%@ Import Namespace="Cambro.Web.DbCombo" %>
<%@ Register TagPrefix="dbCombo" Namespace="Cambro.Web.DbCombo" Assembly="Cambro.Web.DbCombo" %>
<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Freight Summary On-forwarding</title>
		<LINK rel="stylesheet" type="text/css" href="css/Styles.css" name="stylesheet">
		<meta name="GENERATOR" content="Microsoft Visual Studio 7.0">
		<meta name="CODE_LANGUAGE" content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<!--#INCLUDE FILE="msFormValidations.inc"-->
		<script language="javascript" src="Scripts/settingScrollPosition.js"></script>
		<script>
		fieldset {
			margin: 0px;
			padding: 0px;
		}
		</script>
	</HEAD>
	<body onunload="window.top.displayBanner.fnCloseAll(0);" MS_POSITIONING="GridLayout">
		<form id="FreightSummary" method="post" runat="server">
			<asp:label style="Z-INDEX: 103; POSITION: absolute; TOP: 10px; LEFT: 20px" id="lblTitle" runat="server"
				Width="365px" Height="27px" CssClass="maintitleSize">Uplift Statistics</asp:label><asp:button style="Z-INDEX: 100; POSITION: absolute; TOP: 54px; LEFT: 19px" id="btnQuery" runat="server"
				Width="64px" Height="20" CssClass="queryButton" Text="Query"></asp:button><asp:label style="Z-INDEX: 105; POSITION: absolute; TOP: 80px; LEFT: 20px" id="lblErrorMessage"
				runat="server" Width="528px" Height="21px" CssClass="errorMsgColor">Place holder for err msg</asp:label>&nbsp;
			<asp:button style="Z-INDEX: 101; POSITION: absolute; TOP: 54px; LEFT: 84px" id="btnGenerate"
				runat="server" Width="120px" Height="20" CssClass="queryButton" Text="Execute Query"></asp:button>
			<table style="MARGIN-TOP: 80px; MARGIN-LEFT: 15px" id="tblShipmentTracking" border="0"
				runat="server">
				<tr>
					<td>
						<fieldset style="HEIGHT: 130px" align="top"><legend><asp:label id="lblDates" runat="server" CssClass="tableHeadingFieldset" Font-Bold="True">Dates</asp:label></legend>
							<table>
								<tr>
									<td colSpan="4"><asp:radiobutton id="rbInvoicedDate" runat="server" Width="128px" Height="21px" CssClass="tableRadioButton"
											Text="Invoiced Date" AutoPostBack="True" GroupName="QueryDate" Checked="True"></asp:radiobutton><asp:radiobutton id="rbManifestedDate" runat="server" Width="128px" Height="21px" CssClass="tableRadioButton"
											Text="Manifested Date" AutoPostBack="True" GroupName="QueryDate"></asp:radiobutton></td>
								</tr>
								<tr>
									<td><asp:radiobutton id="rbMonth" runat="server" Width="73px" Height="21px" CssClass="tableRadioButton"
											Text="Month" AutoPostBack="True" GroupName="EnterDate" Checked="True"></asp:radiobutton></td>
									<td><asp:dropdownlist id="ddMonth" runat="server" Width="88px" Height="19px" CssClass="textField"></asp:dropdownlist></td>
									<td><asp:label id="Label5" runat="server" Width="30px" Height="22px" CssClass="tableLabel">Year</asp:label></td>
									<td><cc1:mstextbox id="txtYear" runat="server" Width="83" CssClass="textField" MaxLength="5" TextMaskType="msNumeric"
											NumberMaxValue="2999" NumberMinValue="1" NumberPrecision="4"></cc1:mstextbox></td>
								</tr>
								<tr>
									<td><asp:radiobutton id="rbPeriod" runat="server" Width="62px" CssClass="tableRadioButton" Text="Period"
											AutoPostBack="True" GroupName="EnterDate"></asp:radiobutton></td>
									<td><cc1:mstextbox id="txtPeriod" runat="server" Width="82" CssClass="textField" MaxLength="10" TextMaskType="msDate"
											TextMaskString="99/99/9999"></cc1:mstextbox></td>
									<td></td>
									<td><cc1:mstextbox style="Z-INDEX: 0" id="txtTo" runat="server" Width="83" CssClass="textField" MaxLength="10"
											TextMaskType="msDate" TextMaskString="99/99/9999"></cc1:mstextbox></td>
								</tr>
								<TR>
									<TD><asp:radiobutton id="rbDate" runat="server" Width="74px" Height="22px" CssClass="tableRadioButton"
											Text="Date" AutoPostBack="True" GroupName="EnterDate"></asp:radiobutton></TD>
									<TD><cc1:mstextbox id="txtDate" runat="server" Width="82" CssClass="textField" MaxLength="10" TextMaskType="msDate"
											TextMaskString="99/99/9999"></cc1:mstextbox></TD>
									<TD></TD>
									<TD></TD>
								</TR>
							</table>
						</fieldset>
					</td>
					<td>
						<FIELDSET style="HEIGHT: 130px" align="top"><legend><asp:label id="Label1" runat="server" CssClass="tableHeadingFieldset" Font-Bold="True">Payer Type</asp:label></legend>
							<table style="WIDTH: 296px; HEIGHT: 109px">
								<tr>
									<td vAlign="top"><asp:label id="Label6" runat="server" Width="88px" Height="22px" CssClass="tableLabel">Payer Type</asp:label></td>
									<td><asp:listbox id="lsbCustType" runat="server" Width="137px" Height="61px" SelectionMode="Multiple"></asp:listbox></td>
								</tr>
								<tr>
									<td><asp:label id="Label10" runat="server" Width="79px" Height="22px" CssClass="tableLabel">Payer Code</asp:label></td>
									<td style="WIDTH: 360px" class="tableLabel"><cc1:mstextbox id="txtPayerCode" runat="server" Width="139px" CssClass="textField" MaxLength="10"
											TextMaskType="msUpperAlfaNumericWithUnderscore"></cc1:mstextbox>&nbsp;&nbsp;
										<asp:button id="btnPayerCode" runat="server" CssClass="searchButton" Text="..." CausesValidation="False"></asp:button></td>
								</tr>
							</table>
						</FIELDSET>
					</td>
				</tr>
				<TR>
					<TD style="Z-INDEX: 0">
						<fieldset style="HEIGHT: 80px"><legend><asp:label id="lbStatisticsType" runat="server" CssClass="tableHeadingFieldset" Font-Bold="True">Statistics Type</asp:label></legend>
							<table>
								<tr>
									<td><asp:radiobutton id="rbtExHub" runat="server" CssClass="tableRadioButton" Text="Ex Hub" AutoPostBack="True"
											GroupName="StatisticsType" Checked="True"></asp:radiobutton></td>
								</tr>
								<TR>
									<TD><asp:radiobutton style="Z-INDEX: 0" id="rbtByBranch" runat="server" CssClass="tableRadioButton" Text="By Branch"
											AutoPostBack="True" GroupName="StatisticsType"></asp:radiobutton></TD>
								</TR>
								<tr>
									<td><asp:radiobutton id="rbtByAirline" runat="server" CssClass="tableRadioButton" Text="By Airline" AutoPostBack="True"
											GroupName="StatisticsType" Visible="False"></asp:radiobutton></td>
								</tr>
								<tr>
									<td><asp:radiobutton id="rbtByAgent" runat="server" CssClass="tableRadioButton" Text="By Agent" AutoPostBack="True"
											GroupName="StatisticsType" Visible="False"></asp:radiobutton></td>
								</tr>
								<tr>
									<td><asp:radiobutton id="rbtSummary" runat="server" CssClass="tableRadioButton" Text="Summary" AutoPostBack="True"
											GroupName="StatisticsType" Visible="False"></asp:radiobutton></td>
								</tr>
							</table>
						</fieldset>
					</TD>
					<TD>
						<FIELDSET style="HEIGHT: 80px"><LEGEND><asp:label id="Label4" runat="server" CssClass="tableHeadingFieldset" Font-Bold="True">Report Type</asp:label></LEGEND>
							<TABLE id="Table1">
								<TR>
									<TD><asp:radiobutton style="Z-INDEX: 0" id="rbtReportTypeWeight" runat="server" CssClass="tableRadioButton"
											Text="Weight" AutoPostBack="True" GroupName="ReportType" Checked="True"></asp:radiobutton></TD>
								</TR>
								<TR>
									<TD><asp:radiobutton style="Z-INDEX: 0" id="rbtReportTypeShipment" runat="server" CssClass="tableRadioButton"
											Text="No. of Shipment" AutoPostBack="True" GroupName="ReportType"></asp:radiobutton></TD>
								</TR>
							</TABLE>
						</FIELDSET>
					</TD>
				</TR>
			</table>
		</form>
	</body>
</HTML>
