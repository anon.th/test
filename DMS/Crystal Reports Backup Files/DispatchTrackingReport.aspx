<%@ Register TagPrefix="dbCombo" Namespace="Cambro.Web.DbCombo" Assembly="Cambro.Web.DbCombo" %>
<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<%@ Page language="c#" Codebehind="DispatchTrackingReport.aspx.cs" AutoEventWireup="false" 
Inherits="com.ties.DispatchTrackingReport" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>DispatchTrackingReport</title>
		<LINK href="css/Styles.css" type="text/css" rel="stylesheet" name="stylesheet">
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<!--#INCLUDE FILE="msFormValidations.inc"-->
		<script language="javascript" src="Scripts/settingScrollPosition.js"></script>
				<script language="javascript">
			function makeUppercase(objId)
			{
				document.getElementById(objId).value = document.getElementById(objId).value.toUpperCase();
			}
		</script>
	</HEAD>
	<body onunload="window.top.displayBanner.fnCloseAll(0);" MS_POSITIONING="GridLayout">
		<form id="DispatchTrackingReport" method="post" runat="server">
			<asp:button id="btnQuery" style="Z-INDEX: 104; LEFT: 16px; POSITION: absolute; TOP: 51px" tabIndex="100"
				runat="server" CssClass="queryButton" Text="Query" Width="64px"></asp:button>
			<TABLE id="tblShipmentTracking" style="Z-INDEX: 105; LEFT: 12px; WIDTH: 800px; POSITION: absolute; TOP: 90px"
				width="800" border="0" runat="server">
				<TR>
					<TD style="WIDTH: 436px; HEIGHT: 137px" vAlign="top">
						<fieldset style="WIDTH: 423px; HEIGHT: 129px"><legend><asp:label id="lblDates" runat="server" CssClass="tableHeadingFieldset" Width="20px" Font-Bold="True">Dates</asp:label></legend>
							<TABLE id="tblDates" style="WIDTH: 406px; HEIGHT: 113px" cellSpacing="0" cellPadding="0"
								align="left" border="0" runat="server">
								<TR>
									<td></td>
									<TD colSpan="3">&nbsp;
										<asp:radiobutton id="rbBookingDate" runat="server" CssClass="tableRadioButton" Text="Booking Date"
											Width="102px" Checked="True" GroupName="QueryByDate" AutoPostBack="True" Height="22px"></asp:radiobutton><asp:radiobutton id="rbEstDate" runat="server" CssClass="tableRadioButton" Text="Estimated Pickup Date"
											Width="151px" GroupName="QueryByDate" AutoPostBack="True" Height="22px"></asp:radiobutton><asp:radiobutton id="rbActDate" runat="server" CssClass="tableRadioButton" Text="Actual Pickup date"
											Width="133px" GroupName="QueryByDate" AutoPostBack="True" Height="22px"></asp:radiobutton></TD>
								</TR>
								<TR>
									<td></td>
									<TD colSpan="3">&nbsp;
										<asp:radiobutton id="rbMonth" runat="server" CssClass="tableRadioButton" Text="Month" Width="73px"
											Checked="True" GroupName="EnterDate" AutoPostBack="True" Height="21px"></asp:radiobutton>&nbsp;
										<asp:dropdownlist id="ddMonth" runat="server" CssClass="textField" Width="88px" Height="19px"></asp:dropdownlist>&nbsp;
										<asp:label id="lblYear" runat="server" CssClass="tableLabel" Width="30px" Height="10px">Year</asp:label>&nbsp;
										<cc1:mstextbox id="txtYear" runat="server" CssClass="textField" Width="83" NumberPrecision="4"
											NumberMinValue="1" NumberMaxValue="2999" TextMaskType="msNumeric" MaxLength="5"></cc1:mstextbox></TD>
								</TR>
								<TR>
									<td style="HEIGHT: 20px"></td>
									<TD style="HEIGHT: 20px" colSpan="3">&nbsp;
										<asp:radiobutton id="rbPeriod" runat="server" CssClass="tableRadioButton" Text="Period" Width="62px"
											GroupName="EnterDate" AutoPostBack="True"></asp:radiobutton>&nbsp;&nbsp;&nbsp;
										<cc1:mstextbox id="txtPeriod" runat="server" CssClass="textField" Width="82" TextMaskType="msDate"
											MaxLength="10" TextMaskString="99/99/9999"></cc1:mstextbox>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
										&nbsp;
										<cc1:mstextbox id="txtTo" runat="server" CssClass="textField" Width="83" TextMaskType="msDate"
											MaxLength="10" TextMaskString="99/99/9999"></cc1:mstextbox></TD>
								</TR>
								<TR>
									<td></td>
									<TD colSpan="3">&nbsp;
										<asp:radiobutton id="rbDate" runat="server" CssClass="tableRadioButton" Text="Date" Width="74px"
											GroupName="EnterDate" AutoPostBack="True" Height="22px"></asp:radiobutton>&nbsp;&nbsp;<cc1:mstextbox id="txtDate" runat="server" CssClass="textField" Width="82" TextMaskType="msDate"
											MaxLength="10" TextMaskString="99/99/9999"></cc1:mstextbox></TD>
								</TR>
							</TABLE>
						</fieldset>
					</TD>
					<TD style="HEIGHT: 137px" vAlign="top">
						<fieldset style="WIDTH: 320px; HEIGHT: 105px"><legend><asp:label id="lblPayerType" runat="server" CssClass="tableHeadingFieldset" Width="79px" Font-Bold="True">Payer Type</asp:label></legend>
							<TABLE id="tblPayerType" style="WIDTH: 300px; HEIGHT: 73px" cellSpacing="0" cellPadding="0"
								align="left" border="0" runat="server">
								<tr>
									<td></td>
									<td colSpan="2" height="1">&nbsp;</td>
									<td></td>
									<td></td>
								</tr>
								<TR>
									<td></td>
									<TD class="tableLabel" vAlign="top" colSpan="2">&nbsp;
										<asp:label id="Label4" runat="server" CssClass="tableLabel" Width="88px" Height="22px">Payer Type</asp:label></TD>
									<td><asp:listbox id="lsbCustType" runat="server" Width="137px" Height="61px" SelectionMode="Multiple"></asp:listbox></td>
									<td></td>
								</TR>
								<TR height="33">
									<td bgColor="blue"></td>
									<TD class="tableLabel" vAlign="top" colSpan="2">&nbsp;
										<asp:label id="lblPayerCode" runat="server" CssClass="tableLabel" Width="79px" Height="22px">Payer Code</asp:label>&nbsp;&nbsp;</TD>
									<td><asp:TextBox id="txtPayerCode" runat="server" Width="148px" CssClass="textField"></asp:TextBox>&nbsp;&nbsp;
										<asp:button id="btnPayerCode" runat="server" CssClass="searchButton" Text="..." CausesValidation="False"></asp:button></td>
									<td></td>
								</TR>
							</TABLE>
						</fieldset>
					</TD>
				</TR>
				<TR>
					<TD style="WIDTH: 436px; HEIGHT: 60px" vAlign="top">
						<fieldset style="WIDTH: 423px; HEIGHT: 81px"><legend><asp:label id="lblRouteType" runat="server" CssClass="tableHeadingFieldset" Width="160px" Font-Bold="True">Pickup Route / DC Selection</asp:label></legend>
							<TABLE id="tblRouteType" style="WIDTH: 406px; HEIGHT: 54px" cellSpacing="0" cellPadding="0"
								align="left" border="0" runat="server">
								<tr>
									<TD class="tableLabel" width="20%">&nbsp;<asp:label id="lblRouteCode" runat="server" CssClass="tableLabel" Width="111px" Height="22px">Route Code</asp:label></TD>
									<td><DBCOMBO:DBCOMBO id="DbComboPathCode" tabIndex="14" CssClass="tableLabel" Width="25px" AutoPostBack="True"
											ServerMethod="DbComboPathCodeSelect" ShowDbComboLink="False" TextUpLevelSearchButton="v" RegistrationKey=" "
											TextBoxColumns="18" Runat="server"></DBCOMBO:DBCOMBO></td>
								</tr>
								<TR>
									<TD class="tableLabel">&nbsp;<asp:label id="Label3" runat="server" CssClass="tableLabel" Width="154px" Height="22px">Origin Distribution Center</asp:label></TD>
									<TD><DBCOMBO:DBCOMBO id="DbComboOriginDC" tabIndex="14" CssClass="tableLabel" Width="25px" AutoPostBack="True"
											ServerMethod="DbComboDistributionCenterSelect" ShowDbComboLink="False" TextUpLevelSearchButton="v"
											RegistrationKey=" " TextBoxColumns="18" Runat="server"></DBCOMBO:DBCOMBO></TD>
								</TR>
							</TABLE>
						</fieldset>
					</TD>
					<TD style="HEIGHT: 60px" valign="top">
						<fieldset style="WIDTH: 200px; HEIGHT: 50px"><legend><asp:label id="Label5" runat="server" CssClass="tableHeadingFieldset" Width="79px" Font-Bold="True">Cut-Off Time</asp:label></legend>
							<TABLE id="Table1" style="WIDTH: 200px; HEIGHT: 24px" cellSpacing="0" cellPadding="0" align="left"
								border="0" runat="server">
								<TR height="33">
									<td bgColor="blue"></td>
									<TD class="tableLabel" valign="middle" colSpan="2" style="WIDTH: 8px"><FONT face="Tahoma">&nbsp;&nbsp;&nbsp;&nbsp;HH:mm&nbsp;&nbsp;</FONT></TD>
									<td><cc1:mstextbox id="txtCutoffTime" runat="server" Width="103px" CssClass="textField" MaxLength="5"
											TextMaskString="99:99" TextMaskType="msTime"></cc1:mstextbox>&nbsp;&nbsp;</td>
									<td><FONT face="Tahoma"></FONT></td>
								</TR>
							</TABLE>
						</fieldset>
					</TD>
				</TR>
				<TR>
					<TD style="HEIGHT: 72px" vAlign="top" colSpan="2">
						<fieldset style="WIDTH: 566px"><legend><asp:label id="Label1" Runat="server">Party Details</asp:label></legend>
							<table id="tblDates1" style="WIDTH: 566px" align="left" border="0" runat="server">
								<TR height="27">
									<TD style="HEIGHT: 5px"><asp:label id="lblSenderName" runat="server" CssClass="tableLabel" Width="88px" Height="22px">Sender Name</asp:label></TD>
									<TD style="HEIGHT: 5px"><dbcombo:dbcombo id="dbCmbSenderName" tabIndex="13" runat="server" Width="91px" AutoPostBack="True"
											Height="17px" ServerMethod="SenderNameServerMethod" ShowDbComboLink="False" TextUpLevelSearchButton="v" TextBoxColumns="12"></dbcombo:dbcombo></TD>
									<TD style="HEIGHT: 5px">&nbsp;</TD>
									<TD style="HEIGHT: 5px">&nbsp;</TD>
								</TR>
								<TR height="15">
									<TD><asp:label id="lblBookingNo" runat="server" CssClass="tableLabel" Width="78px" Height="22px">Booking No</asp:label></TD>
									<TD><dbcombo:dbcombo id="dbCmbBookingNo" tabIndex="13" runat="server" Width="91px" AutoPostBack="True"
											Height="17px" ServerMethod="BookingNoServerMethod" ShowDbComboLink="False" TextUpLevelSearchButton="v"
											TextBoxColumns="12"></dbcombo:dbcombo></TD>
									<TD><asp:label id="lblDispatchType" runat="server" CssClass="tableLabel" Width="92px" Height="22px">Dispatch Type</asp:label></TD>
									<TD><asp:dropdownlist id="ddDelvType" tabIndex="4" runat="server" CssClass="textField" Width="109px"></asp:dropdownlist></TD>
								</TR>
							</table>
						</fieldset>
					</TD>
				</TR>
				<tr>
					<td vAlign="top" colSpan="2">
						<fieldset style="WIDTH: 571px; HEIGHT: 55px"><legend><asp:label id="Label2" Runat="server">Status Description</asp:label></legend>
							<table id="tblDates2" style="WIDTH: 566px; HEIGHT: 37px" align="left" border="0" runat="server">
								<TR>
									<TD class="tableLabel" style="WIDTH: 116px" colSpan="3"><asp:label id="lblStatusCode" runat="server" CssClass="tableLabel" Width="84px" Height="22px">Status Code</asp:label></TD>
									<TD class="tableLabel" style="WIDTH: 186px" colSpan="6"><dbcombo:dbcombo id="dbCmbStatusCode" tabIndex="13" runat="server" Width="91px" AutoPostBack="True"
											Height="17px" ServerMethod="StatusCodeServerMethod" ShowDbComboLink="False" TextUpLevelSearchButton="v" TextBoxColumns="12"></dbcombo:dbcombo></TD>
									<TD class="tableLabel" colSpan="3"><asp:label id="lblExceptionCode" runat="server" CssClass="tableLabel" Width="102px" Height="22px">Exception Code</asp:label></TD>
									<TD class="tableLabel" colSpan="6"><dbcombo:dbcombo id="dbCmbExceptionCode" tabIndex="13" runat="server" Width="91px" AutoPostBack="True"
											Height="17px" ServerMethod="ExceptionCodeServerMethod" ShowDbComboLink="False" TextUpLevelSearchButton="v" TextBoxColumns="12"></dbcombo:dbcombo></TD>
								</TR>
							</table>
						</fieldset>
					</td>
				</tr>
			</TABLE>
			<asp:label id="lblErrorMessage" style="Z-INDEX: 103; LEFT: 16px; POSITION: absolute; TOP: 72px"
				runat="server" CssClass="errorMsgColor" Width="616px" Height="30px">Place holder for err msg</asp:label><asp:label id="lblTitle" style="Z-INDEX: 102; LEFT: 15px; POSITION: absolute; TOP: 16px" runat="server"
				CssClass="maintitleSize" Width="558px" Height="32px"> Dispatch Tracking</asp:label><asp:button id="btnGenerate" style="Z-INDEX: 101; LEFT: 81px; POSITION: absolute; TOP: 50px"
				runat="server" CssClass="queryButton" Text="Generate" Width="82px"></asp:button>
		</form>
	</body>
</HTML>
