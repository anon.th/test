<%@ Page language="c#" Codebehind="MAWBOverallStatusPopup.aspx.cs" AutoEventWireup="false" Inherits="TIES.WebUI.MAWBOverallStatusPopup" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Master AWB Overall Status</title>
		<meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK rel="stylesheet" type="text/css" href="css/Styles.css" name="stylesheet">
		<script language="javascript" type="text/javascript">
			function fnOnclose()
			{
				//window.opener.CreateUpdateConsignment.submit();
				window.opener.callback();                
			}      
		</script>
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<DIV style="Z-INDEX: 0; POSITION: relative; WIDTH: 760px; HEIGHT: 217px; TOP: 34px; LEFT: 22px"
				MS_POSITIONING="GridLayout">
				<TABLE style="Z-INDEX: 112" id="Table5" border="0" width="760" runat="server">
					<tr>
						<td align="left"><asp:label id="Label65" runat="server" Width="421px" Height="27px" CssClass="mainTitleSize">
							Master AWB Overall Status</asp:label></td>
					</tr>
					<tr class="gridHeading">
						<td><STRONG><FONT size="2">AWB Details</FONT></STRONG></td>
					</tr>
					<tr>
						<td>
							<asp:datagrid style="Z-INDEX: 0" id="GrdAWBDetails" tabIndex="21" runat="server" Width="100%"
								AutoGenerateColumns="False" DataKeyField="MAWBNumber" AlternatingItemStyle-CssClass="gridField"
								ItemStyle-CssClass="gridField" HeaderStyle-CssClass="gridHeading" CellPadding="0" CellSpacing="1"
								BorderWidth="0px" FooterStyle-CssClass="gridHeading" ShowFooter="True">
								<FooterStyle CssClass="gridHeading"></FooterStyle>
								<AlternatingItemStyle CssClass="gridField"></AlternatingItemStyle>
								<ItemStyle CssClass="gridField"></ItemStyle>
								<HeaderStyle CssClass="gridHeading"></HeaderStyle>
								<Columns>
									<asp:TemplateColumn HeaderText="Number">
										<HeaderStyle HorizontalAlign="Center" Width="110px"></HeaderStyle>
										<ItemStyle HorizontalAlign="Center"></ItemStyle>
										<ItemTemplate>
											<asp:LinkButton ID="lnkMAWBNumber" runat="server" Visible="True" CausesValidation="false" CommandName="MAWBNumber" Text='<%#DataBinder.Eval(Container.DataItem,"MAWBNumber")%>'>
											</asp:LinkButton>
										</ItemTemplate>
										<FooterStyle HorizontalAlign="Left"></FooterStyle>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Consolidation Shipper">
										<HeaderStyle HorizontalAlign="Center" Width="160px"></HeaderStyle>
										<ItemStyle HorizontalAlign="Center"></ItemStyle>
										<ItemTemplate>
											<%# DataBinder.Eval(Container.DataItem,"ConsolidatorName") %>
										</ItemTemplate>
										<FooterStyle HorizontalAlign="Left"></FooterStyle>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Expected Arrival">
										<HeaderStyle HorizontalAlign="Center" Width="115px"></HeaderStyle>
										<ItemStyle HorizontalAlign="Center"></ItemStyle>
										<ItemTemplate>
											<%# DataBinder.Eval(Container.DataItem,"ExpectedArrival","{0:dd/MM/yyyy HH:mm}") %>
										</ItemTemplate>
										<FooterStyle HorizontalAlign="Center"></FooterStyle>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Ship/Flight No">
										<HeaderStyle HorizontalAlign="Center" Width="115px"></HeaderStyle>
										<ItemStyle HorizontalAlign="Center"></ItemStyle>
										<ItemTemplate>
											<%# DataBinder.Eval(Container.DataItem,"ShipFlightNo") %>
										</ItemTemplate>
										<FooterStyle HorizontalAlign="Center"></FooterStyle>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="No. Cons">
										<HeaderStyle HorizontalAlign="Center" Width="60px"></HeaderStyle>
										<ItemStyle HorizontalAlign="Center"></ItemStyle>
										<ItemTemplate>
											<asp:label id="Label2" runat="server">
												<%# DataBinder.Eval(Container.DataItem,"NoCons", "{0:#,##0}") %>
											</asp:label>
										</ItemTemplate>
										<FooterStyle HorizontalAlign="Center"></FooterStyle>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="No. Pkgs">
										<HeaderStyle HorizontalAlign="Center" Width="60px"></HeaderStyle>
										<ItemStyle HorizontalAlign="Center"></ItemStyle>
										<ItemTemplate>
											<asp:label id="Label3" runat="server">
												<%# DataBinder.Eval(Container.DataItem,"NoPkgs", "{0:#,##0}") %>
											</asp:label>
										</ItemTemplate>
										<FooterStyle HorizontalAlign="Center"></FooterStyle>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Rec'd Cons">
										<HeaderStyle HorizontalAlign="Center" Width="70px"></HeaderStyle>
										<ItemStyle HorizontalAlign="Center"></ItemStyle>
										<ItemTemplate>
											<asp:label id="Label4" runat="server">
												<%# DataBinder.Eval(Container.DataItem,"ReceivedCons", "{0:#,##0}") %>
											</asp:label>
										</ItemTemplate>
										<FooterStyle HorizontalAlign="Center"></FooterStyle>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Rec'd Pkgs">
										<HeaderStyle HorizontalAlign="Center" Width="70px"></HeaderStyle>
										<ItemStyle HorizontalAlign="Center"></ItemStyle>
										<ItemTemplate>
											<asp:label id="Label5" runat="server">
												<%# DataBinder.Eval(Container.DataItem,"ReceivedPkgs", "{0:#,##0}") %>
											</asp:label>
										</ItemTemplate>
										<FooterStyle HorizontalAlign="Center"></FooterStyle>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Total Weight">
										<HeaderStyle HorizontalAlign="Center" Width="70px"></HeaderStyle>
										<ItemStyle HorizontalAlign="Center"></ItemStyle>
										<ItemTemplate>
											<asp:label id="Label1" runat="server">
												<%# DataBinder.Eval(Container.DataItem,"TotalWeight", "{0:#,##0}") %>
											</asp:label>
										</ItemTemplate>
										<FooterStyle Font-Bold="True" HorizontalAlign="Center"></FooterStyle>
										<FooterTemplate>
											<asp:label id="lblTotalWeight" runat="server">
											</asp:label>
										</FooterTemplate>
									</asp:TemplateColumn>
								</Columns>
							</asp:datagrid>
						</td>
					</tr>
				</TABLE>
			</DIV>
		</form>
	</body>
</HTML>
