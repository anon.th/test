<%@ Page language="c#" Codebehind="AssignSenderLocation.aspx.cs" AutoEventWireup="false" Inherits="TIES.WebUI.AssignSenderLocation" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Assign Sender Location</title>
		<meta name="vs_snapToGrid" content="True">
		<meta name="vs_showGrid" content="True">
		<LINK rel="stylesheet" type="text/css" href="css/Styles.css" name="stylesheet">
		<meta name="GENERATOR" content="Microsoft Visual Studio 7.0">
		<meta name="CODE_LANGUAGE" content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<!--#INCLUDE FILE="msFormValidations.inc"-->
		<script language="javascript" src="Scripts/settingScrollPosition.js"></script>
		<script language="javascript" src="Scripts/JScript_TextControl.js"></script>
		<script language="javascript" src="Scripts/WorkProgress.js"></script>
	</HEAD>
	<body onunload="window.top.displayBanner.fnCloseAll(0);" MS_POSITIONING="GridLayout">
		<form id="ConsignmentsWithoutPkgWeights" method="post" runat="server">
			<INPUT style="Z-INDEX: 101; POSITION: absolute; TOP: 680px; LEFT: 16px" type="hidden" name="ScrollPosition">
			<asp:validationsummary style="Z-INDEX: 105; POSITION: absolute; COLOR: red; TOP: 672px; LEFT: 240px" id="Validationsummary1"
				Runat="server" DisplayMode="BulletList" ShowMessageBox="True" HeaderText="Please enter the missing  fields."
				ShowSummary="False"></asp:validationsummary>
			<div style="Z-INDEX: 102; POSITION: relative; HEIGHT: 1248px; TOP: 16px; LEFT: 14px"
				id="divMain" MS_POSITIONING="GridLayout" runat="server">
				<TABLE style="Z-INDEX: 101; POSITION: absolute; TOP: 8px; LEFT: 0px" id="MainTable" border="0"
					width="100%" runat="server">
					<tr>
						<td colSpan="2" align="left"><asp:label id="Label1" runat="server" CssClass="mainTitleSize" Height="27px" Width="421px">
								Assign Sender Location</asp:label></td>
					</tr>
					<tr>
						<td style="HEIGHT: 24px" vAlign="top" colSpan="2" align="left"><asp:button style="DISPLAY: none; VISIBILITY: hidden" id="btnDelete" runat="server" CssClass="queryButton"
								Width="62px" CausesValidation="False" Text="Delete"></asp:button></td>
					</tr>
					<tr>
						<td vAlign="top" colSpan="2" align="left">
							<table border="0" cellSpacing="1" cellPadding="1">
								<tr>
									<td><asp:label style="Z-INDEX: 0" id="Label3" runat="server" CssClass="tableLabel" Width="55px">Payer ID
										</asp:label></td>
									<td colSpan="3"><asp:dropdownlist style="Z-INDEX: 0" id="Drp_Location" runat="server" CssClass="textField" AutoPostBack="False"
											Enabled="False">
											<asp:ListItem Value="20650">20650</asp:ListItem>
										</asp:dropdownlist></td>
									<td></td>
									<td></td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td colSpan="2"><asp:label style="Z-INDEX: 0" id="lblErrorMsg" runat="server" CssClass="errorMsgColor" Height="19px"
								Width="566px" ForeColor="Black"></asp:label></td>
					</tr>
					<tr>
						<td colSpan="2"><asp:datagrid style="Z-INDEX: 0" id="dgLodgments" runat="server" Height="412px" Width="800px"
								AllowPaging="True" SelectedItemStyle-CssClass="gridFieldSelected" PageSize="25" AutoGenerateColumns="False"
								HorizontalAlign="Left">
								<SelectedItemStyle CssClass="gridFieldSelected"></SelectedItemStyle>
								<ItemStyle Height="15px" CssClass="gridField"></ItemStyle>
								<HeaderStyle Font-Bold="True" HorizontalAlign="Center" Height="40px" Width="2%" CssClass="gridHeading"></HeaderStyle>
								<Columns>
									<asp:ButtonColumn Text="&lt;IMG SRC='images/butt-edit.gif' border='0' title='Select' &gt;" CommandName="Select">
										<ItemStyle HorizontalAlign="Center" Height="25px" Width="40px"></ItemStyle>
									</asp:ButtonColumn>
									<asp:TemplateColumn HeaderText="Master">
										<ItemStyle HorizontalAlign="Center" Height="25px" Width="70px"></ItemStyle>
										<ItemTemplate>
											<asp:CheckBox Enabled="False" Runat="server" ID="Checkbox1" Checked='<%# DataBinder.Eval(Container.DataItem,"Master").ToString().Equals("true") %>'>
											</asp:CheckBox>
										</ItemTemplate>
										<EditItemTemplate>
											<asp:Label>test</asp:Label>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:BoundColumn DataField="Account" HeaderText="Account">
										<HeaderStyle Width="120px"></HeaderStyle>
										<ItemStyle HorizontalAlign="Center"></ItemStyle>
									</asp:BoundColumn>
									<asp:BoundColumn DataField="Username" HeaderText="Username">
										<HeaderStyle Width="120px"></HeaderStyle>
										<ItemStyle HorizontalAlign="Center"></ItemStyle>
									</asp:BoundColumn>
									<asp:BoundColumn DataField="Minimum Copies" HeaderText="Minimum Copies">
										<HeaderStyle Width="100px"></HeaderStyle>
										<ItemStyle HorizontalAlign="Center"></ItemStyle>
									</asp:BoundColumn>
									<asp:TemplateColumn HeaderText="Sender Name">
										<ItemStyle HorizontalAlign="Center"></ItemStyle>
										<ItemTemplate>
											<asp:DropDownList id="DropDownList1" runat="server" Width="100px">
												<asp:ListItem>BILLING</asp:ListItem>
											</asp:DropDownList>
											<asp:DropDownList id="Dropdownlist2" runat="server" Width="100px">
												<asp:ListItem>BANKSPLAE</asp:ListItem>
											</asp:DropDownList>
										</ItemTemplate>
										<EditItemTemplate>
											<asp:Label>test</asp:Label>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:BoundColumn DataField="Sender Address" HeaderText="Sender Address">
										<HeaderStyle Width="100px"></HeaderStyle>
										<ItemStyle HorizontalAlign="Center"></ItemStyle>
									</asp:BoundColumn>
									<asp:BoundColumn DataField="Postal Code" HeaderText="Postal Code">
										<HeaderStyle Width="100px"></HeaderStyle>
										<ItemStyle HorizontalAlign="Center"></ItemStyle>
									</asp:BoundColumn>
								</Columns>
								<PagerStyle NextPageText="Next" PrevPageText="Previous" HorizontalAlign="Left" CssClass="normalText"></PagerStyle>
							</asp:datagrid><asp:datagrid style="Z-INDEX: 0" id="Datagrid1" runat="server" Height="412px" Width="800px" AllowPaging="True"
								SelectedItemStyle-CssClass="gridFieldSelected" PageSize="25" AutoGenerateColumns="False" HorizontalAlign="Left">
								<SelectedItemStyle CssClass="gridFieldSelected"></SelectedItemStyle>
								<ItemStyle Height="15px" CssClass="gridField"></ItemStyle>
								<HeaderStyle Font-Bold="True" HorizontalAlign="Center" Height="40px" Width="2%" CssClass="gridHeading"></HeaderStyle>
								<Columns>
									<asp:BoundColumn>
										<ItemStyle HorizontalAlign="Center" Height="25px" Width="40px"></ItemStyle>
									</asp:BoundColumn>
									<asp:BoundColumn HeaderText="Master">
										<ItemStyle HorizontalAlign="Center" Height="25px" Width="70px"></ItemStyle>
									</asp:BoundColumn>
									<asp:BoundColumn DataField="Account" HeaderText="Account">
										<HeaderStyle Width="120px"></HeaderStyle>
										<ItemStyle HorizontalAlign="Center"></ItemStyle>
									</asp:BoundColumn>
									<asp:BoundColumn DataField="Username" HeaderText="Username">
										<HeaderStyle Width="120px"></HeaderStyle>
										<ItemStyle HorizontalAlign="Center"></ItemStyle>
									</asp:BoundColumn>
									<asp:BoundColumn DataField="Minimum Copies" HeaderText="Minimum Copies">
										<HeaderStyle Width="100px"></HeaderStyle>
										<ItemStyle HorizontalAlign="Center"></ItemStyle>
									</asp:BoundColumn>
									<asp:BoundColumn HeaderText="Sender Name">
										<HeaderStyle Width="100px"></HeaderStyle>
										<ItemStyle HorizontalAlign="Center"></ItemStyle>
									</asp:BoundColumn>
									<asp:BoundColumn DataField="Sender Address" HeaderText="Sender Address">
										<HeaderStyle Width="100px"></HeaderStyle>
										<ItemStyle HorizontalAlign="Center"></ItemStyle>
									</asp:BoundColumn>
									<asp:BoundColumn DataField="Postal Code" HeaderText="Postal Code">
										<HeaderStyle Width="100px"></HeaderStyle>
										<ItemStyle HorizontalAlign="Center"></ItemStyle>
									</asp:BoundColumn>
								</Columns>
								<PagerStyle NextPageText="Next" PrevPageText="Previous" HorizontalAlign="Left" CssClass="normalText"></PagerStyle>
							</asp:datagrid></td>
					</tr>
				</TABLE>
			</div>
			<INPUT type="hidden" name="hdnRefresh">
			<asp:button style="Z-INDEX: 105; POSITION: absolute; DISPLAY: none; TOP: 1064px; LEFT: 672px"
				id="btnHidReport" runat="server" Text=".."></asp:button></form>
	</body>
</HTML>
