<%@ Page language="c#" Codebehind="ImportCustomerFile.aspx.cs" AutoEventWireup="false" Inherits="TIES.WebUI.ImportCustomerFile" %>
<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<%@ Import Namespace="com.common.util" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Import Customer File</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../css/Styles.css" type="text/css" rel="Stylesheet">
		<!--#INCLUDE FILE="../msFormValidations.inc"-->
		<script language="javascript" src="../Scripts/settingScrollPosition.js"></script>
	</HEAD>
	<body onkeypress="microsoftKeyPress()" onclick="GetScrollPosition();" onload="SetScrollPosition();"
		onunload="window.top.displayBanner.fnCloseAll(0);" MS_POSITIONING="ViewLayout">
		<form id="frmImportCustomer" method="post" runat="server">
			<INPUT id="ServerAction" type="hidden" name="ServerAction">
			<table cellSpacing="0" cellPadding="0" width="100%" border="0">
				<tr>
					<td style="WIDTH: 815px">
						<table class="TextBody" cellSpacing="0" cellPadding="3" width="100%" border="0">
							<tr>
								<td colSpan="2"><asp:label id="lblMainTitle" runat="server" Width="477px" CssClass="mainTitleSize"> Import Customer File</asp:label></td>
							</tr>
							<tr style="HEIGHT: 5px">
								<td colSpan="2"><FONT face="Tahoma"></FONT></td>
							</tr>
							<tr>
								
								<td align="center" colspan="2">
									&nbsp;
									<asp:button class="queryButton" id="btnImport" runat="server" Width="60" Text="Import" Height="20"></asp:button>&nbsp;
									<asp:button class="queryButton" id="btnClear" runat="server" Width="60" Text="Clear" Height="20"></asp:button></td>
							</tr>
							<tr>
								<td colSpan="2"><asp:label id="ErrorMsg" runat="server" Width="643px" CssClass="errorMsgColor" Height="3px"></asp:label></td>
							</tr>
							<tr class="gridHeading">
								<td colSpan="2"><STRONG><FONT size="3">Import Customer file</FONT></STRONG></td>
							</tr>
							<tr>
								<td style="WIDTH: 120px; HEIGHT: 2px" align="right">&nbsp;</td>
								<td style="WIDTH: 900px; HEIGHT: 2px" align="left">&nbsp; Select Excel 
									File&nbsp;:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<INPUT id="file_upload" style="WIDTH: 500px; HEIGHT: 24px" type="file" size="38" name="file_upload"
										runat="server">
								</td>
							</tr>
							<tr style="HEIGHT: 5px">
								<td colSpan="2">&nbsp;</td>
							</tr>
							<tr>
								<td colSpan="2">
									<TABLE cellSpacing="0" cellPadding="0" width="100%" align="center" border="0">
										<tr>
											<td style="WIDTH: 10%"><FONT face="Tahoma"></FONT></td>
											<td style="WIDTH: 35%" vAlign="top"><asp:panel id="pnlCheckStep" runat="server">
													<FIELDSET style="WIDTH: 337px; HEIGHT: 224px"><LEGEND><STRONG>Check Step</STRONG></LEGEND><BR>
														<TABLE cellSpacing="0" cellPadding="0" width="100%" align="center" border="0">
															<TR style="HEIGHT: 20px">
																<TD style="WIDTH: 82.1%; HEIGHT: 38px" align="left"><FONT face="Tahoma">&nbsp;&nbsp;&nbsp;
																	</FONT>
																	<asp:Label id="lblCheck1" runat="server" Font-Size="14px" Font-Names="Microsoft Sans Serif">1. Check Excel File Format</asp:Label></TD>
																<TD style="HEIGHT: 38px; WDTH: 80%" vAlign="middle" align="center">
																	<asp:Image id="imgChkFormateFile" runat="server" Width="20px" Height="20px" Visible="False"
																		BorderWidth="0px"></asp:Image></TD>
															</TR>
															<TR style="HEIGHT: 20px">
																<TD style="WIDTH: 321px; HEIGHT: 38px" align="left"><FONT face="Tahoma">&nbsp;&nbsp;&nbsp;
																	</FONT>
																	<asp:Label id="lblCheck3" runat="server" Font-Size="14px" Font-Names="Microsoft Sans Serif">2. Check Required Data in Excel file</asp:Label></TD>
																<TD style="HEIGHT: 38px" vAlign="middle" align="center"><FONT face="Tahoma">
																		<asp:Image id="imgChkRequired" runat="server" Width="20px" Height="20px" Visible="False" BorderWidth="0px"></asp:Image></FONT></TD>
															</TR>
															<TR style="HEIGHT: 20px">
																<TD style="WIDTH: 321px; HEIGHT: 40px" align="left"><FONT face="Tahoma">&nbsp;&nbsp;&nbsp;
																	</FONT>
																	<asp:Label id="lblCheck4" runat="server" Font-Size="14px" Font-Names="Microsoft Sans Serif">3. Check data type</asp:Label></TD>
																<TD style="HEIGHT: 40px" vAlign="middle" align="center">
																	<asp:Image id="imgChkDataType" runat="server" Width="20px" Height="20px" Visible="False" BorderWidth="0px"></asp:Image></TD>
															</TR>
															<TR style="HEIGHT: 20px">
																<TD style="WIDTH: 321px; HEIGHT: 37px" align="left"><FONT face="Tahoma">&nbsp;&nbsp;&nbsp;
																	</FONT>
																	<asp:Label id="lblCheck6" runat="server" Font-Size="14px" Font-Names="Microsoft Sans Serif">4. Data has been imported successfully</asp:Label></TD>
																<TD style="HEIGHT: 37px" vAlign="middle" align="center">
																	<asp:Image id="imgChkImportComplete" runat="server" Width="20px" Height="20px" Visible="False"
																		BorderWidth="0px"></asp:Image></TD>
															</TR>
														</TABLE>
													</FIELDSET>
												</asp:panel></td>
											<td style="WIDTH: 5%">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
											<td style="WIDTH: 40%" vAlign="top"><asp:panel id="pnlDescription" runat="server"><!--FIELDSET><LEGEND><STRONG>Description</STRONG></LEGEND><BR>
														<TABLE cellSpacing="0" width="100%" align="center" border="0">
															<TR>
																<TD>
																	<asp:Label id="lblDescription" runat="server">Process Detail Here...</asp:Label></TD>
															</TR>
															<TR style="HEIGHT: 25px">
																<TD>&nbsp;</TD>
															</TR>
														</TABLE>
													</FIELDSET--></asp:panel><asp:panel id="pnlException" runat="server">
													<FIELDSET style="WIDTH: 559px; HEIGHT: 232px"><LEGEND><STRONG>Exception Message</STRONG></LEGEND><BR>
														<TABLE cellSpacing="0" cellPadding="0" width="100%" align="center" border="0">
															<TR>
																<TD>
																	<asp:TextBox id="txtException" runat="server" Width="550px" Height="168px" ForeColor="Red" TextMode="MultiLine"></asp:TextBox></TD>
															</TR>
														</TABLE>
													</FIELDSET>
												</asp:panel></td>
											<td style="WIDTH: 74.16%"><FONT face="Tahoma"></FONT></td>
										</tr>
									</TABLE>
								</td>
							</tr>
							<tr>
								<td vAlign="top" style="WIDTH: 120px"></td>
								<td></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
