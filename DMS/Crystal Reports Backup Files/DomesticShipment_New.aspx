<%@ Page language="c#" Codebehind="DomesticShipment_New.aspx.cs" AutoEventWireup="false" Inherits="com.ties.DomesticShipment_New" smartNavigation="False" %>
<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<%@ Import Namespace="com.common.util" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>DomesticShipment</title>
		<meta name="vs_snapToGrid" content="True">
		<meta name="vs_showGrid" content="True">
		<LINK rel="stylesheet" type="text/css" href="css/Styles.css" name="stylesheet">
		<meta name="GENERATOR" content="Microsoft Visual Studio 7.0">
		<meta name="CODE_LANGUAGE" content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<!--#INCLUDE FILE="msFormValidations.inc"-->
		<script language="javascript" src="Scripts/settingScrollPosition.js"></script>
		<script language="javascript" src="Scripts/JScript_TextControl.js"></script>
		<script language="javascript" type="text/javascript">
			function callCodeBehind(cusid)
			{//debugger;
				var btn = document.getElementById('btnClientEvent');
				if(btn != null)
				{
					document.getElementById("txtCustID").value=cusid;
					btn.click();
				}else{
					alert('error call code behind');
				}
			}
			
			function callCodeBehindSvcCode()
			{		//debugger;									
				__doPostBack('<%= txtShipPckUpTime.ClientID  %>', '');				
			}
			
			function CalculateTotalAmtFunction()
			{//debugger;
				//var btn = document.getElementById("btnCmd");
				//btn.click();
				var decInsuranceChrg = 0;
				var decFrghtChrg = 0;
				var decTotOtherChrg = 0;
				var decTotVASSurcharge = 0;
				var decESASSurcharge = 0;
				var decTotalAmt = 0;
				
				var InsChrgValue= document.getElementById("txtInsChrg").value;
				var FreightChrgValue= document.getElementById("txtFreightChrg").value;
				var OtherSurcharge2Value= document.getElementById("txtOtherSurcharge2").value;
				var TotVASSurChrgValue= document.getElementById("txtTotVASSurChrg").value;
				var ESASurchrgValue= document.getElementById("txtESASurchrg").value;
				var objInsChrg= document.getElementById("txtShpTotAmt");
				
				if(InsChrgValue.length>0)
				{
					decInsuranceChrg = parseFloat(InsChrgValue);
				}
				if(FreightChrgValue.length>0)
				{
					decFrghtChrg = parseFloat(FreightChrgValue);
				}
				if(OtherSurcharge2Value.length>0)
				{
					decTotOtherChrg = parseFloat(OtherSurcharge2Value);
				}
				if(TotVASSurChrgValue.length>0)
				{
					decTotVASSurcharge = parseFloat(TotVASSurChrgValue);
				}
				if(ESASurchrgValue.length>0)
				{
					decESASSurcharge = parseFloat(ESASurchrgValue);
				}
				decTotalAmt = Math.round(decTotVASSurcharge + decFrghtChrg + decInsuranceChrg + decTotOtherChrg + decESASSurcharge);
				objInsChrg.value = parseFloat(decTotalAmt).toFixed(0);
				
			}
			function ValidateConNo(obj)
			{//debugger;
				var btn = document.getElementById('btnExecQry');
				if(btn != null && btn.disabled ==true )
				{
					__doPostBack('hddConNo','');
					return true;
				}else{
					return false;
				}								
			}
			
			function ExecuteQueryConNo()
			{
				var btn = document.getElementById('btnExecQry');
				if(btn != null)
				{
					btn.disabled = true;
					__doPostBack('ExecuteQuery','');
					//return true;
				}
				//else{
				//	return false;
				//}
			}
			
		</script>
	</HEAD>
	<body onclick="GetScrollPosition();" onunload="window.top.displayBanner.fnCloseAll(0);"
		onload="SetScrollPosition();" MS_POSITIONING="GridLayout">
		<form id="DomesticShipment" method="post" name="DomesticShipment" runat="server">
			<INPUT style="Z-INDEX: 108; POSITION: absolute; DISPLAY: none; TOP: 48px; LEFT: 144px"
				id="ExecuteQuery" type="button" name="hddCon" runat="server" CausesValidation="False">
			<asp:textbox style="Z-INDEX: 0" id="Textbox4" tabIndex="126" runat="server" Visible="False" ReadOnly="True"
				CssClass="textFieldRightAlign" Width="131px"></asp:textbox><input style="DISPLAY: none" id="hddConNo" type="button" name="hddCon" runat="server" CausesValidation="False">
			<P><asp:label style="Z-INDEX: 107; POSITION: absolute; TOP: 8px; LEFT: 8px" id="lblMainTitle"
					runat="server" CssClass="mainTitleSize" Width="477px">Domestic Shipment</asp:label></P>
			<DIV style="Z-INDEX: 102; POSITION: relative; WIDTH: 1225px; HEIGHT: 1602px; TOP: 16px; LEFT: -1px"
				id="divMain" MS_POSITIONING="GridLayout" runat="server">
				<TABLE style="Z-INDEX: 101; POSITION: absolute; WIDTH: 1064px; HEIGHT: 1309px; TOP: 8px; LEFT: 0px"
					id="MainTable" border="0" width="1064" runat="server">
					<TR>
						<TD style="HEIGHT: 70px" width="738">
							<TABLE border="0" cellSpacing="0" cellPadding="0" width="738">
								<tr>
									<td width="61"><asp:button id="btnQry" runat="server" CssClass="queryButton" Width="61px" CausesValidation="False"
											Text="Query"></asp:button></td>
									<td width="130">
										<%--<asp:button id="btnExecQry" runat="server" CssClass="queryButton" Width="130px" CausesValidation="False"
											Text="Execute Query"></asp:button>--%>
										<INPUT style="Z-INDEX: 0" id="btnExecQry" type="button" name="ExecQury" onclick="ExecuteQueryConNo();"
											class="queryButton" runat="server" value="Execute Query" CausesValidation="False">
									</td>
									<td width="62"><asp:button id="btnInsert" runat="server" CssClass="queryButton" Width="62px" CausesValidation="False"
											Text="Insert"></asp:button></td>
									<td width="66"><asp:button id="btnSave" runat="server" CssClass="queryButton" Width="66px" Text="Save"></asp:button></td>
									<td><asp:button style="DISPLAY: none; VISIBILITY: hidden" id="btnDelete" runat="server" CssClass="queryButton"
											Width="62px" CausesValidation="False" Text="Delete"></asp:button><asp:button style="DISPLAY: none" id="btnClientEvent" runat="server" CausesValidation="False"
											Text="ClientEvent"></asp:button><asp:button style="Z-INDEX: 0" id="btnPrintConsNote" runat="server" CssClass="queryButton" Width="130px"
											CausesValidation="False" Text="Print Con Note"></asp:button></td>
									<td width="238"></td>
									<td width="24"><asp:button id="btnGoToFirstPage" runat="server" CssClass="queryButton" Width="24px" CausesValidation="False"
											Text="|<"></asp:button></td>
									<td width="24"><asp:button id="btnPreviousPage" runat="server" CssClass="queryButton" Width="24px" CausesValidation="False"
											Text="<"></asp:button></td>
									<td width="23"><asp:textbox id="txtGoToRec" tabIndex="8" runat="server" Width="23px" Height="19px" Enabled="False"></asp:textbox></td>
									<td style="WIDTH: 27px" width="24"><asp:button id="btnNextPage" runat="server" CssClass="queryButton" Width="24px" CausesValidation="False"
											Text=">"></asp:button></td>
									<td width="24"><asp:button id="btnGoToLastPage" runat="server" CssClass="queryButton" Width="24px" CausesValidation="False"
											Text=">|"></asp:button></td>
								</tr>
								<tr>
									<td colSpan="11" align="right"><asp:label id="lblNumRec" runat="server" CssClass="RecMsg" Width="274px" Height="19px"></asp:label></td>
								</tr>
							</TABLE>
							<asp:label id="lblErrorMsg" runat="server" CssClass="errorMsgColor" Width="566px" Height="19px"></asp:label><asp:regularexpressionvalidator id="Regularpickupdate" runat="server" CssClass="errorMsgColor" Width="467px" ControlToValidate="txtShipPckUpTime"
								Display="None" ValidationExpression="^((1[0-2]|(0[1-9])|[1][\d]|[2][\d]|[3][0]|[3][1])\/((0[1-9])|(1[0-2]))\/([\d][\d][\d][\d])[\s]((0[0-9])|[1][\d]|(2[0-4]))\:(0[0-9]|(1[\d]|2[\d]|3[\d]|4[\d]|5[\d]|6[0])))"></asp:regularexpressionvalidator><asp:regularexpressionvalidator id="RegularEstDlvryDt" runat="server" CssClass="errorMsgColor" Width="467px" ControlToValidate="txtShipEstDlvryDt"
								Display="None" ValidationExpression="^((1[0-2]|(0[1-9])|[1][\d]|[2][\d]|[3][0]|[3][1])\/((0[1-9])|(1[0-2]))\/([\d][\d][\d][\d])[\s]((0[0-9])|[1][\d]|(2[0-4]))\:(0[0-9]|(1[\d]|2[\d]|3[\d]|4[\d]|5[\d]|6[0])))"></asp:regularexpressionvalidator><asp:regularexpressionvalidator style="Z-INDEX: 117; POSITION: absolute; TOP: 80px; LEFT: 0px" id="RegularShpManfstDt"
								runat="server" CssClass="errorMsgColor" Width="467px" ControlToValidate="txtShipManifestDt" Display="None" ValidationExpression="^((1[0-2]|(0[1-9])|[1][\d]|[2][\d]|[3][0]|[3][1])\/((0[1-9])|(1[0-2]))\/([\d][\d][\d][\d])[\s]((0[0-9])|[1][\d]|(2[0-4]))\:(0[0-9]|(1[\d]|2[\d]|3[\d]|4[\d]|5[\d]|6[0])))"></asp:regularexpressionvalidator><asp:validationsummary id="PageValidationSummary" runat="server" Visible="True" Height="35px" ShowMessageBox="True"
								ShowSummary="False"></asp:validationsummary><asp:regularexpressionvalidator style="Z-INDEX: 117; POSITION: absolute; TOP: 80px; LEFT: 0px" id="Regularexpressionvalidator1"
								runat="server" CssClass="errorMsgColor" Width="467px" ControlToValidate="txtBookDate" Display="None" ValidationExpression="^((1[0-2]|(0[1-9])|[1][\d]|[2][\d]|[3][0]|[3][1])\/((0[1-9])|(1[0-2]))\/([\d][\d][\d][\d])[\s]((0[0-9])|[1][\d]|(2[0-4]))\:(0[0-9]|(1[\d]|2[\d]|3[\d]|4[\d]|5[\d]|6[0])))"></asp:regularexpressionvalidator></TD>
					</TR>
					<TR>
						<TD style="HEIGHT: 9px" vAlign="baseline" width="738">
							<TABLE style="WIDTH: 972px; HEIGHT: 8px" id="Table6" border="0" cellSpacing="1" cellPadding="1">
								<TR>
									<TD style="WIDTH: 1px; HEIGHT: 8px" height="8" width="1"><FONT face="Tahoma"></FONT></TD>
									<TD style="WIDTH: 255px; HEIGHT: 8px" vAlign="baseline" align="left"><FONT face="Tahoma"><asp:label id="lblBookingNo" runat="server" CssClass="tableLabel" Width="80px">Booking #</asp:label><cc1:mstextbox id="txtBookingNo" tabIndex="1" runat="server" CssClass="textField" Width="167px"
												AutoPostBack="True" MaxLength="10" TextMaskType="msNumeric" NumberMaxValue="2147483647" NumberPrecision="10"></cc1:mstextbox></FONT></TD>
									<TD style="WIDTH: 197px; HEIGHT: 8px" vAlign="baseline" align="left"><asp:label id="lblBookDate" runat="server" CssClass="tableLabel" Width="80px">Booking D/T</asp:label><cc1:mstextbox id="txtBookDate" tabIndex="7" runat="server" CssClass="textField" Width="114px"
											MaxLength="16" TextMaskType="msDateTime" TextMaskString="99/99/9999 99:99"></cc1:mstextbox></TD>
									<TD style="WIDTH: 280px; HEIGHT: 8px" vAlign="baseline" align="left"><FONT face="Tahoma"><asp:label id="lblOrigin" runat="server" CssClass="tableLabel" Width="86px">Ori. Province</asp:label><asp:textbox id="txtOrigin" tabIndex="3" runat="server" CssClass="textField" Width="180px" Enabled="False"
												MaxLength="10"></asp:textbox></FONT></TD>
									<TD style="HEIGHT: 8px" vAlign="baseline" align="left"><asp:label id="lblStatusCode" runat="server" CssClass="tableLabel" Width="94px">LAST Status</asp:label><asp:textbox id="txtLatestStatusCode" tabIndex="8" runat="server" CssClass="textField" Width="90px"
											Enabled="False"></asp:textbox></TD>
								</TR>
								<TR>
									<TD style="WIDTH: 1px; HEIGHT: 21px"><FONT face="Tahoma"><asp:requiredfieldvalidator id="validConsgNo" Width="3px" ControlToValidate="txtConsigNo" Runat="server" ErrorMessage="Consignment No">*</asp:requiredfieldvalidator></FONT></TD>
									<TD style="WIDTH: 255px; HEIGHT: 21px"><asp:label id="lblConsgmtNo" runat="server" CssClass="tableLabel" Width="80px">Con. #</asp:label><asp:textbox onblur="return ValidateConNo(this);" style="TEXT-TRANSFORM: uppercase" id="txtConsigNo"
											tabIndex="4" runat="server" CssClass="textField" Width="167px" MaxLength="21"></asp:textbox></TD>
									<TD style="WIDTH: 197px; HEIGHT: 21px"><asp:label id="lblShipManifestDt" runat="server" CssClass="tableLabel" Width="80px"> Manifest D/T</asp:label><cc1:mstextbox id="txtShipManifestDt" tabIndex="7" runat="server" CssClass="textField" Width="114px"
											AutoPostBack="True" MaxLength="16" TextMaskType="msDateTime" TextMaskString="99/99/9999 99:99"></cc1:mstextbox></TD>
									<TD style="WIDTH: 280px; HEIGHT: 21px"><asp:label id="lblDestination" runat="server" CssClass="tableLabel" Width="86px">Dest. Province</asp:label><asp:textbox id="txtDestination" tabIndex="6" runat="server" CssClass="textField" Width="180px"
											Enabled="False" MaxLength="10"></asp:textbox></TD>
									<TD style="HEIGHT: 21px"><asp:label id="lblExcepCode" runat="server" CssClass="tableLabel" Width="94px">LAST Exception</asp:label><asp:textbox id="txtLatestExcepCode" tabIndex="11" runat="server" CssClass="textField" Width="90px"
											Enabled="False"></asp:textbox></TD>
								</TR>
								<TR>
									<TD style="WIDTH: 1px; HEIGHT: 21px"></TD>
									<TD style="WIDTH: 255px; HEIGHT: 21px"><FONT face="Tahoma"><asp:label id="lblRefNo" runat="server" CssClass="tableLabel" Width="80px">Cust. Ref. #</asp:label><asp:textbox id="txtRefNo" tabIndex="5" runat="server" CssClass="textField" Width="167px" MaxLength="20"></asp:textbox></FONT></TD>
									<TD style="WIDTH: 197px; HEIGHT: 21px"><FONT face="Tahoma"><asp:label id="lblRouteCode" runat="server" CssClass="tableLabel" Width="80px">Route Code</asp:label><asp:textbox id="txtRouteCode" runat="server" ReadOnly="True" CssClass="textField" Width="114px"
												Enabled="False" MaxLength="12"></asp:textbox></FONT></TD>
									<TD style="WIDTH: 169px; HEIGHT: 21px"><FONT face="Tahoma"><asp:label id="Label3" runat="server" CssClass="tableLabel" Width="86px">Pickup Route</asp:label><asp:textbox id="txtPickupRoute" tabIndex="6" runat="server" CssClass="textField" Width="74px"
												Enabled="False" MaxLength="10"></asp:textbox></FONT></TD>
									<TD style="HEIGHT: 21px"><FONT face="Tahoma"><asp:button id="btnRouteCode" tabIndex="10" runat="server" Visible="False" CssClass="searchButton"
												Width="21px" CausesValidation="False" Text="..." Height="19px" Enabled="False"></asp:button><asp:label style="DISPLAY: none" id="lblDelManifest" runat="server" CssClass="tableLabel" Width="80px">Auto Manifest</asp:label><asp:textbox style="DISPLAY: none" id="txtDelManifest" tabIndex="12" runat="server" CssClass="textField"
												Width="40px" Enabled="False"></asp:textbox></FONT></TD>
								</TR>
							</TABLE>
						</TD>
					</TR>
					<tr>
						<td style="WIDTH: 738px; HEIGHT: 176px" width="738" align="left">
							<TABLE style="WIDTH: 972px; HEIGHT: 154px" id="Table8" border="0" cellSpacing="1" cellPadding="1">
								<TR>
									<TD style="WIDTH: 115px; HEIGHT: 3px"><FONT face="Tahoma"><asp:label id="lblCustInfo" CssClass="tableHeadingFieldset" Width="116px" Runat="server">CUSTOMER PROFILE</asp:label></FONT></TD>
									<TD style="WIDTH: 10px; HEIGHT: 3px"><asp:requiredfieldvalidator id="validCustID" Width="3px" ControlToValidate="txtCustID" Runat="server">*</asp:requiredfieldvalidator></TD>
									<TD style="WIDTH: 16px; HEIGHT: 3px"><FONT face="Tahoma"><asp:label id="lblCustID" runat="server" CssClass="tableLabel" Width="16px"> ID</asp:label></FONT></TD>
									<TD style="WIDTH: 218px; HEIGHT: 3px" colSpan="3"><FONT face="Tahoma"><cc1:mstextbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase" id="txtCustID" tabIndex="15" runat="server"
												CssClass="textField" Width="68px" AutoPostBack="True" MaxLength="20" TextMaskType="msUpperAlfaNumericWithUnderscore"></cc1:mstextbox><asp:button id="btnDisplayCustDtls" runat="server" CssClass="searchButton" Width="21px" CausesValidation="False"
												Text="..." Height="19px"></asp:button><asp:dropdownlist id="ddbCustType" tabIndex="13" runat="server" Visible="False" Width="72px"></asp:dropdownlist></FONT></TD>
									<TD style="WIDTH: 217px; HEIGHT: 3px" colSpan="2"><FONT face="Tahoma"><asp:textbox id="txtCustAdd1" tabIndex="18" runat="server" Visible="False" CssClass="textField"
												Width="40px" AutoPostBack="True" MaxLength="100"></asp:textbox><asp:textbox id="txtCustFax" tabIndex="24" runat="server" Visible="False" CssClass="textField"
												Width="48px" AutoPostBack="True" MaxLength="20"></asp:textbox><asp:label id="lblCustType" runat="server" Visible="False" CssClass="tableLabel" Width="64px">Cust Type</asp:label><asp:requiredfieldvalidator id="validCustType" Visible="False" Width="3px" ControlToValidate="ddbCustType" Runat="server">*</asp:requiredfieldvalidator></FONT></TD>
									<TD style="WIDTH: 13px; HEIGHT: 3px"></TD>
									<TD style="WIDTH: 31px; HEIGHT: 3px"><asp:requiredfieldvalidator id="validCustAdd1" Visible="False" Width="3px" ControlToValidate="txtCustAdd1" Runat="server"
											ErrorMessage="Customer Address">*</asp:requiredfieldvalidator></TD>
									<TD style="WIDTH: 215px; HEIGHT: 3px" colSpan="2"><FONT face="Tahoma"><asp:textbox id="txtCustAdd2" tabIndex="19" runat="server" Visible="False" CssClass="textField"
												Width="65px" AutoPostBack="True"></asp:textbox><asp:label id="lblTelphone" tabIndex="100" runat="server" Visible="False" CssClass="tableLabel"
												Width="58px">Telephone</asp:label></FONT></TD>
									<TD style="WIDTH: 212px; HEIGHT: 3px"><FONT face="Tahoma"><asp:checkbox id="chkNewCust" tabIndex="14" runat="server" Visible="False" CssClass="tableLabel"
												Width="104px" Text="New Customer" Height="16px" AutoPostBack="True"></asp:checkbox></FONT></TD>
									<TD style="WIDTH: 129px; HEIGHT: 3px"></TD>
								</TR>
								<TR>
									<TD style="WIDTH: 115px; HEIGHT: 17px"><FONT face="Tahoma">&nbsp;<asp:label id="lblAddress" runat="server" Visible="False" CssClass="tableLabel" Width="48px">Address</asp:label></FONT></TD>
									<TD style="WIDTH: 10px; HEIGHT: 17px"><asp:requiredfieldvalidator id="validCustName" Width="3px" ControlToValidate="txtCustName" Runat="server">*</asp:requiredfieldvalidator></TD>
									<TD style="WIDTH: 16px; HEIGHT: 17px"><FONT face="Tahoma"><asp:label id="lblName" runat="server" CssClass="tableLabel" Width="32px">Name</asp:label></FONT></TD>
									<TD style="WIDTH: 218px; HEIGHT: 17px" colSpan="3"><FONT face="Tahoma"><asp:textbox id="txtCustName" runat="server" CssClass="textField" Width="214px" MaxLength="100"></asp:textbox></FONT></TD>
									<TD style="WIDTH: 217px; HEIGHT: 17px" colSpan="2"><FONT face="Tahoma"><asp:label id="lblPaymode" runat="server" CssClass="tableLabel" Width="64px"> Payment</asp:label><asp:radiobutton id="rbCash" runat="server" Width="16px" Text="Cash" Enabled="False" Font-Size="Smaller"
												GroupName="PaymentType"></asp:radiobutton><asp:radiobutton id="rbCredit" runat="server" Width="16px" Text="Credit" Enabled="False" Font-Size="Smaller"
												GroupName="PaymentType"></asp:radiobutton></FONT></TD>
									<TD style="WIDTH: 13px; HEIGHT: 17px"><asp:requiredfieldvalidator id="validCustZip" Visible="False" Width="3px" ControlToValidate="txtCustZipCode"
											Runat="server" ErrorMessage="Customer Postal Code">*</asp:requiredfieldvalidator></TD>
									<TD style="WIDTH: 31px; HEIGHT: 17px"><asp:label id="lblZip" runat="server" Visible="False" CssClass="tableLabel" Width="48px"> Zipcode</asp:label></TD>
									<TD style="WIDTH: 215px; HEIGHT: 17px" colSpan="2" align="left"><asp:textbox id="txtCustZipCode" tabIndex="20" runat="server" Visible="False" CssClass="textField"
											Width="45px" AutoPostBack="True" MaxLength="10"></asp:textbox><asp:textbox id="txtCustCity" runat="server" Visible="False" ReadOnly="True" CssClass="textField"
											Width="24px"></asp:textbox><asp:textbox id="txtCustTelephone" tabIndex="23" runat="server" Visible="False" CssClass="textField"
											Width="32px" AutoPostBack="True" MaxLength="20"></asp:textbox><asp:label id="lblFax" tabIndex="100" runat="server" Visible="False" CssClass="tableLabel"
											Width="7px">Fax</asp:label></TD>
									<TD style="WIDTH: 212px; HEIGHT: 17px"><asp:textbox id="txtCustStateCode" runat="server" Visible="False" ReadOnly="True" CssClass="textField"
											Width="79px"></asp:textbox></TD>
									<TD style="WIDTH: 129px; HEIGHT: 17px"></TD>
								</TR>
								<TR>
									<TD style="WIDTH: 115px" height="1" vAlign="top" colSpan="14" noWrap>
										<HR style="WIDTH: 842.82%; HEIGHT: 1px" SIZE="1" width="842.82%">
									</TD>
								</TR>
								<TR>
									<TD style="WIDTH: 115px; HEIGHT: 12px"><asp:label id="lblSendInfo" CssClass="tableHeadingFieldset" Width="40px" Runat="server">FROM:</asp:label><asp:checkbox id="chkSendCustInfo" runat="server" CssClass="tableLabel" Width="57px" Text="Same"
											AutoPostBack="True"></asp:checkbox></TD>
									<TD style="WIDTH: 10px; HEIGHT: 12px"><asp:requiredfieldvalidator id="validSendName" Width="3px" ControlToValidate="txtSendName" Runat="server">*</asp:requiredfieldvalidator></TD>
									<TD style="WIDTH: 16px; HEIGHT: 12px"><FONT face="Tahoma"><asp:label id="lblSendNm" runat="server" CssClass="tableLabel" Width="34px">Name</asp:label></FONT></TD>
									<TD style="WIDTH: 218px; HEIGHT: 12px" colSpan="3"><FONT face="Tahoma"><asp:textbox id="txtSendName" runat="server" CssClass="textField" Width="192px" AutoPostBack="True"
												MaxLength="100"></asp:textbox><asp:button id="btnSendCust" runat="server" CssClass="searchButton" CausesValidation="False"
												Text="..."></asp:button></FONT></TD>
									<TD style="WIDTH: 217px; HEIGHT: 12px" colSpan="2"><FONT face="Tahoma"></FONT><FONT face="Tahoma"><asp:textbox id="txtSendAddr1" runat="server" CssClass="textField" Width="200px" MaxLength="100"></asp:textbox></FONT></TD>
									<TD style="WIDTH: 13px; HEIGHT: 12px"></TD>
									<TD style="WIDTH: 31px; HEIGHT: 12px"></TD>
									<TD style="WIDTH: 215px; HEIGHT: 12px" colSpan="2"><FONT face="Tahoma"></FONT></TD>
									<TD style="WIDTH: 212px; HEIGHT: 12px"><asp:textbox id="txtSendState" runat="server" Visible="False" ReadOnly="True" CssClass="textField"
											Width="79px"></asp:textbox><FONT face="Tahoma"></FONT></TD>
									<TD style="WIDTH: 129px; HEIGHT: 12px"></TD>
								</TR>
								<TR>
									<TD style="WIDTH: 115px; HEIGHT: 24px"><FONT face="Tahoma"></FONT></TD>
									<TD style="WIDTH: 10px; HEIGHT: 24px"><asp:requiredfieldvalidator id="validSenderAdd1" Visible="False" Width="3px" ControlToValidate="txtSendAddr1"
											Runat="server">*</asp:requiredfieldvalidator></TD>
									<TD style="WIDTH: 16px; HEIGHT: 24px"><asp:label id="lblSendAddr1" runat="server" Visible="False" CssClass="tableLabel" Width="42px">Address</asp:label></TD>
									<TD style="WIDTH: 218px; HEIGHT: 24px" colSpan="3"><FONT face="Tahoma"></FONT></TD>
									<TD style="WIDTH: 217px; HEIGHT: 24px" colSpan="2"><FONT face="Tahoma"><asp:textbox id="txtSendAddr2" runat="server" CssClass="textField" Width="200px" MaxLength="100"></asp:textbox></FONT></TD>
									<TD style="WIDTH: 13px; HEIGHT: 24px"><asp:requiredfieldvalidator id="validSendZip" Width="3px" ControlToValidate="txtSendZip" Runat="server">*</asp:requiredfieldvalidator></TD>
									<TD style="WIDTH: 31px; HEIGHT: 24px"><asp:label id="lblSendZip" runat="server" CssClass="tableLabel" Width="48px"> Zipcode</asp:label></TD>
									<TD style="HEIGHT: 24px" width="100%" colSpan="3" align="left"><asp:textbox style="TEXT-TRANSFORM: uppercase" id="txtSendZip" runat="server" CssClass="textField"
											Width="65px" AutoPostBack="True" MaxLength="10"></asp:textbox><asp:textbox id="txtSendCity" runat="server" ReadOnly="True" CssClass="textField" Width="200px"></asp:textbox></TD>
									<TD style="WIDTH: 129px; HEIGHT: 24px"></TD>
								</TR>
								<TR>
									<TD style="WIDTH: 115px; HEIGHT: 17px" height="17"><FONT face="Tahoma"></FONT></TD>
									<TD style="WIDTH: 10px; HEIGHT: 17px" height="17"></TD>
									<TD style="WIDTH: 16px; HEIGHT: 17px" height="17"><asp:label id="lblSendConPer" runat="server" CssClass="tableLabel" Width="40px">Contact</asp:label></TD>
									<TD style="WIDTH: 143px; HEIGHT: 17px" height="17" colSpan="2"><asp:textbox id="txtSendContPer" runat="server" CssClass="textField" Width="150px" MaxLength="100"></asp:textbox></TD>
									<TD style="WIDTH: 63px; HEIGHT: 17px" height="17"><asp:label id="lblSendTel" tabIndex="100" runat="server" CssClass="tableLabel" Width="52px">Telephone</asp:label></TD>
									<TD style="WIDTH: 217px; HEIGHT: 17px" height="17" colSpan="2"><FONT face="Tahoma"><asp:textbox id="txtSendTel" runat="server" CssClass="textField" Width="200px" MaxLength="20"></asp:textbox></FONT></TD>
									<TD style="WIDTH: 13px; HEIGHT: 17px" height="17"></TD>
									<TD style="WIDTH: 31px; HEIGHT: 17px" height="17"><asp:label id="lblSendFax" tabIndex="100" runat="server" CssClass="tableLabel" Width="20px">Fax</asp:label></TD>
									<TD style="HEIGHT: 17px" width="172" colSpan="2"><FONT face="Tahoma"><asp:textbox id="txtSendFax" runat="server" CssClass="textField" Width="173px" MaxLength="20"></asp:textbox></FONT></TD>
									<TD style="WIDTH: 212px; HEIGHT: 17px" height="17"><cc1:mstextbox id="txtSendCuttOffTime" runat="server" Visible="False" ReadOnly="True" CssClass="textField"
											Width="62px" MaxLength="5" TextMaskType="msTime" TextMaskString="99:99"></cc1:mstextbox></TD>
									<TD style="WIDTH: 129px; HEIGHT: 17px" height="17"></TD>
								</TR>
								<TR>
									<TD style="WIDTH: 115px" height="1" vAlign="top" colSpan="14" noWrap>
										<HR style="WIDTH: 842.82%; HEIGHT: 1px" SIZE="1" width="842.82%">
									</TD>
								</TR>
								<TR>
									<TD style="WIDTH: 115px; HEIGHT: 22px"><asp:label id="lblRecInfo" CssClass="tableHeadingFieldset" Width="40px" Runat="server">TO:</asp:label><asp:checkbox id="chkRecip" tabIndex="34" runat="server" CssClass="tableLabel" Width="57px" Text="Same"
											AutoPostBack="True"></asp:checkbox></TD>
									<TD style="WIDTH: 10px; HEIGHT: 22px"><FONT face="Tahoma"></FONT></TD>
									<TD style="WIDTH: 16px; HEIGHT: 22px"><asp:label id="lblRecipTelephone" tabIndex="83" runat="server" CssClass="tableLabel" Width="48px">Tel. 1</asp:label></TD>
									<TD style="WIDTH: 141px; HEIGHT: 22px"><asp:textbox id="txtRecipTel" tabIndex="35" runat="server" CssClass="textField" Width="114px"
											AutoPostBack="True" MaxLength="20"></asp:textbox><asp:button id="btnTelephonePopup" tabIndex="36" runat="server" CssClass="searchButton" CausesValidation="False"
											Text="..."></asp:button></TD>
									<TD style="WIDTH: 2px; HEIGHT: 22px"><asp:requiredfieldvalidator id="validRecipName" Width="1px" ControlToValidate="txtRecName" Runat="server">*</asp:requiredfieldvalidator></TD>
									<TD style="WIDTH: 63px; HEIGHT: 22px"><asp:label id="lblRecipNm" tabIndex="100" runat="server" CssClass="tableLabel" Width="26px">Name</asp:label></TD>
									<TD style="WIDTH: 217px; HEIGHT: 22px" colSpan="2"><FONT face="Tahoma"><asp:textbox id="txtRecName" tabIndex="37" runat="server" CssClass="textField" Width="178px"
												AutoPostBack="True" MaxLength="100"></asp:textbox><asp:button id="btnRecipNm" tabIndex="38" runat="server" CssClass="searchButton" CausesValidation="False"
												Text="..."></asp:button></FONT></TD>
									<TD style="WIDTH: 13px; HEIGHT: 22px"></TD>
									<TD style="WIDTH: 31px; HEIGHT: 22px"><FONT face="Tahoma"></FONT></TD>
									<TD style="WIDTH: 215px; HEIGHT: 22px" colSpan="2"><FONT face="Tahoma"></FONT></TD>
									<TD style="WIDTH: 212px; HEIGHT: 22px"><asp:label id="lblSendCuttOffTime" runat="server" Visible="False" CssClass="tableLabel" Width="43px">Cut-off</asp:label></TD>
									<TD style="WIDTH: 129px; HEIGHT: 22px"></TD>
								</TR>
								<TR>
									<TD style="WIDTH: 115px; HEIGHT: 22px"></TD>
									<TD style="WIDTH: 10px; HEIGHT: 22px"></TD>
									<TD style="WIDTH: 16px; HEIGHT: 22px"></TD>
									<TD style="WIDTH: 141px; HEIGHT: 22px"></TD>
									<TD style="WIDTH: 2px; HEIGHT: 22px"><asp:requiredfieldvalidator id="validRecipAdd1" Width="3px" ControlToValidate="txtRecipAddr1" Runat="server">*</asp:requiredfieldvalidator></TD>
									<TD style="WIDTH: 63px; HEIGHT: 22px"><asp:label id="lblRecipAddr1" tabIndex="100" runat="server" CssClass="tableLabel" Width="32px">Address</asp:label></TD>
									<TD style="WIDTH: 217px; HEIGHT: 22px" colSpan="2"><asp:textbox id="txtRecipAddr1" tabIndex="39" runat="server" CssClass="textField" Width="200px"
											MaxLength="100"></asp:textbox></TD>
									<TD style="WIDTH: 13px; HEIGHT: 22px"></TD>
									<TD style="WIDTH: 268px; HEIGHT: 22px" colSpan="3"><FONT face="Tahoma"><asp:textbox id="txtRecipAddr2" tabIndex="40" runat="server" CssClass="textField" Width="225px"
												MaxLength="100"></asp:textbox></FONT></TD>
									<TD style="WIDTH: 212px; HEIGHT: 22px"><asp:textbox style="DISPLAY: none" id="txtRecipState" runat="server" ReadOnly="True" CssClass="textField"
											Width="56px"></asp:textbox><asp:button style="Z-INDEX: 0" id="btnBind" tabIndex="58" Visible="False" CssClass="queryButton"
											CausesValidation="False" Text="Refresh" Runat="server"></asp:button></TD>
									<TD style="WIDTH: 129px; HEIGHT: 22px"></TD>
								</TR>
								<TR>
									<TD style="WIDTH: 115px">
										<P><FONT face="Tahoma"><asp:button id="btnPopulateVAS" tabIndex="57" Visible="False" CssClass="queryButton" Width="112px"
													CausesValidation="False" Text="GetQuotationVAS" Enabled="False" Runat="server"></asp:button></FONT></P>
									</TD>
									<TD style="WIDTH: 10px"><FONT face="Tahoma"></FONT></TD>
									<TD style="WIDTH: 16px"><asp:label id="lblRecpContPer" tabIndex="100" runat="server" CssClass="tableLabel" Width="40px">Contact</asp:label></TD>
									<TD style="WIDTH: 143px" colSpan="2"><asp:textbox style="Z-INDEX: 0" id="txtRecpContPer" tabIndex="41" runat="server" CssClass="textField"
											Width="150px" MaxLength="100"></asp:textbox></TD>
									<TD style="WIDTH: 63px"><asp:label id="lblRecipFax" tabIndex="85" runat="server" CssClass="tableLabel" Width="40px">Tel. 2</asp:label></TD>
									<TD style="WIDTH: 217px" colSpan="2"><FONT face="Tahoma"><asp:textbox id="txtRecipFax" tabIndex="42" runat="server" CssClass="textField" Width="200px"
												MaxLength="20"></asp:textbox></FONT></TD>
									<TD style="WIDTH: 13px"><asp:requiredfieldvalidator id="validRecipZip" Width="3px" ControlToValidate="txtRecipZip" Runat="server">*</asp:requiredfieldvalidator></TD>
									<TD style="WIDTH: 31px"><FONT face="Tahoma"><asp:label id="lblRecipZip" tabIndex="100" runat="server" CssClass="tableLabel" Width="48px"> Zipcode</asp:label></FONT></TD>
									<TD width="100%" colSpan="3" align="left"><asp:textbox style="TEXT-TRANSFORM: uppercase" id="txtRecipZip" tabIndex="43" runat="server"
											CssClass="textField" Width="65px" AutoPostBack="True" MaxLength="10"></asp:textbox><asp:textbox id="txtRecipCity" runat="server" ReadOnly="True" CssClass="textField" Width="200px"></asp:textbox></TD>
									<TD style="WIDTH: 129px"></TD>
								</TR>
							</TABLE>
						</td>
					</tr>
					<TR>
						<TD style="WIDTH: 738px; HEIGHT: 18px" width="738" align="left">
							<TABLE style="WIDTH: 930px; HEIGHT: 36px" id="Table5" border="0" cellSpacing="1" cellPadding="1">
								<TR>
									<TD style="WIDTH: 96px"><asp:button id="btnPkgDetails" tabIndex="46" runat="server" CssClass="queryButton" Width="112px"
											CausesValidation="False" Text="Package Detail"></asp:button></TD>
									<TD style="WIDTH: 100px"><asp:label id="lblPkgActualWt" runat="server" CssClass="tableLabel" Width="80px">Total Act. Wt.</asp:label></TD>
									<TD style="WIDTH: 73px"><FONT face="Tahoma"><asp:textbox id="txtActualWeight" runat="server" CssClass="textFieldRightAlign" Width="80px"></asp:textbox></FONT></TD>
									<TD style="WIDTH: 92px"><asp:label id="lblPkgTotPkgs" runat="server" CssClass="tableLabel" Width="88px">Total Pkgs.</asp:label></TD>
									<TD style="WIDTH: 69px"><asp:textbox id="txtPkgTotpkgs" runat="server" ReadOnly="False" CssClass="textFieldRightAlign"
											Width="80px"></asp:textbox></TD>
									<TD style="WIDTH: 69px"><asp:label id="lblPkgChargeWt" runat="server" CssClass="tableLabel" Width="88px">Total Chg.Wt.</asp:label></TD>
									<TD><FONT face="Tahoma"><asp:textbox id="txtPkgChargeWt" runat="server" ReadOnly="False" CssClass="textFieldRightAlign"
												Width="80px"></asp:textbox></FONT></TD>
									<td><FONT face="Tahoma"><asp:label id="lblPkgCommCode" runat="server" Visible="true" CssClass="tableLabel" Width="100px">Commodity Code</asp:label></FONT></td>
									<td><FONT face="Tahoma"></FONT><asp:textbox id="txtPkgCommCode" tabIndex="44" runat="server" Visible="true" CssClass="textField"
											Width="100px" AutoPostBack="True" MaxLength="12"></asp:textbox><asp:button id="btnPkgCommCode" tabIndex="45" runat="server" CssClass="searchButton" CausesValidation="False"
											Text="..."></asp:button></td>
								</TR>
								<TR>
									<TD style="WIDTH: 96px"><asp:button id="btnViewOldPD" tabIndex="46" runat="server" Visible="False" CssClass="queryButton"
											Width="112px" CausesValidation="False" Text="View Old PD"></asp:button><asp:textbox id="txtPkgCommDesc" runat="server" Visible="False" ReadOnly="False" CssClass="textField"
											Width="112px" Enabled="False"></asp:textbox></TD>
									<TD style="WIDTH: 100px"><asp:label id="Label6" runat="server" CssClass="tableLabel" Width="96px">Total Act. R Wt.</asp:label></TD>
									<TD style="WIDTH: 73px"><FONT face="Tahoma"><asp:textbox id="txtPkgActualWt" runat="server" ReadOnly="False" CssClass="textFieldRightAlign"
												Width="80px"></asp:textbox></FONT></TD>
									<TD style="WIDTH: 92px"><asp:label id="lblPkgDimWt" runat="server" CssClass="tableLabel" Width="86px">Total Dim. Wt.</asp:label></TD>
									<TD style="WIDTH: 69px"><asp:textbox id="txtPkgDimWt" runat="server" ReadOnly="False" CssClass="textFieldRightAlign"
											Width="80px"></asp:textbox></TD>
									<TD style="WIDTH: 69px"><FONT face="Tahoma"><asp:label id="lblTotalVol" runat="server" CssClass="tableLabel" Width="88px">Total Vol.</asp:label></FONT></TD>
									<TD><asp:textbox id="txttotVol" runat="server" ReadOnly="False" CssClass="textFieldRightAlign" Width="80px"></asp:textbox></TD>
									<td colSpan="2"><FONT face="Tahoma"><asp:label id="lblPD_Replace_Date" runat="server" Visible="False" CssClass="tableLabel"></asp:label></FONT></td>
								</TR>
							</TABLE>
						</TD>
					</TR>
					<tr>
						<td style="WIDTH: 738px; HEIGHT: 52px" width="738" align="left"><FONT face="Tahoma">
								<TABLE style="WIDTH: 938px; HEIGHT: 16px" id="tblShipService" border="0" width="938" runat="server">
									<TR height="25">
										<TD style="WIDTH: 16px; HEIGHT: 23px" width="16"><asp:requiredfieldvalidator id="validSrvcCode" ControlToValidate="txtShpSvcCode" Runat="server">*</asp:requiredfieldvalidator></TD>
										<TD style="WIDTH: 156px; HEIGHT: 23px" width="156"><asp:label id="lblShipSerCode" runat="server" CssClass="tableLabel" Width="100px">Service Type</asp:label></TD>
										<TD style="WIDTH: 46px; HEIGHT: 23px" width="46"><cc1:mstextbox style="TEXT-TRANSFORM: uppercase" id="txtShpSvcCode" tabIndex="47" runat="server"
												CssClass="textField" Width="74px" AutoPostBack="True" MaxLength="12" TextMaskType="msUpperAlfaNumericWithUnderscore"></cc1:mstextbox></TD>
										<TD style="WIDTH: 3px; HEIGHT: 23px" width="3"><asp:button id="btnShpSvcCode" tabIndex="48" runat="server" CssClass="searchButton" CausesValidation="False"
												Text="..."></asp:button></TD>
										<TD style="WIDTH: 36px; HEIGHT: 23px" width="36" colSpan="2"><asp:textbox id="txtShpSvcDesc" runat="server" Visible="False" ReadOnly="True" CssClass="textField"
												Width="202px"></asp:textbox></TD>
										<TD style="WIDTH: 126px; HEIGHT: 23px" width="126"><asp:label id="lblShipPickUpTime" runat="server" CssClass="tableLabel" Width="108px">Actual Pickup D/T</asp:label></TD>
										<TD style="HEIGHT: 21px" width="20%"><asp:textbox id="txtShipPckUpTime" runat="server" CssClass="textField" Width="114px" AutoPostBack="True"></asp:textbox></TD>
										<TD style="WIDTH: 101px; HEIGHT: 21px" width="101"></TD>
										<TD style="HEIGHT: 21px" width="20%"></TD>
									</TR>
									<TR height="25">
										<TD style="WIDTH: 16px; HEIGHT: 15px" width="16"></TD>
										<TD style="WIDTH: 156px; HEIGHT: 15px" width="156"><asp:label id="lblShipDclValue" runat="server" CssClass="tableLabel" Width="120px">Declared Value THB</asp:label></TD>
										<TD style="WIDTH: 46px; HEIGHT: 15px" width="46"><asp:textbox id="txtShpDclrValue" tabIndex="49" runat="server" CssClass="textFieldRightAlign"
												Width="74px" AutoPostBack="True"></asp:textbox></TD>
										<TD style="WIDTH: 3px; HEIGHT: 15px" width="3"></TD>
										<TD style="WIDTH: 77px; HEIGHT: 15px" width="77"><FONT face="Tahoma"><asp:label id="lblShipMaxCovg" runat="server" CssClass="tableLabel" Width="120px">Maximum Coverage</asp:label></FONT></TD>
										<TD style="WIDTH: 19px; HEIGHT: 15px" width="19"><asp:textbox id="txtShpMaxCvrg" runat="server" ReadOnly="True" CssClass="textFieldRightAlign"
												Width="74px"></asp:textbox></TD>
										<TD style="WIDTH: 126px; HEIGHT: 15px" width="126"><asp:label id="lblShipEstDlvryDt" tabIndex="108" runat="server" CssClass="tableLabel" Width="107px">Est. Delivery D/T</asp:label></TD>
										<TD style="HEIGHT: 15px" width="20%"><asp:textbox id="txtShipEstDlvryDt" runat="server" CssClass="textField" Width="114px" AutoPostBack="True"></asp:textbox></TD>
										<TD style="WIDTH: 101px; HEIGHT: 15px" width="101"><asp:label id="lblEstHCDt" tabIndex="108" runat="server" CssClass="tableLabel" Width="107px">Est. HCR D/T</asp:label></TD>
										<TD style="HEIGHT: 15px" width="20%"><asp:textbox id="txtEstHCDt" runat="server" ReadOnly="True" CssClass="textField" Width="114px"
												AutoPostBack="True"></asp:textbox></TD>
									</TR>
									<TR height="25">
										<TD style="WIDTH: 16px; HEIGHT: 23px" width="16"></TD>
										<TD style="WIDTH: 156px; HEIGHT: 23px" width="156"><asp:label id="lblShipAdpercDV" runat="server" CssClass="tableLabel" Width="125px">Insurance Sur. %</asp:label></TD>
										<TD style="WIDTH: 46px; HEIGHT: 23px" width="46"><asp:textbox id="txtShpAddDV" runat="server" ReadOnly="True" CssClass="textFieldRightAlign" Width="74px"></asp:textbox></TD>
										<TD style="WIDTH: 3px; HEIGHT: 23px" width="3"></TD>
										<TD style="WIDTH: 77px; HEIGHT: 23px" width="77"><asp:label id="lblShipInsSurchrg" runat="server" CssClass="tableLabel" Width="120px"> Insurance Surcharge</asp:label></TD>
										<TD style="WIDTH: 19px; HEIGHT: 23px" width="19"><asp:textbox id="txtShpInsSurchrg" runat="server" ReadOnly="True" CssClass="textFieldRightAlign"
												Width="74px"></asp:textbox></TD>
										<TD style="WIDTH: 126px; HEIGHT: 23px" width="126"><asp:label id="Label1" tabIndex="108" runat="server" CssClass="tableLabel" Width="112px">Actual Delivery D/T</asp:label></TD>
										<TD style="HEIGHT: 23px" width="20%"><asp:textbox id="txtActDelDt" runat="server" ReadOnly="True" CssClass="textField" Width="114px"
												AutoPostBack="True"></asp:textbox></TD>
										<TD style="WIDTH: 101px; HEIGHT: 23px" width="101"><asp:label id="lblActHCDt" tabIndex="108" runat="server" CssClass="tableLabel" Width="96px">Actual HCR D/T</asp:label></TD>
										<TD style="HEIGHT: 23px" width="20%"><asp:textbox id="txtActHCDt" runat="server" ReadOnly="True" CssClass="textField" Width="114px"
												AutoPostBack="True"></asp:textbox></TD>
									</TR>
									<TR height="25">
										<TD style="WIDTH: 16px; HEIGHT: 12px" width="16"></TD>
										<TD style="WIDTH: 156px; HEIGHT: 12px" width="156"><asp:label id="Label5" runat="server" Visible="False" CssClass="tableLabel" Width="120px"> Other Surcharge</asp:label></TD>
										<TD style="WIDTH: 46px; HEIGHT: 12px" width="46"><asp:textbox id="txtOtherSurcharge" runat="server" Visible="False" CssClass="textFieldRightAlign"
												Width="74px"></asp:textbox></TD>
										<TD style="WIDTH: 3px; HEIGHT: 12px" width="3"></TD>
										<TD style="WIDTH: 77px; HEIGHT: 12px" width="77"><asp:label id="lblCODAmount" runat="server" CssClass="tableLabel" Width="88px">C.O.D. Amount</asp:label></TD>
										<TD style="WIDTH: 19px; HEIGHT: 12px" width="19"><cc1:mstextbox id="txtCODAmount" tabIndex="52" runat="server" CssClass="textFieldRightAlign" Width="74px"
												AutoPostBack="True" MaxLength="11" TextMaskType="msNumericCOD" NumberPrecision="10" NumberMaxValueCOD="99999999.99" NumberMinValue="0" NumberScale="2"></cc1:mstextbox></TD>
										<TD style="WIDTH: 126px; HEIGHT: 12px" width="126" align="right"></TD>
										<TD style="HEIGHT: 12px" width="20%" align="right"><FONT face="Tahoma"><asp:checkbox id="chkshpRtnHrdCpy" runat="server" CssClass="tableLabel" Width="14px" Text="HCR"
													AutoPostBack="True"></asp:checkbox><asp:checkbox id="chkInvHCReturn" runat="server" CssClass="tableLabel" Width="38px" Text="INVR"
													AutoPostBack="True"></asp:checkbox></FONT></TD>
										<TD style="WIDTH: 101px; HEIGHT: 12px" width="101"><asp:radiobutton id="rbtnShipFrghtPre" runat="server" CssClass="tableLabel" Width="112px" Text="Freight Prepaid"
												Height="16px" GroupName="Freight"></asp:radiobutton></TD>
										<TD style="HEIGHT: 12px" width="20%"><asp:radiobutton id="rbtnShipFrghtColl" tabIndex="55" runat="server" CssClass="tableLabel" Width="108px"
												Text="Freight Collect" Height="16px" GroupName="Freight"></asp:radiobutton></TD>
									</TR>
									<TR>
										<TD style="WIDTH: 16px; HEIGHT: 12px" width="16"></TD>
										<TD style="WIDTH: 156px; HEIGHT: 12px" width="156"></TD>
										<TD style="WIDTH: 46px; HEIGHT: 12px" width="46"></TD>
										<TD style="WIDTH: 3px; HEIGHT: 12px" width="3"></TD>
										<TD style="WIDTH: 77px; HEIGHT: 12px" width="77"></TD>
										<TD style="WIDTH: 19px; HEIGHT: 12px" width="19"></TD>
										<TD style="WIDTH: 126px; HEIGHT: 12px" width="126" align="right"></TD>
										<TD style="HEIGHT: 12px" width="20%" align="right"></TD>
										<TD style="WIDTH: 101px; HEIGHT: 12px" width="101"></TD>
										<TD style="HEIGHT: 12px" width="20%"></TD>
									</TR>
								</TABLE>
							</FONT>
						</td>
					</tr>
					<tr>
						<td>
							<table>
								<TR>
									<TD style="WIDTH: 16px; HEIGHT: 12px" width="16"></TD>
									<TD style="WIDTH: 114px; HEIGHT: 12px" width="114"><FONT face="Tahoma"><asp:checkbox id="cbIntDoc" runat="server" CssClass="tableLabel" Width="120px" Text="Int'l Documents"
												Height="25px" AutoPostBack="True"></asp:checkbox></FONT></TD>
									<TD style="WIDTH: 74px; HEIGHT: 12px" width="74"><FONT face="Tahoma"><asp:checkbox id="cbExport" runat="server" CssClass="tableLabel" Width="96px" Text="Export" Height="25px"
												AutoPostBack="True"></asp:checkbox></FONT></TD>
									<TD style="WIDTH: 81px; HEIGHT: 12px" width="81"><asp:label style="Z-INDEX: 0" id="Label4" runat="server" CssClass="tableLabel" Width="60px">MAWB No.</asp:label></TD>
									<TD style="WIDTH: 77px; HEIGHT: 12px" width="77"><asp:textbox style="Z-INDEX: 0" id="txtMAWBNo" runat="server" ReadOnly="True" CssClass="textField"
											Width="114px" AutoPostBack="True"></asp:textbox></TD>
									<TD style="WIDTH: 19px; HEIGHT: 12px" width="19"></TD>
									<TD style="WIDTH: 126px; HEIGHT: 12px" width="126"><asp:label style="Z-INDEX: 0" id="Label7" runat="server" CssClass="tableLabel" Width="232px">Out-forwarding Freight Charge Paid by</asp:label></TD>
									<TD style="WIDTH: 101px; HEIGHT: 12px" width="101"><asp:textbox style="Z-INDEX: 0" id="txtPayerid1" runat="server" ReadOnly="True" CssClass="textField"
											Width="114px" AutoPostBack="True"></asp:textbox></TD>
									<TD style="HEIGHT: 12px" width="20%"><asp:textbox style="Z-INDEX: 0" id="txtPayerid2" runat="server" ReadOnly="True" CssClass="textField"
											Width="192px" AutoPostBack="True"></asp:textbox></TD>
								</TR>
								<TR>
									<TD style="WIDTH: 16px; HEIGHT: 12px" width="16"></TD>
									<TD style="WIDTH: 114px; HEIGHT: 12px" width="114"></TD>
									<TD style="WIDTH: 74px; HEIGHT: 12px" width="74" colSpan="3"></TD>
									<TD style="WIDTH: 81px; HEIGHT: 12px" width="81"></TD>
									<TD style="WIDTH: 77px; HEIGHT: 12px" width="77"></TD>
									<TD style="WIDTH: 19px; HEIGHT: 12px" width="19"></TD>
									<TD style="WIDTH: 126px; HEIGHT: 12px" width="126"></TD>
									<TD style="WIDTH: 101px; HEIGHT: 12px" width="101"></TD>
									<TD style="HEIGHT: 12px" width="20%"></TD>
								</TR>
							</table>
						</td>
					</tr>
					<%--  HC Return Task --%>
					<%--  HC Return Task --%>
					<%--  HC Return Task --%>
					<TR>
						<TD style="WIDTH: 738px; HEIGHT: 32px" vAlign="baseline" width="738" align="left">
							<TABLE style="WIDTH: 947px; HEIGHT: 4px" id="Table7" border="0" cellSpacing="1" cellPadding="1"
								width="0">
								<TR>
									<TD style="WIDTH: 62px">
										<P><asp:label id="lblSpeHanInstruction" runat="server" CssClass="tableLabel" Width="72px">Special Del. Instructions</asp:label></P>
									</TD>
									<TD><FONT face="Tahoma">
											<P><asp:textbox id="txtSpeHanInstruction" tabIndex="56" runat="server" Width="864px" Height="36px"
													AutoPostBack="True" MaxLength="200" TextMode="MultiLine"></asp:textbox></P>
										</FONT>
									</TD>
								</TR>
							</TABLE>
						</TD>
					</TR>
					<%--  HC Return Task --%>
					<TR vAlign="top">
						<td style="WIDTH: 738px; HEIGHT: 54px" width="738" align="left"><asp:datagrid id="dgVAS" tabIndex="115" runat="server" Width="722px" OnItemDataBound="dgVAS_ItemDataBound"
								OnItemCommand="dgVAS_Button" AutoGenerateColumns="False" OnEditCommand="dgVAS_Edit" AllowPaging="True" OnPageIndexChanged="dgVAS_PageChange" OnCancelCommand="dgVAS_Cancel"
								OnDeleteCommand="dgVAS_Delete" OnUpdateCommand="dgVAS_Update" PageSize="4" ItemStyle-Height="20px" HeaderStyle-Height="20px">
								<Columns>
									<asp:ButtonColumn Text="&lt;IMG SRC='images/butt-delete.gif' border=0 title='Delete' &gt;" ButtonType="LinkButton"
										CommandName="Delete">
										<HeaderStyle Width="3%" CssClass="gridHeading"></HeaderStyle>
										<ItemStyle CssClass="gridField"></ItemStyle>
									</asp:ButtonColumn>
									<asp:EditCommandColumn ButtonType="LinkButton" UpdateText="&lt;IMG SRC='images/butt-update.gif' border=0  title='Update' &gt;"
										CancelText="&lt;IMG SRC='images/butt-cancel.gif' border=0 title='Cancel'&gt;" EditText="&lt;IMG SRC='images/butt-edit.gif' border=0  title='Edit' &gt;">
										<HeaderStyle Width="3%" CssClass="gridHeading"></HeaderStyle>
										<ItemStyle CssClass="gridField"></ItemStyle>
									</asp:EditCommandColumn>
									<asp:TemplateColumn HeaderText="Surcharge Code">
										<HeaderStyle Width="10%" CssClass="gridHeading"></HeaderStyle>
										<ItemStyle CssClass="gridField"></ItemStyle>
										<ItemTemplate>
											<asp:Label ID="lblVASCode" CssClass="gridLabel" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"vas_code")%>'>
											</asp:Label>
										</ItemTemplate>
										<EditItemTemplate>
											<cc1:mstextbox CssClass="gridTextBox" runat="server" ID="txtVASCode" MaxLength="12" TextMaskType="msUpperAlfaNumericWithUnderscore" Text='<%#DataBinder.Eval(Container.DataItem,"vas_code")%>'>
											</cc1:mstextbox>
											<asp:RequiredFieldValidator ID="validVASCode" Runat="server" BorderWidth="0" Display="None" ControlToValidate="txtVASCode"
												ErrorMessage="The VAS Code is required"></asp:RequiredFieldValidator>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:ButtonColumn Text="&lt;IMG SRC='images/butt-search.gif' border=0 title='Delete' &gt;" ButtonType="LinkButton"
										CommandName="Search">
										<HeaderStyle Width="3%" CssClass="gridHeading"></HeaderStyle>
										<ItemStyle CssClass="gridField"></ItemStyle>
									</asp:ButtonColumn>
									<asp:TemplateColumn HeaderText="Description">
										<HeaderStyle Width="15%" CssClass="gridHeading"></HeaderStyle>
										<ItemStyle CssClass="gridField"></ItemStyle>
										<ItemTemplate>
											<asp:Label runat="server" CssClass="gridLabel" ID="lblVASDesc" Text='<%#DataBinder.Eval(Container.DataItem,"vas_description")%>'>
											</asp:Label>
										</ItemTemplate>
										<EditItemTemplate>
											<asp:TextBox CssClass="gridTextBox" runat="server" ID="txtVASDesc" Enabled=True ReadOnly=True EnableViewState=True Text='<%#DataBinder.Eval(Container.DataItem,"vas_description")%>'>
											</asp:TextBox>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Amount">
										<HeaderStyle Width="10%" CssClass="gridHeading"></HeaderStyle>
										<ItemStyle CssClass="gridField"></ItemStyle>
										<ItemTemplate>
											<asp:Label runat="server" CssClass="gridLabelNumber" ID="lblSurcharge" Text='<%#DataBinder.Eval(Container.DataItem,"surcharge",(String)ViewState["m_format"])%>'>
											</asp:Label>
										</ItemTemplate>
										<EditItemTemplate>
											<asp:TextBox ID="txtSurcharge" CssClass="gridTextBoxNumber" runat="server" Enabled="False" EnableViewState="True" Text='<%#DataBinder.Eval(Container.DataItem,"surcharge",(String)ViewState["m_format"])%>'>
											</asp:TextBox>
											<asp:TextBox ID="txtSurchargeHinden" style="DISPLAY: none" runat="server" Enabled="True" Text='<%#DataBinder.Eval(Container.DataItem,"surcharge",(String)ViewState["m_format"])%>'>
											</asp:TextBox>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Remarks">
										<HeaderStyle Width="56%" CssClass="gridHeading"></HeaderStyle>
										<ItemStyle CssClass="gridField"></ItemStyle>
										<ItemTemplate>
											<asp:Label runat="server" CssClass="gridLabel" ID="lblRemarks" Text='<%#DataBinder.Eval(Container.DataItem,"remarks")%>'>
											</asp:Label>
										</ItemTemplate>
										<EditItemTemplate>
											<asp:TextBox CssClass="gridTextBox" runat="server" ID="txtRemarks" Text='<%#DataBinder.Eval(Container.DataItem,"remarks")%>'>
											</asp:TextBox>
										</EditItemTemplate>
									</asp:TemplateColumn>
								</Columns>
								<PagerStyle NextPageText="Next" Font-Size="Smaller" PrevPageText="Previous" HorizontalAlign="Right"
									Height="20"></PagerStyle>
							</asp:datagrid></td>
					</TR>
					<tr vAlign="top">
						<td style="WIDTH: 722px; HEIGHT: 26px" height="26" width="738" align="right"><asp:button id="btnDGInsert" tabIndex="59" runat="server" CssClass="queryButton" Width="69px"
								CausesValidation="False" Text="Insert"></asp:button></td>
					</tr>
					<TR vAlign="top">
						<TD style="WIDTH: 738px" width="738" align="left">
							<TABLE style="Z-INDEX: 120; WIDTH: 737px; HEIGHT: 158px" id="tblCharges" runat="server">
								<TR>
									<TD style="WIDTH: 144px" width="144"></TD>
									<TD width="20%" align="right"><asp:label style="Z-INDEX: 0" id="Label8" tabIndex="121" runat="server" CssClass="tableLabel"
											Height="20px" Font-Bold="True">Final Ratings</asp:label></TD>
									<TD width="10%"></TD>
									<TD width="30%" align="right"><asp:label style="Z-INDEX: 0" id="lbOverridden" tabIndex="121" runat="server" CssClass="tableLabel"
											Width="146px" Height="20px" Font-Bold="True">Overridden Ratings</asp:label></TD>
									<TD width="50%"></TD>
									<TD width="50%" align="right"><asp:label style="Z-INDEX: 0" id="Label10" tabIndex="121" runat="server" CssClass="tableLabel"
											Width="152px" Height="20px" Font-Bold="True">Post-Invoicing Charges</asp:label></TD>
								</TR>
								<TR>
									<TD style="WIDTH: 144px" width="144"><asp:label style="Z-INDEX: 0" id="Label11" tabIndex="117" runat="server" CssClass="tableLabel"
											Width="141px" Height="20px">Basic Charge</asp:label></TD>
									<TD width="20%" align="right"><asp:textbox style="Z-INDEX: 0" id="txtBasicCharge" runat="server" ReadOnly="True" CssClass="textFieldRightAlign"
											Width="131px"></asp:textbox></TD>
									<TD width="10%"></TD>
									<TD width="30%" align="right"><asp:textbox style="Z-INDEX: 0" id="txtOvrBasicCharge" runat="server" ReadOnly="True" CssClass="textFieldRightAlign"
											Width="131px"></asp:textbox></TD>
									<TD width="50%"><asp:label style="Z-INDEX: 0" id="lblPODEX" tabIndex="121" runat="server" CssClass="tableLabel"
											Width="141px" Height="20px">PODEX Surcharges</asp:label></TD>
									<TD width="50%"><asp:textbox style="Z-INDEX: 0" id="txtPODEX" tabIndex="122" runat="server" ReadOnly="True" CssClass="textFieldRightAlign"
											Width="131px"></asp:textbox></TD>
								</TR>
								<tr height="25">
									<td style="WIDTH: 144px" width="144"><asp:label id="lblFreightChrg" tabIndex="117" runat="server" CssClass="tableLabel" Width="141px"
											Height="20px">Freight Charge</asp:label></td>
									<td width="20%"><asp:textbox id="txtFreightChrg" runat="server" ReadOnly="True" CssClass="textFieldRightAlign"
											Width="131px"></asp:textbox></td>
									<td width="10%"></td>
									<td width="30%" align="right"><asp:textbox id="txtOriFreightChrg" runat="server" ReadOnly="True" CssClass="textFieldRightAlign"
											Width="131px"></asp:textbox></td>
									<TD width="50%"><asp:label style="Z-INDEX: 0" id="lblMBGAmt" tabIndex="123" runat="server" CssClass="tableLabel"
											Width="141px" Height="20px">MBG Amount</asp:label></TD>
									<TD width="50%"><asp:textbox style="Z-INDEX: 0" id="txtMBGAmt" tabIndex="124" runat="server" ReadOnly="True"
											CssClass="textFieldRightAlign" Width="131px"></asp:textbox></TD>
								</tr>
								<tr height="25">
									<td style="WIDTH: 144px" width="144"><asp:label id="lblInsChrg" tabIndex="119" runat="server" CssClass="tableLabel" Width="141px"
											Height="20px">Insurance Surcharge</asp:label></td>
									<td width="20%"><asp:textbox id="txtInsChrg" tabIndex="120" runat="server" ReadOnly="True" CssClass="textFieldRightAlign"
											Width="131px"></asp:textbox></td>
									<td width="10%"></td>
									<td width="30%" align="right"><asp:textbox id="txtOriInsChrg" tabIndex="120" runat="server" ReadOnly="True" CssClass="textFieldRightAlign"
											Width="131px"></asp:textbox></td>
									<TD width="50%"><asp:label style="Z-INDEX: 0" id="lblOtherInvC_D" tabIndex="125" runat="server" CssClass="tableLabel"
											Width="141px" Height="20px">Other Invoice Credit/Debit</asp:label></TD>
									<TD width="50%"><asp:textbox style="Z-INDEX: 0" id="txtOtherInvC_D" tabIndex="126" runat="server" ReadOnly="True"
											CssClass="textFieldRightAlign" Width="131px"></asp:textbox></TD>
								</tr>
								<%--By Aoo 22/02/2008--%>
								<TR>
									<TD style="WIDTH: 144px; HEIGHT: 22px" width="144"><asp:label style="Z-INDEX: 0" id="Label12" tabIndex="119" runat="server" CssClass="tableLabel"
											Width="141px" Height="20px">Non-VAS Surcharges</asp:label></TD>
									<TD style="HEIGHT: 22px" width="20%"><asp:textbox style="Z-INDEX: 0" id="txtNonVASSurcharges" tabIndex="120" runat="server" ReadOnly="True"
											CssClass="textFieldRightAlign" Width="131px"></asp:textbox></TD>
									<TD style="HEIGHT: 22px" width="10%"></TD>
									<TD style="HEIGHT: 22px" width="30%" align="right"><asp:textbox style="Z-INDEX: 0" id="txtOvrrNonVASSurcharges" tabIndex="120" runat="server" ReadOnly="True"
											CssClass="textFieldRightAlign" Width="131px"></asp:textbox></TD>
									<TD style="HEIGHT: 22px" width="50%"><asp:label style="Z-INDEX: 0" id="lblTotalInvAmt" tabIndex="121" runat="server" CssClass="tableLabel"
											Width="141px" Height="20px">Total Invoiced Amount</asp:label></TD>
									<TD style="HEIGHT: 22px" width="50%"><asp:textbox style="Z-INDEX: 0" id="txtTotalInvAmt" tabIndex="122" runat="server" ReadOnly="True"
											CssClass="textFieldRightAlign" Width="131px"></asp:textbox></TD>
								</TR>
								<tr height="25">
									<td style="WIDTH: 144px" width="144"><asp:label style="Z-INDEX: 0" id="lblTotVASSurChrg" tabIndex="121" runat="server" CssClass="tableLabel"
											Width="141px" Height="20px"> VAS Surcharges</asp:label></td>
									<td width="20%"><asp:textbox style="Z-INDEX: 0" id="txtTotVASSurChrg" tabIndex="122" runat="server" ReadOnly="True"
											CssClass="textFieldRightAlign" Width="131px"></asp:textbox></td>
									<TD width="10%"></TD>
									<td width="30%" align="right"><asp:textbox style="Z-INDEX: 0" id="txtOritotVas" tabIndex="124" runat="server" ReadOnly="True"
											CssClass="textFieldRightAlign" Width="131px"></asp:textbox></td>
									<TD width="50%"><asp:label style="Z-INDEX: 0" id="lblCreditNotes" tabIndex="123" runat="server" CssClass="tableLabel"
											Width="141px" Height="20px">Credit Notes</asp:label></TD>
									<TD width="50%"><asp:textbox style="Z-INDEX: 0" id="txtCreditNotes" tabIndex="124" runat="server" ReadOnly="True"
											CssClass="textFieldRightAlign" Width="131px"></asp:textbox></TD>
								</tr>
								<%--End By Aoo 22/02/2008--%>
								<tr height="25">
									<td style="WIDTH: 144px" width="144"><asp:label style="Z-INDEX: 0" id="lblESASurchrg" tabIndex="123" runat="server" CssClass="tableLabel"
											Width="141px" Height="20px">ESA Surcharge</asp:label></td>
									<TD width="20%"><asp:textbox style="Z-INDEX: 0" id="txtESASurchrg" tabIndex="124" runat="server" ReadOnly="True"
											CssClass="textFieldRightAlign" Width="131px"></asp:textbox></TD>
									<TD width="10%"></TD>
									<TD width="30%" align="right"><asp:textbox style="Z-INDEX: 0" id="txtOriESASurchrg" tabIndex="124" runat="server" ReadOnly="True"
											CssClass="textFieldRightAlign" Width="131px"></asp:textbox></TD>
									<TD width="50%"><asp:label style="Z-INDEX: 0" id="lblDebitNotes" tabIndex="125" runat="server" CssClass="tableLabel"
											Width="141px" Height="20px">Debit Notes</asp:label></TD>
									<TD width="50%"><asp:textbox style="Z-INDEX: 0" id="txtDebitNotes" tabIndex="126" runat="server" ReadOnly="True"
											CssClass="textFieldRightAlign" Width="131px"></asp:textbox></TD>
								</tr>
								<tr height="25">
									<td style="WIDTH: 144px" width="144"><asp:label style="Z-INDEX: 0" id="lblShpTotAmt" tabIndex="125" runat="server" CssClass="tableLabel"
											Width="141px" Height="20px">Total Rated Amount</asp:label></td>
									<TD width="20%"><asp:textbox style="Z-INDEX: 0" id="txtShpTotAmt" tabIndex="126" runat="server" ReadOnly="True"
											CssClass="textFieldRightAlign" Width="131px"></asp:textbox></TD>
									<td width="10%"></td>
									<td width="30%" align="center"><asp:label style="Z-INDEX: 0" id="lblRateOverrideBy" tabIndex="121" runat="server" CssClass="tableLabel"
											Height="20px">Override Applied by User</asp:label></td>
									<TD width="50%"><asp:label style="Z-INDEX: 0" id="lblTotalConRevenue" tabIndex="121" runat="server" CssClass="tableLabel"
											Width="141px" Height="20px">Total Consignment Revenue</asp:label></TD>
									<TD width="50%"><asp:textbox style="Z-INDEX: 0" id="txtTotalConRevenue" tabIndex="122" runat="server" ReadOnly="True"
											CssClass="textFieldRightAlign" Width="131px"></asp:textbox></TD>
								</tr>
								<tr height="25">
									<td style="WIDTH: 144px" width="144"><asp:label style="Z-INDEX: 0" id="Label13" tabIndex="125" runat="server" CssClass="tableLabel"
											Width="141px" Height="20px">GST on Rated Amount</asp:label></td>
									<td width="20%"><asp:textbox style="Z-INDEX: 0" id="txtGST" tabIndex="126" runat="server" ReadOnly="True" CssClass="textFieldRightAlign"
											Width="131px"></asp:textbox></td>
									<TD width="10%"></TD>
									<td width="30%" align="right"><asp:textbox style="Z-INDEX: 0" id="txtRateOverrideBy" tabIndex="124" runat="server" ReadOnly="True"
											CssClass="textFieldRightAlign" Width="131px" Enabled="False"></asp:textbox></td>
									<TD width="50%"><asp:label style="Z-INDEX: 0" id="lblPaidRevenue" tabIndex="121" runat="server" CssClass="tableLabel"
											Width="141px" Height="20px">Paid Revenue</asp:label></TD>
									<TD width="50%"><asp:textbox style="Z-INDEX: 0" id="txtPaidRevenue" tabIndex="122" runat="server" ReadOnly="True"
											CssClass="textFieldRightAlign" Width="131px"></asp:textbox></TD>
								</tr>
								<%--By Aoo 22/02/2008--%>
								<tr height="25">
									<td style="WIDTH: 144px" width="144"><asp:label style="Z-INDEX: 0" id="Label14" tabIndex="125" runat="server" CssClass="tableLabel"
											Width="141px" Height="20px">Export (International) Freight Charge</asp:label></td>
									<td width="20%"><asp:textbox style="Z-INDEX: 0" id="txtExportInterFreiChar" tabIndex="126" runat="server" ReadOnly="True"
											CssClass="textFieldRightAlign" Width="131px"></asp:textbox></td>
									<td width="10%"></td>
									<td width="30%" align="center"><asp:label style="Z-INDEX: 0" id="lblRateOverrideDate" tabIndex="121" runat="server" CssClass="tableLabel"
											Height="20px">Override Applied on D/T</asp:label></td>
									<TD width="50%"></TD>
									<TD width="50%"></TD>
								</tr>
								<tr height="25">
									<td style="WIDTH: 144px" width="144"><asp:label style="Z-INDEX: 0" id="Label15" tabIndex="119" runat="server" CssClass="tableLabel"
											Width="141px" Height="20px">Customs Fees</asp:label></td>
									<TD width="20%"><asp:textbox style="Z-INDEX: 0" id="txtCustomsFees" tabIndex="126" runat="server" ReadOnly="True"
											CssClass="textFieldRightAlign" Width="131px"></asp:textbox></TD>
									<TD width="10%"></TD>
									<TD width="30%" align="right"><asp:textbox style="Z-INDEX: 0" id="txtRateOverrideDate" tabIndex="124" runat="server" ReadOnly="True"
											CssClass="textFieldRightAlign" Width="131px" Enabled="False"></asp:textbox></TD>
									<TD width="50%"></TD>
									<TD width="50%"></TD>
								</tr>
								<TR height="25">
									<TD style="WIDTH: 144px" width="144"><asp:label style="Z-INDEX: 0" id="Label16" tabIndex="119" runat="server" CssClass="tableLabel"
											Width="141px" Height="20px">Freight Collect Charges</asp:label></TD>
									<TD width="20%"><asp:textbox style="Z-INDEX: 0" id="txtFreCollChar" tabIndex="126" runat="server" ReadOnly="True"
											CssClass="textFieldRightAlign" Width="131px"></asp:textbox></TD>
									<TD width="10%"></TD>
									<TD width="30%" align="right"></TD>
									<TD width="50%"></TD>
									<TD width="50%"></TD>
								</TR>
								<TR height="25">
									<TD style="WIDTH: 144px" width="144"><asp:label style="Z-INDEX: 0" id="lblOtherSurcharge" tabIndex="119" runat="server" Visible="False"
											CssClass="tableLabel" Width="141px" Height="20px">Other Surcharge</asp:label></TD>
									<TD width="20%"><asp:textbox style="Z-INDEX: 0" id="txtOtherSurcharge2" tabIndex="120" runat="server" Visible="False"
											ReadOnly="True" CssClass="textFieldRightAlign" Width="131px"></asp:textbox></TD>
									<TD width="10%"></TD>
									<TD width="30%" align="right"></TD>
									<TD width="50%"></TD>
									<TD width="50%"></TD>
								</TR>
								<tr height="25">
									<td style="WIDTH: 144px" width="144"></td>
									<td width="20%"></td>
									<td width="10%"></td>
									<td width="30%" align="right"><asp:label style="Z-INDEX: 0" id="lblOritotVas" tabIndex="121" runat="server" Visible="False"
											CssClass="tableLabel" Height="12px"></asp:label></td>
									<TD width="50%"><asp:textbox style="Z-INDEX: 0" id="txtOriShpTotAmt" tabIndex="126" runat="server" ReadOnly="True"
											CssClass="textFieldRightAlign" Width="131px"></asp:textbox></TD>
									<TD width="50%"></TD>
								</tr>
								<tr height="25">
									<td style="WIDTH: 144px" width="144"></td>
									<TD width="20%"></TD>
									<TD width="10%"></TD>
									<TD width="30%" align="right"><asp:textbox style="Z-INDEX: 0" id="txtOriOthersurcharge2" tabIndex="120" runat="server" ReadOnly="True"
											CssClass="textFieldRightAlign" Width="131px"></asp:textbox></TD>
									<TD></TD>
									<TD></TD>
								</tr>
								<TR height="25">
									<TD style="WIDTH: 144px" width="144"></TD>
									<TD width="20%"></TD>
									<TD width="10%"></TD>
									<TD width="30%" align="right"></TD>
									<TD width="50%"><asp:label style="Z-INDEX: 0" id="lblRateOverrideByValue" tabIndex="121" runat="server" CssClass="tableLabel"
											Height="20px" Font-Bold="True"></asp:label></TD>
									<TD width="50%"></TD>
								</TR>
								<TR>
									<TD style="WIDTH: 144px" width="144"><FONT face="Tahoma"></FONT></TD>
									<TD width="20%"><FONT face="Tahoma"></FONT></TD>
									<TD width="10%"></TD>
									<TD width="30%" align="right"></TD>
									<TD width="50%"><asp:label style="Z-INDEX: 0" id="lblRateOverrideDateValue" tabIndex="121" runat="server" CssClass="tableLabel"
											Height="20px" Font-Bold="True"></asp:label></TD>
									<TD width="50%"></TD>
								</TR>
								<%--End By Aoo 22/02/2008--%>
							</TABLE>
						</TD>
					</TR>
				</TABLE>
			</DIV>
			&nbsp;
			<DIV style="Z-INDEX: 101; POSITION: relative; WIDTH: 444px; HEIGHT: 224px; TOP: 46px; LEFT: 70px; relative: absolute"
				id="DomstcShipPanel" MS_POSITIONING="GridLayout" runat="server">
				<FIELDSET style="Z-INDEX: 80; POSITION: relative; WIDTH: 374px; HEIGHT: 178px; TOP: 46px; LEFT: 70px; relative: absolute"
					MS_POSITIONING="GridLayout"><LEGEND><asp:label id="lblConf" CssClass="tableHeadingFieldset" Runat="server">Confirmation</asp:label></LEGEND>
					<TABLE style="Z-INDEX: 75; POSITION: absolute; WIDTH: 346px; HEIGHT: 153px; TOP: 23px; LEFT: 7px"
						id="Table1" runat="server">
						<TR>
							<TD>
								<P align="center"><asp:label id="lblConfirmMsg" runat="server" Width="321px"></asp:label></P>
								<P>
								<P align="center"><asp:button id="btnToSaveChanges" runat="server" CssClass="queryButton" Text="Yes"></asp:button><asp:button id="btnNotToSave" runat="server" CssClass="queryButton" CausesValidation="False"
										Text="No"></asp:button><asp:button id="btnToCancel" runat="server" CssClass="queryButton" CausesValidation="False"
										Text="Cancel"></asp:button></P>
								<P></P>
							</TD>
						</TR>
					</TABLE>
				</FIELDSET>
			</DIV>
			<DIV style="Z-INDEX: 104; POSITION: relative; WIDTH: 637px; HEIGHT: 176px; TOP: 46px; LEFT: 7px"
				id="divCustIdCng" MS_POSITIONING="GridLayout" runat="server">
				<TABLE style="Z-INDEX: 105; POSITION: absolute; WIDTH: 530px; HEIGHT: 129px; TOP: 16px; LEFT: 43px"
					id="Table2" runat="server">
					<TR>
						<TD>
							<P>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<asp:label id="lblCustIdCngMsg" runat="server" Width="337px" Height="46px">All changes cannot be reverted back <br>Do you want to change to another customer ID ?</asp:label></P>
							<P>
							<P>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<asp:button id="btnCustIdChgYes" runat="server" CssClass="queryButton" CausesValidation="False"
									Text="Yes"></asp:button>&nbsp;
								<asp:button id="btnCustIdChgNo" runat="server" CssClass="queryButton" CausesValidation="False"
									Text=" No "></asp:button></P>
						</TD>
					</TR>
				</TABLE>
			</DIV>
			<DIV style="Z-INDEX: 106; POSITION: relative; WIDTH: 625px; HEIGHT: 217px; TOP: 46px; LEFT: 22px"
				id="divDelOperation" MS_POSITIONING="GridLayout" runat="server">
				<TABLE style="Z-INDEX: 109; POSITION: absolute; WIDTH: 545px; HEIGHT: 107px; TOP: 20px; LEFT: 21px"
					id="Table3" runat="server">
					<TR>
						<TD>
							<P align="center"><asp:label id="lblDelOperation" runat="server" Width="400px" Height="36px">Record Deleted cannot be reverted back. Confirm Deletion ?</asp:label></P>
							<P align="center"><asp:button id="btnDelOperationYes" runat="server" CssClass="queryButton" CausesValidation="False"
									Text="Yes"></asp:button>&nbsp;
								<asp:button id="btnDelOperationNo" runat="server" CssClass="queryButton" CausesValidation="False"
									Text=" No "></asp:button></P>
						</TD>
					</TR>
				</TABLE>
			</DIV>
			<DIV style="Z-INDEX: 105; POSITION: relative; WIDTH: 625px; HEIGHT: 217px; TOP: 46px; LEFT: 22px"
				id="divUpdateAutoManifest" MS_POSITIONING="GridLayout" runat="server">
				<TABLE style="Z-INDEX: 109; POSITION: absolute; WIDTH: 545px; HEIGHT: 107px; TOP: 20px; LEFT: 21px"
					id="Table4" runat="server">
					<TR>
						<TD>
							<P align="center"><asp:label id="Label2" runat="server" Width="400px" Height="36px">Shipment was originally manifested on a different route /
day. This shipment will be reassigned to a new route.
You must manually remove the shipment from prior
linehaul and / or delivery manifests.</asp:label></P>
							<P align="center"><asp:button id="btnUpdateAutoManifest" runat="server" CssClass="queryButton" CausesValidation="False"
									Text="OK"></asp:button></P>
						</TD>
					</TR>
				</TABLE>
			</DIV>
			<DIV style="Z-INDEX: 103; POSITION: relative; WIDTH: 439px; HEIGHT: 218px; TOP: 40px; LEFT: 46px"
				id="ExceedTimeState" MS_POSITIONING="GridLayout" runat="server"><FONT face="Tahoma"></FONT><FONT face="Tahoma"></FONT><BR>
				<P align="center"><asp:label style="Z-INDEX: 110; POSITION: absolute; TOP: 39px; LEFT: 41px" id="lbl_ExceedTimeState"
						runat="server" Width="359px">The Estimated Pickup Date/Time exceeds the Cut-off time for Sender Postal code. To override the standard cut-off time and still schecule the pick up at the time you have entered press Cancel. To reschedule the pickup before the Cut-off time press OK.</asp:label></P>
				<P align="center"><asp:button style="Z-INDEX: 101; POSITION: absolute; TOP: 153px; LEFT: 220px" id="btnExceedCancel"
						runat="server" CssClass="queryButton" Width="65px" CausesValidation="False" Text="Cancel"></asp:button><asp:button style="Z-INDEX: 103; POSITION: absolute; TOP: 153px; LEFT: 169px" id="btnExceedOK"
						runat="server" CssClass="queryButton" Width="44px" CausesValidation="False" Text="OK"></asp:button></P>
			</DIV>
			<INPUT 
value="<%=strScrollPosition%>" type=hidden name=ScrollPosition> <INPUT type="hidden" name="hdnRefresh">
		</form>
	</body>
</HTML>
