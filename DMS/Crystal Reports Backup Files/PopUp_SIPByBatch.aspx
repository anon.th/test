<%@ Import Namespace="com.common.util" %>
<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<%@ Page language="c#" Codebehind="PopUp_SIPByBatch.aspx.cs" AutoEventWireup="false" Inherits="TIES.WebUI.Manifest.PopUp_SIPByBatch" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
  <HEAD>
		<title>SIP by Batch</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK rel="stylesheet" type="text/css" href="../css/Styles.css">
		<!--#INCLUDE FILE="../msFormValidations.inc"-->
		<script language="javascript" src="../Scripts/settingScrollPosition.js"></script>
</HEAD>
	<BODY onclick="GetScrollPosition();" onunload="window.top.displayBanner.fnCloseAll(0);"
		onload="SetScrollPosition();" MS_POSITIONING="GridLayout" scroll="yes">
		<FORM id="PopUp_CreateUpdateBatch" method="post" runat="server">
			<asp:label style="Z-INDEX: 105; POSITION: absolute; TOP: 8px; LEFT: 16px" id="lblError" runat="server"
				Width="280px" CssClass="errorMsgColor"></asp:label><asp:label style="Z-INDEX: 107; POSITION: absolute; TOP: 32px; LEFT: 120px" id="lblCurrentLocation"
				runat="server" CssClass="tableLabel"></asp:label><asp:label style="Z-INDEX: 106; POSITION: absolute; TOP: 56px; LEFT: 16px" id="lblDisDesctination"
				runat="server" Width="232px" CssClass="tableLabel">SIP consignments from the batch where destination DC is:</asp:label><asp:label style="Z-INDEX: 102; POSITION: absolute; TOP: 32px; LEFT: 16px" id="lblDisCurrentLocatrion"
				runat="server" CssClass="tableLabel"> Current Location:</asp:label>&nbsp;<asp:button style="Z-INDEX: 100; POSITION: absolute; TOP: 32px; LEFT: 328px" id="btnOk" runat="server"
				Width="101px" CssClass="queryButton" Text="OK"></asp:button><asp:button style="Z-INDEX: 101; POSITION: absolute; TOP: 56px; LEFT: 328px" id="btnCancel"
				runat="server" Width="101px" CssClass="queryButton" Text="Cancel"></asp:button>
			<asp:checkboxlist style="Z-INDEX: 104; POSITION: absolute; TOP: 96px; LEFT: 16px" id="ckboxdestination"
				runat="server" CssClass="tablelabel" AutoPostBack="True"></asp:checkboxlist></FORM>
	</BODY>
</HTML>
