<%@ Register TagPrefix="DbCombo" Namespace="Cambro.Web.DbCombo" Assembly="Cambro.Web.DbCombo" %>
<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<%@ Import Namespace="Cambro.Web.DbCombo" %>
<%@ Import Namespace="com.common.util" %>
<%@ Page language="c#" Codebehind="CustomerLastShipmentDate.aspx.cs" AutoEventWireup="false" Inherits="com.ties.CustomerLastShipmentDate" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
  <HEAD>
		<title>ShipmentTrackingQuery</title>
		<LINK href="css/Styles.css" type="text/css" rel="stylesheet" name="stylesheet">
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<!--#INCLUDE FILE="msFormValidations.inc"-->
		<script language="javascript" src="Scripts/settingScrollPosition.js"></script>
</HEAD>
	<body onclick="GetScrollPosition();" onload="SetScrollPosition();" onunload="window.top.displayBanner.fnCloseAll(0);"
		MS_POSITIONING="GridLayout">
		<form id="ShipmentTrackingQueryRpt" method="post" runat="server">
			<asp:button id="btnQuery" style="Z-INDEX: 101; LEFT: 20px; POSITION: absolute; TOP: 40px" runat="server"
				Width="64px" CssClass="queryButton" Text="Query"></asp:button><asp:label id="lblErrorMessage" style="Z-INDEX: 104; LEFT: 20px; POSITION: absolute; TOP: 71px"
				runat="server" Width="700px" CssClass="errorMsgColor" Height="17px">Place holder for err msg</asp:label>&nbsp;
			<asp:button id="btnExecuteQuery" style="Z-INDEX: 102; LEFT: 84px; POSITION: absolute; TOP: 40px"
				runat="server" Width="123px" CssClass="queryButton" Text="Execute Query"></asp:button><asp:label id="Label1" style="Z-INDEX: 103; LEFT: 20px; POSITION: absolute; TOP: 4px" runat="server"
				Width="365px" CssClass="maintitleSize" Height="27px">Customer Last Shipment Date Report</asp:label></TR></TABLE></TR></TABLE><INPUT 
type=hidden value="<%=strScrollPosition%>" name=ScrollPosition>
			<fieldset style="Z-INDEX: 105; LEFT: 16px; WIDTH: 648px; POSITION: absolute; TOP: 88px; HEIGHT: 160px">
				<P><FONT face="Tahoma"></FONT><FONT face="Tahoma"></FONT></P>
				<P><FONT face="Tahoma"></FONT></P>
				<P><FONT face="Tahoma"><asp:radiobutton id="rdMonth" runat="server" Width="56px" Text="Month" Height="20px" GroupName="shp"
							AutoPostBack="True" Visible="False"></asp:radiobutton></FONT></P>
			</fieldset>
			<TABLE class="tableRadioButton" id="Table1" style="Z-INDEX: 106; LEFT: 56px; WIDTH: 528px; POSITION: absolute; TOP: 112px; HEIGHT: 100px"
				cellSpacing="0" cellPadding="0" width="528" border="0">
				<TR>
					<TD style="WIDTH: 541px; HEIGHT: 22px" colSpan="4"><FONT face="Tahoma"></FONT><FONT face="Tahoma">Customer 
							Not Ship After</FONT></TD>
					<TD style="WIDTH: 28px; HEIGHT: 22px"></TD>
				</TR>
				<TR>
					<TD style="WIDTH: 34px; HEIGHT: 23px" align="left"><FONT face="Tahoma">Year</FONT></TD>
					<TD style="WIDTH: 465px; HEIGHT: 23px" colSpan="3"><asp:dropdownlist id="cboyear" runat="server" Width="104px"></asp:dropdownlist></TD>
					<TD style="WIDTH: 28px; HEIGHT: 23px"><FONT face="Tahoma"></FONT></TD>
				</TR>
				<TR>
					<TD style="WIDTH: 34px; HEIGHT: 23px" align="left"><FONT face="Tahoma">Month</FONT></TD>
					<TD style="WIDTH: 183px; HEIGHT: 23px" align="right" colSpan="2"><asp:dropdownlist id="CboMonth" runat="server" Width="144px"></asp:dropdownlist></TD>
					<TD style="WIDTH: 263px; HEIGHT: 23px"><FONT face="Tahoma"> &nbsp;&nbsp;&nbsp;
							<asp:dropdownlist id="cboMonthend" runat="server" Width="146px" Visible="False"></asp:dropdownlist></FONT></TD>
					<TD style="WIDTH: 28px; HEIGHT: 23px" align="right"><FONT face="Tahoma"></FONT></TD>
				</TR>
				<TR>
					<TD style="WIDTH: 34px; HEIGHT: 23px" align="right"><FONT face="Tahoma"></FONT></TD>
					<TD style="WIDTH: 183px; HEIGHT: 23px" colSpan="2"><FONT face="Tahoma"></FONT></TD>
					<TD style="WIDTH: 263px; HEIGHT: 23px"><FONT face="Tahoma"></FONT></TD>
					<TD style="WIDTH: 28px; HEIGHT: 23px"></TD>
				</TR>
			</TABLE>
			<P><FONT face="Tahoma"></FONT></P>
		</form>
	</body>
</HTML>
