<%@ Page language="c#" Codebehind="CurrencyExchangeRates.aspx.cs" AutoEventWireup="false" Inherits="TIES.WebUI.CurrencyExchangeRates" %>
<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Currency Exchange Rates</title>
		<meta name="vs_snapToGrid" content="True">
		<meta name="vs_showGrid" content="False">
		<LINK rel="stylesheet" type="text/css" href="css/Styles.css" name="stylesheet">
		<meta name="GENERATOR" content="Microsoft Visual Studio 7.0">
		<meta name="CODE_LANGUAGE" content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<!--#INCLUDE FILE="msFormValidations.inc"-->
		<script language="javascript" src="Scripts/settingScrollPosition.js"></script>
		<script language="javascript" src="Scripts/JScript_TextControl.js"></script>
		<script language="javascript" type="text/javascript">
			function funcDisBack(){
				history.go(+1);
			}

			function RemoveBadNonNumber(strTemp, obj) {
				strTemp = strTemp.replace(/[^0-9]/g, ''); //replace non-numeric
				obj.value = strTemp.replace(/\s/g, ''); // replace space
			}
			function AfterPasteNonNumber(obj) {
				setTimeout(function () {
					RemoveBadNonNumber(obj.value, obj);
				}, 1); //or 4
			}
			
			function AfterPasteDateEdit(obj) {
				//var strDate = obj.value;
				//obj.value = strDate.toString().trim();
				//alert('xxxx');
			}
			
			function RemoveBadPaseNumberwhithDot(strTemp, obj) {
				strTemp = strTemp.replace(/[^.0-9]/g, ''); //replace non-numeric
				obj.value = strTemp.replace(/\s/g, ''); // replace space
			}
			
			function AfterPasteNumberwhithDot(obj) {
						setTimeout(function () {
							RemoveBadPaseNumberwhithDot(obj.value, obj);
						}, 1); //or 4
					}
			
			
			 function callback()
			{
					var btn = document.getElementById('btnClientEvent');
					if(btn != null)
					{					
						btn.click();
					}else{
						alert('error call code behind');
					}				        
				
			}
			function validateCode(key) { 
            var keycode = (key.which) ? key.which : key.keyCode; 
            if (!(keycode == 8 || keycode == 46) && (keycode < 48 || keycode > 57)) {
                return false;
            }
             
			}
			
			function isValidDate_ddMMYYYY(date) 
			{
				var valid = true;

				date = date.replace('/-/g', '');

				var day   = parseInt(date.substring(0, 2),10);
				var month = parseInt(date.substring(2, 4),10);	
				var year  = parseInt(date.substring(4, 8),10);

				if((month < 1) || (month > 12)) valid = false;
				else if((day < 1) || (day > 31)) valid = false;
				else if(((month == 4) || (month == 6) || (month == 9) || (month == 11)) && (day > 30)) valid = false;
				else if((month == 2) && (((year % 400) == 0) || ((year % 4) == 0)) && ((year % 100) != 0) && (day > 29)) valid = false;
				else if((month == 2) && ((year % 100) == 0) && (day > 29)) valid = false;

				return valid;
			}
			
			function isDecimalValidKey(textboxIn, evt)
			{
				var charCode = (evt.which) ? evt.which : event.keyCode;    

				if (charCode > 31 && (charCode < 45 || charCode == 47 || charCode > 57))
				{
				return false;
				}     

				if(charCode == 46 && textboxIn.value.indexOf(".") != -1)
				{
					return false;
				}   
				return true;
			} 
		</script>
	</HEAD>
	<body onclick="GetScrollPosition();" onunload="window.top.displayBanner.fnCloseAll(0);"
		onload="SetScrollPosition();" MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<div style="Z-INDEX: 0; POSITION: relative; WIDTH: 1007px; HEIGHT: 1248px" id="divMain"
				MS_POSITIONING="GridLayout" runat="server">
				<TABLE style="Z-INDEX: 101; POSITION: absolute; TOP: 8px; LEFT: 0px" id="MainTable" border="0"
					width="100%" runat="server">
					<TBODY>
						<tr>
							<td colSpan="2" align="left"><asp:label id="Label1" runat="server" CssClass="mainTitleSize" Height="27px" Width="421px">Currency Exchange Rates</asp:label></td>
						</tr>
						<tr>
							<td style="HEIGHT: 26px" vAlign="top" colSpan="2" align="left"><asp:button id="btnQry" runat="server" CssClass="queryButton" CausesValidation="False" Text="Query"></asp:button><label style="WIDTH: 5px"></label><asp:button id="btnExecQry" runat="server" CssClass="queryButton" CausesValidation="False" Text="Execute Query"></asp:button><label style="WIDTH: 5px"></label></td>
						</tr>
						<tr>
							<td vAlign="top" colSpan="2" align="left">
								<TABLE style="Z-INDEX: 112; WIDTH: 1000px" id="Table1" border="0" width="730" runat="server">
									<TR width="100%">
										<TD width="100%">
											<table border="0" cellSpacing="1" cellPadding="1" width="800">
												<TBODY>
													<TR>
														<TD style="HEIGHT: 19px" colSpan="6"><asp:label id="lblError" runat="server" ForeColor="Red" Font-Bold="True" Font-Size="X-Small"></asp:label></TD>
													</TR>
													<TR>
														<TD width="130"><asp:label style="Z-INDEX: 0" id="Label2" runat="server" CssClass="tableLabel">Exchange Rate Type:</asp:label></TD>
														<TD width="670" colSpan="5" align="left"><asp:dropdownlist style="Z-INDEX: 0" id="ddlExchangeRateType" runat="server" Width="150px" AutoPostBack="True"
																tabIndex="1"></asp:dropdownlist></TD>
													</TR>
													<tr>
														<td colSpan="6"><FONT face="Tahoma"></FONT><br>
														</td>
													</tr>
													<tr>
														<td colSpan="6"><asp:datagrid style="Z-INDEX: 0" id="gridCurrency" runat="server" Width="550px" PageSize="5" AllowPaging="True"
																AutoGenerateColumns="False" ShowFooter="True" AlternatingItemStyle-CssClass="gridField" ItemStyle-CssClass="gridField"
																HeaderStyle-CssClass="gridHeading" CellPadding="0" CellSpacing="1" BorderWidth="0px" FooterStyle-CssClass="gridHeading">
																<FooterStyle CssClass="gridHeading"></FooterStyle>
																<AlternatingItemStyle CssClass="gridField"></AlternatingItemStyle>
																<ItemStyle CssClass="gridField"></ItemStyle>
																<HeaderStyle CssClass="gridHeading"></HeaderStyle>
																<Columns>
																	<asp:TemplateColumn>
																		<HeaderStyle Width="20%"></HeaderStyle>
																		<ItemStyle HorizontalAlign="Center"></ItemStyle>
																		<ItemTemplate>
																			<asp:LinkButton id="btnEditItem" Runat="server" Text="<img src='images/butt-edit.gif' alt='edit' border=0>"
																				CommandName="EDIT_ITEM" />
																		</ItemTemplate>
																		<FooterStyle HorizontalAlign="Center"></FooterStyle>
																		<FooterTemplate>
																			<asp:LinkButton id="btnAddItem" Runat="server" Text="<img src='images/butt-add.gif' alt='add' border=0>"
																				CommandName="ADD_ITEM" />
																		</FooterTemplate>
																		<EditItemTemplate>
																			<asp:LinkButton id="lbtSave" Runat="server" Text="<img src='images/butt-update.gif' alt='save' border=0>"
																				CommandName="SAVE_ITEM" />
																			<asp:LinkButton id="lbtCancel" Runat="server" Text="<img src='images/butt-red.gif' alt='cancel' border=0>"
																				CommandName="CANCEL_ITEM" />
																		</EditItemTemplate>
																	</asp:TemplateColumn>
																	<asp:TemplateColumn HeaderText="Currency" FooterText="CurrencyCode">
																		<HeaderStyle HorizontalAlign="Center" Width="200px"></HeaderStyle>
																		<ItemStyle HorizontalAlign="Center"></ItemStyle>
																		<ItemTemplate>
																			<%# DataBinder.Eval(Container.DataItem, "CurrencyCode") %>
																		</ItemTemplate>
																		<FooterStyle HorizontalAlign="Center"></FooterStyle>
																		<FooterTemplate>
																			<asp:DropDownList ID="ddlCurrency" tabIndex="2" Runat="server"></asp:DropDownList>
																		</FooterTemplate>
																		<EditItemTemplate>
																			<%# DataBinder.Eval(Container.DataItem, "CurrencyCode") %>
																			<asp:DropDownList ID="ddlCurrencyEdit" Runat="server" Visible="False"></asp:DropDownList>
																		</EditItemTemplate>
																	</asp:TemplateColumn>
																	<asp:TemplateColumn HeaderText="Date" FooterText="rate_date">
																		<HeaderStyle HorizontalAlign="Center" Width="150px"></HeaderStyle>
																		<ItemStyle HorizontalAlign="Center"></ItemStyle>
																		<ItemTemplate>
																			<%# DateTime.Parse(DataBinder.Eval(Container.DataItem, "rate_date").ToString()).ToString("dd/MM/yyyy") %>
																		</ItemTemplate>
																		<FooterStyle HorizontalAlign="Center"></FooterStyle>
																		<FooterTemplate>
																			<cc1:mstextbox style="Z-INDEX: 0" id="mtxtDate" runat="server" CssClass="textField" Width="90px"
																				MaxLength="12" TextMaskString="99/99/9999" tabIndex="3" TextMaskType="msDate" onpaste="AfterPasteDateEdit(this)"></cc1:mstextbox>
																		</FooterTemplate>
																		<EditItemTemplate>
																			<%# DateTime.Parse(DataBinder.Eval(Container.DataItem, "rate_date").ToString()).ToString("dd/MM/yyyy") %>
																			<cc1:mstextbox style="Z-INDEX: 0" id="mtxtDateEdit" runat="server" CssClass="textField" Width="90px" MaxLength="12" TextMaskString="99/99/9999" TextMaskType="msDate" Visible="False" Text='<%# DateTime.Parse(DataBinder.Eval(Container.DataItem, "rate_date").ToString()).ToString("dd/MM/yyyy")  %>' onpaste="AfterPasteDateEdit(this)" >
																			</cc1:mstextbox>
																		</EditItemTemplate>
																	</asp:TemplateColumn>
																	<asp:TemplateColumn HeaderText="Rate" FooterText="exchange_rate">
																		<HeaderStyle HorizontalAlign="Center" Width="150px"></HeaderStyle>
																		<ItemStyle HorizontalAlign="Center"></ItemStyle>
																		<ItemTemplate>
																			<%# DataBinder.Eval(Container.DataItem, "exchange_rate","{0:#####0.0000}") %>
																		</ItemTemplate>
																		<FooterStyle HorizontalAlign="Center"></FooterStyle>
																		<FooterTemplate>
																			<cc1:mstextbox id="txtExchangeRate" runat="server" CssClass="gridTextBoxNumber" NumberPrecision="10"
																				TextMaskType="msNumericCOD" MaxLength="13" NumberScale="4" NumberMinValue="0" onpaste="AfterPasteNumberwhithDot(this)"
																				NumberMaxValueCOD="999999.9999" tabIndex="4"></cc1:mstextbox>
																		</FooterTemplate>
																		<EditItemTemplate>
																			<cc1:mstextbox id="txtExchangeRateEdit" runat="server" CssClass="gridTextBoxNumber" onpaste="AfterPasteNumberwhithDot(this)" NumberPrecision="10" TextMaskType="msNumericCOD" MaxLength="13" NumberScale="4" NumberMinValue="0" NumberMaxValueCOD="999999.9999" Text='<%# DataBinder.Eval(Container.DataItem, "exchange_rate", "{0:#####0.0000}") %>'>
																			</cc1:mstextbox>
																		</EditItemTemplate>
																	</asp:TemplateColumn>
																</Columns>
																<PagerStyle NextPageText="Next" PrevPageText="Previous" HorizontalAlign="Left" CssClass="normalText"></PagerStyle>
															</asp:datagrid></td>
													</tr>
												</TBODY>
											</table>
										</TD>
									</TR>
								</TABLE>
							</td>
						</tr>
					</TBODY>
				</TABLE>
			</div>
			<INPUT 
value="<%=strScrollPosition%>" type=hidden name=ScrollPosition> <INPUT type="hidden" name="hdnRefresh">
		</form>
	</body>
</HTML>
