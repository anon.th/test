<%@ Page language="c#" Codebehind="ConfigureCustomerSelfService.aspx.cs" AutoEventWireup="false" Inherits="TIES.WebUI.ConfigureCustomerSelfService1" %>
<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Configure Customer Self Service</title>
		<meta name="vs_snapToGrid" content="True">
		<meta name="vs_showGrid" content="True">
		<LINK rel="stylesheet" type="text/css" href="css/Styles.css" name="stylesheet">
		<meta name="GENERATOR" content="Microsoft Visual Studio 7.0">
		<meta name="CODE_LANGUAGE" content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<!--#INCLUDE FILE="msFormValidations.inc"-->
		<script language="javascript" src="Scripts/settingScrollPosition.js"></script>
		<script language="javascript" src="Scripts/JScript_TextControl.js"></script>
		<script language="javascript" src="Scripts/WorkProgress.js"></script>
		<script type="text/javascript">
		//for field number
        function RemoveBadNonNumber(strTemp, obj) {
            strTemp = strTemp.replace(/[^0-9]/g, ''); //replace non-numeric
            obj.value = strTemp.replace(/\s/g, ''); // replace space
        }
        function AfterPasteNonNumber(obj) {
            setTimeout(function () {
                RemoveBadNonNumber(obj.value, obj);
            }, 1); //or 4
        }
		</script>
	</HEAD>
	<body onunload="window.top.displayBanner.fnCloseAll(0);" MS_POSITIONING="GridLayout">
		<form id="ConsignmentsWithoutPkgWeights" method="post" runat="server">
			<INPUT style="Z-INDEX: 101; POSITION: absolute; TOP: 680px; LEFT: 16px" type="hidden" name="ScrollPosition">
			<asp:validationsummary style="Z-INDEX: 105; POSITION: absolute; COLOR: red; TOP: 672px; LEFT: 240px" id="Validationsummary1"
				Runat="server" DisplayMode="BulletList" ShowMessageBox="True" HeaderText="Please enter the missing  fields."
				ShowSummary="False"></asp:validationsummary>
			<div style="Z-INDEX: 102; POSITION: relative; HEIGHT: 1248px; TOP: 16px; LEFT: 14px"
				id="divMain" MS_POSITIONING="GridLayout" runat="server">
				<TABLE style="Z-INDEX: 101; POSITION: absolute; TOP: 8px; LEFT: 0px" id="MainTable" border="0"
					width="100%" runat="server">
					<tr>
						<td colSpan="2" align="left"><asp:label id="Label1" runat="server" CssClass="mainTitleSize" Height="27px" Width="421px">
								Configure Customer Self Service</asp:label></td>
					</tr>
					<tr>
						<td style="HEIGHT: 24px" vAlign="top" colSpan="2" align="left"><asp:button id="btnQry" runat="server" CssClass="queryButton" Width="61px" CausesValidation="False"
								Text="Query"></asp:button><asp:button id="btnExecQry" runat="server" CssClass="queryButton" Width="130px" CausesValidation="False"
								Text="Execute Query"></asp:button><asp:button style="DISPLAY: none; VISIBILITY: hidden" id="btnDelete" runat="server" CssClass="queryButton"
								Width="62px" CausesValidation="False" Text="Delete"></asp:button></td>
					</tr>
					<tr>
						<td style="HEIGHT: 24px" vAlign="top" colSpan="2" align="left"><asp:label id="lblErrorMsg" runat="server" CssClass="errorMsgColor" Height="19px" Width="566px"></asp:label></td>
					</tr>
					<tr>
						<td vAlign="top" colSpan="2" align="left">
							<table border="0" cellSpacing="1" cellPadding="1">
								<tr>
									<td><asp:label style="Z-INDEX: 0" id="Label3" runat="server" CssClass="tableLabel" Width="55px">Payer ID
										</asp:label></td>
									<td colSpan="3"><asp:dropdownlist style="Z-INDEX: 0" id="ddlPayerID" runat="server" CssClass="textField" Width="72px"
											AutoPostBack="False">
											<asp:ListItem Value=""></asp:ListItem>
										</asp:dropdownlist></td>
									<td><asp:label style="Z-INDEX: 0" id="lblCustomerAccountLogin" runat="server" CssClass="tableLabel"
											Width="150px" Visible="False">Customer Login Account
										</asp:label></td>
									<td><asp:textbox id="txtCustomerAccountLogin" runat="server" CssClass="textField" Visible="False"></asp:textbox></td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td colSpan="2"><asp:label style="Z-INDEX: 0" id="lblNote" runat="server" CssClass="errorMsgColor" Height="19px"
								Width="566px" ForeColor="Black"></asp:label></td>
					</tr>
					<tr>
						<td colSpan="2"><asp:datagrid style="Z-INDEX: 0" id="dgConfigSSPNG" runat="server" Width="850px" ShowHeader="True"
								SelectedItemStyle-CssClass="gridFieldSelected" AutoGenerateColumns="False" HorizontalAlign="Left" OnUpdateCommand="OnUpdate_dgConfigSSPNG"
								OnCancelCommand="OnCancel_dgConfigSSPNG" OnItemDataBound="OnItemBound_dgConfigSSPNG" OnEditCommand="OnEdit_dgConfigSSPNG">
								<SelectedItemStyle CssClass="gridFieldSelected"></SelectedItemStyle>
								<ItemStyle Height="15px" CssClass="gridField"></ItemStyle>
								<HeaderStyle Font-Bold="True" HorizontalAlign="Center" Height="40px" Width="2%" CssClass="gridHeading"></HeaderStyle>
								<Columns>
									<asp:EditCommandColumn ButtonType="LinkButton" UpdateText="&lt;IMG SRC='images/butt-update.gif' border=0  title='Update' &gt;"
										CancelText="&lt;IMG SRC='images/butt-cancel.gif' border=0 title='Cancel'&gt;" EditText="&lt;IMG SRC='images/butt-edit.gif' border=0  title='Edit' &gt;">
										<HeaderStyle Width="2%" CssClass="gridHeading"></HeaderStyle>
									</asp:EditCommandColumn>
									<asp:TemplateColumn HeaderText="Master">
										<ItemStyle HorizontalAlign="Center" Height="25px" Width="70px"></ItemStyle>
										<ItemTemplate>
											<asp:CheckBox Runat="server" ID="chkMaster" Enabled="False"></asp:CheckBox>
										</ItemTemplate>
										<EditItemTemplate>
											<asp:CheckBox Runat="server" ID="chkMasterEdit" OnCheckedChanged="CheckMaster" AutoPostBack="True"></asp:CheckBox>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Account">
										<ItemStyle HorizontalAlign="Center"></ItemStyle>
										<ItemTemplate>
											<asp:Label ID="lblAccount" Text='<%#DataBinder.Eval(Container.DataItem,"userid")%>' Runat=server>
											</asp:Label>
										</ItemTemplate>
										<EditItemTemplate>
											<asp:textbox ID="lblAccountEdit" Text='<%#DataBinder.Eval(Container.DataItem,"userid")%>' Runat=server Enabled=False CssClass="textField">
											</asp:textbox>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Username">
										<ItemStyle HorizontalAlign="Center"></ItemStyle>
										<ItemTemplate>
											<asp:Label ID="lblUserName" Text='<%#DataBinder.Eval(Container.DataItem,"user_name")%>' Runat=server>
											</asp:Label>
										</ItemTemplate>
										<EditItemTemplate>
											<asp:textbox ID="txtUserNameEdit" Text='<%#DataBinder.Eval(Container.DataItem,"user_name")%>' Runat=server Enabled=False CssClass="textField">
											</asp:textbox>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Enterprise Copies">
										<ItemStyle HorizontalAlign="Center"></ItemStyle>
										<ItemTemplate>
											<asp:Label ID="lblEnterpriseCopies" Text='<%#DataBinder.Eval(Container.DataItem,"EnterpriseCopies")%>' Runat=server>test</asp:Label>
										</ItemTemplate>
										<EditItemTemplate>
											<cc1:mstextbox id="txtEnterpriseCopiesEdit" Runat="server" NumberScale="0" NumberPrecision="4" NumberMinValue="1" NumberMaxValue="9" MaxLength="1" TextMaskType="msNumeric" Width="100px" CssClass="textFieldRightAlign" Text='<%#DataBinder.Eval(Container.DataItem,"EnterpriseCopies")%>' onpaste="AfterPasteNonNumber(this)">
											</cc1:mstextbox>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Customer Copies">
										<ItemStyle HorizontalAlign="Center"></ItemStyle>
										<ItemTemplate>
											<asp:Label ID="lblCustomerCopies" Runat=server Text='<%#DataBinder.Eval(Container.DataItem,"CustomerCopies")%>' >
											</asp:Label>
										</ItemTemplate>
										<EditItemTemplate>
											<cc1:mstextbox id="txtCustomerCopiesEdit" Runat="server" NumberScale="0" NumberPrecision="4" NumberMinValue="1" NumberMaxValue="9" MaxLength="1" TextMaskType="msNumeric" Width="100px" CssClass="textFieldRightAlign" Text='<%#DataBinder.Eval(Container.DataItem,"CustomerCopies")%>' onpaste="AfterPasteNonNumber(this)">
											</cc1:mstextbox>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Sender Name">
										<ItemStyle HorizontalAlign="Center"></ItemStyle>
										<ItemTemplate>
											<asp:DropDownList id="ddlSenderName" runat="server" Width="100px" Enabled="False"></asp:DropDownList>
										</ItemTemplate>
										<EditItemTemplate>
											<asp:DropDownList id="ddlSenderNameEdit" runat="server" Width="100px"></asp:DropDownList>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Sender Address">
										<ItemStyle HorizontalAlign="Center"></ItemStyle>
										<ItemTemplate>
											<asp:Label ID="Label2" Runat=server Text='<%#DataBinder.Eval(Container.DataItem,"address1")%>'>
											</asp:Label>
										</ItemTemplate>
										<EditItemTemplate>
											<asp:textbox id="txtAddress" runat="server" CssClass="textField" Text='<%#DataBinder.Eval(Container.DataItem,"address1")%>' Enabled =False>
											</asp:textbox>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Postal Code">
										<ItemStyle HorizontalAlign="Center"></ItemStyle>
										<ItemTemplate>
											<asp:Label ID="Label4" Runat=server Text='<%#DataBinder.Eval(Container.DataItem,"zipcode")%>'>
											</asp:Label>
										</ItemTemplate>
										<EditItemTemplate>
											<asp:textbox id="txtZipcode" runat="server" CssClass="textField" Text='<%#DataBinder.Eval(Container.DataItem,"zipcode")%>' Enabled =False>
											</asp:textbox>
										</EditItemTemplate>
									</asp:TemplateColumn>
								</Columns>
							</asp:datagrid></td>
					</tr>
				</TABLE>
			</div>
			<INPUT type="hidden" name="hdnRefresh">
			<asp:button style="Z-INDEX: 105; POSITION: absolute; DISPLAY: none; TOP: 1064px; LEFT: 672px"
				id="btnHidReport" runat="server" Text=".."></asp:button></form>
	</body>
</HTML>
