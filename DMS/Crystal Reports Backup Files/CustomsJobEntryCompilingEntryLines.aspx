<%@ Page language="c#" Codebehind="CustomsJobEntryCompilingEntryLines.aspx.cs" AutoEventWireup="false" Inherits="TIES.WebUI.CustomsJobEntryCompilingEntryLines" %>
<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<%@ Import Namespace="com.common.util" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>CustomsJobEntryCompilingEntryLines</title>
		<meta name="vs_snapToGrid" content="True">
		<meta name="vs_showGrid" content="True">
		<LINK rel="stylesheet" type="text/css" href="css/Styles.css" name="stylesheet">
		<meta name="GENERATOR" content="Microsoft Visual Studio 7.0">
		<meta name="CODE_LANGUAGE" content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<!--#INCLUDE FILE="msFormValidations.inc"-->
		<script language="javascript" src="Scripts/settingScrollPosition.js"></script>
		<script language="javascript" src="Scripts/JScript_TextControl.js"></script>
		<script language="javascript" type="text/javascript">
			function funcDisBack(){
				history.go(+1);
			}

			function RemoveBadNonNumber(strTemp, obj) {
				strTemp = strTemp.replace(/[^0-9]/g, ''); //replace non-numeric
				obj.value = strTemp.replace(/\s/g, ''); // replace space
			}
			function AfterPasteNonNumber(obj) {
				setTimeout(function () {
					RemoveBadNonNumber(obj.value, obj);
				}, 1); //or 4
			}
			
			function RemoveBadPaseNumberwhithDot(strTemp, obj) {
				strTemp = strTemp.replace(/[^.0-9]/g, ''); //replace non-numeric
				obj.value = strTemp.replace(/\s/g, ''); // replace space
			}
			
			function AfterPasteNumberwhithDot(obj) {
						setTimeout(function () {
							RemoveBadPaseNumberwhithDot(obj.value, obj);
						}, 1); //or 4
					}
			
			
			 function callback()
			{
					var btn = document.getElementById('btnClientEvent');
					if(btn != null)
					{					
						btn.click();
					}else{
						alert('error call code behind');
					}				        
				
			}
			function validateCode(key) { 
            var keycode = (key.which) ? key.which : key.keyCode; 
            if (!(keycode == 8 || keycode == 46) && (keycode < 48 || keycode > 57)) {
                return false;
            }
             
			}
			
			function isDecimalValidKey(textboxIn, evt)
			{
				var charCode = (evt.which) ? evt.which : event.keyCode;    

				if (charCode > 31 && (charCode < 45 || charCode == 47 || charCode > 57))
				{
				return false;
				}     

				if(charCode == 46 && textboxIn.value.indexOf(".") != -1)
				{
					return false;
				}   
				return true;
			} 
		</script>
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="CustomsJobEntryCompiling" method="post" runat="server">
			<div style="Z-INDEX: 102; POSITION: absolute; WIDTH: 100%; HEIGHT: 300px; TOP: 48px; LEFT: 18px; RIGTH: 0px"
				id="divMain" MS_POSITIONING="GridLayout" runat="server">
				<TABLE style="Z-INDEX: 101; POSITION: absolute; TOP: 8px; LEFT: 0px" id="MainTable" border="0"
					width="100%" runat="server">
					<tbody>
						<tr>
							<td align="left"><asp:label id="Label1" runat="server" Width="100%" Height="27px" CssClass="mainTitleSize">Customs Entry Compiling - Entry Lines</asp:label></td>
						</tr>
						<tr>
							<td><asp:button id="btnClose" runat="server" CssClass="queryButton" Text="Close" CausesValidation="False"></asp:button>
								<asp:button style="Z-INDEX: 0;DISPLAY: none" id="btnExecTariffCodeFooter" runat="server" Width="130px" Text="btnExecTariffCodeFooter"
									CausesValidation="False"></asp:button>
								<asp:button style="Z-INDEX: 0;DISPLAY: none" id="btnExecTariffCodeEdit" runat="server" Width="130px" Text="btnExecTariffCodeEdit"
									CausesValidation="False"></asp:button></td>
						</tr>
						<tr>
							<td><asp:label style="Z-INDEX: 0" id="lblError" runat="server" ForeColor="Red" Font-Size="Smaller"></asp:label><STRONG class="gridHeading"><FONT style="WIDTH: 95%" size="2">Entry 
										Lines</FONT></STRONG>
							</td>
						</tr>
						<tr>
							<td><asp:datagrid style="Z-INDEX: 0" id="gvJobEntryLines" tabIndex="42" runat="server" Width="95%"
									AutoGenerateColumns="False" DataKeyField="SeqNo" AlternatingItemStyle-CssClass="gridField"
									ItemStyle-CssClass="gridField" HeaderStyle-CssClass="gridHeading" CellPadding="0" CellSpacing="1"
									BorderWidth="0px" FooterStyle-CssClass="gridHeading" PageSize="6" ShowFooter="True">
									<FooterStyle CssClass="gridHeading"></FooterStyle>
									<AlternatingItemStyle CssClass="gridField"></AlternatingItemStyle>
									<ItemStyle CssClass="gridField"></ItemStyle>
									<HeaderStyle CssClass="gridHeading"></HeaderStyle>
									<Columns>
										<asp:TemplateColumn>
											<HeaderStyle Width="10%"></HeaderStyle>
											<ItemStyle HorizontalAlign="Center"></ItemStyle>
											<ItemTemplate>
												<asp:LinkButton id="btnEditItem" Runat="server" Text="<img src='images/butt-edit.gif' alt='edit' border=0>"
													CommandName="EDIT_ITEM" />
												<asp:LinkButton id="btnDeleteItem" Runat="server" Text="<img src='images/butt-delete.gif' alt='delete' border=0>"
													CommandName="DELETE_ITEM" />
											</ItemTemplate>
											<FooterStyle HorizontalAlign="Center"></FooterStyle>
											<FooterTemplate>
												<asp:LinkButton id="btnAddItem" Runat="server" Text="<img src='images/butt-add.gif' alt='add' border=0>"
													CommandName="ADD_ITEM" />
											</FooterTemplate>
											<EditItemTemplate>
												<asp:LinkButton id="btnSaveItem" Runat="server" Text="<img src='images/butt-update.gif' alt='save' border=0>"
													CommandName="SAVE_ITEM" />
												<asp:LinkButton id="btnCancelItem" Runat="server" Text="<img src='images/butt-red.gif' alt='cancel' border=0>"
													CommandName="CANCEL_ITEM" />
											</EditItemTemplate>
										</asp:TemplateColumn>
										<asp:TemplateColumn HeaderText="Tariff">
											<HeaderStyle HorizontalAlign="Left" Width="7%"></HeaderStyle>
											<ItemStyle HorizontalAlign="Left"></ItemStyle>
											<ItemTemplate>
												<%# DataBinder.Eval(Container.DataItem,"tariff_code") %>
											</ItemTemplate>
											<FooterTemplate>
												<asp:textbox style="Z-INDEX: 0" tabIndex="1" runat="server" CssClass="textField" id="txtAddtariff_code"
													Width="98%" MaxLength="10" onkeypress="return validateCode(event)" onpaste="AfterPasteNumberwhithDot(this)"
													onblur="if( this.value !=''){document.getElementById('btnExecTariffCodeFooter').click();}"></asp:textbox>
											</FooterTemplate>
											<EditItemTemplate>
												<asp:textbox style="Z-INDEX: 0" tabIndex="12" runat="server" CssClass="textField" 
													id="txtEdittariff_code" Width = "98%" MaxLength = 10 onkeypress="return validateCode(event)" 
													onpaste="AfterPasteNumberwhithDot(this)" Text='<%#DataBinder.Eval(Container.DataItem,"tariff_code" )%>'
													onblur="if( this.value !=''){document.getElementById('btnExecTariffCodeEdit').click();}">
												</asp:textbox>
											</EditItemTemplate>
										</asp:TemplateColumn>
										<asp:TemplateColumn HeaderText="Model">
											<HeaderStyle HorizontalAlign="Center" Width="4%"></HeaderStyle>
											<ItemStyle HorizontalAlign="Center"></ItemStyle>
											<ItemTemplate>
												<%# DataBinder.Eval(Container.DataItem,"ModelOfDeclaration") %>
											</ItemTemplate>
											<FooterTemplate>
												<asp:dropdownlist style="Z-INDEX: 0" tabIndex="2" runat="server" AutoPostBack="True" id="ddlAddModelOfDeclaration"></asp:dropdownlist>
											</FooterTemplate>
											<EditItemTemplate>
												<asp:dropdownlist style="Z-INDEX: 0" tabIndex="12" runat="server" AutoPostBack="True" id="ddlEditModelOfDeclaration"></asp:dropdownlist>
											</EditItemTemplate>
										</asp:TemplateColumn>
										<asp:TemplateColumn HeaderText="Declaration">
											<HeaderStyle HorizontalAlign="Left" Width="6%"></HeaderStyle>
											<ItemStyle HorizontalAlign="Left"></ItemStyle>
											<ItemTemplate>
												<asp:Label ID="lblCustomsProcedureCode" Runat=server Text='<%#DataBinder.Eval(Container.DataItem,"CustomsProcedureCode")%>' >
												</asp:Label>
											</ItemTemplate>
											<FooterTemplate>
												<asp:dropdownlist style="Z-INDEX: 0" tabIndex="3" runat="server" AutoPostBack="True" id="ddlAddCustomsProcedureCode"></asp:dropdownlist>
											</FooterTemplate>
											<EditItemTemplate>
												<asp:dropdownlist style="Z-INDEX: 0" tabIndex="13" runat="server" AutoPostBack="True" id="ddlEditCustomsProcedureCode"></asp:dropdownlist>
											</EditItemTemplate>
										</asp:TemplateColumn>
										<asp:TemplateColumn HeaderText="Description">
											<HeaderStyle HorizontalAlign="Left" Width="13%"></HeaderStyle>
											<ItemStyle HorizontalAlign="Left"></ItemStyle>
											<ItemTemplate>
												<asp:Label ID="lblDescription1" Runat=server Text='<%#DataBinder.Eval(Container.DataItem,"Description1")%>' >
												</asp:Label>
											</ItemTemplate>
											<FooterTemplate>
												<asp:textbox style="Z-INDEX: 0" tabIndex="4" runat="server" CssClass="textField" id="txtAddDescription1"
													Width="130px" MaxLength="100"></asp:textbox>
											</FooterTemplate>
											<EditItemTemplate>
												<asp:textbox style="Z-INDEX: 0" tabIndex="14" runat="server" CssClass="textField" id="txtEditDescription1" Width = "135px" Text='<%#DataBinder.Eval(Container.DataItem,"Description1")%>'>
												</asp:textbox>
											</EditItemTemplate>
										</asp:TemplateColumn>
										<asp:TemplateColumn HeaderText="Description(additional)">
											<HeaderStyle HorizontalAlign="Left" Width="14%"></HeaderStyle>
											<ItemStyle HorizontalAlign="Left"></ItemStyle>
											<ItemTemplate>
												<%# DataBinder.Eval(Container.DataItem,"Description2") %>
											</ItemTemplate>
											<FooterTemplate>
												<asp:textbox style="Z-INDEX: 0" tabIndex="5" runat="server" CssClass="textField" id="txtAddDescription2"
													Width="135px" MaxLength="100"></asp:textbox>
											</FooterTemplate>
											<EditItemTemplate>
												<asp:textbox style="Z-INDEX: 0" tabIndex="15" runat="server" CssClass="textField" id="txtEditDescription2" Width = "135px" MaxLength = 100 Text='<%#DataBinder.Eval(Container.DataItem,"Description2")%>'>
												</asp:textbox>
											</EditItemTemplate>
										</asp:TemplateColumn>
										<asp:TemplateColumn HeaderText="Origin">
											<HeaderStyle HorizontalAlign="Center" Width="4%"></HeaderStyle>
											<ItemStyle HorizontalAlign="Center"></ItemStyle>
											<ItemTemplate>
												<%# DataBinder.Eval(Container.DataItem,"OriginCountry") %>
											</ItemTemplate>
											<FooterTemplate>
												<asp:dropdownlist style="Z-INDEX: 0" tabIndex="6" runat="server" AutoPostBack="True" id="ddlAddOriginCountry"
													OnSelectedIndexChanged="ddlAddOriginCountry_SelectedIndexChanged" Width="98%"></asp:dropdownlist>
											</FooterTemplate>
											<EditItemTemplate>
												<asp:dropdownlist style="Z-INDEX: 0" tabIndex="16" runat="server" AutoPostBack="True" id="ddlEditOriginCountry"
													OnSelectedIndexChanged="ddlEditOriginCountry_SelectedIndexChanged" Width="98%"></asp:dropdownlist>
											</EditItemTemplate>
										</asp:TemplateColumn>
										<asp:TemplateColumn HeaderText="Currency">
											<HeaderStyle HorizontalAlign="Center" Width="6%"></HeaderStyle>
											<ItemStyle HorizontalAlign="Center"></ItemStyle>
											<ItemTemplate>
												<%# DataBinder.Eval(Container.DataItem,"CurrencyCode") %>
											</ItemTemplate>
											<FooterTemplate>
												<asp:dropdownlist style="Z-INDEX: 0" tabIndex="7" runat="server" AutoPostBack="True" Width="98%" id="ddlAddCurrencyCode"></asp:dropdownlist>
											</FooterTemplate>
											<EditItemTemplate>
												<asp:dropdownlist style="Z-INDEX: 0" tabIndex="17" runat="server" AutoPostBack="True" Width="98%" id="ddlEditCurrencyCode"></asp:dropdownlist>
											</EditItemTemplate>
										</asp:TemplateColumn>
										<asp:TemplateColumn HeaderText="Tariff Qty">
											<HeaderStyle HorizontalAlign="Right" Width="9%"></HeaderStyle>
											<ItemStyle HorizontalAlign="Right"></ItemStyle>
											<ItemTemplate>
												<%# DataBinder.Eval(Container.DataItem,"Tariff_Qty","{0:###,##0.000}") %>
											</ItemTemplate>
											<FooterTemplate>
												<cc1:mstextbox CssClass="gridTextBoxNumber" ID="txtAddTariff_Qty" onpaste="AfterPasteNonNumber(this)"
													Runat="server" Enabled="True" TextMaskType="msNumeric" MaxLength="11" NumberMaxValue="9999999"
													NumberMinValue="0" NumberPrecision="11" NumberScale="3" Width="98%" tabIndex="8"></cc1:mstextbox>
											</FooterTemplate>
											<EditItemTemplate>
												<cc1:mstextbox CssClass="gridTextBoxNumber" ID="txtEditTariff_Qty" onpaste="AfterPasteNonNumber(this)" Runat="server" 
													Enabled="True" TextMaskType="msNumeric" MaxLength="11" NumberMaxValue="9999999" NumberMinValue="0" NumberPrecision="11" 
													NumberScale="3" Width="98%" tabIndex="18" Text='<%#DataBinder.Eval(Container.DataItem,"Tariff_Qty","{0:###,##0.000}")%>'>
												</cc1:mstextbox>
											</EditItemTemplate>
										</asp:TemplateColumn>
										<asp:TemplateColumn HeaderText="Supp. Qty">
											<HeaderStyle HorizontalAlign="Right" Width="8%"></HeaderStyle>
											<ItemStyle HorizontalAlign="Right"></ItemStyle>
											<ItemTemplate>
												<%# DataBinder.Eval(Container.DataItem,"Supplemental_Qty","{0:###,##0.000}") %>
											</ItemTemplate>
											<FooterTemplate>
												<cc1:mstextbox style="Z-INDEX: 0" tabIndex="9" runat="server" CssClass="gridTextBoxNumber" onpaste="AfterPasteNonNumber(this)"
													TextMaskType="msNumeric" MaxLength="11" NumberMaxValue="9999999" NumberMinValue="0" NumberPrecision="11"
													NumberScale="3" id="txtAddSupplemental_Qty" Width="98%"></cc1:mstextbox>
											</FooterTemplate>
											<EditItemTemplate>
												<cc1:mstextbox style="Z-INDEX: 0" tabIndex="19" runat="server" CssClass="gridTextBoxNumber" onpaste="AfterPasteNonNumber(this)" TextMaskType="msNumeric" MaxLength="11" NumberMaxValue="9999999" NumberMinValue="0" NumberPrecision="11" NumberScale="3" id="txtEditSupplemental_Qty" Width="98%" Text='<%#DataBinder.Eval(Container.DataItem,"Supplemental_Qty","{0:###,##0.000}")%>'>
												</cc1:mstextbox>
											</EditItemTemplate>
										</asp:TemplateColumn>
										<asp:TemplateColumn HeaderText="UOM">
											<HeaderStyle HorizontalAlign="Center" Width="4%"></HeaderStyle>
											<ItemStyle HorizontalAlign="Center"></ItemStyle>
											<ItemTemplate>
												<%# DataBinder.Eval(Container.DataItem,"UnitsOfMeasure") %>
											</ItemTemplate>
											<FooterTemplate>
												<asp:dropdownlist style="Z-INDEX: 0" tabIndex="10" runat="server" AutoPostBack="True" id="ddlAddUnitsOfMeasure"></asp:dropdownlist>
											</FooterTemplate>
											<EditItemTemplate>
												<asp:dropdownlist style="Z-INDEX: 0" tabIndex="20" runat="server" AutoPostBack="True" id="ddlEditUnitsOfMeasure"></asp:dropdownlist>
											</EditItemTemplate>
										</asp:TemplateColumn>
										<asp:TemplateColumn HeaderText="Amount">
											<HeaderStyle HorizontalAlign="Right" Width="8%"></HeaderStyle>
											<ItemStyle HorizontalAlign="Right"></ItemStyle>
											<ItemTemplate>
												<%# DataBinder.Eval(Container.DataItem,"Amount","{0:###,##0.00}") %>
											</ItemTemplate>
											<FooterTemplate>
												<cc1:mstextbox style="Z-INDEX: 0" tabIndex="11" runat="server" CssClass="gridTextBoxNumber" onpaste="AfterPasteNonNumber(this)"
													TextMaskType="msNumeric" id="txtAddAmount" MaxLength="8" NumberMaxValue="999999999" NumberMinValue="0"
													NumberPrecision="10" NumberScale="2" Width="98%"></cc1:mstextbox>
											</FooterTemplate>
											<EditItemTemplate>
												<cc1:mstextbox style="Z-INDEX: 0" Width = "98%" tabIndex="21" runat="server" CssClass="gridTextBoxNumber" onpaste="AfterPasteNonNumber(this)" TextMaskType="msNumeric" id="txtEditAmount" MaxLength="10" NumberMaxValue="999999999" NumberMinValue="0" NumberPrecision="10" NumberScale="2" Text='<%#DataBinder.Eval(Container.DataItem,"Amount","{0:###,##0.00}")%>'>
												</cc1:mstextbox>
											</EditItemTemplate>
										</asp:TemplateColumn>
										<asp:TemplateColumn HeaderText="Rate">
											<HeaderStyle HorizontalAlign="Right" Width="6%"></HeaderStyle>
											<ItemStyle HorizontalAlign="Right"></ItemStyle>
											<ItemTemplate>
												<%# DataBinder.Eval(Container.DataItem,"ExchangeRate") %>
											</ItemTemplate>
											<EditItemTemplate>
												<asp:textbox id="txtExchange_Rate" runat="server" Enabled=False CssClass="gridTextBoxNumber" MaxLength="20" Width="95%" Text='<%#DataBinder.Eval(Container.DataItem,"ExchangeRate")%>'>
												</asp:textbox>
											</EditItemTemplate>
											<FooterTemplate>
												&nbsp;
											</FooterTemplate>
										</asp:TemplateColumn>
										<asp:TemplateColumn HeaderText="Valuation">
											<HeaderStyle HorizontalAlign="Right"></HeaderStyle>
											<ItemStyle HorizontalAlign="Right"></ItemStyle>
											<ItemTemplate>
												<%# DataBinder.Eval(Container.DataItem,"Valuation","{0:###,##0.00}") %>
											</ItemTemplate>
											<EditItemTemplate>
												<cc1:mstextbox CssClass="gridTextBoxNumber" ID="txtValuation" onpaste="AfterPasteNonNumber(this)" Runat="server" Enabled="False" TextMaskType="msNumeric" MaxLength="20" NumberMaxValue="9999" NumberMinValue="0" NumberPrecision="6" NumberScale="2" Text='<%#DataBinder.Eval(Container.DataItem,"Valuation","{0:###,##0.00}")%>'>
												</cc1:mstextbox>
											</EditItemTemplate>
											<FooterTemplate>
												&nbsp;
											</FooterTemplate>
										</asp:TemplateColumn>
									</Columns>
									<PagerStyle NextPageText="Next" PrevPageText="Previous" HorizontalAlign="Left" CssClass="normalText"></PagerStyle>
								</asp:datagrid></td>
						</tr>
					</tbody>
				</TABLE>
			</div>
		</form>
	</body>
</HTML>
