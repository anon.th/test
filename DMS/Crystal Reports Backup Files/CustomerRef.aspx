<%@ Page language="c#" Codebehind="CustomerRef.aspx.cs" AutoEventWireup="false" Inherits="com.ties.CustomerRef" %>
<%@ Import Namespace="com.common.util" %>
<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Customer</title>
		<meta content="False" name="vs_showGrid">
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../css/Styles.css" type="text/css" rel="Stylesheet">
		<!--#INCLUDE FILE="../msFormValidations.inc"-->
		<!--script language="javascript" src="../Scripts/settingScrollPosition.js"></script-->
		<script language="javascript" type="text/javascript">		
		function DoAction(serveraction){
			var confirmed = true;
            var isSubmitted = true;
            var URL = "";
            var ErrMsg = "";
            switch (serveraction){
                case "SAVE":
                   /* if (document.forms[0].elements("lstExpenseName").value == '')ErrMsg = "* Expense Name!\n";
                    if (isDate(document.forms[0].elements("txtPaymentDate").value) == false)ErrMsg += "* ��Ǩ�ͺ Payment Date!\n";
                    if (IsNumeric(document.forms[0].elements("txtAmount").value) == false)ErrMsg += "* ��Ǩ�ͺ Amount!\n";
                    
                    if (ErrMsg != ""){
                        confirmed = false;
                        alert("��س��кآ�����\n"+ErrMsg);
                    } else {
                        confirmed = confirm("��سҡ� OK �����׹�ѹ��úѹ�֡������");
                    }*/
                    //confirmed = confirm("��سҡ� OK �����׹�ѹ��úѹ�֡������");
                     isSubmitted = true;
                    break;
                case "INSERT":
                    isSubmitted = true;
                    URL = "CustomerRef.aspx?ServerAction=INSERT";
                    break;
                case "DELETE":
                    confirmed = confirm("��سҡ� OK �����׹�ѹ���ź������");
                    break;
            }
            if (URL != ""){
                window.location.href = URL; 
            } else {
                if (isSubmitted && confirmed){
                    if (ErrMsg != ""){
                        alert(ErrMsg);
                    } else {
                        document.forms[0].elements("ServerAction").value = serveraction;
                        document.forms[0].submit(); 
                    }  
                }
            }
		 }
		</script>
	</HEAD>
	<body onkeypress="microsoftKeyPress()" onclick="GetScrollPosition();" onload="SetScrollPosition();"
		onunload="window.top.displayBanner.fnCloseAll(0);" MS_POSITIONING="ViewLayout">
		<form id="frmCustomerRef" method="post" runat="server">
			<INPUT id="ServerAction" type="hidden" name="ServerAction"> <input 
id=CustomerCode type=hidden value="<%=CustomerCode%>" name=CustomerCode>
			<table cellSpacing="0" cellPadding="0" width="100%" border="0">
				<tr>
					<td><asp:label id="lblMainTitle" runat="server" CssClass="mainTitleSize">Consignee</asp:label></td>
				</tr>
				<tr>
					<td>
						<table class="TextBody" cellSpacing="0" cellPadding="3" width="100%" border="0">
							<tr>
								<td colSpan="2">&nbsp;</td>
							</tr>
							<tr class="tablepagingitem" style="HEIGHT: 15px">
								<td style="WIDTH:25%"></td>
								<td style="WIDTH:75%" align="left">
									<a onclick="javascript:DoAction('INSERT','');"><input class="queryButton" id="btnInsert" style="WIDTH: 60px; HEIGHT: 20px" type="button"
											value="Insert" name="btnAdd"></a> <a onclick="javascript:DoAction('SAVE','');">
										<input class="queryButton" id="btnSave" style="WIDTH: 60px; HEIGHT: 20px" type="button"
											value="Save" name="btnSave"></a> <a onclick="javascript:DoAction('DELETE','');">
										<input class="queryButton" id="btnDelete" style="WIDTH: 60px; HEIGHT: 20px" type="button"
											value="Delete" name="btnDelete"></a>
								</td>
							</tr>
							<tr>
								<td colSpan="2"><asp:label id="ErrorMsg" runat="server" CssClass="errorMsgColor" Width="643px" Height="3px"></asp:label><br>
									<asp:label id="ErrorMsg1" runat="server" CssClass="errorMsgColor" Width="643px" Height="3px"></asp:label></td>
							</tr>
							<tr>
								<td colspan="2">&nbsp;</td>
							</tr>
							<tr>
								<td align="right">Consingnee&nbsp;Code&nbsp;:<FONT color="#ff0066">*</FONT></td>
								<td align="left"><asp:textbox id="txtCustomerCode" runat="server" Width="250px" MaxLength="100"></asp:textbox></td>
							</tr>
							<tr>
								<td align="right">Consignee&nbsp;Name&nbsp;:<FONT color="#ff0066">*</FONT>&nbsp;</td>
								<td align="left"><asp:textbox id="txtCustomerName" runat="server" Width="344px" MaxLength="100"></asp:textbox></td>
							</tr>
							<tr>
								<td style="HEIGHT: 53px" vAlign="top" align="right">Address1&nbsp;:<FONT color="#ff0066">*</FONT>&nbsp;</td>
								<td align="left"><asp:textbox id="txtAddress1" runat="server" Width="550px" Height="45px" MaxLength="100" TextMode="MultiLine"></asp:textbox></td>
							</tr>
							<tr>
								<td style="HEIGHT: 53px" vAlign="top" align="right">Address2&nbsp;:<FONT color="#ff0000">*</FONT>&nbsp;</td>
								<td style="HEIGHT: 53px" align="left"><asp:textbox id="txtAddress2" runat="server" Width="550px" Height="45px" MaxLength="100" TextMode="MultiLine"></asp:textbox></td>
							</tr>
							<tr>
								<td align="right">Zipcode&nbsp;:<FONT color="#ff0066">*</FONT>&nbsp;</td>
								<td align="left"><cc1:mstextbox id="txtZipcode" tabIndex="1" runat="server" Width="120px" MaxLength="5" NumberScale="0"
										AutoPostBack="True" TextMaskType="msNumeric" NumberMaxValue="99999" NumberPrecision="5"></cc1:mstextbox>
									&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Province&nbsp;:&nbsp;
									<asp:textbox id="txtProvince" runat="server" CssClass="txtReadOnly" Width="200px" MaxLength="200"
										ReadOnly="True"></asp:textbox>
								</td>
							</tr>
							<tr>
								<td align="right">Telephone No.&nbsp;:<FONT color="#ff0066">*</FONT>&nbsp;</td>
								<td align="left"><asp:textbox id="txtTelephone" runat="server" Width="190px" MaxLength="20"></asp:textbox></td>
							</tr>
							<tr>
								<td align="right">Contact Person&nbsp;:<FONT color="#ff0033">*&nbsp;</FONT></td>
								<td align="left"><asp:textbox id="txtContactPerson" runat="server" Width="340px" MaxLength="100"></asp:textbox></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
