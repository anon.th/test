<%@ Page language="c#" Codebehind="CustProfileFlatTemp.aspx.cs" AutoEventWireup="false" Inherits="TIES.WebUI.CustProfileFlatTemp" %>
<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<%@ Import Namespace="com.common.util" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>CustProfileFlatTemp</title>
		<LINK href="css/Styles.css" type="text/css" rel="stylesheet" name="stylesheet">
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<!--#INCLUDE FILE="msFormValidations.inc"-->
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="CustProfileFlatTemp" method="post" runat="server">
			<TABLE id="CustomerInfoTable" style="Z-INDEX: 112; WIDTH: 750px" width="750" border="0" runat="server">
				<TR width="100%">
					<TD width="100%">
						<TABLE id="tblCustInfo" width="100%" align="left" border="0">
							<TR height="27">
								<td colSpan="1"></td>
								<TD class="tableHeading" colSpan="19">Customer Information</TD>
							</TR>
							<TR height="27">
								<TD width="5%"></TD>
								<TD width="5%"></TD>
								<TD width="5%"></TD>
								<TD width="5%"></TD>
								<TD width="5%"></TD>
								<TD width="5%"></TD>
								<TD width="5%"></TD>
								<TD width="5%"></TD>
								<TD width="5%"></TD>
								<TD width="5%"></TD>
								<TD width="5%"></TD>
								<TD width="5%"></TD>
								<TD width="5%"></TD>
								<TD width="5%"></TD>
								<TD width="5%"></TD>
								<TD width="5%"></TD>
								<TD width="5%"></TD>
								<TD width="5%"></TD>
								<TD width="5%"></TD>
								<TD width="5%"></TD>
							</TR>
							<TR height="27">
								<TD style="HEIGHT: 26px" colSpan="1">&nbsp;</TD>
								<TD colSpan="19"><asp:button id="btnViewAll" runat="server" Width="62px" CssClass="queryButton" Height="21px" CausesValidation="False" Text="View All"></asp:button><asp:button id="btnRefInsert" runat="server" Width="62px" CssClass="queryButton" Height="21px" CausesValidation="False" Text="Insert"></asp:button><asp:button id="btnRefSave" runat="server" Width="66px" CssClass="queryButton" Height="22px" Text="Save"></asp:button><asp:button id="btnRefDelete" runat="server" Width="62px" CssClass="queryButton" Height="21px" CausesValidation="False" Text="Delete" Visible="True"></asp:button>&nbsp;
									<asp:button id="btnFirstRef" runat="server" Width="24px" CssClass="queryButton" CausesValidation="False" Text="|<"></asp:button><asp:button id="btnPreviousRef" runat="server" Width="24px" CssClass="queryButton" CausesValidation="False" Text="<"></asp:button><asp:textbox id="txtRefJumpCount" runat="server" Width="24px"></asp:textbox><asp:button id="btnNextRef" runat="server" Width="24px" CssClass="queryButton" CausesValidation="False" Text=">"></asp:button><asp:button id="btnLastRef" runat="server" Width="24px" CssClass="queryButton" CausesValidation="False" Text=">|"></asp:button></TD>
							</TR>
							<TR height="27">
								<TD align="right" colSpan="1"><asp:requiredfieldvalidator id="validateAccNo" ControlToValidate="txtAccNo" Runat="server" Width="100%">&nbsp;*&nbsp;</asp:requiredfieldvalidator></TD>
								<TD colSpan="3"><asp:label id="lblAccNo" runat="server" Width="100%" CssClass="tableLabel" Height="22px">Account No.</asp:label></TD>
								<TD colSpan="3"><asp:textbox id="txtAccNo" runat="server" Width="100%" CssClass="textField" AutoPostBack="True"></asp:textbox></TD>
								<TD colSpan="1"></TD>
								<TD colSpan="4"><asp:label id="lblRefNo" runat="server" Width="100%" CssClass="tableLabel" Height="22px">Customer Ref No</asp:label></TD>
								<TD colSpan="3"><asp:textbox id="txtRefNo" tabIndex="1" runat="server" Width="100%" CssClass="textField" Enabled="False"></asp:textbox></TD>
								<TD colSpan="2"><asp:label id="lblStatus" runat="server" Width="100%" CssClass="tableLabel" Height="22px">Status</asp:label></TD>
								<TD colSpan="3"><asp:textbox id="txtStatus" tabIndex="1" runat="server" Width="100%" CssClass="textField" Enabled="False"></asp:textbox></TD>
							</TR>
							<TR height="27">
								<TD colSpan="20">&nbsp;</TD>
							</TR>
							<tr height="27">
								<TD colSpan="1">&nbsp;</TD>
								<TD colSpan="3"><asp:label id="lblCustomerName" runat="server" Width="100%" CssClass="tableLabel" Height="22px">Customer Name</asp:label></TD>
								<TD colSpan="6"><asp:textbox id="txtCustomerName" runat="server" Width="100%" CssClass="textField"></asp:textbox></TD>
								<TD align="right" colSpan="1"><asp:requiredfieldvalidator id="Requiredfieldvalidator2" ControlToValidate="txtContactPerson" Runat="server" Width="100%">&nbsp;*&nbsp;</asp:requiredfieldvalidator></TD>
								<TD colSpan="3"><asp:label id="lblContactPerson" runat="server" Width="100%" CssClass="tableLabel" Height="22px">Contact Person</asp:label></TD>
								<TD colSpan="6"><asp:textbox id="txtContactPerson" runat="server" Width="100%" CssClass="textField"></asp:textbox></TD>
							</tr>
							<tr height="27">
								<TD colSpan="1">&nbsp;</TD>
								<TD colSpan="3"><asp:label id="lblAddress1" runat="server" Width="100%" CssClass="tableLabel" Height="22px">Address 1</asp:label></TD>
								<TD colSpan="6"><asp:textbox id="txtAddress1" runat="server" Width="100%" CssClass="textField"></asp:textbox></TD>
								<TD colSpan="1">&nbsp;</TD>
								<TD colSpan="3"><asp:label id="lblTelphone" runat="server" Width="100%" CssClass="tableLabel" Height="22px">Telephone</asp:label></TD>
								<TD colSpan="3"><asp:textbox id="txtTelephone" runat="server" Width="100%" CssClass="textField"></asp:textbox></TD>
								<TD colSpan="3">&nbsp;</TD>
							</tr>
							<tr height="27">
								<TD colSpan="1">&nbsp;</TD>
								<TD colSpan="3"><asp:label id="lblAddress2" runat="server" Width="100%" CssClass="tableLabel" Height="22px">Address 2</asp:label></TD>
								<TD colSpan="6"><asp:textbox id="txtAddress2" runat="server" Width="100%" CssClass="textField"></asp:textbox></TD>
								<TD colSpan="1">&nbsp;</TD>
								<TD colSpan="3"><asp:label id="lblFax" runat="server" Width="100%" CssClass="tableLabel" Height="22px">Fax</asp:label></TD>
								<TD colSpan="3"><asp:textbox id="txtFax" runat="server" Width="100%" CssClass="textField"></asp:textbox></TD>
								<TD colSpan="3">&nbsp;</TD>
							</tr>
							<tr height="27">
								<TD align="right" colSpan="1"><asp:requiredfieldvalidator id="validCustZip" ControlToValidate="txtZipCode" Runat="server" Width="100%" Display="Dynamic">&nbsp;*&nbsp;</asp:requiredfieldvalidator></TD>
								<TD colSpan="3"><asp:label id="lblZip" runat="server" Width="100%" CssClass="tableLabel" Height="22px">Zip Code</asp:label></TD>
								<TD colSpan="2"><asp:textbox id="txtZipCode" runat="server" Width="100%" CssClass="textField" AutoPostBack="True"></asp:textbox></TD>
								<TD colSpan="1"><asp:button id="btnZipcodePopup" runat="server" Width="100%" CssClass="queryButton" CausesValidation="False" Text="..."></asp:button></TD>
								<TD colSpan="1"><asp:label id="lblState" runat="server" Width="100%" CssClass="tableLabel" Height="22px">State</asp:label></TD>
								<TD colSpan="2"><asp:textbox id="txtState" runat="server" Width="100%" CssClass="textField" Enabled="False"></asp:textbox></TD>
								<TD colSpan="1">&nbsp;</TD>
								<TD colSpan="3"><asp:label id="lblEmail" runat="server" Width="100%" CssClass="tableLabel" Height="22px">Email</asp:label></TD>
								<TD colSpan="6"><asp:textbox id="txtEmail" runat="server" Width="100%" CssClass="textField"></asp:textbox></TD>
							</tr>
							<tr height="27">
								<TD align="right" colSpan="1"><asp:requiredfieldvalidator id="Requiredfieldvalidator3" ControlToValidate="txtCountry" Runat="server" Width="100%" Display="Dynamic">&nbsp;*&nbsp;</asp:requiredfieldvalidator></TD>
								<TD colSpan="3"><asp:label id="lblCountry" runat="server" Width="100%" CssClass="tableLabel" Height="22px">Country</asp:label></TD>
								<TD colSpan="2"><asp:textbox id="txtCountry" runat="server" Width="100%" CssClass="textField" AutoPostBack="True"></asp:textbox></TD>
								<TD colSpan="14">&nbsp;</TD>
							</tr>
						</TABLE>
					</TD>
				</TR>
				<tr>
					<td>&nbsp;</td>
				</tr>
				<TR width="100%">
					<TD width="100%">
						<TABLE id="tblPaymentInfo" width="100%" align="left" border="0">
							<TR height="27">
								<td colSpan="1"></td>
								<TD class="tableHeading" colSpan="19">Payment Details</TD>
							</TR>
							<TR height="27">
								<TD width="5%"></TD>
								<TD width="5%"></TD>
								<TD width="5%"></TD>
								<TD width="5%"></TD>
								<TD width="5%"></TD>
								<TD width="5%"></TD>
								<TD width="5%"></TD>
								<TD width="5%"></TD>
								<TD width="5%"></TD>
								<TD width="5%"></TD>
								<TD width="5%"></TD>
								<TD width="5%"></TD>
								<TD width="5%"></TD>
								<TD width="5%"></TD>
								<TD width="5%"></TD>
								<TD width="5%"></TD>
								<TD width="5%"></TD>
								<TD width="5%"></TD>
								<TD width="5%"></TD>
								<TD width="5%"></TD>
							</TR>
							<TR height="27">
								<TD align="right" colSpan="1"><asp:requiredfieldvalidator id="Requiredfieldvalidator1" ControlToValidate="txtCreditTerm" Runat="server" Width="100%" Display="Dynamic">&nbsp;*&nbsp;</asp:requiredfieldvalidator></TD>
								<TD colSpan="4"><asp:label id="lblCreditTerm" runat="server" Width="100%" CssClass="tableLabel" Height="22px">Credit Term</asp:label></TD>
								<TD colSpan="5"><asp:textbox id="txtCreditTerm" runat="server" Width="100%" CssClass="textField" AutoPostBack="True"></asp:textbox></TD>
								<TD colSpan="1">&nbsp;</TD>
								<TD colSpan="3"><asp:label id="lblCreditLimit" runat="server" Width="100%" CssClass="tableLabel" Height="22px">Credit Limit</asp:label></TD>
								<TD colSpan="3"><asp:textbox id="txtCreditLimit" tabIndex="1" runat="server" Width="100%" CssClass="textField" Enabled="False"></asp:textbox></TD>
								<TD colSpan="3">&nbsp;</TD>
							</TR>
							<tr height="27">
								<TD colSpan="1">&nbsp;</TD>
								<TD colSpan="4"><asp:label id="lblCreditOutstanding" runat="server" Width="100%" CssClass="tableLabel" Height="22px">Credit Outstanding</asp:label></TD>
								<TD colSpan="5"><asp:textbox id="txtCreditOutstanding" runat="server" Width="100%" CssClass="textField"></asp:textbox></TD>
								<TD colSpan="1">&nbsp;</TD>
								<TD colSpan="3"><asp:label id="lblPaymentMode" runat="server" Width="100%" CssClass="tableLabel" Height="22px">Payment Mode</asp:label></TD>
								<TD colSpan="3"><asp:radiobutton id="radioBtnCash" runat="server" Width="100%" Height="15px" AutoPostBack="True" Text="Cash" GroupName="PaymentType" Font-Size="Smaller"></asp:radiobutton></TD>
								<TD colSpan="3"><asp:radiobutton id="radioBtnCredit" runat="server" Width="100%" Height="15px" AutoPostBack="True" Text="Credit" GroupName="PaymentType" Font-Size="Smaller"></asp:radiobutton></TD>
							</tr>
							<tr height="27">
								<TD colSpan="1">&nbsp;</TD>
								<TD colSpan="4">
									<asp:label id="lblMBG" runat="server" Height="22px" Width="100%" CssClass="tableLabel">MBG</asp:label></TD>
								<TD colSpan="5">
									<asp:textbox id="txtMBG" runat="server" Width="100%" CssClass="textField"></asp:textbox></TD>
								<TD colSpan="1">&nbsp;</TD>
								<TD colSpan="3">
									<asp:label id="lblActiveQuatationNo" runat="server" Height="22px" Width="100%" CssClass="tableLabel">Active Quation</asp:label></TD>
								<TD colSpan="3">
									<asp:textbox id="txtActiveQuatationNo" runat="server" Width="100%" CssClass="textField" ReadOnly="True"></asp:textbox></TD>
								<TD colSpan="3">&nbsp;</TD>
							</tr>
						</TABLE>
					</TD>
				</TR>
				<tr>
					<td>&nbsp;</td>
				</tr>
			</TABLE>
		</form>
	</body>
</HTML>
