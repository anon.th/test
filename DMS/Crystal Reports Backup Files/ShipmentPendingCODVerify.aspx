<%@ Import Namespace="com.common.util" %>
<%@ Import Namespace="Cambro.Web.DbCombo" %>
<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<%@ Register TagPrefix="DbCombo" Namespace="Cambro.Web.DbCombo" Assembly="Cambro.Web.DbCombo" %>
<%@ Page language="c#" Codebehind="ShipmentPendingCODVerify.aspx.cs" AutoEventWireup="false" Inherits="com.ties.ShipmentPendingCODVerify" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>COD Amount Collected Report</title>
		<LINK rel="stylesheet" type="text/css" href="css/Styles.css" name="stylesheet">
		<meta name="GENERATOR" content="Microsoft Visual Studio 7.0">
		<meta name="CODE_LANGUAGE" content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<!--#INCLUDE FILE="msFormValidations.inc"-->
		<script language="javascript" src="Scripts/settingScrollPosition.js"></script>
	</HEAD>
	<body onclick="GetScrollPosition();" onunload="window.top.displayBanner.fnCloseAll(0);"
		onload="SetScrollPosition();" MS_POSITIONING="GridLayout">
		<form id="CODAmountCollectedReport" method="post" runat="server">
			<asp:button style="Z-INDEX: 101; POSITION: absolute; TOP: 40px; LEFT: 20px" id="btnQuery" runat="server"
				Width="64px" CssClass="queryButton" Text="Query"></asp:button><asp:label style="Z-INDEX: 105; POSITION: absolute; TOP: 69px; LEFT: 24px" id="lblErrorMessage"
				runat="server" Width="700px" CssClass="errorMsgColor" Height="17px">Place holder for err msg</asp:label>&nbsp;
			<asp:button style="Z-INDEX: 102; POSITION: absolute; TOP: 40px; LEFT: 90px" id="btnExecuteQuery"
				runat="server" Width="123px" CssClass="queryButton" Text="Generate"></asp:button>
			<TABLE style="Z-INDEX: 104; POSITION: absolute; WIDTH: 800px; HEIGHT: 200px; TOP: 90px; LEFT: 21px"
				id="tblShipmentTracking" border="0" width="800" runat="server">
				<TR>
					<TD style="WIDTH: 437px; HEIGHT: 137px" vAlign="top">
						<fieldset style="WIDTH: 470px; HEIGHT: 129px"><legend><asp:label id="lblDates" runat="server" Width="20px" CssClass="tableHeadingFieldset" Font-Bold="True">Dates</asp:label></legend>
							<TABLE style="WIDTH: 465px; HEIGHT: 113px" id="tblDates" border="0" cellSpacing="0" cellPadding="0"
								align="left" runat="server">
								<TR>
									<td></td>
									<TD colSpan="3">&nbsp;
										<asp:radiobutton id="rbActualPODDate" runat="server" Width="156px" CssClass="tableRadioButton" Text="Actual POD Date"
											Height="22px" Checked="True" GroupName="QueryByDate" AutoPostBack="True"></asp:radiobutton><asp:radiobutton id="rbCODAmountCollectedDate" runat="server" Width="269px" CssClass="tableRadioButton"
											Text="COD Amount Collected Date" Height="22px" GroupName="QueryByDate" AutoPostBack="True"></asp:radiobutton>&nbsp;</TD>
								</TR>
								<TR>
									<td></td>
									<TD colSpan="3">&nbsp;
										<asp:radiobutton id="rbMonth" runat="server" Width="73px" CssClass="tableRadioButton" Text="Month"
											Height="21px" Checked="True" GroupName="EnterDate" AutoPostBack="True"></asp:radiobutton>&nbsp;
										<asp:dropdownlist id="ddMonth" runat="server" Width="88px" CssClass="textField" Height="19px"></asp:dropdownlist>&nbsp;
										<asp:label id="lblYear" runat="server" Width="30px" CssClass="tableLabel" Height="15px">Year</asp:label>&nbsp;
										<cc1:mstextbox id="txtYear" runat="server" Width="83" CssClass="textField" NumberPrecision="4"
											NumberMinValue="1" NumberMaxValue="2999" TextMaskType="msNumeric" MaxLength="5"></cc1:mstextbox></TD>
								</TR>
								<TR>
									<td style="HEIGHT: 20px"></td>
									<TD style="HEIGHT: 20px" colSpan="3">&nbsp;
										<asp:radiobutton id="rbPeriod" runat="server" Width="62px" CssClass="tableRadioButton" Text="Period"
											GroupName="EnterDate" AutoPostBack="True"></asp:radiobutton>&nbsp;&nbsp;&nbsp;
										<cc1:mstextbox id="txtPeriod" runat="server" Width="82" CssClass="textField" TextMaskType="msDate"
											MaxLength="10" TextMaskString="99/99/9999"></cc1:mstextbox>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
										&nbsp;
										<cc1:mstextbox id="txtTo" runat="server" Width="83" CssClass="textField" TextMaskType="msDate"
											MaxLength="10" TextMaskString="99/99/9999"></cc1:mstextbox></TD>
								</TR>
								<TR>
									<td></td>
									<TD colSpan="3">&nbsp;
										<asp:radiobutton id="rbDate" runat="server" Width="74px" CssClass="tableRadioButton" Text="Date"
											Height="22px" GroupName="EnterDate" AutoPostBack="True"></asp:radiobutton>&nbsp;&nbsp;<cc1:mstextbox id="txtDate" runat="server" Width="82" CssClass="textField" TextMaskType="msDate"
											MaxLength="10" TextMaskString="99/99/9999"></cc1:mstextbox></TD>
								</TR>
							</TABLE>
						</fieldset>
					</TD>
					<TD style="HEIGHT: 137px" vAlign="top">&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;
						<FIELDSET style="WIDTH: 320px; HEIGHT: 121px"><LEGEND><asp:label id="lblDestination" runat="server" Width="79px" CssClass="tableHeadingFieldset"
									Font-Bold="True">Destination</asp:label></LEGEND>
							<TABLE style="WIDTH: 300px; HEIGHT: 57px" id="tblDestination" border="0" cellSpacing="0"
								cellPadding="0" align="left" runat="server">
								<TR height="37">
									<TD style="HEIGHT: 33px"></TD>
									<TD style="WIDTH: 360px; HEIGHT: 33px" class="tableLabel" colSpan="3">&nbsp;
										<asp:label id="lblZipCode" runat="server" Width="84px" CssClass="tableLabel" Height="22px">Postal Code</asp:label>&nbsp;&nbsp;&nbsp;
										<cc1:mstextbox id="txtZipCode" runat="server" Width="139px" CssClass="textField" TextMaskType="msUpperAlfaNumericWithUnderscore"
											MaxLength="10"></cc1:mstextbox>&nbsp;&nbsp;
										<asp:button id="btnZipCode" runat="server" CssClass="searchButton" Text="..." CausesValidation="False"></asp:button></TD>
									<TD style="HEIGHT: 33px"></TD>
								</TR>
								<TR>
									<TD style="HEIGHT: 33px"></TD>
									<TD style="WIDTH: 360px; HEIGHT: 33px" class="tableLabel" colSpan="3">&nbsp;
										<asp:label id="lblStateCode" runat="server" Width="100px" CssClass="tableLabel" Height="22px">State / Province</asp:label><cc1:mstextbox id="txtStateCode" runat="server" Width="139" CssClass="textField" TextMaskType="msUpperAlfaNumericWithUnderscore"
											MaxLength="10"></cc1:mstextbox>&nbsp;&nbsp;
										<asp:button id="btnStateCode" runat="server" CssClass="searchButton" Text="..." CausesValidation="False"></asp:button></TD>
									<TD style="HEIGHT: 33px"></TD>
								</TR>
							</TABLE>
						</FIELDSET>
					</TD>
				</TR>
				<TR>
					<TD style="WIDTH: 437px; HEIGHT: 134px" vAlign="top">
						<fieldset style="WIDTH: 470px; HEIGHT: 137px"><legend><asp:label id="lblRouteType" runat="server" Width="145px" CssClass="tableHeadingFieldset" Font-Bold="True">Route / DC Selection</asp:label></legend>
							<TABLE style="WIDTH: 465px; HEIGHT: 113px" id="tblRouteType" border="0" cellSpacing="0"
								cellPadding="0" align="left" runat="server">
								<TR>
									<TD style="WIDTH: 450px" class="tableLabel" colSpan="2">&nbsp;
										<asp:radiobutton id="rbLinehaul" runat="server" Width="153px" CssClass="tableRadioButton" Text="Linehaul"
											Height="22px" GroupName="QueryByRoute" AutoPostBack="True"></asp:radiobutton><asp:radiobutton id="rbDeliveryRoute" runat="server" Width="131px" CssClass="tableRadioButton" Text="Delivery Route"
											Height="22px" Checked="True" GroupName="QueryByRoute" AutoPostBack="True"></asp:radiobutton><asp:radiobutton id="rbAirRoute" runat="server" Width="152px" CssClass="tableRadioButton" Text="Air Route"
											Height="22px" GroupName="QueryByRoute" AutoPostBack="True"></asp:radiobutton></TD>
								</TR>
								<tr>
									<TD class="tableLabel" width="20%">&nbsp;<asp:label id="lblRouteCode" runat="server" Width="111px" CssClass="tableLabel" Height="22px">Route Code</asp:label></TD>
									<td style="WIDTH: 190px"><DBCOMBO:DBCOMBO id="DbComboPathCode" tabIndex="14" Width="158px" CssClass="tableLabel" AutoPostBack="True"
											ServerMethod="DbComboPathCodeSelect" ShowDbComboLink="False" TextUpLevelSearchButton="v" RegistrationKey=" " TextBoxColumns="18"
											Runat="server"></DBCOMBO:DBCOMBO></td>
								</tr>
								<TR>
									<TD class="tableLabel">&nbsp;<asp:label id="lblOriginDisCenter" runat="server" Width="154px" CssClass="tableLabel" Height="22px">Origin Distribution Center</asp:label></TD>
									<TD style="WIDTH: 190px"><DBCOMBO:DBCOMBO id="DbComboOriginDC" tabIndex="14" Width="161px" CssClass="tableLabel" AutoPostBack="True"
											ServerMethod="DbComboDistributionCenterSelect" ShowDbComboLink="False" TextUpLevelSearchButton="v" RegistrationKey=" "
											TextBoxColumns="18" Runat="server"></DBCOMBO:DBCOMBO></TD>
								</TR>
								<TR>
									<TD class="tableLabel">&nbsp;<asp:label id="lblDesDisCenter" runat="server" Width="181px" CssClass="tableLabel" Height="22px">Destination Distribution Center</asp:label></TD>
									<TD style="WIDTH: 190px"><DBCOMBO:DBCOMBO id="DbComboDestinationDC" tabIndex="14" Width="160px" CssClass="tableLabel" AutoPostBack="True"
											ServerMethod="DbComboDistributionCenterSelect" ShowDbComboLink="False" TextUpLevelSearchButton="v" RegistrationKey=" "
											TextBoxColumns="18" Runat="server"></DBCOMBO:DBCOMBO></TD>
								</TR>
							</TABLE>
						</fieldset>
					</TD>
					<TD style="HEIGHT: 134px" vAlign="top">&nbsp;&nbsp;&nbsp;
						<FIELDSET style="WIDTH: 320px; HEIGHT: 105px"><LEGEND><asp:label id="lblPayerType" runat="server" Width="79px" CssClass="tableHeadingFieldset" Font-Bold="True">Payer Type</asp:label></LEGEND>
							<TABLE style="WIDTH: 300px; HEIGHT: 73px" id="tblPayerType" border="0" cellSpacing="0"
								cellPadding="0" align="left" runat="server">
								<TR>
									<TD></TD>
									<TD style="WIDTH: 118px" height="1" colSpan="2">&nbsp;</TD>
									<TD></TD>
									<TD></TD>
								</TR>
								<TR>
									<TD></TD>
									<TD style="WIDTH: 118px" class="tableLabel" vAlign="top" colSpan="2">&nbsp;
										<asp:label id="lblPayType" runat="server" Width="96px" CssClass="tableLabel" Height="22px">Payer Type</asp:label></TD>
									<TD><asp:listbox id="lsbCustType" runat="server" Width="137px" Height="61px" SelectionMode="Multiple"></asp:listbox></TD>
									<TD></TD>
								</TR>
								<TR height="33">
									<TD bgColor="blue"></TD>
									<TD style="WIDTH: 118px" class="tableLabel" vAlign="top" colSpan="2">&nbsp;
										<asp:label id="lblPayCode" runat="server" Width="79px" CssClass="tableLabel" Height="22px">Payer Code</asp:label>&nbsp;&nbsp;</TD>
									<TD><asp:textbox id="txtPayerCode" runat="server" Width="136px" CssClass="textField"></asp:textbox>&nbsp;&nbsp;
										<asp:button id="btnPayerCode" runat="server" CssClass="searchButton" Text="..." CausesValidation="False"></asp:button></TD>
									<TD></TD>
								</TR>
							</TABLE>
						</FIELDSET>
					</TD>
				</TR>
				<tr>
					<td colspan="2">
						<fieldset style="WIDTH: 470px; HEIGHT: 20px"><legend><asp:label id="lblCODRemittancetoCustomerAdvanceAmount" runat="server" Width="80px" CssClass="tableHeadingFieldset"
									Font-Bold="True">COD Refused</asp:label></legend>
							<TABLE id="tblCODRemittancetoCustomerAdvanceAmount" style="WIDTH: 465px; HEIGHT: 30px"
								cellSpacing="0" cellPadding="0" align="center" border="0" runat="server">
								<TR>
									<td style="WIDTH: 80px">
										<asp:label id="lblCODRefuse" runat="server" Width="111px" CssClass="tableLabel" Height="22px">COD Refused </asp:label></td>
									<td>
										<asp:DropDownList id="ddlCODRefuse" runat="server" Width="72px" Height="19px"></asp:DropDownList>
									</td>
								</TR>
							</TABLE>
						</fieldset>
					</td>
				</tr>
			</TABLE>
			<asp:label style="Z-INDEX: 103; POSITION: absolute; TOP: 4px; LEFT: 20px" id="Label1" runat="server"
				Width="484px" CssClass="maintitleSize" Height="27px"> Shipment Pending COD Verify Report</asp:label></TR></TABLE></TR></TABLE><input 
style="Z-INDEX: 106; POSITION: absolute; TOP: 35px; LEFT: 222px" 
value="<%=strScrollPosition%>" type=hidden name=ScrollPosition>
		</form>
	</body>
</HTML>
