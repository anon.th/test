<%@ Page language="c#" Codebehind="AgentService.aspx.cs" AutoEventWireup="false" Inherits="com.ties.AgentService" %>
<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<%@ Register TagPrefix="dbCombo" Namespace="Cambro.Web.DbCombo" Assembly="Cambro.Web.DbCombo" %>
<%@ Import Namespace="com.common.util" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>AgentService</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<!--#INCLUDE FILE="msFormValidations.inc"-->
		<LINK rev="stylesheet" href="css/styles.css" rel="stylesheet">
		<script language="javascript" src="Scripts/settingScrollPosition.js"></script>
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="AgentService" method="post" runat="server">
			<asp:requiredfieldvalidator id="reqAgentId" ErrorMessage="Agent Id " ControlToValidate="dbCmbAgentId" Display="None" BorderWidth="0" Runat="server"></asp:requiredfieldvalidator><asp:label id="lblTitle" style="Z-INDEX: 105; LEFT: 24px; POSITION: absolute; TOP: 8px" runat="server" Width="336px" Height="32px" CssClass="mainTitleSize">Agent Service</asp:label><asp:label id="lblServiceExcludedTitle" style="Z-INDEX: 119; LEFT: 23px; POSITION: absolute; TOP: 321px" runat="server" CssClass="mainTitleSize">Postal Code Excluded for Service</asp:label><asp:label id="lblZVASEMessage" style="Z-INDEX: 118; LEFT: 541px; POSITION: absolute; TOP: 362px" runat="server" Width="187px"></asp:label><asp:datagrid id="dgZipcodeServiceExcluded" style="Z-INDEX: 119; LEFT: 22px; POSITION: absolute; TOP: 385px" runat="server" Width="710px" Height="40px" AllowCustomPaging="True" PageSize="5" AllowPaging="True" ItemStyle-Height="20" OnPageIndexChanged="dgZipcodeServiceExcluded_PageChange" OnUpdateCommand="dgZipcodeServiceExcluded_Update" OnDeleteCommand="dgZipcodeServiceExcluded_Delete" AutoGenerateColumns="False" OnItemCommand="dgZipcodeServiceExcluded_Button">
				<ItemStyle Height="20px"></ItemStyle>
				<Columns>
					<asp:ButtonColumn Text="&lt;IMG SRC='images/butt-delete.gif' border=0 title='Delete' &gt;" CommandName="Delete">
						<HeaderStyle HorizontalAlign="Center" Width="3%" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle HorizontalAlign="Center" CssClass="gridField"></ItemStyle>
					</asp:ButtonColumn>
					<asp:TemplateColumn HeaderText="Postal Code">
						<HeaderStyle Font-Bold="True" Width="30%" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle CssClass="gridField"></ItemStyle>
						<ItemTemplate>
							<asp:Label CssClass="gridLabel" ID="lblZipcode" Text='<%#DataBinder.Eval(Container.DataItem,"zipcode")%>' Runat="server">
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<cc1:mstextbox CssClass="gridTextBox" ID="txtZipcode" Text='<%#DataBinder.Eval(Container.DataItem,"zipcode")%>' Runat="server" Enabled="True" MaxLength="10" TextMaskType="msUpperAlfaNumericWithUnderscore">
							</cc1:mstextbox>
							<asp:RequiredFieldValidator ID="exValidator" Runat="server" BorderWidth="0" Display="None" ControlToValidate="txtZipcode" ErrorMessage="Postal code is required field"></asp:RequiredFieldValidator>
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="State">
						<HeaderStyle Width="30%" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle CssClass="gridField"></ItemStyle>
						<ItemTemplate>
							<asp:Label CssClass="gridLabel" ID="lblState" Text='<%#DataBinder.Eval(Container.DataItem,"state_name")%>' Runat="server" >
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<asp:TextBox CssClass="gridTextBox" ID="txtState" Text='<%#DataBinder.Eval(Container.DataItem,"state_name")%>' Runat="server" ReadOnly="true">
							</asp:TextBox>
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Country">
						<HeaderStyle Width="30%" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle CssClass="gridField"></ItemStyle>
						<ItemTemplate>
							<asp:Label CssClass="gridLabel" ID="lblCountry" Text='<%#DataBinder.Eval(Container.DataItem,"country")%>' Runat="server" >
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<asp:TextBox CssClass="gridTextBox" ID="txtCountry" Text='<%#DataBinder.Eval(Container.DataItem,"country")%>' Runat="server" ReadOnly="true">
							</asp:TextBox>
						</EditItemTemplate>
					</asp:TemplateColumn>
				</Columns>
				<PagerStyle NextPageText="Next" PrevPageText="Previous" HorizontalAlign="Right" CssClass="normalText"></PagerStyle>
			</asp:datagrid><asp:datagrid id="dgServiceCodes" style="Z-INDEX: 100; LEFT: 20px; POSITION: absolute; TOP: 128px" runat="server" Width="707px" AllowCustomPaging="True" PageSize="5" AllowPaging="True" ItemStyle-Height="20" OnPageIndexChanged="dgServiceCodes_PageChange" OnUpdateCommand="dgServiceCodes_Update" OnDeleteCommand="dgServiceCodes_Delete" AutoGenerateColumns="False" OnItemCommand="dgServiceCodes_Button" SelectedItemStyle-CssClass="gridFieldSelected" OnSelectedIndexChanged="dgServiceCodes_SelectedIndexChanged" OnItemDataBound="dgServiceCodes_Bound" OnCancelCommand="dgServiceCodes_Cancel" OnEditCommand="dgServiceCodes_Edit">
				<SelectedItemStyle CssClass="gridFieldSelected"></SelectedItemStyle>
				<ItemStyle Height="20px" CssClass="gridField"></ItemStyle>
				<Columns>
					<asp:ButtonColumn Text="&lt;IMG SRC='images/butt-select.gif' border=0 title='Select' &gt;" CommandName="Select">
						<HeaderStyle HorizontalAlign="Center" Width="3%" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle HorizontalAlign="Center"></ItemStyle>
					</asp:ButtonColumn>
					<asp:EditCommandColumn ButtonType="LinkButton" UpdateText="&lt;IMG SRC='images/butt-update.gif' border=0 title='Update' &gt;" CancelText="&lt;IMG SRC='images/butt-cancel.gif' border=0 title='Cancel' &gt;" EditText="&lt;IMG SRC='images/butt-edit.gif' border=0 title='Edit' &gt;">
						<HeaderStyle Width="3%" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle HorizontalAlign="Left"></ItemStyle>
					</asp:EditCommandColumn>
					<asp:ButtonColumn Text="&lt;IMG SRC='images/butt-delete.gif' border=0 title='Delete' &gt;" CommandName="Delete">
						<HeaderStyle HorizontalAlign="Center" Width="3%" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle HorizontalAlign="Center"></ItemStyle>
					</asp:ButtonColumn>
					<asp:TemplateColumn HeaderText="Service Code">
						<HeaderStyle Font-Bold="True" Width="19%" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle Wrap="False"></ItemStyle>
						<ItemTemplate>
							<asp:Label CssClass="gridLabel" ID="lblServiceCode" Text='<%#DataBinder.Eval(Container.DataItem,"service_code")%>' Runat="server">
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<cc1:mstextbox CssClass="gridTextBox" ID="txtServiceCode" Text='<%#DataBinder.Eval(Container.DataItem,"service_code")%>' Runat="server" Enabled="True" MaxLength="12" TextMaskType="msUpperAlfaNumericWithUnderscore" Readonly=True>
							</cc1:mstextbox>
							<asp:RequiredFieldValidator ID="scValidator" Runat="server" BorderWidth="0" Display="None" ControlToValidate="txtServiceCode" ErrorMessage="Service Code is required field"></asp:RequiredFieldValidator>
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:ButtonColumn Text="&lt;IMG SRC='images/butt-search.gif' border=0 title='Search' &gt;" CommandName="search">
						<HeaderStyle HorizontalAlign="Center" Width="3%" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle HorizontalAlign="Center"></ItemStyle>
					</asp:ButtonColumn>
					<asp:TemplateColumn HeaderText="Service Description">
						<HeaderStyle Width="23%" CssClass="gridHeading"></HeaderStyle>
						<ItemTemplate>
							<asp:Label CssClass="gridLabel" ID="lblServiceDescription" Text='<%#DataBinder.Eval(Container.DataItem,"service_description")%>' Runat="server" Enabled="True">
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<asp:TextBox CssClass="gridTextBox" ID="txtServiceDescription" Text='<%#DataBinder.Eval(Container.DataItem,"service_description")%>' Runat="server" MaxLength="200">
							</asp:TextBox>
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Service Charge %">
						<HeaderStyle Width="20%" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle Wrap="False"></ItemStyle>
						<ItemTemplate>
							<asp:Label CssClass="gridLabelNumber" ID="lblPercentDiscount" Text='<%#DataBinder.Eval(Container.DataItem,"service_charge_percent","{0:#0.00;-#0.00}")%>' Runat="server">
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<asp:TextBox CssClass="gridTextBoxNumber" ID="txtPercentDiscount" Text='<%#DataBinder.Eval(Container.DataItem,"service_charge_percent","{0:#0.00;-#0.00}")%>' Runat="server" Enabled="True">
							</asp:TextBox>
							<asp:RangeValidator ID="pdValidator" Type="Double" Runat="server" MinimumValue="-100" MaximumValue="100" BorderWidth="0" Display="None" ControlToValidate="txtPercentDiscount" ErrorMessage="Enter value between -100 to 100 for Service Charge %"></asp:RangeValidator>
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Service Charge $">
						<HeaderStyle Width="20%" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle Wrap="False"></ItemStyle>
						<ItemTemplate>
							<asp:Label CssClass="gridLabelNumber" ID="Label1" Text='<%#DataBinder.Eval(Container.DataItem,"service_charge_amt","{0:#0.00;-#0.00}")%>' Runat="server">
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<asp:TextBox CssClass="gridTextBoxNumber" ID="txtServiceAmt" Text='<%#DataBinder.Eval(Container.DataItem,"service_charge_amt","{0:#0.00;-#0.00}")%>' Runat="server" Enabled="True">
							</asp:TextBox>
							<asp:RangeValidator ID="Rangevalidator3" Type="Double" Runat="server" MinimumValue="-100" MaximumValue="100" BorderWidth="0" Display="None" ControlToValidate="txtServiceAmt" ErrorMessage="Pls enter valid  value eg 100 or -100"></asp:RangeValidator>
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Commit Time">
						<HeaderStyle Width="20%" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle Wrap="False"></ItemStyle>
						<ItemTemplate>
							<asp:Label CssClass="gridLabelNumber" ID="lblCommitTime" Text='<%#DataBinder.Eval(Container.DataItem,"commit_time","{0:HH:mm}")%>' Runat="server">
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<cc1:mstextbox CssClass="gridTextBoxNumber" ID="txtCommitTime" Text='<%#DataBinder.Eval(Container.DataItem,"commit_time","{0:HH:mm}")%>' Runat="server" Enabled="True" TextMaskType="msTime" TextMaskString="99:99">
							</cc1:mstextbox>
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Transit Day">
						<HeaderStyle Width="20%" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle Wrap="False"></ItemStyle>
						<ItemTemplate>
							<asp:Label CssClass="gridLabelNumber" ID="lblTransitDay" Text='<%#DataBinder.Eval(Container.DataItem,"transit_day","{0:#0}")%>' Runat="server">
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<cc1:mstextbox CssClass="gridTextBoxNumber" ID="txtTransitDay" Text='<%#DataBinder.Eval(Container.DataItem,"transit_day")%>' Runat="server" Enabled="True" MaxLength="12" TextMaskType="msNumeric" NumberMaxValue="60" NumberMinValue="0" NumberPrecision="2" NumberScale="0">
							</cc1:mstextbox>
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Transit Hour">
						<HeaderStyle Width="23%" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle Wrap="False"></ItemStyle>
						<ItemTemplate>
							<asp:Label CssClass="gridLabelNumber" ID="lblTransitHour" Text='<%#DataBinder.Eval(Container.DataItem,"transit_hour","{0:#0}")%>' Runat="server" Width="30px">
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<cc1:mstextbox CssClass="gridTextBoxNumber" ID="txtTransitHour" Text='<%#DataBinder.Eval(Container.DataItem,"transit_hour")%>' Runat="server" Enabled="True" MaxLength="12" TextMaskType="msNumeric" NumberMaxValue="9" NumberMinValue="0" NumberPrecision="1" NumberScale="0">
							</cc1:mstextbox>
						</EditItemTemplate>
					</asp:TemplateColumn>
				</Columns>
				<PagerStyle NextPageText="Next" PrevPageText="Previous" HorizontalAlign="Right" CssClass="normalText"></PagerStyle>
			</asp:datagrid><asp:button id="btnGoToFirstPage" style="Z-INDEX: 112; LEFT: 608px; POSITION: absolute; TOP: 48px" tabIndex="5" runat="server" Width="24px" CssClass="queryButton" Text="|<" CausesValidation="False"></asp:button><asp:button id="btnPreviousPage" style="Z-INDEX: 106; LEFT: 632px; POSITION: absolute; TOP: 48px" tabIndex="6" runat="server" Width="24px" CssClass="queryButton" Text="<" CausesValidation="False"></asp:button><asp:textbox id="txtCountRec" style="Z-INDEX: 110; LEFT: 656px; POSITION: absolute; TOP: 48px" tabIndex="7" runat="server" Width="24px" Height="19px"></asp:textbox><asp:button id="btnNextPage" style="Z-INDEX: 107; LEFT: 680px; POSITION: absolute; TOP: 48px" tabIndex="8" runat="server" Width="24px" CssClass="queryButton" Text=">" CausesValidation="False"></asp:button><asp:button id="btnGoToLastPage" style="Z-INDEX: 109; LEFT: 704px; POSITION: absolute; TOP: 48px" tabIndex="9" runat="server" Width="24px" CssClass="queryButton" Text=">|" CausesValidation="False"></asp:button><asp:label id="lblAgentName" style="Z-INDEX: 111; LEFT: 386px; POSITION: absolute; TOP: 110px" runat="server" Width="100px" CssClass="tableLabel">Agent Name</asp:label><asp:label id="lblAgentCode" style="Z-INDEX: 108; LEFT: 26px; POSITION: absolute; TOP: 110px" runat="server" Width="100px" CssClass="tableLabel">Agent Code</asp:label><asp:button id="btnQuery" style="Z-INDEX: 101; LEFT: 22px; POSITION: absolute; TOP: 48px" runat="server" Width="62px" CssClass="queryButton" Text="Query" CausesValidation="False"></asp:button><asp:button id="btnExecQry" style="Z-INDEX: 102; LEFT: 84px; POSITION: absolute; TOP: 48px" runat="server" Width="130px" CssClass="queryButton" Text="Execute Query" CausesValidation="False"></asp:button><asp:label id="lblServiceMessage" style="Z-INDEX: 103; LEFT: 28px; POSITION: absolute; TOP: 79px" runat="server" Width="439px" CssClass="errorMsgColor">Error Message</asp:label></TD></TD>&nbsp;</TD>&nbsp;
			<asp:button id="btnInsertVAS" style="Z-INDEX: 113; LEFT: 215px; POSITION: absolute; TOP: 48px" runat="server" Width="57px" CssClass="queryButton" Text="Insert" CausesValidation="False"></asp:button><asp:button id="btnZVASEInsert" style="Z-INDEX: 114; LEFT: 25px; POSITION: absolute; TOP: 355px" runat="server" Width="54px" CssClass="queryButton" Text="Insert" CausesValidation="False"></asp:button><asp:label id="lblEXMessage" style="Z-INDEX: 115; LEFT: 89px; POSITION: absolute; TOP: 358px" runat="server" Width="365px" Height="20px" CssClass="errorMsgColor"></asp:label><dbcombo:dbcombo id="dbCmbAgentName" style="Z-INDEX: 116; LEFT: 490px; POSITION: absolute; TOP: 102px" tabIndex="2" runat="server" Width="182px" Height="17px" AutoPostBack="True" TextBoxColumns="25" ServerMethod="AgentNameServerMethod" TextUpLevelSearchButton="v" ShowDbComboLink="False" ReQueryOnLoad="True"></dbcombo:dbcombo><dbcombo:dbcombo id="dbCmbAgentId" style="Z-INDEX: 117; LEFT: 138px; POSITION: absolute; TOP: 102px" tabIndex="1" runat="server" Width="140px" Height="17px" AutoPostBack="True" TextBoxColumns="20" ServerMethod="AgentIdServerMethod" TextUpLevelSearchButton="v" ShowDbComboLink="False" ReQueryOnLoad="True"></dbcombo:dbcombo><asp:validationsummary id="PageValidationSummary" style="Z-INDEX: 104; LEFT: 39px; POSITION: absolute; TOP: 515px" runat="server" Width="460px" Height="70px" HeaderText="Please enter the missing fields." DisplayMode="BulletList" ShowSummary="False" ShowMessageBox="True"></asp:validationsummary><INPUT 
type=hidden value="<%=strScrollPosition%>" name=ScrollPosition>
		</form>
	</body>
</HTML>
