<%@ Page language="c#" Codebehind="DailyLodgments.aspx.cs" AutoEventWireup="false" 
	Inherits="TIES.WebUI.DailyLodgments" SmartNavigation="True" %>
<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<%@ Import Namespace="com.common.util" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
  <HEAD>
		<title>Daily Lodgments</title>
		<meta name="vs_snapToGrid" content="True">
		<meta name="vs_showGrid" content="True">
		<LINK rel="stylesheet" type="text/css" href="css/Styles.css" name="stylesheet">
		<meta name="GENERATOR" content="Microsoft Visual Studio 7.0">
		<meta name="CODE_LANGUAGE" content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<!--#INCLUDE FILE="msFormValidations.inc"-->
		<script language="javascript" src="Scripts/settingScrollPosition.js"></script>
		<script language="javascript" src="Scripts/JScript_TextControl.js"></script>
		<script language="javascript" src="Scripts/WorkProgress.js"></script>
		<script language="javascript">
			function SetInitialFocus(name)
			{			
				var contrl = document.getElementById(name);
				contrl.focus();
			}
			
			function UpperMask( toField )
			{
				var lcNewChar = String.fromCharCode(window.event.keyCode);
				var llRetVal = true;
				window.event.keyCode = lcNewChar.toUpperCase().charCodeAt(0);
				return llRetVal;
			}
			
			function Confirm_Print() {
				var confirm_value = document.createElement("INPUT");
				confirm_value.type = "hidden";
				confirm_value.name = "confirm_value";
				if (confirm("This manifest may contain information that is not up to date.\nDo you wish to proceed?")) {
					confirm_value.value = "Yes";
				} else {
					confirm_value.value = "No";
				}
				document.forms[0].appendChild(confirm_value);
				document.getElementById("btnHidReport").click();
			}
		</script>
</HEAD>
	<body onclick="GetScrollPosition();" onunload="window.top.displayBanner.fnCloseAll(0);"
		onload="SetScrollPosition();" MS_POSITIONING="GridLayout">
		<form id="DailyLodgments" method="post" runat="server">
			<INPUT style="Z-INDEX: 101; POSITION: absolute; TOP: 680px; LEFT: 16px" type="hidden" name="ScrollPosition">
			<asp:validationsummary style="Z-INDEX: 105; POSITION: absolute; COLOR: red; TOP: 672px; LEFT: 240px" id="Validationsummary1"
				Runat="server" DisplayMode="BulletList" ShowMessageBox="True" HeaderText="Please enter the missing  fields."
				ShowSummary="False"></asp:validationsummary>
			<div style="Z-INDEX: 102; POSITION: relative; HEIGHT: 850px; TOP: 16px; LEFT: 14px" id="divMain"
				MS_POSITIONING="GridLayout" runat="server">
				<TABLE style="Z-INDEX: 101; POSITION: absolute; TOP: 8px; LEFT: 0px" id="MainTable" border="0"
					width="100%" runat="server">
					<tr>
						<td colSpan="2" align="left"><asp:label id="Label1" runat="server" CssClass="mainTitleSize" Height="27px" Width="321px">
								Daily Lodgments</asp:label></td>
					</tr>
					<tr>
						<td style="HEIGHT: 21px" vAlign="top" colSpan="2" align="left"><asp:button id="btnQry" runat="server" CssClass="queryButton" Width="61px" CausesValidation="False"
								Text="Query"></asp:button><asp:button id="btnExecQry" runat="server" CssClass="queryButton" Width="130px" CausesValidation="False"
								Text="Execute Query"></asp:button><asp:button id="btnInsert" runat="server" CssClass="queryButton" Width="62px" CausesValidation="False"
								Text="Insert"></asp:button><asp:button id="btnPrintScanSheet" runat="server" CssClass="queryButton" Width="120px" CausesValidation="False"
								Text="Print Scan Sheet"></asp:button><asp:button id="btnPrintLodgementSummary" runat="server" CssClass="queryButton" Width="180px"
								CausesValidation="False" Text="Print Lodgments Summary"></asp:button><asp:button id="btnManifestScannedPkgs" runat="server" CssClass="queryButton" Width="160px"
								CausesValidation="False" Text="Manifest Scanned Pkgs"></asp:button><asp:button id="btnPrintManifest" runat="server" CssClass="queryButton" Width="100px" CausesValidation="False"
								Text="Print Manifest"></asp:button><asp:button id="btnExport" runat="server" CssClass="queryButton" Width="62px" CausesValidation="False"
								Text="Export"></asp:button><asp:button style="DISPLAY: none; VISIBILITY: hidden" id="btnDelete" runat="server" CssClass="queryButton"
								Width="62px" CausesValidation="False" Text="Delete"></asp:button></td>
					</tr>
					<tr>
						<td colSpan="2"><asp:label id="lblErrorMsg" runat="server" CssClass="errorMsgColor" Height="19px" Width="566px"></asp:label></td>
					</tr>
					<tr>
						<td vAlign="top" colSpan="2" align="left">
							<table border="0" cellSpacing="1" cellPadding="1">
								<tr>
									<td><asp:label id="lbl1" runat="server" CssClass="tableLabel" Width="75px">Batch Date:
										</asp:label></td>
									<td><cc1:mstextbox id="Txt_BatchDate" Runat="server" CssClass="textField" Width="106px" Text="" Enabled="True"
											TextMaskString="99/99/9999" TextMaskType="msDate" MaxLength="10"></cc1:mstextbox></td>
									<td><asp:label id="Label2" runat="server" CssClass="tableLabel" Width="85px">Service Type:
										</asp:label></td>
									<td><asp:dropdownlist id="Drp_ServiceType" runat="server" CssClass="textField" AutoPostBack="False"></asp:dropdownlist></td>
									<td><asp:label id="Label3" runat="server" CssClass="tableLabel" Width="55px">Location:
										</asp:label></td>
									<td><asp:dropdownlist id="Drp_Location" runat="server" CssClass="textField" AutoPostBack="False"></asp:dropdownlist></td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td colSpan="2"><asp:datagrid id="dgLodgments" runat="server" Width="100%" AutoGenerateColumns="False" AllowPaging="True"
								SelectedItemStyle-CssClass="gridFieldSelected" PageSize="25" style="Z-INDEX: 0">
								<SelectedItemStyle CssClass="gridFieldSelected"></SelectedItemStyle>
								<ItemStyle Height="15px" CssClass="gridField"></ItemStyle>
								<Columns>
									<asp:ButtonColumn Text="&lt;IMG SRC='images/butt-select.gif' border=0 title='Select' &gt;" CommandName="Select">
										<HeaderStyle Width="2%" CssClass="gridHeading"></HeaderStyle>
										<ItemStyle CssClass="gridField"></ItemStyle>
									</asp:ButtonColumn>
									<asp:EditCommandColumn ButtonType="LinkButton" UpdateText="&lt;IMG SRC='images/butt-update.gif' border=0 title='Update' &gt;" 
 CancelText="&lt;IMG SRC='images/butt-cancel.gif' border=0 title='Cancel' &gt;" EditText="&lt;IMG SRC='images/butt-edit.gif' border=0 title='Edit' &gt;">
										<HeaderStyle Width="2%" CssClass="gridHeading"></HeaderStyle>
										<ItemStyle Wrap="False" CssClass="gridField"></ItemStyle>
									</asp:EditCommandColumn>
									<asp:TemplateColumn HeaderText="Batch ID">
										<HeaderStyle Font-Bold="True" Width="8%" CssClass="gridHeading"></HeaderStyle>
										<ItemStyle Wrap="False" CssClass="gridField"></ItemStyle>
										<ItemTemplate>
											<%#DataBinder.Eval(Container.DataItem,"BatchID")%>
											<INPUT id ="hSelectedBatchID" runat="server" value='<%#DataBinder.Eval(Container.DataItem,"BatchID")%>' type="hidden" NAME="hSelectedBatchID">
											<INPUT id ="hSelectedBatchNumber" runat="server" value='<%#DataBinder.Eval(Container.DataItem,"BatchNumber")%>' type="hidden" NAME="hSelectedBatchNumber">
											<INPUT id ="hSelectedDest_Pkg_Count" runat="server" value='<%#DataBinder.Eval(Container.DataItem,"Dest_Pkg_Count")%>' type="hidden" NAME="hSelectedDest_Pkg_Count">
											<INPUT id ="hSelectedScanned_Pkg_Count" runat="server" value='<%#DataBinder.Eval(Container.DataItem,"Scanned_Pkg_Count")%>' type="hidden" NAME="hSelectedScanned_Pkg_Count">
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Dest Pkg Count">
										<HeaderStyle Font-Bold="True" Width="8%" CssClass="gridHeading"></HeaderStyle>
										<ItemStyle Wrap="False" CssClass="gridField"></ItemStyle>
										<ItemTemplate>
											<%#DataBinder.Eval(Container.DataItem,"Dest_Pkg_Count")%>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Scanned Pkg Count">
										<HeaderStyle Font-Bold="True" Width="8%" CssClass="gridHeading"></HeaderStyle>
										<ItemStyle Wrap="False" CssClass="gridField"></ItemStyle>
										<ItemTemplate>
											<%#DataBinder.Eval(Container.DataItem,"Scanned_Pkg_Count")%>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Variance">
										<HeaderStyle Font-Bold="True" Width="6%" CssClass="gridHeading"></HeaderStyle>
										<ItemStyle Wrap="False" CssClass="gridField"></ItemStyle>
										<ItemTemplate>
											<%#DataBinder.Eval(Container.DataItem,"Variance")%>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="AWB">
										<HeaderStyle Font-Bold="True" Width="13%" CssClass="gridHeading"></HeaderStyle>
										<ItemStyle Wrap="False" CssClass="gridField"></ItemStyle>
										<ItemTemplate>
											<%#DataBinder.Eval(Container.DataItem,"AWB_Number")%>
										</ItemTemplate>
										<EditItemTemplate>
											<INPUT id ="hBatchID" runat="server" value='<%#DataBinder.Eval(Container.DataItem,"BatchID")%>' type="hidden" NAME="hBatchID">
											<INPUT id ="hBatchNumber" runat="server" value='<%#DataBinder.Eval(Container.DataItem,"BatchNumber")%>' type="hidden" NAME="hBatchNumber">
											<asp:TextBox ID="txtAWB_Number" CssClass="gridTextBox" style="TEXT-TRANSFORM: uppercase" Text='<%#DataBinder.Eval(Container.DataItem,"AWB_Number")%>' Runat="server" Enabled="True" />
											<asp:RequiredFieldValidator ID="cCostCode" Runat="server" BorderWidth="0" Display="None" ControlToValidate="txtAWB_Number" 
 ErrorMessage="AWB"></asp:RequiredFieldValidator>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Flight Number">
										<HeaderStyle Font-Bold="True" Width="10%" CssClass="gridHeading"></HeaderStyle>
										<ItemStyle Wrap="False" CssClass="gridField"></ItemStyle>
										<ItemTemplate>
											<%#DataBinder.Eval(Container.DataItem,"Flight_Number")%>
										</ItemTemplate>
										<EditItemTemplate>
											<asp:TextBox ID="txtFlight_Number" style="TEXT-TRANSFORM: uppercase" CssClass="gridTextBox" Text='<%#DataBinder.Eval(Container.DataItem,"Flight_Number")%>' Runat="server" Enabled="True"/>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Departure D/T">
										<HeaderStyle Font-Bold="True" Width="13%" CssClass="gridHeading"></HeaderStyle>
										<ItemStyle Wrap="False" CssClass="gridField"></ItemStyle>
										<ItemTemplate>
											<%#DataBinder.Eval(Container.DataItem,"DepartureDT","{0:dd/MM/yyyy HH:mm}")%>
										</ItemTemplate>
										<EditItemTemplate>
											<cc1:mstextbox ID="txtDepartureDT" runat="server" CssClass="gridTextBox" Enabled="True" Text='<%#DataBinder.Eval(Container.DataItem,"DepartureDT","{0:dd/MM/yyyy HH:mm}")%>' Runat="server" MaxLength="16" TextMaskString="99/99/9999 99:99" TextMaskType="msDateTime">
											</cc1:mstextbox>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Arrival D/T">
										<HeaderStyle Font-Bold="True" Width="13%" CssClass="gridHeading"></HeaderStyle>
										<ItemStyle Wrap="False" CssClass="gridField"></ItemStyle>
										<ItemTemplate>
											<%#DataBinder.Eval(Container.DataItem,"ArrivalDT","{0:dd/MM/yyyy HH:mm}")%>
										</ItemTemplate>
										<EditItemTemplate>
											<cc1:mstextbox ID="txtArrivalDT" CssClass="gridTextBox" Text='<%#DataBinder.Eval(Container.DataItem,"ArrivalDT","{0:dd/MM/yyyy HH:mm}")%>' Runat="server" MaxLength="16" TextMaskString="99/99/9999 99:99" TextMaskType="msDateTime" AutoPostBack="true">
											</cc1:mstextbox>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Batch No.">
										<HeaderStyle Font-Bold="True" Width="10%" CssClass="gridHeading"></HeaderStyle>
										<ItemStyle Wrap="False" CssClass="gridField"></ItemStyle>
										<ItemTemplate>
											<%#DataBinder.Eval(Container.DataItem,"BatchNumber")%>
										</ItemTemplate>
										<EditItemTemplate>
											<%#DataBinder.Eval(Container.DataItem,"BatchNumber")%>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Status">
										<HeaderStyle Font-Bold="True" Width="7%" CssClass="gridHeading"></HeaderStyle>
										<ItemStyle Wrap="False" CssClass="gridField"></ItemStyle>
										<ItemTemplate>
											<%#DataBinder.Eval(Container.DataItem,"Status")%>
										</ItemTemplate>
									</asp:TemplateColumn>
								</Columns>
								<PagerStyle NextPageText="Next" PrevPageText="Previous" HorizontalAlign="Left" CssClass="normalText"></PagerStyle>
							</asp:datagrid></td>
					</tr>
				</TABLE>
			</div>
			&nbsp;
			<div style="Z-INDEX: 103; POSITION: relative; WIDTH: 637px; HEIGHT: 176px; TOP: 46px; LEFT: 7px"
				id="divExport" MS_POSITIONING="GridLayout" runat="server">
				<TABLE style="Z-INDEX: 105; POSITION: absolute; WIDTH: 350px; HEIGHT: 129px; TOP: 16px; LEFT: 43px"
					id="Table2" runat="server">
					<tr>
						<td colSpan="4">
							<p class="tableHeadingFieldset">Select parameters for export
							</p>
						</td>
					</tr>
					<tr>
						<td width="70" align="right">
							<p class="tableLabel">Date From:
							</p>
						</td>
						<td><cc1:mstextbox id="Txt_Exp_DateFrom" Runat="server" CssClass="textField" Width="106px" Text=""
								Enabled="True" TextMaskString="99/99/9999" TextMaskType="msDate" MaxLength="10"></cc1:mstextbox></td>
						<td>
							<p class="tableLabel" align="right">To:
							</p>
						</td>
						<td><cc1:mstextbox id="Txt_Exp_DateTo" Runat="server" CssClass="textField" Width="106px" Text="" Enabled="True"
								TextMaskString="99/99/9999" TextMaskType="msDate" MaxLength="10"></cc1:mstextbox></td>
					</tr>
					<tr>
						<td>
							<p class="tableLabel" align="right">Location:
							</p>
						</td>
						<td colSpan="3"><asp:dropdownlist id="Drp_Exp_Location" runat="server" CssClass="textField" AutoPostBack="False"></asp:dropdownlist></td>
					</tr>
					<tr>
						<td colSpan="4" align="center"><asp:button id="btnExceedOK" runat="server" CssClass="queryButton" Width="44px" CausesValidation="False"
								Text="OK"></asp:button><asp:button id="btnExceedCancel" runat="server" CssClass="queryButton" Width="65px" CausesValidation="False"
								Text="Cancel"></asp:button></td>
					</tr>
				</TABLE>
				<P align="center"></P>
			</div>
			<INPUT value="<%=strScrollPosition%>" type=hidden name="ScrollPosition"> <INPUT type="hidden" name="hdnRefresh">
			<asp:Button style="Z-INDEX: 105; POSITION: absolute; DISPLAY: none; TOP: 1064px; LEFT: 672px"
				id="btnHidReport" runat="server" Text=".."></asp:Button>
		</form>
	</body>
</HTML>
