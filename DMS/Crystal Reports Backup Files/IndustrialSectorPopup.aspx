<%@ Page language="c#" Codebehind="IndustrialSectorPopup.aspx.cs" AutoEventWireup="false" Inherits="com.ties.IndustrialSectorPopup" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>IndustrialSectorPopup</title>
		<LINK href="css/Styles.css" type="text/css" rel="stylesheet" name="stylesheet">
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="IndustrialSectorPopup" method="post" runat="server">
			<asp:datagrid id="dgIndustrialSector" style="Z-INDEX: 102; LEFT: 45px; POSITION: absolute; TOP: 96px" runat="server" OnPageIndexChanged="Paging" PageSize="10" AllowPaging="True" Width="522px" AutoGenerateColumns="False" BorderColor="#CCCCCC" BackColor="White" CellPadding="3" BorderWidth="1px" BorderStyle="None" CssClass="gridHeading" AllowCustomPaging="True">
				<SelectedItemStyle Font-Bold="True" ForeColor="White" BackColor="#669999"></SelectedItemStyle>
				<ItemStyle CssClass="drilldownField"></ItemStyle>
				<HeaderStyle Width="10%" CssClass="drilldownHeading"></HeaderStyle>
				<FooterStyle ForeColor="#000066" BackColor="White"></FooterStyle>
				<Columns>
					<asp:BoundColumn DataField="industrial_sector_code" HeaderText="Industrial Sector"></asp:BoundColumn>
					<asp:BoundColumn DataField="industrial_sector_description" HeaderText="Description"></asp:BoundColumn>
					<asp:ButtonColumn Text="Select" CommandName="Select"></asp:ButtonColumn>
				</Columns>
				<PagerStyle NextPageText="Next" PrevPageText="Previous" HorizontalAlign="Right" ForeColor="#000066" BackColor="White"></PagerStyle>
			</asp:datagrid>
			<asp:Label id="lblVASCode" style="Z-INDEX: 105; LEFT: 54px; POSITION: absolute; TOP: 58px" runat="server" CssClass="tablelabel">Industrial Sector</asp:Label><asp:button id="btnSearch" style="Z-INDEX: 104; LEFT: 312px; POSITION: absolute; TOP: 54px" runat="server" Width="84px" CssClass="buttonProp" Height="21px" Text="Search" CausesValidation="False"></asp:button><asp:textbox id="txtIndustrialSector" style="Z-INDEX: 103; LEFT: 178px; POSITION: absolute; TOP: 56px" runat="server" Width="116px" CssClass="textField" Height="19px"></asp:textbox><asp:button id="btnOk" style="Z-INDEX: 101; LEFT: 403px; POSITION: absolute; TOP: 54px" runat="server" Width="84px" CssClass="buttonProp" Height="21px" Text="Close" CausesValidation="False"></asp:button></form>
	</body>
</HTML>
