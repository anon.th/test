<%@ Page language="c#" Codebehind="ImExConsignment.aspx.cs" AutoEventWireup="false" Inherits="com.ties.ImExConsignment" %>
<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<%@ Import Namespace="com.common.util" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Import-Export ConsignmentNote</title>
		<meta content="True" name="vs_showGrid">
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../css/Styles.css" type="text/css" rel="Stylesheet">
		<!--#INCLUDE FILE="../msFormValidations.inc"-->
		<script language="javascript" src="../Scripts/settingScrollPosition.js"></script>
		<script language="javascript" type="text/javascript">
		function DoAction(serveraction){
			var confirmed = true;
			var isSubmitted = true;
			var URL = "";
			var ErrMsg = "";
			switch (serveraction){
				case "IMPORTDATA":
					if (document.frmImExConsignment.file_upload.value == "") {
						ErrMsg = "��س��к���������!"; 
					}
					isSubmitted = true;
					break;
				case "EXPORTDATA":
					confirmed = confirm("��سҡ� OK �����׹�ѹ��� Export data");
					break;
				case "CLEAR":
					isSubmitted = true;
					break;
			}
			if (isSubmitted && confirmed){
				if (ErrMsg != ""){
					alert(ErrMsg);
				} else {
					document.forms[0].elements("ServerAction").value = serveraction;
					document.forms[0].submit(); 
				}  
			}
		}
		</script>
	</HEAD>
	<body onkeypress="microsoftKeyPress()" onclick="GetScrollPosition();" onload="SetScrollPosition();"
		onunload="window.top.displayBanner.fnCloseAll(0);" MS_POSITIONING="ViewLayout">
		<form id="frmImExConsignment" method="post" runat="server">
			<INPUT id="ServerAction" type="hidden" name="ServerAction">
			<table cellSpacing="0" cellPadding="0" width="100%" border="0">
				<tr>
					<td>
						<table class="TextBody" cellSpacing="0" cellPadding="3" width="100%" border="0">
							<tr>
								<td colSpan="2"><asp:label id="lblMainTitle" runat="server" CssClass="mainTitleSize" Width="477px">Import - Export ConsignmentNote</asp:label></td>
							</tr>
							<tr style="HEIGHT: 5px">
								<td colSpan="2"></td>
							</tr>
							<tr>
								<td></td>
								<td align="left"><A onclick="javascript:DoAction('IMPORTDATA');"><INPUT class="queryButton" id="btnImport" style="WIDTH: 60px; HEIGHT: 20px" type="button"
											value="Import" name="btnImport" runat="server"></A>&nbsp; <a onclick="javascript:DoAction('EXPORTDATA');">
										<input class="queryButton" id="btnExport" style="WIDTH: 120px; HEIGHT: 20px" type="button"
											value="Export to Excel" name="btnExport"></a>&nbsp; <a onclick="javascript:DoAction('CLEAR');">
										<input class="queryButton" id="btnClear" style="WIDTH: 60px; HEIGHT: 20px" type="button"
											value="Clear" name="btnClear"></a>
									<table id="tabExportStaff" runat="server">
										<tr>
											<td colSpan="5">&nbsp;</td>
										</tr>
										<tr>
											<td class="gridLabel" style="WIDTH: 12%; TEXT-ALIGN: right">Payer ID&nbsp;:&nbsp;</td>
											<td style="WIDTH: 15%; TEXT-ALIGN: left"><cc1:mstextbox id="txtPayerId" CssClass="textField" runat="server" Width="100px" MaxLength="10"></cc1:mstextbox></td>
											<td class="gridLabel" style="WIDTH: 12%; TEXT-ALIGN: right">Consignment 
												Date&nbsp;:&nbsp;</td>
											<td style="WIDTH: 15%; TEXT-ALIGN: left"><cc1:mstextbox id="txtConsDate" CssClass="textField" Width="100px" MaxLength="10" TextMaskString="99/99/9999"
													Enabled="True" AutoPostBack="True" TextMaskType="msDate" Runat="server"></cc1:mstextbox></td>
											<td><asp:button id="btnCancelEx" runat="server" Width="110px" Text="Cancel Export"></asp:button>&nbsp;
												<asp:button id="btnExbyStaff" runat="server" Width="110px" Text="Export by Staff"></asp:button></td>
										</tr>
										<tr>
											<td class="gridLabel" style="FONT-WEIGHT: bold; COLOR: blue; TEXT-ALIGN: left" colSpan="5">&nbsp;
												<asp:label id="lblResult" runat="server"></asp:label></td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td colSpan="2"><asp:label id="ErrorMsg" runat="server" CssClass="errorMsgColor" Width="643px" Height="3px"></asp:label></td>
							</tr>
							<tr class="gridHeading">
								<td colSpan="2"><STRONG><FONT size="3">Import Consignment</FONT></STRONG></td>
							</tr>
							<tr>
								<td style="WIDTH: 250px" align="right">Select Excel File&nbsp;:&nbsp;</td>
								<td style="WIDTH: 900px" align="left"><INPUT id="file_upload" style="WIDTH: 500px; HEIGHT: 24px" type="file" size="38" name="file_upload"
										runat="server">
								</td>
							</tr>
							<tr style="HEIGHT: 5px">
								<td colSpan="2">&nbsp;</td>
							</tr>
							<tr>
								<td colSpan="2">
									<TABLE cellSpacing="0" cellPadding="0" width="100%" align="center" border="0">
										<tr>
											<td style="WIDTH: 10%"></td>
											<td style="WIDTH: 35%" vAlign="top"><asp:panel id="pnlCheckStep" runat="server">
													<FIELDSET><LEGEND><STRONG>Check Step</STRONG></LEGEND><BR>
														<TABLE cellSpacing="0" cellPadding="0" width="100%" align="center" border="0">
															<TR style="HEIGHT: 20px">
																<TD style="WIDTH: 20%" align="left">
																	<asp:Image id="imgCheck1" runat="server" Width="20px" Height="20px" Visible="False" ImageUrl="../images/OK.png"
																		BorderWidth="0px"></asp:Image>
																	<asp:Image id="imgCheck2" runat="server" Width="20px" Height="20px" Visible="False" ImageUrl="../images/NO.png"
																		BorderWidth="0px"></asp:Image></TD>
																<TD style="WDTH: 80%" align="left">
																	<asp:Label id="lblCheck1" runat="server" Font-Size="14px" Font-Names="Microsoft Sans Serif">Check Excel File Format</asp:Label></TD>
															</TR>
															<TR style="HEIGHT: 20px">
																<TD align="left">
																	<asp:Image id="imgCheck3" runat="server" Width="20px" Height="20px" Visible="False" ImageUrl="../images/OK.png"
																		BorderWidth="0px"></asp:Image>
																	<asp:Image id="imgCheck4" runat="server" Width="20px" Height="20px" Visible="False" ImageUrl="../images/NO.png"
																		BorderWidth="0px"></asp:Image></TD>
																<TD>
																	<asp:Label id="lblCheck2" runat="server" Font-Size="14px" Font-Names="Microsoft Sans Serif">Import Data from Excel File</asp:Label></TD>
															</TR>
															<TR style="HEIGHT: 20px">
																<TD align="left">
																	<asp:Image id="imgCheck5" runat="server" Width="20px" Height="20px" Visible="False" ImageUrl="../images/OK.png"
																		BorderWidth="0px"></asp:Image>
																	<asp:Image id="imgCheck6" runat="server" Width="20px" Height="20px" Visible="False" ImageUrl="../images/NO.png"
																		BorderWidth="0px"></asp:Image></TD>
																<TD>
																	<asp:Label id="lblCheck3" runat="server" Font-Size="14px" Font-Names="Microsoft Sans Serif">Check Required Data</asp:Label></TD>
															</TR>
															<TR style="HEIGHT: 20px">
																<TD align="left">
																	<asp:Image id="imgCheck7" runat="server" Width="20px" Height="20px" Visible="False" ImageUrl="../images/OK.png"
																		BorderWidth="0px"></asp:Image>
																	<asp:Image id="imgCheck8" runat="server" Width="20px" Height="20px" Visible="False" ImageUrl="../images/NO.png"
																		BorderWidth="0px"></asp:Image></TD>
																<TD>
																	<asp:Label id="lblCheck4" runat="server" Font-Size="14px" Font-Names="Microsoft Sans Serif">Check Format Data</asp:Label></TD>
															</TR>
															<TR style="HEIGHT: 20px">
																<TD align="left">
																	<asp:Image id="imgCheck9" runat="server" Width="20px" Height="20px" Visible="False" ImageUrl="../images/OK.png"
																		BorderWidth="0px"></asp:Image>
																	<asp:Image id="imgCheck10" runat="server" Width="20px" Height="20px" Visible="False" ImageUrl="../images/NO.png"
																		BorderWidth="0px"></asp:Image></TD>
																<TD>
																	<asp:Label id="lblCheck5" runat="server" Font-Size="14px" Font-Names="Microsoft Sans Serif">Save Data to Database</asp:Label></TD>
															</TR>
															<TR style="HEIGHT: 20px">
																<TD align="left">
																	<asp:Image id="imgCheck11" runat="server" Width="20px" Height="20px" Visible="False" ImageUrl="../images/OK.png"
																		BorderWidth="0px"></asp:Image>
																	<asp:Image id="imgCheck12" runat="server" Width="20px" Height="20px" Visible="False" ImageUrl="../images/NO.png"
																		BorderWidth="0px"></asp:Image></TD>
																<TD>
																	<asp:Label id="lblCheck6" runat="server" Font-Size="14px" Font-Names="Microsoft Sans Serif">Import Data Complete</asp:Label></TD>
															</TR>
															<TR>
																<TD colSpan="2"></TD>
															</TR>
														</TABLE>
													</FIELDSET>
												</asp:panel></td>
											<td style="WIDTH: 5%">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
											<td style="WIDTH: 40%" valign="top">
												<asp:panel id="pnlDescription" runat="server"> <!--FIELDSET><LEGEND><STRONG>Description</STRONG></LEGEND><BR>
														<TABLE cellSpacing="0" width="100%" align="center" border="0">
															<TR>
																<TD>
																	<asp:Label id="lblDescription" runat="server">Process Detail Here...</asp:Label></TD>
															</TR>
															<TR style="HEIGHT: 25px">
																<TD>&nbsp;</TD>
															</TR>
														</TABLE>
													</FIELDSET--></asp:panel>
												<asp:panel id="pnlException" runat="server">
													<FIELDSET><LEGEND><STRONG>Exception Case</STRONG></LEGEND><BR>
														<TABLE cellSpacing="0" cellPadding="0" width="100%" align="center" border="0">
															<TR>
																<TD>
																	<asp:TextBox id="txtException" runat="server" Width="550px" Height="200px" ForeColor="Red" TextMode="MultiLine"></asp:TextBox></TD>
															</TR>
														</TABLE>
													</FIELDSET>
												</asp:panel></td>
											<td style="WIDTH: 10%"></td>
										</tr>
									</TABLE>
								</td>
							</tr>
							<tr>
								<td vAlign="top"></td>
								<td></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
