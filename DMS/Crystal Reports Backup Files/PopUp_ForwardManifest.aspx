<%@ Import Namespace="com.common.util" %>
<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<%@ Page language="c#" Codebehind="PopUp_ForwardManifest.aspx.cs" AutoEventWireup="false" Inherits="TIES.WebUI.Manifest.PopUp_ForwardManifest" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Select Recipient for Manifest</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK rel="stylesheet" type="text/css" href="../css/Styles.css">
		<!--#INCLUDE FILE="../msFormValidations.inc"-->
		<script language="javascript" src="../Scripts/settingScrollPosition.js"></script>
		<script language="javascript">
		function disableListItems(checkBoxListId,  strIndex)
{
    // Get the checkboxlist object.

    objCtrl = document.getElementById(checkBoxListId);
    
    // Does the checkboxlist not exist?

    if(objCtrl == null)
    {
        return;
    }

    var i = 0;

	var strArray=strIndex.split(",");
	for(i=0;i<strArray.length;i++)
		{
			if(strArray[i]!=""){
				var objItemChecked = document.getElementById(checkBoxListId + '_' + strArray[i]);
				if(objItemChecked == null)
				{
					return;
				}
				objItemChecked.disabled = true;
			}
		}
}
function validateForm(element)
{
	var obj = document.getElementById("lblError");
	obj.innerHTML ="Email sending is in progress - please wait...";
	//element.submitButton.disabled = 'disabled';
	__doPostBack("btnSendEmail",'');
	element.disabled = 'disabled';
	
	return true;  
}
		
		</script>
	</HEAD>
	<BODY onclick="GetScrollPosition();" onunload="window.top.displayBanner.fnCloseAll(0);"
		onload="SetScrollPosition();" MS_POSITIONING="GridLayout">
		<FORM id="PopUp_CreateUpdateBatch" method="post" runat="server">
			<asp:label style="Z-INDEX: 104; POSITION: absolute; TOP: 8px; LEFT: 16px" id="Header1" Runat="server"
				Width="376px" CssClass="mainTitleSize">Select Recipient for Manifest</asp:label><asp:label style="Z-INDEX: 135; POSITION: absolute; TOP: 32px; LEFT: 16px" id="lblError" runat="server"
				Width="364px" CssClass="errorMsgColor"></asp:label><asp:label style="Z-INDEX: 106; POSITION: absolute; TOP: 56px; LEFT: 328px" id="lblDCAddress"
				runat="server" CssClass="tableLabel">DC Email Address:</asp:label><asp:label style="Z-INDEX: 105; POSITION: absolute; TOP: 56px; LEFT: 16px" id="lblEmailContent"
				runat="server" CssClass="tableLabel">Enter message to include in email:</asp:label>&nbsp;<asp:button style="Z-INDEX: 101; POSITION: absolute; TOP: 16px; LEFT: 448px" id="btnSendEmail"
				runat="server" Width="101px" CssClass="queryButton" Text="Send Email"></asp:button><asp:button style="Z-INDEX: 102; POSITION: absolute; TOP: 40px; LEFT: 448px" id="btnCancel"
				runat="server" Width="101px" CssClass="queryButton" Text="Cancel"></asp:button>
			<asp:textbox style="Z-INDEX: 103; POSITION: absolute; TOP: 80px; LEFT: 16px" id="txtEmailContent"
				runat="server" Width="288px" CssClass="textField" MaxLength="255" TextMode="MultiLine" Height="152px"></asp:textbox><asp:checkboxlist style="Z-INDEX: 108; POSITION: absolute; TOP: 80px; LEFT: 320px" id="ckboxDCEMail"
				runat="server" CssClass="tablelabel" AutoPostBack="True"></asp:checkboxlist></FORM>
	</BODY>
</HTML>
