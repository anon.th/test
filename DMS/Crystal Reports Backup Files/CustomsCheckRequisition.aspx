<%@ Import Namespace="com.common.util" %>
<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<%@ Page language="c#" Codebehind="CustomsCheckRequisition.aspx.cs" AutoEventWireup="false" Inherits="TIES.WebUI.CustomsReport_CheckRequisitions" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>CustomsReport_CheckRequisitions</title>
		<meta name="vs_snapToGrid" content="True">
		<meta name="vs_showGrid" content="True">
		<LINK rel="stylesheet" type="text/css" href="css/Styles.css" name="stylesheet">
		<meta name="GENERATOR" content="Microsoft Visual Studio 7.0">
		<meta name="CODE_LANGUAGE" content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<!--#INCLUDE FILE="msFormValidations.inc"-->
		<script language="javascript" src="Scripts/settingScrollPosition.js"></script>
		<script language="javascript" src="Scripts/JScript_TextControl.js"></script>
		<script language="javascript" type="text/javascript">
			function funcDisBack(){
				history.go(+1);
			}

			function RemoveBadNonNumber(strTemp, obj) {
				strTemp = strTemp.replace(/[^0-9]/g, ''); //replace non-numeric
				obj.value = strTemp.replace(/\s/g, ''); // replace space
			}
			function AfterPasteNonNumber(obj) {
				setTimeout(function () {
					RemoveBadNonNumber(obj.value, obj);
				}, 1); //or 4
			}
			
			function validate(evt) {
				var theEvent = evt || window.event;
				var key = theEvent.keyCode || theEvent.which;
				key = String.fromCharCode( key );
				var regex = /[0-9]/;
				if( !regex.test(key) ) {
					theEvent.returnValue = false;
					if(theEvent.preventDefault) theEvent.preventDefault();
				}
			}
			
			function ValidateDigitsAndLetters(evt) {
				var theEvent = evt || window.event;
				var key = theEvent.keyCode || theEvent.which;
				key = String.fromCharCode( key );
				var regex = /[a-zA-Z0-9]/;
				if( !regex.test(key) ) {
					theEvent.returnValue = false;
					if(theEvent.preventDefault) theEvent.preventDefault();
				}
			}
			
			function ValidateReceiptNumber(evt) {
				var theEvent = evt || window.event;
				var key = theEvent.keyCode || theEvent.which;
				key = String.fromCharCode( key );
				var regex = /[,a-zA-Z0-9]/; // Accept Digits, Letters and Comma
				if( !regex.test(key) ) {
					theEvent.returnValue = false;
					if(theEvent.preventDefault) theEvent.preventDefault();
				}
			}
			
			function ValidateDigitsAWBNumber(evt) {
				var theEvent = evt || window.event;
				var key = theEvent.keyCode || theEvent.which;
				key = String.fromCharCode( key );
				var regex = /[0-9]/;
				if( !regex.test(key) ) {
					theEvent.returnValue = false;
					if(theEvent.preventDefault) theEvent.preventDefault();
				}
			}
		
			
			function RemoveBadDigitsAndLetters(strTemp, obj) {
				strTemp = strTemp.replace(/[^a-zA-Z0-9]/g, ''); //replace non-numeric
				obj.value = strTemp.replace(/\s/g, ''); // replace space
			}
			function AfterPasteDigitsAndLetters(obj) {
				setTimeout(function () {
					RemoveBadDigitsAndLetters(obj.value, obj);
				}, 1); //or 4
			}
			
			function AfterPasteReceiptNumber(obj) {
				setTimeout(function () {
					strTemp = document.getElementById('txtReceiptNumber').value;
					strTemp = strTemp.replace(/[^a-zA-Z0-9,]/g, ''); //replace non-numeric
					strTemp = strTemp.replace(/\s/g, ''); // replace space
					document.getElementById('txtReceiptNumber').value = strTemp;
				}, 1);
			}
			
			
			function callback()
			{
					var btn = document.getElementById('btnClientEvent');
					if(btn != null)
					{					
						btn.click();
					}else{
						alert('error call code behind');
					}				        
				
			}				
		</script>
	</HEAD>
	<body onclick="GetScrollPosition();" onunload="window.top.displayBanner.fnCloseAll(0);"
		onload="SetScrollPosition();" MS_POSITIONING="GridLayout">
		<form id="CustomsJobEntryCompiling" method="post" runat="server">
			<div style="Z-INDEX: 102; POSITION: relative; WIDTH: 1000px; HEIGHT: 1571px; TOP: 48px; LEFT: 32px"
				id="divMain" MS_POSITIONING="GridLayout" runat="server">
				<TABLE style="Z-INDEX: 101; POSITION: absolute; TOP: 8px; LEFT: 0px" id="MainTable" border="0"
					width="100%" runat="server">
					<tbody>
						<tr>
							<td align="left"><asp:label id="Label1" runat="server" CssClass="mainTitleSize" Height="27px" Width="421px">Create/Update a Cheque Requisition</asp:label></td>
						</tr>
						<tr>
							<td><asp:button id="btnQry" runat="server" CssClass="queryButton" CausesValidation="False" Text="Query"></asp:button><asp:button id="btnExecQry" tabIndex="1" runat="server" CssClass="queryButton" CausesValidation="False"
									Text="Execute Query"></asp:button><asp:button id="btnInsert" tabIndex="2" runat="server" CssClass="queryButton" CausesValidation="False"
									Text="Insert" Enabled="True"></asp:button><asp:button id="btnSave" tabIndex="3" runat="server" CssClass="queryButton" CausesValidation="False"
									Text="Save" Enabled="False"></asp:button><asp:button id="btnPrintChequeReq" tabIndex="4" runat="server" CssClass="queryButton" CausesValidation="False"
									Text="Print Cheque Req" Enabled="True"></asp:button><asp:button style="DISPLAY: none" id="btnClientEvent" runat="server" CausesValidation="False"
									Text="ClientEvent"></asp:button></td>
						</tr>
						<tr>
							<td style="HEIGHT: 19px"><asp:label id="lblError" runat="server" Font-Bold="True" Font-Size="X-Small" ForeColor="Red"></asp:label></td>
						</tr>
						<tr>
							<td>
								<table id="table_content" width="70%">
									<tbody>
										<tr>
											<td width="18%"><asp:label style="Z-INDEX: 0" id="Label2" runat="server" CssClass="tableLabel">Requisition Type</asp:label><asp:label id="Label3" runat="server" CssClass="tableLabel" ForeColor="Red">*</asp:label></td>
											<td colSpan="3"><asp:dropdownlist style="Z-INDEX: 0" id="ddlRequisitionType" tabIndex="5" runat="server" Width="114px"></asp:dropdownlist></td>
										</tr>
										<tr>
											<td><asp:label style="Z-INDEX: 0" id="Label4" runat="server" CssClass="tableLabel">Requisition Number</asp:label></td>
											<td width="35%"><cc1:mstextbox onblur="if( this.value !=''){document.getElementById('btnExecQry').click();}" style="Z-INDEX: 0"
													id="txtRequisitionNumber" tabIndex="6" runat="server" CssClass="textField" Width="112px" NumberPrecision="10"
													NumberMinValue="0" NumberMaxValueCOD="999999999999" TextMaskType="msNumericCOD" MaxLength="15"></cc1:mstextbox></td>
											<td width="12%"><asp:label style="Z-INDEX: 0" id="Label5" runat="server" CssClass="tableLabel">Payee</asp:label><asp:label id="Label6" runat="server" CssClass="tableLabel" ForeColor="Red">*</asp:label></td>
											<td width="35%"><asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase" id="txtPayee" tabIndex="7" runat="server"
													CssClass="textField" Width="230px" Enabled="true" MaxLength="100">COLLECTOR OF CUSTOMS</asp:textbox></td>
										</tr>
										<tr>
											<td><asp:label style="Z-INDEX: 0" id="Label7" runat="server" CssClass="tableLabel">Status</asp:label></td>
											<td colSpan="3"><asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea" id="txtStatus"
													tabIndex="14" runat="server" CssClass="textField" Width="114px" Enabled="False" MaxLength="100" ReadOnly="True"></asp:textbox></td>
										</tr>
										<tr>
											<td colSpan="4">
												<fieldset><legend style="COLOR: black">Approval</legend>
													<table style="MARGIN: 5px; WIDTH: 672px; HEIGHT: 26px" id="table_approval">
														<tbody>
															<tr>
																<td style="WIDTH: 111px"><asp:label style="Z-INDEX: 0" id="Label8" runat="server" CssClass="tableLabel">Cheque Number</asp:label></td>
																<td style="WIDTH: 238px"><asp:textbox style="Z-INDEX: 0" id="txtChequeNumber" tabIndex="8" onkeypress="validate(event)"
																		onpaste="AfterPasteNonNumber(this)" runat="server" CssClass="textField"></asp:textbox></td>
																<td style="WIDTH: 87px"><asp:label style="Z-INDEX: 0" id="Label10" runat="server" CssClass="tableLabel">Date Issued</asp:label></td>
																<td><cc1:mstextbox id="txtDateIssued" tabIndex="9" runat="server" CssClass="textField" Width="136px"
																		Enabled="False" TextMaskType="msDate" MaxLength="12" TextMaskString="99/99/9999"></cc1:mstextbox></td>
															</tr>
														</tbody>
													</table>
												</fieldset>
											</td>
										</tr>
										<tr>
											<td colSpan="4">
												<fieldset><legend style="COLOR: black">Receipt</legend>
													<table style="MARGIN: 5px" id="table_receipt_number">
														<tbody>
															<tr>
																<td style="WIDTH: 111px"><asp:label style="Z-INDEX: 0" id="Label12" runat="server" CssClass="tableLabel">Receipt Number</asp:label></td>
																<td style="WIDTH: 238px"><asp:textbox style="Z-INDEX: 0" id="txtReceiptNumber" tabIndex="10" onkeypress="ValidateReceiptNumber(event)"
																		onpaste="AfterPasteReceiptNumber(this)" runat="server" MaxLength="40" CssClass="textField"></asp:textbox></td>
																<td style="WIDTH: 87px"><asp:label style="Z-INDEX: 0" id="Label14" runat="server" CssClass="tableLabel">Receipt Date</asp:label></td>
																<td><cc1:mstextbox id="txtReceiptDate" tabIndex="11" runat="server" CssClass="textField" Width="136px"
																		Enabled="False" TextMaskType="msDate" MaxLength="12" TextMaskString="99/99/9999"></cc1:mstextbox></td>
															</tr>
														</tbody>
													</table>
												</fieldset>
											</td>
										</tr>
										<tr>
											<td colSpan="4">
												<fieldset><legend style="COLOR: black">History</legend>
													<table style="MARGIN: 5px" id="table_receipt_Historty">
														<tbody>
															<tr>
																<td style="WIDTH: 111px"><asp:label style="Z-INDEX: 0" id="Label16" runat="server" CssClass="tableLabel">Requested by</asp:label></td>
																<td style="WIDTH: 238px"><asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea" id="txtRequestedBy"
																		tabIndex="14" runat="server" CssClass="textField" Width="80px" Enabled="False" MaxLength="100" ReadOnly="True"></asp:textbox>&nbsp;
																	<asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea" id="txtRequestedDate"
																		tabIndex="14" runat="server" CssClass="textField" Width="142px" Enabled="False" MaxLength="100"
																		ReadOnly="True"></asp:textbox></td>
																<td style="WIDTH: 81px"><asp:label style="Z-INDEX: 0" id="Label18" runat="server" CssClass="tableLabel">Modified by</asp:label></td>
																<td><asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea" id="txtModifiedBy"
																		tabIndex="14" runat="server" CssClass="textField" Width="80px" Enabled="False" MaxLength="100"
																		ReadOnly="True"></asp:textbox>&nbsp;<asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea" id="txtModifiedDate"
																		tabIndex="14" runat="server" CssClass="textField" Width="142px" Enabled="False" MaxLength="100" ReadOnly="True"></asp:textbox>
																</td>
															</tr>
															<tr>
																<td style="WIDTH: 107px"><asp:label style="Z-INDEX: 0" id="Label17" runat="server" CssClass="tableLabel">Approved by</asp:label></td>
																<td style="WIDTH: 238px"><asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea" id="txtApprovedBy"
																		tabIndex="14" runat="server" CssClass="textField" Width="80px" Enabled="False" MaxLength="100" ReadOnly="True"></asp:textbox>&nbsp;
																	<asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea" id="txtApprovedDate"
																		tabIndex="14" runat="server" CssClass="textField" Width="142px" Enabled="False" MaxLength="100"
																		ReadOnly="True"></asp:textbox></td>
																<td style="WIDTH: 81px"><asp:label style="Z-INDEX: 0" id="Label19" runat="server" CssClass="tableLabel">Paid by</asp:label></td>
																<td><asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea" id="txtPaidBy"
																		tabIndex="14" runat="server" CssClass="textField" Width="80px" Enabled="False" MaxLength="100"
																		ReadOnly="True"></asp:textbox>&nbsp;<asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea" id="txtPaidDate"
																		tabIndex="14" runat="server" CssClass="textField" Width="142px" Enabled="False" MaxLength="100" ReadOnly="True"></asp:textbox>
																</td>
															</tr>
															<TR>
																<TD style="WIDTH: 107px"><asp:label style="Z-INDEX: 0" id="Label23" runat="server" CssClass="tableLabel">Cheque Exported</asp:label></TD>
																<TD style="WIDTH: 238px"><asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea" id="txtChequeExportedBy"
																		tabIndex="14" runat="server" CssClass="textField" Width="80px" Enabled="False" MaxLength="100" ReadOnly="True"></asp:textbox>&nbsp;
																	<asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea" id="txtChequeExportedDT"
																		tabIndex="14" runat="server" CssClass="textField" Width="142px" Enabled="False" MaxLength="100"
																		ReadOnly="True"></asp:textbox></TD>
																<TD style="WIDTH: 81px"></TD>
																<TD></TD>
															</TR>
														</tbody>
													</table>
												</fieldset>
											</td>
										</tr>
										<tr>
											<td colSpan="4"><asp:button id="btnSearchJobs" tabIndex="10" runat="server" CssClass="queryButton" CausesValidation="False"
													Text="Search Jobs" Enabled="False"></asp:button>&nbsp;
												<asp:label style="Z-INDEX: 0" id="Label20" runat="server" CssClass="tableLabel">Maximum Invoices</asp:label>&nbsp;
												<asp:textbox style="Z-INDEX: 0; TEXT-ALIGN: right; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea"
													id="txtMaximumInvoices" tabIndex="14" runat="server" CssClass="textField" Width="30px" Enabled="False"
													MaxLength="100" ReadOnly="True"></asp:textbox>&nbsp;
												<asp:label style="Z-INDEX: 0" id="Label21" runat="server" CssClass="tableLabel">Remaining Invoices</asp:label>&nbsp;
												<asp:textbox style="Z-INDEX: 0; TEXT-ALIGN: right; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea"
													id="txtRemainingInvoices" tabIndex="14" runat="server" CssClass="textField" Width="30px"
													Enabled="False" MaxLength="100" ReadOnly="True"></asp:textbox>&nbsp;
												<asp:label style="Z-INDEX: 0" id="Label22" runat="server" CssClass="tableLabel">Total Requisition Amount</asp:label>&nbsp;
												<cc1:mstextbox style="Z-INDEX: 0; TEXT-ALIGN: right; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea"
													id="txtTotalRequisitionAmount" onpaste="AfterPasteNonNumber(this)" CssClass="textField" Width="150px"
													Enabled="False" NumberPrecision="6" NumberMinValue="0" TextMaskType="msNumeric" MaxLength="20"
													NumberScale="2" NumberMaxValue="9999" Runat="server"></cc1:mstextbox>&nbsp;
											</td>
										</tr>
										<tr>
											<td colSpan="4"><STRONG class="gridHeading"><FONT style="WIDTH: 100%; HEIGHT: 16px" size="2">Cheque 
														Requisition Details</FONT></STRONG></td>
										</tr>
										<tr>
											<td colSpan="4"><asp:label id="lblGridError" runat="server" Font-Bold="True" Font-Size="X-Small" ForeColor="Red"></asp:label><asp:datagrid style="Z-INDEX: 0" id="gvCheqRequisitionN" tabIndex="39" runat="server" Width="980px"
													AutoGenerateColumns="False" AlternatingItemStyle-CssClass="gridField" ItemStyle-CssClass="gridField" HeaderStyle-CssClass="gridHeading" CellPadding="0" CellSpacing="1" BorderWidth="0px" FooterStyle-CssClass="gridHeading"
													ShowFooter="True">
													<FooterStyle CssClass="gridHeading"></FooterStyle>
													<SelectedItemStyle CssClass="gridFieldSelected"></SelectedItemStyle>
													<AlternatingItemStyle CssClass="gridField"></AlternatingItemStyle>
													<ItemStyle Height="15px" CssClass="gridField"></ItemStyle>
													<HeaderStyle Font-Bold="True" HorizontalAlign="Center" Height="40px" Width="2%" CssClass="gridHeading"></HeaderStyle>
													<Columns>
														<asp:TemplateColumn>
															<HeaderStyle HorizontalAlign="Center" Width="10%"></HeaderStyle>
															<ItemStyle HorizontalAlign="Center"></ItemStyle>
															<ItemTemplate>
																<asp:LinkButton id="btnDeleteItem" Runat="server" Text="<img src='images/butt-delete.gif' alt='delete' border=0>"
																	CommandName="DELETE_ITEM" />
															</ItemTemplate>
															<FooterStyle HorizontalAlign="Center"></FooterStyle>
															<FooterTemplate>
																<asp:LinkButton id="btnAddItem" Runat="server" Text="<img src='images/butt-add.gif' alt='add' border=0>"
																	CommandName="ADD_ITEM" />
															</FooterTemplate>
														</asp:TemplateColumn>
														<asp:TemplateColumn HeaderText="Invoice No">
															<HeaderStyle HorizontalAlign="Center" Width="15%"></HeaderStyle>
															<ItemStyle HorizontalAlign="Center"></ItemStyle>
															<ItemTemplate>
																<asp:Label ID="lblInvoice_No" Visible=True Text='<%#DataBinder.Eval(Container.DataItem,"Invoice_No")%>' Runat="server">
																</asp:Label>
																<asp:Label ID="lblPayerid" Text='<%#DataBinder.Eval(Container.DataItem,"payerid")%>' Visible="False" Runat="server">
																</asp:Label>
																<asp:Label ID="lblConsignment_no" Text='<%#DataBinder.Eval(Container.DataItem,"consignment_no")%>' Visible="False" Runat="server">
																</asp:Label>
																<asp:Label ID="lblRequisition_no" Text='<%#DataBinder.Eval(Container.DataItem,"requisition_no")%>' Visible="False" Runat="server">
																</asp:Label>
															</ItemTemplate>
															<FooterStyle HorizontalAlign="Center"></FooterStyle>
															<FooterTemplate>
																<cc1:mstextbox CssClass="gridTextBoxNumber" ID="txtAddInvoice_No" onpaste="AfterPasteNonNumber(this)"
																	TabIndex="12" Runat="server" Enabled="True" TextMaskType="msNumericCOD" MaxLength="19" NumberMaxValueCOD="9223372036854775807"
																	NumberMinValue="0" width="90px" NumberPrecision="19"></cc1:mstextbox>
															</FooterTemplate>
														</asp:TemplateColumn>
														<asp:TemplateColumn HeaderText="Client">
															<HeaderStyle HorizontalAlign="Center" Width="30%"></HeaderStyle>
															<ItemStyle HorizontalAlign="Center"></ItemStyle>
															<ItemTemplate>
																<asp:Label ID="lblCustomerName" Visible="True" Text='<%#DataBinder.Eval(Container.DataItem,"CustomerName")%>' Runat="server">
																</asp:Label>
															</ItemTemplate>
														</asp:TemplateColumn>
														<asp:TemplateColumn HeaderText="Duty/Tax">
															<HeaderStyle HorizontalAlign="Center" Width="15%"></HeaderStyle>
															<ItemStyle HorizontalAlign="Center"></ItemStyle>
															<ItemTemplate>
																<asp:Label ID="lblDutyTax" Visible="True" Runat="server"></asp:Label>
															</ItemTemplate>
														</asp:TemplateColumn>
														<asp:TemplateColumn HeaderText="EPF">
															<HeaderStyle HorizontalAlign="Center" Width="15%"></HeaderStyle>
															<ItemStyle HorizontalAlign="Center"></ItemStyle>
															<ItemTemplate>
																<asp:Label ID="lblEntryProcessingFee" Visible="True" Runat="server"></asp:Label>
															</ItemTemplate>
														</asp:TemplateColumn>
														<asp:TemplateColumn HeaderText="Total">
															<HeaderStyle HorizontalAlign="Center" Width="15%"></HeaderStyle>
															<ItemStyle HorizontalAlign="Center"></ItemStyle>
															<ItemTemplate>
																<asp:Label ID="lblDisbursementTotal" Visible=True Text='<%#DataBinder.Eval(Container.DataItem,"DisbursementTotal","{0:###,##0.00}")%>' Runat="server">
																</asp:Label>
															</ItemTemplate>
														</asp:TemplateColumn>
													</Columns>
													<PagerStyle NextPageText="Next" PrevPageText="Previous" HorizontalAlign="Right" CssClass="normalText"></PagerStyle>
												</asp:datagrid><asp:datagrid style="Z-INDEX: 0" id="gvCheqRequisitionA" tabIndex="39" runat="server" Width="980px"
													AutoGenerateColumns="False" AlternatingItemStyle-CssClass="gridField" ItemStyle-CssClass="gridField" HeaderStyle-CssClass="gridHeading"
													CellPadding="0" CellSpacing="1" BorderWidth="0px" FooterStyle-CssClass="gridHeading" ShowFooter="True">
													<FooterStyle CssClass="gridHeading"></FooterStyle>
													<SelectedItemStyle CssClass="gridFieldSelected"></SelectedItemStyle>
													<AlternatingItemStyle CssClass="gridField"></AlternatingItemStyle>
													<ItemStyle Height="15px" CssClass="gridField"></ItemStyle>
													<HeaderStyle Font-Bold="True" HorizontalAlign="Center" Height="40px" Width="2%" CssClass="gridHeading"></HeaderStyle>
													<Columns>
														<asp:TemplateColumn>
															<HeaderStyle HorizontalAlign="Center" Width="10%"></HeaderStyle>
															<ItemStyle HorizontalAlign="Center"></ItemStyle>
															<ItemTemplate>
																<asp:LinkButton id="btnEditItemA" Runat="server" Text="<img src='images/butt-edit.gif' alt='edit' border=0>"
																	CommandName="EDIT_ITEM" />
																<asp:LinkButton id="btnDeleteItemA" Runat="server" Text="<img src='images/butt-delete.gif' alt='delete' border=0>"
																	CommandName="DELETE_ITEM" />
															</ItemTemplate>
															<FooterStyle HorizontalAlign="Center"></FooterStyle>
															<FooterTemplate>
																<asp:LinkButton id="btnAddItemA" Runat="server" Text="<img src='images/butt-add.gif' alt='add' border=0>"
																	CommandName="ADD_ITEM" />
															</FooterTemplate>
															<EditItemTemplate>
																<asp:LinkButton id="btnSaveA" Runat="server" Text="<img src='images/butt-update.gif' alt='save' border=0>"
																	CommandName="SAVE_ITEM" />
																<asp:LinkButton id="btnCancelA" Runat="server" Text="<img src='images/butt-red.gif' alt='cancel' border=0>"
																	CommandName="CANCEL_ITEM" />
															</EditItemTemplate>
														</asp:TemplateColumn>
														<asp:TemplateColumn HeaderText="Invoice No">
															<HeaderStyle HorizontalAlign="Center" Width="15%"></HeaderStyle>
															<ItemStyle HorizontalAlign="Center"></ItemStyle>
															<ItemTemplate>
																<asp:Label ID="lblInvoice_NoA" Visible=True Text='<%#DataBinder.Eval(Container.DataItem,"Invoice_No")%>' Runat="server">
																</asp:Label>
																<asp:Label ID="lblPayeridA" Text='<%#DataBinder.Eval(Container.DataItem,"payerid")%>' Visible="False" Runat="server">
																</asp:Label>
																<asp:Label ID="lblConsignment_noA" Text='<%#DataBinder.Eval(Container.DataItem,"consignment_no")%>' Visible="False" Runat="server">
																</asp:Label>
																<asp:Label ID="lblRequisition_noA" Text='<%#DataBinder.Eval(Container.DataItem,"requisition_no")%>' Visible="False" Runat="server">
																</asp:Label>
															</ItemTemplate>
															<FooterStyle HorizontalAlign="Center"></FooterStyle>
															<FooterTemplate>
																<cc1:mstextbox CssClass="gridTextBoxNumber" ID="txtAddInvoice_NoA" onpaste="AfterPasteNonNumber(this)"
																	TabIndex="12" Runat="server" Enabled="True" TextMaskType="msNumericCOD" MaxLength="19" NumberMaxValueCOD="9223372036854775807"
																	NumberMinValue="0" width="90px" NumberPrecision="19"></cc1:mstextbox>
															</FooterTemplate>
															<EditItemTemplate>
																<cc1:mstextbox CssClass="gridTextBoxNumber" ID="txtEditInvoice_NoA" onpaste="AfterPasteNonNumber(this)" TabIndex="12" Runat="server" Enabled="True" Text='<%#DataBinder.Eval(Container.DataItem,"Invoice_No")%>' TextMaskType="msNumericCOD" MaxLength="19" NumberMaxValueCOD="9223372036854775807" NumberMinValue="0" width="90px" NumberPrecision="19">
																</cc1:mstextbox>
																<asp:Label ID="lbllblPayeridEditA" Text='<%#DataBinder.Eval(Container.DataItem,"payerid")%>' Visible="False" Runat="server">
																</asp:Label>
																<asp:Label ID="lblConsignment_noEditA" Text='<%#DataBinder.Eval(Container.DataItem,"consignment_no")%>' Visible="False" Runat="server">
																</asp:Label>
																<asp:Label ID="lblRequisition_noEditA" Text='<%#DataBinder.Eval(Container.DataItem,"requisition_no")%>' Visible="False" Runat="server">
																</asp:Label>
															</EditItemTemplate>
														</asp:TemplateColumn>
														<asp:TemplateColumn HeaderText="Client">
															<HeaderStyle HorizontalAlign="Center" Width="45%"></HeaderStyle>
															<ItemStyle HorizontalAlign="Center"></ItemStyle>
															<ItemTemplate>
																<asp:Label ID="lblCustomerNameA" Visible=True Text='<%#DataBinder.Eval(Container.DataItem,"CustomerName")%>' Runat="server">
																</asp:Label>
															</ItemTemplate>
														</asp:TemplateColumn>
														<asp:TemplateColumn HeaderText="Disbursement">
															<HeaderStyle HorizontalAlign="Center" Width="15%"></HeaderStyle>
															<ItemStyle HorizontalAlign="Center"></ItemStyle>
															<ItemTemplate>
																<asp:Label ID="lblDisbursementType" Visible=True Text='<%#DataBinder.Eval(Container.DataItem,"DisbursementType")%>' Runat="server">
																</asp:Label>
															</ItemTemplate>
															<FooterStyle HorizontalAlign="Center"></FooterStyle>
															<FooterTemplate>
																<asp:textbox id="txtAddDisbursementTypeA" TabIndex="13" runat="server" onkeypress="ValidateDigitsAndLetters(event)"
																	onpaste="AfterPasteDigitsAndLetters(this)" CssClass="textField" MaxLength="12" Width="120px"></asp:textbox>
															</FooterTemplate>
															<EditItemTemplate>
																<asp:textbox id="txtEditDisbursementTypeA" TabIndex=13 runat="server" onkeypress="ValidateDigitsAndLetters(event)" onpaste="AfterPasteDigitsAndLetters(this)" CssClass="textField" MaxLength="12" Width="200px" Text='<%#DataBinder.Eval(Container.DataItem,"DisbursementType")%>'>
																</asp:textbox>
															</EditItemTemplate>
														</asp:TemplateColumn>
														<asp:TemplateColumn HeaderText="Amount">
															<HeaderStyle HorizontalAlign="Center" Width="15%"></HeaderStyle>
															<ItemStyle HorizontalAlign="Center"></ItemStyle>
															<ItemTemplate>
																<asp:Label ID="lblOtherDisbursementA" Visible="True" Runat="server"></asp:Label>
															</ItemTemplate>
															<FooterStyle HorizontalAlign="Center"></FooterStyle>
															<FooterTemplate>
																<cc1:mstextbox CssClass="gridTextBoxNumber" ID="txtAddAmountA" onpaste="AfterPasteNonNumber(this)"
																	Runat="server" Enabled="True" TextMaskType="msNumeric" MaxLength="14" NumberMaxValue="999999"
																	NumberMinValue="0" NumberPrecision="14" TabIndex="14" width="150px" NumberScale="2"></cc1:mstextbox>
															</FooterTemplate>
															<EditItemTemplate>
																<cc1:mstextbox CssClass="gridTextBoxNumber" ID="txtEditAmountA" onpaste="AfterPasteNonNumber(this)"
																	Runat="server" Enabled="True" TextMaskType="msNumeric" MaxLength="14" NumberMaxValue="999999"
																	NumberMinValue="0" width="150px" TabIndex="14" NumberPrecision="14"></cc1:mstextbox>
															</EditItemTemplate>
														</asp:TemplateColumn>
													</Columns>
													<PagerStyle NextPageText="Next" PrevPageText="Previous" HorizontalAlign="Right" CssClass="normalText"></PagerStyle>
												</asp:datagrid><asp:datagrid style="Z-INDEX: 0" id="gvCheqRequisitionFormal" tabIndex="39" runat="server" Width="980px"
													AutoGenerateColumns="False" AlternatingItemStyle-CssClass="gridField" ItemStyle-CssClass="gridField" HeaderStyle-CssClass="gridHeading"
													CellPadding="0" CellSpacing="1" BorderWidth="0px" FooterStyle-CssClass="gridHeading" ShowFooter="True">
													<FooterStyle CssClass="gridHeading"></FooterStyle>
													<SelectedItemStyle CssClass="gridFieldSelected"></SelectedItemStyle>
													<AlternatingItemStyle CssClass="gridField"></AlternatingItemStyle>
													<ItemStyle Height="15px" CssClass="gridField"></ItemStyle>
													<HeaderStyle Font-Bold="True" HorizontalAlign="Center" Height="40px" Width="2%" CssClass="gridHeading"></HeaderStyle>
													<Columns>
														<asp:TemplateColumn>
															<HeaderStyle HorizontalAlign="Center" Width="9%"></HeaderStyle>
															<ItemStyle HorizontalAlign="Center"></ItemStyle>
															<ItemTemplate>
																<asp:LinkButton id="btnDeleteItemFormal" Runat="server" Text="<img src='images/butt-delete.gif' alt='delete' border=0>"
																	CommandName="DELETE_ITEM" />
															</ItemTemplate>
															<FooterStyle HorizontalAlign="Center"></FooterStyle>
															<FooterTemplate>
																<asp:LinkButton id="btnAddItemFormal" Runat="server" Text="<img src='images/butt-add.gif' alt='add' border=0>"
																	CommandName="ADD_ITEM" />
															</FooterTemplate>
															<EditItemTemplate>
																<asp:LinkButton id="Linkbutton1" Runat="server" Text="<img src='images/butt-update.gif' alt='save' border=0>"
																	CommandName="SAVE_ITEM" />
																<asp:LinkButton id="Linkbutton2" Runat="server" Text="<img src='images/butt-red.gif' alt='cancel' border=0>"
																	CommandName="CANCEL_ITEM" />
															</EditItemTemplate>
														</asp:TemplateColumn>
														<asp:TemplateColumn HeaderText="Invoice No">
															<HeaderStyle HorizontalAlign="Center" Width="10%"></HeaderStyle>
															<ItemStyle HorizontalAlign="Center"></ItemStyle>
															<ItemTemplate>
																<asp:Label ID="lblInvoice_NoFormal" Visible=True Text='<%#DataBinder.Eval(Container.DataItem,"Invoice_No")%>' Runat="server">
																</asp:Label>
																<asp:Label ID="lblPayeridFormal" Text='<%#DataBinder.Eval(Container.DataItem,"payerid")%>' Visible="False" Runat="server">
																</asp:Label>
																<asp:Label ID="lblConsignment_noFormal" Text='<%#DataBinder.Eval(Container.DataItem,"consignment_no")%>' Visible="False" Runat="server">
																</asp:Label>
																<asp:Label ID="lblRequisition_noFormal" Text='<%#DataBinder.Eval(Container.DataItem,"requisition_no")%>' Visible="False" Runat="server">
																</asp:Label>
															</ItemTemplate>
															<FooterStyle HorizontalAlign="Center"></FooterStyle>
															<FooterTemplate>
																<cc1:mstextbox CssClass="gridTextBoxNumber" ID="txtAddInvoice_NoFormal" onpaste="AfterPasteNonNumber(this)"
																	TabIndex="12" Runat="server" Enabled="True" TextMaskType="msNumericCOD" MaxLength="19" NumberMaxValueCOD="9223372036854775807"
																	NumberMinValue="0" width="90px" NumberPrecision="19"></cc1:mstextbox>
															</FooterTemplate>
														</asp:TemplateColumn>
														<asp:TemplateColumn HeaderText="Client">
															<HeaderStyle HorizontalAlign="Center" Width="25%"></HeaderStyle>
															<ItemStyle HorizontalAlign="Center"></ItemStyle>
															<ItemTemplate>
																<asp:Label ID="lblCustomerNameFormal" Text='<%#DataBinder.Eval(Container.DataItem,"CustomerName")%>' Visible="True" Runat="server">
																</asp:Label>
															</ItemTemplate>
														</asp:TemplateColumn>
														<asp:TemplateColumn HeaderText="Assessment &lt;br/&gt;Number">
															<HeaderStyle HorizontalAlign="Center" Width="10%"></HeaderStyle>
															<ItemStyle HorizontalAlign="Center"></ItemStyle>
															<ItemTemplate>
																<asp:Label ID="lblAssessmentNumberFormal" Visible="True" Runat="server"></asp:Label>
															</ItemTemplate>
															<FooterStyle HorizontalAlign="Center"></FooterStyle>
															<FooterTemplate>
																<asp:textbox id="txtAssessmentNumberAdd" style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase" runat="server"
																	CssClass="textField" Width="80px" Enabled="True" TabIndex="13" MaxLength="20"></asp:textbox>
															</FooterTemplate>
														</asp:TemplateColumn>
														<asp:TemplateColumn HeaderText="Assessment &lt;br/&gt;Amount">
															<HeaderStyle HorizontalAlign="Center" Width="10%"></HeaderStyle>
															<ItemStyle HorizontalAlign="Center"></ItemStyle>
															<ItemTemplate>
																<asp:Label ID="lblAssessmentAmountFormal" Visible="True" Runat="server"></asp:Label>
															</ItemTemplate>
															<FooterStyle HorizontalAlign="Center"></FooterStyle>
															<FooterTemplate>
																<cc1:mstextbox CssClass="gridTextBoxNumber" ID="txtAssessmentAmountAdd" onpaste="AfterPasteNonNumber(this)"
																	Runat="server" Enabled="True" TextMaskType="msNumericCOD" MaxLength="14" NumberMaxValueCOD="99999999.99"
																	NumberMinValue="0" NumberPrecision="14" width="90px" TabIndex="14" NumberScale="2"></cc1:mstextbox>
															</FooterTemplate>
														</asp:TemplateColumn>
														<asp:TemplateColumn HeaderText="Assessment &lt;br/&gt;Date">
															<HeaderStyle HorizontalAlign="Center" Width="10%"></HeaderStyle>
															<ItemStyle HorizontalAlign="Center"></ItemStyle>
															<ItemTemplate>
																<asp:Label ID="lblAssessmentDateFormal" Visible="True" Runat="server"></asp:Label>
															</ItemTemplate>
															<FooterStyle HorizontalAlign="Center"></FooterStyle>
															<FooterTemplate>
																<cc1:mstextbox id="txtAssessmentDateAdd" tabIndex="15" runat="server" Width="136px" CssClass="textField"
																	Enabled="true" MaxLength="12" TextMaskType="msDate" TextMaskString="99/99/9999"></cc1:mstextbox>
															</FooterTemplate>
														</asp:TemplateColumn>
														<asp:TemplateColumn HeaderText="EPF">
															<HeaderStyle HorizontalAlign="Center" Width="8%"></HeaderStyle>
															<ItemStyle HorizontalAlign="Center"></ItemStyle>
															<ItemTemplate>
																<asp:Label ID="lblEntryProcessingFeeFormal" Visible="True" Runat="server"></asp:Label>
															</ItemTemplate>
														</asp:TemplateColumn>
														<asp:TemplateColumn HeaderText="Total">
															<HeaderStyle HorizontalAlign="Center" Width="13%"></HeaderStyle>
															<ItemStyle HorizontalAlign="Center"></ItemStyle>
															<ItemTemplate>
																<asp:Label ID="lblDisbursementTotalFormal" Visible=True Text='<%#DataBinder.Eval(Container.DataItem,"DisbursementTotal","{0:###,##0.00}")%>' Runat="server">
																</asp:Label>
															</ItemTemplate>
														</asp:TemplateColumn>
													</Columns>
													<PagerStyle NextPageText="Next" PrevPageText="Previous" HorizontalAlign="Right" CssClass="normalText"></PagerStyle>
												</asp:datagrid><asp:datagrid style="Z-INDEX: 0" id="gvITF" tabIndex="39" runat="server" Width="980px" AutoGenerateColumns="False"
													AlternatingItemStyle-CssClass="gridField" ItemStyle-CssClass="gridField" HeaderStyle-CssClass="gridHeading"
													CellPadding="0" CellSpacing="1" BorderWidth="0px" FooterStyle-CssClass="gridHeading" ShowFooter="True">
													<FooterStyle CssClass="gridHeading"></FooterStyle>
													<SelectedItemStyle CssClass="gridFieldSelected"></SelectedItemStyle>
													<AlternatingItemStyle CssClass="gridField"></AlternatingItemStyle>
													<ItemStyle Height="15px" CssClass="gridField"></ItemStyle>
													<HeaderStyle Font-Bold="True" HorizontalAlign="Center" Height="40px" Width="2%" CssClass="gridHeading"></HeaderStyle>
													<Columns>
														<asp:TemplateColumn>
															<HeaderStyle HorizontalAlign="Center" Width="5%"></HeaderStyle>
															<ItemStyle HorizontalAlign="Center"></ItemStyle>
															<ItemTemplate>
																<asp:LinkButton id="btnEditItemITF" Runat="server" Text="<img src='images/butt-edit.gif' alt='edit' border=0>"
																	CommandName="EDIT_ITEM" />
																<asp:LinkButton id="btnDeleteItemITF" Runat="server" Text="<img src='images/butt-delete.gif' alt='delete' border=0>"
																	CommandName="DELETE_ITEM" />
															</ItemTemplate>
															<FooterStyle HorizontalAlign="Center"></FooterStyle>
															<FooterTemplate>
																<asp:LinkButton id="btnAddItemITF" Runat="server" TabIndex="17" Text="<img src='images/butt-add.gif' alt='add' border=0>"
																	CommandName="ADD_ITEM" />
															</FooterTemplate>
															<EditItemTemplate>
																<asp:LinkButton id="lbSaveITF" TabIndex="20" Runat="server" Text="<img src='images/butt-update.gif' alt='save' border=0>"
																	CommandName="SAVE_ITEM" />
																<asp:LinkButton id="lbCancelITF" Runat="server" Text="<img src='images/butt-red.gif' alt='cancel' border=0>"
																	CommandName="CANCEL_ITEM" />
															</EditItemTemplate>
														</asp:TemplateColumn>
														<asp:TemplateColumn HeaderText="Master AWB Number">
															<HeaderStyle HorizontalAlign="Center" Width="10%"></HeaderStyle>
															<ItemStyle HorizontalAlign="Center"></ItemStyle>
															<ItemTemplate>
																<asp:Label ID="lblMasterAWBNumberITF" Visible="True" Text='<%#DataBinder.Eval(Container.DataItem,"consignment_no")%>' Runat="server">
																</asp:Label>
															</ItemTemplate>
															<FooterStyle HorizontalAlign="Center"></FooterStyle>
															<FooterTemplate>
																<%-- onkeypress="ValidateDigitsAWBNumber(event)" ValidateDigitsAWBNumber onkeypress="ValidateDigitsAndLetters(event)" onpaste="AfterPasteDigitsAndLetters(this)" --%>
																<asp:textbox id="txtAddMasterAWBNumberITF" MaxLength="30" TabIndex="13" runat="server" CssClass="textField"
																	Width="100%"></asp:textbox>
															</FooterTemplate>
															<EditItemTemplate>
																<asp:textbox id="txtEditMasterAWBNumberITF" TabIndex="17" runat="server" CssClass="textField" MaxLength="30" Width="200px" Text='<%#DataBinder.Eval(Container.DataItem,"consignment_no")%>'>
																</asp:textbox>
															</EditItemTemplate>
														</asp:TemplateColumn>
														<asp:TemplateColumn HeaderText="Ignore MAWB Validation">
															<HeaderStyle HorizontalAlign="Center" Width="7%"></HeaderStyle>
															<ItemStyle HorizontalAlign="Center"></ItemStyle>
															<ItemTemplate>
																<%--<asp:checkbox id="cbIgnoreMAWBValidation" tabIndex="20" Runat="server"></asp:checkbox>--%>
															</ItemTemplate>
															<FooterStyle HorizontalAlign="Center"></FooterStyle>
															<FooterTemplate>
																<asp:checkbox id="cbAddIgnoreMAWBValidation" TabIndex="14" Runat="server"></asp:checkbox>
															</FooterTemplate>
															<EditItemTemplate>
																<%--<asp:checkbox id="cbEditIgnoreMAWBValidation" tabIndex="20" Runat="server"></asp:checkbox>--%>
															</EditItemTemplate>
														</asp:TemplateColumn>
														<asp:TemplateColumn HeaderText="Folio Number">
															<HeaderStyle HorizontalAlign="Center" Width="5%"></HeaderStyle>
															<ItemStyle HorizontalAlign="Center"></ItemStyle>
															<ItemTemplate>
																<asp:Label ID="lbFolioNo" Visible="True" Runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"FolioNumber")%>'>
																</asp:Label>
															</ItemTemplate>
														</asp:TemplateColumn>
														<asp:TemplateColumn HeaderText="ITF Amount">
															<HeaderStyle HorizontalAlign="Center" Width="10%"></HeaderStyle>
															<ItemStyle HorizontalAlign="Center"></ItemStyle>
															<ItemTemplate>
																<asp:Label ID="lblITFAmount" Visible="True" Runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"ITFAmount","{0:###,##0.00}")%>'>
																</asp:Label>
															</ItemTemplate>
															<FooterStyle HorizontalAlign="Center"></FooterStyle>
															<FooterTemplate>
																<cc1:mstextbox CssClass="gridTextBoxNumber" ID="txtAddITFAmount" onpaste="AfterPasteNonNumber(this)"
																	Runat="server" Enabled="True" TextMaskType="msNumericCOD" MaxLength="14" NumberMaxValueCOD="99999999.99"
																	NumberMinValue="0" NumberPrecision="14" width="120px" TabIndex="15" NumberScale="2"></cc1:mstextbox>
															</FooterTemplate>
															<EditItemTemplate>
																<cc1:mstextbox CssClass="gridTextBoxNumber" ID="txtEditITFAmount" onpaste="AfterPasteNonNumber(this)" Runat="server" Enabled="True" TextMaskType="msNumericCOD" MaxLength="14" NumberMaxValueCOD="99999999.99" NumberMinValue="0" NumberPrecision="14" width="120px" TabIndex="18" NumberScale="2" Text='<%#DataBinder.Eval(Container.DataItem,"ITFAmount","{0:###,##0.00}")%>'>
																</cc1:mstextbox>
															</EditItemTemplate>
														</asp:TemplateColumn>
														<asp:TemplateColumn HeaderText="Freight Collect">
															<HeaderStyle HorizontalAlign="Center" Width="10%"></HeaderStyle>
															<ItemStyle HorizontalAlign="Center"></ItemStyle>
															<ItemTemplate>
																<asp:Label ID="lblFreightColloct" Visible="True" Runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"FreightCollectAmount","{0:###,##0.00}")%>'>
																</asp:Label>
															</ItemTemplate>
															<FooterStyle HorizontalAlign="Center"></FooterStyle>
															<FooterTemplate>
																<cc1:mstextbox CssClass="gridTextBoxNumber" ID="txtAddFreightColloct" onpaste="AfterPasteNonNumber(this)"
																	Runat="server" Enabled="True" TextMaskType="msNumericCOD" MaxLength="14" NumberMaxValueCOD="99999999.99"
																	NumberMinValue="0" NumberPrecision="14" width="120px" TabIndex="16" NumberScale="2"></cc1:mstextbox>
															</FooterTemplate>
															<EditItemTemplate>
																<cc1:mstextbox CssClass="gridTextBoxNumber" ID="txtEditFreightColloct" onpaste="AfterPasteNonNumber(this)" Runat="server" Enabled="True" TextMaskType="msNumericCOD" MaxLength="14" NumberMaxValueCOD="99999999.99" NumberMinValue="0" NumberPrecision="14" width="120px" TabIndex="19" NumberScale="2" Text='<%#DataBinder.Eval(Container.DataItem,"FreightCollectAmount","{0:###,##0.00}")%>'>
																</cc1:mstextbox>
															</EditItemTemplate>
														</asp:TemplateColumn>
														<asp:TemplateColumn HeaderText="Total">
															<HeaderStyle HorizontalAlign="Center" Width="10%"></HeaderStyle>
															<ItemStyle HorizontalAlign="Center"></ItemStyle>
															<ItemTemplate>
																<asp:Label ID="lblITFTotal" Visible="True" Text='<%#DataBinder.Eval(Container.DataItem,"DisbursementTotal","{0:###,##0.00}")%>' Runat="server">
																</asp:Label>
															</ItemTemplate>
														</asp:TemplateColumn>
													</Columns>
													<PagerStyle NextPageText="Next" PrevPageText="Previous" HorizontalAlign="Right" CssClass="normalText"></PagerStyle>
												</asp:datagrid></td>
										</tr>
										<tr>
											<td colSpan="4" align="right"><FONT face="Tahoma"></FONT></td>
										</tr>
									</tbody>
								</table>
							</td>
						</tr>
					</tbody>
				</TABLE>
			</div>
			<INPUT 
value="<%=strScrollPosition%>" type=hidden name=ScrollPosition> <INPUT type="hidden" name="hdnRefresh">
		</form>
		</TR></TBODY></TABLE></TR></TBODY></TABLE></TR></TBODY></TABLE>
		<DIV></DIV>
		</FORM></TR></TBODY></TABLE></TR></TBODY></TABLE></TR></TBODY></TABLE>
		<DIV></DIV>
		</FORM>
	</body>
</HTML>
