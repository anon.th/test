<%@ Import Namespace="com.common.util" %>
<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<%@ Page language="c#" Codebehind="SWB_ScanPkgs.aspx.cs" AutoEventWireup="false" Inherits="TIES.WebUI.Manifest.SWB_ScanPkgs" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Scanning Workbench Emulator</title> 
		<!--<style type="text/css">.scrollable { BORDER-RIGHT: silver 1px solid; BORDER-TOP: silver 1px solid; OVERFLOW: scroll; BORDER-LEFT: silver 1px solid; WIDTH: 70px; BORDER-BOTTOM: silver 1px solid; HEIGHT: 80px }
	.scrollable SELECT { BORDER-RIGHT: medium none; BORDER-TOP: medium none; BORDER-LEFT: medium none; BORDER-BOTTOM: medium none }
	.style1 { FONT-FAMILY: "Courier New" }
	</style>-->
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK rel="stylesheet" type="text/css" href="../css/Styles.css">
		<script language="javascript" src="../Scripts/settingScrollPosition.js"></script>
		<!--#INCLUDE FILE="../msFormValidations.inc"-->
		<script language="javascript" type="text/javascript">
			function callCodeBehind()
			{
				var formObject = document.SWB_ScanPkgs
				var now = new Date();
				var lblErrrMsg =document.getElementById("lblErrrMsg");			
				if(formObject.txtScanerID.value=="")
				{
					formObject.txtScanerID.value= document.getElementById("lblUserIDDisplay").innerHTML;
				}
				var txtItem = formObject.txtLastestScan.value + JsGenSpace(32-formObject.txtLastestScan.value.length);
				txtItem += formatDate(new Date(), "dd/mm/yyyy hh:nn:ss")+JsGenSpace(22-formatDate(new Date(), "dd/mm/yyyy hh:nn:ss").length);
				txtItem += formObject.txtScanerID.value;				
				addOption(formObject.lbScanningPackages,txtItem,"-1");
								
				var btn = document.getElementById('BtnCallCodeBehind');
				if(btn != null)
				{					
					btn.click();

				}else{
					alert('error call code behind');
				}
			}
			
			function LastestScanFocus()
			{
					document.SWB_ScanPkgs.txtLastestScan.focus();
			}
				
			function addItem() {
				var formObject = document.SWB_ScanPkgs
				var now = new Date();
				var lblErrrMsg =document.getElementById("lblErrrMsg");

				if (formObject.btnScanComplete.value!="Select Batch")
				{
					lblErrrMsg.innerHTML="";
					if (formObject.txtLastestScan.value!="") 
					{
    					var txtFirstDate =document.getElementById("txtDate");
    					var txtBeginScanDT =document.getElementById("txtBeginScanDT");
    					var txtEndScanDT =document.getElementById("txtEndScanDT");
    					var txtBatch_No =document.getElementById("txtBatch_No");
						
						if(!VerifiyScannedPkg(formObject.txtLastestScan.value))
						{
								var charCode = formObject.txtLastestScan.value.charCodeAt(0);					
								if(charCode == 36)
								{
									//lblErrrMsg.innerHTML= "";
									callCodeBehind();
									formObject.txtLastestScan.value="";
									formObject.txtScanerID.value="";
									txtFirstDate.value="";
									formObject.txtLastestScan.focus();
									
									return true;
								}else{
									formObject.txtLastestScan.value="";
									formObject.txtScanerID.value="";
									txtFirstDate.value="";
									formObject.txtLastestScan.focus();
								}					
						}else{
						
								//generate text for listbox and value
								if(formObject.txtScanerID.value=="")
									{
										formObject.txtScanerID.value= document.getElementById("lblUserIDDisplay").innerHTML;
									}
								var txtItem = formObject.txtLastestScan.value + JsGenSpace(32-formObject.txtLastestScan.value.length);
								txtItem += formatDate(new Date(), "dd/mm/yyyy hh:nn:ss")+JsGenSpace(22-formatDate(new Date(), "dd/mm/yyyy hh:nn:ss").length);
								txtItem += formObject.txtScanerID.value;
								//txtItem='123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890';
								var txtVal =formObject.txtLastestScan.value+'%'+ formatDate(new Date(), "dd/mm/yyyy hh:nn:ss") +'%' + formObject.txtScanerID.value;
								
								//addOption(formObject.lbScanningPackages,formObject.txtLastestScan.value,formatDate(new Date(), "d-mm-yyyy hh:nn:ss"))

								txtFirstDate.value = formatDate(new Date(), "dd/mm/yyyy hh:nn:ss");
								if(txtBeginScanDT.value=="")
								{
									txtBeginScanDT.value = txtFirstDate.value;
								}
								txtEndScanDT.value = txtFirstDate.value ;
								if(txtBatch_No.value=="")
								{
									txtBatch_No.value=document.getElementById("lblBatchNoDisplay").innerHTML
								}
								addOption(formObject.lbScanningPackages,txtItem,txtVal);
								//if(document.getElementById("txtAjaxState").value=="");
								//{
								//	doCallAjax('ADD');
								//}
								doCallAjax('ADD');

								
						        
								//var txtLastScan =document.getElementById("Text3");
								var lblCnt =document.getElementById("lblScanCountDisplay");
								//txtLastScan.value =formObject.Text1.value;
								var cnt =parseInt(lblCnt.innerHTML);
						        
								lblCnt.innerHTML =  cnt+1;
								////if(txtFirstDate.value=="")
								//// {
								//txtFirstDate.value = formatDate(new Date(), "dd/mm/yyyy hh:nn:ss");
							//// }
						        
								formObject.txtLastestScan.value="";
								formObject.txtScanerID.value="";
								txtFirstDate.value="";
								formObject.txtLastestScan.focus();
						}

					} 
					else
					{
						//alert("Fill form and click Add")
					}

				}
				else
				{
					var charCode = formObject.txtLastestScan.value.charCodeAt(0);		
					if(charCode == 36)
					{
						//lblErrrMsg.innerHTML= "";
						callCodeBehind();
						formObject.txtLastestScan.value="";
						formObject.txtScanerID.value="";
						txtFirstDate.value="";
						formObject.txtLastestScan.focus();
						
						return true;
					}else{
						lblErrrMsg.innerHTML="Please select a batch before scanning";
									formObject.txtLastestScan.value="";
									formObject.txtScanerID.value="";
									txtFirstDate.value="";
									formObject.txtLastestScan.focus();
						return false;
					}		
				} 
			   
			} 


			function addOption(selectObject,optionText,optionValue) {
				var optionObject = new Option(optionText,optionValue)
				var optionRank = selectObject.options.length
				selectObject.options[optionRank]=optionObject
			}

			function stopRKey(evt) {
				var evt = (evt) ? evt : ((event) ? event : null);
				var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);
				if ((evt.keyCode == 13) && (node.type=="text"))  {return false;}
			}
			
			function  suppressNonEng(e)
			{
				var key;
				if(window.event)  key = window.event.keyCode;     //IE
				else  key = e.which;     //firefox

				if(key >128)  
				{
					return false;
				}else if (key == 13){
					addItem();
				}else
				{
					return true;
				}			   
			}	
				
			function suppressOnBluer(e)
			{
				var s= e.value;
				e.value = '';
				for(i=0;i<s.length;i++)
					if (s.charCodeAt(i)<=128)
					{

					e.value += s.charAt(i);
						}
						if(s.length!=e.value.length)
						{
						//alert('Other than english character are not supported')
						var formObject = document.SWB_ScanPkgs
						formObject.txtLastestScan.value="";
						return false;
						}
			        
			}	
			
function VerifiyScannedPkg(strVerify)
{
	var arr_strVerify = strVerify.split(" ");
	if (arr_strVerify.length==4)
	{
		if (arr_strVerify[0]=="")
		{
			return false;
		}
		else if(arr_strVerify[1]=="")
		{
			return false;
		}
		else if(arr_strVerify[2]=="")
		{
			return false;
		}
		else if(arr_strVerify[3]=="")
		{
			return false;
		}				
		else
		{

			 for(i=0;i<arr_strVerify[0].length;i++)// loop for check each char not include "0-9,-,_,space and non english char"
			 {
					var charCode =arr_strVerify[0].charCodeAt(i);
					
					if(charCode > 31&&(charCode < 48 || charCode > 57)&&(charCode < 65 || charCode > 90) && (charCode < 97 || charCode > 122) && (charCode!=32)&& (charCode!=45)&& (charCode!=95)&& (charCode!=47))
					{
						return false;
					}
			 }
			 for(i=0;i<arr_strVerify[1].length;i++)
			 {
					var charCode =arr_strVerify[1].charCodeAt(i);
					if((charCode < 48 || charCode > 57))
					{
						//alert("1");
						return false;
					}
			 }
			for(i=0;i<arr_strVerify[2].length;i++)
			 {
					var charCode =arr_strVerify[2].charCodeAt(i);
					if((charCode < 48 || charCode > 57))
					{
						//alert("2");
						return false;
					}
			 }
			 for(i=0;i<arr_strVerify[3].length;i++)
			 {
					var charCode =arr_strVerify[3].charCodeAt(i);
					if(((charCode < 48 && charCode != 45 )|| charCode > 57)&&((charCode < 65 && charCode != 45) || charCode > 90) && ((charCode < 97 && charCode !=45) || charCode > 122))
					{
						//alert("3");
						return false;
					}
			 }
			 return true;

		}
	}
	else
	{
		return false;
		
	}
}

        /*
            - Function MoveItems()
            - Direction: Specifies direction i.e. L = Left, R = Right
            - All: Move All items 1 = All, 0 = Selected
        */
        function MoveItems(Direction, All,appendtxt)
        {
			var txtAjaxState =document.getElementById("txtAjaxState");
            var LstLeft;
            var LstRight;
            var Removed = '';
            var i;
           
            if (Direction == 'R')
            {
                var LstLeft = document.getElementById("lbScanningPackages");
                //var LstRight = document.getElementById("lbProcessedToDMS");   
                var LstRight = document.getElementById("txtProcesstoDMS");            
            }
            else
            {
                var LstLeft = document.getElementById("txtProcesstoDMS"); 
                var LstRight = document.getElementById("lbScanningPackages");               
            }  
            //for (i=0; i<LstLeft.length; i++)
           // {
                if (LstLeft.options[0].selected || All == 1)
                    //if(ItemExists(LstRight, LstLeft.options[i].value) == 0)
                    //{
                        //LstRight[LstRight.length] = new Option(LstLeft.options[i].text+'    '+appendtxt, LstLeft.options[i].value, true);
                        
						///////Comment By Hong to change Listbox(lbProcessedToDMS) to textarea(txtProcesstoDMS) on 29 Apr 2011
						/*for(j=LstRight.length;j>0;j--)
						{
							LstRight[j]= new Option(LstRight.options[j-1].text, LstRight.options[j-1].value, true);
						}
						LstRight[0]=new Option(LstLeft.options[0].text+'    '+appendtxt, LstLeft.options[0].value, true);*/
						/////
						LstRight.value = LstLeft.options[0].text+'    '+appendtxt+'\n' +LstRight.value;

                        Removed = Removed + LstLeft.options[0].value + ',';
                    //}
            //}
			LstLeft.options[0]=null;
			txtAjaxState.value =""
            //RemoveFromList(LstLeft, Removed);
			doCallAjax('ADD');
            return false;           
        }
   
        function RemoveFromList (Lst, Items)
        {
            var Removed = Items.split(',');
            var j;
            var x;
            for (j=0; j<Removed.length; j++)
            {
                for (x=0; x<Lst.length; x++)
                {
                    if (Lst.options[x] != null && Lst.options[x].value == Removed[j])
                    {
                        Lst.options[x] = null;
                    }
                    else
                    {
						alert("cannot remove");
                    }
                }
            }
        }
       
        function ItemExists(Lst, value)
        {
            var Flag = 0;
            var i = 0;
            for (i=0; i<Lst.length; i++)
            {
                if (Lst.options[i].value == value)
                {
                    Flag = 1;
                    break;
                }
            }
            return Flag;
        }
        function CallMoveItems(appendtxt)
        {
           return MoveItems('R',1,appendtxt);
        }
////// End MoveItems ///////////////
        function RemoveFinnishRecord (Lst, Items,Text)
        {
            var Removed = Items.split(',');
            var j;
            var x;
            for (j=0; j<Removed.length; j++)
            {
                for (x=0; x<Lst.length; x++)
                {
                    if (Lst.options[x] != null && Lst.options[x].value == Removed[j] && Lst.options[x].text == Text)
                    {
                        Lst.options[x] = null;
                    }
                }
            }
        }
       

/// End Prepare Data //////////////

///Start XmlHttpRequest ///////////
	   var HttPRequest = false;

	   function doCallAjax() 
	   {
	  
	    var lbox =document.getElementById("lbScanningPackages");
	    var txtAjaxState =document.getElementById("txtAjaxState");
		var Mode='ADD';
	    if(lbox.length>0&&txtAjaxState.value=="")
	    {
//	    for (i=0; i<lbox.length; i++)
//            {
//    	 do
//    	 {	  
				 
			  txtAjaxState.value = "on";
		      HttPRequest = false;
		      if (window.XMLHttpRequest) { // Mozilla, Safari,...
			     HttPRequest = new XMLHttpRequest();
			     if (HttPRequest.overrideMimeType) {
				    HttPRequest.overrideMimeType('text/html');
			     }
		      } else if (window.ActiveXObject) { // IE
			     try {
				    HttPRequest = new ActiveXObject("Msxml2.XMLHTTP");
			     } catch (e) {
				    try {
				       HttPRequest = new ActiveXObject("Microsoft.XMLHTTP");
				    } catch (e) {}
			     }
		      } 
    		  
		      if (!HttPRequest) {
			     alert('Cannot create XMLHTTP instance');
			     return false;
		      }
    	      
		      var url = 'SWB_ScanPkgs_BackgroundProcess.aspx';
		      var txtparam = lbox.options[0].value.split("%");
		      var formObject = document.SWB_ScanPkgs
			  
			  var appId = formObject.hidAppID.value;
			  var enterpriseId = formObject.hidEnterpriseID.value;
			  var barId = txtparam[0];
			  var timeStamp =txtparam[1];
			  var scannerId = txtparam[2];
			  var batchno = document.getElementById("lblBatchNoDisplay").innerHTML;
			  var status_code =formObject.ddlStatusCode[formObject.ddlStatusCode.selectedIndex].value;
			  var userId = document.getElementById("lblUserIDDisplay").innerHTML;
			  var location =document.getElementById("lblLocationDisplay").innerHTML;
			  var dbCnt = parseInt(document.getElementById("lblSaveInDBDisplay").innerHTML);
			  
		      
		      var pmeters = "tAppID=" + encodeURI(appId) +
							"&tEntID=" + encodeURI(enterpriseId) +
							"&tBarID=" + encodeURI(barId) +
		                    "&tTimeStamp=" + encodeURI(timeStamp) +
		                    "&tBatchNo=" + encodeURI(batchno) +
		                    "&tscannerId=" + encodeURI(scannerId) +
		                    "&tstatus_code=" + encodeURI(status_code) +
		                    "&tuserId=" + encodeURI(userId) +
		                    "&tlocation=" + encodeURI(location) +
		                    "&tDBcnt=" + encodeURI(dbCnt) +
		                    "&tMode=" + Mode;
				//alert(pmeters);
			    HttPRequest.open('POST',url,true);

			    HttPRequest.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			    HttPRequest.setRequestHeader("Content-length", pmeters.length);
			    HttPRequest.send(pmeters);
    			
			    HttPRequest.onreadystatechange = function()
			    {
					
				     if(HttPRequest.readyState == 3)  // Loading Request
				      {
				       //document.getElementById("mySpan").innerHTML = "Now is Loading...";
				      }

				     if(HttPRequest.readyState == 4) // Return Request
				      {
                       //RemoveFinnishRecord(document.getElementById("lbScanningPackages"), lbox.options[0].value,lbox.options[0].text);
						
						var result = HttPRequest.responseText;
						//alert(result);
						//alert(HttPRequest.responseText.length);
						result =result.substring(0,1);
						if(result=='D')
							result='Dup';
						if(result=='I')
							result='Inv';	
						if(result!='E' && result!='Inv' && result!='Dup')
						{
							dbCnt = dbCnt+1;
							
							document.getElementById("lblSaveInDBDisplay").innerHTML =  dbCnt;
						}

						if(HttPRequest.responseText.length==0) // Lost network connection
						{
							txtAjaxState.value="";
							//doCallAjax('ADD');
						}
						else if (result=='E') // Case Return "E" that mean not insert to DB so Loop Insert First rows until can insert.
						{
							txtAjaxState.value="";
							doCallAjax('ADD');
						}
						else
						{
							CallMoveItems(result);						
						}
						
				      }
    				
			    }
//		    }//for do
//		while(lbox.length<=0);
//	        }

	    }
//doCallAjax("ADD");
	   }

///End XmlHttpRequest  ///////////


function JsGenSpace(cnt)
{
	var space='';
	var i=0;
	for (i=0;i<=cnt;i++)
	{
		space+=' ';
	}
	return space;
}

		</script>
	</HEAD>
	<BODY onclick="GetScrollPosition();" onunload="window.top.displayBanner.fnCloseAll(0);"
		onload="SetScrollPosition();" MS_POSITIONING="GridLayout">
		<FORM id="SWB_ScanPkgs" method="post" runat="server">
			<P><FONT face="Tahoma"><cc1:mstextbox style="Z-INDEX: 112; POSITION: absolute; TOP: 408px; LEFT: 648px" id="txtBeginScanDT"
						runat="server" TextMaskType="msDateTime" TextMaskString="99/99/9999 99:99" MaxLength="16" CssClass="textField"
						Width="0px"></cc1:mstextbox></FONT></P>
			<P><asp:label style="Z-INDEX: 101; POSITION: absolute; TOP: 23px; LEFT: 14px" id="Header1" CssClass="mainTitleSize"
					Width="578px" Runat="server">Scanning Workbench Emulator - Status Scan</asp:label></P>
			<P></P>
			<P><cc1:mstextbox style="Z-INDEX: 113; POSITION: absolute; TOP: 456px; LEFT: 648px" id="txtEndScanDT"
					runat="server" TextMaskType="msDateTime" TextMaskString="99/99/9999 99:99" MaxLength="16"
					CssClass="textField" Width="0px"></cc1:mstextbox></P>
			<P></P>
			<asp:label style="Z-INDEX: 102; POSITION: absolute; TOP: 45px; LEFT: 16px" id="lblErrrMsg"
				CssClass="errorMsgColor" Width="900px" Runat="server" HEIGHT="2px"></asp:label><INPUT style="Z-INDEX: 103; POSITION: absolute; TOP: 608px; LEFT: 24px" type="hidden" name="ScrollPosition">&nbsp;
			<FIELDSET style="Z-INDEX: 105; POSITION: absolute; WIDTH: 506px; HEIGHT: 88px; TOP: 80px; LEFT: 360px"><LEGEND><asp:label id="lblCurrentScanResult" CssClass="tableHeadingFieldset" Width="114px" Runat="server"
						HEIGHT="16px">Current Scan Result</asp:label></LEGEND>
				<TABLE style="WIDTH: 501px" id="Table5">
					<TBODY>
						<TR>
							<TD style="WIDTH: 5px; HEIGHT: 23px"></TD>
							<TD style="WIDTH: 136px; HEIGHT: 23px" colSpan="2"><asp:label id="lblLastestScan" runat="server" CssClass="tableLabel">Latest Scan:</asp:label></TD>
							<TD style="WIDTH: 134px; HEIGHT: 23px"><asp:label id="lblScannerID" runat="server" CssClass="tableLabel">Scanner ID:</asp:label></TD>
							<TD style="WIDTH: 134px; HEIGHT: 23px"><asp:label id="lblScanDateTime" runat="server" CssClass="tableLabel">Scan Date/Time:</asp:label></TD>
							<TD style="WIDTH: 5px">&nbsp;</TD>
						</TR>
						<TR>
							<TD style="WIDTH: 5px; HEIGHT: 20px"></TD>
							<TD style="WIDTH: 136px; HEIGHT: 20px" class="tableLabel" colSpan="2"><asp:textbox onblur="suppressOnBluer(this)" id="txtLastestScan" onkeypress="return suppressNonEng(event)"
									runat="server" MaxLength="35" CssClass="tableTextbox" Width="172px"></asp:textbox></TD>
							<TD style="WIDTH: 134px; HEIGHT: 20px"><asp:textbox id="txtScanerID" onkeypress="{if (event.keyCode==13)addItem()}" runat="server" CssClass="tableTextbox"
									Width="77px"></asp:textbox></TD>
							<TD style="WIDTH: 134px; HEIGHT: 20px" class="style2"><cc1:mstextbox id="txtDate" runat="server" TextMaskType="msDate" TextMaskString="99/99/9999 99:99:99"
									MaxLength="10" CssClass="textField" Width="172px" ReadOnly="True"></cc1:mstextbox></TD>
							<TD style="WIDTH: 5px">&nbsp;</TD>
						</TR>
						<TR>
							<TD style="WIDTH: 5px">&nbsp;</TD>
							<TD colSpan="2"><FONT face="Tahoma"></FONT></TD>
							<TD style="WIDTH: 134px"><FONT face="Tahoma"></FONT></TD>
							<TD style="WIDTH: 134px"></TD>
							<TD style="WIDTH: 5px">&nbsp;</TD>
						</TR>
					</TBODY>
				</TABLE>
			</FIELDSET>
			&nbsp;&nbsp;
			<asp:validationsummary style="Z-INDEX: 104; POSITION: absolute; TOP: 608px; LEFT: 224px" id="reqSummary"
				Runat="server" ShowSummary="False" HeaderText="Please enter the missing  fields." ShowMessageBox="True"
				DisplayMode="BulletList"></asp:validationsummary>
			<FIELDSET style="Z-INDEX: 106; POSITION: absolute; WIDTH: 376px; HEIGHT: 210px; TOP: 384px; LEFT: 16px"><LEGEND><asp:label id="lblProcessedToDMS" CssClass="tableHeadingFieldset" Width="108px" Runat="server"
						HEIGHT="16px">Processed to DMS</asp:label></LEGEND>
				<TABLE style="WIDTH: 140.28%; HEIGHT: 221px">
					<TR>
						<td>&nbsp;</td>
						<TD class="tableLabel" noWrap>&nbsp;<asp:label id="lblColPackageScan" runat="server" CssClass="tableLabel">Package Scan</asp:label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							<asp:label id="lblColScan" runat="server" CssClass="tableLabel">Scan</asp:label><asp:label id="lblColDT" runat="server" CssClass="tableLabel">D/T</asp:label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							<asp:label id="lblColID" runat="server" CssClass="tableLabel">ID</asp:label>&nbsp;&nbsp; 
							&nbsp;&nbsp;
							<asp:label id="lblColDuplicate" runat="server" CssClass="tableLabel">Issues</asp:label></TD>
						<td>&nbsp;</td>
					</TR>
					<TR>
						<TD></TD>
						<TD><asp:textbox style="FONT-FAMILY: 'Courier New', Courier, monospace" id="txtProcesstoDMS" runat="server"
								Width="500px" ReadOnly="True" TextMode="MultiLine" Height="176px"></asp:textbox></TD>
						<TD></TD>
					</TR>
					<TR class="tableLabel">
						<td>&nbsp;</td>
						<TD noWrap><asp:label id="lblSaveInDB" runat="server" CssClass="tableLabel">Saved in DB: </asp:label><asp:label id="lblSaveInDBDisplay" runat="server" CssClass="tableLabel">0</asp:label></TD>
						<TD>&nbsp;</TD>
					</TR>
				</TABLE>
			</FIELDSET>
			<FIELDSET style="Z-INDEX: 107; POSITION: absolute; WIDTH: 504px; HEIGHT: 167px; TOP: 192px; LEFT: 360px"><LEGEND><asp:label id="lblScanningPackages02" CssClass="tableHeadingFieldset" Width="80px" Runat="server"
						HEIGHT="16px">Status Scan</asp:label></LEGEND>
				<TABLE style="WIDTH: 100%; HEIGHT: 136px">
					<TR>
						<TD>&nbsp;</TD>
						<TD class="tableLabel">&nbsp;<asp:label id="lblColPackageScan2" runat="server" CssClass="tableLabel">Package Scan</asp:label>
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
							&nbsp;
							<asp:label id="lblColScan2" runat="server" CssClass="tableLabel">Scan</asp:label><asp:label id="lblColDT2" runat="server" CssClass="tableLabel">D/T</asp:label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
							&nbsp;
							<asp:label id="lblColID2" runat="server" CssClass="tableLabel">ID</asp:label></TD>
						<TD>&nbsp;</TD>
					</TR>
					<TR>
						<TD>&nbsp;</TD>
						<TD class="style1">
							<DIV><asp:listbox style="FONT-FAMILY: 'Courier New', Courier, monospace" id="lbScanningPackages" runat="server"
									Width="468px" Height="103px" Font-Size="8pt"></asp:listbox></DIV>
						</TD>
						<TD>&nbsp;</TD>
					</TR>
					<TR class="tableLabel">
						<TD>&nbsp;</TD>
						<TD noWrap>
							<TABLE id="Table1" border="0" cellSpacing="0" cellPadding="0" width="100%">
								<TR>
									<TD><asp:label id="lblScanCount" runat="server" CssClass="tableLabel">Scan Count: </asp:label><asp:label id="lblScanCountDisplay" runat="server" CssClass="tableLabel" text="0"></asp:label></TD>
									<TD align="right"><asp:button id="btnResume" runat="server" CssClass="queryButton" Width="66px" Text="Resume"></asp:button></TD>
								</TR>
							</TABLE>
						</TD>
						<TD>&nbsp;</TD>
					</TR>
				</TABLE>
			</FIELDSET>
			<FIELDSET style="Z-INDEX: 108; POSITION: absolute; WIDTH: 308px; HEIGHT: 288px; TOP: 80px; LEFT: 16px"><LEGEND><asp:label id="lblScanningPackages01" CssClass="tableHeadingFieldset" Width="114px" Runat="server"
						HEIGHT="16px">Scanning Packages</asp:label></LEGEND>
				<TABLE style="WIDTH: 293px; HEIGHT: 181px" id="Table8">
					<TBODY class="tableLabel">
						<TR>
							<TD style="HEIGHT: 13px"></TD>
							<TD style="WIDTH: 136px; HEIGHT: 13px"><FONT face="Tahoma"></FONT></TD>
							<TD style="WIDTH: 134px; HEIGHT: 13px"></TD>
						</TR>
						<TR>
							<TD style="HEIGHT: 23px"></TD>
							<TD style="WIDTH: 136px; HEIGHT: 23px"><asp:label id="lblSelectedStatusCode" runat="server" CssClass="tableLabel">Select Status Code</asp:label></TD>
							<TD style="WIDTH: 134px; HEIGHT: 23px"><asp:dropdownlist id="ddlStatusCode" runat="server" CssClass="textField" Width="106px">
									<asp:ListItem Value="A">SOP</asp:ListItem>
									<asp:ListItem Value="C">SIP</asp:ListItem>
								</asp:dropdownlist></TD>
						</TR>
						<TR>
							<TD>&nbsp;</TD>
							<TD style="WIDTH: 136px" class="tableLabel">&nbsp;</TD>
							<TD style="WIDTH: 134px"><asp:button id="btnScanComplete" runat="server" CssClass="queryButton" Width="113px" Text="Select Batch"></asp:button></TD>
						</TR>
						<TR>
							<TD>&nbsp;</TD>
							<TD style="WIDTH: 136px" class="tableLabel">&nbsp;</TD>
							<TD style="WIDTH: 134px">&nbsp;</TD>
						</TR>
						<TR>
							<TD>&nbsp;</TD>
							<TD style="WIDTH: 136px"><asp:label id="lblBatchSelected" runat="server" CssClass="tableLabel">Batch Selected</asp:label></TD>
							<TD style="WIDTH: 134px"><asp:label id="lblBatchSelectedDisplay" runat="server" CssClass="tableLabel"></asp:label></TD>
						</TR>
						<TR>
							<TD>&nbsp;</TD>
							<TD style="WIDTH: 136px"><asp:label id="lblBatchNo" runat="server" CssClass="tableLabel">Batch Number:</asp:label></TD>
							<TD style="WIDTH: 134px"><asp:label id="lblBatchNoDisplay" runat="server" CssClass="tableLabel"></asp:label></TD>
						</TR>
						<TR>
							<TD>&nbsp;</TD>
							<TD style="WIDTH: 136px"><asp:label id="lblBatchDate" runat="server" CssClass="tableLabel">Batch Date:</asp:label></TD>
							<TD style="WIDTH: 134px"><asp:label id="lblBatchDateDisplay" runat="server" CssClass="tableLabel"></asp:label></TD>
						</TR>
						<TR>
							<TD>&nbsp;</TD>
							<TD style="WIDTH: 136px"><asp:label id="lblTruckID" runat="server" CssClass="tableLabel">Vehicle ID:</asp:label></TD>
							<TD style="WIDTH: 134px"><asp:label id="lblTruckIDDisplay" runat="server" CssClass="tableLabel"></asp:label></TD>
						</TR>
						<TR>
							<TD>&nbsp;</TD>
							<TD style="WIDTH: 136px"><asp:label id="lblBatchID" runat="server" CssClass="tableLabel">Batch ID:</asp:label></TD>
							<TD style="WIDTH: 134px"><asp:label id="lblBatchIDDisplay" runat="server" CssClass="tableLabel"></asp:label></TD>
						</TR>
						<TR>
							<TD></TD>
							<TD style="WIDTH: 136px"><asp:label id="lblBatchType" runat="server" CssClass="tableLabel">Batch Type:</asp:label></TD>
							<TD style="WIDTH: 134px"><asp:label id="lblBatchTypeDisplay" runat="server" CssClass="tableLabel"></asp:label></TD>
						</TR>
						<TR id="trServiceType" style="DISPLAY:none" runat="server">
							<TD>&nbsp;</TD>
							<TD style="WIDTH: 136px"><asp:label id="Label1" runat="server" CssClass="tableLabel">Service Type:</asp:label></TD>
							<TD style="WIDTH: 134px"><asp:label id="lblServiceType" runat="server" CssClass="tableLabel"></asp:label></TD>
						</TR>
						<TR>
							<TD style="HEIGHT: 21px"></TD>
							<TD style="WIDTH: 136px; HEIGHT: 21px"><FONT face="Tahoma">&nbsp;</FONT></TD>
							<TD style="WIDTH: 134px; HEIGHT: 21px"><FONT face="Tahoma">&nbsp;</FONT></TD>
						</TR>
						<TR>
							<TD style="HEIGHT: 15px">&nbsp;</TD>
							<TD style="WIDTH: 136px; HEIGHT: 15px"><asp:label id="lblUserID" runat="server" CssClass="tableLabel">User ID:</asp:label></TD>
							<TD style="WIDTH: 134px; HEIGHT: 15px"><asp:label id="lblUserIDDisplay" runat="server" CssClass="tableLabel"></asp:label></TD>
						</TR>
						<TR>
							<TD></TD>
							<TD style="WIDTH: 136px"><asp:label id="lblLocation" runat="server" CssClass="tableLabel">Location:</asp:label></TD>
							<TD style="WIDTH: 134px"><FONT face="Tahoma"><asp:label id="lblLocationDisplay" runat="server" CssClass="tableLabel"></asp:label></FONT></TD>
						</TR>
					</TBODY>
				</TABLE>
			</FIELDSET>
			<asp:textbox style="Z-INDEX: 109; POSITION: absolute; TOP: 648px; LEFT: 16px" id="hidAppID" runat="server"
				Width="0px"></asp:textbox><asp:textbox style="Z-INDEX: 110; POSITION: absolute; TOP: 648px; LEFT: 40px" id="hidEnterpriseID"
				runat="server" Width="0px"></asp:textbox><asp:textbox style="Z-INDEX: 111; POSITION: absolute; DISPLAY: none; TOP: 416px; LEFT: 592px"
				id="txtBatch_No" runat="server"></asp:textbox><asp:textbox style="Z-INDEX: 114; POSITION: absolute; DISPLAY: none; TOP: 472px; LEFT: 592px"
				id="txtAjaxState" runat="server"></asp:textbox><asp:button style="DISPLAY: none" id="BtnCallCodeBehind" runat="server" Text="Btn Call CodeBehind"></asp:button></FORM>
		<script language="javascript" type="text/javascript">

    function getMovedataInterval()
    {
            //var tbox =document.getElementById("Text2");
            return 600000;
    }

function  refresh()
{
var Mode='refresh';
var txtAjaxState =document.getElementById("txtAjaxState");

	if(txtAjaxState.value=="")
	{
		      HttPRequest = false;
		      if (window.XMLHttpRequest) { // Mozilla, Safari,...
			     HttPRequest = new XMLHttpRequest();
			     if (HttPRequest.overrideMimeType) {
				    HttPRequest.overrideMimeType('text/html');
			     }
		      } else if (window.ActiveXObject) { // IE
			     try {
				    HttPRequest = new ActiveXObject("Msxml2.XMLHTTP");
			     } catch (e) {
				    try {
				       HttPRequest = new ActiveXObject("Microsoft.XMLHTTP");
				    } catch (e) {}
			     }
		      } 
    		  
		      if (!HttPRequest) {
			     alert('Cannot create XMLHTTP instance');
			     return false;
		      }
    	      
		      var url = 'SWB_ScanPkgs_BackgroundProcess.aspx';
		      var formObject = document.SWB_ScanPkgs
			  
			  
		      
		      var pmeters = "tMode=" + Mode;
				//alert(pmeters);
			    HttPRequest.open('POST',url,true);

			    HttPRequest.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			    HttPRequest.setRequestHeader("Content-length", pmeters.length);
			    HttPRequest.send(pmeters);
    			
			    HttPRequest.onreadystatechange = function()
			    {
					
				     if(HttPRequest.readyState == 3)  // Loading Request
				      {
				       //document.getElementById("mySpan").innerHTML = "Now is Loading...";
				      }

				     if(HttPRequest.readyState == 4) // Return Request
				      {
				      
    				  }
			    }
	}
}
//document.getElementById("txtLastestScan").focus();
document.onkeypress = stopRKey; 
setInterval('refresh()',getMovedataInterval());


var formatDate = function (formatDate, formatString) {
	if(formatDate instanceof Date) {
		var months = new Array("Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec");
		var yyyy = formatDate.getFullYear();
		var yy = yyyy.toString().substring(2);
		var m = formatDate.getMonth()+1;
		var mm = m < 10 ? "0" + m : m;
		var mmm = months[m-1];
		var d = formatDate.getDate();
		var dd = d < 10 ? "0" + d : d;
		
		var h = formatDate.getHours();
		var hh = h < 10 ? "0" + h : h;
		var n = formatDate.getMinutes();
		var nn = n < 10 ? "0" + n : n;
		var s = formatDate.getSeconds();
		var ss = s < 10 ? "0" + s : s;

		formatString = formatString.replace(/yyyy/i, yyyy);
		formatString = formatString.replace(/yy/i, yy);
		formatString = formatString.replace(/mmm/i, mmm);
		formatString = formatString.replace(/mm/i, mm);
		formatString = formatString.replace(/m/i, m);
		formatString = formatString.replace(/dd/i, dd);
		formatString = formatString.replace(/d/i, d);
		formatString = formatString.replace(/hh/i, hh);
		formatString = formatString.replace(/h/i, h);
		formatString = formatString.replace(/nn/i, nn);
		formatString = formatString.replace(/n/i, n);
		formatString = formatString.replace(/ss/i, ss);
		formatString = formatString.replace(/s/i, s);

		return formatString;
	} else {
		return "";
	}
}
//alert(formatDate(new Date(), "d-mm-yyyy hh:nn:ss"));
		</script>
	</BODY>
</HTML>
