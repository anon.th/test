<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<%@ Page language="c#" Codebehind="CustomsConfigurations.aspx.cs" AutoEventWireup="false" Inherits="TIES.WebUI.CustomsConfigurations" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Customs Configurations</title>
		<meta name="vs_snapToGrid" content="True">
		<meta name="vs_showGrid" content="False">
		<LINK rel="stylesheet" type="text/css" href="css/Styles.css" name="stylesheet">
		<meta name="GENERATOR" content="Microsoft Visual Studio 7.0">
		<meta name="CODE_LANGUAGE" content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<!--#INCLUDE FILE="msFormValidations.inc"-->
		<script language="javascript" src="Scripts/settingScrollPosition.js"></script>
		<script language="javascript" src="Scripts/JScript_TextControl.js"></script>
		<script>
					function funcDisBack()
					{
						history.go(+1);
					}

					function RemoveBadPaseNumber(strTemp, obj) {
						strTemp = strTemp.replace(/[^0-9]/g, ''); //replace non-integer
						obj.value = strTemp.replace(/\s/g, ''); // replace space
					}
			
					function AfterPasteNumber(obj) {
						setTimeout(function () {
							RemoveBadPaseNumber(obj.value, obj);
						}, 1); //or 4
					}
		</script>
	</HEAD>
	<body onclick="GetScrollPosition();" onunload="window.top.displayBanner.fnCloseAll(0);"
		onload="SetScrollPosition();" MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<div style="Z-INDEX: 102; POSITION: relative; WIDTH: 1007px; HEIGHT: 1248px; TOP: 32px; LEFT: 24px"
				id="divMain" MS_POSITIONING="GridLayout" runat="server">
				<TABLE style="Z-INDEX: 101; POSITION: absolute; TOP: 8px; LEFT: 0px" id="MainTable" border="0"
					width="100%" runat="server">
					<TBODY>
						<tr>
							<td colSpan="2" align="left"><asp:label id="Label1" runat="server" CssClass="mainTitleSize" Height="27px" Width="421px">
							Customs Configurations</asp:label></td>
						</tr>
						<tr>
							<td style="HEIGHT: 26px" vAlign="top" colSpan="2" align="left"><asp:button id="btnQry" runat="server" CssClass="queryButton" CausesValidation="False" Text="Query"></asp:button><label style="WIDTH: 5px"></label><asp:button id="btnExecQry" runat="server" CssClass="queryButton" CausesValidation="False" Text="Execute Query"></asp:button><label style="WIDTH: 5px"></label><label style="WIDTH: 5px"></label><label style="WIDTH: 5px"></label><label style="WIDTH: 5px"></label></td>
						</tr>
						<tr>
							<td vAlign="top" colSpan="2" align="left">
								<TABLE style="Z-INDEX: 112; WIDTH: 1000px" id="Table1" border="0" width="730" runat="server">
									<TR width="100%">
										<TD width="100%">
											<table border="0" cellSpacing="1" cellPadding="1" width="800">
												<TBODY>
													<TR>
														<TD style="HEIGHT: 19px" colSpan="6"><asp:label id="lblError" runat="server" ForeColor="Red" Font-Bold="True" Font-Size="X-Small"></asp:label></TD>
													</TR>
													<TR>
														<TD width="30"><asp:label style="Z-INDEX: 0" id="Label2" runat="server" CssClass="tableLabel">CodeID:</asp:label></TD>
														<TD width="770" colSpan="5" align="left"><asp:dropdownlist style="Z-INDEX: 0" id="dllCodeID" tabIndex="31" runat="server" Width="180px"></asp:dropdownlist></TD>
													</TR>
													<tr>
														<td colSpan="6"><FONT face="Tahoma"></FONT><br>
														</td>
													</tr>
													<tr>
														<td colSpan="6"><asp:datagrid style="Z-INDEX: 0" id="gridConfiguration" tabIndex="42" runat="server" Width="900px"
																AllowPaging="True" AutoGenerateColumns="False" ShowFooter="True" DataKeyField="PK" AlternatingItemStyle-CssClass="gridField"
																ItemStyle-CssClass="gridField" HeaderStyle-CssClass="gridHeading" CellPadding="0" CellSpacing="1" BorderWidth="0px"
																FooterStyle-CssClass="gridHeading">
																<FooterStyle CssClass="gridHeading"></FooterStyle>
																<AlternatingItemStyle CssClass="gridField"></AlternatingItemStyle>
																<ItemStyle CssClass="gridField" Height="20px"></ItemStyle>
																<HeaderStyle CssClass="gridHeading"></HeaderStyle>
																<Columns>
																	<asp:TemplateColumn>
																		<HeaderStyle Width="10%"></HeaderStyle>
																		<ItemStyle HorizontalAlign="Center"></ItemStyle>
																		<ItemTemplate>
																			<asp:LinkButton id="btnEditItem" Runat="server" Text="<img src='images/butt-edit.gif' alt='edit' border=0>"
																				CommandName="EDIT_ITEM" />
																			<asp:LinkButton id="btnDeleteItem" Runat="server" Text="<img src='images/butt-delete.gif' alt='delete' border=0>"
																				CommandName="DELETE_ITEM" />
																		</ItemTemplate>
																		<FooterStyle HorizontalAlign="Center"></FooterStyle>
																		<FooterTemplate>
																			<asp:LinkButton id="btnAddItem" Runat="server" Text="<img src='images/butt-add.gif' alt='add' border=0>"
																				CommandName="ADD_ITEM" />
																		</FooterTemplate>
																		<EditItemTemplate>
																			<asp:LinkButton id="btnSaveItem" Runat="server" Text="<img src='images/butt-update.gif' alt='save' border=0>"
																				CommandName="SAVE_ITEM" />
																			<asp:LinkButton id="btnCancelItem" Runat="server" Text="<img src='images/butt-red.gif' alt='cancel' border=0>"
																				CommandName="CANCEL_ITEM" />
																		</EditItemTemplate>
																	</asp:TemplateColumn>
																	<asp:TemplateColumn HeaderText="PK" Visible="False">
																		<HeaderStyle HorizontalAlign="Center" Width="5%"></HeaderStyle>
																		<ItemStyle HorizontalAlign="Center"></ItemStyle>
																		<ItemTemplate>
																			<asp:Label ID="lbPK" Runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "PK") %>'>
																			</asp:Label>
																			<%--# DataBinder.Eval(Container.DataItem,"Sequence") --%>
																		</ItemTemplate>
																	</asp:TemplateColumn>
																	<asp:TemplateColumn HeaderText="C1">
																		<HeaderStyle HorizontalAlign="Center" Width="5%"></HeaderStyle>
																		<ItemStyle HorizontalAlign="Center"></ItemStyle>
																		<ItemTemplate>
																			<asp:Label ID="lbC1" Runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "C1") %>'>
																			</asp:Label>
																			<%--# DataBinder.Eval(Container.DataItem,"CustomsCode") --%>
																		</ItemTemplate>
																		<EditItemTemplate>
																			<%-- <asp:textbox id="txtEditC1" runat="server" Width="100%" CssClass="textField" > </asp:textbox> --%>
																			<cc1:mstextbox id="txtEditC1" runat="server" CssClass="textField" NumberPrecision="10" TextMaskType="msNumericCOD" MaxLength="6" NumberScale="0" NumberMinValue="0" onpaste="AfterPasteNumber(this)" NumberMaxValueCOD="999999" Width="100%" Text='<%# DataBinder.Eval(Container.DataItem, "C1") %>'>
																			</cc1:mstextbox>
																		</EditItemTemplate>
																		<FooterStyle HorizontalAlign="Center"></FooterStyle>
																		<FooterTemplate>
																			<%-- <asp:textbox id="txtAddC1" runat="server" Width="100%" CssClass="textField" Text=''></asp:textbox> --%>
																			<cc1:mstextbox id="txtAddC1" runat="server" CssClass="textField" NumberPrecision="10" TextMaskType="msNumericCOD"
																				MaxLength="6" NumberScale="0" NumberMinValue="0" onpaste="AfterPasteNumber(this)" NumberMaxValueCOD="999999"
																				Width="100%"></cc1:mstextbox>
																		</FooterTemplate>
																	</asp:TemplateColumn>
																	<asp:TemplateColumn HeaderText="C2">
																		<HeaderStyle HorizontalAlign="Center" Width="10%"></HeaderStyle>
																		<ItemStyle HorizontalAlign="Center"></ItemStyle>
																		<ItemTemplate>
																			<asp:Label ID="lbC2" Runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "C2") %>'>
																			</asp:Label>
																			<%--# DataBinder.Eval(Container.DataItem,"KindDescription") --%>
																		</ItemTemplate>
																		<EditItemTemplate>
																			<asp:textbox id="txtEditC2" runat="server" Width="100%" CssClass="textField" Text='<%# DataBinder.Eval(Container.DataItem, "C2") %>'>
																			</asp:textbox>
																		</EditItemTemplate>
																		<FooterStyle HorizontalAlign="Center"></FooterStyle>
																		<FooterTemplate>
																			<asp:textbox id="txtAddC2" runat="server" Width="100%" CssClass="textField" Text=''></asp:textbox>
																			<%--
																			<cc1:mstextbox CssClass="gridTextBoxNumber" ID="txtFooterLength" onpaste="AfterPasteNonNumber(this)"
																				Text='' Runat="server" TextMaskType="msNumeric" MaxLength="7" NumberMaxValue="9999" NumberMinValue="0"
																				NumberPrecision="6" NumberScale="2"></cc1:mstextbox>
																			--%>
																		</FooterTemplate>
																	</asp:TemplateColumn>
																	<asp:TemplateColumn HeaderText="C3">
																		<HeaderStyle HorizontalAlign="Center" Width="30%"></HeaderStyle>
																		<ItemStyle HorizontalAlign="Center"></ItemStyle>
																		<ItemTemplate>
																			<asp:Label ID="lbC3" Runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "C3") %>'>
																			</asp:Label>
																			<%--# DataBinder.Eval(Container.DataItem,"Default") --%>
																		</ItemTemplate>
																		<EditItemTemplate>
																			<asp:textbox id="txtEditC3" runat="server" Width="100%" CssClass="textField" Text='<%# DataBinder.Eval(Container.DataItem, "C3") %>'>
																			</asp:textbox>
																		</EditItemTemplate>
																		<FooterTemplate>
																			<asp:textbox id="txtAddC3" runat="server" Width="100%" CssClass="textField" Text=''></asp:textbox>
																			<%--
																			<cc1:mstextbox CssClass="gridTextBoxNumber" ID="txtFooterBreadth" onpaste="AfterPasteNonNumber(this)"
																				Text='' Runat="server" TextMaskType="msNumeric" MaxLength="7" NumberMaxValue="9999" NumberMinValue="0"
																				NumberPrecision="6" NumberScale="2"></cc1:mstextbox>
																			--%>
																		</FooterTemplate>
																	</asp:TemplateColumn>
																	<asp:TemplateColumn HeaderText="C4">
																		<HeaderStyle HorizontalAlign="Center" Width="10%"></HeaderStyle>
																		<ItemStyle HorizontalAlign="Center"></ItemStyle>
																		<ItemTemplate>
																			<asp:Label ID="lbC4" Runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "C4") %>'>
																			</asp:Label>
																			<%--# DataBinder.Eval(Container.DataItem,"Default") --%>
																		</ItemTemplate>
																		<EditItemTemplate>
																			<asp:textbox id="txtEditC4" runat="server" Width="100%" CssClass="textField" Text='<%# DataBinder.Eval(Container.DataItem, "C4") %>'>
																			</asp:textbox>
																		</EditItemTemplate>
																		<FooterTemplate>
																			<asp:textbox id="txtAddC4" runat="server" Width="100%" CssClass="textField" Text=''></asp:textbox>
																			<%--
																			<cc1:mstextbox CssClass="gridTextBoxNumber" ID="txtFooterBreadth" onpaste="AfterPasteNonNumber(this)"
																				Text='' Runat="server" TextMaskType="msNumeric" MaxLength="7" NumberMaxValue="9999" NumberMinValue="0"
																				NumberPrecision="6" NumberScale="2"></cc1:mstextbox>
																			--%>
																		</FooterTemplate>
																	</asp:TemplateColumn>
																	<asp:TemplateColumn HeaderText="C5">
																		<HeaderStyle HorizontalAlign="Center" Width="10%"></HeaderStyle>
																		<ItemStyle HorizontalAlign="Center"></ItemStyle>
																		<ItemTemplate>
																			<asp:Label ID="lbC5" Runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "C5") %>'>
																			</asp:Label>
																			<%--# DataBinder.Eval(Container.DataItem,"Default") --%>
																		</ItemTemplate>
																		<EditItemTemplate>
																			<asp:textbox id="txtEditC5" runat="server" Width="100%" CssClass="textField" Text='<%# DataBinder.Eval(Container.DataItem, "C5") %>'>
																			</asp:textbox>
																		</EditItemTemplate>
																		<FooterTemplate>
																			<asp:textbox id="txtAddC5" runat="server" Width="100%" CssClass="textField" Text=''></asp:textbox>
																			<%--
																			<cc1:mstextbox CssClass="gridTextBoxNumber" ID="txtFooterBreadth" onpaste="AfterPasteNonNumber(this)"
																				Text='' Runat="server" TextMaskType="msNumeric" MaxLength="7" NumberMaxValue="9999" NumberMinValue="0"
																				NumberPrecision="6" NumberScale="2"></cc1:mstextbox>
																			--%>
																		</FooterTemplate>
																	</asp:TemplateColumn>
																	<asp:TemplateColumn HeaderText="C6">
																		<HeaderStyle HorizontalAlign="Center" Width="10%"></HeaderStyle>
																		<ItemStyle HorizontalAlign="Center"></ItemStyle>
																		<ItemTemplate>
																			<asp:Label ID="lbC6" Runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "C6") %>'>
																			</asp:Label>
																			<%--# DataBinder.Eval(Container.DataItem,"Default") --%>
																		</ItemTemplate>
																		<EditItemTemplate>
																			<asp:textbox id="txtEditC6" runat="server" Width="100%" CssClass="textField" Text='<%# DataBinder.Eval(Container.DataItem, "C6") %>'>
																			</asp:textbox>
																		</EditItemTemplate>
																		<FooterTemplate>
																			<asp:textbox id="txtAddC6" runat="server" Width="100%" CssClass="textField" Text=''></asp:textbox>
																			<%--
																			<cc1:mstextbox CssClass="gridTextBoxNumber" ID="txtFooterBreadth" onpaste="AfterPasteNonNumber(this)"
																				Text='' Runat="server" TextMaskType="msNumeric" MaxLength="7" NumberMaxValue="9999" NumberMinValue="0"
																				NumberPrecision="6" NumberScale="2"></cc1:mstextbox>
																			--%>
																		</FooterTemplate>
																	</asp:TemplateColumn>
																	<asp:TemplateColumn HeaderText="C7">
																		<HeaderStyle HorizontalAlign="Center" Width="10%"></HeaderStyle>
																		<ItemStyle HorizontalAlign="Center"></ItemStyle>
																		<ItemTemplate>
																			<asp:Label ID="lbC7" Runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "C7") %>'>
																			</asp:Label>
																			<%--# DataBinder.Eval(Container.DataItem,"Default") --%>
																		</ItemTemplate>
																		<EditItemTemplate>
																			<asp:textbox id="txtEditC7" runat="server" Width="100%" CssClass="textField" Text='<%# DataBinder.Eval(Container.DataItem, "C7") %>'>
																			</asp:textbox>
																		</EditItemTemplate>
																		<FooterTemplate>
																			<asp:textbox id="txtAddC7" runat="server" Width="100%" CssClass="textField" Text=''></asp:textbox>
																			<%--
																			<cc1:mstextbox CssClass="gridTextBoxNumber" ID="txtFooterBreadth" onpaste="AfterPasteNonNumber(this)"
																				Text='' Runat="server" TextMaskType="msNumeric" MaxLength="7" NumberMaxValue="9999" NumberMinValue="0"
																				NumberPrecision="6" NumberScale="2"></cc1:mstextbox>
																			--%>
																		</FooterTemplate>
																	</asp:TemplateColumn>
																	<asp:TemplateColumn HeaderText="C8">
																		<HeaderStyle HorizontalAlign="Center" Width="10%"></HeaderStyle>
																		<ItemStyle HorizontalAlign="Center"></ItemStyle>
																		<ItemTemplate>
																			<asp:Label ID="lbC8" Runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "C8") %>'>
																			</asp:Label>
																			<%--# DataBinder.Eval(Container.DataItem,"Default") --%>
																		</ItemTemplate>
																		<EditItemTemplate>
																			<asp:textbox id="txtEditC8" runat="server" Width="100%" CssClass="textField" Text='<%# DataBinder.Eval(Container.DataItem, "C8") %>'>
																			</asp:textbox>
																		</EditItemTemplate>
																		<FooterTemplate>
																			<asp:textbox id="txtAddC8" runat="server" Width="100%" CssClass="textField" Text=''></asp:textbox>
																			<%--
																			<cc1:mstextbox CssClass="gridTextBoxNumber" ID="txtFooterBreadth" onpaste="AfterPasteNonNumber(this)"
																				Text='' Runat="server" TextMaskType="msNumeric" MaxLength="7" NumberMaxValue="9999" NumberMinValue="0"
																				NumberPrecision="6" NumberScale="2"></cc1:mstextbox>
																			--%>
																		</FooterTemplate>
																	</asp:TemplateColumn>
																	<asp:TemplateColumn HeaderText="C9">
																		<HeaderStyle HorizontalAlign="Center" Width="10%"></HeaderStyle>
																		<ItemStyle HorizontalAlign="Center"></ItemStyle>
																		<ItemTemplate>
																			<asp:Label ID="lbC9" Runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "C9") %>'>
																			</asp:Label>
																			<%--# DataBinder.Eval(Container.DataItem,"Default") --%>
																		</ItemTemplate>
																		<EditItemTemplate>
																			<asp:textbox id="txtEditC9" runat="server" Width="100%" CssClass="textField" Text='<%# DataBinder.Eval(Container.DataItem, "C9") %>'>
																			</asp:textbox>
																		</EditItemTemplate>
																		<FooterTemplate>
																			<asp:textbox id="txtAddC9" runat="server" Width="100%" CssClass="textField" Text=''></asp:textbox>
																			<%--
																			<cc1:mstextbox CssClass="gridTextBoxNumber" ID="txtFooterBreadth" onpaste="AfterPasteNonNumber(this)"
																				Text='' Runat="server" TextMaskType="msNumeric" MaxLength="7" NumberMaxValue="9999" NumberMinValue="0"
																				NumberPrecision="6" NumberScale="2"></cc1:mstextbox>
																			--%>
																		</FooterTemplate>
																	</asp:TemplateColumn>
																	<asp:TemplateColumn HeaderText="C10">
																		<HeaderStyle HorizontalAlign="Center" Width="10%"></HeaderStyle>
																		<ItemStyle HorizontalAlign="Center"></ItemStyle>
																		<ItemTemplate>
																			<asp:Label ID="lbC10" Runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "C10") %>'>
																			</asp:Label>
																			<%--# DataBinder.Eval(Container.DataItem,"Default") --%>
																		</ItemTemplate>
																		<EditItemTemplate>
																			<asp:textbox id="txtEditC10" runat="server" Width="100%" CssClass="textField" Text='<%# DataBinder.Eval(Container.DataItem, "C10") %>'>
																			</asp:textbox>
																		</EditItemTemplate>
																		<FooterTemplate>
																			<asp:textbox id="txtAddC10" runat="server" Width="100%" CssClass="textField" Text=''></asp:textbox>
																			<%--
																			<cc1:mstextbox CssClass="gridTextBoxNumber" ID="txtFooterBreadth" onpaste="AfterPasteNonNumber(this)"
																				Text='' Runat="server" TextMaskType="msNumeric" MaxLength="7" NumberMaxValue="9999" NumberMinValue="0"
																				NumberPrecision="6" NumberScale="2"></cc1:mstextbox>
																			--%>
																		</FooterTemplate>
																	</asp:TemplateColumn>
																</Columns>
																<PagerStyle NextPageText="Next" PrevPageText="Previous" HorizontalAlign="Left" CssClass="normalText"></PagerStyle>
															</asp:datagrid></td>
													</tr>
												</TBODY>
											</table>
										</TD>
									</TR>
								</TABLE>
								<DIV></DIV>
							</td>
						</tr>
					</TBODY>
				</TABLE>
			</div>
			<INPUT value="<%=strScrollPosition%>" type=hidden name=ScrollPosition> <INPUT type="hidden" name="hdnRefresh">
		</form>
	</body>
</HTML>
