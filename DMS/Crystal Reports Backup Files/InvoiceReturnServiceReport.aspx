<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<%@ Register TagPrefix="dbCombo" Namespace="Cambro.Web.DbCombo" Assembly="Cambro.Web.DbCombo" %>
<%@ Page language="c#" Codebehind="InvoiceReturnServiceReport.aspx.cs" AutoEventWireup="false" Inherits="TIES.WebUI.InvoiceReturnServiceReport" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Invoice Return Service</title>
		<meta content="False" name="vs_showGrid">
		<LINK href="css/Styles.css" type="text/css" rel="stylesheet" name="stylesheet">
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<!--#INCLUDE FILE="msFormValidations.inc"-->
	</HEAD>
	<body onunload="window.top.displayBanner.fnCloseAll(0);" MS_POSITIONING="GridLayout">
		<form id="ShipmentPendingPODReport" method="post" runat="server">
			<asp:button id="btnQuery" style="Z-INDEX: 100; LEFT: 21px; POSITION: absolute; TOP: 39px" runat="server" CssClass="queryButton" Text="Query" Width="64px"></asp:button>
			<asp:label id="lblErrorMessage" style="Z-INDEX: 104; LEFT: 23px; POSITION: absolute; TOP: 64px" runat="server" CssClass="errorMsgColor" Width="589px" Height="27px">Place holder for err msg</asp:label>&nbsp;
			<asp:button id="btnGenerate" style="Z-INDEX: 101; LEFT: 86px; POSITION: absolute; TOP: 39px" runat="server" CssClass="queryButton" Text="Generate" Width="82px"></asp:button><asp:label id="lblTitle" style="Z-INDEX: 102; LEFT: 20px; POSITION: absolute; TOP: 3px" runat="server" CssClass="maintitleSize" Width="514px" Height="35px"> Invoice Return Service</asp:label>
			<fieldset style="Z-INDEX: 105; LEFT: 24px; WIDTH: 608px; POSITION: absolute; TOP: 80px; HEIGHT: 53px"><legend><asp:label id="Label1" Runat="server">Report Type</asp:label></legend>
				<TABLE id="tblDates" style="WIDTH: 608px; HEIGHT: 53px" width="608" align="left" border="0" runat="server">
					<TR height="27">
						<TD colSpan="9"><asp:radiobutton id="rbInvoiceReturnPeding" runat="server" CssClass="tableRadioButton" Text="Invoice Return Pending" Checked="true" GroupName="QueryByReportType" AutoPostBack="True" tabIndex="1"></asp:radiobutton></TD>
						<TD colSpan="11"><asp:radiobutton id="rbInvoiceReturnException" runat="server" CssClass="tableRadioButton" Text="Invoice Return Exception" Width="219px" GroupName="QueryByReportType" AutoPostBack="True" tabIndex="2"></asp:radiobutton></TD>
					</TR>
					<tr height="5">
						<td colSpan="20"></td>
					</tr>
				</TABLE>
			</fieldset>
			<fieldset style="Z-INDEX: 106; LEFT: 24px; WIDTH: 608px; POSITION: absolute; TOP: 144px; HEIGHT: 186px; padding-top=15px"><legend><asp:label id="Label2" Runat="server">Booking Date</asp:label></legend>
				<table id="table1" style="WIDTH: 608px; HEIGHT: 158px" runat="server">
					<TR height="2">
						<TD width="5%"></TD>
						<TD width="5%"></TD>
						<TD width="5%"></TD>
						<TD width="5%"></TD>
						<TD width="5%"></TD>
						<TD width="5%"></TD>
						<TD width="5%"></TD>
						<TD style="WIDTH: 29px" width="29"></TD>
						<TD width="5%"></TD>
						<TD width="5%"></TD>
						<TD width="5%"></TD>
						<TD width="5%"></TD>
						<TD width="5%"></TD>
						<TD width="5%"></TD>
						<TD width="5%"></TD>
						<TD width="5%"></TD>
						<TD width="5%"></TD>
						<TD width="5%"></TD>
						<TD width="5%"></TD>
						<TD width="5%"></TD>
					</TR>
					<TR height="27">
						<TD colSpan="4"><asp:radiobutton id="rbMonth" runat="server" CssClass="tableRadioButton" Text="Monthly" Width="100%" Height="14px" Checked="True" GroupName="EnterDate" AutoPostBack="True" tabIndex="11"></asp:radiobutton></TD>
						<TD style="WIDTH: 116px" colSpan="4"><asp:dropdownlist id="ddMonth" runat="server" CssClass="textField" Width="100%" tabIndex="12"></asp:dropdownlist></TD>
						<TD align="right" colSpan="3"><asp:label id="lblYear" runat="server" CssClass="tableLabel" Width="100%" Height="22px">Year</asp:label></TD>
						<TD style="WIDTH: 204px" colSpan="4"><cc1:mstextbox id="txtYear" runat="server" CssClass="textField" Width="80px" NumberPrecision="4" NumberMinValue="1" NumberMaxValue="2999" TextMaskType="msNumeric" MaxLength="5" tabIndex="13"></cc1:mstextbox></TD>
						<TD colSpan="5"></TD>
					</TR>
					<TR height="27">
						<TD style="HEIGHT: 20px" colSpan="4"><asp:radiobutton id="rbPeriod" runat="server" CssClass="tableRadioButton" Text="Period" Width="100%" Height="12px" GroupName="EnterDate" AutoPostBack="True" tabIndex="14"></asp:radiobutton></TD>
						<TD style="WIDTH: 116px; HEIGHT: 20px" colSpan="4"><cc1:mstextbox id="txtPeriod" runat="server" CssClass="textField" Width="100%" TextMaskType="msDate" MaxLength="10" TextMaskString="99/99/9999" tabIndex="15"></cc1:mstextbox></TD>
						<TD style="HEIGHT: 20px" align="right" colSpan="3"><asp:label id="lblTo" runat="server" CssClass="tableLabel" Width="100%" Height="22px">To</asp:label></TD>
						<TD style="WIDTH: 204px; HEIGHT: 20px" colSpan="4"><cc1:mstextbox id="txtTo" runat="server" CssClass="textField" Width="80px" TextMaskType="msDate" MaxLength="10" TextMaskString="99/99/9999" tabIndex="16"></cc1:mstextbox></TD>
						<TD style="HEIGHT: 20px" colSpan="5"></TD>
					</TR>
					<TR height="27">
						<TD style="HEIGHT: 18px" colSpan="4"><asp:radiobutton id="rbDate" runat="server" CssClass="tableRadioButton" Text="Daily" Width="100%" Height="10px" GroupName="EnterDate" AutoPostBack="True" tabIndex="17"></asp:radiobutton></TD>
						<TD style="WIDTH: 116px; HEIGHT: 18px" colSpan="4"><cc1:mstextbox id="txtDate" runat="server" CssClass="textField" Width="100%" TextMaskType="msDate" MaxLength="10" TextMaskString="99/99/9999" tabIndex="18"></cc1:mstextbox></TD>
						<TD style="HEIGHT: 18px" colSpan="12"></TD>
					</TR>
					<TR height="5">
						<TD colSpan="20"></TD>
					</TR>
				</table>
			</fieldset>
			&nbsp;&nbsp;&nbsp;
		</form>
	</body>
</HTML>
