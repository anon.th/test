<%@ Page language="c#" Codebehind="CreateUpdateConsignment.aspx.cs" AutoEventWireup="false" Inherits="TIES.WebUI.CreateUpdateConsignment" smartNavigation="False" %>
<%@ Import Namespace="com.common.util" %>
<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>CreateUpdateConsignment</title>
		<meta name="vs_snapToGrid" content="True">
		<meta name="vs_showGrid" content="True">
		<LINK rel="stylesheet" type="text/css" href="css/Styles.css" name="stylesheet">
		<meta name="GENERATOR" content="Microsoft Visual Studio 7.0">
		<meta name="CODE_LANGUAGE" content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<!--#INCLUDE FILE="msFormValidations.inc"-->
		<script language="javascript" src="Scripts/settingScrollPosition.js"></script>
		<script language="javascript" src="Scripts/JScript_TextControl.js"></script>
		<script language="javascript" type="text/javascript">
		function funcDisBack(){
			history.go(+1);
		}

        function RemoveBadNonNumber(strTemp, obj) {
            strTemp = strTemp.replace(/[^0-9]/g, ''); //replace non-numeric
            obj.value = strTemp.replace(/\s/g, ''); // replace space
        }
        function AfterPasteNonNumber(obj) {
            setTimeout(function () {
                RemoveBadNonNumber(obj.value, obj);
            }, 1); //or 4
        }
        
        function callback()
        {
				var btn = document.getElementById('btnClientEvent');
				if(btn != null)
				{					
					btn.click();
				}else{
					alert('error call code behind');
				}				        
			
        }
		</script>
	</HEAD>
	<body onclick="GetScrollPosition();" onunload="window.top.displayBanner.fnCloseAll(0);"
		onload="SetScrollPosition();" MS_POSITIONING="GridLayout">
		<form id="CreateUpdateConsignment" method="post" name="CreateUpdateConsignment" runat="server">
			&nbsp;
			<asp:validationsummary style="Z-INDEX: 105; POSITION: absolute; COLOR: red; TOP: 672px; LEFT: 240px" id="Validationsummary1"
				ShowSummary="False" HeaderText="Please enter the missing  fields." ShowMessageBox="True" DisplayMode="BulletList"
				Runat="server"></asp:validationsummary>
			<div style="Z-INDEX: 102; POSITION: relative; WIDTH: 1007px; HEIGHT: 1543px; TOP: 34px; LEFT: 24px"
				id="divMain" MS_POSITIONING="GridLayout" runat="server">
				<TABLE style="Z-INDEX: 101; POSITION: absolute; TOP: 8px; LEFT: 0px" id="MainTable" border="0"
					width="100%" runat="server">
					<tr>
						<td colSpan="2" align="left"><asp:label id="Label1" runat="server" Width="421px" Height="27px" CssClass="mainTitleSize">
							Create/Update Consignment</asp:label></td>
					</tr>
					<tr>
						<td style="HEIGHT: 26px" vAlign="top" colSpan="2" align="left"><asp:button id="btnQry" runat="server" CssClass="queryButton" Text="Query" CausesValidation="False"></asp:button><asp:button id="btnExecQry" runat="server" CssClass="queryButton" Text="Execute Query" CausesValidation="False"></asp:button><asp:button id="btnPrintConsNote" runat="server" CssClass="queryButton" Text="Insert" CausesValidation="False"></asp:button><asp:button id="btnSave" runat="server" CssClass="queryButton" Text="Save" CausesValidation="False"
								Enabled="False"></asp:button><asp:button id="btnDelete" runat="server" CssClass="queryButton" Text="Delete" CausesValidation="False"
								Enabled="False"></asp:button><asp:button id="btnPrintCons" runat="server" CssClass="queryButton" Text="Print Con Notes" CausesValidation="False"
								Enabled="False"></asp:button><asp:button style="Z-INDEX: 0" id="btnCustomsJobEntry" runat="server" CssClass="queryButton"
								Text="Customs Job Registration" CausesValidation="False" Enabled="True"></asp:button><asp:button style="DISPLAY: none" id="btnClientEvent" runat="server" Text="ClientEvent" CausesValidation="False"></asp:button></td>
					</tr>
					<tr>
						<td vAlign="top" colSpan="2" align="left">
							<TABLE style="Z-INDEX: 112; WIDTH: 1000px" id="Table1" border="0" width="730" runat="server">
								<TR width="100%">
									<TD width="100%">
										<table border="0" cellSpacing="1" cellPadding="1" width="800">
											<TBODY>
												<TR>
													<TD style="HEIGHT: 19px" colSpan="6"><asp:label id="lblError" runat="server" Font-Size="X-Small" Font-Bold="True" ForeColor="Red"></asp:label></TD>
												</TR>
												<tr>
													<td><asp:label style="Z-INDEX: 0" id="Label35" runat="server" Width="100" CssClass="tableLabel">Customer No.</asp:label></td>
													<td style="WIDTH: 235px"><asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase" id="txtCustID" tabIndex="14" runat="server"
															Width="150px" CssClass="textField" Enabled="False" ReadOnly="True" MaxLength="100">20650</asp:textbox><asp:button style="Z-INDEX: 0" id="btnCustomerPopup" tabIndex="11" runat="server" Width="21px"
															Height="19px" CssClass="searchButton" Text="..." CausesValidation="False" Enabled="False"></asp:button></td>
													<td><asp:label id="lblStatus" runat="server" Width="120px" CssClass="tableLabel">Status D/T</asp:label></td>
													<td><asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea" id="txtlast_status_DT"
															tabIndex="14" runat="server" Width="150px" CssClass="textField" Enabled="False" ReadOnly="True"
															MaxLength="100"></asp:textbox></td>
													<td><asp:label id="Label3" runat="server" Width="50px" CssClass="tableLabel"> Status</asp:label></td>
													<td><asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea" id="txtlast_status"
															tabIndex="14" runat="server" Width="160px" CssClass="textField" Enabled="False" ReadOnly="True"
															MaxLength="100"></asp:textbox></td>
												</tr>
												<tr>
													<td><asp:label style="Z-INDEX: 0" id="Label36" runat="server" Width="100" CssClass="tableLabel">Customer Name</asp:label></td>
													<td style="WIDTH: 235px"><asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea" id="txtCustName"
															tabIndex="14" runat="server" Width="150px" CssClass="textField" Enabled="False" ReadOnly="True" MaxLength="100">BANK SOUTH PACIFIC</asp:textbox></td>
													<td><asp:label id="lblRecipPhoneNo" runat="server" CssClass="tableLabel"> Create D/T</asp:label></td>
													<td><asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea" id="txtCreated_DT"
															tabIndex="14" runat="server" Width="150px" CssClass="textField" Enabled="False" ReadOnly="True"
															MaxLength="100"></asp:textbox></td>
													<td width="50"><asp:label id="Label8" runat="server" Width="50px" CssClass="tableLabel">By</asp:label></td>
													<td><asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea" id="txtCreated_By"
															tabIndex="14" runat="server" Width="160px" CssClass="textField" Enabled="False" ReadOnly="True"
															MaxLength="100"></asp:textbox></td>
												</tr>
												<tr>
													<td style="HEIGHT: 22px"><asp:label style="Z-INDEX: 0" id="lblConsNo" runat="server" Width="100" CssClass="tableLabel">Consignment No.</asp:label></td>
													<td style="WIDTH: 246px; HEIGHT: 22px"><asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase" id="consignment_no" tabIndex="15"
															runat="server" Width="150px" CssClass="textField" MaxLength="100"></asp:textbox></td>
													<td style="HEIGHT: 22px"><asp:label id="lblRecipPostCode" runat="server" CssClass="tableLabel"> Update D/T</asp:label></td>
													<td style="HEIGHT: 22px"><asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea" id="txtUpdated_DT"
															tabIndex="14" runat="server" Width="150px" CssClass="textField" Enabled="False" ReadOnly="True" MaxLength="100"></asp:textbox></td>
													<td style="HEIGHT: 22px"><asp:label id="Label12" runat="server" Width="50px" CssClass="tableLabel">By</asp:label></td>
													<td style="HEIGHT: 22px"><asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea" id="txtUpdated_By"
															tabIndex="14" runat="server" Width="160px" CssClass="textField" Enabled="False" ReadOnly="True" MaxLength="100"></asp:textbox></td>
												</tr>
												<TR>
													<TD style="HEIGHT: 22px"><asp:label style="Z-INDEX: 0" id="Label15" runat="server" Width="100" CssClass="tableLabel">Shipping List</asp:label></TD>
													<TD style="WIDTH: 246px; HEIGHT: 22px"><asp:textbox style="Z-INDEX: 0; BACKGROUND-COLOR: #eaeaea" id="ShippingList_No" runat="server"
															Width="185px" CssClass="textField" Enabled="False" MaxLength="50"></asp:textbox></TD>
													<TD style="HEIGHT: 22px"></TD>
													<TD style="HEIGHT: 22px"></TD>
													<TD style="HEIGHT: 22px"></TD>
													<TD style="HEIGHT: 22px"></TD>
												</TR>
												<tr>
													<td colSpan="6"><FONT face="Tahoma"></FONT><br>
													</td>
												</tr>
												<tr class="gridHeading">
													<td colSpan="6"><STRONG><FONT size="2">Sender Details</FONT></STRONG></td>
												</tr>
												<tr>
													<td><asp:label id="Label4" runat="server" CssClass="tableLabel"> Name</asp:label><asp:label id="Label20" runat="server" CssClass="tableLabel" ForeColor="Red">*</asp:label></td>
													<td style="WIDTH: 235px"><asp:dropdownlist style="Z-INDEX: 0" id="sender_name" tabIndex="16" runat="server" Width="150px" AutoPostBack="True">
															<asp:ListItem Value="BILLING">BILLING</asp:ListItem>
														</asp:dropdownlist></td>
													<td><asp:label id="Label5" runat="server" CssClass="tableLabel"> Phone</asp:label><asp:label id="Label22" runat="server" CssClass="tableLabel" ForeColor="Red">*</asp:label></td>
													<td colSpan="3"><asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase" id="sender_telephone" tabIndex="20"
															runat="server" Width="150px" CssClass="textField" MaxLength="100">6753009478</asp:textbox></td>
												</tr>
												<tr>
													<td><asp:label id="Label6" runat="server" CssClass="tableLabel"> Address</asp:label><asp:label id="Label23" runat="server" CssClass="tableLabel" ForeColor="Red">*</asp:label></td>
													<td style="WIDTH: 235px"><asp:textbox style="Z-INDEX: 0" id="sender_address1" tabIndex="17" runat="server" Width="230px"
															CssClass="textField" MaxLength="100">P.O. BOX 173</asp:textbox></td>
													<td><asp:label id="Label7" runat="server" CssClass="tableLabel">Fax</asp:label></td>
													<td colSpan="3"><asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase" id="sender_fax" tabIndex="21" runat="server"
															Width="150px" CssClass="textField" MaxLength="100">3211944</asp:textbox></td>
												</tr>
												<tr>
													<td style="HEIGHT: 22px"></td>
													<td style="WIDTH: 246px; HEIGHT: 22px"><asp:textbox style="Z-INDEX: 0" id="sender_address2" tabIndex="18" runat="server" Width="230px"
															CssClass="textField" MaxLength="100">PORT MORESBY</asp:textbox></td>
													<td style="HEIGHT: 22px"><asp:label id="Label9" runat="server" CssClass="tableLabel"> Contact</asp:label><asp:label id="Label24" runat="server" CssClass="tableLabel" ForeColor="Red">*</asp:label></td>
													<td style="HEIGHT: 22px" colSpan="3"><asp:textbox style="Z-INDEX: 0" id="sender_contact_person" tabIndex="22" runat="server" Width="150px"
															CssClass="textField" MaxLength="100">Carol Ralai</asp:textbox></td>
												</tr>
												<tr>
													<td style="HEIGHT: 22px"><asp:label id="Label10" runat="server" CssClass="tableLabel"> Postal Code</asp:label><asp:label id="Label25" runat="server" CssClass="tableLabel" ForeColor="Red">*</asp:label></td>
													<td style="WIDTH: 246px; HEIGHT: 22px"><asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase" id="sender_zipcode" tabIndex="19"
															runat="server" Width="42px" CssClass="textField" AutoPostBack="True">POM</asp:textbox><asp:button id="btnDisplayCustDtls" runat="server" Width="21px" Height="19px" CssClass="searchButton"
															Text="..." CausesValidation="False"></asp:button><asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea" id="sender_state_name"
															runat="server" Width="165px" CssClass="textField" Enabled="False">NATIONAL CAPITAL DISTRICT</asp:textbox></td>
													<td style="HEIGHT: 22px"><asp:label id="Label11" runat="server" CssClass="tableLabel"> Email</asp:label></td>
													<td style="HEIGHT: 22px" colSpan="3"><asp:textbox style="Z-INDEX: 0" id="sender_email" tabIndex="23" runat="server" Width="230px"
															CssClass="textField" MaxLength="100">Master.contact@bsp.com.jp</asp:textbox></td>
												</tr>
												<tr>
													<td colSpan="6"><FONT face="Tahoma"></FONT><br>
													</td>
												</tr>
												<tr class="gridHeading">
													<td colSpan="6"><STRONG><FONT size="2">Recipient Details</FONT></STRONG></td>
												</tr>
												<tr>
													<td><asp:label style="Z-INDEX: 0" id="Label14" runat="server" CssClass="tableLabel"> Telephone</asp:label><asp:label id="Label26" runat="server" CssClass="tableLabel" ForeColor="Red">*</asp:label></td>
													<td style="WIDTH: 235px"><asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase" id="recipient_telephone" tabIndex="24"
															runat="server" Width="150px" CssClass="textField" MaxLength="100" AutoPostBack="True">2786210</asp:textbox></td>
													<td><asp:label style="Z-INDEX: 0" id="Label19" runat="server" CssClass="tableLabel"> Cust Ref#</asp:label></td>
													<td><asp:textbox style="Z-INDEX: 0" id="ref_no" tabIndex="32" runat="server" Width="150px" CssClass="textField"
															MaxLength="100">PO#123123</asp:textbox></td>
													<td></td>
													<td></td>
												</tr>
												<tr>
													<td><asp:label style="Z-INDEX: 0" id="Label17" runat="server" CssClass="tableLabel"> Cost Centre</asp:label></td>
													<td style="WIDTH: 235px"><asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase" id="recipient_CostCentre" tabIndex="24"
															runat="server" Width="150px" CssClass="textField" MaxLength="100" AutoPostBack="True"></asp:textbox><FONT face="Tahoma"></FONT><asp:button style="Z-INDEX: 0" id="btnRecipient_CostCentre" runat="server" Width="21px" Height="19px"
															CssClass="searchButton" Text="..." CausesValidation="False"></asp:button></td>
													<td></td>
													<td></td>
													<td></td>
													<td></td>
												</tr>
												<tr>
													<td><asp:label style="Z-INDEX: 0" id="Label16" runat="server" CssClass="tableLabel"> Name</asp:label><asp:label id="Label27" runat="server" CssClass="tableLabel" ForeColor="Red">*</asp:label></td>
													<td style="WIDTH: 235px"><asp:textbox style="Z-INDEX: 0" id="recipient_name" tabIndex="25" runat="server" Width="230px"
															CssClass="textField" MaxLength="100">TRANS WONDERLAND LIMITED</asp:textbox></td>
													<td><asp:label style="Z-INDEX: 0" id="lblCODSupportedbyEnterprise" runat="server" CssClass="tableLabel"> COD Amount</asp:label></td>
													<td><cc1:mstextbox id="cod_amount" tabIndex="33" runat="server" Width="150px" CssClass="textFieldRightAlign"
															MaxLength="11" AutoPostBack="False" NumberPrecision="10" TextMaskType="msNumericCOD" NumberScale="2"
															NumberMinValue="0" NumberMaxValueCOD="99999999.99"></cc1:mstextbox></td>
													<td></td>
													<td><FONT face="Tahoma"></FONT></td>
												</tr>
												<tr>
													<td><asp:label style="Z-INDEX: 0" id="Label31" runat="server" CssClass="tableLabel"> Address</asp:label><asp:label id="Label28" runat="server" CssClass="tableLabel" ForeColor="Red">*</asp:label></td>
													<td style="WIDTH: 235px"><asp:textbox style="Z-INDEX: 0" id="recipient_address1" tabIndex="26" runat="server" Width="230px"
															CssClass="textField" MaxLength="100">PO BOX 4270</asp:textbox></td>
													<td><asp:label style="Z-INDEX: 0" id="lblInsSupportedbyEnterprise" runat="server" CssClass="tableLabel"> Declared Value PGK</asp:label></td>
													<td><cc1:mstextbox id="declare_value" tabIndex="34" runat="server" Width="150px" CssClass="textFieldRightAlign"
															MaxLength="11" AutoPostBack="False" NumberPrecision="10" TextMaskType="msNumericCOD" NumberScale="2"
															NumberMinValue="0" NumberMaxValueCOD="99999999.99"></cc1:mstextbox></td>
													<td></td>
													<td></td>
												</tr>
												<tr>
													<td><FONT face="Tahoma"></FONT></td>
													<td style="WIDTH: 235px"><asp:textbox style="Z-INDEX: 0" id="recipient_address2" tabIndex="27" runat="server" Width="230px"
															CssClass="textField" MaxLength="100"></asp:textbox></td>
													<td><asp:label style="Z-INDEX: 0" id="lblHCSupportedbyEnterprise" runat="server" CssClass="tableLabel"> Hard Copy</asp:label></td>
													<td><asp:checkbox style="Z-INDEX: 0" id="return_pod_slip" tabIndex="35" runat="server" CssClass="tableLabel"
															Text="HCR"></asp:checkbox><asp:checkbox style="Z-INDEX: 0" id="return_invoice_hc" tabIndex="36" runat="server" CssClass="tableLabel"
															Text="INVR"></asp:checkbox></td>
													<td></td>
													<td><FONT face="Tahoma"></FONT></td>
												</tr>
												<tr>
													<td><asp:label style="Z-INDEX: 0" id="Label32" runat="server" CssClass="tableLabel"> Postal Code</asp:label><asp:label id="Label29" runat="server" CssClass="tableLabel" ForeColor="Red">*</asp:label></td>
													<td style="WIDTH: 235px"><asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase" id="recipient_zipcode" tabIndex="28"
															runat="server" Width="42px" CssClass="textField" AutoPostBack="True">LAE</asp:textbox><asp:button id="Button2" runat="server" Width="21px" Height="19px" CssClass="searchButton" Text="..."
															CausesValidation="False"></asp:button><asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea" id="recipient_state_name"
															runat="server" Width="165px" CssClass="textField" Enabled="False">MOROBE</asp:textbox></td>
													<td colSpan="4"><asp:label id="Label21" runat="server" CssClass="tableLabel"> Special Delivery Instructions</asp:label></td>
												</tr>
												<tr>
													<td><asp:label style="Z-INDEX: 0" id="Label33" runat="server" CssClass="tableLabel">Fax</asp:label></td>
													<td style="WIDTH: 235px"><asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase" id="recipient_fax" tabIndex="29" runat="server"
															Width="150px" CssClass="textField" MaxLength="100">675321</asp:textbox></td>
													<td rowSpan="3" colSpan="4"><asp:textbox id="remark" tabIndex="37" runat="server" Width="550px" Height="62px" TextMode="MultiLine">Delivery to named contact only</asp:textbox></td>
												</tr>
												<tr>
													<td><asp:label style="Z-INDEX: 0" id="Label34" runat="server" CssClass="tableLabel"> Contact</asp:label></td>
													<td style="WIDTH: 235px"><asp:textbox style="Z-INDEX: 0" id="recipient_contact_person" tabIndex="30" runat="server" Width="150px"
															CssClass="textField" MaxLength="100">Bobby</asp:textbox></td>
													<td></td>
													<td></td>
												</tr>
												<tr>
													<td><asp:label style="Z-INDEX: 0" id="Label18" runat="server" CssClass="tableLabel">Service </asp:label><asp:label id="Label30" runat="server" CssClass="tableLabel" ForeColor="Red">*</asp:label></td>
													<td style="WIDTH: 235px"><asp:dropdownlist id="service_code" tabIndex="31" runat="server" Width="80px">
															<asp:ListItem Value="GEN">GEN</asp:ListItem>
														</asp:dropdownlist></td>
													<td></td>
													<td></td>
												</tr>
												<TR>
													<td><asp:label style="Z-INDEX: 0" id="Label2" runat="server" CssClass="tableLabel">Goods<br />Description</asp:label><asp:label id="lblGoodsDescriptionRequired" runat="server" CssClass="tableLabel" ForeColor="Red">*</asp:label></td>
													<td vAlign="middle" colSpan="5"><asp:textbox id="GoodsDescription" tabIndex="31" runat="server" Width="185px" CssClass="textField"
															MaxLength="100"></asp:textbox></td>
												</TR>
												<tr>
													<td colSpan="6"><br>
														<asp:label style="Z-INDEX: 0" id="lblDangerous" runat="server" CssClass="tableLabel" Font-Size="X-Small"
															Font-Bold="True"></asp:label></td>
												</tr>
												<tr id="PackageDetailsHeading" class="gridHeading" runat="server">
													<td colSpan="6"><STRONG><FONT size="2">Package Details</FONT></STRONG></td>
												</tr>
												<tr>
													<td colSpan="6"><INPUT id="hdnGrd" type="hidden" name="hdnGrd" runat="server">
														<asp:datagrid style="Z-INDEX: 0" id="PackageDetails" tabIndex="42" runat="server" Width="900px"
															FooterStyle-CssClass="gridHeading" BorderWidth="0px" CellSpacing="1" CellPadding="0" HeaderStyle-CssClass="gridHeading"
															ItemStyle-CssClass="gridField" AlternatingItemStyle-CssClass="gridField" DataKeyField="No"
															ShowFooter="True" AutoGenerateColumns="False">
															<FooterStyle CssClass="gridHeading"></FooterStyle>
															<AlternatingItemStyle CssClass="gridField"></AlternatingItemStyle>
															<ItemStyle CssClass="gridField"></ItemStyle>
															<HeaderStyle CssClass="gridHeading"></HeaderStyle>
															<Columns>
																<asp:TemplateColumn>
																	<HeaderStyle Width="11%"></HeaderStyle>
																	<ItemStyle HorizontalAlign="Center"></ItemStyle>
																	<ItemTemplate>
																		<asp:LinkButton id="btnEditItem" Runat="server" Text="<img src='images/butt-edit.gif' alt='edit' border=0>"
																			CommandName="EDIT_ITEM" />
																		<asp:LinkButton id="btnDeleteItem" Runat="server" Text="<img src='images/butt-delete.gif' alt='delete' border=0>"
																			CommandName="DELETE_ITEM" />
																	</ItemTemplate>
																	<FooterStyle HorizontalAlign="Center"></FooterStyle>
																	<FooterTemplate>
																		<asp:LinkButton id="btnAddItem" Runat="server" Text="<img src='images/butt-add.gif' alt='add' border=0>"
																			CommandName="ADD_ITEM" />
																	</FooterTemplate>
																	<EditItemTemplate>
																		<asp:LinkButton id="btnSaveItem" Runat="server" Text="<img src='images/butt-update.gif' alt='save' border=0>"
																			CommandName="SAVE_ITEM" />
																		<asp:LinkButton id="btnCancelItem" Runat="server" Text="<img src='images/butt-red.gif' alt='cancel' border=0>"
																			CommandName="CANCEL_ITEM" />
																	</EditItemTemplate>
																</asp:TemplateColumn>
																<asp:TemplateColumn HeaderText="Act Wt">
																	<HeaderStyle HorizontalAlign="Center" Width="10%"></HeaderStyle>
																	<ItemStyle HorizontalAlign="Center"></ItemStyle>
																	<ItemTemplate>
																		<%# DataBinder.Eval(Container.DataItem,"Act Wt", "{0:#,##0.00}") %>
																	</ItemTemplate>
																	<FooterStyle HorizontalAlign="Center"></FooterStyle>
																	<FooterTemplate>
																		<cc1:mstextbox CssClass="gridTextBoxNumber" ID="txtFooterWeight" onpaste="AfterPasteNonNumber(this)"
																			Text='' Runat="server" Enabled="True" TextMaskType="msNumeric" MaxLength="7" NumberMaxValue="9999"
																			NumberMinValue="0" NumberPrecision="6" NumberScale="2"></cc1:mstextbox>
																	</FooterTemplate>
																	<EditItemTemplate>
																		<cc1:mstextbox CssClass="gridTextBoxNumber" ID="txtWeight" onpaste="AfterPasteNonNumber(this)" Text='<%#DataBinder.Eval(Container.DataItem,"Act Wt")%>' Runat="server" Enabled="True" TextMaskType="msNumeric" MaxLength="7" NumberMaxValue="9999" NumberMinValue="0" NumberPrecision="6" NumberScale="2">
																		</cc1:mstextbox>
																	</EditItemTemplate>
																</asp:TemplateColumn>
																<asp:TemplateColumn HeaderText="Qty">
																	<HeaderStyle HorizontalAlign="Center" Width="10%"></HeaderStyle>
																	<ItemStyle HorizontalAlign="Center"></ItemStyle>
																	<ItemTemplate>
																		<%# DataBinder.Eval(Container.DataItem,"Qty", "{0:#,##0}") %>
																	</ItemTemplate>
																	<FooterStyle HorizontalAlign="Center"></FooterStyle>
																	<FooterTemplate>
																		<cc1:mstextbox CssClass="gridTextBoxNumber" ID="txtFooterQty" onpaste="AfterPasteNonNumber(this)"
																			Text='' Runat="server" Enabled="True" TextMaskType="msNumeric" MaxLength="4" NumberMaxValue="9999"
																			NumberMinValue="0" NumberPrecision="4" NumberScale="0"></cc1:mstextbox>
																	</FooterTemplate>
																	<EditItemTemplate>
																		<cc1:mstextbox CssClass="gridTextBoxNumber" ID="txtQty" onpaste="AfterPasteNonNumber(this)" Text='<%#DataBinder.Eval(Container.DataItem,"Qty")%>' Runat="server" Enabled="True" TextMaskType="msNumeric" MaxLength="4" NumberMaxValue="9999" NumberMinValue="0" NumberPrecision="4" NumberScale="0">
																		</cc1:mstextbox>
																	</EditItemTemplate>
																</asp:TemplateColumn>
																<asp:TemplateColumn HeaderText="Total Kg">
																	<HeaderStyle HorizontalAlign="Center" Width="10%"></HeaderStyle>
																	<ItemStyle HorizontalAlign="Center"></ItemStyle>
																	<ItemTemplate>
																		<%# DataBinder.Eval(Container.DataItem,"Total Kg", "{0:#,##0.00}") %>
																	</ItemTemplate>
																	<FooterStyle HorizontalAlign="Center"></FooterStyle>
																	<FooterTemplate>
																		<label></label>
																	</FooterTemplate>
																</asp:TemplateColumn>
																<asp:TemplateColumn HeaderText="Width (cm)">
																	<HeaderStyle HorizontalAlign="Center" Width="10%"></HeaderStyle>
																	<ItemStyle HorizontalAlign="Center"></ItemStyle>
																	<ItemTemplate>
																		<%# DataBinder.Eval(Container.DataItem,"Width", "{0:#,##0.00}") %>
																	</ItemTemplate>
																	<FooterStyle HorizontalAlign="Center"></FooterStyle>
																	<FooterTemplate>
																		<cc1:mstextbox CssClass="gridTextBoxNumber" ID="txtFooterLength" onpaste="AfterPasteNonNumber(this)"
																			Text='' Runat="server" TextMaskType="msNumeric" MaxLength="7" NumberMaxValue="9999" NumberMinValue="0"
																			NumberPrecision="6" NumberScale="2"></cc1:mstextbox>
																	</FooterTemplate>
																	<EditItemTemplate>
																		<cc1:mstextbox CssClass="gridTextBoxNumber" ID="txtLength" onpaste="AfterPasteNonNumber(this)" Text='<%#DataBinder.Eval(Container.DataItem,"Width")%>' Runat="server" TextMaskType="msNumeric" MaxLength="7" NumberMaxValue="9999" NumberMinValue="0" NumberPrecision="6" NumberScale="2">
																		</cc1:mstextbox>
																	</EditItemTemplate>
																</asp:TemplateColumn>
																<asp:TemplateColumn HeaderText="Breadth (cm)">
																	<HeaderStyle HorizontalAlign="Center" Width="10%"></HeaderStyle>
																	<ItemStyle HorizontalAlign="Center"></ItemStyle>
																	<ItemTemplate>
																		<%# DataBinder.Eval(Container.DataItem,"Breadth", "{0:#,##0.00}") %>
																	</ItemTemplate>
																	<FooterTemplate>
																		<cc1:mstextbox CssClass="gridTextBoxNumber" ID="txtFooterBreadth" onpaste="AfterPasteNonNumber(this)"
																			Text='' Runat="server" TextMaskType="msNumeric" MaxLength="7" NumberMaxValue="9999" NumberMinValue="0"
																			NumberPrecision="6" NumberScale="2"></cc1:mstextbox>
																	</FooterTemplate>
																	<EditItemTemplate>
																		<cc1:mstextbox CssClass="gridTextBoxNumber" ID="txtBreadth" onpaste="AfterPasteNonNumber(this)" Text='<%#DataBinder.Eval(Container.DataItem,"Breadth")%>' Runat="server" TextMaskType="msNumeric" MaxLength="7" NumberMaxValue="9999" NumberMinValue="0" NumberPrecision="6" NumberScale="2">
																		</cc1:mstextbox>
																	</EditItemTemplate>
																</asp:TemplateColumn>
																<asp:TemplateColumn HeaderText="Height (cm)">
																	<HeaderStyle HorizontalAlign="Center" Width="10%"></HeaderStyle>
																	<ItemStyle HorizontalAlign="Center"></ItemStyle>
																	<ItemTemplate>
																		<%# DataBinder.Eval(Container.DataItem,"Height", "{0:#,##0.00}") %>
																	</ItemTemplate>
																	<FooterTemplate>
																		<cc1:mstextbox CssClass="gridTextBoxNumber" ID="txtFooterHeight" onpaste="AfterPasteNonNumber(this)"
																			Text='' Runat="server" TextMaskType="msNumeric" MaxLength="7" NumberMaxValue="9999" NumberMinValue="0"
																			NumberPrecision="6" NumberScale="2"></cc1:mstextbox>
																	</FooterTemplate>
																	<EditItemTemplate>
																		<cc1:mstextbox CssClass="gridTextBoxNumber" ID="txtHeight" onpaste="AfterPasteNonNumber(this)" Text='<%#DataBinder.Eval(Container.DataItem,"Height")%>' Runat="server" TextMaskType="msNumeric" MaxLength="7" NumberMaxValue="9999" NumberMinValue="0" NumberPrecision="6" NumberScale="2">
																		</cc1:mstextbox>
																	</EditItemTemplate>
																</asp:TemplateColumn>
															</Columns>
														</asp:datagrid></td>
												</tr>
											</TBODY>
										</table>
									</TD>
								</TR>
								<tr>
									<td colSpan="2"></td>
								</tr>
								<tr>
									<td colSpan="2"></td>
								</tr>
							</TABLE>
						</td>
					</tr>
				</TABLE>
			</div>
			<DIV style="Z-INDEX: 103; POSITION: relative; WIDTH: 637px; HEIGHT: 500px; TOP: 34px; LEFT: 16px"
				id="divContractService" MS_POSITIONING="GridLayout" runat="server">
				<TABLE style="Z-INDEX: 105; POSITION: absolute; WIDTH: 530px; HEIGHT: 129px; TOP: 16px; LEFT: 43px"
					id="Table2" runat="server">
					<TR>
						<TD style="HEIGHT: 17px"><asp:label id="lblCustIdCngMsg" runat="server" Width="500px" Font-Bold="True">Please accept our Conditions of Contract:</asp:label></TD>
					</TR>
					<tr>
						<td><asp:textbox onkeydown="return  false;" id="txtContractService" onkeypress="return  false;" onkeyup="return  false;"
								runat="server" Width="520px" Height="400px" CssClass="textField" Enabled="true" TextMode="MultiLine"></asp:textbox></td>
					</tr>
					<tr>
						<td style="TEXT-ALIGN: center"><asp:button id="btnAcceptContract" runat="server" CssClass="queryButton" Text="Accept" CausesValidation="False"></asp:button>&nbsp;
							<asp:button id="btnDeclineContrac" runat="server" CssClass="queryButton" Text="Decline" CausesValidation="False"></asp:button></td>
					</tr>
				</TABLE>
			</DIV>
			<DIV style="Z-INDEX: 0; POSITION: relative; WIDTH: 625px; HEIGHT: 217px; TOP: 34px; LEFT: 22px"
				id="divDelOperation" MS_POSITIONING="GridLayout" runat="server">
				<TABLE style="Z-INDEX: 0; POSITION: absolute; WIDTH: 545px; HEIGHT: 107px; TOP: 20px; LEFT: 21px"
					id="Table3" runat="server">
					<TR>
						<TD>
							<P align="center"><asp:label id="lblDelOperation" runat="server" Width="500px" Height="36px">Are you sure you want to delete this consignment?</asp:label></P>
							<P align="center"><asp:button id="btnDelOperationYes" runat="server" CssClass="queryButton" Text="Yes" CausesValidation="False"></asp:button>&nbsp;
								<asp:button id="btnDelOperationNo" runat="server" CssClass="queryButton" Text=" No " CausesValidation="False"></asp:button></P>
						</TD>
					</TR>
				</TABLE>
			</DIV>
			<DIV style="Z-INDEX: 0; POSITION: relative; WIDTH: 625px; HEIGHT: 217px; TOP: 34px; LEFT: 22px"
				id="divDangerous" MS_POSITIONING="GridLayout" runat="server">
				<TABLE style="POSITION: absolute; WIDTH: 545px; HEIGHT: 107px; TOP: 20px; LEFT: 21px" id="Table4"
					runat="server">
					<TR>
						<TD>
							<P align="center"><asp:label id="Label13" runat="server" Width="500px" Height="36px">Does this consignment contain dangerous goods?</asp:label></P>
							<P align="center"><asp:button id="btnDangerousYes" runat="server" CssClass="queryButton" Text="Yes" CausesValidation="False"></asp:button>&nbsp;
								<asp:button id="btnDangerousNo" runat="server" CssClass="queryButton" Text=" No " CausesValidation="False"></asp:button></P>
						</TD>
					</TR>
				</TABLE>
			</DIV>
			<INPUT 
value="<%=strScrollPosition%>" type=hidden name=ScrollPosition> <INPUT type="hidden" name="hdnRefresh">
		</form>
	</body>
</HTML>
