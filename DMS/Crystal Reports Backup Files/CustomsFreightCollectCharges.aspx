<%@ Page language="c#" Codebehind="CustomsFreightCollectCharges.aspx.cs" AutoEventWireup="false" Inherits="TIES.CustomsFreightCollectCharges" %>
<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<%@ Import Namespace="com.common.util" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>CustomsFreightCollectCharges</title>
		<meta name="vs_snapToGrid" content="True">
		<meta name="vs_showGrid" content="True">
		<LINK rel="stylesheet" type="text/css" href="css/Styles.css" name="stylesheet">
		<meta name="GENERATOR" content="Microsoft Visual Studio 7.0">
		<meta name="CODE_LANGUAGE" content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<!--#INCLUDE FILE="msFormValidations.inc"-->
		<script language="javascript" src="Scripts/settingScrollPosition.js"></script>
		<script language="javascript" src="Scripts/JScript_TextControl.js"></script>
		<script language="javascript" type="text/javascript">
		function funcDisBack(){
			history.go(+1);
		}

		function RemoveBadNonNumber(strTemp, obj) {
			strTemp = strTemp.replace(/[^0-9]/g, ''); //replace non-numeric
			obj.value = strTemp.replace(/\s/g, ''); // replace space
		}
		function AfterPasteNonNumber(obj) {
			setTimeout(function () {
				RemoveBadNonNumber(obj.value, obj);
			}, 1); //or 4
		}
		
		function validate(evt) {
			var theEvent = evt || window.event;
			var key = theEvent.keyCode || theEvent.which;
			key = String.fromCharCode( key );
			var regex = /[0-9]/;
			if( !regex.test(key) ) {
				theEvent.returnValue = false;
				if(theEvent.preventDefault) theEvent.preventDefault();
			}
		}
		
		function RemoveBadPaseNumberwhithDot(strTemp, obj) {
			strTemp = strTemp.replace(/[^.0-9]/g, ''); //replace non-numeric
			obj.value = strTemp.replace(/\s/g, ''); // replace space
		}
		
		function AfterPasteNumberwhithDot(obj) {
			setTimeout(function () {
				RemoveBadPaseNumberwhithDot(obj.value, obj);
			}, 1); //or 4
		}
		/*
		function AmountChanged()
        {
			//alert('xxxx');      
			var txtAmount3 = document.getElementById('txtAmount3');        
			var txtCaf = document.getElementById('txtCaf');
			var txtCurrency = document.getElementById('txtCurrency');
			var txtAmountLocalCurrency = document.getElementById('txtAmountLocalCurrency');
			if(txtAmount3.value.trim() =="")
			{
				txtAmountLocalCurrency.value="";
				return;
			}
			if(txtCaf.value.trim() =="")
			{
				txtAmountLocalCurrency.value="";
				return;
			}
			if(txtCurrency.value.trim() =="")
			{
				txtAmountLocalCurrency.value="";
				return;
			}

			var amount = parseFloat(txtAmount3.value.trim());      
			var rate = parseFloat(txtCurrency.value.trim());    
			var caf = parseFloat(txtCaf.value.trim());    
			var amountLC = (amount/rate) * (1 + (caf/100));
			alert(amountLC);
			txtAmountLocalCurrency.value = parseFloat(amountLC).toFixed(2);
        }
        */
		</script>
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="CustomsFreightCollectCharges" method="post" runat="server">
			<div style="Z-INDEX: 102; POSITION: relative; WIDTH: 1008px; HEIGHT: 1543px; TOP: 34px; LEFT: 24px"
				id="divMain" MS_POSITIONING="GridLayout" runat="server">
				<TABLE style="Z-INDEX: 101; POSITION: absolute; TOP: 8px; LEFT: 0px" id="MainTable" border="0"
					width="100%" runat="server">
					<tbody>
						<tr>
							<td><asp:label id="Label1" runat="server" CssClass="mainTitleSize" Height="27px" Width="421px">Freight Collect Entry</asp:label></td>
						</tr>
						<tr>
							<td><asp:button id="btnQuery" runat="server" CssClass="queryButton" CausesValidation="False" Text="Query"></asp:button><asp:button id="btnExecuteQuery" runat="server" CssClass="queryButton" CausesValidation="False"
									Text="Execute Query"></asp:button><asp:button id="btnSave" runat="server" CssClass="queryButton" CausesValidation="False" Text="Save"
									Enabled="False"></asp:button>
								<asp:button style="Z-INDEX: 0; DISPLAY: none" id="btnExecQryHidden" runat="server" Width="130px"
									Text="Execute Query" CausesValidation="False"></asp:button>
							</td>
						</tr>
						<tr>
							<td style="HEIGHT: 19px" colSpan="6"><asp:label id="lblError" runat="server" ForeColor="Red" Font-Bold="True" Font-Size="X-Small"></asp:label></td>
						</tr>
						<tr>
							<td>
								<table style="Z-INDEX: 0; WIDTH: 550px; HEIGHT: 367px" width="550" height="367">
									<tbody>
										<tr>
											<td style="WIDTH: 30%"><asp:label style="Z-INDEX: 0" id="Label2" runat="server" CssClass="tableLabel">Job Entry Number</asp:label></td>
											<td style="WIDTH: 70%"><asp:textbox style="Z-INDEX: 0" id="txtJobEntryNumber" tabIndex="1" onkeypress="validate(event)"
													onpaste="AfterPasteNonNumber(this)" onblur="if( this.value !=''){document.getElementById('btnExecQryHidden').click();}"
													runat="server" CssClass="textField" Width="120px" MaxLength="18"></asp:textbox></td>
										</tr>
										<tr>
											<td style="WIDTH: 30%"><asp:label style="Z-INDEX: 0" id="Label3" runat="server" CssClass="tableLabel">House AWB Number</asp:label></td>
											<td style="WIDTH: 70%"><asp:textbox style="Z-INDEX: 0" id="txtHouseAwbNumber" tabIndex="2" runat="server" CssClass="textField"
													Width="120px" MaxLength="30"></asp:textbox></td>
										</tr>
										<tr>
											<td style="WIDTH: 30%"><asp:label style="Z-INDEX: 0" id="Label4" runat="server" CssClass="tableLabel">Customer</asp:label></td>
											<td style="WIDTH: 70%"><asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea" id="txtCustomer"
													runat="server" CssClass="textField" Width="235px" Enabled="False" MaxLength="100" ReadOnly="True"></asp:textbox></td>
										</tr>
										<tr>
											<td style="WIDTH: 30%; HEIGHT: 20px"><asp:label style="Z-INDEX: 0" id="Label5" runat="server" CssClass="tableLabel">Ship/Flight No</asp:label></td>
											<td style="WIDTH: 70%"><asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea" id="txtShipFlightNo"
													runat="server" CssClass="textField" Width="235px" Enabled="False" MaxLength="100" ReadOnly="True"></asp:textbox></td>
										</tr>
										<tr>
											<td style="WIDTH: 30%"><asp:label style="Z-INDEX: 0" id="Label6" runat="server" CssClass="tableLabel">Arrival Date</asp:label></td>
											<td style="WIDTH: 70%"><asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea" id="txtArrivalDate"
													runat="server" CssClass="textField" Width="114px" Enabled="False" MaxLength="100" ReadOnly="True"></asp:textbox></td>
										</tr>
										<tr>
											<td style="WIDTH: 30%"><asp:label style="Z-INDEX: 0" id="Label7" runat="server" CssClass="tableLabel" Width="55%">From</asp:label><asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea" id="txtFrom1"
													runat="server" CssClass="textField" Width="50px" Enabled="False" MaxLength="100" ReadOnly="True"></asp:textbox></td>
											<td style="WIDTH: 70%"><asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea" id="txtFrom2"
													runat="server" CssClass="textField" Width="50px" Enabled="False" MaxLength="100" ReadOnly="True"></asp:textbox>&nbsp;<asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea" id="txtFrom3"
													runat="server" CssClass="textField" Width="178px" Enabled="False" MaxLength="100" ReadOnly="True"></asp:textbox></td>
										</tr>
										<tr>
											<td style="WIDTH: 30%"><asp:label style="Z-INDEX: 0" id="Label8" runat="server" CssClass="tableLabel">Invoice Printed</asp:label></td>
											<td style="WIDTH: 70%"><asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea" id="txtInvoicePrinted1"
													runat="server" CssClass="textField" Width="114px" Enabled="False" MaxLength="100" ReadOnly="True"></asp:textbox>&nbsp;<asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea" id="txtInvoicePrinted2"
													runat="server" CssClass="textField" Width="114px" Enabled="False" MaxLength="100" ReadOnly="True"></asp:textbox></td>
										</tr>
										<tr>
											<td colSpan="2">
												<fieldset><legend style="COLOR: black">Freight Collect Charge</legend>
													<table style="MARGIN: 5px; WIDTH: 98%">
														<tbody>
															<tr>
																<td style="WIDTH: 88px; HEIGHT: 21px"><asp:label style="Z-INDEX: 0" id="Label9" runat="server" CssClass="tableLabel">Amount</asp:label><asp:label id="Label10" runat="server" CssClass="tableLabel" ForeColor="Red">*</asp:label></td>
																<td style="WIDTH: 41px; HEIGHT: 21px" colSpan="2"><cc1:mstextbox id="txtAmount3" tabIndex="3" onpaste="AfterPasteNumberwhithDot(this)" runat="server"
																		CssClass="textFieldRightAlign" Width="104px" MaxLength="11" NumberMaxValueCOD="99999999.99" NumberMinValue="0" TextMaskType="msNumericCOD"
																		NumberPrecision="10"></cc1:mstextbox></td>
																<td style="WIDTH: 121px; HEIGHT: 21px"><asp:label style="Z-INDEX: 0" id="Label11" runat="server" CssClass="tableLabel">CAF (%)</asp:label></td>
																<td style="HEIGHT: 21px"><asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea" id="txtCaf"
																		runat="server" CssClass="textField" Width="80px" Enabled="False" MaxLength="100" ReadOnly="True"></asp:textbox></td>
															</tr>
															<tr>
																<td style="WIDTH: 88px"><asp:label style="Z-INDEX: 0" id="Label12" runat="server" CssClass="tableLabel">Freight Date</asp:label><asp:label id="Label13" runat="server" CssClass="tableLabel" ForeColor="Red">*</asp:label></td>
																<td colSpan="2" style="WIDTH: 41px"><cc1:mstextbox style="Z-INDEX: 0" id="txtFreightDate" tabIndex="4" runat="server" CssClass="textField"
																		Width="104px" MaxLength="12" TextMaskType="msDate" TextMaskString="99/99/9999"></cc1:mstextbox></td>
																<td>
																	<asp:label style="Z-INDEX: 0" id="Label18" runat="server" Width="125px" CssClass="tableLabel">Express Discount (%)</asp:label></td>
																<td>
																	<asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea" id="txtExpressDiscount"
																		runat="server" Width="80px" CssClass="textField" Enabled="False" MaxLength="100" ReadOnly="True"></asp:textbox></td>
															</tr>
															<tr>
																<td style="WIDTH: 88px"><asp:label style="Z-INDEX: 0" id="Label14" runat="server" CssClass="tableLabel">Currency</asp:label><asp:label id="Label15" runat="server" CssClass="tableLabel" ForeColor="Red">*</asp:label></td>
																<td style="WIDTH: 105px"><asp:dropdownlist style="Z-INDEX: 0" id="ddlCurrency" tabIndex="5" runat="server" Width="104px"></asp:dropdownlist></td>
																<td style="WIDTH: 1px"><asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea" id="txtCurrency"
																		runat="server" CssClass="textField" Width="80px" Enabled="False" MaxLength="100" ReadOnly="True"></asp:textbox></td>
																<td style="WIDTH: 119px"><asp:label style="Z-INDEX: 0" id="Label16" runat="server" CssClass="tableLabel">Rate Date</asp:label></td>
																<td><asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea" id="txtRateDate"
																		runat="server" CssClass="textField" Width="80px" Enabled="False" MaxLength="100" ReadOnly="True"></asp:textbox></td>
															</tr>
															<tr>
																<td style="WIDTH: 197px" colSpan="2"><asp:label style="Z-INDEX: 0" id="Label17" runat="server" CssClass="tableLabel">Amount (local currency)</asp:label></td>
																<td colSpan="3"><asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea" id="txtAmountLocalCurrency"
																		runat="server" CssClass="textField" Width="80px" Enabled="False" MaxLength="100" ReadOnly="True"></asp:textbox></td>
															</tr>
														</tbody>
													</table>
												</fieldset>
											</td>
										<tr>
										</tr>
									</tbody>
								</table>
							</td>
						</tr>
					</tbody>
				</TABLE>
			</div>
		</form>
	</body>
</HTML>
