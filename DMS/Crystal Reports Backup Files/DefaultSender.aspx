<%@ Page language="c#" Codebehind="DefaultSender.aspx.cs" AutoEventWireup="false" Inherits="TIES.WebUI.ConsignmentNote.DefaultSender" smartNavigation="false" %>
<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<%@ Import Namespace="com.common.util" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>DefaultSender</title>
		<META http-equiv="Content-Type" content="text/html; charset=windows-874">
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../css/Styles.css" type="text/css" rel="Stylesheet">
		<script language="javascript" src="../Scripts/settingScrollPosition.js"></script>
		<script language="javascript" type="text/javascript">
			function DoAction(serveraction) {
				frmDefaultSender.elements("ServerAction").value = serveraction;
				frmDefaultSender.submit();
			}
				
			function SelectItem(ConsignmentNo){
				//window.location.href = "../ConsignmentNote/ConsignmentNote.aspx?ConsignmentNo=" + ConsignmentNo; 
			}
			
			var old_bgcolor;
			var old_class;
			function ShowBar(src) {
				if (!src.contains(event.fromElement)) {
					src.style.cursor = 'hand';
					old_class = src.className;
					src.className = 'tabletotal';
				}
			}
			
			function HideBar(src) {
				if (!src.contains(event.toElement)) {
					src.style.cursor = 'default';
					src.className = old_class;
				}
			}
			
			function SetTextbox(obj) {
				document.frmDefaultSender.txtSndNa.value = obj.value;
			}
			
		</script>
	</HEAD>
	<body onkeypress="microsoftKeyPress()" onclick="GetScrollPosition();" onload="SetScrollPosition();"
		onunload="window.top.displayBanner.fnCloseAll(0);" MS_POSITIONING="ViewLayout">
		<form id="frmDefaultSender" runat="server">
			<input id="ServerAction" type="hidden" name="ServerAction">
			<table cellSpacing="0" cellPadding="0" width="100%" border="0">
				<tr>
					<td>
						<table class="TextBody" cellSpacing="0" cellPadding="3" border="0">
							<tr>
								<td colSpan="6"><asp:label id="lblMainTitle" runat="server" CssClass="mainTitleSize" Width="477px">Set Default Sender</asp:label></td>
							</tr>
							<tr class="tablepagingitem" style="HEIGHT: 5px">
								<td style="WIDTH: 120px" align="right">&nbsp;</td>
								<td style="WIDTH: 120px" align="left">&nbsp;</td>
								<td style="WIDTH: 50px" align="right">&nbsp;</td>
								<td style="WIDTH: 100px" align="left">&nbsp;</td>
								<td style="WIDTH: 120px" align="right">&nbsp;</td>
								<td style="WIDTH: 50px" align="right">&nbsp;</td>
							</tr>
							<tr>
								<td>&nbsp;</td>
								<td colSpan="5"><a onclick="javascript:DoAction('CLEAR');"><input class="queryButton" id="btnQuery" style="WIDTH: 60px; HEIGHT: 20px" type="button"
											value="Query" name="btnQuery"></a> <a onclick="javascript:DoAction('SEARCH');">
										<input class="queryButton" id="btnExcute" style="WIDTH: 110px; HEIGHT: 20px" type="button"
											value="Execute Query" name="btnExcute"></a>&nbsp;<a onclick="javascript:DoAction('SAVE');"><input class="queryButton" id="btnSave" style="WIDTH: 60px; HEIGHT: 20px" type="button"
											value="Save" name="btnSave"></a>
								</td>
							</tr>
							<tr class="tablepagingitem" style="HEIGHT: 5px">
								<td colSpan="6">&nbsp;</td>
							</tr>
							<tr>
								<td colSpan="6"><asp:label id="ErrorMsg" runat="server" CssClass="errorMsgColor" Width="643px" Height="3px"></asp:label></td>
							</tr>
							<tr>
								<td colSpan="6">&nbsp;</td>
							</tr>
							<tr>
								<td align="right">Customer Name&nbsp;:&nbsp;</td>
								<td align="left"><asp:textbox id="txtCustNa" runat="server" CssClass="txtReadOnly" Width="280px"></asp:textbox></td>
								<td>&nbsp;</td>
								<td align="right">User ID&nbsp;:&nbsp;</td>
								<td align="left"><asp:textbox id="txtUserID" runat="server" CssClass="txtReadOnly" Width="150px"></asp:textbox></td>
								<td>&nbsp;</td>
							</tr>
							<tr>
								<td align="right">Sender Name (Old)&nbsp;:&nbsp;</td>
								<td align="left"><asp:textbox id="txtSenderName" runat="server" CssClass="txtReadOnly" Width="280px"></asp:textbox></td>
								<td>&nbsp;</td>
								<td align="right">Update Date&nbsp;:&nbsp;</td>
								<td align="left"><asp:textbox id="txtUpdDate" runat="server" CssClass="txtReadOnly" Width="150px"></asp:textbox></td>
								<td>&nbsp;</td>
							</tr>
							<tr>
								<td align="right">Sender Name (New)&nbsp;:&nbsp;</td>
								<td align="left"><input class="txtReadOnly" id="txtSndNa" style="WIDTH: 280px" readOnly type="text" name="txtSndNa"></td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
							</tr>
							<tr>
								<td colSpan="6"></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td align="center"><asp:datagrid id="dgResult" runat="server" AllowPaging="True" AutoGenerateColumns="False" BorderWidth="0px"
							CellSpacing="1" CellPadding="2" HorizontalAlign="Center" HeaderStyle-CssClass="gridHeading" ItemStyle-CssClass="gridField"
							AlternatingItemStyle-CssClass="gridField" PageSize="50">
							<AlternatingItemStyle CssClass="gridField"></AlternatingItemStyle>
							<ItemStyle CssClass="gridField"></ItemStyle>
							<HeaderStyle Font-Bold="True" HorizontalAlign="Center" CssClass="gridHeading"></HeaderStyle>
							<Columns>
								<asp:TemplateColumn HeaderText="Default Value">
									<HeaderStyle HorizontalAlign="Center" Width="5%" VerticalAlign="Top"></HeaderStyle>
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
									<ItemTemplate>
										<%#"<input type=radio id=rdoITEM name=rdoITEM " + DataBinder.Eval(Container.DataItem, "checked_value") + " value='" + DataBinder.Eval(Container.DataItem, "snd_rec_name") + "' onclick='SetTextbox(this);' runat=server>"%>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="No.">
									<HeaderStyle HorizontalAlign="Center" Width="3%" VerticalAlign="Top"></HeaderStyle>
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
									<ItemTemplate>
										<%# (dgResult.PageSize*dgResult.CurrentPageIndex)+Container.ItemIndex+1 %>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:BoundColumn DataField="snd_rec_name" HeaderText="Sender Name">
									<HeaderStyle HorizontalAlign="Center" Width="20%" VerticalAlign="Top"></HeaderStyle>
									<ItemStyle HorizontalAlign="Left"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="contact_person" HeaderText="Contact Person">
									<HeaderStyle HorizontalAlign="Center" Width="15%" VerticalAlign="Top"></HeaderStyle>
									<ItemStyle HorizontalAlign="Left"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="address1" HeaderText="Address 1">
									<HeaderStyle HorizontalAlign="Center" Width="21%" VerticalAlign="Top"></HeaderStyle>
									<ItemStyle HorizontalAlign="Left"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="address2" HeaderText="Address 2">
									<HeaderStyle HorizontalAlign="Center" Width="21%" VerticalAlign="Top"></HeaderStyle>
									<ItemStyle HorizontalAlign="Left"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="zipcode" HeaderText="Postal Code">
									<HeaderStyle HorizontalAlign="Center" Width="5%" VerticalAlign="Top"></HeaderStyle>
									<ItemStyle HorizontalAlign="Left"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="telephone" HeaderText="Telephone">
									<HeaderStyle HorizontalAlign="Center" Width="10%" VerticalAlign="Top"></HeaderStyle>
									<ItemStyle HorizontalAlign="Left"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn Visible="False" DataField="checked_value" HeaderText="checked_value">
									<HeaderStyle HorizontalAlign="Center" Width="0px" VerticalAlign="Top"></HeaderStyle>
									<ItemStyle HorizontalAlign="Left"></ItemStyle>
								</asp:BoundColumn>
							</Columns>
							<PagerStyle NextPageText="Next" Font-Size="Smaller" PrevPageText="Previous" HorizontalAlign="Right"></PagerStyle>
						</asp:datagrid><asp:label id="lblNotFound" runat="server" Font-Bold="True" ForeColor="Red" Visible="False">Data not found.</asp:label></td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
