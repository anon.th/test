<%@ Page language="c#" Codebehind="ShipmentUpdate.aspx.cs" AutoEventWireup="false" Inherits="com.ties.ShipmentUpdate" %>
<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<%@ Import Namespace="com.common.util" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>ShipmentUpdate</title>
		<LINK rel="stylesheet" type="text/css" href="css/Styles.css" name="stylesheet">
		<meta name="GENERATOR" content="Microsoft Visual Studio 7.0">
		<meta name="CODE_LANGUAGE" content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<!--#INCLUDE FILE="msFormValidations.inc"-->
		<script language="javascript" src="Scripts/settingScrollPosition.js"></script>
		<script language="javascript" src="Scripts/WorkProgress.js"></script>
		<script language="javascript">
			function setDateAndTime(myInput)
			{				
				if (myInput.value.length>0)
				{
					if(setDateTimeNoAction(myInput)==false)
					{
						if(setDate(myInput)==false)
						{
							return false;
						}else{
							return true;
						}
					}else{
						return true;
					}
				}
			}
		
function setDateTimeNoAction(myInput)
{
	if (myInput.value.length>0)
	{
		myInput.value = nTrim(myInput.value);
		//alert(myInput.value);
		var error=0;
		var myDate='';
		var myMonth='';
		var myYear='';
		var myHour='';
		var myMin='';
		
		for(i=0; i<2 ; i++)
		{
			if(isDigit(myInput.value.substr(i,1)))
			{
				myDate=myDate + myInput.value.substr(i,1)
			}
			else
			{
				error=1;
			}
		}

		for(i=3; i<5 ; i++)
		{
			if(isDigit(myInput.value.substr(i,1)))
			{
				myMonth=myMonth + myInput.value.substr(i,1)
			}
			else
			{
				error=1;
			}
		}

		for(i=6; i<10; i++)
		{
			if(isDigit(myInput.value.substr(i,1)))
			{
				myYear=myYear + myInput.value.substr(i,1);
			}
			else
			{
				error=1;
			}
		}
		for(i=11; i<13; i++)
		{
			if(isDigit(myInput.value.substr(i,1)))
			{
				myHour=myHour + myInput.value.substr(i,1);
			}
			else
			{
				error=1;
			}
		}
		for(i=14; i<16; i++)
		{
			if(isDigit(myInput.value.substr(i,1)))
			{
				myMin=myMin + myInput.value.substr(i,1);
			}
			else
			{
				error=1;
			}
		}
		

		if(error==0)
		{
			if(parseInt(myDate) <= getDaysInMonth(myMonth, myYear))
			{
				if(parseInt(myMonth) <= 12)
				{
					if((parseInt(myYear) < 2100) && (parseInt(myYear) > 1999))
					{
						if(parseInt(myHour) <= 23)
						{
							if(parseInt(myMin) <= 59)
							{
							
							}
							else
							{
								error=6;
							}
						}
						else
						{
							error=5;
						}
					}
					else
					{
						error=4;
					}
				}
				else
				{
					error=3;
				}
			}
			else
			{
				error=2;
			}
		
			switch(error)
			{
				case 2:
					//alert('Incorrect date in dd/mm/yyyy : '+myDate);
					myInput.value='';
					myInput.focus();
					return false;
					
				case 3:
					alert('Enter month between 1 to 12');
					myInput.value='';
					myInput.focus();
					return false;
					
				case 4:
					alert('Enter year between 2000 to 2099');
					myInput.value='';
					myInput.focus();
					return false;
					
				case 5:
					alert('Enter hour between 0 to 23');
					myInput.value='';
					myInput.focus();
					return false;
					
				case 6:
					alert('Enter minute between 0 to 59');
					myInput.value='';
					myInput.focus();
					return false;
					

				case 0:
					return true;
				default:
					return false;
			}
		}	
		else
		{
			return false;
		}
	}

}		
		
		function setValue()
		{
			var s = document.all['inFile'];
			document.all['txtFilePath'].innerText = s.value;
			if(s.value != '') 
			{
				document.all['txtStatusCode'].innerText = "";
				document.all['txtExceptioncode'].innerText = "";
				document.all['TxtStatusDateTime'].innerText = "";
				document.all['txtStatusDesc'].innerText = "";
				document.all['txtExceptionDesc'].innerText = "";
				document.all['btnImport'].disabled = false;
			}
			else
			{
				document.all['btnImport'].disabled = true;
			}
		}
		
		function clearValue()
		{
			if(document.all['txtFilePath'].innerText == '') {
				document.location.reload();
			}
		}
		
		function upBrowse()
		{
			document.all['divBrowse'].style.backgroundImage='url(images/btn-browse-up.GIF)';
		}
		
		function downBrowse()
		{
			document.all['divBrowse'].style.backgroundImage='url(images/btn-browse-down.GIF)';
		}	
		
			function UpperMask( toField )
			{
				var lcNewChar = String.fromCharCode(window.event.keyCode);
				var llRetVal = true;
				window.event.keyCode = lcNewChar.toUpperCase().charCodeAt(0);
				return llRetVal;
			}
			
			
			
			function confirmNewPkg(objectPkgs)
			{
				var answer = confirm("Package number not valid for this consignment.\nDo you wish to proceed?");
				if (answer)
				{
					document.getElementById('btnPkgConfirm').click();
				}
				else
				{
					document.getElementById(objectPkgs).focus();
				}
			}
			
			function Pop(consingment_no,status,objectConsign)
			{
						params = "Manifest/PopUp_WarningConsignment.aspx?strFormID=ShipmentUpdate";
						params+= "&consingment_no="+consingment_no;
						params+= "&status="+status;
						//alert(SaveButton);
						var obj = new Object(); 
						//debugger;
						showModalDialog(params, obj, 'dialogWidth:600px;dialogHeight:250px;center:yes;status:no;');
						debugger;
						if(obj.returnvalue=='ClickDDlNo')
						{
							document.getElementById('update').click();
							//document.getElementById(objectConsign).focus();
						}
						else if(obj.returnvalue=='ClickDDlYes')
						{
							document.getElementById('btnRegen').click();
						}
						else
						{
							document.getElementById(objectConsign).focus();
						}
			}
			
			function ValidateConNo(e, obj,str_id)
			{//debugger;
				//alert(e.keyCode);
				/*if (e.keyCode == 13) {
					document.getElementById('Txt_hddConNo').value =obj.value;
					__doPostBack('hddConNo','');
				}				*/
				document.getElementById('Txt_hddConNo').value =obj.value;
				document.getElementById('Txt_hddID').value = str_id;
				//alert(document.getElementById('Txt_hddID').value );
				__doPostBack('hddConNo','');								
				return true;							
			}
			
			function ReturnValidateConNo(ret)
			{
				var str_id = document.getElementById('Txt_hddID').value; 
				document.getElementById(str_id).value =ret;
				document.getElementById('Txt_hddID').value='';
			}
		</script>
	</HEAD>
	<body onunload="window.top.displayBanner.fnCloseAll(0);" MS_POSITIONING="FlowLayout">
		<form id="ShipmentUpdate" encType="multipart/form-data" method="post" runat="server">
			<input style="DISPLAY: none" id="hddConNo" type="button" name="hddCon" runat="server" CausesValidation="False">
			<asp:textbox style="DISPLAY: none" id="Txt_hddConNo" runat="server" CssClass="textField" Width="100%"></asp:textbox><asp:textbox style="DISPLAY: none" id="Txt_hddID" runat="server" CssClass="textField" Width="100%"></asp:textbox><FONT face="Tahoma"></FONT><FONT face="Tahoma"></FONT><FONT face="Tahoma"></FONT><FONT face="Tahoma"></FONT><FONT face="Tahoma"></FONT><FONT face="Tahoma"></FONT><FONT face="Tahoma"></FONT><FONT face="Tahoma"></FONT><FONT face="Tahoma"></FONT><FONT face="Tahoma"></FONT><FONT face="Tahoma"></FONT><FONT face="Tahoma"></FONT><FONT face="Tahoma"></FONT><FONT face="Tahoma"></FONT><FONT face="Tahoma"></FONT><FONT face="Tahoma"></FONT><asp:button style="Z-INDEX: 104; POSITION: absolute; DISPLAY: none; TOP: 528px; LEFT: 672px"
				id="btnRegen" tabIndex="9" runat="server" CausesValidation="False" CssClass="queryButton" Text="Regen"></asp:button><asp:label id="TITLE" CssClass="mainTitleSize" Width="602px" Runat="server"> Shipment Update (By Consignment Number/Booking Number)</asp:label>
			<TABLE style="Z-INDEX: 112; WIDTH: 724px; HEIGHT: 462px" id="ShipmentUpdMainTable" border="0"
				width="724" runat="server">
				<tr>
					<td>
						<TABLE style="WIDTH: 721px; HEIGHT: 456px" id="tblShipmentUpdate" border="0" width="721"
							align="left" runat="server">
							<TR height="9">
								<TD width="5%"></TD>
								<TD width="5%"></TD>
								<TD width="5%"></TD>
								<TD width="5%"></TD>
								<TD width="5%"></TD>
								<TD width="5%"></TD>
								<TD width="5%"></TD>
								<TD width="5%"></TD>
								<TD width="5%"></TD>
								<TD width="5%"></TD>
								<TD width="5%"></TD>
								<TD width="5%"></TD>
								<TD width="5%"></TD>
								<TD width="5%"></TD>
								<TD width="5%"></TD>
								<TD width="5%"></TD>
								<TD width="5%"></TD>
								<TD width="5%"></TD>
								<TD style="WIDTH: 35px" width="35"></TD>
								<TD width="5%"></TD>
							</TR>
							<TR height="27">
								<TD colSpan="2"><asp:button id="btnInsert" tabIndex="1" runat="server" CausesValidation="False" CssClass="queryButton"
										Width="100%" Text="Insert" Height="21px"></asp:button></TD>
								<TD colSpan="2"><asp:button id="btnSave" tabIndex="2" runat="server" CausesValidation="False" CssClass="queryButton"
										Width="100%" Text="Save" Height="21px"></asp:button></TD>
								<td colSpan="16"><asp:button style="DISPLAY: none" id="btnFakeSave" tabIndex="2" runat="server" CssClass="queryButton"
										Text="Save" Height="21px"></asp:button></td>
							</TR>
							<tr height="27">
								<td colSpan="20"><asp:label id="lblErrorMsg" runat="server" CssClass="errorMsgColor" Width="100%"></asp:label></td>
							</tr>
							<TR height="27">
								<TD></TD>
								<TD colSpan="4"></TD>
								<TD colSpan="15"></TD>
							</TR>
							<tr height="27">
								<TD align="right"></TD>
								<td colSpan="5"><asp:label id="lblShipment" runat="server" CssClass="tableRadioButton">Shipment Update</asp:label></td>
								<td colSpan="3"><asp:radiobutton id="rbtnDom" tabIndex="5" CssClass="tableRadioButton" Text="Domestic" Runat="server"
										GroupName="GrpUpdateType" AutoPostBack="True" Font-Size="Smaller"></asp:radiobutton></td>
								<td colSpan="5"><asp:radiobutton id="rbtnPickUp" tabIndex="4" CssClass="tableRadioButton" Text="Pickup" Runat="server"
										GroupName="GrpUpdateType" AutoPostBack="True" Font-Size="Smaller"></asp:radiobutton></td>
								<td colSpan="6"></td>
							</tr>
							<TR height="27">
								<TD colSpan="20">&nbsp;</TD>
							</TR>
							<tr height="27">
								<TD><asp:requiredfieldvalidator id="validStatusCode" tabIndex="6" Runat="server" ControlToValidate="txtStatusCode">*</asp:requiredfieldvalidator></TD>
								<TD colSpan="3"><asp:label id="lblStatusCode" tabIndex="7" runat="server" CssClass="tableLabel" Width="100%"
										Height="22px">Status Code</asp:label></TD>
								<TD colSpan="3"><cc1:mstextbox id="txtStatusCode" tabIndex="8" runat="server" CssClass="textField" Width="100%"
										AutoPostBack="True" MaxLength="12" TextMaskType="msUpperAlfaNumericWithUnderscore"></cc1:mstextbox></TD>
								<td><asp:button id="btnStatusCodeSearch" tabIndex="9" runat="server" CausesValidation="False" CssClass="queryButton"
										Text="..."></asp:button></td>
								<TD></TD>
								<td colSpan="4"><asp:textbox id="txtStatusDesc" tabIndex="10" runat="server" CssClass="textField" Width="100%"
										Enabled="False" ReadOnly="True"></asp:textbox></td>
								<TD colSpan="7"></TD>
							</tr>
							<tr height="27">
								<TD></TD>
								<TD colSpan="3"><asp:label id="lblExceptionCode" tabIndex="11" runat="server" CssClass="tableLabel" Width="100%"
										Height="22px">Exception Code</asp:label></TD>
								<TD colSpan="3"><cc1:mstextbox id="txtExceptioncode" tabIndex="12" runat="server" CssClass="textField" Width="100%"
										AutoPostBack="True" MaxLength="12" TextMaskType="msUpperAlfaNumericWithUnderscore"></cc1:mstextbox></TD>
								<td><asp:button id="btnExcepDescSrch" tabIndex="13" runat="server" CausesValidation="False" CssClass="queryButton"
										Text="..."></asp:button></td>
								<td></td>
								<td colSpan="4"><asp:textbox id="txtExceptionDesc" tabIndex="14" runat="server" CssClass="textField" Width="100%"
										Enabled="False" ReadOnly="True"></asp:textbox></td>
								<TD colSpan="7"></TD>
							</tr>
							<tr height="27">
								<TD><asp:requiredfieldvalidator id="validScanDate" Runat="server" ControlToValidate="TxtStatusDateTime">*</asp:requiredfieldvalidator></TD>
								<TD colSpan="3"><asp:label id="lblScanDate" tabIndex="15" runat="server" CssClass="tableLabel" Width="100%"
										Height="22px">Status Date</asp:label></TD>
								<TD colSpan="3"><asp:textbox id="TxtStatusDateTime" tabIndex="16" runat="server" CssClass="textField" idth="100%"></asp:textbox></TD>
								<td colSpan="2"><FONT face="Tahoma">&nbsp; </FONT>
									<asp:label id="lblLocationTop" tabIndex="15" runat="server" CssClass="tableLabel" Width="100%"
										Height="22px">Location</asp:label></td>
								<td colSpan="12"><FONT face="Tahoma"><asp:dropdownlist id="ddlLocation" runat="server" CssClass="textField" Width="136px" Height="19px"></asp:dropdownlist></FONT></td>
							</tr>
							<TR height="27">
								<TD colSpan="20"><asp:panel id="pnImport" runat="server" Visible="true">
										<FIELDSET id="fsImport" runat="server"><LEGEND>
												<asp:label id="lblImportSIPs" runat="server" CssClass="tableHeadingFieldset" Width="100px"
													Height="16px">
											Import Status</asp:label></LEGEND>
											<TABLE style="WIDTH: 90%" id="Table10">
												<TR>
													<TD>&nbsp;</TD>
													<TD vAlign="bottom">
														<DIV style="BACKGROUND-IMAGE: url(images/btn-browse-up.GIF); WIDTH: 70px; DISPLAY: inline; HEIGHT: 20px"
															id="divBrowse" onmouseup="upBrowse();" onmousedown="downBrowse();"><INPUT onblur="setValue();" style="FILTER: alpha(opacity: 0); BORDER-LEFT: #88a0c8 1px outset; BACKGROUND-COLOR: #e9edf0; WIDTH: 20px; FONT-FAMILY: Arial; COLOR: #003068; FONT-SIZE: 11px; BORDER-TOP: #88a0c8 1px outset; BORDER-RIGHT: #88a0c8 1px outset; TEXT-DECORATION: none"
																id="inFile" onfocus="setValue();" type="file" name="inFile" runat="server">
														</DIV>
													</TD>
													<TD vAlign="bottom">&nbsp;
														<asp:textbox id="txtFilePath" runat="server" CssClass="textField" Width="658px"></asp:textbox></TD>
													<TD vAlign="bottom">&nbsp;
														<asp:button id="btnImport" runat="server" CausesValidation="False" CssClass="queryButton" Width="104px"
															Text="Import" Enabled="False"></asp:button></TD>
												</TR>
											</TABLE>
										</FIELDSET>
									</asp:panel></TD>
							</TR>
							<TR height="27">
								<td colSpan="3"><asp:button id="btnScanComplete" runat="server" CausesValidation="False" CssClass="queryButton"
										Width="113px" Text="Select Batch"></asp:button></td>
								<TD colSpan="2"><asp:button id="btnGridInsertRow" runat="server" CausesValidation="False" CssClass="queryButton"
										Width="100%" Text="Insert"></asp:button></TD>
								<td colSpan="15"><asp:button style="DISPLAY: none" id="btnClearHide" runat="server" CausesValidation="False"
										CssClass="queryButton"></asp:button><asp:label id="lblBatch" runat="server" CssClass="tableLabel"></asp:label></td>
							</TR>
							<tr>
								<TD style="WIDTH: 100%" vAlign="top" colSpan="20"><asp:datagrid id="dgShipUpdate" tabIndex="17" runat="server" Width="100%" SelectedItemStyle-CssClass="gridFieldSelected"
										OnDeleteCommand="dgShipUpdate_Delete" AllowCustomPaging="True" PageSize="5" OnUpdateCommand="dgShipUpdate_Update" OnCancelCommand="dgShipUpdate_Cancel"
										OnEditCommand="dgShipUpdate_Edit" AutoGenerateColumns="False" ItemStyle-Height="20">
										<SelectedItemStyle CssClass="gridFieldSelected"></SelectedItemStyle>
										<ItemStyle Height="20px" CssClass="gridField"></ItemStyle>
										<Columns>
											<asp:EditCommandColumn ButtonType="LinkButton" UpdateText="&lt;IMG SRC='images/butt-select.gif' border=0  title='audit/keep' id='update' &gt;"
												CancelText="&lt;IMG SRC='images/butt-cancel.gif' border=0 title='Cancel'&gt;" EditText="&lt;IMG SRC='images/butt-edit.gif' border=0  title='Edit' &gt;">
												<HeaderStyle Width="2%" CssClass="gridHeading"></HeaderStyle>
												<ItemStyle HorizontalAlign="Left"></ItemStyle>
											</asp:EditCommandColumn>
											<asp:ButtonColumn Text="&lt;IMG SRC='images/butt-delete.gif' border=0 title='Delete' &gt;" CommandName="Delete">
												<HeaderStyle HorizontalAlign="Center" Width="2%" CssClass="gridHeading"></HeaderStyle>
												<ItemStyle HorizontalAlign="Center"></ItemStyle>
											</asp:ButtonColumn>
											<asp:TemplateColumn>
												<HeaderStyle Width="10%" CssClass="gridHeading"></HeaderStyle>
												<ItemStyle Wrap="False"></ItemStyle>
												<HeaderTemplate>
													<TABLE>
														<TR>
															<TD><FONT color="red">*</FONT>
															</TD>
															<TD>
																<asp:Label id=Label1 Runat="server" CssClass="gridLabelHeading" Text="<%#strConsgBkNo%>" Font-Bold="True">
																</asp:Label></TD>
														</TR>
													</TABLE>
												</HeaderTemplate>
												<ItemTemplate>
													<asp:Label id=lblCnsgBkNo Runat="server" CssClass="gridLabel" Text='<%#DataBinder.Eval(Container.DataItem,"CnsgBkgNo")%>'>
													</asp:Label>
												</ItemTemplate>
												<EditItemTemplate>
													<asp:TextBox id="txtCnsgBkNo" Runat="server" onkeypress="return UpperMask(this);" onblur="return ValidateConNo(event, this, this.id);" CssClass="gridTextBox" Text='<%#DataBinder.Eval(Container.DataItem,"CnsgBkgNo")%>' Enabled="True">
													</asp:TextBox>
													<asp:RequiredFieldValidator id="csgbkgValidator" Runat="server" ControlToValidate="txtCnsgBkNo" BorderWidth="0"
														Display="None" ErrorMessage="Consignment No"></asp:RequiredFieldValidator>
												</EditItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn>
												<HeaderStyle Width="10%" CssClass="gridHeading"></HeaderStyle>
												<ItemStyle VerticalAlign="Middle"></ItemStyle>
												<HeaderTemplate>
													<TABLE>
														<TR>
															<TD><FONT color="red">*</FONT>
															</TD>
															<TD>
																<asp:Label id=Label4 Runat="server" CssClass="gridLabelHeading" Text="<%#strPackage%>">
																</asp:Label></TD>
														</TR>
													</TABLE>
												</HeaderTemplate>
												<ItemTemplate>
													<asp:Label id=lblPackage Runat="server" CssClass="gridLabel" Text='<%#DataBinder.Eval(Container.DataItem,"pkg_no")%>' Enabled="True">
													</asp:Label>
												</ItemTemplate>
												<EditItemTemplate>
													<cc1:mstextbox id="txtPackage" Name="txtPackage" Runat="server" CssClass="gridTextBox" Width="100%" Text='<%#DataBinder.Eval(Container.DataItem,"pkg_no")%>' TextMaskType="msNumeric" MaxLength="4" Enabled="True" NumberScale="1" NumberPrecision="4" NumberMinValue="1" NumberMaxValue="9999" onpaste="return false;">
													</cc1:mstextbox>
													<asp:RequiredFieldValidator id="validatePgk" Runat="server" ControlToValidate="txtPackage" Enabled="True" BorderWidth="0"
														Display="None" ErrorMessage="Package#"></asp:RequiredFieldValidator>
												</EditItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn>
												<HeaderStyle Width="10%" CssClass="gridHeading"></HeaderStyle>
												<HeaderTemplate>
													<table>
														<tr>
															<td>
																<font color="red">*</font>
															</td>
															<td>
																<asp:Label id="Label2" CssClass="gridLabelHeading" Runat="server" Text='<%#strStatusDT%>'>
																</asp:Label>
															</td>
														</tr>
													</table>
												</HeaderTemplate>
												<ItemTemplate>
													<asp:Label CssClass="gridLabel" ID="lblStatusDtTime" Text='<%#DataBinder.Eval(Container.DataItem,"tracking_datetime","{0:dd/MM/yyyy HH:mm}")%>' Runat="server" Enabled="True">
													</asp:Label>
												</ItemTemplate>
												<EditItemTemplate>
													<cc1:mstextbox CssClass="gridTextBox" ID="txtStatusDtTime" Text='<%#DataBinder.Eval(Container.DataItem,"tracking_datetime","{0:dd/MM/yyyy HH:mm}")%>' Runat="server" MaxLength=16 TextMaskString="99/99/9999 99:99" TextMaskType="msDateTime">
													</cc1:mstextbox>
													<asp:RequiredFieldValidator ID="validTrackDateTime" Runat="server" BorderWidth="0" Display="None" ControlToValidate="txtStatusDtTime"
														ErrorMessage="Status date time"></asp:RequiredFieldValidator>
												</EditItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn Visible="False" HeaderText="Location">
												<HeaderStyle Width="10%" CssClass="gridHeading"></HeaderStyle>
												<ItemStyle VerticalAlign="Middle"></ItemStyle>
												<ItemTemplate>
													<asp:Label CssClass="gridLabel" ID="lblLocation" Text='<%#DataBinder.Eval(Container.DataItem,"location")%>' Runat="server" Enabled="True">
													</asp:Label>
												</ItemTemplate>
												<EditItemTemplate>
													<asp:TextBox CssClass="gridTextBox" Width="100%" ID="txtLocation" Text='<%#DataBinder.Eval(Container.DataItem,"location")%>' Runat="server" MaxLength="25">
													</asp:TextBox>
												</EditItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn>
												<HeaderStyle Width="15%" CssClass="gridHeading"></HeaderStyle>
												<ItemStyle VerticalAlign="Middle"></ItemStyle>
												<HeaderTemplate>
													<table>
														<tr>
															<td>
																<font color="red">*</font>
															</td>
															<td>
																<asp:Label id="Label3" CssClass="gridLabelHeading" Runat="server" Text='<%#strConsgName%>'>
																</asp:Label>
															</td>
														</tr>
													</table>
												</HeaderTemplate>
												<ItemTemplate>
													<asp:Label CssClass="gridLabel" ID="lblConsignee_Name" Text='<%#DataBinder.Eval(Container.DataItem,"consignee_name")%>' Runat="server" Enabled="True">
													</asp:Label>
												</ItemTemplate>
												<EditItemTemplate>
													<asp:TextBox CssClass="gridTextBox" ID="txtConsignee_Name" Text='<%#DataBinder.Eval(Container.DataItem,"consignee_name")%>' Runat="server" MaxLength="100">
													</asp:TextBox>
													<asp:RequiredFieldValidator ID="reqConsnee" Runat="server" BorderWidth="0" Display="None" ControlToValidate="txtConsignee_Name"
														ErrorMessage="Consignee Name"></asp:RequiredFieldValidator>
												</EditItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Person In-Charge">
												<HeaderStyle Width="10%" CssClass="gridHeading"></HeaderStyle>
												<ItemStyle VerticalAlign="Middle"></ItemStyle>
												<ItemTemplate>
													<asp:Label CssClass="gridLabel" ID="lblPersonInchrg" Text='<%#DataBinder.Eval(Container.DataItem,"person_incharge")%>' Runat="server" Enabled="True">
													</asp:Label>
												</ItemTemplate>
												<EditItemTemplate>
													<asp:TextBox CssClass="gridTextBox" ID="txtPersonInchrg" Text='<%#DataBinder.Eval(Container.DataItem,"person_incharge")%>' Runat="server" MaxLength="100">
													</asp:TextBox>
												</EditItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Specify Reason">
												<HeaderStyle Width="21%" CssClass="gridHeading"></HeaderStyle>
												<ItemStyle VerticalAlign="Middle"></ItemStyle>
												<ItemTemplate>
													<asp:Label CssClass="gridLabel" ID="lblremarks" Text='<%#DataBinder.Eval(Container.DataItem,"remarks")%>' Runat="server" Enabled="True">
													</asp:Label>
												</ItemTemplate>
												<EditItemTemplate>
													<asp:TextBox CssClass="gridTextBox" ID="txtRemarks" Text='<%#DataBinder.Eval(Container.DataItem,"remarks")%>' Runat="server" MaxLength="100">
													</asp:TextBox>
												</EditItemTemplate>
											</asp:TemplateColumn>
										</Columns>
										<PagerStyle NextPageText="Next" PrevPageText="Previous" HorizontalAlign="Right" CssClass="normalText"></PagerStyle>
									</asp:datagrid>
									<P>&nbsp;</P>
									<P><asp:datagrid id="dgImport" tabIndex="17" runat="server" Width="100%" Visible="False" SelectedItemStyle-CssClass="gridFieldSelected"
											AllowCustomPaging="True" PageSize="5" AutoGenerateColumns="False" ItemStyle-Height="20">
											<SelectedItemStyle CssClass="gridFieldSelected"></SelectedItemStyle>
											<ItemStyle Height="20px" CssClass="gridField"></ItemStyle>
											<Columns>
												<asp:TemplateColumn>
													<HeaderTemplate>
														<TABLE>
															<TR>
																<TD>
																	<asp:Label id="Label5" Runat="server" CssClass="gridLabelHeading" Text="Booking No" Font-Bold="True"></asp:Label></TD>
															</TR>
														</TABLE>
													</HeaderTemplate>
													<ItemTemplate>
														<%#DataBinder.Eval(Container.DataItem,"booking_no")%>
													</ItemTemplate>
													<HeaderStyle Width="10%" CssClass="gridHeading"></HeaderStyle>
													<ItemStyle Wrap="False"></ItemStyle>
												</asp:TemplateColumn>
												<asp:TemplateColumn>
													<HeaderTemplate>
														<TABLE>
															<TR>
																<TD>
																	<asp:Label id="Label6" Runat="server" CssClass="gridLabelHeading" Text="Consignment No" Font-Bold="True"></asp:Label></TD>
															</TR>
														</TABLE>
													</HeaderTemplate>
													<ItemTemplate>
														<%#DataBinder.Eval(Container.DataItem,"consignment_no")%>
													</ItemTemplate>
													<HeaderStyle Width="15%" CssClass="gridHeading"></HeaderStyle>
													<ItemStyle Wrap="False"></ItemStyle>
												</asp:TemplateColumn>
												<asp:TemplateColumn>
													<HeaderTemplate>
														<TABLE>
															<TR>
																<TD>
																	<asp:Label id="Label8" Runat="server" CssClass="gridLabelHeading" Text="Tracking Datetime"
																		Font-Bold="True"></asp:Label></TD>
															</TR>
														</TABLE>
													</HeaderTemplate>
													<ItemTemplate>
														<%#DataBinder.Eval(Container.DataItem,"tracking_datetime","{0:dd/MM/yyyy HH:mm}")%>
													</ItemTemplate>
													<HeaderStyle Width="15%" CssClass="gridHeading"></HeaderStyle>
													<ItemStyle Wrap="False"></ItemStyle>
												</asp:TemplateColumn>
												<asp:TemplateColumn>
													<HeaderTemplate>
														<TABLE>
															<TR>
																<TD>
																	<asp:Label id="Label9" Runat="server" CssClass="gridLabelHeading" Text="Status Code" Font-Bold="True"></asp:Label></TD>
															</TR>
														</TABLE>
													</HeaderTemplate>
													<ItemTemplate>
														<%#DataBinder.Eval(Container.DataItem,"status_code")%>
													</ItemTemplate>
													<HeaderStyle CssClass="gridHeading"></HeaderStyle>
													<ItemStyle Width="10%" Wrap="False"></ItemStyle>
												</asp:TemplateColumn>
												<asp:TemplateColumn>
													<HeaderTemplate>
														<TABLE>
															<TR>
																<TD>
																	<asp:Label id="Label10" Runat="server" CssClass="gridLabelHeading" Text="Error No" Font-Bold="True"></asp:Label></TD>
															</TR>
														</TABLE>
													</HeaderTemplate>
													<ItemTemplate>
														<%#DataBinder.Eval(Container.DataItem,"error")%>
													</ItemTemplate>
													<HeaderStyle Width="10%" CssClass="gridHeading"></HeaderStyle>
													<ItemStyle Wrap="False"></ItemStyle>
												</asp:TemplateColumn>
												<asp:TemplateColumn>
													<HeaderTemplate>
														<TABLE>
															<TR>
																<TD>
																	<asp:Label id="Label11" Runat="server" CssClass="gridLabelHeading" Text="Error Message" Font-Bold="True"></asp:Label></TD>
															</TR>
														</TABLE>
													</HeaderTemplate>
													<ItemTemplate>
														<%#DataBinder.Eval(Container.DataItem,"errormsg")%>
													</ItemTemplate>
													<HeaderStyle CssClass="gridHeading"></HeaderStyle>
													<ItemStyle></ItemStyle>
												</asp:TemplateColumn>
											</Columns>
											<PagerStyle NextPageText="Next" PrevPageText="Previous" HorizontalAlign="Right" CssClass="normalText"></PagerStyle>
										</asp:datagrid></P>
								</TD>
								<td></td>
							</tr>
						</TABLE>
					</td>
				</tr>
			</TABLE>
			<asp:validationsummary style="Z-INDEX: 101; POSITION: absolute; TOP: 516px; LEFT: 15px" id="PageValidationSummary"
				runat="server" Width="503px" Height="21px" Font-Size="Smaller" ShowSummary="False" ShowMessageBox="True"
				HeaderText="The following field(s) are required:<br><br>" DisplayMode="BulletList"></asp:validationsummary><asp:button style="Z-INDEX: 105; POSITION: absolute; DISPLAY: none; TOP: 520px; LEFT: 704px"
				id="btnPkgConfirm" tabIndex="9" runat="server" CausesValidation="False" CssClass="queryButton" Text="PkgConfirm"></asp:button><asp:label style="Z-INDEX: 102; POSITION: absolute; DISPLAY: none; TOP: 512px; LEFT: 600px"
				id="lblBatchNo" runat="server"></asp:label><asp:button style="Z-INDEX: 103; POSITION: absolute; DISPLAY: none; TOP: 536px; LEFT: 608px"
				id="btnSubmitHidden" runat="server" Text="Button"></asp:button></form>
	</body>
</HTML>
