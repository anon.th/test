<%@ Import Namespace="com.common.util" %>
<%@ Import Namespace="Cambro.Web.DbCombo" %>
<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<%@ Register TagPrefix="DbCombo" Namespace="Cambro.Web.DbCombo" Assembly="Cambro.Web.DbCombo" %>
<%@ Page language="c#" Codebehind="PODReportQuery.aspx.cs" AutoEventWireup="false" Inherits="com.ties.PODReportQuery" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>PODReport</title>
		<LINK href="css/Styles.css" type="text/css" rel="stylesheet" name="stylesheet">
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<!--#INCLUDE FILE="msFormValidations.inc"-->
		<script language="javascript" src="Scripts/settingScrollPosition.js"></script>
				<script language="javascript">
			function makeUppercase(objId)
			{
				document.getElementById(objId).value = document.getElementById(objId).value.toUpperCase();
			}
		</script>
	</HEAD>
	<body onclick="GetScrollPosition();" onload="SetScrollPosition();" onunload="window.top.displayBanner.fnCloseAll(0);"
		MS_POSITIONING="GridLayout">
		<form id="PODReportQuery" method="post" runat="server">
			<asp:button id="btnQuery" style="Z-INDEX: 100; POSITION: absolute; TOP: 40px; LEFT: 20px" runat="server"
				Text="Query" CssClass="queryButton" Width="64px"></asp:button>
			<TABLE id="tblExternal" style="Z-INDEX: 107; POSITION: absolute; WIDTH: 797px; HEIGHT: 426px; TOP: 90px; LEFT: 21px"
				border="0" runat="server">
				<TR>
					<TD style="WIDTH: 400px; HEIGHT: 137px" vAlign="top">
						<FIELDSET style="WIDTH: 432px; HEIGHT: 129px"><LEGEND>
								<asp:label id="Label6" runat="server" Width="20px" CssClass="tableHeadingFieldset" Font-Bold="True">Dates</asp:label></LEGEND>
							<TABLE id="Table1" style="WIDTH: 424px; HEIGHT: 113px" cellSpacing="0" cellPadding="0"
								align="left" border="0" runat="server">
								<TR>
									<TD></TD>
									<TD colSpan="3">&nbsp;
										<asp:radiobutton id="rbBookingDateCust" runat="server" Width="115px" CssClass="tableRadioButton"
											Text="Booking Date" Height="22px" Checked="True" GroupName="QueryByDate" AutoPostBack="True"></asp:radiobutton>
										<asp:radiobutton id="rbActPkDateCust" runat="server" Width="136px" CssClass="tableRadioButton" Text="Actual Pickup Date"
											Height="22px" GroupName="QueryByDate" AutoPostBack="True"></asp:radiobutton>&nbsp;
										<asp:radiobutton id="rbActPODDateCust" runat="server" Width="137px" CssClass="tableRadioButton" Text="Actual POD Date"
											Height="22px" GroupName="QueryByDate" AutoPostBack="True"></asp:radiobutton></TD>
								</TR>
								<TR>
									<TD></TD>
									<TD colSpan="3">&nbsp;
										<asp:radiobutton id="rbMonthCust" runat="server" Width="73px" CssClass="tableRadioButton" Text="Month"
											Height="21px" Checked="True" GroupName="EnterDate" AutoPostBack="True"></asp:radiobutton>&nbsp;
										<asp:dropdownlist id="ddMonthCust" runat="server" Width="88px" CssClass="textField" Height="19px"></asp:dropdownlist>&nbsp;
										<asp:label id="Label5" runat="server" Width="30px" CssClass="tableLabel" Height="15px">Year</asp:label>&nbsp;
										<cc1:mstextbox id="txtYearCust" runat="server" Width="83" CssClass="textField" NumberPrecision="4"
											NumberMinValue="1" NumberMaxValue="2999" TextMaskType="msNumeric" MaxLength="5"></cc1:mstextbox></TD>
								</TR>
								<TR>
									<TD style="HEIGHT: 20px"></TD>
									<TD style="HEIGHT: 20px" colSpan="3">&nbsp;
										<asp:radiobutton id="rbPeriodCust" runat="server" Width="62px" CssClass="tableRadioButton" Text="Period"
											GroupName="EnterDate" AutoPostBack="True"></asp:radiobutton>&nbsp;&nbsp;&nbsp;
										<cc1:mstextbox id="txtPeriodCust" runat="server" Width="82" CssClass="textField" TextMaskType="msDate"
											MaxLength="10" TextMaskString="99/99/9999"></cc1:mstextbox>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
										&nbsp;
										<cc1:mstextbox id="txtToCust" runat="server" Width="83" CssClass="textField" TextMaskType="msDate"
											MaxLength="10" TextMaskString="99/99/9999"></cc1:mstextbox></TD>
								</TR>
								<TR>
									<TD></TD>
									<TD colSpan="3">&nbsp;
										<asp:radiobutton id="rbDateCust" runat="server" Width="74px" CssClass="tableRadioButton" Text="Date"
											Height="22px" GroupName="EnterDate" AutoPostBack="True"></asp:radiobutton>&nbsp;
										<cc1:mstextbox id="txtDateCust" runat="server" Width="82" CssClass="textField" TextMaskType="msDate"
											MaxLength="10" TextMaskString="99/99/9999"></cc1:mstextbox></TD>
								</TR>
							</TABLE>
						</FIELDSET>
					</TD>
					<TD style="HEIGHT: 137px" vAlign="top">
						<P>
						</P>
						<P>
						</P>
						<FIELDSET style="WIDTH: 320px; HEIGHT: 121px"><LEGEND>
								<asp:label id="Label9" runat="server" Width="79px" CssClass="tableHeadingFieldset" Font-Bold="True">Destination</asp:label></LEGEND>
							<TABLE id="Table2" style="WIDTH: 300px; HEIGHT: 57px" cellSpacing="0" cellPadding="0" align="left"
								border="0" runat="server">
								<TR height="37">
									<TD style="HEIGHT: 33px"></TD>
									<TD class="tableLabel" style="WIDTH: 360px; HEIGHT: 33px" colSpan="3">&nbsp;
										<asp:label id="lblPostalCodeCust" runat="server" Width="84px" CssClass="tableLabel" Height="18px">Postal Code</asp:label>&nbsp;&nbsp;&nbsp;
										<cc1:mstextbox id="txtZipCodeCust" runat="server" Width="139px" CssClass="textField" TextMaskType="msUpperAlfaNumericWithHyphen"
											MaxLength="10"></cc1:mstextbox>&nbsp;&nbsp;
										<asp:button id="btnZipCodeCust" runat="server" CssClass="searchButton" Text="..." CausesValidation="False"></asp:button></TD>
									<TD style="HEIGHT: 33px"></TD>
								</TR>
								<TR>
									<TD style="HEIGHT: 33px"></TD>
									<TD class="tableLabel" style="WIDTH: 360px; HEIGHT: 33px" colSpan="3">&nbsp;
										<asp:label id="lblStateCust" runat="server" Width="100px" CssClass="tableLabel" Height="17px"> Province</asp:label>
										<cc1:mstextbox id="txtStateCodeCust" runat="server" Width="139" CssClass="textField" TextMaskType="msUpperAlfaNumericWithUnderscore"
											MaxLength="10"></cc1:mstextbox>&nbsp;&nbsp;
										<asp:button id="btnStateCodeCust" runat="server" CssClass="searchButton" Text="..." CausesValidation="False"></asp:button></TD>
									<TD style="HEIGHT: 33px"></TD>
								</TR>
							</TABLE>
						</FIELDSET>
					</TD>
				</TR>
			</TABLE>
			<asp:label id="lblErrorMessage" style="Z-INDEX: 104; POSITION: absolute; TOP: 69px; LEFT: 24px"
				runat="server" CssClass="errorMsgColor" Width="700px" Height="17px">Place holder for err msg</asp:label>&nbsp;
			<asp:button id="btnExecuteQuery" style="Z-INDEX: 101; POSITION: absolute; TOP: 40px; LEFT: 90px"
				runat="server" Text="Execute Query" CssClass="queryButton" Width="123px"></asp:button>
			<TABLE id="tblInternal" style="Z-INDEX: 103; POSITION: absolute; WIDTH: 800px; HEIGHT: 200px; TOP: 90px; LEFT: 21px"
				width="800" border="0" runat="server">
				<TR>
					<TD style="WIDTH: 437px; HEIGHT: 137px" vAlign="top">
						<fieldset style="WIDTH: 432px; HEIGHT: 129px"><legend><asp:label id="lblDates" runat="server" CssClass="tableHeadingFieldset" Width="20px" Font-Bold="True">Dates</asp:label></legend>
							<TABLE id="tblDates" style="WIDTH: 424px; HEIGHT: 113px" cellSpacing="0" cellPadding="0"
								align="left" border="0" runat="server">
								<TR>
									<td></td>
									<TD colSpan="3">&nbsp;
										<asp:radiobutton id="rbBookingDate" runat="server" Text="Booking Date" CssClass="tableRadioButton"
											Width="115px" Height="22px" AutoPostBack="True" GroupName="QueryByDate" Checked="True"></asp:radiobutton>
										<asp:radiobutton id="rbPickUpDate" runat="server" Text="Actual Pickup Date" CssClass="tableRadioButton"
											Width="136px" Height="22px" AutoPostBack="True" GroupName="QueryByDate"></asp:radiobutton>&nbsp;
										<asp:radiobutton id="rbPODDate" runat="server" Width="137px" CssClass="tableRadioButton" Text="Actual POD Date"
											Height="22px" GroupName="QueryByDate" AutoPostBack="True"></asp:radiobutton></TD>
								</TR>
								<TR>
									<td></td>
									<TD colSpan="3">&nbsp;
										<asp:radiobutton id="rbMonth" runat="server" Text="Month" CssClass="tableRadioButton" Width="73px"
											Height="21px" AutoPostBack="True" GroupName="EnterDate" Checked="True"></asp:radiobutton>&nbsp;
										<asp:dropdownlist id="ddMonth" runat="server" CssClass="textField" Width="88px" Height="19px"></asp:dropdownlist>&nbsp;
										<asp:label id="lblYear" runat="server" CssClass="tableLabel" Width="30px" Height="22px">Year</asp:label>&nbsp;
										<cc1:mstextbox id="txtYear" runat="server" CssClass="textField" Width="83" MaxLength="5" TextMaskType="msNumeric"
											NumberMaxValue="2999" NumberMinValue="1" NumberPrecision="4"></cc1:mstextbox></TD>
								</TR>
								<TR>
									<td style="HEIGHT: 20px"></td>
									<TD style="HEIGHT: 20px" colSpan="3">&nbsp;
										<asp:radiobutton id="rbPeriod" runat="server" Text="Period" CssClass="tableRadioButton" Width="62px"
											AutoPostBack="True" GroupName="EnterDate"></asp:radiobutton>&nbsp;&nbsp;&nbsp;
										<cc1:mstextbox id="txtPeriod" runat="server" CssClass="textField" Width="82" MaxLength="10" TextMaskType="msDate"
											TextMaskString="99/99/9999"></cc1:mstextbox>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
										&nbsp;
										<cc1:mstextbox id="txtTo" runat="server" CssClass="textField" Width="83" MaxLength="10" TextMaskType="msDate"
											TextMaskString="99/99/9999"></cc1:mstextbox></TD>
								</TR>
								<TR>
									<td></td>
									<TD colSpan="3">&nbsp;
										<asp:radiobutton id="rbDate" runat="server" Text="Date" CssClass="tableRadioButton" Width="74px"
											Height="22px" AutoPostBack="True" GroupName="EnterDate"></asp:radiobutton>&nbsp;&nbsp;<cc1:mstextbox id="txtDate" runat="server" CssClass="textField" Width="82" MaxLength="10" TextMaskType="msDate"
											TextMaskString="99/99/9999"></cc1:mstextbox></TD>
								</TR>
							</TABLE>
						</fieldset>
					</TD>
					<TD style="HEIGHT: 137px" vAlign="top">
						<fieldset style="WIDTH: 320px; HEIGHT: 105px"><legend><asp:label id="lblPayerType" runat="server" CssClass="tableHeadingFieldset" Width="79px" Font-Bold="True">Payer Type</asp:label></legend>
							<TABLE id="tblPayerType" style="WIDTH: 300px; HEIGHT: 73px" cellSpacing="0" cellPadding="0"
								align="left" border="0" runat="server">
								<tr>
									<td></td>
									<td colSpan="2" height="1">&nbsp;</td>
									<td></td>
									<td></td>
								</tr>
								<TR>
									<td></td>
									<TD class="tableLabel" vAlign="top" colSpan="2">&nbsp;
										<asp:label id="Label4" runat="server" CssClass="tableLabel" Width="79px" Height="22px">Payer Type</asp:label></TD>
									<td><asp:listbox id="lsbCustType" runat="server" Width="137px" Height="61px" SelectionMode="Multiple"></asp:listbox></td>
									<td></td>
								</TR>
								<TR height="33">
									<td bgColor="blue"></td>
									<TD class="tableLabel" vAlign="top" colSpan="2">&nbsp;
										<asp:label id="lblPayerCode" runat="server" CssClass="tableLabel" Width="79px" Height="22px">Payer Code</asp:label>&nbsp;&nbsp;</TD>
									<td><asp:TextBox id="txtPayerCode" runat="server" CssClass="textField" Width="148px"></asp:TextBox>&nbsp;&nbsp;
										<asp:button id="btnPayerCode" runat="server" Text="..." CssClass="searchButton" CausesValidation="False"></asp:button></td>
									<td></td>
								</TR>
							</TABLE>
						</fieldset>
					</TD>
				</TR>
				<TR>
					<TD style="WIDTH: 437px; HEIGHT: 134px" vAlign="top">
						<fieldset style="WIDTH: 432px; HEIGHT: 137px"><legend><asp:label id="lblRouteType" runat="server" CssClass="tableHeadingFieldset" Width="145px" Font-Bold="True">Route / DC Selection</asp:label></legend>
							<TABLE id="tblRouteType" style="WIDTH: 360px; HEIGHT: 47px" cellSpacing="0" cellPadding="0"
								align="left" border="0" runat="server">
								<TR>
									<TD class="tableLabel" colSpan="2">&nbsp;
										<asp:radiobutton id="rbLongRoute" runat="server" Text="Linehaul" CssClass="tableRadioButton" Width="106px"
											Height="22px" AutoPostBack="True" GroupName="QueryByRoute" Enabled="False"></asp:radiobutton><asp:radiobutton id="rbShortRoute" runat="server" Text="Delivery/CL" CssClass="tableRadioButton"
											Width="131px" Height="22px" AutoPostBack="True" GroupName="QueryByRoute" Checked="True"></asp:radiobutton><asp:radiobutton id="rbAirRoute" runat="server" Text="Air Route" CssClass="tableRadioButton" Width="120px"
											Height="22px" AutoPostBack="True" GroupName="QueryByRoute" Enabled="False"></asp:radiobutton></TD>
								</TR>
								<tr>
									<TD class="tableLabel" width="20%">&nbsp;<asp:label id="lblRouteCode" runat="server" CssClass="tableLabel" Width="111px" Height="22px">Route Code</asp:label></TD>
									<td><DBCOMBO:DBCOMBO id="DbComboPathCode" tabIndex="14" CssClass="tableLabel" Width="25px" AutoPostBack="True"
											Runat="server" TextBoxColumns="18" RegistrationKey=" " TextUpLevelSearchButton="v" ShowDbComboLink="False"
											ServerMethod="DbComboPathCodeSelect"></DBCOMBO:DBCOMBO></td>
								</tr>
								<TR>
									<TD class="tableLabel">&nbsp;<asp:label id="Label2" runat="server" CssClass="tableLabel" Width="154px" Height="22px">Origin Distribution Center</asp:label></TD>
									<TD><DBCOMBO:DBCOMBO id="DbComboOriginDC" tabIndex="14" CssClass="tableLabel" Width="25px" AutoPostBack="True"
											Runat="server" TextBoxColumns="18" RegistrationKey=" " TextUpLevelSearchButton="v" ShowDbComboLink="False"
											ServerMethod="DbComboDistributionCenterSelect"></DBCOMBO:DBCOMBO></TD>
								</TR>
								<TR>
									<TD class="tableLabel">&nbsp;<asp:label id="Label3" runat="server" CssClass="tableLabel" Width="181px" Height="22px">Destination Distribution Center</asp:label></TD>
									<TD><DBCOMBO:DBCOMBO id="DbComboDestinationDC" tabIndex="14" CssClass="tableLabel" Width="25px" AutoPostBack="True"
											Runat="server" TextBoxColumns="18" RegistrationKey=" " TextUpLevelSearchButton="v" ShowDbComboLink="False"
											ServerMethod="DbComboDistributionCenterSelect"></DBCOMBO:DBCOMBO></TD>
								</TR>
							</TABLE>
						</fieldset>
					</TD>
					<TD style="HEIGHT: 134px" vAlign="top">
						<fieldset style="WIDTH: 320px; HEIGHT: 121px"><legend><asp:label id="lblDestination" runat="server" CssClass="tableHeadingFieldset" Width="79px"
									Font-Bold="True">Destination</asp:label></legend>
							<TABLE id="tblDestination" style="WIDTH: 300px; HEIGHT: 57px" cellSpacing="0" cellPadding="0"
								align="left" border="0" runat="server">
								<tr height="37">
									<td style="HEIGHT: 33px"></td>
									<TD class="tableLabel" style="WIDTH: 360px; HEIGHT: 33px" colSpan="3">&nbsp;
										<asp:label id="lblZipCode" runat="server" CssClass="tableLabel" Width="84px" Height="22px">Postal Code</asp:label>&nbsp;&nbsp;&nbsp;
										<cc1:mstextbox id="txtZipCode" runat="server" CssClass="textField" Width="139px" MaxLength="10"
											TextMaskType="msUpperAlfaNumericWithHyphen"></cc1:mstextbox>&nbsp;&nbsp;
										<asp:button id="btnZipCode" runat="server" Text="..." CssClass="searchButton" CausesValidation="False"></asp:button></TD>
									<td style="HEIGHT: 33px"></td>
								</tr>
								<tr>
									<td style="HEIGHT: 33px"></td>
									<TD class="tableLabel" style="WIDTH: 360px; HEIGHT: 33px" colSpan="3">&nbsp;
										<asp:label id="lblStateCode" runat="server" CssClass="tableLabel" Width="100px" Height="22px">State / Province</asp:label><cc1:mstextbox id="txtStateCode" runat="server" CssClass="textField" Width="139" Height="22" MaxLength="10"
											TextMaskType="msUpperAlfaNumericWithUnderscore"></cc1:mstextbox>&nbsp;&nbsp;
										<asp:button id="btnStateCode" runat="server" Text="..." CssClass="searchButton" CausesValidation="False"></asp:button></TD>
									<td style="HEIGHT: 33px"></td>
								</tr>
							</TABLE>
						</fieldset>
						<FIELDSET style="Z-INDEX: 0; WIDTH: 320px; HEIGHT: 50px"><LEGEND>
								<asp:label id="Label8" runat="server" Width="111px" CssClass="tableHeadingFieldset" Font-Bold="True">Service Type</asp:label></LEGEND>
							<TABLE style="WIDTH: 312px" id="Table3" border="0" cellSpacing="0" cellPadding="0" align="left"
								runat="server">
								<TR>
									<TD>&nbsp;
										<asp:label id="lblServiceCode" runat="server" Width="100px" CssClass="tableLabel">Service Code</asp:label></TD>
									<TD style="WIDTH: 197px" class="tableLabel" colSpan="3">
										<dbcombo:dbcombo id="dbCmbServiceCode" tabIndex="1" runat="server" Width="80px" AutoPostBack="True"
											ServerMethod="ServiceCodeServerMethod" ShowDbComboLink="False" TextUpLevelSearchButton="v" TextBoxColumns="4"></dbcombo:dbcombo></TD>
								</TR>
							</TABLE>
						</FIELDSET>
						<P>&nbsp;</P>
					</TD>
				</TR>
			</TABLE>
			<asp:label id="Label1" style="Z-INDEX: 102; POSITION: absolute; TOP: 4px; LEFT: 20px" runat="server"
				CssClass="maintitleSize" Width="365px" Height="27px">POD Report</asp:label></TR></TABLE></TR></TABLE><input 
style="Z-INDEX: 106; POSITION: absolute; TOP: 35px; LEFT: 222px" type=hidden 
value="<%=strScrollPosition%>" name=ScrollPosition>
		</form>
	</body>
</HTML>
