<%@ Import Namespace="com.common.util" %>
<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<%@ Page language="c#" Codebehind="MasterAccount.aspx.cs" AutoEventWireup="false" Inherits="com.ties.MasterAccount" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Zone</title>
		<LINK rel="stylesheet" type="text/css" href="css/Styles.css" name="stylesheet">
		<meta name="GENERATOR" content="Microsoft Visual Studio 7.0">
		<meta name="CODE_LANGUAGE" content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<!--#INCLUDE FILE="msFormValidations.inc"-->
		<script runat="server">
		</script>
		<script language="javascript" type="text/javascript">
			function confirmDel()
			{
				if(confirm('Are you sure you want to delete this Master Account record?'))
				{
					return true;
				}
				else
				{
					return false;
				}
			}
		</script>
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Zone" method="post" runat="server">
			<asp:button style="Z-INDEX: 101; POSITION: absolute; TOP: 62px; LEFT: 30px" id="btnQuery" runat="server"
				Text="Query" CssClass="queryButton" CausesValidation="False" Width="72px"></asp:button><asp:button style="Z-INDEX: 102; POSITION: absolute; TOP: 62px; LEFT: 102px" id="btnExecuteQuery"
				runat="server" Text="Execute Query" CssClass="queryButton" CausesValidation="False" Width="130px"></asp:button><asp:button style="Z-INDEX: 103; POSITION: absolute; TOP: 62px; LEFT: 232px" id="btnInsert"
				runat="server" Text="Insert" CssClass="queryButton" CausesValidation="False"></asp:button><asp:datagrid style="Z-INDEX: 104; POSITION: absolute; TOP: 138px; LEFT: 29px" id="dgZone" runat="server"
				Width="330px" ItemStyle-Height="20" AutoGenerateColumns="False" OnEditCommand="dgZone_Edit" OnCancelCommand="dgZone_Cancel" OnDeleteCommand="dgZone_Delete" OnUpdateCommand="dgZone_Update"
				OnItemDataBound="dgZone_Bound" OnPageIndexChanged="dgZone_PageChange" AllowCustomPaging="True" AllowPaging="True">
				<ItemStyle Height="20px" CssClass="gridField"></ItemStyle>
				<Columns>
					<asp:TemplateColumn HeaderText="Zone Code">
						<HeaderStyle Font-Bold="True" CssClass="gridHeading" Wrap="False" HorizontalAlign="Center"></HeaderStyle>
						<ItemStyle HorizontalAlign="Center" CssClass="gridField"></ItemStyle>
						<HeaderTemplate>
							<FONT face="Tahoma">Credit Term</FONT>
						</HeaderTemplate>
						<EditItemTemplate>
							<asp:ImageButton ID="btncreditterm" CommandName ="linkterm" OnCommand = "dg_LinkCreditTerm" CommandArgument = '<%#DataBinder.Eval(Container.DataItem,"master_account")%>' Runat="server" ImageUrl="images\icons\BandQuotation.ico  " />
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:EditCommandColumn ButtonType="LinkButton" UpdateText="&lt;IMG SRC='images/butt-update.gif' border=0 title='Update' &gt;"
						 ItemStyle-Wrap="false" CancelText="&lt;IMG SRC='images/butt-cancel.gif' border=0 title='Cancel' &gt;" EditText="&lt;IMG SRC='images/butt-edit.gif' border=0 title='Edit' &gt;">
						<HeaderStyle Wrap="false" Width="10%" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle Wrap="false"  Width="10%" HorizontalAlign="Center"></ItemStyle>
					</asp:EditCommandColumn>
					<asp:TemplateColumn>
						<HeaderStyle Font-Bold="True" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle  Width="10%"  HorizontalAlign="Center" CssClass="gridField"></ItemStyle>
						<ItemTemplate>
							<asp:ImageButton ID="btndel" CommandName="del" OnCommand="dg_LinkCreditTerm" Runat="server" ImageUrl="images/butt-delete.gif " />
						</ItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Zone Code">
						<HeaderStyle Font-Bold="True" Width="60%" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle CssClass="gridField"></ItemStyle>
						<HeaderTemplate>
							<FONT face="Tahoma">Master Account</FONT>
						</HeaderTemplate>
						<ItemTemplate>
							<asp:Label id=lblZoneCode CssClass="gridLabel" Text='<%#DataBinder.Eval(Container.DataItem,"master_account")%>' Runat="server">
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<cc1:mstextbox id=txtZoneCode CssClass="gridTextBox" Text='<%#DataBinder.Eval(Container.DataItem,"master_account")%>' Runat="server" MaxLength="20" TextMaskType="msUpperAlfaNumericWithUnderscore" Enabled="True">
							</cc1:mstextbox>
							<asp:RequiredFieldValidator id="zcValidator" Runat="server" ErrorMessage="Master Account is required field"
								ControlToValidate="txtZoneCode" Display="None" BorderWidth="0"></asp:RequiredFieldValidator>
						</EditItemTemplate>
					</asp:TemplateColumn>
				</Columns>
				<PagerStyle NextPageText="Next" PrevPageText="Previous" HorizontalAlign="Right" CssClass="normalText"></PagerStyle>
			</asp:datagrid><asp:label style="Z-INDEX: 111; POSITION: absolute; TOP: 12px; LEFT: 29px" id="lblTitle" runat="server"
				CssClass="mainTitleSize" Width="477px" Height="26px">Master Account</asp:label><asp:label style="Z-INDEX: 105; POSITION: absolute; TOP: 92px; LEFT: 29px" id="lblErrorMessage"
				runat="server" CssClass="errorMsgColor" Width="538px" Height="31px" DESIGNTIMEDRAGDROP="63">Place holder for err msg</asp:label><asp:validationsummary style="Z-INDEX: 111; POSITION: absolute; TOP: 96px; LEFT: 90px" id="ValidationSummary1"
				Width="479px" Height="36px" Runat="server" ShowMessageBox="True" DisplayMode="SingleParagraph" ShowSummary="False"></asp:validationsummary></form>
	</body>
</HTML>
