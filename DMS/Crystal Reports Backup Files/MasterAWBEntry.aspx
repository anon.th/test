<%@ Page language="c#" Codebehind="MasterAWBEntry.aspx.cs" AutoEventWireup="false" Inherits="TIES.WebUI.MasterAWBEntry" %>
<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Master AWB Entry</title>
		<meta content="True" name="vs_snapToGrid">
		<meta content="True" name="vs_showGrid">
		<LINK href="css/Styles.css" type="text/css" rel="stylesheet" name="stylesheet">
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<!--#INCLUDE FILE="msFormValidations.inc"-->
		<script language="javascript" src="Scripts/settingScrollPosition.js"></script>
		<script language="javascript" src="Scripts/JScript_TextControl.js"></script>
		<script>function funcDisBack()
{history.go(+1);}
		</script>
	</HEAD>
	<body onunload="window.top.displayBanner.fnCloseAll(0);" MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<div style="Z-INDEX: 102; POSITION: relative; WIDTH: 1007px; HEIGHT: 1248px; TOP: 32px; LEFT: 24px"
				id="divMain" MS_POSITIONING="GridLayout" runat="server">
				<TABLE style="Z-INDEX: 101; POSITION: absolute; TOP: 8px; LEFT: 0px" id="MainTable" border="0"
					width="100%" runat="server">
					<TBODY>
						<tr>
							<td colSpan="2" align="left"><asp:label id="Label1" runat="server" Width="421px" Height="27px" CssClass="mainTitleSize">
							Master AWB(Manifest) Entry</asp:label></td>
						</tr>
						<tr>
							<td style="HEIGHT: 26px" vAlign="top" colSpan="2" align="left">
								<asp:button id="btnQry" runat="server" CssClass="queryButton" Text="Query" CausesValidation="False"></asp:button>
								<label style="WIDTH:5px"></label>
								<asp:button id="btnExecQry" runat="server" CssClass="queryButton" Text="Execute Query" CausesValidation="False"></asp:button>
								<label style="WIDTH:5px"></label>
								<asp:button id="btnPrintConsNote" runat="server" CssClass="queryButton" Text="Insert" CausesValidation="False"></asp:button>
								<label style="WIDTH:5px"></label>
								<asp:button id="btnSave" runat="server" CssClass="queryButton" Text="Save" CausesValidation="False"
									Enabled="False"></asp:button>
								<label style="WIDTH:5px"></label>
								<asp:button style="Z-INDEX: 0" id="Button1" runat="server" CssClass="queryButton" CausesValidation="False"
									Text=" Verify " Enabled="False"></asp:button>
								<label style="WIDTH:5px"></label>
								<asp:button style="Z-INDEX: 0" id="Button2" runat="server" CssClass="queryButton" CausesValidation="False"
									Text="Export " Enabled="False"></asp:button></td>
						</tr>
						<tr>
							<td vAlign="top" colSpan="2" align="left">
								<TABLE style="Z-INDEX: 112; WIDTH: 1000px" id="Table1" border="0" width="730" runat="server">
									<TR width="100%">
										<TD width="100%">
											<table border="0" cellSpacing="1" cellPadding="1" width="100%">
												<TBODY>
													<TR>
														<TD style="HEIGHT: 19px" colSpan="6"><asp:label id="lblError" runat="server" Font-Size="X-Small" Font-Bold="True" ForeColor="Red"></asp:label></TD>
													</TR>
													<tr>
														<td style="WIDTH: 160px"><asp:label id="Label4" runat="server" CssClass="tableLabel"> Master AWB Number:</asp:label><asp:label id="Label20" runat="server" CssClass="tableLabel" ForeColor="Red">*</asp:label></td>
														<td style="WIDTH: 250px">
															<asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase" id="txtAWBNumber" tabIndex="15" runat="server"
																CssClass="textField" Width="150px" MaxLength="100"></asp:textbox></td>
														<td><asp:label id="Label5" runat="server" CssClass="tableLabel"> Customs Office:</asp:label><asp:label id="Label22" runat="server" CssClass="tableLabel" ForeColor="Red">*</asp:label></td>
														<td colSpan="3">
															<asp:dropdownlist style="Z-INDEX: 0" id="ddlCustomsOffice" tabIndex="16" runat="server" Height="20px"
																Width="100px" AutoPostBack="True"></asp:dropdownlist><FONT face="Tahoma">
																<asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea" id="txtCustomsOffice"
																	runat="server" CssClass="textField" Width="164px" Enabled="False"></asp:textbox></FONT></td>
													</tr>
													<tr>
														<td><asp:label id="Label6" runat="server" CssClass="tableLabel"> Ship/Flight No:</asp:label><asp:label id="Label23" runat="server" CssClass="tableLabel" ForeColor="Red">*</asp:label></td>
														<td style="WIDTH: 235px; HEIGHT: 23px">
															<asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase" id="Textbox1" tabIndex="15" runat="server"
																CssClass="textField" Width="150px" MaxLength="100"></asp:textbox></td>
														<td><asp:label id="Label7" runat="server" CssClass="tableLabel">Entry by:</asp:label></td>
														<td colSpan="3" style="HEIGHT: 23px">
															<asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea" id="Textbox2"
																runat="server" CssClass="textField" Width="97px" Enabled="False"></asp:textbox><FONT face="Tahoma"></FONT>
															<asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea" id="Textbox3"
																runat="server" CssClass="textField" Width="165px" Enabled="False"></asp:textbox></td>
													</tr>
													<tr>
														<td colSpan="6" align="left">
															<table border="0" cellSpacing="1" cellPadding="1" width="800">
																<tr>
																	<td style="WIDTH: 150px"></td>
																	<td style="WIDTH: 100px"><asp:label id="Label2" runat="server" CssClass="tableLabel">Date</asp:label></td>
																	<td style="WIDTH: 100px"><asp:label id="Label3" runat="server" CssClass="tableLabel">Time</asp:label></td>
																	<td style="WIDTH: 50px"><asp:label id="Label8" runat="server" CssClass="tableLabel">City</asp:label></td>
																	<td><asp:label id="Label9" runat="server" CssClass="tableLabel">Country</asp:label></td>
																</tr>
																<tr>
																	<td style="WIDTH: 160px"><asp:label id="Label10" runat="server" CssClass="tableLabel">Departure:</asp:label>
																		<asp:label style="Z-INDEX: 0" id="Label15" runat="server" CssClass="tableLabel" ForeColor="Red">*</asp:label></td>
																	<td>
																		<cc1:mstextbox style="Z-INDEX: 0" id="txtDateFrom" tabIndex="21" runat="server" CssClass="textField"
																			Width="90px" MaxLength="12" TextMaskString="99/99/9999" TextMaskType="msDate"></cc1:mstextbox></td>
																	<td></td>
																	<td>
																		<asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase" id="txtCity" tabIndex="19" runat="server"
																			CssClass="textField" Width="42px" AutoPostBack="True"></asp:textbox></td>
																	<td>
																		<asp:dropdownlist style="Z-INDEX: 0" id="Dropdownlist2" tabIndex="31" runat="server" Width="80px"></asp:dropdownlist><FONT face="Tahoma"></FONT>
																		<asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea" id="Textbox16"
																			tabIndex="14" runat="server" CssClass="textField" Width="180px" Enabled="False" MaxLength="100"
																			ReadOnly="True"></asp:textbox></td>
																</tr>
																<tr>
																	<td><asp:label id="Label14" runat="server" CssClass="tableLabel" style="Z-INDEX: 0">Arrival:</asp:label>
																		<asp:label style="Z-INDEX: 0" id="Label17" runat="server" CssClass="tableLabel" ForeColor="Red">*</asp:label></td>
																	<td>
																		<cc1:mstextbox style="Z-INDEX: 0" id="Mstextbox1" tabIndex="21" runat="server" CssClass="textField"
																			Width="90px" MaxLength="12" TextMaskString="99/99/9999" TextMaskType="msDate"></cc1:mstextbox></td>
																	<td>
																		<cc1:mstextbox style="Z-INDEX: 0" id="Mstextbox2" tabIndex="21" runat="server" CssClass="textField"
																			Width="90px" MaxLength="12" TextMaskString="99:99" TextMaskType="msTime"></cc1:mstextbox></td>
																	<td><FONT face="Tahoma">
																			<asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase" id="Textbox4" tabIndex="19" runat="server"
																				CssClass="textField" Width="42px" AutoPostBack="True"></asp:textbox></FONT></td>
																	<td>
																		<asp:dropdownlist style="Z-INDEX: 0" id="Dropdownlist1" tabIndex="31" runat="server" Width="80px"></asp:dropdownlist><FONT face="Tahoma"></FONT>
																		<asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea" id="Textbox5"
																			tabIndex="14" runat="server" CssClass="textField" Width="180px" Enabled="False" MaxLength="100"
																			ReadOnly="True"></asp:textbox></td>
																</tr>
															</table>
														</td>
													</tr>
													<tr>
														<td><asp:label style="Z-INDEX: 0" id="Label16" runat="server" CssClass="tableLabel"> Carrier:</asp:label><asp:label id="Label27" runat="server" CssClass="tableLabel" ForeColor="Red">*</asp:label></td>
														<td colspan="5"><FONT face="Tahoma">
																<asp:dropdownlist style="Z-INDEX: 0" id="Dropdownlist3" tabIndex="31" runat="server" Width="80px"></asp:dropdownlist>
																<asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea" id="Textbox6"
																	tabIndex="14" runat="server" CssClass="textField" Width="180px" Enabled="False" MaxLength="100"
																	ReadOnly="True"></asp:textbox></FONT></td>
													</tr>
													<tr>
														<td><asp:label style="Z-INDEX: 0" id="Label31" runat="server" CssClass="tableLabel"> Totals � Weight:</asp:label><asp:label id="Label28" runat="server" CssClass="tableLabel" ForeColor="Red">*</asp:label></td>
														<td style="WIDTH: 235px">
															<asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase" id="Textbox7" tabIndex="15" runat="server"
																CssClass="textField" Width="85px" MaxLength="100"></asp:textbox></td>
														<td>
															<asp:label style="Z-INDEX: 0" id="Label11" runat="server" CssClass="tableLabel">House AWBs:</asp:label>
															<asp:label style="Z-INDEX: 0" id="Label18" runat="server" CssClass="tableLabel" ForeColor="Red">*</asp:label></td>
														<td><FONT face="Tahoma">
																<asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase" id="Textbox8" tabIndex="15" runat="server"
																	CssClass="textField" Width="90px" MaxLength="100"></asp:textbox></FONT></td>
														<td></td>
														<td></td>
													</tr>
													<tr>
														<td><FONT face="Tahoma">
																<asp:label style="Z-INDEX: 0" id="lblHCSupportedbyEnterprise" runat="server" CssClass="tableLabel">Mode of Transport: </asp:label>
																<asp:label style="Z-INDEX: 0" id="Label19" runat="server" CssClass="tableLabel" ForeColor="Red">*</asp:label></FONT></td>
														<td colspan="5"><FONT face="Tahoma">
																<asp:dropdownlist style="Z-INDEX: 0" id="Dropdownlist4" tabIndex="31" runat="server" Width="80px"></asp:dropdownlist>
																<asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea" id="Textbox9"
																	tabIndex="14" runat="server" CssClass="textField" Width="180px" Enabled="False" MaxLength="100"
																	ReadOnly="True"></asp:textbox></FONT></td>
													</tr>
													<tr>
														<td><asp:label style="Z-INDEX: 0" id="Label32" runat="server" CssClass="tableLabel"> Remaining:</asp:label></td>
														<td></td>
														<td><FONT face="Tahoma"></FONT></td>
														<td colspan="3" vAlign="middle"><FONT face="Tahoma"></FONT></td>
													</tr>
													<tr>
														<td><asp:label style="Z-INDEX: 0" id="Label33" runat="server" CssClass="tableLabel">Weight:</asp:label></td>
														<td>
															<asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea" id="Textbox10"
																tabIndex="14" runat="server" CssClass="textField" Width="90px" Enabled="False" MaxLength="100"
																ReadOnly="True"></asp:textbox></td>
														<td><FONT face="Tahoma">
																<asp:label style="Z-INDEX: 0" id="Label12" runat="server" CssClass="tableLabel">Verified by: </asp:label></FONT></td>
														<td colspan="3" vAlign="middle">
															<asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea" id="Textbox12"
																tabIndex="14" runat="server" CssClass="textField" Width="90px" Enabled="False" MaxLength="100"
																ReadOnly="True"></asp:textbox><FONT face="Tahoma">
																<asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea" id="Textbox14"
																	runat="server" CssClass="textField" Width="165px" Enabled="False"></asp:textbox></FONT></td>
													</tr>
													<tr>
														<td><asp:label style="Z-INDEX: 0" id="Label34" runat="server" CssClass="tableLabel"> AWBs:</asp:label></td>
														<td>
															<asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea" id="Textbox11"
																tabIndex="14" runat="server" CssClass="textField" Width="90px" Enabled="False" MaxLength="100"
																ReadOnly="True"></asp:textbox></td>
														<td style="WIDTH: 102px"><FONT face="Tahoma">
																<asp:label style="Z-INDEX: 0" id="Label13" runat="server" CssClass="tableLabel"> Exported by: </asp:label></FONT></td>
														<td colspan="3" vAlign="middle">
															<asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea" id="Textbox13"
																tabIndex="14" runat="server" CssClass="textField" Width="90px" Enabled="False" MaxLength="100"
																ReadOnly="True"></asp:textbox><FONT face="Tahoma">
																<asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea" id="Textbox15"
																	runat="server" CssClass="textField" Width="165px" Enabled="False"></asp:textbox></FONT></td>
													</tr>
													<tr>
														<td colSpan="6"><FONT face="Tahoma"></FONT><br>
														</td>
													</tr>
													<tr>
														<td colSpan="6" align="left">
															<table width="900px" border="0" cellpadding="0" cellspacing="0">
																<tr>
																	<td class="gridHeading">
																		<STRONG><FONT size="2">AWB&nbsp;Details</FONT></STRONG>
																	</td>
																</tr>
															</table>
														</td>
													</tr>
													<tr>
														<td colSpan="6">
															<asp:datagrid style="Z-INDEX: 0" id="PackageDetails" tabIndex="42" runat="server" Width="900px"
																FooterStyle-CssClass="gridHeading" BorderWidth="0px" CellSpacing="1" CellPadding="0" HeaderStyle-CssClass="gridHeading"
																ItemStyle-CssClass="gridField" AlternatingItemStyle-CssClass="gridField" DataKeyField="No"
																ShowFooter="True" AutoGenerateColumns="False">
																<FooterStyle CssClass="gridHeading"></FooterStyle>
																<AlternatingItemStyle CssClass="gridField"></AlternatingItemStyle>
																<ItemStyle CssClass="gridField"></ItemStyle>
																<HeaderStyle CssClass="gridHeading"></HeaderStyle>
																<Columns>
																	<asp:TemplateColumn>
																		<HeaderStyle Width="11%"></HeaderStyle>
																		<ItemStyle HorizontalAlign="Center"></ItemStyle>
																		<ItemTemplate>
																			<asp:LinkButton id="btnEditItem" Runat="server" Text="<img src='images/butt-edit.gif' alt='edit' border=0>"
																				CommandName="EDIT_ITEM" />
																			<asp:LinkButton id="btnDeleteItem" Runat="server" Text="<img src='images/butt-delete.gif' alt='delete' border=0>"
																				CommandName="DELETE_ITEM" />
																		</ItemTemplate>
																		<FooterStyle HorizontalAlign="Center"></FooterStyle>
																		<FooterTemplate>
																			<asp:LinkButton id="btnAddItem" Runat="server" Text="<img src='images/butt-add.gif' alt='add' border=0>"
																				CommandName="ADD_ITEM" />
																		</FooterTemplate>
																		<EditItemTemplate>
																			<asp:LinkButton id="btnSaveItem" Runat="server" Text="<img src='images/butt-update.gif' alt='save' border=0>"
																				CommandName="SAVE_ITEM" />
																			<asp:LinkButton id="btnCancelItem" Runat="server" Text="<img src='images/butt-red.gif' alt='cancel' border=0>"
																				CommandName="CANCEL_ITEM" />
																		</EditItemTemplate>
																	</asp:TemplateColumn>
																	<asp:TemplateColumn HeaderText="Number">
																		<HeaderStyle HorizontalAlign="Center" Width="10%"></HeaderStyle>
																		<ItemStyle HorizontalAlign="Center"></ItemStyle>
																		<ItemTemplate>
																			<%# DataBinder.Eval(Container.DataItem,"Number") %>
																		</ItemTemplate>
																		<FooterStyle HorizontalAlign="Center"></FooterStyle>
																		<EditItemTemplate>
																			<asp:textbox id="txtNumber" runat="server" CssClass="gridTextBoxNumber" Text='<%#DataBinder.Eval(Container.DataItem,"Number")%>'>
																			</asp:textbox>
																		</EditItemTemplate>
																	</asp:TemplateColumn>
																	<asp:TemplateColumn HeaderText="Exporter">
																		<HeaderStyle HorizontalAlign="Center" Width="10%"></HeaderStyle>
																		<ItemStyle HorizontalAlign="Center"></ItemStyle>
																		<ItemTemplate>
																			<%# DataBinder.Eval(Container.DataItem,"Exporter") %>
																		</ItemTemplate>
																		<FooterStyle HorizontalAlign="Center"></FooterStyle>
																		<EditItemTemplate>
																			<asp:textbox id="Textbox18" runat="server" CssClass="gridTextBoxNumber" Text='<%#DataBinder.Eval(Container.DataItem,"Exporter")%>'>
																			</asp:textbox>
																		</EditItemTemplate>
																	</asp:TemplateColumn>
																	<asp:TemplateColumn HeaderText="Consignee">
																		<HeaderStyle HorizontalAlign="Center" Width="10%"></HeaderStyle>
																		<ItemStyle HorizontalAlign="Center"></ItemStyle>
																		<ItemTemplate>
																			<%# DataBinder.Eval(Container.DataItem,"Consignee") %>
																		</ItemTemplate>
																		<FooterStyle HorizontalAlign="Center"></FooterStyle>
																		<EditItemTemplate>
																			<asp:textbox id="Textbox20" runat="server" CssClass="gridTextBoxNumber" Text='<%#DataBinder.Eval(Container.DataItem,"Consignee")%>'>
																			</asp:textbox>
																		</EditItemTemplate>
																	</asp:TemplateColumn>
																	<asp:TemplateColumn HeaderText="Qty">
																		<HeaderStyle HorizontalAlign="Center" Width="10%"></HeaderStyle>
																		<ItemStyle HorizontalAlign="Center"></ItemStyle>
																		<ItemTemplate>
																			<%# DataBinder.Eval(Container.DataItem,"Qty", "{0:#,##0.00}") %>
																		</ItemTemplate>
																		<FooterStyle HorizontalAlign="Center"></FooterStyle>
																		<EditItemTemplate>
																			<asp:textbox id="Textbox22" runat="server" CssClass="gridTextBoxNumber" Text='<%#DataBinder.Eval(Container.DataItem,"Qty")%>'>
																			</asp:textbox>
																		</EditItemTemplate>
																	</asp:TemplateColumn>
																</Columns>
															</asp:datagrid></td>
													</tr>
												</TBODY>
											</table>
										</TD>
									</TR>
								</TABLE>
								<DIV></DIV>
							</td>
						</tr>
					</TBODY>
				</TABLE>
			</div>
		</form>
	</body>
</HTML>
