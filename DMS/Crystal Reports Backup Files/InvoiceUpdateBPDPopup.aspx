<%@ Page language="c#" Codebehind="InvoiceUpdateBPDPopup.aspx.cs" AutoEventWireup="false" Inherits="TIES.WebUI.InvoiceUpdateBPDPopup" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>InvoiceUpdateBPDPopup</title>
		<LINK href="css/Styles.css" type="text/css" rel="stylesheet" name="stylesheet">
		<meta name="GENERATOR" Content="Microsoft Visual Studio 7.0">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<asp:button id="btnQry" style="Z-INDEX: 104; LEFT: 185px; POSITION: absolute; TOP: 279px" runat="server"
				CausesValidation="False" Width="82px" Text="OK" CssClass="queryButton"></asp:button>
			<TABLE id="Table3" style="Z-INDEX: 105; LEFT: 27px; WIDTH: 500px; POSITION: absolute; TOP: 21px; HEIGHT: 228px; TEXT-ALIGN: left"
				cellSpacing="0" width="500" align="left" border="0" runat="server">
				<TR height="25">
					<TD style="WIDTH: 159px; HEIGHT: 30px" align="left" width="159"><FONT face="Tahoma">
							<asp:label id="Label52" CssClass="tableLabel" Runat="server">Customer ID:</asp:label></FONT></TD>
					<TD style="WIDTH: 132px; HEIGHT: 30px" width="132">
						<asp:label id="Label6" CssClass="tableLabel" Runat="server">PCAM</asp:label></TD>
					<TD style="WIDTH: 453px; HEIGHT: 30px" width="453"></TD>
					<TD style="HEIGHT: 30px" width="300"><FONT face="Tahoma"></FONT></TD>
				</TR>
				<TR>
					<TD style="WIDTH: 159px; HEIGHT: 30px" align="left" width="159"><FONT face="Tahoma">
							<asp:label id="Label7" CssClass="tableLabel" Runat="server">Invoice Number:</asp:label></FONT></TD>
					<TD style="WIDTH: 132px; HEIGHT: 30px" width="132"><FONT face="Tahoma">
							<asp:label id="Label8" CssClass="tableLabel" Runat="server">IND0920092</asp:label></FONT></TD>
					<TD style="WIDTH: 453px; HEIGHT: 30px" width="453"><FONT face="Tahoma">
							<asp:label id="Label9" CssClass="tableLabel" Runat="server">Last Updated By</asp:label></FONT></TD>
					<TD style="HEIGHT: 30px" width="300">
						<asp:label id="Label10" CssClass="tableLabel" Runat="server">Last Updated Date</asp:label></TD>
				</TR>
				<TR>
					<TD style="WIDTH: 159px; HEIGHT: 30px" align="left" width="159">
						<asp:label id="Label1" CssClass="tableLabel" Runat="server">Due Date:</asp:label></TD>
					<TD style="WIDTH: 132px; HEIGHT: 30px" width="132">
						<asp:textbox id="Textbox5" style="TEXT-TRANSFORM: uppercase" tabIndex="4" runat="server" Width="97px"
							CssClass="textField" MaxLength="20"></asp:textbox></TD>
					<TD style="WIDTH: 453px; HEIGHT: 30px" width="453"><FONT face="Tahoma">
							<asp:label id="Label14" CssClass="tableLabel" Runat="server">Nam</asp:label></FONT></TD>
					<TD style="HEIGHT: 30px" width="300">
						<asp:label id="Label17" Width="35px" CssClass="tableLabel" Runat="server">25/06/2009</asp:label></TD>
				</TR>
				<TR>
					<TD style="WIDTH: 159px; HEIGHT: 30px" align="left" width="159">
						<asp:label id="Label2" CssClass="tableLabel" Runat="server">Actual BPD:</asp:label></TD>
					<TD style="WIDTH: 132px; HEIGHT: 30px" width="132">
						<asp:textbox id="Textbox1" style="TEXT-TRANSFORM: uppercase" tabIndex="4" runat="server" Width="97px"
							CssClass="textField" MaxLength="20"></asp:textbox></TD>
					<TD style="WIDTH: 453px; HEIGHT: 30px" width="453">
						<asp:label id="Label15" CssClass="tableLabel" Runat="server">Nam</asp:label></TD>
					<TD style="HEIGHT: 30px" width="300">
						<asp:label id="Label18" Width="35px" CssClass="tableLabel" Runat="server">25/06/2009</asp:label></TD>
				</TR>
				<TR>
					<TD style="WIDTH: 159px; HEIGHT: 30px" align="left" width="159">
						<asp:label id="Label3" CssClass="tableLabel" Runat="server">Promised Payment Date:</asp:label></TD>
					<TD style="WIDTH: 132px; HEIGHT: 30px" width="132">
						<asp:textbox id="Textbox2" style="TEXT-TRANSFORM: uppercase" tabIndex="4" runat="server" Width="97px"
							CssClass="textField" MaxLength="20"></asp:textbox></TD>
					<TD style="WIDTH: 453px; HEIGHT: 30px" width="453"></TD>
					<TD style="HEIGHT: 30px" width="300"></TD>
				</TR>
				<TR height="25">
					<TD style="WIDTH: 159px; HEIGHT: 26px" noWrap align="left" width="159"><FONT face="Tahoma">
							<asp:label id="Label4" CssClass="tableLabel" Runat="server">Current Amount Paid:</asp:label></FONT></TD>
					<TD style="WIDTH: 132px; HEIGHT: 26px" width="132">
						<asp:label id="Label11" CssClass="tableLabel" Runat="server">0</asp:label></TD>
					<TD style="WIDTH: 453px; HEIGHT: 26px" width="453"><FONT face="Tahoma">&nbsp; </FONT>
					</TD>
					<TD style="HEIGHT: 26px" width="300"><FONT face="Tahoma"></FONT></TD>
				</TR>
				<TR>
					<TD style="WIDTH: 159px; HEIGHT: 26px" noWrap align="left" width="159">
						<asp:label id="Label12" CssClass="tableLabel" Runat="server">Amount Paid:</asp:label></TD>
					<TD style="WIDTH: 132px; HEIGHT: 26px" width="132">
						<asp:textbox id="Textbox3" style="TEXT-TRANSFORM: uppercase" tabIndex="4" runat="server" Width="97px"
							CssClass="textField" MaxLength="20"></asp:textbox></TD>
					<TD style="WIDTH: 453px; HEIGHT: 26px" width="453"><FONT face="Tahoma">
							<asp:label id="Label5" CssClass="tableLabel" Runat="server">Nam</asp:label></FONT></TD>
					<TD style="HEIGHT: 26px" width="300">
						<asp:label id="Label19" Width="35px" CssClass="tableLabel" Runat="server">25/06/2009</asp:label></TD>
				</TR>
				<TR>
					<TD style="WIDTH: 159px; HEIGHT: 26px" noWrap align="left" width="159">
						<asp:label id="Label13" CssClass="tableLabel" Runat="server">Paid Date:</asp:label></TD>
					<TD style="WIDTH: 132px; HEIGHT: 26px" width="132">
						<asp:textbox id="Textbox4" style="TEXT-TRANSFORM: uppercase" tabIndex="4" runat="server" Width="97px"
							CssClass="textField" MaxLength="20"></asp:textbox></TD>
					<TD style="WIDTH: 453px; HEIGHT: 26px" width="453">
						<asp:label id="Label16" CssClass="tableLabel" Runat="server">Bob</asp:label></TD>
					<TD style="HEIGHT: 26px" width="300">
						<asp:label id="Label20" Width="35px" CssClass="tableLabel" Runat="server">26/06/2009</asp:label></TD>
				</TR>
			</TABLE>
			<asp:button id="Button1" style="Z-INDEX: 106; LEFT: 293px; POSITION: absolute; TOP: 279px" runat="server"
				CausesValidation="False" Width="83px" Text="Cancel" CssClass="queryButton"></asp:button>
		</form>
	</body>
</HTML>
