<%@ Page language="c#" Codebehind="CustomsFreightCollectMatching.aspx.cs" AutoEventWireup="false" Inherits="TIES.WebUI.CustomsFreightCollectMatching" %>
<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<%@ Import Namespace="com.common.util" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
  <HEAD>
		<title>CustomsFreightCollectMatching</title>
		<meta name="vs_snapToGrid" content="True">
		<meta name="vs_showGrid" content="True">
		<LINK rel="stylesheet" type="text/css" href="css/Styles.css" name="stylesheet">
		<meta name="GENERATOR" content="Microsoft Visual Studio 7.0">
		<meta name="CODE_LANGUAGE" content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<!--#INCLUDE FILE="msFormValidations.inc"-->
		<script language="javascript" src="Scripts/settingScrollPosition.js"></script>
		<script language="javascript" src="Scripts/JScript_TextControl.js"></script>
		<script language="javascript" src="Scripts/WorkProgress.js"></script>
		<script language="javascript">
		<!--
				
		function setValue()
		{
			var s = document.all['inFile'];
			document.all['txtFilePath'].innerText = s.value;
			if(s.value != '') 
			{//debugger;
				document.all['btnImport'].disabled = false;
				//document.all['btnHiddenPostbackBrowse'].click();
				//__doPostBack(btnHiddenPostbackBrowse,'');
			}
			else
				document.all['btnImport'].disabled = true;
			
		}
		
		function upBrowse()
		{
			document.all['divBrowse'].style.backgroundImage='url(images/btn-browse-up.GIF)';
		}
		
		function downBrowse()
		{
			document.all['divBrowse'].style.backgroundImage='url(images/btn-browse-down.GIF)';
		}
		
		function clearValue()
		{
			if(document.all['txtFilePath'].innerText == '') {
				document.location.reload();
			}
		}
		
		function callCodeBehindSvcCode()
		{
			
		}
		
		function RemoveBadNonNumber(strTemp, obj) {
			strTemp = strTemp.replace(/[^0-9]/g, ''); //replace non-numeric
			obj.value = strTemp.replace(/\s/g, ''); // replace space
		}
		
		function AfterPasteNonNumber(obj) {
			setTimeout(function () {
				RemoveBadNonNumber(obj.value, obj);
			}, 1); //or 4
		}
		
		function validate(evt) {
			var theEvent = evt || window.event;
			var key = theEvent.keyCode || theEvent.which;
			key = String.fromCharCode( key );
			var regex = /[0-9]/;
			if( !regex.test(key) ) {
				theEvent.returnValue = false;
				if(theEvent.preventDefault) theEvent.preventDefault();
			}
		}
		
		function Check(chk)
		{
			b2 = document.getElementById('btnGenerateMatchingReport');
			all = document.getElementsByTagName("input");
			for(i=0;i<all.length;i++)
			{
				if(all[i].type=="checkbox" && all[i].id.indexOf("GrdAWBDetails") > -1)
				{
					if (all[i].checked)
					{
						b2.disabled = false;
						return;
					}
				}
			}
			//b.disabled = true;
			b2.disabled = true;
		}
		
		function CheckBoxCheck(rb) {
		b2 = document.getElementById('btnGenerateMatchingReport');
		
			var gv = document.getElementById('GrdAWBDetails');
			var chk = gv.getElementsByTagName("input");
			var row = rb.parentNode.parentNode;
			b2.disabled = true;
			for (i=0; i<chk.length;i++) {
				if(chk[i].checked )
				{
					b2.disabled = false;
					
					if(chk[i].checked && chk[i] != rb) {
						chk[i].checked = false;
						break;
					}
				}

			}
		}
		
		//-->
		
		function ImportFile()
		{
			document.getElementById("lblError").innerHTML = "";
			var year = document.getElementById("txtYear");
			var week = document.getElementById("ddlWeek");
			var btnBrowse = document.getElementById("btnBrowse");
			var btnImport = document.getElementById("btnImport");
			var btnQry = document.getElementById("btnQry");
			var btnExecQry = document.getElementById("btnExecQry");
			if(year.value=="")
			{
				document.getElementById("lblError").innerHTML = "Year is required.";
				return false;
			}
			
			if(week.selectedIndex==0)
			{
				document.getElementById("lblError").innerHTML = "Week number is required.";
				return false;
			} 
			btnBrowse.disabled=true; 
			btnQry.disabled=true; 
			btnExecQry.disabled=true; 
			//btnImport.disabled=true;
			//__doPostBack("btnImport");
			return true;
		}
		
		function uploadFile(value)
		{
			
			//var s = document.all['inFile'];
			document.all['txtFilePath'].innerText = value;
			if(value != '') 
			{
				var btnImport = document.getElementById("btnImport");
				btnImport.disabled = false;
				btnImport.focus();
			}
			else
			{
				//alert('xxx');
				var btnImport = document.getElementById("btnImport");
				
				btnImport.disabled=false;  
				
				document.all['txtFilePath'].innerText="";
			}
			
		}
		
		function getFile()
        {
            document.getElementById("inFile").click();
            //document.getElementById("txtFilePath").value = document.getElementById("inFile").value;
        }
        
        function disableImport()
		{
			var btnBrowse = document.getElementById("btnBrowse");
			var btnQry = document.getElementById("btnQry");
			var btnExecQry = document.getElementById("btnExecQry");
			var btnImport = document.getElementById("btnImport");
			var inFile = document.getElementById("inFile");
			var divFileUpload = document.getElementById("divFileUpload");
			btnBrowse.disabled = true;
			btnQry.disabled = true;
			btnExecQry.disabled = true;
			btnImport.disabled = true;
			inFile.onclick = function(){return false};
			divFileUpload.className = divFileUpload.className + " disabled";
			return true;
		}
		
		function FileNameChanged()
        {
			var selectedYear = document.getElementById("txtYear").value;
			var selectedWeek = 	document.getElementById("ddlWeek").selectedIndex;
			if (document.getElementById("txtFileName").value != "")
			{
				if (selectedYear == "")
				{
					document.getElementById("lblError").innerHTML = "Year is required.";
					document.getElementById("txtYear").focus();
					document.getElementById("btnImport").disabled = true;	
				}
				else if (selectedWeek == 0)
				{
					ocument.getElementById("lblError").innerHTML = "Week number is required.";
					document.getElementById("ddlWeek").focus();
					document.getElementById("btnImport").disabled = true;
				}
				else
				{
					document.getElementById("btnImport").disabled = false;
				}
			}
			else
			{
				document.getElementById("btnImport").disabled = true;				
			}	
        }
		
		</script>
</HEAD>
	<body MS_POSITIONING="GridLayout" onunload="window.top.displayBanner.fnCloseAll(0);">
		<form id="CustomsFreightCollectMatching" encType="multipart/form-data" method="post" runat="server">
			<div style="Z-INDEX: 102; POSITION: relative; WIDTH: 1008px; HEIGHT: 1543px; TOP: 34px; LEFT: 24px"
				id="divMain" MS_POSITIONING="GridLayout" runat="server">
				<TABLE style="Z-INDEX: 101; POSITION: absolute; TOP: 8px; LEFT: 0px" id="MainTable" border="0"
					width="100%" runat="server">
					<tbody>
						<tr>
							<td colSpan="2"><asp:label id="Label1" runat="server" CssClass="mainTitleSize" Height="27px" Width="808px">Customs Freight Collect Invoice Matching</asp:label></td>
						</tr>
						<tr>
							<td colSpan="2"><asp:button id="btnQry" runat="server" CssClass="queryButton" CausesValidation="False" Text="Query"></asp:button><asp:button id="btnExecQry" tabIndex="1" runat="server" CssClass="queryButton" CausesValidation="False"
									Text="Execute Query"></asp:button><asp:button id="btnGenerateMatchingReport" tabIndex="1" runat="server" CssClass="queryButton"
									Width="200" CausesValidation="False" Text="Generate Matching Report" Enabled="False"></asp:button></td>
						</tr>
						<tr>
							<td style="HEIGHT: 19px" colSpan="2"><asp:label id="lblError" runat="server" Font-Bold="True" Font-Size="X-Small" ForeColor="Red"></asp:label></td>
						</tr>
						<tr>
							<td style="WIDTH: 10%">
								<TABLE style="WIDTH: 168px; HEIGHT: 66px; id: " id="Table1" border="0" cellPadding="0"
									align="left" runat="server" tblDatescellSpacing="0">
									<tbody>
										<TR>
											<TD style="WIDTH: 55px"><asp:label style="Z-INDEX: 0" id="Label2" runat="server" CssClass="tableLabel">Year</asp:label></TD>
											<td><asp:textbox style="Z-INDEX: 0" id="txtYear" tabIndex="1" onkeypress="validate(event)" onpaste="AfterPasteNonNumber(this)"
													runat="server" CssClass="textField" Width="75" MaxLength="4"></asp:textbox></td>
										</TR>
										<TR>
											<TD style="WIDTH: 55px"><asp:label style="Z-INDEX: 0" id="Label3" runat="server" CssClass="tableLabel">Week</asp:label></TD>
											<td><asp:dropdownlist style="Z-INDEX: 0" id="ddlWeek" tabIndex="2" runat="server" Width="80"></asp:dropdownlist></td>
										</TR>
										<TR>
											<TD style="WIDTH: 55px"><asp:label style="Z-INDEX: 0" id="Label4" runat="server" CssClass="tableLabel">Shipper</asp:label></TD>
											<td><asp:dropdownlist style="Z-INDEX: 0" id="ddlShipper" tabIndex="3" runat="server" Width="80"></asp:dropdownlist></td>
										</TR>
									</tbody>
								</TABLE>
							</td>
							<td>
								<fieldset style="MARGIN: 5px; WIDTH: 334px; HEIGHT: 60px"><legend style="COLOR: black">Import 
										Freight Collect Invoice</legend>
									<TABLE style="WIDTH: 232px; HEIGHT: 46px" id="Table2" border="0" cellPadding="0" align="left"
										runat="server">
										<tbody>
											<TR>
												<TD>
													
													<asp:button id="btnBrowse" style="MARGIN: 5px 0px 0px 5px; DISPLAY: none" class="queryButton" runat="server"
														Text="Browse"></asp:button>
														
													<div id="divFileUpload" class="fileUpload queryButton">
														<span style="vertical-align: 50%;">Browse</span>
														<INPUT style="DISPLAY: block" id="inFile" onchange="uploadFile(this.value)" type="file"
														name="inFile" runat="server" enableViewState="True" class="upload">
													</div>
												</TD>
												<td>
													<asp:button style="MARGIN: 5px 0px 0px 5px" id="btnImport" tabIndex="1" runat="server" CssClass="queryButton"
														CausesValidation="False" Text="Import" Enabled="false"></asp:button></td>
											</TR>
											<tr>
												<TD><asp:label style="Z-INDEX: 0; MARGIN: 5px 0px 0px 5px" id="Label5" runat="server" CssClass="tableLabel">File name</asp:label></TD>
												<td><asp:textbox style="Z-INDEX: 0; MARGIN-TOP: 5px" id="txtFilePath" tabIndex="4" runat="server"
														CssClass="textField" Width="246px" onblur="FileNameChanged();" Height="30px" TextMode="MultiLine"
														onKeyDown="return false;"></asp:textbox></td>
											</tr>
										</tbody>
									</TABLE>
								</fieldset>
							</td>
						</tr>
						<tr>
							<td colSpan="2"><STRONG class="gridHeading"><FONT style="WIDTH: 50%; HEIGHT: 16px" size="2">Invoices 
										Matching Selection Criteria</FONT></STRONG></td>
						</tr>
						<tr>
							<td colSpan="2">
								<asp:datagrid style="Z-INDEX: 0" id="GrdAWBDetails" tabIndex="21" runat="server" Width="50%" FooterStyle-CssClass="gridHeading"
									BorderWidth="0px" CellSpacing="1" CellPadding="0" HeaderStyle-CssClass="gridHeading" ItemStyle-CssClass="gridField"
									AlternatingItemStyle-CssClass="gridField" ShowFooter="True" AutoGenerateColumns="False">
									<FooterStyle CssClass="gridHeading"></FooterStyle>
									<AlternatingItemStyle CssClass="gridField"></AlternatingItemStyle>
									<ItemStyle CssClass="gridField"></ItemStyle>
									<HeaderStyle CssClass="gridHeading"></HeaderStyle>
									<Columns>
										<asp:TemplateColumn>
											<HeaderStyle Width="10%"></HeaderStyle>
											<ItemStyle HorizontalAlign="Center"></ItemStyle>
											<ItemTemplate>
												<asp:CheckBox Runat="server" ID="cbRows" onclick="javascript:CheckBoxCheck(this);"></asp:CheckBox>
											</ItemTemplate>
										</asp:TemplateColumn>
										<asp:TemplateColumn HeaderText="Shipper">
											<HeaderStyle HorizontalAlign="Center"></HeaderStyle>
											<ItemStyle HorizontalAlign="Center"></ItemStyle>
											<ItemTemplate>
												<asp:Label ID="lblIShipper" Runat=server Text='<%#DataBinder.Eval(Container.DataItem,"Shipper") %>'>
												</asp:Label>
											</ItemTemplate>
										</asp:TemplateColumn>
										<asp:TemplateColumn HeaderText="Invoice Period">
											<HeaderStyle HorizontalAlign="Center"></HeaderStyle>
											<ItemStyle HorizontalAlign="Center"></ItemStyle>
											<ItemTemplate>
												<asp:Label ID="lblInvoicePeriod" Runat=server Text='<%#DataBinder.Eval(Container.DataItem,"[Invoice Period]") %>'>
												</asp:Label>
											</ItemTemplate>
										</asp:TemplateColumn>
										<asp:TemplateColumn HeaderText="Imported Date">
											<HeaderStyle HorizontalAlign="Center"></HeaderStyle>
											<ItemStyle HorizontalAlign="Center"></ItemStyle>
											<ItemTemplate>
												<asp:Label ID="lblImportedDate" Runat=server Text='<%# DataBinder.Eval(Container.DataItem,"[Imported Date]" , "{0:dd/MM/yyyy HH:mm}" ) %>'>
												</asp:Label>
											</ItemTemplate>
										</asp:TemplateColumn>
										<asp:TemplateColumn HeaderText="Imported By">
											<HeaderStyle HorizontalAlign="Center"></HeaderStyle>
											<ItemStyle HorizontalAlign="Center"></ItemStyle>
											<ItemTemplate>
												<asp:Label ID="lblImportedBy" Runat=server Text='<%#DataBinder.Eval(Container.DataItem,"[Imported By]") %>'>
												</asp:Label>
											</ItemTemplate>
										</asp:TemplateColumn>
										<asp:TemplateColumn HeaderText="No. of Cons">
											<HeaderStyle HorizontalAlign="Center"></HeaderStyle>
											<ItemStyle HorizontalAlign="Center"></ItemStyle>
											<ItemTemplate>
												<asp:Label ID="lblNoOfCons" Runat=server Text='<%#DataBinder.Eval(Container.DataItem,"[NoOfCons]") %>'>
												</asp:Label>
											</ItemTemplate>
										</asp:TemplateColumn>
										<asp:TemplateColumn HeaderText="No. of Unique Cons">
											<HeaderStyle HorizontalAlign="Center"></HeaderStyle>
											<ItemStyle HorizontalAlign="Center"></ItemStyle>
											<ItemTemplate>
												<asp:Label ID="lblNoOfUniqueCons" Runat=server Text='<%#DataBinder.Eval(Container.DataItem,"NoOfUniqueCons") %>'>
												</asp:Label>
											</ItemTemplate>
										</asp:TemplateColumn>
										<asp:TemplateColumn HeaderText="Total Charges">
											<HeaderStyle HorizontalAlign="Center"></HeaderStyle>
											<ItemStyle HorizontalAlign="Center"></ItemStyle>
											<ItemTemplate>
												<asp:Label ID="lblTotalCharges" Runat="server"></asp:Label>
											</ItemTemplate>
										</asp:TemplateColumn>
									</Columns>
								</asp:datagrid></td>
						</tr></TR></tbody></TABLE>
			</div>
		</form>
	</body>
</HTML>
