<%@ Page language="c#" Codebehind="VersusReport_New.aspx.cs" AutoEventWireup="false" Inherits="com.ties.VersusReportDate" %>
<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Versus Report</title>
		<LINK rel="stylesheet" type="text/css" href="css/Styles.css" name="stylesheet">
		<meta name="GENERATOR" content="Microsoft Visual Studio 7.0">
		<meta name="CODE_LANGUAGE" content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<!--#INCLUDE FILE="msFormValidations.inc"-->
		<script language="javascript" src="Scripts/settingScrollPosition.js"></script>
	</HEAD>
	<body onclick="GetScrollPosition();" onunload="window.top.displayBanner.fnCloseAll(0);"
		onload="SetScrollPosition();" MS_POSITIONING="GridLayout">
		<form id="VersusReport_New" method="post" runat="server">
			<asp:label style="Z-INDEX: 103; POSITION: absolute; TOP: 4px; LEFT: 20px" id="Label1" runat="server"
				CssClass="maintitleSize" Width="365px" Height="27px">Versus Report</asp:label><TD class="tableLabel"><asp:button style="Z-INDEX: 100; POSITION: absolute; TOP: 40px; LEFT: 20px" id="btnQuery" runat="server"
					Text="Query" CssClass="queryButton" Width="64px"></asp:button><asp:label style="Z-INDEX: 105; POSITION: absolute; TOP: 71px; LEFT: 20px" id="lblErrorMessage"
					runat="server" CssClass="errorMsgColor" Width="700px" Height="17px" Visible="False">Place holder for err msg</asp:label>&nbsp;
				<asp:button style="Z-INDEX: 101; POSITION: absolute; TOP: 40px; LEFT: 84px" id="btnExecuteQuery"
					runat="server" Text="Execute Query" CssClass="queryButton" Width="123px"></asp:button>
				<TABLE style="Z-INDEX: 104; POSITION: absolute; WIDTH: 800px; HEIGHT: 500px; TOP: 80px; LEFT: 14px"
					id="VersusReportDate" border="0" width="851" runat="server">
					<TR>
						<TD>
							<fieldset style="WIDTH: 750px; HEIGHT: 129px"><legend><asp:label id="lblDates" runat="server" CssClass="tableHeadingFieldset" Width="20px" Font-Bold="True">Dates</asp:label></legend>
								<TABLE style="WIDTH: 445px; HEIGHT: 113px" id="tblDates" border="0" cellSpacing="0" cellPadding="0"
									align="left" runat="server">
									<TR>
										<td></td>
										<TD colSpan="3" class="tableLabel">&nbsp;
											<asp:radiobutton id="rbLastStatusDate" runat="server" Text="Last Status Date" CssClass="tableRadioButton"
												Width="126px" Height="22px" AutoPostBack="True" GroupName="QueryByDate" Checked="True"></asp:radiobutton></TD>
										<td style="WIDTH: 1px"></td>
									</TR>
									<TR>
										<td></td>
										<TD colSpan="3" class="tableLabel">&nbsp;
											<asp:radiobutton id="rbMonth" runat="server" Text="Month" CssClass="tableRadioButton" Width="73px"
												Height="21px" AutoPostBack="True" GroupName="EnterDate" Checked="True"></asp:radiobutton>&nbsp;
											<asp:dropdownlist id="ddMonth" runat="server" CssClass="textField" Width="88px" Height="19px"></asp:dropdownlist>&nbsp;
											<asp:label id="lblYear" runat="server" CssClass="tableLabel" Width="30px" Height="15px">Year</asp:label>&nbsp;
											<cc1:mstextbox id="txtYear" runat="server" CssClass="textField" Width="83" MaxLength="5" TextMaskType="msNumeric"
												NumberMaxValue="2999" NumberMinValue="1" NumberPrecision="4"></cc1:mstextbox></TD>
										<td style="WIDTH: 1px"></td>
									</TR>
									<TR>
										<td style="HEIGHT: 20px"></td>
										<TD colSpan="3" class="tableLabel">&nbsp;
											<asp:radiobutton id="rbPeriod" runat="server" Text="Period" CssClass="tableRadioButton" Width="62px"
												AutoPostBack="True" GroupName="EnterDate"></asp:radiobutton>&nbsp;&nbsp;&nbsp;&nbsp;
											<cc1:mstextbox id="txtPeriod" runat="server" CssClass="textField" Width="82" MaxLength="10" TextMaskType="msDate"
												TextMaskString="99/99/9999"></cc1:mstextbox>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
											&nbsp;
											<cc1:mstextbox id="txtTo" runat="server" CssClass="textField" Width="83" MaxLength="10" TextMaskType="msDate"
												TextMaskString="99/99/9999"></cc1:mstextbox></TD>
										<td style="WIDTH: 1px; HEIGHT: 20px"></td>
									</TR>
									<TR>
										<td></td>
										<TD colSpan="3" class="tableLabel">&nbsp;
											<asp:radiobutton id="rbDate" runat="server" Text="Date" CssClass="tableRadioButton" Width="74px"
												Height="22px" AutoPostBack="True" GroupName="EnterDate"></asp:radiobutton>&nbsp;&nbsp;<cc1:mstextbox id="txtDate" runat="server" CssClass="textField" Width="82" MaxLength="10" TextMaskType="msDate"
												TextMaskString="99/99/9999"></cc1:mstextbox></TD>
										<td style="WIDTH: 1px"></td>
									</TR>
								</TABLE>
							</fieldset>
						</TD>
					</TR>
					<TR>
						<TD>
							<fieldset style="WIDTH: 750px; HEIGHT: 129px"><legend><asp:label id="lblRoute" runat="server" CssClass="tableHeadingFieldset" Width="205px" Font-Bold="True">   Route Type / Service / DC Selection</asp:label></legend>
								<TABLE style="WIDTH: 455px; HEIGHT: 113px" id="Table1" border="0" cellSpacing="0" cellPadding="0"
									align="left" runat="server">
									<TR>
										<td></td>
										<TD colSpan="4" style="HEIGHT: 20px" class="tableLabel">&nbsp;<asp:radiobutton id="rbPickUp" runat="server" Text="Pickup" CssClass="tableRadioButton" Width="100px"
												Height="22px" AutoPostBack="True" GroupName="QueryByRoute" Checked="True"></asp:radiobutton><asp:radiobutton id="rbDelivery" runat="server" Text="Delivery" CssClass="tableRadioButton" Width="104px"
												Height="22px" AutoPostBack="True" GroupName="QueryByRoute"></asp:radiobutton></TD>
										<td style="WIDTH: 1px"></td>
									</TR>
									<TR>
										<td></td>
										<TD class="tableLabel" style="HEIGHT: 20px">&nbsp;<asp:label id="lblRouteCode" runat="server" CssClass="tableLabel" Width="111px">Route Code</asp:label></TD>
										<TD class="tableLabel" style="HEIGHT: 20px"><asp:dropdownlist id="cboRouteCode" runat="server" Height="19px"></asp:dropdownlist></TD>
										<TD class="tableLabel" style="HEIGHT: 20px">&nbsp;&nbsp;<asp:label id="lblOriginDC" runat="server" CssClass="tableLabel" Width="111px">Origin DC</asp:label></TD>
										<TD class="tableLabel" style="HEIGHT: 20px"><asp:dropdownlist id="cboOrigin" runat="server" Width="83px"></asp:dropdownlist></TD>
										<td style="WIDTH: 1px"></td>
									</TR>
									<TR>
										<td style="HEIGHT: 20px"></td>
										<TD class="tableLabel" style="HEIGHT: 20px">&nbsp;<asp:label id="lblServiceType" runat="server" CssClass="tableLabel" Width="111px">Service Type</asp:label></TD>
										<TD class="tableLabel" style="HEIGHT: 20px"><asp:listbox id="lsbServiceType" runat="server" Width="145" Height="60" SelectionMode="Multiple"></asp:listbox></TD>
										<TD class="tableLabel" style="HEIGHT: 20px" valign="top">&nbsp;&nbsp;<asp:label id="lblDestinationDC" runat="server" CssClass="tableLabel" Width="111px">Destination DC</asp:label></TD>
										<TD class="tableLabel" style="HEIGHT: 20px" valign="top"><asp:dropdownlist id="cboDestinationDC" runat="server" Width="83px"></asp:dropdownlist></TD>
										<td style="WIDTH: 1px; HEIGHT: 20px"></td>
									</TR>
								</TABLE>
							</fieldset>
						</TD>
					</TR>
					<TR>
						<TD>
							<fieldset style="WIDTH: 750px; HEIGHT: 129px"><legend><asp:label id="lblExceptionSelection" runat="server" CssClass="tableHeadingFieldset" Width="120px"
										Font-Bold="True">Exception Selection</asp:label></legend>
								<TABLE style="WIDTH: 445px; HEIGHT: 113px" id="Table2" border="0" cellSpacing="0" cellPadding="0"
									align="left" runat="server">
									<TR>
										<td></td>
										<TD colSpan="3" class="tableLabel">&nbsp;<asp:radiobutton id="rbHasAllStatus" runat="server" Text="Has all status (left pane) but missing some status (right pane)"
												CssClass="tableRadioButton" Width="400px" Height="22px" AutoPostBack="True" GroupName="Exception" Checked="True"></asp:radiobutton></TD>
										<td style="WIDTH: 1px"></td>
									</TR>
									<TR>
										<td></td>
										<TD class="tableLabel" align="right">
											<asp:listbox id="lsbHasAllStatus_Left" runat="server" Width="145" Height="60" SelectionMode="Multiple">
												<asp:ListItem Value="C">MDE</asp:ListItem>
												<asp:ListItem Value="A">SIP</asp:ListItem>
												<asp:ListItem Value="N">POD</asp:ListItem>
											</asp:listbox></TD>
										<TD class="tableLabel"></TD>
										<TD class="tableLabel">
											<asp:listbox id="lsbHasAllStatus_Right" runat="server" Width="145" Height="60" SelectionMode="Multiple">
												<asp:ListItem Value="C">MDE</asp:ListItem>
												<asp:ListItem Value="A">SIP</asp:ListItem>
												<asp:ListItem Value="N">POD</asp:ListItem>
											</asp:listbox></TD>
										<td style="WIDTH: 1px"></td>
									</TR>
									<TR>
										<td></td>
										<TD colSpan="3" class="tableLabel">&nbsp;<asp:radiobutton id="rbHasSelectStatus" runat="server" Text="Has status selected but out of sequence"
												CssClass="tableRadioButton" Width="400px" Height="22px" AutoPostBack="True" GroupName="Exception"></asp:radiobutton></TD>
										<td style="WIDTH: 1px"></td>
									</TR>
									<TR>
										<td></td>
										<TD class="tableLabel" align="right">
											<asp:listbox id="lsbHasSelectStatus_Left" runat="server" Width="145" Height="60" SelectionMode="Multiple">
												<asp:ListItem Value="C">MDE</asp:ListItem>
												<asp:ListItem Value="A">SIP</asp:ListItem>
												<asp:ListItem Value="N">POD</asp:ListItem>
											</asp:listbox></TD>
										<TD class="tableLabel" align="center"><asp:button id="btnToRight" runat="server" Text=">" CssClass="queryButton" Width="25px"></asp:button><br>
											<asp:button id="btnToLeft" runat="server" Text="<" CssClass="queryButton" Width="25px"></asp:button><br>
											<asp:button id="btnToUp" runat="server" Text="^" CssClass="queryButton" Width="25px"></asp:button><br>
											<asp:button id="btnToDown" runat="server" Text="&#728;" CssClass="queryButton" Width="25px"></asp:button><br>
										</TD>
										<TD class="tableLabel"><asp:listbox id="lsbHasSelectStatus_Right" runat="server" Width="145" Height="60" SelectionMode="Multiple"></asp:listbox></TD>
										<td style="WIDTH: 1px"></td>
									</TR>
									<TR>
										<td></td>
										<TD colSpan="3" class="tableLabel">&nbsp;<asp:radiobutton id="rbMissingStatus" runat="server" Text="Missing Package scans from group" CssClass="tableRadioButton"
												Width="400px" Height="22px" AutoPostBack="True" GroupName="Exception"></asp:radiobutton></TD>
										<td style="WIDTH: 1px"></td>
									</TR>
									<TR>
										<td></td>
										<TD class="tableLabel" align="right">
											<asp:listbox id="lsbMissingStatus" runat="server" Width="145" Height="60" SelectionMode="Multiple">
												<asp:ListItem Value="C">MDE</asp:ListItem>
												<asp:ListItem Value="A">SIP</asp:ListItem>
												<asp:ListItem Value="N">POD</asp:ListItem>
											</asp:listbox></TD>
										<TD class="tableLabel"></TD>
										<TD class="tableLabel"></TD>
										<td style="WIDTH: 1px"></td>
									</TR>
								</TABLE>
							</fieldset>
						</TD>
					</TR>
				</TABLE>
		</form>
	</body>
</HTML>
