<%@ Page language="c#" Codebehind="VASPopup.aspx.cs" AutoEventWireup="false" Inherits="TIES.WebUI.VASPopup" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
  <HEAD>
		<title>SurchargePopup</title>
		<LINK href="css/Styles.css" type="text/css" rel="stylesheet" name="stylesheet">
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<script language="javascript" src="Scripts/settingScrollPosition.js"></script>
</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="VASPopup" method="post" runat="server">
			<asp:datagrid id="dgVASMaster" style="Z-INDEX: 101; POSITION: absolute; TOP: 96px; LEFT: 45px" runat="server" OnPageIndexChanged="Paging" PageSize="10" AllowPaging="True" Width="522px" AutoGenerateColumns="False" BorderColor="#CCCCCC" BackColor="White" CellPadding="3" BorderWidth="1px" BorderStyle="None" CssClass="gridHeading" AllowCustomPaging="True">
				<SelectedItemStyle Font-Bold="True" ForeColor="White" BackColor="#669999"></SelectedItemStyle>
				<HeaderStyle Width="10%" CssClass="gridHeading"></HeaderStyle>
				<ItemStyle CssClass="popupGridField"></ItemStyle>
				<FooterStyle ForeColor="#000066" BackColor="White"></FooterStyle>
				<Columns>
					<asp:BoundColumn DataField="vas_code" HeaderText="Surcharge Code"></asp:BoundColumn>
					<asp:BoundColumn DataField="vas_description" HeaderText="Description"></asp:BoundColumn>
					<asp:BoundColumn DataField="surcharge" HeaderText="Amount"></asp:BoundColumn>
					<asp:ButtonColumn Text="Select" CommandName="Select"></asp:ButtonColumn>
				</Columns>
				<PagerStyle HorizontalAlign="Left" ForeColor="#000066" BackColor="White" Mode="NumericPages"></PagerStyle>
			</asp:datagrid>
			<asp:button id="btnClose" style="Z-INDEX: 107; POSITION: absolute; TOP: 53px; LEFT: 498px" runat="server" CssClass="buttonProp" Width="68px" CausesValidation="False" Text="Close" Height="21px"></asp:button><asp:button id="btnSearch" style="Z-INDEX: 106; POSITION: absolute; TOP: 53px; LEFT: 412px" runat="server" Width="84px" CssClass="buttonProp" Height="21px" Text="Search" CausesValidation="False"></asp:button><asp:textbox id="txtVASCode" style="Z-INDEX: 104; POSITION: absolute; TOP: 56px; LEFT: 49px" runat="server" Width="116px" CssClass="textField" Height="19px"></asp:textbox><asp:label id="lblVASDesc" style="Z-INDEX: 102; POSITION: absolute; TOP: 26px; LEFT: 199px" runat="server" Width="108px" CssClass="tableLabel" Height="16px">Description</asp:label><asp:label id="lblVASCode" style="Z-INDEX: 100; POSITION: absolute; TOP: 26px; LEFT: 49px" runat="server" Width="100px" CssClass="tableLabel" Height="16px">Surcharge Code</asp:label><asp:textbox id="txtVASDesc" style="Z-INDEX: 103; POSITION: absolute; TOP: 55px; LEFT: 199px" runat="server" Width="202px" CssClass="textField" Height="19px"></asp:textbox></form>
	</body>
</HTML>
