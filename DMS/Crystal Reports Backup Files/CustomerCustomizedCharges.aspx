<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<%@ Register TagPrefix="dbCombo" Namespace="Cambro.Web.DbCombo" Assembly="Cambro.Web.DbCombo" %>
<%@ Import Namespace="com.common.util" %>
<%@ Page language="c#" Codebehind="CustomerCustomizedCharges.aspx.cs" AutoEventWireup="false" Inherits="com.ties.CustomerCustomizedCharges" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>AgentCustomableCharge</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<!--#INCLUDE FILE="msFormValidations.inc"--><LINK rev="stylesheet" href="css/styles.css" rel="stylesheet">
		<script language="javascript" src="Scripts/settingScrollPosition.js"></script>
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="CustomerCustomizedCharges" method="post" runat="server">
			<asp:label id="lblAgentCode" style="Z-INDEX: 113; LEFT: 27px; POSITION: absolute; TOP: 108px" runat="server" Width="100px" CssClass="tableLabel">Customer Code</asp:label><asp:label id="lblServiceMessage" style="Z-INDEX: 115; LEFT: 26px; POSITION: absolute; TOP: 86px" runat="server" ForeColor="Red"></asp:label><asp:datagrid id="dgCustomizedCode" style="Z-INDEX: 114; LEFT: 25px; POSITION: absolute; TOP: 142px" runat="server" Width="707px" SelectedItemStyle-CssClass="gridFieldSelected" OnSelectedIndexChanged="dgCustomizedCode_SelectedIndexChanged" OnItemDataBound="dgCustomizedCode_Bound" AllowCustomPaging="True" PageSize="5" AllowPaging="True" ItemStyle-Height="20" OnPageIndexChanged="dgCustomizedCode_PageChange" OnDeleteCommand="dgCustomizedCode_Delete" AutoGenerateColumns="False" OnItemCommand="dgCustomizedCode_Button" OnCancelCommand="dgCustomizedCode_Cancel" OnUpdateCommand="dgCustomizedCode_Update">
				<SelectedItemStyle CssClass="gridFieldSelected"></SelectedItemStyle>
				<ItemStyle Height="20px" CssClass="gridField"></ItemStyle>
				<Columns>
					<asp:EditCommandColumn ButtonType="LinkButton" UpdateText="&lt;IMG SRC='images/butt-update.gif' border=0 title='Update' &gt;" CancelText="&lt;IMG SRC='images/butt-cancel.gif' border=0 title='Cancel' &gt;" EditText="&lt;IMG SRC='images/butt-edit.gif' border=0 title='Edit' &gt;">
						<HeaderStyle Width="3%" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle HorizontalAlign="Left"></ItemStyle>
					</asp:EditCommandColumn>
					<asp:ButtonColumn Text="&lt;IMG SRC='images/butt-delete.gif' border=0 title='Delete' &gt;" CommandName="Delete">
						<HeaderStyle HorizontalAlign="Center" Width="3%" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle HorizontalAlign="Center"></ItemStyle>
					</asp:ButtonColumn>
					<asp:TemplateColumn HeaderText="Description">
						<HeaderStyle Font-Bold="True" Width="19%" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle Wrap="False"></ItemStyle>
						<ItemTemplate>
							<asp:Label CssClass="gridLabel" ID="lblCustDescription" Text='<%#DataBinder.Eval(Container.DataItem,"code_text")%>' Runat="server">
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<cc1:mstextbox CssClass="gridTextBox" ID="txtCustDescription" Text='<%#DataBinder.Eval(Container.DataItem,"code_text")%>' Runat="server" Enabled="True" Readonly="True" TextMaskType="msUpperAlfaNumericWithUnderscore">
							</cc1:mstextbox>
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="" HeaderStyle-Width="0%">
						<HeaderStyle Font-Bold="True" Width="0%" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle Wrap="False"></ItemStyle>
						<ItemTemplate>
							<asp:Label Width="0" CssClass="gridLabel" ID="lblCustomizedCode" Runat="server"></asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<cc1:mstextbox Width="0" CssClass="gridTextBox" ID="txtCustomizedCode" Text='<%#DataBinder.Eval(Container.DataItem,"customized_assembly_code")%>' Readonly="True" Runat="server" Enabled="True" MaxLength="12" TextMaskType="msUpperAlfaNumericWithUnderscore">
							</cc1:mstextbox>
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:ButtonColumn Text="&lt;IMG SRC='images/butt-search.gif' border=0 title='Search' &gt;" CommandName="search">
						<HeaderStyle HorizontalAlign="Center" Width="3%" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle HorizontalAlign="Center"></ItemStyle>
					</asp:ButtonColumn>
					<asp:TemplateColumn HeaderText="Asscembly Code">
						<HeaderStyle Width="23%" CssClass="gridHeading"></HeaderStyle>
						<ItemTemplate>
							<asp:Label CssClass="gridLabel" ID="lblAssemblyid" Text='<%#DataBinder.Eval(Container.DataItem,"assemblyid")%>' Runat="server" Enabled="True">
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<asp:TextBox CssClass="gridTextBox" ID="txtAssemblyid" Text='<%#DataBinder.Eval(Container.DataItem,"assemblyid")%>' Runat="server" MaxLength="200" ReadOnly="True">
							</asp:TextBox>
							<asp:RequiredFieldValidator id="RequiredFieldValidator1" ControlToValidate="txtAssemblyid" style="Z-INDEX: 116; LEFT: 47px; POSITION: absolute; TOP: 326px" runat="server" ErrorMessage="assembly code is required" Display="None"></asp:RequiredFieldValidator>
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Description">
						<HeaderStyle Width="20%" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle Wrap="False"></ItemStyle>
						<ItemTemplate>
							<asp:Label ID="lblDescription" Text='<%#DataBinder.Eval(Container.DataItem,"Assembly_description")%>' Runat="server" CssClass="gridlabel">
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<asp:TextBox ID="txtDescription" Text='<%#DataBinder.Eval(Container.DataItem,"Assembly_description")%>' Runat="server" Enabled="True" CssClass="gridTextBox">
							</asp:TextBox>
						</EditItemTemplate>
					</asp:TemplateColumn>
				</Columns>
				<PagerStyle NextPageText="Next" PrevPageText="Previous" HorizontalAlign="Right" CssClass="normalText"></PagerStyle>
			</asp:datagrid><dbcombo:dbcombo id="dbCmbAgentId" style="Z-INDEX: 112; LEFT: 138px; POSITION: absolute; TOP: 102px" tabIndex="1" runat="server" Width="140px" AutoPostBack="True" TextBoxColumns="20" ServerMethod="AgentIdServerMethod" TextUpLevelSearchButton="v" ShowDbComboLink="False" ReQueryOnLoad="True" Height="17px"></dbcombo:dbcombo><dbcombo:dbcombo id="dbCmbAgentName" style="Z-INDEX: 111; LEFT: 490px; POSITION: absolute; TOP: 102px" tabIndex="2" runat="server" Width="182px" AutoPostBack="True" TextBoxColumns="25" ServerMethod="AgentNameServerMethod" TextUpLevelSearchButton="v" ShowDbComboLink="False" ReQueryOnLoad="True" Height="17px"></dbcombo:dbcombo><asp:label id="lblAgentName" style="Z-INDEX: 108; LEFT: 386px; POSITION: absolute; TOP: 110px" runat="server" Width="100px" CssClass="tableLabel">Customer Name</asp:label><asp:button id="btnGoToFirstPage" style="Z-INDEX: 109; LEFT: 608px; POSITION: absolute; TOP: 48px" tabIndex="5" runat="server" Width="24px" CssClass="queryButton" Text="|<" CausesValidation="False"></asp:button><asp:button id="btnPreviousPage" style="Z-INDEX: 104; LEFT: 632px; POSITION: absolute; TOP: 48px" tabIndex="6" runat="server" Width="24px" CssClass="queryButton" Text="<" CausesValidation="False"></asp:button><asp:textbox id="txtCountRec" style="Z-INDEX: 107; LEFT: 656px; POSITION: absolute; TOP: 48px" tabIndex="7" runat="server" Width="24px" Height="19px"></asp:textbox><asp:button id="btnNextPage" style="Z-INDEX: 105; LEFT: 680px; POSITION: absolute; TOP: 48px" tabIndex="8" runat="server" Width="24px" CssClass="queryButton" Text=">" CausesValidation="False"></asp:button><asp:button id="btnGoToLastPage" style="Z-INDEX: 106; LEFT: 704px; POSITION: absolute; TOP: 48px" tabIndex="9" runat="server" Width="24px" CssClass="queryButton" Text=">|" CausesValidation="False"></asp:button><asp:button id="btnInsertVAS" style="Z-INDEX: 110; LEFT: 206px; POSITION: absolute; TOP: 48px" runat="server" Width="57px" CssClass="queryButton" Text="Insert" CausesValidation="False"></asp:button><asp:button id="btnExecQry" style="Z-INDEX: 102; LEFT: 76px; POSITION: absolute; TOP: 48px" runat="server" Width="130px" CssClass="queryButton" Text="Execute Query" CausesValidation="False"></asp:button><asp:button id="btnQuery" style="Z-INDEX: 101; LEFT: 22px; POSITION: absolute; TOP: 48px" runat="server" Width="54px" CssClass="queryButton" Text="Query" CausesValidation="False"></asp:button><asp:label id="lblTitle" style="Z-INDEX: 103; LEFT: 24px; POSITION: absolute; TOP: 8px" runat="server" Width="336px" CssClass="mainTitleSize" Height="32px">Customer Customize Functions</asp:label><asp:validationsummary id="ValidationSummary1" style="Z-INDEX: 116; LEFT: 41px; POSITION: absolute; TOP: 322px" runat="server" ShowSummary="False" ShowMessageBox="True"></asp:validationsummary></form>
	</body>
</HTML>
