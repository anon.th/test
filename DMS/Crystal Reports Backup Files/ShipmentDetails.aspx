<%@ Import Namespace="com.common.util" %>
<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<%@ Page language="c#" Codebehind="ShipmentDetails.aspx.cs" AutoEventWireup="false" Inherits="com.ties.ShipmentDetails" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>ShipmentDetails</title>
		<LINK href="css/Styles.css" type="text/css" rel="stylesheet" name="stylesheet">
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<!--#INCLUDE FILE="msFormValidations.inc"-->
		<script language="javascript" src="Scripts/settingScrollPosition.js"></script>
	</HEAD>
	<body onclick="GetScrollPosition();" onload="SetScrollPosition();" onunload="window.opener.top.displayBanner.fnCloseAll(1);" MS_POSITIONING="GridLayout">
		<form id="ShipmentDetails" method="post" runat="server">
			<asp:label id="Label1" runat="server" CssClass="mainTitleSize" Width="465px" Height="34px">Pickup Request-Shipment Details</asp:label>
			<div id="divMainPanel" style="Z-INDEX: 101; LEFT: -1px; WIDTH: 746px; POSITION: relative; TOP: 40px; HEIGHT: 588px" MS_POSITIONING="GridLayout" runat="server">
				<TABLE id="Table1" runat="server">
					<tr height="27">
						<td width="2%"></td>
						<td width="5%"></td>
						<td width="5%"></td>
						<td style="WIDTH: 71px" width="71"></td>
						<td style="WIDTH: 17px" width="17"></td>
						<td width="5%"></td>
						<td width="5%"></td>
						<td style="WIDTH: 166px" width="166"></td>
						<td style="WIDTH: 7px" width="7"></td>
						<td width="5%"></td>
						<td style="WIDTH: 136px" width="136"></td>
						<td width="14" style="WIDTH: 14px"></td>
						<td width="5%"></td>
						<td style="WIDTH: 108px" width="108"></td>
						<td width="5%"></td>
						<td width="5%"></td>
						<td width="5%"></td>
						<td width="5%"></td>
						<td width="5%"></td>
						<td width="5%"></td>
					</tr>
					<tr height="27">
						<TD colSpan="1"></TD>
						<TD style="WIDTH: 350px" colSpan="8"><asp:button id="btnInsert" tabIndex="1" runat="server" CssClass="queryButton" Width="56px" CausesValidation="False" Text="Insert"></asp:button><asp:button id="btnSave" tabIndex="2" runat="server" CssClass="queryButton" Width="46" Text="Save"></asp:button><asp:button id="BtnDelete" tabIndex="3" runat="server" CssClass="queryButton" Width="56px" CausesValidation="False" Text="Delete"></asp:button><asp:button id="btnCancel" tabIndex="4" runat="server" CssClass="queryButton" Width="56px" CausesValidation="False" Text="Cancel"></asp:button><asp:button id="bntClose" tabIndex="5" runat="server" CssClass="queryButton" Width="46" CausesValidation="False" Text="Close"></asp:button></TD>
						<TD colSpan="4"><asp:button id="btnGoToFirstPage" tabIndex="5" runat="server" CssClass="queryButton" Width="31px" CausesValidation="False" Text="|<"></asp:button><asp:button id="btnPreviousPage" tabIndex="6" runat="server" CssClass="queryButton" Width="25px" CausesValidation="False" Text="<"></asp:button><asp:textbox id="txtCountRec" tabIndex="7" runat="server" Width="30px" Height="19px"></asp:textbox><asp:button id="btnNextPage" tabIndex="8" runat="server" CssClass="queryButton" Width="27px" CausesValidation="False" Text=">"></asp:button><asp:button id="btnGoToLastPage" tabIndex="9" runat="server" CssClass="queryButton" Width="26px" CausesValidation="False" Text=">|"></asp:button></TD>
					</tr>
					<TR height="10">
						<TD colSpan="20"><asp:label id="lblErrorMsg" runat="server" CssClass="errorMsgColor"></asp:label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						</TD>
					</TR>
					<TR height="27">
						<TD colSpan="1"></TD>
						<TD style="WIDTH: 87px" align="left" colSpan="3"><asp:label id="lblSerialNo" runat="server" CssClass="tableLabel">Serial Number</asp:label></TD>
						<TD style="WIDTH: 218px" colSpan="4"><asp:textbox id="txtSerialNo" tabIndex="6" runat="server" CssClass="textField" Width="76px" ReadOnly="True"></asp:textbox></TD>
						<TD style="WIDTH: 156px" colSpan="3"><asp:label id="lblEStDevlDt" runat="server" CssClass="tableLabel">Est Delivery Date</asp:label></TD>
						<TD style="WIDTH: 124px" colSpan="3"><cc1:mstextbox id="txtEstDelvdate" tabIndex="10" CssClass="textField" Text="" AutoPostBack="True" Enabled="True" MaxLength="16" Runat="server" TextMaskType="msDateTime" TextMaskString="99/99/9999 99:99"></cc1:mstextbox></TD>
						<TD colSpan="2"></TD>
					</TR>
					<TR height="27">
						<TD colSpan="1">
							<asp:Label id="Label2" runat="server" ForeColor="Red">*</asp:Label></TD>
						<TD style="WIDTH: 87px" align="left" colSpan="3"><asp:label id="lblRecpZipCode" runat="server" CssClass="tableLabel" Width="146px">Recipient Postal Code</asp:label></TD>
						<TD style="WIDTH: 218px" colSpan="4"><asp:textbox id="txtRecipZip" tabIndex="7" runat="server" CssClass="textField" Width="76px" AutoPostBack="True"></asp:textbox><asp:button id="btnRecpZipCode" runat="server" CssClass="searchButton" CausesValidation="False" Text="..."></asp:button></TD>
						<TD style="WIDTH: 156px" colSpan="3"><asp:label id="lblFrightChrg" runat="server" CssClass="tableLabel">Freight Charge</asp:label></TD>
						<TD style="WIDTH: 124px" colSpan="3"><cc1:mstextbox id="txtFreightChrg" tabIndex="23" CssClass="textFieldRightAlign" Width="77px" Text="" ReadOnly="True" Runat="server" TextMaskType="msNumeric" NumberScale="2" NumberPrecision="6" NumberMinValue="0" NumberMaxValue="1000" Visible="True"></cc1:mstextbox></TD>
						<TD colSpan="2"></TD>
					</TR>
					<TR height="27">
						<TD colSpan="1" style="HEIGHT: 48px">
							<asp:Label id="Label3" runat="server" ForeColor="Red">*</asp:Label></TD>
						<TD style="WIDTH: 87px; HEIGHT: 48px" colSpan="3"><asp:label id="lblServicecode" runat="server" CssClass="tableLabel">Service Code</asp:label></TD>
						<TD style="WIDTH: 218px; HEIGHT: 48px" colSpan="4"><asp:textbox id="txtShpSvcCode" tabIndex="8" runat="server" CssClass="textField" Width="76px" AutoPostBack="True"></asp:textbox><asp:button id="btnServiceCode" runat="server" CssClass="searchButton" CausesValidation="False" Text="..."></asp:button></TD>
						<TD style="WIDTH: 156px; HEIGHT: 48px" colSpan="3"><asp:label id="lblTotalVASSurcharge" runat="server" CssClass="tableLabel">Total VAS Surcharge</asp:label></TD>
						<TD style="WIDTH: 124px; HEIGHT: 48px" colSpan="3"><cc1:mstextbox id="txtTotVASSurChrg" tabIndex="23" CssClass="textFieldRightAlign" Width="76px" Text="" ReadOnly="True" Runat="server" TextMaskType="msNumeric" NumberScale="2" NumberPrecision="6" NumberMinValue="0" NumberMaxValue="100000" Visible="True"></cc1:mstextbox></TD>
						<TD colSpan="2" style="HEIGHT: 48px"></TD>
					</TR>
					<TR height="27">
						<TD colSpan="1"></TD>
						<TD style="WIDTH: 87px" colSpan="3">
							<asp:requiredfieldvalidator id="ValidServiceCode" runat="server" Enabled="False" ControlToValidate="txtShpSvcCode" ErrorMessage="Enter Service Code">*</asp:requiredfieldvalidator>
							<asp:requiredfieldvalidator id="ValidRecpZip" runat="server" Enabled="False" ControlToValidate="txtRecipZip" ErrorMessage="Enter Recipient ZipCode">*</asp:requiredfieldvalidator></TD>
						<TD style="WIDTH: 218px" colSpan="4"><asp:textbox id="txtShpSvcDesc" tabIndex="9" runat="server" CssClass="textField" Width="190px" ReadOnly="True"></asp:textbox></TD>
						<TD style="WIDTH: 156px" colSpan="3"><asp:label id="lblDeclareValue" runat="server" CssClass="tableLabel">Declare Value</asp:label></TD>
						<TD style="WIDTH: 124px" colSpan="3"><asp:textbox id="txtShpDclrValue" runat="server" CssClass="textFieldRightAlign" Width="76px" AutoPostBack="True"></asp:textbox></TD>
						<TD colSpan="2"></TD>
					</TR>
					<TR height="27">
						<TD colSpan="1"></TD>
						<TD style="WIDTH: 87px" colSpan="3"><asp:label id="lblESAApplied" runat="server" CssClass="tableLabel">ESA Applied</asp:label></TD>
						<TD style="WIDTH: 218px" colSpan="4"><asp:textbox id="txtESA" runat="server" CssClass="textField" Width="76px" ReadOnly="True"></asp:textbox></TD>
						<TD style="WIDTH: 156px" colSpan="3"><asp:label id="lblAddDV" runat="server" CssClass="tablelabel" Width="163px">Additional % DV</asp:label></TD>
						<TD style="WIDTH: 124px" colSpan="3"><asp:textbox id="txtShpAddDV" runat="server" CssClass="textFieldRightAlign" Width="76px" AutoPostBack="True"></asp:textbox></TD>
						<TD colSpan="2"></TD>
					</TR>
					<TR height="27">
						<TD colSpan="1"></TD>
						<TD style="WIDTH: 87px" colSpan="3"><asp:label id="lblESASurcharge" runat="server" CssClass="tableLabel">ESA Surcharge</asp:label></TD>
						<TD style="WIDTH: 218px" colSpan="4"><asp:textbox id="txtESASurcharge" runat="server" CssClass="textFieldRightAlign" Width="76px" ReadOnly="True"></asp:textbox></TD>
						<TD style="WIDTH: 156px" colSpan="3"><asp:label id="lblInsurSurcharge" runat="server" CssClass="tableLabel" Width="141px">Insurance Surcharge</asp:label></TD>
						<TD style="WIDTH: 124px" colSpan="3"><cc1:mstextbox id="txtInsSurcharge" tabIndex="23" CssClass="textFieldRightAlign" Width="76px" Text="" ReadOnly="True" Runat="server" TextMaskType="msNumeric" NumberScale="2" NumberPrecision="6" NumberMinValue="0" NumberMaxValue="1000" Visible="True"></cc1:mstextbox></TD>
						<TD colSpan="2"></TD>
					</TR>
					<TR height="27">
						<TD colSpan="1"></TD>
						<TD style="WIDTH: 170px" colSpan="4"><asp:button id="btnPopulateVAS" tabIndex="14" runat="server" CssClass="queryButton" Width="176px" Text="GetQuotationVAS"></asp:button></TD>
						<TD style="WIDTH: 167px" colSpan="3"><asp:button id="btnBind" tabIndex="15" runat="server" CssClass="queryButton" Width="60px" Text="Refresh" Visible="False"></asp:button></TD>
						<TD colSpan="3" style="WIDTH: 107px"><asp:label id="lblTolAmt" runat="server" CssClass="tableLabel">Total Amount</asp:label></TD>
						<TD colSpan="3"><cc1:mstextbox id="txtShpTotAmt" tabIndex="23" CssClass="textFieldRightAlign" Width="76px" Text="" ReadOnly="True" Runat="server" TextMaskType="msNumeric" NumberScale="2" NumberPrecision="6" NumberMinValue="0" NumberMaxValue="1000" Visible="True"></cc1:mstextbox></TD>
						<TD colSpan="2"></TD>
					</TR>
					<tr height="50">
						<TD colSpan="1"></TD>
						<td align="left" colSpan="15"><asp:datagrid id="dgVAS" tabIndex="150" runat="server" Width="791px" OnItemCommand="dgVAS_Button" AutoGenerateColumns="False" OnEditCommand="dgVAS_Edit" OnPageIndexChanged="dgVAS_PageChange" OnCancelCommand="dgVAS_Cancel" OnDeleteCommand="dgVAS_Delete" OnUpdateCommand="dgVAS_Update" PageSize="4">
								<Columns>
									<asp:ButtonColumn Text="&lt;IMG SRC='images/butt-delete.gif' border=0 title='Delete' &gt;" ButtonType="LinkButton" CommandName="Delete">
										<HeaderStyle Width="3%" CssClass="gridHeading"></HeaderStyle>
										<ItemStyle CssClass="gridField"></ItemStyle>
									</asp:ButtonColumn>
									<asp:EditCommandColumn ButtonType="LinkButton" UpdateText="&lt;IMG SRC='images/butt-update.gif' border=0  title='Update' &gt;" CancelText="&lt;IMG SRC='images/butt-cancel.gif' border=0 title='Cancel'&gt;" EditText="&lt;IMG SRC='images/butt-edit.gif' border=0  title='Edit' &gt;">
										<HeaderStyle Width="3%" CssClass="gridHeading"></HeaderStyle>
										<ItemStyle CssClass="gridField"></ItemStyle>
									</asp:EditCommandColumn>
									<asp:TemplateColumn HeaderText="VAS">
										<HeaderStyle Width="10%" CssClass="gridHeading"></HeaderStyle>
										<ItemStyle CssClass="gridField"></ItemStyle>
										<ItemTemplate>
											<asp:Label ID="lblVASCode" CssClass="gridLabel" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"vas_code")%>'>
											</asp:Label>
										</ItemTemplate>
										<EditItemTemplate>
											<asp:TextBox CssClass="gridTextBox" runat="server" ID="txtVASCode" Enabled=True EnableViewState=True Text='<%#DataBinder.Eval(Container.DataItem,"vas_code")%>'>
											</asp:TextBox>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:ButtonColumn Text="&lt;IMG SRC='images/butt-search.gif' border=0 title='Search' &gt;" ButtonType="LinkButton" CommandName="Search">
										<HeaderStyle Width="3%" CssClass="gridHeading"></HeaderStyle>
										<ItemStyle CssClass="gridField"></ItemStyle>
									</asp:ButtonColumn>
									<asp:TemplateColumn HeaderText="Description">
										<HeaderStyle Width="40%" CssClass="gridHeading"></HeaderStyle>
										<ItemStyle CssClass="gridField"></ItemStyle>
										<ItemTemplate>
											<asp:Label runat="server" CssClass="gridLabel" ID="lblVASDesc" Text='<%#DataBinder.Eval(Container.DataItem,"vas_description")%>'>
											</asp:Label>
										</ItemTemplate>
										<EditItemTemplate>
											<asp:TextBox CssClass="gridTextBox" runat="server" ID="txtVASDesc" Enabled=True EnableViewState=True Text='<%#DataBinder.Eval(Container.DataItem,"vas_description")%>'>
											</asp:TextBox>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Surcharge">
										<HeaderStyle Width="10%" CssClass="gridHeading"></HeaderStyle>
										<ItemStyle CssClass="gridField"></ItemStyle>
										<ItemTemplate>
											<asp:Label runat="server" CssClass="gridLabelNumber" ID="lblSurcharge" Text='<%#DataBinder.Eval(Container.DataItem,"surcharge","{0:#0.00}")%>'>
											</asp:Label>
										</ItemTemplate>
										<EditItemTemplate>
											<cc1:mstextbox CssClass="gridTextBox" runat="server" ID="txtSurcharge" TextMaskType="msNumeric" MaxLength="9" NumberMaxValue="999999" NumberMinValue="0" NumberPrecision="8" NumberScale="2" Text='<%#DataBinder.Eval(Container.DataItem,"surcharge")%>'>
											</cc1:mstextbox>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Remarks">
										<HeaderStyle Width="20%" CssClass="gridHeading"></HeaderStyle>
										<ItemStyle CssClass="gridField"></ItemStyle>
										<ItemTemplate>
											<asp:Label runat="server" CssClass="gridLabel" ID="lblRemarks" Text='<%#DataBinder.Eval(Container.DataItem,"remarks")%>'>
											</asp:Label>
										</ItemTemplate>
										<EditItemTemplate>
											<asp:TextBox CssClass="gridTextBox" runat="server" ID="txtRemarks" Text='<%#DataBinder.Eval(Container.DataItem,"remarks")%>'>
											</asp:TextBox>
										</EditItemTemplate>
									</asp:TemplateColumn>
								</Columns>
								<PagerStyle NextPageText="Next" PrevPageText="Previous" HorizontalAlign="Right" CssClass="normalText"></PagerStyle>
							</asp:datagrid></td>
						<td colSpan="1"></td>
					</tr>
					<tr height="50">
						<td align="right" colSpan="14"><FONT face="Tahoma">
								<asp:button id="btnDGInsert" tabIndex="200" runat="server" CssClass="queryButton" Text="Insert" Width="76px"></asp:button></FONT></td>
						<td></td>
					</tr>
					<tr height="50">
						<TD colSpan="1"></TD>
						<td colSpan="15"><asp:datagrid id="dgPkgInfo" runat="server" Width="795px" AutoGenerateColumns="False" OnEditCommand="dgPkgInfo_Edit" OnPageIndexChanged="dgPkgInfo_PageChange" OnCancelCommand="dgPkgInfo_Cancel" OnDeleteCommand="dgPkgInfo_Delete" OnUpdateCommand="dgPkgInfo_Update" PageSize="4">
								<Columns>
									<asp:ButtonColumn Text="&lt;IMG SRC='images/butt-delete.gif' border=0 title='Delete' &gt;" ButtonType="LinkButton" CommandName="Delete">
										<HeaderStyle Width="3%" CssClass="gridHeading"></HeaderStyle>
										<ItemStyle CssClass="gridField"></ItemStyle>
									</asp:ButtonColumn>
									<asp:EditCommandColumn ButtonType="LinkButton" UpdateText="&lt;IMG SRC='images/butt-update.gif' border=0  title='Update' &gt;" CancelText="&lt;IMG SRC='images/butt-cancel.gif' border=0 title='Cancel'&gt;" EditText="&lt;IMG SRC='images/butt-edit.gif' border=0  title='Edit' &gt;">
										<HeaderStyle Width="3%" CssClass="gridHeading"></HeaderStyle>
										<ItemStyle CssClass="gridField"></ItemStyle>
									</asp:EditCommandColumn>
									<asp:TemplateColumn HeaderText="MPS No">
										<HeaderStyle Width="10%" CssClass="gridHeading"></HeaderStyle>
										<ItemStyle CssClass="gridField"></ItemStyle>
										<ItemTemplate>
											<asp:Label CssClass="gridLabel" ID="lblMPSNo" Text='<%#DataBinder.Eval(Container.DataItem,"mps_no")%>' Runat="server">
											</asp:Label>
										</ItemTemplate>
										<EditItemTemplate>
											<cc1:mstextbox CssClass="gridTextBox" ID="txtMPSNo" Text='<%#DataBinder.Eval(Container.DataItem,"mps_no")%>' Runat="server" Enabled="True" MaxLength="12" TextMaskType="msUpperAlfaNumericWithUnderscore" >
											</cc1:mstextbox>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Length">
										<HeaderStyle Width="10%" CssClass="gridHeading"></HeaderStyle>
										<ItemStyle CssClass="gridField"></ItemStyle>
										<ItemTemplate>
											<asp:Label CssClass="gridLabel" ID="lblLength" Text='<%#DataBinder.Eval(Container.DataItem,"pkg_length","{0:#0}")%>' Runat="server" Enabled="True">
											</asp:Label>
										</ItemTemplate>
										<EditItemTemplate>
											<cc1:mstextbox CssClass="gridTextBox" ID="txtLength" Text='<%#DataBinder.Eval(Container.DataItem,"pkg_length","{0:#0}")%>' Runat="server" TextMaskType="msNumeric" MaxLength="7" NumberMaxValue="10000" NumberMinValue="0" NumberPrecision="6" NumberScale="1">
											</cc1:mstextbox>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Breadth">
										<HeaderStyle Width="10%" CssClass="gridHeading"></HeaderStyle>
										<ItemStyle CssClass="gridField" VerticalAlign="Middle"></ItemStyle>
										<ItemTemplate>
											<asp:Label CssClass="gridLabel" ID="lblBreadth" Text='<%#DataBinder.Eval(Container.DataItem,"pkg_breadth","{0:#0}")%>' Runat="server" Enabled="True" >
											</asp:Label>
										</ItemTemplate>
										<EditItemTemplate>
											<cc1:mstextbox CssClass="gridTextBox" ID="txtBreadth" Text='<%#DataBinder.Eval(Container.DataItem,"pkg_breadth","{0:#0}")%>' Runat="server" TextMaskType="msNumeric" MaxLength="7" NumberMaxValue="10000" NumberMinValue="0" NumberPrecision="6" NumberScale="1">
											</cc1:mstextbox>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Height">
										<HeaderStyle Width="10%" CssClass="gridHeading"></HeaderStyle>
										<ItemStyle CssClass="gridField" VerticalAlign="Middle"></ItemStyle>
										<ItemTemplate>
											<asp:Label CssClass="gridLabel" ID="lblHeight" Text='<%#DataBinder.Eval(Container.DataItem,"pkg_height","{0:#0}")%>' Runat="server" Enabled="True">
											</asp:Label>
										</ItemTemplate>
										<EditItemTemplate>
											<cc1:mstextbox CssClass="gridTextBox" ID="txtHeight" Text='<%#DataBinder.Eval(Container.DataItem,"pkg_height","{0:#0}")%>' Runat="server" TextMaskType="msNumeric" MaxLength="7" NumberMaxValue="10000" NumberMinValue="0" NumberPrecision="6" NumberScale="1">
											</cc1:mstextbox>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Volume">
										<HeaderStyle Width="10%" CssClass="gridHeading"></HeaderStyle>
										<ItemStyle CssClass="gridField"></ItemStyle>
										<ItemTemplate>
											<asp:Label CssClass="gridLabel" ID="lblVolume" Text='<%#DataBinder.Eval(Container.DataItem,"pkg_volume","{0:#0}")%>' Runat="server">
											</asp:Label>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Weight">
										<HeaderStyle Width="10%" CssClass="gridHeading"></HeaderStyle>
										<ItemStyle CssClass="gridField"></ItemStyle>
										<ItemTemplate>
											<asp:Label CssClass="gridLabelNumber" ID="lblWeight" Text='<%#DataBinder.Eval(Container.DataItem,"pkg_wt","{0:n}")%>' Runat="server">
											</asp:Label>
										</ItemTemplate>
										<EditItemTemplate>
											<cc1:mstextbox CssClass="gridTextBox" ID="txtWeight" Text='<%#DataBinder.Eval(Container.DataItem,"pkg_wt")%>' Runat="server" Enabled="True" TextMaskType="msNumeric" MaxLength="7" NumberMaxValue="10000" NumberMinValue="0" NumberPrecision="6" NumberScale="2">
											</cc1:mstextbox>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Qty">
										<HeaderStyle Width="10%" CssClass="gridHeading"></HeaderStyle>
										<ItemStyle CssClass="gridField"></ItemStyle>
										<ItemTemplate>
											<asp:Label CssClass="gridLabel" ID="lblQty" Text='<%#DataBinder.Eval(Container.DataItem,"pkg_qty","{0:#0.00}")%>' Runat="server">
											</asp:Label>
										</ItemTemplate>
										<EditItemTemplate>
											<cc1:mstextbox CssClass="gridTextBox" ID="txtQty" Text='<%#DataBinder.Eval(Container.DataItem,"pkg_qty")%>' Runat="server" Enabled="True" TextMaskType="msNumeric" MaxLength="4" NumberMaxValue="9999" NumberMinValue="0" NumberPrecision="4" NumberScale="0">
											</cc1:mstextbox>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="TotalWt">
										<HeaderStyle Width="10%" CssClass="gridHeading"></HeaderStyle>
										<ItemStyle CssClass="gridField"></ItemStyle>
										<ItemTemplate>
											<asp:Label CssClass="gridLabelNumber" ID="lblTotWt" Text='<%#DataBinder.Eval(Container.DataItem,"tot_wt","{0:#0.00}")%>' Runat="server">
											</asp:Label>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Total Dim Wt">
										<HeaderStyle Width="14%" CssClass="gridHeading"></HeaderStyle>
										<ItemStyle CssClass="gridField"></ItemStyle>
										<ItemTemplate>
											<asp:Label CssClass="gridLabelNumber" ID="lblDimWt" Text='<%#DataBinder.Eval(Container.DataItem,"tot_dim_wt")%>' Runat="server">
											</asp:Label>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Chargeable Weight">
										<HeaderStyle Width="14%" CssClass="gridHeading"></HeaderStyle>
										<ItemStyle CssClass="gridField"></ItemStyle>
										<ItemTemplate>
											<asp:label CssClass="gridLabelNumber" ID="lblChargwt" Text='<%#DataBinder.Eval(Container.DataItem,"chargeable_wt","{0:#0.00}")%>' Runat="server">
											</asp:label>
										</ItemTemplate>
									</asp:TemplateColumn>
								</Columns>
								<PagerStyle NextPageText="Next" PrevPageText="Previous" HorizontalAlign="Right" CssClass="normalText"></PagerStyle>
							</asp:datagrid></td>
						<td colSpan="1"></td>
					</tr>
					<tr height="50">
						<td align="right" colSpan="14">
							<asp:button id="btnInstPkgDetalis" runat="server" CssClass="queryButton" Text="Insert" Width="75px"></asp:button></td>
						<td></td>
					</tr>
				</TABLE>
			</div>
			<DIV id="ShipmentPanel" style="Z-INDEX: 101; LEFT: -1px; WIDTH: 746px; POSITION: relative; TOP: 46px; HEIGHT: 288px" MS_POSITIONING="GridLayout" runat="server">
				<P align="center"><br>
					<asp:button id="btnToSaveChanges" style="Z-INDEX: 101; LEFT: 292px; POSITION: absolute; TOP: 140px" runat="server" CssClass="queryButton" CausesValidation="False" Text="Yes"></asp:button><asp:button id="btnNotToSave" style="Z-INDEX: 102; LEFT: 336px; POSITION: absolute; TOP: 140px" runat="server" CssClass="queryButton" CausesValidation="False" Text="No"></asp:button><asp:button id="btnToCancel" style="Z-INDEX: 103; LEFT: 370px; POSITION: absolute; TOP: 140px" runat="server" CssClass="queryButton" CausesValidation="False" Text="Cancel"></asp:button><asp:label id="lblConfirmMsg" style="Z-INDEX: 104; LEFT: 136px; POSITION: absolute; TOP: 86px" runat="server" Width="468px"></asp:label></P>
			</DIV>
			<DIV id="divBaseRate" style="Z-INDEX: 101; LEFT: -1px; WIDTH: 746px; POSITION: relative; TOP: 46px; HEIGHT: 288px" MS_POSITIONING="GridLayout" runat="server"><asp:label id="lblBaseRate" style="Z-INDEX: 101; LEFT: 253px; POSITION: absolute; TOP: 67px" runat="server" Width="351px" Height="33px">The customer has no Active Quotation.Base Rates will be used to calculate the freight.<BR>Do you want to continue ?</asp:label>
				<P align="center"><asp:button id="btnBaseRateYes" style="Z-INDEX: 102; LEFT: 343px; POSITION: absolute; TOP: 123px" runat="server" CssClass="queryButton" CausesValidation="False" Text="Yes"></asp:button><asp:button id="btnBaseRateNo" style="Z-INDEX: 103; LEFT: 384px; POSITION: absolute; TOP: 124px" runat="server" CssClass="queryButton" CausesValidation="False" Text="No"></asp:button></P>
			</DIV>
			<input 
type=hidden value="<%=strScrollPosition%>" name=ScrollPosition> <input type="hidden" name="hdnRefresh">
		</form>
	</body>
</HTML>
