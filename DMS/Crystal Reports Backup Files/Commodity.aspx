<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<%@ Page language="c#" Codebehind="Commodity.aspx.cs" AutoEventWireup="false" Inherits="com.ties.Commodity" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Commodity</title>
		<LINK href="css/Styles.css" type="text/css" rel="stylesheet" name="stylesheet">
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<!--#INCLUDE FILE="msFormValidations.inc"-->
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Commodity" method="post" runat="server">
			<asp:button id="btnQuery" style="Z-INDEX: 101; LEFT: 18px; POSITION: absolute; TOP: 63px" runat="server" CausesValidation="False" CssClass="queryButton" Width="100px" Text="Query"></asp:button><asp:button id="btnExecuteQuery" style="Z-INDEX: 103; LEFT: 119px; POSITION: absolute; TOP: 63px" runat="server" CausesValidation="False" CssClass="queryButton" Width="100px" Text="Execute Query" Enabled="False"></asp:button><asp:button id="btnInsert" style="Z-INDEX: 102; LEFT: 220px; POSITION: absolute; TOP: 63px" runat="server" CausesValidation="False" CssClass="queryButton" Width="100px" Text="Insert"></asp:button>&nbsp;
			<asp:datagrid id="dgCommodity" style="Z-INDEX: 104; LEFT: 18px; POSITION: absolute; TOP: 125px" runat="server" Width="648px" ItemStyle-Height="20" OnDeleteCommand="OnDelete_Commodity" AutoGenerateColumns="False" OnCancelCommand="OnCancel_Commodity" OnUpdateCommand="OnUpdate_Commodity" OnEditCommand="OnEdit_Commodity" OnPageIndexChanged="OnCommodity_PageChange" AllowPaging="True" AllowCustomPaging="True" OnItemDataBound="OnItemBound_Commodity">
				<SelectedItemStyle CssClass="gridFieldSelected"></SelectedItemStyle>
				<ItemStyle Height="20px" CssClass="gridField"></ItemStyle>
				<Columns>
					<asp:EditCommandColumn ButtonType="LinkButton" UpdateText="&lt;IMG SRC='images/butt-update.gif' border=0  title='Update' &gt;" CancelText="&lt;IMG SRC='images/butt-cancel.gif' border=0 title='Cancel'&gt;" EditText="&lt;IMG SRC='images/butt-edit.gif' border=0  title='Edit' &gt;">
						<HeaderStyle Width="2%" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle CssClass="gridField"></ItemStyle>
					</asp:EditCommandColumn>
					<asp:ButtonColumn Text="&lt;IMG SRC='images/butt-delete.gif' border=0 title='Delete' &gt;" CommandName="Delete">
						<HeaderStyle HorizontalAlign="Center" Width="3%" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle HorizontalAlign="Center" CssClass="gridField"></ItemStyle>
					</asp:ButtonColumn>
					<asp:TemplateColumn HeaderText="Code">
						<HeaderStyle Width="30%" CssClass="gridHeading" Font-Bold="True"></HeaderStyle>
						<ItemStyle CssClass="gridField"></ItemStyle>
						<ItemTemplate>
							<asp:Label CssClass="gridLabel" ID="lblCode" Visible=True Text='<%#DataBinder.Eval(Container.DataItem,"commodity_code")%>' Runat="server">
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<cc1:mstextbox CssClass="gridTextBox" ID="txtCode" Visible=True Text='<%#DataBinder.Eval(Container.DataItem,"commodity_code")%>' Runat="server" TextMaskType="msUpperAlfaNumericWithUnderscore" MaxLength="12">
							</cc1:mstextbox>
							<asp:RequiredFieldValidator ID="cCommCode" Runat="server" BorderWidth="0" Display="None" ControlToValidate="txtCode" ErrorMessage="Commodity Code "></asp:RequiredFieldValidator>
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Type">
						<HeaderStyle Width="25%" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle CssClass="gridField"></ItemStyle>
						<ItemTemplate>
							<asp:DropDownList ID="ddType" CssClass="gridTextBox" Runat="server"></asp:DropDownList>
						</ItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Description">
						<HeaderStyle Width="45%" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle CssClass="gridField"></ItemStyle>
						<ItemTemplate>
							<asp:Label CssClass="gridLabel" ID="lblDescription" Visible=True Text='<%#DataBinder.Eval(Container.DataItem,"commodity_description")%>' Runat="server">
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<asp:TextBox CssClass="gridTextBox" ID="txtDescription" Visible=True Text='<%#DataBinder.Eval(Container.DataItem,"commodity_description")%>' Runat="server" MaxLength="200">
							</asp:TextBox>
						</EditItemTemplate>
					</asp:TemplateColumn>
				</Columns>
				<PagerStyle NextPageText="Next" PrevPageText="Previous" HorizontalAlign="Right" CssClass="normalText"></PagerStyle>
			</asp:datagrid><asp:label id="lblValCommodity" style="Z-INDEX: 105; LEFT: 22px; POSITION: absolute; TOP: 94px" runat="server" CssClass="errorMsgColor" Width="669px"></asp:label><asp:label id="Label1" style="Z-INDEX: 106; LEFT: 27px; POSITION: absolute; TOP: 16px" runat="server" CssClass="mainTitleSize" Width="399px" Height="37px">Commodity</asp:label><asp:validationsummary id="PageValidationSummary" style="Z-INDEX: 105; LEFT: 728px; POSITION: absolute; TOP: 152px" runat="server" HeaderText="Please enter the missing  fields." DisplayMode="BulletList" ShowSummary="False" ShowMessageBox="True"></asp:validationsummary></form>
	</body>
</HTML>
