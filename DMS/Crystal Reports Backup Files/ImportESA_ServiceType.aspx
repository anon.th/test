<%@ Page language="c#" Codebehind="ImportESA_ServiceType.aspx.cs" AutoEventWireup="false" Inherits="com.ties.ImportESA_ServiceType" %>
<%@ Import Namespace="com.common.util" %>
<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>ImportESA_ServiceType</title>
		<LINK href="css/Styles.css" type="text/css" rel="stylesheet">
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<!--#INCLUDE FILE="msFormValidations.inc"-->
		<script language="javascript">	
		function setValue()
		{
			var s = document.all['inFile'];
			document.all['txtFilePath'].innerText = s.value;
			if(s.value != '') 
				document.all['btnImport'].disabled = false;
			else
				document.all['btnImport'].disabled = true;
		}
	
		function upBrowse()
		{
			document.all['divBrowse'].style.backgroundImage='url(images/btn-browse-up.GIF)';
		}
		
		function downBrowse()
		{
			document.all['divBrowse'].style.backgroundImage='url(images/btn-browse-down.GIF)';
		}		
		
		function clearValue()
		{
			if(document.all['txtFilePath'].innerText == '') {
				document.location.reload();
			}
		}
		</script>
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="ImportESA_ServiceType" method="post" encType="multipart/form-data" runat="server">
			<asp:datagrid id="dgImportESA_Service" style="Z-INDEX: 100; LEFT: 16px; POSITION: absolute; TOP: 117px" runat="server" Width="751px" PageSize="50" ItemStyle-Height="20" AutoGenerateColumns="False" OnEditCommand="dgImportESA_Service_Edit" OnItemCommand="dgImportESA_Service_Button" OnDeleteCommand="dgImportESA_Service_Delete" OnUpdateCommand="dgImportESA_Service_Update" OnCancelCommand="dgImportESA_Service_Cancel" OnPageIndexChanged="dgImportESA_Service_PageChange" AllowPaging="True">
				<ItemStyle Height="20px" CssClass="gridField"></ItemStyle>
				<Columns>
					<asp:TemplateColumn>
						<HeaderStyle CssClass="gridHeading"></HeaderStyle>
						<ItemStyle Wrap="False" CssClass="gridField"></ItemStyle>
						<ItemTemplate>
							<asp:ImageButton id="imgSelect" runat="server" ImageUrl="images/butt-select.gif" Enabled="False"></asp:ImageButton>&nbsp;&nbsp;
						</ItemTemplate>
					</asp:TemplateColumn>
					<asp:EditCommandColumn ButtonType="LinkButton" UpdateText="&lt;IMG SRC='images/butt-update.gif' border=0 title='Update' &gt;" CancelText="&lt;IMG SRC='images/butt-cancel.gif' border=0 title='Cancel' &gt;" EditText="&lt;IMG SRC='images/butt-edit.gif' border=0 title='Edit' &gt;">
						<HeaderStyle CssClass="gridHeading"></HeaderStyle>
						<ItemStyle HorizontalAlign="Left"></ItemStyle>
					</asp:EditCommandColumn>
					<asp:ButtonColumn Text="&lt;IMG SRC='images/butt-delete.gif' border=0 title='Delete' &gt;" CommandName="Delete">
						<HeaderStyle CssClass="gridHeading"></HeaderStyle>
						<ItemStyle HorizontalAlign="Center"></ItemStyle>
					</asp:ButtonColumn>
					<asp:TemplateColumn HeaderText="Customer">
						<HeaderStyle Font-Bold="True" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle CssClass="gridField"></ItemStyle>
						<ItemTemplate>
							<asp:Label CssClass="gridLabel" ID="lblCustID" Text='<%#DataBinder.Eval(Container.DataItem,"custid")%>' Runat="server">
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<cc1:mstextbox CssClass="gridTextBox" ID="txtCustID" Text='<%#DataBinder.Eval(Container.DataItem,"custid")%>' Runat="server" Enabled="True" TextMaskType="msUpperAlfaNumericWithUnderscore" MaxLength="12">
							</cc1:mstextbox>
							<asp:RequiredFieldValidator ID="ozcValidator" Runat="server" BorderWidth="0" Display="None" ControlToValidate="txtCustID" ErrorMessage="Customer ID "></asp:RequiredFieldValidator>
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="ESA Sector">
						<HeaderStyle Font-Bold="True" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle CssClass="gridField"></ItemStyle>
						<ItemTemplate>
							<asp:Label CssClass="gridLabel" ID="lblESA_Sector" Text='<%#DataBinder.Eval(Container.DataItem,"ESA_Sector")%>' Runat="server">
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<cc1:mstextbox CssClass="gridTextBox" ID="txtESA_Sector" Text='<%#DataBinder.Eval(Container.DataItem,"ESA_Sector")%>' Runat="server" Enabled="True" TextMaskType="msUpperAlfaNumericWithUnderscore" MaxLength="12" >
							</cc1:mstextbox>
							<asp:RequiredFieldValidator ID="odcValidator" Runat="server" BorderWidth="0" Display="None" ControlToValidate="txtESA_Sector" ErrorMessage="ESA Sector"></asp:RequiredFieldValidator>
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:ButtonColumn Text="&lt;IMG SRC='images/butt-search.gif' border=0 title='Search' &gt;" CommandName="zoneSearch">
						<HeaderStyle Width="5%" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle Wrap="False" CssClass="gridField"></ItemStyle>
					</asp:ButtonColumn>
					<asp:TemplateColumn HeaderText="Service Type">
						<HeaderStyle Font-Bold="True" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle CssClass="gridField"></ItemStyle>
						<ItemTemplate>
							<asp:Label CssClass="gridLabel" ID="lblServiceCode" Text='<%#DataBinder.Eval(Container.DataItem,"service_code")%>' Runat="server">
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<cc1:mstextbox CssClass="gridTextBox" ID="txtServiceCode" Text='<%#DataBinder.Eval(Container.DataItem,"service_code")%>' Runat="server" Enabled="True" TextMaskType="msUpperAlfaNumericWithUnderscore" MaxLength="12" >
							</cc1:mstextbox>
							<asp:RequiredFieldValidator ID="ScValidator" Runat="server" BorderWidth="0" Display="None" ControlToValidate="txtServiceCode" ErrorMessage="Service Type "></asp:RequiredFieldValidator>
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:ButtonColumn Text="&lt;IMG SRC='images/butt-search.gif' border=0 title='Search' &gt;" CommandName="serviceSearch">
						<HeaderStyle Width="5%" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle Wrap="False" CssClass="gridField"></ItemStyle>
					</asp:ButtonColumn>
					<asp:TemplateColumn HeaderText="ESA Surcharge Amt.">
						<HeaderStyle Font-Bold="True" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle CssClass="gridField"></ItemStyle>
						<ItemTemplate>
							<asp:Label CssClass="gridLabelNumber" ID="lblesa_surcharge_amt" Text='<%#DataBinder.Eval(Container.DataItem,"esa_surcharge_amt","{0:#0.0}")%>' Runat="server" Enabled="True">
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<cc1:mstextbox CssClass="gridTextBoxNumber" ID="txtesa_surcharge_amt" Text='<%#DataBinder.Eval(Container.DataItem,"esa_surcharge_amt","{0:#0.0}")%>' Runat="server" MaxLength="6" TextMaskType="msNumeric" NumberMaxValue="9999" NumberMinValue="0" NumberPrecision="5" NumberScale="1">
							</cc1:mstextbox>
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="ESA Surcharge %">
						<HeaderStyle Font-Bold="True" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle CssClass="gridField"></ItemStyle>
						<ItemTemplate>
							<asp:Label CssClass="gridLabelNumber" ID="lblesa_surcharge_percent" Text='<%#DataBinder.Eval(Container.DataItem,"esa_surcharge_percent","{0:#0.0}")%>' Runat="server" Enabled="True">
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<cc1:mstextbox CssClass="gridTextBoxNumber" ID="txtesa_surcharge_percent" Text='<%#DataBinder.Eval(Container.DataItem,"esa_surcharge_percent","{0:#0.0}")%>' Runat="server" MaxLength="6" TextMaskType="msNumeric" NumberMaxValue="9999" NumberMinValue="0" NumberPrecision="5" NumberScale="1">
							</cc1:mstextbox>
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Min ESA Amount">
						<HeaderStyle CssClass="gridHeading"></HeaderStyle>
						<ItemStyle CssClass="gridField"></ItemStyle>
						<ItemTemplate>
							<asp:Label CssClass="gridLabelNumber" ID="lblmin_esa_amount" Text='<%#DataBinder.Eval(Container.DataItem,"min_esa_amount","{0:n}")%>' Runat="server" Enabled="True">
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<cc1:mstextbox CssClass="gridTextBoxNumber" ID="txtmin_esa_amount" Text='<%#DataBinder.Eval(Container.DataItem,"min_esa_amount","{0:n}")%>' Runat="server" MaxLength="9" TextMaskType="msNumeric" NumberMaxValue="999999" NumberMinValue="0" NumberPrecision="8" NumberScale="2">
							</cc1:mstextbox>
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Max ESA Amount">
						<HeaderStyle CssClass="gridHeading"></HeaderStyle>
						<ItemStyle CssClass="gridField"></ItemStyle>
						<ItemTemplate>
							<asp:Label CssClass="gridLabelNumber" ID="lblmax_esa_amount" Text='<%#DataBinder.Eval(Container.DataItem,"max_esa_amount","{0:n}")%>' Runat="server" Enabled="True">
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<cc1:mstextbox CssClass="gridTextBoxNumber" ID="txtmax_esa_amount" Text='<%#DataBinder.Eval(Container.DataItem,"max_esa_amount","{0:n}")%>' Runat="server" MaxLength="9" TextMaskType="msNumeric" NumberMaxValue="999999" NumberMinValue="0" NumberPrecision="8" NumberScale="2">
							</cc1:mstextbox>
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Effective Date">
						<HeaderStyle CssClass="gridHeading"></HeaderStyle>
						<ItemStyle CssClass="gridField"></ItemStyle>
						<ItemTemplate>
							<asp:Label CssClass="gridLabel" ID="lblEffectivedate" Text='<%#DataBinder.Eval(Container.DataItem,"effective_date","{0:dd/MM/yyyy}")%>' Runat="server" Enabled="True">
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<cc1:mstextbox ID="txtEffectiveDate" Text='<%#DataBinder.Eval(Container.DataItem,"effective_date","{0:dd/MM/yyyy}")%>' MaxLength="10" TextMaskString="99/99/9999" TextMaskType="msDate" Runat="server">
							</cc1:mstextbox>
						</EditItemTemplate>
					</asp:TemplateColumn>
				</Columns>
				<PagerStyle VerticalAlign="Top" NextPageText="Next" Height="40px" PrevPageText="Previous" HorizontalAlign="Right" CssClass="normalText"></PagerStyle>
			</asp:datagrid><asp:label id="lblErrorMessage" style="Z-INDEX: 108; LEFT: 16px; POSITION: absolute; TOP: 88px" runat="server" Width="540px" CssClass="errorMsgColor" Height="35px">Place holder for err msg</asp:label>
			<TABLE id="xxx" style="Z-INDEX: 102; LEFT: 20px; WIDTH: 730px; POSITION: absolute; TOP: 13px; HEIGHT: 78px" width="730" border="0" runat="server">
				<TR>
					<TD style="HEIGHT: 29px"><asp:label id="lblTitle" runat="server" Width="477px" CssClass="mainTitleSize" Height="26px">Import ESA Surcharge by Service Type</asp:label></TD>
				</TR>
				<TR>
					<TD>&nbsp;<asp:button id="btnQuery" style="DISPLAY: inline; Z-INDEX: 103; LEFT: 732px; TOP: 26px" runat="server" Width="60px" Height="20px" CssClass="queryButton" Text="Query" CausesValidation="False"></asp:button><asp:button id="btnExecuteQuery" style="DISPLAY: inline; Z-INDEX: 104; LEFT: 752px; TOP: 46px" runat="server" Width="139px" Height="20px" CssClass="queryButton" Text="Execute Query" CausesValidation="False"></asp:button><asp:button id="btnImport" runat="server" Width="62px" Height="20px" CssClass="queryButton" Text="Import" Enabled="False" CausesValidation="False"></asp:button><asp:button id="btnImport_Save" runat="server" Width="66px" Height="20px" CssClass="queryButton" Text="Save" Enabled="False"></asp:button>
						<DIV onmouseup="upBrowse();" onmousedown="downBrowse();" id="divBrowse" style="DISPLAY: inline; BACKGROUND-IMAGE: url(images/btn-browse-up.GIF); WIDTH: 70px; HEIGHT: 20px"><INPUT id="inFile" onblur="setValue();" style="BORDER-RIGHT: #88a0c8 1px outset; BORDER-TOP: #88a0c8 1px outset; FONT-SIZE: 11px; FILTER: alpha(opacity: 0); BORDER-LEFT: #88a0c8 1px outset; WIDTH: 20px; COLOR: #003068; BORDER-TOP: #88a0c8 1px outset; FONT-FAMILY: Arial; BACKGROUND-COLOR: #e9edf0; TEXT-DECORATION: none" onfocus="setValue();" type="file" name="inFile" runat="server"></DIV>
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<asp:textbox id="txtFilePath" runat="server" Width="240px" CssClass="textField"></asp:textbox></TD>
				</TR>
			</TABLE>
			<asp:button id="btnInsert" style="Z-INDEX: 105; LEFT: 804px; POSITION: absolute; TOP: 65px" runat="server" Width="68px" CssClass="queryButton" Text="Insert" CausesValidation="False" Visible="False"></asp:button><asp:button id="btnImport_Show" style="Z-INDEX: 106; LEFT: 824px; POSITION: absolute; TOP: 85px" runat="server" Width="62px" CssClass="queryButton" Text="Import" Visible="False"></asp:button><asp:button id="btnImport_Cancel" style="Z-INDEX: 107; LEFT: 840px; POSITION: absolute; TOP: 126px" runat="server" Width="62px" CssClass="queryButton" Text="Cancel" Visible="False"></asp:button></form>
	</body>
</HTML>
