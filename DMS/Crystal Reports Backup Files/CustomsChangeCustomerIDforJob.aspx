<%@ Import Namespace="com.common.util" %>
<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<%@ Page language="c#" Codebehind="CustomsChangeCustomerIDforJob.aspx.cs" AutoEventWireup="false" Inherits="TIES.WebUI.CustomsChangeCustomerIDforJob" ValidateRequest="false" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>CustomsChangeCustomerIDforJob</title>
		<meta name="vs_snapToGrid" content="True">
		<meta name="vs_showGrid" content="True">
		<LINK rel="stylesheet" type="text/css" href="css/Styles.css" name="stylesheet">
		<meta name="GENERATOR" content="Microsoft Visual Studio 7.0">
		<meta name="CODE_LANGUAGE" content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<!--#INCLUDE FILE="msFormValidations.inc"-->
		<script language="javascript" src="Scripts/settingScrollPosition.js"></script>
		<script language="javascript" src="Scripts/JScript_TextControl.js"></script>
		<script language="javascript" type="text/javascript">
			function funcDisBack(){
				history.go(+1);
			}

			function RemoveBadNonNumber(strTemp, obj) {
				strTemp = strTemp.replace(/[^0-9]/g, ''); //replace non-numeric
				obj.value = strTemp.replace(/\s/g, ''); // replace space
			}
			
			function AfterPasteNonNumber(obj) {
				setTimeout(function () {
					RemoveBadNonNumber(obj.value, obj);
				}, 1); //or 4
			}
			
			function validate(evt) {
				var theEvent = evt || window.event;
				var key = theEvent.keyCode || theEvent.which;
				key = String.fromCharCode( key );
				var regex = /[0-9]/;
				if( !regex.test(key) ) {
					theEvent.returnValue = false;
					if(theEvent.preventDefault) theEvent.preventDefault();
			}
		}
		</script>
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="CustomsChangeCustomerIDforJob" method="post" runat="server">
			<div style="Z-INDEX: 102; POSITION: relative; WIDTH: 1008px; HEIGHT: 1543px; TOP: 34px; LEFT: 24px"
				id="divMain" MS_POSITIONING="GridLayout" runat="server">
				<TABLE style="Z-INDEX: 101; POSITION: absolute; TOP: 8px; LEFT: 0px" id="MainTable" border="0"
					width="100%" runat="server">
					<tbody>
						<tr>
							<td>
								<asp:label id="Label1" runat="server" Width="808px" Height="27px" CssClass="mainTitleSize">Change Customer ID or House AWB Number for Customs Job</asp:label>
							</td>
						</tr>
						<tr>
							<td>
								<asp:button id="btnChangeCustomerID" runat="server" CssClass="queryButton" Text="Change ID or Number"
									CausesValidation="False"></asp:button>
							</td>
						</tr>
						<tr>
							<TD style="HEIGHT: 22px" colSpan="6">
								<asp:label id="lblError" runat="server" Font-Size="X-Small" Font-Bold="True" ForeColor="Red"></asp:label>
							</TD>
						</tr>
						<tr>
							<TD colSpan="6">
								<FIELDSET style="Z-INDEX: 0; WIDTH: 380px; HEIGHT: 30px"><LEGEND>
										<asp:label id="lblDates" runat="server" CssClass="tableHeadingFieldset" Width="20px" Font-Bold="True">Change</asp:label></LEGEND>
									<TABLE style="WIDTH: 380px; id: " tblDatescellSpacing="0" cellPadding="0" align="left"
										runat="server" border="0">
										<TR>
											<TD>
												<asp:radiobutton style="Z-INDEX: 0" id="rbCustomerID" tabIndex="1" runat="server" CssClass="tableRadioButton"
													Height="22px" Width="112px" Text="Customer ID" Checked="True" GroupName="Change" AutoPostBack="True"></asp:radiobutton>
												<asp:radiobutton style="Z-INDEX: 0" id="rbHouseAWBNumber" tabIndex="2" runat="server" CssClass="tableRadioButton"
													Height="22px" Width="224px" Text="House AWB Number" GroupName="Change" AutoPostBack="True"></asp:radiobutton></TD>
										</TR>
									</TABLE>
								</FIELDSET>
							</TD>
						</tr>
						<tr>
							<td>
								<table style="WIDTH: 352px; HEIGHT: 98px">
									<tbody>
										<tr>
											<td style="WIDTH: 172px">
												<asp:label style="Z-INDEX: 0" id="Label2" runat="server" CssClass="tableLabel">Customs Job Entry Number</asp:label><asp:label id="Label4" runat="server" CssClass="tableLabel" ForeColor="Red">*</asp:label>
											</td>
											<td>
												<asp:textbox id="txtCustomsJobEntryNumber" style="Z-INDEX: 0" tabIndex="1" runat="server" Width="168px"
													onkeypress="validate(event)" onpaste="AfterPasteNonNumber(this)" CssClass="textField" MaxLength="18"></asp:textbox>
											</td>
										</tr>
										<tr>
											<td style="WIDTH: 172px">
												<asp:label style="Z-INDEX: 0" id="Label3" runat="server" CssClass="tableLabel">House AWB Number</asp:label><asp:label id="Label5" runat="server" CssClass="tableLabel" ForeColor="Red">*</asp:label>
											</td>
											<td>
												<asp:textbox id="txtHouseAWBNumber" style="Z-INDEX: 0;TEXT-TRANSFORM: uppercase" tabIndex="2"
													runat="server" Width="168px" CssClass="textField" MaxLength="30"></asp:textbox>
											</td>
										</tr>
										<tr>
											<td style="WIDTH: 172px">
												<asp:label style="Z-INDEX: 0" id="Label6" runat="server" CssClass="tableLabel">Current Customer ID</asp:label><asp:label id="Label7" runat="server" CssClass="tableLabel" ForeColor="Red">*</asp:label>
											</td>
											<td>
												<asp:textbox id="txtCurrentCustomerID" style="Z-INDEX: 0;TEXT-TRANSFORM: uppercase" tabIndex="3"
													runat="server" Width="168px" CssClass="textField" MaxLength="20"></asp:textbox>
											</td>
										</tr>
										<tr>
											<td style="WIDTH: 172px">
												<asp:label style="Z-INDEX: 0" id="lblNewCustomerID" runat="server" CssClass="tableLabel">New Customer ID</asp:label><asp:label id="lblNewCustomerIDRed" runat="server" CssClass="tableLabel" ForeColor="Red">*</asp:label>
											</td>
											<td>
												<asp:textbox id="txtNewCustomerID" style="Z-INDEX: 0;TEXT-TRANSFORM: uppercase" tabIndex="4"
													runat="server" Width="168px" CssClass="textField" MaxLength="20"></asp:textbox>
											</td>
										</tr>
										<tr>
											<td style="WIDTH: 172px">
												<asp:label style="Z-INDEX: 0" id="lblNewHouseAWBNumber" runat="server" CssClass="tableLabel">New House AWB Number</asp:label><asp:label id="lblNewHouseAWBNumberRed" runat="server" CssClass="tableLabel" ForeColor="Red">*</asp:label>
											</td>
											<td>
												<asp:textbox id="txtNewHouseAWBNumber" style="Z-INDEX: 0;TEXT-TRANSFORM: uppercase" tabIndex="4"
													runat="server" Width="168px" CssClass="textField" MaxLength="20"></asp:textbox>
											</td>
										</tr>
									</tbody>
								</table>
							</td>
						</tr>
					</tbody>
				</TABLE>
			</div>
			<DIV style="Z-INDEX: 0; POSITION: relative; WIDTH: 625px; HEIGHT: 217px; TOP: 34px; LEFT: 22px"
				id="divProm" MS_POSITIONING="GridLayout" runat="server">
				<TABLE style="POSITION: absolute; WIDTH: 545px; HEIGHT: 107px; TOP: 20px; LEFT: 21px" id="Table4"
					runat="server">
					<TR>
						<TD>
							<P align="center"><asp:label id="lblProm" runat="server" Height="36px" Width="500px"></asp:label></P>
							<P align="center"><asp:button id="btnPromYes" runat="server" CssClass="queryButton" CausesValidation="False" Text="Yes"></asp:button>&nbsp;
								<asp:button id="btnPromNo" runat="server" CssClass="queryButton" CausesValidation="False" Text=" No "></asp:button>&nbsp;
								<asp:button id="btnCancel" runat="server" CssClass="queryButton" CausesValidation="False" Text=" Cancel "></asp:button></P>
						</TD>
					</TR>
				</TABLE>
			</DIV>
		</form>
	</body>
</HTML>
