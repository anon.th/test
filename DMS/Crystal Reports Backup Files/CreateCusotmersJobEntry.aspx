<%@ Page language="c#" Codebehind="CreateCusotmersJobEntry.aspx.cs" AutoEventWireup="false" Inherits="TIES.WebUI.CreateCusotmersJobEntry" %>
<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Customs Job Registration</title>
		<LINK href="css/Styles.css" type="text/css" rel="stylesheet" name="stylesheet">
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<script language="javascript" src="Scripts/settingScrollPosition.js"></script>
		<script type="text/javascript">
<!--
function onButtonClick()
{
 window.close();
}
// -->
		</script>
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<asp:label style="Z-INDEX: 101; POSITION: absolute; TOP: 8px; LEFT: 8px" id="Label38" runat="server"
				CssClass="tableLabel" ForeColor="Red">*</asp:label>
			<div style="POSITION: relative; WIDTH: 807px; HEIGHT: 492px" id="divMain" MS_POSITIONING="GridLayout"
				runat="server">
				<TABLE style="POSITION: absolute" id="MainTable" border="0" width="100%" runat="server">
					<tr>
						<td colSpan="2" align="left"><asp:label id="Label1" runat="server" Width="421px" Height="27px" CssClass="mainTitleSize">
							Customs Job Registration</asp:label></td>
					</tr>
					<tr>
						<td style="HEIGHT: 26px" vAlign="top" colSpan="2" align="left">
							<asp:button id="btnSave" runat="server" CssClass="queryButton" Text="Save" CausesValidation="False"
								Enabled="False"></asp:button>
							<asp:button id="btnClose" runat="server" CssClass="queryButton" Text="Close" CausesValidation="False"
								Enabled="true"></asp:button></td>
					</tr>
					<tr>
						<td vAlign="top" colSpan="2" align="left">
							<table border="0" cellSpacing="1" cellPadding="1" width="800">
								<TR>
									<TD style="HEIGHT: 19px" colSpan="6"><asp:label id="lblError" runat="server" Font-Size="X-Small" Font-Bold="True" ForeColor="Red"></asp:label></TD>
								</TR>
								<tr>
									<td><asp:label id="Label4" runat="server" CssClass="tableLabel"> Job Entry:</asp:label></td>
									<td style="WIDTH: 235px"><FONT face="Tahoma">
											<asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea" id="txtJobEntryID"
												tabIndex="14" runat="server" CssClass="textField" Width="200px" Enabled="False" MaxLength="100"
												ReadOnly="True"></asp:textbox></FONT></td>
									<td><asp:label id="Label5" runat="server" CssClass="tableLabel"> Info Received Date:</asp:label><asp:label id="Label22" runat="server" CssClass="tableLabel" ForeColor="Red">*</asp:label></td>
									<td colSpan="3">
										<cc1:mstextbox style="Z-INDEX: 0" id="txtDateFrom" tabIndex="21" runat="server" CssClass="textField"
											Width="100px" MaxLength="12" TextMaskString="99/99/9999" TextMaskType="msDate">23/05/2014</cc1:mstextbox></td>
								</tr>
								<TR>
									<TD><FONT face="Tahoma">
											<asp:label style="Z-INDEX: 0" id="Label2" runat="server" CssClass="tableLabel">House AWB Number:</asp:label>
											<asp:label style="Z-INDEX: 0" id="Label23" runat="server" CssClass="tableLabel" ForeColor="Red">*</asp:label></FONT></TD>
									<TD style="WIDTH: 235px">
										<asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea" id="txtHouseAWBNumber"
											tabIndex="15" runat="server" CssClass="textField" Width="200px" MaxLength="100" Enabled="False">543232993</asp:textbox></TD>
									<TD>
										<asp:label style="Z-INDEX: 0" id="Label8" runat="server" CssClass="tableLabel">Master AWB Number:</asp:label>
										<asp:label style="Z-INDEX: 0" id="Label24" runat="server" CssClass="tableLabel" ForeColor="Red">*</asp:label></TD>
									<TD colSpan="3"><FONT face="Tahoma">
											<asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase" id="txtMasterAWBNumber" tabIndex="15"
												runat="server" CssClass="textField" Width="100px" MaxLength="100"></asp:textbox></FONT></TD>
								</TR>
								<TR>
									<TD>
										<asp:label style="Z-INDEX: 0" id="Label3" runat="server" CssClass="tableLabel">Consignee (Customer ID): </asp:label></TD>
									<TD style="WIDTH: 235px">
										<asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea" id="Textbox2"
											tabIndex="14" runat="server" CssClass="textField" Width="200px" Enabled="False" MaxLength="100"
											ReadOnly="True">144840</asp:textbox></TD>
									<TD>
										<asp:label style="Z-INDEX: 0" id="Label9" runat="server" CssClass="tableLabel">Consignee�s Details:</asp:label></TD>
									<TD colSpan="3"><FONT face="Tahoma"></FONT></TD>
								</TR>
								<TR>
									<TD>
										<asp:label style="Z-INDEX: 0" id="Label6" runat="server" CssClass="tableLabel">Customer�s Tax Code: </asp:label></TD>
									<TD style="WIDTH: 235px">
										<asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea" id="Textbox3"
											tabIndex="14" runat="server" CssClass="textField" Width="200px" Enabled="False" MaxLength="100"
											ReadOnly="True">TCS413</asp:textbox></TD>
									<TD colSpan="4">
										<asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea" id="Textbox4"
											tabIndex="14" runat="server" CssClass="textField" Width="352px" Enabled="False" MaxLength="100"
											ReadOnly="True">NEC PNG</asp:textbox></TD>
								</TR>
								<TR>
									<TD>
										<asp:label style="Z-INDEX: 0" id="Label7" runat="server" CssClass="tableLabel">Mode of Transport: </asp:label>
										<asp:label style="Z-INDEX: 0" id="Label25" runat="server" CssClass="tableLabel" ForeColor="Red">*</asp:label></TD>
									<TD style="WIDTH: 235px"><FONT face="Tahoma">
											<asp:dropdownlist style="Z-INDEX: 0" id="service_code" tabIndex="31" runat="server" Width="80px">
												<asp:ListItem Value="A">A</asp:ListItem>
											</asp:dropdownlist>
											<asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea" id="Textbox13"
												tabIndex="14" runat="server" CssClass="textField" Width="118px" Enabled="False" MaxLength="100"
												ReadOnly="True">AIR</asp:textbox></FONT></TD>
									<TD colSpan="4">
										<asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea" id="Textbox5"
											tabIndex="14" runat="server" CssClass="textField" Width="352px" Enabled="False" MaxLength="100"
											ReadOnly="True">P.O.BOX 937</asp:textbox></TD>
								</TR>
								<TR>
									<TD>
										<asp:label style="Z-INDEX: 0" id="Label10" runat="server" CssClass="tableLabel">Exporter:</asp:label></TD>
									<TD style="WIDTH: 235px"><FONT face="Tahoma"></FONT></TD>
									<TD colSpan="4">
										<asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea" id="Textbox6"
											tabIndex="14" runat="server" CssClass="textField" Width="352px" Enabled="False" MaxLength="100"
											ReadOnly="True">BOROKO</asp:textbox></TD>
								</TR>
								<TR>
									<TD colspan="2">
										<asp:label style="POSITION: absolute" id="Label27" runat="server" CssClass="tableLabel" ForeColor="Red">*</asp:label>
										<asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; MARGIN-LEFT: 10px" id="txtExporter1"
											tabIndex="14" runat="server" CssClass="textField" Width="297px" Enabled="False" MaxLength="100"
											ReadOnly="True"></asp:textbox></TD>
									<TD colSpan="4">
										<asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea" id="Textbox9"
											tabIndex="14" runat="server" CssClass="textField" Width="72px" Enabled="False" MaxLength="100"
											ReadOnly="True">POM</asp:textbox>
										<asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea" id="Textbox7"
											tabIndex="14" runat="server" CssClass="textField" Width="275px" Enabled="False" MaxLength="100"
											ReadOnly="True">NATIONAL CAPITAL DISTRICT</asp:textbox></TD>
								</TR>
								<TR>
									<TD colspan="2">
										<asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; MARGIN-LEFT: 10px" id="Textbox11"
											tabIndex="14" runat="server" CssClass="textField" Width="297px" Enabled="False" MaxLength="100"
											ReadOnly="True"></asp:textbox></TD>
									<TD colSpan="4">
										<asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea" id="Textbox8"
											tabIndex="14" runat="server" CssClass="textField" Width="150px" Enabled="False" MaxLength="100"
											ReadOnly="True">6753000300</asp:textbox></TD>
								</TR>
								<TR>
									<TD colspan="2">
										<asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; MARGIN-LEFT: 10px" id="Textbox12"
											tabIndex="14" runat="server" CssClass="textField" Width="297px" Enabled="False" MaxLength="100"
											ReadOnly="True"></asp:textbox></TD>
									<TD></TD>
									<TD colSpan="3"></TD>
								</TR>
								<TR>
									<TD colspan="2">
										<asp:label style="Z-INDEX: 0" id="Label11" runat="server" CssClass="tableLabel">Country:</asp:label>
										<asp:label style="Z-INDEX: 0" id="Label28" runat="server" CssClass="tableLabel" ForeColor="Red">*</asp:label>
										<asp:dropdownlist style="Z-INDEX: 0" id="ddlCountry" tabIndex="31" runat="server" Width="80px">
											<asp:ListItem></asp:ListItem>
											<asp:ListItem Value="AU" Selected="True">AU</asp:ListItem>
										</asp:dropdownlist></FONT>
										<asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea" id="txtCountryName"
											tabIndex="14" runat="server" CssClass="textField" Width="152px" Enabled="False" MaxLength="100"
											ReadOnly="True">AUSTRALIA</asp:textbox></TD>
									<TD style="HEIGHT: 24px"></TD>
									<TD colSpan="3" style="HEIGHT: 24px"></TD>
								</TR>
								<TR>
									<TD align="right">
										<asp:label style="Z-INDEX: 0" id="Label12" runat="server" CssClass="tableLabel">Loading Port (City):</asp:label>
										<asp:label style="Z-INDEX: 0" id="Label29" runat="server" CssClass="tableLabel" ForeColor="Red">*</asp:label></TD>
									<TD style="WIDTH: 235px"><FONT face="Tahoma">
											<asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase" id="txtLoadingPortCity" tabIndex="15"
												runat="server" CssClass="textField" Width="80px" MaxLength="100"></asp:textbox></FONT></TD>
									<TD>
										<asp:label style="Z-INDEX: 0" id="Label21" runat="server" CssClass="tableLabel">Ship/Flight No:</asp:label>
										<asp:label style="Z-INDEX: 0" id="Label36" runat="server" CssClass="tableLabel" ForeColor="Red">*</asp:label></TD>
									<TD colSpan="3">
										<asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase" id="txtShipFlightNo" tabIndex="15"
											runat="server" CssClass="textField" Width="200px" MaxLength="100"></asp:textbox></TD>
								</TR>
								<TR>
									<TD align="right" style="HEIGHT: 22px">
										<asp:label style="Z-INDEX: 0" id="Label13" runat="server" CssClass="tableLabel">Country:</asp:label>
										<asp:label style="Z-INDEX: 0" id="Label30" runat="server" CssClass="tableLabel" ForeColor="Red">*</asp:label></TD>
									<TD style="WIDTH: 235px; HEIGHT: 22px"><FONT face="Tahoma">
											<asp:dropdownlist style="Z-INDEX: 0" id="ddlLoadingPortCountry" tabIndex="31" runat="server" Width="80px">
												<asp:ListItem Selected="True"></asp:ListItem>
												<asp:ListItem Value="AU">AU</asp:ListItem>
											</asp:dropdownlist>
											<asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea" id="txtLoadingPortCountryName"
												tabIndex="14" runat="server" CssClass="textField" Width="118px" Enabled="False" MaxLength="100"
												ReadOnly="True"></asp:textbox></FONT></TD>
									<TD style="HEIGHT: 22px">
										<asp:label style="Z-INDEX: 0" id="Label20" runat="server" CssClass="tableLabel">Expected Arrival Date:</asp:label>
										<asp:label style="Z-INDEX: 0" id="Label37" runat="server" CssClass="tableLabel" ForeColor="Red">*</asp:label></TD>
									<TD colSpan="3" style="HEIGHT: 22px"><FONT face="Tahoma">
											<cc1:mstextbox style="Z-INDEX: 0" id="txtExpectedArrivalDate" tabIndex="21" runat="server" CssClass="textField"
												Width="90px" MaxLength="12" TextMaskString="99/99/9999" TextMaskType="msDate"></cc1:mstextbox></FONT></TD>
								</TR>
								<TR>
									<TD align="right"><FONT face="Tahoma">
											<asp:label style="Z-INDEX: 0" id="Label14" runat="server" CssClass="tableLabel">Discharge Port:</asp:label></FONT>
										<asp:label style="Z-INDEX: 0" id="Label35" runat="server" CssClass="tableLabel" ForeColor="Red">*</asp:label></TD>
									<TD style="WIDTH: 235px"><FONT face="Tahoma">
											<asp:dropdownlist style="Z-INDEX: 0" id="Dropdownlist3" tabIndex="31" runat="server" Width="152px">
												<asp:ListItem Value="JACKSON�S AIRPORT">JACKSON�S AIRPORT</asp:ListItem>
											</asp:dropdownlist></FONT></TD>
									<TD>
										<asp:label style="Z-INDEX: 0" id="Label19" runat="server" CssClass="tableLabel">Actual Goods Arrival Date:</asp:label></TD>
									<TD colSpan="3">
										<cc1:mstextbox style="Z-INDEX: 0" id="txtActualGoodsArrivalDate" tabIndex="21" runat="server" CssClass="textField"
											Width="90px" MaxLength="12" TextMaskString="99/99/9999" TextMaskType="msDate"></cc1:mstextbox></TD>
								</TR>
								<TR>
									<TD align="right">
										<asp:label style="Z-INDEX: 0" id="Label15" runat="server" CssClass="tableLabel">Currency Name:</asp:label>
										<asp:label style="Z-INDEX: 0" id="Label33" runat="server" CssClass="tableLabel" ForeColor="Red">*</asp:label></TD>
									<TD style="WIDTH: 235px"><FONT face="Tahoma">
											<asp:dropdownlist style="Z-INDEX: 0" id="ddlCurrencyName" tabIndex="31" runat="server" Width="80px">
												<asp:ListItem></asp:ListItem>
												<asp:ListItem Value="AUD" Selected="True">AUD</asp:ListItem>
											</asp:dropdownlist></FONT></TD>
									<TD>
										<asp:label style="Z-INDEX: 0" id="Label18" runat="server" CssClass="tableLabel">Entry Type:</asp:label>
										<asp:label style="Z-INDEX: 0" id="Label34" runat="server" CssClass="tableLabel" ForeColor="Red">*</asp:label></TD>
									<TD colSpan="3">
										<asp:dropdownlist style="Z-INDEX: 0" id="Dropdownlist6" tabIndex="31" runat="server" Width="152px">
											<asp:ListItem Value="FORMAL">FORMAL</asp:ListItem>
										</asp:dropdownlist></TD>
								</TR>
								<TR>
									<TD align="right">
										<FONT face="Tahoma">
											<asp:label style="Z-INDEX: 0" id="Label16" runat="server" CssClass="tableLabel">Exchange Rate:</asp:label></FONT>
										<asp:label style="Z-INDEX: 0" id="Label32" runat="server" CssClass="tableLabel" ForeColor="Red">*</asp:label></TD>
									<TD style="WIDTH: 235px">
										<asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea" id="txtExchangeRate"
											tabIndex="14" runat="server" CssClass="textField" Width="140px" Enabled="False" MaxLength="100"
											ReadOnly="True"></asp:textbox></TD>
									<TD>
										<asp:label style="Z-INDEX: 0" id="Label17" runat="server" CssClass="tableLabel">Delivery Terms:</asp:label>
										<asp:label style="Z-INDEX: 0" id="Label39" runat="server" CssClass="tableLabel" ForeColor="Red">*</asp:label></TD>
									<TD colSpan="3">
										<asp:dropdownlist style="Z-INDEX: 0" id="Dropdownlist5" tabIndex="31" runat="server" Width="80px">
											<asp:ListItem Value="FOB ">FOB </asp:ListItem>
										</asp:dropdownlist>
										<asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea" id="Textbox18"
											tabIndex="14" runat="server" CssClass="textField" Width="123px" Enabled="False" MaxLength="100"
											ReadOnly="True">FREE ON BOARD</asp:textbox></TD>
								</TR>
							</table>
							<asp:button style="Z-INDEX: 0;MARGIN-TOP: 20px" id="btnV2" runat="server" CssClass="queryButton"
								CausesValidation="False" Text="V2" Enabled="true"></asp:button>
							<asp:button style="Z-INDEX: 0; MARGIN-TOP: 20px" id="btnV3" runat="server" CssClass="queryButton"
								CausesValidation="False" Text="V3" Enabled="true"></asp:button>
						</td>
					</tr>
				</TABLE>
			</div>
		</form>
	</body>
</HTML>
