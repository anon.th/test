<%@ Import Namespace="Cambro.Web.DbCombo" %>
<%@ Import Namespace="com.common.util" %>
<%@ Import Namespace="System.Configuration" %>
<%@ Register TagPrefix="DbCombo" Namespace="Cambro.Web.DbCombo" Assembly="Cambro.Web.DbCombo" %>
<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<%@ Page language="c#" Codebehind="AgentZoneRates.aspx.cs" AutoEventWireup="false" Inherits="com.ties.AgentZoneRates" smartNavigation="false" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>AgentZoneRates</title>
		<LINK rev="stylesheet" href="css/styles.css" rel="stylesheet">
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<!--#INCLUDE FILE="msFormValidations.inc"-->
		<script language="javascript" src="Scripts/settingScrollPosition.js"></script>
	</HEAD>
	<body onclick="GetScrollPosition();" onload="SetScrollPosition();" MS_POSITIONING="GridLayout">
		<form id="AgentZoneRates" method="post" runat="server">
			<asp:label id="lblTitle" style="Z-INDEX: 104; LEFT: 24px; POSITION: absolute; TOP: 8px" runat="server" CssClass="mainTitleSize" Height="32px" Width="336px">Agent Zone Rates by Weight</asp:label><dbcombo:dbcombo id=dbCmbAgentId style="Z-INDEX: 114; LEFT: 128px; POSITION: absolute; TOP: 112px" tabIndex=1 runat="server" Height="17px" Width="140px" RegistrationKey='<%#System.Configuration.ConfigurationSettings.AppSettings["DbComboRegKey"]%>' ReQueryOnLoad="True" ShowDbComboLink="False" TextUpLevelSearchButton="v" ServerMethod="AgentIdServerMethod" TextBoxColumns="20" AutoPostBack="True"></dbcombo:dbcombo><dbcombo:dbcombo id=dbCmbAgentName style="Z-INDEX: 113; LEFT: 488px; POSITION: absolute; TOP: 112px" tabIndex=2 runat="server" Height="17px" Width="176px" RegistrationKey='<%#System.Configuration.ConfigurationSettings.AppSettings["DbComboRegKey"]%>' ReQueryOnLoad="True" ShowDbComboLink="False" TextUpLevelSearchButton="v" ServerMethod="AgentNameServerMethod" TextBoxColumns="25" AutoPostBack="True"></dbcombo:dbcombo><asp:button id="btnGoToFirstPage" style="Z-INDEX: 112; LEFT: 600px; POSITION: absolute; TOP: 41px" tabIndex="5" runat="server" CssClass="queryButton" Width="24px" CausesValidation="False" Text="|<"></asp:button><asp:button id="btnPreviousPage" style="Z-INDEX: 105; LEFT: 624px; POSITION: absolute; TOP: 41px" tabIndex="6" runat="server" CssClass="queryButton" Width="24px" CausesValidation="False" Text="<"></asp:button><asp:textbox id="txtCountRec" style="Z-INDEX: 109; LEFT: 648px; POSITION: absolute; TOP: 41px" tabIndex="7" runat="server" Height="19px" Width="24px"></asp:textbox><asp:button id="btnNextPage" style="Z-INDEX: 106; LEFT: 672px; POSITION: absolute; TOP: 41px" tabIndex="8" runat="server" CssClass="queryButton" Width="24px" CausesValidation="False" Text=">"></asp:button><asp:button id="btnGoToLastPage" style="Z-INDEX: 108; LEFT: 696px; POSITION: absolute; TOP: 41px" tabIndex="9" runat="server" CssClass="queryButton" Width="24px" CausesValidation="False" Text=">|"></asp:button><asp:label id="lblAgentName" style="Z-INDEX: 110; LEFT: 384px; POSITION: absolute; TOP: 112px" runat="server" CssClass="tableLabel" Width="100px">Agent Name</asp:label><asp:label id="lblAgentCode" style="Z-INDEX: 107; LEFT: 24px; POSITION: absolute; TOP: 112px" runat="server" CssClass="tableLabel" Width="100px">Agent Code</asp:label><asp:button id="btnQuery" style="Z-INDEX: 101; LEFT: 22px; POSITION: absolute; TOP: 41px" runat="server" CssClass="queryButton" Width="62px" CausesValidation="False" Text="Query"></asp:button><asp:button id="btnExecuteQuery" style="Z-INDEX: 102; LEFT: 84px; POSITION: absolute; TOP: 41px" runat="server" CssClass="queryButton" Width="130px" CausesValidation="False" Text="Execute Query"></asp:button><asp:label id="lblErrorMessage" style="Z-INDEX: 103; LEFT: 24px; POSITION: absolute; TOP: 72px" runat="server" CssClass="errorMsgColor" Width="688px">Error Message</asp:label><asp:requiredfieldvalidator id="reqAgentId" Runat="server" BorderWidth="0" Display="None" ControlToValidate="dbCmbAgentId" ErrorMessage="Agent Id "></asp:requiredfieldvalidator><asp:validationsummary id="PageValidationSummary" style="Z-INDEX: 105; LEFT: 728px; POSITION: absolute; TOP: 152px" runat="server" ShowMessageBox="True" ShowSummary="False" DisplayMode="BulletList" HeaderText="Please enter the following mandatory field(s):<br><br>"></asp:validationsummary>
			<TABLE id="tblMain" style="Z-INDEX: 111; LEFT: 16px; WIDTH: 712px; POSITION: absolute; TOP: 144px; HEIGHT: 321px" width="712" runat="server">
				<TR>
					<TD style="HEIGHT: 297px" vAlign="top" colSpan="19"><asp:datagrid id="dgBZRates" runat="server" Width="704px" ItemStyle-Height="20" OnPageIndexChanged="dgBZRates_PageChange" OnUpdateCommand="dgBZRates_Update" OnDeleteCommand="dgBZRates_Delete" OnItemDataBound="dgBZRates_Bound" OnCancelCommand="dgBZRates_Cancel" OnEditCommand="dgBZRates_Edit" AutoGenerateColumns="False" AllowPaging="True" AllowCustomPaging="True" OnItemCommand="dgBZRates_Button">
							<ItemStyle Height="20px" CssClass="gridField"></ItemStyle>
							<Columns>
								<asp:EditCommandColumn ButtonType="LinkButton" UpdateText="&lt;IMG SRC='images/butt-update.gif' border=0 title='Update' &gt;" CancelText="&lt;IMG SRC='images/butt-cancel.gif' border=0 title='Cancel' &gt;" EditText="&lt;IMG SRC='images/butt-edit.gif' border=0 title='Edit' &gt;">
									<HeaderStyle CssClass="gridHeading" Width="5%"></HeaderStyle>
									<ItemStyle HorizontalAlign="Left"></ItemStyle>
								</asp:EditCommandColumn>
								<asp:ButtonColumn Text="&lt;IMG SRC='images/butt-delete.gif' border=0 title='Delete' &gt;" ButtonType="LinkButton" CommandName="Delete">
									<HeaderStyle CssClass="gridHeading" Width="3%"></HeaderStyle>
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
								</asp:ButtonColumn>
								<asp:TemplateColumn HeaderText="Origin Zone Code" HeaderStyle-Font-Bold="true">
									<HeaderStyle CssClass="gridHeading" Width="20%"></HeaderStyle>
									<ItemStyle CssClass="gridField"></ItemStyle>
									<ItemTemplate>
										<asp:Label CssClass="gridLabel" ID="lblOZCode" Text='<%#DataBinder.Eval(Container.DataItem,"origin_zone_code")%>' Runat="server">
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<cc1:mstextbox CssClass="gridTextBox" ID="txtOZCode" Text='<%#DataBinder.Eval(Container.DataItem,"origin_zone_code")%>' Runat="server" Enabled="True" TextMaskType="msUpperAlfaNumericWithUnderscore" MaxLength="12">
										</cc1:mstextbox>
										<asp:RequiredFieldValidator ID="ozcValidator" Runat="server" BorderWidth="0" Display="None" ControlToValidate="txtOZCode" ErrorMessage="Origin Zone Code "></asp:RequiredFieldValidator>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:ButtonColumn Text="&lt;IMG SRC='images/butt-search.gif' border=0 title='Search' &gt;" ButtonType="LinkButton" CommandName="OriginSearch" Visible="true">
									<HeaderStyle CssClass="gridHeading" Width="5%"></HeaderStyle>
									<ItemStyle CssClass="gridField"></ItemStyle>
								</asp:ButtonColumn>
								<asp:TemplateColumn HeaderText="Destination Zone Code" HeaderStyle-Font-Bold="true">
									<HeaderStyle CssClass="gridHeading" Width="20%"></HeaderStyle>
									<ItemStyle CssClass="gridField"></ItemStyle>
									<ItemTemplate>
										<asp:Label CssClass="gridLabel" ID="lblDZCode" Text='<%#DataBinder.Eval(Container.DataItem,"destination_zone_code")%>' Runat="server">
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<cc1:mstextbox CssClass="gridTextBox" ID="txtDZCode" Text='<%#DataBinder.Eval(Container.DataItem,"destination_zone_code")%>' Runat="server" Enabled="True" TextMaskType="msUpperAlfaNumericWithUnderscore" MaxLength="12">
										</cc1:mstextbox>
										<asp:RequiredFieldValidator ID="odcValidator" Runat="server" BorderWidth="0" Display="None" ControlToValidate="txtDZCode" ErrorMessage="Destination Zone Code "></asp:RequiredFieldValidator>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:ButtonColumn Text="&lt;IMG SRC='images/butt-search.gif' border=0 title='Search' &gt;" ButtonType="LinkButton" CommandName="DestinationSearch" Visible="true">
									<HeaderStyle CssClass="gridHeading" Width="5%"></HeaderStyle>
									<ItemStyle CssClass="gridField"></ItemStyle>
								</asp:ButtonColumn>
								<asp:TemplateColumn HeaderText="Start Wt Kg" HeaderStyle-Font-Bold="true">
									<HeaderStyle CssClass="gridHeading" Width="12%"></HeaderStyle>
									<ItemStyle CssClass="gridField"></ItemStyle>
									<ItemTemplate>
										<asp:Label CssClass="gridLabelNumber" ID="lblStartWt" Text='<%#DataBinder.Eval(Container.DataItem,"start_wt","{0:#0.0}")%>' Runat="server" Enabled="True">
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<cc1:mstextbox CssClass="gridTextBoxNumber" ID="txtStartWt" Text='<%#DataBinder.Eval(Container.DataItem,"start_wt")%>' Runat="server" MaxLength="6" TextMaskType="msNumeric" NumberMaxValue="9999" NumberMinValue="0" NumberPrecision="5" NumberScale="1">
										</cc1:mstextbox>
										<asp:RequiredFieldValidator ID="reqStartWt" Runat="server" BorderWidth="0" Display="None" ControlToValidate="txtStartWt" ErrorMessage="Start Weight "></asp:RequiredFieldValidator>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="End Wt Kg" HeaderStyle-Font-Bold="true">
									<HeaderStyle CssClass="gridHeading" Width="12%"></HeaderStyle>
									<ItemStyle CssClass="gridField"></ItemStyle>
									<ItemTemplate>
										<asp:Label CssClass="gridLabelNumber" ID="lblEndWt" Text='<%#DataBinder.Eval(Container.DataItem,"end_wt","{0:#0.0}")%>' Runat="server" Enabled="True">
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<cc1:mstextbox CssClass="gridTextBoxNumber" ID="txtEndWt" Text='<%#DataBinder.Eval(Container.DataItem,"end_wt")%>' Runat="server" MaxLength="6" TextMaskType="msNumeric" NumberMaxValue="9999" NumberMinValue="0" NumberPrecision="5" NumberScale="1">
										</cc1:mstextbox>
										<asp:RequiredFieldValidator ID="reqEndWt" Runat="server" BorderWidth="0" Display="None" ControlToValidate="txtEndWt" ErrorMessage="End Weight "></asp:RequiredFieldValidator>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Start Price">
									<HeaderStyle CssClass="gridHeading"></HeaderStyle>
									<ItemStyle CssClass="gridField" Width="15%"></ItemStyle>
									<ItemTemplate>
										<asp:Label CssClass="gridLabelNumber" ID="lblSPrice" Text='<%#DataBinder.Eval(Container.DataItem,"start_price","{0:n}")%>' Runat="server" Enabled="True">
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<cc1:mstextbox CssClass="gridTextBoxNumber" ID="txtSPrice" Text='<%#DataBinder.Eval(Container.DataItem,"start_price")%>' Runat="server" MaxLength="9" TextMaskType="msNumeric" NumberMaxValue="999999" NumberMinValue="0" NumberPrecision="8" NumberScale="2">
										</cc1:mstextbox>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Incremental Price/Kg">
									<HeaderStyle CssClass="gridHeading" Width="15%"></HeaderStyle>
									<ItemStyle CssClass="gridField"></ItemStyle>
									<ItemTemplate>
										<asp:Label CssClass="gridLabelNumber" ID="lblIPrice" Text='<%#DataBinder.Eval(Container.DataItem,"increment_price","{0:n}")%>' Runat="server" Enabled="True">
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<cc1:mstextbox CssClass="gridTextBoxNumber" ID="txtIPrice" Text='<%#DataBinder.Eval(Container.DataItem,"increment_price")%>' Runat="server" MaxLength="9" TextMaskType="msNumeric" NumberMaxValue="999999" NumberMinValue="0" NumberPrecision="8" NumberScale="2">
										</cc1:mstextbox>
									</EditItemTemplate>
								</asp:TemplateColumn>
							</Columns>
							<PagerStyle NextPageText="Next" PrevPageText="Previous" HorizontalAlign="Right" CssClass="normalText"></PagerStyle>
						</asp:datagrid></TD>
					<TD style="HEIGHT: 297px" vAlign="bottom" align="left" colSpan="1"></TD>
				</TR>
			</TABLE>
			<INPUT type=hidden value="<%=strScrollPosition%>" name="ScrollPosition">
			<asp:button id="btnInsert" style="Z-INDEX: 115; LEFT: 215px; POSITION: absolute; TOP: 41px" runat="server" CssClass="queryButton" Width="88px" CausesValidation="False" Text="Insert"></asp:button></form>
	</body>
</HTML>
