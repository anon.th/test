<%@ Page language="c#" Codebehind="InvoiceGeneration.aspx.cs" AutoEventWireup="false" Inherits="com.ties.InvoiceGeneration" SmartNavigation="false" %>
<%@ Import Namespace="Cambro.Web.DbCombo" %>
<%@ Register TagPrefix="dbCombo" Namespace="Cambro.Web.DbCombo" Assembly="Cambro.Web.DbCombo" %>
<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>InvoiceGeneration</title>
		<LINK rel="stylesheet" type="text/css" href="css/Styles.css" name="stylesheet">
		<META name="GENERATOR" content="Microsoft Visual Studio 7.0">
		<META name="CODE_LANGUAGE" content="C#">
		<META name="vs_defaultClientScript" content="JavaScript">
		<META name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5"> <!--#INCLUDE FILE="msFormValidations.inc"-->
		<SCRIPT language="javascript" src="Scripts/settingScrollPosition.js"></SCRIPT>
	</HEAD>
	<BODY onclick="GetScrollPosition();" onunload="window.top.displayBanner.fnCloseAll(0);"
		onload="SetScrollPosition();" MS_POSITIONING="GridLayout">
		<DIV style="Z-INDEX: 101; POSITION: absolute; WIDTH: 619px; HEIGHT: 421px; TOP: 7px; LEFT: 7px"
			id="divInvoiceGen" MS_POSITIONING="GridLayout" runat="server">
			<FORM id="InvoiceGeneration" method="post" runat="server">
				<asp:button style="Z-INDEX: 100; POSITION: absolute; TOP: 48px; LEFT: 99px" id="btnGenerate"
					runat="server" CssClass="queryButton" Text="Preview" Width="104px"></asp:button><asp:button style="Z-INDEX: 101; POSITION: absolute; TOP: 48px; LEFT: 24px" id="btnClear" runat="server"
					CssClass="queryButton" Text="Clear" Width="74px"></asp:button><asp:label style="Z-INDEX: 105; POSITION: absolute; TOP: 80px; LEFT: 19px" id="lblErrorMessage"
					runat="server" CssClass="errorMsgColor" Width="556px" Height="30px"></asp:label>&nbsp;
				<TABLE style="Z-INDEX: 103; POSITION: absolute; WIDTH: 484px; TOP: 115px; LEFT: 20px" id="tblShipmentExcepPODRepQry"
					border="0" width="484" runat="server">
					<TR width="100%">
						<TD vAlign="top" width="100%">
							<FIELDSET><LEGEND><asp:label id="Label2" CssClass="tableHeadingFieldset" Runat="server">Invoice Generation</asp:label></LEGEND>
								<TABLE style="TOP: 2px; LEFT: 1px" id="tblDates" border="0" width="100%" align="left" runat="server">
									<TR>
										<TD width="5%"></TD>
										<TD width="5%"></TD>
										<TD width="5%"></TD>
										<TD width="5%"></TD>
										<TD width="5%"></TD>
										<TD style="WIDTH: 42px" width="42"></TD>
										<TD width="5%"></TD>
										<TD width="5%"></TD>
										<TD width="5%"></TD>
										<TD width="5%"></TD>
										<TD width="5%"></TD>
										<TD width="5%"></TD>
										<TD width="5%"></TD>
										<TD width="5%"></TD>
										<TD width="5%"></TD>
										<TD width="5%"></TD>
										<TD width="5%"></TD>
										<TD width="5%"></TD>
										<TD width="5%"></TD>
										<TD width="5%"></TD>
									</TR>
									<TR height="27">
										<TD style="WIDTH: 100%" colSpan="10"><asp:radiobutton id="rdtInvoiceConsig" runat="server" CssClass="tableRadioButton" Text="Invoice by Consignment"
												Width="100%" Height="22px" AutoPostBack="True" GroupName="InvoiceGen" Checked="True"></asp:radiobutton></TD>
										<TD style="WIDTH: 100%" colSpan="10"><asp:radiobutton id="rbtInvoiceCustomer" runat="server" Enabled="False" CssClass="tableRadioButton"
												Text="Invoice by Customer" Width="100%" Height="22px" AutoPostBack="True" GroupName="InvoiceGen"></asp:radiobutton></TD>
									</TR>
									<tr>
										<td colSpan="3"></td>
										<td colSpan="17"><asp:radiobutton id="rbtDomesticCons" runat="server" CssClass="tableRadioButton" Text="Domestic Consignments"
												Width="100%" Height="22px" AutoPostBack="True" GroupName="ConsignmentType" Checked="True" Visible="True"></asp:radiobutton></td>
									</tr>
									<tr>
										<td colSpan="3"></td>
										<td colSpan="17"><asp:radiobutton id="rbtExports" runat="server" CssClass="tableRadioButton" Text="Exports" Width="100%"
												Height="22px" AutoPostBack="True" GroupName="ConsignmentType" Visible="True"></asp:radiobutton></td>
									</tr>
									<tr>
										<td colSpan="3"></td>
										<td colSpan="17"><asp:radiobutton id="rbtImports" runat="server" CssClass="tableRadioButton" Text="Imports/On-forwarding"
												Width="100%" Height="22px" AutoPostBack="True" GroupName="ConsignmentType" Visible="True"></asp:radiobutton></td>
									</tr>
									<tr>
										<td colSpan="5"></td>
										<td colSpan="2">&nbsp;&nbsp;&nbsp;<asp:label id="lbPayerAccount" runat="server" CssClass="tablelabel" Width="91px">Payer Account</asp:label></td>
										<td colSpan="10">
											<%--<dbcombo:dbcombo id="dbcPayerAccount" tabIndex="1" runat="server" Width="210px" Height="17px" AutoPostBack="True"
												ReQueryOnLoad="True" ShowDbComboLink="False" TextUpLevelSearchButton="v" ServerMethod="PayerIdServerMethod"
												TextBoxColumns="20"></dbcombo:dbcombo>--%>
											<asp:DropDownList id="dbcPayerAccount" tabIndex="1" runat="server" Width="210px" Height="17px" AutoPostBack="True"></asp:DropDownList>
										</td>
									</tr>
									<tr>
										<td colSpan="5"></td>
										<td colSpan="2">&nbsp;&nbsp;&nbsp;<asp:label id="lbPayerName" runat="server" CssClass="tablelabel" Width="91px">Payer Name</asp:label></td>
										<td colSpan="10"><cc1:mstextbox id="txtPayerName" runat="server" CssClass="textField" Width="100%" MaxLength="100"
												TextMaskType="msUpperAlfaNumericWithUnderscore" Enabled="False"></cc1:mstextbox></td>
									</tr>
									<tr>
										<td colSpan="3"></td>
										<td colSpan="3"><asp:label id="lbManifested" runat="server" CssClass="tablelabel" Width="100%">Manifested Date(s) From</asp:label></td>
										<td colSpan="3"><cc1:mstextbox id="txtManFrom" runat="server" CssClass="textField" Width="100%" MaxLength="10"
												TextMaskType="msDate" TextMaskString="99/99/9999" AutoPostBack="True"></cc1:mstextbox></td>
										<td colSpan="2"><asp:label id="lbManTo" runat="server" CssClass="tablelabel" Width="100%">To</asp:label></td>
										<td colSpan="3"><cc1:mstextbox id="txtManTo" runat="server" CssClass="textField" Width="100%" MaxLength="10" TextMaskType="msDate"
												TextMaskString="99/99/9999"></cc1:mstextbox></td>
									</tr>
									<tr>
										<td colSpan="3"></td>
										<td colSpan="3"><asp:label id="lbInvoiNumIs" runat="server" CssClass="tablelabel" Width="100%">Invoice Number is:</asp:label></td>
										<td colSpan="6">
											<asp:radiobutton id="rbtConsignmentNum" runat="server" CssClass="tableRadioButton" Text="Consignment Number"
												Width="150px" Height="22px" AutoPostBack="True" GroupName="InvoiceFor" Checked="True"></asp:radiobutton>
										</td>
										<td colSpan="5">
											<asp:radiobutton id="rbtAutoGen" runat="server" CssClass="tableRadioButton" Text="Auto-generated"
												Width="100%" Height="22px" AutoPostBack="True" GroupName="InvoiceFor"></asp:radiobutton>
										</td>
										<%--<td colSpan="4"><asp:radiobutton id="rbtAutoGen" runat="server" CssClass="tableRadioButton" Text="Auto-generated"
												Width="100%" Height="22px" AutoPostBack="True" GroupName="InvoiceFor"></asp:radiobutton></td>--%>
									</tr>
									<TR>
										<TD style="WIDTH: 124px" colSpan="6"><asp:label id="lblInvoiceFor" runat="server" CssClass="tablelabel" Width="91px">Payment Mode</asp:label></TD>
										<TD colSpan="8"><asp:radiobutton id="rbCashConsig" runat="server" CssClass="tableRadioButton" Text="Cash Consignments"
												Width="100%" Height="22px" AutoPostBack="True" GroupName="InvoiceFor" Checked="True"></asp:radiobutton></TD>
										<TD colSpan="4"></TD>
										<TD colSpan="12"></TD>
									</TR>
									<TR>
										<TD style="WIDTH: 124px" colSpan="6"></TD>
										<TD colSpan="16"><FONT face="Tahoma">
												<P style="MARGIN-RIGHT: 0px" dir="ltr">&nbsp;&nbsp;&nbsp;
													<asp:checkbox id="chkShipByCash" runat="server" CssClass="tableRadioButton" Text="Shipped by Cash Customers"
														Width="254px" AutoPostBack="True"></asp:checkbox></P>
											</FONT>
										</TD>
									</TR>
									<TR>
										<TD style="WIDTH: 124px" colSpan="6"></TD>
										<TD colSpan="16"><FONT face="Tahoma">&nbsp;&nbsp;&nbsp;
												<asp:checkbox id="chkShipByCredit" runat="server" CssClass="tableRadioButton" Text="Shipped by Credit Customers"
													Width="250px"></asp:checkbox></FONT></TD>
									</TR>
									<TR>
										<TD style="WIDTH: 124px" colSpan="6"></TD>
										<TD colSpan="16"><asp:radiobutton id="rbCreditConsig" runat="server" CssClass="tableRadioButton" Text="Credit Consignments"
												Width="316px" Height="22px" AutoPostBack="True" GroupName="InvoiceFor"></asp:radiobutton></TD>
									</TR>
									<TR>
										<TD style="WIDTH: 124px" colSpan="6"><asp:label id="lblCustomerCode" runat="server" CssClass="tableLabel" Width="100%" Height="22px">Customer Code</asp:label></TD>
										<TD width="210" colSpan="8"><dbcombo:dbcombo id="dbCmbAgentId" tabIndex="1" runat="server" Width="210px" Height="17px" AutoPostBack="True"
												ReQueryOnLoad="True" ShowDbComboLink="False" TextUpLevelSearchButton="v" ServerMethod="AgentIdServerMethod" TextBoxColumns="20"></dbcombo:dbcombo><dbcombo:dbcombo id="dbCmbAgentId_Cash" tabIndex="1" runat="server" Width="210px" Height="17px" AutoPostBack="True"
												ReQueryOnLoad="True" ShowDbComboLink="False" TextUpLevelSearchButton="v" ServerMethod="AgentIdCashServerMethod" TextBoxColumns="20"></dbcombo:dbcombo></TD>
										<TD></TD>
										<TD colSpan="5"></TD>
									</TR>
									<TR>
										<TD style="WIDTH: 124px" colSpan="6"><asp:label id="lblCustomerName" runat="server" CssClass="tableLabel" Width="100%" Height="22px">Customer Name</asp:label></TD>
										<TD colSpan="14"><cc1:mstextbox id="txtCustomerName" runat="server" CssClass="textField" Width="100%" MaxLength="100"
												TextMaskType="msUpperAlfaNumericWithUnderscore"></cc1:mstextbox></TD>
									</TR>
									<TR>
										<TD style="WIDTH: 124px" colSpan="6"><asp:label id="lblInvoiceToDate" runat="server" CssClass="tableLabel" Width="100%" Height="22px">Last Pickup Date</asp:label></TD>
										<TD colSpan="6"><cc1:mstextbox id="txtLastPickupDate" runat="server" CssClass="textField" Width="100%" MaxLength="10"
												TextMaskType="msDate" TextMaskString="99/99/9999"></cc1:mstextbox></TD>
										<TD colSpan="13"></TD>
									</TR>
									<TR>
										<TD style="WIDTH: 124px" colSpan="6"></TD>
										<TD colSpan="2"></TD>
										<TD colSpan="5"></TD>
										<TD colSpan="2" align="right"></TD>
										<TD colSpan="5"></TD>
									</TR>
									<TR>
										<TD colSpan="20"></TD>
									</TR>
								</TABLE>
							</FIELDSET>
						</TD>
					</TR>
				</TABLE>
				<asp:label style="Z-INDEX: 102; POSITION: absolute; TOP: 9px; LEFT: 20px" id="lblTitle" runat="server"
					CssClass="maintitleSize" Width="558px" Height="32px">Invoice Generation</asp:label><input 
value="<%=strScrollPosition%>" type=hidden name=ScrollPosition>
			</FORM>
		</DIV>
		<DIV style="Z-INDEX: 102; POSITION: absolute; WIDTH: 1139px; HEIGHT: 600px; TOP: 7px; LEFT: 24px"
			id="divInvoiceGenPreview" MS_POSITIONING="GridLayout" runat="server">
			<form id="Form1" method="post" runat="server">
				<FONT face="Tahoma">
					<asp:label style="Z-INDEX: 101; POSITION: absolute; TOP: 8px; LEFT: 8px" id="lblMainTitle"
						runat="server" CssClass="mainTitleSize" Width="477px" Height="26px">Invoice Generation Preview</asp:label><asp:datagrid style="Z-INDEX: 115; POSITION: absolute; TOP: 291px; LEFT: 5px" id="dgPreview" runat="server"
						Width="890px" Height="95px" SelectedItemStyle-CssClass="gridFieldSelected" AutoGenerateColumns="False" ItemStyle-Height="20" OnUpdateCommand="dgPreview_SaveCust">
						<SelectedItemStyle CssClass="gridFieldSelected"></SelectedItemStyle>
						<ItemStyle Height="10px" CssClass="gridField"></ItemStyle>
						<Columns>
							<asp:TemplateColumn>
								<HeaderStyle Width="20px" CssClass="gridBlueHeading"></HeaderStyle>
								<ItemTemplate>
									<asp:CheckBox Checked='<%#DataBinder.Eval(Container.DataItem,"CHECKED").ToString().Equals("Y")? true : false %>' OnCheckedChanged="dgPreview_CheckChanged" ID="chkSelect" Runat="server" AutoPostBack="true">
									</asp:CheckBox>
								</ItemTemplate>
							</asp:TemplateColumn>
							<asp:BoundColumn DataField="consignment_no" HeaderText="Consignment No.">
								<HeaderStyle HorizontalAlign="Center" CssClass="gridBlueHeading"></HeaderStyle>
								<ItemStyle CssClass="gridField"></ItemStyle>
							</asp:BoundColumn>
							<asp:BoundColumn DataField="ref_no" HeaderText="Ref No.">
								<HeaderStyle HorizontalAlign="Center" CssClass="gridBlueHeading"></HeaderStyle>
								<ItemStyle CssClass="gridField"></ItemStyle>
							</asp:BoundColumn>
							<asp:TemplateColumn HeaderText="Customer/ Sender">
								<HeaderStyle HorizontalAlign="Center" Width="230px" CssClass="gridBlueHeading"></HeaderStyle>
								<ItemStyle Wrap="False" CssClass="gridField" VerticalAlign="Middle"></ItemStyle>
								<ItemTemplate>
									<asp:DropDownList Width="200px" CssClass="gridDropDown" ID="ddlSCShipmentUpdate" Runat="server"></asp:DropDownList>
									<asp:ImageButton CommandName="Update" id="imgSave" runat="server" ImageUrl="images/butt-update.gif"></asp:ImageButton>
									<asp:Label ID="dgLabelSenderName" visible=False runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Sender_Name")%>' />
								</ItemTemplate>
							</asp:TemplateColumn>
							<asp:BoundColumn DataField="sender_zipcode" HeaderText="Origin">
								<HeaderStyle HorizontalAlign="Center" CssClass="gridBlueHeading"></HeaderStyle>
								<ItemStyle HorizontalAlign="Left" CssClass="gridField"></ItemStyle>
							</asp:BoundColumn>
							<asp:BoundColumn DataField="act_pickup_datetime" HeaderText="Pickup" DataFormatString="{0:dd/MM/yyyy HH:mm}">
								<HeaderStyle HorizontalAlign="Center" CssClass="gridBlueHeading"></HeaderStyle>
								<ItemStyle HorizontalAlign="Left" CssClass="gridField"></ItemStyle>
							</asp:BoundColumn>
							<asp:BoundColumn DataField="recipient_name" HeaderText="Recipient">
								<HeaderStyle HorizontalAlign="Center" CssClass="gridBlueHeading"></HeaderStyle>
								<ItemStyle HorizontalAlign="Left" CssClass="gridField"></ItemStyle>
							</asp:BoundColumn>
							<asp:BoundColumn DataField="recipient_zipcode" HeaderText="Dest.">
								<HeaderStyle HorizontalAlign="Center" CssClass="gridBlueHeading"></HeaderStyle>
								<ItemStyle HorizontalAlign="Left" CssClass="gridField"></ItemStyle>
							</asp:BoundColumn>
							<asp:BoundColumn DataField="act_delivery_date" HeaderText="Del. D/T" DataFormatString="{0:dd/MM/yyyy HH:mm}">
								<HeaderStyle HorizontalAlign="Center" CssClass="gridBlueHeading"></HeaderStyle>
								<ItemStyle HorizontalAlign="Left" CssClass="gridField"></ItemStyle>
							</asp:BoundColumn>
							<asp:BoundColumn DataField="service_code" HeaderText="Service">
								<HeaderStyle HorizontalAlign="Center" CssClass="gridBlueHeading"></HeaderStyle>
								<ItemStyle HorizontalAlign="Center" CssClass="gridField"></ItemStyle>
							</asp:BoundColumn>
							<asp:BoundColumn DataField="tot_freight_charge" HeaderText="Freight" DataFormatString="{0:#,##0.00}">
								<HeaderStyle HorizontalAlign="Center" CssClass="gridBlueHeading"></HeaderStyle>
								<ItemStyle HorizontalAlign="Right" CssClass="gridField"></ItemStyle>
							</asp:BoundColumn>
							<asp:BoundColumn DataField="insurance_surcharge" HeaderText="Insurance" DataFormatString="{0:#,##0.00}">
								<HeaderStyle HorizontalAlign="Center" CssClass="gridBlueHeading"></HeaderStyle>
								<ItemStyle HorizontalAlign="Right" CssClass="gridField"></ItemStyle>
							</asp:BoundColumn>
							<asp:BoundColumn DataField="other_surch_amount" HeaderText="Other" DataFormatString="{0:#,##0.00}">
								<HeaderStyle HorizontalAlign="Center" CssClass="gridBlueHeading"></HeaderStyle>
								<ItemStyle HorizontalAlign="Right" CssClass="gridField"></ItemStyle>
							</asp:BoundColumn>
							<asp:BoundColumn DataField="tot_vas_surcharge" HeaderText="VAS" DataFormatString="{0:#,##0.00}">
								<HeaderStyle HorizontalAlign="Center" CssClass="gridBlueHeading"></HeaderStyle>
								<ItemStyle HorizontalAlign="Right" CssClass="gridField"></ItemStyle>
							</asp:BoundColumn>
							<asp:BoundColumn DataField="esa_surcharge" HeaderText="ESA" DataFormatString="{0:#,##0.00}">
								<HeaderStyle HorizontalAlign="Center" CssClass="gridBlueHeading"></HeaderStyle>
								<ItemStyle HorizontalAlign="Right" CssClass="gridField"></ItemStyle>
							</asp:BoundColumn>
							<asp:BoundColumn DataField="last_exception_code" HeaderText="PODEX Code">
								<HeaderStyle HorizontalAlign="Center" CssClass="gridBlueHeading"></HeaderStyle>
								<ItemStyle HorizontalAlign="Center" CssClass="gridField"></ItemStyle>
							</asp:BoundColumn>
						</Columns>
						<PagerStyle NextPageText="Next" PrevPageText="Previous" HorizontalAlign="Left" CssClass="normalText"></PagerStyle>
					</asp:datagrid><asp:datagrid style="Z-INDEX: 115; POSITION: absolute; TOP: 291px; LEFT: 5px" id="dgPreview2"
						runat="server" Width="950px" Height="95px" SelectedItemStyle-CssClass="gridFieldSelected" AutoGenerateColumns="False"
						ItemStyle-Height="20" OnUpdateCommand="dgPreview2_Edit" OnCancelCommand="dgPreview2_Cancel" OnEditCommand="dgPreview2_SaveCust">
						<SelectedItemStyle CssClass="gridFieldSelected"></SelectedItemStyle>
						<ItemStyle Height="10px" CssClass="gridField"></ItemStyle>
						<Columns>
							<asp:TemplateColumn>
								<HeaderStyle Width="180px" CssClass="gridBlueHeading"></HeaderStyle>
								<ItemTemplate>
									<asp:CheckBox Checked='<%#DataBinder.Eval(Container.DataItem,"CHECKED").ToString().Equals("Y")? true : false %>' OnCheckedChanged="dgPreview2_CheckChanged" ID="Checkbox1" Runat="server" AutoPostBack="true">
									</asp:CheckBox>
									<asp:ImageButton CommandName="Update" id="imbtEdit" runat="server" ImageUrl="images/butt-edit.gif"></asp:ImageButton>
									<asp:ImageButton CommandName="Edit" id="imbUpdate" Visible="False" runat="server" ImageUrl="images/butt-update.gif"></asp:ImageButton>
									<asp:ImageButton CommandName="Cancel" id="imbtCancel" Visible="False" runat="server" ImageUrl="images/butt-cancel.gif"></asp:ImageButton>
								</ItemTemplate>
							</asp:TemplateColumn>
							<asp:TemplateColumn HeaderText="Consignment/Invoice No.">
								<HeaderStyle HorizontalAlign="Center" Width="190px" CssClass="gridBlueHeading"></HeaderStyle>
								<ItemStyle Wrap="False" CssClass="gridField" VerticalAlign="Middle"></ItemStyle>
								<ItemTemplate>
									<%--<asp:ImageButton CommandName="Update" id="Imagebutton2" runat="server" ImageUrl="images/butt-update.gif"></asp:ImageButton>--%>
									<asp:Label ID="lbConsig" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"consignment_no")%>' />
									<asp:TextBox ID="txtConsig2" Visible ="False" CssClass="textField" Enabled="False" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"consignment_no")%>' />
									<br>
									&nbsp;&nbsp;&nbsp;&nbsp;
									<asp:Label ID="lbInvoicNo" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"invoice_no")%>' />
									<asp:TextBox ID="txtConsig" Visible ="False" CssClass="textField" Enabled="False" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"invoice_no")%>' />
								</ItemTemplate>
							</asp:TemplateColumn>
							<asp:TemplateColumn HeaderText="MAWB Number">
								<HeaderStyle HorizontalAlign="Center" Width="190px" CssClass="gridBlueHeading"></HeaderStyle>
								<ItemStyle Wrap="False" CssClass="gridField" VerticalAlign="Middle"></ItemStyle>
								<ItemTemplate>
									<asp:TextBox ID="txtAWBNumber" CssClass="textField" Enabled="False" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"MasterAWBNumber")%>' />
								</ItemTemplate>
							</asp:TemplateColumn>
							<asp:TemplateColumn HeaderText="Intl. Charge">
								<HeaderStyle HorizontalAlign="Center" Width="190px" CssClass="gridBlueHeading"></HeaderStyle>
								<ItemStyle Wrap="False" CssClass="gridField" VerticalAlign="Middle"></ItemStyle>
								<ItemTemplate>
									<cc1:mstextbox style="Z-INDEX: 0" id="txtIntlCharge" tabIndex="20" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"intl_charge", "{0:#,##0.00}")%>' CssClass="textFieldRightAlign" Width="80px" Enabled="False" MaxLength="9" TextMaskType="msNumeric" NumberScale="2" NumberPrecision="8" NumberMinValue="0" NumberMaxValue="99999999">
									</cc1:mstextbox>
								</ItemTemplate>
							</asp:TemplateColumn>
							<%--<asp:BoundColumn DataField="MasterAWBNumber" HeaderText="MAWB Number">
								<HeaderStyle HorizontalAlign="Center" CssClass="gridBlueHeading"></HeaderStyle>
								<ItemStyle HorizontalAlign="Left" CssClass="gridField"></ItemStyle>
							</asp:BoundColumn>
							<asp:BoundColumn DataField="intl_charge" HeaderText="Intl. Charge">
								<HeaderStyle HorizontalAlign="Center" CssClass="gridBlueHeading"></HeaderStyle>
								<ItemStyle HorizontalAlign="Left" CssClass="gridField"></ItemStyle>
							</asp:BoundColumn>--%>
							<asp:BoundColumn DataField="ref_no" HeaderText="Ref No.">
								<HeaderStyle HorizontalAlign="Center" Width="100px" CssClass="gridBlueHeading"></HeaderStyle>
								<ItemStyle CssClass="gridField"></ItemStyle>
							</asp:BoundColumn>
							<asp:TemplateColumn HeaderText="Customer ID/ Customer Name">
								<HeaderStyle HorizontalAlign="Center" Width="230px" CssClass="gridBlueHeading"></HeaderStyle>
								<ItemStyle Wrap="False" CssClass="gridField" VerticalAlign="Middle"></ItemStyle>
								<ItemTemplate>
									<asp:Label ID="Label11" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Sender_Name")%>' />
									<asp:DropDownList Visible="False" Width="200px" CssClass="gridDropDown" ID="Dropdownlist1" Runat="server"></asp:DropDownList>
									<%--<asp:ImageButton CommandName="Update" id="Imagebutton1" runat="server" ImageUrl="images/butt-update.gif"></asp:ImageButton>--%>
									<asp:Label ID="Label10" visible=False runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Sender_Name")%>' />
								</ItemTemplate>
							</asp:TemplateColumn>
							<asp:BoundColumn DataField="sender_zipcode" HeaderText="Origin">
								<HeaderStyle HorizontalAlign="Center" CssClass="gridBlueHeading"></HeaderStyle>
								<ItemStyle HorizontalAlign="Left" CssClass="gridField"></ItemStyle>
							</asp:BoundColumn>
							<asp:BoundColumn DataField="shpt_manifest_datetime" HeaderText="Manisfested D/T" DataFormatString="{0:dd/MM/yyyy HH:mm}">
								<HeaderStyle HorizontalAlign="Center" CssClass="gridBlueHeading"></HeaderStyle>
								<ItemStyle HorizontalAlign="Left" CssClass="gridField"></ItemStyle>
							</asp:BoundColumn>
							<asp:BoundColumn DataField="recipient_name" HeaderText="Recipient">
								<HeaderStyle HorizontalAlign="Center" CssClass="gridBlueHeading"></HeaderStyle>
								<ItemStyle HorizontalAlign="Left" CssClass="gridField"></ItemStyle>
							</asp:BoundColumn>
							<asp:BoundColumn DataField="recipient_zipcode" HeaderText="Dest.">
								<HeaderStyle HorizontalAlign="Center" CssClass="gridBlueHeading"></HeaderStyle>
								<ItemStyle HorizontalAlign="Left" CssClass="gridField"></ItemStyle>
							</asp:BoundColumn>
							<asp:BoundColumn DataField="act_delivery_date" HeaderText="Del. D/T" DataFormatString="{0:dd/MM/yyyy HH:mm}">
								<HeaderStyle HorizontalAlign="Center" CssClass="gridBlueHeading"></HeaderStyle>
								<ItemStyle HorizontalAlign="Left" CssClass="gridField"></ItemStyle>
							</asp:BoundColumn>
							<asp:BoundColumn DataField="service_code" HeaderText="Service">
								<HeaderStyle HorizontalAlign="Center" CssClass="gridBlueHeading"></HeaderStyle>
								<ItemStyle HorizontalAlign="Center" CssClass="gridField"></ItemStyle>
							</asp:BoundColumn>
							<asp:BoundColumn DataField="basic_charge" HeaderText="Basic" DataFormatString="{0:#,##0.00}">
								<HeaderStyle HorizontalAlign="Center" CssClass="gridBlueHeading"></HeaderStyle>
								<ItemStyle HorizontalAlign="Right" CssClass="gridField"></ItemStyle>
							</asp:BoundColumn>
							<asp:BoundColumn DataField="tot_freight_charge" HeaderText="Freight" DataFormatString="{0:#,##0.00}">
								<HeaderStyle HorizontalAlign="Center" CssClass="gridBlueHeading"></HeaderStyle>
								<ItemStyle HorizontalAlign="Right" CssClass="gridField"></ItemStyle>
							</asp:BoundColumn>
							<asp:BoundColumn DataField="insurance_surcharge" HeaderText="Insurance" DataFormatString="{0:#,##0.00}">
								<HeaderStyle HorizontalAlign="Center" CssClass="gridBlueHeading"></HeaderStyle>
								<ItemStyle HorizontalAlign="Right" CssClass="gridField"></ItemStyle>
							</asp:BoundColumn>
							<asp:BoundColumn DataField="other_surch_amount" HeaderText="Surcharges" DataFormatString="{0:#,##0.00}">
								<HeaderStyle HorizontalAlign="Center" CssClass="gridBlueHeading"></HeaderStyle>
								<ItemStyle HorizontalAlign="Right" CssClass="gridField"></ItemStyle>
							</asp:BoundColumn>
							<asp:BoundColumn DataField="tot_vas_surcharge" HeaderText="VAS" DataFormatString="{0:#,##0.00}">
								<HeaderStyle HorizontalAlign="Center" CssClass="gridBlueHeading"></HeaderStyle>
								<ItemStyle HorizontalAlign="Right" CssClass="gridField"></ItemStyle>
							</asp:BoundColumn>
							<asp:BoundColumn DataField="esa_surcharge" HeaderText="ESA" DataFormatString="{0:#,##0.00}">
								<HeaderStyle HorizontalAlign="Center" CssClass="gridBlueHeading"></HeaderStyle>
								<ItemStyle HorizontalAlign="Right" CssClass="gridField"></ItemStyle>
							</asp:BoundColumn>
							<asp:BoundColumn DataField="tax_on_rated_amount" HeaderText="GST" DataFormatString="{0:#,##0.00}">
								<HeaderStyle HorizontalAlign="Center" CssClass="gridBlueHeading"></HeaderStyle>
								<ItemStyle HorizontalAlign="Right" CssClass="gridField"></ItemStyle>
							</asp:BoundColumn>
							<asp:BoundColumn DataField="last_exception_code" HeaderText="PODEX Code">
								<HeaderStyle HorizontalAlign="Center" CssClass="gridBlueHeading"></HeaderStyle>
								<ItemStyle HorizontalAlign="Center" CssClass="gridField"></ItemStyle>
							</asp:BoundColumn>
							<asp:BoundColumn DataField="total_rated_amount" HeaderText="Total Invoice Amount" DataFormatString="{0:#,##0.00}">
								<HeaderStyle HorizontalAlign="Center" CssClass="gridBlueHeading"></HeaderStyle>
								<ItemStyle HorizontalAlign="Right" CssClass="gridField"></ItemStyle>
							</asp:BoundColumn>
							<asp:TemplateColumn Visible="False" HeaderText="IsInvoiceDuplicated">
								<HeaderStyle HorizontalAlign="Center" Width="230px" CssClass="gridBlueHeading"></HeaderStyle>
								<ItemStyle Wrap="False" CssClass="gridField" VerticalAlign="Middle"></ItemStyle>
								<ItemTemplate>
									<asp:Label ID="lbIsInvoiceDuplicated" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"IsInvoiceDuplicated")%>' />
								</ItemTemplate>
							</asp:TemplateColumn>
						</Columns>
						<PagerStyle NextPageText="Next" PrevPageText="Previous" HorizontalAlign="Left" CssClass="normalText"></PagerStyle>
					</asp:datagrid>
					<TABLE style="Z-INDEX: 114; POSITION: absolute; WIDTH: 780px; HEIGHT: 139px; TOP: 95px; LEFT: 7px"
						id="tbDomestic" border="0" cellSpacing="1" width="727" align="left" runat="server">
						<TR height="25">
							<TD style="WIDTH: 96px" bgColor="#008499" vAlign="middle" width="96" align="left">&nbsp;
								<asp:label id="Label4" runat="server" Width="88px" Height="15px" ForeColor="White" BackColor="#008499"
									Font-Size="11px">Invoice Type</asp:label></TD>
							<TD style="WIDTH: 9px" vAlign="top" width="9"></TD>
							<TD style="WIDTH: 1001px" vAlign="middle" width="1001"><asp:label id="lbConsInvoiceType" runat="server" CssClass="tableLabel" Width="143px"></asp:label></TD>
							<TD style="WIDTH: 100px" width="100" align="left">&nbsp;</TD>
							<TD style="WIDTH: 8px" width="8"></TD>
							<TD style="WIDTH: 61px" width="61"></TD>
						</TR>
						<TR height="25">
							<TD style="WIDTH: 96px" bgColor="#008499" vAlign="middle" width="96" align="left">&nbsp;
								<asp:label id="Label7" runat="server" Width="88px" Height="15px" ForeColor="White" BackColor="#008499"
									Font-Size="11px">Manifested From</asp:label></TD>
							<TD style="WIDTH: 9px" vAlign="top" width="9"></TD>
							<TD style="WIDTH: 1001px" vAlign="middle" width="1001"><asp:label id="lbConsManifestedFrom" runat="server" CssClass="tableLabel" Width="143px"></asp:label></TD>
							<TD style="WIDTH: 100px" width="100" align="left">&nbsp;</TD>
							<TD style="WIDTH: 96px" bgColor="#008499" vAlign="middle" width="96" align="left">&nbsp;
								<asp:label id="Label8" runat="server" Width="88px" Height="15px" ForeColor="White" BackColor="#008499"
									Font-Size="11px">Payer ID</asp:label></TD>
							<TD style="WIDTH: 61px" width="61"><asp:label id="lbPayerIDDis" runat="server" Font-Size="11px"></asp:label></TD>
						</TR>
						<TR height="25">
							<TD style="WIDTH: 96px" bgColor="#008499" vAlign="middle" width="96" align="left">&nbsp;
								<asp:label id="lbConsManifestedToDis" runat="server" Width="88px" Height="15px" ForeColor="White"
									BackColor="#008499" Font-Size="11px">Manifested To</asp:label></TD>
							<TD style="WIDTH: 9px" vAlign="top" width="9"></TD>
							<TD style="WIDTH: 1001px" vAlign="middle" width="1001"><asp:label id="lbConsManifestedTo" runat="server" CssClass="tableLabel" Width="143px"></asp:label></TD>
							<TD style="WIDTH: 100px" width="100" align="left">&nbsp;</TD>
							<TD style="WIDTH: 96px" bgColor="#008499" vAlign="middle" width="96" align="left">&nbsp;
								<asp:label id="Label9" runat="server" Width="88px" Height="15px" ForeColor="White" BackColor="#008499"
									Font-Size="11px">Payer Name</asp:label></TD>
							<TD style="WIDTH: 500px" width="500"><asp:label id="lbPayerNameDis" runat="server" Width="100%" Font-Size="11px"></asp:label></TD>
						</TR>
					</TABLE>
					<TABLE style="Z-INDEX: 114; POSITION: absolute; WIDTH: 727px; HEIGHT: 139px; TOP: 95px; LEFT: 7px"
						id="Table10" border="0" cellSpacing="1" width="727" align="left" runat="server">
						<TR height="25">
							<TD style="WIDTH: 96px" bgColor="#008499" vAlign="middle" width="96" align="left">&nbsp;
								<asp:label id="Label5" runat="server" Width="88px" Height="15px" ForeColor="White" BackColor="#008499"
									Font-Size="11px">Invoice Type</asp:label></TD>
							<TD style="WIDTH: 9px" vAlign="top" width="9"></TD>
							<TD style="WIDTH: 1001px" vAlign="middle" width="1001"><asp:label id="lblInvoiceType" runat="server" CssClass="tableLabel" Width="143px"></asp:label></TD>
							<TD style="WIDTH: 148px" width="148" align="left">&nbsp;</TD>
							<TD style="WIDTH: 8px" width="8"></TD>
							<TD style="WIDTH: 61px" width="61"></TD>
						</TR>
						<TR height="25">
							<TD style="WIDTH: 96px" bgColor="#008499" vAlign="middle" width="96" align="left">&nbsp;
								<asp:label id="Label50" runat="server" CssClass="tableLabel" Width="88px" Height="16px" ForeColor="White"
									BackColor="#008499" Font-Size="11px">Customer ID</asp:label></TD>
							<TD style="WIDTH: 9px" vAlign="top" width="9"></TD>
							<TD style="WIDTH: 1001px" vAlign="middle" width="1001"><asp:label id="lblCustID" runat="server" CssClass="tableLabel" Width="143px"></asp:label></TD>
							<TD style="WIDTH: 148px" bgColor="#008499" width="148" align="left">&nbsp;
								<asp:label id="Label6" runat="server" CssClass="tableLabel" Width="81px" Height="15px" ForeColor="White"
									BackColor="#008499" Font-Size="11px"> Province</asp:label></TD>
							<TD style="WIDTH: 8px" width="8"></TD>
							<TD style="WIDTH: 61px" width="61"><asp:label id="lblProvince" runat="server" CssClass="tableLabel" Width="133px"></asp:label></TD>
						</TR>
						<TR height="25">
							<TD style="WIDTH: 96px; HEIGHT: 25px" bgColor="#008499" width="96" align="left">&nbsp;
								<asp:label id="Label48" runat="server" CssClass="tableLabel" Width="96px" Height="15px" ForeColor="White"
									BackColor="#008499" Font-Size="11px">Customer Name</asp:label></TD>
							<TD style="WIDTH: 9px; HEIGHT: 25px" width="9"></TD>
							<TD style="WIDTH: 1001px; HEIGHT: 25px" vAlign="middle" width="1001"><asp:label id="lblCustName" runat="server" CssClass="tableLabel" Width="260px"></asp:label></TD>
							<TD style="WIDTH: 148px; HEIGHT: 25px" bgColor="#008499" width="148" align="left">&nbsp;
								<asp:label id="Label49" runat="server" CssClass="tableLabel" Width="93px" Height="14px" ForeColor="White"
									BackColor="#008499" Font-Size="11px"> Telephone</asp:label></TD>
							<TD style="WIDTH: 8px; HEIGHT: 25px" width="8"></TD>
							<TD style="WIDTH: 61px; HEIGHT: 25px" width="61"><asp:label id="lblTelephone" runat="server" CssClass="tableLabel" Width="133px"></asp:label></TD>
						</TR>
						<TR height="25">
							<TD style="WIDTH: 96px" bgColor="#008499" vAlign="middle" width="96" align="left">&nbsp;
								<asp:label id="Label45" runat="server" CssClass="tableLabel" Width="90px" Height="15px" ForeColor="White"
									BackColor="#008499" Font-Size="11px"> Address 1</asp:label></TD>
							<TD style="WIDTH: 9px" width="9"></TD>
							<TD style="WIDTH: 1001px" width="1001"><asp:label id="lblAddress1" runat="server" CssClass="tableLabel" Width="303px"></asp:label></TD>
							<TD style="WIDTH: 148px" bgColor="#008499" width="148" align="left">&nbsp;
								<asp:label id="Label47" tabIndex="100" runat="server" CssClass="tableLabel" Width="83px" Height="13px"
									ForeColor="White" BackColor="#008499" Font-Size="11px">Fax</asp:label></TD>
							<TD style="WIDTH: 8px" width="8"></TD>
							<TD style="WIDTH: 61px" width="61"><asp:label id="lblFax" runat="server" CssClass="tableLabel" Width="133px"></asp:label></TD>
						</TR>
						<TR height="25">
							<TD style="WIDTH: 96px" bgColor="#008499" width="96">&nbsp;
								<asp:label id="Label1" tabIndex="100" runat="server" CssClass="tableLabel" Width="88px" Height="16px"
									ForeColor="White" BackColor="#008499" Font-Size="11px">Address 2</asp:label></TD>
							<TD style="WIDTH: 9px" width="9"></TD>
							<TD style="WIDTH: 1001px" width="1001"><asp:label id="lblAddress2" runat="server" CssClass="tableLabel" Width="301px"></asp:label></TD>
							<TD style="WIDTH: 148px" bgColor="#008499" width="148">&nbsp;</TD>
							<TD style="WIDTH: 8px" width="8" align="left"></TD>
							<TD style="WIDTH: 61px" width="61" align="left"><asp:label id="lblHC_POD" runat="server" CssClass="tableLabel" Width="127px" Font-Size="11px"></asp:label></TD>
						</TR>
						<TR height="25">
							<TD style="WIDTH: 96px" bgColor="#008499" vAlign="middle" width="96">&nbsp;
								<asp:label id="Label3" tabIndex="100" runat="server" CssClass="tableLabel" Width="99px" Height="16px"
									ForeColor="White" BackColor="#008499" Font-Size="11px">Postal Code</asp:label></TD>
							<TD style="WIDTH: 9px" width="9"></TD>
							<TD style="WIDTH: 1001px" width="1001"><asp:label id="lblPostalCode" runat="server" CssClass="tableLabel" Width="143px"></asp:label></TD>
							<TD style="WIDTH: 148px" bgColor="#008499" width="148">&nbsp;
								<asp:label id="Label46" tabIndex="100" runat="server" CssClass="tableLabel" Width="108px" Height="16px"
									ForeColor="White" BackColor="#008499" Font-Size="11px">HC POD Required</asp:label></TD>
							<TD style="WIDTH: 8px" width="8" align="left"></TD>
							<TD style="WIDTH: 61px" width="61" align="left"><asp:label id="lblHC_POD_Required" runat="server" CssClass="tableLabel" Width="127px" Font-Size="11px"></asp:label></TD>
						</TR>
					</TABLE>
					<asp:button style="Z-INDEX: 116; POSITION: absolute; TOP: 267px; LEFT: 109px" id="btnExport"
						runat="server" CssClass="queryButton" Text="Export" Width="102px" Visible="False"></asp:button><asp:button style="Z-INDEX: 102; POSITION: absolute; TOP: 37px; LEFT: 11px" id="btnCancel" runat="server"
						CssClass="queryButton" Text="Cancel" Width="61px" CausesValidation="False"></asp:button><asp:button style="Z-INDEX: 103; POSITION: absolute; TOP: 37px; LEFT: 74px" id="btnGenerateInv"
						runat="server" CssClass="queryButton" Text="Generate" Width="130px" CausesValidation="False"></asp:button><asp:button style="Z-INDEX: 104; POSITION: absolute; TOP: 37px; LEFT: 205px" id="btnSelectAllHC"
						runat="server" CssClass="queryButton" Text="Select all HC Returned" Width="163px" Visible="False"></asp:button><asp:button style="Z-INDEX: 105; POSITION: absolute; TOP: 35px; LEFT: 597px" id="btnGoToFirstPage"
						runat="server" CssClass="queryButton" Text="|<" Width="24px" CausesValidation="False"></asp:button><asp:button style="Z-INDEX: 106; POSITION: absolute; TOP: 35px; LEFT: 622px" id="btnPreviousPage"
						runat="server" CssClass="queryButton" Text="<" Width="24px" CausesValidation="False"></asp:button><cc1:mstextbox style="Z-INDEX: 107; POSITION: absolute; TOP: 37px; LEFT: 648px" id="txtGoToRec"
						tabIndex="8" runat="server" CssClass="textFieldRightAlign" Width="28px" AutoPostBack="True" MaxLength="5" TextMaskType="msNumeric" Enabled="True" NumberScale="0" NumberPrecision="8" NumberMinValue="1"
						NumberMaxValue="999999" Wrap="False"></cc1:mstextbox><asp:button style="Z-INDEX: 108; POSITION: absolute; TOP: 36px; LEFT: 677px" id="btnNextPage"
						runat="server" CssClass="queryButton" Text=">" Width="24px" CausesValidation="False"></asp:button><asp:button style="Z-INDEX: 109; POSITION: absolute; TOP: 36px; LEFT: 702px" id="btnGoToLastPage"
						runat="server" CssClass="queryButton" Text=">|" Width="24px" CausesValidation="False"></asp:button><asp:label style="Z-INDEX: 110; POSITION: absolute; TOP: 62px; LEFT: 18px" id="lblErrorMsg"
						runat="server" CssClass="errorMsgColor" Width="537px" Height="19px"></asp:label><asp:label style="Z-INDEX: 111; POSITION: absolute; TOP: 60px; LEFT: 538px" id="lblNumRec"
						runat="server" CssClass="RecMsg" Width="187px" Height="19px"></asp:label><asp:validationsummary style="Z-INDEX: 112; POSITION: absolute; TOP: 62px; LEFT: 14px" id="PageValidationSummary"
						runat="server" Width="346px" Height="39px" Visible="True" ShowSummary="False" ShowMessageBox="True"></asp:validationsummary>
					<asp:button style="Z-INDEX: 113; POSITION: absolute; TOP: 267px; LEFT: 7px" id="btnSelectAll"
						runat="server" CssClass="queryButton" Text="Select All" Width="102px" CausesValidation="False"></asp:button></FONT></form>
		</DIV>
	</BODY>
</HTML>
