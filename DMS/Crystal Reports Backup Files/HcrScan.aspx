<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<%@ Import Namespace="com.common.util" %>
<%@ Page language="c#" Codebehind="HcrScan.aspx.cs" AutoEventWireup="false" Inherits="TIES.WebUI.HcrScan" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Hcr Scanning</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<meta content="False" name="vs_snapToGrid">
		<LINK href="css/Styles.css" type="text/css" rel="stylesheet" name="stylesheet">
		<!--#INCLUDE FILE="msFormValidations.inc"-->
		<script language="javascript" src="Scripts/settingScrollPosition.js"></script>
		<script src="Scripts/jquery-1.3.2.min.js" type="text/javascript"></script>
		<script src="Scripts/jquery-ui-1.7.2.custom.min.js" type="text/javascript"></script>
		<script src="Scripts/jquery.json-2.2.js" type="text/javascript"></script>
		<script src="Scripts/jstorage.js" type="text/javascript"></script>
		<script src="Scripts/jquery.timers.js" type="text/javascript"></script>
		<script src="Scripts/HcrScan.js" type="text/javascript"></script>
		<!-- -->
		<script type="text/javascript">
			var actionProcess;
			var val;	  
		  		  
			$(function () {				
				// -------------------- Timer 1000 -------------------
				$(".myActive").everyTime(1000, function () {					
					CallService();    
				});	
						        	        
		        
		        // -------------------- Timer 1500 -------------------
		        //========== View Data
		        $(".myActiveData").everyTime(1500, function () {
					if (actionProcess == 'CLEAR'){
						//do nothing;
					} else if (actionProcess == 'SCAN'){		
						viewdataUpdate();
						viewdataMissing();
					} else if (actionProcess == 'DATAMISSING'){
						viewdataMissing();
					}						      
		        });
		        
			        
		        // -------------------- Timer 2000 -------------------
		        //=========== SET FOCUS
		        $(".myFocus").everyTime(2000, function () {
					if (actionProcess == "SCAN" && document.forms[0].val.disabled == false)
					//if (actionProcess == "SCAN")
						OnFocus();
				});

			});
			
			
			// --------------------------------- OnFocus ---------------------------------	
			function OnFocus()
		    {
		        document.getElementById('val').focus();
		    }
		    

			// -------------------------------  flushData -------------------------------
			function flushData(){
				$.jStorage.flush();
				reDraw();
			}
			
			
			// -------------------------------  CallService -------------------------------
			function CallService()
			{							
		        var index = $.jStorage.index();
		        	        	                		        
		        if (index.length > 0)
		        {		        
		            var userID = document.getElementById('lblUserID').innerHTML;		            
		            var statusCodeAll = document.getElementById('txtUpdStatus').value;		            
		            var barcode = $.jStorage.get(index[0]); 
		            		            		            
		            if (barcode != '' && statusCodeAll != '' && userID != '')
		            {    
	            		//Call Store Procedure
						$.ajax({
							url: serviceURL_Load,
							type: "POST",
							dataType: "json",
							data: "{Barcode:'" + barcode + "',StatusCodeAll:'" + statusCodeAll + "',UpdUserID:'" + userID + "'}",
							contentType: "application/json; charset=utf-8",
							success: function (msg) {	                    
								if (msg["d"]["ReturnVal"]) {
									//alert(msg["d"]["ReturnVal"]);
									//document.getElementById("lberrmsg").innerHTML = "????????";								
									$.jStorage.deleteKey(index[0]);
									reDraw();
									//alert('success');
								} 							
							},
							//error: function (e) {}							
							error: function(msg){
								alert("Call Service error");
							}
						});
					}
		        }		        
		    }
		</script>
		<!-- script for button -- Add/Remove/Add All/Remove All -->
		<!-- -------------------------------------------------- -->
		<script language="javascript" type="text/javascript">
			$(document).ready(function() {
				//If you want to move selected item from fromListBox to toListBox
				$("#btnAdd_").click(function() {
					$("#lbFrom  option:selected").appendTo("#lbTo");
				});
	            
				//If you want to move all item from fromListBox to toListBox
				$("#btnAddAll_").click(function() {
					$("#lbFrom option").appendTo("#lbTo");
				});

				//If you want to remove selected item from toListBox to fromListBox
				$("#btnRemove_").click(function() {
					$("#lbTo option:selected").appendTo("#lbFrom");
				});

				//If you want to remove all items from toListBox to fromListBox
				$("#btnRemoveAll_").click(function() {
					$("#lbTo option").appendTo("#lbFrom");
				});
			});
		</script>
		<!-- -------------------------------------------------- -->
		<script language="javascript" type="text/javascript">
		var browserType;
		
		if (document.layers) {browserType = "nn4"}
		if (document.all) {browserType = "ie"}
		if (window.navigator.userAgent.toLowerCase().match("gecko")) {
			browserType= "gecko"
		}
		</script>
		<!-- -------------------------------------------------- -->
		<style type="text/css">
			.MissCon THEAD { COLOR: red }
			.MissCon TBODY { COLOR: red }
		</style>
		<!-- ===================================================================================================================================== -->
	</HEAD>
	<BODY onload="initialonLoad();" onunload="window.top.displayBanner.fnCloseAll(0);" MS_POSITIONING="GridLayout">
		<FORM id="Form1" method="post" runat="server">
			<asp:label id="Label1" style="Z-INDEX: 107; LEFT: 20px; TOP: 30px" runat="server" Height="27px"
				CssClass="maintitleSize" Width="365px">HCR Scanning</asp:label>
			<br>
			<br>
			<div>
				<asp:label id="lblErrorMessage" style="Z-INDEX: 108; LEFT: 16px; TOP: 40px" runat="server"
					Height="17px" CssClass="errorMsgColor" Width="680px"></asp:label>
				<label id="lberrmsg" style="FONT-SIZE: 20px; Z-INDEX: 109; LEFT: 200px; COLOR: red; TOP: 35px">
				</label>
				<asp:label id="lblUserID" style="DISPLAY: none" runat="server" CssClass="tableLabel"></asp:label>
				<input id="txtUpdStatus" style="DISPLAY: none; WIDTH: 50px" readOnly type="text">
			</div>
			<!-- ========================================================Table Status======================================================== -->
			<div id="divStatus">
				<table id="tblSelectStatus" style="Z-INDEX: 110; LEFT: 200px; VERTICAL-ALIGN: top">
					<tr>
						<td><asp:label id="lblStatusAvail" runat="server" Width="130px" Font-Bold="True">Status</asp:label><br>
							<asp:listbox id="lbFrom" runat="server" Height="100px" Width="130px" SelectionMode="Multiple"></asp:listbox></td>
						<td>&nbsp;<br>
							<input id="btnAdd_" style="WIDTH: 35px" type="button" value=">"><br>
							<input id="btnRemove_" style="WIDTH: 35px" type="button" value="<"><br>
							<input id="btnAddAll_" style="WIDTH: 35px" type="button" value=">>"><br>
							<input id="btnRemoveAll_" style="WIDTH: 35px" type="button" value="<<">
						</td>
						<td><asp:label id="lblToUpdateStatus" runat="server" Width="130px" Font-Bold="True">Update Status</asp:label><br>
							<asp:listbox id="lbTo" runat="server" Height="100px" Width="130px" SelectionMode="Multiple"></asp:listbox></td>
						<td>&nbsp;<br>							
							<input class="queryButton" id="btnClear" style="WIDTH: 100px; HEIGHT: 24px" onclick="clearView()"
								type="button" value="Clear"><br>
							<input class="queryButton" id="btnScan" style="WIDTH: 100px; HEIGHT: 24px" onclick="showScanData()"
								type="button" value="Scan Data"><br>
							<input class="queryButton" id="btnMissing" style="WIDTH: 100px; HEIGHT: 24px" onclick="showDataMissing()"
								type="button" size="20" value="Data Missing"><br>
							<input class="queryButton" id="btnFlush" style="WIDTH: 100px; HEIGHT: 24px" onclick="flushData()"
								type="button" value="Flush">
						</td>
					</tr>
				</table>
			</div>
			<br>
			<br>
			<!-- ========================================================Table Barcode======================================================== -->
			<div id="divBarcode" style="VISIBILITY: hidden">
				<table id="tblShipmentTracking" style="Z-INDEX: 101; LEFT: 10px; POSITION: absolute; TOP: 165px"
					width="100%" border="0" runat="server">
					<TBODY>
						<TR>
							<TD style="WIDTH: 100%" vAlign="top">&nbsp;</TD>
						</TR>
						<TR>
							<TD style="WIDTH: 100%" vAlign="top">
								<FIELDSET style="WIDTH: 37%"><LEGEND><asp:label id="Label6" runat="server" CssClass="maintitleSize" Font-Bold="True">Scan Data</asp:label></LEGEND>
									<table style="WIDTH: 95%" cellSpacing="0" cellPadding="0" align="center">
										<thead>
											<tr>
												<td align="center"><asp:label id="Label2" runat="server" CssClass="tableLabel" Width="288px" Font-Bold="True"
														Font-Size="11pt" ForeColor="Black">Consignment Barcode</asp:label></td>
												<td>&nbsp;</td>
											</tr>
										</thead>
										<tbody>
										</tbody>
										<tfoot>
											<tr align="center">
												<td align="center"><asp:textbox id="val" runat="server" Width="288px" Font-Size="15pt"></asp:textbox></td>
												<td align="center"><input class="searchButton" id="btnAdd" style="WIDTH: 75px; HEIGHT: 29px" onclick="InsertValue()"
														type="button" value="Add">
												</td>
											</tr>
											<tr>
												<td colSpan="2">&nbsp;</td>
											</tr>
										</tfoot>
									</table>
								</FIELDSET>
								<br>
								<table style="WIDTH: 100%; TEXT-ALIGN: center">
									<tr>
										<td style="WIDTH: 5%" vAlign="top" align="center"><asp:label id="Label4" runat="server" CssClass="maintitleSize" Font-Bold="True" ForeColor="Black"
												Font-Underline="True">Local</asp:label>
											<table class="GetCon" style="FONT-SIZE: 10pt; WIDTH: 100%" align="center" border="1">
												<thead>
													<tr>
														<td align="center"><STRONG>&nbsp;Consignment#&nbsp;</STRONG></td>
													</tr>
												</thead>
												<tbody class="viewdataGetCon" id="disp_cons_local">
												</tbody>
												<tfoot>
												</tfoot>
											</table>
										</td>
										<td style="WIDTH: 36%" vAlign="top"><asp:label id="Label5" runat="server" CssClass="maintitleSize" Width="220px" Font-Bold="True"
												ForeColor="Black" Font-Underline="True">D M S</asp:label>
											<table class="CompleteCon" style="FONT-SIZE: 10pt; WIDTH: 100%" align="center" border="1">
												<thead>
													<tr>
														<td align="center"><STRONG>Consignment#</STRONG></td>
														<td align="center"><STRONG>Scan Date</STRONG></td>
														<td align="center"><STRONG>Status</STRONG></td>
													</tr>
												</thead>
												<tbody class="viewdataUpdate">
												</tbody>
												<tfoot>
												</tfoot>
											</table>
										</td>
										<!-- ���� dATA MISSING -->
										<td style="WIDTH: 59%" vAlign="top"><asp:label id="Label3" runat="server" CssClass="maintitleSize" Font-Bold="True">Data Missing</asp:label>
											<table class="MissCon" style="FONT-SIZE: 10pt; WIDTH: 100%" align="center" border="1">
												<thead>
													<TR>
														<td align="center"><STRONG>Consignment#</STRONG></td>
														<td align="center"><STRONG>Scan Date</STRONG></td>
														<td align="center"><STRONG>Status</STRONG></td>
														<td align="center"><STRONG>Remark</STRONG></td>
													</TR>
												</thead>
												<tbody class="viewdataMissing">
												</tbody>
												<tfoot>
												</tfoot>
											</table>
										</td>
									</tr>
								</table>
								<!-- -------------------------------------------------- -->
								<script>reDraw()</script>
								<!-- -------------------------------------------------- -->
								<script src="Scripts/events.js" type="text/javascript"></script>
								<!-- -------------------------------------------------- -->
								<script>
									$('#val').keydown(function (event) 
									{
										if (event.keyCode == '9' || event.keyCode == '13' ) 
										{
											event.preventDefault();											
											InsertValue();
										}
									});													
								</script>
								<!-- -------------------------------------------------- --></TD>
						</TR>
					</TBODY>
				</table>
				<br>
				<br>
			</div>
			<!-- ==================================================Table Data Missing================================================== -->
			<div id="divDataMissing" style="VISIBILITY: hidden">
				<TABLE id="tblDataMissing" style="Z-INDEX: 102; LEFT: 10px; WIDTH: 700px; POSITION: absolute; TOP: 165px"
					width="700" border="0" runat="server">
					<tr>
						<td style="WIDTH: 100%" vAlign="top" colspan="2">&nbsp;</td>
					</tr>
					<tr>
						<td>
							<FIELDSET style="WIDTH: 83%"><LEGEND><asp:label id="Label10" runat="server" CssClass="maintitleSize" Font-Bold="True">Data Missing</asp:label></LEGEND>
								<table id="tblDataMissCriteria" style="WIDTH: 95%" cellSpacing="0" cellPadding="0" align="center"
									border="0" runat="server">
									<tr>
										<td id="t" align="center">
											<asp:label id="lblScanDate" style="TEXT-ALIGN: right" runat="server" Height="8px" CssClass="tableLabel"
												Width="90px" Font-Bold="True" Font-Size="10pt">Scan Date :</asp:label>&nbsp;&nbsp;
											<cc1:mstextbox id="txtScanDate" runat="server" CssClass="textField" Width="100px" Font-Size="10pt"
												MaxLength="10" TextMaskType="msDate" TextMaskString="99/99/9999"></cc1:mstextbox>
											<asp:label id="lblTo" style="TEXT-ALIGN: center" runat="server" Height="8px" CssClass="tableLabel"
												Width="25px" Font-Bold="True" Font-Size="12px">to</asp:label>
											<cc1:mstextbox id="txtScanTo" runat="server" CssClass="textField" Width="100px" Font-Size="10pt"
												MaxLength="10" TextMaskType="msDate" TextMaskString="99/99/9999"></cc1:mstextbox>
										</td>
									</tr>
									<tr>
										<td>&nbsp;</td>
									</tr>
								</table>
							</FIELDSET>
						</td>
						<td>
							<asp:Button id="btnExportData" runat="server" CssClass="queryButton" Text="Export Data Missing"
								Width="150px" Height="33px"></asp:Button>
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<table class="MissCon" style="FONT-SIZE: 10pt; WIDTH: 100%" align="center" border="1">
								<thead>
									<TR>
										<td align="center"><STRONG>Consignment#</STRONG></td>
										<td align="center"><STRONG>Scan Date</STRONG></td>
										<td align="center"><STRONG>Status</STRONG></td>
										<td align="center"><STRONG>Remark</STRONG></td>
									</TR>
								</thead>
								<tbody class="viewdataMissing">
								</tbody>
								<tfoot>
								</tfoot>
							</table>
						</td>
					</tr>
				</TABLE>
			</div>
			<DIV class="myActive">&nbsp;</DIV>
			<DIV class="myFocus">&nbsp;</DIV>
			<DIV class="myActiveData">&nbsp;</DIV>
		</FORM>
	</BODY>
</HTML>