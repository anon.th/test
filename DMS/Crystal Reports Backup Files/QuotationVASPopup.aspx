<%@ Page language="c#" Codebehind="QuotationVASPopup.aspx.cs" AutoEventWireup="false" Inherits="TIES.WebUI.QuotationVASPopup" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>QuotationVASPopup</title>
		<LINK href="css/Styles.css" type="text/css" rel="stylesheet" name="stylesheet">
		<meta name="GENERATOR" Content="Microsoft Visual Studio 7.0">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<script>
			function SetParentWindowPosition()
			{
				if(document.QuotationVASPopup.CloseWindowStatus.value != null && document.QuotationVASPopup.CloseWindowStatus.value =="C")
				{
					window.close();
				}
				{				
					window.opener.scroll(0,window.opener.document.forms[0].ScrollPosition.value);
				}
			}
			function CloseWindow()
			{
				window.close();
			}
			
		</script>
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="QuotationVASPopup" method="post" runat="server">
			<asp:button id="btnOk" style="Z-INDEX: 100; LEFT: 233px; POSITION: absolute; TOP: 177px" runat="server" Width="85px" Height="22px" CssClass="queryButton" Text="Ok" CausesValidation="False"></asp:button>
			<asp:button id="btnClose" style="Z-INDEX: 103; LEFT: 320px; POSITION: absolute; TOP: 176px" runat="server" CausesValidation="False" Text="Close" CssClass="queryButton" Height="22px" Width="85px"></asp:button>
			<asp:label id="lblErrorMsg" style="Z-INDEX: 102; LEFT: 15px; POSITION: absolute; TOP: 212px" runat="server" CssClass="errorMsgColor" Height="19px" Width="566px"></asp:label>
			<asp:datagrid id="dgQtnVAS" runat="server" ItemStyle-Height="20" AutoGenerateColumns="False" OnPageIndexChanged="dgQtnVAS_PageChange" AllowPaging="True" PageSize="5" Width="724px">
				<ItemStyle Height="20px"></ItemStyle>
				<Columns>
					<asp:TemplateColumn HeaderText="Select">
						<HeaderStyle Width="5%" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle CssClass="gridField" VerticalAlign="Middle"></ItemStyle>
						<ItemTemplate>
							<asp:CheckBox ID="chkSelect" Runat="server"></asp:CheckBox>
						</ItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="VAS Code">
						<HeaderStyle Width="10%" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle CssClass="gridField" VerticalAlign="Middle"></ItemStyle>
						<ItemTemplate>
							<asp:Label CssClass="gridLabel" ID="lblVASCode" Text='<%#DataBinder.Eval(Container.DataItem,"vas_code")%>' Runat="server" Enabled="True">
							</asp:Label>
						</ItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Description">
						<HeaderStyle Width="75%" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle CssClass="gridField" VerticalAlign="Middle"></ItemStyle>
						<ItemTemplate>
							<asp:Label CssClass="gridLabel" ID="lblVASDesc" Text='<%#DataBinder.Eval(Container.DataItem,"vas_description")%>' Runat="server" Enabled="True">
							</asp:Label>
						</ItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Surcharge">
						<HeaderStyle Width="10%" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle CssClass="gridField" VerticalAlign="Middle"></ItemStyle>
						<ItemTemplate>
							<asp:Label CssClass="gridLabel" ID="lblSurcharge" Text='<%#DataBinder.Eval(Container.DataItem,"surcharge")%>' Runat="server" Enabled="True">
							</asp:Label>
						</ItemTemplate>
					</asp:TemplateColumn>
				</Columns>
				<PagerStyle Font-Size="Smaller" NextPageText="Next" PrevPageText="Previous" HorizontalAlign="Right"></PagerStyle>
			</asp:datagrid>
			<input type=hidden name="CloseWindowStatus" value="<%=strCloseWindowStatus%>">
		</form>
	</body>
</HTML>
