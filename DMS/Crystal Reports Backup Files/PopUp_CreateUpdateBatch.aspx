<%@ Page language="c#" Codebehind="PopUp_CreateUpdateBatch.aspx.cs" AutoEventWireup="false" Inherits="TIES.WebUI.Manifest.PopUp_CreateUpdateBatch" %>
<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<%@ Import Namespace="com.common.util" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Create or Update Batch</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK rel="stylesheet" type="text/css" href="../css/Styles.css">
		<!--#INCLUDE FILE="../msFormValidations.inc"-->
		<script language="javascript" src="../Scripts/settingScrollPosition.js"></script>
		<script language="javascript">
		function confirmDialog(state,batchNo,batchId,truckId , batchDate,batchType)
		{
						
						//alert("batchDate:"+batchDate);
						var formObject = document.PopUp_CreateUpdateBatch
						var txt;
						var appendtxt;
						//var truckId , batchId, batchDate,batchType
						
						if(batchId=='' || batchId==null)
						{
						batchId =formObject.ddlBatchID[formObject.ddlBatchID.selectedIndex].text;
						}
						if(truckId=="" || truckId==null)
						{
						truckId =formObject.ddlTruckID[formObject.ddlTruckID.selectedIndex].text; 
						}
						if(batchDate=="" || batchDate==null)
						{
						batchDate = formObject.txtBatchDate.value;
						}
						if(batchType=="" || batchType==null)
						{
						batchType = formObject.txtBatchType.value;
						}
						if(batchNo=="" || batchNo==null)
						{
						batchNo = formObject.txtBatchNo.value;
						}
				if(formObject.rdbCreateNewBatch.checked==true && state!="Update")
				{
					txt = "A new batch ID for ";
					if(formObject.txtStatusCode.value!='CLS')
					{
						txt+="Truck ID: " + truckId + " ";
					}
					appendtxt = " will be created. Is this correct?";

				}
				
				else if(formObject.rdbUpdateExistingBatch.checked==true ||  state=="Update")
				{
					txt = "Update to batch ID : " + batchNo + " " ;
					if(formObject.txtStatusCode.value!='CLS' && truckId!="")
					{
						txt+="Truck ID: " + truckId + "";
					}
					appendtxt = ". Is this correct?";


				}
				//alert(batchNo);
					txt+= batchId +" ";
					txt+= batchDate + " ";
					txt+= batchType + "";
					txt += appendtxt;
					//formObject.txtMode.value= window.confirm(txt);
					return window.confirm(txt);

		}
///



function popup()
{
				var formObject = document.PopUp_CreateUpdateBatch
			if(formObject.rdbCreateNewBatch.checked==true)
			{
				batchId =formObject.ddlBatchID[formObject.ddlBatchID.selectedIndex].text;
				truckId =formObject.ddlTruckID[formObject.ddlTruckID.selectedIndex].text; 
				batchDate = formObject.txtBatchDate.value;
				batchNo= formObject.txtBatchNo.value;

		

				
    			params = "PopUp_CreateUpdateBatch_Show.aspx?strFormID=PopUp_CreateUpdateBatch";
				params+= "&batchId="+batchId;
				params+= "&truckId="+truckId;
				params+= "&batchDate="+batchDate;
				params+= "&resultTarget=txtMode";
				if(batchNo!="")
				{
				params+= "&batchNo="+batchNo;
				}
    
					
					var obj = new Object(); 
					obj.data1 = batchId;
					obj.data2 = truckId;
					obj.data3 =	batchDate;
					obj.data4 = batchNo;
     
					showModalDialog(params, obj, 'dialogWidth:600px;center:yes;status:no;');
					formObject.txtBatchNoFromPopup.value=obj.Result;
					//alert(obj.BatchID);
					if(obj.Result=="Cancel")
					{
					return false;
					}
					return confirmDialog(obj.Result,obj.BatchNo,obj.BatchID,obj.TruckID,obj.BatchDate,obj.BatchType);
			}					
			else
			{
			    batchId =formObject.hidBatchID.value;
				truckId =formObject.hidTruckID.value;
				batchDate = formObject.hidBatchDate.value;
				batchNo= formObject.hidBatchNo.value;
				batchType = formObject.hidBatchType.value;
				return confirmDialog("Update",batchNo,batchId,truckId,batchDate,batchType);
			}

}

			function ConfirmUpdate()
			{
				if (confirm("Status code not allowed after departure.\nWould you like to cancel the departure record?")==true)
					document.getElementById("btnConfirmUpdate").click();
				else
					return false;
			}


		</script>
	</HEAD>
	<BODY onunload="window.opener.top.displayBanner.fnCloseAll(1);" MS_POSITIONING="GridLayout">
		<FORM id="PopUp_CreateUpdateBatch" method="post" runat="server">
			<INPUT style="Z-INDEX: 100; POSITION: absolute; TOP: 304px; LEFT: 368px" type="hidden"
				name="ScrollPosition">
			<asp:button style="Z-INDEX: 116; POSITION: absolute; DISPLAY: none; TOP: 368px; LEFT: 376px"
				id="btnConfirmUpdate" runat="server" CssClass="queryButton" Width="101px" Text="btnConfirmUpdate"></asp:button><asp:label style="Z-INDEX: 111; POSITION: absolute; TOP: 24px; LEFT: 16px" id="lblErrrMsg"
				runat="server" CssClass="errorMsgColor" Width="392px" Height="40px" Wrap="True"></asp:label><asp:validationsummary style="Z-INDEX: 102; POSITION: absolute; COLOR: red; TOP: 184px; LEFT: 376px" id="PageValidationSummary"
				ShowSummary="False" HeaderText="Please enter the missing  fields." ShowMessageBox="True" DisplayMode="BulletList" Runat="server"></asp:validationsummary><asp:button style="Z-INDEX: 108; POSITION: absolute; TOP: 88px; LEFT: 368px" id="btnOK" runat="server"
				CssClass="queryButton" Width="101px" Text="OK"></asp:button><asp:button style="Z-INDEX: 109; POSITION: absolute; TOP: 112px; LEFT: 368px" id="btnCancel"
				runat="server" CssClass="queryButton" Width="101px" Text="Cancel"></asp:button>
			<FIELDSET style="Z-INDEX: 110; POSITION: absolute; WIDTH: 288px; HEIGHT: 263px; TOP: 80px; LEFT: 16px"><LEGEND><asp:label id="lblCreateOrUpdateBatch" CssClass="tableHeadingFieldset" Width="138px" Runat="server"
						HEIGHT="16px">Create or Update Batch</asp:label></LEGEND>
				<TABLE style="WIDTH: 119.36%; HEIGHT: 219px" class="tablelabel">
					<TR>
						<TD colSpan="2"><asp:radiobutton id="rdbCreateNewBatch" runat="server" CssClass="tablelabel" Text="Create new batch"
								AutoPostBack="True" GroupName="Batch"></asp:radiobutton></TD>
					</TR>
					<TR>
						<TD style="WIDTH: 116px; HEIGHT: 8px" noWrap>&nbsp;&nbsp;&nbsp;&nbsp;
							<asp:label id="lblBatchID" runat="server" CssClass="tableLabel">Batch ID:</asp:label></TD>
						<TD style="WIDTH: 190px; HEIGHT: 8px"><asp:dropdownlist id="ddlBatchID" runat="server" CssClass="textField" Width="131px" AutoPostBack="True"></asp:dropdownlist></TD>
					</TR>
					<TR>
						<TD style="WIDTH: 116px">&nbsp;&nbsp;&nbsp;&nbsp;
							<asp:label id="lblTruckID" runat="server" CssClass="tableLabel">Vehicle ID:</asp:label></TD>
						<TD style="WIDTH: 190px"><asp:dropdownlist id="ddlTruckID" runat="server" CssClass="textField" Width="131px"></asp:dropdownlist><asp:button id="btnTruckIDSrch" runat="server" CssClass="searchButton" Text="..."></asp:button></TD>
					</TR>
					<TR>
						<TD style="WIDTH: 116px">&nbsp;&nbsp;&nbsp;&nbsp;
							<asp:label id="lblBatchDate" runat="server" CssClass="tableLabel">Batch Date:</asp:label></TD>
						<TD style="WIDTH: 180px"><cc1:mstextbox id="txtBatchDate" runat="server" CssClass="textField" Width="130px" MaxLength="10"
								TextMaskString="99/99/9999" TextMaskType="msDate"></cc1:mstextbox></TD>
					</TR>
					<TR>
						<TD style="WIDTH: 116px; HEIGHT: 33px">&nbsp;&nbsp;&nbsp;&nbsp;
							<asp:label id="lblBatchType" runat="server" CssClass="tableLabel">Batch Type</asp:label></TD>
						<TD style="WIDTH: 130px; HEIGHT: 33px" onselectstart="return false;" onmousedown="return false;"><asp:textbox id="txtBatchType" runat="server" CssClass="tableTextbox" Width="130px" MaxLength="10"
								ReadOnly="True"></asp:textbox></TD>
					</TR>
					<TR id="trServiceType" runat="server" style="DISPLAY:none">
						<TD style="WIDTH: 116px; HEIGHT: 33px">&nbsp;&nbsp;&nbsp;&nbsp;
							<asp:label id="Label1" runat="server" CssClass="tableLabel">Service Type</asp:label></TD>
						<TD style="WIDTH: 130px; HEIGHT: 33px" onselectstart="return false;" onmousedown="return false;">
							<asp:DropDownList id="ddlServiceType" runat="server" DataTextField="DbComboText" DataValueField="DbComboValue"></asp:DropDownList>
						</TD>
					</TR>
					<TR>
						<TD style="HEIGHT: 29px" colSpan="2"><asp:radiobutton id="rdbUpdateExistingBatch" runat="server" CssClass="tablelabel" Text="Update existing batch"
								AutoPostBack="True" GroupName="Batch" Checked="True"></asp:radiobutton></TD>
					</TR>
					<TR vAlign="top">
						<TD style="WIDTH: 91px; HEIGHT: 29px" noWrap>&nbsp;&nbsp;&nbsp;&nbsp;
							<asp:label id="lblBatchNumber" runat="server" CssClass="tableLabel" Width="112px" EnableViewState="False">Batch Number:</asp:label></TD>
						<TD style="WIDTH: 130px; HEIGHT: 29px"><cc1:mstextbox id="txtBatchNo" runat="server" CssClass="tableTextbox" Width="130px" TextMaskType="msNumericCOD"
								BackColor="White" NumberPrecision="10" NumberMaxValueCOD="9999999999"></cc1:mstextbox></TD>
					</TR>
					<TR>
						<TD style="WIDTH: 116px; HEIGHT: 9px" noWrap></TD>
						<TD style="WIDTH: 130px; HEIGHT: 9px"><FONT face="Tahoma"></FONT></TD>
					</TR>
				</TABLE>
			</FIELDSET>
			<asp:textbox style="Z-INDEX: 112; POSITION: absolute; DISPLAY: none; TOP: 336px; LEFT: 16px"
				id="txtStatusCode" runat="server" Width="0px"></asp:textbox><asp:textbox style="Z-INDEX: 113; POSITION: absolute; DISPLAY: none; TOP: 368px; LEFT: 128px"
				id="txtMode" runat="server"></asp:textbox><asp:textbox style="Z-INDEX: 114; POSITION: absolute; DISPLAY: none; TOP: 344px; LEFT: 128px"
				id="txtBatchNoFromPopup" runat="server"></asp:textbox><asp:textbox style="Z-INDEX: 103; POSITION: absolute; DISPLAY: none; TOP: 256px; LEFT: 56px"
				id="hidBatchNo" runat="server"></asp:textbox><asp:textbox style="Z-INDEX: 106; POSITION: absolute; DISPLAY: none; TOP: 256px; LEFT: 56px"
				id="hidTruckID" runat="server"></asp:textbox><asp:textbox style="Z-INDEX: 105; POSITION: absolute; DISPLAY: none; TOP: 256px; LEFT: 56px"
				id="hidBatchDate" runat="server"></asp:textbox><asp:textbox style="Z-INDEX: 104; POSITION: absolute; DISPLAY: none; TOP: 256px; LEFT: 56px"
				id="hidBatchType" runat="server"></asp:textbox><asp:textbox style="Z-INDEX: 101; POSITION: absolute; DISPLAY: none; TOP: 256px; LEFT: 56px"
				id="hidBatchID" runat="server"></asp:textbox><asp:button style="Z-INDEX: 115; POSITION: absolute; DISPLAY: none; TOP: 408px; LEFT: 40px"
				id="btnPopup" runat="server" Text="Button"></asp:button></FORM>
	</BODY>
</HTML>
