<%@ Page language="c#" Codebehind="RecipientReferencesPopup.aspx.cs" AutoEventWireup="false" Inherits="TIES.WebUI.RecipientReferencesPopup" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>RecipientReferencesPopup</title>
		<LINK rel="stylesheet" type="text/css" href="css/Styles.css" name="stylesheet">
		<meta name="GENERATOR" content="Microsoft Visual Studio 7.0">
		<meta name="CODE_LANGUAGE" content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<script language="javascript" src="Scripts/settingScrollPosition.js"></script>
		<script language="javascript">	
			function UpperMaskSpecialWithHyphen(toField)
			{
				var lcNewChar = String.fromCharCode(window.event.keyCode);
				var llRetVal = ((lcNewChar >= '0' && lcNewChar <= '9')||(lcNewChar >= 'A' && lcNewChar <= 'Z') || (lcNewChar >= 'a' && lcNewChar <= 'z') || (lcNewChar == '-'));
				window.event.keyCode = lcNewChar.toUpperCase().charCodeAt(0);
				return llRetVal;
			}
		</script>
	</HEAD>
	<body bottomMargin="0" leftMargin="0" rightMargin="0" topMargin="10" MS_POSITIONING="FlowLayout">
		<form id="RecipientReferencesPopup" method="post" runat="server">
			<TABLE id="Table1" border="0" cellSpacing="1" cellPadding="1" width="100%" align="center">
				<TR>
					<TD style="WIDTH: 31px">&nbsp;</TD>
					<TD width="10%"><asp:label id="lblVASCode" runat="server" CssClass="tablelabel"> Telephone</asp:label></TD>
					<TD style="WIDTH: 200px"><asp:textbox id="txtTelephone" runat="server" CssClass="textField" Height="19px" Width="139px"></asp:textbox></TD>
					<TD style="WIDTH: 622px">&nbsp;</TD>
					<TD>&nbsp;</TD>
				</TR>
				<TR>
					<TD style="WIDTH: 31px">&nbsp;</TD>
					<TD style="HEIGHT: 21px"><asp:label id="lblCountry" runat="server" CssClass="tableLabel" Width="94px">Name</asp:label></TD>
					<TD style="WIDTH: 200px; HEIGHT: 21px"><asp:textbox id="txtName" runat="server" CssClass="textField" Width="139px"></asp:textbox></TD>
					<TD style="WIDTH: 622px; HEIGHT: 21px"><asp:button id="btnQuery" runat="server" CssClass="queryButton" Width="100px" CausesValidation="False"
							Text="Query"></asp:button><asp:button id="btnSearch" runat="server" CssClass="buttonProp" Height="21px" Width="84px" CausesValidation="False"
							Text="Search"></asp:button><asp:button id="btnOk" runat="server" CssClass="buttonProp" Height="21px" Width="84px" CausesValidation="False"
							Text="Close"></asp:button></TD>
					<TD style="HEIGHT: 21px">&nbsp;</TD>
				</TR>
				<TR>
					<TD style="WIDTH: 31px">&nbsp;</TD>
					<TD><asp:label id="Label1" runat="server" CssClass="tablelabel">Postal Code</asp:label></TD>
					<TD style="WIDTH: 200px"><asp:textbox id="txtPostalCode" runat="server" CssClass="textField" Width="139px"></asp:textbox></TD>
					<TD style="WIDTH: 622px">&nbsp;</TD>
					<TD>&nbsp;</TD>
				</TR>
				<TR>
					<TD style="WIDTH: 31px">&nbsp;</TD>
					<TD><asp:label id="Label2" runat="server" CssClass="tableLabel" Width="94px">Contact Person</asp:label></TD>
					<TD style="WIDTH: 200px"><asp:textbox id="txtContactPerson" runat="server" CssClass="textField" Width="139px"></asp:textbox></TD>
					<TD style="WIDTH: 622px">&nbsp;</TD>
					<TD>&nbsp;</TD>
				</TR>
				<TR>
					<TD style="WIDTH: 31px">&nbsp;</TD>
					<TD><asp:label id="Label3" runat="server" CssClass="tableLabel" Width="94px">Fax</asp:label></TD>
					<TD style="WIDTH: 200px"><asp:textbox id="txtFax" runat="server" CssClass="textField" Width="139px"></asp:textbox></TD>
					<TD style="WIDTH: 622px">
						<P align="right"><asp:button id="btnInsert" runat="server" CssClass="queryButton" Width="100px" CausesValidation="False"
								Text="Insert"></asp:button>&nbsp;</P>
					</TD>
					<TD>&nbsp;</TD>
				</TR>
				<TR>
					<TD height="5" colSpan="5" align="left"><asp:label id="lblReferences" runat="server" CssClass="errorMsgColor" Width="669px"></asp:label></TD>
				</TR>
				<TR>
					<TD colSpan="5" align="center"><asp:datagrid id="dgReferences" runat="server" CssClass="gridHeading" Width="98%" OnPageIndexChanged="Paging"
							AllowPaging="True" AutoGenerateColumns="False" BorderColor="#CCCCCC" BackColor="White" CellPadding="3" BorderWidth="1px"
							BorderStyle="None" AllowCustomPaging="True" style="Z-INDEX: 0">
							<SelectedItemStyle Font-Bold="True" ForeColor="White" BackColor="#669999"></SelectedItemStyle>
							<ItemStyle CssClass="drilldownField"></ItemStyle>
							<HeaderStyle Width="10%" CssClass="drilldownHeading"></HeaderStyle>
							<FooterStyle ForeColor="#000066" BackColor="White"></FooterStyle>
							<Columns>
								<asp:ButtonColumn HeaderStyle-Width="2%" CommandName="Select" Text="&lt;IMG SRC='images/butt-select.gif' border=0 title='Select' &gt;"></asp:ButtonColumn>
								<asp:EditCommandColumn HeaderStyle-Width="2%" CancelText="&lt;IMG SRC='images/butt-cancel.gif' border=0 title='Cancel' &gt;"
									EditText="&lt;IMG SRC='images/butt-edit.gif' border=0 title='Edit' &gt;" UpdateText="&lt;IMG SRC='images/butt-update.gif' border=0 title='Update' &gt;"></asp:EditCommandColumn>
								<asp:ButtonColumn HeaderStyle-Width="2%" CommandName="Delete" Text="&lt;IMG SRC='images/butt-delete.gif' border=0 title='Delete' &gt;"></asp:ButtonColumn>
								<asp:TemplateColumn HeaderText="Telephone">
									<HeaderStyle CssClass="gridHeading" Width="10%"></HeaderStyle>
									<ItemStyle></ItemStyle>
									<ItemTemplate>
										<%#DataBinder.Eval(Container.DataItem,"telephone") %>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:TextBox CssClass="gridTextBox" ID="txtGridTelephone" Visible="True" TextMode="SingleLine" Text='<%#DataBinder.Eval(Container.DataItem,"telephone") %>' Runat="server" MaxLength="20" onkeypress="if (window.event.keyCode >= 48 && window.event.keyCode <= 57) return true; else return false;">
										</asp:TextBox>
										<asp:RequiredFieldValidator id="ReqTxtPhone" runat="server" ErrorMessage="Telephone Number" Display="None" ControlToValidate="txtGridTelephone"></asp:RequiredFieldValidator>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Name">
									<HeaderStyle CssClass="gridHeading" Width="15%"></HeaderStyle>
									<ItemTemplate>
										<%#DataBinder.Eval(Container.DataItem,"reference_name") %>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:TextBox ID="txtGridReference_name" Visible="True" TextMode="MultiLine" Columns="30" Width="120px" Rows="3" Text='<%#DataBinder.Eval(Container.DataItem,"reference_name") %>' Runat="server" MaxLength="100">
										</asp:TextBox>
										<asp:RequiredFieldValidator id="ReqtxtGridReference_name" runat="server" ErrorMessage="Name" ControlToValidate="txtGridReference_name"
											Display="None"></asp:RequiredFieldValidator>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Address 1">
									<HeaderStyle CssClass="gridHeading" Width="15%"></HeaderStyle>
									<ItemTemplate>
										<asp:Label CssClass="gridLabel" ID="lblAddress1" Visible="True" Text='<%#DataBinder.Eval(Container.DataItem,"address1") %>' Runat="server">
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:TextBox ID="txtAddress1" Visible="True" TextMode="MultiLine" Columns="30" Rows="3" Width="121px" Text='<%#DataBinder.Eval(Container.DataItem,"address1") %>' Runat="server" MaxLength="100">
										</asp:TextBox>
										<asp:RequiredFieldValidator id="ReqtxtAddress1" runat="server" ErrorMessage="Address 1" ControlToValidate="txtAddress1"
											Display="None"></asp:RequiredFieldValidator>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Address 2">
									<HeaderStyle CssClass="gridHeading" Width="15%"></HeaderStyle>
									<ItemTemplate>
										<asp:Label CssClass="gridLabel" ID="lblAddress2" Visible="True" Text='<%#DataBinder.Eval(Container.DataItem,"address2") %>' Runat="server">
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:TextBox ID="txtAddress2" Visible="True" TextMode="MultiLine" Columns="30" Rows="3" Width="121px" Text='<%#DataBinder.Eval(Container.DataItem,"address2") %>' Runat="server" MaxLength="100">
										</asp:TextBox>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Postal Code">
									<HeaderStyle CssClass="gridHeading" Width="9%"></HeaderStyle>
									<ItemStyle></ItemStyle>
									<ItemTemplate>
										<asp:Label CssClass="gridLabel" ID="lblZipCode" Visible="True" Text='<%#DataBinder.Eval(Container.DataItem,"zipcode") %>' Runat="server">
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:TextBox CssClass="gridTextBox" ID="txtPostal" style="TEXT-TRANSFORM:uppercase" Visible="True" TextMode="SingleLine" Text='<%#DataBinder.Eval(Container.DataItem,"zipcode") %>' Runat="server" MaxLength="10">
										</asp:TextBox>
										<asp:RequiredFieldValidator id="RequiredfieldvalidatorPostal" runat="server" ErrorMessage="Postal Code" ControlToValidate="txtPostal"
											Display="None"></asp:RequiredFieldValidator>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Fax">
									<HeaderStyle CssClass="gridHeading" Width="10%"></HeaderStyle>
									<ItemStyle></ItemStyle>
									<ItemTemplate>
										<asp:Label CssClass="gridLabel" ID="lblFax" Visible="True" Text='<%#DataBinder.Eval(Container.DataItem,"fax") %>' Runat="server">
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:TextBox CssClass="gridTextBox" ID="txtGridFax" Visible="True" TextMode="SingleLine" Text='<%#DataBinder.Eval(Container.DataItem,"fax") %>' Runat="server" MaxLength="20">
										</asp:TextBox>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Contact Person">
									<HeaderStyle CssClass="gridHeading" Width="10%"></HeaderStyle>
									<ItemStyle></ItemStyle>
									<ItemTemplate>
										<asp:Label CssClass="gridLabel" ID="lblContactperson" Visible="True" Text='<%#DataBinder.Eval(Container.DataItem,"contactperson") %>' Runat="server">
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:TextBox CssClass="gridTextBox" ID="txtGridContactperson" Visible="True" TextMode="SingleLine" Text='<%#DataBinder.Eval(Container.DataItem,"contactperson") %>' Runat="server" MaxLength="100">
										</asp:TextBox>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<%--
								2
								2
								2
								10<asp:BoundColumn DataField="telephone" HeaderText="Telephone"></asp:BoundColumn>
								20<asp:BoundColumn DataField="reference_name" HeaderText="Name"></asp:BoundColumn>
								20<asp:BoundColumn DataField="address1" HeaderText="Address 1"></asp:BoundColumn>
								20<asp:BoundColumn DataField="address2" HeaderText="Address 2"></asp:BoundColumn>
								7<asp:BoundColumn DataField="zipcode" HeaderText="Postal Code"></asp:BoundColumn>
								7<asp:BoundColumn DataField="fax" HeaderText="Fax"></asp:BoundColumn>
								10<asp:BoundColumn DataField="contactperson" HeaderText="Contact Person"></asp:BoundColumn>
								--%>
							</Columns>
							<PagerStyle NextPageText="3" PrevPageText="3" HorizontalAlign="Left" ForeColor="#000066" BackColor="White"
								Mode="NumericPages"></PagerStyle>
						</asp:datagrid></TD>
				</TR>
			</TABLE>
			<asp:validationsummary id="validateSum" runat="server" HeaderText="Please enter the missing  fields." ShowMessageBox="True"
				ShowSummary="False"></asp:validationsummary></form>
	</body>
</HTML>
