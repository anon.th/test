<%@ Page language="c#" Codebehind="TopBanner.aspx.cs" AutoEventWireup="false" Inherits="commom.webui.aspx.MainFrame.TopBanner" smartNavigation="False" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>TopBanner</title>
		<LINK href="<%=strStyleSheet%>" type=text/css rel=stylesheet name="style1">
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<base target="_top">
		<style type="text/css"> .handover { cursor: hand ; }
		</style>
		<style type="text/css"> BODY { BACKGROUND-IMAGE: url(images/top.jpg); BACKGROUND-REPEAT: no-repeat }
		</style>
		<script language="JavaScript" type="text/JavaScript">
<!--
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->
		</script>
		<script language="javascript">
		var win_op=null;
		var win_op_child=null;
		var win_op_child_child=null;
		function fnCloseAll(PopupPos)
		{
			if(PopupPos<1)
				fnClose(win_op);
			if(PopupPos<2)
				fnClose(win_op_child);
			if(PopupPos<3)
				fnClose(win_op_child_child);	
			return true;		
		}
		function fnClose(obj)
		{
			if(obj != null)
			{
				if(!obj.closed)	
				{
					obj.close();
					obj=null;
				}
			}
			return true;
		}
		function HomeLogoFn()
		{
		
			window.top.displayContent.location.href="defaultWelcome.aspx";
		}
		function fnSubmit()
		{
			document.TopBanner.action="TopBanner.aspx?str=LOGOUT";
			document.TopBanner.submit()
		}
		</script>
	</HEAD>
	<body bottomMargin=0 bgColor=#133d49 leftMargin=0 background="<%=strBGImage%>" 
topMargin=0 
onload="MM_preloadImages('images/button-home1.gif','images/button-logout1.gif','images/button-help1.gif')" 
rightMargin=0 MS_POSITIONING="GridLayout">
		<form id="TopBanner" method="post" runat="server">
			<table cellSpacing="0" cellPadding="5" width="100%" border="0">
				<tr>
					<td vAlign="bottom" align="right" height="100">
						<table cellSpacing="0" cellPadding="5" width="100" border="0">
							<tr>
								<td><IMG class="handover" onmouseover="MM_swapImage('home','','images/button-home1.gif',1)" onclick="HomeLogoFn();" onmouseout="MM_swapImgRestore()" height="12" alt="Home" src="images/button-home0.gif" width="48" border="0" name="home"></td>
								<td><A onmouseover="MM_swapImage('logout','','images/button-logout1.gif',1)" onmouseout="MM_swapImgRestore()" href="TopBanner.aspx?str=LOGOUT"><IMG height="12" alt="Logout" src="images/button-logout0.gif" width="56" border="0" name="logout"></A></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
