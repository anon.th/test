<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<%@ Register TagPrefix="dbCombo" Namespace="Cambro.Web.DbCombo" Assembly="Cambro.Web.DbCombo" %>
<%@ Page language="c#" Codebehind="SopScanShipmentRpt.aspx.cs" AutoEventWireup="false" Inherits="TIES.WebUI.SopScanShipmentRpt" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>SOP shipping report</title>
		<LINK href="css/Styles.css" type="text/css" rel="stylesheet" name="stylesheet">
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<!--#INCLUDE FILE="msFormValidations.inc"-->
		<script language="javascript" src="Scripts/settingScrollPosition.js"></script>
		<script type="text/javascript">
		function initialonload(){
                //Set focus on scan val  
                document.getElementById("txtConsignmentNo").focus();
                //Set current date
                var vNow = null;
                var strDay = "";
                var strMonth = "";
                var strYear = "";
                vNow = new Date();
                strDay = '0' + vNow.getDate();
                strDay = strDay.substring(strDay.length - 2);
                
                strMonth = '0' + (vNow.getMonth() + 1);
                strMonth = strMonth.substring(strMonth.length - 2);
                
                strYear  = vNow.getFullYear();
                document.getElementById('txtDate').value = strDay + '/' + strMonth + '/' + strYear;
                document.getElementById('txtDateTo').value = strDay + '/' + strMonth + '/' + strYear;
                         
           
                SetScrollPosition();
                
            }   
		</script>
	</HEAD>
	<body onclick="GetScrollPosition();" onload="initialonload();" onunload="window.top.displayBanner.fnCloseAll(0);"
		MS_POSITIONING="GridLayout">
		<form id="ShipmentTrackingQuery" method="post" runat="server">
			<asp:button id="btnQuery" style="Z-INDEX: 101; LEFT: 20px; POSITION: absolute; TOP: 40px" runat="server"
				Width="64px" CssClass="queryButton" Text="Query"></asp:button><asp:label id="lblErrorMessage" style="Z-INDEX: 106; LEFT: 20px; POSITION: absolute; TOP: 71px"
				runat="server" Width="700px" CssClass="errorMsgColor" Height="17px">Place holder for err msg</asp:label>&nbsp;
			<asp:button id="btnExecuteQuery" style="Z-INDEX: 102; LEFT: 84px; POSITION: absolute; TOP: 40px"
				runat="server" Width="123px" CssClass="queryButton" Text="Execute Query"></asp:button><asp:button id="btnCancelQry" style="Z-INDEX: 103; LEFT: 234px; POSITION: absolute; TOP: 40px"
				runat="server" CssClass="queryButton" Text="Cancel Query" Enabled="False" Visible="False"></asp:button>
			<TABLE id="tblShipmentTracking" style="Z-INDEX: 105; LEFT: 14px; WIDTH: 851px; POSITION: absolute; TOP: 96px"
				width="851" border="0" runat="server">
				<tr>
					<td>&nbsp;
						<FIELDSET style="WIDTH: 828px; HEIGHT: 165px"><LEGEND><asp:label id="lblShipment" style="FONT-SIZE: 25px" runat="server" Width="79px" CssClass="tableHeadingFieldset">Shipment</asp:label></LEGEND>
							<TABLE id="tblShipment" style="WIDTH: 768px; HEIGHT: 350px" cellSpacing="0" cellPadding="0"
								width="768" align="left" border="0" runat="server">
								<tr>
									<td style="WIDTH: 37px">&nbsp;</td>
									<td style="WIDTH: 182px">&nbsp;</td>
									<td style="WIDTH: 555px" colSpan="2">&nbsp;</td>
								</tr>
								<TR>
									<td style="WIDTH: 37px">&nbsp;</td>
									<TD class="tableLabel" style="WIDTH: 182px"><asp:label id="Label3" style="FONT-SIZE: 18px" runat="server" CssClass="tableLabel">Consignment No:</asp:label></TD>
									<td style="WIDTH: 555px" colSpan="2"><asp:textbox id="txtConsignmentNo" style="FONT-SIZE: 18px" runat="server" Width="350" CssClass="textField"
											Height="30" AutoPostBack="True" MaxLength="30"></asp:textbox></td>
								</TR>
								<tr>
									<td style="WIDTH: 37px">&nbsp;</td>
									<td style="WIDTH: 182px">&nbsp;</td>
									<td style="WIDTH: 555px" colSpan="2"></td>
								</tr>
								<tr>
									<td style="WIDTH: 37px; HEIGHT: 23px">&nbsp;</td>
									<td style="WIDTH: 182px; HEIGHT: 23px"><FONT face="Tahoma"></FONT></td>
									<td style="WIDTH: 555px; HEIGHT: 23px" colSpan="2"><asp:radiobutton id="rbLongRoute" runat="server" Width="104px" CssClass="tableRadioButton" Text="Linehaul"
											Height="22px" AutoPostBack="True" Checked="True" GroupName="QueryByRoute" style="FONT-SIZE: 18px"></asp:radiobutton><asp:radiobutton id="rbShortRoute" runat="server" Width="192px" CssClass="tableRadioButton" Text="Delivery Route"
											Height="22px" AutoPostBack="True" GroupName="QueryByRoute" style="FONT-SIZE: 18px"></asp:radiobutton><FONT face="Tahoma"></FONT></td>
								</tr>
								<TR>
									<td style="WIDTH: 37px">&nbsp;</td>
									<TD class="tableLabel" style="WIDTH: 182px"><asp:label id="lblRouteCode" style="FONT-SIZE: 18px" runat="server" CssClass="tableLabel">Route Code:</asp:label></TD>
									<td style="WIDTH: 555px" colSpan="2"><DBCOMBO:DBCOMBO id="DbComboPathCode" style="FONT-SIZE: 18px" tabIndex="14" Width="250" CssClass="tableLabel"
											Height="30" AutoPostBack="True" Runat="server" TextBoxColumns="18" RegistrationKey=" " TextUpLevelSearchButton="v" ShowDbComboLink="False"
											ServerMethod="DbComboRouteCodeSelect"></DBCOMBO:DBCOMBO></td>
								</TR>
								<tr>
									<td style="WIDTH: 37px">&nbsp;</td>
									<td style="WIDTH: 182px">&nbsp;</td>
									<td style="WIDTH: 555px" colSpan="2">&nbsp;</td>
								</tr>
								<TR>
									<td style="WIDTH: 37px">&nbsp;</td>
									<TD class="tableLabel" style="WIDTH: 182px"><asp:label id="lblZipCode" style="FONT-SIZE: 18px" runat="server" CssClass="tableLabel">Postal Code:</asp:label></TD>
									<td style="WIDTH: 170px"><cc1:mstextbox id="txtZipCode" style="FONT-SIZE: 18px" runat="server" Width="185" CssClass="textField"
											Height="30" MaxLength="10" TextMaskType="msUpperAlfaNumericWithUnderscore"></cc1:mstextbox></td>
									<td style="WIDTH: 333px"><asp:button id="btnZipCode" runat="server" Width="28" CssClass="searchButton" Text="..." Height="28"
											CausesValidation="False"></asp:button></td>
								</TR>
								<tr>
									<td style="WIDTH: 37px">&nbsp;</td>
									<td style="WIDTH: 182px">&nbsp;</td>
									<td style="WIDTH: 555px" colSpan="2"></td>
								</tr>
								<TR>
									<td style="WIDTH: 37px">&nbsp;</td>
									<TD class="tableLabel" style="WIDTH: 182px"><asp:label id="lblBookingType" style="FONT-SIZE: 18px" runat="server" CssClass="tableLabel">Service Type:</asp:label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									</TD>
									<td style="WIDTH: 555px" colSpan="2"><DBCOMBO:DBCOMBO id="DbComboServiceType" style="FONT-SIZE: 18px" tabIndex="14" Width="250" CssClass="tableLabel"
											Height="30" AutoPostBack="True" Runat="server" TextBoxColumns="18" RegistrationKey=" " TextUpLevelSearchButton="v" ShowDbComboLink="False"
											ServerMethod="ServiceCodeServerMethod"></DBCOMBO:DBCOMBO>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									</td>
								</TR>
								<tr>
									<td style="WIDTH: 37px">&nbsp;</td>
									<td style="WIDTH: 182px">&nbsp;</td>
									<td style="WIDTH: 555px" colSpan="2"></td>
								</tr>
								<TR>
									<td style="WIDTH: 37px; HEIGHT: 21px">&nbsp;</td>
									<TD class="tableLabel" style="WIDTH: 182px; HEIGHT: 21px"><asp:label id="Label2" style="FONT-SIZE: 18px" runat="server" CssClass="tableLabel">Scan Date:</asp:label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									</TD>
									<td style="WIDTH: 555px; HEIGHT: 21px" colSpan="2"><cc1:mstextbox id="txtDate" style="FONT-SIZE: 18px" runat="server" Width="150" CssClass="textField"
											Height="30" MaxLength="10" TextMaskType="msDate" TextMaskString="99/99/9999"></cc1:mstextbox><FONT face="Tahoma">&nbsp;</FONT>
										<asp:label id="Label4" style="FONT-SIZE: 18px" runat="server" CssClass="tableLabel">to</asp:label><FONT face="Tahoma">&nbsp;
											<cc1:mstextbox id="txtDateTo" style="FONT-SIZE: 18px" runat="server" Width="150" CssClass="textField"
												Height="30" MaxLength="10" TextMaskType="msDate" TextMaskString="99/99/9999"></cc1:mstextbox></FONT></td>
								</TR>
								<tr>
									<td style="WIDTH: 37px">&nbsp;</td>
									<td style="WIDTH: 182px">&nbsp;</td>
									<td style="WIDTH: 555px" colSpan="2">&nbsp;</td>
								</tr>
								<tr>
									<td style="WIDTH: 37px">&nbsp;</td>
									<td style="WIDTH: 182px"><asp:label id="Label6" style="FONT-SIZE: 18px" runat="server" CssClass="tableLabel">User ID:</asp:label></td>
									<td style="WIDTH: 555px" colSpan="2"><asp:textbox id="txtUserID" style="FONT-SIZE: 18px" runat="server" Width="185" Height="30"></asp:textbox></td>
								</tr>
								<TR>
									<TD style="WIDTH: 37px">&nbsp;</TD>
									<TD style="WIDTH: 182px">&nbsp;</TD>
									<TD style="WIDTH: 555px" colSpan="2">&nbsp;</TD>
								</TR>
							</TABLE>
						</FIELDSET>
					</td>
				</tr>
			</TABLE>
			<asp:label id="Label1" style="Z-INDEX: 104; LEFT: 20px; POSITION: absolute; TOP: 10px" runat="server"
				CssClass="maintitleSize" Width="365px" Height="27px">SOP Shipping Report</asp:label></TR></TABLE></TR></TABLE><INPUT 
style="Z-INDEX: 107; LEFT: 408px; POSITION: absolute; TOP: 0px" type=hidden 
value="<%=strScrollPosition%>" name=ScrollPosition><asp:checkbox id="cbMissCon" style="DISPLAY: none; FONT-SIZE: 18px; Z-INDEX: 108; LEFT: 328px; POSITION: absolute; TOP: 552px"
				runat="server" Text="Missing Consignment"></asp:checkbox>
		</form>
	</body>
</HTML>
