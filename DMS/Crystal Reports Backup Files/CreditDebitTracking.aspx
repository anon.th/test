<%@ Page language="c#" Codebehind="CreditDebitTracking.aspx.cs" AutoEventWireup="false" Inherits="com.ties.CreditDebitTracking" %>
<%@ Register TagPrefix="dbCombo" Namespace="Cambro.Web.DbCombo" Assembly="Cambro.Web.DbCombo" %>
<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>CreditDebitTracking</title>
		<LINK href="css/Styles.css" type="text/css" rel="stylesheet" name="stylesheet">
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<!--#INCLUDE FILE="msFormValidations.inc"-->
	</HEAD>
	<BODY MS_POSITIONING="GridLayout" onunload="window.top.displayBanner.fnCloseAll(0);">
		<FORM id="CreditDebitTracking" method="post" runat="server">
			<asp:label id="lblTitle" style="Z-INDEX: 101; LEFT: 10px; POSITION: absolute; TOP: 0px" runat="server"
				CssClass="maintitleSize" Width="558px">Credit/Debit Note Tracking</asp:label><asp:label id="lblErrorMessage" style="Z-INDEX: 106; LEFT: 24px; POSITION: absolute; TOP: 32px"
				runat="server" CssClass="errorMsgColor" Width="556px"></asp:label>
			<div id="mainPanel" style="Z-INDEX: 102; LEFT: 7px; WIDTH: 100%; POSITION: relative; TOP: 51px; HEIGHT: 450px"
				runat="server">&nbsp;
				<asp:button id="btnQuery" runat="server" CssClass="queryButton" Width="64px" Text="Query"></asp:button><asp:button id="btnGenerate" tabIndex="1" runat="server" CssClass="queryButton" Width="96px"
					Text="Generate"></asp:button>
				<fieldset><legend><asp:label id="Label2" Runat="server">Retrieval Basis</asp:label></legend>
					<TABLE id="tblCreditDebitQry" style="WIDTH: 90%" border="0" runat="server">
						<TR height="10">
							<TD style="HEIGHT: 4px" width="5%"></TD>
							<TD style="HEIGHT: 4px" width="5%"></TD>
							<TD style="HEIGHT: 4px" width="5%"></TD>
							<TD style="WIDTH: 40px; HEIGHT: 4px" width="40"></TD>
							<TD style="HEIGHT: 4px" width="5%"></TD>
							<TD style="HEIGHT: 4px" width="5%"></TD>
							<TD style="HEIGHT: 4px" width="5%"></TD>
							<TD style="HEIGHT: 4px" width="5%"></TD>
							<TD style="HEIGHT: 4px" width="5%"></TD>
							<TD style="HEIGHT: 4px" width="5%"></TD>
							<TD style="HEIGHT: 4px" width="5%"></TD>
							<TD style="HEIGHT: 4px" width="5%"></TD>
							<TD style="HEIGHT: 4px" width="5%"></TD>
							<TD style="HEIGHT: 4px" width="5%"></TD>
							<TD style="HEIGHT: 4px" width="5%"></TD>
							<TD style="HEIGHT: 4px" width="5%"></TD>
							<TD style="HEIGHT: 4px" width="5%"></TD>
							<TD style="HEIGHT: 4px" width="5%"></TD>
							<TD style="HEIGHT: 4px" width="5%"></TD>
							<TD style="HEIGHT: 4px" width="5%"></TD>
						</TR>
						<TR>
							<TD style="HEIGHT: 4px" width="5%"></TD>
							<TD style="HEIGHT: 4px" width="5%"></TD>
							<TD style="HEIGHT: 4px" width="5%"></TD>
							<TD style="WIDTH: 40px; HEIGHT: 4px" width="40"></TD>
							<TD style="HEIGHT: 4px" width="5%"></TD>
							<TD style="HEIGHT: 4px" width="5%"></TD>
							<TD style="HEIGHT: 4px" width="5%"></TD>
							<TD style="HEIGHT: 4px" width="5%"></TD>
							<TD style="HEIGHT: 4px" width="5%"></TD>
							<TD style="HEIGHT: 4px" width="5%"></TD>
							<TD style="HEIGHT: 4px" width="5%"></TD>
							<TD style="HEIGHT: 4px" width="5%"></TD>
							<TD style="HEIGHT: 4px" width="5%"></TD>
							<TD style="HEIGHT: 4px" width="5%"></TD>
							<TD style="HEIGHT: 4px" width="5%"></TD>
							<TD style="HEIGHT: 4px" width="5%"></TD>
							<TD style="HEIGHT: 4px" width="5%"></TD>
							<TD style="HEIGHT: 4px" width="5%"></TD>
							<TD style="HEIGHT: 4px" width="5%"></TD>
							<TD style="HEIGHT: 4px" width="5%"></TD>
						</TR>
						<TR>
							<td></td>
							<TD style="HEIGHT: 26px" colSpan="4"><asp:label id="lblSearchType" runat="server" Width="113px" Height="18px" cssClass="tableLabel">Search Type</asp:label></TD>
							<TD style="HEIGHT: 26px" colSpan="4"><asp:radiobutton id="rbCreditNote" tabIndex="3" runat="server" CssClass="tableRadioButton" Width="141px"
									Text="Credit Note" Checked="True" AutoPostBack="True" GroupName="TypeToSearch"></asp:radiobutton></TD>
							<TD style="HEIGHT: 26px" colSpan="4"><asp:radiobutton id="rbDebitNote" tabIndex="4" runat="server" CssClass="tableRadioButton" Width="141px"
									Text="Debit Note" AutoPostBack="True" GroupName="TypeToSearch"></asp:radiobutton></TD>
							<TD style="HEIGHT: 26px" colSpan="4"></TD>
							<TD style="HEIGHT: 26px" colSpan="3"></TD>
						</TR>
						<TR>
							<TD style="HEIGHT: 4px" width="5%"></TD>
							<TD style="HEIGHT: 4px" width="5%"></TD>
							<TD style="HEIGHT: 4px" width="5%"></TD>
							<TD style="WIDTH: 40px; HEIGHT: 4px" width="40"></TD>
							<TD style="HEIGHT: 4px" width="5%"></TD>
							<TD style="HEIGHT: 4px" width="5%"></TD>
							<TD style="HEIGHT: 4px" width="5%"></TD>
							<TD style="HEIGHT: 4px" width="5%"></TD>
							<TD style="HEIGHT: 4px" width="5%"></TD>
							<TD style="HEIGHT: 4px" width="5%"></TD>
							<TD style="HEIGHT: 4px" width="5%"></TD>
							<TD style="HEIGHT: 4px" width="5%"></TD>
							<TD style="HEIGHT: 4px" width="5%"></TD>
							<TD style="HEIGHT: 4px" width="5%"></TD>
							<TD style="HEIGHT: 4px" width="5%"></TD>
							<TD style="HEIGHT: 4px" width="5%"></TD>
							<TD style="HEIGHT: 4px" width="5%"></TD>
							<TD style="HEIGHT: 4px" width="5%"></TD>
							<TD style="HEIGHT: 4px" width="5%"></TD>
							<TD style="HEIGHT: 4px" width="5%"></TD>
						</TR>
						<TR height="27">
							<TD colSpan="1"></TD>
							<TD style="WIDTH: 124px" vAlign="top" colSpan="3">
								<asp:Label id="lblCreditDebitNo" runat="server" CssClass="tablelabel">Credit No.</asp:Label></TD>
							<TD style="WIDTH: 191px" vAlign="top" colSpan="5"><dbcombo:dbcombo id="DbCreditDebitNo" tabIndex="15" runat="server" Width="128px" AutoPostBack="True"
									ShowDbComboLink="False" TextUpLevelSearchButton="v" ServerMethod="CreditDebitServerMethod" TextBoxColumns="15" RegistrationKey=" "></dbcombo:dbcombo></TD>
							<TD colSpan="1"></TD>
							<TD style="WIDTH: 76px" vAlign="top" align="center" colSpan="3"><asp:label id="lblInvoiceNo" runat="server" CssClass="tableLabel" Width="96px">Invoice No</asp:label></TD>
							<TD style="WIDTH: 184px" vAlign="top" colSpan="7"><dbcombo:dbcombo id="DbInvoiceNo" tabIndex="16" runat="server" Width="122px" AutoPostBack="True"
									ShowDbComboLink="False" TextUpLevelSearchButton="v" ServerMethod="InvoiceServerMethod" TextBoxColumns="25" RegistrationKey=" "></dbcombo:dbcombo></TD>
						</TR>
						<TR height="27">
							<TD colSpan="1"></TD>
							<TD style="WIDTH: 124px" vAlign="top" colSpan="3">
								<asp:Label id="lblConsignmentNo" runat="server" CssClass="tablelabel">Consignment No.</asp:Label></TD>
							<TD style="WIDTH: 191px" vAlign="top" colSpan="5"><cc1:mstextbox id="txtConsignmentNo" tabIndex="6" runat="server" CssClass="textField" Width="100%"
									MaxLength="20"></cc1:mstextbox></TD>
							<TD colSpan="2"></TD>
							<TD colSpan="3"></TD>
							<TD colSpan="6"></TD>
						</TR>
						<TR>
							<TD style="HEIGHT: 4px" width="5%"></TD>
							<TD style="HEIGHT: 4px" width="5%"></TD>
							<TD style="HEIGHT: 4px" width="5%"></TD>
							<TD style="WIDTH: 40px; HEIGHT: 4px" width="40"></TD>
							<TD style="HEIGHT: 4px" width="5%"></TD>
							<TD style="HEIGHT: 4px" width="5%"></TD>
							<TD style="HEIGHT: 4px" width="5%"></TD>
							<TD style="HEIGHT: 4px" width="5%"></TD>
							<TD style="HEIGHT: 4px" width="5%"></TD>
							<TD style="HEIGHT: 4px" width="5%"></TD>
							<TD style="HEIGHT: 4px" width="5%"></TD>
							<TD style="HEIGHT: 4px" width="5%"></TD>
							<TD style="HEIGHT: 4px" width="5%"></TD>
							<TD style="HEIGHT: 4px" width="5%"></TD>
							<TD style="HEIGHT: 4px" width="5%"></TD>
							<TD style="HEIGHT: 4px" width="5%"></TD>
							<TD style="HEIGHT: 4px" width="5%"></TD>
							<TD style="HEIGHT: 4px" width="5%"></TD>
							<TD style="HEIGHT: 4px" width="5%"></TD>
							<TD style="HEIGHT: 4px" width="5%"></TD>
						</TR>
						<TR>
							<TD colSpan="1"></TD>
							<TD style="HEIGHT: 26px" colSpan="4"><asp:label id="lblDateType" runat="server" Width="113px" Height="18px" cssClass="tableLabel">Date To Search</asp:label></TD>
							<TD style="HEIGHT: 26px" colSpan="4"><asp:radiobutton id="rbCreationDate" tabIndex="2" runat="server" CssClass="tableRadioButton" Width="135px"
									Text="Creation Date" Checked="True" AutoPostBack="True" GroupName="DateToSearch"></asp:radiobutton></TD>
							<TD style="HEIGHT: 26px" colSpan="4"><asp:radiobutton id="rbInvoiceDate" tabIndex="3" runat="server" CssClass="tableRadioButton" Width="141px"
									Text="Invoice Date" AutoPostBack="True" GroupName="DateToSearch"></asp:radiobutton></TD>
							<TD style="HEIGHT: 26px" colSpan="4"></TD>
							<TD style="HEIGHT: 26px" colSpan="3"></TD>
						</TR>
						<TR>
							<TD style="HEIGHT: 4px" width="5%"></TD>
							<TD style="HEIGHT: 4px" width="5%"></TD>
							<TD style="HEIGHT: 4px" width="5%"></TD>
							<TD style="WIDTH: 40px; HEIGHT: 4px" width="40"></TD>
							<TD style="HEIGHT: 4px" width="5%"></TD>
							<TD style="HEIGHT: 4px" width="5%"></TD>
							<TD style="HEIGHT: 4px" width="5%"></TD>
							<TD style="HEIGHT: 4px" width="5%"></TD>
							<TD style="HEIGHT: 4px" width="5%"></TD>
							<TD style="HEIGHT: 4px" width="5%"></TD>
							<TD style="HEIGHT: 4px" width="5%"></TD>
							<TD style="HEIGHT: 4px" width="5%"></TD>
							<TD style="HEIGHT: 4px" width="5%"></TD>
							<TD style="HEIGHT: 4px" width="5%"></TD>
							<TD style="HEIGHT: 4px" width="5%"></TD>
							<TD style="HEIGHT: 4px" width="5%"></TD>
							<TD style="HEIGHT: 4px" width="5%"></TD>
							<TD style="HEIGHT: 4px" width="5%"></TD>
							<TD style="HEIGHT: 4px" width="5%"></TD>
							<TD style="HEIGHT: 4px" width="5%"></TD>
						</TR>
						<TR>
							<TD colSpan="1"></TD>
							<TD style="WIDTH: 124px; HEIGHT: 16px" colSpan="3"><asp:radiobutton id="rbMonth" tabIndex="4" runat="server" CssClass="tableRadioButton" Width="100%"
									Text="Monthly" Checked="True" AutoPostBack="True" GroupName="EnterDate"></asp:radiobutton></TD>
							<TD style="WIDTH: 191px; HEIGHT: 16px" colSpan="5"><asp:dropdownlist id="ddMonth" tabIndex="5" runat="server" CssClass="textField" Width="100%"></asp:dropdownlist></TD>
							<TD style="HEIGHT: 16px" align="right" colSpan="4"><asp:label id="lblYear" runat="server" CssClass="tableLabel" Width="100%">Year</asp:label></TD>
							<TD style="WIDTH: 123px; HEIGHT: 16px" colSpan="3"><cc1:mstextbox id="txtYear" tabIndex="6" runat="server" CssClass="textField" Width="100%" MaxLength="4"
									TextMaskType="msNumeric" NumberMaxValue="2999" NumberMinValue="1" NumberPrecision="4"></cc1:mstextbox></TD>
							<TD style="HEIGHT: 16px" colSpan="4"></TD>
						</TR>
						<TR>
							<TD colSpan="1"></TD>
							<TD style="WIDTH: 124px; HEIGHT: 30px" colSpan="3"><asp:radiobutton id="rbPeriod" tabIndex="7" runat="server" CssClass="tableRadioButton" Width="100%"
									Text="Period" AutoPostBack="True" GroupName="EnterDate"></asp:radiobutton></TD>
							<TD style="WIDTH: 191px; HEIGHT: 30px" colSpan="5"><cc1:mstextbox id="txtPeriod" tabIndex="8" runat="server" CssClass="textField" Width="104px" MaxLength="10"
									TextMaskType="msDate" TextMaskString="99/99/9999"></cc1:mstextbox></TD>
							<TD style="HEIGHT: 30px" align="right" colSpan="4"><asp:label id="lblTo" runat="server" CssClass="tableLabel" Width="100%">To</asp:label></TD>
							<TD style="WIDTH: 123px; HEIGHT: 30px" colSpan="3"><cc1:mstextbox id="txtTo" tabIndex="9" runat="server" CssClass="textField" Width="120px" MaxLength="10"
									TextMaskType="msDate" TextMaskString="99/99/9999"></cc1:mstextbox></TD>
							<TD style="HEIGHT: 30px" colSpan="4"></TD>
						</TR>
						<TR height="27">
							<TD colSpan="1"></TD>
							<TD style="WIDTH: 124px" colSpan="3"><asp:radiobutton id="rbDate" tabIndex="10" runat="server" CssClass="tableRadioButton" Width="100%"
									Text="Daily" AutoPostBack="True" GroupName="EnterDate"></asp:radiobutton></TD>
							<TD style="WIDTH: 191px" colSpan="5"><cc1:mstextbox id="txtDate" tabIndex="11" runat="server" CssClass="textField" Width="104px" MaxLength="10"
									TextMaskType="msDate" TextMaskString="99/99/9999"></cc1:mstextbox></TD>
							<TD colSpan="11"></TD>
						</TR>
						<TR>
							<TD style="HEIGHT: 4px" width="5%"></TD>
							<TD style="HEIGHT: 4px" width="5%"></TD>
							<TD style="HEIGHT: 4px" width="5%"></TD>
							<TD style="WIDTH: 40px; HEIGHT: 4px" width="40"></TD>
							<TD style="HEIGHT: 4px" width="5%"></TD>
							<TD style="HEIGHT: 4px" width="5%"></TD>
							<TD style="HEIGHT: 4px" width="5%"></TD>
							<TD style="HEIGHT: 4px" width="5%"></TD>
							<TD style="HEIGHT: 4px" width="5%"></TD>
							<TD style="HEIGHT: 4px" width="5%"></TD>
							<TD style="HEIGHT: 4px" width="5%"></TD>
							<TD style="HEIGHT: 4px" width="5%"></TD>
							<TD style="HEIGHT: 4px" width="5%"></TD>
							<TD style="HEIGHT: 4px" width="5%"></TD>
							<TD style="HEIGHT: 4px" width="5%"></TD>
							<TD style="HEIGHT: 4px" width="5%"></TD>
							<TD style="HEIGHT: 4px" width="5%"></TD>
							<TD style="HEIGHT: 4px" width="5%"></TD>
							<TD style="HEIGHT: 4px" width="5%"></TD>
							<TD style="HEIGHT: 4px" width="5%"></TD>
						</TR>
					</TABLE>
				</fieldset>
				<fieldset><legend><asp:label id="Label1" Runat="server">Payer Details</asp:label></legend>
					<table>
						<TR height="14">
							<TD colSpan="1"></TD>
							<TD colSpan="19"></TD>
						</TR>
						<TR>
							<TD style="HEIGHT: 4px" width="5%"></TD>
							<TD style="HEIGHT: 4px" width="5%"></TD>
							<TD style="HEIGHT: 4px" width="5%"></TD>
							<TD style="WIDTH: 40px; HEIGHT: 4px" width="40"></TD>
							<TD style="HEIGHT: 4px" width="5%"></TD>
							<TD style="HEIGHT: 4px" width="5%"></TD>
							<TD style="HEIGHT: 4px" width="5%"></TD>
							<TD style="HEIGHT: 4px" width="5%"></TD>
							<TD style="HEIGHT: 4px" width="5%"></TD>
							<TD style="HEIGHT: 4px" width="5%"></TD>
							<TD style="HEIGHT: 4px" width="5%"></TD>
							<TD style="HEIGHT: 4px" width="5%"></TD>
							<TD style="HEIGHT: 4px" width="5%"></TD>
							<TD style="HEIGHT: 4px" width="5%"></TD>
							<TD style="HEIGHT: 4px" width="5%"></TD>
							<TD style="HEIGHT: 4px" width="5%"></TD>
							<TD style="HEIGHT: 4px" width="5%"></TD>
							<TD style="HEIGHT: 4px" width="5%"></TD>
							<TD style="HEIGHT: 4px" width="5%"></TD>
							<TD style="HEIGHT: 4px" width="5%"></TD>
						</TR>
						<TR>
							<TD colSpan="1"></TD>
							<TD style="HEIGHT: 22px" colSpan="4"><asp:label id="lblPayerType" runat="server" CssClass="tableLabel" Width="103px" Font-Bold="True">Payer Type</asp:label></TD>
							<TD style="HEIGHT: 22px" colSpan="4"><asp:radiobutton id="rbCustomer" tabIndex="12" runat="server" CssClass="tableRadioButton" Width="100px"
									Text="Customer" Checked="True" AutoPostBack="True" GroupName="PayerType"></asp:radiobutton></TD>
							<TD colSpan="4"><asp:radiobutton id="rbAgent" tabIndex="13" runat="server" CssClass="tableRadioButton" Width="100px"
									Text="Agent" AutoPostBack="True" GroupName="PayerType"></asp:radiobutton></TD>
							<TD style="WIDTH: 197px; HEIGHT: 22px" colSpan="4"><asp:radiobutton id="rbBoth" tabIndex="14" runat="server" CssClass="tableRadioButton" Width="81px"
									Text="Both" AutoPostBack="True" GroupName="PayerType"></asp:radiobutton></TD>
							<TD style="HEIGHT: 22px" colSpan="3"></TD>
						</TR>
						<TR>
							<TD style="HEIGHT: 4px" width="5%"></TD>
							<TD style="HEIGHT: 4px" width="5%"></TD>
							<TD style="HEIGHT: 4px" width="5%"></TD>
							<TD style="WIDTH: 40px; HEIGHT: 4px" width="40"></TD>
							<TD style="HEIGHT: 4px" width="5%"></TD>
							<TD style="HEIGHT: 4px" width="5%"></TD>
							<TD style="HEIGHT: 4px" width="5%"></TD>
							<TD style="HEIGHT: 4px" width="5%"></TD>
							<TD style="HEIGHT: 4px" width="5%"></TD>
							<TD style="HEIGHT: 4px" width="5%"></TD>
							<TD style="HEIGHT: 4px" width="5%"></TD>
							<TD style="HEIGHT: 4px" width="5%"></TD>
							<TD style="HEIGHT: 4px" width="5%"></TD>
							<TD style="HEIGHT: 4px" width="5%"></TD>
							<TD style="HEIGHT: 4px" width="5%"></TD>
							<TD style="HEIGHT: 4px" width="5%"></TD>
							<TD style="HEIGHT: 4px" width="5%"></TD>
							<TD style="HEIGHT: 4px" width="5%"></TD>
							<TD style="HEIGHT: 4px" width="5%"></TD>
							<TD style="HEIGHT: 4px" width="5%"></TD>
						</TR>
						<TR height="27">
							<TD colSpan="1"></TD>
							<TD style="WIDTH: 124px" vAlign="top" colSpan="3"><asp:label id="lblPayer" runat="server" CssClass="tableLabel" Width="76px">Payer ID</asp:label></TD>
							<TD style="WIDTH: 191px" vAlign="top" colSpan="5"><dbcombo:dbcombo id="dbCmbPayerID" tabIndex="15" runat="server" Width="128px" AutoPostBack="True"
									ShowDbComboLink="False" TextUpLevelSearchButton="v" ServerMethod="PayerIDServerMethod" TextBoxColumns="15" RegistrationKey=" "></dbcombo:dbcombo></TD>
							<TD colSpan="1"></TD>
							<TD style="WIDTH: 76px" vAlign="top" align="center" colSpan="3"><asp:label id="lblPayerAc" runat="server" CssClass="tableLabel" Width="96px">Payer Name</asp:label></TD>
							<TD style="WIDTH: 184px" vAlign="top" colSpan="7"><dbcombo:dbcombo id="dbCmbPayerName" tabIndex="16" runat="server" Width="122px" AutoPostBack="True"
									ShowDbComboLink="False" TextUpLevelSearchButton="v" ServerMethod="PayerNameServerMethod" TextBoxColumns="25" RegistrationKey=" " ClientStateFunction="PayerNameState()"></dbcombo:dbcombo></TD>
						</TR>
						<TR>
							<TD style="HEIGHT: 4px" width="5%"></TD>
							<TD style="HEIGHT: 4px" width="5%"></TD>
							<TD style="HEIGHT: 4px" width="5%"></TD>
							<TD style="WIDTH: 40px; HEIGHT: 4px" width="40"></TD>
							<TD style="HEIGHT: 4px" width="5%"></TD>
							<TD style="HEIGHT: 4px" width="5%"></TD>
							<TD style="HEIGHT: 4px" width="5%"></TD>
							<TD style="HEIGHT: 4px" width="5%"></TD>
							<TD style="HEIGHT: 4px" width="5%"></TD>
							<TD style="HEIGHT: 4px" width="5%"></TD>
							<TD style="HEIGHT: 4px" width="5%"></TD>
							<TD style="HEIGHT: 4px" width="5%"></TD>
							<TD style="HEIGHT: 4px" width="5%"></TD>
							<TD style="HEIGHT: 4px" width="5%"></TD>
							<TD style="HEIGHT: 4px" width="5%"></TD>
							<TD style="HEIGHT: 4px" width="5%"></TD>
							<TD style="HEIGHT: 4px" width="5%"></TD>
							<TD style="HEIGHT: 4px" width="5%"></TD>
							<TD style="HEIGHT: 4px" width="5%"></TD>
							<TD style="HEIGHT: 4px" width="5%"></TD>
						</TR>
					</table>
				</fieldset>
			</div>
			<div id="listPanel" style="Z-INDEX: 103; LEFT: 0px; WIDTH: 100%; POSITION: relative; TOP: 51px; HEIGHT: 450px"
				runat="server" ms_positioning="GridLayout">
				<table id="listTable" style="WIDTH: 100%" runat="server">
					<tr>
						<td colSpan="1"></td>
						<td colSpan="3"><asp:button id="btnListBack" tabIndex="1" runat="server" CssClass="queryButton" Width="46px"
								Text="Back"></asp:button></td>
						<td colSpan="4"></td>
						<td colSpan="4"></td>
						<td colSpan="4"></td>
						<td colSpan="4"></td>
					</tr>
					<tr>
						<td colSpan="1"></td>
						<td colSpan="18">
							<div id="scrollCDList" style="BORDER-RIGHT: #99ffcc 1px solid; BORDER-TOP: #99ffcc 1px solid; OVERFLOW: auto; BORDER-LEFT: #99ffcc 1px solid; BORDER-BOTTOM: #99ffcc 1px solid; HEIGHT: 325px"
								runat="server"><asp:datagrid id="dgCreditDebitList" Width="100%" OnItemCommand="dgCreditDebitList_Button" OnPageIndexChanged="dgCreditDebitList_OnPaging"
									AllowCustomPaging="True" AllowPaging="True" AutoGenerateColumns="False" Runat="server">
									<SelectedItemStyle Font-Size="10pt" CssClass="gridFieldSelected"></SelectedItemStyle>
									<ItemStyle Height="20px" CssClass="gridField"></ItemStyle>
									<HeaderStyle CssClass="gridHeading"></HeaderStyle>
									<Columns>
										<asp:ButtonColumn Text="&lt;IMG SRC='images/butt-select.gif' border=0 title='Select' &gt;" ButtonType="LinkButton"
											CommandName="Select">
											<ItemStyle HorizontalAlign="Center" Width="5%"></ItemStyle>
										</asp:ButtonColumn>
										<asp:ButtonColumn DataTextField="Note_No" HeaderText="Credit/Debit No" CommandName="Choose">
											<HeaderStyle Font-Bold="True"></HeaderStyle>
											<ItemStyle Width="20%"></ItemStyle>
										</asp:ButtonColumn>
										<asp:BoundColumn Visible="False" DataField="Note_No" HeaderText="Credit/Debit No"></asp:BoundColumn>
										<asp:BoundColumn DataField="Invoice_No" HeaderText="Invoice No">
											<HeaderStyle Font-Bold="True"></HeaderStyle>
											<ItemStyle Width="20%"></ItemStyle>
										</asp:BoundColumn>
										<asp:BoundColumn DataField="Consignment_No" HeaderText="Consignment No.">
											<HeaderStyle Font-Bold="True"></HeaderStyle>
											<ItemStyle HorizontalAlign="Left" Width="20%"></ItemStyle>
										</asp:BoundColumn>
										<asp:BoundColumn DataField="Invoice_Amt" HeaderText="Invoice Amount" DataFormatString="{0:n}">
											<ItemStyle HorizontalAlign="Right" Width="15%"></ItemStyle>
										</asp:BoundColumn>
										<asp:BoundColumn DataField="Invoice_Date" HeaderText="Invoice Date" DataFormatString="{0:dd/MM/yyyy HH:mm}">
											<ItemStyle HorizontalAlign="Right" Width="20%"></ItemStyle>
										</asp:BoundColumn>
									</Columns>
									<PagerStyle NextPageText="Next" PrevPageText="Previous" HorizontalAlign="Right" CssClass="normalText"></PagerStyle>
								</asp:datagrid></div>
						</td>
						<td colSpan="1"></td>
					</tr>
				</table>
			</div>
			<div id="detailPanel" style="Z-INDEX: 104; LEFT: 0px; WIDTH: 100%; POSITION: relative; TOP: 51px; HEIGHT: 450px"
				runat="server" ms_positioning="GridLayout">
				<table id="Table1" style="WIDTH: 100%" runat="server">
					<tr>
						<TD style="HEIGHT: 4px" width="5%"></TD>
						<TD style="HEIGHT: 4px" width="5%"></TD>
						<TD style="HEIGHT: 4px" width="5%"></TD>
						<TD style="HEIGHT: 4px" width="5%"></TD>
						<TD style="HEIGHT: 4px" width="5%"></TD>
						<TD style="HEIGHT: 4px" width="5%"></TD>
						<TD style="HEIGHT: 4px" width="5%"></TD>
						<TD style="HEIGHT: 4px" width="5%"></TD>
						<TD style="HEIGHT: 4px" width="5%"></TD>
						<TD style="HEIGHT: 4px" width="5%"></TD>
						<TD style="HEIGHT: 4px" width="5%"></TD>
						<TD style="HEIGHT: 4px" width="5%"></TD>
						<TD style="HEIGHT: 4px" width="5%"></TD>
						<TD style="HEIGHT: 4px" width="5%"></TD>
						<TD style="HEIGHT: 4px" width="5%"></TD>
						<TD style="HEIGHT: 4px" width="5%"></TD>
						<TD style="HEIGHT: 4px" width="5%"></TD>
						<TD style="HEIGHT: 4px" width="5%"></TD>
						<TD style="HEIGHT: 4px" width="5%"></TD>
						<TD style="HEIGHT: 4px" width="5%"></TD>
					</tr>
					<tr>
						<td colSpan="1"></td>
						<td align="left" colSpan="7"><asp:button id="btnQuery1" CssClass="queryButton" Width="50px" Text="Query" Height="20" Runat="server"></asp:button><asp:button id="btnSave" CssClass="queryButton" Width="50px" Text="Save" Height="20" Runat="server"></asp:button><asp:button id="btnDelete" CssClass="queryButton" Width="50px" Text="Cancel" Height="20" Runat="server"></asp:button><asp:button id="btnPrint" CssClass="queryButton" Width="50px" Text="Print" Height="20" Runat="server"></asp:button><asp:button id="btnDetailBack" tabIndex="1" runat="server" CssClass="queryButton" Width="46px"
								Text="Back" Height="20"></asp:button></td>
						<td align="right" colSpan="11"></td>
						<td colSpan="1"></td>
					</tr>
					<tr>
						<TD style="HEIGHT: 4px" width="5%"></TD>
						<TD style="HEIGHT: 4px" width="5%"></TD>
						<TD style="HEIGHT: 4px" width="5%"></TD>
						<TD style="HEIGHT: 4px" width="5%"></TD>
						<TD style="HEIGHT: 4px" width="5%"></TD>
						<TD style="HEIGHT: 4px" width="5%"></TD>
						<TD style="HEIGHT: 4px" width="5%"></TD>
						<TD style="HEIGHT: 4px" width="5%"></TD>
						<TD style="HEIGHT: 4px" width="5%"></TD>
						<TD style="HEIGHT: 4px" width="5%"></TD>
						<TD style="HEIGHT: 4px" width="5%"></TD>
						<TD style="HEIGHT: 4px" width="5%"></TD>
						<TD style="HEIGHT: 4px" width="5%"></TD>
						<TD style="HEIGHT: 4px" width="5%"></TD>
						<TD style="HEIGHT: 4px" width="5%"></TD>
						<TD style="HEIGHT: 4px" width="5%"></TD>
						<TD style="HEIGHT: 4px" width="5%"></TD>
						<TD style="HEIGHT: 4px" width="5%"></TD>
						<TD style="HEIGHT: 4px" width="5%"></TD>
						<TD style="HEIGHT: 4px" width="5%"></TD>
					</tr>
					<tr>
						<td align="right" colSpan="1"></td>
						<td colSpan="3">
							<asp:Label id="lblCreditNoteNo" runat="server" CssClass="tablelabel">Credit No.</asp:Label></td>
						<td colSpan="4"><asp:textbox id="txtCreditNoteNo" tabIndex="1" runat="server" CssClass="textField" MaxLength="16"
								Enabled="False" ReadOnly="True"></asp:textbox></td>
						<td colSpan="3"></td>
						<td colSpan="5"><asp:label id="lblCreditNoteDate" runat="server" CssClass="tableLabel" Height="22px"> Credit Date</asp:label></td>
						<td colSpan="4"><cc1:mstextbox id="txtCreditNoteDate" tabIndex="2" runat="server" CssClass="textField" MaxLength="16"
								TextMaskType="msDateTime" TextMaskString="99/99/9999 99:99"></cc1:mstextbox></td>
					</tr>
					<tr>
						<TD align="right" colSpan="1"></TD>
						<td colSpan="3"><asp:label id="lblInvoice1" runat="server" CssClass="tableLabel" Height="22px">Invoice No</asp:label></td>
						<td colSpan="4"><asp:textbox id="txtInvoiceNo" tabIndex="1" runat="server" CssClass="textField" MaxLength="16"
								Enabled="False" ReadOnly="True"></asp:textbox></td>
						<td colSpan="3"></td>
						<td colSpan="5"><asp:label id="lblInvoiceDate" runat="server" CssClass="tableLabel" Height="22px">Invoice Date</asp:label></td>
						<td colSpan="4"><cc1:mstextbox id="txtInvoiceDate" tabIndex="4" runat="server" CssClass="textField" MaxLength="16"
								TextMaskType="msDateTime" TextMaskString="99/99/9999 99:99" Enabled="False" ReadOnly="True"></cc1:mstextbox></td>
					</tr>
					<tr>
						<td colSpan="1"></td>
						<td colSpan="3"><asp:label id="lblPayerCode" runat="server" CssClass="tableLabel" Height="22px">Payer Code</asp:label></td>
						<td colSpan="4"><asp:textbox id="txtPayerID" tabIndex="5" runat="server" CssClass="textField" MaxLength="100"
								Enabled="False" ReadOnly="True"></asp:textbox></td>
						<td colSpan="3"></td>
						<td colSpan="5"><asp:label id="lblTotalCreditAmt" runat="server" CssClass="tableLabel" Height="22px">Total Credit Amount</asp:label></td>
						<td colSpan="4"><asp:textbox id="txtTotalCreditAmt" onblur="round(this,2)" tabIndex="6" runat="server" CssClass="textFieldRightAlign"
								MaxLength="16" Enabled="False" ReadOnly="True"></asp:textbox></td>
					</tr>
					<tr>
						<td colSpan="1"></td>
						<td colSpan="3">
							<asp:Label id="lblPayerName" runat="server" CssClass="tablelabel">Payer</asp:Label></td>
						<td colSpan="4"><asp:textbox id="txtPayerName" tabIndex="7" runat="server" CssClass="textField" Width="212px"
								MaxLength="100" Enabled="False" ReadOnly="True"></asp:textbox></td>
						<td colSpan="3"></td>
						<td colSpan="5">
							<asp:Label id="lblRemarks" runat="server" CssClass="tablelabel">Remarks</asp:Label></td>
						<td colSpan="4"><asp:textbox id="txtRemarks" tabIndex="8" runat="server" CssClass="textField" Width="276px" Height="40px"
								MaxLength="200" Rows="2" TextMode="MultiLine"></asp:textbox></td>
					</tr>
					<tr>
						<TD style="HEIGHT: 4px" width="5%"></TD>
						<TD style="HEIGHT: 4px" width="5%"></TD>
						<TD style="HEIGHT: 4px" width="5%"></TD>
						<TD style="HEIGHT: 4px" width="5%"></TD>
						<TD style="HEIGHT: 4px" width="5%"></TD>
						<TD style="HEIGHT: 4px" width="5%"></TD>
						<TD style="HEIGHT: 4px" width="5%"></TD>
						<TD style="HEIGHT: 4px" width="5%"></TD>
						<TD style="HEIGHT: 4px" width="5%"></TD>
						<TD style="HEIGHT: 4px" width="5%"></TD>
						<TD style="HEIGHT: 4px" width="5%"></TD>
						<TD style="HEIGHT: 4px" width="5%"></TD>
						<TD style="HEIGHT: 4px" width="5%"></TD>
						<TD style="HEIGHT: 4px" width="5%"></TD>
						<TD style="HEIGHT: 4px" width="5%"></TD>
						<TD style="HEIGHT: 4px" width="5%"></TD>
						<TD style="HEIGHT: 4px" width="5%"></TD>
						<TD style="HEIGHT: 4px" width="5%"></TD>
						<TD style="HEIGHT: 4px" width="5%"></TD>
						<TD style="HEIGHT: 4px" width="5%"></TD>
					</tr>
					<tr>
						<td colSpan="1"></td>
						<td colSpan="3"><asp:button id="btnInsertCons" tabIndex="9" runat="server" CssClass="queryButton" Width="63"
								Text="Insert" CausesValidation="False" Enabled="False" Visible="False"></asp:button></td>
						<td colSpan="4"></td>
						<td colSpan="3"></td>
						<td colSpan="5"></td>
						<td colSpan="4"></td>
					</tr>
					<tr>
						<td colSpan="1"></td>
						<td colSpan="19">
							<asp:datagrid id="dgAssignedConsignment" tabIndex="10" runat="server" Width="100%" AutoGenerateColumns="False"
								ItemStyle-Height="20" OnItemDataBound="dgAssignedConsignment_Bound" OnEditCommand="dgAssignedConsignment_Edit"
								OnCancelCommand="dgAssignedConsignment_Cancel" OnUpdateCommand="dgAssignedConsignment_Update"
								OnDeleteCommand="dgAssignedConsignment_Delete">
								<SelectedItemStyle CssClass="gridFieldSelected"></SelectedItemStyle>
								<ItemStyle Height="20px" CssClass="gridField"></ItemStyle>
								<Columns>
									<asp:EditCommandColumn ButtonType="LinkButton" UpdateText="&lt;IMG SRC='images/butt-update.gif' border=0 title='Save' &gt;"
										CancelText="&lt;IMG SRC='images/butt-cancel.gif' border=0 title='Cancel' &gt;" EditText="&lt;IMG SRC='images/butt-edit.gif' border=0 title='Update' &gt;">
										<HeaderStyle Width="3%" CssClass="gridHeading"></HeaderStyle>
										<ItemStyle HorizontalAlign="Left"></ItemStyle>
									</asp:EditCommandColumn>
									<asp:ButtonColumn Text="&lt;IMG SRC='images/butt-delete.gif' border=0 title='Delete' &gt;" CommandName="Delete">
										<HeaderStyle HorizontalAlign="Center" Width="3%" CssClass="gridHeading"></HeaderStyle>
										<ItemStyle HorizontalAlign="Center"></ItemStyle>
									</asp:ButtonColumn>
									<asp:TemplateColumn HeaderText="Consignment No.">
										<HeaderStyle Width="12%" CssClass="gridHeading"></HeaderStyle>
										<ItemStyle Wrap="False"></ItemStyle>
										<ItemTemplate>
											<asp:Label CssClass="gridLabel" ID="lblConsignmentNo" Text='<%#DataBinder.Eval(Container.DataItem,"Consignment_No")%>' Runat="server">
											</asp:Label>
										</ItemTemplate>
										<EditItemTemplate>
											<dbCombo:DBCOMBO id="DbConsignmentNo" ValidationProperty="Text" Width="72px" Runat="server" ServerMethod="DbConsignmentNoSelect" ShowDbComboLink="False" TextUpLevelSearchButton=" " Text='<%#DataBinder.Eval(Container.DataItem,"Consignment_No")%>' RegistrationKey='<%#ConfigurationSettings.AppSettings["DbComboRegKey"]%>' TextBoxColumns="22" AutoPostBack="True" >
											</dbCombo:DBCOMBO>
											<asp:requiredfieldvalidator id="reqDbConsignment" Runat="server" ErrorMessage="DbConsignmentNo" ControlToValidate="DbConsignmentNo"
												Display="None"></asp:requiredfieldvalidator>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Invoice Amount">
										<HeaderStyle Width="15%" CssClass="gridHeading"></HeaderStyle>
										<ItemTemplate>
											<asp:Label CssClass="gridLabelNumber" ID="lblInvoiceAmt" Runat="server" Enabled="True" Text='<%#DataBinder.Eval(Container.DataItem,"Invoice_Amt","{0:n}")%>'>
											</asp:Label>
										</ItemTemplate>
										<EditItemTemplate>
											<asp:TextBox CssClass="gridTextBoxNumber" Height="21px" ID="txtInvoiceAmt" Runat="server" ReadOnly="True" Text='<%#DataBinder.Eval(Container.DataItem,"Invoice_Amt","{0:n}")%>'>
											</asp:TextBox>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Credit/Debit Description">
										<HeaderStyle Width="30%" CssClass="gridHeading"></HeaderStyle>
										<ItemTemplate>
											<asp:Label CssClass="gridLabel" ID="lblCreditNoteDescription" Text='<%#DataBinder.Eval(Container.DataItem,"Description")%>' Runat="server" Enabled="True">
											</asp:Label>
										</ItemTemplate>
										<EditItemTemplate>
											<asp:TextBox CssClass="gridTextBox" ID="txtCreditNoteDescription" Height="21px" Text='<%#DataBinder.Eval(Container.DataItem,"Description")%>' Runat="server" MaxLength="200">
											</asp:TextBox>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Credit/Debit Remark">
										<HeaderStyle Width="15%" CssClass="gridHeading"></HeaderStyle>
										<ItemStyle Width="15%"></ItemStyle>
										<ItemTemplate>
											<asp:Label CssClass="gridLabel" ID="lblRemark" Text='<%#DataBinder.Eval(Container.DataItem,"Remark")%>' Runat="server" Enabled="True">
											</asp:Label>
										</ItemTemplate>
										<EditItemTemplate>
											<asp:TextBox CssClass="gridTextBox" ID="txtRemark" Height="21px" Text='<%#DataBinder.Eval(Container.DataItem,"Remark")%>' Runat="server" MaxLength="200">
											</asp:TextBox>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Credit/Debit Amount">
										<HeaderStyle Width="10%" CssClass="gridHeading"></HeaderStyle>
										<ItemStyle VerticalAlign="Middle"></ItemStyle>
										<ItemTemplate>
											<asp:Label CssClass="gridLabelNumber" ID="lblCreditAmt" Text='<%#DataBinder.Eval(Container.DataItem,"Amt","{0:n}")%>' Runat="server" Enabled="True">
											</asp:Label>
										</ItemTemplate>
										<EditItemTemplate>
											<cc1:mstextbox ID="txtCreditAmt" width="80px" Text='<%#DataBinder.Eval(Container.DataItem,"Amt","{0:n}")%>' Height="21px" Runat="server" CssClass="gridTextBoxNumber" AutoPostBack="false" TextMaskType="msNumeric" MaxLength="11" NumberMaxValue="99999999" NumberMinValue="0" NumberPrecision="10" NumberScale="2" TextMaskString="#.00" onblur="round(this,2)">
											</cc1:mstextbox>
										</EditItemTemplate>
									</asp:TemplateColumn>
								</Columns>
								<PagerStyle NextPageText="Next" PrevPageText="Previous" HorizontalAlign="Right" CssClass="normalText"></PagerStyle>
							</asp:datagrid>
						</td>
					</tr>
				</table>
			</div>
			<DIV id="msgPanel" style="CLEAR: both; Z-INDEX: 102; LEFT: 8px; WIDTH: 472px; POSITION: relative; TOP: 40px; HEIGHT: 120px"
				runat="server" ms_positioning="GridLayout">
				<asp:label id="lblMessage" style="Z-INDEX: 101; LEFT: 72px; POSITION: absolute; TOP: 24px"
					runat="server" CssClass="tableLabel" Width="249px" Height="30px"></asp:label>
				<asp:button id="btnToSaveChanges" style="Z-INDEX: 102; LEFT: 96px; POSITION: absolute; TOP: 48px"
					tabIndex="9" runat="server" CssClass="queryButton" Width="40px" Text="Yes" CausesValidation="False"></asp:button>
				<asp:button id="btnNotToSave" style="Z-INDEX: 102; LEFT: 137px; POSITION: absolute; TOP: 48px"
					tabIndex="9" runat="server" CssClass="queryButton" Width="40px" Text="No" CausesValidation="False"></asp:button>
				<asp:button id="btnNO" style="Z-INDEX: 103; LEFT: 178px; POSITION: absolute; TOP: 48px" tabIndex="9"
					runat="server" CssClass="queryButton" Width="63" Text="Cancel" CausesValidation="False"></asp:button>
			</DIV>
		</FORM>
	</BODY>
</HTML>
